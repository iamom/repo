/************************************************************************
*   PROJECT:        Janus
*   MODULE NAME:    CLOCK.C
*   
*   DESCRIPTION:    Code for accessing the realtime clock.
*
* ----------------------------------------------------------------------
*               Copyright (c) 2003 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
 REVISION HISTORY: 

 05-Feb-18 sa002pe on FPHX 02.12 shelton branch
 	Changed fnLocalMidniteEventHandler to put in a hard-coded 2-second delay to make sure the processing
	is done AFTER midnight because you can't use OSWakeAfter when running in a HISR.
	Added several syslog messages so we can tell what is going on w/ the timers.

 01-May-15 sa002pe on FPHX 02.12 shelton branch
	In order to make sure all the timers get set up properly when the clock is resynced:
	1. Added fnResyncTimers.
	2. Changed SetRTClockDayLightSavEnable & fnUserAdjustRTC to call fnResyncTimers
	3. Changed fnLocalMidniteEventHandler to not resync the clock because it will
		throw off the timing for all the other timers.

 21-Jan-15 sa002pe on FPHX 02.12 shelton branch
 	For Fraca 228680:
 	1. Changed ConvertFromGMTToLocal to take a BOOL indicating if the GMT correction value
		should be added in because not all of the callers are passing in the system time. Some
		are passing in a time from the PSD, which doesn't know about the GMT correction value.
 	2. Changed fnDSTAutoSwitch to change the calls to ConvertFromGMTToLocal based on the new prototype.

 03-Jun-14 Renhao     on FPHX 02.12 cienet branch
    Modified function fnAutoDateAdvance() to fix fraca 225517.

 15-May-14 Rong Junyu on FPHX 02.12 cienet branch
 	Modified function fnAutoDateAdvance() for fraca 225321.
 	When meter reached the Auto Date Advance Time when printing mail, it should  stop printing
 	and pop up the screen to prompt the date will be changed.

 07-Apr-14 sa002pe on FPHX 02.12 shelton branch
	Merged in the following changes for U.K. EIB from Janus:
	1. Changed fnUserAdjustRTC to also recalculate the auto-date advance event and the Inview midnight event.
	2. Fixed the spacing in a bunch of functions because having the opening brace at the
		end of a line makes it that much more difficult to see what is going on in a function.
	3. Changed GetSysDateTime to not use the clock drift value if GMT time sync is turned on and
		it hasn't been overridden.
	4. Changed fnGetDateParms to get the proper ducking settings based on the current print mode.

 03-APR-14 Renhao  on FPHX 02.12 cienet branch
    Updated function fnAutoDateAdvance(),fnClearDateAdvToZero() for
    G922 Rates Expiration  new feature.

 18-Feb-13 sa002pe on FPHX 02.10 shelton branch
 	Added pTimeLogBuf as a global variable because we don't want to have it
	as a local variable that could take up space on the Alarm HISR stack.
 	Changed ClockHisrHandler to put a message in the syslog to indicate when
	we're in the Alarm HISR function and when there are no Dcap events and when
	there's a Dcap timer problem so we can tell what's going on at midnight.
	Changed fnAutoDateAdvance & fnGetAutoTimeAdvanceStatus to put a message in
	the syslog to indicate why the OIT_UPDATE_DISPLAY message was sent.
	Changed fnLocalMidniteEventHandler to put a message in the syslog w/ the
	date & time the midnight processing was done.
	Changed fnAlarmTimerExpired to put a message in the syslog w/ the address of
	the timer handler function so we can tell what's going on at midnight, and
	to indicate if the address was null, or if there were no timer events available.

 2010.08.11 Raymond Shen    FPHX 02.07
  - Modified fnLocalMidniteEventHandler() to make sure the Auto Date Advance
    confirmation screen will not come out after midnight occurs.

 2010.08.11 Clarisa Bellamy  FPHX 02.07 branch
  Fraca 192992 (plus changes to oifpmain.c, oiclock.c, oiclock.h)
  - Modified fnLocalMidniteEventHandler to call fnClearDriftPastMidnightFlag.
    This flag is set when the user changes the time to the other side of midnight,
    and it is cleared when we do midnight processing, or when we detect and record
    missed midnight, whichever comes first.
   
 2010.07.16 Raymond Shen    FPHX 02.07
  - Modified fnLocalMidniteEventHandler() to resync the system clock with PSD
    at each midnight.

 2008.05.19 Clarisa Bellamy  FPHX01.11
  - Completed the replacement of absolute value with macro for SECS_IN_A_DAY.

 2008.03.06 Clarisa Bellamy  FPHX01.10
  Merge flex debit changes from Janus 15.04.02:
  - Add fnDayOfYear function.
  - Define macro for SECS_IN_A_DAY, and replace absolute value with macro.
  - Add some void typecasts when the return value of a function is ignored.
  NOTE:  Did NOT merge the Forced Upload stuff.

 17-Apr-07 sa002pe on FPHX 01.04 shelton branch
   Added DateCorrectionAmount.
   Changed fnLocalMidniteEventHandler to clear DateCorrectionAmount.
   Changed SetPrintedDateAdvanceDays & GetPrintedDateAdvanceDays to
   check bIsCorrecteDate & act accordingly.
   Added SetPrintedDateCorrectionDays, GetPrintedOrCorrectedDate & fnClearDateCorrection.
   Changed GetPrintedDate to check the print mode & act accordingly.

 05-Apr-07 sa002pe on FPHX 01.04 shelton branch
   Added definition of THISFILE.
   Changed all calls for exception logging to use THISFILE instead of __FILE__.

    28 Mar 2007 Bill Herring
                            Added fnValidateZWZPTimer to validate ZWZP timer value.
    09/08/2006  Vincent Yi  Added function fnIsCloseToMidnightCrossover, removed
                            last changes
    09/06/2006  Vincent Yi  Added wrapper functions fnSetMidnightCrossoverPendingFlag()
                            fnGetMidnightCrossoverPendingFlag()
                            Call fnSetMidnightCrossoverPendingFlag(TRUE) in 
                            fnLocalMidniteEventHandler
   09/05/2006  Dicky Sun   Update function fnLocalMidniteEventHandler for GTS
                           deleting manifest record.
   09/05/2006  Kan Jiang   Added function GetSystemDate() and fnGetSysDateParms() 
                           to fix fraca 103435.
   08/29/2006  Vincent Yi  Added function fnValidateMailInactivityTimer() 
   07/12/2006  Kan Jiang   Modify function fnGetAutoTimeAdvanceStatus() and
                           fnAutoDateAdvance() for new workflow of AutoDate Advance

   06/28/2006  Raymond Shen
   Add function fnCheckRTCTimeAMPM

 11-Jan-06 cr002de on Janus 11.00 shelton branch
   Fix spelling of 'default' case switch in fnUserAdjustRTC, its been misspelled
   since 10_xx.

 12-Dec-05 sa002pe on Janus 11.00 shelton branch
   Changed the Dcap time fudge for Janus so it's added to the offset from the PSD
   instead of the calculation of the system date & time.

 04-Nov-05 cx17598 on Janus 11.00 shelton branch

   Update date advance, always use the function call SetPrintedDateAdvanceDays
   instead of assign the variable.
   Clear the fRRUpdatePending flag anyway when the function SetPrintedDateAdvanceDays
   is called.

 23-Sep-05 sa002pe on Janus 11.00 shelton branch
   Added use of dcap time fudge in GetSysDateTime, but only for Janus.

 19-Sep-05 sa002pe on the 11.00 shelton branch
   Changed SetRTClockDayLightSavEnable to redo the scheduling of the midnight event
   based on the new Daylight Saving Time setting.

* ----------------------------------------------------------------------
* PRE-CLEARCASE REVISION HISTORY: 
*   $Log:   H:/group/SPARK/ARCHIVES/DM150/uic_app/CLOCK.C_v  $
 * 
 *    Rev 1.34   22 Jul 2005 16:35:56   HO501SU
 * Process daily index update in midnight event
 * callback function rather than associate it
 * with sleep screen or main menu check so far.
 * This solve the missing entry problems.
 * 
 *    Rev 1.33   17 Jun 2005 12:18:46   sa002pe
 * Merging in change from 1.29.1.0:
 * Took out the call to the DCAP midnight function in the midnight handler function because
 * it's already invoked via a DCAP event.  This fixes both the double entries in the daily index
 * log and adjustable log, and the double shift of the postdating log.
 * 
 *    Rev 1.32   07 Jun 2005 15:49:22   sa002pe
 * Changed the midnight stuff to set the flag to check if any
 * of the GTS manifests needs to be deleted.
 * 
 *    Rev 1.31   06 May 2005 11:51:06   sa002pe
 * 
 *    Rev 1.30   28 Mar 2005 17:35:58   CR002DE
 * 1. Don't allow advanced date to be set beyond the 
 * inspection period in SetPrintedDateAdvanceDays.
 * 
 *    Rev 1.29   07 Feb 2005 14:16:40   HO501SU
 * Added GMT correction to gmtoffset before
 * checking the range. 
 * 
 *    Rev 1.28   04 Feb 2005 18:41:16   HO501SU
 * Added GMTcorrection to sync GMT time with
 * data center.
 * 
 *    Rev 1.27   Jan 26 2005 07:11:08   NEX3RPR
 * Fix a problem - if date auto advanced more than 2 days, Time of Day of Date Advance function does not work.
 * Fix fraca 13550 - Set date advanced time to Midnight, P770 does not prompt the change of date. - David
 * 
 *    Rev 1.26   Jan 20 2005 04:14:00   NEX3RPR
 * Changed function fnLocalMidniteEventHandler() - send message from EDM to OIT at Midnight to refresh the date of the main screen. -David
 * 
 *    Rev 1.25   29 Dec 2004 16:34:00   HO501SU
 * Changed prototype of fnCMOSRateCallback()
 * to fix Fraca 13236.
 * 
 *    Rev 1.24   01 Dec 2004 11:12:54   cx08856
 * invoke dcap midnight event handler in janus
 * midnight handler if dcap is active
 * 
 *    Rev 1.23   16 Nov 2004 09:24:38   NEX8SGV
 * Fixed Date Advance Prompt problem on Midnight. - David
 * 
 *    Rev 1.22   28 Oct 2004 10:05:52   NEX8SGV
 * Changed function fnGetAutoTimeAdvanceStatus,changed bDateAdvancePrompt value  only if date advance needed.   - David 
 * 
 *    Rev 1.21   25 Oct 2004 20:04:40   sa002pe
 * In fnLocalMidniteEventHandler, added a call to fnScheduleDSTAutoChangeEvent.
 * 
 *    Rev 1.20   20 Oct 2004 09:34:18   NEX8SGV
 *  In functions fnAutoDateAdvance and fnGetAutoTimeAdvanceStatus, set Date Advanced status to Time of day status. -David
 * 
 *    Rev 1.19   Aug 24 2004 13:59:20   cx17598
 * Merged one function from comet to make the gts stuff compile. -Victor
 * 
 *    Rev 1.18   03 Aug 2004 14:40:08   CR002DE
 * 1. created generic fnDayOfYearToDayMonth
 * in the clock module.  It is to be used for 
 * Canada and German mail date translation.
 * 
 *    Rev 1.17   30 Jun 2004 13:08:30   SA002PE
 * Added stuff for Auto Daylight Saving switch, which does
 * nothing as long as the feature is disabled or the PCN
 * parameter indicates that it is disabled.
 * 
 *    Rev 1.16   17 May 2004 17:39:42   CR002DE
 * 1. change fnLocalMidniteEventHandler to send an intertask message
 * that  instigates the CMOSRateCallBack function, instead of calling it from within
 * fnLocalMidniteEventHandler, because we can't obtain the rate manager semephores
 * properly otherwise.
 * 
 *    Rev 1.15   17 May 2004 11:21:56   CR002DE
 * 1. fix fnUserAdjustRTC, there was a bug in the validation of the
 * new time setting.  Also embedded setting up the new midnight event 
 * in this function.
 * 
 *    Rev 1.14   25 Feb 2004 15:07:44   CR002DE
 * 1. fix clock drift adjustment.
 * 2. OOB time set will now be stored in timezone offset.
 * User drift adjustment will now be stored in drift.
 * 3. CMOS preserve for ver 18 will combine current drift setting
 * with current timezone offset and save it in timezone, 
 * drift will be set to zero.
 * 4. drift screens will have current allowed range displayed.
 * 
 *    Rev 1.13   Feb 13 2004 09:39:08   CX17595
 * Added one new funtion fnParseDateString(). Used to parse
 * a date string to DATATIME format. -Tim
 * 
 *    Rev 1.12   03 Dec 2003 13:31:12   defilcj
 * 1. add fnHandleDcapEvent and fnDCAPSetTimer.
 * 
 *    Rev 1.11   07 Nov 2003 14:56:52   BR507HA
 * Added alarm clock functions and midnight event.
 * 
 *    Rev 1.10   Oct 14 2003 14:13:22   CX17598
 * Added some alarm clock events functions. -Victor
 * 
 *    Rev 1.9   Oct 13 2003 16:23:34   CX17598
 * Copied some clock functions from Phoenix source. -Victor
 * 
 *    Rev 1.8   23 Sep 2003 16:40:14   defilcj
 * 1. implement VLT_INDI_DATEADV_LIMIT and 
 * VLT_INDIBACKDATE_LIMIT.
 * 
 *    Rev 1.7   19 Sep 2003 16:21:22   defilcj
 * 1. add support for user clock drift setting via set GMTOffset.
 * 
 *    Rev 1.6   Aug 26 2003 22:32:00   CX17598
 * Added check codes in the function 'SetPrintedDateAdvanceDays'.
 * -Henry
 * 
 *    Rev 1.5   22 Jul 2003 16:15:46   defilcj
 * 1. restored two utility functions:
 * ConvertFromGMTToLocal and fnAdjustMonth
 * 
 *    Rev 1.4   Jul 03 2003 11:51:06   CX17598
 * Added function 'CalcPrintedDate()', modified function'GetPrintedDate()'
 * Added functions GetPrintedDateAdvanceDays and SetPrintedDateAdvanceDays
 * -Henry
 * 
 *    Rev 1.3   04 Jun 2003 15:25:52   CX08856
 * added fnClearChronEvent()
 * 
 *    Rev 1.2   23 Apr 2003 12:06:32   BR507HA
 * Add fnValidDate, remove duplicate defines
 * 
 *    Rev 1.1   19 Mar 2003 10:13:38   defilcj
 * changes for OS_06 build
*     
*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "nucleus.h"
#include "bob.h"
#include "bobutils.h"
#include "Clock.h"
#include "cwrapper.h"
#include "custdat.h"
#include "datdict.h"
#include "features.h"
#include "global.h"

#include "math.h"
#include "oiclock.h"
#include "oit.h"
#include "scheduler.h"
#include "sys.h"
#include "cometclock.h"
#include "fwrapper.h"
#include "oiscreen.h"
//#include "deptaccount.h"
#include "oifpprint.h"
#include "Oiscrnfp.h"
#include "bkgndmgr.h"
#include "cm.h"
#include "trm.h"

#define THISFILE "clock.c"

#define SECS_IN_A_DAY   (60*60*24)


extern SetupParams CMOSSetupParams;
//extern unsigned short   usDcapActionStatus;
extern BOOL             fCMOSDSTAutoUpdated;
extern unsigned char    bDateAdvancePrompt ;
extern ZWZP_DATA    CMOSZwzpParams;//For ZWZP timer
extern BOOL bIsCorrecteDate;
extern DATETIME CMOSLastLocalMidnightDT;
extern unsigned long ulCMOSTimeSyncStatus;
extern BOOL bGoToReady;

// missing prototypes...
void fnSendStopMailRequest (void);
void fnSetAutoDateAdvConfirmFlag(BOOL  fFlag);
void fnWakeUpOIT(void);
void fnInitAlarmInterrupt( void );
/* MERGE FPHX 01.02 -> JNUS 11.14 TBD extern*/ 
BOOL fRRUpdatePending;

const unsigned char Month_days[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
const unsigned char DaysInMonth[12] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

char	pTimeLogBuf[80];


/* globals, private to this module */
static short DateAdvanceAmount = 0;     /* the current amount of days advanced.  this is not
                                            preserved between power cycles so it doesn't need
                                            to be a CMOS variable.  note that this number can
                                            be positive or negative. (It is always positive in
                                            Janus, there is no back date. -Victor) */

static  short   DateCorrectionAmount = 0;
static  BOOL    fNormalPresetEnable = FALSE;
//static  BOOL      fMidnightCrossoverPending = FALSE;

static DATETIME stMidnightDate;

BOOL AutoDateAdvanceActive;             /* if the auto date advance time has tripped, this will
                                            be TRUE until midnight */

ChronEventControlList   alarmClockEvents;           /* Event queue for the RTC alarms */
ChronEvent              autoDateAdvanceTimeEvent;   /* daily time event to advance date automatically */
ChronEvent              autoDateLocalMidniteEvent;  /* daily time event to cancel auto date advance */
extern BOOL             bMidnightProcessingStart;

//**************************  Function Prototypes ***************************
BOOL ReadRTClock(DATETIME *);




/*************************************************************/
/*              GET THE SYSTEM DATE AND TIME                 */
/* Author: R. Rudolph                                        */
/*                                                           */
/* Description: Retrieves date and time from the clock.      */
/*   Populates a structure of type DATETIME. The caller sets */
/*   AddOffsets to one if the daylight savings, Time Zone    */
/*   and Drift offsets are to be added to the time clock.    */
/*************************************************************/
BOOL GetSysDateTime(DATETIME *SysDateTimePtr, BOOL AddOffsets, BOOL Gregorian)
{
	long clockval = 0;
	long     Days;          /* julian days after clock corrections added */
	long    Time;           /* time of day after clock corrections added */
	long    OldDays;
	char WeekDay;
	BOOL retcode;


    /*** caller has selected the Gregorian calendar ***/
    if( Gregorian )
	{       
        if( ReadRTClock(SysDateTimePtr) )
		{
            clockval = ( ((SysDateTimePtr->bHour) * 3600) + ((SysDateTimePtr->bMinutes) * 60) + 
                    (SysDateTimePtr->bSeconds) );

            if (AddOffsets)
			{
                /* if daylight savings time is turned on, add the offset
                     to the real time clock */
                if (CMOSSetupParams.ClockOffset.DaylightSTactivated == TRUE)
                    clockval += (CMOSSetupParams.ClockOffset.DaylightSToff) * 60; 
        
                /* multiply by 60 since time zone is in minutes */
                clockval += (CMOSSetupParams.ClockOffset.TimeZone) * 60; 

				// if the time sync w/ GMT has been disabled -OR-
				// we're not doing GMT time sync, add the clock drift value.
				if (!fnFlashGetByteParm(BP_PBP_TIME_SYNC_ENABLE) ||
					(ulCMOSTimeSyncStatus & TIME_SYNC_DISABLED_BY_UIC))
				{
					/* multiply by 60 since drift correction is in minutes */
					clockval += (CMOSSetupParams.ClockOffset.Drift) * 60;
				}
            }

            /* GMT correction is in seconds and it applies to all cases */
            clockval += CMOSSetupParams.ClockOffset.GMTcorrection; 

            /* RTC value modified with offsets, so now recalculate the */
            /* Days and Time since this may have caused a day rollover */
            /* Days are still in julian                                */
            OldDays = MdyToJulian(SysDateTimePtr);

            Days = OldDays;

            while (clockval < 0)
            {
                Days--;
                clockval += SECS_IN_A_DAY;
            }
            
            while (clockval >= SECS_IN_A_DAY)
            {
                Days++;
                clockval -= SECS_IN_A_DAY;
            }

            /* adjust the day of the week */
            WeekDay = SysDateTimePtr->bDayOfWeek;
            WeekDay += (Days - OldDays);
			
            if (WeekDay < 1)
                WeekDay += 7;
				
            if (WeekDay > 7)
                WeekDay -= 7;
				
            SysDateTimePtr->bDayOfWeek = WeekDay;
        
            Time = clockval % SECS_IN_A_DAY;

            /* Take the calculated Days and put into MDY form */
            if (JulianToMdy(Days, SysDateTimePtr))
			{
                /* convert seconds into HHMMSS format */
                SysDateTimePtr->bHour   = (unsigned char) (Time / 3600);
                SysDateTimePtr->bMinutes = (unsigned char) ((Time % 3600) / 60);
                SysDateTimePtr->bSeconds = (unsigned char) (Time % 60);
            }
   
            retcode = TRUE;
        }
        else
            retcode = FALSE;
    }
    else
        retcode = NonGregorian( SysDateTimePtr );   
		      
    return(retcode);
}

/*************************************************************/
/*        CALCULATE DIFFERENCE BETWEEN TWO DATES/TIMES       */
/* Author: J. Mozdzer                                        */
/*                                                           */
/* Description: Returns the difference, in seconds, between  */
/*   2 date & time settings.                                 */
/*************************************************************/

BOOL CalcDateTimeDifference(DATETIME *ReferenceDT, DATETIME *TargetDT,
                            long *pDiffSeconds )
{
	long refjulian;
	long targetjulian;
	long secDifference;
	BOOL retcode = true;


    refjulian = MdyToJulian(ReferenceDT);
    targetjulian = MdyToJulian(TargetDT);

    /* make sure we won't overflow the output var.  if the difference in the
        days (+1 in case the hm&s are almost a full day apart) times
        the number of seconds per day is greater than the maxiumum
        value that fits in a signed long, we have a failure */
    if ((abs(targetjulian - refjulian) + 1) < (0x7FFFFFFF / (3600 * 24))) {

        /* initialize the difference to the number of days apart between target
            and reference dates */
        secDifference = (targetjulian - refjulian) * (3600 * 24);

        /* add the difference between the hours, minutes and seconds */
        secDifference += (TargetDT->bHour - ReferenceDT->bHour) * 3600;
        secDifference += (TargetDT->bMinutes - ReferenceDT->bMinutes) * 60;
        secDifference += (TargetDT->bSeconds - ReferenceDT->bSeconds);

        *pDiffSeconds = secDifference;
    }
    else
        retcode = false;

    return(retcode);
}


/*************************************************************/
/*        INCREMENT OR DECREMENT THE DAY BY ONE              */
/* Author: J. Mozdzer                                        */
/*                                                           */
/* Description: Advances or backs up the day in the input    */
/*         structure by one.  This function prevents         */
/*         external routines from having to know anything    */
/*         about the number of days in a month, leap year,   */
/*         etc., when wanting to adjust a DATETIME struct.   */
/*************************************************************/

void AdjustDayByOne(DATETIME *pDT, BOOL fForward)
{
    if (fForward == TRUE)
	{ 
        /* GO AHEAD ONE DAY */
        if (pDT->bDay >= Month_days[(pDT->bMonth)-1])
		{
            /* if we're on the last day of the month, we need to flip month */
            if ((pDT->bMonth == 2) && (leapyear(pDT->bYear) == true) && (pDT->bDay == 28))
                /* if it's a leap year, just increment the day to 29 */
                pDT->bDay++;
            else
			{
                if (pDT->bMonth < 12)
				{
                    /* as long as it's not December, set the day to the 1st of the next month */
                    pDT->bMonth++;
                    pDT->bDay = 1;
                }
                else
				{
                    /* otherwise we set to Jan 1st and we need to increment the year as well */
                    pDT->bMonth = 1;
                    pDT->bDay = 1;
                    if (pDT->bYear == 99)
					{
                        pDT->bYear = 0;
                        pDT->bCentury++;
                    }
                    else
                        pDT->bYear++;
                }
            }
        }
        else
            /* if increasing the day won't move us into the next month,
                this just do it and we're done */
            pDT->bDay++;

        /* when we have finished adjusting the MMDDYYYY, adjust the day of the week */
        if (pDT->bDayOfWeek == 7)
            pDT->bDayOfWeek = 1;
        else
            pDT->bDayOfWeek++;
    }
    else
	{
        /* GO BACK ONE DAY */
        if (pDT->bDay == 1)
		{
            /* if it's the first of a month, we need to go back to the previous month */
            if (pDT->bMonth == 1)
			{
                /* if Jan 1st, the month becomes December and we tick back the year */
                pDT->bMonth = 12;
                if (pDT->bYear == 0)
				{
                    pDT->bYear = 99;
                    pDT->bCentury--;
                }
                else
                    pDT->bYear--;
            }
            else
                /* if it wasn't Jan 1st, simply decrement the month */
                pDT->bMonth--;

            /* set the day to the last of the month */
            pDT->bDay = Month_days[(pDT->bMonth) - 1];

            /* if it is February of a leap year, we need to add one to the day */
            if ((pDT->bMonth == 2) && (leapyear(pDT->bYear) == true))
                pDT->bDay++;

        }
        else
            /* if we're not on the first of the month, just decement the
                day by one to go back one day */
            pDT->bDay--;

        /* when we have finished adjusting the MMDDYYYY, adjust the day of the week */
        if (pDT->bDayOfWeek == 1)
            pDT->bDayOfWeek = 7;
        else
            pDT->bDayOfWeek--;
    }
}


/*************************************************************/
/*        COMPARE  TWO DATES/TIMES                           */
/* Author: A. deAtienza                                      */
/*                                                           */
/* Description: Returns the the following:                   */
/*   > 0 when (ReferenceDT - TargetDT) > 0                   */
/*   = 0 when (ReferenceDT - TargetDT) == 0                  */
/*   < 0 when (ReferenceDT - TargetDT) < 0                   */
/*************************************************************/

long CompareDateTimes(DATETIME *ReferenceDT, DATETIME *TargetDT) 
{
    long refjulian;
    long targetjulian;


    refjulian = MdyToJulian(ReferenceDT);
    targetjulian = MdyToJulian(TargetDT);
    if(refjulian!=targetjulian)                 // Date times more than a Day apart?
    {
        return(refjulian-targetjulian);         // Return the difference in Days
    }

    refjulian = ReferenceDT->bHour * 3600;     // Get REFERENCE Date Time in seconds
    refjulian += ReferenceDT->bMinutes * 60;
    refjulian += ReferenceDT->bSeconds;

    targetjulian = TargetDT->bHour * 3600;     // Get TARGET Date Time in seconds
    targetjulian += TargetDT->bMinutes * 60;
    targetjulian += TargetDT->bSeconds;

    return(refjulian-targetjulian);             // Return the difference in seconds
}


/*************************************************************/
/*        INCREMENT OR DECREMENT THE TIME BY ONE MINUTE      */
/* Author: C. DeFilippo                                      */
/*                                                           */
/* Description: Advances or backs up the time by one minute  */
/*         in the input structure. This function prevents    */
/*         external routines from having to know anything    */
/*         about the number of days in a month, leap year,   */
/*         etc., when wanting to adjust a DATETIME struct.   */
/*************************************************************/

void AdjustMinuteByOne(DATETIME *pDT, BOOL fForward)
{
    if (fForward == TRUE)
    {
        // advance
        if(++(pDT->bMinutes) > 59)
        {
            pDT->bMinutes = 0;
            if(++(pDT->bHour) > 23)
            {
                pDT->bHour = 0;
                AdjustDayByOne(pDT,TRUE);
            }
        }                            
    }
    else
    {
        // go back
        if(--(pDT->bMinutes) == 0xff)
        {
            pDT->bMinutes = 59;
            if(--(pDT->bHour) == 0xff)
            {
                pDT->bHour = 23;
                AdjustDayByOne(pDT,FALSE);
            }
        }                            
    }
}

/************************************************************************/
/*   SET REALTIME CLOCK DAYLIGHT SAVINGS TIME ENABLED/DISABLED STATUS   */
/* Description: Enables or disables DST.  (Note the offset is set with  */
/*              a separate function).                                   */                           
/************************************************************************/

void SetRTClockDayLightSavEnable(BOOL enabled)
{
    unsigned char ucDummyData = 0;


    CMOSSetupParams.ClockOffset.DaylightSTactivated = enabled;
    if(fnValidData(BOBAMAT0, BOBID_IPSD_INIT_CLOCK, &ucDummyData) == BOB_OK)
    {
		// Recalculate the alarm timers
		fnResyncTimers();
    }
}

/************************************************************************/
/*   GET REALTIME CLOCK DAYLIGHT SAVINGS TIME ENABLED/DISABLED STATUS   */
/* Description: Returns whether or not daylight savings time is enabled */
/************************************************************************/

BOOL GetRTClockDayLightSavEnable()
{
    return (CMOSSetupParams.ClockOffset.DaylightSTactivated);
}


/*****************************************************************/
/*                      SET CLOCK MIDNIGHT                       */
/*                                                               */
/* Author: R. Rudolph                                            */
/*                                                               */
/* Description: This function takes the system clock time and    */
/*    adds the offsets to calculate the new midnight that is     */
/*    used to set the clock's alarm for midnight. When the       */
/*    clock's time equals this alarm time then an interrupt      */
/*    is generated which can be used to trigger day rollover.    */
/*                                                               */
/*****************************************************************/

void SetClockMidnight()
{
    fnCreateLocalMidniteEvent(SAF_SYNCHRONISE_WITH_MIDNIGHT);   
}

/*****************************************************************/
/*                        ClockHisrHandler                       */
/*                                                               */
/* Author: Rick Rudolph.                                         */
/*                                                               */
/* Description: This function is called when a clock alarm is    */
/*              generated from the RTC.                         */
/*                                                              */
/*  Uses                                                        */
/*                                                               */
/*****************************************************************/
void ClockHisrHandler( void ) 
{
	unsigned short  thisEvent;
	unsigned short  saf;
	BOOL            update = FALSE;


	fnDumpStringToSystemLog("Clock HISR Handler");

    thisEvent = alarmClockEvents.list[alarmClockEvents.currentTimerEvent].eventType;
    saf = alarmClockEvents.list[alarmClockEvents.currentTimerEvent].saf;

	{
		sprintf(pTimeLogBuf, "Special Action Flag = %X", saf);
		fnSystemLogEntry(SYSLOG_TEXT, pTimeLogBuf, strlen(pTimeLogBuf));
	}

    fnAlarmTimerExpired();          

    // Added for daily index update, as this does not involve screen changes
    // So far it has been processed in sleep screen and main menu check fnpJanusMainMenuCheck(), 
    // which caused missing daily index entries if meter is not in sleep or not go through
    // main menu check. 
}


/*************************************************************************************
 ** fnAutoDateAdvance
 *
 *  PARAMETERS:  none
 *
 *  DESCRIPTION:   this function is meant to be called by alarm clock event
 *                 at auto advance date alarm time.
 *                 Also adds a day to the auto date advance record and makes a new 
 *                 Alarm entry for next day.
 *  RETURNS:    none
 *  MODIFICATION HISTORY:
 *  2014.04.03   Renhao  Updated this  function  for G922 Rates Expiration  
 *               new feature.
 *
 *  2014.05.15   Rong Junyu  Updated this function  for fraca 225321.
 *
 *  2014.06.03   Renhao      Updated this function to fix fraca 225517.
 *************************************************************************************/
void    fnAutoDateAdvance (void)  
{
    BOOL    UpdateMode=TRUE;

	
	if (fnGetCurrentScreenID() == GetScreen(SCREEN_MM_INDICIA_PRINTING))	
    	fnSendStopMailRequest(); //For fraca 225321	

    if ( DateAdvanceAmount == 0 )    /* if already advanced manually, skip auto advance  */
    {
       (void)SetPrintedDateAdvanceDays( 1 );
	    bDateAdvancePrompt = DATE_ADVANCE_TIME_OF_DAY ;

         fnDumpStringToSystemLog("fnAutoDateAdvance message");

         OSSendIntertask (OIT, SYS, OIT_UPDATE_DISPLAY, BYTE_DATA, &UpdateMode, 1);

         fnSetAutoDateAdvConfirmFlag(TRUE);
    }
 
    AdjustDayByOne( &autoDateAdvanceTimeEvent.time, TRUE);   
    fnAddTimerEvent(&autoDateAdvanceTimeEvent);
    
    return;
}


/* *************************************************************************
// FUNCTION NAME: fnLocalMidniteEventHandler
//
// DESCRIPTION:
//          this function is meant to be called by alarm clock event
//      at midnite alarm time.
//      Also adds a day to the auto date advance record and makes a new 
//      Alarm entry for next day.
//      Manual date advance is also reset by this operation
//
// INPUTS:
//          None
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:
//         None
//
// MODIFICATION HISTORY:
//  01-May-15  Sandra Peterson  Don't resync the system clock w/ the PSD clock because
//                              it might cause problems w/ the Dcap midnight processing
//                              that is done after the local midnight processing.
//  08/11/2010  Raymond Shen    Clear the AutoDateAdvConfirm flag to make sure the
//                                Auto Date Advance confirmation screen will not come
//                                out after midnight occurs.
//  2010.08.11 Clarisa Bellamy - Add call to fnClearDriftPastMidnightFlag, since
//                          the current function should fix any missed midnight issues.
//    07/16/2010  Raymond Shen    Add code to resync the system clock with PSD 
//                                at each midnight.
//    09/05/2006  Dicky Sun       Add support for GTS auto-delete manifest record 
// *************************************************************************/
void fnLocalMidniteEventHandler(void)
{
    unsigned long ulInx;
    BOOL	UpdateMode = TRUE;
    unsigned char   pMsgData[2];
    int bucket;

	bMidnightProcessingStart = TRUE;
    // Wait about 2 seconds to make sure that midnight has really passed
    ESAL_PR_Delay_USec(2000000u);

/*
    // Resync the system clock with PSD at each midnight.
    if(fnValidData(BOBAMAT0, BOBID_IPSD_INIT_CLOCK, &ucDummyData) != BOB_OK)
    {
        fnProcessSCMError();
    }
*/

    (void)GetSysDateTime(&CMOSLastLocalMidnightDT, ADDOFFSETS, GREGORIAN);
    sprintf(pTimeLogBuf,"Local Midnight %02d/%02d/%02d %02d:%02d:%02d",
            CMOSLastLocalMidnightDT.bMonth, CMOSLastLocalMidnightDT.bDay, CMOSLastLocalMidnightDT.bYear,
            CMOSLastLocalMidnightDT.bHour, CMOSLastLocalMidnightDT.bMinutes, CMOSLastLocalMidnightDT.bSeconds);

    fnDumpStringToSystemLog(pTimeLogBuf);
    fnSystemLogHeader(pTimeLogBuf, 3);     // Also put in log header so it is not overwritten
   
    // Clear this flag to make sure the Auto Date Advance confirmation screen
    // will not come out after midnight occurs.
    fnSetAutoDateAdvConfirmFlag(FALSE);

    // Clear this flag, because now the LastMidnight date should be today.
    fnClearDriftPastMidnightFlag();

    //fnSetMidnightCrossoverPendingFlag(TRUE);
    fnScheduleDSTAutoChangeEvent();
    AutoDateAdvanceActive = FALSE;
    if (CMOSSetupParams.AutoAdvTime == 0)
        bDateAdvancePrompt =  DATE_ADVANCE_FROM_MIDNIGHT ;

    //SetPrintedDateAdvanceDays( 0 );     /* clear current date advance amount */
    DateCorrectionAmount = 0;
    
    OSSendIntertask (OIT, SYS, OIT_LOCAL_MIDNIGHT_OCCURRED, BYTE_DATA, &UpdateMode, 1);
// TODO - this is called from HISR and NU_Sleep and NU_Relinquish cannot be called from HISR
// See if delay is really needed and if so, implement another way.
//    OSWakeAfter(100);

//  #ifdef France_Date_Advanced_Test
//      if ( TRUE )
//  #else
//      if ( fnFlashGetByteParm( BP_MAIL_DATE_CHANGE_CONFIRMATION_REQUIRED ) == TRUE )
            OSSendIntertask (OIT, SYS, OIT_UPDATE_DISPLAY, BYTE_DATA, &UpdateMode, 1);
//  #endif  // This is for fraca 11145, Janus always does not update the date at midnight, it is not for France only.

    AdjustDayByOne( &autoDateLocalMidniteEvent.time, TRUE); 
    fnAddTimerEvent(&autoDateLocalMidniteEvent);

    //Check for and install any pending rate updates
    //(void) fnCMOSRateCallback(TRUE, NULL, TRUE, FALSE);
    // in Janus can't call rate init here because a semaphore can not be obtained, since we are
    // not operating ona  tsk thread at present
    // instead send a message to SYS and let it perform the call
    //OSSendIntertask (SYS, SYS, SYS_CMOS_RATE_CALLBACK_REQ, NO_DATA, 0, 0);


    //Stop running: Write all transaction records to flash.
    //this is to prevent multiple day mail pieces in same flash file.
    //Send mode change msg to OIT task
	pMsgData[0] = OIT_LEFT_PRINT_RDY; //OIT_LEFT_PRINT_RDY;
	pMsgData[1] = 0;
	(void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);

	fnStopRun(STOP_MAIN_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
	bGoToReady = FALSE;
	//Send Update due
	bucket = trmGetActiveBucket();
    trmSendMsgToBackground (MIDNIGHT_ACTIVITY, bucket, TRUE, 0);

}   


/*****************************************************************/
/*                        fnValidDate                            */
/*                                                               */
/* Author: Rick Rudolph.                                         */
/*                                                               */
/* Description: This function does a range check on the passed   */
/*              date parameters.                                 */
/*                                                               */
/* Returns: 1 = Successful                                       */
/*          0 = Failure                                          */
/*****************************************************************/
BOOL fnValidDate( DATETIME *pClockSettings )
{
	unsigned char DateCheck = OUT_OF_RANGE;
	unsigned char TimeCheck = OUT_OF_RANGE;


    /* check to see that date params are within range */
    if( ( (pClockSettings->bCentury) >= 19 ) && ( (pClockSettings->bCentury) <= 20) )
	{
        if( ((pClockSettings->bYear ) >= 0) && ((pClockSettings->bYear ) <= 99) )
            if( ((pClockSettings->bMonth ) >= 1) && ((pClockSettings->bMonth ) <= 12) )
                if( ((pClockSettings->bDay ) >= 1) && ((pClockSettings->bDay ) <= DaysInMonth[(pClockSettings->bMonth)-1])) 
                    if( ((pClockSettings->bDayOfWeek) >= 1) && ((pClockSettings->bDayOfWeek) <= 7) )
                        DateCheck = IN_RANGE;
    }
    
    /* check for leap year */
    if( (DateCheck == IN_RANGE) && ((pClockSettings->bMonth) == 2) && ((pClockSettings->bDay) == 29) ) 
        if( ((pClockSettings->bYear) % 4) != 0 )
            DateCheck = OUT_OF_RANGE;

    /* check the time parameters */
    if( DateCheck == IN_RANGE )
	{
        if( ((pClockSettings->bHour) >= 0) && ((pClockSettings->bHour) <= 23) )
            if( ((pClockSettings->bMinutes) >= 0) && ((pClockSettings->bMinutes) <= 59) )
                if( ((pClockSettings->bSeconds) >= 0) && ((pClockSettings->bSeconds) <= 59) )
                    TimeCheck = IN_RANGE;
    }
 
    return( (DateCheck == IN_RANGE) && (TimeCheck == IN_RANGE) );
}


/*****************************************************************/
/* FUNCTION NAME        : SetPrintedDateAdvanceDays              */
/* AUTHOR               : Joe Mozdzer                            */
/* FUNCTION DESCRIPTION : This function allows the number of     */
/*                        days that we are currently advanced    */
/*                        to be adjusted.  It should be noted    */
/*                        that a) the number can be negative     */
/*                        to support backdating, and b) the      */
/*                        function can fail if adjustment is not */
/*                        allowed for the given PCN or out of    */
/*                        the allowable range.                   */
/*****************************************************************/
BOOL SetPrintedDateAdvanceDays( short NewDays )
{
    BOOL            retcode = false;
    unsigned char   bLimit, bSysStat, bPrtStat;
    short           wOldDateAdvanceAmount;
    DATETIME        newEvent;


    /* clear the date advance pending flag anayway */
    fRRUpdatePending = FALSE;

    if (NewDays < 0)
    {
        /* if setting a negative number of date advance days, make sure
            that back dating is allowed and the number of days is within
            range.  exit returning false if not. */

        if( fnValidData( BOBAMAT0, VLT_INDI_BACKDATE_LIMIT, &bLimit ) != BOB_OK)  //get UIC pcn from CMOS
        {
            fnProcessSCMError();
            goto xit;
        }
        if ( (bLimit == 0) || ((fabs(NewDays) > bLimit) && (bLimit < 255)) )    /* note: 0=no back date; 255 means no limit */
            goto xit;
    }
    else
    {
        /* NOTE:  WE WILL NEED TO ADD A CHECK FOR REMOTE INSPECTION SOMEWHERE */

        /* if positive, make sure we don't exceed the date advance
            limit.  exit returning false if not */
        if( fnValidData( BOBAMAT0, VLT_INDI_DATEADV_LIMIT, &bLimit ) != BOB_OK)  //get UIC pcn from CMOS
        {
            fnProcessSCMError();
            goto xit;
        }

        if ((NewDays > bLimit) && (bLimit < 255))   /* note: 255 means no limit */
            goto xit;
    }

    /* if we get to here, the setting must be valid so save in a global */
    // temporarily and ...
    wOldDateAdvanceAmount = DateAdvanceAmount;
    if(bIsCorrecteDate)
        DateCorrectionAmount = NewDays;
    else
        DateAdvanceAmount = NewDays;

    // check if the new setting is within the PSD inspection period
    // if not, reject the advanced date setting and restore the old DateAdvanceAmount
    fnInspectionStatus (&bSysStat, &bPrtStat);
    if(bPrtStat == INSPECTION_REQUIRED)
    {
        DateAdvanceAmount = wOldDateAdvanceAmount;
        goto xit;
    }

    retcode = true;

xit:
    return (retcode);
}


/*****************************************************************/
/* FUNCTION NAME        : GetPrintedDateAdvanceDays              */
/* AUTHOR               : Joe Mozdzer                            */
/* FUNCTION DESCRIPTION : Returns the number of days that the    */
/*                        printed date is currently advanced.    */
/*                        This can be negative if we are back    */
/*                        dating.                                */
/*****************************************************************/
short GetPrintedDateAdvanceDays( )
{
    return (bIsCorrecteDate ? DateCorrectionAmount : DateAdvanceAmount);
}

/*****************************************************************/
/* FUNCTION NAME        : SetPrintedDateCorrectionDays               */
/* AUTHOR               : Joe Mozdzer                            */
/* FUNCTION DESCRIPTION : This function allows the number of     */
/*                        days that we are currently advanced    */
/*                        to be adjusted.  It should be noted    */
/*                        that a) the number can be negative     */
/*                        to support backdating, and b) the      */
/*                        function can fail if adjustment is not */
/*                        allowed for the given PCN or out of    */
/*                        the allowable range.                   */
/*****************************************************************/
BOOL SetPrintedDateCorrectionDays( short NewDays ) 
{
    BOOL            retcode = false;
    unsigned char   bLimit;
    DATETIME        newEvent;


    if (NewDays < 0)
    {
        /* if setting a negative number of date advance days, make sure
            that back dating is allowed and the number of days is within
            range.  exit returning false if not. */
        if( fnValidData( BOBAMAT0, VLT_INDI_BACKDATE_LIMIT, &bLimit ) != BOB_OK)  //get UIC pcn from CMOS
        {
            fnProcessSCMError();
            goto xit;
        }
		
        if ( (bLimit == 0) || ((fabs(NewDays) > bLimit) && (bLimit < 255)) )    /* note: 0=no back date; 255 means no limit */
            goto xit;
    }
    else
    {
        /* NOTE:  WE WILL NEED TO ADD A CHECK FOR REMOTE INSPECTION SOMEWHERE */

        /* if positive, make sure we don't exceed the date advance
            limit.  exit returning false if not */
        if( fnValidData( BOBAMAT0, VLT_INDI_DATEADV_LIMIT, &bLimit ) != BOB_OK)  //get UIC pcn from CMOS
        {
            fnProcessSCMError();
            goto xit;
        }

        if ((NewDays > bLimit) && (bLimit < 255))   /* note: 255 means no limit */
            goto xit;
    }

    /* if we get to here, the setting must be valid so save in a global */
    DateCorrectionAmount = NewDays;

    retcode = true;

xit:
    return (retcode);
}


/******************************************************************/
/*                                                                */
/* FUNCTION NAME        : GetPrintedDate                          */
/*                                                                */
/* FUNCTION DESCRIPTION : this function will return printed date  */
/*                                                                */
/* INPUTS               : None.                                   */
/*                                                                */
/* OUTPUTS              : The current printed date, regardless of */
/*                        date duck mode.                         */
/*                                                                */
/* RETURNS              : TRUE / FALSE.                           */
/*                                                                */
/******************************************************************/
BOOL GetPrintedDate(DATETIME *PrintedDate, BOOL Gregorian)
{
    if((fnOITGetPrintMode() == PMODE_DATE_CORRECTION ) || (bIsCorrecteDate == TRUE))
        return(CalcPrintedDate(PrintedDate, Gregorian, DateCorrectionAmount));
    else
        return(CalcPrintedDate(PrintedDate, Gregorian, DateAdvanceAmount));
}



/* *************************************************************************
// FUNCTION NAME: GetSystemDate
//
// DESCRIPTION: 
//      This function will get printed date. 
//
// INPUTS:
//
// RETURNS:
//      TRUE / FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      09/05/2006   Kan Jiang   Initial Build
// *************************************************************************/
BOOL GetSystemDate(DATETIME *PrintedDate, 
                   BOOL Gregorian) 
{
     return(CalcPrintedDate(PrintedDate, Gregorian, 0));
}


/*******************************************************************************************/
/*                                                                                         */
/* FUNCTION NAME        : CalcPrintedDate                                             */
/*                                                                                         */
/* FUNCTION DESCRIPTION : this function will calculate the printed date using the advanced */
/*                        date.                                                            */
/*                                                                                         */
/* INPUTS               : None.                                                            */
/*                                                                                         */
/* OUTPUTS              : The calculated printed date, regardless of date duck mode.       */
/*                                                                                         */
/* RETURNS              : TRUE / FALSE.                                                    */
/*                                                                                         */
/*******************************************************************************************/
BOOL CalcPrintedDate(DATETIME *PrintedDate, BOOL Gregorian, unsigned short wDateAdvanced) 
{
	BOOL retcode = false;
	DATETIME SysDateTimeBuff;
	unsigned long JulianPrintDays;
	char        WeekDay;


    if( Gregorian )
	{
        if( GetSysDateTime(&SysDateTimeBuff, ADDOFFSETS, Gregorian) )
		{
            JulianPrintDays = MdyToJulian( &SysDateTimeBuff);
            JulianPrintDays += wDateAdvanced;
            JulianToMdy(JulianPrintDays, PrintedDate);

            /* adjust the day of the week */
            WeekDay = SysDateTimeBuff.bDayOfWeek;
            WeekDay += wDateAdvanced;
			
            while (WeekDay < 1)
                WeekDay += 7;
				
            while (WeekDay > 7)
                WeekDay -= 7;
				
            PrintedDate->bDayOfWeek = WeekDay;

            retcode = true;

            // do NOT change the time part of the given date/time structure because
            // the calling function may have already set it to what it wants.
        }
        else
            retcode = false;
    }
 
    return(retcode);
}


/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : MdyToJulian                          */
/*                                                               */
/* FUNCTION DESCRIPTION : This will convert a date in month,     */
/*                        day,year format to julian format       */
/*                                                               */
/* INPUTS               : month,day,year format date             */
/*                                                               */
/* OUTPUTS              : pjulian number of days                 */
/*                                                               */
/* RETURNS              : julian number of days                  */
/*                                                               */
/*****************************************************************/

long MdyToJulian (DATETIME *p)
{
	int Month, Day, Year;
	long jt = 2299161L;             /* transition date as a Julian date (input) */
	long j;                                 /* calculate output here */
	unsigned short uy;              /* year+4900 */


    Year = ((p->bCentury) * 100) + ((p->bYear) % 100);
    Month = (p->bMonth);
    Day = (p->bDay);
        
    uy = ( Year + (Year < 0) ) + (unsigned short)4900;
    if ( Month < 3 )
	{
        uy -= 1;
        Month += 12;
    }
    
    j = ( (uy * 1461L) / 4) + (( (unsigned short) (Month + 1) * (unsigned short) 153) / (unsigned short) 5) + Day -68730;

    if ( j > jt )
	    j -= ( ((uy / (unsigned short) 100) * (unsigned short) 3) / (unsigned short) 4) - 38;

    return j;
}


/*****************************************************************/
/*                                                               */
/*                  CHECK MDY DATE TO BE VALID                   */
/*                                                               */
/*****************************************************************/

BOOL MdyValid(DATETIME *p)
{
    unsigned char retcode = false;


        /* Verify valid month */
    if (p->bMonth < 1 || p->bMonth > 12)
        return (false);

        /* Verify valid day within the valid month */
    if (p->bDay >= 1 && p->bDay <= Month_days[p->bMonth-1])
        retcode = true;
    else
    {
            /* We get here if we are trying to validate a leap day */
            /* or the day is invalid */
        if (p->bMonth == 2)
                /* Verify leap day since month is february */
            if ((p->bDay == 29) && leapyear(p->bYear))
                retcode = true;
    }
	
   return (retcode);
}


/***********************************************/
/*                                             */
/*              CHECK FOR LEAP YEAR            */
/*                                             */
/***********************************************/

BOOL leapyear(long year)
{
	BOOL retval;


   if ( ((year % 400) == 0) ||
        (((year % 4) == 0) && ((year % 100) != 0)) )
      retval = true;
    else
        retval = false;

   return (retval);
}


/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : JulianToMdy                          */
/*                                                               */
/* FUNCTION DESCRIPTION : This will convert a date in julian     */
/*                        format to month,day,year  format       */
/*                                                               */
/* INPUTS               : julian days                            */
/*                                                               */
/* OUTPUTS              : date in month,day,year format          */
/*                                                               */
/* RETURNS              : TRUE/FALSE                             */
/*                                                               */
/*****************************************************************/

BOOL JulianToMdy (long jul_days, DATETIME *p)
{
	long jt = 2299161L;                     /* transition date as a Julian date (input) */
	unsigned short Month;           /* build output month here                  */
	short Year;                                     /* build output year here                   */
	register unsigned short t;      /* general purpose temp                     */


    t = (jul_days < jt);
    jul_days += 68730L;

    if ( !t )
	{
        /* 48699*3 = 146097 = 365*400+97 = number of days in 400 years */
        t = ( (jul_days * 4) - 643) / 48699;    /*either 643 or 644 works okay here*/
        jul_days += ( ((t / (unsigned short) 3) * (unsigned short) 3) / (unsigned short) 4 ) - 38;
    }

    t = ( (jul_days * 4) - 489 ) / 1461;
    Year = t - 4900;
    t = jul_days - ( (t * 1461L) / 4 );
    Month = ( (t * (unsigned short) 5 ) - (unsigned short) 1 ) / (unsigned short) 153;

    (p->bDay) = t - ( (Month * (unsigned short) 153) / (unsigned short) 5 );

    if (--Month > 12)
	{
        Year += 1;
        Month -= 12;
    }
 
    (p->bMonth) = Month;
    (p->bYear) = (Year - (Year <= 0)) % 100;
    (p->bCentury) = (Year / 100);

    // TODO: fix this - return the correcte, expected value.
    return TRUE;
}


/******************************************************************************
   FUNCTION NAME: fnDayOfYearToDayMonth
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Calculate the Month and Day given the Day of the Year (1-365 or 366)
   PARAMETERS   : Day Of Year,
                : pointer to DATETIME (with bYear and bCentury already set)
   RETURN       : will pass back pointer to DATETIME with Day and Month set
   NOTES        : !! WaRnInG !!
                : The day of the week will not be adjusted by this function
******************************************************************************/
void fnDayOfYearToDayMonth(unsigned short wDays, DATETIME *pDT)
{
    unsigned char ucDaysInMonth;


    for(pDT->bMonth = 1; pDT->bMonth < 13; (pDT->bMonth)++)
    {
        if( (pDT->bMonth == 2) && leapyear((pDT->bCentury * 100) + pDT->bYear) )
            ucDaysInMonth = 29;
        else
            ucDaysInMonth = Month_days[pDT->bMonth-1];
                    
        if(wDays - ucDaysInMonth > 0)
            wDays -= ucDaysInMonth;
        else
            break;
    }

    pDT->bDay = wDays & 0x00FF;
}


/******************************************************************************
   FUNCTION NAME: fnDayOfYear
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Calculate the (julian) day of the year from the date time structure
             : passed in 
   PARAMETERS   : reference DateTime pointer
   RETURN       : day of the year (1 - 366)
   NOTES        : 
                : 
******************************************************************************/
unsigned short fnDayOfYear(const DATETIME *pDT)
{
    uchar   ucDaysInMonth, ucMonth;
    ushort  usDays;


    ucMonth = pDT->bMonth;
    usDays = pDT->bDay;

    while(--ucMonth >= 1)
    {
        if( (ucMonth == 2) && leapyear((pDT->bCentury * 100) + pDT->bYear) )
            ucDaysInMonth = 29;
        else
            ucDaysInMonth = Month_days[ucMonth-1];
                    
        usDays += ucDaysInMonth;
    }

    return(usDays);
}


/****************************************************************************/
/*                                                                          */
/* FUNCTION NAME        : fnGetAutoTimeAdvanceStatus                        */
/*                        wfb                                               */
/* FUNCTION DESCRIPTION : Compare current time with auto advance time       */
/*                        if past time to advance, check for "advancibility"
                          that is, not already advanced 
                          (AutoDateAdvanceActive = true) or manual date advance
                                                                            */
/* INPUTS               : none                                              */
/*                                                                          */
/*                                                                          */
/* OUTPUTS              :  AutoDateAdvanceActive and/or DateAdvanceAmount
                            may be affected                                 */
/*                                                                          */
/* RETURNS              :  TRUE / FALSE                                     */
/*                                                                          */
/****************************************************************************/
BOOL    fnGetAutoTimeAdvanceStatus(void)
{
	long        currTimeInSecs;
	DATETIME    theCurrDateTime;
	BOOL        UpdateMode = TRUE;


    AutoDateAdvanceActive = FALSE;
    if (CMOSSetupParams.AutoAdvTime != 0)
	{   /* if auto advance time is midnight, then skip it */
        (void)GetSysDateTime( &theCurrDateTime, ADDOFFSETS, GREGORIAN );
        currTimeInSecs = fnTodToSecs ( &theCurrDateTime );
        if (currTimeInSecs >= CMOSSetupParams.AutoAdvTime)
		{
            if ( (DateAdvanceAmount == 1 && bDateAdvancePrompt == DATE_ADVANCE_TIME_OF_DAY)
                 ||(DateAdvanceAmount == 0) )
            {
                DATETIME    rPrintedDateTime;
                char        pPrintedDateAscii[9];


                bDateAdvancePrompt = DATE_ADVANCE_TIME_OF_DAY ;
                fnSaveOldDateAdvanceAmount();               

                SetPrintedDateAdvanceDays( 1 );

                if (GetPrintedDate(&rPrintedDateTime, GREGORIAN) != SUCCESSFUL)
                    goto xit;
					
                (void)sprintf(pPrintedDateAscii, "%02d%02d%02d%02d", rPrintedDateTime.bCentury, rPrintedDateTime.bYear,
                            rPrintedDateTime.bMonth, rPrintedDateTime.bDay);
                pPrintedDateAscii[8] = 0;

            }

            AutoDateAdvanceActive = TRUE;

            fnDumpStringToSystemLog("fnGetAutoTimeAdvanceStatus message");

            OSSendIntertask (OIT, SYS, OIT_UPDATE_DISPLAY, BYTE_DATA, &UpdateMode, 1);
        }
    }
 
xit:
    return (AutoDateAdvanceActive);         
}       

/****************************************************************************/
/*                                                                          */

/* FUNCTION NAME        : fnTodToSecs                                       */
/*                        wfb                                               */
/* FUNCTION DESCRIPTION : This will convert an adjusted time (HMS) in DATETIME */
/*                        format secs since midnight */
/*                                                                          */
/* INPUTS               : DATETIME *aLocalDateTime                          */
/*                                                                          */
/*                                                                          */
/* OUTPUTS              :  aLocalDateTime is not changed                */
/*                                                                          */
/* RETURNS              :  value in secs since midnight     */
/*                                                                          */
/****************************************************************************/
unsigned long   fnTodToSecs ( DATETIME *aLocalDateTime )
{
	unsigned long clockval = 0;


    clockval = ( ((aLocalDateTime->bHour)*3600) + ((aLocalDateTime->bMinutes)*60) + 
                    (aLocalDateTime->bSeconds) );

    return (clockval);
}

/****************************************************************************/
/*                                                                          */
/* FUNCTION NAME        : fnTodSecsToDatetime                          */
/*                        wfb                                               */
/* FUNCTION DESCRIPTION : This will convert value in secs  */
/*                        since midnight into (HMS) in DATETIME format.     */
/*                        Offsets will be removed to give GMT if MakeGMT
                             is TRUE        */
/* INPUTS               :                                                   */
/*                        DATETIME *aLocalDateTime                          */
/*                        unsigned long gmtTod                              */
/*                        BOOL MakeGMT                                  */

/* OUTPUTS              :  aLocalDateTime members are changed               */
/*                                                                          */
/* RETURNS              : none    */
/*                                                                          */
/****************************************************************************/
void    fnTodSecsToDatetime (unsigned long Tod, DATETIME *aLocalDateTime, BOOL MakeGMT )
{
long clockval = (long) Tod;


    if (MakeGMT)
	{
        /* if daylight savings time is turned on, add the offset
        to the real time clock */
        if (CMOSSetupParams.ClockOffset.DaylightSTactivated == TRUE)
            clockval -= (CMOSSetupParams.ClockOffset.DaylightSToff) * 60; 
        
        /* multiply by 60 since time zone is in minutes */
        clockval -= (CMOSSetupParams.ClockOffset.TimeZone) * 60; 
        
        /* multiply by 60 since drift correction is in minutes */
        clockval -= (CMOSSetupParams.ClockOffset.Drift) * 60;
    
        /* GMT correction is left as it's part of local GMT */
        // clockval -= CMOSSetupParams.ClockOffset.GMTcorrection;
    
        /* if adjusted time/date is prior to today */
        while ( clockval < 0 )
		{           
            clockval += SECS_IN_A_DAY;
            AdjustDayByOne(aLocalDateTime, FALSE);  
        }
  
        /* if adjusted time/date is after today */
        while (clockval >= SECS_IN_A_DAY) {   
            clockval -= SECS_IN_A_DAY;
            AdjustDayByOne(aLocalDateTime, TRUE);   
        }
    }
 
    aLocalDateTime->bHour =    (unsigned char) (clockval / 3600);
    aLocalDateTime->bMinutes =  (unsigned char) ((clockval % 3600) / 60);
    aLocalDateTime->bSeconds =  (unsigned char) (clockval % 60);
}

/**************************************************************************************/
/*                            RANGE CHECK BCD NUMERIC DATA                            */
/*                                                                                    */
/* Author: R. Rudolph                                                                 */
/* Description: Caller sends a BCD byte and a minimum and maximum number allowed.     */
/*   This function checks to see that the 'Parameter' is within the 'MinNum' and      */
/*   'MaxNum' range. Returns 0 if ok and 1 if error.                                 */
/*                                                                                    */
/**************************************************************************************/
int DTRangeCheck(unsigned char Parameter, unsigned char MinNum, unsigned char MaxNum) 
{ 
    return( ((Parameter >= MinNum) && (Parameter <= MaxNum)) ? 1 : 0 );
}       



/*****************************************************************************/
/*                             NONGREGORIAN DATE                             */
/* Author: R. Rudolph.                                                       */
/* Purpose: This function converts a Gregorian date to a Nongregorian date.  */
/*                                                                           */
/*****************************************************************************/ 

BOOL NonGregorian( DATETIME *DateTimePtr)
{
    return(TRUE);
}

//////////////////////////// rcd modem
/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : fnClearChronEvent(unsigned short event)   */
/*                                                               */
/* FUNCTION DESCRIPTION : Searchs alarmClockEvents and removes   */
/*                        events of type evnet                   */
/*                                                               */
/* INPUTS               : Event codes defined in clock.h`````   */
/*                                                               */
/* OUTPUTS              : none                                   */
/*                                                               */
/* RETURNS              : index used for new entry               */
/*                                                               */
/*****************************************************************/  

void    fnClearChronEvent (unsigned short event)
{
	unsigned char   i;
	unsigned char   evIndex, nextIndex, prevIndex;
	unsigned short  tmp;


    evIndex = alarmClockEvents.currentTimerEvent;
    prevIndex = EMPTY_CHRON_ENTRY;
    nextIndex = alarmClockEvents.nextEvent[evIndex];    
    if (evIndex != EMPTY_CHRON_ENTRY)
	{  /* if no entries in queue, no action required */
        while (1)
		{
            if (alarmClockEvents.list[evIndex].eventType == event)
			{  /* if there is a match, remove it using one of the following */

                alarmClockEvents.numOfEvents--;        /* deduct one event */  

                /* is this the only entry on the list? */
                if ((evIndex == alarmClockEvents.currentTimerEvent) 
                        && (alarmClockEvents.nextEvent[evIndex] == LAST_CHRON_ENTRY))
				{
                    alarmClockEvents.currentTimerEvent = EMPTY_CHRON_ENTRY;  /* signify queue as empty */
                    alarmClockEvents.nextEvent[evIndex] = EMPTY_CHRON_ENTRY;     /* free the slot for reuse */
                    //rcdmodemfnStopClockAlarm();    /* stop the alarm as it is no longer needed */
                }
                else
				{
                /* or the first entry */
                    if (evIndex == alarmClockEvents.currentTimerEvent)
					{
                        alarmClockEvents.currentTimerEvent = nextIndex;       /* make the next in line the current  */  
                        alarmClockEvents.nextEvent[evIndex] = EMPTY_CHRON_ENTRY;      /* free the slot for reuse */
                        //rcdmodemfnSetRtcAlarmTime(&(alarmClockEvents.list[alarmClockEvents.currentTimerEvent])); /* activate new current event */
                    }
                    else
					{
                /* substitute in list, replacing links */
                        alarmClockEvents.nextEvent[prevIndex] = nextIndex;
                        alarmClockEvents.nextEvent[evIndex] = EMPTY_CHRON_ENTRY;      /* free the slot for reuse */
                        evIndex = prevIndex;    /* evIndex is now inactive, make prev new active */     
                    }   
                }       
            }  /* endif Event matched */
			
            if (nextIndex != LAST_CHRON_ENTRY) {
                prevIndex = evIndex;
                evIndex = nextIndex;
                nextIndex = alarmClockEvents.nextEvent[evIndex];
            }
            else
                break;
        }
    }
}


//////////////////////////// rcd modem

/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : fnStoreEvent (private to clock.c)   */
/*                                                               */
/* FUNCTION DESCRIPTION : Adds an event to the list in the order */
/*                        of execution                           */
/*                                                               */
/* INPUTS               : TimerEvent pointer                     */
/*                                                               */
/* OUTPUTS              : none                                   */
/*                                                               */
/* RETURNS              : index used for new entry               */
/*                                                               */
/*****************************************************************/  

unsigned char   fnStoreEvent(ChronEvent *newEvent)
{
	unsigned char   i;


    for (i=0; i < MAX_CHRON_TIMER_EVENTS; i++)
	{
        if (alarmClockEvents.nextEvent[i] == EMPTY_CHRON_ENTRY)
		{
            /* if at the next empty entry, file it in */
            memcpy(&alarmClockEvents.list[i].time, &newEvent->time, sizeof(DATETIME));
            alarmClockEvents.list[i].eventType = newEvent->eventType;
            alarmClockEvents.list[i].saf = newEvent->saf;
            alarmClockEvents.list[i].fn = newEvent->fn;
            alarmClockEvents.nextEvent[i] = LAST_CHRON_ENTRY;
            return (i);
        }
    }
	
    return (E_CHRON_TM_List_Full) ;
}

/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : fnAddTimerEvent                        */
/*                                                               */
/* FUNCTION DESCRIPTION : Adds an event to the list in the order */
/*                        of execution                           */
/*                                                               */
/* INPUTS               : TimerEvent pointer                     */
/*                        Note:  Dates and Times are in GMT      */
/*                                                               */
/* OUTPUTS              : none                                   */
/*                                                               */
/* RETURNS              : error_t
*                                    E_DCAPI_NoError
*                                    E_DCAPI_TM_LIST_FULL
*                                    E_DCAPI_TM_EVENT_EXPIRED
*/
/*                                                               */
/*****************************************************************/

error_t fnAddTimerEvent(ChronEvent * newEvent)
{
long            timeDiffInSecs;     /* For time difference calc */
unsigned char   evIndex, newEntryIndex, prevIndex, tmpIndex;
BOOL            Done = FALSE, TimerSetReq = FALSE;
DATETIME        currentTime;


     /* if already full, return error */
    if (alarmClockEvents.numOfEvents == MAX_CHRON_TIMER_EVENTS)     
        return (E_CHRON_TM_List_Full);

    /* check to see if the event has past */
    (void)GetSysDateTime( &currentTime, NOOFFSETS, GREGORIAN );
    (void)CalcDateTimeDifference( &currentTime,                           /* diff = alarmtime - current time */
                                  &newEvent->time, 
                                  &timeDiffInSecs);           
    /* if it has, then return error indicating attempt to queue past event */   
    if (timeDiffInSecs < 0) 
        return (E_CHRON_TM_Event_Expired);

    /* otherwise, add new event to list array   */
    newEntryIndex = fnStoreEvent(newEvent); 
    alarmClockEvents.numOfEvents++;

    /* Now, let's see where it goes in the chronological order */

    /* if the queue is empty, the new entry goes at the top */
    if (alarmClockEvents.currentTimerEvent == EMPTY_CHRON_ENTRY)
	{
       alarmClockEvents.currentTimerEvent = newEntryIndex;
       TimerSetReq = TRUE;  /* set flag to set/reset timer */
    }
    else
	{
    /* check currently scheduled events to see where the new one should go */
            evIndex = alarmClockEvents.currentTimerEvent;
            prevIndex = evIndex;
        
            do
			{
                (void)CalcDateTimeDifference(&(alarmClockEvents.list[evIndex].time), 
                                             &(newEvent->time), 
                                             &timeDiffInSecs);           /* diff = new - curr */
                if (timeDiffInSecs >= 0)
				{
                    if (alarmClockEvents.nextEvent[evIndex] != LAST_CHRON_ENTRY)
					{ /* if not earlier than this one, see if there is */
                        prevIndex = evIndex;                                 /* save this index for insertion later, if needed */
                        evIndex = alarmClockEvents.nextEvent[evIndex];       /* a next one and check again */   
                    }
                    else
					{
                        alarmClockEvents.nextEvent[evIndex] = newEntryIndex;    /* if at the end, append to the end of the list */                                      
                        Done = TRUE;                            
                    }
                }
                else
                    break; 
            } while (!Done);

            if (!Done)
			{
                /* if we are inserting before the first event, we need to switch to the new     
                    event as the current and notify the clock when we are done              */ 
                if (evIndex == alarmClockEvents.currentTimerEvent)
				{
                    tmpIndex = alarmClockEvents.currentTimerEvent;
                    alarmClockEvents.currentTimerEvent = newEntryIndex;
                    alarmClockEvents.nextEvent[newEntryIndex] =  tmpIndex;
                    TimerSetReq = TRUE; /* set flag to set/reset timer */
                }
                else
				{ 
                /* now, insert newEntryIndex into nextEntry array between prevIndex and evIndex */
                    tmpIndex = alarmClockEvents.nextEvent[prevIndex];
                    alarmClockEvents.nextEvent[prevIndex] = newEntryIndex;
                    alarmClockEvents.nextEvent[newEntryIndex] =  tmpIndex;
                }
                
            }
    }
 
    if (TimerSetReq)
        fnSetRtcAlarmTime(&(alarmClockEvents.list[alarmClockEvents.currentTimerEvent]));

    return (E_CHRON_TM_NoError);
}

/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : fnAlarmTimerExpired(void)              */
/*                                                               */
/* FUNCTION DESCRIPTION : Executes scheduled function at alarm
                            time    */
/*                                                               */
/*                                                               */
/* INPUTS               : Uses global:                           */
/*                        alarmClockEvents                */
/*                                                               */
/* OUTPUTS              : none                                   */
/*                                                               */
/* RETURNS              : TRUE/FALSE                             */
/*                                                               */
/*****************************************************************/
void    fnAlarmTimerExpired(void)
{
    TimedEventHandler	fn;						/* function to call when event occurs */
    unsigned char		currEvent, nextEvent;		  	/* index to the current active event */


    /* If the list has things to process we will, else just exit */
    if(alarmClockEvents.numOfEvents != 0 )
    {
	    currEvent =  alarmClockEvents.currentTimerEvent;
	    fn = alarmClockEvents.list[currEvent].fn;
	
	    /* do the housekeeping */
	
	    nextEvent =  alarmClockEvents.nextEvent[currEvent];
	    alarmClockEvents.nextEvent[currEvent] = EMPTY_CHRON_ENTRY;
	    alarmClockEvents.numOfEvents--;
	
	    if ( (nextEvent == LAST_CHRON_ENTRY) || (alarmClockEvents.numOfEvents == 0) ) 
		{
	        alarmClockEvents.numOfEvents = 0;
	        alarmClockEvents.currentTimerEvent = EMPTY_CHRON_ENTRY;
	    }
	    else
		{
	        alarmClockEvents.currentTimerEvent = nextEvent;
	        fnSetRtcAlarmTime(&(alarmClockEvents.list[nextEvent]));
	    }

	    /* call handler function */
	    if (fn != NULL)
		{
			sprintf(pTimeLogBuf, "Alarm Function Pointer = %08X", (UINT)fn);
			fnSystemLogEntry(SYSLOG_TEXT, pTimeLogBuf, strlen(pTimeLogBuf));
	        fn();
		}
		else
		{
			sprintf(pTimeLogBuf, "Alarm Function Pointer is NULL");
			fnSystemLogEntry(SYSLOG_TEXT, pTimeLogBuf, strlen(pTimeLogBuf));
		}
	}
    else
    {
	    fnStopClockAlarm();		/* shut it down for now */

		sprintf(pTimeLogBuf, "No Events, Stopping Clock");
		fnSystemLogEntry(SYSLOG_TEXT, pTimeLogBuf, strlen(pTimeLogBuf));
    }
}


/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : fnSetRtcAlarmTime(TimerEvent *)     */
/*                                                               */
/* FUNCTION DESCRIPTION : Sets the alarm for the passed event    */
/*                                                               */
/*                                                               */
/* INPUTS               : Uses global:                           */
/*                        a TimerEvent structure                 */
/*                        time must be in GMT to match system clock */
/* OUTPUTS              : none                                   */
/*                                                               */
/* RETURNS              : TRUE/FALSE                             */
/*                                                               */
/*****************************************************************/
void fnSetRtcAlarmTime (ChronEvent * newEvent)
{
	DATETIME  * newTime;


    newTime = &newEvent->time;

    fnStopClockAlarm();  /* stop the clock before modifying */
    
#ifdef JANUS
    (void)fnStartClockAlarmJanus( newTime); /* armed and ready    */
#else
    fnSetClockAlarm( newTime->bDay, newTime->bHour, newTime->bMinutes, newTime->bSeconds );

    fnStartClockAlarm(); /* armed and ready */
#endif
}   


/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : fnInitEventTimer()                     */
/*                                                               */
/* FUNCTION DESCRIPTION : Initializes timer event controller     */
/*                        used for auto date advance and DCAP    */
/*                                                               */
/* INPUTS               : Uses global:                           */
/*                        alarmClockEvents                       */
/*                                                               */
/* OUTPUTS              : none                                   */
/*                                                               */
/* RETURNS              : none                             */
/*                                                               */
/*****************************************************************/

void    fnInitEventTimer(void)
{
    alarmClockEvents.numOfEvents = 0;

    alarmClockEvents.currentTimerEvent = EMPTY_CHRON_ENTRY;

    memset(alarmClockEvents.list,
         0, 
         sizeof(alarmClockEvents.list));

    memset(alarmClockEvents.nextEvent,
         EMPTY_CHRON_ENTRY, 
         sizeof(alarmClockEvents.nextEvent));

    fnInitAlarmInterrupt();     /* set timer interrupt vector */
}

/**************************************************************************************************
 ** fnSetAutoAdvanceTimeEvent
 *  wfb
 *  PARAMETERS:  none
 *
 *  DESCRIPTION:   this function is meant to be called to create or update an auto date advance
 *                  event from the seconds from midnight value stored in:
                                            CMOSSetupParams.AutoAdvTime by alarm clock event

                    also creates and issues a midnight cancel event

 *                 Uses global structure:
                                            autoDateAdvanceTimeEvent
 *                  
 *  RETURNS:    none
 *
 **************************************************************************************************/
error_t     fnSetAutoAdvanceTimeEvent( void ) 
{
    error_t     err_stat = E_CHRON_TM_NoError;


    fnClearChronEvent (AUTO_DATE_ADVANCE_EVENT);    /* clear the way for a new alarm time */

    AutoDateAdvanceActive = FALSE;                /* cancel date advance for now */

//  if ( DateAdvanceAmount == 1 )
//      bDateAdvancePrompt = DATE_ADVANCE_FORCE_TO_TODAY ;
//  if ( DateAdvanceAmount < 2 ) 
//      DateAdvanceAmount = 0;                 /* if date is already advanced, setting a new time does not clear it */
    
    if (CMOSSetupParams.AutoAdvTime == 0)        /* auto advance set to local midnight, don't bother  */
        
        return (err_stat);                       /*    creating an event */

    (void)fnGetAutoTimeAdvanceStatus();               /* if time to advance, do it now */

/*  populate the autoDateAdvanceTimeEvent with the current date and GMT alarm time */
    (void)GetSysDateTime( &autoDateAdvanceTimeEvent.time, ADDOFFSETS, GREGORIAN );
    autoDateAdvanceTimeEvent.saf = SAF_NO_ACTION;
    autoDateAdvanceTimeEvent.eventType = AUTO_DATE_ADVANCE_EVENT;
    autoDateAdvanceTimeEvent.fn = fnAutoDateAdvance;

/* read the seconds from midnight value from CMOS and use it to set HMS in alarm time */
    fnTodSecsToDatetime (CMOSSetupParams.AutoAdvTime,      /* secs from midnite local time */
                         &autoDateAdvanceTimeEvent.time,   /* time event in local datetime */
                         TRUE );                           /* convert whole to GMT */ 

    if (AutoDateAdvanceActive) 
        AdjustDayByOne( &autoDateAdvanceTimeEvent.time, TRUE);

/*  then, add event to the queue */
    return (fnAddTimerEvent(&autoDateAdvanceTimeEvent));
}
    
/**************************************************************************************************
 * FUNCTION NAME: fnCreateLocalMidniteEvent
 *  DESCRIPTION:  Creates an event at midnite local time      
 *
 *  INPUTS: specialActionFlag
 *
 *  Uses global structure: autoDateLocalMidniteEvent
 *                  
 *  RETURNS:    error_t
 *
 **************************************************************************************************/
error_t  fnCreateLocalMidniteEvent(unsigned short specialActionFlag)
{
    fnDumpStringToSystemLog("Create Local Midnight");

    fnClearChronEvent (LOCAL_MIDNITE_EVENT);     // clear the way for a new alarm time 

    // populate autoDateLocalMidniteEvent with the current date and time 
    (void)GetSysDateTime( &autoDateLocalMidniteEvent.time, ADDOFFSETS, GREGORIAN );
    sprintf(pTimeLogBuf, "CurTime: %02d/%02d/%02d %02d:%02d", 
            autoDateLocalMidniteEvent.time.bMonth, autoDateLocalMidniteEvent.time.bDay, autoDateLocalMidniteEvent.time.bYear, 
            autoDateLocalMidniteEvent.time.bHour, autoDateLocalMidniteEvent.time.bMinutes);
    fnDumpStringToSystemLog(pTimeLogBuf);

    autoDateLocalMidniteEvent.saf = specialActionFlag;
    autoDateLocalMidniteEvent.eventType = LOCAL_MIDNITE_EVENT;
    autoDateLocalMidniteEvent.fn = fnLocalMidniteEventHandler;

    // set HMS in alarm time to midnite local time
    fnTodSecsToDatetime (0,
                         &autoDateLocalMidniteEvent.time, 
                         TRUE );

    AdjustDayByOne( &autoDateLocalMidniteEvent.time, TRUE);

    sprintf(pTimeLogBuf, "Next Midnight %02d/%02d/%02d %02d:%02d", 
            autoDateLocalMidniteEvent.time.bMonth, autoDateLocalMidniteEvent.time.bDay, autoDateLocalMidniteEvent.time.bYear, 
            autoDateLocalMidniteEvent.time.bHour, autoDateLocalMidniteEvent.time.bMinutes);
    fnDumpStringToSystemLog(pTimeLogBuf);

    //  then, add event to the queue 
    return (fnAddTimerEvent(&autoDateLocalMidniteEvent));
}


/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : ConvertFromGMTToLocal()                */
/*                                                               */
/* FUNCTION DESCRIPTION : Used to convert the GMT time to local  */
/*                        time by adding offsets                 */
/*                                                               */
/* INPUTS               : DATETIME *SysDateTimePtr               */
/*                        BOOL fbAddGMT							 */
/*                                                               */
/* OUTPUTS              : Local Time                             */
/*                                                               */
/*****************************************************************/

void ConvertFromGMTToLocal(DATETIME *SysDateTimePtr, BOOL fbAddGMT)
{
    long clockval = 0;
    long     Days;          /* julian days after clock corrections added */
    long    Time;           /* time of day after clock corrections added */
    long    OldDays;
    char WeekDay;
    
    
    clockval = ( ((SysDateTimePtr->bHour)*3600) + ((SysDateTimePtr->bMinutes)*60) + 
                    (SysDateTimePtr->bSeconds) );

    if (CMOSSetupParams.ClockOffset.DaylightSTactivated == TRUE)
        clockval += (CMOSSetupParams.ClockOffset.DaylightSToff) * 60;
		
    /* multiply by 60 since time zone is in minutes */
    clockval += (CMOSSetupParams.ClockOffset.TimeZone) * 60;
	
    /* multiply by 60 since drift correction is in minutes */
    clockval += (CMOSSetupParams.ClockOffset.Drift) * 60;

    // GMT correction has already been added when gettting system time, 
    // but not when using a timestamp from the PSD,
	// so add if necessary
	if (fbAddGMT == TRUE)
		clockval += CMOSSetupParams.ClockOffset.GMTcorrection;

    OldDays = MdyToJulian(SysDateTimePtr);
    Days = OldDays;

    while (clockval < 0)
    {
        Days--;
        clockval += SECS_IN_A_DAY;
    }
        
    while (clockval >= SECS_IN_A_DAY)
    {
        Days++;
        clockval -= SECS_IN_A_DAY;
    }

    /* adjust the day of the week */
    WeekDay = SysDateTimePtr->bDayOfWeek;
    WeekDay += (Days - OldDays);
	
    if (WeekDay < 1)
        WeekDay += 7;
		
    if (WeekDay > 7)
        WeekDay -= 7;
		
    SysDateTimePtr->bDayOfWeek = WeekDay;

    Time = clockval % SECS_IN_A_DAY;

    /* Take the calculated Days and put into MDY form */
    if (JulianToMdy(Days, SysDateTimePtr)) {
        /* convert seconds into HHMMSS format */
        SysDateTimePtr->bHour   = (unsigned char) (Time / 3600);
        SysDateTimePtr->bMinutes = (unsigned char) ((Time % 3600) / 60);
        SysDateTimePtr->bSeconds = (unsigned char) (Time % 60);
    }
}


/******************************************************************************
   FUNCTION NAME: fnAdjustMonth
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Adjust a passed DateTime by signed cNumMonths                 
   PARAMETERS   : DATETIME to set as adjust
   RETURN       : none
   NOTES        : !! WaRnInG !!
                : The day of the week will not be adjusted by this function
******************************************************************************/
void fnAdjustMonth(DATETIME *pDT, char cNumMonths)
{
	BOOL    fEndOfMonth = false;


    // determine if the initial date is the end of the initial month
    if( (leapyear((pDT->bCentury * 100) + pDT->bYear)) &&
        (pDT->bMonth == 2) &&
        (pDT->bDay == 29) )
    {
        fEndOfMonth = true;
    }
    else
    {
        if(pDT->bDay == Month_days[pDT->bMonth -1]) 
            fEndOfMonth = true;
    }
        
    //go forward
    if(cNumMonths > 0)
    {
        while(cNumMonths--)
        {
            if(pDT->bMonth < 12)
            {
                pDT->bMonth++;
            }
            else
            {
                pDT->bMonth = 1;
                if (pDT->bYear < 99)
                {
                    pDT->bYear++;
                }
                else
                {
                    pDT->bYear = 0;
                    pDT->bCentury++;                    
                }
            }               
        }
    }
    else
    //go backward
    {
        while(cNumMonths++)
        {
            if(pDT->bMonth > 1)
            {
                pDT->bMonth--;
            }
            else
            {
                pDT->bMonth = 12;
                if (pDT->bYear != 0)
                {
                    pDT->bYear--;
                }
                else
                {
                    pDT->bYear = 99;
                    pDT->bCentury--;                    
                }
            }               
        }
    }

    // if initial date was the end of the initial month, OR
    // if the adjusted day is beyond the end of the adjusted month
    //  then set the adjusted day to the end of the adjusted month
    if( (fEndOfMonth) || (pDT->bDay > Month_days[pDT->bMonth -1]) )
    {
        pDT->bDay = Month_days[pDT->bMonth -1];

        if( (leapyear((pDT->bCentury * 100) + pDT->bYear)) &&
            (pDT->bMonth == 2) )
        {
            pDT->bDay = 29;
        }
    }
}

/******************************************************************************
   FUNCTION NAME: fnUserAdjustRTC
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Set the both the UIC shadow clock drift and the PSD RTC drift
   PARAMETERS   : ADJUST_DRIFT or ADJUST_TIMEZONE
   RETURN       : OUT_OF_RANGE 0 or IN_RANGE 1
   NOTES        : none
******************************************************************************/
char fnUserAdjustRTC(DATETIME* pdtNewDateTime, uchar ucOffsetType)
{
    long lWasOffset, lDiffSecs = 0, gmtOffset = 0;
    DATETIME dtNow;
    char retval = OUT_OF_RANGE;
    char retval2 = FALSE;
    uchar ucDummyData = 0;
    char  tmpLog[50]; // for debugging
    extern  void putString(char *, int);


    if( ucOffsetType == ADJUST_GMT ) 
        retval2 = (char)GetSysDateTime(&dtNow,NOOFFSETS, GREGORIAN);
    else 
        retval2 = (char)GetSysDateTime(&dtNow,ADDOFFSETS, GREGORIAN);

    if(retval2)
    {
        if(CalcDateTimeDifference(&dtNow, pdtNewDateTime, &lDiffSecs))
        {
            // Add together the TimeZone offset, drift and (if activated) daylight 
            //  savings offset.

            // Timezone offset is in minutes, convert to seconds and add it.  
            gmtOffset += ((long)(CMOSSetupParams.ClockOffset.TimeZone) * 60); 

            // Drift correction is in minutes, convert to seconds and add it.
            gmtOffset += ((long)(CMOSSetupParams.ClockOffset.Drift) * 60);

            // add new drift component for bounds check
            gmtOffset += (long) lDiffSecs;

            // If daylight savings time is turned on, add it to the offset (always 1 hour)
            if( CMOSSetupParams.ClockOffset.DaylightSTactivated == TRUE )
                gmtOffset += ((long)(CMOSSetupParams.ClockOffset.DaylightSToff) * 60);

            // Add GMT time correction
            gmtOffset += CMOSSetupParams.ClockOffset.GMTcorrection;

            if(fnCheckGMTOffset(gmtOffset) == IN_RANGE)
            {
                // save the current drift or timezone value in case the vault rejects the new GMTOffset
                // and add new drift component now so BOB script builds the new GMTOffset correctly
                switch( ucOffsetType )
                {
                case ADJUST_DRIFT:
                    lWasOffset = CMOSSetupParams.ClockOffset.Drift;
                    CMOSSetupParams.ClockOffset.Drift += (long) (lDiffSecs/60);
                    break;

                case ADJUST_GMT:
                    (void)sprintf(tmpLog, "GMT current correction %d add %d", CMOSSetupParams.ClockOffset.GMTcorrection, lDiffSecs);
                    putString(tmpLog, __LINE__);
                    lWasOffset = CMOSSetupParams.ClockOffset.GMTcorrection;
                    CMOSSetupParams.ClockOffset.GMTcorrection += lDiffSecs;
                    break;

                default: // ADJUST_TIMEZONE
                    lWasOffset = CMOSSetupParams.ClockOffset.TimeZone;
                    CMOSSetupParams.ClockOffset.TimeZone += (long) (lDiffSecs/60);
                    break;
                }

                if(fnValidData(BOBAMAT0, BOBID_IPSD_INIT_CLOCK, &ucDummyData) == BOB_OK)
                {
                    retval = IN_RANGE;

					// Recalculate the alarm timers
					fnResyncTimers();
                }
                else
                {   
                    // restore CMOS drift or timezone component
                    if( ucOffsetType == ADJUST_DRIFT)
                        CMOSSetupParams.ClockOffset.Drift = lWasOffset;
                    
                    else if( ucOffsetType == ADJUST_GMT)
                        CMOSSetupParams.ClockOffset.GMTcorrection = lWasOffset;

                    else
                        CMOSSetupParams.ClockOffset.TimeZone = lWasOffset;

                    fnProcessSCMError();
                }
            }
        }
    }
    
    return(retval);
}

/**************************************************************************************************
 ** fnHandleDcapEvent
 *
 *  PARAMETERS:  none
 *
 *  DESCRIPTION:   this function is meant to be called by alarm clock event
 *                 at when a DCAP related timed event Expires.
 *  RETURNS:    none
 *
 **************************************************************************************************/
 void fnHandleDcapEvent (void)
 {
       return;
}

/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : fnDCAP_SetTimer                        */
/*                                                               */
/* FUNCTION DESCRIPTION : converts the ExpireTime in mSecs and specialActionFlags
                            to a corresponding TimerEvent and queues it.
*/
/*                                                               */
/* INPUTS               : unsigned long ExpireTime               */
/*                        unsigned short specialActionFlags      */
/*                                                               */
/* OUTPUTS              : adds a new event to the chron queue    */
/*                                                               */
/* RETURNS              : error_t
                                    E_DCAPI_NoError             
                                    E_DCAPI_TM_LIST_FULL        
                                    E_DCAPI_TM_EVENT_EXPIRED
*/
/*                                                               */
/*****************************************************************/
error_t fnDCAPSetTimer(unsigned long ExpireTime, unsigned short specialActionFlags)
{
	long        TodInSecs;
	ChronEvent  newEvent;
	DATETIME    currentTime;
    
	
    if (specialActionFlags & SAF_SYNCHRONISE_WITH_MIDNIGHT)
        return (fnCreateLocalMidniteEvent(specialActionFlags));
    else
	{
        (void)GetSysDateTime (&currentTime, NOOFFSETS, GREGORIAN);
        TodInSecs = fnTodToSecs(&currentTime);   /* convert to GMT */
        TodInSecs += (long) (ExpireTime / 1000);
        fnTodSecsToDatetime (TodInSecs, &currentTime, TRUE ); // Get GMT time
        memcpy(&newEvent.time, &currentTime, sizeof(DATETIME));
        newEvent.eventType = DCAP_TIMED_EVENT;
        newEvent.saf = specialActionFlags;
        newEvent.fn = fnHandleDcapEvent;

        fnClearChronEvent (DCAP_TIMED_EVENT);   /* clear the way for a new dcap alarm event be removing others */

        return (fnAddTimerEvent(&newEvent));
    }
}

/******************************************************************************
   FUNCTION NAME: fnParseDateString
   AUTHOR       : Tim Zhang
   DESCRIPTION  : This function parse a given date string to the DATETIME struct according to 
                 the given format string.
   PARAMETERS   : format - a string like "yymmdd" means that the string to format is in the format
                of year/month/day format.
                  datestring - the string to parse
                  delimiter - the delimiter between the year/month/day in the datestring.
                  dt - Pointer the the return value.
   RETURN       : 0 - if the date is successfully parsed.
               >1 - if something is wrong
   NOTES        : none
******************************************************************************/
unsigned char  fnParseDateString( char * format, char * datestring, char delimiter, DATETIME *dt )
{
    unsigned int p[3];/* temporary buffer to store the parsed number */
    unsigned int yy, dd, mm;
    
    char input_format[10];
    char *pchar;
    unsigned char ind;


    /* build the input string used by sscanf with the passed in delimiter. */
    (void)sprintf(input_format, "%%u%c%%u%c%%u", delimiter,delimiter);

    /* input the number to buffer. */
    if (3 != sscanf( datestring, input_format, &p[0], &p[1], &p[2] ))
        return 1; // general error

    /* now store the correct value according to the passed in format */

    /* get year firstly */
    pchar = strchr(format, 'y');
    ind = pchar - format; /* now we get the index that 'y' first appears. */    
    ind = (unsigned char ) (ind /2 ); /* now the ind becomes the index in p[]. */
    yy = p[ind];

    /* get month secondly */
    pchar = strchr(format, 'm');
    ind = pchar - format; /* now we get the index that 'y' first appears. */    
    ind = (unsigned char ) (ind /2 ); /* now the ind becomes the index in p[]. */
    mm = p[ind];

    /* get day  */
    pchar = strchr(format, 'd');
    ind = pchar - format; /* now we get the index that 'y' first appears. */    
    ind = (unsigned char ) (ind /2 ); /* now the ind becomes the index in p[]. */
    dd = p[ind];    

    if ( mm > 12 )
        return MONTH_OUTOF_RANGE;

    if ( dd > 31)
        return DAY_OUTOF_RANGE;

    /* now fill in the return value */
    memset( dt, 0, sizeof(DATETIME) );
    
    dt->bCentury = (unsigned char) (yy/100);
    dt->bYear = ( unsigned char ) (yy - (unsigned int)(yy/100) * 100);
    dt->bMonth = (unsigned char )mm;
    dt->bDay = (unsigned char )dd;
    
    return 0;
}


/****************************************************************************/
/*                                                                          */
/* FUNCTION NAME        : fnAdjustDateBySecs                                */
/*                        Joe Mozdzer                                       */
/* FUNCTION DESCRIPTION : This will convert adjust a passed in date/time    */
/*                        by a passed in number of seconds                  */
/*                                                                          */
/****************************************************************************/
void fnAdjustDateBySecs (DATETIME *pDateTime, long lSeconds)
{
    long    lSecsSinceMidnight;


    lSecsSinceMidnight = (long)fnTodToSecs( pDateTime );
    lSecsSinceMidnight += lSeconds;

    /* check if we have advanced into tomorrow */
    if (lSecsSinceMidnight >= SECS_IN_A_DAY)
    {
        AdjustDayByOne(pDateTime, TRUE);
        lSecsSinceMidnight -= SECS_IN_A_DAY;
    }
    else
    {
        /* check if we have retreated into yesterday */
        if (lSecsSinceMidnight < 0)
        {
            AdjustDayByOne(pDateTime, FALSE);
            lSecsSinceMidnight += SECS_IN_A_DAY;
        }
    }

    fnTodSecsToDatetime( (ulong)lSecsSinceMidnight, pDateTime, FALSE );
    return;
}


/******************************************************************************
   FUNCTION NAME: fnDSTAutoChangeCallback
   AUTHOR       : Joe Mozdzer
   DESCRIPTION  : Scheduler callback function that executes when it is time
                :  to do an automatic daylight savings time enable/disable
   PARAMETERS   : eventInfoPtr- pointer to the scheduler information block
   RETURN       : none
******************************************************************************/
void fnDSTAutoChangeCallback(EVENT_SCHEDULER_INFO *eventInfoPtr)
{
    static volatile STATUS status;
    char cExceptionMsg[60];
    DSTState DSTEnabledState;


    DSTEnabledState = (DSTState) GetRTClockDayLightSavEnable();
    if (DSTEnabledState != (DSTState) eventInfoPtr->eventData)
    {
        SetRTClockDayLightSavEnable((BOOL) eventInfoPtr->eventData);
        /* Set flag to indicate screen needs to be shown. */
        fCMOSDSTAutoUpdated = TRUE;
    }

    if((status = delEventSchedulerInfo(eventInfoPtr)) != NU_SUCCESS)
    {
        (void)sprintf(cExceptionMsg,"delEventSchedulerInfo() Error #%d", status); 
        taskException(cExceptionMsg, THISFILE, __LINE__);
    }
}

/******************************************************************************
   FUNCTION NAME: fnScheduleDSTAutoChangeEvent
   AUTHOR       : Joe Mozdzer
   DESCRIPTION  : Checks PCN parameters to determine if and when daylight
                :  savings time will be automatically enabled or disabled. 
                :  If this is to happen automatically, the scheduler is configured
                :  with an event for the appropriate time.
                :  This is the function other modules need to call when they
                :  need to have the DST event updated.
                :
   PARAMETERS   : none
   RETURN       : none
   NOTE         : This function should not be called either directly or
                :  indirectly inside an alarm callback function.  Strange things
                :  happen when that is attempted.
******************************************************************************/
//unsigned short    fwdtime = 0x3a17;   // use for testing if needed
//unsigned short    bcktime = 0x000a;   // use for testing if needed
void fnScheduleDSTAutoChangeEvent()
{
    DSTParams   rDSTFwd, rDSTBack;
    DATETIME    rDateTime;
    BOOL        fDateStatus;


    if (fnFlashGetByteParm(BP_DST_AUTO_TIME_TYPE) == LOCAL)
    {
        if (!(fDateStatus = GetSysDateTime(&rDateTime, ADDOFFSETS, GREGORIAN)))
        {
            taskException("GetSysDateTime() failure", THISFILE, __LINE__);
        }
    }
    else
    {
        if (!(fDateStatus = GetSysDateTime(&rDateTime, NOOFFSETS, GREGORIAN)))
        {
            taskException("GetSysDateTime() failure", THISFILE, __LINE__);
        }
    }

    rDSTFwd.uAfterDay = fnFlashGetByteParm(BP_DST_AUTO_FORWARD_AFTER_DAY);
    rDSTFwd.uDay = fnFlashGetByteParm(BP_DST_AUTO_FORWARD_DAY);
    rDSTFwd.uDayType = fnFlashGetByteParm(BP_DST_AUTO_FORWARD_DAY_TYPE);
    rDSTFwd.uMonth = fnFlashGetByteParm(BP_DST_AUTO_FORWARD_MONTH);
    rDSTFwd.uTime = fnFlashGetWordParm(WP_DST_AUTO_FORWARD_TIME);
    rDSTFwd.uTimeType = fnFlashGetByteParm(BP_DST_AUTO_TIME_TYPE);
    rDSTFwd.uYear = (rDateTime.bCentury * 100) + rDateTime.bYear;

    rDSTBack.uAfterDay = fnFlashGetByteParm(BP_DST_AUTO_BACKWARD_AFTER_DAY);
    rDSTBack.uDay = fnFlashGetByteParm(BP_DST_AUTO_BACKWARD_DAY);
    rDSTBack.uDayType = fnFlashGetByteParm(BP_DST_AUTO_BACKWARD_DAY_TYPE);
    rDSTBack.uMonth = fnFlashGetByteParm(BP_DST_AUTO_BACKWARD_MONTH);
    rDSTBack.uTime = fnFlashGetWordParm(WP_DST_AUTO_BACKWARD_TIME);
    rDSTBack.uTimeType = fnFlashGetByteParm(BP_DST_AUTO_TIME_TYPE);
    rDSTBack.uYear = (rDateTime.bCentury * 100) + rDateTime.bYear;

    // use section below for testing if needed
/*  rDSTFwd.uAfterDay = 0;
    rDSTFwd.uDay = 3;       // wednesday
    rDSTFwd.uDayType = 2; // second weekday of the month
    rDSTFwd.uMonth = 4; // april
    rDSTFwd.uTime = fwdtime;    // min|hr 
    rDSTFwd.uTimeType = 0; // gmt

    rDSTBack.uAfterDay = 0;
    rDSTBack.uDay = 4;      // thurs
    rDSTBack.uDayType = 3; // 3rd weekday of month
    rDSTBack.uMonth = 4; // april
    rDSTBack.uTime = bcktime;// min|hr
    rDSTBack.uTimeType = 0; // gmt
*/

    (void)fnDSTAutoSwitch(&rDSTFwd, &rDSTBack);
}


/***************************************************************************/
/*                                                                         */
/* FUNCTION       : fnDayOfWeek                                            */
/*                                                                         */
/* DESCRIPTION    : Calculuate the day of the week for any date.           */
/*                                                                         */
/* AUTHOR         : Lynn W. D'Amico                                        */
/*                                                                         */
/* DATE           : 15-March-2004                                          */
/*                                                                         */
/* PRECONDITIONS  : Function given a valid date (day, month, year)         */
/*                                                                         */
/* POSTCONDITIONS : Returns day of week where 0 = Sunday, 1 = Monday, etc. */
/*                                                                         */
/***************************************************************************/

unsigned int fnDayOfWeek(unsigned int year, unsigned int month, unsigned int day)
{
   int a = (14 - month) / 12;
   int y = (year - a);
   int m = (month + (12 * a) - 2);
   unsigned int dow;


   // Calculate the day of the week for the specified date (dd/mm/yyyy)
   dow = ((day + y + (y / 4) - (y / 100) + (y / 400) + ((31 * m) / 12)) % 7);

   return (dow);
} // end fnDayOfWeek


/****************************************************************************/
/*                                                                         */
/* FUNCTION       : fnCalcDSTSwitchDay                                     */
/*                                                                         */
/* DESCRIPTION    : Calculuate the day of the month, given a date and the  */
/*                  the type of day (i.e. a specific day or a certain      */
/*                  weekday, such as "first Sunday" of April).             */
/*                                                                         */
/* AUTHOR         : Lynn W. D'Amico                                        */
/*                                                                         */
/* DATE           : 20-March-2004                                          */
/*                                                                         */
/* PRECONDITIONS  : Function given a valid date (day, month, year) and day */
/*                  type and after day (sent in as a structure).           */
/*                                                                         */
/* POSTCONDITIONS : Returns day of month corresponding to day type and     */
/*                  date information.                                      */
/*                                                                         */
/* NOTES          : This function compiles properly and has been tested.   */
/*                                                                         */
/***************************************************************************/

unsigned int fnCalcDSTSwitchDay(DSTParams *pDSTpar)
{
                                                    // Function inputs (from DSTParams structure):
    unsigned int dayType = pDSTpar->uDayType;       // Day type
    unsigned int year = pDSTpar->uYear;             // Year
    unsigned int month = pDSTpar->uMonth;           // Month
    unsigned int day = pDSTpar->uDay;               // Day  
    unsigned int afterDay = pDSTpar->uAfterDay;     // After day

                                                    // Variables used in date calculations:
    BOOL leapYear;                                  // TRUE if specified year is a lear year
    unsigned int maxDays;                           // Maximum number of days in specified month
    unsigned int curDay;                            // Date of the current day (used during calculations)
    unsigned int dow;                               // Day of the week
    unsigned int dom;                               // Day of the month
    unsigned int daysInMonth[MAX_MONTHS] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; // Days in each month


    // Calculate the actual date within a month and year of a specific weekday and week.
    // An example: "What is the date of the first Sunday in April, 2004?"
    // For dayType: 0 = Specific day; 1 = First Week, 2 = Second Week, 3 = Third Week, 4 = Forth Week, 5 = Last Week
    // For afterDay: 0 = None; 1-31 = On or after specified day of month
    
    if (dayType == 0)
    {
        // Day Type = 0; Day parameter represents the specific (actual) day of the month
        dom = day;
    }
    else
    {
        // Day Type != 0; Calculate the day of month based on parameter values
        // Subtract one from dayType (used later in date calculations)
        dayType -= 1;

        // It is leap year if the year is divisible by 4 and it's not the turn of a century, except
        // when the turn of the century is a year that is a multiple of 400 (for Gregorian calendar).
        if (((year % 400) == 0) || (((year % 4) == 0) && ((year % 100) != 0)))
        {
            leapYear = TRUE;
        }
        else
        {
            leapYear = FALSE;
        }

        // See if an adjustment to February is needed for leap year
        maxDays = daysInMonth[month -1];
        if (month == 2 && leapYear == TRUE)
        {
            maxDays += 1;
        }

        // Caculate the actual date of the targeted Day Of Month;
        // Start by finding the first desired "weekday" of the month.
        curDay = 0;
        do
        {
            curDay += 1;
            dow = fnDayOfWeek(year, month, curDay);
        } while ((dow != day) && (curDay < MAX_WKDAYS));

        // Now determine the actual date based on the specified week.
        if (dayType <= 3)
        {
            // We want the first, second, third or forth week of the month;
            // Add the correct number of days, depending on which week.
            dom = curDay + (dayType * 7);
            // Check if there is an after day and if so, ensure day of month is on/after that day.
            while ((afterDay != 0) && (dom < afterDay) && ((dom + 7) <= maxDays))
            {
                dom += 7;
            }
        }
        else
        {
            // We want the day in the last week of the month.
            dom = curDay + 21;
            if ((dom + 7) <= maxDays)
            {
                dom += 7;
            }
        }
    }
	
    return (dom);
} // end fnCalcDSTSwitchDay


/***************************************************************************/
/*                                                                         */
/* FUNCTION       : fnCalcDSTFwdBackInfo                                   */
/*                                                                         */
/* DESCRIPTION    : Calculuate the dates, times and types of "switches"    */
/*                  within a specified year for the forward and bacward    */
/*                  adjustments to the time based on Daylight Saving Time  */
/*                  enabling and disabling.                                */
/*                                                                         */
/* AUTHOR         : Lynn W. D'Amico/Joe Mozdzer                            */
/*                                                                         */
/* DATE           : 21-March-2004                                          */
/*                                                                         */
/* PRECONDITIONS  : Function given a forward/backward DST parameters.      */
/*                                                                         */
/* POSTCONDITIONS : Returns dates, types and times for the "Switch 1"      */
/*                  (first) and "Switch 2" (last) DST time adjustments     */
/*                  for the specified year.                                */
/*                                                                         */
/* NOTES          : This function compiles properly and has been tested.   */
/*                                                                         */
/***************************************************************************/

void fnCalcDSTFwdBackInfo(DSTParams *pDSTFwd, DSTParams *pDSTBack, DSTSwitch *pDSTSwitch)
{
    DATETIME rFwdDT, rBackDT;
    long     lTimeDelta;
	
	
    // Note: The Year and Time Type parameters are assumed to be the same in 
    // ----  both the forward and backward parameter structures.

    rFwdDT.bYear = pDSTFwd->uYear % 100;
    rFwdDT.bCentury = pDSTFwd->uYear / 100;
    rFwdDT.bMonth = pDSTFwd->uMonth;
    rFwdDT.bDay = fnCalcDSTSwitchDay(pDSTFwd);
    rFwdDT.bHour = pDSTFwd->uTime & 0xFF;
    rFwdDT.bMinutes = (pDSTFwd->uTime & 0xFF00) / 0x0100;
    rFwdDT.bSeconds = 1;

    rBackDT.bYear = pDSTBack->uYear % 100;
    rBackDT.bCentury = pDSTBack->uYear / 100;
    rBackDT.bMonth = pDSTBack->uMonth;
    rBackDT.bDay = fnCalcDSTSwitchDay(pDSTBack);
    rBackDT.bHour = pDSTBack->uTime & 0xFF;
    rBackDT.bMinutes = (pDSTBack->uTime & 0xFF00) / 0x0100;
    rBackDT.bSeconds = 1;

    /* if the times are specified in local time, we must eliminate the DST offset from the
        time provided since daylight time is expected to be enabled when the time occurs.
        this will enable us to make a fair comparison to see which time is really later */
    if (pDSTBack->uTimeType == LOCAL)
        fnAdjustDateBySecs (&rBackDT, -(CMOSSetupParams.ClockOffset.DaylightSToff * 60));
    
    (void)CalcDateTimeDifference(&rFwdDT, &rBackDT, &lTimeDelta);

    // Check which DST switch month (forward or backward) preceeds the other
    if (lTimeDelta > 0)
    {
        // Backward month is greater (chronologically later) than forward month;
        // Set Switch 1 parameters = forward parameters
        pDSTSwitch->eDSTSwitch_1_Type = ENABLED;
        pDSTSwitch->uDSTSwitch_1_Day = fnCalcDSTSwitchDay(pDSTFwd);
        pDSTSwitch->uDSTSwitch_1_Month = pDSTFwd->uMonth;
        pDSTSwitch->uDSTYear = pDSTFwd->uYear;
        pDSTSwitch->uDSTSwitch_1_Time = pDSTFwd->uTime;
        pDSTSwitch->uDSTTimeType = pDSTFwd->uTimeType;

        // Set Switch 2 parameters = backward parameters
        pDSTSwitch->eDSTSwitch_2_Type = DISABLED;
        pDSTSwitch->uDSTSwitch_2_Day = fnCalcDSTSwitchDay(pDSTBack);
        pDSTSwitch->uDSTSwitch_2_Month = pDSTBack->uMonth;
        pDSTSwitch->uDSTSwitch_2_Time = pDSTBack->uTime;
    }
    else
    {
        // Forward month is greater (chronologically later) than backward month;
        // Set Switch 1 parameters = backward parameters
        pDSTSwitch->eDSTSwitch_1_Type = DISABLED;
        pDSTSwitch->uDSTSwitch_1_Day = fnCalcDSTSwitchDay(pDSTBack);
        pDSTSwitch->uDSTSwitch_1_Month = pDSTBack->uMonth;
        pDSTSwitch->uDSTYear = pDSTBack->uYear;
        pDSTSwitch->uDSTSwitch_1_Time = pDSTBack->uTime;
        pDSTSwitch->uDSTTimeType = pDSTBack->uTimeType;

        // Set Switch 2 parameters = forward parameters
        pDSTSwitch->eDSTSwitch_2_Type = ENABLED;
        pDSTSwitch->uDSTSwitch_2_Day = fnCalcDSTSwitchDay(pDSTFwd);
        pDSTSwitch->uDSTSwitch_2_Month = pDSTFwd->uMonth;
        pDSTSwitch->uDSTSwitch_2_Time = pDSTFwd->uTime;
    }
} // end fnCalcDSTFwdBackInfo


/***************************************************************************/
/*                                                                         */
/* FUNCTION       : fnDSTAutoSwitch                                        */
/*                                                                         */
/* DESCRIPTION    : Checks if automatic Daylight Saving Time is enabled    */
/*                  and changes the state of the DST enable state (or      */
/*                  schedules the change) if the current date/time is      */
/*                  greater than or equal to a Switch 1 (first of the      */
/*                  current year) or Switch 2 (last of the current year)   */
/*                  date/time.                                             */
/*                                                                         */
/* AUTHOR         : Lynn W. D'Amico/Joe Mozdzer                            */
/*                                                                         */
/* DATE           : 21-March-2004                                          */
/*                                                                         */
/* PRECONDITIONS  : Function given valid fowward/backward DST parameters.  */
/*                                                                         */
/* POSTCONDITIONS : Does nothing if neither switch date/time has passed,   */
/*                  else sets the DST enable to the proper state if the    */
/*                  current date/time >= a switch date/time or schedules   */
/*                  an event to switch if the current date >= a switch     */
/*                  date but the current time is < the switch time.        */
/*                                                                         */
/* NOTES          : This function compiles properly and has been tested.   */
/*                                                                         */
/***************************************************************************/

unsigned int fnDSTAutoSwitch(DSTParams *pDSTFwd, DSTParams *pDSTBack)
{   
    DATETIME rDateTime;
    DATETIME rSwitchTime;
    BOOL fDateStatus;
    DSTState autoDSTSwitch, DSTEnabledState;
    DSTSwitch DSTSwitchParams;                  // DST Switch 1 and 2 parameters
    DSTSwitch *pDSTSwitchParams;                // Pointer to DST switch parameters
    EVENT_SCHEDULER_INFO *theInfo;              // Pointer to Event Scheduler structure
    long    lTimeDelta1, lTimeDelta2;
    unsigned char       bFeatureStatus = 0;
    unsigned char       bFeatureIndex = 0;
    BOOL                fEnabled = FALSE;


    pDSTSwitchParams = &DSTSwitchParams;
    DSTEnabledState = (DSTState)GetRTClockDayLightSavEnable();
    autoDSTSwitch = (DSTState)fnFlashGetByteParm(BP_DST_OFFSET_AUTO_SWITCH_ENABLE);

    (void)IsFeatureEnabled(&fEnabled, AUTO_DAYLIGHT_SWITCH_SUPPORT, &bFeatureStatus, &bFeatureIndex);

    // First check if DST automatic time switching is enabled (via EMD parameter and feature code)
    if ((autoDSTSwitch == ENABLED) && (fEnabled == TRUE))
    {
        // Calculate the Switch 1 and Switch 2 dates/times based on the DST forward/backward EMD parameter settings
        fnCalcDSTFwdBackInfo(pDSTFwd, pDSTBack, pDSTSwitchParams);

        // Need to determine which portion of the year we are in.  Start by getting the current reference time
        if (DSTSwitchParams.uDSTTimeType == LOCAL)
        {
            // If using local time, this is tricky because the current DST setting is factored in.  Since that is
            //  the setting we are trying to make correct, we need to normalize all times for comparison to not
            //  have DST factored into them.
            // Start with the actual time
            if (!(fDateStatus = GetSysDateTime(&rDateTime, ADDOFFSETS, GREGORIAN)))
            {
                taskException("GetSysDateTime() failure", THISFILE, __LINE__);
            }
            // If DST currently turned on, adjust the time to get rid of it
            if (DSTEnabledState == ENABLED)
                fnAdjustDateBySecs (&rDateTime, -(CMOSSetupParams.ClockOffset.DaylightSToff * 60));

        }
        else
        {
            // UTC: Get unadjusted clock date/time (raw system time from RTC with no offset adjustments)
            //  no special adjustment needs to be made for this one
            if (!(fDateStatus = GetSysDateTime(&rDateTime, NOOFFSETS, GREGORIAN)))
            {
                taskException("GetSysDateTime() failure", THISFILE, __LINE__);
            }
        }

        /* calculate the delta bewteen the current time and the switch 1 time */
        rSwitchTime.bYear = rDateTime.bYear;
        rSwitchTime.bCentury = rDateTime.bCentury;
        rSwitchTime.bMonth = DSTSwitchParams.uDSTSwitch_1_Month;
        rSwitchTime.bDay = DSTSwitchParams.uDSTSwitch_1_Day;
        rSwitchTime.bHour = DSTSwitchParams.uDSTSwitch_1_Time & 0xFF;
        rSwitchTime.bMinutes = (DSTSwitchParams.uDSTSwitch_1_Time & 0xFF00) / 0x0100;
        rSwitchTime.bSeconds = rDateTime.bSeconds;

        // if using local time and switch 1 is disabling DST, then it means the switch 1 time is
        //  specified in daylight time.  we need to get rid of this for our calculations
        if ((DSTSwitchParams.uDSTTimeType == LOCAL) && (DSTSwitchParams.eDSTSwitch_1_Type == DISABLED))
        {
            fnAdjustDateBySecs (&rSwitchTime, -(CMOSSetupParams.ClockOffset.DaylightSToff * 60));
        }


        if (!CalcDateTimeDifference(&rDateTime, &rSwitchTime, &lTimeDelta1))
        {
            taskException("CalcDateTimeDifference() failure", THISFILE, __LINE__);
        }

        /* calculate the delta bewteen the current time and the switch 2 time */
        rSwitchTime.bYear = rDateTime.bYear;
        rSwitchTime.bCentury = rDateTime.bCentury;
        rSwitchTime.bMonth = DSTSwitchParams.uDSTSwitch_2_Month;
        rSwitchTime.bDay = DSTSwitchParams.uDSTSwitch_2_Day;
        rSwitchTime.bHour = DSTSwitchParams.uDSTSwitch_2_Time & 0xFF;
        rSwitchTime.bMinutes = (DSTSwitchParams.uDSTSwitch_2_Time & 0xFF00) / 0x0100;
        rSwitchTime.bSeconds = rDateTime.bSeconds;

        // if using local time and switch 2 is disabling DST, then it means the switch 2 time is
        //  specified in daylight time.  we need to get rid of this for our calculations
        if ((DSTSwitchParams.uDSTTimeType == LOCAL) && (DSTSwitchParams.eDSTSwitch_2_Type == DISABLED))
        {
            fnAdjustDateBySecs (&rSwitchTime, -(CMOSSetupParams.ClockOffset.DaylightSToff * 60));
        }

        if (!CalcDateTimeDifference(&rDateTime, &rSwitchTime, &lTimeDelta2))
        {
            taskException("CalcDateTimeDifference() failure", THISFILE, __LINE__);
        }

        // Check if the current date is greater than or equal to the Switch 2 date or less than the switch 1 date
        if ((lTimeDelta2 <= 0) || (lTimeDelta1 > 0))
        {
            (void)delAllScheduledEventsMatchingDescription("DST Auto Switch Event");
            // Check if the current DST enabled state is different than the Switch 2 DST enabled state
            //  If that is the case, it should be changed to match the switch 2 state
            if (DSTSwitchParams.eDSTSwitch_2_Type != DSTEnabledState)
            {
                SetRTClockDayLightSavEnable((BOOL)DSTSwitchParams.eDSTSwitch_2_Type);
                fCMOSDSTAutoUpdated = TRUE;
            }

            /* if we are within 24 hours of the switch 1 time occurring, schedule the switch 1 event */             
            if ((lTimeDelta1 > 0) && (lTimeDelta1 < SECS_IN_A_DAY))
            {
                // first delete any stale events that may have been previously scheduled
                // Current time < Switch 1 time; Schedule a DST enable state switch event
                theInfo = newEventSchedulerInfo();
                if(!theInfo)
                {
                    taskException("newEventSchedulerInfo() failure", THISFILE, __LINE__);
                }
				
                // Setup the date/time to switch DST state
                // year/century and seconds are already set to current
                rDateTime.bMonth = (unsigned char)DSTSwitchParams.uDSTSwitch_1_Month;   
                rDateTime.bDay = (unsigned char)DSTSwitchParams.uDSTSwitch_1_Day;
                rDateTime.bHour = DSTSwitchParams.uDSTSwitch_1_Time & 0xFF;
                rDateTime.bMinutes = (DSTSwitchParams.uDSTSwitch_1_Time & 0xFF00) / 0x0100;

                // Schedule the event at 2 seconds past the switch time to avoid any potential race 
                // conditions between the current time and the scheduling of the event.  2 seconds
                // is necessary because we want to guarantee scheduling for at least 1 second after
                // the current time and it is possible a second will tick off before scheduling is complete.
                fnAdjustDateBySecs (&rDateTime, 2);

                // if switch time is specified in GMT, we must convert this to a local time since
                //  that is what the scheduler uses
                if (DSTSwitchParams.uDSTTimeType != LOCAL)
                    ConvertFromGMTToLocal(&rDateTime, FALSE);
					
                theInfo->eventTime = rDateTime;
                memcpy(theInfo->eventDescription, "DST Auto Switch Event", strlen("DST Auto Switch Event"));
                memcpy(theInfo->registeredCallBackFunction, "DSTAutoChange", strlen("DSTAutoChange"));
				
                // Set Data to the value of DST switch 1 type (enable / disable)
                theInfo->eventData = (unsigned long)DSTSwitchParams.eDSTSwitch_1_Type; 
                theInfo->eventDataPtr = NULL;
                scheduleEvent(theInfo);             
            }
        }
        else
        {
            // Check if the current DST enabled state is different than the Switch 1 DST enabled state
            (void)delAllScheduledEventsMatchingDescription("DST Auto Switch Event");
			
            //  Make them the same if there are differences
            if (DSTSwitchParams.eDSTSwitch_1_Type != DSTEnabledState)
            {
                SetRTClockDayLightSavEnable((BOOL)DSTSwitchParams.eDSTSwitch_1_Type);
                fCMOSDSTAutoUpdated = TRUE;
            }

            // if within 1 day (86400 seconds) of the switch 2 time, schedule it
            if ((lTimeDelta2 > 0) && (lTimeDelta2 < SECS_IN_A_DAY))
            {
                // first delete any stale events that may have been previously scheduled
                theInfo = newEventSchedulerInfo();
                if(!theInfo)
                {
                    taskException("newEventSchedulerInfo() failure", THISFILE, __LINE__);
                }
				
                // Setup the date/time to switch DST state
                // year/century and seconds are already set to current
                rDateTime.bMonth = (unsigned char)DSTSwitchParams.uDSTSwitch_2_Month;   
                rDateTime.bDay = (unsigned char)DSTSwitchParams.uDSTSwitch_2_Day;
                rDateTime.bHour = DSTSwitchParams.uDSTSwitch_2_Time & 0xFF;
                rDateTime.bMinutes = (DSTSwitchParams.uDSTSwitch_2_Time & 0xFF00) / 0x0100;

                // Schedule the event at 2 seconds past the switch time to avoid any potential race 
                // conditions between the current time and the scheduling of the event.  2 seconds
                // is necessary because we want to guarantee scheduling for at least 1 second after
                // the current time and it is possible a second will tick off before scheduling is complete.
                fnAdjustDateBySecs (&rDateTime, 2);

                // if switch time is specified in GMT, we must convert this to a local time since
                //  that is what the scheduler uses
                if (DSTSwitchParams.uDSTTimeType != LOCAL)
                    ConvertFromGMTToLocal(&rDateTime, FALSE);
					
                theInfo->eventTime = rDateTime;
                memcpy(theInfo->eventDescription, "DST Auto Switch Event", strlen("DST Auto Switch Event"));
                memcpy(theInfo->registeredCallBackFunction, "DSTAutoChange", strlen("DSTAutoChange"));
				
                // Set Data to the value of DST switch 1 type (enable / disable)
                theInfo->eventData = (unsigned long)DSTSwitchParams.eDSTSwitch_2_Type; 
                theInfo->eventDataPtr = NULL;
                scheduleEvent(theInfo);             
            }
        }
    }

    return (0);
} 


/****************************************************************************************/
/* Business Rules Extraction staff*******************************************************/
/****************************************************************************************/

/* *************************************************************************
// FUNCTION NAME: 
//        fnGetDateParms
//
// DESCRIPTION:
//        Get the date and date parameters to format the date 
//
// INPUTS:
//        pDate - pointer to DATETIME structure containing the date to be returned,
//        pDateFormat - pointer to date format to be returned.
//        pDDS - pointer to date duck status to be returned,
//        pDateSepChar - pointer to date sep  character  to be returned.
//
// RETURNS:
//        TRUE - if there is no error
//        FALSE - if there is any error
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//        02/28/2006    John Gao  Business rules extraction from fnfPrintedDate      
// *************************************************************************/
BOOL fnGetDateParms(DATETIME    *pDate, 
                    UINT8       *pDateFormat, 
                    UINT16      *pDDS, 
                    UINT16      *pDateSepChar )
{
    //Get the displayed date
    if (GetPrintedDate(pDate, GREGORIAN) != SUCCESSFUL)
    {
        return (FALSE);
    }
    
    //Get the proper print flags
	switch (fnOITGetPrintMode())
	{
		case PMODE_PERMIT:
			if (fnValidData(BOBAMAT0, GET_PERMIT_PRINT_FLAGS, pDDS) != BOB_OK)
            {
                fnProcessSCMError();
                return (FALSE); 
			}
			break;

		case PMODE_TIME_STAMP:
		case PMODE_AD_TIME_STAMP:
			if (fnValidData(BOBAMAT0, GET_DATETIME_PRINT_FLAGS, pDDS) != BOB_OK)
			{
				fnProcessSCMError();
		        return (FALSE); 
			}
			break;

		default:
			if (fnValidData(BOBAMAT0, GET_INDICIA_PRINT_FLAGS, pDDS) != BOB_OK)
			{
				fnProcessSCMError();
		        return (FALSE); 
			}
			break;
    }
    
    //Get the date format
    *pDateFormat = fnFlashGetByteParm(BP_REPORT_DATE_FORMAT);
    
    //Get the date sep char
    *pDateSepChar = fnFlashGetWordParm(WP_DISP_DATE_SEP_CHAR);

    return (TRUE);
}



/* *************************************************************************
// FUNCTION NAME: 
//        fnGetSysDateParms
//
// DESCRIPTION:
//        Get the system date and date parameters to format the date 
//
// INPUTS:
//        pDate - pointer to DATETIME structure containing the date to be returned,
//        pDateFormat - pointer to date format to be returned.
//        pDDS - pointer to date duck status to be returned,
//        pDateSepChar - pointer to date sep  character  to be returned.
//
// RETURNS:
//        TRUE - if there is no error
//        FALSE - if there is any error
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//        09/05/2006    Kan Jiang    Initial version.     
// *************************************************************************/
BOOL fnGetSysDateParms(DATETIME    *pDate, 
                       UINT8       *pDateFormat, 
                       UINT16      *pDDS, 
                       UINT16      *pDateSepChar )
{   
    //Get the displayed date
    if (GetSystemDate(pDate, GREGORIAN) != SUCCESSFUL)
    {
        return (FALSE);
    }
    
    //Get the indicia print flag
    if (fnValidData((UINT16)BOBAMAT0, 
                    (UINT16)GET_INDICIA_PRINT_FLAGS, 
                    pDDS) != (UINT8)SUCCESSFUL)
    {
        fnProcessSCMError();
        return (FALSE); 
    }
    
    //Get the date format
    *pDateFormat = fnFlashGetByteParm(BP_REPORT_DATE_FORMAT);
    
    //Get the date sep char
    *pDateSepChar = fnFlashGetWordParm(WP_DISP_DATE_SEP_CHAR);

    return (TRUE);
}

/* *************************************************************************
// FUNCTION NAME: 
//        fnGetMidnightDateParms
//
// DESCRIPTION:
//        Get the midnight cross date and parameters to format the date 
//
// INPUTS:
//        pDate - pointer to DATETIME structure containing the date to be returned,
//        pDateFormat - pointer to date format to be returned.
//        pDDS - pointer to date duck status to be returned,
//        pDateSepChar - pointer to date sep  character  to be returned.
//
// RETURNS:
//        TRUE - if there is no error
//        FALSE - if there is any error
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//  09/08/2006  Vincent Yi      Initial version.     
//
// *************************************************************************/
BOOL fnGetMidnightDateParms(DATETIME    *pDate, 
                            UINT8       *pDateFormat, 
                            UINT16      *pDDS, 
                            UINT16      *pDateSepChar )
{   
    memcpy (pDate, &stMidnightDate, sizeof(DATETIME));
    
    //Get the indicia print flag
    if (fnValidData((UINT16)BOBAMAT0, 
                    (UINT16)GET_INDICIA_PRINT_FLAGS, 
                    pDDS) != (UINT8)SUCCESSFUL)
    {
        fnProcessSCMError();
        return (FALSE); 
    }
    
    //Get the date format
    *pDateFormat = fnFlashGetByteParm(BP_REPORT_DATE_FORMAT);
    
    //Get the date sep char
    *pDateSepChar = fnFlashGetWordParm(WP_DISP_DATE_SEP_CHAR);

    return (TRUE);
}


/* *************************************************************************
// FUNCTION NAME: 
//        fnClearDateAdvToZero 
//
// DESCRIPTION:
//        function that clears the date advance value to zero.
//
// INPUTS:
//        None
//
// RETURNS:
//        None
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//        03/04/2016    Renhao    Updated for G922 Rates Expiration  new feature.
//        02/29/2006    John Gao  Business rules extraction from fnhkDateAdvClearToZero       
// *************************************************************************/
BOOL fnClearDateAdvToZero (void)
{
    UINT32           currTimeInSecs;
    DATETIME        theCurrDateTime;
    BOOL fRetVal = FALSE;
	UINT16  wAutoAdvEMDParam = fnFlashGetWordParm(WP_AUTO_DATE_ADVANCE_OPTIONS);
	
	
    if ( fnIsDateAdvanceChanged ( ) == TRUE )
    {
		/*
		 If the EMD parameter indicates that the Auto-Date Advance is mandatory, the date can be reset to the 
		 system date if it is before the Auto-Date Advance Time; otherwise, the date must be set to the system date + 1.
		*/
		if(wAutoAdvEMDParam & AUTO_ADV_DATE_IS_MANDATORY)
		{
	        (void)GetSysDateTime (&theCurrDateTime, ADDOFFSETS, GREGORIAN);
		    currTimeInSecs = (long)fnTodToSecs ( &theCurrDateTime );
			
			bDateAdvancePrompt = DATE_ADVANCE_RESET_TO_TODAY ;
		    fnSaveOldDateAdvanceAmount();
			
		    if (currTimeInSecs < (long)CMOSSetupParams.AutoAdvTime) 
		    {
				
		        (void)SetPrintedDateAdvanceDays(0);	/* no need to check return val since this can't fail */

		    }
		    else
		    {
                (void)SetPrintedDateAdvanceDays(1);
		    }
		 }             
    	else
    	{
             bDateAdvancePrompt = DATE_ADVANCE_RESET_TO_TODAY ;
             fnSaveOldDateAdvanceAmount();               
             SetPrintedDateAdvanceDays(0);   /* no need to check return val since this can't fail */
    	}
		
		 fRetVal = TRUE;
    }
	
    return (fRetVal);
}


/* *************************************************************************
// FUNCTION NAME: 
//        fnIsTimeFormat12
//
// DESCRIPTION:
//        Check if the date format is TIME_FORMAT_12
//
// INPUTS:
//
// RETURNS:
//        TRUE - if the date format is TIME_FORMAT_12
//        FALSE - if it is not
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//        02/28/2006    John Gao  Business rules extraction from fnfPrintedDate      
// *************************************************************************/
BOOL fnIsTimeFormat12(void)
{
    BOOL fRetVal = FALSE;


    if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
    {
        fRetVal = TRUE;
    }
	
    return (fRetVal);
}


/* *************************************************************************
// FUNCTION NAME: 
//        fnCheckTimeAMPM
//
// DESCRIPTION:
//        Check if the time is AM or PM
//
// INPUTS:
//        bAmPm - the pointer of output value
//
// OUTPUTS:
//        bAmPm - TIME_AM or TIME_PM
//
// RETURNS:
//        TRUE - if there is no error,
//        FALSE - else
//
// WARNINGS/NOTES:
//        Check if the time format is TIME_FORMAT_12 before call this function 
//
// MODIFICATION  HISTORY:
//        02/28/2006    John Gao  Business rules extraction     
// *************************************************************************/
BOOL fnCheckTimeAMPM (UINT8 *bAmPm)
{
    DATETIME        stDateTime;


    if(GetSysDateTime(&stDateTime, ADDOFFSETS, GREGORIAN)== FALSE)
    {
        //Something wrong when retrive the datetime
        fnReportMeterError(ERROR_CLASS_OIT, ECOIT_DATE_RETRIEVE_FAILED);
        return (FALSE);
    }
	
    if (stDateTime.bHour <= 11)
    {
        //AM
        *bAmPm = (UINT8)TIME_AM;
    }
    else 
    {
        //PM
        *bAmPm = (UINT8)TIME_PM;
    }
	
    return (TRUE);
}


/* *************************************************************************
// FUNCTION NAME: 
//        fnCheckRTCTimeAMPM
//
// DESCRIPTION:
//        Check if the time is AM or PM
//
// INPUTS:
//        bAmPm - the pointer of output value
//
// OUTPUTS:
//        bAmPm - TIME_AM or TIME_PM
//
// RETURNS:
//        TRUE - if there is no error,
//        FALSE - else
//
// WARNINGS/NOTES:
//        Check if the time format is TIME_FORMAT_12 before call this function 
//
// MODIFICATION  HISTORY:
//        06/28/2006    Raymond Shen    Business rules extraction      
// *************************************************************************/
BOOL fnCheckRTCTimeAMPM (UINT8 *bAmPm)
{
    DATETIME        stDateTime;


    if(ReadRTClock(&stDateTime) == FALSE)
    {
        //Something wrong when retrive the datetime
        fnReportMeterError(ERROR_CLASS_OIT, ECOIT_DATE_RETRIEVE_FAILED);
        return (FALSE);
    }
	
    if (stDateTime.bHour <= 11)
    {
        //AM
        *bAmPm = (UINT8)TIME_AM;
    }
    else 
    {
        //PM
        *bAmPm = (UINT8)TIME_PM;
    }
	
    return (TRUE);
}


/* *************************************************************************
// FUNCTION NAME: 
//        fnSetUserTime
//
// DESCRIPTION:
//        function that sets the user time
//
// INPUTS:
//        ulHours - the hours to be set
//        ulMinutes - the minutes to be set
//
// RETURNS:
//        TRUE - if the time is set successfully.
//        FALSE - if failed.
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//        02/28/2006    John Gao  Business rules extraction from fnhkValidateUserTimeOption      
// *************************************************************************/
BOOL fnSetUserTime (UINT32 ulHours, 
                    UINT32 ulMinutes)
{
    DATETIME        stDateTime;


    /* validate the hours and minutes */
    if ((ulHours > 23) || (ulMinutes > 59))
    {
        return(FALSE);
    }
	
    /* get the current time/date, adjusting for time zone, 
      aylight savings and drift */
    if(GetSysDateTime(&stDateTime, ADDOFFSETS, GREGORIAN)== FALSE)
    {
        fnReportMeterError(ERROR_CLASS_OIT, ECOIT_DATE_RETRIEVE_FAILED);
        return(FALSE);
    }
         
    /* copy the time entered into a structure with today's date */
    stDateTime.bHour = (unsigned char) ulHours;
    stDateTime.bMinutes = (unsigned char) ulMinutes;

    /* is the drift value in range */
    if (fnUserAdjustRTC(&stDateTime, ADJUST_TIMEZONE) != IN_RANGE)
    {
        return(FALSE);
    }

    return(TRUE);
}


/* *************************************************************************
// FUNCTION NAME: 
//        fnValidateNormalPresetTimer
//
// DESCRIPTION:
//        function that validates the normal preset timer.
//
// INPUTS:
//        ulPresetTimer - The timer to be set
//
// RETURNS:
//        TRUE - if the timer is in range
//        FALSE- if not
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//        02/29/2006    John Gao  Business rules extraction from fnhkValidateNormalPresetTimer      
// *************************************************************************/
BOOL fnValidateNormalPresetTimer (UINT32 ulPresetTimer)
{
    BOOL fRetVal = FALSE;


    if ((ulPresetTimer >= PRESET_TIMER_MIN) && 
        (ulPresetTimer <=PRESET_TIMER_MAX))
    {
        /*save the preset timer into CMOS */
        fNormalPresetEnable = TRUE;
        CMOSSetupParams.wpUserSetPresetClearTimeout = (unsigned short) ulPresetTimer;
		
        /* adjust minutes to milliseconds */
        fnSetNormalPresetTimeout(CMOSSetupParams.wpUserSetPresetClearTimeout * 60 * 1000);  
        fRetVal = TRUE;
    }
	
    return(fRetVal);
}


/* *************************************************************************
// FUNCTION NAME: 
//        fnValidateSleepTimer 
//
// DESCRIPTION:
//        function that validates the sleep timer.
//
// INPUTS:
//        ulSleepTimer - The timer to be set
//
// RETURNS:
//        TRUE - if the timer is in range
//        FALSE- if not
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//        02/29/2006    John Gao  Business rules extraction from fnhkValidateSleepTimeout       
// *************************************************************************/
BOOL fnValidateSleepTimer  (UINT32 ulSleepTimer)
{
    BOOL fRetVal = FALSE;


    if ((ulSleepTimer >= SLEEP_TIMER_MIN) && (ulSleepTimer <= SLEEP_TIMER_MAX))
    {
        CMOSSetupParams.wSleepTimeout = (unsigned short) ulSleepTimer;
        fnSetSleepTimeout(CMOSSetupParams.wSleepTimeout * 60 * 1000);  /* adjust minutes to milliseconds */
        fRetVal = TRUE;
    }
	
    return(fRetVal);
}


/* *************************************************************************
// FUNCTION NAME: 
//        fnValidateMailInactivityTimer 
//
// DESCRIPTION:
//        function that validates the mail inactivity timer.
//
// INPUTS:
//        ulInputTimeout - The timeout to be set
//
// RETURNS:
//        TRUE - if the timer is in range
//        FALSE- if not
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//  08/29/2006  Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnValidateMailInactivityTimer  (UINT32 ulInputTimeout)
{
    BOOL fRetVal = FALSE;


    if ((ulInputTimeout >= MAIL_INACTIVITY_TIMER_MIN) && 
        (ulInputTimeout <= MAIL_INACTIVITY_TIMER_MAX))
    {
        CMOSSetupParams.ulMailInactivityTimeout = ulInputTimeout;
        fRetVal = TRUE;
    }
	
    return(fRetVal);
}


/* *************************************************************************
// FUNCTION NAME: 
//        fnDisableDateAdv 
//
// DESCRIPTION:
//        function that disable auto date advance.
//
// INPUTS:
//        None
//
// RETURNS:
//        TRUE - if auto date advance disabled successfully
//        FALSE- if not
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//  05/19/2006    James Wang    Initial version.       
// *************************************************************************/
BOOL fnDisableDateAdv(void)
{
    BOOL    fRetVal = FALSE;
    UINT32  ulOldCMOSAutoTime = fnCMOSSetupGetCMOSSetupParams()->AutoAdvTime;


    fnCMOSSetupGetCMOSSetupParams()->AutoAdvTime = 0;
    if ( fnIsDateAdvanceChanged ( ) )
    {
        SINT32      lCurrTimeInSecs;
        DATETIME    theCurrDateTime;


        (void)GetSysDateTime (&theCurrDateTime, ADDOFFSETS, GREGORIAN);
        lCurrTimeInSecs = fnTodToSecs ( &theCurrDateTime );

        if ( GetPrintedDateAdvanceDays() == 1 
            && lCurrTimeInSecs >= ulOldCMOSAutoTime )
        {
            SetPrintedDateAdvanceDays(0); /* no need to check return val since this can't fail */
            fRetVal = TRUE;
        }
    }

    return  fRetVal; 
}

/* *************************************************************************
// FUNCTION NAME: 
//        fnEnableNormalPresetTimeout 
//
// DESCRIPTION:
//        function that enable Normal Preset timeout if there is a non-zero 
//        number in the normal preset timer.
//
// INPUTS:
//        None
//
// RETURNS:
//        None
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//  05/19/2006    James Wang    Initial version.       
// *************************************************************************/
void fnEnableNormalPresetTimeout(void)
{
    SetupParams *   pCMOSSetupParams = fnCMOSSetupGetCMOSSetupParams();
    
	
    if ((pCMOSSetupParams->wpUserSetPresetClearTimeout) && 
        (fnSYSIsBaseAttached() == TRUE))
    {
        /* adjust minutes to milliseconds */
        fnSetNormalPresetTimeout(pCMOSSetupParams->wpUserSetPresetClearTimeout * 
                                                                    60 * 1000); 
        fnEnableNormalPreset();
    }
}

//Moved the following util functions from oiclock.c

/* **********************************************************************
// FUNCTION NAME: fnNormalPresetTimerExpire
// DESCRIPTION:  Timer expiration routine that occurs when system has been
//                  inactive for the fixed duration.
//
// AUTHOR: Sam Thillaikumaran
//
// INPUTS:  none used
// **********************************************************************/
void fnNormalPresetTimerExpire(unsigned long lwArg)
{
    /* put a message in the OIT queue indicating that the timer expired */
//    (void)OSSendIntertask (OIT, OIT, OIT_NORMAL_PRESET_TIMER_EXPIRED, NO_DATA, NULL, 0);
    
    /* stop this timer so it doesn't automatically retrigger */
    (void)OSStopTimer(NORMAL_PRESET_TIMER);
}


/* **********************************************************************
// FUNCTION NAME: fnEnableNormalPreset
// DESCRIPTION:  Starts the Normal Preset timer
//
// AUTHOR: Sam Thillaikumaran
//
// INPUTS:  none used
// **********************************************************************/
void fnEnableNormalPreset()
{
    /* start the sleep timer */
    (void)OSStartTimer(NORMAL_PRESET_TIMER);
    fNormalPresetEnable = TRUE;
}


/* **********************************************************************
// FUNCTION NAME: fnDisableNormalPreset
// DESCRIPTION:  Stops the Normal Preset timer
//
// AUTHOR: Sam Thillaikumaran
//
// INPUTS:  none used
// **********************************************************************/
void fnDisableNormalPreset()
{
    /* stop the sleep timer */
    (void)OSStopTimer(NORMAL_PRESET_TIMER);
    fNormalPresetEnable = FALSE;
}


/* **********************************************************************
// FUNCTION NAME: fnResetNormalPresetTimer
// DESCRIPTION:  Resets the Normal Preset timer to its full duration
//
// AUTHOR: Sam Thillaikumaran
//
// INPUTS:  none used
// **********************************************************************/
void fnResetNormalPresetTimer()
{
    /* if the Normal Preset timer is enabled, turn the timer off then on again to
        reset it.  if the timer is not enabled, we don't want to do this
        because it would inadvertently reenable the timer */
    if (fNormalPresetEnable == TRUE)
    {
        (void)OSStopTimer(NORMAL_PRESET_TIMER);
        (void)OSStartTimer(NORMAL_PRESET_TIMER);
    }
}


//******************************************************************************
// Function Name: fnSetNormalPresetTimeout                                     *
// Description:  Sets the Normal Preset timer duration                         *
//                                                                             *
// AUTHOR: Sam Thillaikumaran                                                  *
//                                                                             *
// INPUTS:  none used                                                          *
//                                                                             *
//******************************************************************************
void fnSetNormalPresetTimeout(unsigned long lwMilliseconds)
{
    // change the timer duration
    (void)OSChangeTimerDuration(NORMAL_PRESET_TIMER, lwMilliseconds);

    // if we were enabled, reset.  if we weren't enabled, make sure it didn't
    // start as a side effect of the os call.
    if (fNormalPresetEnable == TRUE)
    {
        fnResetNormalPresetTimer();
    }
	
    // Otherwise, put sleeper to pernament sleep
    else
    {
        (void)OSStopTimer(NORMAL_PRESET_TIMER);
    }
}


/* *************************************************************************
// FUNCTION NAME:  
//      fnIsCloseToMidnightCrossover
//
// DESCRIPTION:
//      Check whether it is close to midnight
//
// INPUTS:
//      None
//
// RETURNS:
//      TRUE or FALSE
//
// WARNINGS/NOTES: 
//      None
//
// MODIFICATION HISTORY:               
//  09/08/2006  Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnIsCloseToMidnightCrossover(void)
{
    DATETIME    stMidnightTime;
    DATETIME    stCurTime;
    SINT32      lTimeDiff;
    BOOL        fRet = FALSE;


    (void)GetSysDateTime (&stCurTime, ADDOFFSETS, GREGORIAN);
    memcpy (&stMidnightTime, &stCurTime, sizeof(DATETIME));

    AdjustDayByOne(&stMidnightTime, TRUE);

    stMidnightTime.bHour = 0;
    stMidnightTime.bMinutes = 0;
    stMidnightTime.bSeconds = 0;

    (void)CalcDateTimeDifference(&stCurTime, &stMidnightTime, &lTimeDiff);
    
    if (lTimeDiff<=TIME_AHEAD_MIDNIGHT && lTimeDiff>=0)
    {
        // Save the date of this midnight
        memcpy (&stMidnightDate, &stMidnightTime, sizeof(DATETIME));
        fRet = TRUE;    
    } 

    return fRet;    
}


/* *************************************************************************
// FUNCTION NAME: 
//        fnValidateZWZPTimer 
//
// DESCRIPTION:
//        function that validates the ZWZP timer value.
//
// INPUTS:
//        ulInputTimeout - The timeout to be set
//
// RETURNS:
//        TRUE - if the timer is in range
//        FALSE- if not
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//  26 Mar 2007 BIll Herring    Initial
//
// *************************************************************************/
BOOL fnValidateZWZPTimer  (UINT32 ulInputTimeout)
{
    BOOL fRetVal = FALSE;


    if ((ulInputTimeout >= ZWZP_TIMER_MIN) && 
        (ulInputTimeout <= ZWZP_TIMER_MAX))
    {
        CMOSZwzpParams.timeout = ulInputTimeout;
        fRetVal = TRUE;
    }
	
    return(fRetVal);
}


/******************************************************************/
/*                                                                */
/* FUNCTION NAME        : GetPrintedOrCorrectedDate                          */
/*                                                                */
/* FUNCTION DESCRIPTION : this function will return printed date  */
/*                                                                */
/* INPUTS               : None.                                   */
/*                                                                */
/* OUTPUTS              : The current printed date, regardless of */
/*                        date duck mode.                         */
/*                                                                */
/* RETURNS              : TRUE / FALSE.                           */
/*                                                                */
/******************************************************************/
BOOL GetPrintedOrCorrectedDate(DATETIME *PrintedDate, BOOL Gregorian) {

    if(bIsCorrecteDate)
        return(CalcPrintedDate(PrintedDate, Gregorian, DateCorrectionAmount));
    else
        return(CalcPrintedDate(PrintedDate, Gregorian, DateAdvanceAmount));
}


/******************************************************************/
/*                                                                */
/* FUNCTION NAME        : fnClearDateCorrection                          */
/*                                                                */
/* FUNCTION DESCRIPTION : this function will clear the amount of date correction  */
/*                                                                */
/* INPUTS               : None.                                   */
/*                                                                */
/* OUTPUTS              : None									*/
/*                                                                */
/* RETURNS              :                              			*/
/*                                                                */
/******************************************************************/
void fnClearDateCorrection()
{
    DateCorrectionAmount = 0;
}

/* *************************************************************************
// FUNCTION NAME: 
//        fnResyncTimers
//
// DESCRIPTION:
//        Resyncs the alarm timers. 
//
// INPUTS:
//        None
//
// RETURNS:
//        Nothing
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//        01-May-15    Sandra Peterson	Initial version      
// *************************************************************************/
void fnResyncTimers(void)
{
	// Update the alarm timers since time has been resynced
	// Reset midnight time, which also resets Dcap midnight time.
    SetClockMidnight();

	// Don't need to reschedule the DST auto-change event because that will
	// be handled by the midnight processing.
	// Don't need to reschedule the Dcap midnight event because that is done as
	// a result of the midnight event going off.
	// Don't need to reschedule the Dcap Next Upload Date/Time event because that will
	// be scheduled either as a result of the Dcap midnight processing or as a result
	// of getting a new Next Upload Date/Time list during the rest of the connection w/ Tier3.

	if (CMOSSetupParams.AutoAdvTime != 0 ) 
		(void)fnSetAutoAdvanceTimeEvent();	// reset the auto-date advance time event

    /* Check if Inview accounting is in use. If so, reschedule the major period alarm & the Inview midnight alarm
       so that the local midnight event trigger time and Inview event trigger times
       are calculated based on the same adjustment. */
/*
    if (fnAGDCheckAbacusFeaturePackageEnabled() && (fnGetAccountingType() == ACT_SYS_TYPE_ABACUS ) && !fnIsInOOBMode())
	{
        (void)fnAGDScheduleMajorPeriodAlarm();
						
		// also reschedule the Inview midnight alarm.
		(void)fnAGDScheduleMidnightAlarm();
	}
*/
}
