#include "kernel/nu_kernel.h"
#include "am335x_pbdefs.h"
#include "HAL_AM335x_GPIO.h"
#include "gtypes.h"
#include "pbos.h"
#include "commontypes.h"
#include "stdio.h"
#include "stddef.h"
#include "hal.h"
#include "clock.h"
#include "bsp/csd_2_3_defs.h"
#include "debugTask.h"
#include "am335x_timer.h"
#include "feeder_pwm.h"
#include "feeder_timer.h"
#include "feeder_qep.h"

static NU_HISR				feederQEPHISR_CB;
static VOID             	(*old_feederLISR)(INT);
static UINT					localQEPLISRCount = 0 ;
static UINT32				localQEPLastPosCount = 0;
static structQEP1Control	localQEP1Control ;
//         start drive for rate of                  0   10   20   30   40   50   60  70   80   90
static const UINT8			localPreStartDrive[] = {0, 161, 286, 360, 420, 510, 610, 708, 770, 870};
static const UINT16         localSizeOfPreStart = sizeof(localPreStartDrive)/ sizeof(UINT8);
///////////////////////////////////////////////////////////////////////////////////////////////////////////
void initializeQEP()
{
	STATUS 			status = NU_SUCCESS;
    VOID			*hisrStack ;
    NU_MEMORY_POOL 	*mem_pool;

    // Enable Sitara clocks...
	static PEQEP_STRUCT localQEP1 = (PEQEP_STRUCT)EQEP1_ADDRESS;
	ESAL_GE_MEM_WRITE32(EQEP_CLKCTRL, PWM_WKP_ENABLE);
	while((ESAL_GE_MEM_READ32(EQEP_CLKCTRL) >> 16) != 0) ;

	// prepare the QEP1 input pins
	clearGPIOPinAsOutput(1, 20) ;
	clearGPIOPinAsOutput(1, 21) ;
	// Set QEP1A input GPIO1,20 == eQEP1A_in (mux 6)
	UINT32 *qepIn = addressGPIOPin(1, 20) ;
	// set mux6  enable pull up  - input
	*qepIn = 6 + (1 << 4) + (1 << 5);

	// Set QEP1A input GPIO1,21 == eQEP1B_in (mux 6)
	qepIn = addressGPIOPin(1, 21) ;
	// set mux6  enable pull up  - input
	*qepIn = 6 + (1 << 4) + (1 << 5);


	//				      Direction Count
	localQEP1->QDECCTL = 0; //(UINT16)(1 << 14);

	// QEP Control       enb module     enb unit timer    latch Pos on unit timeout      clr Pos on unit time event
	localQEP1->QEPCTL = (1 << 3)    +   (1 << 1)    +    (1 << 2)                           +     (3 << 12);

	// preset the max and cpomparator values
	localQEP1->QPOSMAX = 0xFFFFFFFF;
	localQEP1->QPOSCMP = 0xFFFFFFFF;

	// Unit timer start it at 0 and set the compare to max
	localQEP1->QUTMR	= 0;
	localQEP1->QUPRD	= 0xFFFFFFFF;

	// prep the IRQ handlers
    if(status == NU_SUCCESS)
    {
		/* Retrieve a pointer to the system memory resources. */
		status = NU_System_Memory_Get(&mem_pool, NULL);
    }
    if(status == NU_SUCCESS)
    {
    	status = NU_Allocate_Memory(mem_pool, &hisrStack, QEP1_HISR_STACKSIZE, NU_SUSPEND);
    }

    if(status == NU_SUCCESS)
    {
    	status = NU_Create_HISR(&feederQEPHISR_CB, "QEP1", feederQEP1HISR, 2, hisrStack, QEP1_HISR_STACKSIZE);
    }

    if(status == NU_SUCCESS)
    {
        status = NU_Register_LISR(ESAL_PR_INT_IRQ88_EQEP1INT, feederQEP1LISR,&old_feederLISR);

    }

    if(status == NU_SUCCESS)
    {
    	debugDisplayOnly("QEP Initialization completed successfully\r\n");
    	enableQEPUnitPeriodInt();
    }

}

//////////////////////////////////////////////////////////////////////////////////////
// enableQEPUnitPeriodInt
//
// enable the period interrupt both at QEP level and an nucleus IRQ level
VOID enableQEPUnitPeriodInt()
{
	static PEQEP_STRUCT localQEP1 = (PEQEP_STRUCT)EQEP1_ADDRESS;
    ESAL_GE_INT_Enable(ESAL_PR_INT_IRQ88_EQEP1INT,0,0x5);
	localQEP1->QEINT |= (1 << 11);

}

//////////////////////////////////////////////////////////////////////////////////////
//	disableQEPUintPeriodInt
//
// disable the period interrupt both at the QEp and the nucleus level.
VOID disableQEPUintPeriodInt()
{
	static PEQEP_STRUCT localQEP1 = (PEQEP_STRUCT)EQEP1_ADDRESS;
	localQEP1->QEINT &= ~(1 << 11);
    ESAL_GE_INT_Disable(ESAL_PR_INT_IRQ88_EQEP1INT);

}

//////////////////////////////////////////////////////////////////////////////////////
// setQEPUnitTimerFrequency
//
// provide the desired unit timer IRQ frequency in Hz
VOID setQEPUnitTimerFrequency(UINT32 uVal)
{
	// set period and reset timer
	// timer is clocked with 62.5MHz clock
	static PEQEP_STRUCT localQEP1 = (PEQEP_STRUCT)EQEP1_ADDRESS;
	localQEP1->QUPRD = QEP_SOURCE_CLK/uVal;
	localQEP1->QUTMR = 0 ;
}


//////////////////////////////////////////////////////////////////////////////////////
// readQEPUnitTimer
//
// read the instantaneous (non latched) value of the unit count-up timer
UINT32 readQEPUnitTimer()
{
	static PEQEP_STRUCT localQEP1 = (PEQEP_STRUCT)EQEP1_ADDRESS;
	return localQEP1->QUTMR;
}


//////////////////////////////////////////////////////////////////////////////////////
// readQEPStatus
//
// read the value of the QEP status register
UINT32 readQEPStatus()
{
	static PEQEP_STRUCT localQEP1 = (PEQEP_STRUCT)EQEP1_ADDRESS;
	return localQEP1->QEPSTS;
}

//////////////////////////////////////////////////////////////////////////////////////
// clearQEPCounter
//
// clear the value in the encoder counter register.
VOID clearQEPCounter()
{
	static PEQEP_STRUCT localQEP1 = (PEQEP_STRUCT)EQEP1_ADDRESS;
	localQEP1->QPOSCNT = 0;
}

//////////////////////////////////////////////////////////////////////////////////////
//	getQEPLISRCount
//
// get the counter holding the number of times the QEP unit timer LISR has been called
UINT getQEPLISRCount()
{
	return localQEPLISRCount;
}

//////////////////////////////////////////////////////////////////////////////////////
// getQEPLastPosCount
//
// when the unit timer expires it snapshots the value in the encoder counter register
// prior to clearing it. This register holds the last snapshot value.
UINT32 getQEPLastPosCount()
{
	return  localQEP1Control.lastRate;
;
}

//////////////////////////////////////////////////////////////////////////////////////
// getQEPUnlatchedPosCount
//
// read the unlatched value of the encoder counter register.
UINT32 getQEPUnlatchedPosCount()
{
	static PEQEP_STRUCT localQEP1 = (PEQEP_STRUCT)EQEP1_ADDRESS;
	return localQEP1->QPOSCNT;
}

//////////////////////////////////////////////////////////////////////////////////////
// setQEPEncoderRate
//
// set the required encoder rate
VOID setRequiredQEPEncoderRate(UINT32 rate)
{
	localQEP1Control.requiredRate = rate ;
	localQEP1Control.fbIntegrator = 0;

	localQEP1Control.pwmPerThousandths = getPrestartPWMPerThousandths(rate) ;
	setPWM0DutyCyclePerThousandths(localQEP1Control.pwmPerThousandths);
}

//////////////////////////////////////////////////////////////////////////////////////
// getPrestartPWMPerThousandths
//
UINT16 getPrestartPWMPerThousandths(UINT32 rate)
{
	UINT16 idx = rate/10;
	idx = MIN(idx, localSizeOfPreStart);
	return localPreStartDrive[idx] ;
}

//////////////////////////////////////////////////////////////////////////////////////
// feederQEP1LISR
//
// QEP ISR handler - currently for unit timer reaching its timeout register value.
VOID    feederQEP1LISR(INT vector)
{
	static PEQEP_STRUCT localQEP1 = (PEQEP_STRUCT)EQEP1_ADDRESS;

	// clear all interrupt flags...
	localQEP1->QCLR = 0xFFFF;
	localQEPLISRCount++;
	NU_Activate_HISR(&feederQEPHISR_CB) ;
}

//////////////////////////////////////////////////////////////////////////////////////
//	feederQEP1HISR
//
// high level ISR
VOID feederQEP1HISR()
{
	static PEQEP_STRUCT localQEP1 = (PEQEP_STRUCT)EQEP1_ADDRESS;

    localQEP1Control.lastRate = localQEP1->QPOSLAT;

    // apply feedback
    // note: hte integrator allow us to slow down feedback without slowing the sampling timers
    // currently 1 seems to work ok.
    if(localQEPLISRCount % FEEDBACK_INTEGRATOR_VALUE == 0)
    {
        if(localQEP1Control.fbIntegrator > 0)
        {
        	// too slow, integrator wants us to go faster
        	if(localQEP1Control.pwmPerThousandths < 1000)
        		setPWM0DutyCyclePerThousandths((localQEP1Control.pwmPerThousandths+=1));
        }
        else if(localQEP1Control.fbIntegrator < 0)
    	{
        	// too fast, integrator wants us to slow down...
        	if(localQEP1Control.pwmPerThousandths > 0)
        		setPWM0DutyCyclePerThousandths((localQEP1Control.pwmPerThousandths-=1));
    	}

        localQEP1Control.fbIntegrator = 0 ;
    }

	if(localQEP1Control.requiredRate > localQEP1Control.lastRate)
		localQEP1Control.fbIntegrator += 1;
	else if(localQEP1Control.requiredRate < localQEP1Control.lastRate)
   		localQEP1Control.fbIntegrator -= 1;

	// debug ... output to display port if open.
	if((localQEPLISRCount % 2000) == 0)
	{
	    /* get the system date/time so that this can be recorded along with the error */
		debugDisplayOnly("QEP Unit timer HISR Activated %5lu  Required = %3d : Actual = %3d  PWM/1000 = %3d\r\n",
				localQEPLISRCount,
				localQEP1Control.requiredRate,
				localQEP1Control.lastRate,
				localQEP1Control.pwmPerThousandths) ;
	}
}

