
/*************************************************************************
*   PROJECT:        Janus
*   COMPANY:        Pitney Bowes
*   AUTHOR :        Tim Zhang
*   MODULE NAME:    $Workfile:   TextMsg.c  $
*   REVISION:       $Revision:   1.8  $
*       
*   DESCRIPTION:    Text entry array functions
*
* -----------------------------------------------------------------------
*               Copyright (c) 2004 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* -----------------------------------------------------------------------
*
*   REVISION HISTORY:
*   $Log:   H:/group/SPARK/ARCHIVES/DM150/uic_app/TextMsg.c_v  $
 * 
 *    Rev 1.8   Jan 18 2005 04:49:24   NEX3RPR
 * Changed function fnDeleteTextByIndex to fix fraca 11342. -David
 * 
 *    Rev 1.7   16 Sep 2004 18:46:14   SA002PE
 * Changed to use the #defines in custdat.h so 
 * everyone is using the same values.
 * Namely:
 *   1. MAX_TEXT_ENTRY instead of TEXT_ENTRY.
 *   2. MAX_TEXT_ENTRY_LINES instead of TEXT_ENTRY_LINES.
 *   3. MAX_TEXT_ENTRY_UNICHARS instead of TEXT_ENTRY_UNICHARS.
 * 
 *    Rev 1.6   06 May 2004 11:02:24   CR002DE
 * 1.  Used the PCN parameters for Text Message size information instead of the macros. - Tim
 * 2. correctly merged Tim's changes with my own. - CD
 * 
 *    Rev 1.5   04 May 2004 12:02:04   CR002DE
 * 1. unit tested new functions, fixed null termination problem in fnSetTextEntryLineOfText
 * 
 *    Rev 1.4   04 May 2004 09:55:14   CR002DE
 * 1. add EMD bounds check of passed index to fnGetTextEntryComponentAddr
 * 
 *    Rev 1.3   03 May 2004 11:18:52   CR002DE
 * 1. added fnGetTextEntryName, fnSetTextEntryName,
 * fnGetTextEntryLineOfText, fnSetTextEntryLineOfText.
 * These functions enforce  index and string length restrictions
 * due to memory constraints and EMD parameters.
 * 
 *    Rev 1.2   Apr 29 2004 15:07:24   NEX5EAL
 * Abandoned using the active text message index CMOSTextEntryArray.bIndex on the OIT side. Get the active text message ID from BOB by calling fnAIGetActive() instead. - Tim
 * 
 *    Rev 1.1   26 Apr 2004 11:08:10   CR002DE
 * 1. added
 * const TEXT_ENTRY_COMPONENT* fnGetTextEntryComponentAddr(uchar ucIndex );
 * 
 *    Rev 1.0   Apr 22 2004 16:52:16   NEX5EAL
 * Initial version. - Tim
 * 
* 
*************************************************************************/

#include "TextMsg.h"
#include "unistring.h"
#include "custdat.h"
#include "bob.h"
#include "aiservic.h"
#include "datdict.h"
#include "cwrapper.h"

extern TEXT_ENTRY_ARRAY		CMOSTextEntryArray;
extern TEXT_ENTRY_COMPONENT	stTextEntryBuf;
extern SINT8				cCurrentTextLine;
extern UINT8				ucCurrentTextSlotInd;

/**************************************************************************
// FUNCTION NAME: 
//      fnGetTextEntryComponentAddr
//
// DESCRIPTION: 
//      Utility function that get the text entry component address of the
//		active text entry message. 
//
// INPUTS:  
//	 	ucIndex			-Text Entry Index (0 - x)
//
// RETURNS:
//	 	NULL  			-if the current passed index is out of range
//		pstActiveText	-address of text entry component with the index
//						passed in
//
// NOTES:
//
// MODIFICATION HISTORY:
//      03/24/2006	Oscar Wang     Adapted for FuturePhoenix
//      		 	Tim Zhang and Craig DeFilippo  Initial version
// *************************************************************************/
const TEXT_ENTRY_COMPONENT* fnGetTextEntryComponentAddr(UINT8 ucIndex )
{
	TEXT_ENTRY_COMPONENT * pstActiveText = NULL;
	
	if( (ucIndex < fnFlashGetByteParm(BP_TXTMSG_MAX_ALLOWED)) 
		&& (ucIndex < MAX_TEXT_ENTRY) )
	{
		pstActiveText = &(CMOSTextEntryArray.sTextEntryArray[ucIndex]);
	}

	return ( pstActiveText );
}

/**************************************************************************
// FUNCTION NAME: 
//      fnGetActiveTextEntryComponentAddr
//
// DESCRIPTION: 
// 		Utility function that get the text entry component address of the
//		active text entry message.
// 
// INPUTS:
// 		None
//
// RETURNS:
//	 	NULL  			-if current active text message set to None
//		pstActiveText	-address of active text entry component
//
// NOTES:
//
// MODIFICATION HISTORY:
//      	 	Tim Zhang  	Initial version
// *************************************************************************/
const TEXT_ENTRY_COMPONENT* fnGetActiveTextEntryComponentAddr( void )
{
	TEXT_ENTRY_COMPONENT * pstActiveText = NULL;
	UINT16	usImageID;
	
	if (fnAIGetActive(TEXT_COMP_TYPE, &usImageID) != SUCCESSFUL)
	{
		fnProcessSCMError();
		goto xit;
	}
	
	if ( usImageID != TEXT_SELECT_NONE && usImageID != TURN_OFF_IMAGE)
		pstActiveText = &(CMOSTextEntryArray.sTextEntryArray[usImageID -1]);
xit:
	return ( pstActiveText );
}
	
/**************************************************************************
// FUNCTION NAME: 
//      fnFindFreeTextEntryComponentAddr
//
// DESCRIPTION: 
// 		Utility function that search an empty text message slot and return 
//		the pointer to it.
//
// INPUTS:
// 		None
//
// RETURNS:
//	 	NULL  			-if no free slot is found
//		pstActiveText	-address of the 1st free text entry component
//
// NOTES:
//
// MODIFICATION HISTORY:
//      03/24/2006	Oscar Wang	Adapted for FuturePhoenix
//		      	 	Tim Zhang  	Initial version
// *************************************************************************/
TEXT_ENTRY_COMPONENT* fnFindFreeTextEntryComponentAddr( void )
{
	UINT8 					ucIdx ;
	TEXT_ENTRY_COMPONENT 	*pstRetval = NULL;
	UINT8 ucMaxTextMsgNum =  fnFlashGetByteParm(BP_TXTMSG_MAX_ALLOWED); 
	
	for ( ucIdx = 0; ucIdx < ucMaxTextMsgNum && ucIdx < MAX_TEXT_ENTRY; ucIdx++)
	{
		if ( CMOSTextEntryArray.sTextEntryArray[ucIdx].sTextEntryName[0] == 0 )
		{
			/* find a empty slot, jump out. */
			pstRetval = &CMOSTextEntryArray.sTextEntryArray[ucIdx];
			break;
		}
	}

	return ( pstRetval );
}

/**************************************************************************
// FUNCTION NAME: 
//      fnFind1stTextComponentAddr
//
// DESCRIPTION: 
// 		Utility function that search the first avaialbe text message slot
//		index.
//
// INPUTS:
// 		None
//
// RETURNS:
//	 	NULL  			-if no occupied slot is found
//		pstActiveText	-address of the 1st occupied text entry component
//
// NOTES:
//
// MODIFICATION HISTORY:
//      05/25/2006  John Gao    Business rules extraction
//      03/24/2006	Oscar Wang	Adapted for FuturePhoenix
// 		     	 	Tim Zhang  	Initial version
// *************************************************************************/
TEXT_ENTRY_COMPONENT* fnFind1stTextComponentAddr(void)
{
	UINT8	ucIdx;
	TEXT_ENTRY_COMPONENT *  pstRetval = NULL;
	UINT8 	ucMaxTextMsgNum =  fnGetTxtmsgMaxAllowed(); 
	TEXT_ENTRY_ARRAY *pTextEntryArray = fnGetCMOSTextEntryArray();

	for ( ucIdx = 0; ucIdx < ucMaxTextMsgNum && ucIdx < MAX_TEXT_ENTRY; ucIdx++)
	{
		if ( pTextEntryArray->sTextEntryArray[ucIdx].sTextEntryName[0] != 0 )
		{
			// find an occupied slot, jump out.
			pstRetval = &pTextEntryArray->sTextEntryArray[ucIdx];
			break;
		}
	}

	return ( pstRetval );
}

/**************************************************************************
// FUNCTION NAME: 
//      fnFindTextEntryCompAddrByName
//
// DESCRIPTION: 
// 		Utility function that search the Manual Text Entry index by
//		name and returns the address of the text component.
//
// INPUTS:
// 		psName			-text name to be sought
//
// RETURNS:
//	 	NULL  			-if no text message with this name found.
//		pstRetval		-address of text entry component with the name
//
// NOTES:
//
// MODIFICATION HISTORY:
//      03/24/2006	Oscar Wang	Adapted for FuturePhoenix
// 		     	 	Tim Zhang  	Initial version
// *************************************************************************/
TEXT_ENTRY_COMPONENT*  fnFindTextEntryCompAddrByName(  UINT16 	* psName)
{
	UINT8 	ucIdx;
	TEXT_ENTRY_COMPONENT *  pstRetval = NULL;
	UINT8 	ucMaxTextMsgNum =  fnFlashGetByteParm(BP_TXTMSG_MAX_ALLOWED); 
	
	for ( ucIdx = 0; ucIdx < ucMaxTextMsgNum && ucIdx < MAX_TEXT_ENTRY; ucIdx++)
	{
		if ( CMOSTextEntryArray.sTextEntryArray[ucIdx].sTextEntryName[0] != 0 )
		{
			if ( unistrcmp( psName,  
				CMOSTextEntryArray.sTextEntryArray[ucIdx].sTextEntryName ) == 0)
			{	
				pstRetval = &CMOSTextEntryArray.sTextEntryArray[ucIdx];
				break;
			}
		}
	}

	return ( pstRetval );
}

/**************************************************************************
// FUNCTION NAME: 
//      fnDeleteTextByIndex
//
// DESCRIPTION: 
// 		Utility function that delete a text by the index passed in.
//
// INPUTS:
// 		ucIdx			-the component index to be deleted.
//
// RETURNS:
//	 	true  			-delete successfully.
//		false			-index is out of range or there is no active text
//						message
//
// NOTES:
//
// MODIFICATION HISTORY:
//      03/24/2006	Oscar Wang	Adapted for FuturePhoenix
//		      	 	Tim Zhang  	Initial version
// *************************************************************************/
BOOL fnDeleteTextByIndex(  UINT8 ucIdx)
{
	BOOL 	fRet = false;
	UINT16	usImageID;
	UINT8 	ucMaxTextMsgNum =  fnFlashGetByteParm(BP_TXTMSG_MAX_ALLOWED); 
	
	if ( ucIdx < ucMaxTextMsgNum && ucIdx < MAX_TEXT_ENTRY )
	{
		/* clear the text now. */
		memset( &(CMOSTextEntryArray.sTextEntryArray[ucIdx]), 0 , 
					sizeof( CMOSTextEntryArray.sTextEntryArray[ucIdx]) );
		fRet = true;

		/* if we are deleting the actvie text message,
		we must inform BOB to set the active text message to NONE.*/
		if (fnAIGetActive(TEXT_COMP_TYPE, &usImageID) != SUCCESSFUL)
		{
			fnProcessSCMError();
			fRet = false;
		}
		else if ( usImageID == ucIdx + 1)
		{
			if ( fnAISetActive(TEXT_COMP_TYPE, TEXT_SELECT_NONE) != SUCCESSFUL )
			{
				fnProcessSCMError();
				fRet = false;
			}
		}
	}

	return ( fRet );
}

/**************************************************************************
// FUNCTION NAME: 
//      fnGetTextEntryName
//
// DESCRIPTION: 
// 		Get Read only pointer to name of Text Entry component
//
// INPUTS:
// 		ucTextEntryIndex	-component index (0 - x)
//
// RETURNS:
//	 	psTextEntryName  	-read only pointer to name of text entry component
//
// NOTES:
//
// MODIFICATION HISTORY:
//      01/29/1999 	Craig DeFilippo   	Initial version
// *************************************************************************/
const UINT16 * fnGetTextEntryName(UINT8 ucTextEntryIndex)
{
	const UINT16  *psTextEntryName = NULL;

	if( (ucTextEntryIndex < fnFlashGetByteParm(BP_TXTMSG_MAX_ALLOWED)) 
		&& (ucTextEntryIndex < MAX_TEXT_ENTRY ) )
	{
		psTextEntryName = CMOSTextEntryArray.sTextEntryArray[ucTextEntryIndex]
							.sTextEntryName;
	}
	
	return(psTextEntryName);
}	

/**************************************************************************
// FUNCTION NAME: 
//      fnSetTextEntryName
//
// DESCRIPTION: 
// 		Set name of Text Entry component. Copies only if text entry component
//		is within range. 
//
// INPUTS:
// 		ucTextEntryIndex	-component index (0 - x)
//		psTextEntryName		-pointer to text name
//
// RETURNS:
//	 	TEXT_ENTRY_SUCCESS  				-set successfully.
//		TEXT_ENTRY_COMP_INDEX_OUT_OF_RANGE	-if index is out of range
//
// NOTES:
//		copies only maximum of TEXT_NAME_LEN characters
//
// MODIFICATION HISTORY:
//      01/29/1999 	Craig DeFilippo   Initial version
// *************************************************************************/
UINT8 fnSetTextEntryName(UINT8 ucTextEntryIndex, 
						 UINT16 *psTextEntryName)
{
	UINT8 ucRetval = TEXT_ENTRY_SUCCESS;

	if( (ucTextEntryIndex < fnFlashGetByteParm(BP_TXTMSG_MAX_ALLOWED)) 
		&& (ucTextEntryIndex < MAX_TEXT_ENTRY ) )
	{
		(void)unistrncpy(CMOSTextEntryArray.sTextEntryArray[ucTextEntryIndex].
				sTextEntryName, psTextEntryName, TEXT_NAME_LEN);

		/* null terminate in case source was greater than or equal to
		TEXT_NAME_LEN */
		CMOSTextEntryArray.sTextEntryArray[ucTextEntryIndex].
			sTextEntryName[TEXT_NAME_LEN] = 0;
	}
	else
	{
		ucRetval = TEXT_ENTRY_COMP_INDEX_OUT_OF_RANGE;
	}

	return( ucRetval );
}

/**************************************************************************
// FUNCTION NAME: 
//      fnGetTextEntryLineOfText
//
// DESCRIPTION: 
// 		Get read only pointer to text entry line of text.
//
// INPUTS:
// 		ucTextEntryIndex		-component index (0 - x)
//		ucTextEntryLineIndex	-text line index
//
// RETURNS:
//	 	NULL  					-if passed component index is out of range or if
//								passed line index is out of range.
//		psTextEntryLineOfText	-pointer to text entry line
//
// NOTES:
//
// MODIFICATION HISTORY:
//      01/29/1999 	Craig DeFilippo		Initial version
// *************************************************************************/
const UINT16 *fnGetTextEntryLineOfText(UINT8 ucTextEntryIndex,
										UINT8 ucTextEntryLineIndex)
{
	const UINT16 *psTextEntryLineOfText = NULL;

	if( (ucTextEntryIndex < fnFlashGetByteParm(BP_TXTMSG_MAX_ALLOWED)) 
		&& (ucTextEntryIndex < MAX_TEXT_ENTRY ) 
		&& (ucTextEntryLineIndex < fnFlashGetByteParm(BP_TXTMSG_MAXLINES_ALLOWED))
		&& (ucTextEntryLineIndex < MAX_TEXT_ENTRY_LINES) )
	{
		psTextEntryLineOfText = CMOSTextEntryArray.
		sTextEntryArray[ucTextEntryIndex].sTextEntryData[ucTextEntryLineIndex];
	}
	
	return(psTextEntryLineOfText);
}	

/**************************************************************************
// FUNCTION NAME: 
//      fnSetTextEntryLineOfText
//
// DESCRIPTION: 
// 		Set line of text for a text entry component.
//
// INPUTS:
// 		ucTextEntryIndex			-component index (0 - x).
//		ucTextEntryLineIndex		-line index
//		psTextEntryLineOfText		-pointer of text content
//
// RETURNS:
//	 	TEXT_ENTRY_SUCCESS  				-set text line content successfully.
//		TEXT_ENTRY_LINE_INDEX_OUT_OF_RANGE	-out of component range of out of
//											line range
//
// NOTES:
//
// MODIFICATION HISTORY:
//      03/24/2006	Oscar Wang			Adapted for FuturePhoenix
//      01/29/1999 	Craig DeFilippo   	Initial version
// *************************************************************************/
UINT8 fnSetTextEntryLineOfText(UINT8 ucTextEntryIndex, 
							   UINT8 ucTextEntryLineIndex,
							   UINT16 *psTextEntryLineOfText)
{
	UINT8 ucRetval = TEXT_ENTRY_SUCCESS;
	UINT8 ucNumChars = fnFlashGetByteParm(BP_TXTMSG_MAXCHARS_LINE_ALLOWED);

	if( (ucTextEntryIndex < fnFlashGetByteParm(BP_TXTMSG_MAX_ALLOWED))
		&& (ucTextEntryIndex < MAX_TEXT_ENTRY ) )
	{
		if( (ucTextEntryLineIndex < fnFlashGetByteParm(BP_TXTMSG_MAXLINES_ALLOWED)) 
			&& (ucTextEntryLineIndex < MAX_TEXT_ENTRY_LINES) )
		{
			if(ucNumChars > MAX_TEXT_ENTRY_UNICHARS)
				ucNumChars = MAX_TEXT_ENTRY_UNICHARS;

			(void)unistrncpy(CMOSTextEntryArray.sTextEntryArray[ucTextEntryIndex].
					sTextEntryData[ucTextEntryLineIndex],
					psTextEntryLineOfText,
					ucNumChars);

			/* null terminate in case source was greater than or equal to
			ucNumChars */
			CMOSTextEntryArray.sTextEntryArray[ucTextEntryIndex].
				sTextEntryData[ucTextEntryLineIndex][ucNumChars] = 0;
		}
		else
		{
			ucRetval = TEXT_ENTRY_LINE_INDEX_OUT_OF_RANGE;
		}
	}
	else
	{
		ucRetval = TEXT_ENTRY_COMP_INDEX_OUT_OF_RANGE;
	}

	return(ucRetval);
}

/**************************************************************************
// FUNCTION NAME: 
//      fnFindFreeTextSlot
//
// DESCRIPTION: 
// 		Utility function that search an empty text message slot index.
//
// INPUTS:															  
//		None.
//
// RETURNS:
//	 	0  			-if no empty slot found.
//		ucRetval	-the first empty slot index + 1.
//
// NOTES:
//
// MODIFICATION HISTORY:
//      03/24/2006	Oscar Wang		Adapted for FuturePhoenix
//		      	 	Tim Zhang  		Initial version
// *************************************************************************/
UINT8	fnFindFreeTextSlot( void )
{
	UINT8	ucIdx;
	UINT8	ucRetval = 0;
	UINT8 	ucMaxTextMsgNum =  fnFlashGetByteParm(BP_TXTMSG_MAX_ALLOWED); 

	for (ucIdx = 0;
		(ucIdx < ucMaxTextMsgNum) && (ucIdx < MAX_TEXT_ENTRY);
		 ucIdx++)
	{
		if ( CMOSTextEntryArray.sTextEntryArray[ucIdx].sTextEntryName[0] == 0 )
		{
			/* find a empty slot, jump out. */
			ucRetval = ucIdx + 1;
			break;
		}
	}

	return ( ucRetval );
}

/**************************************************************************
// FUNCTION NAME: 
//      fnCopyTextMessageToBuffer
//
// DESCRIPTION: 
// 		Utility function that copy text message to buffer index.
//
// INPUTS:															  
//		None.
//
// RETURNS:
//		None.
//
// NOTES:
//
// MODIFICATION HISTORY:
//      05/25/2006  John Gao    Business rules extraction
//      03/24/2006	Oscar Wang		Initial version
// *************************************************************************/
void	fnCopyTextMessageToBuffer( void )
{
	/*  make sure that it does not exceed the boundary */
	if ( ucCurrentTextSlotInd > 0 
		&& ucCurrentTextSlotInd <= fnGetTxtmsgMaxAllowed())
	{ 
		memcpy( &stTextEntryBuf, 
			&(fnGetCMOSTextEntryArray()->sTextEntryArray[ ucCurrentTextSlotInd - 1]),
			sizeof( stTextEntryBuf) );
	}
}

/**************************************************************************
// FUNCTION NAME: 
//      fnFindTextEntryCompAddrByName
//
// DESCRIPTION: 
// 		Utility function that search the Manual Text Entry index by
//		name and returns the index of text message.
//
// INPUTS:															  
//		psName 		-name to be sought
//
// RETURNS:
//		TEXT_SELECT_NONE	-if no text message with this name found.
//		ucRetIdx			-component index with the same name.
//
// NOTES:
//
// MODIFICATION HISTORY:
//      03/24/2006	Oscar Wang		Initial version
// *************************************************************************/
UINT8  fnFindTextEntryIndexAddrByName(  UINT16	* psName)
{
	UINT8	ucRetIdx = TEXT_SELECT_NONE;
	UINT8 	ucTextSlotIdx = 0;
	UINT8 	ucMaxTextMsgNum =  fnFlashGetByteParm(BP_TXTMSG_MAX_ALLOWED); 
	
	for ( ucTextSlotIdx = 0; 
		 (ucTextSlotIdx < ucMaxTextMsgNum) && (ucTextSlotIdx < MAX_TEXT_ENTRY);
		  ucTextSlotIdx++ )
	{
		if ( CMOSTextEntryArray.sTextEntryArray[ucTextSlotIdx].sTextEntryName[0]
				 != 0 )
		{
			if ( unistrcmp( psName,  CMOSTextEntryArray.
					sTextEntryArray[ucTextSlotIdx].sTextEntryName ) == 0)
			{	
				ucRetIdx = ucTextSlotIdx + 1;
				break;
			}
		}
	}

	return ( ucRetIdx);
}
