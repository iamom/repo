// **************************************************************
// Filename:                tealeavs.c
//
// !! Before checking in, be sure to read important NOTES section below !!
//
// DESCRIPTION:    
//  Tables, generated from the orbit database and used by the bobtask.  
//  Contains the Tealeaves table, which has offsets to all the other tables:
//  bobtask scripts, error conversion tables, Message Control Table, message 
//  header table, appdata table, replydata table, variable access tables, 
//  tables for updating the date and time, and more.
//
// File Location for build:     FPHX_SADev\fpapp\source
//
// Extraction Details:
//   Orbit Version:           2.3.5-1
//   Orbit Database Name:     FUTURE_PHOENIX2009
//    Extraction Date:        07-26-2010 12:09:10  
//    Backup File Name:       FUTURE_PHOENIX2009_26_7_2010_12_30.BAK.gz
//
// Original file extraction location:  Laptop CL501BE-SHL2-01
//      C:\cc_views\cl501be_S1_F_02.07_int\FPHX_SADev\fpapp\source\tealeavs.c
//
//----------------------------------------------------------------------
//              Copyright (c) 1998 Pitney Bowes Inc.
//                   35 Waterview Drive
//                  Shelton, Connecticut  06484
//----------------------------------------------------------------------
// NOTES:
//  1.   This file is generated automatically from the Orbit data base
//  2.   Manual edits to this file will be lost!
//  3.   Header information, including HISTORY, is retained and updated 
//       BY HAND !! after extraction.
//----------------------------------------------------------------------
//   REVISION HISTORY:
/*
 2018.08.09 Clarisa Bellamy  CSD-Sr
 - By Hand:  Changed the variable type of GET_VLT_PIECE_COUNTER from 
     DECIMAL_TO_STRING(0x01) to LE_ULONG_TO_STRING(0x10), because Decimal
     version is too short for this type, and the LE_ULONG_TO_STRING has been  
     fixed now. 
 - By Hand: changed the comments for the Conversion type LONG_TO_STRING (0x05)  
  	 to SLONG_TO_STRING, because the name was changed in bobutils.h to be less 
  	 ambiguous and reflect what the function actually does.
 
 2018.08.09 Clarisa Bellamy  CSD-Sr
 - By Hand:  Changed the variable type of GET_VLT_PIECE_COUNTER from 
    LONG_TO_STRING (0x05)  to DECIMAL_TO_STRING	 (0x01), because the former
	type is presumed to be stored in BE format, as it is in the PSD, but the 
	latter is presumed to be stored in LE format, as it is on the processor. 
	This is because it is retrieved from the PSD Glob and the Glob2Blob2Glob 
	functions can account for the bob variable needing to be stored as LE. 

 2018.06.26 Clarisa Bellamy  CSD-Sr
 - Change the timeout, by hand, for the self-test message from 1/2 second (0x0) 
   to 2 seconds. (0x03) because 
	 1) this message can take longer than 500 ms on an ECDSA ibutton, and 
	 2) handling other messages (such as the Pong message) can happen between 
	    the response from the ibutton and the reply to the bobTask, causing 
	    the roundtrip from BOB to Ibutton and back to take longer than 1 second. 

 2012.08.24 Clarisa Bellamy     fphx02.10
  THIS FIX is done "BY HAND", meaning that it is not in the database yet!!!!!!
  - Fix problem where German refill does not correctly save the DC status when 
    the refill is unsuccessful.  Set the TRY_TO_DISTRIB flag in the msg control
    record for the German PVD response message.

 2010.07.26 Clarisa Bellamy     FPHX 02.07
  - Add another line to the SCRIPT_UPDATE_INDICIA_TO_PRINT, which handles 
    indicias printed as leading images. For now, lagging images do not change 
    on the fly, so this script doesn't need to update them.
    NOTE: This did not change the orbit.h file.

 2010.07.24 Clarisa Bellamy     FPHX 02.07
  Add access to current bobs current indicia type:
  - Add new variable id:    BOBID_UPDATE_AND_GET_INDICIA_TO_PRINT
  - Add new script id:  SCRIPT_UPDATE_INDICIA_TO_PRINT
  - Add new script, with 1 line, and new entry in MsgStartegy tab for this 
    script.
  - Add new entry in bobamat0 table, for the new variable ID, which calls the 
    new script.

 2010.07.19 Clarisa Bellamy     FPHX 02.07
  Support for Auto Ads:
  - Added Variable ID:  BOBID_UPDATE_AND_READ_AD_SELECT
  - Added Script ID:    SCRIPT_UPDATE_AD_SELECTION
  - Added 2 ErrorCodes:         JSCM_AUTO_AD_NOT_FOUND      JSCM_AUTO_AD_OUT_OF_RANGE   
  - Added 2 InternalBobErrors:  BOB_AUTO_AD_NOT_FOUND       BOB_AUTO_AD_OUT_OF_RANGE
  - Added Function ID:  fnDetermineAdSelection
  - Added new script and entry in strategy table for SCRIPT_UPDATE_AD_SELECTION.
     Only calls fnDetermineAdSelection.
  - Added entry in Variable Access table for  BOBID_UPDATE_AND_READ_AD_SELECT, that
     is ALWAYS_SCRIPTED, and runs the  SCRIPT_UPDATE_AD_SELECTION script.
  - Added entries in bobsInternalLookupTable for the new error codes.
  - Add a line in each of these scripts, calling fnDetermineAdSelection:
        SCRIPT_STATIC_INDICIA
        SCRIPT_STATIC_AD_INSC
        SCRIPT_STATIC_PERMIT
        SCRIPT_STATIC_DATE_TIME

 2010.07.17 Clarisa Bellamy     FPHX 02.02
  Support for Dcap/AR discrepancy fix: 
  - Added 2 new bobtask and JSCM_ error codes, 0x63 and 0x64:
            BADTIME_BOBSPOSTAGEVAL_CHANGE
            BAD_REGISTER_HANDUPDATE       
  - Added 2 new entries to the bobInternalLookup table.
 
 2009.07.28  Clarisa Bellamy     FPHX 02.00
  To add support of Auto Inscriptions in WOW mode:
  - Add new Variable ID and entry in BOBAMAT0:   BOBID_UPDATE_AND_READ_INSCR_SELECT 
  - Modify entry in BOBAMAT0 for READ_CURRENT_INSCR_SELECT, so that it does NOT
    call a script anymore, and only reads the current inscription.
  - Changed the name of the private subscript from SCRIPT_GET_INSCRIPTION_SELECTION 
    to SCRIPT_UPDATE_INSCRIPTION_SELECTION.

 2009.07.24  Clarisa Bellamy     FPHX 02.00
  To fix problem with PCT for Asteroids, change the timeout selection in the
  Message Control Table for these messages:
    IPSD_GENERATE_PSD_KEY       From � second to 5 seconds.
    IPSD_LOAD_PROVIDER_KEY      From 1 second to 2 seconds.

 2009.07.20  Clarisa Bellamy     FPHX 02.00
  - Added new variable ID:    BOBID_IPSD_FIRMWAREVERDATESTR
  - Added new entry in BOBAMAT0 table for access to this variable.

 2009.06.15  Clarisa Bellamy     FPHX 01.15
  Gemini Boot loader support:
  - Added 1 new message header and header ID:   JHDIPSDMANUFACTURE 
  - Add 1 new device message, and entry in the message control table:       
        IPSD_MANUFACTURE.
  - Added 4 new script IDs, Scripts, and entries in MsgStrategy table:
        SCRIPT_EDM_IPSD_RUN_SELF_TESTS 
        SCRIPT_IPSD_MANUFACTURE         
        SCRIPT_EDM_IPSD_MANUFACTURE     
        SCRIPT_CHECK_GEMINI
  - Moved the Gemini stuff from the SCRIPT_INIT_IPSD into its own subscript,
    which is called from SCRIPT_INIT_IPSD.          
  - Added the BOBID_ prefix to 5 variable IDs.
  - Added 2 new variable IDs, and entries in the Bobamat0 table.  
        BOBID_LD_IPSD_TRANSPORT_LOCK_KEY   
        BOBID_RUNSELFTESTS_GETSTATUS
  - In msgControlTable, changed the timeout for the IPSDBL_PASSTHROUGH_MSG
    message from 2 seconds to 5 seconds, and the timeout for the MANUFACTURE
    message from 1 second to 2 seconds.     

 2009.03.02 - 2009.03.12 Clarisa Bellamy     FPHX 01.15
  Gemini Boot loader support:
  - Added new msg Header, and msg Header ID: JHDIPSDBLEXITAPPMODE
  - Added new entry in the message control table, and a new device message 
    ID: IPSDBL_EXIT_APPMODE
  - Change timeout index for the IPSD_RUN_SELF_TESTS message to 1/2 second 
    (from two seonds.)
  - Modify SCRIPT_INIT_IPSD, to include 2 new lines, one that exits the 
    application mode if the SelfTests failed, and one that gets the MicroStatus
    again.  

 2009.03.23 - 2009.03.12 Clarisa Bellamy     FPHX 01.15
  For new RandomNumberGenerator error detection:
  - Added new IPSD error code:      IPSDERR_REPEATED_RNG 
  - Add 1 new entry to the IPSDLookup Table.
  More changes for Gemini Boot Loader support:
   Enumerations:
   - Added 2 more JSCM_ and BOB_ class errors:
            BOB_IPSD_BL_TOO_RISKY
            BOB_IPSD_BL_EDM_ERR
   - Added 1 new Device Message ID:      IPSD_RUN_SELF_TESTS
   Tables:
   - Added 1 new entry to MessageControlTable: IPSD_RUN_SELF_TESTS
   - In Bobamat0 table, changed entry for BOBID_IPSD_BL_PASSTHROUGH_MSG from 
     ALWAYS_SCRIPTED to XFER_CONTROL_SCRIPT.
   - Added 2 new entries to the bobInternalLookup table.
   - Changed SCRIPT_INIT_IPSD by adding a new line to run the selftests if we
     have a Gemini in Application mode.

 2009.03.09 - 2009.03.12 Clarisa Bellamy     FPHX 01.15
  Merged change from Janus:
  - Added new bobsInternalError definition: BOB_GLOB_VAR_TOO_SMALL   
    and corresponding jbobtask error code: JSCM_GLOB_VAR_TOO_SMALL
  - Add a new entry to the bobInternalLookup table for BOB_GLOB_VAR_TOO_SMALL.

  Changes for Gemini Boot Loader support:
   Enumerations:
   - Added 11 new bobsInternalError definitions and corresponding jbobtask 
     error codes. 
   - Added 10 new variable IDs BOBID_IPSD_BL_*
   - Added 1 new variable ID: BOBID_IPSD_GET_PROG_MODE
   - Added 1 new ErrorClassCode: ERROR_CLASS_IPSD_BL
   - Added 1 new Error Directory Table Name: IPSDBLLookup
   - Added 7 new Error Codes for the new IPSD_BL error class.
   - Added 1 new Record Length Type: ERRLOOKUP_REK_SIZ_BYTE
   - Added 4 new function cross reference IDs: 
            fnPreCheckIPSDType, 
            fnPostCheckGeminiMode,
            fnPreGBLPassthroughFromEDM, 
            fnPostGBLPassthroughFromEDM
   - Added 1 new Device: IPSD_BL
   - Add a new Reply Type: RPLY_IPSD_BL. 
   - Added 2 new reply Lengh Exception Codes:
      LEX_SIMPLE_COPY, which is used to store the entire message, but keep 
                       the reply ptr where it is.
      LEX_REDIRECTED_IPSD_BL, which is used to reformat and store the Boot
                    Loader reply in a Transfer Control structure for the 
                    EDM task.
   - Added 2 new message header IDs: 
            JHDIPSDBLGETDEVICESTATUS  
            JHD_DUMMY  (used when the header is actually in the data passed
                        through a Transfer Control structure.)
   - Added 2 new reply definition ID: 
            RPDIPSDBL_BL_GENERIC
            RPDIPSDBL_REDIRECTED
   - Added 2 new deviceMsgIDs: 
        IPSDBL_GET_DEVICE_STATUS 
        IPSDBL_PASSTHROUGH_MSG.
   - Added 1 new Script ID (Intertask Message ID):
        SCRIPT_GBL_PASSTHROUGH_EDM
   Tables:
   - Add a new lookup table, IPSDBLLookup, with 7 entries.
   - Add 12 new entries to the bobInternalLookup table.
   - Add a new entry to BobsErrorDirectory table.
   - Add 3 new entries to the Bobamat0 table: 
        BOBID_IPSD_BL_PASSTHROUGH_MSG
        BOBID_IPSD_GET_PROG_MODE
        BOBID_IPSD_BL_GET_MICRO_STATUS
   - Add 2 new Reply Data Definition records.
   - Add 2 new Message Headers.
   - Add 2 new entries to the MessageControl table
   - To the SCRIPT_INIT_IPSD script, add a line to call fnPreCheckIPSDType,
     send the IPSDBL_GET_DEVICE_STATUS message to the IPSD_BL device, then
     call the fnPostCheckGeminiMode function.  This is the new 2nd line in 
     the script.
   - Add 1 new script, and a new entry for it in the message strategy table.

 2008.08.28 Clarisa Bellamy     FPHX 01.11
  - Database is backed up in file FUTURE_PHOENIX2007_29_8_2008_0_18.BAK.gz
  For NetSet2 support:
  - Added call to new function fnGetVasBarcodes to debit script, before the 
    commit_transaction message.

 2008.04.14 Clarisa Bellamy     FPHX 01.11
   - Database is backed up in file FUTURE_PHOENIX2007_17_4_2008_16_16.BAK.gz
   - Two new JSCM_ error codes for glob overrun.  Add the 2 new BOB error
     codes, the two new JSCM_ error codes, and the two new entries to the Bob 
     Internal error code table.

 2008.04.14 Clarisa Bellamy     FPHX 01.11
   - Database is backed up in file FUTURE_PHOENIX2007_11_4_2008_16_4.BAK.gz
   - Requires changes in ipsd.c, ipsd.h, and cjunior.c.
   Fix the problem where rx'd data globs all had their lengths stored in the 
   same variable.  
     - Add these 3 variable IDs for the lengths of received globs.  They do NOT 
       need to be in BOBAMAT, since they should not be accessed outside of the 
       bobtask.
        BOBID_IPSD_PARAMLISTGLOB_SIZE 
        BOBID_IPSD_FLEXDEBIT_OUTPUT_GLOB_SIZE
        BOBID_IPSD_DEBIT_OUTPUT_GLOB_SIZE
    - Modidfy these 3 reply definitions to store the size of the glob received 
      in its very own variable, by adding a no-size, GLOB_LEN_LEN entry to each,  
      immediately AFTER the glob entry, and before the status word.
         RPDIPSDINDICIADATAGLOB
         RPDIPSDPARAMLISTGLOB
         RPDIPSDFLEXDEBITOUTPUTGLOB

 2008.03.18 Clarisa Bellamy     FPHX 01.10
   - Remove IPSD_CREATE_INDICIUM_FLEX_DEBIT and IPSD_PRECREATE_INDICIUM_FLEX_DEBIT
     from the WhenAmI table because it causes an update of the structure BETWEEN the
     loading of the barcode data and the Human Readable data.  
     The function fnIPSDLoadCreateIndiciaData calls the fnMsgDateTime function 
     explicitly, so that most of the WhenAmI table entries are not necessary with the 
     I-button.
     This change only affected tealeavs.c, not orbit.h, but they are checked in as a set.

 2008.03.18 Clarisa Bellamy     FPHX 01.10 
  Database Repair:
  - The FUTURE_PHOENIX2007 orbit database had errors in it that made the include and 
    source files NOT match up.  Bob Arsenault fixed the database, and I did a new 
    extraction.  The database is saved as: FUTURE_PHOENIX2007_18_3_2008_10_28.BAK.gz

 2008.03.13 Clarisa Bellamy     FPHX 01.10 
  Cleanup:
   - Changed temp script CB_PRINT_COMPLETE to SCRIPT_PRINT_COMPLETE
   - Change the name of variable ID WIPSD_FIRMWAREVERNUM to LIPSD_FIRMWAREVERNUM
  Merged miscellaneous changes from Janus to orbit database:
   - Changed the order of items in the REFUND_RESPONSE_RECORD script.
   - Changed SCRIPT_IPSD_GET_PSDPARAMS so that it does not call fnPOSTIPSDGETPARAMLIST
     at the end, because the calling script does that now.
   - Change time on Enable and Disable messages.
  For future merge:
   - To the SCRIPT_INIT_IPSD script, added call to fnInitShadowLogs
  Merged changes for flex debit to orbit database:
   - Add 2 new bob errors, and the BOB_INTERNAL_LOOKUP_SIZ definition, and added all
     3 to the bobInternalLookup Table.
   - Added 2 new entries to the IPSD error lookup table.
   - Added 3 new entries to the IGB error lookup table.
   - Added 25 new variable IDs.
   - Added 8 new message IDs for 9.0 i-button, and 8 new entries to the MsgControl 
     table, 5 new message headers, 2 new message reply definitons.
   - Removed the reply definition for the getPSDParameters message, since this
     now uses glob technology.
   - Added 4 new messages to the WhenAmI table.
   - Changed some bobamat0 entries from STATIK_PSD to STATIK_IPSD.
   - Changed bobamat0 entries for GET_VLT_PIECE_COUNTER, BOBID_IPSD_ZEROPOSTAGEPIECECOUNT
     to always scripted. The script will determine which message to send, depending
     on the i-button version.
   - Added 15 new bobamat0 entries.
   - Add 3 new mfgParmaMat0 entries.
   - Added 10 new scripts and changed some: 
   - Replaced SCRIPT_IPSD_PRECREATEINDICIA with SCRIPT_IPSD_PRECRE_INDI_STD
   - Replaced SCRIPT_IPSD_PRECRE_GERMAN_INDI with SCRIPT_IPSD_PRECRE_INDI_GERM and
   - Removed SCRIPT_IPSD_PRECRE_STD_INDI.
   - Changed SCRIPT_IPSD_PRE_DEBIT script to call SCRIPT_IPSD_PRECRE_INDI_STD by
     default.
   - Updated the MsgStrategy table. (Script Directory)
------------------------------------------------------------------------------*/
//<EOH>
const unsigned char tealeaves[] = {
/*
Tealeavs Constants Section
*/
/* Tea Header Field               */0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
/* TeaDirectory Offset            */0x00,0x00,0x00,0x24,
/* teaConstMsgTableSize           */0x00,0x37,
/* teaConstHeaderCount            */0x00,0x3A,
/* teaConstAppDataCount           */0x00,0x0B,
/* teaConstReplyCount             */0x00,0x15,
/* teaConstMaxDateDependence      */0x07,
/* teaConstMaxDateException       */0x00,
/* teaConstMaxSysDateReference    */0x0C,
/* teaConstMaxPrintDateReferences */0x06,
/* teaConstMaxTimeoutIndex        */0x07,
/* teaConstErrorGroupCount        */0x04,
/* teaConstMaxReplyMaps           */0x02,
/* teaConstBobDevices             */0x01,
/* teaConstScriptCount            */0x00,0x69,
/* teaConstFakeMsgCount           */0x00,0x00,
/* teaConstPCTTableCount          */0x00,0x00,
/* teaConstSDOIDCount             */0x00,0x00,
/* currOffset = 0x24 */
0x00,0x00,0x00,0x6C,  /* Offset to Device Table */
0x00,0x00,0x00,0x76,  /* Offset to Msg Control Table */
0x00,0x00,0x04,0x15,  /* Offset to Head Data Index */
0x00,0x00,0x06,0x0F,  /* Offset to Append Data Index */
0x00,0x00,0x06,0x7E,  /* Offset to Reply Data Index */
0x00,0x00,0x10,0xB0,  /* Offset to Script Index */
0x00,0x00,0x07,0xB7,  /* Offset to Data Mat Index */
0x00,0x00,0x03,0x78,  /* Offset to WhenAmI Table */
0x00,0x00,0x03,0xA9,  /* Offset to xCeptAmI Table */
0x00,0x00,0x03,0xA9,  /* Offset to MsgSysDates Table */
0x00,0x00,0x03,0xD9,  /* Offset to Msg Print Dates Tables */
0x00,0x00,0x03,0xF1,  /* Offset to TimeoutList Table */
0x00,0x00,0x0C,0x07,  /* Offset to BobsErrorDirectory */
0x00,0x00,0x10,0xA2,  /* Offset to ReplyMaps Index */
0x00,0x00,0x04,0x0D,  /* Offset to Special Lengths Table */
0x00,0x00,0x1A,0x9D,  /* Offset to Fake Msg Index */
0x00,0x00,0x00,0x00,  /* Offset to PCT table  */
0x00,0x00,0x00,0x00,  /* Offset to SDOIDs table */
/* currOffset = 0x6C */
//Device Table 
//DeviceID,  StatusVarID, 0,0,0,0,0,0,0
//IPSD, BOBID_IPSD_CMDSTATUSRESP iButton device
0x0A,  0x00,0x01,  0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/* currOffset = 0x76 */
//MsgControlTable 
//Flag(2),  ReplyTally(2),            MsgHeaderID(2), AppDataHeaderID(2), ReplyDataHeaderID(2), DeviceID, MsgCase, Toindex, Retries
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDTRANSPORTUNLOCK, APDIPSDTRANSPORTUNLOCK, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x04, NORETRIES  //IPSD_TRANSPORT_UNLOCK
0x4B,0x00, 0x00,0x02,                 0x00,0x00, 0x00,0x02, 0x00,0x00, 0x09, 0x00, 0x04, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDINITIALIZE, APDIPSDPACKETIZED, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x04, NORETRIES  //IPSD_INITIALIZE
0x4B,0x00, 0x00,0x02,                 0x00,0x01, 0x00,0x01, 0x00,0x00, 0x09, 0x00, 0x04, 0x02,
//RPLY_IPSD|VNONE     0x00,0x00,0x06, JHDIPSDGETSTATE, DUMMYDATA, RPDIPSDGETSTATE, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GET_STATE
0x0B,0x00, 0x00,0x06,                 0x00,0x02, 0x00,0x00, 0x00,0x01, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0x00,0x06, JHDIPSDGETRTC, DUMMYDATA, RPDIPSDGETRTC, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GET_RTC
0x0B,0x00, 0x00,0x06,                 0x00,0x23, 0x00,0x00, 0x00,0x0B, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDSETGMTOFFSET, APDIPSDSETGMTOFFSET, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_SET_GMT_OFFSET
0x4B,0x00, 0x00,0x02,                 0x00,0x1D, 0x00,0x07, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0x00,0x0B, JHDIPSDGETMODULESTATUS, DUMMYDATA, RPDIPSDREGISTERSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GET_MODULE_STATUS
0x0B,0x00, 0x00,0x0B,                 0x00,0x10, 0x00,0x00, 0x00,0x03, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0xFF,0xFF, JHDIPSDGETPSDPARAMS, DUMMYDATA, RPDIPSDPARAMLISTGLOB, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GET_PSD_PARAMS
0x0B,0x00, 0xFF,0xFF,                 0x00,0x1A, 0x00,0x00, 0x00,0x0F, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0x00,0x0A, JHDIPSDGETCHALLENGE, DUMMYDATA, RPDIPSDGETCHALLENGE, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GET_CHALLENGE
0x0B,0x00, 0x00,0x0A,                 0x00,0x11, 0x00,0x00, 0x00,0x04, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDUSERLOGIN, APDIPSDUSERLOGIN, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_USER_LOGIN
0x4B,0x00, 0x00,0x02,                 0x00,0x12, 0x00,0x04, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDPRECREATEINDI, APDIPSDPACKETIZED, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_PRECREATE_INDICIUM
0x4B,0x00, 0x00,0x02,                 0x00,0x1F, 0x00,0x01, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0xFF,0xFF, JHDIPSDCOMMITTRANSACTION, DUMMYDATA, RPDIPSDINDICIADATAGLOB, IPSD, 0x00, 0x03, NORETRIES  //IPSD_COMMIT_TRANSACTION
0x0B,0x00, 0xFF,0xFF,                 0x00,0x20, 0x00,0x00, 0x00,0x0E, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0xFF,0xFF, JHDIPSDCREATEINDICIUM, APDIPSDPACKETIZED, RPDIPSDINDICIADATAGLOB, IPSD, 0x00, 0x03, NORETRIES  //IPSD_CREATE_INDICIUM
0x4B,0x00, 0xFF,0xFF,                 0x00,0x0B, 0x00,0x01, 0x00,0x0E, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0xFF,0xFF, JHDIPSDGETFIRMWAREVERSION, DUMMYDATA, RPDIPSDGETFIRMWAREVERSION, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GET_FIRMWARE_VER
0x0B,0x00, 0xFF,0xFF,                 0x00,0x21, 0x00,0x00, 0x00,0x09, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0x00,0x0A, JHDIPSDGETSPEED, DUMMYDATA, RPDIPSDGETSPEED, IPSD, 0x00, 0x03, NORETRIES  //IPSD_SPEED
0x0B,0x00, 0x00,0x0A,                 0x00,0x1E, 0x00,0x00, 0x00,0x08, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0x00,0x04, JHDIPSDGETFREERAM, DUMMYDATA, RPDIPSDGETFREERAM, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GET_FREE_RAM
0x0B,0x00, 0x00,0x04,                 0x00,0x22, 0x00,0x00, 0x00,0x0A, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0x00,0x04, JHDIPSDGETPORCOUNT, DUMMYDATA, RPDIPSDGETPORCOUNT, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GET_POR_COUNT
0x0B,0x00, 0x00,0x04,                 0x00,0x25, 0x00,0x00, 0x00,0x0C, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDLDPOSTALCFGDATA, APDIPSDPACKETIZED, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_LOAD_POSTAL_CFG_DATA
0x4B,0x00, 0x00,0x02,                 0x00,0x05, 0x00,0x01, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDAUTHORIZE, APDIPSDPACKETIZED, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_AUTHORIZE
0x4B,0x00, 0x00,0x02,                 0x00,0x06, 0x00,0x01, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDMASTERERASE, APDIPSDPACKETIZED, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x04, NORETRIES  //IPSD_MASTER_ERASE
0x4B,0x00, 0x00,0x02,                 0x00,0x1C, 0x00,0x01, 0x00,0x00, 0x09, 0x00, 0x04, 0x02,
//RPLY_IPSD|VAPEND    0x00,0xFF,0xFF, JHDIPSDGETRANDOM, APDIPSDGETRANDOM, RPDIPSDGETRANDOM, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GET_RANDOM
0x4B,0x00, 0xFF,0xFF,                 0x00,0x24, 0x00,0x08, 0x00,0x0D, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAP_DIS   0x00,0xFF,0xFF, JHDIPSDGENPVDREQ, APDIPSDGENPVDREQ, RPIPSDREDIRECTED, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GENERATE_PVD_REQUEST
0xCB,0x00, 0xFF,0xFF,                 0x00,0x07, 0x00,0x03, 0x00,0x10, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAP_DIS   0x00,0x00,0x02, JHDIPSDPROCPVDMSG, APDIPSDPACKETIZED, RPDIPSDDATACENTERSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_PROCESS_PVD_MESSAGE
0xCB,0x00, 0x00,0x02,                 0x00,0x08, 0x00,0x01, 0x00,0x02, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|STAT_DIS  0x00,0xFF,0xFF, JHDIPSDGENPVRREQ, DUMMYDATA, RPIPSDREDIRECTED, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GENERATE_PVR_REQUEST
0x8B,0x00, 0xFF,0xFF,                 0x00,0x09, 0x00,0x00, 0x00,0x10, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAP_DIS   0x00,0x00,0x02, JHDIPSDPROCPVRMSG, APDIPSDPACKETIZED, RPDIPSDDATACENTERSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_PROCESS_PVR_MESSAGE
0xCB,0x00, 0x00,0x02,                 0x00,0x0A, 0x00,0x01, 0x00,0x02, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0xFF,0xFF, JHDIPSDCREATEDEVAUDITMSG, DUMMYDATA, RPIPSDREDIRECTED, IPSD, 0x00, 0x03, NORETRIES  //IPSD_CREATE_DEVICE_AUDIT_MSG
0x0B,0x00, 0xFF,0xFF,                 0x00,0x0D, 0x00,0x00, 0x00,0x10, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDPROCAUDITRESP, APDIPSDPACKETIZED, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_PROCESS_AUDIT_RESPONSE
0x4B,0x00, 0x00,0x02,                 0x00,0x0E, 0x00,0x01, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0xFF,0xFF, JHDIPSDGENKEYS, APDIPSDPACKETIZED, RPIPSDREDIRECTED, IPSD, 0x00, 0x04, NORETRIES  //IPSD_GENERATE_PSD_KEY
0x4B,0x00, 0xFF,0xFF,                 0x00,0x04, 0x00,0x01, 0x00,0x10, 0x09, 0x00, 0x04, 0x02,
//RPLY_IPSD|VAPEND    0x00,0xFF,0xFF, JHDIPSDLDSECRETKEY, APDIPSDPACKETIZED, RPIPSDREDIRECTED, IPSD, 0x00, 0x03, NORETRIES  //IPSD_LD_SECRET_KEY
0x4B,0x00, 0xFF,0xFF,                 0x00,0x03, 0x00,0x01, 0x00,0x10, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0x00,0xD2, JHDIPSDGETREFILLLOG, DUMMYDATA, RPDIPSDGETREFILLLOG, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GET_REFILL_LOG
0x0B,0x00, 0x00,0xD2,                 0x00,0x18, 0x00,0x00, 0x00,0x06, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0x00,0x02, JHDIPSDUSERLOGOUT, DUMMYDATA, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_USER_LOGOUT
0x0B,0x00, 0x00,0x02,                 0x00,0x13, 0x00,0x00, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDVERIFYHASHSIG, APDIPSDPACKETIZED, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_VERIFY_HASH_SIGNATURE
0x4B,0x00, 0x00,0x02,                 0x00,0x14, 0x00,0x01, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDLOADPROVIDERKEY, APDIPSDPACKETIZED, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_LOAD_PROVIDER_KEY
0x4B,0x00, 0x00,0x02,                 0x00,0x27, 0x00,0x01, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0x00,0x02, JHDIPSDPRECOMPUTER, DUMMYDATA, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_PRECOMPUTE_R
0x0B,0x00, 0x00,0x02,                 0x00,0x0C, 0x00,0x00, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDGERMINITIALIZE, APDIPSDPACKETIZED, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GERMAN_INITIALIZE
0x4B,0x00, 0x00,0x02,                 0x00,0x28, 0x00,0x01, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAP_DIS    0x00,0x00,0x02, JHDIPSDGERMPROCPVDMSG, APDIPSDPACKETIZED, RPDIPSDDATACENTERSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GERMAN_PROCESS_PVD_MESSAGE
0xCB,0x00, 0x00,0x02,                 0x00,0x29, 0x00,0x01, 0x00,0x02, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0xFF,0xFF, JHDIPSDGERMGETPSDPARAMS, DUMMYDATA, RPDIPSDPARAMLISTGLOB, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GERMAN_GET_PSD_PARAMS
0x0B,0x00, 0xFF,0xFF,                 0x00,0x2A, 0x00,0x00, 0x00,0x0F, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDGERMPRECREATEINDI, APDIPSDPACKETIZED, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GERMAN_PRECREATE_INDICIUM
0x4B,0x00, 0x00,0x02,                 0x00,0x2B, 0x00,0x01, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0xFF,0xFF, JHDIPSDGERMCREATEINDICIUM, APDIPSDPACKETIZED, RPDIPSDINDICIADATAGLOB, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GERMAN_CREATE_INDICIUM
0x4B,0x00, 0xFF,0xFF,                 0x00,0x2C, 0x00,0x01, 0x00,0x0E, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0xFF,0xFF, JHDIPSDGERMCREATEFINALIZINGFRANK, APDIPSDPRODPRICEVERSION, RPIPSDREDIRECTED, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GERMAN_CREATE_FINALIZING_FRANK
0x4B,0x00, 0xFF,0xFF,                 0x00,0x2D, 0x00,0x09, 0x00,0x10, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDENABLE, APDIPSDPBPXFERDATA, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_ENABLE
0x4B,0x00, 0x00,0x02,                 0x00,0x2E, 0x00,0x0A, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDDISABLE, APDIPSDPBPXFERDATA, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_DISABLE
0x4B,0x00, 0x00,0x02,                 0x00,0x2F, 0x00,0x0A, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0xFF,0xFF, JHDIPSDCREATEDEVAUDITWPCMSG, DUMMYDATA, RPIPSDREDIRECTED, IPSD, 0x00, 0x03, NORETRIES  //IPSD_CREATE_DEVICE_AUDIT_WPC_MSG
0x0B,0x00, 0xFF,0xFF,                 0x00,0x30, 0x00,0x00, 0x00,0x10, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDKEYPADREFILL, APDIPSDKEYPADREFILL, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_KEYPAD_REFILL
0x4B,0x00, 0x00,0x02,                 0x00,0x15, 0x00,0x05, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDKEYPADWITHDRAWAL, APDIPSDKEYPADWITHDRAWAL, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_KEYPAD_WITHDRAWAL
0x4B,0x00, 0x00,0x02,                 0x00,0x16, 0x00,0x06, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0xFF,0xFF, JHDIPSDCREATEINDICIUMWITHMSGTYPE, APDIPSDPACKETIZED, RPDIPSDINDICIADATAGLOB, IPSD, 0x00, 0x03, NORETRIES  //IPSD_CREATE_INDICIUM_WITH_MSG_TYPE
0x4B,0x00, 0xFF,0xFF,                 0x00,0x31, 0x00,0x01, 0x00,0x0E, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDPRECREATEINDICIUMWITHMSGTYPE, APDIPSDPACKETIZED, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_PRECREATE_INDICIUM_WITH_MSG_TYPE
0x4B,0x00, 0x00,0x02,                 0x00,0x32, 0x00,0x01, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0xFF,0xFF, JHDIPSDCREATEINDICIUMFLEXDEBIT, APDIPSDPACKETIZED, RPDIPSDFLEXDEBITOUTPUTGLOB, IPSD, 0x00, 0x03, NORETRIES  //IPSD_CREATE_INDICIUM_FLEX_DEBIT
0x4B,0x00, 0xFF,0xFF,                 0x00,0x33, 0x00,0x01, 0x00,0x11, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x02, JHDIPSDPRECREATEINDICIUMFLEXDEBIT, APDIPSDPACKETIZED, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_PRECREATE_INDICIUM_FLEX_DEBIT
0x4B,0x00, 0x00,0x02,                 0x00,0x34, 0x00,0x01, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0xFF,0xFF, JHDIPSDCOMMITTRANSACTION, DUMMYDATA, RPDIPSDFLEXDEBITOUTPUTGLOB, IPSD, 0x00, 0x03, NORETRIES  //IPSD_COMMIT_FLEX_DEBIT
0x0B,0x00, 0xFF,0xFF,                 0x00,0x20, 0x00,0x00, 0x00,0x11, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD|VNONE     0x00,0x00,0x17, JHDIPSDGETMODULESTATUSALL, DUMMYDATA, RPDIPSDGETREGISTERSTATUSALL, IPSD, 0x00, 0x03, NORETRIES  //IPSD_GET_MODULE_STATUS_ALL
0x0B,0x00, 0x00,0x17,                 0x00,0x35, 0x00,0x00, 0x00,0x12, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD_BL|VNONE  0x00,0x00,0x00, JHDIPSDBLGETDEVICESTATUS, DUMMYDATA, RPDIPSDBL_BL_GENERIC, IPSD_BL, 0x00, 0x03, NORETRIES  //IPSDBL_GET_DEVICE_STATUS
0x0C,0x00, 0x00,0x00,                 0x00,0x36, 0x00,0x00, 0x00,0x13, 0x0A, 0x00, 0x03, 0x02,
//RPLY_IPSD_BL|VAPEND 0x00,0x00,0x00, JHD_DUMMY, APDIPSDPBPXFERDATA, RPDIPSDBL_REDIRECTED, IPSD_BL, 0x00, 0x04, NORETRIES  //IPSDBL_PASSTHROUGH_MSG
0x4C,0x00, 0x00,0x00,                 0x00,0x37, 0x00,0x0A, 0x00,0x14, 0x0A, 0x00, 0x04, 0x02,
//RPLY_IPSD|VNONE     0x00,0x00,0x00, JHDIPSDRUNSELFTESTS, DUMMYDATA, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, NORETRIES  //IPSD_RUN_SELF_TESTS
0x0B,0x00, 0x00,0x00,                 0x00,0x0F, 0x00,0x00, 0x00,0x00, 0x09, 0x00, 0x03, 0x02,
//RPLY_IPSD_BL|VNONE  0x00,0x00,0x00, JHDIPSDBLEXITAPPMODE, DUMMYDATA, RPDIPSDBL_BL_GENERIC, IPSD_BL, 0x00, 0x02, NORETRIES  //IPSDBL_EXIT_APPMODE
0x0C,0x00, 0x00,0x00,                 0x00,0x38, 0x00,0x00, 0x00,0x13, 0x0A, 0x00, 0x02, 0x02,
//RPLY_IPSD|VAPEND    0x00,0x00,0x00, JHDIPSDMANUFACTURE, APDIPSDPACKETIZED, RPDIPSD_JUSTSTATUS, IPSD, 0x00, 0x03, STDTRIES  //IPSD_MANUFACTURE
0x4B,0x00, 0x00,0x00,                 0x00,0x39, 0x00,0x01, 0x00,0x00, 0x09, 0x00, 0x03, 0x00,

/* currOffset = 0x378 */
/*
WhenAmI - Date/Time Dependence Table
NOTE: This table is not sequence dependent.  The message ID (first 2 columns) is used to lookup the appropriate record.

Msg ID     Dest ID    Source ID  Exception case*/
//IPSD_PRECREATE_INDICIUM, BOBID_IPSD_MAILINGDATE, JDATAMAILDATE, NO_EXCEPTION
0x00,0x09, 0x00,0x48, 0x00,0x7B, 0x00, /* IPSD_PRECREATE_INDICIUM */
//IPSD_CREATE_INDICIUM, BOBID_IPSD_MAILINGDATE, JDATAMAILDATE, NO_EXCEPTION
0x00,0x0B, 0x00,0x48, 0x00,0x7B, 0x00, /* IPSD_CREATE_INDICIUM */
//IPSD_GERMAN_PRECREATE_INDICIUM, BOBID_IPSD_MAILINGDATE, JDATAMAILDATE, NO_EXCEPTION
0x00,0x24, 0x00,0x48, 0x00,0x7B, 0x00, /* IPSD_GERMAN_PRECREATE_INDICIUM */
//IPSD_GERMAN_CREATE_INDICIUM, BOBID_IPSD_MAILINGDATE, JDATAMAILDATE, NO_EXCEPTION
0x00,0x25, 0x00,0x48, 0x00,0x7B, 0x00, /* IPSD_GERMAN_CREATE_INDICIUM */
//IPSD_CREATE_INDICIUM_WITH_MSG_TYPE, BOBID_IPSD_MAILINGDATE, JDATAMAILDATE, NO_EXCEPTION
0x00,0x2C, 0x00,0x48, 0x00,0x7B, 0x00, /* IPSD_CREATE_INDICIUM_WITH_MSG_TYPE */
//IPSD_PRECREATE_INDICIUM_WITH_MSG_TYPE, BOBID_IPSD_MAILINGDATE, JDATAMAILDATE, NO_EXCEPTION
0x00,0x2D, 0x00,0x48, 0x00,0x7B, 0x00, /* IPSD_PRECREATE_INDICIUM_WITH_MSG_TYPE */
//DEVICE_MSG_CNT, UPDATETEST, JDATAMAILDATE, NO_EXCEPTION
0x00,0x37, 0x00,0x59, 0x00,0x7B, 0x00, /* DEVICE_MSG_CNT */

/* currOffset = 0x3A9 */
/*
xCeptAMI - Date/Time Exception Table
NOTE: This table is not sequence dependent.  The message ID (first 2 columns) is used to lookup the appropriate record.

Msg ID     Dest ID    Source ID  Exception case*/

/* currOffset = 0x3A9 */
/*
MsgSysDates - System Date/Time Reference
NOTE: This table is not sequence dependent.  All records are used to complete a system time/date field update operation.

Dest ID    Source ID*/
//JDATACURRENTTIME_CENTURYYY, MSGSYSTIME_BCENTURY
0x00,0x63, 0x00,0x5A,
//JDATACURRENTTIME_YEARYY, MSGSYSTIME_BYEAR
0x00,0x64, 0x00,0x5B,
//JDATACURRENTTIME_MONTHMM, MSGSYSTIME_BMONTH
0x00,0x65, 0x00,0x5C,
//JDATACURRENTTIME_DAYDD, MSGSYSTIME_BDAY
0x00,0x66, 0x00,0x5D,
//JDATACURRENTTIME_HOURHH, MSGSYSTIME_BHOUR
0x00,0x67, 0x00,0x5E,
//JDATACURRENTTIME_MINUTEMM, MSGSYSTIME_BMINUTES
0x00,0x68, 0x00,0x5F,
//JDATACURRENTDATE_CENTURYYY, MSGSYSTIME_BCENTURY
0x00,0x69, 0x00,0x5A,
//JDATACURRENTDATE_YEARYY, MSGSYSTIME_BYEAR
0x00,0x6A, 0x00,0x5B,
//JDATACURRENTDATE_MONTHMM, MSGSYSTIME_BMONTH
0x00,0x6B, 0x00,0x5C,
//JDATACURRENTDATE_DAYDD, MSGSYSTIME_BDAY
0x00,0x6C, 0x00,0x5D,
//JDATACURRENTMONTH_YEARYY, MSGSYSTIME_BYEAR
0x00,0x6D, 0x00,0x5B,
//JDATACURRENTMONTH_MONTHMM, MSGSYSTIME_BMONTH
0x00,0x6E, 0x00,0x5C,

/* currOffset = 0x3D9 */
/*
MsgPrintDates - System Date/Time Reference
NOTE: This table is not sequence dependent.  All records are used to complete a printed time/date field update operation.

Dest ID    Source ID*/
//JDATAPRINTEDDATE_CENTURYYY, MSGPRINTTIME_BCENTURY
0x00,0x75, 0x00,0x6F,
//JDATAPRINTEDDATE_YEARYY, MSGPRINTTIME_BYEAR
0x00,0x76, 0x00,0x70,
//JDATAPRINTEDDATE_MONTHMM, MSGPRINTTIME_BMONTH
0x00,0x77, 0x00,0x71,
//JDATAPRINTEDDATE_DAYDD, MSGPRINTTIME_BDAY
0x00,0x78, 0x00,0x72,
//JDATACURRENTMONTH_YEARYY, MSGPRINTTIME_BYEAR
0x00,0x6D, 0x00,0x70,
//JDATAPRINTEDMONTH_MONTHMM, MSGPRINTTIME_BMONTH
0x00,0x7A, 0x00,0x71,

/* currOffset = 0x3F1 */
/*
TimeOutList - Timeout Reference
Must be in the same sequnce as the timeout indexes used in the Message Control Table.

Timeout in MS*/
0x00, 0x00, 0x01, 0xF4,  /* 500 */
0x00, 0x00, 0x03, 0xE8,  /* 1000 */
0x00, 0x00, 0x07, 0xD0,  /* 2000 */
0x00, 0x00, 0x13, 0x88,  /* 5000 */
0x00, 0x00, 0x27, 0x10,  /* 10000 */
0x00, 0x00, 0xEA, 0x60,  /* 60000 */
0x00, 0x01, 0x86, 0xA2,  /* 100002 */

/* currOffset = 0x40D */
/*
Special Lengths Table
NOTE: Not sequence dependent.  If a special length is required for a message, the special code, LEN_XCEPTION_LIST will be 
inserted in the size column of the reply data definition for that message.  This table is than scanned for the record associated 
with the message's ID (column 1 and 2).  Column 3 and 4 contain the ID of the function that is to be used to determine the length 
of the reply.

The table is delimted by a zero record.

Msg ID     Function ID*/
//IPSD_TRANSPORT_UNLOCK, fnSTDPREMSGTASK
0x00,0x00, 0x00,0x00,
0x00,0x00, 0x00,0x00,

/* currOffset = 0x415 */
/*
Header data Index
*/
0x00,0x00,0x04,0xFD,
0x00,0x00,0x05,0x03,
0x00,0x00,0x05,0x06,
0x00,0x00,0x05,0x0C,
0x00,0x00,0x05,0x0F,
0x00,0x00,0x05,0x12,
0x00,0x00,0x05,0x15,
0x00,0x00,0x05,0x18,
0x00,0x00,0x05,0x1E,
0x00,0x00,0x05,0x21,
0x00,0x00,0x05,0x27,
0x00,0x00,0x05,0x2A,
0x00,0x00,0x05,0x2D,
0x00,0x00,0x05,0x33,
0x00,0x00,0x05,0x39,
0x00,0x00,0x05,0x3C,
0x00,0x00,0x05,0x42,
0x00,0x00,0x05,0x48,
0x00,0x00,0x05,0x4E,
0x00,0x00,0x05,0x54,
0x00,0x00,0x05,0x5A,
0x00,0x00,0x05,0x5D,
0x00,0x00,0x05,0x63,
0x00,0x00,0x05,0x69,
0x00,0x00,0x05,0x6F,
0x00,0x00,0x05,0x75,
0x00,0x00,0x05,0x7B,
0x00,0x00,0x05,0x81,
0x00,0x00,0x05,0x87,
0x00,0x00,0x05,0x8A,
0x00,0x00,0x05,0x90,
0x00,0x00,0x05,0x96,
0x00,0x00,0x05,0x99,
0x00,0x00,0x05,0x9F,
0x00,0x00,0x05,0xA5,
0x00,0x00,0x05,0xAB,
0x00,0x00,0x05,0xB1,
0x00,0x00,0x05,0xB7,
0x00,0x00,0x05,0xBD,
0x00,0x00,0x05,0xC0,
0x00,0x00,0x05,0xC3,
0x00,0x00,0x05,0xC6,
0x00,0x00,0x05,0xC9,
0x00,0x00,0x05,0xCF,
0x00,0x00,0x05,0xD2,
0x00,0x00,0x05,0xD5,
0x00,0x00,0x05,0xDB,
0x00,0x00,0x05,0xE1,
0x00,0x00,0x05,0xE7,
0x00,0x00,0x05,0xED,
0x00,0x00,0x05,0xF0,
0x00,0x00,0x05,0xF3,
0x00,0x00,0x05,0xF6,
0x00,0x00,0x05,0xF9,
0x00,0x00,0x05,0xFF,
0x00,0x00,0x06,0x05,
0x00,0x00,0x06,0x06,
0x00,0x00,0x06,0x0C,
/* currOffset = 0x4FD */
/*
Header DEFINITIONS
These are the constant portion, the header, of each device message.
Column 1 specifies the number of bytes in the header and the remaining
columns provide the constant values to be inserted in the message.

        Headsiz         Header.....
*/
/* JHDIPSDTRANSPORTUNLOCK */    0x05, 0x80, 0x01, 0x00, 0x00, 0x08, /* IPSD_TRANSPORT_UNLOCK */
/* JHDIPSDINITIALIZE */         0x02, 0x80, 0x02, /* IPSD_INITIALIZE */
/* JHDIPSDGETSTATE */           0x05, 0x80, 0x03, 0x00, 0x00, 0x00, /* IPSD_GET_STATE */
/* JHDIPSDLDSECRETKEY */        0x02, 0x80, 0x04, /* IPSD_LD_SECRET_EXCHG_KEY */
/* JHDIPSDGENKEYS */            0x02, 0x80, 0x06, /* IPSD_GENERATE_KEYS */
/* JHDIPSDLDPOSTALCFGDATA */    0x02, 0x80, 0x07, /* IPSD_LD_POSTAL_CFG_DATA */
/* JHDIPSDAUTHORIZE */          0x02, 0x80, 0x08, /* IPSD_AUTHORIZE */
/* JHDIPSDGENPVDREQ */          0x05, 0x80, 0x09, 0x00, 0x00, 0x04, /* IPSD_GEN_PVD_REQ */
/* JHDIPSDPROCPVDMSG */         0x02, 0x80, 0x0A, /* IPSD_PROC_PVD_MSG */
/* JHDIPSDGENPVRREQ */          0x05, 0x80, 0x0B, 0x00, 0x00, 0x00, /* IPSD_GEN_PVR_REQ */
/* JHDIPSDPROCPVRMSG */         0x02, 0x80, 0x0C, /* IPSD_PROC_PVR_MSG */
/* JHDIPSDCREATEINDICIUM */     0x02, 0x80, 0x0D, /* IPSD_CREATE_INDICIUM */
/* JHDIPSDPRECOMPUTER */        0x05, 0x80, 0x0E, 0x00, 0x00, 0x00, /* IPSD_PRECOMPUTE_R */
/* JHDIPSDCREATEDEVAUDITMSG */  0x05, 0x80, 0x0F, 0x00, 0x00, 0x00, /* IPSD_CREATE_AUDIT_MSG */
/* JHDIPSDPROCAUDITRESP */      0x02, 0x80, 0x10, /* IPSD_PROCESS_AUDIT_RESP */
/* JHDIPSDRUNSELFTESTS */       0x05, 0x80, 0x11, 0x00, 0x00, 0x00, /* IPSD_RUN_SELF_TESTS */
/* JHDIPSDGETMODULESTATUS */    0x05, 0x80, 0x12, 0x00, 0x00, 0x00, /* IPSD_GET_MODULE_STATUS */
/* JHDIPSDGETCHALLENGE */       0x05, 0x80, 0x13, 0x00, 0x00, 0x00, /* IPSD_GET_CHALLENGE */
/* JHDIPSDUSERLOGIN */          0x05, 0x80, 0x14, 0x00, 0x00, 0x14, /* IPSD_USER_LOGIN */
/* JHDIPSDUSERLOGOUT */         0x05, 0x80, 0x15, 0x00, 0x00, 0x00, /* IPSD_USER_LOGOUT */
/* JHDIPSDVERIFYHASHSIG */      0x02, 0x80, 0x16, /* IPSD_VERIFY_HASH_SIG */
/* JHDIPSDKEYPADREFILL */       0x05, 0x80, 0x17, 0x00, 0x00, 0x0E, /* IPSD_KEYPAD_REFILL */
/* JHDIPSDKEYPADWITHDRAWAL */   0x05, 0x80, 0x18, 0x00, 0x00, 0x0A, /* IPSD_KEYPAD_WITHDRAWAL */
/* JHDIPSDGETINDCREATELOG */    0x05, 0x80, 0x19, 0x00, 0x01, 0x00, /* IPSD_GET_INDICIA_CREATE_LOG */
/* JHDIPSDGETREFILLLOG */       0x05, 0x80, 0x19, 0x00, 0x02, 0x00, /* IPSD_GET_REFILL_LOG */
/* JHDIPSDGETAUDITLOG */        0x05, 0x80, 0x19, 0x00, 0x03, 0x00, /* IPSD_GET_AUDIT_LOG */
/* JHDIPSDGETPSDPARAMS */       0x05, 0x80, 0x1A, 0x00, 0x00, 0x00, /* IPSD_GET_PSD_PARAMS */
/* JHDIPSDGETPSDKEYDATA */      0x05, 0x80, 0x1B, 0x00, 0x00, 0x00, /* IPSD_GET_PSD_KEY_DATA */
/* JHDIPSDMASTERERASE */        0x02, 0x80, 0x1C, /* IPSD_MASTER_ERASE */
/* JHDIPSDSETGMTOFFSET */       0x05, 0x80, 0x1D, 0x00, 0x00, 0x04, /* IPSD_SET_GMT_OFFSET */
/* JHDIPSDGETSPEED */           0x05, 0x80, 0x1E, 0x02, 0x00, 0x00, /* IPSD_GET_SPEED */
/* JHDIPSDPRECREATEINDI */      0x02, 0x80, 0x21, /* IPSD_PRECREATE_INDICIUM */
/* JHDIPSDCOMMITTRANSACTION */  0x05, 0x80, 0x22, 0x00, 0x00, 0x00, /* IPSD_COMMIT_TRANSACTION */
/* JHDIPSDGETFIRMWAREVERSION */ 0x05, 0xD0, 0x95, 0x01, 0x00, 0x00, /* IPSD_GET_FIRMWARE_VER */
/* JHDIPSDGETFREERAM */         0x05, 0xD0, 0x95, 0x01, 0x01, 0x00, /* IPSD_GET_FREE_RAM */
/* JHDIPSDGETRTC */             0x05, 0xD0, 0x95, 0x01, 0x0C, 0x00, /* IPSD_GET_RTC */
/* JHDIPSDGETRANDOM */          0x05, 0xD0, 0x95, 0x01, 0x0D, 0x02, /* IPSD_GET_RANDOM */
/* JHDIPSDGETPORCOUNT */        0x05, 0xD0, 0x95, 0x02, 0x00, 0x00, /* IPSD_GET_POR_COUNT */
/* JHDIPSDRUNALGORITHM */       0x02, 0x80, 0x20, /* IPSD_RUN_ALGORITHM */
/* JHDIPSDLOADPROVIDERKEY */    0x02, 0x80, 0x29, /* IPSD_LOAD_PROVIDER_KEY */
/* JHDIPSDGERMINITIALIZE */     0x02, 0x80, 0x23, /* IPSD_GERMAN_INITIALIZE */
/* JHDIPSDGERMPROCPVDMSG */     0x02, 0x80, 0x24, /* IPSD_GERMAN_PROCESS_PVD_MESSAGE */
/* JHDIPSDGERMGETPSDPARAMS */   0x05, 0x80, 0x25, 0x00, 0x00, 0x00, /* IPSD_GERMAN_GET_PSD_PARAMS */
/* JHDIPSDGERMPRECREATEINDI */  0x02, 0x80, 0x26, /* IPSD_GERMAN_PRECREATE_INDICIUM */
/* JHDIPSDGERMCREATEINDICIUM */ 0x02, 0x80, 0x27, /* IPSD_GERMAN_CREATE_INDICIUM */
/* JHDIPSDGERMCREATEFINALIZINGFRANK */  0x05, 0x80, 0x28, 0x00, 0x00, 0x01, /* IPSD_GERMAN_CREATE_FINALIZING_FRANK */
/* JHDIPSDENABLE */             0x05, 0x80, 0x2D, 0x00, 0x00, 0x5B, /* IPSD_ENABLE */
/* JHDIPSDDISABLE */            0x05, 0x80, 0x2C, 0x00, 0x00, 0x5B, /* IPSD_DISABLE */
/* JHDIPSDCREATEDEVAUDITWPCMSG */  0x05, 0x80, 0x0F, 0x00, 0x01, 0x00, /* IPSD_CREATE_AUDIT_WPC_MSG */
/* JHDIPSDCREATEINDICIUMWITHMSGTYPE */  0x02, 0x80, 0x2E, /* IPSD_CREATE_INDICIUM_WITH_MSG_TYPE */
/* JHDIPSDPRECREATEINDICIUMWITHMSGTYPE */  0x02, 0x80, 0x2F, /* IPSD_PRECREATE_INDICIUM_WITH_MSG_TYPE */
/* JHDIPSDCREATEINDICIUMFLEXDEBIT */  0x02, 0x80, 0x30, /* IPSD_CREATE_INDICIUM_FLEX_DEBIT */
/* JHDIPSDPRECREATEINDICIUMFLEXDEBIT */  0x02, 0x80, 0x31, /* IPSD_PRECREATE_INDICIUM_FLEX_DEBIT */
/* JHDIPSDGETMODULESTATUSALL */ 0x05, 0x80, 0x12, 0x00, 0x01, 0x00, /* IPSD_GET_MODULE_STATUS_ALL */
/* JHDIPSDBLGETDEVICESTATUS */  0x05, 0x80, 0x00, 0x00, 0x00, 0x00, /* IPSDBL_GET_DEVICE_STATUS */
/* JHD_DUMMY */                 0x00, /* Empty Header, when header is included in "data". */
/* JHDIPSDBLEXITAPPMODE */      0x05, 0x20, 0x00, 0x00, 0x00, 0x00, /* IPSDBL_EXIT_APPMODE */
/* JHDIPSDMANUFACTURE */        0x02, 0x80, 0x1F, /* IPSD_MANUFACTURE */

/* currOffset = 0x60F */
/*
Appended data Index
*/
0x00,0x00,0x06,0x3B,
0x00,0x00,0x06,0x3C,
0x00,0x00,0x06,0x4D,
0x00,0x00,0x06,0x52,
0x00,0x00,0x06,0x57,
0x00,0x00,0x06,0x5C,
0x00,0x00,0x06,0x65,
0x00,0x00,0x06,0x6A,
0x00,0x00,0x06,0x6F,
0x00,0x00,0x06,0x74,
0x00,0x00,0x06,0x79,
/* currOffset = 0x63B */
/*
APPENDED DATA DEFINITIONS
Appended Data Definitions for PHC and PSD messages

        NAME  COUNT,  VAR ID, SIZE TO COPY or EXCPT,
                      VAR ID, SIZE TO COPY or EXCPT,  ** Message Id  */
/* DUMMYDATA */                 0x00,   /* Used Many Places */
/* APDIPSDPACKETIZED */         0x04, 
                              // BOBID_IPSD_PACKETCODE, 0x00,0x01,
                                0x00,0x05, 0x00,0x01,
                              // BOBID_IPSD_P2, 0x00,0x01,
                                0x00,0x06, 0x00,0x01,
                              // BOBID_IPSD_PACKETSIZE, 0x00,0x01,
                                0x00,0x07, 0x00,0x01,
                              // BOBID_IPSD_PACKETDATA, IPSD_PACKET_LEN
                                0x00,0x08, 0xFF,0x0D,  /* IPSD_PACKET_DATA */
/* APDIPSDTRANSPORTUNLOCK */    0x01, 
                              // BOBID_IPSD_ENCRYPTEDNONCE, 0x00,0x08,
                                0x00,0x0B, 0x00,0x08,  /* IPSD_TRANSPORT_UNLOCK */
/* APDIPSDGENPVDREQ */          0x01, 
                              // BOBID_IPSD_POSTAGEREQUESTED, 0x00,0x04,
                                0x00,0x27, 0x00,0x04,  /* IPSD_GEN_PVD_REQ */
/* APDIPSDUSERLOGIN */          0x01, 
                              // BOBID_IPSD_LOGINDATA, 0x00,0x14,
                                0x00,0x0D, 0x00,0x14,  /* IPSD_USER_LOGIN */
/* APDIPSDKEYPADREFILL */       0x02, 
                              // BOBID_IPSD_POSTAGEREQUESTED, 0x00,0x04,
                                0x00,0x27, 0x00,0x04,
                              // BOBID_IPSD_KEYPADREFILLCOMBODATA, 0x00,0x0A,
                                0x00,0x25, 0x00,0x0A,  /* IPSD_KEYPAD_REFILL */
/* APDIPSDKEYPADWITHDRAWAL */   0x01, 
                              // BOBID_IPSD_KEYPADWITHDRAWALCOMBODATA, 0x00,0x0A,
                                0x00,0x26, 0x00,0x0A,  /* IPSD_KEYPAD_WITHDRAWAL */
/* APDIPSDSETGMTOFFSET */       0x01, 
                              // BOBID_IPSD_GMTOFFSET, 0x00,0x04,
                                0x00,0x2D, 0x00,0x04,  /* IPSD_SET_GMT_OFFSET */
/* APDIPSDGETRANDOM */          0x01, 
                              // BOBID_IPSD_RANDOMSZ, 0x00,0x01,
                                0x00,0x3E, 0x00,0x01,  /* IPSD_GET_RANDOM */
/* APDIPSDPRODPRICEVERSION */   0x01, 
                              // BOBID_IPSD_PRODPRICE_VERSION, 0x00,0x01,
                                0x00,0xCB, 0x00,0x01,  /* IPSD_GERMAN_CREATE_FINALIZING_FRANK */
/* APDIPSDPBPXFERDATA */        0x01, 
                              // RPLOCALXFERCTRLSTRUCT, REDIRECTED_DATA
                                0x00,0x82, 0xFF,0x03,  /* all xfer struc data from pbptask */

/* currOffset = 0x67E */
/*
Reply data Index
*/
0x00,0x00,0x06,0xD2,
0x00,0x00,0x06,0xD7,
0x00,0x00,0x06,0xE0,
0x00,0x00,0x06,0xE9,
0x00,0x00,0x06,0xF6,
0x00,0x00,0x06,0xFF,
0x00,0x00,0x07,0x08,
0x00,0x00,0x07,0x11,
0x00,0x00,0x07,0x1A,
0x00,0x00,0x07,0x23,
0x00,0x00,0x07,0x2C,
0x00,0x00,0x07,0x35,
0x00,0x00,0x07,0x3E,
0x00,0x00,0x07,0x47,
0x00,0x00,0x07,0x50,
0x00,0x00,0x07,0x5D,
0x00,0x00,0x07,0x6A,
0x00,0x00,0x07,0x6F,
0x00,0x00,0x07,0x7C,
0x00,0x00,0x07,0x95,
0x00,0x00,0x07,0xA6,
/* currOffset = 0x6D2 */
/*
REPLY DATA DEFINITIONS
Reply Data Definitions for PHC and PSD messages

        NAME  COUNT,  VAR ID, SIZE TO COPY or EXCPT,
                      VAR ID, SIZE TO COPY or EXCPT,  ** Message Id  */
/* RPDIPSD_JUSTSTATUS */        0x01, 
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDGETSTATE */           0x02, 
                              // BOBID_IPSD_STATE, 0x00,0x04,
                                0x00,0x02, 0x00,0x04,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDDATACENTERSTATUS */   0x02, 
                              // BOBID_IPSD_DCSTATUS, 0x00,0x04,
                                0x00,0x04, 0x00,0x04,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDREGISTERSTATUS */     0x03, 
                              // BOBID_IPSD_ASCREG, 0x00,0x05,
                                0x00,0x20, 0x00,0x05,
                              // BOBID_IPSD_DESCREG, 0x00,0x04,
                                0x00,0x21, 0x00,0x04,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDGETCHALLENGE */       0x02, 
                              // BOBID_IPSD_NONCE, 0x00,0x08,
                                0x00,0x0A, 0x00,0x08,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /* IPSD_GET_CHALLENGE */
/* RPDIPSDGETINDCREATELOG */    0x02, 
                              // BOBID_IPSD_INDCREATELOG, 0x02,0x8A,
                                0x00,0x37, 0x02,0x8A,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDGETREFILLLOG */       0x02, 
                              // BOBID_IPSD_REFILLLOG, 0x00,0xD2,
                                0x00,0x38, 0x00,0xD2,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDGETAUDITLOG */        0x02, 
                              // BOBID_IPSD_AUDITLOG, 0x00,0x54,
                                0x00,0x39, 0x00,0x54,
                              // BOBID_IPSD_STATE, 0x00,0x04,
                                0x00,0x02, 0x00,0x04,  /*  */
/* RPDIPSDGETSPEED */           0x02, 
                              // BOBID_IPSD_SPEED, 0x00,0x08,
                                0x00,0x3A, 0x00,0x08,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDGETFIRMWAREVERSION */ 0x02, 
                              // BOBID_IPSD_FIRMWAREVERSTR, IPSD_GLOB_LEN
                                0x00,0x16, 0xFF,0x0E,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDGETFREERAM */         0x02, 
                              // BOBID_IPSD_FREERAM, 0x00,0x02,
                                0x00,0x3B, 0x00,0x02,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDGETRTC */             0x02, 
                              // BOBID_IPSD_RTC, 0x00,0x04,
                                0x00,0x2F, 0x00,0x04,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDGETPORCOUNT */        0x02, 
                              // BOBID_IPSD_PORCOUNT, 0x00,0x02,
                                0x00,0x3C, 0x00,0x02,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDGETRANDOM */          0x02, 
                              // BOBID_IPSD_RANDOM, IPSD_GLOB_LEN
                                0x00,0x3D, 0xFF,0x0E,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDINDICIADATAGLOB */    0x03, 
                              // BOBID_IPSD_INDICIADATAGLOB, IPSD_GLOB_LEN
                                0x00,0x1F, 0xFF,0x0E,
                              // BOBID_IPSD_DEBIT_OUTPUT_GLOB_SIZE, IPSD_GLOB_LEN_LEN
                                0x00,0xFD, 0xFF,0x10,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDPARAMLISTGLOB */      0x03, 
                              // BOBID_IPSD_PARAMLISTGLOB, IPSD_GLOB_LEN
                                0x00,0x47, 0xFF,0x0E,
                              // BOBID_IPSD_PARAMLISTGLOB_SIZE, IPSD_GLOB_LEN_LEN
                                0x00,0xFB, 0xFF,0x10,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /* IPSD_GET_PSD_PARAMS */
/* RPIPSDREDIRECTED */          0x01, 
                              // RPLOCALXFERCTRLSTRUCT, REDIRECTED_DATA_IPSD
                                0x00,0x82, 0xFF,0x0F,  /* redirect ipsd reply to xfercntrlstruct */
/* RPDIPSDFLEXDEBITOUTPUTGLOB */0x03, 
                              // BOBID_IPSD_FLEXDEBIT_OUTPUT_GLOB, IPSD_GLOB_LEN
                                0x00,0xEF, 0xFF,0x0E,
                              // BOBID_IPSD_FLEXDEBIT_OUTPUT_GLOB_SIZE, IPSD_GLOB_LEN_LEN
                                0x00,0xFC, 0xFF,0x10,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDGETREGISTERSTATUSALL */  0x06, 
                              // BOBID_IPSD_ASCREG, 0x00,0x05,
                                0x00,0x20, 0x00,0x05,
                              // BOBID_IPSD_DESCREG, 0x00,0x04,
                                0x00,0x21, 0x00,0x04,
                              // BOBID_IPSD_PIECECOUNT, 0x00,0x04,
                                0x00,0x22, 0x00,0x04,
                              // BOBID_IPSD_ZEROPOSTAGEPIECECOUNT, 0x00,0x04,
                                0x00,0xE1, 0x00,0x04,
                              // BOBID_IPSD_RTC_LOCAL_DATE, 0x00,0x04,
                                0x00,0xF9, 0x00,0x04,
                              // BOBID_IPSD_CMDSTATUSRESP, 0x00,0x02,
                                0x00,0x01, 0x00,0x02,  /*  */
/* RPDIPSDBL_BL_GENERIC */      0x04, 
                              // BOBID_IPSD_BL_REPLYRAW, LEX_SIMPLE_COPY
                                0x00,0xFE, 0xFF,0x11,
                              // BOBID_IPSD_BL_REPLYSIZE, IPSD_GLOB_LEN_LEN
                                0x00,0xFF, 0xFF,0x10,
                              // BOBID_IPSD_BL_RESPHDR, 0x00,0x04,
                                0x01,0x00, 0x00,0x04,
                              // BOBID_IPSD_BL_RESPSTATUS, 0x00,0x01,
                                0x01,0x01, 0x00,0x01,  /*  */
/* RPDIPSDBL_REDIRECTED */      0x04, 
                              // BOBID_IPSD_BL_RESPHDR, 0x00,0x04,
                                0x01,0x00, 0x00,0x04,
                              // BOBID_IPSD_BL_RESPSTATUS, 0x00,0x01,
                                0x01,0x01, 0x00,0x01,
                              // BOBID_IPSD_BL_HDR_REMAINDER, 0x00,0x03,
                                0x01,0x03, 0x00,0x03,
                              // BOBID_IPSD_BL_PASSTHROUGH_MSG, LEX_REDIRECTED_IPSD_BL
                                0x01,0x04, 0xFF,0x12,  /*  */

/* currOffset = 0x7B7 */
/*
Data Mat Index
*/
0x00,0x00,0x07,0xE1,  0x00,0x91,
0x00,0x00,0x0B,0x47,  0x00,0x00,
0x00,0x00,0x0B,0x47,  0x00,0x00,
0x00,0x00,0x0B,0x47,  0x00,0x00,
0x00,0x00,0x0B,0x47,  0x00,0x00,
0x00,0x00,0x0B,0x47,  0x00,0x00,
0x00,0x00,0x0B,0x47,  0x00,0x20,
/* currOffset = 0x7E1 */
/*
bobaMat0
VARIABLE ID     VARIANCE    CONTROL ID (MSG)    CONVERSION
---------------------------------------------------------- */

//** GET_VLT_DESCENDING_REG **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_REGS, MONEY_TO_STRING
0x00,0x50, 0x0E, 0x00,0x0E, 0x03,
//** GET_VLT_ASCENDING_REG **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_REGS, MONEY_TO_STRING
0x00,0x51, 0x0E, 0x00,0x0E, 0x03,
//** UIC_PRINT_MODE **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x56, 0x06, 0x00,0x37, 0x08,
//** VAULT_DEBIT_VALUE **, UIC_PARAM, DEVICE_MSG_CNT, SLONG_TO_STRING
0x00,0x49, 0x06, 0x00,0x37, 0x05,
//** GET_VLT_CONTROL_SUM **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_REGS, MONEY_TO_STRING
0x00,0x4D, 0x0E, 0x00,0x0E, 0x03,
//** READ_CURRENT_INDI_SELECTION **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x57, 0x06, 0x00,0x37, 0x08,
//** SET_CURRENT_INDI_SELECTION **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x58, 0x06, 0x00,0x37, 0x08,
//** VLT_PURSE_MAX_DESC_REG **, STATIK_IPSD, DEVICE_MSG_CNT, MONEY_TO_STRING
0x00,0x4E, 0x15, 0x00,0x37, 0x03,
//** VLT_PURSE_DECIMALS **, IPSD_FLASH_PARM, IBYTE_PARAM, HEX_TO_STRING
0x00,0x4F, 0x17, 0x00,0x08, 0x00,
//** VAULT_REFILL_AMT **, UIC_PARAM, DEVICE_MSG_CNT, MONEY4_TO_STRING
0x00,0x4C, 0x06, 0x00,0x37, 0x0D,
//** GET_VLT_PIECE_COUNTER **, SCRIPTED, SCRIPT_GET_PIECE_COUNT, LE_ULONG_TO_STRING
0x00,0x4B, 0x07, 0x00,0x60, 0x10,
//** BOBID_IPSD_FIRMWAREVERSTR **, STATIK_IPSD, DEVICE_MSG_CNT, STRING_NEEDS_NULL
0x00,0x16, 0x15, 0x00,0x37, 0x06,
//** BOBID_IPSD_FIRMWAREVERSUBSTR **, STATIK_IPSD, DEVICE_MSG_CNT, STRING_NEEDS_NULL
0x00,0x7C, 0x15, 0x00,0x37, 0x06,
//** VLT_PURSE_SETTABLE_DIGITS **, IPSD_FLASH_PARM, IBYTE_PARAM, DECIMAL_TO_STRING
0x00,0x4A, 0x17, 0x00,0x08, 0x01,
//** READ_CURRENT_AD_SELECTION **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x7D, 0x06, 0x00,0x37, 0x08,
//** SET_CURRENT_AD_SELECTION **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x7E, 0x06, 0x00,0x37, 0x08,
//** READ_CURRENT_INSCR_SELECT **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x7F, 0x06, 0x00,0x37, 0x08,
//** SET_CURRENT_INSCR_SELECT **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x80, 0x06, 0x00,0x37, 0x08,
//** MFG_METER_STATUS **, SCRIPTED, COMPILE_METER_STATUS, NO_FORMAT_CHANGE
0x00,0x81, 0x07, 0x00,0x12, 0x08,
//** GET_PSD_CHALLENGE **, ALWAYS_PSD, IPSD_GET_CHALLENGE, NO_FORMAT_CHANGE
0x00,0x84, 0x14, 0x00,0x07, 0x08,
//** SET_PSD_XPRT_AUTH **, FOLLOW_UP_MSG, IPSD_TRANSPORT_UNLOCK, NO_FORMAT_CHANGE
0x00,0x85, 0x0F, 0x00,0x00, 0x08,
//** GET_PSD_STATUS **, ALWAYS_PSD, IPSD_GET_STATE, NO_FORMAT_CHANGE
0x00,0x86, 0x14, 0x00,0x02, 0x08,
//** MFG_SCRAP_PSD_INIT_3 **, XFER_CONTROL_SCRIPT, SCRIPT_EDM_IPSD_MASTER_ERASE, NO_FORMAT_CHANGE
0x00,0x87, 0x11, 0x00,0x20, 0x08,
//** GET_PSD_REAL_TIME_CLOCK **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_DATE_TIME, NO_FORMAT_CHANGE
0x00,0x88, 0x0E, 0x00,0x1A, 0x08,
//** SET_IPSD_REAL_TIME_CLOCK **, ALWAYS_SCRIPTED, SCRIPT_IPSD_SET_DATE_TIME, NO_FORMAT_CHANGE
0x00,0x89, 0x0E, 0x00,0x19, 0x08,
//** GET_IPSD_CLOCK_OFFSET **, ALWAYS_SCRIPTED, SCRIPT_GET_IPSD_CLOCK_OFFSET, NO_FORMAT_CHANGE
0x00,0x8A, 0x0E, 0x00,0x1B, 0x08,
//** PSD_TYPE **, UIC_PARAM, DEVICE_MSG_CNT, HEX_TO_STRING
0x00,0x8B, 0x06, 0x00,0x37, 0x00,
//** BOBID_IPSD_PROVIDERKEYSIZE **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x8E, 0x06, 0x00,0x37, 0x08,
//** BOBID_IPSD_INITDATASIZE **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x8F, 0x06, 0x00,0x37, 0x08,
//** PSD_XPORT_MSG_STATUS **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x90, 0x06, 0x00,0x37, 0x08,
//** BOBID_IPSD_INITDATAGLOB **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x8D, 0x06, 0x00,0x37, 0x08,
//** BOBID_IPSD_PROVIDERKEYDATA **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x8C, 0x06, 0x00,0x37, 0x08,
//** BOBID_IPSD_PBISERIALNUM **, STATIK_IPSD, DEVICE_MSG_CNT, STRING_NEEDS_NULL
0x00,0x13, 0x15, 0x00,0x37, 0x06,
//** BOBID_IPSD_PCN **, STATIK_IPSD, DEVICE_MSG_CNT, STRING_NEEDS_NULL
0x00,0x11, 0x15, 0x00,0x37, 0x06,
//** BOBID_MFG_IPSD_COMMIT_PARAMETER **, ALWAYS_SCRIPTED, SCRIPT_IPSD_COMMIT_PARAMS, NO_FORMAT_CHANGE
0x00,0x91, 0x0E, 0x00,0x1C, 0x08,
//** GET_GEN_PSD_KEY_REQ **, XFER_CONTROL_SCRIPT, SCRIPT_EDM_IPSD_GEN_PSD_KEY, NO_FORMAT_CHANGE
0x00,0x92, 0x11, 0x00,0x1E, 0x08,
//** BOBID_SET_IPSD_LD_ENCR_KEY_TYPE **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x93, 0x06, 0x00,0x37, 0x08,
//** BOBID_MFG_IPSD_LD_ENCR_KEY **, XFER_CONTROL_SCRIPT, SCRIPT_EDM_IPSD_LD_ENCR_KEY, NO_FORMAT_CHANGE
0x00,0x94, 0x11, 0x00,0x22, 0x08,
//** BOBID_IPSD_INDICIASN **, STATIK_IPSD, DEVICE_MSG_CNT, STRING_NEEDS_NULL
0x00,0x14, 0x15, 0x00,0x37, 0x06,
//** VLT_PURSE_FIXED_ZEROS **, IPSD_FLASH_PARM, IBYTE_PARAM, HEX_TO_STRING
0x00,0x99, 0x17, 0x00,0x08, 0x00,
//** VLT_PURSE_REFILL_INCREMNT **, IPSD_FLASH_PARM, IBYTE_PARAM, HEX_TO_STRING
0x00,0x9A, 0x17, 0x00,0x08, 0x00,
//** VLT_DEBOPTS_FILE_DATEDUCK_FLAG **, IPSD_FLASH_PARM, IBYTE_PARAM, HEX_TO_STRING
0x00,0x9B, 0x17, 0x00,0x08, 0x00,
//** VLT_DEBOPTS_FILE_TOWNDUCK_FLAG **, IPSD_FLASH_PARM, IBYTE_PARAM, HEX_TO_STRING
0x00,0x9C, 0x17, 0x00,0x08, 0x00,
//** VLT_SECDATA_INSP_LOCK_ENABLED **, IPSD_FLASH_PARM, IBYTE_PARAM, HEX_TO_STRING
0x00,0x9D, 0x17, 0x00,0x08, 0x00,
//** VLT_PURSE_MAX_REFILL_VAL **, IPSD_FLASH_PARM, ILONG_PARAM, SLONG_TO_STRING
0x00,0x9E, 0x17, 0x00,0x0A, 0x05,
//** VLT_PURSE_MIN_REFILL_VAL **, IPSD_FLASH_PARM, ILONG_PARAM, SLONG_TO_STRING
0x00,0x9F, 0x17, 0x00,0x0A, 0x05,
//** VLT_DEBOPTS_FILE_MAX_DEB_VAL **, STATIK_IPSD, DEVICE_MSG_CNT, SLONG_TO_STRING
0x00,0xA0, 0x15, 0x00,0x37, 0x05,
//** VLT_DEBOPTS_FILE_MIN_DEB_VAL **, STATIK_IPSD, DEVICE_MSG_CNT, SLONG_TO_STRING
0x00,0xA1, 0x15, 0x00,0x37, 0x05,
//** VLT_PURSE_REFILL_FAIL_LIMIT **, IPSD_FLASH_PARM, ILONG_PARAM, SLONG_TO_STRING
0x00,0xA2, 0x17, 0x00,0x0A, 0x05,
//** VLT_USERSET_LOW_POST_WARN **, IPSD_FLASH_PARM, ILONG_PARAM, SLONG_TO_STRING
0x00,0xA3, 0x17, 0x00,0x0A, 0x05,
//** VLT_SECDATA_INSPECT_WARN_PER **, IPSD_FLASH_PARM, IWORD_PARAM, HEX_TO_STRING
0x00,0xA4, 0x17, 0x00,0x09, 0x00,
//** BOBID_IPSD_REFILLTYPE **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x23, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_CLEARARWITHPVR **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x2B, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_INDICIAVERNUM **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x1C, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_CURRENCYCODE **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x0F, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_ALGORITHID **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x1D, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_FRENCHMAC_SZ **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x35, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_FRENCHOCR_SZ **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x36, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_INDICIATYPE **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x1B, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_KEYPADREFILLINVOKESINSPECT **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x24, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_ORIGINCOUNTRY **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x0E, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_BACKDATELIMIT **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x31, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_DATEADVANCELIMIT **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x30, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_ASCREGPRESET **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x32, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_MANUFSMR **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x18, 0x15, 0x00,0x37, 0x08,
//** BOBID_MFG_GET_IPSD_DEVICE_ID **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xA5, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_MANUFACTURERID **, STATIK_IPSD, DEVICE_MSG_CNT, STRING_NEEDS_NULL
0x00,0x19, 0x15, 0x00,0x37, 0x06,
//** BOBID_IPSD_MODELNUMBER **, STATIK_IPSD, DEVICE_MSG_CNT, STRING_NEEDS_NULL
0x00,0x1A, 0x15, 0x00,0x37, 0x06,
//** BOBID_IPSD_MAXASCREG **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x33, 0x15, 0x00,0x37, 0x08,
//** BOBID_MFG_GET_IPSD_CHALLENGE_AND_STAT **, ALWAYS_SCRIPTED, SCRIPT_MFG_GET_PSD_CHALLENGE_AND_STAT, NO_FORMAT_CHANGE
0x00,0xA6, 0x0E, 0x00,0x29, 0x08,
//** GET_INDICIA_PRINT_FLAGS **, UIC_PARAM, DEVICE_MSG_CNT, HEX_TO_STRING
0x00,0xA7, 0x06, 0x00,0x37, 0x00,
//** SET_INDICIA_PRINT_FLAGS **, UIC_PARAM, DEVICE_MSG_CNT, HEX_TO_STRING
0x00,0xA8, 0x06, 0x00,0x37, 0x00,
//** BOBID_IPSD_RTC **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_RTC, HEX_TO_STRING
0x00,0x2F, 0x0E, 0x00,0x2B, 0x00,
//** BOBID_IPSD_MFGSNCRC **, STATIK_IPSD, DEVICE_MSG_CNT, HEX_TO_STRING
0x00,0xA9, 0x15, 0x00,0x37, 0x00,
//** BOBID_IPSD_MFGSNASCII **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x46, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_FAMILYCODE **, STATIK_IPSD, DEVICE_MSG_CNT, HEX_TO_STRING
0x00,0x44, 0x15, 0x00,0x37, 0x00,
//** VLT_CVA_NEXT_INSPECTION_DATE **, ALWAYS_SCRIPTED, SCRIPT_NEXT_INSPECTION_DATE, NO_FORMAT_CHANGE
0x00,0xAA, 0x0E, 0x00,0x31, 0x08,
//** VLT_PURSE_DEBIT_GRACE **, UIC_CONST, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xAB, 0x18, 0x00,0x37, 0x08,
//** BOBID_IPSD_INIT_CLOCK **, ALWAYS_SCRIPTED, SCRIPT_IPSD_INIT_CLOCK, NO_FORMAT_CHANGE
0x00,0xAC, 0x0E, 0x00,0x06, 0x08,
//** BOBID_IPSD_FAKE_ORIGIN_ZIPCODE **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xAD, 0x06, 0x00,0x37, 0x08,
//** VLT_IDENT_FILE_ZIPCODE **, ALWAYS_SCRIPTED, SCRIPT_PICK_A_ZIPCODE, NO_FORMAT_CHANGE
0x00,0xAE, 0x0E, 0x00,0x33, 0x08,
//** VLT_INDI_DATEADV_LIMIT **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xAF, 0x15, 0x00,0x37, 0x08,
//** VLT_INDI_BACKDATE_LIMIT **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xB0, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_VERIFY_HASH_SIGNATURE_DATA_PTR **, XFER_CONTROL_SCRIPT, SCRIPT_IPSD_VERIFY_HASH_SIGNATURE, NO_FORMAT_CHANGE
0x00,0xB7, 0x11, 0x00,0x34, 0x08,
//** IPSD_LD_PROV_KEY **, XFER_CONTROL_SCRIPT, SCRIPT_EDM_IPSD_LD_PROV_KEY, NO_FORMAT_CHANGE
0x00,0xB8, 0x11, 0x00,0x37, 0x08,
//** GET_LOCAL_AR **, UIC_PARAM, DEVICE_MSG_CNT, MONEY_TO_STRING
0x00,0xB9, 0x06, 0x00,0x37, 0x03,
//** GET_LOCAL_PIECE_COUNT **, UIC_PARAM, DEVICE_MSG_CNT, SLONG_TO_STRING
0x00,0xBA, 0x06, 0x00,0x37, 0x05,
//** GET_LOCAL_DR **, UIC_PARAM, DEVICE_MSG_CNT, MONEY_TO_STRING
0x00,0xBB, 0x06, 0x00,0x37, 0x03,
//** MFG_UIC_PCN **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xBC, 0x06, 0x00,0x37, 0x08,
//** MFG_UIC_SERIAL_NUM **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xBD, 0x06, 0x00,0x37, 0x08,
//** MFG_UIC_SMR **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xBE, 0x06, 0x00,0x37, 0x08,
//** MFG_UIC_HW_REV **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xBF, 0x06, 0x00,0x37, 0x08,
//** READ_CURRENT_PERMIT_SELECTION **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xC0, 0x06, 0x00,0x37, 0x08,
//** SET_CURRENT_PERMIT_SELECTION **, FOLLOW_UP_SCRIPT, SCRIPT_SET_CURRENT_PERMIT_SELECTION, NO_FORMAT_CHANGE
0x00,0xC1, 0x0A, 0x00,0x3F, 0x08,
//** READ_CURRENT_PERMIT_NAME **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xC2, 0x06, 0x00,0x37, 0x08,
//** SET_CURRENT_PERMIT_NAME **, FOLLOW_UP_SCRIPT, SCRIPT_SET_CURRENT_PERMIT_NAME, NO_FORMAT_CHANGE
0x00,0xC3, 0x0A, 0x00,0x40, 0x08,
//** READ_CURRENT_TEXTENTRY_SELECTION **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xC4, 0x06, 0x00,0x37, 0x08,
//** SET_CURRENT_TEXTENTRY_SELECTION **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xC5, 0x06, 0x00,0x37, 0x08,
//** GET_PERMIT_PRINT_FLAGS **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xC6, 0x06, 0x00,0x37, 0x08,
//** SET_PERMIT_PRINT_FLAGS **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xC7, 0x06, 0x00,0x37, 0x08,
//** GET_DATETIME_PRINT_FLAGS **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xC8, 0x06, 0x00,0x37, 0x08,
//** SET_DATETIME_PRINT_FLAGS **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xC9, 0x06, 0x00,0x37, 0x08,
//** GET_STATIC_IMAGE_STATUS **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xCA, 0x06, 0x00,0x37, 0x08,
//** PSD_PBP_FINAL_FRANK_REC **, XFER_CONTROL_MSG, IPSD_GERMAN_CREATE_FINALIZING_FRANK, NO_FORMAT_CHANGE
0x00,0xCC, 0x12, 0x00,0x26, 0x08,
//** BOBID_IPSD_ADDDRTOARONPVR **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xD6, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_GERMAN_ARATLASTREF **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, MONEY_TO_STRING
0x00,0xD2, 0x0E, 0x00,0x0F, 0x03,
//** BOBID_IPSD_GERMAN_CRYPTOSTR **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0xD3, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_GERMAN_POSTIDDATA **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xD4, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_GERMAN_AUTHKEYREV **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xD5, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_GERMAN_SUPPLE_LINE1 **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xD7, 0x06, 0x00,0x37, 0x08,
//** BOBID_IPSD_GERMAN_SUPPLE_LINE2 **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xD8, 0x06, 0x00,0x37, 0x08,
//** PSD_LD_ENCR_KEY_REQ **, XFER_CONTROL_SCRIPT, SCRIPT_EDM_IPSD_LD_ENCR_KEY, NO_FORMAT_CHANGE
0x00,0xD9, 0x11, 0x00,0x22, 0x08,
//** PSD_REFILL_TYPE **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0xDA, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_ENBLDSBLSTAT **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0xB6, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_WATCHDOGTMRRESET **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x40, 0x0E, 0x00,0x0F, 0x08,
//** BOBS_LAST_DCSTATUS **, UIC_PARAM, DEVICE_MSG_CNT, HEX_TO_STRING
0x00,0xDC, 0x06, 0x00,0x37, 0x00,
//** SET_CURRENT_TEST_PRINT **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xDD, 0x06, 0x00,0x37, 0x08,
//** BOBID_IPSD_FRENCH_ALPHANUMPIECECOUNT **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xDE, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_RESETPCONPVR **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xDF, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_ALLOWZEROPPOSTAGE **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xE0, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_ZEROPOSTAGEPIECECOUNT **, SCRIPTED, DUMMY_SCRIPT, SLONG_TO_STRING
0x00,0xE1, 0x07, 0x00,0x00, 0x05,
//** READ_CURRENT_PERMIT_TYPE_SELECTION **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xE2, 0x06, 0x00,0x37, 0x08,
//** SET_CURRENT_PERMIT_TYPE_SELECTION **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xE3, 0x06, 0x00,0x37, 0x08,
//** BOBID_IPSD_BELGIANAPPVALUE **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xE4, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_REFILLCOUNT **, SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x2C, 0x07, 0x00,0x0F, 0x08,
//** LIPSD_FIRMWAREVERNUM **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0x83, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_ZIPCODE **, STATIK_IPSD, DEVICE_MSG_CNT, STRING_NEEDS_NULL
0x00,0x10, 0x15, 0x00,0x37, 0x06,
//** GET_LOCAL_TOTAL_PIECE_COUNT **, STATIK_IPSD, DEVICE_MSG_CNT, SLONG_TO_STRING
0x00,0xE5, 0x15, 0x00,0x37, 0x05,
//** GET_LOCAL_ZERO_PIECE_COUNT **, STATIK_IPSD, DEVICE_MSG_CNT, SLONG_TO_STRING
0x00,0xE6, 0x15, 0x00,0x37, 0x05,
//** BOBID_IPSD_IBILITEINDICIAVERSION **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xE7, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_IBILITEVENDORMODEL **, STATIK_IPSD, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xE8, 0x15, 0x00,0x37, 0x08,
//** BOBID_IPSD_FLEXDEBIT_OUTPUT_GLOB **, TAKE_ASIS, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xEF, 0x0B, 0x00,0x37, 0x08,
//** BOBID_IPSD_FLEXDEBIT_DSASIG **, TAKE_ASIS, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xF0, 0x0B, 0x00,0x37, 0x08,
//** BOBID_IPSD_FLEXDEBIT_HMAC **, TAKE_ASIS, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xF1, 0x0B, 0x00,0x37, 0x08,
//** BOBID_IPSD_FLEXDEBIT_2LSB_3DESMAC **, TAKE_ASIS, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xF2, 0x0B, 0x00,0x37, 0x08,
//** BOBID_IPSD_FLEXDEBIT_DESMAC **, TAKE_ASIS, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x00,0xF3, 0x0B, 0x00,0x37, 0x08,
//** BOBID_IPSD_BL_PASSTHROUGH_MSG **, XFER_CONTROL_SCRIPT, SCRIPT_GBL_PASSTHROUGH_EDM, NO_FORMAT_CHANGE
0x01,0x04, 0x11, 0x00,0x61, 0x08,
//** BOBID_IPSD_GET_PROG_MODE **, UIC_PARAM, DEVICE_MSG_CNT, NO_FORMAT_CHANGE
0x01,0x08, 0x06, 0x00,0x37, 0x08,
//** BOBID_IPSD_BL_GET_MICRO_STATUS **, STATIK_IPSD, DEVICE_MSG_CNT, STRING_NEEDS_NULL
0x01,0x02, 0x15, 0x00,0x37, 0x06,
//** BOBID_RUNSELFTESTS_GETSTATUS **, ALWAYS_SCRIPTED, SCRIPT_EDM_IPSD_RUN_SELF_TESTS, NO_FORMAT_CHANGE
0x01,0x0A, 0x0E, 0x00,0x62, 0x08,
//** BOBID_LD_IPSD_TRANSPORT_LOCK_KEY **, XFER_CONTROL_SCRIPT, SCRIPT_EDM_IPSD_MANUFACTURE, NO_FORMAT_CHANGE
0x01,0x09, 0x11, 0x00,0x64, 0x08,
//** BOBID_IPSD_FIRMWAREVER_DATE_STR **, STATIK_PSD, NOT_A_MESSAGE, NO_FORMAT_CHANGE
0x01,0x0B, 0x0C, 0x00,0x38, 0x08,
//** BOBID_UPDATE_AND_READ_INSCR_SELECT **, ALWAYS_SCRIPTED, SCRIPT_UPDATE_INSCRIPTION_SELECTION, NO_FORMAT_CHANGE
0x01,0x0C, 0x0E, 0x00,0x53, 0x08,
//** BOBID_UPDATE_AND_READ_AD_SELECT **, ALWAYS_SCRIPTED, SCRIPT_UPDATE_AD_SELECTION, NO_FORMAT_CHANGE
0x01,0x0D, 0x0E, 0x00,0x66, 0x08,
//** BOBID_UPDATE_AND_READ_INDICIA_TO_PRINT **, ALWAYS_SCRIPTED, SCRIPT_UPDATE_INDICIA_TO_PRINT, NO_FORMAT_CHANGE
0x01,0x0E, 0x0E, 0x00,0x67, 0x08,
/*
bobaMat1
VARIABLE ID     VARIANCE    CONTROL ID (MSG)    CONVERSION
---------------------------------------------------------- */

/*
bobaMat2
VARIABLE ID     VARIANCE    CONTROL ID (MSG)    CONVERSION
---------------------------------------------------------- */

/*
repDataMat0
VARIABLE ID     VARIANCE    CONTROL ID (MSG)    CONVERSION
---------------------------------------------------------- */

/*
repDataMat1
VARIABLE ID     VARIANCE    CONTROL ID (MSG)    CONVERSION
---------------------------------------------------------- */

/*
repDataMat2
VARIABLE ID     VARIANCE    CONTROL ID (MSG)    CONVERSION
---------------------------------------------------------- */

/*
mfgParmaMat0
VARIABLE ID     VARIANCE    CONTROL ID (MSG)    CONVERSION
---------------------------------------------------------- */

//** BOBID_IPSD_REFILLTYPE **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x23, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_REFILLCOUNT **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x2C, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_INDICIAVERNUM **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x1C, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_CURRENCYCODE **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x0F, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_ALGORITHID **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x1D, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_FRENCHMAC_SZ **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x35, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_FRENCHOCR_SZ **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x36, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_INDICIATYPE **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x1B, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_KEYPADREFILLINVOKESINSPECT **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x24, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_ORIGINCOUNTRY **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x0E, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_MAXDESCREG **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x34, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_BACKDATELIMIT **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x31, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_DATEADVANCELIMIT **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x30, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_CLOCKOFFSET **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x2E, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_ASCREGPRESET **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x32, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_MAXASCREG **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x33, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_INDICIASN **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x14, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_MANUFSMR **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x18, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_PCN **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x11, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_PBISERIALNUM **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x13, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_CLEARARWITHPVR **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0x2B, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_ITALYCURCODE **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0xB1, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_INDICIUMCODE **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0xB2, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_ALGRTHMOBJID **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0xB3, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_ADDDRTOARONPVR **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0xD6, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_CANADA_PROVKEYREV **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0xDB, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_FRENCH_ALPHANUMPIECECOUNT **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0xDE, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_RESETPCONPVR **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0xDF, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_ALLOWZEROPPOSTAGE **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0xE0, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_BELGIANAPPVALUE **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0xE4, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_IBILITEINDICIAVERSION **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0xE7, 0x0E, 0x00,0x0F, 0x08,
//** BOBID_IPSD_IBILITEVENDORMODEL **, ALWAYS_SCRIPTED, SCRIPT_IPSD_GET_PSDPARAMS, NO_FORMAT_CHANGE
0x00,0xE8, 0x0E, 0x00,0x0F, 0x08,

/* currOffset = 0xC07 */
/*
Bobs Error Directory
DevID  ErrTabId  ErrTableOffset  Items  KeySize  StatusVarID   StateVarID  Record Size         */
// NOT_A_DEVICE, BOB_INTERNAL, 0x00,0x00,0x0C,0x3F,  0x00,0x67,  0x02,  MEGABOBSTAT, MEGABOBSTATE, ERRLOOKUP_REK_SIZ
  0x0B,  0x01,  0x00,0x00,0x0C,0x3F,  0x00,0x67,  0x02,  0x00,0x52,  0x00,0x53,  0x05,
// IPSD, STD_ERRS, 0x00,0x00,0x0E,0x42,  0x00,0x42,  0x02,  BOBID_IPSD_CMDSTATUSRESP, BOBID_IPSD_STATE, ERRLOOKUP_REK_SIZ
  0x09,  0x00,  0x00,0x00,0x0E,0x42,  0x00,0x42,  0x02,  0x00,0x01,  0x00,0x02,  0x05,
// NOT_A_DEVICE, STD_ERRS, 0x00,0x00,0x0F,0x8C,  0x00,0x32,  0x02,  IGBC_STATUS, IGBC_STATE, ERRLOOKUP_REK_SIZ
  0x0B,  0x00,  0x00,0x00,0x0F,0x8C,  0x00,0x32,  0x02,  0x00,0x98,  0x00,0x97,  0x05,
// IPSD_BL, STD_ERRS, 0x00,0x00,0x10,0x86,  0x00,0x07,  0x01,  BOBID_IPSD_BL_RESPSTATUS, BOBID_IPSD_BL_RESPSTATE, ERRLOOKUP_REK_SIZ_BYTE
  0x0A,  0x00,  0x00,0x00,0x10,0x86,  0x00,0x07,  0x01,  0x01,0x01,  0x01,0x07,  0x04,

/* currOffset = 0xC3F */
/*
ERROR DEFINITION TABLES
---------------------------------------------------------- */

/*
Error Table:  bobInternalLookup
DEVICECODE     POSTEDCODE    CLASSCODE    SPECIALTY
---------------------------------------------------------- */

//DO_NOT_POST_BOB_ERROR          MEGABOB_STATE_OK                  ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x00, 0x00, 0x25, 0x00,
//BOB_BAD_SCRIPT_ID              JSCM_BAD_SCRIPT_ID                ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x01, 0x01, 0x25, 0x00,
//BOB_PHC_COM_FAILURE            JSCM_PHC_COM_FAILURE              ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x00,0x03, 0x03, 0x25, 0x00,
//BOB_XCARD_COM_FAILURE          JSCM_XCARD_COM_FAILURE            ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x04, 0x04, 0x25, 0x00,
//BOB_UNKNOWN_COM_FAILURE        JSCM_UNKNOWN_COM_FAILURE          ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x05, 0x05, 0x25, 0x00,
//BOB_PSD_HANDSHAKE_FAILURE      JSCM_PSD_HANDSHAKE_FAILURE        ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x06, 0x06, 0x25, 0x00,
//BOB_PHC_HANDSHAKE_FAILURE      JSCM_PHC_HANDSHAKE_FAILURE        ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x07, 0x07, 0x25, 0x00,
//BOB_XCARD_HANDSHAKE_FAILURE    JSCM_XCARD_HANDSHAKE_FAILURE      ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x08, 0x08, 0x25, 0x00,
//BOB_INTERNAL_ERROR             JSCM_INTERNAL_ERROR               ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x09, 0x09, 0x25, 0x00,
//BOB_UNKNOWN_HANDSHAKE_FAILURE  JSCM_UNKNOWN_HANDSHAKE_FAILURE    ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x0A, 0x0A, 0x25, 0x00,
//BOB_NG_TASK_MESSAGE            JSCM_NG_TASK_MESSAGE              ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x0B, 0x0B, 0x25, 0x00,
//BOB_ACCESS_CODE_ERROR          JSCM_ACCESS_CODE_ERROR            ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x0C, 0x0C, 0x25, 0x00,
//BOB_OPERATION_NOT_AVAILABLE    JSCM_OPERATION_NOT_AVAILABLE      ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x0D, 0x0D, 0x25, 0x00,
//BOB_FONT_ERROR                 JSCM_FONT_ERROR                   ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x0E, 0x0E, 0x25, 0x00,
//BOB_FONT_DOWNLOAD_ERROR        JSCM_FONT_DOWNLOAD_ERROR          ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x0F, 0x0F, 0x25, 0x00,
//BOB_REPORT_HASH_ERROR          JSCM_REPORT_HASH_ERROR            ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x10, 0x10, 0x25, 0x00,
//BOB_PHC_POWER_PARM_ERR         JSCM_PHC_POWER_PARM_ERR           ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x11, 0x11, 0x25, 0x00,
//BOB_PRINT_MODE_ERROR           JSCM_PRINT_MODE_ERROR             ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x12, 0x12, 0x25, 0x00,
//BOB_NO_METER_STATUS            JSCM_NO_METER_STATUS              ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x13, 0x13, 0x25, 0x00,
//BOB_BATCHUPDATE_ERR            JSCM_BATCHUPDATE_ERR              ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x14, 0x14, 0x25, 0x00,
//BOB_REPORT_GATHERING_ERR       JSCM_REPORT_GATHERING_ERR         ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x15, 0x15, 0x25, 0x00,
//BOB_AD_ACCESS_ERROR            JSCM_AD_ACCESS_ERROR              ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x16, 0x16, 0x25, 0x00,
//BOB_INSCR_ACCESS_ERR           JSCM_INSCR_ACCESS_ERR             ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x17, 0x17, 0x25, 0x00,
//BOB_PHC_DOWNLOAD_SETUP_ERR     JSCM_PHC_DOWNLOAD_SETUP_ERR       ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x18, 0x18, 0x25, 0x00,
//BOB_LEAD_IMAGE_ERR             JSCM_LEAD_IMAGE_ERR               ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x19, 0x19, 0x25, 0x00,
//BOB_LAG_IMAGE_ERR              JSCM_LAG_IMAGE_ERR                ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x1A, 0x1A, 0x25, 0x00,
//BOB_PREDEB_PREP_ERR            JSCM_PREDEB_PREP_ERR              ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x1B, 0x1B, 0x25, 0x00,
//BOB_PREP_PHC_CFG_ERR           JSCM_PREP_PHC_CFG_ERR             ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x1C, 0x1C, 0x25, 0x00,
//BOB_PREP_VENDORKEY_ERR         JSCM_PREP_VENDORKEY_ERR           ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x1D, 0x1D, 0x25, 0x00,
//BOB_SNIPPET_SIGN_ERR           JSCM_SNIPPET_SIG_FAIL             ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x1E, 0x1E, 0x25, 0x00,
//BOB_INDICIA_NOT_FOUND          JSCM_INDICIA_NOT_FOUND            ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x1F, 0x1F, 0x25, 0x00,
//BOB_INDICIA_FONT_NOT_FOUND     JSCM_INDICIA_FONT_NOT_FOUND       ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x20, 0x20, 0x25, 0x00,
//BOB_REPORT_NOT_FOUND           JSCM_REPORT_NOT_FOUND             ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x21, 0x21, 0x25, 0x00,
//BOB_REPORT_FONT_NOT_FOUND      JSCM_REPORT_FONT_NOT_FOUND        ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x22, 0x22, 0x25, 0x00,
//BOB_CIRCLE_NOT_FOUND           JSCM_CIRCLE_NOT_FOUND             ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x23, 0x23, 0x25, 0x00,
//BOB_CIRCLE_FONT_NOT_FOUND      JSCM_CIRCLE_FONT_NOT_FOUND        ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x24, 0x24, 0x25, 0x00,
//BOB_DATE_TIME_ACCESS_ERROR     JSCM_DATE_TIME_ACCESS_ERROR       ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x25, 0x25, 0x25, 0x00,
//BOB_MESSAGE_BUILD_ERROR        JSCM_MESSAGE_BUILD_ERROR          ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x26, 0x26, 0x25, 0x00,
//BOB_BELATED_SCRIPT_SKIP        JSCM_BELATED_SCRIPT_SKIP          ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x27, 0x27, 0x25, 0x00,
//BOB_BAD_REPLY_LENGTH           JSCM_BAD_REPLY_LENGTH             ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x28, 0x28, 0x25, 0x00,
//BOB_BAD_PACKED_PZONE_COMPID    JSCM_BAD_PACKED_PZONE_COMPID      ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x29, 0x29, 0x25, 0x00,
//BOB_BAD_PACKED_PZONE_FUNKID    JSCM_BAD_PACKED_PZONE_FUNKID      ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x2A, 0x2A, 0x25, 0x00,
//BOB_MESSAGE_RESEND             JSCM_BOB_MESSAGE_RESEND           ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x2B, 0x2B, 0x25, 0x00,
//BOB_TEST_PRINT_NOT_FOUND       JSCM_TEST_PRINT_NOT_FOUND         ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x2C, 0x2C, 0x25, 0x00,
//BOB_ACCOUNT_EXPIRED            JSCM_ACCOUNT_EXPIRED              ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x2D, 0x2D, 0x25, 0x00,
//BOB_PREP_TRUSTEDKEY_ERR        JSCM_PREP_TRUSTEDKEY_ERR          ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x2E, 0x2E, 0x25, 0x00,
//BOB_UNKNOWN_DEVICE_ID          JSCM_UNKNOWN_DEVICE_ID            ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x2F, 0x2F, 0x25, 0x00,
//BOB_ACCOUNT_OUTTA_FUNDS        JSCM_ACCOUNT_OUTTA_FUNDS          ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x30, 0x30, 0x25, 0x00,
//BOB_ACCOUNT_OUTTA_PEACE        JSCM_ACCOUNT_OUTTA_PEACE          ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x31, 0x31, 0x25, 0x00,
//BOB_UNKNOWN_ACCOUNT_ERR        JSCM_UNKNOWN_ACCOUNT_ERR          ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x32, 0x32, 0x25, 0x00,
//BOB_MISC_ACCESS_ERROR          JSCM_MISC_ACCESS_ERROR            ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x33, 0x33, 0x25, 0x00,
//BOB_STAR_ACCESS_ERROR          JSCM_STAR_ACCESS_ERROR            ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x34, 0x34, 0x25, 0x00,
//BOB_EB_ACCESS_ERROR            JSCM_EB_ACCESS_ERROR              ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x35, 0x35, 0x25, 0x00,
//BOB_CANNOT_READ_BAR_PARAMS     JSCM_CANNOT_READ_BAR_PARAMS       ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x36, 0x36, 0x25, 0x00,
//BOB_NPCG_MISS_ERROR            JSCM_NPCG_MISS_ERROR              ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x37, 0x37, 0x25, 0x00,
//BOB_PCNPCG_MISS_ERROR          JSCM_PCNPCG_MISS_ERROR            ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x38, 0x38, 0x25, 0x00,
//BOB_TPKEY_MISS_ERROR           JSCM_TPKEY_MISS_ERROR             ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x39, 0x39, 0x25, 0x00,
//BOB_UNKNOWN_GATEKEEPER         JSCM_UNKNOWN_GATEKEEPER           ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x3A, 0x3A, 0x25, 0x00,
//BOB_SNIPPET_ERROR              JSCM_SNIPPET_ERROR                ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x3D, 0x3B, 0x25, 0x00,
//BOB_IPSD_COM_FAILURE           JSCM_IPSD_COM_FAILURE             ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x3B, 0x3C, 0x25, 0x00,
//BOB_IPSD_HANDSHAKE_FAILURE     JSCM_IPSD_HANDSHAKE_FAILURE       ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x3C, 0x3D, 0x25, 0x00,
//BOB_FLASH_GRAPHIC_ACCESS_ERRO  JSCM_FLASH_GRAPHIC_ACCESS_ERROR   ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x3E, 0x3E, 0x25, 0x00,
//BOB_DYNAMIC_IG_BLOB_ERROR      JSCM_DYNAMIC_IG_BLOB_ERROR        ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x3F, 0x3F, 0x25, 0x00,
//BOB_STATIC_IG_BLOB_ERROR       JSCM_STATIC_IG_BLOB_ERROR         ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x40, 0x40, 0x25, 0x00,
//BOB_REPORT_IG_BLOB_ERROR       JSCM_REPORT_IG_BLOB_ERROR         ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x41, 0x41, 0x25, 0x00,
//BOB_SHADOW_REFILL_ERROR        JSCM_SHADOW_REFILL_ERROR          ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x42, 0x42, 0x25, 0x00,
//BOB_SHADOW_DEBIT_ERROR         JSCM_SHADOW_DEBIT_ERROR           ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x43, 0x43, 0x25, 0x00,
//BOB_IPSD_COM_TIMEOUT_FAILURE   JSCM_IPSD_COM_TIMEOUT_FAILURE     ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x44, 0x44, 0x25, 0x00,
//BOB_IPSD_COM_OS_NO_EVENT_MATC  JSCM_IPSD_COM_OS_NO_EVENT_MATCH   ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x45, 0x45, 0x25, 0x00,
//BOB_IPSD_COM_OS_TIMEOUT        JSCM_IPSD_COM_OS_TIMEOUT          ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x46, 0x46, 0x25, 0x00,
//BOB_CURRENCY_MISMATCH          JSCM_CURRENCY_MISMATCH            ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x47, 0x47, 0x25, 0x00,
//BOB_CMOS_GMTOFFSET_OUT_OF_RAN  JSCM_CMOS_GMTOFFSET_OUT_OF_RANGE  ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x48, 0x48, 0x25, 0x00,
//BOB_PERMIT_NOT_FOUND           JSCM_PERMIT_NOT_FOUND             ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x49, 0x49, 0x25, 0x00,
//BOB_DATETIME_NOT_FOUND         JSCM_DATETIME_NOT_FOUND           ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x4A, 0x4A, 0x25, 0x00,
//BOB_PERMIT_BATCH_COUNT_SET_ER  JSCM_PERMIT_BATCH_COUNT_SET_ERRO  ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x4B, 0x4B, 0x25, 0x00,
//BOB_AUTO_INSCRIPTION_NOT_FOUN  JSCM_AUTO_INSCRIPTION_NOT_FOUND   ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x4C, 0x4C, 0x25, 0x00,
//BOB_AUTO_INSCRIPTION_OUT_OF_R  JSCM_AUTO_INSCRIPTION_OUT_OF_RAN  ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x4D, 0x4D, 0x25, 0x00,
//BOB_PC_EOL_ERROR               JSCM_PC_EOL_ERROR                 ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x4E, 0x4E, 0x25, 0x00,
//BOB_SHADOW_CORRECTION_FAILURE  JSCM_SHADOW_CORRECTION_FAILURE    ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /*  */
0x00,0x4F, 0x4F, 0x25, 0x00,
//BOB_SHADOW_REFUND_ERROR        JSCM_SHADOW_REFUND_ERROR          ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* Error logging postage value refund message to shadow log. */
0x00,0x50, 0x50, 0x25, 0x00,
//BOB_VALID_DATA_ERR             JSCM_VALID_DATA_ERR               ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* Error in fnValidData. Probably a bad table lookup. */
0x00,0x51, 0x51, 0x25, 0x00,
//BOB_WRITE_DATA_ERR             JSCM_WRITE_DATA_ERR               ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* Error in fnWriteData. Probably a bad table lookup. */
0x00,0x52, 0x52, 0x25, 0x00,
//BOB_GLOB_OVERRUN_G2B           JSCM_GLOB_OVERRUN_G2B             ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* During glob to bob conversion, data access beyond glob buffer. */
0x00,0x53, 0x53, 0x25, 0x00,
//BOB_GLOB_OVERRUN_B2G           JSCM_GLOB_OVERRUN_B2G             ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* During bob to glob conversion, data access beyond glob buffer. */
0x00,0x54, 0x54, 0x25, 0x00,
//BOB_GLOB_VAR_TOO_SMALL         JSCM_GLOB_VAR_TOO_SMALL           ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* Rx'd msg with glob that was bigger than the glob's destination array. */
0x00,0x55, 0x55, 0x25, 0x00,
//BOB_IPSD_BL_2_ASTEROID         JSCM_IPSD_BL_2_ASTEROID           ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* Tried to send a boot loader message to an Asteroid. */
0x00,0x56, 0x56, 0x25, 0x00,
//BOB_BL_CMD_2_GEMINI_IN_APP     JSCM_BL_CMD_2_GEMINI_IN_APP       ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* Tried to send a boot laoder command to a gemini in App or Secure App Mode. */
0x00,0x57, 0x57, 0x25, 0x00,
//BOB_APP_CMD_2_GEMINI_IN_BL     JSCM_APP_CMD_2_GEMINI_IN_BL       ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* Tried to send an Application comamnd to a Gemini in Boot Loader Mode. */
0x00,0x58, 0x58, 0x25, 0x00,
//BOB_IPSD_BL_COM_OS_NO_EVENT_M  JSCM_IPSD_BL_COM_OS_NO_EVENT_MAT  ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* IPSD Boot Loader communications driver os no event match error. */
0x00,0x59, 0x59, 0x25, 0x00,
//BOB_IPSD_BL_COM_OS_TIMEOUT     JSCM_IPSD_BL_COM_OS_TIMEOUT       ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* IPSD Boot Loader communications driver os timeout error. */
0x00,0x5A, 0x5A, 0x25, 0x00,
//BOB_IPSD_BL_COM_FAILURE        JSCM_IPSD_BL_COM_FAILURE          ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* IPSD Boot Loader failed to reply to a message. */
0x00,0x5B, 0x5B, 0x25, 0x00,
//BOB_IPSD_BL_COM_TIMEOUT_FAILU  JSCM_IPSD_BL_COM_TIMEOUT_FAILURE  ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* IPSD Boot Loader communications driver timeout error. */
0x00,0x5C, 0x5C, 0x25, 0x00,
//BOB_IPSD_BL_HANDSHAKE_FAILURE  JSCM_IPSD_BL_HANDSHAKE_FAILURE    ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* IPSD USB driver failure on Boot Loader Message. */
0x00,0x5D, 0x5D, 0x25, 0x00,
//BOB_IPSD_BL_BAD_REPLY_FORMAT   JSCM_IPSD_BL_BAD_REPLY_FORMAT     ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* Reply from the boot loader did not start with "JRB". */
0x00,0x5E, 0x5E, 0x25, 0x00,
//BOB_IPSD_BL_REPLY_TOO_SHORT    JSCM_IPSD_BL_REPLY_TOO_SHORT      ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* Reply from the boot loader had less than 8 bytes. */
0x00,0x5F, 0x5F, 0x25, 0x00,
//BOB_IPSD_UNKNOWN_TYPE_OR_MODE  JSCM_IPSD_UNKNOWN_TYPE_OR_MODE    ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* Unknown IPSD family code, or could not read the Micro Status. */
0x00,0x60, 0x60, 0x25, 0x00,
//BOB_IPSD_BL_TOO_RISKY          JSCM_IPSD_BL_TOO_RISKY            ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* Attempt to disable the bootloader (or set security lock) before entering and testing the APP mode. */
0x00,0x61, 0x61, 0x25, 0x00,
//BOB_IPSD_BL_EDM_ERR            JSCM_IPSD_BL_EDM_ERR              ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* EDM trying to passthrough msg to IPSD BL, but bob can't talk to IPSD. */
0x00,0x62, 0x62, 0x25, 0x00,
//BOB_BADTIME_BOBSPOSTAGEVAL_CH  JSCM_BADTIME_BOBSPOSTAGEVAL_CHAN  ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* A task changed bobsPostageValue between the precreate and the debit. */
0x00,0x63, 0x63, 0x25, 0x00,
//BOB_BAD_REGISTER_HANDUPDATE    JSCM_BAD_REGISTER_HANDUPDATE      ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* Registers are out of synch with IPSD after a hand-update. */
0x00,0x64, 0x64, 0x25, 0x00,
//BOB_AUTO_AD_NOT_FOUND          JSCM_AUTO_AD_NOT_FOUND            ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* Could not find a graphic in the .GAR file with a matching unicode nam. */
0x00,0x65, 0x65, 0x25, 0x00,
//BOB_AUTO_AD_OUT_OF_RANGE       JSCM_AUTO_AD_OUT_OF_RANGE         ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* Auto Ad name too long. */
0x00,0x66, 0x66, 0x25, 0x00,
//BOB_INTERNAL_LOOKUP_SIZ        JBOB_INTERNAL_LOOKUP_SIZ          ERROR_CLASS_JBOB_TASK     NO_OI_INFO             /* ALWAYS AT THE END */
0x00,0x67, 0x67, 0x25, 0x00,
/*
Error Table:  IPSDLookup
DEVICECODE     POSTEDCODE    CLASSCODE    SPECIALTY
---------------------------------------------------------- */

//ERROR CODE                     IPSD_STATE_OK                     ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x90,0x00, 0x00, 0x20, 0x00,
//ERROR CODE                     IPSD_PACKET_OK                    ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x63,0x01, 0x01, 0x20, 0x00,
//ERROR CODE                     IPSDERR_SELFTEST_FAIL             ERROR_CLASS_IPSD2         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x64,0x01, 0x01, 0x21, 0x00,
//ERROR CODE                     IPSDERR_INV_PSD_PUBKEY            ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x10, 0x04, 0x20, 0x00,
//ERROR CODE                     IPSDERR_LOGIN_FAIL                ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x11, 0x0F, 0x20, 0x00,
//ERROR CODE                     IPSDERR_DES_OVERLIMIT             ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x12, 0x11, 0x20, 0x00,
//ERROR CODE                     IPSDERR_ASCDESSUM_OVERLIMIT       ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x13, 0x12, 0x20, 0x00,
//ERROR CODE                     IPSDERR_INV_REGISTER_VAL          ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x14, 0x05, 0x20, 0x00,
//ERROR CODE                     IPSDERR_SIGVERIFY_FAIL            ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x15, 0x14, 0x20, 0x00,
//ERROR CODE                     IPSDERR_MAILDATE_OUTOFRANGE       ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x16, 0x15, 0x20, 0x00,
//ERROR CODE                     IPSDERR_POSTAGEVAL_OUTOFRANGE     ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x17, 0x16, 0x20, 0x00,
//ERROR CODE                     IPSDERR_INSUFFICIENT_FUNDS        ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x18, 0x17, 0x20, 0x00,
//ERROR CODE                     IPSDERR_DESREG_NONZERO            ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x19, 0x18, 0x20, 0x00,
//ERROR CODE                     IPSDERR_AUDITFAIL                 ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x1A, 0x19, 0x20, 0x00,
//ERROR CODE                     IPSDERR_PCN_SN_MATCHFAIL          ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x1B, 0x0E, 0x20, 0x00,
//ERROR CODE                     IPSDERR_INV_DC_STATUS             ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x1C, 0x0D, 0x20, 0x00,
//ERROR CODE                     IPSDERR_USPS_INDICIA_FAIL         ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x1D, 0x1A, 0x20, 0x00,
//ERROR CODE                     IPSDERR_FRANCE_INDICIA_FAIL       ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x1E, 0x1B, 0x20, 0x00,
//ERROR CODE                     IPSDERR_ITALY_INDICIA_FAIL        ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x1F, 0x1C, 0x20, 0x00,
//ERROR CODE                     IPSDERR_KEYPAD_REFILL_FAIL        ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x20, 0x1D, 0x20, 0x00,
//ERROR CODE                     IPSDERR_INV_CHALLENGE             ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x21, 0x08, 0x20, 0x00,
//ERROR CODE                     IPSDERR_INV_COUNTRY               ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x22, 0x09, 0x20, 0x00,
//ERROR CODE                     IPSDERR_ASCREG_OVERLIMIT          ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x23, 0x13, 0x20, 0x00,
//ERROR CODE                     IPSDERR_INV_ARGUMENT              ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x24, 0x07, 0x20, 0x00,
//ERROR CODE                     IPSDERR_TRANSPORT_UNLOCK_FAIL     ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x25, 0x10, 0x20, 0x00,
//ERROR CODE                     IPSDERR_KEYPAD_WITHDRAWAL_FAIL    ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x26, 0x1E, 0x20, 0x00,
//ERROR CODE                     IPSDERR_INV_OFFSET                ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x27, 0x0C, 0x20, 0x00,
//ERROR CODE                     IPSDERR_TRANSACTION_FAIL          ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x28, 0x21, 0x20, 0x00,
//ERROR CODE                     IPSDERR_NO_AUTHORIZED_KEY         ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x29, 0x22, 0x20, 0x00,
//ERROR CODE                     IPSDERR_NO_SECRET_KEY             ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x30, 0x23, 0x20, 0x00,
//ERROR CODE                     IPSDERR_PSD_TAMPERED              ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x31, 0x20, 0x20, 0x00,
//ERROR CODE                     IPSDERR_SELFTEST_CRCFAIL          ERROR_CLASS_IPSD2         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x33, 0x02, 0x21, 0x00,
//ERROR CODE                     IPSDERR_SELFTEST_RNGFAIL_MONOBIT  ERROR_CLASS_IPSD2         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x34, 0x04, 0x21, 0x00,
//ERROR CODE                     IPSDERR_SELFTEST_RNGFAIL_POKER    ERROR_CLASS_IPSD2         NO_OI_INFO             /*  */
0x69,0x35, 0x05, 0x21, 0x00,
//ERROR CODE                     IPSDERR_SELFTEST_RNGFAIL_RUNS     ERROR_CLASS_IPSD2         NO_OI_INFO             /*  */
0x69,0x36, 0x06, 0x21, 0x00,
//ERROR CODE                     IPSDERR_SELFTEST_RNGFAIL_LONGRUN  ERROR_CLASS_IPSD2         NO_OI_INFO             /*  */
0x69,0x37, 0x07, 0x21, 0x00,
//ERROR CODE                     IPSDERR_SELFTEST_RNGFAIL          ERROR_CLASS_IPSD2         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x38, 0x03, 0x21, 0x00,
//ERROR CODE                     IPSDERR_SELFTEST_SHA1FAIL         ERROR_CLASS_IPSD2         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x39, 0x09, 0x21, 0x00,
//ERROR CODE                     IPSDERR_SELFTEST_DESFAIL          ERROR_CLASS_IPSD2         NO_OI_INFO             /*  */
0x69,0x3A, 0x0A, 0x21, 0x00,
//ERROR CODE                     IPSDERR_INV_INDICIA_SN            ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x3B, 0x0A, 0x20, 0x00,
//ERROR CODE                     IPSDERR_INV_MAC                   ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x3C, 0x0B, 0x20, 0x00,
//ERROR CODE                     IPSDERR_INSPECTION_OVERDUE        ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x3D, 0x1F, 0x20, 0x00,
//ERROR CODE                     IPSDERR_SELFTEST_DSAFAIL          ERROR_CLASS_IPSD2         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x3E, 0x0B, 0x21, 0x00,
//ERROR CODE                     IPSDERR_SELFTEST_RNGFAIL_KAT      ERROR_CLASS_IPSD2         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x3F, 0x08, 0x21, 0x00,
//ERROR CODE                     IPSDERR_INV_MSG_DATA              ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x84, 0x06, 0x20, 0x00,
//ERROR CODE                     IPSDERR_INV_STATE                 ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x85, 0x03, 0x20, 0x00,
//ERROR CODE                     IPSDERR_REFILL_NOT_PVD            ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x32, 0x24, 0x20, 0x00,
//ERROR CODE                     IPSDERR_INVALID_INDICIA_TYPE      ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x40, 0x25, 0x20, 0x00,
//ERROR CODE                     IPSDERR_INVALID_KEY               ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x41, 0x26, 0x20, 0x00,
//ERROR CODE                     IPSDERR_FINALIZE_FRANK_REQUIRED   ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x42, 0x27, 0x20, 0x00,
//ERROR CODE                     IPSDERR_GERMANY_INDICIA_FAIL      ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x43, 0x28, 0x20, 0x00,
//ERROR CODE                     IPSDERR_CANADA_DSA_INDICIA_FAIL   ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x44, 0x29, 0x20, 0x00,
//ERROR CODE                     IPSDERR_DEVICE_DISABLED           ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x45, 0x2A, 0x20, 0x00,
//ERROR CODE                     IPSDERR_INS_NOT_SUPPORTED         ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x6D,0x00, 0x2B, 0x20, 0x00,
//ERROR CODE                     IPSDERR_CLA_NOT_SUPPORTED         ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x6E,0x00, 0x2C, 0x20, 0x00,
//ERROR CODE                     IPSDERR_REFILL_AMOUNT_MISMATCH    ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x46, 0x2D, 0x20, 0x00,
//ERROR CODE                     IPSDERR_CHALLENGE_TIMEOUT         ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x47, 0x2D, 0x20, 0x00,
//ERROR CODE                     IPSDERR_KEY_ALREADY_GENERATED     ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x48, 0x2E, 0x20, 0x00,
//ERROR CODE                     IPSDERR_NO_PROVIDER_KEY           ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x49, 0x2F, 0x20, 0x00,
//ERROR CODE                     IPSDERR_KEY_ALREADY_LOADED        ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x4A, 0x30, 0x20, 0x00,
//ERROR CODE                     IPSDERR_SELFTEST_ECDSAFAIL        ERROR_CLASS_IPSD2         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x4B, 0x0C, 0x21, 0x00,
//ERROR CODE                     IPSDERR_EXPIRY_DATE_EXPIRED       ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x4C, 0x32, 0x20, 0x00,
//ERROR CODE                     IPSDERR_HMAC_KEY_NOT_LOADED       ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
0x69,0x4D, 0x33, 0x20, 0x00,
//ERROR CODE                     IPSDERR_SELFTEST_SHA1HMACFAIL     ERROR_CLASS_IPSD2         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x4E, 0x0D, 0x21, 0x00,
//ERROR CODE                     IPSDERR_POSTAGEVALUE_MISMATCH     ERROR_CLASS_IPSD1         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x69,0x4F, 0x34, 0x20, 0x00,
//ERROR CODE                     IPSDERR_REPEATED_RNG              ERROR_CLASS_IPSD1         NO_OI_INFO             /* The Random Number Generator watch detected a suspicious repeat. */
0x69,0x50, 0x35, 0x20, 0x00,
/*
Error Table:  IGBCLookup
DEVICECODE     POSTEDCODE    CLASSCODE    SPECIALTY
---------------------------------------------------------- */

//ERROR CODE                     IG_SCHEMA_ERROR                   ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x00,0x01, 0x01, 0x22, 0x00,
//ERROR CODE                     IG_TOO_MANY_IMAGES                ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x02, 0x02, 0x22, 0x00,
//ERROR CODE                     IG_WRONG_STATE                    ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x03, 0x03, 0x22, 0x00,
//ERROR CODE                     IG_NO_DATE_FIELD                  ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x04, 0x04, 0x22, 0x00,
//ERROR CODE                     IG_NO_EB_REGION                   ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x05, 0x05, 0x22, 0x00,
//ERROR CODE                     IG_NO_PIN_FIELD                   ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x06, 0x06, 0x22, 0x00,
//ERROR CODE                     IG_NO_TC_REGION                   ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x07, 0x07, 0x22, 0x00,
//ERROR CODE                     IG_INVALID_REGION_MAP             ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x08, 0x08, 0x22, 0x00,
//ERROR CODE                     IG_NO_LOW_INDICIA                 ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x09, 0x09, 0x22, 0x00,
//ERROR CODE                     IG_NEED_RELOAD                    ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x0a, 0x0A, 0x22, 0x00,
//ERROR CODE                     IG_INVALID_COMPONENT              ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x0b, 0x0B, 0x22, 0x00,
//ERROR CODE                     IG_NO_INDICIA_MAP                 ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x0c, 0x0C, 0x22, 0x00,
//ERROR CODE                     IG_NO_PERMIT_MAP                  ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x0d, 0x0D, 0x22, 0x00,
//ERROR CODE                     IG_NO_TAX_MAP                     ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x0e, 0x0E, 0x22, 0x00,
//ERROR CODE                     IG_NO_INDICIA_SELECTED            ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x0f, 0x0F, 0x22, 0x00,
//ERROR CODE                     IG_NO_PERMIT_SELECTED             ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x10, 0x10, 0x22, 0x00,
//ERROR CODE                     IG_MISSING_VCR_DATA               ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x11, 0x11, 0x22, 0x00,
//ERROR CODE                     IG_MISSING_COMPONENT              ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x12, 0x12, 0x22, 0x00,
//ERROR CODE                     IG_MISSING_FONT                   ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x13, 0x13, 0x22, 0x00,
//ERROR CODE                     IG_INVALID_COORDINATES            ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x14, 0x14, 0x22, 0x00,
//ERROR CODE                     IG_INVALID_SIZE                   ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x15, 0x15, 0x22, 0x00,
//ERROR CODE                     IG_INVALID_IMAGE                  ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x16, 0x16, 0x22, 0x00,
//ERROR CODE                     IG_INVALID_COMPRESSION            ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x17, 0x17, 0x22, 0x00,
//ERROR CODE                     IG_INVALID_DECOMPRESSION          ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x18, 0x18, 0x22, 0x00,
//ERROR CODE                     IG_INVALID_VCRS                   ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x19, 0x19, 0x22, 0x00,
//ERROR CODE                     IG_UNKNOWN_JUSTIFICATION          ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x1a, 0x1A, 0x22, 0x00,
//ERROR CODE                     IG_INVALID_FIELD_SIZE             ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x1b, 0x1B, 0x22, 0x00,
//ERROR CODE                     IG_TEXT_AD_TOO_WIDE               ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x1c, 0x1C, 0x22, 0x00,
//ERROR CODE                     IG_TEXT_AD_TOO_TALL               ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x1d, 0x1D, 0x22, 0x00,
//ERROR CODE                     IG_AD_AREA_TOO_SMALL              ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x1e, 0x1E, 0x22, 0x00,
//ERROR CODE                     IG_BAD_FONT                       ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x1f, 0x1F, 0x22, 0x00,
//ERROR CODE                     IG_FAILED                         ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x20, 0x20, 0x22, 0x00,
//ERROR CODE                     INVALID_NUMBER_OF_ECC_CODEWORDS   ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x00,0x40, 0x40, 0x22, 0x00,
//ERROR CODE                     INVALID_EEC_MODE                  ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x41, 0x41, 0x22, 0x00,
//ERROR CODE                     UNSUPPORTED_DM_SYMBOL_SIZE        ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x42, 0x42, 0x22, 0x00,
//ERROR CODE                     TOO_MANY_BARCODE_SYMBOLS          ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x43, 0x43, 0x22, 0x00,
//ERROR CODE                     NOT_ENOUGH_BARCODE_MEMORY         ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x44, 0x44, 0x22, 0x00,
//ERROR CODE                     TOO_MUCH_DATA_FOR_BARCODE         ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x45, 0x45, 0x22, 0x00,
//ERROR CODE                     NO_BARCODE_INFORMATION            ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x46, 0x46, 0x22, 0x00,
//ERROR CODE                     IG_NO_AD_INSCR                    ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x00,0x21, 0x21, 0x22, 0x00,
//ERROR CODE                     IG_NO_PBC_FIELD                   ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x22, 0x22, 0x22, 0x00,
//ERROR CODE                     IG_NO_TE_REGION                   ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x23, 0x23, 0x22, 0x00,
//ERROR CODE                     IG_NO_DATE_TIME_MAP               ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x24, 0x24, 0x22, 0x00,
//ERROR CODE                     IG_NO_TIME_FIELD                  ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x25, 0x25, 0x22, 0x00,
//ERROR CODE                     IG_MISSING_1D_INFO                ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x00,0x50, 0x50, 0x22, 0x00,
//ERROR CODE                     IG_INVALID_BARCODE_WIDTH          ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
0x00,0x51, 0x51, 0x22, 0x00,
//ERROR CODE                     IG_TOO_MANY_COLUMNS               ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x00,0x26, 0x26, 0x22, 0x00,
//ERROR CODE                     IG_TOO_MUCH_BARCODE_DATA          ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x00,0x52, 0x52, 0x22, 0x00,
//ERROR CODE                     TOO_MANY_ENCODING_SWITCHES        ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x00,0x47, 0x47, 0x22, 0x00,
//ERROR CODE                     IG_INVALID_BARCODE_DATA           ERROR_CLASS_IG_BC         NO_OI_INFO             /*  */
/**** <WARNING> Break In Sequence ****/
0x00,0x53, 0x53, 0x22, 0x00,
/*
Error Table:  IPSDBLLookup
DEVICECODE     POSTEDCODE    CLASSCODE    SPECIALTY
---------------------------------------------------------- */

//ERROR CODE                     IPSDBL_NO_ERROR                   ERROR_CLASS_IPSD_BL       NO_OI_INFO             /* No error */
0x0, 0x00, 0x3A, 0x00,
//ERROR CODE                     IPSDBLERR_FLPROGFAIL_HW           ERROR_CLASS_IPSD_BL       NO_OI_INFO             /* Flash Programming failed due to hardware reported error. */
0x1, 0x01, 0x3A, 0x00,
//ERROR CODE                     IPSDBLERR_FLPROGRAIL_SWTO         ERROR_CLASS_IPSD_BL       NO_OI_INFO             /* Flash Programming failed due to software timeout. */
0x2, 0x02, 0x3A, 0x00,
//ERROR CODE                     IPSDBLERR_UNKNOWN_CMD             ERROR_CLASS_IPSD_BL       NO_OI_INFO             /* Valid Boot Loader signature, but unrecognized command. */
0x3, 0x03, 0x3A, 0x00,
//ERROR CODE                     IPSDBLERR_INVALID_INPUT_FORMAT    ERROR_CLASS_IPSD_BL       NO_OI_INFO             /* Valid Boot Loader signature, but invalid input format. */
0x4, 0x04, 0x3A, 0x00,
//ERROR CODE                     IPSDBLERR_PROGRAM_VERIFY_FAILED   ERROR_CLASS_IPSD_BL       NO_OI_INFO             /* Program verification failed. */
0x6, 0x05, 0x3A, 0x00,
//ERROR CODE                     IPSDBLERR_TOO_SHORT               ERROR_CLASS_IPSD_BL       NO_OI_INFO             /* Input data less than required minimum length */
0x8, 0x06, 0x3A, 0x00,

/* currOffset = 0x10A2 */
/*
Intertask Reply Maps Index
*/
0x00,0x00,0x10,0xAA,
0x00,0x00,0x10,0xAD,
/* currOffset = 0x10AA */
/*
INTERTASK REPLY MAPS

Used to map the variables that are to be copied into the data array appended to an intertask reply.

Format: column 1 = size of field, columns 2 and 3 = ID of variable to be copied.
A size of 0 in column 1 delimits the end of map.
*/
/* MAPNOTUSED */
0x00, 0x00,0x00,  /* BAD_VARIABLE_NAME */

/* BOBREPMOSTMSGS */
0x00, 0x00,0x00,  /* BAD_VARIABLE_NAME */


/* currOffset = 0x10B0 */
/*
Script Directory (rMsgStrategy)
Note that the script offsets are filled in by fnInitTeaLeaves for the developer's tealeaves.  The directory must
be in the same sequence as the enumerated script IDs (intertask message IDs).  Refer to bob.h for the enumeration.
Record; Format:
scriptSize  scriptOffset  interTaskReplyMapID  IntertaskReplyID */
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*DUMMY_SCRIPT*/
0x00,0x01, 0x00,0x00,0x14,0x61, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_TEST*/
0x00,0x01, 0x00,0x00,0x14,0x68, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_INIT_SCM*/
0x00,0x02, 0x00,0x00,0x14,0x6F, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_INIT_VAULT*/
0x00,0x01, 0x00,0x00,0x14,0x7D, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_INIT_IPSD*/
0x00,0x0B, 0x00,0x00,0x14,0x84, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_LOGIN*/
0x00,0x03, 0x00,0x00,0x14,0xD1, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_INIT_CLOCK*/
0x00,0x03, 0x00,0x00,0x14,0xE6, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_DEBIT*/
0x00,0x07, 0x00,0x00,0x14,0xFB, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_TEST_IPSD1*/
0x00,0x07, 0x00,0x00,0x15,0x2C, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_TEST_IPSD2*/
0x00,0x03, 0x00,0x00,0x15,0x5D, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*BOB_SELECT_IMAGE_REQ*/
0x00,0x01, 0x00,0x00,0x15,0x72, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_GEN_STATIC_IMAGE_RSP, /*BOB_GEN_STATIC_IMAGE_REQ*/
0x00,0x07, 0x00,0x00,0x15,0x79, 0x00,0x01, 0x00,
//LineCnt, offset BOBREPMOSTMSGS, BC_COMMIT_COMPLETE, /*SCM_PREPARE_FOR_MAILRUN*/
0x00,0x01, 0x00,0x00,0x15,0xAA, 0x00,0x01, 0x00,
//LineCnt, offset BOBREPMOSTMSGS, BC_COMMIT_COMPLETE, /*SCM_MAIL_IN_PROGRESS*/
0x00,0x01, 0x00,0x00,0x15,0xB1, 0x00,0x01, 0x00,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_GET_REGS*/
0x00,0x01, 0x00,0x00,0x15,0xB8, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_GET_PSDPARAMS*/
0x00,0x02, 0x00,0x00,0x15,0xBF, 0x00,0x01, 0xFD,
//LineCnt, offset MAPNOTUSED, PVD_REQ_RECORD_ORB, /*GET_PVD_RECORD*/
0x00,0x01, 0x00,0x00,0x15,0xCD, 0x00,0x00, 0x07,
//LineCnt, offset MAPNOTUSED, PVD_RSP_RECORD_REPLY_ORB, /*PVD_RSP_RECORD*/
0x00,0x02, 0x00,0x00,0x15,0xD4, 0x00,0x00, 0x08,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*COMPILE_METER_STATUS*/
0x00,0x01, 0x00,0x00,0x15,0xE2, 0x00,0x01, 0xFD,
//LineCnt, offset MAPNOTUSED, REFUND_RECORD_REPLY_ORB, /*GET_REFUND_RECORD*/
0x00,0x01, 0x00,0x00,0x15,0xE9, 0x00,0x00, 0x09,
//LineCnt, offset MAPNOTUSED, REFUND_RESPONSE_RECORD_REPLY_ORB, /*REFUND_RESPONSE_RECORD*/
0x00,0x06, 0x00,0x00,0x15,0xF0, 0x00,0x00, 0x0A,
//LineCnt, offset MAPNOTUSED, AUDIT_REQ_RECORD_ORB, /*GET_AUDIT_RECORD*/
0x00,0x02, 0x00,0x00,0x16,0x1A, 0x00,0x00, 0x01,
//LineCnt, offset MAPNOTUSED, AUDIT_RSP_RECORD_REPLY_ORB, /*AUDIT_RSP_RECORD*/
0x00,0x02, 0x00,0x00,0x16,0x28, 0x00,0x00, 0x02,
//LineCnt, offset MAPNOTUSED, AUTH_REQ_RECORD_REPLY_ORB, /*AUTH_REQ_RECORD*/
0x00,0x04, 0x00,0x00,0x16,0x36, 0x00,0x00, 0x03,
//LineCnt, offset MAPNOTUSED, POSTAL_CONFIG_RECORD_REPLY_ORB, /*POSTAL_CONFIG_RECORD*/
0x00,0x05, 0x00,0x00,0x16,0x52, 0x00,0x00, 0x06,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_SET_DATE_TIME*/
0x00,0x01, 0x00,0x00,0x16,0x75, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_GET_DATE_TIME*/
0x00,0x01, 0x00,0x00,0x16,0x7C, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_GET_IPSD_CLOCK_OFFSET*/
0x00,0x01, 0x00,0x00,0x16,0x83, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_COMMIT_PARAMS*/
0x00,0x03, 0x00,0x00,0x16,0x8A, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_GEN_PSD_KEY*/
0x00,0x02, 0x00,0x00,0x16,0x9F, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_EDM_IPSD_GEN_PSD_KEY*/
0x00,0x01, 0x00,0x00,0x16,0xAD, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_MASTER_ERASE*/
0x00,0x02, 0x00,0x00,0x16,0xB4, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_EDM_IPSD_MASTER_ERASE*/
0x00,0x01, 0x00,0x00,0x16,0xC2, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_LD_ENCR_KEY*/
0x00,0x02, 0x00,0x00,0x16,0xC9, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_EDM_IPSD_LD_ENCR_KEY*/
0x00,0x01, 0x00,0x00,0x16,0xD7, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_PRECRE_INDI_STD*/
0x00,0x02, 0x00,0x00,0x16,0xDE, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_CREATEINDICIA*/
0x00,0x06, 0x00,0x00,0x16,0xEC, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_COMMIT_TRANSACTION*/
0x00,0x01, 0x00,0x00,0x17,0x16, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_LOAD_TEST_PRINT*/
0x00,0x01, 0x00,0x00,0x17,0x1D, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_LOAD_REPORT*/
0x00,0x01, 0x00,0x00,0x17,0x24, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_GET_REFILL_LOG*/
0x00,0x01, 0x00,0x00,0x17,0x2B, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_MFG_GET_PSD_CHALLENGE_AND_STAT*/
0x00,0x01, 0x00,0x00,0x17,0x32, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_STATIC_INDICIA*/
0x00,0x0B, 0x00,0x00,0x17,0x39, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_GET_RTC*/
0x00,0x01, 0x00,0x00,0x17,0x86, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_REFILL_RECEIPT_RPT*/
0x00,0x04, 0x00,0x00,0x17,0x8D, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_REG_RPT*/
0x00,0x03, 0x00,0x00,0x17,0xA9, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_REFILL_LOG_RPT*/
0x00,0x03, 0x00,0x00,0x17,0xBE, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_CONFIG_RPT*/
0x00,0x02, 0x00,0x00,0x17,0xD3, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_ERROR_RPT*/
0x00,0x02, 0x00,0x00,0x17,0xE1, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_NEXT_INSPECTION_DATE*/
0x00,0x01, 0x00,0x00,0x17,0xEF, 0x00,0x01, 0xFD,
//LineCnt, offset MAPNOTUSED, PSD_KEY_RECORD_REPLY_ORB, /*PSD_KEY_RECORD*/
0x00,0x01, 0x00,0x00,0x17,0xF6, 0x00,0x00, 0x0C,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_PICK_A_ZIPCODE*/
0x00,0x01, 0x00,0x00,0x17,0xFD, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_VERIFY_HASH_SIGNATURE*/
0x00,0x02, 0x00,0x00,0x18,0x04, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_LOGOUT*/
0x00,0x02, 0x00,0x00,0x18,0x12, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_LD_PROV_KEY*/
0x00,0x02, 0x00,0x00,0x18,0x20, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_EDM_IPSD_LD_PROV_KEY*/
0x00,0x01, 0x00,0x00,0x18,0x2E, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_SINGLE_ACCOUNT_RPT*/
0x00,0x02, 0x00,0x00,0x18,0x35, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_MULTI_ACCOUNT_RPT*/
0x00,0x02, 0x00,0x00,0x18,0x43, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_SIMPLE_RPT*/
0x00,0x01, 0x00,0x00,0x18,0x51, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_STATIC_AD_INSC*/
0x00,0x05, 0x00,0x00,0x18,0x58, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_PRE_DEBIT*/
0x00,0x03, 0x00,0x00,0x18,0x7B, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_PRECOMPUTE_R*/
0x00,0x01, 0x00,0x00,0x18,0x90, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_STATIC_PERMIT*/
0x00,0x08, 0x00,0x00,0x18,0x97, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_SET_CURRENT_PERMIT_SELECTION*/
0x00,0x01, 0x00,0x00,0x18,0xCF, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_SET_CURRENT_PERMIT_NAME*/
0x00,0x01, 0x00,0x00,0x18,0xD6, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_STATIC_DATE_STAMP*/
0x00,0x05, 0x00,0x00,0x18,0xDD, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_PERMIT_PRINT*/
0x00,0x01, 0x00,0x00,0x19,0x00, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_INITIALIZE_GERMANY*/
0x00,0x01, 0x00,0x00,0x19,0x07, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_INITIALIZE_STD*/
0x00,0x01, 0x00,0x00,0x19,0x0E, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_GET_GERMAN_PSDPARMS*/
0x00,0x01, 0x00,0x00,0x19,0x15, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_GET_STD_PSDPARMS*/
0x00,0x01, 0x00,0x00,0x19,0x1C, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_STD_CREATE_INDI*/
0x00,0x01, 0x00,0x00,0x19,0x23, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_CREATE_GERMAN_INDI*/
0x00,0x01, 0x00,0x00,0x19,0x2A, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_PRECRE_INDI_GERM*/
0x00,0x02, 0x00,0x00,0x19,0x31, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_NORMAL_PVD_RSP_RECORD*/
0x00,0x03, 0x00,0x00,0x19,0x3F, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_GERMAN_PVD_RSP_RECORD*/
0x00,0x03, 0x00,0x00,0x19,0x54, 0x00,0x01, 0xFD,
//LineCnt, offset MAPNOTUSED, PSD_EDC_NONCE_REPLY_ORB, /*PSD_EDC_NONCE*/
0x00,0x01, 0x00,0x00,0x19,0x69, 0x00,0x00, 0x04,
//LineCnt, offset MAPNOTUSED, PSD_EDC_RECORD_REPLY_ORB, /*PSD_EDC_RECORD*/
0x00,0x01, 0x00,0x00,0x19,0x70, 0x00,0x00, 0x05,
//LineCnt, offset MAPNOTUSED, BOB_RESPONSE, /*PSD_EDC_ENABLE_RECORD*/
0x00,0x01, 0x00,0x00,0x19,0x77, 0x00,0x00, 0xFD,
//LineCnt, offset MAPNOTUSED, BOB_RESPONSE, /*PSD_EDC_DISABLE_RECORD*/
0x00,0x01, 0x00,0x00,0x19,0x7E, 0x00,0x00, 0xFD,
//LineCnt, offset MAPNOTUSED, BOB_RESPONSE, /*PSD_EDC_CANCEL_RECORD*/
0x00,0x01, 0x00,0x00,0x19,0x85, 0x00,0x00, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_STATIC_DATE_STAMP_ONLY*/
0x00,0x04, 0x00,0x00,0x19,0x8C, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_DATE_STAMP_PRINT*/
0x00,0x01, 0x00,0x00,0x19,0xA8, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_UPDATE_INSCRIPTION_SELECTION*/
0x00,0x01, 0x00,0x00,0x19,0xAF, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_CREATE_DEV_AUDIT_NO_PC*/
0x00,0x01, 0x00,0x00,0x19,0xB6, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_CREATE_DEV_AUDIT_W_PC*/
0x00,0x01, 0x00,0x00,0x19,0xBD, 0x00,0x01, 0xFD,
//LineCnt, offset MAPNOTUSED, BOB_RESPONSE, /*SCRIPT_PRINT_COMPLETE*/
0x00,0x01, 0x00,0x00,0x19,0xC4, 0x00,0x00, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_GET_INDICIA_PRINT_FLAGS*/
0x00,0x01, 0x00,0x00,0x19,0xCB, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_SET_TEXTMSG_SELECTION*/
0x00,0x01, 0x00,0x00,0x19,0xD2, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_PRECRE_INDI_WITH_MSG_TYPE*/
0x00,0x02, 0x00,0x00,0x19,0xD9, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_CREATE_INDI_WITH_MSG_TYPE*/
0x00,0x01, 0x00,0x00,0x19,0xE7, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_COMMIT_FLEX_DEBIT*/
0x00,0x01, 0x00,0x00,0x19,0xEE, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_GET_REGS_ALL*/
0x00,0x02, 0x00,0x00,0x19,0xF5, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_GET_ALL_REGS_PRE9*/
0x00,0x02, 0x00,0x00,0x1A,0x03, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_GET_ALL_REGS_SOMEHOW*/
0x00,0x01, 0x00,0x00,0x1A,0x11, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_PRECRE_INDI_FLEX_DEBIT*/
0x00,0x02, 0x00,0x00,0x1A,0x18, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_GET_PIECE_COUNT*/
0x00,0x01, 0x00,0x00,0x1A,0x26, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_GBL_PASSTHROUGH_EDM*/
0x00,0x02, 0x00,0x00,0x1A,0x2D, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_EDM_IPSD_RUN_SELF_TESTS*/
0x00,0x01, 0x00,0x00,0x1A,0x3B, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_IPSD_MANUFACTURE*/
0x00,0x03, 0x00,0x00,0x1A,0x42, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_EDM_IPSD_MANUFACTURE*/
0x00,0x01, 0x00,0x00,0x1A,0x57, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_CHECK_GEMINI*/
0x00,0x05, 0x00,0x00,0x1A,0x5E, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_UPDATE_AD_SELECTION*/
0x00,0x01, 0x00,0x00,0x1A,0x81, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*SCRIPT_UPDATE_INDICIA_TO_PRINT*/
0x00,0x02, 0x00,0x00,0x1A,0x88, 0x00,0x01, 0xFD,
//LineCnt, offset BOBREPMOSTMSGS, BOB_RESPONSE, /*HEAD_MFG_DEBUG*/
0x00,0x01, 0x00,0x00,0x1A,0x96, 0x00,0x01, 0xFD,

/* currOffset = 0x1461 */
/*
Scripts

MessageID,  DEVorEXCEPT,  PRE_MSGID, POST_MSGID,
MessageID,  DEVorEXCEPT,  PRE_MSGID, POST_MSGID
-----------------------------------------------
*/
/* DUMMY_SCRIPT */
//DUMMY_SCRIPT, TASKONLY, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x00, 0x00,0x01,

/* SCRIPT_TEST */
//DUMMY_SCRIPT, SUBSCRIPT, fnTESTIPSDPRETASK, fnTESTIPSDPOSTTASK
0x00,0x00, 0x06, 0x00,0x04, 0x00,0x05,

/* SCRIPT_INIT_SCM */
//SCRIPT_INIT_VAULT, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x03, 0x06, 0x00,0x00, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnINITIMAGGEN, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x0F, 0x00,0x01,

/* SCRIPT_INIT_VAULT */
//SCRIPT_INIT_IPSD, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x04, 0x06, 0x00,0x00, 0x00,0x01,

/* SCRIPT_INIT_IPSD */
//DUMMY_SCRIPT, TASKONLY, fnIPSDSETUPPOWERUP, fnIPSDSTOREPOWERUP
0x00,0x00, 0x08, 0x00,0x02, 0x00,0x03,
//SCRIPT_CHECK_GEMINI, SUBSCRIPT, fnPreCheckIPSDType, fnPostCheckGemini
0x00,0x65, 0x06, 0x00,0x5D, 0x00,0x67,
//IPSD_GET_FIRMWARE_VER, IPSD, fnSTDPREMSGTASK, fnCONVERTFIRMWAREVERSION
0x00,0x0C, 0x09, 0x00,0x00, 0x00,0x11,
//IPSD_GET_STATE, IPSD, fnSTDPREMSGTASK, fnPOSTGETSTATECHECKMFG
0x00,0x02, 0x09, 0x00,0x00, 0x00,0x21,
//SCRIPT_IPSD_INIT_CLOCK, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x06, 0x06, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_GET_REGS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0E, 0x06, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnANDYOUSHALLBECALLED
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x36,
//DUMMY_SCRIPT, TASKONLY, fnInitShadowLogs, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x59, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnVALIDATESHADOWLOGS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x4D, 0x00,0x01,
//IPSD_GET_STATE, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x02, 0x09, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_LOGIN, REDIRECT, fnIPSDCHECKLOGINSTATE, fnSTDPOSTMSGTASK
0x00,0x05, 0x07, 0x00,0x06, 0x00,0x01,

/* SCRIPT_IPSD_LOGIN */
//IPSD_GET_CHALLENGE, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x07, 0x09, 0x00,0x00, 0x00,0x01,
//IPSD_USER_LOGIN, IPSD, fnIPSDLOGINPRE, fnSTDPOSTMSGTASK
0x00,0x08, 0x09, 0x00,0x07, 0x00,0x01,
//IPSD_GET_STATE, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x02, 0x09, 0x00,0x00, 0x00,0x01,

/* SCRIPT_IPSD_INIT_CLOCK */
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x01,
//IPSD_GET_RTC, IPSD, fnSTDPREMSGTASK, fnIPSDPOSTCLOCKINIT
0x00,0x03, 0x09, 0x00,0x00, 0x00,0x08,
//IPSD_SET_GMT_OFFSET, IPSD, fnIPSDCALCGMTOFFSET, fnSTDPOSTMSGTASK
0x00,0x04, 0x09, 0x00,0x09, 0x00,0x01,

/* SCRIPT_IPSD_DEBIT */
//DUMMY_SCRIPT, TASKONLY, fnCHECKINDICIACURRENCY, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x3C, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnCHECKACCOUNTFULL, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x35, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnGetVasBarcodes, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x5C, 0x00,0x01,
//SCRIPT_IPSD_COMMIT_TRANSACTION, REDIRECT, fnSWITCHDEBITSCRIPT, fnBOBSHADOWDEBIT
0x00,0x25, 0x07, 0x00,0x3A, 0x00,0x34,
//DUMMY_SCRIPT, TASKONLY, fnLOADDYNAMICVARS, fnBobEarlyReply
0x00,0x00, 0x08, 0x00,0x10, 0x00,0x52,
//SCRIPT_IPSD_GET_REGS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0E, 0x06, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnTESTIPSDPRETASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x06, 0x00,0x04, 0x00,0x01,

/* SCRIPT_TEST_IPSD1 */
//DUMMY_SCRIPT, TASKONLY, fnIPSDSETUPPOWERUP, fnIPSDSTOREPOWERUP
0x00,0x00, 0x08, 0x00,0x02, 0x00,0x03,
//IPSD_GET_RTC, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x03, 0x09, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_GET_REGS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0E, 0x06, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x01,
//IPSD_SPEED, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0D, 0x09, 0x00,0x00, 0x00,0x01,
//IPSD_GET_FREE_RAM, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0E, 0x09, 0x00,0x00, 0x00,0x01,
//IPSD_GET_POR_COUNT, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x09, 0x00,0x00, 0x00,0x01,

/* SCRIPT_TEST_IPSD2 */
//SCRIPT_IPSD_LOGOUT, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x35, 0x06, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_LOGIN, REDIRECT, fnIPSDCHECKLOGINSTATE, fnSTDPOSTMSGTASK
0x00,0x05, 0x07, 0x00,0x06, 0x00,0x01,
//IPSD_GET_STATE, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x02, 0x09, 0x00,0x00, 0x00,0x01,

/* BOB_SELECT_IMAGE_REQ */
//DUMMY_SCRIPT, TASKONLY, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x00, 0x00,0x01,

/* BOB_GEN_STATIC_IMAGE_REQ */
//DUMMY_SCRIPT, TASKONLY, fnCLEARCMREPLYSTATUS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x2D, 0x00,0x01,
//SCRIPT_IPSD_GET_REGS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0E, 0x06, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x01,
//DUMMY_SCRIPT, REDIRECT, fnGENSTATICIMAGEREQSCRIPTSELECT, fnSTDPOSTMSGTASK
0x00,0x00, 0x07, 0x00,0x1F, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnBobClearBC, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x54, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLoadKindofDynamicVars, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x53, 0x00,0x01,
//SCRIPT_IPSD_PRECOMPUTE_R, SUBSCRIPT, fnTESTIPSDPRETASK, fnSTDPOSTMSGTASK
0x00,0x3D, 0x06, 0x00,0x04, 0x00,0x01,

/* SCM_PREPARE_FOR_MAILRUN */
//SCRIPT_IPSD_PRE_DEBIT, SUBSCRIPT, fnISDEBITMODE, fnSTDPOSTMSGTASK
0x00,0x3C, 0x06, 0x00,0x0C, 0x00,0x01,

/* SCM_MAIL_IN_PROGRESS */
//DUMMY_SCRIPT, REDIRECT, fnGETPRINTPREP, fnSTDPOSTMSGTASK
0x00,0x00, 0x07, 0x00,0x0D, 0x00,0x01,

/* SCRIPT_IPSD_GET_REGS */
//IPSD_GET_MODULE_STATUS, IPSD, fnSTDPREMSGTASK, fnCOMPUTECONTROLSUM
0x00,0x05, 0x09, 0x00,0x00, 0x00,0x0E,

/* SCRIPT_IPSD_GET_PSDPARAMS */
//SCRIPT_IPSD_GET_STD_PSDPARMS, SUBSCRIPT, fnISNOTGERMAN, fnSTDPOSTMSGTASK
0x00,0x46, 0x06, 0x00,0x45, 0x00,0x01,
//SCRIPT_IPSD_GET_GERMAN_PSDPARMS, SUBSCRIPT, fnISGERMAN, fnSTDPOSTMSGTASK
0x00,0x45, 0x06, 0x00,0x44, 0x00,0x01,

/* GET_PVD_RECORD */
//IPSD_GENERATE_PVD_REQUEST, IPSD, fnIPSDGETREPLYXFERCTRLSTRUCTPTR, fnREPLYIPSDREDIRECT
0x00,0x14, 0x09, 0x00,0x15, 0x00,0x13,

/* PVD_RSP_RECORD */
//DUMMY_SCRIPT, TASKONLY, fnIPSDPACKETIZE2IBUTTONP20, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x30, 0x00,0x01,
//DUMMY_SCRIPT, REDIRECT, fnSWITCHPVDSCRIPT, fnSTDPOSTMSGTASK
0x00,0x00, 0x07, 0x00,0x46, 0x00,0x01,

/* COMPILE_METER_STATUS */
//DUMMY_SCRIPT, TASKONLY, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x00, 0x00,0x01,

/* GET_REFUND_RECORD */
//IPSD_GENERATE_PVR_REQUEST, IPSD, fnIPSDGETREPLYXFERCTRLSTRUCTPTR, fnSTDPOSTMSGTASK
0x00,0x16, 0x09, 0x00,0x15, 0x00,0x01,

/* REFUND_RESPONSE_RECORD */
//DUMMY_SCRIPT, TASKONLY, fnIPSDPACKETIZE2IBUTTONP20, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x30, 0x00,0x01,
//IPSD_PROCESS_PVR_MESSAGE, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETSALLOWLPE
0x00,0x17, 0x09, 0x00,0x14, 0x00,0x2A,
//IPSD_GET_STATE, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x02, 0x09, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_GET_REGS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0E, 0x06, 0x00,0x00, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnBOBSHADOWREFUND, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x4F, 0x00,0x01,
//SCRIPT_IPSD_LOGIN, REDIRECT, fnIPSDCHECKLOGINSTATE, fnSTDPOSTMSGTASK
0x00,0x05, 0x07, 0x00,0x06, 0x00,0x01,

/* GET_AUDIT_RECORD */
//DUMMY_SCRIPT, TASKONLY, fnVALIDATESHADOWLOGS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x4D, 0x00,0x01,
//SCRIPT_CREATE_DEV_AUDIT_NO_PC, REDIRECT, fnSWITCHAUDITSCRIPT, fnSTDPOSTMSGTASK
0x00,0x54, 0x07, 0x00,0x51, 0x00,0x01,

/* AUDIT_RSP_RECORD */
//DUMMY_SCRIPT, TASKONLY, fnIPSDPACKETIZE2IBUTTONP20, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x30, 0x00,0x01,
//IPSD_PROCESS_AUDIT_RESPONSE, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x19, 0x09, 0x00,0x14, 0x00,0x12,

/* AUTH_REQ_RECORD */
//DUMMY_SCRIPT, TASKONLY, fnIPSDPACKETIZE2IBUTTONP20, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x30, 0x00,0x01,
//IPSD_AUTHORIZE, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x11, 0x09, 0x00,0x14, 0x00,0x12,
//IPSD_GET_STATE, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x02, 0x09, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_LOGIN, REDIRECT, fnIPSDCHECKLOGINSTATE, fnSTDPOSTMSGTASK
0x00,0x05, 0x07, 0x00,0x06, 0x00,0x01,

/* POSTAL_CONFIG_RECORD */
//DUMMY_SCRIPT, TASKONLY, fnIPSDPACKETIZE2IBUTTONP20, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x30, 0x00,0x01,
//IPSD_LOAD_POSTAL_CFG_DATA, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x10, 0x09, 0x00,0x14, 0x00,0x12,
//IPSD_GET_STATE, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x02, 0x09, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_LOGIN, REDIRECT, fnIPSDCHECKLOGINSTATE, fnSTDPOSTMSGTASK
0x00,0x05, 0x07, 0x00,0x06, 0x00,0x01,

/* SCRIPT_IPSD_SET_DATE_TIME */
//IPSD_GET_RTC, IPSD, fnSTDPREMSGTASK, fnIPSDSETDATETIME
0x00,0x03, 0x09, 0x00,0x00, 0x00,0x16,

/* SCRIPT_IPSD_GET_DATE_TIME */
//IPSD_GET_RTC, IPSD, fnSTDPREMSGTASK, fnIPSDGETDATETIME
0x00,0x03, 0x09, 0x00,0x00, 0x00,0x17,

/* SCRIPT_GET_IPSD_CLOCK_OFFSET */
//IPSD_GET_RTC, IPSD, fnSTDPREMSGTASK, fnIPSDGETCLOCKOFFSET
0x00,0x03, 0x09, 0x00,0x00, 0x00,0x18,

/* SCRIPT_IPSD_COMMIT_PARAMS */
//DUMMY_SCRIPT, TASKONLY, fnIPSDGATHERINIT, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x1A, 0x00,0x01,
//SCRIPT_INITIALIZE_STD, SUBSCRIPT, fnISNOTGERMAN, fnSTDPOSTMSGTASK
0x00,0x44, 0x06, 0x00,0x45, 0x00,0x01,
//SCRIPT_INITIALIZE_GERMANY, SUBSCRIPT, fnISGERMAN, fnSTDPOSTMSGTASK
0x00,0x43, 0x06, 0x00,0x44, 0x00,0x01,

/* SCRIPT_IPSD_GEN_PSD_KEY */
//DUMMY_SCRIPT, TASKONLY, fnIPSDPACKETIZE2IBUTTONP20, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x30, 0x00,0x01,
//IPSD_GENERATE_PSD_KEY, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x1A, 0x09, 0x00,0x14, 0x00,0x12,

/* SCRIPT_EDM_IPSD_GEN_PSD_KEY */
//SCRIPT_IPSD_GEN_PSD_KEY, SUBSCRIPT, fnLOADEDMXFRCTRLPTR, fnSTDPOSTMSGTASK
0x00,0x1D, 0x06, 0x00,0x19, 0x00,0x01,

/* SCRIPT_IPSD_MASTER_ERASE */
//DUMMY_SCRIPT, TASKONLY, fnIPSDPACKETIZE2IBUTTONP20, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x30, 0x00,0x01,
//IPSD_MASTER_ERASE, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x12, 0x09, 0x00,0x14, 0x00,0x12,

/* SCRIPT_EDM_IPSD_MASTER_ERASE */
//SCRIPT_IPSD_MASTER_ERASE, SUBSCRIPT, fnLOADEDMXFRCTRLPTR, fnSTDPOSTMSGTASK
0x00,0x1F, 0x06, 0x00,0x19, 0x00,0x01,

/* SCRIPT_IPSD_LD_ENCR_KEY */
//DUMMY_SCRIPT, TASKONLY, fnIPSDLOADENCRKEY, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x1B, 0x00,0x01,
//IPSD_LD_SECRET_KEY, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x1B, 0x09, 0x00,0x14, 0x00,0x12,

/* SCRIPT_EDM_IPSD_LD_ENCR_KEY */
//SCRIPT_IPSD_LD_ENCR_KEY, SUBSCRIPT, fnLOADEDMXFRCTRLPTR, fnSTDPOSTMSGTASK
0x00,0x21, 0x06, 0x00,0x19, 0x00,0x01,

/* SCRIPT_IPSD_PRECRE_INDI_STD */
//DUMMY_SCRIPT, TASKONLY, fnIPSDLOADCREATEINDICIADATA, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x0A, 0x00,0x01,
//IPSD_PRECREATE_INDICIUM, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x09, 0x09, 0x00,0x14, 0x00,0x12,

/* SCRIPT_IPSD_CREATEINDICIA */
//DUMMY_SCRIPT, TASKONLY, fnIPSDLOADCREATEINDICIADATA, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x0A, 0x00,0x01,
//SCRIPT_IPSD_STD_CREATE_INDI, SUBSCRIPT, fnISNOTGERMAN, fnSTDPOSTMSGTASK
0x00,0x47, 0x06, 0x00,0x45, 0x00,0x01,
//SCRIPT_IPSD_CREATE_GERMAN_INDI, SUBSCRIPT, fnISGERMAN, fnSTDPOSTMSGTASK
0x00,0x48, 0x06, 0x00,0x44, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnSTDPREMSGTASK, fnUPDATETHEBATCH
0x00,0x00, 0x08, 0x00,0x00, 0x00,0x1E,
//SCRIPT_IPSD_GET_REGS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0E, 0x06, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x01,

/* SCRIPT_IPSD_COMMIT_TRANSACTION */
//IPSD_COMMIT_TRANSACTION, IPSD, fnSTDPREMSGTASK, fnUpdateTheBatchAndRegs
0x00,0x0A, 0x09, 0x00,0x00, 0x00,0x55,

/* SCRIPT_LOAD_TEST_PRINT */
//DUMMY_SCRIPT, TASKONLY, fnLOADTESTPRINT, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x1C, 0x00,0x01,

/* SCRIPT_LOAD_REPORT */
//DUMMY_SCRIPT, REDIRECT, fnSWITCHREPORTSCRIPT, fnSTDPOSTMSGTASK
0x00,0x00, 0x07, 0x00,0x2C, 0x00,0x01,

/* SCRIPT_IPSD_GET_REFILL_LOG */
//IPSD_GET_REFILL_LOG, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x1C, 0x09, 0x00,0x00, 0x00,0x01,

/* SCRIPT_MFG_GET_PSD_CHALLENGE_AND_STAT */
//IPSD_GET_CHALLENGE, IPSD, fnSTDPREMSGTASK, fnPOSTMFGGETPSDCHALLENGEANDSTAT
0x00,0x07, 0x09, 0x00,0x00, 0x00,0x20,

/* SCRIPT_STATIC_INDICIA */
//DUMMY_SCRIPT, TASKONLY, fnSETNORMALORLOWINDICIA, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x23, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnSETLEADINGIMAGE, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x4B, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnSETLAGGINGIMAGE, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x4C, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnDETERMINEINSCRSELECTION, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x50, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnDetermineAdSelection, fnSTDPREMSGTASK
0x00,0x00, 0x08, 0x00,0x68, 0x00,0x00,
//DUMMY_SCRIPT, TASKONLY, fnLOADMAILSPECS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x22, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnSETIGINDICIATYPE, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x24, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnSETIGAD, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x25, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnSETIGINSCR, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x26, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADINDICIA, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x27, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnINDICIAQUACKERS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x28, 0x00,0x01,

/* SCRIPT_IPSD_GET_RTC */
//IPSD_GET_RTC, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x03, 0x09, 0x00,0x00, 0x00,0x01,

/* SCRIPT_REFILL_RECEIPT_RPT */
//SCRIPT_IPSD_GET_REGS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0E, 0x06, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_GET_REFILL_LOG, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x28, 0x06, 0x00,0x00, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADREPORT, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x1D, 0x00,0x01,

/* SCRIPT_REG_RPT */
//SCRIPT_IPSD_GET_REGS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0E, 0x06, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADREPORT, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x1D, 0x00,0x01,

/* SCRIPT_REFILL_LOG_RPT */
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_GET_REFILL_LOG, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x28, 0x06, 0x00,0x00, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADREPORT, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x1D, 0x00,0x01,

/* SCRIPT_CONFIG_RPT */
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADREPORT, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x1D, 0x00,0x01,

/* SCRIPT_ERROR_RPT */
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADREPORT, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x1D, 0x00,0x01,

/* SCRIPT_NEXT_INSPECTION_DATE */
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnPOSTCONVERTWATCHDOGTIMER
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x2F,

/* PSD_KEY_RECORD */
//SCRIPT_IPSD_GEN_PSD_KEY, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x1D, 0x06, 0x00,0x00, 0x00,0x01,

/* SCRIPT_PICK_A_ZIPCODE */
//DUMMY_SCRIPT, TASKONLY, fnPICKAZIPCODE, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x31, 0x00,0x01,

/* SCRIPT_IPSD_VERIFY_HASH_SIGNATURE */
//DUMMY_SCRIPT, TASKONLY, fnPREIPSDVERIFYHASHSIGNATURE, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x32, 0x00,0x01,
//IPSD_VERIFY_HASH_SIGNATURE, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x1E, 0x09, 0x00,0x14, 0x00,0x12,

/* SCRIPT_IPSD_LOGOUT */
//IPSD_USER_LOGOUT, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x1D, 0x09, 0x00,0x00, 0x00,0x01,
//IPSD_GET_STATE, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x02, 0x09, 0x00,0x00, 0x00,0x01,

/* SCRIPT_IPSD_LD_PROV_KEY */
//DUMMY_SCRIPT, TASKONLY, fnIPSDPACKETIZE2IBUTTONP20, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x30, 0x00,0x01,
//IPSD_LOAD_PROVIDER_KEY, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x1F, 0x09, 0x00,0x14, 0x00,0x12,

/* SCRIPT_EDM_IPSD_LD_PROV_KEY */
//SCRIPT_IPSD_LD_PROV_KEY, SUBSCRIPT, fnLOADEDMXFRCTRLPTR, fnSTDPOSTMSGTASK
0x00,0x36, 0x06, 0x00,0x19, 0x00,0x01,

/* SCRIPT_SINGLE_ACCOUNT_RPT */
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADREPORT, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x1D, 0x00,0x01,

/* SCRIPT_MULTI_ACCOUNT_RPT */
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADREPORT, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x1D, 0x00,0x01,

/* SCRIPT_SIMPLE_RPT */
//DUMMY_SCRIPT, TASKONLY, fnLOADREPORT, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x1D, 0x00,0x01,

/* SCRIPT_STATIC_AD_INSC */
//DUMMY_SCRIPT, TASKONLY, fnDetermineAdSelection, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x68, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADMAILSPECS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x22, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnSETIGAD, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x25, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnSETIGINSCRNONE, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x49, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADADINSCONLY, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x38, 0x00,0x01,

/* SCRIPT_IPSD_PRE_DEBIT */
//DUMMY_SCRIPT, TASKONLY, fnVALIDATESHADOWLOGS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x4D, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnCHECKPCENDOFLIFE, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x4E, 0x00,0x01,
//SCRIPT_IPSD_PRECRE_INDI_STD, REDIRECT, fnSWITCHPREDEBITSCRIPT, fnSTDPOSTMSGTASK
0x00,0x23, 0x07, 0x00,0x39, 0x00,0x01,

/* SCRIPT_IPSD_PRECOMPUTE_R */
//IPSD_PRECOMPUTE_R, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x20, 0x09, 0x00,0x00, 0x00,0x01,

/* SCRIPT_STATIC_PERMIT */
//DUMMY_SCRIPT, TASKONLY, fnDETERMINEINSCRSELECTION, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x50, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnDetermineAdSelection, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x68, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADMAILSPECS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x22, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnSETIGPERMITTYPE, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x3E, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnSETIGAD, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x25, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnSETIGINSCR, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x26, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADPERMIT, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x3D, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnPERMITQUACKERS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x42, 0x00,0x01,

/* SCRIPT_SET_CURRENT_PERMIT_SELECTION */
//DUMMY_SCRIPT, TASKONLY, fnSETCURRENTPERMITNAME, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x3F, 0x00,0x01,

/* SCRIPT_SET_CURRENT_PERMIT_NAME */
//DUMMY_SCRIPT, TASKONLY, fnSETCURRENTPERMITSELECTION, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x40, 0x00,0x01,

/* SCRIPT_STATIC_DATE_STAMP */
//DUMMY_SCRIPT, TASKONLY, fnDetermineAdSelection, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x68, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADMAILSPECS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x22, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnSETIGAD, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x25, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADDATETIME, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x41, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnDATETIMEQUACKERS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x43, 0x00,0x01,

/* SCRIPT_PERMIT_PRINT */
//DUMMY_SCRIPT, TASKONLY, fnLOADDYNAMICVARS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x10, 0x00,0x01,

/* SCRIPT_INITIALIZE_GERMANY */
//IPSD_GERMAN_INITIALIZE, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x21, 0x09, 0x00,0x14, 0x00,0x12,

/* SCRIPT_INITIALIZE_STD */
//IPSD_INITIALIZE, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x01, 0x09, 0x00,0x14, 0x00,0x12,

/* SCRIPT_IPSD_GET_GERMAN_PSDPARMS */
//IPSD_GERMAN_GET_PSD_PARAMS, IPSD, fnSTDPREMSGTASK, fnPOSTIPSDGETPARAMLIST
0x00,0x23, 0x09, 0x00,0x00, 0x00,0x0B,

/* SCRIPT_IPSD_GET_STD_PSDPARMS */
//IPSD_GET_PSD_PARAMS, IPSD, fnSTDPREMSGTASK, fnPOSTIPSDGETPARAMLIST
0x00,0x06, 0x09, 0x00,0x00, 0x00,0x0B,

/* SCRIPT_IPSD_STD_CREATE_INDI */
//IPSD_CREATE_INDICIUM, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x0B, 0x09, 0x00,0x14, 0x00,0x12,

/* SCRIPT_IPSD_CREATE_GERMAN_INDI */
//IPSD_GERMAN_CREATE_INDICIUM, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x25, 0x09, 0x00,0x14, 0x00,0x12,

/* SCRIPT_IPSD_PRECRE_INDI_GERM */
//DUMMY_SCRIPT, TASKONLY, fnIPSDLOADCREATEINDICIADATA, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x0A, 0x00,0x01,
//IPSD_GERMAN_PRECREATE_INDICIUM, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x24, 0x09, 0x00,0x14, 0x00,0x12,

/* SCRIPT_NORMAL_PVD_RSP_RECORD */
//IPSD_PROCESS_PVD_MESSAGE, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x15, 0x09, 0x00,0x14, 0x00,0x12,
//SCRIPT_IPSD_GET_REGS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0E, 0x06, 0x00,0x00, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnBOBSHADOWREFILL, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x33, 0x00,0x01,

/* SCRIPT_GERMAN_PVD_RSP_RECORD */
//IPSD_GERMAN_PROCESS_PVD_MESSAGE, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x22, 0x09, 0x00,0x14, 0x00,0x12,
//SCRIPT_IPSD_GET_REGS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0E, 0x06, 0x00,0x00, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnBOBSHADOWREFILL, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x33, 0x00,0x01,

/* PSD_EDC_NONCE */
//IPSD_GET_CHALLENGE, IPSD, fnSTDPREMSGTASK, fnXFERPSDNONCE2XFERCONTSTRUCT
0x00,0x07, 0x09, 0x00,0x00, 0x00,0x47,

/* PSD_EDC_RECORD */
//DUMMY_SCRIPT, REDIRECT, fnCHOOSEEDCRECORD, fnSTDPOSTMSGTASK
0x00,0x00, 0x07, 0x00,0x48, 0x00,0x01,

/* PSD_EDC_ENABLE_RECORD */
//IPSD_ENABLE, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x27, 0x09, 0x00,0x00, 0x00,0x01,

/* PSD_EDC_DISABLE_RECORD */
//IPSD_DISABLE, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x28, 0x09, 0x00,0x00, 0x00,0x01,

/* PSD_EDC_CANCEL_RECORD */
//DUMMY_SCRIPT, TASKONLY, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x00, 0x00,0x01,

/* SCRIPT_STATIC_DATE_STAMP_ONLY */
//DUMMY_SCRIPT, TASKONLY, fnLOADMAILSPECS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x22, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnSETIGADNONE, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x4A, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnLOADDATETIME, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x41, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnDATETIMEQUACKERS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x43, 0x00,0x01,

/* SCRIPT_DATE_STAMP_PRINT */
//DUMMY_SCRIPT, TASKONLY, fnLOADDYNAMICVARS, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x10, 0x00,0x01,

/* SCRIPT_UPDATE_INSCRIPTION_SELECTION */
//DUMMY_SCRIPT, TASKONLY, fnDETERMINEINSCRSELECTION, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x50, 0x00,0x01,

/* SCRIPT_CREATE_DEV_AUDIT_NO_PC */
//IPSD_CREATE_DEVICE_AUDIT_MSG, IPSD, fnIPSDGETREPLYXFERCTRLSTRUCTPTR, fnSTDPOSTMSGTASK
0x00,0x18, 0x09, 0x00,0x15, 0x00,0x01,

/* SCRIPT_CREATE_DEV_AUDIT_W_PC */
//IPSD_CREATE_DEVICE_AUDIT_WPC_MSG, IPSD, fnIPSDGETREPLYXFERCTRLSTRUCTPTR, fnSTDPOSTMSGTASK
0x00,0x29, 0x09, 0x00,0x15, 0x00,0x01,

/* SCRIPT_PRINT_COMPLETE */
//DUMMY_SCRIPT, TASKONLY, fnBobPrintComplete, fnTESTIPSDPOSTTASK
0x00,0x00, 0x08, 0x00,0x56, 0x00,0x05,

/* SCRIPT_GET_INDICIA_PRINT_FLAGS */
//DUMMY_SCRIPT, TASKONLY, fnCheckIndiciaQuackers, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x5A, 0x00,0x01,

/* SCRIPT_SET_TEXTMSG_SELECTION */
//DUMMY_SCRIPT, TASKONLY, fnSetIGText, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x5B, 0x00,0x01,

/* SCRIPT_IPSD_PRECRE_INDI_WITH_MSG_TYPE */
//DUMMY_SCRIPT, TASKONLY, fnIPSDLOADCREATEINDICIADATA, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x0A, 0x00,0x01,
//IPSD_PRECREATE_INDICIUM_WITH_MSG_TYPE, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x2D, 0x09, 0x00,0x14, 0x00,0x12,

/* SCRIPT_IPSD_CREATE_INDI_WITH_MSG_TYPE */
//IPSD_CREATE_INDICIUM_WITH_MSG_TYPE, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x2C, 0x09, 0x00,0x14, 0x00,0x12,

/* SCRIPT_IPSD_COMMIT_FLEX_DEBIT */
//IPSD_COMMIT_FLEX_DEBIT, IPSD, fnSTDPREMSGTASK, fnUpdateTheBatchAndRegs
0x00,0x30, 0x09, 0x00,0x00, 0x00,0x55,

/* SCRIPT_IPSD_GET_REGS_ALL */
//IPSD_GET_MODULE_STATUS_ALL, IPSD, fnSTDPREMSGTASK, fnCOMPUTECONTROLSUM
0x00,0x31, 0x09, 0x00,0x00, 0x00,0x0E,
//DUMMY_SCRIPT, TASKONLY, fnSTDPREMSGTASK, fnComputeDerivedPieceCount
0x00,0x00, 0x08, 0x00,0x00, 0x00,0x58,

/* SCRIPT_IPSD_GET_ALL_REGS_PRE9 */
//SCRIPT_IPSD_GET_REGS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0E, 0x06, 0x00,0x00, 0x00,0x01,
//SCRIPT_IPSD_GET_PSDPARAMS, SUBSCRIPT, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x0F, 0x06, 0x00,0x00, 0x00,0x01,

/* SCRIPT_IPSD_GET_ALL_REGS_SOMEHOW */
//SCRIPT_IPSD_GET_ALL_REGS_PRE9, REDIRECT, fnSwitchGetAllRegsScript, fnSTDPOSTMSGTASK
0x00,0x5D, 0x07, 0x00,0x57, 0x00,0x01,

/* SCRIPT_IPSD_PRECRE_INDI_FLEX_DEBIT */
//DUMMY_SCRIPT, TASKONLY, fnIPSDLOADCREATEINDICIADATA, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x0A, 0x00,0x01,
//IPSD_PRECREATE_INDICIUM_FLEX_DEBIT, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x2F, 0x09, 0x00,0x14, 0x00,0x12,

/* SCRIPT_GET_PIECE_COUNT */
//SCRIPT_IPSD_GET_PSDPARAMS, REDIRECT, fnSwitchGetAllRegsScript, fnSTDPOSTMSGTASK
0x00,0x0F, 0x07, 0x00,0x57, 0x00,0x01,

/* SCRIPT_GBL_PASSTHROUGH_EDM */
//IPSDBL_PASSTHROUGH_MSG, IPSD_BL, fnPreGBLPassthroughFromEDM, fnPostGBLPassthroughFromEDM
0x00,0x33, 0x0A, 0x00,0x5F, 0x00,0x60,
//IPSDBL_GET_DEVICE_STATUS, IPSD_BL, fnIfGetMicroStatusAgain, fnPostCheckGeminiMode
0x00,0x32, 0x0A, 0x00,0x65, 0x00,0x5E,

/* SCRIPT_EDM_IPSD_RUN_SELF_TESTS */
//IPSD_RUN_SELF_TESTS, IPSD, fnPreRunSelfTests, fnPostRunSelfTests
0x00,0x34, 0x09, 0x00,0x61, 0x00,0x62,

/* SCRIPT_IPSD_MANUFACTURE */
//DUMMY_SCRIPT, TASKONLY, fnIPSDPACKETIZE2IBUTTONP20, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x30, 0x00,0x01,
//IPSD_MANUFACTURE, IPSD, fnSENDIPSDPACKET, fnWHILEMOREIPSDPACKETS
0x00,0x36, 0x09, 0x00,0x14, 0x00,0x12,
//IPSD_GET_STATE, IPSD, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x02, 0x09, 0x00,0x00, 0x00,0x01,

/* SCRIPT_EDM_IPSD_MANUFACTURE */
//SCRIPT_IPSD_MANUFACTURE, SUBSCRIPT, fnLOADEDMXFRCTRLPTR, fnSTDPOSTMSGTASK
0x00,0x63, 0x06, 0x00,0x19, 0x00,0x01,

/* SCRIPT_CHECK_GEMINI */
//IPSDBL_GET_DEVICE_STATUS, IPSD_BL, fnSTDPREMSGTASK, fnPostCheckGeminiMode
0x00,0x32, 0x0A, 0x00,0x00, 0x00,0x5E,
//IPSD_GET_FIRMWARE_VER, IPSD, fnSTDPREMSGTASK, fnAlwaysOK
0x00,0x0C, 0x09, 0x00,0x00, 0x00,0x66,
//IPSD_RUN_SELF_TESTS, IPSD, fnPreRunSelfTests, fnPostRunSelfTests
0x00,0x34, 0x09, 0x00,0x61, 0x00,0x62,
//IPSDBL_EXIT_APPMODE, IPSD_BL, fnPreExitIpsdApp, fnPostExitIbtnApp
0x00,0x35, 0x0A, 0x00,0x63, 0x00,0x64,
//IPSDBL_GET_DEVICE_STATUS, IPSD_BL, fnPreCheckIPSDType, fnPostCheckGeminiMode
0x00,0x32, 0x0A, 0x00,0x5D, 0x00,0x5E,

/* SCRIPT_UPDATE_AD_SELECTION */
//DUMMY_SCRIPT, TASKONLY, fnDetermineAdSelection, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x68, 0x00,0x01,

/* SCRIPT_UPDATE_INDICIA_TO_PRINT */
//DUMMY_SCRIPT, TASKONLY, fnSETNORMALORLOWINDICIA, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x23, 0x00,0x01,
//DUMMY_SCRIPT, TASKONLY, fnSETLEADINGIMAGE, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x4B, 0x00,0x01,

/* HEAD_MFG_DEBUG */
//DUMMY_SCRIPT, TASKONLY, fnSTDPREMSGTASK, fnSTDPOSTMSGTASK
0x00,0x00, 0x08, 0x00,0x00, 0x00,0x01,


/* currOffset = 0x1A9D */
/*
Fake Message Index
*/
/* currOffset = 0x1A9D */
/*
Fake Message Tables
*/

};
