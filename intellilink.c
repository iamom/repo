/************************************************************************
*   PROJECT:        Horizon CSD2-3
*   MODULE NAME:    Intellilink.c
*   
*   DESCRIPTION:    This file contains all routines related to communication 
*                   with the PB Data Center.
*                   
* ----------------------------------------------------------------------
*               Copyright (c) 2016 Pitney Bowes Inc.
*                    37 Executive Drive
*                   Danbury, Connecticut  06810
* ----------------------------------------------------------------------
* MODIFICATION HISTORY:

 02-Jun-16 sa002pe on FPHX 02.12 shelton branch
 	Removed the remote logging capabilities.

************************************************************************/
#ifndef INTELLILINK_C
#define INTELLILINK_C

#ifdef __cplusplus
extern  "C" {                              
#endif

#include <string.h>
#include <stdio.h>
#include <networking/target.h>

#include "features.h"
#include "fcntl.h"
#include "nu_networking.h"
#include "networking/externs.h"
#include "..\include\pbos.h"
#include "..\include\osnet.h"
#include "..\include\lcd.h"
#include "..\include\lcdutils.h"
#include "..\include\ossetup.h"
#include "..\include\rftypes.h"
#include "..\include\exception.h"
#include "..\include\urlencode.h"
#include "..\include\xmlform.h"
#include "..\include\xmlparse.h"
#include "..\include\vstack.h"
#include "..\include\remotelog.h"
#include "..\include\oit.h"
#include "..\include\bob.h"
#include "..\include\bobutils.h"
//#include "oioob.h"
#include "cm.h"
#include "oifpinfra.h"              // for fnOITSetRequestedService
#include "..\include\datdict.h"
#include "..\include\scheduler.h"
#include "..\include\fwrapper.h"
#include "..\include\datdict.h"
#include "..\include\download.h"
#include "..\include\ccdapi.h"
#include "..\include\cometphc.h"
#include "..\include\intellilink.h"
#include "..\include\pc_daemon.h"
//TODO - cleanly remove PPP dependent code
//#include "nu_ppp.h"
//#include "ppp\inc\pppurt.h"
//#include "ppp\inc\mdm_defs.h"
//#include "ppp\inc\lcp_defs.h"
//#include "ppp\inc\chp_defs.h"
//#include "ppp\inc\pap_defs.h"
//#include "ppp\inc\ncp_defs.h"
//#include "ppp\inc\ppp_defs.h"
//#include "..\include\abupload.h"
#include "..\include\dcapi.h"
//#include "lan.h"
#include "pbptask.h"
#include "api_utils.h"
#include "nstask.h"
#include "sslinterface.h"
#include "cmos.h"
#include "oit.h"
#include "flashutil.h"
#include "trm.h"
#include "sysdata.h"
#include "sysdatadefines.h"
#include "api_temporary.h"
#include "api_status.h"
#include "aiservic.h"
#include "rateadpt.h"
#include "cmpublic.h"
#include "debugTask.h"
#include "counters/fram_log.h"

#define THISFILE "intellilink.c"

extern NU_MEMORY_POOL  *MEM_Cached;
unsigned long       dnldErr = 0;
//---------------------------------------------------------------------
//      Local Macros:
//---------------------------------------------------------------------

//60000 = 1 minute in msecs
#define ONE_MINUTE_IN_MS    60000    

//---------------------------------------------------------------------
//      External Function Declarations:
//---------------------------------------------------------------------

extern BOOL fnOITSetCurrentServiceDateTime(DATETIME *pDateTime);
extern short delFile(XmlChar *pName, XmlChar *pChars, unsigned long iLen);
extern void registerUrl(CRACKED_URL *originalUrl);
//extern STATUS initNetworkServices();
// From custdat.c;
extern EuroConversion cmosEuroConversion;
//   CTY archive data. Required to support inclusion of CTY xml in the download request.
extern CTY_ARCHIVE_PROPS PctyArchiveRegistry[];
extern SystemErrorLog    CMOSSystemErrorLog;

#ifndef JANUS
extern void fnUSBRaiseExternalTaskPriority(void);
extern void fnUSBRestoreExternalTaskPriority(void);

#else
extern int totalBytesToDownload;   
extern unsigned long   dnldErr;
extern BOOL          fOITModemSignalLost; // 

static unsigned long newPercentComplete = 0;
static unsigned long oldPercentComplete = 0;
static int newFileNum = 0;
#endif
//
extern BOOL     fUploadInProgress;
extern INTERTASK_LOG_ENTRY pIntertaskMsgLog[];
extern NLOG_ENTRY NLOG_Entry_List[NLOG_MAX_ENTRIES];
extern const char *log_names[];

#ifdef __cplusplus
}
#endif

#ifndef JANUS
    #define USINGUSBM
#endif

//---------------------------------------------------------------------
//      External Variable Declarations:
//---------------------------------------------------------------------

// From netw/net/src/nlog.c:
extern INT NLOG_Avail_Index;
extern NLOG_ENTRY NLOG_Entry_List[NLOG_MAX_ENTRIES];

//We use this to signal that the PPP is connected
extern NU_EVENT_GROUP NUC_NET_UP_EVENT;

extern CMOS_Signature CMOSSignature;
extern SetupParams CMOSSetupParams;
extern unsigned long lwModemConnectionSpeed;
extern NETCONFIG CMOSNetworkConfig;
extern const char UicVersionString[];
extern FILES_TO_DOWNLOAD CMOSFilesToDownload;
extern unsigned long infrastructureNVM[2048];
extern CMOSRATECOMPSMAP CMOSRateCompsMap;
extern BOOL psdClockFlag;
extern int  bytesDownloadedSoFar;
extern int  totalBytesToDownload;
extern int  dlFileNum; 
extern int verbosity;

extern BOOL remoteLoggingIsOn(void);
extern BOOL fnGetLocalLoggingState(void);
extern m4IPSD  m4IPSD_postageRequested;
//---------------------------------------------------------------------
//      Global Variable Definitions:
//---------------------------------------------------------------------

// When the connectionFailed flag is set, it stays set for this long, then 
//  gets cleared.  While it is set, the canConnectToDistributor() will 
//  skip the attempt to connect to the distributor, and return FALSE.
// It can be changed via PHSIMVB with the SetTestData command. 
// This value is in msecs, and is set to 5 minutes by default.
UINT32 ulSkipDistributorTime = (5 * ONE_MINUTE_IN_MS);    
BOOL DISTTASK_Waiting = FALSE;

//---------------------------------------------------------------------
//      Prototypes for Local Functions:
//---------------------------------------------------------------------

//TODO - cleanly remove PPP dependent code
STATUS initPppDevice(void);
//STATUS disconnectFromAtt(void);
//STATUS connectToAtt(char *numberToDial);

unsigned long *getInfrastructureNvmAddress(void);
unsigned long *getNvmHeapAddress(void);
size_t getSizeOfInfrastructureNVM(void);
DATETIME fnInfraUpdateParseDateTime(char *pDate, char *pTime);
void USBModemCyclePower(void);
void fnGetNLogInfo( char *pDest, size_t wLen );


//---------------------------------------------------------------------
//      Local Variables:
//---------------------------------------------------------------------

//File scope statics

//A nice place to put NU_Function_x() returns
//static STATUS nucStatusRet;

//File scope scratchpad
static char scratchPad[256];        // This is used to build the message for AT&T, and for throwing exceptions
static char scratchpad100[ 100 ];   // This is used for syslog entries.

static CRACKED_URL crackedDistURL;

//Using this to determine if status updates are to be output
static BOOL verbose = 0;

//POST header using HTTP 1.1
static const char *loginHdr = "\
Connection: close\r\n\
Content-type: application/x-www-form-urlencoded\r\n\
Content-length: ";

//Going to "Share" receive + transmit + encoding buffers
//#define RECV_BUFFER_SIZE	8192
//static char gRecvBuffer[RECV_BUFFER_SIZE];

//#define XMIT_BUFFER_SIZE	2048
//static char gTransmitBuffer[XMIT_BUFFER_SIZE];

//#define ENCODE_BUFFER_SIZE	2048
//static unsigned char gUrlEncodingBuffer[ENCODE_BUFFER_SIZE];

/*Initialized the infrastructureNvmPtr directly with the CMOS Variable since the data cature
 init function utilize this variable before even the Distributor task get initialized.
 By Sam & Rick - Mar 22, 2006  */
//File scope NVM pointer
static INFRASTRUCTURE_NVM *infrastructureNvmPtr = (INFRASTRUCTURE_NVM *) infrastructureNVM;

//Container for text message from Distributor
static char textMessageFromDistributor[256];

static BOOL connectedToAtt = FALSE;

static BOOL operatorPushedCheckForUpdatesButton;

static BOOL needTogoToPbpFlag;

static BOOL talkedToDistributor = FALSE;

static const unsigned long sDistResponseTimeout = 60000;

unsigned char bOITRequestedService;

static unsigned long timeOfFailure = 0;
	
static BOOL connectionFailed = FALSE;

static char lastDistributorURL[256];
static api_status_t distTaskErrorEnum;
static int distTaskErrorCode;

/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////
The file scope data structures (or primitives) in this section are used for processing service entries. 
For now note that the distributor just gives us dummy data to test with.

They are pointers to Service Map Table Entry, and Service Required Table Entry "Links". A "Link", in this
case contains a pointer to the Service XXXX Table Entry, and a pointer to the next "Link". The lists are
managed dynamically, since we do not know beforehand how many of either type entries the distributor 
will respond with. Vstack.c contains the code for Service XXXX Table Entry list object management.
*///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////

OBJECT: currentSmtEntryLinkPtr

DESCRIPTION:

The pointer "currentSmtEntryLinkPtr" tracks the service map table entry link "in process". In this context 
"in process" means that either data is being entered into or extracted from a particular Service Map Table Entry.

USAGE:

A new SMT_ENTRY_LINK is created for each Service Map Table Entry returned by the distributor. This occurs as
a result of XMLParse() finding a DstSmtEnt start tag, thus calling dstSmtEntStart(), which creates the new link
and assigns currentSmtEntryLinkPtr to point to it.

The pointer "currentSmtEntryLinkPtr" is pushed on the "Virtual Stack" when the DstSmtEnt end tag is parsed via 
the parser calling dstSmtEntEnd() which in turn calls pushSmtEntry().

It is also used as the destination for a popSmtEntry(). More on that later.

*///////////////////////////////////////////////////////////////////////////////////////////////////////////////
static SMT_ENTRY_LINK *currentSmtEntryLinkPtr = 0;

/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////

OBJECT: headSmtEntryLinkPtr

DESCRIPTION:

The pointer "headSmtEntryLinkPtr" always points to the head of the list of SMT_ENTRY_LINK pointers.

USAGE:

The pointer "headSmtEntryLinkPtr" is used by the virtual stack management functions in vstack.c.


*///////////////////////////////////////////////////////////////////////////////////////////////////////////////
static SMT_ENTRY_LINK *headSmtEntryLinkPtr = 0;

/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////

OBJECT: currentSrtEntryLinkPtr

DESCRIPTION:

The pointer "currentSrtEntryLinkPtr" tracks the service required table entry link "in process". In this context 
"in process" means that either data is being entered into or extracted from a particular Service Required Table 
Entry.

USAGE:

A new SRT_ENTRY_LINK is created for each Service Required Table Entry returned by the distributor. This occurs 
as a result of XMLParse() finding a DstSrtEnt start tag, thus calling dstSrtEntStart(), which creates the new 
link and assigns currentSrtEntryLinkPtr to point to it.

The pointer "currentSrtEntryLinkPtr" is pushed on the "Virtual Stack" when the DstSrtEnt end tag is parsed via 
the parser calling dstSrtEntEnd() which in turn calls pushSrtEntry().

It is also used as the destination for a popSrtEntry(). More on that later. I promise.

*///////////////////////////////////////////////////////////////////////////////////////////////////////////////
static SRT_ENTRY_LINK *currentSrtEntryLinkPtr = 0;

/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////

OBJECT: headSrtEntryLinkPtr

DESCRIPTION:

The pointer "headSrtEntryLinkPtr" always points to the head of the list of SRT_ENTRY_LINK pointers.

USAGE:

The pointer "headSrtEntryLinkPtr" is used by the virtual stack management functions in vstack.c.


*///////////////////////////////////////////////////////////////////////////////////////////////////////////////
static SRT_ENTRY_LINK *headSrtEntryLinkPtr = 0;

/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////

OBJECT: pbpSmtPtr

DESCRIPTION:

The pointer "pbpSmtPtr" is used to process PBP Service Map Table Entry information.

USAGE:

The pointer "pbpSmtPtr" is assigned to in meterSmtHandler() if it determines the requested service is for
postage by phone.

NOTES:

Currently, only requests for "Refill", "Refund", and "AcctBal" are considered valid PBP services by 
meterSmtHandler().

*///////////////////////////////////////////////////////////////////////////////////////////////////////////////
static SMT_ENTRY_LINK *pbpSmtPtr = 0;
static SMT_ENTRY_LINK *pbp2ndaryRefillPtr = 0;

/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////

OBJECT: uicSwDownloadPtr

DESCRIPTION:

The pointer "uicSwDownloadPtr" is used to process UIC software download service required entries.

USAGE:

The pointer "uicSwDownloadPtr" is assigned to in baseSrtHandler() if it determines the requested service is for
software download.

*///////////////////////////////////////////////////////////////////////////////////////////////////////////////
static SRT_ENTRY_LINK *uicSwDownloadPtr = 0;

//This one for explicity requested download
static SMT_ENTRY_LINK *swDnldSmtPtr  = 0;

//One for rates download too...
static SRT_ENTRY_LINK *uicRatesDownloadPtr = 0;

/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////

OBJECT: meterSwDownloadPtr

DESCRIPTION:

The pointer "meterSwDownloadPtr" is used to process Meter software download service required entries.

USAGE:

The pointer "meterSwDownloadPtr" is assigned to in meterSrtHandler() if it determines the requested service is 
for software download.

*///////////////////////////////////////////////////////////////////////////////////////////////////////////////
static SRT_ENTRY_LINK *meterSwDownloadPtr = 0;

/***********************************************************************************
OBJECT: AccountingUploadPtr

DESCRIPTION:

The pointer "uicAcctUploadPtr" is used to hold the service that handles accounig
data upload

USAGE:

The pointer "uicAcctUploadPtr" is assigned in 
*************************************************************************************/
SMT_ENTRY_LINK *uicAcctUploadPtr = 0;
 
//There will be more...


/*//////////////////////////////////////////////////////////////////////////////
cmdStaCmpData()

PARAMETERS:

The parameters follow our standard XML start tag handling function 
definition.

DESCRIPTION: 

This function looks at the overall sign on command response field.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK. 

NOTES:

For now I am just displaying the status.

*///////////////////////////////////////////////////////////////////////////////
static short cmdStaCmpData(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    if(verbose)
    {
        sprintf(scratchPad, "Command Completion Status = %s", pChars);
        putMsg(scratchPad, nextLine());
    }
    
    if(strcmp((char *)pChars, "0"))    //Status should always be "0" or there is a problem
    {
        sprintf(scratchPad, "Error: CmdStaCmp = %s", pChars);
        throwException(scratchPad);
    }

    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSmtEntStart()

PARAMETERS:

The parameters follow our standard XML start tag handling function 
definition.

DESCRIPTION: 

This function is called by the parser when it finds a DstSmtEnt start tag. 
It creates a new SMT_ENTRY_LINK.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK. 

NOTES:

The real item of interest is the pointer to the new link that is written to 
the file scope currentSmtEntryLinkPtr.See also the function newSmtEntryLink() 
for additional relevant information.

The tag "DstSmtEnt" is short for "Distributor Service Map Table Entry".

*///////////////////////////////////////////////////////////////////////////////
static short dstSmtEntStart(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
STATUS status;

    //Create a service map table entry link 
    if((status = newSmtEntryLink(&currentSmtEntryLinkPtr)) != NU_SUCCESS)
    {
        sprintf(scratchPad, "newSmtEntryLink() ERR# %d", status);
        throwException(scratchPad);
    }

    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSmtEntEnd()

PARAMETERS:

The parameters follow our standard XML end tag handling function definition.

DESCRIPTION:

This function is called by the parser when it finds a DstSmtEnt end tag. 
It pushes the current SMT_ENTRY_LINK onto the XML entry stack.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES:

The real item of interest is that the file scope headSmtEntryLinkPtr pointer is
updated via the pushSmtEntry() call. See also the function pushSmtEntry() 
for additional relevant information.

*///////////////////////////////////////////////////////////////////////////////
static short dstSmtEntEnd(XmlChar *pName)
{
    //Push the link
    headSmtEntryLinkPtr = pushSmtEntry(headSmtEntryLinkPtr, currentSmtEntryLinkPtr);
    
    //Make sure it worked
    if(!headSmtEntryLinkPtr)
        throwException("pushSmtEntry() returned NULL");
        
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSmtStaData()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function called by the parser when it finds a SmtStaData tag entity body.
The SmtStaData tag entity body contains the command completion status for the 
current service map table entry. The command completion status is stored in the
SERVICE_MAP_TABLE_ENTRY_INFO portion of the current SMT_ENTRY_LINK pointed to
by the file scope currentSmtEntryLinkPtr.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short dstSmtStaData(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("Error: dstSmtStaData() NULL argument received");
    
    if(!currentSmtEntryLinkPtr)
        throwException("Error: dstSmtStaData() NULL currentSmtEntryLinkPtr");
    
    //Store the service map table entry data
    if(strlen((char*)pChars) < sizeof(currentSmtEntryLinkPtr->smtEntryPtr->status))
        memcpy(currentSmtEntryLinkPtr->smtEntryPtr->status, pChars, strlen((char*)pChars)); //It fits
    
    else  //Truncate
        memcpy(currentSmtEntryLinkPtr->smtEntryPtr->status, pChars, 
            sizeof(currentSmtEntryLinkPtr->smtEntryPtr->status) - 1);
        
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSmtPcnData()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function called by the parser when it finds a SmtPcnData tag entity body.
The SmtPcnData tag entity body contains the PCN for the current service map table 
entry. The PCN is then stored in the SERVICE_MAP_TABLE_ENTRY_INFO portion of the 
current SMT_ENTRY_LINK pointed to by the file scope currentSmtEntryLinkPtr.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short dstSmtPcnData(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("Error: dstSmtPcnData() NULL argument received");
    
    if(!currentSmtEntryLinkPtr)
        throwException("Error: dstSmtPcnData() NULL currentSmtEntryLinkPtr");
    
    //Store the service map table entry data
    if(strlen((char*)pChars) < sizeof(currentSmtEntryLinkPtr->smtEntryPtr->pcn))
        memcpy(currentSmtEntryLinkPtr->smtEntryPtr->pcn, pChars, strlen((char*)pChars));    //It fits
    
    else  //Truncate
        memcpy(currentSmtEntryLinkPtr->smtEntryPtr->pcn, pChars, 
            sizeof(currentSmtEntryLinkPtr->smtEntryPtr->pcn) - 1);
        
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSmtSvcData()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function called by the parser when it finds a SmtSvcData tag entity body.
The SmtSvcData tag entity body contains the service for the current service map 
table entry. The service is then stored in the SERVICE_MAP_TABLE_ENTRY_INFO
portion of the current SMT_ENTRY_LINK pointed to by the file scope
currentSmtEntryLinkPtr.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short dstSmtSvcData(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("Error: dstSmtSvcData() NULL argument received");
    
    if(!currentSmtEntryLinkPtr)
        throwException("Error: dstSmtSvcData() NULL currentSmtEntryLinkPtr");
    
    //Store the service map table entry data
    if(strlen((char*)pChars) < sizeof(currentSmtEntryLinkPtr->smtEntryPtr->serviceIdentifier))
        memcpy(currentSmtEntryLinkPtr->smtEntryPtr->serviceIdentifier, pChars, strlen((char*)pChars));  //It fits
    
    else  //Truncate
        memcpy(currentSmtEntryLinkPtr->smtEntryPtr->serviceIdentifier, pChars, 
            sizeof(currentSmtEntryLinkPtr->smtEntryPtr->serviceIdentifier) - 1);
        
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSmtUrlData()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function called by the parser when it finds a SmtUrlData tag entity body.
The SmtUrlData tag entity body contains the URL for the current service map
table entry. The URL is then stored in the SERVICE_MAP_TABLE_ENTRY_INFO portion
of the current SMT_ENTRY_LINK pointed to by the file scope
currentSmtEntryLinkPtr.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short dstSmtUrlData(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("Error: dstSmtUrlData() NULL argument received");
    
    if(!currentSmtEntryLinkPtr)
        throwException("Error: dstSmtUrlData() NULL currentSmtEntryLinkPtr");
    
    //Store the service map table entry data
    if(strlen((char*)pChars) < sizeof(currentSmtEntryLinkPtr->smtEntryPtr->serverUrl))
        memcpy(currentSmtEntryLinkPtr->smtEntryPtr->serverUrl, pChars, strlen((char*)pChars));  //It fits
    
    else  //Truncate
        memcpy(currentSmtEntryLinkPtr->smtEntryPtr->serverUrl, pChars, 
            sizeof(currentSmtEntryLinkPtr->smtEntryPtr->serverUrl) - 1);
        
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSrtEntStart()

PARAMETERS:

The parameters follow our standard XML start tag handling function 
definition.

DESCRIPTION: 

This function is called by the parser when it finds a DstSrtEnt start tag. 
It creates a new SRT_ENTRY_LINK.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK. 

NOTES:

The real item of interest is the pointer to the new link that is written to 
the file scope currentSrtEntryLinkPtr.See also the function newSrtEntryLink() 
for additional relevant information.

The tag "DstSrtEnt" is short for "Distributor Service Required Table Entry".

*///////////////////////////////////////////////////////////////////////////////
static short dstSrtEntStart(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
	STATUS status;

    //Create a service map table entry link 
    if((status = newSrtEntryLink(&currentSrtEntryLinkPtr)) != NU_SUCCESS)
    {
        sprintf(scratchPad, "newSrtEntryLink() ERR# %d", status);
        throwException(scratchPad);
    }

    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSrtEntEnd()

PARAMETERS:

The parameters follow our standard XML end tag handling function definition.

DESCRIPTION:

This function is called by the parser when it finds a DstSrtEnt end tag. 
It pushes the current SRT_ENTRY_LINK onto the XML entry stack.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES:

The real item of interest is that the file scope headSrtEntryLinkPtr pointer is
updated via the pushSrtEntry() call. See also the function pushSrtEntry() 
for additional relevant information.

*///////////////////////////////////////////////////////////////////////////////
static short dstSrtEntEnd(XmlChar *pName)
{
    //Push the link
    headSrtEntryLinkPtr = pushSrtEntry(headSrtEntryLinkPtr, currentSrtEntryLinkPtr);
    
    //Make sure it worked
    if(!headSrtEntryLinkPtr)
        throwException("pushSrtEntry() returned NULL");
        
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSrtPcnData()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function is called by the parser when it finds a DstSrtPcn tag entity body.
The DstSrtPcn tag entity body contains the PCN for the current service required
table entry. The PCN is then stored in the SERVICE_REQ_TABLE_ENTRY_INFO portion
of the current SRT_ENTRY_LINK pointed to by the file scope currentSrtEntryLinkPtr.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short dstSrtPcnData(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("Error: dstSrtPcnData() NULL argument received");
    
    if(!currentSrtEntryLinkPtr)
        throwException("Error: dstSrtPcnData() NULL currentSrtEntryLinkPtr");
    
    //Store the service map table entry data
    if(strlen((char*)pChars) < sizeof(currentSrtEntryLinkPtr->srtEntryPtr->pcn))
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->pcn, pChars, strlen((char*)pChars));    //It fits
    
    else  //Truncate
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->pcn, pChars, 
            sizeof(currentSrtEntryLinkPtr->srtEntryPtr->pcn) - 1);
        
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSrtSvcData()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function is called by the parser when it finds a DstSrtSvc tag entity body.
The DstSrtSvc tag entity body contains the service identifier for the current 
service required table entry. The service identifier is then stored in the 
SERVICE_REQ_TABLE_ENTRY_INFO portion of the current SRT_ENTRY_LINK pointed to by 
the file scope currentSrtEntryLinkPtr.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short dstSrtSvcData(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("Error: dstSrtSvcData() NULL argument received");
    
    if(!currentSrtEntryLinkPtr)
        throwException("Error: dstSrtSvcData() NULL currentSrtEntryLinkPtr");
    
    //Store the service map table entry data
    if(strlen((char*)pChars) < sizeof(currentSrtEntryLinkPtr->srtEntryPtr->service))
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->service, pChars, strlen((char*)pChars));    //It fits
    
    else  //Truncate
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->service, pChars, 
            sizeof(currentSrtEntryLinkPtr->srtEntryPtr->service) - 1);
        
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSrtPriData()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function is called by the parser when it finds a DstSrtPri tag entity body.
The DstSrtPri tag entity body contains the priority of the current service 
required table entry. The priority is then stored in the 
SERVICE_REQ_TABLE_ENTRY_INFO portion of the current SRT_ENTRY_LINK pointed to 
by the file scope currentSrtEntryLinkPtr.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short dstSrtPriData(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("Error: dstSrtPriData() NULL argument received");
    
    if(!currentSrtEntryLinkPtr)
        throwException("Error: dstSrtPriData() NULL currentSrtEntryLinkPtr");
    
    //Store the service map table entry data
    if(strlen((char*)pChars) < sizeof(currentSrtEntryLinkPtr->srtEntryPtr->priority))
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->priority, pChars, strlen((char*)pChars));   //It fits
    
    else  //Truncate
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->priority, pChars, 
            sizeof(currentSrtEntryLinkPtr->srtEntryPtr->priority) - 1);
        
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSrtUrlData()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function is called by the parser when it finds a DstSrtUrl tag entity body.
The DstSrtUrl tag entity body contains the URL for the current service required
table entry. The URL is then stored in the SERVICE_REQ_TABLE_ENTRY_INFO portion
of the current SRT_ENTRY_LINK pointed to by the file scope currentSrtEntryLinkPtr.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short dstSrtUrlData(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("Error: dstSrtUrlData() NULL argument received");
    
    if(!currentSrtEntryLinkPtr)
        throwException("Error: dstSrtUrlData() NULL currentSrtEntryLinkPtr");
    
    //Store the service map table entry data
    if(strlen((char*)pChars) < sizeof(currentSrtEntryLinkPtr->srtEntryPtr->url))
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->url, pChars, strlen((char*)pChars));    //It fits
    
    else  //Truncate
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->url, pChars, 
            sizeof(currentSrtEntryLinkPtr->srtEntryPtr->url) - 1);
        
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSrtDatSvcData()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function is called by the parser when it finds a DstSrtDatSvc tag entity 
body. The DstSrtDatSvc tag entity body contains the perform by date for the 
current service required table entry. The perform by date is then stored in the 
SERVICE_REQ_TABLE_ENTRY_INFO portion of the current SRT_ENTRY_LINK pointed to 
by the file scope currentSrtEntryLinkPtr.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short dstSrtDatSvcData(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("Error: dstSrtDatSvcData() NULL argument received");
    
    if(!currentSrtEntryLinkPtr)
        throwException("Error: dstSrtDatSvcData() NULL currentSrtEntryLinkPtr");
    
    //Store the service map table entry data
    if(strlen((char*)pChars) < sizeof(currentSrtEntryLinkPtr->srtEntryPtr->performByDate))
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->performByDate, pChars, strlen((char*)pChars));  //It fits
    
    else  //Truncate
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->performByDate, pChars, 
            sizeof(currentSrtEntryLinkPtr->srtEntryPtr->performByDate) - 1);
        
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSrtTimSvcData()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function is called by the parser when it finds a DstSrtTimSvc tag entity 
body. The DstSrtTimSvc tag entity body contains the perform by time for the 
current service required table entry. The perform by time is then stored in the 
SERVICE_REQ_TABLE_ENTRY_INFO portion of the current SRT_ENTRY_LINK pointed to 
by the file scope currentSrtEntryLinkPtr.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short dstSrtTimSvcData(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("Error: dstSrtTimSvcData() NULL argument received");
    
    if(!currentSrtEntryLinkPtr)
        throwException("Error: dstSrtTimSvcData() NULL currentSrtEntryLinkPtr");
    
    //Store the service map table entry data
    if(strlen((char*)pChars) < sizeof(currentSrtEntryLinkPtr->srtEntryPtr->performByTime))
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->performByTime, pChars, strlen((char*)pChars));  //It fits
    
    else  //Truncate
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->performByTime, pChars, 
            sizeof(currentSrtEntryLinkPtr->srtEntryPtr->performByTime) - 1);
        
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSrtUidData()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function is called by the parser when it finds a DstSrtUid tag entity 
body. The DstSrtUid tag entity body contains the unique identifier for the 
current service required table entry. The unique identifier is then stored in 
the SERVICE_REQ_TABLE_ENTRY_INFO portion of the current SRT_ENTRY_LINK pointed 
to by the file scope currentSrtEntryLinkPtr.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short dstSrtUidData(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("Error: dstSrtUidData() NULL argument received");
    
    if(!currentSrtEntryLinkPtr)
        throwException("Error: dstSrtUidData() NULL currentSrtEntryLinkPtr");
    
    //Store the service map table entry data
    if(strlen((char*)pChars) < sizeof(currentSrtEntryLinkPtr->srtEntryPtr->uniqueIdentifier))
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->uniqueIdentifier, pChars, strlen((char*)pChars));   //It fits
    
    else  //Truncate
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->uniqueIdentifier, pChars, 
            sizeof(currentSrtEntryLinkPtr->srtEntryPtr->uniqueIdentifier) - 1);
        
    return XML_STATUS_OK;
}


/*//////////////////////////////////////////////////////////////////////////////
dstSrtDatPstData()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function is called by the parser when it finds a DstSrtDatPst tag entity 
body. The DstSrtDatPst tag entity body contains the post date for the current
service required table entry. The post date is then stored in the 
SERVICE_REQ_TABLE_ENTRY_INFO portion of the current SRT_ENTRY_LINK pointed to 
by the file scope currentSrtEntryLinkPtr.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short dstSrtDatPstData(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("Error: dstSrtDatPstData() NULL argument received");
    
    if(!currentSrtEntryLinkPtr)
        throwException("Error: dstSrtDatPstData() NULL currentSrtEntryLinkPtr");
    
    //Store the service map table entry data
    if(strlen((char*)pChars) < sizeof(currentSrtEntryLinkPtr->srtEntryPtr->servicePostDate))
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->servicePostDate, pChars, strlen((char*)pChars));    //It fits
    
    else  //Truncate
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->servicePostDate, pChars, 
            sizeof(currentSrtEntryLinkPtr->srtEntryPtr->servicePostDate) - 1);
        
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
dstSrtTimPstData()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function is called by the parser when it finds a DstSrtTimPst tag entity 
body. The DstSrtTimPst tag entity body contains the post time for the current
service required table entry. The post time is then stored in the 
SERVICE_REQ_TABLE_ENTRY_INFO portion of the current SRT_ENTRY_LINK pointed to 
by the file scope currentSrtEntryLinkPtr.

RETURNS:

The actual "return" value is simply the default XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short dstSrtTimPstData(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("Error: dstSrtTimPstData() NULL argument received");
    
    if(!currentSrtEntryLinkPtr)
        throwException("Error: dstSrtTimPstData() NULL currentSrtEntryLinkPtr");
    
    //Store the service map table entry data
    if(strlen((char*)pChars) < sizeof(currentSrtEntryLinkPtr->srtEntryPtr->servicePostTime))
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->servicePostTime, pChars, strlen((char*)pChars));    //It fits
    
    else  //Truncate
        memcpy(currentSrtEntryLinkPtr->srtEntryPtr->servicePostTime, pChars, 
            sizeof(currentSrtEntryLinkPtr->srtEntryPtr->servicePostTime) - 1);
        
    return XML_STATUS_OK;
}

//-------------------------------------------------------------------------------------------
//Remtote logging...
short setRlUrl(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
   return XML_STATUS_OK;
}

short RlOff(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    return XML_STATUS_OK;
}

//Unique identifier reset
short resetUniqueId(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    memset(infrastructureNvmPtr->uicSrtUniqueIdentifer, 0, sizeof(infrastructureNvmPtr->uicSrtUniqueIdentifer));

    pushDisplay();

    putMsg("Resetting Unique Identifier", nextLine());
    OSWakeAfter(4000);

    popDisplay();

    return XML_STATUS_OK;
}

//Set that pesky RM present bit
short setRmPresentBit(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{

	dbgTrace(DBG_LVL_INFO, "DISTTASK: Ignoring unsupported SetRmPresentBit command");

    return XML_STATUS_OK;
}

//remote exception log retrival
short gimmeXlog(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{

	dbgTrace(DBG_LVL_INFO, "DISTTASK: Ignoring unsupported GimmeXlog command");

    return XML_STATUS_OK;
}

//Clean slate
short wipeFfs(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{

	dbgTrace(DBG_LVL_INFO, "DISTTASK: Ignoring unsupported WipeFfs command");

    //lint -e(527)
    return XML_STATUS_OK;
}

//---------------------------------------------------------------------
//	delFile
//---------------------------------------------------------------------
short delFile(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{

    //Validate arguments
    if(!pChars)
    {
    	dbgTrace(DBG_LVL_INFO, "DISTTASK: Ignoring unsupported Delete File command: file %s", (char *)pChars);
    }
    return XML_STATUS_OK;
}

short setPcn(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("SetPcn() NULL argument received");

    //clean
    memset(CMOSSignature.bUicPcn, 0, sizeof(CMOSSignature.bUicPcn));
    
    //set it
    if( strlen( (char *)pChars ) >= sizeof( CMOSSignature.bUicPcn ) )
    {
        memcpy( CMOSSignature.bUicPcn, pChars, sizeof( CMOSSignature.bUicPcn ) - 1 );
    }
    else 
    {
        memcpy(CMOSSignature.bUicPcn, pChars, strlen((char*)pChars));
    }   
    CMOSSignature.bUicPcn[ SIZEOFUICPCN-1 ] = 0;
            
            
    pushDisplay();

    sprintf(scratchPad, "PCN set to %s", (char*)CMOSSignature.bUicPcn); 
    
    putMsg(scratchPad, nextLine());
    OSWakeAfter(4000);

    popDisplay();
                
    return XML_STATUS_OK;
}

short setSerialNumber(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("setSerialNumber() NULL argument received");

    //clean
    memset(CMOSSignature.bUicSN, 0, sizeof(CMOSSignature.bUicSN));
    
    //set it
    if( strlen( (char *)pChars ) >= sizeof( CMOSSignature.bUicSN ) )
    {
        memcpy( CMOSSignature.bUicSN, pChars, sizeof( CMOSSignature.bUicSN ) - 1 );
    }
    else 
    {
        memcpy(CMOSSignature.bUicSN, pChars, strlen((char*)pChars));
    }   
    CMOSSignature.bUicSN[ SIZEOFUICSN-1 ] = 0;
            
    pushDisplay();

    sprintf(scratchPad, "Serial # set to %s", (char*)CMOSSignature.bUicSN); 
    
    putMsg(scratchPad, nextLine());
    OSWakeAfter(4000);

    popDisplay();
    
    return XML_STATUS_OK;
}

static short gotTextMsgFromDistributor(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    //Validate arguments
    if(!pChars)
        throwException("Error: NULL argument");
    
    //Store the data
    if(strlen((char*)pChars) < sizeof(textMessageFromDistributor))
        memcpy(textMessageFromDistributor, pChars, strlen((char*)pChars));    //It fits
    
    else  //Truncate
        memcpy(textMessageFromDistributor, pChars, sizeof(textMessageFromDistributor) - 1);

    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
fnDISTRDefaultSTag()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function is called by the parser when it finds a start tag that does not 
have a corresponding entry in the ElementLookupEntry table.

RETURNS:

XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short fnDISTRDefaultSTag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
fnDISTRDefaultChars()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function is called by the parser when it finds a entity body that does not 
have a corresponding entry in the ElementLookupEntry table.

RETURNS:

XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short fnDISTRDefaultChars(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    return XML_STATUS_OK;
}

/*//////////////////////////////////////////////////////////////////////////////
fnDISTRDefaultETag()

PARAMETERS:

The parameters follow our standard XML entity body handling function 
definition.

DESCRIPTION:

This function is called by the parser when it finds a end tag that does not 
have a corresponding entry in the ElementLookupEntry table.

RETURNS:

XML_STATUS_OK.

NOTES: 

*///////////////////////////////////////////////////////////////////////////////
static short fnDISTRDefaultETag(XmlChar *pName)
{
    return XML_STATUS_OK;
}

//Here is the ElementLookupEntry table for the distributor
static const ElementLookupEntry pDISTRSignonRspEntries[] = 
{{(XmlChar *)"CmdStaCmp",{NULL, cmdStaCmpData, NULL}},
{(XmlChar *)"DstSmtEnt",{dstSmtEntStart,NULL,dstSmtEntEnd}},
{(XmlChar *)"DstSmtSta",{NULL,dstSmtStaData,NULL}},
{(XmlChar *)"DstSmtPcn",{NULL,dstSmtPcnData,NULL}},
{(XmlChar *)"DstSmtSvc",{NULL,dstSmtSvcData,NULL}},
{(XmlChar *)"DstSmtUrl",{NULL,dstSmtUrlData,NULL}},
{(XmlChar *)"DstSrtEnt",{dstSrtEntStart,NULL,dstSrtEntEnd}},
{(XmlChar *)"DstSrtPcn",{NULL,dstSrtPcnData,NULL}},
{(XmlChar *)"DstSrtSvc",{NULL,dstSrtSvcData,NULL}},
{(XmlChar *)"DstSrtPri",{NULL,dstSrtPriData,NULL}},
{(XmlChar *)"DstSrtUrl",{NULL,dstSrtUrlData,NULL}},
{(XmlChar *)"DstSrtDatSvc",{NULL,dstSrtDatSvcData,NULL}},
{(XmlChar *)"DstSrtTimSvc",{NULL,dstSrtTimSvcData,NULL}},
{(XmlChar *)"DstSrtUid",{NULL,dstSrtUidData,NULL}},
{(XmlChar *)"DstSrtDatPst",{NULL,dstSrtDatPstData,NULL}},
{(XmlChar *)"DstSrtTimPst",{NULL,dstSrtTimPstData,NULL}},
{(XmlChar *)"GimmeXlog",{gimmeXlog,NULL,NULL}},
{(XmlChar *)"WipeFfs",{wipeFfs,NULL,NULL}},
{(XmlChar *)"ResetUniqueId",{resetUniqueId,NULL,NULL}},
{(XmlChar *)"SetRmPresentBit",{setRmPresentBit,NULL,NULL}},
{(XmlChar *)"EnableLogging",{NULL,setRlUrl,NULL}},
{(XmlChar *)"SetPcn",{NULL,setPcn,NULL}},
{(XmlChar *)"SetSerialNumber",{NULL,setSerialNumber,NULL}},
{(XmlChar *)"TextMsg",{NULL,gotTextMsgFromDistributor,NULL}},
{(XmlChar *)"DelFile",{NULL,delFile,NULL}},
{(XmlChar *)"DisableLogging",{RlOff,NULL,NULL}}};

///////////////////////////////////////////////////////////////////////////////////
/* ----------------------------------------------------------------------------------------------[csd_logs_save_to_file]-- */
STATUS csd_logs_save_to_file()
{
	const int MAX_BUFFER_SIZE = 0x17FFF;
	char *bufferPtr;
    char *path = "Logs";
    STATUS status = NU_SUCCESS;

    NU_Allocate_Memory(MEM_Cached, (void **)&bufferPtr, MAX_BUFFER_SIZE, NU_NO_SUSPEND);
    if(!bufferPtr)
    {
    	status = setDnldException(API_OUT_OF_MEMORY, MEMORY_ERR);
    }
    else
    {
		dbgTrace(DBG_LVL_INFO, "DISTTASK Saving Logs . . .");
		storeNetlogToFile(path, log_names[LOG_NETWORK]);
		storeErrorLogToFile(path, log_names[LOG_ERROR], bufferPtr, MAX_BUFFER_SIZE);
		storeTrnFramLogToFile(path, log_names[LOG_TRM_FRAM], bufferPtr, MAX_BUFFER_SIZE);
		storeDiagnosticsLogToFile(path, log_names[LOG_DIAGNOSTICS], bufferPtr, MAX_BUFFER_SIZE);
		storeFramLogToFile(path, log_names[LOG_FRAM], bufferPtr, MAX_BUFFER_SIZE);
		storeSyslogToFile(path, log_names[LOG_SYSTEM]);
    }

	//release memory
	if((status = NU_Deallocate_Memory(bufferPtr)) != NU_SUCCESS)
    {
    	status = setDnldException(API_NULL_POINTER, MEMORY_ERR);
    }

	return status;
}


//Task entry point...
void distributorTask(unsigned long argc, void *argv)
{
	//Local variables...

	//For receiving messages
	INTERTASK msgIn;
	INT32 refillAmount;
	INT32 decimalPlaces;
	INT32 systemErrorLog;
	//A nice place to put NU_Function_x() returns
	//static STATUS nucStatusRet;

	//Use these for search returns, etc
	//char *charPtr;
	//char *charPtr2;
	char *recvBuffer;
	const int recvBufferSize = 8192;

    char pLogBuf[40];
    int *ptr;

	//BOOL AlreadyBeenToDls = FALSE;

    //Get the memory
    NU_Allocate_Memory(MEM_Cached, (void **)&recvBuffer, recvBufferSize, NU_NO_SUSPEND);
    if(!recvBuffer)
    {
        throwDnldException("Out of Memory!",MEMORY_ERR);
    }
    else
    {
        (void)memset(recvBuffer, 0, recvBufferSize);   //clear everything
    }

    msgIn.bMsgId = 0;

    fnSysLogTaskStart( "distributorTask", DISTTASK );  
    initIntelliLink();  //Initialization....................
    
    fnSysLogTaskLoop( "distributorTask", DISTTASK );  
    //Now hang out and process messages..........................
    while(1)
    {
    	if(msgIn.bMsgId != 0)
    	{
       		send_Dist_Task_Complete(msgIn.IntertaskUnion.bByteData[0]);
            send_PbpIO("DIST Task Complete", Base,'\0','\0');
    	}

        if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK Waiting . . .");
    	if(OSReceiveIntertask(DISTTASK, &msgIn, OS_SUSPEND) == SUCCESSFUL)
        {
            switch (msgIn.bMsgId) 
            {
                case CONNECT_TO_ATT:
                {
                    STATUS connectionResult = intellilinkConnectToAtt();

#ifndef JANUS
                    if(connectionResult)
                        fnUSBRestoreExternalTaskPriority(); //restore here if connect NG
#endif
                    //Inform the OIT...
                    (void)sprintf(pLogBuf, "Connection Result = %d", connectionResult);
                    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));

                    if(!OSSendIntertask (OIT, DISTTASK, (unsigned char)OIT_ATT_CONNECTION_RESULT, LONG_DATA, &connectionResult, 4))
                        throwException("OSSendIntertask() fail");

                    break;
                }
        
                case OIT_CLIENT_SERVICE_REQ:
                {
                    STATUS status;
                    
#ifdef LOG_OIT_MESSAGES
                    if(remoteLoggingIsOn())
                        (void)remoteLog("Received OIT_CLIENT_SERVICE_REQ message", NULL);
#endif                    
                    initBasePtrs();     //clean VStack

                    //let's get the arguments
                    ptr = (msgIn.IntertaskUnion.bByteData) + 1; //skip over the MsgId
                    refillAmount = *ptr++;
                    decimalPlaces = (*ptr & 0x0000FFFF); //use only the lower two bytes
                    systemErrorLog = ((*ptr & 0xFFFF0000) >> 16); //use only the higher two bytes

                    if (msgIn.IntertaskUnion.bByteData[0] == LOG_SERVICE)
                    	status = csd_logs_save_to_file();
                    else
                    {
						//check for REFILL
						if(msgIn.IntertaskUnion.bByteData[0] == REFILL_SERVICE)
						{
							//shift the refillAmount by the decimalPlaces
							//refillAmount *= (decimalPlaces == 1) ? 100 : ((decimalPlaces == 2) ? 10 : ((decimalPlaces == 3) ? 1 : 1000));
							//flip the Endian
							refillAmount = EndianSwap32(refillAmount);
							//update the global variable for refill amount
							memcpy(&m4IPSD_postageRequested, &refillAmount, sizeof(INT32));
						}


						if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK processing . . .");

					   //Format, send XML to Distributor, and place response in the receive buffer
						status = distributorHttpTransaction(&msgIn, recvBuffer, recvBufferSize);

#ifdef LOG_OIT_MESSAGES
						if(remoteLoggingIsOn())
							(void)remoteLog("Sending OIT OIT_DISTRIBUTOR_CONTACTED message", NULL);
#endif                                        
						//if(!OSSendIntertask (OIT, DISTTASK, (unsigned char)OIT_DISTRIBUTOR_CONTACTED, NO_DATA, NULL, 0))
							//throwException("OSSendIntertask() fail");

						// Transaction with Distributor worked? (This is a hook as oitClientServiceReqHandler() returns whatever
						// signOnToDistributor() returns, and signOnToDistributor() always either returns NU_SUCCESS or throws
						// an exception on one of the low level TCP/IP calls
						if(status == NU_SUCCESS)
						{
							if(!OSSendIntertask (OIT, DISTTASK, (unsigned char)OIT_DISTRIBUTOR_CONTACTED, NO_DATA, NULL, 0))
								throwException("OSSendIntertask() fail");

							if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK parsing Distributor Response . . .");
							parseDistributorResponse(recvBuffer, recvBufferSize); //Parse the response...

							clrNeedTogoToPbpFlag();

							if (pbpServices() == NO_PROBLEM)  //PbP comes first
							{
	//                            acctUploadService();  //Perform Accounting Upload Service

								// in OOB mode, if operation timeout in PBP balance, refill postage
								// and verify amount screen, the download step will be skipped
	//                            if (fnIsOOBPBPTimeout() == FALSE)
								if (msgIn.IntertaskUnion.bByteData[0] == SWDOWNLOAD_SERVICE)
								{
									dlaServices2(recvBuffer, recvBufferSize, systemErrorLog);  //Then DLA
								}
							}

							deleteVptrs();  //clean VStack

#ifdef LOG_OIT_MESSAGES
							if(remoteLoggingIsOn())
								(void)remoteLog("Sending OIT OIT_ALL_SERVICES_COMPLETE message", NULL);
#endif  
							// In case the OIT screen switches from printing to no printing state,
							// wait to make sure the screen is ready for receiving the message.
							OSWakeAfter(100);

							if(!OSSendIntertask (OIT, DISTTASK, (unsigned char)OIT_ALL_SERVICES_COMPLETE, NO_DATA, 0, 0))
								throwException("OSSendIntertask() fail");
						}
#ifdef JANUS
						else
						{
							(void)sprintf(pLogBuf, "Distributor Result = %d", status);
							fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
						}
#endif
                    }

            		if (distTaskErrorEnum != API_STATUS_SUCCESS)
            		{
            			//send message to tablet with error status
            			send_DistTaskError(msgIn.IntertaskUnion.bByteData[0], distTaskErrorEnum, distTaskErrorCode);
            			//reset error for next task
            			status = setDnldException(API_STATUS_SUCCESS, 0);
            		}
                    break;
                }

                case OIT_ATT_DISCONNECT:
                {
                    lwModemConnectionSpeed = 0;
                    intellilinkDisconnectFromAtt();
                    
                    break;
                }
                default:
                {
                    sprintf(scratchPad, "Unexpected message Src %d ID -> %d", msgIn.bSource, msgIn.bMsgId);
                    throwException(scratchPad);
                    break;
                }
            }
        }
        else
            throwException("OSReceiveIntertask() fail");
    }

    //Free the buffers...
	if(NU_Deallocate_Memory(recvBuffer) != NU_SUCCESS)
    {
        throwDnldException("Invalid Pointer",INVALID_PTR);
    }

}




uchar   bOITModemDialMode;

#ifdef JANUS
void ChangeModemDialPrefix( void )
{
#define DIALING_TONE  1
#define DIALING_PULSE 0
    if (bOITModemDialMode)
    {
        CMOSNetworkConfig.dial_prefix[3] = 'T';
        CMOSSetupParams.ToneDialing = DIALING_TONE;
    }
    else
    {
        CMOSNetworkConfig.dial_prefix[3] = 'P';
        CMOSSetupParams.ToneDialing = DIALING_PULSE;
    }
#undef DIALING_PULSE
#undef DIALING_TONE
}
#endif




void initIntelliLink(void)
{       
	STATUS nucStatusRet;
	
   
#ifdef JANUS 

#define DIALING_TONE  1
#define DIALING_PULSE 0

    CMOSNetworkConfig.dial_prefix[4] = 0;

    if (CMOSNetworkConfig.dial_prefix[3] == 'T')
    {
        bOITModemDialMode = 1;      // TONE
        CMOSSetupParams.ToneDialing = DIALING_TONE;
    }
    else
    {
        bOITModemDialMode = 0;      // PULSE
        CMOSSetupParams.ToneDialing = DIALING_PULSE;
    }
#undef DIALING_PULSE
#undef DIALING_TONE
#endif
    //TODO - Senior Enable after PSD Get GMT is working
    //while(!psdClockFlag)
        OSWakeAfter(1000);      //Need time to be sure RTC is up before registering events with fnAddTimerEvent()!
        
    if(verbose)
        initDisplay();  //Get the display  ready for direct use if applicable  
    
    //Initialize NVM pointer
    infrastructureNvmPtr = (INFRASTRUCTURE_NVM *)getInfrastructureNvmAddress();
    
    //Check for valid data
    if(infrastructureNvmPtr->NvmCheckword != VALID_CHECKWORD)
    {
        initInfrastructureNvm();
        initNvmHeap();
        
        if(verbose)
        {
            pushDisplay();
            putMsg("Initializing Infrastructure NVM.", nextLine());
            OSWakeAfter(5000);      
            popDisplay();
        }
    }
    else
    {
        versionCheckNVM();
        initEventScheduler();    
    }

    if(verbose) 
        {
        putMsg("Initializing network.", nextLine());
        OSWakeAfter(1000);
        }
//TODO - cleanly remove PPP dependent code
#if 0 //removing modem code
    //Just the PPP device as NU_Init_Net() was moved to systask
    if((nucStatusRet = initPppDevice()) != NU_SUCCESS)
        {
        sprintf(scratchPad, "initPppDevice() ERR# %d", nucStatusRet);
        throwException(scratchPad);
        }
#endif
    /**--------- 
    //And the file abstaction layer
    if (FAL_Sys_Init(0) != NU_SUCCESS)
    {
        sprintf(scratchPad, "FAL_Sys_Init() ERR# %d", FAL_Get_Last_Error());
        throwException(scratchPad);
    }
    ----------- */
}


STATUS intellilinkConnectToAtt(void)
{
    static STATUS nucLastModemStatus = NU_SUCCESS;
    STATUS nucStatusRet;
    unsigned long nucNetEventPtr;
    unsigned long lwModemEvent, lwModemSuspend;
    unsigned char bModemStatus;
    UINT8       bWhichLAN;
    BOOL        fLANConfigComplete;


#ifdef G900 
    fnDumpStringToSystemLog( "intellilinkConnectToAtt" );
/* no pc_daemon anymore
    if(usePcHostForIntelliLink())       // no need to connect via moden if PC host exist
    {
        fnDumpStringToSystemLog( "PCHostForIntellilink" );
        return NU_SUCCESS;
    }
    else
*/
    {
        nucLastModemStatus = NU_SUCCESS;
    }

    if(fnGetConnectionMode()==CONNECTION_MODE_AUTO_DETECT)
    {
        fnDumpStringToSystemLog( "ConnectionModeisAutoDetect" );
        if (connectedToInternetViaLan(&bWhichLAN))  // no need to connect via modem if there is a ethernet lan adapter present
        {
            if (bWhichLAN == LAN_ADAPTER_USB)   
            {
                // usb lan.  must make sure it is fully initialized (e.g. DHCP complete) otherwise we are not really ready
                //  and should fail here.
                fnDumpStringToSystemLog( "USBLanAdapterDetected" );
/*
                if (GetInitializedEtherCount() == 0)
                {
                    fnDumpStringToSystemLog( "USBLanNotEstablished" );

                    //give UI enough time to paint the InfraContactPB screen for the extern USB LAN adapter.
                    //otherwise, user can only see the connection lost screen.
                    OSWakeAfter(1000);
                   
                    return NU_NOT_ESTAB;
                }
                else
                    return NU_SUCCESS;          
*/
            }
            else
            {
                // integrated lan
                fnDumpStringToSystemLog( "ERROR iLanTask support removed" );

/**
                fnDumpStringToSystemLog( "LanDetected" );
                if (fnLANGetIntegratedLANLinkStatus(&fLANConfigComplete) == true)
                {
                    // like the usb lan, it is possible for the integrated lan to have a valid link but not be usable yet
                    if (fLANConfigComplete)
                        return NU_SUCCESS;          
                    else
                    {
                        fnDumpStringToSystemLog( "IntegratedLanNotEstablished" );

                        //give UI enough time to paint the InfraContactPB screen for the integrated lan.
                        //otherwise, user can only see the connection lost screen.
                        OSWakeAfter(1000);
  
                        return NU_NOT_ESTAB;
                    }
                }
**/
            }
        }
        else
        {
            fnDumpStringToSystemLog( "NoLanDetected" );
            nucLastModemStatus = NU_SUCCESS;
        }
    }
#endif

    if(verbose) 
    {
        putMsg("Connecting to ATT.", nextLine());
        OSWakeAfter(1000);
    }
        
    //Ok, now it's time to place the call...
    memset(scratchPad , 0, sizeof(scratchPad));
    
    //For now we will use the 800 number... 
    //Someday the LCZ number may be used
    sprintf(scratchPad,"%s%s",getOutsideLinePrefix(),getAttPhoneNumber());

    // if we are using the USB modem
    if (CMOSSetupParams.fUseExternalModem == FALSE)
    {
#ifdef USINGUSBM
        /* if the last connection failed, try power cycling */
        if (nucLastModemStatus != NU_SUCCESS)
        {
            /* power cycle */
            USBModemCyclePower();

            /* wait at most 20 seconds for the modem to re-enumerate */
            lwModemSuspend = 20000;
        }
        else
        {
            /* the last connection was ok, just make sure the modem is there */
            lwModemSuspend = OS_NO_SUSPEND;
        }

        /* wait for either modem to enumerate or just see if its there */
        bModemStatus = OSReceiveEvents(MODEM_EVENT_GROUP,USB_MODEM_PRESENT,lwModemSuspend,OS_OR,&lwModemEvent);

        if ((bModemStatus == SUCCESSFUL) && ((lwModemEvent & USB_MODEM_PRESENT) > 0))
        {
            /* the modem is good to go */
            fnDumpStringToSystemLog( "USBModem Success" );
            nucLastModemStatus = NU_SUCCESS;                                    
        }
        else
        {
            /* There is a problem with the modem, tell the 
            OIT it isn't communicating. */
            nucLastModemStatus = NU_NO_MODEM;
        }
#endif
    }
    else
        nucLastModemStatus = NU_SUCCESS;                /* assume serial modem is ok */
    
    /* clear the modem connection speed */
    lwModemConnectionSpeed = 0;
    //TODO - cleanly remove PPP dependent code
#if 0
    /* If the modem looks good, try to connect to ATT. */
    if (nucLastModemStatus == NU_SUCCESS)
        nucLastModemStatus = connectToAtt(scratchPad);
#endif
    if(nucLastModemStatus == NU_SUCCESS)
    {
        if(verbose) 
        {
            putMsg("Connected to ATT.", nextLine());
            OSWakeAfter(1000);
        }
        
        connectedToAtt = TRUE;

        //Wait a while to be sure all TCP services are up etc...
        OSWakeAfter(3000);

        //Suspend until the Net and PPP stuff are running (really it should be ready immediately)
        if((nucStatusRet = NU_Retrieve_Events(&NUC_NET_UP_EVENT, NET_UP, NU_AND_CONSUME, &nucNetEventPtr, NU_SUSPEND)) == NU_SUCCESS)
            return nucStatusRet;    //Done, SUCCESS
        
        else
        {
            sprintf(scratchPad, "NU_Retrieve_Events() ERR# %d", nucStatusRet);
            throwException(scratchPad);
        }
        
/* //Upload our DHCP assigned IP                                                                                                         */
/*          if(remoteLoggingIsOn())                                                                                                               */
/*          {                                                                                                                                     */
/*  extern CHAR    PPP_Local_Ip[];                                                                                                           */
/*                                                                                                                                           */
/*              remoteLog("*******  Session start  *******", NULL);                                                                               */
/*      sprintf(scratchPad, "DHCP assigned UIC Address: %u.%u.%u.%u",                                                                        */
/*          (unsigned char)PPP_Local_Ip[0], (unsigned char)PPP_Local_Ip[1], (unsigned char)PPP_Local_Ip[2], (unsigned char)PPP_Local_Ip[3]); */
/*              remoteLog(NULL, scratchPad);                                                                                                      */
/*          }                                                                                                                                     */
/*                                                                                                                                                */
        //getLczPhoneNumber();    //Update the phone #
    }
    else
    {
        return nucLastModemStatus;
    }

    // TODO: ensure this return value is correct, there was none initially and compiler generated warning.
    return NU_SUCCESS;
}
                    
                    
void intellilinkDisconnectFromAtt(void)
{
    if(   !connectedToInternetViaLan(NULL_PTR) 
       && connectedToAtt )
    {                
        if(verbose) 
        {
            putMsg("Disconnecting from ATT.", nextLine());
            OSWakeAfter(1000);
        }

        //TODO - cleanly remove PPP dependent code
//        (void)disconnectFromAtt();
        connectedToAtt = FALSE;
		
#ifndef JANUS
        fnUSBRestoreExternalTaskPriority();
#endif
    }
}

// Record the usb100MDriver.dev_count
void sysLogDevCount( char *PrefixMsg, unsigned int count )
{
    char * pStr;

    if( *PrefixMsg != 0 )
    {
        pStr = PrefixMsg;
    }
    else
    {
        pStr = "Usb device";
    }
	
    sprintf( scratchpad100, "%s count=%d", pStr, count );
    fnDumpStringToSystemLog( scratchpad100 );
}

//TODO - remove this function but callers are many
BOOL connectedToInternetViaLan(UINT8 *pWhichLANAdapter)
{
    BOOL bFeatEnabled, bRet;

    bRet = FALSE;

    return (bRet);
}


/*****************************************************************************
 * Name:                fnCheckAllowDistributorConnection
 * Description:  
 *      When we fail to connect to the distributor, we do not attempt again
 *      for a minimum amount of time.  If th user tries to connect during 
 *      that time, we go directly to backup URLs for the services that have 
 *      them.  
 * Inputs:   
 *      None.
 * Returns:  
 *      BOOL:    TRUE if connection to distributor is allowed.
 *              FALSE if connection to distributor is not allowed, because we 
 *                  already tried and failed within the last xx minutes.
 *
 * NOTES: 
 *
 * Revision History
 *  2013.08.17 Clarisa Bellamy - Pulled out of the canConnectToDistributor function. 
 -------------------------------------------------------------------------*/
BOOL fnCheckAllowDistributorConnection( void )
{
    UINT32  ulDistTimeSinceFailure;

	 // If NVRAM does not contain valid URL, then return error
	 if (crackUrl(&crackedDistURL, CMOSNetworkConfig.distributorUrl) != NU_SUCCESS)
	 {
         return( FALSE );
	 }

//	if(remoteLoggingIsOn())
//        (void)remoteLog( "Verify this Distrubutor Url hasn't recently failed.", NULL_PTR );

    // If we tried this URL recently and failed, check how long ago that was.
    if ((connectionFailed != FALSE) &&
        !strncmp(lastDistributorURL, CMOSNetworkConfig.distributorUrl, sizeof(lastDistributorURL) - 1) )
    {
        ulDistTimeSinceFailure = getMsClock() - timeOfFailure;

        // If it failed recently, block connection, and go directly to backup URLs:
        if( ulDistTimeSinceFailure < ulSkipDistributorTime )  
        {                                                     
            sprintf( scratchpad100, "Block attempt to connect to recently failed Dist URL for %lu mins",
                                 (ulSkipDistributorTime / ONE_MINUTE_IN_MS));
	        if(remoteLoggingIsOn())
	        {
	            (void)remoteLog( scratchpad100, NULL );
	        }
			else
			{
				fnDumpStringToSystemLog(scratchpad100);
			}
        
            return( FALSE );
        }
    }
	
    // If block-time has passed, or this URL is not the one that failed, 
    // clear the failure flag and allow the attempt to connect.
    connectionFailed = FALSE;
	
    // Save the current URL for comparision next time
    memset( lastDistributorURL, 0, sizeof(lastDistributorURL) );
	
    if(strlen(CMOSNetworkConfig.distributorUrl) < sizeof(lastDistributorURL))
        memcpy(lastDistributorURL, CMOSNetworkConfig.distributorUrl, strlen(CMOSNetworkConfig.distributorUrl));  //It fits
    else  //Truncate so we can try to match as much as possible
        memcpy(lastDistributorURL, CMOSNetworkConfig.distributorUrl, sizeof(lastDistributorURL) - 1);

    return( TRUE );
}

STATUS setDnldException(api_status_t errorEnum, int errorCode)
{
    distTaskErrorEnum = errorEnum;
    distTaskErrorCode = errorCode;
    return distTaskErrorCode;
}

/* -----------------------------------------------------------------------------------------[fnHTTPLiteDoPost]-- */
//---------------------------------------------------------------------
//	fnHTTPLiteDoPost
//---------------------------------------------------------------------
STATUS fnHTTPLiteDoPost(CHAR *uri, CHAR *bodyPrefix, CHAR *xmldoc, CHAR *recvBuffer, int maxRecvLen)
{
	UINT16						HTTP_OK = 200;
	STATUS                      status = NU_SUCCESS;
	HTTP_LITE_CLIENT_QUERY      cli;
	HTTP_LITE_DICT              *req_headers;
	HTTP_LITE_DICT              *resp_headers;
	CHAR                        uri_buffer[HTTP_URI_LEN] = {0};
//	CHAR                        len_buffer[5] = {0};
	CHAR                        *req_buffer;
//	CHAR                        *resp_buffer;
//	int							urlEncodeLen = 0;
//	int							MAX_HEADER_SIZE = 1024;
	CRACKED_URL					crackedURL;
	SSL_DESCRIPTOR              *sslDesc = NULL;
	//get some space to work in
//	req_buffer = (char *)malloc(strlen(xmldoc) * 1.5);
//  memset(req_buffer,0,strlen(xmldoc) * 1.5);

    (void)remoteLog("URL:", uri);
    (void)remoteLog("HTTPLite Post:", xmldoc);

    //Get the memory
    NU_Allocate_Memory(MEM_Cached, (void **)&req_buffer, strlen(xmldoc) * 1.5, NU_NO_SUSPEND);
    if(!req_buffer)
    {
        throwDnldException("Out of Memory!",MEMORY_ERR);
    }
    else
    {
        (void)memset(req_buffer, 0, strlen(xmldoc) * 1.5);   //clear everything
    }
    memset(recvBuffer,0,maxRecvLen);

	//encode the msg for http sending
	strcpy(req_buffer, bodyPrefix);
//	urlEncodeLen =
	urlEncode((unsigned char*)(req_buffer+strlen(bodyPrefix)), (unsigned char*)xmldoc);

	//integer to ascii
//	(void)sprintf(len_buffer,"%d",urlEncodeLen);

	/* Create a valid URI. */
	strcpy(uri_buffer, uri);
	/* Initialize the request dictionary. */
	status = HTTP_LITE_HDR_Init(&req_headers, 1, strlen(xmldoc) * 1.5);    //, DICT_KEYS_IGNORE_CASE);
	/* Initialize the response dictionary. */
	status = HTTP_LITE_HDR_Init(&resp_headers, 10, 1024); //, DICT_KEYS_IGNORE_CASE);
	/* Add transfer encoding header in the dictionary. */
//	status = HTTP_LITE_HDR_Add(req_headers, "transfer-encoding", 17, "chunked", 7);
	status = HTTP_LITE_HDR_Add(req_headers, "Content-type", 12, "application/x-www-form-urlencoded", 33);

	/* Clear the HTTP_LITE_CLIENT_QUERY structure. */
	memset(&cli, 0, sizeof(HTTP_LITE_CLIENT_QUERY));
	cli.request.uri = uri_buffer;
	cli.request.headers = req_headers;
	cli.request.body = req_buffer;
	cli.request.body_len = strlen(req_buffer);
	cli.response.headers = resp_headers;
	cli.response.body = recvBuffer;
	cli.response.total_body_len = maxRecvLen;

	/* Set up SSL if necessary */
	status = crackUrl(&crackedURL, uri);
	if (status == NU_SUCCESS)
	{
		if (strncmp(crackedURL.protocol,"https",5) == 0)
		{
			sslDesc = fnCreateSSLContext(&crackedURL);
			if (sslDesc == NULL)
			{
				dbgTrace(DBG_LVL_ERROR,"HTTP client SSL creation failure");
				status = HTTP_SSL_ERROR;
			}
			else
			{ // setup the http client structure to use SSL and provide SSL context
				cli.ssl_struct.ssl_flags = NU_HTTP_USER_CTX;
				cli.ssl_struct.ssl_context = sslDesc->sslContext;
			}
		}
	}
	else
	{
		dbgTrace(DBG_LVL_ERROR,"HTTP client cannot crack URI: %s\n", uri);
	}

	if (status != NU_SUCCESS)
	{
		return status; // Don't even attempt post call
	}

	send_PbpIO(xmldoc, PostTx,'<','>');
	do
	{
		/* Send data to the server. */
		status = HTTP_Lite_Do_Query("POST", &cli, (INT)(HTTP_LITE_WRITE_REQUEST | HTTP_LITE_READ_RESPONSE));
		if(cli.response.status_code == HTTP_OK)
		{
			//TODO Process Response
			if(strlen(cli.response.body) == 0)
			{
				status = 1; //PBPS_DISTRIBUTOR_CONNECTION_FAILED;
			}
		}
	}while (status == HTTP_ERROR_UNSENT_DATA);

	HTTP_LITE_HDR_Destroy(&req_headers);
	HTTP_LITE_HDR_Destroy(&resp_headers);

    //Free the buffers...
	if(NU_Deallocate_Memory(req_buffer) != NU_SUCCESS)
    {
        throwDnldException("Invalid Pointer",INVALID_PTR);
    }

	send_PbpIO(recvBuffer, PostRx,'<','>');

	if (sslDesc != NULL)
	{
		(void) fnDestroySSLContext(sslDesc);
	}

	return status;
}


//Dispatch service request to distributor
STATUS distributorHttpTransaction(INTERTASK *msgIn, char* recvBuffer, int recvBufferSize)
{
	char *			charPtr;
    DATETIME tempDate;
    unsigned int loopIndex;
    STATUS status = NU_SUCCESS;

    unsigned char   ucFeatureIndex;
    unsigned char   ucCurrentEnableStatus;
	char			cPcnScratchPad[30];

    BOOL            distributorIsReachable = FALSE;   
    BOOL            fEnabled;
	BOOL			fLoadAcntRecord = TRUE;
	BOOL			fLoadDlaRecord = TRUE;
	CHAR 			dlaFormName[7] = {"PBXML="};
    char 			*tmpBuffer;
    char			lastChar;
    char 			pcnSSL[2] = { 'S', 0 };
    int  			tmpBufferSize = 1024;

    talkedToDistributor = FALSE;   

    if( fnCheckAllowDistributorConnection())
	{
        distributorIsReachable = TRUE;
	}
    else
    {
		// distributorIsReachable is still init'ed to FALSE
	    if(remoteLoggingIsOn())
	        (void)remoteLog( "Distributor URL is blank.", NULL_PTR );
    }

    do
    {
        if(distributorIsReachable == FALSE)
        	break;

        //Get the memory
        NU_Allocate_Memory(MEM_Cached, (void **)&tmpBuffer, tmpBufferSize, NU_NO_SUSPEND);
        if(!tmpBuffer)
        {
        	status = setDnldException(API_OUT_OF_MEMORY, MEMORY_ERR);
            break;
        }
        else
        {
            (void)memset(tmpBuffer, 0, tmpBufferSize);   //clear everything
        }
        
        //login XML...
        strcpy(tmpBuffer, "<SignonReq>\r\n\t<PrdIdtPcn module=\"UIC\" serialNum=\"");

        //How's the Serial # lookin..?
		CMOSSignature.bUicSN[SIZEOFUICSN-1] = 0;
        if(!strlen(CMOSSignature.bUicSN) || (strlen(CMOSSignature.bUicSN) > SIZEOFUICSN-1))
        {
        	status = setDnldException(API_INVALID_SERIAL_NUMBER, INVALID_SN);
            break;
        }
        
        for(loopIndex = 0; loopIndex < strlen(CMOSSignature.bUicSN); loopIndex++)         
        {
            if(!isprint(CMOSSignature.bUicSN[loopIndex]))
            {
            	status = setDnldException(API_INVALID_SERIAL_NUMBER, INVALID_SN);
               break;
            }
        }
    
        if (status != NU_SUCCESS)
        	break;

        //Looks OK...
        strcat(tmpBuffer, CMOSSignature.bUicSN);

        //add the UIC SW version #
        strcat(tmpBuffer, "\" swVers=\"");
        strcat(tmpBuffer, UicVersionString); //UicVersionString "5.00.0077.0001"
        strcat(tmpBuffer, "\">");

        //How's the PCN lookin..?
        CMOSSignature.bUicPcn[SIZEOFUICPCN-1] = 0;
	    if ((strlen(CMOSSignature.bUicPcn) > SIZEOFUICPCN-1) || (strlen(CMOSSignature.bUicPcn) < 3))
        {
	    	status = setDnldException(API_INVALID_PCN, INVALID__PCN);
	    	break;
	    }
        
        for(loopIndex = 0; loopIndex < strlen(CMOSSignature.bUicPcn); loopIndex++)         
        {
        	lastChar = CMOSSignature.bUicPcn[loopIndex];
            if(!isprint(CMOSSignature.bUicPcn[loopIndex]))
	        {
            	status = setDnldException(API_INVALID_PCN, INVALID__PCN);
            	break;
            }
        }

        if (status != NU_SUCCESS)
        	break;

        //Looks OK...
        strcat(tmpBuffer, CMOSSignature.bUicPcn);
        if (lastChar != pcnSSL[0])
        	strcat(tmpBuffer, pcnSSL);
        strcat(tmpBuffer, "</PrdIdtPcn>\r\n\t<PrdIdtPcn module=\"PSD\" serialNum=\"");

        memset(scratchPad, 0, sizeof(scratchPad));   //clear everything

        //How's the serial number lookin..?
        BOOL bobstatus = fnValidData(BOBAMAT0, VLT_INDI_FILE_PBP_SERIAL, scratchPad);
        if(bobstatus != BOB_OK)
        {
            free(tmpBuffer);
            status = setDnldException(API_INCORRECT_SERIAL_NUMBER, INVALID_SN);
            break;
        }   

        strcat(tmpBuffer, scratchPad);
        strcat(tmpBuffer, "\">");

        memset(scratchPad, 0, sizeof(scratchPad));   //clear everything

        bobstatus = fnValidData(BOBAMAT0, GET_VLT_METER_PCN, scratchPad);
		if(bobstatus != BOB_OK)
        {
            free(tmpBuffer);
            status = setDnldException(API_INCORRECT_PCN, INVALID__PCN);
            break;
        }   
    
        //Looks OK...
        strcat(tmpBuffer, scratchPad);

#ifdef ALLOW_NON_SSL
        // Replace runtime option for using SSL with build time option because
        // old "Secure Communications Required" feature would be disabled by default (no SSL by default, which is no good)
#ifndef NO_SSL
        (void)strcat(tmpBuffer, "S");
#endif

#else
        (void)strcat(tmpBuffer, "S");
#endif
        strcat(tmpBuffer, "</PrdIdtPcn>\r\n\t<CusPbpAcc>");

        //How's the Customer Account # lookin..?
        memset(scratchPad, 0, sizeof(scratchPad));
        fnPackedBcdToAsciiBcd(scratchPad, (const unsigned char *)&CMOSSetupParams.lwPBPAcctNumber,
											sizeof(CMOSSetupParams.lwPBPAcctNumber));
        if(!strlen(scratchPad))
        {
        	status = setDnldException(API_INVALID_ACCOUNT_NUMBER, BAD_CUST_ACCOUNT_NUM);
        	break;
        }
        
        for(loopIndex = 0; loopIndex < strlen(scratchPad); loopIndex++)         
        {
            if(!isprint(scratchPad[loopIndex]))
            {
            	status = setDnldException(API_INVALID_ACCOUNT_NUMBER, BAD_CUST_ACCOUNT_NUM);
            	break;
            }
        }

        if (status != NU_SUCCESS)
        	break;

        //Looks OK...
        strcat(tmpBuffer, scratchPad);
        strcat(tmpBuffer, "</CusPbpAcc>");
    
	    // Now check the service requested in the incoming InterTask message. 
        switch (msgIn->IntertaskUnion.bByteData[0])
        {
            case ET_CALL_HOME:
            case SWDOWNLOAD_SERVICE:
            {
                operatorPushedCheckForUpdatesButton = TRUE;
                break; 
            }

            case ACCTBAL_SERVICE:
            {
                strcat(tmpBuffer, "\r\n\t<PrdSvcReq module=\"PSD\">AcctBal</PrdSvcReq>");
				
				//ask for refill url too
                strcat(tmpBuffer, "\r\n\t<PrdSvcReq module=\"PSD\">Refill</PrdSvcReq>");
                break; 
            }

            case REFILL_SERVICE:
            {
                strcat(tmpBuffer, "\r\n\t<PrdSvcReq module=\"PSD\">Refill</PrdSvcReq>");
                break; 
            }
    
            case REFUND_SERVICE:
            {
                strcat(tmpBuffer, "\r\n\t<PrdSvcReq module=\"PSD\">Refund</PrdSvcReq>");
                break; 
            }
    
            case DCAP_UPLOAD_SERVICE:
            {
                strcat(tmpBuffer, "\r\n\t<PrdSvcReq module=\"PSD\">DcpUpld</PrdSvcReq>");
                break; 
            }
    
            case CONFIRMATION_SERVICE:
            {
                strcat(tmpBuffer, "\r\n\t<PrdSvcReq module=\"PSD\">DelcUpld</PrdSvcReq>");
                break; 
            }

            case METER_MOVE_SERVICE:
            {
                strcat(tmpBuffer, "\r\n\t<PrdSvcReq module=\"PSD\">Move</PrdSvcReq>");
                break;
            }
    
            case TRAN_RECORD_UPLOAD_SERVICE:
            {
            	strcat(tmpBuffer, "\r\n\t<PrdSvcReq module=\"PSD\">DcpUpld</PrdSvcReq>");
            	//strcat(tmpBuffer, "\r\n\t<PrdSvcReq module=\"PSD\">TrcUpld</PrdSvcReq>");
            	break;
            }

            default:
            {
            	if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "Invalid service request = %02Xh", msgIn->IntertaskUnion.bByteData[0]);
                status = setDnldException(API_INVALID_SERVICE_REQUEST, BAD_SERVICE_REQUEST);
                break;
            }
        }

        if (status != NU_SUCCESS)
        	break;
		
        // Need to get the update info from DLA except when doing a withdrawal
        if(msgIn->IntertaskUnion.bByteData[0] != REFUND_SERVICE) 
        {
            strcat(tmpBuffer, "\r\n\t<PrdSvcReq module=\"UIC\">SwDnld</PrdSvcReq>");
        }

        // load the PSD battery born on date
        fnBobGetPSDBatteryDate(&tempDate);
        (void)strcat(tmpBuffer, "\r\n\t<PsdBatBorn>");
        (void)sprintf(scratchPad, "%02d%02d/%02d/%02d",
                        tempDate.bCentury, tempDate.bYear, tempDate.bMonth, tempDate.bDay);
        (void)strcat(tmpBuffer, scratchPad);
        (void)strcat(tmpBuffer, "</PsdBatBorn>");

        // load the PSD battery days til EOL
        (void)strcat(tmpBuffer, "\r\n\t<PsdBatDaysRem>");
        (void)sprintf(scratchPad, "%u", fnBobGetPSDRemainingDays());
        (void)strcat(tmpBuffer, scratchPad);
        (void)strcat(tmpBuffer, "</PsdBatDaysRem>");

        // indicate if calling via modem or PC proxy or lan
        fnXMLAppendConnectivityInfo( tmpBuffer, TRUE );
    
#if defined (JANUS)     // must be Janus or K700 or G900
#if !defined (K700)
        // indicate waste ink tank status
        switch(fnCMGetWasteTankStatus())
        {
            case W_WTANK_ABOUT_FULL:
                (void)strcat(tmpBuffer, "\r\n\t<WasteInkStatus>3 month</WasteInkStatus>");
                break;
            
            case W_WTANK_NEAR_FULL:
                (void)strcat(tmpBuffer, "\r\n\t<WasteInkStatus>1 month</WasteInkStatus>");
                break;
            
            case W_WTANK_FULL:
                (void)strcat(tmpBuffer, "\r\n\t<WasteInkStatus>Full</WasteInkStatus>");
                break;
            
            default:
                // don't send anything
                break;
        }
    
        // indicate ink compatibility
        switch(fnCMGetInkTankMfgType())
        {
            case OUR_OEM_MFG:
                (void)strcat(tmpBuffer, "\r\n\t<InkMfgType>OEM</InkMfgType>");
                break;
        
            case SOME_OTHER_MFG:
                (void)strcat(tmpBuffer, "\r\n\t<InkMfgType>nonOEM</InkMfgType>");
                break;
        
            case UNKNOWN_MFG:
            default:
                (void)strcat(tmpBuffer, "\r\n\t<InkMfgType>Unknown</InkMfgType>");
                break;
        }
    
        // load ink tank serial number
        {
			(void)memset(scratchPad, 0, sizeof(scratchPad));
            fnCMGetInkTankSerialNumber(scratchPad);
    
            (void)strcat(tmpBuffer, "\r\n\t<InkTkSerNum>");
            (void)strcat(tmpBuffer, scratchPad);
            (void)strcat(tmpBuffer, "</InkTkSerNum>");
        }
    
        // load ink tank manufacture date
        {
            fnCMGetInkTankMfgDate(&tempDate);
    
    
            (void)strcat(tmpBuffer, "\r\n\t<InkTkMfgDate>");
            (void)sprintf(scratchPad, "%02d%02d/%02d/%02d",
                    tempDate.bCentury, tempDate.bYear, tempDate.bMonth, tempDate.bDay);
            (void)strcat(tmpBuffer, scratchPad);
            (void)strcat(tmpBuffer, "</InkTkMfgDate>");
        }
#endif
#endif
 
        strcat(tmpBuffer, "</SignonReq>");
        
        // Use the SignOn message to sign onto the distributor. 
        //  If successful, this waits for a reply.
        status = fnHTTPLiteDoPost(CMOSNetworkConfig.distributorUrl, dlaFormName, tmpBuffer, recvBuffer, recvBufferSize);
        if(status != NU_SUCCESS)
        {
            distributorIsReachable = FALSE;
         	status = setDnldException(API_STATUS_DATA_CENTER_COMM_FAIL, status);

            //if upload in progress send base event to tablet
            if(fUploadInProgress == TRUE)
			{
				trmUploadFailed(status);
			}
        }
		else
		{
		    if(remoteLoggingIsOn())
		    {
		        (void)remoteLog("XML response from distributor -\n", recvBuffer);
		    }
                                                                                            
		    charPtr = findString("PBXML", recvBuffer); //validation #1
		    if(!charPtr) //Did we find PBXML?
		    {
		        if(remoteLoggingIsOn())                                                                       
		        {
		            (void)remoteLog("Invalid XML reply from server -> ", recvBuffer);
		        }
        
		        charPtr = findString("The page cannot be displayed", recvBuffer);
		        if (charPtr)
		        {
		            logException("The page cannot be displayed, possible invalid Distributor URL entry");
			        status = INVALID_URL;
		        }
		        else
		        {
		            logException("No PBXML in reply from Distributor");
			        status = XML_REPLY_ERR;
		        }

	            distributorIsReachable = FALSE;

				// indicate that the connection failed
	            connectionFailed = TRUE;

	            // Save the time, so that we can block attempts to connect for a short time.
	            timeOfFailure = getMsClock(); 

                //send message to tablet with error status
                //TODO - see if general message can be sent
                if(msgIn->IntertaskUnion.bByteData[0] == ACCTBAL_SERVICE)
                {
            	   status = setDnldException(API_STATUS_DATA_CENTER_COMM_FAIL, status);
                }

		    }
		}
        
    	if(NU_Deallocate_Memory(tmpBuffer) != NU_SUCCESS)
        {
    		status = setDnldException(API_INVALID_POINTER,INVALID_PTR);
    		break;
        }

    } while(0);
	
    if (status != NU_SUCCESS)
    	return status;

    do
    {
        // if we had a problem connecting to the distributor, use the backup URLs, if they exist...
        if (distributorIsReachable != FALSE)
        	break;

		// since we're going to try to use the fake message, clear the status
		status = NU_SUCCESS;

		// make sure the URLs are nul-terminated
		CMOSNetworkConfig.backupPbpUrl[255] = 0;
		CMOSNetworkConfig.backupAcntUrl[255] = 0;
		CMOSNetworkConfig.backupDlaUrl[255] = 0;

		// log error in CMOS, but don't tell the operator about it
        fnLogError(ERR_TYPE_NORMAL, ERROR_CLASS_PBP,(unsigned char)(PBPS_USING_BACKUP_URLS - XML_USER_ERROR_BASE));
        fnLogSystemError( ERROR_CLASS_PBP, (unsigned char)(PBPS_USING_BACKUP_URLS - XML_USER_ERROR_BASE), 0, 0, 0 );

//        if(remoteLoggingIsOn())
//            (void)remoteLog("Either Distributor URL is blank or we were unable to connect to the Distributor.", NULL);
		fnDumpStringToSystemLog("Either Distributor URL is blank or we were unable to connect to the Distributor.");

//        if(remoteLoggingIsOn())
//            (void)remoteLog("Checking for backup URLs...", NULL);
		fnDumpStringToSystemLog("Checking for backup URLs...");
        
        if (!(*CMOSNetworkConfig.backupPbpUrl) && remoteLoggingIsOn())
            (void)remoteLog("Backup Data Center URL is blank too! (Exception may eventually be thrown)", NULL);
        
		// load the very beginning of the message
        (void)memset(recvBuffer, 0, recvBufferSize);
        (void)strcpy(recvBuffer, "PBXML=<SignonRsp><CmdStaCmp>0</CmdStaCmp>");
        (void)strcat(recvBuffer, "<PrdSvcInf><DstSmt><DstSmtEnt><DstSmtSta>0</DstSmtSta><DstSmtPcn>");

		// load the PCN in the first record in the message
        (void)memset(cPcnScratchPad, 0, sizeof(cPcnScratchPad));   //clear everything
        if(fnValidData(BOBAMAT0, GET_VLT_METER_PCN, cPcnScratchPad) != BOB_OK)
        {
        	status = setDnldException(API_INCORRECT_PCN_BACKUP_URL, INVALID__PCN);
        	break;
        }

        (void)strcat(recvBuffer, cPcnScratchPad);

        switch (msgIn->IntertaskUnion.bByteData[0])
        {
            case ET_CALL_HOME:
            case SWDOWNLOAD_SERVICE:
            {
				// indicate that records for the Accounting Server and DLA don't need to be added
				// when we get out of this switch statement since we don't go to the Accounting Server
				// for a direct connect to DLA and we're loading the DLA record in this case statement.
				fLoadAcntRecord = FALSE;
				fLoadDlaRecord = FALSE;

                if (!(*CMOSNetworkConfig.backupDlaUrl))
                {
                	status = setDnldException(API_BACKUP_DLA_URL_IS_BLANK, BAD_DLA_BACKUP_URL);
                	break;
                }
                
                operatorPushedCheckForUpdatesButton = TRUE;
				
                strcat(recvBuffer, "</DstSmtPcn><DstSmtSvc>SwDnld</DstSmtSvc><DstSmtUrl>");
                strcat(recvBuffer, CMOSNetworkConfig.backupDlaUrl);
                strcat(recvBuffer, "</DstSmtUrl></DstSmtEnt>");
				
                break; 
            }

            case ACCTBAL_SERVICE:
            {
                if (*CMOSNetworkConfig.backupPbpUrl)
                {
                    //Looks OK...
                    strcat(recvBuffer, "</DstSmtPcn><DstSmtSvc>AcctBal</DstSmtSvc><DstSmtUrl>");
                    strcat(recvBuffer, CMOSNetworkConfig.backupPbpUrl);
                    strcat(recvBuffer, "</DstSmtUrl></DstSmtEnt>");

                    //supply refill url too
                    strcat(recvBuffer, "<DstSmtEnt><DstSmtSta>0</DstSmtSta><DstSmtPcn>");
                    strcat(recvBuffer, cPcnScratchPad);
                    strcat(recvBuffer, "</DstSmtPcn><DstSmtSvc>Refill</DstSmtSvc><DstSmtUrl>");
                    strcat(recvBuffer, CMOSNetworkConfig.backupPbpUrl);
                    strcat(recvBuffer, "</DstSmtUrl></DstSmtEnt>");
                }
                else
                {// Distributor is not reachable and there is no backup PBP URL
                	status = NU_RETURN_ERROR;
                }
                break; 
            }

            case REFILL_SERVICE:
            {
                if(!(*CMOSNetworkConfig.backupPbpUrl))
                {
                	status = setDnldException(API_BACKUP_TIER3_URL_IS_BLANK, BAD_PBP_BACKUP_URL);
                	break;
                }
                
                //Looks OK...
                strcat(recvBuffer, "</DstSmtPcn><DstSmtSvc>Refill</DstSmtSvc><DstSmtUrl>");
                strcat(recvBuffer, CMOSNetworkConfig.backupPbpUrl);
                strcat(recvBuffer, "</DstSmtUrl></DstSmtEnt>");

                break; 
            }
    
            case REFUND_SERVICE:
            {
				// indicate that record for DLA doesn't need to be added when we get out of this switch statement
				// since there's no need to get an update from DLA if the meter is being withdrawn.
				fLoadDlaRecord = FALSE;

                if(!(*CMOSNetworkConfig.backupPbpUrl))
                {
                	status = setDnldException(API_BACKUP_TIER3_URL_IS_BLANK, BAD_PBP_BACKUP_URL);
                	break;
                }
                
                //Looks OK...
                strcat(recvBuffer, "</DstSmtPcn><DstSmtSvc>Refund</DstSmtSvc><DstSmtUrl>");
                strcat(recvBuffer, CMOSNetworkConfig.backupPbpUrl);
                strcat(recvBuffer, "</DstSmtUrl></DstSmtEnt>");

                break; 
            }
    
            case DCAP_UPLOAD_SERVICE:
            {
                if(!(*CMOSNetworkConfig.backupPbpUrl))
                {
                	status = setDnldException(API_BACKUP_TIER3_URL_IS_BLANK, BAD_PBP_BACKUP_URL);
                	break;
                }
                
                //Looks OK...
                strcat(recvBuffer, "</DstSmtPcn><DstSmtSvc>DcpUpld</DstSmtSvc><DstSmtUrl>");
                strcat(recvBuffer, CMOSNetworkConfig.backupPbpUrl);
                strcat(recvBuffer, "</DstSmtUrl></DstSmtEnt>");

                break; 
            }
    
            case CONFIRMATION_SERVICE:
            {
                if(!(*CMOSNetworkConfig.backupPbpUrl))
                {
                	status = setDnldException(API_BACKUP_TIER3_URL_IS_BLANK, BAD_PBP_BACKUP_URL);
                	break;
                }
                
                //Looks OK...
                strcat(recvBuffer, "</DstSmtPcn><DstSmtSvc>DelcUpld</DstSmtSvc><DstSmtUrl>");
                strcat(recvBuffer, CMOSNetworkConfig.backupPbpUrl);
                strcat(recvBuffer, "</DstSmtUrl></DstSmtEnt>");

                break; 
            }
    
            case METER_MOVE_SERVICE:
            {
                if(!(*CMOSNetworkConfig.backupPbpUrl))
                {
                	status = setDnldException(API_BACKUP_TIER3_URL_IS_BLANK, BAD_PBP_BACKUP_URL);
                	break;
                }
                
                //Looks OK...
                strcat(recvBuffer, "</DstSmtPcn><DstSmtSvc>Move</DstSmtSvc><DstSmtUrl>");
                strcat(recvBuffer, CMOSNetworkConfig.backupPbpUrl);
                strcat(recvBuffer, "</DstSmtUrl></DstSmtEnt>");

                break; 
            }
    
            case DATA_UPLOAD_SERVICE:
            {
                if(!(*CMOSNetworkConfig.backupPbpUrl))
                {
                	status = setDnldException(API_BACKUP_TIER3_URL_IS_BLANK, BAD_PBP_BACKUP_URL);
                	break;
                }
                
                strcat(recvBuffer, CMOSNetworkConfig.backupPbpUrl);
                strcat(recvBuffer, "</DstSmtUrl></DstSmtEnt>");

                break; 
            }
            case TRAN_RECORD_UPLOAD_SERVICE:
			{
				if(!(*CMOSNetworkConfig.backupPbpUrl))
				{
					status = setDnldException(API_BACKUP_TIER3_URL_IS_BLANK, BAD_PBP_BACKUP_URL);
					break;
				}

				//Looks OK...
				strcat(recvBuffer, "</DstSmtPcn><DstSmtSvc>DcpUpld</DstSmtSvc><DstSmtUrl>");
				//strcat(recvBuffer, "</DstSmtPcn><DstSmtSvc>TrcUpld</DstSmtSvc><DstSmtUrl>");
				strcat(recvBuffer, CMOSNetworkConfig.backupPbpUrl);
				strcat(recvBuffer, "</DstSmtUrl></DstSmtEnt>");

				break;
			}
            default:
            {
            	if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "Invalid service request = %02Xh", msgIn->IntertaskUnion.bByteData[0]);
                status = setDnldException(API_INVALID_SERVICE_REQUEST, BAD_SERVICE_REQUEST);
                break;
            }
        }
		
        if (status != NU_SUCCESS)
        	break;

		// if threw an exception, we wouldn't have gotten here, so check if we need to
		// add records for the Accounting Server and DLA
		if (fLoadAcntRecord == TRUE)
		{
/*
			if (IsAbAccountingUploadEnabled())
			{
                if (!(*CMOSNetworkConfig.backupAcntUrl))
				{
					fnDumpStringToSystemLog("Backup Acnt Server URL is blank!");
					logException("Backup Acnt Server URL is blank!");
				}
				else
				{
	                strcat(recvBuffer, "<DstSmtEnt><DstSmtSta>0</DstSmtSta><DstSmtPcn>");
	                strcat(recvBuffer, cPcnScratchPad);
	                strcat(recvBuffer, "</DstSmtPcn><DstSmtSvc>AcctUpld</DstSmtSvc><DstSmtUrl>");
	                strcat(recvBuffer, CMOSNetworkConfig.backupAcntUrl);
	                strcat(recvBuffer, "</DstSmtUrl></DstSmtEnt>");
				}
			}
*/
		}
		
		if (fLoadDlaRecord == TRUE)
		{
            if (!(*CMOSNetworkConfig.backupDlaUrl))
			{
				fnDumpStringToSystemLog("Backup DLA URL is blank!");
				logException("Backup DLA URL is blank!");
			}
			else
			{
                strcat(recvBuffer, "<DstSmtEnt><DstSmtSta>0</DstSmtSta><DstSmtPcn>");
                strcat(recvBuffer, cPcnScratchPad);
                strcat(recvBuffer, "</DstSmtPcn><DstSmtSvc>SwDnld</DstSmtSvc><DstSmtUrl>");
                strcat(recvBuffer, CMOSNetworkConfig.backupDlaUrl);
                strcat(recvBuffer, "</DstSmtUrl></DstSmtEnt>");
			}
		}
				
		// if threw an exception, we wouldn't have gotten here, so load the end of the message.
        strcat(recvBuffer, "</DstSmt></PrdSvcInf></SignonRsp>");
		
	    if( fnGetLocalLoggingState() == TRUE )
	    {
			fnDumpStringToSystemLog("Fake Dist Response Msg:");
	        fnDumpStringToSystemLog(recvBuffer);
	    }
    } while(0);

    return status;
}

//---------------------------------------------------------------------
//	addSystemErrorLog
//---------------------------------------------------------------------
void addSystemErrorLog(char *fileUpdateRequestPtr, int fileUpdateRequestSize)
{
    SystemErrorLogEntry *pLog;
    ErrorLogEntry *pErrLog;
    UINT8 realLogIndex;
    UINT8 adjustedLogIndex;
    int     g /*, rv*/;
    char   tempString[20] = {0};   // Used for conversion from binary to ascii.

	(void)strcat(fileUpdateRequestPtr, "<SystemErrorLog>");

	// The log index is pointing to the next log to write, so the most
	// recent log is one back
	if (CMOSSystemErrorLog.LogIndex == 0)
		realLogIndex = SYS_ERR_LOG_SZ - 1;
	else
		realLogIndex = CMOSSystemErrorLog.LogIndex - 1;

	tempString[14] = 0;

	for (g = 0; g < SYS_ERR_LOG_SZ; g++)
	{
		// Get the appropriate record
		if (g > realLogIndex)
			adjustedLogIndex = (unsigned char)(realLogIndex - g + SYS_ERR_LOG_SZ);
		else
			adjustedLogIndex = (unsigned char)(realLogIndex - g);

		pLog = &CMOSSystemErrorLog.SystemErrorTable[adjustedLogIndex];

		// If the record has no date, it's unused, so exit the loop
		if (pLog->rDateTime.bCentury == 0)
			break;

		tempString[0] = (char)((pLog->rDateTime.bCentury / 10) + 0x30);
		tempString[1] = (pLog->rDateTime.bCentury % 10) + 0x30;
		tempString[2] = (char)((pLog->rDateTime.bYear / 10) + 0x30);
		tempString[3] = (pLog->rDateTime.bYear % 10) + 0x30;
		tempString[4] = (char)((pLog->rDateTime.bMonth / 10) + 0x30);
		tempString[5] = (pLog->rDateTime.bMonth % 10) + 0x30;
		tempString[6] = (char)((pLog->rDateTime.bDay / 10) + 0x30);
		tempString[7] = (pLog->rDateTime.bDay % 10) + 0x30;
		tempString[8] = (char)((pLog->rDateTime.bHour / 10) + 0x30);
		tempString[9] = (pLog->rDateTime.bHour % 10) + 0x30;
		tempString[10] = (char)((pLog->rDateTime.bMinutes / 10) + 0x30);
		tempString[11] = (pLog->rDateTime.bMinutes % 10) + 0x30;
		tempString[12] = (char)((pLog->rDateTime.bSeconds / 10) + 0x30);
		tempString[13] = (pLog->rDateTime.bSeconds % 10) + 0x30;

		(void)strcat( fileUpdateRequestPtr, "<EntryDateTime>" );
		(void)strcat( fileUpdateRequestPtr, tempString );
		(void)strcat( fileUpdateRequestPtr, "</EntryDateTime><EntryCode>" );

		(void)strcat( fileUpdateRequestPtr, MakeIntoAscii( (&pLog->bSysErrorClass), FIVE_BYTES_INTO_ASCII ) );
		(void)strcat( fileUpdateRequestPtr, "</EntryCode>" );
	}

	(void)strcat(fileUpdateRequestPtr, "</SystemErrorLog><ErrorLog>");

	// The log index is pointing to the next log to write, so the most
	// recent log is one back
	if (CMOSErrorLog.LogIndex == 0)
		realLogIndex = ERR_LOG_SZ - 1;
	else
		realLogIndex = CMOSErrorLog.LogIndex - 1;

	tempString[14] = 0;

	for (g = 0; g < ERR_LOG_SZ; g++)
	{
		// Get the appropriate record
		if (g > realLogIndex)
			adjustedLogIndex = (unsigned char)(realLogIndex - g + ERR_LOG_SZ);
		else
			adjustedLogIndex = (unsigned char)(realLogIndex - g);

		pErrLog = &CMOSErrorLog.ErrorTable[adjustedLogIndex];

		// If the record has no date, it's unused, so exit the loop
		if (pErrLog->rDateTime.bCentury == 0)
			break;

		tempString[0] = (char)((pErrLog->rDateTime.bCentury / 10) + 0x30);
		tempString[1] = (pErrLog->rDateTime.bCentury % 10) + 0x30;
		tempString[2] = (char)((pErrLog->rDateTime.bYear / 10) + 0x30);
		tempString[3] = (pErrLog->rDateTime.bYear % 10) + 0x30;
		tempString[4] = (char)((pErrLog->rDateTime.bMonth / 10) + 0x30);
		tempString[5] = (pErrLog->rDateTime.bMonth % 10) + 0x30;
		tempString[6] = (char)((pErrLog->rDateTime.bDay / 10) + 0x30);
		tempString[7] = (pErrLog->rDateTime.bDay % 10) + 0x30;
		tempString[8] = (char)((pErrLog->rDateTime.bHour / 10) + 0x30);
		tempString[9] = (pErrLog->rDateTime.bHour % 10) + 0x30;
		tempString[10] = (char)((pErrLog->rDateTime.bMinutes / 10) + 0x30);
		tempString[11] = (pErrLog->rDateTime.bMinutes % 10) + 0x30;
		tempString[12] = (char)((pErrLog->rDateTime.bSeconds / 10) + 0x30);
		tempString[13] = (pErrLog->rDateTime.bSeconds % 10) + 0x30;

		(void)strcat( fileUpdateRequestPtr, "<EntryDateTime>" );
		(void)strcat( fileUpdateRequestPtr, tempString );
		(void)strcat( fileUpdateRequestPtr, "</EntryDateTime><EntryCode>" );

		(void)strcat( fileUpdateRequestPtr, MakeIntoAscii( (&pErrLog->bErrorType), THREE_BYTES_INTO_ASCII ) );
		(void)strcat( fileUpdateRequestPtr, "</EntryCode><RepeatCount>" );

		(void)sprintf( tempString, "%u", pErrLog->bRepeatCount );
		(void)strcat( fileUpdateRequestPtr, tempString );
		(void)strcat( fileUpdateRequestPtr, "</RepeatCount>" );
	}

	(void)strcat(fileUpdateRequestPtr, "</ErrorLog>");

}

//---------------------------------------------------------------------
//	ratesComponentsAreStored
//---------------------------------------------------------------------
BOOL ratesComponentsAreStored(void)
{
    unsigned int loopIndex;

    for(loopIndex = 0 ; loopIndex < MAX_CMOS_RATE_COMPONENTS; loopIndex++)
    {
        if(strlen((const char *)CMOSRateCompsMap.CMOSRateComponents[loopIndex].SWPartNO) &&    //There is an entry
            (CMOSRateCompsMap.CMOSRateComponents[loopIndex].DeviceID == RATES_DATA_COMPONENT))//and it's rate data
        {
            return TRUE;
        }
    }
    return FALSE;
}

//---------------------------------------------------------------------
//	dcdComponentsAreStored
//---------------------------------------------------------------------
BOOL dcdComponentsAreStored(void)
{
    unsigned int loopIndex;

    for(loopIndex = 0 ; loopIndex < MAX_CMOS_RATE_COMPONENTS; loopIndex++)
    {
        if((strlen((const char *)CMOSRateCompsMap.CMOSRateComponents[loopIndex].SWPartNO) > 0) &&    //There is an entry
            ((CMOSRateCompsMap.CMOSRateComponents[loopIndex].DeviceID == COMPRESSED_DCAP_DATA_COMPONENT)  ||
            (CMOSRateCompsMap.CMOSRateComponents[loopIndex].DeviceID == DCAP_DATA_COMPONENT)) )//and it's dc data
        {
            return TRUE;
        }
    }
    return FALSE;
}

/* *************************************************************************
// FUNCTION NAME:       buildFileUpdateRequest
// DESCRIPTION:
//      Creates the first message sent to DLA, which is known as the File
//      Update Request message.  This file contains a bunch of information
//      about the meter.
// INPUTS:
//      fileUpdateRequestPtr - Pointer to the buffer where the message is to
//                      be built.
// OUTPUTS:
//       Return NU_SUCCESS.  If there is a problem, this function calls
//                      throwDnldException, which does not return, but does
//                      allow other tasks to run.
// NOTES:
//  1. .
//
// HISTORY:
//  2011.08.14 Clarisa Bellamy - Modify so that if there is no DCE, the default
//                       partnumber is put into the request so that a DCE will
//                       be downloaded from DLA.
// --------------------------------------------------------------------------*/
STATUS buildFileUpdateRequest(char *fileUpdateRequestPtr, int fileUpdateRequestSize,
							  char* pcn, char* serialNum, int systemErrorLog)
{
	STATUS status = NU_SUCCESS;
    DOWNLOAD_FILE_ENTRY *dlFileEntry;
    UINT8 * LinkPtr;        // LinkPtr of current graphic to check, if it is NULL_PTR, then there aren't any more.
    UINT8 * pDataNameVer;   // Ptr to dataNameVer in GF-file directory entry.
//    char    *pTempStr;              // Temp pointer to an ascii string.
    char    *pRMPartNum = NULL_PTR;

    unsigned int        loopIndex;
//    int     readCount;
//    int     errorCode;
    int     g /*, rv*/;

    UINT16  featureList[MAX_FEATURES];
    UINT16  wDirInx;        // Index into GF file directory of the current graphic to check.

    UINT8   numFeatures;

    unsigned char   ucFeatureIndex;
    unsigned char   ucCurrentEnableStatus;

//    char    EMDVer[10];
    char   tempString[20] = {0};   // Used for conversion from binary to ascii.
    UINT8   bWhichLAN;
    char resultBuf[ sizeof(cmosEuroConversion.ucCurrencyID) + 1 ];
    char pTmpBuff1[50] = {0};
    char *pTmp;
    char    fCode[5];

    BOOL     fEnabled = FALSE;
//	BOOL	fForceHTTPS = FALSE;


    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK running buildFileUpdateRequest . . .");
    do
    {
		if( fileUpdateRequestPtr == NULL_PTR )
		{
			status = setDnldException(API_NULL_POINTER,NULL_PTR_ERR);
			break;
		}

		(void)strcpy(fileUpdateRequestPtr, "<FileUpdateRequest><PrdPcn>");

		//How's the PCN lookin..?
		if(strlen(CMOSSignature.bUicPcn) > SIZEOFUICPCN-1 ||  strlen(CMOSSignature.bUicPcn) < 3)
		{
			status = setDnldException(API_INVALID_PCN,INVALID__PCN);
			break;
		}

		for(loopIndex = 0; loopIndex < strlen(CMOSSignature.bUicPcn); loopIndex++)
		{
			if(!isprint(CMOSSignature.bUicPcn[loopIndex]))
			{
				status = setDnldException(API_INVALID_PCN,INVALID__PCN);
				break;
			}
		}
		if (status != NU_SUCCESS)
			break;

		//Looks OK...
		(void)strcpy(pcn, CMOSSignature.bUicPcn);
		(void)strcat(fileUpdateRequestPtr, CMOSSignature.bUicPcn);
		(void)strcat(fileUpdateRequestPtr, "</PrdPcn>");
		if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK buildFileUpdateRequest pcn %s", CMOSSignature.bUicPcn);

		(void)strcat(fileUpdateRequestPtr, pTmpBuff1);
		(void)strcat(fileUpdateRequestPtr, "<PrdSer>");

		//How's the Serial # lookin..?
		if(!strlen(CMOSSignature.bUicSN) || (strlen(CMOSSignature.bUicSN) > SIZEOFUICSN-1))
		{
			status = setDnldException(API_INVALID_SERIAL_NUMBER,INVALID_SN);
			break;
		}

		for(loopIndex = 0; loopIndex < strlen(CMOSSignature.bUicSN); loopIndex++)
		{
			if(!isprint(CMOSSignature.bUicSN[loopIndex]))
			{
				status = setDnldException(API_INVALID_SERIAL_NUMBER,INVALID_SN);
				break;
			}
		}
		if (status != NU_SUCCESS)
			break;

		//Looks OK...
		(void)strcpy(serialNum, CMOSSignature.bUicSN);
		(void)strcat(fileUpdateRequestPtr, CMOSSignature.bUicSN);
		(void)strcat(fileUpdateRequestPtr, "</PrdSer>");
		if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK buildFileUpdateRequest sn %s", CMOSSignature.bUicSN);


/* RD 13Nov2017  this section of code does not seem to do anything anymore. All comms are secure now even in proxy mode.
 *
		if (IsFeatureEnabled(&fEnabled, DLA_SECURE_DWNLD_REQUIRED_SUPPORT, &ucCurrentEnableStatus, &ucFeatureIndex))
		{
			if (fEnabled == TRUE)
			{
	//    				fForceHTTPS = TRUE;
			}
		}
		else
		{
			if( fnGetConnectionMode() == CONNECTION_MODE_AUTO_DETECT )
			{
				if ((connectedToInternetViaLan(&bWhichLAN) == TRUE) && (fnMightWeNeedToUseProxy(DLA_CONNECTION) == TRUE))
				{
	//    					fForceHTTPS = TRUE;
				}
			}
		}
*/

		(void)strcat(fileUpdateRequestPtr, "<HTTPFTP>1</HTTPFTP>");

		//Communication Type
		(void)strcat(fileUpdateRequestPtr, "<CommType>Int.");
		(void)strcat(fileUpdateRequestPtr, "LAN"); //todo change to WiFi when its active
		(void)strcat(fileUpdateRequestPtr, "</CommType>");

		//Base
		(void)strcat(fileUpdateRequestPtr, "<Base><PCN>");
		(void)strcat(fileUpdateRequestPtr, (fnGetMeterModel() == CSD2) ? "2H" : "3H");
		(void)strcat(fileUpdateRequestPtr, "</PCN><SwVersion>");
		(void)strcat(fileUpdateRequestPtr, UicVersionString);
		(void)strcat(fileUpdateRequestPtr, "</SwVersion><BaseModel>");
		fnGetPrintSerialNumber((uchar *)pTmpBuff1);
		(void)strcat(fileUpdateRequestPtr, pTmpBuff1);  //todo get the printer's serial number
		(void)strcat(fileUpdateRequestPtr, "</BaseModel><ProfileVersion /></Base>");
		if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK buildFileUpdateRequest printer sn %s", pTmpBuff1);


		// ----- Currency ID -----
		(void)strcat( fileUpdateRequestPtr, "<CurrencyID>" );
		(void)sprintf( resultBuf, "%d", cmosEuroConversion.ucCurrencyID );
		(void)strcat( fileUpdateRequestPtr, (const char *)resultBuf );
		(void)strcat( fileUpdateRequestPtr, "</CurrencyID>" );

		//Until there are "real" parameters some fields will just be "0"
		(void)strcat(fileUpdateRequestPtr, "<Software><Device Type=\"UIC\"><HdwPrt>");
		(void)strcat(fileUpdateRequestPtr, "0");
		(void)strcat(fileUpdateRequestPtr, "</HdwPrt><MinHdwVer>");
		(void)strcat(fileUpdateRequestPtr, "0");
		(void)strcat(fileUpdateRequestPtr, "</MinHdwVer><SofPrt>");

		pTmp = (char *) fnFlashGetAsciiStringParm(ASP_UIC_SW_PART_NUMBER);
		if (pTmp)
		{// Assume this is not so large that it causes overflow of buffer
			(void)strcat(fileUpdateRequestPtr, pTmp);
		}
		(void)strcat(fileUpdateRequestPtr, "</SofPrt><SofVer>");

		//fetch the UIC SW version #
		(void)strcat(fileUpdateRequestPtr, UicVersionString);
		(void)strcat(fileUpdateRequestPtr, "</SofVer><SofSerLvl>");
		if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK buildFileUpdateRequest uic version %s", UicVersionString);
		(void)strcat(fileUpdateRequestPtr, "0");
		(void)strcat(fileUpdateRequestPtr, "</SofSerLvl></Device>");

		//RM was removed JW June 7 2017

		(void)strcat(fileUpdateRequestPtr, "</Software>");

		/* Now Country Specific (CTY) Files */
		(void)strcat(fileUpdateRequestPtr, "<CountrySpec>");
		for (loopIndex = 0; loopIndex < TOTAL_CTY_ARCHIVES; loopIndex++)
		{
			if (PctyArchiveRegistry[loopIndex].cIsActive == TRUE)
			{
				(void)strcat(fileUpdateRequestPtr, "<FileC Type=\"AF\"><ConPrt>");
				(void)strcat( fileUpdateRequestPtr, PctyArchiveRegistry[loopIndex].cPartNo );
				(void)strcat(fileUpdateRequestPtr, "</ConPrt><ConVer>");
				(void)strcat( fileUpdateRequestPtr, PctyArchiveRegistry[loopIndex].cVersion );
				(void)strcat(fileUpdateRequestPtr, "</ConVer><ConSerLvl>");
				(void)strcat( fileUpdateRequestPtr, PctyArchiveRegistry[loopIndex].cServiceLevel );
				(void)strcat(fileUpdateRequestPtr, "</ConSerLvl></FileC>");
			}
		}

		(void)strcat(fileUpdateRequestPtr, "</CountrySpec>");
		if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK buildFileUpdateRequest CountrySpec");

		//Now gather the rate/zip info, if there is any
		if((CMOSRateCompsMap.ucCMOSRateSignature == CMOS_RATE_SIGNATURE) && ratesComponentsAreStored())
		{
			(void)strcat(fileUpdateRequestPtr, "<Rate>");

			for(loopIndex = 0 ; loopIndex < MAX_CMOS_RATE_COMPONENTS; loopIndex++)
			{
				if(strlen((char *)CMOSRateCompsMap.CMOSRateComponents[loopIndex].SWPartNO) &&  //There is an entry
					(CMOSRateCompsMap.CMOSRateComponents[loopIndex].DeviceID == RATES_DATA_COMPONENT))//and it's Rate Data or Zip/Zone
				{
					(void)strcat(fileUpdateRequestPtr, "<FileR><RatPrt>");
					(void)strncat(fileUpdateRequestPtr, (const char *)CMOSRateCompsMap.CMOSRateComponents[loopIndex].SWPartNO,
						sizeof(CMOSRateCompsMap.CMOSRateComponents[loopIndex].SWPartNO));
					(void)strcat(fileUpdateRequestPtr, "</RatPrt><RatVer>");
					(void)strncat(fileUpdateRequestPtr, (const char *)CMOSRateCompsMap.CMOSRateComponents[loopIndex].SWVersion,
						sizeof(CMOSRateCompsMap.CMOSRateComponents[loopIndex].SWVersion));
					(void)strcat(fileUpdateRequestPtr, "</RatVer><RatSerLvl>");
					(void)strncat(fileUpdateRequestPtr, (const char *)CMOSRateCompsMap.CMOSRateComponents[loopIndex].ServiceLevel,
						sizeof(CMOSRateCompsMap.CMOSRateComponents[loopIndex].ServiceLevel));
					(void)strcat(fileUpdateRequestPtr, "</RatSerLvl></FileR>");
				}
			}

			(void)strcat(fileUpdateRequestPtr, "</Rate>");
		}
		if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK buildFileUpdateRequest Rate");

		//Now gather the DCD info, if there is any
		if((CMOSRateCompsMap.ucCMOSRateSignature == CMOS_RATE_SIGNATURE) && dcdComponentsAreStored())
		{
			(void)strcat(fileUpdateRequestPtr, "<DCD>");

			for(loopIndex = 0 ; loopIndex < ARRAY_LENGTH(CMOSRateCompsMap.CMOSRateComponents); loopIndex++)
			{
				if(strlen((char *)CMOSRateCompsMap.CMOSRateComponents[loopIndex].SWPartNO) &&  //There is an entry
					( ( CMOSRateCompsMap.CMOSRateComponents[loopIndex].DeviceID == COMPRESSED_DCAP_DATA_COMPONENT ) ||
					  ( CMOSRateCompsMap.CMOSRateComponents[loopIndex].DeviceID == DCAP_DATA_COMPONENT )))
				{
					(void)strcat(fileUpdateRequestPtr, "<FileDCD><DCDPrt>");
					(void)strncat(fileUpdateRequestPtr, (const char *)CMOSRateCompsMap.CMOSRateComponents[loopIndex].SWPartNO,
						sizeof(CMOSRateCompsMap.CMOSRateComponents[loopIndex].SWPartNO));
					(void)strcat(fileUpdateRequestPtr, "</DCDPrt><DCDVer>");
					(void)strncat(fileUpdateRequestPtr, (const char *)CMOSRateCompsMap.CMOSRateComponents[loopIndex].SWVersion,
						sizeof(CMOSRateCompsMap.CMOSRateComponents[loopIndex].SWVersion));
					(void)strcat(fileUpdateRequestPtr, "</DCDVer><DCDSerLvl>");
					(void)strncat(fileUpdateRequestPtr, (const char *)CMOSRateCompsMap.CMOSRateComponents[loopIndex].ServiceLevel,
						sizeof(CMOSRateCompsMap.CMOSRateComponents[loopIndex].ServiceLevel));
					(void)strcat(fileUpdateRequestPtr, "</DCDSerLvl></FileDCD>");
				}
			}

			(void)strcat(fileUpdateRequestPtr, "</DCD>");
		}
		if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK buildFileUpdateRequest DCD");

		// ----- Enabled Features -----------
		if ((numFeatures = fnGetEnabledFeatureList( featureList )) > 0)
		{
			(void)strcat( fileUpdateRequestPtr, "<Ftble>" );

			for (g = 0; g < numFeatures; g++)
			{
				(void)strcat( fileUpdateRequestPtr, "<EnFtr>" );
				(void)strcat( fileUpdateRequestPtr, HexToAscii( featureList[g], fCode  ) );
				(void)strcat( fileUpdateRequestPtr, "</EnFtr>" );
			}

			(void)strcat( fileUpdateRequestPtr, "</Ftble>" );
		}
		if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK buildFileUpdateRequest Ftble");

		// ----- Graphics in Graphics File -----------
		(void)strcat( fileUpdateRequestPtr, "<Graphics>" );

		// For each used entry in the .GAR File directory, get the DataName and DataVersion,
		//  convert them to ASCII, and concat them into the buffer with delimiters.
		//  Include ALL .GAR-file graphics, no filtering.
		wDirInx = 0;
		LinkPtr = fnGFGetNextDirInxByCompType( &wDirInx, ANY_COMPONENT_TYPE, GRFX_ANY_MATCH );
		while( LinkPtr != NULL_PTR )
		{
			pDataNameVer = fnGFGetDataNamePtrByDirInx( wDirInx );
			if( pDataNameVer != NULL_PTR )
			{
				 // Convert the 8-byte binary DataName to a null-terminated, 16-byte ASCII string.
				(void)fnGrfxMakeDataNameVerIntoAscii( pDataNameVer, GRFXDATANAME, tempString );
				(void)strcat( fileUpdateRequestPtr, "<FileG><GfxPrt>" );
				(void)strcat( fileUpdateRequestPtr, tempString );
				(void)strcat( fileUpdateRequestPtr, "</GfxPrt>" );
				 //  Convert the 4-byte DataVersion to a null-terminated 8-byte ASCII string.
				(void)fnGrfxMakeDataNameVerIntoAscii( pDataNameVer, GRFXDATAVERSION, tempString );
				(void)strcat( fileUpdateRequestPtr, "<GfxVer>" );
				(void)strcat( fileUpdateRequestPtr, tempString );
				(void)strcat( fileUpdateRequestPtr, "</GfxVer><GfxSerLvl></GfxSerLvl></FileG>" );
			}
			// else - logically this should not happen, but if it does, do nothing

			// Get the directory index of the NEXT used directory entry.
			wDirInx++;
			LinkPtr = fnGFGetNextDirInxByCompType( &wDirInx, ANY_COMPONENT_TYPE, GRFX_ANY_MATCH );

		} // All the graphics in the graphics file are done

		(void)strcat( fileUpdateRequestPtr, "</Graphics>" );
		if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK buildFileUpdateRequest Graphics");

		// modified access of Download Files to use the updated CMOSDownloadFileName structure
		//
		if (CMOSFilesToDownload.DownloadFileCount > 0)
		{
			for (loopIndex = 0, dlFileEntry = &CMOSFilesToDownload.DownloadFileEntry[0];
				 loopIndex < CMOSFilesToDownload.DownloadFileCount;
				 loopIndex++, dlFileEntry++)
			{
				// add to the request string, based upon the request file type
				//
				switch (dlFileEntry->DownloadFileType)
				{
				  case PROFILE_FILE_TYPE:
					(void)strcat(fileUpdateRequestPtr, "<BasePro><FileP>");
					(void)strcat(fileUpdateRequestPtr, dlFileEntry->DownloadFileName);
					(void)strcat(fileUpdateRequestPtr, "</FileP></BasePro>");
					break;

				  case SNIPPET_FILE_TYPE:
				  case REPORT_FILE_TYPE:

					(void)strcat(fileUpdateRequestPtr, "<Snippets><FileN>");
					(void)strcat(fileUpdateRequestPtr, dlFileEntry->DownloadFileName);
					(void)strcat(fileUpdateRequestPtr, "</FileN></Snippets>");
					break;

				  default:
					break;
				}
			}

			// now that everything's been formatted to send, clear the structure
			//
			(void)memset(&CMOSFilesToDownload, 0, sizeof(CMOSFilesToDownload));
		}
		if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK buildFileUpdateRequest Snippets");

		//now the system information
#ifndef JANUS
		buildSysInfoMsg(fileUpdateRequestPtr + strlen(fileUpdateRequestPtr));
#endif

		if(systemErrorLog)
		{
			addSystemErrorLog(fileUpdateRequestPtr, fileUpdateRequestSize);
		}

		(void)strcat(fileUpdateRequestPtr, "</FileUpdateRequest>");
		if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK buildFileUpdateRequest Done");
    } while(0);

    return status;

}

/*****************************************************************************
 * Name                fnFileUpdateRequest
 * Description  
 *      This function is called from distributorHttpTransaction, after the 
 *      SignonReq message, to request a list of files to download
 * Inputs   
 *      theMessageToSend - Ptr to message to send to the dla.
 * Returns  
 *      STATUS - indicating if the status of the send AND receive.
 *
 * NOTES: 
 *      
 * Revision History
 -------------------------------------------------------------------------*/
static STATUS fnFileUpdateRequest(SRT_ENTRY_LINK *content, char* recvBuffer, int recvBufferSize, int systemErrorLog)
{
	STATUS				status = NU_SUCCESS;
    char 				*reqMessageBufferPtr;
    const int 			reqMessageBufferSize = 32768;
	CHAR        		pcn[16] = {0};
	CHAR        		serialNum[16] = {0};
	CHAR        		bodyPrefix[9] = {"lpbData="};
    UPDATE_COMBO_LINK 	updateComboLink;
    UPDATE_COMBO_LINK 	*updateComboLinkPtr = &updateComboLink;

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "DISTTASK calling buildFileUpdateRequest . . .");

    //Build the outgoing message
    //Get the memory
    NU_Allocate_Memory(MEM_Cached, (void **)&reqMessageBufferPtr, reqMessageBufferSize, NU_NO_SUSPEND);
    if(!reqMessageBufferPtr)
    {
        throwDnldException("Out of Memory!",MEMORY_ERR);
    }
    else
    {
        (void)memset(reqMessageBufferPtr, 0, reqMessageBufferSize);   //clear everything
    }

    (void)buildFileUpdateRequest(reqMessageBufferPtr, reqMessageBufferSize, pcn, serialNum, systemErrorLog);

#ifdef BASE_DLA
    status = fnHTTPLiteDoPost(content->srtEntryPtr->url, bodyPrefix, reqMessageBufferPtr, recvBuffer, recvBufferSize);
    if(status == 0)
    {
        talkedToDistributor = TRUE;
        memset(&updateComboLink, 0, sizeof(UPDATE_COMBO_LINK));
    	if(parseFileUpdateResponse(recvBuffer, &updateComboLink))
    		status = 0;
    	else
    		status = 1; //no files to download
    }
#else
    send_NetworkDLAReq(content->srtEntryPtr->url, pcn, serialNum, reqMessageBufferPtr, reqMessageBufferSize);
#endif

	if((status = NU_Deallocate_Memory(reqMessageBufferPtr)) != NU_SUCCESS)
    {
        throwDnldException("Invalid Pointer",INVALID_PTR);
    }

    return status;
}

void parseDistributorResponse(char* recvBuffer, int recvBufferSize)
{
	short xmlParsingResult;
    char *charPtr = findString("PBXML", recvBuffer); //validation #1


//    if(remoteLoggingIsOn())
//        remoteLog("XML response from distributor -\n", recvBuffer);

    if(!charPtr) //Did we find PBXML?
    {
        if(remoteLoggingIsOn())
        {
            remoteLog("Invalid XML reply from server -> ", recvBuffer);
        }

        charPtr = findString("The page cannot be displayed", recvBuffer);
        if (charPtr)
        {
            throwDnldException("The page cannot be diplayed, possible invalid URL entry", INVALID_URL);
        }
        else
        {
            throwException("No PBXML in reply from Distributor");
        }
    }

    //Now look for the start of the first XML tag
    charPtr = findString("<", charPtr);

    if(!charPtr) //Did we find PBXML?
        throwException("Can't find XML root tag");

    //Looking good so far. Let's parse it...
    //Clear out any previously received text message
    memset(textMessageFromDistributor, 0, sizeof(textMessageFromDistributor));

    //Good... Invoke the parser to process the XML response from the distibutor
    xmlParsingResult = XMLParse((XmlChar *)charPtr,
                                (XmlChar *)charPtr + strlen(charPtr) - 1,
                                ARRAY_LENGTH(pDISTRSignonRspEntries),
                                (ElementLookupEntry *)pDISTRSignonRspEntries,
                                NULL,//start
                                NULL,//end
                                fnDISTRDefaultSTag,//start el
                                fnDISTRDefaultETag,//end el
                                fnDISTRDefaultChars,//el chars
                                0);

    //Parser happy?
    if(xmlParsingResult != XML_STATUS_OK)
    {
        sprintf(scratchPad, "XMLParse() ERR# %d", xmlParsingResult);
        throwException(scratchPad);
    }
    
    sortTableEntries();
}

void sortTableEntries(void)
{
    //Now pop the srt entries and schedule any required updates
    while(headSrtEntryLinkPtr)
    {
        STATUS_RET srtHandlerStatus;

        currentSrtEntryLinkPtr = popSrtEntry(&headSrtEntryLinkPtr);
        if((srtHandlerStatus = srtHandler(currentSrtEntryLinkPtr)) != NO_PROBLEM)
        {
            sprintf(scratchPad, "srtHandler() ERR# %d", srtHandlerStatus);
            throwException(scratchPad);
        }
    }

    //Now pop the smt entries and schedule the services we requested
    while(headSmtEntryLinkPtr)
    {                                                                         
        STATUS_RET smtHandlerStatus;

        currentSmtEntryLinkPtr = popSmtEntry(&headSmtEntryLinkPtr);
        if((smtHandlerStatus = smtHandler(currentSmtEntryLinkPtr)) != NO_PROBLEM)
        {
            sprintf(scratchPad, "smtHandler() ERR# %d", smtHandlerStatus);
            throwException(scratchPad);
        }
    }
}


STATUS_RET srtHandler(SRT_ENTRY_LINK *srtEntryLinkPtr)
{
	STATUS_RET result = NO_PROBLEM;

    
    if(!srtEntryLinkPtr)
        throwException("NULL pointer");                                                                   
    
    //First determine the subsystem based on the PCN
    memset(scratchPad, 0, sizeof(scratchPad));   //clear everything

#ifndef JANUS
    if(fnValidData(BOBAMAT0, GET_VLT_METER_PCN, scratchPad) != BOB_OK)
    {
        throwException("fnValidData() fail");
    }
#endif

    if(!strcmp(srtEntryLinkPtr->srtEntryPtr->pcn, scratchPad))
        result = meterSrtHandler(srtEntryLinkPtr);

    else if(!strcmp(srtEntryLinkPtr->srtEntryPtr->pcn, CMOSSignature.bUicPcn))
        result = uicSrtHandler(srtEntryLinkPtr);

    else result = INVALID_PCN;

    return result;
}

STATUS_RET uicSrtHandler(SRT_ENTRY_LINK *srtEntryLinkPtr)
{
	STATUS_RET result = NO_PROBLEM;
	STATUS status;


    if(!srtEntryLinkPtr)
        throwException("NULL pointer");
    
    //Now check for any valid UIC services, the only one so far is software download
    if(!strcmp(srtEntryLinkPtr->srtEntryPtr->service, "SwDnld"))
    {
    //An update is either available now, or in the future
    DATETIME now;
    DATETIME then;
    long differenceInSeconds;


        if(!GetSysDateTime(&now, ADDOFFSETS, GREGORIAN))
            throwException("GetSysDateTime() failure");

        if(!asciiDateToDateTime(&then, srtEntryLinkPtr->srtEntryPtr->servicePostDate))
            throwException("asciiDateToDateTime() failure");

        if(!CalcDateTimeDifference(&now, &then, &differenceInSeconds))  //then - now
            throwException("CalcDateTimeDifference() failure");

        if(differenceInSeconds <= 0)  //Update available now?
        {
            //Really there's only supposed to be one SRT for SwDnld at a time, but I'm going to check anyway in case someone on the other end does not adhere to our policy
            if(!uicSwDownloadPtr)
                uicSwDownloadPtr = srtEntryLinkPtr; //This is the first
            else
            {
                //Critical supersedes required or optional. Required supersedes optional
                if(!strcmp(uicSwDownloadPtr->srtEntryPtr->priority, OPTIONAL_UPDATE))
                {
                    if(!strcmp(srtEntryLinkPtr->srtEntryPtr->priority, CRITICAL_UPDATE) || !strcmp(srtEntryLinkPtr->srtEntryPtr->priority, REQUIRED_UPDATE))
                    {
                        if((status = delSrtEntryLink(uicSwDownloadPtr)) != NU_SUCCESS)  //out with old
                        {
                            sprintf(scratchPad, "delSrtEntryLink() ERR# %d", status);
                            throwException(scratchPad);
                        }
                        uicSwDownloadPtr = srtEntryLinkPtr; //in with the new
                    }
                    else
                    {
                        //Another optional update...? Get rid of it...
                        if((status = delSrtEntryLink(srtEntryLinkPtr)) != NU_SUCCESS)   //out with old
                        {
                            sprintf(scratchPad, "delSrtEntryLink() ERR# %d", status);
                            throwException(scratchPad);
                        }
                    }
                }
                else if(!strcmp(uicSwDownloadPtr->srtEntryPtr->priority, REQUIRED_UPDATE))
                {
                    if(!strcmp(srtEntryLinkPtr->srtEntryPtr->priority, CRITICAL_UPDATE))
                    {
                        if((status = delSrtEntryLink(uicSwDownloadPtr)) != NU_SUCCESS)  //out with old
                        {
                            sprintf(scratchPad, "delSrtEntryLink() ERR# %d", status);
                            throwException(scratchPad);
                        }
                        uicSwDownloadPtr = srtEntryLinkPtr; //in with the new
                    }
                    else
                    {
                        //lower or same priority update. Get rid of it...
                        if((status = delSrtEntryLink(srtEntryLinkPtr)) != NU_SUCCESS)   //out with old
                        {
                            sprintf(scratchPad, "delSrtEntryLink() ERR# %d", status);
                            throwException(scratchPad);
                        }
                    }
                }
                else    //already have a critical update... get rid of it
                {
                    if((status = delSrtEntryLink(srtEntryLinkPtr)) != NU_SUCCESS)   //out with old
                    {
                        sprintf(scratchPad, "delSrtEntryLink() ERR# %d", status);
                        throwException(scratchPad);
                    }
                }

            }
        }
        else    //advance notification of update
        {
            //Auto schedule...?
        }
    }

    //Unused entry. Return the link to the heap
    else if((status = delSrtEntryLink(srtEntryLinkPtr)) != NU_SUCCESS)
    {
        sprintf(scratchPad, "delSrtEntryLink() ERR# %d", status);
        throwException(scratchPad);
    }

    return result;
}

STATUS_RET meterSrtHandler(SRT_ENTRY_LINK *srtEntryLinkPtr)
{
STATUS_RET result = NO_PROBLEM;
STATUS status;

    if(!srtEntryLinkPtr)
        throwException("NULL pointer");

    //Unused entry. Return the link to the heap
    if((status = delSrtEntryLink(srtEntryLinkPtr)) != NU_SUCCESS)
    {
        sprintf(scratchPad, "delSrtEntryLink() ERR# %d", status);
        throwException(scratchPad);
    }

    return result;
}

#ifdef JANUS
STATUS_RET smtHandler(SMT_ENTRY_LINK *smtEntryLinkPtr)
{
	STATUS_RET result = NO_PROBLEM;
	char psdPcnBuff[16];
	STATUS status;


    memset(scratchPad, 0, sizeof(scratchPad));   //clear everything

    if((!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "Refill")) ||
       (!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "Refund")) ||
       (!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "AcctBal")) ||
       (!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "DcpUpld")) ||
       (!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "DelcUpld")) ||
       (!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "Move")))
    {
        result = meterSmtHandler(smtEntryLinkPtr);
    }
    else if((!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "SwDnld")) ||
            (!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "AcctUpld")))
    {
        result = uicSmtHandler(smtEntryLinkPtr);
    }
    else
    {
        result = INVALID_SERVICE_IDENTIFIER;

        //Return the link to the heap
        if((status = delSmtEntryLink(smtEntryLinkPtr)) != NU_SUCCESS)
        {
            sprintf(scratchPad, "delSmtEntryLink() ERR# %d", status);
            taskException(scratchPad, THISFILE, __LINE__);
        }
    }

    return result;
}
#else
STATUS_RET smtHandler(SMT_ENTRY_LINK *smtEntryLinkPtr)
{
	STATUS_RET result = NO_PROBLEM;
	char psdPcnBuff[16];
	STATUS status;


    if(!smtEntryLinkPtr)
        throwException("NULL pointer");

    //First determine the subsystem based on the PCN
    if(fnValidData(BOBAMAT0, GET_VLT_METER_PCN, (void*)&psdPcnBuff[0]) == BOB_OK && !strcmp(smtEntryLinkPtr->smtEntryPtr->pcn,psdPcnBuff))
        result = meterSmtHandler(smtEntryLinkPtr);

    else if(!strcmp(smtEntryLinkPtr->smtEntryPtr->pcn, CMOSSignature.bUicPcn))
        result = uicSmtHandler(smtEntryLinkPtr);

    else
    {
        result = INVALID_PCN;

        //Return the link to the heap
        if((status = delSmtEntryLink(smtEntryLinkPtr)) != NU_SUCCESS)
        {
            sprintf(scratchPad, "delSmtEntryLink() ERR# %d", status);
            throwException(scratchPad);
        }
    }

    return result;
}
#endif

STATUS_RET meterSmtHandler(SMT_ENTRY_LINK *smtEntryLinkPtr)
{
	STATUS_RET result = NO_PROBLEM;
	STATUS status;

    if(!smtEntryLinkPtr)
        throwException("NULL pointer");

    //Now check for any valid meter services
    if((!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "Refill")) ||
            (!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "Refund")) ||
                (!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "AcctBal")) ||
                    (!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "DcpUpld")) ||
                        (!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "DelcUpld")) ||
                            (!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "Move")))
    {
        //If we get a Refill SMT and we have already seen an Account Balance SMT
        if(!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "Refill") && pbpSmtPtr)
        {
            pbp2ndaryRefillPtr = smtEntryLinkPtr;   //Store secondary SMT entry
        }
        else
        {
            //Just in case the order of the SMTs is inverted
            if(!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "AcctBal") && pbpSmtPtr)
            {
                pbp2ndaryRefillPtr = pbpSmtPtr;   //Store secondary SMT entry
                pbpSmtPtr = smtEntryLinkPtr;
            }
            else
                pbpSmtPtr = smtEntryLinkPtr;
        }
    }

    //All other services are just ignored for now. Note that not all services
    //may be applicable to all PCN's.

    //Unused entry. Return the link to the heap
    else if((status = delSmtEntryLink(smtEntryLinkPtr)) != NU_SUCCESS)
    {
        sprintf(scratchPad, "delSmtEntryLink() ERR# %d", status);
        throwException(scratchPad);
    }

    return result;
}


STATUS_RET uicSmtHandler(SMT_ENTRY_LINK *smtEntryLinkPtr)
{
STATUS_RET result = NO_PROBLEM;
STATUS status;

    if(!smtEntryLinkPtr)
        throwException("NULL pointer");

    if(!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "AcctUpld"))
        uicAcctUploadPtr = smtEntryLinkPtr;

    //Now check for any valid UIC services, the only ones so far are SwDnld
    else if(!strcmp(smtEntryLinkPtr->smtEntryPtr->serviceIdentifier, "SwDnld"))
        swDnldSmtPtr = smtEntryLinkPtr;

    //Unused entry. Return the link to the heap
    else if((status = delSmtEntryLink(smtEntryLinkPtr)) != NU_SUCCESS)
    {
        sprintf(scratchPad, "delSmtEntryLink() ERR# %d", status);
        throwException(scratchPad);
    }

    return result;
}


// This function is used to do all the PbP services
STATUS_RET pbpServices(void)
{
    STATUS status;
    STATUS_RET  result = NO_PROBLEM;
    char    pLogBuf[40];


    //Any PBP services?
    if(pbpSmtPtr)
    {

        result = pbpService(pbpSmtPtr);

        //Send "OIT_SERVICE_COMPLETE" with status
        (void)sprintf(pLogBuf, "PbP Service Result = %d", result);
        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));

        //Return the link to the heap
        if((status = delSmtEntryLink(pbpSmtPtr)) != NU_SUCCESS)
        {
            sprintf(scratchPad, "delSmtEntryLink() ERR# %d", status);
            throwException(scratchPad);
        }

        //Reset for next time
        pbpSmtPtr = 0;
    }

    return (result);
}


// This function is used to do each individual PbP service
STATUS_RET pbpService(SMT_ENTRY_LINK *pbpSmtPtr)
{
	STATUS_RET result = NO_PROBLEM;
	INTERTASK msgIn;

    if(!OSSendIntertask (PBPTASK, DISTTASK, PBP_SERVICE_URL, GLOBAL_PTR_DATA, pbpSmtPtr->smtEntryPtr->serviceIdentifier, sizeof(SERVICE_URL_INFO)))
        throwException("OSSendIntertask() fail");

    DISTTASK_Waiting = TRUE;
	dbgTrace(0, "-->-->--> pbpService DISTTASK Go to Sleep! <--<--<--");
    if(OSReceiveIntertask(DISTTASK, &msgIn, OS_SUSPEND) != SUCCESSFUL)
        throwException("OSReceiveIntertask() fail");

    DISTTASK_Waiting = FALSE;
    result = msgIn.IntertaskUnion.lwLongData[0];

    if (result != NU_SUCCESS)
    {
    	dbgTrace(0, "-->-->--> PBPTASK Error: [%d] <--<--<--", result);
    	result = setDnldException(API_STATUS_DATA_CENTER_COMM_FAIL, result);
    }

    // save last good PbP URL & Accounting Server URL as long as they came from the Distributor
    // the DLA URL will be saved as part of the DLA services processing
    if (!result && talkedToDistributor)
    {
	// if using PCMC, only update the Accounting Server backup URL &
	// the Tier3 backup URL if not using a PCMC alias URL.
	// else, not using PCMC, so there should be no problem updating the backup URLs w/ the given URLs
    	// no more pc_daemon...
    	//								if (!usePcHostForIntelliLink() || !strstr(pbpSmtPtr->smtEntryPtr->serverUrl, "PCMCHTTPS"))
	if (!strstr(pbpSmtPtr->smtEntryPtr->serverUrl, "PCMCHTTPS"))
	{
		// PbP URL
		(void)memset(CMOSNetworkConfig.backupPbpUrl, 0, sizeof(CMOSNetworkConfig.backupPbpUrl));

        if (strlen(pbpSmtPtr->smtEntryPtr->serverUrl) < sizeof(CMOSNetworkConfig.backupPbpUrl))  //It fits
            (void)memcpy(CMOSNetworkConfig.backupPbpUrl, pbpSmtPtr->smtEntryPtr->serverUrl, strlen(pbpSmtPtr->smtEntryPtr->serverUrl));
        else  //Truncate
            (void)memcpy(CMOSNetworkConfig.backupPbpUrl, pbpSmtPtr->smtEntryPtr->serverUrl, sizeof(CMOSNetworkConfig.backupPbpUrl) - 1);
    }
	// no more pc_daemon...
	//	if (!usePcHostForIntelliLink() || !strstr(pbpSmtPtr->smtEntryPtr->serverUrl, "PCMCHTTPS"))
	if (!strstr(pbpSmtPtr->smtEntryPtr->serverUrl, "PCMCHTTPS"))
	{
		// Accounting Server URL
		(void)memset(CMOSNetworkConfig.backupAcntUrl, 0, sizeof(CMOSNetworkConfig.backupAcntUrl));

        if (strlen(uicAcctUploadPtr->smtEntryPtr->serverUrl) < sizeof(CMOSNetworkConfig.backupAcntUrl))  //It fits
			(void)memcpy(CMOSNetworkConfig.backupAcntUrl, uicAcctUploadPtr->smtEntryPtr->serverUrl, strlen(uicAcctUploadPtr->smtEntryPtr->serverUrl));
        else  //Truncate
			(void)memcpy(CMOSNetworkConfig.backupAcntUrl, uicAcctUploadPtr->smtEntryPtr->serverUrl, sizeof(CMOSNetworkConfig.backupAcntUrl) - 1);
        }
    }


    return result;
}




// This function is called if the operator requested the connection
void dlaServices2(char *recvBuffer, int recvBufferSize, int systemErrorLog)
{
	BOOL AlreadyBeenToDls = FALSE;
	STATUS_RET result = NO_PROBLEM;
	INTERTASK msgIn;
	STATUS status;

    //First check for and process out any SRTs
    if(uicSwDownloadPtr)                                                      
    {                                                                         
	    STATUS_RET result;                                                    


        //Service already done?
        if(!atoi(uicSwDownloadPtr->srtEntryPtr->uniqueIdentifier) || 
            (atoi(uicSwDownloadPtr->srtEntryPtr->uniqueIdentifier) > atoi(infrastructureNvmPtr->uicSrtUniqueIdentifer)))
        {                                                                              
            result = fnFileUpdateRequest(uicSwDownloadPtr, recvBuffer, recvBufferSize, systemErrorLog);
#ifdef BASE_DLA
            if(result == 0)
            	downloadAllUpdates();
#endif

            // save last good DLA URL as long as it came from the Distributor
            if (!result && talkedToDistributor)
            {
				// if using PCMC, don't change the DLA backup URL if using a PCMC alias URL
				// else, not using PCMC, so there should be no problem updating the back up URL w/ the given URL
// no more pc_daemon
//				if (!usePcHostForIntelliLink() || !strstr(uicSwDownloadPtr->srtEntryPtr->url, "PCMCHTTPS"))
				if (!strstr(uicSwDownloadPtr->srtEntryPtr->url, "PCMCHTTPS"))
				{
				// DLA URL
	                (void)memset(CMOSNetworkConfig.backupDlaUrl, 0, sizeof(CMOSNetworkConfig.backupDlaUrl));
                                
                if (strlen(uicSwDownloadPtr->srtEntryPtr->url) < sizeof(CMOSNetworkConfig.backupDlaUrl))  //It fits
	                    (void)memcpy(CMOSNetworkConfig.backupDlaUrl, uicSwDownloadPtr->srtEntryPtr->url, strlen(uicSwDownloadPtr->srtEntryPtr->url));
                else  //Truncate
	                    (void)memcpy(CMOSNetworkConfig.backupDlaUrl, uicSwDownloadPtr->srtEntryPtr->url, sizeof(CMOSNetworkConfig.backupDlaUrl) - 1);
	            }
            }

            AlreadyBeenToDls = TRUE;    //Been there, done that...
        }
        else
        {
            if(remoteLoggingIsOn())                                                                       
            {
                sprintf(scratchPad, "SRT Unique Identifier %s has already been done", uicSwDownloadPtr->srtEntryPtr->uniqueIdentifier);         
                remoteLog(NULL, scratchPad);                            
            }
            
            //Return the link to the heap
            if((status = delSrtEntryLink(uicSwDownloadPtr)) != NU_SUCCESS)
            {
                sprintf(scratchPad, "delSrtEntryLink() ERR# %d", status);
                throwException(scratchPad);
            }
        }
        //Reset
        uicSwDownloadPtr = 0;
    }

    //Now for the SMT
    if(swDnldSmtPtr)
    {
	    STATUS status;
	    SRT_ENTRY_LINK *mySrtEntryLinkPtr;
	    STATUS_RET result = NO_PROBLEM;


        //First see if we still need the files (we may have already been directed to DLS via a SRT
        if(!AlreadyBeenToDls)
        {
            if(remoteLoggingIsOn())                                                                       
            {
                remoteLog("Processing explicit SwDnld SMT Request", NULL);                            
            }
            
            if(strcmp(swDnldSmtPtr->smtEntryPtr->status, "0"))  //Status should always be "0" or there is a problem  
            {
                sprintf(scratchPad, "Distributor has no SwDnld Service Map Table Entry posted for PCN %s", swDnldSmtPtr->smtEntryPtr->pcn);         
                if(remoteLoggingIsOn())                                                                       
                {
                    remoteLog(NULL, scratchPad);                            
                }

                throwException(scratchPad);
            }
        
            //format the smt into an srt with the priority set to low
            if((status = newSrtEntryLink(&mySrtEntryLinkPtr)) != NU_SUCCESS)
            {
                sprintf(scratchPad, "newSrtEntryLink() ERR# %d", status);
                throwException(scratchPad);
            }

            strncpy(mySrtEntryLinkPtr->srtEntryPtr->url, swDnldSmtPtr->smtEntryPtr->serverUrl, sizeof(mySrtEntryLinkPtr->srtEntryPtr->url));
            strncpy(mySrtEntryLinkPtr->srtEntryPtr->priority, OPTIONAL_UPDATE, strlen(OPTIONAL_UPDATE));
            strncpy(mySrtEntryLinkPtr->srtEntryPtr->uniqueIdentifier, "0", strlen("0"));

            result = fnFileUpdateRequest(mySrtEntryLinkPtr, recvBuffer, recvBufferSize, systemErrorLog);
#ifdef BASE_DLA
            if(result == 0)
            	downloadAllUpdates();
#endif
            // save last good DLA URL as long as it came from the Distributor
            if (!result && talkedToDistributor)
            {
				// if using PCMC, don't change the DLA backup URL if using a PCMC alias URL
				// else, not using PCMC, so there should be no problem updating the backup URL w/ the given URL
#if 0
// no more pc_daemon
//				if (!usePcHostForIntelliLink() || !strstr(swDnldSmtPtr->smtEntryPtr->serverUrl, "PCMCHTTPS"))
				{
				// DLA URL
	                (void)memset(CMOSNetworkConfig.backupDlaUrl, 0, sizeof(CMOSNetworkConfig.backupDlaUrl));
                                
                if (strlen(swDnldSmtPtr->smtEntryPtr->serverUrl) < sizeof(CMOSNetworkConfig.backupDlaUrl))  //It fits
	                    (void)memcpy(CMOSNetworkConfig.backupDlaUrl, swDnldSmtPtr->smtEntryPtr->serverUrl, strlen(swDnldSmtPtr->smtEntryPtr->serverUrl));
                else  //Truncate
	                    (void)memcpy(CMOSNetworkConfig.backupDlaUrl, swDnldSmtPtr->smtEntryPtr->serverUrl, sizeof(CMOSNetworkConfig.backupDlaUrl) - 1);
	            }
#endif
            }
        }
        
        //Return the link to the heap 
        if((status = delSmtEntryLink(swDnldSmtPtr)) != NU_SUCCESS)         
        {                                                                     
            sprintf(scratchPad, "delSmtEntryLink() ERR# %d", status);   
            throwException(scratchPad);                    
        }                                                                     

        //Reset for next time
        swDnldSmtPtr = 0;                                                                      
    }    
}


void initInfrastructureNvm(void)
{
INFRASTRUCTURE_NVM *infrastructureNvmPtr = (INFRASTRUCTURE_NVM *)getInfrastructureNvmAddress();

memset(infrastructureNvmPtr, 0, getSizeOfInfrastructureNVM());

infrastructureNvmPtr->NvmVersionId = INFRASTRUCTURE_NVM_VERSION;    
strncpy(infrastructureNvmPtr->attTollFreePhoneNumber, "1-800-590-4857", sizeof(infrastructureNvmPtr->attTollFreePhoneNumber));
dottedQuadToArray(infrastructureNvmPtr->primaryDnsServer, "161.228.214.136");
dottedQuadToArray(infrastructureNvmPtr->secondaryDnsServer, "152.144.145.137");
strncpy(infrastructureNvmPtr->distributorUrl, "distserv.pb.com", sizeof(infrastructureNvmPtr->distributorUrl)); 
strncpy(infrastructureNvmPtr->attLoginAccountAndUserId, "dualaccess.i411.v411jxs", sizeof(infrastructureNvmPtr->attLoginAccountAndUserId)); 
strncpy(infrastructureNvmPtr->attPassword, "pitney05", sizeof(infrastructureNvmPtr->attPassword));  
infrastructureNvmPtr->scheduledEventsHeadPtr = NULL;
infrastructureNvmPtr->lastNvmHeapAddress = getNvmHeapAddress();
infrastructureNvmPtr->NvmCheckword = 0xDEADBEEF;
}

//Function to return the event scheduler head pointer
EVENT_SCHEDULER_INFO *getScheduledEventsHeadPtr(void)
{
    if(!infrastructureNvmPtr)
        throwException("NULL Pointer");                                         
    
    return infrastructureNvmPtr->scheduledEventsHeadPtr;
}


void setScheduledEventsHeadPtr(EVENT_SCHEDULER_INFO *eventSchedulerInfo)
{
    if(!infrastructureNvmPtr)
        throwException("NULL Pointer");                                         
    
    infrastructureNvmPtr->scheduledEventsHeadPtr = eventSchedulerInfo;
}
DATETIME fnInfraUpdateParseDateTime(char *pDate, char *pTime)
{
    DATETIME rDate;
    memset(&rDate,0,sizeof(rDate));

    // parse date according to MM97004
    // date should be YYYY/MM/DD
    rDate.bCentury = (pDate[0] - '0') * 10;
    rDate.bCentury += (pDate[1] - '0');     
    rDate.bYear = (pDate[2] - '0') * 10;
    rDate.bYear += (pDate[3] - '0');
    rDate.bMonth = (pDate[5] - '0') * 10;
    rDate.bMonth += (pDate[6] - '0');
    rDate.bDay = (pDate[8] - '0') * 10;
    rDate.bDay += (pDate[9] - '0');
    rDate.bDayOfWeek = 1;

    // parse time according to MM97004
    // time should be HH:MM:SS
    rDate.bHour = (pTime[0] - '0') * 10;
    rDate.bHour += (pTime[1] - '0');
    rDate.bMinutes = (pTime[3] - '0') * 10;
    rDate.bMinutes += (pTime[4] - '0');
    rDate.bSeconds = (pTime[6] - '0') * 10;
    rDate.bSeconds += (pTime[7] - '0');

    return rDate;
}

void versionCheckNVM(void)
{
INFRASTRUCTURE_NVM *infrastructureNvmPtr = (INFRASTRUCTURE_NVM *)getInfrastructureNvmAddress();

    //Contents changed?
    if(infrastructureNvmPtr->NvmVersionId != INFRASTRUCTURE_NVM_VERSION) 
    {
        switch(infrastructureNvmPtr->NvmVersionId)
        {
            case 1:
            {
                infrastructureNvmPtr->lastNvmHeapAddress = getNvmHeapAddress(); //added storage for last heap pointer in v2
            
                break;
            }
        
            default:
            {
                //If they went backwards re-init
                if(infrastructureNvmPtr->NvmVersionId > INFRASTRUCTURE_NVM_VERSION)
                {
                    initInfrastructureNvm();
                    initNvmHeap();
                }
            
                else
                    throwException("Invalid NVM Version ID");
                
                break;
            }
        }
    }
    
    //Heap relocated?
    if(infrastructureNvmPtr->lastNvmHeapAddress != getNvmHeapAddress())
    {
         //If there are any events...
        if(getScheduledEventsHeadPtr())
        {
        //Fix pointers...
        long offset = getNvmHeapAddress() - infrastructureNvmPtr->lastNvmHeapAddress;   //calculate offset
        EVENT_SCHEDULER_INFO  *currentPtr;

            //Fix scheduled events head pointer
            infrastructureNvmPtr->scheduledEventsHeadPtr = (EVENT_SCHEDULER_INFO * )((unsigned long * )infrastructureNvmPtr->scheduledEventsHeadPtr + offset);
            
            //iterate through the events fixing pointers as we go...
            currentPtr = getScheduledEventsHeadPtr();

            while(currentPtr)
            {
                //fixup any data pointers
                if(currentPtr->eventDataPtr)
                    currentPtr->eventDataPtr = ((unsigned long *)currentPtr->eventDataPtr + offset);

                //fixup the pointer to the next event if it's set
                if(currentPtr->nextEvent)
                    currentPtr->nextEvent = (EVENT_SCHEDULER_INFO * )((unsigned long * )currentPtr->nextEvent + offset);
                    
                currentPtr = currentPtr->nextEvent;  
            }
        
        }
        
    infrastructureNvmPtr->lastNvmHeapAddress = getNvmHeapAddress();
    }
}


void dottedQuadToArray(unsigned char *array, char *dotttedQuad)
{
    //Buffer for atoi() conversion
    char buffer[16];
    //Index into buffer
    int index = 0;
    //duh
    int loopIndex;

    //Convert from dotted quad to packed IP
    for(loopIndex = 0; loopIndex < 4; loopIndex++)  //4 cause it's dotted "quad"
    {
        memset(buffer,0,sizeof(buffer)); //clean buffer
        index = 0;                          //fresh index

        //Each field seperated by dot has up to 3 digits. Check whether the index is in bounds. 
        while((*dotttedQuad != '.') && (*dotttedQuad != 0) && (index < 3)) //copy a quad
            buffer[index++] = *dotttedQuad++;       

        array[loopIndex] = (unsigned char)atoi(buffer); //translate from ascii to byte...
        dotttedQuad++;
    }
    
}

const char *getAttPhoneNumber(void)
{
    return CMOSSetupParams.PhoneNumber;
}

const char *getOutsideLinePrefix(void)
{    
    return CMOSSetupParams.DialingPrefix;
}


void fnInfraUpdateFtpTransfer(unsigned long lwBytes)
{
#ifdef JANUS
    int dnldStatus[2];
//??
//	char pLogBuf[50];
//??


    dnldStatus[0] = dlFileNum;
    dnldStatus[1] = bytesDownloadedSoFar;

    #ifdef JANUS_FTP_TIMER
    OSStopTimer(DLS_FTP_TIMER);
    putString("Ftp timer stopped",0);
    #endif

//?? old
//    (void)OSSendIntertask (OIT, DLS_TASK, (unsigned char)OIT_INFRA_DOWNLOAD_XFER_STATUS, LONG_DATA, dnldStatus, sizeof( dnldStatus ));

//?? new
	oldPercentComplete = newPercentComplete;
	newPercentComplete = (unsigned long)(((unsigned int)bytesDownloadedSoFar * 100)/(unsigned int)totalBytesToDownload);
	if (!newPercentComplete)
		newPercentComplete = 1;

//??
//	if (fnGetLocalLoggingState() == TRUE)
//	{
//		(void)sprintf(pLogBuf, "old percent = %d, new percent = %d", oldPercentComplete, newPercentComplete);
//		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
//	}
//??

	if ((newFileNum != dlFileNum) || (oldPercentComplete != newPercentComplete))
	{
	    dbgTrace(DBG_LVL_ERROR, "Removed message to DLS_TASK (removed)");
	//  (void)OSSendIntertask (OIT, DLS_TASK, (unsigned char)OIT_INFRA_DOWNLOAD_XFER_STATUS, LONG_DATA, dnldStatus, sizeof( dnldStatus ));
//    	OSWakeAfter(70);

		// If downloaded everything, prepare for next download
		// Else, update the current file number
		if (newPercentComplete >= 100)
		{
			newFileNum = 0;
			newPercentComplete = 0;
		}
		else
		{
			newFileNum = dlFileNum;
		}
	}
//??
#else
    /* send a message to the OIT to tell it that we have
    received some data from download services ftp server. */
    (void)OSSendIntertask (OIT, DLS_TASK, (unsigned char)OIT_INFRA_DOWNLOAD_XFER_STATUS, LONG_DATA, &lwBytes, sizeof(lwBytes));
#endif
}

void fnModemConnecting(void)
{
    /* send a message to the OIT to tell it that we have sent the 
    dial command to the modem and are going to try to connect */
    (void)OSSendIntertask (OIT, DISTTASK, (unsigned char)OIT_CONNECTION_START, NO_DATA, NULL, 0);
    /* suspend the distr task so the OIT has a chance to update
    the screen with the new connection status. */
    OSWakeAfter(100);
}

void fnModemConnectionComplete(unsigned long lwBaud)
{
    lwModemConnectionSpeed = lwBaud;
}


/*
 ** asciiDateToDateTime
 *
 *  FILENAME: C:\Co0811bld\co0811\source\IntelliLink.c
 *
 *  PARAMETERS:
 *      
 *      - dateTimePtr for output
 *      - asciiDatePtr for input (YYYY/MM/DD)
 *
 *  DESCRIPTION:
 *
 *      Converts an ascii date string to DATETIME format
 *
 *  RETURNS:
 *
 *  1 on success    0 on fail
 *
 */
BOOL asciiDateToDateTime(DATETIME *dateTimePtr, char *asciiDatePtr)
{
    if(!dateTimePtr || !asciiDatePtr)
        throwException("NULL pointer");
    
    memset(dateTimePtr, 0, sizeof(DATETIME));    //clean
	
    memset(scratchPad, 0, sizeof(scratchPad));
    memcpy(scratchPad, asciiDatePtr, 2);
    dateTimePtr->bCentury = atoi(scratchPad);
	
    memset(scratchPad, 0, sizeof(scratchPad));
    memcpy(scratchPad, asciiDatePtr + 2, 2);
    dateTimePtr->bYear = atoi(scratchPad);
	
    memset(scratchPad, 0, sizeof(scratchPad));
    memcpy(scratchPad, asciiDatePtr + 5, 2);
    dateTimePtr->bMonth = atoi(scratchPad);
	
    memset(scratchPad, 0, sizeof(scratchPad));
    memcpy(scratchPad, asciiDatePtr + 8, 2);
    dateTimePtr->bDay = atoi(scratchPad);

    dateTimePtr->bDayOfWeek = 1;    //fudge day of week so I can use fnValidDate()

    return fnValidDate(dateTimePtr);
}



BOOL mediumPriorityUpdateSkippedOnce(void)
{
    return infrastructureNvmPtr->mediumPriorityUpdateSkipped;
}

void setMediumPriorityUpdateSkippedOnce(void)
{
    infrastructureNvmPtr->mediumPriorityUpdateSkipped = 1;
}

void clearMediumPriorityUpdateSkippedOnce(void)
{
    infrastructureNvmPtr->mediumPriorityUpdateSkipped = 0;
}

void initBasePtrs(void)
{
    currentSmtEntryLinkPtr = 0;
    headSmtEntryLinkPtr = 0;
    currentSrtEntryLinkPtr = 0;
    headSrtEntryLinkPtr = 0;
    pbpSmtPtr = 0;
    pbp2ndaryRefillPtr = 0;
    uicSwDownloadPtr = 0;
    swDnldSmtPtr  = 0;
    uicRatesDownloadPtr = 0;
    meterSwDownloadPtr = 0;
}


void deleteVptrs(void)
{
	//clean up any remaining allocations 
}

void setNeedTogoToPbpFlag(void)
{
     needTogoToPbpFlag = TRUE;
}
void clrNeedTogoToPbpFlag(void)
{
     needTogoToPbpFlag = FALSE;
}
BOOL needTogoToPbP(void)
{
    return needTogoToPbpFlag;
}


/* *************************************************************************
// FUNCTION NAME:       canConnectToDistributor
// DESCRIPTION: 
//      Attempts to connect to the distributor using the parameters in CMOSNetwork. 
// INPUTS:  
//      None.
// OUTPUTS: 
//      TRUE if connection succeeded.
//      FALSE if connection failed.
// NOTES: 
//  1. If this function was called, and NU_Connect failed in the last xx time,
//      and the distributor URL has not changed since that attempt, then it 
//      will not attempt to connect again and will return FALSE until xx time
//      has passed. 
//  2. xx time was 24 hours, but has been set to 1 minute in this version.
// HISTORY:
//  2013.03.11 Clarisa Bellamy - Added more diagnostics after NU_Connect fails,
//                      and added function header.
// --------------------------------------------------------------------------*/

#if 0			// no longer need this function.  It's replaced by fnCheckAllowDistributorConnection

BOOL canConnectToDistributor(void)
{
	SOCKET_DESCRIPTOR mySocket;

//	static CRACKED_URL distributorUrl;  
//	STATUS status;
//	ADDR_STRUCT sockAddress;
//	NU_HOSTENT hostEnt;
	
//	char *pHostAddrList=NULL;
//	int socketDescriptor;
	int socketStatus;
	

    if(remoteLoggingIsOn())
        (void)remoteLog("Testing for Distributor connectivity", NULL);

    if(connectionFailed && ((getMsClock() - timeOfFailure) < ulSkipDistributorTime) && 
	   !strcmp(lastDistributorURL, CMOSNetworkConfig.distributorUrl))  
    {
        (void)sprintf( scratchpad100, "Connection failed in last %d hrs, %d mins. Not trying again now.",
                     ((ulSkipDistributorTime / ONE_MINUTE_IN_MS) / 60), ((ulSkipDistributorTime / ONE_MINUTE_IN_MS) % 60) );
        if(remoteLoggingIsOn())
        {
            (void)remoteLog( scratchpad100, NULL );
        }
		else
		{
			fnDumpStringToSystemLog(scratchpad100);
		}
        
        return FALSE;
    }
    
    connectionFailed  = 0;  //Reset
    
    //save the current URL for comparision next time
    memset(lastDistributorURL, 0, sizeof(lastDistributorURL));
    
    if(strlen(CMOSNetworkConfig.distributorUrl) < sizeof(lastDistributorURL))
        memcpy(lastDistributorURL, CMOSNetworkConfig.distributorUrl, strlen(CMOSNetworkConfig.distributorUrl));  //It fits
    else  //Truncate
        memcpy(lastDistributorURL, CMOSNetworkConfig.distributorUrl, sizeof(lastDistributorURL) - 1);
    
    //initialize socket
	(void)fnInitializeSocket(&mySocket);

    //Crack the URL...
    // the following will result in the string in CMOSNetworkConfig.distributorUrl being changed to all lower case
	socketStatus = fnGetServerAddress(&mySocket, CMOSNetworkConfig.distributorUrl, "Distributor", DIST_CONNECTION);
    if (socketStatus != SSLIF_SUCCESS)
    {
		if (socketStatus == SSLIF_HOST_LOOKUP_ERR)
		{
			mySocket.urlInfo.host[63] = 0;	// make sure it's null terminated
            sprintf(scratchPad, "DNS unable to resolve host = %s", mySocket.urlInfo.host);
            if (remoteLoggingIsOn())
                remoteLog( NULL, scratchPad );

            throwException(scratchPad);
		}
		else
		{
			(void)sprintf(scratchPad, "Could not resolve Distributor URL = ");
			(void)strncat(scratchPad, CMOSNetworkConfig.distributorUrl, 128);
	        throwDnldException(scratchPad, INVALID_URL);
		}
	}
	 
/*
    //Crack the URL stored in flash (using explicit one until supported)
    if((status = crackUrl(&distributorUrl, CMOSNetworkConfig.distributorUrl))!= NU_SUCCESS)
    {
        if(remoteLoggingIsOn())
            (void)remoteLog("Invalid Distributor URL", NULL);
    
        throwDnldException("The page cannot be diplayed, possible invalid Distributor URL entry", INVALID_URL);
    }
	
    //Clear the address structure  and receive buffer
    if(usePcHostForIntelliLink())
        registerUrl(&distributorUrl);

    memset(&sockAddress, 0, sizeof(sockAddress));
*/

    /* Create the socket connection to the server. */
    if (fnCreateSocket(&mySocket) != SSLIF_SUCCESS)
    {
	    fnDumpStringToSystemLog("Couldn't create the socket");
        throwDnldException("Couldn't create the socket", SOCKET_CREATE_ERR);
    }

    //Create the socket
/*
    if((socketDescriptor = NU_Socket(NU_FAMILY_IP, NU_TYPE_STREAM, NU_NONE)) < 0)
    {
        sprintf(scratchPad, "NU_Socket() ERR# %d", socketDescriptor);
        throwException(scratchPad);
    }

    memset(&hostEnt, 0, sizeof(NU_HOSTENT)); //make Lint happy...
    hostEnt.h_addr_list = &pHostAddrList;
    
    //Additions for raw IP usage
    if(isIpAddress(distributorUrl.host))
    {
        dottedQuadToArray((unsigned char *)bHostIpAddr, distributorUrl.host);
        //hostEnt.h_addr = (char*)bHostIpAddr;
        pHostAddrList = (char *)bHostIpAddr;
    }
    else
    {
        if(remoteLoggingIsOn())                                                                       
        {
            sprintf(scratchPad, "Looking up %s", distributorUrl.host);
            remoteLog(NULL, scratchPad);                            
        }
        
        if((status = NU_Get_Host_By_Name(distributorUrl.host, &hostEnt)) != NU_SUCCESS)
        {
			distributorUrl.host[63] = 0;	// make sure it's null terminated
            sprintf(scratchPad, "DNS unable to resolve host = %s - ERR# %d", distributorUrl.host, status);
            
            if (remoteLoggingIsOn())
                remoteLog( NULL, scratchPad );
            
            throwException(scratchPad);
        }
    }
    
    if(remoteLoggingIsOn())                                                                       
    {
        sprintf(scratchPad, "Distributor IP is %d.%d.%d.%d", 
            (unsigned char)hostEnt.h_addr[0], (unsigned char)hostEnt.h_addr[1], 
                (unsigned char)hostEnt.h_addr[2], (unsigned char)hostEnt.h_addr[3]);
        (void)remoteLog(NULL, scratchPad);                            
    }
    
    //Setup socket address
    sockAddress.family = NU_FAMILY_IP;
    
    if(*distributorUrl.port)    //explicit specification
        sockAddress.port = atoi(distributorUrl.port);
    else if(findString("https://", distributorUrl.protocol))
        sockAddress.port = 443;  //SSL port           
    else
        sockAddress.port = 80;  //HTTP port           
    
    sockAddress.name = "Distributor";  
    sockAddress.id.is_ip_addrs[0] = hostEnt.h_addr[0];    
    sockAddress.id.is_ip_addrs[1] = hostEnt.h_addr[1];    
    sockAddress.id.is_ip_addrs[2] = hostEnt.h_addr[2];    
    sockAddress.id.is_ip_addrs[3] = hostEnt.h_addr[3];    
*/

    (void)sprintf( scratchPad, "Distributor Port is %u", mySocket.Address.port );

    if(remoteLoggingIsOn())
    {
        (void)remoteLog("Attempting to connect to Distributor...", NULL);
        (void)remoteLog( NULL, scratchPad );
    }
	else
	{
        fnDumpStringToSystemLog("Attempting to connect to Distributor...");
        fnDumpStringToSystemLog( scratchPad );
	}
    
    //Ensure appropriate timeout
    if(CMOSNetworkConfig.maxRto > 2400)
        CMOSNetworkConfig.maxRto = 2400;
    
    /* Connect to the server. */
    if (fnConnectSocket(&mySocket, NU_BLOCK) != SSLIF_SUCCESS)
    {
        connectionFailed = TRUE;
        timeOfFailure = getMsClock(); //save for later
        
        fnDumpStringToSystemLog( "Unable to connect to Distributor." );

        if( remoteLoggingIsOn() )
        {
            // Get the last entry in the NLOG in case it helps
            fnDumpStringToSystemLog( "Last NLOG entry: " );
            fnGetNLogInfo( scratchPad, sizeof( scratchPad ) );
            fnDumpStringToSystemLog( scratchPad );
        }
        
        return FALSE;
    }

    //Make the connection
/*
    if((status = NU_Connect(socketDescriptor, &sockAddress, 0)) < 0)
    {
        connectionFailed = TRUE;
        timeOfFailure = getMsClock(); //save for later
        
        fnDumpStringToSystemLog( "Unable to connect to Distributor." );
        (void)sprintf( scratchpad100, "NU_Connect ERR: %d", status );
        fnDumpStringToSystemLog( scratchpad100 );

        if( remoteLoggingIsOn() )
        {
            // Get the last entry in the NLOG in case it helps
            fnDumpStringToSystemLog( "Last NLOG entry: " );
            fnGetNLogInfo( scratchPad, sizeof( scratchPad ) );
            fnDumpStringToSystemLog( scratchPad );
        }
        
        return FALSE;
    }
*/

    if(remoteLoggingIsOn())
        (void)remoteLog("Succeeded in connecting to Distributor.", NULL);

    fnDestroySocket(&mySocket);     //clean up

/*
    if((status = NU_Close_Socket(socketDescriptor)) != NU_SUCCESS)     //clean up
    {
        sprintf(scratchPad, "NU_Close_Socket() ERR# %d", status);
        throwException(scratchPad);
    }
*/

    return TRUE;
}
#endif

/* *************************************************************************
// FUNCTION NAME:       fnGetNLogInfo
// DESCRIPTION: 
//      Retrieves the last entryin the net library's NLOG. 
// INPUTS:  
//      pBuf - Pointer to NULL-terminated XML output buffer.  May point to 
//              beginning, end or somewhere in the middle, as long as it is
//              currently null-terminated, since the strcat function is used.
// OUTPUTS: 
//      Appends the CommType start tag, contents, and end tag to the XML
//      buffer.  Nothing returned.
// NOTES: 
// HISTORY:
//  2013.03.11 Clarisa Bellamy - - Initial.
// --------------------------------------------------------------------------*/
void fnGetNLogInfo( char *pDest, size_t wLen )
{
    INT         index;
    NLOG_ENTRY  *ptr;


    // Validate arguments:
    if( (pDest == NULL_PTR) || (wLen < 1)  )    
        return;

    if( NLOG_Avail_Index < 1 )
    {
        index = NLOG_MAX_ENTRIES -1;
    }
    else
    {
        index = NLOG_Avail_Index - 1;
    }

    if( index >=  NLOG_MAX_ENTRIES )
    {
        strncpy( pDest, "NLOG_Avail_Index out of range" , wLen - 1 );
    }
	else
	{
	    ptr = &NLOG_Entry_List[ index ];
	    memset( pDest, 0, wLen );
	    strncpy( pDest, ptr->log_msg, wLen - 1 );
	}
}


/* *************************************************************************
// FUNCTION NAME:       fnXMLAppendConnectivityInfo
// DESCRIPTION: 
//      Given a ptr to an XML output buffer, append the CommType element. 
// INPUTS:  
//      pBuf - Pointer to NULL-terminated XML output buffer.  May point to 
//              beginning, end or somewhere in the middle, as long as it is
//              currently null-terminated, since the strcat function is used.
//      flag indicating if the XML tag should be loaded in the buffer.
// OUTPUTS: 
//      Appends the CommType start tag, contents, and end tag to the XML
//      buffer.  Nothing returned.
// NOTES: 
//  1. This function is also called by download.c to send the CommType element
//     to DLA to make it easier to debug problems.  So it must include the 
//     whole element: start tag, contents, and end tag.
// --------------------------------------------------------------------------*/
// These strings cannot be longer than 9 characters, per Distributor spec MM97004:
#define     XML_COMMTYPE_MODEM          "modem"
#define     XML_COMMTYPE_PCPROXY        "PCProxy"
#define     XML_COMMTYPE_USB_LAN        "USB LAN"
#define     XML_COMMTYPE_INTEGRATED_LAN "Int. LAN"
#define     XML_COMMTYPE_ANY_LAN        "lan"
#define     XML_COMMTYPE_WIRED_NOVA     "WiredNOVA"
#define     XML_COMMTYPE_WIFI_NOVA      "WiFiNOVA"

void fnXMLAppendConnectivityInfo( char *pBuf, BOOL fbLoadTag )
{

	if (fbLoadTag == TRUE)
    	(void)strcat( pBuf, "\r\n\t<CommType>" );

    (void)strcat(pBuf, XML_COMMTYPE_ANY_LAN);

	if (fbLoadTag == TRUE)
    (void)strcat( pBuf, "</CommType>" );
}

#endif	// end #ifndef INTELLILINK_C



