/************************************************************************
*   PROJECT:        Phoenix/Janus (I-button)
   MODULE NAME:    ipsd.c 
   
   DESCRIPTION:    This file contains the variable declarations for 
                   the I-button PSD bobdata.
            
 ----------------------------------------------------------------------
               Copyright (c) 2001 Pitney Bowes Inc.
                    35 Waterview Drive
                   Shelton, Connecticut  06484
 ----------------------------------------------------------------------
 CHANGE HISTORY:

 2009.07.16 Clarisa Bellamy     FPHX 2.00
  Added string to store the Firmware version Date. BECAUSE, when Dallas/Maxim 
  makes a change in the firmware (fixes something), they do not change the 
  firmware version, but they do change the date in the firmware version string.
  They only change the firmware version for a MAJOR change in the product.
  (For example, the Asteroid and the Gemini have a different 2nd-part version 
   number. And the ECDSA has a different 1st-part version number, from non-ECDSA
   PSDs.)

 2009.06.18 Clarisa Bellamy     FPHX 2.00 , merged from FPHX 01.15 Dev.
  Update Gemini Boot Loader support:
  Add variables for Gemini Boot Loader support, and for testing it.

 2009.06.10 Clarisa Bellamy - FPHX 2.00 Changes for Canada:
 - Add a new flag fIPSD_FWVerModsIndiciaType.

 2008.09.05 Clarisa Bellamy     FPHX 01.11
  - For IBI-Lite: add variable fIPSD_IbiLiteOn.
  - For globsize cleanup: add wIPSD_FirmwareVerRawSize, and pIPSD_RandomGlobSz. 
  - To avoid possible errors if the firmware version string is as long as, or 
    longer than, our buffer: add space for a null-terminator to be added to 
    pIPSD_FirmwareVer[]. 

 2008.04.14 Clarisa Bellamy     FPHX 01.10
   To fix the problem where rx'd data globs all had their lengths stored in 
   the same variable.  Now, wIPSD_rxGlobSize, should only be used by the 
   fnTeaDistribReply function in bobwrapr.c for temporary storage of the size
   of a received glob. 
   - Add  wIPSD_paramListGlobSize and wIPSD_flexDebitOutputGlobSize.

 2008.02.27 Clarisa Bellamy     FPHX01.10
  Complete merge from Janus 15.04.02 to pick up changes for 9.0 i-button 
  and flex debit functionality.
  - Added 17 new variables. 

 Rev 1.36   Oct 23 2007            db001ko on fphx01.09_secap
   Add ulDerivedPieceCount (merged from Janus, needed for Switzerland)

* ----------------------------------------------------------------------
* PRE-CLEARCASE REVISION HISTORY:
*   $Log:   H:/group/SPARK/ARCHIVES/UIC/ipsd.c_v  $
* 
*    Rev 1.35   06 Jul 2005 16:12:14   cr002de
* 1. support for new 4.0 ibutton parameters
* 
*    Rev 1.34   Apr 21 2005 14:36:52   RO001AR
* added support for the Brazil Human Readable Piece Count: IPSDHRPieceCount[IPSD_SZ_ASCII_PIECE_CNT]
* 
*    Rev 1.33   Feb 01 2005 15:12:16   arsenbg
* put a default value of IPSD_STATE_ENABLED in fIPSD_EnableDisableStatus
* 
* 
*    Rev 1.32   15 Dec 2004 15:13:56   sa002pe
* For Janus:
* Changed wIPSD_FirmwareVerNum to lIPSD_FirmwareVerNum,
* in other words, from a word to a long because the version for
* the v3 PSDs should be 030002.
* 
*    Rev 1.31   Nov 28 2004 20:46:16   arsenbg
* the following changes were made in this file and other files in support of Canada ECDSA PCT operations:
* ibobutil.h    added new long parameter id: MFG_IPSDPROVIDERKEY
* 
* ibobutils.c   added new record in mfgVaultIPSDLongRef for MFG_IPSDPROVIDERKEY
* cjunior       added reference for BOBID_IPSD_CANADA_PROVKEYREV (ulIPSD_ProviderKeyRev)
* glob2bob.h    unmasked GLOBID_CANADAPROVKEYREV
* glob2bob.c    unmasked GLOBID_CANADAPROVKEYREV
* ipsd.h        added reference for ulIPSD_ProverKeyRev
* ipsd.c        added creation of ulIPSD_ProverKeyRev storage
* 
*    Rev 1.30   Nov 11 2004 17:40:14   arsenbg
* added new variable lIPSDSchema_1_5 and initialized it to version 1.5.
* Note that this has been added as a RAM variable that is initialized.  That is so that I can do some testing.
* At some point the variable can become a CONST since it is only used for version comparisons.
* 
* I also added a definition for IPSD_ENABLE_STATE.  TRUE means that the iButton is enabled.
* 
*    Rev 1.29   23 Aug 2004 16:21:28   cx08856
* 1. set nonzero default for prod/price version input
* to finalize franking transaction. - Craig
* 
*    Rev 1.28   Aug 18 2004 19:55:56   arsenbg
* next group of changes for Frankit.  Mostly regarding the indicia supplemental service lines and the discount piece count
* 
*    Rev 1.27   Aug 10 2004 14:10:14   arsenbg
* add support for German specific flag: add DR to AR on PVR
* 
*    Rev 1.26   Jul 25 2004 20:02:56   arsenbg
* added:
* pIPSD_GermanARatLastRefill
* pIPSD_GermanCryptoString
* pIPSD_GermanPostalIDdata
* pIPSD_GermanAuthKeyRev
* 
*    Rev 1.25   10 Jun 2004 16:47:38   CR002DE
* 1. add new IPSD variables for Germany
* 2. make entries in the message control table for new Germany commands
* 3. NO NEW SCRIPT SUPPORT YET
* 4. new ibutton error codes supported
* 
*    Rev 1.24   05 Apr 2004 16:44:16   CR002DE
* 1. support for modified barcode data glob
* 
*    Rev 1.23   Jan 20 2004 15:23:12   CL501BE
* Added new flag variable for indicating that the 
* IPSD is in the middle of a preCreate-commit cycle.
* 
*    Rev 1.22   Dec 11 2003 13:14:34   CL501BE
* Requires ipsd.h 1.29
* Changes to handle IPSD verification of graphics components:
* Added wIPDSVerifyHashSigDataSize and modified the 
* ulVerifyHashSignatureDataPtr to be 4-char array instead of a 
* long, so it doesn't have to be typecast everywhere.
* 
*    Rev 1.21   Oct 30 2003 16:05:50   CL501BE
* _Added new variable used to save a copy of the 4-byte cmdStatusResp 
*  of a redirected message.  If the status is an error, it may NOT
*  be redirected normally, and just the status needs to be put 
*  into the transfer control structure.  This will be done in 
*  bobReply, possibly AFTER other messages in the script have 
*  been sent to the IPSD and written over the 4ByteCmdStatusResp.
* _ Put conditionals around the initilization of the user password. 
*   Janus gets this from EMD, and we will too, but not yet.
* 
*    Rev 1.20   09 Oct 2003 17:14:22   defilcj
* 1. support verify hash signature message to i button
* 
*    Rev 1.19   08 Oct 2003 10:31:02   defilcj
* 1. added psd parameter variables for new i button
* 2. added variable for globsize read from i button
* 3. no initialization of password, it comes from the EMD now.
* 
*    Rev 1.18   Oct 07 2003 13:31:44   CL501BE
* _ Added BigEndian version of MfgSN which is needed for the 
*   IPSD version of Myko's VLT_PB_SYSFILE_MFG_SEQUENCE.  A 
*   corresponding change in ibobPriv.c fills this data in.
* 
*    Rev 1.17   Oct 02 2003 00:41:26   CL501BE
* Added 3 new variables for Myko-mood-simulation.
* 
*    Rev 1.16   23 Sep 2003 16:40:20   defilcj
* 1. implement VLT_INDI_DATEADV_LIMIT and 
* VLT_INDIBACKDATE_LIMIT.
* 
*    Rev 1.15   19 Sep 2003 16:20:14   defilcj
* 1. add support for UIC origin zip code manipulation.
* 
*    Rev 1.14   Sep 17 2003 17:36:40   thillas
* Modified to add two new variables
* for printing and debiting.
* 
*    Rev 1.13   Sep 16 2003 11:52:40   CL501BE
* Requires cjunior.c 1.170, ipsd.h 1.16, bobavar_p.c 1.10, 
* ibobpriv.c 1.17
* _ Moved 4 variables here from cjunior.c file: 
*   IPSDSetimeclock,pEdmIPSDClockOffset, IPSDTimerecord, 
*   bIPSDEncrKeyType (formerly bEncrKeyType.)
* _ Added new variables for refill flash parameters 
*   that are not the same size as the original myko variables.
* 
*    Rev 1.12   15 Sep 2003 14:19:18   defilcj
* 1. create pIPSD_InspectionDueDate and bIPSD_DebitGracePeriod.
* 
*    Rev 1.11   Sep 14 2003 12:06:20   thillas
* Modified the size of the random number
* to be 8 bytes instead of 12.
* 
*    Rev 1.10   11 Sep 2003 17:37:34   defilcj
* 1. add container for manufacturing serial number crc checksum.
* 
*    Rev 1.9   Sep 09 2003 22:55:48   CL501BE
* Requires files glob2bob.c rev. 1.8, ipsd.h rev. 1.12.
* For Phoenix: bobavar_p.c rev. 1.7, ibobpriv.c rev. 1.12
* _ Name of variable changed from pIPSD_indicia_data to 
*   pIPSD_barcodeData, and name of its typedef changed too.
* _ Added pIPSD_barcodeDataGlobSize, and removed iBarcodeDataSize.
* 
*    Rev 1.8   Sep 05 2003 17:29:06   thillas
* Added code for LFSR mechanism and
* Print & debiting 
* 
*    Rev 1.7   Aug 20 2003 11:33:20   thillas
* Added initial stuffs for LFSR implementation
* 
*    Rev 1.6   18 Aug 2003 14:28:16   defilcj
* 1. added new variable pIPSD_4ByteCmdStatusResp
* which is a four byte version of the ipsd status
* 
*    Rev 1.5   11 Aug 2003 10:49:38   defilcj
* 1. get 8th byte of device id after powerup
* 
*    Rev 1.4   05 Aug 2003 16:18:42   defilcj
* 1. added new variable pIPSD_DeviceID
* 
*    Rev 1.3   Jun 28 2003 17:01:30   bellamyc
* Added variable for the size of the createIndicia Glob Data
* and changed all the glob arrays from char to uchar.
* Also added some comments.
* 
*    Rev 1.2   Jun 19 2003 14:10:14   bellamyc
* Add variables for sizes of PublicProviderKey and InitDataGlob.
* Added InitDataGlob and change publicproviderkey data to be a 
* glob (size is bigger than needed in case structure changes in
* the future.)  
* Commented out a bunch of variables that haven't been used yet.
* They may not be needed or their uses may be combined.
* 
*    Rev 1.1   May 30 2003 12:09:44   bellamyc
* Copied from latest version of Janus archive, revision 1.9
* Janus will use this archive from now on.
*
*-------------------------------------------------------------------
*  This development was done on the Janus version of the archive.
     * 
     *    Rev 1.9   May 21 2003 12:11:12   bellamyc
     * Fixed Firmware Version Sub String to have room for the null-terminator.
     * 
     *    Rev 1.8   May 20 2003 12:14:24   bellamyc
     * Added 2 new variables for the PSD Firmware Version: a substring with what we THINK is just
     * the version  string, and a 2-byte numeric equivalent, since the current display function 
     * expects a word.
     * 
     *    Rev 1.7   May 12 2003 12:00:44   bellamyc
     * Added new constant data: bVltDecimalPlaces
     * 
     *    Rev 1.6   May 10 2003 21:46:26   bellamyc
     * Fix originCountry variable from 1-byte to an array of 2 bytes.
     * 
     *    Rev 1.5   May 07 2003 00:09:38   bellamyc
     * _Added new variables that are 5-byte versions of the 4-byte-money variables stored in the
     *   I-button.  They will be filled by script post functions.  
     * _changed control sum variable to use the same naming standard as the new variables.
     * 
     *    Rev 1.4   May 05 2003 10:07:46   bellamyc
     * _Added ControlSum variable, in ram, used to be in Myko.
     * _Added glob variables for Parameter List and Create Indicia data. A glob is a bunch of
     *   data which has it's structure defined in a definition record (by the emd.)
     * 
     *    Rev 1.3   Apr 26 2003 01:47:30   bellamyc
     * UserPassword is initialized to a default value on startup.
     * 
     *    Rev 1.2   Apr 12 2003 20:07:24   bellamyc
     * Fix function name.
     * 
     *    Rev 1.1   Apr 10 2003 15:57:06   bellamyc
     * Added 3 new variables for I-button driver initialization response data.
     * 
     *    Rev 1.0   Apr 04 2003 15:02:28   bellamyc
     * Initial revision.  Copied from Comet rev. 1.0.
     * 
*----------------------------------------------------------------
* History previous to Janus:
* 
*    Rev 1.0   Mar 19 2003 11:02:20   bellamyc
* Initial revision.
* 
*************************************************************************/


#include "global.h"
#include "ipsd.h"
#include "rateglbl.h"
#include "datdict.h"
#include "fwrapper.h"





//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//
//      These are variables that used to be in the MYKO psd, but are now in RAM, or
//      someplace else...?
// ----------------------------------------------------


// These are arrays of size SPARK_MONEY_SIZE, which currently = 5.
MONEY   mStd_ControlSum;
MONEY   mStd_DescendingReg;
MONEY   mStd_MaxDescReg;
MONEY   mStd_MinPostage;
MONEY   mStd_MaxPostage;

// These ram values are converted from Flash parameters that are
//  a different size than the psd parameters that they replace.
// They are converted and stored when the IPSD is first powered up.
ushort  wIPSD_RefillIncrement;      // From IBP_REFILL_INCREMENT
MONEY   mStd_minRefillVal;          // From ILP_MIN_REFILL_VAL
MONEY   mStd_maxRefillVal;          // From ILP_MAX_REFILL_VAL


// This used to be in Myko psd, but is now a constant.  For compatibility, it needs to
//  be accessible through fnValidData().
uchar bVltDecimalPlaces = 0;


//----------------------------------------------------------------------------------------
//      These are conversions of static variables for when the application expects it 
//          in a different format.
// ----------------------------------------------------

//---------------------------------------------------------------------
// Firmware Version info:

char    pIPSD_FirmwareVerSubStr[ IPSD_MAXSZ_FWVER_SUB_LEN +1 ];
UINT32  lIPSD_FirmwareVerNum;

char    pIPSD_FirmwareVerDateStr[ IPSD_MAXSZ_FWVER_DATE_LEN +1 ];

// This flag is set to TRUE if the firmware version is 3.x or 5.x, because....
// For IPSD versions 3.x and 5.x:       for all others:      
//       Indicia Type 5 = USPS ECDSA            Indicia Type 5 =  BELGIUM
//       Indicia Type 6 = Canada ECDSA          Indicia Type 6 =  NETHERLANDS
// The function that sets it is fnConvertFirmwareVersion in cbobpriv.c 
BOOL    fIPSD_FWVerModsIndiciaType = FALSE;


//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------


//---------------------------------------------------------------------
//  Command Status stuff:

//  We always want to know the status of the command...
uchar   pIPSD_cmdStatusResp[ IPSD_SZ_CMD_STATUS ];              // Every response from IPSD should have this.
uchar   pIPSD_4ByteCmdStatusResp[ IPSD_SZ_4BYTE_CMD_STATUS ];   // the 4 byte version, cuz mfg wants it that way
uchar   pIPSD_savedStatus[ IPSD_SZ_4BYTE_CMD_STATUS ];          // Save the above data here whenever doing packetized/ 
                                                                // redirected messages.  So that it can be returned
                                                                // in the transfer control structure, AFTER other
                                                                // messages in the script have been sent/rx'd.


//----------------------------------------------------------------------------------------

uchar   bIPSD_msgType;                              // Used in longer messages TO the IPSD.
uchar   pIPSD_state[ IPSD_SZ_STATE ];               // Current state of the IPSD.
BOOL    fIpsdIndiPrecreatedState;                   // Set by bobtask when it successfully sends a 
                                                    //  Precreate message, and cleared when any other message is sent.
uchar   pIPSD_DCstatus[ IPSD_SZ_PB_DATACENTER_STATUS ]; // Latest Data Center Status received from DC.
uchar   pIPSD_auditStatus[ IPSD_SZ_AUDIT_STATUS ];  // Latest Audit Status received from infrastructure.

// This is the first piece of information we get from the IPSD.
//  We need to send back the Encrypted Nonce to do TransportUnlock 
uchar   pIPSD_nonce[ IPSD_SZ_NONCE ];               // Use GetChallenge to get it from the IPSD.
uchar   pIPSD_encryptedNonce[ IPSD_SZ_NONCE ];      // Encrypted version comes PCT.
uchar   pIPSD_ChallengeAndStat[ IPSD_SZ_NONCE + 4 ]; // 4  byte ipsd status + nonce

// The following long variable stores the ASCII version ID at which the 
// iButton parameter list schema changed.  It is set to 1.500 because, prior
// to that version the iButton did not support the enable/disable states.
// The variable below can be directly compared to the first four bytes of pIPSD_hostSW_ID
// to determine if the current iButton is older than version 1.5
ulong   lIPSDSchema_1_5 = 0x31353030;

// Set in IPSD by Initialize Cmd
//      Janus uses EMD to initialize, but Phoenix will initialize here for until the emd is updated.
#ifdef JANUS
uchar   pIPSD_userPassword[ IPSD_SZ_USER_PWD ];
#else   
uchar   pIPSD_userPassword[ IPSD_SZ_USER_PWD ] =
   {     // This is the value used for testing.  Will be replaced by actual value when EMD is updated.
        0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38 
   };      
#endif

//*************************************************************************************************************
//  GPPL =  Get PSD Parameter List Command retrieves this info from the PSD.


//----------------------------
//  Where am I going?
uchar   bIPSD_currencyCode;                             // Set in IPSD by Initialize Cmd - GPPL  
uchar   pIPSD_originCountry[ IPSD_SZ_ORIGIN_COUNTRY ];  // Set in IPSD by Initialize Cmd - GPPL
uchar   pIPSD_zipCode[ IPSD_SZ_ZIPCODE ];               // Set in IPSD by Ld Postal CfgData Cmd - GPPL

// IPSD does not allow an origin zip change unless you perform a load postal config, using a signed data record
// The Rates group must be able to change the origin zip code via a remote messaging protocol without
// using the load postal config message to facilitate automated testing. The UIC will manage this variable and
// give it to the rating module for postage and fee calculation, when it is non-zero.
uchar   pIPSD_FakeOriginZipcode[ IPSD_SZ_ZIPCODE ] = {0,0,0,0,0};

// This point is mapped to variable ID VLT_IDENT_FILE_ZIPCODE, the legacy ID the system uses to get the origin zip
// It is set to the real IPSD zip, unless the fake one is not NULL
uchar   pIPSD_OriginZipCode[ IPSD_SZ_ZIPCODE ];

// Used by UIC to facilitate remote rates testing


//----------------------------
//  Who am I?

// Low-level info.  Obtained by init command. 
//  The first two should match numbers stamped on outside of I-button package:
uchar   bIPSD_familyCode;                               // 46
uchar   bIPSD_MfgSNcrc;                                 // Manufacturer's serial number crc checksum
uchar   pIPSD_MfgSNlsbf[ IPSD_MFGSN_SZ ];               // Manufacturer's serial number, in hex, little endian. (how it comes from the driver.)
unichar puIPSD_MfgSNAscii[ IPSD_MFGSNASCII_LEN +1 ];    // Serial number translated to ASCII (hex), Big Endian, in case we want to display it anywhere.
uchar   pIPSD_DeviceID[ IPSD_DEVICEID_SZ ];             // family code + Manufacturer's serial number + CRC (8 bytes)
// Not part of a message...
unsigned long   ulIPSD_MfgSN_Be = 0;                    // Manufacturer's serial number, in hex, converted to BIGEndian.


UINT8   pIPSD_FirmwareVer[ IPSD_MAXSZ_FWVER_LEN +1 ];   // In IPSD firmware, Retrieved by GetFirmwareVersion cmd.
UINT16  wIPSD_FirmwareVerRawSize;                       // Number of bytes rx'd in the response to the GetFirmwareVersion cmd.
uchar   pIPSD_hostSW_ID[ IPSD_SZ_HOST_SW_ID ];          // Set in IPSD by LdPostalCfgData Cmd - GPPL

uchar   pIPSD_PCN[ IPSD_SZ_PSDPCN ];                    // Set in IPSD by Initialize Cmd - GPPL
uchar   pIPSD_certificatSN[ IPSD_SZ_SN_CERTIFICATE ];   // Set in IPSD by Authorize Cmd - GPPL
uchar   pIPSD_PBISerialNum[ IPSD_SZ_SN_PBI ];           // Set in IPSD by Initialize Cmd - GPPL
uchar   pIPSD_indiciaSN[ IPSD_SZ_SN_INDICIA ];          // Set in IPSD by Initialize Cmd - GPPL
// IndiciaSN and psdSerialNum:  
//   USA, Italy:
//      The Indicia serial num is an ASCII string with 10 digits in it.
//      This is converted to binary and stored in the "PSD Serial Number" in the IPSD.
//   France:
//      The Indicia serial num is an ASCII string with 9 digits in it, and is used as-is in
//      the french indicium.  The "PSD Serial Number" is not used for France.
uchar   pIPSD_psdSerialNum[ IPSD_SZ_SN_IND_BIN ];       // The only time the UIC sees this is as part
                                                        // of the indicium message returned from the IPSD.     


uchar   pIPSD_manufSMR[ IPSD_SZ_MANUF_SMR ];         // Set in IPSD by Initialize Cmd - GPPL
uchar   pIPSD_manufacturerID[ IPSD_SZ_MANUF_ID ];    // Set in IPSD by Ld Postal CfgData Cmd - GPPL
uchar   pIPSD_modelNumber[ IPSD_SZ_MODEL_NUM ];      // Set in IPSD by Ld Postal CfgData Cmd - GPPL

uchar   bIPSD_indiciaType;                           // Set in IPSD by Initialize Cmd - GPPL
uchar   bIPSD_indiciaVerNum;                         // Set in IPSD by Initialize Cmd - GPPL
uchar   bIPSD_algorithID;                        // Set in IPSD by Initialize Cmd - GPPL


//  French stuff:
uchar   bIPSD_frenchMAC_Sz;                          // Set in IPSD by Initialize Cmd - GPPL
uchar   bIPSD_frenchOCR_Sz;                          // Set in IPSD by Initialize Cmd - GPPL

// Misc stuff:
uchar   pIPSD_FreeRam[ IPSD_SZ_FREERAM ]; 
uchar   pIPSD_PORCount[ IPSD_SZ_POR_CNT ];
uchar   pIPSD_Speed[ IPSD_SZ_SPEED ];
uchar   bIPSD_ItalyCurrencyCode;                            //  i-button with firmware >= 1.5
uchar   bIPSD_IndiciumCode;                                 //  i-button with firmware >= 1.5
uchar   bIPSD_AlgorithmObjectID;                            //  i-button with firmware >= 1.5
uchar   pIPSD_PieceCountAtRefill[ IPSD_SZ_PIECE_CNT ];      //  i-button with firmware >= 1.5
uchar   pIPSD_MeterKeyExpiryDate[ IPSD_SZ_KEY_EXP_DATE ];   //  i-button with firmware >= 1.5
uchar   fIPSD_EnableDisableStatus = IPSD_STATE_ENABLED;     //  i-button with firmware >= 1.5
uchar   IPSDHRPieceCount[IPSD_SZ_ASCII_PIECE_CNT];          //  i-button, added for Brazil
uchar   bIPSD_FrenchAlphaNumPieceCount;                     //  i-button with firmware >= 4.0
uchar   bIPSD_ResetPcOnPvr;                                 //  i-button with firmware >= 4.0
uchar   bIPSD_AllowZeroPostage;                             //  i-button with firmware >= 4.0
uchar   pIPSD_ZeroPostagePieceCount[IPSD_SZ_PIECE_CNT];     //  i-button with firmware >= 4.0
ulong   ulDerivedPieceCount;                                //  for most markets its the same as the total piece count
                                                            //  for some markets its the non zero pc = total pc - zero pc

uchar   bIPSD_BelgianApplicationValue;                      //  ibutton with firmware >= 6.0
uchar   bIPSD_IbiLiteIndiciaVersion;                        //  ibutton with firmware >= 8.0
uchar   bIPSD_IbiLiteVendorModel;                           //  ibutton with firmware >= 8.0
uchar   pIPSD_HmacDesMacKeyID[IPSD_SZ_KEY_ID];              //  ibutton with firmware >= 9.0
uchar   pIPSD_PsdAuthorizedKeyRev[IPSD_SZ_KEY_REV];         //  ibutton with firmware >= 9.0

uchar   pIPSD_FlexDebitValueKeyData[IPSD_SZ_FLX_VALKEY];    //  ibutton with firmware >= 9.0


//----------------------------
// Postage Download and Refill stuff:
uchar   fIPSD_keypadRefillInvokesInspect;            // Set in IPSD by Initialize Cmd - GPPL
uchar   bIPSD_refillType;                            // Set in IPSD by Initialize Cmd - GPPL
uchar   fIPSD_clearARwithPVR;                        // Set in IPSD by Initialize Cmd - GPPL
uchar   pIPSD_refillCount[ IPSD_SZ_REFILL_CNT ];        // Parameter FROM PSD - GPPL
uchar   pIPSD_pieceCount[ IPSD_SZ_PIECE_CNT ];          // Parameter FROM PSD - GPPL


//----------------------------
// Time/Date stuff:

//  Relative times are in binary seconds.
//  Date/Times are in binary seconds since 00:00:00 01/01/2000
uchar   pIPSD_dateAdvanceLimit[ IPSD_SZ_TIME ];      // Set in IPSD by Initialize Cmd - GPPL
uchar   pIPSD_backdateLimit[ IPSD_SZ_TIME ];         // Set in IPSD by Initialize Cmd - GPPL
//  
char    bIPSD_dateAdvanceLimitDaze;                 // signed number of days equal to pIPSD_dateAdvanceLimit      
char    bIPSD_backdateLimitDaze;                    // signed number of days equal to pIPSD_backdateLimit

//  The PSD's Real Time Clock is the time (binary seconds) since it was "started".  
uchar   pIPSD_RealTimeClock[ IPSD_SZ_TIME ];        // Retrieved with GetRTC msg..

//  The PSD will add this clock offset to it's RTC time to get GMT date/time.
uchar   pIPSD_clockOffset[ IPSD_SZ_TIME ];           // Set in IPSD by Initialize Cmd - GPPL

//  The PSD will add this offset to the GMT time calculated above to get local time.  Includes drift.
uchar   pIPSD_GMTOffset[ IPSD_SZ_TIME ];            // Set in IPSD by SetGMTOffset Cmd - GPPL

uchar    pIPSD_RTCLocalDate[IPSD_SZ_TIME];  // retreived with get module status messag (P2 =1), new in 9.0 ibutton


//----------------------------
// Watchdog Timer stuff:
//      When the time in the watchdog timer is less than the current time, no indicia can be printed...
//     IF a DevAuditResp message is successful, the Reset value is added to the current time
//       and stored in the watchdog timer 
//     If KeypadRefillInvokesInspect is set, then the Reset value is added to the current time
//       and stored in the watchdog timer when there is a keypad refill.
uchar   pIPSD_watchDogTmr[ IPSD_SZ_TIME ];           // Set to current local time by an Initialize Cmd - GPPL
uchar   pIPSD_watchDogTmrReset[ IPSD_SZ_TIME ];      // Set in IPSD by Ld Postal CfgData Cmd - GPPL
uchar   pIPSD_watchDogTmrExpire[ IPSD_SZ_TIME ];     // Part of the DevAudit Msg from the IPSD.
                                                     // Must be returned unchanged in the DevAudit Resp.

uchar   pIPSD_InspectionDueDate[4];                  // four bytes: century,year,month,day  
                                                     // derived from watchdog timer value

const   uchar   bIPSD_DebitGracePeriod = 0;          // no grace period supported by ibutton

//----------------------------
// $$$$ Parameters:
m5IPSD  m5IPSD_ascRegPreset;                // Set in IPSD by Initialize Cmd - GPPL
m5IPSD  m5IPSD_maxAscReg;                   // Set in IPSD by Initialize Cmd - GPPL
m4IPSD  m4IPSD_maxDescReg;                  // Set in IPSD by Initialize Cmd - GPPL

m4IPSD  m4IPSD_postageMax;                  // Set in IPSD by LdPostalCfgData Cmd - GPPL
m4IPSD  m4IPSD_postageMin;                  // Set in IPSD by LdPostalCfgData Cmd - GPPL



//*************************************************************************************
//          GLOB DATA, used in conjunction with data defintion records:
//----------------------------------------------------
//          GLOBS
// Not sure how big to make these, since they must be able to accomodate data
//  structures that will be defined in the future.

// wIPSD_rxGlobSize is now obsolete, replaced by a static in bobwrapr.c.  

// The response from the GetPsdParameters message is in glob form,
//  since it will be different for different countries.  The data format
//  will be defined in a definition record in flash. 
uchar    pIPSD_paramListGlob[ IPSD_MAXGLOBSZ_PARAMLIST ];
ushort   wIPSD_paramListGlobSize;

// The createIndicia message data is in glob form since it may change 
//  from country to country.  The data format will be defined in a 
//  definition record in flash.  The function that uses the definition 
//  to load up the glob will also figure out the size.
UINT8    pIPSD_createIndDataGlob[ IPSD_MAXGLOBSZ_CREATEIND ];
UINT16   wIPSD_createIndDataGlobSize;
// If this is set to TRUE, then we want to use P2 to tell the IPSD to 
//  create the IBI-Lite indicia data instead of the normal indicia data.
UINT8   fIPSD_IbiLiteOn      = FALSE;


uchar    pIPSD_flexDebitDataGlob[ IPSD_MAXGLOBSZ_FLEXDEBIT ];
uchar    pIPSD_flexDebitTempData[ IPSD_MAXGLOBSZ_TEMPFLEXDATA ];
ushort   wIPSD_flexDebitDataSize;


// Comes from EDM (PCT) and is stored here until the COMMIT_PSD_PARAMS message
// from EDM (PCT) instructs us to send this and the provider key data to the 
//  IPSD in the INITIALIZE message.
uchar    pIPSD_InitDataGlob[ IPSD_MAXGLOBSZ_INITDATA ];
// Comes from EDM.  Actual size of data in initdataglob above.
ushort  wIPSD_InitDataSize = 0;

//----------------------------------------------------
// Components of Indicia Input Data ...

uchar bIPSD_CreateIndiciaMessageType;

// Components of Indicia Input Data
// for Germany....
uchar   pIPSD_GermanServiceID[IPSD_GERMAN_SERVICEID_SZ];
uchar   pIPSD_GermanProductKey[IPSD_GERMAN_PRODUCTKEY_SZ];
uchar   pIPSD_GermanSecondDate[IPSD_GERMAN_SECONDDATE_SZ];
uchar   pIPSD_GermanPieceCount[IPSD_GERMAN_PIECECOUNT_SZ];
uchar   pIPSD_GermanVariablePostalData[IPSD_GERMAN_VARPOSTALDATA_SZ];
// Other data for Germany
uchar   pIPSD_GermanARatLastRefill[SPARK_MONEY_SIZE];
uchar   pIPSD_GermanCryptoString[IPSD_GERMAN__CRYPTO_SZ];
uchar   pIPSD_GermanPostalIDdata[IPSD_GERMAN__POSTID_SZ];
uchar   pIPSD_GermanAuthKeyRev[IPSD_GERMAN__AUTHKEYREV_SZ];
uchar   bIPSD_GermanAddDRtoARonPVRFlag;
unichar usIPSD_GermanSuppleLine1[PCL_STRING_LENGTH];
unichar usIPSD_GermanSuppleLine2[PCL_STRING_LENGTH];
uchar   ulIPSD_ProviderKeyRev[IPSD_SZ_KEY_REV];


// Components of Indicia Input Data
// for NetSet2....(some are generic, most are not)
uchar pIPSD_NetSetCLR[IPSD_NETSET_CLR_SZ];
uchar pIPSD_NetSetIssuerCode[IPSD_NETSET_ISSUERCODE_SZ];
uchar pIPSD_AdvancedDateOffset[IPSD_ADVANCEDDATEOFFSETASCII_SZ];
uchar pIPSD_NetSetProductCode[IPSD_NETSET_PRODUCTCODE_SZ];
uchar pIPSD_NetSetSecurityCodeVer[IPSD_NETSET_SECURITYCODEVER_SZ];


//*************************************************************************************
//          KEY DATA:
//----------------------------------------------------

// Set in IPSD by Initialize Cmd. Comes from EDM (PCT).
// This needs to be stored in the UIC until the EDM sends the COMMIT_PSD_PARAMS message
//  (or something like that?).
uchar   pIPSD_providerPublicKeyData[ IPSD_MAXGLOBSZ_PROVIDERKEYDATA ];
// Comes from EDM.  Actual size of data in providerpublickeyglob above.
ushort  wIPSD_providerPublicKeyDataSize = 0;
//tKEY_OFFSET_TBL   pIPSD_providerPublicKeyTbl;


// From PCT, used to verify key information when sending it to IPSD.  
//  The IPSD computes it's own, and they should match.
uchar   pIPSD_DESMAC[ IPSD_SZ_DESMAC ];


// Hash value and Key data to verify it are sent in Verify Hash Value Msg.
//uchar   pIPSD_hashValue[ IPSD_SZ_HASH_VAL ];
//uchar   pIPSD_signingKeyData[ IPSD_MAXSZ_KEY_DATA ];
//tKEY_OFFSET_TBL   pIPSD_signingKeyTbl;


//----------------------------
//  These are the Secret Keys, they are loaded using the LdEncrKey Msgs.
//  IPSD Secret Key (Until this is loaded during initialization, the IPSD uses the Transport Key)
//      Encrypted using previous secret key.
//uchar   pIPSD_secretKey[ IPSD_SZ_SECRET_KEY_DATA ];
//  IPSD key derived from the Secret Key and the PSD Serial Number.  French Only.
//      Encrypted using secret key.
//uchar   pIPSD_KfabMaKey[ IPSD_SZ_SECRET_KEY_DATA ];
//  IPSD key loaded at customer sight.  French Only.
//      Encrypted using KfabMaKey key.
//uchar   pIPSD_MaKey[ IPSD_SZ_SECRET_KEY_DATA ];
//  IPSD key used for keypad refills.
//      Encrypted using secret key.
//uchar   pIPSD_keypadRefillKey[ IPSD_SZ_SECRET_KEY_DATA ];

// This is where the encrypted version of any of the above keys is stored, temp
//uchar   pIPSD_encryptedNewKey[ IPSD_SZ_SECRET_KEY_DATA ];

//uchar   pIPSD_keyName[ IPSD_SZ_KEY_NAME ];
//uchar   pIPSD_secretKeyRev[ IPSD_SZ_KEY_REV ];
//uchar   pIPSD_refillKeyRev[ IPSD_SZ_KEY_REV ];
char    *ulVerifyHashSignatureDataPtr;
ushort  wIPSDVerifyHashSigDataSize = 0;    

//*************************************************************************************************************
// Status info:

// Flag indicating that a preCommit was issued.  
//  Cleared when the CommitTransaction is sent, OR when...
BOOL    fIPSDWaitForCommit = FALSE;


//*************************************************************************************************************
//  Variable data for Commands:

// Login data, created from HASH and nonce...
uchar   pIPSD_loginData[ IPSD_SZ_LOGIN_DATA ];

// German Finalizing Frank Products/Prices version
uchar   bIPSD_ProdPriceVersion = 01;               // some nonzero default

// Get Module Status data...
m5IPSD  m5IPSD_ascReg;                             // 
m4IPSD  m4IPSD_descReg;                            // 

// Place to put the data for requesting an indicia from the IPSD.
tIPSD_INDICIUM_CREATE_DATA    rIPSD_indiciumCreate;

// Indicia Data from PSD stored here.  Changed the name to indicate it is just the barcode data.
uIPSD_BARCODE_DATA      pIPSD_barcodeData;
ushort                  wIPSD_barcodeDataGlobSize;

// Flex Debit data output (unionized)
uIPSD_FLEXDEBIT_OUTPUT  pIPSD_flexDebitOutput;
ushort                  wIPSD_flexDebitOutputGlobSize = 0;



// Barcode Data originally from IPSD but modified by UIC
uIPSD_BARCODE_DATA      pIPSD_TempModifiedBarcodeData;
uIPSD_BARCODE_DATA      pIPSD_ModifiedBarcodeData;
ushort                  wIPSD_ModifiedBarcodeDataGlobSize = 0;

// This is the packet data 
tIPSD_PACKET_INFO       rIPSD_packetInfo;
char    pIPSD_packet_data[ MAX_IPSD_PCKT_SZ ];

// This is where messages can be stored before they are broken into chunks. (Probably don't really need, but....)
uchar pIPSDLongMsgBuffer[ IPSD_MAX_UNCHUNKED_MSGDATA_LEN ];


uchar   pIPSD_keypadRefillComboData[IPSD_SZ_KEYPAD_TRANS_SIG ] ;        // 10 bytes of ASCII
uchar   pIPSD_keypadWithdrawalComboData[IPSD_SZ_KEYPAD_TRANS_SIG ] ;    // 10 bytes of ASCII

m4IPSD  m4IPSD_postageRequested;                   // For PVD or Keypad Refill
m4IPSD  m4IPSD_postageValueDownloaded;

// Don't know if we ever use these in the UIC.
//uchar   pIPSD_PVDnldReqVerNum;
//uchar   bIPSD_PVDnldMsgVerNum;
//uchar   bIPSD_PVRefundReqVerNum;
//uchar   bIPSD_PVRefundMsgVerNum;
//uchar   pIPSD_devAuditMsgVerNum;



// Used to store various signatures, temp
uchar   pIPSD_signature1[ IPSD_MAXSZ_SIG ];
uchar   pIPSD_signature2[ IPSD_MAXSZ_SIG ];

UINT8   pIPSD_RandomSz;
UINT16  pIPSD_RandomGlobSz;     // The number of random bytes actually returned from the IPSD.
UINT8   pIPSD_Random[ IPSD_MAXSZ_RANDOM ];

//*************************************************************************************************************
//  Log Data:

uchar   pIPSD_IndCreateLog[ IPSD_LOGSZ_INDCREATE ];
uchar   pIPSD_RefillLog[ IPSD_LOGSZ_REFILL ];
uchar   pIPSD_AuditLog[ IPSD_LOGSZ_AUDIT ];



//*************************************************************************************************************
//  Signed Message Data that must come from an outside source:

tIPSD_PVD_MSG           rIPSD_PVDMessage;
tIPSD_PVR_MSG           rIPSD_PVRMessage;
tIPSD_DEV_AUDIT_RESP    rIPSD_devAuditResponse;


//*************************************************************************************************************
// data needed for LFSR implementation for phoenix
uchar bIPSD_CountryID;
uchar bIPSD_CurrencyType;
uchar bIPSD_PrintSessionType;
uchar pIPSD_phcNonce[LFSR_PHC_NONCE_SIZE];
uchar pIPSD_phID[LFSR_PH_ID_SIZE];
uchar pIPSD_LFSRrandomNumber[LFSR_RANDOM_NUMBER_SIZE];

uchar IPSDAlphaSubDate[9];
uchar IPSDHRPostageVal[5];

//*************************************************************************************************************
//  Stuff for EDM (manufacturing):

// When EDM wants to set the system clock, it stores the time here.
DATETIME    IPSDSetimeclock;
// After the UIC calculates what the clock Offset SHOULD be, it is
//  stored here for retrieval by the EDM.
uchar       pEdmIPSDClockOffset[ IPSD_SZ_TIME ];
// Time in string format for retrieval by EDM.
uchar       IPSDTimerecord[ 16 ];

// Must set this before seding Secret Key data, so UIC knows WHICH 
//  secret key message to send.
uchar       bIPSDEncrKeyType;

//*************************************************************************************************************
//  Stuff for Myko-Mood-Simulation :

// These are reset to 0 for every power up, then modified by functions 
//      fnIPSDCheckLowBattAndInspect and fnEvalIpsdMood 
//  in ibobPriv when appropriate.
//  Should be done at least once a day, and anytime state changes, or
//  there is a refill.  
ushort  wIPSD_mood = 0;         // IPSD equivalent of Myko PSD mode
ushort  wIPSD_mood2 = 0;        // IPSD equivalent of Myko PSD internal state
ulong   lIPSD_mood3 = 0;        // IPSD equivalent of Myko PSD PSDStatusWord.

//*************************************************************************************************************
//  Stuff for Gemini Boot Loader:

//-----------------------------------------------------------------
// Program Mode of the I-button.  Values defined in ipsd.h:
UINT8   bIPSD_ProgMode  = IPSD_MODE_NONE;

//-------------------------------------------------------------------------------- 
// These are used to create the outgoing message to the Gemini Boot Loader:

// The header is 8 bytes, but some are not used, and must be zero.           
const UINT8  pIPSD_BL_HdrFill[ 8 ]  = { 0, 0, 0, 0, 0, 0, 0, 0 };

 // Address to load data at, low-byte first.
UINT8   pIPSD_BL_HdrAdr[ 3 ];

//-----------------------------------------------------------------------------
// These are used to store data received from the Gemini Boot Loader:

// This is used to store the raw data received from the driver:
UINT8   pIPSD_Bl_ReplyRaw[ IPSD_BL_MAX_RPLY_LEN ];     
// Place to store the length of a variable-length reply:
UINT16  wIPSD_Bl_ReplySize;             

// Variable-length Reply string from Boot Loader ReadMicroStatus message.
UINT8   pIPSD_Bl_MicroStatusString[ ISPD_BL_MICROSTATLEN +1 ];       


//-----------------------------------------------------------------
// Boot Loader Reply:
UINT8   pIPSD_Bl_ReplyHeader[ 4 ];       // "KBR" followed by 1-byte command-ID that this is a response to.
UINT8   bIPSD_Bl_ReplyStatus;            // 1-byte Reply Message status
UINT8   bIPSD_Bl_HdrRemainder;           // Place to store the remainder of the reply header.


//-----------------------------------------------------------------------------
// Every device in the error directory has to have "status" and a "state" variables.
UINT8   bIPSD_Bl_ReplyState;             // Not sure yet how to use this.  Required by Err Directory table.


// End of Gemini Stuff
//*************************************************************************************************************
     


void fnInitPSDNumDecimal()
{

  bVltDecimalPlaces = fnFlashGetIBByteParm(IBP_DECIMAL_PLACES);
  return;

}
// ----------------------------------------


