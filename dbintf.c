/************************************************************************
*   PROJECT:        Mega
*   COMPANY:        Pitney Bowes
*   AUTHOR :        George Monroe
*   MODULE NAME:    $Workfile:   dbintf.c  $
*   REVISION:       $$
*       
*   DESCRIPTION:
* ----------------------------------------------------------------------
*               Copyright (c) 2002 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*
*       REVISION HISTORY:
*       $Log:   H:/group/SPARK/ARCHIVES/UIC/dbintf.c_v  $
* 
*    Rev 1.5   04 Nov 2003 12:05:44   MA507HA
* Lint Clean Up
* 
*    Rev 1.4   Jan 07 2003 11:32:02   MA507HA
* Added function to initialize a result set
* 
* 
*    Rev 1.3   Jan 07 2003 09:00:18   MA507HA
* Added include files for function prototypes
* 
*    Rev 1.2   Dec 11 2002 18:35:42   MA507HA
* added result set type member to the record set
* 
* 
*    Rev 1.1   Dec 10 2002 16:12:14   monrogt
* Added a function to return the current cursor position
* 
* 
*    Rev 1.0   Dec 06 2002 19:11:46   monrogt
* support for new abacus ds
* 
* 
* 
*************************************************************************/
#include "stdlib.h"
#include "dbintf.h"
#include "string.h"


/* **********************************************************************
// FUNCTION NAME: int DBIntfAddResultRec( DBRESULTSET *set , RESENTRY *entry )
// PURPOSE: 
//
// AUTHOR: George Monroe
//
// INPUTS: 
//         
//         
// RETURNS:
//        
// **********************************************************************/
int DBIntfAddResultRec( DBRESULTSET *set , RESENTRY *entry )
{
  unsigned long sz;
  RESENTRY *rlp;
  int ec = 0;

  if( set->numOfRecords < set->recordBufSize )
    {
      set->resRecs[ set->numOfRecords ] = *entry;
      set->numOfRecords++;
    }
  else
    {
      sz = set->recordBufSize + RECBUFFERSIZEINCR;
      rlp = ( RESENTRY *) malloc( sz * sizeof(RESENTRY) );
      if(rlp)
	{
	  if(set->resRecs)
	    {
	      memcpy( rlp , set->resRecs , set->numOfRecords * sizeof(RESENTRY ));
	      free(set->resRecs);
	    }
	  set->resRecs = rlp;
	  set->recordBufSize = sz;
	  rlp[ set->numOfRecords ] = *entry;
	  set->numOfRecords++;
	}
      else
	ec = -1;
    }
  return(ec);
}


/* **********************************************************************
// FUNCTION NAME: int DBIntfEmptyResultSet( DBRESULTSET *set )
// PURPOSE: reset record set to contain no records
//
// AUTHOR: George Monroe
//
// INPUTS: 
//         
//         
// RETURNS:
//        
// **********************************************************************/
int DBIntfEmptyResultSet( DBRESULTSET *set )
{
  set->numOfRecords = 0;
  set->nextRecIndex = 0;
  return(0);
}

/* **********************************************************************
// FUNCTION NAME: int DBIntfResetResultSet( DBRESULTSET *set )
// PURPOSE: reset the retrieval position back to the beginning
//
// AUTHOR: George Monroe
//
// INPUTS: 
//         
//         
// RETURNS:
//        
// **********************************************************************/
int DBIntfResetResultSet( DBRESULTSET *set )
{
  set->nextRecIndex = 0;
  return(0);
}

/* **********************************************************************
// FUNCTION NAME: int DBIntfSetResultSetReadPos( DBRESULTSET *set , int pos )
// PURPOSE: reset the retrieval position to the position given
//
// AUTHOR: George Monroe
//
// INPUTS: 
//         
//         
// RETURNS:
//        
// **********************************************************************/

int DBIntfSetResultSetReadPos( DBRESULTSET *set , int pos )
{
  if( pos > set->numOfRecords )
    set->nextRecIndex = set->numOfRecords;
  else
    {
      if(pos < 0 )
	set->nextRecIndex = 0;
      else
	set->nextRecIndex = pos;
    }
  return(set->nextRecIndex );
}

/* **********************************************************************
// FUNCTION NAME: int DBIntfGetResultSetReadPos( DBRESULTSET *set )
// PURPOSE: return the current record position
//
// AUTHOR: George Monroe
//
// INPUTS: 
//         
//         
// RETURNS:
//        
// **********************************************************************/

int DBIntfGetResultSetReadPos( DBRESULTSET *set )
{
  return(set->nextRecIndex );
}

/* **********************************************************************
// FUNCTION NAME: int DBIntfReturnResultSetCount( DBRESULTSET *set )
// PURPOSE: 
//
// AUTHOR: George Monroe
//
// INPUTS: 
//         
//         
// RETURNS:
//        
// **********************************************************************/
int DBIntfReturnResultSetCount( DBRESULTSET *set , RESENTRY **entries )
{
  *entries = set->resRecs;
  return( set->numOfRecords );
}


/* **********************************************************************
// FUNCTION NAME: int DBIntfGetResultRec( DBRESULTSET *set , int numRec , RESENTRY *entries  )
// PURPOSE: 
//
// AUTHOR: George Monroe
//
// INPUTS: 
//         
//         
// RETURNS:
//        
// **********************************************************************/
int DBIntfGetResultRec( DBRESULTSET *set , int numRec , RESENTRY *entries  )
{
  int rem,num;

  if( set->nextRecIndex < set->numOfRecords )
    {
      rem = set->numOfRecords - set->nextRecIndex;
      if(rem < numRec)
	{
	  memcpy( entries , &set->resRecs[ set->nextRecIndex ] , rem * sizeof(RESENTRY));
	  set->nextRecIndex += rem;
	  num = rem;
	}
      else
	{
	  memcpy( entries , &set->resRecs[ set->nextRecIndex ] , numRec * sizeof(RESENTRY));
	  set->nextRecIndex += numRec;
	  num = numRec;
	}
    }
  else
    num = -1;
  return(num);
}

/* **********************************************************************
// FUNCTION NAME: int DBIntfPutResultRec( DBRESULTSET *set , int numRec , RESENTRY *entries  )
// PURPOSE: 
//
// AUTHOR: George Monroe
//
// INPUTS: 
//         
//         
// RETURNS:
//        
// **********************************************************************/
//int DBIntfPutResultRec( DBRESULTSET *set , int numRec , RESENTRY *entries  )
//{
//  int x,ec;
//
//  for( x = 0 ; x < numRec ; x++ )
//    {
//      ec = DBIntfAddResultRec( set , entries );
//      if( ec < 0 )
//	break;
//      entries++;
//    }
//  return(ec);
//}


/* **********************************************************************
// FUNCTION NAME: int DBIntfSetResultRecSetType( DBRESULTSET *set , int type  )
// PURPOSE:  sets the record set type. used by caller as a way of identifing the type
//
// AUTHOR: George Monroe
//
// INPUTS: 
//         
//         
// RETURNS:
//        
// **********************************************************************/
int DBIntfSetResultRecSetType( DBRESULTSET *set , int type )
{
  set->recordSetType = type;
  return(type);
}

/* **********************************************************************
// FUNCTION NAME: int DBIntfGetResultRecSetType( DBRESULTSET *set )
// PURPOSE:  returns the record set type. used by caller as a way of identifing the type
//
// AUTHOR: George Monroe
//
// INPUTS: 
//         
//         
// RETURNS:
//        
// **********************************************************************/
int DBIntfGetResultRecSetType( DBRESULTSET *set )
{
  int type;
 
  type = set->recordSetType;
  return(type);
}

/* **********************************************************************
// FUNCTION NAME: DBIntfInitializeResultSet( DBRESULTSET *set )
// PURPOSE: reset record set to contain no records
//
// AUTHOR: George Monroe
//
// INPUTS: 
//         
//         
// RETURNS:
//        
// **********************************************************************/
int DBIntfInitializeResultSet( DBRESULTSET *set )
{
  set->numOfRecords = 0;
  set->nextRecIndex = 0;
  set->recordBufSize = 0;
  set->resRecs = ( RESENTRY *) NULL;
  return(0);
}
/* **********************************************************************
// FUNCTION NAME: int DBIntfDeleteResultSet( DBRESULTSET *set )
// PURPOSE: 
//
// AUTHOR: George Monroe
//
// INPUTS: 
//         
//         
// RETURNS:
//        
// **********************************************************************/
int DBIntfDeleteResultSet( DBRESULTSET *set )
{
  if(set->resRecs)
    free(set->resRecs);
  set->numOfRecords = 0;
  set->nextRecIndex = 0;
  set->recordBufSize = 0;
  set->resRecs = ( RESENTRY *) NULL;
  return(0);
}
