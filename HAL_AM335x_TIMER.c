//=====================================================================
//
//	FileName:	HAL_AM335x_TIMER.c
//
//	Description: Hardware abstraction layer implementation for AM335x TIMER utilities
//=====================================================================
#include "commontypes.h"
#include "stdio.h"
#include "stddef.h"
#include "hal.h"
#include "am335x_pbdefs.h"
#include "bsp/drivers/cpu/am335x/cpu_tgt.h"

/*************************************************************************
*
* FUNCTION
*
*       HALDelayTimerTicks
*
* DESCRIPTION
*
*       Wait at a minimum for the specified number of ticks on OS timer.
*
* INPUTS
*
*       no_of_ticks                          No of ticks to wait.
*
* OUTPUTS
*
*       None
*
*************************************************************************/
void  HALDelayTimerTicks (UINT32 no_of_ticks)
{
	UINT32            timer_ticks;
	UINT32            start_time;
	UINT32            end_time;

	// to get minimum ticks elapsed, add one to tick
	timer_ticks = no_of_ticks + 1;
    /* Retrieve Start Time value */
    NU_Retrieve_Hardware_Clock(start_time);

    do
    {
        /* Retrieve Clock value */
        NU_Retrieve_Hardware_Clock(end_time);

    } while (ESAL_PR_CALC_TIME(start_time,end_time)  < timer_ticks);

}

/*************************************************************************
*
* FUNCTION
*
*       HALStartClock
*
* DESCRIPTION
*
*       Generates a clock using specified timer at a frequency that does
*       not exceed maximum.  Timer must be one of the 4 brought out to a
*       pad - 4, 5, 6, 7.  Frequency must be between 1 and 25 MHz.
*
* INPUTS
*
*       timer                          which timer to use
*       max_freq                       max frequency of clock in MHz
*
* OUTPUTS
*
*       None
*
*************************************************************************/
void  HALStartClock (UINT32 timer, UINT32 max_freq)
{
	unsigned byteOffsetPRC; // peripheral control
	unsigned byteOffsetTCS; // Timer clock select
	unsigned byteOffsetCMC; // Clock module control
	unsigned timerLoadValue = 0;

	// validate inputs
	if ((timer < 4) || (timer > 7)) return;
	if ((max_freq < 1) || (max_freq > 25)) return;

    // set up pad
	switch(timer)
	{
		case 4:
			byteOffsetPRC = AM335X_CTRL_PADCONF_GPMC_ADVN_ALE;
			byteOffsetTCS = AM335X_TMR_CM_DPLL_CLKSEL_TIMER4_OFFSET;
			byteOffsetCMC = AM335X_CM_PER_TIMER4_CLKCTRL;
			break;
		case 5:
			byteOffsetPRC = AM335X_CTRL_PADCONF_GPMC_BEN0_CLE;
			byteOffsetTCS = AM335X_TMR_CM_DPLL_CLKSEL_TIMER5_OFFSET;
			byteOffsetCMC = AM335X_CM_PER_TIMER5_CLKCTRL;
			break;
		case 6:
			byteOffsetPRC = AM335X_CTRL_PADCONF_GPMC_WEN;
			byteOffsetTCS = AM335X_TMR_CM_DPLL_CLKSEL_TIMER6_OFFSET;
			byteOffsetCMC = AM335X_CM_PER_TIMER6_CLKCTRL;
			break;
		case 7:
			byteOffsetPRC = AM335X_CTRL_PADCONF_GPMC_OEN_REN;
			byteOffsetTCS = AM335X_TMR_CM_DPLL_CLKSEL_TIMER7_OFFSET;
			byteOffsetCMC = AM335X_CM_PER_TIMER7_CLKCTRL;
			break;
	}
    ESAL_GE_MEM_WRITE32(ESAL_PR_CONTROL_BASE + byteOffsetPRC,
    		AM335X_CTRL_PADCONF_PULL_DIS | AM335X_CTRL_PADCONF_INPUT_DIS | AM335X_CTRL_PADCONF_MODE2 );

	// set up timer
	// select clock input
    ESAL_GE_MEM_WRITE32(AM335X_CM_PER_BASE + byteOffsetTCS, 0x1);
	// enable timer
    ESAL_GE_MEM_WRITE32(AM335X_CM_PER_BASE + byteOffsetCMC, AM335X_CM_CLKCTRL_MODULEMODEEN);

	// - determine timer load value based on frequency
	timerLoadValue = AM335X_TIMER_OVERFLOW_VALUE - (((unsigned) AM335X_TIMER_MAX_FREQ_MHZ/max_freq)/2); // divide by 2 because timer overflow only gives half a cycle
    ESAL_GE_MEM_WRITE32(ESAL_PR_TMR_DMTIMER4_BASE_ADDR + ((timer - 4) * 0x2000) + ESAL_PR_TMR_TLDR, timerLoadValue);

	// use PWM in overflow mode
    ESAL_GE_MEM_WRITE32(ESAL_PR_TMR_DMTIMER4_BASE_ADDR + ((timer - 4) * 0x2000) + ESAL_PR_TMR_TCLR,
    		AM335X_TIMER_TCLR_TOGGLE_MODE |
    		AM335X_TIMER_TCLR_TRIGGER_ON_OVERFLOW |
    		AM335X_TIMER_TCLR_AUTO_RELOAD |
    		AM335X_TIMER_TCLR_START_TIMER
    		);

}

/*************************************************************************
*
* FUNCTION
*
*       HALStopClock
*
* DESCRIPTION
*
*       Stops clock on specified timer.
*
* INPUTS
*
*       timer                          which timer to use
*
* OUTPUTS
*
*       None
*
*************************************************************************/
void  HALStopClock (UINT32 timer)
{
	// validate inputs
	if ((timer < 4) || (timer > 7)) return;

    ESAL_GE_MEM_WRITE32(ESAL_PR_TMR_DMTIMER4_BASE_ADDR + ((timer - 4) * 0x2000) + ESAL_PR_TMR_TCLR,
    		0
    		);

}

#ifdef CFG_NU_BSP_BEAGLEBONE_BLACK_ENABLE
/*************************************************************************
*
* FUNCTION
*
*       HALSetWatchdogTimer
*
* DESCRIPTION
*
*       Enables or disables watchdog timer.
*
* INPUTS
*
*       onOff                          Enable or disable
*
* OUTPUTS
*
*       None
*
*************************************************************************/
void HALSetWatchdogTimer(BOOL onOff)
{
	volatile unsigned * pStartStopReg = (volatile unsigned *) (AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WSPR_OFFSET);
	volatile unsigned * pWritePostingReg = (volatile unsigned *) (AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WWPS_OFFSET);

	if (onOff == TRUE)
	{// enable sequence
		*pStartStopReg = 0x0000BBBB;
		while ((*pWritePostingReg & 0x00000010) != 0); // wait for write to actually occur
		*pStartStopReg = 0x00004444;
		while ((*pWritePostingReg & 0x00000010) != 0); // wait for write to actually occur

	}
	else
	{//disable sequence
		*pStartStopReg = 0x0000AAAA;
		while ((*pWritePostingReg & 0x00000010) != 0); // wait for write to actually occur
		*pStartStopReg = 0x00005555;
		while ((*pWritePostingReg & 0x00000010) != 0); // wait for write to actually occur

	}

}
#endif
