/*
* 05-Apr-07 sa002pe on FPHX 01.04 shelton branch
*	Added definition of THISFILE.
*	Changed all calls for exception logging to use THISFILE instead of __FILE__.
*/
#ifndef URL_ENCODE_C
#define URL_ENCODE_C

#ifdef __cplusplus
extern  "C" {                              
#endif
#include "..\include\exception.h"
#include "..\include\urlencode.h"
#include "kernel/nu_kernel.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#ifdef __cplusplus
}                              
#endif

#define THISFILE "urlencode.c"

/*******************************************************************************
 ** urlEncode()
 *
 *  FILENAME: urlencode.c
 *
 *  PARAMETERS:
 *
 *		destPtr - Pointer to the destination buffer.
 *		sourcePtr - Pointer to the source buffer.
 *
 *  DESCRIPTION:
 *
 *		URL encodes the source text. 
 *
 *  RETURNS:
 *
 *		The number of characters written to the destination.
 *
 *	NOTES:	put in exception stuff if STRICT_ARG_CHECKING!!!
 *
 ******************************************************************************/
int urlEncode(unsigned char *destPtr, const unsigned char *sourcePtr)
{
int count = 0;
		
	while(*sourcePtr)
	{
		if(isalnum(*sourcePtr))
		{
			*destPtr++ = *sourcePtr++;
			count++;
		}

		else if(*sourcePtr	== ' ')
		{
			*destPtr++ = '+';
			sourcePtr++;
			count++;
		}
	
		else
		{
			sprintf((char *)destPtr, "%%%02X", *sourcePtr++);
			destPtr += 3;
			count += 3;
		}
	}
	
	return count;

}

int urlDecode(unsigned char *destPtr, unsigned char *sourcePtr)
{
int count = 0;
unsigned char theChar;

	while(*sourcePtr)
	{
		switch(*sourcePtr)
		{
			case '%':
			{
				theChar = urlDecodeChar(++sourcePtr) * 16;  
				theChar += urlDecodeChar(++sourcePtr);
				count += sprintf((char *)destPtr + count, "%c", theChar);
				sourcePtr++;
				break;
			}
			
			case '+':
			{
				*(destPtr + count) = ' ';
				sourcePtr++;
				count++;
				break;
			}
		
			default:
			{
				*(destPtr + count) = *sourcePtr++;
				count++;
				break;
			}
		
		}
	
	}

	return count;
}

unsigned char urlDecodeChar(unsigned char *inCharPtr)
{
	if((*inCharPtr >= '0') && (*inCharPtr <= '9')) return(*inCharPtr - '0');
	if((*inCharPtr >= 'a') && (*inCharPtr <= 'f')) return(*inCharPtr - 'a' + 10);
	if((*inCharPtr >= 'A') && (*inCharPtr <= 'F')) return(*inCharPtr - 'A' + 10);
	return 0;
}



char *findString(char *stringToFind, char *lineToSearch)
{
	int length;

	length = strlen(stringToFind);

	if(length == 1)
	{
		while (*lineToSearch)
		{
			if(*stringToFind == *lineToSearch) 
				return(lineToSearch);
			lineToSearch++;
		}
		return(0);
	}

	while(*lineToSearch)
	{
		if(!strncmp(stringToFind,lineToSearch,length)) 
			return(lineToSearch);
		
		lineToSearch++;
	}
	return(0);
}


//Break a fully qualified URL into its constituents
STATUS crackUrl(CRACKED_URL *crackedUrl, const char *urlToCrack)
{
char localScratchPad[256];
char *charPtr;
char *charPtr2;

    memset(localScratchPad, '\0', sizeof(localScratchPad));
    memset(crackedUrl, '\0', sizeof(CRACKED_URL));

    //Validate length
    if((!strlen(urlToCrack)) || (strlen(urlToCrack) >= sizeof(localScratchPad))) 
        return -1;

    else
        strcpy(localScratchPad, urlToCrack);

    //Convert to uniform case...
	charPtr = localScratchPad;
	while(*charPtr)
	{
		*charPtr = tolower(*charPtr);
		charPtr++;
	}

    //There's no guarantee that the http:// or ftp:// portion will be there, but we need to check
	if((charPtr = findString("http://", localScratchPad)) != NULL)
	{
	    memcpy(crackedUrl->protocol, charPtr, strlen("http://"));	
		charPtr += strlen("http://");
	}
	
	else if((charPtr = findString("https://", localScratchPad)) != NULL)
	{
	    memcpy(crackedUrl->protocol, charPtr, strlen("https://"));	
		charPtr += strlen("https://");
	}
	else if((charPtr = findString("ftp://", localScratchPad)) != NULL)
	{
	    memcpy(crackedUrl->protocol, charPtr, strlen("ftp://"));	
		charPtr += strlen("ftp://");
	}

	//No protocol specified
	else
		charPtr = localScratchPad;

	//Now charPtr will be pointing to the host name
	charPtr2 = crackedUrl->host;
	while((*charPtr != '/') && (*charPtr != ':') && (*charPtr != '\0'))
	{
		*charPtr2++ = *charPtr++;
	}

    if(!*charPtr)   //invalid URL (No Path)
        return -1;

	//Ok, now charPtr will be pointing to either the beginning of the path or the colon preceeding the port #
	if(*charPtr == ':')
	{
		charPtr2 = crackedUrl->port;
		charPtr++;
		while((*charPtr != '/') && (*charPtr != '\0'))
		{
			*charPtr2++ = *charPtr++;
		}
	}

    if(!*charPtr)   //invalid URL (No Path)
        return -1;

    charPtr2 = crackedUrl->path;
	while(*charPtr)
	{
		*charPtr2++ = *charPtr++;
	}

    return NU_SUCCESS;
}


//This "snippet" tests to see if a host name is an IP Address (dotted quad) or a host name
BOOL isIpAddress(char *host)
{
    if(!host)
    	taskException("NULL Pointer", THISFILE, __LINE__);
					
	while(*host)
	{
		if(!isdigit(*host) && (*host != '.'))	return 0; 
		host++;
	}
	
	return 1;
}

// **************************************************************************
// FUNCTION NAME:  isDottedQuad
//
// DESCRIPTION: 
//      Check if the IP Address is dotted quad (XXX.XXX.XXX.XXX). 
//
// INPUTS:
//      pIPAddr - IP address string. 
//
// RETURNS:
//      TRUE or FALSE
//
// WARNINGS/NOTES:
//      A valid IP Address string --
//        1. Length > 7 (X.X.X.X)
//        2. consist of digits and '.'
//        3. The format must be XXX.XXX.XXX.XXX
//           the number of X between 2 dots is 1,2 or 3
//        4. Each field number(XXX) < 256  
// *************************************************************************
BOOL isDottedQuad(const char *pIPAddr) 
{ 
    int section = 0; 
    int dot = 0; 
    int num = 0;

    while(*pIPAddr)
    {
        //current character is dot and there should be a digit before "."
        if(*pIPAddr == '.' && num > 0)
        { 
            dot++; 

            //the dots is never greater than 3 in a valid IP address
            if(dot > 3)
                return FALSE; 

            //clear for next field
            section = 0;
            num = 0;
        }
        else if(*pIPAddr >= '0' && *pIPAddr <= '9')
        {
            //append the new digit to the current byte
            section = section * 10 + *pIPAddr - '0';
            num++;

            //check the digit number of every field and the byte that is never greater than 255 
            if(num > 3 || section > 255)
            { 
                return FALSE; 
            } 
        }
        else
        { 
            return FALSE; 
        }
        
        pIPAddr++;        
    }

    //a valid IP address has 3 dots and the digit number in last field is greater than 0
    if(dot != 3 || num == 0)
    {
        return FALSE;
    }
    
    return TRUE; 
}


#endif

