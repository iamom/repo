/**********************************************************************
 PROJECT    :   Future Phoenix
 COPYRIGHT  :   2005, Pitney Bowes, Inc.  
 AUTHOR     :   
 MODULE     :   OiErrhnd.c

 DESCRIPTION:
    This file contains all types of screen interface functions (hard key, 
    event, pre, post, field init and field refresh) and global variables 
    pertaining to meter error screens.  Errors which are specifically handled 
    with their own screen (e.g. jam, bad refill combination) do not
                    appear in this file.

* ----------------------------------------------------------------------
    MODIFICATION HISTORY:
	
 20-Apr-15 sa002pe on FPHX 02.12 shelton branch
 	Added ECOIT_LOCAL_MIDNIGHT_MISSED, ECOIT_DCAP_MIDNIGHT_MISSED & ECOIT_LOCAL_AND_DCAP_MIDNIGHT_MISSED
	to pErrorStringMap[].

 11-Mar-14 sa002pe on FPHX 02.12 shelton branch
	Merging in changes from Janus tips:
----*
	* 11-Feb-14 sa002pe on Janus tips shelton branch
	*	Added IG_FAILED to pErrorStringMap[] as an error that needs a soft reboot because
	*	you can't clear it by pressing the soft key.
----*

 04-Mar-13 sa002pe on FPHX 02.10 shelton branch
 	Added BAD_PBP_BACKUP_URL, BAD_DLA_BACKUP_URL, BAD_SERVICE_REQUEST & BAD_CUST_ACCOUNT_NUM
	to pErrorStringMap[].
	Changed INVALID_URL, INVALID__PCN, INVALID_SN to be clearable by a soft reboot since
	they aren't hardware errors and G900 doesn't have the problem w/ connecting to the USB Host
	devices that Janus Refresh has. (For Janus Refresh, all the DExx errors require a hard
	reboot because some of the USB Host devices don't properly connect after a soft reboot.)

 18-Feb-13 sa002pe on FPHX 02.10 shelton branch
 	Added a bunch of 2Exx & DExx errors to pErrorStringMap[].  Rearranged some things
	in the table so all the errors of a particular error class are together.
	Added ARCHIVE_FILE_ERR to pErrorStringMap[].
	
 	For Fraca 219326:
	1. Merged in change from Janus:
		* 26-Aug-10 sa002pe on Janus tips shelton branch
		*	In order to avoid the possibility of having the error recovery method downgraded
		*	(e.g., from "must cycle power" to "press to clear".):
		*	1. Changed the init of bRecoveryMethod from 0 to RM_NO_ERROR.
		*	2. Changed fnReportMeterError to not downgrade the recovery method.
	2. Changed pErrorStringMap[] to add ECOIT_NO_MODE_CHANGE_FROM_SYS, PSOC_PH_SEED_FAILURE &
		PSOC_CM_SEED_FAILURE.

 15-Oct-12 sa002pe on FPHX 02.10 shelton branch
	Changed ucOIErrorClass & ucOIErrorCode so they can be set externally.

 2010.08.11 Clarisa Bellamy - FPHX 02.07
  - Added entry in pErrorStringMap for 2546 error, which requires a reboot (per Sandra).

 2010.08.04 Clarisa Bellamy 
  - Add entries to pErrorStringMap table for 254D, 2565 and 2566 errors so that 
    the unit recovers, just like the 254C error (missing auto inscription). 
    These are the AutoInscription Name Too Long, Missing AutoAd and AutoAd Name
    Too Long errors.
  - Update comments a little.

 2010.07.16 Clarisa Bellamy (& Raymond Shen) 
  Fix for GMSE 00191990 - DCAP / AR Discrepancy:
  - Modified fnfPaperErrorsCaption() to add a case for the new ERR_PAPER_SKIPPED, 
    which is set in WOW mode when we detect a new envelope before the current one is 
    debited.

  02/25/2010  Raymond Shen      Added fnpPreSystemRebootNeeded(), fnfSystemRebootTimeoutClockValue(), fnevSystemRebootTimeoutClockTick(), 
                                and fnevRebootUIC() for screen SystemRebootNeeded.
  01/12/2009  Raymond Shen      Modified fnpPreErrInsufficientFunds() to fix GMSE00181058 (G950/DM475- Meter reports 37B6 error
                                after insufficient funds warning cleared.)
  11/18/2009  Raymond Shen      Modified fnReportMeterError() to fix GMSE00175569(G970/DM475 System fails to wake from sleep
                                after being left on over weekend.).
  07/24/2009  Raymond Shen      Modified fnfWOWErrorsTitle() to add support for MCP's error "Mailpiece Too Long".
  07/17/2009  Raymond Shen      Added fnIsWOWNeedCalibrationError(), fnfWOWErrorSKIndicator(), fnfWOWErrorsTitle(), fnfWOWErrorsCaption(),
                                and modified fnfFeederErrorsCaption(), fnfWOWSBRErrorsCaption(), fnfClearTransportCaption()
                                for WOW/SBR error handling.
  07/08/2009  Raymond Shen      Modified fnpPrepPaperError() and added fnevPaperCleared() to let "CANCEL" key 
                                on "Mail Jammed" screen navigate to Home screen with disabling condition.
  05/20/2009  Raymond Shen      Added fnfWOWSBRErrorsCaption() and fnfClearTransportCaption() for 
                                wow error handling.
  05/18/2009  Raymond Shen      Modified fnevJamLeverErrCleared() and fnevTopCoverClosed() for WOW.
  05/11/2009  Fred Xu           Modified function fnfMeterErrorHex to fix Fraca GMSE00161088
  03/31/2009    Jingwei,Li      Modified function fnfGraphicErrorHex() and fnfDiagError() 
                                for no error/warning case.
  11/20/2007  Oscar Wang        Modified functions to handle GTS interruption cases.
  11/12/2007  Raymond Shen      Added fnpPreErrInsufficientFunds() to fix 
                                a GTS issue with Insufficient Funds.
  11/06/2007  Oscar Wang        Modified function fnfTapeErrorSKIndicator()to fix FRACA GMSE00131947: 
                                for GTS mail, if debit is done and paper error happens, we can't resume
                                printing even for tape since barcode is already used.
  10/25/2007  Oscar Wang        Modified function fnfPaperErrorSKIndicator(), fnhkPaperErrorSK3(),
                                and fnhkPaperErrorSK4() to fix FRACA GMSE00131947: for GTS mail, 
                                if debit is done and paper error happens, we can't resume printing 
                                even for DM400 since barcode is already used.
  09/20/2007  Oscar Wang        Modified function fnhkGTSStoreResumePrint: for DM300, don't 
                                clear fGTSStoreFinished flag.
  09/06/2007  Oscar Wang        Fixed FRACA GMSE00128739 per new requirement.
                                Added functions fnfGTSStoreIndicator, fnhkGTSStoreResumePrint
                                and fnhkGTSStoreCancelPrint
  08/31/2007  Vincent Yi        Renamed functions fnClearFlagDWPiecePrintedForDM400C()
                                to fnClearFlagDWPiecePrinted(), fnIsDWPiecePrintedForDM400C()
                                to fnIsDWPiecePrinted()
  07/25/2007    Vincent Yi      Merge from Janus 1306, changed the table 
                                pErrorStringMap, register the ERROR_CLASS_GTS_API class 
                                and GTS_FILE_SYSTEM_ERROR code as RM_POWER_CYCLE. 
  07/23/2007    Vincent Yi      Fixed lint errors
  07/05/2007      I. Le Goff   Add new function fnfFeederErrorSKIndicator()
  06/29/2007      I. Le Goff   Add new function fnfTapeErrorSKIndicator()
  29 Jun 2007     Simon Fox     Merged US and EAME changes for handling paper
                                errors.
  06/28/2007      I. Le Goff              In fnSetResumePrintingFlag, Do not allow resume printing from error
                                          when incident report is enabled.
  06/28/2007      I. Le Goff              In fnpPrepPaperError(), cancel previous modification due to side effects.
  06/27/2007      I. Le Goff              In fnpPrepPaperError(), fRedirect to MMAjoutNextPieceTransient for all errors
                                          in mode ajout.
                                          In fnSetResumePrintingFlag, Do not allow resume printing from error
                                          in mode ajout
    15 Jun 2007 Simon Fox   App, RM & DCE checksum failures now cause fatal errors.
    06/22/2007  Vincent Yi      Modified function fnfPaperErrorsCaption()
                                Added functions fnfPaperErrorSKIndicator()
                                fnhkPaperErrorSK3(), fnhkPaperErrorSK4()
    06/21/2007  Vincent Yi      Modified function fnevInvokeErrScreen() to fix fraca 121246    
    04/20/2007  Andy Mo     Modified in fnpErrRMManualPostageFile()by setting *pNextScreen=0
    04/19/2007  Andy Mo     Added function fnpErrRMManualPostageFile() for RateErrManualPostageFile.
  04/12/2007      I. Le Goff              In fnpPrepPaperError(), fix bug in case of out of tapes while doing
                                          mode ajout
*
* 06-Apr-07 sa002pe on FPHX 01.04 shelton branch
*   Changed the use of BP_KEY_IN_POSTAGE_MODE to clear the Dcap-related bit to get the
*   true KIP mode value.
*
  03/30/2007      I. Le Goff              In fnpPrepPaperError(), fix bug in case of paper jam while doing
                                          mode ajout
  03/30/2007      I. Le Goff              In fnpPrepPaperError(), redirect to mode ajout screens if we are
                                                franking mode ajout items
    09/13/2006  Vincent Yi      Modified fnCheckCMStatusInPrinting() to handle feeder errors
    09/08/2006  Vincent Yi      Added functions for Midnight Crossover screen
    09/04/2006  Vincent Yi      Modified function fnReportMeterError to transition
                                to ErrNetworkPowerOff screen when Data Center 
                                connection lost 
    08/22/2006  Vincent Yi      Added functions for ErrJamLeverOpen, ErrTopCoverOpen
                                screens
    08/16/2006  Vincent Yi      Added event function fnevCMErrorNotCleared
    07/24/2006  Vincent Yi      Modified function fnevInvokeErrScreen() for 
                                resuming print flow correctly
    07/06/2006  Vincent Yi      Added functions fnSetErrCode(), fnfDiagError(), 
                                fngDiagError() for Diagnostics feature
    
    05/10/2006  Steve Terebesi  Merged from rearchitecture branch
    03/17/2006  Steve Terebesi  Updated call to fnRequestNewMode
    03/24/2006  Vincent Yi      Added screens ErrMeterErrorPowerOff, ErrMissingImage 
                                and code clean up
                                    
-----------------------------------------------------------------------
    OLD PVCS REVISION HISTORY
 * \main\jnus11.00_i1_shelton\3 cx17598 19 oct 2005
 *
 *  Set a pending flag instead of going to the rate rule update required screen right away
 *  since the update required status can be reported during restoring normal preset on power up,
 *  we will re-check the status in the prefunction of main screen.
 *
*************************************************************************/


/**********************************************************************
        Include Header Files
**********************************************************************/

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "bob.h"
#include "global.h"
#include "cmerr.h"
#include "CMPM.H"
#include "DatDict.h"
#include "errcode.h"
#include "fwrapper.h"
#include "imageErrs.h"
#include "oicommon.h"
#include "oierrhnd.h"
//#include "oidiagnostic.h"
#include "oifields.h"
#include "oifpmain.h"
#include "oifpprint.h"
//#include "oirateservice.h"
#include "oiscreen.h"
#include "oiscrnfp.h"
#include "oit.h"
#include "oitcm.h"
#include "nucleus.h"
#include "ossetup.h"
#include "sys.h"
#include "fdrmcptask.h"
#include "ajout.h"
#include "oifunctfp.h"
#include "flashutil.h"
#include "utils.h"



/**********************************************************************
        Local Function Prototypes
**********************************************************************/
static void fnSetResumePrintingFlag (void);

//TODO - determine if static or not
T_SCREEN_ID fnCheckCMStatusInPrinting (void);

/**********************************************************************
        Local Defines, Typedefs, and Variables
**********************************************************************/
#define OI_ERR_LEVEL_MAX      7

//following is for saving the current screen ID before entrying the error screen
//and then we can return to it after errors have been handled.
static UINT8        bCurrErrLevel = 0;                  //current error level(deep of the return screen IDs stack)
static T_SCREEN_ID  pwScrIDToReturn[OI_ERR_LEVEL_MAX];  //the return screen IDs stack
static BOOL         fErrorStepIn = FALSE;
static BOOL         fErrorStepOut = FALSE;

static UINT8        bRecoveryMethod = RM_NO_ERROR;

unsigned char        ucOIErrorClass = 0;                 // bErrorClass
unsigned char        ucOIErrorCode = 0;                  // bErrorCode
static UINT8        ucErrorAlias = 0;                   // bErrorAlias
static void         (*pfnRecoveryFunct)(void) = NULL;   // pRecoveryFunct

static BOOL         fResumePrintingFromErr = FALSE;

static BOOL         fDisplayMissingGraphicError = TRUE; // show the errors until they are cleared

static SINT32    lSystemRebootNeededScrnTimeoutTicks = 0;
static BOOL fSystemRebootNeededScrnTimeoutExpired = FALSE;

/**********************************************************************
        Global Functions Prototypes
**********************************************************************/
extern void RebootSystem(void);



/**********************************************************************
        Global Variables
**********************************************************************/
extern BOOL bGoToReady;

/* This is what this table did a long time ago, in a galaxy far, far away, but NOT 
   what it does on this product:
    error string map:  this table maps a error class/code combination to a field text string number.  the
    field text string number tells us which string (e.g. the 0th, 1st, 2nd, 100th, etc.) in the list of
    strings associated with the error text field to use.  the order in which the strings appear in the
    screen tool must match. */

// NOW, this table is used by fnReportMeterError() to determine which error
//  screen to go to next, and how to recover.  (It doesn't even look a the string.)
//   Currently:
//      If the error class is EXCEPTION,                goto SCREEN_NETWORK_ERR_POWER_OFF screen
//      If the error recovery method is RM_PHONE_HOME,  goto SCREEN_IMAGE_GEN_ERROR screen, and call
//                                                      the function in this table entry when we exit 
//                                                      that screen.
//      If the error recovery method is RM_POWER_CYCLE, goto SCREEN_METER_ERROR_POWER_OFF screen
//      else                                            goto SCREEN_METER_ERROR
//      If the error type is FATAL, send OIT event message: OIT_NOTIFY_FATAL_ERROR 
//          which should end up on ?? screen.
const ERROR_CTRL_MAP pErrorStringMap[] =
    /*  class           code        string num  error type  recovery method     alias       error handler */
{
	//0810
	{ ERROR_CLASS_OIT,	ECOIT_NO_MODE_CHANGE_FROM_SYS,	0,	ERR_TYPE_NORMAL,    RM_POWER_CYCLE, 	0,	NULL },
	
	//0819, 081A, 081B
    { ERROR_CLASS_OIT,	ECOIT_LOCAL_MIDNIGHT_MISSED,			0,  ERR_TYPE_FATAL,     RM_CANNOT_CLEAR,     0,  NULL },
    { ERROR_CLASS_OIT,	ECOIT_DCAP_MIDNIGHT_MISSED,				0,  ERR_TYPE_FATAL,     RM_CANNOT_CLEAR,     0,  NULL },
    { ERROR_CLASS_OIT,	ECOIT_LOCAL_AND_DCAP_MIDNIGHT_MISSED,	0,  ERR_TYPE_FATAL,     RM_CANNOT_CLEAR,     0,  NULL },

	// 081C
	{ ERROR_CLASS_OIT,	ECOIT_NO_RESPONSE_CM,	0,	ERR_TYPE_NORMAL,    RM_CANNOT_CLEAR, 	0,	NULL },

	//253C, 2544, 2546
	{ ERROR_CLASS_JBOB_TASK,  	JSCM_IPSD_COM_FAILURE,			0,	ERR_TYPE_NORMAL,    RM_POWER_CYCLE, 	0,	NULL },
	{ ERROR_CLASS_JBOB_TASK,  	JSCM_IPSD_COM_TIMEOUT_FAILURE,	0,	ERR_TYPE_NORMAL,    RM_POWER_CYCLE, 	0,	NULL },
    { ERROR_CLASS_JBOB_TASK,    JSCM_IPSD_COM_OS_TIMEOUT,       0,  ERR_TYPE_NORMAL,    RM_POWER_CYCLE,     0,  NULL },

    //254C, 254D, 2565, 2566
//    { ERROR_CLASS_JBOB_TASK,    JSCM_AUTO_INSCRIPTION_NOT_FOUND,    0, ERR_TYPE_NORMAL,    RM_PHONE_HOME,  0,  fnOIClearRatingInfo },
//    { ERROR_CLASS_JBOB_TASK,    JSCM_AUTO_INSCRIPTION_OUT_OF_RANGE, 0, ERR_TYPE_NORMAL,    RM_PHONE_HOME,  0,  fnOIClearRatingInfo },
    //{ ERROR_CLASS_JBOB_TASK,    JSCM_AUTO_AD_NOT_FOUND,             0, ERR_TYPE_NORMAL,    RM_PHONE_HOME,  0,  fnOIClearRatingInfo },
//    { ERROR_CLASS_JBOB_TASK,    JSCM_AUTO_AD_OUT_OF_RANGE,          0, ERR_TYPE_NORMAL,    RM_PHONE_HOME,  0,  fnOIClearRatingInfo },

    // 2212 //2213 //2214 //2215 //2216 //2220
    { ERROR_CLASS_IG_BC,        (UINT8)IG_MISSING_COMPONENT,    0,  ERR_TYPE_NORMAL,    RM_PHONE_HOME,  0,  NULL },
    { ERROR_CLASS_IG_BC,        (UINT8)IG_MISSING_FONT,         0,  ERR_TYPE_NORMAL,    RM_PHONE_HOME,  0,  NULL },
    { ERROR_CLASS_IG_BC,        (UINT8)IG_INVALID_COORDINATES,  0,  ERR_TYPE_NORMAL,    RM_PHONE_HOME,  0,  NULL },
    { ERROR_CLASS_IG_BC,        (UINT8)IG_INVALID_SIZE,         0,  ERR_TYPE_NORMAL,    RM_PHONE_HOME,  0,  NULL },
    { ERROR_CLASS_IG_BC,        (UINT8)IG_INVALID_IMAGE,        0,  ERR_TYPE_NORMAL,    RM_PHONE_HOME,  0,  NULL },
	{ ERROR_CLASS_IG_BC,		(IG_FAILED & 0xFF),				0,	ERR_TYPE_NORMAL,    RM_CANNOT_CLEAR, 0,	NULL },

    // 23xx
    { ERROR_CLASS_PSOC,     PSOC_VER_MISMATCH,      0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_PSOC,     PSOC_PROG_FATAL,        0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_PSOC,     PSOC_COMM_FATAL,        0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_PSOC,     PSOC_COMM_TIMEOUT,      0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_PSOC,     PSOC_SEED_MISMATCH,     0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_PSOC,     PSOC_PH_SEED_FAILURE,   0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_PSOC,     PSOC_CM_SEED_FAILURE,   0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },

    //DExx
    { ERROR_CLASS_EXCEPTION,    GENERAL_TASK_EXCEPTION, 0,  ERR_TYPE_NORMAL,    RM_CANNOT_CLEAR,    0,  NULL },
    { ERROR_CLASS_EXCEPTION,    GFX_VALIDATE_ERROR,     0,  ERR_TYPE_NORMAL,    RM_CANNOT_CLEAR,    0,  NULL },
    { ERROR_CLASS_EXCEPTION,    GFX_INSTALL_ERROR,      0,  ERR_TYPE_NORMAL,    RM_CANNOT_CLEAR,    0,  NULL },
    { ERROR_CLASS_EXCEPTION,    XML_ATTR_NUM_ERR,       0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    XML_ATTR_NAME_ERR,      0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    XML_STRUC_ERR,          0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    XML_REPLY_ERR,          0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    XML_PARSE_ERR,          0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    MEMORY_ERR,             0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    NULL_ARGUMENT,          0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    ADD_DEVICE_ERR,         0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    NULL_PTR_ERR,           0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    INVALID_PTR,            0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    IT_MSG_ERR,             0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    INVALID_MSG,            0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    INVALID_URL,            0,  ERR_TYPE_NORMAL,    RM_CANNOT_CLEAR,    0,  NULL },
    { ERROR_CLASS_EXCEPTION,    NO_RATE_MGR,            0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    SOCKET_CREATE_ERR,      0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    SOCKET_CLOSE_ERR,       0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    DNS_HOST_ERR,           0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    SRV_CONN_ERR,           0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    DATA_TX_ERR,            0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    DATA_RX_ERR,            0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    INVALID_UPDATE_TYPE,    0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    UNDEFINED_UPDATE_TYPE,  0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    FTPC_OPEN_ERR,          0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    FTPC_LOGIN_ERR,         0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    FTPC_CHDIR_ERR,         0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    FTPC_TYPE_ERR,          0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    FTPC_CLOSE_ERR,         0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    FTPC_TIME_OUT,          0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    FAL_REGISTER_ERR,       0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    FILE_OPEN_ERR,          0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    FILE_READ_ERR,          0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    FILE_WRITE_ERR,         0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    FILE_DEL_ERR,           0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    FILE_REN_ERR,           0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    FILE_CLOSE_ERR,         0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    FILE_ALLOCATE_ERR,      0,  ERR_TYPE_FATAL,     RM_POWER_CYCLE,     0,  NULL },
    { ERROR_CLASS_EXCEPTION,    INVALID__PCN,           0,  ERR_TYPE_NORMAL,    RM_CANNOT_CLEAR,    0,  NULL },
    { ERROR_CLASS_EXCEPTION,    INVALID_SN,             0,  ERR_TYPE_NORMAL,    RM_CANNOT_CLEAR,    0,  NULL },
	{ ERROR_CLASS_EXCEPTION,	LOST_CARRIER,			0,	ERR_TYPE_FATAL,	    RM_POWER_CYCLE, 	0,	NULL },
	{ ERROR_CLASS_EXCEPTION,	PC_HOST_INTERNET_DISCON_ERR,0,	ERR_TYPE_FATAL,	RM_POWER_CYCLE, 	0,	NULL },
	{ ERROR_CLASS_EXCEPTION,	USB_DISCONNECT_ERR,		0,	ERR_TYPE_FATAL,		RM_POWER_CYCLE, 	0,	NULL },
	{ ERROR_CLASS_EXCEPTION,	COMM_SEND_PROXY_UNAVAIL_ERR,0,	ERR_TYPE_FATAL,	RM_POWER_CYCLE, 	0,	NULL },
	{ ERROR_CLASS_EXCEPTION,	DLA_FILE_NAME_TOO_BIG,	0,	ERR_TYPE_FATAL,		RM_POWER_CYCLE, 	0,	NULL },
	{ ERROR_CLASS_EXCEPTION,	DOWNLOADED_FILE_WRONG_SIZE,0,	ERR_TYPE_FATAL,	RM_POWER_CYCLE, 	0,	NULL },
	{ ERROR_CLASS_EXCEPTION,	GFX_DOWNLOAD_ERROR,     0,	ERR_TYPE_FATAL,		RM_POWER_CYCLE, 	0,	NULL },
	{ ERROR_CLASS_EXCEPTION,	BAD_PBP_BACKUP_URL,     0,	ERR_TYPE_NORMAL,    RM_CANNOT_CLEAR, 	0,	NULL },
	{ ERROR_CLASS_EXCEPTION,	BAD_DLA_BACKUP_URL,     0,	ERR_TYPE_NORMAL,    RM_CANNOT_CLEAR, 	0,	NULL },
	{ ERROR_CLASS_EXCEPTION,	BAD_SERVICE_REQUEST,    0,	ERR_TYPE_NORMAL,    RM_CANNOT_CLEAR, 	0,	NULL },
	{ ERROR_CLASS_EXCEPTION,	BAD_CUST_ACCOUNT_NUM,   0,	ERR_TYPE_NORMAL,    RM_CANNOT_CLEAR, 	0,	NULL },
	{ ERROR_CLASS_EXCEPTION,	ARCHIVE_FILE_ERR,       0,	ERR_TYPE_FATAL,		RM_POWER_CYCLE, 	0,	NULL },

    { 0,    0,  0,  0,  0, 0,   NULL    },  // end of table marker, keep this as last entry
};
 




/**********************************************************************
        Public Functions
**********************************************************************/
/* Utility functions */

BOOL fnGetDisplayMissingGraphicError()
{
    return fDisplayMissingGraphicError;
}
void fnSetDisplayMissingGraphicError(const BOOL fValue)
{
    fDisplayMissingGraphicError = fValue;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnReportMeterError
//
// DESCRIPTION: 
//      This is a public function available to all tasks in the
//          system which does the following:
//          1. Saves an error class number and an error code number in
//              global variables.
//          2. Requests transition to the error handling state from
//              the system controller.  This is done by sending a
//              mode change request message for the mode that is
//              associated with the meter error screen.
//
//          The result of these operations is that the system controller
//          will be informed of the error state and will change the
//          OIT to the error mode.  This will cause a meter error screen
//          to be displayed with an appropriate code number and description,
//          and the type of recovery mechanism will determine what keys
//          are active and what the system's response will be.
//
// INPUTS:
//      bSetErrorClass  - error class
//      bSetErrorCode   - error code
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12-Dec-12   S. Peterson     Change to not allow lower priority errors to supercede
//                              higher priority errors.
//  11/18/2009 Raymond Shen Modify this function to fix GMSE00175569(G970/DM475 System
//                          fails to wake from sleep after being left on over weekend.).
//  12/06/2005  Vincent Yi      Code cleanup
//              Joe Mozdzer     Initial version
//
// *************************************************************************/
void fnReportMeterError(UINT8   bSetErrorClass, 
                        UINT8   bSetErrorCode)
{
    void    (*fnFunctToCall)(void);
    UINT32  i;
    UINT16  wErrScreenID;
//    UINT8   bCurrentMode;
//    UINT8   bCurrentSubMode;
	unsigned char	bNewRecoveryMethod;
    UINT8   bErrorType;
    UINT8   bSetErrorAlias = 0;
    UINT8   bMsg[2];


    /* set the default recovery method to simple clearing.  this means that 
        we just go to the main screen without doing anything special. */
    bRecoveryMethod = RM_SIMPLE_CLEARING;

    /* set the default error type to normal.  this means that the error is 
        not fatal and is not preserved between power cycles. */
    bErrorType = ERR_TYPE_NORMAL;

    /* set the pointer of function to call to NULL */
    fnFunctToCall = NULL;

	// search the error string map table for an entry with the matching class & code
    // to see if we need to do something different from the default processing.
    i = 0;
    while(pErrorStringMap[i].bClass)
    {
        if ((pErrorStringMap[i].bClass == bSetErrorClass) && (pErrorStringMap[i].bCode == bSetErrorCode))
        {
            /* if we find a match, save the recovery method and error type and stop searching */
            bNewRecoveryMethod = pErrorStringMap[i].bRecoveryMethod;
            bErrorType = pErrorStringMap[i].bErrorType;
            bSetErrorAlias = pErrorStringMap[i].bCompatibilityAlias;
            fnFunctToCall = pErrorStringMap[i].pFunct;

            break;
        }
		
        i++;
    }

    /* log the error */
    fnLogError(bErrorType, bSetErrorClass, bSetErrorCode); 

	// don't let the recovery method get downgraded
	switch (bRecoveryMethod)
	{
		case RM_NO_ERROR:
			// first error the meter has had since powerup, so set to the proposed recovery method
			bRecoveryMethod = bNewRecoveryMethod;
			break;

			// equally lowest levels of recovery
		case RM_SIMPLE_CLEARING:
			// mostly an informational error. The error just needs to be acknowledged before we move on.
		case RM_PHONE_HOME:
			// need to call Tier3 and/or DLA

			// if we're proposing to set to a level that isn't this level, it must be a higher level or
			// the other lowest level, so set to the proposed recovery method
			if (bNewRecoveryMethod != bRecoveryMethod)
			{
				bRecoveryMethod = bNewRecoveryMethod;
			}
			break;

		case RM_CANNOT_CLEAR:
			// middle level of recovery
			// meter needs a soft reboot
			// if we're want to set to a higher lever, set to the proposed recovery method
			if (bNewRecoveryMethod == RM_POWER_CYCLE)
			{
				bRecoveryMethod = bNewRecoveryMethod;
			}
			break;

		case RM_POWER_CYCLE:
			// highest level of recovery
			// customer has to set physically reset the meter
			// we're already set to the highest level, so we don't want to change
			break;

		default:
			// unknown recovery method, so set to the proposed recovery method
			bRecoveryMethod = bNewRecoveryMethod;
			break;
	}

	// pick the proper screen
    if( bSetErrorClass == ERROR_CLASS_EXCEPTION )
    {
        wErrScreenID = GetScreen(SCREEN_NETWORK_ERR_POWER_OFF);
    }
    else if(bRecoveryMethod == RM_PHONE_HOME)
    {
        wErrScreenID = GetScreen(SCREEN_IMAGE_GEN_ERROR);
    }
    else if(bRecoveryMethod == RM_POWER_CYCLE)
    {
        wErrScreenID = GetScreen(SCREEN_METER_ERROR_POWER_OFF);
    }
    else
    {
        wErrScreenID = GetScreen(SCREEN_METER_ERROR);
    }

	// if we actually switched the recovery method, update the recovery function
	// and save the new error codes in globals for use by display functions
	if (bRecoveryMethod == bNewRecoveryMethod)
	{
		ucOIErrorClass = bSetErrorClass;
		ucOIErrorCode = bSetErrorCode;
		ucErrorAlias = bSetErrorAlias;
		pfnRecoveryFunct = fnFunctToCall;
	}

    if (bErrorType == ERR_TYPE_FATAL)
    {
        bMsg[0] = OIT_NOTIFY_FATAL_ERROR;
        bMsg[1] = 0;
        (void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, bMsg, sizeof(bMsg));
    }

    // Change to the error mode.
    fnRequestNewMode(OIT_ERROR_METER, wErrScreenID, FALSE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnProcessSCMError
//
// DESCRIPTION: 
//      Processes an error returned from the bob task.  This involves retrieving
//      the error class and code from bob's module and then calling 
//      fnReportMeterError.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  10/26/2005  Vincent Yi      Code cleanup
//              Joe Mozdzer     Initial version
//
// *************************************************************************/
void fnProcessSCMError (void)
{
    UINT8       bClass;
    UINT8       bCode;

    if (fnBobErrorInfo(&bClass, &bCode) != SUCCESSFUL)
    {
        bClass = ERROR_CLASS_BOB_VAULT;
        bCode = 0;
    }

    fnReportMeterError(bClass, bCode);

    return;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnSetErrReturnScreen
//
// DESCRIPTION: 
//      Utility function that set return screen ID before the entrance of 
//      the error screen.
//
// INPUTS:
//      wNext   - the return screen id
//
// OUTPUTS:
//      None
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  04/21/2006  Vincent Yi      Code cleanup
//              Henry Liu       Initial version
//
// *************************************************************************/
void fnSetErrReturnScreen(T_SCREEN_ID wNext)
{
    if (bCurrErrLevel < OI_ERR_LEVEL_MAX)
    {
        if (bCurrErrLevel > 0)
        {   // Step in a new error screen from current error screen
            fErrorStepIn = TRUE;
        }
        else
        {
            fErrorStepIn = FALSE;
        }
        pwScrIDToReturn[bCurrErrLevel++] = wNext;
    }
    else
    {
        fErrorStepIn = FALSE;
        fnReportMeterError(ERROR_CLASS_OIT, ECOIT_ERR_LEVEL_EXCEEDED);
    }   
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnRetFromErrScreen
//
// DESCRIPTION: 
//      Utility function that return the previous screen ID which is before the
//      entrance of the error screen.
//
// INPUTS:
//      bNumNext    -   number of next screens
//      pNextScreens -  pointer to a group of next screens.
//
// RETURNS:
//      Next screens
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/22/2006  Vincent Yi      Code cleanup
//              Henry Liu       Initial version
//
// *************************************************************************/
T_SCREEN_ID fnRetFromErrScreen(UINT8            bNumNext, 
                               T_SCREEN_ID *    pNextScreens)
{
    T_SCREEN_ID     usNext = 0;

    if(fnIsProcessingErrors())
    {
        usNext = pwScrIDToReturn[--bCurrErrLevel];
        fErrorStepOut = TRUE;
    }
/*Vincent
    else if(fnIsInOOBMode())
    {
        usNext = fnhkNextScrOrExitOOB(0, bNumNext, pNextScreens);
    }
*/
    else if (bNumNext > 0)                           
    {
        usNext = 0; //GetScreen(SCREEN_MM_INDICIA_ERROR);
    }
    else
    {
        usNext = 0; //GetScreen(SCREEN_MM_INDICIA_ERROR);        //MMIndiciaReady screen
    }

    return(usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsProcessingErrors
//
// DESCRIPTION: 
//      Utility function that return whether OI is processing errors.
//
// INPUTS:
//      None
//
// OUTPUTS:
//      None
//
// RETURNS:
//      TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  04/21/2006  Vincent Yi      Code cleanup
//              Henry Liu       Initial version
//
// *************************************************************************/
BOOL fnIsProcessingErrors(void)
{
    return(bCurrErrLevel > 0);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnErrClearReturnScreens
//
// DESCRIPTION: 
//      Utility function that clear the OI error return screens stack.
//
// INPUTS:
//      None
//
// OUTPUTS:
//      None
//
// RETURNS:
//      TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/10/2006  Vincent Yi      Code cleanup
//              Henry Liu       Initial version
//
// *************************************************************************/
void fnErrClearReturnScreens(void)
{
    bCurrErrLevel = 0;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnSetErrCode
//
// DESCRIPTION: 
//      Utility function that stores error code of latest error
//
// INPUTS:
//      ucErrClass  - error class
//      ucErrCode   - error code
//
// OUTPUTS:
//      None
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  06/30/2006  Vincent Yi      Initial version
//
// *************************************************************************/
void fnSetErrCode (UINT8    ucErrClass,
                   UINT8    ucErrCode)
{
    ucOIErrorClass = ucErrClass;
    ucOIErrorCode = ucErrCode;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckCMStatusInPrinting 
//
// DESCRIPTION: 
//      Check the status of CM and transition to error screen accordingly
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      error screen ids
//
// MODIFICATION HISTORY:
//  09/08/2006  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnCheckCMStatusInPrinting (void)
{
    T_SCREEN_ID usNext = 0;
    
    if (fnIsCmFatalError() == TRUE) 
    {   
        usNext = GetScreen (SCREEN_PRINTER_FAULT);
    }
    else if (fnIsCmTopCoverOpen() == TRUE) 
    {   
        usNext = GetScreen (SCREEN_TOP_COVER_OPEN);
    }
    else if (fnIsCmInkTankLidOpen() == TRUE) 
    {   
        usNext = GetScreen (SCREEN_INKTANK_LID_OPEN);
    }                                               
    else if (fnIsCmPaperError() == TRUE) 
    {   
        usNext = GetScreen (SCREEN_PAPER_ERROR);
        if (fnGetLatestCMStatus()->ucPaperStatus == ERR_JAM_LEVER_OPEN)
        {
            usNext = GetScreen (SCREEN_JAM_LEVEL_OPEN);
        }
        else if (fnGetLatestCMStatus()->ucPaperStatus == ERR_JAM)
        {
            UINT8   ucLastPrintEvent = fnGetResumingPrintEvent();
            
            if ((ucLastPrintEvent == OIT_PRINT_TAPE)        ||
                (ucLastPrintEvent == OIT_PRINT_TAPE_PERMIT) || 
                (ucLastPrintEvent == OIT_PRINT_TAPE_TEST)   || 
                (ucLastPrintEvent == OIT_PRINT_RPT_TO_TAPE))
            {
                usNext = GetScreen (SCREEN_TAPE_ERRORS);
            }
        }
        else if (fnGetLatestCMStatus()->ucPaperStatus == 0x85)  // hard code temporarily
        {
            usNext = GetScreen (SCREEN_TAPE_ERRORS);
        }
    }
    else if (fnIsCmTapeError() == TRUE) 
    {   
        usNext = GetScreen (SCREEN_TAPE_ERRORS);
    }
    else if (fnIsCmFeederFatalFault() == TRUE) 
    {   
        usNext = GetScreen (SCREEN_FEEDER_FAULT);
    }
    else if (fnIsCmFeederError() == TRUE) 
    {   
        usNext = GetScreen (SCREEN_FEEDER_ERRORS);
    }
    else if (fnIsCmInkOut()) 
    {
        usNext = GetScreen (SCREEN_INK_OUT);
    }
    else if (fnIsCmInkTankError() == TRUE) 
    {
        if (fnIsCmNoInkTank() == TRUE)
        {
            usNext = GetScreen (SCREEN_NO_INKTANK);
        }
        else
        {   
            usNext = GetScreen (SCREEN_INKTANK_ERROR);
        }
    }
    if (fnIsCmPrintheadError() == TRUE) 
    {
        if (fnIsCmNoPrinthead() == TRUE)
        {
            usNext = GetScreen (SCREEN_NO_PRINTHEAD);
        }
        else
        {   
            usNext = GetScreen (SCREEN_PRINTHEAD_ERROR);
        }
    }
    if (fnIsCmWasteTankFull() == TRUE) 
    {   
        usNext = GetScreen (SCREEN_WASTETANK_FULL);
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsWOWNeedCalibrationError 
//
// DESCRIPTION: 
//      Check whether the current error is "WOW needs Calibration before weighing"
//
// INPUTS:
//      None
//
// RETURNS:
//      TRUE or False
//
// MODIFICATION HISTORY:
//  07/17/2009  Raymond Shen    Initial version
//
// *************************************************************************/
BOOL fnIsWOWNeedCalibrationError(void)
{
    BOOL    fResult = FALSE;
    
    if(ucOIErrorCode == MCP_WOW_CAL_WO_INIT)
    {
        fResult = TRUE;
    }

    return fResult;
}


/* Pre/post functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepFeederError
//
// DESCRIPTION: 
//      Pre function that is called to prepare the feeder error class and 
//              error code before displaying them.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Victor Li       Initial version
//  2/1/2006    Vincent Yi      Code cleanup 
//
// *************************************************************************/
SINT8 fnpPrepFeederError( void      (** pContinuationFcn)(), 
                         UINT32         *   pMsgsAwaited,
                         T_SCREEN_ID *  pNextScr )
{
    ucOIErrorClass = ERROR_CLASS_FP_FEEDER;
    ucOIErrorCode = fnFeederGetLastMeterErrorCode();

    *pNextScr = 0;

    fnSetResumePrintFlag (FALSE);
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepPaperError
//
// DESCRIPTION: 
//      Pre function that is called to prepare the paper error class and 
//              error code before displaying them.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Victor Li       Initial version
//  2/1/2006    Vincent Yi      Code cleanup 
//      03/30/2007      I. Le Goff              Redirect to mode ajout screens 
//                                              if we are in mode ajout
//      04/10/2007      I. Le Goff              Fix bug in case of paper jam 
//                                              in mode ajout
//      04/12/2007      I. Le Goff              Fix bug in case of out of tapes
//                                              in mode ajout
//      06/27/2007      I. Le Goff              Redirect to MMAjoutNextPieceTransient for all errors
//                                              in mode ajout.
//      06/28/2007      I. Le Goff              Cancel previous modifications due to side effects.
//
// *************************************************************************/
SINT8 fnpPrepPaperError( void       (** pContinuationFcn)(), 
                         UINT32         *   pMsgsAwaited,
                         T_SCREEN_ID *  pNextScr )
{
		UINT32 pMsgData[2];
        ucOIErrorClass = ERROR_CLASS_JPM;
        ucOIErrorCode = fnGetCmPaperErrorCode();
      
        *pNextScr = 0;
          
        fnSetResumePrintFlag (FALSE);   
        fnSetPaperErrorShownStatus(TRUE);

        if(ucOIErrorCode == ERR_NO_PAPER || ucOIErrorCode == ERR_NO_PRINT){

            // error type, not equal to the last status
            pMsgData[0] = (UINT32) ERTYPE_PAPER_ERR;
            // sometime sensor status update will cause jam lever open, not only
            // paper error does, so we use ucOIErrorCode that is prepared in the
            // pre function of this screen instead of bLastPaperStatus;
            pMsgData[1] = (UINT32) ucOIErrorCode;

            (void)OSSendIntertask(CM, OIT, OC_CLR_ERR_REQ, LONG_DATA, pMsgData,
                                    sizeof(pMsgData));

            if(ucOIErrorCode == ERR_NO_PAPER)
            {
                //clear No Paper error and return to home screen
                *pNextScr = GetScreen (SCREEN_MM_INDICIA_READY);
            }
            else
            {
                bGoToReady = FALSE;
            }
        }
/*
        // error type, not equal to the last status
        pMsgData[0] = (UINT32) ERTYPE_PAPER_ERR;
        // sometime sensor status update will cause jam lever open, not only
        // paper error does, so we use ucOIErrorCode that is prepared in the
        // pre function of this screen instead of bLastPaperStatus;
        pMsgData[1] = (UINT32) ucOIErrorCode;

        (void)OSSendIntertask(CM, OIT, OC_CLR_ERR_REQ, LONG_DATA, pMsgData,
                                sizeof(pMsgData));
*/


    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepInktankError
//
// DESCRIPTION: 
//      Pre function that is called to prepare the ink tank error class and 
//              error code before displaying them.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Victor Li       Initial version
//  2/2/2006    Vincent Yi      Code cleanup 
//
// *************************************************************************/
SINT8 fnpPrepInkTankError( void         (** pContinuationFcn)(), 
                           UINT32       *   pMsgsAwaited,
                           T_SCREEN_ID *    pNextScr )
{
    ucOIErrorClass = ERROR_CLASS_JPM;
    ucOIErrorCode = fnGetCmInktankErrorCode();

    fnInkTankErrShown (TRUE);
    *pNextScr = 0;

    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepInkOutError
//
// DESCRIPTION: 
//      
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/17/2006  Vincent Yi      Initial version
//
// *************************************************************************/
SINT8 fnpPrepInkOutError( void      (** pContinuationFcn)(), 
                          UINT32        *   pMsgsAwaited,
                          T_SCREEN_ID * pNextScr )
{
    fnInkOutErrorShown(TRUE);
    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepNoInkTankError
//
// DESCRIPTION: 
//      
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/17/2006  Vincent Yi      Initial version
//
// *************************************************************************/
SINT8 fnpPrepNoInkTankError( void       (** pContinuationFcn)(), 
                             UINT32         *   pMsgsAwaited,
                             T_SCREEN_ID *  pNextScr )
{
    fnNoInkTankErrorShown(TRUE);
    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepNoPrintHeadError
//
// DESCRIPTION: 
//      
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/17/2006  Vincent Yi      Initial version
//
// *************************************************************************/
SINT8 fnpPrepNoPrintHeadError( void         (** pContinuationFcn)(), 
                             UINT32         *   pMsgsAwaited,
                             T_SCREEN_ID *  pNextScr )
{
    fnNoPrintHeadErrorShown(TRUE);
    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepPrinterFault
//
// DESCRIPTION: 
//      Pre function that is called to prepare the fatal error class and 
//              error code before displaying them.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Victor Li       Initial version
//  2/2/2006    Vincent Yi      Code cleanup 
//
// *************************************************************************/
SINT8 fnpPrepPrinterFault( void         (** pContinuationFcn)(), 
                           UINT32       *   pMsgsAwaited,
                           T_SCREEN_ID *    pNextScr )
{
    ucOIErrorClass = ERROR_CLASS_JPM;
    ucOIErrorCode = fnGetCmFatalErrorCode();

    fnPrinterFaultShown (TRUE);
    *pNextScr = 0;

    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepPheadError
//
// DESCRIPTION: 
//      Pre function that is called to prepare the printhead error class and 
//              error code before displaying them.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Victor Li       Initial version
//  2/2/2006    Vincent Yi      Code cleanup 
//
// *************************************************************************/
SINT8 fnpPrepPheadError( void       (** pContinuationFcn)(), 
                         UINT32         *   pMsgsAwaited,
                         T_SCREEN_ID *  pNextScr )
{
    ucOIErrorClass = ERROR_CLASS_JPM;
    ucOIErrorCode = fnGetCmPheadErrorCode();

    fnPrintHeadErrShown (TRUE);
    *pNextScr = 0;

    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepWasteTankFull
//
// DESCRIPTION: 
//      
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/17/2006  Vincent Yi      Initial version
//
// *************************************************************************/
SINT8 fnpPrepWasteTankFull( void        (** pContinuationFcn)(), 
                            UINT32      *   pMsgsAwaited,
                            T_SCREEN_ID *   pNextScr )
{
    fnWasteTankFullShown(TRUE);
    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepWasteTankNearFull
//
// DESCRIPTION: 
//      
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/17/2006  Vincent Yi      Initial version
//
// *************************************************************************/
SINT8 fnpPrepWasteTankNearFull( void        (** pContinuationFcn)(), 
                                UINT32      *   pMsgsAwaited,
                                T_SCREEN_ID *   pNextScr )
{
    fnWasteTankNearFullShown(TRUE);
    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepInkTankExpiring
//
// DESCRIPTION: 
//      
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/17/2006  Vincent Yi      Initial version
//
// *************************************************************************/
SINT8 fnpPrepInkTankExpiring( void      (** pContinuationFcn)(), 
                              UINT32        *   pMsgsAwaited,
                              T_SCREEN_ID * pNextScr )
{
    fnInkTankExpiringShown(TRUE);
    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepJamLeverOpen
//
// DESCRIPTION: 
//      
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/22/2006  Vincent Yi      Initial version
//
// *************************************************************************/
SINT8 fnpPrepJamLeverOpen( void         (** pContinuationFcn)(), 
                           UINT32       *   pMsgsAwaited,
                           T_SCREEN_ID  *   pNextScr )
{

    fnSetResumePrintingFlag();
        
    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepTopCoverOpen
//
// DESCRIPTION: 
//      
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/22/2006  Vincent Yi      Initial version
//
// *************************************************************************/
SINT8 fnpPrepTopCoverOpen( void         (** pContinuationFcn)(), 
                           UINT32       *   pMsgsAwaited,
                           T_SCREEN_ID  *   pNextScr )
{
    fnSetResumePrintingFlag();
    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepMidnightAdvDate
//
// DESCRIPTION: 
//      
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  09/05/2006  Vincent Yi      Initial version
//
// *************************************************************************/
SINT8 fnpPrepMidnightAdvDate( void      (** pContinuationFcn)(), 
                              UINT32        *   pMsgsAwaited,
                              T_SCREEN_ID   *   pNextScr )
{
    fnSetResumePrintingFlag();
        
    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostMissingImage
//
// DESCRIPTION: 
//      
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  04/21/2006  Vincent Yi      Initial version 
//
// *************************************************************************/
SINT8 fnpPostMissingImage (void     (** pContinuationFcn)(), 
                           UINT32      *   pMsgsAwaited)
{
    /* call to the recovery function if there one */
    if (pfnRecoveryFunct != NULL)
    {
        pfnRecoveryFunct();
    }

    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostGeneralErrorScreen
//
// DESCRIPTION: 
//      General post function for printing error screens.
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/10/2006  Vincent Yi      Initial version 
//
// *************************************************************************/
SINT8 fnpPostGeneralErrorScreen (void     (** pContinuationFcn)(), 
                                 UINT32      *   pMsgsAwaited)
{
    if (fErrorStepIn == TRUE)
    {
        fErrorStepIn = FALSE;
    }
    else
    {
        if (fErrorStepOut == TRUE)
        {
            fErrorStepOut = FALSE;
        }
        else
        {
            if (bCurrErrLevel > 0)
            {
                bCurrErrLevel--;    
            }
        }
    }

    return (PREPOST_COMPLETE);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnpPreErrInsufficientFunds
//
// DESCRIPTION: 
//      
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  01/12/2010  Raymond Shen    Remove my original code to fix
//  11/12/2007  Raymond Shen    Initial version
//
// *************************************************************************/
SINT8 fnpPreErrInsufficientFunds( void      (** pContinuationFcn)(), 
                              UINT32        *   pMsgsAwaited,
                              T_SCREEN_ID   *   pNextScr )
{

    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPreSystemRebootNeeded
//
// DESCRIPTION: 
//      Pre Function that starts system reboot timer when need to auto-restart.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY
//  02/25/2010  Raymond Shen    Initial version
//
// *************************************************************************/
SINT8 fnpPreSystemRebootNeeded( void        (** pContinuationFcn)(), 
                           UINT32        *  pMsgsAwaited,
                           T_SCREEN_ID  *   pNextScr )
{
    // Initialize the timer.
    lSystemRebootNeededScrnTimeoutTicks = SYSTEM_REBOOT_TIMEOUT_INTERVAL_COMPLETE;

    // Reset the expire flag.
    fSystemRebootNeededScrnTimeoutExpired = FALSE;
    
    *pNextScr = 0;

    return (PREPOST_COMPLETE);
}


/* Field functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fnfMeterErrorHex
//
// DESCRIPTION: 
//      Output field function that displays the 4-digit meter error number in 
//      hexadecimal.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      Always successful
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Joe Mozdzer     Initial version
// 05/11/2009   Fred Xu         Fix Fraca GMSE00161088
// 10/28/2005   Vincent Yi      Code cleanup
//
// *************************************************************************/
BOOL fnfMeterErrorHex (UINT16  *pFieldDest, 
                       UINT8    bLen,   
                       UINT8    bFieldCtrl, 
                       UINT8    bNumTableItems,
                       UINT8   *pTableTextIDs,
                       UINT8   *pAttributes)
{
    char       pErrorNumAscii[8];

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);
    if((ucOIErrorClass != 0) || (ucOIErrorCode != 0))
    {
        /* note that the class and code are treated as individual numbers... we do 
            not merge the two together before making the translation to hex */
        sprintf(pErrorNumAscii, "%.2X", ucOIErrorClass);
        sprintf(pErrorNumAscii+2, "%.2X", ucOIErrorCode);

        fnAscii2Unicode(pErrorNumAscii, pFieldDest, METER_ERROR_CODE_LEN);

        /* if the field is right-aligned, pad left of string with spaces */
        if (bFieldCtrl & ALIGN_MASK)
        {
            pFieldDest[METER_ERROR_CODE_LEN] = (UINT16) NULL;
            fnUnicodePadSpace(pFieldDest, METER_ERROR_CODE_LEN, bLen);
        }

        pFieldDest[bLen] = (UINT16) NULL;
    }
    else
    {
        //If no error, don't show anything.
        *pFieldDest = NULL_VAL;
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfPaperErrorsCaption
//
// DESCRIPTION: 
//      Output field function that displays prompt message for the printing 
//      error screen.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      Always successful
//
// WARNINGS/NOTES:
//      Menu possibilities:
//      1. Paper Error          
//      2. No Paper
//      3. Mail Skewed          Check last piece.
//      4. Mail Too Short       Clear printer transport.
//      5. Mail Jammed          Clear printer transport.
//      6. Paper Skipped
//      7. Jam Lever open
//
// MODIFICATION HISTORY:
//  07/06/2010  Clarisa Bellamy (& Raymond) - Add case for ERR_PAPER_SKIPPED, 
//                      same as WRN_PIECE_SKIPPED.
//  06/22/2007  Vincent Yi              Added Jam lever open
//  01/28/2006  Vincent Yi              Code cleanup
//              Henry Liu, Victor Li    Initial version
//
// *************************************************************************/
BOOL fnfPaperErrorsCaption (UINT16  *pFieldDest, 
                            UINT8   bLen,   
                            UINT8   bFieldCtrl, 
                            UINT8   bNumTableItems,
                            UINT8   *pTableTextIDs,
                            UINT8   *pAttributes)
{
    UINT8   bWhichString = 0xff;
    
    switch (ucOIErrorCode)
    {
    case ERR_NO_PAPER:
        bWhichString = 1; // No paper
        break;

    case ERR_PAPER_SKEW:
        bWhichString = 2; // Mail skewed
        break;

    case ERR_PIECE_TOO_SHORT:
        bWhichString = 3; // Mail too short
        break;

    case ERR_JAM:
        bWhichString = 4; // Mail jammed
        break;

    case ERR_PAPER_SKIPPED:
    case WRN_PIECE_SKIPPED:
        bWhichString = 5; // Paper skipped
        break;

    case ERR_JAM_LEVER_OPEN:
        bWhichString = 6; // Jam Level open
        break;

    default:
        bWhichString = 0; // Paper error
        break;
    }

    if (bWhichString < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                pTableTextIDs, bWhichString);
    }
    else
    {
        SET_SPACE_UNICODE(pFieldDest, bLen);
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfPaperErrorSKIndicator
//
// DESCRIPTION: 
//      Output field function to display softkey indicator in Paper error screen
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  10/25/2007  Oscar Wang      Fix FRACA GMSE00131947: for GTS mail, if debit 
//                              is done and paper error happens, we can't resume 
//                              printing even for DM400 since barcode is already 
//                              used.
//  29 Jun 2007 Simon Fox       Merged French and US versions.
//  06/21/2007  Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnfPaperErrorSKIndicator (UINT16      *pFieldDest, 
                               UINT8        bLen, 
                               UINT8        bFieldCtrl,
                               UINT8        bNumTableItems, 
                               UINT8       *pTableTextIDs,
                               UINT8       *pAttributes)
{
    UINT8   bWhichString = 0xff;
    
    if (fnFlashGetByteParm(BP_DCAP_INDICIA_PWR_FAIL_REPORT) == TRUE ||
        fnOITGetBaseType() == OIT_BASE_300C ||
        (fnOITGetBaseType() == OIT_BASE_400C && ucOIErrorCode == ERR_JAM_LEVER_OPEN))
    {
        bWhichString = 0;
    }
    else
    {
        bWhichString = 1;
    }
    
    if (bWhichString < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                pTableTextIDs, bWhichString);
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfTapeErrorsCaption
//
// DESCRIPTION: 
//      Output field function to display prompt message for tape errors screen.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  04/21/2006  Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnfTapeErrorsCaption (UINT16      *pFieldDest, 
                           UINT8        bLen, 
                           UINT8        bFieldCtrl,
                           UINT8        bNumTableItems, 
                           UINT8       *pTableTextIDs,
                           UINT8       *pAttributes)
{
    UINT8   ucFieldIndex = bNumTableItems;

    SET_SPACE_UNICODE(pFieldDest, bLen);

    switch (ucOIErrorCode)
    {
    case ERR_NO_TAPE:
        ucFieldIndex = 1; // No tape
        break;

    case ERR_JAM:   // treated paper jam as a tape error
    case ERR_TAPE_FEED:
        ucFieldIndex = 2; // Mail jammed
        break;

    default:
        ucFieldIndex = 0; // Paper error
        break;
    }

    if (ucFieldIndex < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                            pTableTextIDs, ucFieldIndex);
    }

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnfTapeErrorSKIndicator
//
// DESCRIPTION: 
//      Output field function to display softkey indicator in Paper error screen
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  11/06/2007  Oscar Wang    Fix FRACA GMSE00131947: for GTS mail, if debit 
//                            is done and paper error happens, we can't resume 
//                            printing even for tape since barcode is already 
//                            used.
//  06/29/2007  I. Le Goff    Initial version
//
// *************************************************************************/
BOOL fnfTapeErrorSKIndicator (UINT16      *pFieldDest, 
                               UINT8        bLen, 
                               UINT8        bFieldCtrl,
                               UINT8        bNumTableItems, 
                               UINT8       *pTableTextIDs,
                               UINT8       *pAttributes)
{
  UINT8 bWhichString = 0xff;
    
    if ( fnFlashGetByteParm(BP_DCAP_INDICIA_PWR_FAIL_REPORT) == TRUE )
    {
        bWhichString = 0;
    }
    else
    {
        bWhichString = 1;
    }
    
  if (bWhichString < bNumTableItems)
  {
    fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                pTableTextIDs, bWhichString);
  }

  return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfFeederErrorsCaption
//
// DESCRIPTION: 
//      Output field function to display prompt message for feeder errors screen.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/12/2006  Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnfFeederErrorsCaption (UINT16     *pFieldDest, 
                             UINT8      bLen, 
                             UINT8      bFieldCtrl,
                             UINT8      bNumTableItems, 
                             UINT8      *pTableTextIDs,
                             UINT8      *pAttributes)
{
    UINT8   ucFieldIndex = bNumTableItems;

    SET_SPACE_UNICODE(pFieldDest, bLen);

    switch (ucOIErrorCode)
    {
    case ERR_FDR_JAM:
    case MCP_TAR1_EXIT_JAM:
    case MCP_TAR2_EXIT_JAM:
        ucFieldIndex = 1; // Feeder jammed
        break;

    default:
        ucFieldIndex = 0; // Feeder error
        break;
    }

    if (ucFieldIndex < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                            pTableTextIDs, ucFieldIndex);
    }

    return ((BOOL) SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfFeederErrorSKIndicator
//
// DESCRIPTION: 
//      Output field function to display softkey indicator in a feeder error screen
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/05/2007  I. Le Goff    Initial version
//
// *************************************************************************/
BOOL fnfFeederErrorSKIndicator (UINT16      *pFieldDest, 
                               UINT8        bLen, 
                               UINT8        bFieldCtrl,
                               UINT8        bNumTableItems, 
                               UINT8       *pTableTextIDs,
                               UINT8       *pAttributes)
{
  UINT8 bWhichString = 0xff;
    
    if ( fnFlashGetByteParm(BP_DCAP_INDICIA_PWR_FAIL_REPORT) == TRUE )
    {
        bWhichString = 0;
    }
    else
    {
        bWhichString = 1;
    }
    
  if (bWhichString < bNumTableItems)
  {
    fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                pTableTextIDs, bWhichString);
  }

  return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfWOWErrorSKIndicator
//
// DESCRIPTION: 
//      Output field function to display softkey indicator in a WOW error screen
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/17/2009  Raymond Shen    Initial version
//
// *************************************************************************/
BOOL fnfWOWErrorSKIndicator (UINT16      *pFieldDest, 
                               UINT8        bLen, 
                               UINT8        bFieldCtrl,
                               UINT8        bNumTableItems, 
                               UINT8       *pTableTextIDs,
                               UINT8       *pAttributes)
{
  UINT8 bWhichString = 0xff;
    
    if ( fnFlashGetByteParm(BP_DCAP_INDICIA_PWR_FAIL_REPORT) == TRUE
        || (fnIsWOWNeedCalibrationError() == TRUE) )
    {
        bWhichString = 0;
    }
    else
    {
        bWhichString = 1;
    }
    
  if (bWhichString < bNumTableItems)
  {
    fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                pTableTextIDs, bWhichString);
  }

  return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfGraphicErrorHex
//
// DESCRIPTION: 
//      Output field function that displays the 6-digit missing graphic error
//      number in hexadecimal.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      Always successful
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Victor Li       Initial version
// 04/24/2005   Vincent Yi      Code cleanup
// 03/31/2009  Jingwei,Li        Don't display background color when no warning/error.
//
// *************************************************************************/
BOOL fnfGraphicErrorHex (UINT16  *pFieldDest, 
                         UINT8  bLen,   
                         UINT8  bFieldCtrl, 
                         UINT8  bNumTableItems,
                         UINT8   *pTableTextIDs,
                         UINT8   *pAttributes)
{
    char       pErrorNumAscii[10];
    UINT8   *   pMissingGraphList = fnGetMissingGraphicList();

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);
   if((pMissingGraphList[0] != 0) || (pMissingGraphList[1] != 0)||(pMissingGraphList[2] !=0))
   {
        /* note that the class and code are treated as individual numbers... 
            we do not merge the two together before making the translation to hex */
        sprintf(pErrorNumAscii, "%.2X %.2X %.2X", pMissingGraphList[0], 
                pMissingGraphList[1], pMissingGraphList[2]);

        fnAscii2Unicode(pErrorNumAscii, pFieldDest, GRAPHIC_ERROR_CODE_LEN);

        /* if the field is right-aligned, pad left of string with spaces */
        if (bFieldCtrl & ALIGN_MASK)
        {
            pFieldDest[GRAPHIC_ERROR_CODE_LEN] = (unsigned short) NULL;
            fnUnicodePadSpace(pFieldDest, GRAPHIC_ERROR_CODE_LEN, bLen);
        }

        pFieldDest[bLen] = (UINT16) NULL;
    }
    else
    {
        //If no error, don't show anything.
        *pFieldDest = NULL_VAL;
    }
    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfDiagError
//
// DESCRIPTION: 
//      Output field function for displaying error code if diagnostic operation 
//      failed
//
// PRE-CONDITION: 
//      ucOIErrorClass and ucOIErrorCode have been updated if diagnostic operation 
//      failed
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  03/31/2009  Jingwei,Li        Don't display background color when no warning/error.
//  06/30/2006  Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnfDiagError (UINT16     *pFieldDest, 
                   UINT8        bLen, 
                   UINT8        bFieldCtrl,
                   UINT8        bNumTableItems, 
                   UINT8      *pTableTextIDs,
                   UINT8      *pAttributes)
{
    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

/*
    if (fnGetDiagSuccessFlag() == FALSE)
    {   // Error code only displayed when diagnostic failed
        (void)fnfMeterErrorHex (pFieldDest, bLen, bFieldCtrl, 
                                bNumTableItems, pTableTextIDs,
                                pAttributes);   
    }
    else
    {
        *pFieldDest = NULL_VAL;
    }
*/

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfJamLeverOpenIndication
//
// DESCRIPTION: 
//      Output field function for indication for user when jam lever open prompt
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/22/2006  Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnfJamLeverOpenIndication (UINT16     *pFieldDest, 
                                UINT8       bLen, 
                                UINT8       bFieldCtrl,
                                UINT8       bNumTableItems, 
                                UINT8      *pTableTextIDs,
                                UINT8      *pAttributes)
{
    UINT8   ucFieldIndex = bNumTableItems;

    if (fResumePrintingFromErr == TRUE)
    {
        ucFieldIndex = 1;   // Continue printing after jam lever closed
    }
    else
    {
        ucFieldIndex = 0;   // Clear message after jam lever closed
    }

    if (ucFieldIndex < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                            pTableTextIDs, ucFieldIndex);
    }
    return ((BOOL) SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: fnfGTSStoreIndicator
//
// DESCRIPTION:
//              Output field function that display in Store GTS record to 
//              Flash screen..
//
// INPUTS:
//          Standard field function inputs.
//
// RETURNS:
//          always return successful
//
// WARNINGS/NOTES:  
//
// MODIFICATION HISTORY:
// 09/05/2007  Oscar Wang    Initial version  
// *************************************************************************/
BOOL fnfGTSStoreIndicator (UINT16 *pFieldDest, 
                             UINT8   bLen, 
                             UINT8   bFieldCtrl,
                             UINT8   bNumTableItems, 
                             UINT8  *pTableTextIDs,
                             UINT8  *pAttributes)
{
    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfWOWSBRErrorsCaption
//
// DESCRIPTION: 
//      Output field function to display prompt message for WOW SBR errors screen.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/20/2009  Raymond Shen        Initial version
//
// *************************************************************************/
BOOL fnfWOWSBRErrorsCaption (UINT16     *pFieldDest, 
                             UINT8      bLen, 
                             UINT8      bFieldCtrl,
                             UINT8      bNumTableItems, 
                             UINT8      *pTableTextIDs,
                             UINT8      *pAttributes)
{
    UINT8   ucFieldIndex = bNumTableItems - 1;
    UINT8   ucSBRStatus; //= fnRateGetMailpieceDimStatus();

    SET_SPACE_UNICODE(pFieldDest, bLen);

/*
    if(ucSBRStatus > MAILPIECE_OKAY)
    {
        ucFieldIndex = ucSBRStatus - MAILPIECE_OKAY -1;
    }
*/

    if (ucFieldIndex < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                            pTableTextIDs, ucFieldIndex);
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfWOWErrorsTitle
//
// DESCRIPTION: 
//      Output field function to display the title of WOW errors screen.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/24/2009  Raymond Shen        Added support to "Mailpiece Too Long" error.
//  07/16/2009  Raymond Shen        Initial version
//
// *************************************************************************/
BOOL fnfWOWErrorsTitle (UINT16     *pFieldDest, 
                             UINT8      bLen, 
                             UINT8      bFieldCtrl,
                             UINT8      bNumTableItems, 
                             UINT8      *pTableTextIDs,
                             UINT8      *pAttributes)
{
    UINT8   ucFieldIndex = 0; // WOW Error

    SET_SPACE_UNICODE(pFieldDest, bLen);

    switch (ucOIErrorCode)
    {
    case MCP_WOW_MID_JAM:
    case MCP_WOW_EXIT_JAM:
        ucFieldIndex = 1; // Mail Jammed
        break;

    case MCP_WOW_MAIL_ILLEGAL:
        ucFieldIndex = 2; // Mailpiece Too Heavy
        break;

    case MCP_WOW_CAL_WO_INIT:
        ucFieldIndex = 3; // WOW Calibration Error
        break;
        
    case MCP_WOW_EXCESSIVE_ZERO:
    case MCP_WOW_EXCESSIVE_ZERO_LO:
        ucFieldIndex = 4; // WOW Re-zero Error
        break;

    case MCP_WOW_PIECE_TOO_LONG:
        ucFieldIndex = 5; // Mailpiece Too Long
        break;

    default:
        ucFieldIndex = 0; // WOW Error
        break;
    }

    if (ucFieldIndex < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                            pTableTextIDs, ucFieldIndex);
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfWOWErrorsCaption
//
// DESCRIPTION: 
//      Output field function to display prompt message for WOW errors screen.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/17/2009  Raymond Shen        Initial version
//
// *************************************************************************/
BOOL fnfWOWErrorsCaption (UINT16     *pFieldDest, 
                             UINT8      bLen, 
                             UINT8      bFieldCtrl,
                             UINT8      bNumTableItems, 
                             UINT8      *pTableTextIDs,
                             UINT8      *pAttributes)
{
    UINT8   ucFieldIndex = 0; // Blank

    SET_SPACE_UNICODE(pFieldDest, bLen);

    if(ucOIErrorCode == MCP_WOW_CAL_WO_INIT)
    {
        ucFieldIndex = 4;   // Call for Service
    }
    else if(fnIsCmPaperInTransport() == TRUE)
    {
        ucFieldIndex = 1; // Clear Printer Transport
    }
    else if(fnIsPaperInWOWTransport() == TRUE)
    {
        ucFieldIndex = 2; // Clear WOW Transport
    }
    else if(fnIsPaperInFeederTransport() == TRUE)
    {
        ucFieldIndex = 3; // Clear Feeder Transport
    }

    if (ucFieldIndex < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                            pTableTextIDs, ucFieldIndex);
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfClearTransportCaption
//
// DESCRIPTION: 
//      Output field function to display prompt message for user to clear transport.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/20/2009  Raymond Shen        Initial version
//
// *************************************************************************/
BOOL fnfClearTransportCaption (UINT16     *pFieldDest, 
                             UINT8      bLen, 
                             UINT8      bFieldCtrl,
                             UINT8      bNumTableItems, 
                             UINT8      *pTableTextIDs,
                             UINT8      *pAttributes)
{
    UINT8   ucFieldIndex = 0;

    if(fnIsCmPaperInTransport() == TRUE)
    {
        ucFieldIndex = 1;
    }
    else if(fnIsPaperInWOWTransport() == TRUE)
    {
        ucFieldIndex = 2;
    }
    else if(fnIsPaperInFeederTransport() == TRUE)
    {
        ucFieldIndex = 3;
    }
    
    if (ucFieldIndex < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                            pTableTextIDs, ucFieldIndex);
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfSystemRebootTimeoutClockValue
//
// DESCRIPTION: 
//      This field function displays the number of seconds left in the 
//      SystemRebootNeeded Screen Timeout Clock.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY:
//  02/25/2010  Raymond Shen    Initial version
//
// *************************************************************************/
BOOL fnfSystemRebootTimeoutClockValue (UINT16     * pFieldDest, 
                                UINT8           bLen, 
                                UINT8           bFieldCtrl,
                                UINT8           bNumTableItems, 
                                UINT8      *    pTableTextIDs,
                                UINT8      *    pAttributes)
{
    UINT8   bTimeLen;

    SET_SPACE_UNICODE(pFieldDest, bLen);

    bTimeLen = fnBin2Unicode(pFieldDest, lSystemRebootNeededScrnTimeoutTicks);

    /* if field is right-aligned, pad left of string with spaces */
    if (bFieldCtrl & ALIGN_MASK)
    {
        pFieldDest[bTimeLen] = (UINT16) NULL;
        fnUnicodePadSpace(pFieldDest, bTimeLen, bLen);
    }

    /* make sure the field is NULL terminated.  if the translation happened 
    to exceed the width of this field, this will truncate the unicode 
    string that is sitting there */
    pFieldDest[bLen] = (UINT16) NULL;

    return ((BOOL) SUCCESSFUL);
}


/* Hard key functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkClearPaperErrors
//
// DESCRIPTION: 
//      
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              
//  04/24/2005  Vincent Yi      Code cleanup
//
// *************************************************************************/
T_SCREEN_ID fnhkClearPaperErrors  (UINT8          bKeyCode, 
                                   UINT8          bNumNext, 
                                   T_SCREEN_ID *  pNextScreens)
{
    T_SCREEN_ID     usNext;

    if (ucOIErrorCode != WRN_PIECE_SKIPPED)
    {
        UINT32  pMsgData[2];

        // error type, not equal to the last status
        pMsgData[0] = (UINT32) ERTYPE_PAPER_ERR;
        // sometime sensor status update will cause jam lever open, not only 
        // paper error does, so we use ucOIErrorCode that is prepared in the 
        // pre function of this screen instead of bLastPaperStatus; 
        pMsgData[1] = (UINT32) ucOIErrorCode;   

        (void)OSSendIntertask(CM, OIT, OC_CLR_ERR_REQ, LONG_DATA, pMsgData, 
                                sizeof(pMsgData));

        /* don't goto ready screen, wait for error clearing response.*/
        usNext = 0;
    }
    else // just ignore the warning
    {
        usNext = fnRetFromErrScreen(bNumNext, pNextScreens);
    }
    
    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkClearTapeErrors
//
// DESCRIPTION: 
//      Hard key function that clear tape errors. 
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  11/06/2007  Oscar Wang      Handle tape mail too short error.
//  03/21/2006  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkClearTapeErrors( UINT8          bKeyCode,
                                 UINT8          bNumNext, 
                                 T_SCREEN_ID *  pNextScreens )
{
    T_SCREEN_ID     usNext;

    if (ucOIErrorCode != WRN_PIECE_SKIPPED)
    {
        UINT32  pMsgData[2];

        if (ucOIErrorCode == ERR_JAM || ucOIErrorCode == ERR_PIECE_TOO_SHORT)
        {
            pMsgData[0] = (UINT32) ERTYPE_PAPER_ERR; 
        }
        else
        {
            pMsgData[0] = (UINT32) ERTYPE_TAPE_ERR; 
        }
        // sometime sensor status update will cause jam lever open, not only 
        // paper error does, so we use ucOIErrorCode that is prepared in the 
        // pre function of this screen instead of bLastPaperStatus; 
        pMsgData[1] = (UINT32) ucOIErrorCode;   

        OSSendIntertask(CM, OIT, OC_CLR_ERR_REQ, LONG_DATA, pMsgData, sizeof(pMsgData));

        /* don't goto ready screen, wait for error clearing response.*/
        usNext = 0;
    }
    else // just ignore the warning
    {
        usNext = fnRetFromErrScreen(bNumNext, pNextScreens);
    }
    
    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkClearFeederErrors
//
// DESCRIPTION: 
//      Hard key function that clear feeder errors. 
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/15/2006  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkClearFeederErrors( UINT8            bKeyCode,
                                   UINT8            bNumNext, 
                                   T_SCREEN_ID *    pNextScreens )
{
    T_SCREEN_ID     usNext = 0;
    UINT32          pMsgData[2];

    // error type, not equal to the last status
    pMsgData[0] = (UINT32) ERTYPE_FEEDER_ERR;
                                            
    pMsgData[1] = (UINT32) ucOIErrorCode;   

    OSSendIntertask(CM, OIT, OC_CLR_ERR_REQ, LONG_DATA, pMsgData, sizeof(pMsgData));
    
    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkClearInvalidPostage
//
// DESCRIPTION: 
//      
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  03/21/2006  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkClearInvalidPostage( UINT8          bKeyCode,
                                     UINT8          bNumNext, 
                                     T_SCREEN_ID *  pNextScreens )
{
    T_SCREEN_ID  usNext = 0 ;
    
    if((fnFlashGetByteParm(BP_KEY_IN_POSTAGE_MODE) & ~KIP_USE_MDS_TOKENS_MASK) != KIP_NOT_ALLOWED)
    {
        if (bNumNext > 0)
        {
            usNext = pNextScreens[0];
        }
    }
    else
    {
        if (bNumNext > 1)
        {
            usNext = pNextScreens[1];
        }
    }

    return (usNext );
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkClearMeterError
//
// DESCRIPTION:                                         
//      Hard key function to clear meter error.
//
// INPUTS:
//      Standard hard key function inputs (format 1).
//
// RETURNS:
//      Alwasys to next screen 1, InfraContactPB
//
// MODIFICATION HISTORY:
//  02/22/2006  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkClearMeterError (UINT8          bKeyCode, 
                                 T_SCREEN_ID    wNextScr1, 
                                 T_SCREEN_ID    wNextScr2)
{
    T_SCREEN_ID     usNext = 0;

    switch (bRecoveryMethod)
    {
    case RM_SIMPLE_CLEARING:
    case RM_PHONE_HOME:         
        usNext = wNextScr1;
        break;

    case RM_CANNOT_CLEAR:
    case RM_POWER_CYCLE:
        RebootSystem();
        break;
    
    default:
        break;
    }
    
    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkPaperErrorClearedReturn
//
// DESCRIPTION: 
//      Hard key function that clears the latest paper status when received paper 
//      error cleared message from CM, then return
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/15/2006  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkPaperErrorClearedReturn( UINT8          bKeyCode,
                                         UINT8          bNumNext, 
                                         T_SCREEN_ID *  pNextScreens )
{
    fnClearCMPaperErrorStatus();
                
    return fnRetFromErrScreen (bNumNext, pNextScreens);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkRetFromErrScreen
//
// DESCRIPTION: 
//      Hard key function that return the previous screen which is  before the 
//      entrance of the error screen.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      Next screens
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/15/2006  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkRetFromErrScreen( UINT8         bKeyCode,
                                  UINT8         bNumNext, 
                                  T_SCREEN_ID * pNextScreens )
{
    return(fnRetFromErrScreen(bNumNext, pNextScreens));
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnhkResumeFromMidnightAdvDate
//
// DESCRIPTION:
//      
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      Next screen
//      
//
// WARNINGS/NOTES: 
//      None.
//
// MODIFICATION HISTORY:               
//  09/05/2006  Vincent Yi      Initial version 
//
// *************************************************************************/
T_SCREEN_ID fnhkResumeFromMidnightAdvDate(UINT8         bKeyCode,
                                          UINT8         bNumNext,
                                          T_SCREEN_ID * pNextScreens)
{
    T_SCREEN_ID usNext = 0;
    if (fResumePrintingFromErr == TRUE)
    {
        OSSendIntertask(CM, OIT, OC_GET_STATUS_REQ, NO_DATA, NULL, 0);
        fnSetMessageTimeout(CM, CO_GET_STATUS_RSP, STANDARD_INTERTASK_TIMEOUT);
    }
    else
    {
        usNext = fnhkRetFromErrScreen(bKeyCode, bNumNext, pNextScreens);
    }
    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnhkClearMissingGraphicError
//
// DESCRIPTION:
//      Clears flag that indicates whether or not the missing graphics
//      error screen should be shown.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      Next screen
//      
//
// WARNINGS/NOTES: 
//      None.
//
// *************************************************************************/
T_SCREEN_ID fnhkClearMissingGraphicError(UINT8          bKeyCode,
                                          UINT8         bNumNext,
                                          T_SCREEN_ID * pNextScreens)
{
    T_SCREEN_ID usNext = 0;

    fnSetDisplayMissingGraphicError(FALSE);

    if (bNumNext)
        usNext = pNextScreens[0];

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkPaperErrorSK3
//
// DESCRIPTION:
//      Softkey 3 handling in paper error screen
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      Next screen
//      
//
// WARNINGS/NOTES: 
//      None.
//
// MODIFICATION HISTORY:    
//  10/25/2007  Oscar Wang      Fix FRACA GMSE00131947: for GTS mail, if debit 
//                              is done and paper error happens, we can't resume 
//                              printing even for DM400 since barcode is already 
//                              used.
//  06/22/2007  Vincent Yi      Initial version 
//
// *************************************************************************/
T_SCREEN_ID fnhkPaperErrorSK3(UINT8         bKeyCode,
                              UINT8         bNumNext,
                              T_SCREEN_ID * pNextScreens)
{
    if (fnAreWeInModeAjout() == TRUE || fnFlashGetByteParm(BP_DCAP_INDICIA_PWR_FAIL_REPORT) == TRUE ||
        fnOITGetBaseType() == OIT_BASE_300C ||
        (fnOITGetBaseType() == OIT_BASE_400C && ucOIErrorCode == ERR_JAM_LEVER_OPEN)||
        (fnIsGTSInterrupted() == TRUE))
    {
        return 0;
    }
    else
    {
        return fnhkResumePrinting (bKeyCode, bNumNext, pNextScreens);
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkPaperErrorSK4
//
// DESCRIPTION:
//      Softkey 4 handling in paper error screen
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      Next screen
//      
//
// WARNINGS/NOTES: 
//      None.
//
// MODIFICATION HISTORY:               
//  10/25/2007  Oscar Wang      Fix FRACA GMSE00131947: for GTS mail, if debit 
//                              is done and paper error happens, we can't resume 
//                              printing even for DM400 since barcode is already 
//                              used.
//  06/22/2007  Vincent Yi      Initial version 
//
// *************************************************************************/
T_SCREEN_ID fnhkPaperErrorSK4(UINT8      bKeyCode,
                              UINT8         bNumNext,
                              T_SCREEN_ID * pNextScreens)
{
    if ((fnOITGetBaseType() == OIT_BASE_300C)   ||
        (fnOITGetBaseType() == OIT_BASE_400C && ucOIErrorCode == ERR_JAM_LEVER_OPEN) ||
        (fnIsGTSInterrupted() == TRUE))
    {
        return fnhkClearPaperErrors(bKeyCode, bNumNext, pNextScreens);
    }
    else
    {
        return fnhkCancelPrinting (bKeyCode, bNumNext, pNextScreens);
    }
}


/* *************************************************************************
// FUNCTION NAME: fnhkGTSStoreResumePrint
//
// DESCRIPTION: 
//          
// INPUTS:
//          Standard hard key function inputs.
//
// RETURNS:
//          Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  09/20/2007  Oscar Wang    For DM300, don't clear fGTSStoreFinished flag.
//  09/05/2007  Oscar Wang    Initial version
// *************************************************************************/
T_SCREEN_ID fnhkGTSStoreResumePrint(UINT8   bKeyCode, 
                                UINT8   bNumNext, 
                                UINT16 *pNextScreens)
{
    T_SCREEN_ID usNext = 0;
    
    // if storing is not done, no indicator is active.
    if(fnGetGTSStoreFinishedFlag() == TRUE)
    {
        // For DM400 or tape printing, display "Resume"
        if((fnGetResumingPrintEvent() == OIT_PRINT_TAPE) || (fnOITGetBaseType() == OIT_BASE_400C ))
        {
            fnClearGTSStoreFinishedFlag();
            usNext = fnevContinueToPrint(bNumNext, pNextScreens, 0);
        }
        else
        {
            usNext = 0;
        }
    }

    return usNext;   
}


/* *************************************************************************
// FUNCTION NAME: fnhkGTSStoreCancelPrint
//
// DESCRIPTION: 
//          
// INPUTS:
//          Standard hard key function inputs.
//
// RETURNS:
//          Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  09/05/2007  Oscar Wang      Initial version
// *************************************************************************/
T_SCREEN_ID fnhkGTSStoreCancelPrint(UINT8   bKeyCode, 
                                UINT8   bNumNext, 
                                UINT16 *pNextScreens)
{
    T_SCREEN_ID usNext = 0;
    
    // if storing is not done, no indicator is active.
    if(fnGetGTSStoreFinishedFlag() == TRUE)
    {
        usNext = GetScreen(SCREEN_MM_INDICIA_READY); 
    }

    fnClearGTSStoreFinishedFlag();

    return usNext;   // go to EServiceConsignorEntry screen.
}


/* Event functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fnevInvokeErrScreen
//
// DESCRIPTION: 
//      Event function that saves the return screen ID before going to the 
//      error screen.
//
// INPUTS:
//      Standard event function inputs (format 2).
//
// RETURNS:
//      Next screen: the error screens.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  06/21/2007  Vincent Yi      Fix fraca 121246, handle the exception in diff
//                              weight mode on DM400C.
//  02/22/2006  Vincent Yi      Code cleanup
//              Henry Liu       Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevInvokeErrScreen(UINT8           bNumNext, 
                                T_SCREEN_ID *   pNextScreens, 
                                INTERTASK   *   pIntertask)
{
    T_SCREEN_ID     usReturn = 0;
    UINT8           ucEvent;

    // On DM400C, in diff weight mode, mail piece has been printed, before mail job
    // complete message comes, if there is a printing error like feeder cover open, 
    // it will transition to remove one piece screen directly instead of corresponding 
    // error screen.
    if (fnIsDWPiecePrinted() ==  TRUE)
    {
        fnDWPostMailPiecePrinted ();
        fnClearFlagDWPiecePrinted ();
        return pNextScreens[1]; // MMIndiciaReady, redirection to diff remove 
                                // one piece screen
    }

    if (bNumNext > 1)
    {
        // Next screen 2 is the return screen
        usReturn = pNextScreens[1]; 
    }
    else
    {   // If no next screen 2, current screen is the return screen
        usReturn = fnGetCurrentScreenID();
    }
    // Set the error return screen
    fnSetErrReturnScreen(usReturn);
    
    fnGetScreenEventById(fnGetCurrentScreenID(), &ucEvent);
    if (!(ucEvent == OIT_LEFT_PRINT_RDY     || 
          ucEvent == OIT_ERROR_METER        ||
          ucEvent == OIT_ENTER_POWERUP      ||
          ucEvent == OIT_GOTO_SLEEP         ||
          ucEvent == OIT_ENTER_POWERDOWN    ||
          ucEvent == OIT_ENTER_DIAG_MODE))
    {   // Except any disabling mode, store current mode for later resuming
        // print flow
        fnSetResumingPrintFlow (ucEvent);
    }

    return((bNumNext > 0)?pNextScreens[0]:0);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnevRetFromErrScreen
//
// DESCRIPTION: 
//      Event function that return the previous screen which is before the 
//      entrance of the error screen.
//
// INPUTS:
//      Standard event function inputs (format 2).
//
// RETURNS:
//      Next screens
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Henry Liu       Initial version
//  02/22/2006  Vincent Yi      Code cleanup
//
// *************************************************************************/
T_SCREEN_ID fnevRetFromErrScreen(UINT8          bNumNext, 
                                 T_SCREEN_ID *  pNextScreens, 
                                 INTERTASK  *   pIntertask)
{
    return(fnRetFromErrScreen(bNumNext, pNextScreens));
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnevPaperErrorClearedReturn
//
// DESCRIPTION: 
//      Event function that clears the latest paper status when received paper 
//      error cleared message from CM, then return 
//
// INPUTS:
//      Standard event function inputs (format 2).
//
// RETURNS:
//      Next screens: always next screen 1
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  03/10/2006  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevPaperErrorClearedReturn(UINT8           bNumNext, 
                                        T_SCREEN_ID *   pNextScreens, 
                                        INTERTASK   *   pIntertask)
{
    fnClearCMPaperErrorStatus();
    
    return fnevRetFromErrScreen (bNumNext, pNextScreens, pIntertask);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnevClearJamLeverOpen
//
// DESCRIPTION: 
//      Event function that clears the Jam lever open error status when 
//        received Jam close event from CM, then return 
//
// INPUTS:
//      Standard event function inputs (format 2).
//
// RETURNS:
//      Next screens: always next screen 1
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/24/2006  Adam Liu        Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevClearJamLeverOpen(UINT8             bNumNext, 
                                  T_SCREEN_ID *     pNextScreens, 
                                  INTERTASK     *   pIntertask)
{
    T_SCREEN_ID     usNext;

    UINT32  pMsgData[2];

    // error type, not equal to the last status
    pMsgData[0] = (UINT32) ERTYPE_PAPER_ERR;

    pMsgData[1] = (UINT32) ERR_JAM_LEVER_OPEN;  

    (void)OSSendIntertask(CM, OIT, OC_CLR_ERR_REQ, LONG_DATA, pMsgData, 
                            sizeof(pMsgData));

    /* don't goto ready screen, wait for error clearing response.*/
    usNext = 0;

    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnevCMErrorNotCleared
//
// DESCRIPTION: 
//      Event function for prompting error screen according to response message
//      for CM clearing error.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      error screens
//
// MODIFICATION HISTORY:
//  08/16/2006  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevCMErrorNotCleared(UINT8             bNumNext, 
                                  T_SCREEN_ID   *   pNextScreens, 
                                  INTERTASK     *   pIntertask)
{
    T_SCREEN_ID usNext = 0;
    UINT32      ulStatus = (UINT32)pIntertask->IntertaskUnion.lwLongData[0];
    UINT32      ulErrCode = (UINT32)pIntertask->IntertaskUnion.lwLongData[1];

    if (ulStatus != 0)
    {
        usNext = fnTransToErrorScreens(ulStatus, ulErrCode);
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnevJamLeverErrCleared
//
// DESCRIPTION: 
//      Event function after jam lever open error cleared.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      
//
// MODIFICATION HISTORY:
//  08/22/2006  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevJamLeverErrCleared(UINT8            bNumNext, 
                                   T_SCREEN_ID  *   pNextScreens, 
                                   INTERTASK    *   pIntertask)
{
    T_SCREEN_ID usNext = 0;
    UINT8           ucEvent;

    if (fResumePrintingFromErr == TRUE)
    {
        fnGetScreenEventById(fnGetPreviousScreenID(), &ucEvent);
        
        if((ucEvent == OIT_PRINTING) && (fnIsWOWMailJammed() == TRUE))
        {
            usNext = pNextScreens[0];
        }
        else
        {
            usNext = fnevContinueToPrint(bNumNext, pNextScreens, pIntertask);
        }
    }
    else
    {
        fnClearCMPaperErrorStatus();
        usNext = fnevRetFromErrScreen(bNumNext, pNextScreens, pIntertask);
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnevTopCoverClosed
//
// DESCRIPTION: 
//      Event function after top cover closed.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      error screens
//
// MODIFICATION HISTORY:
//  08/22/2006  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevTopCoverClosed(UINT8            bNumNext, 
                               T_SCREEN_ID  *   pNextScreens, 
                               INTERTASK    *   pIntertask)
{
    T_SCREEN_ID usNext = 0;
    UINT8           ucEvent;

    if (fResumePrintingFromErr == TRUE)
    {
        fnGetScreenEventById(fnGetPreviousScreenID(), &ucEvent);
        
        if((ucEvent == OIT_PRINTING) && (fnIsWOWMailJammed() == TRUE))
        {
            usNext = pNextScreens[0];
        }
        else
        {
            usNext = fnevContinueToPrint(bNumNext, pNextScreens, pIntertask);
        }
    }
    else
    {
        usNext = fnevRetFromErrScreen(bNumNext, pNextScreens, pIntertask);
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnevCheckCMStatusBeforePrint
//
// DESCRIPTION: 
//      Event function that checks the status of CM before continue printing
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      error screens if there are some errors
//      else to print
//
// MODIFICATION HISTORY:
//  09/08/2006  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevCheckCMStatusBeforePrint (UINT8          bNumNext, 
                                          T_SCREEN_ID  * pNextScreens, 
                                          INTERTASK    * pIntertask)
{
    T_SCREEN_ID usNext = 0;
    
    usNext = fnCheckCMStatusInPrinting();

    if (usNext == 0)
    {   // No error
        usNext = fnevContinueToPrint (bNumNext, pNextScreens, pIntertask);
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: fnevPaperCleared
//
// DESCRIPTION: 
//      Event function that handles paper cleared event.
//
// INPUTS:  
//      Standard event function inputs (format 2).
//
// RETURNS:
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/07/2009 Raymond Shen     Initial Version
// *************************************************************************/
UINT16 fnevPaperCleared(UINT8 bNumNext, 
                                   UINT16 *pNextScreens, 
                                   INTERTASK *pIntertask)
{
    UINT16 usNext = pNextScreens[0]; //MMIndiciaReady
    UINT32  pMsgData[2];

    if(fnGetPaperErrorShownStatus() == TRUE)
    {
        // error type, not equal to the last status
        pMsgData[0] = (UINT32) ERTYPE_PAPER_ERR;
        pMsgData[1] = (UINT32) ucOIErrorCode;   

        (void)OSSendIntertask(CM, OIT, OC_CLR_ERR_REQ, LONG_DATA, pMsgData, 
                                sizeof(pMsgData));
    }
    
    return usNext;  
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnevSystemRebootTimeoutClockTick
//
// DESCRIPTION: 
//      Event function that counts down from the SystemRebootNeeded screen timeout 
//      value and create time out event when it expires.
//
// INPUTS:
//      Standard event function inputs. (format 2)
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/25/2010  Raymond Shen    Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevSystemRebootTimeoutClockTick (UINT8             bNumNext, 
                                       T_SCREEN_ID  *   pNextScreens, 
                                       INTERTASK      * pIntertask)
{   
    /* Has the screen timed out? */ 
    if (lSystemRebootNeededScrnTimeoutTicks > 0)
    {
        /* The screen has NOT timed out. Decrement the timeout value. */
        lSystemRebootNeededScrnTimeoutTicks--;
    }
    else
    {
        if (fSystemRebootNeededScrnTimeoutExpired == FALSE)
        {
            /* Set the expiration flag so that only one message is sent 
                to the OIT. */
            fSystemRebootNeededScrnTimeoutExpired = TRUE;
            fnProcessEvent (EVENT_SYSTEM_REBOOT_TIMEOUT, pIntertask);
        }
    }

    return 0;
}

/* *************************************************************************
// FUNCTION NAME: fnevRemoteRefillDone
//
// DESCRIPTION: 
//      Event function that reboots the UIC.
//
// INPUTS:  
//      Standard event function inputs (format 2).
//
// RETURNS:
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/25/2010  Raymond Shen  Initial version
// *************************************************************************/
UINT16 fnevRebootUIC(UINT8 bNumNext, 
                                   UINT16 *pNextScreens, 
                                   INTERTASK *pIntertask)
{
    RebootSystem();
    return (0);
}


/* Variable graphic functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fngDiagError
//
// DESCRIPTION: 
//      Variable graphic function that displays the bottom separator line if 
//      diagnostics operation failed
//
// INPUTS:
//      Standard graphic function inputs.
//
// RETURNS:
//      always returns successful
//
// MODIFICATION HISTORY:
//  06/30/2006  Vincent Yi      Initial Version
//
// *************************************************************************/
BOOL fngDiagError (VARIABLE_GRAPHIC  *  pVarGraphic, 
                   UINT16            *  pSymID, 
                   UINT8                bNumSymListItems,                       
                   UINT8             *  pSymList,
                   UINT8             *  pAttributes)
{
    pVarGraphic->fUsed = FALSE;
    *pSymID = (UINT16) 0x0000;

/*
    if( (bNumSymListItems > 0) && (fnGetDiagSuccessFlag() == FALSE))
    {
    	EndianAwareCopy((UINT8 *)pSymID, pSymList, sizeof(UINT16));
    }
*/
    
    return (SUCCESSFUL);
}



/**********************************************************************
        Private Functions
**********************************************************************/

/* *************************************************************************
// FUNCTION NAME: 
//      fnSetResumePrintingFlag
//
// DESCRIPTION: 
//      Set resume-printing flag according to last event
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      error screens
//
// MODIFICATION HISTORY:
//  09/04/2006  Vincent Yi      Initial version
//      06/27/2007      I. Le Goff              Do not allow resume printing from error
//                                              in mode ajout
//
// *************************************************************************/
static void fnSetResumePrintingFlag (void)
{
    UINT8   ucLastPrintEvent = fnGetResumingPrintEvent();

    // For some errors, only on DM400C or tape printing, resuming
    // print is needed.
  if ( ( fnFlashGetByteParm(BP_DCAP_INDICIA_PWR_FAIL_REPORT) == FALSE)  && ((fnOITGetBaseType() == OIT_BASE_400C && ucLastPrintEvent == OIT_PRINTING)   ||
        (ucLastPrintEvent == OIT_PRINT_TAPE)        ||
        (ucLastPrintEvent == OIT_PRINT_TAPE_PERMIT) || 
        (ucLastPrintEvent == OIT_PRINT_TAPE_TEST)   || 
        (ucLastPrintEvent == OIT_PRINT_RPT_TO_TAPE)) )
    {
        fResumePrintingFromErr = TRUE;  
    }
    else
    {
        fResumePrintingFromErr = FALSE;
    }
}



/* *************************************************************************
// FUNCTION NAME: fnpErrRMManualPostageFile
// DESCRIPTION: Pre function that prepares the error code of RM manual postage
//              file error
//
// INPUTS:  Standard pre function inputs.
// CONTINUATION FUNCTION:  
// NEXT SCREEN:
//          always goes to home screen.
// MODIFICATION HISTORY:
//  04/19/2007   Andy MO    Inital version
// *************************************************************************/
char fnpErrRMManualPostageFile( void (** pContinuationFcn)(), unsigned long *pMsgsAwaited,
                             unsigned short *pNextScr )
{
    UINT8 bKIPMode;
    
    /* turn off the menu page up/down LEDs */
    SET_PAGEUP_LED_OFF();
    SET_PAGEDOWN_LED_OFF();     

    bKIPMode = fnFlashGetByteParm(BP_KEY_IN_POSTAGE_MODE);
    
    ucOIErrorClass = ERROR_CLASS_RATING;
    
    if(bKIPMode == KIP_ALLOWED_FENCINGLIMIT)
    {
        ucOIErrorCode = fnRateGetFencingLimitStatus();
    }
    else if(bKIPMode == KIP_ALLOWED_MIN_KIP_LIMIT )
    {
        ucOIErrorCode = fnRateGetMinKIPAmtStatus();  
    }
    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}








