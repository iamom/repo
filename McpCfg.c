/**********************************************************************
 PROJECT    :   Multiple Projects
 COPYRIGHT  :   2006, Pitney Bowes, Inc.  
 AUTHOR     :   Derek DeGennaro
 MODULE     :   FdrMcpTask.c
 
 DESCRIPTION:
    Public constants, data, and functions from the
	MCP Configuration

 MODIFICATION HISTORY:
    2/17/2006	 Derek DeGennaro	Initial Version
      .           .                       .
    <Date>	 <Developer_Name>	<Modification_Description>
 **********************************************************************/

/**********************************************************************
		Include Header Files
**********************************************************************/
#include "MCPCfg.h"


/*********************************************************************
		Data
**********************************************************************/

// Project Specific Data
/*
UINT8 MCP2DevTypeMap[MAX_MCPS_ALLOWED] = {
 //PB_MCP_2212,		// 0 - 0x46
 //PB_MCP_1663,   	// 1 - 0x47
 0,					// 2 - 0x48
 0,					// 3 - 0x49
 0,					// 4 - 0x4A
 0,					// 5 - 0x4B
 0,					// 6 - 0x4C
 0,					// 7 - 0x4D
 //PB_MCP_2215,		// 8 - 0x4E
 0					// 9 - 0x4F
};
*/
