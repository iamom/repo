/* ***********************************************************************
 PROJECT: Spark
 COMPANY: Pitney Bowes
 MODULE : $Workfile:   COMMUTIL.C  $
 REVISION:$Revision:   1.5  $

                Copyright (c) 1993 Pitney Bowes Inc.
                    35 WaterView Drive
                   Shelton, Connecticut  06484

 DESCRIPTION:
  This module contains routines that will parse the FOX link layer
  protocol, create a Fox frame and calculate a fletcher checksum.
 This file contains modules

 *	REVISION HISTORY:
 *
 *  10/18/2012  John Gao
 * Modified fnPlatformLinkLayerReceive to fix Fraca GMSE00218677.
 *
 *  08/15/2008  Joey Cui
 * Merged stuffs from Janus to support PC proxy feature.
 * 
 *	$Log:   H:/group/SPARK/ARCHIVES/DM150/uic_app/COMMUTIL.C_v  $
 *
 *    Rev 1.5   27 Dec 2004 12:11:46   HO501SU
 * Fixed a UART unrecoverable buffer 
 * overflow bug that caused dead scale 
 * problem after writing FFS.
 * 
 *    Rev 1.4   09 Feb 2004 17:47:24   BR507HA
 * Removed code for Canon LCD panel protocol - the LCD panel is always PB232.
 * 
 *    Rev 1.3   May 05 2003 09:54:22   mozdzjm
 * Brian's first pass at platform comm handler.
 * 
 *    Rev 1.2   Mar 24 2003 13:19:14   BR507HA
 * Remove test code
 * 
 *    Rev 1.1   19 Mar 2003 10:13:38   defilcj
 * changes for OS_06 build

 ********************************************************************** */

#include "sys.h"
#include "commglob.h"
#include "pbos.h"
#include "sac.h"
/* #include "scterr.h"
   #include "syserr.h" 
 */
#include "keypad.h"

//#define PLAT_BUF_DEBUG
#ifdef PLAT_COMM_BUF_DEBUG
#define PLAT_BUF_SIZE   100
char    rxPlatBuffer[PLAT_BUF_SIZE];
int     rxPlBufIdx=0;
#endif

#define MINIMUM_DLE_CHAR    0x80              /*Byte stuff char should not be less
                                                than 0x80                         */
#define DLE_MASK            0x7f              /*Used to strip DLE bit from data   */
#define SEQ_MASK            0x7f              /*Used to strip sequence bit from Class code. */
#define WAIT_STX            0                 /*State in LinkLayerReceive().         */
#define WAIT_HEADER         1                 /*State in LinkLayerReceive().         */
#define WAIT_CHAR           2                 /*State in LinkLayerReceive().         */
#define GOT_DLE             3                 /*State in LinkLayerReceive().         */

#ifdef JANUS

// Private Function Protytypes 
extern void fnProcessKey(unsigned char bKeyCode, unsigned char bKeyStatus);

#endif

#define LARGEST_CTRL_CHAR   0x1f              /*Largest Control Character. */


#define MINIMUM_HEADER_BYTE 0x20              /*Minimum Header byte value.  */
#define MAX_RECEIVE_FRAME_LENGTH 303          /*Mmax length of data for a   */
                                              /*receive frame. Includes     */
                                              /*ClassCode,Data,CheckSum     */

#define WAIT_FOR_ESC    0
#define WAIT_FOR_PLUS   1
#define WAIT_FOR_C      2
#define GET_CMD         3
#define GET_DATA        4


#define MIN_ASCII_NUM_CHAR		0X30
#define MAX_ASCII_NUM_CHAR		0X39

#define MSG_LEN_SIZE    6
unsigned short sum1[16];
unsigned short sum2[16];
unsigned short sumA[16];
unsigned short sumB[16];
unsigned short sumC[16];
unsigned short tpos;
unsigned char GetPB232Buffer( COMMDEVICE *dev )
{
  int i;
  for( i = 0 ; i < dev->wMsgCnt ; i++ )
    if(dev->pMsgBufs[ i ].fBusy == false )
      break;
  return(i);
}

/***********************PROTOTYPES********************************************/

void SendACK(COMMDEVICE *dev);
void SendNAK(COMMDEVICE *dev);
void FletcherChecksumB
                    (unsigned short wSum1, unsigned short wSum2,
                     unsigned char *pbARC1, unsigned char *pbARC2);
void FletcherChecksumA
                    (unsigned char bChar,unsigned short *pwSum1, unsigned short *pwSum2);

/* SB */
/*  ************************************************************************ */
/*  MODULE: LinkLayerReceive */
/*  AUTHOR: Gary Heiden */
/*  DESCRIPTION: Header to be used for each Spark function. */
/*     This routine parses the low level Fox Link Layer receive protocol. */
/*  LIMITATIONS: */
/*  INPUTS     : */
/*  OUTPUTS    :  FUNCTIONS CALLED: */
/* 			none */
/*   PDL: */
/*   NAME        -  Receive */
/*   TYPE        -  array of type RECEIVE_MANAGEMENT_STRUCTURE */
/*   DESCRIPTION -  Contain state information for a particular port. */
/*   NAME        -  CommTaskIds */
/*   TYPE        -  array of unsigned long */
/*   DESCRIPTION -  Contains the tasks ids for the communication tasks. */
/*  */
/*  */
/*  RETURNED VALUES:    none */
/*  */
/*  LIMITIATIONS AND */
/*  RESTRICTIONS:    none */
/*  */
/*  PSEUDOCODE: */
/*     If the character is a control character, filter it according */
/*     to the following rules: */
/*         STX, ACK, NAK only valid if we're waiting for a */
/*             frame type chcaracter. */
/*         ETX, DLE only valid if we're waiting for a frame */
/*             character */
/*     If the control character matches one of the above rules, it is */
/*     allowed to "drop thru" to the parser below (which also catches */
/*     non-control characters.  Otherwise, we have a bad frame */
/*     reception. */
/*  */
/*     Process any characters which fall thru according to state: */
/*     WAIT_STX: */
/*         any character other than an STX is ignored */
/*    WAIT_HEADER: */
/*         a valid header character causes change to WAIT_SEQUENCE */
/*         state, others cause protocol to reset */
/*     WAIT_CHAR: */
/*         ETX causes us to "end" reception and await ARC.  DLE */
/*         causes a state change for "data link escape".  Normal */
/*         characters are simply buffered, unless we're out of */
/*         room (which is fatal to this transmission) */
/*     GOT_DLE: */
/*         The character better have the top bit set, per the */
/*         protocol.  If not, we abort this reception.  Otherwise */
/*         if we have room, buffer the modified character.  If */
/*         we're out of room, abort and reset */
/*  REVISION HISTORY: */
/*  Rev #		Name					Date				Reason */
/*  -----    ----------------- -------------- ---------------------------- */
/*  1.19     Steve Tymoszuk    11/14/94       Added receive flag logic */
/* SE**********************************************************************/


void
LinkLayerReceive(unsigned char bChar,COMMDEVICE *dev)
{
void * addr ;                                /*  Used to get memory partitions. */
BOOL fSendFlag,fStatus;
short sDid;
short sSid;
BOOL  fResult;
PB232BUFFER *aMsg;

    /*  Filter the control characters. */
    if (bChar <= LARGEST_CTRL_CHAR)
    {
        switch(bChar)
        {
          case PBACK:
            /*  ACK is not valid while receiveing a frame. */
            /*  ACK is only recognized when in the WAIT_STX state. */
            if (dev->Receive.bState != WAIT_STX)
              {
						dev->fReceivingFrame = FALSE;
						dev->Receive.bState = WAIT_STX;
               /* Initialize */
						dev->fReceivingFrame = TRUE;
						dev->Receive.bState = WAIT_HEADER;
						dev->Receive.wFletcherSum1 = 0;
						dev->Receive.wFletcherSum2 = 0;
						dev->Receive.wBufferLength = 0;
					       if( dev->wCurMsg < dev->wMsgCnt )
						 {
						   dev->Receive.pbBuffer = dev->pMsgBufs[dev->wCurMsg].bData;
						 }
					       else
								 dev->Receive.pbBuffer = 0;
		         }
		         else
		         {
				         /*  Indicate to comm task that a ACK was received. */
					    dev->lwActivateEvent |= RECEIVED_ACK_EVENT;
					    fStatus = OSActivateHISR( ( OS_HISR *) &dev->devHisr);
	            }
            break;

          case PBNAK:
            /*  NAK is not valid while receiveing a frame. */
            /*  NAK is only recognized when in the WAIT_STX state. */
            if (dev->Receive.bState != WAIT_STX)
              {
               dev->fReceivingFrame = FALSE;
               dev->Receive.bState = WAIT_STX;
               dev->Statistics.lwBadFramesReceived++;
               /* Initialize */
	       dev->fReceivingFrame = TRUE;
	       dev->Receive.bState = WAIT_HEADER;
	       dev->Receive.wFletcherSum1 = 0;
	       dev->Receive.wFletcherSum2 = 0;
	       dev->Receive.wBufferLength = 0;
	       if( dev->wCurMsg < dev->wMsgCnt )
			 {
			   dev->Receive.pbBuffer = dev->pMsgBufs[ dev->wCurMsg ].bData;
			 }
	       else
				 dev->Receive.pbBuffer = 0;
       }
       else
       {
               /*  Indicate to comm task that a NAK was received. */
				dev->lwActivateEvent |= RECEIVED_NAK_EVENT;
				fStatus = OSActivateHISR( ( OS_HISR *) &dev->devHisr);
        }
        break;

         case PBSTX:
            /*  If STX is received and not in WAIT_STX state then */
            /*  this specifies the start of a new message.  Ignore */
            /*  current messge. */
            if (dev->Receive.bState != WAIT_STX)
            {
               dev->fReceivingFrame = FALSE;
               dev->Receive.bState = WAIT_STX;
               dev->Statistics.lwBadFramesReceived++;
               /* Initialize */
	       dev->fReceivingFrame = TRUE;
	       dev->Receive.bState = WAIT_HEADER;
	       dev->Receive.wFletcherSum1 = 0;
	       dev->Receive.wFletcherSum2 = 0;
		    tpos = 0;
	       dev->Receive.wBufferLength = 0;
	       if( dev->wCurMsg < dev->wMsgCnt )
			 {
			   dev->Receive.pbBuffer = dev->pMsgBufs[dev->wCurMsg].bData;
			 }
	       else
				 dev->Receive.pbBuffer = 0;

            }
            break;

        case PBETX:
        case PBDLE:
        default:


            /*  ETX and DLE are not valid while receiveing a frame. */
            /*  ETX and DLE are only recognized when in the WAIT_STX state. */
            if (dev->Receive.bState != WAIT_CHAR)
            {
               dev->fReceivingFrame = FALSE;
               /*Return Partition current frame is invalid. */
               dev->Statistics.lwBadFramesReceived++;
               /* Initialize */
	            dev->fReceivingFrame = TRUE;
	            dev->Receive.bState = WAIT_HEADER;
	            dev->Receive.wFletcherSum1 = 0;
	            dev->Receive.wFletcherSum2 = 0;
		         tpos = 0;
	            dev->Receive.wBufferLength = 0;
	            if( dev->wCurMsg < dev->wMsgCnt )
			      {
			        dev->Receive.pbBuffer = dev->pMsgBufs[ dev->wCurMsg ].bData;
			      }
	            else
			      	  dev->Receive.pbBuffer = 0;
               dev->Receive.bState = WAIT_STX;
	            break;
            }
        }
    }

    switch(dev->Receive.bState)
    {
      case WAIT_STX:
        if (bChar == PBSTX)
         {

               /* Initialize */
               dev->fReceivingFrame = TRUE;
               dev->Receive.bState = WAIT_HEADER;
               dev->Receive.wFletcherSum1 = 0;
               dev->Receive.wFletcherSum2 = 0;
               dev->Receive.wBufferLength = 0;
			      if( dev->wCurMsg < dev->wMsgCnt )
			        {
			          dev->Receive.pbBuffer = dev->pMsgBufs[ dev->wCurMsg ].bData;
			        }
			      else
			        dev->Receive.pbBuffer = 0;
				    tpos = 0;
         }
         /*  If event display is on DIAG port this handles screen switching */
         /*  the screens. */
         /*  else if( fDisplaylog == true && bPortId == EXT_PC_LOGICAL_PORT ) */
         /*  { */
         /*    fChangeDisplayState = true; */
         /*  } */
         break;

      case WAIT_HEADER:
        if ((bChar & SEQ_MASK) >= MINIMUM_HEADER_BYTE)     /* if received header byte is */
          {
            /*  Add header byte to checksum */
            FletcherChecksumA(bChar,&dev->Receive.wFletcherSum1,&dev->Receive.wFletcherSum2);

            /*  Save header byte */
            dev->Receive.bCurrentHeader = bChar;
            dev->Receive.bState = WAIT_CHAR;

            /*  Strip off the sequence bit. */
		    if(dev->Receive.pbBuffer)
			{
//BCH
//#if defined(COMET) || defined(COMET1)  /* Don't strip the sequence bit if CGPMI */
//			  if (dev->bInComingQueue == CGPMI)
//		    	  *(dev->Receive.pbBuffer + dev->Receive.wBufferLength++) = bChar;
//			  else
//#endif
		    	  *(dev->Receive.pbBuffer + dev->Receive.wBufferLength++) = bChar & SEQ_MASK;
			}		
          }
        else
          {
            /*  Not a valid header. Send NAK. */
            SendNAK(dev);
          }
        break;


      case WAIT_CHAR:
        switch(bChar)
        {
        /*  Received a DLE.  Next char received will be an original char. */
        case PBDLE:
            dev->Receive.bState = GOT_DLE;
            break;

        /*  Received ETX. Signifies an end of frame. */
        case PBETX:
            fSendFlag = dev->fReceiveDataFlag;
            if( fSendFlag == true && dev->Receive.wBufferLength > 1 )
            {
                dev->Receive.wFletcherSum2 %= 255;
                if ((dev->Receive.wFletcherSum1 == 0) && (dev->Receive.wFletcherSum2 == 0))
                {
                  /* don't add the checksum byte subtract two bytes from count */
                  dev->Receive.wBufferLength -= 2;
                  SendACK(dev);

                  /*  Check if previous header was the same. */
                  /*  if current header not the same as last valid header send frame to task. */
                  if (dev->Receive.bLastValidHeader == dev->Receive.bCurrentHeader)
                  {
                      /*  Duplicate frame. */
                      dev->Statistics.lwDuplicateFramesReceived++;

                  }
                  else
                  {
                     /*  Save header value. */
                     dev->Receive.bLastValidHeader = dev->Receive.bCurrentHeader;
				     if( dev->wCurMsg < dev->wMsgCnt )
				       {
					      dev->pMsgDel = &dev->pMsgBufs[ dev->wCurMsg ];
					      dev->pMsgDel->fBusy = true;
					      dev->pMsgDel->wLength = dev->Receive.wBufferLength;
					      dev->wCurMsg = GetPB232Buffer( dev );
					      if( dev->wCurMsg < dev->wMsgCnt )
					        {
					          dev->Receive.pbBuffer = dev->pMsgBufs[ dev->wCurMsg ].bData;
					        }
					      else
					        dev->Receive.pbBuffer = 0;
					      dev->lwActivateEvent |= DELIVER_MSG_EVENT;
					      fStatus = OSActivateHISR( ( OS_HISR *) &dev->devHisr);
				       }
				     else
				       dev->Receive.pbBuffer = 0;
		  }
		  /*                   Next state. */
                  dev->Receive.bState = WAIT_STX;
                }
                else
                {
		  /*                   Not a valid frame. Send NAK */
                  SendNAK(dev);
                }
            }
            else
            {
                dev->Receive.bState = WAIT_STX;

                SendNAK(dev);

            }
          break;

        default:
	  /*             Check if too many bytes have been received for current frame. */
            if (dev->Receive.wBufferLength >= MAX_RECEIVE_FRAME_LENGTH)
             {
                SendNAK(dev);
                break;
             }
	    /*             Add character to checksum. */
            FletcherChecksumA(bChar,&dev->Receive.wFletcherSum1,&dev->Receive.wFletcherSum2);

	    /*             Save character in buffer. */
	    if(dev->Receive.pbBuffer)
	      *(dev->Receive.pbBuffer + dev->Receive.wBufferLength++) = bChar;
        }
        break;

      case GOT_DLE:
        if (bChar < MINIMUM_DLE_CHAR)
         {
            SendNAK(dev);
            break;
         }

        if (dev->Receive.wBufferLength >= MAX_RECEIVE_FRAME_LENGTH)
         {
            SendNAK(dev);
            break;
         }

        bChar &= DLE_MASK;
        FletcherChecksumA(bChar,&dev->Receive.wFletcherSum1,&dev->Receive.wFletcherSum2);
	if(dev->Receive.pbBuffer)
	  *(dev->Receive.pbBuffer + dev->Receive.wBufferLength++) = bChar;
        dev->Receive.bState = WAIT_CHAR;
        break;


      default:
	/*        Return Partition back to pool. */
        dev->Receive.bState = WAIT_STX;
        dev->fReceivingFrame = FALSE;
        break;
    }
}



void SendNAK(COMMDEVICE *dev)
{
  BOOL fStatus;

  /*   Set Next state. */
  dev->Receive.bState = WAIT_STX;

  /*   Indicate that this port is currently not receiving a frame. */
  dev->fReceivingFrame = FALSE;

   /*   Log statistics. */
  dev->Statistics.lwNaksTransmited++;
  dev->Statistics.lwBadFramesReceived++;
  /* ACK Change */
  if (dev -> fTransmittingFrame == FALSE)
     (*dev->pfUart->fnWriteFIFO)((unsigned short*) dev->pbUartBase,(unsigned char) PBNAK);
  else
     dev -> bRespondToPreviousFrame = NAK_RESPONSE_REQUIRED ;
}





void SendACK(COMMDEVICE *dev)
{
  /*   Log statistics. */
  dev->Statistics.lwAcksTransmited++;
  dev->Statistics.lwGoodFramesReceived++;


   /*   Indicate that this port is currently not receiving a frame. */
  dev->fReceivingFrame = false;

  /* ACK Change */
  if (dev->fTransmittingFrame == FALSE)
      (*dev->pfUart->fnWriteFIFO)((unsigned short*)dev->pbUartBase,(unsigned char) PBACK);
  else
      dev -> bRespondToPreviousFrame = ACK_RESPONSE_REQUIRED ;


}



/************************************************************************/
/*                                                                       */
/* FUNCTION NAME:    CalculateFletcherA                                  */
/*                                                                       */
/* FUNCTION DESCRIPTION:                                                 */
/*    This function is the first part of the fletcher checksum algorithm.*/
/*    This function calculates the two checksum values.                  */
/*                                                                       */
/* AUTHOR:      Gary Heiden                                              */
/*                                                                       */
/* DATE:        08/18/93                                                 */
/*                                                                       */
/* INPUT PARAMETERS:    none                                             */
/*                                                                       */
/* LOCAL VARIABLES:    none                                              */
/* INPUT PARAMETERS:                                                     */
/*    name        data type       description                            */
/*    ----        ---------       -----------                            */
/*    bChar      unsigned char    byte to add to checksum                */
/*    pwSum1     unsigned int     sum value used in fletcher checksum    */
/*    pwSum2     unsigned int     sum value used in fletcher checksum    */
/*                                                                       */
/*                                                                       */
/* UNIT GLOBALS:      none                                               */
/*                                                                       */
/* SYSTEM GLOBALS:    none                                               */
/*                                                                       */
/* RETURNED VALUES:    none                                              */
/*                                                                       */
/*                                                                       */
/* LIMITIATIONS AND                                                      */
/* RESTRICTIONS:    none                                                 */
/*                                                                       */
/* PSEUDOCODE:                                                           */
/* Add the received character to the sum1 value                          */
/*  if the sum1 value is greater than or equal to 255                    */
/*   subtract 255 from sum1                                              */
/*  add the sum1 value to sum2                                           */
/*                                                                       */
/*                                                                       */
/* REVISION HISTORY:                                                     */
/*    rev #   name        date    reason                                 */
/*    ----    ----        ----    ------                                 */
/*                                                                       */
/************************************************************************/

void FletcherChecksumA
                    (unsigned char bChar,unsigned short *pwSum1, unsigned short *pwSum2)
{
	sum1[ tpos ] = *pwSum1;
	sum2[ tpos ] = *pwSum2;
	sumA[ tpos ] = *pwSum1;
	sumB[ tpos ] = *pwSum2;
   *pwSum1 += bChar;
   if (*pwSum1 >= 255)
     *pwSum1 -= 255 ;
   *pwSum2 +=  *pwSum1;
   if (*pwSum2 >= 255)
     *pwSum2 -= 255 ;
	sumA[ tpos ] = *pwSum1;
	sumB[ tpos ] = *pwSum2;
	sumC[ tpos++ ] = bChar;
	tpos &= 0xf;
}

/************************************************************************/
/*                                                                       */
/* FUNCTION NAME:    CalculateFletcherB                                  */
/*                                                                       */
/* FUNCTION DESCRIPTION:                                                 */
/*    This function is the second part of the fletcher algorithm.        */
/*    This function prepares the checksum values so they can be          */
/*    encorporated into the data stream.                                 */
/*                                                                       */
/* AUTHOR:      Gary Heiden                                              */
/*                                                                       */
/* DATE:        08/18/93                                                 */
/*                                                                       */
/* INPUT PARAMETERS:                                                     */
/*    name        data type    description                               */
/*    ----        ---------    -----------                               */
/*    wSum1      unsigned int     sum value used in fletcher checksum    */
/*    wSum2      unsigned int     sum value used in fletcher checksum    */
/*    pbARC1     unsigned char *  transmitted fletcher checksum byte 1   */
/*    pbARC2     unsigned char *  transmitted fletcher checksum byte 2   */
/*                                                                       */
/* LOCAL VARIABLES:   none                                               */
/*                                                                       */
/* UNIT GLOBALS:      none                                               */
/*                                                                       */
/* SYSTEM GLOBALS:    none                                               */
/*                                                                       */
/* RETURNED VALUES:    none                                              */
/*                                                                       */
/* LIMITIATIONS AND                                                      */
/* RESTRICTIONS:    none                                                 */
/*                                                                       */
/* PSEUDOCODE:                                                           */
/*                                                                       */
/*                                                                       */
/*                                                                       */
/* REVISION HISTORY:                                                     */
/*    rev #   name        date    reason                                 */
/*    ----    ----        ----    ------                                 */
/*                                                                       */
/************************************************************************/


void FletcherChecksumB
                    (unsigned short wSum1, unsigned short wSum2,
                     unsigned char *pbARC1, unsigned char *pbARC2)
{
    *pbARC1 = (unsigned char) (255 - (( wSum1 + (wSum2 % 255) ) % 255));

    *pbARC2 = (unsigned char)(255 - ((wSum1 + *pbARC1 ) % 255));

}





/************************************************************************/
/*                                                                       */
/* FUNCTION NAME:    PrepareNextFrame()                                  */
/*                                                                       */
/* FUNCTION DESCRIPTION:                                                 */
/*    This routine formats a application data into the link layer        */
/*    format.                                                            */
/*                                                                       */
/* AUTHOR:      Gary Heiden                                              */
/*                                                                       */
/* DATE:        08/24/94                                                 */
/*                                                                       */
/* INPUT PARAMETERS: none.                                               */
/*                                                                       */
/* LOCAL VARIABLES:                                                      */
/*    pbPtr -   unsigned char*    pointer into Msg_buffer                */
/*                                                                       */
/*    wCount -  unsigned int    number of bytes in Msg_buffer            */
/*                                                                       */
/*    fletcherSum1 - unsigned int contains the first byte of fletcher    */
/*    checksum calculation.                                              */
/*                                                                       */
/*    fletcherSum2 - unsigned int contains the second byte of fletcher   */
/*    checksum calculation.                                              */
/*                                                                       */
/*    frameDataLength - unsigned int that indicated the number of raw    */
/*    application data used for the current frame build.                 */
/*                                                                       */
/*    fletcherChecksum - unsigned char array of two elements that will   */
/*    contain the actual two byte fletcher checksum that will be         */
/*    transmited with the frame.                                         */
/*                                                                       */
/* UNIT GLOBALS:                                                         */
/*  NAME        -  wApplicationMessageBlockLength                        */
/*  TYPE        -  unsigned int                                          */
/*  DESCRIPTION -  Indicates the working length of the application       */
/*                    message that the link layer is currently processing*/
/*                                                                       */
/*  NAME        -  pbApplicationMessageBlock                             */
/*  TYPE        -  unsigned char *                                       */
/*  DESCRIPTION -  Points to the application message buffer that the     */
/*                 link layer is currently processing.                   */
/*                                                                       */
/*  NAME        -  bSequenceBit                                          */
/*  TYPE        -  unsigned char                                         */
/*  DESCRIPTION -  Provides the sequence bit for the frame that is       */
/*                 currently being built.                                */
/*                                                                       */
/*  NAME        -  bClassCode                                            */
/*  TYPE        -  unsigned char                                         */
/*  DESCRIPTION -  Indicates the class code of the current application   */
/*                 message.                                              */
/*                                                                       */
/*  NAME        -  fTheirIsAFrame                                        */
/*  TYPE        -  unsigned char                                         */
/*  DESCRIPTION -  Indicates that there is a frame to transmit.          */
/*                                                                       */
/*                                                                       */
/*  NAME        -  FrameBuffer1                                          */
/*  TYPE        -  unsigned char array                                   */
/*  DESCRIPTION -  One of two buffers used to build transmit frames.     */
/*                 One frame buffer will be the current frame the other  */
/*                 will be the used    to build the next frame.          */
/*                                                                       */
/*  NAME        -  FrameBuffer2                                          */
/*  TYPE        -  unsigned char array                                   */
/*  DESCRIPTION -  One of two buffers used to build transmit frames.     */
/*                 One frame buffer will be the current frame the other  */
/*                 will be the used    to build the next frame.          */
/*                                                                       */
/*  NAME        -  wFrameBuffer1Length                                   */
/*  TYPE        -  unsigned int                                          */
/*  DESCRIPTION -  Indicates the length of the frame in FrameBuffer1.    */
/*                                                                       */
/*  NAME        -  wFrameBuffer2Length                                   */
/*  TYPE        -  unsigned int                                          */
/*  DESCRIPTION -  Indicates the length of the frame in FrameBuffer2.    */
/*                                                                       */
/*  NAME        -  pbNextFrame                                           */
/*  TYPE        -  unsigned char *                                       */
/*  DESCRIPTION -  Pointer used to point to the either FrameBuffer1 or   */
/*                 FrameBuffer2.  The next frame is then built in this   */
/*                 buffer.                                               */
/*                                                                       */
/*  NAME        -  pwNextFrameLength                                     */
/*  TYPE        -  unsigned int *                                        */
/*  DESCRIPTION -  Pointer used to point to the location where the length*/
/*                 of the frame stored in FrameBuffer1 or FrameBuffer2 is*/
/*                 stored.                                               */
/*                                                                       */
/*                                                                       */
/* LIMITIATIONS AND                                                      */
/* RESTRICTIONS:                                                         */
/*    Message may not exceed 300 chars in length                         */
/*                                                                       */
/* PSEUDOCODE:                                                           */
/*                                                                       */
/*    Get the next block length                                          */
/*    Format beginning of frame as follows:                              */
/*        First byte STX                                                 */
/*        Second byte sequence bit and message class code                */
/*    Init the fletcher checksum based on those bytes                    */
/*    Point the output pointer just after the message class code         */
/*    For each byte in the frame to be sent                              */
/*        If the character is a control character, output                */
/*            a DLE followed by the char with the MSB set                */
/*        else output the character                                      */
/*        In either case, add the "real" character to the Fletcher       */
/*        Checksum                                                       */
/*    Add the fletcher checksum to the frame                             */
/*    Add the ETX to the end of the frame                                */
/*                                                                       */
/* REVISION HISTORY:                                                     */
/*    rev #    name        date    reason                                */
/*    ----     ----        ----    ------                                */
/*     1     G. Heiden    08/24/93                                       */
/************************************************************************/


BOOL PrepareNextFrame(unsigned char *pbFrameBuffer, unsigned short *pwFrameLength,
TRANSMIT_MANAGEMENT_STRUCTURE *Transmit)
{
    unsigned short  wFletcherSum1 = 0;
    unsigned short  wFletcherSum2 = 0;
    unsigned char fletcherChecksum[2];
    unsigned char *pbPtr;
    unsigned int i = 0;
    unsigned char bChar;




  if (Transmit->wMessageLength > MAX_USB_TRANSMIT_FRAME_SIZE)
    return((unsigned long)FAILURE);     /*message length exceeded. */


  if (Transmit->bMessageSequenceBit == SET)
     Transmit->bMessageSequenceBit = CLEAR;
  else
     Transmit->bMessageSequenceBit = SET;



  pbPtr = pbFrameBuffer;
  *(pbPtr++) = PBSTX;
  *pbPtr = (unsigned char)(Transmit->bMessageSequenceBit  | Transmit->bMessageClassCode);
  FletcherChecksumA(*(pbPtr++),&wFletcherSum1,&wFletcherSum2);

  *pwFrameLength = 2;

  while(Transmit->wMessageLength > 0)
    {
        switch(*(Transmit->pbMessageData))
        {
            case PBSTX:
            case PBACK:
            case PBNAK:
            case PBETX:
            case PBDLE:
                *(pbPtr++) = PBDLE;
                *(pbPtr++) = (unsigned char)(*(Transmit->pbMessageData) | 0x80);
                *pwFrameLength += 2;
                break;
            default :
                *(pbPtr++) = *(Transmit->pbMessageData);
                *pwFrameLength += 1;
        }
        bChar = *(Transmit->pbMessageData++);
        FletcherChecksumA(bChar,&wFletcherSum1,&wFletcherSum2);

        --Transmit->wMessageLength;
    }

    FletcherChecksumB(wFletcherSum1,wFletcherSum2,
                     &fletcherChecksum[0],&fletcherChecksum[1]);


    for (i=0;i<2;i++)
    {
        switch(fletcherChecksum[i])
        {
            case PBSTX:
            case PBACK:
            case PBNAK:
            case PBETX:
            case PBDLE:
                *(pbPtr++) = PBDLE;
                *(pbPtr++) = (unsigned char)(fletcherChecksum[i] | 0x80);
                *pwFrameLength += 2;
                break;
            default :
                *(pbPtr++) = fletcherChecksum[i];
                *pwFrameLength += 1;
        }

    }

    *(pbPtr++) = PBETX;
    *pwFrameLength += 1;


 return(SUCCESSFUL);

}


