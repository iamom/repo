/**********************************************************************
 PROJECT    :   Future Phoenix
 COPYRIGHT  :   2005, Pitney Bowes, Inc.  
 AUTHOR     :   
 MODULE     :   OitMsg.c

 DESCRIPTION:
    Task entry point & message handler for the operator interface task. 

//-----------------------------------------------------------------------------
 MODIFICATION HISTORY: 

 2012.09.11 Bob Li           FPHX 02.10
   Added mAbacusErrAccountingDiscrepancy3 to event table to fix fraca GMSE00166440.
   Updated the screen mErrMissingImage event to OIT_LEFT_PRINT_RDY to fix fraca 170093.
   
 2012.09.06 Bob and Liu Jupeng   FPHX 02.09
    Added the screen mOOBPhoneDialingPrefixSetting into the screen event array for 
    hiding the prefix screen on oob when using the LAN connection.

 2012.08.23  Adam Liu    FPHX 02.09
    Merged the code change of Enhancement Request 217515 from the branch fphx02.10.
    Clarisa Bellamy - Modify fnPaintCurrentScreen: Add syslog message with screen ID.

 2011.07.20 Clarisa Bellamy  FPHX 02.07
  Changes for detecting errors in mode-change request software:
  Functions affected: 
       fnDoModeAdjustments, fnRequestNewMode, fnmProcessEvent, 
       fnProcessModeChange, fnOITDeadmanExpire
  - If a mode change request uses an event that is not in the StEventNextScreen, 
    put an ERROR entry in the syslog identifying the event, and in the 
    exception.log FFS file. Then report a meter error 081F (on the screen.) 
  - Uncomment the Start and Stop commands for the OIT deadman timer.  It is 
    started when a mode request is sent to the SysTask and the fIgnoreKeysAndEvents
    flag is set.  It is cleared in fnProcessModeChange and in fnmProcessEvent, 
    (whether or not the modechange request or the event to process is the reply 
    from the systask).  In both cases, the fIgnoreKeysAndEvents is cleared. This 
    should add the ability to detect problems with unanswered mode change requests.
  - If the OIT Deadman timer expires, put an entry in the syslog identifying 
    the event that was unanswered, and report a meter error.
  - Add new local variable ucPendingModeChangeEvent, so the unanswered request
    can be identified in the logs.
  - Add THISFILE definition used by logException macro.
  - Add file-scope scratchpad100 buffer for composing entries to syslog.
  - Make UcConvertEvent a const table, since it is not designed to be written to.
  - Comment out entries in StEventNextScreen that are duplicates.  (They have 
    the same event as a previous entry, and so will never be seen by the code.)
  - Remove unused variable, wLastPrintReadyRoot, and some commented out code.
  - Updated a lot of inaccurate comments, and added new ones.

 06/30/2011 Joe Zhang     Added a new item into StScreenEventMap to fix double refills issue.

 2010.05.20 Rocky Dai - FPHX 2.06
    Modified function fnBuildFieldsListAfterError to fix Fraca GMSE00187696.

 2009.11.18 Raymond Shen - FPHX 2.02
    Added a third parameter "fIsModeChangePending" to function fnRequestNewMode() and modified 
    all the places that call this function accordingly.

 2009.09.17 Jingwie,Li   - FPHX2.01
    Added mEServicePrintTape in StEventAdjTable[] for Canada Package service.

  2009.07.15 Clarisa Bellamy - FPHX 2.0
   Updates for Screen Version changed from 2-part to 3-part:
   - Modified fnGetScrVer to allow for 3-part screen version numbers and to 
     allow the caller to only ask for one of the versions (tree or lang.)
     Update function header.
   - Modified fnDoVersionCompare to allow for 3-part version numbers by 
     converting all the arguments and the mask to ulongs.  Update function 
     header.
   - Modify fnOITValidateScreenVersions to handle 3-part screen version numbers.
   LINT and other cleanup:
   - Replaced ambiguous NULL with either NULL_PTR or NULL_VAL, as appropriate.
   - Replaced empty arg lists with void arg lists.
   - Cast functions as void if return value is ignored.
                                - 
    07/17/2009  Raymond Shen    Changed mErrWOWDimRating, mErrWOWSBR to mErrSBRError, mErrWOWError respectively in StScreenEventMap[].
    07/15/2009  Jingwei,Li      Modified fnBuildFieldsList() to make blinking cursor displayed.
    06/23/2009  Joey Cui        Added fnChangeScreenAfterError(), fnPaintCurrentScreenAfterError()
                                fnBuildFieldsListAfterError() to fix fraca GMSE00163022 and GMSE00161091
    05/20/2009  Raymond Shen    Added mErrWOWMailJammed, mErrWOWDimRating, mErrWOWSBR in StScreenEventMap[]
                                for wow error handling.
    04/20/2009  Jingwei Li      Added mServDiagWOWSBROptions in StScreenEventMap[] for SBR diagnostics.
    03/31/2009  Raymond Shen    Added mServDiagWOWFeederMenu and mServModeWOWThicknessSensorStatus in 
                                StScreenEventMap[] for WOW diagnosis.
    03/27/2009  Jingwei Li      Added mServDiagWOWFeederMotorTest in StScreenEventMap[] for WOW diagnosis.
    03/12/2008  Raymond Shen    Added mServModeWOWOptions in StScreenEventMap[] for WOW diagnosis.
    10/24/2008  Raymond Shen    Added mKIPAdditionalServices in StScreenEventMap[].
    08/07/2008  Joey Cui    Merged stuffs from Janus to support PC proxy feature.
    07/18/2008  Raymond Shen    Added mWrnBatteryLow and mWrnBatteryEOL in StScreenEventMap[]
    03/10/2008  Oscar Wang  Added mErrPieceCountEOL and mErrPieceCountEOLWarning in StScreenEventMap[]
    10/10/2007  Adam Liu    Added mILinkRemoteRefill in StScreenEventMap[]
    10/08/2007  Raymond Shen    Removed mEServiceLogNearFull from StScreenEventMap[]
    08/28/2007  Joey Cui    Added mEServiceRecordManifesting in StScreenEventMap[]
    08/24/2007  Andy Mo     Added mEServiceManifestPowerOnRecoveryDeleteProgress and
                            mEServiceManifestPowerOnRecoveryUpdateProgress in StScreenEventMap[]
    08/15/2007  Oscar Wang  Add mEServiceCMOSLogFull in table StScreenEventMap[]
    08/09/2007  Oscar Wang  Add mErrOptionDisable in table StScreenEventMap[]
    08/07/2007  Joey Cui    Add mEServiceManifestLimitReached in table StScreenEventMap[]
    08/06/2007  Joey Cui    Add mRateEnterReplyMail in table StScreenEventMap[]
    08/03/2007  Joey Cui    Add mEServiceLogNearFull,mEServiceMaxNumberOfRecords,
                            mEServiceLowBarcodeWarning,mEServiceNoBarcodeAvailable
                            in table StScreenEventMap[]
    08/02/2007  Raymond Shen    Add mGermanEKPTaskSelectionMenu in table StScreenEventMap[]
    07/31/2007  Andy Mo     Added mGermanPieceID in the table of StScreenEventMap[]
    05/24/2007  Oscar Wang  Added utility function fnFindEvent().
    05/15/2007  Vincent Yi  Removed screen mErrAbacusTransactionFailed
    04/26/2007  Andy Mo     Added mMMDateCorrectionEnterAmount, mMMDateCorrectionError,mMMDateCorretionReady,
                            mMMDateCorrectionPrinting, mMMDateCorrectionTapePrinting
                            mMMManifestEnterAmount,mMMManifestReady,mMMManifestPrinting ,mMMManifestTapePrinting
                            in StScreenEventMap[]. 
    04/24/2007  Raymond Shen    Add mErrAbacusTransactionFailed in StScreenEventMap[].
    04/20/2007  Andy Mo     Added mMMPostageCorrectionEnterAmount,mMMPostageCorrectionPrintingComplete
                            and mMMPostageCorrectionInstruction in StScreenEventMap[]. 
    04/19/2007  Andy Mo     Added mMMPostageCorrectionReady,mMMPostageCorrectionError
                            in StScreenEventMap.
    04/10/2007  Raymond Shen    Added mAbacusOperatorLogonIDEnter into StScreenEventMap[].
    04/03/2007  Derek DeGennaro Added PSOC Please Wait Screen.
        03/30/2007  I. Le Goff      Add new screen mMMAjoutNextPieceTransient
    03/29/2007  Oscar Wang      Added rate change screens into StScreenEventMap[].
    03/29/2007  Vincent Yi      Added Abacus screens into StScreenEventMap[].
        03/27/2007  I. Le Goff      Rename mMMAjoutNextEnvelope into mMMAjoutNextPiece
        03/26/2007  I. Le Goff      Added Ajout main and printing screens into StEventAdjTable[] and StScreenEventMap[].
                              Modifiy StEventNextScreen[] for mode ajout
    03/22/2007  Adam Liu         Added Abacus Home and printing screens into StEventAdjTable[] and StScreenEventMap[].
    01/19/2007  Oscar Wang      Added mManufacturingNoPSD into StEventNextScreen[] and StScreenEventMap[].
    12/19/2006  Kan Jiang       Added mEServiceUploadRecordPrompt into StScreenEventMap[].
    12/19/2006  Vincent Yi      Added mErrWasteTankNearFull into StScreenEventMap[].
    12/14/2006  Adam Liu        Added mPBPDcapBackup,mPBPDcapUpdateReq,mPBPDcapUploadReq,
                                mPBPDcapUploadDue,mRateErrDataCaptureEngine into
                                StScreenEventMap[].
    12/12/2006  Oscar Wang      Added mKIPCheckPassword and mKIPEnterAmount into
                                StScreenEventMap[].
    12/11/2006  Kan Jiang       Added mOptionAutoDaylightSavingOccur into StScreenEventMap[]
    12/04/2006  Vincent Yi      Added mWrnTapePrintingPaused into StScreenEventMap[]
    11/15/2006  Oscar Wang      Modified function fnOITIsHighValueWarningActive
                                to fix misuse of high value warning screen.
    09/14/2006  Raymond Shen    Added mPrintOptionMain into StScreenEventMap[].
    09/12/2006  Vincent Yi      Modified StScreenEventMap[], StEventNextScreen[],
                                StEventAdjTable[] for permit tape print
    09/08/2006  Vincent Yi      Added midnight coming check in fnScreenTickTimerExpire()
    09/08/2006  Adam Liu        Added mMMPermitTapePrinting into StScreenEventMap[]
    09/07/2006  Vincent Yi      Added mSleepPreReady into StScreenEventMap[]
    09/06/2006  Vincent Yi      Added mErrMidnightAdvDateConfirm and mOptionAutoDateMsgConfirm
                                into StScreenEventMap[]
    09/04/2006  Raymond Shen    Added mScaleSetLocationCode into StScreenEventMap[]
    09/04/2006  Vincent Yi      Added mErrNetworkPowerOff into StScreenEventMap[]
    07/31/2006  Oscar Wang      Set DWM screens to OIT_LEFT_PRINT_RDY in mode 
                                table.
    07/17/2006  Vincent Yi      Add Diagnostics screens in to StScreenEventMap[]
    07/07/2006  Dicky Sun       Add mEServiceAnotherPackage in mode table.
    07/03/2006  Raymond Shen    Modified function fnOITCanEDMChangeParams().
    06/22/2006  Raymond Shen    Added mOOBPrinterMaintReplacePrintTestPat and
                                mOOBInstallNotComplete in mode table.
    05/11/2006  Steve Terebesi  Added StEventAdjTable and fnGetScreenEventById
    05/11/2006  Steve Terebesi  Fixed index initialization in fnmProcessEvent
    05/10/2006  Steve Terebesi  Merged from rearchitecture branch    
    05/05/2006  F.Z             Added fnOITGetTreeDirectory() to get the tree 
                                version info. for display purpose.
    03/21/2006  Steve Terebesi  Added fnmProcessEvent function
                                Removed code to start OIT_DEADMAN_TIMER
    03/17/2006  Steve Terebesi  Added StScreenEventMap, StEventNextScreen
                                Modified fnProcessModeChange,fnRequestNewMode,
                                    fnChangeScreen for OI/SYS split

-----------------------------------------------------------------------
    OLD PVCS REVISION HISTORY
    
    *    Rev 1.27   Jul 26 2005 04:03:52   cx17598
    * Rollback to previous previsos version, we do not need a new timer to handle alpha character input.
    * -Victor
    *************************************************************************/


/**********************************************************************
        Include Header Files
**********************************************************************/
#include <stdio.h>
#include <string.h>
#include "clock.h"
#include "nucleus.h"
#include "oit.h"
#include "oitpriv.h"
#include "oifields.h"
#include "oifunctfp.h"
#include "oiscreen.h"
#include "oiparm.h"
//#include "oirefill.h"
#include "global.h"
#include "keypad.h"
#include "lcd.h"
//#include "pmcitask.h"
#include "bob.h"
#include "sys.h"        /* this is temp only.  there are pmci message definitions in here that must be moved to pmcitask.h */
#include "errcode.h"
#include "printer.h"
#include "datdict.h"
#include "exception.h"  // logException()

//#include "ratetask.h"
#include "OitCm.h"
#include "pbplatform.h"
#include "debugTask.h"
#include "mmcimjet.h"
#include "version.h"
#include "fwrapper.h"
#include "lcdpriv.h"  // Kan added for compiling successfully
#include "oientry.h"
#include "flashutil.h"
#include "utils.h"

#define THISFILE "oitmsg.c"


//-----------------------------------------------------------------------------
// Prototypes for external functions that do not yet have a prototype in 
//  an include file.
//-----------------------------------------------------------------------------


/**********************************************************************
        Local Function Prototypes
**********************************************************************/
static void fnPreProcessOITMessage (const INTERTASK * pstCommand);
static BOOL fnProcessOITMessage(const OIT_MSG_TABLE_ENTRY * pMsgTable, 
                INTERTASK         * pMsg);
static void fnPostProcessOITMessage (const INTERTASK * pstCommand);

void fnChangeScreen(unsigned short wScreenNumber, BOOL fCallPreFn, BOOL fCallPostFn);
void fnChangeScreenAfterError(unsigned short wScreenNumber, BOOL fCallPreFn, BOOL fCallPostFn);
static void fnSetCurrentScreen(unsigned short wScreenNumber);

//static void fnProcessKey(unsigned char bKeyCode, unsigned char bDirection, unsigned char fControl);

//static void fnProcessModeChange(unsigned char bMode, unsigned char bSubMode);
static void fnDoModeAdjustments(unsigned short wScreen);
static void fnBuildFixedTextList(LCD_STRING **ppHead, LCD_STRING **ppTail);
static void fnBuildFixedGraphicList(LCD_STRING **ppHead, LCD_STRING **ppTail, BOOL fOnTop);
static void fnBuildFieldsList(LCD_STRING **ppHead, LCD_STRING **ppTail, LCD_CURSOR **ppCursor, BOOL fInit);
static void fnBuildFieldsListAfterError(LCD_STRING **ppHead, LCD_STRING **ppTail, LCD_CURSOR **ppCursor, BOOL fInit);
static void fnPaintCurrentScreenAfterError(BOOL fRefreshOnly);
static void fnBuildVarGraphicsList(LCD_STRING **ppHead, LCD_STRING **ppTail, BOOL fInit, BOOL fOnTop);
static BOOL fnProcessKeyOrEventMatch(unsigned char bKeyCode, INTERTASK *pIntertaskMsg, unsigned short wFnID,
                     unsigned char bNumNextScreens, unsigned short *pNextScreens,
                     BOOL fIsHardKey, BOOL fIgnorePre, BOOL fIgnorePost, BOOL fIgnoreNext);
static void fnOITInitializeTree( void );
static BOOL fnOITDestroyStrings(LCD_STRING *pStringsToDestroy);
static BOOL fnDoVersionCompare( UINT32 ulVersionA, UINT32 ulVersionB,
                                UINT32 ulVersionMask, UINT32 ulActualVersion );

static BOOL fnFinishScreenChange(void);

static char scratchpad100[ 100 ]; // Used for syslog entries.

/**********************************************************************
        Local Defines, Typedefs, and Variables
**********************************************************************/

#ifdef JANUS
// adjust the coordinates to character base instead of pixels. -Victor
/*
  #define   JANUS_ADJUST_COORDINATES(STRING)    do { \
  (STRING)->bXCoordinate /= 0x8; \
  (STRING)->bYCoordinate /= 0xf; \
  } while (0);
*/

// No adjustment for FP
#define JANUS_ADJUST_COORDINATES(STRING)
//  do {                   \
//  (STRING)->bXCoordinate /= 0x1; \
//  (STRING)->bYCoordinate /= 0x1; \
//} while (0);
#endif


/* this struct contains the task ID and message ID that is being waited for as a response to
   an OIT initiated message.  if the OIT message timer expires before we receive
   the message, it's an error.  fnSetMessageTimeout sets these globals. */
static MSGTO_WAIT_CTRL_BLOCK        stMsgTOWaitCtrl = { FALSE, 0, 0 };

/* flash component directories/records/pointers, etc. */
static unsigned char            *pTreeComponent;    /* pointer to the screen tree component in flash */
static unsigned char            *pLangComponent;    /* pointer to the current screen language component in flash */
static TREE_DIRECTORY           rTreeDir;           /* screen tree directory */
LANGUAGE_DIRECTORY      rLanguageDir;
static TREE_RECORD              rCurrentTreeRecord;
static FIXED_TEXT_RECORD        rCurrentFixedTextRecord;
static FIELDS_RECORD            rCurrentFieldsRecord;
static GENERIC_RECORD           rCurrentFixedGraphicRecord;
static GENERIC_RECORD           rCurrentVarGraphicRecord;
static unsigned short   wCurrentBGColor = FP_COLORLCD_USE_DEFAULT;
static unsigned short   wCurrentScreenID = 0;
static ELEMENT_DIRECTORY        rFixedTextDir;
static ELEMENT_DIRECTORY        rFieldsDir;
static ELEMENT_DIRECTORY        rTableTextDir;
static ELEMENT_DIRECTORY        rFixedGraphicDir;
static ELEMENT_DIRECTORY        rVarGraphicDir;
static unsigned short           wDiagnosticMessageData[3] = {0,0,0};    /* diagnostic response message data */
static unsigned char            *pHistogramData;

static BOOL         fModeChangePending = FALSE;
static BOOL         fAutoChangeOccurred = FALSE;

/* current mode information */
static UINT8        ucOITCurrentMode;       // bCurrentMode
static UINT8        ucOITCurrentSubMode;    // bCurrentSubMode

static UINT8    ucPendingModeChangeEvent;

// For adding Screen ID to syslog... add it every screen init, but only 
//  every nth screen refresh.            n = REFRESH_REMINDER_COUNT 
static uchar    bRefreshLogCount = 0;
#define REFRESH_REMINDER_COUNT  10


/**********************************************************************
        Global Functions Prototypes
**********************************************************************/



/**********************************************************************
        Global Variables
**********************************************************************/

extern pInitFn              aInitFns[];
extern pRefreshFn           aRefreshFns[];
extern pHKeyFn              aHKeyFns[];
extern pHKeyFn2ndFormat     aKeyFnsInFormat2[];
extern pEventFn2ndFormat    aEventFnsInFormat2[];
extern pEventFn             aEventFns[];
extern pPreFn               aPreFns[];
extern pPostFn              aPostFns[];
extern pGraphicInitFn       aGraphicInitFns[];
extern pGraphicRefreshFn    aGraphicRefreshFns[];
//extern const char cArrayLang[];
//extern const char cArrayScreen[];
//extern const OIT_MSG_TABLE_ENTRY pOITMsgTable[];
extern const size_t         OITEventIDMax;

extern unsigned char        bCursorPosition;    /* these two globals are used to communicate cursor info */
extern unsigned char        bCursorStyle;       /*  from the field functions to the field engine that calls */
/*  them.  this could have been done by passing a reference */
/*  parameter to every field function, but the cursor doesn't */
/*  apply to all fields so that would have been a waste. */

extern SetupParams          CMOSSetupParams;
extern unsigned short       usDcapActionStatus;

//extern const SCREEN_MODE_TABLE      pScreenModeMap[];   /* these are the mode tables found in oimode*.c */
//extern SCREEN_MODE_TABLE            pModeNextScreen[];
//extern const SCREEN_MODE_ADJ_TABLE  pModeAdjTable[];
extern LCD_STRING       *pIntertaskLCDStringXferI;
extern LCD_STRING       *pIntertaskLCDStringXferR;
extern unsigned long    lwLCDStringXferIDI;
extern unsigned long    lwLCDStringXferIDR;

/* keycodes in the digit group */
extern unsigned char *aDigitGroup;
extern unsigned char *aQwertyGroup;

unsigned long           lwNumPartitionsRcvd = 0;
unsigned short          wPreviousScreenID = 0;
KEY_MAP                 rKeyMap;

/* this struct is the control block for handling pre and post functions that have
   to be executed in steps because message responses are needed in between */
PREPOS_WAIT_CTRL_BLOCK      rWaitCtrl = { 0, 0, 0, NULL_PTR, NULL_PTR, FALSE };
BOOL                fIgnoreKeysAndEvents = FALSE;

KEY_BUFFER          rKeyBuffer = { FALSE, 0, 0, 0 };


unsigned char   bCurrentLanguage = 0;       /* the currently selected language */


BOOL fScreenTickEventQueued = FALSE;

unsigned short wCursorBGColor;
unsigned short wCursorFGColor;


//Screen event table: 
const SCREEN_EVENT_TYPE StScreenEventMap[] = 
  { // On the way to this screen... send this mode change event to sys.
    { mPowerOn,                         OIT_ENTER_POWERUP},
//    { mEServiceManifestPowerOnRecoveryDeleteProgress,   OIT_ENTER_POWERUP},
    
//    { mEServiceManifestPowerOnRecoveryUpdateProgress,   OIT_ENTER_POWERUP},
    { mErrInitializingFailed,           OIT_LEFT_PRINT_RDY},

    { mMMEnterPostage,                  OIT_LEFT_PRINT_RDY},

    { mMMIndiciaReady,                  OIT_ENTER_PRINT_RDY},
    { mMMIndiciaReadyTransient,         OIT_ENTER_PRINT_RDY},
    { mMMIndiciaPrinting,               OIT_PRINTING},
    { mMMIndiciaTapePrinting,           OIT_PRINT_TAPE},                                
    { mMMIndiciaError,                  OIT_LEFT_PRINT_RDY},
    { mMMIndiciaTransient,              OIT_LEFT_PRINT_RDY},
//    { mMMFeesDetails,                   OIT_LEFT_PRINT_RDY},

//    { mMMSealOnlyReady,                 OIT_PRINT_NO_INDICIA},
//    { mMMSealOnlyReadyTransient,        OIT_PRINT_NO_INDICIA},
//    { mMMSealOnlyError,                 OIT_LEFT_PRINT_RDY},
//    { mMMSealOnlyRunning,               OIT_RUNNING_SEAL},

//    { mMMTextAdDateTimeReady,               OIT_ENTER_PRINT_RDY},
//    { mMMTextAdDateTimeError,               OIT_LEFT_PRINT_RDY},
//    { mMMTextAdDateTimeDisabled,            OIT_LEFT_PRINT_RDY},
//    { mMMTextAdDateTimePrinting,            OIT_PRINTING},
//    { mMMTextAdDateTimeTapePrinting,        OIT_PRINT_TAPE},

//    { mMMPermitReady,                   OIT_PRINT_PERMIT },
//    { mMMPermitReadyTransient,          OIT_PRINT_PERMIT},
//    { mMMPermitPrinting,                OIT_RUNNING_PERMIT },
//    { mMMPermitTapePrinting,            OIT_PRINT_TAPE_PERMIT},
//    { mMMPermitError,                   OIT_LEFT_PRINT_RDY },
//    { mPermitSelection,                 OIT_LEFT_PRINT_RDY},
    
//    { mMMIndiciaAbacus,                 OIT_ENTER_PRINT_RDY },
//    { mMMIndiciaPrintingAbacus,         OIT_PRINTING },
//    { mMMIndiciaTapePrintingAbacus,         OIT_PRINT_TAPE},
//    { mMMIndiciaErrorAbacus,            OIT_LEFT_PRINT_RDY },
//    { mAbacusAccountIndexSelect,        OIT_LEFT_PRINT_RDY},
//    { mAbacusAccountPeriodEnded,        OIT_LEFT_PRINT_RDY},
//    { mAbacusAccountingSetupMenu,       OIT_LEFT_PRINT_RDY},
//    { mAbacusErrAccountingDiscrepancy3, OIT_LEFT_PRINT_RDY},
//    { mHostedAbacusAccountIndexSelect,  OIT_LEFT_PRINT_RDY},
        
//    { mEServiceAnotherPackage,          OIT_LEFT_PRINT_RDY},

//    { mReportsMenu,                     OIT_LEFT_PRINT_RDY},
//    { mReportPHPrintReady,              OIT_PREP_FOR_RPT_PRINTING},
//    { mReportPHPrinting,                OIT_RUNNING_REPORT},
//    { mReportPHTapePrinting,            OIT_PRINT_RPT_TO_TAPE},
//    { mReportPHPrintError,              OIT_LEFT_PRINT_RDY},
//    { mReportPHPrintComplete,           OIT_LEFT_PRINT_RDY},
//    { mReportPrintReadyTransient,       OIT_PREP_FOR_RPT_PRINTING},
//    { mReportPrepareNextGroup,          OIT_PREP_FOR_RPT_PRINTING},
//    { mReportPrepareNextTapeGroup,  OIT_PREP_FOR_RPT_PRINTING},

//    { mPmcDiagPrintTestPat,             OIT_PRINT_ENV_TEST},
//    { mPmcDiagTestPatReadyTransient,    OIT_PRINT_ENV_TEST},
//    { mPmcDiagPrintTestPatError,        OIT_LEFT_PRINT_RDY},
//    { mPmcDiagTestPatPrinting,          OIT_RUNNING_TEST},
//    { mPmcDiagTestPatTapePrinting,      OIT_PRINT_TAPE_TEST},
    { mTestPrintPatAcceptable,          OIT_LEFT_PRINT_RDY},

    { mAdSelection,                     OIT_LEFT_PRINT_RDY},        
    { mInscSelection,                   OIT_LEFT_PRINT_RDY},        
//    { mPresetSelectCustomPreset,        OIT_LEFT_PRINT_RDY},
//    { mPresetNoCustomPresets,           OIT_LEFT_PRINT_RDY},
//    { mPresetInvalidSettings,           OIT_LEFT_PRINT_RDY},
    { mErrFeederCoverOpen,              OIT_LEFT_PRINT_RDY},
    { mErrFeederError,                  OIT_LEFT_PRINT_RDY},
    { mErrFeederEnvelopeOut,            OIT_LEFT_PRINT_RDY},
    { mErrFeederFatalFault,             OIT_ERROR_METER},
    { mErrHighPostageExceeded,          OIT_LEFT_PRINT_RDY},
    { mErrInkOut,                       OIT_LEFT_PRINT_RDY},
    { mErrInkTank,                      OIT_LEFT_PRINT_RDY},
    { mErrInkTankExpiring,              OIT_LEFT_PRINT_RDY},
    { mErrInkTankLidOpen,               OIT_LEFT_PRINT_RDY},
    { mErrInsufficientFunds,            OIT_LEFT_PRINT_RDY},
    { mErrInvalidPostage,               OIT_LEFT_PRINT_RDY},
    { mErrJamLeverOpen,                 OIT_LEFT_PRINT_RDY},
    { mErrMeterError,                   OIT_ERROR_METER},
    { mErrMissingImage,                 OIT_LEFT_PRINT_RDY},   
    { mErrMeterErrorPowerOff,           OIT_ERROR_METER},
    { mErrNetworkPowerOff,              OIT_ERROR_METER},
    { mErrMeterUnauthorized,            OIT_LEFT_PRINT_RDY},
    { mErrMissingGraphic,               OIT_LEFT_PRINT_RDY},
    { mErrNoInkTank,                    OIT_LEFT_PRINT_RDY},
    { mErrNoPrinthead,                  OIT_LEFT_PRINT_RDY},
    { mMMIndiciaError,                  OIT_LEFT_PRINT_RDY},
    { mErrPrinterFault,                 OIT_ERROR_METER},
    { mErrPrinthead,                    OIT_LEFT_PRINT_RDY},
    { mWrnBatteryLow,               OIT_LEFT_PRINT_RDY},
    { mWrnBatteryEOL,               OIT_LEFT_PRINT_RDY},
    { mErrTapeErrors,                   OIT_LEFT_PRINT_RDY},    
    { mErrTopCoverOpen,                 OIT_LEFT_PRINT_RDY},
    { mErrWasteTankFull,                OIT_LEFT_PRINT_RDY},
    { mErrWasteTankNearFull,            OIT_LEFT_PRINT_RDY},
    { mErrMidnightAdvDateConfirm,       OIT_LEFT_PRINT_RDY},
    { mErrOptionDisabled,               OIT_LEFT_PRINT_RDY},
    { mErrWOWMailJammed,               OIT_LEFT_PRINT_RDY},
    { mErrSBRError,               OIT_LEFT_PRINT_RDY},
    { mErrWOWError,               OIT_LEFT_PRINT_RDY},
//    { mOptionAutoDateMsgConfirm,        OIT_LEFT_PRINT_RDY},
    { mInspectionRequired,              OIT_LEFT_PRINT_RDY},
    { mInspectionWarning,               OIT_LEFT_PRINT_RDY},
    { mManufacturingPSDDisabled,        OIT_LEFT_PRINT_RDY},
    { mManufacturingPSDMisMatch,        OIT_LEFT_PRINT_RDY},
    { mManufacturingNoPSD,      OIT_ENTER_MANUFACTURING},
    { mWrnTapePrintingPaused,           OIT_LEFT_PRINT_RDY},

//    { mLockEnterLockCode,               OIT_LEFT_PRINT_RDY},
//    { mLockEnterPassword,               OIT_LEFT_PRINT_RDY},
//    { mAbacusOperatorLogonIDEnter,      OIT_LEFT_PRINT_RDY},

    { mSleep,                           OIT_GOTO_SLEEP},
    { mSleepPreReady,                   OIT_GOTO_SLEEP},

    { mFunds,                           OIT_LEFT_PRINT_RDY},
//    { mMenuMainMenu,                    OIT_LEFT_PRINT_RDY},
//    { mPrintOptionMain,                 OIT_LEFT_PRINT_RDY},
//    { mPrinterMaint,                    OIT_LEFT_PRINT_RDY},
//    { mSetupMultipleTapes,              OIT_LEFT_PRINT_RDY},

//    { mRefill,                          OIT_LEFT_PRINT_RDY},
//    { mRefillEnterAmount,               OIT_LEFT_PRINT_RDY},
//    { mRefillSuccessful,                OIT_LEFT_PRINT_RDY},
//    { mInfraContactPB,                  OIT_LEFT_PRINT_RDY},

//    { mWeightRateOptionMain,                OIT_LEFT_PRINT_RDY},
//    { mRateErrNegativeWeight,               OIT_LEFT_PRINT_RDY},
//    { mRateErrWeightOverCapacity,           OIT_LEFT_PRINT_RDY},
//    { mRateSelectClass,                     OIT_LEFT_PRINT_RDY},
//    { mRateServices,                        OIT_LEFT_PRINT_RDY},
//    { mRatePromptZipZone,                   OIT_LEFT_PRINT_RDY},
//    { mRateCountrySelection,                OIT_LEFT_PRINT_RDY},
            
//    { mTextSelectTextMessage,               OIT_LEFT_PRINT_RDY},

//    { mAcctSelection,                       OIT_LEFT_PRINT_RDY},
//    { mRateErrWeightOverCapacityDiffWeight, OIT_LEFT_PRINT_RDY},
//    { mRateDiffPrompt,                      OIT_LEFT_PRINT_RDY},
//    { mRateDiffRemoveItem,                  OIT_LEFT_PRINT_RDY},
//    { mRateDiffLastItem,                    OIT_LEFT_PRINT_RDY},

//    { mOOBPrinterMaintReplacePrintTestPat,  OIT_LEFT_PRINT_RDY},
//    { mOOBInstallNotComplete,               OIT_LEFT_PRINT_RDY},
//    { mOOBPhoneDialingPrefixSetting,        OIT_LEFT_PRINT_RDY},

//    { mServModeMain,                        OIT_LEFT_PRINT_RDY},
//    { mServModeMainDiag,                    OIT_LEFT_PRINT_RDY},
//    { mServModePMDiag,                      OIT_LEFT_PRINT_RDY},
//    { mServModePMMotorDiag,                 OIT_ENTER_DIAG_MODE},
//    { mServDiagFeeder,                      OIT_ENTER_DIAG_MODE},
//    { mServModeWOWOptions,                  OIT_ENTER_DIAG_MODE},
//    { mServDiagWOWFeederMenu,               OIT_ENTER_DIAG_MODE},
//    { mServModeWOWThicknessSensorStatus,    OIT_ENTER_DIAG_MODE},
//    { mServDiagWOWSBROptions,               OIT_ENTER_DIAG_MODE},

//    { mScaleSetLocationCode,                OIT_LEFT_PRINT_RDY},
//    { mEServiceUploadRecordPrompt,           OIT_LEFT_PRINT_RDY},

//    { mOptionAutoDaylightSavingOccur,           OIT_LEFT_PRINT_RDY},

    { mKIPCheckPassword,             OIT_LEFT_PRINT_RDY},
    { mKIPEnterAmount,           OIT_LEFT_PRINT_RDY},

//    { mPBPDcapBackup,                       OIT_LEFT_PRINT_RDY},
//    { mPBPDcapUpdateReq,                    OIT_LEFT_PRINT_RDY},
//    { mPBPDcapUploadReq,                    OIT_LEFT_PRINT_RDY},
//   { mPBPDcapUploadDue,                    OIT_LEFT_PRINT_RDY},
//    { mRateErrDataCaptureEngine,            OIT_LEFT_PRINT_RDY},
//    { mReportAccounts,                      OIT_LEFT_PRINT_RDY},
//    { mAcctSelectionToDo,                 OIT_LEFT_PRINT_RDY},    should be in this table ... but AcctSelectionToDo is NOT Exported
//                                                                  not really an issue since the clear keys on the "Print Single Account
//                                                                  Report" goes to "Select Account To Report" which goes to "Accounts"
//                                                                  screen not "Account Reports" screen. Therefore ... have to go through 
//                                                                  Left Print ready event sometime before getting back to printing a report
//    { mEServicePromptReceipt,               OIT_LEFT_PRINT_RDY},

//    { mMMAjoutReady,          OIT_ENTER_PRINT_RDY},
//    { mMMAjoutPrinting,         OIT_PRINTING},
//    { mMMAjoutTapePrinting,       OIT_PRINT_TAPE},
//    { mMMAjoutError,          OIT_LEFT_PRINT_RDY},
//    { mMMAjoutNextPiece,          OIT_ENTER_PRINT_RDY},
//    { mMMAjoutNextPieceTransient,    OIT_LEFT_PRINT_RDY},
//    { mAjoutConfirmExit,          OIT_LEFT_PRINT_RDY},
//    { mAjoutEnterBatch,          OIT_LEFT_PRINT_RDY},
//    { mAjoutEnterOldValue,          OIT_LEFT_PRINT_RDY},
//    { mRateNewActive,               OIT_LEFT_PRINT_RDY},
//    { mRateNewInstalled,                OIT_LEFT_PRINT_RDY},
//    { mRateNewPending,              OIT_LEFT_PRINT_RDY},

    { mPSOCPleaseWait , OIT_LEFT_PRINT_RDY},

   //Andy added below to support Canada printing mode   
//    { mMMPostageCorrectionReady,                OIT_ENTER_PRINT_RDY},
//    { mMMPostageCorrectionError,                OIT_LEFT_PRINT_RDY},
//    { mMMPostageCorrectionPrinting,             OIT_PRINTING},
//    { mMMPostageCorrectionTapePrinting,         OIT_PRINT_TAPE},
//    { mMMCorrectionPrintingComplete,        OIT_LEFT_PRINT_RDY},
//    { mMMPostageCorrectionEnterAmount,          OIT_LEFT_PRINT_RDY},
//    { mMMPostageCorrectionInstruction,          OIT_LEFT_PRINT_RDY},

//    { mMMDateCorrectionReady,               OIT_ENTER_PRINT_RDY},
//    { mMMDateCorrectionError,               OIT_LEFT_PRINT_RDY},
//    { mMMDateCorrectionPrinting,            OIT_PRINTING},
//    { mMMDateCorrectionTapePrinting,        OIT_PRINT_TAPE},
//    { mMMDateCorrectionChoice,              OIT_LEFT_PRINT_RDY},
 // { mMMDateCorrectionAddDays,             OIT_LEFT_PRINT_RDY},

//    { mMMManifestReady,             OIT_ENTER_PRINT_RDY},
//    { mMMManifestError,             OIT_LEFT_PRINT_RDY},
//    { mMMManifestPrinting,          OIT_PRINTING},
//    { mMMManifestTapePrinting,      OIT_PRINT_TAPE},
//    { mMMManifestEnterAmount,               OIT_LEFT_PRINT_RDY},

//    { mGermanPieceID,                OIT_LEFT_PRINT_RDY},
//    { mGermanEKPTaskSelectionMenu,          OIT_LEFT_PRINT_RDY},
    
//    { mEServiceMaxNumberOfRecords,      OIT_LEFT_PRINT_RDY},
//    { mEServiceLowBarcodeWarning,       OIT_LEFT_PRINT_RDY},
//    { mEServiceNoBarcodeAvailable,      OIT_LEFT_PRINT_RDY},
//    { mEServiceManifestLimitReached,      OIT_LEFT_PRINT_RDY},
//    { mEServiceCMOSLogFull,      OIT_LEFT_PRINT_RDY},
        
//    { mRateEnterReplyMail,      OIT_LEFT_PRINT_RDY},
//    { mEServiceRecordManifesting,      OIT_LEFT_PRINT_RDY},

//    { mILinkRemoteRefill,       OIT_LEFT_PRINT_RDY},
    { mErrPieceCountEOL,       OIT_LEFT_PRINT_RDY},
    { mErrPieceCountEOLWarning,       OIT_LEFT_PRINT_RDY},

    { mKIPAdditionalServices,               OIT_LEFT_PRINT_RDY},
//   { mInfraCallErrGeneralRefillSuccess,    OIT_LEFT_PRINT_RDY},

    { END_OF_SCREEN_MODE_TABLE, 0} 
  };


// Event to Screen table:
// In this table, the events are left alone, but the ulScreenIDs are modified  
//  by fnDoModeAdjustments() and by fnRequestNewMode(). When a change mode 
//  request event comes along, the function fnRequestNewMode finds the event
//  in this table, and stores the next screen in that entry, then sends the 
//  mode change request to the systask.  When the systask responds with the 
//  same change mode request event, the event is found in this table, and the 
//  saved screen is used in the ChangeScreen function.
// There should not be more than 1 entry for each event. The entries do not 
//  have to be in any particular order.
static SCREEN_EVENT_TYPE StEventNextScreen[] =
  { // ulScreenID,                      ucEvent                 
    { 0,                                OIT_ENTER_POWERUP},
    { mMMIndiciaError,                  OIT_LEFT_PRINT_RDY},

    { 0,                                OIT_ENTER_PRINT_RDY},
    { 0,                                OIT_PRINT_PERMIT},
    { 0,                                OIT_PRINT_NO_INDICIA},
    { 0,                                OIT_PRINT_ENV_TEST},
    { 0,                                OIT_PREP_FOR_RPT_PRINTING},

    { mMMIndiciaPrinting,               OIT_PRINTING},
    { mMMIndiciaTapePrinting,           OIT_PRINT_TAPE},                                

    { 0,               OIT_RUNNING_SEAL},

    { 0,                OIT_RUNNING_PERMIT},
    { 0,            OIT_PRINT_TAPE_PERMIT},
        
    { 0,          OIT_RUNNING_TEST},
    { 0,      OIT_PRINT_TAPE_TEST},

    { 0,                OIT_RUNNING_REPORT},
    { 0,            OIT_PRINT_RPT_TO_TAPE},

    { mErrMeterError,                   OIT_ERROR_METER},
    { 0,                                OIT_GOTO_SLEEP},
    { 0,                                OIT_ENTER_POWERDOWN},

    { 0,                  OIT_ENTER_DIAG_MODE},
    {mManufacturingNoPSD,                OIT_ENTER_MANUFACTURING},
    { 0,              OIT_LOCK_EDM},

// These will never be seen, because the events are duplicates.
//     { mMMAjoutPrinting,         OIT_PRINTING},
//    { mMMAjoutTapePrinting,       OIT_PRINT_TAPE},

    { END_OF_SCREEN_MODE_TABLE, 0 } 
  };

// Adjustment table:
//  There may be more than one entry for each screen (in first column), but 
//   there should not be more than 1 entry for any screen/event combo. 
const SCREEN_EVENT_ADJ_TABLE    StEventAdjTable[] =
  /* when we go to this screen...,   ... map this event,      ... to this screen */
  {// ulScreenID,                       ucEvent,                ulNewScreenID
    { mMMIndiciaReady,                  OIT_PRINTING,           mMMIndiciaPrinting  },
    { mMMIndiciaReady,                  OIT_PRINT_TAPE,         mMMIndiciaTapePrinting},

//    { mMMTextAdDateTimeReady,           OIT_PRINTING,           mMMTextAdDateTimePrinting },
//    { mMMTextAdDateTimeReady,           OIT_PRINT_TAPE,         mMMTextAdDateTimeTapePrinting},

//    { mMMIndiciaAbacus,                 OIT_PRINTING,           mMMIndiciaPrintingAbacus},
//    { mMMIndiciaAbacus,                 OIT_PRINT_TAPE,         mMMIndiciaTapePrintingAbacus},

//    { mMMAjoutReady,          OIT_PRINTING,     mMMAjoutPrinting  },
//    { mMMAjoutReady,          OIT_PRINT_TAPE,     mMMAjoutTapePrinting},
    
//    { mMMAjoutNextPiece,          OIT_PRINTING,     mMMAjoutPrinting  },
//    { mMMAjoutNextPiece,          OIT_PRINT_TAPE,     mMMAjoutTapePrinting},

//    { mMMPermitReady,                 OIT_PRINTING,           mMMPermitPrinting },
//    { mMMPermitReady,                 OIT_PRINT_TAPE,         mMMPermitTapePrinting},

//Andy added below to support Canada printing mode
//    { mMMPostageCorrectionReady,            OIT_PRINTING,           mMMPostageCorrectionPrinting },
//    { mMMPostageCorrectionReady,            OIT_PRINT_TAPE,         mMMPostageCorrectionTapePrinting},
//    { mMMDateCorrectionReady,               OIT_PRINTING,           mMMDateCorrectionPrinting },
//    { mMMDateCorrectionReady,               OIT_PRINT_TAPE,         mMMDateCorrectionTapePrinting},
//    { mMMManifestReady,                     OIT_PRINTING,           mMMManifestPrinting },
//    { mMMManifestReady,                     OIT_PRINT_TAPE,         mMMManifestTapePrinting},
 // 2009.09.17 added by Jingwei,Li for Canada Package Service
//    { mEServicePrintTape,                     OIT_PRINT_TAPE,         mMMIndiciaTapePrinting},
    { END_OF_SCREEN_MODE_TABLE, 0, 0} 
  };



extern unsigned char UcLastEvent;

typedef struct {
  unsigned char ucMode;
  unsigned char ucSubmode;
  unsigned char ucEvent;
} EVENT_CONVERT_TYPE;

// If we get a mode change request from SYS, convert the requested
//  mode/submode to the mode-change event.  The event is used to
//  look up the next screen in StEventNextScreen[].
const EVENT_CONVERT_TYPE UcConvertEvent[] = 
{
  {OIT_POWERUP,             OIT_NO_SUBMODE,      OIT_ENTER_POWERUP         },
  {OIT_PRINT_NOT_READY,         OIT_NO_SUBMODE,      OIT_LEFT_PRINT_RDY        },
  //{OIT_SERVICE,               OIT_NO_SUBMODE,      OIT_ENTER_SERVICE         },
  {OIT_PRINT_READY,             OIT_INDICIA,         OIT_ENTER_PRINT_RDY       },       
  {OIT_PRINT_READY,             OIT_SEAL_ONLY,       OIT_PRINT_NO_INDICIA      },
  {OIT_PRINT_READY,             OIT_TEST_PATTERN,    OIT_PRINT_ENV_TEST        },
  {OIT_PRINT_READY,             OIT_REPORT,          OIT_PREP_FOR_RPT_PRINTING },
  {OIT_RUNNING,                 OIT_INDICIA,         OIT_PRINTING              },
  {OIT_PRINTING_TAPE,       OIT_INDICIA,         OIT_PRINT_TAPE            },
  {OIT_PRINTING_TAPE,       OIT_TEST_PATTERN,    OIT_PRINT_TAPE_TEST       },
  {OIT_PRINTING_TAPE,       OIT_REPORT,          OIT_PRINT_RPT_TO_TAPE     },
  //{OIT_POST_OFFICE_TEST_MODE, OIT_PO_TRIP,         OIT_POST_OFF_TEST         },
  {OIT_PRINT_NOT_READY,     OIT_EDM_LOCK,        OIT_LOCK_EDM              },
  //{OIT_DIAGNOSTICS,           OIT_NO_SUBMODE,      OIT_ENTER_DIAGNOSTICS     },
  //{OIT_ERROR_HANDLING,        OIT_JAM,             OIT_ERROR_JAM             },
  //{OIT_ERROR_HANDLING,        OIT_SKEW,            OIT_ERROR_SKEW            },
  {OIT_METER_ERROR,             OIT_NO_SUBMODE,      OIT_ERROR_METER           },
  //{OIT_USER_INIT_MAINTENANCE, OIT_MAINT_USER,      OIT_INIT_MAINT_USER       },
  //{OIT_USER_INIT_MAINTENANCE, OIT_MAINT_OOB,       OIT_INIT_MAINT_OOB        },
  //{OIT_USER_INIT_MAINTENANCE, OIT_MAINT_PH_CHANGE, OIT_INIT_MAINT_PH         },
  //{OIT_USER_INIT_MAINTENANCE, OIT_MAINT_INK_INS,   OIT_INIT_MAINT_INK        },
  //{OIT_PMC_INIT_MAINTENANCE,  OIT_NO_SUBMODE,      OIT_MAINT_PMC             },
//Ram added to disable sleep
//  {OIT_SLEEP,               OIT_NORMAL_SLEEP,    OIT_GOTO_SLEEP            },
  {OIT_MANUFACTURING,       OIT_NO_SUBMODE,      OIT_ENTER_MANUFACTURING   },
  //{OIT_DATA_CENTER,           OIT_NO_SUBMODE,      OIT_ENTER_DATACENTER      },
  //{OIT_SYS_GET_PMCI_STATUS
  {OIT_PRINT_READY,           OIT_PERMIT,          OIT_PRINT_PERMIT          },
//Ram added to disable sleep
//  {OIT_SLEEP,                 OIT_SOFT_POWER_DOWN, OIT_ENTER_POWERDOWN       },
  {OIT_RUNNING,               OIT_PERMIT,          OIT_RUNNING_PERMIT        },
  {OIT_RUNNING,               OIT_SEAL_ONLY,       OIT_RUNNING_SEAL          },
  {OIT_RUNNING,               OIT_REPORT,          OIT_RUNNING_REPORT        },
  {OIT_RUNNING,               OIT_TEST_PATTERN,    OIT_RUNNING_TEST          }
};





/**********************************************************************
        Public Functions
**********************************************************************/

/* OIT Task */
/* *************************************************************************
// FUNCTION NAME: 
//      fnOperatorInterfaceTask
//
// DESCRIPTION: 
//      High level control of the operator interface task.  The OS entry
//          point for the OIT task.
//
// INPUTS:
//      pstCommand  - the input message
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  6/12/1998   Joe Mozdzer     Initial Version
//  1/04/2006   Vincent Yi      Code cleanup 
//
// *************************************************************************/
void fnOperatorInterfaceTask (unsigned long argc, void *argv)
{
  INTERTASK     stCommand;      /* input message command from another task */

   fnSysLogTaskStart( "OITask", OIT );  
  /* turn on the keypad */
  fnEnableKeys();

  // Clear main menu event to avoid lock problem for test pattern printing 
  // for OOB, and possibly other cases that did not go to ready screen
  (void)OSSetEvents( OIT_EVENT_GROUP, (unsigned long) MAIN_MENU_CHK_END_EVT, OS_OR );

  /* initialize to powerup mode.  a screen does not get put up until we 
     receive the initialize request. */
  ucOITCurrentMode = OIT_POWERUP;
  ucOITCurrentSubMode = OIT_NO_SUBMODE;

  // if Language id is invalid, reset it
  if(CMOSSetupParams.Language >= fnFlashGetNumLanguages())
    {
      CMOSSetupParams.Language = 0;
    }

  /* set up the tree and language pointers */
  fnOITInitializeTree();
  bCurrentLanguage = CMOSSetupParams.Language;
  fnOITInitializeLanguage(bCurrentLanguage);

  fnSysLogTaskLoop( "OITask", OIT );  
  /* INTERTASK MESSAGE HANDLER */
  /* Continually wait for and process input messages */
  while (1)
    {
      /* wait indefinitely for a message */
      if (OSReceiveIntertask (OIT, &stCommand, OS_SUSPEND) == SUCCESSFUL)
        {
          fnPreProcessOITMessage (&stCommand);

          /* now process the message */
          (void) fnProcessOITMessage( fnGetOITMessageTable(), &stCommand ) ;
              
          fnPostProcessOITMessage (&stCommand);

        }
    }
}
// end fnOperatorInterfaceTask

/* Timer Expiration Routines */

/* **********************************************************************
// FUNCTION NAME: fnScreenChangeTimerExpire
// DESCRIPTION:  Timer expiration routine that occurs 2 seconds after we
//                  change to a new screen.  Some screens automatically
//                  time out to another screen and this timer is used as
//                  the trigger.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none used
// **********************************************************************/
void fnScreenChangeTimerExpire(unsigned long lwArg)
{
  /* put a message in the OIT queue indicating that the timer expired */
  (void)OSSendIntertask (OIT, OIT, OIT_SCREEN_CHG_TMR_EXPIRED, NO_DATA, NULL_PTR, 0);

  /* stop this timer so it doesn't automatically retrigger */
  OSStopTimer(SCREEN_CHANGE_TIMER);

  return;
}

/* **********************************************************************
// FUNCTION NAME: fnScreenTickTimerExpire
// DESCRIPTION:  Timer expiration routine that occurs every 1 second all
//                  of the time.  This is a general purpose timer used
//                  by the OI for polling or other reasons.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none used
// **********************************************************************/
void fnScreenTickTimerExpire(unsigned long lwArg)
{
  static SINT32     lTick = 0;
  static SINT32     lLastMidnightTick = 0;
  
  lTick++;
  if (fnIsCloseToMidnightCrossover() == TRUE)
    {
      if (lTick - lLastMidnightTick > TIME_AHEAD_MIDNIGHT)
        {   // Only one event triggered before every midnight
          lLastMidnightTick = lTick;
          (void)OSSendIntertask( OIT, OIT, OIT_MIDNIGHT_COMING, NO_DATA, NULL_PTR, 0 );
        }
    }
  
  if (!fScreenTickEventQueued)
    {
      fScreenTickEventQueued = TRUE;
      
      /* put a message in the OIT queue indicating that the timer expired */
//      (void)OSSendIntertask( OIT, OIT, OIT_SCREEN_TICK_OCCURRED, NO_DATA, NULL_PTR, 0 );
    }
  
  
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnSavePresetFFSTickTimerExpire
// DESCRIPTION:  Timer expiration routine that occurs every 1 second all
//                  of the time.  This is a general purpose timer used
//                  by the OI for polling or other reasons.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none used
// **********************************************************************/
void fnShortTickTimerExpire(unsigned long lwArg)
{
  /* put a message in the OIT queue indicating that the timer expired */
  (void)OSSendIntertask( OIT, OIT, OIT_SHORT_TICK_OCCURRED, NO_DATA, NULL_PTR, 0 );
  (void)OSStopTimer( SHORT_TICK_TIMER );

  return;
}


/* **********************************************************************
// FUNCTION NAME: fnUserTickExpire
// DESCRIPTION:  Timer expiration routine that is set by user to reset
//               main screen to normal default if no process is made after
//               that period of time
//
// AUTHOR: Maya Mansjur
//
// INPUTS:  none used
// **********************************************************************/
void fnUserTickExpire(unsigned long lwArg)
{
  /* put a message in the OIT queue indicating that the timer expired */
  (void)OSSendIntertask( OIT, OIT, OIT_USER_TICK_OCCURRED, NO_DATA, NULL_PTR, 0 );
    
  /* stop this timer so it doesn't automatically retrigger */
  (void)OSStopTimer( USER_TICK_TIMER );

  return;
}

/* **********************************************************************
// FUNCTION NAME:       fnOITDeadmanExpire
// DESCRIPTION:  
/       Timer expiration routine that occurs when we are waiting
//      for a change mode message from the system controller
//      but did not receive one in time.  An error message is
//      displayed on the screen.
//
// INPUTS:  none used
// NOTES:
//  1. The timer that triggers this by expiring is the OIT_DEADMAN_TIMER.
//
// HISTORY:
//  2011.07.20 Clarisa Bellamy - Put an entry in the syslog when this timer 
//                      expires. Cleanup comments.
//  ??? This used to be a display error direct if the "OIT did not receive
//      mode change from SYS" message.  It has been changed to use the
//      central error handler so that customer's can be instructed to
//      power cycle the machine to recover. */
// AUTHOR: Joe Mozdzer
// **********************************************************************/
void fnOITDeadmanExpire(unsigned long lwArg)
{
    // Put an entry in the syslog that indicates which unanswered requrest
    //  for a mode change triggered this timer.
    sprintf( scratchpad100, "OitModeChgTimeout: Event=%d", ucPendingModeChangeEvent );
    fnSystemLogEntry( SYSLOG_TEXT, scratchpad100, strlen(scratchpad100) );             
    
    // For now, allow the user to "clear" the error, by pressing a key.
    //  The meter will attempt to go back to the main screen.
    // At this point, Janus forces a reboot.
    fnReportMeterError( ERROR_CLASS_OIT, ECOIT_NO_MODE_CHANGE_FROM_SYS );
    return;
}

/* **********************************************************************
// FUNCTION NAME: fnOITMessageExpire
// DESCRIPTION:  Timer expiration routine that occurs when we are waiting
//                  for a message response from some other task, but never
//                  get it.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none used
// **********************************************************************/
void fnOITMessageExpire(unsigned long lwArg)
{
#define	MAX_STRINGLEN		150
  unsigned char bErrorCode;
  char          pLogBuf[MAX_STRINGLEN+1];

  switch (stMsgTOWaitCtrl.bMsgTimeoutExpectedTaskID)
    {
        case SCM:
          bErrorCode = ECOIT_NO_RESPONSE_BOB;
          break;
    #if 0
        case PMCI:
          bErrorCode = ECOIT_NO_RESPONSE_PMCI;
          break;
    #endif

        default:
          bErrorCode = ECOIT_NO_RESPONSE_OTHER;
          break;
    }

  char *pIDName = fnMsgIDToName(stMsgTOWaitCtrl.bMsgTimeoutExpectedMsgID);
  snprintf(pLogBuf,MAX_STRINGLEN, "OI Msg Timeout: Task=%02X ID=%02X",
          stMsgTOWaitCtrl.bMsgTimeoutExpectedTaskID,
          stMsgTOWaitCtrl.bMsgTimeoutExpectedMsgID);

  fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
  snprintf(pLogBuf,MAX_STRINGLEN, "OI Msg TimO: Tsk=%s ID=%s",
          syslogSourceDestToString(stMsgTOWaitCtrl.bMsgTimeoutExpectedTaskID),
          pIDName);
  fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));



  fnReportMeterError(ERROR_CLASS_OIT, bErrorCode);
  stMsgTOWaitCtrl.fWaiting = FALSE;
  (void)OSStopTimer( OIT_MESSAGE_TIMER );

  return;
}


/* Utility functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetPrePostWaitCtrlBlock
//
// DESCRIPTION: 
//      Accessor function to return rWaitCtrl pointer
//
// INPUTS:
//      None.
//
// RETURNS:
//      Pointer to rWaitCtrl
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  8/1/2005    Vincent Yi  Initial version
//
// *************************************************************************/
PREPOS_WAIT_CTRL_BLOCK * fnGetPrePostWaitCtrlBlock(void)
{
  return (&rWaitCtrl);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnDisplayFirstScreen
//
// DESCRIPTION: 
//      Displaying the first screen after initializing.
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/06/2005  Vincent Yi      Initial version
//
// *************************************************************************/
void fnDisplayFirstScreen(void)
{
  fnChangeScreen(GetScreen(SCREEN_POWER_ON), TRUE, FALSE);
}

/* **********************************************************************
// FUNCTION NAME: fnSetMessageTimeout
// DESCRIPTION: Sets up a timeout on an expected message response.  This
//              amounts to saving expected message information in global
//              variables and starting the OIT_MESSAGE_TIMER with the
//              appropriate timeout value.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  bRespTaskID, bRespMsgID - task ID and message ID of the expected
//                                      response.
//          lwMilliTimeout - number of milliseconds to wait
//
// OUTPUTS: none
// *************************************************************************/
void fnSetMessageTimeout(unsigned char bRespTaskID, unsigned char bRespMsgID,
             unsigned long lwMilliTimeout)
{

  /* save expected message parameters in a global struct */
  stMsgTOWaitCtrl.bMsgTimeoutExpectedTaskID = bRespTaskID;
  stMsgTOWaitCtrl.bMsgTimeoutExpectedMsgID = bRespMsgID;
  stMsgTOWaitCtrl.fWaiting = TRUE;

  /* set the timer duration and start the timer */
  (void)OSChangeTimerDuration( OIT_MESSAGE_TIMER, lwMilliTimeout );
  (void)OSStartTimer( OIT_MESSAGE_TIMER );
}

/* **********************************************************************
// FUNCTION NAME:           fnProcessModeChange
// DESCRIPTION: 
//      Processes a change mode command received from the system controller.
//
// Notes:
//  1. When mode changes are requested, the id of the screen is saved in 
//     the EventToScreen table then the request is sent to systask, and
//     the flag is set to ignore keys and EVENTS.  When the mode change
//     request comes from the systask, then this function converts it to
//     the mode change event, and retrieves the next screen from the 
//     EventToScreen table.
// HISTORY:
//  2011.07.20 Clarisa Bellamy -Added check for mode/submode not in conversion-
//                      to-event table, and for event not in event-to-screen
//                      table.  If not found in table, put error in syslog, 
//                      and in exception.log FFS file, and report a meter error.
//                      (Before, it would attempt to change to a non-existant 
//                      screen.) 
//                      - Stop DeadMan timer.
// AUTHOR: Joe Mozdzer
// *************************************************************************/
void fnProcessModeChange(unsigned char bMode, unsigned char bSubMode)
{
  UINT8     loop, loopEnd;
  UINT8     j = 0;
  UINT8     ucEvent;
  BOOL      fScreenFound = FALSE;

    loopEnd = ARRAY_LENGTH( UcConvertEvent );
    for( loop = 0; loop < loopEnd; loop++)
    {
        if(   (UcConvertEvent[ loop ].ucMode == bMode) 
           && (UcConvertEvent[ loop ].ucSubmode == bSubMode) )
        {
            // Convert mode/submode to an event.
            ucEvent = UcConvertEvent[ loop ].ucEvent;

            // Find the event in the EventToScreen table.
            j = 0;
            while (StEventNextScreen[j].ulScreenID != END_OF_SCREEN_MODE_TABLE)
            {
                if (ucEvent == StEventNextScreen[j].ucEvent)
                {
                    UcLastEvent = ucEvent;
                    fScreenFound = TRUE;
                    break;
                }
                else
                {
                  j++;
                }
            }
            break;
        }
    }
     
    if( fScreenFound == FALSE )
    {
        if( loop == loopEnd )
        {
            sprintf( scratchpad100, 
                    "ERROR! Mode x%X.x%X not in UcConvertEvent (mode-to-evetn) Table", 
                    bMode, bSubMode );
            fnDumpStringToSystemLog( scratchpad100 );
            logException( scratchpad100 );
            fnReportMeterError( ERROR_CLASS_OIT, ECOIT_MISSING_TABLE_ENTRY );
        }
        else 
        {
            sprintf( scratchpad100, "ERROR! Evnt #x%X not in EvntToScr Table", ucEvent );
            fnDumpStringToSystemLog( scratchpad100 );
            logException( scratchpad100 );
            fnReportMeterError( ERROR_CLASS_OIT, ECOIT_MISSING_TABLE_ENTRY );
        }
    }


    // Stop the deadman timer since we will not ignore keys or events anymore.
    (void)OSStopTimer(OIT_DEADMAN_TIMER);
    fIgnoreKeysAndEvents = FALSE; /* keys & events can once again be processed */

    if( fScreenFound == TRUE )
    {
        // If the next screen was found in the table, change to that screen now.
        if (fModeChangePending == TRUE)
        {
            // OIT requested the mode change- no need to call pre/post fns again.
            fnChangeScreen(StEventNextScreen[j].ulScreenID, FALSE, FALSE);
        }
        else
        {
            // Mode change was unsolicited- call the pre/post functions
            fnChangeScreen(StEventNextScreen[j].ulScreenID, TRUE, TRUE);
        }
        fModeChangePending = FALSE;
    }

    return;
}



/* *************************************************************************
// FUNCTION NAME:           fnmProcessEvent
// DESCRIPTION: 
//      This function is responsible for completing the steps necessary for
//      processing a received event request..
// INPUTS:
//      pIntertask      - Pointer to intertask message (for input)
// RETURNS:
//      SUCCESSFUL      - function completed 
//
// WARNINGS/NOTES:
//  1. When mode changes are requested, the id of the screen is saved in 
//     the EventToScreen table, then the OIT_EVENT request is sent to systask, 
//     and the fIgnoreKeysAndEvents flag is set.  When the OIT_REC_EVENT msg
//     comes from the systask, then this function retrieves the next screen
//     from the EventToScreen table, then clears the fIgnoreKeysAndEvents flag, 
//     and changes to that screen. 
//  2. If the event is not in the table, put a message in the system log.  Then 
//     stay on this screen.  Whether or not this event was expected, the  
//     the ModeChangePending flag is cleared, the fIgnoreKeysAndEvents flag is 
//     cleared and the the deadman timer is stopped.       
//
// MODIFICATION HISTORY:
//  2011.07.20 Clarisa Bellamy - Stop the deadman timer, since we are clearing 
//                            the modeChangePending condition.
//                          - If a mode change event is pending, but this received
//                            event is a different one, then do the same as if 
//                            it was expected, except call the pre-function for
//                            the next screen.
//                          - If there is no entry in the event-to-next-screen
//                            table, note it in the syslog and exception.log.
//                          - Add a syslog entry for each event received, to 
//                            indicate if it was expected or not.
//      3/21/2006   Steve Terebesi  Initial version
// *************************************************************************/
BOOL fnmProcessEvent(INTERTASK *pIntertask)
{
  UINT8 EvToScrIndex = 0;
  UINT8 ucEvent = pIntertask->IntertaskUnion.bByteData[0];

    // Find the entry in the event-to-screen table that matches this event.
    while( StEventNextScreen[EvToScrIndex].ulScreenID != END_OF_SCREEN_MODE_TABLE )
    {
        if (ucEvent == StEventNextScreen[EvToScrIndex].ucEvent)
        {
          break;
        }
        else
        {
          EvToScrIndex++;
        }
    }

    // Stop the deadman timer since the IgnoreKeysAndEvents flag is being cleared.
    (void)OSStopTimer( OIT_DEADMAN_TIMER );

    // Put an entry in the syslog. 
    if( fModeChangePending )
    {
        sprintf( scratchpad100, "OIT Rxd event=x%X, expectd event=x%X", 
                            ucEvent, ucPendingModeChangeEvent );
    }
    else
    {
        sprintf( scratchpad100, "OIT Rxd unsolicited event=x%X", ucEvent );
    }
    fnSystemLogEntry( SYSLOG_TEXT, scratchpad100, strlen(scratchpad100) );                 

    // Don't ignore keys or events anymore.
    fIgnoreKeysAndEvents = FALSE; 

    // If an entry was found for this event in the table...
    if( StEventNextScreen[EvToScrIndex].ulScreenID != END_OF_SCREEN_MODE_TABLE )
    {
        // Change to the screen indicated by (or last saved in) that entry. 
        if (fModeChangePending == TRUE)
        {
            if( ucPendingModeChangeEvent == ucEvent )
            {
                // We requested the mode change- no need to call pre/post fns again.
                fnChangeScreen(StEventNextScreen[EvToScrIndex].ulScreenID, FALSE, FALSE);
                // Once the mode change event comes back, "clear" tbe event.
                ucPendingModeChangeEvent = 0xFF;
            }
            else
            {
                // We requested a different mode change- no need to call post fn again.
                fnChangeScreen( StEventNextScreen[EvToScrIndex].ulScreenID, TRUE, FALSE );
            }
        }
        else
        {
            /* mode change was unsolicited- call the pre/post functions */
            fnChangeScreen(StEventNextScreen[EvToScrIndex].ulScreenID, TRUE, TRUE);
        }
    }
    else
    {
        // There is no entry for this event in the StEventNextScreen table:
        //  This MAY be a bug, or not.  Put entry in syslog, but not in the 
        //  exception.log FFS  file and don't report error on the screen.
        sprintf( scratchpad100, "ERROR? Event x%X not in EvntToScr Table", ucEvent );
        fnDumpStringToSystemLog( scratchpad100 );
        //logException( scratchpad100 );
        //fnReportMeterError( ERROR_CLASS_OIT, ECOIT_MISSING_TABLE_ENTRY );
    }

    fModeChangePending = FALSE;
    fIgnoreKeysAndEvents = FALSE; /* keys & events can once again be processed */

    return (SUCCESSFUL);
}



///* **********************************************************************
//// FUNCTION NAME: fnRequestNewMode
//// DESCRIPTION: Requests a mode change by sending a message to the system
////                controller.  Saves the ID# of the screen to go to if
////                the system controller approves the mode change.
//// AUTHOR: Joe Mozdzer
////
//// INPUTS:    bMode, bSubMode - new mode & submode.  there is a special constant to
////                                use for the submode if it is not applicable.
////
////            wNextScreen - screen to go to when the system controller approves
////                            the new mode request
////
//// OUTPUTS:   none
//// *************************************************************************/
//void fnRequestNewMode(unsigned char bMode, unsigned char bSubMode, unsigned short wNextScreen)
//{
//
//  unsigned char   pMsgData[2];    /* send 2 bytes- 1st is mode, 2nd is submode */
//  unsigned long   i;
//
//  /* send a change mode request to the system controller */
//  pMsgData[0] = bMode;
//  pMsgData[1] = bSubMode;
//  OSSendIntertask(SYS, OIT, OIT_NEW_MODE_REQ, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);
//  fIgnoreKeysAndEvents = TRUE;    /* don't process keys & events while in this transitional state */
//
//
//  /* start the deadman timer for a response from SYS */
////    OSStopTimer(OIT_DEADMAN_TIMER); /* stop the timer first so that it gets reset to its full duration */
////    OSStartTimer(OIT_DEADMAN_TIMER);
//  fModeChangePending = TRUE;
//
//  /* update the global table to save the next screen ID */
//  i = 0;
//  while (pModeNextScreen[i].wScreenID != END_OF_SCREEN_MODE_TABLE)
//  {
//      /* find the appropriate ram location in the table that stores the next screen for
//          the given mode/submode combo */
//      if ((pModeNextScreen[i].bMode == bMode) && (pModeNextScreen[i].bSubMode == bSubMode))
//      {
//          pModeNextScreen[i].wScreenID = wNextScreen;
//          break;
//      }
//      i++;
//  }
//
//  /* if the table did not contain an mode/submode entry that matches the one we want to go to,
//      this is a software error since all mode/submode combos should appear in the table */
//  if (pModeNextScreen[i].wScreenID == END_OF_SCREEN_MODE_TABLE)
//  {
//      fnDisplayErrorDirect("Attempt to change to a mode/submode that is missing from mode-to-next-screen table", 0, OIT);
//  }
//
//  return;
//
//}


/* **********************************************************************
// FUNCTION NAME: fnProcessKey
// DESCRIPTION: Processes a key up or key down notificaton received by the
//              OIT task from the key driver task.
// AUTHOR: Joe Mozdzer
// *************************************************************************/
void fnProcessKey(unsigned char bKeyCode, unsigned char bDirection, unsigned char fControl)
{
  unsigned char i, j;
  unsigned char bKeyCtrlData, bHardKeyID;
  unsigned char *pEntryPtr;
  unsigned char *pNextEntryPtr;
  unsigned short    wFnID, wNext1, wNext2;
  unsigned short    pNextScreens[MAX_NEXT_SCREENS];
  unsigned char bParmID;
  unsigned char bParmCompareType;
  unsigned short    wOperand;

  //    /* initialize the pNextScreens[] */
  memset (pNextScreens, 0, MAX_NEXT_SCREENS * sizeof(UINT16));

  if ((rWaitCtrl.lwMsgsAwaited == 0) && (fIgnoreKeysAndEvents == FALSE))    /* keys have no effect if we are waiting for a message */
    {
      pNextEntryPtr = (unsigned char *) rCurrentTreeRecord.pFirstKeyEntry;
      for (i = 0; i < rCurrentTreeRecord.bNumKeyEntries; i++)
        {
          if (rTreeDir.wComponentFormat >= 0x0200)
            {
              /* the first byte of the entry is the length of the entry.  use it
             to calculate the pointer to the beginning of the following entry. */
              pEntryPtr = pNextEntryPtr;
              pNextEntryPtr += *pEntryPtr++;
            }
          else
            pEntryPtr = ((unsigned char *) rCurrentTreeRecord.pFirstKeyEntry) + (i * TREE_ENTRY_SIZE);

          bKeyCtrlData = *pEntryPtr++;
          bHardKeyID = *pEntryPtr++;

          /* get the parameter compare options */
          bParmID = *pEntryPtr++;
          bParmCompareType = *pEntryPtr++;
          EndianAwareCopy(&wOperand, pEntryPtr, sizeof(unsigned short));
          pEntryPtr += 2;

          /* check if the screen entry uses the special digits group code */
          if (bHardKeyID == KEY_GROUP_DIGITS)
            {
              /* look if the key code is a member of the group */
              j = 0;
              while (aDigitGroup[j] != END_KEY_GROUP)
                {
                  /* if it is, then make it look like that's the exact key we were looking for */
                  if (aDigitGroup[j] == bKeyCode)
                    {
                      bHardKeyID = bKeyCode;
                      break;
                    }
                  j++;
                }
            }

          /* check if the screen entry uses the special qwerty group code */
          if (bHardKeyID == KEY_GROUP_QWERTY)
            {
              /* look if the key code is a member of the group */
              j = 0;
              while (aQwertyGroup[j] != END_KEY_GROUP)
                {
                  /* if it is, then make it look like that's the exact key we were looking for */
                  if (aQwertyGroup[j] == bKeyCode)
                    {
                      bHardKeyID = bKeyCode;
                      break;
                    }
                  j++;
                }
            }

          /* check if the keycode and shift requirements match an entry in the screen tree record */
          if ((bHardKeyID == bKeyCode) && ((fControl & SHIFT_KEY_MASK) == (bKeyCtrlData & SHIFT_KEY_MASK)))
            {
              /* make sure the direction matches */
              if (((bKeyCtrlData & DIRECTION_MASK) && (bDirection == KEYDOWN)) ||
                  (((bKeyCtrlData & DIRECTION_MASK) == 0) && (bDirection == KEYUP)))
                {
                  BOOL  fParmResult;

                  /* if the direction matches, do the parameter comparison */
                  if (fnCheckKeyOrEventParameter(bParmID, bParmCompareType,
                                                 wOperand, &fParmResult) != SUCCESSFUL)
                    fnReportMeterError(ERROR_CLASS_OIT, ECOIT_BAD_PARAMETER_CHECK);

                  /* if the parameter compare was true, we have a match */
                  if (fParmResult == TRUE)
                    {
                      unsigned char bNumNextScreens;
                      BOOL          fIgnorePre;
                      BOOL          fIgnorePost;
                      BOOL          fIgnoreNext;
                      BOOL          fContinueSearching;
                      BOOL          fScreenChanging;

                      /* it's a match, so process the key.  start by getting the function ID */
                      EndianAwareCopy(&wFnID, pEntryPtr, 2);
                      if (wFnID >= OITEventIDMax)
                      {// Bad function ID in EMD, so get out before there is a crash
                      	dbgTrace(DBG_LVL_ERROR,"Error: Invalid function ID %d in EMD", wFnID);
                      	break;
                      }
                      pEntryPtr += 2;

                      /* check the format of the tree component */
                      if (rTreeDir.wComponentFormat >= 0x0200)
                        {
                          /* if format is > 2.00, low nibble of the next screen control byte
                             contains the number of next screen choices, which is variable.
                             next screen control byte also contains ignore pre, ignore post,
                             ignore next screen and continue searching flags */
                          bNumNextScreens = (*pEntryPtr) & 0x0F;
                          fIgnorePre = (*pEntryPtr & 0x80) ? TRUE : FALSE;
                          fIgnorePost = (*pEntryPtr & 0x40) ? TRUE : FALSE;
                          fIgnoreNext = (*pEntryPtr & 0x20) ? TRUE : FALSE;
                          fContinueSearching = (*pEntryPtr++ & 0x10) ? TRUE : FALSE;
                          if (bNumNextScreens > MAX_NEXT_SCREENS)
                            bNumNextScreens = MAX_NEXT_SCREENS;
                          EndianAwareArray16Copy(pNextScreens, pEntryPtr, bNumNextScreens * sizeof(unsigned short));
                        }
                      else
                        {
                          /* if format < 2.00, the ignore pre, post, and next options are always
                             false because the features were not available then */
                          fIgnorePre = FALSE;
                          fIgnorePost = FALSE;
                          fIgnoreNext = FALSE;
                          fContinueSearching = FALSE;

                          /* if format is < 2.00, there are always 2 next screen choices */
                          bNumNextScreens = 2;
                          /* get the next screen ID#1 */
                          EndianAwareCopy(&wNext1, pEntryPtr, 2);
                          pEntryPtr += 2;
                          /* get the next screen ID#2 */
                          EndianAwareCopy(&wNext2, pEntryPtr, 2);

                          pNextScreens[0] = wNext1;
                          pNextScreens[1] = wNext2;
                        }

                      /* call the function that processes a hard key match */
                      fScreenChanging = fnProcessKeyOrEventMatch( bKeyCode, NULL_PTR, wFnID,
                                                                  bNumNextScreens, pNextScreens, TRUE,
                                                                  fIgnorePre, fIgnorePost, fIgnoreNext );
                      /* don't break the loop if we should continue searching for further matches */
                      if ((fScreenChanging == TRUE) || (fContinueSearching == FALSE))
                        break;
                    }
                }
            }
        }
    }

  return;
}

/* **********************************************************************
// FUNCTION NAME: fnProcessEvent
// DESCRIPTION: Processes an event notification.  This function checks to
//              see if the current screen has an entry for the event
//              that happened, and processes it if so.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  bOccurredEventID - ID number of the event that occurred.  the ID
//                              numbers are defined in oifunct.h
//
// OUTPUTS: Returns TRUE if the current screen processed the event.
//          Returns FALSE if the current screen does not care about the event,
//           or event processing is disabled.
// *************************************************************************/
BOOL fnProcessEvent(unsigned char bOccurredEventID, INTERTASK *pIntertaskMsg)
{
  unsigned long     i;
  unsigned char     *pEntryPtr;
  unsigned char     *pNextEntryPtr;
  unsigned char     bEventCtrlData;
  unsigned char     bRecordEventID;
  unsigned short        wFnID;
  unsigned short        wNext1, wNext2;
  unsigned short        pNextScreens[MAX_NEXT_SCREENS];
  BOOL              fRetval = FALSE;
  unsigned char     bParmID;
  unsigned char     bParmCompareType;
  unsigned short        wOperand;

  //    /* initialize the pNextScreens[] */
  memset (pNextScreens, 0, MAX_NEXT_SCREENS * sizeof(UINT16));

  if ((rWaitCtrl.lwMsgsAwaited == 0) && (fIgnoreKeysAndEvents == FALSE))    /* events have no effect if we are waiting for a message */
    {
      pNextEntryPtr = (unsigned char *) rCurrentTreeRecord.pFirstEventEntry;
      for (i = 0; i < rCurrentTreeRecord.bNumEventEntries; i++)
        {
          if (rTreeDir.wComponentFormat >= 0x0200)
            {
              /* the first byte of the entry is the length of the entry.  use it
             to calculate the pointer to the beginning of the following entry. */
              pEntryPtr = pNextEntryPtr;
              pNextEntryPtr += *pEntryPtr++;
            }
          else
            pEntryPtr = ((unsigned char *) rCurrentTreeRecord.pFirstEventEntry) + (i * TREE_ENTRY_SIZE);

          bEventCtrlData = *pEntryPtr++;
          bRecordEventID = *pEntryPtr++;

          /* get the parameter compare options */
          bParmID = *pEntryPtr++;
          bParmCompareType = *pEntryPtr++;
          EndianAwareCopy(&wOperand, pEntryPtr, sizeof(unsigned short));
          pEntryPtr += 2;

          if (bRecordEventID == bOccurredEventID)
            {
              BOOL  fParmResult;

              /* if the direction matches, do the parameter comparison */
              if (fnCheckKeyOrEventParameter(bParmID, bParmCompareType,
                                             wOperand, &fParmResult) != SUCCESSFUL)
                fnReportMeterError(ERROR_CLASS_OIT, ECOIT_BAD_PARAMETER_CHECK);

              /* if the parameter compare was true, we have a match */
              if (fParmResult == TRUE)
                {
                  unsigned char bNumNextScreens;
                  BOOL          fIgnorePre;
                  BOOL          fIgnorePost;
                  BOOL          fIgnoreNext;
                  BOOL          fContinueSearching;
                  BOOL          fScreenChanging;

                  /* it's a match, so process the event.  start by getting the function ID */
                  EndianAwareCopy(&wFnID, pEntryPtr, 2);
                  if (wFnID >= OITEventIDMax)
                  {// Bad function ID in EMD, so get out before there is a crash
                  	dbgTrace(DBG_LVL_ERROR,"Error: Invalid function ID %d in EMD", wFnID);
                  	break;
                  }
                  pEntryPtr += 2;
    
                  /* check the format of the tree component */
                  if (rTreeDir.wComponentFormat >= 0x0200)
                    {
                      /* if format is > 2.00, low nibble of the next screen control byte
                     contains the number of next screen choices, which is variable.
                     next screen control byte also contains ignore pre, ignore post,
                     ignore next screen and continue searching flags */
                      bNumNextScreens = (*pEntryPtr) & 0x0F;
                      fIgnorePre = (*pEntryPtr & 0x80) ? TRUE : FALSE;
                      fIgnorePost = (*pEntryPtr & 0x40) ? TRUE : FALSE;
                      fIgnoreNext = (*pEntryPtr & 0x20) ? TRUE : FALSE;
                      fContinueSearching = (*pEntryPtr++ & 0x10) ? TRUE : FALSE;
                      if (bNumNextScreens > MAX_NEXT_SCREENS)
                        bNumNextScreens = MAX_NEXT_SCREENS;
                      EndianAwareArray16Copy(pNextScreens, pEntryPtr, bNumNextScreens * sizeof(unsigned short));
                    }
                  else
                    {
                      /* if format < 2.00, the ignore pre, post, and next options are always
                     false because the features were not available then */
                      fIgnorePre = FALSE;
                      fIgnorePost = FALSE;
                      fIgnoreNext = FALSE;
                      fContinueSearching = FALSE;
                      /* if format is < 2.00, there are always 2 next screen choices */
                      bNumNextScreens = 2;
                      /* get the next screen ID#1 */
                      EndianAwareCopy(&wNext1, pEntryPtr, 2);
                      pEntryPtr += 2;
                      /* get the next screen ID#2 */
                      EndianAwareCopy(&wNext2, pEntryPtr, 2);

                      pNextScreens[0] = wNext1;
                      pNextScreens[1] = wNext2;
                    }

                  /* call the function that processes a hard key or event match */
                  fScreenChanging = fnProcessKeyOrEventMatch(0, pIntertaskMsg, wFnID, bNumNextScreens,
                                                             pNextScreens, FALSE, fIgnorePre, 
                                                             fIgnorePost, fIgnoreNext);
                  fRetval = TRUE;   /* this function will return true because we processed the event */

                            /* don't break the loop if we should continue searching for further matches */
                  if ((fScreenChanging == TRUE) || (fContinueSearching == FALSE))
                    break;
                }
            }
        }
    }

  return (fRetval);

}

/* **********************************************************************
// FUNCTION NAME: fnPaintCurrentScreen
// DESCRIPTION: Sends fixed text, field, and cursor information for the
//              current screen to the LCD task for display.
// INPUTS: 
//     fRefreshOnly- if set to true, the refresh functions of the fields
//                          and variable graphics are used.  Elements which
//                          do not require repainting are not sent to the
//                          LCD controller.
//                       if set to false, the init functions are used and
//                          all screen elements are repainted.  Also, the
//                          LCD clear command is sent before painting.
//
// OUTPUTS: none
// HISTORY:
//  2012.08.06 Clarisa Bellamy - Add a syslog message with the screen ID.
// AUTHOR: Joe Mozdzer
// *************************************************************************/
unsigned long   lwDestroyedPacketsOIT = 0;
unsigned long   lwCreatedPacketsOIT = 0;
void fnPaintCurrentScreen(BOOL fRefreshOnly)
{
  LCD_STRING        *pFTHead, *pFTTail, *pFLDHead, *pFLDTail;
  LCD_STRING        *pFGHead, *pFGTail, *pVGHead, *pVGTail;
  LCD_STRING        *pFGOnTopHead, *pFGOnTopTail;
  LCD_STRING        *pVGOnTopHead, *pVGOnTopTail;
  LCD_STRING        *pHeadToSend = NULL_PTR;
  LCD_STRING        *pStringsToDestroy1;
  LCD_STRING        *pStringsToDestroy2;
  LCD_CURSOR        *pLCDCursor;
  unsigned long lwIntSettings; /* used for temporary disable of irq's */
  unsigned long pMsgData[2];
  static unsigned long  lwDisplayStringsID = 0;

    // Lint:
    pFTHead = pFTTail = pFLDHead = pFLDTail = NULL_PTR;
    pFGHead = pFGTail = pVGHead = pVGTail = NULL_PTR;
    pFGOnTopHead = pFGOnTopTail = NULL_PTR;
    pVGOnTopHead = pVGOnTopTail = NULL_PTR;

    // Update the refresh-only syslog counter.
    if(   (++bRefreshLogCount > REFRESH_REMINDER_COUNT) 
       || !fRefreshOnly )
    {   // If it's a new screen, always put the ID into the syslog, but if its a 
        // refresh, only put it into the syslog occassionally to save space for 
        // more informative messages. 
        bRefreshLogCount = 0;
    }
    // Add syslog entry with screen ID if it is a new screen, or it is the nth
    //  time the screen has been refreshed.
    if( bRefreshLogCount == 0)
    {
        // Put current screen ID into the syslog: The person debugging can look  
        //  it up in oifunctfp.h for the version of code running.
        sprintf( scratchpad100, "OI: %s screen: ID= %d", 
                                 (fRefreshOnly ? "Refresh" : "Paint new"), 
                                 (int)wCurrentScreenID ); 
        fnDumpStringToSystemLog( scratchpad100 );
    }

    /* clear the current contents of the display if not refreshing only */
    if (!fRefreshOnly)
    {
      (void)OSSendIntertask( LCDTASK, OIT, LCD_CLEAR_DISPLAY, SHORT_DATA, 
                             &wCurrentBGColor, sizeof(wCurrentBGColor) );
    }


    /* get a fixed text, fixed graphic, fields, and variable graphics list */
    if (fRefreshOnly)
    {
      pFGHead = NULL_PTR;   /* fixed graphics that aren't on top don't need to be refreshed since they are on the bottom */
      fnBuildVarGraphicsList(&pVGHead, &pVGTail, FALSE, FALSE);

//RAM added
//      if( pVGHead != NULL_PTR )
        fnBuildFixedTextList(&pFTHead, &pFTTail);   /* if a variable graphic is being repainted, we must also
                                   repaint the fixed texts since one of them might be on
                                   top of the variable graphic */
//      else
//        pFTHead = NULL_PTR;

//END
      fnBuildFieldsList(&pFLDHead, &pFLDTail, &pLCDCursor, FALSE);

      if( (pVGHead != NULL_PTR) || (pFLDHead != NULL_PTR) )
        fnBuildFixedGraphicList(&pFGOnTopHead, &pFGOnTopTail, TRUE);    /* same logic as w/ fixed text, except there
                                           are two things that can be below */
      else
        pFGOnTopHead = NULL_PTR;

      fnBuildVarGraphicsList(&pVGOnTopHead, &pVGOnTopTail, FALSE, TRUE);
    }
    else
    {
      fnBuildFixedGraphicList(&pFGHead, &pFGTail, FALSE);
      fnBuildVarGraphicsList(&pVGHead, &pVGTail, TRUE, FALSE);
      fnBuildFixedTextList(&pFTHead, &pFTTail);
      fnBuildFieldsList(&pFLDHead, &pFLDTail, &pLCDCursor, true);
      fnBuildFixedGraphicList(&pFGOnTopHead, &pFGOnTopTail, TRUE);
      fnBuildVarGraphicsList(&pVGOnTopHead, &pVGOnTopTail, TRUE, TRUE);
    }

    
    /* now chain the lists together into one large list.
       paint order is:  fixed graphics, var graphics, fixed text, fields, fixed graphics that are
       on top, variable graphics that are on top */
    if( pVGOnTopHead != NULL_PTR ) /* variable graphics designated as "on top" are last in the chain.
                     if not NULL_PTR, find the next lowest element to attach to.  this algorithm
                     is then used for the remaining lists */
    {
      if( pFGOnTopHead != NULL_PTR )
        pFGOnTopTail->pNext = pVGOnTopHead;
      else
      {
          if( pFLDHead != NULL_PTR )
            pFLDTail->pNext = pVGOnTopHead;
          else
          {
              if( pFTHead != NULL_PTR )
                pFTTail->pNext = pVGOnTopHead;
              else
              {
                  if( pVGHead != NULL_PTR )
                    pVGTail->pNext = pVGOnTopHead;
                  else
                  {
                      if( pFGHead != NULL_PTR )
                        pFGTail->pNext = pVGOnTopHead;
                  }
              }
          }
      }
    }


    if( pFGOnTopHead != NULL_PTR ) /* fixed graphics on top */
    {
      if( pFLDHead != NULL_PTR )
        pFLDTail->pNext = pFGOnTopHead;
      else
      {
          if( pFTHead != NULL_PTR )
            pFTTail->pNext = pFGOnTopHead;
          else
          {
              if( pVGHead != NULL_PTR )
                pVGTail->pNext = pFGOnTopHead;
              else
              {
                  if( pFGHead != NULL_PTR )
                    pFGTail->pNext = pFGOnTopHead;
              }
          }
      }
    }

    if( pFLDHead != NULL_PTR ) /* fields */                        
    {
      if( pFTHead != NULL_PTR )
        pFTTail->pNext = pFLDHead;
      else
      {
          if( pVGHead != NULL_PTR )
            pVGTail->pNext = pFLDHead;
          else
          {
              if( pFGHead != NULL_PTR )
                pFGTail->pNext = pFLDHead;
          }
      }
    }

    if( pFTHead != NULL_PTR )  /* fixed text */
    {
      if( pVGHead != NULL_PTR )
        pVGTail->pNext = pFTHead;
      else
      {
          if( pFGHead != NULL_PTR )
            pFGTail->pNext = pFTHead;
      }
    }

    if( pVGHead != NULL_PTR )  /* variable graphics (not on top) */
    {
      if( pFGHead != NULL_PTR )
        pFGTail->pNext = pVGHead;
    }


  /* fixed graphics (not on top) do not need to be chained onto anything since they
     will be at the head of the list. */

    /* determine the first list that is not NULL_PTR */
    if( pFGHead != NULL_PTR )
      pHeadToSend = pFGHead;
    else
    {
      if( pVGHead != NULL_PTR )
        pHeadToSend = pVGHead;
      else
      {
          if( pFTHead != NULL_PTR )
            pHeadToSend = pFTHead;
          else
          {
              if( pFLDHead != NULL_PTR )
                pHeadToSend = pFLDHead;
              else
              {
                  if( pFGOnTopHead != NULL_PTR )
                    pHeadToSend = pFGOnTopHead;
                  else
                  {
                      if( pVGOnTopHead != NULL_PTR )
                        pHeadToSend = pVGOnTopHead;
                  }
              }
          }
      }
    }

    /* if at least one list was not NULL_PTR, send it to the LCD task */
    if( pHeadToSend != NULL_PTR )
    {
      lwCreatedPacketsOIT++;
      lwDisplayStringsID++;

      if (fRefreshOnly)
        {
          /* if doing a refresh, destroy any stale refreshes that were not yet handled 
             by the LCD and replace with this one.  Any pending inits are left alone
             because they may contain screen elements that aren't covered in the refreshes. */
          /* protected area */
          lwIntSettings = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
          if (pIntertaskLCDStringXferR)
            pStringsToDestroy1 = pIntertaskLCDStringXferR;
          else
            pStringsToDestroy1 = NULL_PTR;
          pIntertaskLCDStringXferR = pHeadToSend;
          lwLCDStringXferIDR = lwDisplayStringsID;
          NU_Local_Control_Interrupts(lwIntSettings);
          /* end protected area */

          pStringsToDestroy2 = NULL_PTR;
        }
      else
        {
          /* if doing an init, destroy and replace and old inits that have not yet been
             handled by LCD, as well as wiping out any old refreshes that also haven't
             been done (since they must be for an old screen that is now overwritten) */
          /* protected area */
          lwIntSettings = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
          if (pIntertaskLCDStringXferI)
            pStringsToDestroy1 = pIntertaskLCDStringXferI;
          else
            pStringsToDestroy1 = NULL_PTR;
          pIntertaskLCDStringXferI = pHeadToSend;
          lwLCDStringXferIDI = lwDisplayStringsID;
          if (pIntertaskLCDStringXferR)
            {
              pStringsToDestroy2 = pIntertaskLCDStringXferR;
              pIntertaskLCDStringXferR = NULL_PTR;
            }
          else
            pStringsToDestroy2 = NULL_PTR;
          NU_Local_Control_Interrupts(lwIntSettings);
          /* end protected area */

        }

      if (pStringsToDestroy1)
        {
          if (fnOITDestroyStrings(pStringsToDestroy1))
            lwDestroyedPacketsOIT++;
        }
      if (pStringsToDestroy2)
        {
          if (fnOITDestroyStrings(pStringsToDestroy2))
            lwDestroyedPacketsOIT++;
        }
      pMsgData[0] = (unsigned long) pHeadToSend;
      pMsgData[1] = lwDisplayStringsID;
      (void)OSSendIntertask( LCDTASK, OIT, LCD_DISPLAY_STRINGS, LONG_DATA, 
                             pMsgData, sizeof(pMsgData) );
    }
        

  /* if the cursor control structure is not empty (which means that at least one of
     the fields uses the blinking cursor), send it to the LCD task.
     (n.b.- make sure this is done after sending the strings over, otherwise the
     fields will overwrite the cursor and the LCD controller will lose track
     of its inverted/not-inverted state.  then it won't look right when it moves
     thru the field.) */
  if( pLCDCursor != NULL_PTR )
    {
      (void)OSSendIntertask( LCDTASK, OIT, LCD_CONTROL_CURSOR, PART_PTR_DATA, 
                             pLCDCursor, sizeof(LCD_CURSOR) );
    }

  /* start the screen change timer.  some screens are shown for a fixed duration and then
     are automatically changed to another screen.  this timer serves as the trigger. */
  if (!fRefreshOnly)
    {
      (void)OSStopTimer( SCREEN_CHANGE_TIMER ); /* stop the timer first so that it gets reset to its full duration */
      (void)OSStartTimer( SCREEN_CHANGE_TIMER );
    }

  /* if we are refreshing a screen, make sure LCD task gets a chance to run */
  if (fRefreshOnly)
    OSWakeAfter(0);

  return;
}

void fnPaintCurrentScreenAfterError(BOOL fRefreshOnly)
{
  LCD_STRING        *pFTHead, *pFTTail, *pFLDHead, *pFLDTail;
  LCD_STRING        *pFGHead, *pFGTail, *pVGHead, *pVGTail;
  LCD_STRING        *pFGOnTopHead, *pFGOnTopTail;
  LCD_STRING        *pVGOnTopHead, *pVGOnTopTail;
  LCD_STRING        *pHeadToSend = NULL_PTR;
  LCD_STRING        *pStringsToDestroy1;
  LCD_STRING        *pStringsToDestroy2;
  LCD_CURSOR        *pLCDCursor;
  unsigned long lwIntSettings; /* used for temporary disable of irq's */
  unsigned long pMsgData[2];
  static unsigned long  lwDisplayStringsID = 0;


  // Lint:
  pFTHead = pFTTail = pFLDHead = pFLDTail = NULL_PTR;
  pFGHead = pFGTail = pVGHead = pVGTail = NULL_PTR;
  pFGOnTopHead = pFGOnTopTail = NULL_PTR;
  pVGOnTopHead = pVGOnTopTail = NULL_PTR;

  /* clear the current contents of the display if not refreshing only */
  if (!fRefreshOnly)
    {
      (void)OSSendIntertask( LCDTASK, OIT, LCD_CLEAR_DISPLAY, SHORT_DATA, 
                             &wCurrentBGColor, sizeof(wCurrentBGColor) );
    }


  /* get a fixed text, fixed graphic, fields, and variable graphics list */
  if (fRefreshOnly)
    {
      pFGHead = NULL_PTR;   /* fixed graphics that aren't on top don't need to be refreshed since they are on the bottom */
      fnBuildVarGraphicsList(&pVGHead, &pVGTail, FALSE, FALSE);

      if( pVGHead != NULL_PTR )
        fnBuildFixedTextList(&pFTHead, &pFTTail);   /* if a variable graphic is being repainted, we must also
                                   repaint the fixed texts since one of them might be on
                                   top of the variable graphic */
      else
        pFTHead = NULL_PTR;

      fnBuildFieldsListAfterError(&pFLDHead, &pFLDTail, &pLCDCursor, FALSE);

      if( (pVGHead != NULL_PTR) || (pFLDHead != NULL_PTR) )
        fnBuildFixedGraphicList(&pFGOnTopHead, &pFGOnTopTail, TRUE);    /* same logic as w/ fixed text, except there
                                           are two things that can be below */
      else
        pFGOnTopHead = NULL_PTR;

      fnBuildVarGraphicsList(&pVGOnTopHead, &pVGOnTopTail, FALSE, TRUE);
    }
  else
    {
      fnBuildFixedGraphicList(&pFGHead, &pFGTail, FALSE);
      fnBuildVarGraphicsList(&pVGHead, &pVGTail, TRUE, FALSE);
      fnBuildFixedTextList(&pFTHead, &pFTTail);
      fnBuildFieldsListAfterError(&pFLDHead, &pFLDTail, &pLCDCursor, true);
      fnBuildFixedGraphicList(&pFGOnTopHead, &pFGOnTopTail, TRUE);
      fnBuildVarGraphicsList(&pVGOnTopHead, &pVGOnTopTail, TRUE, TRUE);
    }

    
  /* now chain the lists together into one large list.
     paint order is:  fixed graphics, var graphics, fixed text, fields, fixed graphics that are
     on top, variable graphics that are on top */
  if( pVGOnTopHead != NULL_PTR ) /* variable graphics designated as "on top" are last in the chain.
                   if not NULL_PTR, find the next lowest element to attach to.  this algorithm
                   is then used for the remaining lists */
    {
      if( pFGOnTopHead != NULL_PTR )
        pFGOnTopTail->pNext = pVGOnTopHead;
      else
        {
          if( pFLDHead != NULL_PTR )
            pFLDTail->pNext = pVGOnTopHead;
          else
            {
              if( pFTHead != NULL_PTR )
                pFTTail->pNext = pVGOnTopHead;
              else
                {
                  if( pVGHead != NULL_PTR )
                    pVGTail->pNext = pVGOnTopHead;
                  else
                    {
                      if( pFGHead != NULL_PTR )
                        pFGTail->pNext = pVGOnTopHead;
                    }
                }
            }
        }
    }


  if( pFGOnTopHead != NULL_PTR ) /* fixed graphics on top */
    {
      if( pFLDHead != NULL_PTR )
        pFLDTail->pNext = pFGOnTopHead;
      else
        {
          if( pFTHead != NULL_PTR )
            pFTTail->pNext = pFGOnTopHead;
          else
            {
              if( pVGHead != NULL_PTR )
                pVGTail->pNext = pFGOnTopHead;
              else
                {
                  if( pFGHead != NULL_PTR )
                    pFGTail->pNext = pFGOnTopHead;
                }
            }
        }
    }

  if( pFLDHead != NULL_PTR ) /* fields */
    {
      if( pFTHead != NULL_PTR )
        pFTTail->pNext = pFLDHead;
      else
        {
          if( pVGHead != NULL_PTR )
            pVGTail->pNext = pFLDHead;
          else
            {
              if( pFGHead != NULL_PTR )
                pFGTail->pNext = pFLDHead;
            }
        }
    }

  if( pFTHead != NULL_PTR )  /* fixed text */
    {
      if( pVGHead != NULL_PTR )
        pVGTail->pNext = pFTHead;
      else
        {
          if( pFGHead != NULL_PTR )
            pFGTail->pNext = pFTHead;
        }
    }

  if( pVGHead != NULL_PTR )  /* variable graphics (not on top) */
    {
      if( (pFGHead != NULL_PTR) && (pFGTail != NULL_PTR) )
        pFGTail->pNext = pVGHead;
    }


  /* fixed graphics (not on top) do not need to be chained onto anything since they
     will be at the head of the list. */

  /* determine the first list that is not NULL_PTR */
  if( pFGHead != NULL_PTR )
    pHeadToSend = pFGHead;
  else
    {
      if( pVGHead != NULL_PTR )
        pHeadToSend = pVGHead;
      else
        {
          if( pFTHead != NULL_PTR )
            pHeadToSend = pFTHead;
          else
            {
              if( pFLDHead != NULL_PTR )
                pHeadToSend = pFLDHead;
              else
                {
                  if( pFGOnTopHead != NULL_PTR )
                    pHeadToSend = pFGOnTopHead;
                  else
                    {
                      if( pVGOnTopHead != NULL_PTR )
                        pHeadToSend = pVGOnTopHead;
                    }
                }
            }
        }
    }

  /* if at least one list was not NULL_PTR, send it to the LCD task */
  if( pHeadToSend != NULL_PTR )
    {
      lwCreatedPacketsOIT++;
      lwDisplayStringsID++;

      if (fRefreshOnly)
        {
          /* if doing a refresh, destroy any stale refreshes that were not yet handled 
             by the LCD and replace with this one.  Any pending inits are left alone
             because they may contain screen elements that aren't covered in the refreshes. */
          /* protected area */
          lwIntSettings = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
          if (pIntertaskLCDStringXferR)
            pStringsToDestroy1 = pIntertaskLCDStringXferR;
          else
            pStringsToDestroy1 = NULL_PTR;
          pIntertaskLCDStringXferR = pHeadToSend;
          lwLCDStringXferIDR = lwDisplayStringsID;
          NU_Local_Control_Interrupts(lwIntSettings);
          /* end protected area */

          pStringsToDestroy2 = NULL_PTR;
        }
      else
        {
          /* if doing an init, destroy and replace and old inits that have not yet been
             handled by LCD, as well as wiping out any old refreshes that also haven't
             been done (since they must be for an old screen that is now overwritten) */
          /* protected area */
          lwIntSettings = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
          if (pIntertaskLCDStringXferI)
            pStringsToDestroy1 = pIntertaskLCDStringXferI;
          else
            pStringsToDestroy1 = NULL_PTR;
          pIntertaskLCDStringXferI = pHeadToSend;
          lwLCDStringXferIDI = lwDisplayStringsID;
          if (pIntertaskLCDStringXferR)
            {
              pStringsToDestroy2 = pIntertaskLCDStringXferR;
              pIntertaskLCDStringXferR = NULL_PTR;
            }
          else
            pStringsToDestroy2 = NULL_PTR;
          NU_Local_Control_Interrupts(lwIntSettings);
          /* end protected area */

        }

      if (pStringsToDestroy1)
        {
          if (fnOITDestroyStrings(pStringsToDestroy1))
            lwDestroyedPacketsOIT++;
        }
      if (pStringsToDestroy2)
        {
          if (fnOITDestroyStrings(pStringsToDestroy2))
            lwDestroyedPacketsOIT++;
        }
      pMsgData[0] = (unsigned long) pHeadToSend;
      pMsgData[1] = lwDisplayStringsID;
      (void)OSSendIntertask( LCDTASK, OIT, LCD_DISPLAY_STRINGS, LONG_DATA, 
                             pMsgData, sizeof(pMsgData) );
    }
        

  /* if the cursor control structure is not empty (which means that at least one of
     the fields uses the blinking cursor), send it to the LCD task.
     (n.b.- make sure this is done after sending the strings over, otherwise the
     fields will overwrite the cursor and the LCD controller will lose track
     of its inverted/not-inverted state.  then it won't look right when it moves
     thru the field.) */
  if( pLCDCursor != NULL_PTR )
    {
      (void)OSSendIntertask( LCDTASK, OIT, LCD_CONTROL_CURSOR, PART_PTR_DATA, pLCDCursor, sizeof(LCD_CURSOR) );
    }

  /* start the screen change timer.  some screens are shown for a fixed duration and then
     are automatically changed to another screen.  this timer serves as the trigger. */
  if (!fRefreshOnly)
    {
      (void)OSStopTimer( SCREEN_CHANGE_TIMER ); /* stop the timer first so that it gets reset to its full duration */
      (void)OSStartTimer( SCREEN_CHANGE_TIMER );
    }

  /* if we are refreshing a screen, make sure LCD task gets a chance to run */
  if (fRefreshOnly)
    OSWakeAfter(0);

  return;
}

/* **********************************************************************
// FUNCTION NAME: fnOITValidateScreenVersions
// PURPOSE: Compares version numbers of screen tree and language components that
//          are loaded in flash with those that are specified as being
//          compatible with this software in version.h
//      This must also create 4 strings, each containing a 3-part version number.
//      one for Tree and one for Lang.  
//
// INPUTS:  (compatible versions are set up through constants in version.h)
//          pBadTree- if the tree version is incompatible, TRUE is returned here
//          pBadLang- if one or more language versions are incompatible, TRUE
//                      is returned here
//          treeEMDVer - Ptr to char array, minimum of 9 bytes long.  Destination
//                      of string containing the Tree Version from the EMD.
//          treeUICVer - Ptr to char array, minimum of 9 bytes long.  Destination
//                      of string containing the Version from the UIC used to
//                      validate the Tree version.
//          langEMDVer - Ptr to char array, minimum of 9 bytes long.Destination
//                      of string containing the Language Version from the EMD.
//          langUICVer - Ptr to char array, minimum of 9 bytes long. Destination
//                      of string containing the Version from the UIC used to
//                      validate the Language version.
// RETURNS: 
//  TRUE if tree and language files are all compatible, FALSE otherwise.
// OUTPUTS:
//  WRites the 4 strings to the addresses indicated by the input pointer arguments.
//
// NOTES:
//  1. All the strings will be null-terminated.
//  2. All strings will only contain a 3-part version number without decimals.
//  3. Destinations must each be 7 bytes minimum.
//
// HISTORY:
//  2009.07.13  Clarisa Bellamy - Modified to accomodate change to screen versions 
//                      from 2-part to 3-part.
// AUTHOR: Joe Mozdzer
// *************************************************************************/
BOOL fnOITValidateScreenVersions( BOOL *pBadTree, BOOL *pBadLang, 
                                  char* treeEMDVer, char *treeUICVer,
                                  char* langEMDVer, char *langUICVer )
{
    UINT8       loop;
    UINT8       bNumLangs;
    UINT32      ulActualVersion = 0;
    UINT8       bBcdVersion;
    BOOL        fCompareResult;
    BOOL        fLangsValid = TRUE;
    BOOL        fTreeValid = TRUE;

    bNumLangs = fnFlashGetNumLanguages();
    for( loop = 0; loop < bNumLangs; loop++ )
    {
        ulActualVersion = 0;
        fnOITInitializeLanguage( loop );
        // Version number needs to be converted to a long BCD for the version 
        //  comparison:
        bBcdVersion = BinToBCD(rLanguageDir.wVersion >> 8);
        ulActualVersion |= (UINT32)bBcdVersion << 16;
        bBcdVersion = BinToBCD(rLanguageDir.wVersion & 0xFF);
        ulActualVersion |= (UINT32)bBcdVersion << 8;
        if( rLanguageDir.fExtendedVersionValid == TRUE )   
            ulActualVersion |= (UINT32)BinToBCD( rLanguageDir.bExtendedVersion );
        // Fill the Lang version strings.
        sprintf( langEMDVer, "%02x%04x", (UINT16)(ulActualVersion >> 16), (UINT16)(ulActualVersion & 0xFFFF) );
        sprintf( langUICVer, "%02x%04x", (UINT16)(UIC_LANGUAGE_VERSION_A >> 16), (UINT16)(UIC_LANGUAGE_VERSION_A & 0xFFFF) );
        // Check if the EMD Tree Tversion is valid.
        fCompareResult = fnDoVersionCompare( UIC_LANGUAGE_VERSION_A, UIC_LANGUAGE_VERSION_B, 
                                             UIC_LANGUAGE_MASK, ulActualVersion );
        if( fCompareResult == FALSE )
        {
          fLangsValid = FALSE;
          break;
        }
    }

    fnOITInitializeTree();
    ulActualVersion = 0;
    bBcdVersion = BinToBCD(rTreeDir.wVersion >> 8);
    ulActualVersion |= (UINT32)bBcdVersion  << 16;
    bBcdVersion = BinToBCD(rTreeDir.wVersion & 0xFF);
    ulActualVersion |= (UINT32)bBcdVersion  << 8;
    bBcdVersion = BinToBCD(rTreeDir.bExtendedVersion);
    if( rTreeDir.fExtendedVersionValid == TRUE )   
        ulActualVersion |= (UINT32)BinToBCD( rTreeDir.bExtendedVersion );
    // Fill the Tree version strings.
    sprintf( treeEMDVer, "%02x%04x", (UINT16)(ulActualVersion >> 16), (UINT16)(ulActualVersion & 0xFFFF) );
    sprintf( treeUICVer, "%02x%04x", (UINT16)(UIC_TREE_VERSION_A >> 16), (UINT16)(UIC_TREE_VERSION_A & 0xFFFF) );
    // Check if the EMD Tree Tversion is valid.
    fTreeValid = fnDoVersionCompare( UIC_TREE_VERSION_A, UIC_TREE_VERSION_B, 
                                     UIC_TREE_MASK, ulActualVersion );

  if (pBadTree)
    *pBadTree = fTreeValid ? FALSE : TRUE;
  if (pBadLang)
    *pBadLang = fLangsValid ? FALSE : TRUE;

  return ((fTreeValid && fLangsValid) ? TRUE : FALSE);  
}

/* **********************************************************************
// FUNCTION NAME: fnSendBobImageMessage
// DESCRIPTION: General purpose OIT utility function that builds and sends
//               an image request message to Bob.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  sIndiciaSelection - the indicia selection (choices are defined in bob.h)
//                               set to -1 to avoid making a new selection (and
//                               to keep the previous selection)
//
//          sPermitSelection - the permit selection (choices are defined in bob.h)
//                               set to -1 to avoid making a new selection
//
//          sReportSelection - the report selection (choices are defined in bob.h)
//                               set to -1 to avoid making a new selection
//
//          sAdSelection - the ad selection.  (the bob task maintains an array of
//                           valid selections.)
//                           set to -1 to avoid making a new selection
//
//          sInscSelection - the inscription selection.  (the bob task maintains an array of
//                           valid selections.)
//                           set to -1 to avoid making a new selection
//
// *************************************************************************/
void fnSendBobImageMessage(short sIndiciaSelection, short sPermitSelection, short sAdSelection,
               short sInscSelection, short sReportSelection)
{
  unsigned char pMsgBuf[16];    /* max we need is ((NumSelections * 2) + 1) so 16 is more than enough */
  unsigned char bNumSelections = 0;
  unsigned char bSelectionIndex;

  bSelectionIndex = FIRST_SELECTION_BYTE_LOC;

  /* Note Indicia must always be the first selection checked */
  /* check if we are making a selection for this image type */
  if (sIndiciaSelection != -1)
    {
      /* validate selection range.  must fit in a byte to be valid */
      if (sIndiciaSelection > 255)
        goto error;

      /* load the message buf array */
      pMsgBuf[bSelectionIndex++] = INDICIA_SELECTION;
      pMsgBuf[bSelectionIndex++] = (unsigned char) sIndiciaSelection;
      /* increment the counter of the number of selections being made */
      bNumSelections++;
    }

  /* check if we are making a selection for this image type */
  if (sPermitSelection != -1)
    {
      /* validate selection range.  must fit in a byte to be valid */
      if (sPermitSelection > 255)
        goto error;

      /* load the message buf array */
      pMsgBuf[bSelectionIndex++] = PERMIT_SELECTION;
      pMsgBuf[bSelectionIndex++] = (unsigned char) sPermitSelection;
      /* increment the counter of the number of selections being made */
      bNumSelections++;
    }

  /* check if we are making a selection for this image type */
  if (sAdSelection != -1)
    {
      /* validate selection range.  must fit in a byte to be valid */
      if (sAdSelection > 255)
        goto error;

      /* load the message buf array */
      pMsgBuf[bSelectionIndex++] = AD_SELECTION;
      pMsgBuf[bSelectionIndex++] = (unsigned char) sAdSelection;
      /* increment the counter of the number of selections being made */
      bNumSelections++;
    }

  /* check if we are making a selection for this image type */
  if (sInscSelection != -1)
    {
      /* validate selection range.  must fit in a byte to be valid */
      if (sInscSelection > 255)
        goto error;

      /* load the message buf array */
      pMsgBuf[bSelectionIndex++] = INSCRIPTION_SELECTION;
      pMsgBuf[bSelectionIndex++] = (unsigned char) sInscSelection;
      /* increment the counter of the number of selections being made */
      bNumSelections++;
    }

  /* check if we are making a selection for this image type */
  if (sReportSelection != -1)
    {
      /* validate selection range.  must fit in a byte to be valid */
      if (sReportSelection > 255)
        goto error;

      /* load the message buf array */
      pMsgBuf[bSelectionIndex++] = REPORT_SELECTION;
      pMsgBuf[bSelectionIndex++] = (unsigned char) sReportSelection;
      /* increment the counter of the number of selections being made */
      bNumSelections++;
    }

  if (bNumSelections == 0)
    fnDisplayErrorDirect("No images selected in fnSendBobImageMessage", 0, OIT);

  pMsgBuf[NUM_SELECTIONS_BYTE_LOC] = bNumSelections;

  (void)OSSendIntertask( SCM, OIT, BOB_SELECT_IMAGE_REQ, BYTE_DATA, pMsgBuf,
                         sizeof(unsigned char) * ((bNumSelections * 2) + 1) );
  fnSetMessageTimeout(SCM, BOB_SELECT_IMAGE_RSP, IMAGE_DOWNLOAD_TIMEOUT);

  return;

 error:
  fnDisplayErrorDirect("Illegal image selection within OIT", 0, OIT);

}

/* **********************************************************************
// FUNCTION NAME: fnGetKeyNextScreenInfo
// DESCRIPTION: Searches for and returns data associated with the first
//              hard key record for the current screen that:
//              1) matches the requested key control and key id.
//              2) has its parameter comparison satisfied.
//
// INPUTS:  bKeyCtrl, bKeyID - the requested key control and key ID
//          pFnID - hard key function to call is returned here
//          pNext1, pNext2 - choice for next screen are returned here
//
// RETURNS: SUCCESSFUL if a matching record was found,
//          FAILURE otherwise (pFnID, pNext1, and pNext2 are only valid
//              when SUCCESSFUL is returned)
//
// AUTHOR: Joe Mozdzer
// NOTES:  This function was created to support field based menus.  Since
//          the key numbers that will map to each menu function are not
//          known until run time, the hard key functions must do additional
//          work to determine the correct next screen.  It became necessary
//          for one hard key function to have knowledge of another's next
//          screen information, so that is why this function exists.
// *************************************************************************/
BOOL fnGetKeyNextScreenInfo(unsigned char bKeyCtrl, unsigned char bKeyID, unsigned short *pFnID,
                unsigned short *pNext1, unsigned short *pNext2)
{
  unsigned char i;
  unsigned char bKeyCtrlData, bHardKeyID;
  unsigned char *pEntryPtr;
  unsigned char *pNextEntryPtr;
  BOOL          fRetval = FAILURE;
  unsigned char     bParmID;
  unsigned char     bParmCompareType;
  unsigned short    wOperand;


  pNextEntryPtr = (unsigned char *) rCurrentTreeRecord.pFirstKeyEntry;
  for (i = 0; i < rCurrentTreeRecord.bNumKeyEntries; i++)
    {
      /* go through each hard key record looking for the one requested */

      if (rTreeDir.wComponentFormat >= 0x0200)
        {
          pEntryPtr = pNextEntryPtr;
          pNextEntryPtr += *pEntryPtr++;
        }
      else
        pEntryPtr = ((unsigned char *) rCurrentTreeRecord.pFirstKeyEntry) + (i * TREE_ENTRY_SIZE);

      bKeyCtrlData = *pEntryPtr++;
      bHardKeyID = *pEntryPtr++;

      /* get the parameter compare options */
      bParmID = *pEntryPtr++;
      bParmCompareType = *pEntryPtr++;
      EndianAwareCopy(&wOperand, pEntryPtr, sizeof(unsigned short));
      pEntryPtr += 2;

      if ((bKeyCtrlData == bKeyCtrl) && (bHardKeyID == bKeyID))
        {
          BOOL  fParmResult;

          /* if the key and direction matches, do the parameter comparison */
          if (fnCheckKeyOrEventParameter(bParmID, bParmCompareType,
                                         wOperand, &fParmResult) != SUCCESSFUL)
            fnReportMeterError(ERROR_CLASS_OIT, ECOIT_BAD_PARAMETER_CHECK);

          /* if the parameter compare was true, we have a match */
          if (fParmResult == TRUE)
            {
              /* return the function ID */
        	  EndianAwareCopy(pFnID, pEntryPtr, sizeof(unsigned short));
              pEntryPtr += sizeof(unsigned short);

              /* return the first two next screen IDs.  the format they are
             in depends on the format of the tree component. */
              if (rTreeDir.wComponentFormat >= 0x0200)
                {
                  unsigned char bNumNextScreens;

                  *pNext1 = 0;
                  *pNext2 = 0;
                  bNumNextScreens = (*pEntryPtr++) & 0x0F;
                  if (bNumNextScreens > 0)
                	  EndianAwareCopy(pNext1, pEntryPtr, sizeof(unsigned short));
                  if (bNumNextScreens > 1)
                	  EndianAwareCopy(pNext2, pEntryPtr + sizeof(unsigned short), sizeof(unsigned short));
                }
              else
                {
                  /* get the next screen ID#1 */
            	  EndianAwareCopy(pNext1, pEntryPtr, sizeof(unsigned short));
                  pEntryPtr += sizeof(unsigned short);
                  /* get the next screen ID#2 */
                  EndianAwareCopy(pNext2, pEntryPtr, sizeof(unsigned short));
                }
              fRetval = SUCCESSFUL;
              break;
            }
        }
    }

  return (fRetval);
}

/* **********************************************************************
// FUNCTION NAME: fnGetKeyNextScreenInfoFmt2
// DESCRIPTION: Searches for and returns data associated with the first
//              hard key record for the current screen that:
//              1) matches the requested key control and key id.
//              2) has its parameter comparison satisfied.
//              This function differs from fnGetKeyNextScreenInfo in that
//              it returns the next screen information as a variable sized
//              list instead of two fixed choices.
//
// INPUTS:  bKeyCtrl, bKeyID - the requested key control and key ID
//          pFnID - hard key function to call is returned here
//          pNext1, pNext2 - choice for next screen are returned here
//
// RETURNS: SUCCESSFUL if a matching record was found,
//          FAILURE otherwise (pFnID, pNext1, and pNext2 are only valid
//              when SUCCESSFUL is returned)
//
// AUTHOR: Joe Mozdzer
// NOTES:  This function was created to support field based menus.  Since
//          the key numbers that will map to each menu function are not
//          known until run time, the hard key functions must do additional
//          work to determine the correct next screen.  It became necessary
//          for one hard key function to have knowledge of another's next
//          screen information, so that is why this function exists.
// *************************************************************************/
BOOL fnGetKeyNextScreenInfoFmt2(unsigned char bKeyCtrl, unsigned char bKeyID, unsigned short *pFnID,
                unsigned char *pNumNext, unsigned short *pNextScreenList)
{
  unsigned char i;
  unsigned char bKeyCtrlData, bHardKeyID;
  unsigned char *pEntryPtr;
  unsigned char *pNextEntryPtr;
  BOOL          fRetval = FAILURE;
  unsigned char     bParmID;            
  unsigned char     bParmCompareType;
  unsigned short    wOperand;


  pNextEntryPtr = (unsigned char *) rCurrentTreeRecord.pFirstKeyEntry;
  for (i = 0; i < rCurrentTreeRecord.bNumKeyEntries; i++)
    {
      /* go through each hard key record looking for the one requested */

      if (rTreeDir.wComponentFormat >= 0x0200)
        {
          pEntryPtr = pNextEntryPtr;
          pNextEntryPtr += *pEntryPtr++;
        }
      else
        pEntryPtr = ((unsigned char *) rCurrentTreeRecord.pFirstKeyEntry) + (i * TREE_ENTRY_SIZE);

      bKeyCtrlData = *pEntryPtr++;
      bHardKeyID = *pEntryPtr++;

      /* get the parameter compare options */
      bParmID = *pEntryPtr++;
      bParmCompareType = *pEntryPtr++;
      EndianAwareCopy(&wOperand, pEntryPtr, sizeof(unsigned short));
      pEntryPtr += 2;

      if ((bKeyCtrlData == bKeyCtrl) && (bHardKeyID == bKeyID))
        {
          BOOL  fParmResult;

          /* if the key and direction matches, do the parameter comparison */
          if (fnCheckKeyOrEventParameter(bParmID, bParmCompareType,
                                         wOperand, &fParmResult) != SUCCESSFUL)
            fnReportMeterError(ERROR_CLASS_OIT, ECOIT_BAD_PARAMETER_CHECK);

          /* if the parameter compare was true, we have a match */
          if (fParmResult == TRUE)
            {
              /* return the function ID */
        	  EndianAwareCopy(pFnID, pEntryPtr, sizeof(unsigned short));
              pEntryPtr += sizeof(unsigned short);

              /* return the first two next screen IDs.  the format they are
                 in depends on the format of the tree component. */
              if (rTreeDir.wComponentFormat >= 0x0200)
                {
                  *pNumNext = (*pEntryPtr++) & 0x0F;

                  EndianAwareArray16Copy(pNextScreenList, pEntryPtr, sizeof(unsigned short) * *pNumNext);
                }
              else
                {
                  *pNumNext = 2;
                  /* get the next screen ID#1 */
                  EndianAwareCopy(pNextScreenList, pEntryPtr, sizeof(unsigned short));
                  pNextScreenList++;
                  EndianAwareCopy(pNextScreenList, pEntryPtr + 2, sizeof(unsigned short));
                }
              fRetval = SUCCESSFUL;
              break;
            }
        }
    }

  return (fRetval);
}

/* **********************************************************************
// FUNCTION NAME: fnGetFieldInfo
// DESCRIPTION: Searches for and returns data associated with the nth
//              field record of the "nested" type for the current screen.
//
// INPUTS:  bKeyCtrl, bKeyID - the requested key control and key ID
//          pFnID - hard key function to call is returned here
//          pNext1, pNext2 - choice for next screen are returned here
//
// RETURNS: SUCCESSFUL if a matching record was found,
//          FAILURE otherwise (pFnID, pNext1, and pNext2 are only valid
//              when SUCCESSFUL is returned)
//
// AUTHOR: Joe Mozdzer
// NOTES:  This function was created to support field based menus.  Since
//          the key numbers that will map to each menu function are not
//          known until run time, the hard key functions must do additional
//          work to determine the correct next screen.  It became necessary
//          for one hard key function to have knowledge of another's next
//          screen information, so that is why this function exists.
// *************************************************************************/
BOOL fnGetFieldInfo(unsigned char bRecordNum, unsigned short *pInitFnID, unsigned short *pRefreshFnID, unsigned char *pLen,
            unsigned char *pFieldCtrl, UINT8 *pAttributes, unsigned char *pNumTableItems, unsigned char **pTableTextIDLocation)
{
    unsigned char   *pFLDRecordParse;
    unsigned char   *pFLDRecordStart;
    unsigned long   i;
    BOOL            fRetval = FALSE;            /* indicates if we found the desired record  */
    unsigned char   bNumNestedFieldsFound = 0;  /* number of field functions we have found so far that are of the "nested" type */
    unsigned char   bFieldLen;
    unsigned char   bFieldCtrl;
    unsigned char   bRecordLen;
    UINT8           bAttributes;

    pFLDRecordStart = rCurrentFieldsRecord.pFirstFieldEntry;


    for (i = 0; i < rCurrentFieldsRecord.bNumEntries; i++)
    {
        /* initialize the parse pointer to the start of the current record */
        pFLDRecordParse = pFLDRecordStart;

        /* get the record length, then copy the string data from flash to the node we just created */
        bRecordLen = *pFLDRecordParse++;
        if (rLanguageDir.wComponentFormat < 0x0200)
        {
            pFLDRecordParse += 3;               /* skip over the x,y coordinates, the font*/
            bAttributes = *pFLDRecordParse++;
        }
        else
        {
            pFLDRecordParse += 5;               /* skip over the x,y coordinates, the font*/
            bAttributes = *pFLDRecordParse++;
            pFLDRecordParse += 2;   /*skip the field width if compiled in format 02.00 or greater */
        }

        //        bAttributes = *pFLDRecordParse++;
        bFieldLen = *pFLDRecordParse++;
        bFieldCtrl = *pFLDRecordParse++;

        /* check if this is a nested field */
        if ((bFieldCtrl & FIELD_TYPE_MASK) == TYPE_NESTED)
            bNumNestedFieldsFound++;

        /* if it is a nested field and it is the one we are looking for, copy the field information into the
         return locations and return success */
        if (bNumNestedFieldsFound == bRecordNum)
        {
            fRetval = TRUE;

            *pLen = bFieldLen;
            *pFieldCtrl = bFieldCtrl;
            *pAttributes = bAttributes;

            EndianAwareCopy(pInitFnID, pFLDRecordParse, 2);
            pFLDRecordParse += 2;
            EndianAwareCopy(pRefreshFnID, pFLDRecordParse, 2);
            pFLDRecordParse += 2;

            if (rLanguageDir.wComponentFormat >= 0x0301)
            {
                // if format 3.01+, skip over the FG and BG colors
                pFLDRecordParse += 4;
            }

            *pNumTableItems = *pFLDRecordParse++;

            memcpy(pTableTextIDLocation, &pFLDRecordParse, sizeof (unsigned char *));
            break;
        }

        pFLDRecordStart += bRecordLen;
    }

    return (fRetval);
}

/* **********************************************************************
// FUNCTION NAME: fnGetOITMode
// DESCRIPTION: Returns the current mode of the OIT.
//
// INPUTS:  pMode- return location for the mode
//          pSubMode- return location for the submode
//
// AUTHOR: Joe Mozdzer
// *************************************************************************/
void fnGetOITMode(unsigned char *pMode, unsigned char *pSubMode)
{
  *pMode = ucOITCurrentMode;
  *pSubMode = ucOITCurrentSubMode;
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnGetCurrentScreenID
// DESCRIPTION: Returns the ID of the current screen.  Used by other OIT modules
//              to obtain this information, which is stored in a variable private
//              to this file only.
// AUTHOR: Joe Mozdzer
// *************************************************************************/
unsigned short fnGetCurrentScreenID()
{
  return (wCurrentScreenID);
}

/* **********************************************************************
// FUNCTION NAME: fnCallKeyFunction
// DESCRIPTION: Wrapper for calling a key function.  A key function can be
//              in one of two different formats and this function facilitates
//              the calling process.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  wFnID- ID number of the function to call
//          unsigned char- the key code that we are processing
//          bNumNextScreens- number of choices of next screens to pass into the function
//          pNextScreen- pointer to array of choices for next screen
//
// RETURNS: returns the next screen that was returned by the key function
// *************************************************************************/
unsigned short fnCallKeyFunction(unsigned short wFnID, unsigned char bKeyCode,
                 unsigned char bNumNextScreens, unsigned short *pNextScreens)
{
  pHKeyFn           pfnKeyFunction;
  BOOL          fInFormat2 = FALSE;
  unsigned long i = 0;
  unsigned short    wNextScreen;
        
  /* retrieve pointer to the function from the screen-tool-generated array */
  pfnKeyFunction = aHKeyFns[wFnID];

  /* determine if the function is in the 2nd format.  functions which are in this format
     appear in the aKeyFnsInFormat2 array. */
  do
    {       
      if (aKeyFnsInFormat2[i] == (pHKeyFn2ndFormat) pfnKeyFunction)
        {
          fInFormat2 = TRUE;
          break;
        }
    } while (aKeyFnsInFormat2[i++] != (pHKeyFn2ndFormat) 0xFFFFFFFF);

  /* make the call depending on the format the function is in.  note that for format 2,
     we have to cast the function pointer to that format before making the call. */
  if (fInFormat2 == FALSE)
    {
      unsigned short    wNext1 = 0;
      unsigned short    wNext2 = 0;

      if (bNumNextScreens >= 2)
        wNext2 = pNextScreens[1];

      if (bNumNextScreens >= 1)
        wNext1 = pNextScreens[0];

      wNextScreen = pfnKeyFunction(bKeyCode, wNext1, wNext2);
    }
  else
    wNextScreen = ((pHKeyFn2ndFormat) pfnKeyFunction) (bKeyCode, bNumNextScreens, pNextScreens);

  return (wNextScreen);
}

/* **********************************************************************
// FUNCTION NAME: fnCallEventFunction
// DESCRIPTION: Wrapper for calling a event function.  An event function can be
//              in one of two different formats and this function faciliates
//              the calling process.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  wFnID- ID number of the function to call
//          pIntertask- pointer to intertask message that launched the event
//          bNumNextScreens- number of choices of next screens to pass into the function
//          pNextScreen- pointer to array of choices for next screen
//
// RETURNS: returns the next screen that was returned by the key function
// *************************************************************************/
unsigned short fnCallEventFunction(unsigned short wFnID, unsigned char bNumNextScreens,
                   unsigned short *pNextScreens, INTERTASK *pIntertask)
{
  pEventFn      pfnEventFunction;
  BOOL          fInFormat2 = FALSE;
  unsigned long i = 0;
  unsigned short    wNextScreen;
        
  /* retrieve pointer to the function from the screen-tool-generated array */
  pfnEventFunction = aEventFns[wFnID];

  /* determine if the function is in the 2nd format.  functions which are in this format
     appear in the aKeyFnsInFormat2 array. */
  do
    {       
      if (aEventFnsInFormat2[i] == (pEventFn2ndFormat) pfnEventFunction)
        {
          fInFormat2 = TRUE;
          break;
        }
    } while (aEventFnsInFormat2[i++] != (pEventFn2ndFormat) 0xFFFFFFFF);

  /* make the call depending on the format the function is in.  note that for format 2,
     we have to cast the function pointer to that format before making the call. */
  if (fInFormat2 == FALSE)
    {
      unsigned short    wNext1 = 0;
      unsigned short    wNext2 = 0;

      if (bNumNextScreens >= 2)
        wNext2 = pNextScreens[1];

      if (bNumNextScreens >= 1)
        wNext1 = pNextScreens[0];

      wNextScreen = pfnEventFunction(wNext1, wNext2, pIntertask);
    }
  else
    wNextScreen = ((pEventFn2ndFormat) pfnEventFunction) (bNumNextScreens, pNextScreens, pIntertask);

  return (wNextScreen);
}

/* **********************************************************************
// FUNCTION NAME: fnOITCanEDMChangeParams
// DESCRIPTION: Returns TRUE if the OIT is currently on a screen that can
//              accept a transition to the OIT_SETUP_OPERATION-OIT_EDM_LOCK
//              mode.  A transition to that mode allows the EDM task to
//              change parameters.  This is a public function.
// AUTHOR: Joe Mozdzer
// *************************************************************************/

BOOL fnOITCanEDMChangeParams()
{
  BOOL          fRetval = FALSE;
  unsigned long i = 0;
  /* COMET screen table */

#ifdef JANUS
#ifdef G900
  static const unsigned short pEDMParmScreens[] =
    {   mMMIndiciaReady,
    mMMIndiciaError,
//    mMMRemotePostageSetting,
    0xFFFF
    };
#else
  static const unsigned short pEDMParmScreens[] =
    {   mSleep, 
    0xFFFF
    };
#endif /* end of G900 */
#else
#ifdef PHOENIX
  static const unsigned short pEDMParmScreens[] =
    {   mMMIndiciaReady,
    mMMIndiciaDisabled,
    mMMIndiciaError,
    mMMRemotePostageSetting, 
    0xFFFF
    };

#else                                          /* COMET screen table */

  static const unsigned short pEDMParmScreens[] =
    {  mMMIndiciaReady,
       mMMIndiciaWarning,
       mMMIndiciaError,
       mMMRemoteEDMLock,
       mSleep,
       0xFFFF
    };

#endif /* end of PHOENIX */
#endif /* end of JANUS */


  while (pEDMParmScreens[i] != 0xFFFF)
    {
      if (wCurrentScreenID == pEDMParmScreens[i])
    {
      fRetval = TRUE;
      break;
    }
      i++;
    }

  return (fRetval);
}

/* **********************************************************************
// FUNCTION NAME: fnOITIsHighValueWarningActive
// DESCRIPTION: Returns TRUE if the OIT is currently on the high value warning
//              screen.
// AUTHOR: Joe Mozdzer
// *************************************************************************/
BOOL fnOITIsHighValueWarningActive()
{

  if (wCurrentScreenID == GetScreen(SCREEN_HIGHPOSTAGE_EXCEEDED))
    return (TRUE);
  else
    return (FALSE);
}


/* **********************************************************************
// FUNCTION NAME: fnDiagMessageStatus
// DESCRIPTION: Returns TRUE if there is an error status
//
// AUTHOR: Steve Terebesi
// *************************************************************************/
BOOL fnDiagMessageStatus(void)
{
  return ((wDiagnosticMessageData[0] > 0));
}

/* **********************************************************************
// FUNCTION NAME: fnDiagMessageResponse
// DESCRIPTION: Returns message SUCCESSFUL or FAILURE response
//
// AUTHOR: Steve Terebesi
// *************************************************************************/
BOOL fnDiagMessageResponse(void)
{
  return (wDiagnosticMessageData[0]);
}

/* **********************************************************************
// FUNCTION NAME: fnWasteCapacity
// DESCRIPTION: Returns waste capacity value
//
// AUTHOR: Steve Terebesi
// *************************************************************************/
unsigned short fnWasteCapacity(void)
{
  return (wDiagnosticMessageData[0]);
}

/* **********************************************************************
// FUNCTION NAME: fnDataByte
// DESCRIPTION: Returns paramter data byte
//
// AUTHOR: Steve Terebesi
// *************************************************************************/
unsigned char fnDataByte(void)
{
  return (wDiagnosticMessageData[0]);
}

/* **********************************************************************
// FUNCTION NAME: fnHistogram
// DESCRIPTION: Returns histogram data pointer
//
// AUTHOR: Steve Terebesi
// *************************************************************************/
unsigned char *fnHistogram(void)
{
  return (pHistogramData);
}

/* **********************************************************************
// FUNCTION NAME: fnSetSpaceUnicode
// DESCRIPTION: Sets the given array to a NULL terminated string of unicode spaces.
//              This code used to be buried inside the SET_SPACE_UNICODE macro,
//              but it has been pulled into this function as an optimization.
//
// AUTHOR: Joe Mozdzer
// *************************************************************************/
void fnSetSpaceUnicode(unsigned short *pBuf, unsigned long lwLen)
{
    UINT32 i; 
    
    for (i=0; i < lwLen; i++)
    { 
        pBuf[i] = UNICODE_SPACE;
    }

    pBuf[lwLen - 1] = NULL_VAL;  
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnGetKeyMap
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct rKeyMap.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of rKeyMap
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      12/05/2005 Adam Liu Initial version
//
// *************************************************************************/
KEY_MAP   *fnGetKeyMap(void)
{
  return(&rKeyMap);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnOITGetTableTextDirectory
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct rTableTextDir.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of rTableTextDir
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      12/05/2005 Adam Liu Initial version
//
// *************************************************************************/
ELEMENT_DIRECTORY   *fnOITGetTableTextDirectory(void)
{
  return(&rTableTextDir);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnOITGetLanguageDirectory
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct rLanguageDir.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of rLanguageDir
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      12/05/2005 Adam Liu Initial version
//
// *************************************************************************/
LANGUAGE_DIRECTORY  *fnOITGetLanguageDirectory(void)
{
  return(&rLanguageDir);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnGetPreviousScreenID
//
// DESCRIPTION: 
//      Utility function that get the value of wPreviousScreenID.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The value of wPreviousScreenID
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      12/05/2005 Adam Liu Initial version
//
// *************************************************************************/
T_SCREEN_ID fnGetPreviousScreenID(void)
{
  return(wPreviousScreenID);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnOITSetToNormalVideo
//
// DESCRIPTION: 
//      Utility function to change the field to normal video mode
//
// INPUTS:
//      pAttributes - pointer to the attribute of the field
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  2/1/2006    Vincent Yi      Initial version
//
// *************************************************************************/
void fnOITSetToNormalVideo (UINT8 * pAttributes)
{
  *pAttributes &= ~REVERSE_MASK;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnOITSetToReverseVideo
//
// DESCRIPTION: 
//      Utility function to change the field to reverse video mode
//
// INPUTS:
//      pAttributes - pointer to the attribute of the field
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  2/1/2006    Vincent Yi      Initial version
//
// *************************************************************************/
void fnOITSetToReverseVideo (UINT8 * pAttributes)
{
  *pAttributes |= REVERSE_MASK; 
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnmProcessSYSAck
//
// DESCRIPTION: 
//      This function is called when the OI receives an OIT_ACK message from
//      SYS
//
// INPUTS:
//      pIntertask      - pointer to intertask message.  Not used.
//
// RETURNS:
//      SUCCESSFUL      - function exited successfully
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      3/13/2006   Steve Terebesi  Initial version
// *************************************************************************/
BOOL fnmProcessSYSAck(INTERTASK* pIntertask)
{
  //  keys & events can once again be processed */
  fIgnoreKeysAndEvents = FALSE;   

  //  Finish screen change processing
  (void)fnFinishScreenChange();

  return (SUCCESSFUL);
}



/**********************************************************************
        Private Functions
**********************************************************************/



/* *************************************************************************
// FUNCTION NAME: 
//      fnFinishScreenChange
//
// DESCRIPTION: 
//      This function is responsible for completing the steps necessary for
//      performing a screen change.
//
// INPUTS:
//      None
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      3/13/2006   Steve Terebesi  Initial version
// *************************************************************************/
static BOOL fnFinishScreenChange(void)
{
  /*  if we get to here, it is ok to officially change and paint the new screen */
  (void)OSStopTimer(SCREEN_TICK_TIMER);   /* stop the timer first so that it gets reset to its full duration */
  (void)OSStartTimer(SCREEN_TICK_TIMER);  /* start the 1 second continuous tick timer */

  fScreenTickEventQueued = FALSE;
  fAutoChangeOccurred    = FALSE;

  /* if user set a timer, the screen will refresh to default value after certain amount of time */
  (void)OSStopTimer(USER_TICK_TIMER);
  (void)OSChangeTimerDuration(USER_TICK_TIMER, CMOSSetupParams.wpUserSetPresetClearTimeout * 60 * 1000 );
  (void)OSStartTimer(USER_TICK_TIMER);

  fnPaintCurrentScreen(FALSE);

  /*  if we have buffered any keys during the screen transition process, now is the time to
      send them back to the main queue for processing */
  if (rKeyBuffer.fFull == TRUE)
    {
      struct 
      {
    unsigned char   bKeyCode;
    unsigned char   bDirection;
    unsigned char   fControl;
      } rKeydataMsg;      /* struct for building the RX_KEYDATA message */

      rKeydataMsg.bKeyCode   = rKeyBuffer.bKeyCode;
      rKeydataMsg.bDirection = rKeyBuffer.bDirection;
      rKeydataMsg.fControl   = rKeyBuffer.fControl;

      /*send the key message, making it look like it came from the keypad driver task */
      (void)OSSendIntertask(OIT, KEYTASK, KEY_RX_KEYDATA, BYTE_DATA, &rKeydataMsg, sizeof(rKeydataMsg));

      rKeyBuffer.fFull = FALSE;   /* buffer is now empty */
    }

  return TRUE;
}



/* *************************************************************************
// FUNCTION NAME:           fnRequestNewMode
// DESCRIPTION: 
//      Requests a mode change by sending a message to the system controller.  
//      Saves the ID# of the screen to go to if the system controller approves
//      the mode change.
//
// INPUTS:
//      ucEvent         - ID of the event to be processed by SYS
//      usNextScreen    - next screen ID
//      fIsModeChangePending - Set to TRUE if the screen post function and
//                      prefunction have already been run, and we don't need
//                      to do them again.
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//  1. If no entry is found in the event-to-screen table for the event, then 
//     this is a serious bug, that should never happen.  This is why it is put
//     into the exception.log file, so we have a way to find out if it happened, 
//     and what the missing entry is, even from a meter that is out in the field.
//
// MODIFICATION HISTORY:
//  2011.07.19 Clarisa Bellamy - Modified the exception handling when no entry 
//                      is found in the event-to-screen table for the event.  
//                      Replaced call to fnDisplayErrorDirect(), which just puts 
//                      an entry in the syslog, with directly putting an entry 
//                      in the syslog, then one in the exception.log FFS file,
//                      then reporting a meter error.
//                      - Uncommented the code to stop and restart the DEADMAN
//                        timer, so OIT will timeout waiting for a changemode 
//                        (event) reply.
//                      - Save the pending mode-change event, to use later in 
//                        syslog if there is no response.  
//                      - Updated comments.
//  11/18/2009  Raymond Shen    Add the third parameter "fIsModeChangePending" to indicate
//                              whether the pre / post function shall be called when OIT
//                              changes screen later on.
//  3/13/2006   Steve Terebesi  Initial version
// *************************************************************************/
void fnRequestNewMode( const UINT8  ucEvent, const UINT16 usNextScreen, const BOOL fIsModeChangePending )
{
  UINT8     pMsgData[2];    /* send 2 bytes- 1st is event, 2nd is 0 */
  UINT32    i;

    pMsgData[0] = ucEvent;
    pMsgData[1] = 0;

    // Send a change mode request to the system controller, then set flag to 
    //  ignore keys and events.
    (void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);
    fIgnoreKeysAndEvents = TRUE;    /* don't process keys & events while in this transitional state */

    // Start the deadman timer for a response from SYS.  Stp[ the timer first so
    //  that it gets reset to its full duration.
    (void)OSStopTimer(OIT_DEADMAN_TIMER);   
    (void)OSStartTimer(OIT_DEADMAN_TIMER);
    fModeChangePending = fIsModeChangePending;
    // If we are waiting for  areply, make sure we save what we waint for.
    if( fModeChangePending )
    {
        ucPendingModeChangeEvent = ucEvent;
    }

    // Update the EventToScreen table to save the next screen ID */
    i = 0;
    while (StEventNextScreen[i].ulScreenID != END_OF_SCREEN_MODE_TABLE)
    {
        // Find the entry in the table for this event..
        if (StEventNextScreen[i].ucEvent == ucEvent)
        {
            // Entry found for this event, save the next screen, and exit loop.
            StEventNextScreen[i].ulScreenID = usNextScreen;
            break;
        }
        i++;
    }

    // If there is no entry with this event in it, then this is a severe error.
    //  Put an entry in the syslog, and go to the error screen.
    if (StEventNextScreen[i].ulScreenID == END_OF_SCREEN_MODE_TABLE)
    {
        sprintf( scratchpad100, "ERROR!! Evnt x%X not in EvntToScr Table", ucEvent );
        fnDumpStringToSystemLog( scratchpad100 );
        logException( scratchpad100 );
        fnReportMeterError( ERROR_CLASS_OIT, ECOIT_MISSING_TABLE_ENTRY );
    }

    return;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnOITGetTreeDirectory
//
// DESCRIPTION: 
//      Utility function that get the pointer of Tree DIR.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of rLanguageDir
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      05/05/2006 F.Z Initial version
//
// *************************************************************************/
TREE_DIRECTORY  *fnOITGetTreeDirectory(void)
{
  return(&rTreeDir);
}
/* *************************************************************************
// FUNCTION NAME: 
//      fnPreProcessOITMessage
//
// DESCRIPTION: 
//      Before process the input message, check and process the OIT timers 
//      as per this message. This function is separated from 
//      fnOperatorInterfaceTask
//
// INPUTS:
//      pstCommand  - the input message
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  1/04/2006   Vincent Yi      Initial version 
//
// *************************************************************************/
static void fnPreProcessOITMessage (const INTERTASK * pstCommand)
{

  /* if we have received a message, first check if the message timer
     is currently active */
  if (stMsgTOWaitCtrl.fWaiting == TRUE)
    {
      /* if it is, check if the message received matches the message
         the timer is waiting for, and if so, the timer can be
         shut off */
      if ((stMsgTOWaitCtrl.bMsgTimeoutExpectedTaskID == pstCommand->bSource) &&
      (stMsgTOWaitCtrl.bMsgTimeoutExpectedMsgID == pstCommand->bMsgId))
        {
          stMsgTOWaitCtrl.fWaiting = FALSE;
          (void)OSStopTimer(OIT_MESSAGE_TIMER);
        }
    }

  /* reset sleep timer if necessary */
  if (/*(pstCommand->bSource == SYS) ||*/
      (pstCommand->bSource == KEYTASK) ||
      (pstCommand->bSource == DISTTASK)|| 
      ((pstCommand->bSource == CM) && (pstCommand->bMsgId == CO_MAIL_PROGRESS)) || 
      ((pstCommand->bSource == PLAT) && (pstCommand->bMsgId == PLAT_UPDATE))|| 
      ((pstCommand->bSource == OIT) && (pstCommand->bMsgId == OIT_INFRA_SCREEN_TIMEOUT))
      /*|| (pstCommand->bSource == PRINTER)*/)
    {
      fnResetSleepTimer();
      fnResetNormalPresetTimer();
    }
}

/* **********************************************************************
// FUNCTION NAME: fnProcessOITMessage
// DESCRIPTION: Private message processing engine for the OIT.
//              This function takes an intertask message and a lookup table
//              as input, and either calls a message processing function or
//              directly generates an OIT event, depending on the entry
//              in the table.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pMsgTable- The message table that maps an input message to either
//                      a function call or a state transition event.
//          pMsg- The message being processed.
//
// OUTPUT:  returns TRUE if the message found in the lookup table and
//              processed
//          FALSE is returned if there was not a table entry for this message
//
// *************************************************************************/
static BOOL fnProcessOITMessage(const   OIT_MSG_TABLE_ENTRY *   pMsgTable, 
                INTERTASK           *   pMsg)
{
  register unsigned long    i = 0;
  unsigned char         bTableTaskID;
  BOOL                  fRetval = FALSE;

  bTableTaskID = pMsgTable[i].bMsgSource;

  while (bTableTaskID != OIT_MSG_END_OF_TABLE)
    {
      if ((bTableTaskID == (unsigned short) pMsg->bSource) &&
      (pMsgTable[i].bMsgID == pMsg->bMsgId))
        {
          if (((pMsgTable[i].wByte0 == OIT_DC) ||
               (pMsgTable[i].wByte0 == (unsigned short) pMsg->IntertaskUnion.bByteData[0])) &&
              ((pMsgTable[i].wByte1 == OIT_DC) ||
               (pMsgTable[i].wByte1 == (unsigned short) pMsg->IntertaskUnion.bByteData[1])))
            {
              fRetval = TRUE;

              /* either call the message function or directly generate the event */
              if (pMsgTable[i].wEventToGenerate == 0xFFFF)
                (void)pMsgTable[i].pfnMsgFunction(pMsg);
              else
                (void)fnProcessEvent( (UINT8)(pMsgTable[i].wEventToGenerate), pMsg);
    
              break;
            }
        }
      bTableTaskID = pMsgTable[++i].bMsgSource;
    }

  return (fRetval);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnPostProcessOITMessage
//
// DESCRIPTION: 
//      All works after processing the input message. This function is separated 
//      from fnOperatorInterfaceTask
//
// INPUTS:
//      pstCommand  - the input message
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  1/04/2006   Vincent Yi      Initial version 
//
// *************************************************************************/
static void fnPostProcessOITMessage (const INTERTASK * pstCommand)
{
  T_SCREEN_ID       usNextScreen = 0;
  SINT8         cStatus;
  pPreFn            pContFunctionPre = (pPreFn) NULL_PTR;
  pPostFn           pContFunctionPost = (pPostFn) NULL_PTR;
  UINT32            ulMsgsAwaited;
  
  /* after processing the input message, check what messages (if any) we are waiting for.  if
     we have now received all messages that we were waiting for, then we can call
     the continuation function */
  if ((rWaitCtrl.lwMsgsAwaited != 0) &&
      ((rWaitCtrl.lwMsgsAwaited & rWaitCtrl.lwMsgsReceived) == rWaitCtrl.lwMsgsAwaited))
    {
      /* call the continuation function */
      if (rWaitCtrl.fIsPreFunction == TRUE)
        {
          cStatus = rWaitCtrl.pfnNextPreFunction((void (**) ()) &pContFunctionPre,
                                                 &ulMsgsAwaited, &usNextScreen);
        }
      else
        {
          cStatus = rWaitCtrl.pfnNextPostFunction((void (**) ()) &pContFunctionPost,
                                                   &ulMsgsAwaited);
        }

      if (cStatus == PREPOST_WAITING)
        {
          /* if it we need to wait for another message, prepare the wait control block */
          rWaitCtrl.lwMsgsAwaited = ulMsgsAwaited;
          rWaitCtrl.lwMsgsReceived = 0;
          rWaitCtrl.pfnNextPreFunction = pContFunctionPre;
          rWaitCtrl.pfnNextPostFunction = pContFunctionPost;
        }
      else
        {
          /* otherwise we are done processing the pre or post function sequence.
             reset the wait control block. */
          rWaitCtrl.lwMsgsReceived = 0;
          rWaitCtrl.lwMsgsAwaited = 0;

          /* if the continuation function returns a zero as the next screen, 
             it means we should go to the original screen (that owns the 
             pre/post function).  otherwise we want to go to the new screen 
             returned by the continuation function. */
          if (rWaitCtrl.fIsPreFunction == TRUE)
            {
              if (usNextScreen == 0)
                {
                  /* if we completed a pre function call that did not reroute the screen, we
                     can now officially change to the screen that was waiting.  we don't
                     want to call the pre or post functions this time since that is what
                     was just completed to get to this point. */
                  fnChangeScreen(rWaitCtrl.wScreenWaiting, FALSE, FALSE);
                }
              else
                {
                  /* if the pre function call did reroute the screen, we must call the
                     pre function of the new screen */
                  fnChangeScreen(usNextScreen, TRUE, FALSE);
                }
            }
          else
            {
              /* if a post function was just completed, attempt to put up the next screen
             after calling its pre function */
              fnChangeScreen(rWaitCtrl.wScreenWaiting, TRUE, FALSE);
            }
        }
    } /* end "wait" processing */

  /* if not waiting for any message, generate the auto change event which can be used
     on some screens to automatically go somewhere or do something without waiting for
     a key or some other message to generate an event.*/
  if ((rWaitCtrl.lwMsgsAwaited == 0) && !fAutoChangeOccurred)
    {
      fAutoChangeOccurred = TRUE;
      (void)fnProcessEvent( EVENT_AUTO_CHANGE, NULL_PTR );
    }

  /* free any partition data if it was received */
  if (pstCommand->bMsgType == PART_PTR_DATA)
    {
      lwNumPartitionsRcvd++;    /* tick a counter so we can see if this is ever nonzero */
      (void)OSReleaseMemory( pstCommand->IntertaskUnion.PointerData.pData );
    }
}

/* **********************************************************************
// FUNCTION NAME: fnBuildFixedTextList
// DESCRIPTION: Creates a linked list of LCD_STRING structures containing
//              the fixed text for a screen.  The global structure
//              rCurrentFixedTextRecord must contain the screen's information
//              before this function gets called.
// AUTHOR: Joe Mozdzer
//
// OUTPUTS: A pointer to the head node is returned in ppHead.
//          A pointer to the tail node is returned in ppTail.
// *************************************************************************/
static void fnBuildFixedTextList(LCD_STRING **ppHead, LCD_STRING **ppTail)
{
    LCD_STRING      *pPrevious, *pString, *pHead;
    unsigned char   i;
    unsigned char   *pFTRecordStart;    /* pointer to the start of the current fixed text record
       being worked on */
    unsigned char   *pFTRecordParse;    /* pointer within the current fixed text record that is used
       to process the pieces that it is composed of */
    unsigned char   bRecordLen;         /* length of the current fixed text record */

    pString = NULL_PTR;
    pHead = NULL_PTR;
    pPrevious = NULL_PTR;
    /* create a linked list of all fixed text to be shown on the display */

    /* initialize current fixed text record pointer to the first one */
    pFTRecordStart = rCurrentFixedTextRecord.pFirstTextEntry;

    for (i = 0; i < rCurrentFixedTextRecord.bNumEntries; i++)
    {
        /* first allocate an LCD_STRING structure */
        if (OSGetMemory((void **) &pString, sizeof(LCD_STRING)) != SUCCESSFUL)
        {
            fnDisplayErrorDirect("Out of memory", 10, OIT);
            break;
        }
        else
        {
            /* if this is not the first structure in the linked list, update the
                 next node pointer of the previous node. if it was the first, save
                 that pointer for later use. */
            if( pPrevious != NULL_PTR )
                pPrevious->pNext = pString;
            else
                pHead = pString;

            /* initialize the parse pointer to the start of the current record */
            pFTRecordParse = pFTRecordStart;

            /* get the record length, then copy the string data from flash to the node we just created */
            bRecordLen = *pFTRecordParse++;

            if (rLanguageDir.wComponentFormat < 0x0200)
            {
                pString->bXCoordinate = *pFTRecordParse++;
                pString->bYCoordinate = *pFTRecordParse++;
#ifdef JANUS
                JANUS_ADJUST_COORDINATES(pString);
#endif
                pString->bFont = *pFTRecordParse++;
                pString->fAttributes = *pFTRecordParse++;
                pString->wFGColor = FP_COLORLCD_USE_DEFAULT;
                pString->wBGColor = FP_COLORLCD_USE_DEFAULT;
                EndianAwareArray16Copy(pString->pUnicodeString, pFTRecordParse, bRecordLen - 5); /* there are 5 bytes beside the unicode part */
            }
            else
            {
                if (rLanguageDir.wComponentFormat < 0x0301)
                {
                    EndianAwareCopy(&pString->bXCoordinate, pFTRecordParse, 2);
                    pFTRecordParse += 2;
                    EndianAwareCopy(&pString->bYCoordinate, pFTRecordParse, 2);
                    pFTRecordParse += 2;
#ifdef JANUS
                    JANUS_ADJUST_COORDINATES(pString);
#endif
                    pString->bFont = *pFTRecordParse++;
                    pString->fAttributes = *pFTRecordParse++;
                    pString->wFGColor = FP_COLORLCD_USE_DEFAULT;    // color does not come from the DB, so use defaults in LCD driver
                    pString->wBGColor = FP_COLORLCD_USE_DEFAULT;
                    EndianAwareArray16Copy(pString->pUnicodeString, pFTRecordParse, bRecordLen - 7); /* there are 7 bytes beside the unicode part
                      in format 02.00+ */
                }
                else
                {   /* format 3.01 + supports color */
                    EndianAwareCopy(&pString->bXCoordinate, pFTRecordParse, 2);
                    pFTRecordParse += 2;
                    EndianAwareCopy(&pString->bYCoordinate, pFTRecordParse, 2);
                    pFTRecordParse += 2;
#ifdef JANUS
                    JANUS_ADJUST_COORDINATES(pString);
#endif
                    pString->bFont = *pFTRecordParse++;
                    pString->fAttributes = *pFTRecordParse++;
                    // FG color, then BG color
                    EndianAwareCopy(&pString->wFGColor, pFTRecordParse, 2);
                    pFTRecordParse += 2;
                    EndianAwareCopy(&pString->wBGColor, pFTRecordParse, 2);
                    pFTRecordParse += 2;
                    EndianAwareArray16Copy(pString->pUnicodeString, pFTRecordParse, bRecordLen - 11); /* there are 11 bytes beside the unicode part
                                                                                                                in format 3.01 + */

//RAM added
                    // For debugging:
                    fnSystemLogEntry( SYSLOG_TEXT, (char*)pString->pUnicodeString, MAX_LCD_CHARS_PER_LINE );
//End
                }
            }

            pString->rGraphic.fUsed = FALSE;

            /* mark the current node as the previous one, so we can update its next node pointer
                 on the next pass through the loop */
            pPrevious = pString;

            /* update pointer to the next record */
            pFTRecordStart += bRecordLen;
        }

    }
    /* as long as there is at least one record in the list, set the next node pointer
    of the last node to NULL_PTR */
    if( (pPrevious != NULL_PTR) && (pString != NULL_PTR ) )
    {
        pString->pNext = NULL_PTR;
    }

    *ppHead = pHead;
    *ppTail = pString;
    return;
}

//#if 0
/* **********************************************************************
// FUNCTION NAME: fnBuildFixedGraphicList
// DESCRIPTION: Creates a linked list of LCD_STRING structures containing
//              the fixed graphics for a screen.  The global structure
//              rCurrentFixedGraphicRecord must contain the screen's information
//              before this function gets called.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  fOnTop- if set to true, the function builds a list of only those
//                  records that are designated as being brought forward in the
//                  attributes byte.  if set to false, the list contains only
//                  records that are not designated as being brought forward.
//
// OUTPUTS: A pointer to the head node is returned in ppHead.
//          A pointer to the tail node is returned in ppTail.
// *************************************************************************/

static void fnBuildFixedGraphicList(LCD_STRING **ppHead, LCD_STRING **ppTail, BOOL fOnTop)
{
    LCD_STRING      *pPrevious, *pString, *pHead;
    unsigned char   i;
    unsigned char   *pFGRecordStart;    /* pointer to the start of the current fixed text record
       being worked on */
    unsigned char   *pFGRecordParse;    /* pointer within the current fixed text record that is used
       to process the pieces that it is composed of */
    unsigned char   bRecordLen;         /* length of the current fixed text record */
    unsigned char   bAttributes;
    unsigned long  pS;

    /* create a linked list of all fixed graphics to be shown on the display */

    /* initialize current fixed graphic record pointer to the first one */
    pFGRecordStart = rCurrentFixedGraphicRecord.pFirstEntry;

    pPrevious = NULL_PTR;
    pHead = NULL_PTR;
    pString = NULL_PTR;

    /* fixed graphics not supported prior to format 02.00, so bail now if we are using an older version */
    if (rLanguageDir.wComponentFormat < 0x0200)
    {
        *ppHead = NULL_PTR;
        *ppTail = NULL_PTR;
        goto xit;
    }

    for (i = 0; i < rCurrentFixedGraphicRecord.bNumEntries; i++)
    {
        bRecordLen = *pFGRecordStart;
        bAttributes = *(pFGRecordStart + 6);

        if (((bAttributes & 0x08) && fOnTop) || (!(bAttributes & 0x08) && !fOnTop))
        {

            /* first allocate an LCD_STRING structure */
            if (OSGetMemory((void **) &pString, sizeof(LCD_STRING)) != SUCCESSFUL)
            {
                fnDisplayErrorDirect("Out of memory", 11, OIT);
                break;
            }
            else
            {
                /* if this is not the first structure in the linked list, update the
                 next node pointer of the previous node. if it was the first, save
                 that pointer for later use. */
                if( pPrevious != NULL_PTR )
                    pPrevious->pNext = pString;
                else
                    pHead = pString;

                /* initialize the parse pointer to the start of the current record */
                pFGRecordParse = pFGRecordStart;

                /* get the record length, then copy the string data from flash to the node we just created */
                bRecordLen = *pFGRecordParse++;
                EndianAwareCopy(&pString->bXCoordinate, pFGRecordParse, sizeof(unsigned short));
                pFGRecordParse += 2;
                EndianAwareCopy(&pString->bYCoordinate, pFGRecordParse, sizeof(unsigned short));
                pFGRecordParse += 2;
                pS = ( unsigned long )pString;
#ifdef JANUS
                JANUS_ADJUST_COORDINATES(pString);
#endif
                pString->bFont = *pFGRecordParse++;
                pString->fAttributes = *pFGRecordParse++;
                if (rLanguageDir.wComponentFormat < 0x0301)
                {
                    pString->wFGColor = FP_COLORLCD_USE_DEFAULT;    // color does not come from the DB, so use defaults in LCD driver
                    pString->wBGColor = FP_COLORLCD_USE_DEFAULT;
                    EndianAwareArray16Copy(pString->pUnicodeString, pFGRecordParse, bRecordLen - 7); /* there are 7 bytes beside the unicode part */
                }
                else
                {
                    // FG color, then BG color
                    EndianAwareCopy(&pString->wFGColor, pFGRecordParse, 2);
                    pFGRecordParse += 2;
                    EndianAwareCopy(&pString->wBGColor, pFGRecordParse, 2);
                    pFGRecordParse += 2;
                    EndianAwareArray16Copy(pString->pUnicodeString, pFGRecordParse, bRecordLen - 11); /* there are 11 bytes beside the unicode part */
                }

                pString->rGraphic.fUsed = FALSE;

                /* mark the current node as the previous one, so we can update its next node pointer
                 on the next pass through the loop */
                pPrevious = pString;
            }
            /* update pointer to the next record */
            pFGRecordStart += bRecordLen;
        }
    }

    /* as long as there is at least one record in the list, set the next node pointer
    of the last node to NULL_PTR */
    if( (pPrevious != NULL_PTR) && ( pString != NULL_PTR) )
    {
        pString->pNext = NULL_PTR;
    }

    *ppHead = pHead;
    *ppTail = pString;

xit:
    return;
}

/* **********************************************************************
// FUNCTION NAME: fnBuildVarGraphicsList
// DESCRIPTION: Creates a linked list of LCD_STRING structures containing
//              the variable graphics for a screen.  The global structure
//              rCurrentVarGraphicsRecord must contain the screen's information
//              before this function gets called.  Note that a cursor
//              is not supported for variable graphics.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  fInit - if true, the init function will be called for each field
//                  if false, the refresh function will be called for each field
//
//          fOnTop- if set to true, the function builds a list of only those
//                  records that are designated as being brought forward in the
//                  attributes byte.  if set to false, the list contains only
//                  records that are not designated as being brought forward.
//
// OUTPUTS: A pointer to the head node is returned in ppHead.
//          A pointer to the tail node is returned in ppTail.
// *************************************************************************/
static void fnBuildVarGraphicsList(LCD_STRING **ppHead, LCD_STRING **ppTail, BOOL fInit, BOOL fOnTop)
{
    LCD_STRING          *pPrevious, *pString, *pHead;
    unsigned char       i;
    unsigned char       *pVGRecordStart;    /* pointer to the start of the current var graphic record
       being worked on */
    unsigned char       *pVGRecordParse;    /* pointer within the current var graphic record that is used
       to process the pieces that it is composed of */
    unsigned char       bRecordLen; /* length of the current fields record */
    unsigned short      wInitFnID, wRefreshFnID;
    unsigned char       bNumTextItems;
    BOOL                fGraphicFnStatus;
    unsigned short      wMaxXSize, wMaxYSize;
    unsigned char       bAttributes;


    pString = NULL_PTR;
    
    /* create a linked list of all variable graphics to be shown on the display */


    /* variable graphics not supported prior to format 02.00, so bail now if we are using an older version */
    if (rLanguageDir.wComponentFormat < 0x0200)
    {
        *ppHead = NULL_PTR;
        *ppTail = NULL_PTR;
        goto xit;
    }

    /* initialize current fields record pointer to the first one */
    pVGRecordStart = rCurrentVarGraphicRecord.pFirstEntry;

    pPrevious = NULL_PTR;
    pHead = NULL_PTR;

    for (i = 0; i < rCurrentVarGraphicRecord.bNumEntries; i++)
    {
        bRecordLen = *pVGRecordStart;
        bAttributes = *(pVGRecordStart + 9);
        if (((bAttributes & 0x08) && fOnTop) || (!(bAttributes & 0x08) && !fOnTop))
        {

            /* first allocate an LCD_STRING structure */
            if (OSGetMemory((void **) &pString, sizeof(LCD_STRING)) != SUCCESSFUL)
            {
                fnDisplayErrorDirect("Out of memory", 12, OIT);
                break;
            }
            else
            {
                /* if this is not the first structure in the linked list, update the
                 next node pointer of the previous node. if it was the first, save
                 that pointer for later use. */
                if( pPrevious != NULL_PTR )
                    pPrevious->pNext = pString;
                else
                    pHead = pString;

                /* initialize the parse pointer to the start of the current record */
                pVGRecordParse = pVGRecordStart;

                /* get the record length */
                bRecordLen = *pVGRecordParse++;

                /* get the display coordinates for the variable graphic */
                EndianAwareCopy(&pString->bXCoordinate, pVGRecordParse, sizeof(unsigned short));
                pVGRecordParse += 2;
                EndianAwareCopy(&pString->bYCoordinate, pVGRecordParse, sizeof(unsigned short));
                pVGRecordParse += 2;
#ifdef JANUS
                JANUS_ADJUST_COORDINATES(pString);
#endif

                /* get the maximum x size and y size of the variable graphic and save in local variable */
                EndianAwareCopy(&wMaxXSize, pVGRecordParse, sizeof(unsigned short));
                pVGRecordParse += 2;
                EndianAwareCopy(&wMaxYSize, pVGRecordParse, sizeof(unsigned short));
                pVGRecordParse += 2;

                /* get the attributes byte */
                pString->fAttributes = *pVGRecordParse++;

                /* get the init and refresh function IDs */
                EndianAwareCopy(&wInitFnID, pVGRecordParse, 2);
                pVGRecordParse += 2;
                EndianAwareCopy(&wRefreshFnID, pVGRecordParse, 2);
                pVGRecordParse += 2;

                /* get the font ID (not used if direct graphic pointer returned) */
                pString->bFont = *pVGRecordParse++;

                if (rLanguageDir.wComponentFormat >= 0x0301)
                {
                    // FG color, then BG color
                    EndianAwareCopy(&pString->wFGColor, pVGRecordParse, 2);
                    pVGRecordParse += 2;
                    EndianAwareCopy(&pString->wBGColor, pVGRecordParse, 2);
                    pVGRecordParse += 2;
                }
                else
                {
                    pString->wFGColor = FP_COLORLCD_USE_DEFAULT;    // color does not come from the DB, so use defaults in LCD driver
                    pString->wBGColor = FP_COLORLCD_USE_DEFAULT;
                }
                

                bNumTextItems = *pVGRecordParse++;

                /* functions will return variable graphic structure OR symbol ID */
                if (fInit)
                    fGraphicFnStatus = aGraphicInitFns[wInitFnID](&pString->rGraphic, pString->pUnicodeString,
                        bNumTextItems, pVGRecordParse, &bAttributes);
                else
                    fGraphicFnStatus = aGraphicRefreshFns[wRefreshFnID](&pString->rGraphic, pString->pUnicodeString,
                        bNumTextItems, pVGRecordParse, &bAttributes);

                /* if the function wants to display a graphic directly, make sure its dimensions do not
                 exceed the max allowed.  */
                if ((fGraphicFnStatus == SUCCESSFUL) && (pString->rGraphic.fUsed == TRUE) &&
                    ((pString->rGraphic.wXSize > wMaxXSize) || (pString->rGraphic.wYSize > wMaxYSize)))
                {
                    pString->pUnicodeString[0] = NULL_VAL;
                    pString->rGraphic.fUsed = FALSE;
                }

                /* if the init or refresh function failed, we stop building any more fields and exit the
                 function with failure status.  the field function that failed should have already
                 called the function that requests the error mode. */
                if (fGraphicFnStatus != SUCCESSFUL)
                {
                    pString->pUnicodeString[0] = NULL_VAL;
                    pPrevious = pString;
                    break;
                }
                else
                {
                    /* if we were successful, make sure the "string" is null terminated.  we do this
                         at index 1, since a variable graphic function will not want to display
                         a string of graphics.  this allows the variable graphic functions to
                         only worry about returning a single unicode char and not null terminating them. */
                    pString->pUnicodeString[1] = NULL_VAL;
                }

                /* Check reverse mask of bAttributes and  pString->fAttributes , change pString->fAttributes reverse bit
                 to bAttributes reverse bit if changed. */
                if ((pString->fAttributes & REVERSE_MASK) != (bAttributes & REVERSE_MASK))
                {
                    if (bAttributes & REVERSE_MASK)
                    {
                        //if reverse flag is on
                        pString->fAttributes |= REVERSE_MASK;
                    }
                    else
                    {
                        //if reverse flag is off
                        pString->fAttributes &= ~REVERSE_MASK;
                    }
                }


                /* mark the current node as the previous one, so we can update its next node pointer
                 on the next pass through the loop */
                pPrevious = pString;
            }

            /* update pointer to the next record */
            pVGRecordStart += bRecordLen;
        }
    }

    /* as long as there is at least one record in the list, set the next node pointer
        of the last node to NULL_PTR */
    if( (pPrevious != NULL_PTR) && ( pString != NULL_PTR) )
    {
        pString->pNext = NULL_PTR;
    }

    *ppHead = pHead;
    *ppTail = pString;

xit:return;
}
//#endif

/* **********************************************************************
// FUNCTION NAME: fnBuildFieldsList
// DESCRIPTION: Creates a linked list of LCD_STRING structures containing
//              the field text for a screen.  The global structure
//              rCurrentFieldsRecord must contain the screen's information
//              before this function gets called.  This function also
//              creates an LCD_CURSOR structure that has the cursor information
//              for a screen.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  fInit - if true, the init function will be called for each field
//                  if false, the refresh function will be called for each field
//
// OUTPUTS: A pointer to the head node is returned in ppHead.
//          A pointer to the tail node is returned in ppTail.
//          A pointer to the cursor structure is returned in ppCursor.  NULL_PTR
//          is returned if there is no cursor information for a screen.
// *************************************************************************/
static void fnBuildFieldsList(LCD_STRING **ppHead, LCD_STRING **ppTail, LCD_CURSOR **ppCursor, BOOL fInit)
{
    LCD_STRING      *pPrevious, *pString, *pHead;
    unsigned char   i;
    unsigned char   *pFLDRecordStart;   /* pointer to the start of the current fields record
       being worked on */
    unsigned char   *pFLDRecordParse;   /* pointer within the current fields record that is used
       to process the pieces that it is composed of */
    unsigned char   bRecordLen; /* length of the current fields record */
    unsigned char   bFieldLen, bFieldCtrl;
    unsigned short  wInitFnID, wRefreshFnID;
    unsigned char   bNumTextItems;
    BOOL            fFieldFnStatus;
    UINT8           bAttributes;

    pString = NULL_PTR;

    /* create a linked list of all fields to be shown on the display */


    /* initialize current fields record pointer to the first one */
    pFLDRecordStart = rCurrentFieldsRecord.pFirstFieldEntry;

    pPrevious = NULL_PTR;
    pHead = NULL_PTR;
    *ppCursor = NULL_PTR;


    for (i = 0; i < rCurrentFieldsRecord.bNumEntries; i++)
    {
        /* initialize the parse pointer to the start of the current record and take an advanced peek
         at the record length and the field ctrl byte (to check the field type) */
        pFLDRecordParse = pFLDRecordStart;
        if (rLanguageDir.wComponentFormat < 0x0200)
            bFieldCtrl = *(pFLDRecordParse + 6);
        else
            bFieldCtrl = *(pFLDRecordParse + 10);
        bRecordLen = *pFLDRecordParse;

        if ((bFieldCtrl & FIELD_TYPE_MASK) != (TYPE_NESTED))    /* if this is a nested field type, don't paint it */
        {

            /* first allocate an LCD_STRING structure */
            if (OSGetMemory((void **) &pString, sizeof(LCD_STRING)) != SUCCESSFUL)
            {
                fnDisplayErrorDirect("Out of memory", 13, OIT);
                break;
            }
            else
            {
                /* if this is not the first structure in the linked list, update the
                 next node pointer of the previous node. if it was the first, save
                 that pointer for later use. */
                if( pPrevious != NULL_PTR )
                    pPrevious->pNext = pString;
                else
                    pHead = pString;

                /* get the record length, then copy the string data from flash to the node we just created */
                bRecordLen = *pFLDRecordParse++;

                if (rLanguageDir.wComponentFormat < 0x0200)
                {
                    pString->bXCoordinate = *pFLDRecordParse++;
                    pString->bYCoordinate = *pFLDRecordParse++;
#ifdef JANUS
                    JANUS_ADJUST_COORDINATES(pString);
#endif
                    pString->bFont = *pFLDRecordParse++;
                    pString->fAttributes = *pFLDRecordParse++;
                    bFieldLen = *pFLDRecordParse++;
                    bFieldCtrl = *pFLDRecordParse++;
                }
                else
                {
                    EndianAwareCopy(&pString->bXCoordinate, pFLDRecordParse, 2);                   
					pFLDRecordParse += 2;
                    EndianAwareCopy(&pString->bYCoordinate, pFLDRecordParse, 2);
                    pFLDRecordParse += 2;
#ifdef JANUS
                    JANUS_ADJUST_COORDINATES(pString);
#endif
                    pString->bFont = *pFLDRecordParse++;
                    pString->fAttributes = *pFLDRecordParse++;
                    pFLDRecordParse += 2;       /* skip over the field width in pixels for now */
                    bFieldLen = *pFLDRecordParse++;
                    bFieldCtrl = *pFLDRecordParse++;
                }

                bAttributes = pString->fAttributes;
                pString->rGraphic.fUsed = FALSE;


                /* THESE 2 LINES CAN BE USEFUL FOR UNIT TESTING */
                /* bFieldCtrl ^= 0x80; */           /* this will change the justification of all fields- right becomes left,
                   left becomes right */
                /* pString->fAttributes ^= 0x02; */ /* this will reverse all field text.  this makes it easier to see where
                   the fields really begin and end */
                /* END OF UNIT TESTING STUFF */

                EndianAwareCopy(&wInitFnID, pFLDRecordParse, 2);
                pFLDRecordParse += 2;
                EndianAwareCopy(&wRefreshFnID, pFLDRecordParse, 2);
                pFLDRecordParse += 2;

                if (rLanguageDir.wComponentFormat >= 0x0301)
                {
                    /* if format 3.01 or greater, grab the foreground and background color */
                    EndianAwareCopy(&pString->wFGColor, pFLDRecordParse, 2);
                    pFLDRecordParse += 2;
                    EndianAwareCopy(&pString->wBGColor, pFLDRecordParse, 2);
                    pFLDRecordParse += 2;
                }
                else
                {
                    pString->wFGColor = FP_COLORLCD_USE_DEFAULT;    // color does not come from the DB, so use defaults in LCD driver
                    pString->wBGColor = FP_COLORLCD_USE_DEFAULT;
                }

                bNumTextItems = *pFLDRecordParse++;

                /* call either the init or refresh function for the field, depending on the parameter passed in */
                if (fInit)
                    fFieldFnStatus = aInitFns[wInitFnID](pString->pUnicodeString, bFieldLen, bFieldCtrl, bNumTextItems, pFLDRecordParse, &bAttributes);
                else
                    fFieldFnStatus = aRefreshFns[wRefreshFnID](pString->pUnicodeString, bFieldLen, bFieldCtrl, bNumTextItems, pFLDRecordParse, &bAttributes);

                /* if the init or refresh function failed, we stop building any more fields and exit the
                 function with failure status.  the field function that failed should have already
                 called the function that requests the error mode. */
                if (fFieldFnStatus != SUCCESSFUL)
                {
                    pString->pUnicodeString[0] = NULL_VAL;
                    pPrevious = pString;
                    break;
                }

//RAM added
                fnSystemLogEntry( SYSLOG_TEXT, (char*)pString->pUnicodeString, MAX_LCD_CHARS_PER_LINE );
//end
                /* Check reverse mask of bAttributes and  pString->fAttributes , change pString->fAttributes reverse bit
                 to bAttributes reverse bit if changed. */
                if ((pString->fAttributes & REVERSE_MASK) != (bAttributes & REVERSE_MASK))
                {
                    if (bAttributes & REVERSE_MASK)
                    {
                        //if reverse flag is on
                        pString->fAttributes |= REVERSE_MASK;
                    }
                    else
                    {
                        //if reverse flag is off
                        pString->fAttributes &= ~REVERSE_MASK;
                    }
                }
                /* if the field just processed uses a cursor and it is active, send the cursor
                 info to the LCD task */
                if ((bFieldCtrl & CURSOR_MASK) && (bCursorPosition != CURSOR_INACTIVE))
                {
                    /* double check that this is the only active cursor; freeze on error if not */
                    if( *ppCursor == NULL_PTR )
                    {
                        if (OSGetMemory((void **) ppCursor, sizeof(LCD_CURSOR)) != SUCCESSFUL)
                        {
                            fnDisplayErrorDirect("Out of memory", 14, OIT);
                            break;
                        }
                        else
                        {
                            if (bCursorPosition != CURSOR_OFF)
                            {
                                (*ppCursor)->fOn = true;
                                (*ppCursor)->wFieldXCoord = pString->bXCoordinate;
                                (*ppCursor)->wFieldYCoord = pString->bYCoordinate;
                                (*ppCursor)->bCursorPos = bCursorPosition;
                                (*ppCursor)->bFont = pString->bFont;
                                (*ppCursor)->bStyle = bCursorStyle;
                                wCursorBGColor = pString->wBGColor;
                                wCursorFGColor = pString->wFGColor;
                            }
                            else
                                (*ppCursor)->fOn = false;
                        }
                    }
                    else
                    {
                        fnDisplayErrorDirect("Attempt to use multiple cursors on one screen", 0, OIT);
                    }
                }

                /* mark the current node as the previous one, so we can update its next node pointer
                 on the next pass through the loop */
                pPrevious = pString;
            }

            /* update pointer to the next record */
            pFLDRecordStart += bRecordLen;
        }
    }

    /* as long as there is at least one record in the list, set the next node pointer
    of the last node to NULL_PTR */
    if( (pPrevious != NULL_PTR) && ( pString != NULL_PTR) )
    {
        pString->pNext = NULL_PTR;
    }

    *ppHead = pHead;
    *ppTail = pString;

    return;
}

/* **********************************************************************
// FUNCTION NAME: fnBuildFieldsListAfterError
// DESCRIPTION: Creates a linked list of LCD_STRING structures containing
//              the field text for a screen.  The global structure
//              rCurrentFieldsRecord must contain the screen's information
//              before this function gets called.  This function also
//              creates an LCD_CURSOR structure that has the cursor information
//              for a screen.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  fInit - if true, the init function will be called for each field
//                  if false, the refresh function will be called for each field
//
// OUTPUTS: A pointer to the head node is returned in ppHead.
//          A pointer to the tail node is returned in ppTail.
//          A pointer to the cursor structure is returned in ppCursor.  NULL_PTR
//          is returned if there is no cursor information for a screen.
//
// HISTORY: May-20-2010 Rocky Dai   Modified to fix Fraca GMSE00187696.
// *************************************************************************/
static void fnBuildFieldsListAfterError(LCD_STRING **ppHead, LCD_STRING **ppTail, LCD_CURSOR **ppCursor, BOOL fInit)
{
    LCD_STRING      *pPrevious, *pString, *pHead;
    unsigned char   i;
    unsigned char   *pFLDRecordStart;   /* pointer to the start of the current fields record
       being worked on */
    unsigned char   *pFLDRecordParse;   /* pointer within the current fields record that is used
       to process the pieces that it is composed of */
    unsigned char   bRecordLen; /* length of the current fields record */
    unsigned char   bFieldLen, bFieldCtrl;
    unsigned short  wInitFnID, wRefreshFnID;
    unsigned char   bNumTextItems;
    BOOL            fFieldFnStatus;
    UINT8           bAttributes;

    pString = NULL_PTR;
   
    /* create a linked list of all fields to be shown on the display */


    /* initialize current fields record pointer to the first one */
    pFLDRecordStart = rCurrentFieldsRecord.pFirstFieldEntry;

    pPrevious = NULL_PTR;
    pHead = NULL_PTR;
    *ppCursor = NULL_PTR;


    for (i = 0; i < rCurrentFieldsRecord.bNumEntries; i++)
    {
        /* initialize the parse pointer to the start of the current record and take an advanced peek
         at the record length and the field ctrl byte (to check the field type) */
        pFLDRecordParse = pFLDRecordStart;
        if (rLanguageDir.wComponentFormat < 0x0200)
            bFieldCtrl = *(pFLDRecordParse + 6);
        else
            bFieldCtrl = *(pFLDRecordParse + 10);
        bRecordLen = *pFLDRecordParse;

        if ((bFieldCtrl & FIELD_TYPE_MASK) != (TYPE_NESTED))    /* if this is a nested field type, don't paint it */
        {

            /* first allocate an LCD_STRING structure */
            if (OSGetMemory((void **) &pString, sizeof(LCD_STRING)) != SUCCESSFUL)
            {
                fnDisplayErrorDirect("Out of memory", 13, OIT);
                break;
            }
            else
            {
                /* if this is not the first structure in the linked list, update the
                 next node pointer of the previous node. if it was the first, save
                 that pointer for later use. */
                if( pPrevious != NULL_PTR )
                    pPrevious->pNext = pString;
                else
                    pHead = pString;

                /* get the record length, then copy the string data from flash to the node we just created */
                bRecordLen = *pFLDRecordParse++;

                if (rLanguageDir.wComponentFormat < 0x0200)
                {
                    pString->bXCoordinate = *pFLDRecordParse++;
                    pString->bYCoordinate = *pFLDRecordParse++;
#ifdef JANUS
                    JANUS_ADJUST_COORDINATES(pString);
#endif
                    pString->bFont = *pFLDRecordParse++;
                    pString->fAttributes = *pFLDRecordParse++;
                    bFieldLen = *pFLDRecordParse++;
                    bFieldCtrl = *pFLDRecordParse++;
                }
                else
                {
                	EndianAwareCopy(&pString->bXCoordinate, pFLDRecordParse, 2);
                    pFLDRecordParse += 2;
                    EndianAwareCopy(&pString->bYCoordinate, pFLDRecordParse, 2);
                    pFLDRecordParse += 2;
#ifdef JANUS
                    JANUS_ADJUST_COORDINATES(pString);
#endif
                    pString->bFont = *pFLDRecordParse++;
                    pString->fAttributes = *pFLDRecordParse++;
                    pFLDRecordParse += 2;       /* skip over the field width in pixels for now */
                    bFieldLen = *pFLDRecordParse++;
                    bFieldCtrl = *pFLDRecordParse++;
                }

                bAttributes = pString->fAttributes;
                pString->rGraphic.fUsed = FALSE;


                /* THESE 2 LINES CAN BE USEFUL FOR UNIT TESTING */
                /* bFieldCtrl ^= 0x80; */           /* this will change the justification of all fields- right becomes left,
                   left becomes right */
                /* pString->fAttributes ^= 0x02; */ /* this will reverse all field text.  this makes it easier to see where
                   the fields really begin and end */
                /* END OF UNIT TESTING STUFF */

                EndianAwareCopy(&wInitFnID, pFLDRecordParse, 2);
                pFLDRecordParse += 2;
                EndianAwareCopy(&wRefreshFnID, pFLDRecordParse, 2);
                pFLDRecordParse += 2;

                if (rLanguageDir.wComponentFormat >= 0x0301)
                {
                    /* if format 3.01 or greater, grab the foreground and background color */
                	EndianAwareCopy(&pString->wFGColor, pFLDRecordParse, 2);
                    pFLDRecordParse += 2;
                    EndianAwareCopy(&pString->wBGColor, pFLDRecordParse, 2);
                    pFLDRecordParse += 2;
                }
                else
                {
                    pString->wFGColor = FP_COLORLCD_USE_DEFAULT;    // color does not come from the DB, so use defaults in LCD driver
                    pString->wBGColor = FP_COLORLCD_USE_DEFAULT;
                }

                bNumTextItems = *pFLDRecordParse++;

                /* call either the init or refresh function for the field, depending on the parameter passed in */
                if (fInit){
                    
                    //There's no need to call fnfStdEntryInit init function, because every field on this screen has
                    //its own init function. To fix Fraca GMSE00187696 --Rocky Dai 
                    
                    /*if(  ( (bFieldCtrl & FIELD_TYPE_MASK) == TYPE_INPUT_INSERT )
                      || ( (bFieldCtrl & FIELD_TYPE_MASK) == TYPE_INPUT_OVERTYPE ) )
                    {
                        fFieldFnStatus = fnfStdEntryInit(pString->pUnicodeString, bFieldLen, bFieldCtrl, bNumTextItems, pFLDRecordParse, &bAttributes);
                    }
                    else*/
                    {
                        fFieldFnStatus = aInitFns[wInitFnID](pString->pUnicodeString, bFieldLen, bFieldCtrl, bNumTextItems, pFLDRecordParse, &bAttributes);
                    }
                }   
                else
                    fFieldFnStatus = aRefreshFns[wRefreshFnID](pString->pUnicodeString, bFieldLen, bFieldCtrl, bNumTextItems, pFLDRecordParse, &bAttributes);

                /* if the init or refresh function failed, we stop building any more fields and exit the
                 function with failure status.  the field function that failed should have already
                 called the function that requests the error mode. */
                if (fFieldFnStatus != SUCCESSFUL)
                {
                    pString->pUnicodeString[0] = NULL_VAL;
                    pPrevious = pString;
                    break;
                }

                /* Check reverse mask of bAttributes and  pString->fAttributes , change pString->fAttributes reverse bit
                 to bAttributes reverse bit if changed. */
                if ((pString->fAttributes & REVERSE_MASK) != (bAttributes & REVERSE_MASK))
                {
                    if (bAttributes & REVERSE_MASK)
                    {
                        //if reverse flag is on
                        pString->fAttributes |= REVERSE_MASK;
                    }
                    else
                    {
                        //if reverse flag is off
                        pString->fAttributes &= ~REVERSE_MASK;
                    }
                }
                /* if the field just processed uses a cursor and it is active, send the cursor
                 info to the LCD task */
                if ((bFieldCtrl & CURSOR_MASK) && (bCursorPosition != CURSOR_INACTIVE))
                {
                    /* double check that this is the only active cursor; freeze on error if not */
                    if( *ppCursor == NULL_PTR )
                    {
                        if (OSGetMemory((void **) ppCursor, sizeof(LCD_CURSOR)) != SUCCESSFUL)
                        {
                            fnDisplayErrorDirect("Out of memory", 14, OIT);
                            break;
                        }
                        else
                        {
                            if (bCursorPosition != CURSOR_OFF)
                            {
                                (*ppCursor)->fOn = true;
                                (*ppCursor)->wFieldXCoord = pString->bXCoordinate;
                                (*ppCursor)->wFieldYCoord = pString->bYCoordinate;
                                (*ppCursor)->bCursorPos = bCursorPosition;
                                (*ppCursor)->bFont = pString->bFont;
                                (*ppCursor)->bStyle = bCursorStyle;
                            }
                            else
                                (*ppCursor)->fOn = false;
                        }
                    }
                    else
                    {
                        fnDisplayErrorDirect("Attempt to use multiple cursors on one screen", 0, OIT);
                    }
                }

                /* mark the current node as the previous one, so we can update its next node pointer
                 on the next pass through the loop */
                pPrevious = pString;
            }

            /* update pointer to the next record */
            pFLDRecordStart += bRecordLen;
        }
    }

    /* as long as there is at least one record in the list, set the next node pointer
    of the last node to NULL_PTR */
    if( (pPrevious != NULL_PTR) && ( pString != NULL_PTR) )
    {
        pString->pNext = NULL_PTR;
    }

    *ppHead = pHead;
    *ppTail = pString;

    return;
}

/* **********************************************************************
// FUNCTION NAME:       fnDoModeAdjustments
// DESCRIPTION:  
//          When we display a screen on the LCD, it is sometimes desirable
//          to automatically remap one or more mode/submode combos
//          to a new screen assignment.  For example, when we display
//          the test pattern prompt screen ("insert envelope to print
//          test pattern") we would want to map the PRINTING_ENVELOPE mode
//          to the "printing test pattern" screen.  Likewise, when we
//          are on the main indicia print ready screen, we would want
//          to map the PRINTING_ENVELOPE mode to the indicia printing
//          screen.  This would allow a mode transition to PRINTING_ENVELOPE
//          to go to the correct screen depending on what the previous
//          screen was.  There is a global table that specifies which
//          mode adjustments (if any) are necessary for each screen.
//          This function uses that table to make the adjustments for
//          a given input screen.
//
// INPUTS:  wScreen - The screen which is triggering the adjustments.
//
// HISTORY:
//  2011.07.19 Clarisa Bellamy - If the event is not in the event-to-screen
//                          table, put an entry in the syslog, and one in 
//                          the exception.log FFS file, then report a meter
//                          error. 
// AUTHOR: Joe Mozdzer
// --------------------------------------------------------------------------*/
static void fnDoModeAdjustments(unsigned short wScreen)
{
  unsigned long     i, j;
  unsigned short    wNewScreenAssignment;
  UINT8             ucEvent;


    i = 0;
    // Step through each entry in the mode adjustment table.  There may be more
    //  than one entry for this screen.
    while (StEventAdjTable[i].ulScreenID != END_OF_SCREEN_MODE_TABLE)
    {
        if (StEventAdjTable[i].ulScreenID == wScreen)
        {
            // For every entry that has the input screen listed in the first column,
            //  do a mode adjustment.  The mode adjustment involves finding the
            //  given mode-change event in the event-to-next-screen table 
            //  (StEventNextScreen) and replacing its screen assignment with the 
            //  next screen specified in the adjustment table (StEventAdjTable) 
            ucEvent = StEventAdjTable[i].ucEvent;
            wNewScreenAssignment = StEventAdjTable[i].ulNewScreenID;

            j = 0;
            // Look for the entry in the event-to-next-screen table that contains
            //  this event.  There should only be one, so we stop looking when
            //  it is found.  There is not necessarily an entry for every event.
            while (StEventNextScreen[j].ulScreenID != END_OF_SCREEN_MODE_TABLE)
            {
                if (StEventNextScreen[j].ucEvent == ucEvent)
                {
                  // If we find event, change the screen assignment, for this
                  //  entry. Then stop looking in this table, and look for the 
                  //  next matching entry in the adjustment table.
                  StEventNextScreen[j].ulScreenID = wNewScreenAssignment;
                  break;
                }
                j++;
            }
            // If the event is not in the table, it is a major error. 081F
            if (StEventNextScreen[j].ulScreenID == END_OF_SCREEN_MODE_TABLE)
            {
                // Event numbers are defined in oit.h, "Event definitions"
                // Put an entry in the syslog, try to put an entry inthe 
                //  exception.log file in FFS, and go to an error screen.
                sprintf( scratchpad100, 
                         "ERROR! Evnt x%X not in StEventNextScreen table.", 
                         ucEvent );
                fnDumpStringToSystemLog( scratchpad100 );
                logException( scratchpad100 );
                fnReportMeterError( ERROR_CLASS_OIT, ECOIT_MISSING_TABLE_ENTRY );
            }
        }
        // Found in mode adjustment table or not, keep looking.
        i++;
    }
    return;
}



/* **********************************************************************
// FUNCTION NAME: fnProcessKeyOrEventMatch
// DESCRIPTION: Calls the function assigned to a keypress or event.
//              Invokes the next screen returned by the function or refreshes
//              the current screen.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  bKeyCode - the key code of the keypress being processed.  this is
//                      only used when processing a keypress.  if an event
//                      is being processed, this parameter is unused.
//          pIntertaskMsg - pointer to the intertask message that may have
//                          been the trigger for the event being processed.
//                          if a key is being processed, this parameter is unused.
//          wFnID - id number of the function to call.  this is used to
//                  lookup the function pointer in the event function
//                  table or the hard key function table, depending on
//                  which is being processed
//          bNumNextScreens - number of next screen choices
//          pNextScreens - pointer to array of next screen choices (array size depends
//                          on value of bNumNextScreens)
//          fIsHardKey - set to TRUE to process a hard key
//                       set to FALSE to process an event
//          fIgnorePre - set to TRUE if pre-function of next screen should be skipped
//          fIgnorePost - set to TRUE if post-function of this screen should be skipped
//          fIgnoreNext - set to TRUE if next screen selection returned from the
//                          key or event function should be disregarded and we should
//                          not change screens
//
// RETURNS: TRUE if processing the key or event is resulting in the screeen changing
//          FALSE if we are staying on the same screen
//
// NOTES:  fIgnorePre and fIgnorePost flags are only applicable if the next screen is
//          being changed as a result of processing THIS key or event record.
//                          
//
// OUTPUTS: none
// *************************************************************************/
static BOOL fnProcessKeyOrEventMatch(unsigned char bKeyCode, INTERTASK *pIntertaskMsg, unsigned short wFnID,
                     unsigned char bNumNextScreens, unsigned short *pNextScreens,
                     BOOL fIsHardKey, BOOL fIgnorePre, BOOL fIgnorePost, BOOL fIgnoreNext)
{
  unsigned short    wNextScreen;
  BOOL          fRetval;

  /* call the hard key or event function */
  if (fIsHardKey == TRUE)
    wNextScreen = fnCallKeyFunction(wFnID, bKeyCode, bNumNextScreens, pNextScreens);
  else
    wNextScreen = fnCallEventFunction(wFnID, bNumNextScreens, pNextScreens, pIntertaskMsg);

  if (fIgnoreNext)
    wNextScreen = 0;

  /* change the screen if a new one was returned */
  if (wNextScreen != 0)
    {
      fnChangeScreen(wNextScreen, fIgnorePre ? FALSE : TRUE, fIgnorePost ? FALSE : TRUE);
      fRetval = TRUE;   /* screen is changing */
    }
  /* otherwise if we are staying on the same screen, refresh the fields */
  else
    {
      fnPaintCurrentScreen(TRUE);  /* "refresh only" argument set to true */
      fRetval = FALSE;  /* screen is not changing */
    }

  return (fRetval);
}


/* **********************************************************************
// FUNCTION NAME: fnChangeScreen
// DESCRIPTION: Displays a new screen.  This function updates all global
//              information associated with a screen and displays it on
//              the LCD by sending a message to the LCD task.  The post
//              function of the previous screen and the pre function of
//              the new screen are also called.
// AUTHOR: Joe Mozdzer
//
// OUTPUTS: none
// *************************************************************************/
extern KEY_BUFFER           rKeyBuffer; /* this is temp- there will be some functions that hide this info */
void fnChangeScreen(unsigned short wScreenNumber, BOOL fCallPreFn, BOOL fCallPostFn)
{

  unsigned long i;

  /* call the post function of the screen being exited */
  if (fCallPostFn == TRUE)
    {
      pPostFn           pContinuationFunction;
      unsigned long lwMsgsAwaited;
      char          chPostFnStatus;

      chPostFnStatus =
        aPostFns[rCurrentTreeRecord.wPostFnID]((void (**) ()) &pContinuationFunction, &lwMsgsAwaited);

      if (chPostFnStatus == PREPOST_WAITING)
        {

          rWaitCtrl.lwMsgsAwaited = lwMsgsAwaited;
          rWaitCtrl.lwMsgsReceived = 0;
          rWaitCtrl.wScreenWaiting = wScreenNumber;
          rWaitCtrl.pfnNextPostFunction = pContinuationFunction;
          rWaitCtrl.fIsPreFunction = FALSE;
          goto xit;
        }
      else
        lwMsgsAwaited = 0;
    }


  if (fCallPreFn == TRUE)
    {
      pPreFn            pContinuationFunction;
      unsigned short    wNextScreen;
      unsigned long lwMsgsAwaited;
      char          chPreFnStatus;

      fnSetCurrentScreen(wScreenNumber);        /* not the cleanest but it works */
      wNextScreen = 0;

      chPreFnStatus =
        aPreFns[rCurrentTreeRecord.wPreFnID]((void (**) ()) &pContinuationFunction, &lwMsgsAwaited, &wNextScreen);

      if (chPreFnStatus == PREPOST_WAITING)
        {

          rWaitCtrl.lwMsgsAwaited = lwMsgsAwaited;
          rWaitCtrl.lwMsgsReceived = 0;
          rWaitCtrl.wScreenWaiting = wScreenNumber;
          rWaitCtrl.pfnNextPreFunction = pContinuationFunction;
          rWaitCtrl.fIsPreFunction = TRUE;
          goto xit;
        }
      else
        {
          lwMsgsAwaited = 0;
          if (wNextScreen != 0)
            {
              /* if the prefunction call is complete and it rerouted us to a different screen,
                 use a recursive call to do the screen change.  note that we only need to
                 call the pre function of the new screen and not the post function of this
                 screen because it never really got displayed */
              fnChangeScreen(wNextScreen, TRUE, FALSE);
              goto xit;
            }
        }
    }



  /* if we get to here, it means the post & pre functions have been completed, so nothing
     else is being awaited */
  rWaitCtrl.lwMsgsAwaited = 0;

  /* check if the new screen is associated with a mode that is different than the
     one we are in now.  see if the next screen appears in the screen to mode map. */
  //    i = 0;
  //    while (pScreenModeMap[i].wScreenID != END_OF_SCREEN_MODE_TABLE)
  //    {
  //        if (pScreenModeMap[i].wScreenID == wScreenNumber)
  //            break;
  //        i++;
  //    }

  i = 0;
  //  New event mapping table for OI/SYS split
  while (StScreenEventMap[i].ulScreenID != END_OF_SCREEN_MODE_TABLE)
    {
      if (StScreenEventMap[i].ulScreenID == wScreenNumber)
        break;
      i++;
    }


  //    /* if the screen is in the table, compare the current mode to the mode in the table */
  //    if (pScreenModeMap[i].wScreenID != END_OF_SCREEN_MODE_TABLE)
  //    {
  //        /* if the modes are different, we must get permission from the system controller before
  //            changing to the new screen.  call the function that builds the message and saves the
  //            next screen information. */
  //        if ((pScreenModeMap[i].bMode != ucOITCurrentMode) || (pScreenModeMap[i].bSubMode != ucOITCurrentSubMode))
  //        {
  //            fnRequestNewMode(pScreenModeMap[i].bMode, pScreenModeMap[i].bSubMode, wScreenNumber);
  //            goto xit;
  //        }
  //    }

  /* if the screen is in the table, perform event processing */
  if ((StScreenEventMap[i].ulScreenID != END_OF_SCREEN_MODE_TABLE) &&
      (StScreenEventMap[i].ucEvent    != OIT_ENTER_POWERUP)         &&
      (StScreenEventMap[i].ucEvent    != UcLastEvent))
    {
      fnRequestNewMode(StScreenEventMap[i].ucEvent, wScreenNumber, TRUE);
      goto xit;
    }

  /* if we get to here, it is ok to officially change and paint the new screen */
  (void)OSStopTimer(SCREEN_TICK_TIMER);     /* stop the timer first so that it gets reset to its full duration */
  (void)OSStartTimer(SCREEN_TICK_TIMER);    /* start the 1 second continuous tick timer */

  fScreenTickEventQueued = FALSE;
  fAutoChangeOccurred = FALSE;

  /* if user set a timer, the screen will refresh to default value after certain amount of time */
  (void)OSStopTimer(USER_TICK_TIMER);
  (void)OSChangeTimerDuration(USER_TICK_TIMER, CMOSSetupParams.wpUserSetPresetClearTimeout * 60 * 1000 );
  (void)OSStartTimer(USER_TICK_TIMER);


  fnSetCurrentScreen(wScreenNumber);
  fnPaintCurrentScreen(FALSE);

  /* modify the mode-to-next-screen table as necessary */
  fnDoModeAdjustments(wScreenNumber);

  /* if we have buffered any keys during the screen transition process, now is the time to
     send them back to the main queue for processing */
  if (rKeyBuffer.fFull == TRUE)
    {
      struct {
        unsigned char   bKeyCode;
        unsigned char   bDirection;
        unsigned char   fControl;
      } rKeydataMsg;        /* struct for building the RX_KEYDATA message */

      rKeydataMsg.bKeyCode = rKeyBuffer.bKeyCode;
      rKeydataMsg.bDirection = rKeyBuffer.bDirection;
      rKeydataMsg.fControl = rKeyBuffer.fControl;

      /*send the key message, making it look like it came from the keypad driver task */
      (void)OSSendIntertask(OIT, KEYTASK, KEY_RX_KEYDATA, BYTE_DATA, &rKeydataMsg, sizeof(rKeydataMsg));


      rKeyBuffer.fFull = FALSE; /* buffer is now empty */
    }



 xit:return;
}

/* **********************************************************************
// FUNCTION NAME: fnChangeScreenAfterError
// DESCRIPTION: Displays a new screen.  This function updates all global
//              information associated with a screen and displays it on
//              the LCD by sending a message to the LCD task.  The post
//              function of the previous screen and the pre function of
//              the new screen are also called.
// AUTHOR: Joe Mozdzer
//
// OUTPUTS: none
// *************************************************************************/
void fnChangeScreenAfterError(unsigned short wScreenNumber, BOOL fCallPreFn, BOOL fCallPostFn)
{

  unsigned long i;

  /* call the post function of the screen being exited */
  if (fCallPostFn == TRUE)
    {
      pPostFn           pContinuationFunction;
      unsigned long lwMsgsAwaited;
      char          chPostFnStatus;

      chPostFnStatus =
        aPostFns[rCurrentTreeRecord.wPostFnID]((void (**) ()) &pContinuationFunction, &lwMsgsAwaited);

      if (chPostFnStatus == PREPOST_WAITING)
        {

          rWaitCtrl.lwMsgsAwaited = lwMsgsAwaited;
          rWaitCtrl.lwMsgsReceived = 0;
          rWaitCtrl.wScreenWaiting = wScreenNumber;
          rWaitCtrl.pfnNextPostFunction = pContinuationFunction;
          rWaitCtrl.fIsPreFunction = FALSE;
          goto xit;
        }
      else
        lwMsgsAwaited = 0;
    }


  if (fCallPreFn == TRUE)
    {
      pPreFn            pContinuationFunction;
      unsigned short    wNextScreen;
      unsigned long lwMsgsAwaited;
      char          chPreFnStatus;

      fnSetCurrentScreen(wScreenNumber);        /* not the cleanest but it works */
      wNextScreen = 0;

      chPreFnStatus =
        aPreFns[rCurrentTreeRecord.wPreFnID]((void (**) ()) &pContinuationFunction, &lwMsgsAwaited, &wNextScreen);

      if (chPreFnStatus == PREPOST_WAITING)
        {

          rWaitCtrl.lwMsgsAwaited = lwMsgsAwaited;
          rWaitCtrl.lwMsgsReceived = 0;
          rWaitCtrl.wScreenWaiting = wScreenNumber;
          rWaitCtrl.pfnNextPreFunction = pContinuationFunction;
          rWaitCtrl.fIsPreFunction = TRUE;
          goto xit;
        }
      else
        {
          lwMsgsAwaited = 0;
          if (wNextScreen != 0)
            {
              /* if the prefunction call is complete and it rerouted us to a different screen,
                 use a recursive call to do the screen change.  note that we only need to
                 call the pre function of the new screen and not the post function of this
                 screen because it never really got displayed */
              fnChangeScreenAfterError(wNextScreen, TRUE, FALSE);
              goto xit;
            }
        }
    }



  /* if we get to here, it means the post & pre functions have been completed, so nothing
     else is being awaited */
  rWaitCtrl.lwMsgsAwaited = 0;

  /* check if the new screen is associated with a mode that is different than the
     one we are in now.  see if the next screen appears in the screen to mode map. */
  //    i = 0;
  //    while (pScreenModeMap[i].wScreenID != END_OF_SCREEN_MODE_TABLE)
  //    {
  //        if (pScreenModeMap[i].wScreenID == wScreenNumber)
  //            break;
  //        i++;
  //    }

  i = 0;
  //  New event mapping table for OI/SYS split
  while (StScreenEventMap[i].ulScreenID != END_OF_SCREEN_MODE_TABLE)
    {
      if (StScreenEventMap[i].ulScreenID == wScreenNumber)
        break;
      i++;
    }


  //    /* if the screen is in the table, compare the current mode to the mode in the table */
  //    if (pScreenModeMap[i].wScreenID != END_OF_SCREEN_MODE_TABLE)
  //    {
  //        /* if the modes are different, we must get permission from the system controller before
  //            changing to the new screen.  call the function that builds the message and saves the
  //            next screen information. */
  //        if ((pScreenModeMap[i].bMode != ucOITCurrentMode) || (pScreenModeMap[i].bSubMode != ucOITCurrentSubMode))
  //        {
  //            fnRequestNewMode(pScreenModeMap[i].bMode, pScreenModeMap[i].bSubMode, wScreenNumber);
  //            goto xit;
  //        }
  //    }

  /* if the screen is in the table, perform event processing */
  if ((StScreenEventMap[i].ulScreenID != END_OF_SCREEN_MODE_TABLE) &&
      (StScreenEventMap[i].ucEvent    != OIT_ENTER_POWERUP)         &&
      (StScreenEventMap[i].ucEvent    != UcLastEvent))
    {
      fnRequestNewMode(StScreenEventMap[i].ucEvent, wScreenNumber, TRUE);
      goto xit;
    }

  /* if we get to here, it is ok to officially change and paint the new screen */
  (void)OSStopTimer(SCREEN_TICK_TIMER);     /* stop the timer first so that it gets reset to its full duration */
  (void)OSStartTimer(SCREEN_TICK_TIMER);    /* start the 1 second continuous tick timer */

  fScreenTickEventQueued = FALSE;
  fAutoChangeOccurred = FALSE;

  /* if user set a timer, the screen will refresh to default value after certain amount of time */
  (void)OSStopTimer(USER_TICK_TIMER);
  (void)OSChangeTimerDuration(USER_TICK_TIMER, CMOSSetupParams.wpUserSetPresetClearTimeout * 60 * 1000 );
  (void)OSStartTimer(USER_TICK_TIMER);


  fnSetCurrentScreen(wScreenNumber);
  fnPaintCurrentScreenAfterError(FALSE);

  /* modify the mode-to-next-screen table as necessary */
  fnDoModeAdjustments(wScreenNumber);

  /* if we have buffered any keys during the screen transition process, now is the time to
     send them back to the main queue for processing */
  if (rKeyBuffer.fFull == TRUE)
    {
      struct {
        unsigned char   bKeyCode;
        unsigned char   bDirection;
        unsigned char   fControl;
      } rKeydataMsg;        /* struct for building the RX_KEYDATA message */

      rKeydataMsg.bKeyCode = rKeyBuffer.bKeyCode;
      rKeydataMsg.bDirection = rKeyBuffer.bDirection;
      rKeydataMsg.fControl = rKeyBuffer.fControl;

      /*send the key message, making it look like it came from the keypad driver task */
      (void)OSSendIntertask(OIT, KEYTASK, KEY_RX_KEYDATA, BYTE_DATA, &rKeydataMsg, sizeof(rKeydataMsg));


      rKeyBuffer.fFull = FALSE; /* buffer is now empty */
    }



 xit:return;
}

/* **********************************************************************
// FUNCTION NAME: fnSetCurrentScreen
// DESCRIPTION: Sets the given screen number as the current screen.  Global
//              pointers are updated which allows us to access the screen
//              record and paint the screen, interpret hard keys, etc.
// AUTHOR: Joe Mozdzer
//
// OUTPUTS: none
// *************************************************************************/
static void fnSetCurrentScreen(unsigned short wScreenNumber)
{
  unsigned long lwOffset;
  unsigned long i;
  unsigned char *pFirstEventLocator;
  unsigned short wNumBGColorEntries;

  /* lookup the offset of the desired screen tree record.  4-byte alignment is
     not guaranteed, so we cannot directly access what is stored at rTreeDir.pOffsetList.
     we have to use a memcpy instead */
  EndianAwareCopy(&lwOffset, rTreeDir.pOffsetList + wScreenNumber, 4);

  /* update the current tree record global variable */

  rCurrentTreeRecord.bNumKeyEntries = * (unsigned char *) (pTreeComponent + lwOffset);
  rCurrentTreeRecord.bNumEventEntries = * (unsigned char *) (pTreeComponent + lwOffset + 1);
  EndianAwareCopy(&rCurrentTreeRecord.wPreFnID, pTreeComponent + lwOffset + 2, 2);
  EndianAwareCopy(&rCurrentTreeRecord.wPostFnID, pTreeComponent + lwOffset + 4, 2);
  rCurrentTreeRecord.pFirstKeyEntry = pTreeComponent + lwOffset + 6;

  if (rTreeDir.wComponentFormat >= 0x0200)
    {
      i = rCurrentTreeRecord.bNumKeyEntries;
      pFirstEventLocator = (unsigned char *) rCurrentTreeRecord.pFirstKeyEntry;
      while (i--)
        {
          pFirstEventLocator += *pFirstEventLocator;
        }
      rCurrentTreeRecord.pFirstEventEntry = pFirstEventLocator;
    }
  else
    rCurrentTreeRecord.pFirstEventEntry = pTreeComponent + lwOffset + 6 +
      (rCurrentTreeRecord.bNumKeyEntries * TREE_ENTRY_SIZE);
  /* LANGUAGE DATA */

  /* setup the current fixed text record */
  EndianAwareCopy(&lwOffset, rFixedTextDir.pOffsetList + wScreenNumber, 4);
  rCurrentFixedTextRecord.bNumEntries = * (unsigned char *)
    (rLanguageDir.pFixedTextElement + lwOffset);
  rCurrentFixedTextRecord.pFirstTextEntry = rLanguageDir.pFixedTextElement + lwOffset + 1;

  /* setup the current fields record */
  EndianAwareCopy(&lwOffset, rFieldsDir.pOffsetList + wScreenNumber, 4);
  rCurrentFieldsRecord.bNumEntries = * (unsigned char *)
    (rLanguageDir.pFieldsElement + lwOffset);
  rCurrentFieldsRecord.pFirstFieldEntry = rLanguageDir.pFieldsElement + lwOffset + 1;

  //#if 0
  /* setup the current fixed graphic record */
  if (rLanguageDir.wComponentFormat >= 0x0200)
    {
	  EndianAwareCopy(&lwOffset, rFixedGraphicDir.pOffsetList + wScreenNumber, 4);
      rCurrentFixedGraphicRecord.bNumEntries = * (unsigned char *)
                                                (rLanguageDir.pFixedGraphicsElement + lwOffset);
      rCurrentFixedGraphicRecord.pFirstEntry = rLanguageDir.pFixedGraphicsElement + lwOffset + 1;
    }
  else
    {
      rCurrentFixedGraphicRecord.bNumEntries = 0;
      rCurrentFixedGraphicRecord.pFirstEntry = NULL_PTR;
    }


  /* setup the current variable graphic record */
  if (rLanguageDir.wComponentFormat >= 0x0200)
    {
	  EndianAwareCopy(&lwOffset, rVarGraphicDir.pOffsetList + wScreenNumber, 4);
      rCurrentVarGraphicRecord.bNumEntries = * (unsigned char *)
                                            (rLanguageDir.pVarGraphicsElement + lwOffset);
      rCurrentVarGraphicRecord.pFirstEntry = rLanguageDir.pVarGraphicsElement + lwOffset + 1;
    }
  else
    {
      rCurrentVarGraphicRecord.bNumEntries = 0;
      rCurrentVarGraphicRecord.pFirstEntry = NULL_PTR;
    }
  //#endif

    if (rLanguageDir.wComponentFormat >= 0x0301)
    {
        if (rLanguageDir.pBGColorElement)
        	EndianAwareCopy(&wNumBGColorEntries, rLanguageDir.pBGColorElement, 2);
        else
            wNumBGColorEntries = 0;

        if (wNumBGColorEntries > wScreenNumber)
        {
        	EndianAwareCopy(&wCurrentBGColor, rLanguageDir.pBGColorElement + 2 + (wScreenNumber * 2), 2);
        }
        else
            wCurrentBGColor = FP_COLORLCD_USE_DEFAULT;
    }
    else
        wCurrentBGColor = FP_COLORLCD_USE_DEFAULT;

  /* update the global that contains the screen number.  it is possible that this function
     is called with the same screen ID as the one that is presently displayed, so only
     do the update when we actually change the screen ID. */
  if (wCurrentScreenID != wScreenNumber)
    {
      wPreviousScreenID = wCurrentScreenID;
      wCurrentScreenID = wScreenNumber;
    }

  return;
}


/* **********************************************************************
// FUNCTION NAME: fnOITDestroyStrings
// PURPOSE: Frees an LCD_STRING linked list.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none
// RETURNS: FAILURE if one or more of the elements of the list could not be freed
// *************************************************************************/
static BOOL fnOITDestroyStrings(LCD_STRING *pStringsToDestroy)
{
  LCD_STRING    *pCurrent, *pNext;
  BOOL      fRetval = SUCCESSFUL;

  pCurrent = pStringsToDestroy;

  do
    {
      pNext = (LCD_STRING *) pCurrent->pNext;
      if (OSReleaseMemory((void *) pCurrent) != SUCCESSFUL)
        fRetval = FAILURE;
      pCurrent = pNext;
    }while( pCurrent != NULL_PTR );
                                        
  return (fRetval);
}


/* **********************************************************************
// FUNCTION NAME: fnOITInitializeTree
// PURPOSE: Initializes the global variables that contain screen tree information.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none
// *************************************************************************/

static void fnOITInitializeTree( void )
{
    UINT16  wTreeHeader;

    pTreeComponent = (unsigned char *) fnFlashGetTree();

    /* grab the first two bytes of the tree component.  these can be used to determine
       the format of the rest of the component. */
    wTreeHeader = ((UINT16)(pTreeComponent[0]) << 8) + pTreeComponent[1];

    rTreeDir.fExtendedVersionValid = FALSE;

    if (wTreeHeader < 0x0200)
    {
        /* if the tree header is < 0x0200, then this is the original format.  this format
         used to have the version number of the tree in these two bytes, but there
         was no release that had a version number larger than 01.xx.  That is the
         reason that the comparison against 0x0200 is safe. */
        rTreeDir.wComponentFormat = 0x0100;
        rTreeDir.wVersion = wTreeHeader;
        rTreeDir.wNumRecords = ((UINT16)(pTreeComponent[2]) << 8) + pTreeComponent[3];
        rTreeDir.pOffsetList = (UINT32 *)(pTreeComponent +4 ); 
    }
    else
    {
        /* if the tree header is >= 0x0200, then we are assured that the first two
         bytes of the component specify the format. */
        rTreeDir.wComponentFormat = wTreeHeader;

        if (rTreeDir.wComponentFormat < 0x0250)
        {
            rTreeDir.wVersion = ((UINT16)(pTreeComponent[2]) << 8) + pTreeComponent[3];
            rTreeDir.wNumRecords = ((UINT16)(pTreeComponent[4]) << 8) + pTreeComponent[5];
            rTreeDir.pOffsetList = (UINT32 *)(pTreeComponent +6 ); 
        }
        else
        {
            // version 02.50 and greater uses a 3 byte version number
            rTreeDir.wVersion = ((UINT16)(pTreeComponent[2]) << 8) + pTreeComponent[3];
            rTreeDir.bExtendedVersion = pTreeComponent[4];
            rTreeDir.fExtendedVersionValid = TRUE;
            rTreeDir.wNumRecords = ((UINT16)(pTreeComponent[6]) << 8) + pTreeComponent[7];
            rTreeDir.pOffsetList = (UINT32 *)(pTreeComponent +8 ); 
        }
    }

    return;

}

/* **********************************************************************
// FUNCTION NAME: fnOITInitializeLanguage
// PURPOSE: Initializes the global variables that contain language component
//          information.  There can be more than one language component
//          in the flash, so the language to use is provided as an input.  The
//          global language information is referenced throughout the OIT task
/           for putting up screens, managing fields, etc.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  bLangID - ID# of the language to use
// *************************************************************************/

//#if 0
  const unsigned char rFixedGraphicStub[] = {
  /* fixed graphic directory */
  0x00,  0x00 }; /* 0 records, needed to keep languages compiled in format 01.00 compatible */

const unsigned char rVarGraphicStub[] = {
  /* var graphic directory */
  0x00, 0x00 }; /* 0 records */
//#endif

void fnOITInitializeLanguage( UINT8 bLangID )
{
    unsigned long   lwOffset;       /* temporary storage of an offset */
    BOOL            fGraphSupport = FALSE;


    if (bLangID >= fnFlashGetNumLanguages())
        fnDisplayErrorDirect("Illegal language ID.  CMOS may be corrupted", 0, OIT);

    /* Initialize language component stuff.  This will be moved to a function since it may need to be done more
        than once because the language can change.  */
    pLangComponent = (unsigned char *) fnFlashGetLanguage(bLangID);

    /* save the version number of this language */
    rLanguageDir.wVersion = ((UINT16)(pLangComponent[0]) << 8) + pLangComponent[1];

    rLanguageDir.fExtendedVersionValid = FALSE;

    /* save the component format - needs to be integrated w/ screen tool when 02.00 compile available */
    if (rLanguageDir.wVersion >= 0x0200)
    {
        fGraphSupport = TRUE;
        rLanguageDir.wComponentFormat = rLanguageDir.wVersion;
        if ((rLanguageDir.wComponentFormat < 0x0350) && (rLanguageDir.wComponentFormat != 0x0250))
        {
            rLanguageDir.wVersion = ((UINT16)(pLangComponent[2]) << 8) + pLangComponent[3];
        }
        else
        {
            /* version 2.50 has the extended version number but no color,
                version 3.50+ has both the extended version number and color */
            rLanguageDir.wVersion = ((UINT16)(pLangComponent[2]) << 8) + pLangComponent[3];
            rLanguageDir.bExtendedVersion = pLangComponent[4];
            rLanguageDir.fExtendedVersionValid = TRUE;
        }

    }
    else
        rLanguageDir.wComponentFormat = 0x0100;

    /* FIXED TEXT ELEMENT initialization */

    /* look in the language component directory to get the offset to the fixed text element */
    if (fGraphSupport)
    	EndianAwareCopy(&lwOffset, pLangComponent + 4 + (rLanguageDir.fExtendedVersionValid ? 2 : 0), 4);
    else
    	EndianAwareCopy(&lwOffset, pLangComponent + 2, 4);
    /* save a pointer to the fixed text element in the language directory structure */
    rLanguageDir.pFixedTextElement = pLangComponent + lwOffset;
    /* save the number of records in the fixed text element */
    EndianAwareCopy(&rFixedTextDir.wNumRecords, rLanguageDir.pFixedTextElement, 2);
    /* calculate and save a pointer to the first record offset in the offset list */
    rFixedTextDir.pOffsetList = (UINT32 *)(rLanguageDir.pFixedTextElement + 2);

    /* FIELDS ELEMENT initialization */

    /* look in the language component directory to get the offset to the fields element */
    if (fGraphSupport)
    	EndianAwareCopy(&lwOffset, pLangComponent + 8 + (rLanguageDir.fExtendedVersionValid ? 2 : 0), 4);
    else
    	EndianAwareCopy(&lwOffset, pLangComponent + 6, 4);
    /* save a pointer to the fields element in the language directory structure */
    rLanguageDir.pFieldsElement = pLangComponent + lwOffset;
    /* save the number of records in the fields text element */
    EndianAwareCopy(&rFieldsDir.wNumRecords, rLanguageDir.pFieldsElement, 2);
    /* calculate and save a pointer to the first record offset in the offset list */
    rFieldsDir.pOffsetList = (unsigned long *) (rLanguageDir.pFieldsElement + 2);


    /* TABLE TEXT ELEMENT initialization */

    /* look in the language component directory to get the offset to the table text element */
    if (fGraphSupport)
    	EndianAwareCopy(&lwOffset, pLangComponent + 12 + (rLanguageDir.fExtendedVersionValid ? 2 : 0), 4);
    else
    	EndianAwareCopy(&lwOffset, pLangComponent + 10, 4);
    /* save a pointer to the table text element in the language directory structure */
    rLanguageDir.pTableTextElement = pLangComponent + lwOffset;
    /* save the number of records in the fields text element */
    EndianAwareCopy(&rTableTextDir.wNumRecords, rLanguageDir.pTableTextElement, 2);
    /* calculate and save a pointer to the first record offset in the offset list */
    rTableTextDir.pOffsetList = (unsigned long *) (rLanguageDir.pTableTextElement + 2);


    /* FIXED GRAPHIC ELEMENT initialization */

    //#if 0

    /* compiler is not implemented yet, so the stub array above this function is used for now */
    if (fGraphSupport)
    {
    	EndianAwareCopy(&lwOffset, pLangComponent + 16 + (rLanguageDir.fExtendedVersionValid ? 2 : 0), 4);
        rLanguageDir.pFixedGraphicsElement = pLangComponent + lwOffset;
        EndianAwareCopy(&rFixedGraphicDir.wNumRecords, rLanguageDir.pFixedGraphicsElement, 2);
    }
    else
    {
        rLanguageDir.pFixedGraphicsElement = rFixedGraphicStub;
        EndianAwareCopy(&rFixedGraphicDir.wNumRecords, rLanguageDir.pFixedGraphicsElement, 2);
    }
    rFixedGraphicDir.pOffsetList = (unsigned long *) (rLanguageDir.pFixedGraphicsElement + 2);


    /* VARIABLE GRAPHIC ELEMENT initialization */

    /* compiler is not implemented yet, so the stub array above this function is used for now */
    if (fGraphSupport)
    {
    	EndianAwareCopy(&lwOffset, pLangComponent + 20 + (rLanguageDir.fExtendedVersionValid ? 2 : 0), 4);
        rLanguageDir.pVarGraphicsElement = pLangComponent + lwOffset;
        EndianAwareCopy(&rVarGraphicDir.wNumRecords, rLanguageDir.pVarGraphicsElement, 2);
    }
    else
    {
        rLanguageDir.pVarGraphicsElement = rVarGraphicStub;
        EndianAwareCopy(&rVarGraphicDir.wNumRecords, rLanguageDir.pVarGraphicsElement, 2);
    }
    rVarGraphicDir.pOffsetList = (unsigned long *) (rLanguageDir.pVarGraphicsElement + 2);

    //#endif

    /* KEY MAP ELEMENT initialization */

    /* look in the language component directory to get the offset to the key map element */
    if (fGraphSupport)
    	EndianAwareCopy(&lwOffset, pLangComponent + 24 + (rLanguageDir.fExtendedVersionValid ? 2 : 0), 4);
    else
    	EndianAwareCopy(&lwOffset, pLangComponent + 14, 4);
    /* save a pointer to the key map element in the language directory structure */
    rLanguageDir.pKeyMapElement = pLangComponent + lwOffset;
    /* initialize the key map structure- first is the number of key bindings */
    EndianAwareCopy(&rKeyMap.wNumBindings, rLanguageDir.pKeyMapElement, 2);
    /* save the pointer to the first key binding */
    rKeyMap.pFirstBinding = (void *) (rLanguageDir.pKeyMapElement + 2);

    /* BACKGROUND COLOR ELEMENT initialization */
    if (rLanguageDir.wComponentFormat >= 0x0301)    // version 3.01+ has color
    {
        // this only exists in version 3.01+ of the output file
    	EndianAwareCopy(&lwOffset, pLangComponent + 28 + (rLanguageDir.fExtendedVersionValid ? 2 : 0), 4);
        rLanguageDir.pBGColorElement = pLangComponent + lwOffset;
    }
    else
        rLanguageDir.pBGColorElement = 0;

    return;
}

/* **********************************************************************
// FUNCTION NAME:       fnDoVersionCompare
// DESCRIPTION: 
//      Private utility function that checks validity of screen version numbers.
//  
// INPUTS:  
//      ulActualVersion- version number being compared.  all version numbers
//                          passed into this function are in packed BCD format
//                          (e.g. 0x00123456 = version 12.34.56)
//      ulVersionA, ulVersionB- operands for version comparison.  version A is
//                          used for digits that must be an exact match and
//                          also for the lower limit of digits where we are
//                          doing range matches.  version B is only used as
//                          the higher limit of digits where we are doing range
//                          matches.
//      ulVersionMask- specifies what type of comparison is being done on
//                          each digit of the version number.  F means an exact
//                          match is required, 1 means range matching is done 
//                          (actual version digit must be >= version A 
//                           and <= version B), and 0 means this is a don't 
//                          care digit.
// RETURNS: 
//      TRUE if version number is OK, FALSE if it is not compatible
//
// HISTORY:
//  2009.07.13  Clarisa Bellamy - Modify so that all version arguments are longs,
//                      to accommodate the 3-part versions.
// AUTHOR: Joe Mozdzer
// *************************************************************************/
static BOOL fnDoVersionCompare( UINT32 ulVersionA, UINT32 ulVersionB,
                                UINT32 ulVersionMask, UINT32 ulActualVersion )
{
    BOOL        fRetval = TRUE;
    UINT32      ulDigitMask;

    for( ulDigitMask = 0xF00000; ulDigitMask; ulDigitMask >>= 4 )
    {
        if( (ulVersionMask & ulDigitMask) == ulDigitMask )
        {
            // Mask is configured to do equality check on this digit.
            if( (ulVersionA & ulDigitMask) != (ulActualVersion & ulDigitMask) )
            {
                fRetval = FALSE;
                break;
            }
        }
        else
        {   
            if( ulVersionMask & ulDigitMask )
            {
                // Mask is configured to do range check on this digit.
                if(   ((ulVersionA & ulDigitMask) > (ulActualVersion & ulDigitMask)) 
                   || ((ulVersionB & ulDigitMask) < (ulActualVersion & ulDigitMask)) )
                {
                    fRetval = FALSE;
                    break;
                }
            }
        }
    }
    return (fRetval);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnGetScreenEventById
//
// DESCRIPTION: 
//      Retrieve the event in the event table by input screen ID.
//
// INPUTS:
//      usScreenID  - screen ID
//
// OUTPUTS:
//      pEvent - event of the screen
//
// RETURNS:
//      Found or not
//
// WARNINGS/NOTES:
//      None
//
// *************************************************************************/
BOOL fnGetScreenEventById (T_SCREEN_ID usScreenID, UINT8* pEvent)
{
  BOOL  fFound = FAILURE;
  UINT16    usIndex = 0;

  while (StScreenEventMap[usIndex].ulScreenID != END_OF_SCREEN_MODE_TABLE)
    {
      if (StScreenEventMap[usIndex].ulScreenID == usScreenID)
        {
          *pEvent = StScreenEventMap[usIndex].ucEvent;
          fFound = SUCCESSFUL;
          break;
        }
      usIndex++;
    }
    
  return fFound;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetCurrentLanguage
//
// DESCRIPTION: 
//      Wrapper function of current language setting
//
// INPUTS:      
//      None.
//
// RETURNS:
//      value of bCurrentLanguage
//
// NOTES: 
//      None
//
// MODIFICATION HISTORY:
//  06/05/2006  Vincent Yi      Initial version
//      
// *************************************************************************/
UINT8 fnGetCurrentLanguage (void)
{
  return bCurrentLanguage;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnSetCurrentLanguage
//
// DESCRIPTION: 
//      Wrapper function of current language setting
//
// INPUTS:      
//      ucCurrLanguage.
//
// RETURNS:
//      None
//
// NOTES: 
//      None
//
// MODIFICATION HISTORY:
//  06/05/2006  Vincent Yi      Initial version
//      
// *************************************************************************/
void fnSetCurrentLanguage (UINT8 ucCurrLanguage)
{
  bCurrentLanguage = ucCurrLanguage;
}


/* **********************************************************************
// FUNCTION NAME: fnFindEvent
// DESCRIPTION: Find an event notification.  This function checks to
//              see if the current screen has an entry for the event
//              that happened.
//
// AUTHOR: 
//
// INPUTS:  bOccurredEventID - ID number of the event that occurred.  the ID
//                              numbers are defined in oifunct.h
//
// OUTPUTS: Returns TRUE if the current screen has the event.
//          Returns FALSE if the current screen does not care about the event.
// *************************************************************************/
BOOL fnFindEvent(unsigned char bOccurredEventID)
{
    unsigned long       i;
    unsigned char       *pEntryPtr;
    unsigned char       *pNextEntryPtr;
    unsigned char       bEventCtrlData;
    unsigned char       bRecordEventID;
    BOOL                fRetval = FALSE;
    unsigned char       bParmID;
    unsigned char       bParmCompareType;
    unsigned short      wOperand;
    BOOL    fParmResult;

    pNextEntryPtr = (unsigned char *) rCurrentTreeRecord.pFirstEventEntry;
    for (i = 0; i < rCurrentTreeRecord.bNumEventEntries; i++)
    {
        if (rTreeDir.wComponentFormat >= 0x0200)
        {
            /* the first byte of the entry is the length of the entry.  use it
            to calculate the pointer to the beginning of the following entry. */
            pEntryPtr = pNextEntryPtr;
            pNextEntryPtr += *pEntryPtr++;
        }
        else
        {
            pEntryPtr = ((unsigned char *) rCurrentTreeRecord.pFirstEventEntry) + (i * TREE_ENTRY_SIZE);
        }

        bEventCtrlData = *pEntryPtr++;
        bRecordEventID = *pEntryPtr++;

        /* get the parameter compare options */
        bParmID = *pEntryPtr++;
        bParmCompareType = *pEntryPtr++;
        EndianAwareCopy(&wOperand, pEntryPtr, sizeof(unsigned short));
        pEntryPtr += 2;

        if (bRecordEventID == bOccurredEventID)
            {
            /* if the direction matches, do the parameter comparison */
            if (fnCheckKeyOrEventParameter(bParmID, bParmCompareType,
                         wOperand, &fParmResult) != SUCCESSFUL)
            {
                fnReportMeterError(ERROR_CLASS_OIT, ECOIT_BAD_PARAMETER_CHECK);
            }
            /* if the parameter compare was true, we have a match */
            if (fParmResult == TRUE)
            {
                fRetval = TRUE;
                break;
            }
        }
    }

    return (fRetval);
}

// **********************************************************************
// FUNCTION NAME:           fnGetScrVer
// DESCRIPTION:  
//      Get tree version and lang version, convert to an ascii string.
//
// INPUT:  
//  pTreeVer - Pointer to the destination buffer where the ascii string 
//              representing the Tree version is to be written.   
//              NULL_PTR means the caller is not interested in this value.
//  pLanVer  - Pointer to the destination buffer where the ascii string 
//              representing the Language version is to be written.  
//              NULL_PTR means the caller is not interested in this value. 
// RETURNS: 
//      None.
//
// NOTES: 
//  1. Each pointer must either be a NULL_PTR or point to a char array that is 
//      at least 9-bytes big, to include the max data and the null-terminator 
//      and not stomp on other data.  
//
// MODIFICATION HISTORY:
//  2009.07.13  Clarisa Bellamy - Modified to allow for 3-part screen version
//                          numbers and to allow caller to only ask for one of
//                          the versions (tree or language).
//  08/07/2008  Joey Cui        Merged from Janus
// AUTHOR: Andy Mo
//-----------------------------------------------------------------------------
void fnGetScrVer( char *pTreeVer, char *pLanVer )
{    
    UINT8   bPart1;
    UINT8   bPart2;
    UINT8   bPart3;
    
    if( pTreeVer != NULL_PTR )
    {
        // Generate the version number string.  
        // Numbers come from the global tree directory structure in ram.
        bPart1 = (rTreeDir.wVersion & 0xFF00) >> 8;
        bPart2 = rTreeDir.wVersion & 0x00FF;
        bPart3 = rTreeDir.bExtendedVersion;
        if( rTreeDir.fExtendedVersionValid == TRUE )   
           (void)sprintf( pTreeVer, "%02d.%02d.%02d", bPart1, bPart2, bPart3 );    
        else
           (void)sprintf( pTreeVer, "%02d.%02d", bPart1, bPart2 );    
    }

    if( pLanVer != NULL_PTR )
    {
        // Generate the version number string.  
        // Numbers come from the global language directory structure in ram.
        bPart1 = (rLanguageDir.wVersion & 0xFF00) >> 8;
        bPart2 = rLanguageDir.wVersion & 0x00FF;
        bPart3 = rLanguageDir.bExtendedVersion;
        if( rLanguageDir.fExtendedVersionValid == TRUE )   
           (void)sprintf( pLanVer, "%02d.%02d.%02d", bPart1, bPart2, bPart3 );    
        else
           (void)sprintf( pLanVer, "%02d.%02d", bPart1, bPart2 );        
    }

    return;
}

