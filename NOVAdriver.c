/************************************************************************
 *	PROJECT:		K700
 *	MODULE NAME:	NOVAdriver.c
 *	DESCRIPTION:	This file contains the device driver for the integrated
 *					LAN including interrupt handling and integration with
 *					the OS.
 * ----------------------------------------------------------------------
 *               Copyright (c) 2009 Pitney Bowes Inc.
 *                    35 Waterview Drive
 *                   Shelton, Connecticut  06484
 * ----------------------------------------------------------------------
 *  REVISION HISTORY:

 28-Mar-14 Kevin Brown on FPHX 02.12 shelton branch
    1. Modified fnCreateHttpConnectTunnel() to handle all proxy server responses to the CONNECT request.

 03-Mar-14 Kevin Brown on FPHX 02.12 shelton branch
    1. Modified fnDoWeNeedToUseProxy() to check usePcHostForIntelliLink() so the meter's built-in
       proxy functionality cannot be used with PCMC.
    
 18-Feb-14 Kevin Brown on FPHX 02.12 shelton branch
    1. Removed unused code.
    2. Added defines: TXRX_BUFFER_SIZE, PROXY_AUTH_SIZE, SOCKET_HOST_SIZE, SOCKET_SERVER_URL_SIZE, SOCKET_RECEIVE_TIMEOUT.
    3. Added function fnCreateHttpConnectTunnel().

 01-Aug-13 Kevin Brown on FPHX 02.10 shelton branch
    1. Removed the global variable dbgBuf.
    2. Modified fnSetupProxySocket() to return codes instead of a boolean value.
    3. Added pDebugBuffer to fnSetupProxySocket() to replace dbgBuf.

 26-Jun-13 Kevin Brown on FPHX 02.10 shelton branch
    1. Modified fnDoWeNeedToUseProxy, fnMightWeNeedToUseProxy, fnGetProxyAuthStringForHeader, and
       fnSetupProxySocket for the G9 implementation of proxy.
    2. Created fnGetProxyProtocolSettings.
    3. Moved unused NOVA code into an "#if 0" region.
    
 03-Jun-13 sa002pe on FPHX 02.10 shelton branch
 	Commented out several functions that aren't actually used.
	Changed fnDoWeNeedToUseProxy to also have the protocol & connection type as an input parameter.
	Changed fnMightWeNeedToUseProxy to also have the connection type as an input parameter.
 	Changed fnSetupProxySocket to support the way G9 does the host address.

 31-May-13 sa002pe on FPHX 02.10 shelton branch
	More stuff for having a network proxy:
	1. Changed fnDoWeNeedToUseProxy to check for network proxy usage if using LAN instead of
		if using the NOVA box.
	2. Added fnMightWeNeedToUseProxy.

 18-Mar-13 sa002pe on FPHX 02.10 shelton branch
	Preliminary stuff for having a LAN proxy:
	1. Replaced the include of 7705usb.h (which is for K7) w/ include of usbfunc.h (which is for G9).
	2. Added fnNOVAClearStructures.
	3. Commented out the contents of several functions since G9 doesn't actually support the NOVA box.
	4. Changed fnIsNovaEtherReady to always return FALSE, fnIsNovaNetDisconnected to always return TRUE,
		fnIsNovaWireless to always return FALSE since G9 doesn't actually support the NOVA box.
	5. Changed fnDoWeNeedToUseProxy to always return FALSE until we figure out how to determine
		if a proxy is being used w/ the LAN connection.
	
 ************************************************************************/

#include "cmos.h"
#include "intellilink.h"
#include "networking/externs.h"
#include "NOVAdriver.h"
#include "urlencode.h"
#include "networkmonitor.h"

/********************
*   Local Defines   *
********************/
#define THISFILE "NOVAdriver.c"
#define TXRX_BUFFER_SIZE 1024
#define PROXY_AUTH_SIZE 128
#define SOCKET_HOST_SIZE 64
#define SOCKET_SERVER_URL_SIZE 256
#define SOCKET_RECEIVE_TIMEOUT 60000

extern CSDProxyConfig 			globalProxyConfig;
BOOL remoteLoggingIsOn(void);
STATUS remoteLog(char *string, char *buff);

/********************************
*   Local Function Prototypes   *
********************************/
CSDProxyProtocolSettings *fnGetProxyProtocolSettings(char *pDestinationProtocol);


/******************************************************************************
| FUNCTION: fnDoWeNeedToUseProxy
|
| SUMMARY:  This function is used to check if proxy should be used for the
|           connection. If a LAN connection is used and if globalProxyConfig is
|           set up to allow proxy for the given *pDestinationProtocol and
|           ubConnectionType, this function will return TRUE. Otherwise, it
|           will return FALSE.
|
| PARAMS:   char *pDestinationProtocol - Protocol for the destination URL.
|           UINT8 ubConnectionType - Type of destination connection.
| 
| RETURN:   BOOL - TRUE if proxy should be used, FALSE if it shouldn't be used.
|
| HISTORY:  06/25/2013 - Kevin Brown / Sam Thillaikumaran
|               Modified Sam's NOVA code for the G9 implementation of proxy.
******************************************************************************/
BOOL fnDoWeNeedToUseProxy(char *pDestinationProtocol, UINT8 ubConnectionType)
{
    BOOL fConnectedViaLan = FALSE;
    BOOL fUseProxy = FALSE;
    UINT8 ubWhichLAN = 0;
    CSDProxyProtocolSettings *pProtocolSettings = NULL;

    // RD disable this routine, we handle the process elsewhere.
    return FALSE ;

    #ifdef G900
    {
        if (fnGetConnectionMode() == CONNECTION_MODE_AUTO_DETECT)
        {
            if (connectedToInternetViaLan(&ubWhichLAN) == TRUE)
            {
                fConnectedViaLan = TRUE;
            }
        }
    }
    #elif defined(JANUS) && !defined(K700)
    {
        if (connectedToInternetViaLan() == TRUE)
        {
            fConnectedViaLan = TRUE;
        }
    }
    #endif

    if ((fConnectedViaLan == TRUE) && (fnMightWeNeedToUseProxy(ubConnectionType) == TRUE)) // && (usePcHostForIntelliLink() == FALSE))
    {
        pProtocolSettings = fnGetProxyProtocolSettings(pDestinationProtocol);

        if (*pProtocolSettings->pAddress)
        {
            fUseProxy = TRUE;
        }
    }

    return fUseProxy;
}


/******************************************************************************
| FUNCTION: fnMightWeNeedToUseProxy
|
| SUMMARY:  This function is used to do an initial check to see if proxy might
|           be used for the connection. If the structure has proxy enabled and
|           it is not disabled for the for the given ubConnectionType, this
|           function will return TRUE. Otherwise, it will return FALSE.
|
| PARAMS:   UINT8 ubConnectionType - Type of destination connection.
| 
| RETURN:   BOOL - TRUE if proxy might be used, FALSE if it might not be used.
|
| HISTORY:  06/25/2013 - Kevin Brown / Sandra Peterson
|               Modified Sandra's function to work with the globalProxyConfig
|               structure.
******************************************************************************/
BOOL fnMightWeNeedToUseProxy(UINT8 ubConnectionType)
{
	/* RD - removed 13Nov2017 - progress toward an exclusion list handling system
    BOOL fMightUseProxy = FALSE;

	if ((globalProxyConfig.fEnabled == TRUE) && (globalProxyConfig.pConnectionExcluded[ubConnectionType] == FALSE))
    {
		fMightUseProxy = TRUE;
    }
    return fMightUseProxy;
    */
    return globalProxyConfig.fEnabled ;
}

/******************************************************************************
| FUNCTION: fnGetProxyAuthStringForHeader
|
| SUMMARY:  This function creates the proxy authorization string for the HTTP
|           header. If the globalProxyConfig structure does not contain a
|           username for the given pDestinationProtocol, it will return zero.
|           Otherwise, it will return the length of the proxy authorization
|           string.
|
| PARAMS:   char *pDestinationProtocol - Protocol for the destination URL.
|           char *pBuffer - Buffer for the proxy authorization string.
| 
| RETURN:   SINT32 - Total length of the data written to pBuffer.
|
| HISTORY:  06/25/2013 - Kevin Brown / Sam Thillaikumaran
|               Modified Sam's NOVA code for the G9 implementation of proxy.
******************************************************************************/
SINT32 fnGetProxyAuthStringForHeader(char *pDestinationProtocol, char *pBuffer)
{
    char pProxyAuth[2 * PROXY_NAME_PWD_SIZE] = {0};
    char pProxyAuthB64[3 * PROXY_NAME_PWD_SIZE] = {0};
    UINT32 wB64Length = 0;
    SINT32 lTotalLength = 0;
    CSDProxyProtocolSettings *pProtocolSettings = NULL;
    
    pProtocolSettings = fnGetProxyProtocolSettings(pDestinationProtocol);

    // if there is no username then assume no authentication stage is required.
    if (*pProtocolSettings->pUsername)
    {
        sprintf(pProxyAuth, "%s:%s", (char *)pProtocolSettings->pUsername, (char *)pProtocolSettings->pPassword);
        (void)B64Encode((UINT8 *)pProxyAuth, strlen(pProxyAuth), pProxyAuthB64, sizeof(pProxyAuthB64), (unsigned long *)&wB64Length, FALSE);
        lTotalLength = sprintf(pBuffer, "Proxy-Authorization: Basic %s\r\n", pProxyAuthB64);
        fnDumpStringToSystemLog(pBuffer);
    }
    else
    {
    	dbgTrace(DBG_LVL_INFO, "PROXY: Username is EMPTY no authorization element included\r\n");
    }

    return lTotalLength;
}


/******************************************************************************
| FUNCTION: fnGetProxyProtocolSettings
|
| SUMMARY:  This function returns a pointer to a ProxyProtocolSettings
|           structure from within the globalProxyConfig structure for the given
|           pDestinationProtocol. If an unsupported pDestinationProtocol is
|           passed in, it will default to HTTP.
|
| PARAMS:   char *pDestinationProtocol - Protocol for the destination URL.
| 
| RETURN:   ProxyProtocolSettings - Appropriate protocol settings.
|
| HISTORY:  06/25/2013 - Kevin Brown
|               Initial version.
******************************************************************************/
CSDProxyProtocolSettings *fnGetProxyProtocolSettings(char *pDestinationProtocol)
{
    CSDProxyProtocolSettings *pTempProtocolSettings = NULL;
    // look at the URL to be used and check it against the proxy's exclusion list
    // this will also check the proxy enable flag so INCLUDED is only true if enabled and not in list.
    CRACKED_URL crkUrl ;
    if(crackUrl(&crkUrl, pDestinationProtocol) == NU_SUCCESS)
    {
    	if(getProxyUseState(crkUrl.host) != PROXY_URL_INCLUDED)
    		return NULL;
    }
    
    if (findString("http://", pDestinationProtocol))
    {
        pTempProtocolSettings = &globalProxyConfig.Http;
    }
    else if (findString("https://", pDestinationProtocol))
    {
        pTempProtocolSettings = &globalProxyConfig.Https;
    }
    else
    {
        /* Unsupported protocol. Default to HTTP for now. */
        pTempProtocolSettings = &globalProxyConfig.Http;
    }
    
    return pTempProtocolSettings;
}


/******************************************************************************
| FUNCTION: fnSetupProxySocket
|
| SUMMARY:  This function is used to set up the proxy socket for communication.
|           It modifies the *pSockAddress parameter with the data stored in the
|           globalProxyConfig struct for the given protocol. The given protocol
|           is supplied by the *pDestinationProtocol parameter. Call this only
|           after checking if proxy is needed with fnDoWeNeedToUseProxy().
|
| PARAMS:   ADDR_STRUCT *pSockAddress - Struct to modify with proxy data.
|           char *pDestinationProtocol - Protocol for the destination URL.
| 
| RETURN:   char - The proxy socket setup code.
|
| HISTORY:  06/25/2013 - Kevin Brown / Sam Thillaikumaran
|               Modified Sam's NOVA code for the G9 implementation of proxy.
******************************************************************************/
SINT8 fnSetupProxySocket(ADDR_STRUCT *pSockAddress, char *pDestinationProtocol)
{
    char pProxyAddress[PROXY_ADDRESS_SIZE + 1] = {0};
    char pDebugBuffer[256] = {0};
    UINT8 pProxyIp[4] = {0};
    CRACKED_URL crackedProxyUrl = {0};
    NU_HOSTENT hostEnt = {0};
    CSDProxyProtocolSettings *pProtocolSettings = NULL;
    STATUS status = NU_SUCCESS;
    
    pProtocolSettings = fnGetProxyProtocolSettings(pDestinationProtocol);
    sprintf(pProxyAddress, "%s/", pProtocolSettings->pAddress);

    /* Parse the address for the hostname */
    if ((status = crackUrl(&crackedProxyUrl, pProxyAddress)) != NU_SUCCESS)
    {
        sprintf(pDebugBuffer, "Failed to crack proxy URL: %s", pProtocolSettings->pAddress);
        fnDumpStringToSystemLog(pDebugBuffer);
        
        if (remoteLoggingIsOn() == TRUE)
        {
            remoteLog(NULL, pDebugBuffer);
        }
        
        return PROXY_SOCKET_ERR_CRACK_URL;
    }

    /* Fill pProxyIp with IP data */
    if (isIpAddress(crackedProxyUrl.host) == TRUE)
    {
        dottedQuadToArray(pProxyIp, crackedProxyUrl.host);
    }
    else if ((status = NU_Get_Host_By_Name(crackedProxyUrl.host, &hostEnt)) == NU_SUCCESS)
    {
        memcpy(pProxyIp, hostEnt.h_addr, sizeof(pProxyIp));
    }
    else
    {
        sprintf(pDebugBuffer, "Failed to obtain an IP address for proxy hostname: %s", crackedProxyUrl.host);
        fnDumpStringToSystemLog(pDebugBuffer);
        
        if (remoteLoggingIsOn() == TRUE)
        {
            remoteLog(NULL, pDebugBuffer);
        }
        
        return PROXY_SOCKET_ERR_RESOLVE_HOST;
    }

    sprintf(pDebugBuffer, "Proxy IP is %d.%d.%d.%d port:%d", pProxyIp[0], pProxyIp[1], pProxyIp[2], pProxyIp[3], pProtocolSettings->uwPort);
    fnDumpStringToSystemLog(pDebugBuffer);

    /* Set up the socket address */
    pSockAddress->family = NU_FAMILY_IP;
    pSockAddress->name = "Proxy";
    pSockAddress->port = pProtocolSettings->uwPort;
    memcpy(pSockAddress->id.is_ip_addrs, pProxyIp, sizeof(pSockAddress->id.is_ip_addrs));
    return PROXY_SOCKET_SUCCESS;
}


