/************************************************************************
*	PROJECT:		Spark
*   COMPANY:        Pitney Bowes
*   AUTHOR :        Rick Rudolph
*	MODULE NAME:	$Workfile:   global.c  $
*	REVISION:		$Revision:   1.5  $
*	
*	DESCRIPTION:	Common utility and conversion functions..
*
* ----------------------------------------------------------------------
*               Copyright (c) 1998 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*
*	REVISION HISTORY:
*	$Log:   H:/group/SPARK/ARCHIVES/UIC/GLOBAL.C_V  $
* 
*    Rev 1.5   08 Aug 1999 19:20:40   arsenbg
* corrected spelling of pIntertaskMsg in GetIntertaskPointerAndLength
* 
* 
* 
*    Rev 1.4   Aug 06 1999 09:54:56   kirscwa
* Added GLOBAL_PTR_DATA to 
* GetIntertaskPtrAndLength().
* 
*    Rev 1.3   May 03 1999 16:06:04   monrogt
* Added Support For Internal Modem
* Modem Task
* C6029 Conversion Routines
* 
* 
*    Rev 1.2   29 Oct 1998 17:07:40   rudolra
*  
* 
*    Rev 1.1   Aug 28 1998 12:35:10   monrogt
*  
* 
*    Rev 1.0   29 Jul 1998 13:35:30   rudolra
* Initial Check in.
* 
* 
* 
*************************************************************************/
#include "pbos.h"


/******************************************************************/
/*                                                                */
/*                   BINARY TO BCD CONVERSION                     */
/*                                                                */
/******************************************************************/

unsigned char BinToBCD(unsigned char BinaryByte) {
unsigned char bBCDByte = 0;

	if( BinaryByte <= 99 ) {
		bBCDByte += (BinaryByte % 10);
		BinaryByte /= 10;

		bBCDByte += ( (BinaryByte % 10) * 16 );
		BinaryByte /= 10;
								
		return(bBCDByte);
	}
	else
		return( 0 );
}



/**************************************************/
/*                                                */
/*            CONVERT BCD BYTE TO BINARY          */
/*                                                */
/**************************************************/

unsigned char BCDtoBinary(unsigned char BCDVal) {

	unsigned char ParamBinary = 0;
	unsigned char BinVal = 0x00;

     /* Check for a valid BCD formated number */
    if( ((BCDVal & 0xF0) <= 0x90) &&
	((BCDVal & 0x0F) <= 0x09) ) {

	while(BCDVal > 9) {
	    BCDVal -= 0x10;
	    BinVal += 10;
	}
	BinVal += BCDVal;     
    }
    return(BinVal);
}

/**************************************************************************
   MODULE     :    GetIntertaskPointerAndLength
   AUTHOR    :     Wes Kirschner
   DESCRIPTION:    Returns the pointer and length from the intertask file.
 ***************************************************************************/   
void GetIntertaskPointerAndLength (void **ptr, unsigned long *lwLength, void *pMsg)
{
	INTERTASK *pIntertaskMsg = (INTERTASK *) pMsg ;
	
	if (pIntertaskMsg -> bMsgType == PART_PTR_DATA || pIntertaskMsg -> bMsgType == COMM_RECEIVE_DATA  ||
	    pIntertaskMsg -> bMsgType == GLOBAL_PTR_DATA)
	{
		*ptr = (void *) pIntertaskMsg -> IntertaskUnion.PointerData.pData ;
		*lwLength = pIntertaskMsg->IntertaskUnion.PointerData.lwLength;
	}
        else if (pIntertaskMsg -> bMsgType == COMM_DATA)
        {
                *lwLength = pIntertaskMsg -> IntertaskUnion.CommData.bLength;;
                *ptr = (void *) (pIntertaskMsg -> IntertaskUnion.CommData.bByteData);

        }
	else
	{
		*ptr = (void *) pIntertaskMsg -> IntertaskUnion.bByteData;
		*lwLength = 0;
	}
}	




