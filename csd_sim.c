
#ifdef MEMORYTEST

#include "nucleus.h"
#include "networking/nu_networking.h"
#include "cJSON.h"
#include "wsox_tx_queue.h"
#include "events.h"
#include "statemachine.h"
#include "varassert.h"
#include "memory_tests.h"

#define  CRLF   "\r\n"

/* ----------------------------------------------------------[ State Data ]-- */
typedef struct
{
    timer_id_t chg_state_timer_id;
} sm_mode_t;

sm_context_t sm_mode_context;

trigger_state_t mode_trigger;

sm_mode_t sm_mode;

typedef struct
{
    timer_id_t  chg_state_timer_id;
    timer_id_t  test_timer_id;
    uint32_t	count_target;
//    uint32_t    test_delay;
//    uint32_t	count;

//
//    uint32_t	address;
//    uint32_t	size;
//    uint32_t	value;
//    uint32_t	memory_type;
//    uint32_t	test_type;

    memory_test_data_t test_data;

} sm_test_data;

sm_test_data sm_test;

void gettestdata(memory_test_data_t *testdata)
{
	memcpy(testdata, &sm_test.test_data, sizeof(memory_test_data_t));
}

FORWARD_DECL sm_tbl_tt smtbl_mode_idle;

FORWARD_DECL sm_tbl_tt smtbl_mode_starting_test;
FORWARD_DECL sm_tbl_tt smtbl_mode_running_test;
FORWARD_DECL sm_tbl_tt smtbl_mode_stoping_test;

void smafn_notify_mode_change(sm_context_t *p_context, ev_t *p_ev, void *p_param)
{
    cJSON *chgEvt = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON_BROADCAST, 0, {.cjson_info = {chgEvt}}};

    cJSON_AddStringToObject(chgEvt, "MsgID", "CsdStateChangeEvent");
    cJSON_AddStringToObject(chgEvt, "State", p_param);

    addEntryToTxQueue(&entry, NULL, "  smafn_notify_mode_change: Added CsdStateChangeEvent to tx queue for broadcast. " CRLF);

   //todo: send all listeners a state change message with state (char *)p_param 

}

static void smafn_start_chg_test_state_timer(sm_context_t *p_context, ev_t *p_ev, void *p_param)
{
   uint32_t timeout_val;

   varassert(p_context != NULL, "%s: p_context shouldn't be NULL" CRLF, __func__);

   timeout_val = 2 * 1000L;

   sm_test.chg_state_timer_id = ev_create_timer_rel(timeout_val, 0, 0, 0, EV_TIMEOUT_CHG_TEST_STATE);

   UNUSED_PARAMETER(p_ev);
   UNUSED_PARAMETER(p_param);
}

 
static void smafn_start_test_delay(sm_context_t *p_context, ev_t *p_ev, void *p_param)
{
   uint32_t init_delay;
   uint32_t period;

   varassert(p_context != NULL, "%s: p_context shouldn't be NULL" CRLF, __func__);

   init_delay = 500L; /* 0.5 sec */
   period     = sm_test.test_data.memory_test_req.delay;

   sm_test.test_timer_id = ev_create_timer_rel(init_delay, period, 0, 0, EV_TIMEOUT_TESTS);

   UNUSED_PARAMETER(p_ev);
   UNUSED_PARAMETER(p_param);
}

static void smafn_stop_test_delay(sm_context_t *p_context, ev_t *p_ev, void *p_param)
{

   varassert(p_context != NULL, "%s: p_context shouldn't be NULL" CRLF, __func__);

   if (sm_test.test_timer_id != 0)
       ev_kill_timer(sm_test.test_timer_id);

   sm_test.test_timer_id = 0;

   UNUSED_PARAMETER(p_ev);
   UNUSED_PARAMETER(p_param);
}


static void smafn_perform_memory_test(sm_context_t *p_context, ev_t *p_ev, void *p_param)
{
   varassert(p_context != NULL, "%s: p_context shouldn't be NULL" CRLF, __func__);

   if(sm_test.test_data.buffer == 0)
   {
	   sm_test.test_data.buffer = malloc(512);
   }

   //if (perform_memory_test(&sm_test.test_data))
   if(PerformMemoryTests(&sm_test.test_data))
   {
       cJSON *mpPrintedEvt = cJSON_CreateObject();
       WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON_BROADCAST, 0, {.cjson_info = {mpPrintedEvt}}};

       sm_test.test_data.memory_test_req.count++;
        cJSON_AddStringToObject(mpPrintedEvt, "MsgID", "CsdMemoryTestEvent");

        memory_test_get_json_object_cJSON(mpPrintedEvt);
        addEntryToTxQueue(&entry, NULL, "  smafn_perform_memory_test: Added CsdMemoryTestEvent to tx queue for broadcast. " CRLF);
       /* todo: send all listeners a CsdMailPiecePrintedEvent */
   }
   else
   {
       ev_raise_event(EV_STOP_TEST_REQ, NULL);
   }


   UNUSED_PARAMETER(p_ev);
   UNUSED_PARAMETER(p_param);
}

static void smafn_chg_state_if_test_tgt_reached(sm_context_t *p_context, ev_t *p_ev, void *p_param)
{
   if (sm_test.count_target <= sm_test.test_data.memory_test_req.count)
   {
       p_context->next_sm_tbl = p_param;
   }

   UNUSED_PARAMETER(p_ev);
}
sm_tbl_entry_t smtbl_mode_idle_entries[] = 
{
   {EV_STATE_ENTRY,              smafn_notify_mode_change,            "Idle"},

   {EV_START_TEST_REQ,           smafn_change_state_machine,          &smtbl_mode_starting_test},

   SM_TBL_END
};


sm_tbl_entry_t smtbl_mode_starting_test_entries[] =
{
	{EV_STATE_ENTRY,              smafn_notify_mode_change,            "Starting Tests"},
	{EV_STATE_ENTRY,              smafn_start_chg_test_state_timer,    0},

	{EV_TIMEOUT_CHG_TEST_STATE,   smafn_change_state_machine,          &smtbl_mode_running_test},

    SM_TBL_END
};


sm_tbl_entry_t smtbl_mode_running_test_entries[] =
{
   {EV_STATE_ENTRY,              smafn_notify_mode_change,            "Running Tests"},
   {EV_STATE_ENTRY,              smafn_start_test_delay,       		  0},

   {EV_TIMEOUT_TESTS,      		 smafn_perform_memory_test,           0},
   {EV_TIMEOUT_TESTS,      		 smafn_chg_state_if_test_tgt_reached, &smtbl_mode_stoping_test},

   {EV_STOP_TEST_REQ,            smafn_change_state_machine,          &smtbl_mode_stoping_test},

   SM_TBL_END
};

sm_tbl_entry_t smtbl_mode_stopping_test_entries[] =
{
   {EV_STATE_ENTRY,              smafn_notify_mode_change,            "Stopping Test"},
   {EV_STATE_ENTRY,              smafn_stop_test_delay,        	  	  0},
   {EV_STATE_ENTRY,              smafn_start_chg_test_state_timer,    0},

   {EV_TIMEOUT_CHG_TEST_STATE,        smafn_change_state_machine,     &smtbl_mode_idle},

   SM_TBL_END
};


sm_tbl_tt smtbl_mode_idle                    = {(char *)"mode idle",                  smtbl_mode_idle_entries};

sm_tbl_tt smtbl_mode_starting_test			 = {(char *)"mode startingtest",          smtbl_mode_starting_test_entries};
sm_tbl_tt smtbl_mode_running_test            = {(char *)"mode runningtest",           smtbl_mode_running_test_entries};
sm_tbl_tt smtbl_mode_stoping_test            = {(char *)"mode stopingtest",           smtbl_mode_stopping_test_entries};

/* --------------------------------------------------------[ Event filter ]-- */

static void sm_csd_sim_event_sink( ev_t *ev_ptr)
{

   if( ev_ptr->ev_id == EV_TIMEOUT_CHG_STATE)
   {
      if( ev_ptr->ev_data.d.timer.timer_id == sm_mode.chg_state_timer_id )
      {
         sm_handle_event(&sm_mode_context, ev_ptr);
      }
   }
   else if( ev_ptr->ev_id == EV_TIMEOUT_CHG_TEST_STATE)
   {
		if( ev_ptr->ev_data.d.timer.timer_id == sm_test.chg_state_timer_id )
		{
		   sm_handle_event(&sm_mode_context, ev_ptr);
		}
   }
   else if (ev_ptr->ev_id == EV_START_TEST_REQ)
  {
	  /* todo don't assume num_dec == 3 */
	  memset(&sm_test.test_data, 0, sizeof(sm_test.test_data));
	  memcpy(&sm_test.test_data, &ev_ptr->ev_data.d.memory_test_data, sizeof(sm_test.test_data));
	  memory_test_init(&sm_test.test_data.memory_test_req, 0, 0);

	  sm_handle_event(&sm_mode_context, ev_ptr);
  }
   else
   {
      sm_handle_event(&sm_mode_context, ev_ptr);
   }
}   

void csd_sim_init()
{
    ev_initialise();

    memset(&sm_mode, 0, sizeof(sm_mode));

    sm_mode_context.type = SM_CONTEXT_MODE;

    sm_mode_context.sm_tbl = &smtbl_mode_idle;

    sm_mode_context.next_sm_tbl = sm_mode_context.sm_tbl;

    sm_mode_context.p_trigger = &mode_trigger;

    ev_register_event_handler( EV_TIMEOUT_CHG_STATE        , sm_csd_sim_event_sink);

    ev_register_event_handler( EV_START_TEST_REQ	       , sm_csd_sim_event_sink);
    ev_register_event_handler( EV_STOP_TEST_REQ	           , sm_csd_sim_event_sink);
    ev_register_event_handler( EV_TIMEOUT_CHG_TEST_STATE   , sm_csd_sim_event_sink);
    ev_register_event_handler( EV_TIMEOUT_TESTS	           , sm_csd_sim_event_sink);
}

#endif /* MEMORYTEST */
