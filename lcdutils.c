/************************************************************************
*	PROJECT:		Janus
*	MODULE NAME:	$Workfile:   LCDUTILS.C  $
*	REVISION:		$Revision:   1.7  $
*	
*	DESCRIPTION:	LCD support routines
*
* ----------------------------------------------------------------------
*               Copyright (c) 2003 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*
*	REVISION HISTORY:
* 	$Log:   H:/group/SPARK/ARCHIVES/DM150/uic_app/LCDUTILS.C_v  $
 * 
 *    Rev 1.7   15 Jul 2004 18:41:40   HO501SU
 * Changed prototype for putString
 * 
 *    Rev 1.6   02 Sep 2003 15:14:36   BR507HA
 * Remove unused stuff
 * 
 *    Rev 1.5   01 Aug 2003 16:02:56   BR507HA
 * Disabled putString() output to serial port to avoid ASIC UART lockups.
 * 
 *    Rev 1.4   28 May 2003 14:32:00   BR507HA
 * Added timeout in PutString() to avoid lockup if UART hangs.
 * Fixed platform port conditional
 * 
 *    Rev 1.3   16 May 2003 15:24:02   BR507HA
 * Comment out PutString() writes to serial port
 * 
 *    Rev 1.2   May 05 2003 09:41:18   mozdzjm
 * Added check to avoid sending debug messages out of the serial port if we are debugging the platform.
 * 
 *    Rev 1.1   19 Mar 2003 10:13:40   defilcj
 * changes for OS_06 build
*     
*************************************************************************/

#include "pbos.h"
#include "lcd.h"
#include "rftypes.h"
//#include "lcdutils.h"
#include "pb232.h"
#include "commglob.h"

#define ABACUS_VERBOSE

#ifdef ABACUS_VERBOSE
void AbacusDisp();
#endif

static int currentLine;
static unsigned char lines;
static unsigned char columns;


void initDisplay(void)
{
	fnLCDInit();
	fnLCDGetStandardFontInfo(&lines, &columns);
}

void putMsg(char *msg, unsigned char lineNumber)
{
// No need to write to the RAM LCD shadow for Horizon CSD, just put the message in the log
	dbgTrace(DBG_LVL_INFO,"putMsg: %s\n", (unsigned char *)msg);
	
}


//*****************************************************************************
// putString - Debug routine to send string to PLATFORM serial port with no formatting
//*****************************************************************************
void putString(char *msg, int lineNumber)
{
	char  dbg_buf[200];

	sprintf(dbg_buf,"%s#%d",msg,lineNumber); 
	fnSystemLogEntry(SYSLOG_TEXT, dbg_buf, strlen(dbg_buf));
}


unsigned char nextLine(void)
{
	currentLine =  currentLine < lines -1 ? currentLine + 1 : 0;
	
	return currentLine;
}

void clearnextLine(void)
{
	fnLCDPutLineDirect((unsigned char *)"                                                    ", nextLine());	 //spaces to clear line!
	fnLCDRefresh();
}


void clearDisplay(void)
{
	currentLine = 0;
	fnLCDClear();
}

void pushDisplay(void)
{
    fnLCDTakeSnapshot();
    clearDisplay();
}

void popDisplay(void)
{
    fnLCDRestoreSnapshot();
    fnLCDRefresh();
}


void pauseMsg(char *msg, unsigned int seconds)
{
int line = nextLine();

    if(strlen(msg) < 32)
    {
        while(seconds)
        {
        char scratchPad[64];
    
        memset(scratchPad, '\0', sizeof(scratchPad));
        sprintf(scratchPad,"%s... (%d)", msg, seconds);
	    fnLCDPutLineDirect((unsigned char *)"                                                    ", line);	 //spaces to clear line!
	    fnLCDRefresh();
        putMsg(scratchPad, line);    
        seconds--;
        OSWakeAfter(1000);
        }
    }
    else
    {
        putMsg("Pausing...", nextLine());    
        OSWakeAfter(seconds * 1000);
    }


}


#ifdef ABACUS_VERBOSE

void AbacusDisp()
{
	currentLine = 0;
	fnLCDGetStandardFontInfo(&lines, &columns);
}

#endif