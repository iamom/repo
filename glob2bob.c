/************************************************************************
*   PROJECT:        Phoenix
*   MODULE NAME:    glob2bob.c
*   
*   DESCRIPTION:    This file contains tables and functions for interpreting 
*                   the glob definition records.  Globs are hunks of data
*                   whose format and content are determined by a definiton
*                   record.  Though the format and content of the data
*                   glob may change, the UIC code handling the data 
*                   should NOT change, because the UIC uses the definition
*                   record, which is set by the EMD, to interpret the glob.
*                   Of course, the UIC code may need enhancing as new TYPES 
*                   of data, or variables that are not yet supported, are  
*                   added to the glob.
*
* ----------------------------------------------------------------------
*               Copyright (c) 2003 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
 ----------------------------------------------------------------------
 CHANGE HISTORY:

 04-Nov-14 sa002pe on FPHX 02.12 int branch
 	Added new entry to the glob2bob table for GLOBID_IPSD_FLEXDEBIT_DSASIG for use initially by G9 Japan Car Tax.

 08-Mar-13 sa002pe on FPHx 02.10 shelton branch
 	Added new entry to the glob2bob table for GLOBID_IPSD_FLEXDEBIT_HMAC for use initially by G9 Philipines.

 2009.03.23 FPHX 1.15
  For consistency, replace JSCM_ error codes with BOB_ error codes.
     
 2008.10.21 FPHX 1.11    
  Merged from Clarisa's branch:
  + 2008.09.30 FPHX 1.11    
    - Moved function fnReverseIt to utils.c
  + 2008.09.04  Clarisa Bellamy - FPHX 1.11 dev.
    Modify the reporting of errors when the PSD Parameter List glob is not long
    enough for all the parameters requested.  This can happen when an EMD that 
    supports newer IPSDs is used with older version IPSDs.  Because of backwards
    compatibility, this is not necessarily an error.  Since the PSD Parameter 
    List is a message that is used VERY often, we don't want to report this 
    condition every time.
    - Add new local definitions.
    - Add new local variables: pG2BListBadPsdParams, ubG2BNumBadPsdParams, 
      fG2BGettingPsdParamList, uwG2BIgnorePSDParmErrCount.
    -  Add new functions: fnG2BClearBadParamList, fnG2BReportG2BError, 
       fnG2BSavePsdParamListError, fnG2BReportPSDParamListErrors.
    - Modify functions fnIPSDDistribParamList and fnGetDefRecordInfo.
   Merge from Janus:
    - In the pMapGlobVarToBobVar table, correct the type for bob variable
      BOBID_IPSD_IBILITEVENDORMODEL.  When converted to ASCII, the hex value
      is displayed, instead of the binary value.
    - The names of the Field Ops (Variable Types in the Definition records)
      that are defined in fwrapper.h have changed so that they all have the 
      common prefix DRFOP_.  So...
      Replace the eGlobVarTypes enums with the FieldOps enums.
 +----

 2008.08.27 Joe Qu  FPHX 01.13
  - Added an entry in pMapGlobVarToBobVar[] for  NetSet2 PST Version.

 2008.08.07  Clarisa Bellamy - FPHX 1.11 dev.
  Get rid of compiler warning.
  - Change function declaration for fnUpdatePSDParamListFromBob to return void, 
    since it does not actually return anything.
  Get rid of LINT warnings.
  - In function fnGlobFindConvertFunc, verify ptr is not NULL_PTR before using
    it.
  - Add include of clock.h, and utils.h.  Remove include of ossetup.h.
  - Update comments.
  - Cast function calls as void if return value is ignored.
  - Add casts; cast some pointers as ptrs to const.
  - Comment out aGlob2UniTbl[], since it is not used.  Remove unused local variables. 

 2008.04.14 Clarisa Bellamy     FPHX 01.10
  - Remove all references to wIPSD_rxGlobSize, which stores the size of the last
    glob received.  (This was introduced when there was only 1 glob.)  Now each
    glob has its own size variable.  The reply definitions in orbit must be 
    updated so that they are set correctly each time a glob is received.
  - Modify fnGetDefRecordInfo to take an argument that is the size of the
    glob buffer.  Add check for glob buffer size when the glob is the destination.
    Add logging of error if access is attempted beyond glob buffer size.
  - Modify fnConvertFromGlobToBobToGlob to take an argument that is the size 
    of the glob buffer and pass it to fnGetDefRecordInfo.
  - Modify fnIPSDLoadCreateIndiciaGlob to pass the max size of the create indicia
    glob to fnConvertFromGlobToBobToGlob.
  - Modify fnUpdatePSDParamListFromBob to pass the max size of the parameter 
    list glob to fnConvertFromGlobToBobToGlob.
  - Modify fnIPSDDistribParamList to pass the size of the glob that was just
    received.

 2008.03.21 Clarisa Bellamy  FPHX01.10
  - Modify fnIPSDLoadCreateIndiciaGlob to use the variable wIPSD_flexDebitDataSize
    to load the GLOBID_FLEXDEBIT_DATA variable if the size in the def record is FFFF.
    This way, the IPB_CREATE_INDICIA_DEF_REC can be the same for MOST PCNs that use
    flex debit.

 2008.03.13 Clarisa Bellamy  FPHX01.10
    Forgot to add usGlob2BobArrayLen.  OOps!

 2008.03.12 Clarisa Bellamy  FPHX01.10
  For Brazil:
   Added new entry to the glob2bob table for GLOBID_IPSD_FLEXDEBIT_DESMAC.

 2008.03.06 Clarisa Bellamy  FPHX01.10
  Merge flex debit changes from Janus 15.04.02:
  - Added 16 new glob variables in pMapGlobVarToBobVar table.
  - New conversion type, RTYPE_BYTEARY, in aGlobConvFuncTableXref table, 
  - Add new conversion function, aConvFuncTbl_bytearray.
  Bug fix from 15.04.02: 
  - Change fnCopyItLeftJust to copy based on size of 
    Dest, if dest is smaller than source.

 2007.01.24 Clarisa Bellamy  FPHX - 1.03
  - Added new function that will update the PSD parameter glob with the 
    modified bob variable values so that the updated values are used by 
    the image generator in the print. 
    This is required for FPHX, where we don't have time to read the glob
    between the debit and the print, but for some PCNs PSD parameter glob 
    data that varies per mail piece is in the print.
  - Removed excessive typecasts.

* ----------------------------------------------------------------------
*   PRE-CLEARCASE REVISION HISTORY:
*   $Log:   H:/group/SPARK/ARCHIVES/UIC/glob2bob.c_v  $
* 
*    Rev 1.19   06 Jul 2005 16:13:24   cr002de
* 1. support for new 4.0 ibutton parameters
* 
*    Rev 1.18   Nov 28 2004 20:46:14   arsenbg
* the following changes were made in this file and other files in support of Canada ECDSA PCT operations:
* ibobutil.h    added new long parameter id: MFG_IPSDPROVIDERKEY
* 
* ibobutils.c   added new record in mfgVaultIPSDLongRef for MFG_IPSDPROVIDERKEY
* cjunior       added reference for BOBID_IPSD_CANADA_PROVKEYREV (ulIPSD_ProviderKeyRev)
* glob2bob.h    unmasked GLOBID_CANADAPROVKEYREV
* glob2bob.c    unmasked GLOBID_CANADAPROVKEYREV
* ipsd.h        added reference for ulIPSD_ProverKeyRev
* ipsd.c        added creation of ulIPSD_ProverKeyRev storage
* 
*    Rev 1.17   Aug 20 2004 17:53:34   arsenbg
* changes in the format indicator for german glob variables
* 
*    Rev 1.16   Aug 11 2004 10:31:58   arsenbg
* added GLOBID_ADDDRTOARONPVR to pMapGlobVarToBobVar
* 
*    Rev 1.15   Aug 09 2004 18:28:52   arsenbg
* unmasked the german piece count and service ID
* 
*    Rev 1.14   Jul 26 2004 18:43:54   arsenbg
* corrected the table pMapGlobVarToBobVar so that it matches the flash spec, but
* some of the entries will remain masked out until matching bob task variables can be 
* created in Orbit.
* 
*    Rev 1.13   Jul 25 2004 20:01:54   arsenbg
* added to pMapGlobVarToBobVar cross-references for:
* GLOBID_GERMANARATLASTREF
* GLOBID_GERMANCRYPTOSTR
* GLOBID_GERMANPOSTIDDATA
* GLOBID_GERMANAUTHKEYREV
* 
*    Rev 1.12   10 Jun 2004 16:47:36   CR002DE
* 1. add new IPSD variables for Germany
* 2. make entries in the message control table for new Germany commands
* 3. NO NEW SCRIPT SUPPORT YET
* 4. new ibutton error codes supported
* 
*    Rev 1.11   18 Mar 2004 16:18:56   CR002DE
* 1. fixed a problem with a function pointer copy
* satement in fnGlobFindConvertFunc.
* 
*    Rev 1.10   21 Jan 2004 15:46:32   defilcj
* 1. change name of IPB_DEBIT_CERT_DEF_REC to
* IPB_GET_INDICIA_DEF_REC to match data dictionary reference
* 
*    Rev 1.9   08 Oct 2003 10:24:42   defilcj
* 1. created a variable for the glob size read from the ibutton.
* 2. it is referenced when parsing data from a glob based on a definition record.
* 3. if the offset to a member of the glob is larger than the size of the glob...ahh we don't parse it.
* 4. this allows definiton records to be APPENDED with more data as we march ahead.
* 5. added new psd parameter variables for newer ibutton
* 
* 
*    Rev 1.8   Sep 09 2003 22:54:54   CL501BE
* Requries ipsd.h rev. 1.12, and ipsd.c rev. 1.9.
* _ Name of variable changed from pIPSD_indicia_data to 
*   pIPSD_barcodeData.
* 
*    Rev 1.7   08 Sep 2003 15:23:20   defilcj
* 1. fix compile warnings for Janus build
* 
*    Rev 1.6   Sep 03 2003 20:26:28   CL501BE
* Requires datdict.h rev. 1.168 and glob2bob.h rev. 1.3
* _ Add 2 new functions: fnGetFromPSDParamList and fnGetFromDebitCert.
*   These are just the functions fnReadPSDParms and fnReadDebitCertParms
*   modified to convert to EITHER unicode OR Ascii.  
* _ Then I trimmed the original functions to call the new ones.
* 
*    Rev 1.5   21 Aug 2003 11:33:38   defilcj
* 1. fix janus link error
* 
*    Rev 1.4   21 Aug 2003 11:22:46   defilcj
* 1. fix compile error for Janus
* 
*    Rev 1.3   21 Aug 2003 10:41:38   defilcj
* 1. changed i packed byte parameters prefix to 
* IPB_ so it is consistent with the data dictionary doc.
* 
*    Rev 1.2   Aug 02 2003 19:23:00   bellamyc
* _ Change local variable names and structure member names to use convention.
* _ Added missing return value for one of the conversion routines.
* _ Changed size of some variables for consistency.
* _ Modified union definitions to put larger value first.
* _ Changed order of members in TypeToTblXref structure to save space, and made 
*   the conversion tables constants.
* _ Add Craig's stuff for using definition records to grab from a data glob
*   to a unistring: Added local function (from Janus/fwrapper.c) and 1 local 
*   function and table from (Janus/creport.c)  Conditionally added 2 more 
*   functions (also from Janus/creport.c) that will soon be supported by changes 
*   to utils.c and cbobutils.c  
* 
*    Rev 1.1   Jun 28 2003 18:58:18   bellamyc
*  _ Completed and uncommented the entries in the MapGlobVarToBobVar table for 
*   the postage value and the rateclass, used in the createIndicia glob.
* _ Changed most of the chars to uchars.
* _ Replaced the fnGetPreCreateParams function with fnIPSDLoadCreateIndiciaGlob,
*   which is NOT a script function, but is called by one instead.  This function
*   has now been debugged and works, AND determines the SIZE of the glob data
*   and stores the size.  Very Important!
* _ Moved fnIPSDDistribParamList function here, and now it is NOT a postMessage
*   function, but called by one instead.
* 
*    Rev 1.0   Jun 26 2003 15:46:10   bellamyc
* Initial Revision.
*   This file contains tables and functions for interpreting 
* the glob definition records.  
*   Globs are hunks of data whose format and content are determined 
* by a definiton record.  Though the format and content of the data
* glob may change, the UIC code handling the data should NOT change, 
* because the UIC uses the definition record, which is set by the EMD, 
* to interpret the glob.
*   Of course, the UIC code may need enhancing as new TYPES of data, 
* or variables that are not yet supported, are added to the glob.
* 
*************************************************************************/


#include <stdio.h>
#include <string.h>
#include "cjunior.h"
#include "clock.h"      // MdyValid()
#include "datdict.h"
#include "fwrapper.h"
#include "global.h"
#include "glob2bob.h"
#include "ipsd.h"
#include "tealeavs.h"
#include "utils.h"      // fnLogError(), fnReverseIt()


// Turn off lint warnings for the file:
//   641:  converting enums to ints.
//lint -e641  

// ------------------------------------------------------
// Local Definitions:

// These are used for reporting the globIDs when the PSD ParameterList
//  message is missing some of the data that is defined in the PSD parameter
//  list definition record.  This is not an error, since it will happen due
//  to backwards compatibility of current EMDs with older PSDs.  However,
//  it is reported in the system log on a regular basis, just less often 
//  than it actually happens.
// Define the max number of GlobIDs that can be stored and reported in the 
//  system log message.
#define MAX_GLOBIDS_TO_REPORT   100
// Define the number of GlobIDs that can be reported on each line of the system log.
#define MAX_GLOBIDS_TO_REPORT_EACHLINE   5
// Most of the time, we ignore glob2bob overrun errors if the message is a 
//  PSDParameter List reply message.  
// This defines the number of times in a row we DON'T check for the error, 
//  between each time that we do check.
#define UNREPORTED_ERROR_INTERVAL  25




// ------------------------------------------------------
// Prototypes of local functions:
#ifndef JANUS
//  Prototype of local function.  This is defined in fwrapper.h for JANUS and later.
BOOL fnFlashGetIBPRecVar( tIPSDdefRecord* pFieldRec, unsigned short wRecID, unsigned short wVarID )
#endif

void fnG2BClearBadParamList( void );
void fnG2BReportG2BError( tStoreGlob *pStoreStruct ); 
void fnG2BSavePsdParamListError( tStoreGlob *pStoreStruct ); 
void fnG2BReportPSDParamListErrors( void ); 


// ------------------------------------------------------
// Local Constants:

const UINT16 bUnreportedErrorInterval = UNREPORTED_ERROR_INTERVAL;


// !!!! Debug stuff
//const UINT16 bUnreportedErrorInterval = 10;
//const UINT8  bSubFromPSDParamListSize = 25;
// !!!!!!!!!!!!!!!!!


// ------------------------------------------------------
// Local variables:

//      Flag, that "this" glob is a PSD Parameter List glob.
static BOOL    fG2BGettingPsdParamList = FALSE;
//      List the GlobIDs that are in the Definition record, but not in the PsdParameterList.
static UINT16  pG2BListBadPsdParams[ MAX_GLOBIDS_TO_REPORT ];
//      Number of GlobIDs in the List of Bad PSD Params:
static UINT8   ubG2BNumBadPsdParams;
//      Only report PSParameterList glob access errors when this is zero.
//      This is incremented everytime we parse the PSDParameter List from the IPSD.
//      It is reset to 0 when it reaches the value of bUnreportedErrorInterval. 
static UINT16 uwG2BIgnorePSDParmErrCount = 0;




// ------------------------------------------------------
//      TABLE TO MAP GLOB FIELD ID TO BOB VARIABLE ID:

// Turn off lint warnings for the table:
//   778:  >> operation on 0, it is part of the cmpc() macro.
//   572:  shifting a byte by 8 bits.
//lint -e778  -e572  

//  Note:  Irrespective of type, the bob variables larger than 1 byte are all 
//  character arrays, to avoid boundary issues.  
const tG2B2GIDmap  pMapGlobVarToBobVar[] =
{   // GlobVarID                         BobVarID                                    BobVarType
  
  { cmpc( GLOBID_POSTAGEMAX ),          cmpc( BOBID_IPSD_POSTAGEMAX ),              DRFOP_BE_MON_3DEC,  },
  { cmpc( GLOBID_PIECECOUNT ),          cmpc( BOBID_IPSD_PIECECOUNT ),              DRFOP_LE_INT,       }, 
  { cmpc( GLOBID_MFGSMR     ),          cmpc( BOBID_IPSD_MANUFSMR ),                DRFOP_ASKII,        },
//  { cmpc( GLOBID_PIECECOUNTAXCI ),      cmpc(  ),
  { cmpc( GLOBID_MODELNUM   ),          cmpc( BOBID_IPSD_MODELNUMBER ),             DRFOP_ASKII,        },
  { cmpc( GLOBID_REFILLCOUNT ),         cmpc( BOBID_IPSD_REFILLCOUNT ),             DRFOP_LE_INT,       },
/// Need to change this when changed in Orbit tool...
  { cmpc( GLOBID_POSTAGEVAL  ),         cmpc( VAULT_DEBIT_VALUE ),                  DRFOP_BE_MON_3DEC,  },
  { cmpc( GLOBID_MFGID       ),         cmpc( BOBID_IPSD_MANUFACTURERID ),          DRFOP_ASKII,        },
  { cmpc( GLOBID_INDSN       ),         cmpc( BOBID_IPSD_INDICIASN ),               DRFOP_ASKII,        },
  { cmpc( GLOBID_INDVERSION  ),         cmpc( BOBID_IPSD_INDICIAVERNUM  ),          DRFOP_BE_INT,       },
  { cmpc( GLOBID_HOSTSWID    ),         cmpc( BOBID_IPSD_HOSTSW_ID ),               DRFOP_BE_BITE_ARY,  },
//  { cmpc( GLOBID_PAD         ),         cmpc(  ) ,
  { cmpc( GLOBID_RATECLASS   ),         cmpc( RATE_STRING ),                        DRFOP_ASKII,        },
  { cmpc( GLOBID_ALGORITHMID ),         cmpc( BOBID_IPSD_ALGORITHID ),              DRFOP_BE_INT,       },
  { cmpc( GLOBID_PBISN       ),         cmpc( BOBID_IPSD_PBISERIALNUM ),            DRFOP_ASKII,        },
  { cmpc( GLOBID_CERTSN      ),         cmpc( BOBID_IPSD_CERTIFICATSN ),            DRFOP_LE_INT,       },
  { cmpc( GLOBID_ASCENDINGREG   ),      cmpc( BOBID_IPSD_ASCREG ),                  DRFOP_BE_MON_3DEC,  },
  { cmpc( GLOBID_DESCENDINGREG  ),      cmpc( BOBID_IPSD_DESCREG ),                 DRFOP_BE_MON_3DEC,  },
//  { cmpc( GLOBID_MAC            ),      cmpc(  ),
  { cmpc( GLOBID_DATEOFMAILING  ),      cmpc( BOBID_IPSD_MAILINGDATE ),             DRFOP_DATETIME_IBUT,},
  { cmpc( GLOBID_ZIPCODE        ),      cmpc( BOBID_IPSD_ZIPCODE ),                 DRFOP_ASKII,        },
  { cmpc( GLOBID_ASCREGPRESET   ),      cmpc( BOBID_IPSD_ASCREGPRESET ),            DRFOP_BE_MON_3DEC,  },
  { cmpc( GLOBID_BACKDATELMT    ),      cmpc( BOBID_IPSD_BACKDATELIMIT ),           DRFOP_LE_INT,       },
  { cmpc( GLOBID_CLRARWPVRFLAG  ),      cmpc( BOBID_IPSD_CLEARARWITHPVR ),          DRFOP_BE_INT,       },
  { cmpc( GLOBID_CLOCKOFFSET    ),      cmpc( BOBID_IPSD_CLOCKOFFSET ),             DRFOP_LE_INT,       },
  { cmpc( GLOBID_CURRENCYCODE   ),      cmpc( BOBID_IPSD_CURRENCYCODE ),            DRFOP_BE_INT,       },
  { cmpc( GLOBID_DATEADVLMT     ),      cmpc( BOBID_IPSD_DATEADVANCELIMIT ),        DRFOP_LE_INT,       },
  { cmpc( GLOBID_GMTOFFSET      ),      cmpc( BOBID_IPSD_GMTOFFSET ),               DRFOP_LE_INT,       },
  { cmpc( GLOBID_INDTYPE        ),      cmpc( BOBID_IPSD_INDICIATYPE ),             DRFOP_BE_INT,       },
  { cmpc( GLOBID_KPADREFILLINVINSP ),   cmpc( BOBID_IPSD_KEYPADREFILLINVOKESINSPECT ), DRFOP_BE_INT,    },
  { cmpc( GLOBID_FRENCHMACSIZE     ),   cmpc( BOBID_IPSD_FRENCHMAC_SZ ),            DRFOP_BE_INT,       },
  { cmpc( GLOBID_MAXASCREG         ),   cmpc( BOBID_IPSD_MAXASCREG ),               DRFOP_BE_MON_3DEC,  },
  { cmpc( GLOBID_MAXDESCREG        ),   cmpc( BOBID_IPSD_MAXDESCREG ),              DRFOP_BE_MON_3DEC,  },
  { cmpc( GLOBID_MSGTYPE           ),   cmpc( BOBID_IPSD_MSGTYPE ),                 DRFOP_BE_INT,       },
  { cmpc( GLOBID_FRENCHOCRSIZE     ),   cmpc( BOBID_IPSD_FRENCHOCR_SZ ),            DRFOP_BE_INT,       },
  { cmpc( GLOBID_ORIGINCOUNTRY     ),   cmpc( BOBID_IPSD_ORIGINCOUNTRY ),           DRFOP_BE_BITE_ARY,  },
  { cmpc( GLOBID_PCN               ),   cmpc( BOBID_IPSD_PCN ),                     DRFOP_ASKII,        },
  { cmpc( GLOBID_POSTAGEMIN        ),   cmpc( BOBID_IPSD_POSTAGEMIN ),              DRFOP_BE_MON_3DEC,  },
  { cmpc( GLOBID_REFILLTYPE        ),   cmpc( BOBID_IPSD_REFILLTYPE ),              DRFOP_BE_INT,       },
//  { cmpc( GLOBID_SIGBYPSDKEY       ),   cmpc(  ),
  { cmpc( GLOBID_WDTIMER           ),   cmpc( BOBID_IPSD_WATCHDOGTMR ),             DRFOP_DATETIME_IBUT,},
  { cmpc( GLOBID_WDTIMERRESET      ),   cmpc( BOBID_IPSD_WATCHDOGTMRRESET ),        DRFOP_BE_INT,       },
  { cmpc( GLOBID_ITALYCURCODE      ),   cmpc( BOBID_IPSD_ITALYCURCODE ),            DRFOP_BE_INT,       },  
  { cmpc( GLOBID_INDICIUMCODE      ),   cmpc( BOBID_IPSD_INDICIUMCODE ),            DRFOP_BE_INT,       },
  { cmpc( GLOBID_ALGRTHMOBJID      ),   cmpc( BOBID_IPSD_ALGRTHMOBJID ),            DRFOP_BE_INT,       },
  { cmpc( GLOBID_PCCNTATREFILL     ),   cmpc( BOBID_IPSD_PCCNTATREFILL ),           DRFOP_BE_INT,       },
  { cmpc( GLOBID_MTRKEYEXPDATE     ),   cmpc( BOBID_IPSD_MTRKEYEXPDATE ),           DRFOP_BE_INT,       },
  { cmpc( GLOBID_ENBLDSBLSTAT      ),   cmpc( BOBID_IPSD_ENBLDSBLSTAT ),            DRFOP_BE_INT,       },

  { cmpc( GLOBID_GERMANARATLASTREF ),   cmpc( BOBID_IPSD_GERMAN_ARATLASTREF ),      DRFOP_BE_MON_3DEC,  },
  { cmpc( GLOBID_GERMANAUTHKEYREV  ),   cmpc( BOBID_IPSD_GERMAN_AUTHKEYREV ),       DRFOP_BE_INT,       },
  { cmpc( GLOBID_GERMANCRYPTOSTR   ),   cmpc( BOBID_IPSD_GERMAN_CRYPTOSTR ),        DRFOP_BE_BITE_ARY,  },
//{ cmpc( GLOBID_GERMANCURRENCY    ),   cmpc( BOBID_IPSD_GERMAN_CURRENCY
//{ cmpc( GLOBID_GERMANKEYPHASE    ),   cmpc( BOBID_IPSD_GERMAN_KEYPHASE
//{ cmpc( GLOBID_GERMANPOSTCOMPANY ),   cmpc( BOBID_IPSD_GERMAN_POSTCOMPANY
  { cmpc( GLOBID_GERMANPOSTIDDATA  ),   cmpc( BOBID_IPSD_GERMAN_POSTIDDATA ),       DRFOP_BE_BITE_ARY,  },
  { cmpc( GLOBID_GERMANPRODUCTKEY  ),   cmpc( BOBID_IPSD_GERMAN_PRODUCTKEY ),       DRFOP_BE_INT,       },
  { cmpc( GLOBID_GERMANSECONDDATE  ),   cmpc( BOBID_IPSD_GERMAN_SECONDDATE ),       DRFOP_BE_BITE_ARY,  },
//{ cmpc( GLOBID_GERMANTRUNKHASH   ),   cmpc( BOBID_IPSD_GERMAN_TRUNKHASH),         DRFOP_??            },
//{ cmpc( GLOBID_GERMANFRANKTYPE   ),   cmpc( BOBID_IPSD_GERMAN_FRANKTYPE),         DRFOP_??            },
  { cmpc( GLOBID_GERMANVARPOSTALDATA ), cmpc( BOBID_IPSD_GERMAN_VARPOSTALDATA ),    DRFOP_BE_BITE_ARY,  },
//{ cmpc( GLOBID_GERMANPRODVERSION ),   cmpc( BOBID_IPSD_GERMAN_PRODVERSION),       DRFOP_??            },
//{ cmpc( GLOBID_GARBAGE_PAD       ),   cmpc( BOBID_IPSD_GARBAGE_PAD),              DRFOP_??            },
//{ cmpc( GLOBID_CANADAESDSASIG    ),   cmpc( BOBID_IPSD_CANADA_ESDSASIG),          DRFOP_??            },
//{ cmpc( GLOBID_CANADAPUBKEYDATA  ),   cmpc( BOBID_IPSD_CANADA_PUBKEYDATA),        DRFOP_??            },
  { cmpc( GLOBID_CANADAPROVKEYREV  ),   cmpc( BOBID_IPSD_CANADA_PROVKEYREV),        DRFOP_BE_INT,       },
  { cmpc( GLOBID_GERMANSERVICEID   ),   cmpc( BOBID_IPSD_GERMAN_SERVICEID ),        DRFOP_BE_BITE_ARY,  },
  { cmpc( GLOBID_GERMANPIECECOUNT  ),   cmpc( BOBID_IPSD_GERMAN_PIECECOUNT ),       DRFOP_LE_INT,       },
  { cmpc( GLOBID_ADDDRTOARONPVR    ),   cmpc( BOBID_IPSD_ADDDRTOARONPVR),           DRFOP_BE_INT,       },
  { cmpc( GLOBID_FRENCHALPHANUMPC  ),   cmpc( BOBID_IPSD_FRENCH_ALPHANUMPIECECOUNT),DRFOP_BE_INT,       }, 
  { cmpc( GLOBID_RESETPCONPVR      ),   cmpc( BOBID_IPSD_RESETPCONPVR),             DRFOP_BE_INT,       },
  { cmpc( GLOBID_ALLOWZEROPPOSTAGE ),   cmpc( BOBID_IPSD_ALLOWZEROPPOSTAGE),        DRFOP_BE_INT,       },
  { cmpc( GLOBID_ZEROPOSTAGEPC     ),   cmpc( BOBID_IPSD_ZEROPOSTAGEPIECECOUNT),    DRFOP_LE_INT,       },
  { cmpc( GLOBID_BELGIANAPPVALUE   ),   cmpc( BOBID_IPSD_BELGIANAPPVALUE),          DRFOP_BE_INT,       },
  { cmpc( GLOBID_IBILITEVENDMODEL  ),   cmpc( BOBID_IPSD_IBILITEVENDORMODEL),       DRFOP_BE_HEX_1,     },
  { cmpc( GLOBID_IBILITEINDVER      ),  cmpc( BOBID_IPSD_IBILITEINDICIAVERSION),    DRFOP_BE_INT,       },
  { cmpc( GLOBID_CREATEINDICIA_MSGTYPE),    cmpc( BOBID_IPSD_CREATEINDICIA_MSGTYPE),DRFOP_BE_INT,       },
  { cmpc( GLOBID_NETSET_CLR),               cmpc( BOBID_IPSD_NETSET_CLR),           DRFOP_ASKII,        },
  { cmpc( GLOBID_NETSET_ISSUERCODE),        cmpc( BOBID_IPSD_NETSET_ISSUERCODE),    DRFOP_ASKII,        },
  { cmpc( GLOBID_ADVANCEDDATEOFFSET),       cmpc( BOBID_IPSD_ADVANCEDDATEOFFSETASCII),  DRFOP_ASKII,        },
  { cmpc( GLOBID_NETSET_PRODUCTCODE),       cmpc( BOBID_IPSD_NETSET_PRODUCTCODE),       DRFOP_ASKII,        },
  { cmpc( GLOBID_NETSET_SECURITYCODEVER),   cmpc( BOBID_IPSD_NETSET_SECURITYCODEVER),   DRFOP_ASKII,        },
  { cmpc( GLOBID_FLEXDEBIT_VALUEKEY_DATA),  cmpc( BOBID_IPSD_FLEXDEBIT_VALKEY_DATA),    DRFOP_BE_BITE_ARY,  },
  { cmpc( GLOBID_FLEXDEBIT_DATA_SZ),        cmpc( BOBID_IPSD_FLEXDEBIT_DATA_SZ),        DRFOP_BE_INT,       },
  { cmpc( GLOBID_FLEXDEBIT_DATA),           cmpc( BOBID_IPSD_FLEXDEBIT_DATAGLOB),       DRFOP_GEN_BITE_ARY, },
  { cmpc( GLOBID_BARCODEDATA_GLOB),         cmpc( BOBID_IPSD_INDICIADATAGLOB),          DRFOP_GEN_BITE_ARY, },
  { cmpc( GLOBID_FLEXDEBIT_2LSB_3DESMAC),   cmpc( BOBID_IPSD_FLEXDEBIT_2LSB_3DESMAC),   DRFOP_GEN_BITE_ARY, },
  { cmpc( GLOBID_HMACDESMAC_KEYID),         cmpc( BOBID_IPSD_HMACDESMAC_KEYID),         DRFOP_BE_BITE_ARY,  },
  { cmpc( GLOBID_PSD_AUTH_KEY_REV),         cmpc( BOBID_IPSD_PSDAUTH_KEYREV),           DRFOP_BE_BITE_ARY,  },
  { cmpc( GLOBID_IPSD_FLEXDEBIT_DESMAC),    cmpc( BOBID_IPSD_FLEXDEBIT_DESMAC),         DRFOP_BE_BITE_ARY,  },
// Comparing this with GLOBID_GERMANSERVICEID above, lets you know how a bob variable can 
//  be converted differently for different PCNs.
  { cmpc( GLOBID_NETSET_PSTVERSION   ),     cmpc( BOBID_IPSD_GERMAN_SERVICEID ),        DRFOP_ASKII,        },
  { cmpc( GLOBID_IPSD_FLEXDEBIT_HMAC),      cmpc( BOBID_IPSD_FLEXDEBIT_HMAC),           DRFOP_BE_BITE_ARY,  },
  { cmpc( GLOBID_IPSD_FLEXDEBIT_DSASIG),    cmpc( BOBID_IPSD_FLEXDEBIT_DSASIG),         DRFOP_BE_BITE_ARY,  },
};
ushort usGlob2BobArrayLen = ARRAY_LENGTH( pMapGlobVarToBobVar );

//lint +e778 +e572  

//----------------------------------------------------------------------------------------
// ------------------------------------------------------
//          CONVERSION FUNCTIONS:


//****************************************************************************
// FUNCTION NAME:   fnReverseIt
// Moved to utils.c file.
//-----------------------------------------------------------------------------

//****************************************************************************
// FUNCTION NAME:   fnCopyItLeftJust
//   DESCRIPTION:   Copies data from the Glob to a bob variable.
//                  If the sizes are not the same, the data is left- 
//                  justified.  
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   Pointer to tStoreCnvrtSomething struct that contains the  
//                  address and size of both the source and destination. 
//
//        RETURN:   TRUE if data was copied.
//
//         NOTES:  Examples:
//      Source[3]   = 0x01, 0x02, 0x03
//      Dest[5]     = 0x01, 0x02, 0x03, 0x00, 0x00
//      Dest[2]     = 0x01, 0x02
//-----------------------------------------------------------------------------
BOOL fnCopyItLeftJust( tStoreCnvrtSomething *pAddrSizes )
{
    BOOL    fRetval = FALSE;

    if( pAddrSizes->usDestVarSize > 0 )
    {
        memset( pAddrSizes->pDestVarLoc , 0, pAddrSizes->usDestVarSize );
        if( pAddrSizes->usDestVarSize >= pAddrSizes->usSrcVarSize )
        {
            memcpy( pAddrSizes->pDestVarLoc, pAddrSizes->pSrcVarLoc, pAddrSizes->usSrcVarSize );        
        }
        else
        {
            memcpy( pAddrSizes->pDestVarLoc, pAddrSizes->pSrcVarLoc, pAddrSizes->usDestVarSize  );
        }
        
        fRetval = TRUE;
    }

    return( fRetval );
}

//****************************************************************************
// FUNCTION NAME:   fnCopyItRightJust
//   DESCRIPTION:   Copies data from the Glob to a bob variable.
//                  If the sizes are not the same, the data is right- 
//                  justified.  
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   pAddrSizes - Pointer to tStoreCnvrtSomething struct that 
//                              contains the address and size of both the source 
//                              and destination. 
//
//        RETURN:   TRUE if data was copied.
//
//         NOTES:   Examples:
//      Source[3]   = 0x01, 0x02, 0x03
//      Dest[5]     = 0x00, 0x00, 0x01, 0x02, 0x03
//      Dest[2]     = 0x02, 0x03
//-----------------------------------------------------------------------------
BOOL fnCopyItRightJust( tStoreCnvrtSomething *pAddrSizes )
{
    ushort  usDiff;
    BOOL    pRetval = FALSE;

    if( pAddrSizes->usDestVarSize > 0 )
    {
        memset( pAddrSizes->pDestVarLoc , 0, pAddrSizes->usDestVarSize );
        if( pAddrSizes->usDestVarSize >= pAddrSizes->usSrcVarSize )
        {
            usDiff = pAddrSizes->usDestVarSize - pAddrSizes->usSrcVarSize;
            memcpy( pAddrSizes->pDestVarLoc + usDiff, pAddrSizes->pSrcVarLoc, pAddrSizes->usSrcVarSize );        
        }
        else
        {
            usDiff = pAddrSizes->usSrcVarSize - pAddrSizes->usDestVarSize;
            memcpy( pAddrSizes->pDestVarLoc, pAddrSizes->pSrcVarLoc + usDiff, pAddrSizes->usDestVarSize );
        }
    
        pRetval = TRUE;
    }

    return( pRetval );
}
 

//****************************************************************************
// FUNCTION NAME:   fnCopyLeftReverse,  fnCopyRightReverse
//   DESCRIPTION:   Copies data from a Source address to a dest address.
//                  If the sizes are not the same, the data is left/right 
//                  justified (depending upon the function used).
//                  Once the data is copied to the dest address, it is 
//                  reversed (Big Endian to Little Endian, or vice versa.)   
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   pAddrSizes - Pointer to tStoreCnvrtSomething struct that 
//                              contains the address and size of both the source 
//                              and destination. 
//
//        RETURN:   Returns TRUE if data was copied.
//
//         NOTES:   
//-----------------------------------------------------------------------------
BOOL fnCopyLeftReverse( tStoreCnvrtSomething *pAddrSizes )
{
    BOOL    pRetval;

    pRetval = fnCopyItLeftJust( pAddrSizes );
    if( pRetval )
        fnReverseIt( pAddrSizes->pDestVarLoc, pAddrSizes->usDestVarSize );

    return( pRetval );
}

BOOL fnCopyRightReverse( tStoreCnvrtSomething *pAddrSizes )
{
    BOOL    pRetval;

    pRetval = fnCopyItRightJust( pAddrSizes );
    if( pRetval )
        fnReverseIt( pAddrSizes->pDestVarLoc, pAddrSizes->usDestVarSize );
    
    return( pRetval );    
}
 

//****************************************************************************
// FUNCTION NAME:   fnBEHex2Ascii
//   DESCRIPTION:   Converts data from Big Endian Hex to ASCII.
//                  Always clears the destination, whether source was copied 
//                  or not.  
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   Pointer to tStoreCnvrtSomething struct that contains the  
//                  address and size of both the source and destination. 
//
//        RETURN:   TRUE if data was copied.
//
//         NOTES:  Examples:
//  Source                  Dest
//   Src[3] = 0x000103        Dest[3] = 31 30 33   ;Dest[5] = 31 30 33 00 00            
//   Src[5] = 0x0567801234    Dest[3] = 32 33 34   ;Dest[5] = 30 31 32 33 34
//   Src[3] = 0x010000        Dest[3] = 30 30 30   ;Dest[5] = 31 30 30 30 30   
//-----------------------------------------------------------------------------
BOOL fnBEHex2Ascii( tStoreCnvrtSomething *pAddrSizes )
{
    ushort  wSrcSize;
    ushort  wDestSize;
    uchar    *pDest;
    uchar    *pSrc;
    ushort  wNumLeadZeros;
    ushort  usDiff;
    uchar    tempBuff[ 3 ];
    ushort  tempInt;
    BOOL    firstChar = FALSE;
    ushort  i, len;
    BOOL    pRetval = FALSE;

    wSrcSize = pAddrSizes->usSrcVarSize;
    wDestSize = pAddrSizes->usDestVarSize;
    pDest   = pAddrSizes->pDestVarLoc;
    pSrc    = pAddrSizes->pSrcVarLoc;

    if( wDestSize & wSrcSize )
    {
        (void)memset( pDest, 0, wDestSize );

        // Find the number of leading zeros by looking for most significant byte in source.
        wNumLeadZeros = 0;
        for( i = 0; i < wSrcSize; i++ )
        {
            if( pSrc[ i ] )
            {
                // Found Most Significant Byte.
                wNumLeadZeros = i * 2;
                if( (pSrc[ i ] & 0xF0) == 0 )
                    wNumLeadZeros++;
                break;
            }
        }

        usDiff = i;

        // Check number of ascii chars needed to display this number.
        if( ((wSrcSize * 2) - wNumLeadZeros) > wDestSize )
        {
            // Destination is too small.  Chop off MSBytes.
            firstChar = TRUE;
            usDiff = (wSrcSize *2) - wDestSize;
            i = usDiff / 2; 
            pSrc += i;
            wSrcSize -= i;
            // If odd size for destination, use half of a byte.
            if( usDiff % 2 )
            {
                tempInt = ((ushort) *pSrc) & 0x0F;
                pSrc++;
                wSrcSize --;
                len = (UINT16)sprintf( (char *)tempBuff, "%.2X", tempInt );
                memcpy( pDest, tempBuff, len );
                pDest += len;                
                wDestSize -= len;
                pRetval = TRUE;
            }
        }
        else
        {
            // Plenty of room... Just skip leading zeros.
            wSrcSize -= usDiff;
            pSrc += usDiff;
        }
         
        // Convert one byte at a time byte 
        while( wSrcSize && wDestSize )
        {
            tempInt = (ushort) *pSrc;
            pSrc++;
            wSrcSize --;
            if( firstChar )
               len = (UINT16)sprintf( (char *)tempBuff, "%X", tempInt );
            else
                len = (UINT16)sprintf( (char *)tempBuff, "%.2X", tempInt );
            if( len )
            {
                memcpy( pDest, tempBuff, len );
                pDest += len;                
                wDestSize -= len;
                firstChar = TRUE;
                pRetval = TRUE;
            }
        }
    }

    return( pRetval );
}

//****************************************************************************
// FUNCTION NAME:   
//   DESCRIPTION:   Converts data from Little Endian Hex to ASCII.  
//                  Always clears the destination, whether source was copied 
//                  or not.  
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   Pointer to tStoreCnvrtSomething struct that contains the  
//                  address and size of both the source and destination. 
//
//        RETURN:   TRUE if data was copied.
//
//         NOTES:  Examples:
//  Source                  Dest
//   Src[3] = 0x000103        Dest[3] = 30 30 30   ;Dest[5] = 30 31 30 30 30            
//   Src[5] = 0x0567001234    Dest[3] = 36 35 30   ;Dest[5] = 30 37 36 35 30
//   Src[3] = 0x010000        Dest[3] = 31 30 00   ;Dest[5] = 31 30 00 00 00   
//-----------------------------------------------------------------------------
BOOL fnLEHex2Ascii( tStoreCnvrtSomething *pAddrSizes )
{
    ushort  wSrcSize;
    ushort  wDestSize;
    uchar   *pDest;
    uchar   *pSrc;
    ushort  wNumLeadZeros;
    ushort  usDiff;
    uchar   tempBuff[ 3 ];
    ushort  tempInt;
    BOOL    firstChar = FALSE;
    ushort  i, len;
    BOOL    fRetval = FALSE;

    wSrcSize = pAddrSizes->usSrcVarSize;
    wDestSize = pAddrSizes->usDestVarSize;
    pDest   = pAddrSizes->pDestVarLoc;
    pSrc    = pAddrSizes->pSrcVarLoc;

    if( wDestSize & wSrcSize )
    {
        (void)memset( pDest, 0, wDestSize );

        // Find the number of leading zeros by looking for most significant byte in source.
        wNumLeadZeros = 0;
        for( i = wSrcSize; i; i-- )
        {
            if( pSrc[ i -1 ] )
            {
                // Found Most Significant Byte.
                wNumLeadZeros = (wSrcSize - i) * 2;
                if( (pSrc[ i -1 ] & 0xF0) == 0 )
                    wNumLeadZeros++;
                break;
            }
        }

        // Adjustment for little Endian
        pSrc += (wSrcSize -1);
        usDiff = wNumLeadZeros / 2;

        // Check number of ascii chars needed to display this number.
        if( ((wSrcSize * 2) - wNumLeadZeros) > wDestSize )
        {
            // Destination is too small.  Chop off MSBytes.
            firstChar = TRUE;
            usDiff = (wSrcSize *2) - wDestSize;
            i = usDiff / 2; 
            pSrc -= i;
            wSrcSize -= i;
            // If odd size for destination, use half of a byte.
            if( usDiff % 2 )
            {
                tempInt = ((ushort) *pSrc) & 0x0F;
                pSrc--;
                wSrcSize --;
                len = (UINT16)sprintf( (char *)tempBuff, "%.2X", tempInt );
                 memcpy( pDest, tempBuff, len );
                 pDest += len;                
                 wDestSize -= len;
            }
        }
        else
        {
            // Plenty of room...
            wSrcSize -= usDiff;
            pSrc -= usDiff;
        }
         
        // Convert one byte at a time byte 
        while( wSrcSize && wDestSize )
        {
            tempInt = (ushort) *pSrc;
            pSrc--;
            wSrcSize --;
            if( !firstChar )
               len = (UINT16)sprintf( (char *)tempBuff, "%X", tempInt );
            else
                len = (UINT16)sprintf( (char *)tempBuff, "%.2X", tempInt );
            if( len )
            {
                memcpy( pDest, tempBuff, len );
                pDest += len;                
                wDestSize -= len;
                firstChar = TRUE;
            }
        }
        fRetval = TRUE;
    }
    return( fRetval );
}

//****************************************************************************
// FUNCTION NAME:   
//   DESCRIPTION:   Converts data from  to   
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   lSrc - ulong time in IPSD format.
//                  pDest - Pointer to 4-byte char array to put Time in format 1.
//
//        RETURN:   TRUE if data was copied.
//                  FALSE if no data copied (didn't work out to a valid time.)
//
//         NOTES:  Mail Date Format 1: (Little-Endian): decimal value of the form YYYYMMDD, 
//                  converted to binary. 
//                 Destination size must be 4 bytes.
//-----------------------------------------------------------------------------
BOOL fnIPSDtimeToTimeF1( ulong ulSrc, uchar *pDest )
{
    DATETIME    rSysTime;
    union {
            ulong   ulDecSum;    
            uchar   array[ SIZE_F1TIME ];
    }uF1time;

    // Convert IPSD time to rSysTime format.
    fnIPSDTimeToSys( &rSysTime, ulSrc );
    // If it's not a valid time, return FALSE and do not copy.
    if( MdyValid( &rSysTime ) == FALSE )
        return( FALSE );

    // Convert Month-Day-Year to decimal value YYYYMMDD
    uF1time.ulDecSum = rSysTime.bCentury;
    uF1time.ulDecSum *= 100;
    uF1time.ulDecSum += rSysTime.bYear;
    uF1time.ulDecSum *= 100;
    uF1time.ulDecSum += rSysTime.bMonth;
    uF1time.ulDecSum *= 100;
    uF1time.ulDecSum += rSysTime.bDay;
    
    // Array is now BE and must be converted to LE.
    fnReverseIt( uF1time.array, SIZE_F1TIME );
    memcpy( pDest, uF1time.array, SIZE_F1TIME );

    return( TRUE );    
}


//****************************************************************************
// FUNCTION NAME:   fnIPSDtime2TimeF1
//   DESCRIPTION:   Converts data from IPSD time format to Mail Date Format1.
//                  IPSD time is number of seconds since Jan.1. 2000.
//                  Mail Date Format 1 is decimal value of the form YYYYMMDD, 
//                  converted to binary, made Little Endian.
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   Pointer to tStoreCnvrtSomething struct that contains the  
//                  address and size of both the source and destination. 
//
//        RETURN:   TRUE if data was copied.
//
//         NOTES:  If destination size is < 4 bytes long, returns FALSE.
//                  If destination size is > 4 byts long, fills in first 4 bytes,
//                  and fills the rest with 0's.
//-----------------------------------------------------------------------------
BOOL fnIPSDtime2TimeF1( tStoreCnvrtSomething *pAddrSizes )
{
    union {
            ulong   ulIPSDtime;    
            uchar   array[ IPSD_SZ_TIME ];
    } uIPSDtime;
    
    BOOL    fRetval;
    ushort  usSize;

    // Make sure destination is big enough
    if( pAddrSizes->usDestVarSize < SIZE_F1TIME )
        return( FALSE );

    if( pAddrSizes->usDestVarSize > SIZE_F1TIME )
        memset( &(pAddrSizes->pDestVarLoc[SIZE_F1TIME]), 0, pAddrSizes->usDestVarSize - SIZE_F1TIME );
         
    // Copy IPSD time to local, account for Source being to small or large.
    uIPSDtime.ulIPSDtime = 0;
    usSize = pAddrSizes->usSrcVarSize;
    if( usSize <= IPSD_SZ_TIME )
    {
        memcpy( &(uIPSDtime.array[ IPSD_SZ_TIME - usSize ]), pAddrSizes->pSrcVarLoc, usSize );
    }
    else
    {
        memcpy( uIPSDtime.array, &(pAddrSizes->pSrcVarLoc[ usSize - IPSD_SZ_TIME ]), IPSD_SZ_TIME );
    }

    fRetval = fnIPSDtimeToTimeF1( uIPSDtime.ulIPSDtime, pAddrSizes->pDestVarLoc );

    return( fRetval );    
}

//****************************************************************************
// FUNCTION NAME:   
//   DESCRIPTION:   Converts data from  to   
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   ulSrc - ulong time in IPSD format.
//                  pDest - Pointer to 6-byte char array to put Time in format 1.
//
//        RETURN:   TRUE if data was copied.
//                  FALSE if no data copied (didn't work out to a valid time.)
//
//         NOTES:  Mail Date Format 2: ASCII DDMMYY
//                 Destination size must be 6 bytes.
//-----------------------------------------------------------------------------
BOOL fnIPSDtimeToTimeF2( ulong ulSrc, uchar *pDest )
{
    uchar       pTemp[ SIZE_F2TIME +1 ];
    DATETIME    rSysTime;

    // Convert IPSD time to rSysTime format.
    fnIPSDTimeToSys( &rSysTime, ulSrc );
    // If it's not a valid time, return FALSE and do not copy.
    if( MdyValid( &rSysTime ) == FALSE )
        return( FALSE );

    // Convert Month-Day-Year to ASCII DDMMYY
    sprintf( (char *)pTemp, "%.2d%.2d%.2d", rSysTime.bDay, rSysTime.bMonth, rSysTime.bYear ); 
            
    // Copy all but the trailing null terminator.    
    memcpy( pDest, pTemp, SIZE_F2TIME );

    return( TRUE );    
}

//****************************************************************************
// FUNCTION NAME:   fnIPSDtime2TimeF2
//   DESCRIPTION:   Converts data from IPSD time format to Mail Date Format2.
//                  IPSD time is number of seconds since Jan.1. 2000.
//                  Mail Date Format 2 is ASCII DDMMYY 
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   Pointer to tStoreCnvrtSomething struct that contains the  
//                  address and size of both the source and destination. 
//
//        RETURN:   TRUE if data was copied.
//
//         NOTES:  If destination size is < 6 bytes long, returns FALSE.
//                  If destination size is > 6 byts long, fills in first 6 bytes,
//                  and fills the rest with 0's.
//-----------------------------------------------------------------------------
BOOL fnIPSDtime2TimeF2( tStoreCnvrtSomething *pAddrSizes )
{
    union {
            ulong   ulIPSDtime;    
            uchar   array[ IPSD_SZ_TIME ];
    } uIPSDtime;
    
    BOOL    fRetval;
    ushort  usSize; 


    // Make sure destination is big enough
    if( pAddrSizes->usDestVarSize < SIZE_F2TIME )
        return( FALSE );

    if( pAddrSizes->usDestVarSize > SIZE_F2TIME )
        memset( &(pAddrSizes->pDestVarLoc[SIZE_F2TIME]), 0, pAddrSizes->usDestVarSize - SIZE_F2TIME );
         
    // account for Source being to small or large, get a copy of the source.
    uIPSDtime.ulIPSDtime = 0;
    usSize = pAddrSizes->usSrcVarSize;
    if( usSize <= IPSD_SZ_TIME )
    {
        memcpy( &(uIPSDtime.array[ IPSD_SZ_TIME - usSize]), pAddrSizes->pSrcVarLoc, usSize );
    }
    else
    {
        memcpy( uIPSDtime.array, &(pAddrSizes->pSrcVarLoc[ usSize - IPSD_SZ_TIME ]), IPSD_SZ_TIME );
    }

    fRetval = fnIPSDtimeToTimeF2( uIPSDtime.ulIPSDtime, pAddrSizes->pDestVarLoc );

    return( fRetval );    

}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//          CONVERSION FUNCTION TABLES:
//          
typedef struct
{
    uchar    ucDestVarType;
    void    *pFunc;
} tConversionFuncTblEntry;


const tConversionFuncTblEntry    aConvFuncTbl_be_binint[] =       //  Source type = DRFOP_BE_INT,
{ //   Destination Variable Type   function      
 {      DRFOP_BE_INT,               fnCopyItRightJust,   },     
 {      DRFOP_LE_INT,               fnCopyRightReverse,  },  
};

const tConversionFuncTblEntry    aConvFuncTbl_le_binint[] =       //  Source type = DRFOP_LE_INT,
{//    Destination Variable Type   function      
 {      DRFOP_LE_INT,               fnCopyItLeftJust,    },
 {      DRFOP_BE_INT,               fnCopyLeftReverse,   },
};

const tConversionFuncTblEntry    aConvFuncTbl_be_money3d[] =      //  Source type = DRFOP_BE_MON_3DEC,     
{//    Destination Variable Type   function      
 {      DRFOP_BE_MON_3DEC,          fnCopyItRightJust,   },
 {      DRFOP_LE_MON_3DEC,          fnCopyRightReverse,  },
};

const tConversionFuncTblEntry    aConvFuncTbl_le_money3d[] =      //  Source type = DRFOP_LE_MON_3DEC
{//    Destination Variable Type   function      
 {      DRFOP_LE_MON_3DEC,          fnCopyItLeftJust,     },
 {      DRFOP_BE_MON_3DEC,          fnCopyLeftReverse,    },
};

const tConversionFuncTblEntry    aConvFuncTbl_ascii[] =           //  Source type = DRFOP_ASKII
{//    Destination Variable Type   function      
 {      DRFOP_ASKII,                fnCopyItLeftJust,      },
};

const tConversionFuncTblEntry    aConvFuncTbl_le_bytearray[] =    //  Source type =  DRFOP_LE_BITE_ARY
{//    Destination Variable Type   function      
 {      DRFOP_LE_BITE_ARY,          fnCopyItLeftJust,     },
 {      DRFOP_BE_BITE_ARY,          fnCopyLeftReverse,    },
};

const tConversionFuncTblEntry    aConvFuncTbl_be_bytearray[] =    //  Source type = DRFOP_BE_BITE_ARY
{//    Destination Variable Type   function      
 {      DRFOP_BE_BITE_ARY,          fnCopyItRightJust,    },
 {      DRFOP_LE_BITE_ARY,          fnCopyRightReverse,   },
};

// Maildate format 1 is a Little Endian binary number which, when converted to decimal, has the numeric format YYYYMMDD
const tConversionFuncTblEntry    aConvFuncTbl_Maildate_frmt1[] =  //  Source type = DRFOP_MAILDATE_FMT1 
{//    Destination Variable Type   function      
 {      DRFOP_MAILDATE_FMT1,        fnCopyItLeftJust,      },
//        DRFOP_DATETIME_IBUT,  fnDateTimeConvF1ToIPSD ,
//        DRFOP_MAILDATE_FMT2,   fnDateTimeConvF1ToF2 ,
};

const tConversionFuncTblEntry    aConvFuncTbl_Maildate_frmt2[] =  //  Source type = DRFOP_MAILDATE_FMT2
{//    Destination Variable Type   function      
 {      DRFOP_MAILDATE_FMT2,        fnCopyItLeftJust,     },
//        DRFOP_DATETIME_IBUT,  fnDateTimeConvF2ToIPSD,      
//        DRFOP_MAILDATE_FMT1,   fnDateTimeConvF2ToF1,      
};

const tConversionFuncTblEntry    aConvFuncTbl_Maildate_IPSD[] =   //  Source type = DRFOP_DATETIME_IBUT
{//    Destination Variable Type   function      
 {      DRFOP_DATETIME_IBUT,        fnCopyItRightJust,    },
 {      DRFOP_MAILDATE_FMT1,        fnIPSDtime2TimeF1,    },      
 {      DRFOP_MAILDATE_FMT2,        fnIPSDtime2TimeF2,    }, 
};


const tConversionFuncTblEntry    aConvFuncTbl_DSA_sig[] =         //  Source type = DRFOP_DSA_SIG_IBUT
{//    Destination Variable Type   function      
 {      DRFOP_DSA_SIG_IBUT,         fnCopyItLeftJust,     },
};

const tConversionFuncTblEntry    aConvFuncTbl_bytearray[] =       //  Source type = DRFOP_GEN_BITE_ARY
{//    Destination Variable Type   function      
 {      DRFOP_GEN_BITE_ARY,         fnCopyItLeftJust,      },
};




typedef struct 
{
    uchar   ucSrcType;
    uchar   ucTblSize;
    const tConversionFuncTblEntry *pTable;
}tTypeToTblXref;

const tTypeToTblXref  aGlobConvFuncTableXref[] = 
{ // Source Type        Num entries in table     Pointer to conversion Table       
 {  DRFOP_BE_INT,                2,            aConvFuncTbl_be_binint,          }, 
 {  DRFOP_LE_INT,                2,            aConvFuncTbl_le_binint,          },
 {  DRFOP_BE_MON_3DEC,           2,            aConvFuncTbl_be_money3d,         },
 {  DRFOP_LE_MON_3DEC,           2,            aConvFuncTbl_le_money3d,         },
 {  DRFOP_ASKII,                 1,            aConvFuncTbl_ascii,              },
 {  DRFOP_LE_BITE_ARY,           2,            aConvFuncTbl_le_bytearray,       },
 {  DRFOP_MAILDATE_FMT1,         1,            aConvFuncTbl_Maildate_frmt1,     },
 {  DRFOP_MAILDATE_FMT2,         1,            aConvFuncTbl_Maildate_frmt2,     },
 {  DRFOP_DSA_SIG_IBUT,          1,            aConvFuncTbl_DSA_sig,            },
 {  DRFOP_DATETIME_IBUT,         3,            aConvFuncTbl_Maildate_IPSD,      },
 {  DRFOP_BE_BITE_ARY,           2,            aConvFuncTbl_be_bytearray,       },
 {  DRFOP_GEN_BITE_ARY,          1,            aConvFuncTbl_bytearray,       },
};


//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
// ----------------------------------------------------
//****************************************************************************
// FUNCTION NAME:       fnG2BClearBadParamList
// DESCRIPTION:  
//      Clears the list of GlobIds not supported in this IPSDs PSD Parameter
//      list glob, and sets the number of GlobIDs in the list to 0.
//
// INPUTS:      None
// ARGUMENTS:   None
// RETURNS:     None.
// OUTPUTS:         
//     pG2BListBadPsdParams[] is filled with 0's.
//      ubG2BNumBadPsdParams is set to 0.

// NOTES:   None
//       
// MODIFICATION:    
//  2008.09.04  Clarisa Bellamy - Initial.
//-----------------------------------------------------------------------------
void fnG2BClearBadParamList( void )
{
    if( ubG2BNumBadPsdParams > 0 )
    {
        memset( pG2BListBadPsdParams, 0, sizeof( pG2BListBadPsdParams ) );
    }
    ubG2BNumBadPsdParams = 0;
    return;    
}


//****************************************************************************
// FUNCTION NAME:       fnG2BReportG2BError
// DESCRIPTION:  
//      While distributing glob data to bob data, the Definition Record called 
//  out a field with an offset beyond the length of the incoming glob.
//      If this is a PSDParameterList glob, then it could be because the IPSD
//  is an older version, and this should not be reported every time it happens,
//  because it will happen over and over. So just put the GlobId in a list.
//      If this is not a PSDParameterList glob, then report the error in the 
//  system log.
//
// INPUTS:      
//      fGettingPsdParamList -  flag indicating if we are distributing a 
//                      PSD ParameterList right now, or not.  
// ARGUMENTS:   
//      pStoreStruct - Pointer to structure containing info about the glob data 
//                     and bob data.
// RETURNS:     None.
// OUTPUTS:         
//     Either puts an error into the system log OR puts a GlobID into 
//      pG2BListBadPsdParams.
//
// NOTES:   Uses pBobsLogScratchPad
//       
// MODIFICATION:    
//  2008.09.04  Clarisa Bellamy - Initial.
//-----------------------------------------------------------------------------
void fnG2BReportG2BError( tStoreGlob *pStoreStruct ) 
{
    // If we are getting the PSD parameter list, this may be an error that 
    //  will be repeated OVER and OVER and OVER.  
    if( fG2BGettingPsdParamList == TRUE )
    {
        // Instead of reporting it, save the error in a list.
        //  The list will be checked later to see if anything needs to be reported.
        fnG2BSavePsdParamListError( pStoreStruct );
    }
    else
    {
        // Otherwise, just report the problem.
        sprintf( pBobsLogScratchPad, "Glob2Bob access beyond glob: GlobID= %d", (int)pStoreStruct->usGlobVarID );
        fnDumpStringToSystemLog( pBobsLogScratchPad );
        fnLogError( ERR_TYPE_NORMAL, ERROR_CLASS_JBOB_TASK, BOB_GLOB_OVERRUN_G2B );
    }
    return;
}

//****************************************************************************
// FUNCTION NAME:       fnG2BReportB2GError
// DESCRIPTION:  
//      While creating a glob, the Definition Record called out a field with an
//  offset beyond the length of the glob buffer.  Report an error.
//
// INPUTS:          None.
// ARGUMENTS:   
//      pStoreStruct - Pointer to structure containing info about the glob data 
//                     and bob data.
// RETURNS:     None.
// OUTPUTS:         
//     Puts an error into the system log.
//
// NOTES:   Uses pBobsLogScratchPad
//       
// MODIFICATION:    
//  2008.09.04  Clarisa Bellamy - Initial.
//-----------------------------------------------------------------------------
void fnG2BReportB2GError( tStoreGlob *pStoreStruct ) 
{
    sprintf( pBobsLogScratchPad, "Bob2Glob access beyond glob: GlobID= %d", (int)pStoreStruct->usGlobVarID );
    fnDumpStringToSystemLog( pBobsLogScratchPad );
    fnLogError( ERR_TYPE_NORMAL, ERROR_CLASS_JBOB_TASK, BOB_GLOB_OVERRUN_B2G );
    return;
}


//****************************************************************************
// FUNCTION NAME:       fnG2BSavePsdParamListError
// DESCRIPTION:  
//      While distributing PSDParameterList glob data to bob data, the Definition 
//  Record called out a field with an offset beyond the length of the incoming glob.
//  Just put the GlobId in a list.  When the entire list is distributed, another
//  function will check to see if we want to report it.
//
// INPUTS:      
//      ubG2BNumBadPsdParams -  Number of GlobIds already in the list.
//      uwG2BIgnorePSDParmErrCount - If zero, don't ignore this error.
// ARGUMENTS:   
//      pStoreStruct - Pointer to structure containing info about the glob data 
//                     and bob data.
// RETURNS:     None.
// OUTPUTS:         
//     If there is room, puts a GlobID into pG2BListBadPsdParams and updates 
//      ubG2BNumBadPsdParams. 
//
// NOTES:   Won't store the GlobID if it is zero.
//       
// MODIFICATION:    
//  2008.09.04  Clarisa Bellamy - Initial.
//-----------------------------------------------------------------------------
void fnG2BSavePsdParamListError( tStoreGlob *pStoreStruct ) 
{
    UINT16  uwGlobVarID;

    if( uwG2BIgnorePSDParmErrCount == 0 )
    {
        uwGlobVarID = pStoreStruct->usGlobVarID;
        // Don't report error if GlobVarID = 0.  
        //  Just a glich we can live with which prevents reporting some bogus errors.
        if( uwGlobVarID )
        {
            pG2BListBadPsdParams[ ubG2BNumBadPsdParams ] = uwGlobVarID;
            ubG2BNumBadPsdParams++;
            if( ubG2BNumBadPsdParams >= MAX_GLOBIDS_TO_REPORT )
            {
                ubG2BNumBadPsdParams = MAX_GLOBIDS_TO_REPORT - 1;
            }
        }
    }
    return;
}


//****************************************************************************
// FUNCTION NAME:       fnG2BReportPSDParamListErrors
// DESCRIPTION:  
//      Checks to see if there are any GlobIDs in the pG2BListBadPsdParams, and 
//      if so, sends a list of them, and the PSD Version, to the System Log.
//      This function increments the value uwG2BIgnorePSDParmErrCount, and 
//      clears it if it has reached bUnreportedErrorInterval, 
//
// INPUTS:      
//      ubG2BNumBadPsdParams -  Number of GlobIds in the bad GlobID list.
//      pG2BListBadPsdParams - List of bad GlobIds.
//      uwG2BIgnorePSDParmErrCount - Count of instances of overrun replys ignored.
//      bUnreportedErrorInterval - Instances of overrun replys TO ignore.
// ARGUMENTS:   
//      None.
// RETURNS:     None.
// OUTPUTS:         
//     If there are GlobIDs that have not been reported, put an error message in the
//     system log listing those GlobIDs, and update ubG2BNumReportedBadPsdParams and 
//     pG2BListReportedBadPsdParams. 
//
// NOTES:   None.
//       
// MODIFICATION:    
//  2008.09.04  Clarisa Bellamy - Initial.
//-----------------------------------------------------------------------------
void fnG2BReportPSDParamListErrors( void ) 
{
    UINT8   ubIdx;
    UINT8   ubLoop;
    int     msgLen;

    // If we had any bad GlobIds when trying to distribute the PSD Parameter list...
    if( ubG2BNumBadPsdParams > 0 )
    {
        // Print the header line and the PSD Version, then the list of bad GlobIDs.
        msgLen = sprintf( pBobsLogScratchPad, 
                          "Glob2Bob access beyond PSDParamList glob" );
        fnDumpStringToSystemLog( pBobsLogScratchPad );
        msgLen = sprintf( pBobsLogScratchPad, 
                          "PsdVer. = %s.   Missing GlobIDs = ", 
                          pIPSD_FirmwareVerSubStr );
        ubIdx = 0;
        while( ubIdx < ubG2BNumBadPsdParams )
        {
            fnDumpStringToSystemLog( pBobsLogScratchPad );
            // As long as we have more bad GlobIDs to list, start a new line...
            msgLen = sprintf( pBobsLogScratchPad, "    %.4d", pG2BListBadPsdParams[ ubIdx++ ] );
            // Loop to add more IDs to the current line.
            for( ubLoop = 0;  
                    (ubIdx < ubG2BNumBadPsdParams) 
                 && (ubLoop < MAX_GLOBIDS_TO_REPORT_EACHLINE) ; 
                        ubLoop++ )
            {
                msgLen += sprintf( pBobsLogScratchPad + msgLen, ", %.4d", pG2BListBadPsdParams[ ubIdx++ ] );
            }
        }
        // Indicate it is the last number in the list.
        msgLen += sprintf( pBobsLogScratchPad + msgLen, ".", pG2BListBadPsdParams[ ubIdx++ ] );
        fnDumpStringToSystemLog( pBobsLogScratchPad );
        msgLen = sprintf( pBobsLogScratchPad, "This message suspended for %d counts.", bUnreportedErrorInterval );
        fnDumpStringToSystemLog( pBobsLogScratchPad );
    }

    // Increment our counter for ignoring any PSD Param List glob2bob overrun errors.
    if( ++uwG2BIgnorePSDParmErrCount > bUnreportedErrorInterval )
    {
        uwG2BIgnorePSDParmErrCount = 0;
    }
    return;
}

//****************************************************************************
// FUNCTION NAME:   fnGetDefRecord
// DESCRIPTION:   
//      Read 1 entry from Glob definition record, and populate the StoreStruct 
//      with the Glob Variable ID, Location, Size, and type.
//
// ARGUMENTS:   
//      pStoreStruct    -Pointer to tStoreGlob struct where data should be stored.
//      pDefRecord      - Pointer to entry in definition record to read data from.
//      pGlob           - Pointer to beginning of Glob
//      wMaxGlobSize    - If we converting Bob2Glob, it is the size of the glob
//                        destination buffer.  If we are converting Glob2Bob, 
//                        it is the size of the glob actually received.
//
// RETURN:   
//      TRUE if record member offset is within glob range.
// NOTES: 
//
// Modification: 
//  2008.09.04 Clarisa Bellamy - Call new subroutines to deal with the error 
//      reporting.
//  2008.04.09 Clarisa Bellamy - Needed to eliminate the use of wIPSD_rxGlobSize
//      as this variable is temporary and used for a variety of sizes.  Added a
//      new argument, wMaxGlobSize.  If offset is more than wMaxGlobSize, then 
//      log an error because something is wrong or something is not big enough.
//  AUTHOR:   Clarisa Bellamy
//-----------------------------------------------------------------------------
BOOL fnGetDefRecordInfo( tStoreGlob *pStoreStruct, const tIPSDdefRecord *pDefRecord, 
                         uchar *pGlob, ushort wMaxGlobSize )
{
    BOOL    fRetVal = TRUE;
    ushort  usOffset;

    pStoreStruct->usGlobVarID = pDefRecord->usFieldIdentifer;
    pStoreStruct->ucGlobVarType = pDefRecord->ucFieldType;
    usOffset = pDefRecord->usFieldOffset;

    if( pStoreStruct->fGlob2bob == GLOB2BOB )
    {
        pStoreStruct->pAddrsAndSizes->usSrcVarSize = pDefRecord->usFieldLength;

        if( usOffset < wMaxGlobSize )
        {
            pStoreStruct->pAddrsAndSizes->pSrcVarLoc = pGlob + usOffset;    
        }
        else
        {
            fRetVal = FALSE;    // this member's offset is out of range
            fnG2BReportG2BError( pStoreStruct );
        }
    }
    else
    {
        pStoreStruct->pAddrsAndSizes->usDestVarSize = pDefRecord->usFieldLength;
        if( usOffset < wMaxGlobSize )
        {
            pStoreStruct->pAddrsAndSizes->pDestVarLoc = pGlob + usOffset;
        }
        else
        {
            fRetVal = FALSE;    // this member's offset is out of range !
            fnG2BReportB2GError( pStoreStruct );
        }
    }

    if( fRetVal )
    {
        pStoreStruct->usMaybesize = usOffset + pDefRecord->usFieldLength;  
    }

    return( fRetVal );    
}

//****************************************************************************
// FUNCTION NAME:   fnGlobToBobToGlobInfo
//   DESCRIPTION:   Search for the Glob Definition Record Field ID in the 
//                  Glob-to-BobID table.  If found, get the bob-variable ID 
//                  and type from the table and the get the bob-variable 
//                  size and location from the bobaVarAddresses table.
//                  Store all this information in the Store structure.
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   pStoreStruct - Pointer to tStoreGlob struct which should 
//                              have the Glob variable ID populated.
//
//        RETURN:   TRUE if the Glob Variable ID was in the table.
//                  FALSE if not found.
//
//         NOTES:    
//-----------------------------------------------------------------------------
BOOL fnGlobToBobToGlobInfo( tStoreGlob *pStoreStruct )
{
    ushort          i;
    ushort          arrayLength = ARRAY_LENGTH(pMapGlobVarToBobVar);
    ushort          usGlobFieldID;
    const tG2B2GIDmap   *pGlob2Var;             
    uchar           ucGlobIDhigh, ucGlobIDlow;
    BOOL            fFoundIt = FALSE;

    usGlobFieldID = pStoreStruct->usGlobVarID;
    ucGlobIDhigh = usGlobFieldID >>8;
    ucGlobIDlow  = usGlobFieldID &0xFF;

    // Search table for Glob Variable ID that matches.
    for( i = 0; i < arrayLength; i++ )
    {
        pGlob2Var = &pMapGlobVarToBobVar[ i ];
        
        if( (ucGlobIDhigh == pGlob2Var->pGlobVarID[ 0 ]) && (ucGlobIDlow == pGlob2Var->pGlobVarID[ 1 ]) )
        {
            fFoundIt = TRUE;
            break;            
        }
    }

    if( fFoundIt )
    {
        // Copy the Bob ID, bob Type, bob address and bob size into the Store structure.
        EndianAwareCopy( &(pStoreStruct->usBobVarID), &(pGlob2Var->pBobVarID[ 0 ]), sizeof( short ) );
        memcpy( &(pStoreStruct->ucBobVarType),  &(pGlob2Var->ucBobVarType), sizeof( uchar ) );
        if( pStoreStruct->fGlob2bob == GLOB2BOB )
        {
            pStoreStruct->pAddrsAndSizes->pDestVarLoc  = bobaVarAddresses[ pStoreStruct->usBobVarID ].addr;
            pStoreStruct->pAddrsAndSizes->usDestVarSize = bobaVarAddresses[ pStoreStruct->usBobVarID ].siz;    
        }
        else
        {
            pStoreStruct->pAddrsAndSizes->pSrcVarLoc  = bobaVarAddresses[ pStoreStruct->usBobVarID ].addr;
            pStoreStruct->pAddrsAndSizes->usSrcVarSize = bobaVarAddresses[ pStoreStruct->usBobVarID ].siz;    
        }
    }
    
    return( fFoundIt );
}

// ----------------------------------------------------
//****************************************************************************
// FUNCTION NAME:   fnGlobFindConvert
//   DESCRIPTION:   Find a conversion function.
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   pStoreStruct - Pointer to tStoreGlob struct which should 
//                              have the Glob AND Bob variable ID populated.
//
//        RETURN:   None.
//
//         NOTES:   If no conversion function found, Function pointer in 
//                  Store structure is set to 0.
//-----------------------------------------------------------------------------
void fnGlobFindConvertFunc( tStoreGlob *pStoreStruct )
{
    const tConversionFuncTblEntry * pTable = NULL_PTR;
    BOOL            fFoundIt = FALSE;
    UINT8           ucNumEntries = 0;
    UINT16          index;
    UINT8           ucSrcType;
    UINT8           ucDestType;

    pStoreStruct->pConvertFunc = (void *)0;

    // If the types are identical, the conversion is fairly straightforward...
    if( pStoreStruct->ucBobVarType == pStoreStruct->ucGlobVarType )
    {
        // If the type and size are the same, just copy.  Justification irrelevent unless diff sizes.
        if( pStoreStruct->pAddrsAndSizes->usDestVarSize == pStoreStruct->pAddrsAndSizes->usSrcVarSize )
        {
            pStoreStruct->pConvertFunc = fnCopyItLeftJust;
            return;
        }
    }

    if( pStoreStruct->fGlob2bob == GLOB2BOB )
    {
        ucSrcType = pStoreStruct->ucGlobVarType;
        ucDestType = pStoreStruct->ucBobVarType;   
    }
    else
    {
        ucDestType = pStoreStruct->ucGlobVarType;
        ucSrcType = pStoreStruct->ucBobVarType;   
    }

    // Different types and/or sizes:
    // Check table for Source Variable type, get it's table.
    for( index = 0; !fFoundIt && (index < ARRAY_LENGTH(aGlobConvFuncTableXref)); index++ )
    {
        if( aGlobConvFuncTableXref[ index ].ucSrcType ==  ucSrcType )
        {
            fFoundIt = TRUE;
            pTable = aGlobConvFuncTableXref[ index ].pTable;
            ucNumEntries = aGlobConvFuncTableXref[ index ].ucTblSize;
        }
    }

    // Check table specific to this Source Variable type for an entry with the matching 
    //  Destination Variable Type...
    if(   (fFoundIt == TRUE)
       && (pTable != NULL_PTR) )
    {
        fFoundIt = FALSE;
        // First byte is the number of entries in the table.
        for( index = 0; !fFoundIt && (index < ucNumEntries); index++ )
        {
            // Search for an entry with the Bob-variable type...
            if( pTable->ucDestVarType == ucDestType )
            {
                // If find the right type, store the conversion function and exit loop.
                fFoundIt = TRUE;
                pStoreStruct->pConvertFunc = pTable->pFunc;
            }
            else
                pTable++ ;
        }            
    }
}


//****************************************************************************
// FUNCTION NAME:   fnConvertFromGlobToBobToGlob
// DESCRIPTION:   
//      Uses a definition record entry to convert a field of data in the glob 
//     and store it properly in a bob variable, or vice versa.
//
// ARGUMENTS:   
//      pDefRecord  - Pointer to a single definition record structure.
//                  pGlob - Pointer to beginning of Glob.
//                  fGlob2Bob - Flag indicating whether we are converting a bob 
//                              variable to put into a glob, or converting a glob
//                              value to put into a bob variable.
//                  wMaxGlobSize - If we are converting Bob2Glob, it is the size of 
//                              the glob buffer.  If we are converting Glob2Bob,
//                              it is the size of the glob actually received.
//  RETURN:   
//      0 if nothing stored.   
//      Else return the offset from the beginning of the glob to the end 
//           of the data stored or read.
//  NOTES:   
//  1.  If there is no entry in the pMapGlobVarToBobVar table for the Glob ID,
//      then nothing can be stored.
//
// Modification History:
//  2008.04.09 Clarisa Bellamy - Needed to eliminate the use of wIPSD_rxGlobSize
//      as this variable is temporary and used for a variety of sizes.  Added a
//      new argument, wMaxGlobSize to pass fnGetDefRecordInfo.  
//  AUTHOR:   Clarisa Bellamy
//-----------------------------------------------------------------------------
ushort  fnConvertFromGlobToBobToGlob( tIPSDdefRecord *pDefRecord, uchar *pGlob, 
                                      BOOL fGlob2Bob, ushort wMaxGlobSize )
{
    BOOL                    fSuccess = FALSE;
    ushort                  usRetval = 0;
    tStoreCnvrtSomething    rLocsAndSizes;
    tStoreGlob              rStoreStruct;
                                     
    rStoreStruct.pAddrsAndSizes = &rLocsAndSizes;

    rStoreStruct.fGlob2bob = fGlob2Bob;

    // Get info on the glob data described by the single record pointed to by
    //  pDefRecord.  If the offset is beyond the size of the glob, then this 
    //  will return FALSE (0).  Else the info is stored in rStoreStruct.
    if( fnGetDefRecordInfo( &rStoreStruct, pDefRecord, pGlob, wMaxGlobSize ) == TRUE )
    {
        // Get info on the corresponding bob variable into the structure...
        fSuccess = fnGlobToBobToGlobInfo( &rStoreStruct );

        // If not in the table, don't store the value, just return.
        if( fSuccess == TRUE )
        {
            // Glob ID is in the table,
            // See if the types are compatible...
            fnGlobFindConvertFunc( &rStoreStruct );
            if( rStoreStruct.pConvertFunc )
            {
                // This converts and copies the data!!
                fSuccess = (rStoreStruct.pConvertFunc)( &rLocsAndSizes );
                if( fSuccess == TRUE )
                {
                    // If successful, return a non-zero value: the offset to the 
                    // end of the data just added (or read.)
                    usRetval = rStoreStruct.usMaybesize;

                    //See if there are special cases for Horizon to swap to little endian
                    switch(rStoreStruct.usGlobVarID)
                    {
                    //case 0x000A:  //TODO do we need to do refill count
                    //case 0x0F01:  //TODO do we need to do back date limit
                    case 0x0F03:  //clock offset
                    //case 0x0F05:  //TODO do we need to do date advance limit?
                    case 0x0F06:  //GMT Offset
                    case 0x0F47:  //FlexDebit size
                    //case 0x0F18:  //TODO do we need to do PieceCountAtRefill
						fnReverseIt(rStoreStruct.pAddrsAndSizes->pDestVarLoc, rStoreStruct.pAddrsAndSizes->usDestVarSize);
                    	break;
                    default:
                    	break;
                    }
                }
            }       
        }
    }       
    return( usRetval );
}

//****************************************************************************
// FUNCTION NAME:   fnGetDefRecord
//   DESCRIPTION:   Copies 1 definition record from an array of characters 
//                  to a structure.
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   pDefRecord - Pointer to definition record structure.
//                  pSrc - Pointer to array of chars where next definition
//                          record entry is located.
//
//        RETURN:   None.
//         NOTES:   None.
//-----------------------------------------------------------------------------
void fnGetDefRecord( tIPSDdefRecord *DestRecord, const uchar *pSrc )
{
    EndianAwareCopy( &(DestRecord->usFieldIdentifer),  pSrc, sizeof(DestRecord->usFieldIdentifer) );
    pSrc += sizeof(DestRecord->usFieldIdentifer);
    EndianAwareCopy( &(DestRecord->usFieldOffset),     pSrc, sizeof(DestRecord->usFieldOffset) );
    pSrc += sizeof(DestRecord->usFieldOffset);
    EndianAwareCopy( &(DestRecord->usFieldLength),     pSrc, sizeof(DestRecord->usFieldLength) );
    pSrc += sizeof(DestRecord->usFieldLength);
    EndianAwareCopy( &(DestRecord->ucFieldType),       pSrc, sizeof(DestRecord->ucFieldType) );
//    pSrc += sizeof(DestRecord->ucFieldType);
//    memcpy( &DestRecord->ucSpare,         pSrc, sizeof(DestRecord->ucSpare) );
//    pSrc += sizeof(DestRecord->ucSpare);
    return; 
}





//---------------------------------------------------------------------------------------------
//  These are the functions specific to the diferent Message/Definition-Records.


//**********************************************************************
// FUNCTION NAME:   fnDistribParamList  
// PURPOSE:   
//      Parses the Parameter List glob data using the data definition records 
//      in Flash, into the ipsd variables that can be accessed using the 
//      variable IDs and bobamat tables.
//        INPUTS:   None
//        RETURN:   BOB_OK
//         NOTES:
//
// HISTORY:
//  2008.09.04 Clarisa Bellamy - Call new subroutines to deal with the error 
//      reporting, which needs to be scaled back for this particular function.
// 2008.04.14  Clarisa Bellamy - Pass the size of the paramlist glob that was 
//             received to the function fnConvertFromGlobToBobToGlob, because
//             now it does correct range-checking.
// AUTHOR:   Clarisa Bellamy
//----------------------------------------------------------------------------
char fnIPSDDistribParamList( void ) 
{
    uchar           *pDefRekord;        // Traveling pointer to PSDParamList Definition as iPacked Byte Parameter
    ushort          usDefCount;         // number of entries in record
    ushort          usRecCnt;           // running count of definitions processed
    tIPSDdefRecord  rMyDefRecord;       // holds a single record from the definition
    ushort          usStored = 0;       //lint -e550 For debugging, non-zero if data from -this- record was stored.

    pDefRekord = fnFlashGetIBPackedByteParm( IPB_PSD_PARM_DEF_REC );

    // Get the number of entries in the record.
    EndianAwareCopy( &usDefCount, pDefRekord, sizeof(short) );
    pDefRekord += sizeof(short);

    // If there are entries in the record...
    if( usDefCount ) 
    {
        // Clear out the list of bad GlobIDs
        fnG2BClearBadParamList();
        // Let the generic subroutines know this is a PSDParameterList glob 
        //  that we are distributing.
        fG2BGettingPsdParamList = TRUE;
        // This is for debugging only:  wIPSD_paramListGlobSize -= bSubFromPSDParamListSize;   

        // for each entry in the iPacked Byte Parameter...
        for( usRecCnt = 0; usRecCnt < usDefCount; usRecCnt++ ) 
        {
            // Copy the entry into a local structure.
            fnGetDefRecord( &rMyDefRecord, pDefRekord );
            usStored = fnConvertFromGlobToBobToGlob( &rMyDefRecord, pIPSD_paramListGlob, 
                                                     GLOB2BOB, wIPSD_paramListGlobSize );
            pDefRekord +=  SIZE_IPSDDEFREC;
        }
     }
    // Done distributing PSD Parameter List Glob
    fG2BGettingPsdParamList = FALSE;
    // Check if there are any glob2bob errors that need to be reported.   
    fnG2BReportPSDParamListErrors();

    return( BOB_OK );
}


//****************************************************************************
// FUNCTION NAME:   fnIPSDLoadCreateIndiciaGlob
// DESCRIPTION:   
//      Uses the CreateIndicia definition record to load up the create-indicia-glob.  
//      All data used in glob (such as maildate) must be updated prior to entering 
//      this function by the calling routine.  
//      At this time, the appended data portion of the precreateIndicia message 
//      is identical to the appended data portion of the createIndicia message.
//
//     ARGUMENTS:   None.
//        RETURN:   Return BOB_OK always.
// OUTPUT:
//      If successful, the puts all the correct data into pIPSD_createIndDataGlob.
// NOTES: 
//
// HISTORY: 
//  2004.04.14  Clarisa Bellamy - Pass the size of the createIndicia glob buffer 
//              to the fnConvertFromGlobToBobToGlob function so that it won't 
//             overrun the buffer.
// AUTHOR: Clarisa Bellamy
//-----------------------------------------------------------------------------
char fnIPSDLoadCreateIndiciaGlob( void ) 
{
    uchar           *pDefRekord;        // Traveling pointer to PSDParamList Definition as iPacked Byte Parameter
    ushort          usDefCount;         // number of entries in record
    ushort          usRecCnt;           // running count of definitions processed
    tIPSDdefRecord  rMyDefRecord;       // hold a single record from the definition
    ushort          usGlobSize;         // Offset to the last byte added to the glob so far.
    ushort          usMaybesize;        // Offset to the last byte that was JUST added.

    pDefRekord = fnFlashGetIBPackedByteParm( IPB_CREATE_INDICIA_DEF_REC );
    usGlobSize = 0;

    // Get the number of entries in the record.     
    EndianAwareCopy( &usDefCount, pDefRekord, sizeof(short) );
    pDefRekord += sizeof(short);

    // If there are entries in the record...
    if( usDefCount ) 
    {
        // for each entry in the iPacked Byte Parameter...
        for( usRecCnt = 0; usRecCnt < usDefCount; usRecCnt++ ) 
        {
            // Copy the entry into a local structure.
            //memcpy( &rMyDefRecord, pDefRekord, sizeof(rMyDefRecord) );  //FIXME JAH do we neeed to fight endians here
            EndianAwareCopy( &rMyDefRecord.usFieldIdentifer, pDefRekord, sizeof(short) );
            EndianAwareCopy( &rMyDefRecord.usFieldOffset, pDefRekord+sizeof(short), sizeof(short) );
            EndianAwareCopy( &rMyDefRecord.usFieldLength, pDefRekord+(2*sizeof(short)), sizeof(short) );
            memcpy( &rMyDefRecord.ucFieldType, pDefRekord+(3*sizeof(short)), sizeof(uchar) );


            // Some special variables have special variable sizes:
            if( rMyDefRecord.usFieldLength == 0xFFFF )
            {
                switch( rMyDefRecord.usFieldIdentifer )
                {
                    case GLOBID_FLEXDEBIT_DATA:
                        rMyDefRecord.usFieldLength = wIPSD_flexDebitDataSize;
                        break;

                    default:
                        // By default, leave the size as it is.
                        break;
                }
            }

            // Use the local structure to convert and write 1 bob variable to a glob field.
            usMaybesize = fnConvertFromGlobToBobToGlob( &rMyDefRecord, pIPSD_createIndDataGlob, 
                                                        BOB2GLOB, IPSD_MAXGLOBSZ_CREATEIND );
            // Keep track of the last data in the glob...
            if( usMaybesize > usGlobSize ) 
                usGlobSize = usMaybesize;
            // Goto next record.
            pDefRekord +=  SIZE_IPSDDEFREC;
        }
        // Record the offset to the last piece of data stored.
        wIPSD_createIndDataGlobSize = usGlobSize;
    }
    return( BOB_OK );
}


//-----------------------------------------------------------------------------
//      Stuff for writing bob data back to a glob...
//---
//****************************************************************************
// FUNCTION NAME:   fnUpdatePSDParamListFromBob
// DESCRIPTION:   
//       Checks the psd parameter list definition record to see if the variable 
//      exists in the psd parameter glob. If so, uses fnConvertFromGlobToBobToGlob() 
//      to try to find the bob variable, convert it to the glob format, and place
//      it in the proper place in the existing pIPSD_paramListGlob.
//      This is so that the image generator (IG) can use the data that was 
//      updated "by hand".  
//       This is necessary on Future Phoenix, because there is not enough time
//      in the debit cycle to get the modified PSD parameters between the debit 
//      and the print.
//
// ARGUMENTS:   
//      usGlobVarID - Variable ID used in the defintion record.
// RETURN:      None.
// OUTPUT:  
//      If successfull, the pIPSD_paramListGlob array may have its data altered.
// NOTES:  
//      1. Type IPSD_DEF_REC has the same layout as type tIPSDdefRecord; the 
//          members just have different names.
// 
//  MODS:
//  2008.04.14 Clarisa Bellamy - Pass the max size of the paramList glob buffer
//             to the function fnConvertFromGlobToBobToGlob so that it won't 
//             overrun the buffer.
// AUTHOR:   Clarisa Bellamy 
//-----------------------------------------------------------------------------
typedef union u_IBPDefRecord
{
    tIPSDdefRecord  rType1;
    IPSD_DEF_REC    rType2;
}T_U_GLOBDefRecord;

void fnUpdatePSDParamListFromBob( unsigned short usGlobVarID )
{
    T_U_GLOBDefRecord   rMyDefRecord;

    // If the variable exists in the psd parameters glob...
    if( fnFlashGetIBPRecVar( &(rMyDefRecord.rType2), IPB_PSD_PARM_DEF_REC, usGlobVarID ) )
    {
        // Convert the bob variable, and copy to psd parameters glob.
        // Ignore any errors, because nothing is done differently if there was an error.
        // because we should not do anything differently at this point.
        (void)fnConvertFromGlobToBobToGlob( &(rMyDefRecord.rType1), pIPSD_paramListGlob, 
                                             BOB2GLOB, IPSD_MAXGLOBSZ_PARAMLIST );
    }
    return;
}


//-----------------------------------------------------------------------------
//      Stuff for converting from one glob to another...
//---
#ifndef JANUS
/******************************************************************************
   FUNCTION NAME: fnFlashGetIBPRecVar
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Find a field record with an ID that matches the one passed in.
                : Will search the type of record ID passed in.
   PARAMETERS   : wRecID = Record Type to search
                : wVarID = Varaible Type to search for
                : pass back a stucture with the data record
   RETURN       : FALSE if no matching variable ID found
   NOTES        : Can be used to search IPB_GET_INDICIA_DEF_REC or
                : IPB_CREATE_INDICIA_DEF_REC
******************************************************************************/
BOOL fnFlashGetIBPRecVar( tIPSDdefRecord* pFieldRec, unsigned short wRecID, unsigned short wVarID )
{
    BOOL        fRetVal = FALSE;
    uchar   *   pRecord;
    ushort      i, usFieldID, usNumFields;

    pRecord = fnFlashGetIBPackedByteParm( wRecID );

    memcpy( &usNumFields, pRecord, sizeof(ushort) );
    pRecord += sizeof(ushort);

    for( i = 0; i<usNumFields; i++ )
    {
        memcpy( &usFieldID, pRecord, sizeof(ushort) );
        if( usFieldID == wVarID ) 
        {
            fnGetDefRecord( pFieldRec, pRecord );
            fRetVal = TRUE;
            break;
        }
        pRecord += SIZE_IPSDDEFREC;
    } 

    return( fRetVal );
}
#endif
//****************************************************
//  Copied from Craig's code in Janus creport.c file.
//****************************************************

#ifndef JANUS
//      At this time, this is only used in PHOENIX.
// Table used to convert the different types of glob data to 
//  unistring format for use in the debit certificate.
const tGlob2Uni aGlob2UniTbl[] = {
// fieldOp,         length,   convType
{ DRFOP_ASKII,         0,    NO_FORMAT_CHANGE},
{ DRFOP_BE_INT,        4,    LONG_TO_STRING},  
{ DRFOP_LE_INT,        4,    LE_LONG_TO_STRING},
{ DRFOP_BE_MON_3DEC,   4,    BE_INT_MONEY4_TO_STRING},
{ DRFOP_LE_MON_3DEC,   4,    LE_INT_MONEY4_TO_STRING},
{ DRFOP_BE_MON_3DEC,   5,    BE_INT_MONEY5_TO_STRING},
{ DRFOP_LE_MON_3DEC,   5,    LE_INT_MONEY5_TO_STRING},
{ DRFOP_BE_MON_3DEC,   3,    BE_INT_MONEY3_TO_STRING},
{ DRFOP_LE_MON_3DEC,   3,    LE_INT_MONEY3_TO_STRING},
{ DRFOP_MAILDATE_FMT1, 4,    IB_MAILDATE_TO_STRING},
{ DRFOP_MAILDATE_FMT2, 6,    NO_FORMAT_CHANGE},
{ 0,0,0},                                   //table terminator
};
#endif


#ifndef JANUS
/******************************************************************************
   FUNCTION NAME: fnFindConvType
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : search a table to find a conversion type based on the fieldType
                : and length of the data
   PARAMETERS   : fieldType and length from debit certificate record or pSD parm record
                : pass back convType 
   RETURN       : FALSE if no match is found, true otherwise
   NOTES        : None
******************************************************************************/

BOOL fnFindConvType( ushort usFieldLength, uchar ucFieldType, uchar* pConvType )
{
    BOOL fRetVal = FALSE;

    tGlob2Uni* pXref = &aGlob2UniTbl[0];
    
    while( pXref->ucFieldType )
    {
        if( pXref->ucFieldType == ucFieldType )
        {
            if(   (pXref->ucFieldType == DRFOP_ASKII) 
               || (pXref->usFieldLength == usFieldLength) )
            {
                *pConvType = pXref->ucConvType;
                fRetVal = TRUE;
                break;
            }
        }
        pXref++; 
    }

    return( fRetVal );
}


//****************************************************************************
// FUNCTION NAME:   fnGetFromDebitCert
//   DESCRIPTION:   Searches the defintion record for an entry corresponding
//                  to the Variable ID. IF found, it reads that data from the
//                  debit certificate (IPSD indicia data) and converts it to
//                  either hex or ascii.
//
//        AUTHOR:   Craig DeFilippo, 
//                   Clarisa Bellamy mod to convert to Ascii string OR unicode.
//
//     ARGUMENTS:   pStrOrValue - Pointer to a destination buffer.
//                  usVarID - Variable ID used in the defintion record.
//                  fUnicode:   TRUE convert to unicode string.
//                              FALSE convert to ASCII string.
//
//        RETURN:   number of unicode or ascii chars after conversion.
//                  IF there was no matching record for this variable ID,
//                  then it returns 0.
//
//         NOTES:   Moved the code from fnReadDebitCertParms() and changed that 
//                  function to call this one. 
//-----------------------------------------------------------------------------
unsigned short fnGetFromDebitCert( void * pStrOrValue, unsigned short usVarID, BOOL fUnicode )
{
    tIPSDdefRecord  rFieldRec;
    char    *   pData;
    uchar       ucConvType;
    ushort      usLen = 0; 

    //get the IBI record from flash
    if( fnFlashGetIBPRecVar( &rFieldRec, IPB_GET_INDICIA_DEF_REC, usVarID ) )
    {
        pData = (char*)&pIPSD_barcodeData.pGlob;    //aDebitCert;
        if( fnFindConvType( rFieldRec.usFieldLength, rFieldRec.ucFieldType, &ucConvType ) )
        {
            if( fUnicode )
                usLen = fnPleaseConvertThis2Unicode( pData+rFieldRec.usFieldOffset, 
                                                     rFieldRec.usFieldLength,
                                                     ucConvType,
                                                     pStrOrValue );
            else
                usLen = fnPleaseConvertThis2Ascii( pData+rFieldRec.usFieldOffset, 
                                                   rFieldRec.usFieldLength,
                                                   ucConvType,
                                                   pStrOrValue );
        }
    }
    return( usLen );    
}

//****************************************************************************
// FUNCTION NAME:   fnGetFromPSDParamList
//   DESCRIPTION:   Searches the defintion record for an entry corresponding
//                  to the Variable ID. IF found, it reads that data from the
//                  PSD ParameterList glob and converts it to either hex or 
//                  ascii.
//
//        AUTHOR:   Craig DeFilippo, 
//                   Clarisa Bellamy mod to convert to Ascii string OR unicode.
//
//     ARGUMENTS:   pStrOrValue - Pointer to a destination buffer.
//                  usVarID - Variable ID used in the defintion record.
//                  fUnicode:   TRUE convert to unicode string.
//                              FALSE convert to ASCII string.
//
//        RETURN:   number of unicode or ascii chars after conversion.
//                  IF there was no matching record for this variable ID,
//                  then it returns 0.
//
//         NOTES:  Moved the code from fnReadPSDParms() and changed that 
//                 function to call this one. 
//-----------------------------------------------------------------------------
unsigned short fnGetFromPSDParamList( void * pStrOrValue, unsigned short usVarID, BOOL fUnicode )
{
    tIPSDdefRecord  rFieldRec;
    char    *   pData;
    uchar       ucConvType;
    ushort      usLen = 0;

    //get the PSD record from flash
    if( fnFlashGetIBPRecVar( &rFieldRec, IPB_PSD_PARM_DEF_REC, usVarID ) )
    {
        pData = (char*)&pIPSD_paramListGlob;        //aPSDParam;
        if( fnFindConvType( rFieldRec.usFieldLength, rFieldRec.ucFieldType, &ucConvType ) )
        {
            if( fUnicode )
                usLen = fnPleaseConvertThis2Unicode( pData+rFieldRec.usFieldOffset, 
                                                     rFieldRec.usFieldLength,
                                                     ucConvType,
                                                     pStrOrValue );
            else
                usLen = fnPleaseConvertThis2Ascii( pData+rFieldRec.usFieldOffset, 
                                                   rFieldRec.usFieldLength,
                                                   ucConvType,
                                                   pStrOrValue );
        }
    }

    return( usLen );    
}
#endif


#ifndef JANUS
/******************************************************************************
   FUNCTION NAME: fnReadDebitCertParms
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Read a parameter from the Debit Certificate and convert it to 
                : unicode.
   PARAMETERS   : Unicode string destination pointer, variable ID
   RETURN       : length of unicode string, number of unicode chars 
   NOTES        : This is the one used by Janus (it was in creport.c)
******************************************************************************/
unsigned short fnReadDebitCertParms( void * pStrOrValue, unsigned short usVarID )
{
    // Use generic function above, tell it to convert to Unicode.
    return( fnGetFromDebitCert( pStrOrValue, usVarID, TRUE ) );
}

/******************************************************************************
   FUNCTION NAME: fnReadPSDParms
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  :
                : 
   PARAMETERS   : Unicode string destination pointer, variable ID
   RETURN       : length of unicode string, number of unicode chars 
   NOTES        : None
******************************************************************************/
unsigned short fnReadPSDParms( void * pStrOrValue,unsigned short usVarID )
{
    // Use generic function above, tell it to convert to Unicode.
    return( fnGetFromDebitCert( pStrOrValue, usVarID, TRUE ) );
}

#endif


//lint +e641  





