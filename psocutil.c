/************************************************************************
 *	PROJECT:        DM150
 *	COMPANY:        Pitney Bowes
 *	AUTHOR :        
 *   MODULE NAME:    PsocUtil.c
 *   REVISION:       
 *       
 *   DESCRIPTION:    PSOC Utilities - Interfaces to Print Head PSOC 
 *					and Host PSOC
 *
 * ----------------------------------------------------------------------
 *               Copyright (c) 2003 Pitney Bowes Inc.
 *                    35 Waterview Drive
 *                   Shelton, Connecticut  06484
 * ----------------------------------------------------------------------
 *
 *       REVISION HISTORY:
*
* 18-Feb-13 sa002pe on FPhx 02.10 shelton branch
*	For fraca 219326:
*	1. Changed LogPsocError to add cases for PSOC_SEED_MISMATCH, PSOC_PH_SEED_FAILURE & PSOC_CM_SEED_FAILURE.
*	2. Changed fnPSOCVerifySync to:
*		a. Do retries on getting the seeds from the PSOC chips.
*		b. Declare a different error if the seeds couldn't be retrieved from the PSOC chips.
*		c. Rearrange the function so the retrieval & comparison of the seeds is done inside a while loop
*			so the code doesn't have to be duplicated after the PSOC chips are reseeded.
*
* 15-Oct-12 sa002pe on FPHX 02.10 shelton branch
*	For Janus Fraca 214723: Changed GetPsocVerNum to remove the use of the event log. 
*	It isn't needed anymore for G900.  Instead an entry will be put in the exception log and the syslog.
*
*	Changed ReseedPsocs to:
*	1. Only report the comm "failure" if the retry worked instead of declaring a comm "failure" then a comm "fatal".
*
*	Changed LogPsocError to have a switch statement for the different error code values instead of
*	a multi-clause if statement.
 * 
 *************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "pbos.h"
#include "global.h"
#include "sys.h"
#include "hal.h"
#include "psoctask.h"
#include "psocvecs.h"
#include "psoc_hex.h"
#include "errcode.h"
#include "nucleus_gen_cfg.h"
#include "psocutil.h"


#define MAXPLOG 2048
#define MAXVLOG 2048

PSOC_SECREC PSOCSecurity[2];

typedef struct proglog
{
  unsigned char dev;
  unsigned char portI;
  unsigned char portO;
  unsigned char giveUp;
}PROGLOG;

typedef struct vectlog
{
  unsigned long ch;
  INTEL_HEX_REC *Addr;
}VECTLOG;

typedef struct bitdec
{
  unsigned char startDetected;
  unsigned char bitRead;
  unsigned char j;
  unsigned char i;
  unsigned char edgeDetected;
  unsigned char msk;
  unsigned short byte;
  unsigned char  fill[8];
}PSOCBITTRACE;
PROGLOG pLog[ MAXPLOG ];
unsigned long pLogIndex = 0;

VECTLOG vLog[ MAXVLOG ];
unsigned long vLogIndex = 0;

#define PSOCMAXBUFFERSIZE 48
#define PSOCMAXRECVBUFFERSIZE 2048

extern void	fnLogSystemError( unsigned char, unsigned char, unsigned char, unsigned char, unsigned char );
void fnSystemLogEntry( UINT8 bType, char *pData, UINT32 lwLen );

char *psocImageVNum = "01.01.0014";

char  verNum[2][PSOCVERLENGTH];


unsigned long PSOCReadTimeouts = 0;
unsigned long PSOCWrongMsgLens = 0;
unsigned long PSOCIncompleteMsgs = 0;
unsigned long PSOCNumOfClocksAfterXmit =  0; 
unsigned long PSOCMisCompares = 0;
unsigned long PSOCBlockFailures;
unsigned short MCBPsocId;
unsigned short PHPsocId;
unsigned char PSOCNumOfDummyReads = 1;
unsigned char PSOCNumOfDummyReadsAfterReset = 4;
unsigned char PSOCCLOCKSETUPREADS = 4;
unsigned char PSOCNumOfClkPerBit = 8;
unsigned char PSOCInvertSerialData = 0;
unsigned char PSOCInvertSerialClk = 0;
unsigned char PSOCWithStartStop = 1;
unsigned char PSOCCRSync[ PSOC_READ_SYNC_RSP_LEN ];
unsigned char PSOCPHSync[ PSOC_READ_SYNC_RSP_LEN ];
unsigned char PSOCCRRand[PSOC_READ_SEED_RSP_LEN];
unsigned char PSOCPHRand[PSOC_READ_SEED_RSP_LEN];
unsigned char PSOCCRSeed[PSOC_READ_SEED_RSP_LEN];
unsigned char PSOCPHSeed[PSOC_READ_SEED_RSP_LEN];
unsigned char PSOCDebCRResp;
unsigned char PSOCDebPHResp;
unsigned short	imageChkSum[2], numImageBlocks[2];
unsigned short PSOCReadBuffer[ PSOCMAXBUFFERSIZE ];
PSOCBITTRACE PSOCRecv[ PSOCMAXRECVBUFFERSIZE ],*pT;
unsigned char PSOCBufferInputCnt = 0;
unsigned char PSOCBufferReadPos = 0;
unsigned PSOCRecvCnt = 0;
unsigned char PSOCBufferWritePos = 0;
unsigned long badVectorTypes = 0;
static BOOL		giveUpWaiting;
INTEL_HEX_REC	*securityRec_p[2];
unsigned short byteRead = 0;
unsigned char startDetected = 0;
unsigned char curReadBit = 0;
unsigned char haveByte = 0;


/* ************************************************************************* */
// FUNCTION NAME: ResetPsoc
// DESCRIPTION: This function will toggle pin 15 on a PSOC causing it to reset
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// *************************************************************************/
int ResetPsoc( unsigned char psocId )
{
  int			err = -1;
  unsigned short		id = 0;
  
  /********************************* CODE ***********************************/
  
  // DOWNLOAD INITIALIZE 1 VECTORS
  HALWritePSOCVector( psocId, InitVec1, numBitsInitVec1, TRUE);
  if( HALPSOCWaitForHighToLow( psocId ) == 0 )
    {
      // DOWNLOAD INITIALIZE 2 VECTORS
	  HALWritePSOCVector( psocId, InitVec2, numBitsInitVec2, FALSE);
			
      if( HALPSOCWaitForHighToLow( psocId ) == 0)
	{
	  // DOWNLOAD INITIALIZE 3 VECTORS
#ifdef CFG_NU_BSP_BEAGLEBONE_BLACK_ENABLE
    	  // BBB interfaces with PSOC Eval Kit only at 3.3V
    	  HALWritePSOCVector( psocId, InitVec3_3V, numBitsInitVec3_3, FALSE);
#else
    	  HALWritePSOCVector( psocId, InitVec3_5V, numBitsInitVec3_5, FALSE);
#endif
#if 0 // InitVec3_X does not need to wait
	  if(HALPSOCWaitForHighToLow( psocId ) == 0)
	    {
#endif
	      // GET THE SILICON ID
		  HALWritePSOCVector( psocId, ReadIDSetupVec, numBitsReadIDSetup, FALSE);
	      if( HALPSOCWaitForHighToLow( psocId ) == 0)
		{
		  id = HALReadPSOCData( psocId , ReadIDByte1Vec[ 0 ], numBitsReadIDByte1 ) << 8;
		  id |= HALReadPSOCData( psocId, ReadIDByte2AVec[ 0 ], numBitsReadIDByte2A);
		  //tell PSOC done reading
		  HALWritePSOCVector( psocId, dnRdngVec, numBitsDoneReading, FALSE);
		  dbgTrace(DBG_LVL_INFO,"Silicon ID for PSOC %d: %x\r\n", psocId, id);
		  if(id == PSOCCHIPID)
		    err = 0;
		  else
		    err = PSOCINVALIDCHIPID;
		}
	      else
		err = PSOCREADIDERROR;
#if 0
	    }
	  else
	    err = PSOCVEC3ERROR;
#endif
	}
      else
	err = PSOCVEC2ERROR;
    }
  else
    err = PSOCVEC1ERROR;
  return(err);
}


/* *************************************************************************
// FUNCTION NAME: WriteSerialPort
// DESCRIPTION: This transmit a byte out to the PSOC
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// *************************************************************************/
void WriteSerialPort( unsigned char psocId , unsigned char ch)
{

  HALWritePSOCSerialData(psocId , ch);
  PSOCReadBuffer[ PSOCBufferWritePos ] = ( 0x5000 | ( psocId << 8 ) ) | ch;
  PSOCBufferWritePos++;
  if(PSOCBufferWritePos == PSOCMAXBUFFERSIZE)
    PSOCBufferWritePos = 0;
  PSOCBufferInputCnt++;
  return;
}


/* *************************************************************************
// FUNCTION NAME: ReadSerialPort
// DESCRIPTION: This function will tx and rx a single byte using SCIF1
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// *************************************************************************/
unsigned short ReadSerialPort( unsigned char psocId )
{
  unsigned short readValue = HALReadPSOCSerialData(psocId);
  PSOCReadBuffer[ PSOCBufferWritePos ] = readValue;
  PSOCBufferWritePos++;
  if(PSOCBufferWritePos == PSOCMAXBUFFERSIZE)
    PSOCBufferWritePos = 0;
  PSOCBufferInputCnt++;
  return(readValue);
}

unsigned char	failCode, *junk1;

// *************************************************************************
// FUNCTION NAME: SendHostCmd
// DESCRIPTION: This function will send 1 byte command to the desired psoc.		
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// *************************************************************************
unsigned char SendHostCmd( unsigned char psocId, unsigned char cmd, unsigned char respLen , unsigned char *resp_p )
{
  unsigned char	rv = SUCCESSFUL, len;
  int		x, j;
  unsigned short byteRead,junk1;
  

  if(psocId == PH_PSOC)
    {
      if (cmd != PSOC_NULL_CMD)
	cmd |= 0x80;
    }
  WriteSerialPort( psocId , cmd );
  OSWakeAfter( 20 );
  if (cmd == PSOC_NULL_CMD)
    {
      byteRead = ReadSerialPort( psocId );
      *resp_p = ( unsigned char ) byteRead;
      if (byteRead & 0xff00)
	{
	  rv = FAILURE;
	  failCode = 0xed;
	}
    }
  else
    {
      j = 0;
      while( j < 5)
	{
	  //If there was an error try again. The psoc could be busy
	  byteRead = ReadSerialPort( psocId );
	  if(byteRead & 0xff00)
	    {
	      rv = FAILURE;
	      PSOCReadTimeouts++;
	      failCode = 0xed;
	    }
	  else
	    {
	      len = ( byteRead & 0xff );
	      //If the length is not what we expect
	      if ( len != respLen )
		{
		  PSOCWrongMsgLens++;
		  //could be an error response which we will read
		  if ( len == PSOC_ERROR_RSP_LEN )
		    {
		      for (x = 0; x < len; x++, resp_p++)
			{
			  byteRead = ReadSerialPort( psocId );
			  resp_p[0] = byteRead & 0xff;
			}
		    }
		  rv = FAILURE;
		  failCode = ( unsigned char ) byteRead;
		  // CLEAR ANY DATA THAT MIGHT BE IN THE UARTS
		  while(1)
		    {
		      junk1 = ReadSerialPort( psocId );
		      if(junk1 & 0xff00)
			break;
		    }
		  break;
		}
	      else
		{
		  x = 0;
		  while(x < len)
		    {
		      byteRead = ReadSerialPort( psocId );
		      if((byteRead & 0xff00) == 0 )
			{
			  resp_p[0] = byteRead & 0xff;
			  resp_p++;
			  x++;
			}
		      else
			{
			  PSOCIncompleteMsgs++;
			  rv = FAILURE;
			  failCode = 0xed;
			  break;
			}
		    }
		  break;
		}
	    }
	  j++;
	}
    }
  return( rv );
}


/* *************************************************************************
// FUNCTION NAME: GetPsocVerNum
// DESCRIPTION: This function will command a PSOC to read and return its
//				version number.
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// *************************************************************************/
unsigned char GetPsocVerNum( unsigned char psocId )
{
	unsigned char		rv;
	unsigned char     i;
	unsigned char     rsp[11];
	char	pLogBuf[100];
  
  // ******************************** CODE ***********************************
  
	// make sure the previous number is cleared out
	(void)memset(verNum[psocId], 0, sizeof(verNum[psocId]));

	rv = SendHostCmd( psocId, PSOC_READ_VNUM, PSOC_READ_VNUM_RSP_LEN, rsp );

	if (rv == SUCCESSFUL)
	{
		if (rsp[0] == PSOC_READ_VNUM_RSP)
		{
			for (i = 0; i < 10; i++)
				verNum[psocId][i] = (char)rsp[i+1];

			verNum[psocId][i] = 0;
		}
		else
		{
			if (psocId == MCB_PSOC)
				(void)sprintf(pLogBuf, "Wanted CM PSOC ver msg 83h, got %02Xh", rsp[0]);
			else
				(void)sprintf(pLogBuf, "Wanted PH PSOC ver msg 83h, got %02Xh", rsp[0]);

			// put the message in the syslog first just in case there's a problem w/ the exception log
			fnSystemLogEntry( SYSLOG_TEXT, pLogBuf, 38);

			rv = FAILURE;
		}
	}
  
	return( rv );
}



/* *************************************************************************
// FUNCTION NAME: atoh
// DESCRIPTION: This function converts an ascii char to a hex value
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// *************************************************************************/
unsigned char atoh( char ac )
{
  unsigned char	val = 0;
  
  // ******************************** CODE ***********************************
  
  if (ac >= 'a')
    val = ac - 87;
  else
    if (ac >= 'A') 
      val = ac - 55;
    else
      val = ac - 48;
  
  return (val);
}


/* *************************************************************************
// FUNCTION NAME: iatob
// DESCRIPTION: This function converts an Intel 2 char ascii value to a byte
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// *************************************************************************/
unsigned char iatob( char *num_p )
{
  unsigned char	byteVal = 0;
  
  // ********************************* CODE ***********************************
  
  byteVal = atoh( *num_p++ ) << 4;
  byteVal |= atoh( *num_p );
  
  return (byteVal);
}


/* *************************************************************************
// FUNCTION NAME: iatos
// DESCRIPTION: This function converts an Intel 4 char ascii value to a short
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// *************************************************************************/
unsigned short iatos( char *num_p )
{
  unsigned short	shortVal = 0;
  
  // ********************************* CODE ***********************************
  
  shortVal  = atoh( *num_p++ ) << 12;
  shortVal |= atoh( *num_p++ ) << 8;
  shortVal |= atoh( *num_p++ ) << 4;
  shortVal |= atoh( *num_p );
  
  return (shortVal);
}


/* *************************************************************************
// FUNCTION NAME: ConvertRecord
// DESCRIPTION: This function converts an Intel hex record which is in ascii
/				format to a record in numeric format.  This is used for PSOC
//				flash downloading.
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// *************************************************************************/
char *ConvertRecord( char *cp , PSOC_REC *psr_p )
{
  unsigned char	i, x;
  // ********************************* CODE ***********************************
  
  if( *cp == ':')
    {
      cp++;
      psr_p->len  = iatob( cp );
      cp += 2;
      psr_p->addr = iatos( cp );
      cp += 4;
      psr_p->type  = iatob( cp );
      cp += 2;
      for (i = 0, x = 0; i < psr_p->len; i++, x += 2)
	{
	  psr_p->data[i] = iatob( cp );
	  cp += 2;
	}
      psr_p->chkSum = iatob( cp );
      cp += 2;
    }
  return(cp);
}

#if 0 // unused code
/* *************************************************************************
// FUNCTION NAME: ErasePsoc
// DESCRIPTION: This function will erase a given PSOC
//
// AUTHOR: George Monroe
//
// *************************************************************************/
int ErasePsoc( unsigned char psocId )
{
  int err = -1,resCnt;

  resCnt = 0;
  while(resCnt < 3)
    {
      err = ResetPsoc( psocId );
      if(err == 0 )
	break;
      resCnt++;
    }
  if(err == 0)
    {
      //Erase the flash
      WritePsocVectors( psocId, EraseVec , sizeof(EraseVec)/sizeof(unsigned long), EraseVecMask ,  32);
      //Wait for completion
      err = WaitForHighToLow( psocId );
    }
  return(err);
}
#endif

/* *************************************************************************
// FUNCTION NAME: ProgPsocFlash
// DESCRIPTION: This function will download the PSOC exe and store it in flash
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// *************************************************************************/
int ProgPsocFlash( unsigned char psocId )
{
  char			*c_p;
  int err = -1,inSecPiece = 0,resCnt = 0;
  unsigned char			a, notDone = TRUE,ch = 0;
  unsigned long			blkNum,oldWbVec,wrByteVec[1],setBlkNumVec[1],curBankNum;
  unsigned long			oldSbVec, setBankVector[3];
  unsigned long			bankNum;
  PSOC_REC		psocRec;

  // ********************************* CODE ***********************************

  PSOCSecurity[ psocId ].numSecBlks = 0;
  numImageBlocks[psocId] = 0;
  PSOCMisCompares = 0;  
  bankNum = 0;
  curBankNum = 0;
  blkNum = 0;

  while(resCnt < 3)
    {
      err = ResetPsoc( psocId );
      if(err == 0 )
	break;
      resCnt++;
    }
  oldWbVec = WriteByteVec[0];
  oldSbVec = SetBlkNumVec[0];
  setBankVector[ 0 ] = SetBankNum[ 0 ];
  setBankVector[ 1 ] = SetBankNum[ 1 ];
  setBankVector[ 2 ] = SetBankNum[ 2 ];
  if(err == 0)
    {
      if (psocId == MCB_PSOC)
	c_p = (char *)mcbCodeImage;
      else
	c_p = (char *)phCodeImage;
      badVectorTypes = 0;  
      //PERFORM A BULK ERASE ON THE FLASH JUST IN CASE
	  HALWritePSOCVector( psocId, EraseVec, numBitsErase, FALSE);
      err = HALPSOCWaitForHighToLow( psocId );
      if( err == 0 )
	{
	  setBankVector[ 1 ] = SetBankNum[ 1 ];
	  setBankVector[ 1 ] |= ( curBankNum << 23);
	  HALWritePSOCVector( psocId, setBankVector, numBitsSetBankNum, FALSE);
	  while (notDone && err == 0)
	    {
	      vLog[ vLogIndex ].ch = ch;
	      vLog[ vLogIndex ].Addr = ( INTEL_HEX_REC *) c_p;
	      vLogIndex++;
	      if(vLogIndex >= MAXVLOG )
		vLogIndex = 0;
	      c_p = ConvertRecord( c_p, &psocRec );  //INTEL HEX TO NORMAL HEX
	      switch (psocRec.type)
		{
		case 0x00:
		  if(inSecPiece == 0 )
		    {
		      bankNum = psocRec.addr / PSOCBANKSIZE;
		      blkNum =  (psocRec.addr & (PSOCBANKSIZE - 1)) / PSOCRECORDSIZE;
		      if(bankNum != curBankNum )
			{
			  setBankVector[ 1 ] = SetBankNum[ 1 ];
			  setBankVector[ 1 ] |= ( bankNum << 23);
			  HALWritePSOCVector( psocId, setBankVector, numBitsSetBankNum, FALSE);
			  curBankNum = bankNum;
			}
		      //WRITE ALL THE BYTES TO PSOC SRAM
		      for (a = 0; a < psocRec.len; a++)
			{
			  wrByteVec[ 0 ] = oldWbVec | (a  << 21);
			  wrByteVec[ 0 ] |= (psocRec.data[a] << 13);
			  HALWritePSOCVector( psocId, wrByteVec, numBitsWriteByte, FALSE);
			}
		      //TODO - See if writing bank number again is really necessary
		      setBankVector[ 1 ] = SetBankNum[ 1 ];
		      setBankVector[ 1 ] |= ( curBankNum << 23);
			  HALWritePSOCVector( psocId, setBankVector, numBitsSetBankNum, FALSE);
					
		      //MOVE THE DATA BLOCK FROM SRAM TO PSOC FLASH
		      setBlkNumVec[ 0 ] = oldSbVec | (blkNum << 13);
			  HALWritePSOCVector( psocId, setBlkNumVec, numBitsSetBlkNum, FALSE);
			  HALWritePSOCVector( psocId, ProgBlkVec, numBitsProgBlk, FALSE);
		      err = HALPSOCWaitForHighToLow( psocId );
		      if( err )
			err = PSOCWRBLKERROR;
		      blkNum++;
		      numImageBlocks[psocId]++;
		    }
		  else
		    {
		      if(PSOCSecurity[ psocId ].numSecBlks < MAXPSOCSECRECS )
			{
			  PSOCSecurity[ psocId ].secRecs[ PSOCSecurity[ psocId ].numSecBlks++ ] = psocRec;
			}
		    }
		  break;
		  
		case 0x04:
		  if(inSecPiece)
		    {
		      PSOCSecurity[ psocId ].chkSumRec = psocRec;
		    }
		  else
		    {
		      inSecPiece = 1;
		      PSOCSecurity[ psocId ].Secur = psocRec;
		    }
		  break;
		  
		case 0x01:
		  notDone = FALSE;
		  break;
		  
		default:
		  badVectorTypes++;
		  break;
		}
	      
	      OSWakeAfter( 10 );
	    }
	}
      else
	err = PSOCERASEVECERROR;
    }
  return(err);
}

/* *************************************************************************
// FUNCTION NAME: VerifyPsocFlash
// DESCRIPTION: This function will read back the PSOC flash and compare it 
// 				to the code image that was used to program it.
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// *************************************************************************/
int VerifyPsocFlash( unsigned char psocId )
{
  unsigned char			 x, addr, flashByte;
  char *c_p;
  unsigned long			oldSbVec,setBlkNum[1],rdByteVec[1],cBlk;
  unsigned long			blkNum,oldRbVec,bankNum,curBankNum;
  unsigned long			setBankVector[3];
  int err = -1;
  PSOC_REC		psocRec;
  PSOC_REC		psocVerRec;

  // ******************************** CODE ***********************************
									
  oldSbVec = SetBlkNumVec[0];
  oldRbVec = ReadByteVec[0];
  setBankVector[ 0 ] = SetBankNum[ 0 ];
  setBankVector[ 1 ] = SetBankNum[ 1 ];
  setBankVector[ 2 ] = SetBankNum[ 2 ];
  PSOCBlockFailures = 0;
  if (psocId == MCB_PSOC)
    c_p = (char *)mcbCodeImage;
  else
    c_p = (char *)phCodeImage;

  curBankNum = 0;
  bankNum = 0;
  setBankVector[ 1 ] = SetBankNum[ 1 ];
  setBankVector[ 1 ] |= ( bankNum << 23);
  HALWritePSOCVector( psocId, setBankVector, numBitsSetBankNum, FALSE);
  for (cBlk = 0; cBlk < numImageBlocks[psocId]; cBlk++)
    {
      c_p = ConvertRecord( c_p, &psocRec );  //INTEL HEX TO NORMAL HEX
      bankNum = psocRec.addr / PSOCBANKSIZE;
      blkNum =  (psocRec.addr & (PSOCBANKSIZE - 1)) / PSOCRECORDSIZE;
      if(bankNum != curBankNum )
	{
	  setBankVector[ 1 ] = SetBankNum[ 1 ];
	  setBankVector[ 1 ] |= ( bankNum << 23);
	  HALWritePSOCVector( psocId, setBankVector, numBitsSetBankNum, FALSE);
	  curBankNum = bankNum;
	}
      setBlkNum[ 0 ] = oldSbVec | (blkNum << 13);
	  HALWritePSOCVector( psocId, setBlkNum, numBitsSetBlkNum, FALSE);
	  HALWritePSOCVector( psocId, VerifySetupVec, numBitsVerifySetup, FALSE);
      err = HALPSOCWaitForHighToLow( psocId );
      if(err)
	err = PSOCRDBLKERROR;
      addr = 0x0;
      //Read ALL THE BYTES From PSOC SRAM
      for( x = 0 ; x < PSOCRECORDSIZE ; x++)
	{
	  rdByteVec[ 0 ] = oldRbVec | (addr++ << 21);
	  flashByte = HALReadPSOCData( psocId , rdByteVec[ 0 ], numBitsReadByte );
	  //tell PSOC done reading
	  HALWritePSOCVector( psocId, dnRdngVec, numBitsDoneReading, FALSE);
	  psocVerRec.data[ x ] = flashByte;
	}
      for( x = 0 ; x < PSOCRECORDSIZE ; x++ )
	{
	  if (psocVerRec.data[ x ] != psocRec.data[x])
	    {
	      err = PSOCVERIFYFAILED;
	      PSOCBlockFailures++;
	      break;
	    } 
	}
      if (err)
	break;
    }
  return (err);
}


/* *************************************************************************
// FUNCTION NAME: LockPsocFlash
// DESCRIPTION: This function will write the security record to flash.
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// *************************************************************************/
int LockPsocFlash( unsigned char psocId )
{
  unsigned char	x,y,i;
  unsigned long	curBankNum,oldWbVec;
  unsigned long	setBankVector[3],wrSecByte[1];
  int err = 0;
  PSOC_REC	*pRec;
  PSOC_SECREC *pSecur;
  unsigned char *pData;
  // ********************************* CODE ***********************************

  //Each Security Block is 32 bytes in length
  //Each Security Record contains two security block
  //The 32 byte security block is programmed into
  //the first 32 bytes of each bank
  //Our part, 466 contains 4 banks
  //The first two records in the security array contain the security records

  oldWbVec = WriteByteVec[0];
  setBankVector[ 0 ] = SetBankNum[ 0 ];
  setBankVector[ 1 ] = SetBankNum[ 1 ];
  setBankVector[ 2 ] = SetBankNum[ 2 ];
  pSecur = &PSOCSecurity[ psocId ];
  if(pSecur->numSecBlks > ( PSOCNUMBANKS / PSOCSECURBLKSPERRECORD ))
    {
      curBankNum = 0;
      for( x = 0 , pRec = &pSecur->secRecs[ 0 ] ; x < ( PSOCNUMBANKS / PSOCSECURBLKSPERRECORD ) ; x++ , pRec++ )
	{
	  if(pRec->len == PSOCRECORDSIZE )
	    {
	      for( y = 0, pData = pRec->data ; y < PSOCSECURBLKSPERRECORD ; y++ )
		{
		  
		  setBankVector[ 1 ] = SetBankNum[ 1 ];
		  setBankVector[ 1 ] |= ( curBankNum << 23);
		  HALWritePSOCVector( psocId, setBankVector, numBitsSetBankNum, FALSE);
		  for (i = 0; i < PSOCSECURBLKSIZE ; i++)
		    {
		      wrSecByte[ 0 ]  = oldWbVec | (i << 21);
		      wrSecByte[ 0 ] |= (*pData << 13);
		      HALWritePSOCVector( psocId, wrSecByte, numBitsWriteByte, FALSE);
		      pData++;
		    }
	      HALWritePSOCVector( psocId, SecureVec, numBitsSecure, FALSE);
		  err = HALPSOCWaitForHighToLow( psocId );
		  if( err )
		    break;
		  curBankNum++;
		}
	      if(err)
		break;
	    }
	  else
	    {
	      err = PSOCINVALIDSECURRECORDSIZE;
	      break;
	    }
	}
    }
  else
    err = PSOCNOTENOUGHSECURRECORD;
  return(err);
}

/* *************************************************************************
// FUNCTION NAME: VerifyPsocChkSum
// DESCRIPTION: This function will instruct the PSOC to compute a check sum
// 				on its flash.  The computed sum will be compared to the one
//				in the program image..
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// *************************************************************************/
BOOL VerifyPsocChkSum( unsigned char psocId )
{
  // ********************************* CODE ***********************************

  //TODO - The G900 checksum is not calculated correctly and there is no comparison.
  // A checksum comparison is not technically needed since we already read back every byte in flash and compare to original.
  // But if this is deemed necessary, the below code should be used as the base for the implementation.
#if 0
	  unsigned short		chkSum;
	  int err;

  HALWritePSOCVector( psocId, ChkSumSetupVecA, numBitsChkSumSetupA, FALSE);
  err = HALPSOCWaitForHighToLow( psocId);
  if( err == 0)
	{//For each bank, call the below code, adding the checksums for all the banks
	  chkSum = HALReadPSOCData( psocId , ReadChkSum1Vec[ 0 ], numBitsReadChkSumByte1 ) << 8;
	  chkSum |= HALReadPSOCData( psocId, ReadChkSum2Vec[ 0 ], numBitsReadChkSumByte2);
	  //tell PSOC done reading
	  HALWritePSOCVector( psocId, dnRdngVec, numBitsDoneReading, FALSE);
    }
  // Compare final checksum with that generated during programming and return result
#endif
  return TRUE;
}


/* *************************************************************************
// FUNCTION NAME: LogPsocError
// DESCRIPTION: This function will place an entry in the SystemErrorLog and
//				then spit an error string to hyperterminal.
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// ************************************************************************ */
void LogPsocError( unsigned char type, unsigned char data1, unsigned char data2, unsigned char data3, char *msg )
{
    fnLogSystemError( ERROR_CLASS_PSOC, type, data1, data2, data3 );
    fnSystemLogEntry( SYSLOG_TEXT, msg, strlen( msg ));
	switch (type)
    {
	    case PSOC_VER_MISMATCH:
		case PSOC_COMM_FATAL:
		case PSOC_COMM_TIMEOUT:
		case PSOC_PROG_FATAL:
		case PSOC_SEED_MISMATCH:
		case PSOC_PH_SEED_FAILURE:
		case PSOC_CM_SEED_FAILURE:
	        // PSOC_PROG_FAILURE or PSOC_COMM_FAILURE are not meter error screen candidates 
			// because we try to program or communicate again. If that fails, then it logs 
			// a PSOC_PROG_FATAL or PSOC_COMM_FATAL, which will put up a meter error screen
// All BBBs will not have 2 PSOCs so will generate this error.  But we don't want to put meter in error state.
#ifdef CFG_CSD_2_3_ENABLE
	        fnReportMeterError( ERROR_CLASS_PSOC, type );
#endif
			break;
		
		default:
			break;
    }
}


char PSOCPHGENSeed[] = "PSOC PH Gen Seed";
char PSOCMCBGENSeed[] = "PSOC MCB Gen Seed";
char PSOCGENSeedComp[] = "PSOC Gen Seed Complete";
char PSOCComSeedComp[] = "PSOC Commit Seed Complete";
char PSOCPHComSeed[] = "PSOC PH Commit Seed";
char PSOCMCBComSeed[] = "PSOC MCB Commit Seed";

/* *************************************************************************
// FUNCTION NAME: ReseedPsocs
// DESCRIPTION: This function will interface with both psocs instructing them
//				to perform a reseeding procedure.
//
// AUTHOR: 		R.Day - Complete Software Solutions
//
// ************************************************************************ */
unsigned char ReseedPsocs( unsigned char msgId )
{
    unsigned char		rv[5];
    unsigned char		retVal = PSOC_COMPLETE;
  
    // ********************************* CODE *********************************** * //
  
    switch (msgId)
    {
        case RESEED_MSG:
            fnSystemLogEntry( SYSLOG_TEXT, PSOCPHGENSeed , sizeof(PSOCPHGENSeed));
            if (SendHostCmd( PH_PSOC, PSOC_GEN_SEED, PSOC_GENSEED_RSP_LEN, rv ) == FAILURE)
            {
                if (SendHostCmd( PH_PSOC, PSOC_GEN_SEED, PSOC_GENSEED_RSP_LEN, rv ) == FAILURE)
                {
                    LogPsocError( PSOC_COMM_FATAL, RESEED_MSG, PH_PSOC, failCode,
                        "**Comm Fatal => reseed msg - PH" );
                    retVal = PSOC_ERROR;
                    break;
                }
				else
				{
	                LogPsocError( PSOC_COMM_FAILURE, RESEED_MSG, PH_PSOC, failCode,
	                    "**Comm Fail => reseed msg - PH" );
				}
            }

            /* must have succeeded with Gen Seed PH PSOC - so do MCB PSOC */
            // Have the MCB generate the Polynomial based on the random number
            // sent from the PH psoc
            fnSystemLogEntry( SYSLOG_TEXT, PSOCMCBGENSeed , sizeof(PSOCMCBGENSeed));
            if (SendHostCmd( MCB_PSOC, PSOC_GEN_SEED,PSOC_GENSEED_RSP_LEN, rv ) == FAILURE)
	        {
                if (SendHostCmd( MCB_PSOC, PSOC_GEN_SEED,PSOC_GENSEED_RSP_LEN, rv ) == FAILURE)
                {
                    LogPsocError( PSOC_COMM_FATAL, RESEED_MSG, MCB_PSOC, failCode,
                        "**Comm Fatal => reseed msg - CM" );
                    retVal = PSOC_ERROR;
                }
				else
				{
	                LogPsocError( PSOC_COMM_FAILURE, RESEED_MSG, MCB_PSOC, failCode,
	                    "**Comm Fail => reseed msg - CM" );
				}
            }
		 
            fnSystemLogEntry( SYSLOG_TEXT, PSOCGENSeedComp , sizeof(PSOCGENSeedComp));
            break;

        case COMSEED_MSG:
            fnSystemLogEntry( SYSLOG_TEXT, PSOCPHComSeed , sizeof(PSOCPHComSeed));
            if (SendHostCmd( PH_PSOC, PSOC_COM_SEED, PSOC_COMSEED_RSP_LEN , rv ) == FAILURE)
            {
                if (SendHostCmd( PH_PSOC, PSOC_COM_SEED,PSOC_COMSEED_RSP_LEN, rv ) == FAILURE)
                {
                    LogPsocError( PSOC_COMM_FATAL, COMSEED_MSG, PH_PSOC, failCode,
                        "**Comm Fatal => commit seed msg - PH" );
                    retVal = PSOC_ERROR;
                    break;
                }
				else
				{
	                LogPsocError( PSOC_COMM_FAILURE, COMSEED_MSG, PH_PSOC, failCode,
	                    "**Comm Fail => commit seed msg - PH" );
				}
            }

            /* must have succeeded with Gen Seed PH PSOC - so do MCB PSOC */
            fnSystemLogEntry( SYSLOG_TEXT, PSOCMCBComSeed , sizeof(PSOCMCBComSeed));
            if (SendHostCmd( MCB_PSOC, PSOC_COM_SEED, PSOC_COMSEED_RSP_LEN , rv ) == FAILURE)
	        {
                if (SendHostCmd( MCB_PSOC, PSOC_COM_SEED, PSOC_COMSEED_RSP_LEN , rv ) == FAILURE)
                {
                    LogPsocError( PSOC_COMM_FATAL, COMSEED_MSG, MCB_PSOC, failCode,
                        "**Comm Fatal => commit seed msg - CM" );
                    retVal = PSOC_ERROR;
                }
				else
				{
	                LogPsocError( PSOC_COMM_FAILURE, COMSEED_MSG, MCB_PSOC, failCode,
	                    "**Comm Fail => commit seed msg - CM" );
				}
	        }
			
            fnSystemLogEntry( SYSLOG_TEXT, PSOCComSeedComp , sizeof(PSOCComSeedComp));
            break;

        case RANDNUM_MSG:
            if (SendHostCmd( PH_PSOC, PSOC_RAND_NUM,PSOC_RAND_NUM_RSP_LEN, rv ) == FAILURE)
	        {
                LogPsocError( PSOC_COMM_FAILURE, RANDNUM_MSG, PH_PSOC, failCode,
                    "**Comm Fail => Random Num msg" );
                retVal = PSOC_ERROR;
            }
            break;
			
        case DESYNC_MSG:
            if (SendHostCmd( PH_PSOC, PSOC_XOR_SEED, PSOC_COMSEED_RSP_LEN, rv ) == FAILURE)
	        {
                LogPsocError( PSOC_COMM_FAILURE, DESYNC_MSG, PH_PSOC, failCode,
                    "**Comm Fail => DeSync msg" );
                retVal = PSOC_ERROR;
            }
            OSWakeAfter( 100 );
            if (SendHostCmd( PH_PSOC, PSOC_GEN_SEED, PSOC_GENSEED_RSP_LEN, rv ) == FAILURE)
            {
                LogPsocError( PSOC_COMM_FATAL, RESEED_MSG, PH_PSOC, failCode,
                    "**Comm Fatal => reseed msg - PH" );
                retVal = PSOC_ERROR;
                break;
            }
            break;

		default:
			break;
    }
    return (retVal);
}


// *************************************************************************/
// *************************************************************************/

unsigned char	CheckImageVerNum( unsigned char psocId )
{
  return (strcmp( (void *)verNum[psocId], (void *)psocImageVNum ));
}

// *************************************************************************/
// *************************************************************************/

char *fnReadPSOCVersion( unsigned char bPh)
{
  char *pVer;

  if(bPh == MCB_PSOC)
    pVer = &verNum[ MCB_PSOC ][0];
  else
    pVer = &verNum[ PH_PSOC ][0];
  return(pVer);
}

// *************************************************************************/
// *************************************************************************/

void PsocSignalExpire( unsigned long dummyArg )
{
  giveUpWaiting = TRUE;
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
// =====================================================================
void fnInitPSOCState( )
{
  HALInitPSOCState(); // No ability to invert serial clock and/or data
  strcpy( &verNum[ MCB_PSOC ][0] , "99.99.9999" );
  strcpy( &verNum[  PH_PSOC ][0] , "99.99.9999" );
}

//=====================================================================
//
//	Function Name:	fnFinalPSOCState
//
//	Description: Sets the state of the port pins after
//		programming is completed
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
// =====================================================================
void fnFinalPSOCState()
{
	HALSetFinalPSOCState();
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================

BOOL fnPSOCVerifySync( BOOL reSync )
{
    BOOL synced = FALSE;
    unsigned char rv = SUCCESSFUL;
	unsigned char ucTryAgain = 0;
    char pPsocErrorMsg[45];
    char semstat;

// Do not do this checking if this semaphore is acquired, which means that the PH is being replaced
// This function does not acquire semaphore to avoid delaying PH replacement or causing problems if somehow semaphore not released
    semstat = OSGetSemaphoreState (PH_PSOC_SEM_ID);
    if (semstat != OS_SEMAPHORE_AVAILABLE)
    {
		dbgTrace(DBG_LVL_ERROR,"Skip Sync Verify - Print Head replacement in progress: status %d\r\n", semstat);
		return TRUE; // tell caller there is no problem
    }

	while (ucTryAgain < 2)
	{
	    rv = SendHostCmd( MCB_PSOC, PSOC_READ_SYNC, PSOC_READ_SYNC_RSP_LEN, PSOCCRSync);
		if (rv != SUCCESSFUL)
		{
//??
		sprintf(pPsocErrorMsg, "MCB Read Sync Err = %02X", rv);
		fnSystemLogEntry( SYSLOG_TEXT, pPsocErrorMsg, strlen(pPsocErrorMsg));
//??
			rv = SendHostCmd( MCB_PSOC, PSOC_READ_SYNC, PSOC_READ_SYNC_RSP_LEN, PSOCCRSync);
			if (rv != SUCCESSFUL)
			{
				LogPsocError( PSOC_CM_SEED_FAILURE, 0, MCB_PSOC, rv, "Cannot get MCB Seed" );
				return (synced);
			}
		}
//??
//		sprintf(pPsocErrorMsg, "MCB Sync = %02X%02X%02X", PSOCCRSync[0], PSOCCRSync[1], PSOCCRSync[2]);
//		fnSystemLogEntry( SYSLOG_TEXT, pPsocErrorMsg, strlen(pPsocErrorMsg));
//??
	
	    rv = SendHostCmd( PH_PSOC, PSOC_READ_SYNC, PSOC_READ_SYNC_RSP_LEN, PSOCPHSync);
		if (rv != SUCCESSFUL)
		{
//??
		sprintf(pPsocErrorMsg, "PH Read Sync Err = %02X", rv);
		fnSystemLogEntry( SYSLOG_TEXT, pPsocErrorMsg, strlen(pPsocErrorMsg));
//??
			rv = SendHostCmd( PH_PSOC, PSOC_READ_SYNC, PSOC_READ_SYNC_RSP_LEN, PSOCPHSync);
			if (rv != SUCCESSFUL)
			{
				// In case PH replacement started after the check at the start of this function, check semaphore again.
				// If semaphore locked, then ignore error because it was caused by PH replacement
			    semstat = OSGetSemaphoreState (PH_PSOC_SEM_ID);
			    if (semstat != OS_SEMAPHORE_AVAILABLE)
			    {
					dbgTrace(DBG_LVL_ERROR,"Ignore Sync Verify PSOC Comm Error - Print Head replacement in progress: status %d\r\n", semstat);
					return TRUE; // tell caller there is no problem
			    }
			    else
			    {
					LogPsocError( PSOC_PH_SEED_FAILURE, 0, PH_PSOC, rv, "Cannot get PH Seed" );
					return (synced);
			    }
			}
		}
//??
//		sprintf(pPsocErrorMsg, "PH Sync = %02X%02X%02X", PSOCPHSync[0], PSOCPHSync[1], PSOCPHSync[2]);
//		fnSystemLogEntry( SYSLOG_TEXT, pPsocErrorMsg, strlen(pPsocErrorMsg));
//??
	
	    if (memcmp( PSOCCRSync, PSOCPHSync, PSOC_READ_SYNC_RSP_LEN ) == 0 )
		{
	        synced = TRUE;
			ucTryAgain = 2;
		}
		else
		{
			if ((reSync == TRUE) && (ucTryAgain == 0))
			{
			    ReseedPsocs( RESEED_MSG );		// GENERATE INITIAL SEED VALUE
			    ReseedPsocs( COMSEED_MSG );		// COMMIT INITIAL SEED VALUE
				ucTryAgain++;
		    }
		 	else
			{
				ucTryAgain = 2;
			}
		}
	}

	if (synced != TRUE)
	{
		// In case PH replacement started after the check at the start of this function, check semaphore again.
		// If semaphore locked, then ignore error because it was caused by PH replacement
	    semstat = OSGetSemaphoreState (PH_PSOC_SEM_ID);
	    if (semstat != OS_SEMAPHORE_AVAILABLE)
	    {
			dbgTrace(DBG_LVL_ERROR,"Ignore Sync Verify Error - Print Head replacement in progress: status %d\r\n", semstat);
			return TRUE; // tell caller there is no problem
	    }
	    else
	    {
			sprintf(pPsocErrorMsg, "MCB Sync = %02X%02X%02X", PSOCCRSync[0], PSOCCRSync[1], PSOCCRSync[2]);
			fnSystemLogEntry( SYSLOG_TEXT, pPsocErrorMsg, strlen(pPsocErrorMsg));

			sprintf(pPsocErrorMsg, "PH Sync = %02X%02X%02X", PSOCPHSync[0], PSOCPHSync[1], PSOCPHSync[2]);
			fnSystemLogEntry( SYSLOG_TEXT, pPsocErrorMsg, strlen(pPsocErrorMsg));

			LogPsocError( PSOC_SEED_MISMATCH, MCB_PSOC, 0, 0, "Sync Mismatch" );
	    }
	}
	
    return(synced);
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================
void fnGetPSOCData()
{
  SendHostCmd( MCB_PSOC , PSOC_RET_SEED  , PSOC_READ_SEED_RSP_LEN , PSOCCRSeed  );
  SendHostCmd( PH_PSOC  , PSOC_RET_SEED  , PSOC_READ_SEED_RSP_LEN , PSOCPHSeed  );
  SendHostCmd( MCB_PSOC , PSOC_READ_SEED , 2 , PSOCCRRand  );
  SendHostCmd( PH_PSOC  , PSOC_READ_SEED , 2 , PSOCPHRand  );
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================
void fnZeroPSOCs()
{
  SendHostCmd( MCB_PSOC , PSOC_ZERO_LFSR  , PSOC_DEBUG_RSP_LEN , &PSOCDebCRResp );
  SendHostCmd( PH_PSOC  , PSOC_ZERO_LFSR  , PSOC_DEBUG_RSP_LEN , &PSOCDebPHResp );
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================
void fnPSOCXorDebug()
{
  SendHostCmd( MCB_PSOC , PSOC_XOR_CONFIG  , PSOC_DEBUG_RSP_LEN , &PSOCDebCRResp );
  SendHostCmd( PH_PSOC  , PSOC_XOR_CONFIG  , PSOC_DEBUG_RSP_LEN , &PSOCDebPHResp );
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================
void fnPSOCPDebug()
{
  SendHostCmd( MCB_PSOC , PSOC_PDEB_CONFIG  , PSOC_DEBUG_RSP_LEN , &PSOCDebCRResp );
  SendHostCmd( PH_PSOC  , PSOC_PDEB_CONFIG  , PSOC_DEBUG_RSP_LEN , &PSOCDebPHResp );
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================
void  fnPSOCNormal()
{
  SendHostCmd( MCB_PSOC , PSOC_NORMAL_CONFIG  , PSOC_DEBUG_RSP_LEN , &PSOCDebCRResp );
  SendHostCmd( PH_PSOC  , PSOC_NORMAL_CONFIG  , PSOC_DEBUG_RSP_LEN , &PSOCDebPHResp );
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================
void  fnPSOCInvert()
{
  SendHostCmd( PH_PSOC  , PSOC_INV_CONFIG  , PSOC_DEBUG_RSP_LEN , &PSOCDebPHResp );
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================
void fnPSOCCRXorDebug()
{
  SendHostCmd( MCB_PSOC , PSOC_XOR_CONFIG  , PSOC_DEBUG_RSP_LEN , &PSOCDebCRResp );
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================
void fnPSOCPHXorDebug()
{
  SendHostCmd( PH_PSOC  , PSOC_XOR_CONFIG  , PSOC_DEBUG_RSP_LEN , &PSOCDebPHResp );
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================
void fnPSOCDoReSeed()
{
  ReseedPsocs( RESEED_MSG );		// GENERATE INITIAL SEED VALUE
  ReseedPsocs( COMSEED_MSG );		// COMMIT INITIAL SEED VALUE
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================
void fnPSOCResetPH()
{
  HALResetLFSR(PH_PSOC);
  OSWakeAfter( 6000 );
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================
void fnPSOCResetCR()
{
  HALResetLFSR(MCB_PSOC);
  OSWakeAfter( 6000 );
}


//=====================================================================
//
//  Function Name:
//
//  Description: Normally this should not be used.  PM will control PH power.
//
//
//  Parameters:
//
//
//
//  Returns:
//
//
//  Preconditions:
//
//
//=====================================================================
 void fnSetPrintHeadPowerState( BOOL powerOn )
{
   HALSetPHPowerState(powerOn);
}

/* **********************************************************************
// FUNCTION NAME:fnPrintHeadPowerOn
// PURPOSE: Return True if PH power is turned on
//
// AUTHOR: George Monroe
//
// INPUTS:
// RETURNS:
// **********************************************************************/
BOOL fnPrintHeadPowerOn()
{
  BOOL fPwrOn = FALSE;

   if(HALGetPHPowerState())
      fPwrOn = TRUE;

  return(fPwrOn);
}

