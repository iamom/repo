#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#define __define_variables_btldr_fram_h__
#include "btldr_fram.h"

#include "crc16.h"
#include "cache.h"



/* --------------------------------------------------------------------------------------------[calc_bldr_info_crc]-- */
uint16_t calc_bldr_info_crc()
{
    uint32_t crc_data_len = (sizeof(Bootloader_Info_t) - offsetof(Bootloader_Info_t, flags));
    return _crc16(0, (char *)&BldrInfo.flags, crc_data_len);
}

/* --------------------------------------------------------------------------------------[is_bldr_info_initialized]-- */
bool is_bldr_info_initialized()
{
    return (BldrInfo.magic_number == BLDR_INFO_AREA);
}
/* ------------------------------------------------------------------------------------------[verify_bldr_info_crc]-- */
bool verify_bldr_info_crc()
{
    uint16_t calc_crc;
    bool verified = false;

    calc_crc = calc_bldr_info_crc();
    verified = (calc_crc == BldrInfo.crc);
    return verified;
}

/* -----------------------------------------------------------------------------------[are_unsigned_images_allowed]-- */
bool are_unsigned_images_allowed()
{
    return (BldrInfo.flags & BI_FLAG_UNSIGNED_IMGS_ALLOWED)!= 0;
}

/* ------------------------------------------------------------------------------------------[bldr_info_update_crc]-- */
void bldr_info_update_crc()
{
    BldrInfo.crc = calc_bldr_info_crc();
    CacheDataCleanBuff((unsigned int)&BldrInfo, sizeof(BldrInfo));
}

/* -------------------------------------------------------------------------------------------[bldr_info_set_flags]-- */
void bldr_info_set_flags(uint16_t flags)
{
    BldrInfo.flags |= flags;
    bldr_info_update_crc();
}

/* -------------------------------------------------------------------------------------------[bldr_info_clr_flags]-- */
void bldr_info_clr_flags(uint16_t flags)
{
    BldrInfo.flags &= ~flags;
    bldr_info_update_crc();
}

/* -----------------------------------------------------------------------------------------[bldr_info_write_flags]-- */
void bldr_info_write_flags(uint16_t flags)
{
    BldrInfo.flags = flags;
    bldr_info_update_crc();
}

/* ----------------------------------------------------------------------------------------------[format_bldr_info]-- */
void format_bldr_info(uint16_t initial_flags)
{
    memset(&BldrInfo, 0, sizeof(BldrInfo));
    BldrInfo.s_version = 1;
    BldrInfo.magic_number = BLDR_INFO_AREA;
    BldrInfo.flags = initial_flags;

    #if KMS_RELEASE == 1
        BldrInfo.build_type = BI_BUILD_TYPE_PROD;
    #else
        BldrInfo.build_type = BI_BUILD_TYPE_QA;
    #endif

    bldr_info_update_crc();
}

/* ----------------------------------------------------------------------------------------[bldr_info_flags_to_str]-- */
char *bldr_info_flags_to_str(char *buf)
{
    sprintf(buf, "%s%s%s%s%s"
                , (BldrInfo.flags & BI_FLAG_ERASE_HCC_FLASH)       ? "ERASE_HCC_FLASH " : " "
                , (BldrInfo.flags & BI_FLAG_FORMAT_HCC_FFS)        ? "FORMAT_HCC_FLASH " : " "
                , (BldrInfo.flags & BI_FLAG_REBOOT_WHEN_DONE)      ? "REBOOT_WHEN_DONE " : " "
                , (BldrInfo.flags & BI_FLAG_PROCESS_INSTALL_DIR)   ? "PROCESS_INSTALL_DIR " : " "
                , (BldrInfo.flags & BI_FLAG_UNSIGNED_IMGS_ALLOWED) ? "UNSIGNED_IMGS_ALLOWED" : " ");

    return buf;
}

/* -----------------------------------------------------------------------------------[bldr_info_build_type_to_str]-- */
char *bldr_info_build_type_to_str(char *buf)
{
    if (BldrInfo.build_type == BI_BUILD_TYPE_QA)
        strcpy(buf, "QA");
    else if (BldrInfo.build_type == BI_BUILD_TYPE_PROD)
        strcpy(buf, "Prod");
    else
        sprintf(buf, "Unknown build type %d", BldrInfo.build_type);
 
    return buf;
}

