/*
* 05-Apr-07 sa002pe on FPHX 01.04 shelton branch
*	Added definition of THISFILE.
*	Changed all calls for exception logging to use THISFILE instead of __FILE__.
*/
#ifndef VSTACK_C
#define VSTACK_C

#ifdef __cplusplus
extern  "C" {                              
#endif
#include <stdlib.h>
#include <string.h>
#include "..\include\exception.h"
#include "..\include\vstack.h"
#ifdef __cplusplus
}                              
#endif


extern NU_MEMORY_POOL  *pSystemMemoryCached;

#define THISFILE "vstack.c"


//Vstack object broker tracking
int vstackTracker[TYPES_OF_VSTACK_OBJECTS];

#ifndef JANUS
BOOL vStackClean(void)
{
int i;
    
    for(i = 0; i < ARRAY_LENGTH(vstackTracker); i++)
    {
        if(vstackTracker[i]) return FALSE;
    }

    return TRUE;
}
#endif




//This should be called with the return value going into the head pointer
//ie: SMT_ENTRY_LINK *headLinkPtr = NULL;
//	  ... 
//	SMT_ENTRY_LINK *newLinkPtr = newSmtEntryLink();
//    ...
//	headLinkPtr = pushSmtEntry(headLinkPtr, newLinkPtr);
//
SMT_ENTRY_LINK *pushSmtEntry(SMT_ENTRY_LINK *headLinkPtr, SMT_ENTRY_LINK *linkToStore)
{
	if(!linkToStore)
		return 0;
	
	linkToStore->nextLinkPtr = headLinkPtr;
	return linkToStore;	
}

//This one adjusts the head link pointer and returns the previous head
SMT_ENTRY_LINK *popSmtEntry(SMT_ENTRY_LINK **headLinkPtr)
{
	SMT_ENTRY_LINK *tmpptr = *headLinkPtr;
	
	if(!(*headLinkPtr)) 
		return 0;
	
	*headLinkPtr = (*headLinkPtr)->nextLinkPtr;

	return tmpptr;
		
}

STATUS newSmtEntryLink(SMT_ENTRY_LINK **theLinkPtr)
{
STATUS status;
	
	//Get the memory for the link
	if((status = NU_Allocate_Memory(pSystemMemoryCached,	(void **)theLinkPtr,
		sizeof(SMT_ENTRY_LINK), NU_NO_SUSPEND)) != NU_SUCCESS) 
		
		return status;
	
 	//Got it. Set it to 0, just to be tidy
 	memset(*theLinkPtr, '\0', sizeof(SMT_ENTRY_LINK));

 	//Now get the memory for the data                                             
	if((status = NU_Allocate_Memory(pSystemMemoryCached,	(void **)&((*theLinkPtr)->smtEntryPtr),
		sizeof(SERVICE_MAP_TABLE_ENTRY_INFO), NU_NO_SUSPEND)) != NU_SUCCESS) 
		
		return status;
 	
 	//Got it. Set it to 0, just to be tidy
 	memset((*theLinkPtr)->smtEntryPtr, '\0', sizeof(SERVICE_MAP_TABLE_ENTRY_INFO));

	//track allocation
    vstackTracker[SMT_ENTRY_LINK_INDEX]++;
	
	return NU_SUCCESS;
}

STATUS delSmtEntryLink(SMT_ENTRY_LINK *theLinkPtr)
{
STATUS status;
	
	//Free the memory for the data
	if((status = NU_Deallocate_Memory(theLinkPtr->smtEntryPtr)) != NU_SUCCESS) 
		return status;
	
	//Free the memory for the link
	if((status = NU_Deallocate_Memory(theLinkPtr)) != NU_SUCCESS) 
		return status;
		
	//track deallocation
    vstackTracker[SMT_ENTRY_LINK_INDEX]--;
	
	return NU_SUCCESS;

}


SRT_ENTRY_LINK *pushSrtEntry(SRT_ENTRY_LINK *headLinkPtr, SRT_ENTRY_LINK *linkToStore)
{
	if(!linkToStore)
		return 0;
	
	linkToStore->nextLinkPtr = headLinkPtr;
	return linkToStore;	
}

//This one adjusts the head link pointer and returns the previous head
SRT_ENTRY_LINK *popSrtEntry(SRT_ENTRY_LINK **headLinkPtr)
{
	SRT_ENTRY_LINK *tmpptr = *headLinkPtr;
	
	if(!(*headLinkPtr)) 
		return 0;
	
	*headLinkPtr = (*headLinkPtr)->nextLinkPtr;

	return tmpptr;
		
}

STATUS newSrtEntryLink(SRT_ENTRY_LINK **theLinkPtr)
{
STATUS status;
	
	//Get the memory for the link
	if((status = NU_Allocate_Memory(pSystemMemoryCached,	(void **)theLinkPtr,
		sizeof(SRT_ENTRY_LINK), NU_NO_SUSPEND)) != NU_SUCCESS) 
		
		return status;
	
 	//Got it. Set it to 0, just to be tidy
 	memset(*theLinkPtr, '\0', sizeof(SRT_ENTRY_LINK));

 	//Now get the memory for the data                                             
	if((status = NU_Allocate_Memory(pSystemMemoryCached,	(void **)&((*theLinkPtr)->srtEntryPtr),
		sizeof(SERVICE_REQUIRED_TABLE_ENTRY_INFO), NU_NO_SUSPEND)) != NU_SUCCESS) 
		
		return status;
 	
 	//Got it. Set it to 0, just to be tidy
 	memset((*theLinkPtr)->srtEntryPtr, '\0', sizeof(SERVICE_REQUIRED_TABLE_ENTRY_INFO));

	//track allocation
    vstackTracker[SRT_ENTRY_LINK_INDEX]++;
	
	return NU_SUCCESS;
}

STATUS delSrtEntryLink(SRT_ENTRY_LINK *theLinkPtr)
{
STATUS status;
	
	//Free the memory for the data
	if((status = NU_Deallocate_Memory(theLinkPtr->srtEntryPtr)) != NU_SUCCESS) 
		return status;
	
	//Free the memory for the link
	if((status = NU_Deallocate_Memory(theLinkPtr)) != NU_SUCCESS) 
		return status;
		
	//track deallocation
    vstackTracker[SRT_ENTRY_LINK_INDEX]--;
	
	return NU_SUCCESS;
}


DEVICE_UPDATE_LINK *newDeviceUpdateLink(void)
{
//Grab some memory
DEVICE_UPDATE_LINK *linkPtr = (DEVICE_UPDATE_LINK *)malloc(sizeof(DEVICE_UPDATE_LINK));   
    
	if(linkPtr)	//Got the mem, so initialize it
		memset(linkPtr, '\0', sizeof(DEVICE_UPDATE_LINK));   //clear everything

	//track allocation
    vstackTracker[DEVICE_UPDATE_LINK_INDEX]++;
    
    return linkPtr;
}

DEVICE_INFO	*newDeviceInfo(void)
{
//Grab some memory
DEVICE_INFO *deviceInfoPtr = (DEVICE_INFO *)malloc(sizeof(DEVICE_INFO));   
    
	if(deviceInfoPtr)	//Got the mem, so initialize it
		memset(deviceInfoPtr, '\0', sizeof(DEVICE_INFO));   //clear everything

	//track allocation
    vstackTracker[DEVICE_INFO_INDEX]++;
    
    return deviceInfoPtr;

}

STATUS addDeviceInfo(DEVICE_INFO *deviceInfoToAdd, DEVICE_UPDATE_LINK *deviceUpdateLink)
{
    if(!deviceInfoToAdd || !deviceUpdateLink)
        taskException("NULL Pointer", THISFILE, __LINE__);
        	
	deviceInfoToAdd->nextDeviceInfoPtr = deviceUpdateLink->firstDeviceInfoPtr;
    deviceUpdateLink->firstDeviceInfoPtr = deviceInfoToAdd;
	
	return NU_SUCCESS;	
}

STATUS delDeviceInfo(DEVICE_INFO *deviceInfoToDelete)
{
    if(!deviceInfoToDelete)
        taskException("NULL Pointer", THISFILE, __LINE__);
        	
    if(NU_Deallocate_Memory(deviceInfoToDelete))
        taskException("Invalid Pointer", THISFILE, __LINE__);

	//track deallocation
    vstackTracker[DEVICE_INFO_INDEX]--;
	
	return NU_SUCCESS;	
}


DEVICE_UPDATE_LINK *pushDeviceUpdate(DEVICE_UPDATE_LINK *headLinkPtr, DEVICE_UPDATE_LINK *linkToStore)
{
    if(!linkToStore)
        taskException("NULL Pointer", THISFILE, __LINE__);
	
	linkToStore->nextDeviceUpdateLinkPtr = headLinkPtr;
	return linkToStore;	

}


DEVICE_UPDATE_LINK *popDeviceUpdate(DEVICE_UPDATE_LINK **headLinkPtr)
{
    DEVICE_UPDATE_LINK *tmpptr = *headLinkPtr;
	
	if(!(*headLinkPtr)) 
		return 0;

	*headLinkPtr = (*headLinkPtr)->nextDeviceUpdateLinkPtr;

	return tmpptr;
}

STATUS delDeviceUpdateLink(DEVICE_UPDATE_LINK *theLinkPtr)
{
    //First free all device info structures
    while(theLinkPtr->firstDeviceInfoPtr)
    {
    DEVICE_INFO *tempPtr = theLinkPtr->firstDeviceInfoPtr;

        theLinkPtr->firstDeviceInfoPtr = theLinkPtr->firstDeviceInfoPtr->nextDeviceInfoPtr;
    
        if(NU_Deallocate_Memory(tempPtr))
            taskException("Invalid Pointer", THISFILE, __LINE__);

	    //track deallocation
        vstackTracker[DEVICE_INFO_INDEX]--;
    }

    //Now free the link
    if(NU_Deallocate_Memory(theLinkPtr))
        taskException("Invalid Pointer", THISFILE, __LINE__);

	//track deallocation
    vstackTracker[DEVICE_UPDATE_LINK_INDEX]--;

    return NU_SUCCESS;
}

STATUS delDeviceUpdateLinkOnly(DEVICE_UPDATE_LINK *theLinkPtr)
{
    //free just the link
    if(NU_Deallocate_Memory(theLinkPtr))
        taskException("Invalid Pointer", THISFILE, __LINE__);

	//track deallocation
    vstackTracker[DEVICE_UPDATE_LINK_INDEX]--;

    return NU_SUCCESS;
}

STATUS delDeviceUpdateChain(DEVICE_UPDATE_LINK *theLinkPtr)
{
    while(theLinkPtr)
    {
    DEVICE_UPDATE_LINK *tmpPtr = theLinkPtr;    
    
        theLinkPtr = theLinkPtr->nextDeviceUpdateLinkPtr;

        delDeviceUpdateLink(tmpPtr);
    }    
    
    return NU_SUCCESS;
}

FILE_UPDATE_LINK *newFileUpdateLink(void)
{
//Grab some memory
FILE_UPDATE_LINK *linkPtr = (FILE_UPDATE_LINK *)malloc(sizeof(FILE_UPDATE_LINK));   
    
	if(linkPtr)	//Got the mem, so initialize it
		memset(linkPtr, '\0', sizeof(FILE_UPDATE_LINK));   //clear everything

	//track allocation
    vstackTracker[FILE_UPDATE_LINK_INDEX]++;
    
    return linkPtr;

}

FILE_INFO *newFileInfo(void)
{
//Grab some memory
FILE_INFO *fileInfoPtr = (FILE_INFO *)malloc(sizeof(FILE_INFO));   
    
	if(fileInfoPtr)	//Got the mem, so initialize it
		memset(fileInfoPtr, '\0', sizeof(FILE_INFO));   //clear everything

	//track allocation
    vstackTracker[FILE_INFO_INDEX]++;
    
    return fileInfoPtr;
}

STATUS addFileInfo(FILE_INFO *fileInfoToAdd, FILE_UPDATE_LINK *fileUpdateLink)
{
    if(!fileInfoToAdd || !fileUpdateLink)
        taskException("NULL Pointer", THISFILE, __LINE__);
        	
	fileInfoToAdd->nextFileInfoPtr = fileUpdateLink->firstFileInfoPtr;
    fileUpdateLink->firstFileInfoPtr = fileInfoToAdd;
	
	return NU_SUCCESS;	

}

STATUS delFileInfo(FILE_INFO *fileInfoToDelete)
{
    if(!fileInfoToDelete)
        taskException("NULL Pointer", THISFILE, __LINE__);
        	
    if(NU_Deallocate_Memory(fileInfoToDelete))
        taskException("Invalid Pointer", THISFILE, __LINE__);

	//track deallocation
    vstackTracker[FILE_INFO_INDEX]--;
	
	return NU_SUCCESS;	
}

FILE_UPDATE_LINK *pushFileUpdate(FILE_UPDATE_LINK *headLinkPtr, FILE_UPDATE_LINK *linkToStore)
{
    if(!linkToStore)
        taskException("NULL Pointer", THISFILE, __LINE__);
	
	linkToStore->nextFileUpdateLinkPtr = headLinkPtr;
	return linkToStore;	
}

FILE_UPDATE_LINK *popFileUpdate(FILE_UPDATE_LINK **headLinkPtr)
{
    FILE_UPDATE_LINK *tmpptr = *headLinkPtr;
	
	if(!(*headLinkPtr)) 
		return 0;

	*headLinkPtr = (*headLinkPtr)->nextFileUpdateLinkPtr;

	return tmpptr;
}

STATUS delFileUpdateLink(FILE_UPDATE_LINK *theLinkPtr)
{
    //First free all file info structures
    while(theLinkPtr->firstFileInfoPtr)
    {
    FILE_INFO *tempPtr = theLinkPtr->firstFileInfoPtr;

        theLinkPtr->firstFileInfoPtr = theLinkPtr->firstFileInfoPtr->nextFileInfoPtr;
    
        if(NU_Deallocate_Memory(tempPtr))
            taskException("Invalid Pointer", THISFILE, __LINE__);

	    //track deallocation
        vstackTracker[FILE_INFO_INDEX]--;
        
    }

    //Now free the link
    if(NU_Deallocate_Memory(theLinkPtr))
        taskException("Invalid Pointer", THISFILE, __LINE__);

	//track deallocation
    vstackTracker[FILE_UPDATE_LINK_INDEX]--;

    return NU_SUCCESS;
}

STATUS delFileUpdateLinkOnly(FILE_UPDATE_LINK *theLinkPtr)
{
    //free just the link
    if(NU_Deallocate_Memory(theLinkPtr))
        taskException("Invalid Pointer", THISFILE, __LINE__);

	//track deallocation
    vstackTracker[FILE_UPDATE_LINK_INDEX]--;

    return NU_SUCCESS;
}

STATUS delFileUpdateChain(FILE_UPDATE_LINK *theLinkPtr)
{
    while(theLinkPtr)
    {
    FILE_UPDATE_LINK *tmpPtr = theLinkPtr;    
    
        theLinkPtr = theLinkPtr->nextFileUpdateLinkPtr;

        delFileUpdateLink(tmpPtr);
    }    
    
    return NU_SUCCESS;
}

COMMON_UPDATE_INFO *newGenericUpdateInfo(void)
{
//Grab some memory
COMMON_UPDATE_INFO *infoPtr = (COMMON_UPDATE_INFO *)malloc(sizeof(COMMON_UPDATE_INFO));   
    
	if(!infoPtr)	
	    throwException("Out of memory")	;

	memset(infoPtr, '\0', sizeof(COMMON_UPDATE_INFO));   //clear everything

	//track allocation
    vstackTracker[COMMON_UPDATE_INFO_INDEX]++;
    
    return infoPtr;
}

STATUS addGenericUpdateInfo(COMMON_UPDATE_INFO *GenericUpdateInfoToAdd, COMMON_UPDATE_INFO **baseGenericUpdateInfoPtr)
{
    if(!(*baseGenericUpdateInfoPtr))  //starting a new list?
    {
        *baseGenericUpdateInfoPtr = GenericUpdateInfoToAdd;
    }
    else
    {
    COMMON_UPDATE_INFO *tempPtr = *baseGenericUpdateInfoPtr;

        while(tempPtr->nextCommonUpdateInfoPtr)    //iterate until end of list...
            tempPtr = tempPtr->nextCommonUpdateInfoPtr;

        GenericUpdateInfoToAdd->prevCommonUpdateInfoPtr = tempPtr; //link back
        tempPtr->nextCommonUpdateInfoPtr = GenericUpdateInfoToAdd; //link forward
    }
    
    return NU_SUCCESS;
}

STATUS delGenericUpdateInfoChain(COMMON_UPDATE_INFO **baseGenericUpdateInfoPtr)
{
    if(*baseGenericUpdateInfoPtr)
    {
    COMMON_UPDATE_INFO *tempPtr = *baseGenericUpdateInfoPtr;

        while(tempPtr)
        {
        COMMON_UPDATE_INFO *tempPtr2 = tempPtr;

            tempPtr = tempPtr->nextCommonUpdateInfoPtr;

            if(NU_Deallocate_Memory(tempPtr2))
                taskException("Invalid Pointer", THISFILE, __LINE__);

	        //track allocation
            vstackTracker[COMMON_UPDATE_INFO_INDEX]--;
        }

        *baseGenericUpdateInfoPtr = NULL;
    }

    return NU_SUCCESS;
}










/* //this creates a new link node                                                    */
/* smtentrylink *newsmtentrylink(void)                                               */
/* {                                                                                 */
/* 	tmpptr = malloc(sizeof(smtentrylink);                                            */
/* 	if(tmpptr)	//allocate ok?                                                       */
/* 		{                                                                            */
/* 		memset(tmpptr, '\0', sizeof(smtentrylink))                                   */
/* 		tmpptr->smtentry = malloc(sizeof(smtentry);	  //need memory for the data too */
/* 		}                                                                            */
/* 	                                                                                 */
/* 	return tmpptr->smtentry) ? tmpptr : NULL;		 //if that allocate succeeded... */
/* }                                                                                 */
/*                                                                                   */
/* delsmtentrylink(smtentrylink *theptr)                                             */
/* {                                                                                 */
/* //first deference the data ptr and free it                                        */
/* //then free the node itself                                                       */
/* }                                                                                 */







#endif
