/************************************************************************
   PROJECT:        Horizon CSD
   COMPANY:        Pitney Bowes
   AUTHOR :        George Monroe
   MODULE NAME:    grapfile.c
       
   DESCRIPTION:
----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
----------------------------------------------------------------------
*************************************************************************/

// This file, and aiservic.c make up the "graphics module".
#define GRFX_MODULE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "posix.h"
#include "aiservic.h"
#include "bob.h"
#include "datdict.h"
#include "errcode.h"
#include "fcntl.h"
#include "fnames.h"
#include "fwrapper.h"
#include "global.h"
#include "grapfile.h"
#include "nucleus.h"
#include "unistring.h"  // unistrcmp()
#include "utils.h"
#include "lcd.h"
#include "flashutil.h"
#include "dir_defs.h"
#include "pcdisk.h"
#include "download.h"

//---------------------------------------------------------------------
//      Definitions to help with future transitions:
//---------------------------------------------------------------------

// In the current schema, the graphics directory is copied to ram
//  and the graphics blob is also copied to ram.

#define READ_DIRENTRIES_FROM_RAM
#define READ_GRAPHICS_FROM_RAM

//---------------------------------------------------------------------
// The values for BP_MAX_AD_SLOGAN and BP_MAX_INSC should be set to 0 if
//  they are only limited by the size and directory of the .GAR file.
// But today, 0 means 0.  So we are keeping it because there needs to be
//  meetings before any of this can be fixed.

//#define EMD_ZERO_MEANS_UNLIMITED
#define EMD_ZERO_MEANS_ZERO


//---------------------------------------------------------------------
//      Local Typedef:
//---------------------------------------------------------------------

// These are functions that find the position of something in the graphics file.
typedef int (*T_pfnGFFindPos)( INT fcb, UINT16 wDirInx, int *posDest );



//---------------------------------------------------------------------
//      Local MACROS:
//---------------------------------------------------------------------

// For fixing bad GenIds in grapfile link headers:
#define LNKHDRSIZE          COMPONENTGRAPHIC_OFFSET


#define GRFX_CLNUP_DELETE_ENTRY     0xFF
// Bit maps:
#define GRFX_CLNUP_WRITE_LNKHDR     0x01
#define GRFX_CLNUP_WRITE_DIRENTRY   0x02



//---------------------------------------------------------------------
//      External Function Declarations:
//---------------------------------------------------------------------

extern int UpdateBaseInstallLog(char *filename, char *installStatus);
extern  BOOL fnVerifyUpdateDownload( char * );

//---------------------------------------------------------------------
//      External Variable Declarations:
//---------------------------------------------------------------------

// From custdat.c:
extern unsigned short  uwCMOSGFNeedsCmpxn;


//---------------------------------------------------------------------
//      Local Variables:
//---------------------------------------------------------------------

static GFRESIDENT grfxFileInfo;
static GFRESIDENT * const pGFInfo = &grfxFileInfo;

// For debugging:
UINT16  GrfxCleanupVer = 0;

//---------------------------------------------------------------------
//      Prototypes for Local Functions:
//---------------------------------------------------------------------

static int  	fnGFVerifyVersionDirInx( INT32 fd, UINT16 wThisDirInx );
static int  	fnGFCalcDirEntryOffset( INT ignore, UINT16 wDirInx, int *posDest );
static int  	fnGFFindGraphicInGF( INT32 fd, UINT16 wDirInx, int *posDest );
static int  	fnGFWriteOneThingToGF( UINT16 wDirInx, char *pSrc, size_t zSize, T_pfnGFFindPos pFindPosFunc );
static int  	fnGFWriteOneDirEntry( UINT16 wDirInx, GFENT *pDirEntrySrc );
static int  	fnGFWriteOneGrapHeader( UINT16 wDirInx, UINT8 *LnkPtrSrc );

static GFENT 	*fnGFGetRamCopyDirEntry( UINT16 wDirInx );
static void 	fnGFRamSetStatOne( GFENT *pDirEntry, UINT16 uwFlagTest );
static void 	fnGFRamDeleteOne( GFENT *pDirEntry );

static int 		fnGFReadInGraphicsFile(GFRESIDENT *resident, UINT32 extraSpace );

int         	fnGFGrowGraphicFile( INT32 fd,  int incr );
static  UINT16  fnGFGetDirLen( void );
static  GFENT * fnGFGetFirstDirPtr( void );
static  UINT8 * fnGFGetLnkPtrByDirInx( UINT16 wThisDirInx );
static int 		fnGFGrfxCleanupOne( GFENT *pDirEntry, UINT8 **ppImageWithLnk, UINT8 *pModifiedFlag );

int 			fnGFGrfxCleanup( void );

static int		fnGFCopyCustGraphics( INT32 fd2, char *fname );

static void 	fnGFJustLogIt( int iErrorCode, UINT8 bWhere, UINT8 bInfo1, UINT8 bInfo2 );
BOOL fnGFAnyRoomAtTheInnPrivate( UINT8 bCompType,  UINT16 *pDirInx, BOOL fLimitedByEmd );
BOOL installGfxFiles(char * pFileName);
unsigned char validateGfxFiles(char *pFileName);


/**********************************************************************************************
 * getResidentGFGrfx
 *
 * return pointer to the memoried GAR structure
 *********************************************************************************************/
GFRESIDENT *getResidentGFGrfx()
{
	return pGFInfo;
}

/**********************************************************************************************
 * cleanGFGrfxFileInfo
 *
 * the existing code has a problem at startup, this function creates a specific initialization point
 **********************************************************************************************/
void cleanGFGrfxFileInfo()
{
	memset(&grfxFileInfo, 0, sizeof(GFRESIDENT));
}


  
/* **********************************************************************
// FUNCTION NAME:fnGFAddGraphic( INT32 fd , char *buffer , GFENT *pDirEntry )
// PURPOSE:         
//      Adds a graphic to the RAM version of GAR file.
//      1. Finds the first unused directory entry.
//      2. Finds the end, and records its "position" (offset).
//      3. Appends the graphic to the end. 
//      4. Copies the offset into the directory entry (arg) in ram.
//      5. Copies the ram directory entry over the unused one.
//
// AUTHOR: George Monroe
//
// INPUTS:  
//  buffer      - Pointer to a buffer containing the graphic, with a link 
//                header.
//  pDirEntry   - Pointer to temporary directory entry in Ram.  It has all the 
//                correct info in it already, except for the offset.
//         
// RETURNS:     Error Code  (defined in grapfile.h: GFE_...)
//        
// **********************************************************************/
static int fnGFAddGraphicInMemory(UINT8 *buffer, GFENT *pDirEntry  )
{
  int       ec = 0;
  UINT16    wDirInx = 0;
  UINT32    pos;
  UINT32    gpos;
  UINT8    *pAddr;

  if(pGFInfo->rgfExtraSpace < pDirEntry->gfSize) 
  {
     // Not enough space for the image
	 ec = GFE_MALLOCFAILED;
  }
  else if (fnGFAnyRoomAtTheInnPrivate( pDirEntry->gfType, &wDirInx, TRUE ) == FALSE)
  {
	 //We have a failure to find an empty entry
	  ec = GFE_FILEDIRFULL;
  }
  else
  {
	  gpos = pGFInfo->rgfCurrSize;
	  pAddr = pGFInfo->allocAddr;
	  pAddr += gpos;
	  memcpy(pAddr, buffer, pDirEntry->gfSize);
	  pGFInfo->rgfCurrSize += pDirEntry->gfSize;
	  pGFInfo->rgfExtraSpace -= pDirEntry->gfSize;
	  gpos += sizeof(GFHDR); //graphic offset is from the beginning of the file so we need to include the header here
	  pDirEntry->gfOffset = gpos;
	  pos = wDirInx * sizeof(GFENT);
	  pAddr = pGFInfo->allocAddr;
	  pAddr += pos;
	  memcpy(pAddr, pDirEntry, sizeof(GFENT));
	  pGFInfo->fsUpdated = TRUE;
  }
 
  if( ec )
    fnGFJustLogIt( ec, GFW_ADDGRAPHIC, 0, 0 );
  
  return( ec );
}

  
// ********************************************************************************
// FUNCTION NAME: fnGFCalcDirEntryOffset()                               
// PURPOSE:     Get the position of the Directory Entry in the Graphics File.
//
// AUTHOR:      C.Bellamy
// INPUTS:      
//      fcb - handle of graphics file that has already been opened.
//      wDirInx - Index into directory of entry to find.
//      posDest - Pointer to place to store the position when the entry is found.
//
// OUTPUTS:     ec = Error code from Reading/Writing file.
//
// NOTES:   
//      The return value type and argument list is set because the function ptr
//      is passed, and that is the format rquired by some of the functions.
///
//---------------------------------------------------------------------
static int fnGFCalcDirEntryOffset( INT ignore, UINT16 wDirInx, int *posDest )
{
    int       ec = 0;
    int       len = 0;

    len = sizeof( GFHDR ) + (wDirInx * sizeof( GFENT ));    
    *posDest = len;
    return( ec );
}

// ********************************************************************************
// FUNCTION NAME: fnGFFindGraphicInGF()                             
// PURPOSE:     Get the position of the graphic (starting with the link header)
//              in the Graphics File.
//
// AUTHOR:      C.Bellamy
// INPUTS:      
//      pFcb - Pointer to handle of graphics file that has already been opened.
//      wDirInx - Index into directory of entry of the graphic to find.
//      posDest - Pointer to place to store the position when the entry is found.
//
// OUTPUTS:     ec = Error code from Reading/Writing file.
//
// NOTES:   
//  The file must be successfully opened before this is called, and the 
//    calling function MUST pass valid posDest, wDirInx, and pFcb, as these
//    are NOT checked here.
//  This reads the data from the file, and does not rely on data in pGFInfo,
//    in case we are NOT looking in the .GAR file (perhaps the .GAU?)
//
//---------------------------------------------------------------------
static int fnGFFindGraphicInGF( INT32 fd, UINT16 wThisDirInx, int *posDest )
{
    int     ec = 0;
    GFENT   rEntry;
    unsigned    distance = 0;
    int     pos;
    int     len;

    // Get position of directory entry.
    (void)fnGFCalcDirEntryOffset( 0, wThisDirInx, &pos );
    // position to the start of the dir Entry
    len = NU_Seek( fd, pos , PSEEK_SET);
    if( len >= 0 )
    {
        // Read in the dir entry to get the offset.
        len = NU_Read( fd, (char *)&rEntry, sizeof( GFENT ) );
        if( len == sizeof( GFENT ) )
        {
            if( rEntry.gfID != UNUSEDGFENTRYID )
                distance = rEntry.gfOffset;
            else
                ec = GFE_ENTRYNOTUSED;
        }
        else // Can't read the entire directory entry.
            ec =  GFE_INVALIDFILEFORMAT;
    }
    else
        ec = GFE_FILESEEKERROR;        

    *posDest = distance;

    if (len < 0)
        fnCheckFileSysCorruption(len);
        
    return( ec );
}

// ********************************************************************************
// FUNCTION NAME: fnGFVerifyVersionDirInx()                             
// PURPOSE:     Read the header, and verify the version and that the directory
//          index is in range for this file.
//
// AUTHOR:      C.Bellamy
// INPUTS:      
//      pFcb - Pointer to handle of graphics file that has already been opened.
//      wDirInx - Index into directory of entry to find.
//
// OUTPUTS:     ec = Error code.  0 = no errors.
//
// NOTES:   
//  The file must be successfully opened before this is called, and the 
//    calling function MUST pass valid wDirInx, and pFcb, as these
//    are NOT checked here.
//  This reads the data from the file, and does not rely on data in pGFInfo,
//    in case we are NOT looking in the .GAR file (perhaps the .GAU?)
///
//---------------------------------------------------------------------
static int fnGFVerifyVersionDirInx( INT32 fd, UINT16 wThisDirInx )
{
    int       ec = 0;
    int       len = 0;
    GFHDR     hdr;

    // Position to the start if the file
    len = NU_Seek( fd , 0L, PSEEK_SET );
    if( len >= 0 )
    {
        // Read the header.
        len = NU_Read( fd, (char *)&hdr, sizeof(hdr) );
        if( len == sizeof(hdr) )
        {
            // Verify version and that DirInx is in range.
            if( hdr.gfVersion == GRAPHIC_FILE_VERSION )
            {                                   
                if( wThisDirInx >= hdr.gfDirSize )
                    ec = GFE_DIRINXOUTOFRANGE;
            }
            else    // Wrong Version 
                ec = GFE_VERSNOTSUPPORTED;
        }
        else  // Not enough bytes in file for header.
            ec = GFE_INVALIDFILEFORMAT;            
    }
    else
        ec = GFE_FILESEEKERROR;

    if(len < 0)
        fnCheckFileSysCorruption(len);
    
    return( ec );
}



// ********************************************************************************
// FUNCTION NAME: fnGFWriteOneThingToGF()                               
// PURPOSE:     Copies data into the file.
//
// AUTHOR:      C.Bellamy
// INPUTS:      
//      wDirInx - Index into directory of entry/graphic we are interested in.
//      pSrc - Pointer to data in ram that we want to write into the graphics file.
//      zSize - Size of the data we want to write into the graphics file.
//      pFindPosFunc - Pointer to a function that finds the write place to write 
//               the data.
//
// OUTPUTS:     ec = Error code from Reading/Writing file.
//
// NOTES:   
//
//---------------------------------------------------------------------
static int fnGFWriteOneThingToGF( UINT16 wDirInx, char *pSrc, size_t zSize, T_pfnGFFindPos pFindPosFunc )
{
  int       ec = 0;
  int       rc = 0;
  char fname[64];
  UINT8 *   sp;
  int       len = 0;
  int       pos;
  INT32		fd;

  // Make sure we have a valid file system pointer, and have initialized the RGF
  if( (pGFInfo->fsInit == TRUE) )
  {
      // Make sure the inputs are valid.
      if(     pSrc  &&  zSize  &&  pFindPosFunc  
          &&  (wDirInx < pGFInfo->rgfHdr.gfDirSize) )
      {
          // Find and open the .GAR file.
          sp = fnFindFirstFileWithExt( GFFILENAMEEXT , (UINT8 *)fname );
          if( sp )
          {
              fd = NU_Open(fname, PO_RDWR, PS_IWRITE | PS_IREAD);
			  if(fd >= 0)
              {
                  // Verify the index and the file version.
                  ec = fnGFVerifyVersionDirInx( fd, wDirInx );
                  if( ec == 0 )
                  {
                      // Get the correct position in the Graphics File
                      ec = pFindPosFunc( fd, wDirInx, &pos );
                      if( ec == 0 )
                      {
                          // Seek to the position in the graphic file.
                          len = NU_Seek( fd, pos , PSEEK_SET);
                          if( len >= 0 )
                          {
                              // Copy the data from ram to the graphics file.
                              len = NU_Write( fd, pSrc, zSize );
                              if( len  != zSize )
                              {
                                  ec = GFE_FILEWRITEERROR;
                                  if( len < 0 )
                                  {
                                    ec = len;
                                    fnCheckFileSysCorruption(len);
                                  }
                              }
                          }
                          else
                          {
                              ec = GFE_FILESEEKERROR; 
                              fnCheckFileSysCorruption(len);
                          }
                      }                  
                  }

                  // Whether we were successfull or not, close the file.
                  rc = NU_Close( fd );
                  if( rc < 0)
                    fnCheckFileSysCorruption(rc);
                  if( ec == 0 )
                      ec = rc;                        
              }
              else
              {
                  ec = GFE_NOGRAPHICSFILE;
                  fnCheckFileSysCorruption(fd);
              }
          }
          else
          {
              ec = GFE_NOGRAPHICSFILE;
              uwCMOSGFNeedsCmpxn = 0;
          }
      }
      else 
          ec =  GFE_BADARG;              
  }
  else 
      ec = GFE_GFINFONOTINITED;              

  return( ec );      
}

// ********************************************************************************
// FUNCTION NAME: fnGFWriteOneDirEntry()                                
// PURPOSE:     Copies a directory entry into the file.
//
// AUTHOR:      C.Bellamy
// INPUTS:      
//      wDirInx - Index into directory of entry to copy over.
//      pDirEntrySrc - Pointer to Directory Entry to copy from.
//
// OUTPUTS:     ec = Error code from Reading/Writing file.
//
// NOTES:   
//
//---------------------------------------------------------------------
static int fnGFWriteOneDirEntry( UINT16 wDirInx, GFENT *pDirEntrySrc )
{
    int       ec = 0;

    // Pass the directory index, ptr to the data to copy, the size of the data to copy,
    //  and ptr to the function that will find the position of this directory entry in 
    //  the graphics file.
    ec = fnGFWriteOneThingToGF( wDirInx, (char *)pDirEntrySrc, sizeof( GFENT ), fnGFCalcDirEntryOffset );
    
    if( ec )
        fnGFJustLogIt( ec, GFW_WRTONEDIRENT, 0, (UINT8)wDirInx );

    return( ec );
}


// ********************************************************************************
// FUNCTION NAME: fnGFWriteOneGrapHeader()                              
// PURPOSE:     Copies a graphic header into the file.
//
// AUTHOR:      C.Bellamy
// INPUTS:      
//      wDirInx - Index into directory of entry to copy over.
//      pDirEntrySrc - Pointer to Directory Entry to copy from.
//
// OUTPUTS:     ec = Error code from Reading/Writing FFfile.
//
// NOTES:   
//
//---------------------------------------------------------------------
static int fnGFWriteOneGrapHeader( UINT16 wDirInx, UINT8 *LnkPtrSrc )
{
    int       ec = 0;

    ec = fnGFWriteOneThingToGF( wDirInx, (char *)LnkPtrSrc, LNKHDRSIZE, fnGFFindGraphicInGF );

    if( ec )
        fnGFJustLogIt( ec, GFW_WRTONEGRHDR, 0, 0 );

    return( ec );
}




/* **********************************************************************
// FUNCTION NAME: fnGFGrowGraphicFile( INT32 fd , int incr )
// PURPOSE:     Increase the number of entries in the graphics file by 
//          x entries.  The graphics data is all shifted out to make room
//          for the new entries.
//
// AUTHOR: George Monroe
//
// INPUTS: 
//      fd		File handle.
//      incr    The number of entries by which to increase the directory size.
//         
// RETURNS:     Error Code: 0 = No Errors.
//
// NOTES:   Not called by anything yet.  
//
//      The way it is currently written, this will create a NEW file, with a 
//      temporary name.  The new file will have the new directory size.  
//      The header will be copied from the existing file to the new file.  
//      Then each directory entry will be copied, with its offset adjusted by 
//      the correct number of bytes.  Then the new directory entries will be 
//      initialized.  Then the graphics data will be copied. Then the new file
//      will be closed and renamed to a GROW name.  (This is checked for on 
//      power up, in case a power fail happens during the process.)  
//      Finally, the old file is deleted, and the GROW file is renamed.
//        
// **********************************************************************/
int fnGFGrowGraphicFile(INT32 fd ,  int incr )
{
  GFHDR     hdr;
  GFENT     ent;
  INT32     fd2;
  int   	ec = 0,rc = 0,len,i,oldSize,wlen;
  ulong     hdrDelta;
  char      buf[256];

  // Read The header in the graphics file to determine the current size of
  // the directory. Determine the new size of the directory in bytes. 
  // When the new graphic directory entries are written we adjust the offsets
  // of each graphic current entry to account for the fact the graphic
  // will move deeper into the file because of the increase in directory size
  // computing the new offsets at the time we write the new directory allows
  // us to make a single pass to create the new graphics file
  // position to the start if the file

  len = NU_Seek( fd , 0L, SEEK_SET );
  if( len >= 0 )
  {
    // read the header
    len = NU_Read( fd, (char *)&hdr, sizeof(hdr) );
    if( len == sizeof(hdr) )
    {
		if( hdr.gfVersion == GRAPHIC_FILE_VERSION )
		{
          // we know we are probably using the internal file system
          // therefore we generate the file will only writting sequencially
          // rather than try to perform random I/O
          fd2 = NU_Open(GFTEMPNAME , PO_CREAT|PO_WRONLY|PO_TRUNC , PS_IWRITE | PS_IREAD);
          if( fd2 >= 0 )
          { 
            // old directory size
            oldSize = hdr.gfDirSize;
            hdr.gfDirSize += incr;
            len = NU_Write( fd2, (char *)&hdr, sizeof(GFHDR) );
            if( len == sizeof(GFHDR) )
            {
              // number of bytes each graphic will move due to the increase in directory size
              hdrDelta = incr * sizeof(ent);
              // write out each entry adjusting the positional offset
              for( i = 0 ; i < oldSize ; i++ )
              {
                // read the directory entry
                len = NU_Read( fd, (char *)&ent, sizeof(ent) );
                // if it is a valid entry adjust the offset and write it to the new file
                if( len == sizeof(ent) )
                {
                  if( ent.gfID != UNUSEDGFENTRYID )
                  {
                    ent.gfOffset += hdrDelta; 
                  }
                  else
                  {
                    ent.gfID = UNUSEDGFENTRYID;
                  }
                  // write the new directory entry
                  len = NU_Write( fd2, (char *)&ent, sizeof(GFENT) );
                  if( len != sizeof(GFENT) )
                  {
                    fnCheckFileSysCorruption(len);
                    ec = GFE_FILEWRITEERROR;
                    if( len < 0 )
                        ec = len;
                    break;
                  }
                }
              }
              // if everything is okay so far write the new free entries
              if( ec == 0 )
              {
                ent.gfID = UNUSEDGFENTRYID;
                // write out each new entry
                for( i = 0 ; i < incr ; i++ )
                {
                  len = NU_Write( fd2, (char *)&ent, sizeof(GFENT) );
                  if( len != sizeof(GFENT) )
                  {
                    fnCheckFileSysCorruption(len);
                    ec = GFE_FILEWRITEERROR;
                    if( len < 0 )
                        ec = len;
                    break;
                  }
                }
                if( ec == 0 )
                {
                  // last is to write the contents of the graphic images
                  while(1)
                  {
                    // copy the graphic image contents to the new file
                    len = NU_Read( fd, buf, sizeof(buf) );
                    if( len > 0 )
                    {
                      wlen = NU_Write( fd2, buf, len );
                      if( wlen != len )
                      {
                        fnCheckFileSysCorruption(wlen);
                        ec = GFE_FILEWRITEERROR;
                        if( len < 0 )
                            ec = len;
                        break;
                      }
                    }
                    else
                    {
                        fnCheckFileSysCorruption(len);
                        break;
                    }
                  }
                }
                else
                    ec = GFE_FILEWRITEERROR;
              }
              else
                ec = GFE_FILEWRITEERROR;
            }
            else
            {
                fnCheckFileSysCorruption(len);
                ec = GFE_FILEWRITEERROR;
            }
            
            rc = NU_Close( fd2 );
            fnCheckFileSysCorruption(rc);
            if( ec == 0 )
            {
              if( rc == 0 )
              {
              	//TODO - check protection - replaced FFSProtRename
                ec = NU_Rename( GFTEMPNAME, GFGROWNAME );
                fnCheckFileSysCorruption(ec);
              }
              else
              {
                rc = ec;
                fnCheckFileSysCorruption(unlink( GFTEMPNAME ));
              }
            }
            else
              fnCheckFileSysCorruption(unlink( GFTEMPNAME ));
          }
          else
          {
            fnCheckFileSysCorruption(fd2);
            ec = GFE_FILECREATEERROR;
          }
        }
        else  // Wrong Version
          ec = GFE_VERSNOTSUPPORTED;
      }
      else  // Not enough bytes in file for header.
      {
        fnCheckFileSysCorruption(len);
        ec = GFE_INVALIDFILEFORMAT;
      }
    }
    else
    {
        fnCheckFileSysCorruption(len);
        ec = GFE_FILESEEKERROR;
    }

    if( ec )
        fnGFJustLogIt( ec, GFW_GROWGRAPHICS, 0, 0 );
    
    //TODO - clean up return code generation
  
  return( ec );
} 
 



// ********************************************************************************
// FUNCTION NAME: fnGFAnyRoomAtTheInn()                                
// PURPOSE:    Checks if there is an available entry in the Directory for 
//              a new graphic.  IF instructed, checks the EMD parameters that 
//              can limit the number of a particular type of graphic.
//
//          If pDirInx is not null, this routine will return the index of the 
//          first empty entry.  This is for local functions only.
//
// AUTHOR:      C.Bellamy
// INPUTS:      None.
//
// Returns:    TRUE if their is an available entry in the directory and the
//                  EMD limit isn't reached, or we ignore the limit.
//             FALSE if all entries are used up, or if we are limited by the
//                  EMD and the limit has been reached.
//
// NOTES:   For speed, this only checks the copy in Ram. 
//---------------------------------------------------------------------
 BOOL fnGFAnyRoomAtTheInnPrivate( UINT8 bCompType,  UINT16 *pDirInx, BOOL fLimitedByEmd )
{
  BOOL  fRetVal = FALSE;
  UINT16    wDirInx;
  UINT16    wNumEntries;
  GFENT *   pDirEntry;
  UINT16    wTypeCount = 0;
  UINT16    wMaxTypeAllowed;
  UINT16    wEmptyDirInx = 0xFFFF;

    wNumEntries = fnGFGetDirLen();
    pDirEntry = fnGFGetFirstDirPtr();
    wMaxTypeAllowed = wNumEntries;
    
    if(   (bCompType == INSCR_COMP_TYPE)  
       && (fnFlashGetByteParm(BP_INSC_SUPPORTED) == 0) )
    {
        return( FALSE );
    }

    // If there are no entries, then there is no .GAR file or it is empty.
    //  In either case, there is plenty of room.
    if(   (wNumEntries == 0)
       || (pDirEntry == NULL_PTR) )
    {
        return( TRUE );
    }

    if( fLimitedByEmd )
    {
        switch( bCompType )
        {
            case AD_COMP_TYPE:
                wMaxTypeAllowed = fnFlashGetByteParm(BP_MAX_AD_SLOGAN);
                break;

            case INSCR_COMP_TYPE:
                wMaxTypeAllowed = fnFlashGetByteParm(BP_MAX_INSC);
                break;

            case ANY_COMPONENT_TYPE:
            default:
                // No limit, except size of .GAR file.
                fLimitedByEmd = FALSE;
                break;
        }
        if( wMaxTypeAllowed == 0 )
        {

#ifdef EMD_ZERO_MEANS_ZERO 
            return( FALSE );                
#else  
        // zero means unlimited.
            fLimitedByEmd = FALSE;
#endif
        }
    }    

    // SCAN Through each entry looking for first free entry
    for( wDirInx = 0 ; wDirInx < wNumEntries ; wDirInx++ )
    {
        if( pDirEntry->gfID == UNUSEDGFENTRYID )
        {
            if( wEmptyDirInx == 0xFFFF )
            {
                // Found first empty entry.
                wEmptyDirInx = wDirInx;
                if( fLimitedByEmd == 0 )
                {
                    // If we can have as many as will fit, then we are done looking.
                    break;
                }
            }
        } // End, this is an empty one.
        else 
        {
            // This is an occuppied entry.
            // If we are limiting by component type, and this entry is the
            //  same component type, and it is deletable, then count it and 
            //  check that we haven't reached the limit.
            if(   fLimitedByEmd
               && (pDirEntry->gfType == bCompType)
               && ((pDirEntry->gfFlags & GRFXFLG_PERMANENT) == 0) )
            {
                wTypeCount++;
                if( wTypeCount >= wMaxTypeAllowed )
                {
                    // We have reached our allowable limit of this component type.
                    // Indicate that we can't store it after all.
                    wEmptyDirInx = 0xFFFF;
                    break;
                }
            } // End must count this one and check.
        } // End look at occupied entry.

        // Check the next entry...
        pDirEntry++;
    }

    // If we found an empty entry, and there is room...
    if( wEmptyDirInx != 0xFFFF )
    {
        fRetVal = TRUE;
        // If the caller wants to know WHICH directory entry is empty...
        if( pDirInx != NULL_PTR )
        {
            *pDirInx = wEmptyDirInx;
        }
    }

    return( fRetVal );
}


// ********************************************************************************
// FUNCTION NAME: fnGFAnyRoomAtTheInn()                                
// PURPOSE:    Checks if there is an available entry in the Directory for 
//              a new graphic.  IF instructed, checks the EMD parameters that 
//              can limit the number of a particular type of graphic.
//
// AUTHOR:      C.Bellamy
// INPUTS:      None.
//
// OUTPUTS:    
// Returns:    TRUE if their is an available entry in the directory and the
//                  EMD limit isn't reached, or we ignore the limit.
//             FALSE if all entries are used up, or if we are limited by the
//                  EMD and the limit has been reached.
//
// NOTES:   For speed, this only checks the copy in Ram. 
//---------------------------------------------------------------------
BOOL fnGFAnyRoomAtTheInn( UINT8 bCompType, BOOL fLimitedByEmd )
{
    return( fnGFAnyRoomAtTheInnPrivate( bCompType, (UINT16 *) NULL_PTR, fLimitedByEmd ) );
}

// ********************************************************************************
// FUNCTION NAME: fnGFCheckFlagsByDirPtr()                                
// PURPOSE:     
//      Just checks the graphics flags in one directory entry.
//      Bits set in the high byte of flagtest indicae which bits to test.
//      The desired value of those bits are set in the low byte of flagtest.
//      bits that are not tested, according to the high byte, are ignored 
//      in the low byte.
//
// AUTHOR:      C.Bellamy
// INPUTS:      
//      pDirEntry - Pointer to entry in graphics directory.
//      uwFlagTest - word: low byte are the bits to test.
//                        high byte are the required values of the bits to test.        
//                  0 includes everything.
//
// OUTPUTS:     Returns TRUE if the tested flags are set as required. 
//              Returns FALSE if any bits are set/cleared incorrectly. 
//
// NOTES:   
//  1.  If the input pointer is NULL_PTR, return FALSE 
//  2.  There should be definitions for the test possibilities, located in 
//      grapext.h file.  They should be or'd to test more than one flag.
//          
//---------------------------------------------------------------------
BOOL fnGFCheckFlagsByDirPtr( const GFENT *pDirEntry, UINT16 uwFlagTest )
{
  BOOL      fRetval = FALSE;
  UINT8     ubMask;
  UINT8     ubFlags;
  UINT8     ubTest;

    
    if( pDirEntry )
    {
        if( uwFlagTest == 0 )  // If not 0, There are no conditions, include all.
        {
            fRetval = TRUE;
        }
        else
        {   // Separate the mask, which bits to test, from the test values.
            ubMask = (uwFlagTest & 0x0FF);
            ubFlags = uwFlagTest >> 8;
            // In ubTest, set the bits that are bad.
            ubTest = ( ( pDirEntry->gfFlags ^ ubFlags ) & ubMask );
            if( ubTest == 0 )
            {
                fRetval = TRUE;
            }
        }
    }

    return( fRetval );
}

//*********************************************************************************
// FUNCTION NAME: fnGFGetDirLen()                               
// PURPOSE:     Returns the number of entries allowed in the directory.
//
// AUTHOR:      C.Bellamy
// INPUTS:      None
//
// OUTPUTS:  Returns the number of entries allowed in the directory.
//
//  NOTE:   This will not work correctly if there are more than 65,535 
//      directory entries in the graphics file.  Neither will many of
//      the other functions since the directory index is an unsigned short.//
//---------------------------------------------------------------------
static UINT16 fnGFGetDirLen( void )
{
  UINT16    wNumEntries = 0;

    if( pGFInfo->fsInit == TRUE )
    {
        wNumEntries = (UINT16)pGFInfo->rgfHdr.gfDirSize;
    }
    return( wNumEntries );
}


//*********************************************************************************
// FUNCTION NAME: fnGFGetFirstDirPtr()                              
// PURPOSE:     Returns pointer to the first directory entry.
//
// AUTHOR:      C.Bellamy
// INPUTS:      None
//
// OUTPUTS:  Returns the number of entries allowed in the directory.
//
//---------------------------------------------------------------------
static GFENT * fnGFGetFirstDirPtr( void )
{
  GFENT     *   pDirEntry = NULL_PTR;

    if( pGFInfo->fsInit == TRUE )
    {
        pDirEntry = pGFInfo->rgfEntries;        
    }
    return( pDirEntry );
}

//*********************************************************************************
// FUNCTION NAME: fnGFGetLnkPtrByDirPtr()                               
// PURPOSE:     Given a pointer to the Directory Entry (in ram), returns pointer 
//      to the graphic (including link header) in ram. 
//
// AUTHOR:      C.Bellamy
// INPUTS:      None
//
// OUTPUTS:  Returns pointer to lnk header of graphic.
//
//---------------------------------------------------------------------
UINT8 * fnGFGetLnkPtrByDirPtr( const GFENT *pDirEntry )
{
    UINT8 * LnkPtr;

    LnkPtr = pGFInfo->rgfImages + (pDirEntry->gfOffset - pGFInfo->rgfImageOffset);        
    return( LnkPtr );
}



// ********************************************************************************
// FUNCTION NAME: fnGFGetDirPtrByDirInx()                               
// PURPOSE:     . 
//
// AUTHOR:      C.Bellamy
// INPUTS:      wThisDirInx - Index to entry in graphics directory.
//
// OUTPUTS:     Returns ptr to directory entry.
//
// NOTES:   If the input pointer is NULL_PTR, return FALSE 
//---------------------------------------------------------------------
GFENT *fnGFGetDirPtrByDirInx( UINT16 wThisDirInx )
{
  UINT16    wNumEntries;
  GFENT *   pDirEntry = NULL_PTR;

    wNumEntries = fnGFGetDirLen();
    if( wThisDirInx < wNumEntries )
    {
        pDirEntry = fnGFGetFirstDirPtr();
        if( pDirEntry != NULL_PTR )
        {
            pDirEntry += wThisDirInx;
        }
    }

    return( pDirEntry );
}


// ********************************************************************************
// FUNCTION NAME: fnGFGetDataNamePtrByDirInx()                              
// PURPOSE:      
//
// AUTHOR:      C.Bellamy
// INPUTS:      wThisDirInx - Index to entry in graphics directory.
//
// OUTPUTS:     Returns .
//
// NOTES:   
//---------------------------------------------------------------------
UINT8 *fnGFGetDataNamePtrByDirInx( UINT16 wThisDirInx )
{
  UINT8 *   pDataName = NULL_PTR; 
  GFENT *   pDirEntry;

    pDirEntry = fnGFGetDirPtrByDirInx( wThisDirInx );
    if( pDirEntry  &&  (pDirEntry->gfID != UNUSEDGFENTRYID) )
    {
        pDataName = pDirEntry->gfDataName;
    }

    return( pDataName );
}

// ********************************************************************************
// FUNCTION NAME: fnGFGetGenIDByDirInx()                                
// PURPOSE:      
//
// AUTHOR:      C.Bellamy
// INPUTS:      wThisDirInx - Index to entry in graphics directory.
//
// OUTPUTS:     Returns .
//
// NOTES:  Gets the GenID from the directory entry and not the graphic. 
//---------------------------------------------------------------------
UINT16 fnGFGetGenIDByDirInx( UINT16 wThisDirInx )
{
  UINT16    wGenID = UNUSEDGFENTRYID ; 
  GFENT *   pDirEntry;

    pDirEntry = fnGFGetDirPtrByDirInx( wThisDirInx );
    if( pDirEntry ) 
        wGenID = pDirEntry->gfID;

    return( wGenID );
}


// **********************************************************************
// FUNCTION NAME:   fnGrfxCheckDataNameVer
//
// PURPOSE:     Compare the DataNameVer in the graphic with the Reference
//          dataNameVer pointed to by second argument.  If they are the 
//          same, return TRUE, else FALSE.
//                                      
//  AUTHOR:  C.Bellamy  May 8, 2005
//
//  INPUTS:     pLnkPtr - Ptr to link header of graphic.
//              pRefDataNameVer - Pointer to dataName and Version array to match.
//
//  OUTPUTS:    TRUE if there is a match between the DataNameVer and the 
//          one in the graphic.
//
//  NOTES:  None
// 
//---------------------------------------------------------------------
BOOL fnGFCheckDataNameVerByDirInx( UINT16 wThisDirInx, const UINT8 *pRefDataNameVer, UINT8 bLen )
{
    BOOL    retval = FALSE;
    UINT8 * pThisDataNameVer;

    pThisDataNameVer = fnGFGetDataNamePtrByDirInx( wThisDirInx );
    if( pThisDataNameVer )
    {
        if( memcmp( pRefDataNameVer, pThisDataNameVer, bLen ) == 0 )
            // Found it!
            retval = TRUE;        
    }
    
    return( retval );    
}


// **********************************************************************
// FUNCTION NAME:   fnGFGetGenIDListByCompType
//
// PURPOSE:     Based on type, searches either the EMD Linked Graphics List 
//          or the Resident graphics (RAM copy of graphics file) to find as
//          many matches as it can.  If the pointer to the destination list 
//          is NULL_PTR, it just counts them and returns the number. 
//          Otherwise, it fills up the destination list with the Generated IDs,
//          stopping when bMaxList entries have been found and copied.
//                                      
//  AUTHOR:  C.Bellamy
//
//  INPUTS:
//       bCompType - byte, Component Type to look for.
//       pDestIDList - Pointer to pre-initialized list of UINT16s, to store the
//                     found IDs in.  If pointer is NULL_PTR, just count IDs
//                     don't copy them.
//       wMaxList - UINT16, max number of entries to look for.
//                    This is ONLY looked at if we are writing the IDs to the list.
//       BOOL - EnabledOnly - if TRUE, ignore NOT_PURCHASED graphics.
//
//  OUTPUTS:    Fills the pDestIDList, if pointer is not NULL_PTR.
//              Returns the number found.
//
// NOTE: Replaces fnGFGetResidentGraphicList & fnGFGetGraphicList// 
//---------------------------------------------------------------------
UINT16 fnGFGetGenIDListByCompType( UINT8 bCompType, UINT16 *pDestIDList, 
                                   UINT16 wMaxReturn, UINT16 uwFlagTest )
{
  UINT16    wDirInx;
  UINT16    wNumFound = 0;      // Number matches found so far.
  GFENT *   pDirEntry;
  UINT16    wDirSize;
  BOOL  forever = TRUE;
    
    // Get pointer to first entry in directory.
    pDirEntry = fnGFGetFirstDirPtr();
    if( pDirEntry )
    {
        wDirSize = fnGFGetDirLen();
        for( wDirInx = 0; forever  && (wDirInx < wDirSize) ; wDirInx++, pDirEntry++ )
        {
            // If we found another used entry...
            if( pDirEntry->gfID != UNUSEDGFENTRYID )
            {
                // If it is our type.. 
                if( pDirEntry->gfType == bCompType )
                {  
                    if( uwFlagTest &&  (fnGFCheckFlagsByDirPtr( pDirEntry, uwFlagTest ) == FALSE) )
                    {
                    }//skip
                    else
                    {  
                        // Check if we are making a list . 
                        if( pDestIDList )
                        {
                            pDestIDList[ wNumFound ] = pDirEntry->gfID;
                        } 
                        wNumFound++;

                        // Check for count limiter.
                        if( (wMaxReturn == 0)  ||  (wNumFound < wMaxReturn) )
                         {   // Keep looking.
                         }
                        else
                            forever = FALSE;
                    }
                }
            }
        }
    }
    return( wNumFound );
}


//*********************************************************************************
// FUNCTION NAME: fnGFGetLnkPtrByDirInx()                               
// PURPOSE:     Get the link pointer from the directory index.
//
// AUTHOR:      C.Bellamy
// INPUTS:      Index into Graphics file directory.
//
// OUTPUTS:  Returns pointer to graphic with link header.
//          If directory index out of range, returns NULL_PTR.
//
//---------------------------------------------------------------------
static UINT8 *fnGFGetLnkPtrByDirInx( UINT16 wThisDirInx )
{
  GFENT *   pDirEntry;
  UINT8 *   LnkPtr = NULL_PTR;

    pDirEntry = fnGFGetDirPtrByDirInx( wThisDirInx );
    
    if( pDirEntry  &&  (pDirEntry->gfID != UNUSEDGFENTRYID) )
    {
        LnkPtr = fnGFGetLnkPtrByDirPtr( pDirEntry );
    }

    return( LnkPtr );
}


//*********************************************************************************
// FUNCTION NAME: fnGFGetNextDirInxByCompType()                             
// PURPOSE:     Find the next used directory entry with the matching compType.
//          CompType MAY be ANY type.
//          Start looking at the directory entry passed in.
//
// AUTHOR:      C.Bellamy
// INPUTS:      
//      pDirInx - Pointer to starting directory index AND destination 
//                  Directory Index. If no more matches found, this is left alone.
//      bCompType - byte, Type of component to find. May be ANY_COMP_TYPE.
//      uwFlagTest - word: low byte are the bits to test.
//                        high byte are the required values of the bits to test.        
//                  0 includes everything.
//
// OUTPUTS: If an entry is found, writes over the directory index and returns
//      a pointer to the graphic, including link header.
//      If no matching entry is found, return NULL_PTR and leave the DirInx alone.
//
//---------------------------------------------------------------------
UINT8 *fnGFGetNextDirInxByCompType( UINT16 *pDirInx, UINT8 bCompType, UINT16 uwFlagTest )
{
  UINT16    wThisDirInx = *pDirInx;
  UINT16    wDirSize;
  GFENT *   pDirEntry;
  UINT8 *   LnkPtr = NULL_PTR;

    wDirSize = fnGFGetDirLen();
    // Get pointer to first entry in directory.
    pDirEntry = fnGFGetFirstDirPtr();
    if( pDirEntry )
    {
        for( pDirEntry += wThisDirInx ; wThisDirInx < wDirSize ; wThisDirInx++, pDirEntry++ )
        {
            if( pDirEntry->gfID != UNUSEDGFENTRYID )
            {
                if(   (pDirEntry->gfType == bCompType)
                   || (bCompType == ANY_COMPONENT_TYPE) )
                {   // Found a match, check if it needs to be enabled.
                    if( uwFlagTest && (fnGFCheckFlagsByDirPtr( pDirEntry, uwFlagTest ) == FALSE) )
                    {
                        // Not enabled, so skip it
                    }
                    else
                    {
                        LnkPtr = fnGFGetLnkPtrByDirInx( wThisDirInx );
                        if( LnkPtr != NULL_PTR )
                        {
                            *pDirInx = wThisDirInx;
                            return( LnkPtr );
                        }
                    }
                }
            }
        }
    }

    // Couldn't find any more valid entries in the list.    
    return( NULL_PTR );
}

//*********************************************************************************
// FUNCTION NAME: fnGFGetDirInxByID()                               
// PURPOSE:     
//      Find a graphic with the matching CompType and GenID.
//      Saves the directory index in the destination argument, pDirInx.
//      Returns a pointer to that entry in Ram.  If none found, the 
//      ptr is a NULL_PTR and the data in pDirInx is left alone.
//
// AUTHOR:      C.Bellamy
// INPUTS:      bCompType - byte, Type of component to find.
//              GenID - word, graphic ID that must also match (A.K.A. GraphicID.)
//
// OUTPUTS: Index into directory, of the matching directory entry.
// RETURNS: Pointer to that entry in the ram copy of the directory.
//---------------------------------------------------------------------
GFENT * fnGFGetDirInxByID( UINT8 bCompType, UINT16 wGenId, UINT16 uwFlagTest, UINT16 * pDirInx )
{
  UINT16    wDirInx = 0;
  GFENT *   pDirEntry;
  GFENT *   pReturnDirEntry   = NULL_PTR;
  UINT16    wDirSize;
  BOOL  forever = TRUE;

    // Get pointer to first entry in directory.
    pDirEntry = fnGFGetFirstDirPtr();
    if( wGenId   &&   pDirEntry )
    {
        wDirSize = fnGFGetDirLen();
        forever = TRUE;
        for( wDirInx = 0 ; (wDirInx < wDirSize) && forever ; wDirInx++, pDirEntry++ )
        {
            if( pDirEntry->gfID != UNUSEDGFENTRYID )
            {
                if(   (pDirEntry->gfType == bCompType)
                   && (pDirEntry->gfID == wGenId )    )
                {
                    // Found it, stop looking.
                    forever = FALSE;
                    if( uwFlagTest && (fnGFCheckFlagsByDirPtr( pDirEntry, uwFlagTest ) == FALSE) )
                        // Return NULL_PTR.
                        pReturnDirEntry = NULL_PTR;
                    else
                    {
                        *pDirInx = wDirInx;
                        pReturnDirEntry = pDirEntry;
                    }
                }
            }
        }
    }
    
    // If we got here, we did not find it.
    return( pReturnDirEntry );
}

//*********************************************************************************
// FUNCTION NAME: fnGFGetDirPtrByID()                               
// PURPOSE:     Find a graphic with the matching CompType and GenID.
//              Returns a pointer to the directory entry for the graphic.
//
// AUTHOR:      C.Bellamy
// INPUTS:      bCompType - byte, Type of component to find.
//              GenID - word, graphic ID that must also match (A.K.A. GraphicID.)
//
// OUTPUTS: Returns pointer to the directory entry.
//
//---------------------------------------------------------------------
GFENT *fnGFGetDirPtrByID( UINT8 bCompType, UINT16 wGenId, UINT16 uwFlagTest )
{
  GFENT     *   pDirEntry;
  UINT16    wDirInx = 0;

    pDirEntry = fnGFGetDirInxByID( bCompType, wGenId, uwFlagTest, &wDirInx );    
    
    return( pDirEntry );
}


//**********************************************************************
// FUNCTION NAME:   fnGFGetFirstLnkPtrByCompType( unsigned char type )
// PURPOSE:     Find the first graphic of the given type and return a
//          pointer to the graphic, including the link header.
//
// AUTHOR: Clarisa Bellamy
//
// INPUTS:  Component Type            
//         
// RETURNS:     Pointer to graphic image with link header;
//              NULL_PTR if not found.
//
//  NOTES:  Copied from fnGFGetGraphicByFC to replace it.
//        
//------------------------------------------------------------------------
UINT8 *fnGFGetFirstLnkPtrByCompType( UINT8 bCompType, UINT16 uwFlagTest )
{
    UINT8 *LnkPtr = NULL_PTR;
    UINT16  wDirInx = 0;

    LnkPtr = fnGFGetNextDirInxByCompType( &wDirInx, bCompType, uwFlagTest );

    return( LnkPtr );
}

// **********************************************************************
// FUNCTION NAME:fnGFGetGraphic( unsigned char type , unsigned short id )
// PURPOSE: 
//
// AUTHOR: Clarisa Bellamy
//
// INPUTS:  Graphic Type, 
//          Graphic ID, 
//      uwFlagTest - word: low byte are the bits to test.
//                        high byte are the required values of the bits to test.        
//                  0 includes everything.
//         
//         
// RETURNS: pointer to graphic image if found NULL otherwise
//        
//------------------------------------------------------------------/
UINT8 *fnGFGetLnkPtrByID( UINT8 bCompType, UINT16 wGenId, UINT16 uwFlagTest )
{
  UINT8 *LnkPtr = NULL_PTR;
  GFENT *pDirEntry;

    pDirEntry = fnGFGetDirPtrByID( bCompType, wGenId, uwFlagTest );
    if( pDirEntry )
        LnkPtr = fnGFGetLnkPtrByDirPtr( pDirEntry );
    
    return( LnkPtr );
}


// ********************************************************************************
// FUNCTION NAME: fnGFGetRamCopyDirEntry()                                
// PURPOSE:     In preparation of changing a directory entry, it will do one
//          of the following:
//          If we have a copy of the directory in ram, it will return a pointer
//              to the entry in the copy.
//          If we do NOT have a copy of the directory in ram, it will copy the
//              directory entry from the GF file into rOneDirEntry so it can
//              be manipulated, and then return a pointer to rOneDirEntry.
//          In either case, if the Directory Index is out of range, then a 
//              NULL_PTR is returned.
//
// AUTHOR:      C.Bellamy
// INPUTS:      wDirInx - Index into directory of entry to modify.
//
// OUTPUTS:     Pointer to a ram copy of the directory entry.
//
// NOTES:    
//---------------------------------------------------------------------
static GFENT * fnGFGetRamCopyDirEntry( UINT16 wDirInx )
{
  GFENT *   pDirEntry = NULL_PTR;
          
#ifdef READ_DIRENTRIES_FROM_RAM
    pDirEntry = fnGFGetDirPtrByDirInx( wDirInx );
#else
    // Get a copy of the directory entry into ram.
    ec = fnGFReadOneDirEntry( wDirInx, &rOneDirEntry )
    // If there was an error, log it, else 
    if( ec )
        fnGFJustLogIt( ec, GFW_GETDIRENTRY, 0, 0 );
    else
        pDirEntry = &rOneDirEntry;
#endif    
    return( pDirEntry );
}

// ********************************************************************************
// FUNCTION NAME: fnGFRamSetStatOne()                               
// PURPOSE:     Change the state of the directory in ram copy of the directory
//              entry.
//
// AUTHOR:      C.Bellamy
// INPUTS:      
//      pDirEntry - Pointer to ram copy of directory entry.
//      uwFlagTest - word: high byte are the bits to modify, others are left alone.
//                        low byte are the required values of the bits to modify.
//                      If a bit is not set in low byte, its value in high byte is ignored.
//
// OUTPUTS:     Pointer to the directory entry that was just modified.
//
// NOTES:   For now, this just sets the purchased bit, may want to do more
//      anding and oring later. 
//---------------------------------------------------------------------
static void fnGFRamSetStatOne( GFENT *pDirEntry, UINT16 uwFlagTest )
{
    UINT8     ubMask;
    UINT8     ubFlags;
    UINT8     ubTest;

    if( pDirEntry )
    {
        // Change the flag...
        // Separate the mask, which bits to test, from the test values.
        ubMask = (uwFlagTest & 0x0FF);
        ubFlags = uwFlagTest >> 8;
        // Clear the bits that are set in mask, then set the ones we want.
        ubTest = pDirEntry->gfFlags & ~ubMask;
        ubTest |= (ubMask & ubFlags);

        pDirEntry->gfFlags = ubTest;
    }

    return;        
}

// ********************************************************************************
// FUNCTION NAME: fnGFChgStatusByDirInx()                               
// PURPOSE:     Change the entry in ram, then copy the change into the 
//          graphics file.
//
// AUTHOR:      C.Bellamy
// INPUTS:      
//      wDirInx - Index into directory of entry to modify.
//      uwFlagTest - word: high byte are the bits to modify, others are left alone.
//                        low byte are the required values of the bits to modify.
//                      If a bit is not set in low byte, its value in high byte is ignored.
//
// OUTPUTS:     ErrorCode from attempts to write to file.
//
// NOTES:  This isn't called yet, but it may be useful in the future.
//
//---------------------------------------------------------------------
int fnGFChgStatusByDirInx( UINT16 wDirInx, UINT16 uwFlagTest )
{
  int       ec = 0;
  GFENT *   pDirEntrySrc = NULL_PTR;

    // This function range checks wDirInx, and returns NULL_PTR if out of range.
    // If in range, it returns a pointer to a ram copy of the directory entry.
    pDirEntrySrc = fnGFGetRamCopyDirEntry( wDirInx );
    if( pDirEntrySrc )
    {
        // Modify the ram copy.
        fnGFRamSetStatOne( pDirEntrySrc, uwFlagTest );
        // Copy the modified directory entry into the file.   
        ec = fnGFWriteOneDirEntry( wDirInx, pDirEntrySrc );
    }
    else 
        ec = GFE_DIRINXOUTOFRANGE;

    return( ec );      
}

// ********************************************************************************
// FUNCTION NAME: fnGFRamDeleteOne()                              
// PURPOSE:     Delete the directory in the local copy of graphics file data. 
//
// AUTHOR:      C.Bellamy
// INPUTS:      pDirEntry - Pointer to a ram copy of a directory index to delete.
//
// OUTPUTS:     None.
//
// NOTES:   A deleted entry is one that has a GenID of UNUSEDGFENTRYID.
//      The rest of the data in the directory entry is zeroed out.
//      If the Directory Entry Pointer input is NULL_PTR, do nothing.
//---------------------------------------------------------------------
static void fnGFRamDeleteOne( GFENT *pDirEntry )
{
          
    if( pDirEntry )
    {
        // Clear the entire entry, then set the GenID to UNUSED.
        (void)memset( pDirEntry, 0, sizeof(GFENT) );
        pDirEntry->gfID = UNUSEDGFENTRYID;
    }

    return;        
}


// ********************************************************************************
// FUNCTION NAME: fnGFDeleteByDirInx()                              
// PURPOSE:     
//      Change the GenID of the directory entry in ram, to indicate that the
//      entry is not used.  Then copy the changed directory entry into the 
//      graphics file.
//
// AUTHOR:      C.Bellamy
// INPUTS:      wDirInx - Index into directory of entry to delete.
//              updateFile - Bool that determine whether or not to update the file in the file system
//
// OUTPUTS:     ErrorCode from attempts to write to file.
//
// NOTES:   A deleted entry is one that has a GenID of UNUSEDGFENTRYID.
//      The rest of the data in the directory entry is zeroed out.
//      If the Directory index input was out of range, do nothing. 
//---------------------------------------------------------------------
int fnGFDeleteByDirInx( UINT16 wDirInx, BOOL updateFile )
{
  int       ec = 0;
  GFENT *   pDirEntrySrc = NULL_PTR;

    // This function range checks wDirInx, and returns NULL_PTR if out of range.
    // If in range, it returns a pointer to a ram copy of the directory entry.
    pDirEntrySrc = fnGFGetRamCopyDirEntry( wDirInx );
    if( pDirEntrySrc )
    {
        // Modify the ram copy to indicate the graphic is not used.
        fnGFRamDeleteOne( pDirEntrySrc );
        // Copy the modified directory entry into the file.

		if(updateFile == TRUE)
		{
			ec = fnGFWriteOneDirEntry( wDirInx, pDirEntrySrc );
		}

        // This counts the deleted graphics since the last file compression.
        uwCMOSGFNeedsCmpxn++;
    }
    else 
        ec = GFE_DIRINXOUTOFRANGE;

    return( ec );      
}


// **********************************************************************
// FUNCTION NAME:   fnGFDeleteGraphicById
// PURPOSE: 
//      Find a directory entry that has the indicated Component type and 
//      GenID, then call fnGFDeleteByDirInx to "delete" it from the ram copy
//      and the graphics file.
//
// AUTHOR:  Clarisa Bellamy 5/8/2005
//
// INPUTS: 
//      bCompType - Component type of graphic to delete.
//      wGenID - GenID of Graphic to Delete.
//         
// RETURNS:  ErrorCode for Reading/Writing files.  0 = Success.
// 
//  NOTES:  This "deletes" the graphic locally, then in the Graphics file.       
//      "Deleting" is setting the GenID in the directory entry to UNUSEDGRAPHIC,
//       and clearing all the other data in the directory entry.
//------------------------------------------------------------------/
int fnGFDeleteGraphicById( UINT8 bCompType, UINT16 wGenId, BOOL updateFile)
{
  int       ec = 0;
  UINT16    wDirInx =  0x0FFF;
  GFENT *   pDirEntry;

    // Make sure this type resides in the graphic file.  Can't delete from the Linked list.
    if( fnGrfxIsNonEmdCompType( bCompType ) )
    {
        // This returns NULL_PTR if entry is not found.  GRFX_ANY_FLAGS = Ignore purchased bit.
        pDirEntry = fnGFGetDirInxByID( bCompType, wGenId, GRFX_ANY_FLAGS, &wDirInx );
        if( pDirEntry )
            ec = fnGFDeleteByDirInx( wDirInx , updateFile);
        else
            fnGFJustLogIt( GFEC_INFO, GFW_DELETE, GFI_GRFKNOTFOUND, bCompType );
    }
    else
        ec = GFE_INVALIDGRAPHICTYPE;
        
    if( ec )
        fnGFJustLogIt( ec, GFW_DELETE, 0, 0 );
    return( ec );
}

/* **********************************************************************
// FUNCTION NAME: int fnGFReadInGraphicsFile(GFRESIDENT *resident )
// PURPOSE: 
//
// AUTHOR: George Monroe
//
// INPUTS: 
//         
//         
// RETURNS:
//        
// **********************************************************************/
static int fnGFReadInGraphicsFile( GFRESIDENT *resident, UINT32 extraSpace )
{
  int ec = NU_SUCCESS,rc = 0,len;
  DSTAT statobj;
  BOOL  call_NU_Done = false;
  CHAR  pattern[6] = "*.GAR";
  INT fd;
  BOOL fixEndians = FALSE;
  UINT32    allocsize;
  UINT32    entrysz;
  UINT32    imageSize;

	//Find GAR file
	ec = NU_Get_First(&statobj, pattern);
	if(ec == NU_SUCCESS)
	{
	  call_NU_Done = true;
	  //Open and Read
	  fd = NU_Open(statobj.lfname, (PO_BINARY | PO_RDONLY ), PS_IREAD);
	  if(fd < 0)
	  {
		dbgTrace(DBG_LVL_ERROR, "fnGFReadInGraphicsFile: Failed to open gar file %s \r\n", statobj.lfname);
		ec = fd;
        fnCheckFileSysCorruption(ec);
	  }
	  else
	  {
		dbgTrace(DBG_LVL_ERROR, "GAR file name :%s \r\n", statobj.lfname);
		// read the header
 		len = NU_Read(fd, (char *)&resident->rgfHdr, sizeof(resident->rgfHdr));
		if( len == sizeof(resident->rgfHdr) )
		{
		  if( resident->rgfHdr.gfVersion == EndianSwap32((UINT32)GRAPHIC_FILE_VERSION) )
		  {
			fixEndians = TRUE;
			resident->rgfHdr.gfVersion = EndianSwap32(resident->rgfHdr.gfVersion);
			resident->rgfHdr.gfDirSize = EndianSwap32(resident->rgfHdr.gfDirSize);
		  }

		  if( resident->rgfHdr.gfVersion == GRAPHIC_FILE_VERSION )
		  {
			allocsize = (UINT32)statobj.fsize + extraSpace;
			resident->allocAddr = (UINT8 *)malloc( allocsize );
			if( resident->allocAddr )
			{
			  resident->rgfExtraSpace = extraSpace;
			  entrysz = resident->rgfHdr.gfDirSize * sizeof(GFENT);
			  if( allocsize >= entrysz + sizeof(GFHDR) )
			  {                
				imageSize = (allocsize - entrysz) - sizeof(GFHDR) - extraSpace;
 			    resident->rgfCurrSize = entrysz + imageSize;
                resident->rgfEntries = (GFENT *)resident->allocAddr;
                resident->rgfImages = resident->allocAddr + entrysz;
                len = NU_Read(fd, (char *)resident->rgfEntries, entrysz );
				if( len == entrysz)
                {
                  resident->rgfImageOffset = entrysz + sizeof(GFHDR);
                  if( imageSize )
                  {
                    len =  NU_Read(fd, (char *)resident->rgfImages, imageSize );
                    if( len != imageSize)
                    {
                      fnCheckFileSysCorruption(len);
                      ec = GFE_FILEREADERROR;
                    }
                  } 
                }
                else
                {
                  fnCheckFileSysCorruption(len);
                  ec = GFE_FILEREADERROR;
                }
              }
              else  
              { // Not enough bytes in the file for hdr & directory.
                ec = GFE_INVALIDFILEFORMAT;
              }
            }
            else
            {
              ec = GFE_MALLOCFAILED;
            }
          }
          else  // Wrong version.
          {
            ec = GFE_VERSNOTSUPPORTED;
          }
        }
        else  // Not enough bytes in file for header.
        {
          fnCheckFileSysCorruption(len);
          ec = GFE_INVALIDFILEFORMAT;
        }
        rc = NU_Close( fd );
        fnCheckFileSysCorruption(rc);
        if( ec == 0 )
            ec = rc;
	  }
    }
    else   // Could not FIND a *.GAR file.
    {
      uwCMOSGFNeedsCmpxn = 0;
      fnCheckFileSysCorruption(ec);
      ec = GFE_NOGRAPHICSFILE;
    }

	if((fixEndians == TRUE) && (ec == 0))
	{
		GFENT *pDirEntry;
		INT cnt = 0;
		UINT8 *   LnkPtr = NULL_PTR;
		UINT8     pLnkHdrFix[ LNKHDRSIZE ];
		UINT8 * pNewGraphic;
		BOOL      fStat;
		char      bVisibility;

		pDirEntry = resident->rgfEntries;
		
		while((cnt < resident->rgfHdr.gfDirSize) && (ec == 0))
		{
			pDirEntry->gfOffset =  EndianSwap32(pDirEntry->gfOffset);
			pDirEntry->gfSize =  EndianSwap32(pDirEntry->gfSize);
			pDirEntry->gfID =  EndianSwap16(pDirEntry->gfID);
			if(pDirEntry->gfID == UNUSEDGFENTRYID)
				break;
			
			LnkPtr = NULL_PTR;

			if( pDirEntry->gfFixVer < GRFX_CLEANUP_VER )
            {
                // Prior to FixVer1, graphics that were added to an existing .GAR file
                //  had FeatID set to 0xFF.  
                if(   (pDirEntry->gfFixVer < 1)
                   && ((pDirEntry->gfFlags & GRFXFLG_PURCHASED) == 0) 
                   && (pDirEntry->gfFeatID == 0xFF))
                {
                    //bDirEntryModified = GRFX_CLNUP_DELETE_ENTRY; //TODO: this entry should be deleted
                }
                else
                {
                    if( pDirEntry->gfFixVer < 2 )
                    {
                        // Make sure this is set.
                        pDirEntry->gfFlags |= GRFXFLG_PURCHASED;
						
						// The dir entry is in our copy of the directory.
                        LnkPtr = fnGFGetLnkPtrByDirPtr( pDirEntry );
                       // *ppImageWithLnk = LnkPtr;

						if( LnkPtr != NULL_PTR )
                        {
                            // This is a valid graphic...
                            (void)memcpy( pLnkHdrFix, LnkPtr, LNKHDRSIZE );
							
                            // Find out if the GenID in the LnkHdr matches the directory entry.
                            if(  pDirEntry->gfID != fnGrfxGetGenID( LnkPtr ) )
                            {
                                // This lnkHdr needs to be fixed.  
                                // Change the ID to match the dir entry.
                                (void)memcpy( pLnkHdrFix + COMPONENTID_OFFSET, &(pDirEntry->gfID), sizeof( UINT16 ) );
								
                                // Copy it into the local ram copy of the graphic.
                                (void)memcpy( LnkPtr, pLnkHdrFix, LNKHDRSIZE );
                            }
							
                            // Make sure the directory flags match the info in the graphic.
                            pNewGraphic = fnGrfxGetGraphic( LnkPtr );
                            fStat = fnFlashGetGCvisibility( pNewGraphic , &bVisibility);
                            if( fStat == SUCCESSFUL )
                            {
                                if( bVisibility & VSBLTY_DONT_DELETE )
                                {
                                    pDirEntry->gfFlags |= GRFXFLG_PERMANENT;
                                }
								
                                // Remove all but visibility bits.
                                bVisibility &= VISIBILITYMASK; 
                                if( bVisibility == VSBLTY_AUTOGRAPHIC )
                                {
                                    // If it is an autographic, set the autographic flag.
                                    pDirEntry->gfFlags |= GRFXFLG_AUTOGRFX;    
                                }
                                else
                                {
                                    // Clear the autographic flag.
                                    pDirEntry->gfFlags &= ~GRFXFLG_AUTOGRFX;
                                }
                            }
                            else
                            {
                                // Can't read from graphic.
                                ec = GFE_INVALIDHEADER;
                            }
							
                            pDirEntry->gfFixVer = 2;
                        } // End of got LnkPtr
                    }  // Endof FixVer < 2
                } // End of dir entry is not deleted here.                
            } // End of FixVer < Current Fix dir.

			pDirEntry++;
			cnt++;
		}

		//Write out updated .GAR file so we do not keep converting it
		if(ec == 0)
		{
			fd = NU_Open(GFTEMPNAME, PO_CREAT| PO_WRONLY| PO_TRUNC, PS_IWRITE);
			if(fd >= 0)
			{
				len = NU_Write(fd, (char *)&resident->rgfHdr, sizeof(resident->rgfHdr));
				if( len == sizeof(resident->rgfHdr) )
				{
					len = NU_Write(fd, (char *) resident->allocAddr, imageSize + entrysz);
					if( len != imageSize + entrysz )
					{
						ec = GFE_FILEWRITEERROR;
						fnCheckFileSysCorruption(len);
					}
				}
				else
				{
					fnCheckFileSysCorruption(len);
					ec = GFE_FILEWRITEERROR;
				}

				rc = NU_Close( fd );
				fnCheckFileSysCorruption(rc);
				if( ec == 0 )
					ec = rc;

                if( ec == 0 )
                {
					ec = NU_Rename( GFTEMPNAME, GFGROWNAME );
					if(ec == 0)
					{
					  fnCheckFileSysCorruption(unlink( statobj.lfname));
					  ec = NU_Rename( GFGROWNAME, statobj.lfname);
					}
					else
					{
						fnCheckFileSysCorruption(unlink( GFTEMPNAME));
					}
					fnCheckFileSysCorruption(ec);
				}
			}
			else
			{
				ec = fd;
				fnCheckFileSysCorruption(ec);
			}

			if(ec != 0)
			{
				//Issues converting GAR file
				if( resident->allocAddr )
					free( resident->allocAddr );

				(void)memset( (UINT8 *)resident, 0 , sizeof(GFRESIDENT) );
				dbgTrace(DBG_LVL_ERROR, "Error converting endians for GAR file name :%s \r\n", statobj.lfname);
			}

		}

	}

	if(ec == 0)
	{
		//See if we need to increase the size of the GAR file
		if( resident->rgfHdr.gfDirSize < DEFAULT_GF_DIR_SIZE)
		{
			//Open and Read
			fd = NU_Open(statobj.lfname, (PO_BINARY | PO_RDONLY ), PS_IREAD);
			if(fd < 0)
			{
				fnCheckFileSysCorruption(fd);
				dbgTrace(DBG_LVL_ERROR, "fnGFReadInGraphicsFile: Failed to open gar file %s for growth\r\n", statobj.lfname);
				ec = fd;
			}
			else
			{
				dbgTrace(DBG_LVL_ERROR, "fnGFReadInGraphicsFile: Growing gar file %s by %d entries\r\n", statobj.lfname, (DEFAULT_GF_DIR_SIZE - resident->rgfHdr.gfDirSize));
				ec = fnGFGrowGraphicFile( fd ,  (DEFAULT_GF_DIR_SIZE - resident->rgfHdr.gfDirSize) );
				rc = NU_Close(fd);
				fnCheckFileSysCorruption(rc);

				if(ec == 0)
				{
					fnCheckFileSysCorruption(unlink(statobj.lfname));
					ec = NU_Rename( GFGROWNAME, statobj.lfname);
					fnCheckFileSysCorruption(ec);
				}
				else
				{
					dbgTrace(DBG_LVL_ERROR, "fnGFReadInGraphicsFile: Failed to grow gar file %s\r\n", statobj.lfname);
				}

			}
		}
	}

    if (call_NU_Done)
        (void) NU_Done(&statobj);

	return( ec );
}

/* **********************************************************************
// FUNCTION NAME: fnGFInitResidentGraphics( UINT32 extraSpace)
// PURPOSE: Read the graphic file into memory and make ready for access
//
// AUTHOR: George Monroe
//
// INPUTS:  Additioanl space to allocate
//         
// RETURNS: status of opening and reading in of the graphics
//        
// **********************************************************************/
int fnGFInitResidentGraphics(UINT32 extraSpace )
{
  int ec;

    if( pGFInfo->allocAddr )
        free( pGFInfo->allocAddr );

    (void)memset( (UINT8 *)pGFInfo, 0 , sizeof(GFRESIDENT) );
    ec = fnGFReadInGraphicsFile( pGFInfo, extraSpace );

    // If we got an error, log it, if not, save the fs value.
    //   The fs value can be used to verify that the ram Graphics Info
    //   was successfully initialized and contains valid info.
    //  So clear it if we were not successful.
    if( ec )
    {
        fnGFJustLogIt( ec, GFW_INITGRFX, 0, 0 );
        pGFInfo->fsInit = FALSE;        
    }
    else
    {
        pGFInfo->fsInit = TRUE;
    }

   return( ec );
}

//**********************************************************************
// FUNCTION NAME: fnGFVerifyGFInfoInitialized( void )
//
// PURPOSE: 
//      Verify that fnGFInitResidentGraphics has been called at least 
//      once.
//      It checks if the .fsInit member has been initialized, if not, 
//      then call fnGFInitResidentGraphics, which will, at the end, 
//      set the .fsInit member to TRUE.
//
// INPUTS:  None.         
//         
// RETURNS: Error code from trying to run fnGFInitResidentGraphics.  
//      If no error, return 0.
//      All error codes are negative numbers: -1000, -1001, etc.
//          
//---------------------------------------------------------------------
int fnGFVerifyGFInfoInitialized( void )
{
    int ec = 0;

    if( pGFInfo->fsInit == FALSE )
        ec = fnGFInitResidentGraphics(0);        

    if( ec )
        fnGFJustLogIt( ec, GFW_VERIFYGRFX, 0, 0 );
    return( ec );
}


/* **********************************************************************
// FUNCTION NAME:fnGFRemoveAllByType( )
//         
// PURPOSE:   This function will find and remove all graphics of a particular 
//          component type.
//
// AUTHOR:  Clarisa Bellamy 
//
// INPUTS:  bCompType - The component type to purge.
//         
// RETURNS: The number of graphics of that type that were found and deleted.
//        
// **********************************************************************/
UINT16 fnGFRemoveAllByType( UINT8 bCompType, BOOL updateFile )
{
    UINT16  fFoundAndDeleted = 0;
    UINT8 * LnkPtr = NULL_PTR;
    UINT16  wDirInx = 0;
    int     ec = 0;

    LnkPtr = fnGFGetNextDirInxByCompType( &wDirInx, bCompType, GRFX_ANY_FLAGS );
    while( LnkPtr )
    {
        fFoundAndDeleted++;
        ec = fnGFDeleteByDirInx( wDirInx , updateFile);
        // If we got an error, log it 
        if( ec )
            fnGFJustLogIt( ec, GFW_DELETALLBYTYPE, 0, 0 );

        wDirInx++;
        LnkPtr = fnGFGetNextDirInxByCompType( &wDirInx, bCompType, GRFX_ANY_FLAGS );
    }

    return( fFoundAndDeleted );
}

// ********************************************************************************
// FUNCTION NAME: fnGFInitTempDirEntry()                             
// PURPOSE:     Initializes a temporary directory entry in RAM.
//
// AUTHOR:      C.Bellamy
// INPUTS:      
//      pTempDirEntry - Ptr to the temporary directory entry to initialize.
//      pLHdr - Ptr to a link header (only) that has information about the 
//              graphic.
//      pImageWithLnk - Ptr to image, with a link header that may have some
//              information that needs to be updated.
//      bCompType - Component type of this graphic image.  Read from the 
//              image itself by the calling function.
//      bSrc - Byte indicating the source of this graphic.  This was indicated
//              in various mysterious (read "undocumented") ways in the past.
//              It is apparently important, but I am not sure why.
//
// OUTPUTS:     ec = Error code from Reading/Writing file.
//
// NOTES:   To fix an entry, This will WRITE to the directory entries in the 
//          graphics file. 
//
// --------------------------------------------------------------------------
static int fnGFInitTempDirEntry( GFENT *pTempDirEntry, const struct LinkHeader *pLHdr,
                                 const UINT8 *pImageWithLnk, UINT8 bCompType, UINT8 bSrc )
{
    int     ec = 0;
    UINT8 * pNewGraphic;
    BOOL    fStat;

    pNewGraphic = fnGrfxGetGraphic( pImageWithLnk );
    fStat = fnGrfxCopyDataNameVerFromGraphic( pNewGraphic, pTempDirEntry->gfDataName, GRFX_DATANAME_WITHVER );
    if( fStat )
    {
        pTempDirEntry->gfFlags  = GRFXFLG_PURCHASED;
        pTempDirEntry->gfFixVer = 1;
        pTempDirEntry->gfType   = bCompType;
        pTempDirEntry->gfSize   = pLHdr->SizeOfThisLinkGraphic;
        pTempDirEntry->gfID     = pLHdr->GeneratedID;
        pTempDirEntry->gfFeatID = pLHdr->FeatureCode;
        pTempDirEntry->gfSource = bSrc;
    }
    else
    {
        ec = GFE_INVALIDHEADER;
    }

    return( ec );
}

// ********************************************************************************
// FUNCTION NAME: fnGFGrfxCleanupOne()                             
// PURPOSE:     Latest cleanup. Checks that the "Cleanup Level" within the graphic
//          directory entry matches the current cleanup level.  If not, cleanup.
//           This function only cleans up the image in RAM.  It is up to the 
//          calling function to write the data back to the graphics file.
//          When a graphic is clean up to a certain level, then that level
//          is set in the directory entry (gfFixVer).
//
// AUTHOR:      C.Bellamy
// INPUTS:     
//   pDirEntry - Pointer to a directory entry in RAM
//   ppImageWithLnk - Pointer to a pointer, in RAM, to an image with a link 
//                    header.  The image is also in RAM.
//                  If this PTR == NULL_PTR, then the image can be found
//                  in our copy of the graphics data, using the directory 
//                  entry.
//   Modified - Ptr to a byte where this function will indicate the results 
//              of the cleanup.  
//
// OUTPUTS:     
//   ec = Error code from Reading/Writing file.
//   *Modified - Indicates the results of the cleanup.  
//              If this value is 0, then the calling function does not need 
//              to do anything.  Otherwise it needs to modify the graphics 
//              file: More than 1 bit may be set.
//          GRFX_CLNUP_DELETE_ENTRY     0xFF
//          GRFX_CLNUP_WRITE_DIRENTRY   0x01    (Bit map)
//          GRFX_CLNUP_WRITE_LNKHDR     0x02    (Bit map)
//
// NOTES:   This only fixes the entry in RAM, not in the graphics file 
//          itself.  The calling routing must write to the graphics file
//          if a fix is made here. 
// CLEANUP SUMMARY:
//   1) No "disabled" graphics should ever be loaded on a UIC.  If they are
//      there, they are enabled.  This was not always enforced in the past.
//      The first cleanup, was that all graphics that are not "enabled" with
//      the enable bit, shall be deleted.
//   2) Previously, when graphics were stored, the generic ID was NOT stored
//      in the link header, but only in the directory entry.  This fix 
//      copies the genID from the directory entry to the link header of the
//      graphic.
//      Their are new flags in the directory to indicate if the graphic is 
//      permanent (Not-deletable by the user), and to indicate if a graphic 
//      is an autographic (not selectable by the user, but automatically 
//      selected, as by the class.)  These flags are set based on values 
//      in the visibility byte in the graphic header (not link header). 
// 
//---------------------------------------------------------------------
static int fnGFGrfxCleanupOne( GFENT *pDirEntry, UINT8 **ppImageWithLnk, UINT8 *pModifiedFlag )
{
  int   ec = 0;
  UINT8 *   LnkPtr = NULL_PTR;
  UINT8 *   pNewGraphic;
  UINT8     pLnkHdrFix[ LNKHDRSIZE ];
  char      bVisibility;
  BOOL      fStat = FALSE;


    *pModifiedFlag = 0;
    ec = fnGFVerifyGFInfoInitialized();
    if(   (ec == 0)
       && (pDirEntry)
       && (ppImageWithLnk) )
    {
        if( pDirEntry->gfID != UNUSEDGFENTRYID )
        {
            // Check for enabled was added 2007.05.18 by cBellamy.
            // ALL GRAPHICS on the unit should be enabled.  Delete any
            // graphics that are not. 
            if( pDirEntry->gfFixVer < GRFX_CLEANUP_VER )
            {
                // Prior to FixVer1, graphics that were added to an existing .GAR file
                //  had FeatID set to 0xFF.  
                if(   (pDirEntry->gfFixVer < 1)
                   && ((pDirEntry->gfFlags & GRFXFLG_PURCHASED) == 0) 
                   && (pDirEntry->gfFeatID == 0xFF))
                {
                    *pModifiedFlag = GRFX_CLNUP_DELETE_ENTRY;
                }
                else
                {
                    if( pDirEntry->gfFixVer < 2 )
                    {
                        // Make sure this is set.
                        pDirEntry->gfFlags |= GRFXFLG_PURCHASED;
                        LnkPtr = *ppImageWithLnk;
                        if( LnkPtr == NULL_PTR )
                        {
							// The dir entry is in our copy of the directory.
                            LnkPtr = fnGFGetLnkPtrByDirPtr( pDirEntry );
                            *ppImageWithLnk = LnkPtr;
                        }
						
                        if( LnkPtr != NULL_PTR )
                        {
                            // This is a valid graphic...
                            (void)memcpy( pLnkHdrFix, LnkPtr, LNKHDRSIZE );
							
                            // Find out if the GenID in the LnkHdr matches the directory entry.
                            if(  pDirEntry->gfID != fnGrfxGetGenID( LnkPtr ) )
                            {
                                // This lnkHdr needs to be fixed.  
                                // Change the ID to match the dir entry.
                                (void)memcpy( pLnkHdrFix + COMPONENTID_OFFSET, &(pDirEntry->gfID), sizeof( UINT16 ) );
								
                                // Copy it into the local ram copy of the graphic.
                                (void)memcpy( LnkPtr, pLnkHdrFix, LNKHDRSIZE );
                                *pModifiedFlag = GRFX_CLNUP_WRITE_LNKHDR;
                            }
							
                            // Make sure the directory flags match the info in the graphic.
                            pNewGraphic = fnGrfxGetGraphic( LnkPtr );
                            fStat = fnFlashGetGCvisibility( pNewGraphic , &bVisibility);
                            if( fStat == SUCCESSFUL )
                            {
                                if( bVisibility & VSBLTY_DONT_DELETE )
                                {
                                    pDirEntry->gfFlags |= GRFXFLG_PERMANENT;
                                }
								
                                // Remove all but visibility bits.
                                bVisibility &= VISIBILITYMASK; 
                                if( bVisibility == VSBLTY_AUTOGRAPHIC )
                                {
                                    // If it is an autographic, set the autographic flag.
                                    pDirEntry->gfFlags |= GRFXFLG_AUTOGRFX;    
                                }
                                else
                                {
                                    // Clear the autographic flag.
                                    pDirEntry->gfFlags &= ~GRFXFLG_AUTOGRFX;
                                }
                            }
                            else
                            {
                                // Can't read from graphic.
                                ec = GFE_INVALIDHEADER;
                            }
							
                            pDirEntry->gfFixVer = 2;
                            *pModifiedFlag |= GRFX_CLNUP_WRITE_DIRENTRY; 
                        } // End of got LnkPtr
                    }  // Endof FixVer < 2
/*
                    if( FixVer < 3 )
                    {
                        // When GRFX_CLEANUP_VER  is  3
                        // The next fix we need to do goes here.
                    }
*/
                } // End of dir entry is not deleted here.                
            } // End of FixVer < Current Fix dir.
        } // End if dir entry is used.
    } // End GFInfoInitialized.
	
    // Log any error...
    if( ec )
        fnGFJustLogIt( ec, GFW_GRAPCLEANUPONE, 0, GRFX_CLEANUP_VER );
    
    return( ec );
}


// ********************************************************************************
// FUNCTION NAME: fnGFGrfxCleanup()                             
// PURPOSE:     Goes thru each entry in the directory.  For each directory entry,
//              calls fnGFGrfxCleanupOne to make sure that the directory entry
//              and the graphic are cleaned up to the latest level.  If a cleanup 
//              is made, and this function will write the RAM data back to the 
//              graphics file.
//
// AUTHOR:      C.Bellamy
// INPUTS:      None
//
// OUTPUTS:     ec = Error code from Reading/Writing file.
//
// NOTES:   To fix an entry, This will WRITE to the directory entries in the 
//          graphics file. 
//
// MODIFICATION HISTORY:
//      02-18-14 Renhao Modified function fnGFGrfxCleanup() to fix fraca 224416.
//---------------------------------------------------------------------
int fnGFGrfxCleanup( void )
{
  int   ec = 0;
  GFENT *   pDirEntry;
  UINT8 *   LnkPtr = NULL_PTR;
  UINT16    wDirSize;
  UINT16    wThisDirInx = 0;
  UINT8     bDirEntryModified = FALSE;


    ec = fnGFVerifyGFInfoInitialized();
    if( ec == 0 )
    {
        wDirSize = fnGFGetDirLen();
		
        // Get pointer to first entry in directory.
        pDirEntry = fnGFGetFirstDirPtr();
        if( pDirEntry )
        {
            while(   (wThisDirInx < wDirSize)
                  && (ec == 0) )
            {
                bDirEntryModified = FALSE;
                LnkPtr = NULL_PTR;
                ec = fnGFGrfxCleanupOne( pDirEntry, &LnkPtr, &bDirEntryModified );
                if( ec == 0 )
                {
                    if( bDirEntryModified == GRFX_CLNUP_DELETE_ENTRY )
                    {
                       ec = fnGFDeleteByDirInx( wThisDirInx , TRUE);
                    }
                    else
                    {
                        if( bDirEntryModified & GRFX_CLNUP_WRITE_LNKHDR )
                        {
                            // Write the fix into the grapfile.
                            ec = fnGFWriteOneGrapHeader( wThisDirInx, LnkPtr );
                        }
						
                        if(   (ec == 0)
                           && (bDirEntryModified & GRFX_CLNUP_WRITE_DIRENTRY) ) 
                        {
                            ec = fnGFWriteOneDirEntry( wThisDirInx, pDirEntry );                                
                        }
                    }
                }
				
                if( ec == 0 )
                {
                    // Check the next one.
                    wThisDirInx++;
                    pDirEntry++;
                }
            } // End of while (wThisDirInx < wDirSize), loop through dir entries.
			
            // Indicate the last level of cleanup we did.   Useful only for some diagnostics.
            //  This is not checked at the beginning because a .GAR file may have been deleted and replaced,
            //  so the flags IN the file are the ones that count.
            if( ec == 0 )
            {
                GrfxCleanupVer = GRFX_CLEANUP_VER;                
            }
        } // End, did find first directory entry.    
    }  // End Ram version is valid.

    // Log any error...
    if( ec )
    {
        fnGFJustLogIt( ec, GFW_GRAPCLEANUP, 0, GRFX_CLEANUP_VER );
    }
    
    return( ec );
}


/* **********************************************************************
// FUNCTION NAME: fnGFAddOneImageInMemory( pLHdr, pImageWithLnk, bSrc )
//
// PURPOSE:     Add a graphic to the graphics filein Ram. 
//              No duplicate ID is checked. So if that is a criteria the 
//              duplicate must be checked before hand
//
// INPUTS: 
//  pLHdr           - Pointer to link header with generated ID 
//  pImageWithLnk   - Ptr to graphic with link header. 
//  bSrc            - How is this graphic being added: See GRFXSRC_ defs in grapext.h
//
// RETURNS: Error Code: 0 = No Error.
//
// **********************************************************************/
int fnGFAddOneImageInMemory( const struct LinkHeader *pLHdr , UINT8 *pImageWithLnk, UINT8 bSrc )
{
	int		  ec = 0;
	UINT8     bCompType;
	GFENT     TempDirEntry;
	UINT8     ubModified = 0;

    bCompType = fnGrfxGetCompType( pImageWithLnk );
    switch( bCompType )
    {
        case IMG_ID_TOWN_CIRCLE:
        case IMG_ID_TOWN_CIRCLE_2:
            // IF WE ARE ADDING A TOWN CIRCLE, WE MUST DELETE THE OLD ONE FIRST
            (void)fnGFRemoveAllByType( bCompType , FALSE);
            break;
        default:
            break;
    }

    if( fnGrfxIsNonEmdCompType( bCompType ) == TRUE )
    {
        // Initialize a directory Entry 
        ec = fnGFInitTempDirEntry( &TempDirEntry, pLHdr, pImageWithLnk, bCompType, bSrc );
        if( ec == 0 )
        {           
            ec = fnGFGrfxCleanupOne( &TempDirEntry, &pImageWithLnk, &ubModified );
            if( ec == 0 )
            {
                // Add the graphic to the file
                ec = fnGFAddGraphicInMemory(pImageWithLnk, &TempDirEntry );
            }

			if( ec == 0 )
            {
                // set the TC_INSTALLED_BIT if the graphic was a Town Circle
                if ((bCompType == IMG_ID_TOWN_CIRCLE) || (bCompType == IMG_ID_TOWN_CIRCLE_2))
                {
                    CMOSPCNParams.zipFlags |= TC_INSTALLED_BIT;                 /* set the bit that will be stored in   */
                }
			}
		}
    }
    else  // This component type is in the EMD.
        ec = GFE_INVALIDGRAPHICTYPE;

    if( ec )
    {
        fnGFJustLogIt( ec, GFW_ADDIMAGE, 0, 0 );
    }
  
    return( ec );
}


/* **********************************************************************
// FUNCTION NAME: fnGFUpdateGraphic( unsigned flags , unsigned char *image , int imageSize )
// PURPOSE:     Add a new graphic to the system.
//
//      From DLA, single files with link headers but not directory entries. 
//
// AUTHOR: George Monroe
//
// INPUTS: flags to determine what to do if the graphic exists
//         address of the image
//         size of the image including link header
// RETURNS:
// NOTES:  *.GFX files from the infrastructure already contain a link header 
//   When checking to see if this graphic exists already, all the graphics 
//      of the same type are checked.  If any have a matching dataName AND 
//      Version, then we say it already exists, otherwise, this graphic will 
//      be loaded as a new graphic.
// **********************************************************************/
int fnGFUpdateGraphic( unsigned DupFlags , UINT8 *pImageWithLnk , UINT32 imageSize )
{
  int ec = 0;
  T_GRF_LNK_HDR     lh;
  BOOL      fStat;
  UINT8     dataName[ DATANAMEVERSIZE ];
  UINT8     asciiDataName[(DATANAMEVERSIZE+1) * 2];
  UINT16    wGenId;
  UINT8     bCompType;
  UINT8 *   pMyGraphic = NULL_PTR;
  UINT8 *   LnkPtr = NULL_PTR;     // Pointer to graphic with same dataNameVer in graphics file.
  
  bCompType =  fnGrfxGetCompType( pImageWithLnk );
  
  lh.SizeOfThisLinkGraphic = imageSize;
  lh.OffsetToNextGraphic = 0;
  lh.Context = 0;
  lh.FeatureCode = 0xff;
  // !!! lh.Context =  pImageWithLnk[ COMPONENTCONTEXT_OFFSET ];
  // !!! lh.FeatureCode = pImageWithLnk[ COMPONENTFEATURE_OFFSET ];
  
    // Get a copy of the data-name-ver from the new graphic into local buffer.
    (void)memset( dataName, 0, sizeof( dataName ) );
    (void)memset( asciiDataName, 0, sizeof( asciiDataName ) );
    pMyGraphic = fnGrfxGetGraphic( pImageWithLnk );
    fStat = fnGrfxCopyDataNameVerFromGraphic( pMyGraphic, dataName, GRFX_DATANAME_WITHVER );
    if( fStat )
    {
		(void) fnGrfxMakeDataNameVerIntoAscii(dataName, GRFXDATANAMEANDVERSION, asciiDataName );        
		
		// Check to see if a graphic with this data-name-ver exists already.
        LnkPtr = fnGrfxFindByDataNameVer( bCompType, dataName, GRFX_DATANAME_ANDVER_LEN );
        if( LnkPtr == NULL_PTR )
        {   // No graphic found with this dataName (and component type).
            wGenId = fnGrfxGetAvailableGenID( bCompType );
			dbgTrace(DBG_LVL_INFO,"Adding id: %d, Dataname - %s", wGenId, asciiDataName);            
			lh.GeneratedID = wGenId;
            fnPutAILinkHeader( &lh , pImageWithLnk, TRUE );
            ec = fnGFAddOneImageInMemory( &lh , pImageWithLnk, GRFXSRC_DOWNLOADED );

        }
        else
        {
            // Graphic already exists what should we do ???
            if( DupFlags & GF_UPDATE )
            {
                // Get the GenID from the old graphic.
                wGenId = fnGrfxGetGenID( LnkPtr );
				dbgTrace(DBG_LVL_INFO,"Replacing id: %d, Dataname - %s", wGenId, asciiDataName);            
                // Delete the old graphic
                ec = fnGFDeleteGraphicById( bCompType, wGenId, FALSE );
                if( ec == 0 )
                {
                    // Add the new graphic with the old GenID .
                    lh.GeneratedID = wGenId;
                    fnPutAILinkHeader( &lh , pImageWithLnk , TRUE);
                    ec = fnGFAddOneImageInMemory( &lh , pImageWithLnk, GRFXSRC_DOWNLOADED );
                }
            }
            else
                ec = GFE_DUPLICATEGRAPHICERROR;    
        }
    }
    else
        ec = GFE_INVALIDHEADER;    

    if( ec )
    {
        fnGFJustLogIt( ec, GFW_UPDATEGRFX, 0, 0 );
    }

    return( ec );
}

#if 0
/* **********************************************************************
// FUNCTION NAME: fnGFCopyCustGraphics( INT32 fd2 , char *fname )
// PURPOSE:     Copy one file to another one.
//
// FLOW:
//  Opens fname file.
//  Reads it in, in 1000-byte chunks, and writes it to fd2 file.
//  Depending upon the calling function, this could APPEND fname to the file.
//  (But so far, the calling function doesn't do that.)
//
// AUTHOR: George Monroe
//
// INPUTS: 
//  fd2   - Handle to ALREADY OPENED file to be written with customer's graphics
//              (The GFMRGTempFile file.)
//  fname   - Ptr to file name of customer's graphics (.GAR file)
//
// RETURNS: Error code  (0 = no error.)
//
// **********************************************************************/
static int fnGFCopyCustGraphics( INT32 fd2, char *fname )
{
  int ec = 0,len,wlen;
  INT32     fd;
  char *    allocbuf = NULL_PTR;
  unsigned  allocsize = 0;

    // need to add the graphic to the existing graphics
    fd = NU_Open( fname , PO_RDWR|PO_BINARY , PS_IWRITE | PS_IREAD );
    if(fd >= 0)
    {
      // allocate a buffer
      allocbuf = (char *)malloc( DEFCOPYBUFSIZE );
      if( allocbuf )
      {
        allocsize = DEFCOPYBUFSIZE;
        // Copy over existing graphic images to the merge file
        len = NU_Read( fd, allocbuf, allocsize );
        while( len == allocsize )
        {
          wlen = NU_Write( fd2, (char *)allocbuf, allocsize );
          if( wlen != allocsize )
          {
            ec = GFE_FILEWRITEERROR;
            if( len < 0 )
              ec = len;
            break;
          }
          len = NU_Read( fd, allocbuf, allocsize );
        }
        // copy any remaining bytes over to the merge file
        if( ec == 0 && len > 0 )
        {
          wlen = NU_Write( fd2, (char *)allocbuf, len );
          if( wlen != len )
          {
            ec = GFE_FILEWRITEERROR;
            if( len < 0 )
              ec = len;
          }
        }
        // close the customers graphics file
        (void)NU_Close( fd );
        free(allocbuf);
      }
      else
        ec = GFE_MALLOCFAILED;
    }
    else
      ec = GFE_NOGRAPHICSFILEFOUND;

    if( ec )
      fnGFJustLogIt( ec, GFW_COPYCUSTGRFX, 0, 0 );

    return( ec );
}
    sp = fnFindFirstFileWithExt( NULL, GFFILENAMEEXT , ( unsigned char *) fname );
          sp = fnFindFirstFileWithExt( NULL, GFFILENAMEEXT , ( unsigned char *) fname );
          sp = fnFindFirstFileWithExt( NULL, GFUPDFILENAMEEXT , ( unsigned char *) fname );
#endif

/* **********************************************************************
// FUNCTION NAME: fnGFPowerUpCheck( void )
// PURPOSE: Perform any power fail recovery related to graphics
//
// AUTHOR: George Monroe
//
// INPUTS: 
//         
//         
// RETURNS:
//        
// **********************************************************************/
int fnGFPowerUpCheck( void )
{
  int ec = 0;
  INT32 fd;
  char fname[64];
  unsigned char *sp;

    // Just delete any temporary graphics files.
    fnCheckFileSysCorruption(unlink( GFTEMPNAME ));
    fnCheckFileSysCorruption(unlink( GFMERGETEMPNAME ));
    // Did we finish merging, but not yet change the name of the GFMerge file ?
    fd = NU_Open(GFMERGENAME , PO_RDONLY , PS_IWRITE | PS_IREAD );
    if(fd >= 0)
    {
        fnCheckFileSysCorruption(NU_Close( fd ));
        do
        {
            // Remove any existing graphic (.GAR) files
            sp = fnFindFirstFileWithExt( GFFILENAMEEXT , ( unsigned char *) fname );
            if( sp != NULL_PTR )
                fnCheckFileSysCorruption(unlink( fname ));
        }while( sp != NULL_PTR );
        do
        {
            // Remove any existing graphic update (.GAU) files
            sp = fnFindFirstFileWithExt( GFUPDFILENAMEEXT , ( unsigned char *) fname );
            if( sp != NULL_PTR )
                fnCheckFileSysCorruption(unlink( fname ));
        }while( sp != NULL_PTR );
        // Rename GFMerge file to graphics file: (GFGraph.GAR)
        (void)strcpy(fname , GFFILENAME );
        (void)strcat( fname , GFFILENAMEEXT );
        ec = NU_Rename( GFMERGENAME , fname );
        fnCheckFileSysCorruption(ec);
    }
    else
        fnCheckFileSysCorruption(fd);
    
    // check if the grow file is there
    fd = NU_Open(GFGROWNAME , PO_RDONLY , PS_IWRITE | PS_IREAD );
    // If no error we have a grow file else the grow file doesn't exist
    if( fd >= 0 )
    {
        fnCheckFileSysCorruption(NU_Close( fd ));
        sp = fnFindFirstFileWithExt( GFFILENAMEEXT , ( unsigned char *) fname );
        if( sp == NULL_PTR )
        {
            (void)strcpy(fname , GFFILENAME );
            (void)strcat(fname , GFFILENAMEEXT );
        }
        // The grow file exists. Finish deleteing the old graphics file
        fnCheckFileSysCorruption(unlink(fname ));
        // Rename the grow file to the actual graphics file
      	//TODO - check protection - replaced FFSProtRename
        ec = NU_Rename( GFGROWNAME , fname );
        fnCheckFileSysCorruption(ec);
    }
    else
    {
        ec = 0;
        fnCheckFileSysCorruption(fd);
    }

    if( ec )
        fnGFJustLogIt( ec, GFW_POWERUPCHECK, 0, 0 );
    
    return( ec );
}
  
// **********************************************************************
// FUNCTION NAME:   fnGFGetGraphicsListAllTypes
//
// PURPOSE:     Searches the graphics file for active graphics (those that
//          have a valid GenID.)  Puts the CompTypes in one array and the 
//          GenIDs in another, and returns the total count.
//
// AUTHOR:  Clarisa Bellamy
// INPUTS:  pDestCompTypes - Pointer to list of UINT8s to store the component types.
//          pDestGenIDs - Pointer to list of UINT16s to store the GenIDs.
//          wMaxReturn - UINT16, max number of graphics to find.  
//                      0 = Find all.  So we will ALWAYS find at least 1.
//          fEnabledOnly - BOOL, if TRUE, only find graphics marked as "purchased". 
//         
// RETURNS: Number of directory entries that do not have the graphics ID 
//          set to UNUSEDGFENTRYID.   Caller may filter using the flag byte
//          in the directory entry. For an explanation search on TestFlag in 
//          grapext.h
//
//  NOTES:
//          If caller passes a NULL_PTR for the destination args, then it
//          doesn't want that data.
//          Stops counting when wMaxReturn is found. 
//          A 0 value for maxReturn means count them all.
//
//---------------------------------------------------------------------
UINT16 fnGFGetGraphicsListAllTypes( UINT8 *pDestCompTypes, UINT16 *pDestGenIDs, 
                                    UINT16 wMaxReturn, UINT16 uwFlagTest )
{
  UINT16    wDirInx;
  UINT16    wNumFound = 0;
  GFENT *   pDirEntry;
  UINT16    wDirSize;
  BOOL      forever;

    wDirSize = fnGFGetDirLen();

    // Get pointer to first entry in directory.
    pDirEntry = fnGFGetFirstDirPtr();
    if( pDirEntry )
    {
        forever = TRUE;
        for( wDirInx = 0; forever && (wDirInx < wDirSize) ; wDirInx++, pDirEntry++ )
        {
            // If we found another used entry...
            if( pDirEntry->gfID != UNUSEDGFENTRYID )
            {
                if( uwFlagTest && (fnGFCheckFlagsByDirPtr( pDirEntry, uwFlagTest ) == FALSE) )
                {
                    // skip
                }
                else
                {
                    // Store what is asked for, and increment the count.
                    if( pDestCompTypes != NULL_PTR )
                        pDestCompTypes[ wNumFound ] = pDirEntry->gfType;
                    if( pDestGenIDs != NULL_PTR )
                        pDestGenIDs[ wNumFound ] = pDirEntry->gfID;
                    wNumFound++;
                    // Check for count limiter.
                    if( (wMaxReturn == 0)  ||  (wNumFound < wMaxReturn) )
                    {
                        // Keep looking
                    }
                    else
                        // If we've run out of room, stop checking.
                        forever = FALSE;
                }
            }
        }
    }
    return( wNumFound );
}


// **********************************************************************
// FUNCTION NAME:   fnGFGetDataNamePtrByID
//
// PURPOSE:     Finds a graphic wiht the matching CompType and GenID, and
//          returns a pointer to the DataNameVer (12-UINT8) array.
//
// AUTHOR:  George Kulik, Clarisa Bellamy
//
// INPUTS:  bCompType - byte, Type of component to find.
//          GenID - word, graphic ID that must also match (A.K.A. GraphicID.)
//          fEnabledOnly - Flag: GRFX_ENABLED_ONLY, Only check enabled graphics.
//                               GRFX_ANY_FLAGS, check all graphics.
//
//         
// OUTPUTS:     Returns Pointer to the DataNameVer array in the directory 
//          entry for the found graphic.
//
//  NOTES:  bCompType must NOT be ANY_COMPONENT_TYPE.
//          wGenId must NOT be 0.
//
//---------------------------------------------------------------------
UINT8 * fnGFGetDataNamePtrByID( UINT8 bCompType, UINT16 wGenId, UINT16 uwFlagTest )
{
    UINT8 *pDataName = NULL_PTR;
    GFENT *pDirEntry;
    UINT16  wDirInx;

    // Find an entry in the Graphics file directory that matches the Component type and GenId.
    pDirEntry = fnGFGetDirInxByID( bCompType, wGenId, uwFlagTest, &wDirInx );
    // If found, get a pointer to the DataName.
    if( pDirEntry )
        pDataName = fnGFGetDataNamePtrByDirInx( wDirInx ); 

    return( pDataName );
}

// **********************************************************************
// FUNCTION NAME:   fnGFGetDirInxByDataNameVer
//
// PURPOSE:     Finds the first active directory index with dataNameVer 
//          that matches the first bLen chars of the reference DataNameVer.
//
// AUTHOR:  George Kulik, Clarisa Bellamy
//
// INPUTS:  pDirInx - Pointer to Index into directory to start looking at.
//          bCompType - byte, Type of component to find.
//          pRefDataNameVer - Pointer to DataNameVer array to match.
//          bLen - Number in bytes in array to compare.
//          fEnabledOnly - Flag: GRFX_ENABLED_ONLY, Only check enabled graphics.
//                               GRFX_ANY_FLAGS, check all graphics.
//
// OUTPUTS: IF match found:  Returns the Pointer to graphic, with link header,
//                      and sets the value of pDirInx. 
//          If no match found: returns NULL_PTR, and does not copy anything to the 
//                      pDirInx.
//
//  NOTES:  Only looks at enabled graphics. 
//          This function does NOT work for EMD graphic component types.
//
//---------------------------------------------------------------------
UINT8 *fnGFGetDirInxByDataNameVer( UINT16 *pDirInx, UINT8 bCompType, const UINT8 * pRefDataNameVer, 
                                   UINT8 bLen, UINT16 uwFlagTest )
{
    UINT16  wDirInx = 0;
    UINT8   *LnkPtr = NULL_PTR;

    // Just a precaution....
    if( bLen > GRFX_DATANAME_ANDVER_LEN )
        bLen = GRFX_DATANAME_ANDVER_LEN;

    wDirInx = *pDirInx;
    if( (bCompType == ANY_COMPONENT_TYPE)  ||  (fnGrfxIsNonEmdCompType( bCompType )) )
    {
        LnkPtr = fnGFGetNextDirInxByCompType( &wDirInx, bCompType, uwFlagTest );
        
        while( LnkPtr != NULL_PTR )
        {
            // For each graphic of this comptype in the graphics file... 
            // check the DataNameVer
            if( fnGFCheckDataNameVerByDirInx( wDirInx, pRefDataNameVer, bLen ) )
            {
                // Found it!
                *pDirInx = wDirInx;
                return( LnkPtr );
            }
            // Not found yet... Check Next entry in Graphics file directory.
            wDirInx++;
            LnkPtr = fnGFGetNextDirInxByCompType( &wDirInx, bCompType, uwFlagTest );            
        }        
    }

    // No match found...
    return( NULL_PTR );
}


// **********************************************************************
// FUNCTION NAME:   fnGFGetDirInxByUniName
//
// PURPOSE:     Finds the first active directory index with Unicode name 
//          that matches the first bLen chars of the reference Unicode Name.
//
// AUTHOR:  Clarisa Bellamy
//
// INPUTS:  
//      pDirInx -   Pointer to Index into directory to start looking at.
//      bCompType - byte, Type of component to find.
//      pRefUniName - Pointer to Unistring Name to match.
//      bLen -      Number in bytes in name to compare.
//      uwFlagTest - word: low byte are the bits to test.
//                        high byte are the required values of the bits to test.        
//                  0 includes everything.
//
// OUTPUTS: IF match found:  Returns the Pointer to graphic, with link header,
//                      and sets the value of pDirInx. 
//          If no match found: returns NULL_PTR, and does not copy anything to the 
//                      pDirInx.
//
//  NOTES:  Only looks at names in first language. 
//          This function does NOT work for EMD graphic component types.
//
//---------------------------------------------------------------------
UINT8 *fnGFGetDirInxByUniName( UINT16 *pDirInx, UINT8 bCompType, const UNICHAR * pRefUniName,
                               UINT8 bLen, UINT16 uwFlagTest )
{
    UINT16      wDirInx = 0;
    UINT8       *LnkPtr = NULL_PTR;
    UNICHAR     pTestName[ GRFX_UNINAME_MAX_COMPARE_LEN +1 ];


    // Just a precaution.... This is an arbitrary number, but we shouldn't have names this long.
    if( bLen > GRFX_UNINAME_MAX_COMPARE_LEN  )
        bLen = GRFX_UNINAME_MAX_COMPARE_LEN;

    wDirInx = *pDirInx;
    if( (bCompType == ANY_COMPONENT_TYPE)  ||  (fnGrfxIsNonEmdCompType( bCompType )) )
    {
        LnkPtr = fnGFGetNextDirInxByCompType( &wDirInx, bCompType, uwFlagTest );
        
        while( LnkPtr != NULL_PTR )
        {
            // For each graphic of this comptype in the graphics file... 
            // check the UniName
            (void)fnGrfxCpyUniName( pTestName, LnkPtr, 0, GRFX_UNINAME_MAX_COMPARE_LEN );
            if( unistrcmp( pTestName, pRefUniName ) == 0 )      // If match found
            {
                // Found it!
                *pDirInx = wDirInx;
                return( LnkPtr );
            }
            // Not found yet... Check Next entry in Graphics file directory.
            wDirInx++;
            LnkPtr = fnGFGetNextDirInxByCompType( &wDirInx, bCompType, uwFlagTest );            
        }        
    }

    // No match found...
    return( NULL_PTR );
}



// ********************************************************************************
// FUNCTION NAMES: fnGFJustLogIt()  
//                  
// PURPOSE:     Log a graphics error into the CMOSSystemErrorLog.
//          The type of graphic error is indicated by the range of the error code.
//
// INPUTS:  iErrorCode - A code indicating the type of error that occured.
//          bWhere - 1-byte code indicating the function that reported the error.
//          bInfo1, bInfo2 - bytes to store in an informational entry.
//                          The first is GFI_x.  The meaning of the second depends
//                          upon the value of the first.
//
// OUTPUTS:   None
//
// NOTES:    
//      Errors in the range -1 thru -1000 are FFS error codes.  
//          2C, 02, ww, ff, ff
//      Errors less than -1000 are Graphics File specific error codes:
//          2C, 01, ww, ee, ee
//      Error code 0 indicates informational entry:
//          2C, 00, ww, ii, ii
//
//      Where:  
//          ww = Code indicating the function where the error occurred.
//          iiii = information code (GFI_x )
//          eeee = Graphics Error Code (GFE_x ) converted to 1-based positive number.
//          ffff = FFS Error Code (FFSx ) converted to positive number.
//  
// AUTHOR:  2000.09.26  Clarisa Bellamy
// MODS:
//
//-----------------------------------------------------------------------------
static void fnGFJustLogIt( int iErrorCode, UINT8 bWhere, UINT8 bInfo1, UINT8 bInfo2 )
{
  int   iLogErrorCode = 0;
  UINT8 bErrorType = GFEC_INFO;

    // If the error code is not zero, then find out what kind of error it is
    //  and convert the error code to a smaller, 1-based range.
    if( iErrorCode )
    {
        if( iErrorCode < -1000 )
        {
            bErrorType = GFEC_ERROR;
            iLogErrorCode = -1000 - iErrorCode;
        }
        else if( iErrorCode < 0 )
        {
            bErrorType = GFEC_FFSERROR;
            iLogErrorCode = 0 - iErrorCode;
        }
        else
        {
            bErrorType = GFEC_UNKNOWNERRTYPE;
            iLogErrorCode = iErrorCode;
        }
        bInfo1 = (UINT8)((iLogErrorCode/256)%256);
        bInfo2 = (UINT8)(iLogErrorCode%256);
    }
    // If iErrorCode is 0, leave bErrorType as INFO, and use bInfo1 and bInfo2 input args.

    fnLogSystemError( ERROR_CLASS_GRAPHIC, bErrorType, bWhere, bInfo1, bInfo2 );    

    return;
}
                               

// ********************************************************************************
// FUNCTION NAME: fnGrfxIsItThere()                                
// PURPOSE:     Given a component Type and GenID, make sure the graphic is there.
//
// AUTHOR:      CBellamy
// INPUTS:  
//     bCompType    - byte, Type of component to find.
//     GenID        - word, graphic ID that must also match (A.K.A. GraphicID.)
//     uwFlagTest   - word: low byte are the bits to test.
//                          high byte are the required values of the bits to test.        
//                          0 includes everything.
// OUTPUTS:     
//      Returns TRUE if the graphic is found in the .GAR file.
//
// NOTES:  
// HISTORY:
//  2008.05.22  Clarisa Bellamy - Initial revision.
//---------------------------------------------------------------------
BOOL fnGrfxIsItThereById( UINT8 bCompType, UINT16 wGenId, UINT16 uwFlagTest )
{
    UINT16  wDirInx;

    if( fnGFGetDirInxByID( bCompType, wGenId, uwFlagTest,&wDirInx ) == NULL_PTR )
        return( FALSE );
    else 
        return( TRUE );
}


// ********************************************************************************
// FUNCTION NAME: fnInstallGraphics()                                
// PURPOSE:     Checks to see if there are any graphics files to add to the >GAR file.
//				Base install log will be updated with SUCCESS or FAILED for each file found.
//
// INPUTS:  
//     None.
// OUTPUTS:     
//      None.
//
// NOTES:  
//---------------------------------------------------------------------
void fnInstallGraphicsOld(void)
{
	int  ec = 0;
	STATUS haveFile = NU_SUCCESS;
	char pattern[128];
	char fileName[128];
	char* fileName_p = fileName;
	DSTAT	dstat ;

	sprintf(pattern, "%s:\\%s\\*%s", DRIVE_LETTER, GRAPHIC_INSTALL_PATH, GFINSTALLEXT);
	haveFile = NU_Get_First(&dstat, pattern);

	while (haveFile == NU_SUCCESS)
	{
		sprintf(fileName_p, "%s:\\%s\\%s", DRIVE_LETTER, GRAPHIC_INSTALL_PATH, dstat.lfname);
		dbgTrace(DBG_LVL_INFO,"Located new graphic file %s", dstat.lfname);
		
		errno = 0;
		ec = validateGfxFiles(fileName_p);
		if(ec == 0)
		{
			if(!installGfxFiles(fileName_p))
			{
				//Probably will do something else someday too, but for now...
				dbgTrace(DBG_LVL_INFO,"file %s not installed", fileName_p);
				ec = -1;
			}
			else
			{
				dbgTrace(DBG_LVL_INFO,"file %s installed", fileName_p);
			}
		}

		//Add to install log
		if(ec == 0)
		{
			char installStatus[]= "SUCCESS";
			UpdateBaseInstallLog(dstat.lfname, installStatus);
		}
		else
		{
			char installStatus[]= "FAILED";
			UpdateBaseInstallLog(dstat.lfname, installStatus);
		}

		//Remove file
		fnCheckFileSysCorruption( unlink(fileName_p) ); 

        (void) NU_Done(&dstat);

		NU_Sleep(1);

		//Locate next file to try
		haveFile = NU_Get_First(&dstat, pattern);
	}

    if (haveFile != NU_SUCCESS)
        fnCheckFileSysCorruption(haveFile);        
}

int fnGFWriteGraphicFile(void)
{
	INT32     fd2;
	int   	ec = 0,rc = 0,len,wlen;

	// Read The header in the graphics file to determine the current size of
	// the directory. Determine the new size of the directory in bytes. 
	// When the new graphic directory entries are written we adjust the offsets
	// of each graphic current entry to account for the fact the graphic
	// will move deeper into the file because of the increase in directory size
	// computing the new offsets at the time we write the new directory allows
	// us to make a single pass to create the new graphics file
	// position to the start if the file


	// we know we are probably using the internal file system
	// therefore we generate the file will only writting sequencially
	// rather than try to perform random I/O
	fd2 = NU_Open(GFTEMPNAME , PO_CREAT|PO_WRONLY|PO_TRUNC , PS_IWRITE | PS_IREAD);
	if( fd2 >= 0 )
	{ 
		len = NU_Write( fd2, (char *)&pGFInfo->rgfHdr, sizeof(GFHDR) );
		if( len == sizeof(GFHDR) )
		{
			wlen = NU_Write( fd2, pGFInfo->rgfEntries, pGFInfo->rgfCurrSize );
			if( wlen != pGFInfo->rgfCurrSize )
			{
				ec = GFE_FILEWRITEERROR;
				if( wlen < 0 )
				{
					ec = len;
					fnCheckFileSysCorruption(ec);
				}
			}
		}
		else
		{
			ec = GFE_FILEWRITEERROR;
			if( len < 0 )
			{
				ec = len;
				fnCheckFileSysCorruption(ec);
			}
		}

		rc = NU_Close( fd2 );
		if( ec == 0 )
		{
			if( rc == 0 )
			{
				//TODO - check protection - replaced FFSProtRename
				ec = NU_Rename( GFTEMPNAME, GFGROWNAME );
				if(ec != 0)
				{
					rc = ec;
					fnCheckFileSysCorruption(rc);  //check the return value of NU_Rename
					fnCheckFileSysCorruption(unlink( GFTEMPNAME ));
				}
			}
			else
			{
				fnCheckFileSysCorruption(rc);   //check the return value of NU_Close
				fnCheckFileSysCorruption(unlink( GFTEMPNAME ));
			}
		}
	}
	else
	{
		fnCheckFileSysCorruption(fd2);
		fnCheckFileSysCorruption(unlink( GFTEMPNAME ));
	}

	if(ec == 0)
		ec = rc;

    if( ec )
      fnGFJustLogIt( ec, GFW_GROWGRAPHICS, 0, 0 );
    
    //TODO - clean up return code generation
  
  return( ec );
} 

void GarbageCollectGarFile(void)
{
	GFENT *   pDirEntry;
	UINT32    startOffset;
	UINT32    nextOffset = 0xFFFFFFFF;
	UINT32	  nextSize = 0;
	UINT32    difference;
	UINT32    bytesToMove;
	UINT32 	  i;
	BOOL	  done = FALSE;
	UINT8    *pAddr;
	
	pAddr = pGFInfo->allocAddr - sizeof(GFHDR);
	startOffset = pGFInfo->rgfImages - pGFInfo->allocAddr + sizeof(GFHDR); //startAddr is 8 bytes greater than actual offset
	
	while (done == FALSE)
	{
		for(i = 0; i < DEFAULT_GF_DIR_SIZE; i++)
		{
			pDirEntry = fnGFGetDirPtrByDirInx( i );
			if(pDirEntry->gfID != UNUSEDGFENTRYID)
			{
				if((pDirEntry->gfOffset >= startOffset) && (pDirEntry->gfOffset < nextOffset))
				{
					nextOffset = pDirEntry->gfOffset;
					nextSize = pDirEntry->gfSize;
				}
			}
		}

		if(nextOffset == 0xFFFFFFFF)
		{
			done = TRUE;
		}
		else
		{
			//move on up
			//rgfCurrSize just include directory entry size and image size, but graphic offset 
			//nextOffset is from the beginning of the file so we need to include the size of 
			//header to calculate the correct bytes to move
			bytesToMove = pGFInfo->rgfCurrSize + sizeof(GFHDR) - nextOffset;
			difference = nextOffset-startOffset;

			//garbage collection is only done if there is a deleted graphic to be removed
			//Don't remove the pad bytes (less than 4 bytes) for the graphics in intial Gar file
			if(difference > 3)
			{
				pGFInfo->fsUpdated = TRUE;
				memcpy(pAddr+startOffset, pAddr+nextOffset, bytesToMove);
				for(i = 0; i < DEFAULT_GF_DIR_SIZE; i++)
				{
					pDirEntry = fnGFGetDirPtrByDirInx( i );
					if(pDirEntry->gfID != UNUSEDGFENTRYID)
					{
						if(pDirEntry->gfOffset >= nextOffset)
						{
							pDirEntry->gfOffset -= difference;
						}
					}
				}

				nextOffset -= difference;
				pGFInfo->rgfCurrSize -= difference;
				pGFInfo->rgfExtraSpace += difference;
			}
		}

		startOffset = nextOffset + nextSize;
		nextOffset = 0xFFFFFFFF;
	}
}

//---------------------------------------------------------------------
//	validateGfxFiles
//---------------------------------------------------------------------
unsigned char validateGfxFiles(char *pFileName)
{
    unsigned char gfxError = GFX_VALIDATE_ERROR;


    if(pFileName != NULL)
    {
        if(fnVerifyUpdateDownload(pFileName))
        {
			dbgTrace(DBG_LVL_INFO,"file %s passed SDR verification", pFileName);
			gfxError = 0;
        }
        else
        {
			dbgTrace(DBG_LVL_INFO,"file %s failed SDR verification", pFileName);
        }
    }

    return (gfxError);
}


//---------------------------------------------------------------------
//	installGfxStub
//---------------------------------------------------------------------
BOOL installGfxStub(char * pFileName)
{
    DSTAT   statobj;
    int errorCode;
    char *memPtr = NULL;
    int readCount;
    long OverSignLen = 0, OverSignHdrLen = 0;
    INT fileControlBlock;

    if(pFileName != NULL)
    {
        dbgTrace(DBG_LVL_INFO, "Installing Graphics File : %s ", pFileName );
        
        //Here is where to do the actual install...

        errorCode = NU_Get_First(&statobj,pFileName);
        if(errorCode != NU_SUCCESS)
        {
            fnCheckFileSysCorruption(errorCode);
            dbgTrace(DBG_LVL_ERROR, "Cannot access %s ERR# %d", pFileName, errorCode);
            (void) NU_Done(&statobj);
            return(FALSE);
        }
            
        //Open the file we downloaded
        fileControlBlock = NU_Open(pFileName, PO_RDONLY, PS_IREAD);

        if(fileControlBlock < 0)
        {
            fnCheckFileSysCorruption(fileControlBlock);
            dbgTrace(DBG_LVL_ERROR, "NU_Open() %s ERR# %d", pFileName, fileControlBlock);
            (void) NU_Done(&statobj);
            return( FALSE ); 
        }
        else
        {
            // determine the size of the oversign header
            // and the total size of the oversign SDR(s)
            OverSignLen = fnGetSizeOfOverSign(fileControlBlock, &OverSignHdrLen);
        }
        
        //get some memory
        memPtr = (char *)malloc(statobj.fsize + 1);
            
        if(!memPtr)
        {
            fnCheckFileSysCorruption( NU_Close( fileControlBlock ) );
            dbgTrace(DBG_LVL_ERROR, "Out of memory!");
            (void) NU_Done(&statobj);
            return(FALSE);
        }
                
        (void)memset(memPtr, 0, statobj.fsize + 1);   //clean

        //Copy the data
        readCount = NU_Read(fileControlBlock, memPtr, statobj.fsize + 1 - OverSignHdrLen );

        if(readCount < 0)
        {
            fnCheckFileSysCorruption(readCount);
            fnCheckFileSysCorruption(NU_Close( fileControlBlock ));
            dbgTrace(DBG_LVL_ERROR, "NU_Read() ERR# %d", readCount );
            NU_Deallocate_Memory(memPtr);
            (void) NU_Done(&statobj);
            return(FALSE);
        }

        if(readCount != statobj.fsize - OverSignHdrLen)
        {
            fnCheckFileSysCorruption(NU_Close( fileControlBlock ));
            dbgTrace(DBG_LVL_ERROR, "NU_Read() read count mismatch",FILE_READ_ERR);
            NU_Deallocate_Memory(memPtr);
            (void) NU_Done(&statobj);
			return(FALSE);
        }
            
        //Close the file
        errorCode = NU_Close(fileControlBlock);
        if(errorCode != NU_SUCCESS)
        {
            fnCheckFileSysCorruption(errorCode);
            dbgTrace(DBG_LVL_ERROR, "NU_Close() ERR# %d", errorCode );
            NU_Deallocate_Memory(memPtr);
            (void) NU_Done(&statobj);
            return(FALSE);
        }

        errorCode = fnGFUpdateGraphic( GF_UPDATE, (UINT8 *)(memPtr+OverSignLen), ((UINT32)readCount-OverSignLen) );

        if( errorCode != 0 )  //anything but zero is bad
        {       
            dbgTrace(DBG_LVL_ERROR, "Graphics file (%s) install ERR# %d", pFileName, errorCode);               
            NU_Deallocate_Memory(memPtr);
            (void) NU_Done(&statobj);
            return (FALSE);           
        }               
            
        if(memPtr != NULL)
        {
            NU_Deallocate_Memory(memPtr);
        }

        (void) NU_Done(&statobj);
    }
    
    return TRUE;
}


//---------------------------------------------------------------------
//	installGfxFiles
//---------------------------------------------------------------------
BOOL installGfxFiles(char * pFileName)
{
    BOOL rv;

    //Call the stub...  
    rv = installGfxStub(pFileName);

    return (rv);
}



int fnInstallGraphics(void)
{
	int  ec = 0, i;
	STATUS haveFile = NU_SUCCESS;
	char pattern[128];
	char fileName[128];
	char* fileName_p = fileName;
	DSTAT	dstat ;
	BOOL    release_dstat = false;
	GFINSTALLENTRY * pList = NULL;
	GFINSTALLENTRY * pCurrEntry = NULL;
	int size = 0;
	int numberOfInstalledGraphics = 0;

	int  index = 0;

	pList = malloc(sizeof(GFINSTALLENTRY) * DEFAULT_GF_DIR_SIZE);

	if(pList != NULL)
	{
		memset(pList, 0,  sizeof(GFINSTALLENTRY) * DEFAULT_GF_DIR_SIZE);
		pCurrEntry = pList;

		sprintf(pattern, "%s:\\%s\\*%s", DRIVE_LETTER, GRAPHIC_INSTALL_PATH, GFINSTALLEXT);
		haveFile = NU_Get_First(&dstat, pattern);
		release_dstat = (haveFile == NU_SUCCESS);

		while (haveFile == NU_SUCCESS)
		{
			strncpy(pCurrEntry->fileName, dstat.lfname, GFX_INSTALL_NAME_SZ-1);
			pCurrEntry->fileSize = dstat.fsize; 
			size += dstat.fsize;
			sprintf(fileName_p, "%s:\\%s\\%s", DRIVE_LETTER, GRAPHIC_INSTALL_PATH, dstat.lfname);
			dbgTrace(DBG_LVL_INFO,"Located new graphic file %s", dstat.lfname);
			errno = 0;
			ec = validateGfxFiles(fileName_p);
			if(ec != 0)
			{
				pCurrEntry->failStatus = TRUE;
				size -= dstat.fsize;
			}

			index ++;
			pCurrEntry++;

			NU_Sleep(1);
			//Locate next file to try
			haveFile = NU_Get_Next(&dstat);
		}

        if (haveFile != NU_SUCCESS)
            fnCheckFileSysCorruption(haveFile);

        if (release_dstat)
        {
            (void) NU_Done(&dstat);
            release_dstat = false;
        }

		if(index > 0)
		{
			pCurrEntry = pList;

			//Reallocate current .GAR size + additional graphics size, then read .GAR into it so we have extra space to install the new graphics.
			ec = fnGFInitResidentGraphics(size);
			if(ec != 0)
			{
				//Mark all as failed
				for(i = 0; i < index; i++)
				{
					pCurrEntry->failStatus = TRUE;
					pCurrEntry++;
				}

			}
			else
			{
				for(i = 0; i < index; i++)
				{
					if(pCurrEntry->failStatus == FALSE)
					{
						sprintf(fileName_p, "%s:\\%s\\%s", DRIVE_LETTER, GRAPHIC_INSTALL_PATH, pCurrEntry->fileName);

						if(!installGfxFiles(fileName_p))
						{
							//Probably will do something else someday too, but for now...
							dbgTrace(DBG_LVL_INFO,"file %s not installed", pCurrEntry->fileName);
							pCurrEntry->failStatus = TRUE;
						}
						else
						{
							dbgTrace(DBG_LVL_INFO,"file %s installed", pCurrEntry->fileName);
							numberOfInstalledGraphics++;
						}
					}

					pCurrEntry++;
				}
			}

			GarbageCollectGarFile();

			dbgTrace(DBG_LVL_INFO,"Start writing updated GAR File");
			if(pGFInfo->fsUpdated == TRUE)
			{
				DSTAT statobj;
				CHAR  pattern[6] = "*.GAR";

				//Find GAR file
				ec = NU_Get_First(&statobj, pattern);
				if(ec == NU_SUCCESS)
				{
					//Write out the .GAR file 
					ec = fnGFWriteGraphicFile();

					if(ec == 0)
					{
						pGFInfo->fsUpdated = FALSE;
						ec = unlink(statobj.lfname);

						if(ec == 0)
							ec = NU_Rename( GFGROWNAME, statobj.lfname);
					}

					(void) NU_Done(&statobj);
				}

				if(ec != 0)
				{
					fnCheckFileSysCorruption(ec);
					pCurrEntry = pList;

					//Mark all as failed
					for(i = 0; i < index; i++)
					{
						pCurrEntry->failStatus = TRUE;
						pCurrEntry++;
					}
					numberOfInstalledGraphics = 0;

				}
			}

			NU_Sleep(1);
		
			if(UpdateGraphicsBaseInstallLog(pList, index) != SUCCESS)
			{
				//Bulk update failed so add to install log 1 by 1
				pCurrEntry = pList;

				for(i = 0; i < index; i++)
				{
					//Add to install log
					if(pCurrEntry->failStatus == FALSE)
					{
						UpdateBaseInstallLog(pCurrEntry->fileName, "SUCCESS");
					}
					else
					{
						UpdateBaseInstallLog(pCurrEntry->fileName, "FAILED");
					}

					pCurrEntry++;
					NU_Sleep(1);
				}
			}

			//Remove files
			pCurrEntry = pList;

			dbgTrace(DBG_LVL_INFO,"Removing Graphics install files");
			for(i = 0; i < index; i++)
			{
				sprintf(fileName_p, "%s:\\%s\\%s", DRIVE_LETTER, GRAPHIC_INSTALL_PATH, pCurrEntry->fileName);
				ec = unlink(fileName_p); 
				fnCheckFileSysCorruption(ec);
				pCurrEntry++;
				NU_Sleep(1);
			}
			dbgTrace(DBG_LVL_INFO,"Graphic install files removed");
		}

	}
	else
	{
		//Need to do it 1 at a time
		fnInstallGraphicsOld();
	}

	if(pList != NULL)
		free(pList);

	return numberOfInstalledGraphics;
}
