typedef unsigned long useconds_t;

#include                <stdio.h>
#include 				<ctype.h>
#include                "nucleus.h"
#include				"pbos.h"
#include                "kernel/nu_kernel.h"
#include                "connectivity/nu_connectivity.h"
#include 				"debugTask.h"
#include 				"HAL_AM335x_I2C.h"


#define					I2CMAXDATALENGTH				0x80
#define					I2C_EVENT_WRITECOMPLETE			0x01
#define					I2C_EVENT_DATAPRESENT			0x02
#define					I2C_DEFAULT_TX_BUFFER_SIZE		512
#define					I2C_DEFAULT_RX_BUFFER_SIZE		512
#define					I2C_EVENTWAIT_TIMEOUT			500

// this is a Nucleus constant defined in beaglebone_black.platform ...
const DV_DEV_LABEL		NUCLEUS_I2C1_LABEL = {{0x53,0x28,0x67,0xf2,0x24,0x1d,0x49,0x9f,0xbe,0x0e,0x4b,0x5b,0xbf,0xec,0x70,0x7c}};
const DV_DEV_LABEL		NUCLEUS_I2C0_LABEL = {{0x53,0x28,0x67,0xf2,0x24,0x1d,0x49,0x9f,0xbe,0x0e,0x4b,0x5b,0xbf,0xec,0x70,0x7b}};

// WARNING!
// Use global flags to help track process completion. Ideally I would like this to be tied to the node
// but this is not directly possible since no user context is carried through the the relevant callbacks.
// The relationship could be re-acquired by using a lookup table to map handle to node. Until then make
// sure that multiple I2C accesses are not performed simultaneously.
int						globalDataPresentFlag 	= 0;
int						globalWriteCompleteFlag = 0;

NU_EVENT_GROUP			globalI2CEventGroup ;
INT						globalEventGroupEnabled = 0 ;
PB_I2C_LASTASYNCSTATE	globalI2CLastAsyncState;
OS_HISR 				globalI2CErrhisr;

void pbI2CErrorHISR()
{
	NU_Set_Events(&globalI2CEventGroup, I2C_EVENT_DATAPRESENT | I2C_EVENT_WRITECOMPLETE, NU_OR) ;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// pbInitializeBBBI2C
//
// Setup the I2C operation, open the I2C1 device.
///
void pbInitializeI2C(PB_I2C *pbI2C, NU_MEMORY_POOL* mem_pool)
{
	STATUS status;
	// Setup an HISR for the event callback bug...
	OSCreateHISR(&globalI2CErrhisr, &pbI2CErrorHISR, 1000, "hisrI2CErr"); //

	// I2C startup...
	if(globalEventGroupEnabled == 0)
	{
		pbI2C->I2CInit.i2c_memory_pool 							= mem_pool; //pointerI2C ;
		pbI2C->I2CInit.i2c_driver_mode 							= I2C_INTERRUPT_MODE;
		pbI2C->I2CInit.i2c_baudrate 							= I2C_DEFAULT_BAUDRATE;
		pbI2C->I2CInit.i2c_tx_buffer_size 						= I2C_DEFAULT_TX_BUFFER_SIZE;
		pbI2C->I2CInit.i2c_rx_buffer_size 						= I2C_DEFAULT_RX_BUFFER_SIZE;
		//
		pbI2C->I2CInit.i2c_app_cb.i2c_data_indication 			= i2cDataIndication;
		pbI2C->I2CInit.i2c_app_cb.i2c_address_indication 		= i2cAddressIndication;
		pbI2C->I2CInit.i2c_app_cb.i2c_ack_indication			= i2cAckIndication;
		pbI2C->I2CInit.i2c_app_cb.i2c_transmission_complete 	= i2cTransmissionComplete;
		pbI2C->I2CInit.i2c_app_cb.i2c_error 					= i2cError;
		//
		pbI2C->I2CInit.i2c_node_address.i2c_slave_address		= I2C_DEFAULT_SLAVE_ADDRESS;
		pbI2C->I2CInit.i2c_node_address.i2c_address_type		= I2C_DEFAULT_ADDRESS_TYPE;
		pbI2C->I2CInit.i2c_node_address.i2c_node_type			= I2C_MASTER_NODE; // I2C_MASTER_SLAVE;

		status = NU_I2C_Open((DV_DEV_LABEL *)&NUCLEUS_I2C0_LABEL, &(pbI2C->I2CHandle), &(pbI2C->I2CInit));
		if(status == NU_SUCCESS)
		{
	//		dbgTrace(DBG_LVL_INFO,"Initialized I2C subsystem successfully\r\n");
		}
		else
		{
			dbgTrace(DBG_LVL_INFO,"ERROR initializing I2C subsystem [%d] ", status) ;
			printI2CErrorDetails(status) ;
			dbgTrace(DBG_LVL_INFO,"\r\n") ;
		}

		status = NU_I2C_Master_Free_Bus(pbI2C->I2CHandle);
		if(status == NU_SUCCESS)
		{
	//		dbgTrace(DBG_LVL_INFO,"Master FREE Bus completed successfully\r\n");
		}
		else
		{
			dbgTrace(DBG_LVL_INFO,"ERROR with Master FREE Bus operation [%d] ", status) ;
			printI2CErrorDetails(status) ;
			dbgTrace(DBG_LVL_INFO,"\r\n") ;
		}
		i2cClearLastAsyncState();
		pbCreateI2CEventGroup() ;
	}
	else
	{
		dbgTrace(DBG_LVL_INFO,"\r\nI2C Subsystem seems to already be setup !\r\n");
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	pbDestroyI2C
///
void pbDestroyI2C(PB_I2C *pbI2C)
{
	NU_I2C_Close(pbI2C->I2CHandle);
	pbDestroyI2CEventGroup() ;
}


// create the Nucleus event group
void pbCreateI2CEventGroup()
{
	if(globalEventGroupEnabled == 0)
	{
//		dbgTrace(DBG_LVL_INFO,"\r\nCreated I2C event group\r\n");
		NU_Create_Event_Group(&globalI2CEventGroup, "I2CEG") ;
		globalEventGroupEnabled = 1 ;
	}
}

// destroy the nucleus event group
void pbDestroyI2CEventGroup()
{
	if(globalEventGroupEnabled == 1)
	{
//		dbgTrace(DBG_LVL_INFO,"Destroying I2C Event Group\r\n") ;
		NU_Delete_Event_Group(&globalI2CEventGroup);
		globalEventGroupEnabled = 0 ;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// printI2CErrorDetails
 //
VOID printI2CErrorDetails(STATUS s)
{
	switch(abs((int)s))
	{
		case I2C_OS_ERROR:
			debugDisplayOnly("I2C OS ERROR") ;
			break ;
		case I2C_NULL_GIVEN_FOR_MEM_POOL:
			debugDisplayOnly("I2C OS ERROR") ;
			break ;
		case I2C_NO_ACTIVE_NOTIFICATION:
			debugDisplayOnly("I2C_NULL_GIVEN_FOR_MEM_POOL") ;
			break ;
		case I2C_NULL_GIVEN_FOR_INIT:
			debugDisplayOnly("I2C_NULL_GIVEN_FOR_INIT") ;
			break ;
		case I2C_INVALID_SLAVE_ADDRESS:
			debugDisplayOnly("I2C_INVALID_SLAVE_ADDRESS") ;
			break ;
		case I2C_SLAVE_NOT_ACKED:
			debugDisplayOnly("I2C_SLAVE_NOT_ACKED") ;
			break ;
		case I2C_BUS_BUSY:
			debugDisplayOnly("I2C_BUS_BUSY") ;
			break ;
		case I2C_TRANSFER_INIT_FAILED:
			debugDisplayOnly("I2C_TRANSFER_INIT_FAILED") ;
			break ;
		case I2C_ACK_TX_FAILED:
			debugDisplayOnly("I2C_ACK_TX_FAILED") ;
			break ;
//
		case I2C_INVALID_ADDRESS_TYPE:
			debugDisplayOnly("I2C_ACK_TX_FAILED") ;
			break ;
		case I2C_7BIT_ADDRESS_TYPE:
			debugDisplayOnly("I2C_INVALID_ADDRESS_TYPE") ;
			break ;
		case I2C_10BIT_ADDRESS_TYPE:
			debugDisplayOnly("I2C_10BIT_ADDRESS_TYPE") ;
			break ;
		case I2C_TRANSFER_STOP_FAILED:
			debugDisplayOnly("I2C_TRANSFER_STOP_FAILED") ;
			break ;
		case I2C_NO_STOP_SIGNAL:
			debugDisplayOnly("I2C_NO_STOP_SIGNAL") ;
			break ;
		case I2C_NO_RESTART_SIGNAL:
			debugDisplayOnly("I2C_NO_RESTART_SIGNAL") ;
			break ;
		case I2C_ADDRESS_TX_FAILED:
			debugDisplayOnly("I2C_ADDRESS_TX_FAILED") ;
			break ;
		case I2C_TRANSFER_IN_PROGRESS:
			debugDisplayOnly("I2C_TRANSFER_IN_PROGRESS") ;
			break ;
		case I2C_INVALID_NODE_TYPE:
			debugDisplayOnly("I2C_INVALID_NODE_TYPE") ;
			break ;
		case I2C_NODE_NOT_MASTER:
			debugDisplayOnly("I2C_NODE_NOT_MASTER") ;
			break ;
		case I2C_INVALID_OPERATION:
			debugDisplayOnly("I2C_INVALID_OPERATION") ;
			break ;
		case I2C_INVALID_PORT_ID:
			debugDisplayOnly("I2C_INVALID_PORT_ID") ;
			break ;
//
		case I2C_INVALID_DEVICE_ID:
			debugDisplayOnly("I2C_INVALID_DEVICE_ID") ;
			break ;
		case I2C_INVALID_HANDLE:
			debugDisplayOnly("I2C_INVALID_HANDLE") ;
			break ;
		case I2C_INVALID_HANDLE_POINTER:
			debugDisplayOnly("I2C_INVALID_HANDLE_POINTER") ;
			break ;
		case I2C_SHUTDOWN_ERROR:
			debugDisplayOnly("I2C_SHUTDOWN_ERROR") ;
			break ;
		case I2C_DEV_ALREADY_INIT:
			debugDisplayOnly("I2C_DEV_ALREADY_INIT") ;
			break ;
		case I2C_DEV_SLEEPING:
			debugDisplayOnly("I2C_DEV_SLEEPING") ;
			break ;
		case I2C_DRIVER_REGISTER_FAILED:
			debugDisplayOnly("I2C_DRIVER_REGISTER_FAILED") ;
			break ;
		case I2C_DRIVER_INIT_FAILED:
			debugDisplayOnly("I2C_DRIVER_INIT_FAILED") ;
			break ;
		case I2C_INVALID_DRIVER_MODE:
			debugDisplayOnly("I2C_INVALID_DRIVER_MODE") ;
			break ;
//
		case I2C_SLAVE_TIME_OUT:
			debugDisplayOnly("I2C_SLAVE_TIME_OUT") ;
			break ;
		case I2C_INVALID_VECTOR:
			debugDisplayOnly("I2C_INVALID_VECTOR") ;
			break ;
		case I2C_INVALID_PARAM_POINTER:
			debugDisplayOnly("I2C_INVALID_PARAM_POINTER") ;
			break ;
		case I2C_INVALID_SLAVE_COUNT:
			debugDisplayOnly("I2C_INVALID_SLAVE_COUNT") ;
			break ;
		case I2C_DATA_RECEPTION_FAILED:
			debugDisplayOnly("I2C_DATA_RECEPTION_FAILED") ;
			break ;
		case I2C_DATA_TRANSMISSION_FAILED:
			debugDisplayOnly("I2C_DATA_TRANSMISSION_FAILED") ;
			break ;
		case I2C_INVALID_IOCTL_OPERATION:
			debugDisplayOnly("I2C_INVALID_IOCTL_OPERATION") ;
			break ;
		case I2C_DEVICE_IN_USE:
			debugDisplayOnly("I2C_DEVICE_IN_USE") ;
			break ;
		case I2C_BUFFER_EMPTY:
			debugDisplayOnly("I2C_BUFFER_EMPTY") ;
			break ;
//
		case I2C_BUFFER_FULL:
			debugDisplayOnly("I2C_BUFFER_FULL") ;
			break ;
		case I2C_BUFFER_NOT_EMPTY:
			debugDisplayOnly("I2C_BUFFER_NOT_EMPTY") ;
			break ;
		case I2C_BUFFER_HAS_LESS_DATA:
			debugDisplayOnly("I2C_BUFFER_HAS_LESS_DATA") ;
			break ;
		case I2C_INVALID_TX_BUFFER_SIZE:
			debugDisplayOnly("I2C_INVALID_TX_BUFFER_SIZE") ;
			break ;
		case I2C_INVALID_RX_BUFFER_SIZE:
			debugDisplayOnly("I2C_INVALID_RX_BUFFER_SIZE") ;
			break ;
		case I2C_INVALID_TX_DATA_LENGTH:
			debugDisplayOnly("I2C_INVALID_TX_DATA_LENGTH") ;
			break ;
		case I2C_INVALID_RX_DATA_LENGTH:
			debugDisplayOnly("I2C_INVALID_RX_DATA_LENGTH") ;
			break ;
		case I2C_TX_BUFFER_NOT_ENOUGH:
			debugDisplayOnly("I2C_TX_BUFFER_NOT_ENOUGH") ;
			break ;
		case I2C_RX_BUFFER_NOT_ENOUGH:
			debugDisplayOnly("I2C_RX_BUFFER_NOT_ENOUGH") ;
			break ;
//
		case I2C_SESSION_UNAVAILABLE:
			debugDisplayOnly("I2C_SESSION_UNAVAILABLE") ;
			break ;
		case I2C_DEV_NOT_FOUND:
			debugDisplayOnly("I2C_DEV_NOT_FOUND") ;
			break ;
		case I2C_DEV_REG_PATH_TOO_LONG:
			debugDisplayOnly("I2C_DEV_REG_PATH_TOO_LONG") ;
			break ;
		case I2C_GENERAL_HARDWARE_ERROR:
			debugDisplayOnly("I2C_GENERAL_HARDWARE_ERROR") ;
			break ;
		case I2C_INVALID_BAUDRATE:
			debugDisplayOnly("I2C_INVALID_BAUDRATE") ;
			break ;
		case I2C_TRANSMISSION_ABORTED:
			debugDisplayOnly("I2C_TRANSMISSION_ABORTED") ;
			break ;
		case I2C_POLLING_ABORTED:
			debugDisplayOnly("I2C_POLLING_ABORTED") ;
			break ;
		case I2C_INVALID_PARAM:
			debugDisplayOnly("I2C_INVALID_PARAM") ;
			break ;
		default:
			debugDisplayOnly("! UNKNOWN ERROR CODE !");
			break;
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	pbI2CRead
//
STATUS pbI2CRead(PB_I2C *pbI2C, PB_I2C_NODE *node, UINT8 *Val, UINT uSize)
{
	STATUS status ;
	*Val = 0 ;
	i2cClearLastAsyncState();
	status = NU_I2C_Master_Read(pbI2C->I2CHandle, node->node, Val, uSize) ;

	// wait for data present flag, if no error then read data else set error state
	status = (waitForDataPresentFlag(node) == NU_SUCCESS) ? NU_I2C_Receive_Data(pbI2C->I2CHandle, Val, uSize) : NU_RETURN_ERROR ;

	if(status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_INFO,"I2C ERROR during Read phase [%d] ", (int)status);
		printI2CErrorDetails(status) ;
		dbgTrace(DBG_LVL_INFO,"\r\n") ;
	}
	return status ;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	pbI2CWrite
//
STATUS pbI2CWrite(PB_I2C *pbI2C, PB_I2C_NODE *node, UINT8 *buf, UINT len)
{
	STATUS status ;
	i2cClearLastAsyncState();
	status = NU_I2C_Master_Write(pbI2C->I2CHandle, node->node, buf, len) ;

	if(status == NU_SUCCESS)
	{
		waitForWriteComplete(node) ;
		if(globalI2CLastAsyncState.err_code != 0)
			status = NU_RETURN_ERROR;
	}
	else
	{
		dbgTrace(DBG_LVL_INFO,"I2C ERROR writing - ");
		printI2CErrorDetails(status) ;
		dbgTrace(DBG_LVL_INFO,"\r\n") ;
	}

	return status;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	waitForWriteComplete
//
STATUS waitForWriteComplete(PB_I2C_NODE *node)
{
	UNSIGNED uEvents ;

	STATUS status = NU_Retrieve_Events(&globalI2CEventGroup, I2C_EVENT_WRITECOMPLETE, NU_OR_CONSUME, &uEvents, I2C_EVENTWAIT_TIMEOUT);


	if((status != NU_SUCCESS) ||  (globalI2CLastAsyncState.err_code != 0))
	{
		debugDisplayOnly("ERROR waitForWriteComplete: Unable to find write complete flag [%X]\r\n", globalI2CLastAsyncState.err_code);
		// a timeout does not register an I2C error so transfer the status over since err_code is used elsewhere
		globalI2CLastAsyncState.err_code = globalI2CLastAsyncState.err_code == 0 ? status : globalI2CLastAsyncState.err_code;
	}
	globalWriteCompleteFlag = 0;
	return status ;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	waitForReadComplete
//
STATUS waitForDataPresentFlag(PB_I2C_NODE *node)
{
	UNSIGNED uEvents ;
	STATUS status = NU_Retrieve_Events(&globalI2CEventGroup, I2C_EVENT_DATAPRESENT, NU_OR_CONSUME, &uEvents, I2C_EVENTWAIT_TIMEOUT);

	globalDataPresentFlag = 0;
	return status;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// I2C Callbacks...
/* Callback for telling the users of the received data. */
 VOID    i2cDataIndication (I2C_HANDLE    i2c_handle, UINT8 i2c_node_type, UNSIGNED_INT  byte_count)
 {
// Bug I think in the Nucleus code, it does not seem to place the result into the buffer we supply. Use the commented code to get the driver buffer
// so you can get at the returned data.
//	 I2C_CB         *i2c_cb;
//	 I2CS_Get_CB(i2c_handle, &i2c_cb);
//	 memcpy(globalData, &(i2c_cb->i2c_io_buffer.i2cbm_tx_buffer.i2cbm_data_read[1]), (int)(i2c_cb->i2c_io_buffer.i2cbm_tx_buffer.i2cbm_data_read[0]));

	 globalI2CLastAsyncState.handle 		= i2c_handle ;
	 globalI2CLastAsyncState.di_nodeType 	= i2c_node_type ;
	 globalI2CLastAsyncState.di_byteCount 	= byte_count ;
	 globalDataPresentFlag 					= 1;
	 NU_Set_Events(&globalI2CEventGroup, I2C_EVENT_DATAPRESENT, NU_OR) ;
 }

 /* Callback for telling the user about the completion of write operation by master/slave. */
 VOID    i2cTransmissionComplete(I2C_HANDLE    i2c_handle, UINT8 i2c_node_type)
 {
	 globalI2CLastAsyncState.handle 		= i2c_handle;
	 globalI2CLastAsyncState.tc_nodeType 	= i2c_node_type;
 	 globalWriteCompleteFlag 				= 1;
	 NU_Set_Events(&globalI2CEventGroup, I2C_EVENT_WRITECOMPLETE, NU_OR) ;
 }

 /* Callback to tell the user about address indication/match. */
 VOID    i2cAddressIndication(I2C_HANDLE i2c_handle, UINT8 rw)
 {
	 globalI2CLastAsyncState.handle 	= i2c_handle;
	 globalI2CLastAsyncState.addi_rw 	= rw;
 }

 /* Callback for telling the user about acknowledgment. */
 VOID    i2cAckIndication(I2C_HANDLE    i2c_handle, UINT8         ack_type)
 {
	 globalI2CLastAsyncState.handle 		= i2c_handle;
	 globalI2CLastAsyncState.acki_ackType 	= ack_type;
 }

 /* Callback function which will indicate the error code of a serious I2C error. */
 VOID    i2cError(I2C_HANDLE    i2c_handle, STATUS    error_code)
 {
	 globalI2CLastAsyncState.handle 		= i2c_handle;
	 globalI2CLastAsyncState.err_code 		= error_code;

	 // Work around for Nucleus bug - use a custom HISR for now, this should be fixed in 2016 release,
	 // if so then remove HISR and uncomment the Set_Events call and hope for the best.
	 OSActivateHISR(&globalI2CErrhisr);
	 //	 NU_Set_Events(&globalI2CEventGroup, I2C_EVENT_DATAPRESENT | I2C_EVENT_WRITECOMPLETE, NU_OR) ;
 }

/* callback state tracker since printing out causes a crash. */
VOID i2cClearLastAsyncState()
{
	memset(&globalI2CLastAsyncState, 0, sizeof(PB_I2C_LASTASYNCSTATE));
}


#if 0
// RD this should not be needed anymore...
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////  4604  specific helper functions  /////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	write4604ConfigurationRegister
//
// write the supplied value to the regNumber specified.
STATUS write4604ConfigurationRegister(PB_I2C *pbI2C, PB_I2C_NODE *node, int regNumber, UINT8 value)
{
	STATUS status;
	UINT8 	cmdStep1[8] = {0x00,0x00,0x05,0x00,0x01,0x00,0x00,0x00};
	UINT8 	cmdStep2[3] = {0x99,0x37,0x00};

	// setup the register to write to...
	cmdStep1[5] = (UINT8)(regNumber >> 8);
	cmdStep1[6] = (UINT8)regNumber;
	// place the data to be written
	cmdStep1[7] = value;

	globalDataPresentFlag	= 0 ;
	globalWriteCompleteFlag = 0;

	status = pbI2CWrite(pbI2C, node, cmdStep1, sizeof(cmdStep1)) ;
	if(status == NU_SUCCESS)
		status = pbI2CWrite(pbI2C, node, cmdStep2, sizeof(cmdStep2)) ;

	return status ;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	read4604ConfigurationRegister
//
//	Follows spec in section 2.4.2 of the '4604 configuration' manual.
//	Process consists of 3 writes followed by a read operation, first write is the instructions written to the
//	deviec's memory indicating which register to read. The second is an instruction to execute the instructions in memory.
//	The third is telling the system to read back the memory from address 4 this is where the device places the result.
//
STATUS read4604ConfigurationRegister(PB_I2C *pbI2C, PB_I2C_NODE *node, int regNumber, UINT8 *retValue)
{
	UINT8 	cmdStep1[7] = {0x00,0x00,0x04,0x01,0x01,0x00,0x00};
	UINT8 	cmdStep2[3] = {0x99,0x37,0x00};
	UINT8 	cmdStep3[2] = {0x00,0x04};
	STATUS 	status;

	status = NU_I2C_Master_Free_Bus(pbI2C->I2CHandle);

	cmdStep1[5] = (UINT8)(regNumber >> 8);
	cmdStep1[6] = (UINT8)regNumber;

	// Now try and read a configuration register from the USB hub. This involves
	// Writing data to memory
	// Writing an execute command (?)
	// reading data from memory...
	// write the command to read data ....
	globalDataPresentFlag	= 0 ;
	globalWriteCompleteFlag = 0;

	status = pbI2CWrite(pbI2C, node, cmdStep1, sizeof(cmdStep1)) ;
	if(status == NU_SUCCESS)
		status = pbI2CWrite(pbI2C, node, cmdStep2, sizeof(cmdStep2)) ;

	if(status == NU_SUCCESS)
		status = pbI2CWrite(pbI2C, node, cmdStep3, sizeof(cmdStep3)) ;

	if(status == NU_SUCCESS)
		status = pbI2CReadUINT8(pbI2C, node, retValue) ;

	return status;
}
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ASCIIToHex
//
// return numeric value for ASCII Hex character.
// NOTE this routine returns 0 if character is not recognized. No error!
UINT8 ASCIIToHex(UINT8 u)
{
	if((u >= '0') && (u <= '9'))
	{
		return u - '0';
	}
	else
		if((toupper(u) >= 'A') && (toupper(u) <= 'F'))
		{
			return toupper(u) - 'A' + 10;
		}
	return 0 ;
}


void dumpAsBinary(UINT8 u)
{
	int iPass ;
	for(iPass=0 ; iPass < 8 ; iPass++)
	{
		if((u & 0x80) == 0x80)
			debugDisplayOnly("1") ;
		else
			debugDisplayOnly("0") ;
		u <<= 1 ;
	}
}

