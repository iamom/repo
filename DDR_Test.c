
#ifdef MEMORYTEST

#include <stdio.h>
#include "DDR_Test.h"
//#include "CRC16.h"



#define DDR_START			(0x80300000) //(0x801b49d8)//(0x80000000)
#define DDR_END				(0x87FFFFFF) //128MB
#define DDR_SIZE			(0x07A00000)//((DDR_END - DDR_START + 1)/2)
#define BASE_ADDRESS  		((volatile datum *) DDR_START)


/**********************************************************************
 *
 * Function:    memTestDataBus()
 *
 * Description: Test the data bus wiring in a memory region by
 *              performing a walking 1's test at a fixed address
 *              within that region.  The address (and hence the
 *              memory region) is selected by the caller.
 *
 * Notes:       
 *
 * Returns:     0 if the test succeeds.  
 *              A non-zero result is the first pattern that failed.
 *
 **********************************************************************/
datum
memTestDataBus(volatile datum * address)
{
	datum pattern;
    /*
     * Perform a walking 1's test at the given address.
     */
    for (pattern = 1; pattern != 0; pattern <<= 1)
    {
        /*
         * Write the test pattern.
         */
        *address = pattern;
        /*
         * Read it back (immediately is okay for this test).
         */
        if (*address != pattern)
        {
            return (pattern);
        }
    }
    return (0);
}   /* memTestDataBus() */


/**********************************************************************
*
* Function:    memTestDataBus_WalkinZero()
*
* Description: Test the data bus wiring in a memory region by
*              performing a walking 0's test at a fixed address
*              within that region.  The address (and hence the
*              memory region) is selected by the caller.
*
* Notes:
*
* Returns:     0 if the test succeeds.
*              A non-zero result is the first pattern that failed.
*
**********************************************************************/
datum
memTestDataBus_WalkinZero(volatile datum * address)
{
	datum pattern;

	/*
	* Perform a walking 0's test at the given address.
	*/
	for (pattern = 65535; (pattern) != 0; pattern <<= 1)
	{
		/*
		* Write the test pattern.
		*/
		*address = pattern;
		/*
		* Read it back (immediately is okay for this test).
		*/
		if (*address != pattern)
		{
			return (pattern);
		}
	}
	return (0);
}   /* memTestDataBus() */

/**********************************************************************
 *
 * Function:    memTestAddressBus()
 *
 * Description: Test the address bus wiring in a memory region by
 *              performing a walking 1's test on the relevant bits
 *              of the address and checking for aliasing. This test
 *              will find single-bit address failures such as stuck
 *              -high, stuck-low, and shorted pins.  The base address
 *              and size of the region are selected by the caller.
 *
 * Notes:       For best results, the selected base address should
 *              have enough LSB 0's to guarantee single address bit
 *              changes.  For example, to test a 64-Kbyte region,
 *              select a base address on a 64-Kbyte boundary.  Also,
 *              select the region size as a power-of-two--if at all
 *              possible.
 *
 * Returns:     NULL if the test succeeds.  
 *              A non-zero result is the first address at which an
 *              aliasing problem was uncovered.  By examining the
 *              contents of memory, it may be possible to gather
 *              additional information about the problem.
 *
 **********************************************************************/
datum *
memTestAddressBus(volatile datum * baseAddress, unsigned long nBytes)
{
    unsigned long addressMask = (nBytes/sizeof(datum) - 1);
    unsigned long offset;
    unsigned long testOffset;
    datum pattern     = (datum) 0xAAAA;
    datum antipattern = (datum) 0x5555;
    /*
     * Write the default pattern at each of the power-of-two offsets.
     */
    for (offset = 1; (offset & addressMask) != 0; offset <<= 1)
    {
        baseAddress[offset] = pattern;
    }
    /*
     * Check for address bits stuck high.
     */
    testOffset = 0;
    baseAddress[testOffset] = antipattern;
    for (offset = 1; (offset & addressMask) != 0; offset <<= 1)
    {
        if (baseAddress[offset] != pattern)
        {
            return ((datum *) &baseAddress[offset]);
        }
    }
    baseAddress[testOffset] = pattern;
    /*
     * Check for address bits stuck low or shorted.
     */
    for (testOffset = 1; (testOffset & addressMask) != 0; testOffset <<= 1)
    {
        baseAddress[testOffset] = antipattern;
        if (baseAddress[0] != pattern)
        {
            return ((datum *) &baseAddress[testOffset]);
        }
        for (offset = 1; (offset & addressMask) != 0; offset <<= 1)
        {
            if ((baseAddress[offset] != pattern) && (offset != testOffset))
            {
                return ((datum *) &baseAddress[testOffset]);
            }
        }
        baseAddress[testOffset] = pattern;
    }
    return (NULL);
}   /* memTestAddressBus() */


/**********************************************************************
 *
 * Function:    memTestDevice()
 *
 * Description: Test the integrity of a physical memory device by
 *              performing an increment/decrement test over the
 *              entire region.  In the process every storage bit
 *              in the device is tested as a zero and a one.  The
 *              base address and the size of the region are
 *              selected by the caller.
 *
 * Notes:       
 *
 * Returns:     NULL if the test succeeds.  Also, in that case, the
 *              entire memory region will be filled with zeros.
 *
 *              A non-zero result is the first address at which an
 *              incorrect value was read back.  By examining the
 *              contents of memory, it may be possible to gather
 *              additional information about the problem.
 *
 **********************************************************************/
datum *
memTestDevice(volatile datum * baseAddress, unsigned long nBytes)    
{
    unsigned long offset;
    unsigned long nWords = nBytes / sizeof(datum);
    datum pattern;
    datum antipattern;
    /*
     * Fill memory with a known pattern.
     */
    for (pattern = 1, offset = 0; offset < nWords; pattern++, offset++)
    {
        baseAddress[offset] = pattern;
    }
    /*
     * Check each location and invert it for the second pass.
     */
    for (pattern = 1, offset = 0; offset < nWords; pattern++, offset++)
    {
        if (baseAddress[offset] != pattern)
        {
            return ((datum *) &baseAddress[offset]);
        }
        antipattern = ~pattern;
        baseAddress[offset] = antipattern;
    }
    /*
     * Check each location for the inverted pattern and zero it.
     */
    for (pattern = 1, offset = 0; offset < nWords; pattern++, offset++)
    {
        antipattern = ~pattern;
        if (baseAddress[offset] != antipattern)
        {
            return ((datum *) &baseAddress[offset]);
        }
    }
    return (NULL);
}   /* memTestDevice() */



/**********************************************************************
*
* Function:    CRCTestDevice()
*
* Description: Test the integrity of a physical memory device by
*              performing an increment/decrement test over the
*              entire region.  In the process every storage bit
*              in the device is tested as a zero and a one.  The
*              base address and the size of the region are
*              selected by the caller.
*
* Notes:
*
* Returns:     NULL if the test succeeds.  Also, in that case, the
*              entire memory region will be filled with zeros.
*
*              A non-zero result is the first address at which an
*              incorrect value was read back.  By examining the
*              contents of memory, it may be possible to gather
*              additional information about the problem.
*
**********************************************************************/
datum *
CRCTestDevice(volatile datum * baseAddress, unsigned long nBytes)
{
	unsigned long offset;
	unsigned long nWords = nBytes / sizeof(datum);
	unsigned long value;
	unsigned short crc;

	for (offset = 0; offset < nWords; offset++)
	{
		
		value = (unsigned long)&baseAddress[offset];
		baseAddress[offset] = _crc16(0, (char *)&value, sizeof(unsigned long));
	}

	for (offset = 0; offset < nWords; offset++)
	{
		value = (unsigned long)&baseAddress[offset];
		crc = _crc16(0, (char *)&value, sizeof(unsigned long));
		if (baseAddress[offset] != crc)
		{
			return ((datum *)&baseAddress[offset]);
		}
	}

	return (NULL);
}   /* CRCTestDevice() */



/**********************************************************************
 *
 * Function:    DDRTest()

 *
 * Description: Test a 64-k chunk of SRAM.
 *
 * Notes:       
 *
 * Returns:     0 on success.
 *              Otherwise -1 indicates failure.
 *
 **********************************************************************/
int DDRTest(void)
{

//	for (cnt = 0x3DF; cnt < 0x3E8; cnt++) //7D00000/0x2000
//		CRCTestDevice((void*)BASE_ADDRESS + (cnt*0x20000), 0x20000);
//

	if ((memTestDataBus(BASE_ADDRESS) != 0) || 
		(memTestDataBus_WalkinZero(BASE_ADDRESS) != 0) ||
		(memTestAddressBus(BASE_ADDRESS, DDR_SIZE) != NULL) ||
        (memTestDevice(BASE_ADDRESS, DDR_SIZE) != NULL) ||
		(CRCTestDevice(BASE_ADDRESS, DDR_SIZE) != NULL))

    {
        return (-1);
    }
    else
    {
        return (0);
    }

//	delete ptr;
        
}   /* DDRTest() */


#endif //MEMORYTEST


