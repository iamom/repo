/* TAB SETTING = 5 (e.g. 5, 9, 13, 17...)   */
/************************************************************************
  PROJECT:        Horizon CSD
  MODULE NAME:    cbobutil.c
  
  DESCRIPTION:    
        Contains bobtask functions that can be called from outside of the 
        bobtask files.  This is kindof the API for bob, sortof.

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
* HISTORY: 
*				  
 24-Apr-18 sa002pe on FPHX 02.12 shelton branch
   For Janus Fraca 236437: Changed fnGetCurrentIndiSelection to make sure the return value is a valid value.

* 2018.08.13 CWB - Modified fnPleaseConvertThis2Ascii():
* 		: Mod case BIGHEX_TO_STRING: Added null terminator, since the function is 
*			defined such that: "The ResultString must be null terminated before return." 
*		: Mod to default conversion type to unsigned instead of a signed when no 
*			indication of sign is given (DECIMAL_TO_STRING, THREE_B_TO_STRING)  
* 		: Replaced some ambiguous conversion names: LONG_TO_STRING is now SLONG_TO_STRING.
*			LE_LONG_TO_STRING is now LE_SLONG_TO_STRING.
*		: Fixed ULONG_TO_STRING and LE_SLONG_TO_STRING to be correct for CSD LE processor. 
*		: Added LE_ULONG_TO_STRING
*		: Add typedefs to clear some warnings.
*
*************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "aiservic.h"
#include "bob.h"
#include "bobutils.h"
#include "cjunior.h"        // bPsdType, and lots of other externs.
#include "clock.h"
#include "cm.h"
#include "custdat.h"
#include "datdict.h"
#include "features.h"
#include "flashutil.h"
#include "fwrapper.h"
#include "glob2bob.h"     // GLOBID_DATEOFMAILING
#include "global.h"
#include "ipsd.h"
#include "junior.h"
#include "oit.h"            // fnProcessSCMError()
#include "ossetup.h"
#include "pbos.h"
#include "permits.h"
#include "report1.h"
#include "tealeavs.h"
#include "textmsg.h"
#include "utils.h"
#include "unistring.h"

/* globals */


//---------------------------------------------------------------------
//  Prototypes for external functions that are not prototyped in an 
//          include file yet.
//---------------------------------------------------------------------

// From cbobpriv.c:
extern  UINT8 fnBobCheckGBL_CmdInCurrProgMode( UINT8 ubCmd, UINT8 ubProgMode );

// From utils.c:
extern unsigned char fnIntMoney2UnicodeFmt( UINT16 *pDest, double dblGreenbacks, BOOL fShowDecimals,
                                            UINT8 bMinWhole, UINT8 bMinDecimal,
                                            BOOL bRemPadding, UINT8 ucNumDecimals );


//---------------------------------------------------------------------
//   
//      External Data Declarations:
//
//---------------------------------------------------------------------

extern struct mfgXXref IPSDmfgTables[];

extern DATETIME CMOSLastBatteryDisplay;
extern DATETIME CMOSPsdBatteryTestDate;


// For conversion of Myko IDs to IPSD IDs:-----------------

const tConvertIDs aBobVarMyko2IpsdTbl[] = 
{   // Myko ID                          IPSD ID
 {  VLT_INDI_FILE_PBP_SERIAL,       BOBID_IPSD_PBISERIALNUM,        },
 {  GET_VLT_SW_VERSION,             LIPSD_FIRMWAREVERNUM,           },
 {  GET_VLT_METER_PCN,              BOBID_IPSD_PCN,                 },
 {  VLT_INDI_FILE_INDICIA_SERIAL,   BOBID_IPSD_INDICIASN,           },
 {  VERIFY_SDR_WITH_PSD,            BOBID_IPSD_VERIFY_HASH_SIGNATURE_DATA_PTR,       },
 {  GET_VLT_ACTIVE_CURRENCY,        BOBID_IPSD_CURRENCYCODE,        },
//  This must be the last entry...
 {  ENDOFCONVERT,                   ENDOFCONVERT,      }
};

const tConvertIDs aBobVarMyko2LCpsdTbl[] = 
{   // Myko ID                          LowCost Psd ID
 
//  This must be the last entry...
 {  ENDOFCONVERT,                       ENDOFCONVERT,      }
     
};


const tConvertIDs aScriptMyko2IpsdTbl[] = 
{   // Myko ID                          LowCost Psd ID
 
//  This must be the last entry...
 {  ENDOFCONVERT,                       ENDOFCONVERT,      }
     
};


const tConvertIDs aScriptMyko2LCpsdTbl[] = 
{   // Myko ID                          LowCost Psd ID
 
//  This must be the last entry...
 {  ENDOFCONVERT,                       ENDOFCONVERT,      }
     
};



//-------------------------------------------------------------


#ifndef BAD_VARIABLE_NAME
#define BAD_VARIABLE_NAME 0
#endif

/* CROSS-REFERENCES BETWEEN MANUFACTURING PARAMETER ID'S AND BOB MESSAGE ID'S
   --------------------------------------------------------------------------

    Manufacturing Number which is an index  bobutilities ID For Access              bob Message ID For Writing
*/
/* New bobamat IDs that must be implemented
   Temporarily defined until they are implemented
   Note that the xx is just for ease in finding them in the tables below
*/
//#define xxREAD_BACKDATE_LIMIT   0       
//#define xxREAD_DATEADV_LIMIT    0       

const struct mfgXref mfgPHCByteRef[] =      {{BAD_VARIABLE_NAME,            0,                      0,  MEM_ACCESS, MEM_ACCESS  }};
const struct mfgXref mfgPHCStringRef[]=     {{BAD_VARIABLE_NAME,            0,                      0,  MEM_ACCESS, MEM_ACCESS  }};
const struct mfgXref mfgPHCMultiPackedRef[]={{BAD_VARIABLE_NAME,            0,                      0,  MEM_ACCESS, MEM_ACCESS  }};
/* CURRENTLY NOT USED BY THE COMET PRINT HEAD   */
const struct mfgXref mfgPHCPackedRef[] =    {{BAD_VARIABLE_NAME,            0,                      0,  MEM_ACCESS, MEM_ACCESS  }}; 
const struct mfgXref mfgPHCWordRef[] =      {{BAD_VARIABLE_NAME,            0,                      0,  MEM_ACCESS, MEM_ACCESS  }};
const struct mfgXref mfgPHCLongRef[] =      {{BAD_VARIABLE_NAME,            0,                      0,  MEM_ACCESS, MEM_ACCESS  }};

static unsigned long ulCmosRemainingSeconds = 0;
static unsigned long ulPsdRemainingSeconds = 0;

// -------------------------------------------------------------------------------------------------
//              Prototypes for LOCAL functions:
// -------------------------------------------------------------------------------------------------


// ------------------------------------------------------------------
//  Stuff for local function fnScheduleBob:
//
//          Typedef for arg, macro defines for input, and prototype.

typedef struct rWhyScheduleBob
{
    ushort  usGroupId;
    ushort  usItemId;
    uchar   ucAccessFuncCode;
}T_WHY_SCHEDULE_BOB;

// ---------
//      Values for ucAccessFuncCode:
// This will get put into the msgID member in the syslog, so it should not
//  overlap the script IDs or the msgTypes.
#define VALID_DATA_REQ  0xF0
#define WRITE_DATA_REQ  0xF1
#define PARAM_JAN_REQ   0xF7

static BOOL fnScheduleBob( UINT16 msgID, UINT8 msgType, BOOL replyStrat, const T_WHY_SCHEDULE_BOB *pWhy );   

static  char   pLogScratchPad[ 150 ];

// Static data for Printhead reports
static unsigned int reportBatchCount;
static unsigned int reportBatchValue;

// ---------------------------------------------------------------------




// ***************************************************************************
// FUNCTION NAME:       fnCbobUtilLogItemAndGroup 
// DESCRIPTION:
//   Adds a text line to the system log with the Item ID and Group ID.
// INPUTS:
//      uwItemID    - itemID
//      uwGroupID   - GroupID
// OUTPUS:
//      Puts a single line in the system log.
// RETURNS:
//      Nothing.
// NOTES:
// HISTORY:
//  2009.10.22  Clarisa Bellamy - Initial version.                  
//---------------------------------------------------------------------------*/
void fnCbobUtilLogItemAndGroup( UINT16 uwItemID, UINT16 uwGroupID )
{
    sprintf( pLogScratchPad, " ItemID= %d, GroupID= %d", uwItemID, uwGroupID );
    fnDumpStringToSystemLog( pLogScratchPad );

    return;   
}


/* ***************************************************************************
*
* FUNCTION NAME:    fnTeaGetIdConvertTable 
*
*       PURPOSE:    Retrieves the address of the correct conversion table
*                   
*       AUTHOR:     Clarisa Bellamy
*
*       INPUTS:     pTableEntry - Pointer to pointer to table of tConvertIDs
*                           structures. 
*                           
*                   wConvertType - ushort, Conversion Type: Are we converting
*                           a variable ID, a script ID or something else...
*   
*       NOTES:      Table to use is based upon the value in bPsdType and the
*                   conversion type.
*               When these tables are put into Orbit, this will totally change.
*---------------------------------------------------------------------------*/
void fnTeaGetIdConvertTable( const tConvertIDs **pTableEntry, ushort wConvertType )
{
    switch( bPsdType )
    {
        case PSDT_IBUTTON:
        case PSDT_GEMINI:
            switch( wConvertType )
            {
                case CONV_VARID:
                    *pTableEntry = aBobVarMyko2IpsdTbl;
                    break;

                case CONV_SCRIPTID:
                    *pTableEntry = aScriptMyko2IpsdTbl;
                    break;

                default:
                    *pTableEntry = NULL_PTR;
                    break;
            }
            break;

        case PSDT_LOWCOST:
            switch( wConvertType )
            {
                case CONV_VARID:
                    *pTableEntry = aBobVarMyko2LCpsdTbl;
                    break;

                case CONV_SCRIPTID:
                    *pTableEntry = aScriptMyko2LCpsdTbl;
                    break;

                default:
                    *pTableEntry = NULL_PTR;
                    break;
            }
            break;

        default:
            *pTableEntry = NULL_PTR;
            break;
    }    
}


/* ***************************************************************************
*
* FUNCTION NAME:    fnConvertIDMyko2PsdX 
*
*       PURPOSE:    Searches a table to see if an ID used for Myko
*                   PSD is replaced by a different one for the current
*                   current ID.
*                   
*       AUTHOR:     Clarisa Bellamy
*
*       INPUTS:     itemID - ushort, Myko Item ID that may need conversion 
*                           if the items are different between Myko and
*                           current psd.
*                   wConvertType - enum from bob.h  Indicates type of 
*                           ID to convert, Variable, Script, etc.
*   
*       NOTES:      If there is an entry in the table for the Myko ID,
*                   return the corresponding replacement ID. If there 
*                   is no entry return the original Myko ID.
*---------------------------------------------------------------------------*/
ushort fnConvertIDMyko2PsdX( ushort itemID, ushort wConvertType )
{
    ushort  retval;
    const tConvertIDs *pTableEntry;

    // If not in table, use Myko ID.
    retval = itemID;
    // Clear pointer.
    pTableEntry = NULL_PTR;

    // Get the pointer to the table, depending upon the PSD type.
    fnTeaGetIdConvertTable( &pTableEntry, wConvertType );
    // Check each entry until the end of the table...
    if( pTableEntry )
    {
        while( pTableEntry->fromID != ENDOFCONVERT )
        {
            if( pTableEntry->fromID == itemID )
            {
                // IF matching ID found, convert and quit the loop.
                retval = pTableEntry->toID;
                break;
            }
            // If no match, check next entry.
            pTableEntry++;
        }    
    }
    
    // Return the conversion if there was one, else the original ID.
    return( retval );
}


/*************************************************************************

 FUNCTION NAME:     BOOL fnValidData(unsigned short groupID, unsigned short itemID, void **itemPtr)

 PURPOSE:           Allows any task to retrieve information sourced from a 7816 device.
                    If the sought information is available, static, and already retrieved
                    from the source device, the address of the information is written in 
                    the address supplied by the passed argument (itemPtr).

                    If the information is variable during run time or if static but not
                    yet loaded, the caller's task thread is used to send a message to 
                    bob and suspended while bob updates the information.  If the 
                    information is successfully retrieved the address of the information
                    is written in the address supplied by the passed argument (itemPtr).

                    The returned bool indicates success (BOB_OK) if the address contains
                    valid data.  A return of failure (BOB_SICK) indicates that an error was
                    encountered and the data in the address is not to be trusted.
                    
                    
 AUTHOR:            R. Arsenault

 INPUTS:            
 *************************************************************************/
BOOL fnValidData(   unsigned short groupID, // identification of the group containing the sought information
                    unsigned short itemID,  // identification of the sought information                 
                    void    *itemPtr )      // will fill with the requested pointer     
{               
    BOOL                retStat;            // success-failure return flag                      
    BOOL                getit;              // set if requested info must be refreshed using a script           
    BOOL                gotitFromFlash = FALSE;
    uchar               msgType = 0;        // type of instruction sent to bob task             
    struct dataMatRec   dataInfo;
    void                *srcAddr = NULL_PTR;// Address from bobaVarAddresses table.
    ushort              flashID;            // 
    uchar               flashType;
    T_WHY_SCHEDULE_BOB  stWhy;              // In case we need to call fnScheduleBob;


    retStat = BOB_OK;
    stWhy.ucAccessFuncCode = VALID_DATA_REQ;
    stWhy.usGroupId = groupID;
    stWhy.usItemId = itemID;
    
    if( groupID == BOBAMAT1 )
        retStat = BOB_SICK;                 // BOBAMAT1 is reserved for write only items 
    
    if( fnWaitForBobInit() )        retStat = BOB_OK;
    else                            retStat = BOB_SICK;

    getit = FALSE;                          // preset to information immediately available  

    // Converts the ID if the PSD is not the original Myko.
    itemID = fnConvertIDMyko2PsdX( itemID, CONV_VARID );    

    if( fnTeaGetDataMat(groupID, itemID, &dataInfo) ) 
    {
        if( retStat == BOB_OK ) 
        {                                   // test for valid group and item identification 
            msgType = MEM_ACCESS;
            srcAddr =  bobaVarAddresses[itemID].addr;

            switch( dataInfo.variance ) 
            {                           
                case UIC_PARAM:                                 // UIC read/write - not device related variable
                case TAKE_ASIS:                                 // or info that can't be arbitrarily refreshed
                case FOLLOW_UP_SCRIPT:                          // note: followup scripts ignored during reads
                case FOLLOW_UP_MSG:
                case FOLLOW_UP_TASK:
                case UIC_CONST:
                break;                                          // they are performed when writing          

                // Parameters stored in Flash, that used to be stored in the I-button.
                //  When the variance = IPSD_FLASH_PARAM, the srcAddr really has the flash ID and
                //   the controlID has the flashType
                case IPSD_FLASH_PARAM:  //  I-button Flash Parameter
                    flashID =  (ushort)(UINT32)srcAddr;
                    flashType = (UINT8)dataInfo.controlID;    
                    if( fnIsFlashParameterIdValid( flashType, flashID ) == FALSE )
                    {
                        retStat = BOB_SICK;
                        break;
                    }

                    switch( dataInfo.controlID )
                    {
                        case IBYTE_PARAM:
                            // Read the data and indicate it is done. 
                            *(uchar *)itemPtr = fnFlashGetIBByteParm( flashID );
                            gotitFromFlash = TRUE;
                        break;
            
                        case IWORD_PARAM:
                            *(ushort *)itemPtr = fnFlashGetIBWordParm( flashID );
                            gotitFromFlash = TRUE;
                        break;

                        case ILONG_PARAM:
                            *(ulong *)itemPtr = fnFlashGetIBLongParm( flashID );
                            gotitFromFlash = TRUE;
                        break;                                          
                
                        case ISTRING_PARAM:
                            srcAddr = fnFlashGetIBAsciiStringParm( flashID );
                            if (srcAddr && (strlen(srcAddr) > 0) && (strlen(srcAddr) < bobaVarAddresses[itemID].siz))
                            {
                            	strncpy(itemPtr, srcAddr, bobaVarAddresses[itemID].siz);
                            }
                            gotitFromFlash = TRUE;
                        break;

                        case IMONEY_PARAM: 
                            srcAddr = fnFlashGetIBMoneyParm( flashID );
                        break;
                            
                        case IPACKED_PARAM: 
                            srcAddr = fnFlashGetIBPackedByteParm( flashID );
                        break;

                        case IUNICODE_PARAM: 
                           srcAddr = fnFlashGetIBUnicodeStringParm( flashID );
                           if (srcAddr && (unistrlen(srcAddr) > 0) && (unistrlen(srcAddr) < bobaVarAddresses[itemID].siz))
                           {
                        	   memcpy(itemPtr, srcAddr, bobaVarAddresses[itemID].siz);
                           }
                           gotitFromFlash = TRUE;
                        break;
                        
                        case IMULTIPACKED_PARAM:
                            srcAddr = fnFlashGetIBMPackedByteParm( flashID );
                        break;

                        default:
                            // Unrecognized flash type.
                            retStat = BOB_SICK;
                            break;
                    }
                 break;


                // Based on the requested information's integrity setting, determine if the
                //  information will have to be refreshed.
                case STATIK_PSD:
                case STATIK_VLT:
                case STATIK_IPSD:                                       
                    if( !fVltDataInitialized ) 
                        getit = TRUE;               
                break;                                          

                // note that, given an integrity setting, the requirement to refresh is determined from
                // either the power up status of the source (STATIK info) device or the last message of
                case STATIK_PH: 
                    if( !fphDataInitialized ) 
                        getit = TRUE;           
                break;                  
                                                                    
                case STATIK_EXT:                                
                    if( !fExtDataInitialized ) 
                        getit = TRUE;       // the subject device (VARIES info)         
                break;

                case VARIES_VLT: 
                case VARIES_PSD:
                    if( dataInfo.controlID != lastVaultMessage )    // the integrity settings are device specific
                        getit = TRUE;                           // in order to differentiate which power up 
                break;                                      // variable or last message variable to     
                                                            // evaluate                                 

                case VARIES_IPSD:  
                    // If we did NOT just get this info, then get it now.
                    if( dataInfo.controlID != lastIpsdMessage ) 
                        getit = TRUE;
                break;

                case VARIES_PH: 
                    if( dataInfo.controlID != lastphMessage )
                        getit = TRUE;                           // the power up variables are defaulted to  
                break;                                      // FALSE and the last message variables are 
                                                            // defaulted to a non-valid-message value   
                case VARIES_EXT: 
                    if( dataInfo.controlID != lastExtCardMessage )
                        getit = TRUE;
                break;

                case ALWAYS_PH:
                case ALWAYS_PSD:
                    getit = TRUE;                               // Always get the message / info from PH    
                break;

                case STAT_SCRIPT:                               
                    // once SCM has been initialized, static does not have to be refreshed  
                    if( !fVltDataInitialized ) 
                    {
                        getit = TRUE;
                        msgType = PERFORM_SCRIPT;
                    }
                break;

                case SCRIPTED:                                  // if a script is required and the script was 
                    if( !fVltDataInitialized ) 
                    {                       // not the last one performed               
                        getit = TRUE;                               // well...                                  
                        msgType = PERFORM_SCRIPT;                   // perform it                               
                    }
                    else 
                    {
                        if( dataInfo.controlID != lastScriptPerformed ) 
                        {
                            getit = TRUE;
                            msgType = PERFORM_SCRIPT;
                        }
                    }
                break;

                case ALWAYS_SCRIPTED:                           // Perform script every time    
                    getit = TRUE;                                   
                    msgType = PERFORM_SCRIPT;
                break;

                case XFER_CONTROL_SCRIPT:
                    getit = TRUE;                                   
                    msgType = PERFORM_SCRIPT;
                    memcpy( (char *)srcAddr, &itemPtr, bobaVarAddresses[itemID].siz );
                    itemPtr = NULL_PTR;
                break;

                case XFER_CONTROL_MSG:
                    getit = TRUE;                                   
                    msgType = MEM_ACCESS;
                    memcpy( (char *)srcAddr, &itemPtr, bobaVarAddresses[itemID].siz );
                    itemPtr = NULL_PTR;
                break;

                default: 
                    retStat = BOB_SICK;
                break;
            }
        }
        else 
            retStat = BOB_SICK;

        if( getit && (retStat == BOB_OK) )
        {
            retStat = fnScheduleBob( dataInfo.controlID,    // if the information must be refreshed     
                                     msgType,               // schedule bob to refresh the info         
                                     WAIT_REPLY, &stWhy );              
        }

        // If the ItemPtr is set, and we haven't already gotten the data from flash, 
        //  Then copy it now from where the sourcePtr is pointing.
        if( !gotitFromFlash && (retStat == BOB_OK) && (itemPtr != NULL_PTR) && (srcAddr != NULL_PTR) )
            memcpy( itemPtr, srcAddr, bobaVarAddresses[itemID].siz );
    }
    else 
    {
        // if the var or group ID is bad, put something in the system log and return an err
        sprintf( pLogScratchPad, "fnValidData bad arg:" ); 
        fnDumpStringToSystemLog( pLogScratchPad );
        fnCbobUtilLogItemAndGroup( itemID, groupID );
        retStat = BOB_SICK;                             // if the var or group ID is bad, return an err
        // if the error flag has not been set yet then throw this internal error by default.
        if( !bobErrFlag )
        {
            fnSetBobInternalErr( BOB_VALID_DATA_ERR );
            fnBobErrorStandards( retStat );
        }
    }

    return( retStat );
}


/*************************************************************************

 FUNCTION NAME:     BOOL fnScheduleBob( UINT16 msgID, UINT8 msgType, BOOL replyStrat, const T_WHY_SCHEDULE_BOB *pWhy )

 PURPOSE:           Uses the current task thread to send a command to the
                    bob task.  This function has a special handshake with
                    bob and will use the handshake to wait until bob is
                    available to perform an errand.  It will suspend the
                    current task both while waiting for bob to be free and
                    while waiting for bob to complete the task.

                    A return of BOB_OK indicates that the errand was successfully
                    completed.  A return of BOB_SICK will indicate either a
                    timeout while waiting for bob or a failure in bob's attempt
                    to complete the errand.  In most cases a BOB_SICK return
                    should be treated as an error.

 AUTHOR:            R. Arsenault

 INPUTS:            

 // For logging purposes, the data of the message is formatted this way:
 bytes 0,1 = msgID (DevMsgID for MEM_ACCESS or scriptID for PERFORM_SCRIPT)
 byte  2   = msgType (MEM_ACCESS or PERFORM_SCRIPT)
 byte  3   = AccessFuncCode (VALID_DATA_REQ, WRITE_DATA_REQ, or PARAM_JAN_REQ)
 byte 4,5  = GroupID (BOBAMAT0)
 byte 6,7  = ItemID 

 *************************************************************************/
static BOOL fnScheduleBob( UINT16 msgID, UINT8 msgType, BOOL replyStrat, const T_WHY_SCHEDULE_BOB *pWhy )   
{
    uchar               caller;             // callers task ID                             
    BOOL                retStat;            // success-failure return flag                 
    BOOL                fEventity;          // status received when checking events        
    ulong               lwEventsReceived;   // copy of event group after waiting for event 
    short               waiting4bob;        // set to TRUE while waiting for bob to be free
    T_BOB_EVENT_MASK    stCallerEvents;     // Has the bits and event ID associated with the calling task.
    ulong               lwGoodEvent;        // Bob sets this when it is successful.
    ulong               lwBadEvent;         // Bob sets this when it fails.
    ulong               lwBothEvents;       // Used to initialize the event flags                     
    uchar               ucEventGroupId = BUTILS3_EVENT_GROUP;  //Should not be used if it is not first set.  But set it anyway.
    UINT8               msgData[12];        // Message sent to bob. Initialise to all zeros.  Upto 12 bytes of data
                                            //  can be sent without changing the type to PART_PTR_DATA */
    UINT16              uwEventPairID = 0;  // This is set to a non-zero value if the caller is not a task.
    char               *pFuncName;
    char               *pHisrName = NULL_PTR;
       
    // Clear out the msgData buffer.
    memset( msgData, 0, sizeof( msgData ) );
    // Load up the InterTask message buffer:
    //      Put the Script ID or the message ID into the InterTask message.    
    memcpy( &msgData[ BOBMSG_OFFS_SCRIPTID ], &msgID, 2 );
    //      Not sure why we put a copy of the message type IN the message?
    msgData[ BOBMSG_OFFS_MSGTYPE ] = msgType;
    //      This is one of: VALID_DATA_REQ, WRITE_DATA_REQ, PARAM_JAN_REQ
    msgData[ BOBMSG_OFFS_ACCESSFUNCID ] = pWhy->ucAccessFuncCode;
    //      Put the functionID, group ID and ITEM ID in for debugging purposes.        
    memcpy( &msgData[ BOBMSG_OFFS_GROUPID ], &pWhy->usGroupId, 2 ); 
    memcpy( &msgData[ BOBMSG_OFFS_ITEMID ], &pWhy->usItemId,  2 ); 
    // Add the EventPair later...

    retStat = BOB_OK;

    while( bobsBeenInitialized == FALSE ) 
        OSWakeAfter(50);   // do not run without bob                

    // Retrieve the caller's task identification
    caller = OSGetCurrentTask();                            

    // Retrieve the caller specific event masks
    if( fnGetEventMask( caller, &stCallerEvents, &uwEventPairID ) == BOB_OK ) 
    {   // Each task has its own event bits in one of the bob Event Groups
        lwGoodEvent = stCallerEvents.ulSuccessMask;
        lwBadEvent = stCallerEvents.ulErrorMask;
        ucEventGroupId = stCallerEvents.ucEventGroupId;

        // The EventID needs to be sent in the message, In case the caller is not a task:
        memcpy( &msgData[ BOBMSG_OFFS_EVNTID ], &uwEventPairID,  2 ); 
        
        if( caller != SCM ) 
        {
            waiting4bob = 1;
            while( waiting4bob ) 
            {
                // Wait for bob to be available.  Note that there is a timeout applied...
                //  when bob is available, break out of this loop.
                fEventity = OSReceiveEvents( BUTILSX_EVENT_GROUP, BOB_INACTIVE, 15000,              
                                             OS_OR, &lwEventsReceived );
                if( fEventity == SUCCESSFUL ) 
                {
                    if( (lwEventsReceived & BOB_INACTIVE) == BOB_INACTIVE ) 
                    {
                        waiting4bob = 0;
                    }
                }
                else 
                {   // If a timeout is encountered, return an error.
                   retStat = BOB_SICK;
                    break;
                }
            }
        }
        // Clear the events associated with the calling task in the bob utility event group
        lwBothEvents = lwGoodEvent | lwBadEvent;     
        (void)OSSetEvents( ucEventGroupId, ~lwBothEvents, OS_AND );

        // Send a message from the caller to bob, to up-date the requested item value, or 
        //  run a script.
        // These are messages that are NOT replied to.  Instead, an event is set when they 
        //  are finished.       
        // The caller-specific event masks are set in bob in order to provide a unique 
        //  interface for this request.   If caller = FF, the EventPairID in the IT message
        //  will be used.
        (void)OSSendIntertask( SCM, caller, 0, msgType, msgData, BOBMSG_SCHEDULER_LEN );

        if( replyStrat == WAIT_REPLY ) 
        {
            // Wait for bob to refresh the requested item.  Note that there is a 
            //  timeout applied.
            fEventity = OSReceiveEvents( ucEventGroupId, lwBothEvents, 15000,
                                         OS_OR, &lwEventsReceived );

            // If bob didn't timeout and posted a successful refresh, the return  
            //  to the caller is an ok status.
            if(   (fEventity == SUCCESSFUL)       
               && (lwEventsReceived & lwGoodEvent) ) 
            {    
                retStat = BOB_OK;
            }
            else
            {
                retStat = BOB_SICK;
            }
        }
        else  // Don't wait for reponse
        {
            retStat = BOB_OK;
        }

        // If a non-task event pair was used, this frees it up for use again.
        if( uwEventPairID )
        {
            fnFreeNonTaskEvenMask( uwEventPairID );
        }
    }
    else
    {
        // Need to add an error code for this case.
        switch( pWhy->ucAccessFuncCode )
        {
            case VALID_DATA_REQ:
                pFuncName = "ValidData";
                break;
            case WRITE_DATA_REQ:
                pFuncName = "WriteData";
                break;
            case PARAM_JAN_REQ:
                pFuncName = "ParameterJanitor";
                break;
            default:
                pFuncName = "fnSchedulBob";
                break;
        }
        // This is temporary.  When we fix this, we will return BOB_SICK, and remove the 
        //  "IGNORED" line.  The rest will stay.
        sprintf( pLogScratchPad, "Bob Variable Access Error IGNORED!! " );
        fnDumpStringToSystemLog( pLogScratchPad );
        // Check if this was called from a Hisr...
        if( caller == OS_NO_TASK )
        {
            pHisrName = OSGetCurrentHisrName();
        }
        if( pHisrName == NULL_PTR )
            pHisrName = "non-task";
        sprintf( pLogScratchPad, "Call to %s from %s.", pFuncName, pHisrName );
        fnDumpStringToSystemLog( pLogScratchPad );
        fnCbobUtilLogItemAndGroup( pWhy->usItemId, pWhy->usGroupId );
        retStat = BOB_SICK;
    }

    return( retStat );
}



// Only re-enable the 2 functions above... CB

/*************************************************************************

 FUNCTION NAME:     BOOL fnWriteData(unsigned short groupID, unsigned short itemID, void *valuPtr)

 PURPOSE:           Allows any task to write information related to 7816 device data.

                    the passed itemID identifies the item that is to be edited.
                    the passed valuPtr is a pointer to the information that is to be
                    copied into the item.  Be sure to have information in the same format
                    as the item storage.  This utility already knows the size of each item.
                    
                    The returned bool indicates success (BOB_OK) if the information is
                    successfully copied into item storage.  A return of failure (BOB_SICK) 
                    indicates that an error was encountered and the data was not successfully
                    copied into item storage.
                    
 AUTHOR:            R. Arsenault

 INPUTS:            
 *************************************************************************/
BOOL fnWriteData(   UINT16 groupID,         // identification of group containing the bob-variable.
                    UINT16 itemID,          // identification of the bob-variable to write to.                 
                    const void *valuPtr )   // Location of data to write to the bob-variable.
{                           
    struct dataMatRec   dataInfo;
    unsigned char       caller;             // callers task ID                                          
    BOOL                retStat;            // success-failure return flag                              
    BOOL                setit;              // set if okay to write item                                
    ulong               lwEventsReceived;   // copy of event group flags                                
    BOOL                fEventity;          // status received when checking events                     
    preMessage          *followTask;
    void                *itemPtr = NULL_PTR;     // address of item to be edited                             
    size_t              itemSz = 0;              // Size of item to be edited.
    T_WHY_SCHEDULE_BOB  stWhy;              // In case we need to call fnScheduleBob;


    setit = TRUE;                                           // preset to "ok to edit"                   
    retStat = BOB_SICK;                                     // preset to an error condition             
    stWhy.ucAccessFuncCode = WRITE_DATA_REQ;    // In case we need to call fnScheduleBob;
    stWhy.usGroupId = groupID;
    stWhy.usItemId = itemID;

    // Converts the ID if the PSD is not the original Myko.
    itemID = fnConvertIDMyko2PsdX( itemID, CONV_VARID );    

    if( fnTeaGetDataMat(groupID, itemID, &dataInfo) ) 
    {
        if( (dataInfo.variance == IPSD_FLASH_PARAM) ||
            (dataInfo.variance == UIC_CONST)            )
        {
            // Flash parameters and constants cannot be changed with this utility.
            setit = FALSE;
            retStat = BOB_SICK;   
        }

        else
        {   // Retrieve the address and size of the item.
            itemPtr = (char *)(bobaVarAddresses[itemID].addr);          
            itemSz = bobaVarAddresses[itemID].siz;
            // Retrieve the caller's task ID. 
            caller = OSGetCurrentTask();

            if(   (dataInfo.variance != UIC_PARAM) 
               && (dataInfo.variance != FOLLOW_UP_TASK)
               && (dataInfo.variance != IPSD_FLASH_PARAM)  )
            {
                if( fnWaitForBobInit() ) 
                        retStat = BOB_OK;
                else 
                        retStat = BOB_SICK;
            }

            if( caller != SCM ) 
            {   // if the caller isn't bob, wait for bob to be inactive             
                // note that there is a timeout applied.
                fEventity = OSReceiveEvents( BUTILSX_EVENT_GROUP, BOB_INACTIVE,
                                             4000, OS_OR, &lwEventsReceived );
                // if unable to find bob in an inactive moment, fail the edit operation
                if( fEventity != SUCCESSFUL ) 
                    setit = FALSE;      
            }                                                                       
        }
        
        // If an edit period is available, and the ptrs and size are valid, write the data 
        //  to the item and return a success status.
        if( setit && (itemPtr != NULL_PTR) && (itemSz != 0) &&(valuPtr != NULL_PTR) ) 
        {           
            if( (dataInfo.variance == XFER_CONTROL_SCRIPT) || 
                (dataInfo.variance == XFER_CONTROL_MSG) ||
                (dataInfo.variance == XC_DELAY_REPLY) ) 
            {
                memcpy( itemPtr, &valuPtr, itemSz );
            }

            else 
            {
                memcpy( itemPtr, valuPtr, itemSz );
            }
            retStat = BOB_OK;
            }

        switch( dataInfo.variance ) 
        {
            // If the new information requires a follow up activity from
            // bob, schedule bob to perform the activity.
            case XFER_CONTROL_SCRIPT:
            case FOLLOW_UP_SCRIPT:                          
                retStat = fnScheduleBob( dataInfo.controlID, PERFORM_SCRIPT, WAIT_REPLY, &stWhy );
                break;                            

            case FOLLOW_UP_MSG:
            case XFER_CONTROL_MSG:
            case ALWAYS_PSD:
            case ALWAYS_PH:
                retStat = fnScheduleBob( dataInfo.controlID, MEM_ACCESS, WAIT_REPLY, &stWhy );
            break;

            case FOLLOW_UP_TASK:
            followTask = (preMessage *)funcCrossRef[ dataInfo.controlID ];

            if( (followTask)(itemID) == BOB_OK ) 
                retStat = BOB_OK;
            else 
                retStat = BOB_SICK;

            break;

            case XC_DELAY_REPLY:
            retStat = fnScheduleBob( dataInfo.controlID,
                                     MEM_ACCESS,
                                         NO_REPLY, &stWhy );
            STAT_EdmCommand = KEYX_IN_PROGRESS;
            break;

            default:
            break;
        }
    }
    else
    {
        // if the var or group ID is bad, put something in the system log and return an err
        sprintf( pLogScratchPad, "fnWriteData bad arg: " );
        fnDumpStringToSystemLog( pLogScratchPad );
        fnCbobUtilLogItemAndGroup( itemID, groupID );
        retStat = BOB_SICK; 
        // if the error flag has not been set yet then throw this internal error by default.
        if(!bobErrFlag)
        {
            fnSetBobInternalErr( BOB_WRITE_DATA_ERR );
            fnBobErrorStandards( retStat );
        }
    }
    return( retStat );
}
// End of fnWriteData()


/*************************************************************************

 FUNCTION NAME:     uchar fnParameterJanitor(uchar operation, ushort parmId, void **location)

 PURPOSE:           This function both reads and writes manufacturing parameters to and from
                    the vault and the print head.
                    
                    Passed argument "operation" identifies both the storage type of parameter(s)
                    and the direction (e.g. read or write).  Valid settings for "operation" are:

                        MFG_BYTE_READ
                        MFG_BYTE_WRITE
                        MFG_WORD_READ
                        MFG_WORD_WRITE
                        MFG_LONG_READ
                        MFG_LONG_WRITE
                        MFG_MONETARY_READ
                        MFG_MONETARY_WRITE
                        MFG_STRING_READ
                        MFG_STRING_WRITE
                        MFG_UNICODE_READ
                        MFG_UNICODE_WRITE
                        MFG_PACKED_BYTE_READ
                        MFG_PACKED_BYTE_WRITE
                        MFG_PHC_BYTE_READ
                        MFG_PHC_BYTE_WRITE
                        MFG_PHC_WORD_READ
                        MFG_PHC_WORD_WRITE
                        MFG_PHC_LONG_READ
                        MFG_PHC_LONG_WRITE
                        MFG_PHC_STRING_READ
                        MFG_PHC_STRING_WRITE
                        MFG_PHC_PACKED_READ
                        MFG_PHC_PACKED_WRITE


                    Passed argument "parmid", which is only used in read operations, identifies
                    the argument to be written.  Write operations which can write several parameters
                    at a time, have the parameter id concatenated with the data such that this argument
                    is not used.

                    Passed argument "location" is used in two different manners, based on the
                    operation.  If the operation is a read operation, on return the address of the 
                    parameter identified by parmid, will be written to the address pointed to by location.
                    If the operation specifies a write operation, location points to a list of parameter ids
                    followed by the data to be written.  The list MUST be terminated with the "END_OF_LIST" 
                    tokens (for both the parameter id and value). The list has the following format:


                                    parameter ID 1      value 1
                                    parameter ID 2      value 2
                                    parameter ID 3      value 3
                                    parameter ID 4      value 4
                                        .
                                        .
                                        .
                                    END_OF_LIST         END_OF_LIST

                    NOTES:  
                    1. The parameter IDs are unsigned words.
                    2. The value of END_OF_LIST is reserved and defined in bobutils.h
                    3. All parameters in the list must be of the storage type specified by passed argument operation.  
                    4. Parameters for the vault and the print head may be included in the same list.
                    5. The record length of the list is dependent on the storage type.
                    6. The records are to to include any padding (e.g. between an ID and value).


                    
 AUTHOR:            R. Arsenault

 INPUTS:            
 *************************************************************************/

enum parameterJanitorErrors 
{
    PJ_LEN_ID_OUT_OF_RANGE = 0x13,          // ID in passed list is not in range for a get length operation
    PJ_READ_ID_OUT_OF_RANGE,                // ID in passed list is not in range for a read operation
    PJ_WRITE_ID_OUT_OF_RANGE,               // ID in passed list is not in range for a write operation
    PJ_READ_READ_OP_FAILED,                 // Read file to refresh local copy of structure containing current param failed in read op
    PJ_WRITE_READ_OP_FAILED,                // Read file to refresh local copy of structure containing current param failed in write op
    PJ_WRITE_INVALID_MONY,                  // Problem determining money formatted number size 
    PJ_WRITE_WRITEDATA_FAILED,              // Local copy of parameter update failed
    PJ_WR_VAR_UNDEFD_OR_TEAGET_FAIL,        // An ID is BAD_VARIABLE_NAME or teaGetDataMat returned fail in write op param list
    PJ_WRITE_WRITE_OPERATION_FAILED,        // problem executing PSD file writeback loop
    PJ_VAR_UNDEFD_OR_TEAGET_FAIL,           // First ID is BAD_VARIABLE_NAME or teaGetDataMat returned fail
    PJ_INVALID_OPERATION_REQUESTED,         // operation not defined in bobsParmOps enumeration
};

UINT8 fnParameterJanitor( UINT8 operation, UINT16 parmId, void **location ) 
{
    UINT8       retStat;                            // success-failure return flag                  
    UINT16      ourID = BAD_VARIABLE_NAME;          // translation of mfg ID to bobutils ID         
    UINT16      accessed[MAX_FILE_SCRIPT_LIST];     // list of file scripts that are accessed       
    UINT16      i, i2;                              // indexes into list of files that are accessed 
    struct recTableDef doubleCheck;                 // determines if a file has already been accessed
    UINT8       *listt;                             // pointer to a list of write operations        
    struct      mfgXref *mfgToDevice = NULL_PTR;    // pointer to mfg number - bobUtils parameter IDs
    struct dataMatRec   dataInfo;
    UINT8       *dataPtr;               // pointer to data to be written                
    UINT16      byteCntr;               // size of data to be written                   
    UINT16      wLowLimit = 0;          // Low Limit of the table                          
    UINT16      wHighLimit = 0;         // High Limit of the table                         
    UINT16      wIndex;                 // Index into the table                            
    UINT8       bBobOperation;          // Operation to perform:    Script or msg       
    UINT16      groupID = 0;
    void        *varAddr;
    UINT8       endOfList;
    BOOL        noRepeat = FALSE;
    UINT8       progStat = 0;
    T_WHY_SCHEDULE_BOB  stWhy = { 0, 0, 0 };    // In case we need to call fnScheduleBob;
    
    retStat = BOB_OK;
    endOfList = END_OF_LIST;
    stWhy.ucAccessFuncCode = PARAM_JAN_REQ;     // In case we need to call fnScheduleBob;
    
    // Initialize to no error.
    if( (operation >= MFG_BYTE_READ)  &&  (operation < BAD_MFG_OPERATION) )
    {  
        // Verify that the PSD type is some type of I-button         
        if( bPsdType & PSDT_ALL_IBUTTON )
        {
            mfgToDevice = IPSDmfgTables[operation].listt;
            wLowLimit = IPSDmfgTables[operation].lolimitt;                      // Low Limit of the table needed for indexing   */
            wHighLimit = IPSDmfgTables[operation].hilimitt;
        }
        else
        {
            // need different tables for myko or lowcost psd
        }
        
        if( mfgToDevice != NULL_PTR )
        {
            groupID = mfgToDevice[parmId-wLowLimit].whichTable;
            ourID = mfgToDevice[parmId-wLowLimit].ourNumber;                // Retrieve the bob id for the parameter.*/
        }

        if(   (mfgToDevice != NULL_PTR)
           && (ourID != BAD_VARIABLE_NAME) 
           && fnTeaGetDataMat(groupID, ourID, &dataInfo) ) 
        {
            // Perform an operation based on the passed specification.
            switch( operation )
            { 
                case MFG_PHC_STRING_LEN:
                case MFG_STRING_LEN:
                case MFG_PACKED_LEN:
                case MFG_MONETARY_LEN:
                case MFG_PHC_MULTI_PACKED_LEN:
                case MFG_PHC_PACKEDBYTE_LEN:
                case MFG_PSD_MULTI_PACKED_LEN:
                    if(    (parmId <= wHighLimit)
                        && (parmId >= wLowLimit) ) 
                    {                                     // Make sure the parameter is valid */
                        ourID = mfgToDevice[parmId-wLowLimit].ourNumber;                // Retrieve the bob id for the parameter.*/
                        *location = (void *)&bobaVarAddresses[ourID].siz;
                    }                                                                       
                    else 
                    {
                        progStat=PJ_LEN_ID_OUT_OF_RANGE; 
                        retStat = BOB_SICK;                                             // Bob is sick*/
                    }
                    break;

                case MFG_PHC_BYTE_READ:                                                     
                case MFG_PHC_WORD_READ:
                case MFG_PHC_LONG_READ:
                case MFG_PHC_STRING_READ:
                case MFG_PHC_MULTI_PACKED_READ:
                case MFG_PHC_PACKED_READ:                                               
                case MFG_BYTE_READ:                                                     
                case MFG_WORD_READ:
                case MFG_LONG_READ:
                case MFG_MONETARY_READ:
                case MFG_STRING_READ:
                case MFG_UNICODE_READ:                                                  // Operations To Read A Parameter           
                case MFG_PACKED_BYTE_READ:  
                                                                                        // --------------------------------------------
                    if(   (parmId <= wHighLimit)                                        // ensure this is a valid parameter  
                       && (parmId >= wLowLimit) )                                       // look up the passed parameter ID in the
                    {                                                                       
                        ourID = mfgToDevice[parmId-wLowLimit].ourNumber;                // cross-reference table, then load the matching*/
                        varAddr = bobaVarAddresses[ourID].addr;
                        memcpy(location, &varAddr, sizeof(void *));                     // data location into the caller's passed   
                        if(   (mfgToDevice[parmId-wLowLimit].whichTable != 1)           // address and, if the file is readable,    
                           && (dataInfo.variance != UIC_PARAM ))           
                        {
                                bBobOperation = mfgToDevice[parmId-wLowLimit].bobReadMsgType;
                                retStat = fnScheduleBob( dataInfo.controlID,             // force a refresh of the data              
                                                        bBobOperation,
                                                         WAIT_REPLY, &stWhy );
                                if (retStat == BOB_SICK)
                                    progStat = PJ_READ_READ_OP_FAILED;                  // update file read failed
                        }
                    }                                                                       // or... if not listed in cross-reference,  
                    else 
                    {
                        progStat=PJ_READ_ID_OUT_OF_RANGE; 
                        retStat = BOB_SICK;                                             // or not a valid ID, signal an error return
                    }
                    break;

            /*                  VAULT PARAMETERIZATION SOFTWARE                     */
                                                                                    
                case MFG_PHC_BYTE_WRITE:
                case MFG_PHC_WORD_WRITE:
                case MFG_PHC_LONG_WRITE:
                case MFG_PHC_STRING_WRITE:
                case MFG_PHC_PACKED_WRITE:
                case MFG_PHC_MULTI_PACKED_WRITE:
                    endOfList = PHC_END_OF_LIST;
                    //lint -fallthrough no break

                case MFG_BYTE_WRITE:
                case MFG_WORD_WRITE:
                case MFG_LONG_WRITE:
                case MFG_PSD_MULTI_PACKED_WRITE:
                case MFG_MONETARY_WRITE:
                case MFG_UNICODE_WRITE:                                                 // -------------------------------------------- */
                case MFG_STRING_WRITE:                                                  // Operations To Edit A Parameter           
                case MFG_PACKED_BYTE_WRITE:                                             // define the table that will track which files
                {
                    doubleCheck.tableAddr   = (char *)(&accessed[0]);                       // have been accessed and will need to be   
                    doubleCheck.reclen      = sizeof (short);                               // rewritten                                
                    doubleCheck.offsett     = 0;
                    doubleCheck.keySize     = sizeof (short);
    
                    listt = *location;
                    i = 0;                                                                  // init index into list of accessed files   

                    while( (*listt != endOfList) && (retStat == BOB_OK) )               // For each record in the list:  
                    {                                                                   // - - - - - - - - - - - - - - 
                        if(   (*listt <= wHighLimit)                                    // ensure this is a valid parameter              
                           && (*listt >= wLowLimit) )                                   // the start of each record contains the
                        {                                                           // parameter ID... cross-reference it to a 
                            wIndex = *listt - wLowLimit;                                    
                            groupID = mfgToDevice[wIndex].whichTable;
                            ourID = mfgToDevice[wIndex].ourNumber;                          
                            if( (ourID != BAD_VARIABLE_NAME) && fnTeaGetDataMat(groupID, ourID, &dataInfo )) 
                            {                                                                   // if this is not the first record in the list
                                if( i ) 
                                {                                                        
                                    doubleCheck.tooFar = i + 1;                                 // determine if the message ID has already  
                                    doubleCheck.keyValue=&mfgToDevice[wIndex].writeMsg;         // been performed (these messages are for   
                                    doubleCheck.foundRec = 0;                                   // summoning device files)                  
                                    if( !(lookyLooky(&doubleCheck)) ) 
                                    {
                                        accessed[i] = mfgToDevice[wIndex].writeMsg;                 
                                        if( mfgToDevice[wIndex].whichTable != 1 )                // if the message to retrieve the file has not
                                        {              
                                            bBobOperation = mfgToDevice[wIndex].bobReadMsgType;
                                            retStat = fnScheduleBob( dataInfo.controlID,     // been performed, and the file is readable,    
                                                                     bBobOperation,          // retrieve a copy of the file now          
                                                                     WAIT_REPLY, &stWhy );
                                            if (retStat == BOB_SICK)
                                                progStat = PJ_WRITE_READ_OP_FAILED;             // update file read failed
                                        }
                                        else 
                                            retStat = BOB_OK;
                                        i++;
                                    }                                                           // note that files must only be retrieved once
                                }                                                               // per list to avoid writing on top of      
                                else 
                                {                                                               // arguments that were edited by a previous 
                                    doubleCheck.keyValue = &mfgToDevice[wIndex].ourNumber;  
                                    accessed[i] = mfgToDevice[wIndex].writeMsg;                 // record in the list                                   
                                    if( mfgToDevice[wIndex].whichTable != 1 ) 
                                    {                                                           // if the message to retrieve the file has not
                                        bBobOperation = mfgToDevice[wIndex].bobReadMsgType;
                                        retStat = fnScheduleBob( dataInfo.controlID,
                                                                 bBobOperation,
                                                                 WAIT_REPLY, &stWhy );
                                        if (retStat == BOB_SICK)
                                            progStat = PJ_WRITE_READ_OP_FAILED;                 // update file read failed 
                                    }
                                    else
                                        retStat = BOB_OK;                   
                                    i++;
                                }
                                if( retStat == BOB_OK ) 
                                {                                        
                                    dataPtr = listt + 1;                                        // if the parameter is monetary, the PB232  
                                    if( operation == MFG_MONETARY_WRITE ) 
                                    {                      // message handle would have translated the 
                                        byteCntr = bobaVarAddresses[ourID].siz;                         // data into a right justified 5 byte binary... */
                                        while( byteCntr < SPARK_MONEY_SIZE ) 
                                        {                   // if the parameter's storage is shorter than
                                            if( *dataPtr == 0 ) 
                                            {                                // 5 bytes, truncate the passed data        
                                                dataPtr++;
                                                byteCntr++;                                     // if the data can not be truncated without 
                                            }                                                   // effecting its value, the value is invalid
                                            else 
                                            {                                              // and this process will be aborted         
                                                retStat = BOB_SICK;
                                                progStat = PJ_WRITE_INVALID_MONY;               // update file read failed 
                                            }
                                        }// End of While( byteCntr < SPARK_MONEY_SIZE )
                                    }
                                    if( retStat == BOB_OK ) 
                                    {                                    // updata the UIC copy of the file, using the
                                        retStat = fnWriteData(  mfgToDevice[wIndex].whichTable, // the passed data                          
                                                                ourID, dataPtr);
                                        if ( retStat == BOB_SICK )
                                            progStat = PJ_WRITE_WRITEDATA_FAILED;               // update file read failed 
                                    }
                                }
                                if( retStat == BOB_OK ) 
                                {                                        
                                    if( operation == MFG_MONETARY_WRITE ) 
                                    {
                                        byteCntr = SPARK_MONEY_SIZE;
                                    }
                                    else 
                                    {
                                        byteCntr = bobaVarAddresses[ourID].siz;                             // Adjust the Byte Counter based on the size    */
                                    }
                                    listt += sizeof(unsigned char) + byteCntr;                      // Move the list to the next element */
                                }
                            }
                            else 
                            { 
                                retStat = BOB_SICK;                                             // Looks like we found a hole in the table with 
                                progStat = PJ_WR_VAR_UNDEFD_OR_TEAGET_FAIL;                     // an undefined variable name   
                                break;
                            }                                       
                        }
                        else 
                        { 
                            retStat = BOB_SICK;
                            progStat = PJ_WRITE_ID_OUT_OF_RANGE;                               // if any record has an invalid parameter ID, abort
                            break; 
                        }
                        if( retStat == BOB_OK ) 
                        {                                                            // when all of the record instructions have been*/
                            bBobOperation = mfgToDevice[wIndex].bobWriteMsgType;
                            for( i2 = 0; i2 < i; i2++ ) 
                            {                                                       // fullfilled schedule bob to rewrite the files
                                retStat = fnScheduleBob( accessed[i2], bBobOperation, WAIT_REPLY, &stWhy );   // to the device they were sourced from     
                                if( retStat != BOB_OK ) 
                                {
                                    progStat = PJ_WRITE_WRITE_OPERATION_FAILED; 
                                    break;
                                }
                            }
                        }
                        switch( operation ) 
                        {                                                         // bail out of non-listed parameters after one */
                            case MFG_PHC_STRING_WRITE:
                            case MFG_PHC_PACKED_WRITE:
                            case MFG_PHC_MULTI_PACKED_WRITE:
                            case MFG_PSD_MULTI_PACKED_WRITE:
                            case MFG_PACKED_BYTE_WRITE:
                                noRepeat = TRUE;    
                                break;

                            default:    // Leave as FALSE.
                                break;
                        }
                        if( noRepeat ) 
                            break;
                    }// End of while 
                } // End of cases MFG_x_WRITE
                break;
                
                default:
                break;
            } // End of switch( operation )
        } // End fnTeaGet...was successful
        else  
        {
            retStat = BOB_SICK;
            progStat = PJ_VAR_UNDEFD_OR_TEAGET_FAIL; 
        }
    } // End of operation is within range.
    else 
    { 
        retStat = BOB_SICK;
        progStat = PJ_INVALID_OPERATION_REQUESTED;
    }                                                       // convert the return status to a TRUE-FALSE
    if( retStat != BOB_OK ) 
        retStat = progStat;

    return( retStat );
}


/*************************************************************************

 FUNCTION NAME:     BOOL lookyLooky(struct recTableDef *looker)

 PURPOSE:           Locates a fixed length record in a table.  Normal use is 
                    to look up the first record in the table that contains a 
                    specified field.

                    Passed recTableDef structure looker contains information that controls the
                    search.  It should be a RAM based table since the structure is
                    edited by this function.

                    recTableDed Information
                    -----------------------
                    MEMBER      SET BY CALLER                               VALUE ON SUCCESS RETURN
                    .tableAddr  to the addess of the table                  same
                    .reclen     to the length of each record in the table   same
                    .tooFar     to one greater than the last valid record   same
                    .offsett    byte offset in record to the key field      same
                    .keyValue   pointer to key value to be found            same
                    .keySize    size of key field                           same
                    .foundRec   not set by caller                           record number of record containing the specified key value
                    .foundAddr  set by caller, edited by lookyLooky         address of record containing the specified key value

                    Note that foundRec can be initialized by the caller in order to search for
                    subsequent matches in the table.

                    Returns TRUE if the specified key value is found in the specified records.

                    
 AUTHOR:            R. Arsenault

 INPUTS:            
 *************************************************************************/
BOOL lookyLooky(struct recTableDef *looker) 
{
    BOOL    searching;                                          // true while looking for a match           
    char    *tabl;
    char    *keyPtr;
    ushort  offs; 
    ushort  n;
    BOOL    retStat;

    searching = TRUE;
    retStat = FALSE;
    offs = looker->offsett;
    keyPtr = looker->keyValue;
    tabl = (char *)(looker->tableAddr) + offs;                  // access the item field in the first record
    tabl += looker->foundRec * looker->reclen;
    while(looker->foundRec < looker->tooFar && searching) {     // for each record in the table             
        searching = FALSE;
        for (n = 0; n < looker->keySize; n++) {                 // compare each byte in the item to each byte 
            if (tabl[n] != keyPtr[n]) {                         // in the field of the current record       
                searching = TRUE;                               // if the field does not completely match the
                looker->foundRec++;
                tabl += looker->reclen;
            }                                                   // sought for item, keep on searching       
        }
    }
    if (searching == FALSE) {
        looker->foundAddr = (void *)(tabl);
        retStat = TRUE;
    }
    return(retStat);
}

//#if 0 //defilcj
/*************************************************************************

 FUNCTION NAME:     BOOL fnGetReportData

 PURPOSE:           Retrieves global data from bobutils.  Should only be called from BOB! 

                    
 AUTHOR:            R. Arsenault

 INPUTS:            
 *************************************************************************/
BOOL fnGetReportData(   ushort groupID,     // identification of the group containing the sought information
                        ushort itemID,      // identification of the sought information                     
                        void   *itemPtr)    // will fill with the requested data                            
{
    struct dataMatRec   dataInfo;
    BOOL                retStat;                // success-failure return flag                                  



    retStat = BOB_SICK;

    // Converts the ID if the PSD is not the original Myko.
    itemID = fnConvertIDMyko2PsdX( itemID, CONV_VARID );    

    if (fnTeaGetDataMat(groupID, itemID, &dataInfo)) {
        
        memcpy( itemPtr,                                // write the address of the item for the caller
                bobaVarAddresses[itemID].addr, 
                bobaVarAddresses[itemID].siz);
        retStat = fnConvertBobParameter ((char *)itemPtr, groupID, itemID); 
    }
    return(retStat);
}
//#endif

//#if 0
/*************************************************************************

 FUNCTION NAME:     BOOL fnGetUnicodeReportData

 PURPOSE:           Retrieves global data from bobutils.  Should only be called from BOB! 

                    
 AUTHOR:            R. Arsenault

 INPUTS:            
 *************************************************************************/
BOOL fnGetUnicodeReportData( ushort groupID,    // identification of the group containing the sought information
                            ushort itemID,      // identification of the sought information                     
                            void   *itemPtr)    // will fill with the requested pointer                         
{    //needs work here, defilcj                  
    struct dataMatRec   dataInfo;
    BOOL                retStat;                // success-failure return flag                                  

    retStat = BOB_SICK;

    // Converts the ID if the PSD is not the original Myko.
    itemID = fnConvertIDMyko2PsdX( itemID, CONV_VARID );    

    if (fnTeaGetDataMat(groupID, itemID, &dataInfo)) 
    {        
        memcpy( itemPtr,                                // write the address of the item for the caller
                bobaVarAddresses[itemID].addr, 
                bobaVarAddresses[itemID].siz);
        retStat = fnConvertBobParameter2Unicode (itemPtr, groupID, itemID); 
    }
    return(retStat);
}
//#endif

/*************************************************************************

 FUNCTION NAME:     BOOL fnConvertBobParameter

 PURPOSE:           Converts the Bob Parameter into a unicode string

                    
 AUTHOR:            Wes Kirschner

 INPUTS:            
 *************************************************************************/
BOOL fnConvertBobParameter( void *itemPtr, 
                            UINT16 groupID,     // identification of the group containing the sought information
                            UINT16 itemID )     // identification of the sought information
{      
    struct dataMatRec   dataInfo;
    UINT16              itemSize;
    char            ResultString[MAX_RESULT_STRING+1];
    BOOL                retStat;            // success-failure return flag      

    retStat = BOB_OK;
    itemSize = bobaVarAddresses[itemID].siz;

    memset(ResultString,0, MAX_RESULT_STRING+1);

    // Converts the ID if the PSD is not the original Myko.
    itemID = fnConvertIDMyko2PsdX( itemID, CONV_VARID );    

    if( fnTeaGetDataMat(groupID, itemID, &dataInfo) ) 
    {
        retStat = fnPleaseConvertThis2Ascii( (char*)itemPtr, itemSize, dataInfo.conversion, ResultString );
    }
    else retStat = BOB_SICK;

    if( retStat == BOB_OK ) 
        strcpy (itemPtr, ResultString) ;
    return( retStat );
}

/*************************************************************************

 FUNCTION NAME:     BOOL fnConvertBobParameter2Unicode

 PURPOSE:           Converts the Bob Parameter into a unicode string

                    
 AUTHOR:            Wes Kirschner

 INPUTS:            
 *************************************************************************/
BOOL fnConvertBobParameter2Unicode( void *itemPtr, 
                                    UINT16 groupID,     // identification of the group containing the sought information
                                    UINT16 itemID )     // identification of the sought information  
{  
    struct dataMatRec   dataInfo;
    UINT16              itemSize;
    unichar             UniResultString[MAX_RESULT_STRING+1];
    UINT8               ucLen = 0;
    BOOL                retStat;            // success-failure return flag      

    retStat = BOB_OK;
    itemSize = bobaVarAddresses[itemID].siz;

    memset(UniResultString,0, MAX_RESULT_STRING+1);

    // Converts the ID if the PSD is not the original Myko.
    itemID = fnConvertIDMyko2PsdX( itemID, CONV_VARID );    

    if (fnTeaGetDataMat(groupID, itemID, &dataInfo))
    {
        ucLen = fnPleaseConvertThis2Unicode( itemPtr, itemSize, dataInfo.conversion, UniResultString);
    }
    else
        retStat = BOB_SICK;

    if (ucLen)
        (void)unistrcpy (itemPtr, UniResultString) ;

    return (retStat);
}

/******************************************************************************
   FUNCTION NAME: fnPleaseConvertThis2Ascii
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : A polite means of converting data of all sorts to ASCII strings
                 : The ResultString must be null terminated before return.
   PARAMETERS   : source pointer, length in bytes, conversion type, 
                : destination string location
   RETURN       : False if conversion not found or conversion failed, true otherwise
   NOTES        : conversion type enum in bobutils.h
   2. Default is BigEndian, unless LittleEndian "LE_" is speicified.  
   3. Default is unsigned, unless Signed "S" is specified. 

 09/16/2009  Raymond Shen     Replace the leading zero of the postage in indicia with
                              squiggle char if needed for Japan Car Tax.
 08/27/2008  Joe Qu   Merged changes from Janus for Netherlands NetSet2,
                      add support for these cases  IB_NETSET2_PRODUCTIONDATE_TO_STRING,
                      IB_NETSET2_MAILDATE_TO_STRING, ASKII_MON_WCS_TO_STRING.
             Craig DeFilippo  Initial version
******************************************************************************/

BOOL fnPleaseConvertThis2Ascii( char* itemPtr,
                                UINT16 itemSize,    
                                UINT8  convType,
								char* ResultString )
{
    IPSD_DEF_REC        rFieldRec;
    DATETIME            dt;
    unsigned char       *pData; 
    union
    {
        char           TempString[10];
        UINT8           TempArray[10];
        UINT8           TempLonger[5];
        UINT8           TempLong[4];
    } TempU;
	
	double              dblMoney;
    unsigned int        iVal;
    UINT32      		LongVar;
    UINT16      		usLength = 0;
    UINT16      		wDecimalVar;
	unsigned short      pUnicode[10];
	         short      wDays;
    unsigned short      i, j;
    UINT8       		bDecimalVar;
             char       tempResult[2];
    BOOL                retStat = BOB_OK;            


    switch (convType)
	{
        case VLT_DATE_TO_PCN_FORMAT:
        case VLT_TIME_TO_PCN_FORMAT:
        case DATE_TIME_TO_STRING:
        	break;

        case HEX_TO_STRING:
            sprintf (ResultString, "%X", *itemPtr);
        	break;

        case HEX2_TO_STRING:
            (void) memcpy(&wDecimalVar, itemPtr, sizeof(wDecimalVar));
            sprintf (ResultString, "%04X", wDecimalVar);
        	break;

        case HEX4_TO_STRING:
            (void) memcpy(&LongVar, itemPtr, sizeof(LongVar));
            sprintf (ResultString, "%08lX", (ulong)LongVar);
        	break;
            
        case BIGHEX_TO_STRING:
            pData = (unsigned char *) itemPtr;

            // Each ASCII character of the result string gets one nibble of the input hex
            for (i = 0, j = 0; j < itemSize; i += 2, j++)
            {
                iVal = (unsigned int) pData[j]; // Convert char to int because sprintf expects an int
                (void)sprintf(tempResult, "%02X", iVal);
                ResultString[i] = tempResult[0];
                ResultString[i + 1] = tempResult[1];
            }
			ResultString[i] = 0;
            break;

        case DECIMAL_TO_STRING:
            if (itemSize == 1)
			{                                        // if one byte */
                memcpy( &bDecimalVar, itemPtr, itemSize);
                sprintf (ResultString, "%u", bDecimalVar) ;
            }
            else
			{                                                      // if two bytes */
                memcpy( &wDecimalVar, itemPtr, itemSize);
                sprintf (ResultString, "%u", wDecimalVar) ;
            }
        	break;

        case PBCD_TO_STRING:
            fnPackedBcdToAsciiBcd(ResultString, (unsigned char *) itemPtr, itemSize);
            ResultString[itemSize*2] = 0;
        	break;

        case PBCDZIP_TO_STRING:
            fnRjPackedBcdToAsciiBcd(ResultString, (unsigned char *) itemPtr, itemSize);
            ResultString[itemSize*2] = 0;
        	break;

        case THREE_B_TO_STRING:
            TempU.TempLong[0] = 0 ;
            memcpy (TempU.TempLong + 1, itemPtr, 3) ;
            sprintf (ResultString, "%lu", EndianAwareGet32(TempU.TempLong)) ;
        	break;

        case SLONG_TO_STRING:
            sprintf (ResultString, "%ld", (long)EndianAwareGet32(itemPtr)) ;
        	break;

        case ULONG_TO_STRING:
            sprintf (ResultString, "%lu", EndianAwareGet32(itemPtr)) ;
        	break;

        case ULONG_TO_STRING_10CH_LZP:                      // 10 char, left zero pad
            sprintf (ResultString, "%010lu", EndianAwareGet32(itemPtr)) ;
	        break;      

        case STRING_NEEDS_NULL:
            strncpy (ResultString, itemPtr, itemSize) ;
            ResultString[itemSize] = 0 ;
	        break;

        case TRUE_FALSE_STRING:
            if (*itemPtr) strcpy (ResultString, "T") ;
            else strcpy (ResultString, "F") ;
	        break;

        case NO_FORMAT_CHANGE:
            strncpy( ResultString, itemPtr, itemSize );
            ResultString[itemSize] = 0;   //too many orbit variables should be designated STRING_NEEDS_NULL
	        break;                        // but are not, so the null terminator is provided here also.
	                                      // should fix orbit tables and remove this 

        case IB_MAILDATE_TO_STRING:
            memcpy(&LongVar, itemPtr,itemSize);
            usLength = (UINT16)sprintf( TempU.TempString, "%lu", (ulong)LongVar );
            memset(&dt,0,sizeof(DATETIME));
			
            dt.bCentury = (UINT8)((10 * (TempU.TempArray[0] - '0')) + (TempU.TempArray[1] - '0'));             
            dt.bYear    = (UINT8)((10 * (TempU.TempArray[2] - '0')) + (TempU.TempArray[3] - '0'));            
            dt.bMonth   = (UINT8)((10 * (TempU.TempArray[4] - '0')) + (TempU.TempArray[5] - '0'));           
            dt.bDay     = (UINT8)((10 * (TempU.TempArray[6] - '0')) + (TempU.TempArray[7] - '0'));             
            dt.bDayOfWeek = 2;      // I met her on a Monday and my heart stood still 
			
            if(fnFormatIndiciaDate(ResultString,&dt) != SUCCESSFUL)
                retStat = BOB_SICK;
	        break;

        case IB_LP_MAILDATE_TO_STRING:  // laposte ascii DDMMYY
            memset(&dt,0,sizeof(DATETIME));
			
            dt.bCentury = 20;
            dt.bYear  = (UINT8)(((itemPtr[4] - '0') * 10) + (itemPtr[5] - '0'));
            dt.bMonth = (UINT8)(((itemPtr[2] - '0') * 10) + (itemPtr[3] - '0'));
            dt.bDay   = (UINT8)(((itemPtr[0] - '0') * 10) + (itemPtr[1] - '0'));
            dt.bDayOfWeek = 2;      // I met her on a Monday and my heart stood still 
			
            if(fnFormatIndiciaDate(ResultString,&dt) != SUCCESSFUL)
                retStat = BOB_SICK;
	        break;

        case IB_CANADA_CREATEDATE_TO_STRING:
            fnFormatMailCreationDate((uchar*) itemPtr, ResultString, &dt);
	        break;

        case IB_CANADA_MAILDATE_TO_STRING:
            retStat = BOB_SICK;         

            // first get the creation date
            if(fnFlashGetIBPRecVar(&rFieldRec,IPB_GET_INDICIA_DEF_REC, INDICIA_DEF_REC_CAN_CREAT_DATE_ID))
            {
                pData = pIPSD_barcodeData.pGlob;    
                fnFormatMailCreationDate(pData+rFieldRec.usFieldOff, TempU.TempString, &dt);
				
                // then get the mail date offset
                bDecimalVar = *itemPtr;
				
                // add the mail date to the creation date
                while (bDecimalVar--)
                {
                    AdjustDayByOne(&dt, TRUE);
                }
				
                if(fnFormatIndiciaDate(ResultString,&dt) == SUCCESSFUL)
                    retStat = BOB_OK;
            }
	        break;

        case IB_CANADA_CREATETIME_TO_STRING:
            memset(&dt, 0, sizeof(DATETIME));
            fnIPSDTimeToSys( &dt, EndianAwareGet32(itemPtr));
            sprintf(ResultString,"%02d%02d%02d",dt.bHour,dt.bMinutes,dt.bSeconds);
	        break;

        case IB_CANADA_SECURITYCODE_TO_STRING:
	        // do this if the data is in the security code area of debit cert...
	        //fnFormatSecurityCode((uchar*) itempPtr, ResultString);
	        // or do this if the data is in the beggining of the 40 DSA sig..
            retStat = BOB_SICK;         

            if(fnFlashGetIBPRecVar(&rFieldRec,IPB_GET_INDICIA_DEF_REC, INDICIA_DEF_REC_CAN_DSA_SIG_ID))
            {
                pData = pIPSD_barcodeData.pGlob;    
                fnFormatSecurityCode(pData+rFieldRec.usFieldOff, ResultString);
                retStat = BOB_OK;
            }
	        break;

        case IB_GERMAN_MAILDATE_TO_STRING:
            memset(&dt, 0, sizeof(DATETIME));
            wDecimalVar = 0;
            memcpy(&wDecimalVar, itemPtr,itemSize);
            sprintf(TempU.TempString,"%05d",wDecimalVar);
			
            dt.bDayOfWeek = 2;      // I met her on a Monday and my heart stood still 
            dt.bCentury = 20;
            dt.bYear    = (UINT8)(((TempU.TempArray[3] - '0') * 10) + (TempU.TempArray[4] - '0'));
			
            TempU.TempString[3] = 0;
            sscanf(TempU.TempString, "%ld", &LongVar);
            wDays = (SINT16)(LongVar & 0x0000FFFF);     // It should be between 1 and 366
            fnDayOfYearToDayMonth(wDays, &dt);
			
            if(lIPSD_FirmwareVerNum < 0x00080000)
            {
                // add in the advanced days for german human readable maildate, defilcj
                // this was an ibutton bug that was fixed in ibutton ver 8.0
                wDays = GetPrintedDateAdvanceDays();
                while(wDays-- > 0)
                {
                    AdjustDayByOne(&dt, TRUE);
                }
            }
			
            if(fnFormatIndiciaDate(ResultString,&dt) != SUCCESSFUL)
                retStat = BOB_SICK;
	        break;

        case LE_SLONG_TO_STRING:
            memcpy(&LongVar, itemPtr, itemSize);
            sprintf (ResultString, "%ld", (long)LongVar);
	        break;

        case LE_ULONG_TO_STRING:
            memcpy(&LongVar, itemPtr, itemSize);
            sprintf (ResultString, "%lu", (ulong)LongVar);
	        break;

        //5, 4, 3, and 2 byte, big endian money
        case MONEY_TO_STRING:
        case BE_INT_MONEY5_TO_STRING:
            if (convType == MONEY_TO_STRING)
                (void)fnMoney2Ascii( &usLength, ResultString, (UINT8 *)itemPtr );
            else
                (void)fnIntMoney2Ascii( &usLength, ResultString, (UINT8 *)itemPtr, USE_VAULT_DECIMALS );
				         
            if( usLength == 0 )
                retStat = BOB_SICK;
	        break; 

        case MONEY4_TO_STRING:
        case BE_INT_MONEY4_TO_STRING:
            TempU.TempLonger[0] = 0;            
            memcpy( &TempU.TempLonger[1], itemPtr, itemSize);
            if (convType == MONEY4_TO_STRING)
                (void)fnMoney2Ascii( &usLength, ResultString, TempU.TempLonger );
            else
                (void)fnIntMoney2Ascii( &usLength, ResultString, TempU.TempLonger, USE_VAULT_DECIMALS );
				
            if( usLength == 0 )
                retStat = BOB_SICK;
	        break; 

        case MONEY3_TO_STRING:
        case BE_INT_MONEY3_TO_STRING:
        case SQUIG_BE_INT_MONEY3:
            TempU.TempLonger[0] = 0;
            TempU.TempLonger[1] = 0;
            memcpy(&TempU.TempLonger[2], itemPtr, itemSize);
            if (convType == MONEY3_TO_STRING)
                (void)fnMoney2Ascii( &usLength, ResultString, TempU.TempLonger );
            else
            {
                (void)fnIntMoney2Ascii( &usLength, ResultString, TempU.TempLonger, USE_VAULT_DECIMALS );
				if (convType == SQUIG_BE_INT_MONEY3)
				{
	                // Replace the leading zero with squiggle char if needed.
/*
	                if( ResultString[0] == UNICHR_ZERO )
	                {
	                    if( fnFlashGetByteParm(BP_PRINT_BARS_LEFT) == TRUE )
	                    {
	                        ResultString[0] = (UNICHAR) fnFlashGetByteParm(BP_PRINTABLE_SQUIGGLE_CHAR);
	                    }
	                }
*/

	                // Replace the trailing zero with squiggle char if needed.
/*
	                if( ResultString[usLength-1] == UNICHR_ZERO )
	                {
	                    if( fnFlashGetByteParm(BP_PRINT_BARS_RIGHT) == TRUE )
	                    {
	                        ResultString[usLength-1] = (UNICHAR) fnFlashGetByteParm(BP_PRINTABLE_SQUIGGLE_CHAR);
	                    }
	                }
*/
                }
            }
				
            if( usLength == 0 )
                retStat = BOB_SICK;
	        break;

        case SQUIG_ASCII_POSTAGE:
            if( itemSize == 0 )
                retStat = BOB_SICK;
				
            strncpy( ResultString, itemPtr, itemSize );
            ResultString[itemSize] = 0;
			
            // Replace the leading zero with squiggle char if needed.
/*
            if( ResultString[0] == UNICHR_ZERO )
            {
                if( fnFlashGetByteParm(BP_PRINT_BARS_LEFT) == TRUE )
                {
                    ResultString[0] = (UNICHAR) fnFlashGetByteParm(BP_PRINTABLE_SQUIGGLE_CHAR);
                }
            }
*/

            // Replace the trailing zero with squiggle char if needed.
/*
            if( ResultString[itemSize-1] == UNICHR_ZERO )
            {
                if( fnFlashGetByteParm(BP_PRINT_BARS_RIGHT) == TRUE )
                {
                    ResultString[itemSize-1] = (UNICHAR) fnFlashGetByteParm(BP_PRINTABLE_SQUIGGLE_CHAR);
                }
            }
*/
	        break;

        case MONEY2_TO_STRING:
        case BE_INT_MONEY2_TO_STRING:
            TempU.TempLonger[0] = 0;
            TempU.TempLonger[1] = 0;
            TempU.TempLonger[2] = 0;
            memcpy(&TempU.TempLonger[3], itemPtr, itemSize);
            if (convType == MONEY2_TO_STRING)
                (void)fnMoney2Ascii( &usLength, ResultString, TempU.TempLonger );
            else
                (void)fnIntMoney2Ascii( &usLength, ResultString, TempU.TempLonger, USE_VAULT_DECIMALS );
				
            if( usLength == 0 )
                retStat = BOB_SICK;
	        break;

        // Canadian oddball case:
        // vault decimals = 3, but postage decimals = 2 in debit certificate
        case BE_INT_MONEY2_DEC2_TO_STRING:
            TempU.TempLonger[0] = 0;
            TempU.TempLonger[1] = 0;
            TempU.TempLonger[2] = 0;
            memcpy(&TempU.TempLonger[3], itemPtr, itemSize);
            (void)fnIntMoney2Ascii( &usLength, ResultString, TempU.TempLonger, 2 );
            if( usLength == 0 )
                retStat = BOB_SICK;
	        break;

        //5, 4, 3, and 2 byte, little endian money
        case LE_MONEY_TO_STRING:
        case LE_INT_MONEY5_TO_STRING:   
            TempU.TempLonger[0] = (UINT8)*(itemPtr+4);
            TempU.TempLonger[1] = (UINT8)*(itemPtr+3);
            TempU.TempLonger[2] = (UINT8)*(itemPtr+2);
            TempU.TempLonger[3] = (UINT8)*(itemPtr+1);
            TempU.TempLonger[4] = (UINT8)*itemPtr;
            if (convType == LE_MONEY_TO_STRING)
                (void)fnMoney2Ascii( &usLength, ResultString, TempU.TempLonger );
            else
                (void)fnIntMoney2Ascii( &usLength, ResultString, TempU.TempLonger, USE_VAULT_DECIMALS );
				
            if( usLength == 0 )
                retStat = BOB_SICK;
	        break;

        case LE_MONEY4_TO_STRING:   
        case LE_INT_MONEY4_TO_STRING:
            TempU.TempLonger[0] = 0;
            TempU.TempLonger[1] = *(itemPtr+3);
            TempU.TempLonger[2] = *(itemPtr+2);
            TempU.TempLonger[3] = *(itemPtr+1);
            TempU.TempLonger[4] = *itemPtr;
            if (convType == LE_MONEY4_TO_STRING)
                (void)fnMoney2Ascii( &usLength, ResultString, TempU.TempLonger );
            else
                (void)fnIntMoney2Ascii( &usLength, ResultString, TempU.TempLonger, USE_VAULT_DECIMALS );
				
            if( usLength == 0 )
                retStat = BOB_SICK;
	        break;

        case LE_MONEY3_TO_STRING:   // 1 little, 2 little, 3 little endian
        case LE_INT_MONEY3_TO_STRING:
            TempU.TempLonger[0] = 0;
            TempU.TempLonger[1] = 0;
            TempU.TempLonger[2] = *(itemPtr+2);
            TempU.TempLonger[3] = *(itemPtr+1);
            TempU.TempLonger[4] = *itemPtr;
            if (convType == LE_MONEY3_TO_STRING)
                (void)fnMoney2Ascii( &usLength, ResultString, TempU.TempLonger );
            else
                (void)fnIntMoney2Ascii( &usLength, ResultString, TempU.TempLonger, USE_VAULT_DECIMALS );
				
            if( usLength == 0 )
                retStat = BOB_SICK;
	        break;

        case LE_MONEY2_TO_STRING:   
        case LE_INT_MONEY2_TO_STRING:
            TempU.TempLonger[0] = 0;
            TempU.TempLonger[1] = 0;
            TempU.TempLonger[2] = 0;
            TempU.TempLonger[3] = *(itemPtr+1);
            TempU.TempLonger[4] = *itemPtr;
            if (convType == LE_MONEY2_TO_STRING)
                (void)fnMoney2Ascii( &usLength, ResultString, TempU.TempLonger );
            else
                (void)fnIntMoney2Ascii( &usLength, ResultString, TempU.TempLonger, USE_VAULT_DECIMALS );
				
            if( usLength == 0 )
                retStat = BOB_SICK;
	        break;

        case LE_INT_MONEY3_DEC_EMD_TO_STRING:
            TempU.TempLonger[0] = 0;
            TempU.TempLonger[1] = 0;
            TempU.TempLonger[2] = *(itemPtr+2);
            TempU.TempLonger[3] = *(itemPtr+1);
            TempU.TempLonger[4] = *itemPtr;
            
            dblMoney = fnBinFive2Double(TempU.TempLonger);
            usLength = fnIntMoney2UnicodeFmt( pUnicode, dblMoney, TRUE, 
                                            fnFlashGetByteParm(BP_MIN_PRINTED_INTEGERS), 
                                            fnFlashGetByteParm(BP_MIN_PRINTED_DECIMALS) , 
                                            TRUE, USE_VAULT_DECIMALS );
											
            if( fnUnicode2Ascii(pUnicode, ResultString, usLength) )
            {
                ResultString[usLength] = 0;
            }

            if( usLength == 0 )
                retStat = BOB_SICK;
	        break;

        case IB_IP_MAILDATE_TO_STRING:
            // India Post maildate format is ASCII DDDY
            (void)GetPrintedDate(&dt,GREGORIAN);
			
            wDecimalVar = (*itemPtr - 0x30) * 100;
            wDecimalVar += (*(itemPtr+1) - 0x30) * 10;
            wDecimalVar += *(itemPtr+2) - 0x30;
			
            fnDayOfYearToDayMonth(wDecimalVar, &dt);
            if(fnFormatIndiciaDate(ResultString,&dt) != SUCCESSFUL)
                retStat = BOB_SICK;
	        break;

        case IB_IBIL_MAILDATE_TO_STRING:
            // IBI Lite maildate format is 2 byte BCD YDDD, but little endian (DDYD), got that?
            (void)GetPrintedDate(&dt,GREGORIAN);
			
            wDecimalVar = *itemPtr & 0x0F;
            wDecimalVar += ((*itemPtr & 0xF0) >> 4) * 10;
            wDecimalVar += (*(itemPtr+1) & 0x0F) * 100;
			
            fnDayOfYearToDayMonth(wDecimalVar, &dt);
            if(fnFormatIndiciaDate(ResultString,&dt) != SUCCESSFUL)
                retStat = BOB_SICK;
	        break;

        // These cases are not yet supported in FPHX:
        case IB_NETSET2_PRODUCTIONDATE_TO_STRING:
            fnFormatNetSet2ProdDate( (UINT8 *)itemPtr, ResultString, &dt );
	        break;
        
        case IB_NETSET2_MAILDATE_TO_STRING:
            retStat = BOB_SICK;            

            // first get the production date
            if( fnFlashGetIBPRecVar(&rFieldRec, IPB_GET_INDICIA_DEF_REC, GLOBID_DATEOFMAILING) )
            {
                pData = pIPSD_barcodeData.pGlob;     
                fnFormatNetSet2ProdDate( pData+rFieldRec.usFieldOff, TempU.TempString, &dt );
				
                // then get the mail date offset
                memcpy( TempU.TempString, itemPtr, itemSize );     //IPSD_ADVANCEDDATEOFFSET_SZ
                TempU.TempString[itemSize] = 0; 
                wDecimalVar = (UINT16)atoi( TempU.TempString );
				
                // add the mail date to the creation date
                while( wDecimalVar-- )
                {
                    AdjustDayByOne( &dt, TRUE );
                }
				
                if( fnFormatIndiciaDate( ResultString, &dt ) == SUCCESSFUL )
                    retStat = BOB_OK;
            }
	        break;
        
        case ASKII_MON_WCS_NO_FIXED_TO_STRING:
        case ASKII_MON_WCS_TO_STRING:
            // For NetSet2 postage amount:
            // The debit certificate format of amount is nnnnn with 2 implied decimals and padded with leading zeros
			//
            // For U.K. postage amount:
            // For the debit, the postage has a format of nnnnn with 3 implied decimals, of which 1 is a fixed zero,
			// and padded with leading zeros, but the postage in the flex-debit data for the 2D barcode and the postage
			// that is printed in the indicia has to have 2 implied decimals.  This change needs to be done when setting
			// up the data for the flex-debit, which is done before we get to this point, so need to use the
			// ASKII_MON_WCS_TO_STRING field op value instead of the ASKII_MON_WCS_NO_FIXED_TO_STRING field op value.
			//
			// In addition, for the human-readable postage value, Need to strip off the leading zeros except for the 
			// first one, then add the currency symbol.
			//
            // e.g. for NetSet2, convert 00050 to $050, 00150 to $150, 01250 to $1250, 12350 to $12350, etc.
            // e.g. for the U.K., convert 00050 to $005, 00150 to $015, 01250 to $125, 12350 to $1235, etc.

			bDecimalVar = 0;
			if (convType == ASKII_MON_WCS_NO_FIXED_TO_STRING)
			{
				bDecimalVar = fnFlashGetIBByteParm(IBP_FIXED_ZEROS);
			}

            memcpy( TempU.TempString, itemPtr, itemSize );
            pData = (uchar*) &TempU.TempString[0];

			if (bDecimalVar < itemSize)
			{
				itemSize -= bDecimalVar;
			}

            TempU.TempString[itemSize] = 0;
            usLength = itemSize;
			
            // skip over any leading zeros except the least significant integer digit
            while((usLength > 3) && (*pData == '0'))
            {
                pData++;
                usLength--;
            }

            usLength = fnAsciiAddMoneySign(pData, (unsigned char)usLength);
            pData[usLength] = 0;
            (void)strcpy(ResultString, (char*) pData);
	        break;

		case IB_ASCII_MAILDATE_TO_STRING:
			bDecimalVar = fnFlashGetIBByteParm(IBP_FLEX_DEBIT_BC_DATE_FORMAT) & DATE_FORMAT_BASE;
		    (void)memset(&dt, 0, sizeof(DATETIME));
		    dt.bDayOfWeek = 1;		// just so there's something in there since a value of 0 is invalid.
		    dt.bCentury = 20;

			if (itemSize == 6)
			{
				switch (bDecimalVar)
				{
					case DATE_FORMAT_MMDDYY:
					    dt.bYear =  (unsigned char)(((itemPtr[4] - 0x30) * 10) + (itemPtr[5] - 0x30));
					    dt.bMonth = (unsigned char)(((itemPtr[0] - 0x30) * 10) + (itemPtr[1] - 0x30));
					    dt.bDay =   (unsigned char)(((itemPtr[2] - 0x30) * 10) + (itemPtr[3] - 0x30));
					break;

		            case DATE_FORMAT_DDMMYY:        
					    dt.bYear =  (unsigned char)(((itemPtr[4] - 0x30) * 10) + (itemPtr[5] - 0x30));
					    dt.bMonth = (unsigned char)(((itemPtr[2] - 0x30) * 10) + (itemPtr[3] - 0x30));
					    dt.bDay =   (unsigned char)(((itemPtr[0] - 0x30) * 10) + (itemPtr[1] - 0x30));
					break;

					case DATE_FORMAT_YYMMDD:
					    dt.bYear =  (unsigned char)(((itemPtr[0] - 0x30) * 10) + (itemPtr[1] - 0x30));
					    dt.bMonth = (unsigned char)(((itemPtr[2] - 0x30) * 10) + (itemPtr[3] - 0x30));
					    dt.bDay =   (unsigned char)(((itemPtr[4] - 0x30) * 10) + (itemPtr[5] - 0x30));
					break;

					case DATE_FORMAT_MMMDDYY:
					case DATE_FORMAT_DDMMMYY:
					default:
						retStat = BOB_SICK;
				}
			}
			else	// itemSize must be 8
			{
				switch (bDecimalVar)
				{
					case DATE_FORMAT_MMDDYYYY:
					    dt.bYear =  (unsigned char)(((itemPtr[6] - 0x30) * 10) + (itemPtr[7] - 0x30));
					    dt.bMonth = (unsigned char)(((itemPtr[0] - 0x30) * 10) + (itemPtr[1] - 0x30));
					    dt.bDay =   (unsigned char)(((itemPtr[2] - 0x30) * 10) + (itemPtr[3] - 0x30));
					break;

		            case DATE_FORMAT_DDMMYYYY:        
					    dt.bYear =  (unsigned char)(((itemPtr[6] - 0x30) * 10) + (itemPtr[7] - 0x30));
					    dt.bMonth = (unsigned char)(((itemPtr[2] - 0x30) * 10) + (itemPtr[3] - 0x30));
					    dt.bDay =   (unsigned char)(((itemPtr[0] - 0x30) * 10) + (itemPtr[1] - 0x30));
					break;

					case DATE_FORMAT_YYYYMMDD:
					    dt.bYear =  (unsigned char)(((itemPtr[2] - 0x30) * 10) + (itemPtr[3] - 0x30));
					    dt.bMonth = (unsigned char)(((itemPtr[4] - 0x30) * 10) + (itemPtr[5] - 0x30));
					    dt.bDay =   (unsigned char)(((itemPtr[6] - 0x30) * 10) + (itemPtr[7] - 0x30));
					break;

					case DATE_FORMAT_MMMDDYYYY:
					case DATE_FORMAT_DDMMMYYYY:
					default:
						retStat = BOB_SICK;
				}
			}

			if (retStat != 	BOB_SICK)
				if (fnFormatDate(ResultString, &dt, TRUE) != SUCCESSFUL)
					retStat = BOB_SICK;
			break;
        
        default:
            retStat = BOB_SICK;
	        break;
    }

    return (retStat);   //you're welcome
}

/******************************************************************************
   FUNCTION NAME: fnPleaseConvertThis2Unicode
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  :
                : 
   PARAMETERS   : pointer to data to be converted,
                : size of data to be converted
                : conversion type 
                : pointer to unicode result string
   RETURN       : Number of unicode characters in result string
   NOTES        : None
******************************************************************************/

uchar fnPleaseConvertThis2Unicode(  char *itemPtr, 
                            unsigned short itemSize,    
                            unsigned char  convType,
                            unichar* UniResultString)
{
    uchar ucLen= 0;
    char    ResultString[MAX_RESULT_STRING+1];

    memset(ResultString,0, MAX_RESULT_STRING+1);

    if(fnPleaseConvertThis2Ascii(itemPtr, itemSize, convType, ResultString))
    {
        ucLen = (UINT8)strlen( ResultString );
        (void)unistrcpyFromCharStr( UniResultString, ResultString );
    }

    return(ucLen);     //aah shucks...it was nothin
}

BOOL fnWaitForBobInit() 
{
    BOOL    retStat;
    ushort  waitCountDown;
    uchar   caller;

    retStat = TRUE;
    caller = OSGetCurrentTask();                    // retrieve the caller's task identification
    if (caller != SCM) {
        waitCountDown = 600;                        // 600 * 100 ms = 60 second max wait for bob
        while (bobsBeenInitialized == FALSE) {
            OSWakeAfter(100);                       // do not run without bob being initialized 
            if (waitCountDown) waitCountDown--;     // but do not wait forever                  
            else {
                retStat = FALSE;
                break;
            }
        }
    }
    return(retStat);
}

/*************************************************************************

 FUNCTION NAME:     BOOL fnIsFlashParameterIdValid(TYPE, ID)

 PURPOSE:           
 checks a table cooresponding to the TYPE for availablity
 
// Table names are in flashIdLookupXref[] = {
//      UIC_FlashByteDataValid,         //  BYTE_PARM  (TYPE names from EDM.h )  */ 
//      UIC_FlashWordDataValid,         //  WORD_PARM      */   
//      UIC_FlashLongDataValid,         //  LONG_PARM      */   
//      UIC_FlashStringDataValid,       //  STRING_PARM    */   
//      UIC_FlashMonyDataValid,         //  MONETARY_PARM  */   
//      UIC_FlashPackedDataValid,       //  PACKED_PARM    */
//  } 
// AUTHOR:          Bill Bailey
// INPUTS:          
/**************************************************************************/

BOOL    fnIsFlashParameterIdValid( uchar parmType, unsigned short parmId ) 
{
    BOOL    retVal = TRUE;

    if( parmType <= IMULTIPACKED_PARAM )  
    {
        switch( parmType ) 
        {
            case BYTE_PARM:
                if (parmId >= MAX_BYTE_PARM )
                    retVal = FALSE;
                break;

            case WORD_PARM:
                if (parmId >= MAX_WORD_PARM )
                    retVal = FALSE;
                break;

            case LONG_PARM:
                if (parmId >= MAX_LONG_PARM )
                    retVal = FALSE;
                break;   

            case STRING_PARM:    
                if (parmId >= MAX_ASCII_STR_PARM )
                    retVal = FALSE;
                break;
                     
            case MONETARY_PARM:
                if (parmId >= MAX_MONETARY_PARM )
                    retVal = FALSE;
                break;
                     
            case PACKED_PARM:    
                if (parmId >= MAX_PACKED_BYTE_PARM )
                    retVal = FALSE;
                break;

            case UNICODE_PARM: 
                if (parmId >= MAX_UNICODE_PARM )
                    retVal = FALSE;
                break;
                
            case IBYTE_PARAM:
                if (parmId >= MAX_IBYTE_PARM )
                    retVal = FALSE;
                break;
    
                case IWORD_PARAM:
                if (parmId >= MAX_IWORD_PARM )
                    retVal = FALSE;
                break;

            case ILONG_PARAM:
                if (parmId >= MAX_ILONG_PARM )
                    retVal = FALSE;
                break;                                          
        
            case ISTRING_PARAM:
                if (parmId >= MAX_ISTRING_PARM )
                    retVal = FALSE;
                break;

            case IMONEY_PARAM: 
                if (parmId >= MAX_IMONEY_PARM )
                    retVal = FALSE;
                break;
                    
            case IPACKED_PARAM: 
                if (parmId >= MAX_IPACKED_PARM )
                    retVal = FALSE;
                break;

            case IUNICODE_PARAM: 
                if (parmId >= MAX_IUNICODE_PARM )
                    retVal = FALSE;
                break;
                
            case IMULTIPACKED_PARAM:
                if (parmId >= MAX_IMULTIPACKED_PARM )
                    retVal = FALSE;
                break;

            default:
                retVal = FALSE;
                break;
        }
    }
    else 
        retVal = FALSE;
    return( retVal );    
}

//******************************************************************************
//   FUNCTION NAME: fnGetPsdTypes
//   AUTHOR       : Craig DeFilippo
//   DESCRIPTION  : Query PSD type
//   
//   PARAMETERS   : None
//   RETURN       : bPsdType
//   NOTES       : mmm could be : PSDT_NA, PSDT_MYKO, PSDT_IBUTTON, PSDT_LOWCOST
//               : defined in bob.h
//*****************************************************************************
uchar fnGetPsdTypes( void )
{
    return( bPsdType );
}

/******************************************************************************
   FUNCTION NAME: fnGetCurrentPsdType
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Query PSD type
                : 
   PARAMETERS   : None
   RETURN       : bPsdType
   NOTES        : mmm could be : PSDT_NA, PSDT_MYKO, PSDT_IBUTTON, PSDT_LOWCOST
                : defined in bob.h
******************************************************************************/
uchar fnGetCurrentPsdType(void)
{
    return(bPsdType);
}

/******************************************************************************
   FUNCTION NAME: fnGetPsdState
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Query PSD state
                : 
   PARAMETERS   : passback ptr to psd detailed status
   RETURN       : True if ready to print, false otherwise
   NOTES        : none
 MODS:
  2009.03.12 Clarisa Bellamy - Update for Gemini Boot Loader support.
                Make sure that we are not dealing with a Gemini in Boot loader
                mode before any attempt to send an application-level message
                to the I-button.
******************************************************************************/
BOOL fnGetPsdState(ulong* pulPsdState)
{
    BOOL    fPsdReadyToPrint = FALSE;
    UINT8   ubIpsdProgMode = IPSD_MODE_NONE;
    UINT8   ucClass;
    UINT8   ucCode;

    if( fnValidData( BOBAMAT0, BOBID_IPSD_GET_PROG_MODE, &ubIpsdProgMode) != BOB_OK )
    {
        // If the Intertask stuff is not working, then we have an error.
        fnProcessSCMError();
        *pulPsdState = (UINT32)eIPSDSTATE_INCOMMUNICADO;      
    }
    else  // Got the IpsdProgMode, so check the mode to see if the App is even running.
    {
        if( ubIpsdProgMode == IPSD_MODE_NONE )
        {
            // This means that we didn't successfully complete the initialization
            //  of the IPSD driver.
            // If there is no i-button, we end up here.
            *pulPsdState = (UINT32)eIPSDSTATE_INCOMMUNICADO;
            //clear bobs error info
            (void)fnBobErrorInfo( &ucClass, &ucCode );
        }
        else if( ubIpsdProgMode == IPSD_MODE_BOOT_LOADER )
        {
            // Not an error, but we don't want to send any app-level messages
            //  to the IPSD.
            *pulPsdState = (UINT32)eIPSDSTATE_BOOT_LOADER;
        }
        else if( ubIpsdProgMode == IPSD_MODE_UNTESTEDAPP )
        {
            // Not an error, but we don't want to send any app-level messages
            //  to the IPSD.
            *pulPsdState = (UINT32)eIPSDSTATE_UNTESTED_APP;
        }
        else if( ubIpsdProgMode == IPSD_MODE_GEMINI_UNKNOWN )
        {
            // This IS an error.  But we need to be able to querry the
            //  MicroStatus again so PCT people can figure out what's 
            //  going on.
            *pulPsdState = (UINT32)eIPSDSTATE_BAD_GEMINI;
        }
        else // Can talk to IPSD, and application is running...
        {
            // Try to read the PSD "Mood" (state)
            if( fnValidData( BOBAMAT0, GET_PSD_STATUS, pulPsdState ) == BOB_OK )
            {
            	*pulPsdState = EndianAwareGet32(pulPsdState); // in place swap
                if( (*pulPsdState == eIPSDSTATE_LOGGEDIN)       || 
                    (*pulPsdState == eIPSDSTATE_INDICIUMREADY)  ||
                    (*pulPsdState == eIPSDSTATE_GERM_LOGGEDIN)  || 
                    (*pulPsdState == eIPSDSTATE_GERM_INDICIUMREADY) )
                {
                    fPsdReadyToPrint = TRUE;    
                }
                // Any other state, not ready to print.       
            }   
            else
            {
                // Could not get the state...
                fnProcessSCMError();
                *pulPsdState = (UINT32)eIPSDSTATE_INCOMMUNICADO;      
            }
        } // End not in Boot Loader Mode.
    } // End could get IpsdProgMode.

    return(fPsdReadyToPrint);
}

/******************************************************************************
   FUNCTION NAME: fnCheckVaultInMfgState
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Query PSD state, if in any mfg state return true
                : 
   PARAMETERS   : passback ptr to psd detailed status
   RETURN       : True if vault in any mfg state, false otherwise
   NOTES        : none
 MODS:
  2009.03.12 Clarisa Bellamy - Update for Gemini Boot Loader support.
                Make sure that we are not dealing with a Gemini in Boot loader
                mode before any attempt to send an application-level message
                to the I-button.
******************************************************************************/
BOOL fnCheckVaultInMfgState(ulong* pulPsdState)
{
    BOOL    fRetVal = FALSE;
    UINT8   ubIpsdProgMode = IPSD_MODE_NONE;
    UINT8   ucClass;
    UINT8   ucCode;

    if( fnValidData( BOBAMAT0, BOBID_IPSD_GET_PROG_MODE, &ubIpsdProgMode) != BOB_OK )
    {
        // Bob has some kind of error.
        fnProcessSCMError();
        *pulPsdState = (UINT32)eIPSDSTATE_INCOMMUNICADO;      
    }
    else  // Got the IpsdProgMode, so check the mode to see if the App is even running.
    {

    	if( ubIpsdProgMode == IPSD_MODE_NONE )
        {
            // This means that we didn't successfully complete the intiialization
            //  of the IPSD driver.
            // If the i-button is missing, we end up here.
            *pulPsdState = (UINT32)eIPSDSTATE_INCOMMUNICADO;
            (void)fnBobErrorInfo( &ucClass, &ucCode );
        }
        else if( ubIpsdProgMode == IPSD_MODE_UNTESTEDAPP )
        {
            // Not an error, but we don't want to send any app-level messages
            //  to the IPSD.
            *pulPsdState = (UINT32)eIPSDSTATE_UNTESTED_APP;
            fRetVal = TRUE; 
        }
        else if( ubIpsdProgMode == IPSD_MODE_BOOT_LOADER )
        {
            // Not an error, but we don't want to send any app-level messages
            //  to the IPSD.
            *pulPsdState = (UINT32)eIPSDSTATE_BOOT_LOADER;
            fRetVal = TRUE; 
        }
        else if( ubIpsdProgMode == IPSD_MODE_GEMINI_UNKNOWN )
        {
            // This IS an error.  But we need to be able to querry the
            //  MicroStatus again so PCT people can figure out what's 
            //  going on.  And we want to display this state on the 
            //  Manufacturing screen so we know what is going on.
            *pulPsdState = (UINT32)eIPSDSTATE_BAD_GEMINI;
            fRetVal = TRUE; 
        }
        else  // We can communicate with the IPSD, and it is in App mode.
        {
            // Can communicate with the device.
            if( fnValidData( BOBAMAT0, GET_PSD_STATUS, pulPsdState) == BOB_OK  )
            {
            	*pulPsdState = EndianAwareGet32(pulPsdState); // in place swap
                if( (*pulPsdState < eIPSDSTATE_KEYSGENERATED)       ||
                    (*pulPsdState == eIPSDSTATE_PROVIDERKEYLOADED)  ||
                    (*pulPsdState == eIPSDSTATE_GERM_INITIALIZED)   ||
                    (*pulPsdState == eIPSDSTATE_GERM_KEYSGENERATED) )       
                {
                    fRetVal = TRUE; 
                }
                // Any other state, and we are not in manufacturing mode.       
            }
            else    // Could not get the state...
            {
                fnProcessSCMError();
                *pulPsdState = (UINT32)eIPSDSTATE_INCOMMUNICADO;
            }
        }
    }

    return( fRetVal );
}

/******************************************************************************
   FUNCTION NAME: fnCheckVaultAllowsVerifyHashSignature
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Query PSD state, if it allows the verify hash signature
                : operation return true
                : 
   PARAMETERS   : none
   RETURN       : True if vault in state allowing verify hash-sig, false otherwise
   NOTES        : none
 MODS:
  2009.03.12 Clarisa Bellamy - Update for Gemini Boot Loader support.
                Make sure that we are not dealing with a Gemini in Boot loader
                mode before any attempt to send an application-level message
                to the I-button.
******************************************************************************/
BOOL fnCheckVaultAllowsVerifyHashSignature(void)
{
    BOOL fRetVal = FALSE;
    UINT32  ulPsdState;
    UINT8   ubIpsdProgMode = IPSD_MODE_NONE;
    UINT8   ucClass;
    UINT8   ucCode;

    if( fnValidData( BOBAMAT0, BOBID_IPSD_GET_PROG_MODE, &ubIpsdProgMode) != BOB_OK )
    {
        // Bob has some kind of error.
        fnProcessSCMError();
        ulPsdState = (UINT32)eIPSDSTATE_INCOMMUNICADO;      
    }
    else  // Got the IpsdProgMode, so check the mode to see if the App is even running.
    {
        if( ubIpsdProgMode == IPSD_MODE_NONE )
        {
            // This means that we didn't successfully complete the initialization
            //  of the IPSD driver.
            // We get here if the ibutton is not there.
            ulPsdState = (UINT32)eIPSDSTATE_INCOMMUNICADO;
            //clear bobs error info
            (void)fnBobErrorInfo( &ucClass, &ucCode );
        }
        else if( ubIpsdProgMode == IPSD_MODE_BOOT_LOADER )
        {
            // Set the state, but the answer is FALSE.
            ulPsdState = (UINT32)eIPSDSTATE_BOOT_LOADER;
        }
        else if( ubIpsdProgMode == IPSD_MODE_UNTESTEDAPP )
        {
            // Set the state, but the answer is FALSE.
            ulPsdState = (UINT32)eIPSDSTATE_UNTESTED_APP;
        }
        else if( ubIpsdProgMode == IPSD_MODE_GEMINI_UNKNOWN )
        {
            // Set the state, but the answer is FALSE.
            ulPsdState = (UINT32)eIPSDSTATE_BAD_GEMINI;
        }
        else // Can talk to IPSD, and application is running...
        {
            // Now check the IPSD state...
            if( fnValidData( BOBAMAT0, GET_PSD_STATUS, &ulPsdState) == BOB_OK )
            {
            	ulPsdState = EndianAwareGet32(&ulPsdState); // in place swap
                if( (ulPsdState == eIPSDSTATE_FULLPOSTAL)       ||
                    (ulPsdState == eIPSDSTATE_WITHDRAWN)        ||
                    (ulPsdState == eIPSDSTATE_LOGGEDIN)         ||
                    (ulPsdState == eIPSDSTATE_GERM_FULLPOSTAL)  ||
                    (ulPsdState == eIPSDSTATE_GERM_WITHDRAWN)   ||
                    (ulPsdState == eIPSDSTATE_GERM_LOGGEDIN) )      
                {
                    fRetVal = TRUE; 
                }
                // In the other states, the answer is FALSE.       
            }
            else
            {
                fnProcessSCMError();
            }
        }
    }

    return(fRetVal);
}

//******************************************************************************
// Function Name :  fnBobStatusContainsNoError                                 *
// Author        :  P. Jacques                                                 *
// Date          :  03/01/99                                                   *
//                                                                             *
// Description :                                                               *
//    This message retrieves the state of Print Head                           *
//                                                                             *
// Input  :                                                                    *
//           None                                                              *
//                                                                             *
// Output :                                                                    *
//           None                                                              *
//                                                                             *
// Returns :                                                                   *
//           None                                                              *
//                                                                             *
// Functions Called :                                                          *
//  fnValidData()                    - It's a Bob Thing                        *
//                                                                             *
// Note(s) :                                                                   *
// 1. The print head is not required to be in Manufacturing mode for this      *
//    message to be processed                                                  *
// 2. Bob's Status Request ID is either PHC or PSD!                            *
//******************************************************************************

extern  struct  TransferControlStructure  EdmPhcPsd;    // EDM/BOB Control Struct

BOOL  fnBobStatusContainsNoError( unsigned short  BobStatusReqId )
{
    unsigned char  i;
    unsigned char  Status = 0;
    ushort         usIPSDStat = 0;
        

    // Get Command Status from Bob
    (void)fnValidData( (unsigned short)BOBAMAT0,                        // Group ID
                       (unsigned short)BobStatusReqId,                  // Item ID
                       (void *) EdmPhcPsd.deviceReplyStatus);           // Parameter Pointer


    // Check if the PSD is some type of I-button.
    if( bPsdType & PSDT_ALL_IBUTTON )
    {
        memcpy( (uchar*) &usIPSDStat, &EdmPhcPsd.deviceReplyStatus[2], sizeof(usIPSDStat));
        // Check for the OK value from the IPSD.
        if (usIPSDStat == IPSD_CMDRESP_OK)
            Status = 0;
        else 
            Status = 1;
        // If the last command was a boot loader command, the OK value is 0
        if( usIPSDStat == IPSD_BL_CMDRESP_OK )
            Status = 0;

    }
    else
    {
        // Convert 4 byte Error Code into a single status byte!
        for (i=0; i < DEVICE_STAT_SIZ4 ; i++ )
        {
            Status  |=  EdmPhcPsd.deviceReplyStatus[ i ];
        }
    }
        
    // If all 4 Bob Status bytes are Zero, return true
    if ( Status  ==  0)
    {
        return( true );
    }
    // Otherwise, return false as we have a valid Error Code somewhere within Status Bytes
    else
    {
        return( false );
    }
}

/******************************************************************************
   FUNCTION NAME: fnPermitMailPrintComplete
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : BOB post processing to be called after a Permit mail piece is
                : printed.
   PARAMETERS   : None
   RETURN       : BOOL true if no problems, FALSE otherwise
   NOTES        : none
******************************************************************************/
BOOL fnPermitMailPrintComplete(void)
{
    // kicks the global piece count and the permit specific batch count
    IncrementPermitBatch();

    // kick the departmental account's piece count
/*
    if(fnGetAccountingType() == ACT_SYS_TYPE_INTERNAL_ON)
    {
        ushort usActiveAcct = 0;

        usActiveAcct = fnGetActiveAccount();
        if (usActiveAcct)
        {
            (void)fnChargeAccount( usActiveAcct, 0.0, 1 );
        }
    }
*/

    return(TRUE);
}

/******************************************************************************
   FUNCTION NAME: fnBobGetPermitGraphicPtrByName
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Get pointer to permit graphic (WITHOUT the link header) with 
                  name matching the one passed in
   PARAMETERS   : None
   RETURN       : BOOL true if no problems, FALSE otherwise
   NOTES        : might not need this, we'll see
******************************************************************************/
#if 0
void* fnBobGetPermitGraphicPtrByName( unichar* sPermitName )
{
    void    *pGrfk = NULL_PTR;
    UINT8   i = 0;
    UINT8   *pGName = NULL_PTR;
    unichar sName[MAX_PERMIT_UNICHARS+1];
    //ushort aPermitIDs[10];
    AIPermitList pPermitList;
    UINT16  usNumPermits = 0;

    memset( &pPermitList, 0, sizeof(pPermitList) );
    usNumPermits = fnAIGetEnabledPermitIdList(&pPermitList);
    while( i < usNumPermits )
    {
        pGName = fnGrfxGetUniNameByGenID( pPermitList.Permit[i].usPermitType, pPermitList.Permit[i].usPermitID, TRUE );
        if( pGName != NULL_PTR )
        {
            memcpy( sName, pGName, sizeof(sName) );
            sName[MAX_PERMIT_UNICHARS] = 0; 
            if( unistrcmp(sName, sPermitName) == 0 )
            {   // Found a matching name.  Get a pointer to the graphic WITHOUT the link header.
                pGrfk = fnGrfxGetEnbldGrfkPtrByGenID( pPermitList.Permit[i].usPermitType, pPermitList.Permit[i].usPermitID );
                break;
            }
        }
        i++;
    }

    return( pGrfk );
}
#endif

/******************************************************************************
   FUNCTION NAME: fnScrubCurrentGraphicsList
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Utility to check if all currently selected ads, inscriptions
                : permits, and text messagess are still available in the system.
                : If something is no longer available remove it from the selection
                : list. 
                : For Ads, Inscriptions and Permits, check that there is an
                : entry in the Graphics File directory with the compType and
                : GenID in the selection variable.
                : For text Ads, check that the selection is in range, and maps
                : to an entry in the CMOS table with a name of at least one character.
   PARAMETERS   : None
   RETURN       : none
   NOTES        : should be called routinely after a graphics download
******************************************************************************/
void fnScrubCurrentGraphicsList( void )
{
    void    *pLnkPtr;
    BOOL    fEnabled = FALSE;
    UINT8   ubIndex; 
    UINT8   ubStatus;
    UINT8   ubCompType;
    const TEXT_ENTRY_COMPONENT *pTextEntry = NULL_PTR;
    
 extern UINT8 bUICPrintMode;

    if(IsFeatureEnabled(&fEnabled, PERMIT_MODE_SUPPORT, &ubStatus, &ubIndex) && (fEnabled == FALSE) )
    {
        if(bUICPrintMode == BOB_PERMIT_MODE)
        {
            currentPermitSelection = TURN_OFF_IMAGE;
            bUICPrintMode = BOB_DEBIT_FUNDS;
        }
    }

    if(   IsFeatureEnabled(&fEnabled, MANUAL_TEXT_MSGS_SUPPORT, &ubStatus, &ubIndex) 
       && (fEnabled == FALSE) )
    {
        currentTextEntrySelection = TURN_OFF_IMAGE;
    }
    
    if(currentAdSelection != TURN_OFF_IMAGE)
    {
        pLnkPtr = fnGrfxGetLnkPtrByGenID( IMG_ID_AD_SLOGAN, currentAdSelection, GRFX_ANY_MATCH );  
        if( pLnkPtr == NULL_PTR )
        {
            currentAdSelection = TURN_OFF_IMAGE;
        }
    }
    
    if(currentInscrSelection != TURN_OFF_IMAGE)
    {
        pLnkPtr = fnGrfxGetLnkPtrByGenID( IMG_ID_INSCRIPTION, currentInscrSelection, GRFX_ANY_MATCH );   
        if( pLnkPtr == NULL_PTR )
        {
            currentInscrSelection = TURN_OFF_IMAGE;
            currentUserInscrSelection = TURN_OFF_IMAGE;
        }
    }

    if(currentTextEntrySelection != TURN_OFF_IMAGE)
    {
        // (The system limit for Text Entries is 5)
        pTextEntry = fnGetTextEntryComponentAddr( (UINT8)currentTextEntrySelection - 1 );
        if( !unistrlen(pTextEntry[currentTextEntrySelection - 1].sTextEntryName) )
        {
            currentTextEntrySelection = TURN_OFF_IMAGE;
        }
    }

    // If nothing is selected, skip this.
    if( currentPermitSelection != TURN_OFF_IMAGE )
    {
        // Set the currently selected permit ID and CompType to the named permit selection
        currentPermitSelection = fnGrfxGetPermitIdAndTypeByName( &ubCompType, currentPermitName );  
        currentPermitTypeSelection = ubCompType;
        if( currentPermitSelection == 0 )
        {
            // No permit exists with the currently selected name, so deselect the current permit.
            // The CompType has already been set to TURN_OFF_IMAGE by fnAIGetPermitIdAndTypeByName.
            currentPermitSelection = TURN_OFF_IMAGE;
            currentPermitTypeSelection = TURN_OFF_IMAGE;  // Change it from 0xFF to 0xFFFF.
            memset( currentPermitName, 0, sizeof(currentPermitName) );
        }
    }
    return;
}

/******************************************************************************
   FUNCTION NAME: fnAnyGraphicsMissing
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Utility to check if all currently selected graphics
                : are available in the system.
   PARAMETERS   : None
   RETURN       : TRUE if any graphics are missing
   NOTES        : should be called in main menu check
******************************************************************************/
BOOL fnAnyGraphicsMissing(uchar* pMissingGraphicList)
{
    BOOL fRetVal = FALSE;
    int i;
    extern uchar aMissingGraphic[]; 

    for (i=0; i<MISSING_GRAPHIC_LIST_SZ; i++)
    {
        if (aMissingGraphic[i] > 0) 
        {
            fRetVal = TRUE;
            break;
        }
    }

    memcpy(pMissingGraphicList, aMissingGraphic, MISSING_GRAPHIC_LIST_SZ);

    return(fRetVal);
}

BOOL    fnFrancanSense()
{
    BOOL    stat = FALSE;

    if (fnFlashGetIBByteParm(IBP_COUNTRY_ID) == ISO_DEU) 
    {
        stat = TRUE;
    }
    return(stat);
}   

//****************************************************************************
// FUNCTION NAME:   fnGetPsdManufactureDate
//   DESCRIPTION:   Get PSD manufacture date in DATETIME format 
//
//        AUTHOR:   Howell Sun
//
//     ARGUMENTS:   DATETIME *tIPsdBirthday
//        RETURN:   TRUE: 
//
//         NOTES:   
//-----------------------------------------------------------------------------
BOOL fnGetPsdManufactureDate(DATETIME *tIPsdBirthday)
{
    ulong lClockOffset = 0;
    ulong lGmtOffset = 0;
    ulong lPsdBirthday = 0;

    EndianAwareCopy( (char *)&lGmtOffset, pIPSD_GMTOffset, IPSD_SZ_TIME );
    EndianAwareCopy( (char *)&lClockOffset, pIPSD_clockOffset, IPSD_SZ_TIME );

    lPsdBirthday = lGmtOffset + lClockOffset;

    // Convert IPSD time format to system time format.
    fnIPSDTimeToSys( tIPsdBirthday, lPsdBirthday );

    return TRUE;

} // fnGetPsdManufactureDate()

/******************************************************************************
   FUNCTION NAME: fnGetPsdDateTime
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Get a fresh copy of GMT and Adjusted time from the PSD's
                : RTC clock.
   PARAMETERS   : pointer to pass back GMT, pointer to passback adjusted time
   RETURN       : TRUE if no BOB_ERRORs, FALSE otherwise
   NOTES        : None
******************************************************************************/
BOOL fnGetPsdDateTime(DATETIME *dtPsdGmt, DATETIME *dtPsdAdj)
{
    BOOL    fRetVal = TRUE;
    long    lRTC, lOffset, lGMTOffset;
    
    memset(dtPsdGmt, 0, sizeof(DATETIME));
    memset(dtPsdAdj, 0, sizeof(DATETIME));

    dtPsdGmt->bDayOfWeek = 2;  // I met her on a monday and my heart stood still
    dtPsdAdj->bDayOfWeek = 2;  // I met her on a monday and my heart stood still

    // get the RTC, i.e. seconds since power applied to ibutton
    if(fnValidData(BOBAMAT0,BOBID_IPSD_RTC,&lRTC) != BOB_OK)
    {
        fnProcessSCMError();
        fRetVal = FALSE;    
    }
    
    if(fRetVal == TRUE)
    {
        // get the clock offset (offset to obtain GMT) and the GMT offset (offset from GMT)
        //  both are retreived in one fnValidData call which gets the PSD parameters
        if(fnValidData(MFGPARMAMAT0,BOBID_IPSD_CLOCKOFFSET,&lOffset) != BOB_OK)
        {
            fnProcessSCMError();
            fRetVal = FALSE;
        }
        else
        {

#if defined (JANUS) && !defined (K700)
            extern long lDcapTimestampFudge;
            extern unsigned short wBuildTestMode;

            /* data capture time fudge is in seconds and it applies as long as we are in test mode */
            if (wBuildTestMode == IN_BUILD_TEST_MODE)
                lRTC += lDcapTimestampFudge;
#endif

            lRTC += EndianAwareGet32(&lOffset);
            fnIPSDTimeToSys( dtPsdGmt, lRTC );

            EndianAwareCopy(&lGMTOffset, pIPSD_GMTOffset, sizeof(lGMTOffset));
            lRTC += lGMTOffset;
            fnIPSDTimeToSys( dtPsdAdj, lRTC );
        }
    }

    return(fRetVal);
}

/******************************************************************************
   FUNCTION NAME: fnCheckIfUserSelectGraphicAllowedInMode
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Check the Print map associated with the passed BOB mode for
                : the presence of the passed component type. Only checks for
                : Ads, Insc's, and Text messages.
   PARAMETERS   : BOB mode, component type
   RETURN       : TRUE if the component type is present in the region map
   NOTES        : None
******************************************************************************/
BOOL fnCheckIfUserSelectGraphicAllowedInMode( UINT8 ucBobMode, UINT8 ucCompType )
{
    BOOL    fRetVal = FALSE;
    BOOL    fScan = TRUE;
    UINT8   ucPrintMapType = 0xFF;

    // determine which print map to scan
    // and check for obviously prohibited graphics
    switch(ucBobMode)
    {
        case BOB_AD_ONLY_MODE:
            if(ucCompType != IMG_ID_AD_SLOGAN)
            {
                fScan = FALSE;
            }
            //lint -fallthrough no break
        case BOB_DEBIT_FUNDS:
            ucPrintMapType = IMG_ID_NORMAL_INDICIA;
            break;

        case BOB_PERMIT_MODE:
            ucPrintMapType = IMG_ID_PERMIT1_PE;
            break;

        case BOB_DATE_STAMP_ONLY_MODE:
            if(ucCompType != IMG_ID_DATE_TIME)
                fScan = FALSE;
            //lint -fallthrough no break
        case BOB_DATE_STAMP_MODE:
            ucPrintMapType = IMG_ID_DATE_TIME;
            break;

        case BOB_TAX_MODE:
            ucPrintMapType = IMG_ID_TAX_GRAPHIC;
            break;

        default:
            fScan = FALSE;
            break;
    }

    if (fScan)
    {
        fRetVal = fnCheckForComponentInPrintMap(ucPrintMapType, ucCompType);
    }

    return(fRetVal);
}

/******************************************************************************
   FUNCTION NAME: fnCheckPieceCountEndOfLife
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Check if the piece count end of life warning threshold or
                : lockout threshold has been reached. 
   PARAMETERS   : none
   RETURN       : PSD_EOL_OK or PSD_EOL_LOCKOUT or PSD_EOL_WARNING 
   NOTES        : Warning! this function does not refresh BOB's piece count prior
                : to checking it
******************************************************************************/
uchar fnCheckPieceCountEndOfLife(void)
{
    uchar ucRetVal = PSD_EOL_OK, ucEolLLockEnabled;
    ulong ulPC, ulEolWarning, ulEolLockout;

    ucEolLLockEnabled = fnFlashGetIBByteParm(IBP_REGDATA_EOLLOCK_ENABLED);
    if(ucEolLLockEnabled)
    {
        memcpy(&ulPC, pIPSD_pieceCount, sizeof(long));
        ulEolLockout = fnFlashGetIBLongParm(ILP_REGDATA_EOLLOCK_VAL);
        if(ulPC >= ulEolLockout)
        {
            ucRetVal = PSD_EOL_LOCKOUT;
        }
        else 
        {
            // check for the warning threshold, the lockout value - the warning offset
            ulEolWarning = fnFlashGetIBLongParm(ILP_REGDATA_EOLLOCK_WARN);
            if(ulPC >= ulEolLockout - ulEolWarning)
            {
                ucRetVal = PSD_EOL_WARNING;
            }
        }
    }

    return(ucRetVal);
}

/******************************************************************************
   FUNCTION NAME: fnUpdateAutoInscriptionSelection
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Run through BOB's inscription selection logic. 
   PARAMETERS   : none
   RETURN       : none 
   NOTES        : For use by OI to invoke the inscription selection logic in 
                : instances where the rating informtion changed but BOB will not
                : get a BOB_GEN_STATIC_IMAGE_REQ message, which is when this
                : logic would normally be automatically invoked. 
******************************************************************************/
void fnUpdateAutoInscriptionSelection(void)
{
    if(fnDetermineInscrSelection(0) == BOB_SICK)
    {
        fnProcessSCMError();
    }   
}

/******************************************************************************
   FUNCTION NAME: fnGetIpsdPreCreateState
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : Returns true if IPSD is in PreCreated state. 
                    otherwise returns FALSE.
   PARAMETERS   : none
   RETURN       : none 
   NOTES        : Uses a private bob flag. 
******************************************************************************/
BOOL fnGetIpsdPreCreateState( void )
{
    return( fIpsdIndiPrecreatedState );
}


/*lint -restore */ // converting enum to int and loss of info */

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetDecimalsAccepted
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets the number 
//         of characters that may be typed after the decimal point.
//
// INPUTS:
//         pData - pointer to the number of characters that may be typed after 
//                 the decimal point.
//
// RETURNS:
//         TRUE - if the Value returns successfully.
//         FALSE - if failed.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      04/24/2006    James Wang     Initial version  
//
// *************************************************************************/
BOOL fnGetDecimalsAccepted(SINT32 *pData)
{
    BOOL        fRetv = TRUE;
    UINT8       bMoneyDecimals;
    UINT8       bFixedZeroes;

    // get some vault information from the bob task
    if ( (fnValidData((UINT16)BOBAMAT0, VLT_PURSE_DECIMALS, 
                        (void *) &bMoneyDecimals) != (BOOL)BOB_OK)  ||
        (fnValidData((UINT16)BOBAMAT0, VLT_PURSE_FIXED_ZEROS, 
                        (void *) &bFixedZeroes) != (BOOL)BOB_OK))
    {
        fnProcessSCMError();
        fRetv = FALSE;
    }
    else
    {
        // use it to determine the number of characters that may be typed after
        // the decimal point
        *pData = bMoneyDecimals - bFixedZeroes;
    }

    return fRetv;
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetDebitValue
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets the  
//         register value from the Bob task.
//
// INPUTS:
//         pData - pointer to the debit register value 
//
// RETURNS:
//         TRUE - if the Value returns successfully.
//         FALSE - if failed.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/19/2006    James Wang     Initial version  
//
// *************************************************************************/
BOOL fnGetDebitValue(UINT32 *pData)
{
    BOOL        fRetv = TRUE;
	UINT32      data;

    if (fnValidData((UINT16)BOBAMAT0, 
                    VAULT_DEBIT_VALUE, 
                    (void *) &data) != (BOOL)BOB_OK)
    {
        fnProcessSCMError();
        fRetv = FALSE;
    }
	else
	{
		*pData = EndianSwap32(data);
	}

    return fRetv;
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnSetDebitValue
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function sets the  
//         register debit value in the Bob task.
//
// INPUTS:
//         pData - pointer to the debit register value 
//
// RETURNS:
//         TRUE - if the Value sets successfully.
//         FALSE - if failed.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/19/2006    James Wang     Initial version  
//
// *************************************************************************/
BOOL fnSetDebitValue( const UINT32 *pData )
{
    BOOL        fRetv = TRUE;
	//Swap the endianism so it is correct for the iButton, fnGetDebitValue will unswap endianism
	UINT32      data = EndianAwareGet32(pData);

    if( fnWriteData( (UINT16)BOBAMAT0, 
                     VAULT_DEBIT_VALUE, 
                     &data ) != (BOOL)BOB_OK)
    {
        fnProcessSCMError();
        fRetv = FALSE;
    }

    return( fRetv );
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetPSDVersion
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets  
//         the PSD version from BOB.
//
// INPUTS:
//         pPSDVersion - The pointer to PSD version string to be returned 
//
// RETURNS:
//         TRUE - if there is no error
//         FALSE - else
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/19/2006    James Wang     Initial version  
//
// *************************************************************************/
BOOL fnGetPSDVersion(char *pPSDVersion)
{
    BOOL        fRetv = TRUE;

    if (fnValidData( (UINT16)BOBAMAT0, 
                     BOBID_IPSD_FIRMWAREVERSUBSTR, 
                     (void *)pPSDVersion ) != (BOOL)BOB_OK)
    {
        fnProcessSCMError();
        fRetv = FALSE;
    }

    return fRetv;
}

// *************************************************************************
// FUNCTION NAME:       fnGetPSDVerDate
// DESCRIPTION:
//         Wrapper function for retrieving the PSD Version Date.
//
// INPUTS:
//         pPSDVerDate - Pointer to destination char array to write the string to. 
// RETURNS:
//         TRUE - if there is no error
//         FALSE - else
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//   2009.07.17 Clarisa Bellamy - Initial.
// ----------------------------------------------------------------------------
BOOL fnGetPSDVerDate( char *pPSDVerDate )
{
    BOOL        fRetv = TRUE;

    if( fnValidData( BOBAMAT0, 
                     BOBID_IPSD_FIRMWAREVER_DATE_STR, 
                     pPSDVerDate ) != (BOOL)BOB_OK )
    {
        fnProcessSCMError();
        fRetv = FALSE;
    }
 
    return fRetv;
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnBobGetDescendingReg
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets the  
//         descending register value from the Bob task.
//
// INPUTS:
//         pData - pointer to the descending register value 
//
// RETURNS:
//         TRUE - if the Value returns successfully.
//         FALSE - if failed.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/19/2006    James Wang     Initial version  
//
// *************************************************************************/
BOOL fnBobGetDescendingReg(UINT8 *pData)
{
    BOOL        fRetv = TRUE;

    if (fnValidData((UINT16)BOBAMAT0, 
                    GET_VLT_DESCENDING_REG, 
                    (void *)pData) != (BOOL)BOB_OK)
    {
        fnProcessSCMError();
        fRetv = FALSE;
    }

    return fRetv;
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetVaultSerialNum
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets the  
//         vault serial number from the Bob task.
//
// INPUTS:
//         pData - pointer to the vault serial number  
//
// RETURNS:
//         TRUE - if the Value returns successfully.
//         FALSE - if failed.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/19/2006    James Wang     Initial version  
//
// *************************************************************************/
BOOL fnGetVaultSerialNum(char *pData)
{
    BOOL        fRetv = TRUE;

    if (fnValidData( (UINT16)BOBAMAT0, 
                    VLT_INDI_FILE_INDICIA_SERIAL, 
                    (void*)pData ) != (BOOL)BOB_OK)
    {
        fnProcessSCMError();
        fRetv = FALSE;
    }

    return fRetv;
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnBobGetMaxDescendingReg
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets the  
//         maximum descending register value from the Bob task.
//
// INPUTS:
//         pData - pointer to the maximum descending register value 
//
// RETURNS:
//         TRUE - if the Value returns successfully.
//         FALSE - if failed.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/19/2006    James Wang     Initial version  
//
// *************************************************************************/
BOOL fnBobGetMaxDescendingReg(UINT8 *pData)
{
    BOOL        fRetv = TRUE;

    if (fnValidData((UINT16)BOBAMAT0, 
                    VLT_PURSE_MAX_DESC_REG, 
                    (void *)pData) != (BOOL)BOB_OK)
    {
        fnProcessSCMError();
        fRetv = FALSE;
    }

    return fRetv;
}

// *************************************************************************
// FUNCTION NAME: fnIsIBILiteCapable()
// DESCRIPTION: Return state of capability of the feature and the graphics to
//              do IBI Lite
//
// AUTHOR: Clarisa Bellamy
//
// INPUTS:  None
// OUTPUT:  True = can do IBI Lite.
//-------------------------------------------------------------------------
BOOL fnIsIBILiteCapable( void )
{
    UINT8   feature_state;
    UINT8   feature_index;
    BOOL    enabled;
    BOOL    fRetval = FALSE;

    // If the feature is enabled and the graphic exists, return true.
    if( IsFeatureEnabled(&enabled, IBI_LITE_SUPPORT, &feature_state, &feature_index) )
    { 
        if (enabled == TRUE)
        {
            // Make sure we actually HAVE an IBI-Lite indicia image.
            if( fnGrfxGetFirstGrfkPtrByType( IMG_ID_LITE_INDICIA_IMAGE, 
                                             GRFX_ANY_MATCH             ) != NULL_PTR )
            {
                fRetval = TRUE;
            }
        }
    }

    return( fRetval );
}

// *************************************************************************
// FUNCTION NAME: fnIsIBIExtraLiteCapable()
// DESCRIPTION: Return state of capability of the feature and the graphics to
//              do IBI Extra Lite
//
// AUTHOR: Clarisa Bellamy
//
// INPUTS:  None
// OUTPUT:  True = can do IBI Lite.
//-------------------------------------------------------------------------
BOOL fnIsIBIExtraLiteCapable( void )
{
    UINT8   feature_state;
    UINT8   feature_index;
    BOOL    enabled;
    BOOL    fRetval = FALSE;

    // If the feature is enabled and the graphic exists, return true.
    if( IsFeatureEnabled(&enabled, IBI_ULTRA_LITE_SUPPORT, &feature_state, &feature_index) )
    {
        if( enabled == TRUE )
        {
            // Make sure we actually HAVE an IBI-Extra Lite indicia image.
            if( fnGrfxGetFirstGrfkPtrByType( IMG_ID_EXTRA_LITE_INDICIA_IMAGE, 
                                             GRFX_ANY_MATCH                   ) != NULL_PTR )
            {
                fRetval = TRUE;
            }
        }
    }

    return( fRetval );
}

// *************************************************************************
// FUNCTION NAME: fnSetCurrentIndiSelection
// DESCRIPTION: Tries to set the UIC indicia type to ucDesiredSelection
//
// AUTHOR: Clarisa Bellamy
//
// INPUTS:  None
// OUTPUT:  True = successful indicia change.
// NOTES:   
//  1. If trying to set to IBI-Lite or IBI-ExtraLite, then the function first
//     checks to see if the system supports this setting.  It is "capable" if
//     the feature is turned on in feature table.
//  2. For Janus, the bobvalue is 2-bytes, but the application expects it to be a 
//     1-byte value. So this function converts it. (So far the max value is 3)
//     If the size is changed in the orbit database (and in cjunior.h and
//     cjunior.c) then the conversion will no longer be necessary.
// History:
//  2008.07.21  Clarisa Bellamy - updated for Janus.
//-------------------------------------------------------------------------
BOOL  fnSetCurrentIndiSelection( UINT8 ucDesiredSelection )
{
    // The bob variable is a 2-byte value, though the accessor functions 
    //  only use the lower 8-bits.  It may be changed to an 8-bit value 
    //  later on.
    UINT16   uwDesiredSelection = (UINT16)ucDesiredSelection;
    UINT8   ucCurrentSelection;
    BOOL    fStat = FALSE;
    BOOL    fBobStat;

    ucCurrentSelection = fnGetCurrentIndiSelection();
    if( ucDesiredSelection == ucCurrentSelection )
    {
        fStat = TRUE;
    }
    else
    {
        switch( ucDesiredSelection )
        {
            case NORMAL_INDICIA_SETTING:
                fBobStat = fnWriteData( BOBAMAT0, 
                                        SET_CURRENT_INDI_SELECTION, 
                                        &uwDesiredSelection );
                if( fBobStat == BOB_OK )
                {
                    fStat = TRUE;
                }
                break;

            case LITE_INDICIA_SETTING:
                if( fnIsIBILiteCapable() )
                {
                    fBobStat = fnWriteData( BOBAMAT0, 
                                            SET_CURRENT_INDI_SELECTION, 
                                            &uwDesiredSelection );
                    if( fBobStat == BOB_OK )
                    {
                        fStat = TRUE;
                    }
                }
                break;

            case EXTRA_LITE_INDICIA_SETTING:
                if( fnIsIBIExtraLiteCapable() )
                {
                    fBobStat = fnWriteData( BOBAMAT0, 
                                            SET_CURRENT_INDI_SELECTION, 
                                            &uwDesiredSelection );
                    if( fBobStat == BOB_OK )
                    {
                        fStat = TRUE;
                    }
                }
                break;

            default:
                break;
        }
    }

    return( fStat );
}

// *************************************************************************
// FUNCTION NAME: fnGetCurrentIndiSelection
// DESCRIPTION: Gets the Current UIC indicia type from bobtask
//
// AUTHOR: WI001BA
//
// INPUTS:  None
// OUTPUT:  Return current value of selected indicia or PID_INDI_NORMAL.
// NOTES:   
//  1. For Janus, the bobvalue is 2-bytes, but the application expects it to be a 
//     1-byte value. So this function converts it. (So far the max value is 3)
//     If the size is changed in the orbit database (and in cjunior.h and
//     cjunior.c) then the conversion will no longer be necessary.
// History: 
// 2008.07.21 Clarisa Bellamy - updated for Janus.
//-------------------------------------------------------------------------
UINT8 fnGetCurrentIndiSelection( void )
{
    unsigned short usPrintIdSelection = NORMAL_INDICIA_SETTING;


    if( fnValidData( BOBAMAT0, READ_CURRENT_INDI_SELECTION, &usPrintIdSelection) != BOB_OK )
    {
         usPrintIdSelection = NORMAL_INDICIA_SETTING;
    }

    // make sure the setting is one of the valid values
    switch (usPrintIdSelection)
    {
        case NORMAL_INDICIA_SETTING:
        case LITE_INDICIA_SETTING:
        case EXTRA_LITE_INDICIA_SETTING:
            break;

        default:
            usPrintIdSelection = NORMAL_INDICIA_SETTING;
            (void)fnSetCurrentIndiSelection( NORMAL_INDICIA_SETTING );
            break;
    }

    return( (unsigned char)usPrintIdSelection );
}

// *************************************************************************
// FUNCTION NAME: fnGetDefaultIndiSelection
// DESCRIPTION: Gets Always available default Indicia Selection
//
// AUTHOR: MA507HA
//
// INPUTS:  None
// OUTPUT:  Returns default indicia.
//-------------------------------------------------------------------------
uchar fnGetDefaultIndiSelection(void)
{
    return(NORMAL_INDICIA_SETTING);
}


//****************************************************************************
// FUNCTION NAME:   fnBobCheckBatteryStatus
//   DESCRIPTION:   Check PSD & CMOS batteries based on lifetime requirements
//
//        AUTHOR:   Sandra Peterson
//
//     ARGUMENTS:   none
//        RETURN:   One of the battery status values from bobutils.h 
//
//         NOTES:   
//-----------------------------------------------------------------------------
unsigned char fnBobCheckBatteryStatus(void)
{
    DATETIME        BatteryBuildDate;
    DATETIME        TodayDate;

    long            lBatteryPassedLife = 0;     // actual life time in seconds
    unsigned long   ulDesignedLife = 0;         // allowed life time in seconds
    unsigned long   ulDesignedWarning = 0;      // allowed warning time in seconds

    unsigned char   ucPsdDesignedLifeInMonths = fnFlashGetIBByteParm(IBP_PSD_LOW_BATTERY_WARNING);
    unsigned char   ucCmosDesignedLifeInMonths = fnFlashGetIBByteParm(IBP_CMOS_BATTERY_EOL);
    unsigned char   ucRetVal = BOTH_BATTERIES_OK_STATUS;


	// Get the PSD life based on the type of PSD
	switch(bPsdType)
	{
		case PSDT_GEMINI:
			ucPsdDesignedLifeInMonths = fnFlashGetIBByteParm(IBP_GEMINI_BATTERY_EOL);
			break;

		case PSDT_IBUTTON:
		default:
			// already got it above
			break;
	}

    // Start out with the number of seconds remaining being the max
    // just in case we can't get the data
    ulPsdRemainingSeconds = ucPsdDesignedLifeInMonths * SECONDS_IN_30DAYS;
    ulCmosRemainingSeconds = ucCmosDesignedLifeInMonths * SECONDS_IN_30DAYS;

    // Get today's date w/o the offsets since the PSD date is based on GMT.
    if (GetSysDateTime(&TodayDate, NOOFFSETS, GREGORIAN) == TRUE)
    {
        // clear the time in the current date because the time in the Canon
        // build date is also all zeros. Also we only care about the difference in days.
        TodayDate.bHour = 0;
        TodayDate.bMinutes = 0;
        TodayDate.bSeconds = 0;

        // If the designated life is zero, don't need to check the CMOS battery status.
        // If we can't get the board build date, can't check the CMOS battery status.
        if (ucCmosDesignedLifeInMonths &&
            (fnCMGetBoardBuildDate(&BatteryBuildDate) == TRUE))
        {
            // Calculate the allowed life in seconds
            ulDesignedLife = ucCmosDesignedLifeInMonths * SECONDS_IN_30DAYS;

            // Calculate the allowed warning in seconds
            ulDesignedWarning = ulDesignedLife -
                                (fnFlashGetIBWordParm(IWP_CMOS_BATTERY_PRE_WARNING) * SECONDS_IN_1DAY);

            // Since we were able to get the board build date, check if it's a valid date. 
            if (fnValidDate(&BatteryBuildDate) == TRUE)
            {
                // Get the difference between the two dates
                // diff = today's date - build date
                (void)CalcDateTimeDifference(&BatteryBuildDate, &TodayDate, &lBatteryPassedLife);

                // If the difference is positive, check if we're in the warning period
                // or past the end of life.
                if (lBatteryPassedLife > 0)
                {
                    // assume there's sometime left
                    ulCmosRemainingSeconds = ulDesignedLife - (unsigned long)lBatteryPassedLife;

                    // if more seconds have passed since the build date than the allowed life,
                    // indicate that the battery is around the end of life.
                    if ((unsigned long)lBatteryPassedLife > ulDesignedLife)
                    {
                        ulCmosRemainingSeconds = 0;
                        ucRetVal = CMOS_BATTERY_EOL_STATUS;
                    }

                    // else, if enough seconds have passed since the build date to get us in 
                    // the warning period, indicate that the battery is running low
                    else if ((unsigned long)lBatteryPassedLife > ulDesignedWarning)
                        ucRetVal = CMOS_BATTERY_LOW_STATUS;

                    // else, the CMOS battery is OK.
                }
            }
        }

        // If the designated life is zero, don't need to check the PSD battery status.
        // If no PSD, don't need to check the PSD battery status.
        if (ucPsdDesignedLifeInMonths && (fVltDataInitialized == TRUE))
        {
            // Calculate the allowed life in seconds
            ulDesignedLife = ucPsdDesignedLifeInMonths * SECONDS_IN_30DAYS;

            // Calculate the allowed warning in seconds
            ulDesignedWarning = ulDesignedLife -
                                (fnFlashGetIBWordParm(IWP_PSD_BATTERY_PRE_WARNING) * SECONDS_IN_1DAY);

            // get the PSD born on date
            fnBobGetPSDBatteryDate(&BatteryBuildDate);

            // Get the difference between the two dates
            // diff = today's date - born on date
            (void)CalcDateTimeDifference(&BatteryBuildDate, &TodayDate, &lBatteryPassedLife);

            if (lBatteryPassedLife > 0)
            {
                // assume there's sometime left
                ulPsdRemainingSeconds = ulDesignedLife - (unsigned long)lBatteryPassedLife;

                // if more seconds have passed since the build date than the allowed life,
                // indicate that the battery is around the end of life.
                if ((unsigned long)lBatteryPassedLife > ulDesignedLife)
                {
                    ulPsdRemainingSeconds = 0;

                    // if CMOS battery is also at end of life, indicate that both are at end of life
                    if (ucRetVal == CMOS_BATTERY_EOL_STATUS)
                        ucRetVal = BOTH_BATTERIES_EOL_STATUS;

                    // else, PSD battery end of life trumps CMOS battery low or CMOS battery OK
                    else
                        ucRetVal = PSD_BATTERY_EOL_STATUS;
                }

                // else, if enough seconds have passed since the build date to get us in 
                // the warning period, indicate that the battery is running low
                else if ((unsigned long)lBatteryPassedLife > ulDesignedWarning)
                {
                    // CMOS battery end of life trumps PSD battery low, so if CMOS
                    // battery is not at end of life, continue checking
                    if (ucRetVal != CMOS_BATTERY_EOL_STATUS)
                    {
                        // if CMOS battery is also low, indicate that both are low
                        if (ucRetVal == CMOS_BATTERY_LOW_STATUS)
                            ucRetVal = BOTH_BATTERIES_LOW_STATUS;

                        // else, PSD battery low trumps CMOS battery OK
                        else
                            ucRetVal = PSD_BATTERY_LOW_STATUS;
                    }
                }

                // else, the PSD battery is OK.
            }
        }

        // Get the difference between today and the last date we showed the warning
        // diff = today's date - last display date
        lBatteryPassedLife = 0;
        (void)CalcDateTimeDifference(&CMOSLastBatteryDisplay, &TodayDate, &lBatteryPassedLife);

        switch (ucRetVal)
        {
            case CMOS_BATTERY_EOL_STATUS:
            case PSD_BATTERY_EOL_STATUS:
            case BOTH_BATTERIES_EOL_STATUS:
                // if more than one day of seconds have passed, show the warning again.
                // else, indicate that everything is OK.
                if ((lBatteryPassedLife > 0) &&
                    ((unsigned long)lBatteryPassedLife >= SECONDS_IN_1DAY))
                    CMOSLastBatteryDisplay = TodayDate;
                else
                    ucRetVal = BOTH_BATTERIES_OK_STATUS;
                break;

            case CMOS_BATTERY_LOW_STATUS:
            case PSD_BATTERY_LOW_STATUS:
            case BOTH_BATTERIES_LOW_STATUS:
                // if more than five days of seconds have passed, show the warning again.
                // else, indicate that everything is OK.
                if ((lBatteryPassedLife > 0) &&
                    ((unsigned long)lBatteryPassedLife >=
                            (SECONDS_IN_1DAY * fnFlashGetIBByteParm(IBP_BATTERY_DISPLAY_INTERVAL))))
                    CMOSLastBatteryDisplay = TodayDate;
                else
                    ucRetVal = BOTH_BATTERIES_OK_STATUS;
                break;
    
            default:
                break;
        }
    }

    return (ucRetVal);

} // fnBobCheckBatteryStatus()


//****************************************************************************
// FUNCTION NAME:   fnBobGetPSDBatteryDate
//   DESCRIPTION:   Gets the PSD battery "turn on" date
//
//        AUTHOR:   Sandra Peterson
//
//     ARGUMENTS:   pointer to a DATETIME structure
//        RETURN:   none 
//
//         NOTES:   
//-----------------------------------------------------------------------------
void fnBobGetPSDBatteryDate(DATETIME *pDate)
{
    if (fnValidDate(&CMOSPsdBatteryTestDate) == FALSE)
    {
        (void)fnGetPsdManufactureDate(pDate);
    }
    else
    {
        pDate->bCentury = CMOSPsdBatteryTestDate.bCentury;
        pDate->bYear = CMOSPsdBatteryTestDate.bYear;
        pDate->bMonth = CMOSPsdBatteryTestDate.bMonth;
        pDate->bDay = CMOSPsdBatteryTestDate.bDay;
        pDate->bDayOfWeek = CMOSPsdBatteryTestDate.bDayOfWeek;
    }
    
    // we don't care about the time, so clear it
    pDate->bHour = 0;
    pDate->bMinutes = 0;
    pDate->bSeconds = 0;
}


//****************************************************************************
// FUNCTION NAME:   fnBobGetCMOSRemainingDays
//   DESCRIPTION:   Get the number of days remaining until the calculated
//                  end-of-life for the CMOS battery
//
//        AUTHOR:   Sandra Peterson
//
//     ARGUMENTS:   none
//        RETURN:   number of days to EOL 
//
//         NOTES:   
//-----------------------------------------------------------------------------
unsigned short fnBobGetCMOSRemainingDays(void)
{
    return((unsigned short)(ulCmosRemainingSeconds / SECONDS_IN_1DAY));
}


//****************************************************************************
// FUNCTION NAME:   fnBobGetPSDRemainingDays
//   DESCRIPTION:   Get the number of days remaining until the calculated
//                  end-of-life for the PSD battery
//
//        AUTHOR:   Sandra Peterson
//
//     ARGUMENTS:   none
//        RETURN:   number of days to EOL 
//
//         NOTES:   
//-----------------------------------------------------------------------------
unsigned short fnBobGetPSDRemainingDays(void)
{
    return((unsigned short)(ulPsdRemainingSeconds / SECONDS_IN_1DAY));
}

void GetPSDInfo(PSD_INFO *pPsdInfo){

	memcpy(pPsdInfo->sPSDVersion, pIPSD_FirmwareVerSubStr, sizeof(pIPSD_FirmwareVerSubStr));

	memcpy(pPsdInfo->PCN, pIPSD_PCN, sizeof(pIPSD_PCN));

	pPsdInfo->FamilyCode = bIPSD_familyCode;

	memcpy(pPsdInfo->modelNumber, pIPSD_modelNumber, sizeof(pIPSD_modelNumber));

	fnUnicode2Ascii(puIPSD_MfgSNAscii, pPsdInfo->MfgSNAscii, sizeof(pPsdInfo->MfgSNAscii));

	memcpy(pPsdInfo->indiciaSN, pIPSD_indiciaSN, sizeof(pIPSD_indiciaSN));

	memcpy(pPsdInfo->PBISerialNum, pIPSD_PBISerialNum, sizeof(pIPSD_PBISerialNum));

	pPsdInfo->checkSum = bIPSD_MfgSNcrc;

}


BOOL fnGetIButtonData(UINT16 msgID)
{
    BOOL                retStat;            // success-failure return flag                      
	T_WHY_SCHEDULE_BOB  stWhy;

    stWhy.ucAccessFuncCode = IBUTTON_ACCESS;
    stWhy.usGroupId = 0x000a;
    stWhy.usItemId = msgID;
 	retStat = fnScheduleBob( msgID, IBUTTON_ACCESS, WAIT_REPLY, &stWhy );

	return(retStat);
}
// Printhead Reports

// Set up data needed for Funds report
void fnSetCurReportFunds(unsigned int batchCount, unsigned int batchValue )
{
	reportBatchCount = batchCount;
	reportBatchValue = batchValue;
	fnSetCurReport(REG_RPT);
}

// Set up data needed for Refill report
void fnSetCurReportRefill(void)
{
	fnSetCurReport(REFILL_RECEIPT_RPT);
}

void fnSetCurRefundReceipt(void)
{
	fnSetCurReport(REFUND_RECEIPT_RPT);
}

//Getters
unsigned int fnGetCurReportBatchCount(void)
{
	return reportBatchCount;
}

unsigned int fnGetCurReportBatchValue(void)
{
	return reportBatchValue;
}

// Set up data needed for Date correction report
void fnSetCurReportDateCorrection(void)
{
	fnSetCurReport(DELCON_RPT);
}

/* *************************************************************************
// FUNCTION NAME:
//         fnSetRateCategory
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function sets the
//         rate category in the Bob task.
// INPUTS:
//         pData - pointer to the debit register value
//
// RETURNS:
//         TRUE - if the Value sets successfully.
//         FALSE - if failed.
//
// WARNINGS/NOTES:
//         None
//
// MODIFICATION HISTORY:
// *************************************************************************/
BOOL fnSetRateCategory( const char *pData )
{
    BOOL        fRetv = TRUE;

	memcpy(bobsRateClass, pData, 4);

    return( fRetv );
}

// From edmxml.c
BOOL fnGetGMT_LOCAL_DateTime(char *pGMTDateTime, char *pLOCALDateTime)
{
    DATETIME  stGMTDateTime;

    // get GMT Time
    if(GetSysDateTime( &stGMTDateTime, NOOFFSETS, GREGORIAN ))
        (void)sprintf(pGMTDateTime, "%02d%02d-%02d-%02dT%02d:%02d:%02d",stGMTDateTime.bCentury, stGMTDateTime.bYear, stGMTDateTime.bMonth, stGMTDateTime.bDay, stGMTDateTime.bHour, stGMTDateTime.bMinutes, stGMTDateTime.bSeconds);

    ConvertFromGMTToLocal(&stGMTDateTime, FALSE);
    (void)sprintf(pLOCALDateTime, "%02d%02d-%02d-%02dT%02d:%02d:%02d",stGMTDateTime.bCentury, stGMTDateTime.bYear, stGMTDateTime.bMonth, stGMTDateTime.bDay, stGMTDateTime.bHour, stGMTDateTime.bMinutes, stGMTDateTime.bSeconds);

    return(TRUE);
}

/************************************************************************
DESCRIPTION :
     Takes the unformatted string and pads space chars either before or after out to
     the width specified.  The function is used because the euro chars >128 don't process
     in the sprintf %s formatter.

     IMPORTANT:  Formedstr must point to a buffer area AT LEAST formedWidth+1 in size.

INPUT PARAMS :
     formedStr:    output string padded per format info
     unformedStr:   input string to be formatted
     formedWidth:   final string width
     placement:     BEFORE or AFTER

RETURN VALUE :
     TRUE if SUCCESS
     FALSE if unformedStr string bigger than formedWidth
************************************************************************/

BOOL fnPadString(char *formedStr, char * unformedStr, int formedWidth, int placement)
{
    int     tmpFieldSize = strlen(unformedStr);
    int     tmpUpperLimit = formedWidth - tmpFieldSize;
    BOOL    stat=TRUE;

    *formedStr = '\0';
    if  (tmpUpperLimit >= 0)
        if (placement == BEFORE)
        {
            tmpFieldSize = 0;
            while (tmpFieldSize++ < tmpUpperLimit)
                strcat(formedStr, " ");
            strcat(formedStr, unformedStr);
        }
        else
        {
            strcpy(formedStr, unformedStr);
            while (tmpFieldSize++ <= formedWidth)
                strcat(formedStr, " ");
        }
    else
        stat = FALSE;
    return(stat);
}

BOOL fnPadStringWithFormat(char *formedStr, char * unformedStr, int formedWidth, int placement, UINT16 len)
{
    int     tmpFieldSize = len;
    int     tmpUpperLimit = formedWidth - tmpFieldSize;
    BOOL    stat=TRUE;

    *formedStr = '\0';
    if  (tmpUpperLimit >= 0)
        if (placement == BEFORE)
        {
            tmpFieldSize = 0;
            while (tmpFieldSize++ < tmpUpperLimit)
                strcat(formedStr, " ");
            strcat(formedStr, unformedStr);
        }
        else
        {
            strcpy(formedStr, unformedStr);
            while (tmpFieldSize++ <= formedWidth)
                strcat(formedStr, " ");
        }
    else
        stat = FALSE;
    return(stat);
}


