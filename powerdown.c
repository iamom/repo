/************************************************************************
  PROJECT:        Horizon CSD
  MODULE NAME:    powerdown.c

  DESCRIPTION:    implement power down related functionality

 ----------------------------------------------------------------------
               Copyright (c) 2017 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------

*************************************************************************/

#include "nucleus.h"
#include "networking/nu_networking.h"

#include "pbos.h"
#include "powerdown.h"

/* ------------------------------------------------------------------------------------------------[PowerDown_Task]-- */
VOID PowerDown_Task(UNSIGNED argc, VOID *argv)
{
    STATUS  status;
    NU_MEMORY_POOL* mem_pool;
    unsigned long   lwCurrentEvents;
    char osstat;

    /* Reference unused parameters to avoid toolset warnings. */
    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);


    dbgTrace(DBG_LVL_INFO, "PowerDown Task started...\n");

    fnSysLogTaskStart("PowerDown_Task", PWRDOWNTASK);

    while(1)
    {
        osstat = OSReceiveEvents(PWR_DOWN_EVENT_GROUP, (EV_PWR_DOWN_SAVE_LOGS), 120000, OS_OR_CONSUME, &lwCurrentEvents);
        if (osstat == (char) SUCCESSFUL)
        {
            /* save logs to filesystem */
            dbgTrace(DBG_LVL_INFO, "PowerDown Task: saving logs");
            preShutdownHook() ;
            (void)OSSetEvents(PWR_DOWN_EVENT_GROUP, EV_PWR_DOWN_SAVE_LOGS_DONE, OS_OR);

        }
    }

}
