
#include <stdio.h>
#include "fram.h"
#include "errors.h"
#include "btldr_fram.h"


int32_t fram_read_block(void *buf, void* offset, int32_t size)
{
	int32_t result = 0;

	if(buf == 0 )
		return FRAM_COMMON_ERR_NULL_PTR;

	if(((uint32_t)offset < 0 ) || (((uint32_t)offset + size ) > (FRAM_SIZE )))
		return E_CB_OUT_OF_BOUNDS;

	memcpy(buf, (uint8_t*)(FRAM_START + (uint32_t)offset), size);

	return result;
}

int32_t fram_write_block(void* offset, void* buf, int32_t size)
{
	if((uint32_t)buf == 0 )
		return FRAM_COMMON_ERR_NULL_PTR;

	if(((uint32_t)offset < 0 ) || (((uint32_t)offset + size ) > (FRAM_SIZE )))
			return E_CB_OUT_OF_BOUNDS;

	memcpy((uint8_t*)(FRAM_START + (uint32_t)offset), buf,  size);

	return 0;
}

int32_t fram_write_16(void* offset, uint16_t value)
{
	if(((uint32_t)offset < 0 ) || (((uint32_t)offset + sizeof(value) ) > (FRAM_SIZE )))
			return E_CB_OUT_OF_BOUNDS;


	*(uint16_t*)(FRAM_START + (uint32_t)offset) = value;

	return 0;

}

int32_t fram_fill(void* offset, uint8_t fill, int32_t size)
{
	//get 2byte/4 byte boundary
	int32_t result = 0;

	if(((uint32_t)offset < 0 ) || (((uint32_t)offset + size ) > (FRAM_SIZE )))
		return E_CB_OUT_OF_BOUNDS;


	memset((uint8_t*)(FRAM_START + (uint32_t)offset), fill, size);

	return result;
}

extern int __end_bootldr_fram;  /* defined in linker script (csgnu_arm.csd_2_3.link_ram.ld) */
extern int _ld_region_end_fram;

void InitFRAM(void)
{
	void *start_addr = (void *)&__end_bootldr_fram;
	void *end_addr   = (void *)&_ld_region_end_fram;
    uint32_t fill_size = (end_addr - start_addr);
	fram_fill(start_addr, 0, fill_size);
}

/////////////////////////////

void* fram_sector_to_fram_addr(uint32_t sec_num)
{
	if (sec_num < FRAM_MAX_SECTORS)
		return ((void*)(FRAM_START + (sec_num * FRAM_SECTOR_SIZE)));

	return 0;
}


//uint32_t fram_sector_size_from_addr(uint32_t offset)
//{
//
//    return E_CB_SECTOR_SIZE;
//}


uint32_t fram_sector_size(uint32_t sec_num)
{

	return FRAM_SECTOR_SIZE;
}



