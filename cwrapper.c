
/************************************************************************
*   PROJECT:        Mega
*   COPYRIGHT:      2005, Pitney Bowes, Inc. 
*   AUTHOR:
*   MODULE: oicommon.c
*   
*   DESCRIPTION:   Wrapper function for CMOS variables
*
*   MODIFICATION HISTORY:
*       12/11/2008   Jingwei,Li   Added the following functions for WOW feature:
*                                             fnCMOSSetHybridMode(),
*                                             fnCMOSIsHybridModeRevertToWOW(),
*                                             fnCheckSBRAutoPostSet(),
*                                             fnGetSBRAutoPostSetting(),
*                                             fnCMOSSetWOWWeightLimit(),
*                                             fnCMOSGetWOWWeightLimit().
*       02/29/2008   Joey Cui    Added wrapper functions for CMOSLongDHOnTime
*       03/29/2006   Oscar Wang  Added wrapper functions for 
*							CmosDownloadCtrlBuf,
*							fCMOSShowRateActive,
*							fCMOSShowRateEffective,
*							fCMOSUserDenyRateActivePrompt,
*							fCMOSUserDenyRateEffectivePrompt.
*       09/08/2006   Oscar Wang  Added wrapper function for fCMOSPresetRatesUpdated
*       08/03/2006   Dicky Sun   Add and update the following functions for serial
*	                             port setting:
*                                     fnSetCMOSSerialConnection
*                                     fnIsCMOSAttachedScale
*                                     fnIsCMOSExternalAccounting
*		06/27/2006 	Oscar Wang 		Added wrapper functions for auto clear.
*       08/04/2005 	John Gao  		Initial ersion
*
*************************************************************************/

#include   "cwrapper.h"
#include   "datdict.h"


extern CMOS_Signature CMOSSignature;
extern FeatureTbl     CMOSFeatureTable;
extern SetupParams    CMOSSetupParams;
extern Diagnostics    CMOSDiagnostics;
extern ZWZP_DATA      CMOSZwzpParams;
extern EKP_TASK_NUMBER_DATA    CMOSEKPTaskNumberData;
extern UINT8          fCMOSDiffWAutoTapeEnable;
extern UINT8          fCMOSScaleLocationCodeInited;
extern UINT8          bCMOSScaleLocationCode;
extern UINT8          fCMOSScaleLocallyCalibrated;
extern NETCONFIG      CMOSNetworkConfig;
extern FILES_TO_DOWNLOAD CMOSFilesToDownload;
extern ErrorLog                CMOSErrorLog;
extern CMOSACTIVEVERSIONDATA        CMOSActiveVersionData;
extern SystemErrorLog              CMOSSystemErrorLog;
extern PrintParams          CMOSPrintParams;
extern TEXT_ENTRY_ARRAY     CMOSTextEntryArray;
extern MoneyParams          CMOSMoneyParams;
extern SupervisorInfo		CMOSSupervisorInfo;

extern BOOL 			fCMOSMultiTapes;
extern BOOL            fCMOSPresetRatesUpdated;

extern CMOSDownloadCtrlBuf CmosDownloadCtrlBuf;
extern UINT8 fCMOSShowRateActive;
extern UINT8 fCMOSShowRateEffective;
extern BOOL fCMOSUserDenyRateActivePrompt;
extern BOOL fCMOSUserDenyRateEffectivePrompt;
extern UINT32 CMOSLongDHOnTime;
extern UINT8    pCMOSWOWWeightLimit[] ;  
extern SBR_Control  sbrErrorScreen; 
extern BOOL      fCMOSHybridModeRevertToWOW;

/* *************************************************************************
// FUNCTION NAME: 
//      fnCMOSSetupGetCMOSSignature
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct CMOSSignature.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of CMOSSignature
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      8/4/2005 John Gao Initial version
//      
// *************************************************************************/
CMOS_Signature *fnCMOSSetupGetCMOSSignature(void)
{
    return   &CMOSSignature;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnCMOSSetupGetFeatureTable
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct CMOSFeatureTable.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of CMOSFeatureTable
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      8/4/2005 John Gao Initial version
//      
// *************************************************************************/
FeatureTbl *fnCMOSSetupGetFeatureTable(void)
{
    return &CMOSFeatureTable;
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnCMOSSetupGetCMOSSetupParams
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct CMOSSetupParams.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of CMOSSetupParams
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      8/4/2005 John Gao Initial version
//      
// *************************************************************************/
SetupParams *fnCMOSSetupGetCMOSSetupParams(void)
{
    return &CMOSSetupParams;
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnCMOSSetupGetCMOSDiagnostics
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct CMOSDiagnostics.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of CMOSDiagnostics
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      8/10/2005 Kan Jiang Initial version
//      
// *************************************************************************/
Diagnostics  *fnCMOSSetupGetCMOSDiagnostics(void)                  
{
      return &CMOSDiagnostics;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnCMOSGetZwzpParams
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct CMOSZwzpParams.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of CMOSZwzpParams
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      12/07/2005 Adam Liu Initial version
//      
// *************************************************************************/
ZWZP_DATA  *fnCMOSGetZwzpParams(void)                  
{
      return &CMOSZwzpParams;
}


/**************************************************************************
// FUNCTION NAME: 
//          fnGetCMOSDiffWAutoTapeEnable
//
// DESCRIPTION:
//          Get the value of the variable fCMOSDiffWAutoTapeEnable
//
// INPUTS:
//          None
//
// RETURNS:
//          The value of fCMOSDiffWAutoTapeEnable
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//            Kan Jiang      Initial version
//*************************************************************************/
UINT8  fnGetCMOSDiffWAutoTapeEnable(void)
{
      return fCMOSDiffWAutoTapeEnable;
}



/**************************************************************************
// FUNCTION NAME: 
//           fnSetCMOSDiffWAutoTapeEnable
//
// DESCRIPTION:
//           Set the value of the variable fCMOSDiffWAutoTapeEnable
//
// INPUTS:
//           The new value of fCMOSDiffWAutoTapeEnable
//
// RETURNS:
//           None
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//           Kan Jiang      Initial version
//*************************************************************************/
void    fnSetCMOSDiffWAutoTapeEnable(UINT8  ucTapeEnable)
{  
      fCMOSDiffWAutoTapeEnable = ucTapeEnable;
}



/**************************************************************************
// FUNCTION NAME: 
//          fnGetCMOSScaleLocationCodeInited
//
// DESCRIPTION:
//          Get the value of the variable fCMOSScaleLocationCodeInited
//
// INPUTS:
//          None
//
// RETURNS:
//          The value of fCMOSScaleLocationCodeInited
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//              Kan Jiang      Initial version
//*************************************************************************/
UINT8  fnGetCMOSScaleLocationCodeInited(void)
{
      return fCMOSScaleLocationCodeInited;
}



/**************************************************************************
// FUNCTION NAME: 
//          fnSetCMOSScaleLocationCodeInited
//
// DESCRIPTION:
//          Set the value of the variable fCMOSScaleLocationCodeInited
//
// INPUTS:
//          The new value of fCMOSScaleLocationCodeInited
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//          Kan Jiang      Initial version
//*************************************************************************/
void    fnSetCMOSScaleLocationCodeInited(UINT8  ucCodeInited)
{  
      fCMOSScaleLocationCodeInited = ucCodeInited;
}



/**************************************************************************
// FUNCTION NAME: 
//          fnGetCMOSScaleLocationCode
//
// DESCRIPTION:
//          Get the value of the variable bCMOSScaleLocationCode
//
// INPUTS:
//          None
//
// RETURNS:
//          The value of bCMOSScaleLocationCode
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//          Kan Jiang      Initial version
//*************************************************************************/
UINT8  fnGetCMOSScaleLocationCode(void)
{
      return bCMOSScaleLocationCode;
}



/**************************************************************************
// FUNCTION NAME: 
//          fnSetCMOSScaleLocationCode
//
// DESCRIPTION:
//          Set the value of the variable bCMOSScaleLocationCode
//
// INPUTS:
//          The new value of bCMOSScaleLocationCode
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//              Kan Jiang      Initial version
//*************************************************************************/
void    fnSetCMOSScaleLocationCode(UINT8  ucLocationCode)
{  
      bCMOSScaleLocationCode = ucLocationCode;
}



/**************************************************************************
// FUNCTION NAME: 
//          fnGetCMOSScaleVibrationSetting
//
// DESCRIPTION:
//          Get the value of the variable fCMOSScaleLocallyCalibrated
//
// INPUTS:
//          None
//
// RETURNS:
//          The value of fCMOSScaleLocallyCalibrated
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//          Kan Jiang      Initial version
//*************************************************************************/
UINT8  fnGetCMOSScaleVibrationSetting(void)
{
      return fCMOSScaleLocallyCalibrated;
}



/**************************************************************************
// FUNCTION NAME: 
//          fnSetCMOSScaleVibrationSetting
//
// DESCRIPTION:
//          Set the value of the variable fCMOSScaleLocallyCalibrated
//
// INPUTS:
//          The new value of fCMOSScaleLocallyCalibrated
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//           Kan Jiang      Initial version
//*************************************************************************/
void    fnSetCMOSScaleVibrationSetting(UINT8  ucVibrationSetting)
{  
      fCMOSScaleLocallyCalibrated = ucVibrationSetting;
}








/* *************************************************************************
// FUNCTION NAME: 
//      fnCMOSGetNetworkConfig
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct CMOSNetworkConfig.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of CMOSNetworkConfig
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      8/14/2005 John Gao Initial version
//      
// *************************************************************************/
NETCONFIG *fnCMOSGetNetworkConfig(void)
{
    return &CMOSNetworkConfig;
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnCMOSGetErrorLog
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct CMOSErrorLog.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of CMOSErrorLog
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      8/14/2005 John Gao Initial version
//      
// *************************************************************************/
ErrorLog *fnCMOSGetErrorLog(void)
{
     return(&CMOSErrorLog);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnCMOSGetErrorLog
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct CMOSFilesToDownload.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of CMOSFilesToDownload
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      8/14/2005 John Gao Initial version
//      
// *************************************************************************/
FILES_TO_DOWNLOAD *fnCMOSGetFilesToDownload(void)
{
     return(&CMOSFilesToDownload);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnCMOSGetCMOSActiveVersionData
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct CMOSActiveVersionData.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of CMOSActiveVersionData
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      8/14/2005 John Gao Initial version
//      
// *************************************************************************/
CMOSACTIVEVERSIONDATA *fnCMOSGetCMOSActiveVersionData(void)
{
    return(&CMOSActiveVersionData);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnCMOSGetCMOSSystemErrorLog
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct CMOSSystemErrorLog.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of CMOSSystemErrorLog
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      9/13/2005 John Gao Initial version
//      
// *************************************************************************/
SystemErrorLog *fnCMOSGetCMOSSystemErrorLog(void)
{
    return(&CMOSSystemErrorLog);
}







/* *************************************************************************
// FUNCTION NAME: 
//      fnCMOSGetCMOSPrintParams
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct CMOSPrintParams.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of CMOSPrintParams
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      9/13/2005 John Gao Initial version
//      
// *************************************************************************/
PrintParams *fnCMOSGetCMOSPrintParams(void)
{
    return(&CMOSPrintParams);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnSetCMOSMultiTapes
//
// DESCRIPTION: 
//		Wrapper function that set the Tape Printing	configuration
//
// INPUTS:      
//		fMultiTapes - TRUE, multiple tapes print
//					  FALSE, one tape print
//
// RETURNS:
//		None.
//
// NOTES: 
//		None
//
// MODIFICATION HISTORY:
//	11/30/2005	Vincent Yi		Initial version
//		
// *************************************************************************/
void fnSetCMOSMultiTapes (BOOL fMultiTapes)
{
	fCMOSMultiTapes = fMultiTapes;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnIsCMOSMultiTapes
//
// DESCRIPTION: 
//		Wrapper function that get the Tape Printing	configuration
//
// INPUTS:      
//		None.
//
// RETURNS:
//		TRUE, multiple tapes print
//		FALSE, one tape print
//
// NOTES: 
//		None
//
// MODIFICATION HISTORY:
//	11/30/2005	Vincent Yi		Initial version
//		
// *************************************************************************/
BOOL fnIsCMOSMultiTapes(void)
{
	return fCMOSMultiTapes;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnSetCMOSSerialConnection
//
// DESCRIPTION: 
//		Wrapper function that set the Serial Connection	configuration
//
// INPUTS:      
//		ucSerialCon - SERIAL_PLATFORM, Serial platform
//					  SERIAL_EDM, External accounting
//
// RETURNS:
//		None.
//
// NOTES: 
//		None
//
// MODIFICATION HISTORY:
//	08/03/2006	Dicky Sun		Initial version
//		
// *************************************************************************/
void fnSetCMOSSerialConnection (UINT8 ucSerialCon)
{
   CMOSSetupParams.SerialConnection = ucSerialCon;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnIsCMOSAttachedScale
//
// DESCRIPTION: 
//		Wrapper function that get the attached scale configuration
//
// INPUTS:      
//		None.
//
// RETURNS:
//		TRUE, Attached Scale
//		FALSE, No scale
//
// NOTES: 
//		None
//
// MODIFICATION HISTORY:
//	08/03/2006  Dicky Sun		Initial version
//		
// *************************************************************************/
BOOL fnIsCMOSAttachedScale(void)
{
	BOOL fRet = FALSE;
	if(CMOSSetupParams.SerialConnection == SERIAL_PLATFORM)
	{
	    fRet = TRUE;
	}
	 return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnIsCMOSExternalAccounting
//
// DESCRIPTION: 
//		Wrapper function that get the external accounting configuration
//
// INPUTS:      
//		None.
//
// RETURNS:
//		TRUE, External accounting
//		FALSE, No external accounting
//
// NOTES: 
//		None
//
// MODIFICATION HISTORY:
//	08/03/2006  Dicky Sun		Initial version
//		
// *************************************************************************/
BOOL fnIsCMOSExternalAccounting(void)
{
	BOOL fRet = FALSE;
	if(CMOSSetupParams.SerialConnection == SERIAL_EDM)
	{
	    fRet = TRUE;
	}
	 return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetCMOSTextEntryArray
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct CMOSTextEntryArray.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of CMOSTextEntryArray
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      5/18/2006 James Wang Initial version
//      
// *************************************************************************/
TEXT_ENTRY_ARRAY *fnGetCMOSTextEntryArray(void)
{
    return(&CMOSTextEntryArray);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCMOSSetupGetCMOSMoneyParm
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct CMOSMoneyParams.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of CMOSMoneyParams
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//  5/22/2006   James Wang      Initial version
//      
// *************************************************************************/
MoneyParams *fnCMOSSetupGetCMOSMoneyParm(void)
{
    return(&CMOSMoneyParams);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetCMOSSupervisorInfo
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct CMOSSupervisorInfo.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of CMOSSupervisorInfo
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//  5/22/2006   James Wang      Initial version
//      
// *************************************************************************/
SupervisorInfo *fnGetCMOSSupervisorInfo(void)
{
    return(&CMOSSupervisorInfo);
}

/* *************************************************************************
// FUNCTION NAME: fnIsAutoScaleModeEnabled
//
// DESCRIPTION: 
//		check if the auto scale mode enabled
// INPUTS:
//      None.
//
// RETURNS:
//      TRUE or FALSE
//
// WARNINGS/NOTES:
//  		
// MODIFICATION HISTORY:
//      05/18/2006    John Gao   Initial version
// *************************************************************************/
BOOL fnIsAutoScaleModeEnabled(void)
{
	BOOL fStatus;

	if (fnfGetCMOSByteParam(CMOS_BP_AUTO_SCALE_MODE_ENABLED) != 0)
	{
		fStatus = TRUE;
	}
	else
	{
		fStatus = FALSE;
	}

	return (fStatus);
}

/* *************************************************************************
// FUNCTION NAME: fnCMOSGetNewPieceACClassOption
//
// DESCRIPTION: 
//		Check if Class If New Piece is set to clear
// INPUTS:
//      None.
//
// RETURNS:
//      CLEAR_RATING_DATA/RETAIN_RATING_DATA
//
// WARNINGS/NOTES:
//  		
// MODIFICATION HISTORY:
//      06/05/2006    Oscar Wang 	Initial version
// *************************************************************************/
UINT8 fnCMOSGetNewPieceACClassOption(void)
{
	return CMOSSetupParams.bRatingClassIfNewPiece;
}

/* *************************************************************************
// FUNCTION NAME: fnCMOSGetNewPieceACDestOption
//
// DESCRIPTION: 
//		Check if Dest If New Piece is set to clear
// INPUTS:
//      None.
//
// RETURNS:
//      CLEAR_RATING_DATA/RETAIN_RATING_DATA
//
// WARNINGS/NOTES:
//  		
// MODIFICATION HISTORY:
//      06/05/2006    Oscar Wang 	Initial version
// *************************************************************************/
UINT8 fnCMOSGetNewPieceACDestOption(void)
{
	return CMOSSetupParams.bRatingDestinationIfNewPiece;
}

/* *************************************************************************
// FUNCTION NAME: fnCMOSGetNewClassACDestOption
//
// DESCRIPTION: 
//		Check if Dest If New Class is set to clear
// INPUTS:
//      None.
//
// RETURNS:
//      CLEAR_RATING_DATA/RETAIN_RATING_DATA
//
// WARNINGS/NOTES:
//  		
// MODIFICATION HISTORY:
//      06/05/2006    Oscar Wang 	Initial version
// *************************************************************************/
UINT8 fnCMOSGetNewClassACDestOption(void)
{
	return CMOSSetupParams.bRatingDestinationIfNewClass;
}

/* *************************************************************************
// FUNCTION NAME: fnSetCMOSClassIfNewPiece
//
// DESCRIPTION: 
//		set parameter bRatingClassIfNewPiece with input value
// 
// INPUTS:
//      CLEAR_RATING_DATA/RETAIN_RATING_DATA
//
// RETURNS:
//	   None.
//
// WARNINGS/NOTES:
//  		
// MODIFICATION HISTORY:
//      06/05/2006    Oscar Wang 	Initial version
// *************************************************************************/
void fnSetCMOSClassIfNewPiece(BOOL fIsRetain)
{
	CMOSSetupParams.bRatingClassIfNewPiece = fIsRetain;
}

/* *************************************************************************
// FUNCTION NAME: fnSetCMOSDestIfNewPiece
//
// DESCRIPTION: 
//		set parameter bRatingDestinationIfNewPiece with input value
//
// INPUTS:
//      CLEAR_RATING_DATA/RETAIN_RATING_DATA
//
// RETURNS:
//	   None.
//
// WARNINGS/NOTES:
//  		
// MODIFICATION HISTORY:
//      06/05/2006    Oscar Wang 	Initial version
// *************************************************************************/
void fnSetCMOSDestIfNewPiece(BOOL fIsRetain)
{
	CMOSSetupParams.bRatingDestinationIfNewPiece = fIsRetain;
}

/* *************************************************************************
// FUNCTION NAME: fnSetCMOSDestIfNewClass
//
// DESCRIPTION: 
//		set parameter bRatingDestinationIfNewClass with input value
//
// INPUTS:
//      CLEAR_RATING_DATA/RETAIN_RATING_DATA
//
// RETURNS:
//	   None.
//
// WARNINGS/NOTES:
//  		
// MODIFICATION HISTORY:
//      06/05/2006    Oscar Wang 	Initial version
// *************************************************************************/
void fnSetCMOSDestIfNewClass(BOOL fIsRetain)
{
	CMOSSetupParams.bRatingDestinationIfNewClass = fIsRetain;
}

/**************************************************************************
// FUNCTION NAME: 
//          fnGetCMOSNewRateEffective
//
// DESCRIPTION:
//          Get the value of the variable fCMOSPresetRatesUpdated
//
// INPUTS:
//          None
//
// RETURNS:
//          The value of fCMOSPresetRatesUpdated
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
// 09/08/2006    Oscar Wang      Initial version
//*************************************************************************/
BOOL  fnGetCMOSNewRateEffective(void)
{
      return ( fCMOSPresetRatesUpdated);
}

/**************************************************************************
// FUNCTION NAME: 
//          fnSetCMOSNewRateEffective
//
// DESCRIPTION:
//          Set the value of the variable fCMOSPresetRatesUpdated
//
// INPUTS:
//          The new value of the variable
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
// 09/08/2006    Oscar Wang      Initial version
//*************************************************************************/
void   fnSetCMOSNewRateEffective(BOOL  fNewStatus)
{
      fCMOSPresetRatesUpdated = fNewStatus;
}


/**************************************************************************
// FUNCTION NAME: 
//          fnGetCMOSDownloadCtrlBuf
//
// DESCRIPTION:
//          Get structure of CmosDownloadCtrlBuf
//
// INPUTS:
//          None
//
// RETURNS:
//          The address of CmosDownloadCtrlBuf
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
// 03/29/2007    Oscar Wang      Initial version
//*************************************************************************/
CMOSDownloadCtrlBuf  *fnGetCMOSDownloadCtrlBuf(void)
{
      return ( &CmosDownloadCtrlBuf);
}


/**************************************************************************
// FUNCTION NAME: 
//          fnGetCMOSShowRateActive
//
// DESCRIPTION:
//          Get the value of the variable fCMOSShowRateActive
//
// INPUTS:
//          None
//
// RETURNS:
//          The value of fCMOSShowRateActive
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
// 03/29/2007    Oscar Wang      Initial version
//*************************************************************************/
UINT8  fnGetCMOSShowRateActive(void)
{
      return  fCMOSShowRateActive;
}

/**************************************************************************
// FUNCTION NAME: 
//          fnSetCMOSShowRateActive
//
// DESCRIPTION:
//          Set the value of the variable fCMOSShowRateActive
//
// INPUTS:
//          The new value of the variable
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
// 09/08/2006    Oscar Wang      Initial version
//*************************************************************************/
void   fnSetCMOSShowRateActive(UINT8  ucValue)
{
      fCMOSShowRateActive = ucValue;
}

/**************************************************************************
// FUNCTION NAME: 
//          fnCMOSShowRateEffective
//
// DESCRIPTION:
//          Get the value of the variable fCMOSShowRateEffective
//
// INPUTS:
//          None
//
// RETURNS:
//          The value of fCMOSShowRateEffective
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
// 03/29/2007    Oscar Wang      Initial version
//*************************************************************************/
UINT8  fnCMOSShowRateEffective(void)
{
      return  fCMOSShowRateEffective;
}

/**************************************************************************
// FUNCTION NAME: 
//          fnSetCMOSShowRateEffective
//
// DESCRIPTION:
//          Set the value of the variable fCMOSShowRateEffective
//
// INPUTS:
//          The new value of the variable
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
// 09/08/2006    Oscar Wang      Initial version
//*************************************************************************/
void   fnSetCMOSShowRateEffective(UINT8  ucValue)
{
      fCMOSShowRateEffective = ucValue;
}

/**************************************************************************
// FUNCTION NAME: 
//          fnGetCMOSUserDenyRateActivePrompt
//
// DESCRIPTION:
//          Get the value of the variable fCMOSUserDenyRateActivePrompt
//
// INPUTS:
//          None
//
// RETURNS:
//          The value of fCMOSUserDenyRateActivePrompt
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
// 03/29/2007    Oscar Wang      Initial version
//*************************************************************************/
BOOL  fnGetCMOSUserDenyRateActivePrompt(void)
{
      return  fCMOSUserDenyRateActivePrompt;
}

/**************************************************************************
// FUNCTION NAME: 
//          fnSetCMOSShowRateActive
//
// DESCRIPTION:
//          Set the value of the variable fCMOSShowRateActive
//
// INPUTS:
//          The new value of the variable
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
// 09/08/2006    Oscar Wang      Initial version
//*************************************************************************/
void   fnSetCMOSUserDenyRateActivePrompt(BOOL  fValue)
{
      fCMOSUserDenyRateActivePrompt = fValue;
}

/**************************************************************************
// FUNCTION NAME: 
//          fnGetCMOSUserDenyRateEffectivePrompt
//
// DESCRIPTION:
//          Get the value of the variable fCMOSUserDenyRateEffectivePrompt
//
// INPUTS:
//          None
//
// RETURNS:
//          The value of fCMOSUserDenyRateEffectivePrompt
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
// 03/29/2007    Oscar Wang      Initial version
//*************************************************************************/
BOOL  fnGetCMOSUserDenyRateEffectivePrompt(void)
{
      return  fCMOSUserDenyRateEffectivePrompt;
}

/**************************************************************************
// FUNCTION NAME: 
//          fnSetCMOSUserDenyRateEffectivePrompt
//
// DESCRIPTION:
//          Set the value of the variable fCMOSUserDenyRateEffectivePrompt
//
// INPUTS:
//          The new value of the variable
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
// 09/08/2006    Oscar Wang      Initial version
//*************************************************************************/
void   fnSetCMOSUserDenyRateEffectivePrompt(BOOL  fValue)
{
      fCMOSUserDenyRateEffectivePrompt = fValue;
}

/**************************************************************************
// FUNCTION NAME: 
//          fnGetCMOSLongDHOnTime
//
// DESCRIPTION:
//          Get the value of the variable CMOSLongDHOnTime
//
// INPUTS:
//          None
//
// RETURNS:
//          The value of CMOSLongDHOnTime
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
// 02/29/2008    Joey Cui      Initial version
//*************************************************************************/
UINT32  fnGetCMOSLongDHOnTime(void)
{
      return  CMOSLongDHOnTime;
}

/**************************************************************************
// FUNCTION NAME: 
//          fnSetCMOSLongDHOnTime
//
// DESCRIPTION:
//          Set the value of the variable CMOSLongDHOnTime
//
// INPUTS:
//          The new value of the variable
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
// 02/29/2008    Joey Cui      Initial version
//*************************************************************************/
void   fnSetCMOSLongDHOnTime(UINT32  ulValue)
{
      CMOSLongDHOnTime = ulValue;
}
/* *************************************************************************
// FUNCTION NAME:   fnCMOSGetWOWWeightLimit
// DESCRIPTION:     returns current value of WOW Weight Limit from CMOS variable
// AUTHOR:          cristeb
// INPUTS:          None
// RETURNS:         pointer to (const) array of unsigned char containing WOW weight
//                  limit in rating weight format (4 bytes packed BCD)
//  MODIFICATION HISTORY:
//  11-18-08    Jingwei,Li     Initial version.  
// *************************************************************************/
const UINT8 *fnCMOSGetWOWWeightLimit( void )
{
    return (const UINT8 * ) pCMOSWOWWeightLimit ;
}
 /*end fnRateGetCMOSWOWWeightLimit*/


/* *************************************************************************
// FUNCTION NAME:   fnCMOSSetWOWWeightLimit
// DESCRIPTION:     updates value of WOW Weight Limit variable in CMOS with value
//                  passed by caller if valid
// AUTHOR:          cristeb
// INPUTS:          pNewWOWWeightLimit, pointer to array of unsigned char containing
//                  WOW weight limit in rating weight format (4 bytes packed BCD)
// RETURNS:         Nothing
// NOTES:
// 1)   Weight argument assumed to be valid. Usage rules:
//      - weight validated when User enters new value via UI
//        (see fnValidateWOWWeightLimit() )
//      - initialization values, internally defined, assumed to be accurate
//  MODIFICATION HISTORY:
//  11-18-08    Jingwei,Li     Initial version.
// *************************************************************************/
void fnCMOSSetWOWWeightLimit( UINT8 *pNewWOWWeightLimit )
{
    (void) memcpy( (void *) pCMOSWOWWeightLimit, (const void *) pNewWOWWeightLimit, (size_t) LENWEIGHT ) ;
}
 /* *************************************************************************
 // FUNCTION NAME: fnGetSBRAutoPostSetting
 //
 // DESCRIPTION:
 //      Get the current CMOS variable value of sbrCurrentSetting
 //
 // INPUTS:
 //
 // RETURNS: TRUE
 //          FALSE
 //
 // NOTES:
 //
 // MODIFICATION HISTORY:
 //      11/21/2008  Jingwei,Li   Initial version
 //
 // *************************************************************************/
 char fnGetSBRAutoPostSetting(void)
 {
       return sbrErrorScreen.ch_BpDef;
 }
 
 /* *************************************************************************
 // FUNCTION NAME: fnSetSBRAutoPostSetting
 //
 // DESCRIPTION:
 //      Set the CMOS variable value of sbrCurrentSetting
 //
 // INPUTS:
 //
 // RETURNS: TRUE
 //                 FALSE
 //
 // NOTES:
 //
 // MODIFICATION HISTORY:
 //      11/21/2008  Jingwei,Li   Initial version
 //
 // *************************************************************************/
 void fnSetSBRAutoPostSetting(char bVal)
 {
       sbrErrorScreen.ch_BpDef= bVal;
 }
/* *************************************************************************
// FUNCTION NAME: fnCheckSBRAutoPostSet
//
// DESCRIPTION:  Wrapper used to tell if the AutoPost functionality is turned on
//      Note: This flag cannot be set unless BP_SBR_AUTOPOST_LG_ENVELOPES
//            is enabled in the EMD
// INPUTS:
//
// RETURNS:
//
// NOTES:
//
// MODIFICATION HISTORY:
//      11/21/2008  Jingwei,Li   Initial version
// *************************************************************************/
BOOL fnCheckSBRAutoPostSet( void )
{
    BOOL fRet = FALSE;
    
    if( sbrErrorScreen.ch_BpDef == 1 )
    {
        fRet = TRUE ;
    }
    else
    {
        fRet = FALSE ;
    }

    return fRet;
}
 /* *************************************************************************
 // FUNCTION NAME: 
 //      fnCMOSIsHybridModeRevertToWOW
 //
 // DESCRIPTION: 
 //      Utility function that get the value of fCMOSHybridModeRevertToWOW.
 //
 // INPUTS:      
 //      None.
 //
 // RETURNS:
 //      The value of fCMOSHybridModeRevertToWOW
 //
 //   NOTES: 
 //      None
 //
 // MODIFICATION HISTORY:
 //      11/25/2008 Jingwei,Li  Initial version.
 //      
 // *************************************************************************/
 BOOL fnCMOSIsHybridModeRevertToWOW(void)
 {
     return fCMOSHybridModeRevertToWOW;
 }

 /* *************************************************************************
 // FUNCTION NAME: 
 //      fnCMOSSetHybridMode
 //
 // DESCRIPTION: 
 //      Utility function that set the value of fCMOSHybridModeRevertToWOW.
 //
 // INPUTS:      
 //      bVal 
 //
 // RETURNS:
 //      None.
 //
 //   NOTES: 
 //      None
 //
 // MODIFICATION HISTORY:
 //      11/25/2008 Jingwei,Li  Initial version.
 //      
 // *************************************************************************/
 void fnCMOSSetHybridMode(BOOL bVal)
 {
     fCMOSHybridModeRevertToWOW = bVal;
 }

 /**************************************************************************
 // FUNCTION NAME:
 //          fnGetNVRAMOOBInkInstalled
 //
 // DESCRIPTION:
 //          Get the value of the variable fOOBInkInstalled
 //
 // *************************************************************************/
 UINT32  fnGetNVRAMOOBInkInstalled(void)
 {
       return (UINT32) CMOSDiagnostics.fOOBInkInstalled;
 }

 /**************************************************************************
 // FUNCTION NAME:
 //          fnSetNVRAMOOBInkInstalled
 //
 // DESCRIPTION:
 //          Set the value of the variable fOOBInkInstalled
 //
 // *************************************************************************/
 void fnSetNVRAMOOBInkInstalled(BOOL bVal)
 {
	 CMOSDiagnostics.fOOBInkInstalled = bVal;
 }

 /**************************************************************************
 // FUNCTION NAME:
 //          fnGetBeforePrintPurgeStrong
 //
 // DESCRIPTION:
 //          Get the value of the variable bBeforePrintPurgeStrong
 //
 // *************************************************************************/
 UINT8  fnGetBeforePrintPurgeStrong(void)
 {
       return CMOSPrintParams.bBeforePrintPurgeStrong;
 }

 /**************************************************************************
 // FUNCTION NAME:
 //          fnSetBeforePrintPurgeStrong
 //
 // DESCRIPTION:
 //          Set the value of the variable bBeforePrintPurgeStrong
 //
 // *************************************************************************/
 void fnSetBeforePrintPurgeStrong(UINT8 bVal)
 {
	 CMOSPrintParams.bBeforePrintPurgeStrong = bVal;
 }

 /**************************************************************************
 // DESCRIPTION:
 //          Get the value of the distributor URL source
 //
 // *************************************************************************/
 DIST_URL_SOURCE  fnGetNVRAMDistributorURLSource(void)
 {
       return (DIST_URL_SOURCE) CMOSNetworkConfig.distributorUrlSource;
 }

 /**************************************************************************
 // DESCRIPTION:
 //          Set the value of the distributor URL source
 //
 // *************************************************************************/
 void fnSetNVRAMDistributorURLSource(DIST_URL_SOURCE bVal)
 {
	 CMOSNetworkConfig.distributorUrlSource = (UINT8) bVal;
 }

 /**************************************************************************
 // DESCRIPTION:
 //          Get the value of the distributor URL type
 //
 // *************************************************************************/
 DIST_URL_TYPE  fnGetNVRAMdistributorEMDURLType(void)
 {
       return (DIST_URL_TYPE) CMOSNetworkConfig.distributorEMDUrlType;
 }

 /**************************************************************************
 // DESCRIPTION:
 //          Set the value of the variable distributor URL type
 //
 // *************************************************************************/
 void fnSetNVRAMdistributorEMDURLType(DIST_URL_TYPE bVal)
 {
	 CMOSNetworkConfig.distributorEMDUrlType = (UINT8) bVal;
 }

 /**************************************************************************
 // DESCRIPTION:
 //          Get the value of the distributor URL
 //
 // *************************************************************************/
char *fnGetNVRAMdistributorURL(void)
 {
       return CMOSNetworkConfig.distributorUrl;
 }

 /**************************************************************************
 // DESCRIPTION:
 //          Set the value of the distributor URL
 //
 // *************************************************************************/
 void fnSetNVRAMdistributorURL(char *bVal)
 {
     (void)strncpy(CMOSNetworkConfig.distributorUrl, bVal, sizeof(CMOSNetworkConfig.distributorUrl));
 }



