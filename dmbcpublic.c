/*************************************************************************
*   PROJECT:        Janus
*   COMPANY:        Pitney Bowes
*   AUTHOR :        Sandra Peterson
*   MODULE NAME:    $Workfile:   DmBcPublic.c  $
*   REVISION:       $Revision:   1.6  $
*       
*   DESCRIPTION:    Barcode generation public functions
*
* -----------------------------------------------------------------------
*               Copyright (c) 2003 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* -----------------------------------------------------------------------
*
*   REVISION HISTORY:
*
* 04-Jan-08 sa002pe on Janus tips shelton branch
*	Changed FinishGenDMBarcode to return an error code.
*
* 26-Oct-07 sa002pe on janus tips shelton branch
*	Added SetNumDataBytes function.
*
* 30-Apr-07 sa002pe on FPHX 01.04 shelton branch
*	Merged in the 13-Jul-06 changes from Janus tips.
*	Changed the function prototypes so pointers to sDmBarCode &
*	sDmBarCode.sBarcodeSymbols aren't passed between functions. They're now
*	represented by global pointers in dmbcprivate.c.
*	Moved stuff from StartGenDMBarcode to FinishGenDMBarcode so all the
*	actual barcode generation is done by FinishGenDMBarcode.
*
* 13-Jul-06 sa002pe on janus tips shelton branch
*	Changes for lint.
*	1. Made several things const in the parameter list for the functions.
*	2. Cleaned up several warnings & infos.
* 
*   $Log:   H:/group/SPARK/ARCHIVES/DM150/uic_app/DmBcPublic.c_v  $
 * 
 *    Rev 1.6   29 May 2003 18:37:56   SA002PE
 * Changed ERR_CODE to UINT32 so its clearer what we're dealing with.
 * Changed NO_ERROR to IG_NO_ERROR for consistency.
 * Changed to include ImageErrs.h instead of DmBcErrors.h.
 * Changed the call to DMGen per the new prototype.
 * 
 *    Rev 1.5   23 May 2003 17:13:42   SA002PE
 * Removed the timing variables.  They can't be used with the Janus emulator.
 * Added debug statements for timing analysis.
 * Changed FinishGenDMBarcode per its new prototype and the new
 * prototype for DMGen.
 * 
 *    Rev 1.4   21 May 2003 18:56:50   SA002PE
 * Changed variable names to be more consistent with
 * the naming conventions.
 * Removed "DOF" from the variable names and references
 * to DOF wherever possible.
 * Moved the Swap16 function to DmBcPrivate.c in case it's
 * needed.
 * Removed the big-endian vs. little-endian stuff that was
 * commented out.
 * 
 *    Rev 1.3   16 May 2003 21:58:20   SA002PE
 * Removed a lot of stuff that had already been commented out because it wasn't needed.
 * Changed StartGenDMBarcode for its new prototype and the new prototype for
 * SetECCParameters.
 * Changed SetupDMBarcode to comment out the byte swapping.  Isn't needed.
 * 
 *    Rev 1.2   13 May 2003 19:06:04   SA002PE
 * Changed FinishGenDMBarcode per the new prototype.
 * Changed the call to DMDOFGen per the new prototype.
 * 
 *    Rev 1.1   May 07 2003 16:51:36   CX17595
 * Sandra changed prototype for FinishGenDMBarcode (pwBitmap argument added).
 * 
 *    Rev 1.0   03 May 2003 00:36:46   SA002PE
 * Initial Version
* 
* 
*************************************************************************/

#include <stdio.h>

#include "DmBcPublic.h"		// must be first
#include "DmBcPrivate.h"
#include "ImageErrs.h"


/************************ FinishGenDMBarcode **************************/

unsigned long FinishGenDMBarcode(const UINT8 *pbPrintBuf, const UINT8 bRowBytes, const UINT8 *pbData)
{
    UINT32      lError = IG_NO_ERROR;   /* error results                         */

#ifdef IG_TIME_DEBUG
	char			pLogBuf[50];
#endif


    if (BarCodeAvailable())
    {
#ifdef IG_TIME_DEBUG
		sprintf(pLogBuf, "Start Data Word Generation");
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

	    /* Generate the Codewords from the data */
	    lError = GenDMCodeWord(pbData);

	    if (lError == IG_NO_ERROR)
	    {

#ifdef IG_TIME_DEBUG
			sprintf(pLogBuf, "Start ECC Word Generation");
			fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
			ECCEngine();
    
#ifdef IG_TIME_DEBUG
			sprintf(pLogBuf, "Place Barcode Words");
			fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

	        /* Place the data code words and then the ECC words */
	        PlaceDMDataWords();
	        PlaceDMECCWords();

#ifdef IG_TIME_DEBUG
			sprintf(pLogBuf, "Blow up Barcode");
			fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

	        /* Generate the instructions that will print the barcode */
	        DMGen(pbPrintBuf, bRowBytes);
	    }
    }
    else
        lError = NO_BARCODE_INFORMATION;

	return (lError);
}


/*************************** SetNumDataBytes ***************************/

void SetNumDataBytes(const unsigned short usBarcodeDataLen)
{
	ChangeNumDataBytes(usBarcodeDataLen);
}


/*************************** SetupDMBarcode ***************************/

UINT32 SetupDMBarcode(EMD_BARCODE *psData)
{
    UINT32      lError = IG_NO_ERROR;   /* error results                         */
    

    if (psData->bNumberSymbols == MAX_SYMBOLS)
    {
		/* assume a big-endian processor */

        /* We only support 1 symbol with this code, so by default there can */
        /* never be a symbol below this one. */
        psData->sBarcodeSymbols.fbSymbolBelow = FALSE;

        /* Setup the barcode structures using the data from the host */
        lError = InitializeBarCode(psData);
		if (lError == IG_NO_ERROR)
		{
			/* Generate the bitLookUpTable */
			GenerateBitLookUpTable();

			/* Initialize the raw bitmap */
			InitializeBitMap();
		}
    }
    else
        lError = TOO_MANY_BARCODE_SYMBOLS;

    return (lError);
}


/************************* StartGenDMBarcode **************************/

UINT32 StartGenDMBarcode()
{
    UINT32    lError = IG_NO_ERROR;   /* error status */

#ifdef IG_TIME_DEBUG
	char			pLogBuf[50];
#endif
    

#ifdef IG_TIME_DEBUG
	sprintf(pLogBuf, "Start Barcode Setup");
	fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

    if (BarCodeAvailable())
    {
        /* Initial the raw bitmaps for the Data Matrix code */
        InitializeBitMap();
    
        /* Setup to start ECC generation */
	    lError = SetECCParameters(MODE_DM);
    }
    else
        lError = NO_BARCODE_INFORMATION;
    
    return (lError);
}
