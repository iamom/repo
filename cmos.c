/*************************************************************************
*   PROJECT:        Horizon CSD2-3
*	MODULE NAME:	cmos.c
*
*	DESCRIPTION:	Global declarations for variables that live in non volatile RAM (FRAM)
*
* ----------------------------------------------------------------------
*               Copyright (c) 2017 Pitney Bowes Inc.
*                    37 Executive drive
*                   Danbury, Connecticut  06810
* ----------------------------------------------------------------------
*
*************************************************************************/

#pragma GCC diagnostic ignored "-Wmissing-braces"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-function"

#define NO_CMOS_EXTERNALS 1

// Removed the code that this definition excluded. So it isn't needed anymore.
//  This is a FPHX-specific file.
//#define JANUS_PREPRODUCTION 1    

//#define CMOS_INIT_NO_EMD        TRUE
//#define CMOS_INIT_WITH_EMD      FALSE

#include <stdio.h>
#include <string.h>
#include "sys/stat.h"
#include "posix.h"
#include "dirent.h"

#include "aiservic.h"
#include "apphdr.h"
#include "bob.h"
#include "bobutils.h"
#include "ccdapi.h"         // CCD_DONE_FLAGS_*
#include "chasing_shadows.h"
#include "cmos.h"
#include "datdict.h"
//#include "deptaccount.h"
#include "download.h"
#include "fdrmcptask.h"
#include "features.h"
#include "fnames.h"
#include "fwrapper.h"
#include "cwrapper.h"
#include "intellilink.h"
#include "lcd.h"            // fnLCDSetContrastValue()
#include "oi_intellilink.h"
#include "oidongle.h"
#include "oifpprint.h"
//#include "oioob.h"
#include "oipostag.h"
//#include "oipreset.h"
//#include "oiscanner.h"
#include "oiscreen.h"
#include "pbos.h"
#include "picklist.h"
#include "pmcinfo.h"
#include "RateAdpt.h"
//#include "rateservice.h"
//#include "ratesvcpublic.h"
#include "suprcode.h"       // fnCMOSInitLockCodeWithEmd(), fnInitSuperPasswordWithEMD(), etc.
#include "pbtarget.h"
#include "utils.h"          // fnUtilReLogEarlyErrors()
#include "version.h"
#include "vstack.h"         // DEVICE_INFO
#include "hal.h"
#include "flashutil.h"
#include "pbio.h"
#include "urlencode.h"
#include "networkmonitor.h"

//-----------------------------------------------------------------------------
//  Prototypes for functions that are not prototyped in an include file yet:
//-----------------------------------------------------------------------------

// From fileupdate.c:
void fnInitializeInternalFFSPreserveState( const UINT32 version );

extern void fnLCDPutStringDirect( unsigned char *pString , int x , int y , int font );

// From CRC library :
#ifdef JANUS
extern unsigned int crc16(char *StartAddress, unsigned long MemoryLength, BOOL resetFlg);
#else
extern unsigned int crc16(char *StartAddress, unsigned long MemoryLength);
#endif


//-----------------------------------------------------------------------------
//  External Variable declarations:
//-----------------------------------------------------------------------------
// From cmosupd.c:
extern  UINT32  ulDataMapResults_Ver;  

#define PO_HORIZ_PRINT_POS_LOW		5
#define PO_HORIZ_PRINT_POS_HIGH		25
#define PO_HORIZ_PRINT_POS_DEFAULT	9

// From systask.c:
extern BOOL NoEMDPresent;
extern BOOL NewEMDPresent;

extern RAM_ITEMS_XREF sRamItemsXref;

extern BOOL fCMOSAbacusAcntProblem;
extern unsigned char ucCMOSAbacusAcntErrorCode;

// From oiclock.c

extern BOOL CMOSRatesExpired;

extern BOOL bRateExpHasBeenPrompted;

extern BOOL fAllOldRatesExpNewRatesEff;


// From custdat.c
extern tRefillData          CMOS2LastRefillData;

//-----------------------------------------------------------------------------
//  Prototypes for private functions :
//-----------------------------------------------------------------------------

//void    initNewNetworkParameters( void );

void    fnUpdateCmosFromNewEmd( void );

static void *fnCMOSGetNextRateCompAdrFromIndex( UINT8 *index );


//-----------------------------------------------------------------------------
//  Global Variable declarations: 
//-----------------------------------------------------------------------------
BOOL fDeletedBoot = FALSE;

BOOL bIsExistAnyActiveRates = TRUE;

BOOL bIfExistAnyRates = TRUE;

BOOL bIfExistAnyDcaps = TRUE;

BOOL fUploadModeSpecial = FALSE;

BOOL fDcapModeSpecial = FALSE;

BOOL bIfExpireAnyRates = FALSE;



UINT8 bNumPreviouslyActiveRates = 0;

UINT8 bNumOfActiveRates = 0;

UINT8 bNumOfActiveDcaps = 0;



static BOOL fbRatesLoaded = FALSE;
static CRACKED_URL crackedDistURL;

const char cQaDistributor[]        = "http://distserv.pb.com/dstproduct.asp";
const char cSecapQaDistributor[]   = "http://distserv.pb.com/dstproductsecap.asp";

const char cNonProdAttLogin[] = "dualaccess.i411.v411jxs";
const char cNonProdAttPassword[] = "PITNEY05";
const char cNonProdAttAniServerIp[] = "10.60.00.254";
const char cNonProdAttAniServerPort[] = "7676";

//-----------------------------------------------------------------------------
//Reserved      Old Flash Status Section:
char reserved002[16] __attribute__ ((section ("sigcmos")));


//-----------------------------------------------------------------------------
//      CMOS Signature Section 
//
// This section MUST REMAIN IN THIS ORDER 
//  because the Protected Sector also references this space:
//

// ----------------- Application Header -----
// DON'T PUT EXTRA STUFF IN HERE!!! 

CMOS_Signature      CMOSSignature __attribute__ ((section ("sigcmos")));  /* don't move, otherwise pcmos.c must also be modified */
char reserved003[4] __attribute__ ((section ("sigcmos"))); // Reserved - old Last Exception Address
char reserved004[108] __attribute__ ((section ("sigcmos"))); // Reserved - old FFSProtect structure
char reserved005[12] __attribute__ ((section ("sigcmos"))); // Reserved - old CMOS preservation flags

//-----------------------------------------------------------------------------
//      Preserved CMOS Section:
//      
// This should only have externs in it, because all the stuff that is in this 
//  section should be declared in custdat.c if it is to be preserved.
//
// *****************************************************************************
// ********    Boot Sequence and Procedure for adding a cmos variable   ********
// *****************************************************************************
//
// Janus note:  This is a lengthy comment block written by George for Mega.  It has
//              been modified to apply to Janus, which has one primary difference
//              from Mega.  That is that the "fnWithEmdInit" and "fnNoEmdInit" routines
//              have been replaced with a single function fnFullCMOSInit that uses
//              a parameter to determine what kind of init is being done.  This
//              eliminates redundant code and results in one less place that needs
//              to be modified when adding a cmos variable.
//
// FPHX note:  Seems we wanted to be more Mega like ... 
//
// When the application powers up the EMD status is set to not present by default
// The reason for this is that the preserve must run before any disposition can be
// made about the EMD. If there is an EMD present in the FFS it may or may not
// be valid for this version of the UIC. Therefore the preserve process has
// two sets of two phases.
//
// The first set of of two phases is when we are updating from one version
// of the UIC to another say for example 8.17 to 9.03.
// When upgrading the initial phase is function fnInitializeCMOS.
// In here any initialization NOT requiring an EMD parameter is placed.
// The Loop variable used is CMOSSignature.ulExtVersion
// After an EMD is installed the second part of the CMOS initialization
// is placed in the fnCMOSSecondStageInit function. Just like fnInitializeCMOS
// there is a while loop iterating over the same range of versions in the
// loop located in fnInitializeCMOS. But for the fnCMOSSecondStageInit function
// we use the CMOS variable CMOSExtUpdatedToVersion as the loop variable.
//
// The second set of two phases is if we are initializing the CMOS from
// a cleared CMOS state. This occurs during PCT or when the CMOS cleared AND
// no UicDataMapsFile is present. The initial phase is the function
// fnNoEMDInit no version while is required.
// In this function any initialization that doesn't require the
// EMD is done in here. After a valid EMD is installed the function fnWithEMDInit
// is called. In here no CMOSUpdatedToVersion is used ( no While Loop ) and
// and all CMOS variables initialized from EMD parameters is done here.
//
// To Recap for an upgrade we use
//  fnInitializeCMOS for non-EMD initialization
//  fnCMOSSecondStageInit for EMD initialization
// For a first time initialization we use
//  fnNoEMDInit for non-EMD initialization
//  fnWithEMDInit for EMD Initialization
//
// To give an example, I will add a new structure to the cmos where one of its
// members require a flash parameter. we will be upgrading to CMOS Version 100.
// in the cases where a test needs to be performed on the loop variable, I use
// a value that is one less than the CURRENT version, which is the value of CMOS_VER
// for THIS build. Some existing code uses a less than test. But this is not required
// because you will be setting that variable for every iteration of the update loop.
// the only case you need to be concerned about is when you are at the version which
// is just before the current version. So for my example the CURRENT value of the CMOS_VER
// define is 100, so I will be using the loop variable value of 99 for my loop tests.
//
// This is the hypothetical new CMOS variable declaration:
// struct
// {
//    unsigned char newName[ 20 ];
//    unsigned long newCount;
//    unsigned long newLimit;
// } someNewVar;
//
// To Handle a case where we are doing an upgrade we place in fnInitializeCMOS
//   fnInitializeCMOS()
//   ..
//   ..
//   ..
//      if(CMOSSignature.ulExtVersion == 99 )
//       {
//         someNewVar.newCount = 0;
//         someNewVar.newLimit = 10;
//         someNewVar.newName[ 0 ] = '\0';
//       }
//   ..
//
// To handle the upgrade case for when the EMD IS present, we put in
// fnCMOSSecondStageInit()
//  ..
//      if(CMOSExtUpdatedToVersion == 99 )
//       {
//         unsigned char *ucP = fnFlashGetAsciiStringParm(ASP_ISP_INITIAL_NEW_NAME);
//         if(ucP)
//          strcpy(someNewVar.newName , ucP );
//       }
//  ..
//
// Note the use of the CMOSExtUpdatedToVersion variable instead of CMOSSignature.ulExtVersion
//
//
//
// The initialization from a zeroed cmos is the same as for an upgrade
//  except the initialization is not dependent on the CMOSSignature.ulExtVersion or
//  CMOSExtUpdatedToVersion loop variable
//
//  in fnNoEmdInit()
//  ..
//  ..
//  ..
//         someNewVar.newCount = 0;
//         someNewVar.newLimit = 10;
//         someNewVar.newName[ 0 ] = '\0';
//  ..
// For initialization of the member which requires the EMD
// in fnWithEMDInit
//  ..
//  ..
//  ..
//         unsigned char *ucP = fnFlashGetAsciiStringParm(ASP_ISP_INITIAL_NEW_NAME);
//         if(ucP)
//          strcpy(someNewVar.newName , ucP );
//  ..
//
/**********************************CMOS Data*********************************/


// This should only have externs in it, because all the stuff that is in this 
//  section should be declared in custdat.c if it is to be preserved.

extern CMOSPRESERVE                CMOSPreserve __attribute__ ((section ("cmos")));
extern Diagnostics                 CMOSDiagnostics __attribute__ ((section ("cmos")));
extern BatchData                   CMOSBatchData1 __attribute__ ((section ("cmos")));
extern ccStatus                    CMOSStatus1 __attribute__ ((section ("cmos")));
extern MemKeyTbl                   CMOSMemKeys __attribute__ ((section ("cmos")));
extern SetupParams                 CMOSSetupParams __attribute__ ((section ("cmos")));
extern PCNParams                   CMOSPCNParams __attribute__ ((section ("cmos")));
extern PrintParams                 CMOSPrintParams __attribute__ ((section ("cmos")));
extern FeatureTbl                  CMOSFeatureTable __attribute__ ((section ("cmos")));
extern InspectParams               CMOSInspectParams __attribute__ ((section ("cmos")));
extern ErrorLog                    CMOSErrorLog __attribute__ ((section ("cmos")));
extern ServiceModeLog              CMOSServiceLog __attribute__ ((section ("cmos")));
extern DeptAccountHeader           CMOSAccountHeader __attribute__ ((section ("cmos")));
//extern ShadowDebitLog              CMOSShadowDebitLog;    // Externed in datdict.h for some reason.
//extern ShadowRefillLog             CMOSShadowRefillLog;   // Externed in datdict.h for some reason.
extern PostageChangeLog            CMOSPostageChangeLog __attribute__ ((section ("cmos")));
extern MoneyParams                 CMOSMoneyParams __attribute__ ((section ("cmos")));
extern C6029Params                 CMOSC6029Params __attribute__ ((section ("cmos")));
//extern NETCONFIG                   CMOSNetworkConfig;    // externed in cmos.h
extern TEXT_AD_ARRAY               CMOSTextAdArray __attribute__ ((section ("cmos")));
extern TEXT_ENTRY_ARRAY            CMOSTextEntryArray __attribute__ ((section ("cmos")));
extern PERMIT_ARRAY                CMOSPermitArray __attribute__ ((section ("cmos")));
extern ZWZP_DATA                   CMOSZwzpParams __attribute__ ((section ("cmos")));
extern KIPPWInfo                          CMOSKIPPaswordInfo __attribute__ ((section ("cmos")));
extern char marriedVaultNumber[16] __attribute__ ((section ("cmos")));
extern PF_RECOVERY_LOG             CMOSpfDebitLogRec __attribute__ ((section ("cmos")));
extern BOOL                        fCMOSGuardedFFRecXferInProgress __attribute__ ((section ("cmos")));
extern UINT32                      CMOSPieceID __attribute__ ((section ("cmos")));
extern BOOL                        bCMOSConnectionMode __attribute__ ((section ("cmos")));
extern BOOL                        bCMOSLANFirewallPingEnabled __attribute__ ((section ("cmos")));
extern UINT32                      CMOSLongDHOnTime __attribute__ ((section ("cmos")));
extern CTY_ARCHIVE_PROPS           PctyArchiveRegistry[] __attribute__ ((section ("cmos")));

extern SystemErrorLog              CMOSSystemErrorLog __attribute__ ((section ("cmos")));
extern SysBasePCNLog               CMOSSysBasePCNLog __attribute__ ((section ("cmos")));
extern DeptAccountListManager      CMOSAccountLM __attribute__ ((section ("cmos")));
extern ACCOUNTING_UNION            CMOSAccountingUnion __attribute__ ((section ("cmos")));
extern DCAP_PF_SEQ                 DcapState __attribute__ ((section ("cmos")));
extern DCB_DIRECTORY_t             cmosDcapDirectory __attribute__ ((section ("cmos")));
// In FPHX, these two are the same, so one is commented out:
// extern DCB_CURRENT_PERIOD_t        cmosDcapCurrPeriod;
extern DCB_CURRENT_PERIOD_t_EXP    cmosDcapCurrPeriodEXP __attribute__ ((section ("cmos")));
extern DCB_PARAMETERS_t            cmosDcapParameters __attribute__ ((section ("cmos")));
extern DCB_RULES_TABLE_t           cmosDcapRuleSet[DC_MAX_RULES_TABLES] __attribute__ ((section ("cmos")));
extern DCB_BUCKET_NAME_LIST_t      cmosDcapBucketNameList[DC_MAX_BUCKET_NAME_LISTS] __attribute__ ((section ("cmos")));
extern DCB_CLOSED_PERIOD_t         cmosDcapClosedPeriod[DC_MAX_CLOSED_PERIODS] __attribute__ ((section ("cmos")));
extern pfp_t                       cmosDcapPfp __attribute__ ((section ("cmos")));
extern Daily_Register_Logs          cmosDailyLogs __attribute__ ((section ("cmos")));
extern Tech_Supervision_Data        cmosTechData __attribute__ ((section ("cmos")));
extern PostdatingLog                cmosPostdatingLog __attribute__ ((section ("cmos")));
extern Adjustable_Log           cmosAdjustableLog __attribute__ ((section ("cmos")));
extern DCAP_UIC_TO_BASE_LOCKING_TABLE_t cmosDcapUicBaseLockingTbl __attribute__ ((section ("cmos")));
//extern DCAP_EKP_TABLE_t        cmosDcapEkpTable;    // Not used in this file.
extern SupervisorInfo       CMOSSupervisorInfo __attribute__ ((section ("cmos")));
extern T_CMOSBalanceInquiryLog  CMOSBalanceInquiryLog __attribute__ ((section ("cmos")));

// These are not in the FPHX project:
//extern unsigned long    ulDcapSectorSize;
//extern unsigned short   usDcapNumOfSectors;
//extern unsigned long    *pulDcapBackupSector;
//extern unsigned long    *pulDcapFlashStart;
//extern unsigned long    CmosDataCaptureArea[CMOS_DATA_CAPTURE_LENGTH / sizeof(unsigned long)];
//extern unsigned long    ExpandedDummy;

extern unsigned char    fCMOSLockCodeEnable __attribute__ ((section ("cmos")));
extern unsigned char    fCMOSAutoScaleEnable __attribute__ ((section ("cmos")));
extern unsigned char    fCMOSScaleLocationCodeInited __attribute__ ((section ("cmos")));  /* Added by JPM*/
extern unsigned char    bCMOSScaleLocationCode __attribute__ ((section ("cmos")));        /* Added by JPM*/
extern unsigned char    pCMOSDiffTripWeight[] __attribute__ ((section ("cmos")));
extern unsigned char    pCMOSWOWWeightLimit[]  __attribute__ ((section ("cmos")));
extern BOOL             fCMOSPresetRatesUpdated __attribute__ ((section ("cmos")));
//extern BOOL             fCMOSDSTAutoUpdated;      // Not used in this file.
extern BOOL             fCMOSIgnoreReverseRateParam __attribute__ ((section ("cmos")));

extern char         CMOSLocalPhoneNumber[MAX_PHONE_LEN + 1] __attribute__ ((section ("cmos")));

extern FILES_TO_DOWNLOAD CMOSFilesToDownload __attribute__ ((section ("cmos")));

extern double           dblCMOSPbpDebitBal,dblCMOSPbpPrevDebitBal,dblCMOSPbpCreditBal,dblCMOSPbpPrevCreditBal __attribute__ ((section ("cmos")));
extern double           dblCMOSPbpRefillAmt __attribute__ ((section ("cmos")));

extern CMOSRATECOMPSMAP CMOSRateCompsMap __attribute__ ((section ("cmos")));  // The Active and Pending Rate Data Elements for the system.

extern char CMOSPrintedIndiciaNum[PRINTED_INDICIA_NUM_SZ] __attribute__ ((section ("cmos")));
extern char CMOSIndiciaLanguageCode[INDICIA_LANGUAGE_CODE_SZ] __attribute__ ((section ("cmos")));

extern char CMOSCustomerID_RRN[CUSTOMERID_RRN_SZ] __attribute__ ((section ("cmos")));

extern EuroConversion   cmosEuroConversion __attribute__ ((section ("cmos")));

extern unsigned long    CMOSExtUpdatedToVersion __attribute__ ((section ("cmos")));

extern BOOL fCMOSInfraUpdateReq __attribute__ ((section ("cmos")));

extern unsigned long infrastructureNVM[2048] __attribute__ ((section ("cmos")));
extern unsigned long nvmHeap[3124] __attribute__ ((section ("cmos")));

extern CMOSACTIVEVERSIONDATA    CMOSActiveVersionData __attribute__ ((section ("cmos"))); // added for firmware upgrades
extern CMOSBASEREVISIONDATA     CMOSBaseRevisionData __attribute__ ((section ("cmos")));  // added for infrastructure upload

// This isn't used in this file:
//extern DATA_ELEMENTS_FOR_UPLOAD_TO_DLS dataElementsForUploadToDls;  //For reporting up to Intellilink

extern unsigned char    CMOS_DpServiceId[8] __attribute__ ((section ("cmos")));

extern long lDcapTimestampFudge __attribute__ ((section ("cmos")));
extern unsigned short   wMinPrintMargin __attribute__ ((section ("cmos")));
extern unsigned short   wBuildTestMode __attribute__ ((section ("cmos")));

extern picklist_file_header     picklist_file_headers[] __attribute__ ((section ("cmos")));

extern CMOSDownloadCtrlBuf     CmosDownloadCtrlBuf __attribute__ ((section ("cmos")));
extern unsigned char fCMOSShowRateActive __attribute__ ((section ("cmos")));
extern unsigned char fCMOSShowRateEffective __attribute__ ((section ("cmos")));
extern BOOL fCMOSUserDenyRateActivePrompt __attribute__ ((section ("cmos")));
extern BOOL fCMOSUserDenyRateEffectivePrompt __attribute__ ((section ("cmos")));
extern BOOL fCMOSDateAdvUseNewRates __attribute__ ((section ("cmos")));

extern tBMWVDataUploadLog CMOSWebVisDataUploadLog[BMWV_MAX_DATA_UPLOAD_LOG] __attribute__ ((section ("cmos")));

extern DATETIME CMOSLastBatteryDisplay __attribute__ ((section ("cmos")));
extern DATETIME CMOSPsdBatteryTestDate __attribute__ ((section ("cmos")));
extern SBR_Control  sbrErrorScreen __attribute__ ((section ("cmos")));
extern BOOL      fCMOSHybridModeRevertToWOW __attribute__ ((section ("cmos")));

// New CMOS stuff for WOW:
extern unsigned char bCMOSWeightLogHead __attribute__ ((section ("cmos")));
extern unsigned char bCMOSWeightLogNumEntries __attribute__ ((section ("cmos")));
extern unsigned short wCMOSWeightLog[MAIL_PIECE_WEIGHT_LOG_NUM_ENTRIES] __attribute__ ((section ("cmos")));
extern unsigned char bCMOSPortraitMode __attribute__ ((section ("cmos")));
extern WOW_CALIBRATION_LOG        CMOSWOWCalibrationLog __attribute__ ((section ("cmos")));

extern DATETIME CMOSLastLocalMidnightDT __attribute__ ((section ("cmos")));
extern DATETIME CMOSLastDCAPPeriodCheckDT __attribute__ ((section ("cmos")));

extern UNICHAR CMOSRefillCode[STD_PWD_LEN + 1] __attribute__ ((section ("cmos")));

extern EXP_MTR_CMOS_DATA exp_mtr_cmos __attribute__ ((section ("cmos")));

extern BOOL CMOSPlatformFilteringOn __attribute__ ((section ("cmos")));



// end of cmos section.

//JY. WOW. The full Weight Break table stays in the new section of CMOS on the new hardware board.
//extern XB_WEIGHT_BREAK      asWB_Buffer_Full[WB_BUFF_SZ_FULL];  

// end of second cmos section.

// ****************************************************************************
//  Print Manager CMOS Area
//  This section is NOT preserved by the CMOSPreservation mechanism.
//  It is reserved for use by the PrintManager.
// ****************************************************************************
unsigned char CAI_CMOS[1024] __attribute__ ((section ("pm_cmos")));


//-----------------------------------------------------------------------------
//      Externs of data in Normal RAM section:

// From cjunior.c:
extern BOOL psdClockFlag;

//  End of External Variable declarations.
//____________________________________________________________________________


//-----------------------------------------------------------------------------
//  Local Function Prototypes: 
//-----------------------------------------------------------------------------

static void fnInitBarcodeScannerConfig( S_BCS_CONFIGURATION *pBCSConfig ) ;

void fnCMOSCleanUpRmAndRates(void);

//-----------------------------------------------------------------------------
//  Local Variable declarations: 
//-----------------------------------------------------------------------------



// This is used when making successive searches through the RateCompsMap.
// It is reset by fnResetRateSearchIndex.
static unsigned char bRateCompIndex = 0;



/***********************************************************CMOS Data*********************************/
MONEY DummyMoney = { 0, 0, 0, 0, 0 };
double DummyDouble = (double) 0;
unsigned long DummyLong = (unsigned long ) 0;
unsigned short DummyWord = (unsigned short ) 0;
unsigned short  DummyUC[1];
unsigned char DummyByte = (unsigned char) 0;
unsigned char  DummyPB[2];
char  DummyString  = (char ) 0;


//-----------------------------------------------------------------------------
//  Local Constant declarations: 
//-----------------------------------------------------------------------------
CMOSByteTableType const CMOSByteTable[MAX_CMOS_BYTE_PARM+1] =

  { &DummyByte,                                             /*  0 */        /* CMOSMemKeys[m].AdSloganNum, */
    &DummyByte,                                             /*  1 */        /* was &CMOSSetupParams.TimeOutSet.AdSloganPos,*/
    &CMOSSetupParams.AutoAdvLim,                            /*  2 */
    &CMOSSignature.Version,                                 /*  3 */
    &DummyByte,                                             /*  4 */        /* was CMOSDiagnostics.ConversionDone */
    &CMOSSetupParams.ClockOffset.DaylightSTactivated,       /*  5 */
    &DummyByte,                                             /*  6 */        /* CMOS_BP_DEPT_ACCTING_STATUS, */
    &DummyByte,                                             /*  7 */
    &DummyByte,                                             /*  8 */        /* CMOS_BP_DISPLAY_CONTRAST, */
    &DummyByte,                                             /*  9 */        /* CMOSDiagnostics.DRCleared */
    &DummyByte,                                             /* 10 */              /*CMOSEntgeltSettings.currentState */
    &DummyByte,                                             /* 11 */              /*CMOSEntgeltSettings.defaultState, */
    &DummyByte,                                             /*  12 0Ch */   /* &CMOSErrorLog[i].TotalFatalCount,*/
    &CMOSDiagnostics.fOOBComplete,                          /*  13 0Dh */
    &CMOSPrintParams.fbPrintHeadReplacement,                /*  14 0Eh */
    &CMOSSetupParams.bSavedBacklightIntensity,              /*  15 0Fh */
    &DummyByte,                                             /*  16 10h */   /* was &CMOSSetupParams.TimeOutSet.InscripPos,*/
    &DummyByte,                                             /*  17 11h */   /* was CMOSDiagnostics.KeepMeterFatal */
    &CMOSSetupParams.Language,                              /*  18 12h */
    &CMOSPrintParams.bSpeedToRunMachineAt,                  /*  19 13h */
    &CMOSInspectParams.NextPOInspED,                        /*  20 14h */
    &DummyByte,                                             /*  21 15h */   /* CMOS_BP_ORIGIN_CODE_INIT */
    &DummyByte,                                             /*  22 16h */   /* was &CMOSSetupParams.TimeOutSet.PermitMailID*/
    &DummyByte,                                             /*  23 17h */   /* was CMOSDiagnostics.PBLock */
    &CMOSPrintParams.PINType,                               /*  24 18h */
    &DummyByte,                                             /*  25 19h */   /* was CMOSDiagnostics.POLock */
    &CMOSSetupParams.RefillMode,                            /*  26 1Ah */
    &DummyByte,                                             /*  27 1Bh */   /* unused */
    &CMOSSetupParams.SerialPrinter,                         /*  28 1Ch */
    &DummyByte,                                             /*  29 1Dh */   /*&CMOSPCNParams.SoftwareVersion[i],*/
    &DummyByte,                                             /*  30 1Eh */   /* unused */
    &DummyByte,                                             /*  31 1Fh */   /* unused on comet */
    &CMOSPrintParams.bBeforePrintPurgeStrong,               /*  32 20h */
    &CMOSSetupParams.ToneDialing,                           /*  33 21h */
    (uchar *) &CMOSSetupParams.ClockOffset.DaylightSToff,   /*  34 22h */
    &DummyByte,                                             /*  35 23h */   /* CMOS_BP_MODEM_TYPEM */
    /* Added after 1/7/00 */
    &CMOSSetupParams.bLCDSavedContrast,                     // 36 24h 
    &DummyByte,                                             // 37 25h   unused 
    &DummyByte,                                             // 38 26h   unused 
    &DummyByte,                                             // 39 27h   CMOSDiagnostics.rFatalCode.bCode 
    &DummyByte,                                             // 40 28h   CMOSDiagnostics.rFatalCode.bClass 
    &CMOSSetupParams.fUseExternalModem,                     // 41 29h 
    &CMOSSetupParams.fUseExternalVault,                     // 42 2Ah 
    &CMOSDiagnostics.fOOBInkInstalled,                      // 43 2BH 
    &CMOSDiagnostics.fReceiptPending,                       // 44 2CH 
    &CMOSPCNParams.FeaturesUseCommSN,                       // 45 2Dh 
    &CMOSErrorLog.LogIndex,                                 // 51 2EH 
    &DummyByte,                                             // 56 2FH   unused
    &DummyByte,                                             // 57 30H 
    &DummyByte,                                             // 58 31h   CMOSDiagnostics.rFatalCode.fMeterFataled 
    &CMOSC6029Params.CreditBalanced,                        // 59 32h   CMOS_BP_CREDIT_BALANCED,       
    &CMOSC6029Params.DebitBalanced,                         // 60 33h   CMOS_BP_DEBIT_BALANCED,        
    &CMOSC6029Params.Refilled,                              // 61 34h   CMOS_BP_REFILLED,              
    &CMOSPrintParams.fbTapeCalibrationAllowed,              // 62 35h   CMOS_BP_TAPE_CALIBRATION_ALLOW,
    &CMOSPrintParams.fbTapeCalibrationActive,               // 63 36h   CMOS_BP_TAPE_CALIBRATION_ACTIVE
    &CMOSSetupParams.bSealerWetnessSetting,                 // 64 37h   CMOS_BP_SEALER_WETNESS_SETTING,
    &CMOSSetupParams.ucSerialPortStatus,                    // 65 38h   CMOS_BP_SERIAL_PORT_STATUS,    
    NULL_PTR
  };

/* this table is used to support the extended CMOS area wrappers */
CMOSByteTableType const CMOSExtendedByteTable[] =
  {
    &fCMOSLockCodeEnable,                                   /*  0 */
    &CMOSSystemErrorLog.LogIndex,                           /*  1 */
    &fCMOSAutoScaleEnable,                                  /*  2 */
    &fCMOSScaleLocationCodeInited,                          /*  3 */  /* Added by JPM*/
    &bCMOSScaleLocationCode,                                /*  4 */ /* Added by JPM*/
    &fCMOSPresetRatesUpdated,                               /*  5 */ /* Added by CJD*/

    /* add entries here */
    0           /* need to keep at least one entry in this table so it compiles for Spark */
  };


CMOSWORDTableType const CMOSWORDTable[MAX_CMOS_WORD_PARM+1] =
  {
    &DummyWord,                                                 /*  0 */    /*&CMOSSignature.pInitSignature[0],*/
    &CMOSStatus1.BARollover,                                    /*  1 */
    &CMOSStatus1.BCRollover,                                    /*  2 */
    &CMOSAccountHeader.usMaxNumAccount,                         /*  3 */
    &CMOSDiagnostics.PowerupCount,                              /*  4 */
    &DummyWord,                                                 /*  5 */    /*&CMOSMemKeys[0].AccountNum*/
    &DummyWord,                                                 /*  6 */    /*&CMOSMemKeyTbl.InscriptionId,*/
    &CMOSDiagnostics.wOOBFirstScreen,                           /*  7 */
    &DummyWord,                                                 /*  8 */   /* remove &CMOSSetupParams.ClockOffset.Drift, kboria */
    (unsigned short *) &CMOSSetupParams.ClockOffset.TimeZone,   /*  9 */
    &DummyWord,                                                 /* 0A */    /*&CMOSMemKeys[0].AdSloganId*/
    &DummyWord,                                                 /* 0B */    /*&CMOSMemKeys[0].InscriptionId*/
    /* Added after 1/7/00 */
    &CMOSPrintParams.wTapeLength,                               /* 0C */
    &CMOSPrintParams.RMarginPos,                                /* 0D */  /* Moved from  CMOSLONGTable */
    &DummyWord
  };

/* this table is used to support the extended CMOS area wrappers */
CMOSWORDTableType const CMOSExtendedWordTable[] =
  {
    /* add entries here */
    &CMOSFilesToDownload.DownloadFileCount,
    &CMOSActiveVersionData.activeVersionEntryCount,
    &CMOSBaseRevisionData.baseRevisionEntryCount,
    0           /* need to keep at least one entry in this table so it compiles for Spark */
  };


CMOSLONGTableType const CMOSLONGTable[MAX_CMOS_LONG_PARM+1] =
  {
    &CMOSSetupParams.AutoAdvTime,                       /*  0 */
    &CMOSStatus1.BatchCntMax,                           /*  1 */
    &CMOSBatchData1.BatchCountTotal,                    /*  2 */
    &CMOSPreserve.lwClockWasSet,                        /*  3 */
    &DummyLong,                                         /*  4 */    /*CMOSErrLog[i].DtTime */
    &DummyLong,                                         /*  5 */    /*CMOSInspectLog[i].DtTime */
    &DummyLong,                                         /*  6 */    /*CMOSServiceLog.DtTime[i] */
    &DummyLong,                                         /*  7 */    /* &CMOSMemKeys[0].PostageValue*/
    &CMOSSetupParams.lwDefRefillAmount,                 /*  8 */
    &DummyLong,                                         /*  9 */    /*CMOSErrorLog[i].ErrorCode */
    &CMOSSetupParams.lwHighValueWarning,                /*  10  0Ah */
    &CMOSPrintParams.lwJamCycleCount,                   /*  11  0Bh */
    &CMOSSetupParams.lwLowFundsWarning,                 /*  12  0Ch */
    &CMOSSetupParams.MaxSettable,                       /*  13  0Dh */
    &CMOSInspectParams.NextInspectionDate,              /*  14  0Eh */
    &CMOSPrintParams.PermitPC,                          /*  15  0Fh */
    &CMOSPrintParams.PC,                                /*  16  10h */  /*CMOS_LP_PC_AT_LAST_PIN_UPDATE, */
    &CMOSPrintParams.PinNumReg,                         /*  17  11h */
    &DummyLong,                                         /*  18  12h */  /*CMOS_LP_POWER_UP_COUNT  */
    &CMOSPrintParams.lwPrintedEnvCount,                 /*  19  13h */
    &CMOSPrintParams.lwSealOnlyCount,                   /*  20  14h */
    &CMOSPrintParams.lwSkewCycleCount,                  /*  21  15h */
    &CMOSPrintParams.lwTapeCycleCount,                  /*  22  16h */
    &CMOSSetupParams.RollerTimeOut,                     /*  23  17h */
    &CMOSPrintParams.lwTotalPieceCount,                 /*  24  18h */
    /* added after 1/7/00 */
    &DummyLong,                                         /*  25  19h */  /* was CMOSDiagnostics.LastPowerTime*/
    &CMOSDiagnostics.lwOOBStepsCompleted,               /*  26  1ah */
    &CMOSPrintParams.PrtHdNum,                          /*  27  1bh */
    &CMOSPrintParams.PrtHdSftVs,                        /*  28  1Ch */
    &CMOSServiceLog.DtTime[0],                          /*  29  1Dh */
    &CMOSServiceLog.DtTime[1],                          /*  30  1EH */
    &CMOSServiceLog.DtTime[2],                          /*  31  1FH */
    &CMOSServiceLog.DtTime[3],                          /*  32  20H */
    &CMOSSetupParams.ulMailInactivityTimeout,           /*  33  21H */  /* Mail 'Auto-start' timer */
    /* Added 8/16/2002 */
    &CMOSPrintParams.lwWeighCount,
    &CMOSPrintParams.lwStripCount,
    &DummyLong  };

CMOSLONGTableType const CMOSExtendedLongTable[] =
  {
    /* add entries here */
    0           /* need to keep at least one entry in this table so it compiles for Spark */
  };


/*
 * The format for the following table is:
 * address, size
 */

CMOSASCIITableType const CMOSASCIITable[MAX_CMOS_STR_PARM+1] =
  {
    {&CMOSPCNParams.CurrSMR[0],                                 SMR_LEN+1},             /*   0 */
    {&DummyString,                                              0},                     /*   1 */ /* CMOS_STR_DEPT_ACCT_NAME,*/
    {&DummyString,                                              0},                     /*   2 */ /* CMOS_STR_DEPT_ACCT_PWD, */
    {&CMOSPCNParams.PsrId[0],                                   PSR_LEN+1},             /*   3 */
    {&DummyString,                                              0},                     /*   4 */  /* CMOSMemKeys[m].PresetName[k] */
    {&DummyString,                                              0},                     /*   5 */ /* CMOSInspectLog[i].SCSerialNo[j] */
    {(char *) &CMOSNetworkConfig.modemInit[0],                  MAXINITLEN+1},          /*   6 */
    {&CMOSSetupParams.PhoneNumber[0],                           MAX_PHONE_LEN+1},       /*   7 */
    {&DummyString,                                              0},                     /*   8 */ /* was CMOSSetupParams.SuperPassword[0] */
    {(char *) &CMOSNetworkConfig.extmodemInit[0],               MAXINITLEN+1},          /*   9 */
    /* Added after 1/7/00 */
    {&CMOSSetupParams.DialingPrefix[0],                         MAX_PREFIX_LEN+1},      /* 0xA */
    /* Added @ 2/10/00 */
    {&DummyString,                                              0},                     /* 0xB */
    {&CMOSSetupParams.MailOriginEKPNumber[0],                   MAX_EKP_LEN+1},         /* 0xC */
    {&DummyString,                                              0}

  };


/* this table is used to support the extended CMOS area wrappers */
CMOSASCIITableType const CMOSExtendedASCIITable[] =
  {
    /* add entries here */
    {&CMOSLocalPhoneNumber[0],                                  MAX_PHONE_LEN+1},                   /*   0 */
    {NULL_PTR,                      0}
  };


CMOSDoubleTableType const CMOSDoubleTable[MAX_CMOS_DB_PARM+1] =
  /* */
  {
    &CMOSStatus1.BatchAmtMax,              /*   0 */
    &CMOSBatchData1.BatchAmountTotal,      /*   1 */
    &DummyDouble                           /*   1 */
  };

CMOSDoubleTableType const CMOSExtendedDoubleTable[] =
  {
    /* add entries here */
    &dblCMOSPbpDebitBal,                                    /*  0   */
    &dblCMOSPbpPrevDebitBal,                                /*  1   */
    &dblCMOSPbpCreditBal,                                   /*  2   */
    &dblCMOSPbpPrevCreditBal,                               /*  3   */
    &dblCMOSPbpRefillAmt,                                   /*  4   */
    0           /* need to keep at least one entry in this table so it compiles for Spark */
  };


CMOSPBTableType const CMOSPBTable[MAX_CMOS_PB_PARM+1] =
  {
    (unsigned char *) &CMOSSetupParams.lwPBPAcctNumber,         /*   0 */
    (unsigned char *) &DummyPB[0],                              //   1
    (unsigned char *) &CMOSFeatureTable.fTbl[0],                /*   2 */
    (unsigned char *) &DummyPB[0],                              //   3
    /* After 1/13/00 */
    (unsigned char *) &CMOSPCNParams.SoftwareVersion[0],           /*   4 */
    (unsigned char *) &CMOSErrorLog.ErrorTable[0],               /*   5 */ /* Error Table 1 */
    (unsigned char *) &CMOSErrorLog.ErrorTable[1],               /*   6 */ /* Error Table 2 */
    (unsigned char *) &CMOSErrorLog.ErrorTable[2],               /*   7 */ /* Error Table 3 */
    (unsigned char *) &CMOSErrorLog.ErrorTable[3],               /*   8 */ /* Error Table 4 */
    (unsigned char *) &CMOSErrorLog.ErrorTable[4],               /*   9 */ /* Error Table 5 */
    (unsigned char *) &CMOSErrorLog.ErrorTable[5],               /* 10 0AH */ /* Error Table 6 */
    (unsigned char *) &CMOSErrorLog.ErrorTable[6],               /* 11 0BH */ /* Error Table 7 */
    (unsigned char *) &CMOSErrorLog.ErrorTable[7],               /* 12 0CH */ /* Error Table 8 */
    (unsigned char *) &CMOSErrorLog.ErrorTable[8],               /* 13 0DH */ /* Error Table 9 */
    (unsigned char *) &CMOSErrorLog.ErrorTable[9],               /* 14 0EH */ /* Error Table 10 */
    &DummyPB[0],                                                   /* 15 0FH */ /* CMOSInspectLog[0] */
    &DummyPB[0],                                                   /* 16 10H */ /* CMOSInspectLog[1] */
    &DummyPB[0],                                                   /* 17 11H */ /* CMOSInspectLog[2] */
    &DummyPB[0],                                                   /* 18 12H */ /* CMOSInspectLog[3] */
    &DummyPB[0],                                                   /* 19 13H */ /* CMOSAuditLog.AuditTable[0]*/
    &DummyPB[0],                                                   /* 20 14H */ /* CMOSAuditLog.AuditTable[1]*/
    &DummyPB[0],                                                   /* 21 15H */ /* CMOSAuditLog.AuditTable[2]*/
    &DummyPB[0],                                                   /* 22 16H */ /* CMOSAuditLog.AuditTable[3]*/
    &DummyPB[0],                                                   /* 23 17H */ /* CMOSAuditLog.AuditTable[4]*/
    &DummyPB[0],                                                   /* 24 18H */ /* CMOSAuditLog.AuditTable[5]*/
    &DummyPB[0],                                                   /* 25 19H */ /* CMOSAuditLog.AuditTable[6]*/
    &DummyPB[0],                                                   /* 26 1AH */ /* CMOSAuditLog.AuditTable[7]*/
    &DummyPB[0],                                                   /* 27 1BH */ /* CMOSAuditLog.AuditTable[0]*/
    &DummyPB[0],                                                   /* 28 1CH */ /* CMOSAuditLog.AuditTable[9]*/
    &DummyPB[0]
  } ;


/* this table is used to support the extended CMOS area wrappers */

/*
 * 07/13/2001 Joseph P. Tokarski (CSS, LLC) - Added entries for the Midjet MCS Error Log
 *
 * 01/08/2002 Joseph P. Tokarski - Removed entries for the Midjet MCS Error Log and supplanted
 *                                  them with entries for the Comet System Error Log
 */
CMOSPBTableType const CMOSExtendedPBTable[] =
  {
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[0],         /*   0 */ /* System Error Table 1 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[1],         /*   1 */ /* System Error Table 2 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[2],         /*   2 */ /* System Error Table 3 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[3],         /*   3 */ /* System Error Table 4 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[4],         /*   4 */ /* System Error Table 5 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[5],         /*   5 */ /* System Error Table 6 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[6],         /*   6 */ /* System Error Table 7 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[7],         /*   7 */ /* System Error Table 8 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[8],         /*   8 */ /* System Error Table 9 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[9],         /*   9 */ /* System Error Table 10 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[10],        /*   10 */ /* System Error Table 11 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[11],        /*   11 */ /* System Error Table 12 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[12],        /*   12 */ /* System Error Table 13 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[13],        /*   13 */ /* System Error Table 14 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[14],        /*   14 */ /* System Error Table 15 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[15],        /*   15 */ /* System Error Table 16 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[16],        /*   16 */ /* System Error Table 17 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[17],        /*   17 */ /* System Error Table 18 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[18],        /*   18 */ /* System Error Table 19 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[19],        /*   19 */ /* System Error Table 20 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[20],        /*   20 */ /* System Error Table 21 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[21],        /*   21 */ /* System Error Table 22 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[22],        /*   22 */ /* System Error Table 23 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[23],        /*   23 */ /* System Error Table 24 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[24],        /*   24 */ /* System Error Table 25 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[25],        /*   25 */ /* System Error Table 26 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[26],        /*   26 */ /* System Error Table 27 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[27],        /*   27 */ /* System Error Table 28 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[28],        /*   28 */ /* System Error Table 29 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[29],        /*   29 */ /* System Error Table 30 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[30],        /*   30 */ /* System Error Table 31 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[31],        /*   31 */ /* System Error Table 32 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[32],        /*   32 */ /* System Error Table 33 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[33],        /*   33 */ /* System Error Table 34 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[34],        /*   34 */ /* System Error Table 34 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[35],        /*   35 */ /* System Error Table 36 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[36],        /*   36 */ /* System Error Table 37 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[37],        /*   37 */ /* System Error Table 38 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[38],        /*   38 */ /* System Error Table 39 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[39],        /*   39 */ /* System Error Table 40 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[40],        /*   40 */ /* System Error Table 41 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[41],        /*   41 */ /* System Error Table 42 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[42],        /*   42 */ /* System Error Table 43 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[43],        /*   43 */ /* System Error Table 44 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[44],        /*   44 */ /* System Error Table 45 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[45],        /*   45 */ /* System Error Table 46 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[46],        /*   46 */ /* System Error Table 47 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[47],        /*   47 */ /* System Error Table 48 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[48],        /*   48 */ /* System Error Table 49 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[49],        /*   49 */ /* System Error Table 50 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[50],        /*   50 */ /* System Error Table 51 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[51],        /*   51 */ /* System Error Table 52 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[52],        /*   52 */ /* System Error Table 53 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[53],        /*   53 */ /* System Error Table 54 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[54],        /*   54 */ /* System Error Table 55 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[55],        /*   55 */ /* System Error Table 56 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[56],        /*   56 */ /* System Error Table 57 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[57],        /*   57 */ /* System Error Table 58 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[58],        /*   58 */ /* System Error Table 59 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[59],        /*   59 */ /* System Error Table 60 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[60],        /*   60 */ /* System Error Table 61 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[61],        /*   61 */ /* System Error Table 62 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[62],        /*   62 */ /* System Error Table 63 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[63],        /*   63 */ /* System Error Table 64 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[64],        /*   64 */ /* System Error Table 65 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[65],        /*   65 */ /* System Error Table 66 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[66],        /*   66 */ /* System Error Table 67 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[67],        /*   67 */ /* System Error Table 68 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[68],        /*   68 */ /* System Error Table 69 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[69],        /*   69 */ /* System Error Table 70 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[70],        /*   70 */ /* System Error Table 71 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[71],        /*   71 */ /* System Error Table 72 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[72],        /*   72 */ /* System Error Table 73 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[73],        /*   73 */ /* System Error Table 74 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[74],        /*   74 */ /* System Error Table 75 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[75],        /*   75 */ /* System Error Table 76 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[76],        /*   76 */ /* System Error Table 77 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[77],        /*   77 */ /* System Error Table 78 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[78],        /*   78 */ /* System Error Table 79 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[79],        /*   79 */ /* System Error Table 80 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[80],        /*   80 */ /* System Error Table 81 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[81],        /*   81 */ /* System Error Table 82 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[82],        /*   82 */ /* System Error Table 83 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[83],        /*   83 */ /* System Error Table 84 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[84],        /*   84 */ /* System Error Table 85 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[85],        /*   85 */ /* System Error Table 86 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[86],        /*   86 */ /* System Error Table 87 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[87],        /*   87 */ /* System Error Table 88 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[88],        /*   88 */ /* System Error Table 89 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[89],        /*   89 */ /* System Error Table 90 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[90],        /*   90 */ /* System Error Table 91 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[91],        /*   91 */ /* System Error Table 92 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[92],        /*   92 */ /* System Error Table 93 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[93],        /*   93 */ /* System Error Table 94 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[94],        /*   94 */ /* System Error Table 95 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[95],        /*   95 */ /* System Error Table 96 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[96],        /*   96 */ /* System Error Table 97 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[97],        /*   97 */ /* System Error Table 98 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[98],        /*   98 */ /* System Error Table 99 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[99],        /*   99 */ /* System Error Table 100 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[100],        /*   100 */ /* System Error Table 101 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[101],        /*   101 */ /* System Error Table 102 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[102],        /*   102 */ /* System Error Table 103 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[103],        /*   103 */ /* System Error Table 104 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[104],        /*   104 */ /* System Error Table 105 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[105],        /*   105 */ /* System Error Table 106 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[106],        /*   106 */ /* System Error Table 107 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[107],        /*   107 */ /* System Error Table 108 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[108],        /*   108 */ /* System Error Table 109 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[109],        /*   109 */ /* System Error Table 110 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[110],        /*   110 */ /* System Error Table 111 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[111],        /*   111 */ /* System Error Table 112 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[112],        /*   112 */ /* System Error Table 113 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[113],        /*   113 */ /* System Error Table 114 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[114],        /*   114 */ /* System Error Table 115 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[115],        /*   115 */ /* System Error Table 116 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[116],        /*   116 */ /* System Error Table 117 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[117],        /*   117 */ /* System Error Table 118 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[118],        /*   118 */ /* System Error Table 119 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[119],        /*   119 */ /* System Error Table 120 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[120],        /*   120 */ /* System Error Table 121 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[121],        /*   121 */ /* System Error Table 122 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[122],        /*   122 */ /* System Error Table 123 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[123],        /*   123 */ /* System Error Table 124 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[124],        /*   124 */ /* System Error Table 125 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[125],        /*   125 */ /* System Error Table 126 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[126],        /*   126 */ /* System Error Table 127 */
    (unsigned char *) &CMOSSystemErrorLog.SystemErrorTable[127],        /*   127 */ /* System Error Table 128 */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[0],          /*   128 */ /* Download File Entry 1  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[1],          /*   129 */ /* Download File Entry 2  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[2],          /*   130 */ /* Download File Entry 3  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[3],          /*   131 */ /* Download File Entry 4  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[4],          /*   132 */ /* Download File Entry 5  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[5],          /*   133 */ /* Download File Entry 6  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[6],          /*   134 */ /* Download File Entry 7  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[7],          /*   135 */ /* Download File Entry 8  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[8],          /*   136 */ /* Download File Entry 9  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[9],          /*   137 */ /* Download File Entry 10  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[10],         /*   138 */ /* Download File Entry 11  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[11],         /*   139 */ /* Download File Entry 12  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[12],         /*   140 */ /* Download File Entry 13  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[13],         /*   141 */ /* Download File Entry 14  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[14],         /*   142 */ /* Download File Entry 15  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[15],         /*   143 */ /* Download File Entry 16  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[16],         /*   144 */ /* Download File Entry 17  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[17],         /*   145 */ /* Download File Entry 18  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[18],         /*   146 */ /* Download File Entry 19  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[19],         /*   147 */ /* Download File Entry 20  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[20],         /*   148 */ /* Download File Entry 21  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[21],         /*   149 */ /* Download File Entry 22  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[22],         /*   150 */ /* Download File Entry 23  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[23],         /*   151 */ /* Download File Entry 24  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[24],         /*   152 */ /* Download File Entry 25  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[25],         /*   153 */ /* Download File Entry 26  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[26],         /*   154 */ /* Download File Entry 27  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[27],         /*   155 */ /* Download File Entry 28  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[28],         /*   156 */ /* Download File Entry 29  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[29],         /*   157 */ /* Download File Entry 30  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[30],         /*   158 */ /* Download File Entry 31  */
    (unsigned char *) &CMOSFilesToDownload.DownloadFileEntry[31],         /*   159 */ /* Download File Entry 32  */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[0],    /*   160 */ /* Active Base Entry 1     */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[1],    /*   161 */ /* Active Base Entry 2     */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[2],    /*   162 */ /* Active Base Entry 3     */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[3],    /*   163 */ /* Active Base Entry 4     */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[4],    /*   164 */ /* Active Base Entry 5     */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[5],    /*   165 */ /* Active Base Entry 6     */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[6],    /*   166 */ /* Active Base Entry 7     */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[7],    /*   167 */ /* Active Base Entry 8     */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[8],    /*   168 */ /* Active Base Entry 9     */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[9],    /*   169 */ /* Active Base Entry 10    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[10],   /*   170 */ /* Active Base Entry 11    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[11],   /*   171 */ /* Active Base Entry 12    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[12],   /*   172 */ /* Active Base Entry 13    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[13],   /*   173 */ /* Active Base Entry 14    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[14],   /*   174 */ /* Active Base Entry 15    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[15],   /*   175 */ /* Active Base Entry 16    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[16],   /*   176 */ /* Active Base Entry 17    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[17],   /*   177 */ /* Active Base Entry 18    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[18],   /*   178 */ /* Active Base Entry 19    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[19],   /*   179 */ /* Active Base Entry 20    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[20],   /*   180 */ /* Active Base Entry 21    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[21],   /*   181 */ /* Active Base Entry 22    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[22],   /*   182 */ /* Active Base Entry 23    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[23],   /*   183 */ /* Active Base Entry 24    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[24],   /*   184 */ /* Active Base Entry 25    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[25],   /*   185 */ /* Active Base Entry 26    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[26],   /*   186 */ /* Active Base Entry 27    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[27],   /*   187 */ /* Active Base Entry 28    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[28],   /*   188 */ /* Active Base Entry 29    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[29],   /*   189 */ /* Active Base Entry 30    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[30],   /*   190 */ /* Active Base Entry 31    */
    (unsigned char *) &CMOSActiveVersionData.activeVersionData[31],   /*   191 */ /* Active Base Entry 32    */
    (unsigned char *) &CMOSBaseRevisionData.baseRevisionData[0],          /*   192 */ /* Base Revision Entry 1   */
    (unsigned char *) &CMOSBaseRevisionData.baseRevisionData[1],          /*   193 */ /* Base Revision Entry 2   */
    (unsigned char *) &CMOSBaseRevisionData.baseRevisionData[2],          /*   194 */ /* Base Revision Entry 3   */
    (unsigned char *) &CMOSBaseRevisionData.baseRevisionData[3],          /*   195 */ /* Base Revision Entry 4   */
    (unsigned char *) &CMOSBaseRevisionData.baseRevisionData[4],          /*   196 */ /* Base Revision Entry 5   */
    (unsigned char *) &CMOSBaseRevisionData.baseRevisionData[5],          /*   197 */ /* Base Revision Entry 6   */
    (unsigned char *) &CMOSBaseRevisionData.baseRevisionData[6],          /*   198 */ /* Base Revision Entry 7   */
    (unsigned char *) &CMOSBaseRevisionData.baseRevisionData[7],          /*   199 */ /* Base Revision Entry 8   */
    (unsigned char *) &CMOSBaseRevisionData.baseRevisionData[8],          /*   200 */ /* Base Revision Entry 9   */
    (unsigned char *) &CMOSBaseRevisionData.baseRevisionData[9],          /*   201 */ /* Base Revision Entry 10  */
    /* add entries here */
    0           /* need to keep at least one entry in this table so it compiles for Spark */
  };


CMOSUCTableType const CMOSUCTable[MAX_CMOS_UC_PARM+1] =
  {
    {&CMOSSetupParams.pLockCodeUnicode[0], LOCK_CODE_LEN},      /*   0 */
    {&DummyUC[0],1}
  };

/* this table is used to support the extended CMOS area wrappers */
CMOSUCTableType const CMOSExtendedUCTable[] =
  {
    /* add entries here */
    0           /* need to keep at least one entry in this table so it compiles for Spark */
  };


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//                              Functions: 
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

/*****************************************************************************
 * Name                fnClearPostgeChangeLog
 * Description:
 *   .
 * Inputs:   None.
 * Returns:  None.
 *
 * Revision History:
 -------------------------------------------------------------------------*/
static void fnClearPostgeChangeLog( void )
{
    (void)memset(&CMOSPostageChangeLog, 0, sizeof(CMOSPostageChangeLog));
    CMOSPostageChangeLog.LogIndex = 0;
}

/*****************************************************************************
 * Name                InitializeNetworkDefaults
 * Description  
 *      This function set the CMOS version of the network variables to their 
 *      default values.  It is called when the CMOS is brand new from both 
 *      the function that is called with an EMD and without an EMD. 
 *
  -------------------------------------------------------------------------*/
void InitializeNetworkDefaults()
{
    CMOSNetworkConfig.bEnableSSLCertVerify = 1;  // verify certs by default
    fnSetNVRAMdistributorEMDURLType(DIST_URL_TYPE_PROD);  // default to PROD
	fnSetNVRAMDistributorURLSource(DIST_URL_SOURCE_EMD);
    // this will force the URL to be loaded from the EMD
    (void)memset(CMOSNetworkConfig.distributorUrl, 0, sizeof(CMOSNetworkConfig.distributorUrl));
}

/*****************************************************************************
 * Description
 *      Loads the Distributor URL from EMD to NVRAM
 *      Leaves URL empty if no valid URL is available
 -------------------------------------------------------------------------*/
void loadEMDDistributorURL()
{
    char *retStr = NULL;

    (void)memset(CMOSNetworkConfig.distributorUrl, 0, sizeof(CMOSNetworkConfig.distributorUrl));
	// Select URL based on NVRAM setting
	retStr = (char *) fnFlashGetAsciiStringParm((CMOSNetworkConfig.distributorEMDUrlType == DIST_URL_TYPE_PROD)? ASP_DISTRIBUTOR_URL : ASP_QA_DISTRIBUTOR_URL);
	if(retStr == NULL)
	{
    	dbgTrace(DBG_LVL_ERROR,"Error: EMD Distributor URL not found");
    	return;
	}

	if (crackUrl(&crackedDistURL, retStr) != NU_SUCCESS)
	{
    	dbgTrace(DBG_LVL_ERROR,"Error: EMD Distributor URL invalid: %s", retStr);
    	return;
	}

#ifndef ALLOW_NON_SSL
	if (strncmp(crackedDistURL.protocol,"https",5) != 0)
	{
		dbgTrace(DBG_LVL_ERROR,"Error: EMD Distributor URL does not use SSL: %s", retStr);
		return;
	}
#endif

	(void)strncpy(CMOSNetworkConfig.distributorUrl, retStr, sizeof(CMOSNetworkConfig.distributorUrl) - 1);
}


void initNewNetworkParameters(void)
{
    char *retStr;
	
	
    (void)memset(CMOSNetworkConfig.attLczPhoneNumber, 0, sizeof(CMOSNetworkConfig.attLczPhoneNumber));
    (void)memset(CMOSNetworkConfig.primaryDnsServer, 0, sizeof(CMOSNetworkConfig.primaryDnsServer));
    retStr = (char *) fnFlashGetAsciiStringParm(ASP_PRIMARY_DNS_SERVER);
    if(retStr)
    {
        (void)strncpy(CMOSNetworkConfig.primaryDnsServer, retStr, sizeof(CMOSNetworkConfig.primaryDnsServer ) - 1);
    }

    (void)memset(CMOSNetworkConfig.secondaryDnsServer, 0, sizeof(CMOSNetworkConfig.secondaryDnsServer));
    retStr = (char *) fnFlashGetAsciiStringParm(ASP_SECONDARY_DNS_SERVER);
    if(retStr)
    {
        (void)strncpy(CMOSNetworkConfig.secondaryDnsServer, retStr, sizeof(CMOSNetworkConfig.secondaryDnsServer) - 1);
    }

// If URL in NVRAM is OK, then leave it alone in Debug mode and confirm SSL in Release mode,
// otherwise load EMD Distributor URL
	if (crackUrl(&crackedDistURL, CMOSNetworkConfig.distributorUrl) == NU_SUCCESS)
	{
#ifndef ALLOW_NON_SSL
		if (strncmp(crackedDistURL.protocol,"https",5) != 0)
		{
			dbgTrace(DBG_LVL_ERROR,"Error: Stored Distributor URL does not use SSL: %s", CMOSNetworkConfig.distributorUrl);
			dbgTrace(DBG_LVL_ERROR,"Error: Switching to use EMD URL");
			loadEMDDistributorURL();
		}
#endif
	}
	else
	{
    	loadEMDDistributorURL();
	}

    (void)memset(CMOSNetworkConfig.attLoginAccountAndUserId, 0, sizeof(CMOSNetworkConfig.attLoginAccountAndUserId));
    retStr = (char *) fnFlashGetAsciiStringParm(ASP_ISP_ACCOUNT_AND_USER_ID);
    if(retStr)
    {
        (void)strncpy(CMOSNetworkConfig.attLoginAccountAndUserId, retStr, sizeof(CMOSNetworkConfig.attLoginAccountAndUserId) - 1);
    }

    (void)memset(CMOSNetworkConfig.attPassword, 0, sizeof(CMOSNetworkConfig.attPassword));
    retStr = (char *) fnFlashGetAsciiStringParm(ASP_ISP_PASSWORD);
    if(retStr)
    {
        (void)strncpy(CMOSNetworkConfig.attPassword, retStr, sizeof(CMOSNetworkConfig.attPassword) - 1);
    }

    (void)memset(CMOSNetworkConfig.attAniServerIp, 0, sizeof(CMOSNetworkConfig.attAniServerIp));
    retStr = (char *) fnFlashGetAsciiStringParm(ASP_ISP_ANI_LCZ_SERVER_IP);
    if(retStr)
    {
        (void)strncpy(CMOSNetworkConfig.attAniServerIp, retStr, sizeof(CMOSNetworkConfig.attAniServerIp) - 1);
    }

    (void)memset(CMOSNetworkConfig.attAniServerPortNumber, 0, sizeof(CMOSNetworkConfig.attAniServerPortNumber));
    retStr = (char *) fnFlashGetAsciiStringParm(ASP_ISP_ANI_LCZ_PORT_NUMBER);
    if(retStr)
    {
        (void)strncpy(CMOSNetworkConfig.attAniServerPortNumber, retStr, sizeof(CMOSNetworkConfig.attAniServerPortNumber) - 1);
    }
}

/* **********************************************************************
// FUNCTION NAME: fnInitializeNetworkDataFromFlash()
// PURPOSE: This function is called to fill in the initial
//          network parameters from flash. Once done they
//          then can be changed.
// AUTHOR: George Monroe
//
// INPUTS:
// RETURNS:
// **********************************************************************/

void fnInitializeNetworkDataFromFlash( void )
{
    char *retStr;
	
    initNewNetworkParameters();

}


/* ************************************************************************/
// FUNCTION NAME: fnWithEmdInit
// DESCRIPTION: 
//      Finish CMOS Default Initialization WITH an EMD
// OUTPUTS: 
//      none
// MODS:
//
// AUTHOR: George Monroe
// NOTES:
 //         To Recap for an upgrade we use
 //             fnInitializeCMOS for non-EMD initialization
 //             fnCMOSSecondStageInit for EMD initialization
 //         For a first time initialization we use
 //             fnNoEMDInit for non-EMD initialization
 //             fnWithEMDInit for EMD Initialization
// *************************************************************************/
void fnWithEmdInit( void )
{   /* Needed for false variables in table */

//    unsigned char*    pPrnSettingTable;
    UINT16  uwTempWord;
//    ulong       i;
    /* for SetupParams.Factory_IPIN */
    char pStr[MAX_IPIN_LEN+1];
    unsigned char bRateManagerPresent = FALSE;
    unsigned char bStatus;
    unsigned char index;
    unsigned char *pEMDStrParam;
    BOOL  fEnabled;

    bRateManagerPresent = CMOSSetupParams.bRatingIsRateManagerPresent;

    fnCMOSInitLockCodeWithEmd();    // Disables lock code and "clears" it.  If disabling is not
    EndianAwareCopy(&CMOSSetupParams.lwDefRefillAmount, fnFlashGetMoneyParm(MP_FACTORY_REFILL_AMT) + 1, sizeof(ulong));
    (void)memcpy( &CMOSMoneyParams.RefillAmt[1], &CMOSSetupParams.lwDefRefillAmount, sizeof(CMOSSetupParams.lwDefRefillAmount) );
    EndianAwareCopy(&CMOSSetupParams.lwHighValueWarning, fnFlashGetMoneyParm(MP_FACTORY_HIGH_VALUE_WARNING) + 1, sizeof(ulong));
    EndianAwareCopy(&CMOSSetupParams.lwLowFundsWarning, fnFlashGetMoneyParm(MP_FACTORY_LOW_FUNDS_WARNING) + 1, sizeof(ulong));
    EndianAwareCopy(&CMOSSetupParams.lwMinDelConPostage, fnFlashGetMoneyParm(MP_DEFAULT_DELCON_POSTAGE_VAL) + 1, sizeof(ulong));
    CMOSSetupParams.ClockOffset.TimeZone = (short) fnFlashGetWordParm(WP_TIME_LOCALIZATION_OFFSET);
    CMOSSetupParams.ClockOffset.DaylightSToff = (char) fnFlashGetByteParm(BP_FACTORY_DST_OFFSET);
    CMOSSetupParams.ClockOffset.DaylightSTactivated = (BOOL) fnFlashGetByteParm(BP_DST_OFFSET_ENABLE);
    CMOSSetupParams.wpUserSetPresetClearTimeout    = fnFlashGetWordParm(WP_FACTORY_SLEEP_TIMEOUT);  /* init to default sleep time */
    if (fnFlashGetByteParm(BP_PBP_REFILL_MODE) == PBP_MODE_KEYPAD_ONLY)
    {
        CMOSSetupParams.RefillMode       = REFILL_MODE_KEYPAD;
    }
    else
    {
        CMOSSetupParams.RefillMode  = REFILL_MODE_MODEM;
    }

    CMOSSetupParams.Language             = fnFlashGetByteParm(BP_FACTORY_DISPLAY_LANG);
    (void)sprintf(pStr,"%lu",fnFlashGetLongParm(LP_FACTORY_IPIN));
    (void)memcpy(&CMOSSetupParams.FactoryIPIN[0],pStr,MAX_IPIN_LEN+1);
	
    /* Ignore the lint warning 644 about bRateManagerPresent not being initialized */
    CMOSSetupParams.bRatingIsRateManagerPresent = bRateManagerPresent;
    CMOSPrintParams.PermitPC = fnFlashGetLongParm(LP_INITIAL_PERMIT_MAIL_COUNT); /* Permit Mail Piece count*/
    CMOSPrintParams.PINType = fnFlashGetByteParm(BP_PIN_TYPE);
    bCMOSScaleLocationCode = fnFlashGetByteParm(BP_FACTORY_SCALE_LOCATION_CODE); /* Default code */
    CMOSZwzpParams.timeout = fnFlashGetByteParm(BP_FACTORY_POSTAGE_DISPLAY_TIMEOUT); /* Default time */

  {
        unsigned char   *pFeatureList;
        long            n;
        BOOL    fbIsEnabled;
        unsigned char bFeatType;
        unsigned char bInx;

        pFeatureList = fnFlashGetPackedByteParm(PBP_FEATURE_LIST);

        /* ignore the warning 644 about pFeatureList not being initialized since it should
        have been initialized above. */
        for (n = 0; n < MAX_FEATURES; n++)
        {
            CMOSFeatureTable.fTbl[n] = ( unsigned char) *(pFeatureList + n);
        }

        if (IsFeatureEnabled(&fbIsEnabled, SERIAL_PORT_MSG_SUPRT, &bFeatType, &bInx) == TRUE)
        {
            if (fbIsEnabled == TRUE)
            {
                CMOSSetupParams.ucSerialPortStatus = ENBLD;
            }
            else
            {
                CMOSSetupParams.ucSerialPortStatus = DISBLD;
            }
        }
        else
        {
            CMOSSetupParams.ucSerialPortStatus = DISBLD;
        }
    }

	{
		unsigned char	*pRightMarginData = fnFlashGetPackedByteParm(PBP_RIGHT_MARGIN_ADJUST_RANGE);

		if (pRightMarginData)
			CMOSPrintParams.RMarginPos = pRightMarginData[0];
		else
			CMOSPrintParams.RMarginPos = PO_HORIZ_PRINT_POS_DEFAULT;
	}

	//TODO - AB - confirm have to initialize accounting ptrs before doing init pwds
//	fnInitDeptAccountAndSupervisorPointers();
    fnInitSuperPasswordWithEMD();
	
    pEMDStrParam = fnFlashGetAsciiStringParm(ASP_CURRENT_UIC_SMR);
    if (pEMDStrParam && (strlen((const char *) pEMDStrParam) > 0))
    {
        memcpy(&CMOSPCNParams.CurrSMR[0], pEMDStrParam,SMR_LEN+1);
    }

    pEMDStrParam = fnFlashGetAsciiStringParm(ASP_PSR_FILE_ID);
    if (pEMDStrParam && (strlen((const char *) pEMDStrParam) > 0))
    {
        memcpy(&CMOSPCNParams.PsrId[0], pEMDStrParam,PSR_LEN+1);
    }



#ifndef TARGET_FLASH /* development environment......       */
//    devOverideFlags.vltOveride = REAL_CARD;
//    devOverideFlags.headOveride = REAL_HEAD;
#endif

//    fnInitializeNetworkDataFromFlash();

    CMOSPermitArray.bMaxPermitsByPcn = fnGetMaxPermitAllowedNum();

    /* Added some bytes to the text ad & text msg arrays */
    /* Init them to emd settings. */
    CMOSTextAdArray.bMaxTextAdsByPcn = fnGetMaxADSloganNum();
    CMOSTextEntryArray.bMaxTextMsgsByPcn = fnGetTxtmsgMaxAllowed();
}

// ********************************************************************************
// FUNCTION NAME: fnCMOSClearCmos()
// PURPOSE:     
//      Zero-out the cmos, and extended cmos, but not the cmos signature data
//---------------------------------------------------------------------
void fnCMOSClearCmos( void )
{
    size_t  szNumBytesToClear;
    UINT8 * pStart; 

    // Set parameters for first cmos section:
    pStart = (UINT8 *) HALGetCMOSBase();          // For all systems, this is the start of the BSSCmos section.
    szNumBytesToClear = (size_t) HALGetCMOSSize();  // Number of cmos bytes actually used in the BSScmos section.
    
    // Clear out the bytes to be used in the first section.
    (void)memset( pStart, 0, szNumBytesToClear );

    //Now clear second section if it is not empty
    szNumBytesToClear = (size_t) HALGetCMOS2Size();

    if (szNumBytesToClear != 0)
    {
        pStart = (UINT8 *) HALGetCMOS2Base();
        (void)memset( pStart, 0, szNumBytesToClear );
    }
}


/* ************************************************************************/
// FUNCTION NAME: fnNoEmdInit
// DESCRIPTION: 
//      Performs the ground-zero default init of cmos variables.  
//      First it clears out all the cmos variables that are NOT part of the 
//      cmossig, ffscmos, flhstat, or pm_cmos.
//      Then it sets any cmos variables that require a NON-ZERO default value,
//       except the ones that require a value, or other information, from the 
//       EMD.
//      After this function is called, if/when there is an EMD, then the 
//      fnWithEmdInit function will be called.
//
// INPUTS:  None
// OUTPUTS: None
// NOTES:
//  1) This currently contains a lot of unnecessary initialization to zero 
//      values, but what the hey!   
//  2) There is a long comment near the top of this file that explains
//     what needs to be done when adding a cmos variable.  Please refer
//     to that if you are unsure.
//  3) This is different than Janus.
//     For Janus the fnNoEmdInit and fnWithEmdInit functions were rolled into
//     a single function so that there are fewer places we need to worry about
//     updating when adding or changing the initialization of specific cmos 
//     variables. 
//
//         To Recap for an upgrade we use
//             fnInitializeCMOS for non-EMD initialization
//             fnCMOSSecondStageInit for EMD initialization
//         For a first time initialization we use
//             fnNoEMDInit for non-EMD initialization
//             fnWithEMDInit for EMD Initialization
//
// Modification History:
// 2011.09.05   D. Kohl added init of exp_mtr_cmos.town_name_x
// 2011.08.08  Clarisa Bellamy - Add clearing of Pcn and SN in CMOSsignature
//                      since the CMOSSignature is not cleared by the ClearCMOS 
//                      routine and bad things can happen if the PCN is wrong,
//                      like the archive file being deleted before the files 
//                      are extracted from it.
// 2009.02.17  Clarisa Bellamy - Replace inline memset() with call to 
//                      fnCMOSClearCmos, which checks the hardware and clears
//                      both sections of cmos if we are on DM475 hardware.
// 11/21/2008    Jingwei,Li     Modified to initialize sbrErrorScreen and fCMOSHybridModeRevertToWOW.
// 07-31-2008    Joe Qu         Modified to add code initializing the 
//                              currency ID to 0 for Sweden
//               Joe Mozdzer    Initial version
// *************************************************************************/
void fnNoEmdInit( void )
{   /* Needed for false variables in table */

    ulong     i;
    /* for SetupParams.Factory_IPIN */
    char      pStr[MAX_IPIN_LEN+1];
    char      LocalLicense[16] = {0};
    UINT8   bHardwareMajorVersion;
    UINT8   bDontCare;

    /* Backup the current married vault number here */
    (void)memcpy( LocalLicense, marriedVaultNumber, sizeof(LocalLicense) );

    (void)memset(pStr,0,MAX_IPIN_LEN+1);

    // Clear out the sections of CMOS that will be used.  This checks the hardware
    //  for a second cmos section (DM475) and clears that out too.  
    fnCMOSClearCmos();

    /* initialize batch register stuff */
    CMOSBatchData1.BatchAmountTotal = 0.0;
    CMOSBatchData1.BatchCountTotal = 0;
    CMOSBatchData1.lwSealOnlyBatchCnt = 0;

    CMOSStatus1.BatchAmtMax = 0.0;
    CMOSStatus1.BatchCntMax = 0;
    CMOSStatus1.BARollover = 0;
    CMOSStatus1.BCRollover = 0;
    CMOSStatus1.BCSealOnlyRollover = 0;
    DcapState.lwStatus = 0;
    USBBootCycle = 0;
    USBPCLogIndex = 0;

    // If we are initializing the CMOS from scratch, either for the first time,
    //  or because our CMOS is corrupted, we clear out the PCN and SN.  They
    //  will be read from the PSD in time, if there is one.
    (void)memset(CMOSSignature.bUicPcn, 0, SIZEOFUICPCN);
    (void)memset(CMOSSignature.bUicSN, 0, SIZEOFUICSN);

    /*******************CMOSMemKeys ****************************************************/
    // Clear the RAM copy, then save it to FFS. (CMOSMemKeys is in RAM, not CMOS.)
//    (void)memset( CMOSMemKeys, 0, sizeof(CMOSMemKeys) );

    /*******************CMOSSetupParams ************************************************/
    (void)memset( (void *)&CMOSSetupParams, 0, sizeof( CMOSSetupParams ) );

    fnCMOSInitLockCodeNoEmd();      // This disables and "clears" the lockcode.
    CMOSSetupParams.lwDefRefillAmount = 100;
    CMOSSetupParams.lwHighValueWarning = 100;
    CMOSSetupParams.lwLowFundsWarning = 100;
    CMOSSetupParams.lwMinDelConPostage = 100;

    //Platform owns the serial port initially
    CMOSSetupParams.SerialConnection = SERIAL_PLATFORM;
    /* PBP account number initialization */
    CMOSSetupParams.lwPBPAcctNumber = (unsigned long ) 0;

    CMOSSetupParams.ClockOffset.TimeZone = 0;
    CMOSSetupParams.ClockOffset.Drift = 0;
    CMOSSetupParams.ClockOffset.DaylightSToff = 0;
    CMOSSetupParams.ClockOffset.DaylightSTactivated = 0;
    CMOSSetupParams.ClockOffset.GMTcorrection = 0;

    CMOSSetupParams.MaxSettable          = 100000;            /* Max Settable postage */
    CMOSSetupParams.AutoAdvTime          = (unsigned long) 0;   /* Auto advance time */
    // To fix fraca 12470, Dec.30,04, David
    CMOSSetupParams.wpUserSetPresetClearTimeout    = 64;      /* mega uses this default val... don't know why */
    CMOSSetupParams.RefillMode         = REFILL_MODE_MODEM;
    CMOSSetupParams.AutoAdvLim           = (unsigned char) 0; /* Auto advance limit on days */
    CMOSSetupParams.Language             = 0;
    CMOSSetupParams.SerialPrinter      = SER_PRNT_SCALE_ONLY; /* indicates printer/scale configuratino */
    CMOSSetupParams.fUseExternalVault  = FALSE;               /* False if we are using the internal vault */
    CMOSSetupParams.fUseExternalModem  = MODEM_TYPE_SOCKET;
    CMOSSetupParams.fUseSocketModem        = TRUE;        /* TRUE = Socket */
    CMOSSetupParams.fRefillPwdEnabled  = FALSE;               /* Refill password disabled */

    //    (void)memset( (void *)CMOSSetupParams.SuperPassword, 0, (DEP_PWD_LEN+1));

    (void)sprintf(pStr,"%u",0);
    (void)memcpy(&CMOSSetupParams.FactoryIPIN[0],pStr,MAX_IPIN_LEN+1);

    (void)memset( CMOSSetupParams.bRatingDiscLvls, 0, DISCLEVELS );       /* this is an array, so don't need the "&" in front */
    (void)memset( CMOSSetupParams.bRatingIntraZips, 0, MAXINTRAZIPS );    /* this is an array, so don't need the "&" in front */

    CMOSSetupParams.bRatingClassIfNewPiece = RETAIN_RATING_DATA;
    CMOSSetupParams.bRatingDestinationIfNewPiece = CLEAR_RATING_DATA;         /* TBD should this be RETAIN_RATING_DATA (Mega) */
    CMOSSetupParams.bRatingDestinationIfNewClass = CLEAR_RATING_DATA;        

    CMOSSetupParams.bRatingIsRateManagerPresent = 0;

    // initialize EKP number to all ASCII zeros
    (void)memset(CMOSSetupParams.MailOriginEKPNumber, 0x30, MAX_EKP_LEN);
    CMOSSetupParams.MailOriginEKPNumber[MAX_EKP_LEN] = 0; // make sure null terminated in case of flash mistake

    /* 05/17/2002 Setup the default for the display of low ink warning */
    //Set the notify the first level low ink warning to false
    CMOSSetupParams.fFirstLowInkWarning       = FALSE;
    //Set the notify the Second level low ink warning to True
    CMOSSetupParams.fSecondLowInkWarning      = TRUE;
    (void)memset(pCMOSDiffTripWeight, 0, LENWEIGHT);

    /* Initialize the SealerWetnessSetting */
    CMOSSetupParams.bSealerWetnessSetting = 3;

#ifndef JANUS
    /* Only gets set if DM950 */
    fnMMCSetWaterPumpVolume(CMOSSetupParams.bSealerWetnessSetting);
#endif

    fnInitBarcodeScannerConfig(&CMOSSetupParams.sBarcodeScanner);

    /* Initialize the Blind Dialing setting */
    CMOSSetupParams.blindDialing = FALSE;

    CMOSSetupParams.bUseHttpFtpProxyDnld = PC_PROXY_USE_FTP;

    /********************CMOSDiagnostics ************************************************/

    (void)memset((char *)&CMOSDiagnostics.sLastExceptionDateTime, 0, sizeof(DATETIME));
    (void)memset((char *)&CMOSDiagnostics.sLastExceptionUploadDateTime, 0, sizeof(DATETIME));
    CMOSDiagnostics.PowerupCount   = 0;       /* Power up count */

    CMOSDiagnostics.lwOOBStepsCompleted = 0;  /* initialize to no steps completed */
    CMOSDiagnostics.wOOBFirstScreen   = 0;    /* this is the first out of box screen */
    CMOSDiagnostics.fOOBComplete = FALSE;
    CMOSDiagnostics.fOOBInkInstalled = FALSE;

    CMOSDiagnostics.fReceiptPending = FALSE;
    CMOSDiagnostics.RR_SyncOverride = 0;

    CMOSDiagnostics.fOOBIgnoreContactPB = FALSE;
    CMOSDiagnostics.ucWhyDoingOOB = 0;

    /*******************CMOSPCNParams **************************************************/
    for (i=0;i<2;i++)
    {
        CMOSPCNParams.SoftwareVersion[i]     = (unsigned char) 0; /* Software version date */
    }

    for (i=0;i<SMR_LEN+1;i++)
    {
        CMOSPCNParams.CurrSMR[i] = (char) 0;
    }

    (void)memcpy(&CMOSPCNParams.CurrSMR[0],"AAA",SMR_LEN+1);

    for (i=0;i<PSR_LEN+1;i++)
    {
        CMOSPCNParams.PsrId[i] = (char) 0;
    }

    /*******************CMOSInscrTable **************************************************/

    /* initialize the ad slogan and inscription lists */
    //    fnAIInitCmosList(&CMOSAdSloganTable, &CMOSInscrTable );
    //    fnAIInitOthGraphicsList(&CMOSOthGraphics);
    /*******************CMOSFeatureTable ************************************************/

    {
        long            n;

        /* ignore the warning 644 about pFeatureList not being initialized since it should
        have been initialized above. */
        for (n = 0; n < MAX_FEATURES; n++)
        {
            CMOSFeatureTable.fTbl[n] =  0;
        }
    }

    CMOSSetupParams.ucSerialPortStatus = ENBLD;
    /*******************CMOSInspectParams ***********************************************/
    CMOSInspectParams.NextInspectionDate = (unsigned long) 0;    /* Next inspection date */
    CMOSInspectParams.NextPOInspED       = FALSE;                /* Next inspection */

    /*******************CMOSErrorLog ****************************************************/
    for (i=0;i<ERR_LOG_SZ;i++)
    {
        (void)memset(&CMOSErrorLog.ErrorTable[i], 0, sizeof(ErrorLogEntry));
    }
    CMOSErrorLog.LogIndex = 0;    /* index at which to store the next entry */

    /*******************CMOSServiceLog **************************************************/
    for(i=0;i<SVC_LOG_SZ;i++)
    {
        CMOSServiceLog.DtTime[i]             = (unsigned long) 0;                 /*DtTime should be the first element of the struc!! */
    }

    /*******************CMOS Shadow Register Logs ********************************/
    fnClearShadowLogs( );

    /*****************CMOSPostageChangeLog **********************************************/
    fnClearPostgeChangeLog( );

    /************ CMOSDeliveryConfirmationLog **********************************/
    // FPHX is using GTS now, so we don't need this.

    /*******************CMOSMoneyParam ****************************************************/
    (void)memset( CMOSMoneyParams.DebitBalance, 0, SPARK_MONEY_SIZE );
    (void)memset( CMOSMoneyParams.CreditBalance, 0, SPARK_MONEY_SIZE );
    (void)memset( CMOSMoneyParams.RefillAmt, 0, SPARK_MONEY_SIZE );
    (void)memset( CMOSMoneyParams.DebitAmt, 0, SPARK_MONEY_SIZE );
    (void)memcpy( &CMOSMoneyParams.RefillAmt[1], &CMOSSetupParams.lwDefRefillAmount, sizeof(CMOSSetupParams.lwDefRefillAmount) );

    /*******************CMOSC6029Params ***************************************************/

    CMOSC6029Params.CreditBalanced = FALSE;
    CMOSC6029Params.DebitBalanced  = FALSE;
    CMOSC6029Params.Refilled       = FALSE;

    /* Clear DATA Capture */
    (void)memset(&cmosDcapDirectory,0,sizeof(cmosDcapDirectory));
    (void)memset(&cmosDcapCurrPeriod,0,sizeof(cmosDcapCurrPeriod));
    (void)memset(&cmosDcapParameters,0,sizeof(cmosDcapParameters));
    (void)memset(cmosDcapRuleSet,0,sizeof(cmosDcapRuleSet));                    /* this is an array, so don't need the "&" in front */
    (void)memset(cmosDcapBucketNameList,0,sizeof(cmosDcapBucketNameList));  /* this is an array, so don't need the "&" in front */
    (void)memset(cmosDcapClosedPeriod,0,sizeof(cmosDcapClosedPeriod));      /* this is an array, so don't need the "&" in front */
    (void)memset(&cmosDcapPfp,0,sizeof(cmosDcapPfp));
    (void)memset(&cmosDailyLogs,0,sizeof(cmosDailyLogs));
    (void)memset(&cmosTechData,0,sizeof(cmosTechData));
    (void)memset(&cmosAdjustableLog,0,sizeof(cmosAdjustableLog));
    (void)memset(&cmosPostdatingLog,0,sizeof(cmosPostdatingLog));
    (void)memset(&cmosDcapUicBaseLockingTbl,0,sizeof(cmosDcapUicBaseLockingTbl));

    // initialize the "who" value to an unused value
    cmosDcapCurrPeriod.chWhoCalledUpdatePostage = 0xFF;

    // initialize the flag that forces an upload due to new rates tables to say that
    // we don't need an upload because we don't yet have any rates tables.
    // if we need to do an upload, we'll do it because we have no rules.
    cmosDcapCurrPeriod.bDcapInitFlag = TRUE;

    // clear out the data capture timestamp fudge factor
    // clear the test mode
    lDcapTimestampFudge = 0;
    wBuildTestMode = 0;

    /*****************initialize the Shape Based Rating oversize error screen************/
    (void)memset(&sbrErrorScreen,0,sizeof(sbrErrorScreen));
    sbrErrorScreen.ch_BpDef = 1;
    /*****************initialize the WOW Start Key Setting*******************/
    fCMOSHybridModeRevertToWOW = TRUE;


    /*******************CMOSPrintParams *************************************************/

    /* the PMC stuff uses hardcoded defaults */
    CMOSPrintParams.lwTotalPieceCount = 0;
    CMOSPrintParams.lwPrintedEnvCount = 0;
    CMOSPrintParams.lwTapeCycleCount = 0;
    CMOSPrintParams.lwSealOnlyCount = 0;
    CMOSPrintParams.lwJamCycleCount = 0;
    CMOSPrintParams.lwSkewCycleCount = 0;
    CMOSPrintParams.lwWasteTankCount = 0;
    CMOSPrintParams.fbPrintHeadReplacement = FALSE;
    CMOSPrintParams.lwWeighCount = 0;
    CMOSPrintParams.lwStripCount = 0;
    CMOSPrintParams.bSpeedToRunMachineAt = (unsigned char) PRINT_THRUPUT_LOW;
    CMOSPrintParams.bBeforePrintPurgeStrong = 0;
    CMOSPrintParams.PermitPC = 0;
    CMOSPrintParams.PinNumReg = (unsigned long) NULL; /* Pin number registers */
	CMOSPrintParams.RMarginPos = PO_HORIZ_PRINT_POS_DEFAULT;
    CMOSPrintParams.PINType  = 0;
    CMOSPrintParams.wTapeLength  = TAPE_PREFEED_UNCALIBRATED;   /* JPT changed from 'TapePrefeedTimeout' */
    CMOSPrintParams.fbTapeCalibrationActive = TRUE;

    /*-------------Printer Settings table Initialization Ends------------*/

    /*  The Accounting stuff */
    (void)memset( ( void *) &CMOSAccountLM , 0 , sizeof(CMOSAccountLM));
    (void)memset( ( void *) &CMOSAccountHeader, 0 , sizeof(CMOSAccountHeader));
    (void)memset( ( void *) &CMOSAccountingUnion, 0 , sizeof(CMOSAccountingUnion));

    /* ----------------------------------------------------------------------------
    * This is commented out because it is redundant (like much of the code in this function.)  
    * Since the beginning of this function sets the entire BSScmos and BSScmos_Section2 
    *  sections to zeros, the only additional initialization that needs to be done
    *  is for NON-ZERO values.  (Including any doubles that are initialized to 0.)
    *
    * // JY, WOW. Init the Weight Break Table array in the additional area of CMOS based on the hardware.
    * {
    *    UINT8 bMajorHwVer, bMinorHwVer;   //JY, WOW. For get the hardware version.
    *    fnGetHardwareVersion(&bMajorHwVer, &bMinorHwVer); //Don't really care about bMinorHwVer at this point.
    *    if (bMajorHwVer == FP_MAIN_BD_HW_VERSION2)    //New board have externed CMOS for a full Weight Break table.
    *    {
    *        memset(asWB_Buffer_Full, 0, sizeof(XB_WEIGHT_BREAK) * WB_BUFF_SZ_FULL);
    *    } 
    * }
    *------------------------------------------------------------------------------------*/

    fnInitSuperPasswordNoEMD();

    // Initialize Germany IBI parameters
    (void)memset(CMOS_DpServiceId, 0, sizeof(CMOS_DpServiceId));

    // Initialize the picklist file headers
    for (i = 0; i < NUM_OF_CMOS_PICKLIST_HEADERS; i++)
    {
        picklist_file_headers[i].status = PL_STATUS_UNUSED;
    }
	
    // clear the Key-In-Postage password
    (void)memset( ( void *) &CMOSKIPPaswordInfo, 0 , sizeof(CMOSKIPPaswordInfo));

    /* Retrieve the married vault number here */
    (void)memcpy(marriedVaultNumber, LocalLicense, sizeof(LocalLicense));

    // Initialize ZWZP variable
    CMOSZwzpParams.timeout = ZWZP_DEFAULT_DISPLAY_TIMEOUT;  // 60 seconds as default

    /* Set power failure incident default to FALSE (print completed) */
    CMOSpfDebitLogRec.pfPmPrintStatePtr = (uchar *)((UINT32)HALGetPM_CMOSBase() + PM_CMOS_PRINT_STATUS_OFFSET);
    *CMOSpfDebitLogRec.pfPmPrintStatePtr = PF_TRANSPORT_STOP;

    /* Initialize Final Frankit Record flag */
    fCMOSGuardedFFRecXferInProgress = FALSE;
    bCMOSConnectionMode = FALSE;

    /* Initialize Firewall Ping setting variable */
    bCMOSLANFirewallPingEnabled = LAN_FW_PING_ENABLE;

    // clear out the min print fudge factor
    wMinPrintMargin = 0;

    /* SW/Rates/Graphics Download control buffer */
    (void)memset( ( void *) &CmosDownloadCtrlBuf, 0 , sizeof(CmosDownloadCtrlBuf));
    fCMOSShowRateActive = 0;
    fCMOSShowRateEffective = 0;
    fCMOSUserDenyRateActivePrompt = FALSE;
    fCMOSUserDenyRateEffectivePrompt = FALSE;
    fCMOSDateAdvUseNewRates = FALSE;

    /* Web vis CMOS last 10 Uploads log */
    (void)memset( CMOSWebVisDataUploadLog, 0, sizeof(CMOSWebVisDataUploadLog) );

    /************************************************************************************************/

    /* Initialize Normal Preset Defaults if not preserving the CMOS*/

    // now done in fnInitializeNormalPreset(), called from fnevCometPowerUpComplete
    // which is found in oipresetc.c

    /************************************************************************************************/
    /* This was the original Mega logic but it is incorrect because it causes the full 
    *   no-emd init to run everytime we power up until an EMD is loaded: */ 
    /*  CMOSSignature.Version = fNoEmdInit ? 0 : CMOS_VER; */   
    CMOSSignature.Version = CMOSVERCLAMPVALUE;
    CMOSSignature.ulExtVersion = CMOS_VER;
    CMOSSignature.fCacheEnabled = TRUE;

#ifndef TARGET_FLASH                                            /* development environment......        */
//  devOverideFlags.vltOveride = REAL_CARD;
//  devOverideFlags.headOveride = REAL_HEAD;
#endif

    /* Someone who knows what they are doing needs to toggle this flag.
        For use in emulators and software upgrades. */
    CMOSPreserve.lwPreserveCMOS = DONT_PRESERVE_CMOS;

    /* ---this section used to be buried in a call to fnInitializeExtendedCMOS. that separate
        function serves no useful purpose anymore since we don't share this code with Spark.--- */

    //    Redundant - sjp.  By the time we get to this point, this byte has already
    //    been correctly set by a call to fnCMOSInitLockCodeNoEmd or fnCMOSInitLockCodeWithEmd.
    //    Lock code data should not be accessed directly except in suprcode.c file.
    //  fCMOSLockCodeEnable = FALSE;

    fCMOSAutoScaleEnable = TRUE;
    fCMOSScaleLocationCodeInited = FALSE;   /* Default is NOT "inited" */
    bCMOSScaleLocationCode = 16;         /* Default code for Shelton Location JPM*/

    fCMOSInfraUpdateReq = FALSE;
    fCMOSPresetRatesUpdated = FALSE;
    fCMOSIgnoreReverseRateParam = FALSE;

    /*
    * 07/17/2001 Joseph P. Tokarski - Added section for initialization of the MCS Error Log
    *                                   section in the Extended CMOS Parameters area.
    *
    * 01/08/2002 Joseph P. Tokarski - Converted from MCS Error Log to the System Error Log.
    */
    /*******************CMOSSystemErrorLog ****************************************************/
    for (i=0;i<SYS_ERR_LOG_SZ;i++)
    {
        (void)memset(&CMOSSystemErrorLog.SystemErrorTable[i], 0, sizeof(SystemErrorLogEntry));
    }
    CMOSSystemErrorLog.LogIndex = 0;  /* index at which to store the next entry */

    /******************CMOS Local Phone Number *********************************************/
    CMOSLocalPhoneNumber[0] = 0;  /* NULL terminate the local phone number for now. */

    /******************CMOS PBP Balance & Refill Data****************************************/
    dblCMOSPbpDebitBal = dblCMOSPbpPrevDebitBal = dblCMOSPbpCreditBal = dblCMOSPbpPrevCreditBal = 0.0;
    dblCMOSPbpRefillAmt = 0.0;

    /********** Base PCN Log ******************************************************************/
    CMOSSysBasePCNLog.bCurrent = 0;
    CMOSSysBasePCNLog.bCount = 0;
    for (i = 0; i < SYS_BASE_PCN_LOG_SZ; i++)
    {
        (void)memset(&(CMOSSysBasePCNLog.sLog[i].bPCN[0]),0,SYS_BASE_PCN_LOG_ITEM_SZ);
        CMOSSysBasePCNLog.sLog[i].bPCNLength = 0;
    }

    /********** Printed Indica Number & Indicia Language Code ********************************/
    (void)memset(&CMOSPrintedIndiciaNum[0],0,PRINTED_INDICIA_NUM_SZ);
    (void)memset(&CMOSIndiciaLanguageCode[0],0,INDICIA_LANGUAGE_CODE_SZ);

    (void)memset(&CMOSCustomerID_RRN[0],0,CUSTOMERID_RRN_SZ);

	(void)memset(&CMOSTextAdArray, 0, sizeof(CMOSTextAdArray));
	CMOSTextAdArray.bMaxTextAdsByPcn = MAX_TEXT_ADS;

	(void)memset(&CMOSTextEntryArray, 0, sizeof(CMOSTextEntryArray));
	CMOSTextEntryArray.bMaxTextMsgsByPcn = MAX_TEXT_ENTRY;

	(void)memset(&CMOSPermitArray, 0, sizeof(CMOSPermitArray));
	CMOSPermitArray.bIndex = 0xFF;
	CMOSPermitArray.bMaxPermitsByPcn = MAX_PERMITS;

    /*
    * the following initialization is done to set the special service (delcon)
    * MasterUploadRecordID to 0.  It will be used as a unique ID for each records
    * - VS
    * THis is obsolete now that we use GTS instead of delcon.
    */
    //  MasterUploadRecordId = 0;

  //    fnInitNetConfigForTest();

	// first clear the entire proxy structure, then set the items
	// that need specific values.
/* remove proxy setup from cmos, now handled by tablet...
    (void)memset(&globalProxyConfig, 0x00, sizeof(globalProxyConfig));
						
    globalProxyConfig.fEnabled = FALSE;
    globalProxyConfig.Http.uwPort = 80;
    globalProxyConfig.Https.uwPort = 80;
*/
    /* everything in this section is part of the update loop for Mega.  in Janus, it can
        be part of the ground-zero init because these variables already exist prior to
        any release that supports cmos preservation. */
    (void)memset(&CMOSFilesToDownload, 0, sizeof(CMOSFilesToDownload));

    //    CMOSSetupParams.wSleepTimeout = fNoEmdInit ? 0 : fnFlashGetWordParm(WP_FACTORY_SLEEP_TIMEOUT);
    CMOSSetupParams.wSleepTimeout = 0;    /* no sleep mode support until after 4.30 release */

    CMOSSetupParams.RollerTimeOut           = DEFAULT_FEEDER_ROLLER_TIMEOUT_VALUE;
    CMOSSetupParams.ulMailInactivityTimeout   = DEFAULT_FEEDER_INACTIVITY_TIMEOUT_VALUE;

    //Set the notify the first level low ink warning to false
    CMOSSetupParams.fFirstLowInkWarning       = FALSE;
	
    //Set the notify the Second level low ink warning to True
    CMOSSetupParams.fSecondLowInkWarning      = TRUE;
    (void)memset(pCMOSDiffTripWeight, 0, LENWEIGHT);

    CMOSAjout.bAjoutPrintModeForPWR = PMODE_AJOUT_PLATFORM;

    /* Set the default Piece ID count to 0 */
    CMOSPieceID= 0;

    /* Set the default DH On time to 5pm, 61200(17*3600) */
    CMOSLongDHOnTime = 61200;

    /* Init Country specific archive file registry variable*/
    (void)memset((void *)PctyArchiveRegistry, 0 ,sizeof(CTY_ARCHIVE_PROPS) * (TOTAL_CTY_ARCHIVES));
      
    // Init the default last battery warning display date to 01/01/2003,
    // and clear PSD battery test date.
    (void)memset(&CMOSLastBatteryDisplay, 0, sizeof(DATETIME));
    CMOSLastBatteryDisplay.bCentury = 20;
    CMOSLastBatteryDisplay.bYear = 3;
    CMOSLastBatteryDisplay.bMonth = 1;
    CMOSLastBatteryDisplay.bDay = 1;
    (void)memset(&CMOSPsdBatteryTestDate, 0, sizeof(DATETIME));

    /* end of section that maps to Mega's update loop */

//    TurnCacheOn();

    CMOSExtUpdatedToVersion =  0;

    // initialize the currency ID for possible Euro conversion processing.
    cmosEuroConversion.ucCurrencyID = 0;

    (void)memset(&CMOSLastLocalMidnightDT, 0, sizeof(DATETIME));
    (void)memset(&CMOSLastDCAPPeriodCheckDT, 0, sizeof(DATETIME));

    // Initialize the Refill password.
    memset( CMOSRefillCode, 0, sizeof(CMOSRefillCode) );

    memset(exp_mtr_cmos.town_name_3, 0, sizeof(UNICHAR)*ITALY_TOWN_NAME_3_SIZE_BUF);
    memset(exp_mtr_cmos.town_name_4, 0, sizeof(UNICHAR)*ITALY_TOWN_NAME_4_SIZE_BUF);

	// default to the filtering being off.
	CMOSPlatformFilteringOn = FALSE;

    /* set the CMOS signature so that we mark it as being initialized -
        this should always be the last line of code in this function.*/
    CMOSSignature.pInitSignature[0] = 0x3771;
    CMOSSignature.pInitSignature[1] = 0x3317;
    CMOSSignature.pInitSignature[2] = 0x3441;
    CMOSSignature.pInitSignature[3] = 0xABCD;
}

/*************************************************************************
 // FUNCTION NAME: fnCMOSCheckNumRatesActive
 // DESCRIPTION: Check the number of already activated Rates and DCAP Module. 
 // AUTHOR: Liu Jupeng
 //
 // INPUTS: none
 //
 // OUTPUTS:    none
 // MODIFICATION HISTORY:
 // 2014-03-27     Renhao   Merged code from juns_tips.
 // *************************************************************************/

void fnCMOSCheckNumRatesActive(void)
{
    unsigned char ucInx;


    bNumPreviouslyActiveRates = 0;

    for (ucInx = 0; ucInx <MAX_CMOS_RATE_COMPONENTS; ucInx++)
    {
        if ((CMOSRateCompsMap.CMOSRateComponents[ucInx].SWPartNO[0] != 0) &&
            ((CMOSRateCompsMap.CMOSRateComponents[ucInx].DeviceID == RATES_DATA_COMPONENT)||
            (CMOSRateCompsMap.CMOSRateComponents[ucInx].DeviceID == DCAP_DATA_COMPONENT)||
            (CMOSRateCompsMap.CMOSRateComponents[ucInx].DeviceID == COMPRESSED_DCAP_DATA_COMPONENT)) &&
            (CMOSRateCompsMap.CMOSRateComponents[ucInx].bRateComponentActive == TRUE))
        {
			bNumPreviouslyActiveRates++;
		}
	}
}
/*************************************************************************
 // FUNCTION NAME: fnCMOSCheckNumofActiveRates
 // DESCRIPTION: Check the number of already activated Rates. 
 // AUTHOR: Renhao
 //
 // INPUTS: none
 //
 // OUTPUTS:    none
 // MODIFICATION HISTORY:
 // 2014-05-05     Renhao   Initial version.
 // *************************************************************************/

void fnCMOSCheckNumofActiveRates(void)
{
    unsigned char ucInx;


    bNumOfActiveRates= 0;

    for (ucInx = 0; ucInx <MAX_CMOS_RATE_COMPONENTS; ucInx++)
    {
        if ((CMOSRateCompsMap.CMOSRateComponents[ucInx].SWPartNO[0] != 0) &&
            (CMOSRateCompsMap.CMOSRateComponents[ucInx].DeviceID == RATES_DATA_COMPONENT) &&
            (CMOSRateCompsMap.CMOSRateComponents[ucInx].bRateComponentActive == TRUE))
        {
			bNumOfActiveRates++;
		}
	}
}
/*************************************************************************
 // FUNCTION NAME: fnCMOSCheckNumofActiveDcap
 // DESCRIPTION: Check the number of already activated Dcap Module. 
 // AUTHOR: Renhao
 //
 // INPUTS: none
 //
 // OUTPUTS:    none
 // MODIFICATION HISTORY:
 // 2014-05-05     Renhao   Initial version.
 // *************************************************************************/

void fnCMOSCheckNumofActiveDcap(void)
{
    unsigned char ucInx;


    bNumOfActiveDcaps= 0;

    for (ucInx = 0; ucInx <MAX_CMOS_RATE_COMPONENTS; ucInx++)
    {
        if ((CMOSRateCompsMap.CMOSRateComponents[ucInx].SWPartNO[0] != 0) &&
            ((CMOSRateCompsMap.CMOSRateComponents[ucInx].DeviceID == DCAP_DATA_COMPONENT)||
            (CMOSRateCompsMap.CMOSRateComponents[ucInx].DeviceID == COMPRESSED_DCAP_DATA_COMPONENT)) &&
            (CMOSRateCompsMap.CMOSRateComponents[ucInx].bRateComponentActive == TRUE))
        {
			bNumOfActiveDcaps++;
		}
	}
}

/*************************************************************************
 // FUNCTION NAME: fnOldInitializeCMOS
 // DESCRIPTION: Initializes all CMOS variables to either factory defaults
 //             stored in flash or hardcoded defaults.  This is a ground-
 //             zero init that reinitializes everything.
 // AUTHOR: Joe Mozdzer
 //
 // INPUTS: none
 //
 // OUTPUTS:    none
 // *************************************************************************/
void fnOldInitializeCMOS( void )
{
    // We always call NoEmdInit and call With Emd Init
    // if we do have a valid EMD
    fnNoEmdInit();
    if(NoEMDPresent == FALSE)
        fnWithEmdInit();
}

/*************************************************************************
 // FUNCTION NAME: fnInitNetConfigForTest
 // DESCRIPTION: Initializes CMOSNetworkConfig structure for connecting
 //             to test infrastructure.
 // AUTHOR: Joe Mozdzer
 //
 // INPUTS: bInitType (see cmos.h)- indicates which kind of init to do
 //
 // OUTPUTS:    none
 // *************************************************************************/
void fnInitNetConfigForTest(unsigned char bInitType)
{
	char *	pRetStr;


    if (bInitType == NET_INIT_EMD)
    {
        initNewNetworkParameters();
    }
    else
    {
        if (bInitType == NET_INIT_QA)
        {
            (void)strcpy( CMOSNetworkConfig.distributorUrl, cQaDistributor );
        }
		else if (bInitType == NET_INIT_SECAP)
		{
			(void)strcpy( CMOSNetworkConfig.distributorUrl, cSecapQaDistributor );
		}
        else if (bInitType == NET_INIT_BACKUP)
        {
			// The following is mainly just for France, so it probably won't work for other countries.
			// Some of it may work in QA, but not in Production, or vice versa.
			// It also may not work at all for France if the URLs have been changed since the code was written.
            CMOSNetworkConfig.distributorUrl[0] = 0;
		    (void)memset(CMOSNetworkConfig.backupPbpUrl, '\0', sizeof(CMOSNetworkConfig.backupPbpUrl));
			pRetStr = (char *) fnFlashGetAsciiStringParm(ASP_PBP_SERVER_HOST_NAME);
			if(pRetStr)
				(void)strncpy(CMOSNetworkConfig.backupPbpUrl, pRetStr, sizeof(CMOSNetworkConfig.backupPbpUrl) - 1);

            (void)strcpy( CMOSNetworkConfig.backupAcntUrl, "https://distserv.pb.com/AcctService/Default.aspx");
            (void)strcpy( CMOSNetworkConfig.backupDlaUrl, "http://pbdlst1.pb.com/PrdUpdate.dll");
        }

		(void)memset(CMOSNetworkConfig.primaryDnsServer, '\0', sizeof(CMOSNetworkConfig.primaryDnsServer));
		pRetStr = (char *) fnFlashGetAsciiStringParm(ASP_QA_PRIMARY_DNS_SERVER);
		if(pRetStr)
			(void)strncpy(CMOSNetworkConfig.primaryDnsServer, pRetStr, sizeof(CMOSNetworkConfig.primaryDnsServer) - 1);

		(void)memset(CMOSNetworkConfig.secondaryDnsServer, '\0', sizeof(CMOSNetworkConfig.secondaryDnsServer));
		pRetStr = (char *) fnFlashGetAsciiStringParm(ASP_QA_SECONDARY_DNS_SERVER);
		if(pRetStr)
			(void)strncpy(CMOSNetworkConfig.secondaryDnsServer, pRetStr, sizeof(CMOSNetworkConfig.secondaryDnsServer) - 1);

        (void)strcpy( CMOSNetworkConfig.attLoginAccountAndUserId, cNonProdAttLogin );
        (void)strcpy( CMOSNetworkConfig.attPassword, cNonProdAttPassword );
        (void)strcpy( CMOSNetworkConfig.attAniServerIp, cNonProdAttAniServerIp );
        (void)strcpy( CMOSNetworkConfig.attAniServerPortNumber, cNonProdAttAniServerPort );
    }
}

/* **********************************************************************
// FUNCTION NAME:       PerformAnyPowerFailRecovery()
// PURPOSE: Any modules that perform their own power fail
// recovery routines place the call here. This routine is
// called after every power up. If a cmos update is required
// this gets called AFTER the preserve and cmos init stuff
// this allows us to place state information in the cmos
// to handle power fail recovery conditions
//
// AUTHOR: George Monroe
//
// INPUTS: none
// RETURNS: none
// **********************************************************************/
void PerformAnyPowerFailRecovery( void )
{

// Nucleus file system power fail recovery should be done before this point

}


// **********************************************************************
// FUNCTION NAME: fnCMOSSecondStageInit()
// PURPOSE: This Function Is Called once a valid EMD is loaded
// Any initialization that required flash parameters can be placed
// here
//
// AUTHOR: George Monroe
//
// INPUTS:
// NOTE:  There is a long comment near the top of this file that explains
//          what needs to be done when adding a cmos variable.  Please refer
//          to that if you are unsure.
 //         To Recap for an upgrade we use
 //             fnInitializeCMOS for non-EMD initialization
 //             fnCMOSSecondStageInit for EMD initialization
 //         For a first time initialization we use
 //             fnNoEMDInit for non-EMD initialization
 //             fnWithEMDInit for EMD Initialization
//
// MODIFICATION HISTORY:
//   10/29/2008 Jingwei,Li     Set Batch clearing requires password from EMD.
// **********************************************************************/
void fnCMOSSecondStageInit()
{
    unsigned char *pFeatureList;
    unsigned char bInx;
    UINT16  uwTempWord;     // Place to put a 16-bit EMD parameter temporarily.


	if ((CMOSExtUpdatedToVersion == 0) || (CMOSExtUpdatedToVersion < CMOSSignature.ulExtVersion))
	{
		// indicate that we could be awhile
	    fnLCDPutStringDirect((unsigned char *)"CMOS........... RE-CHKED  ", 0, 1, LCD_CHARACTER_NORMAL);

		// Wait to give the screen time to update
		OSWakeAfter(100);
	}

    if(CMOSExtUpdatedToVersion == 0 )
    {
        fnWithEmdInit();
    }
    else
    {
        // we do several updates based on the feature table, so get the pointer just once.
        pFeatureList = fnFlashGetPackedByteParm(PBP_FEATURE_LIST);

        /* this loop contains the WITH EMD initialization for newly added variables */
        while(CMOSExtUpdatedToVersion < CMOSSignature.ulExtVersion )
        {
            switch(CMOSExtUpdatedToVersion)
            {
                case 0x190:
                    {
                        /* Version 0x191 added new feature codes, set these to the factory defaults from flash. 
                             (OLD MAX_FEATURES = 41, so first new index = 41, NEW MAX_FEATURES = 55) */
                        for (bInx = 41; bInx < MAX_FEATURES; bInx++)
                        {
                            CMOSFeatureTable.fTbl[bInx] = (unsigned char) *(pFeatureList + bInx);
                        }

                        cmosDcapParameters.ucSaveDailyRegistersType = fnFlashGetByteParm( BP_DCAP_SAVE_DAILY_REGISTERS_TYPE );
                        cmosDcapParameters.ucMaintainTechSuperData = fnFlashGetByteParm( BP_DCAP_MAINTAIN_TECH_SUPERVISION_DATA );
         
                        // update the adjustable log from the old number of actually used entries (old value for ADJUSTABLE_LOG_ENTRIES : 61)
                        // to the new number of actually used entries.
                        // do it here just in case the function wants to use some EMD parameters.
                        //TODO JAH remove dcperiod.c fnDCAP_PM_ConvertAdjustableLog( ADJ_LOG_OLD_SIZE_1, ADJ_LOG_NO_PREVIOUS_CHANGES );
                    }
                    break;

                case 0x196:
                    {
                        // Now we actually support IBI-Lite features, so update with the
                        // values from the possibly new EMD
                        CMOSFeatureTable.fTbl[IBI_LITE_INX] = (unsigned char) pFeatureList[IBI_LITE_INX];
                        CMOSFeatureTable.fTbl[IBI_ULTRA_LITE_INX] = (unsigned char) pFeatureList[IBI_ULTRA_LITE_INX];

                        // Init the new ucMeterStampType variable in the presets
                        for (bInx = 0; bInx < MKEY_TABLE_SZ; bInx++)
                        {
//                            CMOSMemKeys[bInx].ucMeterStampType = NORMAL_INDICIA_SETTING;
                        }

                    }
                    break;
                
                case 0x242:
                    {
                        CMOSFeatureTable.fTbl[IBI_CONFIRM_INX] = (unsigned char) pFeatureList[IBI_CONFIRM_INX];
                        CMOSFeatureTable.fTbl[LOW_WOW_THROUGHPUT_SPEED_INX] = (unsigned char) pFeatureList[LOW_WOW_THROUGHPUT_SPEED_INX];
                        CMOSFeatureTable.fTbl[MED_WOW_THROUGHPUT_SPEED_INX] = (unsigned char) pFeatureList[MED_WOW_THROUGHPUT_SPEED_INX];
                        CMOSFeatureTable.fTbl[HI_WOW_THROUGHPUT_SPEED_INX] = (unsigned char) pFeatureList[HI_WOW_THROUGHPUT_SPEED_INX];
                    }
                    break;
                
                case 0x263:
                    {
                        // Initialize the new Auto Ads setting.
                        CMOSFeatureTable.fTbl[AUTO_AD_INX] = (unsigned char) pFeatureList[AUTO_AD_INX];
                    }
                    break;

                case 0x264:
                    {
                        // Change Adjustable Log structure to add new member, which allows us to
                        //  only upload the entries that have not been uploaded before.
                        // Do it here just in case the function wants to use some EMD parameters.
                        // Indicate that we know that the "need to upload" flag was already added.
                        //TODO JAH remove dcperiod.c fnDCAP_PM_ConvertAdjustableLog( ADJ_LOG_OLD_SIZE_2, ADJ_LOG_NEED_UPLOAD_CHANGE_DONE );
                    }
                    break;

                case 0x279:
                    {
                        // Version 280 added 2 new feature codes: 
                        //  OVERRIDE_KIP_PARAMETERS_INX  60 - THIS IS NOT USED IN G9 (but set it anyway.)
                        //  SECURE_COMM_REQUIRED_INX     61 - This is HTTPS support.
                        // Set these to the factory defaults from EMD. 
                        // The new MAX_FEATURES = 62
                        for( bInx = 60; bInx < MAX_FEATURES; bInx++ )
                            CMOSFeatureTable.fTbl[ bInx ] = pFeatureList[ bInx ];
                            
                        // Feature code for Premium Address previously existed but was 
                        //  not used, read it again from the EMD this one time.
                        CMOSFeatureTable.fTbl[ PREMIUM_ADDRESS_INX ] = pFeatureList[ PREMIUM_ADDRESS_INX ];                   
                    }
                    break;

                case 0x284:
                    {
                        // Version 285 added a new feature code: 
                        //  INTELLIGENT_INK_CARTRIDGE_INX  62 - This turns on/off non-pb ink warning screens.
                        // Set this to the factory default from EMD. 
                        // The new MAX_FEATURES = 63
                        for( bInx = INTELLIGENT_INK_CARTRIDGE_INX; bInx < MAX_FEATURES; bInx++ )
                            CMOSFeatureTable.fTbl[ bInx ] = pFeatureList[ bInx ];
                    }
                    break;
                    
                case 0x285:
                    {
                        // Version 286 increased the size of the AdjustableLog from 120 to 130.: 
                        // Update the adjustable log from the old number of actually used entries
                        // to the new number of actually used entries.
                        // Do it here just in case the function wants to use some EMD parameters.
                        // Indicate that we know that the "need to upload" flag was already added and
                        //  that the ucFirstEntry2Upload has already been updated.
                        //TODO JAH remove dcperiod.c fnDCAP_PM_ConvertAdjustableLog( ADJ_LOG_OLD_SIZE_2, ADJ_LOG_UPLOAD_INX_CHANGE_DONE );
                    }
                    break;

                case 0x28A:
                    {
                        // Version 28B added a new feature code: DLA_SECURE_DWNLD_REQUIRED_INX
                        // Set this to the factory default from EMD. 
                        // The new MAX_FEATURES = 64
                        for( bInx = DLA_SECURE_DWNLD_REQUIRED_INX; bInx < MAX_FEATURES; bInx++ )
                            CMOSFeatureTable.fTbl[ bInx ] = pFeatureList[ bInx ];
                    }
                    break;

                case 0x291:
					/* Version 292 added a new feature code, set it to the factory default from flash. 
						(first new code is for the Welsh postal logo for U.K. EIB development. new MAX_FEATURES = 65)*/
					for (bInx = WELSH_POSTAL_LOGO_INX; bInx < MAX_FEATURES; bInx++)
						CMOSFeatureTable.fTbl[bInx] = (unsigned char)pFeatureList[ bInx ];
                    break;

                default:
                    break;
            }//end of switch()

            CMOSExtUpdatedToVersion++;
        }

        // Make sure the PCN and serial number are properly nul-terminated.
        // The size of the buffers are "size" instead of "size+1", so the
        //  nul-terminator must be put at "size-1"
        CMOSSignature.bUicPcn[SIZEOFUICPCN-1] = 0;
        CMOSSignature.bUicSN[SIZEOFUICSN-1] = 0;

    } // End of NOT a virgin CMOS

    // If there is a new EMD, update things in CMOS that may have changed in the EMD
    // even though the CMOS version didn't change.
    if (NewEMDPresent == TRUE)
    {
        fnUpdateCmosFromNewEmd();
    }

    CMOSExtUpdatedToVersion = CMOSSignature.ulExtVersion;

} // End of fnCMOSSecondStageInit


/*************************************************************************
 // FUNCTION NAME:              fnInitializeCMOS
 // DESCRIPTION: Initializes all CMOS variables to either factory defaults
 //             stored in flash or hardcoded defaults.  The extended CMOS
 //             variables are also initialized if we are compiled for COMET.
// AUTHOR: Joe Mozdzer
 //
 // INPUTS:     none
 // OUTPUTS:    none
 // NOTES:   
 //  1. There is a long comment near the top of this file that explains
 //         what needs to be done when adding a cmos variable.  Please refer
 //         to that if you are unsure.
 //         To Recap for an upgrade we use
 //             fnInitializeCMOS for non-EMD initialization
 //             fnCMOSSecondStageInit for EMD initialization
 //         For a first time initialization we use
 //             fnNoEMDInit for non-EMD initialization
 //             fnWithEMDInit for EMD Initialization
 //   
 //   2. NEW CMOS variables/members need to be initialized only if they are doubles,
 //      or are being initilized to a non-zero value. 
 //     If you care why, read on: 
 //      When CMOS is "preserved", a section of RAM the size of the preserved 
 //      CMOS area is cleared.  Then cmos variables that previously existed are 
 //      copied from their existing(previous) offsets in CMOS to their offsets 
 //      in the RAM area.  Then the RAM area is copied to a file in FFS (in case 
 //      there is a power fail while overwriting the old CMOS), then the new RAM 
 //      area is copied over the existing CMOS area.
 //
// Modification History:
// 2011.08.08 Clarisa Bellamy - Only call fnCMOSCleanUpRmAndRates here if initing 
//                        CMOS from scratch.  Otherwise, it is called from
//                        fnCMOSSecondStageInit.
// 2011.08.08 Clarisa Bellamy - Comment out old test code that is not used anymore.
//                      - Add syslog entry if FFS is not initialized yet, and if 
//                        cmos version is going backwards.
//                      - Add clearing of PCN and SN in CMOSSignature if the CMOS
//                        needs to be reset from scratch.
//                      - Remove the saving of the marriedVaultNumber before the 
//                        CMOSPreserve, because 1) It is not used here, and 2) there 
//                        is no way to know if it is located in the same spot.
//                      - Removed stack variables that are no longer used.
//                      - Since same thing is done whether starting from version 0 or 
//                        going backwards, consolidate the code.
// 11/21/2008    Jingwei,Li     Initialize CMOS variables pCMOSWOWWeightLimit,sbrErrorScreen                                     
//                                          and  fCMOSHybridModeRevertToWOW.
 // *************************************************************************/
void fnInitializeCMOS( void )
{
    UINT8   bInx;           // Index for loops < 255.
    UINT8   ubMajorHwVer;   // Used to determine Display type (color or B&W)
    BOOL    fVersionBackingUp = FALSE;  // Set if new CMOS_VER is lower than existing cmos version.
    BOOL    fVersionZero = FALSE;   // Set if the existing cmos version is 0.

    // Check if the current CMOS_VER is 0 or higher than the new CMOS_VER,
    if( CMOSSignature.ulExtVersion == 0 )
    {
        fVersionZero = TRUE;
    }
    else if( CMOSSignature.ulExtVersion > CMOS_VER )
    {
        fnDumpStringToSystemLog( "CMOS_VER is backing up. " );         
        fVersionBackingUp = TRUE;
    }

    // If the current CMOS_VER is 0 or higher than the new CMOS_VER, 
    //  then re-initialize CMOS from scratch.
    if( (fVersionZero == TRUE) || (fVersionBackingUp == TRUE) )
    {
    	dbgTrace(DBG_LVL_INFO,"Complete CMOS Init");
        // Re-init CMOS from scratch.
        //  - First, clear out the PCN & serial number because we could be
        //    building to a different PCN and/or serial number and we don't 
        //    want to delete the wrong files from FFS when the clean-up is 
        //    done in the second stage of CMOS init.
        CMOSSignature.bUicPcn[0] = 0;
        CMOSSignature.bUicSN[0] = 0;

        fnOldInitializeCMOS();

        // set network defaults
        InitializeNetworkDefaults();

    }
    else    // We have a non-zero CMOS version that is <= new CMOS_VER.
    {
    	dbgTrace(DBG_LVL_ERROR,"Error: CMOS Init from old version: %d", CMOSSignature.ulExtVersion);

        /* Now we need to handle when we get updated */
        /* Basically we will run through each version number until we hit this one
        We hard code the version number prior to this version. Since we know in
        what version we added the new variables we can tell if we need to do this.
        We hard code because CMOS_VER gets bumped with each change
        */
        CMOSExtUpdatedToVersion = CMOSSignature.ulExtVersion;

        // These are done based on the layout version within the logs because
        //  different branches are adding them at different CMOS versions.
        fnCMOSShadowUpgradeDebit();
        fnCMOSShadowUpgradeRefill();

        // set the CMOS signature so that we mark it as being initialized
        CMOSSignature.pInitSignature[0] = 0x3771;
        CMOSSignature.pInitSignature[1] = 0x3317;
        CMOSSignature.pInitSignature[2] = 0x3441;
        CMOSSignature.pInitSignature[3] = 0xABCD;
        CMOSSignature.ulExtVersion = CMOS_VER;
        CMOSSignature.Version = CMOSVERCLAMPVALUE;
    }
    // If any system errors were logged (in ram) before the CMOS preservation is complete, 
    //  go ahead and relog them (into CMOS) now.
    fnUtilReLogEarlyErrors();
}


/* **********************************************************************
// FUNCTION NAME:fnPrepareForSoftwareUpdate()
// PURPOSE: Perform any steps needed before a software update
//
// AUTHOR: George Monroe
//
// INPUTS: void
// RETURNS: void
// **********************************************************************/
void fnPrepareForSoftwareUpdate(void)
{
  //BCH JANUS  fnAISaveDownloadedGraphicst(&CMOSAdSloganTable, &CMOSInscrTable , CMOSOthGraphics );
}




// **********************************************************************
// FUNCTION NAME: fnCheckIfCMOSInitialized
// DESCRIPTION: Checks if the CMOS has been initialized by looking at the
//              signature.
// AUTHOR: George Monroe
//
// INPUTS:  none
// OUTPUTS: none
// HISTORY:
// 2011.08.08 Clarisa Bellamy - Look for saved graphics BEFORE deleting the 
//                      update file instead of after.
// *************************************************************************/
BOOL fnCheckIfCMOSInitialized(void)
{
    BOOL status;

    if ((CMOSSignature.pInitSignature[0] == 0x3771) &&
        (CMOSSignature.pInitSignature[1] == 0x3317) &&
        (CMOSSignature.pInitSignature[2] == 0x3441) &&
        (CMOSSignature.pInitSignature[3] == 0xABCD) &&
        (CMOSSignature.ulExtVersion == CMOS_VER))
    {
        status = TRUE;
    }
    else
    {
        status = FALSE;
    }

    // If no CMOS Update is required, we can perform the power fail recovery step here.
    // else, we must wait until any cmos variables used to track the power fail have been
    // moved.
    if( status == TRUE )
    {
        PerformAnyPowerFailRecovery();
    }

	// If we're running in the app, any update of the software image should be completed by
	// now, so make sure the state is no longer set to update the software image
	CMOSSignature.fState = RUNIMAGE;
	

    return(status);
}

/**********************************************************************************/
/*                       CMOS WRAPPER FUNCTIONS                                   */
/* unsigned char fnCMOSGetByteParm(unsigned short wParmID);                       */
/* unsigned short fnCMOSGetWordParm(unsigned short wParmID);                      */
/* unsigned long fnCMOSGetLongParm(unsigned short wParmID);                       */
/* unsigned char * fnCMOSGetAsciiStringParm(unsigned short wParmID);              */
/* double * fnCMOSGetDoubleParm(unsigned short wParmID);                          */
/* unsigned short * fnCMOSGetUnicodeStringParm(unsigned short wParmID);           */
/* unsigned char * fnCMOSGetPackedByteParm(unsigned short   wParmID);             */
/* unsigned char * fnFlashGetMoneyParm(unsigned short wParmID);                   */
/*                                                                                */
/* AS IN THE FLASH, THE CMOS WRAPPERS DO NOT DO ERROR CHECKING. THEY DO NOT CHECK */
/* IF THE VARID FALLS WITHIN THE VALID RANGE. THEY DO NOT CHECK IF THERE IS A     */
/* A DUMMY VARIABLE IN THE TABLE THAT HAS BEEN REFERENCED INADVERTANTLY.THE ERROR */
/* CHECKING IS LEFT TO THE USER.                                                  */
/*                                                                                */
/*                                                                                */
/*                                                                                */
/* Remember copy the unsigned char * or unsigned short *  of the CMOS address     */
/* into a local parameter so that you modify the local parameter, not the CMOS    */
/* address.                                                                       */
/*                                                                                */
/**********************************************************************************/




/********************************************************************************/
/* TITLE: fnfGetCMOSByteParam                                                   */
/* AUTHOR: BMP                                                                  */
/* INPUT:  VARID is passed as a parameter into the CMOS Table                   */
/* DESCRIPTION: Retrieve address of structure of Byte Parameter from CMOS       */
/* OUTPUT: Return unsigned char or error if range error or if dummy_var         */
/********************************************************************************/
unsigned char fnfGetCMOSByteParam(unsigned short wVarID)
{
    if (wVarID & EXTENDED_CMOS_ID_MASK)
    {
        wVarID &= ~EXTENDED_CMOS_ID_MASK;
        return (*(CMOSExtendedByteTable[wVarID].pCMOS_BP));
    }
    else
        return(*(CMOSByteTable[wVarID].pCMOS_BP));
}

/********************************************************************************/
/* TITLE: fnfSetCMOSByteParam                                                   */
/* AUTHOR: BMP                                                                  */
/* INPUT:  VARID is passed as a parameter into the CMOS Table                   */
/*          bValue is the value to set the CMOS variable to                     */
/* DESCRIPTION: Sets CMOS byte parameter                                        */
/* OUTPUT: Returns successful.  If we decide to do error checking in these      */
/*          routines it can return successful/failure.                          */
/********************************************************************************/
BOOL fnfSetCMOSByteParam(unsigned short wVarID, unsigned char bValue)
{
    if (wVarID & EXTENDED_CMOS_ID_MASK)
    {
        wVarID &= ~EXTENDED_CMOS_ID_MASK;
        *(CMOSExtendedByteTable[wVarID].pCMOS_BP) = bValue;
    }
    else
        *(CMOSByteTable[wVarID].pCMOS_BP) = bValue;
    return (SUCCESSFUL);
}


/********************************************************************************/
/* TITLE: fnfGetCMOSWordParam                                                   */
/* AUTHOR: BMP                                                                  */
/* INPUT:  VARID is passed as a parameter into the CMOS Table                   */
/* DESCRIPTION: Retrieve address of structure of word Parameter from CMOS table */
/* OUTPUT: Return unsigned short or DUMMYVar or error                           */
/********************************************************************************/
unsigned short fnfGetCMOSWordParam( unsigned short wVarID)
{
    if (wVarID & EXTENDED_CMOS_ID_MASK)
    {
        wVarID &= ~EXTENDED_CMOS_ID_MASK;
        return (*(CMOSExtendedWordTable[wVarID].psCMOS_WORDPARM));
    }
    else
        return(*(CMOSWORDTable[wVarID].psCMOS_WORDPARM));

}

/********************************************************************************/
/* TITLE: fnfSetCMOSWordParam                                                   */
/* AUTHOR: BMP                                                                  */
/* INPUT:  VARID is passed as a parameter into the CMOS Table                   */
/*          wValue is the value to set the CMOS variable to                     */
/* DESCRIPTION: Sets CMOS word parameter                                        */
/* OUTPUT: Returns successful.  If we decide to do error checking in these      */
/*          routines it can return successful/failure.                          */
/********************************************************************************/
BOOL fnfSetCMOSWordParam(unsigned short wVarID, unsigned short wValue)
{
    if (wVarID & EXTENDED_CMOS_ID_MASK)
    {
        wVarID &= ~EXTENDED_CMOS_ID_MASK;
        *(CMOSExtendedWordTable[wVarID].psCMOS_WORDPARM) = wValue;
    }
    else
        *(CMOSWORDTable[wVarID].psCMOS_WORDPARM) = wValue;
    return (SUCCESSFUL);
}


/********************************************************************************/
/* TITLE: fnfGetCMOSLongParam                                                   */
/* AUTHOR: BMP                                                                  */
/* INPUT:  VARID is passed as a parameter into the CMOS Table                   */
/* DESCRIPTION: Retrieve address of structure of long Parameter from CMOS table */
/* OUTPUT: Return unsigned long or DUMMYVAR or error                            */
/********************************************************************************/
unsigned long fnfGetCMOSLongParam(unsigned short wVarID)
{
    if (wVarID & EXTENDED_CMOS_ID_MASK)
    {
        wVarID &= ~EXTENDED_CMOS_ID_MASK;
        return (*(CMOSExtendedLongTable[wVarID].pCMOS_LONGPARM));
    }
    else
        return(*(CMOSLONGTable[wVarID].pCMOS_LONGPARM));

}


/********************************************************************************/
/* TITLE: fnfGetCMOSDoubleParam                                                 */
/* AUTHOR: BMP                                                                  */
/* INPUT: VARID is passed as a parameter into the CMOS Table                    */
/* DESCRIPTION: Retrieve address of structure of Dble Parameter from CMOS table */
/* OUTPUT: Return unsigned long, or DummyVar or error                           */
/********************************************************************************/
double fnfGetCMOSDoubleParam(unsigned short wVarID)
{
    if (wVarID & EXTENDED_CMOS_ID_MASK)
    {
        wVarID &= ~EXTENDED_CMOS_ID_MASK;
        return (*(CMOSExtendedDoubleTable[wVarID].pCMOS_DB));
    }
    else
        return(*(CMOSDoubleTable[wVarID].pCMOS_DB));
}

/********************************************************************************/
/* TITLE: fnfSetCMOSDoubleParam                                                 */
/* AUTHOR: BMP                                                                  */
/* INPUT:  VARID is passed as a parameter into the CMOS Table                   */
/*          dblValue is the value to set the CMOS variable to                   */
/* DESCRIPTION: Sets CMOS double parameter                                      */
/* OUTPUT: Returns successful.  If we decide to do error checking in these      */
/*          routines it can return successful/failure.                          */
/********************************************************************************/
BOOL fnfSetCMOSDoubleParam(unsigned short wVarID, double dblValue)
{
    if (wVarID & EXTENDED_CMOS_ID_MASK)
    {
        wVarID &= ~EXTENDED_CMOS_ID_MASK;
        *(CMOSExtendedDoubleTable[wVarID].pCMOS_DB) = dblValue;
    }
    else
        *(CMOSDoubleTable[wVarID].pCMOS_DB) = dblValue;
    return (SUCCESSFUL);
}


/********************************************************************************/
/* TITLE: fnfSetCMOSUCStringParam                                               */
/* AUTHOR: BMP                                                                  */
/* INPUT:  VARID is passed as a parameter into the CMOS Table                   */
/*          pUC points to the value to set the CMOS variable to                 */
/* DESCRIPTION: Sets CMOS unicode parameter                                     */
/* OUTPUT: Returns successful.  If we decide to do error checking in these      */
/*          routines it can return successful/failure.                          */
/********************************************************************************/
BOOL fnfSetCMOSUCStringParam(unsigned short wVarID, const UINT16 *pUC)
{
    if (wVarID & EXTENDED_CMOS_ID_MASK)
    {
        wVarID &= ~EXTENDED_CMOS_ID_MASK;
        (void)memcpy(CMOSExtendedUCTable[wVarID].pCMOS_UC, pUC,
                    sizeof(unsigned short) * CMOSExtendedUCTable[wVarID].UCLength);
    }
    else
    {
        (void)memcpy(CMOSUCTable[wVarID].pCMOS_UC, pUC,
                    sizeof(unsigned short) * CMOSUCTable[wVarID].UCLength);
    }
    return (SUCCESSFUL);
}



/********************************************************************************/
/* TITLE: fnfGetCMOSStringParam                                                 */
/* AUTHOR: BMP                                                                  */
/* INPUT: VARID is passed as a parameter into the CMOS Table                    */
/* DESCRIPTION: Retrieve address of structure of string Parameter from CMOS     */
/* OUTPUT: Returns pointer to requested string in CMOS                          */
/********************************************************************************/
char * fnfGetCMOSStringParam(unsigned short wVarID)
{
    if (wVarID & EXTENDED_CMOS_ID_MASK)
    {
        wVarID &= ~EXTENDED_CMOS_ID_MASK;
        return (CMOSExtendedASCIITable[wVarID].pCMOS_ASCIIPARM);
    }
    else
        return (CMOSASCIITable[wVarID].pCMOS_ASCIIPARM);
}

/********************************************************************************/
/* TITLE: fnfSetCMOSStringParam                                                 */
/* AUTHOR: BMP                                                                  */
/* INPUT:  VARID is passed as a parameter into the CMOS Table                   */
/*          pString points to the value to set the CMOS variable to             */
/* DESCRIPTION: Sets CMOS ascii string parameter                                */
/* OUTPUT: Returns successful.  If we decide to do error checking in these      */
/*          routines it can return successful/failure.                          */
/********************************************************************************/
BOOL fnfSetCMOSStringParam( UINT16 wVarID, const char *pString )
{
    if (wVarID & EXTENDED_CMOS_ID_MASK)
    {
        wVarID &= ~EXTENDED_CMOS_ID_MASK;
        (void)memcpy(CMOSExtendedASCIITable[wVarID].pCMOS_ASCIIPARM, pString,
            CMOSExtendedASCIITable[wVarID].chASCIIlength);
    }
    else
        (void)memcpy(CMOSASCIITable[wVarID].pCMOS_ASCIIPARM, pString,
        CMOSASCIITable[wVarID].chASCIIlength);
    return (SUCCESSFUL);
}


/********************************************************************************/
/* TITLE: fnfGetCMOSPackedByteParam                                             */
/* AUTHOR: BMP                                                                  */
/* INPUT:  VARID is passed as a parameter into the CMOS Table                   */
/* DESCRIPTION: Retrieve Packed Byte Parameter from CMOS wrapper                */
/* OUTPUT: returns pointer to requested packed byte parameter                   */
/********************************************************************************/
unsigned char * fnfGetCMOSPackedByteParam(unsigned short wVarID)
{
    if (wVarID & EXTENDED_CMOS_ID_MASK)
    {
        wVarID &= ~EXTENDED_CMOS_ID_MASK;
        return (CMOSExtendedPBTable[wVarID].pCMOS_PB);
    }
    else
        return (CMOSPBTable[wVarID].pCMOS_PB);
}

/********************************************************************************/
/* TITLE: fnfSetCMOSPackedByteParam                                             */
/* AUTHOR: BMP                                                                  */
/* INPUT:  VARID is passed as a parameter into the CMOS Table                   */
/*          pPBString points to the value to set the CMOS variable to           */
/*          wLen is the number of bytes to write                                */
/* DESCRIPTION: Sets CMOS packed byte parameter                                 */
/* OUTPUT: Returns successful.  If we decide to do error checking in these      */
/*          routines it can return successful/failure.                          */
/********************************************************************************/
BOOL fnfSetCMOSPackedByteParam( UINT16 wVarID, const UINT8 *pPBString, UINT16 wLen )
{
    if (wVarID & EXTENDED_CMOS_ID_MASK)
    {
        wVarID &= ~EXTENDED_CMOS_ID_MASK;
        (void)memcpy(CMOSExtendedPBTable[wVarID].pCMOS_PB, pPBString, wLen);
    }
    else
        (void)memcpy(CMOSPBTable[wVarID].pCMOS_PB, pPBString, wLen);
    return (SUCCESSFUL);
}


/********************************************************************************/
/* Title       : fnCmosGetLogParm                                               */
/* Author      : Edward J. Greene                                               */
/* Description :                                                                */
/*          Copy the source string into the destination string                  */
/* Input :                                                                      */
/*          Pointer to destination string                                       */
/*          Pointer to source      string                                       */
/*          Length of string                                                    */
/*                                                                              */
/* Output:                                                                      */
/*          None                                                                */
/* Notes:                                                                       */
/* 1. The data structure,to copy, is treated as a char string.                  */
/* 2. Only a single structure is copied here.                                   */
/********************************************************************************/

void  fnCmosGetLogParm( void *pdest, const void *psrc, size_t DataSize )
{

    /* Copy the selected record into the specified string  */
    (void)memcpy(pdest, psrc, DataSize);
}


/********************************************************************************/
/* Title       : fnCmosClearLogParms                                            */
/* Author      : Edward J. Greene                                               */
/* Description :                                                                */
/*          Clear the record.                                                   */
/*                                                                              */
/* Input :                                                                      */
/*          Pointer to destination string                                       */
/*          Value to copy into destination string                               */
/*          Length of string                                                    */
/* Output:                                                                      */
/*          None                                                                */
/* Notes:                                                                       */
/* 1. The data structure,to copy, is treated as a char string.                  */
/* 2. Only a single structure is cleared here.                                  */
/********************************************************************************/

void  fnCmosClearLogParms( void *pdest, size_t DataSize )
{

    /* Clear each byte in structure  */
    (void)memset(pdest, 0, DataSize);
}


/********************************************************************************/
/* Title       : fnCmosGetPostageChangeLogIndex                                 */
/* Author      : Edward J. Greene                                               */
/* Description :                                                                */
/*          Return the Current Postage Change Log Index                         */
/* Input :                                                                      */
/*          None                                                                */
/*                                                                              */
/* Output:                                                                      */
/*          The Current Postage Change Index                                    */
/*                                                                              */
/* Notes:                                                                       */
/* 1. Index into a 10 record circulare buffer.                                                                             */
/********************************************************************************/

unsigned char  fnCmosGetPostageChangeLogIndex( void )
{

    /* The Log Index is used to select the active Postage Change Log Structure */
    return ( CMOSPostageChangeLog.LogIndex );
}

/********************************************************************************/
/* Title       : fnCmosUpdatePostageChangeCount                                 */
/* Author      : Edward J. Greene                                               */
/* Description :                                                                */
/*          Update the current Postage Change Log Count.  When the Max Count is */
/*          reached, count is no longer increased.  At this point, the next     */
/*          record is used and it is cleared, ready for the next run.           */
/*          In this way, a job's data will be stored in two records.            */
/* Input :                                                                      */
/*          None                                                                */
/*                                                                              */
/* Output:                                                                      */
/*          lwCount                                                             */
/*                                                                              */
/* Notes:                                                                       */
/* 1. This counter is only cleared during a CMOS init or an increment to the    */
/*    record log index.                                                         */
/* 2. Overflow is prevented by limiting count to a Max Value of 4,294,967,294   */
/* 3. An overflow condition should never happen during the life of this         */
/*    product, but this case is covered here.                                   */
/********************************************************************************/

void  fnCmosUpdatePostageChangeCount( void )
{
    unsigned long  TempPostageValue;


    /* Increment counter til Max value is reached, thereafter switch to the next    */
    /* record, clear the new record, Increment Postage Count, & Save Postage Value  */
    if ( CMOSPostageChangeLog.PostageChangeTable[ fnCmosGetPostageChangeLogIndex( ) ]
        .PostageChangeLogData.lwCount >= MAX_POSTAGE_COUNT)
    {
        /* Save the current Postage Value                                      */
        TempPostageValue = fnCmosGetCurrentPostageValue ();

        /* Switch to the Next Record in the Circular Buffer                    */
        fnCmosBumpPostageChangeLogIndex( );

        /* Clear all Entries in this New Record                                */
        fnCmosClearPostageChangeRecord( );

        /* Lastly, set the New Postage Value to the Current Postage Value      */
        fnCmosUpdatePostageChangeValue( TempPostageValue );
    }

    /* Always increment the Postage Change Count                               */
    CMOSPostageChangeLog.PostageChangeTable[ fnCmosGetPostageChangeLogIndex( )].
        PostageChangeLogData.lwCount++;
}


/********************************************************************************/
/* Title       : fnCmosGetPostageChangeCount                                    */
/* Author      : Edward J. Greene                                               */
/* Description :                                                                */
/*          Get the current Postage Change Log Count                            */
/* Input :                                                                      */
/*          None                                                                */
/*                                                                              */
/* Output:                                                                      */
/*          Current Postage Change Log Count                                    */
/*                                                                              */
/* Notes:                                                                       */
/* 1. This counter is only cleared during a CMOS init.                          */
/* 2. There is no check for overflow.                                           */
/********************************************************************************/

unsigned long  fnCmosGetPostageChangeCount( void )
{

    /* Return the Postage Chang eLog Count for the active Record               */
    return( CMOSPostageChangeLog.PostageChangeTable[
                          fnCmosGetPostageChangeLogIndex( ) ].PostageChangeLogData.lwCount );
}


/********************************************************************************/
/* Title       : fnCmosGetCurrentPostageValue                                   */
/* Author      : Edward J. Greene                                               */
/* Description :                                                                */
/*          Update only the current Postage Change Log Count                    */
/* Input :                                                                      */
/*          None                                                                */
/*                                                                              */
/* Output:                                                                      */
/*          Postage Change Log COunt                                            */
/*                                                                              */
/* Notes:                                                                       */
/* 1. This counter is only cleared during a CMOS init.                          */
/* 2. There is no check for overflow.                                           */
/********************************************************************************/

unsigned long  fnCmosGetCurrentPostageValue ( void )
{

    return( CMOSPostageChangeLog.PostageChangeTable[
                          fnCmosGetPostageChangeLogIndex ()].PostageChangeLogData.lwPostageValue );
}


/********************************************************************************/
/* Title       : fnCmosBumpPostageChangeLogIndex                                */
/* Author      : Edward J. Greene                                               */
/* Description :                                                                */
/*          Increment the Postage Change Log Structure Circular Index           */
/* Input :                                                                      */
/*          None                                                                */
/*                                                                              */
/* Output:                                                                      */
/*          LogIndex                                                            */
/*                                                                              */
/* Notes:                                                                       */
/* 1. Increment Index as a 10 record circulare buffer.                          */
/********************************************************************************/

void  fnCmosBumpPostageChangeLogIndex ( void )
{

    CMOSPostageChangeLog.LogIndex = ++CMOSPostageChangeLog.LogIndex <
        POSTAGE_CHANGE_LOG_SZ ? CMOSPostageChangeLog.LogIndex : 0;

}

/********************************************************************************/
/* Title       : fnCmosUpdatePostageChangeValue                                 */
/* Author      : Edward J. Greene                                               */
/* Description :                                                                */
/*          Update the current Postage Change Log Count                         */
/* Input :                                                                      */
/*          New Postage Value                                                   */
/*                                                                              */
/* Output:                                                                      */
/*          lwPostageValue                                                      */
/*                                                                              */
/* Notes:                                                                       */
/* 1. Current Postage Value is overwritten by new value.                        */
/********************************************************************************/

void  fnCmosUpdatePostageChangeValue( unsigned long PostageValue )
{


    /* Update the current Postage Value                                        */
    CMOSPostageChangeLog.PostageChangeTable[ fnCmosGetPostageChangeLogIndex( )
                        ].PostageChangeLogData.lwPostageValue = PostageValue;
}


/********************************************************************************/
/* Title       : fnCmosUpdatePostageChangeAscReg                                */
/* Author      : Edward J. Greene                                               */
/* Description :                                                                */
/*          Update the current Postage Change Ascending Register                */
/* Input :                                                                      */
/*          Ascending Register                                                  */
/*                                                                              */
/* Output:                                                                      */
/*          Postage Change Ascending Register                                   */
/*                                                                              */
/* Notes:                                                                       */
/* 1. Current Ascending Register is overwritten by new ascending value.         */
/********************************************************************************/
void  fnCmosUpdatePostageChangeAscReg( const UINT8 *pAscReg )
{
    UINT8     bIndex;

    bIndex = fnCmosGetPostageChangeLogIndex();
    (void)memcpy( CMOSPostageChangeLog.PostageChangeTable[ bIndex ].PostageChangeLogData.AscReg,
                    pAscReg,
                    sizeof( CMOSPostageChangeLog.PostageChangeTable[0].PostageChangeLogData.AscReg ) );
}


/********************************************************************************/
/* Title       : fnCmosUpdatePostageChangeDesReg                                */
/* Author      : Edward J. Greene                                               */
/* Description :                                                                */
/*          Update the current Postage Change Descending Register               */
/* Input :                                                                      */
/*          Descending Register                                                 */
/*                                                                              */
/* Output:                                                                      */
/*          Postage Change Descending Register                                  */
/*                                                                              */
/* Notes:                                                                       */
/* 1. Current Descending Register is overwritten by new descending value.       */
/********************************************************************************/
void  fnCmosUpdatePostageChangeDesReg( const UINT8 *pDescReg )
{
    UINT8     bIndex;

    bIndex = fnCmosGetPostageChangeLogIndex();

    (void)memcpy( CMOSPostageChangeLog.PostageChangeTable[ bIndex ].PostageChangeLogData.DescReg,
                pDescReg,
                sizeof( CMOSPostageChangeLog.PostageChangeTable[0].PostageChangeLogData.DescReg) );
}


/********************************************************************************/
/* Title       : fnCmosUpdatePostageChangePieceC                                */
/* Author      : Edward J. Greene                                               */
/* Description :                                                                */
/*          Update the current Postage Change Piece Count                       */
/* Input :                                                                      */
/*          New Piece Count                                                     */
/*                                                                              */
/* Output:                                                                      */
/*          Postage Change Piece Count                                          */
/*                                                                              */
/* Notes:                                                                       */
/* 1. Current Piece Count is overwritten by new Piece Count.                    */
/********************************************************************************/
void  fnCmosUpdatePostageChangePieceC( const UINT8 *pPieceCount )
{
    UINT8     bIndex;

    bIndex = fnCmosGetPostageChangeLogIndex();

    (void)memcpy( CMOSPostageChangeLog.PostageChangeTable[ bIndex ].PostageChangeLogData.PieceCnt,
                pPieceCount,
                sizeof( CMOSPostageChangeLog.PostageChangeTable[0].PostageChangeLogData.PieceCnt) );
}


/********************************************************************************/
/* Title       : fnCmosClearPostageChangeRecord                                 */
/* Author      : Edward J. Greene                                               */
/* Description :                                                                */
/*          Clear each of the Postage Change Structure Members                  */
/* Input :                                                                      */
/*          None                                                                */
/*                                                                              */
/* Output:                                                                      */
/*          Each Postage Change structure element is cleared.  (0)              */
/*                                                                              */
/* Notes:                                                                       */
/********************************************************************************/
void  fnCmosClearPostageChangeRecord ( void )
{

    CMOSPostageChangeLog.PostageChangeTable[ fnCmosGetPostageChangeLogIndex( )]
        .PostageChangeLogData.lwCount        = 0;
    CMOSPostageChangeLog.PostageChangeTable[ fnCmosGetPostageChangeLogIndex( )]
        .PostageChangeLogData.lwPostageValue = 0;

    (void)memset( CMOSPostageChangeLog.PostageChangeTable[ fnCmosGetPostageChangeLogIndex( )
                           ].PostageChangeLogData.AscReg,
                    0,
                    sizeof( CMOSPostageChangeLog.PostageChangeTable[
                              fnCmosGetPostageChangeLogIndex( )].PostageChangeLogData.AscReg) );

    (void)memset( CMOSPostageChangeLog.PostageChangeTable[ fnCmosGetPostageChangeLogIndex( )]
                        .PostageChangeLogData.DescReg,
                    0,
                    sizeof( CMOSPostageChangeLog.PostageChangeTable[
                              fnCmosGetPostageChangeLogIndex( )].PostageChangeLogData.DescReg) );

    (void)memset( CMOSPostageChangeLog.PostageChangeTable[ fnCmosGetPostageChangeLogIndex( )]
                        .PostageChangeLogData.PieceCnt,
                    0,
                    sizeof( CMOSPostageChangeLog.PostageChangeTable[
                              fnCmosGetPostageChangeLogIndex( )].PostageChangeLogData.PieceCnt) );
}


/*******************************************************************************/
/* Title       : fnCmosPostageChangeRecord                                     */
/* Author      : Edward J. Greene                                              */
/* Description :                                                               */
/*                                                                             */
/* Input :                                                                     */
/*          Del Con Record Index                                               */
/*                                                                             */
/*                                                                             */
/* Output:                                                                     */
/*          None                                                               */
/*                                                                             */
/* Returns :                                                                   */
/*          Void Pointer to the start of the current Del Con Record            */
/*                                                                             */
/* Notes:                                                                      */
/*******************************************************************************/
void  *fnCmosPostageChangeRecord( unsigned char  RecordIndex )
{

    return( &CMOSPostageChangeLog.PostageChangeTable[ RecordIndex ]);
}


/*******************************************************************************/
/* Title       : fnCmosClearPostageChangeIndex                                 */
/* Author      : Edward J. Greene                                              */
/* Description :                                                               */
/*                                                                             */
/* Input :                                                                     */
/*          None                                                               */
/*                                                                             */
/*                                                                             */
/* Output:                                                                     */
/*          Log Index is Cleared                                               */
/*                                                                             */
/*                                                                             */
/* Notes:                                                                      */
/*******************************************************************************/
void  fnCmosClearPostageChangeIndex ( void )
{

    CMOSPostageChangeLog.LogIndex = 0; /*index at which to store the next entry*/
}

/* **********************************************************************
// FUNCTION NAME: fnCmosGetRightMargin
// DESCRIPTION: Gets the current right margin position from CMOS
//
// AUTHOR: Henry Liu
//
// INPUTS:  none
//
// OUTPUTS: Right Margin Position (in # of steps)
// MODIFICATION HISTORY:
//  2009.02.19 Clarisa Bellamy  LINT: copy flash instead of casting pointer.
//  08/02/2006  Raymond Shen    Right Margin parameters are retrieved from EMD
// *************************************************************************/
unsigned short fnCmosGetRightMargin( void )
{
    UINT16 wMarginPosition = CMOSPrintParams.RMarginPos;
    ST_PO_RIGHT_MARGIN    sRightMargin;
    UINT8 *pFlash;

    pFlash = fnFlashGetPackedByteParm( PBP_RIGHT_MARGIN_ADJUST_RANGE );
    (void)memcpy( &sRightMargin, pFlash, sizeof( sRightMargin ) );

    if(   (wMarginPosition > sRightMargin.maxRange) 
       || (wMarginPosition < sRightMargin.minRange) )
    {
        wMarginPosition = sRightMargin.defaultValue;
    }

    return( wMarginPosition );
}

/* **********************************************************************
// FUNCTION NAME: fnCmosSetRightMargin
// DESCRIPTION: Stores the right margin position in CMOS
//
// AUTHOR: Henry Liu
//
// INPUTS:  New Right Margin Position (in # of steps)
//
// OUTPUTS: TRUE - Valid data passed to function; value stored
//          FALSE - Invalid data passed to function; value not stored
// MODIFICATION HISTORY:
//  2009.02.19 Clarisa Bellamy  LINT: copy flash instead of casting pointer.
//  09/07/2006  Raymond Shen    Add some code to deal with the condition that
//                              for some countries margin adjustment is not supported.
//  08/02/2006  Raymond Shen    Right Margin parameters are retrieved from EMD
// *************************************************************************/
BOOL fnCmosSetRightMargin( unsigned short wMarginPosition )
{
    BOOL fStatus = TRUE;
    ST_PO_RIGHT_MARGIN    sRightMargin;
    UINT8 *pFlash;

    pFlash = fnFlashGetPackedByteParm( PBP_RIGHT_MARGIN_ADJUST_RANGE );
    (void)memcpy( &sRightMargin, pFlash, sizeof( sRightMargin ) );

  // Some countries(eg. UK) do not support margin adjustment and just use the default position.
  // On this condition, both min and max range are euqal to zero.
    if(   (sRightMargin.maxRange == 0)
       && (sRightMargin.minRange == 0) )
    {
        CMOSPrintParams.RMarginPos = sRightMargin.defaultValue;
    }
    else if(   (wMarginPosition > sRightMargin.maxRange) 
            || (wMarginPosition < sRightMargin.minRange) )
    {
        fStatus = FALSE;
    }
    else
    {
        CMOSPrintParams.RMarginPos = wMarginPosition;
    }

    return( fStatus );
}

/******************************************************************************
   MODULE: fnCmosSetTapeCalibrationAllowed
   AUTHOR: Craig DeFilippo
   DESCRIPTION: wrapper to allow/disallow the tape calibration feature
   PARAMETERS:   BOOL: True to enable, false to disable
   RETURNS:  none
******************************************************************************/

void fnCmosSetTapeCalibrationAllowed(BOOL fbTapeCalState)
{
    CMOSPrintParams.fbTapeCalibrationAllowed  = fbTapeCalState;
}

/******************************************************************************
   MODULE: fnCmosGetTapeCalibrationAllowed
   AUTHOR: Craig DeFilippo
   DESCRIPTION: wrapper to get the tape calibration allowed PCN parameter
   PARAMETERS:   none
   RETURNS:  BOOL: True if allowed, false otherwise
******************************************************************************/

BOOL fnCmosGetTapeCalibrationAllowed(void)
{
    return(CMOSPrintParams.fbTapeCalibrationAllowed);
}

/******************************************************************************
   MODULE: fnCmosSetTapeCalibrationActive
   AUTHOR: Craig DeFilippo
   DESCRIPTION: wrapper to activate/deactivate the tape calibration feature
   PARAMETERS:   BOOL: True to enable, false to disable
   RETURNS:  none
******************************************************************************/

void fnCmosSetTapeCalibrationActive(BOOL fbTapeCalState)
{
    CMOSPrintParams.fbTapeCalibrationActive   = fbTapeCalState;
}


/******************************************************************************
   MODULE: fnCmosGetTapeCalibrationActive
   AUTHOR: Craig DeFilippo
   DESCRIPTION: wrapper to get the tape calibration active CMOS variable
   PARAMETERS:   none
   RETURNS:  BOOL: True if allowed, false otherwise
******************************************************************************/

BOOL fnCmosGetTapeCalibrationActive(void)
{
    return(CMOSPrintParams.fbTapeCalibrationActive);
}

/******************************************************************************
   MODULE       : fnCMOSSetTapePrefeedTimeout
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : wrapper to set the Tape Motion control Profile prefeed timeout
   PARAMETERS   : timeout value (ms)
   RETURNS  : none
******************************************************************************/
void fnCMOSSetwapeLength(ushort wTapeLength)
{
    CMOSPrintParams.wTapeLength = wTapeLength;
}

/******************************************************************************
   MODULE       : fnCMOSGetTapePrefeedTimeout
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : wrapper to get the Tape Motion control Profile prefeed timeout
   PARAMETERS   : none
   RETURNS  : timeout value (ms)
******************************************************************************/
ushort  fnCMOSGetTapeLength(void)
{
    return(CMOSPrintParams.wTapeLength);
}

/******************************************************************************
   MODULE       : getInfrastructureNvmAddress
   AUTHOR       : R. Francisco
   DESCRIPTION  : Return pointer to Infrastructure NVM
   PARAMETERS   : none
   RETURNS      : Duh
******************************************************************************/
unsigned long *getInfrastructureNvmAddress(void)
{
    return infrastructureNVM;
}

//How big is it?
size_t getSizeOfInfrastructureNVM(void)
{
    return sizeof(infrastructureNVM);
}

/******************************************************************************
   MODULE       : getNvmHeapAddress
   AUTHOR       : R. Francisco
   DESCRIPTION  : Return pointer to NVM heap
   PARAMETERS   : none
   RETURNS      : Duh
******************************************************************************/
unsigned long *getNvmHeapAddress(void)
{
    return nvmHeap;
}

//How big is it?
size_t getSizeOfNvmHeap(void)
{
    return sizeof(nvmHeap);
}



/* *************************************************************************/
// FUNCTION NAME:   fnInitBarcodeScannerConfig
// DESCRIPTION:     apply default values for all Barcode Scanner parameters
// AUTHOR:          cristeb
// INPUTS:          None
// RETURNS:         Nothing
// NOTES:           
/* *************************************************************************/
static void fnInitBarcodeScannerConfig( S_BCS_CONFIGURATION *pBCSConfig )
{
/*    // default scan type
    // apply default for all accounting system configurations (including "none")
    pBCSConfig->bDefaultScanType = (unsigned char) BCS_ACCOUNT_NAME_ID_ENTRY ;

    // number of prefix characters required
    pBCSConfig->bNbrPrefixCharacters = (unsigned char) BCS_DEFAULT_PREFIX_LENGTH ;

    // default prefixes
    // apply all defaults regardless of current accounting system type
    fnAscii2Unicode( BCS_DEFAULT_ACCT_NAME_PFX, pBCSConfig->uniAcctName_ID_Prefix, BCS_DEFAULT_PREFIX_LENGTH) ;
    fnAscii2Unicode( BCS_DEFAULT_ACCT_CODE_PFX, pBCSConfig->uniAcctCode_SpeedCode_Prefix, BCS_DEFAULT_PREFIX_LENGTH) ;
    fnAscii2Unicode( BCS_DEFAULT_PRESET_PFX, pBCSConfig->uniPresetPrefix, BCS_DEFAULT_PREFIX_LENGTH) ;
    fnAscii2Unicode( BCS_DEFAULT_JOBID_1_PFX, pBCSConfig->uniJobID_1_Prefix, BCS_DEFAULT_PREFIX_LENGTH) ;
    fnAscii2Unicode( BCS_DEFAULT_JOBID2_PFX, pBCSConfig->uniJobID2Prefix, BCS_DEFAULT_PREFIX_LENGTH) ;

    // default continuation character
    fnAscii2Unicode( BCS_DEFAULT_CONTINUE_CHAR, pBCSConfig->uniContinuationChar, BCS_DEFAULT_PREFIX_LENGTH) ;

    // default Autoenter feature status
    pBCSConfig->bAutoEnter = (unsigned char) BCS_AUTOENTER_OFF ;*/
}

/*********************************************************************************
 *
 * Function Name : fnIsCmosBatteryLow
 * Description   : This function checks CMOS battery life timee
 *
 * Return: TRUE if CMOS battery is low
 *         FALSE otherwise
 *
 * Input         : NONE
 *
 ***********************************************************************************/
#if 0
BOOL fnIsCmosBatteryLow(void)
{
    long   lCmosBatteryLifeTime = 0; // Life time in seconds

    extern NVRAM_DATA pm_nvram_data;

    return FALSE;

}  // fnIsCmosBatteryLow()
#endif



//=====================================================================
//
//  Function Name:  void PBLogUSBHubStatusChange
//
//  Description: Logs a USB Hub Status change to cmos
//
//
//  Parameters: PortNum Port number on the hub
//          portStatus Port Change Status
//          hub  address as a long
//          child device address as a long
//  Returns: none
//
//
//  Preconditions:
//
//
//=====================================================================

void PBLogUSBHubStatusChange( unsigned long portNum , unsigned long portStatus ,
                  unsigned long hub , unsigned long child)
{
    unsigned long index;

    index = USBPCLogIndex++;
    if(USBPCLogIndex == MAX_USB_PC_LOGGING_ENTRIES)
        USBPCLogIndex = 0;
		
    USBPCLog[ index ].portStatus = portStatus;
    USBPCLog[ index ].portNum = portNum;
    USBPCLog[ index ].deviceAddr = child;
    USBPCLog[ index ].hubAddr = hub;
    USBPCLog[ index ].bootCycle = USBBootCycle;
    USBPCLog[ index ].logArg1 = 0;
    USBPCLog[ index ].logArg2 = 0;
    USBPCLog[ index ].logArg3 = 0;
}

//=====================================================================
//
//  Function Name:  PBLogUSBRegistration
//
//  Description: Log a new enumuration
//
//
//  Parameters:
//
//
//
//  Returns:
//
//
//  Preconditions:
//
//
//=====================================================================

void PBLogUSBRegistration( unsigned long device , unsigned long devType ,
               unsigned long devId )
{
    unsigned long index;

    index = USBRegistrationIndex++;
    if(USBRegistrationIndex == MAX_TRACKED_USB_REGISTRATIONS)
        USBRegistrationIndex = 0;
		
    USBEnumRegistrations[ index ].device = device;
    USBEnumRegistrations[ index ].devId = devId;
    USBEnumRegistrations[ index ].devType = devType;
    USBEnumRegistrations[ index ].bootCycle = USBBootCycle;
}

//=====================================================================
//
//  Function Name:
//
//  Description:
//
//
//  Parameters:
//
//
//
//  Returns:
//
//
//  Preconditions:
//
//
//=====================================================================
void PBLogUSBRegisterHub( unsigned long device , unsigned long devId )
{
    PBLogUSBRegistration( device , REG_HUB_DEVICE , devId );
}

//=====================================================================
//
//  Function Name:
//
//  Description:
//
//
//  Parameters:
//
//
//
//  Returns:
//
//
//  Preconditions:
//
//
//=====================================================================
void PBLogUSBRegisterEther( unsigned long device , unsigned long devId )
{
    PBLogUSBRegistration( device , REG_ETHER_DEVICE , devId );
}

//=====================================================================
//
//  Function Name:
//
//  Description:
//
//
//  Parameters:
//
//
//
//  Returns:
//
//
//  Preconditions:
//
//
//=====================================================================
void PBLogUSBRegisterMCP( unsigned long device , unsigned long devId )
{
    PBLogUSBRegistration( device , REG_MCP_DEVICE , devId );
}

/* **********************************************************************
// FUNCTION NAME:
// PURPOSE:
//
// AUTHOR: George Monroe
//
// INPUTS:
// RETURNS:
// **********************************************************************/
void PBLogUSBRegisterPRT( unsigned long device , unsigned long devId )
{
    PBLogUSBRegistration( device , REG_PRT_DEVICE , devId );
}

/* **********************************************************************
// FUNCTION NAME:
// PURPOSE:
//
// AUTHOR: George Monroe
//
// INPUTS:
// RETURNS:
// **********************************************************************/
void PBLogUSBRegisterPLAT( unsigned long device , unsigned long devId )
{
    PBLogUSBRegistration( device , REG_PLAT_DEVICE , devId );
}

//=====================================================================
//
//  Function Name:
//
//  Description:
//
//
//  Parameters:
//
//
//
//  Returns:
//
//
//  Preconditions:
//
//
//=====================================================================
void PBLogUSBRegisterScan( unsigned long device , unsigned long devId )
{
    PBLogUSBRegistration( device , REG_SCAN_DEVICE , devId );
}

//=====================================================================
//
//  Function Name:
//
//  Description:
//
//
//  Parameters:
//
//
//
//  Returns:
//
//
//  Preconditions:
//
//
//=====================================================================
void PBLogUSBRegisterMS( unsigned long device , unsigned long devId )
{
    PBLogUSBRegistration( device , REG_MS_DEVICE , devId );
}

//=====================================================================
//
//  Function Name:
//
//  Description:
//
//
//  Parameters:
//
//
//
//  Returns:
//
//
//  Preconditions:
//
//
//=====================================================================
void PBLogUSBRegisterDFU( unsigned long device , unsigned long devId )
{
    PBLogUSBRegistration( device , REG_DFU_DEVICE , devId );
}


/******************************************************************************
   FUNCTION NAME: fnCMOSSetLanguage
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Wrapper to manage setting the UIC and rating NVM language id
   PARAMETERS   : language id
   RETURN       : CMOS Language setting
   NOTES    : None
******************************************************************************/
uchar fnCMOSSetLanguage(uchar ucLanguage)
{
    // make sure the requested UIC language is in range according to the EMD
    if (ucLanguage < fnFlashGetNumLanguages())
    {
        CMOSSetupParams.Language = ucLanguage;
        // make sure the requested rating language is in range according to the RM
        if (ucLanguage < 2)
            CMOSSetupParams.ucRatingLanguage = ucLanguage;
    }
    return(CMOSSetupParams.Language);
}

// ***************************************************************************
// FUNCTION NAME:       fnCMOSOkToWriteIt
// PURPOSE:     
//      Can be called before writing to CMOS to see if any necessary CMOS 
//      preservation has been completed..
//      It is NOT ok to write to CMOS if CMOS Preservation is required but
//      has not been completed.
// INPUTS:      None.
// RETURNS:     
//      BOOL - TRUE if CMOS Preservation is not needed or has been completed.
//           - FALSE if CMOS needs to be preserved, but hasn't been.
// HISTORY:
//  2008.12.05  C.Bellamy - Ported from Mega, and modified for FPHX.
// NOTES:   
//  1. To determine if CMOSPreservation has completed (or is not needed) 
//      checks the CMOSSignature.ulExtVersion, which should be set CMOS_VER
//      only if any required CMOS preservation has been performed, whether 
//      or not we have an EMD. 
//---------------------------------------------------------------------------
BOOL fnCMOSOkToWriteIt( void )
{
    BOOL    fOkToWriteCmos = FALSE;

    if( CMOSSignature.ulExtVersion == CMOS_VER )
    {
        fOkToWriteCmos = TRUE;
    }

    return( fOkToWriteCmos );
}

/******************************************************************************
   FUNCTION NAME: fnUpdateCmosFromNewEmd
   AUTHOR       : Sandra Peterson
   DESCRIPTION  : Updates stuff in CMOS with data from a new EMD.
   PARAMETERS   : None
   RETURN       : None
   NOTES        : None
******************************************************************************/
void fnUpdateCmosFromNewEmd(void)
{
	unsigned char	*pRightMarginData = fnFlashGetPackedByteParm(PBP_RIGHT_MARGIN_ADJUST_RANGE);
	char 			*retStr;
	
	unsigned short	wMarginPosition = CMOSPrintParams.RMarginPos;
	unsigned short	wAutoDateOptions = fnFlashGetWordParm(WP_AUTO_DATE_ADVANCE_OPTIONS);
	unsigned char	ucNominal = PO_HORIZ_PRINT_POS_DEFAULT;
	unsigned char	ucMin = PO_HORIZ_PRINT_POS_LOW;
	unsigned char	ucMax = PO_HORIZ_PRINT_POS_HIGH;
	unsigned char	bInx;

// Note: The update of the Dcap parameters will be done during the Dcap powerup processing.

	// update the Delivery Confirmation min postage value
    (void)memcpy( &CMOSSetupParams.lwMinDelConPostage,
                  fnFlashGetMoneyParm(MP_DEFAULT_DELCON_POSTAGE_VAL) + 1, sizeof(ulong) );

	// Update the DST offset, which might have been changed because the country no longer
	// supports DST, or because the country now does support DST
    CMOSSetupParams.ClockOffset.DaylightSToff = (char) fnFlashGetByteParm(BP_FACTORY_DST_OFFSET);

	// Update the time zone offset and the drift amount because the country might have changed
	// to sync'ing w/ the GMT time from Tier3.
	if (fnFlashGetByteParm(BP_PBP_TIME_SYNC_ENABLE))
	{
		CMOSSetupParams.ClockOffset.TimeZone = (short)fnFlashGetWordParm(WP_TIME_LOCALIZATION_OFFSET);
		CMOSSetupParams.ClockOffset.Drift = 0;
	}

	// If the auto-date advance time can't be changed, make sure it's set to the time
	// in the EMD.
	// If auto-date advance is mandatory and the current auto-date advance time is midnight,
	// make sure the auto-date advance time is set to the time in the EMD.
	if ((wAutoDateOptions & AUTO_ADV_CANNOT_BE_CHANGED) ||
		((wAutoDateOptions & AUTO_ADV_DATE_IS_MANDATORY) && !CMOSSetupParams.AutoAdvTime))
	{
		CMOSSetupParams.AutoAdvTime  = fnFlashGetLongParm(LP_FACTORY_DATE_ADVANCE_TIME);
	}
                    
	// Update the refill mode to make sure it matches how the code actually uses it.
    if (fnFlashGetByteParm(BP_PBP_REFILL_MODE) == PBP_MODE_KEYPAD_ONLY)
        CMOSSetupParams.RefillMode = REFILL_MODE_KEYPAD;
    else
        CMOSSetupParams.RefillMode = REFILL_MODE_MODEM;

	// Update the starting permit piece count if it is currently set to zero
    if (!CMOSPrintParams.PermitPC)
        CMOSPrintParams.PermitPC = fnFlashGetLongParm(LP_INITIAL_PERMIT_MAIL_COUNT);

	// Update the type of Package ID Number supported
    CMOSPrintParams.PINType = fnFlashGetByteParm(BP_PIN_TYPE);

	// Updated the allowed right-hand margin range.
	if (pRightMarginData)
	{
		ucNominal = pRightMarginData[0];
		ucMin = pRightMarginData[1];
		ucMax = pRightMarginData[2];
	}

	// make sure the print position is valid
	if ((wMarginPosition > ucMax) || (wMarginPosition < ucMin))
	{
		// if the current position is out of range based on the data in the new EMD,
		//	reset it back to the nominal.
		CMOSPrintParams.RMarginPos = ucNominal;
	}

	// make sure the print position in the presets is valid
	for (bInx = 0; bInx < MKEY_TABLE_SZ; bInx++)
	{
/*
		if (CMOSMemKeys[bInx].PresetName[0] != 0)
		{
			if ((CMOSMemKeys[bInx].bPrintPos > ucMax) || (CMOSMemKeys[bInx].bPrintPos < ucMin))
			{
				// if the current position is out of range based on the data in the new EMD,
				//	reset it back to the nominal.
				CMOSMemKeys[bInx].bPrintPos = ucNominal;
				fbDidPresetChanges = TRUE;
			}
		}
*/
	}

    // Without an EMD, this is initilialized to MAX_PERMITS.
    CMOSPermitArray.bMaxPermitsByPcn = fnFlashGetByteParm(BP_PERMIT_MAX_ALLOWED);
	
    // Without an EMD, this is initilialized to MAX_TEXT_ENTRY
    CMOSTextEntryArray.bMaxTextMsgsByPcn = fnFlashGetByteParm(BP_TXTMSG_MAX_ALLOWED);

	// Check if the Supervisor password needs to be updated
	if (fnCheckSuperPasswordDefined() == TRUE)
	{
		// if the supervisor password is enabled, make sure it has the latest required usage settings
	    CMOSSupervisorInfo.usSuperProtectedFeature |= fnFlashGetWordParm( WP_SUPER_PROTECTION_REQUIRED );
	}
	else
	{
	    // Else, update Supervisor Protection if a supervisor password is now required
	    // but none is defined.
	    if ( ( (fnFlashGetByteParm(BP_EXT_TERMINAL_SECURITY) == TRUE) ||
	           (fnFlashGetByteParm(BP_ALLOW_SUPER_PASSWORD_DISABLE) == TRUE) ) &&
	         (fnCheckSuperPasswordDefined() == FALSE) )
	    {
	        fnInitSuperPasswordWithEMD();
	    }
	}
	
    //reinitialize the lock code if the lock code parameters are updated.
	// if the backwards EMD parameter indicates that the Lock Code can't be disabled & it isn't enabled,
	// enable it.
	if ((fnFlashGetByteParm(BP_ALLOW_LOCK_CODE_DISABLE) == TRUE) &&
		 	 (fnCheckLockCodeEnabled() == FALSE))
	{
        fnCMOSInitLockCodeWithEmd();
	}

    // Reset minRto to 1 second. (Was 1/4 second.)
    // This can't be changed by the user, and should be set to 1 second from this
    //  point forward, no matter what the CMOSVER is.
    CMOSNetworkConfig.minRto = TICKS_PER_SECOND;

	// Update the DNS primary and secondary server IP addresses as long as we aren't
	// connecting to the QA Distributor.
	if ((strcmp(CMOSNetworkConfig.distributorUrl, cQaDistributor) != 0) &&
		(strcmp(CMOSNetworkConfig.distributorUrl, cSecapQaDistributor) != 0))
	{
	    (void)memset(CMOSNetworkConfig.primaryDnsServer, '\0', sizeof(CMOSNetworkConfig.primaryDnsServer));
	    retStr = (char *) fnFlashGetAsciiStringParm(ASP_PRIMARY_DNS_SERVER);
	    if(retStr)
	        (void)strncpy(CMOSNetworkConfig.primaryDnsServer, retStr, sizeof(CMOSNetworkConfig.primaryDnsServer ) - 1);

	    (void)memset(CMOSNetworkConfig.secondaryDnsServer, '\0', sizeof(CMOSNetworkConfig.secondaryDnsServer));
	    retStr = (char *) fnFlashGetAsciiStringParm(ASP_SECONDARY_DNS_SERVER);
	    if(retStr)
	        (void)strncpy(CMOSNetworkConfig.secondaryDnsServer, retStr, sizeof(CMOSNetworkConfig.secondaryDnsServer) - 1);
	}
	else	// load the primary and secondary server IP addresses for QA.
	{
		(void)memset(CMOSNetworkConfig.primaryDnsServer, '\0', sizeof(CMOSNetworkConfig.primaryDnsServer));
		retStr = (char *) fnFlashGetAsciiStringParm(ASP_QA_PRIMARY_DNS_SERVER);
		if(retStr)
			(void)strncpy(CMOSNetworkConfig.primaryDnsServer, retStr, sizeof(CMOSNetworkConfig.primaryDnsServer) - 1);

		(void)memset(CMOSNetworkConfig.secondaryDnsServer, '\0', sizeof(CMOSNetworkConfig.secondaryDnsServer));
		retStr = (char *) fnFlashGetAsciiStringParm(ASP_QA_SECONDARY_DNS_SERVER);
		if(retStr)
			(void)strncpy(CMOSNetworkConfig.secondaryDnsServer, retStr, sizeof(CMOSNetworkConfig.secondaryDnsServer) - 1);
	}

}


// *************************************************************************
// FUNCTION NAME:       fnCMOSClearBalanceInqLogTemp 
// DESCRIPTION:
//      Clears the Balance InquiryLog temporary spot data.
//
// INPUTS:
//      None
// RETURNS:
//      None
// NOTES:
// MODIFICATION HISTORY:
//   2009.05.18 Clarisa Bellamy  Initial version
//-----------------------------------------------------------------------------
void fnCMOSClearBalanceInqLogTemp( void )
{
    memset( &CMOSBalanceInquiryLog.sTempEntry, 0, sizeof( T_CMOSBalInqLogEntry ) );
}

// *************************************************************************
// FUNCTION NAME:       fnCMOSLogBalanceInqToTemp
// DESCRIPTION:
//      Saves the Balance InquiryLog data to a temporary spot.
//
// INPUTS:
//      None
// RETURNS:
//      None
// NOTES:
//  1. A wrapper function will determine if the data should be logged now.
//
// HISTORY:
//   2009.05.18 Clarisa Bellamy  Initial version
//-----------------------------------------------------------------------------
void fnCMOSLogBalanceInqToTemp( void )
{
    DATETIME    sDateTime;

     // This saves the data to a temp location.  It will be copied in bulk
     //  to the log array later.
     GetSysDateTime( &sDateTime, ADDOFFSETS, GREGORIAN );
     memcpy( &CMOSBalanceInquiryLog.sTempEntry.sTimeStamp, 
             &sDateTime, 
             sizeof( DATETIME ) );
}


// *************************************************************************
// FUNCTION NAME:       fnCMOSLogBalanceInquiryFromTemp
// DESCRIPTION:
//      Saves the data in the Temp Balance Inquiry Entry to the actual log, 
//      and increments the index.
//
// INPUTS:
//      None
// RETURNS:
//      None.
// NOTES:
//
// MODIFICATION HISTORY:
//   2009.05.18 Clarisa Bellamy  Initial version
//-----------------------------------------------------------------------------
void fnCMOSLogBalanceInqFromTemp( void )
{
    UINT8   index = CMOSBalanceInquiryLog.ucIndex;
    T_CMOSBalInqLogEntry *pEntry;

    pEntry = &CMOSBalanceInquiryLog.aLog[ index ]; 

    if( CMOSBalanceInquiryLog.sTempEntry.sTimeStamp.bCentury > 0 ) 
    {
        // Only store a valid temp value is not set, don't store it.
        memcpy( pEntry, &CMOSBalanceInquiryLog.sTempEntry, sizeof( T_CMOSBalInqLogEntry ) );
        if( ++index >= NUM_BAL_INQ_ENTRIES )
        {
            index = 0;
            CMOSBalanceInquiryLog.fWrapped = TRUE;
        }

        CMOSBalanceInquiryLog.ucIndex = index;
		
        // To make sure the same inquiry doesn't get logged twice.
        fnCMOSClearBalanceInqLogTemp();
    }
}



/* **********************************************************************
 FUNCTION NAME: fnStoreRefillAR

 PURPOSE:       Stores given refill data AR value in CMOS structure   
                    
 AUTHOR:        Steve Terebesi  

* INPUTS:       *pRefillAR - pointer to refill AR structure

* OUTPUTS:      None
 *************************************************************************/
void fnStoreRefillAR(MONEY* pRefillAR)
{
    memcpy(CMOS2LastRefillData.rAR, pRefillAR, sizeof(CMOS2LastRefillData.rAR));
}



/* **********************************************************************
 FUNCTION NAME: fnStoreRefillDR

 PURPOSE:       Stores given refill data DR value in CMOS structure   
                    
 AUTHOR:        Steve Terebesi  

* INPUTS:       *pRefillDR - pointer to refill DR structure

* OUTPUTS:      None
 *************************************************************************/
void fnStoreRefillDR(MONEY* pRefillDR)
{
    memcpy(CMOS2LastRefillData.rDR, pRefillDR, sizeof(CMOS2LastRefillData.rDR));
}



/* **********************************************************************
 FUNCTION NAME: fnStoreRefillAmount

 PURPOSE:       Stores given refill amount value in CMOS structure   
                    
 AUTHOR:        Steve Terebesi  

* INPUTS:       pRefillAmount - refill amount

* OUTPUTS:      None
 *************************************************************************/
void fnStoreRefillAmount(SINT32* pRefillAmount)
{
    memcpy(&CMOS2LastRefillData.refillAmt, pRefillAmount, sizeof(CMOS2LastRefillData.refillAmt));
}



/* **********************************************************************
 FUNCTION NAME: fnGetLastRefillAR

 PURPOSE:       Gets AR from last refill   
                    
 AUTHOR:        Steve Terebesi  

* INPUTS:       *pAR - pointer to AR (MONEY) variable

* OUTPUTS:      None
 *************************************************************************/
void fnGetLastRefillAR(MONEY *pAR)
{
    memcpy(pAR, &CMOS2LastRefillData.rAR, sizeof(CMOS2LastRefillData.rAR));
}



/* **********************************************************************
 FUNCTION NAME: fnGetLastRefillDR

 PURPOSE:       Gets DR from last refill   
                    
 AUTHOR:        Steve Terebesi  

* INPUTS:       *pDR - pointer to DR (MONEY) variable

* OUTPUTS:      None
 *************************************************************************/
void fnGetLastRefillDR(MONEY *pDR)
{
    memcpy(pDR, &CMOS2LastRefillData.rDR, sizeof(CMOS2LastRefillData.rDR));
}



/* **********************************************************************
 FUNCTION NAME: fnGetLastRefillAmount

 PURPOSE:       Gets amount from last refill   
                    
 AUTHOR:        Steve Terebesi  

* INPUTS:       *pAmount - pointer to refill (SINT32) variable

* OUTPUTS:      None
 *************************************************************************/
void fnGetLastRefillAmount(SINT32 *pAmount)
{
    memcpy(pAmount, &CMOS2LastRefillData.refillAmt, sizeof(CMOS2LastRefillData.refillAmt));
}


