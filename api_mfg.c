/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    api_mfg.c

   DESCRIPTION:    API implementations for PCT/manufacturing messages. 
 ----------------------------------------------------------------------
               Copyright (c) 2017 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
* HISTORY: 
*				  
* 2018.07.07 CWB - Modified on_PSDRunGeminiSelfTest() to add the bobStatus code
*					 to the reply if it is not 0 (OK)
* 				- Added a macro that puts a "TestCode" item in the api reply to easily 
*					identify if/which test code is running on the meter.  Then 
*					added this macro to most of the PSD messages. 
* 				- After testing, commented out the macro, and added a second version 
*					that does nothing, and is used for release code.   
* 2017.07.28 CWB - Change "ProgMode" to "PSDFirmwareMode", and use short strings for value.
* 2017.07.27 CWB - Fixed on_PSDMasterErase function to use the correct BOBID to
*                   run the MasterErase script.
*                - Added new utility function fnCheckPSDModeForState(), which is
*                   desgined to be called by on_GetPSDState and others, to check
*                   that the PSD is running in APP mode, and if not, add the 
*                   progMode to the response.
* 2017.07.26 CWB - Fixed function to check PSD mode before sending an App msg so
*                   it returns the correct error if the IPSD is in Boot Loader mode.
* 2017.07.26 CWB - Adding GetPSDParamList handling.
* 2017.07.17 CWB - Adding PSD App message handling.
* 2017.06.13 CWB - Starting out.
******************************************************************************/


#include "commontypes.h"
#include "cJSON.h"
#include "wsox_tx_queue.h"
#include "wsox_services.h"
#include "bobutils.h"
#include "ipsd.h"
#include "oit.h"
#include "oifpinfra.h"
#include "networkmonitor.h"
#include "utils.h"
#include "flashutil.h"
#include "bob.h"
#include "clock.h"
#include "api_funds.h"
#include "api_utils.h"  // generateResponse().


#define  CRLF   "\r\n"

// Defining this before the include file creates the table of string 
//  pointers to the const Gemini Boot Loader Status strings.
#define __API_GBLDR_STATUS_DEFINE_TABLES__
#include "api_mfg.h"



// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// OK This is a real shortcut, that technically shouldn't be done, but this is
//  quick and will work for now.



// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// Boot Loader Command format definitions
#define PSDBL_MSG_HDR_LEN           5
#define PSDBL_ADDRESS_OFFSET        1
#define PSDBL_ADDRESS_LEN           3

// Boot Loader Reply format definitions
#define PSDBL_REPLY_CMD_OFFS        0
#define PSDBL_REPLY_CODE_OFFS       1
#define PSDBL_REPLY_DATA_OFFS       2
#define PSDBL_REPLY_HDR_LEN         2
#define PSDBL_CRC_SIZE              4

// Max number of bytes allowed by Gemini BL in one multi-packet load message.
#define PSDBL_MAX_MSG_BYTES    (1016)  

// Just make it 10 bytes, so there is room for at least 8 chars and a the null-terminator.
#define PSD_STATUS_STRING_SZ    10 
 


// Blank one is for checked in code.  The other is for test code, so test code can be 
//  easily identified using CSDTesterTool: 
#define ADD_TEST_ID_TO_RESPONSE(x)    
//#define ADD_TEST_ID_TO_RESPONSE(x)    cJSON_AddStringToObject( x, "TestCode", "K" )



extern BOOL fNoTabletConnection;
static mfg_handle_entry_t mfg_entry;


static const UINT8      pBLCalcProgCRC     [ PSDBL_MSG_HDR_LEN ] = { 0x04, 0, 0, 0, 0 };
static const UINT8      pBLEnterAppMode    [ PSDBL_MSG_HDR_LEN ] = { 0x08, 0, 0, 0, 0 };
static const UINT8      pBLExitAppMstrErase[ PSDBL_MSG_HDR_LEN ] = { 0x20, 0, 0, 0, 0 };
static const UINT8      pBLDisableBootLoader[PSDBL_MSG_HDR_LEN ] = { 0x65, 0, 0, 0, 0 };
static const UINT8      pBLGetMicroInfoMsg [ PSDBL_MSG_HDR_LEN ] = { 0x80, 0, 0, 0, 0 };
// This one isn't constant, because the address needs to be written to it dynamically.
static       UINT8      pBLLoadAndVerifyHdr[ PSDBL_MSG_HDR_LEN ] = { 0x12, 0, 0, 0, 0 };


// Buffer should be large enough for largest message plus header
UINT8  pMfgBuff_Big[ PSDBL_MAX_MSG_BYTES + 10 ];

// Buffer should be large enough for short message plus header
UINT8  pMfgBuff_Small[ 32 ];

// Buffer should be large enough for largest message *2 +1 
char  pMfgBuff_BigAscii[ (PSDBL_MAX_MSG_BYTES *2) +1 ];

static const char *ProgModeStrings[ IPSD_MODE_MAX +1 ] =
{
 /* IPSD_MODE_NONE              */   "NotAvailable",
 /* IPSD_MODE_ASTEROID          */   "Asteroid",
 /* IPSD_MODE_BOOT_LOADER       */   "BootLoader",
 /* IPSD_MODE_UNTESTEDAPP       */   "UntestedApp",
 /* IPSD_MODE_APPLICATION       */   "OpenApp",
 /* IPSD_MODE_SECURE_APP        */   "SecureApp",
 /* IPSD_MODE_GEMINI_UNKNOWN    */   "GeminiUnknown",
 /* IPSD_MODE_GEMINI_EXITING    */   "GeminiTempUnavail",
 /* all the rest                */   "UnknownMode"
};


#define ADDIF_NONE           0x0001 
#define ADDIF_ASTEROID       0x0002
#define ADDIF_BOOT_LOADER    0x0004 
#define ADDIF_UNTESTEDAPP    0x0008
#define ADDIF_APPLICATION    0x0010 
#define ADDIF_SECURE_APP     0x0020
#define ADDIF_GEMINI_UNKNOWN 0x0040 
#define ADDIF_GEMINI_EXITING 0x0080
#define ADDIF_UNKOWN         0x0100

#define ADDIF_NOT_APP           ~(ADDIF_UNTESTEDAPP | ADDIF_APPLICATION | ADDIF_SECURE_APP)
#define ADDIF_NOT_SECURE_APP    ~(ADDIF_SECURE_APP)
#define ADDIF_NOT_BOOT_LOADER   ~(ADDIF_BOOT_LOADER)
#define ADDIF_ANYMODE           0xFFFF


// ----------------------------------------------------------------------------
//      External Function declarations: 
// ----------------------------------------------------------------------------

// From api_diags.c:
extern uint8_t hex_char_to_bin(int c);

// From cbobpriv.c (probably should have been moved to cbobutil):
extern void fnSysTimeToIPSD( long *, DATETIME * );

// global moved from cedmmfg.c
struct  TransferControlStructure  EdmPhcPsd;    // EDM/BOB Control Struct

// From intellilink.c:
extern DATETIME fnInfraUpdateParseDateTime( char *pDate, char *pTime );


// From wsox_EMDHandlers.c:
STATUS binaryToASCII(unsigned char *pIn, int inSize, unsigned char *pOut, int outMaxSize, int *outSize);
//#define  EXT_PARM_STATUS_LEN        4

// Notes: 
//  Using the BOBID_IPSD_BL_PASSTHROUGH_MSG, means that the bob task will save
//  the reply this way: 
//      [0] = Cmd
//      [1] = Status Code
//      [3] onward = data after the 8-byte reply header. 
// If the length of the reply in the xfer structure is less than 2 bytes, then 
//  the reply from the boot loader did not have the requisite 8 byte header 
//  and should be considered invalid. 



//---------------------------------------------------------------------
// ********************************************************************************
//      FUNCTIONS!
// ********************************************************************************
//---------------------------------------------------------------------


// John's clever little function to get the 2-byte status AND swap it.
UINT16 fnGetIPsdStatusRsp(void)
{
    UINT32 ulStatus;
    UINT16 uwStatus;
    
    fnValidData( BOBAMAT0, PSD_XPORT_MSG_STATUS, &ulStatus);
    uwStatus = ulStatus >> 16;
    return( EndianSwap16(uwStatus) );
}

// ********************************************************************************
// FUNCTION NAME:               fnCheckBootLoaderModeAvailble()                               
// PURPOSE:     Check for BootLoaderMode, and return error if not available.
// INPUTS:      
//      eRspCode - If no error is detected, this will be the return value.
//                  It is the caller's current eRspCode.
// RETURN:      
//      gbldr_status_t - If an error is detected, it returns an error code,
//                      otherwise, it returns the code that was passed in.
// OUTPUTS:     None.
// NOTES:       None 
// HISTORY:
// 2017.07.24 C.Bellamy - Initial
// 2017.07.27 C.Bellamy - Add checks for Asteroid mode, and any other mode that
//                      won't respond to BL Messages.
//---------------------------------------------------------------------
gbldr_status_t fnCheckBootLoaderModeAvailble( gbldr_status_t eRspCode ) 
{
    UINT8   ubIpsdProgMode = IPSD_MODE_NONE;

    if( fnValidData( BOBAMAT0, BOBID_IPSD_GET_PROG_MODE, &ubIpsdProgMode ) != BOB_OK )
    {
        eRspCode = GBLDR_STS_CANT_TALK_TO_IPSD;
    }
    else if( ubIpsdProgMode == IPSD_MODE_SECURE_APP )
    {
        eRspCode = GBLDR_STS_BL_DISABLED;   
    }
    else if( ubIpsdProgMode == IPSD_MODE_ASTEROID )
    {
        eRspCode = GBLDR_STS_ASTEROID;   
    }
    else if( ubIpsdProgMode == IPSD_MODE_NONE )
    {
        eRspCode = GBLDR_STS_CANT_TALK_TO_IPSD;   
    }
    else if( ubIpsdProgMode == IPSD_MODE_GEMINI_UNKNOWN )
    {
        eRspCode = GBLDR_STS_IN_UNKNOWN_GEMINI_MODE;   
    }
    else if( ubIpsdProgMode == IPSD_MODE_GEMINI_EXITING )
    {
        eRspCode = GBLDR_STS_TEMP_UNAVAILABLE;   
    }

    return( eRspCode );
}


// ********************************************************************************
// FUNCTION NAME:               fnCheckIpsdInAppMode()                               
// PURPOSE:     Check that iButton is running in App mode.  If not, return the 
//              appropriate Response Code.  This is used to verify that the IPSD
//              is running in the correct mode to be able to respond to an IPSD 
//              App message.
// INPUTS:      
//      eRspCode - If no error is detected, this will be the return value.
//                  It is the caller's current eRspCode.
// RETURN:      
//      gbldr_status_t - If an error is detected, it returns an error code,
//                      otherwise, it returns the code that was passed in.
// OUTPUTS:     None.
// NOTES:       None 
// HISTORY:
// 2017.07.24 C.Bellamy - Initial
// 2017.07.26 C.Bellamy - Fixed error code returned for ISPD in BootLoader mode.
// 2017.07.27 C.Bellamy - Don't check for Asteroid mode here.
//---------------------------------------------------------------------
gbldr_status_t fnCheckIpsdInAppMode( gbldr_status_t eRspCode ) 
{
    UINT8   ubIpsdProgMode = IPSD_MODE_NONE;

    fnValidData( BOBAMAT0, BOBID_IPSD_GET_PROG_MODE, &ubIpsdProgMode ); 
    
    if( ubIpsdProgMode == IPSD_MODE_BOOT_LOADER )
    {
        eRspCode = GBLDR_STS_IN_BL_MODE;   
    }
    else if( ubIpsdProgMode == IPSD_MODE_NONE )
    {
        eRspCode = GBLDR_STS_CANT_TALK_TO_IPSD;   
    }
    else if( ubIpsdProgMode == IPSD_MODE_GEMINI_UNKNOWN )
    {
        eRspCode = GBLDR_STS_IN_UNKNOWN_GEMINI_MODE;   
    }
    else if( ubIpsdProgMode == IPSD_MODE_GEMINI_EXITING )
    {
        eRspCode = GBLDR_STS_TEMP_UNAVAILABLE;   
    }

    return( eRspCode );
}


// ********************************************************************************
// FUNCTION NAME:               fnCheckPSDModeForState()                               
// PURPOSE:     Used check and possibly report the PSD ProgMode as PSDFirmwareMode.
// INPUTS:      
//      rspMsg - Points to the Response Msg json object that the caller has 
//              set up.
//      bfAddFieldIf - BitFlags indicating which modes the caller wants reported.
// RETURN:      
//      UINT8 - The actual current ProgMode.
// OUTPUTS:     
//      If the current ProgMode's flag is set in bfAddFieldIf, then add the field
//      to the response buffer
//
// NOTES:       None 
// HISTORY:
// 2017.07.27 C.Bellamy - Initial
//---------------------------------------------------------------------
UINT8 fnCheckPSDProgModeState( cJSON *rspMsg, UINT16 bfAddFieldIf )
{
    UINT8           ubIpsdProgMode = IPSD_MODE_NONE;
    UINT8           ubProgModeIdx = IPSD_MODE_NONE;

    // Get ProgMode, and range check.
    fnValidData( BOBAMAT0, BOBID_IPSD_GET_PROG_MODE, &ubIpsdProgMode );
    if( ubIpsdProgMode < IPSD_MODE_MAX )
        ubProgModeIdx = ubIpsdProgMode;
    else 
        ubProgModeIdx = IPSD_MODE_MAX;

    // If caller wants this mode reported, add the field to the response message
    if( ((UINT16)0x0001 << (ubProgModeIdx)) & bfAddFieldIf  )
    {
        cJSON_AddStringToObject( rspMsg, "PSDFirmwareMode", ProgModeStrings[ubProgModeIdx] );
    }
    return( ubIpsdProgMode );
}               

// This is called by on_GetPSDState to report the ProgMode if not in Secure App mode.
//  AND return FALSE if not in any App mode. 
INT fnCheckPSDModeForState( cJSON *rspMsg )
{
    INT             iPSDAppRunning = FALSE; // Set to TRUE if in ANY app mode.  
    UINT8           ubIpsdProgMode;

    // Get ISPD ProgMode and add field if not in Secure App Mode.
    ubIpsdProgMode = fnCheckPSDProgModeState( rspMsg, ADDIF_NOT_SECURE_APP );
    switch( ubIpsdProgMode )
    {
        // Return TRUE if in ANY APP mode.
        case  IPSD_MODE_UNTESTEDAPP   :   
        case  IPSD_MODE_APPLICATION   : 
        case  IPSD_MODE_SECURE_APP    :
            iPSDAppRunning = TRUE;  
            break;
    }
    return( iPSDAppRunning );
}               


//---------------------------------------------------------------------
// ********************************************************************************
//     on_  FUNCTIONS!
// ********************************************************************************
//---------------------------------------------------------------------


// ----------------------------------------------------------------------------[on_PSDBLGetMicroInfo]--

void on_PSDBLGetMicroInfo( UINT32 handle, cJSON *root )
{
    const UINT8 *pCommandToSend;
    char    *pData; 
    UINT16  uwRspLen = 0;
    UINT8   ubBobStatus;
    UINT8   ubRspCmd;
//    UINT8   ubRspCode;
    char    pStatusCodeStr[ 6 ]; // Holds ASCII of 1-byte hex: "12"
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;

    pCommandToSend = pBLGetMicroInfoMsg;

    // Setup Transfer Control Function to send message to PSD, and 
    // get response in pMfgBuff.
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    // Setup Transfer Control Structure to send message to PSD
    EdmPhcPsd.ptrToDataToSendToDevice = pCommandToSend;
    EdmPhcPsd.bytesInSource           = PSDBL_MSG_HDR_LEN;
    EdmPhcPsd.ptrToCallersDestBuffer  = pMfgBuff_Big;


    // Writing to this Variable, sets bobs local XfrControl ptr to point
    //  to this XfrControl struct, and kicks off the script which 
    //  writes the message, and saves the reply.
    ubBobStatus = fnWriteData( BOBAMAT0,                       // GroupID
                                BOBID_IPSD_BL_PASSTHROUGH_MSG,  // Bariable ID
                                &EdmPhcPsd );                   // Source

    // The Passthrough script saves the reply: 
    //  The data is just the string.
    uwRspLen = EdmPhcPsd.bytesWrittenToDest[0];
    ubRspCmd = pMfgBuff_Big[ PSDBL_REPLY_CMD_OFFS ];
    eRspCode = pMfgBuff_Big[ PSDBL_REPLY_CODE_OFFS ];
    pData = (char *)&pMfgBuff_Big[ PSDBL_REPLY_DATA_OFFS ];
    // Make sure any string is null-terminated.
    pMfgBuff_Big[ uwRspLen ] = 0;

    // Verify Reply: 
    // A valid reply from the boot loader must be at least 2 bytes long: 
    //      the command byte echo and the "code" (status)
    //      Also verify that the command is the same as the command sent.
    if(   (ubBobStatus == BOB_OK)
       && (uwRspLen >= PSDBL_REPLY_HDR_LEN) 
       && (ubRspCmd == pCommandToSend[0]) )    
    {
        // Length of string is bytesWrittenToDest - 2 bytes: cmd and code.
        uwRspLen -= PSDBL_REPLY_HDR_LEN;   

        // The data should be a single, null-terminated string.
        // The null-terminator may or may not have been part of the original string.
        if( uwRspLen && (uwRspLen != (UINT16)strlen(pData) ) && (uwRspLen != (UINT16)strlen(pData) +1) )
        {
            // IF the length is bad, then don't include the string.
            uwRspLen = 0; 
            if( eRspCode == GBLDR_STS_NO_ERROR ) 
                eRspCode = GBLDR_STS_MICROINFO_STRLEN_ERR;             
        }
    }
    else
    {
        // Did not get a valid response from the Boot Loader.
        eRspCode = GBLDR_STS_BAD_RESPONSE;
        uwRspLen = 0;
    }

    // Convert 1-byte Status to a Hex string:
    sprintf( pStatusCodeStr, "0x%2.2X", eRspCode);
    cJSON_AddStringToObject( rspMsg, "StsCode", pStatusCodeStr );

    // If the microInfo string is valid, add it.
    if( uwRspLen > 0 )
    {
        cJSON_AddStringToObject( rspMsg, "MicroStatus", pData );
    }

	ADD_TEST_ID_TO_RESPONSE( rspMsg );

    generateResponse( &entry, rspMsg, "PSDBLGetMicroStatus", gbldr_status_to_string_tbl[eRspCode], root);

}

// ----------------------------------------------------------------------------[on_PSDBLCalcCRCProgSpace]--

void on_PSDBLCalcCRCProgSpace( UINT32 handle, cJSON *root )
{
    const UINT8 *pCommandToSend;
    UINT32      ulCRC = 0;
    UINT16      uwRspLen = 0;
    UINT8       ubBobStatus;
    UINT8       ubRspCmd;
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    char        pStatusCodeStr[ 6 ]; // Holds ASCII of 1-byte hex: "0x12"
    char        pCRCStr[ 12 ];      // Holds ASCII of CRC: "0x12345678" 

    pCommandToSend = pBLCalcProgCRC;

    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    eRspCode = fnCheckBootLoaderModeAvailble( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Setup Transfer Control Function to send message to PSD, and 
        // get response in pMfgBuff_Small.
        EdmPhcPsd.ptrToDataToSendToDevice = pCommandToSend;
        EdmPhcPsd.bytesInSource           = PSDBL_MSG_HDR_LEN;
        EdmPhcPsd.ptrToCallersDestBuffer  = pMfgBuff_Small;


        // Writing to this Variable, sets bobs local XfrControl ptr to point
        //  to this XfrControl struct, and kicks off the script which 
        //  writes the message, and saves the reply, and regets the MicroStatus!
        ubBobStatus = fnWriteData( BOBAMAT0,                       // GroupID
                                    BOBID_IPSD_BL_PASSTHROUGH_MSG,  // Bariable ID
                                    &EdmPhcPsd );                   // Source

        // The Passthrough script saves the reply: 
        //  The data is the 4-byte CRC.
        uwRspLen = EdmPhcPsd.bytesWrittenToDest[0];
        ubRspCmd = pMfgBuff_Small[ PSDBL_REPLY_CMD_OFFS ];
        eRspCode = pMfgBuff_Small[ PSDBL_REPLY_CODE_OFFS ];

        // Verify Reply: 
        // A valid reply from the boot loader must be at least 2 bytes long: 
        //      the command byte echo and the "code" (status)
        //      Also verify that the command is the same as the command sent.
        if(   (ubBobStatus == BOB_OK)
           && (uwRspLen >= PSDBL_REPLY_HDR_LEN) 
           && (ubRspCmd == pCommandToSend[0]) 
           && (eRspCode == GBLDR_STS_NO_ERROR) ) 
        {
            // Length of response data is bytesWrittenToDest - 2 bytes: cmd and code.
            uwRspLen -= PSDBL_REPLY_HDR_LEN; 
            // Verify size of CRC:  
            if( uwRspLen < PSDBL_CRC_SIZE )
            {
                // Did not get a valid response from the Boot Loader.
                eRspCode = GBLDR_STS_BAD_RESPONSE;
            }
            else
                memcpy( &ulCRC, &pMfgBuff_Small[PSDBL_REPLY_DATA_OFFS], PSDBL_CRC_SIZE );
        }
        else if( eRspCode == GBLDR_STS_NO_ERROR )
        {
           // Did not get a valid response from the Boot Loader.
           eRspCode = GBLDR_STS_BAD_RESPONSE;
        }
    } // End of BootLoader is Available.

    // Convert 1-byte Status to a Hex string:
    sprintf( pStatusCodeStr, "0x%2.2X", eRspCode);
    cJSON_AddStringToObject( rspMsg, "StsCode", pStatusCodeStr );

    // If the CRC Code is valid string is valid, add it.
    if( uwRspLen >= 4 )
    {
        // Convert 4-byte CRC Code to a Hex string:  Need to change Endianess
        sprintf( pCRCStr, "%8.8X", (uint)EndianSwap32(ulCRC) );
        cJSON_AddStringToObject( rspMsg, "CRC", pCRCStr );
    }
    generateResponse( &entry, rspMsg, "PSDBLCalcCRCProgSpace", gbldr_status_to_string_tbl[eRspCode], root);

}



// ----------------------------------------------------------------------------[on_PSDBLExitAppAndMasterErase]--

void on_PSDBLExitAppAndMasterErase( UINT32 handle, cJSON *root )
{
    const UINT8 *pCommandToSend;
    UINT16      uwRspLen = 0;
    UINT8       ubBobStatus;
    UINT8       ubRspCmd;
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    char        pStatusCodeStr[ 6 ]; // Holds ASCII of 1-byte hex: "0x12"

    pCommandToSend = pBLExitAppMstrErase;
   
    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    eRspCode = fnCheckBootLoaderModeAvailble( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Setup Transfer Control Function to send message to PSD, and 
        // get response in pMfgBuff_Small.
        EdmPhcPsd.ptrToDataToSendToDevice = pCommandToSend;
        EdmPhcPsd.bytesInSource           = PSDBL_MSG_HDR_LEN;
        EdmPhcPsd.ptrToCallersDestBuffer  = pMfgBuff_Small;


        // Writing to this Variable, sets bobs local XfrControl ptr to point
        //  to this XfrControl struct, and kicks off the script which 
        //  writes the message, and saves the reply, and regets the MicroStatus/ProgMode!
        ubBobStatus = fnWriteData( BOBAMAT0,                       // GroupID
                                    BOBID_IPSD_BL_PASSTHROUGH_MSG,  // Bariable ID
                                    &EdmPhcPsd );                   // Source

        // The Passthrough script saves the reply: 
        uwRspLen = EdmPhcPsd.bytesWrittenToDest[0];
        ubRspCmd = pMfgBuff_Small[ PSDBL_REPLY_CMD_OFFS ];
        eRspCode = pMfgBuff_Small[ PSDBL_REPLY_CODE_OFFS ];

        // Verify Reply: 
        // A valid reply from the boot loader must be at least 2 bytes long: 
        //      the command byte echo and the "code" (status)
        //      Also verify that the command is the same as the command sent.
        if(   (ubBobStatus != BOB_OK)
           || (uwRspLen < PSDBL_REPLY_HDR_LEN) 
           || (ubRspCmd != pCommandToSend[0])  )
        {
            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
               // Did not get a valid response from the Boot Loader.
               eRspCode = GBLDR_STS_BAD_RESPONSE;
            }
        }
    } // End of BootLoader is Available.

    // Convert 1-byte Status to a Hex string:
    sprintf( pStatusCodeStr, "0x%2.2X", eRspCode);
    cJSON_AddStringToObject( rspMsg, "StsCode", pStatusCodeStr );
	ADD_TEST_ID_TO_RESPONSE( rspMsg );
    generateResponse( &entry, rspMsg, "PSDBLExitAppAndMasterErase", gbldr_status_to_string_tbl[eRspCode], root);

}


// ----------------------------------------------------------------------------[on_PSDBLEnterAppMode]--

void on_PSDBLEnterAppMode( UINT32 handle, cJSON *root )
{
    const UINT8 *pCommandToSend;
    UINT16      uwRspLen = 0;
    UINT8       ubBobStatus;
    UINT8       ubRspCmd;
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    char        pStatusCodeStr[ 6 ]; // Holds ASCII of 1-byte hex: "0x12"

    pCommandToSend = pBLEnterAppMode;
   
    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    eRspCode = fnCheckBootLoaderModeAvailble( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Setup Transfer Control Function to send message to PSD, and 
        // get response in pMfgBuff_Small.
        EdmPhcPsd.ptrToDataToSendToDevice = pCommandToSend;
        EdmPhcPsd.bytesInSource           = PSDBL_MSG_HDR_LEN;
        EdmPhcPsd.ptrToCallersDestBuffer  = pMfgBuff_Small;


        // Writing to this Variable, sets bobs local XfrControl ptr to point
        //  to this XfrControl struct, and kicks off the script which 
        //  writes the message, and saves the reply, and regets the MicroStatus!
        ubBobStatus = fnWriteData( BOBAMAT0,                       // GroupID
                                    BOBID_IPSD_BL_PASSTHROUGH_MSG,  // Bariable ID
                                    &EdmPhcPsd );                   // Source

        // The Passthrough script saves the reply: 
        uwRspLen = EdmPhcPsd.bytesWrittenToDest[0];
        ubRspCmd = pMfgBuff_Small[ PSDBL_REPLY_CMD_OFFS ];
        eRspCode = pMfgBuff_Small[ PSDBL_REPLY_CODE_OFFS ];

        // Verify Reply: 
        // A valid reply from the boot loader must be at least 2 bytes long: 
        //      the command byte echo and the "code" (status)
        //      Also verify that the command is the same as the command sent.
        if(   (ubBobStatus != BOB_OK)
           || (uwRspLen < PSDBL_REPLY_HDR_LEN) 
           || (ubRspCmd != pCommandToSend[0])  )
        {
            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
               // Did not get a valid response from the Boot Loader.
               eRspCode = GBLDR_STS_BAD_RESPONSE;
            }
        }
    } // End of BootLoader is Available.

    // Convert 1-byte Status to a Hex string:
    sprintf( pStatusCodeStr, "0x%2.2X", eRspCode);
    cJSON_AddStringToObject( rspMsg, "StsCode", pStatusCodeStr );
	ADD_TEST_ID_TO_RESPONSE( rspMsg );

    generateResponse( &entry, rspMsg, "PSDBLEnterAppMode", gbldr_status_to_string_tbl[eRspCode], root);
	
	OSWakeAfter( 50 );

}


// ----------------------------------------------------------------------------[on_PSDBLDisableBootLoader]--
required_fields_tbl_t required_fields_tbl_DisableBootLoader =
    {
      "PSDBLDisableBootLoader", {  "DeviceID"
                                    , NULL }
    };

void on_PSDBLDisableBootLoader( UINT32 handle, cJSON *root )
{
    UINT32      iLoop;
    size_t      wInDataLen;
    size_t      wPsdDataLen;
    const UINT8 *pCommandToSend;
    uint8_t     bVal;
    UINT8       *pDest;
    char        *pSrc;
    UINT16      uwRspLen = 0;
    UINT8       ubBobStatus;
    char        *pHexDeviceID = NULL_PTR;
    UINT8       ubRspCmd;
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    char        pStatusCodeStr[ 6 ]; // Holds ASCII of 1-byte hex: "0x12"

    pCommandToSend = pBLDisableBootLoader;

    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    eRspCode = fnCheckBootLoaderModeAvailble( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Get DeviceID and Data from incoming message
        if( RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_DisableBootLoader) )
        {
            pHexDeviceID = cJSON_GetObjectItem(root, "DeviceID")->valuestring;

            // Verify HexData string is a good length.
            if( pHexDeviceID )  wInDataLen = strlen( pHexDeviceID );
            if( !pHexDeviceID || !wInDataLen || (wInDataLen %4 != 0) )
            {
                eRspCode = GBLDR_STS_HEXDATA_BAD_LEN;
            }

            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
                // Add header to pMfgBuff_Big:
                memcpy( pMfgBuff_Big, pCommandToSend, PSDBL_MSG_HDR_LEN);

                // Convert data from json HexData string to hex in pMfgBuff
                pSrc = pHexDeviceID;
                wPsdDataLen = wInDataLen / 2;
                pDest = pMfgBuff_Big + PSDBL_MSG_HDR_LEN;
                for( iLoop = 0; iLoop < wPsdDataLen; iLoop++ )
                {
                    bVal = hex_char_to_bin( *(pSrc++) ) << 4;
                    bVal += hex_char_to_bin( *(pSrc++) );
                    *pDest++ = bVal;
                }

                // Setup Transfer Control Function to send message to PSD, and 
                // get response in pMfgBuff_Small.
                EdmPhcPsd.ptrToDataToSendToDevice = pMfgBuff_Big;
                EdmPhcPsd.bytesInSource           = PSDBL_MSG_HDR_LEN + (wPsdDataLen);
                EdmPhcPsd.ptrToCallersDestBuffer  = pMfgBuff_Small;

                // Writing to this Variable, sets bobs local XfrControl ptr to point
                //  to this XfrControl struct, and kicks off the script which 
                //  writes the message, and saves the reply, and regets the MicroStatus!
                //  writes the message, and saves the reply, and regets the MicroStatus!
                ubBobStatus = fnWriteData( BOBAMAT0,                       // GroupID
                                            BOBID_IPSD_BL_PASSTHROUGH_MSG,  // Bariable ID
                                            &EdmPhcPsd );                   // Source

                // The Passthrough script saves the reply: 
                uwRspLen = EdmPhcPsd.bytesWrittenToDest[0];
                ubRspCmd = pMfgBuff_Small[ PSDBL_REPLY_CMD_OFFS ];
                eRspCode = pMfgBuff_Small[ PSDBL_REPLY_CODE_OFFS ];

                // Verify Reply: 
                // A valid reply from the boot loader must be at least 2 bytes long: 
                //      the command byte echo and the "code" (status)
                //      Also verify that the command is the same as the command sent.
                if( uwRspLen < PSDBL_REPLY_HDR_LEN  )
                {
                    // Did not get a valid response from the Boot Loader.
                    eRspCode = GBLDR_STS_INVALID_BLRSP_TOO_SHRT;
                }
                else if( ubRspCmd != pCommandToSend[0] )
                {
                   // Did not get a valid response from the Boot Loader.
                   eRspCode = GBLDR_STS_BLCMD_NOT_IN_RSP;
                }
                else if( ubBobStatus != BOB_OK )
                {
                   // Did not get a valid response from the Boot Loader.
                   eRspCode = GBLDR_STS_BLCMD_BOB_SICK;
                }
            }
        } // End message contains required fields.
        else
        {
           // Required Fields were missing from the json command.
           eRspCode = GBLDR_STS_MISSING_REQD_FIELDS;
        }
    } //  End of if BootLoaderModeAvailable.

    // Convert 1-byte Status to a Hex string:
    sprintf( pStatusCodeStr, "0x%2.2X", eRspCode);
    cJSON_AddStringToObject( rspMsg, "StsCode", pStatusCodeStr );
	ADD_TEST_ID_TO_RESPONSE( rspMsg );

    generateResponse( &entry, rspMsg, "PSDBLDisableBootLoader", gbldr_status_to_string_tbl[eRspCode], root);

}


// ----------------------------------------------------------------------------[on_PSDBLEnterAppMode]--
required_fields_tbl_t required_fields_tbl_LoadAndVerify =   
    {
      "PSDBLLoadAndVerify", {  "HexAddress"
                              , "HexData"
                              , NULL }
    };

void on_PSDBLLoadAndVerify( UINT32 handle, cJSON *root )
{
    UINT32      iLoop;
    size_t      wInDataLen;
    size_t      wPsdDataLen;
    UINT8       *pCommandToSend;
    char        *pHexAddress = NULL_PTR;
    char        *pHexData = NULL_PTR;
    uint8_t     bVal;
    UINT8       *pDest;
    char        *pSrc;
    UINT16      uwRspLen = 0;
    UINT8       ubBobStatus;
    UINT8       ubRspCmd;
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    char        pStatusCodeStr[ 6 ]; // Holds ASCII of 1-byte hex: "0x12"

    pCommandToSend = pBLLoadAndVerifyHdr;
   
    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    eRspCode = fnCheckBootLoaderModeAvailble( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Get address and Data from incoming message
        if( RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_LoadAndVerify) )
        {
            pHexAddress = cJSON_GetObjectItem(root, "HexAddress")->valuestring;
            pHexData = cJSON_GetObjectItem(root, "HexData")->valuestring;

            // Check for bad input data:
            if( !pHexAddress || (strlen( pHexAddress ) != (PSDBL_ADDRESS_LEN *2)) )
            {
                eRspCode = GBLDR_STS_ADDRS_WRONG_LEN;   
            }

            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
                if( pHexData )  wInDataLen = strlen( pHexData );
                if( !pHexData || !wInDataLen || (wInDataLen %4 != 0) )
                {
                    eRspCode = GBLDR_STS_HEXDATA_BAD_LEN;   
                }
            }

            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
                // Convert Address from json Address string to hex in GBL header
                pDest = pCommandToSend;
                pDest++;
                pSrc = pHexAddress;
                for( iLoop = 0; iLoop < PSDBL_ADDRESS_LEN; iLoop++ )
                {
                    bVal = hex_char_to_bin( *(pSrc++) ) << 4;
                    bVal += hex_char_to_bin( *(pSrc++) );
                    *pDest++ = bVal;               
                }

                // Add header to pMfgBuff_Big:
                memcpy( pMfgBuff_Big, pCommandToSend, PSDBL_MSG_HDR_LEN);

                // Convert data from json HexData string to hex in pMfgBuff
                pSrc = pHexData;
                wPsdDataLen = wInDataLen / 2;
                pDest = pMfgBuff_Big + PSDBL_MSG_HDR_LEN;
                for( iLoop = 0; iLoop < wPsdDataLen; iLoop++ )
                {
                    bVal = hex_char_to_bin( *(pSrc++) ) << 4;
                    bVal += hex_char_to_bin( *(pSrc++) );
                    *pDest++ = bVal;               
                }

                // Setup Transfer Control Function to send the message in the pMfgBuff to
                //  PSD, and then get response in pMfgBuff.
                EdmPhcPsd.ptrToDataToSendToDevice = pMfgBuff_Big;
                EdmPhcPsd.bytesInSource           = PSDBL_MSG_HDR_LEN + (wPsdDataLen);
                EdmPhcPsd.ptrToCallersDestBuffer  = pMfgBuff_Small;


                // Writing to this Variable, sets bobs local XfrControl ptr to point
                //  to this XfrControl struct, and kicks off the script which 
                //  writes the message, and saves the reply, and regets the MicroStatus!
                ubBobStatus = fnWriteData( BOBAMAT0,                       // GroupID
                                            BOBID_IPSD_BL_PASSTHROUGH_MSG,  // Bariable ID
                                            &EdmPhcPsd );                   // Source

                // The Passthrough script saves the reply: 
                uwRspLen = EdmPhcPsd.bytesWrittenToDest[0];
                ubRspCmd = pMfgBuff_Small[ PSDBL_REPLY_CMD_OFFS ];
                eRspCode = pMfgBuff_Small[ PSDBL_REPLY_CODE_OFFS ];

                // Verify Reply: 
                // A valid reply from the boot loader must be at least 2 bytes long: 
                //      the command byte echo and the "code" (status)
                //      Also verify that the command is the same as the command sent.
                if(   (ubBobStatus != BOB_OK)
                   || (uwRspLen < PSDBL_REPLY_HDR_LEN) 
                   || (ubRspCmd != pCommandToSend[0])  )
                {
                    if( eRspCode == GBLDR_STS_NO_ERROR )
                    {
                       // Did not get a valid response from the Boot Loader.
                       eRspCode = GBLDR_STS_BAD_RESPONSE;
                    }
                }
            }
        }
        else
        {
            // Required Fields were missing from the json command.
            eRspCode = GBLDR_STS_MISSING_REQD_FIELDS;
        }
    } //  End of if BootLoaderModeAvailable.

    // Convert 1-byte Status to a Hex string:
    sprintf( pStatusCodeStr, "0x%2.2X", eRspCode);
    cJSON_AddStringToObject( rspMsg, "StsCode", pStatusCodeStr );
    generateResponse( &entry, rspMsg, "PSDBLLoadAndVerify", gbldr_status_to_string_tbl[eRspCode], root);

}

// ----------------------------------------------------------------------------[on_PSDRunGeminiSelfTest]--
//  CWB - 2017.07.18 - Modified Status field name to match spec.

void on_PSDRunGeminiSelfTest( UINT32 handle, cJSON *root )
{
    UINT8       ubBobStatus;
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    char        pPsdStatusString[ PSD_STATUS_STRING_SZ +1];
    UINT8       b4PsdStatus[4];
     UINT16     wStatusCode = 0;

    // Setup Response Object.
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    // This bobID will run selftests, update progMode if needed, put the 4-byte
    //  PSD CmdStatus into the 4-byte destination array. 
    (void)memset( pPsdStatusString, 0, sizeof( pPsdStatusString ) );
    (void)memset( &b4PsdStatus, 0, sizeof( b4PsdStatus ) );
    ubBobStatus = fnValidData( BOBAMAT0, BOBID_RUNSELFTESTS_GETSTATUS, &b4PsdStatus );

    // Did it work?
    if( ubBobStatus != BOB_OK )
    {
         // Did not get a valid response from the Boot Loader.
        eRspCode = GBLDR_STS_SELF_TEST_FAILED;
    }
    // RunSelfTests script uses 4 byte Status:
    // First two bytes: MegabobState (high byte first) 
    //  00,00 = Ran with no errors.  
    //  90,01 = Skipped test because wrong ProgMode. 
    //  Anything else is the BobError returned. 
    fnValidData( BOBAMAT0, PSD_XPORT_MSG_STATUS, (void *)&b4PsdStatus ); 
    if( (b4PsdStatus[0] == 0x90)  && (b4PsdStatus[1] == 0x01) )
    {
        eRspCode = GBLDR_STS_SELFTESTS_SKIPPED;
		// Put the progMode into the reply.
        (void)fnCheckPSDProgModeState( rspMsg, ADDIF_ANYMODE );
    }
    else if( (b4PsdStatus[0] == 0x00)  && (b4PsdStatus[1] == 0x00) )
    {
		// The second word contains the PSD status. High byte first.
        wStatusCode = EndianSwap16( *(UINT16 *)&b4PsdStatus[2] );
        if( wStatusCode != 0x9000 )
            eRspCode = GBLDR_STS_SELF_TEST_FAILED;

        memset( pPsdStatusString, 0, sizeof( pPsdStatusString ) ); 
        sprintf( pPsdStatusString, "%4.4X", wStatusCode);
        cJSON_AddStringToObject( rspMsg, "StsCode", pPsdStatusString );
    }
 	else // Top two bytes contain the bob error (BOB_ from orbit.h, not the JBOB_)
    {
        wStatusCode = EndianSwap16( *(UINT16 *)&b4PsdStatus[0] );
        eRspCode = GBLDR_STS_SELF_TEST_FAILED;

        memset( pPsdStatusString, 0, sizeof( pPsdStatusString ) ); 
        sprintf( pPsdStatusString, "%4.4X", wStatusCode);
        cJSON_AddStringToObject( rspMsg, "PSDCommErrCode", pPsdStatusString );
    }

	ADD_TEST_ID_TO_RESPONSE( rspMsg );

    generateResponse( &entry, rspMsg, "PSDRunGeminiSelfTest", gbldr_status_to_string_tbl[eRspCode], root);
}

// ----------------------------------------------------------------------------[on_PSDLoadTransportKey]--
required_fields_tbl_t required_fields_tbl_LoadTransportKey =
    {
      "PSDLoadTransportKey", { "HexData"
                              , NULL }
    };


void on_PSDLoadTransportKey( UINT32 handle, cJSON *root )
{
    UINT32      iLoop;
    size_t      wInDataLen;
    size_t      wPsdDataLen;
    char        *pHexData = NULL_PTR;
    UINT8       bVal;
    UINT8       *pDest;
    char        *pSrc;
    UINT8       ubBobStatus;
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    char        pPsdStatusString[ PSD_STATUS_STRING_SZ +1 ];
    UINT8       pTransportLockDLKey[ IPSD_SZ_TRANSPORT_LOCK_KEY + 1 ]; // Data to download to IPSD

    memset(pPsdStatusString, 0x0, sizeof(pPsdStatusString));
    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    // Check that i-button is running in an App mode.
    eRspCode = fnCheckIpsdInAppMode( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Get Data from incoming message
        if( RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_LoadTransportKey) )
        {

            pHexData = cJSON_GetObjectItem(root, "HexData")->valuestring;

            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
                if( pHexData )
                    wInDataLen = strlen( pHexData );

                if( !pHexData || !wInDataLen || (wInDataLen %4 != 0) )
                {
                    eRspCode = GBLDR_STS_HEXDATA_BAD_LEN;
                }
            }

            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
                memset( pTransportLockDLKey, 0x0, sizeof(pTransportLockDLKey) );
                // Convert data from json HexData string to hex in pMfgBuff
                pSrc = pHexData;
                wPsdDataLen = wInDataLen / 2;
                pDest = pTransportLockDLKey;
                for( iLoop = 0; iLoop < wPsdDataLen; iLoop++ )
                {
                    bVal = hex_char_to_bin( *(pSrc++) ) << 4;
                    bVal += hex_char_to_bin( *(pSrc++) );
                    *pDest++ = bVal;
                }

                // Set up Pointer to the Hash Record and length of same
                EdmPhcPsd.ptrToDataToSendToDevice = pTransportLockDLKey;
                EdmPhcPsd.bytesInSource           = IPSD_SZ_TRANSPORT_LOCK_KEY;

                // Set up Pointer to the response buffer
                //  The message does NOT put the status here. (see tealeavs.c)
                EdmPhcPsd.ptrToCallersDestBuffer  = pMfgBuff_Small;

                // Send the data to the Bob Task
                ubBobStatus = fnWriteData( BOBAMAT0, BOBID_LD_IPSD_TRANSPORT_LOCK_KEY, &EdmPhcPsd ); 

                // Verify Reply:
                if(ubBobStatus != BOB_OK)
                {
                    // Did not get a valid response from the Boot Loader.
                   eRspCode = GBLDR_STS_TRANS_RESPONSE;
                    memset( pPsdStatusString, 0, sizeof( pPsdStatusString ) ); 
                    fnPackedBcdToAsciiBcd( pPsdStatusString, &pIPSD_savedStatus[2], 2 );
                    cJSON_AddStringToObject( rspMsg, "StsCode", pPsdStatusString );
                }
                else
                {
                    // Should do a fnValidData, but this data is not in BOBAMAT table.
                    //  Skipping the first 2 bytes, which are zero, and converting the 
                    //  second 2 bytes, which should be the PSD status after the last 
                    //  packet was sent. 
                    memset( pPsdStatusString, 0, sizeof( pPsdStatusString ) ); 
                    fnPackedBcdToAsciiBcd( pPsdStatusString, &pIPSD_savedStatus[2], 2 );
                    cJSON_AddStringToObject( rspMsg, "StsCode", pPsdStatusString );
                }
            }
        }
        else
        {
            // Required Fields were missing from the json command.
            eRspCode = GBLDR_STS_MISSING_REQD_FIELDS;
        }
    } // End of AppMode check
	ADD_TEST_ID_TO_RESPONSE( rspMsg );
    
    generateResponse( &entry, rspMsg, "PSDLoadTransportKey", gbldr_status_to_string_tbl[eRspCode], root);

}



// ----------------------------------------------------------------------------[on_PSDGetChallengeNonce]--
// This gets the Nonce (from the PSD) that is required for use in TransportAuthenticate 
//  and other messages.
// The nonce is only good for the next message to the PSD.
// When fnValidData returns, the NonceRsp has 2 0's, the 2-byte PSD rsp status, and the 
//  8-byte Nonce.
#define BOBRSP_2B_STATUS_OFFSET     2
#define BOBRSP_NONCE_OFFSET         4
#define ACTUAL_NONCE_LEN            8


void on_PSDGetChallengeNonce( UINT32 handle, cJSON *root )
{
    UINT8   pNonceRsp[ PSD_NONCE_LEN ];
    char    pNonceAscii[ (ACTUAL_NONCE_LEN *2) +1 ];
    char    pPsdStatusString[ PSD_STATUS_STRING_SZ +1 ];
    UINT8   ubBobStatus;    // BOB_OK or not
    UINT16  uwStatus = 0x00FF;      // Endian Swapped PSD status for adding to json.
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;

    memset( pPsdStatusString, 0x0, sizeof(pPsdStatusString) );

    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    // Check that i-button is running in an App mode.
    eRspCode = fnCheckIpsdInAppMode( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Ask Bob to retrieve Nonce data from IPSD, Bob placing Status & Nonce in 
        //  Nonce[] array                
        ubBobStatus = fnValidData( BOBAMAT0, BOBID_MFG_GET_IPSD_CHALLENGE_AND_STAT, 
                                   (void *)pNonceRsp );

        // Convert 2-byte PSDResponse Status Code to a Hex string.  
        // Need to change Endianess if we use sprintf.
        memcpy( &uwStatus, &pNonceRsp[BOBRSP_2B_STATUS_OFFSET], 2 );
        sprintf( pPsdStatusString, "%4.4X", EndianSwap16(uwStatus) );
        cJSON_AddStringToObject( rspMsg, "StsCode", pPsdStatusString );
        
        // If Bob was successful...
        if( ubBobStatus == BOB_OK )
        {
            // Convert 8-byte Nonce to a Hex string, then add to response  
            memset( pNonceAscii, 0, sizeof( pNonceAscii ) );
            fnPackedBcdToAsciiBcd( pNonceAscii, &pNonceRsp[BOBRSP_NONCE_OFFSET], 
                                   ACTUAL_NONCE_LEN );
            cJSON_AddStringToObject( rspMsg, "HexNonce", pNonceAscii );
        }
        else 
        {
            eRspCode = GBLDR_STS_FAILURE;
        }
    } // End of AppMode check
    
	ADD_TEST_ID_TO_RESPONSE( rspMsg );
    generateResponse( &entry, rspMsg, "PSDGetChallengeNonce", gbldr_status_to_string_tbl[eRspCode], root );
}


// ----------------------------------------------------------------------------[on_PSDTransportUnlock]--
// This Uses the Encrypted Nonce to unlock the PSD from Transport mode.
required_fields_tbl_t required_fields_tbl_TransportUnlock =
    {
      "PSDTransportUnlock", { "HexAuthCode"
                              , NULL }
    };

void on_PSDTransportUnlock( UINT32 handle, cJSON *root )
{
    UINT32      iLoop;
    size_t      wInDataLen;
    char        *pHexAuthData;
    char        *pSrc;
    UINT8       *pDest;
    UINT8       pEncryptedNonce[ACTUAL_NONCE_LEN];
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    UINT16      uwStatus = 0x00FF;      // Endian Swapped PSD status for adding to json.
    char        pPsdStatusString[ PSD_STATUS_STRING_SZ +1 ];
    UINT8       ubBobStatus;    // BOB_OK or not
    UINT8       bVal;

    memset( pPsdStatusString, 0x0, sizeof(pPsdStatusString) );

    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    // Check that i-button is running in an App mode.
    eRspCode = fnCheckIpsdInAppMode( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Get Data from incoming message
        if( RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_TransportUnlock) )
        {
            pHexAuthData = cJSON_GetObjectItem(root, "HexAuthCode")->valuestring;

            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
                if( pHexAuthData )
                    wInDataLen = strlen( pHexAuthData );

                if( !pHexAuthData || !wInDataLen || (wInDataLen != (ACTUAL_NONCE_LEN *2)) )
                {
                    eRspCode = GBLDR_STS_HEXDATA_BAD_LEN;
                }
            }
            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
                memset( pEncryptedNonce, 0x0, sizeof(pEncryptedNonce) );
                // Convert data from json pHexAuthCode string to hex in pEncryptedNonce
                pSrc = pHexAuthData;
                pDest = pEncryptedNonce;
                for( iLoop = 0; iLoop < ACTUAL_NONCE_LEN; iLoop++ )
                {
                    bVal = hex_char_to_bin( *(pSrc++) ) << 4;
                    bVal += hex_char_to_bin( *(pSrc++) );
                    *pDest++ = bVal;
                }

                // Writes the Encrypted Nonce to a buffer, then sends the TransportUnlock
                //  command to the IPSD. 
                ubBobStatus = fnWriteData( BOBAMAT0, SET_PSD_XPRT_AUTH, &pEncryptedNonce );           

                // Get 2-byte PSDResponse Status Code and add it
                //  to the response. 
                uwStatus = fnGetIPsdStatusRsp();  
                sprintf( pPsdStatusString, "%4.4X", (uint)uwStatus );
                cJSON_AddStringToObject( rspMsg, "StsCode", pPsdStatusString );

                // Verify Reply:
                if( ubBobStatus != BOB_OK )
                {
                    // Did not get a valid response from the PSD.
                    eRspCode = GBLDR_STS_TRANS_UNLOCK_FAIL;
                }
            }
        }
        else
        {
            // Required Fields were missing from the json command.
            eRspCode = GBLDR_STS_MISSING_REQD_FIELDS;
        }
    } // End of AppMode check
	ADD_TEST_ID_TO_RESPONSE( rspMsg );
    
    generateResponse( &entry, rspMsg, "PSDTransportUnlock", gbldr_status_to_string_tbl[eRspCode], root );
}



// ----------------------------------------------------------------------------[on_PSDLoadProviderKey]--
// This Uses the Encrypted Nonce to unlock the PSD from Transport mode.
required_fields_tbl_t required_fields_tbl_LoadProviderKey =
    {
      "PSDLoadProviderKey", {  "NumBytes"
                             , "HexData"
                             , NULL }
    };


void on_PSDLoadProviderKey( UINT32 handle, cJSON *root )
{
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    UINT32      iLoop;
    size_t      wInDataLen;
    UINT16      wNumBytes = 0;
    UINT16      uwStatus; 
    char        *pHexData = NULL_PTR;
    UINT8       *pDest;
    char        *pSrc;
    char        pPsdStatusString[ PSD_STATUS_STRING_SZ +1 ];
    UINT8       ubBobStatus;
    UINT8       bVal;

    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    // Check that i-button is running in an App mode.
    eRspCode = fnCheckIpsdInAppMode( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Get NumBytes and Data from incoming message
        if( RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_LoadProviderKey) )
        {
            wNumBytes = (UINT16)cJSON_GetObjectItem(root, "NumBytes")->valueint;
            pHexData = cJSON_GetObjectItem(root, "HexData")->valuestring;
            wInDataLen = strlen( pHexData );

            if( !wNumBytes || ((wNumBytes * 2) > (wInDataLen)) )
            {   //If hex byte string too short...
                eRspCode = GBLDR_STS_BAD_INPUT_LEN;   
            }
            else if( wNumBytes > sizeof(pMfgBuff_Big) )
            {
                // Check for internal buffers too short 
                // (If we get this error, the code needs to be modified.)
                eRspCode = GBLDR_STS_INDATA_TOO_LONG; 
            }

            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
                // Clear pMfgBuff_Big:
                memset( pMfgBuff_Big, 0, sizeof(pMfgBuff_Big));

                // Convert data from json HexData string to hex in pMfgBuff_Big
                pSrc = pHexData;
                pDest = pMfgBuff_Big;
                for( iLoop = 0; iLoop < wNumBytes; iLoop++ )
                {
                    bVal = hex_char_to_bin( *(pSrc++) ) << 4;
                    bVal += hex_char_to_bin( *(pSrc++) );
                    *pDest++ = bVal;               
                }

                // Setup Transfer Control Function to send the message in the pMfgBuff to
                //  PSD, and then get response in pMfgBuff.
                EdmPhcPsd.ptrToDataToSendToDevice = pMfgBuff_Big;
                EdmPhcPsd.bytesInSource           = wNumBytes;
                EdmPhcPsd.ptrToCallersDestBuffer  = pMfgBuff_Small;


                // Writing to this Variable, sets bobs local XfrControl ptr to point
                //  to this XfrControl struct, and kicks off the script which 
                //  writes the message.  The status is NOT written to the xfer control 
                //  structure, but should still be in pIPSD_cmdStatusResp.
                ubBobStatus = fnWriteData( BOBAMAT0, IPSD_LD_PROV_KEY, &EdmPhcPsd );                  

                // Get 2-byte swapped PSDResponse Status Code.  
                uwStatus = fnGetIPsdStatusRsp();  
                sprintf( pPsdStatusString, "%4.4X", (uint)uwStatus );
                cJSON_AddStringToObject( rspMsg, "StsCode", pPsdStatusString );

                // Verify Reply:
                if( ubBobStatus != BOB_OK )
                {
                    // Did not get a valid response from the PSD.
                    eRspCode = GBLDR_STS_FAILURE;
                }
            }
        }
        else
        {
            // Required Fields were missing from the json command.
            eRspCode = GBLDR_STS_MISSING_REQD_FIELDS;
        }
    } // End of AppMode check
	ADD_TEST_ID_TO_RESPONSE( rspMsg );

    generateResponse( &entry, rspMsg, "PSDLoadProviderKey", gbldr_status_to_string_tbl[eRspCode], root );
}


// ----------------------------------------------------------------------------[on_PSDCalcClockOffset]--
// This Uses the Encrypted Nonce to unlock the PSD from Transport mode.
required_fields_tbl_t required_fields_tbl_CalcClockOffset =
    {
      "PSDCalcClockOffset", {  "GMT"
                              , NULL }
    };


void on_PSDCalcClockOffset( UINT32 handle, cJSON *root )
{
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    char        *pDateTimeIn;
    char        *pTimeIn;
    char        pClockOffsetString[25]; 
    char        pPsdStatusString[ PSD_STATUS_STRING_SZ +1 ];
    DATETIME    sDateTime;
    UINT32      ulPsdSeconds_IEndian;
    UINT32      ulPsdSeconds;
    UINT32      ulGMTSeconds;
    UINT32      ulClockOffsetSecs = 0;
    UINT16      uwStatus; 
    UINT8       ubBobStatus;

    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    // Check that i-button is running in an App mode.
    eRspCode = fnCheckIpsdInAppMode( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Get the IPSD RTC (seconds since "start")
        ubBobStatus = fnValidData( BOBAMAT0, BOBID_IPSD_RTC, &ulPsdSeconds_IEndian );
        EndianAwareCopy( (char *)&ulPsdSeconds, &ulPsdSeconds_IEndian, IPSD_SZ_TIME );
            
        // Get 2-byte swapped PSDResponse Status Code.  
        uwStatus = fnGetIPsdStatusRsp();  

        if( ubBobStatus == BOB_OK )
        {
            // Get DateTime from request and verify it is a valid date/time.
            if( RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_CalcClockOffset) )
            {
                pDateTimeIn = cJSON_GetObjectItem(root, "GMT")->valuestring;
                pTimeIn = strstr( pDateTimeIn, "T" );
                if( pTimeIn == NULL_PTR )
                {
                    eRspCode = GBLDR_STS_MISSING_TIME;   
                }
                else
                {
                    pTimeIn++; //Advance past the 'T'
                    sDateTime = fnInfraUpdateParseDateTime( pDateTimeIn, pTimeIn );
                    if( fnValidDate( &sDateTime ) == FALSE )
                    {
                        eRspCode = GBLDR_STS_INVALID_DATE_TIME;    
                    }
                }

                // Convert Valid Date Time from  request to IPSD time.
                if( eRspCode == GBLDR_STS_NO_ERROR )
                {
                    // Convert GMT time to IPSD time (seconds since epoch)
                    //  This 
                    fnSysTimeToIPSD( (long *)&ulGMTSeconds, &sDateTime );
                    if( ulGMTSeconds < ulPsdSeconds ) 
                    {
                        // Trying to set date/time to a value before this PSD was started. 
                        eRspCode = GBLDR_STS_DATE_TIME_TOO_EARLY;    
                    }
                    else
                    {
                        ulClockOffsetSecs = ulGMTSeconds - ulPsdSeconds;  
                    }
                }
                // Put offset into reply .
                if( eRspCode == GBLDR_STS_NO_ERROR )
                {
                    //sprintf( pClockOffsetString, "%8.8X", EndianSwap32(ulClockOffsetSecs) );
                    sprintf( pClockOffsetString, "%8.8X", ulClockOffsetSecs );
                    cJSON_AddStringToObject( rspMsg, "HexClockOffset", pClockOffsetString );
                }
                sprintf( pPsdStatusString, "%4.4X", (uint)uwStatus );
                cJSON_AddStringToObject( rspMsg, "StsCode", pPsdStatusString );
            }
            else
            {
                // Required Fields were missing from the json command.
                eRspCode = GBLDR_STS_MISSING_REQD_FIELDS;
            }
        }
        else
        {
            // Read IPSD RTC failed.
            eRspCode = GBLDR_STS_NO_RTC;
            sprintf( pPsdStatusString, "%4.4X", (uint)uwStatus );
            cJSON_AddStringToObject( rspMsg, "StsCode", pPsdStatusString );
        }
    } // End of AppMode check
	ADD_TEST_ID_TO_RESPONSE( rspMsg );

    generateResponse( &entry, rspMsg, "PSDCalcClockOffset", gbldr_status_to_string_tbl[eRspCode], root );
}





// ----------------------------------------------------------------------------[on_PSDInitializeParameters]--
// This Uses the Encrypted Nonce to unlock the PSD from Transport mode.
required_fields_tbl_t required_fields_tbl_PSDInitialize =
    {
      "PSDInitializeParameters", {  "NumBytes"
                                  , "HexData"
                                  , NULL }
    };

void on_PSDInitializeParameters( UINT32 handle, cJSON *root )
{
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    UINT32      iLoop;
    size_t      wInDataLen;
    UINT16      wNumBytes = 0;
    UINT16      uwStatus; 
    UINT32      ulPsdStatus; 
    char        *pHexData = NULL_PTR;
    UINT8       *pDest;
    char        *pSrc;
    char        pPsdStatusString[ PSD_STATUS_STRING_SZ +1 ];
    UINT8       ubBobStatus;
    UINT8       bVal;

    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    // Check that i-button is running in an App mode.
    eRspCode = fnCheckIpsdInAppMode( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Get NumBytes and Data from incoming message
        if( RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_LoadProviderKey) )
        {
            wNumBytes = (UINT16)cJSON_GetObjectItem(root, "NumBytes")->valueint;
            pHexData = cJSON_GetObjectItem(root, "HexData")->valuestring;
            wInDataLen = strlen( pHexData );

            if( !wNumBytes || ((wNumBytes * 2) > (wInDataLen)) )
            {   //If hex byte string too short...
                eRspCode = GBLDR_STS_BAD_INPUT_LEN;   
            }
            else if( wNumBytes > sizeof(pMfgBuff_Big) )
            {
                // Check for internal buffers too short 
                // (If we get this error, the code needs to be modified.)
                eRspCode = GBLDR_STS_INDATA_TOO_LONG; 
            }

            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
                // Clear pMfgBuff_Big:
                memset( pMfgBuff_Big, 0, sizeof(pMfgBuff_Big));

                // Convert data from json HexData string to hex in pMfgBuff_Big
                pSrc = pHexData;
                pDest = pMfgBuff_Big;
                for( iLoop = 0; iLoop < wNumBytes; iLoop++ )
                {
                    bVal = hex_char_to_bin( *(pSrc++) ) << 4;
                    bVal += hex_char_to_bin( *(pSrc++) );
                    *pDest++ = bVal;               
                }
                
                // Save the InitData Glob and the InitData Size
                fnWriteData( BOBAMAT0, BOBID_IPSD_INITDATASIZE, &wNumBytes );
                fnWriteData( BOBAMAT0, BOBID_IPSD_INITDATAGLOB, &pMfgBuff_Big );

                // This calls the script that writes the Initialize messages to the IPSD.
                //  The script detects if German or not, and uses the correct header.
                //  When done, loads the itemPtr with the 4-byte version of the command status.
                ubBobStatus = fnValidData( BOBAMAT0, BOBID_MFG_IPSD_COMMIT_PARAMETER, &ulPsdStatus );  

                // Get 2-byte swapped PSDResponse Status Code.  
                uwStatus = fnGetIPsdStatusRsp();  
                sprintf( pPsdStatusString, "%4.4X", (uint)uwStatus );
                cJSON_AddStringToObject( rspMsg, "StsCode", pPsdStatusString );

                // Verify Reply:
                if( ubBobStatus != BOB_OK )
                {
                    // Did not get a valid response from the PSD.
                    eRspCode = GBLDR_STS_FAILURE;
                }
            }
        }
        else
        {
            // Required Fields were missing from the json command.
            eRspCode = GBLDR_STS_MISSING_REQD_FIELDS;
        }
    } // End of AppMode check
	ADD_TEST_ID_TO_RESPONSE( rspMsg );

    generateResponse( &entry, rspMsg, "PSDInitializeParameters", gbldr_status_to_string_tbl[eRspCode], root );
}


// ----------------------------------------------------------------------------[on_PSDGenerateKeys]--
// This sends the Generate Keys command to the PSD with the GeneratePSDKeyData 
//  blob from PCT, which is signed by the provider key.
required_fields_tbl_t required_fields_tbl_PSDGenerateKeys =
    {
      "PSDGenerateKeys", {  "NumBytes"
                          , "HexData"
                          , NULL }
    };


void on_PSDGenerateKeys( UINT32 handle, cJSON *root )
{
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    UINT32      iLoop;
    size_t      wInDataLen;
    UINT16      uwStatus; 
    UINT16      wNumBytes = 0;
    char        *pHexData = NULL_PTR;
    UINT8       *pDest;
    char        *pSrc;
    char        pPsdStatusString[ PSD_STATUS_STRING_SZ +1 ];
    UINT8       ubBobStatus;
    UINT8       bVal;

    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    // Check that i-button is running in an App mode.
    eRspCode = fnCheckIpsdInAppMode( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Get NumBytes and Data from incoming message
        if( RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_PSDGenerateKeys) )
        {
            wNumBytes = (UINT16)cJSON_GetObjectItem(root, "NumBytes")->valueint;
            pHexData = cJSON_GetObjectItem(root, "HexData")->valuestring;
            wInDataLen = strlen( pHexData );

            if( !wNumBytes || ((wNumBytes * 2) > (wInDataLen)) )
            {   //If hex byte string too short...
                eRspCode = GBLDR_STS_BAD_INPUT_LEN;   
            }
            else if( wNumBytes > sizeof(pMfgBuff_Big) )
            {
                // Check for internal buffers too short 
                // (If we get this error, the code needs to be modified.)
                eRspCode = GBLDR_STS_INDATA_TOO_LONG; 
            }

            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
                // Clear pMfgBuff_Big:
                memset( pMfgBuff_Big, 0, sizeof(pMfgBuff_Big));

                // Convert data from json HexData string to hex in pMfgBuff_Big
                pSrc = pHexData;
                pDest = pMfgBuff_Big;
                for( iLoop = 0; iLoop < wNumBytes; iLoop++ )
                {
                    bVal = hex_char_to_bin( *(pSrc++) ) << 4;
                    bVal += hex_char_to_bin( *(pSrc++) );
                    *pDest++ = bVal;               
                }
                
                // Set up the xfer control structure: 
                EdmPhcPsd.ptrToDataToSendToDevice = pMfgBuff_Big;
                EdmPhcPsd.bytesInSource           = wNumBytes;

                // Have bob put the response in the same buffer, overwriting the 
                //  message to the IPSD.
                EdmPhcPsd.ptrToCallersDestBuffer  = pMfgBuff_Big;

                // This calls the script that sends the Generate PSD Keys command to the IPSD.
                //  The script uses the xfer control structure.
                //  When done, loads the itemPtr with the 4-byte version of the command status.
                ubBobStatus = fnValidData( BOBAMAT0, GET_GEN_PSD_KEY_REQ, &EdmPhcPsd );  

                // Get 2-byte swapped Status Code.  
                uwStatus = fnGetIPsdStatusRsp();  
                sprintf( pPsdStatusString, "%4.4X", (uint)uwStatus );
                cJSON_AddStringToObject( rspMsg, "StsCode", pPsdStatusString );

                // Verify Reply:
                if( ubBobStatus != BOB_OK )
                {
                    // Did not get a valid response from the PSD.
                    eRspCode = GBLDR_STS_FAILURE;
                }
                else 
                {
                    // Convert the PSD Public Key Data from Hex to ASCII string:
                    wNumBytes = EdmPhcPsd.bytesWrittenToDest[0];
                    if( wNumBytes > PSDBL_MAX_MSG_BYTES )
                    {
                        // Check for internal buffers too short 
                        // (If we get this error, the code needs to be modified.)
                        eRspCode = GBLDR_STS_OUTDATA_TOO_LONG;
                    }
                }
                if( eRspCode == GBLDR_STS_NO_ERROR )
                {
                    // Put the PSD Public Key Data in the response.
                    memset( pMfgBuff_BigAscii, 0, sizeof( pMfgBuff_BigAscii ) );
                    fnPackedBcdToAsciiBcd( pMfgBuff_BigAscii, pMfgBuff_Big, wNumBytes );
                    cJSON_AddStringToObject( rspMsg, "PSDPublicKeyData", pMfgBuff_BigAscii );
                }
            }
        }
        else
        {
            // Required Fields were missing from the json command.
            eRspCode = GBLDR_STS_MISSING_REQD_FIELDS;
        }
    } // End of AppMode check
	ADD_TEST_ID_TO_RESPONSE( rspMsg );

    generateResponse( &entry, rspMsg, "PSDGenerateKeys", gbldr_status_to_string_tbl[eRspCode], root );
}


// ----------------------------------------------------------------------------[on_PSDGenerateKeys]--
// This sends the MasterErase command to the PSD with the MasterEraseData blob 
//  (which is signed with the Provider Key) from PCT, That blob must contain the 
//  nonce, which should have been retrieved in the most recent message to the PSD.
required_fields_tbl_t required_fields_tbl_PSDMasterErase =
    {
      "PSDMasterErase", {  "NumBytes"
                          , "HexData"
                          , NULL }
    };


void on_PSDMasterErase( UINT32 handle, cJSON *root )
{
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    UINT32      iLoop;
    size_t      wInDataLen;
    UINT16      uwStatus; 
    UINT16      wNumBytes = 0;
    char        *pHexData = NULL_PTR;
    UINT8       *pDest;
    char        *pSrc;
    char        pPsdStatusString[ PSD_STATUS_STRING_SZ +1 ];
    UINT8       ubBobStatus;
    UINT8       bVal;

    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    // Check that i-button is running in an App mode.
    eRspCode = fnCheckIpsdInAppMode( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Get NumBytes and Data from incoming message
        if( RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_PSDMasterErase) )
        {
            wNumBytes = (UINT16)cJSON_GetObjectItem(root, "NumBytes")->valueint;
            pHexData = cJSON_GetObjectItem(root, "HexData")->valuestring;
            wInDataLen = strlen( pHexData );

            if( !wNumBytes || ((wNumBytes * 2) > (wInDataLen)) )
            {   //If hex byte string too short...
                eRspCode = GBLDR_STS_BAD_INPUT_LEN;   
            }
            else if( wNumBytes > sizeof(pMfgBuff_Big) )
            {
                // Check for internal buffers too short 
                // (If we get this error, the code needs to be modified.)
                eRspCode = GBLDR_STS_INDATA_TOO_LONG; 
            }

            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
                // Clear pMfgBuff_Big:
                memset( pMfgBuff_Big, 0, sizeof(pMfgBuff_Big));

                // Convert data from json HexData string to hex in pMfgBuff_Big
                pSrc = pHexData;
                pDest = pMfgBuff_Big;
                for( iLoop = 0; iLoop < wNumBytes; iLoop++ )
                {
                    bVal = hex_char_to_bin( *(pSrc++) ) << 4;
                    bVal += hex_char_to_bin( *(pSrc++) );
                    *pDest++ = bVal;               
                }
                
                // Set up the xfer control structure: 
                EdmPhcPsd.ptrToDataToSendToDevice = pMfgBuff_Big;
                EdmPhcPsd.bytesInSource           = wNumBytes;
                // Doesn't much matter where this points, this isn't used for this command.
                EdmPhcPsd.ptrToCallersDestBuffer  = (UINT8 *)&uwStatus;

                // This calls the script that sends the Mater Erase command to the IPSD.
                //  The script uses the xfer control structure.
                //  The PSD status of the last message is in pIPSD_4ByteCmdStatusResp
                ubBobStatus = fnValidData( BOBAMAT0, MFG_SCRAP_PSD_INIT_3, &EdmPhcPsd );  

                // Get 2-byte swapped Status Code.  
                uwStatus = fnGetIPsdStatusRsp();  
                sprintf( pPsdStatusString, "%4.4X", (uint)uwStatus );
                cJSON_AddStringToObject( rspMsg, "StsCode", pPsdStatusString );

                // Verify Reply:
                if( ubBobStatus != BOB_OK )
                {
                    // Did not get a valid response from the PSD.
                    eRspCode = GBLDR_STS_FAILURE;
                }
            }
        }
        else
        {
            // Required Fields were missing from the json command.
            eRspCode = GBLDR_STS_MISSING_REQD_FIELDS;
        }
    } // End of AppMode check
	ADD_TEST_ID_TO_RESPONSE( rspMsg );

    generateResponse( &entry, rspMsg, "PSDMasterErase", gbldr_status_to_string_tbl[eRspCode], root );
}

// ********************************************************************************
// FUNCTION NAME:               on_GetPSDParameterList()                               
// PURPOSE:     Process the GetPSDParameterList json message.  
//          It should retrieve the ParameterList fresh from the I-button and 
//          return the list as a string of hex bytes in the Json reply.
// INPUTS: 
//      Standard json message primary handler function inputs.    
//      handle - Pointer to handle of graphics file that has already been opened.
//      root - Index into directory of entry to find.
//
// RETURN:      None.
// OUTPUTS:     Sends a reply.
// NOTES:       None 
// HISTORY:
// 2017.07.26 C.Bellamy - Initial
//---------------------------------------------------------------------
void on_PSDGetParameterList( UINT32 handle, cJSON *root )
{
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    UINT8       *pDataSrc;
    UINT16      uwStatus; 
    UINT16      wNumBytes = 0;
    char        pPsdStatusString[ PSD_STATUS_STRING_SZ +1 ];
    UINT8       ubBobStatus;
    UINT8       bRefillType;

    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    // Check that i-button is running in an App mode.
    eRspCode = fnCheckIpsdInAppMode( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Retrieve the PSDParameterList from the iButton.
        // A few different variables will work.  We just chose this one.
        ubBobStatus = fnValidData( BOBAMAT0, PSD_REFILL_TYPE, &bRefillType );  
        // Verify Reply:
        if( ubBobStatus != BOB_OK )
        {
            // Did not get a valid response from the PSD.
            eRspCode = GBLDR_STS_FAILURE;
        }
        else 
        {
            // Now we directly access two bob variables, which we normally should 
            //  NEVER do from a non-bob task.  But hopefully this will only be 
            //  used during PCT!
            // First, the glob size, which is already in correct Endianess.
            wNumBytes = wIPSD_paramListGlobSize;
            pDataSrc = pIPSD_paramListGlob;

            // Get 2-byte swapped Status Code.  
            uwStatus = fnGetIPsdStatusRsp();  
            sprintf( pPsdStatusString, "%4.4X", (uint)uwStatus );
            cJSON_AddStringToObject( rspMsg, "StsCode", pPsdStatusString );
            cJSON_AddNumberToObject( rspMsg, "NumBytes", wNumBytes );
        }

        if( wNumBytes )
        {
            // Convert the PSD Parameter List Data from Hex to ASCII string:
            if( wNumBytes > PSDBL_MAX_MSG_BYTES )
            {
                // Check for internal buffers too short 
                // (If we get this error, the code needs to be modified.)
                eRspCode = GBLDR_STS_OUTDATA_TOO_LONG;
            }
            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
                // Put the PSD Public Key Data in the response.
                memset( pMfgBuff_BigAscii, 0, sizeof( pMfgBuff_BigAscii ) );
                fnPackedBcdToAsciiBcd( pMfgBuff_BigAscii, pDataSrc, wNumBytes );
                cJSON_AddStringToObject( rspMsg, "HexParamData", pMfgBuff_BigAscii );
            }
        }
    } // End of AppMode check
	ADD_TEST_ID_TO_RESPONSE( rspMsg );

    generateResponse( &entry, rspMsg, "PSDGetParameterList", gbldr_status_to_string_tbl[eRspCode], root );
}


// ----------------------------------------------------------------------------[on_PSDLoadEncryptedKeys]--
// This Uses the Encrypted Nonce to unlock the PSD from Transport mode.
required_fields_tbl_t required_fields_tbl_PSDLoadEncryptedKeys =
    {
      "PSDLoadEncryptedKeys", {  "NumBytes"
                               , "HexKeyData"
                               , "KeyType"
                               , NULL }
    };


void on_PSDLoadEncryptedKeys( UINT32 handle, cJSON *root )
{
    gbldr_status_t  eRspCode = GBLDR_STS_NO_ERROR;
    UINT32      iLoop;
    size_t      wInDataLen;
    UINT16      uwStatus; 
    UINT16      wNumBytes = 0;
    char        *pKeyData = NULL_PTR;
    UINT8       *pDest;
    char        *pSrc;
    char        pPsdStatusString[ PSD_STATUS_STRING_SZ +1 ];
    UINT8       ubBobStatus;
    UINT8       bVal;
    UINT8       bKeyType;

    // Setup buffer for response message
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    // Check that i-button is running in an App mode.
    eRspCode = fnCheckIpsdInAppMode( eRspCode );
    if( eRspCode == GBLDR_STS_NO_ERROR )
    {
        // Get NumBytes and Data from incoming message
        if( RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_PSDLoadEncryptedKeys) )
        {
            bKeyType = (UINT8)cJSON_GetObjectItem(root, "KeyType")->valueint;
            wNumBytes = (UINT16)cJSON_GetObjectItem(root, "NumBytes")->valueint;
            pKeyData = cJSON_GetObjectItem(root, "HexKeyData")->valuestring;
            wInDataLen = strlen( pKeyData );

            if( !wNumBytes || ((wNumBytes * 2) > (wInDataLen)) )
            {   //If hex byte string too short...
                eRspCode = GBLDR_STS_BAD_INPUT_LEN;   
            }
            else if( wNumBytes > sizeof(pMfgBuff_Big) )
            {
                // Check for internal buffers too short 
                // (If we get this error, the code needs to be modified.)
                eRspCode = GBLDR_STS_INDATA_TOO_LONG; 
            }
            // Must set the KeyType before writing the key data to the IPSD.
            if( fnWriteData( BOBAMAT0, BOBID_SET_IPSD_LD_ENCR_KEY_TYPE, &bKeyType ) != BOB_OK )
            {
                eRspCode = GBLDR_STS_FAILURE; 
            }

            if( eRspCode == GBLDR_STS_NO_ERROR )
            {
                // Clear pMfgBuff_Big:
                memset( pMfgBuff_Big, 0, sizeof(pMfgBuff_Big));

                // Convert data from json KeyData string to hex in pMfgBuff_Big
                pSrc = pKeyData;
                pDest = pMfgBuff_Big;
                for( iLoop = 0; iLoop < wNumBytes; iLoop++ )
                {
                    bVal = hex_char_to_bin( *(pSrc++) ) << 4;
                    bVal += hex_char_to_bin( *(pSrc++) );
                    *pDest++ = bVal;               
                }
                
                // Set up the xfer control structure: 
                EdmPhcPsd.ptrToDataToSendToDevice = pMfgBuff_Big;
                EdmPhcPsd.bytesInSource           = wNumBytes;

                // Have bob put the response in the same buffer, overwriting the 
                //  message to the IPSD.
                EdmPhcPsd.ptrToCallersDestBuffer  = pMfgBuff_Big;

                // This calls the script that sends the Generate PSD Keys command to the IPSD.
                //  The script uses the xfer control structure.
                ubBobStatus = fnValidData( BOBAMAT0, PSD_LD_ENCR_KEY_REQ, &EdmPhcPsd );  

                // Get 2-byte swapped Status Code.  
                uwStatus = fnGetIPsdStatusRsp();  
                sprintf( pPsdStatusString, "%4.4X", (uint)uwStatus );
                cJSON_AddStringToObject( rspMsg, "StsCode", pPsdStatusString );

                // Verify Reply:
                if( ubBobStatus != BOB_OK )
                {
                    // Did not get a valid response from the PSD.
                    eRspCode = GBLDR_STS_FAILURE;
                }
                else 
                {
                    // Convert the Verification Data from Hex to ASCII string:
                    wNumBytes = EdmPhcPsd.bytesWrittenToDest[0];
                    if( wNumBytes > PSDBL_MAX_MSG_BYTES )
                    {
                        // Check for internal buffers too short 
                        // (If we get this error, the code needs to be modified.)
                        eRspCode = GBLDR_STS_OUTDATA_TOO_LONG;
                    }
                }
                if( eRspCode == GBLDR_STS_NO_ERROR )
                {
                    cJSON_AddNumberToObject( rspMsg, "KeyType", bKeyType );

                    memset( pMfgBuff_BigAscii, 0, sizeof( pMfgBuff_BigAscii ) );
                    fnPackedBcdToAsciiBcd( pMfgBuff_BigAscii, pMfgBuff_Big, wNumBytes );
                    cJSON_AddStringToObject( rspMsg, "VerificationData", pMfgBuff_BigAscii );

                }
            }
        }
        else
        {
            // Required Fields were missing from the json command.
            eRspCode = GBLDR_STS_MISSING_REQD_FIELDS;
        }
    } // End of AppMode check
	ADD_TEST_ID_TO_RESPONSE( rspMsg );

    generateResponse( &entry, rspMsg, "PSDLoadEncryptedKeys", gbldr_status_to_string_tbl[eRspCode], root );
}

void on_GetTabletInfoReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();
    char *pTemp = "";

	if(fNoTabletConnection == false)
	{
		//Get tablet handle
		uint32_t tablet_handle = wsox_get_tablet_handle();
		if(tablet_handle > 0){
			WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, tablet_handle, {.cjson_info = {rspMsg}}};

		    // get transaction ID, if any, and store it
		    if (cJSON_HasObjectItem(root, "TranID") == 1)
		    {

				//store mfg_handle, Transaction ID and message ID
				mfg_entry.mfg_handle = handle;

		    	// There is a TranID in the req_msg. Copy that
		    	pTemp = cJSON_GetObjectItem(root, "TranID")->valuestring;
		    	strncpy(mfg_entry.transID, pTemp, sizeof(mfg_entry.transID));


		    	strcpy(mfg_entry.msgID, "GetTabletInfoReq");

		    	//Build request to send to Tablet
		    	cJSON_AddStringToObject(rspMsg, "MsgID", "GetFullTabletStatusReq");

		    	cJSON_AddStringToObject(rspMsg, "TranID", pTemp);

		    	BOOL osStat = OSStartTimer(MFG_TABLET_TIMEOUT_TIMER); // Start timer
		    	if (osStat == (BOOL) FAILURE)
		    	{
		    		dbgTrace(DBG_LVL_ERROR, "on_GetTabletInfoReq Error: Cannot start timer");
		    	}
		    	addEntryToTxQueue(&entry, NULL, "  GetTabletInfoReq: Added GetTabletInfoRsp to tx queue. status=%d" CRLF);
		    }
		    else
			{

			    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
			    cJSON_AddStringToObject(rspMsg, "MsgID", "GetTabletInfoRsp");

				cJSON_AddStringToObject(rspMsg, "Status", "TabletNotAvailable");

			    addEntryToTxQueue(&entry, root, "  on_GetTabletInfoReq: Added GetTabletInfoRsp to tx queue. status=%d" CRLF);

			}
		}
		else
		{

		    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
		    cJSON_AddStringToObject(rspMsg, "MsgID", "GetTabletInfoRsp");

			cJSON_AddStringToObject(rspMsg, "Status", "TabletNotAvailable");

		    addEntryToTxQueue(&entry, root, "  on_GetTabletInfoReq: Added GetTabletInfoRsp to tx queue. status=%d" CRLF);

		}
	}
	else
	{

	    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
	    cJSON_AddStringToObject(rspMsg, "MsgID", "GetTabletInfoRsp");

		cJSON_AddStringToObject(rspMsg, "Status", "TabletNotAvailable");

	    addEntryToTxQueue(&entry, root, "  on_GetTabletInfoReq: Added GetTabletInfoRsp to tx queue. status=%d" CRLF);

	}

}


void on_GetFullTabletStatusRsp(UINT32 handle, cJSON *root)
{

	char *pTransID = "";
	char *pStatus = "";
    cJSON *rspMsg = cJSON_CreateObject();
    cJSON *rTabletInfo;

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, mfg_entry.mfg_handle, {.cjson_info = {rspMsg}}};

    if(mfg_entry.mfg_handle == 0)
    {
    	dbgTrace(DBG_LVL_ERROR, "on_GetFullTabletStatusRsp:Error : mfg_entry.mfg_handle %d\n ",mfg_entry.mfg_handle);
    }

	(void)OSStopTimer(MFG_TABLET_TIMEOUT_TIMER);

    //Before sending to PCT check Transaction ID and status
    //Get Transaction ID
    if (cJSON_HasObjectItem(root, "TranID") == 1)
    {
        // There is a TranID in the req_msg. Copy that
    	pTransID = cJSON_GetObjectItem(root, "TranID")->valuestring;
    }

    //Get status
    if (cJSON_HasObjectItem(root, "Status") == 1)
    {
        // There is a TranID in the req_msg. Copy that
    	pStatus = cJSON_GetObjectItem(root, "Status")->valuestring;
    }


    if(strncmp(mfg_entry.transID, pTransID, strlen(mfg_entry.transID)) == 0 && strcmp(pStatus, "Success") == 0 )
    {
		cJSON_AddStringToObject(rspMsg, "MsgID", "GetTabletInfoRsp");

		if (cJSON_HasObjectItem(root, "TabletStatus") == 1){

			rTabletInfo = cJSON_DetachItemFromObject(root,"TabletStatus");
			cJSON_AddItemToObject(rspMsg, "TabletInfo", rTabletInfo);
			cJSON_AddStringToObject(rspMsg, "Status", pStatus);

			addEntryToTxQueue(&entry, root, "  on_GetTabletInfoRsp: Added error response to tx queue." CRLF);
		}
    }
    else
    {
   	    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, mfg_entry.mfg_handle, {.cjson_info = {rspMsg}}};
   	    cJSON_AddStringToObject(rspMsg, "MsgID", "GetTabletInfoRsp");
   	    if(strcmp(mfg_entry.transID, pTransID) == 0)
   	    	cJSON_AddStringToObject(rspMsg, "Status", "TabletFailure");
   	    else
   	    	cJSON_AddStringToObject(rspMsg, "Status", "InvalidTranID");

   	    addEntryToTxQueue(&entry, root, "  on_GetTabletInfoReq: Added GetTabletInfoRsp to tx queue. status=%d" CRLF);
    }
    init_mfg_entry();
}

void mfgTabletStatusTimeOut(unsigned long lwArg)
{
    cJSON *rspMsg = cJSON_CreateObject();
    /* stop this timer so it doesn't automatically retrigger */
    (void)OSStopTimer(MFG_TABLET_TIMEOUT_TIMER);
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, mfg_entry.mfg_handle, {.cjson_info = {rspMsg}}};
    cJSON_AddStringToObject(rspMsg, "MsgID", "GetTabletInfoRsp");
    cJSON_AddStringToObject(rspMsg, "Status", "TabletNotAvailable");
    cJSON_AddStringToObject(rspMsg, "TranID", mfg_entry.transID);

    dbgTrace(DBG_LVL_INFO, "mfgTabletStatusTimeOut:on_GetFullTabletStatusRsp: Timeout");


    addEntryToTxQueue(&entry, NULL, "  on_GetTabletInfoReq: Added GetTabletInfoRsp to tx queue. status=%d" CRLF);

    init_mfg_entry();
}

void init_mfg_entry()
{
	mfg_entry.mfg_handle = 0;
	memset(mfg_entry.msgID, 0x0, sizeof(mfg_entry.msgID));
	memset(mfg_entry.transID, 0x0, sizeof(mfg_entry.transID));
}
