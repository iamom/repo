/**********************************************************************
 PROJECT    :   Comet 
 COPYRIGHT  :   2005, Pitney Bowes, Inc.  
 AUTHOR     :   ST001TE
 MODULE     :   sysdata.c
 
 DESCRIPTION:
    This module contain the common SYS functions for handling and 
    storing system data.

 MODIFICATION HISTORY:fnGetAllStatusInfo

 05-May-14 sa002pe on FPHX 02.10 shelton branch
 	For Fraca 225182:
 	1. Fixed fnGetAllStatusInfo so it would set up the bytes in the correct order
	    required by the Diag Protocol message.
	2. Added fnGetDisabled3.
 
	05/31/2006  Steve Terebesi  Merged from FPHX_01.02_C1_Shelton_ST001TE
    05/25/2006  Steve Terebesi  Added functions fnGetDisabled1(), fnGetDisabled2(),
                                fnGetWarning1()
    05/24/2006  Steve Terebesi  Added functions fnClearAllDisabledStatus(), 
                                fnClearAllWarningStatus(), fnClearAllFlagStatus()
                                Updated fnInitSystemStatus() to call new status 
                                initialization functions

    03/01/2006  Steve Terebesi  Initial version.
 **********************************************************************/
     

/******************************************************************************
        Include Header Files
******************************************************************************/
#include <stdlib.h>
#include <string.h>
#include "sysdata.h"
#include "sysdatadefines.h"


/******************************************************************************
        Local Function Prototypes
******************************************************************************/
static void fnAllocateStatusBytes(const UINT8  ucStatusCount, 
                                  const UINT8* *pStatusPtr);
static BOOL fnGetBitSetting(const UINT32       ulStatusInd, 
                            const UINT8* const pStatusPtr);
static void fnSetBitSetting(const UINT32       ulStatusInd, 
                                  UINT8* const pStatusPtr,
                            const BOOL         fValue);
static void fnGetStatusBitIndex(const UINT32       ulStatusInd, 
                                      UINT8* const pByte, 
                                      UINT8* const pBit);
static UINT32 fnGetAllStatusInfo(const UINT8* const pData,
                                 const UINT8        ucOffset);


/******************************************************************************
        Local Defines, Typedefs, and Variables
******************************************************************************/
static UINT8* PDisabled;       // pointer to disabled status
static UINT8* PWarnings;       // pointer to warning status
static UINT8* PFlags;          // pointer to system flags
static UINT8* PSysData;        // pointer to system data


/******************************************************************************
        Public Functions
******************************************************************************/

/******************************************************************************
FUNCTION NAME:
    fnPostDisabledStatus

DESCRIPTION:
    This function sets the given disabled condition.

INPUTS:
    ulDStatusID         - disabled status indicator

RETURNS:
    None

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    03/01/2006  Steve Terebesi  Initial revision
******************************************************************************/
void fnPostDisabledStatus(const UINT32 ulDStatusID)
{
//  Set the selected disabled status to TRUE    
    fnSetBitSetting(ulDStatusID, PDisabled, TRUE); 
}


/******************************************************************************
FUNCTION NAME:
    fnClearDisabledStatus

DESCRIPTION:
    This function clears the given disabled condition.

INPUTS:
    ulDStatusID         - disabled status indicator

RETURNS:
    None

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    03/01/2006  Steve Terebesi  Initial revision
******************************************************************************/
void fnClearDisabledStatus(const UINT32 ulDStatusID)
{
//  Set the selected disabled status to FALSE    
    fnSetBitSetting(ulDStatusID, PDisabled, FALSE); 
}


/******************************************************************************
FUNCTION NAME:
    fnPostWarningStatus

DESCRIPTION:
    This function sets the given warning condition.

INPUTS:
    ulWStatusID         - warning status indicator

RETURNS:
    None

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    03/01/2006  Steve Terebesi  Initial revision
******************************************************************************/
void fnPostWarningStatus(const UINT32 ulWStatusID)
{
//  Set the given warning status to TRUE    
    fnSetBitSetting(ulWStatusID, PWarnings, TRUE); 
}


/******************************************************************************
FUNCTION NAME:
    fnClearWarningStatus

DESCRIPTION:
    This function clears the given warning condition

INPUTS:
    ulWStatusID         - warning status indicator

RETURNS:
    None

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    11/28/2005  Steve Terebesi  Initial revision
******************************************************************************/
void fnClearWarningStatus(const UINT32 ulWStatusID)
{
//  Set the given warning status to FALSE    
    fnSetBitSetting(ulWStatusID, PWarnings, FALSE); 
}


/******************************************************************************
FUNCTION NAME:
    fnPostSystemFlag

DESCRIPTION:
    This function sets the given system flag.

INPUTS:
    ulFlagID            - system flag indicator

RETURNS:
    None

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    03/01/2006  Steve Terebesi  Initial revision
******************************************************************************/
void fnPostSystemFlag(const UINT32 ulFlagID)
{
//  Set the given system flag to TRUE    
    fnSetBitSetting(ulFlagID, PFlags, TRUE); 
}


/******************************************************************************
FUNCTION NAME:
    fnClearSystemFlag

DESCRIPTION:
    This function clear the given system flag.

INPUTS:
    ulFlagID            - system flag indicator

RETURNS:
    None

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    03/01/2005  Steve Terebesi  Initial revision
******************************************************************************/
void fnClearSystemFlag(const UINT32 ulFlagID)
{
//  Set the given system flag to FALSE    
    fnSetBitSetting(ulFlagID, PFlags, FALSE); 
}


/******************************************************************************
FUNCTION NAME:
    fnIsDisabledStatusSet

DESCRIPTION:
    This function indicates whether the given disabled status is set or not.

INPUTS:
    ulDStatusID         - disabled status indicator

RETURNS:
    TRUE                - given disabled status is set
    FALSE               - given disabled status is not set

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    03/01/2005  Steve Terebesi  Initial revision
******************************************************************************/
BOOL fnIsDisabledStatusSet(const UINT32 ulDStatusID)
{
//  Get the current value for the selected disabled status
    return (fnGetBitSetting(ulDStatusID, PDisabled)); 
}


/******************************************************************************
FUNCTION NAME:
    fnIsWarningStatusSet

DESCRIPTION:
    This function indicates whether the given warning status is set or not.

INPUTS:
    ulWStatusID         - warning status indicator

RETURNS:
    TRUE                - given warning status is set
    FALSE               - given warning status is not set

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    03/01/2006  Steve Terebesi  Initial revision
******************************************************************************/
BOOL fnIsWarningStatusSet(const UINT32 ulWStatusID)
{
//  Get the current value for the selected warning status
    return (fnGetBitSetting(ulWStatusID, PWarnings)); 
}


/******************************************************************************
FUNCTION NAME:
    fnIsSystemFlagSet

DESCRIPTION:
    This function indicates whether the given system flag is set or not.

INPUTS:
    ulFlagID            - system flag indicator

RETURNS:
    TRUE                - given system flag is set
    FALSE               - given system flag is not set

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    03/01/2006  Steve Terebesi  Initial revision
******************************************************************************/
BOOL fnIsSystemFlagSet(const UINT32 ulFlagID)
{
//  Get the current value for the selected system flag
    return (fnGetBitSetting(ulFlagID, PFlags)); 
}


/******************************************************************************
FUNCTION NAME:
    fnAnyDisabledStatusSet

DESCRIPTION:
    This function indicates if any disabled status is set or not.

INPUTS:
    None

RETURNS:
    TRUE                - there is a disabled status set
    FALSE               - there is no disabled status set

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    03/01/2006  Steve Terebesi  Initial revision
******************************************************************************/
BOOL fnAnyDisabledStatusSet(void)
{
    BOOL  fResult = FALSE;
    UINT8 ucByteCount = ((NUMBER_OF_DISABLED - 1) / 8) + 1;
    UINT8 i;


//  Check to see if any byte is nonzero (one or more status bits set)
    for (i = 0; ((i < ucByteCount) && (fResult == FALSE)); i++)
    {
        if (PDisabled[i] != 0)
        {
//          Disabled status bit(s) set
            fResult = TRUE;
        }
    }

    return (fResult);
}


/******************************************************************************
FUNCTION NAME:
    fnAnyWarningStatusSet

DESCRIPTION:
    This function indicates if any warning status is set or not.

INPUTS:
    None

RETURNS:
    TRUE                - there is a warning status set
    FALSE               - there is no warning status set

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    03/01/2005  Steve Terebesi  Initial revision
******************************************************************************/
BOOL fnAnyWarningStatusSet(void)
{
    BOOL  fResult = FALSE;
    UINT8 ucByteCount = ((NUMBER_OF_WARNINGS - 1) / 8) + 1;
    UINT8 i;


//  Check to see if any byte is nonzero (one or more status bits set)
    for (i = 0; ((i < ucByteCount) && (fResult == FALSE)); i++)
    {
        if (PWarnings[i] != 0)
        {
//          Disabled status bit(s) set
            fResult = TRUE;
        }
    }

    return (fResult);
}


/******************************************************************************
FUNCTION NAME:
    fnInitSystemStatus

DESCRIPTION:
    This function initializes the system status storage areas and perform any
    other tasks necessary (i.e. gather status data) to properly handle status.

INPUTS:
    None

RETURNS:
    None

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    03/01/2005  Steve Terebesi  Initial revision
******************************************************************************/
void fnInitSystemStatus(void)
{
    UINT8  i;
    UINT32 ulDataSize;      // size of system data storage area


//  Alocate memory for disbaled status storage
    fnAllocateStatusBytes(NUMBER_OF_DISABLED, (const UINT8 **)&PDisabled);

//  Alocate memory for warning status storage
    fnAllocateStatusBytes(NUMBER_OF_WARNINGS, (const UINT8 **)&PWarnings);

//  Alocate memory for system flag storage
    fnAllocateStatusBytes(NUMBER_OF_FLAGS, (const UINT8 **)&PFlags);

//  Clear Disabled status area
    fnClearAllDisabledStatus();
	fnClearDisabledStatus(DCOND_INSUFFICIENT_FUNDS); //Insufficient funds needs to be manually cleared

//  Clear Warning status area
    fnClearAllWarningStatus();

//  Clear System flag status area
    fnClearAllFlagStatus();

//  Calculate offsets for the system data
    SysDataInfo[0].ulOffset = 0;

    for (i = 1; i < NUMBER_OF_SYSDATA; i++)
    {
        SysDataInfo[i].ulOffset = SysDataInfo[i-1].ucSize + 
                                  SysDataInfo[i-1].ulOffset;
    }

//  Calculate size of system data storage area (last element size + offset)
    ulDataSize = SysDataInfo[NUMBER_OF_SYSDATA - 1].ucSize +
                 SysDataInfo[NUMBER_OF_SYSDATA - 1].ulOffset;

//  Allocate memory for the system data storage
    PSysData = (UINT8 *)malloc(ulDataSize);

//  Call data initialization functions - not implemented yet - do not remove
//    for (i = 0; i < STATUS_INIT_LIST_SIZE; i++)
//    {
//        PInitStatusList[i] ();
//    }

}


/******************************************************************************
FUNCTION NAME:
    fnQuerySystemStatus

DESCRIPTION:
    This function performs any system status gathering required.

INPUTS:
    pFunctList          - pointer to the function list to execute

RETURNS:
    None

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    03/01/2006  Steve Terebesi  Initial revision
******************************************************************************/
void fnQuerySystemStatus(void)
{
    UINT8 i;


//  Call system data query functions
    for (i = 0; i < STATUS_QUERY_LIST_SIZE; i++)
    {
        PQueryStatusList[i]();
    }
}


/******************************************************************************
FUNCTION NAME:
    fnChangeSystemData

DESCRIPTION:
    This function sets the given system data to the given value in the system
    data storage area.

INPUTS:
    ulSysDataInd        - system data indicator
    pValue              - pointer to new value (for input)

RETURNS:
    None

WARNING/NOTES:
    Indexing:
        data offset = SysDataInfo[indicator -1].ulOffset

MODIFICATION HISTORY:
    03/01/2006  Steve Terebesi  Initial revision
******************************************************************************/
void fnChangeSystemData(const UINT32       ulSysDataInd,
                        const UINT8* const pValue)
{
    UINT32 ulOffset;
    UINT8  ucSize;
    
	
//  Check to ensure indicator has a valid value 
    if ((ulSysDataInd > 0) && (ulSysDataInd <= NUMBER_OF_SYSDATA))
    {
//      Calculate data offset and data size
        ulOffset = SysDataInfo[ulSysDataInd - 1].ulOffset;
        ucSize   = SysDataInfo[ulSysDataInd - 1].ucSize;

//      Copy new value to system data storage
        memcpy((void *)&PSysData[ulOffset], pValue, ucSize);
    }
}


/******************************************************************************
FUNCTION NAME:
    fnGetSystemStatus

DESCRIPTION:
    This function gets the value of the given system data from the system
    data storage area.

INPUTS:
    ulSysDataInd        - system data indicator
    pValue              - pointer to new value (for ouput)

RETURNS:
    None

WARNING/NOTES:
    Indexing:
        data offset = SysDataInfo[indicator -1].ulOffset

MODIFICATION HISTORY:
    03/01/2006  Steve Terebesi  Initial revision
******************************************************************************/
void fnGetSystemData(const UINT32       ulSysDataInd,
                           UINT8* const pValue)
{
    UINT32 ulOffset;
    UINT8  ucSize;
    
	
//  Check to ensure indicator has a valid value 
    if ((ulSysDataInd > 0) && (ulSysDataInd <= NUMBER_OF_SYSDATA))
    {
//      Calculate data offset and data size
        ulOffset = SysDataInfo[ulSysDataInd - 1].ulOffset;
        ucSize   = SysDataInfo[ulSysDataInd - 1].ucSize;

//      Copy new value to system data storage
        memcpy((void *)pValue, 
               (void *)&PSysData[ulOffset], 
               ucSize);
    }
}


/******************************************************************************
FUNCTION NAME:
    fnClearAllDisabledStatus

DESCRIPTION:
    This function clears the disabled status area

INPUTS:
    None

RETURNS:
    None

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    05/23/2006  Steve Terebesi  Initial revision
******************************************************************************/
void fnClearAllDisabledStatus(void)
{
    UINT8 i;
    UINT8 ucByteCount;
    BOOL insufFunds;

	insufFunds = fnIsDisabledStatusSet(DCOND_INSUFFICIENT_FUNDS);

//  Determine number of bytes to clear
    ucByteCount = ((NUMBER_OF_DISABLED - 1) / 8) + 1;

//  Clear Disabled status bits
    for (i = 0; i < ucByteCount; i++)
    {
			PDisabled[i] = 0;
    }

	if(insufFunds == TRUE)
	{
		fnPostDisabledStatus(DCOND_INSUFFICIENT_FUNDS);
	}
}


/******************************************************************************
FUNCTION NAME:
    fnClearAllWarningStatus

DESCRIPTION:
    This function clears the warning status area

INPUTS:
    None

RETURNS:
    None

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    05/23/2006  Steve Terebesi  Initial revision
******************************************************************************/
void fnClearAllWarningStatus(void)
{
    UINT8 i;
    UINT8 ucByteCount;
    
	
//  Determine number of bytes to clear
    ucByteCount = ((NUMBER_OF_WARNINGS - 1) / 8) + 1;

//  Clear Warning status bits
    for (i = 0; i < ucByteCount; i++)
    {
        PWarnings[i] = 0;
    }
}


/******************************************************************************
FUNCTION NAME:
    fnClearAllFlagStatus

DESCRIPTION:
    This function clears the flag status area

INPUTS:
    ulWStatusID         - warning status indicator

RETURNS:
    None

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    05/24/2006  Steve Terebesi  Initial revision
******************************************************************************/
void fnClearAllFlagStatus(void)
{
    UINT8 i;
    UINT8 ucByteCount;
    
	
//  Determine number of bytes to clear
    ucByteCount = ((NUMBER_OF_FLAGS - 1) / 8) + 1;

//  Clear Flag status bits
    for (i = 0; i < ucByteCount; i++)
    {
        PFlags[i] = 0;
    }
}


/******************************************************************************
FUNCTION NAME:
    fnGetDisabled1

DESCRIPTION:
    This function get the first set (32) of disabled status bits 

INPUTS:
    None

RETURNS:
    UINT32              - 4 bytes of status bit data

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    05/25/2006  Steve Terebesi  Initial revision
******************************************************************************/
UINT32 fnGetDisabled1(void)
{
    UINT32 ulDisabled1;
    
	
//  Retrieve disabled status info
    ulDisabled1 = fnGetAllStatusInfo(PDisabled, 0);

    return (ulDisabled1);
}


/******************************************************************************
FUNCTION NAME:
    fnGetDisabled2

DESCRIPTION:
    This function get the next set (32) of disabled status bits 

INPUTS:
    None

RETURNS:
    UINT32              - 4 bytes of status bit data

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    05/25/2006  Steve Terebesi  Initial revision
******************************************************************************/
UINT32 fnGetDisabled2(void)
{
    UINT32 ulDisabled2;


//  Retrieve disabled status info
    ulDisabled2 = fnGetAllStatusInfo(PDisabled, 4);

    return (ulDisabled2);
}


/******************************************************************************
FUNCTION NAME:
    fnGetDisabled3

DESCRIPTION:
    This function get the third set (32) of disabled status bits 

INPUTS:
    None

RETURNS:
    UINT32              - 4 bytes of status bit data

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    29-Apr-14  S. Peterson  Initial revision
******************************************************************************/
UINT32 fnGetDisabled3(void)
{
    UINT32 ulDisabled3;
   
   
//  Retrieve disabled status info
    ulDisabled3 = fnGetAllStatusInfo(PDisabled, 8);

    return (ulDisabled3);
}


/******************************************************************************
FUNCTION NAME:
    fnGetWarning1

DESCRIPTION:
    This function get the first set (32) of warning status bits 

INPUTS:
    None

RETURNS:
    UINT32              - 4 bytes of status bit data

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    05/25/2006  Steve Terebesi  Initial revision
******************************************************************************/
UINT32 fnGetWarning1(void)
{
    UINT32 ulWarning1;
   
   
//  Retrieve warning status info
    ulWarning1 = fnGetAllStatusInfo(PWarnings, 0);

    return (ulWarning1);
}
 

/******************************************************************************
        Private Functions
******************************************************************************/

/******************************************************************************
FUNCTION NAME:
    fnAllocateStatusBytes

DESCRIPTION:
    This function allocated space for status storage based on the number of
    unique status indicators.

INPUTS:
    ucStatusCount           - number of status indicators
    pStatusPtr              - pointer to a pointer to status storage area

RETURNS:
    None

WARNING/NOTES:
    # bytes to allocate = ((count - 1) / 8) + 1, where count > 0

MODIFICATION HISTORY:
    03/01/2006  Steve Terebesi  Initial revision
******************************************************************************/
static void fnAllocateStatusBytes(const UINT8  ucStatusCount, 
                                  const UINT8* *pStatusPtr)
{
    UINT8 ucBytes;


//  Check if number of bytes to allocate is not zero
    if (ucStatusCount > 0)
    {
//      Calculate number of bytes to allocate
        ucBytes    = ((ucStatusCount - 1) / 8) + 1;

//      Allocate space
        *pStatusPtr = malloc(ucBytes);
    }
    else
    {
//      Set pointer to NULL if no space is allocated
        *pStatusPtr = NULL;
    }
}


/******************************************************************************
FUNCTION NAME:
    fnGetBitSetting

DESCRIPTION:
    This function takes the given status indicator and storage area pointer 
    and returns the requested status bit information (TRUE/FALSE).

INPUTS:
    ulStatusInd             - status indicator
    pStatusPtr              - pointer to status storage area

RETURNS:
    TRUE                    - status bit is set
    FALSE                   - status bit is not set

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    03/01/2006  Steve Terebesi  Initial revision
******************************************************************************/
static BOOL fnGetBitSetting(const UINT32       ulStatusInd, 
                            const UINT8* const pStatusPtr)
{
    BOOL  fResult = FALSE;
    UINT8 ucByte;
    UINT8 ucBit;
    UINT8 ucMask;


//  Get the byte and bit offset of the selected status indicator
    fnGetStatusBitIndex(ulStatusInd, &ucByte, &ucBit);

//  Calculate the status indicator bit mask
    ucMask = 1 << ucBit;

//  Determine the value of the status indicator
    if ((pStatusPtr[ucByte] & ucMask) != 0)
    {
        fResult = TRUE;
    }

    return (fResult);
}


/******************************************************************************
FUNCTION NAME:
    fnSetBitSetting

DESCRIPTION:
    This function takes the given status pointer and sets the requested
    status bit to the give value (TRUE/FALSE).

INPUTS:
    ulStatusInd             - status indicator
    pStatusPtr              - pointer to status storage area
    fValue                  - new value for status bit

RETURNS:
    None

WARNING/NOTES:
    None

MODIFICATION HISTORY:
    11/29/2005  Steve Terebesi  Initial revision
******************************************************************************/
static void fnSetBitSetting(const UINT32       ulStatusInd, 
                                  UINT8* const pStatusPtr,
                            const BOOL         fValue)
{
    UINT8 ucByte;
    UINT8 ucBit;
    UINT8 ucMask;


//  Get the byte and bit offset of the selected status indicator
    fnGetStatusBitIndex(ulStatusInd, &ucByte, &ucBit);

//  Calculate the status indicator bit mask
    ucMask = 1 << ucBit;

//  Set the status bit to the requested value
    if (fValue == TRUE)
    {
//      Set the status bit to TRUE
        pStatusPtr[ucByte] |= ucMask;
    }
    else
    {
//      Set the status bit to FALSE
        pStatusPtr[ucByte] &= ~ucMask;
    }
}


/******************************************************************************
FUNCTION NAME:
    fnGetStatusBitIndex

DESCRIPTION:
    This function takes the given value and uses it to determine the byte and
    bit number of the data to be accessed.

INPUTS:
    ulStatusInd             - status indicator
    pByte                   - pointer to byte number (for output)
    pBit                    - pointer to bit number (for output)

RETURNS:
    None

WARNING/NOTES:
    Indexing:
        Byte # = ((indicator - 1) / 8)
        Bit #  = (indicator - 1) mod 8 

MODIFICATION HISTORY:
    03/01/2006  Steve Terebesi  Initial revision
******************************************************************************/
static void fnGetStatusBitIndex(const UINT32       ulStatusInd, 
                                      UINT8* const pByte, 
                                      UINT8* const pBit)
{
    *pByte = (UINT8)((ulStatusInd - 1) / 8);
    *pBit  = (UINT8)((ulStatusInd - 1) % 8);
}


/******************************************************************************
FUNCTION NAME:
    fnGetAllStatusInfo

DESCRIPTION:
    This function copies 4 bytes of data from the given offest of the given
    status storage area.

INPUTS:
    pData                   - pointer to status storage area
    ucOffset                - offset of requested data in storage area

RETURNS:
    UINT32                  - 4 bytes of status bit data

WARNING/NOTES:
    pData must be valid
	offset must not exceed the data area
	(offset + 3) must not exceed the data area

MODIFICATION HISTORY:
	29-Apr-14	S. Peterson		The bytes were being retrieved in reverse order
	                            of how they were wanted by the Diag Protocol msg.
    05/25/2006  Steve Terebesi  Initial revision
******************************************************************************/
static UINT32 fnGetAllStatusInfo(const UINT8* const pData,
                                 const UINT8        ucOffset)
{
    SINT8  i;
    UINT32 ulStatus = 0;


//  Copy 4 bytes of data from given status area
    for (i = ucOffset + 3; (i >= ucOffset) && (i >= 0); i--)
    {
        ulStatus = ulStatus << 8;
        ulStatus |= pData[i];
    }

    return (ulStatus);
}
