/************************************************************************
   PROJECT:        Horizon CSD
   COMPANY:        Pitney Bowes
   MODULE NAME:    infchk.c
       
   DESCRIPTION:
----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
----------------------------------------------------------------------
*************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "sys/stat.h"
#include "posix.h"
#include "global.h"
#include "sig.h"
#include "sdrtypes.h"
#include "string.h"
#include "tealeavs.h"
#include "bobutils.h"
#include "bob.h"
#include "hash.h"
#include "ipsd.h"
#include "flashutil.h"
#include "pcdisk.h"


extern CMOS_Signature CMOSSignature;

typedef struct _sgnhdr
{
  unsigned char ignore;
  unsigned char numSdrs;
}SGNHDR;

extern const HASH_DATA_TYPE initChainVariable;
extern ERR_CODE ComputeSHA1Hash(UINT8 *pSrc, HASH_DATA_TYPE *pDest, UINT32 numBytes, const HASH_DATA_TYPE *pChain);
extern BOOL ComputeSHA1HashFromFile(FILE *pCertFileHandle, HASH_DATA_TYPE *destHash, long numberOfBytes, const HASH_DATA_TYPE *iChainVariable);

/******************************************************************************
   FUNCTION NAME: fnGetSizeOfOverSign
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Return the length of all the prepended oversign records at
   				: the top of a file.
   PARAMETERS   : file control block, pass back size of oversign header
   RETURN       : total size of all prepended oversigns
   NOTES		: none
******************************************************************************/
long fnGetSizeOfOverSign(INT fcb, long *pOverSignHdrSiz)
{
	long overSize = 0, len,ec;
	HASH_SDR_TYPE hrec;
  	SGNHDR sdrh;
	uchar  sdrType = 0;
	ushort usSdrSz = 0;

	*pOverSignHdrSiz = sizeof(SGNHDR);

	len = NU_Read( fcb, ( void *) &sdrh  , *pOverSignHdrSiz);
	fnCheckFileSysCorruption(len);
	if( len == *pOverSignHdrSiz)
	{
	  	// There are two possible HASH_SDR_TYPEs, but there can only be one SDR in the oversign.

		// point to first SDR rec type
		len = NU_Read( fcb, ( void *) &sdrType, 1);
		fnCheckFileSysCorruption(len);
		// reposition file read pointer to start of first SDR
		ec = NU_Seek(fcb, sizeof(SGNHDR), PSEEK_SET );
		fnCheckFileSysCorruption(ec);
		switch( sdrType )
		{
			case IPSD_ECDSA_VHS_REC_TYPE:
				usSdrSz = IPSD_SZ_ECDSA_VERIFY_HASH_SIGNATURE;
				break;
			case IPSD_DSA_VHS_REC_TYPE:
			default:
				usSdrSz = IPSD_SZ_DSA_VERIFY_HASH_SIGNATURE;
			break;
		}

	    overSize = sdrh.numSdrs * usSdrSz;
	}
	return(overSize);
}

/* **********************************************************************
// FUNCTION NAME:fnVerifyUpdateDownload( char * )
// PURPOSE: Verify a download image. First
//          verify the Signed Data Record at the beginning then
//          verify the hash of the image.
//          Normally if the SDR check for Hash Check fails we delete the download
//          We are leaving now to be able to check why
// AUTHOR: George Monroe
//
// INPUTS: filename
// RETURNS: VERIFY_FILESIG_STATUS enum
// **********************************************************************/

VERIFY_FILESIG_STATUS fnVerifyFileSignature( char *fileName, long *dataOffset, long *dataSize )
{
  unsigned char *hp = 0;
  long filesize,hashsize,skipsize;
  int len,result;
  VERIFY_FILESIG_STATUS rc = VFS_NOT_VERIFIED;
  HASH_SDR_TYPE hrec;
  HASH_DATA_TYPE destHash;
  SGNHDR sdrh;
  ushort usSdrSz = 0;
  FILE *pCertFileHandle = NULL;
  int fileStatus;
  struct stat fileInfo;
  
//  unsigned char testdat[] = { 0x61 , 0x62 , 0x63  };
//  hashRc = ComputeSHA1Hash( testdat , &destHash , 3 , &initChainVariable );

  fileStatus = stat( fileName, &fileInfo );
  if (fileStatus == POSIX_ERROR)
  {// can't find file
      fnCheckFileSysCorruption(fileStatus);
      return VFS_FILE_NOT_EXIST;
  }
  filesize = fileInfo.st_size;

  pCertFileHandle = fopen(fileName,"rb");
  if (pCertFileHandle == NULL)
  {// can't open file      
      fnCheckFileSysCorruption(POSIX_ERROR);
      return VFS_FILE_READ_ERR;
  }

  /* Read in header */
  len = fread((void*)&sdrh, 1, sizeof(SGNHDR), pCertFileHandle);
  if( len != sizeof(SGNHDR))
  {// can't read file      
      fnCheckFileSysCorruption(POSIX_ERROR);
	  fnCheckFileSysCorruption(fclose(pCertFileHandle));
      return VFS_FILE_READ_ERR;
  }
	/* Read in initial SDR from the file */
	// For Mega there is only one HASH_SDR_TYPE.
	// For Janus there are two HASH_SDR_TYPEs.  The one defined in sdrtypes.h is the larger of the two so we'll
	// read in at least as much data as we need here and BOB will figure out which record type it is prior to sending
	// the message to the PSD
  len = fread((void*)&hrec, 1, sizeof(HASH_SDR_TYPE), pCertFileHandle);
  if( len != sizeof(HASH_SDR_TYPE))
  {// can't read file      
      fnCheckFileSysCorruption(POSIX_ERROR);
	  fnCheckFileSysCorruption(fclose(pCertFileHandle));
      return VFS_FILE_READ_ERR;
  }


  hp = (uchar*) &hrec;
  // For Mega there is only one HASH_SDR_TYPE, but there can be multiple SDRs in the oversign.
  // For Janus there are two possible HASH_SDR_TYPEs, but there can only be one SDR in the oversign.
  switch( *hp )
  {
    case IPSD_ECDSA_VHS_REC_TYPE:
	  usSdrSz = IPSD_SZ_ECDSA_VERIFY_HASH_SIGNATURE;
	  break;
    case IPSD_DSA_VHS_REC_TYPE:
  	  usSdrSz = IPSD_SZ_DSA_VERIFY_HASH_SIGNATURE;
	  break;
   default:
	  // this is not a signed file
	  fnCheckFileSysCorruption(fclose(pCertFileHandle));
	  return VFS_FILE_NOT_SIGNED;
	  break;
  }

  //Verify signature
  skipsize = sdrh.numSdrs * usSdrSz;
  skipsize += sizeof(SGNHDR);

  /* Verify the hash signature on the first SDR only */
  result = fnWriteData(BOBAMAT0, VERIFY_SDR_WITH_PSD, &hrec );

	if(result == BOB_OK)
	{ /* SDR Check Results */
	  /* Now compute the hash for the image */
	  /* Now we advance past all of the SDR's in the file */
	  fileStatus = fseek(pCertFileHandle, skipsize, SEEK_SET);
		if (fileStatus == POSIX_SUCCESS)
		{
		  hashsize = filesize - skipsize;

		  //Compute the hash directly from the file
		  if (ComputeSHA1HashFromFile(pCertFileHandle, &destHash, hashsize, &initChainVariable ))
		  {
			  //Compare the two hashes but first endian swap the big endian hash from the file
			  EndianSwapInPlace( ( void *) hrec.chunkData.hashData.value, sizeof(hrec.chunkData.hashData.value));
			  if( memcmp( ( void *) &destHash , ( void *) hrec.chunkData.hashData.value, sizeof(destHash) ) == 0 )
			  {
				  if (dataOffset != NULL)
					*dataOffset = skipsize;
				  if (dataSize != NULL)
					*dataSize = hashsize;
                  rc = VFS_SUCCESS;
			  }
		  }
		}
		else
			fnCheckFileSysCorruption(fileStatus);
	}

    fnCheckFileSysCorruption(fclose(pCertFileHandle));
    return(rc);
}

/* **********************************************************************
// FUNCTION NAME:fnVerifyUpdateDownload( char * )
// PURPOSE: Verify a download image. First
//          verify the Signed Data Record at the beginning then
//          verify the hash of the image.
//          Normally if the SDR check for Hash Check fails we delete the download
//          We are leaving now to be able to check why
// AUTHOR: George Monroe
//
// INPUTS: filename
// RETURNS: SUCESSFUL if OK else FAILURE
// **********************************************************************/
//TODO - this needs to be tested if downloads are done in base
BOOL fnVerifyUpdateDownload( char *fileName )
{	
	long dataOffset = 0;
	VERIFY_FILESIG_STATUS rc = fnVerifyFileSignature(fileName, &dataOffset, NULL);
	return((rc == VFS_SUCCESS)? TRUE : FALSE);
}
