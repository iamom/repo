/* $TOG: uncompr.c /main/1 1998/02/04 14:34:26 kaleb $ */
/* uncompr.c -- decompress a memory buffer
 * Copyright (C) 1995-1998 Jean-loup Gailly.
 * For conditions of distribution and use, see copyright notice in zlib.h 
 */

#include "zlib.h"
#include "zutil.h"
#include "commontypes.h"
#include "kernel/plus_common.h"
#include "pcdisk.h"
#include "pbos.h"


extern NU_MEMORY_POOL    *pSystemMemoryCached;
VOID fnFilePath(CHAR* outPath, CHAR* path, CHAR* fileName);

/* ===========================================================================
     Decompresses the source buffer into the destination buffer.  sourceLen is
   the byte length of the source buffer. Upon entry, destLen is the total
   size of the destination buffer, which must be large enough to hold the
   entire uncompressed data. (The size of the uncompressed data must have
   been saved previously by the compressor and transmitted to the decompressor
   by some mechanism outside the scope of this compression library.)
   Upon exit, destLen is the actual size of the compressed buffer.
     This function can be used to decompress a whole file at once if the
   input file is mmap'ed.

     uncompress returns Z_OK if success, Z_MEM_ERROR if there was not
   enough memory, Z_BUF_ERROR if there was not enough room in the output
   buffer, or Z_DATA_ERROR if the input data was corrupted.
*/
int EXPORT uncompress (dest, destLen, source, sourceLen)
    Bytef *dest;
    uLongf *destLen;
    const Bytef *source;
    uLong sourceLen;
{
    z_stream stream;
    int err;

    stream.next_in = (Bytef*)source;
    stream.avail_in = (uInt)sourceLen;
    /* Check for source > 64K on 16-bit machine: */
    if ((uLong)stream.avail_in != sourceLen) return Z_BUF_ERROR;

    stream.next_out = dest;
    stream.avail_out = (uInt)*destLen;
    if ((uLong)stream.avail_out != *destLen) return Z_BUF_ERROR;

    stream.zalloc = (alloc_func)0;
    stream.zfree = (free_func)0;

    err = inflateInit(&stream);
    if (err != Z_OK) return err;

    err = inflate(&stream, Z_FINISH);
    if (err != Z_STREAM_END) {
        inflateEnd(&stream);
        return err == Z_OK ? Z_BUF_ERROR : err;
    }
    *destLen = stream.total_out;

    err = inflateEnd(&stream);
    return err;
}


//int EXPORT uncompressWithHeaderEx (dest, destLen, srcFolder, sourceFile)
//    Bytef *dest;
//    uLongf *destLen;
//    const Bytef *srcFolder;
//    const Bytef *sourceFile;
//
//{
//
//    int status = SUCCESS;
//    Uzp_File_Hdr *precHdr;
////    Uzp_cdir_Rec cdRec;
////    Uzp_end_cdir_Rec ecdRec;
//    char path[64];
//    int fileLen = 0;
//    uch* compBuffer = NULL;
//    uch* compBuffer2 = NULL;
//    ulg signature;
//	INT fd;
//	int bytes_read;
//	unsigned short adjustedLength;
//
//	uch len[2];
//
//	memset(&len, 0, sizeof(len));
//    fnFilePath(path, srcFolder, sourceFile);
//
//	fd =  NU_Open(path, (PO_TEXT|PO_RDONLY), PS_IREAD);
//	if(fd < 0)
//	{
//		dbgTrace(DBG_LVL_ERROR, "uncompressWithHeaderEx: Failed to open file %s \r\n", path);
//		status = fd;
//	}
//	else
//	{
//		fileLen = NU_Seek( fd , 0 , PSEEK_END );
//		if(fileLen > 0)
//		{
//			status = NU_Seek( fd, 0L, PSEEK_SET );
//			if(status == SUCCESS)
//			{
//				status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &compBuffer, fileLen, NU_NO_SUSPEND);
//				status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &compBuffer2, fileLen, NU_NO_SUSPEND);
//				if( (compBuffer != NULL) && (compBuffer2 != NULL) )
//				{
//
//					bytes_read = NU_Read(fd, (char *)compBuffer, fileLen);
//					if(bytes_read != fileLen)
//					{
//						dbgTrace(DBG_LVL_ERROR, "uncompressWithHeaderEx - Failed to read file info read:%d, expected:%d \n", bytes_read, fileLen);
//						status = -200;
//					}
//					else
//					{
//						precHdr = (Uzp_File_Hdr*)compBuffer;
//
//						signature = 0x04034b50; //0x504B0304;
//
//						if(memcmp(&precHdr->signature, &signature, sizeof(signature)) == 0)
//						{
//							if( precHdr->ucsize <= *destLen && (precHdr->csize < (fileLen - sizeof(Uzp_File_Hdr)) ) )
//							{
//								memcpy(compBuffer2 + 2, compBuffer + sizeof(Uzp_File_Hdr) + precHdr->filename_length, precHdr->csize );
//
//								//Assing first two bytes length value
//								adjustedLength = precHdr->ucsize;
//								memcpy(compBuffer2, &adjustedLength, sizeof(adjustedLength));
//
//								status = uncompress (dest, destLen, compBuffer2, precHdr->csize);
//								if(status != SUCCESS)
//								{
//									dbgTrace(DBG_LVL_INFO, "uncompressWithHeaderEx - Failed to uncompress file status:%d ", status);
//									status = -300;
//									*destLen = 0;
//
//								}
//								else
//								{
//
//									dbgTrace(DBG_LVL_INFO, "uncompressWithHeaderEx - uncopmression success: :%d ", *destLen);
//
//								}
//							}
//							else
//							{
//								dbgTrace(DBG_LVL_ERROR, "uncompressWithHeaderEx - Invalid Destination Length :%d / ucsize:%d,  \n", *destLen, precHdr->ucsize);
//								status = -400;
//							}
//						}
//						else
//						{
//							dbgTrace(DBG_LVL_ERROR, "uncompressWithHeaderEx - Invalid Compressed file:%lx,  \n", precHdr->signature, &signature);
//							status = -500;
//						}
//
//					}
//
//				}
//				else
//				{
//					dbgTrace(DBG_LVL_ERROR, "uncompressWithHeaderEx: Failed to Allocate buffer size:%d \r\n", fileLen);
//					status = -600;
//				}
//
//			}
//		}
//		else
//		{
//			dbgTrace(DBG_LVL_ERROR, "uncompressWithHeaderEx: Invalid file size %d \r\n", fileLen);
//			status = -700;
//		}
//
//		NU_Close(fd);
//	}
//
//	if(compBuffer!= NULL)
//		NU_Deallocate_Memory(compBuffer);
//
//
//
//    return status;
//}
