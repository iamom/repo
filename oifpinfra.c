/************************************************************************
	PROJECT:		Future Phoenix
	COPYRIGHT:		2005, Pitney Bowes, Inc.
	AUTHOR:			
	MODULE NAME:	OifpInfra.c

	DESCRIPTION:	This file contains all types of screen interface functions 
*					(hard key, event, pre, post, field init and field refresh)
*					and global variables pertaining to infrastructure.

 	MODIFICATION HISTORY: 
     
 22-May-17 Jennifer Hao on FPHX 02.12 cienet branch
      Commented out  functions fnOITGetCurrentService,  fnOITGetCurrentServicePriority, fnOITGetCurrentOnlineServType,
      fnhkMDMGiveupPort, fnhkInfraBufClearOrChangeScr because they aren't used by G9.

 07-Jun-16 sa002pe on FPHX 02.12 shelton branch
 	Removed the use of the Distributor shortcuts for the Gandalf & Test servers.

 23-Sep-14  Renhao  on FPHX 02.12 cienet branch
 Modified function fnhkDistrValidateParam() fix fraca 227302 & 102652.
 	
 18-Sep-14  Wang Biao  on FPHX 02.12 cienet branch
   Modified function fnfInfraContactPBCaption() fix fraca 227025.
   
 03-July-14 Rong Junyu on FPHX 02.12 cienet branch
   Updated function fnpPostContactPB() to fix fraca 225939.
 
 08-Jun-14 Wang Biao on FPHX 02.12 cienet branch
    Updated function fnpPostContactPB() & fnfInfraContactPBCaption() for fraca 225401.
    
 22-May-14 Renhao on FPHX 02.12 cienet branch
    Modified function fnhkSavEntryBufToTmpArray() to fix fraca 225381.

 10-Apr-14 sa002pe on FPHX 02.12 shelton branch
	Added fnFormatDcapDate, fnFormatDcapTime, fnFormatDcapAMPM, fnfDatCapReqTime & fnfDatCapReqAMPM.
	Changed fnfDcapNextUploadPlanned & fnfDCAPUploadReqDate to use fnFormatDcapDate.
	Changed fnfDatCapNextCallTime to use fnFormatDcapTime.
	Changed fnfDatCapNextCallAMPM to use fnFormatDcapAMPM.

 08-Apr-14 sa002pe on FPHX 02.12 shelton branch
	Merged in the following changes for U.K. EIB from Janus:
	1. Added fnfDatCapNextCallTime & fnfDatCapNextCallAMPM.
	2. Changed fnhkDCAPDoUploadLater to not cancel the Auto-Upload event.

 10-Mar-14 sa002pe on FPHX 02.12 shelton branch
	Merging in changes from Janus tips:
----*
	* 27-Nov-13 sa002pe on Janus tips shelton branch
	*	Added pSecapKeyword and changed fnhkDistrValidateParam so we can have a shortcut for setting the Distributor URL
	*	to the one for the Secap QA server.
----*

 04-Jun-13 sa002pe on FPHX 02.10 shelton branch
 	For Fraca 118888: Changed fnpRefillSuccessful & fnhkExitPBPBalance to set fOOBIgnoreContactPB
	to TRUE if in OOB so the step for calling Tier3 & DLA will be considered complete because
	the refill was successful or we didn't want to do a refill after a successful balance check.

 18-Mar-13 sa002pe on FPHX 02.10 shelton branch
 	Changed fnevDLADownloadInfo & fnProcessDownloadXferStatusMsg to simplify the calculation of the remaining time.

 24-Feb-13 sa002pe on FPHx 02.10 shelton branch
 	Now that I've added the Balance Inquiry Receipt screen, it's in a different place
	on the "next screens" list than it was for the 01.10 sw, so had to update the
	screen number for it in fnProcessServiceCompleteMsg.

 22-Feb-13 sa002pe on FPHx 02.10 shelton branch
 	Changed fnProcessServiceCompleteMsg to go to the Balance Inquiry Receipt screen
	as long as the screen is in the list of screens.

 19-Feb-13 sa002pe on FPHx 02.10 shelton branch
 	Merging in changes from the 01.10 branch for Brazil that didn't get merged back to the main branch
----
    2009.05.27  Clarisa Bellamy
	    - FPHX 1.10 For support of Balance Inquiry reports in Brazil: 
          Add include of cmos.h.
          Added 6 new functions: fnOiSetBalanceInquiry, fnOiIsItABalanceInquiry, fnLogBalanceInquiryTemp,
              fnLogBalanceInquiryCommit, fnSetIntelliServiceNotCompleteFlag, fnGetIntelliServiceNotCompleteFlag
          Modify function fnInfraAllServicesComplete to clear the Balance Inquiry flag and 
		      the Temp data in the Balance Inquiry log.
          Modify fnDisconnectDistributor, fnProcessAllServicesCompleteMsg, and fnProcessServiceCompleteMsg
		      to clear the Balance Inquiry flag.
          Modify fnhkServiceComplete to call fnSetIntelliServiceNotCompleteFlag before pritnting the report.
          Modify fnProcessStartServiceRequest to store the Balance Inquiry info in the Balance Inquiry log.
          Update a few comments.
    2009.05.14  Clarisa Bellamy 
	    - In function fnProcessServiceCompleteMsg, change the test for the Balance Inquiry receipt from
		      testing for the country to testing for the existence of the report.
    05/12/2009  Raymond Shen
	    Modified fnProcessServiceCompleteMsg to support Balance Inquiry receipt for Brazil/France. 
----

 18-Feb-13 sa002pe on FPHX 02.10 shelton branch
 	Added backupAcntUrl & backupDLAUrl to oiDistrParamInfo[]. (Enh Req 220392)
 	
 	11/07/2012 Bob Li       FPHX 02.10 cienet branch
 	Modified fnhkDistrValidateParam() to fix fraca 102652.
 	
    09/29/2012Bob Li        FPHX 02.10 cienet branch
	Implement Enh Req 213340. 
	Don't prompt user to upload account data. 
	Make sure abacus transactions data can be uploaded to server in time.
	Make sure displaying the correct ongoing service status.

    09/06/2012  Bob and Liu Jupeng FPHX 02.09 branch
                               Modified the functiion fnpPrepContactPB() for hiding the prefix screen on oob 
                               when using the LAN connection.
 	
	06/30/2011	Joe Zhang	Adapted from Janus 16.80 to fix double refills issue
					Changed the function fnevInfraDialogueErrGeneric,
					and add new OI functions fnhkCallLostButRefillSuccess() and fnpInfraGeneralErrButRefillSuccess() to the file end.
*
* 21-Dec-10 ming.huang on cienet Fp02.07 tips branch
* changes for not passing the null pointers to the pre/post-functions any more.  One function fnhkServiceComplete() was changed.
* Totally three  files changed: oigts.c, oireport.c,oifpinfra.c
*
 	07/12/2010  Raymond Shen    Modified fnhkRefillPostage() to add support the refill code feature.
 	06/29/2010  Jingwei,Li     Added function fnProcessRefillServiceStatusMsg() and modified functions
 	                           fnevProcessDistrMsg(),fnhkRefillPostage() to support reissue case.
 	06/09/2010  Jingwei,LI     Modified fnhkDistrParamBufferKey() and fnhkSavEntryBufToTmpArray() for RTL.
 	06/02/2010  Jingwei,Li     Modified fnhkDistrParamBufferKey() and fnhkSavEntryBufToTmpArray()for RTL.
 	05/18/2010  Jingwei,Li     Modified fnfInfraDownloadProgressBar() to make progress bar increasing from
 	                           right to left for RTL writting.
 	04/27/2010  Jingwei,Li     Modified fnhkDistrValidateParam() to support RTL language writing.
 	04/23/2010  Jingwei,Li     Modified fnIsDistrParamOK() to support RTL language writing.
 	04/21/2010  Jingwei,LI     Modified fnfInfraDownloadProgressBar() and fnhkDistrParamBufferKey() for RTL.
 	09/09/2009  Raymond Shen    Modified fnfInfraContactPBCaption() to fix GMSE00170347: G910/DM300c-Meter should 
 	                            not display "Conf Svc Records Being Uploaded" screen and print receipt after OOB
 	                            without CPS records.
 	08/17/2009  Jingwei,Li      Modified fnProcessServiceCompleteMsg() for Canda Pkg Service.
 	06/17/2009  Raymond Shen    Modified fnProcessAllServicesCompleteMsg() and added fnOITSetDataUploadReqFromStatus(),
 	                            fnOITGetDataUploadReqFromStatus(), fnpPostAbacusDataUploadAlert (), 
 	                            fnhkPerformAbacusDataUpload(), fnhkQuitAbacusDataUploadAlert() for
 	                            Budget Manager Web Visibility support.
 	03/31/2009  Jingwei,Li      Modified function fnfInfraPbpError() for no error/warning case.
    10/03/2008  Deborah Kohl    Fix fraca GMSE00150065 : In fnGetDCErrorScr() add a call 
                                to fnBobErrorInfo() to clear the error and to avoid the
                                system fault error 200D after the refill error screen
 	09/23/2008  Joe Qu          Modified function fnfInfraContactPBCaption() to support
                                downloading EService Barcodes for G9G2 Netherlands.
 	08/29/2008  Joe Qu          Modified function fnpInfraGeneralErr() to direct
 	                            PBP general screen to the newly added Refill Error screens.
 	                            Merged function fnGetDCErrorScr()s from Janus to check the DC 
 	                            errors and return the error screen.
 	08/07/2008  Joey Cui        Merged stuffs from Janus to support PC proxy feature
 	08/01/2008  Raymond Shen    Added code to support Web Visibility feature.
 	03/03/2008  Raymond Shen    Added fnhSRefillPostage() and modified fnhkRefillPostage() 
 	                            to add super password protection on Refill.
 	12/13/2007    Andy Mo       Modified fnpInfProcessingDone()to fix134412&134549
 	12/03/2007  Vincent Yi      Code clean up 
 	11/29/2007    Andy Mo       Modified fnhkPerformConfirmationUpload to fix fraca133739&133684
 	10/24/2007    Oscar Wang    Modified function fnhkPerformConfirmationUpload to fix FRACA GMSE00131603:
 	                            Replace fnhkModeSetDefault() with fnRatingSetDefault().
 	09/27/2007    Andy Mo       Added functions to support connection mode setting in data
 	                            center option menu
 	08/15/2007    Andy Mo       Modified in fnhkPerformConfirmationUpload().
    08/10/2007    Andy Mo       Modified fnhkPerformConfirmationUpload() to support Germany Manifest PH report
    08/07/2007    Joey Cui      Added code to handle fOITInfraManifestLimitReqShown 
    08/03/2007    Joey Cui      Added fnhkCPerformConfirmationUpload to retrieve barcode
                                Modified fnProcessAllServicesCompleteMsg and 
                                fnProcessServiceCompleteMsg to reset displaying flags
     3 Aug 2007 Martin O'Brien  Fixed FRACA 126765 where extra character was 
                                displayed on Meter Move screen
    07/23/2007    Vincent Yi    Fixed lint errors
    14 Jun 2007 Simon Fox       Added pre and post functions for modem init
                                edit string screen to control use of accents.
    12 Jun 2007 Simon Fox       Added pre and post functions for modem init
                                string screen to control use of accents.
  30 May 2007 I. Le Goff    Move the reset of fLowFundsWarningPending flag to FALSE to
                            fnPBPSHandlePSDMsg() in pbptask.c. That way, the flag is reset
                            after each refill, even if the refill wasn't requested by the user.
 	05/30/2007  Vincent Yi      Modified functions fnevInfraNoConnect and fnevInfraNoModem 
	29 Mar 2007 Bill Herring    Completed functions fnhkCPerformMeterMove(),
                                fnhkCPerformMeterMoveOutsideRegion(),
                                fnfCurrentLocationCode() and
                                fnpPostMeterMoveComplete() for meter move.
                                Modified fnfInfraContactPBCaption() and
                                fnProcessServiceCompleteMsg() to support meter move.
	02/08/2007	Vincent Yi		Merge from Janus 11.22:
							*	Changed fnevDCAPWakeUpAndUpload to clear the meter 
								lock flag when we setup to do an unattended upload 
								so the auto-upload will take precedence over the 
								meter lock.
							*	Changed fnevDCAPDoUnattendedUpload to make sure the 
								period is archived before doing the auto-upload.
	02/06/2007	Vincent Yi		Modified fnpInfraGeneralErr() and fnProcessAllServicesCompleteMsg()
								to modified the OOB pbp flow
 	01/08/2007  Adam Liu       Changed fnProcessDownloadXferStatusMsg() to correct
 	                           the calc of download remain time.
 	01/05/2006  Adam Liu       Changed fnfInfraDownloadPercentage() to fix fraca
 	                           GMSE00112552.
 	12/14/2006  Adam Liu       Merged dcap related functions from JA1114/K700 and 
 	                           code cleanup.
 	                           fnpErrDataCaptureEngine()
 	                           fnfDcapNextUploadPlanned()
 	                           fnfDcapNextUploadDate()
 	                           fnfDCAPUploadReqDate()
 	                           fnhkCPerformDcapUpload()
 	                           fnhkDCAPDoUploadLater()
 	                           fnevDCAPWakeUpAndUpload()
 	                           fnpInfraDCAPStartTimeoutClock()
 	                           fnpInfProcessingDone()
 	                           fnevDCAPDoUnattendedUpload()
 	                           Modified functions:
 	                           fnfInfraContactPBCaption()
 	                           fnProcessServiceCompleteMsg()
 	                           
	08/22/2006  Kan Jiang      Do the followings for fixing fraca 102670
	                           Change function name fnfDialingPrefixInit to fnfOOBDialingPrefixInit
	                           Add a new function whose name is fnfDialingPrefixInit
	08/10/2006  Dicky Sun      Add the following functions for Clear key and 
	                           Home key in GTS screens:
							       fnhkServiceComplete
 	08/03/2006  Raymond Shen    Added fnfBlindDialingSetting(), fnhkToggleBlindDialingSetting()
 	                            and modified fnpPrepPhoneSettings() for Blind Dialing feature.
	07/28/2006  Dicky Sun      For uploading reocrd workflow, modify and add the
	                           following functions:
	                               fnpPrepContactPB
								   fnpRefillSuccessful
	                               fnhkExitPBPBalance
								   fnProcessServiceCompleteMsg
								   fnhkRefillPostage
	07/28/2006	Vincent Yi		Modified function fnevDownloadFinished() to made 
								timeout event also effect to return Home when
								UIC doesn't need to restart
	07/25/2006   Dicky Sun     Update function fnProcessServiceCompleteMsg for
	                           uploading record workflow.
	07/19/2006   Dicky Sun     Update the following functions for GTS uploading 
	                           record error.
							       fnfInfraGenErrorCaption
							       fnhkPerformConfirmationUpload
 	07/14/2006   Raymond Shen  Modified function fnhkValidateModemInitStr() and
 	                           fnhkRestoreModemInitStr() to correct initialition of modem string.
	07/18/2006   Dicky Sun     Modify fnProcessServiceCompleteMsg for GTS 
	                           uploading record.
	07/14/2006   Raymond Shen  Modified function fnfStdDistrParamEntryInit(), 
	                           fnfInfraScrollEntryRefresh(), and fnhkRestoreDistrib() 
	                           to fix some bugs of Distributer Parameter setting.
	07/14/2006   Dicky Sun     Update function fnhkPerformConfirmationUpload to 
	                           go to the correct next screen.
	07/13/2006	Vincent Yi		Added functions to add timer to online-wait screens
	07/12/2006   Dicky Sun      Update function fnhkPerformConfirmationUpload to 
	                            make sure all pending records are saved to flash.
	07/10/2006   Kan Jiang      Modify to fix some Lint Errors

 	07/07/2006   Dicky Sun      Add function fnhkPerformConfirmationUpload
 	                            for GTS record upload.

    07/05/2006   Kan Jiang   Modify functions fnhkDistrValidateParam(), fnhkValidateModemInitStr()
                             fnhkRestoreModemInitStr() and fnhkRestoreDistrib() to calling 
                             fnSetNetworkDefaults() for fixing FRACA 100003
 	
 	06/22/2006	Raymond Shen	Changed fnDisconnectDistributor() from static to
 								public function; Added fnIsDistributorConnected();
 								Modified fnProcessAllServicesCompleteMsg().

	OLD PVCS REVISION HISTORY
 * \main\jnus11.00_i1_shelton\3 cx17598 18 oct 2005
 *
 * 	Moved field functions fnfDcapRRSyncStatus1 & fnfDcapRRSyncStatus2 to oierrhnd.c.
 *
*************************************************************************/

/**********************************************************************
		Include Header Files
**********************************************************************/

#include "commontypes.h"

#include "bob.h"
#include "bobutils.h"
#include "cmos.h"
#include "ctype.h"
#include "custdat.h"
#include "cwrapper.h"
#include "datdict.h"
#include "dcapi.h"
#include "download.h"
#include "features.h"
#include "fwrapper.h"
#include "intellilink.h"
#include "oicmenucontrol.h"
#include "oicommon.h"
#include "oientry.h"
#include "oierrhnd.h"
#include "oifields.h"
#include "oifpmain.h"
#include "oifpinfra.h"
#include "oifpprint.h"
#include "oifunctfp.h"
//#include "oioob.h"
//#include "oirefill.h"
//#include "oireport.h"
#include "oiscreen.h"
#include "oit.h"
#include "nucleus.h"
#include "pbptask.h"
#include "pbplatform.h"
//#include "ratesvcdcai.h"
#include "rftypes.h"
#include "sysdatadefines.h"
#include "oiscrnfp.h"
//#include "deptaccount.h"

#include "networking/externs.h"
//TODO - cleanly remove PPP dependent code
//#include "nu_ppp.h"
//#include "ppp\inc\pppurt.h"
//#include "ppp\inc\mdm_defs.h"
//#include "ppp\inc\lcp_defs.h"
//#include "ppp\inc\chp_defs.h"
//#include "ppp\inc\pap_defs.h"
//#include "ppp\inc\ncp_defs.h"
//#include "ppp\inc\ppp_defs.h"

//#include "Abacustypes.h"
//#include "Abupload.h"
#include "oipostag.h"
//#include "abacusXBAPI.h"
//#include "AbacusXCM.h"
//#include "abacusgdm.h"
#include "pbtarget.h"
#include "flashutil.h"

/**********************************************************************
		Local Function Prototypes
**********************************************************************/

static BOOL fnSMenuDistrParamShown ( UINT16 *pFieldDest, 
                                     UINT8   bLen, 
                                     UINT8   bFieldCtrl, 
                                     UINT8   bNumTableItems, 
                                     UINT8  *pTableTextIDs, 
                                     UINT8  *pAttributes,
                                     UINT16  usItemIndex);
static UINT8 fnIsDistrParamOK(void);

static SINT8 fnc1ContactPB( void 		(** pContinuationFcn)(), 
	  			  		    UINT32 	  	*	pMsgsAwaited,
			  			    T_SCREEN_ID *	pNextScr );
static SINT8 fnc1PostContactPB( void 		(** pContinuationFcn)(), 
		  			  		    UINT32 	  	*	pMsgsAwaited,
				  			    T_SCREEN_ID 	*	pNextScr );

static T_SCREEN_ID fnProcessUploadXferStatusMsg (UINT8 			bNumNext, 
											    T_SCREEN_ID  *	pNextScreens, 
											    INTERTASK 	*	pIntertask);
static T_SCREEN_ID fnProcessDownloadXferStatusMsg (UINT8 			bNumNext, 
												   T_SCREEN_ID  *	pNextScreens, 
												   INTERTASK 	*	pIntertask);
static T_SCREEN_ID fnProcessStartServiceRequest (UINT8 			bNumNext, 
												 T_SCREEN_ID  *	pNextScreens, 
												 INTERTASK 	*	pIntertask);
static T_SCREEN_ID fnProcessAllServicesCompleteMsg (UINT8 			bNumNext, 
												    T_SCREEN_ID  *	pNextScreens, 
												    INTERTASK 	*	pIntertask);
static T_SCREEN_ID fnProcessServiceCompleteMsg (UINT8 			bNumNext, 
											    T_SCREEN_ID  *	pNextScreens, 
												INTERTASK 	*	pIntertask);
static T_SCREEN_ID fnProcessRefillServiceStatusMsg (UINT8           bNumNext, 
												T_SCREEN_ID  *  pNextScreens, 
												INTERTASK   *   pIntertask);

BOOL fnGetUpdateNow (UINT8 bServiceCode, INT32 value, INT32 value2);

static T_SCREEN_ID fnhSRefillPostage(UINT8          bKeyCode, 
                                 UINT8          bNumNext, 
                                 T_SCREEN_ID *  pNextScreens);

BOOL fnIsDataCenterUploadAvail(void);

static BOOL fnfDataUploadLogNumCommon( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes,
                                UINT16  usSlotIndex);
static BOOL fnfDataUploadLogDate( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes,
                                UINT16  usSlotIndex);
static BOOL fnfDataUploadLogTime( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes,
                                UINT16  usSlotIndex);
static BOOL fnfDataUploadLogAMPM( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes,
                                UINT16  usSlotIndex);
static T_SCREEN_ID fnDataUploadLogSelect( UINT16    usIndex,
                                            UINT8   bKeyCode, 
                                            UINT8   bNumNext, 
                                            T_SCREEN_ID *pNextScreens);


/**********************************************************************
		Local Defines, Typedefs, and Variables
**********************************************************************/
/* definitions for distributor parameters */
#define DISTR_PARAM_ACCOUNT_ID 0
#define DISTR_PARAM_ACCOUNT_PASSWORD 1
#define DISTR_PARAM_ANILCZ_SERVER_IP 2
#define DISTR_PARAM_ANILCZ_SERVER_PORT 3
#define DISTR_PARAM_PRIMARY_DNS 4
#define DISTR_PARAM_SECONDARY_DNS 5
#define DISTR_PARAM_DISTR_URL 6
#define DISTR_PARAM_PBP_BACKUP_URL 7

// The sequence number of error messages should be consistent to the index in 
// field text list.
#define ERR_INFRA_NONE							0xff
#define ERR_INFRA_NO_DIAL_TONE					0
#define ERR_INFRA_LINE_BUSY						1
#define ERR_INFRA_CONNECTION_LOST				2
#define ERR_INFRA_NEGOCIATION					3
#define ERR_INFRA_LOGIN							4
#define ERR_INFRA_NO_ANSWER						5
#define ERR_INFRA_NO_CARRIER					6
#define ERR_INFRA_NO_CONNECT					7
#define ERR_INFRA_NO_MODEM						8


#define DISTR_PARAM_STATUS_ERR      0
#define DISTR_PARAM_STATUS_OK    1
#define DISTR_PARAM_STATUS_KEEP_GOING 2

// The index of dialing error in the field text list
static UINT8	ucInfraDialingErrorInfoNo = ERR_INFRA_NONE;	

/* flags for distr param stuff */
#define DISTR_PARAM_INFO_IP         0x0001
#define DISTR_PARAM_INFO_PORT       0x0002
#define DISTR_PARAM_INFO_TEXT       0x0004
#define DISTR_PARAM_INFO_PASSWORD   0x0008

// kan added for modem setup
#define  DIALING_PULSE   0
#define  DIALING_TONE     1

//length of array which is used to save the string and used by pDistParamTmp---Kan
#define DISTR_LEN_TMPARRAY  65

#define DLA_CONCT_SPEED					1640


#define DLA_TIME_TO_CONNECT_DLA_SERVER  	(UINT32)20
#define DLA_TIME_TO_SEND_CONFIRM            (UINT32)60 

#define    CONNECTION_MODE_AUTO_DETECT 0
#define    CONNECTION_MODE_PHONE_LINE 1
#define    COMING_FROM_LOGFULL 2
static BOOL		fOITMeterMoveOutsideRegion = FALSE;

static BOOL 	fOITDistrConnected = FALSE; 	// flag for distributor connectted
												// fOITDistrConnectStatus

static INFRA_CONTACT_PB_STATE eInfraContactPBState = INFRA_CONTACT_PB_DIALING;

static UINT8 	ucOITRequestedService = INFRA_REQ_SERVICE_NONE;	// bOITRequestedService

static BOOL		fOITDistrDnsLookupErr = FALSE;

static UINT8	ucOITCurrentDistrMsgID;	// bOITCurrentDistrMsgID
static UINT8	ucOITCurrentService;			// bOITCurrentService
static UINT8	ucOITCurrentServicePriority;	// bOITCurrentServicePriority

static UINT32 	ulOITCurrentServiceResult;

//static BOOL		fDialingServiceComplete = FALSE;	// bDialingServiceComplete
//static BOOL		fOITPerformedOnlineUpdate = FALSE;


//static BOOL	fOITScheduledUpdateOnline;
static BOOL		fOITCurrentServiceTimeSet;
static DATETIME	rOITCurrentServiceDateTime;

//Array that is used to stored the string which is displayed in Distributor 
//Parameter Setup screens and Modem String Setup screen.  -- Kan
static char  	pDistParamTmp[DISTR_LEN_TMPARRAY];

//Reused from Mega1502. It is used to record the which kind of 
//distributor parameters has been selected -- Kan
static UINT8  	bCurrDistrParam = 100;

static UINT32 	ulOITInfraTotalBytesToDownload = 0;	// lwOITInfraTotalBytesToDownload
static UINT32 	ulOITInfraDownloadedBytes = 0;		// lwOITInfraUpdateTotalBytes
static UINT8  	bOITInfraCurrentFileToDownload = 0;

static UINT32 	ulDownloadEstimateRemainTime = 0;	// lwDownloadEstimateRemainTime

static SINT32	lOITInfraScrnTimeoutTicks = 0;	// iOITInfraScrnTimeoutTicks
static BOOL		fOITInfraScrnTimeoutExpired = FALSE;
static BOOL		fOITInfraScrnTimeoutEnabled = FALSE;

static UINT8   ucOnlineServiceType = OI_ONLINE_SERV_AUTO;

/*Data Upload Log Details Index*/
static UINT16 usDataUploadCurLogIndex = 0;

/*Budget Manager Webvisibility Support*/
UINT8   bOITDataUploadStatus = 0;
SINT16  sOITTotalDataUploadCounts = 0;
SINT16  sOITSentDataUploadCounts = 0;
BOOL fStrDCapUploaded = OIT_INFRA_UPLOAD_NOT;
BOOL fStrEServiceUploaded =OIT_INFRA_UPLOAD_NOT;
BOOL fStrAcctUploaded = OIT_INFRA_UPLOAD_NOT;
UINT32 lDataUploadErrCode = 0;

UINT8   bDataUploadInProgress = FALSE;/* Only set when Webvis feature is enabled 
                                             and 1 of the 3 service started. */

UINT16  usDataUploadDateSepChar = (UINT16)'.';

/* Data Upload happend but where are we from?? */
UINT8   bDataUploadReqFrom = OI_DATA_UPLD_FROM_NONE;                                     
UINT16 usUploadFlagClear = 0;
extern UINT16   usOITCurrentDisplayService;
extern BOOL fUploadModeSpecial;
extern BOOL fDcapModeSpecial;
extern unsigned long lwPsdState;


//-----------------
// Set if the user may still select another service after the report is printed.
//  When this is set, it will keep fnevReportPrinted from sending the 
//  OIT_SERVICE_COMPLETE_REPLY message.  Sending that message causes the 
//  distributor task to disconnect the call. 
//  
static BOOL  fIntelliServiceNotCompleteFlag = FALSE;

//-----------------
// When set, the current session was selected as a Balance Inquiry by the user.
static BOOL fUserChoseActBal = FALSE;


/**********************************************************************
		Global Functions Prototypes
**********************************************************************/

extern void fnValidateExtFiles(void);
extern void RebootSystem(void);
extern void ChangeModemDialPrefix( void );
//TODO JAH Remove dcapi.c extern error_t fnDCAPTimerExpired(void);
extern void fnClearDSRState(void);


/**********************************************************************
		Global Variables
**********************************************************************/

/* if this matches the beginning of the string typed for the distributor URL,
 * we initialize the set of net config parameters to the QA setup */
const unsigned short pQAKeyword[] = { 'Q', 'a' };

/* if this matches the beginning of the string typed for the distributor URL,
 * we initialize the set of net config parameters to the EMD defaults */
const unsigned short pEMDKeyword[] = { 'E', 'm', 'd'};

/* if this matches the beginning of the string typed for the distributor URL,
 * we initialize the set of net config parameters to the Backup defaults */
const unsigned short pBackupKeyword[] = { 'B', 'a', 'c', 'k', 'u', 'p'};

/* if this matches the beginning of the string typed for the distributor URL,
 * we initialize the set of net config parameters to the Secap QA setup */
const unsigned short pSecapKeyword[] = { 'S', 'e', 'c', 'a', 'p' };

int 	iOITDistrLoopCount = 0;
BOOL 	fOITModemSignalLost = FALSE; // 
//BOOL fOITAbortModemConnection = FALSE;
BOOL 	fOITConfSrvUploadDone = FALSE;
UINT8	bOITInfraTotalFilesToDownload = 0;
//BOOL fOITInspectionRequested = FALSE;
BOOL 	fOITDownloadDoneReboot = FALSE;
BOOL 	fOITDCAPDoUnattendedUpload = FALSE;
BOOL 	fOITMeterMoved = FALSE;
UINT32 	lwModemConnectionSpeed = 0;

extern int verbosity;
extern BOOL    bCMOSConnectionMode  ; 

extern BOOL fMeterLocked;


extern BOOL		fScreenTickEventQueued;

extern BOOL 	fCMOSInfraUpdateReq;
extern BOOL  	fLowBarcodeWarning;

extern ENTRY_BUFFER  rGenPurposeEbuf;
extern NETCONFIG    CMOSNetworkConfig;
extern UINT8	bOITModemDialMode;
extern ENTRY_BUFFER_CTRL    rEntryBuf;

extern UINT8 bDelconPackagesDuplicate;	

extern CMOSDownloadCtrlBuf CmosDownloadCtrlBuf;

//extern unsigned char bOITCurrentService;
extern unsigned short 	 usDcapWriteStatus;
// upload due warning shown flag
extern BOOL	fDCAPUploadDueShown;

extern BOOL	fRRUpdatePending;
extern BOOL	fRRUpdateDone;
extern CHAR bFromScreenOptions;
//BOOL fOITDCAPDoUnattendedUpload = FALSE;
//static int		iOITInfraScrnTimeoutTicks = OI_INFRA_SCREEN_TIMOUT_VALUE;
//static BOOL		fOITInfraScrnTimeoutExpired = FALSE, fOITInfraScrnTimeoutEnabled = FALSE;


const struct _oiDistrParamInfo oiDistrParamInfo[] = {
    {CMOSNetworkConfig.attLoginAccountAndUserId,63, DISTR_PARAM_INFO_TEXT},
    {CMOSNetworkConfig.attPassword,             31, (DISTR_PARAM_INFO_PASSWORD | DISTR_PARAM_INFO_TEXT)},
    {CMOSNetworkConfig.attAniServerIp,          15, DISTR_PARAM_INFO_IP},
    {CMOSNetworkConfig.attAniServerPortNumber,   5, DISTR_PARAM_INFO_PORT},
    {CMOSNetworkConfig.primaryDnsServer,        15, DISTR_PARAM_INFO_IP},
    {CMOSNetworkConfig.secondaryDnsServer,      15, DISTR_PARAM_INFO_IP},
    {CMOSNetworkConfig.distributorUrl,          64, DISTR_PARAM_INFO_TEXT},
    {CMOSNetworkConfig.backupPbpUrl,            64, DISTR_PARAM_INFO_TEXT},
    {CMOSNetworkConfig.backupAcntUrl,           64, DISTR_PARAM_INFO_TEXT},
    {CMOSNetworkConfig.backupDlaUrl,            64, DISTR_PARAM_INFO_TEXT}
    };

extern FILES_TO_DOWNLOAD CMOSFilesToDownload;

/* Web vis CMOS Data Upload log */
extern tBMWVDataUploadLog CMOSWebVisDataUploadLog[BMWV_MAX_DATA_UPLOAD_LOG];
    
extern BOOL	fLowFundsWarningPending;
extern BOOL	fLowFundsWarningDisplayed;
extern DNS_HOST_LIST  DNS_Hosts;

/**********************************************************************
		Public Functions
**********************************************************************/
/* Utility functions */

#if 0   // obsolete
/* *************************************************************************
// FUNCTION NAME: 
//      fnInfraIsDialingServiceComplete
//
// DESCRIPTION:
//      Wrapper function to return the infra service complete or not.
//
// INPUTS:
//      None
//
// RETURNS:
//      TRUE or FALSE
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
// 	1/24/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnInfraIsDialingServiceComplete(void)
{
	return fDialingServiceComplete;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnInfraSetDialingServiceComplete
//
// DESCRIPTION:
//      Wrapper function to set the infra service complete flag.
//
// INPUTS:
//      None
//
// RETURNS:
//      TRUE or FALSE
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
// 	07/28/2006	Dicky Sun		Initial version
//
// *************************************************************************/
void fnInfraSetDialingServiceComplete(BOOL fServiceComplete)
{
	fDialingServiceComplete = fServiceComplete;
}
#endif 

BOOL fnOITIsMeterMoveOutOfRegion(void)
{
	return fOITMeterMoveOutsideRegion;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnOITGetRequestedService
//
// DESCRIPTION: 
//      Utility function that gets requested service.
//
// INPUTS:      
//      None
//
// RETURNS:
//      current requested service.
//
// NOTES: 
//      None
//
// MODIFICATION HISTORY:
//	12/29/2005	Vincent Yi 		Initial version
//      
// *************************************************************************/
UINT8 fnOITGetRequestedService (void)
{
	return ucOITRequestedService;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnOITSetRequestedService
//
// DESCRIPTION: 
//      Utility function that sets requested service.
//
// INPUTS:      
//      ucService
//
// RETURNS:
//      None
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//	12/29/2005	Vincent Yi 		Initial version
//      
// *************************************************************************/
void fnOITSetRequestedService (UINT8 ucService)
{
	 ucOITRequestedService = ucService;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnInfraSetScreenTimeoutInterval
//
// DESCRIPTION: 
//      Utility function that sets value of lOITInfraScrnTimeoutTicks.
//
// INPUTS:      
//      lInterval - 
//
// RETURNS:
//      None
//
// NOTES: 
//      None
//
// MODIFICATION HISTORY:
//	07/12/2006	Vincent Yi 		Initial version
//      
// *************************************************************************/
void fnInfraSetScreenTimeoutInterval (SINT32 lInterval)
{
	lOITInfraScrnTimeoutTicks = lInterval;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnInfraModemActivated
//
// DESCRIPTION: 
// 		OIT message handling function that received message MDM_CONNECTED from 
//		DIST and activates Modum to Uart Port
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 				Victor Li		Initial version
//	12/29/2005	Vincent Yi		Code cleanup
//
// *************************************************************************/
void fnInfraModemActivated (INTERTASK *pIntertask)
{
/*	fnOITSetUartPort (UART_PORT_MODEM);
	fnGetPrePostWaitCtrlBlock()->lwMsgsReceived |= MDM_ACTIVATE_AWAITED_MASK;
*/
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnInfraModemDisconnected
//
// DESCRIPTION: 
// 		OIT message handling function that received message MDM_DISCONNECT from 
//		DIST then disconneted Modem from Uart Port
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 				Victor Li		Initial version
//	12/29/2005	Vincent Yi		Code cleanup
//
// *************************************************************************/
void fnInfraModemDisconnected(INTERTASK *pIntertask)
{
	/* set the modem giveup port falg to false here */
/*	if (fnOITGetUartPort() == UART_PORT_MODEM)
	{
		fnOITSetUartPort (UART_PORT_NONE);
	}
	fnGetPrePostWaitCtrlBlock()->lwMsgsReceived |= MDM_GIVEUP_PORT_AWAITED_MASK;
*/
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnInfraConnectionResult
//
// DESCRIPTION: 
// 		OIT message handling function that handles the results of the modem/PPP 
//		connection.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 				Derek DeGennaro	Initial version
//				Victor Li		Ported for Janus
//	12/29/2005	Vincent Yi		Code cleanup
//
// *************************************************************************/
void fnInfraConnectionResult(INTERTASK *pIntertask)
{
//TODO - cleanly remove PPP dependent code
#if 0 // remove modem code, some of which does not compile
	SINT32 	lNucStatus = NU_SUCCESS;
	UINT8 	ucServiceMsg[2];

	if (pIntertask->bMsgType == LONG_DATA)
	{
		lNucStatus = (SINT32)pIntertask->IntertaskUnion.lwLongData[0];
		if (lNucStatus == NU_SUCCESS)
		{
			fOITDistrConnected = TRUE;

//			Following two lines are obsolete for FP
			fOITCurrentServiceTimeSet = FALSE;
//			fOITScheduledUpdateOnline = FALSE;
						
			ucServiceMsg[0] = ucOITRequestedService;
			ucServiceMsg[1] = USER_REQUESTED_SERVICE;

//Vincent	if (fOITAbortModemConnection == FALSE)
			{	// No abort operation, send service request to dist task
				(void)OSSendIntertask( DISTTASK, OIT, OIT_CLIENT_SERVICE_REQ, 
										BYTE_DATA, ucServiceMsg, 2 );
				(void)fnProcessEvent(EVENT_DC_CONNECTED, pIntertask);
			}
/*Vincent
			else
			{	// Abort operation during connecting
				fnDisconnectDistributor ();

				(void)fnProcessEvent(EVENT_DC_DIALING_ERR, pIntertask);
			}
*/
		}	// if (lNucStatus == NU_SUCCESS)
		else
		{	// Error return from DIST task during dialing or connecting

			if (fOITDistrConnected == TRUE)
			{
				// Send disconnect message to distributor if it's successfully 
				// connected so modem can hang up the phone properly
				fnDisconnectDistributor ();
			}

			// split to different events for some of cases as required by Janus 
			// to display different error messages
			switch (lNucStatus) 
			{
			case NU_LCP_FAILED:
			case NU_NCP_FAILED:
				(void)fnProcessEvent(EVENT_DC_LINK_NEGOTATION_ERR, pIntertask);
				break;
			case NU_LOGIN_FAILED:
				(void)fnProcessEvent(EVENT_DC_LOGIN_ERR, pIntertask);
				break;
			case NU_NO_CONNECT:			
				(void)fnProcessEvent(EVENT_DC_NO_CONNECT, pIntertask);
				break;
			case NU_NO_CARRIER:
				(void)fnProcessEvent(EVENT_DC_NO_CARRIER, pIntertask);
				break;
			case NU_NO_MODEM:
				(void)fnProcessEvent(EVENT_DC_NO_MODEM, pIntertask);
				break;
			case NU_NO_ANSWER:
				(void)fnProcessEvent(EVENT_DC_NO_ANSWER, pIntertask);
				break;
			case NU_BUSY:
				(void)fnProcessEvent(EVENT_DC_BUSY, pIntertask);
				break;		
			case NU_NO_DIALTONE:
				(void)fnProcessEvent(EVENT_DC_NO_DIAL_TONE, pIntertask);
				break;
			case NU_INVALID_LINK:
			case NU_NETWORK_BUSY:
			default:
				(void)fnProcessEvent(EVENT_DC_DIALING_ERR, pIntertask);
			}	// switch (...)
		}
	}
	else
	{
		(void)fnProcessEvent(EVENT_DC_DIALING_ERR, pIntertask);
	}
#endif
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnInfraDNSLookupError
//
// DESCRIPTION: 
// 		OIT message handling function that handles the DNS error
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/3/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
void fnInfraDNSLookupError(INTERTASK *pIntertask)
{
	fOITDistrDnsLookupErr = TRUE;	
	fnGetPrePostWaitCtrlBlock()->lwMsgsAwaited = 0;
	fnGetPrePostWaitCtrlBlock()->lwMsgsReceived = 0;
//	(void)fnProcessEvent(EVENT_DC_DNS_TIMEOUT, pIntertask);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnInfraStartService
//
// DESCRIPTION: 
// 		OIT message handling function after receiving message OIT_START_SERVICE_REQ 
//		from DIST task
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/3/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
void fnInfraStartService(INTERTASK *pIntertask)
{
	ucOITCurrentDistrMsgID = pIntertask->bMsgId;
	ucOITCurrentService = pIntertask->IntertaskUnion.bByteData[0];
	ucOITCurrentServicePriority = pIntertask->IntertaskUnion.bByteData[1];
//	fOITScheduledUpdateOnline = FALSE;
	(void)fnProcessEvent(EVENT_DISTR_TASK_MSG, pIntertask);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnInfraEndService
//
// DESCRIPTION: 
// 		OIT message handling function after receiving message OIT_SERVICE_COMPLETE 
//		from DIST task
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/3/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
void fnInfraEndService(INTERTASK *pIntertask)
{
	ucOITCurrentDistrMsgID = pIntertask->bMsgId;
	ulOITCurrentServiceResult = pIntertask->IntertaskUnion.lwLongData[0];
	fOITCurrentServiceTimeSet = FALSE;

	if (pIntertask->IntertaskUnion.lwLongData[0] != 0)
	{
//		(void)fnProcessEvent(EVENT_DC_DIALOGUE_ERR, pIntertask);
	}
	else
	{
		(void)fnProcessEvent(EVENT_DISTR_TASK_MSG, pIntertask);
	}

//	fOITScheduledUpdateOnline = FALSE;
}	

/* *************************************************************************
// FUNCTION NAME: 
//		fnInfraAllServicesComplete
//
// DESCRIPTION: 
// 		OIT message handling function after receiving message 
//		OIT_ALL_SERVICES_COMPLETE from DIST task
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	1/3/2006	Vincent Yi		Code cleanup
//				Victor Li		Initial version
//  2009.05.27  Clarisa Bellamy - Add stuff for Brazil for new Balance 
//                      Inquiry reports.
// *************************************************************************/
void fnInfraAllServicesComplete(INTERTASK *pIntertask)
{

	ucOITCurrentDistrMsgID = pIntertask->bMsgId;
	fnProcessEvent(EVENT_DISTR_TASK_MSG, pIntertask);
    
    // Clear the temp Balance Inquiry info
    fnCMOSClearBalanceInqLogTemp();

    fnResetInspectionCheckFlags();
    fnGetPsdState(&lwPsdState);
    /* reset the low funds warning flags */
	fLowFundsWarningPending = FALSE;
	fLowFundsWarningDisplayed = FALSE;

    // Clear the flag indicating a Balance Inquiry is in progress.
    fnOiSetBalanceInquiry( FALSE );
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnDisconnectDistributor
//
// DESCRIPTION: 
//  	Funtion to disconnect distributor
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None
//
// RETURNS:
//		None
//
// WARNINGS/NOTES:
// 		None
//
// MODIFICATION HISTORY:
//	06/20/2006	Raymond Shen	changed from static to public function
//	04/10/2006	Vincent Yi		Initial version
//  2009.05.27  Clarisa Bellamy - Add stuff for Brazil for new Balance 
//                      Inquiry reports.
// *************************************************************************/
void fnDisconnectDistributor (void)
{
	fOITDistrConnected = FALSE;
	lwModemConnectionSpeed = 0;

    // Any Balance Inquiry is done, whether it completed or not.
    fnOiSetBalanceInquiry( FALSE );

	(void)OSSendIntertask( DISTTASK, OIT, OIT_ATT_DISCONNECT, 
							NO_DATA, NULL, 0 );
	OSWakeAfter(300);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnGetConnectionMode
//
// DESCRIPTION: 
//  	Funtion to get the connection mode
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None
//
// RETURNS:
//		None
//
// WARNINGS/NOTES:
// 		None
//
// MODIFICATION HISTORY:
//	09/27/2007	Andy Mo		Initial version
//
// *************************************************************************/
BOOL fnGetConnectionMode(void)
{
    return (bCMOSConnectionMode);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnConnectionModeAuto
//
// DESCRIPTION: 
//  	Utinity function to decide if allocate a menu slot to connection 
//      mode depending on if current connection mode is AUTO detect
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None
//
// RETURNS:
//		None
//
// WARNINGS/NOTES:
// 		None
//
// MODIFICATION HISTORY:
//	09/27/2007	Andy Mo		Initial version
//
// *************************************************************************/
BOOL fnConnectionModeAuto(void)
{
    BOOL bRev=TRUE;
	
	
    if(fnGetConnectionMode()== CONNECTION_MODE_PHONE_LINE)
        bRev = FALSE;
		
    return (bRev);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnConnectionModePhone
//
// DESCRIPTION: 
//  	Utinity function to decide if allocate a menu slot to connection 
//      mode depending on if current connection mode is phone line
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None
//
// RETURNS:
//		None
//
// WARNINGS/NOTES:
// 		None
//
// MODIFICATION HISTORY:
//	09/27/2007	Andy Mo		Initial version
//
// *************************************************************************/
BOOL fnConnectionModePhone(void)
{
    BOOL bRev=TRUE;
	
	
    if(fnGetConnectionMode()==CONNECTION_MODE_AUTO_DETECT)
        bRev = FALSE;
		
    return (bRev);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnIsDistributorConnected
//
// DESCRIPTION: 
//  	Funtion to get distributor's connection status
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
// 		None
//
// MODIFICATION HISTORY:
//	06/20/2006	Raymond Shen	Initial version
//
// *************************************************************************/
BOOL fnIsDistributorConnected(void)
{
	return ( fOITDistrConnected );
}

/**************************************************************************
// FUNCTION NAME: 
//          fnOITSetDataUploadReqFromStatus
//
// DESCRIPTION:
//         Get the value of the variable bDataUploadReqFrom
//
// INPUTS:
//          None
//
// RETURNS:
//     value of the variable fOITAbortModemConnection
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//           Zhang Yingbo    Initial version
// *************************************************************************/
void fnOITSetDataUploadReqFromStatus(unsigned char bStatus)
{
    bDataUploadReqFrom = bStatus;
}

/**************************************************************************
// FUNCTION NAME: 
//          fnOITGetDataUploadReqFromStatus
//
// DESCRIPTION:
//         Get the value of the variable bDataUploadReqFrom
//
// INPUTS:
//          None
//
// RETURNS:
//     value of the variable fOITAbortModemConnection
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//           Zhang Yingbo    Initial version
// *************************************************************************/
UINT8  fnOITGetDataUploadReqFromStatus(void)
{
/*
    if(IsAbAccountingUploadEnabled())
	    return(bDataUploadReqFrom);
    else
*/
	    return(0);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnOITGetDataUploadResult
//
// DESCRIPTION: 
//  	Function to get data upload result.
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None
//
// RETURNS:
//		value of the variable
//
// WARNINGS/NOTES:
// 		None
//
// MODIFICATION HISTORY:
//           Zhang Yingbo    Initial version
//
// *************************************************************************/
INT8 fnOITGetDataUploadResult(void)
{
    BOOL retval = OI_DUPLOAD_SUCCESSFUL;
    
	
    if( (fStrAcctUploaded == OIT_INFRA_UPLOAD_NOT)
             && (fStrDCapUploaded == OIT_INFRA_UPLOAD_NOT)
             && (fStrEServiceUploaded == OIT_INFRA_UPLOAD_NOT) )
    {
        retval = OI_DUPLOAD_NONE;
    }

    else if( (fStrAcctUploaded == OIT_INFRA_UPLOAD_FAILED)
            || (fStrAcctUploaded == OIT_INFRA_UPLOAD_FAILED_MODE_1)
            || (fStrAcctUploaded == OIT_INFRA_UPLOAD_FAILED_MODE_2)
            || (fStrDCapUploaded == OIT_INFRA_UPLOAD_FAILED)
            || (fStrEServiceUploaded == OIT_INFRA_UPLOAD_FAILED) )
    {
        retval = OI_DUPLOAD_FAILED;
    }

    else
    {
        retval = OI_DUPLOAD_SUCCESSFUL;
    }

    return(retval);
}


/**************************************************************************
// FUNCTION NAME: 
//          fnOITSetCurrentOnlineServType
//
// DESCRIPTION:
//         Set the value of the variable fOITAbortModemConnection
//
// INPUTS:
//          None
//
// RETURNS:
//     value of the variable fOITAbortModemConnection
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//           Zhang Yingbo    Initial version
//*************************************************************************/
void fnOITSetCurrentOnlineServType(UINT8 ucType)
{
    ucOnlineServiceType = ucType;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnInfraCheckTimeSyncStatus
//
// DESCRIPTION: 
// 		OIT message handling function after receiving message 
//		CHECK_TIME_SYNC from DIST task
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	03/28/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
/*Vincent
static BOOL		fTimeSyncCompleted = FALSE;
void fnInfraCheckTimeSyncStatus(INTERTASK *pIntertask)
{
	UINT32 	ulTimeSyncStatus;

	ulTimeSyncStatus = fnInfraGetTimeSyncStatus();

	if (!(ulTimeSyncStatus & TIME_SYNC_NEGATIVE_DRIFT_LOCKOUT))
	{
		// set a pending flag here since the event may not be handled during
		// the screen changing.
		fTimeSyncCompleted = TRUE;
		// triggle the event.
		fnProcessEvent(EVENT_TIME_ADJUST_COMPLETED, pIntertask);
	}
}
*/

//-----------------------------------------------------------------------------
// FUNCTION NAME:       fnOiSetBalanceInquiry
// DESCRIPTION:
//      Sets the flag that indicates that the user has selected Balance 
//      Inquiry for this infrastructure connection.
//
// INPUTS:
//      BOOL fSetClear - TRUE if user has selected Balance Inquiry.
//                      FALSE when the BalanceInquiry connection is done.
// RETURNS:
//      None
// NOTES:
//  1. This is required for Brazil, because they want to ignore DCAP period
//      data, unless the user explicitely selected a Balance Inquiry.
//
// MODIFICATION HISTORY:
//   2009.05.15 Clarisa Bellamy  Initial version
//-----------------------------------------------------------------------------
void fnOiSetBalanceInquiry( BOOL fSetClear )
{
    fUserChoseActBal = fSetClear;   
}

// *************************************************************************
// FUNCTION NAME:       fnOiIsItABalanceInquiry
// DESCRIPTION:
//      Returns the flag that indicates whether the user has selected Balance 
//      Inquiry for this infrastructure connection or not.
//
// INPUTS:
//      None
// RETURNS:
//      BOOL fSetClear - TRUE if user has selected Balance Inquiry.
//                      FALSE when the BalanceInquiry connection is done.
// NOTES:
//  1. This is required for Brazil, because they want to ignore DCAP period
//      data, unless the user explicitely selected a Balance Inquiry.
//
// MODIFICATION HISTORY:
//   2009.05.15 Clarisa Bellamy  Initial version
//-----------------------------------------------------------------------------
BOOL fnOiIsItABalanceInquiry( void )
{
    return( fUserChoseActBal );   
}

// *************************************************************************
// FUNCTION NAME:       fnLogBalanceInquiryTemp
// DESCRIPTION
//      Saves the Balance InquiryLog data to a temporary spot. 
//
// INPUTS:      None
// RETURNS: None
//
// NOTES:
// MODIFICATION HISTORY: 
//   2009.05.18 Clarisa Bellamy  Initial version
//-----------------------------------------------------------------------------
static void fnLogBalanceInquiryTemp( void )
{
    UINT8   wCountryCode;

    // If we are in Brazil, and we are doing a Balance Inquiry, but not a Refill.
    wCountryCode = fnGetDefaultRatingCountryCode();
    if( wCountryCode == BRAZIL )
    {
        if( fnOiIsItABalanceInquiry() == TRUE )
        {
            // This saves the data to a temp location.  It will be copied in bulk
            //  to the log array later.  No data is passed, because the only data 
            // stored is the current date/time, which the subroutine retrieves.
            fnCMOSLogBalanceInqToTemp();
        }
    }
}

// *************************************************************************
// FUNCTION NAME:       fnLogBalanceInqCommit
// DESCRIPTION:
//      Saves the data in the Temp Balance Inquiry Entry to the actual log, 
//      and increments the index.
//
// INPUTS:      None
// RETURNS:     None
//
// NOTES:
// MODIFICATION HISTORY:
//   2009.05.18 Clarisa Bellamy  Initial version
//-----------------------------------------------------------------------------
static void fnLogBalanceInquiryCommit( void )
{
    fnCMOSLogBalanceInqFromTemp();
}


// *************************************************************************
// FUNCTION NAME:           fnSetIntelliServiceNotCompleteFlag
// DESCRIPTION: 
//      Utility function that sets the fIntelliServiceNotCompleteFlag.
// INPUTS:
//      BOOL - Value to set the flag to.
// RETURNS:
//      None
// WARNINGS/NOTES:
//      This flag indicates to the bob task that this online report should NOT
//      end with a SERVICE_COMPLETE message to the distributor.
// MODIFICATION HISTORY:
//  2009.05.22  Clarisa Bellamy - Initial revision.
//-----------------------------------------------------------------------------
void fnSetIntelliServiceNotCompleteFlag( BOOL fPrompt )
{
    fIntelliServiceNotCompleteFlag = fPrompt;
}

// *************************************************************************
// FUNCTION NAME:               fnGetIntelliServiceNotCompleteFlag
// DESCRIPTION: 
//      Utility function that returns the fIntelliServiceNotCompleteFlag.
// INPUTS:
//      None
// RETURNS:
//      TRUE    - Set       There are more services.
//      FALSE   - Not Set
// WARNINGS/NOTES:
//      This flag indicates to the bob task that this online report should NOT
//      end with a SERVICE_COMPLETE message to the distributor.
// MODIFICATION HISTORY:
//  2009.05.22  Clarisa Bellamy - Initial revision.
//-----------------------------------------------------------------------------
BOOL fnGetIntelliServiceNotCompleteFlag( void )
{
    return( fIntelliServiceNotCompleteFlag );
}


/* Pre/post functions */


/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepPhoneSettings
//
// DESCRIPTION: 
//      Pre function that set up Phone setup screen. 
//
// INPUTS:
//      Standard pre function inputs.
//
// RETURNS:
//      PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//  		1. Dialing Type
//			2. Dialing Prefix
//			3. PBP Phone
//          4. Blind Dialing
//			5. Modem String
// MODIFICATION HISTORY:
//      08/02/2006  Raymond Shen    Added Blind Dialing option.
//      01/12/2006   Kan jiang   Initial version
// *************************************************************************/
SINT8 fnpPrepPhoneSettings ( void (** pContinuationFcn)(), 
                             UINT32 *pMsgsAwaited,
							 UINT16 *pNextScr )
{
	UINT32	 ulSlotIndex = 0;
	
    static S_MENU_SETUP_ENTRY   pSetupMenuInitTable[] = 
    {
        // force menu table reset before defining new slots
        {   CLEAR_MENU_TABLE,   (UINT8) 0,  (UINT8) 0,  FALSE,  NULL_PTR },									
        /* Dial Type */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 1,    (UINT8) 1,   TRUE,    NULL_PTR },
        /* Dialing Prefix */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 2,    (UINT8) 2,   TRUE,    NULL_PTR},
        /* PBP Phone */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 3,   TRUE,    NULL_PTR },
        /* Blind Dialing */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 4,    (UINT8) 4,   TRUE,    NULL_PTR },
        /* Modem String */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 5,    (UINT8) 5,   TRUE,    NULL_PTR },
    
        {   END_MENU_TABLE_MARKER,  (UINT8) 0,    (UINT8) 0,   FALSE,   NULL_PTR }  
    } ;

        
    // invoke generic slot initialization function with this menu initializer table
    ulSlotIndex = fnSetupMenuSlots( pSetupMenuInitTable, ulSlotIndex, (UINT8) 4 ) ;
	
	// Set the total number of entries which will be use for displaying 
	// scroll bar
	fnSetMenuEntriesTotalNumber (ulSlotIndex);
      
	// Update LED indicator
	fnUpdateLEDPageIndicator();
    *pNextScr = 0;  /* leave next screen unchanged */

	return ( PREPOST_COMPLETE );
}




/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostPhoneSettings 
//
// DESCRIPTION: 
//      Post function set LED off.
//
// INPUTS:  
//      Standard post function inputs.
//
// RETURNS:
//      PREPOST_COMPLETE
//
// NOTES:
//
// MODIFICATION HISTORY:
//      01/12/2006   Kan jiang    Initial version 
//
// *************************************************************************/
SINT8 fnpPostPhoneSettings( void (** pContinuationFcn)(), 
                            UINT32 *pMsgsAwaited)
{
    SET_MENU_LED_OFF();
    return (PREPOST_COMPLETE);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepDistributionParameters
//
// DESCRIPTION: 
//      Pre function that set up distributor setup screen. 
//
// INPUTS:
//      Standard pre function inputs.
//
// RETURNS:
//      PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      01/12/2006   Kan jiang   Initial version
//
// *************************************************************************/
SINT8 fnpPrepDistributionParameters ( void (** pContinuationFcn)(), 
                                      UINT32 *pMsgsAwaited,
							          UINT16 *pNextScr )
{
	UINT32 ulSlotIndex = 0;

     static S_MENU_SETUP_ENTRY   pSetupMenuInitTable[] = 
    {
        // force menu table reset before defining new slots
        {   CLEAR_MENU_TABLE,   (UINT8) 0,  (UINT8) 0,  FALSE,  NULL_PTR },									
        // Accout ID
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 1,    (UINT8) 1,   TRUE,    NULL_PTR },
        // Password
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 2,    (UINT8) 2,   TRUE,    NULL_PTR},
        // Server IP
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 3,   TRUE,    NULL_PTR },
        // Server Port
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 4,    (UINT8) 4,   TRUE,    NULL_PTR },
        // Primary DSN Server
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 5,    (UINT8) 5,   TRUE,    NULL_PTR },
        // Second DNS Server
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 6,    (UINT8) 6,   TRUE,    NULL_PTR},
        // Distributor URL
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 7,    (UINT8) 7,   TRUE,    NULL_PTR },
    
        {   END_MENU_TABLE_MARKER,  (UINT8) 0,    (UINT8) 0,   FALSE,   NULL_PTR }  
    } ;

    // invoke generic slot initialization function with this menu initializer table
    ulSlotIndex = fnSetupMenuSlots( pSetupMenuInitTable, ulSlotIndex, (UINT8) 4 );
    fnSetMenuEntriesTotalNumber(ulSlotIndex);
      
    // Initialize array for saving string which is displayed in Edit Setting screen.
    memset(pDistParamTmp, 0, sizeof(char) * DISTR_LEN_TMPARRAY);

	*pNextScr = 0;	 /* leave next screen unchanged */
	
    // Update LED indicator
    fnUpdateLEDPageIndicator();
       
	return ( PREPOST_COMPLETE );
}




/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostDistributionParameters 
//
// DESCRIPTION: 
//      Post function set LED off.
//
// INPUTS:  
//      Standard post function inputs.
//
// RETURNS:
//      PREPOST_COMPLETE
//
// NOTES:
//
// MODIFICATION HISTORY:
//      01/12/2006   Kan jiang    Initial version 
//
// *************************************************************************/
SINT8 fnpPostDistributionParameters ( void (** pContinuationFcn)(), 
                                      UINT32 *pMsgsAwaited)
{
    SET_MENU_LED_OFF();
    return (PREPOST_COMPLETE);
}


/* *************************************************************************
// FUNCTION NAME: 
//		fnpPrepContactPB
//
// DESCRIPTION: 
// 		Pre function that is called prior to going to the Home Ready screen.
//
// INPUTS:
//		Standard screen pre function inputs.
//
// RETURNS:
//		PREPOST_COMPLETE, if UART has connected to Modem
//		PREPOST_WAITING,  else, to connect Modem
//
// WARNINGS/NOTES:
//		No screen change
//
// MODIFICATION HISTORY
//	09/29/2012	Bob Li			Updated this function to make sure the Abacus transactions
//								can be closed before connecting PBP service.
//  09/06/2012  Bob and Liu Jupeng   Fixed the issue for hiding the prefix 
//                                   screen on oob when using the LAN connection.
//  07/28/2006  Dicky Sun       Update for uploading record workflow.
//	1/03/2006	Vincent Yi		Code cleanup
//				Derek DeGennaro	Initial version
//
// *************************************************************************/
SINT8 fnpPrepContactPB( void 		(** pContinuationFcn)(), 
				  		UINT32 	  	*	pMsgsAwaited,
				  		T_SCREEN_ID *	pNextScr )
{
	SINT8 cRetVal = PREPOST_COMPLETE;
//	UINT8 ucUartPort = fnOITGetUartPort();
    SINT16  sAbacusStatus;
    UINT16  usDetailAbacusStatus;

	*pNextScr = 0;

	if(fOITDistrConnected == TRUE &&
       ucOITCurrentService != CONFIRMATION_SERVICE &&
	   fnShowPrintUploadReceiptPrompt() == TRUE)
	{
	    //Check pending record.
	    *pNextScr = GetScreen(ESERVICE_CHECK_PENDING_RECORD); 
 	}

    //make sure the abacus transactions are closed before connecting PBP server.
	//Implement Enh Req 213340.
    if(IsAbAccountingUploadEnabled())
    {
        sAbacusStatus = fnAGDGetAbacusStatus(&usDetailAbacusStatus);
/*
        if (sAbacusStatus == ABACUS_IN_USE)
        {
            if (fnAXB_AreThereOpenTransactions() == TRUE)
            {
                (void)fnAXC_JournalXactions();
            }
        }
*/
    }

/*Vincent
	if( fnIsInOOBMode() )
	{
		fnpOOBCheckScreenAndBit(pContinuationFcn, pMsgsAwaited, pNextScr);
		if(*pNextScr != 0) 
			goto xit;
	}
*/

/*	if (ucUartPort != UART_PORT_MODEM)
	{	// if it's disconnected, send messages to distributor task to 
		// request a connection.
 
		// send message to platform to relinquish port.
		if (ucUartPort == UART_PORT_PLATFORM)
		{
			OSSendIntertask(PLAT, OIT, PLAT_RELINQUISH_PORT, NO_DATA, NULL, 0);
			OSWakeAfter(200);
		}

		// send message to distributor to active port.
		*pContinuationFcn = (void (*)()) fnc1ContactPB;
		*pMsgsAwaited = 0 | MDM_ACTIVATE_AWAITED_MASK;
		OSSendIntertask(DISTTASK, OIT, MDM_ACTIVATE_PORT, NO_DATA, NULL, 0);
		fnSetMessageTimeout(DISTTASK, MDM_CONNECTED, STANDARD_INTERTASK_TIMEOUT);

		// wait for the modem message response
		cRetVal = PREPOST_WAITING;
	}
	else 
*/
	{	// Modem has been connected to Uart Port
		if (fOITDistrConnected == FALSE)
		{
	        // check for possible DLA missing file request
	        (void)fnValidateExtFiles();

			(void)OSSendIntertask (DISTTASK, OIT, CONNECT_TO_ATT, NO_DATA, 
									NULL, 0);

			iOITDistrLoopCount = 0;
//			fOITAbortModemConnection = FALSE;
			eInfraContactPBState = INFRA_CONTACT_PB_DIALING;
			fOITMeterMoved = FALSE;
	        fOITModemSignalLost = FALSE;
			ucInfraDialingErrorInfoNo = ERR_INFRA_NONE;
			if( TRUE == isProxyReachable() || TRUE == connectedToInternetViaLan(NULL) )
            {
                //change the state to connecting before successfully Connect To Att
                // for avoiding displaying dialing when the LAN is connected but not ready.
                eInfraContactPBState = INFRA_CONTACT_PB_CONNECTING;
            }
		}
	}
	// Otherwise, already connected to DC.

	fOITConfSrvUploadDone = FALSE;
	
	return (cRetVal);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpPostContactPB
//
// DESCRIPTION: 
//		Post function that give up the uart port for modem.
//
// INPUTS:
//		Standard screen post function inputs.
//
// OUTPUTS:
//		None
//
// RETURNS:
//		PREPOST_WAITING, if user want Modem give UART port
//		PREPOST_COMPLETE, else
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/23/2006	Vincent Yi		Code cleanup
//  6/8/2014    Wang Biao       Updated code for fraca 225401.
//  7/3/2014    Rong Junyu      Updated to fix fraca 225939.
// *************************************************************************/
SINT8 fnpPostContactPB (void	   		(** pContinuationFcn)(), 
           	    	    UINT32 		* 	pMsgsAwaited)
{
	char chRetVal = PREPOST_COMPLETE;
	
	if(usUploadFlagClear == DCAP_UPLOAD_SERVICE || usUploadFlagClear == DATA_UPLOAD_SERVICE)
    {
         fUploadModeSpecial = FALSE;
         fDcapModeSpecial = FALSE;
         /* Changed the 4th arguments to FALSE to fix fraca 225939*/
         (void) fnCMOSRateCallback( TRUE, NULL, TRUE, FALSE);
         usUploadFlagClear = 0;
    }
	return (chRetVal);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpInfraGeneralErr
//
// DESCRIPTION: 
// 		Pre function that sends the message to distributor to disconnect
// 		from att.
//
// INPUTS:
//		Standard screen pre function inputs.
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//		No screen change
//
// MODIFICATION HISTORY
//	02/06/2007	Vincent Yi		Clear OOB pbp timeout flag if in OOB mode
//	1/24/2006	Vincent Yi		Code cleanup
//				Victor Li		Initial version
//
// *************************************************************************/
SINT8 fnpInfraGeneralErr( void 		(** pContinuationFcn)(), 
					  	  UINT32 	  *	pMsgsAwaited,
					  	  T_SCREEN_ID *	pNextScr )
{
	if (fOITDistrConnected == TRUE)
	{
		/* the service complete reply message needs to be sent before sending the disconnect message */
		(void)OSSendIntertask( DISTTASK, OIT, (UINT8)OIT_SERVICE_COMPLETE_REPLY, 
								NO_DATA, NULL, 0 );

		fnDisconnectDistributor ();
	}

/*
	if (fnIsInOOBMode() == TRUE)
	{
		fnSetOOBPBPTimeout (FALSE);
	}
*/
	*pNextScr = fnGetDCErrorScr();

	return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpDownloadComplete
//
// DESCRIPTION: 
// 		Pre Function that starts infra timer when need to auto-restart.
//
// INPUTS:
//		Standard screen pre function inputs.
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//		None.
//
// MODIFICATION HISTORY
//	07/13/2006	Vincent	Yi		Set timeout interval before call fnpInfraStartTimeoutClock
//	04/13/2006	Vincent Yi		Initial version
//
// *************************************************************************/
SINT8 fnpDownloadComplete( void 		(** pContinuationFcn)(), 
						   UINT32 	 	 *	pMsgsAwaited,
						   T_SCREEN_ID 	*	pNextScr )
{
//	if (fOITDownloadDoneReboot == TRUE)
	{
		lOITInfraScrnTimeoutTicks = INFRA_OI_TIMEOUT_INTERVAL_DOWNLOAD_COMPLETE;
		fnpInfraStartTimeoutClock (pContinuationFcn, pMsgsAwaited,
											pNextScr);		
	}
	*pNextScr = 0;

	return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpDownloadUpdateAvailable
//
// DESCRIPTION: 
// 		Pre Function that starts infra timer before entering DownloadUpdateAvailable
//		screen.
//
// INPUTS:
//		Standard screen pre function inputs.
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//		None.
//
// MODIFICATION HISTORY
//	07/12/2006	Vincent Yi		Initial version
//
// *************************************************************************/
SINT8 fnpDownloadUpdateAvailable( void 		(** pContinuationFcn)(), 
								  UINT32 	 	 *	pMsgsAwaited,
								  T_SCREEN_ID 	*	pNextScr )
{
	lOITInfraScrnTimeoutTicks = INFRA_OI_TIMEOUT_INTERVAL_DOWNLOAD_UPDATE_AVAILABLE;
	fnpInfraStartTimeoutClock (pContinuationFcn, pMsgsAwaited, pNextScr);		
	*pNextScr = 0;

	return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpDownloadNoUpdateAvailable
//
// DESCRIPTION: 
// 		Pre Function that starts infra timer before entering DownloadNoUpdateAvailable
//		screen.
//
// INPUTS:
//		Standard screen pre function inputs.
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//		None.
//
// MODIFICATION HISTORY
//	07/12/2006	Vincent Yi		Initial version
//
// *************************************************************************/
SINT8 fnpDownloadNoUpdateAvailable( void 		(** pContinuationFcn)(), 
								    UINT32 	 	 *	pMsgsAwaited,
								    T_SCREEN_ID 	*	pNextScr )
{
	lOITInfraScrnTimeoutTicks = INFRA_OI_TIMEOUT_INTERVAL_DOWNLOAD_NO_UPDATE_AVAILABLE;
	fnpInfraStartTimeoutClock (pContinuationFcn, pMsgsAwaited, pNextScr);		
	*pNextScr = 0;

	return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpRefillSuccessful
//
// DESCRIPTION: 
// 		Pre Function that starts infra timer before entering RefillSuccessful
//		screen.
//
// INPUTS:
//		Standard screen pre function inputs.
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//		None.
//
// MODIFICATION HISTORY
//  07/28/2006  Dicky Sun       Update the receipt delay flag for uploading
//                              record workflow.
//	07/12/2006	Vincent Yi		Initial version
//
// *************************************************************************/
SINT8 fnpRefillSuccessful( void 		(** pContinuationFcn)(), 
						   UINT32 	 	 *	pMsgsAwaited,
						   T_SCREEN_ID 	*	pNextScr )
{
	// if in OOB, we've already done the refill part, set a flag so we know we don't
	// have to do the refill again if the download fails.
	if (fnIsInOOBMode() == TRUE)
	{
		CMOSDiagnostics.fOOBIgnoreContactPB = TRUE;
	}
	
	lOITInfraScrnTimeoutTicks = INFRA_OI_TIMEOUT_INTERVAL_REFILL_SUCCESSFUL;
	fnpInfraStartTimeoutClock (pContinuationFcn, pMsgsAwaited, pNextScr);		
	*pNextScr = 0;

	return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpInfraStartTimeoutClock
//
// DESCRIPTION: 
// 		Pre Function that sets the Infrastructure Screen Timout Clock.
//
// PRE-CONDITIONS:
//		lOITInfraScrnTimeoutTicks has been set.
//
// INPUTS:
//		Standard screen pre function inputs.
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//		No screen change
//
// MODIFICATION HISTORY
//  08/13/2008  Joey Cui        Send refill receipt message if PC Proxy is connected
//	07/12/2006	Vincent Yi		Move the setting timeout interval code to specific
//								pre function, the pre-conditions of this function
//								is this interval should have been set.
//	04/12/2006	Vincent Yi		Code cleanup
//				Derek DeGennaro	Initial version
//
// *************************************************************************/
SINT8 fnpInfraStartTimeoutClock( void 		(** pContinuationFcn)(), 
							  	 UINT32 	  *	pMsgsAwaited,
							  	 T_SCREEN_ID *	pNextScr )
{
	*pNextScr = 0;	
	
	/* Reset the expire flag. */
	fOITInfraScrnTimeoutExpired = FALSE;
	/* Enable the clock. */
	fOITInfraScrnTimeoutEnabled = TRUE;
	
	if(usePcHostForIntelliLink())
	{
		fnXMLSendRefillReceiptMessage();
		
		if(fnIsInOOBMode()== TRUE)
		{
			fnPCSendUnsolicitedStatus(fnGetCurrentScreenID());
		}
	}
	return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpPostInfraStopTimeoutClock
//
// DESCRIPTION: 
//		Post Function that stops the Infrastructure Screen Timout Clock.
//
// INPUTS:
//		Standard screen post function inputs.
//
// OUTPUTS:
//		None
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Derek DeGennaro	Initial version
//	04/12/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
SINT8 fnpPostInfraStopTimeoutClock (void	   (** pContinuationFcn)(), 
			           	    	    UINT32 	* 	pMsgsAwaited)
{
	/* Disable the clock. */   
	fOITInfraScrnTimeoutEnabled = FALSE;	
	
	return (PREPOST_COMPLETE);
}


/* *************************************************************************
// FUNCTION NAME: 
//		fnpInfraDCAPStartTimeoutClock
//
// DESCRIPTION: 
// 		Pre Function that sets the DCAP Screen Timout Clock.
//
// PRE-CONDITIONS:
//		lOITInfraScrnTimeoutTicks has been set.
//
// INPUTS:
//		Standard screen pre function inputs.
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//		No screen change
//
// MODIFICATION HISTORY
//	12/14/2006	Adam Liu		Code Cleanup
//				Derek DeGennaro	Initial version
//
// *************************************************************************/
SINT8 fnpInfraDCAPStartTimeoutClock( void 		(** pContinuationFcn)(), 
							  	     UINT32 	  *	pMsgsAwaited,
							  	     T_SCREEN_ID *	pNextScr )
{
	// turn off the menu LEDs
	SET_MENU_LED_OFF();

	// clear the warning so we don't show it again.
	fDCAPUploadDueShown = TRUE;

	// check if we are going to do an unattended upload
#if 0 //TODO JAH remove dcap
	if ((fnDCAPIsDataCaptureActive() == TRUE)
			&& (fOITDCAPDoUnattendedUpload == TRUE)
			// do not start the timer if the software download completed
			// and the meter is required to reboot.
			&& (fOITDownloadDoneReboot == FALSE))
	{
	    lOITInfraScrnTimeoutTicks = INFRA_OI_TIMEOUT_INTERVAL_DCAP_UPLOAD_COMPLETE;
		return fnpInfraStartTimeoutClock(pContinuationFcn,pMsgsAwaited,pNextScr);
	}
	else
#endif //TODO JAH remove dcap 
	{
		*pNextScr = 0;
		return (PREPOST_COMPLETE);
	}	
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpInfProcessingDone
//
// DESCRIPTION: 
// 		Post function that sends a service-complete message to the
//		Distributor task.
//
// PRE-CONDITIONS:
//		None.
//
// INPUTS:
//		Standard screen post function inputs.
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//		Use this function on the way to the online processing screen.
//
// MODIFICATION HISTORY
//	12/14/2006	Adam Liu		Code Cleanup
//				Derek DeGennaro	Initial version
//
// *************************************************************************/
SINT8 fnpInfProcessingDone         ( void 		(** pContinuationFcn)(), 
							  	     UINT32 	  *	pMsgsAwaited)
{
    if(fnShowPrintUploadReceiptPrompt() == FALSE)
    {
    	(void)OSSendIntertask( DISTTASK, OIT, (unsigned char)OIT_SERVICE_COMPLETE_REPLY, NO_DATA, NULL, 0 );
	}
	fnpPostInfraStopTimeoutClock(pContinuationFcn, pMsgsAwaited);

	return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpErrDataCaptureEngine
//
// DESCRIPTION: 
//		Pre function that prepares the error code returned by the
//		Data Capture Engine/Module.
//
// INPUTS:
//		Standard screen pre function inputs.
//
// OUTPUTS:
//		None
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Sam Thillaikumaran	Initial version
//	12/14/2006	Adam Liu		    Adapted from K700 and Code cleanup
//
// *************************************************************************/
SINT8 fnpErrDataCaptureEngine( void (** pContinuationFcn)(), 
                               UINT32 *pMsgsAwaited,
							   T_SCREEN_ID *pNextScr )
{
	SINT32  DCStatus = 0;

    //Use new rating design.
	//bErrorCode = fnGetDCBucketStatus();
    fnRateGetBucketStatus( &DCStatus );

    fnSetErrCode(ERROR_CLASS_RATING, (UINT8)DCStatus);
    *pNextScr = 0;

	return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpPostMeterMoveComplete
//
// DESCRIPTION: 
//		Post Function that finishes the meter move process
//
// INPUTS:
//		Standard screen post function inputs.
//
// OUTPUTS:
//		None
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	29 Mar 2007 Bill Herring    Initial
//	
//
// *************************************************************************/
SINT8 fnpPostMeterMoveComplete (void	   (** pContinuationFcn)(), 
			           	    	UINT32 	* 	pMsgsAwaited)
{
	fOITMeterMoved = FALSE;
	return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpPhoneModemStringSetting
//
// DESCRIPTION: 
//      Switches off character combinations so that the ^ symbol can be used on
//      its own (usually ^, like other accents can only be combined with
//      suitable characters).
//
// INPUTS:
//		Standard screen pre function inputs.
//
// OUTPUTS:
//		None
//
// RETURNS:
//		
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      14 Jun 2007 Simon Fox - Initial version.
//
// *************************************************************************/
SINT8 fnpPhoneModemStringSetting( void         (** pContinuationFcn)(), 
                                  UINT32       *pMsgsAwaited,
        						  T_SCREEN_ID  *pNextScr )
{
    *pNextScr = 0;
    fnSwitchOffCharacterCombinations();
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpPostPhoneModemStringSetting
//
// DESCRIPTION: 
//      Switches character combinations back on so that the ^ symbol can't be
//      typed on its own on other screens.
//
// INPUTS:
//		Standard screen post function inputs.
//
// OUTPUTS:
//		None
//
// RETURNS:
//		
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      14 Jun 2007 Simon Fox - Initial version.
//
// *************************************************************************/
SINT8 fnpPostPhoneModemStringSetting (void	   (** pContinuationFcn)(), 
    			           	    	  UINT32 	* 	pMsgsAwaited)
{
    fnSwitchOnCharacterCombinations();
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPhoneModemStringEdit
//
// DESCRIPTION: 
//      Switches off character combinations so that the ^ symbol can be used on
//      its own (usually ^, like other accents can only be combined with
//      suitable characters).
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      14 Jun 2007 Simon Fox - Initial version.
//
// *************************************************************************/
SINT8 fnpPhoneModemStringEdit( void         (** pContinuationFcn)(), 
                               UINT32       *pMsgsAwaited,
                               T_SCREEN_ID  *pNextScr )
{
    *pNextScr = 0;
    fnSwitchOffCharacterCombinations();
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostPhoneModemStringEdit
//
// DESCRIPTION: 
//      Switches character combinations back on so that the ^ symbol can't be
//      typed on its own on other screens.
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      14 Jun 2007 Simon Fox - Initial version.
//
// *************************************************************************/
SINT8 fnpPostPhoneModemStringEdit (void     (** pContinuationFcn)(), 
                                   UINT32    *   pMsgsAwaited)
{
    fnSwitchOnCharacterCombinations();
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpInfraAccountUploadStartTimeoutClock
//
// DESCRIPTION: 
//      Pre Function that sets the Infrastructure Screen Timout Clock
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      Zhang Yingbo    Initial version.
//
// *************************************************************************/
SINT8 fnpInfraAccountUploadStartTimeoutClock( void         (** pContinuationFcn)(), 
                               UINT32       *pMsgsAwaited,
                               T_SCREEN_ID  *pNextScr )
{       
    *pNextScr = 0;  
    
    /* Reset the timeout value. */
    lOITInfraScrnTimeoutTicks = INFRA_OI_TIMEOUT_INTERVAL_DATA_UPLOAD;
    
    /* Reset the expire flag. */
    fOITInfraScrnTimeoutExpired = FALSE;
    
    /* Enable the clock. */
    fOITInfraScrnTimeoutEnabled = TRUE;
    
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpInfAccountUploadStopClock
//
// DESCRIPTION: 
//      Post function of InfraPromptDataUploadAvailable screen.
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//  Use this function on the way to the online processing screen.
//
// MODIFICATION HISTORY:
//      Zhang Yingbo    Initial version.
//
// *************************************************************************/
SINT8 fnpInfAccountUploadStopClock (void     (** pContinuationFcn)(), 
                                   UINT32    *   pMsgsAwaited)
{
   
    /* Reset the timeout value. */
    lOITInfraScrnTimeoutTicks = INFRA_OI_TIMEOUT_INTERVAL_DATA_UPLOAD;
    
    /* Reset the expire flag. */
    fOITInfraScrnTimeoutExpired = FALSE;
    
    /* Disable the clock. */
    fOITInfraScrnTimeoutEnabled = FALSE;    
    
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpInfraDataUploadStartTimeoutClock
//
// DESCRIPTION: 
//      Pre Function that sets the Infrastructure Screen Timout Clock
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      Zhang Yingbo    Initial version.
//
// *************************************************************************/
SINT8 fnpInfraDataUploadStartTimeoutClock( void         (** pContinuationFcn)(), 
                               UINT32       *pMsgsAwaited,
                               T_SCREEN_ID  *pNextScr )
{       
    *pNextScr = 0;  
    
    /* Reset the timeout value. */
    lOITInfraScrnTimeoutTicks = INFRA_OI_TIMEOUT_INTERVAL_DATA_UPLOAD;
    
    /* Reset the expire flag. */
    fOITInfraScrnTimeoutExpired = FALSE;
    
    /* Enable the clock. */
    fOITInfraScrnTimeoutEnabled = TRUE;
    
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpInfDataUploadDone
//
// DESCRIPTION: 
//      Post Function of InfraProcessDataUploadComplete screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      Zhang Yingbo    Initial version.
//
// *************************************************************************/
SINT8 fnpInfDataUploadDone (void     (** pContinuationFcn)(), 
                                   UINT32    *   pMsgsAwaited)
{
   
    DATETIME  rDateTime;

    UINT8 i = 0;    

    /* Record this Data Upload to CMOS log before reset the flags  */
    if((fStrDCapUploaded == OIT_INFRA_UPLOAD_SUCCESS)
        || (fStrEServiceUploaded == OIT_INFRA_UPLOAD_SUCCESS)
         || (fStrAcctUploaded == OIT_INFRA_UPLOAD_SUCCESS))//If There are valid data to record to log
    {
      /* Get the Gregorian date/time with offset adjustments */
      if (GetSysDateTime(&rDateTime, ADDOFFSETS, GREGORIAN) == SUCCESSFUL)
      {
        /* push the slot down with 1 step */
        for( i = (BMWV_MAX_DATA_UPLOAD_LOG - 1) ; i > 0; i-- )
        {
            CMOSWebVisDataUploadLog[i].bUploadType = CMOSWebVisDataUploadLog[i-1].bUploadType;

            CMOSWebVisDataUploadLog[i].rDateTime.bCentury = CMOSWebVisDataUploadLog[i-1].rDateTime.bCentury;            
            CMOSWebVisDataUploadLog[i].rDateTime.bYear = CMOSWebVisDataUploadLog[i-1].rDateTime.bYear;
            CMOSWebVisDataUploadLog[i].rDateTime.bMonth = CMOSWebVisDataUploadLog[i-1].rDateTime.bMonth;
            CMOSWebVisDataUploadLog[i].rDateTime.bDay = CMOSWebVisDataUploadLog[i-1].rDateTime.bDay;
            CMOSWebVisDataUploadLog[i].rDateTime.bDayOfWeek = CMOSWebVisDataUploadLog[i-1].rDateTime.bDayOfWeek;
            CMOSWebVisDataUploadLog[i].rDateTime.bHour = CMOSWebVisDataUploadLog[i-1].rDateTime.bHour;
            CMOSWebVisDataUploadLog[i].rDateTime.bMinutes = CMOSWebVisDataUploadLog[i-1].rDateTime.bMinutes;
            CMOSWebVisDataUploadLog[i].rDateTime.bSeconds = CMOSWebVisDataUploadLog[i-1].rDateTime.bSeconds;
        }

        /* Set the new record to the first slot, which is available to fill in now */

        /* Set Upload details to the bUploadType field  */
        /*
          #define OI_USAGE_UPLOADED      0x01
          #define OI_E_RECORDS_UPLOADED  0x02
          #define OI_ACCOUNTING_UPLOADED 0x04
        */
        /* Reset all UPLOAD TYPE bits to zero first*/
        CMOSWebVisDataUploadLog[0].bUploadType = 0;

        if(fStrDCapUploaded == OIT_INFRA_UPLOAD_SUCCESS)//Usage Data Uploaded
        {
            CMOSWebVisDataUploadLog[0].bUploadType |= OI_USAGE_UPLOADED;
        }

        if(fStrEServiceUploaded == OIT_INFRA_UPLOAD_SUCCESS)//E-Records Uploaded
        {
            CMOSWebVisDataUploadLog[0].bUploadType |= OI_E_RECORDS_UPLOADED;
        }

        if(fStrAcctUploaded == OIT_INFRA_UPLOAD_SUCCESS)//Accounting Data Uploaded
        {
            CMOSWebVisDataUploadLog[0].bUploadType |= OI_ACCOUNTING_UPLOADED;
        }      

        /* Set the DATE AND TIME */
        CMOSWebVisDataUploadLog[0].rDateTime.bCentury = rDateTime.bCentury;            
        CMOSWebVisDataUploadLog[0].rDateTime.bYear = rDateTime.bYear;
        CMOSWebVisDataUploadLog[0].rDateTime.bMonth = rDateTime.bMonth;
        CMOSWebVisDataUploadLog[0].rDateTime.bDay = rDateTime.bDay;
        CMOSWebVisDataUploadLog[0].rDateTime.bDayOfWeek = rDateTime.bDayOfWeek;
        CMOSWebVisDataUploadLog[0].rDateTime.bHour = rDateTime.bHour;
        CMOSWebVisDataUploadLog[0].rDateTime.bMinutes = rDateTime.bMinutes;
        CMOSWebVisDataUploadLog[0].rDateTime.bSeconds = rDateTime.bSeconds;           
      
      }       

    }
   
    /* Reset the timeout value. */
    lOITInfraScrnTimeoutTicks = INFRA_OI_TIMEOUT_INTERVAL_DATA_UPLOAD;
    
    /* Reset the expire flag. */
    fOITInfraScrnTimeoutExpired = FALSE;
    
    /* Disable the clock. */
    fOITInfraScrnTimeoutEnabled = FALSE;

    /* Reset those string display flags */
    fStrDCapUploaded = OIT_INFRA_UPLOAD_NOT;
    fStrEServiceUploaded = OIT_INFRA_UPLOAD_NOT;

/* comment out by Raymond Shen
    if(fStrAcctUploaded == OIT_INFRA_UPLOAD_SUCCESS)
    {
        // notify OI we are successfully done with account data upload
        fnBMWVSetDataUploadStatus(TRUE);        
    }
*/

    fStrAcctUploaded = OIT_INFRA_UPLOAD_NOT;

    /*reset the counts*/
    sOITSentDataUploadCounts = 0;
    sOITTotalDataUploadCounts = 0; 

    /* Reset the flag */
    bDataUploadInProgress = FALSE;
    fnOITSetCurrentOnlineServType(OI_ONLINE_SERV_AUTO);

    usOITCurrentDisplayService = 0;

    /* This complete message is always sent once the DataUpload(WebVis) Feature is enabled.*/
    /*Send a reply back as what is done in other service.*/
    (void)OSSendIntertask( DISTTASK, OIT, (unsigned char)OIT_SERVICE_COMPLETE_REPLY, NO_DATA, NULL, 0 );

    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpInfraDUploadErrStartTimeoutClock
//
// DESCRIPTION: 
//      Pre Function that sets the Infrastructure Screen Timout Clock
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/31/2008  Raymond Shen    Adapted for FPHX
//              Zhang Yingbo    Initial version.
//
// *************************************************************************/
SINT8 fnpInfraDUploadErrStartTimeoutClock( void         (** pContinuationFcn)(), 
                               UINT32       *pMsgsAwaited,
                               T_SCREEN_ID  *pNextScr )
{       
    *pNextScr = 0;  

/* Commented out by Raymond Shen, it seems ARM is not supported by FPHX for now.
    if(fnIsAMRActive()
        && (bOITRequestedService == REFILL_SERVICE)
        && (fnGetAutoRefill() == TRUE))//AMR Refill Service Requested
    {
        // Reset the timeout value.
        lOITInfraScrnTimeoutTicks = OI_INFRA_SCREEN_TIMOUT_VALUE;    
        // Reset the expire flag.
        fOITInfraScrnTimeoutExpired = FALSE;    
        // Enable the clock.
        fOITInfraScrnTimeoutEnabled = TRUE;        
    }
    else
*/
    {        
        /* Reset the timeout value. */
        lOITInfraScrnTimeoutTicks = INFRA_OI_TIMEOUT_INTERVAL_DATA_UPLOAD;    
        /* Reset the expire flag. */
        fOITInfraScrnTimeoutExpired = FALSE;    
        /* Disable the clock. */
        fOITInfraScrnTimeoutEnabled = FALSE;        
    }
    
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpInfDataUploadErr
//
// DESCRIPTION: 
//      Post Function of InfraProcessDataUploadErr screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      Zhang Yingbo    Initial version.
//
// *************************************************************************/
SINT8 fnpInfDataUploadErr (void     (** pContinuationFcn)(), 
                                   UINT32    *   pMsgsAwaited)
{
    DATETIME  rDateTime;

    UINT8 i = 0;    

    /* Record this Data Upload to CMOS log before reset the flags  */
    if((fStrDCapUploaded == OIT_INFRA_UPLOAD_SUCCESS)
        || (fStrEServiceUploaded == OIT_INFRA_UPLOAD_SUCCESS)
         || (fStrAcctUploaded == OIT_INFRA_UPLOAD_SUCCESS))//If There are valid data to record to log
    {
      /* Get the Gregorian date/time with offset adjustments */
      if (GetSysDateTime(&rDateTime, ADDOFFSETS, GREGORIAN) == SUCCESSFUL)
      {
        /* push the slot down with 1 step */
        for( i = (BMWV_MAX_DATA_UPLOAD_LOG - 1) ; i > 0; i-- )
        {
            CMOSWebVisDataUploadLog[i].bUploadType = CMOSWebVisDataUploadLog[i-1].bUploadType;

            CMOSWebVisDataUploadLog[i].rDateTime.bCentury = CMOSWebVisDataUploadLog[i-1].rDateTime.bCentury;            
            CMOSWebVisDataUploadLog[i].rDateTime.bYear = CMOSWebVisDataUploadLog[i-1].rDateTime.bYear;
            CMOSWebVisDataUploadLog[i].rDateTime.bMonth = CMOSWebVisDataUploadLog[i-1].rDateTime.bMonth;
            CMOSWebVisDataUploadLog[i].rDateTime.bDay = CMOSWebVisDataUploadLog[i-1].rDateTime.bDay;
            CMOSWebVisDataUploadLog[i].rDateTime.bDayOfWeek = CMOSWebVisDataUploadLog[i-1].rDateTime.bDayOfWeek;
            CMOSWebVisDataUploadLog[i].rDateTime.bHour = CMOSWebVisDataUploadLog[i-1].rDateTime.bHour;
            CMOSWebVisDataUploadLog[i].rDateTime.bMinutes = CMOSWebVisDataUploadLog[i-1].rDateTime.bMinutes;
            CMOSWebVisDataUploadLog[i].rDateTime.bSeconds = CMOSWebVisDataUploadLog[i-1].rDateTime.bSeconds;
        }

        /* Set the new record to the first slot, which is available to fill in now */

        /* Set Upload details to the bUploadType field  */
        /*
          #define OI_USAGE_UPLOADED      0x01
          #define OI_E_RECORDS_UPLOADED  0x02
          #define OI_ACCOUNTING_UPLOADED 0x04
        */
        /* Reset all UPLOAD TYPE bits to zero first*/
        CMOSWebVisDataUploadLog[0].bUploadType = 0;

        if(fStrDCapUploaded == OIT_INFRA_UPLOAD_SUCCESS)//Usage Data Uploaded
        {
            CMOSWebVisDataUploadLog[0].bUploadType |= OI_USAGE_UPLOADED;
        }

        if(fStrEServiceUploaded == OIT_INFRA_UPLOAD_SUCCESS)//E-Records Uploaded
        {
            CMOSWebVisDataUploadLog[0].bUploadType |= OI_E_RECORDS_UPLOADED;
        }

        if(fStrAcctUploaded == OIT_INFRA_UPLOAD_SUCCESS)//Accounting Data Uploaded
        {
            CMOSWebVisDataUploadLog[0].bUploadType |= OI_ACCOUNTING_UPLOADED;
        }      

        /* Set the DATE AND TIME */
        CMOSWebVisDataUploadLog[0].rDateTime.bCentury = rDateTime.bCentury;            
        CMOSWebVisDataUploadLog[0].rDateTime.bYear = rDateTime.bYear;
        CMOSWebVisDataUploadLog[0].rDateTime.bMonth = rDateTime.bMonth;
        CMOSWebVisDataUploadLog[0].rDateTime.bDay = rDateTime.bDay;
        CMOSWebVisDataUploadLog[0].rDateTime.bDayOfWeek = rDateTime.bDayOfWeek;
        CMOSWebVisDataUploadLog[0].rDateTime.bHour = rDateTime.bHour;
        CMOSWebVisDataUploadLog[0].rDateTime.bMinutes = rDateTime.bMinutes;
        CMOSWebVisDataUploadLog[0].rDateTime.bSeconds = rDateTime.bSeconds;           
      
      }     

    }  
   
    /* Reset the timeout value. */
    lOITInfraScrnTimeoutTicks = INFRA_OI_TIMEOUT_INTERVAL_DATA_UPLOAD;
    
    /* Reset the expire flag. */
    fOITInfraScrnTimeoutExpired = FALSE;
    
    /* Disable the clock. */
    fOITInfraScrnTimeoutEnabled = FALSE;

    /* Reset those string display flags */
    fStrDCapUploaded = OIT_INFRA_UPLOAD_NOT;
    fStrEServiceUploaded = OIT_INFRA_UPLOAD_NOT;

/* comment out by Raymond Shen
    if( (fStrAcctUploaded == OIT_INFRA_UPLOAD_FAILED)
        || (fStrAcctUploaded == OIT_INFRA_UPLOAD_FAILED_MODE_1) 
        || (fStrAcctUploaded == OIT_INFRA_UPLOAD_FAILED_MODE_2))
    {
        // notify OI we failed to do account data upload
        fnBMWVSetDataUploadStatus(FALSE);        
    }
*/

    fStrAcctUploaded = OIT_INFRA_UPLOAD_NOT;

    /*reset the error code*/
    lDataUploadErrCode = 0;

    /*reset the counts*/
    sOITSentDataUploadCounts = 0;
    sOITTotalDataUploadCounts = 0; 

    /* Reset the flag */
    bDataUploadInProgress = FALSE;
    fnOITSetCurrentOnlineServType(OI_ONLINE_SERV_AUTO);

    usOITCurrentDisplayService = 0;

    /* This complete message is always sent once the DataUpload(WebVis) Feature is enabled.*/
    /*Send a reply back as what is done in other service.*/
    (void)OSSendIntertask( DISTTASK, OIT, (unsigned char)OIT_SERVICE_COMPLETE_REPLY, NO_DATA, NULL, 0 );

    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepDataUploadDetails
//
// DESCRIPTION: 
//      Pre Function that prepares for showing Data Upload details
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      Zhang Yingbo    Initial version.
//
// *************************************************************************/
SINT8 fnpPrepDataUploadDetails( void         (** pContinuationFcn)(), 
                               UINT32       *pMsgsAwaited,
                               T_SCREEN_ID  *pNextScr )
{
    UINT8 bUploadType = CMOSWebVisDataUploadLog[usDataUploadCurLogIndex].bUploadType;

    *pNextScr = 0; 

    /* Reset the flags*/
    fStrDCapUploaded = OIT_INFRA_UPLOAD_NOT;
    fStrEServiceUploaded = OIT_INFRA_UPLOAD_NOT;
    fStrAcctUploaded = OIT_INFRA_UPLOAD_NOT;

    /* Set the flags */
    if(( bUploadType & OI_USAGE_UPLOADED ) != 0)
    {
        fStrDCapUploaded = OIT_INFRA_UPLOAD_SUCCESS;
    }
   
    if(( bUploadType & OI_E_RECORDS_UPLOADED ) != 0)
    {
        fStrEServiceUploaded = OIT_INFRA_UPLOAD_SUCCESS;
    }

    if(( bUploadType & OI_ACCOUNTING_UPLOADED ) != 0)
    {
        fStrAcctUploaded = OIT_INFRA_UPLOAD_SUCCESS;
    }      
    
    
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostDataUploadDetails
//
// DESCRIPTION: 
//      Post Function of AbacusDataUploadsDetails screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//              Zhang Yingbo    Initial version.
//
// *************************************************************************/
SINT8 fnpPostDataUploadDetails (void     (** pContinuationFcn)(), 
                                   UINT32    *   pMsgsAwaited)
{
    /*Reset Index to 0*/
    usDataUploadCurLogIndex = 0;

    /* Reset the flags*/
    fStrDCapUploaded = OIT_INFRA_UPLOAD_NOT;
    fStrEServiceUploaded = OIT_INFRA_UPLOAD_NOT;
    fStrAcctUploaded = OIT_INFRA_UPLOAD_NOT;
    
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepDataUploadLog
//
// DESCRIPTION: 
//      Pre Function that prepares for showing Data Upload summary
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/25/2008    Raymond Shen    Initial version.
//
// *************************************************************************/
SINT8 fnpPrepDataUploadLog( void         (** pContinuationFcn)(), 
                               UINT32       *pMsgsAwaited,
                               T_SCREEN_ID  *pNextScr )
{
    UINT16  usSlotIndex = 0;
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();

    // Initializing
    fnClearSMenuTable();

    // Just do this for screen scrolling and item selection. Don't need to 
    // fill contents into the menu slots because here we will not use the
    // general function fnfSMenuLabelSlot1~4 to display the list.
    while(CMOSWebVisDataUploadLog[usSlotIndex].bUploadType != 0)
    {
        pMenuTable->pSlot[usSlotIndex].bKeyAssignment = usSlotIndex;
        pMenuTable->pSlot[usSlotIndex].fActive = TRUE;
        usSlotIndex ++;
    }

    // Set the max entry number per page will show.
    pMenuTable->bEntriesPerPage = 4;

    // Set the total number of entries which will be use for displaying scroll bar
    fnSetMenuEntriesTotalNumber (usSlotIndex);

    // Update LED indicator
    fnUpdateLEDPageIndicator();

    *pNextScr = 0;		/* leave next screen unchanged */
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostDataUploadLog
//
// DESCRIPTION: 
//      Post Function of AbacusDataUploadsSummary screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//  07/25/2008    Raymond Shen    Initial version.
//
// *************************************************************************/
SINT8 fnpPostDataUploadLog (void     (** pContinuationFcn)(), 
                                   UINT32    *   pMsgsAwaited)
{
    SET_MENU_LED_OFF();

    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostAbacusDataUploadAlert
//
// DESCRIPTION: 
//      Post Function of AbacusDataUploadAlert screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//  06/17/2009  Raymond Shen    Initial version
//
// *************************************************************************/
SINT8 fnpPostAbacusDataUploadAlert (void     (** pContinuationFcn)(), 
                                   UINT32    *   pMsgsAwaited)
{
    // Clear the Flags
    fnOITSetDataUploadReqFromStatus(OI_DATA_UPLD_FROM_NONE);
    fnBMWVSetActChgToType(0);

    return (BOOL)PREPOST_COMPLETE;
}


/* Field functions */

/* *************************************************************************
// FUNCTION NAME: 
//		fnfInfraContactPBCaption
//
// DESCRIPTION: 
// 		Output field function to display the caption of InfraContactPB screen
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//		Menu possibilities:
//		1. Connecting To Data Center
//		2. Refilling Postage
//		3. Requesting PBP Balance
//		4. Withdrawing Funds
//		5. Processing
//		6. Checking For System Updates
//      7. Checking For Pending Uploads
//      8. Data Capture Data Being Upload
//
// MODIFICATION HISTORY:
//  09/17/2014  Wang Biao       Moved the usUploadFlagClear setting to acctUploadService() &
//                              fnDCAP_SM_StartUpload() for fraca 227025.
//  06/08/2014  Wang Biao       Modified to fix fraca 225401.
//	09/29/2012	Bob Li			Modified this function to display the correct service status.
//  09 Sep 2009 Raymond Shen    Modified case CONFIRMATION_SERVICE to fix GMSE00170347: 
//                              G910/DM300c-Meter should not display "Conf Svc Records
//                              Being Uploaded" screen and print receipt after OOB 
//                              without CPS records.
//  23 Sep 2008 Joe Qu          Modified case CONFIRMATION_SERVICE to support
//                              downloading EService Barcodes for G9G2 Netherlands
//  29 Mar 2007 Bill Herring    Added case for Meter Move
//  12/14/2006  Adam Liu        Add DCAP field text
//  07/07/2006  Dicky Sun       Add Uploading GTS record prompt
// 	1/23/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnfInfraContactPBCaption (UINT16      *pFieldDest, 
							   UINT8 		bLen, 
							   UINT8 		bFieldCtrl,
							   UINT8 		bNumTableItems, 
							   UINT8       *pTableTextIDs,
	                           UINT8       *pAttributes)
{
	UINT8	ucFieldIndex = bNumTableItems;

	SET_SPACE_UNICODE(pFieldDest, bLen);
	
	if (eInfraContactPBState == INFRA_CONTACT_PB_DIALING ||
		eInfraContactPBState == INFRA_CONTACT_PB_CONNECTING)
	{
		ucFieldIndex = 0;	// "Connecting to Data Center"	
	}
	else
	{	// INFRA_CONTACT_PB_REQUESTING
		switch (ucOITRequestedService)
		{
		case REFILL_SERVICE:
			ucFieldIndex = 1;	// "Refilling Postage"
			break;
		
		case ACCTBAL_SERVICE:
			ucFieldIndex = 2;	// "Requesting PBP Balance"
			break;

		case REFUND_SERVICE:
			ucFieldIndex = 3;	// "Withdrawing Funds"
			break;

		case ET_CALL_HOME:
		case SWDOWNLOAD_SERVICE:
			ucFieldIndex = 5;	// "Checking For System Updates"
			break;
        case DCAP_UPLOAD_SERVICE:
		    ucFieldIndex = 7;   // "Data Capture Data being uploaded"
       		break;            
        case METER_MOVE_SERVICE:
		    ucFieldIndex = 8;   // "Meter Move"
       		break;            
        case DATA_UPLOAD_SERVICE:
		    ucFieldIndex = 9;   // "Checking For Pending Uploads"
       		break;

        case TRAN_RECORD_UPLOAD_SERVICE:
        	ucFieldIndex = 14;
        	break;
		default:
			ucFieldIndex = 4;	// "Processing"	
			break;
		}	// switch (...)
	}

    /* If the Data Upload Started at this point, change caption to indicate this */
/*
    if(IsAbAccountingUploadEnabled() 
            && (bDataUploadInProgress == TRUE)
            && (ucOITCurrentService == DATA_UPLOAD_SERVICE))
    {
         We are now in data upload Processing screen so check the current service for the display
        switch (usOITCurrentDisplayService)
        {
        case DCAP_UPLOAD_SERVICE:
                ucFieldIndex = 10;   // "Usage Data being uploaded"
                break;
        case CONFIRMATION_SERVICE:
                ucFieldIndex = 11;   // "E-Records Data being uploaded"
                break;
        case DATA_UPLOAD_SERVICE:
                ucFieldIndex = 12;  // "Accounting Data being uploaded"
                break;
        default:
                //ucFieldIndex = 4;   // "Processing" 
                                        //Don't display the unexpected message
                                        //so as to display the correct message according to ucOITRequestedService
                break;
       }
    }
*/

	if (ucFieldIndex < bNumTableItems)
	{
		fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
							pTableTextIDs, ucFieldIndex);
	}

	return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfInfraGenErrorCaption
//
// DESCRIPTION: 
// 		Output field function to display the caption of InfraCallErrGeneral screen
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//		Menu possibilities:
//		1. Cannot Connect To Data Center
//		2. Refilling Failed
//		3. Checking PBP Balance Failed
//		4. ReFunds Failed
//		5. PBP Error
//
// MODIFICATION HISTORY:
//  07/19/2006  Dicky Sun       Add GTS uploading record error
// 	04/12/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnfInfraGenErrorCaption (UINT16      *pFieldDest, 
							  UINT8 	   bLen, 
							  UINT8 	   bFieldCtrl,
							  UINT8 	   bNumTableItems, 
							  UINT8       *pTableTextIDs,
	                          UINT8       *pAttributes)
{
	UINT8	ucFieldIndex = bNumTableItems;

	SET_SPACE_UNICODE(pFieldDest, bLen);
	
	if (eInfraContactPBState == INFRA_CONTACT_PB_DIALING ||
		eInfraContactPBState == INFRA_CONTACT_PB_CONNECTING)
	{
		ucFieldIndex = 0;	// "Connecting to Data Center"	
	}
	else
	{	// INFRA_CONTACT_PB_REQUESTING
		switch (ucOITRequestedService)
		{
		case REFILL_SERVICE:
			ucFieldIndex = 1;	// "Refilling Postage"
			break;
		
		case ACCTBAL_SERVICE:
			ucFieldIndex = 2;	// "Requesting PBP Balance"
			break;

		case REFUND_SERVICE:
			ucFieldIndex = 3;	// "Withdrawing Funds"
			break;

        case CONFIRMATION_SERVICE: // "Data Upload Failed"
		    ucFieldIndex = 5;
		    break;
		default:
			ucFieldIndex = 4;	// "Processing"	
			break;
		}	// switch (...)
	}

	if (ucFieldIndex < bNumTableItems)
	{
		fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
							pTableTextIDs, ucFieldIndex);
	}

	return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfInfraDialingInfo1
//
// DESCRIPTION: 
// 		Output field function to display information of line1 in InfraContactPB screen
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	1/23/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnfInfraDialingInfo1 (UINT16      *pFieldDest, 
						   UINT8 		bLen, 
						   UINT8 		bFieldCtrl,
						   UINT8 		bNumTableItems, 
						   UINT8       *pTableTextIDs,
                           UINT8       *pAttributes)
{
	UINT8	ucFieldIndex = bNumTableItems;

	SET_SPACE_UNICODE(pFieldDest, bLen);

	if (eInfraContactPBState == INFRA_CONTACT_PB_DIALING)
	{
		ucFieldIndex = 0;	
	}
	else if (eInfraContactPBState == INFRA_CONTACT_PB_CONNECTING)
	{
		ucFieldIndex = 1;
	}

	if (ucFieldIndex < bNumTableItems)
	{
		fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
							pTableTextIDs, ucFieldIndex);
	}

	return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfInfraDialingInfo2
//
// DESCRIPTION: 
// 		Output field function to display information of line2 in InfraContactPB screen
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//		Menu possibilities:
//
// MODIFICATION HISTORY:
// 	1/23/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnfInfraDialingInfo2 (UINT16      *pFieldDest, 
						   UINT8 		bLen, 
						   UINT8 		bFieldCtrl,
						   UINT8 		bNumTableItems, 
						   UINT8       *pTableTextIDs,
                           UINT8       *pAttributes)
{
	char 		cPhoneNumBuff[MAX_PREFIX_LEN + MAX_PHONE_LEN + 2];
	UINT8 		ucNumLen;
	SetupParams	*pCMOSSetupParams = fnCMOSSetupGetCMOSSetupParams();

	SET_SPACE_UNICODE(pFieldDest, bLen);
	
	if (eInfraContactPBState == INFRA_CONTACT_PB_DIALING)
	{
		// Display the data center number

		/* concatinate dialing prefix with phone number */
		if(pCMOSSetupParams->DialingPrefix[0] != (char) 0)
		{
			ucNumLen = (UINT8)sprintf(cPhoneNumBuff, "%s%c%s",
										pCMOSSetupParams->DialingPrefix, ',',
										pCMOSSetupParams->PhoneNumber);
		}
		else
		{
			ucNumLen = (UINT8)sprintf(cPhoneNumBuff, "%s%s",
										pCMOSSetupParams->DialingPrefix,
										pCMOSSetupParams->PhoneNumber);
		}

		if (ucNumLen > bLen)
		{
			ucNumLen = bLen;
		}

		fnAscii2Unicode(cPhoneNumBuff, pFieldDest, ucNumLen);
		fnUnicodeCenterAligned (pFieldDest, ucNumLen, bLen);
		pFieldDest[bLen] = (UINT16)NULL;
	}

	return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfInfraDialingErrorInfo
//
// DESCRIPTION: 
// 		Output field function to display error info during dialing and connecting
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//		Menu possibilities:
//			0.	No dial tone. Connect analog phone cord.
//			1.	Line is busy.
//			2.	Connection lost.
//			3.	Link negotiation failed.
//			4.	Invalid user name & password.
//			5.	The modem failed to answer.
//			6.	No carrier detected on line.
//			7.	Modem connection to ATT failed.
//			8.	No modem detected.
//
// MODIFICATION HISTORY:
// 	1/24/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnfInfraDialingErrorInfo (UINT16      *pFieldDest, 
							   UINT8 		bLen, 
							   UINT8 		bFieldCtrl,
							   UINT8 		bNumTableItems, 
							   UINT8       *pTableTextIDs,
	                           UINT8       *pAttributes)
{
	if (ucInfraDialingErrorInfoNo < bNumTableItems)
	{
		fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
							pTableTextIDs, ucInfraDialingErrorInfoNo);
	}

	return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfInfraPbpError
//
// DESCRIPTION: 
// 		Output field function to display Infrastructure errors
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//		Menu possibilities:
//
// MODIFICATION HISTORY:
//  03/31/2009  Jingwei,Li      Don't display background color when no warning/error.
// 	02/16/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
BOOL fnfInfraPbpError (UINT16      *pFieldDest, 
					   UINT8 		bLen, 
					   UINT8 		bFieldCtrl,
					   UINT8 		bNumTableItems, 
					   UINT8       *pTableTextIDs,
                       UINT8       *pAttributes)
{
	UINT8 		bErrLen;
	UINT32 		ulDCStatus = 0;

	/* initialize the field to all spaces */
	SET_SPACE_UNICODE(pFieldDest, bLen);

	if (fnValidData(BOBAMAT0, BOBS_LAST_DCSTATUS, (void *) &ulDCStatus) != BOB_OK)
	{
		fnProcessSCMError();
		*pFieldDest = NULL_VAL;
	}
	else
	{
		bErrLen = fnBin2Unicode(pFieldDest, ulDCStatus);

		/* if field is right-aligned, pad left of string with spaces */
		if (bFieldCtrl & ALIGN_MASK)
		{
			fnUnicodePadSpace(pFieldDest, bErrLen, bLen);
		}
		pFieldDest[bLen] = (unsigned short) NULL;
	}

	return ((BOOL)SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfInfraDownloadFileCountTotal
//
// DESCRIPTION: 
// 		This function displays the total number of files that the UIC will 
// 		download while connected to the download services server.
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//		None
//
// MODIFICATION HISTORY:
//				Derek DeGennaro	Initial version
// 	04/12/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
BOOL fnfInfraDownloadFileCountTotal (UINT16     *pFieldDest, 
									 UINT8 		bLen, 
									 UINT8 		bFieldCtrl,
									 UINT8 		bNumTableItems, 
									 UINT8      *pTableTextIDs,
				                     UINT8      *pAttributes)
{
	char 	cFileCountBuff[10];

	/* initialize the field to all spaces */
	SET_SPACE_UNICODE(pFieldDest, bLen);

	sprintf(cFileCountBuff,"%lu",(UINT32)bOITInfraTotalFilesToDownload);
	fnAscii2Unicode(cFileCountBuff, pFieldDest, strlen(cFileCountBuff));

	if (bFieldCtrl & ALIGN_MASK)
	{
		pFieldDest[strlen(cFileCountBuff)] = (UINT16) NULL;
		fnUnicodePadSpace(pFieldDest, strlen(cFileCountBuff), bLen);
	}
	
	return ((BOOL)SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfInfraDownloadFileCountCurrent
//
// DESCRIPTION: 
// 		This function displays number of files that have already been downloaded
// 		from the server during this connection.
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//		None
//
// MODIFICATION HISTORY:
//				Derek DeGennaro	Initial version
// 	04/12/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
BOOL fnfInfraDownloadFileCountCurrent (UINT16     *	pFieldDest, 
									   UINT8 		bLen, 
									   UINT8 		bFieldCtrl,
									   UINT8 		bNumTableItems, 
									   UINT8      *	pTableTextIDs,
					                   UINT8      *	pAttributes)
{
	char	cFileCountBuff[10];

	/* initialize the field to all spaces */
	SET_SPACE_UNICODE(pFieldDest, bLen);

	sprintf(cFileCountBuff,"%lu",(UINT32)bOITInfraCurrentFileToDownload);
	fnAscii2Unicode(cFileCountBuff, pFieldDest, strlen(cFileCountBuff));

	if (bFieldCtrl & ALIGN_MASK)
	{
		pFieldDest[strlen(cFileCountBuff)] = (UINT16) NULL;
		fnUnicodePadSpace(pFieldDest, strlen(cFileCountBuff), bLen);
	}
	
	return ((BOOL)SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfInfraDownloadPercentage
//
// DESCRIPTION: 
// 		This function displays the percentage of finished for the file currently 
//		downloaded.
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//		None
//
// MODIFICATION HISTORY:
//				Derek DeGennaro	Initial version
// 	04/12/2006	Vincent Yi		Code cleanup
//  01/05/2006  Adam Liu        If the percentage >= 100, change it to 99
//  01/31/2006  Adam Liu        If the percentage >= 100, change it to 100
//
// *************************************************************************/
BOOL fnfInfraDownloadPercentage (UINT16     *	pFieldDest, 
							     UINT8 			bLen, 
							     UINT8 			bFieldCtrl,
							     UINT8 			bNumTableItems, 
							     UINT8      *	pTableTextIDs,
			                     UINT8      *	pAttributes)
{
	UINT8	bPercentage = 0;
	char 	cPercentageBuff[20];

	/* initialize the field to all spaces */
	SET_SPACE_UNICODE(pFieldDest, bLen);

	if (ulOITInfraTotalBytesToDownload != 0)
	{
		bPercentage = (ulOITInfraDownloadedBytes * 100) / 
							ulOITInfraTotalBytesToDownload;
	}
	else
	{
		bPercentage = 0;
	}

	// An additional time is needed to finished the dowloading so we display 
	// 100% with remain time 00:00.   
	if(bPercentage >= 100)
	{
		bPercentage = 100;   
	}

	sprintf(cPercentageBuff,"%lu",(UINT32)bPercentage);
	fnAscii2Unicode(cPercentageBuff, pFieldDest, strlen(cPercentageBuff));

	if (bFieldCtrl & ALIGN_MASK)
	{
		pFieldDest[strlen(cPercentageBuff)] = (UINT16) NULL;
		fnUnicodePadSpace(pFieldDest, strlen(cPercentageBuff), bLen);
	}

	return ((BOOL)SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfInfraDownloadProgressBar
//
// DESCRIPTION: 
// 		This function displays the process bar during download.
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//		None.
//
// MODIFICATION HISTORY:
//				Derek DeGennaro	Initial version
// 	04/12/2006	Vincent Yi		Code cleanup
//  04/21/2010  Jingwei,LI    Modified to support RTL.
//  05/18/2010  Jingwei,Li    Make the progress bar increasing from Right to Left 
//                            for RTL writting.
//
// *************************************************************************/
BOOL fnfInfraDownloadProgressBar (UINT16     *	pFieldDest, 
							     UINT8 			bLen, 
							     UINT8 			bFieldCtrl,
							     UINT8 			bNumTableItems, 
							     UINT8      *	pTableTextIDs,
			                     UINT8      *	pAttributes)
{
	UINT8		ucBarLen = 0;

	/* initialize the field to all spaces */
	SET_SPACE_UNICODE(pFieldDest, bLen);

	if (ulOITInfraTotalBytesToDownload != 0)
	{
		ucBarLen = (ulOITInfraDownloadedBytes * bLen) / 
					ulOITInfraTotalBytesToDownload;
	}
	else
	{
		ucBarLen = 0;
	}

	if (bNumTableItems > 0)
	{
		UINT8		ucIndex;
		UINT16		usTextID;
		UNICHAR		uniBarUnicode;
		UINT8   ubWritingDir;
        
		ubWritingDir = fnGetWritingDir();

		EndianAwareCopy(&usTextID, pTableTextIDs, 2);
		if(ubWritingDir == RIGHT_TO_LEFT)
		{
		    (void) fnCopyTableTextForRTL(&uniBarUnicode, usTextID, 1);
		    for (ucIndex = ucBarLen; ucIndex > 0; ucIndex --)
		    {
		        pFieldDest[bLen-ucIndex] = uniBarUnicode;
		    }
		}
		else
		{
		    (void) fnCopyTableText(&uniBarUnicode, usTextID, 1);
		    for (ucIndex = 0; ucIndex < ucBarLen; ucIndex++)
		    {
		        pFieldDest[ucIndex] = uniBarUnicode;
		    }
		}
	}

	return ((BOOL)SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfInfraDownloadComplete
//
// DESCRIPTION: 
// 		This function displays Return Home or Restart Now
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//		Menu possibilities:
//		1. Return Home
//		2. Restart Now
//
// MODIFICATION HISTORY:
//				Derek DeGennaro	Initial version
// 	04/12/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
BOOL fnfInfraDownloadComplete (UINT16     *	pFieldDest, 
							   UINT8 		bLen, 
							   UINT8 		bFieldCtrl,
							   UINT8 		bNumTableItems, 
							   UINT8      *	pTableTextIDs,
			                   UINT8      *	pAttributes)
{
	UINT8	ucFieldIndex = bNumTableItems;

	if (fOITDownloadDoneReboot == TRUE)
	{
		ucFieldIndex = 1; // Restart Now
	}
	else
	{
		ucFieldIndex = 0; // Return Home
	}

	if (ucFieldIndex < bNumTableItems)
	{
		fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
							pTableTextIDs, ucFieldIndex);
	}

	return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfInfraTimeoutClockValue
//
// DESCRIPTION: 
// 		This field function displays the number of seconds left in the 
// 		Infrastructure Screen Timeout Clock.
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//		None.
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
// 	04/12/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
BOOL fnfInfraTimeoutClockValue (UINT16     *	pFieldDest, 
							    UINT8 			bLen, 
							    UINT8 			bFieldCtrl,
							    UINT8 			bNumTableItems, 
							    UINT8      *	pTableTextIDs,
			                    UINT8      *	pAttributes)
{
	UINT8 	bTimeLen;

	SET_SPACE_UNICODE(pFieldDest, bLen);

	/* Make sure the clock is enabled. */
	if (fOITInfraScrnTimeoutEnabled == TRUE 	&&
		fOITDownloadDoneReboot == TRUE)
	{
		bTimeLen = fnBin2Unicode(pFieldDest, lOITInfraScrnTimeoutTicks);

		/* if field is right-aligned, pad left of string with spaces */
		if (bFieldCtrl & ALIGN_MASK)
		{
			pFieldDest[bTimeLen] = (UINT16) NULL;
			fnUnicodePadSpace(pFieldDest, bTimeLen, bLen);
		}

		/* make sure the field is NULL terminated.  if the translation happened 
			to exceed the width of this field, this will truncate the unicode 
			string that is sitting there */
		pFieldDest[bLen] = (UINT16) NULL;
	}

	return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfInfraDownloadRemainTime
//
// DESCRIPTION: 
// 		Field function that displays the estimated left time during downloading.
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//		None.
//
// MODIFICATION HISTORY:
// 	06/07/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnfInfraDownloadRemainTime (UINT16     *	pFieldDest, 
							     UINT8 			bLen, 
							     UINT8 			bFieldCtrl,
							     UINT8 			bNumTableItems, 
							     UINT8      *	pTableTextIDs,
			                     UINT8      *	pAttributes)
{
	UINT32 		ulMinutes;
	UINT32 		ulSeconds;
	UINT8		ucStrLen=0;
	char		cTempStr[10];

	/* initialize the field to all spaces */
	SET_SPACE_UNICODE(pFieldDest, bLen);

	ulMinutes = ulDownloadEstimateRemainTime / 60;
    ulSeconds = ulDownloadEstimateRemainTime % 60;
    sprintf(cTempStr, "%d:%2.2d", ulMinutes, ulSeconds);
    ucStrLen = strlen(cTempStr);

    /* convert the binary number to Unicode */
    fnAscii2Unicode(cTempStr, pFieldDest, bLen);

	/* if field is right-aligned, pad left of string with spaces */
	if (bFieldCtrl & ALIGN_MASK)
	{
		pFieldDest[bLen] = (UINT16) NULL;
		fnUnicodePadSpace(pFieldDest, ucStrLen, bLen);
	}

	/* make sure the field is NULL terminated.  if the translation happened to exceed
		the width of this field, this will truncate the unicode string that is sitting
		there */
	pFieldDest[bLen] = (UINT16) NULL;

	return ((BOOL)SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfDialTonePulse
//
// DESCRIPTION: 
//      Output field function for the current dialing configuration - Tone / Pulse.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//    02/15/2006    Kan Jiang    Initial version
// *************************************************************************/
BOOL fnfDialTonePulse(UINT16 *pFieldDest,
                      UINT8   bLen,
                      UINT8   bFieldCtrl, 
                      UINT8   bNumTableItems,
                      UINT8  *pTableTextIDs, 
                      UINT8  *pAttributes)
{
	UINT16 usTextID;
	
	SET_SPACE_UNICODE(pFieldDest, bLen);

    // current dialing configuration is Tone
	if ( bOITModemDialMode == DIALING_TONE) 
	{
		usTextID = 0;
	}
	else
	{
		usTextID = 1;
	}

	fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
	                    pTableTextIDs, usTextID);

	return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME:
//        fnfStdDistrParamEntryInit
//
// DESCRIPTION:
//        Initializes the entry field where the distributor parameter is typed.
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//		 07/18/2006	  Raymond Shen	Deleted parameter specific code since this action
//									has been added into fnfInfraScrollEntryRefresh().
//       01/12/2006   Kan Jiang  Initial version
// *************************************************************************/
BOOL fnfStdDistrParamEntryInit (UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
     UINT8   bBuffLen; 
     UINT8  *pStringPtr;
     UINT16  ucIndex;

     (void)fnfScrollableEntryInit(pFieldDest, bLen, bFieldCtrl, bNumTableItems,
                                  pTableTextIDs, pAttributes);

     // the distributor parameters already stores in array pDistParamTmp
     pStringPtr = (UINT8 *)pDistParamTmp;  

     bBuffLen = (UINT8)strlen((char *)pStringPtr);
     
     ucIndex = 0; 
     while (ucIndex < bBuffLen)
     {
	      fnBufferUnicodeScroll((UINT16)pStringPtr[ucIndex++]);
     }

     return( fnfInfraScrollEntryRefresh(pFieldDest, bLen, bFieldCtrl, 
                                        bNumTableItems, pTableTextIDs, pAttributes));
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnfInfraScrollEntryRefresh
//
// DESCRIPTION:
//          Scrollable entry field refresh routine used for distributor setting.  This function is similar  
//          to fnfScrollableEntryRefresh. The difference is that this function can output the password
//          as "****"
//
// INPUTS:
//          Standard field function inputs.
//
// RETURNS:
//          always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//          Only left aligned fields are supported at this time.
//
// MODIFICATION HISTORY:
//		07/18/2006	  Raymond Shen	Modified it to fix password problem.
//      02/16/2006    Kan Jiang   Initial version
// *************************************************************************/
BOOL fnfInfraScrollEntryRefresh ( UINT16 *pFieldDest, 
                                  UINT8   bLen, 
                                  UINT8   bFieldCtrl, 
                                  UINT8   bNumTableItems, 
                                  UINT8  *pTableTextIDs, 
                                  UINT8  *pAttributes )
{
	BOOL	fStatus;

	if (bCurrDistrParam == DISTR_PARAM_ACCOUNT_PASSWORD)
	{
		fStatus = fnfScrollableHideEntryRefresh(pFieldDest, bLen, bFieldCtrl, 
												bNumTableItems, pTableTextIDs, pAttributes);
	}     
	else
	{
		 fStatus = fnfScrollableEntryRefresh(pFieldDest, bLen, bFieldCtrl, 
												bNumTableItems, pTableTextIDs, pAttributes);
	}
 
	return (fStatus);
}



/* *************************************************************************
// FUNCTION NAME:
//        fnfStdDistrParamPartShown
//
// DESCRIPTION:
//        Initializes the entry field where the distributor parameters is shown.
//        And part of the string that is longer than the field will not be shown.
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//       01/16/2006   Kan Jiang  Initial version
// *************************************************************************/
BOOL fnfStdDistrParamPartShown ( UINT16 *pFieldDest, 
                                 UINT8   bLen, 
                                 UINT8   bFieldCtrl, 
                                 UINT8   bNumTableItems, 
                                 UINT8  *pTableTextIDs, 
                                 UINT8  *pAttributes )
{
    UINT8   bBuffLen; 
    UINT8  *pStringPtr;
    UINT16  ucIndex;

    (void)fnfStdEntryInit(pFieldDest,bLen,bFieldCtrl,bNumTableItems,pTableTextIDs,pAttributes);

    //display the string which is input in Edit Option screen -- Kan
    pStringPtr = (UINT8 *)pDistParamTmp; 
 
     bBuffLen = (UINT8)strlen((char*)pStringPtr);
     if( bBuffLen > bLen)
     {
          bBuffLen = bLen;
     }
     
     if (bCurrDistrParam == DISTR_PARAM_ACCOUNT_PASSWORD)
     {
         // if Password,  output "****...."
         for (ucIndex = 0; ucIndex < bBuffLen; ucIndex++)
         {
             fnBufferUnicode((UINT16)'*');               
         }
     }     
     else
     {
         ucIndex = 0; 
	     while (ucIndex < bBuffLen)
	     {
		     fnBufferUnicode((UINT16)pStringPtr[ucIndex++]);
	     }
     }

     return( fnfStdEntryRefresh(pFieldDest,bLen,bFieldCtrl,bNumTableItems,
                                pTableTextIDs, pAttributes) );
}

/* *************************************************************************
// FUNCTION NAME: 
//          fnfDialingPhoneNumberInit
//
// DESCRIPTION:
//          Initializes the entry field where the Phone Number is typed.
//
// INPUTS:
//          Standard field function inputs.
//
// RETURNS:
//          always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//          None.
//
// MODIFICATION HISTORY:
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//      Joe Mozdzer /David Huang  Initial version  
// *************************************************************************/
BOOL fnfDialingPhoneNumberInit (  UINT16 *pFieldDest, 
                                  UINT8   bLen, 
                                  UINT8   bFieldCtrl, 
                                  UINT8   bNumTableItems, 
                                  UINT8  *pTableTextIDs, 
                                  UINT8  *pAttributes )
{
    UINT8         ucPBPNumLen  = 0;
    UINT8         ucIndex      = 0;
    SetupParams  *pSetupParams = NULL;

    /* Function always returns SUCCESSFUL, so ignore return value */
    (void)fnfStdEntryInit(pFieldDest,bLen,bFieldCtrl,bNumTableItems,pTableTextIDs,pAttributes);
       
    pSetupParams = fnCMOSSetupGetCMOSSetupParams();
    ucPBPNumLen = ( UINT8 )strlen(pSetupParams->PhoneNumber);

     if(ucPBPNumLen > bLen)
     {
          ucPBPNumLen = bLen;
     }
       
      ucIndex = 0; 
      while (ucIndex < ucPBPNumLen)
      {
           fnBufferUnicode( pSetupParams->PhoneNumber[ucIndex++] );
      }
         
    return( fnfStdEntryRefresh(pFieldDest,bLen,bFieldCtrl,bNumTableItems,
    	                       pTableTextIDs, pAttributes) );
}

/* *************************************************************************
// FUNCTION NAME: 
//          fnfBlindDialingSetting
//
// DESCRIPTION:
//          Initializes the entry which shows the Blind Dialing setting.
//
// INPUTS:
//          Standard field function inputs.
//
// RETURNS:
//          always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//          None.
//
// MODIFICATION HISTORY:
//      08/03/2005    Raymond Shen     Initial version
// *************************************************************************/
BOOL fnfBlindDialingSetting (  UINT16 *pFieldDest, 
                                  UINT8   bLen, 
                                  UINT8   bFieldCtrl, 
                                  UINT8   bNumTableItems, 
                                  UINT8  *pTableTextIDs, 
                                  UINT8  *pAttributes )
{
	UINT16 usTextID;
	
	SET_SPACE_UNICODE(pFieldDest, bLen);

	if ( fnCMOSSetupGetCMOSSetupParams()->blindDialing == TRUE) 
	{
		usTextID = 0;   //ON
	}
	else
	{
		usTextID = 1;   //OFF
	}

	fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
	                    pTableTextIDs, usTextID);

	return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//          fnfModemStringInit
//
// DESCRIPTION:
//          Initializes the entry field where the Modem string is output.
//
// INPUTS:
//          Standard field function inputs.
//
// RETURNS:
//          always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//          None.
//
// MODIFICATION HISTORY:
//      02/17/2006   Kan Jiang    Initial version
// *************************************************************************/
BOOL fnfModemStringInit ( UINT16 *pFieldDest, 
                          UINT8   bLen, 
                          UINT8   bFieldCtrl, 
                          UINT8   bNumTableItems, 
                          UINT8  *pTableTextIDs, 
                          UINT8  *pAttributes )
{
    UINT8 bStrLen = 0;
    UINT8 bIndex  = 0;

    (void)fnfStdEntryInit(pFieldDest,bLen,bFieldCtrl,bNumTableItems,pTableTextIDs,pAttributes);

    bStrLen = (UINT8)strlen(fnCMOSGetNetworkConfig()->socketmodemInit);

    if (bStrLen > bLen)
    {
        bStrLen = bLen;
    }
    
    fnAscii2Unicode(fnCMOSGetNetworkConfig()->socketmodemInit, pFieldDest, bStrLen);

    /* if the field is right-aligned, pad left of string with spaces */
    if (bFieldCtrl & ALIGN_MASK)
    {
		pFieldDest[bStrLen] = (UINT16) NULL;
		fnUnicodePadSpace(pFieldDest, bStrLen, bLen);
    }

    return((BOOL) SUCCESSFUL);
}




/* *************************************************************************
// FUNCTION NAME:
//        fnfOOBDialingPrefixInit
//
// DESCRIPTION:
//        Initializes the entry field where the dialing prefix is typed. 
//        If the dialing prefix is empty then output "9" as default.
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//                   Mozdzer     Initial version
//      11/30/2005   Adam Liu     Adapted for FuturePhoenix
//      02/16/2006   Kan Jiang     Modified
// *************************************************************************/
BOOL fnfOOBDialingPrefixInit ( UINT16 *pFieldDest, 
                               UINT8   bLen, 
                               UINT8   bFieldCtrl, 
                               UINT8   bNumTableItems, 
                               UINT8  *pTableTextIDs, 
                               UINT8  *pAttributes )
{
    UINT8         ucPrefixLen  = 0;
    UINT8         ucIndex      = 0;
    SetupParams  *pSetupParams = NULL;

    /* Function always returns SUCCESSFUL, so ignore return value */
    (void)fnfStdEntryInit(pFieldDest,bLen,bFieldCtrl,bNumTableItems,
                          pTableTextIDs,pAttributes);
       
    pSetupParams = fnCMOSSetupGetCMOSSetupParams();
    ucPrefixLen = ( UINT8 )strlen(pSetupParams->DialingPrefix);

     if(ucPrefixLen == 0)
    {
       fnBufferUnicode( (UINT16)'9' );  // input number "9" to entry buffer. 
    }     
    else 
    {
       if(ucPrefixLen > bLen)
       {
           ucPrefixLen = bLen;
       }
       
       ucIndex = 0; 
       while (ucIndex < ucPrefixLen)
       {
           fnBufferUnicode( pSetupParams->DialingPrefix[ucIndex++] );
       }
    }
         
    return( fnfStdEntryRefresh(pFieldDest,bLen,bFieldCtrl,bNumTableItems,
    	                       pTableTextIDs, pAttributes) );
}


/* *************************************************************************
// FUNCTION NAME:
//        fnfDialingPrefixInit
//
// DESCRIPTION:
//        Initializes the entry field where the dialing prefix is typed. 
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//        08/21/2006  Kan Jiang  Initial version
// *************************************************************************/
BOOL fnfDialingPrefixInit ( UINT16 *pFieldDest, 
                            UINT8   bLen, 
                            UINT8   bFieldCtrl, 
                            UINT8   bNumTableItems, 
                            UINT8  *pTableTextIDs, 
                            UINT8  *pAttributes )
{
    UINT8         ucPrefixLen  = 0;
    UINT8         ucIndex      = 0;
    SetupParams  *pSetupParams = NULL;

    /* Function always returns SUCCESSFUL, so ignore return value */
    (void)fnfStdEntryInit(pFieldDest,bLen,bFieldCtrl,bNumTableItems,
                          pTableTextIDs,pAttributes);
       
    pSetupParams = fnCMOSSetupGetCMOSSetupParams();
    ucPrefixLen = ( UINT8 )strlen(pSetupParams->DialingPrefix);
	
    if(ucPrefixLen != 0) 
    {
       if(ucPrefixLen > bLen)
       {
           ucPrefixLen = bLen;
       }
       
       ucIndex = 0; 
       while (ucIndex < ucPrefixLen)
       {
           fnBufferUnicode( pSetupParams->DialingPrefix[ucIndex++] );
       }
    }
         
    return( fnfStdEntryRefresh(pFieldDest,bLen,bFieldCtrl,bNumTableItems,
    	                       pTableTextIDs, pAttributes) );
}



/* *************************************************************************
// FUNCTION NAME:
//        fnfDialingPrefixOutput
//
// DESCRIPTION:
//        Initializes the entry field where the dialing prefix is typed. 
//        If the dialing prefix is empty, then output "None". 
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//      02/16/2006   Kan Jiang     Initial version
// *************************************************************************/
BOOL fnfDialingPrefixOutput ( UINT16 *pFieldDest, 
                              UINT8   bLen, 
                              UINT8   bFieldCtrl, 
                              UINT8   bNumTableItems, 
                              UINT8  *pTableTextIDs, 
                              UINT8  *pAttributes )
{
    UINT8         ucPrefixLen  = 0;
    UINT8         ucIndex      = 0;
    SetupParams  *pSetupParams = NULL;

    SET_SPACE_UNICODE(pFieldDest, bLen);
       
    pSetupParams = fnCMOSSetupGetCMOSSetupParams();
    ucPrefixLen  = (UINT8)strlen(pSetupParams->DialingPrefix);

    if(ucPrefixLen == 0)
    {
        fnDisplayTableText(pFieldDest,bLen,bFieldCtrl,bNumTableItems,pTableTextIDs,ucPrefixLen); 
    }     
    else 
    {
	    if (ucPrefixLen > bLen)
	    {
		   ucPrefixLen = bLen;
	    }

		fnAscii2Unicode(pSetupParams->DialingPrefix, pFieldDest, ucPrefixLen);

		/* if the field is right-aligned, pad left of string with spaces */
		if (bFieldCtrl & ALIGN_MASK)
		{
			pFieldDest[ucPrefixLen] = (UINT16) NULL;
			fnUnicodePadSpace(pFieldDest, ucPrefixLen, bLen);
		}
    }
         
    return( SUCCESSFUL );
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnfScrollableEntryModemStr
//
// DESCRIPTION:
//          Initializes the entry field as scroll field and display array pDistParamTmp which 
//          store the input  modem string.
//
// INPUTS:
//          Standard field function inputs.
//
// RETURNS:
//          always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//          None
//
// MODIFICATION HISTORY:
//      02/17/2006    Kan Jiang    Modified
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                    David Huang  Initial version  
// *************************************************************************/
BOOL fnfScrollableEntryModemStr ( UINT16 *pFieldDest, 
                                  UINT8   bLen, 
                                  UINT8   bFieldCtrl, 
                                  UINT8   bNumTableItems, 
                                  UINT8  *pTableTextIDs, 
                                  UINT8  *pAttributes )
{
    UINT16 bPrefixLen = 0;
    UINT16 bIndex = 0;

    (void)fnfScrollableEntryInit(pFieldDest, bLen, bFieldCtrl,
                                 bNumTableItems,pTableTextIDs, pAttributes);

    /* initialize the buffer to spaces */
	SET_SPACE_UNICODE(rGenPurposeEbuf.pBuf, 80);

    bPrefixLen = strlen(pDistParamTmp);

    bIndex = 0; 
    while (bIndex < bPrefixLen)
    {
        //lint -e571 this cast would not cause error
        fnBufferUnicodeScroll((UINT16)pDistParamTmp[bIndex++]);
        //lint +e571
    }

    return(fnfScrollableEntryRefresh(pFieldDest, bLen, bFieldCtrl, 
                                     bNumTableItems, pTableTextIDs, pAttributes));
}



/* *************************************************************************
// FUNCTION NAME:
//        fnfSMenuDistrParamShown1~4
//
// DESCRIPTION:
//        field function that output the distributor parameters in the corresponding field
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//   02/15/2006   Kan Jiang     Initial version
//   05/22/2006   James Wang    Change it to call a more common function.
//
// *************************************************************************/
BOOL fnfSMenuDistrParamShown1 ( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{       
    return fnSMenuDistrParamShown( pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                   pTableTextIDs, pAttributes, 0);
}



BOOL fnfSMenuDistrParamShown2 ( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    return fnSMenuDistrParamShown( pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                   pTableTextIDs, pAttributes, 1);
}



BOOL fnfSMenuDistrParamShown3 ( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    return fnSMenuDistrParamShown( pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                   pTableTextIDs, pAttributes, 2);
}



BOOL fnfSMenuDistrParamShown4 ( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    return fnSMenuDistrParamShown( pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                   pTableTextIDs, pAttributes, 3);
}

/* *************************************************************************
// FUNCTION NAME:
//        fnfDcapNextUploadPlanned
//
// DESCRIPTION:
//        field function that output the next upload title
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//   12/14/2006   Adam Liu     Initial version
//
// *************************************************************************/
BOOL fnfDcapNextUploadPlanned ( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
	DATETIME	rDCAPEndDate;


	//TODO JAH remove dcperiod.c rDCAPEndDate = fnDCAP_GetNext_Upload_Datetime();

	rDCAPEndDate.bDayOfWeek = 1; // fake the fnValidDate call since we dont get it from above call

	if (fnValidDate(&rDCAPEndDate))		// Next upload planned
		fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, 0);
	else					// <empty>
		SET_SPACE_UNICODE(pFieldDest, bLen);

	return ((BOOL)SUCCESSFUL);
}


/*---------------------------------------------*/
void fnFormatDcapDate( UINT16 *pFieldDest, UINT8 bLen, UINT8 bFieldCtrl, UINT8 bNumTableItems, 
                       UINT8 *pTableTextIDs, DATETIME *pDcapDateTime )
{
    DATETIME    stDateTime;
    UINT16      usDDS;
    UINT16      usDateSepChar;
    UINT8       bDateFormat;
	BOOL		fRetval = FAILURE;


	pDcapDateTime->bDayOfWeek = 1; // fake for the fnValidDate call since it might not have been filled in

	if (fnValidDate(pDcapDateTime))
	{
        //Get the date parameters and format the date
        if (fnGetDateParms(&stDateTime, &bDateFormat, &usDDS, &usDateSepChar) == SUCCESSFUL)
        {
               // do not duck date
               fRetval = fnBuildDateFmt(pDcapDateTime, bDateFormat, usDDS, usDateSepChar, TRUE, 
                                     TRUE, FALSE, TRUE, pFieldDest, bLen, bNumTableItems, 
                                     pTableTextIDs, bFieldCtrl);
        }
	}

	if (fRetval == FAILURE)
	{
		/* make the field blank if we have failed */
		SET_SPACE_UNICODE(pFieldDest, bLen);
	}
}


/*---------------------------------------------*/
void fnFormatDcapTime( UINT16 *pFieldDest, UINT8 bLen, UINT8 bFieldCtrl, UINT8 bNumTableItems, 
                       UINT8 *pTableTextIDs, DATETIME *pDcapDateTime )
{
	int				iStrLen = 0;
	unsigned long	i = 0;
	unsigned short	wTextID = 0;
	char			pStr[8];


	pDcapDateTime->bDayOfWeek = 1; // fake the fnValidDate call since we dont get it from above call

    if (fnValidDate(pDcapDateTime))
    {
	    /* Load the hour */
    	if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
	    {
	    	/* adjust for a 12 hour clock format, if applicable */
		    if (pDcapDateTime->bHour >= 13)
			    pDcapDateTime->bHour -= 12;
    		else if (pDcapDateTime->bHour == 0)
	    		pDcapDateTime->bHour = 12;
				
	        iStrLen = sprintf(pStr, "%d", pDcapDateTime->bHour);
    	}
    	else
    		iStrLen = sprintf(pStr, "%02d", pDcapDateTime->bHour);

    	if ((unsigned long)iStrLen <= bLen)
    	{
    		fnAscii2Unicode(pStr, pFieldDest, (unsigned short)iStrLen);
	    	i += (unsigned long)iStrLen;
    	}

    	/* if there is one, add the separation string that goes between the hours and minutes */
    	if (bNumTableItems > 0)
    	{
    		EndianAwareCopy(&wTextID, pTableTextIDs, 2);
    		i += fnCopyTableText(pFieldDest + i, wTextID, bLen - (unsigned char)i);	
    	}
	
    	/* if it will fit in the field, add the minutes */
    	iStrLen = sprintf(pStr, "%02d", pDcapDateTime->bMinutes);
    	if ((i + (unsigned long)iStrLen) <= bLen)
	    {
    		fnAscii2Unicode(pStr, pFieldDest + i, (unsigned short)iStrLen);
	    	i += (unsigned long)iStrLen;
    	}

    	/* if there is one, add the string that goes after the minutes */
    	if (bNumTableItems > 1)
    	{
    		EndianAwareCopy(&wTextID, pTableTextIDs + (1 * sizeof(unsigned short)), 2);
    		i += fnCopyTableText(pFieldDest + i, wTextID, bLen - (unsigned char)i);
    	}

    	/* if field is right-aligned, pad left of string with spaces */
    	if (bFieldCtrl & ALIGN_MASK)
    	{
    		pFieldDest[i] = 0;
    		fnUnicodePadSpace(pFieldDest, (unsigned short)i, bLen);
    	}
    }
}


/*---------------------------------------------*/
void fnFormatDcapAMPM( UINT16 *pFieldDest, UINT8 bLen, UINT8 bFieldCtrl, UINT8 bNumTableItems, 
                       UINT8 *pTableTextIDs, DATETIME *pDcapDateTime )
{
	unsigned short	wTextID = 0;
	
	
	pDcapDateTime->bDayOfWeek = 1; // fake the fnValidDate call since we dont get it from above call

    if (fnValidDate(pDcapDateTime))
    {
    	if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
	    {
    		/* check to make sure there are at least two table items, which are required for this field */
	    	if (bNumTableItems >= 2)
		    {
			    if (pDcapDateTime->bHour <= 11)
				    /* "am" is the first string */
			    	EndianAwareCopy(&wTextID, pTableTextIDs, 2);
	    		else
		    		/* "pm" is the second one */
	    			EndianAwareCopy(&wTextID, pTableTextIDs + sizeof(unsigned short), 2);

    			(void)fnCopyTableText(pFieldDest, wTextID, bLen);
	    	}
	    }
    }
}


/* *************************************************************************
// FUNCTION NAME:
//        fnfDcapNextUploadDate
//
// DESCRIPTION:
//        field function that output the next upload date
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//   12/14/2006   Adam Liu     Initial version
//
// *************************************************************************/
BOOL fnfDcapNextUploadDate    ( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
	DATETIME	rDCAPEndDate;

  
	/* initialize the buffer to spaces */
	SET_SPACE_UNICODE(pFieldDest, bLen);

	//TODO JAH remove dcperiod.c rDCAPEndDate = fnDCAP_GetNext_Upload_Datetime();
	fnFormatDcapDate( pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, &rDCAPEndDate );

	return ((BOOL)SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: fnfDatCapNextCallTime
// DESCRIPTION: Output field function for the Data Capture
//              Next Call Time.
// AUTHOR: Sandra Peterson
//
// INPUTS:  Standard field function inputs.
// TEXT TABLE USAGE:  none
// *************************************************************************/
BOOL fnfDatCapNextCallTime (unsigned short *pFieldDest, unsigned char bLen, unsigned char bFieldCtrl, 
                            unsigned char bNumTableItems, unsigned char *pTableTextIDs, unsigned char *pAttributes)
{
    DATETIME        rDCAPUploadDateTime;
    

	/* initialize the buffer to spaces */
	SET_SPACE_UNICODE(pFieldDest, bLen);

#if 0 //TODO JAH Remove dcapi.c 
	if (fnDCAPShowUploadTime() == TRUE)
	{
	    //TODO JAH remove dcperiod.c rDCAPUploadDateTime = fnDCAP_GetNext_Upload_Datetime();
		fnFormatDcapTime( pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, &rDCAPUploadDateTime );
	}
#endif //TODO JAH Remove dcapi.c 

    return ((BOOL) SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: fnfDatCapNextCallAMPM
// DESCRIPTION: Output field function for the Data Capture
//              Next Call AMPM.
// AUTHOR: Sandra Peterson
//
// INPUTS:  Standard field function inputs.
// TEXT TABLE USAGE:  none
// *************************************************************************/
BOOL fnfDatCapNextCallAMPM (unsigned short *pFieldDest, unsigned char bLen, unsigned char bFieldCtrl, 
                            unsigned char bNumTableItems, unsigned char *pTableTextIDs, unsigned char *pAttributes)
{
    DATETIME        rDCAPUploadDateTime;


	/* initialize the buffer to spaces */
	SET_SPACE_UNICODE(pFieldDest, bLen);

#if 0 //TODO JAH Remove dcapi.c 
	if (fnDCAPShowUploadTime() == TRUE)
	{
	    //TODO JAH remove dcperiod.c rDCAPUploadDateTime = fnDCAP_GetNext_Upload_Datetime();
		fnFormatDcapAMPM( pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, &rDCAPUploadDateTime );
    }
#endif //TODO JAH Remove dcapi.c 
    return ((BOOL) SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME:
//        fnfDCAPUploadReqDate
//
// DESCRIPTION:
//        field function that output the required upload date
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//   12/14/2006   Adam Liu     Initial version
//
// *************************************************************************/
BOOL fnfDCAPUploadReqDate     ( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
	DATETIME	rDCAPEndDate;


	/* initialize the buffer to spaces */
	SET_SPACE_UNICODE(pFieldDest, bLen);

#if 0 //TODO JAH Remove dcapi.c 
	if (fnDCAPGetUploadRequiredDate( &rDCAPEndDate ) == TRUE)
	{
		fnFormatDcapDate( pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, &rDCAPEndDate );
	}
#endif//TODO JAH Remove dcapi.c 
    return ((BOOL)SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: fnfDatCapReqTime
// DESCRIPTION: Output field function for the Data Capture
//              Upload Required Time.
// AUTHOR: Sandra Peterson
//
// INPUTS:  Standard field function inputs.
// TEXT TABLE USAGE:  none
// *************************************************************************/
BOOL fnfDatCapReqTime (unsigned short *pFieldDest, unsigned char bLen, unsigned char bFieldCtrl, 
                       unsigned char bNumTableItems, unsigned char *pTableTextIDs, unsigned char *pAttributes)
{
    DATETIME        rDCAPUploadDateTime;
    

	/* initialize the buffer to spaces */
	SET_SPACE_UNICODE(pFieldDest, bLen);

#if 0 //TODO JAH Remove dcapi.c 
	if ((fnDCAPShowRequiredTime() == TRUE) && (fnDCAPGetUploadRequiredDate( &rDCAPUploadDateTime ) == TRUE))
	{
		fnFormatDcapTime( pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, &rDCAPUploadDateTime );
	}
#endif //TODO JAH Remove dcapi.c 
    return ((BOOL) SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: fnfDatCapReqAMPM
// DESCRIPTION: Output field function for the Data Capture
//              Upload Required AMPM.
// AUTHOR: Sandra Peterson
//
// INPUTS:  Standard field function inputs.
// TEXT TABLE USAGE:  none
// *************************************************************************/
BOOL fnfDatCapReqAMPM (unsigned short *pFieldDest, unsigned char bLen, unsigned char bFieldCtrl, 
                       unsigned char bNumTableItems, unsigned char *pTableTextIDs, unsigned char *pAttributes)
{
    DATETIME        rDCAPUploadDateTime;


	/* initialize the buffer to spaces */
	SET_SPACE_UNICODE(pFieldDest, bLen);

#if 0 //TODO JAH Remove dcapi.c 
	if ((fnDCAPShowRequiredTime() == TRUE) && (fnDCAPGetUploadRequiredDate( &rDCAPUploadDateTime ) == TRUE))
	{
		fnFormatDcapAMPM( pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, &rDCAPUploadDateTime );
    }
#endif //TODO JAH Remove dcapi.c 
    return ((BOOL) SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME:
//        fnfCurrentLocationCode
//
// DESCRIPTION:
//        field function that gets the current location code
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//   29 Mar 2007    Bill Herring    Initial
//    3 Aug 2007    Martin O'Brien  FRACA 126765 - extra character displayed
//                                  on Meter Move screen
//
// *************************************************************************/
BOOL fnfCurrentLocationCode   ( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
	char opc[OPC_MAX+1];
	unsigned char bStrLen;
	
	SET_SPACE_UNICODE(pFieldDest, bLen);

	(void)memset((void*)opc,0,sizeof(opc));
	if (fnValidData( BOBAMAT0, (unsigned short) VLT_IDENT_FILE_ZIPCODE, (void *) opc ) == BOB_OK)
	{
        /* -1 because REGATE is 6 characters and LENZIP is 7 */
		bStrLen = LENZIP - 1;
						
		fnAscii2Unicode(opc, pFieldDest, bStrLen);

		/* if the field is right-aligned, pad left of string with spaces */
		if (bFieldCtrl & ALIGN_MASK)
		{
			pFieldDest[bStrLen] = '\0';
			fnUnicodePadSpace(pFieldDest, bStrLen, bLen);
		}
	}
	
	return SUCCESSFUL;
}

/* *************************************************************************
// FUNCTION NAME:
//      fnfUploadCompleteStringSelect1
//
// DESCRIPTION:
//      Field function that selects the correct field function to
//      display the dowloaded sw string prompts
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//  07/24/2008  Raymond Shen    Adapted for FPHX
//              Fox Zhang       Initial version
// *************************************************************************/
BOOL fnfUploadCompleteStringSelect1( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    UINT8   ucIndex = 0;
    BOOL    fDisplay = FALSE;
    
    SET_SPACE_UNICODE(pFieldDest, bLen);

    if(fStrDCapUploaded == OIT_INFRA_UPLOAD_SUCCESS)
    {
        ucIndex = 0;
        fDisplay = TRUE;
    }
    else
    {
        if(fStrEServiceUploaded == OIT_INFRA_UPLOAD_SUCCESS)
        {
            ucIndex = 1;
            fDisplay = TRUE;
        }

        else if(fStrAcctUploaded == OIT_INFRA_UPLOAD_SUCCESS)
        {
            ucIndex = 2;
            fDisplay = TRUE;
        }
    }

    if(fDisplay == TRUE)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, ucIndex);
    }
    
    return(SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnfUploadCompleteStringSelect2
//
// DESCRIPTION:
//      Field function that selects the correct field function to
//      display the dowloaded sw string prompts
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//  07/24/2008  Raymond Shen    Adapted for FPHX
//              Fox Zhang       Initial version
// *************************************************************************/
BOOL fnfUploadCompleteStringSelect2( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    UINT8   ucIndex = 0;
    BOOL    fDisplay = FALSE;
    
    SET_SPACE_UNICODE(pFieldDest, bLen);

    if(fStrDCapUploaded == OIT_INFRA_UPLOAD_SUCCESS)
    {
        if(fStrEServiceUploaded == OIT_INFRA_UPLOAD_SUCCESS)
        {
            ucIndex = 0;
            fDisplay = TRUE;
        }
        else if(fStrAcctUploaded == OIT_INFRA_UPLOAD_SUCCESS)
        {
            ucIndex = 1;
            fDisplay = TRUE;
        }
    }
    else
    {
        if(fStrEServiceUploaded == OIT_INFRA_UPLOAD_SUCCESS)
        {
            if(fStrAcctUploaded == OIT_INFRA_UPLOAD_SUCCESS)
            {
                ucIndex = 1;
                fDisplay = TRUE;
            }
        }
    }

    if(fDisplay == TRUE)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, ucIndex);
    }

    return(SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnfUploadCompleteStringSelect3
//
// DESCRIPTION:
//      Field function that selects the correct field function to
//      display the dowloaded sw string prompts
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//  07/24/2008  Raymond Shen    Adapted for FPHX
//              Fox Zhang       Initial version
// *************************************************************************/
BOOL fnfUploadCompleteStringSelect3( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{   
    SET_SPACE_UNICODE(pFieldDest, bLen);

    if( (fStrDCapUploaded == OIT_INFRA_UPLOAD_SUCCESS)
       && (fStrEServiceUploaded == OIT_INFRA_UPLOAD_SUCCESS)
       && (fStrAcctUploaded == OIT_INFRA_UPLOAD_SUCCESS) )
    {     
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, 0);
    }
    
     return(SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnfInfDataUploadErrString
//
// DESCRIPTION:
//      Field function that display data upload error string.
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//  07/24/2008  Raymond Shen    Adapted for FPHX
//              Fox Zhang       Initial version
// *************************************************************************/
BOOL fnfInfDataUploadErrString( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    UINT8 bWhichString = 0xFF;    

     /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    if( fStrAcctUploaded == OIT_INFRA_UPLOAD_FAILED ) 
    {
        bWhichString = 0;
    }
    else if( fStrAcctUploaded == OIT_INFRA_UPLOAD_FAILED_MODE_1) 
    {
        bWhichString = 1;
    }
    
    else if( fStrAcctUploaded == OIT_INFRA_UPLOAD_FAILED_MODE_2) 
    {
        bWhichString = 2;
    }
    
    if (bWhichString < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, bWhichString);      
    }
    
    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnfInfDataUploadErrCode
//
// DESCRIPTION:
//      Field function that display data upload error code.
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//  07/24/2008  Raymond Shen    Adapted for FPHX
//              Fox Zhang       Initial version
// *************************************************************************/
BOOL fnfInfDataUploadErrCode( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    char pErrorNumAscii[9];

     /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    (void)sprintf(pErrorNumAscii, "%.8X", lDataUploadErrCode);
    pErrorNumAscii[8] = 0;

    fnAscii2Unicode(pErrorNumAscii, pFieldDest, 8);
    pFieldDest[8] = 0;

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnfDataUploadNum1~4
//
// DESCRIPTION:
//      Field function that displays data upload record's number in 
//      AbacusDataUploadsSummary screen.
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//  07/25/2008  Raymond Shen    Initial version
//
// *************************************************************************/
BOOL fnfDataUploadNum1( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();

    return(fnfDataUploadLogNumCommon (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex));
}

BOOL fnfDataUploadNum2( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();

    return(fnfDataUploadLogNumCommon (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex + 1));
}

BOOL fnfDataUploadNum3( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();

    return(fnfDataUploadLogNumCommon (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex + 2));
}

BOOL fnfDataUploadNum4( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();

    return(fnfDataUploadLogNumCommon (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex + 3));
}

/* *************************************************************************
// FUNCTION NAME:
//      fnfDataUploadLogDate1~4
//
// DESCRIPTION:
//      Field function that displays data upload record's date in 
//      AbacusDataUploadsSummary screen.
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//  07/25/2008  Raymond Shen    Initial version
//
// *************************************************************************/
BOOL fnfDataUploadLogDate1( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();
    
    return(fnfDataUploadLogDate (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex));
}

BOOL fnfDataUploadLogDate2( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();
    
    return(fnfDataUploadLogDate (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex + 1));
}

BOOL fnfDataUploadLogDate3( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();
    
    return(fnfDataUploadLogDate (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex + 2));
}

BOOL fnfDataUploadLogDate4( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();
    
    return(fnfDataUploadLogDate (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex + 3));
}

/* *************************************************************************
// FUNCTION NAME:
//      fnfDataUploadLogTime1~4
//
// DESCRIPTION:
//      Field function that displays data upload record's time in 
//      AbacusDataUploadsSummary screen.
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//  07/25/2008  Raymond Shen    Initial version
//
// *************************************************************************/
BOOL fnfDataUploadLogTime1( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();
    
    return(fnfDataUploadLogTime (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex));
}

BOOL fnfDataUploadLogTime2( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();
    
    return(fnfDataUploadLogTime (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex + 1));
}

BOOL fnfDataUploadLogTime3( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();
    
    return(fnfDataUploadLogTime (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex + 2));
}

BOOL fnfDataUploadLogTime4( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();
    
    return(fnfDataUploadLogTime (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex + 3));
}

/* *************************************************************************
// FUNCTION NAME:
//      fnfDataUploadLogAMPM1~4
//
// DESCRIPTION:
//      Field function that displays data upload record's AM/PM status in 
//      AbacusDataUploadsSummary screen.
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//  07/25/2008  Raymond Shen    Initial version
//
// *************************************************************************/
BOOL fnfDataUploadLogAMPM1( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();
    
    return(fnfDataUploadLogAMPM (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex));
}

BOOL fnfDataUploadLogAMPM2( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();
    
    return(fnfDataUploadLogAMPM (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex + 1));
}

BOOL fnfDataUploadLogAMPM3( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();
    
    return(fnfDataUploadLogAMPM (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex + 2));
}

BOOL fnfDataUploadLogAMPM4( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();
    
    return(fnfDataUploadLogAMPM (pFieldDest,bLen, bFieldCtrl,bNumTableItems,
                        pTableTextIDs, pAttributes, pMenuTable->usFirstDisplayedSlotIndex + 3));
}

/* *************************************************************************
// FUNCTION NAME:
//      fnfDataUploadCurDetailDate
//
// DESCRIPTION:
//      Field function that displays data upload record's detail date in 
//      AbacusDataUploadsDetails screen.
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//  07/25/2008  Raymond Shen    Adapted for FPHX
//              Zhang Yingbo    Initial version
//
// *************************************************************************/
BOOL fnfDataUploadCurDetailDate( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    UINT8   bUploadType = CMOSWebVisDataUploadLog[usDataUploadCurLogIndex].bUploadType;  
    DATETIME    dtDate;
    UINT8          bDateFormat;
    UINT16        usDDS;
    UINT16        usDateSepChar;
 
    /* Check if this is a valid entry */
    if( bUploadType != 0 )
    {
        (void)fnGetDateParms(&dtDate, &bDateFormat, &usDDS, &usDateSepChar);
        bDateFormat = DATE_FORMAT_MMDDYY;
        usDateSepChar = usDataUploadDateSepChar;
        
        (void)fnBuildDateFmt(&(CMOSWebVisDataUploadLog[usDataUploadCurLogIndex].rDateTime), 
                                                    bDateFormat, usDDS, usDateSepChar, TRUE, TRUE, FALSE, TRUE,  
                                                    pFieldDest, bLen, bNumTableItems, pTableTextIDs, bFieldCtrl);
    }   

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnfDataUploadCurDetailTime
//
// DESCRIPTION:
//      Field function that displays data upload record's detail time in 
//      AbacusDataUploadsDetails screen.
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//  07/25/2008  Raymond Shen    Adapted for FPHX
//              Zhang Yingbo    Initial version
//
// *************************************************************************/
BOOL fnfDataUploadCurDetailTime( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    UINT8   bUploadType = CMOSWebVisDataUploadLog[usDataUploadCurLogIndex].bUploadType;  

     /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* Check if this is a valid entry */
    if( bUploadType != 0 )
    {
        (void)fnBuildTime(CMOSWebVisDataUploadLog[usDataUploadCurLogIndex].rDateTime, FALSE, TRUE, pFieldDest, bLen, bNumTableItems, pTableTextIDs);
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnfDataUploadCurDetailAMPM
//
// DESCRIPTION:
//      Field function that displays data upload record's detail AM/PM status 
//      in AbacusDataUploadsDetails screen.
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//        None
//
// MODIFICATION HISTORY:
//  07/25/2008  Raymond Shen    Adapted for FPHX
//              Zhang Yingbo    Initial version
//
// *************************************************************************/
BOOL fnfDataUploadCurDetailAMPM( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    UINT8   bUploadType = CMOSWebVisDataUploadLog[usDataUploadCurLogIndex].bUploadType;  

     /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* Check if this is a valid entry */
    if( bUploadType != 0 )
    {
        (void)fnShowAmPm(CMOSWebVisDataUploadLog[usDataUploadCurLogIndex].rDateTime.bHour, pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs);
    }

    return ((BOOL) SUCCESSFUL);
}


/* Hard key functions */

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkExitPBPBalance
//
// DESCRIPTION: 
// 		This gets called when the clear key is pressed in PBP Balance screen,
// 		thus to send disconnect message and let modem give up uart port.
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Next scree: always goes to next screen 1
//
// MODIFICATION HISTORY:
//  07/28/2006  Dicky Sun           Update for uploading record workflow
//	1/24/2006	Vincent Yi			Code cleanup
// 				Victor Li			Initial version
// *************************************************************************/
T_SCREEN_ID fnhkExitPBPBalance (UINT8 			bKeyCode, 
							    UINT8			bNumNext, 
							    T_SCREEN_ID 	*	pNextScreens)
{
	T_SCREEN_ID 	usNext = 0;

	usNext = pNextScreens[0]; // stay on processing screen

	// if in OOB, we've already done the Tier3 part, set a flag so we know we don't
	// have to do the Tier3 part again if the download fails.
	if (fnIsInOOBMode() == TRUE)
	{
		CMOSDiagnostics.fOOBIgnoreContactPB = TRUE;
	}
	
/*Vincent
	if ( fnIsInOOBMode() )
	{
		bOITCurrentService = EXIT_FROM_OOB_PBP ;
	}
*/

	return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: 
//		fnhkGetUpdateNow
//
// DESCRIPTION: 
// 		Hard key function to goto InfraContactPB to do update
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Next scree: always goes to next screen 1
//
// MODIFICATION HISTORY:
// 				David Huang			Initial version
//	03/28/2006	Vincent Yi			Code cleanup
//
// *************************************************************************/
T_SCREEN_ID fnhkGetUpdateNow (UINT8 			bServiceCode,
							  INT32				value,
							  T_SCREEN_ID 	*	pNextScreens)
{
	(void) fnGetUpdateNow(bServiceCode, value, 0);

	return pNextScreens[0];
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkDownloadContactNow
//
// DESCRIPTION: 
// 		Hard key function to request download service
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Next scree: always goes to next screen 1
//
// MODIFICATION HISTORY:
// 08/01/2008   Raymond Shen    Replaced SWDOWNLOAD_SERVICE with ET_CALL_HOME to  
//                              avoid doing Budget account upload while downloading.
// 04/11/2006	Vincent Yi			Code cleanup
// 									Initial version
// *************************************************************************/
T_SCREEN_ID fnhkDownloadContactNow (UINT8 			bKeyCode, 
									UINT8			bNumNext, 
									T_SCREEN_ID *	pNextScreens)
{
	T_SCREEN_ID 	usNext = 0;
	
	ucOITRequestedService = ET_CALL_HOME;	
	
	usNext = pNextScreens[0];

	return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkToggleConnectionMode
//
// DESCRIPTION: 
// 		Hard key function that toggles the connection mode selection
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Next scree: always goes to next screen 1
//
// MODIFICATION HISTORY:
// 									
//	09/26/2007	Andy Mo			Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkToggleConnectionMode (UINT8 			bKeyCode, 
    									UINT8			bNumNext, 
    									T_SCREEN_ID *	pNextScreens)
{
	T_SCREEN_ID 	usNext = pNextScreens[0];
	
    if(bCMOSConnectionMode==CONNECTION_MODE_AUTO_DETECT)
	   bCMOSConnectionMode = CONNECTION_MODE_PHONE_LINE;
	else if(bCMOSConnectionMode==CONNECTION_MODE_PHONE_LINE)
	   bCMOSConnectionMode = CONNECTION_MODE_AUTO_DETECT;
	return (usNext);
}
/* *************************************************************************
// FUNCTION NAME: 
//		fnhkConnectLater
//
// DESCRIPTION: 
// 		This function is called when user skips to get the update, send a reply 
//		message with don't do it parameter.
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Next scree: always goes to next screen 1
//
// MODIFICATION HISTORY:
// 			David Huang, Victor Li	Initial version
//	04/12/2006	Vincent Yi			Code cleanup
//
// *************************************************************************/
T_SCREEN_ID fnhkConnectLater (UINT8 		bKeyCode, 
							  UINT8			bNumNext, 
							  T_SCREEN_ID *	pNextScreens)
{
	T_SCREEN_ID  usNext = pNextScreens[0]; // InfraContactPB screen
	UINT8   	 bDoService = DONT_DO_IT;

	/* send a reply message with dont do it flag */
	(void)OSSendIntertask( DISTTASK, OIT, (UINT8)OIT_START_SERVICE_REPLY, 
							BYTE_DATA, &bDoService, 1 );

	return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkDLAContinue
//
// DESCRIPTION: 
// 		Hard key function that sends a continue message.
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Next scree: always goes to next screen 1
//
// MODIFICATION HISTORY:
// 				Victor Li		Initial version
//	04/12/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
T_SCREEN_ID fnhkDLAContinue (UINT8 			bKeyCode, 
							 UINT8			bNumNext, 
							 T_SCREEN_ID *	pNextScreens)
{
	OSSendIntertask( DISTTASK, OIT, (UINT8)DLA_CONTINUE, NO_DATA, NULL, 0 );

	return (pNextScreens[0]);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkDownloadFinished
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Depends on the value of fOITDownloadDoneReboot
//
// MODIFICATION HISTORY:
// 				Victor Li		Initial version
//	04/12/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
T_SCREEN_ID fnhkDownloadFinished (UINT8 		bKeyCode, 
								  UINT8			bNumNext, 
								  T_SCREEN_ID *	pNextScreens)
{
	T_SCREEN_ID  	usNext = 0;

	if (fOITDownloadDoneReboot == TRUE)
	{
		RebootSystem();
	}
	else
	{
		usNext = pNextScreens[0]; // stay on processing screen

		/* send a reply message */
		OSSendIntertask( DISTTASK, OIT, (UINT8)OIT_SERVICE_COMPLETE_REPLY, 
							NO_DATA, NULL, 0 );
	}

	return (usNext);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnhkToggleTonePulseSetting
//
// DESCRIPTION: 
//      Hard key functions that toggle the Tone/Pulse setting for dialing mode.
//
// INPUTS:
//      Standard hard key function inputs.
//
// OUTPUTS:
//      Always go to next screen 1.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  Tim Zhang       Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkToggleTonePulseSetting(UINT8   bKeyCode, 
                                       UINT8   bNumNext, 
                                       T_SCREEN_ID *pNextScreens)
{
	UINT16 usNext = pNextScreens[0];
	
	bOITModemDialMode = !bOITModemDialMode;
	ChangeModemDialPrefix();
	
	return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: fnhkDistrValidateParam
//
// DESCRIPTION: 
//       This function validates the characters entered by the user on the 
//       Distributor Network Configuration Parameter entry screen.
//
// INPUTS:
//       Standard hard key function inputs.
//
// OUTPUTS:
//       Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  09/23/2014  Renhao       Modified code to fix fraca 227302 & 102652.
//  11/07/2012  Bob Li       Modified to fix fraca 102652.
//  04/27/2010  Jingwei,Li   Modified for support RTL writting.
//  07/05/2006  Kan Jiang    Calling fnSetNetworkDefaults() for fixing Fraca 100003
//  02/15/2006  Kan Jiang    Initial version
// *************************************************************************/
T_SCREEN_ID fnhkDistrValidateParam(UINT8        bKeyCode, 
                                   UINT8        bNumNext,
                                   T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;
    UINT8       bBuffLen = 0;
    UINT8       bMaxLen = 0;
    UINT8       bStatus = DISTR_PARAM_STATUS_ERR;
    UINT8   ubWritingDir;
    UINT8   i = 0;
    UINT8   dnsIp[4];


    ubWritingDir = fnGetWritingDir();
    if(ubWritingDir == RIGHT_TO_LEFT )
    {
        while(rGenPurposeEbuf.pBuf[i]== UNICODE_SPACE)
        {
            i++;
        }
    }

    bStatus = fnIsDistrParamOK();
    if (bStatus & DISTR_PARAM_STATUS_OK)
    {
        bBuffLen = fnCharsBuffered();
        bMaxLen = oiDistrParamInfo[bCurrDistrParam].bMaxLen;

        if (bBuffLen > bMaxLen)
        {
            bBuffLen = bMaxLen;
        }
      
        /* convert unicode entry buff to ascii, but it should be done when the entry is valid */
        //(void)fnUnicode2Ascii(&rGenPurposeEbuf.pBuf[i],oiDistrParamInfo[bCurrDistrParam].pStr,bBuffLen);

        if ( bCurrDistrParam == DISTR_PARAM_DISTR_URL )
        {
            /* initialize the distributor string */
            memset(oiDistrParamInfo[bCurrDistrParam].pStr, 0, oiDistrParamInfo[bCurrDistrParam].bMaxLen);

            if (memcmp(&rGenPurposeEbuf.pBuf[i], pQAKeyword, sizeof(pQAKeyword)) == 0)
			{
                fnInitNetConfigForTest(NET_INIT_QA);
            }
            else if(memcmp(&rGenPurposeEbuf.pBuf[i], pSecapKeyword, sizeof(pSecapKeyword)) == 0)
			{
                fnInitNetConfigForTest(NET_INIT_SECAP);
            }
            else if (memcmp(&rGenPurposeEbuf.pBuf[i], pEMDKeyword, sizeof(pEMDKeyword)) == 0)
			{
                fnInitNetConfigForTest(NET_INIT_EMD);
            }
            else
            {
                /* convert unicode entry buff to ascii */
                (void)fnUnicode2Ascii(&rGenPurposeEbuf.pBuf[i], oiDistrParamInfo[bCurrDistrParam].pStr,
                                                      bBuffLen);
               
                if (bBuffLen < bMaxLen)
                {
                    oiDistrParamInfo[bCurrDistrParam].pStr[bBuffLen] = 0;
                }
            }
        }
		
        else if ( bCurrDistrParam == DISTR_PARAM_PBP_BACKUP_URL )
        {
            memset(oiDistrParamInfo[bCurrDistrParam].pStr, 0, oiDistrParamInfo[bCurrDistrParam].bMaxLen);
            if (memcmp(&rGenPurposeEbuf.pBuf[i], pBackupKeyword, sizeof(pBackupKeyword)) == 0)
                fnInitNetConfigForTest(NET_INIT_BACKUP);
            else
            {
                /* convert unicode entry buff to ascii */
                (void)fnUnicode2Ascii(&rGenPurposeEbuf.pBuf[i],oiDistrParamInfo[bCurrDistrParam].pStr,bBuffLen);
               
                if (bBuffLen < bMaxLen)
                {
                    oiDistrParamInfo[bCurrDistrParam].pStr[bBuffLen] = 0;
                }
            }
        }
		
        /* Validate entry of the DNS server ip, if the new entry is valid, ip will be set to new one, or ip keeps old one.  */
        else if (bCurrDistrParam == DISTR_PARAM_PRIMARY_DNS || bCurrDistrParam == DISTR_PARAM_SECONDARY_DNS)
        {
            UINT32  server_ip;
            char   ipstr[16] = {0};
            UINT8   ipArray[4] = {0};


            (void)fnUnicode2Ascii(&rGenPurposeEbuf.pBuf[i],ipstr, bBuffLen);

            //convert the dot IP to array
             dottedQuadToArray(ipArray, ipstr);

            server_ip = IP_ADDR(ipArray);
            /* If this is not a class A, B, or C address, or if it is 0 then it is not 
             * a valid IP address. 
             */
            if ((!IP_CLASSA_ADDR(server_ip) && !IP_CLASSB_ADDR(server_ip) && 
                  !IP_CLASSC_ADDR(server_ip)) || server_ip == 0)
            {
                fnSetEntryErrorCode( ENTRY_INVALID_VALUE);
                usNext = 0;
                return usNext;
            }
            else
            {
                /* convert unicode entry buff to ascii */
                (void)fnUnicode2Ascii(&rGenPurposeEbuf.pBuf[i],oiDistrParamInfo[bCurrDistrParam].pStr,bBuffLen);

                if (bBuffLen < bMaxLen)
                {
                    oiDistrParamInfo[bCurrDistrParam].pStr[bBuffLen] = 0;
                }

                
                //fistly, we need to delete the current DNS in the list table.
                if( bCurrDistrParam == DISTR_PARAM_PRIMARY_DNS )
                {
                    dottedQuadToArray(dnsIp, NucNetworkConfig.primaryDnsServer);
                    NU_Delete_DNS_Server(dnsIp);
                }
                else if( bCurrDistrParam == DISTR_PARAM_SECONDARY_DNS )
                {
                    dottedQuadToArray(dnsIp, NucNetworkConfig.secondaryDnsServer);
                    NU_Delete_DNS_Server(dnsIp);
                }  

                
            }
        }
        else
        {
            memset(oiDistrParamInfo[bCurrDistrParam].pStr, 0, oiDistrParamInfo[bCurrDistrParam].bMaxLen);
            /* convert unicode entry buff to ascii */
            (void)fnUnicode2Ascii(&rGenPurposeEbuf.pBuf[i],oiDistrParamInfo[bCurrDistrParam].pStr,bBuffLen);

            if (bBuffLen < bMaxLen)
            {
            oiDistrParamInfo[bCurrDistrParam].pStr[bBuffLen] = 0;
            }
        }
    
        if( bNumNext > 0)
        {
            usNext = pNextScreens[0];
        }
        
        if( bCurrDistrParam == DISTR_PARAM_PRIMARY_DNS )
        {
            dottedQuadToArray(dnsIp, NucNetworkConfig.primaryDnsServer);
            NU_Add_DNS_Server(dnsIp , DNS_ADD_TO_FRONT);
        }
        else if( bCurrDistrParam == DISTR_PARAM_SECONDARY_DNS )
        {
            dottedQuadToArray(dnsIp, NucNetworkConfig.secondaryDnsServer);
            NU_Add_DNS_Server(dnsIp , DNS_ADD_TO_END);
        }
        
        //Clear the DNS hosts
        DNS_Hosts.dns_head = NU_NULL;
        DNS_Hosts.dns_tail = NU_NULL;
    }
//lint -e774 this macro should not be parenthesized
    else if( bStatus & DISTR_PARAM_STATUS_ERR )
//lint +e774
    {
        fnSetEntryErrorCode( ENTRY_INVALID_VALUE);
        usNext = 0;
    }

    return usNext;
}



/* *************************************************************************
// FUNCTION NAME:  fnhkDistrParamBufferKey
//
// DESCRIPTION: 
//     This function validates the characters entered by the user on the 
//     Distributor Network Configuration Parameter entry screen.  It checks 
//     to see if the new character will invalidate the parameter and if not adds
//     the character to the entry buffer.
//
// INPUTS:
//     Standard hard key function inputs.
//
// OUTPUTS:
//     screen 1 - If parameter is valid
//     same screen if parameter is not valid
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Derek DeGennaro  Initial version
//  02/15/2006  Kan Jiang        Code cleanup
//  04/21/2010  Jingwei,Li       Updated for RTL.
//  06/02/2010  Jingwei,Li       Call function fnDistrParamBufferKeyScrollRTL() to 
//                                buffer distributor parameters for RTL.
//  06/09/2010  Jingwei,Li       Don't return 0 after call function fnDistrParamBufferKeyScrollRTL(). 
// *************************************************************************/
T_SCREEN_ID fnhkDistrParamBufferKey(UINT8       bKeyCode, 
                                    T_SCREEN_ID wNextScr1, 
                                    T_SCREEN_ID wNextScr2)
{
    UINT8  bBuffLen;
    UINT8  bStatus = DISTR_PARAM_STATUS_ERR;
    UINT8  bOldShiftState;
    BOOL   fProcessIt;
    UINT16 usNextChar;
    UINT8   ubWritingDir; 

    /* lookup char and find length of buffer */
    fProcessIt = fnSearchKeyMap(bKeyCode, &usNextChar);
    bBuffLen   = fnCharsBuffered();   

    /* if this is the first character entered, then clear the buffer */
    if (bBuffLen == 0)
    {
        fnClearEntryBuffer();
        fnSetEntryBufCharIndex(0);
    }

    /* make sure the character is ok for the type of param */
    if (fProcessIt == TRUE && bBuffLen <= oiDistrParamInfo[bCurrDistrParam].bMaxLen)
    {
        fProcessIt = FALSE;
        if (oiDistrParamInfo[bCurrDistrParam].lwFlags & DISTR_PARAM_INFO_IP 
           && (isdigit(usNextChar) || (usNextChar == (UINT16)'.' && (bBuffLen > 0))) )
        {
            fProcessIt = TRUE;
        }
        else if (oiDistrParamInfo[bCurrDistrParam].lwFlags & DISTR_PARAM_INFO_PORT 
        	  && isdigit(usNextChar))
        {
            fProcessIt = TRUE;
        }
        else if (oiDistrParamInfo[bCurrDistrParam].lwFlags & DISTR_PARAM_INFO_TEXT)
        {
            fProcessIt = TRUE;
        }
        else if (oiDistrParamInfo[bCurrDistrParam].lwFlags & DISTR_PARAM_INFO_PASSWORD)
        {
            fProcessIt = TRUE;
        }
    }

    if(fProcessIt == FALSE)
    {
          return 0;
    }

    /* see if the current entry is ok and it is accepting new chars */
    bStatus = fnIsDistrParamOK();

    if (bStatus & (DISTR_PARAM_STATUS_KEEP_GOING))
    {
        /* the new char appears ok for this param so far, save the buffer */
        bOldShiftState = fnGetShiftState();
        fnEntryBufferSnapshotCapture();  
        
        ubWritingDir = fnGetWritingDir();
        /* add the new char */
        if(ubWritingDir == RIGHT_TO_LEFT)
        {
            (void)fnDistrParamBufferKeyScrollRTL(bKeyCode);
        }
        else
        {
            (void)fnhkBufferKeyScroll(bKeyCode,wNextScr1,wNextScr2);
        }
        
        /* determine if the new char is ok after all */
        bStatus = fnIsDistrParamOK();
        
        /* restore the entry buff to before adding the new char */
        fnEntryBufferSnapshotRestore();
        fnChangeShiftState(bOldShiftState);
        
        /* if the new char was in fact ok, then add it for good */
        if (bStatus & ( DISTR_PARAM_STATUS_OK | DISTR_PARAM_STATUS_KEEP_GOING))
        {
             if(ubWritingDir == RIGHT_TO_LEFT)
             {
                 fnDistrParamBufferKeyScrollRTL(bKeyCode);
             }
             else
             {
                  return fnhkBufferKeyScroll(bKeyCode,wNextScr1,wNextScr2);
             }
        }
   
    }
    
    return 0;    
}



/* *************************************************************************
// FUNCTION NAME:  fnhkDistrParamXXXX
//
// DESCRIPTION: 
//      These function set the current distributor parameter id and save the distributor parameter  
//      string into array pDistParamTmp, then go to the distributor parameter entry screen.
//
// INPUTS:
//      Standard hard key function inputs.
//
// OUTPUTS:
//      Next screen1
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/15/2006  Kan Jiang          Initial version
// *************************************************************************/
T_SCREEN_ID fnhkDistrParamAcctID(UINT8       bKeyCode, 
                                 T_SCREEN_ID wNextScr1, 
                                 T_SCREEN_ID wNextScr2)
{
    UINT16 usStrLen = 0;
    bCurrDistrParam = DISTR_PARAM_ACCOUNT_ID;

    usStrLen = strlen(oiDistrParamInfo[bCurrDistrParam].pStr);
    memcpy(pDistParamTmp, oiDistrParamInfo[bCurrDistrParam].pStr,  usStrLen);
    pDistParamTmp[usStrLen] = 0;

    return wNextScr1;
}


T_SCREEN_ID fnhkDistrParamAcctPwd(UINT8 bKeyCode, 
								  T_SCREEN_ID wNextScr1, 
								  T_SCREEN_ID wNextScr2)
{
    UINT16 usStrLen = 0;
    bCurrDistrParam = DISTR_PARAM_ACCOUNT_PASSWORD;

    usStrLen = strlen(oiDistrParamInfo[bCurrDistrParam].pStr);
    memcpy(pDistParamTmp, oiDistrParamInfo[bCurrDistrParam].pStr,  usStrLen);
    pDistParamTmp[usStrLen] = 0;

    return wNextScr1;
}

T_SCREEN_ID fnhkDistrParamAniLczIP(UINT8       bKeyCode, 
                                   T_SCREEN_ID wNextScr1, 
                                   T_SCREEN_ID wNextScr2)
{
    UINT16 usStrLen = 0;
    bCurrDistrParam = DISTR_PARAM_ANILCZ_SERVER_IP;

    usStrLen = strlen(oiDistrParamInfo[bCurrDistrParam].pStr);
    memcpy(pDistParamTmp, oiDistrParamInfo[bCurrDistrParam].pStr,  usStrLen);
    pDistParamTmp[usStrLen] = 0;

    return wNextScr1;
}

T_SCREEN_ID fnhkDistrParamAniLczPort(UINT8       bKeyCode, 
                                     T_SCREEN_ID wNextScr1, 
                                     T_SCREEN_ID wNextScr2)
{
    UINT16 usStrLen = 0;
    bCurrDistrParam = DISTR_PARAM_ANILCZ_SERVER_PORT;

    usStrLen = strlen(oiDistrParamInfo[bCurrDistrParam].pStr);
    memcpy(pDistParamTmp, oiDistrParamInfo[bCurrDistrParam].pStr,  usStrLen);
    pDistParamTmp[usStrLen] = 0;

    return wNextScr1;
}

T_SCREEN_ID fnhkDistrParamPriDNS(UINT8       bKeyCode, 
                                 T_SCREEN_ID wNextScr1, 
                                 T_SCREEN_ID wNextScr2)
{
    UINT16 usStrLen = 0;
    bCurrDistrParam = DISTR_PARAM_PRIMARY_DNS;

    usStrLen = strlen(oiDistrParamInfo[bCurrDistrParam].pStr);
    memcpy(pDistParamTmp, oiDistrParamInfo[bCurrDistrParam].pStr,  usStrLen);
    pDistParamTmp[usStrLen] = 0;

    return wNextScr1;
}

T_SCREEN_ID fnhkDistrParamSecDNS(UINT8       bKeyCode, 
                                 T_SCREEN_ID wNextScr1, 
                                 T_SCREEN_ID wNextScr2)
{
    UINT16 usStrLen = 0;
    bCurrDistrParam = DISTR_PARAM_SECONDARY_DNS;

    usStrLen = strlen(oiDistrParamInfo[bCurrDistrParam].pStr);
    memcpy(pDistParamTmp, oiDistrParamInfo[bCurrDistrParam].pStr,  usStrLen);
    pDistParamTmp[usStrLen] = 0;

    return wNextScr1;
}

T_SCREEN_ID fnhkDistrParamDistrURL(UINT8       bKeyCode, 
                                   T_SCREEN_ID wNextScr1, 
                                   T_SCREEN_ID wNextScr2)
{
    UINT16 usStrLen = 0;
    bCurrDistrParam = DISTR_PARAM_DISTR_URL;

    usStrLen = strlen(oiDistrParamInfo[bCurrDistrParam].pStr);
    memcpy(pDistParamTmp, oiDistrParamInfo[bCurrDistrParam].pStr,  usStrLen);
    pDistParamTmp[usStrLen] = 0;

    return wNextScr1;
}


/* *************************************************************************
// FUNCTION NAME: 
//          fnhkSelectDistribParamsEntryScreen
//
// DESCRIPTION:
//          Select the right screen to go.
//
// INPUTS:
//          Standard hard key function inputs (format 2).
//
// RETURNS:
//          TBD.
//
// WARNINGS/NOTES: 
//          None.
//
// MODIFICATION HISTORY:
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                    David Huang  Initial version  
// *************************************************************************/
T_SCREEN_ID fnhkSelectDistribParamsEntryScreen (UINT8        bKeyCode, 
                                                UINT8        bNumNext,
                                                T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;

    switch (bCurrDistrParam)
    {
        case DISTR_PARAM_ACCOUNT_ID: // Account & User ID
            if ( bNumNext > 0 )
            {
                usNext = pNextScreens[0];
            }
            break;
        case DISTR_PARAM_ACCOUNT_PASSWORD: // Global Password
            if ( bNumNext > 1 )
            {
                usNext = pNextScreens[1];
            }
            break;
        case DISTR_PARAM_ANILCZ_SERVER_IP: // ANI/LCZ Server IP
            if ( bNumNext > 2 )
            {
                usNext = pNextScreens[2];
            }
            break;
        case DISTR_PARAM_ANILCZ_SERVER_PORT: // ANI/LCZ Server Port
            if ( bNumNext > 3 )  
            {       
                usNext = pNextScreens[3];
            }
            break;
        case DISTR_PARAM_PRIMARY_DNS: // Primary DNS Server
            if ( bNumNext > 4 )
            {
                usNext = pNextScreens[4];
            }
            break;
        case DISTR_PARAM_SECONDARY_DNS: // Second DNS Server
            if ( bNumNext > 5 )
            {
                usNext = pNextScreens[5];
            }
            break;
        case DISTR_PARAM_DISTR_URL:
            if ( bNumNext > 6 )
            {
                usNext = pNextScreens[6];
            }
            break;
        default:
            break;
    }

    return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnhkRestoreDistrib
//
// DESCRIPTION:
//          Save the default value to the cmos and array pDistParamTmp.
//
// INPUTS:
//          Standard hard key function inputs (format 2).
//
// RETURNS:
//          stay on the same screen.
//
// WARNINGS/NOTES: 
//          None.
//
// MODIFICATION HISTORY:
//      07/18/2006    Raymond Shen  Remove the code which changes CMOS variables,
//                                  and only change the temp variable for display.
//      07/05/2006    Kan Jiang   Calling fnSetNetworkDefaults() for fixing Fraca 100003
//      02/20/2006    Kan Jiang    Modified to save the sting into pDistParamTmp
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                    David Huang  Initial version
// *************************************************************************/
T_SCREEN_ID fnhkRestoreDistrib (UINT8        bKeyCode, 
                                UINT8        bNumNext,
                                T_SCREEN_ID *pNextScreens)
{
    char *retStr;
    UINT16 usStrLen;

    memset(pDistParamTmp, '\0', sizeof(pDistParamTmp));
    switch (bCurrDistrParam)
    {
        // Account & User ID
        case 0: 
            retStr = (char *) fnFlashGetAsciiStringParm(ASP_ISP_ACCOUNT_AND_USER_ID);
            if(retStr != 0)
            {
                strncpy(pDistParamTmp, retStr,
                        sizeof(fnCMOSGetNetworkConfig()->attLoginAccountAndUserId) - 1);
            }
            break;

             // Global Password
        case 1: 
            retStr = (char *) fnFlashGetAsciiStringParm(ASP_ISP_PASSWORD);
            if(retStr != 0)
            {
                strncpy(pDistParamTmp, retStr,
                        sizeof(fnCMOSGetNetworkConfig()->attPassword) - 1);
            }
            break;
        
            // ANI/LCZ Server IP
        case 2:
            retStr = (char *) fnFlashGetAsciiStringParm(ASP_ISP_ANI_LCZ_SERVER_IP);
            if(retStr != 0)
            {
                strncpy(pDistParamTmp, retStr,
                        sizeof(fnCMOSGetNetworkConfig()->attAniServerIp) - 1);
            }
            break;

            // ANI/LCZ Server Port
        case 3: 
            retStr = (char *) fnFlashGetAsciiStringParm(ASP_ISP_ANI_LCZ_PORT_NUMBER);
            if(retStr != 0)
            {
                strncpy(pDistParamTmp, retStr,
                        sizeof(fnCMOSGetNetworkConfig()->attAniServerPortNumber) - 1);
            }
            break;  

            // Primary DNS Server
        case 4:
            retStr = (char *) fnFlashGetAsciiStringParm(ASP_PRIMARY_DNS_SERVER);
            if(retStr != 0)
            {
                strncpy(pDistParamTmp, retStr,
                        sizeof(fnCMOSGetNetworkConfig()->primaryDnsServer ) - 1);
            }
            break;

            // Second DNS Server
        case 5:
            retStr = (char *) fnFlashGetAsciiStringParm(ASP_SECONDARY_DNS_SERVER);
            if(retStr != 0)
            {
                strncpy(pDistParamTmp, retStr,
                        sizeof(fnCMOSGetNetworkConfig()->secondaryDnsServer) - 1);
            }
            break;

            // Distributor URL
        case 6:
            retStr = (char *) fnFlashGetAsciiStringParm(ASP_DISTRIBUTOR_URL);
            if(retStr != 0)
            {
//lint -e419 this macro should not be parenthesized
                strncpy(pDistParamTmp, retStr,
                        sizeof(fnCMOSGetNetworkConfig()->distributorUrl) - 1);
//lint +e419
            }
            break;

        default:
            break;
    }

    return( pNextScreens[0] );
}



/* *************************************************************************
// FUNCTION NAME: fnhkNoPrefixRequired
//
// DESCRIPTION: 
//       Hard key function that set the dialing prefix as NULL
//
// INPUTS:
//       Standard hard key function inputs.
//
// OUTPUTS:
//       always return wNextScr1
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/15/2006   Kan Jiang   Initial Version
// *************************************************************************/
T_SCREEN_ID fnhkNoPrefixRequired(UINT8       bKeyCode, 
                                 T_SCREEN_ID wNextScr1, 
                                 T_SCREEN_ID wNextScr2)
{
      memset( fnCMOSSetupGetCMOSSetupParams()->DialingPrefix,  0, 
              sizeof(fnCMOSSetupGetCMOSSetupParams()->DialingPrefix) );

      return wNextScr1;
}



/* *************************************************************************
// FUNCTION NAME: fnhkValidatePhoneNumber
//
// DESCRIPTION: 
//       Hard key function that stores the PB data center phonen number
//		 in CMOS.  The string is taken from the contents of the current
//		 entry buffer.  Any decimal point characters that were entered
//		 need to be translated to commas which are understood by the
//		 modem (for pauses).
//
// INPUTS:
//       Standard hard key function inputs.
//
// OUTPUTS:
//       always return wNextScr1
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//               Joe Mozdzer  Initial Version
//  02/15/2006   Kan Jiang    Code cleanup
// *************************************************************************/
T_SCREEN_ID fnhkValidatePhoneNumber(UINT8       bKeyCode, 
                                      T_SCREEN_ID wNextScr1, 
                                      T_SCREEN_ID wNextScr2)
{
	UINT32	i;
	UINT32  j;
	UINT16	usUniChar;
	char	chAsciiChar;

	/* first set the number to all NULL's in CMOS.  this keeps it neat in case the
		string entered is shorter than the maximum possible length. */
	for (i = 0; i <= MAX_PHONE_LEN; i++)
	{
		fnCMOSSetupGetCMOSSetupParams()->PhoneNumber[i] = (char) NULL;
	}

	i = j = 0;
	/* move the Unicode characters from the entry buffer into the CMOS buffer.
		Spaces aren't moved since they don't do anything and decimal characters
		are converted to commas. */
	do
	{
		usUniChar = rGenPurposeEbuf.pBuf[i++];
		if (usUniChar != UNICODE_SPACE)
		{
			if (usUniChar == fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR))
			{
				chAsciiChar = ',';
			}
			else
			{
				chAsciiChar = (char) usUniChar;
			}

			fnCMOSSetupGetCMOSSetupParams()->PhoneNumber[j++] = chAsciiChar;
		}
	} while ((usUniChar != (UINT16) NULL) && (j < MAX_PHONE_LEN));

	return (wNextScr1);
}



/* *************************************************************************
// FUNCTION NAME: fnhkBufferKeyDialPrefix
//
// DESCRIPTION: 
//       Hard key function that buffers a key press on the dialing
//	     prefix entry screen.  This function converts the decimal point character
//	     to a comma otherwise the character is buffered.
//
// INPUTS:
//       Standard hard key function inputs.
//
// OUTPUTS:
//       always return wNextScr1
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//               Derek DeGennaro  Initial Version
//  02/15/2006   Kan Jiang        Code cleanup
// *************************************************************************/
T_SCREEN_ID fnhkBufferKeyPhoneNumChar(UINT8       bKeyCode, 
                                      T_SCREEN_ID wNextScr1, 
                                      T_SCREEN_ID wNextScr2)
{
	BOOL   fProcessIt;
	UINT16 usNextChar;
	UINT16 usDecimalChar;

	fProcessIt = fnSearchKeyMap(bKeyCode, &usNextChar);
	usDecimalChar = fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR);

	if (fProcessIt == TRUE && 
		(isdigit(usNextChar) || usNextChar == usDecimalChar || 
		usNextChar == (UINT16)',' || usNextChar == '#' || 
		usNextChar == (UINT16)'*' || (usNextChar >= 'A' && usNextChar <= 'D')))
	{
		if (usNextChar == usDecimalChar)
		{
			fnBufferUnicode((UINT16)',');
		}
		else
		{
			fnhkBufferKeyStd(bKeyCode,wNextScr1,wNextScr2);
		}
	}

	return (wNextScr1);
}




/* *************************************************************************
// FUNCTION NAME: fnhkValidatePrefix
//
// DESCRIPTION: 
//       Hard key function that stores the dialing prefix in CMOS.
//       The string is taken from the contents of the current
//	     entry buffer.  Any decimal point characters that were entered
//		 need to be translated to commas which are understood by the
//       modem (for pauses).
//
// INPUTS:
//       Standard hard key function inputs.
//
// OUTPUTS:
//       always return wNextScr1
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//               Joe Mozdzer  Initial Version
//  02/15/2006   Kan Jiang    Code cleanup
// *************************************************************************/
T_SCREEN_ID fnhkValidatePrefix(UINT8       bKeyCode, 
                               T_SCREEN_ID wNextScr1, 
                               T_SCREEN_ID wNextScr2)
{
	UINT32	i;
	UINT32  j;
	UINT16	usUniChar;
	char   chAsciiChar;

	/* first set the number to all NULL's in CMOS.  this keeps it neat in case the
	  string entered is shorter than the maximum possible length. */
	for (i = 0; i <= MAX_PREFIX_LEN; i++)
	{
		fnCMOSSetupGetCMOSSetupParams()->DialingPrefix[i] = (char) NULL;
	}

	i = j = 0;
	/* move the Unicode characters from the entry buffer into the CMOS buffer.
		Spaces aren't moved since they don't do anything and decimal characters
		are converted to commas. */
	do
	{
		usUniChar = rGenPurposeEbuf.pBuf[i++];
		if (usUniChar != UNICODE_SPACE)
		{
			if (usUniChar == fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR))
			{
				chAsciiChar = ',';
			}
			else
			{
				chAsciiChar = (char) usUniChar;
			}

			fnCMOSSetupGetCMOSSetupParams()->DialingPrefix[j++] = chAsciiChar;
		}
	
	} while ((usUniChar != (unsigned short) NULL) && (j < MAX_PREFIX_LEN));

	return (wNextScr1);
}





/* *************************************************************************
// FUNCTION NAME: 
//          fnhkValidateModemInitStr
//
// DESCRIPTION:
//          Hard key function that validates modem init string
//
// INPUTS:
//          Standard hard key function inputs (format 2).
//
// RETURNS:
//          TBD.
//
// WARNINGS/NOTES: 
//          None.
//
// MODIFICATION HISTORY:
//    07/19/2006    Raymond Shen    Removed Country Code related code.
//    07/05/2006    Kan.Jiang   Add calling of fnSetNetworkDefaults() for Fraca 100003
//    12/06/2005    Adam Liu    Adapted for FuturePhoenix
//                  David Huang  Initial version  
// *************************************************************************/
T_SCREEN_ID fnhkValidateModemInitStr(UINT8        bKeyCode, 
                                     UINT8        bNumNext,
                                     T_SCREEN_ID *pNextScreens)
{
    return( pNextScreens[0]);        
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnhkRestoreModemInitStr
//
// DESCRIPTION:
//          Save the default modem string to cmos.
//
// INPUTS:
//          Standard hard key function inputs (format 2).
//
// RETURNS:
//          next screen.
//
// WARNINGS/NOTES: 
//          None.
//
// MODIFICATION HISTORY:
//    07/19/2006    Raymond Shen    Just restore value of the buffer string, not really
/                                   modify the CMOS variable value; Remove Contry
//                                  Code related code.
//    07/05/2006    Kan.Jiang   Add calling of fnSetNetworkDefaults() for Fraca 100003
//    12/06/2005    Adam Liu    Adapted for FuturePhoenix
//                 David Huang  Initial version  
// *************************************************************************/
T_SCREEN_ID fnhkRestoreModemInitStr(UINT8        bKeyCode, 
                                    UINT8        bNumNext,
                                    T_SCREEN_ID *pNextScreens)
{
    return( pNextScreens[0]);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnhkEnterModemEditScr
//
// DESCRIPTION:
//          Hard key function to save modem string to pDistParamTmp array before go to Modem 
//          Edit screen
//
// INPUTS:
//          Standard hard key function inputs (format 2).
//
// RETURNS:
//          next screen.
//
// WARNINGS/NOTES: 
//          None.
//
// MODIFICATION HISTORY:
//         02/18/2006     Kan Jiang    Initial verion
// *************************************************************************/
T_SCREEN_ID fnhkEnterModemEditScr(UINT8        bKeyCode, 
                                  UINT8        bNumNext,
                                  T_SCREEN_ID *pNextScreens)
{
    UINT16 usStrLen = 0;

    memset(pDistParamTmp, 0, sizeof(char) * DISTR_LEN_TMPARRAY);

    usStrLen = strlen(fnCMOSGetNetworkConfig()->socketmodemInit);
    memcpy(pDistParamTmp, fnCMOSGetNetworkConfig()->socketmodemInit, usStrLen);
    pDistParamTmp[usStrLen] = 0;

    return pNextScreens[0];
}



/* *************************************************************************
// FUNCTION NAME: fnhkSavEntryBufToTmpArray
//
// DESCRIPTION: 
//       This function save the string which is input in 
//       Edit Option screen to pDistParamTmp
//
// INPUTS:
//       Standard hard key function inputs.
//
// OUTPUTS:
//       Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/22/2014  Renhao        Updated code to fix fraca 225381.
//  06/09/2010  Jingwei,Li    Comment the special logic to get RTL buffer becase
//                            "Edit opitons" has been disabled for RTL writting.
//  06/02/2010  Jingwei,Li    Disable "Edit Options" for RTL writting.
//  02/17/2006  Kan Jiang         Initial version
// *************************************************************************/
T_SCREEN_ID fnhkSavEntryBufToTmpArray(UINT8        bKeyCode, 
                                      UINT8        bNumNext,
                                      T_SCREEN_ID *pNextScreens)
{
      UINT8   bBuffLen = 0;
      UINT8   ubWritingDir;
      UINT8   i = 0;    

      //Added  for fraca 225381
      if(fnCharsBuffered() <= 0)
      {
          return (UINT16)0;
      }

      ubWritingDir = fnFlashGetByteParm(BP_LANGUAGE_WRITING_DIR);
      if(ubWritingDir != 0)
      {
          return (UINT16)0;
      }
      
      memset(pDistParamTmp, 0, sizeof(char) * DISTR_LEN_TMPARRAY);

      bBuffLen = fnCharsBuffered();
      if (bBuffLen > DISTR_LEN_TMPARRAY)
      {
          bBuffLen = DISTR_LEN_TMPARRAY;
	  }
      /* "Edit Options" has been removed for RTL, so it's useless now.
      if((ubWritingDir = fnGetWritingDir())== RIGHT_TO_LEFT )
      {
        while(rGenPurposeEbuf.pBuf[i]== UNICODE_SPACE)
        {
            i++;
        }
      }
      */
      /* convert unicode entry buff to ascii */
      (void)fnUnicode2Ascii(&rGenPurposeEbuf.pBuf[i], pDistParamTmp, bBuffLen) ;

      if (bBuffLen < DISTR_LEN_TMPARRAY)
	  {
          pDistParamTmp[bBuffLen] = 0;
	  }
      
      return pNextScreens[0];
}


/* *************************************************************************
// FUNCTION NAME: fnhkDistrSavBufToTmpArray
//
// DESCRIPTION: 
//       This function save the string which is input in Edit Option screen 
//       to pDistParamTmp
//
// INPUTS:
//       Standard hard key function inputs.
//
// OUTPUTS:
//       Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/17/2006  Kan Jiang   Initial version
// *************************************************************************/
T_SCREEN_ID fnhkDistrSavBufToTmpArray(UINT8        bKeyCode, 
                                      UINT8        bNumNext,
                                      T_SCREEN_ID *pNextScreens)
{
    UINT8  bBuffLen = 0;
    UINT8   ubWritingDir;
    UINT8   i = 0;    

    memset(pDistParamTmp, 0, sizeof(char) * DISTR_LEN_TMPARRAY);
    
    bBuffLen = fnCharsBuffered();

    if (bBuffLen > DISTR_LEN_TMPARRAY)
	{
            bBuffLen = DISTR_LEN_TMPARRAY;
	}
    ubWritingDir = fnGetWritingDir();
    if(ubWritingDir == RIGHT_TO_LEFT )
    {
        while(rGenPurposeEbuf.pBuf[i]== UNICODE_SPACE)
        {
            i++;
        }
    }   
    /* convert unicode entry buff to ascii */
    (void)fnUnicode2Ascii(&rGenPurposeEbuf.pBuf[i], pDistParamTmp, bBuffLen) ;

    if (bBuffLen < DISTR_LEN_TMPARRAY)
	{
        pDistParamTmp[bBuffLen] = 0;
	}
      
    return pNextScreens[bCurrDistrParam];
}



/* *************************************************************************
// FUNCTION NAME: 
//         fnhkDistrDelCharOrChangeScrOverLap
//
// DESCRIPTION:
//         Deletes a character from the current entry buffer.  If no 
//         characters were previously typed into the buffer (buffer is
//         already clear), go to next screen according to bCurrDistrParam.
//
// INPUTS:
//         Standard hard key function inputs (format 2).
//
// RETURNS:
//         Next screen
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      02/22/2006  Kan Jiang    Initial version
// *************************************************************************/
T_SCREEN_ID fnhkDistrDelCharOrChangeScrOverLap (UINT8        bKeyCode, 
                                                UINT8        bNumNext,
                                                T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID  usNext = 0;

    if (fnCharsBuffered() > 0)
    {
        (void)fnhkDeleteCharacterScroll(bKeyCode, pNextScreens[0], pNextScreens[1]);
    }
    else
    {
        if (bNumNext > bCurrDistrParam)
        {
            usNext = pNextScreens[bCurrDistrParam];
        }
    }

    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnhkPerformConfirmationUpload
//
// DESCRIPTION:
//         Hard key function that sets the type of service to 
//         upload confirmation service records.
//
// INPUTS:
//         Standard hard key function inputs (format 2).
//
// RETURNS:
//         Next screen
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      10/24/2007  Oscar Wang   Replace fnhkModeSetDefault()
//                               with fnRatingSetDefault().
//      07/19/2006  Dicky Sun    Move the service request to check pending
//                               upload screen.
//      07/14/2006  Dicky Sun    Update the logic.Go to the correct next 
//                               screen.
//      07/12/2006  Dicky Sun    Make sure all pending records are 
//                               saved in FFS.
//      07/07/2006  Dicky Sun    Initial version
// *************************************************************************/
T_SCREEN_ID fnhkPerformConfirmationUpload (UINT8 		  bKeyCode, 
                                           UINT8		  bNumNext, 
                                           T_SCREEN_ID    *pNextScreens)
{
    T_SCREEN_ID usNextScreen = 0;

    return (usNextScreen); 
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnhkRefillPostage
//
// DESCRIPTION:
//         Hard key function that sets the printing uploading record receipt flag. 
//
// INPUTS:
//         Standard hard key function inputs (format 2).
//
// RETURNS:
//         Next screen
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      06/29/2010  Jingwei,LI      Added code to support reissue case.
//      06/22/2010  Raymond Shen    Add code to support the Refill Code feature.
//      02/29/2008  Raymond Shen    Added code to support super password protection.
//      07/27/2006  Dicky Sun    Initial version
// *************************************************************************/
T_SCREEN_ID fnhkRefillPostage (UINT8 		  bKeyCode, 
                               UINT8		  bNumNext, 
                               T_SCREEN_ID    *pNextScreens)
{
    T_SCREEN_ID  usNext = 0;   

//    if(fnFlashGetByteParm(BP_REFILL_REISSUE_METER) == REFIIL_IS_BY_REISSUE)
//    {
//        return 0;// don't allow refill for reissue meter
//    }

    // Enable the Refill Code option only when refill is not protected 
    // by Super Pswd.
    if( fnIsRefillCodeSupported() == TRUE )
    {
        if(fnHasRefillcode() == TRUE) // Refill code has been set.
        {
            // Setup the navgation informanion for Refill Code entry.
            fnSPasswordEnterPrep( bKeyCode, bNumNext - 2, pNextScreens + 2, 
                                                        pNextScreens[1], 
                                                        fnhSRefillPostage);

            usNext = pNextScreens[3]; // RefillVerifyPassword screen
        }
        else // Refill code hasn't been set, so directly go to the destination.
        {
            usNext = fnhSRefillPostage(bKeyCode, bNumNext-2, &pNextScreens[2]); 
        }
    }
    else
    {
/*
        usNext = fnhSPWDCheckProtection(bKeyCode, bNumNext, pNextScreens,
                            fnhSRefillPostage, SUPER_FEATURE_REFILL, TRUE);
*/
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkToggleBlindDialingSetting
//
// DESCRIPTION: 
//      Hard key functions that toggle the ON/OFF setting for Blind Dialing.
//
// INPUTS:
//      Standard hard key function inputs.
//
// OUTPUTS:
//      Return to current screen.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      08/03/2006   Raymond Shen    Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkToggleBlindDialingSetting(UINT8   bKeyCode, 
                                       UINT8   bNumNext, 
                                       T_SCREEN_ID *pNextScreens)
{
	return (pNextScreens[0]);
}



/* *************************************************************************
// FUNCTION NAME: 
//       fnhkCPerformDcapUpload
//
// DESCRIPTION: 
//       Hard key function that sets the type of service to DCAP upload.
//
// INPUTS:
//       Standard hard key function inputs.
//
// OUTPUTS:
//       goes to wNextScr1 if dcap enabled, otherwise stays on same screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//               Derek DeGennaro  Initial Version
//  12/14/2006   Adam Liu         Code cleanup
// *************************************************************************/
T_SCREEN_ID fnhkCPerformDcapUpload(UINT8       bKeyCode, 
                                   T_SCREEN_ID wNextScr1, 
                                   T_SCREEN_ID wNextScr2)
{
	T_SCREEN_ID wNext = 0;	/* by default, stay on current screen. */

	if (fnOITGetDisablingConditions2() & DCOND2_FFXFER_INTERRUPTED)
	{
		/* set flag to indicate call PBP */
		ucOITRequestedService = DCAP_UPLOAD_SERVICE;
		
		/* goto screen 1 */
		wNext = wNextScr1; // InfraContactPB
        
	    /* Clear the auto upload. */
	    //TODO JAH Remove dcapi.c if (fnDCAPIsAutoUploadEventPending() == TRUE)
	    //TODO JAH Remove dcapi.c     fnDCAPCancelAutoUploadEvent();  
	}
	else
	{
#if 0 //TODO JAH remove dcap 		/* check to see if dcap is enabled. */
		if (fnDCAPIsDataCaptureActive() == TRUE)
		{
			/* set flag to indicate data capture upload */
			ucOITRequestedService = DCAP_UPLOAD_SERVICE;
			
			/* goto screen 1 */
			wNext = wNextScr1; // InfraContactPB
        
	        /* Clear the auto upload. */
	        //TODO JAH Remove dcapi.c if (fnDCAPIsAutoUploadEventPending() == TRUE)
	        //TODO JAH Remove dcapi.c     fnDCAPCancelAutoUploadEvent();  
		}
#endif
	}

	return wNext;
}


/* *************************************************************************
// FUNCTION NAME: 
//       fnhkDCAPDoUploadLater
//
// DESCRIPTION: 
//       Hard key function that is called when the user chooses not
//       to do a Data Capture Upload.
//
// INPUTS:
//       Standard hard key function inputs.
//
// OUTPUTS:
//       goes to wNextScr1
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//               Derek DeGennaro  Initial Version
//  12/14/2006   Adam Liu         Code cleanup
// *************************************************************************/
T_SCREEN_ID fnhkDCAPDoUploadLater (UINT8       bKeyCode, 
                                   T_SCREEN_ID wNextScr1, 
                                   T_SCREEN_ID wNextScr2)
{	
	/* Clear the unattended upload flags. */
//	fOITDCAPDoUnattendedUpload = FALSE;

	/* Clear the auto upload event. */
//	if (fnDCAPIsDataCaptureActive() == TRUE && fnDCAPIsAutoUploadEventPending() == TRUE)	
//		fnDCAPCancelAutoUploadEvent();
	
	return wNextScr1;	
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnhkCPerformMeterMove
//
// DESCRIPTION:
//         Hard key function that sets the type of service to meter move (within
//         region).
//
// INPUTS:
//         Standard hard key function inputs (format 2).
//
// RETURNS:
//         Next screen
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//  29 Mar 2007 Bill Herring    Initial
//      
// *************************************************************************/
T_SCREEN_ID fnhkCPerformMeterMove (UINT8 		  bKeyCode, 
	                               UINT8		  bNumNext, 
	                               T_SCREEN_ID    *pNextScreens)
{
	unsigned short wNext = 0;
	fnOITSetRequestedService( METER_MOVE_SERVICE );
	fOITMeterMoveOutsideRegion = FALSE;
	if (bNumNext > 0)
		wNext = pNextScreens[0];
	return wNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnhkCPerformMeterMoveOutsideRegion
//
// DESCRIPTION:
//         Hard key function that sets the type of service to meter move (outside
//         region).
//
// INPUTS:
//         Standard hard key function inputs (format 2).
//
// RETURNS:
//         Next screen
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//  29 Mar 2007 Bill Herring    Initial
//      
// *************************************************************************/
T_SCREEN_ID fnhkCPerformMeterMoveOutsideRegion (UINT8 		  bKeyCode, 
				                               UINT8		  bNumNext, 
				                               T_SCREEN_ID    *pNextScreens)
{
	unsigned short wNext = 0;
	fnOITSetRequestedService( METER_MOVE_SERVICE );
	fOITMeterMoveOutsideRegion = TRUE;
	if (bNumNext > 0)
		wNext = pNextScreens[0];
	return wNext;
}

/* *************************************************************************
// FUNCTION NAME: fnhkCPerformConfirmationUpload
//
// DESCRIPTION: 
//          Hard key function that sets the type of service to 
//				upload confirmation service records.
//
// INPUTS:
//          Standard hard key function inputs (format 2).
//
// RETURNS:
//          always pNextScreens[0]
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
// 08/01/2008   Raymond Shen    Replaced SWDOWNLOAD_SERVICE with ET_CALL_HOME to  
//                              avoid doing Budget account upload while downloading.
//          07/25/2007  Joey Cui    Reuse from JNUS 
// *************************************************************************/
T_SCREEN_ID fnhkCPerformConfirmationUpload( UINT8 bKeyCode, 
                                            UINT8 bNumNext, 
                                            T_SCREEN_ID *pNextScreens)
{
	/* set flag to indicate refund */
	ucOITRequestedService = CONFIRMATION_SERVICE;

	// check for possible DLA missing file request
	(void)fnValidateExtFiles();
    if (CMOSFilesToDownload.DownloadFileCount)
    {
        // Force a DLA 1st before performing any Confirmation services!!!
        ucOITRequestedService = ET_CALL_HOME;
    }
	(void)fnhkSetModeToPostage(bKeyCode,bNumNext,pNextScreens);

	return (pNextScreens[0]);
}

/* *************************************************************************
// FUNCTION NAME: fnhkCPerformDataUpload
//
// DESCRIPTION: 
 //          Hard key function that sets the type of service to Data Upload.
//
// INPUTS:
//          Standard hard key function inputs (format 2).
//
// RETURNS:
//          go to next screen
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//  07/24/2008  Raymond Shen    Adapted for FPHX 
//              Zhang Yingbo    Initial version for Mega
// *************************************************************************/
T_SCREEN_ID fnhkCPerformDataUpload( UINT8 bKeyCode, 
                                            UINT8 bNumNext, 
                                            T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;

    if(IsAbAccountingUploadEnabled() == TRUE)
    {
        if(fnIsDataCenterUploadAvail() == TRUE)
        {
            /* set flag to indicate account balance */
            ucOITRequestedService = DATA_UPLOAD_SERVICE;
        
            fStrDCapUploaded = OIT_INFRA_UPLOAD_NOT;
            fStrEServiceUploaded = OIT_INFRA_UPLOAD_NOT;
            fStrAcctUploaded = OIT_INFRA_UPLOAD_NOT;

            fnOITSetCurrentOnlineServType(OI_ONLINE_SERV_MANUAL);
            
            usNext = pNextScreens[0];   // InfraContactPB
         }

         else
         {
            usNext = pNextScreens[1];/* No data to upload Screen*/
         }
    }
    else
    {
        if (fnOITGetDisablingConditions2() & DCOND2_FFXFER_INTERRUPTED)
        {
        	/* set flag to indicate call PBP */
        	ucOITRequestedService = DCAP_UPLOAD_SERVICE;
			
        	/* goto screen 1 */
        	usNext = pNextScreens[0]; // InfraContactPB
            
            /* Clear the auto upload. */
            //TODO JAH Remove dcapi.c if (fnDCAPIsAutoUploadEventPending() == TRUE)
            //TODO JAH Remove dcapi.c     fnDCAPCancelAutoUploadEvent();  
        }
        else
        {
#if 0 //TODO JAH remove dcap 
        	/* check to see if dcap is enabled. */
        	if (fnDCAPIsDataCaptureActive() == TRUE)
        	{
        		/* set flag to indicate data capture upload */
        		ucOITRequestedService = DCAP_UPLOAD_SERVICE;
				
        		/* goto screen 1 */
        		usNext = pNextScreens[0]; // InfraContactPB
            
                /* Clear the auto upload. */
                //TODO JAH Remove dcapi.c if (fnDCAPIsAutoUploadEventPending() == TRUE)
                //TODO JAH Remove dcapi.c     fnDCAPCancelAutoUploadEvent();  
        	}
#endif //TODO JAH remove dcap 
        }
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: fnhkPerformAbacusDataUpload
//
// DESCRIPTION: 
 //          Hard key function that sets the type of service to Data Upload.
//
// INPUTS:
//          Standard hard key function inputs (format 2).
//
// RETURNS:
//          go to next screen
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//  06/17/2009  Raymond Shen    Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkPerformAbacusDataUpload( UINT8 bKeyCode, 
                                            UINT8 bNumNext, 
                                            T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;

    if( (fnBMWVCheckAnyActDataUpload() == TRUE) && (bNumNext > 0) )
    {
        /* set flag to indicate account balance */
        ucOITRequestedService = DATA_UPLOAD_SERVICE;
    
        fStrDCapUploaded = OIT_INFRA_UPLOAD_NOT;
        fStrEServiceUploaded = OIT_INFRA_UPLOAD_NOT;
        fStrAcctUploaded = OIT_INFRA_UPLOAD_NOT;

        fnOITSetCurrentOnlineServType(OI_ONLINE_SERV_MANUAL);

        usNext = pNextScreens[0];
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: fnhkQuitAbacusDataUploadAlert
//
// DESCRIPTION: 
//          Hard key function that quit screen AbacusDataUploadAlert without 
//          connecting data center.
//
// INPUTS:
//          Standard hard key function inputs (format 2).
//
// RETURNS:
//          go to next screen
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//  06/17/2009  Raymond Shen    Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkQuitAbacusDataUploadAlert( UINT8 bKeyCode, 
                                            UINT8 bNumNext, 
                                            T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;
    UINT8   bIndex = 0;
    UINT8   ucRequestFrom = OI_DATA_UPLD_FROM_NONE;

    ucRequestFrom = fnOITGetDataUploadReqFromStatus();

    switch(ucRequestFrom){
    case OI_DATA_UPLD_FROM_TRANSLOG:
        usNext = pNextScreens[4]; // mAbacusTransactionClearQueryCon
        //GetScreen(SCREEN_ABACUS_TRANS_CLEAR_QUERY);
        break;
    case OI_DATA_UPLD_FROM_ACCTTYPE:
        bIndex = fnBMWVGetActChgToType();

        switch(bIndex){
        case 1:
        	usNext = fnhkAccountTypeSelect2(0, 3, &pNextScreens[1]);
        	break;

        case 2:
            usNext = fnhkAccountTypeSelect3(0, 3, &pNextScreens[1]);
            break;

        case 3:
            usNext = fnhkAccountTypeSelect4(0, 3, &pNextScreens[1]);
            break;

        case 0:
        default:
            usNext = fnhkAccountTypeSelect1(0, 1, &pNextScreens[0]);
            break;
        }
        ///Clear the Flag
        fnBMWVSetActChgToType(0);
        break;
        
    case OI_DATA_UPLD_FROM_ACCTDATA:
        usNext = pNextScreens[5]; // AbacusAccountResetAllQuery
        break;
    case OI_DATA_UPLD_FROM_ACCTEND:
        usNext = pNextScreens[5]; // AbacusAccountResetAllQuery
        break;
    default:
        break;
    }

    // Clear the flag
    fnOITSetDataUploadReqFromStatus(OI_DATA_UPLD_FROM_NONE);

    return usNext;
}
/* *************************************************************************
// FUNCTION NAME: fnhkAccountDataUploadNow
//
// DESCRIPTION: 
//      Hard key function that send a "do the service now" response
//      to distributor.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      go to next screen
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//  07/24/2008  Raymond Shen    Adapted for FPHX 
//              Zhang Yingbo    Initial version for Mega
// *************************************************************************/
T_SCREEN_ID fnhkAccountDataUploadNow( UINT8 bKeyCode, 
                                            UINT8 bNumNext, 
                                            T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;

    UINT8 pMsgData[2];

    /* Set the message data to be sent*/
    pMsgData[0] = TRUE;
    pMsgData[1] = 0;

    /* Response to the start account data upload message */
    (void)OSSendIntertask(DISTTASK, OIT, DATA_UPLOAD_SERVICE, BYTE_DATA,
                          pMsgData, sizeof(pMsgData));

    if (bNumNext > 0)
    {
        usNext = pNextScreens[0];
    }
        
    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: fnhkAccountDataUploadLater
//
// DESCRIPTION: 
//      Hard key function that send a "don't do the service"
//      response to distributor.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      go to next screen
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//  07/24/2008  Raymond Shen    Adapted for FPHX 
//              Zhang Yingbo    Initial version for Mega
// *************************************************************************/
T_SCREEN_ID fnhkAccountDataUploadLater( UINT8 bKeyCode, 
                                            UINT8 bNumNext, 
                                            T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;

    UINT8 pMsgData[2];

    /* Set the message data to be sent*/
    pMsgData[0] = FALSE;
    pMsgData[1] = 0;

    /* Response to the start account data upload message */
    (void)OSSendIntertask(DISTTASK, OIT, DATA_UPLOAD_SERVICE, BYTE_DATA,
                          pMsgData, sizeof(pMsgData));

    if (bNumNext > 0)
    {
        usNext = pNextScreens[0];
    }
        
    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: fnhkDataUploadLogSelect1~4
//
// DESCRIPTION: 
//      Hard key function that select a data upload log to see details.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      go to next screen
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//              Zhang Yingbo    Initial version for Mega
// *************************************************************************/
T_SCREEN_ID fnhkDataUploadLogSelect1( UINT8 bKeyCode, 
                                            UINT8 bNumNext, 
                                            T_SCREEN_ID *pNextScreens)
{
    return (fnDataUploadLogSelect( 0, bKeyCode, bNumNext, pNextScreens ));
}

T_SCREEN_ID fnhkDataUploadLogSelect2( UINT8 bKeyCode, 
                                            UINT8 bNumNext, 
                                            T_SCREEN_ID *pNextScreens)
{
    return (fnDataUploadLogSelect( 1, bKeyCode, bNumNext, pNextScreens ));
}

T_SCREEN_ID fnhkDataUploadLogSelect3( UINT8 bKeyCode, 
                                            UINT8 bNumNext, 
                                            T_SCREEN_ID *pNextScreens)
{
    return (fnDataUploadLogSelect( 2, bKeyCode, bNumNext, pNextScreens ));
}

T_SCREEN_ID fnhkDataUploadLogSelect4( UINT8 bKeyCode, 
                                            UINT8 bNumNext, 
                                            T_SCREEN_ID *pNextScreens)
{
    return (fnDataUploadLogSelect( 3, bKeyCode, bNumNext, pNextScreens ));
}


/* Event functions */




/* *************************************************************************
// FUNCTION NAME: 
//		fnevInfraStartConnectToATT
//
// DESCRIPTION: 
// 		Event function that is triggered after sending the dial string
//		to the modem.
//		This function sets a flag to update the notification line on the 
//		ContactPB screen.
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		eInfraContactPBState = INFRA_CONTACT_PB_CONNECTING
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 				Derek DeGennaro		Initial version
//	1/23/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
T_SCREEN_ID fnevInfraStartConnectToATT (UINT8 			bNumNext,
									    T_SCREEN_ID  *	pNextScreens,
									    INTERTASK 	*	pIntertask)
{
	eInfraContactPBState = INFRA_CONTACT_PB_CONNECTING;

	return 0; 
}



/* *************************************************************************
// FUNCTION NAME: 
//		fnevProcessDistrMsg
//
// DESCRIPTION: 
//  	This function is called after receiving an intertask message from the 
//	  	distributor task.  The distributor task sends the OIT messages to request
//  	that a service be performed, to indicate that a service has been performed,
//  	that there are no more services to be performed, and the status of services
//  	being performed.  The OIT generates an EVENT_DISTR_TASK_MSG screen event for
//  	these messages.  This function handles this screen event.
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
// 		Next screens:
//		1. Home screen
//		2. Refill Successful
//		3. PBP Balances
//		4. Refund complete
//		5. Download update available
//		6. Download complete
//      7. EServicePromptReceipt
//      8. EServiceInvalidZipWarning
//      9. PBPDcapUploadComplete
//     10. PBPBalancesSuccessful
//
//
// MODIFICATION HISTORY:
// 				Derek DeGennaro		Initial version
//	01/23/2006	Vincent Yi			Code cleanup
//  06/29/2010  Jinwei,LI       Added case to process messge OIT_INFRA_REFILL_SERV_STATUS.
//
// *************************************************************************/
T_SCREEN_ID fnevProcessDistrMsg (UINT8 			bNumNext, 
							     T_SCREEN_ID  *	pNextScreens, 
							     INTERTASK 	*	pIntertask)
{
	T_SCREEN_ID 	usNext = 0;
	UINT8			ucMsgId = pIntertask->bMsgId;

/*Vincent
	unsigned char bService[2];
*/

//	fDialingServiceComplete = FALSE;

	/* the distributor task sent a message to OIT. now we must decide where to go. */
	switch (ucMsgId)
	{
	case OIT_INFRA_UPLOAD_XFER_STATUS:
		usNext = fnProcessUploadXferStatusMsg(bNumNext, pNextScreens, pIntertask);
		break;
	
	case OIT_INFRA_DOWNLOAD_XFER_STATUS:
		usNext = fnProcessDownloadXferStatusMsg (bNumNext, pNextScreens, pIntertask);
		break;

	case OIT_START_SERVICE_REQ:
		usNext = fnProcessStartServiceRequest (bNumNext, pNextScreens, pIntertask);
		break;

	case OIT_ALL_SERVICES_COMPLETE:
		usNext = fnProcessAllServicesCompleteMsg (bNumNext, pNextScreens, pIntertask);
		break;

	case OIT_SERVICE_COMPLETE:
		usNext = fnProcessServiceCompleteMsg (bNumNext, pNextScreens, pIntertask);
		break;
    case OIT_INFRA_REFILL_SERV_STATUS:
		usNext = fnProcessRefillServiceStatusMsg (bNumNext, pNextScreens, pIntertask);
		break;    

	default:
		/* OIT/DIST messageing really screwed up. */
		usNext = 0;
		break;

	} // switch (ucMsgId)

	return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnevInfraTimeoutClockTick
//
// DESCRIPTION: 
// 		Event function that counts down from the Infrastructure	screen timeout 
//		value and create time out event when it expires.
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Derek DeGennaro	Initial version
//	03/28/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
T_SCREEN_ID fnevInfraTimeoutClockTick (UINT8 			bNumNext, 
									   T_SCREEN_ID  *	pNextScreens, 
									   INTERTASK 	  *	pIntertask)
{	
	if (fOITInfraScrnTimeoutEnabled == TRUE)
	{
		/* Has the screen timed out? */	
		if (lOITInfraScrnTimeoutTicks > 0)
		{
			/* The screen has NOT timed out. Decrement the timeout value. */
			lOITInfraScrnTimeoutTicks--;
		}
		else
		{
			if (fOITInfraScrnTimeoutExpired == FALSE)
			{
				/* Set the expiration flag so that only one message is sent 
					to the OIT. */
				fOITInfraScrnTimeoutExpired = TRUE;
				fnProcessEvent (EVENT_INFRA_SCREEN_TIMEOUT, pIntertask);
			}
		}
	}

	return 0;
}



/* *************************************************************************
// FUNCTION NAME: 
//		fnevRefillSuccessfulTimeout
//
// DESCRIPTION: 
// 		Event function when timer in RefillSuccessful screen expired,
//		create psudo softkey4 "Continue" to continue processing
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Always to next screen 1
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	07/12/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevRefillSuccessfulTimeout (UINT8 		bNumNext, 
										 T_SCREEN_ID  *	pNextScreens, 
										 INTERTASK   *	pIntertask)
{
//	fnhkNotPrintRefillReceipt (0, bNumNext, pNextScreens);
	
	return (pNextScreens[0]);
}




/* Variable graphic functions */



/**********************************************************************
		Private Functions
**********************************************************************/


/* *************************************************************************
// FUNCTION NAME: 
//		fnc1ContactPB
//
// DESCRIPTION: 
// 		Continuation function that sets up Infrastructure stuff.
//
// INPUTS:
//		Standard screen pre function inputs.
//
// OUTPUTS:
//		*pNextScr = 0
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Derek DeGennaro	Initial version
//	1/03/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
static SINT8 fnc1ContactPB( void 		(** pContinuationFcn)(), 
	  			  		    UINT32 	  	*	pMsgsAwaited,
			  			    T_SCREEN_ID 	*	pNextScr )
{
	*pNextScr = 0;

    // check for possible DLA missing file request
	(void)fnValidateExtFiles();
    
	(void)OSSendIntertask (DISTTASK, OIT, CONNECT_TO_ATT, NO_DATA, NULL, 0);

	iOITDistrLoopCount = 0;
//	fOITAbortModemConnection = FALSE;
	eInfraContactPBState = INFRA_CONTACT_PB_DIALING;
	fOITMeterMoved = FALSE;

	return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnc1PostContactPB
//
// DESCRIPTION: 
// 		Continuation function that give up the uart port for modem.
//
// INPUTS:
//		Standard screen pre function inputs.
//
// OUTPUTS:
//		*pNextScr, depends on the conditions checking
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/23/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
static SINT8 fnc1PostContactPB( void 		(** pContinuationFcn)(), 
		  			  		    UINT32 	  	*	pMsgsAwaited,
				  			    T_SCREEN_ID 	*	pNextScr )
{
	/* do nothing here */
	return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnProcessUploadXferStatusMsg
//
// DESCRIPTION: 
//  	Funtion to process the message OIT_INFRA_UPLOAD_XFER_STATUS
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None
//
// RETURNS:
//		
//
// WARNINGS/NOTES:
// 		Next screens: See fnevProcessDistrMsg()
//
// MODIFICATION HISTORY:
//	09/29/2012	Bob Li			Updated this function for implementing Enh Req 213340.
//  07/16/2008  Raymond Shen    Adapted for FP
//              Zhang Yingbo    Initial version
// *************************************************************************/
static T_SCREEN_ID fnProcessUploadXferStatusMsg (UINT8 			bNumNext, 
											    T_SCREEN_ID  *	pNextScreens, 
											    INTERTASK 	*	pIntertask)
{
    T_SCREEN_ID usNext = 0;
    BOOL    fEnabled;
    UINT8   ucStatus;
    UINT8   ucIndex;

    /*  If WebVisData Upload Service msg from Distr Task handler,  */
/*
    if( IsAbAccountingUploadEnabled() == TRUE)
    {
        short *pMsgData16;
        short wDataUploadStatus = 0;
        short wMsgDataA = 0; Sent counts/Complete Status
        short wMsgDataB = 0; Send counts

        pMsgData16 = (short *)&(pIntertask->IntertaskUnion.bByteData[0]);

        wDataUploadStatus = *(pMsgData16 + 1);
        wMsgDataA = *(pMsgData16 + 2);
        wMsgDataB = *(pMsgData16 + 3);

         Data Upload Service Start Req from Distr Task
        if(wDataUploadStatus == OIT_INFRA_UPLOAD_START)
        {
            reset the counts to zero
            sOITTotalDataUploadCounts = 0;
            sOITSentDataUploadCounts = 0; 
                
             Check if the total counts greater than zero,
               then put up the new account data upload prompt screen
            if( usOITCurrentDisplayService == DATA_UPLOAD_SERVICE)
            {
                if((wMsgDataA > 0)//Data0 is the pending(total) counts in START message
                && (ucOITCurrentService != DATA_UPLOAD_SERVICE) )                      
                {
#if 0	//Don't prompt user if updating the account data for implementing Enh Req 213340.
                    if(fnOITGetCurrentOnlineServType() == OI_ONLINE_SERV_AUTO)
                    {
                        usNext = pNextScreens[9];//GetScreen(ACCOUNT_DATA_UPLOAD_PROMPT);
                        return usNext;
                    }
                    else//Manual Data Upload, Send a reponse to AccountDataUpload service to bypass the prompt screen
#endif
                    {
                        UINT8 pMsgData[2];

                         Set the message data to be sent
                        pMsgData[0] = TRUE;
                        pMsgData[1] = 0;
                
                         Response to the start account data upload message
                        (void)OSSendIntertask(DISTTASK, OIT, DATA_UPLOAD_SERVICE, BYTE_DATA,
                                          pMsgData, sizeof(pMsgData));
                        
                    }
                } 
                else//No Data to upload, Do NOT set the bDataUploadStarted flag.
                {
                     UINT8 pMsgData[2];

                      Set the message data to be sent
                     pMsgData[0] = FALSE;
                     pMsgData[1] = 0;
                
                      Response to the start account data upload message
                     (void)OSSendIntertask(DISTTASK, OIT, DATA_UPLOAD_SERVICE, BYTE_DATA,
                                          pMsgData, sizeof(pMsgData));                  
                 }
            }              
            else if( (usOITCurrentDisplayService == DCAP_UPLOAD_SERVICE)
                       ||(usOITCurrentDisplayService == CONFIRMATION_SERVICE) )
            {   
                  bDataUploadInProgress = TRUE; Help to show the Caption of InfraProccessing
            }
		}

         Data Upload Service In Progress
        else if(wDataUploadStatus == OIT_INFRA_UPLOAD_INPROGRESS)
        {                
            if(usOITCurrentDisplayService == DATA_UPLOAD_SERVICE)
            {    
                  bDataUploadInProgress= TRUE; Help to show the Caption of InfraProccessing
                    
                  Get the Counts
                  sOITSentDataUploadCounts = wMsgDataA;
                  sOITTotalDataUploadCounts = wMsgDataB; 
            }
                
            if(usOITCurrentDisplayService == DCAP_UPLOAD_SERVICE)
            {   
                  Do NOT set the counts for now
                 sOITSentDataUploadCounts = 0;
                 sOITTotalDataUploadCounts = 0; 
            }
                
            if(usOITCurrentDisplayService == CONFIRMATION_SERVICE)
            {  Do NOT set the counts for now
                 sOITSentDataUploadCounts = 0;
                 sOITTotalDataUploadCounts = 0; 
            }               
        }

         Data Upload Service Complete
        else if(wDataUploadStatus == OIT_INFRA_UPLOAD_COMPLETE)
        {
            Go to the new Data Upload Complete Screen to see a summary
                
             OIT_INFRA_UPLOAD_NOT            0     Nothing Uploaded
               OIT_INFRA_UPLOAD_SUCCESS        1     Successful Upload
               OIT_INFRA_UPLOAD_FAILED         2     Upload Failed - Bad Data
               OIT_INFRA_UPLOAD_FAILED_MODE_1  3     Upload Failed - Invalid Meter Info
               OIT_INFRA_UPLOAD_FAILED_MODE_2  4     Upload Failed - Server Not Responding
               UINT16 sResult = wMsgDataA;//Generic Result
               UINT16 sCode = wMsgDataB;//Failed Code 0x8ABC(Internal: 1E20ABCD)/0x0ABC(Ext:1E21ABCD)
               UINT16 sUpperCode = 0;//Upper Word of Error Code                
               lDataUploadErrCode = 0;//Reset Error Code to 0

               if(usOITCurrentDisplayService == DATA_UPLOAD_SERVICE)                    
               {
                     Set the Generic Result to Global Flag
                    fStrAcctUploaded = sResult;

                    if( (fStrAcctUploaded == OIT_INFRA_UPLOAD_FAILED)
                        || (fStrAcctUploaded == OIT_INFRA_UPLOAD_FAILED_MODE_1)
                        || (fStrAcctUploaded == OIT_INFRA_UPLOAD_FAILED_MODE_2))
                    {
                        sUpperCode = (ERROR_CLASS_ABACUS << 8);//Abacus Error Type

                         check the Error Type Bit
                        if(sCode & WV_INTERNAL_ERROR)
                        {
                            sUpperCode += ABACUS_ACCT_UPLOAD_INT_ERROR;

                            sCode &= 0x0FFF;//Leave the last 3 Hex
                        }
                        else External Error
                        {
                            sUpperCode += ABACUS_ACCT_UPLOAD_EXT_ERROR;
                        }

                         Done with the error code setup
                        lDataUploadErrCode = (sUpperCode << 16) + sCode; 
                  }
            }

            else if(usOITCurrentDisplayService == DCAP_UPLOAD_SERVICE)               
            {
                 Set the Generic Result to Global Flag
                if(sResult == OIT_INFRA_UPLOAD_FAILED)
                {
                    fStrDCapUploaded = OIT_INFRA_UPLOAD_FAILED;check this then show the err on 'complete' msg
                }                    
                else
                {
                     SET the string flags for 'upload complete screen'
                    fStrDCapUploaded = OIT_INFRA_UPLOAD_SUCCESS;
                }                    
            }   

            else if(usOITCurrentDisplayService == CONFIRMATION_SERVICE)                  
            {
                 Set the Generic Result to Global Flag
                if(sResult == OIT_INFRA_UPLOAD_FAILED)
                {
                    fStrEServiceUploaded = OIT_INFRA_UPLOAD_FAILED;check this then show the err on 'complete' msg
                }                    
                else
                {
                     SET the string flags for 'upload complete screen'
                    fStrEServiceUploaded = OIT_INFRA_UPLOAD_SUCCESS;
                } 
            } 
        }

        else
        {
             Wrong Message from DataUpload Service, need to do something here
             OIT/DIST messagin really screwed up.
            fnDisplayErrorDirect("OIT received bad Distr Msg on Data Upload Service",0,OIT);
        }           
    }
*/
    
    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnProcessDownloadXferStatusMsg
//
// DESCRIPTION: 
//  	Funtion to process the message OIT_INFRA_DOWNLOAD_XFER_STATUS
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
// 		Next screens: See fnevProcessDistrMsg()
//
// MODIFICATION HISTORY:
// 				Derek DeGennaro		Initial version
//	1/23/2006	Vincent Yi			Code cleanup
//  01/08/2007  Adam Liu            Modified the calc of download remain time
//
// *************************************************************************/
static T_SCREEN_ID fnProcessDownloadXferStatusMsg (UINT8 			bNumNext, 
												   T_SCREEN_ID  *	pNextScreens, 
												   INTERTASK 	*	pIntertask)
{
	if (ucOITCurrentService == SWDOWNLOAD_SERVICE 	|| 
		ucOITCurrentService == RATESDOWNLOAD_SERVICE)
	{
		/* update the current file number that is being downloaded */
		bOITInfraCurrentFileToDownload = (UINT8)pIntertask->IntertaskUnion.lwLongData[0];
		
		if(bOITInfraCurrentFileToDownload > bOITInfraTotalFilesToDownload)
		{
			bOITInfraCurrentFileToDownload = bOITInfraTotalFilesToDownload; 
		}

		/* update the number of bytes we have downloaded */
		ulOITInfraDownloadedBytes = pIntertask->IntertaskUnion.lwLongData[1];

		/* re-estimate the download remain time */
		if ((0 != ulOITInfraDownloadedBytes) && 
            (ulOITInfraDownloadedBytes <= ulOITInfraTotalBytesToDownload))
		{
		    ulDownloadEstimateRemainTime = 
	    		((ulOITInfraTotalBytesToDownload - ulOITInfraDownloadedBytes) / DLA_CONCT_SPEED) +		
				((bOITInfraTotalFilesToDownload - bOITInfraCurrentFileToDownload) * DLA_TIME_TO_CONNECT_DLA_SERVER) +
				DLA_TIME_TO_SEND_CONFIRM;
		}
		else
		{
		    ulDownloadEstimateRemainTime = 0;
		}
	}

	return 0;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnProcessStartServiceRequest
//
// DESCRIPTION: 
//  	Funtion to process the message OIT_START_SERVICE_REQ
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None
//
// RETURNS:
//		
//
// WARNINGS/NOTES:
// 		Next screens: See fnevProcessDistrMsg()
//
// MODIFICATION HISTORY:
// 				Derek DeGennaro		Initial version
//	1/23/2006	Vincent Yi			Code cleanup
//
// *************************************************************************/
static T_SCREEN_ID fnProcessStartServiceRequest (UINT8 			bNumNext, 
												 T_SCREEN_ID  *	pNextScreens, 
												 INTERTASK 	*	pIntertask)
{
	T_SCREEN_ID	usNext = 0;
	UINT8		bDoService = DONT_DO_IT;
	SINT32		ulTimeDiff = 0;
	BOOL 		fDateHasPassed = FALSE;
	DATETIME 	stNow;


//	fOITPerformedOnlineUpdate = FALSE;
	switch (ucOITCurrentService)
	{
	case ACCTBAL_SERVICE:
        fnLogBalanceInquiryTemp();
        fnLogBalanceInquiryCommit();
        //lint continue...
		
	case REFILL_SERVICE:
	case REFUND_SERVICE:
	case DCAP_UPLOAD_SERVICE:
	case CONFIRMATION_SERVICE:
	case METER_MOVE_SERVICE:
		if (ucOITCurrentService == REFUND_SERVICE)
		{
			// Before starting refund service, save current fund balance for
			// displaying withdraw amount after refund successful.
//			fnSaveFundBalanceBeforeRefund();
		}
		bDoService = DO_IT;
		(void)OSSendIntertask( DISTTASK, OIT, (UINT8)OIT_START_SERVICE_REPLY, 
								BYTE_DATA, &bDoService, 1 );
		break;

	case SWDOWNLOAD_SERVICE:
	case RATESDOWNLOAD_SERVICE:
		/* clear the bytes transferred */
		// ulOITInfraDownloadedBytes = 0;

		switch (ucOITCurrentServicePriority)
		{
		case MANDATORY_SERVICE:
			/* An udpate is required, set the cmos flag so the condition is 
			persistent and go to InfraContactPB screen to wait further 
			information from download task. The user MUST perform this update. */
			fCMOSInfraUpdateReq = TRUE;
			(void) fnGetUpdateNow (ET_CALL_HOME, 0, 0);	// stay in InfraContactPB screen
			break;

		case MANDATORY_BY_DATE:
			/* An update is required by a certain date. */
			fDateHasPassed = FALSE;

			// get the current time
			if (GetSysDateTime(&stNow, NOOFFSETS, GREGORIAN))
			{
				// check to see if the date it is due has passed.
				if (fOITCurrentServiceTimeSet == TRUE && 
					CalcDateTimeDifference(&stNow,&rOITCurrentServiceDateTime,
											&ulTimeDiff))
				{
					if (ulTimeDiff <= 0)
					{
						fDateHasPassed = TRUE;
					}
				}
			}
			if (fDateHasPassed)
			{
				/* If the update is required by a date that has	already passed 
				set the flag in CMOS so that the condition is persistent and 
				go to InfraContactPB screen to wait further information from 
				download task. The user MUST perform this update.*/
				fCMOSInfraUpdateReq = TRUE;
				(void) fnGetUpdateNow (ET_CALL_HOME, 0, 0);	// stay in InfraContactPB screen
			}
			else
			{
				/* The update is required by a certain date. The
				user can do it, schedule it for later, or not 
				do it. So go to the update available screen. */
				usNext = pNextScreens[4]; // goto the DownloadUpdateAvailable screen.
			}
			break;

		case OPTIONAL_SERVICE:
			/* The update is optional. The
			user can do it, schedule it for later, or not 
			do it. So go to the update available screen. */
			usNext = pNextScreens[4];  // goto the DownloadUpdateAvailable screen.
			break;

		case USER_REQUESTED_SERVICE:
			/* The update has been requested by the user, so just do it. */
			bDoService = DO_IT;
			// stay on the InfraContactPB screen and just send the reply message.
			(void)OSSendIntertask( DISTTASK, OIT, (UINT8)OIT_START_SERVICE_REPLY, 
									BYTE_DATA, &bDoService, 1 );
			break;

		default:
			/* The priority for the update is unknown, don't do it. */
			bDoService = DONT_DO_IT;
			// stay on the InfraContactPB screen and send the reply message.
			(void)OSSendIntertask( DISTTASK, OIT, (UINT8)OIT_START_SERVICE_REPLY, 
									BYTE_DATA, &bDoService, 1 );
			break;
		} // switch (ucOITCurrentServicePriority)
		break;

	default:
		/* The Distr task told us to do a a service that we don't
		recognize so don't do it. */
		bDoService = DONT_DO_IT;
		(void)OSSendIntertask( DISTTASK, OIT, (UINT8)OIT_START_SERVICE_REPLY, 
								BYTE_DATA, &bDoService, 1 );
		break;

	} // switch (ucOITCurrentService)

	return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnProcessAllServicesCompleteMsg
//
// DESCRIPTION: 
//  	Funtion to process the message OIT_ALL_SERVICES_COMPLETE
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None
//
// RETURNS:
//		
//
// WARNINGS/NOTES:
// 		Next screens: See fnevProcessDistrMsg()
//
// MODIFICATION HISTORY:
//	07/31/2007	Joey Cui			Added code to reset inspection flag 
//	02/06/2007	Vincent Yi			Added code to handle OOB pbp timeout 
//	6/14/2006	Raymond Shen		Added OOB code
//	1/23/2006	Vincent Yi			Code cleanup
// 				Derek DeGennaro		Initial version
//
// *************************************************************************/
static T_SCREEN_ID fnProcessAllServicesCompleteMsg (UINT8 			bNumNext, 
												    T_SCREEN_ID  *	pNextScreens, 
												    INTERTASK 	*	pIntertask)
{
	T_SCREEN_ID	usNext = 0;


	/* Both the OIT and distr task are done, tell the distr task
	to kill the connection and go back to the main menu. */
	fnDisconnectDistributor ();
	
	fOITDCAPDoUnattendedUpload = FALSE;
    fnResetInspectionCheckFlags();

    // Any Balance Inquiry is done, whether it completed or not.
    fnOiSetBalanceInquiry( FALSE );

/*
	if ( fnIsInOOBMode() )
	{
		if (fnIsOOBPBPTimeout() == TRUE)
		{	// if PBP timeout in OOB mode, to connection lost screen.
			// reset the request service to PBP Balance.
			eInfraContactPBState = INFRA_CONTACT_PB_DIALING;
			ucOITRequestedService = ACCTBAL_SERVICE;
			fnSetOOBPBPTimeout (FALSE);
			usNext = GetScreen (SCREEN_INFRA_CALL_ERR);	// InfraCallErrGeneral
			return usNext;
		}
		else
		{
			usNext = fnhkOOBGotoNextScreen(0, 0, 0);
		}
	}
	else
*/
	{ 
		/* Before Go back to main screen, check the Data Upload From 
			Status and make a screen nav accordingly */
		if(fnOITGetDataUploadReqFromStatus() == OI_DATA_UPLD_FROM_TRANSLOG)
		{
			/* Clear the flag */
			fnOITSetDataUploadReqFromStatus(OI_DATA_UPLD_FROM_NONE);
                    
			usNext = GetScreen(SCREEN_ABACUS_TRANS_CLEAR_QUERY);/* goto Clear Transaction Log screen. */
		}

		else if(fnOITGetDataUploadReqFromStatus() == OI_DATA_UPLD_FROM_ACCTTYPE)
		{
			unsigned char bIndex = 0;
			T_SCREEN_ID usTempScreen;
                    
			/* Clear the flag */
			fnOITSetDataUploadReqFromStatus(OI_DATA_UPLD_FROM_NONE);                  


		}
                
		else if(fnOITGetDataUploadReqFromStatus() == OI_DATA_UPLD_FROM_ACCTDATA)
		{
			/* Clear the flag */
			fnOITSetDataUploadReqFromStatus(OI_DATA_UPLD_FROM_NONE); 

			usNext = GetScreen(SCREEN_ABACUS_RESET_ALL_QUERY);/* Continue clearing account data */
		}

		else if(fnOITGetDataUploadReqFromStatus() == OI_DATA_UPLD_FROM_ACCTEND)
		{
			/* Clear the flag */
			fnOITSetDataUploadReqFromStatus(OI_DATA_UPLD_FROM_NONE); 
                    
			usNext = GetScreen(SCREEN_ABACUS_RESET_ALL_QUERY);/* Continue clearing account Period*/            
		}

		else
		{
			usNext = pNextScreens[0];  // MMIndiciaReady screen
		}
	}

	if ( fnIsIPSDEnabled() == FALSE )
	{
		usNext = pNextScreens[0];	// MMIndiciaReady screen
	}

	return usNext;
}


/* *************************************************************************
// FUNCTION NAME: 
//		fnProcessServiceCompleteMsg
//
// DESCRIPTION: 
//  	Funtion to process the message OIT_SERVICE_COMPLETE
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None
//
// RETURNS:
//      Next Screen ID
//
// Next screens: 
//      See fnevProcessDistrMsg()
//
// WARNINGS/NOTES:
// 		Next screens: See fnevProcessDistrMsg()
//
// MODIFICATION HISTORY:
//  08/17/2009  Jingwei,Li          Show upload receipt for Canda Pkg service.
//  2009.05.14  Clarisa Bellamy Change the test for the Balance Inquiry receipt from
//                              testing for the country to testing for the existence
//                              of the report.
//  05/12/2009  Raymond Shen        Added Balance Inquiry reciept for Brazil/France.
//  08/13/2008  Joey Cui            Send complete messages for some services
//  30/7/2007   Joey Cui            Reset the barcode & tracking log 
                                    error/warning flags
//  29 Mar 2007 Bill Herring        Activated meter move processing
//  12/14/2006  Adam Liu            Add DCAP Upload complete screen branch
//  07/28/2006  Dicky Sun           Update for CONFIRMATION_SERVICE.
//                                  We should do refill and balance before
//                                  uploading record.
//  07/25/2006  Dicky Sun           Update for uploading record:
//                                    After printing receipt, reset the
//                                    service type to none.
//                                    If invalid ZIP, go to warning screen.
//  07/18/2006  Dicky Sun           Update for GTS uploading record
//  07/07/2006  Dicky Sun           Add GTS support
//	1/23/2006	Vincent Yi			Code cleanup
// 				Derek DeGennaro		Initial version
// *************************************************************************/
static T_SCREEN_ID fnProcessServiceCompleteMsg (UINT8 			bNumNext, 
											    T_SCREEN_ID  *	pNextScreens, 
											    INTERTASK 	*	pIntertask)
{
	T_SCREEN_ID	usNext = 0;
	BOOL 		fEnabled;
	UINT8 		ucStatus;
	UINT8 		ucIndex;


    // If service was an Account Balance, its over now.
    fnOiSetBalanceInquiry( FALSE );
    
    // if the service completes with an error, hang up the phone here
	if (pIntertask->IntertaskUnion.lwLongData[0] != 0)
	{
		// send a reply before hanging up the phone
		(void)OSSendIntertask( DISTTASK, OIT, (UINT8)OIT_SERVICE_COMPLETE_REPLY, 
								NO_DATA, NULL, 0 );
		
		fnDisconnectDistributor();

//		fnProcessEvent(EVENT_DC_DIALOGUE_ERR, pIntertask); // Connection Lost
		return usNext;
	}

	if (ucOITCurrentService == SWDOWNLOAD_SERVICE || 
		ucOITCurrentService == RATESDOWNLOAD_SERVICE)
	{
		fCMOSInfraUpdateReq = FALSE;
		
		// This is for fixing the FRACA 15023.
		// After software download, call this function to clear the DSR state to force
		// platform to get the scale version again when OIT activates the port.
		fnClearDSRState();

	}

	// Clear requested service flag
	ucOITRequestedService = INFRA_REQ_SERVICE_NONE;
			
    switch (ucOITCurrentService)
    {
		case ACCTBAL_SERVICE:
//			fDialingServiceComplete = TRUE;
            // If this PCN has a BalanceInquiryReceipt report, then go to the
            //  screen that asks the user if they want to print a receipt.
            if (( fnFlashGetReport( BALANCE_INQ_RECEIPT_RPT ) != NULL_PTR ) &&
				(bNumNext > 14))
            {
                usNext = pNextScreens[14];	// goto PBP Balance Inquiry Complete screen
            }
            else if(usOITCurrentDisplayService == DATA_UPLOAD_SERVICE)
            {
                fnFnishDataUpload(&usNext);
            }
            else
            {
				usNext = pNextScreens[2];	// goto PBP Account Balance screen
            }
			break;

		case REFILL_SERVICE:
//			fDialingServiceComplete = TRUE; 
                if(usOITCurrentDisplayService == DATA_UPLOAD_SERVICE)
                {
                    fnFnishDataUpload(&usNext);
                }
                else
                {
			usNext = pNextScreens[1];	// goto Refill Successful screen
                }
			break;

		case SWDOWNLOAD_SERVICE:
		case RATESDOWNLOAD_SERVICE:		// Actually, no RATESDOWNLOAD_SERVICE
										// sent from distributor
//			fDialingServiceComplete = TRUE;
			usNext = pNextScreens[5];	// goto DownloadComplete screen
			break;

		case REFUND_SERVICE:
//			fDialingServiceComplete = TRUE;
                if(usOITCurrentDisplayService == DATA_UPLOAD_SERVICE)
                {
                    fnFnishDataUpload(&usNext);
                }
                else
                {
			usNext = pNextScreens[3];	// goto Withdraw successful screen
                }
			break;

		case DCAP_UPLOAD_SERVICE:
                if(usOITCurrentDisplayService == DCAP_UPLOAD_SERVICE)
                {
                    (void)OSSendIntertask( DISTTASK, OIT, (unsigned char)OIT_SERVICE_COMPLETE_REPLY, NO_DATA, NULL, 0 );
                    usNext = GetScreen(SCREEN_INFRA_CONTACT_PB); 
                }
                else if(usOITCurrentDisplayService == DATA_UPLOAD_SERVICE)
                {
                    fnFnishDataUpload(&usNext);
                }
                else
                {

//			fDialingServiceComplete = TRUE;
         	    usNext = pNextScreens[8];	// Data Capture Upload Complete
                }
			break;

		case METER_MOVE_SERVICE:
			if (fnPBPWasMeterMoved() == TRUE)
			{
				(void)OSSendIntertask( DISTTASK, OIT, (UINT8)OIT_SERVICE_COMPLETE_REPLY, 
										NO_DATA, NULL, 0 );

				// set flag so that meter moved screen is shown. 
				fOITMeterMoved = TRUE;		
			}
			else
			{
				// do not display meter move after the not scheduled for move 
				// screen is shown
//				fDialingServiceComplete = TRUE; 

				//ulOITCurrentServiceResult = PBPS_METER_REFUND_NOT_ALLOWED;

				// goto meter move error screen. 
				usNext = GetScreen(SCREEN_METER_MOVE_ERROR);   
			}
			break;

		default :
                    if(usOITCurrentDisplayService == DATA_UPLOAD_SERVICE)
                    {
                        fnFnishDataUpload(&usNext);
                    }
                    else
                    {
			/* the reply message must be sent, otherwise the distributor task 
				will result in an task exception.*/
			(void)OSSendIntertask( DISTTASK, OIT, (UINT8)OIT_SERVICE_COMPLETE_REPLY, 
									NO_DATA, NULL, 0 );
                    }
			break;

	} // switch (ucOITCurrentService)
 

	return usNext;
}
/* *************************************************************************
// FUNCTION NAME: 
//		fnProcessRefillServiceStatusMsg
//
// DESCRIPTION: 
//  	Funtion to process the message OIT_INFRA_REFILL_SERVICE_STATUS
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None
//
// RETURNS:
//		
//
// WARNINGS/NOTES:
// 		Next screens: See fnevProcessDistrMsg()
//
// MODIFICATION HISTORY:
//  06/25/2010  Jingwei,Li    Initial version
//              
// *************************************************************************/
static T_SCREEN_ID fnProcessRefillServiceStatusMsg (UINT8 		    bNumNext, 
                                                    T_SCREEN_ID  *	pNextScreens, 
                                                    INTERTASK 	*	pIntertask)
{
    UINT16 usNext = 0;
    short *pMsgData16;
    short wRefillStatus = 0;

    pMsgData16 = (short *)&(pIntertask->IntertaskUnion.bByteData[0]);

    wRefillStatus = *(pMsgData16 + 1);
    if( wRefillStatus = OIT_INFRA_REFILL_START)
    {
         ucOITCurrentService = REFILL_SERVICE;
         fnOITSetRequestedService(REFILL_SERVICE);
    }
    return usNext;
    
}
/**************************************************************************
// FUNCTION NAME: 
//      fnValidateUnicodePortNum
//
// DESCRIPTION: 
//
// INPUTS:
//
// RETURNS:     
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//
// *************************************************************************/
static UINT8 fnValidateUnicodePortNum(UINT16 *pValue, 
                                      UINT8   bBuffLen, 
                                      UINT8   bMaxLen )
{
    UINT8  bStatus = DISTR_PARAM_STATUS_ERR;
	SINT32 i;
    UINT32 lwPortNum = 0;
    UINT32 lwTemp = 0;

    if (bBuffLen == 0)
	{
        bStatus = DISTR_PARAM_STATUS_KEEP_GOING;
	}
    else if (pValue != NULL && bBuffLen <= bMaxLen)
    {
        for (i = 0; i < (SINT32)bBuffLen && isdigit(pValue[i]); i++)
        {
            lwTemp = ((lwPortNum * 10) + (pValue[i] - 0x30));
            if (lwTemp < 65536)
			{
                lwPortNum = lwTemp;
			}
            else
			{
                break;
			}
        }

        /* if we got to the end,  the port is ok */
        if (i == bBuffLen)
        {
            bStatus = DISTR_PARAM_STATUS_OK;
            if (i < bMaxLen)
			{
                bStatus |= DISTR_PARAM_STATUS_KEEP_GOING;
			}
        }
    }

    return bStatus;
}



/**************************************************************************
// FUNCTION NAME: 
//      fnValidateUnicodeIP
//
// DESCRIPTION: 
//
// INPUTS:
//
// RETURNS:     
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
// 11/08/2012  Bob Li    Updated to avoid invalid IP entry.
// *************************************************************************/
static UINT8 fnValidateUnicodeIP(UINT16 *pValue, 
                                 UINT8   bBuffLen, 
                                 UINT8   bMaxLen)
{
    UINT8  bStatus = DISTR_PARAM_STATUS_OK;
    BOOL   fContinue = TRUE;
    UINT8  bIpNum[4] = {0,0,0,0};
    UINT8  bBytesParsed = 0;
    SINT32 iIpBytes = 0;
    UINT32 lwCurr = 0;
    UINT32 lwTemp = 0;

    if (pValue != NULL && bBuffLen <= bMaxLen)
    {
        while (fContinue && iIpBytes < 4)
        {
            lwCurr = 0;
            if (bBuffLen != 0 && bBytesParsed >= bBuffLen)
            {
                /* we are done processing the buffer */
                fContinue = FALSE;
            }           
            else
            {
                while(fContinue == TRUE && bBytesParsed < bBuffLen && isdigit(*pValue))
                {
                    lwTemp = ((lwCurr * 10) + (*pValue - 0x30));
                    if (lwTemp < 256)
                    {
                        /* the byte is in range, keep it */
                        lwCurr = lwTemp;
                        pValue++;
                        bBytesParsed++;
                    }
                    else
                    {
                        /* this byte was > 255 */
                        bStatus = DISTR_PARAM_STATUS_ERR;
                        fContinue = FALSE;
                    }
                }
                if (iIpBytes != 3 && bBytesParsed < bBuffLen )
                {
                    /* we are looking for a '.' */
                    if (*pValue != (UINT16)'.')
                    {
                        /* we are expecting a '.' but didn't get one */
                        bStatus = DISTR_PARAM_STATUS_ERR;
                        fContinue = FALSE;
                    }
                    else 
                    {
                        /* skip the '.' */
                        pValue++;
                        bBytesParsed++;
                        if (bBytesParsed < bBuffLen && !isdigit(*pValue))
                        {
                            /* a digit must follow the '.' or it is an error */
                            bStatus = DISTR_PARAM_STATUS_ERR;
                            fContinue = FALSE;
                        }
                    }
                }

                /* store this byte in the buffer */
                if (bStatus == DISTR_PARAM_STATUS_OK && iIpBytes < 4)
				{
                    bIpNum[iIpBytes++] = (UINT8)lwCurr;
				}
            }
        }
		if(iIpBytes == 4 && *pValue ==(UINT16)'.')
		{
			bStatus = DISTR_PARAM_STATUS_ERR;
			fContinue = FALSE;			
		}
    }
    else if (pValue != NULL && bBuffLen > bMaxLen)
    {
        /* The length is greater than the max. */
        bStatus = DISTR_PARAM_STATUS_ERR;
        fContinue = FALSE;
    }

    if (bBuffLen == 0)
	{
        bStatus = DISTR_PARAM_STATUS_KEEP_GOING;
	}
    else if (iIpBytes == 4)
    {
        if (bStatus == DISTR_PARAM_STATUS_OK)
        {
            bStatus = DISTR_PARAM_STATUS_OK;
            if (bIpNum[3] != 255)
			{
                bStatus |= DISTR_PARAM_STATUS_KEEP_GOING;
			}
        }
        else
		{
            bStatus = DISTR_PARAM_STATUS_ERR;
		}
    }
    else
    {           
        if (bStatus == DISTR_PARAM_STATUS_OK)
		{
            /* there were less than 4 bytes and we can still keep goin */
            bStatus = DISTR_PARAM_STATUS_KEEP_GOING;
		}
        else
		{
            /* there were less than 4 bytes cause there was an error */
            bStatus = DISTR_PARAM_STATUS_ERR;
		}
    }

    return bStatus;
}



/**************************************************************************
// FUNCTION NAME: fnIsDistrParamOK
//
// DESCRIPTION: 
//
// INPUTS:
//
// RETURNS:     
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//
// *************************************************************************/
static UINT8 fnIsDistrParamOK(void)
{
    UINT8  bStatus = DISTR_PARAM_STATUS_OK;
    UINT8  bBuffLen;
    UINT8   ubWritingDir;
    UINT8   i = 0;    

    ubWritingDir = fnGetWritingDir();
    if(ubWritingDir == RIGHT_TO_LEFT )
    {
        while(rGenPurposeEbuf.pBuf[i]== UNICODE_SPACE)
        {
            i++;
        }
    }
    bBuffLen = fnCharsBuffered();

    if (bBuffLen == 0)
	{
        bStatus = (DISTR_PARAM_STATUS_KEEP_GOING | DISTR_PARAM_STATUS_OK);
	}
    else if (bBuffLen > 0)
    {
        if (oiDistrParamInfo[bCurrDistrParam].lwFlags & DISTR_PARAM_INFO_IP)
        {
            bStatus = fnValidateUnicodeIP(&rGenPurposeEbuf.pBuf[i], bBuffLen,
                                          oiDistrParamInfo[bCurrDistrParam].bMaxLen);
        }
        else if (oiDistrParamInfo[bCurrDistrParam].lwFlags & DISTR_PARAM_INFO_PORT)
		{
            bStatus = fnValidateUnicodePortNum(&rGenPurposeEbuf.pBuf[i], bBuffLen,
                                               oiDistrParamInfo[bCurrDistrParam].bMaxLen);
	    }
        else if ((oiDistrParamInfo[bCurrDistrParam].lwFlags & DISTR_PARAM_INFO_TEXT) ||
                (oiDistrParamInfo[bCurrDistrParam].lwFlags & DISTR_PARAM_INFO_PASSWORD))
        {           
            bStatus = DISTR_PARAM_STATUS_OK;
            if (bBuffLen <= oiDistrParamInfo[bCurrDistrParam].bMaxLen)
			{
                bStatus |= DISTR_PARAM_STATUS_KEEP_GOING;
			}
        }
    }

    return bStatus;
}



/**************************************************************************
// FUNCTION NAME: fnSMenuDistrParamShown
//
// DESCRIPTION: 
//      utility function that is used to output the distributor parameters  
//      in the field by field functions according to ucIndex 
//
// INPUTS:
//      Standard field input
//      usItemIndex - the item index number;
//
// RETURNS:     
//       SUCCESSFUL
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//      02/15/2006      Kan Jiang       Initial version
//      05/22/2006      James Wang      Change it to be a more common function.
//
// *************************************************************************/
static BOOL fnSMenuDistrParamShown ( UINT16 *pFieldDest, 
                                     UINT8   bLen, 
                                     UINT8   bFieldCtrl, 
                                     UINT8   bNumTableItems, 
                                     UINT8  *pTableTextIDs, 
                                     UINT8  *pAttributes,
                                     UINT16  usItemIndex)
{
    UINT8   bStrLen = 0;
    char  *pString = NULL;
    UINT8   i = 0;
    UINT16  usIndex;

    usIndex = fnGetSMenuTable()->usFirstDisplayedSlotIndex + usItemIndex;

    (void)fnfStdEntryInit(pFieldDest,bLen,bFieldCtrl,bNumTableItems,
                          pTableTextIDs,pAttributes);

    if (usIndex >= DISTR_PARAM_PBP_BACKUP_URL)
    {
       return (SUCCESSFUL);
    }
     
    pString = oiDistrParamInfo[usIndex].pStr;
  
    bStrLen = strlen(pString);
    if (bStrLen > bLen)
    {
       bStrLen = bLen;
    }
    
    if (usIndex == DISTR_PARAM_ACCOUNT_PASSWORD)
    {
        for (i = 0; i < bStrLen; i++)
        {
            fnBufferUnicode((UINT16)'*');               
        }
    }     
    else
    {
	    while (i < bStrLen)
	    {
	        //lint -e571 this cast would not cause error
		    fnBufferUnicode((UINT16)pString[i++]);
		    //lint +e571
	    }
    }
             
    return(fnfStdEntryRefresh(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                              pTableTextIDs, pAttributes));
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnGetUpdateNow
//
// DESCRIPTION: 
//  	Funtion to do update
//
// INPUTS:
//
//
// OUTPUTS:
//		None
//
// RETURNS:
//		None
//
// WARNINGS/NOTES:
// 		None
//
// MODIFICATION HISTORY:
// 08/01/2008   Raymond Shen    Replaced SWDOWNLOAD_SERVICE with ET_CALL_HOME to  
//                              avoid doing Budget account upload while downloading.
//	04/10/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnGetUpdateNow (UINT8 bServiceCode, INT32 value, INT32 value2)
{
	BOOL msgStat;
	UINT8 	ucServiceMsg[MAX_INTERTASK_BYTES] = {0};

//TODO The OIT state change in the following 2 lines should not cause problems but this needs to be confirmed
	eInfraContactPBState = INFRA_CONTACT_PB_REQUESTING;
	fnOITSetRequestedService (bServiceCode);

	ucServiceMsg[0] = bServiceCode;
	memcpy(&ucServiceMsg[1], &value, sizeof(INT32));
	memcpy(&ucServiceMsg[1+sizeof(INT32)], &value2, sizeof(INT32));

    if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "--> OSSendIntertask DISTTASK [%d]\r\n", bServiceCode);
	msgStat = OSSendIntertask( DISTTASK, OIT, (unsigned char)OIT_CLIENT_SERVICE_REQ,
							BYTE_DATA, ucServiceMsg, MAX_INTERTASK_BYTES );
	return msgStat;
}

/* *************************************************************************
// FUNCTION NAME: fnhSRefillPostage
//
// DESCRIPTION:
//      Pseudo hard key function that called by hard 
//      key function fnhkRefillPostage().
//
// INPUTS:
//      Standard hard key function inputs(format 2)
//
// RETURNS:
//      Next screen
//
// WARNINGS/NOTES:  
//      NONE.	
//
// MODIFICATION HISTORY:
//  02/29/2008	Raymond Shen		Initial version
//
// *************************************************************************/
static T_SCREEN_ID fnhSRefillPostage(UINT8          bKeyCode, 
                                 UINT8          bNumNext, 
                                 T_SCREEN_ID *  pNextScreens)
{
	
    return (pNextScreens[0]);
}

/* *************************************************************************
// FUNCTION NAME: fnIsDataCenterUploadAvail
//
// DESCRIPTION:
//      Check to see if Data Center Upload is available
//
// INPUTS:
//
// RETURNS: TRUE: either of the data(AD/UD/CSR) is available for upload
//         FALSE: none of data is available
//
// NOTES:
//
// MODIFICATION HISTORY:
//       Zhang Yingbo  Initial version
//
// *************************************************************************/
BOOL fnIsDataCenterUploadAvail(void)
{
     BOOL retval = FALSE;     
     SINT16  sAbacusStatus;
     UINT16  usDetailAbacusStatus;
     BOOL fOpenXactionAvail = FALSE;

     return retval;
}

/* *************************************************************************
// FUNCTION NAME:  fnfDataUploadLogNumCommon
//
// DESCRIPTION:
//      Utility output field function that displays data upload record's 
//      number in AbacusDataUploadsSummary screen.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:
//
//
// NOTES:
//      None
//
// MODIFICATION HISTORY:
//  07/25/2008  Raymond Shen    Adapted for FPHX
//              Zhang Yingbo    Initial version
//
// *************************************************************************/
static BOOL fnfDataUploadLogNumCommon( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes,
                                UINT16  usSlotIndex)
{
    char pOutString[4];
    INT32 iStrLen=0;
    UINT16 usTextID = 0;     
    UINT8 bUploadType = CMOSWebVisDataUploadLog[usSlotIndex].bUploadType;

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* Check Num of Table Texts */
    if (bNumTableItems < 1)
    {
        return ((BOOL) SUCCESSFUL);
    }

    /* Check if this is a valid entry */
    if( bUploadType != 0)
    {
        /* Build string 'Data Uploaded:' */    
    	EndianAwareCopy(&usTextID, pTableTextIDs, sizeof(UINT16));
        (void)fnCopyTableText((pFieldDest), usTextID, 2);      
        
         //Build Output String
        iStrLen = sprintf( pOutString, "%d.", (usSlotIndex + 1));

        //make sure the string fits in the field
       if( iStrLen <= bLen )
       {   // display string
            fnAscii2Unicode(pOutString, (pFieldDest + 1), iStrLen);
       }
    }
    
    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:  fnfDataUploadLogDate
//
// DESCRIPTION:
//      Utility output field function that displays data upload record's 
//      date in AbacusDataUploadsSummary screen.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:
//
//
// NOTES:
//      None
//
// MODIFICATION HISTORY:
//  07/25/2008  Raymond Shen    Adapted for FPHX
//              Zhang Yingbo    Initial version
//
// *************************************************************************/
static BOOL fnfDataUploadLogDate( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes,
                                UINT16  usSlotIndex)
{   
    UINT8 bUploadType = CMOSWebVisDataUploadLog[usSlotIndex].bUploadType;  
    DATETIME    dtDate;
    UINT8          bDateFormat;
    UINT16        usDDS;
    UINT16        usDateSepChar;

     /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* Check if this is a valid entry */
    if( bUploadType != 0 )
    {
        (void)fnGetDateParms(&dtDate, &bDateFormat, &usDDS, &usDateSepChar);
        bDateFormat = DATE_FORMAT_MMDDYY;

        // Hard code it for now because it seems the DateSepChar in EMD is not correct.
        usDateSepChar = usDataUploadDateSepChar;
        
        (void)fnBuildDateFmt(&(CMOSWebVisDataUploadLog[usSlotIndex].rDateTime), 
                                                    bDateFormat, usDDS, usDateSepChar, TRUE, TRUE, FALSE, TRUE,  
                                                    pFieldDest, bLen, bNumTableItems, pTableTextIDs, bFieldCtrl);
    }   

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:  fnfDataUploadLogTime
//
// DESCRIPTION:
//      Utility output field function that displays data upload record's 
//      time in AbacusDataUploadsSummary screen.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:
//
//
// NOTES:
//      None
//
// MODIFICATION HISTORY:
//  07/25/2008  Raymond Shen    Adapted for FPHX
//              Zhang Yingbo    Initial version
//
// *************************************************************************/
static BOOL fnfDataUploadLogTime( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes,
                                UINT16  usSlotIndex)
{
    UINT8 bUploadType = CMOSWebVisDataUploadLog[usSlotIndex].bUploadType;  

     /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* Check if this is a valid entry */
    if( bUploadType != 0 )
    {
        (void)fnBuildTime(CMOSWebVisDataUploadLog[usSlotIndex].rDateTime, FALSE, TRUE, pFieldDest, bLen, bNumTableItems, pTableTextIDs);
    }

    return ((BOOL) SUCCESSFUL);

}

/* *************************************************************************
// FUNCTION NAME:  fnfDataUploadLogAMPM
//
// DESCRIPTION:
//      Utility output field function that displays data upload record's 
//      AM/PM status in AbacusDataUploadsSummary screen.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:
//
//
// NOTES:
//      None
//
// MODIFICATION HISTORY:
//  07/25/2008  Raymond Shen    Adapted for FPHX
//              Zhang Yingbo    Initial version
//
// *************************************************************************/
static BOOL fnfDataUploadLogAMPM( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes,
                                UINT16  usSlotIndex)
{
    UINT8 bUploadType = CMOSWebVisDataUploadLog[usSlotIndex].bUploadType;  

      /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* Check if this is a valid entry */
    if( bUploadType != 0 )
    {
        (void)fnShowAmPm(CMOSWebVisDataUploadLog[usSlotIndex].rDateTime.bHour, pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs);
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: fnDataUploadLogSelect
//
// DESCRIPTION: 
//      Utility function that select a data upload log to see details.
//
// INPUTS:
//      slot index + Standard hard key function inputs (format 2).
//
// RETURNS:
//      go to next screen
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//  07/24/2008  Raymond Shen    Adapted for FPHX 
//              Zhang Yingbo    Initial version for Mega
// *************************************************************************/
static T_SCREEN_ID fnDataUploadLogSelect( UINT16    usIndex,
                                            UINT8   bKeyCode, 
                                            UINT8   bNumNext, 
                                            T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;
    S_MENU_TABLE *	pMenuTable = fnGetSMenuTable();
    UINT16  usSlotIndex = pMenuTable->usFirstDisplayedSlotIndex + usIndex;
    UINT8   bUploadType = CMOSWebVisDataUploadLog[usSlotIndex].bUploadType;  

    /* Check if this is a valid entry */
    if( bUploadType != 0 )
    {
        usDataUploadCurLogIndex = usSlotIndex;

         usNext = pNextScreens[0];
    }

    return(usNext);

}

void fnFnishDataUpload(T_SCREEN_ID *pNextScreens)
{
    if(fnOITGetDataUploadResult() == OI_DUPLOAD_NONE)
    {
        /* Send the service complete reply to DIST task */
        (void)OSSendIntertask( DISTTASK, OIT, (unsigned char)OIT_SERVICE_COMPLETE_REPLY, NO_DATA, NULL, 0 );

        usOITCurrentDisplayService = 0;
        
        *pNextScreens = GetScreen(SCREEN_INFRA_CONTACT_PB);/* Goto Processing.. */  
        
    }
    
    else if(fnOITGetDataUploadResult() == OI_DUPLOAD_FAILED)
    {
        *pNextScreens = GetScreen(INFRA_DATA_UPLOAD_ERROR);/* Data Upload error screen */
    }

    else
    {
        usOITCurrentDisplayService = 0;
        *pNextScreens = GetScreen(INFRA_DATA_UPLOAD_COMPLETE);/* Data Upload Complete */
    }

    return;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnIsFilesDownloadedDuringTheCall
//
// DESCRIPTION: 
// 		util function to check is file downloaded during the call
//
// INPUTS:
//		NULL
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/13/2008  Joey Cui    Merged from Janus
// *************************************************************************/
BOOL fnIsFilesDownloadedDuringTheCall()
{
	BOOL bRet=FALSE;
	if((CmosDownloadCtrlBuf.gphsDownloaded==TRUE)||(CmosDownloadCtrlBuf.swDownloaded==TRUE)||
	  (CmosDownloadCtrlBuf.rateDownloaded==TRUE)||(CmosDownloadCtrlBuf.miscDownloaded==TRUE))
		bRet=TRUE;
	return(bRet);

}



/* *************************************************************************
// FUNCTION NAME: fnGetDCErrorScr
//
// DESCRIPTION: 
//      This function check the DC errors and return the error screen.
//      The error codes (given in the iCode parameter) 
//      are listed in MM97003.
//
// INPUTS:
//      None
//
// RETURNS:
//      Screen ID to go
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY: 
//  10/03/08  Deborah Kohl : Add a call to fnBobErrorInfo, to avoid the 
//            system fault error screen 200D after the refill error screen
//              Tom Zhao        Initial version for Janus
// *************************************************************************/
T_SCREEN_ID fnGetDCErrorScr(void)
{
    UINT32    ulDCStatus = 0;
    T_SCREEN_ID NextScr = 0;
    uchar bClass, bCode;
     
    if (fnValidData( BOBAMAT0, BOBS_LAST_DCSTATUS, (void *) &ulDCStatus ) == BOB_OK)
    {
           switch(ulDCStatus)
        {
            case PBP_DC_STAT_REFILL_EXC_DES_REG_CAP:
                NextScr = GetScreen(SCRN_REFILL_ERR_EXC_MAX);
//                bFromScreenOptions = FROM_REFILL;
                // since we are not using fnProcessSCMError clear the bob error flag
                (void)fnBobErrorInfo(&bClass, &bCode);
                break;
            case PBP_DC_STAT_REFILL_EXC_AUTH_LIMIT:
                NextScr = GetScreen(SCRN_REFILL_ERR_EXC_AUTH_LIMIT);
//                bFromScreenOptions = FROM_REFILL;
                // since we are not using fnProcessSCMError clear the bob error flag
                (void)fnBobErrorInfo(&bClass, &bCode);
                break;
            case PBP_DC_STAT_REFILL_BELOW_MIN:
                NextScr = GetScreen(SCRN_REFILL_ERR_BELOW_MIN);
//                bFromScreenOptions = FROM_REFILL;
                // since we are not using fnProcessSCMError clear the bob error flag
                (void)fnBobErrorInfo(&bClass, &bCode);
                break;
            default:
                NextScr = 0;
                break;
        }
    }
    
    return NextScr;
}

/* *************************************************************************
// FUNCTION NAME: fnhkCallLostButRefillSuccess
// DESCRIPTION: On the screen of InfraCallErrGeneralRefillSuccess, get called when operator
//				hit the soft4 key.  Do a balance inquiry if NOT in OOB, Connect to DLA if in OOB
// AUTHOR: David Huang
// INPUTS:	Standard hard key function inputs.
// NEXT SCREEN:  InfraContactPB
//
// MODIFICATION HISTORY:
//      06/30/2011   Joe Zhang   Adapted from Janus 16.80 to fix double refills issue
// *************************************************************************/
unsigned short fnhkCallLostButRefillSuccess(unsigned char bKeyCode, unsigned char bNumNext, unsigned short *pNextScreens)
{
	unsigned short wNext = 0;

	if( fnIsInOOBMode() == TRUE)
	{
		ucOITRequestedService = SWDOWNLOAD_SERVICE ;
		if(bNumNext>0)
			wNext = pNextScreens[0];  // InfraConnectPB screen
	}
	return ( wNext );
}

/* *************************************************************************
// FUNCTION NAME: fnpInfraGeneralErrButRefillSuccess
// DESCRIPTION: Pre function that sends the message to distributor to disconnect
// 				from att and flag not to do refill again when go through OOB again.
//
// AUTHOR: David Huang
//
// INPUTS:	Standard pre function inputs.
// CONTINUATION FUNCTION:  none
// NEXT SCREEN:	unchanged
//
// MODIFICATION HISTORY:
//      06/30/2011   Joe Zhang   Adapted from Janus 16.80 to fix double refills issue
// *************************************************************************/
char fnpInfraGeneralErrButRefillSuccess ( void (** pContinuationFcn)(), unsigned long *pMsgsAwaited,
						 unsigned short *pNextScr )
{
	if (fOITDistrConnected == TRUE)
	{
		fnpInfraGeneralErr ( pContinuationFcn, pMsgsAwaited,pNextScr );
	}
	
	return (PREPOST_COMPLETE);
}




