
#ifdef MEMORYTEST

#include <stdio.h>
#include "framTests.h"
//#include "crc16.h"


uint8_t fram_test_pattern_str[] ="FRAM Test";

#define FRAM_PATTERN_STRING_OFFSET 		(0)
#define TEST_PATTERN_OFFSET 		((sizeof(fram_test_pattern_str) + 4))

#define VALID_FRAM_PATTERN 			0x1234

#define TEST_PREAMBLE_PATTERN 		0x00
#define TEST_PREAMBLE_VALUE 		0x0000
#define TEST_FOLLOW_PATTERN 		0x33
//#define TEST_FOLLOW_VALUE 			0x033333333
#define TEST_FOLLOW_VALUE 			0x3333
#define TEST_WRITE_OVER_PATTERN		0x77
//#define TEST_WRITE_OVER_VALUE		0x077777777
#define TEST_WRITE_OVER_VALUE		0x07777
#define TEST_FILL_ZERO  			0x00
#define TEST_FILL_ONES  			0x011
//#define TEST_END_PATTERN			0x055443322
#define TEST_END_PATTERN			0x05544

#define FRAM_WRITE_OFFSET			(TEST_PATTERN_OFFSET + 2)



//int32_t fram_fill(void* addr, uint8_t fill, int32_t size)
void fram_fill_test(memory_test_data_t *memory_test)
{
	memory_test->result = fram_fill((uint8_t*)memory_test->memory_test_req.address, memory_test->memory_test_req.value8, memory_test->memory_test_req.size);

}

void fram_write_test(memory_test_data_t *memory_test)
{

	memory_test->result = fram_fill((uint8_t*)memory_test->memory_test_req.address, memory_test->memory_test_req.value8, memory_test->memory_test_req.size);
}

void fram_erase_test(memory_test_data_t *memory_test)
{
	memory_test->result = fram_fill((void*)memory_test->memory_test_req.address, 0, memory_test->memory_test_req.size);
}


//int32_t fram_read_block(void *buf, void* addr, int32_t size)
void fram_read_test(memory_test_data_t *memory_test)
{
	memory_test->result = fram_read_block((void*)&memory_test->info, (void*)memory_test->memory_test_req.address, sizeof(memory_test->memory_test_req.value16));

	if(memory_test->memory_test_req.size > 4)
	{
		if(memory_test->memory_test_req.size > 512)
			memory_test->result = fram_read_block((void*)memory_test->buffer, (void*)memory_test->memory_test_req.address, 512);
		else
			memory_test->result = fram_read_block((void*)memory_test->buffer, (void*)memory_test->memory_test_req.address, memory_test->memory_test_req.size);
	}
}



//int32_t fram_write_16(void* addr, uint16_t value)
void fram_write_16_test(memory_test_data_t *memory_test)
{
	memory_test->result = fram_write_block((void*)memory_test->memory_test_req.address, &memory_test->memory_test_req.value16, sizeof(UINT16));
}

//int32_t fram_write_32(void* addr, uint32_t value)
void fram_write_32_test(memory_test_data_t *memory_test)
{
	memory_test->result = fram_write_block((void*)memory_test->memory_test_req.address, &memory_test->memory_test_req.value32, sizeof(UINT32));
}


/******************************************************************************/
void fram_data_retention(memory_test_data_t *memory_test)
{
	/** Return value */
	int32_t 	result = 0;
	uint16_t 	fram_pattern = 0;
	uint8_t		fram_patern_string[10];
	uint32_t 	offset;
	uint32_t	value;
	uint16_t	crc;


	/** Read in FRAM 0, check for valid test pattern */
	result = fram_read_block(fram_patern_string, FRAM_PATTERN_STRING_OFFSET, sizeof(fram_patern_string));
	if (result != 0)
	{
		memory_test->result =  0x01;
		return;
	}

	/** If written display */
	if (memcmp(fram_patern_string, fram_test_pattern_str, sizeof(fram_patern_string)) == 0)
	{

		result = fram_read_block(&fram_pattern, (void*)TEST_PATTERN_OFFSET, sizeof(fram_pattern));
		if (result != 0)
		{
			memory_test->result =  0x02;
			return;
		}



		//verify write : read and compare against CRC
		for(offset = FRAM_WRITE_OFFSET;  offset < FRAM_SIZE; offset+=2)
		{
			value =  (uint32_t)(offset);
			crc = _crc16(0, (char *)&value, sizeof(uint32_t));

			if ( *(uint16_t*)(FRAM_START + (uint32_t)value) != crc)
			{
				result = E_CB_MEMORY_RETENTION_FAILED;
				printf("FAIL: Verification Failed to offset %08lx, result %0lx.\n", value, result);
				memory_test->result =  result;
				memory_test->info = (uint32_t)value;
				return;
			}

		}
		printf("FRAM Data retention Successful.\n");

	}
	else
	{
//		printf("***************************\n");
//		printf("Pattern NOT found. Setting.\n");
//		printf("***************************\n");
		/** Check bounds checking. */
		/** First read */
		/** Fill with 0's */
		result = fram_fill((void*)0, 0x00, FRAM_SIZE);
		if (result != E_CB_NO_ERROR)
		{
			//printf("Error occured while clearing fram: 0x%X\n", result);
			memory_test->result =  0x50;
			return;
		}


		/** Otherwise write pattern into fram */
		//printf("No pattern found in FRAM, writing...\n");
		//printf("Should find pattern on next power cycle | reset | boot.\n");
		fram_pattern = VALID_FRAM_PATTERN;
		result = fram_write_block((void*)TEST_PATTERN_OFFSET, (void*)&fram_pattern, sizeof(uint16_t));
		if (result != E_CB_NO_ERROR)
		{
			//printf("Error occured while writeing FRAM 0x%X\n", result);
			memory_test->result =  result;
			return;
		}

		result = fram_write_block((void*)FRAM_PATTERN_STRING_OFFSET, (void*)fram_test_pattern_str, sizeof(fram_test_pattern_str));
		if (result != E_CB_NO_ERROR)
		{
			//printf("Error occured while writeing FRAM 0x%X\n", result);
			memory_test->result =  result;
			return;
		}

		//fill rest of the FRAM with known value.
		for(offset = FRAM_WRITE_OFFSET;  offset < FRAM_SIZE ; offset+=2)
		{
			value =  (uint32_t)(offset);
			crc = _crc16(0, (char *)&value, sizeof(uint32_t));
			result = fram_write_16((void*)offset,  crc);
			if(E_CB_NO_ERROR != result)
			{
				printf("FAIL: Failed to offset %08lx, result %lx.\n", value, result);
				memory_test->result =  result;
				memory_test->info = (uint32_t)value;
				return;
			}
		}


		//verify write : read and compare against CRC
		for(offset = FRAM_WRITE_OFFSET;  offset < FRAM_SIZE; offset+=2)
		{
			value =  (uint32_t)(offset);
			crc = _crc16(0, (char *)&value, sizeof(uint32_t));

			if ( *(uint16_t*)(FRAM_START + (uint32_t)value) != crc)
			{
				result = E_CB_VERIFICATION_FAILURE;
				printf("FAIL: Verification Failed to offset %08lx, result %0lx.\n", value, result);
				memory_test->result =  result;
				memory_test->info = (uint32_t)value;
				return;
			}

		}

		//printf("Test Finished. Please powercycle to check for Pattern or rewrite pattern.\n");
	}

	//printf("+++++++++++++++++++++++\n");
	//printf("FRAM Test Passed.\n");
	//printf("+++++++++++++++++++++++\n");

	/** We're done */
	return ;
}

bool PerformMemoryTests(memory_test_data_t *memory_test);
void fram_tests(void)
{
	memory_test_data_t stest;

	stest.memory_test_req.address = 0;
	stest.memory_test_req.count = 1;
	stest.memory_test_req.memory_type = 2;
	stest.memory_test_req.test_type = DATA_RETENTION; //read
	stest.memory_test_req.value8 = 0xAA;
	stest.memory_test_req.value16 = 0xBBBB;
	stest.memory_test_req.value32 = 0xCCCCCCCC;
	stest.memory_test_req.size = FRAM_SIZE;
	stest.info = 0;
	stest.result = 0;

		//Data retention test
		//fram_data_retention(&stest);
		PerformMemoryTests(&stest);
		 if(stest.result)
			printf("fram_data_retention failed \r\n");


		stest.memory_test_req.test_type = READ; //read

		//fram_fill_test(&stest);
		PerformMemoryTests(&stest);
		if(stest.result)
			printf("Fram_fill failed \r\n");

		stest.memory_test_req.test_type = ERASE_CHIP; //read
		//fram_erase_test(&stest);
		PerformMemoryTests(&stest);
		if(stest.result)
			printf("fram_erase failed \r\n");

		stest.memory_test_req.test_type = READ; //read
		//fram_read_test(&stest);
		PerformMemoryTests(&stest);
		if(stest.result)
			printf("fram_read failed \r\n");
		else
			printf("fram_read %4lx\r\n", stest.info);


		//fram_write_16_test(&stest);
		stest.memory_test_req.test_type = WRITE_WORD; //read
		PerformMemoryTests(&stest);
		if(stest.result)
			printf("fram_write_16 failed \r\n");
		else
		{
			stest.memory_test_req.test_type = READ; //read
			//fram_read_test(&stest);
			PerformMemoryTests(&stest);
			if(stest.result)
				printf("fram_read failed \r\n");
			else
				printf("fram_read %4lx\r\n", stest.info);
		}


		stest.memory_test_req.test_type = WRITE_WORD; //read
		//fram_write_32_test(&stest);
		PerformMemoryTests(&stest);
		if(stest.result)
			printf("fram_write_32 failed \r\n");
		else
		{
			//fram_read_test(&stest);
			stest.memory_test_req.test_type = READ; //read
			PerformMemoryTests(&stest);
			if(stest.result)
				printf("fram_read failed \r\n");
			else
				printf("fram_read %4lx\r\n", stest.info);
		}

		fram_read_test(&stest);
		if(stest.result)
			printf("fram_read failed \r\n");
		else
			printf("fram_read: %4lx\r\n", stest.info);

		fram_write_test(&stest);
		if(stest.result)
			printf("fram_write failed \r\n");

		fram_read_test(&stest);
		if(stest.result)
			printf("fram_read failed \r\n");
		else
			printf("fram_read: %4lx\r\n", stest.info);

}

#endif //MEMORYTEST

/******************************************************************************/
/* EOF */
