
#include <string.h>
#include <assert.h>
#include "nucleus.h"
#include "kernel/nu_kernel.h"
#include "lists.h"
//#include "interrupt.h"
//#include "2357s.h"

/* ----------------------------------------------------------------------------------------[list_reset_water_marks]-- */
void list_reset_water_marks(list_hdr_t *p_list_hdr)
{
    if (p_list_hdr)
    {
        p_list_hdr->low_water_mark = p_list_hdr->num_entries;
        p_list_hdr->high_water_mark = 0;
    }
}

/* ---------------------------------------------------------------------------------------------------[list_create]-- */
int list_create(list_t *p_list, size_t payload, const void *mem, uint32_t mem_size, cmp_fn_t cmp_fn)
{
    int rv = -1;

    if (p_list)
    {
        p_list->active.head = NULL;
        p_list->active.tail = NULL;
        p_list->active.num_entries = 0;
        p_list->active.payload_size = payload;
        p_list->active.sorted = (cmp_fn != NULL);
        p_list->active.cmp_fn = cmp_fn;

        p_list->active_high_priority.head = NULL;
        p_list->active_high_priority.tail = NULL;
        p_list->active_high_priority.num_entries = 0;
        p_list->active_high_priority.payload_size = payload;
        p_list->active_high_priority.sorted = (cmp_fn != NULL);
        p_list->active_high_priority.cmp_fn = cmp_fn;


        p_list->free.head = NULL;
        p_list->free.tail = NULL;
        p_list->free.num_entries = 0;
        p_list->free.payload_size = payload;
        p_list->free.sorted = 0;
        p_list->free.cmp_fn = NULL;

        assert(payload % 4 == 0);/* assumption : payload size is chosen to prevent alignment problems */
        p_list->max_entries = mem_size/ (payload + sizeof(struct list_entry_s *));

        /* zero stats */
        p_list->stats.list_prepends = 0;
        p_list->stats.list_appends = 0;
        p_list->stats.list_adds_in_order = 0;
        p_list->stats.list_appends_high_priority = 0;

        p_list->mem_block_size =   p_list->max_entries * (payload + sizeof(struct list_entry_s *));

        assert(mem != NULL);
        if (mem)
        {
            const char *ptr;
            uint32_t i;

            /* we got the memory --> create the free list */
            ptr = mem;
            for (i=0; i<p_list->max_entries; i++)
            {
                /* use the free_entry function to add entries */
                /* to the list of free entries                */
                list_free_entry( p_list, (/*list_entry_t*/ void *)ptr);
                ptr += payload + sizeof(struct list_entry_s *);
            }
            rv = 0;
        }

        list_reset_water_marks(&p_list->active);
        list_reset_water_marks(&p_list->active_high_priority);
        list_reset_water_marks(&p_list->free);

    }
    return rv;
}

/* --------------------------------------------------------------------------------------------------[list_destroy]-- */
void list_destroy(list_t *p_list)
{
    if (p_list)
    {
        memset(p_list, 0, sizeof(list_t));
    }
}

/* ------------------------------------------------------------------------------------[list_update_low_water_mark]-- */
static void list_update_low_water_mark(list_hdr_t *p_list_hdr)
{
    if (p_list_hdr->num_entries < p_list_hdr->low_water_mark)
    {
        p_list_hdr->low_water_mark = p_list_hdr->num_entries;
    }
}

/* -----------------------------------------------------------------------------------[list_update_high_water_mark]-- */
static void list_update_high_water_mark(list_hdr_t *p_list_hdr)
{
    if (p_list_hdr->num_entries > p_list_hdr->high_water_mark)
    {
        p_list_hdr->high_water_mark = p_list_hdr->num_entries;
    }
}

/* --------------------------------------------------------------------------------------------[list_get_new_entry]-- */
list_entry_t *list_get_new_entry(list_t *p_list)
{
    //asm("orc #7, EXR"); /* block all interrupts */
    INT old_intr_level = NU_Control_Interrupts(NU_DISABLE_INTERRUPTS);
    list_entry_t *p_entry = NULL;


    if (p_list->free.num_entries)
    {
        p_entry = p_list->free.head;
        p_list->free.head = p_entry->next;
        p_list->free.num_entries--;
        list_update_low_water_mark(&p_list->free);
    }
    NU_Control_Interrupts(old_intr_level); 
    return p_entry;
}

/* --------------------------------------------------------------------------------------------[list_prepend_entry]-- */
int list_prepend_entry(list_t *p_list, list_entry_t *p_entry)
{
    //asm("orc #7, EXR"); /* block all interrupts */
    INT old_intr_level = NU_Control_Interrupts(NU_DISABLE_INTERRUPTS);
    int rv = -1;
    if (p_list && p_entry)
    {
        //DISABLE_INTR;
        p_entry->next = p_list->active.head;
        if (!p_list->active.head)
        {
            /* list was empty */
            p_list->active.tail = p_entry;
            p_list->active.num_entries = 1;
        }
        else
        {
            /* list was not empty */
            p_list->active.num_entries++;
        }
        p_list->active.head = p_entry;
        list_update_high_water_mark(&p_list->active);
        p_list->stats.list_prepends++;
        //ENABLE_INTR;
        rv = 0;
    }
    NU_Control_Interrupts(old_intr_level); 
    return rv;
}

/* ---------------------------------------------------------------------------------------------[list_append_entry]-- */
int list_append_entry(list_t *p_list, list_entry_t *p_entry)
{
   // asm("orc #7, EXR"); /* block all interrupts */
   INT old_intr_level = NU_Control_Interrupts(NU_DISABLE_INTERRUPTS);
   int rv = -1;

   p_entry->next = NULL;
   if (!p_list->active.head)
   {
      /* list was empty */
      p_list->active.head = p_entry;
      p_list->active.num_entries = 1;
   }
   else
   {
      /* list was not empty */
      p_list->active.tail->next = p_entry;
      p_list->active.num_entries++;
   }
   p_list->active.tail = p_entry;
   p_list->stats.list_appends++;
   list_update_high_water_mark(&p_list->active);
   rv = 0;

   NU_Control_Interrupts(old_intr_level); 
   return rv;
}

/* -------------------------------------------------------------------------------[list_append_entry_high_priority]-- */
int list_append_entry_high_priority(list_t *p_list, list_entry_t *p_entry)
{
   //asm("orc #7, EXR"); /* block all interrupts */
   INT old_intr_level = NU_Control_Interrupts(NU_DISABLE_INTERRUPTS);
   int rv = -1;

   p_entry->next = NULL;
   if (!p_list->active_high_priority.head)
   {
      /* list was empty */
      p_list->active_high_priority.head = p_entry;
      p_list->active_high_priority.num_entries = 1;
   }
   else
   {
      /* list was not empty */
      p_list->active_high_priority.tail->next = p_entry;
      p_list->active_high_priority.num_entries++;
   }
   p_list->active_high_priority.tail = p_entry;
   p_list->stats.list_appends_high_priority++;
   list_update_high_water_mark(&p_list->active_high_priority);
   rv = 0;
   NU_Control_Interrupts(old_intr_level); 

   return rv;
}

/* ---------------------------------------------------------------------------------------------[list_add_in_order]-- */
int list_add_in_order(list_t *p_list, list_entry_t *p_entry)
{
    //asm("orc #1, EXR"); /* block all but the TX & RX interrupts */
    INT old_intr_level = NU_Control_Interrupts(NU_DISABLE_INTERRUPTS);
    int rv = -1;

    if (p_list && p_entry && p_list->active.sorted && p_list->active.cmp_fn)
    {
        //DISABLE_INTR;
        if (!p_list->active.head)
        {
            /* empty list */
            p_list->active.head = p_list->active.tail = p_entry;
            p_entry->next = NULL;
            p_list->active.num_entries = 1;
        }
        else
        {

            list_entry_t *p_cur, *p_prev = NULL;
            p_cur = p_list->active.head;
            while ( p_cur && p_list->active.cmp_fn(p_cur->data, p_entry->data) <= 0)
            {
                p_prev = p_cur;
                p_cur = p_cur->next;
            }
            if (p_cur == NULL)
            {
                /* new entry should go at the end */
                p_list->active.tail->next = p_entry;
                p_entry->next = NULL;
                p_list->active.tail = p_entry;
                p_list->active.num_entries++;
            }
            else
            {
                if (p_prev == NULL)
                {
                    /* new entry should go at beginning */
                    p_entry->next = p_list->active.head;
                    p_list->active.head = p_entry;
                    p_list->active.num_entries++;
                }
                else
                {
                    /* insert after p_prev */
                    p_entry->next = p_prev->next;
                    p_prev->next = p_entry;
                    p_list->active.num_entries++;
                }
            }
        }
        p_list->stats.list_adds_in_order++;
        list_update_high_water_mark(&p_list->active);
        //ENABLE_INTR;
        rv = 0;
    }
    NU_Control_Interrupts(old_intr_level); 
    return rv;
}

/* ----------------------------------------------------------------------------------[list_get_first_high_priority]-- */
list_entry_t *list_get_first_high_priority(list_t *p_list)
{
    //asm("orc #7, EXR"); /* block all interrupts */
    INT old_intr_level = NU_Control_Interrupts(NU_DISABLE_INTERRUPTS);
    list_entry_t *p_entry = NULL;
    if (p_list && p_list->active_high_priority.head)
    {
        //DISABLE_INTR;
        p_entry = p_list->active_high_priority.head;
        if (p_list->active_high_priority.head == p_list->active_high_priority.tail)
        {
            /* list had one item */
            p_list->active_high_priority.head = p_list->active_high_priority.tail = NULL;
            p_list->active_high_priority.num_entries = 0;
        }
        else
        {
            p_list->active_high_priority.head = p_list->active_high_priority.head->next;
            p_list->active_high_priority.num_entries--;
        }
        list_update_low_water_mark(&p_list->active_high_priority);
        //ENABLE_INTR;
        p_entry->next = NULL;
    }
    NU_Control_Interrupts(old_intr_level); 
    return p_entry;
}


/* ------------------------------------------------------------------------------------------------[list_get_first]-- */
list_entry_t *list_get_first(list_t *p_list)
{
    //asm("orc #7, EXR"); /* block all interrupts */
    INT old_intr_level = NU_Control_Interrupts(NU_DISABLE_INTERRUPTS);
    list_entry_t *p_entry = NULL;
    if (p_list && p_list->active.head)
    {
        //DISABLE_INTR;
        p_entry = p_list->active.head;
        if (p_list->active.head == p_list->active.tail)
        {
            /* list had one item */
            p_list->active.head = p_list->active.tail = NULL;
            p_list->active.num_entries = 0;
        }
        else
        {
            p_list->active.head = p_list->active.head->next;
            p_list->active.num_entries--;
        }
        list_update_low_water_mark(&p_list->active);
        //ENABLE_INTR;
        p_entry->next = NULL;
    }
    NU_Control_Interrupts(old_intr_level); 
    return p_entry;
}

/* -----------------------------------------------------------------------------------------------[list_peek_first]-- */
list_entry_t *list_peek_first(list_t *p_list)
{
    if (p_list)
        return p_list->active.head;
    else
        return NULL;
}


/* -----------------------------------------------------------------------------------------------[list_free_entry]-- */
int list_free_entry(list_t *p_list, list_entry_t *p_entry)
{
    //asm("orc #7, EXR"); /* block all interrupts */
    INT old_intr_level = NU_Control_Interrupts(NU_DISABLE_INTERRUPTS);
    int rv = -1;

    p_entry->next = NULL;
    //DISABLE_INTR;
    if (p_list->free.head)
    {
        /* non empty list */
        p_list->free.tail->next = p_entry;
    }
    else
    {
        /* empty free list */
        p_list->free.head = p_entry;
    }
    p_list->free.tail = p_entry;
    p_list->free.num_entries++;
    list_update_high_water_mark(&p_list->free);
    //ENABLE_INTR;
    rv = 0;

    NU_Control_Interrupts(old_intr_level); 
    return rv;
}


/* ---------------------------------------------------------------------------------------[list_free_entry_prepend]-- */
int list_free_entry_prepend(list_t *p_list, list_entry_t *p_entry)
{
    //asm("orc #7, EXR"); /* block all interrupts */
    INT old_intr_level = NU_Control_Interrupts(NU_DISABLE_INTERRUPTS);
    int rv = -1;

    p_entry->next = p_list->free.head;
    //DISABLE_INTR;
    if (p_list->free.head == NULL)
    {
        /* free list was empty */
        p_list->free.tail = p_entry;
    }
    p_list->free.head = p_entry;
    p_list->free.num_entries++;
    list_update_high_water_mark(&p_list->free);
    //ENABLE_INTR;
    rv = 0;

    NU_Control_Interrupts(old_intr_level); 

    return rv;
}

/* -------------------------------------------------------------------------------------------[list_process_active]-- */
void list_process_active(list_t *p_list, fn_process_t fn_process, void *p_context)
{
    if (p_list && fn_process)
    {
        int go_on = TRUE;
        list_entry_t *p_entry = list_peek_first(p_list);
        while (p_entry && go_on)
        {
            go_on = fn_process(p_context, p_entry->data);
            p_entry = p_entry->next;
        }
    }
}

/* ---------------------------------------------------------------------------------------------[list_process_free]-- */
void list_process_free(list_t *p_list, fn_process_t fn_process, void *p_context)
{
    if (p_list && fn_process)
    {
        int go_on = TRUE;

        list_entry_t *p_entry = p_list->free.head;
        while (p_entry && go_on)
        {
            go_on = fn_process(p_context, p_entry->data);
            p_entry = p_entry->next;
        }
    }
}

/* ----------------------------------------------------------------------------------------[list_get_with_criteria]-- */
list_entry_t *list_get_with_criteria(list_t *p_list, fn_loc_entry_t fn_locate, void *p_context)
{
    list_entry_t *p_entry = NULL;
    INT old_intr_level;

    if (p_list && fn_locate)
    {
        list_locate_result_t loc_res = LOCATE_NO_MATCH;

        list_entry_t *p_prev = NULL;
        old_intr_level = NU_Control_Interrupts(NU_DISABLE_INTERRUPTS);
        p_entry = list_peek_first(p_list);

        while (p_entry && LOCATE_NO_MATCH == (loc_res = fn_locate(p_context, p_entry->data)))
        {
            p_prev = p_entry;
            p_entry = p_entry->next;
        }
        if (LOCATE_MATCH == loc_res)
        {
            if (p_list->active.head == p_entry)
            {
                /* found item was first in list */
                p_list->active.head = p_entry->next;
            }
            else
            {
                p_prev->next = p_entry->next;
            }

            if (p_list->active.tail == p_entry)
            {
                /* found entry was last entry in list */
                p_list->active.tail = p_prev;
            }
            p_list->active.num_entries--;
            list_update_low_water_mark(&p_list->active);
            p_entry->next = NULL;
        }
        else
        {
            p_entry = NULL;
        }
        NU_Control_Interrupts(old_intr_level);
    }
    return p_entry;
}

#ifdef TEST_LIST

/* ----------------------------------------------------------------------------------------------------[fn_cmp_int]-- */
int fn_cmp_int(const void *a, const void *b)
{
    return (*(int *)a - *(int *)b);
}

typedef struct
{
    int n;
} data_t;

typedef struct 
{
    int entry_num;
} print_list_context_t;

/* --------------------------------------------------------------------------------------------[fn_print_list_item]-- */
static int fn_print_list_item(void *p_context, void *p_data)
{
    if (p_context && p_data)
    {
         
        if ( ((print_list_context_t *)p_context)->entry_num++ == 0)
        {
            printf("\n\tnode | value\n\t-----+------\n");
        }
        printf("\t%4d | %5d\n", ((print_list_context_t *)p_context)->entry_num, ((data_t *)p_data)->n);
    }
    return 1;
}

/* ---------------------------------------------------------------------------------------------[fn_test_disp_list]-- */
void fn_test_disp_list(list_t *p_list)
{
    print_list_context_t context;

    printf("\n#active %d (%d/%d) #free %d (%d/%d) max entries %d \n", 
               p_list->active.num_entries,
               p_list->active.low_water_mark,
               p_list->active.high_water_mark,
               p_list->free.num_entries,
               p_list->free.low_water_mark,
               p_list->free.high_water_mark,
               p_list->max_entries
               );

    context.entry_num = 0;
    list_process_active(p_list, fn_print_list_item, &context);
    printf("\n");
}

/* -------------------------------------------------------------------------------------------------[fn_test_lists]-- */
void fn_test_lists(void)
{
    list_t list;
    list_entry_t *p_entry;
    data_t *p_data;
    int i;
    char list_mem[10*(sizeof(int) + sizeof(* int))];

    list_create(&list, sizeof(int), list_mem, sizeof(list_mem), fn_cmp_int);

    for (i=0; i < 5; i++)
    {
        fn_test_disp_list(&list);
        if (p_entry = list_get_new_entry(&list))
        {
            p_data = (data_t *)p_entry->data;
            p_data->n = rand();
            list_add_in_order(&list, p_entry);
            printf("      creating entry %d (%d)\n", i+1, p_data->n);
        }
        else
        {
            printf("error creating entry %d \n", i+1);
        }
    }

    fn_test_disp_list(&list);

    printf("\nTesting removing from front\n");
    while (p_entry = list_get_first(&list))
    {
        printf("Removed entry (%d)\n", *(int *)p_entry->data);
        list_free_entry(&list, p_entry);

        fn_test_disp_list(&list);
    }


    printf("\nTesting prepending\n");
    for (i=0; i < 5; i++)
    {
        if (p_entry = list_get_new_entry(&list))
        {
            p_data = (data_t *)p_entry->data;
            p_data->n = rand();
            list_prepend_entry(&list, p_entry);
            printf("      creating entry %d (%d)\n", i+1, p_data->n);
        }
        else
        {
            printf("error creating entry %d\n", i+1);
        }
    }

    fn_test_disp_list(&list);

    printf("\nTesting appending\n");
    for (i=0; i < 5; i++)
    {
        if (p_entry = list_get_new_entry(&list))
        {
            p_data = (data_t *)p_entry->data;
            p_data->n = rand();
            list_append_entry(&list, p_entry);
            printf("      creating entry %d (%d)\n", i+1, p_data->n);
        }
        else
        {
            printf("error creating entry %d\n", i+1);
        }
    }

    fn_test_disp_list(&list);

}

#endif /* TEST_LIST */

/* -----------------------------------------------------------------------------------------------------------[eof]-- */

