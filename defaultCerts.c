/*************************************************************************
*   PROJECT:        Horizon CSD2-3
*	MODULE NAME:	defaultCerts.c
*
*	DESCRIPTION:	This file contains hardcoded default certificates that
*	are used only if no cert files are found in the filesystem.
*
* ----------------------------------------------------------------------
*               Copyright (c) 2017 Pitney Bowes Inc.
*                    37 Executive drive
*                   Danbury, Connecticut  06810
* ----------------------------------------------------------------------
*
*************************************************************************/

// To replace these, use Block Selection Mode

// default encrypted private key in PEM format
const unsigned char defaultEncryptedPrivateKey[] =
"-----BEGIN ENCRYPTED PRIVATE KEY-----\n\
MIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQI5Z2Ocqp9fkwCAggA\n\
MBQGCCqGSIb3DQMHBAgbhICW+bh/EgSCBMhJbHVfw8N2S3FAMDM+8evl862qK3FN\n\
ZCuocHAQrHYrQhS7FsI0J7TkFjlhBf6XFANIr8aSKihTx7JMMQ5sWeBQVzDTz7pk\n\
MDFFvD3CDO7Mrrwye7K8FLKhePZc+Vcqx7invIa+levwT4Px7xVcjVsTWWxVldkx\n\
VVJ7lzYEqjUioxlsZ/c8tUPO54ORk9kbrM87mChZHI0viSqf8chCrIQK4ms2ZfEz\n\
RGaNnJ3lkqspPnrAQ5h4X0NxB0/ndz5l4yrD5w/XstdgdCwQODno3Tu94wS8i3R6\n\
AjgMSlX+RUn32G5VetaMU9Fv6DWdbuMaocqY2h1VmxOaSZq11UiJ3tQEi8ooZABK\n\
bCzmf8PG3y966bJHNXV4RJbjL0lUDBSNWp8c/dioBgZ+iWdyAWRkGsLfhotE1qGi\n\
8X4prrBrYMpBmtArAC2mxJdcQXGzsbyc4mty5gDwpaSmwJdAVNllPIoPLZ9Zo12F\n\
zmud0fdEA39K4HSlT2vpb1diTIQEYM4YmDaxJl9xleBipMQYH2gacrJavMvi68nA\n\
QXiA0hXZPd1Yy9v3ix98GeHU1fGWt5alSXplq/hdzBoa8kpkNTBbHhGgzkn1O05o\n\
Eh2yaiGrwgjGPh18E77tFE39C/v92aRFJE53KOEyp817B1+Nof1AP78MBAJNmgfw\n\
ULB60NS3KXUE6P2cRp4w5XpxFdbsoeDqhfapOjXoNHugcTr7NfE72EOAxyqpwz2H\n\
kQ5C+br4fWqVDbvxUM5R5sb9QZ9cCa4D+Eik6O6a9kL7bJ8ySrKKkrRDcDIpNuns\n\
YjKTzUBOKPEKCyOKpYUwKCx+3JIY56UdHvJc1pFt9pQfS3pfN9CXAi1DnNdMXCPu\n\
yu7SSLcGK/Hkup5qi5/RmNx8czMe/s8OgzbW0wPUiaMkcnJpZxMMIYXyhx2RBIUa\n\
Z3Fr+o70jNw5W2SYEw9M2zz5YJqjml9v+h2zNYmKw+IRq6l1Hy31DouCGLyi05ok\n\
eryB9tUZsI4ATs1Aj6Eow3nQMMssPKIKnQt9MChXUV3lf4K8Pb6llocF2sEcEobr\n\
GoJyd6KOLw075pz2ulEN6e9IacMnWo4viP0jOJXObs1+ucucnX9ckoyH92NMcyxs\n\
VgAwPiM942pvzmdSQuhO/uREZzFanftwlZQny6m8nznPkJFH2a10Sj+rTe3yIknY\n\
mAG8OTmcsJpG8Y0SZRFp85Omjc4f46j8+WQamS63FSO4E3jnZL20NzGnygZ6MEx/\n\
jQ3egtk5EDF+k8mtUQYqobT965zAimTP+SQwmzcoKXSOQdD3uOCGTYMKgyzcVHoT\n\
dnjLcslFXFaLBLODetyvO7s+MfcrjPBDBstEEZRC/bu3toPolaOnlRR3qe8ynR1k\n\
ycQloGb7EBdYqVTOVBbTyB319b/WmVq8ITkkfXpIDwXfNdFbIxcZ4ty9QVAl2Vt4\n\
sPxcMLJrz8FAFMp7ANxrSqfSg0aWGE07bGYMDkNsh0f+wfJIpB2FKvLq0huucoWO\n\
ZqO93Jw8cKar4fuBtOP3dZldkhoudaw7K/ClBE+urIAOVdtiAl8Y9h/Y8hh2EHH3\n\
B9cmCjzSGZ2UJGba0dlxD30tv5MoOkYlfrRScLygGuXjK3q3PcoitXfRak0ir3Vn\n\
I0c=\n\
-----END ENCRYPTED PRIVATE KEY-----\n";

// default server cert Not Before Date - MUST update this when updating defaultServerCert
const unsigned char defaultServerCertNotBeforeDateTime[] = "20170623005154"; //YYYYMMDDHHMMSS

// default server cert Not After Date - MUST update this when updating defaultServerCert
const unsigned char defaultServerCertNotAfterDateTime[] =  "20441108005154"; //YYYYMMDDHHMMSS

// default server certificate in PEM format
const unsigned char defaultServerCert[] =
"-----BEGIN CERTIFICATE-----\n\
MIID7jCCAtagAwIBAgIJAIaB78fZGFlDMA0GCSqGSIb3DQEBCwUAMIGLMQswCQYD\n\
VQQGEwJVUzEUMBIGA1UECAwLQ29ubmVjdGljdXQxEDAOBgNVBAcMB0RhbmJ1cnkx\n\
FzAVBgNVBAoMDlBpdG5leUJvd2VzSW5jMR0wGwYDVQQLDBRLZXlNYW5hZ2VtZW50\n\
U3lzdGVtczEcMBoGA1UEAwwTYmFzZS5zZW5kcHJvLnBiLmNvbTAeFw0xNzA2MjMw\n\
MDUxNTRaFw00NDExMDgwMDUxNTRaMIGLMQswCQYDVQQGEwJVUzEUMBIGA1UECAwL\n\
Q29ubmVjdGljdXQxEDAOBgNVBAcMB0RhbmJ1cnkxFzAVBgNVBAoMDlBpdG5leUJv\n\
d2VzSW5jMR0wGwYDVQQLDBRLZXlNYW5hZ2VtZW50U3lzdGVtczEcMBoGA1UEAwwT\n\
YmFzZS5zZW5kcHJvLnBiLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC\n\
ggEBAKjcIj1CKPySAPYEtUsvEmCWWMLu28I3PC/XIMDyPjd8W2BXI4/ibbZaS5Mt\n\
zSb6b5PCh4uSdumrcH+mIhoiIArLOEmEn3qhTRJVoz6w9hmZrL93Fi8svgDDQwpT\n\
LQYb3GS5hdUnhQifrwsbwhjUhNQ1x6aqowb6uV9UbZkpFJT848FWyCZGBJ0dmRno\n\
0gaCbzBwn/OW0zBFVK0dBNkC4auj2Q79B6+RtU74gchLKH7QlxQ6q0xWqAH6ogKU\n\
0RzAY3wO2SJ6vNbsWf0bAfhre363WlkKj+cfnf5bwa/jeKbxjep/NdQi0VkLxl8E\n\
vo8waNIvvWGqgDX16XagPNH+hg8CAwEAAaNTMFEwHQYDVR0OBBYEFHpe+WvtQkCK\n\
e6m/O9A2fhyGoMxsMB8GA1UdIwQYMBaAFHpe+WvtQkCKe6m/O9A2fhyGoMxsMA8G\n\
A1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAGcLx5bwrzWa0d96jIpi\n\
H6wsvAK1Or2tczQFy3EiuRBlNGRMqbo6JcWtWJn9W4+lgdfbJnl54GdioKy35e7t\n\
/qgW3opvPrH6j+127+8qR87URogQPgkX0Bl5qAp24rNsAM7Yp6PefMPEJxt5LG0o\n\
/sS9hbvQRIA2cgg76LV0lslESb4KXhAA2bhuy2GsNqw15Ko/gRgMUFPcLD9mR27s\n\
HQxnQXeULngBWjDhy4Jt1es/0JgdhbFA1aXPUp064Rhruo/9ZFVTdT5CDxHcikHc\n\
ZSEqQMxTECkrFZiFll8pCLFyMStimSQdP5LxA8dqG4HrVTobF36UBqubk+be6QyF\n\
BkY=\n\
-----END CERTIFICATE-----\n";
