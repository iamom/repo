/* pb_tracing.c  -- instrument functions our own way */

#include "commontypes.h"
#include "asm_funcs.h"
#include "am335x_timer.h"

typedef struct
{
    uint16_t type;  /*0 == func_enter 1 = func exit */
    uint16_t tracing_level_hwm;
    uint32_t tick_32kHz;          /* actually it is the Timer Counter Register (TCRR) of timer 0 */ 
    //uint32_t tmr7_CRR;
    uint32_t rtc;
    uint32_t this_fn;
    uint32_t call_site;
} pb_func_trace_t;

#define NUM_PB_FUNC_ENTRIES  (4*1024*1024 / sizeof(pb_func_trace_t))  // 4MB worth of trace
pb_func_trace_t pb_func_trace[NUM_PB_FUNC_ENTRIES];

uint32_t tr_wr_idx = 0;

int tracing_level = 0;
int tracing_level_hwm = 0;  /* high water mark for tracing level */
int tracing_blocked = 0;

extern volatile uint32_t tick_count_1ms;


void enable_pb_tracing()
{
    tracing_level++;
    if (tracing_level > tracing_level_hwm)
        tracing_level_hwm = tracing_level;
}

void disable_pb_tracing()
{
    tracing_level--;
    if (tracing_level < 0)
        tracing_level = 0;

}

void start_trace_upload(void **blk1, uint32_t *len1, void **blk2, uint32_t *len2)
{
    tracing_blocked = true;
    if (tr_wr_idx < NUM_PB_FUNC_ENTRIES)
    {
        /* we haven't wrapped around yet: just one contiguous block */
        *blk1 = &pb_func_trace[0];
        *len1 = tr_wr_idx * sizeof(pb_func_trace_t);
        *blk2 = NULL;
        *len2 = 0;
    }
    else
    {
        /* we have wrapped --> 
         *     - first block starts where the tr_wr_idx points to now 
         *       and ends at the end of pb_func_trace table
         *     - 2nd block start at start of pb_func_trace and ends
         *       at the entry before the tr_wr_idx
         */
        uint32_t idx = tr_wr_idx % NUM_PB_FUNC_ENTRIES;
        *blk1 = &pb_func_trace[idx];  /* oldest entry */
        *len1 = (NUM_PB_FUNC_ENTRIES - idx) * sizeof(pb_func_trace_t);
        *blk2 = &pb_func_trace[0];
        *len2 = idx * sizeof(pb_func_trace_t);
    }
}

void end_trace_upload()
{
    tracing_blocked = false;
}

void __cyg_profile_func_enter(void *this_fn, void *call_site) __attribute__((no_instrument_function));
void __cyg_profile_func_enter(void *this_fn, void *call_site)
{
    if (tracing_blocked) return;

	if (tracing_level > 0)
	{
	    uint32_t idx = tr_wr_idx++;

        idx %= NUM_PB_FUNC_ENTRIES;
        pb_func_trace[idx].type = 0;
        pb_func_trace[idx].tracing_level_hwm = tracing_level_hwm;
        pb_func_trace[idx].tick_32kHz = *(volatile uint32_t *)(TIMER0_BASE + TIMER_TCRR);
      //pb_func_trace[idx].tmr7_CRR = *(volatile uint32_t *)0x4804A03C;    // Timer 7 Counter Register
        pb_func_trace[idx].rtc = get_rtc_DDhhmmss();
        pb_func_trace[idx].this_fn = this_fn;
        pb_func_trace[idx].call_site = call_site;

		//NU_TASK *task = NU_Current_Task_Pointer();

    	//NU_Trace_Mark("function.entry","%d%d%d",task->tc_id,this_fn,call_site);
	}
}

void __cyg_profile_func_exit(void *this_fn, void *call_site) __attribute__((no_instrument_function));
void __cyg_profile_func_exit(void *this_fn, void *call_site)
{
    if (tracing_blocked) return;

	if (tracing_level > 0)
	{
	    uint32_t idx = tr_wr_idx++;

        idx %= NUM_PB_FUNC_ENTRIES;
        pb_func_trace[idx].type = 1;
        pb_func_trace[idx].tracing_level_hwm = tracing_level_hwm;
        pb_func_trace[idx].tick_32kHz = *(volatile uint32_t *)(TIMER0_BASE + TIMER_TCRR);
      //pb_func_trace[idx].tmr7_CRR = *(volatile uint32_t *)0x4804A03C;    // Timer 7 Counter Register
        pb_func_trace[idx].rtc = get_rtc_DDhhmmss();
        pb_func_trace[idx].this_fn = this_fn;
        pb_func_trace[idx].call_site = call_site;

		//NU_TASK *task = NU_Current_Task_Pointer();
    	//NU_Trace_Mark("function.exit","%d%d%d",task->tc_id,this_fn,call_site);
	}
}


/* -----------------------------------------------------------------------------------------------------------[EOF]-- */
