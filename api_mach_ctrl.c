/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    api_mach_ctrl.c

   DESCRIPTION:    API implementations for machine control messages

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
* HISTORY: 
*				  
* 2018.08.23 CWB - 1H10 - Jira 8243 - Service Code in Barcode is 0.
* - Added new function validateServicCodeNumeric(), to check if the rating 
*    country is Canada (EMD), and if so, reply with appropriate error if 
*    ServiceCodeNumeric item is missing or out of range.
* - To prepareForIndiciaPrinting(), added call to new function.   
* - To remove existing warnings: 
*	1. Added prototypes for external functions. 
*	2. In validateIndiciaStrings added check for NullPointer AND Empty string.	  
* 	3. Changed fnfGetSelectedIndiciaString() to return the correct type.
*
******************************************************************************/
#include "commontypes.h"
#include "cJSON.h"
#include "am335x_pbdefs.h"
#include "hal.h"
#include "HAL_Power.h"
#include "HAL_AM335x_GPIO.h"
#include "wsox_tx_queue.h"
#include "wsox_services.h"
#include "oifpprint.h"  //TODO: Remove this content from oi?
#include "oit.h"
#include "bobutils.h"
#include "bob.h"
#include "syspriv.h"
#include "cwrapper.h"
#include "api_diags.h"
#include "grapfile.h"
#include "aiservic.h"
#include "utils.h"
#include "cmos.h"
#include "settingsmgr.h"
#include "textmsg.h"
#include "flashutil.h"
#include "imagegen.h"
#include "features.h"
#include "api_status.h"
#include "clock.h"
#include "intellilink.h"
#include "debugTask.h"
#include "api_utils.h"
#include "unistring.h"
#include "sysdatadefines.h"
#include "oifpmain.h"
#include "oiscreen.h"
#include "cm.h"
#include "powerdown.h"
#include "trmstrg.h"
#include "networkmonitor.h"
#include "oiclock.h"

#define __MACH_CTRL_DEFINE_TABLES__
#include "api_mach_ctrl.h"

#define  CRLF   "\r\n"

void fnSetThroughput(PRINT_THRUPUT thruput);
void fnSendBobPrintMode (unsigned char bMode);
void fnSendStopMailRequest (void);
extern void fnUpdateJsonErrorStatus(cJSON *disableCondRsp);

extern uchar bVltDecimalPlaces;
extern const char *api_status_to_string_tbl[];
extern BOOL fnGetSysTaskPwrupSeqCompleteFlag( void );
extern void fnChangeScreen(unsigned short wScreenNumber, BOOL fCallPreFn, BOOL fCallPostFn);
static BOOL checkRateVersion(cJSON *invalid);

extern BOOL bIsCorrecteDate;
extern CM_SYSTEM_STATUS_t sSystemStatus;
extern BOOL fDCAPUploadReqShown;

BOOL bGoToReady = FALSE;
static BOOL UploadReqForRateVers = FALSE;


// ----------------------------------------------------------------------------
//
//		External Function Declarations
//							For functions not declared in an include file.
// ----------------------------------------------------------------------------

// From oifpmain.c:
T_SCREEN_ID fnCheckPSDState (void);
T_SCREEN_ID fnCheckInspection (void);
T_SCREEN_ID fnPostCheckInspection (void);



/* ----------------------------------------------------------------------------------------[on_SetThroughputReq]-- */
/*
 * Sets the throughput for the base.
 */
static required_fields_tbl_t required_fields_tbl_SetThruputReq =
    {
    "SetThroughputReq", { "Throughput"
                       , NULL }
    };

// Must match PRINT_THRUPUT enum
static const char      *valid_throughputs [] = { "Low", "Medium", "High"};

void on_SetThroughputReq(UINT32 handle, cJSON *root)
{
    char            *requested_thruput;
    bool             valid_thruput     = false;
    PRINT_THRUPUT    thruput;

    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_SetThruputReq))
    {
        int  i;

        requested_thruput = cJSON_GetObjectItem(root, "Throughput")->valuestring;
        if (requested_thruput == NULL_PTR) requested_thruput = ""; // Prevent crash below

        for(i=0; i<NELEMENTS(valid_throughputs) && !valid_thruput; i++)
        {
            if (0 == NCL_Stricmp(valid_throughputs[i], requested_thruput))
            {
            	thruput = (PRINT_THRUPUT) i;
                valid_thruput = true;
            }
        }

		if (valid_thruput == true)
		{
			fnSetThroughput(thruput);
			generateResponse(&entry, rspMsg, "SetThroughput", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
		}
		else
		{
			generateResponse(&entry, rspMsg, "SetThroughput", api_status_to_string_tbl[API_STATUS_INVALID_THROUGHPUT], root);
		}
    }//RequiredFieldsArePresent
    else
    {
		generateResponse(&entry, rspMsg, "SetThroughput", api_status_to_string_tbl[API_STATUS_INVALID_PARAMETERS], root);
    }

}

/* ----------------------------------------------------------------------------------------[on_GetThroughputReq]-- */
/*
 * Get throughput value .
 */
//extern PrintParams          CMOSPrintParams;
void on_GetThroughputReq(UINT32 handle, cJSON *root){
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetThroughputRsp");

    PrintParams *ptrPrintParams = fnCMOSGetCMOSPrintParams();
    int throughput = ptrPrintParams->bSpeedToRunMachineAt;
    cJSON_AddStringToObject(rspMsg, "Throughput", valid_throughputs[throughput]);

    addEntryToTxQueue(&entry, root, "  GetThroughputRsp: Added GetThroughputRsp to tx queue. status=%d" CRLF);

}


/* ----------------------------------------------------------------------------------------[on_GoToReadyReq]-- */
/*
 * Sets the Printer state.
 */
static required_fields_tbl_t required_fields_tbl_GoToReadyReq =
    {
    	"GoToReadyReq", {
    			            "PrintMode",
    						NULL }
    };

/* All functions that validate inputs will put validated values into this structure */
typedef struct structGoToReadyReq{
	mach_ctrl_printmodes_t printMode;
	mach_ctrl_indiciatypes_t indiciaType;
	unsigned int postageValue;
	unsigned int baseRate;		
	char	carrier[4];
	char 	class[16];
	char	classToken;
	unsigned int weight;
	unsigned char numDec;
	mach_ctrl_reporttypes_t reportID;
	unsigned char reportLang;
	unsigned int batchCount;
	unsigned int batchValue;
	trm_fee_list_t feeList;
	unichar	  	indiciaString[MAX_INDICIA_STRING_LEN];
    unichar   	indiciaStringList[MAX_INDICIA_STRINGS][MAX_INDICIA_STRING_LEN];
    unsigned char miscData[MAX_MISC_DATA];
    char    prodId[32];

}GO_TO_READY_REQ ;

static 	GO_TO_READY_REQ goToReadyReq;

trm_fee_list_t* GetFeeList(void)
{
	return &goToReadyReq.feeList;
}

/* utility functions used by GoToReadyReq */

/* validates PrintMode and puts enum value in result structure */
static BOOL validatePrintMode(cJSON *root, cJSON *invalid)
{
	int  i = 0;
	char *pTemp;
	BOOL valid_mode = FALSE;

	pTemp = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_PRINTMODE])->valuestring;
    if (pTemp == NULL_PTR) pTemp = ""; // Prevent crash below

	for(i=0; i<NELEMENTS(mach_ctrl_printmodes_to_string_tbl) ; i++)
	{
		if (0 == NCL_Stricmp(mach_ctrl_printmodes_to_string_tbl[i], pTemp))
		{
			valid_mode = TRUE;
			break;
		}
	}
	if (valid_mode == TRUE)
	{
		goToReadyReq.printMode = i;
	}
	else
	{
		//Add invalid item to invalid list
		cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_PRINTMODE], pTemp));
	}

	return valid_mode;
}

/* Adjust fixed decimal value using NumDec value */
static UINT32 adjustFINTWithNumDec(UINT32 fint, UINT8 numDec)
{
	UINT32 numDecInternal = (UINT32) bVltDecimalPlaces;
	UINT32 i;

// Convert to internal FINT format - this only works when numDecInternal == numDecMax
	for (i = 0; i < (numDecInternal - numDec); i++)
		fint *= 10;

	return fint;
}

/* validates Postage  */
static BOOL validatePostage(cJSON *root, cJSON *invalid)
{
	UINT32 postage;
	BOOL passed = TRUE;

	// validate postage field
	if (cJSON_HasObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_POSTAGE]))
	{
		postage = (UINT32) cJSON_GetObjectItem(root,  mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_POSTAGE])->valueint;
	}
	else
	{
		//Add missing required item to invalid list
		passed = FALSE;
		cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_POSTAGE], ""));
	}

	if (passed == TRUE)
	{
		goToReadyReq.postageValue = postage;
	}
	else
	{
		goToReadyReq.postageValue = 0;
	}
	return passed;
}

/* validates Date Ducking  */
static BOOL validateDateDucking(cJSON *root, cJSON *invalid)
{
	char *pTemp;
	int  typeIdx = 0;
	BOOL valid = FALSE;
	unsigned char bobRetVal = SUCCESSFUL;

	// ducking is optional, so if not present, return
	if (!cJSON_HasObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_DATE_DUCKING]))
	{
		return TRUE;
	}

	// Validate date ducking type
	pTemp = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_DATE_DUCKING])->valuestring;
    if (pTemp == NULL_PTR)
    {
    	pTemp = ""; // Prevent crash below
    }

	// Validate input date ducking type
	for(typeIdx=0; typeIdx<NELEMENTS(mach_ctrl_datetypes_to_string_tbl) ; typeIdx++)
	{
		if (0 == NCL_Stricmp(mach_ctrl_datetypes_to_string_tbl[typeIdx], pTemp))
		{
			valid = TRUE;
			break;
		}
	}
	if (valid == FALSE)
	{
		cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_DATE_DUCKING], pTemp));
		return valid;
	}

	switch(typeIdx)
	{
		case 0:
			bobRetVal = fnDateDuckOff();
			break;

		case 1:
			bobRetVal = fnDateDuckDayOnly();
			break;

		case 2:
			bobRetVal = fnDateDuckEntire();
			break;
	}
	if (bobRetVal == (unsigned char)FAILURE)
	{
		cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_DATE_DUCKING], "Operation Failed"));
		valid = FALSE;
	}

	return valid;

}

/* validates BaseRate   */
static BOOL validateBaseRate(cJSON *root, cJSON *invalid)
{
	UINT32 baseRate;
	BOOL passed = TRUE;

	// validate base rate field
	if (cJSON_HasObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_BASE_RATE]))
	{
		baseRate = (UINT32) cJSON_GetObjectItem(root,  mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_BASE_RATE])->valueint;
	}
	else
	{
		//Add missing required item to invalid list
		passed = FALSE;
		cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_BASE_RATE], ""));
	}

	if (passed == TRUE)
	{
		goToReadyReq.baseRate = baseRate;
	}
	else
	{
		goToReadyReq.baseRate = 0;
	}
	return passed;
} 

/* validates NumDec  */
static BOOL validateNumDec(cJSON *root, cJSON *invalid)
{
	UINT32 numDec;
	UINT32 numDecMax = (UINT32) bVltDecimalPlaces; //Currently they are the same
	BOOL passed = TRUE;

	// validate postage field
	if (cJSON_HasObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_NUMDEC]))
	{
		numDec = (UINT32) cJSON_GetObjectItem(root,  mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_NUMDEC])->valueint;
	}
	else
	{
		//Add missing required item to invalid list
		passed = FALSE;
		cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_NUMDEC], ""));
	}

	if (passed == TRUE)
	{
		if (numDec <= numDecMax)
		{
			goToReadyReq.numDec = numDec;
		}
		else
		{
			passed = FALSE;
			cJSON_AddItemToArray(invalid, CreateNumberItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_NUMDEC], numDec));
		}
	}
	return passed;
}

// Validates ServiceCodeNumeric  
static BOOL validateServicCodeNumeric( cJSON *root, cJSON *invalid )
{
	UINT32 ulServiceCode;
	const UINT32 ulMaxCanadaSvcCode = 0xFFFF;	  // Canadian Serivice Codes are only 2 bytes long.
	BOOL passed = TRUE;

	// Check if required.  It is required in Canada, so check EMD for CANADA.
    if( fnGetDefaultRatingCountryCode() == CANADA )
    {
		// Find out if there IS a ServiceCodeNumeric field... 
		if( cJSON_HasObjectItem( root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_SRVC_CODE_NUM]) )
		{
			// Verify it is in range.  Canada only allows a 16-bit service code.
			ulServiceCode = (UINT32)cJSON_GetObjectItem( root,  mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_SRVC_CODE_NUM] )->valueint;
			if( ulServiceCode > ulMaxCanadaSvcCode )
			{
				passed = FALSE;
				cJSON_AddItemToArray(invalid, CreateNumberItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_SRVC_CODE_NUM], ulServiceCode));
			}
		}
		else
		{
			//Add missing required item to invalid list
			passed = FALSE;
			cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_SRVC_CODE_NUM], ""));
    	}
	} // End of CANADA

	return( passed );
}

/* validates IndiciaType if it is provided  */
static BOOL validateIndiciaType(cJSON *root, cJSON *invalid)
{
	int  i = 0;
	char *pTemp;
	BOOL valid_type = FALSE;

	if (cJSON_HasObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_INDICIA_TYPE]))
	{
		pTemp = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_INDICIA_TYPE])->valuestring;
		if (pTemp == NULL_PTR) pTemp = ""; // Prevent crash below

		for(i=0; i<NELEMENTS(mach_ctrl_indiciatypes_to_string_tbl) ; i++)
		{
			if (0 == NCL_Stricmp(mach_ctrl_indiciatypes_to_string_tbl[i], pTemp))
			{
				valid_type = TRUE;
				break;
			}
		}
		if (valid_type == TRUE)
		{
			goToReadyReq.indiciaType = i;
		}
		else
		{
			//Add invalid item to invalid list
			cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_INDICIA_TYPE], pTemp));
		}
	}
	else
	{
        if (fnGetDefaultRatingCountryCode() == CANADA)
        {
//          this field is required for Canada
	    	valid_type = FALSE;
        }
        else
        {    
//          this field is optional - default to no indicia type
    		goToReadyReq.indiciaType = MACH_CTRL_INDICIATYPES_NONE;
	    	valid_type = TRUE;
        }
	}

	return valid_type;
}

/* **********************************************************************
 FUNCTION NAME: getIndiciaType

 PURPOSE:       Get the current Indicia Type

 AUTHOR:        Steve Terebesi

* INPUTS:       None

* OUTPUTS:      Current indicia type (mach_ctrl_indiciatypes_t)
 *************************************************************************/
mach_ctrl_indiciatypes_t getIndiciaType(void)
{
    return (goToReadyReq.indiciaType);
}

/* checks prerequisites based on IndiciaType  */
static BOOL validateSpecialPrinting(cJSON *invalid)
{
	BOOL valid = FALSE;
	char *pTemp;

	switch (goToReadyReq.indiciaType)
	{
		case MACH_CTRL_INDICIATYPES_NONE:
			{//this field is optional
				valid = TRUE;
			}
			break;
		case MACH_CTRL_INDICIATYPES_NO_UNIQUE:
			{//not special printing
				valid = TRUE;
			}
			break;
		case MACH_CTRL_INDICIATYPES_RETURN_POSTAGE_PREPAID:
			{
                valid = TRUE;
			}
			break;
		case MACH_CTRL_INDICIATYPES_CA_DATE_CORRECTION:
			{// Is there a date? Is postage 0?
				bool is_date_set;
				uint16_t length;
				uint32_t type;

				smgr_get_settings_info( SETTING_ITEM_SUBMISSION_DATE, &length, &type, &is_date_set);
				if (is_date_set && (goToReadyReq.postageValue == 0))
				{
					valid = TRUE;
				}
				else
				{
					pTemp = "No Date Set and/or Postage not zero";
				}
			}
			break;
		default:
			// should not get here
			break;
	}

	if (valid == FALSE)
	{
		//Add invalid item to invalid list
		cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_indiciatypes_to_string_tbl[goToReadyReq.indiciaType], pTemp));
	}

	return valid;
}

/* validates Fee List  */
static BOOL validateFeeList(cJSON *root, cJSON *invalid)
{
	BOOL passed = TRUE;
	cJSON * feeListArray = cJSON_CreateArray();
	int size, cnt;
	cJSON *item;

	memset(&goToReadyReq.feeList, 0, sizeof(goToReadyReq.feeList));
	// validate Fee List
	if (cJSON_HasObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_LIST]))
	{
		feeListArray = cJSON_GetObjectItem(root,  mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_LIST]);
		size = cJSON_GetArraySize(feeListArray);
		if(size <= MAX_FEE_CNT_SUPPORTED)
		{
			goToReadyReq.feeList.cnt = size;
			for ( cnt = 0; cnt < size; cnt++)
			{
				if(cJSON_HasObjectItem(cJSON_GetArrayItem(feeListArray, cnt), mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_TOKEN]))
				{
					item = cJSON_GetObjectItem(cJSON_GetArrayItem(feeListArray, cnt), mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_TOKEN]);
	                if (item->type == cJSON_Number)
	                {
						goToReadyReq.feeList.list[cnt].token = (UINT32) item->valueint;
	                }
	                else
	                {
	        			passed = FALSE;
	        			cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_TOKEN], "Fee list value wrong type"));
	                }
				}

				if(cJSON_HasObjectItem(cJSON_GetArrayItem(feeListArray, cnt), mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_SERVICE_CODE]))
				{
					item = cJSON_GetObjectItem(cJSON_GetArrayItem(feeListArray, cnt), mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_SERVICE_CODE]);
	                if (item->type == cJSON_Number)
	                {
						goToReadyReq.feeList.list[cnt].servicecode = (UINT32) item->valueint;
	                }
	                else
	                {
	        			passed = FALSE;
	        			cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_SERVICE_CODE], "Fee list value wrong type"));
	                }
				}

				if(cJSON_HasObjectItem(cJSON_GetArrayItem(feeListArray, cnt), mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_AMOUNT]))
				{
					item = cJSON_GetObjectItem(cJSON_GetArrayItem(feeListArray, cnt), mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_AMOUNT]);
	                if (item->type == cJSON_Number)
	                {
						goToReadyReq.feeList.list[cnt].amount = (UINT32) item->valueint;
	                }
	                else
	                {
	        			passed = FALSE;
	        			cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_AMOUNT], "Fee list value wrong type"));
	                }
				}

				if(cJSON_HasObjectItem(cJSON_GetArrayItem(feeListArray, cnt), mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_VALUE]))
				{
					item = cJSON_GetObjectItem(cJSON_GetArrayItem(feeListArray, cnt), mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_VALUE]);
	                if (item->type == cJSON_Number)
	                {
						goToReadyReq.feeList.list[cnt].value = (UINT32) item->valueint;
	                }
	                else
	                {
	        			passed = FALSE;
	        			cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_VALUE], "Fee list value wrong type"));
	                }
				}

				if(cJSON_HasObjectItem(cJSON_GetArrayItem(feeListArray, cnt), mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_SYMBOL]))
				{
					item = cJSON_GetObjectItem(cJSON_GetArrayItem(feeListArray, cnt), mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_SYMBOL]);
	                if (item->type == cJSON_String)
	                {
						memset(goToReadyReq.feeList.list[cnt].feesymbol, 0, sizeof(goToReadyReq.feeList.list[cnt].feesymbol));

						strncpy(goToReadyReq.feeList.list[cnt].feesymbol,
								item->valuestring,
								sizeof(goToReadyReq.feeList.list[cnt].feesymbol) );
	                }
	                else
	                {
	        			passed = FALSE;
	        			cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_SYMBOL], "Fee list value wrong type"));
	                }

				}
			}
		}
		else
		{
			passed = FALSE;
			cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_FEE_LIST], ""));
		}
	}
	else
	{
		// clean up message if it is not used.
		cJSON_Delete(feeListArray) ;
	}

	return passed;
}


/* This gets the graphic using the name and validates it.  If ad is not valid, ads are turned off. */
static BOOL validateAdGraphic(cJSON *root, cJSON *invalid)
{
	UINT8   gfName[ GRFX_UNINAME_MAX_COMPARE_LEN ];
	short len;
	GFENT *pDirEntry;
	UINT16 dirIndex = 0;
	UINT8 *pLinkPtr;
	BOOL passed = FALSE;

	cJSON *adDataNameRev = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_ADGRAPHIC]);
    if (adDataNameRev)
    {
		passed = FALSE;
		len = fnAsciiBcdToPackedBcd(gfName, sizeof(gfName), adDataNameRev->valuestring, TRUE);
		if(len > 0)
		{
			gfName[len] = 0;
			gfName[len+1] = 0;

			pLinkPtr = fnGFGetDirInxByUniName(&dirIndex, AD_COMP_TYPE, (UNICHAR *)gfName, len, GRFX_ENABLED_ONLY);
			if(pLinkPtr != NULL)
			{
				pDirEntry = fnGFGetDirPtrByDirInx(dirIndex);
				if(pDirEntry != NULL)
				{
					passed = fnAISetActive(pDirEntry->gfType, pDirEntry->gfID);
				}
			}
		}
		else if(len == 0)
		{
			passed = fnAISetActive(AD_COMP_TYPE, 0);		// Turn off ad
		}

		if(passed == FALSE)
		{
			//Add failed ad to the invalid list
			cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_ADGRAPHIC], adDataNameRev->valuestring));
		}
	}
	else
	{
		passed = fnAISetActive(AD_COMP_TYPE, 0);		// Turn off ad
	}

    return passed;
}

/* This gets the graphic using the name and validates it.  If inscription is not valid, inscriptions are turned off. */
static BOOL validateInscGraphic(cJSON *root, cJSON *invalid)
{
	UINT8   gfName[ GRFX_UNINAME_MAX_COMPARE_LEN ];
	short len;
	GFENT *pDirEntry;
	UINT16 dirIndex = 0;
	UINT8 *pLinkPtr;
	BOOL passed = FALSE;

    cJSON *inscDataNameRev = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_INSCGRAPHIC]);
    if (inscDataNameRev)
    {
		passed = FALSE;
		len = fnAsciiBcdToPackedBcd(gfName, sizeof(gfName), inscDataNameRev->valuestring, TRUE);
		if(len > 0)
		{
			gfName[len] = 0;
			gfName[len+1] = 0;

			pLinkPtr = fnGFGetDirInxByUniName(&dirIndex, INSCR_COMP_TYPE, (UNICHAR *)gfName, len, GRFX_ENABLED_ONLY);
			if(pLinkPtr != NULL)
			{
				pDirEntry = fnGFGetDirPtrByDirInx(dirIndex);
				if(pDirEntry != NULL)
				{
					passed = fnAISetActive(pDirEntry->gfType, pDirEntry->gfID);
				}
			}
		}
		else if(len == 0)
		{
            passed = fnAISetActive(INSCR_COMP_TYPE, 0);	// Turn off inscr
		}

		if(passed == FALSE)
		{
			//Add failed Insc to the invalid list
			cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_INSCGRAPHIC], inscDataNameRev->valuestring));
		}
	}
	else
	{
        passed = fnAISetActive(INSCR_COMP_TYPE, 0);	// Turn off inscr
	}

    return passed;
}


static BOOL validateTextMessageInputs(cJSON *root, cJSON *invalid)
{
	UINT16	usImageID;
	BOOL passed = FALSE;
	BOOL passedText[5] = {FALSE,FALSE,FALSE,FALSE,FALSE};

	cJSON *uText1 = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG1]);
	if (uText1)
		passedText[0] = TRUE;

	cJSON *uText2 = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG2]);
	if (uText2)
		passedText[1] = TRUE;

	cJSON *uText3 = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG3]);
	if (uText3)
		passedText[2] = TRUE;

	cJSON *uText4 = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG4]);
	if (uText4)
		passedText[3] = TRUE;

	cJSON *uText5 = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG5]);
	if (uText5)
		passedText[4] = TRUE;

	if(passedText[0] == TRUE ||
		passedText[1] == TRUE ||
		passedText[2] == TRUE ||
		passedText[3] == TRUE ||
		passedText[4] == TRUE)
	{
		passed = TRUE;

	}
	else
	{
		if (fnAIGetActive(TEXT_COMP_TYPE, &usImageID) == SUCCESSFUL)
		{
			fnDeleteTextByIndex(0);
			fnAISetActive(TEXT_COMP_TYPE, 0);
		}
	}
	return passed;
}



/* This gets the Text Message creates and send to Bob to print it.  If there are no text messages it is turned off. */
static BOOL validateTextMessage(cJSON *root, cJSON *invalid)
{
	short len;
	BOOL passed = FALSE;
	UINT8 status;
	UINT16	usImageID;
	BOOL passedText[5] = {FALSE,FALSE,FALSE,FALSE,FALSE};
	UINT8   sTextEntryLineOfText[ MAX_TEXT_ENTRY_UNICHARS * 2 ];
	UINT8   sTempTextEntryLineOfText[ MAX_TEXT_ENTRY_UNICHARS * 2 ];

    UINT8       ucStatus, ucIndex;
    BOOL        fEnabled = FALSE;


	if( IsFeatureEnabled(&fEnabled, MANUAL_TEXT_MSGS_SUPPORT, &ucStatus, &ucIndex) && fEnabled )
	{

		if (fnAIGetActive(TEXT_COMP_TYPE, &usImageID) == SUCCESSFUL)
		{
			fnDeleteTextByIndex(0);
		}

		memset(sTextEntryLineOfText, 0x0, sizeof(sTextEntryLineOfText));
		memset(sTempTextEntryLineOfText, 0x0, sizeof(sTempTextEntryLineOfText));
		cJSON *uText1 = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG1]);
		if (uText1)
		{
			//TODO to remove when Ladder2 pattern as not default
	//    	passed = fnAISetActive(AD_COMP_TYPE, 0);
			len = fnAsciiBcdToPackedBcd(sTempTextEntryLineOfText, sizeof(sTempTextEntryLineOfText), uText1->valuestring, TRUE);
			if(len > 0 && len <= 40){
				EndianAwareArray16Copy(sTextEntryLineOfText, sTempTextEntryLineOfText, len);
				status = fnSetTextEntryLineOfText(0,0,(UNICHAR *)sTextEntryLineOfText);
				if(status == TEXT_ENTRY_SUCCESS)
					passedText[0] = TRUE;

			}
			else
			{
				passed = FALSE;
				cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG1], uText1->valuestring));
				return passed;
			}
		}

		memset(sTextEntryLineOfText, 0x0, sizeof(sTextEntryLineOfText));
		memset(sTempTextEntryLineOfText, 0x0, sizeof(sTempTextEntryLineOfText));
		cJSON *uText2 = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG2]);
		if (uText2)
		{
			len = fnAsciiBcdToPackedBcd(sTempTextEntryLineOfText, sizeof(sTempTextEntryLineOfText), uText2->valuestring, TRUE);
			if(len > 0 && len <= 40){
				EndianAwareArray16Copy(sTextEntryLineOfText, sTempTextEntryLineOfText, len);
				status = fnSetTextEntryLineOfText(0,1,(UNICHAR *)sTextEntryLineOfText);
				if(status == TEXT_ENTRY_SUCCESS)
					passedText[1] = TRUE;
			}
			else
			{
				passed = FALSE;
				cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG2], uText2->valuestring));
				return passed;
			}

		}

		memset(sTextEntryLineOfText, 0x0, sizeof(sTextEntryLineOfText));
		memset(sTempTextEntryLineOfText, 0x0, sizeof(sTempTextEntryLineOfText));
		cJSON *uText3 = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG3]);
		if (uText3)
		{
			len = fnAsciiBcdToPackedBcd(sTempTextEntryLineOfText, sizeof(sTempTextEntryLineOfText), uText3->valuestring, TRUE);
			if(len > 0 && len <= 40){
				EndianAwareArray16Copy(sTextEntryLineOfText, sTempTextEntryLineOfText, len);
				status = fnSetTextEntryLineOfText(0,2,(UNICHAR *)sTextEntryLineOfText);
				if(status == TEXT_ENTRY_SUCCESS)
					passedText[2] = TRUE;
			}
			else
			{
				passed = FALSE;
				cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG3], uText3->valuestring));
				return passed;
			}

		}

		memset(sTextEntryLineOfText, 0x0, sizeof(sTextEntryLineOfText));
		memset(sTempTextEntryLineOfText, 0x0, sizeof(sTempTextEntryLineOfText));
		cJSON *uText4 = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG4]);
		if (uText4)
		{
			len = fnAsciiBcdToPackedBcd(sTempTextEntryLineOfText, sizeof(sTempTextEntryLineOfText), uText4->valuestring, TRUE);
			if(len > 0 && len <= 40){
				EndianAwareArray16Copy(sTextEntryLineOfText, sTempTextEntryLineOfText, len);
				status = fnSetTextEntryLineOfText(0,3,(UNICHAR *)sTextEntryLineOfText);
				if(status == TEXT_ENTRY_SUCCESS)
					passedText[3] = TRUE;
			}
			else
			{
				passed = FALSE;
				cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG4], uText4->valuestring));
				return passed;
			}

		}

		memset(sTextEntryLineOfText, 0x0, sizeof(sTextEntryLineOfText));
		memset(sTempTextEntryLineOfText, 0x0, sizeof(sTempTextEntryLineOfText));
		cJSON *uText5 = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG5]);
		if (uText5)
		{
			len = fnAsciiBcdToPackedBcd(sTempTextEntryLineOfText, sizeof(sTempTextEntryLineOfText), uText5->valuestring, TRUE);
			if(len > 0 && len <= 40){
				EndianAwareArray16Copy(sTextEntryLineOfText, sTempTextEntryLineOfText, len);
				status = fnSetTextEntryLineOfText(0,4,(UNICHAR *)sTextEntryLineOfText);
				if(status == TEXT_ENTRY_SUCCESS)
					passedText[4] = TRUE;
			}
			else
			{
				passed = FALSE;
				cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG5], uText5->valuestring));
				return passed;
			}

		}

		if(passedText[0] == TRUE ||
				passedText[1] == TRUE ||
				passedText[2] == TRUE ||
				passedText[3] == TRUE ||
				passedText[4] == TRUE)
		{
				UINT8   sTextEntryName[ TEXT_NAME_LEN * 2 ];
				UINT8   sTempTextEntryName[ TEXT_NAME_LEN * 2 ];
				memset(sTextEntryName, 0x0, sizeof(sTextEntryName));
				memset(sTempTextEntryName, 0x0, sizeof(sTempTextEntryName));
				len = fnAsciiBcdToPackedBcd(sTempTextEntryName, sizeof(sTempTextEntryName), "005000690074006E00650079005F0054006500780074", TRUE);
				EndianAwareArray16Copy(sTextEntryName, sTempTextEntryName, len);
				status = fnSetTextEntryName(0,(UINT16*)sTextEntryName);
				if(status == TEXT_ENTRY_SUCCESS)
				{
					passed = fnAISetActive(TEXT_COMP_TYPE, 1);
					IG_DuckTextEntry(FALSE);

				}

		 }

		if(passed == FALSE)
		{
			passed = fnAISetActive(TEXT_COMP_TYPE, 0);
		}
	}
	else
	{
		passed = FALSE;
		cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_TEXTMSG1], "Feature Not Enabled"));
	}
    return passed;
}


static BOOL validateSubmissionDate(cJSON *root, cJSON *invalid)
{


	BOOL 			passed = FALSE;
    DATETIME        rOurDate;
	DATETIME 		dateTimePtr;
	int				days = 0;
	long            timeDiffInSecs;     /* For time difference calc */

    cJSON *submissionDate = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_SUBDATE]);
    if (submissionDate)
    {

		char pTempDate[11];
		memset(pTempDate, 0x0, sizeof(pTempDate));
		memcpy(pTempDate, submissionDate->valuestring, strlen(submissionDate->valuestring));
		passed = asciiDateToDateTime(&dateTimePtr, pTempDate);

		if(passed == TRUE)
		{

			//get the system date and zero out the time
			GetSysDateTime(&rOurDate, ADDOFFSETS, GREGORIAN);
			rOurDate.bHour = 0;
			rOurDate.bMinutes = 0;
			rOurDate.bSeconds = 0;

			//calculate difference between our date and the date of submission
			CalcDateTimeDifference(&rOurDate, &dateTimePtr, &timeDiffInSecs);

			if(timeDiffInSecs >= 0)
			{
				days = timeDiffInSecs / SECONDS_IN_1DAY;

				passed = SetPrintedDateAdvanceDays((short)days);

				if(passed == FALSE){
					//Add failed Submission Date to the invalid list
					cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_SUBDATE], submissionDate->valuestring));

				}
			}
			else{

				passed = FALSE;
				//Add failed Submission Date to the invalid list
				cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_SUBDATE], submissionDate->valuestring));
			}

		}
		else
		{

			//Add failed Submission Date to the invalid list
			cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_SUBDATE], "Missing Submission Date"));

		}

	}
	else
	{
		passed = SetPrintedDateAdvanceDays(0);
	}

    return passed;
}


static BOOL validateIndiciaString(cJSON *root, cJSON *invalid)
{

	short 			len;
	BOOL 			passed = FALSE;
	UINT8 			sIndiciaString[MAX_INDICIA_STRING_LEN * 2 + 1];

	memset(goToReadyReq.indiciaString, 0x0, sizeof(goToReadyReq.indiciaString));
	memset(sIndiciaString, 0x0, sizeof(sIndiciaString));

	cJSON *indiciaString1 = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_INDICIASTR]);
    if (indiciaString1)
    {

		len = fnAsciiBcdToPackedBcd(sIndiciaString, sizeof(sIndiciaString), indiciaString1->valuestring, TRUE);
		if (len > 0)
		{
			EndianAwareArray16Copy(goToReadyReq.indiciaString, sIndiciaString, len);
			passed = TRUE;
		}
		else
		{
			//Add failed Indicia String1 to the invalid list
			cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_INDICIASTR], "Missing Indicia String"));
		}
    }
	else
	{
		SET_SPACE_UNICODE(goToReadyReq.indiciaString, MAX_INDICIA_STRING_LEN);
		passed = TRUE;
	}

    return passed;
}



/* **********************************************************************
 FUNCTION NAME: validateIndiciaStrings

 PURPOSE:       Save indicia strings to local goToReadyReq structure   
                    
 AUTHOR:        Steve Terebesi  

* INPUTS:       root    - pointer to message
                invalid - pointer to invalid list

* OUTPUTS:      Message successfully parsed (TRUE/FALSE)
 *************************************************************************/
static BOOL validateIndiciaStrings(cJSON *root, cJSON *invalid)
{
	short 			len;
	BOOL 			passed = FALSE;
	UINT8 			sIndiciaString[MAX_INDICIA_STRING_LEN * 2 + 1];
    UINT8           i;
    UINT8           size;

//  Check to see if the IndiciaStringList tag is in the message
    if (cJSON_HasObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_INDICIASTR_LIST]))
    {
//      Clear indicia string list
        for (i = 0; i < MAX_INDICIA_STRINGS; i++)
        {
    	    memset(goToReadyReq.indiciaStringList[i], 0x0, sizeof(goToReadyReq.indiciaStringList[i]));
        }

//      Get the indicia string list from the message
    	cJSON *indiciaList = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_INDICIASTR_LIST]);

//      Get the number of strings in the list
    	size = cJSON_GetArraySize(indiciaList);

//      Make sure the list does not exceed the max number of strings supported
        if (size > MAX_INDICIA_STRINGS)
        {
            size = MAX_INDICIA_STRINGS;
        }

//      Grab each item from the list and copy to the goToReady structure
        for (i = 0; i < size; i++)
        {
    		cJSON *item = cJSON_GetArrayItem(indiciaList, i);

            if( (item->valuestring != NULL_PTR)	&& (*item->valuestring != NULL_VAL) )
            {
        	    len = fnAsciiBcdToPackedBcd(sIndiciaString, sizeof(sIndiciaString), item->valuestring, TRUE);
    		    EndianAwareArray16Copy(goToReadyReq.indiciaStringList[i], sIndiciaString, len);
            }
            else
            {
        		SET_SPACE_UNICODE(goToReadyReq.indiciaStringList[i], MAX_INDICIA_STRING_LEN);
            }
        }
		passed = TRUE;
    }
    else
    {
//      If tag is not in message, set the first indicia string to blank
		SET_SPACE_UNICODE(goToReadyReq.indiciaStringList[0], MAX_INDICIA_STRING_LEN);
        passed = TRUE;
    }

    return passed;
}

/* **********************************************************************
 FUNCTION NAME: validateMiscData

 PURPOSE:       Save misc data to local goToReadyReq structure   
                    
 AUTHOR:        Ivan Le Goff  

* INPUTS:       root    - pointer to message
                invalid - pointer to invalid list

* OUTPUTS:      Message successfully parsed (TRUE/FALSE)
 *************************************************************************/
static BOOL validateMiscData(cJSON *root, cJSON *invalid)
{
	short 			len;
	BOOL 			passed = FALSE;
	unsigned char 	cMiscData;
    UINT8           i;
    UINT8           size;

//  Check to see if the MiscData tag is in the message
    if (cJSON_HasObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_MISC_DATA]))
    {
//      Clear Misc Data list
        for (i = 0; i < MAX_MISC_DATA; i++)
        {
    	    goToReadyReq.miscData[i] = 0x0;
        }

//      Get the misc data from the message
    	cJSON *miscData = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_MISC_DATA]);

//      Get the number of misc data in the list
    	size = cJSON_GetArraySize(miscData);

//      Make sure the list does not exceed the max number of strings supported
        if (size > MAX_MISC_DATA)
        {
            size = MAX_MISC_DATA;
        }

//      Grab each item from the list and copy to the goToReady structure
        for (i = 0; i < size; i++)
        {
    		cJSON *item = cJSON_GetArrayItem(miscData, i);

            if (item->type == cJSON_Number)
            {
        	    goToReadyReq.miscData[i] = (unsigned char) item->valueint;
            }
            else
            {
        		goToReadyReq.miscData[i] = 0;
            }
        }
		passed = TRUE;
    }
    else
    {
//      If tag is not in message, set the first misc data to zero
		for (i = 0; i < MAX_MISC_DATA; i++)
        {
    	    goToReadyReq.miscData[i] = 0x0;
        }
        passed = TRUE;
    }

    return passed;
}

static BOOL validateProdId(cJSON *root, cJSON *invalid)
{

	short 			len;
	BOOL 			passed = FALSE;
	char 			sProdId[SIZE_32+1];

	memset(goToReadyReq.prodId, 0x0, sizeof(goToReadyReq.prodId));
	memset(sProdId, 0x0, sizeof(sProdId));

	cJSON *prodId = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_PRODID]);
    if (prodId)
    {

		(void) strcpy(sProdId, prodId->valuestring);
		len = strlen(sProdId);
		if (len > 0)
		{
			EndianAwareCopy(goToReadyReq.prodId, sProdId, len);
			passed = TRUE;
		}
		else
		{
			//Add failed ProdId to the invalid list
			cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_PRODID], "Missing ProdId"));
		}
    }
	else
	{
		memset(goToReadyReq.prodId, 0, SIZE_32);
		passed = TRUE;
	}

    return passed;
}

char* fnGetProdId()
{
	return goToReadyReq.prodId;
}

unichar *fnGetIndiciaString()
{
	return goToReadyReq.indiciaString;
}



/* **********************************************************************
 FUNCTION NAME: fnfGetSelectedIndiciaString

 PURPOSE:       Retrieve the selected indicia string from string list   
                    
 AUTHOR:        Steve Terebesi  

* INPUTS:       index - index of selected string in string list

* OUTPUTS:      unichar - selected indicia string
 *************************************************************************/
unichar *fnfGetSelectedIndiciaString(UINT8 index)
{
//  Check index.  If greater than max strings, return first string (index = 0)
    if (index > MAX_INDICIA_STRINGS)
    {
        index = 0;
    }
    
    return ( &goToReadyReq.indiciaStringList[index][0] );    
}

/* **********************************************************************
 FUNCTION NAME: fnfGetSelectedMiscData

 PURPOSE:       Retrieve the selected misc data from list   
                    
 AUTHOR:       Ivan Le Goff 

* INPUTS:       index - index of selected misc data in list

* OUTPUTS:      unsigned char - selected misc data 
 *************************************************************************/
unsigned char *fnfGetSelectedMiscData(UINT8 index)
{
//  Check index.  If greater than max elements, return first element (index = 0)
    if (index > MAX_MISC_DATA)
    {
        index = 0;
    }
    
    return ( &goToReadyReq.miscData[index]);    
}



/* Send print message to SYS task that depends on params being OK */
static BOOL sendPrintMessage(BOOL paramsOk)
{
	BOOL msgStat;
    UINT8     pMsgData[2];    /* send 2 bytes */

    if(paramsOk == TRUE)
    {
		pMsgData[0] = OIT_ENTER_PRINT_RDY;
    }
    else
    {
    	pMsgData[0] = OIT_LEFT_PRINT_RDY;
    }
    pMsgData[1] = 0;

	msgStat = OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);
#ifdef NO_PRINTER
	if ((msgStat == TRUE) && (paramsOk == TRUE))
    {
		OSWakeAfter(620);
		pMsgData[0] = 0;
		msgStat = OSSendIntertask(CM, BJCTRL, 0x80, BYTE_DATA, pMsgData, 2);
    }
#endif
    if(msgStat == TRUE)
    {
    	msgStat = paramsOk;
    }
    return msgStat;
}


static BOOL prepareForIndiciaPrinting(cJSON *root, cJSON *invalid)
{
	BOOL passed = FALSE;
	BOOL paramsOk = TRUE;
	double dDR = 0;
	double dPostage = 0;
	
 	//Check rate version which should already be in rate manager if provided
	passed = checkRateVersion(invalid);
	paramsOk = paramsOk && passed;

	passed = validateIndiciaType(root, invalid);
	paramsOk = paramsOk && passed;

	passed = validatePostage(root, invalid);
	paramsOk = paramsOk && passed;

//	passed = validateBaseRate(root, invalid);
//	paramsOk = paramsOk && passed;

	passed = validateNumDec(root, invalid);
	paramsOk = paramsOk && passed;

	// Checks if this is required (for example: Canada)
	passed = validateServicCodeNumeric( root, invalid );
	paramsOk = paramsOk && passed;
 
	if (paramsOk == TRUE)
	{
		goToReadyReq.postageValue = adjustFINTWithNumDec(goToReadyReq.postageValue, goToReadyReq.numDec);

		//Set postage/debit value
		passed = (NU_SUCCESS == smgr_set_setting_data(SETTING_ITEM_POSTAGE, &(goToReadyReq.postageValue), sizeof(UINT32)));

		if(passed == TRUE)
		{
            fnOITSetPrintMode(PMODE_MANUAL);
            fnCheckPSDState();
            fnCheckInspection();
            fnPostCheckInspection();
			dDR = fnBinFive2Double(mStd_DescendingReg);
			dPostage = (double) goToReadyReq.postageValue;
			passed = (dDR >= dPostage);

			if(passed == TRUE)
			{
				fnClearDisabledStatus(DCOND_INSUFFICIENT_FUNDS);
			}
			else
			{
				fnPostDisabledStatus(DCOND_INSUFFICIENT_FUNDS);
			}
		}
		paramsOk = paramsOk && passed;

		if(passed == FALSE)
		{
			//Setting postage failed
			cJSON_AddItemToArray(invalid, CreateNumberItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_POSTAGE], goToReadyReq.postageValue));
		}
	}


	passed = validateAdGraphic(root, invalid);
	paramsOk = paramsOk && passed;

	passed = validateInscGraphic(root, invalid);
	paramsOk = paramsOk && passed;

	if(validateTextMessageInputs(root, invalid) == TRUE){
		passed = validateTextMessage(root, invalid);
		paramsOk = paramsOk && passed;
	}

	passed = validateSubmissionDate(root, invalid);
	paramsOk = paramsOk && passed;

	passed = validateFeeList(root, invalid);
	paramsOk = paramsOk && passed;

	passed = validateIndiciaString(root, invalid);
	paramsOk = paramsOk && passed;

	passed = validateIndiciaStrings(root, invalid);
	paramsOk = paramsOk && passed;

	passed = validateMiscData(root, invalid);
	paramsOk = paramsOk && passed;

	passed = validateProdId(root, invalid);
	paramsOk = paramsOk && passed;

	passed = validateDateDucking(root, invalid);
	paramsOk = paramsOk && passed;

	if (paramsOk == TRUE)
	{
		//Set print mode to Indicia
	    fnOITSetPrintMode(PMODE_MANUAL);
	    fnSendBobPrintMode(BOB_DEBIT_FUNDS);
	}

	passed = validateSpecialPrinting(invalid);
	paramsOk = paramsOk && passed;

    return sendPrintMessage(paramsOk);
}

static BOOL sendTestPrintMessage(void)
{
	BOOL msgStat;
    UINT8     pMsgData[2];    /* send 2 bytes */

    // Initiate test print
    pMsgData[0] = OIT_PRINT_ENV_TEST;
    pMsgData[1] = 0;
    msgStat = OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);

    return msgStat;
}

static BOOL validateAdOnlyInputs(cJSON *root, cJSON *invalid)
{
	BOOL passed = FALSE;

	cJSON *adDataNameRev = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_ADGRAPHIC]);
    if (adDataNameRev)
    {
    	passed = TRUE;
    }
    else
    {
    	fnAISetActive(AD_COMP_TYPE, 0);		// Turn off ad
    	cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_ADGRAPHIC], ""));
		passed = FALSE;
	}
    return passed;
}

static BOOL sendAdOnlyMessage(cJSON *root, cJSON *invalid)
{
	BOOL paramsOk = TRUE;
	BOOL passed = FALSE;
	BOOL validAd = FALSE;
	BOOL validText = FALSE;
	BOOL validPostage = FALSE;



	validText = validateTextMessageInputs(root, invalid);
	validAd = validateAdOnlyInputs(root, invalid);
	validPostage = validatePostage(root, invalid);
	if(validText != TRUE && validAd != TRUE)
	{
		paramsOk = FALSE;
		return sendPrintMessage(paramsOk);
	}

	if(validText == TRUE)
	{
		passed = validateTextMessage(root, invalid);
		paramsOk = paramsOk && passed;
	}

	if(validAd == TRUE)
	{
		passed = validateAdGraphic(root, invalid);
		paramsOk = paramsOk && passed;
	}

	if(validPostage == TRUE)
	{
		cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_POSTAGE], ""));
		paramsOk = FALSE;
		return sendPrintMessage(paramsOk);

	}
	if(paramsOk == TRUE){
			fnOITSetPrintMode(PMODE_AD_ONLY);
		}

	return sendPrintMessage(paramsOk);

}

static BOOL sendDateTimeMessage(cJSON *root, cJSON *invalid)
{
	BOOL paramsOk = TRUE;
	BOOL passed = FALSE;
	BOOL haveAd = FALSE;
	BOOL haveText = FALSE;
	BOOL validPostage = FALSE;


	haveText = validateTextMessageInputs(root, invalid);
	haveAd = validateAdOnlyInputs(root, invalid);
	// Doing validation only after this point so clear invalid field
	clearJSONArray(invalid);

	validPostage = validatePostage(root, invalid);
	if(validPostage == TRUE)
	{
		paramsOk = FALSE;
		cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_POSTAGE], ""));
		return sendPrintMessage(paramsOk);

	}

	if(haveText != TRUE && haveAd != TRUE)
	{
		passed = validateSubmissionDate(root, invalid);
		paramsOk = paramsOk && passed;
		if(paramsOk == TRUE)
		{
			fnOITSetPrintMode(PMODE_TIME_STAMP);
			return sendPrintMessage(paramsOk);
		}
		else{
			fnOITSetPrintMode(PMODE_TIME_STAMP);
			return paramsOk;
		}

	}
	else
	{

		if(haveText == TRUE)
		{
			passed = validateTextMessage(root, invalid);
			paramsOk = paramsOk && passed;
		}

		if(haveAd == TRUE)
		{
			passed = validateAdGraphic(root, invalid);
			paramsOk = paramsOk && passed;
		}

		if(paramsOk == TRUE){
			passed = validateSubmissionDate(root, invalid);
			paramsOk = paramsOk && passed;
			fnOITSetPrintMode(PMODE_AD_TIME_STAMP);
			}

	}

    return sendPrintMessage(paramsOk);
}

static BOOL sendSealOnlyMessage(void)
{
	BOOL msgStat;
    UINT8     pMsgData[2];    /* send 2 bytes */

    pMsgData[0] = OIT_PRINT_NO_INDICIA;
    pMsgData[1] = 0;
    msgStat = OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);

    return msgStat;
}

static BOOL validateReportName(cJSON *root, cJSON *invalid)
{
	int  i = 0;
	char *pTemp = NULL_PTR;
	BOOL passed = TRUE;
	BOOL valid_type = FALSE;

	// get report type
	if (cJSON_HasObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_REPORTNAME]))
	{
		pTemp = cJSON_GetObjectItem(root,  mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_REPORTNAME])->valuestring;
	}
	else
	{
		//Add missing required item to invalid list
		passed = FALSE;
		cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_REPORTNAME], ""));
	}

    if (pTemp == NULL_PTR) pTemp = ""; // Prevent crash below
	// validate report type value
	for(i=0; i<NELEMENTS(mach_ctrl_reporttypes_to_string_tbl) ; i++)
	{
		if (0 == NCL_Stricmp(mach_ctrl_reporttypes_to_string_tbl[i], pTemp))
		{
			valid_type = TRUE;
			break;
		}
	}
	if (valid_type == TRUE)
	{ // get reportID enum; convert to switch statement if more report types added
		goToReadyReq.reportID = i;
	}
	else
	{
		//Add invalid item to invalid list
		passed = FALSE;
		cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_REPORTNAME], pTemp));
	}

    return passed;
}

static BOOL validateReportLanguage(cJSON *root, cJSON *invalid)
{
	UINT32 tempVal;
	BOOL passed = TRUE;

	// get report language
	if (cJSON_HasObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_REPORTLANGUAGE]))
	{
		tempVal = (UINT32) cJSON_GetObjectItem(root,  mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_REPORTLANGUAGE])->valueint;
	}
	else
	{
		//Add missing required item to invalid list
		passed = FALSE;
		cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_REPORTLANGUAGE], ""));
	}

	// validate report language
	if (passed == TRUE)
	{
		if (tempVal < 2)
		{
			goToReadyReq.reportLang = (unsigned char) tempVal;
		}
		else
		{
			passed = FALSE;
			cJSON_AddItemToArray(invalid, CreateNumberItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_REPORTLANGUAGE], tempVal));
		}
	}

    return passed;
}

static BOOL validateBatchCount(cJSON *root, cJSON *invalid)
{
	UINT32 tempVal;
	BOOL passed = TRUE;

	// get batch count
	if (cJSON_HasObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_BATCHCOUNT]))
	{
		tempVal = (UINT32) cJSON_GetObjectItem(root,  mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_BATCHCOUNT])->valueint;
	}
	else
	{
		//Add missing required item to invalid list
		passed = FALSE;
		cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_BATCHCOUNT], ""));
	}

	// no validation of number
	if (passed == TRUE)
	{
		goToReadyReq.batchCount = tempVal;
	}

    return passed;
}

static BOOL validateBatchValue(cJSON *root, cJSON *invalid)
{
	UINT32 tempVal;
	BOOL passed = TRUE;

	// get batch count
	if (cJSON_HasObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_BATCHVALUE]))
	{
		tempVal = (UINT32) cJSON_GetObjectItem(root,  mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_BATCHVALUE])->valueint;
	}
	else
	{
		//Add missing required item to invalid list
		passed = FALSE;
		cJSON_AddItemToArray(invalid, CreateStringItem( mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_BATCHVALUE], ""));
	}

	// adjust number with numdec; no validation of number
	if (passed == TRUE)
	{
		goToReadyReq.batchValue = adjustFINTWithNumDec(tempVal, goToReadyReq.numDec);
	}

    return passed;
}

static BOOL validateSubmissionDateInputs(cJSON *root, cJSON *invalid)
{

	BOOL passed = TRUE;

	cJSON *submissionDate = cJSON_GetObjectItem(root, mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_SUBDATE]);
    if (submissionDate)
    {
    	passed = TRUE;
    	bIsCorrecteDate = TRUE;
    }
    else
    {
		passed = FALSE;
		cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_SUBDATE], "Missing Submission Date"));

    }

    return passed;

}

// If rate version was provided, check if version has changed.
// If rate version has changed and TR uploads are pending, then reject GoToReady and force Upload Required.
// If rate version has changed and TR uploads are not pending, then apply new rate version.
// If rate version not provided, use previously provided one.  If no previous one, then fail.
static BOOL checkRateVersion(cJSON *invalid)
{
	BOOL passed = TRUE;

	STATUS status;
	uint8_t buffer[64 + 1]; //last byte for null terminator
	uint16_t length;
	uint32_t type;
	bool is_set;

	smgr_get_settings_info( SETTING_ITEM_RATE_EFF_DATE, &length, &type, &is_set);
	if (is_set)
	{
		memset(buffer, 0x0, sizeof(buffer)); // clear buffer before every read
		status = smgr_get_setting_data(SETTING_ITEM_RATE_EFF_DATE, buffer, length);
		if (status == NU_SUCCESS)
		{
			//Check for version change
			if (trmRateVerChange((char*)buffer))
				{
					if (trmTRUploadPending())
					{// don't allow rate version change if TRs not uploaded
						passed = FALSE;
						cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_RATE_EFF_DATE],
																		(char *) api_status_to_string_tbl[API_STATUS_TRM_UPLOAD_REQ]));
						UploadReqForRateVers = TRUE; //used to generate base event after response sent
					}
					else
					{
						trmApplyRateVer((char*)buffer);
					}
				}
		}
		else
		{
			passed = FALSE;
			cJSON_AddItemToArray(invalid, CreateStringItem(mach_ctrl_fields_to_string_tbl[MACH_CTRL_FIELDS_RATE_EFF_DATE], "Internal Error"));
		}
	}
	else
	{// Ratever not provided - Reject if the ratever stored in FRAM is not populated from a previous call
		if (!trmRateVerValid())
			passed = FALSE;
	}

    return passed;
}



static BOOL validateReportInputs(cJSON *root, cJSON *invalid)
{
	BOOL passed = FALSE;
	BOOL paramsOk = TRUE;

	passed = validateReportName(root, invalid);
	paramsOk = paramsOk && passed;

	passed = validateReportLanguage(root, invalid);
	paramsOk = paramsOk && passed;

	if (goToReadyReq.reportID == MACH_CTRL_REPORTTYPES_FUNDS)
	{
		passed = validateBatchCount(root, invalid);
		paramsOk = paramsOk && passed;

		passed = validateNumDec(root, invalid);
		paramsOk = paramsOk && passed;

		passed = validateBatchValue(root, invalid);
		paramsOk = paramsOk && passed;
	}
	else if (goToReadyReq.reportID == MACH_CTRL_REPORTTYPES_DATECORRECTION){
		passed = validateSubmissionDateInputs(root, invalid);
		paramsOk = paramsOk && passed;

	}
    return paramsOk;
}

static BOOL prepareForReportPrinting(cJSON *root, cJSON *invalid)
{
	BOOL      msgStat;
    UINT8     pMsgData[2];    /* send 2 bytes */

    // validate inputs
	msgStat = validateReportInputs(root, invalid);
	if(msgStat == TRUE)
	{
		//TODO - replace with settings manager
		// set language first
		(void) fnCMOSSetLanguage(goToReadyReq.reportLang);

		//set current printing report
		if (goToReadyReq.reportID == MACH_CTRL_REPORTTYPES_FUNDS)
		{
			fnSetCurReportFunds(goToReadyReq.batchCount, goToReadyReq.batchValue );
		}
		else if(goToReadyReq.reportID ==  MACH_CTRL_REPORTTYPES_REFILL)
		{
			fnSetCurReportRefill();
		}
		else if(goToReadyReq.reportID ==  MACH_CTRL_REPORTTYPES_DATECORRECTION){
			msgStat = validateSubmissionDate(root, invalid);
			if(msgStat == TRUE){
				fnSetCurReportDateCorrection();
			}
		}
		else if (goToReadyReq.reportID ==  MACH_CTRL_REPORTTYPES_REFUND)
		{
			fnSetCurRefundReceipt();
		}

		if(msgStat == TRUE)
		{
			pMsgData[0] = OIT_PREP_FOR_RPT_PRINTING;
			pMsgData[1] = 0;
			msgStat = OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);
		}
	}
    return msgStat;
}


void ClearSettings(void)
{
	UINT32 postage = 0;

	(void) fnDateDuckOff();
	(void) smgr_set_setting_data(SETTING_ITEM_POSTAGE, &postage, sizeof(UINT32));
	smgr_invalidate_all();
}

// Checks to see if CM state is valid for printing;
// More checks can be added here in the future
static BOOL isPrinterAvailable(void)
{
    BOOL      avail  = TRUE;

    //if OIT send OIT_EVENT to SYS to go to print ready status when transport is stopping and 
    //printer is moving to home position,  CM will not call fnSetNmlMode or fnSetSlOnlyMode
    //to send Set Mode request to PM for this state. 
    if (fnCmGetCrntState() == s259_StopTransport)
    {
        dbgTrace(DBG_LVL_ERROR,"Error: isPrinterAvailable - CM state = %d\n", fnCmGetCrntState()); 
        avail = FALSE;
    }

    return avail;
}

void on_GoToReadyReq(UINT32 handle, cJSON *root)
{
	BOOL msgStat = TRUE;
	bGoToReady = TRUE;
	cJSON *rspMsg = cJSON_CreateObject();
	cJSON *invalid = cJSON_CreateArray();
	WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

	//Clear Settings in settings manager
	ClearSettings();
	fnClearDisabledStatus(DCOND_INSUFFICIENT_FUNDS); //Clear insufficient funds here because it will not be cleared with the other disabling conditions
	//Complete any pending tx file writes if necessary may be due to power fail
	//Initialize Package info
    trmInitializeDcapPackageInfo();

    fnClearDisabledStatus(DCOND_PAPER_IN_TRANSPORT);//Clear Paper in Transport

    //If No PSD, don't print/seal any mail to avoid the error debit time in transaction record.
    fnCheckPSDPresent();
	 
    fnUpdateJsonErrorStatus(invalid);
    int size = cJSON_GetArraySize(invalid);
    if(size != 0)
    {
    	msgStat = FALSE;
    }

    if (isTier3TransactionInProgress())
    {
		cJSON_AddItemToArray(invalid, CreateStringItem(api_status_to_string_tbl[API_TRANSACTION_IN_PROGRESS], "")); 
    	msgStat = FALSE;
    }	 
    if (!RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_GoToReadyReq))
	{
		//Add missing required item to invalid list
		cJSON_AddItemToArray(invalid, CreateStringItem(required_fields_tbl_GoToReadyReq.fieldnames[0], ""));
		msgStat = FALSE;
	}
    else
    {
		if(msgStat == TRUE)
		{
   			if (smgr_set_values(root) != NU_SUCCESS)
   			{
   				msgStat = FALSE;
   				cJSON_AddItemToArray(invalid, settingsMgrErrorItem);
   			}
		}
    }

	if(msgStat == TRUE)
	{
		// validate PrintMode
		msgStat = validatePrintMode(root, invalid);
	}

	//Check for TRC files from previous files are deleted.
	if(trmCheckForUploadCleanup() != SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR,"Error: GoToReadyReq- Pending cleanup after upload\n");
		//cJSON_AddItemToArray(invalid, CreateStringItem("Incomplete Cleanup"), ""));
		msgStat = FALSE;
	}

    if (isPrinterAvailable() == FALSE) 
    {
        dbgTrace(DBG_LVL_ERROR,"Error: GoToReadyReq- Printer is not available");
        msgStat = FALSE;
    }

	if(msgStat == TRUE)
	{
		switch (goToReadyReq.printMode)
		{
			case MACH_CTRL_PRINTMODES_INDICIA:
				{// Indicia
					msgStat = prepareForIndiciaPrinting(root, invalid);

					if (msgStat != SUCCESSFUL)
					{
       					dbgTrace(DBG_LVL_ERROR,"GoToReadyReq: Failed to send Indicia message\n");
					}
				}
				break;
			case MACH_CTRL_PRINTMODES_TESTPRINT:
				{//TestPrint mode
					msgStat = sendTestPrintMessage();
					if (msgStat != SUCCESSFUL)
					{
        				dbgTrace(DBG_LVL_ERROR,"GoToReadyReq: Failed to send TestPrint message\n");
					}

				}
				break;
			case MACH_CTRL_PRINTMODES_ADONLY:
				{//AdOnly mode
					msgStat = sendAdOnlyMessage(root, invalid);
					if (msgStat != SUCCESSFUL)
					{
						dbgTrace(DBG_LVL_ERROR,"GoToReadyReq: Failed to send AdOnly message\n");
					}
				}
				break;
			case MACH_CTRL_PRINTMODES_DATETIME:
				{//DateTime mode
					msgStat = sendDateTimeMessage(root, invalid);
					if (msgStat != SUCCESSFUL)
					{
						dbgTrace(DBG_LVL_ERROR,"GoToReadyReq: Failed to send DateTime message\n");
					}
				}
				break;
			case MACH_CTRL_PRINTMODES_SEALONLY:
				{//SealOnly mode
					msgStat = sendSealOnlyMessage();
					if (msgStat != SUCCESSFUL)
					{
						dbgTrace(DBG_LVL_ERROR,"GoToReadyReq: Failed to send SealOnly message\n");
					}
				}
				break;
			case MACH_CTRL_PRINTMODES_REPORT:
				{//Report mode
					msgStat = prepareForReportPrinting(root, invalid);
					if (msgStat != SUCCESSFUL)
					{
						dbgTrace(DBG_LVL_ERROR,"GoToReadyReq: Failed to send Report message\n");
					}
				}
				break;
			default:
        		// should not get here
				msgStat = FALSE;
				//Invalid value already added to invalid list
				break;
		}
	}

	if(msgStat == TRUE)
	{

		cJSON_Delete(invalid);
		generateResponse(&entry, rspMsg, "GoToReady", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
	}
	else
	{
		unsigned char   pMsgData[2];

		cJSON_AddItemToObject(rspMsg, "Invalid", invalid);
		generateResponse(&entry, rspMsg, "GoToReady", api_status_to_string_tbl[API_STATUS_NOT_ACCEPTED], root);
		bGoToReady = FALSE;

		NU_Sleep(20);
		//We had issues clear Settings in settings manager
		ClearSettings();

		//Send mode change msg to OIT task to make sure we leave print ready
		pMsgData[0] = OIT_LEFT_PRINT_RDY; //OIT_LEFT_PRINT_RDY;
		pMsgData[1] = 0;
		(void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);

		//generate Upload Required base event if rate version changed and uploads pending
		// this is done after response sent and after ready state exited in order to not confuse tablet
		if (UploadReqForRateVers == TRUE)
			{
			NU_Sleep(20);
			fDCAPUploadReqShown = TRUE;
			SendBaseEventToTablet(BASE_EVENT_TX_UPLOAD_REQUIRED, NULL);
			UploadReqForRateVers = FALSE;
			}

	}

}


void on_GoToNotReadyReq(UINT32 handle, cJSON *root)
{
       unsigned char   pMsgData[2] = {OIT_LEFT_PRINT_RDY, 0};
       int i = 0;
	   UINT8       pDescRegValue[SPARK_MONEY_SIZE];

       bGoToReady = FALSE;
       cJSON *rspMsg = cJSON_CreateObject();
       WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

       // Wait up to one second in case mailpiece processing is in progress
       while (sSystemStatus.fPieceInProgress == TRUE)
       {
              NU_Sleep(3); //minimum of 20 ms; average 25 ms
              i++;
              if(i == 50)
                     break;
       }

       //Clear Settings in settings manager
       ClearSettings();
       if(fnIsDisabledStatusSet(DCOND_INSUFFICIENT_FUNDS) == TRUE) //  Only if the meter is in Insufficient funds disabling status we do the below check.
       {
           if (fnBobGetDescendingReg(pDescRegValue) == TRUE) // get the DR value
           {
               if (fnBinFive2Double(pDescRegValue)>0) // if there is sufficient funds, then clear the disabling condition.
               {
                   fnClearDisabledStatus(DCOND_INSUFFICIENT_FUNDS);
               }
           }
       }

       //Turn off mail simulation in case it was on
       fnSetMailSimuPara(FALSE, 0, 0, TRUE);

       //Send mode change msg to OIT task
       pMsgData[0] = OIT_LEFT_PRINT_RDY; //OIT_LEFT_PRINT_RDY;
       pMsgData[1] = 0;
       (void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);

       fnSendStopMailRequest();

       generateResponse(&entry, rspMsg, "GoToNotReady", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
}


void on_GoToMFGReq(UINT32 handle, cJSON *root)
{
	unsigned char   pMsgData[2] = {OIT_ENTER_MANUFACTURING, 0};

	cJSON *rspMsg = cJSON_CreateObject();
	WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

	//Send mode change msg to OIT task
	pMsgData[0] = OIT_ENTER_MANUFACTURING;
	pMsgData[1] = 0;
	(void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);

	generateResponse(&entry, rspMsg, "GoToMFGReq", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);

}


void on_StartReq(UINT32 handle, cJSON *root)
{
	unsigned char   pMsgData[2] = {OIT_RUNNING, 0};

	cJSON *rspMsg = cJSON_CreateObject();
	WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

	//Send mode change msg to OIT task
	OSSendIntertask (OIT, SYS, OIT_CHANGE_MODE, BYTE_DATA, pMsgData, 2);

	generateResponse(&entry, rspMsg, "Start", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);

	//TODO: Start printing

    pMsgData[0] = OIT_ENTER_PRINT_RDY;
    pMsgData[1] = 0;
    (void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);
#ifdef NO_PRINTER
	OSWakeAfter(620);
	pMsgData[0] = 0;
	(void)OSSendIntertask(CM, BJCTRL, 0x80, BYTE_DATA, pMsgData, 2);
#endif

}

void on_StopReq(UINT32 handle, cJSON *root)
{
	unsigned char   pMsgData[2] = {OIT_PRINT_READY, 0};

	cJSON *rspMsg = cJSON_CreateObject();
	WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

	//Send mode change msg to OIT task
	OSSendIntertask (OIT, SYS, OIT_CHANGE_MODE, BYTE_DATA, pMsgData, 2);

	generateResponse(&entry, rspMsg, "Stop", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// Power management /////////////////////////////////////////////////////////////////

/* **********************************************************************
// DESCRIPTION: MonitorPowerButtonForExtendedOffPress.
//
// Monitors the power button  (GPIO3:18), if this is kept pressed down for
// the determined amount of time the base power supply will be turned off.
//
// INPUTS: none
// RETURNS: none
// **********************************************************************/
// this task is assumed to be called a number of times a second, continuously.
#define SLEEP_PRESS_DELAY	POWEROFF_CALL_FREQ/2
static UINT16 globalPowerButtonExtendedPressCount	= 0 ;
void MonitorPowerButtonForExtendedOffPress()
{
	if(fnGetSysTaskPwrupSeqCompleteFlag() == TRUE)
	{
        switch(getShutDownState())
        {
            case SHUTDOWNSTATE_AWAKE        :   /* fall through */
            case SHUTDOWNSTATE_GOINGTOSLEEP :   /* fall through */
            case SHUTDOWNSTATE_SLEEPING     :
                {
                    if(readSimpleGPIOPin(HAL_POWER_BUTTON_GPIO, HAL_POWER_BUTTON_GPIOPIN) == HAL_POWERBUTTON_PRESSED)
                    {
                        // Issue event message to tablet....
                        if(globalPowerButtonExtendedPressCount == 0)
                        {
                            storeJAMLeverState();
                        }
                        globalPowerButtonExtendedPressCount++;
                        if(globalPowerButtonExtendedPressCount >= (POWEROFF_CALL_FREQ * POWEROFF_SECONDS))
                        {
                            globalPowerButtonExtendedPressCount = 0;
                            setShutDownState(SHUTDOWNSTATE_SAVINGLOGS);

                        }
                    }
                    else // if button has been released
                    {
                        // apply a little debounce delay... about 1/2 second
                        if((globalPowerButtonExtendedPressCount > SLEEP_PRESS_DELAY) && (globalPowerButtonExtendedPressCount < (POWEROFF_CALL_FREQ * POWEROFF_SECONDS)))
                        {
                            SendBaseEventToTablet(BASE_EVENT_BUTTON_SHORT_PRESS, NULL);
                                /**
                                  if(getShutDownState() == SHUTDOWNSTATE_AWAKE)
                                  {
                                // execute any goto sleep code here.
                                BaseGotoSleep() ;
                                }
                                else if(getShutDownState() == SHUTDOWNSTATE_SLEEPING)
                                {
                                BaseWakeup() ;
                                setShutDownState(SHUTDOWNSTATE_AWAKE) ;
                                debugDisplayOnly("OK I've woken up\r\n") ;
                                }
                                 **/
                        }
                        globalPowerButtonExtendedPressCount = 0;
                    }
                }
                break;

            case SHUTDOWNSTATE_SAVINGLOGS  :
                {
                    ulong lwCurrentEvent = 0;

                    if(globalPowerButtonExtendedPressCount == 0)
                    {
                        /* saving logs to filesystem can only happen from a task context
                         * which is why we raise an event that the power down task waits for
                         */
                        (void)OSSetEvents(PWR_DOWN_EVENT_GROUP, EV_PWR_DOWN_SAVE_LOGS, OS_OR);
                        globalPowerButtonExtendedPressCount++;
                    }
                    else if (OSReceiveEvents(PWR_DOWN_EVENT_GROUP, EV_PWR_DOWN_SAVE_LOGS_DONE, OS_NO_SUSPEND, OS_OR_CONSUME, &lwCurrentEvent) == SUCCESSFUL)
                    {
                        setShutDownState(SHUTDOWNSTATE_POWERINGOFF);
                        globalPowerButtonExtendedPressCount = 0;
                    }
                    else
                    {
                        /* todo: implement a timeout waiting for the end of the saving of the logs */
                        globalPowerButtonExtendedPressCount++;
                    }
                }
                break;

                case SHUTDOWNSTATE_POWERINGOFF  :
                    {
                        // sent long press event
                        SendBaseEventToTablet(BASE_EVENT_BUTTON_LONG_PRESS, NULL);
                        //					/**
                        // Comment this block out when the tablet wants to be in control of the power down business.
                        BaseTurnOffPower() ;
                        //					**/
                    }
                    break;
            }
	}
}

/* **********************************************************************
// DESCRIPTION: BaseTurnOffPower.
//
// Fire the GPIO pin that turns off power to the base, if we cannot complete
// this request return a NU_RETURN_ERROR status
//
// INPUTS: none
// RETURNS: none
// **********************************************************************/
// pins should match schematic
static GPIO_INFO CSD_GPIO_Table[] = {
		{AM335X_CTRL_PADCONF_MII1_RX_CLK, AM335X_GPIO_BIT_10, GPIO_BANK_3, DIR_OUTPUT, GPIO_PULL_OFF}, // OFF_CTRL
		{AM335X_CTRL_PADCONF_UART0_CTSN, AM335X_GPIO_BIT_8, GPIO_BANK_1, DIR_INPUT, GPIO_PULL_OFF} // MODEL_SEL
};
static const unsigned numPinsUsed = sizeof(CSD_GPIO_Table) / sizeof(GPIO_INFO);
static P_GPIO_INFO pGPIOTable = &CSD_GPIO_Table[0];
STATUS BaseTurnOffPower(void)
{
	STATUS status = NU_SUCCESS;
	setShutDownState(SHUTDOWNSTATE_POWERINGOFF);
	dbgTrace(DBG_LVL_INFO, "Base Power Off");
	//preShutdownHook() ;  /* now called in PowerDown_Task */
	setGPIOPinHigh(*(pGPIOTable + POWER_IDX_OFF_CTRL));
	return status;
}


/* **********************************************************************
// DESCRIPTION: BaseGotoSleep.
//
// Initiate the sleep mode, issue a message to the tablet informing them
//
// INPUTS: none
// RETURNS: none
// **********************************************************************/
STATUS BaseGotoSleep()
{
	STATUS status = NU_SUCCESS;
	if(getShutDownState() == SHUTDOWNSTATE_AWAKE)
	{
		setShutDownState(SHUTDOWNSTATE_GOINGTOSLEEP);
//		sendSleepModeRequestMessage();
		// Handle whatever needs handling
		HALSetVM27VPowerState(FALSE) ;

		// shutdown state to sleeping
		setShutDownState(SHUTDOWNSTATE_SLEEPING);
		dbgTrace(DBG_LVL_INFO, "\r\nBase entering sleep mode!\r\n");
	}
	else
	{
		dbgTrace(DBG_LVL_INFO, "\r\nUnable to start Sleep Mode since base is in %s state", shutDownStateToText());
		status = NU_RETURN_ERROR;
	}
	return status;
}

/* **********************************************************************
// DESCRIPTION: BaseWakeup.
//
// Come out of sleep mode if possible
//
// INPUTS: none
// RETURNS: none
// **********************************************************************/
void BaseWakeup()
{
	// only apply the work if the base is in the expected sleeping state.
	if(getShutDownState() == SHUTDOWNSTATE_SLEEPING)
	{
		HALSetVM27VPowerState(TRUE) ;
		setShutDownState(SHUTDOWNSTATE_AWAKE) ;
		dbgTrace(DBG_LVL_INFO, "\r\nBase now awake\r\n");
	}
	else
	{
		dbgTrace(DBG_LVL_INFO, "\r\nUnable to apply WakeUp state, device is currently in %s state\r\n",shutDownStateToText());

	}

}

/* **********************************************************************
// DESCRIPTION: sendSleepModeRequestMessage.
//
// Send the JSON goto sleep message to the tablet with the correct source info
//
// INPUTS: none
// RETURNS: none
// **********************************************************************/
/* -- not needed anymore
void sendSleepModeRequestMessage()
{
    cJSON *pMsg = cJSON_CreateObject();
    cJSON_AddStringToObject(pMsg, "MsgID", "SleepModeReq");
    cJSON_AddStringToObject(pMsg, "Src", 	"Base");
    Send_JSON(pMsg,BROADCAST_MASK);
}
*/

/* **********************************************************************
// DESCRIPTION: setShutDownState.
//
// Set the shutdown mode flag to the requested value
//
// INPUTS: UINT16 state value
// RETURNS: none
// **********************************************************************/
static UINT16 localShutDownState = 0;
void setShutDownState(UINT16 uState)
{
	/// state switching should be done here.
	// ...
	///
	localShutDownState = uState;
}

/* **********************************************************************
// DESCRIPTION: getShutDownMode.
//
// Return the value of the system's shutdown mode flag
//
// INPUTS: none
// RETURNS: none
// **********************************************************************/
UINT16 getShutDownState()
{
	return localShutDownState;
}

/* **********************************************************************
// DESCRIPTION: shutDownStateToText.
//
// Return a string representing the current shutdown state of the base
//
// INPUTS: UINT16 shutdown mode flag
// RETURNS: none
// **********************************************************************/
CHAR *shutDownStateToText()
{
	switch(localShutDownState)
	{
		case SHUTDOWNSTATE_AWAKE:
			return "Awake";
			break ;
		case SHUTDOWNSTATE_GOINGTOSLEEP:
			return "Going To Sleep";
			break ;
		case SHUTDOWNSTATE_SLEEPING:
			return "Sleeping";
			break ;
		case SHUTDOWNSTATE_POWERINGOFF:
			return "Powering off";
			break ;
	}
	return "Unknown";
}

/* **********************************************************************
// DESCRIPTION: on_SleepModeReq.
//
// Tablet response to SleepModeReq, tablet should populate src with Table
//
// INPUTS: UINT32 handle	- handle to ws transaction handler
//         cJSON  *root		- source message received.
// RETURNS: none
// **********************************************************************/
void on_SleepModeReq(UINT32 handle, cJSON *root)
{
	CHAR 	strTemp[150] ;
	cJSON						*rspMsg 	= cJSON_CreateObject();
	WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

	// this is a response message so we do not need to parse and complain if there is a problem, just check the source and accept
	cJSON_AddStringToObject(rspMsg, "MsgID", "SleepModeRsp");

	// execute any goto sleep code here.
	if(BaseGotoSleep() == NU_SUCCESS)
		cJSON_AddStringToObject(rspMsg, "Status", "Success");
	else
	{
		snprintf(strTemp, 150, "Base received tablet Wake Request but base is in %s mode - no action taken", shutDownStateToText()) ;
		cJSON_AddStringToObject(rspMsg, "Status", "Rejected");
		cJSON_AddStringToObject(rspMsg, "Error", strTemp);
		dbgTrace(DBG_LVL_INFO, strTemp);
	}

	addEntryToTxQueue(&entry, root, "  on_SleepModeReq: Added response to tx queue.\r\n");

}

/* **********************************************************************
// DESCRIPTION: on_PowerOffReq.
//
// Handle the tablet issued Power Off request message, if we cannot turn off issue a
// status Rejected response.
//
// INPUTS: UINT32 handle	- handle to ws transaction handler
//         cJSON  *root		- source message received.
// RETURNS: none
// **********************************************************************/
void on_PowerOffReq(UINT32 handle, cJSON *root)
{
//	NetworkInformationStruct	nisTemp;

	if(BaseTurnOffPower() != NU_SUCCESS)
	{
		cJSON						*rspMsg 	= cJSON_CreateObject();
		cJSON_AddStringToObject(rspMsg, "MsgID", "PowerOffRsp");
		cJSON_AddStringToObject(rspMsg, "Status", "Rejected");
		WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
		addEntryToTxQueue(&entry, root, "  on_WakeRsp: Added response to tx queue.\r\n");

	}
}

/* **********************************************************************
// DESCRIPTION: on_WakeReq.
//
// Handle the tablet issued wake request message
//
// INPUTS: UINT32 handle	- handle to ws transaction handler
//         cJSON  *root		- source message received.
// RETURNS: none
// **********************************************************************/
void on_WakeReq(UINT32 handle, cJSON *root)
{
	NetworkInformationStruct	nisTemp;
	cJSON						*rspMsg 	= cJSON_CreateObject();
	cJSON_AddStringToObject(rspMsg, "MsgID", "WakeRsp");
	CHAR strTemp[150] ;

	memset(&nisTemp, 0, sizeof(NetworkInformationStruct));

	WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

	// OK Tablet sent this, we will act upon it
	if(getShutDownState() == SHUTDOWNSTATE_SLEEPING)
	{
		cJSON_AddStringToObject(rspMsg, "Status", "Accepted");
		BaseWakeup() ;
	}
	else
	{
		snprintf(strTemp, 150, "Base received tablet Wake Request but base is in %s mode - no action taken", shutDownStateToText()) ;
		cJSON_AddStringToObject(rspMsg, "Status", "Rejected");
		cJSON_AddStringToObject(rspMsg, "Error", strTemp);
		dbgTrace(DBG_LVL_INFO, strTemp);
	}

	/* an error may have been added to rspMsg by RequiredFieldsArePresent() */
	cJSON_AddStringToObject(rspMsg, WS_KEY_SRC, "Base");
	addEntryToTxQueue(&entry, root, "  on_WakeRsp: Added response to tx queue.\r\n");

}

