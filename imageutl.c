/************************************************************************
    PROJECT:        Horizon CSD
    MODULE NAME:    IMAGEUTL.C.c 
    
    DESCRIPTION:    Image generation utility functions
                    
 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
*
*   REVISION HISTORY:

 09-Apr-18 sa002pe on FPHX 02.12 shelton branch
 	Removed everything for text ads.  Never implemented.

 27-Apr-17 sa002pe on FPHX 02.12 shelton branch
 	Commented out IGU_LoadTextAd.  Feature was never implemented.
	
 	Merging in changes from K7:
----*
	* 25-Apr-16 sa002pe on K7 tips shelton branch
	*	Made changes to put a message in the syslog w/ the missing font ID when
	*	an IG_MISSING_FONT error is determined.
----*
 
*************************************************************************/

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "imagegen.h"		// must be first
#include "imageutl.h"
#include "ImageErrs.h"
#include "bobutils.h"		// must come before bob.h
#include "bob.h"
#include "datdict.h"
#include "dmbcpublic.h"
#include "fwrapper.h"
#include "report1.h"
#include "unistring.h"


// prototypes for external global data
extern IG_BARCODE_DATA sBarCodeData;
extern FONT_DATA sFontData[IG_MAX_FONTS];
extern PRINTED_VCRS sPrintedVCRs[IG_MAX_VCRS];
extern REGISTER_GROUPS sRegisterGroups[IG_MAX_GROUPS];

//extern unsigned short wTotalVCRs;
extern unsigned short wIGTownCircleName[MAX_TC_UNICHARS+1];	// Name of the TC graphic currently being used

extern unsigned char bDateState;			// date ducking state
extern unsigned char bNumOfFonts;
extern unsigned char bTotalGroups;
extern unsigned char bImageBuffer[MAX_COLS][IMAGE_ROW_BYTES];
extern unsigned char bImageBuffer2[MAX_COLS][IMAGE_ROW_BYTES];


extern BOOL    fbBatchCountState[MAX_TYPES];			// Printed Batch Count ducking states
extern BOOL    fbPINState;					// PIN ducking state
extern BOOL    fbTCState;					// Town line-circle ducking state
extern BOOL    fbTimeState;					// Time ducking state

// Decompression states and masks
#define DE_NEED_CMD		0
#define DE_REPEAT		1
#define DE_LITERAL		2

#define DE_REP_BIT		0x80
#define DE_CNT_MASK		0x7F

// Justification values
// The "HORIZ" & "VERT" values are for 1D barcodes
#define	STRING_L	0
#define	STRING_R	1
#define	STRING_C	2
#define	HORIZ_L		4
#define	HORIZ_R		5
#define	HORIZ_C		6
#define	VERT_B		8
#define	VERT_T		9
#define	VERT_C		10

/* General 1D Barcode defines */
#define MAX_1D_DATA_SIZE	32

#define DO_CODE_128				0
#define DO_CODE_39_STANDARD		1
#define DO_CODE_39_EXTENDED		2
#define DO_CODE_25_INTERLEAVED	3

/* Code 128 1D Barcode defines */
#define USE_CODE_B	0
#define USE_CODE_C	1

#define ONE_D_MOD    			103
#define ONE_D_MOD_TIMES_10		(ONE_D_MOD * 10)

#define CODE_C           99
#define CODE_B          100
#define START_B         104
#define START_C         105
#define STOP_CODE       106
 
/* Code 39 1D Barcode defines */
#define CODE39_BASE_CHAR	(' ')	// space character

#define MAX_CODE39_BARS		10			// 9 bars + 1 separator bar
#define MAX_CODE39_CHARS	('Z' - CODE39_BASE_CHAR + 1)

#define CODE39_START_CODE		('*' - CODE39_BASE_CHAR)
#define CODE39_STOP_CODE		('*' - CODE39_BASE_CHAR)

/* Code 25 1D Barcode defines */
#define CODE25_BASE_CHAR	('0')	// zero character

#define MAX_CODE25_BARS		5
#define MAX_CODE25_CHARS	10

// internal global data
unsigned short	wLastEndCol;	// ending column + 1 of the last region cleared/loaded
unsigned short	wLastEndRow;	// ending row + 1 of the last region cleared/loaded
unsigned short	wLastStartCol;	// starting column of the last region cleared/loaded
unsigned short	wLastStartRow;	// starting row of the last region cleared/loaded

// this table is for reversing the order of a bits in a byte.
// for example, a value of 47 (01000111) becomes E2 (11100010).
const unsigned char ByteMirror[256] =
{	
	0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0,	// 0x00 - 0x07
	0x10, 0x90, 0x50, 0xD0, 0x30, 0xB0, 0x70, 0xF0,	// 0x08 - 0x0F
	0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68, 0xE8,	// 0x10 - 0x17
	0x18, 0x98, 0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8,	// 0x18 - 0x1F
	0x04, 0x84, 0x44, 0xC4, 0x24, 0xA4, 0x64, 0xE4,	// 0x20 - 0x27
	0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74, 0xF4,	// 0x28 - 0x2F
	0x0C, 0x8C, 0x4C, 0xCC, 0x2C, 0xAC, 0x6C, 0xEC,	// 0x30 - 0x37
	0x1C, 0x9C, 0x5C, 0xDC, 0x3C, 0xBC, 0x7C, 0xFC,	// 0x38 - 0x3F
	0x02, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2,	// 0x40 - 0x47
	0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, 0xF2,	// 0x48 - 0x4F
	0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA,	// 0x50 - 0x57
	0x1A, 0x9A, 0x5A, 0xDA, 0x3A, 0xBA, 0x7A, 0xFA,	// 0x58 - 0x5F
	0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6,	// 0x60 - 0x67
	0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6,	// 0x68 - 0x6F
	0x0E, 0x8E, 0x4E, 0xCE, 0x2E, 0xAE, 0x6E, 0xEE,	// 0x70 - 0x77
	0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE,	// 0x78 - 0x7F
	0x01, 0x81, 0x41, 0xC1, 0x21, 0xA1, 0x61, 0xE1,	// 0x80 - 0x87
	0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71, 0xF1,	// 0x88 - 0x8F
	0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9,	// 0x90 - 0x97
	0x19, 0x99, 0x59, 0xD9, 0x39, 0xB9, 0x79, 0xF9,	// 0x98 - 0x9F
	0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65, 0xE5,	// 0xA0 - 0xA7
	0x15, 0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5,	// 0xA8 - 0xAF
	0x0D, 0x8D, 0x4D, 0xCD, 0x2D, 0xAD, 0x6D, 0xED,	// 0xB0 - 0xB7
	0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D, 0xFD,	// 0xB8 - 0xBF
	0x03, 0x83, 0x43, 0xC3, 0x23, 0xA3, 0x63, 0xE3,	// 0xC0 - 0xC7
	0x13, 0x93, 0x53, 0xD3, 0x33, 0xB3, 0x73, 0xF3,	// 0xC8 - 0xCF
	0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B, 0xEB,	// 0xD0 - 0xD7
	0x1B, 0x9B, 0x5B, 0xDB, 0x3B, 0xBB, 0x7B, 0xFB,	// 0xD8 - 0xDF
	0x07, 0x87, 0x47, 0xC7, 0x27, 0xA7, 0x67, 0xE7,	// 0xE0 - 0xE7
	0x17, 0x97, 0x57, 0xD7, 0x37, 0xB7, 0x77, 0xF7,	// 0xE8 - 0xEF
	0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F, 0xEF,	// 0xF0 - 0xF7
	0x1F, 0x9F, 0x5F, 0xDF, 0x3F, 0xBF, 0x7F, 0xFF,	// 0xF8 - 0xFF
};

typedef struct sOneDBarPatterns {
    unsigned char  black1;
    unsigned char  white1;
    unsigned char  black2;
    unsigned char  white2;
    unsigned char  black3;
    unsigned char  white3;
} ONE_D_BAR_PATTERNS;

/* Code 128 1D Barcode pattern look up table */
const ONE_D_BAR_PATTERNS OneDBarPatterns[] = {
                       //     Code A     Code B     Code C
    {2, 1, 2, 2, 2, 2},  //0       SP         SP         00
    {2, 2, 2, 1, 2, 2},  //1       !          !          01
    {2, 2, 2, 2, 2, 1},  //2       "          "          02
    {1, 2, 1, 2, 2, 3},  //3       #          #          03
    {1, 2, 1, 3, 2, 2},  //4       $          $          04
    {1, 3, 1, 2, 2, 2},  //5       %          %          05
    {1, 2, 2, 2, 1, 3},  //6       &          &          06
    {1, 2, 2, 3, 1, 2},  //7       '          '          07
    {1, 3, 2, 2, 1, 2},  //8       (          (          08
    {2, 2, 1, 2, 1, 3},  //9       )          )          09
    {2, 2, 1, 3, 1, 2},  //10      *          *          10
    {2, 3, 1, 2, 1, 2},  //11      +          +          11
    {1, 1, 2, 2, 3, 2},  //12      ,          ,          12
    {1, 2, 2, 1, 3, 2},  //13      -          -          13
    {1, 2, 2, 2, 3, 1},  //14      .          .          14
    {1, 1, 3, 2, 2, 2},  //15      /          /          15
    {1, 2, 3, 1, 2, 2},  //16      0          0          16
    {1, 2, 3, 2, 2, 1},  //17      1          1          17
    {2, 2, 3, 2, 1, 1},  //18      2          2          18
    {2, 2, 1, 1, 3, 2},  //19      3          3          19
    {2, 2, 1, 2, 3, 1},  //20      4          4          20
    {2, 1, 3, 2, 1, 2},  //21      5          5          21
    {2, 2, 3, 1, 1, 2},  //22      6          6          22
    {3, 1, 2, 1, 3, 1},  //23      7          7          23
    {3, 1, 1, 2, 2, 2},  //24      8          8          24
    {3, 2, 1, 1, 2, 2},  //25      9          9          25
    {3, 2, 1, 2, 2, 1},  //26      :          :          26
    {3, 1, 2, 2, 1, 2},  //27      ;          ;          27
    {3, 2, 2, 1, 1, 2},  //28      <          <          28
    {3, 2, 2, 2, 1, 1},  //29      =          =          29
    {2, 1, 2, 1, 2, 3},  //30      >          >          30
    {2, 1, 2, 3, 2, 1},  //31      ?          ?          31
    {2, 3, 2, 1, 2, 1},  //32      @          @          32
    {1, 1, 1, 3, 2, 3},  //33      A          A          33
    {1, 3, 1, 1, 2, 3},  //34      B          B          34
    {1, 3, 1, 3, 2, 1},  //35      C          C          35
    {1, 1, 2, 3, 1, 3},  //36      D          D          36
    {1, 3, 2, 1, 1, 3},  //37      E          E          37
    {1, 3, 2, 3, 1, 1},  //38      F          F          38
    {2, 1, 1, 3, 1, 3},  //39      G          G          39
    {2, 3, 1, 1, 1, 3},  //40      H          H          40
    {2, 3, 1, 3, 1, 1},  //41      I          I          41
    {1, 1, 2, 1, 3, 3},  //42      J          J          42
    {1, 1, 2, 3, 3, 1},  //43      K          K          43
    {1, 3, 2, 1, 3, 1},  //44      L          L          44
    {1, 1, 3, 1, 2, 3},  //45      M          M          45
    {1, 1, 3, 3, 2, 1},  //46      N          N          46
    {1, 3, 3, 1, 2, 1},  //47      O          O          47
    {3, 1, 3, 1, 2, 1},  //48      P          P          48
    {2, 1, 1, 3, 3, 1},  //49      Q          Q          49
    {2, 3, 1, 1, 3, 1},  //50      R          R          50
    {2, 1, 3, 1, 1, 3},  //51      S          S          51
    {2, 1, 3, 3, 1, 1},  //52      T          T          52
    {2, 1, 3, 1, 3, 1},  //53      U          U          53
    {3, 1, 1, 1, 2, 3},  //54      V          V          54
    {3, 1, 1, 3, 2, 1},  //55      W          W          55
    {3, 3, 1, 1, 2, 1},  //56      X          X          56
    {3, 1, 2, 1, 1, 3},  //57      Y          Y          57
    {3, 1, 2, 3, 1, 1},  //58      Z          Z          58
    {3, 3, 2, 1, 1, 1},  //59      [          [          59
    {3, 1, 4, 1, 1, 1},  //60      \          \          60
    {2, 2, 1, 4, 1, 1},  //61      ]          ]          61
    {4, 3, 1, 1, 1, 1},  //62      ^          ^          62
    {1, 1, 1, 2, 2, 4},  //63       _          _         63
    {1, 1, 1, 4, 2, 2},  //64      NUL        `          64
    {1, 2, 1, 1, 2, 4},  //65      SOH        a          65
    {1, 2, 1, 4, 2, 1},  //66      STX        b          66
    {1, 4, 1, 1, 2, 2},  //67      ETX        c          67
    {1, 4, 1, 2, 2, 1},  //68      EOT        d          68
    {1, 1, 2, 2, 1, 4},  //69      ENQ        e          69
    {1, 1, 2, 4, 1, 2},  //70      ACK        f          70
    {1, 2, 2, 1, 1, 4},  //71      BEL        g          71
    {1, 2, 2, 4, 1, 1},  //72      BS         h          72
    {1, 4, 2, 1, 1, 2},  //73      HT         i          73
    {1, 4, 2, 2, 1, 1},  //74      LF         j          74
    {2, 4, 1, 2, 1, 1},  //75      VT         k          75
    {2, 2, 1, 1, 1, 4},  //76      FF         I          76
    {4, 1, 3, 1, 1, 1},  //77      CR         m          77
    {2, 4, 1, 1, 1, 2},  //78      SO         n          78
    {1, 3, 4, 1, 1, 1},  //79      SI         o          79
    {1, 1, 1, 2, 4, 2},  //80      DLE        p          80
    {1, 2, 1, 1, 4, 2},  //81      DC1        q          81
    {1, 2, 1, 2, 4, 1},  //82      DC2        r          82
    {1, 1, 4, 2, 1, 2},  //83      DC3        s          83
    {1, 2, 4, 1, 1, 2},  //84      DC4        t          84
    {1, 2, 4, 2, 1, 1},  //85      NAK        u          85
    {4, 1, 1, 2, 1, 2},  //86      SYN        v          86
    {4, 2, 1, 1, 1, 2},  //87      ETB        w          87
    {4, 2, 1, 2, 1, 1},  //88      CAN        x          88
    {2, 1, 2, 1, 4, 1},  //89      EM         y          89
    {2, 1, 4, 1, 2, 1},  //90      SUB        z          90
    {4, 1, 2, 1, 2, 1},  //91      ESC        {          91
    {1, 1, 1, 1, 4, 3},  //92      FS         |          92
    {1, 1, 1, 3, 4, 1},  //93      GS         }          93
    {1, 3, 1, 1, 4, 1},  //94      RS         ~          94
    {1, 1, 4, 1, 1, 3},  //95      US         DEL        95
    {1, 1, 4, 3, 1, 1},  //96      FNC 3      FNC 3      96
    {4, 1, 1, 1, 1, 3},  //97      FNC 2      FNC 2      97
    {4, 1, 1, 3, 1, 1},  //98      SHIFT      SHIFT      98
    {1, 1, 3, 1, 4, 1},  //99      CODE C     CODE C     99
    {1, 1, 4, 1, 3, 1},  //100     CODE B     FNC 4      CODE B
    {3, 1, 1, 1, 4, 1},  //101     FNC 4      CODE A     CODE A
    {4, 1, 1, 1, 3, 1},  //102     FNC 1      FNC 1      FNC 1
    {2, 1, 1, 4, 1, 2},  //103     Start A    Start A    Start A
    {2, 1, 1, 2, 1, 4},  //104     Start B    Start B    Start B
    {2, 1, 1, 2, 3, 2},  //105     Start C    Start C    Start C
    {2, 3, 3, 1, 1, 1}   //106     Stop       Stop       Stop    Stop has 2 black bars to
                       //                                      the right of it 
};

/* Code39 1D Barcode pattern look up table */
/*
given in order of black width, white width, black width, white width, black width,
white width, black width, white width, black width, white spacer between characters --
where 0 = not supported, 1 = narrow & 2 = wide
If a particular ASCII character isn't supported, nothing will be printed in the barcode
for it since all of the widths for unsupported characters are zero.
*/
const unsigned char Code39OneDBarPatterns[MAX_CODE39_CHARS][MAX_CODE39_BARS] =
{
	{1,2,2,1,1,1,2,1,1,1},	//0x20	space
	{0,0,0,0,0,0,0,0,0,0},	//0x21	!
	{0,0,0,0,0,0,0,0,0,0},	//0x22	"
	{0,0,0,0,0,0,0,0,0,0},	//0x23	#
	{1,2,1,2,1,2,1,1,1,1},	//0x24	$
	{1,1,1,2,1,2,1,2,1,1},	//0x25	%
	{0,0,0,0,0,0,0,0,0,0},	//0x26	&
	{0,0,0,0,0,0,0,0,0,0},	//0x27	' (apostrophe)
	{0,0,0,0,0,0,0,0,0,0},	//0x28	(
	{0,0,0,0,0,0,0,0,0,0},	//0x29	)
	{1,2,1,1,2,1,2,1,1,1},	//0x2A	*
	{1,2,1,1,1,2,1,2,1,1},	//0x2B	+
	{0,0,0,0,0,0,0,0,0,0},	//0x2C	, (comma)
	{1,2,1,1,1,1,2,1,2,1},	//0x2D	- (dash, minus)
	{2,2,1,1,1,1,2,1,1,1},	//0x2E	. (period)
	{1,2,1,2,1,1,1,2,1,1},	//0x2F	/
	{1,1,1,2,2,1,2,1,1,1},	//0x30	0
	{2,1,1,2,1,1,1,1,2,1},	//0x31	1
	{1,1,2,2,1,1,1,1,2,1},	//0x32	2
	{2,1,2,2,1,1,1,1,1,1},	//0x33	3
	{1,1,1,2,2,1,1,1,2,1},	//0x34	4
	{2,1,1,2,2,1,1,1,1,1},	//0x35	5
	{1,1,2,2,2,1,1,1,1,1},	//0x36	6
	{1,1,1,2,1,1,2,1,2,1},	//0x37	7
	{2,1,1,2,1,1,2,1,1,1},	//0x38	8
	{1,1,2,2,1,1,2,1,1,1},	//0x39	9
	{0,0,0,0,0,0,0,0,0,0},	//0x3A	:
	{0,0,0,0,0,0,0,0,0,0},	//0x3B	;
	{0,0,0,0,0,0,0,0,0,0},	//0x3C	<
	{0,0,0,0,0,0,0,0,0,0},	//0x3D	=
	{0,0,0,0,0,0,0,0,0,0},	//0x3E	>
	{0,0,0,0,0,0,0,0,0,0},	//0x3F	?
	{0,0,0,0,0,0,0,0,0,0},	//0x40	@
	{2,1,1,1,1,2,1,1,2,1},	//0x41	A
	{1,1,2,1,1,2,1,1,2,1},	//0x42	B
	{2,1,2,1,1,2,1,1,1,1},	//0x43	C
	{1,1,1,1,2,2,1,1,2,1},	//0x44	D
	{2,1,1,1,2,2,1,1,1,1},	//0x45	E
	{1,1,2,1,2,2,1,1,1,1},	//0x46	F
	{1,1,1,1,1,2,2,1,2,1},	//0x47	G
	{2,1,1,1,1,2,2,1,1,1},	//0x48	H
	{1,1,2,1,1,2,2,1,1,1},	//0x49	I
	{1,1,1,1,2,2,2,1,1,1},	//0x4A	J
	{2,1,1,1,1,1,1,2,2,1},	//0x4B	K
	{1,1,2,1,1,1,1,2,2,1},	//0x4C	L
	{2,1,2,1,1,1,1,2,1,1},	//0x4D	M
	{1,1,1,1,2,1,1,2,2,1},	//0x4E	N
	{2,1,1,1,2,1,1,2,1,1},	//0x4F	O
	{1,1,2,1,2,1,1,2,1,1},	//0x50	P
	{1,1,1,1,1,1,2,2,2,1},	//0x51	Q
	{2,1,1,1,1,1,2,2,1,1},	//0x52	R
	{1,1,2,1,1,1,2,2,1,1},	//0x53	S
	{1,1,1,1,2,1,2,2,1,1},	//0x54	T
	{2,2,1,1,1,1,1,1,2,1},	//0x55	U
	{1,2,2,1,1,1,1,1,2,1},	//0x56	V
	{2,2,2,1,1,1,1,1,1,1},	//0x57	W
	{1,2,1,1,2,1,1,1,2,1},	//0x58	X
	{2,2,1,1,2,1,1,1,1,1},	//0x59	Y
	{1,2,2,1,2,1,1,1,1,1}	//0x5A	Z
};

/* Code39 1D Barcode data translation table */
/*
Shows the ASCII character string to use to replace the given ASCII character
when using the Code 39 extending character set.
Keep in mind that doing the translation will result in most of the special characters
from the standard character set taking up 2 barcode characters instead of just one
because a distinction has to be made between when the special character is being used
to define a normally unsupported character & when we want the actual special character.
*/
const char Code39OneDTranslation[128][3] =
{
	{"%U"},	//0x00	(NUL)
	{"$A"},	//0x01	(SOH)
	{"$B"},	//0x02	(STX)
	{"$C"},	//0x03	(ETX)
	{"$D"},	//0x04	(EOT)
	{"$E"},	//0x05	(ENQ)
	{"$F"},	//0x06	(ACK)
	{"$G"},	//0x07	(BEL)
	{"$H"},	//0x08	(BS)
	{"$I"},	//0x09	(HT)
	{"$J"},	//0x0A	(LF)
	{"$K"},	//0x0B	(VT)
	{"$L"},	//0x0C	(FF)
	{"$M"},	//0x0D	(CR)
	{"$N"},	//0x0E	(SO)
	{"$O"},	//0x0F	(SI)
	{"$P"},	//0x10	(DLE)
	{"$Q"},	//0x11	(DC1)
	{"$R"},	//0x12	(DC2)
	{"$S"},	//0x13	(DC3)
	{"$T"},	//0x14	(DC4)
	{"$U"},	//0x15	(NAK)
	{"$V"},	//0x16	(SYN)
	{"$W"},	//0x17	(ETB)
	{"$X"},	//0x18	(CAN)
	{"$Y"},	//0x19	(EM)
	{"$Z"},	//0x1A	(SUB)
	{"%A"},	//0x1B	(ESC)
	{"%B"},	//0x1C	(FS)
	{"%C"},	//0x1D	(GS)
	{"%D"},	//0x1E	(RS)
	{"%E"},	//0x1F	(US)
	{" "},	//0x20	space
	{"/A"},	//0x21	!
	{"/B"},	//0x22	"
	{"/C"},	//0x23	#
	{"/D"},	//0x24	$
	{"/E"},	//0x25	%
	{"/F"},	//0x26	&
	{"/G"},	//0x27	' (apostrophe)
	{"/H"},	//0x28	(
	{"/I"},	//0x29	)
	{"/J"},	//0x2A	*
	{"/K"},	//0x2B	+
	{"/L"},	//0x2C	, (comma)
	{"-"},	//0x2D	- (dash, minus)
	{"."},	//0x2E	. (period)
	{"/O"},	//0x2F	/
	{"0"},	//0x30	0
	{"1"},	//0x31	1
	{"2"},	//0x32	2
	{"3"},	//0x33	3
	{"4"},	//0x34	4
	{"5"},	//0x35	5
	{"6"},	//0x36	6
	{"7"},	//0x37	7
	{"8"},	//0x38	8
	{"9"},	//0x39	9
	{"/Z"},	//0x3A	:
	{"%F"},	//0x3B	;
	{"%G"},	//0x3C	<
	{"%H"},	//0x3D	=
	{"%I"},	//0x3E	>
	{"%J"},	//0x3F	?
	{"%V"},	//0x40	@
	{"A"},	//0x41	A
	{"B"},	//0x42	B
	{"C"},	//0x43	C
	{"D"},	//0x44	D
	{"E"},	//0x45	E
	{"F"},	//0x46	F
	{"G"},	//0x47	G
	{"H"},	//0x48	H
	{"I"},	//0x49	I
	{"J"},	//0x4A	J
	{"K"},	//0x4B	K
	{"L"},	//0x4C	L
	{"M"},	//0x4D	M
	{"N"},	//0x4E	N
	{"O"},	//0x4F	O
	{"P"},	//0x50	P
	{"Q"},	//0x51	Q
	{"R"},	//0x52	R
	{"S"},	//0x53	S
	{"T"},	//0x54	T
	{"U"},	//0x55	U
	{"V"},	//0x56	V
	{"W"},	//0x57	W
	{"X"},	//0x58	X
	{"Y"},	//0x59	Y
	{"Z"},	//0x5A	Z
	{"%K"},	//0x5B	[
	{"%L"},	//0x5C	\	//lint !e427
	{"%M"},	//0x5D	]
	{"%N"},	//0x5E	^
	{"%O"},	//0x5F	_ (underline)
	{"%W"},	//0x60	`
	{"+A"},	//0x61	a
	{"+B"},	//0x62	b
	{"+C"},	//0x63	c
	{"+D"},	//0x64	d
	{"+E"},	//0x65	e
	{"+F"},	//0x66	f
	{"+G"},	//0x67	g
	{"+H"},	//0x68	h
	{"+I"},	//0x69	i
	{"+J"},	//0x6A	j
	{"+K"},	//0x6B	k
	{"+L"},	//0x6C	l
	{"+M"},	//0x6D	m
	{"+N"},	//0x6E	n
	{"+O"},	//0x6F	o
	{"+P"},	//0x70	p
	{"+Q"},	//0x71	q
	{"+R"},	//0x72	r
	{"+S"},	//0x73	s
	{"+T"},	//0x74	t
	{"+U"},	//0x75	u
	{"+V"},	//0x76	v
	{"+W"},	//0x77	w
	{"+X"},	//0x78	x
	{"+Y"},	//0x79	y
	{"+Z"},	//0x7A	z
	{"%P"},	//0x7B	{
	{"%Q"},	//0x7C	|
	{"%R"},	//0x7D	}
	{"%S"},	//0x7E	~
	{"%T"}	//0x7F	(DEL)
};

/* Code25 1D Barcode pattern look up table */
/* 0 = not supported, 1 = narrow & 2 = wide
If a particular ASCII character isn't supported, nothing will be printed in the
barcode for it since all of the widths for unsupported characters are zero.
*/
const unsigned char Code25OneDBarPatterns[MAX_CODE25_CHARS][MAX_CODE25_BARS] =
{
	{1,1,2,2,1},	//0x30	0
	{2,1,1,1,2},	//0x31	1
	{1,2,1,1,2},	//0x32	2
	{2,2,1,1,1},	//0x33	3
	{1,1,2,1,2},	//0x34	4
	{2,1,2,1,1},	//0x35	5
	{1,2,2,1,1},	//0x36	6
	{1,1,1,2,2},	//0x37	7
	{2,1,1,2,1},	//0x38	8
	{1,2,1,2,1},	//0x39	9
};

// for clearing and loading shifted bytes
union
{
	unsigned short wShift;
	struct
	{		
		unsigned char bHigh;	
		unsigned char bLow;
	} bShift;	
}uByteAccess;

/*****************************************************************************
//                   NEW TOWN CIRCLE MANAGEMENT 
*****************************************************************************/

#define PI  3.1415926535897932  /* pi */


typedef struct sIGU_TC_OUTPUT_DEV_INFO {
  unsigned short wMaxCols; 
  unsigned short wMaxRows;
  unsigned short wMaxRowBytes;
  unsigned char  *pbBuff;
  double         dHorizontalScaleFactor;
  double         dVerticalScaleFactor;
} IGU_TC_OUTPUT_DEV_INFO;


IGU_TC_OUTPUT_DEV_INFO gsTCOutputDevInfo;


typedef struct sIGU_TC_TEXT_INFO {
  unsigned char  bFontId;
  unsigned char  bFontInx;
  unsigned short wFontWidth;
  unsigned short wFontHeight;
  unsigned short wFontCharBitmapBytes;
  unsigned short wNumTextChars;
  unsigned short wWidth;
  unsigned short wHeight;
  const UNICHAR  *pszText;
  unsigned char  *pbTextImage;
  double         dAngleMinRad;
  double         dAngleMaxRad;
  double         dAngleDiffRad;
  BOOL           fIsTopText;
} IGU_TC_TEXT_INFO;


IGU_TC_TEXT_INFO gsTCTextInfoTop;
IGU_TC_TEXT_INFO gsTCTextInfoBottom;


typedef struct sIGU_TC_TOWN_CIRCLE_PARAMETERS {
  unsigned short wOriginX;
  unsigned short wOriginY;
  unsigned short wTextRadius;
} IGU_TC_TOWN_CIRCLE_PARAMETERS;


IGU_TC_TOWN_CIRCLE_PARAMETERS gsTCCircleInfo;

/*****************************************************************************
//                   END OF NEW TOWN CIRCLE MANAGEMENT 
*****************************************************************************/

// prototypes for internal subroutines
BOOL DoCodeC(const unsigned short *pwData, const unsigned char bMode);

unsigned long IGU_Load1DBarcode(const unsigned short *pString, const unsigned short wLen,
								const unsigned short wStartCol, const unsigned short wStartRow,
								const unsigned short wWidth, const unsigned short wHeight,
								const unsigned char bJust, const unsigned short wVCR);

static void IGU_SetPixel(const IGU_TC_OUTPUT_DEV_INFO *psDevInfo, 
                         const unsigned short wColumn, const unsigned short wRow);

static void IGU_TCCalculateAngles(const BOOL fIsTopText, const unsigned short wRadiusPixels, 
                                  IGU_TC_TEXT_INFO *psTextInfo);

static void IGU_TCDrawText(const IGU_TC_OUTPUT_DEV_INFO *psDevInfo,
	                         const IGU_TC_TEXT_INFO *psTextInfo,
	                         const IGU_TC_TOWN_CIRCLE_PARAMETERS *psCircleInfo);

static void IGU_TCInitializeOutputDevInfo(IGU_TC_OUTPUT_DEV_INFO *psDevInfo);

static void IGU_TCScaleFontCharBitmap(const unsigned char *pbFontImage, const unsigned short wCharIndex, 
                                      const IGU_TC_TEXT_INFO *pTextInfo);

static void IGU_TCSetupCircleParameters(IGU_TC_TOWN_CIRCLE_PARAMETERS *psCircleInfo,
                                        REGION_MAP *pGraphicRegion, TOWN_CIRCLE_DATA *pTCInfo,
                                        IGU_TC_OUTPUT_DEV_INFO *psDevInfo);

static BOOL IGU_TCSetupTextBuffer(const BOOL fIsTopText, const unsigned short wTextRadiusPixels, 
                                  const UNICHAR *pszText,const unsigned char bFontId, 
                                  IGU_TC_TEXT_INFO *psFontInfo);

unsigned long PaintChar(const unsigned char *pbImage, unsigned short wBytes,
						const unsigned short wStartCol, const unsigned short wStartRow,
						const unsigned short wWidth, const unsigned short wHeight,
						const unsigned char bLoadBlack);
						
void PaintNextLine(unsigned long *plErrCode, unsigned short *pwCompHLeft, unsigned short *pwCompWLeft,
					const unsigned short wCompWidth, const unsigned short wStartCol, unsigned short *pwRowByte,
					unsigned char **pbBuf);

/*****************************************************************************
//                   UTILITY FUNCTIONS FOR THE IMAGE GENERATOR
*****************************************************************************/
/****************************************************************************
// FUNCTION NAME: IGU_ClearRegion
// DESCRIPTION: Clear a region in the print area
//
// AUTHOR: Sandra Peterson
//
// INPUTS: wStartCol = starting column for the region
//         wStartRow = starting row for the region
//         wMaxCols = number of columns in the region
//         wMaxRows = number of rows in the region
//
// OUTPUTS:	Error code
*****************************************************************************/


unsigned long IGU_ClearRegion(const unsigned short wStartCol, const unsigned short wStartRow,
							  const unsigned short wMaxCols, const unsigned short wMaxRows)
{
	unsigned char	*pbBuf;
	unsigned long	lErrCode = IG_NO_ERROR;
	unsigned long	lBytes;
	unsigned short	wCol, wRow, wBytes;
	unsigned short	wEndRow;
	unsigned short	wStartRowByte, wEndRowByte;
	unsigned char	bStartBit, bEndBit;
	unsigned char	bStartMask, bEndMask;
	char			pLogBuf[50];


	wEndRow = wStartRow+wMaxRows;

	// make sure the region stays within the boundaries of the print area
	if ((wStartCol >= (IMAGE_COLS + START_OFFSET)) || (wStartRow >= MAX_ROWS) ||
		(wEndRow > (MAX_ROWS + ROW_ADJUST)))
		return (IG_INVALID_COORDINATES);

	if (wStartCol+wMaxCols > (IMAGE_COLS + START_OFFSET))
		return (IG_TOO_MANY_COLUMNS);

	// make sure the region has a size
	if ((wMaxCols == 0) || (wMaxRows == 0))
	{
		(void)sprintf(pLogBuf, "Region has width/height of zero");
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
		return (IG_INVALID_SIZE);
	}

	if ((wStartCol == 0) && (wStartRow == 0))
	{
		// For possible need in the future
		wLastStartCol = 0;
		wLastStartRow = 0;
		wLastEndCol = 0;
		wLastEndRow = 0;
	}

	// Clear the area
	//	If the area doesn't start on a row that is evenly divisible by 8, or
	//		the height isn't evenly divisible by 8, clear as shifted data
	//	Else, clear as whole bytes
	if (((wStartRow % 8) || (wMaxRows % 8)) && (wEndRow != (MAX_ROWS + ROW_ADJUST)))
	{       
//?? will need to change this if the bytes are flipped back
//??
		wStartRowByte = wStartRow / 8;
		bStartBit = wStartRow % 8;
		wEndRowByte = (wEndRow - 1) / 8;
		bEndBit = (wEndRow - 1) % 8;
		if (wStartRowByte == wEndRowByte) 
		{
			pbBuf = &bImageBuffer[wStartCol][wStartRowByte];

			// shift x zeros into the upper byte, where x is
			// the number of rows to clear
//			uByteAccess.wShift = 0xff00 << (bEndBit - bStartBit + 1);

			// fill the lower byte as the bottom of the mask
//			uByteAccess.bShift.bLow = 0xff;

			// shift y ones into the upper byte, where y is 
			// the number of rows to keep
			// note, bit 0 is for row 0, bit 7 is for row 7
//			uByteAccess.wShift <<= bStartBit;

			// shift x zeros into the upper byte, where x is
			// the number of rows to clear
			uByteAccess.wShift = 0xff00 << (bEndBit - bStartBit + 1);

			// fill the lower byte as the bottom of the mask
			uByteAccess.bShift.bLow = 0xff;

			// shift y ones into the upper byte, where y is 
			// the number of rows to keep
			// note, bit 0 is for row 7, bit 7 is for row 0
			uByteAccess.wShift <<= (7 - bEndBit);

			// take the upper byte as the mask for the clearing
			// the bits for the row
			bStartMask = uByteAccess.bShift.bHigh;
			for (wCol = 0; wCol < wMaxCols; wCol++) 
			{
				*pbBuf &= bStartMask;
				pbBuf += IMAGE_ROW_BYTES;
			}
		}
		else 
		{
			wBytes = wEndRowByte - wStartRowByte + 1;
//			bStartMask = 0xff >> (8 - bStartBit);
//			bEndMask = 0xff << (bEndBit + 1);
//			for (wCol = wStartCol; wCol < wStartCol + wMaxCols; wCol++) 
//			{
//				pbBuf = &bImageBuffer[wCol][wStartRowByte];
//				*pbBuf++ &= bStartMask;
//				for (wRow = 2; wRow < wBytes; wRow++)
//				{
//					*pbBuf++ = 0;
//				}
//				*pbBuf &= bEndMask;
//			}

			bStartMask = 0xff << (8 - bStartBit);
			bEndMask = 0xff >> (bEndBit + 1);
			for (wCol = wStartCol; wCol < wStartCol + wMaxCols; wCol++) 
			{
				pbBuf = &bImageBuffer[wCol][wStartRowByte];
				*pbBuf++ &= bStartMask;
				for (wRow = 2; wRow < wBytes; wRow++)
				{
					*pbBuf++ = 0;
				}
				*pbBuf &= bEndMask;
			}
		}
	}
	else
	{
		// If the area is the full height, do it as a block clear
		// Else, clear it by columns
		if (wEndRow == MAX_ROWS)
			wBytes = (wMaxRows + (IMAGE_ROWS - MAX_ROWS + 7)) / 8;
		else
			wBytes = wMaxRows / 8;

		if ((wStartRow == 0) && (wBytes == IMAGE_ROW_BYTES))
		{
			lBytes = wBytes * wMaxCols;
			(void)memset(&bImageBuffer[wStartCol][0], 0, lBytes);
		}
		else
		{
			wStartRowByte = wStartRow / 8;
			for (wCol = wStartCol; wCol < wStartCol + wMaxCols; wCol++)
				(void)memset(&bImageBuffer[wCol][wStartRowByte], 0, wBytes);
		}
	}

	// For possible need in the future
	wLastStartCol = wStartCol;
	wLastStartRow = wStartRow;
	wLastEndCol = wStartCol + wMaxCols;
	wLastEndRow = wStartRow + wMaxRows;

	return (lErrCode);
}
	

/****************************************************************************
// FUNCTION NAME: IGU_DrawTownCircle
// DESCRIPTION: Write some texts in 2 curved zones of the Town Circle.
//              One curved zone is on the top of the circle, and the second zone
//              is at the bottom.
//
// AUTHOR: Bruno Debuire
//
// GLOBALS USED: gsTCOutputDevInfo, gsTCCircleInfo, gsTCTextInfoTop, gsTCTextInfoBottom
//
// INPUTS: plszTextTop    : Pointer on the text to print on the top zone of the circle
//         bFontIdTop     : Id of the font to use for the top zone
//         plszTextBottom : Pointer on the text to print on the bottom zone of the circle
//         bFontIdBottom  : Id of the font to use for the bottom zone 
//         pGraphicRegion : Pointer on the graphic region where the Town Circle is printed
//         pTCInfo        : Pointer on the structure which describes the component used
//                          to support the curved text
//
// OUTPUTS:
*****************************************************************************/
void IGU_DrawTownCircle(const UNICHAR *plszTextTop,   const unsigned char bFontIdTop,               
                        const UNICHAR *plszTextBottom,const unsigned char bFontIdBottom,
                        const REGION_MAP *pGraphicRegion,
                        const TOWN_CIRCLE_DATA *pTCInfo)
{
  const BOOL fIsTopText = TRUE;
  const BOOL fIsBottomText = FALSE;


  // Setup parameters for the device.
  IGU_TCInitializeOutputDevInfo(&gsTCOutputDevInfo);
  
  // Setup parameters for the circle.
  IGU_TCSetupCircleParameters(&gsTCCircleInfo, (REGION_MAP *)pGraphicRegion, (TOWN_CIRCLE_DATA *) pTCInfo, &gsTCOutputDevInfo);	//lint !e605 !e605
        
  //(void)memset((void*)&gsTCTextInfoTop,0,sizeof(gsTCTextInfoTop));
  //(void)memset((void*)&gsTCTextInfoBottom,0,sizeof(gsTCTextInfoBottom));
  
  // Setup parameters for the text.
  if (TRUE == IGU_TCSetupTextBuffer(fIsTopText,  gsTCCircleInfo.wTextRadius,
                                    plszTextTop, bFontIdTop, &gsTCTextInfoTop))
  {
    if (TRUE == IGU_TCSetupTextBuffer(fIsBottomText,  gsTCCircleInfo.wTextRadius,
                                      plszTextBottom, bFontIdBottom, &gsTCTextInfoBottom))		
    {   
      // Draw the text.
      IGU_TCDrawText(&gsTCOutputDevInfo, &gsTCTextInfoTop, &gsTCCircleInfo);	
     
      IGU_TCDrawText(&gsTCOutputDevInfo, &gsTCTextInfoBottom, &gsTCCircleInfo);
    }
  }
  
  // free any memory allocated for the text images
  if (gsTCTextInfoTop.pbTextImage != NULL)
    free(gsTCTextInfoTop.pbTextImage);
  if (gsTCTextInfoBottom.pbTextImage != NULL)
    free(gsTCTextInfoBottom.pbTextImage);
}


/****************************************************************************
// FUNCTION NAME: IGU_LoadBackground
// DESCRIPTION: Clears a given region and then loads a background graphic
//				into the region.
//
// AUTHOR: Sandra Peterson
//
// GLOBALS USED: bImageBuffer
//
// INPUTS: pbImage = pointer to the compressed image data
//         wBytes = number of bytes of image data
//         wStartCol = starting column for the region
//         wStartRow = starting row for the region
//         wWidth = width of the background graphic
//         wHeight = height of the background graphic
//         wMaxCols = number of columns in the region
//         wMaxRows = number of rows in the region
//
// OUTPUTS:	Error Code
*****************************************************************************/

unsigned long IGU_LoadBackground(const unsigned char *pbImage, unsigned short wBytes,
								 const unsigned short wStartCol, const unsigned short wStartRow,
								 const unsigned short wWidth, const unsigned short wHeight,
								 const unsigned short wMaxCols, const unsigned short wMaxRows)
{
	unsigned char	*pbBuf;
	unsigned long	lErrCode = IG_NO_ERROR;

	unsigned short	wCompHLeft;		// number of row bytes left to load for the graphic
	unsigned short	wCompWidth;		// number of columns in the graphic
	unsigned short	wCompWLeft;		// number of columns left to do on the current row
	unsigned short	wRowByte;		// starting/current row for loading the graphic

	unsigned char	bCmdByte;		// current command byte
	unsigned char	bCompCount = 0;	// number of compression bytes to load
	unsigned char	bCompShift;		// number of bits to shift the data to get it to start
									// on the proper row.
	unsigned char	bCompState;		// compression state
	unsigned char	bDataByte;		// current data byte

	char			pLogBuf[50];


	// make sure the background graphic has a valid size and that it
	// isn't bigger than the max size of the region
	if ((wWidth == 0) || (wHeight == 0) ||
		(wWidth > wMaxCols) || (wHeight > wMaxRows))
	{
		(void)sprintf(pLogBuf, "Graphic width = %02X", wWidth);
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
		(void)sprintf(pLogBuf, "Graphic max width = %02X", wMaxCols);
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
		(void)sprintf(pLogBuf, "Graphic height = %02X", wHeight);
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
		(void)sprintf(pLogBuf, "Graphic max height = %02X", wMaxRows);
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
		return (IG_INVALID_SIZE);
	}

	// make sure the data was provided
	if ((pbImage == NULL) || (wBytes == 0))
		return (IG_INVALID_IMAGE);

	// clear the region where the background is being loaded
	// only clear as wide as the actual graphic
	lErrCode = IGU_ClearRegion(wStartCol, wStartRow, wWidth, wMaxRows);

	// if no problems clearing the region, load the background graphic
	if (lErrCode == IG_NO_ERROR)
	{
		// set up the initial data for loading the graphic
		wCompHLeft = (wHeight / 8) + 1;
		bCompShift = wStartRow % 8;
		wCompWidth = wWidth;
		wCompWLeft = wCompWidth;
		wRowByte = wStartRow / 8;

		pbBuf = &bImageBuffer[wStartCol][wRowByte];
		bCompState = DE_NEED_CMD;

		/*
		 *	For each byte in the input so long as there isn't a problem
		 */

		while((wBytes != 0) && (lErrCode == IG_NO_ERROR)) 
		{
			switch (bCompState) 
			{
			case DE_NEED_CMD :
				/*
				 *	If we're waiting for a command, set it up.
				 *	This will take care of adjusting bCompState appropriately.
				 *
				 *	If the command byte is a zero, this is an "end of line" command.
				 */

				bCmdByte = *pbImage;
				if (bCmdByte == 0)
				{
					/*
					 *	PaintNextLine handles going to a new line.  
					 *	If PaintNextLinereturns successfully, we're great.
					 */

					PaintNextLine(&lErrCode, &wCompHLeft, &wCompWLeft, wCompWidth, wStartCol, &wRowByte, &pbBuf);
					if (lErrCode != IG_NO_ERROR)
					{
						/*
						 *	OK, PaintNextLine failed.  This means that there's not enough
						 *	space in the buffer for another line of data.  There is a
						 *	special case to worry about, however.  If the last line of
						 *	graphics goes right up to the edge of the graphics buffer,
						 *	and if the very last command in the data is an "end of line"
						 *	with nothing following it, we can let it slide.  (Since no
						 *	data will follow, "no harm, no foul".  The "bytesLeft" parameter
						 *	tells us how many compressed graphics bytes remain so that
						 *	we can make this decision.)
						 *
						 *	To protect against anything else being loaded, we make sure 
						 *	the "space left" variables are set to zero, which will force
						 *	us back through here (or through PaintNextLine) if another block
						 *	is processed.
						 */

						wCompWLeft = 0;
						wCompHLeft = 0;

						if (wBytes == 1)
							lErrCode = IG_NO_ERROR;
							
					}
				}
				else
				{
					/*
					 *	If we get here, we have a non-zero data byte.  The top bit
					 *	tells us what type of command it is.  (bit set: repeat command
					 *	bit clear: literal command)
					 */

					if (bCmdByte & DE_REP_BIT)
						bCompState = DE_REPEAT;
					else
						bCompState = DE_LITERAL;

						/*
						 *	The count is in the rest of the byte.  The count should be non-zero
						 *	because a "repeat" of zero bytes is not valid.  (A "literal" of
						 *	zero bytes is treated as an "end of line", and was handled above)
						 */

					bCompCount = bCmdByte & DE_CNT_MASK;
					if (bCompCount == 0)
						lErrCode = IG_INVALID_COMPRESSION;

					/*
					 *	The command byte itself gets skipped over when pbImage is
					 *	incremented at the end of the switch.
					 */
				}

				break;

			case DE_REPEAT:
				/*
				 *	We need to repeat this byte. The repeat count is in bCompCount.
				 */

				bDataByte = *pbImage;
				bDataByte = ByteMirror[bDataByte];

				/*
				 *	While we have data left to do.....
				 */

				while (bCompCount)
				{
					/*
					 *	If we've run out of room on this row, we need to wrap
					 *	to the next row.  PaintNextLine will fail if we run
					 *	off the edge of the graphics buffer, which is a bad
					 *	thing.  This test needs to be done before the byte
					 *	is painted, rather than after, to avoid a boundary
					 *	condition when the graphic finishes exactly at the
					 *	end of the buffer.
					 */

					if (wCompWLeft == 0)
					{
						PaintNextLine(&lErrCode, &wCompHLeft, &wCompWLeft, wCompWidth, wStartCol, &wRowByte, &pbBuf);
						if (lErrCode != IG_NO_ERROR)
							break;
					}

					/*
					 *	If we get here, we've got space, so put out this byte.
					 *	If necessary, shift the contents of the byte.
					 *	Move the pointer to the same row in the next column,
					 *	and decrement the "space left" and "left to do" counts.
					 */

					if (bCompShift == 0)
					{
						*pbBuf = bDataByte;
					}
					else
					{
//?? will need to change this if the bytes are flipped back
//??
//						uByteAccess.wShift = bDataByte << bCompShift;

						uByteAccess.wShift = bDataByte << (8 - bCompShift);

						*pbBuf		|= uByteAccess.bShift.bHigh;
						*(pbBuf+1)	|= uByteAccess.bShift.bLow;
					}

					pbBuf += IMAGE_ROW_BYTES;
					wCompWLeft--;
					bCompCount--;
				}

				/*
				 *	At this point we've exhausted this command.  pbImage is still
				 *	pointing at the data byte, but it will be incremented
				 *	after the switch.  Now we need a command.
				 */

				bCompState = DE_NEED_CMD;
				break;

			case DE_LITERAL:
				/*
				 *	We need to copy this byte "as is" out.
				 */

				bDataByte = *pbImage;
				bDataByte = ByteMirror[bDataByte];

				/*
				 *	If we've run out of room on this row, we need to wrap
				 *	to the next row.  PaintNextLine will fail if we run
				 *	off the edge of the graphics buffer, which is a bad
				 *	thing.  This test needs to be done before the byte
				 *	is painted, rather than after, to avoid a boundary
				 *	condition when the graphic finishes exactly at the
				 *	end of the buffer.
				 */

				if (wCompWLeft == 0)
				{
					PaintNextLine(&lErrCode, &wCompHLeft, &wCompWLeft, wCompWidth, wStartCol, &wRowByte, &pbBuf);
					if (lErrCode != IG_NO_ERROR)
						break;
				}

				/*
				 *	If we get here, we've got space, so put out this byte.
				 *	If necessary, shift the contents of the byte.
				 *	Move the pointer to the same row in the next column,
				 *	and decrement the "space left" and "left to do" counts.
				 */

				if (bCompShift == 0)
				{
					*pbBuf = bDataByte;
				}
				else
				{
//?? will need to change this if the bytes are flipped back
//??
//					uByteAccess.wShift = bDataByte << bCompShift;

					uByteAccess.wShift = bDataByte << (8 - bCompShift);

					*pbBuf		|= uByteAccess.bShift.bHigh;
					*(pbBuf+1)	|= uByteAccess.bShift.bLow;
				}

				pbBuf += IMAGE_ROW_BYTES;
				wCompWLeft--;
				bCompCount--;

				/*
				 *	If that was the last byte to be copied, we're back to
				 *	needing a command.  pbImage is still pointing at the byte
				 *	to be copied, but it will be incremented after the switch.
				 */

				if (bCompCount == 0) 
					bCompState = DE_NEED_CMD;
				break;

			default:
				break;
			}

			/*
			 *	If we've successfully gotten through the switch, we've disposed
			 *	of one character (command or data byte).
			 */

			pbImage++;
			wBytes--;
		}
	}

	return (lErrCode);
}


/****************************************************************************
// FUNCTION NAME: IGU_LoadVCRs
// DESCRIPTION: Loads the designated VCRs with data from the given blob.
//
// AUTHOR: Sandra Peterson
//
// GLOBALS USED: sRegisterGroups, sPrintedVCRs, sFontData, bTotalGroups,
//				bNumOfFonts
//
// INPUTS: psBlob = pointer to an IG_BLOB_DATA structure which has the data
//					for the static VCRs
//         bNumBlobItems = number of items in the IG_BLOB_DATA structure
//         bWhatToDo = IG_ALL, IG_STATIC, IG_VARIABLE
//         fbSkipSame = TRUE if the loading of the graphic for the VCR should
//					be skipped if the VCR already contains the desired value
//					FALSE otherwise 
//
// OUTPUTS:	Error Code
*****************************************************************************/
#define	MAX_CHARS	255

unsigned long IGU_LoadVCRs(const IG_BLOB_DATA *psBlob, const unsigned char bNumBlobItems,
						   const unsigned char bWhatToDo, const BOOL fbSkipSame,
						   const unsigned char *pbDebitData)
{
	unsigned short	*pwBlobStart;
	unsigned char	*pbFontImage;
	unsigned char	*pbBuf;

	unsigned long	lErrCode = IG_NO_ERROR;

	unsigned short	wBlobLength = 0;
	unsigned short	wBytes;
    unsigned short	wData[MAX_CHARS];
	unsigned short	wFiller;
	unsigned short	wInx, wID;
	unsigned short	wOffset;
	unsigned short	wTempLength = 0;
	
	unsigned char	bBlobIndex = 0;
	unsigned char	bInx, bID;
	unsigned char	bLoadBlack;
    unsigned char	bStartIndex;
    unsigned char	bTableIndex;
	char			pLogBuf[50];

	BOOL			fbDiffFont;
	BOOL			fbSkipField;


//	if (bNumBlobItems == 0)
//		return (lErrCode);

    /* Get the data for each register field and put it in the appropriate VCRs. */
    for (bTableIndex = 0; (bTableIndex < bTotalGroups) && (lErrCode == IG_NO_ERROR);
		 bTableIndex++)
    {
		/* Fill in the data for the current field only if it is the  */
		/* desired type of data.                                       */
		if (((sRegisterGroups[bTableIndex].bFieldType & IG_VCR_MASK) == bWhatToDo) ||
			(bWhatToDo == IG_ALL))
		{
			// copy the ID to a local variable to make things easier
			// during debugging
			wID = sRegisterGroups[bTableIndex].wVariableID;
			fbSkipField = FALSE;

			switch(wID)
			{
			case VCR_PIN:
				// if the PIN is ducked, don't load it.
				if (fbPINState == DUCKED)
					fbSkipField = TRUE;
				break;

			case VCR_PRINTED_DATE:
			case VCR_FLEX_DEBIT_DATE:
				// if the date is fully ducked, don't load it.
				if (bDateState == VLT_DUCKED_OUT_DATE)
					fbSkipField = TRUE;
				break;

			case VCR_PRINTED_IBC:
				// if the printed indicia batch count is ducked, don't load it.
				if (fbBatchCountState[INDICIA] == DUCKED)
					fbSkipField = TRUE;
				break;

			case VCR_PRINTED_PBC:
				// if the printed permit batch count is ducked, don't load it.
				if (fbBatchCountState[PERMIT] == DUCKED)
					fbSkipField = TRUE;
				break;

			case VCR_PRINTED_TBC:
				// if the printed tax batch count is ducked, don't load it.
				if (fbBatchCountState[TAX] == DUCKED)
					fbSkipField = TRUE;
				break;

			case VCR_PRINTED_TIME:
				// if the time is ducked, don't load it.
				if (fbTimeState == DUCKED)
					fbSkipField = TRUE;
				break;

			case VCR_TOWN_LINE:
			case VCR_ZIP_CODE:
				// if the town line is ducked, don't load the town
				// line (which may or may not include the zip code)
				// or the zip code
				if (fbTCState == DUCKED)
					fbSkipField = TRUE;
				break;

			default:
				break;
			}

			// if the load for this field needs to be skipped,
			// make sure the field contains spaces.
			if (fbSkipField == TRUE)
			{
				wInx = sRegisterGroups[bTableIndex].wStartRegID;
				for (bInx = 0; bInx < sRegisterGroups[bTableIndex].bLengthOfField;
					 bInx++, wInx++)
				{
					if (sPrintedVCRs[wInx].wValue != 0x0020)
					{
						sPrintedVCRs[wInx].wValue = 0x0020;
						sPrintedVCRs[wInx].bFontID = sRegisterGroups[bTableIndex].bFontID;
						sPrintedVCRs[wInx].fbDirty = TRUE;

						sRegisterGroups[bTableIndex].fbDirty = TRUE;
					}
				}
			}
			else
			{
				/* The data buffer must be large enough to hold the field data. */
				/* if not, it's an error.                                       */
				// Ignore the lint warning so we can keep the check against MAX_CHARS
				// in case it changes in the future.
				if ((sRegisterGroups[bTableIndex].bLengthOfField == 0) ||
					(sRegisterGroups[bTableIndex].bLengthOfField > MAX_CHARS))	//lint !e685
				{
					lErrCode = IG_INVALID_FIELD_SIZE;
					continue;		// consider this instance in the for-loop for bTableIndex done.
				}
				else
				{
					// If the field isn't the one for the additional post office data
					// from the town circle graphic component, see if the blob has
					// the data for the field.
					if (wID != VCR_POST_DATA)
					{
						// first check if the desired data is at the current blob index
						if (psBlob->sBlobHeader[bBlobIndex].wVariableID != wID)
						{
							/* Retrieve the index data required for the current field. */
							for (bBlobIndex = 0; bBlobIndex < bNumBlobItems; bBlobIndex++)
								if (psBlob->sBlobHeader[bBlobIndex].wVariableID == wID)
									break;
						}

						// if the desired ID is in the blob,
						// setup to load all black for the field.
						if (bBlobIndex != bNumBlobItems)
						{
							// Get the starting address of the string for this field
							pwBlobStart = (unsigned short *)(psBlob->wStrings + psBlob->sBlobHeader[bBlobIndex].wOffset);

							// Get the length of the blob string
							wTempLength = psBlob->sBlobHeader[bBlobIndex].wLength;
						}
						else
						{
							// the desired ID isn't in the blob (e.g., bBlobIndex == bNumBlobItems == 7), or
							// there is nothing in the blob (i.e., bBlobIndex == bNumBlobItems == 0), so
							// set the field length from the blob to zero.
							wTempLength = 0;
						}

					}
					else
					{
						// Get the starting address of the string for this field
						pwBlobStart = &wIGTownCircleName[0];

						// Get the length of the blob string
						wTempLength = unistrlen(wIGTownCircleName);
					}

					// if the length of the data in the blob for the desired ID is zero OR
					// the desired ID isn't in the blob (in which case the length should be zero) OR
					// there is no data at all in the blob (in which case the length should be zero),
					// setup to load all black for the field.
/*//??
					if (!wTempLength)
					{
//??					lErrCode = IG_MISSING_VCR_DATA;
						wInx = sRegisterGroups[bTableIndex].wStartRegID;
						for (bInx = 0; bInx < sRegisterGroups[bTableIndex].bLengthOfField;
							 bInx++, wInx++)
						{
							sPrintedVCRs[wInx].wValue = 0x0080;	// because this is an unused character code, it will
																// result in black being loaded for this character
							sPrintedVCRs[wInx].bFontID = sRegisterGroups[bTableIndex].bFontID;
							sPrintedVCRs[wInx].fbDirty = TRUE;
						}

						sRegisterGroups[bTableIndex].fbDirty = TRUE;

						// reset the blob index & go to the next field
						bBlobIndex = 0;
						continue;		// consider this instance in the for-loop for bTableIndex done.
					}
//??*/
					// If not a 1D barcode field, load the data into the VCRs.
					// Else, clear the 1D barcode field
#if defined(IG_TEST_CODE128_1D) || defined(IG_TEST_CODE39_1D)
					if (wID == VCR_IND_SN)
						sRegisterGroups[bTableIndex].bFieldType |= IG_1D_MASK;
#endif

					if ((sRegisterGroups[bTableIndex].bFieldType & IG_1D_MASK) == 0)
					{
						// If the data is too large for the field, truncate it to the left or
						// right depending on the justification.
						if (wTempLength > sRegisterGroups[bTableIndex].bLengthOfField)
						{
							// use the length of the field as defined in the register group
							wBlobLength = sRegisterGroups[bTableIndex].bLengthOfField;
						
							// determine the starting address of the truncated field
							switch (sRegisterGroups[bTableIndex].bJustification)
							{
							case STRING_L:
								// use the starting address as is
								break;

							case STRING_R:
								// start at wBlobLength characters from the end of the string
								// for this field
								// Ignore the lint warning. We wouldn't be here unless wTempLength is
								// non-zero & when wTempLength is set, pwBlobStart is set.
								pwBlobStart += wTempLength - wBlobLength;		//lint !e644
								break;

							case STRING_C:
								// start at a point which will give the center wBlobLength 
								// characters of the string for this field
								pwBlobStart += (wTempLength - wBlobLength) / 2;
								break;

							default:
								/* Unknown justification parameter. */
								lErrCode = IG_UNKNOWN_JUSTIFICATION;
								break;
							}
						}
						else
						{
							// use the length of the field as defined in the blob
							wBlobLength = wTempLength;

							// if there was something in the blob, use the normal filler
							// else, just fill with spaces
							if (wBlobLength)
							{
								// fill any potentially unused characters
								switch (wID)
								{
								case VCR_FULL_AR:
								case VCR_FULL_DR:
								case VCR_ZIP_CODE:
									// make sure the field is filled with leading zeros
									wFiller = UNICODE_ZERO;
									break;

								case VCR_INDICIA_PC:
								case VCR_INDICIA_PC_SLR:
								case VCR_PC:
								case VCR_NZPC:
								case VCR_ZPC:
								case VCR_PRINTED_IBC:
								case VCR_PRINTED_PBC:
								case VCR_PRINTED_TBC:
								case VCR_PIN:
									if ((sRegisterGroups[bTableIndex].bFieldType & IG_VCR_MASK) == IG_REPORT)
										// for reports, make sure the field is filled with spaces
										wFiller = UNICODE_SPACE;
									else
										// for everything else,make sure the field is filled with leading zeros
										wFiller = UNICODE_ZERO;
									break;

								default:
									// make sure the field is filled with spaces
									wFiller = UNICODE_SPACE;
									break;
								}
							}
							else
							{
								wFiller = UNICODE_SPACE;
							}

							for (bInx = 0; bInx < sRegisterGroups[bTableIndex].bLengthOfField; bInx++)
								wData[bInx] = wFiller;
						}

						if (lErrCode != IG_NO_ERROR)
							continue;		// consider this instance in the for-loop for bTableIndex done.

						bStartIndex = 0;
						switch (sRegisterGroups[bTableIndex].bJustification)
						{
						case STRING_L:
								//start at the first character in wData
								break;

						case STRING_R:
								/* Right justify the data in the field. */
								bStartIndex += sRegisterGroups[bTableIndex].bLengthOfField -
												(unsigned char)wBlobLength;
								break;

						case STRING_C:
								/* Right justify the data in the field. */
								bStartIndex += (sRegisterGroups[bTableIndex].bLengthOfField -
												(unsigned char)wBlobLength)/2;
								break;

						default:
								/* Unknown justification parameter. */
								lErrCode = IG_UNKNOWN_JUSTIFICATION;
								break;
						}

						if (lErrCode != IG_NO_ERROR)
							continue;		// consider this instance in the for-loop for bTableIndex done.

						/* Copy the data to the field. */
						(void)unistrncpy(&wData[bStartIndex], pwBlobStart, wBlobLength);

						// Load the VCRs
						wInx = sRegisterGroups[bTableIndex].wStartRegID;
						for (bInx = 0; bInx < sRegisterGroups[bTableIndex].bLengthOfField;
							 bInx++, wInx++)
						{
							if ((fbSkipSame == FALSE) || 
								(sPrintedVCRs[wInx].wValue != wData[bInx]) ||
								(sPrintedVCRs[wInx].bFontID != sRegisterGroups[bTableIndex].bFontID))
							{
								sPrintedVCRs[wInx].wValue = wData[bInx];
								sPrintedVCRs[wInx].bFontID = sRegisterGroups[bTableIndex].bFontID;
								sPrintedVCRs[wInx].fbDirty = TRUE;

								sRegisterGroups[bTableIndex].fbDirty = TRUE;
							}
						}

						// If the VCR is for a 2D barcode and we're supposed to load the barcode,
						// do it now.
						if (((wID == VCR_BARCODE) || (wID == VCR_REPORT_BARCODE)) &&
							(wData[0] == BARCODE_ACTION_LOAD_BARCODE)) 
						{
							// get the index for the VCR location data again because the for-loop above
							// will have changed the initial value.
							wInx = sRegisterGroups[bTableIndex].wStartRegID;

//?? for debugging, copy the ID to a local variable
							bID = sPrintedVCRs[wInx].bFontID;

							// find the proper font
							for (bInx = 0; bInx < bNumOfFonts; bInx++)
							{
								if (sFontData[bInx].bFontID == bID)
									break;
							}

							// if we don't have the font, declare an error.
							// else, try to generate & load the 2D barcode.
							if (bInx == bNumOfFonts)
							{
								lErrCode = IG_MISSING_FONT;
								(void)sprintf(pLogBuf, "Missing Font, ID = %u", bID);
								fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
							}
							else
							{
								// make sure the barcode element has a valid size and that it
								// isn't bigger than the size of the font
								if ((sBarCodeData.wWidth == 0) || 
									(sBarCodeData.wHeight == 0) ||
									(sBarCodeData.wWidth > sFontData[bInx].wWidth) || 
									(sBarCodeData.wHeight > sFontData[bInx].wHeight))
								{
									(void)sprintf(pLogBuf, "2D barcode width = %02X", sBarCodeData.wWidth);
									fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
									(void)sprintf(pLogBuf, "VCR font width = %02X", sFontData[bInx].wWidth);
									fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
									(void)sprintf(pLogBuf, "2D barcode height = %02X", sBarCodeData.wHeight);
									fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
									(void)sprintf(pLogBuf, "VCR font height = %02X", sFontData[bInx].wHeight);
									fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
									lErrCode = IG_INVALID_SIZE;
								}

#ifdef IG_TIME_DEBUG
								(void)sprintf(pLogBuf, "Start VCR Barcode Clearing");
								fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
								// clear out the barcode region
								if (lErrCode == IG_NO_ERROR)
									lErrCode = IGU_ClearRegion(sPrintedVCRs[wInx].wCol,
																sPrintedVCRs[wInx].wRow,
																sFontData[bInx].wWidth,
																sFontData[bInx].wHeight);

#ifndef G900	// done in IG_ClearBarcode for G900
								if (lErrCode == IG_NO_ERROR)
									lErrCode = StartGenDMBarcode();
#endif

								if (lErrCode == IG_NO_ERROR)
								{
									unsigned char	bRowByte;

									// calculate the starting address for the VCR
									bRowByte = (unsigned char)((sPrintedVCRs[wInx].wRow + 7) / 8);
									pbBuf = &bImageBuffer[sPrintedVCRs[wInx].wCol][bRowByte];

									// if the data pointer isn't zero, generate the actual barcode
									if (pbDebitData)
									{
//??
/*										unsigned char tempDebitData[] = {'N', '0',
																		'N', '2', '1',
																		'P', 'B', '8', '2', '5', '6', '0', '3',
																		'0', '7', '1', '1', '0', '7',
																		'0', '0', '0', '0', '0', '2',
																		'0', '0',
																		'1', '0', '0', '0',
																		'0', '0', '0', '4', '3',
																		'0', '0', '0', '0', '8', '6',
																		0x7b, 0xd4, 0xf0, 0x60,
																		'0', '1',
																		'0', '1',
																		'A', '0', '0'};
*/
//??										SetNumDataBytes(53);

										// Place the barcode into the print area
										lErrCode = FinishGenDMBarcode(pbBuf, IMAGE_ROW_BYTES, pbDebitData);
//??										lErrCode = FinishGenDMBarcode(pbBuf, IMAGE_ROW_BYTES, tempDebitData);
									}
									else
									{
										(void)sprintf(pLogBuf, "Null pointer given for barcode data");
										fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
									}
								}

#ifdef IG_TIME_DEBUG
								(void)sprintf(pLogBuf, "End Blow Up");
								fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

								// indicate that this field has been loaded
								for (bInx = 0; bInx < sRegisterGroups[bTableIndex].bLengthOfField;
									 bInx++, wInx++)
								{
									sPrintedVCRs[wInx].fbDirty = FALSE;
								}

								sRegisterGroups[bTableIndex].fbDirty = FALSE;
							}
						}
					}
					else	// it's a 1D barcode field
					{
						unsigned short	wTempWidth, wTempHeight;
						unsigned short	wLeftMostVCRInx;

#if defined(IG_TEST_CODE128_1D) || defined(IG_TEST_CODE39_1D)
						unsigned short	wTempCol, wTempRow;
//??						unsigned short	wTestString39[] = {'3', 'S', 'A', 'R', 'F', 'F', '1', '9', '7', '5', '8', '2', '4', '3', '0', 0};
//??						unsigned short	wTestString39[] = {'3', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
//??						unsigned short	wTestString128[] = {'R', 'R', '0', '0', '2', '4', '3', '5', '5', '1', '8', 'D', 'E', '1', '1', '0', 0};
						unsigned short	wTestString25i[] = {'9', '8', '4', '0', '0', '1', '2', '3', '4', '5', '6', '7', 0};
						unsigned char	bTempJust;


						wTempWidth = 1500;
						wTempHeight = 500;
						wTempCol = MAX_COLS - wTempWidth;
						wTempRow = (MAX_ROWS - wTempHeight) / 2;
						bTempJust = HORIZ_R;

						// make sure the barcode region is clear
						if (lErrCode == IG_NO_ERROR)
							lErrCode = IGU_ClearRegion(wTempCol-100, 0 , wTempWidth, MAX_ROWS);

						if (lErrCode == IG_INVALID_SIZE)
						{
							(void)sprintf(pLogBuf, "Bad 1D barcode area");
							fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
						}

	#ifdef IG_TEST_CODE25_INTERLEAVED_1D
						// use the length of the field as defined in the blob
						wBlobLength = (unsigned short)unistrlen(wTestString25i);

						// calculate & load the 1D barcode field
						if (lErrCode == IG_NO_ERROR)
							lErrCode = IGU_Load1DBarcode(wTestString25i, wBlobLength,
														wTempCol, wTempRow,
														wTempWidth, wTempHeight,
														bTempJust, wID);
	#else		// code 128 or code 39
		#ifdef IG_TEST_CODE39_1D
						// use the length of the field as defined in the blob
						wBlobLength = (unsigned short)unistrlen(wTestString39);

						// calculate & load the 1D barcode field
						if (lErrCode == IG_NO_ERROR)
							lErrCode = IGU_Load1DBarcode(wTestString39, wBlobLength,
														wTempCol, wTempRow,
														wTempWidth, wTempHeight,
														bTempJust, wID);
		#else		// code 128
						// use the length of the field as defined in the blob
						wBlobLength = (unsigned short)unistrlen(wTestString128);

						// calculate & load the 1D barcode field
						if (lErrCode == IG_NO_ERROR)
							lErrCode = IGU_Load1DBarcode(wTestString128, wBlobLength,
														wTempCol, wTempRow,
														wTempWidth, wTempHeight,
														bTempJust, wID);
		#endif
	#endif
#else

						// use the length of the field as defined in the blob
						wBlobLength = wTempLength;

						// save the index for the left-most VCR
						wLeftMostVCRInx = sRegisterGroups[bTableIndex].wStartRegID;

						// find the rightmost VCR
						wInx = wLeftMostVCRInx +
								sRegisterGroups[bTableIndex].bLengthOfField - 1;

//?? for debugging, copy the ID to a local variable
						bID = sPrintedVCRs[wInx].bFontID;

						// find the proper font
						for (bInx = 0; bInx < bNumOfFonts; bInx++)
						{
							if (sFontData[bInx].bFontID == bID)
								break;
						}

						if (bInx == bNumOfFonts)
						{
							lErrCode = IG_MISSING_FONT;
							(void)sprintf(pLogBuf, "Missing Font, ID = %u", bID);
							fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
							continue;		// consider this instance in the for-loop for bTableIndex done.
						}

						// calculate the total width & height for the 1D barcode area
						switch(sRegisterGroups[bTableIndex].bJustification)
						{
						case HORIZ_L:
						case HORIZ_R:
						case HORIZ_C:
							wTempWidth = (unsigned short)(sFontData[bInx].wWidth +
										(sPrintedVCRs[wLeftMostVCRInx].wCol - sPrintedVCRs[wInx].wCol));
							wTempHeight = sFontData[bInx].wHeight;
							break;

						case VERT_B:
						case VERT_T:
						case VERT_C:
							wTempWidth = sFontData[bInx].wWidth;
							wTempHeight = (unsigned short)(sFontData[bInx].wHeight +
										(sPrintedVCRs[wLeftMostVCRInx].wRow - sPrintedVCRs[wInx].wRow));
							break;

						default :
							lErrCode = IG_UNKNOWN_JUSTIFICATION;
							continue;		// consider this instance in the for-loop for bTableIndex done.
						}

						// at this point we could clear the 1D barcode area to check if the
						// coordinates are all OK, but since the 1D barcode loading loads the
						// appropriate white space as it goes along,
						// save time by just checking the coordinates here.
						if (lErrCode == IG_NO_ERROR)
						{
							// make sure the 1D barcode area stays within the boundaries of the
							// print area and that the 1D barcode area actually has a size
							if ((sPrintedVCRs[wInx].wCol >= (IMAGE_COLS + START_OFFSET)) ||
								(sPrintedVCRs[wInx].wRow >= MAX_ROWS) ||
								(sPrintedVCRs[wInx].wCol+wTempWidth > (IMAGE_COLS + START_OFFSET)) ||
								(sPrintedVCRs[wInx].wRow+wTempHeight > (MAX_ROWS + ROW_ADJUST)))
							{
								lErrCode = IG_INVALID_COORDINATES;
							}
							else if ((wTempWidth == 0) || (wTempHeight == 0))
							{
								(void)sprintf(pLogBuf, "1D barcode has Width/Height of zero");
								fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
								lErrCode = IG_INVALID_SIZE;
							}
							else
							{
						// calculate & load the 1D barcode field
						lErrCode = IGU_Load1DBarcode(pwBlobStart, wBlobLength,
													sPrintedVCRs[wInx].wCol, sPrintedVCRs[wInx].wRow,
													wTempWidth, wTempHeight,
														sRegisterGroups[bTableIndex].bJustification, wID);
							}
						}
#endif

						// indicate that this field has been loaded
						wInx = sRegisterGroups[bTableIndex].wStartRegID;
						for (bInx = 0; bInx < sRegisterGroups[bTableIndex].bLengthOfField;
							 bInx++, wInx++)
						{
							sPrintedVCRs[wInx].fbDirty = FALSE;
						}

						sRegisterGroups[bTableIndex].fbDirty = FALSE;
					}
				}
            }
		}
		else
		{
			if (((sRegisterGroups[bTableIndex].bFieldType & IG_VCR_MASK) == IG_VARIABLE) &&
				(bWhatToDo == IG_STATIC))
			{
				// reloading just the static data, so clear out the variable data
				wInx = sRegisterGroups[bTableIndex].wStartRegID;
				for (bInx = 0; bInx < sRegisterGroups[bTableIndex].bLengthOfField;
					 bInx++, wInx++)
				{
					sPrintedVCRs[wInx].wValue = 0x0020;
					sPrintedVCRs[wInx].bFontID = sRegisterGroups[bTableIndex].bFontID;
					sPrintedVCRs[wInx].fbDirty = TRUE;
				}

				sRegisterGroups[bTableIndex].fbDirty = TRUE;
			}
		}
    }

	if (lErrCode == IG_NO_ERROR)
	{
		unsigned short	wEndInx;

		// Initialize the variables to the first character of the first font
		// This will allow us to skip the searches and calculations as we go along
		// if the font and character are the same for two or more consecutive VCRs
		fbDiffFont = FALSE;
		bInx = 0;
		wOffset = 0;
		wBytes = (unsigned short)(sFontData[bInx].wWidth * (sFontData[bInx].wHeight / 8));
		pbFontImage = sFontData[bInx].pbBitmap + (wBytes * wOffset);

		// Check each register field to see if we should check the individual VCRs for the
		// field to see if any are "dirty"
		for (bTableIndex = 0; (bTableIndex < bTotalGroups) && (lErrCode == IG_NO_ERROR);
			 bTableIndex++)
		{
			if (sRegisterGroups[bTableIndex].fbDirty == TRUE)
			{
				wInx = sRegisterGroups[bTableIndex].wStartRegID;
				wEndInx = wInx + sRegisterGroups[bTableIndex].bLengthOfField;

		// Load the data into the print area
				for (; (wInx < wEndInx) && (lErrCode == IG_NO_ERROR); wInx++)
		{
			//?? for debugging, copy the ID to a local variable
			bID = sPrintedVCRs[wInx].bFontID;

			if (sPrintedVCRs[wInx].fbDirty == TRUE)
			{
				// make sure the index is pointing to the correct font
				if (sFontData[bInx].bFontID != bID)
				{
					fbDiffFont = TRUE;
					for (bInx = 0; bInx < bNumOfFonts; bInx++)
					{
						if (sFontData[bInx].bFontID == bID)
									break;						// exit the font search loop
					}

					if (bInx == bNumOfFonts)
					{
						lErrCode = IG_MISSING_FONT;
						(void)sprintf(pLogBuf, "Missing Font, ID = %u", bID);
						fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
								continue;						// exit the VCR check for loop
					}

							wBytes = (unsigned short)(sFontData[bInx].wWidth * (sFontData[bInx].wHeight / 8));
				}

				// if the data was missing from the blob, load an alternating pattern
				// else, look for the actual character data in the font.
				if (sPrintedVCRs[wInx].wValue == 0x0080)
				{
					bLoadBlack = 0x0F;
				}
				else
				{
					bLoadBlack = 0;

					if ((fbDiffFont == TRUE) ||
						(sFontData[bInx].wCharList[wOffset] != sPrintedVCRs[wInx].wValue))
					{
						fbDiffFont = FALSE;
						for (wOffset = 0; wOffset < sFontData[bInx].wNumChars; wOffset++)
						{
							if (sFontData[bInx].wCharList[wOffset] == sPrintedVCRs[wInx].wValue)
							{
								pbFontImage = sFontData[bInx].pbBitmap + (wBytes * wOffset);
								break;
							}
						}

						if (wOffset == sFontData[bInx].wNumChars)
							bLoadBlack = 0xFF;
					}
				}
				
				lErrCode = PaintChar(pbFontImage, wBytes,
										sPrintedVCRs[wInx].wCol, sPrintedVCRs[wInx].wRow,
										sFontData[bInx].wWidth, sFontData[bInx].wHeight,
										bLoadBlack);

				sPrintedVCRs[wInx].fbDirty = FALSE;
			}
		}

				sRegisterGroups[bTableIndex].fbDirty = FALSE;
			}
		}
	}

	return (lErrCode);
}


/*****************************************************************************
//                   INTERNAL SUBROUTINES
*****************************************************************************/
/**********************************************************************
DESCRIPTION:
The DoCodeC function determines whether or not CodeC encoding should be used
when placing the data into the Code128 1D barcode.
	 
INPUT PARAMS:
	pData - Ptr to data still to be incorporated into the 1D Barcode.    
    bMode  - Current mode that is being used in the 1D Barcode Generation.
RETURN VALUE:
    BOOL - Returns TRUE if CodeC should be used, FALSE otherwise.
**********************************************************************/

BOOL DoCodeC(const unsigned short *pwData, const unsigned char bMode)
{
    unsigned char  bNumInARow = 4;
    BOOL    fbUseCodeC = TRUE;


    if (bMode == USE_CODE_C)
        bNumInARow = 2;
    
    while ((bNumInARow > 0) && (fbUseCodeC == TRUE))
    {
        if ((*pwData >= 0x30) && (*pwData <= 0x39))
        {
            pwData++;
            bNumInARow--;
        }
        else
        {
            fbUseCodeC = FALSE;
        }
    }
    
    return(fbUseCodeC);
}


/****************************************************************************
// FUNCTION NAME: IGU_Load1DBarcode
// DESCRIPTION: This function loads a 1D barcode in the specified
//				rectangle.
//
// AUTHOR: Sandra Peterson
//
// GLOBALS USED: bImageBuffer
//
// INPUTS: pString = pointer to the string of data to encode
//         wLen = length of the string
//         wStartCol = starting column for the rectangle
//         wStartRow = starting row for the rectangle
//         wWidth = width of the rectangle
//         wHeight = height of the rectangle
//         bJust = justification
//         wVCR = VCR ID
//
// OUTPUTS:	Error Code
//
// NOTES: In order to minimize the effects of ink bleeding, an EMD parameter
//		indicates whether thinning or dithering is needed.  
*****************************************************************************/

unsigned long IGU_Load1DBarcode(const unsigned short *pwInput, const unsigned short wDataBytes,
								const unsigned short wStartCol, const unsigned short wStartRow,
								const unsigned short wWidth, const unsigned short wHeight,
								const unsigned char bJust, const unsigned short wVCR)
{
    unsigned short *pwString;
    unsigned char *pbWorkBuffer;
	unsigned char *pbBlack;							/* Pointer to the black bar data */
	unsigned char *pbEvenBlack;
    unsigned char *pbImagePtr;
//    unsigned char *pbSavedImagePtr;		// needed for vertical barcodes
    unsigned char *pbBarWidth;

	unsigned long lErrorCode = IG_NO_ERROR;
    unsigned long lCheckSumValue;

    unsigned short wBarcodeWidth, wBlackCnt;
    unsigned short wIndex;
	unsigned short wNumBytesData = 0;
    unsigned short wNumCols, wUsedCols, wNumPadCols;
    unsigned short wString[MAX_1D_DATA_SIZE*3] = {0};

	unsigned char bBars;
	unsigned char bBarLength;						/* Length of a 1D Barcode Bar */
    unsigned char bCnt, bLoopCount;
//	unsigned char bInx;							// needed for vertical barcodes
    unsigned char bMode = USE_CODE_B;
	unsigned char bExtraBitsAbove = 0;
    unsigned char bWorkBuffer[MAX_1D_DATA_SIZE*2] = {0};
	unsigned char bBlackBarData[IMAGE_ROW_BYTES] = {0};
	unsigned char bEvenBlackBarData[IMAGE_ROW_BYTES] = {0};
	unsigned char bEvenMask = 0xAA;
	unsigned char bOddMask = 0x55;
	unsigned char bNormalNarrowWidth, bWideWidth, bSpacerWidth;
	unsigned char bBarcodeType, bThinWideBlackBars, bThinNarrowBlackBars, bExtraWhite = 0;
	unsigned char ucCode25Buffer[((MAX_1D_DATA_SIZE + 2) *  MAX_CODE25_BARS)];

	BOOL		fbDoDithering;
	BOOL		fbFirstChar = FALSE;
	BOOL		fbThinWideWithSubstitute, fbThinNarrowWithSubstitute;


#ifdef IG_TIME_DEBUG
	char			pLogBuf[50];


	(void)sprintf(pLogBuf, "Start IG_1DBarcode");
	fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

	// Set up the initial data
#ifdef IG_TEST_CODE25_INTERLEAVED_1D
	bBarcodeType = DO_CODE_25_INTERLEAVED;
	bNormalNarrowWidth = 8;
	bWideWidth = 16;
	bSpacerWidth = 0;
	bThinWideBlackBars = 0;
	fbThinWideWithSubstitute = 0;
	fbDoDithering = 0;
	bThinNarrowBlackBars = 0;
	fbThinNarrowWithSubstitute = 0;
#else
	#ifdef IG_TEST_CODE39_1D
		bBarcodeType = DO_CODE_39_STANDARD;
		bNormalNarrowWidth = 5;
		bWideWidth = 12;
		bSpacerWidth = 5;
		bThinWideBlackBars = 0;
		fbThinWideWithSubstitute = 0;
		fbDoDithering = 1;
		bThinNarrowBlackBars = 0;
		fbThinNarrowWithSubstitute = 0;
	#else
		#ifdef IG_TEST_CODE128_1D
			bBarcodeType = DO_CODE_128;
			bNormalNarrowWidth = 4;
			bWideWidth = 0;
			bSpacerWidth = 0;
			bThinWideBlackBars = 0;
			fbThinWideWithSubstitute = 0;
			fbDoDithering = 0;
			bThinNarrowBlackBars = 0;
			fbThinNarrowWithSubstitute = 0;

		#else
	// not doing any testing of a particular 1D barcode, so do normal processing
	// determine which type of barcode we're trying to generate.
	switch(wVCR)
	{
		case VCR_CODE_128_DATA:
		case VCR_CODE_128_RPT_START:
		case VCR_CODE_128_RPT_END:
			bBarcodeType = DO_CODE_128;
			break;

		case VCR_CODE_39S_DATA:
			bBarcodeType = DO_CODE_39_STANDARD;
			break;

//		case VCR_CODE_39E_DATA:
//			bBarcodeType = DO_CODE_39_EXTENDED;
//			break;

		case VCR_CODE_25I_DATA:
			bBarcodeType = DO_CODE_25_INTERLEAVED;
			break;

		default:
			lErrorCode = IG_UNKNOWN_BARCODE_VCR_ID;
			return (lErrorCode);
			//break;
	}				

	// try to get the barcode details from one of the EMD parameters.
	pbWorkBuffer = fnFlashGetPackedByteParm(PBP_1D_BARCODE_DETAILS);
	if (pbWorkBuffer)
	{
		if (*pbWorkBuffer != bBarcodeType)
		{
			// the first EMD parameter isn't for the type of 1D barcode we want to
			// generated, so check the other EMD parameter
			pbWorkBuffer = fnFlashGetPackedByteParm(PBP_1D_BARCODE_DETAILS_2);
			if (pbWorkBuffer)
			{
				if (*pbWorkBuffer != bBarcodeType)
				{
					// the second EMD parameter also isn't for the type of 1D barcode
					// we want to do, so return an error.
					lErrorCode = IG_INVALID_BARCODE_TYPE;
					return (lErrorCode);
				}
			}
			else
			{
				lErrorCode = IG_NO_BARCODE_DATA;
				return (lErrorCode);
			}
		}

		// advance the pointer past the barcode type byte and get the rest of the details
		pbWorkBuffer++;
		bNormalNarrowWidth = *pbWorkBuffer++;
		bWideWidth = *pbWorkBuffer++;
		bSpacerWidth = *pbWorkBuffer++;
		bThinWideBlackBars = *pbWorkBuffer & 0x0F;
		fbThinWideWithSubstitute = *pbWorkBuffer++ & 0xF0;
		fbDoDithering = *pbWorkBuffer++;
		bThinNarrowBlackBars = *pbWorkBuffer & 0x0F;
		fbThinNarrowWithSubstitute = *pbWorkBuffer++ & 0xF0;
	}
	else
	{
		lErrorCode = IG_NO_BARCODE_DATA;
		return (lErrorCode);
	}

		#endif
	#endif
#endif

	// Set up the pointers
	pwString = wString;
    pbWorkBuffer = bWorkBuffer;
	pbBlack = bBlackBarData;
	pbEvenBlack = bEvenBlackBarData;

	switch(bBarcodeType)
	{
	case DO_CODE_128:
		if (wDataBytes > MAX_1D_DATA_SIZE * 2)
		{
			lErrorCode = IG_TOO_MUCH_BARCODE_DATA;
			return (lErrorCode);
		}

		// Remove any spaces from the input string
		for (wIndex = 0; wIndex < wDataBytes; wIndex++)
		{
			if (pwInput[wIndex] != 0x20)
				wString[wNumBytesData++] = pwInput[wIndex];
		}

		if (wNumBytesData > MAX_1D_DATA_SIZE)
		{
			lErrorCode = IG_TOO_MUCH_BARCODE_DATA;
			return (lErrorCode);
		}
		break;

	case DO_CODE_39_STANDARD:
	case DO_CODE_39_EXTENDED:
		if (bBarcodeType == DO_CODE_39_EXTENDED)
		{
			// if the input string is bigger than the max size, the translated
			// string probably won't fit in the work buffer, so return an error.
			if (wDataBytes > MAX_1D_DATA_SIZE)
			{
				lErrorCode = IG_TOO_MUCH_BARCODE_DATA;
				return (lErrorCode);
			}

			// convert the input string
			for (wIndex = 0; wIndex < wDataBytes; wIndex++)
			{
				if (pwInput[wIndex] > 0x7F)
				{
					lErrorCode = IG_INVALID_BARCODE_DATA;
					return (lErrorCode);
				}
				else
				{
					bLoopCount = (unsigned char)strlen(&(Code39OneDTranslation[pwInput[wIndex]][0]));
					for (bCnt = 0; bCnt < bLoopCount; bCnt++)
					{
						wString[wNumBytesData++] = (unsigned char)Code39OneDTranslation[pwInput[wIndex]][bCnt];
					}
				}
			}

			// if the converted data is too big, return an error.
			if (wNumBytesData > MAX_1D_DATA_SIZE * 2)
			{
				lErrorCode = IG_TOO_MUCH_BARCODE_DATA;
				return (lErrorCode);
			}
		}
		else		// Code 39 Standard Set
		{
			if (wDataBytes > MAX_1D_DATA_SIZE)
			{
				lErrorCode = IG_TOO_MUCH_BARCODE_DATA;
				return (lErrorCode);
			}
			else
			{
				// copy the input string to the wString buffer because the rest of the algorithm
				// expects the data to be in the wString buffer.
				(void)memcpy((unsigned char *)wString, (unsigned char *)pwInput, wDataBytes * 2);
				wNumBytesData = wDataBytes;
			}
		}
		break;

	case DO_CODE_25_INTERLEAVED:
		if (wDataBytes > MAX_1D_DATA_SIZE * 2)
		{
			lErrorCode = IG_TOO_MUCH_BARCODE_DATA;
			return (lErrorCode);
		}

		// Remove any non-numeric characters from the input string
		for (wIndex = 0; wIndex < wDataBytes; wIndex++)
		{
			if ((pwInput[wIndex] >= 0x30) && (pwInput[wIndex] <= 0x39))
				wString[wNumBytesData++] = pwInput[wIndex] - CODE25_BASE_CHAR;
		}

		// have to have an even number of characters to do interleaved 2 of 5,
		// so if there is an odd number of characters, add a zero character
		if (wNumBytesData & 0x01)
		{
			// first make sure the even number of characters won't be too many characters
			if (wNumBytesData + 1 > MAX_1D_DATA_SIZE)
			{
				lErrorCode = IG_TOO_MUCH_BARCODE_DATA;
				return (lErrorCode);
			}

			switch(bSpacerWidth)
			{
				case 0:
					// prepend a zero character to the beginning of the string
					for (wIndex = wNumBytesData; wIndex >0; wIndex--)
					{
						wString[wIndex] = wString[wIndex - 1];
					}
					
					wString[0] = 0x30;
					break;

				case 1:
				default:
					// append a zero character to the end of the string
					wString[wNumBytesData] = 0x30;
					break;
			}

			wNumBytesData++;
		}
		else
		{
			if (wNumBytesData > MAX_1D_DATA_SIZE)
			{
				lErrorCode = IG_TOO_MUCH_BARCODE_DATA;
				return (lErrorCode);
			}
		}
		break;

	default:
		// at this point, we should have already determined if we have an invalid barcode type and returned,
		// so just return w/ no error at this point because don't know what else to do.
		return (IG_NO_ERROR);
		//break;
	}


    /* Do we have what we need to generate the 1D Barcode? */
    if (!wNumBytesData)
	{
		lErrorCode = IG_MISSING_1D_INFO;
		return (lErrorCode);
	}

	wIndex = 1;

    /* Lets get ready to encode the Barcode Data */
	switch(bBarcodeType)
	{
		case DO_CODE_128:
	        /* See if we should start encoding with Code C */
	        /* We only want to start in code C if there are at least 4 numbers in a row */
	        if ((wNumBytesData > 3) && (DoCodeC(pwString, bMode) == TRUE))
	        {
	            /* Start with the Code C encoding */
	            bMode = USE_CODE_C;
	            *pbWorkBuffer = START_C;
	        }
	        else
	        {
	            /* Start with the Code B encoding */
	            bMode = USE_CODE_B;
	            *pbWorkBuffer = START_B;
	        }
    
	        /* Start the Code 128 check digit calculation */
	        lCheckSumValue = *pbWorkBuffer++;

			// while we still have bytes to process and we haven't hit a null terminator,
			// calculate the code
	        while ((wNumBytesData) && (*pwString))
	        {
	            if(bMode == USE_CODE_C)
	            {
	                /* Encode the next 2 pieces of data which are numbers */
						*pbWorkBuffer = (unsigned char)((*pwString++ - 0x30) * 10);
	                *pbWorkBuffer += (unsigned char) (*pwString++ - 0x30);
	                wNumBytesData -= 2;
                
	                /* Continue the Check Digit Calculation */
	                lCheckSumValue += (wIndex * (*pbWorkBuffer++));
	                wIndex++;
                
	                /* See if we should stay in code C or switch to code B */
					/* i.e., if less than 2 bytes left or the next 2 bytes of data aren't
						UNICODE digits, switch to code B as long as we haven't hit a
						null terminator
					*/
	                if(((wNumBytesData < 2) || (DoCodeC(pwString, bMode) == FALSE)) && 
	                   (*pwString))
	                {
	                    /* Switch to Code B */
	                    bMode = USE_CODE_B;
	                    *pbWorkBuffer = CODE_B;
                
	                    /* Continue the Check Digit Calculation */
	                    lCheckSumValue += (wIndex * (*pbWorkBuffer++));
	                    wIndex++;
	                }
                
	            }
	            else
	            {
	                /* Encode the next character */
					*pbWorkBuffer = (unsigned char)(*pwString++ - 0x20);
	                wNumBytesData--;

	                /* Continue the Check Digit Calculation */
	                lCheckSumValue += (wIndex * (*pbWorkBuffer++));
	                wIndex++;
                
	                /* See if we should stay in code B or switch to code C */
	                if((wNumBytesData > 3) && (DoCodeC(pwString, bMode) == TRUE))
	                {
	                    /* Switch to Code C */
	                    bMode = USE_CODE_C;
	                    *pbWorkBuffer = CODE_C;
                
	                    /* Continue the Check Digit Calculation */
	                    lCheckSumValue += (wIndex * (*pbWorkBuffer++));
	                    wIndex++;
	                }
	            }
	        }
        
	        /* We are done encoding the data, finish the Check Digit Calculation. */
	        /* We need to compute a mod ONE_D_MOD, but we don't want to use the   */
	        /* "mod" command because it will pull in the 'C' libraries and the    */
	        /* code grows too much                                                */
        
	        /* Start by subtracting ONE_D_MOD * 10 and then subtract          */
	        /* ONE_D_MOD, so we don't have to do so many loops                */
	        while( lCheckSumValue >= ONE_D_MOD_TIMES_10)
	            lCheckSumValue -= ONE_D_MOD_TIMES_10;

	        /* Now subtract the ONE_D_MOD until less than ONE_D_MOD        */
	        while( lCheckSumValue >= ONE_D_MOD)
	            lCheckSumValue -= ONE_D_MOD;

	        /* Place the checksum into the buffer */
	        *pbWorkBuffer++ = (unsigned char) lCheckSumValue;
	        wIndex++;

	        /* Place the Stop character in the buffer */        
	        *pbWorkBuffer = STOP_CODE;
	        wIndex++;

			/* Determine the width of the Code 128 barcode */
			/*
				# of bars = # of characters (i.e., index value) * # of bars in a character (i.e., 11)
				Add 2 bars for the 2 black bars that have to be to the right of the stop character.
				width = total # of bars * width of a bar.
			*/
			wBarcodeWidth = (unsigned short)(((wIndex * 11) + 2) * bNormalNarrowWidth);
			break;

		case DO_CODE_39_STANDARD:
		case DO_CODE_39_EXTENDED:
			*pbWorkBuffer = CODE39_START_CODE;
			pbWorkBuffer++;

			while (wNumBytesData)
			{
				*pbWorkBuffer = (unsigned char)(*pwString++ - CODE39_BASE_CHAR);
				if (*pbWorkBuffer >= MAX_CODE39_CHARS)
				{
					lErrorCode = IG_INVALID_BARCODE_DATA;
					return (lErrorCode);
				}
				else
				{
					pbWorkBuffer++;
					wNumBytesData--;
					wIndex++;
				}
			}


			*pbWorkBuffer = CODE39_STOP_CODE;
			wIndex++;

			/* Determine the width of the Code 39 barcode */
			/*
				width = # of characters (i.e., index value) * size of a character
				(i.e., 6 narrow bars + 3 wide bars + spacer bar).
				Subtract the width of a spacer bar from the total because we don't
				need a spacer bar to the right of the stop character.
			*/
			wBarcodeWidth = (unsigned short)((wIndex * ((6 * bNormalNarrowWidth) + (3 * bWideWidth) + bSpacerWidth)) - bSpacerWidth);

			// indicate to skip the spacer bar for the first character.
			fbFirstChar = TRUE;
			break;

		case DO_CODE_25_INTERLEAVED:
			// load the bar values for the start character
			ucCode25Buffer[wIndex-1] = 1;
			ucCode25Buffer[wIndex++] = 1;
			ucCode25Buffer[wIndex++] = 1;
			ucCode25Buffer[wIndex++] = 1;

			// load the bar values for the actual characters
			// load two characters at a time while alternating between the bar values
			// for each character
			for (bCnt = 0; bCnt < (wNumBytesData / 2); bCnt++)
			{
				for (bLoopCount = 0; bLoopCount < MAX_CODE25_BARS; bLoopCount++)
				{
					ucCode25Buffer[wIndex++] = Code25OneDBarPatterns[pwString[bCnt*2]][bLoopCount];
					ucCode25Buffer[wIndex++] = Code25OneDBarPatterns[pwString[(bCnt*2)+1]][bLoopCount];
				}
			}

			// load the bar values for the stop character
			ucCode25Buffer[wIndex++] = 2;
			ucCode25Buffer[wIndex++] = 1;
			ucCode25Buffer[wIndex++] = 1;

			// load the empty bars to bring the total to a number that is a multiple of 10
			ucCode25Buffer[wIndex++] = 0;
			ucCode25Buffer[wIndex++] = 0;
			ucCode25Buffer[wIndex++] = 0;

			// change the index value to the number of multiples of 10 bars
			wIndex = wIndex / 10;

			/* Determine the width of the Code 25 barcode */
			/*
				width = # of characters * size of a character (i.e., width of 3 narrow bars +
				width of 2 wide bars) plus the size of the start character plus
				the size of the stop character.
			*/
			wBarcodeWidth = (unsigned short)((wNumBytesData * ((3 * bNormalNarrowWidth) + (4 * bWideWidth))) +
												(6 * bNormalNarrowWidth) + bWideWidth);
			break;

		default:
			// at this point, we should have already determined if we have an invalid barcode type and returned,
			// so just return w/ no error at this point because don't know what else to do.
			return (IG_NO_ERROR);
			//break;
		}

        /* Load the barcode into the print area */
		// We load from right to left, or top to bottom
		switch (bJust)
		{
		case HORIZ_L:
		case HORIZ_R:
		case HORIZ_C:
			// First setup some preliminary stuff
			/* Determine the amount padding that is necessary */
			if (wWidth >= wBarcodeWidth)
			{
				wNumPadCols = wWidth - wBarcodeWidth;
				if (bJust == HORIZ_C)
					wNumPadCols /= 2;
			}
			else
			{
#if !defined(IG_TEST_CODE128_1D) && !defined(IG_TEST_CODE39_1D)
	            lErrorCode = IG_INVALID_BARCODE_WIDTH;
				break;			// exit the switch statement so we go to the end of the function
#else
				if ((wBarcodeWidth + wStartCol + 50) < (IMAGE_COLS + START_OFFSET))
					wNumPadCols = 50;
				else
				{
		            lErrorCode = IG_INVALID_BARCODE_WIDTH;
					break;			// exit the switch statement so we go to the end of the function
				}
#endif
			}
        
			wUsedCols = wWidth;
			pbImagePtr = &bImageBuffer[wStartCol][wStartRow/8];

			// Determine if the bar has to start within a byte
			bExtraBitsAbove = wStartRow % 8;

			// Setup the black bar and the length
			bBarLength = (unsigned char)((wHeight / 8) + (bExtraBitsAbove ? 1 : 0));
			wBlackCnt = wHeight;

			if (fbDoDithering)
			{
				// first, clear the buffers
				(void)memset(bBlackBarData, 0, bBarLength);
				(void)memset(bEvenBlackBarData, 0, bBarLength);

				// Load the first byte
				*pbBlack++ = bOddMask & (0xFF >> bExtraBitsAbove);
				*pbEvenBlack++ = bEvenMask & (0xFF >> bExtraBitsAbove);
				wBlackCnt -= 8 - bExtraBitsAbove;

				// load the rest of the whole bytes
				while (wBlackCnt >= 8)
				{
					*pbBlack++ = bOddMask;
					*pbEvenBlack++ = bEvenMask;
					wBlackCnt -= 8;
				}

				// load any remaining partial byte
				if (wBlackCnt)
				{
					*pbBlack = bOddMask & (0xFF << (8 - (unsigned char)wBlackCnt));
					*pbEvenBlack = bEvenMask & (0xFF << (8 - (unsigned char)wBlackCnt));
				}
			}
			else
			{
				// first, clear the buffer
				(void)memset(bBlackBarData, 0, bBarLength);

				// Load the first byte
				*pbBlack++ = 0xFF >> bExtraBitsAbove;
				wBlackCnt -= 8 - bExtraBitsAbove;

				// load the rest of the whole bytes
				while (wBlackCnt >= 8)
				{
					*pbBlack++ = 0xFF;
					wBlackCnt -= 8;
				}

				// load any remaining partial byte
				if (wBlackCnt)
					*pbBlack = 0xFF << (8 - (unsigned char)wBlackCnt);
			}

			// Now do the actual loading of the data
			/* Add in the pad columns if left-justified or center-justified */
			if (((bJust == HORIZ_L) || (bJust == HORIZ_C)) && (wNumPadCols))
			{
				while (wNumPadCols && wUsedCols)
				{
					(void)memset(pbImagePtr, 0, bBarLength);
					pbImagePtr += IMAGE_ROW_BYTES;
					wNumPadCols--;
					wUsedCols--;
				}
			}

			if (bBarcodeType == DO_CODE_128)
			{
				/* Place the 2 Black bars to the right of the Stop code */
				wNumCols = bNormalNarrowWidth * 2;
				if (bThinWideBlackBars)
				{
					wNumCols -= bThinWideBlackBars;

					// if we're supposed to substitute white columns for the
					// thinned black columns, set the substitution value
					if (fbThinWideWithSubstitute)
						bExtraWhite = bThinWideBlackBars;
				}

				while (wNumCols && wUsedCols)
				{
					if (fbDoDithering)
					{
						if (wNumCols & 0x01)
							(void)memcpy(pbImagePtr, bBlackBarData, bBarLength);
						else
							(void)memcpy(pbImagePtr, bEvenBlackBarData, bBarLength);
					}
					else
					{
						(void)memcpy(pbImagePtr, bBlackBarData, bBarLength);
					}

					pbImagePtr += IMAGE_ROW_BYTES;
					wNumCols--;
					wUsedCols--;
				}
			}

			/* Start building the rest of the barcode */
			while (wIndex)
			{
				/* Find the last white bar width for current character and */
				/* then move to the next character in the buffer           */
				switch(bBarcodeType)
				{
				case DO_CODE_128:
					pbBarWidth = (unsigned char *) &(OneDBarPatterns[*pbWorkBuffer--].white3);
					bLoopCount = 3;
					break;

				case DO_CODE_39_STANDARD:
				case DO_CODE_39_EXTENDED:
					pbBarWidth = (unsigned char *) &(Code39OneDBarPatterns[*pbWorkBuffer--][MAX_CODE39_BARS-1]);
					bLoopCount = 5;
					break;

				case DO_CODE_25_INTERLEAVED:
					pbBarWidth = (unsigned char *) &(ucCode25Buffer[(wIndex * 10) - 1]);
					bLoopCount = 5;
					break;

				default:
					// at this point, we should have already determined if we have an invalid barcode type and returned,
					// so just return w/ no error at this point because don't know what else to do.
					return (IG_NO_ERROR);
					//break;
				}
            
				/* Generate white and black bars for the current data */
				for (bCnt = 0; bCnt < bLoopCount; bCnt++)
				{
					/* Generate the white bars */
					/* Determine the width of the bar and then move to the */
					/* next bar width                                      */
					bBars = *pbBarWidth--;

					switch(bBarcodeType)
					{
					case DO_CODE_128:
						// add in whatever was removed from the set of black bars
						// that are to the right of this set of white bars
						wNumCols = (bBars * bNormalNarrowWidth) + bExtraWhite;
						break;

					case DO_CODE_39_STANDARD:
					case DO_CODE_39_EXTENDED:
						if (fbFirstChar == TRUE)
						{
							// doing the first (right-most) character for the barcode
							// this is the stop character
							// we don't need the white spacer bar to the right of
							// the stop character, so set the width to zero.
							fbFirstChar = FALSE;
							wNumCols = 0;
						}
						else
						{
							if (!bCnt)
							{
								// doing the white space bar
								wNumCols = bSpacerWidth + bExtraWhite;
							}
							else
							{
								// doing a white bar in the actual barcode character
								switch (bBars)
								{
								case 1:
									wNumCols = bNormalNarrowWidth + bExtraWhite;
									break;

								case 2:
									wNumCols = bWideWidth + bExtraWhite;
									break;

								default:
									wNumCols = 0;
									break;
								}
							}
						}
						break;

					case DO_CODE_25_INTERLEAVED:
						switch (bBars)
						{
						case 1:
							wNumCols = bNormalNarrowWidth + bExtraWhite;
							break;

						case 2:
							wNumCols = bWideWidth + bExtraWhite;
							break;

						default:
							wNumCols = 0;
							break;
						}
						break;

					default:
						// at this point, we should have already determined if we have an invalid barcode type and returned,
						// so just return w/ no error at this point because don't know what else to do.
						return (IG_NO_ERROR);
						//break;
					}

					while (wNumCols && wUsedCols)
					{
						(void)memset(pbImagePtr, 0, bBarLength);
						pbImagePtr += IMAGE_ROW_BYTES;
						wNumCols--;
						wUsedCols--;
					}

					/* Generate the black bars */
					/* Determine the width of the bar and then move to the  */
					/* next bar width                                       */
					bBars = *pbBarWidth--;

					switch(bBarcodeType)
					{
					case DO_CODE_128:
						bExtraWhite = 0;	// clear the thinning substitution value

						wNumCols = bBars * bNormalNarrowWidth;
						
						// if need to do thinning, only do it if more than one bar
						if (bThinWideBlackBars && (bBars > 1))
						{
							wNumCols -= bThinWideBlackBars;

							// if we're supposed to substitute white columns for the
							// thinned black columns, set the substitution value
							if (fbThinWideWithSubstitute)
								bExtraWhite = bThinWideBlackBars;
						}
						break;

					case DO_CODE_39_STANDARD:
					case DO_CODE_39_EXTENDED:
					case DO_CODE_25_INTERLEAVED:
						bExtraWhite = 0;	// clear the thinning substitution value

						switch (bBars)
						{
						case 1:
							if (bThinNarrowBlackBars)
							{
								wNumCols = bNormalNarrowWidth - bThinNarrowBlackBars;

								// if we're supposed to substitute white columns for the
								// thinned black columns, set the substitution value
								if (fbThinNarrowWithSubstitute)
									bExtraWhite = bThinNarrowBlackBars;
							}
							else
								wNumCols = bNormalNarrowWidth;
							break;

						case 2:
							if (bThinWideBlackBars)
							{
								wNumCols = bWideWidth - bThinWideBlackBars;

								// if we're supposed to substitute white columns for the
								// thinned black columns, set the substitution value
								if (fbThinWideWithSubstitute)
									bExtraWhite = bThinWideBlackBars;
							}
							else
								wNumCols = bWideWidth;
							break;

						default:
							wNumCols = 0;
							break;
						}
						break;

					default:
						// at this point, we should have already determined if we have an invalid barcode type and returned,
						// so just return w/ no error at this point because don't know what else to do.
						return (IG_NO_ERROR);
						//break;
					}

					while (wNumCols && wUsedCols)
					{
						if (fbDoDithering)
						{
							if (wNumCols & 0x01)
								(void)memcpy(pbImagePtr, bBlackBarData, bBarLength);
							else
								(void)memcpy(pbImagePtr, bEvenBlackBarData, bBarLength);
						}
						else
						{
							(void)memcpy(pbImagePtr, bBlackBarData, bBarLength);
						}

						pbImagePtr += IMAGE_ROW_BYTES;
						wNumCols--;
						wUsedCols--;
					}
				}

				/* we are done with this character */
				wIndex--;
			}

			/* Add in the pad columns for whatever is left in the width */
			while (wUsedCols)
			{
				(void)memset(pbImagePtr, 0, bBarLength);
				pbImagePtr += IMAGE_ROW_BYTES;
				wUsedCols--;
			}
			break;

//?? The vertical barcode processing isn't needed yet
/*
		case VERT_B:
		case VERT_T:
		case VERT_C:
			break;
*/
		default:
			lErrorCode = IG_UNKNOWN_JUSTIFICATION;
			break;
	    }

#ifdef IG_TIME_DEBUG
	(void)sprintf(pLogBuf, "End IG_1DBarcode");
	fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

	return (lErrorCode);
}


/****************************************************************************
// FUNCTION NAME: IGU_SetPixel
// DESCRIPTION: Set the defined pixel
//
// AUTHOR: 
//
// GLOBALS USED: 
//
// INPUTS: psDevInfo : Pointer on the structure which describes the type of device
//         wColumn :   X coordinate of the pixel to set
//         wRow :      Y coordinate of the pixel to set
//
// OUTPUTS:
*****************************************************************************/
static void IGU_SetPixel(const IGU_TC_OUTPUT_DEV_INFO *psDevInfo, 
                         const unsigned short wColumn, const unsigned short wRow)
{
  unsigned short wRowByte;
  unsigned char  bRowShiftCount;
  unsigned char  bRowBitMask;


  wRowByte = wRow / 8;
  bRowShiftCount = wRow % 8;
  bRowBitMask = (0x01) << bRowShiftCount;
  bRowBitMask = ByteMirror[bRowBitMask]; // (?) TO BE CHECKED WITH TESTS BDE 4EXPMET

  //bImageBuffer[wColumn][wRowByte] |= bRowBitMask;
  *(psDevInfo->pbBuff + (wColumn * psDevInfo->wMaxRowBytes) + wRowByte) |= bRowBitMask;
}


/****************************************************************************
// FUNCTION NAME: IGU_TCCalculateAngles
// DESCRIPTION: Computes the 2 angles that define the beginning and
//              the end of the text that will be printed on the Town Circle.
//              The top part of the Town Circle and the bottom part are
//              managed separately.
//
// AUTHOR: B. Debuire
//
// GLOBALS USED: 
//
// INPUTS: fIsTopText :    True if the text is to be printed on the top part
//                         of the Town Circle.
//         wRadiusPixels : Radius in pixels of the circle on which the chars
//                         will sit.
//         psTextInfo :    Pointer on the structure that contains all information 
//                         about the text to print
//
// OUTPUTS:
//
// MODIFICATION HISTORY:
//      07/24/2008  Adam Liu      Fix the defect for bending town names. 
//                                  Exchange the min/max angles. 
//                  B. Debuire    Initial version
*****************************************************************************/
static void IGU_TCCalculateAngles(const BOOL fIsTopText, const unsigned short wRadiusPixels, 
                                  IGU_TC_TEXT_INFO *psTextInfo)
{	
  double dAngleFromXAxisRad = 0.0;


  psTextInfo->fIsTopText = fIsTopText;
  psTextInfo->wWidth = (unsigned short)(psTextInfo->wNumTextChars * psTextInfo->wFontWidth);
  psTextInfo->dAngleDiffRad = (double)psTextInfo->wWidth / (double)wRadiusPixels;
  dAngleFromXAxisRad = (PI / 2.0) - (psTextInfo->dAngleDiffRad / 2.0);
  if (psTextInfo->dAngleDiffRad < PI)
  {		
    if (psTextInfo->fIsTopText)
    {
      psTextInfo->dAngleMinRad = -(PI - dAngleFromXAxisRad);
      psTextInfo->dAngleMaxRad = -dAngleFromXAxisRad;     
    }
    else
    {
      psTextInfo->dAngleMinRad = +(PI - dAngleFromXAxisRad);
      psTextInfo->dAngleMaxRad = +dAngleFromXAxisRad;
    }
  }
  else
  {
    // length required is greater than 180 degrees (PI radians),
    // truncate it to 180 degrees
    dAngleFromXAxisRad = PI;
    if (psTextInfo->fIsTopText)
    {
      psTextInfo->dAngleMinRad = -(PI - dAngleFromXAxisRad);
      psTextInfo->dAngleMaxRad = -dAngleFromXAxisRad;
    }
    else
    {
      psTextInfo->dAngleMinRad = +(PI - dAngleFromXAxisRad);
      psTextInfo->dAngleMaxRad = +dAngleFromXAxisRad;
    }
  }
}


/****************************************************************************
// FUNCTION NAME: IGU_TCDrawText
// DESCRIPTION: Computes the coordinates of a pixel, in order to find the
//              correspondance in a circle  which center and radius are known.
//              This algorithm is used to print a town name inside a Town Circle,
//              using a curved print.
//              The text to print has already been stored in a buffer
//
// AUTHOR: 
//
// GLOBALS USED: 
//
// INPUTS: psDevInfo :    Pointer on a structure which describes the type of device
//                        this print is for (type of meter)
//         psTextInfo :   Pointer on a structure containing the information about 
//                        the text to print (font, angles, buffer address, etc...)
//         psCircleInfo : Pointer on a structure which describes the basic circle 
//                        used to curve the text.
//
// OUTPUTS:
//
// MODIFICATION HISTORY:
//      07/24/2008  Adam Liu      Fix the defect for bending town names. 
//                                  Correct the data types and algorithm. 
//                  B. Debuire    Initial version
*****************************************************************************/
static void IGU_TCDrawText(const IGU_TC_OUTPUT_DEV_INFO *psDevInfo, const IGU_TC_TEXT_INFO *psTextInfo,
	                         const IGU_TC_TOWN_CIRCLE_PARAMETERS *psCircleInfo)
{
  double         dAngle, dCosAngle, dSinAngle, dAminRad, dAmaxRad;
//  short wX, wY;
  short wR1, wX1, wY1, wX2, wY2, wXmax, wYmax, wRowByte, wYAdj;
//  unsigned short wColMax, wRowMax;
  unsigned char  bDataByte, bFontBitMask;


  dAminRad = psTextInfo->dAngleMinRad;
  dAmaxRad = psTextInfo->dAngleMaxRad;
	
  // use algorithm from france
  wXmax = (short)psTextInfo->wWidth;
  wYmax = (short)psTextInfo->wHeight;
  for(wX1 = 0; wX1 < wXmax; wX1++)  // for each column of text string image
  {
    dAngle = dAminRad + (dAmaxRad - dAminRad) * (double)(wXmax - wX1) / (double)wXmax;
    dCosAngle = cos(dAngle);
    dSinAngle = sin(dAngle);
    for (wY1 = 0; wY1 < wYmax; wY1++)  // for each row of text string image
    {
      // locate the pixel in the original text string image
      wYAdj = (wYmax - wY1 - 1); // The characters are scanned to be compliant with the system coordinates
      
      if (psTextInfo->fIsTopText)
      { // The bottom of the characters is on the text radius
        wR1 = (short)(psCircleInfo->wTextRadius + wY1);
      }
      else
      { // The top of the characters is on the text radius
        wR1 = (short)(psCircleInfo->wTextRadius + wYmax - wY1);
      }
	  
      wRowByte = wYAdj / 8;
      bFontBitMask = (0x01 << (wYAdj % 8));
      bDataByte = *( psTextInfo->pbTextImage + (wRowByte * wXmax) +  wX1 );

      // if the pixel is set in the text string image, then set it in the circle
      if (bDataByte & bFontBitMask)
      {
        // locate position in circle
        //wR1 = psCircleInfo->wRmax - (wYmax - wY1);
        wX2 = (short)psCircleInfo->wOriginX + (short)((double)wR1 * dCosAngle * psDevInfo->dHorizontalScaleFactor);
		
        // BDE 4EXPMET
        // THIS SHOULD BE CHECKED DURING THE TESTS. I'M NOT SURE OF THAT LINE (ABOVE)
        // FOR ME, THE LINE SHOULD BE THE FOLLOWING ONE : (BELOW)
        //wX2 = psCircleInfo->wOriginX - ((unsigned short)(wR1 * dCosAngle * psDevInfo->dHorizontalScaleFactor));
        // BDE 4EXPMET
        wY2 = (short)psCircleInfo->wOriginY + (short)((double)wR1 * dSinAngle * psDevInfo->dVerticalScaleFactor);
        
        IGU_SetPixel(psDevInfo, (unsigned short)wX2, (unsigned short)wY2);
      }			
    }
  }
}


/****************************************************************************
// FUNCTION NAME: IGU_TCInitializeOutputDevInfo
// DESCRIPTION: Initialization of the structure that will be used to manage
//              the printing of the Town Circle
//              This initialization is designed to be meter-dependent
//
// AUTHOR: Bruno Debuire
//
// GLOBALS USED: bImageBuffer
//
// INPUTS: psDevInfo : Pointer on the structure to initialize
//
// OUTPUTS:
*****************************************************************************/
static void IGU_TCInitializeOutputDevInfo(IGU_TC_OUTPUT_DEV_INFO *psDevInfo)
{
  if (psDevInfo)
  {
    (void)memset((void*)psDevInfo,0,sizeof(IGU_TC_OUTPUT_DEV_INFO));
    psDevInfo->wMaxCols = MAX_COLS;
    psDevInfo->wMaxRows = MAX_ROWS;
    psDevInfo->wMaxRowBytes = IMAGE_ROW_BYTES;
    psDevInfo->pbBuff = (unsigned char*)bImageBuffer;
    
    psDevInfo->dVerticalScaleFactor = 1.0;
    
#ifdef K700
///////////////////////////////////////
// K700 Start
    psDevInfo->dHorizontalScaleFactor = ((double)300)/((double)600);
// K700 End
///////////////////////////////////////
#else
  #ifdef G900
  ///////////////////////////////////////
  // G900 Start
    psDevInfo->dHorizontalScaleFactor = ((double)300)/((double)600);
  // G900 End
  ///////////////////////////////////////
  #else
  ///////////////////////////////////////
  // Janus Start
    psDevInfo->dHorizontalScaleFactor = ((double)400)/((double)600);
  // Janus End
  ///////////////////////////////////////
  #endif
#endif
  }
}
  

/****************************************************************************
// FUNCTION NAME: IGU_TCScaleFontCharBitmap
// DESCRIPTION: Place the required character in the buffer containing
//              the text that will be printed in the Town Circle.
//              Reverse the columns order 
//              This function is executed once for each character of the text
//              to print.
//
// AUTHOR: B. Debuire
//
// GLOBALS USED: 
//
// INPUTS: pbFontImage : Pointer on the beginning of the bitmap which contains the 
//                       character to place.
//         wCharIndex  : Index of the char inside the whole text to print
//         pTextInfo   : Pointer on the structure which defines the text to print.
//                       (dimensions of the font, number of chars to print,
//                        destination buffer)
//
// OUTPUTS:
*****************************************************************************/
static void IGU_TCScaleFontCharBitmap(const unsigned char *pbFontImage, const unsigned short wCharIndex, 
                                      const IGU_TC_TEXT_INFO *pTextInfo)
{
//  unsigned char *pbDestBuffer;
  unsigned short wXPixels, wYPixels, wDestByteOffset;
  unsigned short wXSrcPixels, wSrcByteOffset;
  unsigned short wWidthFactor, wDestByteOffsetYFactor;
  unsigned char bSrcByte, bSrcMask, bDestByte, bDestMask;
//  unsigned char bTCScaleFontCharBuff[512];


//  pbDestBuffer = pTextInfo->pbTextImage;
	
  for (wYPixels = 0; wYPixels < pTextInfo->wFontHeight; wYPixels++)
  {
    wWidthFactor = (unsigned short)((wYPixels / 8) * pTextInfo->wFontWidth);
	
    // Compute the mask associated with that pixel (which bit in the byte?)
    bSrcMask = 0x80 >> (wYPixels % 8);
	
    // Compute the mask associated with that pixel (which bit in the byte?)
    bDestMask = bSrcMask;
    wDestByteOffsetYFactor = (unsigned short)(((wYPixels / 8) * pTextInfo->wFontWidth * pTextInfo->wNumTextChars) + (pTextInfo->wFontWidth * wCharIndex));

    for (wXPixels = 0; wXPixels < pTextInfo->wFontWidth; wXPixels++)
    {
      // The standard font components store the bitmap in reverse column order
      // in other words, the column data at the smaller address in memory is for the greater x-coordinate
      // therefore, we must reverse it to match the rotation algorithm.
      
      // Pick the column inside the character, from the right of the char to the left
      wXSrcPixels = (pTextInfo->wFontWidth - wXPixels - 1);

      // Source
      // Compute the offset to reach the pixel inside the character (line and column)
      wSrcByteOffset = wWidthFactor + wXSrcPixels;
      // Read the value of the whole byte related to the pixel
      bSrcByte = *(pbFontImage + wSrcByteOffset);			

      // Destination
      // Pick the destination pixel inside the whole text to print (?)
      wDestByteOffset = wDestByteOffsetYFactor + wXPixels;
	  
      // BDE 4EXPMET
      // THIS SHOULD BE CHECKED DURING THE TESTS. I'M NOT SURE OF THAT CALCULATION (ABOVE)
      // FOR ME, THE FORMULA SHOULD BE THE FOLLOWING ONE : (2 LINES BELOW)
      // // (Remember that the font height is always a multiple of 8)
      // ulNbOfPreviousBytes = pTextInfo->wFontCharBitmapBytes * wCharIndex; // Total number of bytes for all the previous chars
      // wDestByteOffset = ulNbOfPreviousBytes
      //                 + ((wYPixels / 8) * pTextInfo->wFontWidth) + wXPixels; // Nb of bytes to reach that pixel in that char
      // BDE 4EXPMET

      // Read the value of the byte related to the destination pixel
      bDestByte = *(pTextInfo->pbTextImage + wDestByteOffset);

      if (bSrcByte & bSrcMask)
      { // If the source pixel is on
        // Set the destination pixel (the other pixels on the byte will be unchanged)
        bDestByte |= bDestMask;
        *(pTextInfo->pbTextImage + wDestByteOffset) = bDestByte;
      }

/*      (void)sprintf((char*)bTCScaleFontCharBuff,"(%d,%d)<-(%d,%d) (%04X=%02X)<-(%04X=%02X)",
              wXSrcPixels,wYPixels,
              wXPixels,wYPixels,
              wSrcByteOffset,bSrcMask,wDestByteOffset,bDestMask); */
    }
  }	
}


/****************************************************************************
// FUNCTION NAME: IGU_TCSetupCircleParameters
// DESCRIPTION: Initialization of the structure which describes the basic circle
//              used for the Town Circle management. (Center and radius).
//              This is done in accordance with the Region Map, and the Town Circle
//              data.
//
// AUTHOR: B. Debuire
//
// GLOBALS USED: 
//
// INPUTS: psCircleInfo   : Pointer on the structure to initialize
//         pGraphicRegion : Pointer on the graphic region where the Town Circle is printed
//         pTCInfo        : Pointer on the structure which describes the component used
//                          to support the curved text
//         psDevInfo      : Pointer on the structure which describes the type of device
//                          (contains the scale factor)
//
// OUTPUTS:
*****************************************************************************/
static void IGU_TCSetupCircleParameters(IGU_TC_TOWN_CIRCLE_PARAMETERS *psCircleInfo,
                                        REGION_MAP *pGraphicRegion, TOWN_CIRCLE_DATA *pTCInfo,
                                        IGU_TC_OUTPUT_DEV_INFO *psDevInfo)
{
  if (!psCircleInfo)
    return;

  (void)memset((void*)psCircleInfo,0,sizeof(IGU_TC_TOWN_CIRCLE_PARAMETERS));

  psCircleInfo->wOriginX = pGraphicRegion->wStartCol + (unsigned short)((double)(pTCInfo->wHeight/2) * psDevInfo->dHorizontalScaleFactor); //lint !e653
  psCircleInfo->wOriginY = pGraphicRegion->wStartRow + (pTCInfo->wHeight/2);
  
  psCircleInfo->wTextRadius = fnFlashGetWordParm(WP_TC_TEXT_RADIUS);
}


/****************************************************************************
// FUNCTION NAME: IGU_TCSetupTextBuffer
// DESCRIPTION: Initialization of the structure used for the
//              management of the text that will be printed inside the 2
//              possible parts of the Town Circle.
//              This structure defines the dimensions of the chars, the
//              starting and ending angles, the font to use, etc...
//
// AUTHOR: Bruno Debuire
//
// GLOBALS USED: bNumOfFonts, sFontData
//
// INPUTS: fIsTopText : True if the text is to be printed on the top part
//                      of the Town Circle.
//         wTextRadiusPixels : Distance in pixels between the center of the
//                             circle and the bottom of the text on the top part
//                             (or the top of the text on the bottom part)
//         pszText :    Pointer on the string to print
//         bFontId :    Identifier of the font to use for the printing
//         psFontInfo : Pointer on the structure to initialize
//
// OUTPUTS:
//
// MODIFICATION HISTORY:
//      07/24/2008  Adam Liu      Fix the defect for bending town names. 
//                                  Limit the max length of printed town names. 
//                  B. Debuire    Initial version
*****************************************************************************/
static BOOL IGU_TCSetupTextBuffer(const BOOL fIsTopText, const unsigned short wTextRadiusPixels, 
                                  const UNICHAR *pszText,const unsigned char bFontId, 
                                  IGU_TC_TEXT_INFO *psFontInfo)
{
  unsigned char	 *pbFontImage/*,*pbDestTextImage,*pbSrcTextImage*/;
  const UNICHAR  *pCurrChar;
  unsigned short wFontCharOffset;
  unsigned short wCurrTextChar;
  unsigned short wCurrChar;
//  unsigned short wX,wY,wXmax,wYmax;
  static unsigned char  bFontInx;
  static unsigned char *pbDefaultGlyph = NULL;
  static unsigned char bLastFontId;
  unsigned char  bMaxTextLength;


  if (pszText == NULL || psFontInfo == NULL)
    return FALSE;

  psFontInfo->pszText = pszText;
  psFontInfo->bFontId = bFontId;

  // Find the font
  if (pbDefaultGlyph == NULL || bLastFontId != bFontId)
  {
      for (bFontInx = 0; bFontInx < bNumOfFonts; bFontInx++)
      {
        if (sFontData[bFontInx].bFontID == bFontId)
          break;
      }
  }
  if (bFontInx == bNumOfFonts)
    return FALSE;

  // Find the offset to the font's bitmap

  psFontInfo->bFontInx = bFontInx;
  psFontInfo->wFontWidth = sFontData[bFontInx].wWidth;
  psFontInfo->wFontHeight = sFontData[bFontInx].wHeight;
  // (Remember that the font height is always a multiple of 8)
  psFontInfo->wFontCharBitmapBytes = (unsigned short)(sFontData[bFontInx].wWidth * (sFontData[bFontInx].wHeight / 8));
  
  psFontInfo->wNumTextChars = unistrlen(pszText); //Length in number of unichars
  bMaxTextLength = fnFlashGetByteParm(BP_ORIGIN_INFO_SIZE);
  if (psFontInfo->wNumTextChars > (unsigned short)bMaxTextLength)
  {
    psFontInfo->wNumTextChars = (unsigned short)bMaxTextLength;
  }
  
  psFontInfo->wWidth = (unsigned short)(psFontInfo->wNumTextChars * psFontInfo->wFontWidth);
  psFontInfo->wHeight = psFontInfo->wFontHeight;
  psFontInfo->pbTextImage = (unsigned char*)calloc(psFontInfo->wNumTextChars * psFontInfo->wFontCharBitmapBytes,1);
  if (psFontInfo->pbTextImage == NULL)
  {
    if (psFontInfo->wNumTextChars)
    {
      return FALSE;
    }
    else
    { // If there is no character to print
      return TRUE;
    }
  }

  // Compute the starting and ending angles, from font and radius (and text length)
  IGU_TCCalculateAngles(fIsTopText,(wTextRadiusPixels+psFontInfo->wFontHeight),psFontInfo);

  /* Before we fill the buffer, first find a character we can use as a default
     character to put in place of any characters in the town name that are not
     present in the selected font.  This will allow us to print all of the
     characters that are present while showing a default character for the
     others and will make it easy for someone to figure out which characters
     are causing the problem. */

  /* Use the space glyph as the default */
  if (pbDefaultGlyph == NULL || bLastFontId != bFontId)
  {
      for (wFontCharOffset = 0; wFontCharOffset < sFontData[bFontInx].wNumChars; wFontCharOffset++)
      {
          if (sFontData[bFontInx].wCharList[wFontCharOffset] == UNICODE_SPACE)
          {
              pbDefaultGlyph = sFontData[bFontInx].pbBitmap + (psFontInfo->wFontCharBitmapBytes * wFontCharOffset);
              break;
          }
      }
	  
      /* If the space glyph is not present then just use the very first glyph in
         the font */
      if (!pbDefaultGlyph)
          pbDefaultGlyph = sFontData[bFontInx].pbBitmap;

      bLastFontId = bFontId;
  }

  pCurrChar = pszText;
  wCurrTextChar = 0;
  while ((*pCurrChar != UNICODE_NULL) && (wCurrTextChar < psFontInfo->wNumTextChars))
  { // For each char of the text ....
    // Find the font bitmap for the char
    wCurrChar = (unsigned short)(*pCurrChar);
    pbFontImage = NULL;
    for (wFontCharOffset = 0; wFontCharOffset < sFontData[bFontInx].wNumChars; wFontCharOffset++)
    {
      if (sFontData[bFontInx].wCharList[wFontCharOffset] == wCurrChar)
      {
        pbFontImage = sFontData[bFontInx].pbBitmap + (psFontInfo->wFontCharBitmapBytes * wFontCharOffset);
        break;
      }
    }
	
    if (pbFontImage == NULL)
    {
      /* The requested glyph wasn't found.  Use the default one. */
      pbFontImage = pbDefaultGlyph;
    }

    // Place the character in the destination buffer
    IGU_TCScaleFontCharBitmap(pbFontImage,wCurrTextChar,psFontInfo);
    
    wCurrTextChar++; // Next place to set
    pCurrChar++; // Next char of the text
  }

  return TRUE;
}


/****************************************************************************
// FUNCTION NAME: PaintChar
// DESCRIPTION: This function paints a single character in the specified
//				rectangle.
//
// AUTHOR: Sandra Peterson
//
// GLOBALS USED: bImageBuffer
//
// INPUTS: pbImage = pointer to the font image data
//         wBytes = number of bytes of image data
//         wStartCol = starting column for the rectangle
//         wStartRow = starting row for the rectangle
//         wWidth = width of the rectangle
//         wHeight = height of the rectangle
//         bLoadBlack = non-zero if a solid block is to be loaded for the character
//
// OUTPUTS:	Error Code
*****************************************************************************/

unsigned long PaintChar(const unsigned char *pbImage, unsigned short wBytes,
						const unsigned short wStartCol, const unsigned short wStartRow,
						const unsigned short wWidth, const unsigned short wHeight,
						const unsigned char bLoadBlack)
{
	union
	{
		unsigned short wShift;
		struct
		{		
			unsigned char bHigh;	
			unsigned char bLow;
		} bShift;	
	}uShiftAccess;

	union
	{
		unsigned short wShift;
		struct
		{		
			unsigned char bHigh;	
			unsigned char bLow;
		} bShift;	
	}uDataAccess;

	register unsigned char	*pbBuf, *pbStartBuf;
	unsigned long lErrCode = IG_NO_ERROR;

	unsigned short	wCompHLeft;		// number of row bytes left to load for the graphic
	unsigned short	wCompWidth;		// number of columns in the graphic
	unsigned short	wCompWLeft;		// number of columns left to do on the current row
	unsigned short	wRowByte;		// starting/current row for loading the graphic

	unsigned char	bCompShift;		// number of bits to shift the data to get it to start
									// on the proper row.
	unsigned char	bDataByte;		// current data byte

	unsigned char bDummyValue, bFirstValue;

	char			pLogBuf[50];


	bDummyValue = bLoadBlack;
	bFirstValue = bLoadBlack;

	// make sure the character stays within the boundaries of the print area
	if ((wStartCol >= (IMAGE_COLS + START_OFFSET)) || (wStartRow >= MAX_ROWS) ||
		(wStartCol+wWidth > (IMAGE_COLS + START_OFFSET)) || (wStartRow+wHeight > (MAX_ROWS + ROW_ADJUST)))
		return(IG_INVALID_COORDINATES);

	// make sure the font graphic has a valid size
	if ((wWidth == 0) || (wHeight == 0))
	{
		(void)sprintf(pLogBuf, "Font has width/height of zero");
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
		return (IG_INVALID_SIZE);
	}

	// make sure the  data was provided
	if ((pbImage == NULL) || (wBytes == 0))
		return (IG_INVALID_IMAGE);

	// set up the initial data for loading the graphic
	wCompHLeft = wHeight / 8;
	bCompShift = wStartRow % 8;
	wCompWidth = wWidth;
	wCompWLeft = wCompWidth;
	wRowByte = wStartRow / 8;

	pbStartBuf = &bImageBuffer[wStartCol][wRowByte];
	pbBuf = pbStartBuf;

	// Create a mask for the bits that will be loaded.
	// Since we're only loading 8 bits, using the opposite bytes for the mask
	// will clear out the bits that will be loaded.
	// E.g., if the mask for the bits is 00011111 11100000, using the lower
	// mask byte for the upper buffer byte and the upper mask byte for the
	// lower buffer byte clears the desired bits.
	uShiftAccess.wShift = 0xFF00 >> bCompShift;

	/*
	 *	For each byte in the input so long as there isn't a problem
	 */

	while((wBytes != 0) && (lErrCode == IG_NO_ERROR)) 
	{
		/*
		 *	We need to copy this byte "as is" since the data for
		 *	the font characters isn't compressed.
		 */

		if (bLoadBlack)
		{
			if (bLoadBlack != 0xFF)
			{
				if (!(wCompWLeft & 0x07))
					bDummyValue = bDummyValue ^ 0xFF;
				bDataByte = bDummyValue;
			}
			else
				bDataByte = bLoadBlack;
		}
		else
		{
			bDataByte = ByteMirror[*pbImage];
		}

		/*
		 *	If we've run out of room on this row, we need to wrap
		 *	to the next row.  Moving to the next line will fail if we run
		 *	off the edge of the graphics buffer, which is a bad
		 *	thing.  This test needs to be done before the byte
		 *	is painted, rather than after, to avoid a boundary
		 *	condition when the graphic finishes exactly at the
		 *	end of the buffer.
		 */

		if (wCompWLeft == 0)
		{
			if (bLoadBlack)
			{
				bFirstValue ^= 0xFF;
				bDummyValue = bFirstValue;
			}

			/*
			 *	If there is no space left in the buffer, fail, ensuring that the
			 *	"width left" variable is set to zero to prevent further
			 *	painting.
			 */

			if (wCompHLeft == 0)
			{
				wCompWLeft = 0;
				lErrCode = IG_INVALID_DECOMPRESSION;
				continue;
			}
			else
			{
				/*
				 *	One less row available, move the "current row" indicator based
				 *	on the current paint direction.
				 */

				wCompHLeft--;
				wRowByte++;

				/*
				 *	This is for paranoia's sake, to make sure that we don't set up
				 *	the pointer outside of the graphics buffer and trash some other
				 *	area of memory.
				 */

				if (wRowByte >= IMAGE_ROW_BYTES)
				{
					lErrCode = IG_INVALID_DECOMPRESSION;
					continue;
				}
				else
				{
					pbStartBuf++;
					pbBuf = pbStartBuf;
					wCompWLeft = wCompWidth;
				}
			}
		}

		/*
		 *	If we get here, we've got space, so put out this byte.
		 *	If necessary, shift the contents of the byte.
		 *	Move the pointer to the same row in the next column,
		 *	and decrement the "space left" and "left to do" counts.
		 */

		if (bCompShift == 0)
		{
			*pbBuf = bDataByte;
		}
		else
		{
//?? will need to change this if the bytes are flipped
//??
			// Load the data by clearing the appropriate bits & loading the appropriate bits
			// uShiftAccess has ones for the bits that we will be loading, so in order to clear
			// the bits, we have to use the low byte of uShiftAccess on the upper buffer byte
			// and the high byte of uShiftAccess on the lower buffer byte.
			uDataAccess.wShift = bDataByte << (8 - bCompShift);

			*pbBuf		= (*pbBuf     & uShiftAccess.bShift.bHigh)  | uDataAccess.bShift.bLow;
			*(pbBuf+1)	= (*(pbBuf+1) & uShiftAccess.bShift.bLow) | uDataAccess.bShift.bHigh;	//lint !e661
		}

		pbBuf += IMAGE_ROW_BYTES;	//lint !e662
		wCompWLeft--;

		/*
		 *	We've disposed of one data byte.
		 */

		pbImage++;
		wBytes--;
	}

	return (lErrCode);
}


/****************************************************************************
// FUNCTION NAME: PaintNextLine
// DESCRIPTION: This function handles situations in which it is necessary to
//				move to the next line of the print buffer, whether due to
//				a wrap or an end-of-line code.
//
// AUTHOR: Sandra Peterson
//
// GLOBALS USED: None
//
// INPUTS: pwCompHLeft, pwCompWLeft, wCompWidth, wStartCol, pwRowByte, pbBuf
//
// OUTPUTS:	*plErrCode has been updated if there was an error.
//			Else, wCompHLeft, wCompWLeft, wRowByte & pbBuf updated
*****************************************************************************/

void PaintNextLine(unsigned long *plErrCode, unsigned short *pwCompHLeft, unsigned short *pwCompWLeft,
					const unsigned short wCompWidth, const unsigned short wStartCol, unsigned short *pwRowByte,
					unsigned char **pbBuf)
{
	/*
	 *	If there is no space left in the buffer, fail, ensuring that the
	 *	"width left" variable is set to zero to prevent further
	 *	painting.
	 */

	if (*pwCompHLeft == 0)
	{
		*pwCompWLeft = 0;
		*plErrCode = IG_INVALID_DECOMPRESSION;
		return;
	}

	/*
	 *	One less row available, move the "current row" indicator based
	 *	on the current paint direction.
	 */

	*pwCompHLeft -= 1;
	*pwRowByte += 1;

	/*
	 *	This is for paranoia's sake, to make sure that we don't set up
	 *	the pointer outside of the graphics buffer and trash some other
	 *	area of memory.
	 */

	if (*pwRowByte >= IMAGE_ROW_BYTES)
	{
		*plErrCode = IG_INVALID_DECOMPRESSION;
	}
	else
	{
		*pbBuf = &bImageBuffer[wStartCol][*pwRowByte];
		*pwCompWLeft = wCompWidth;
	}

	return;
}

void PaintNextLine2(unsigned long *plErrCode, unsigned short *pwCompHLeft, unsigned short *pwCompWLeft,
					const unsigned short wCompWidth, const unsigned short wStartCol, unsigned short *pwRowByte,
					unsigned char **pbBuf)
{
	/*
	 *	If there is no space left in the buffer, fail, ensuring that the
	 *	"width left" variable is set to zero to prevent further
	 *	painting.
	 */

	if (*pwCompHLeft == 0)
	{
		*pwCompWLeft = 0;
		*plErrCode = IG_INVALID_DECOMPRESSION;
		return;
	}

	/*
	 *	One less row available, move the "current row" indicator based
	 *	on the current paint direction.
	 */

	*pwCompHLeft -= 1;
	*pwRowByte += 1;

	/*
	 *	This is for paranoia's sake, to make sure that we don't set up
	 *	the pointer outside of the graphics buffer and trash some other
	 *	area of memory.
	 */

	if (*pwRowByte >= IMAGE_ROW_BYTES)
	{
		*plErrCode = IG_INVALID_DECOMPRESSION;
	}
	else
	{
		*pbBuf = &bImageBuffer2[wStartCol][*pwRowByte];
		*pwCompWLeft = wCompWidth;
	}

	return;
}


unsigned long IGU_ClearRegion2(const unsigned short wStartCol, const unsigned short wStartRow,
							  const unsigned short wMaxCols, const unsigned short wMaxRows)
{
	unsigned char	*pbBuf;
	unsigned long	lErrCode = IG_NO_ERROR;
	unsigned long	lBytes;
	unsigned short	wCol, wRow, wBytes;
	unsigned short	wEndRow;
	unsigned short	wStartRowByte, wEndRowByte;
	unsigned char	bStartBit, bEndBit;
	unsigned char	bStartMask, bEndMask;
	char			pLogBuf[50];


	wEndRow = wStartRow+wMaxRows;

	// make sure the region stays within the boundaries of the print area
	if ((wStartCol >= (IMAGE_COLS + START_OFFSET)) || (wStartRow >= MAX_ROWS) ||
		(wEndRow > (MAX_ROWS + ROW_ADJUST)))
		return (IG_INVALID_COORDINATES);

	if (wStartCol+wMaxCols > (IMAGE_COLS + START_OFFSET))
		return (IG_TOO_MANY_COLUMNS);

	// make sure the region has a size
	if ((wMaxCols == 0) || (wMaxRows == 0))
	{
		(void)sprintf(pLogBuf, "Region has width/height of zero");
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
		return (IG_INVALID_SIZE);
	}

	if ((wStartCol == 0) && (wStartRow == 0))
	{
		// For possible need in the future
		wLastStartCol = 0;
		wLastStartRow = 0;
		wLastEndCol = 0;
		wLastEndRow = 0;
	}

	// Clear the area
	//	If the area doesn't start on a row that is evenly divisible by 8, or
	//		the height isn't evenly divisible by 8, clear as shifted data
	//	Else, clear as whole bytes
	if (((wStartRow % 8) || (wMaxRows % 8)) && (wEndRow != (MAX_ROWS + ROW_ADJUST)))
	{
//?? will need to change this if the bytes are flipped back
//??
		wStartRowByte = wStartRow / 8;
		bStartBit = wStartRow % 8;
		wEndRowByte = (wEndRow - 1) / 8;
		bEndBit = (wEndRow - 1) % 8;
		if (wStartRowByte == wEndRowByte)
		{
			pbBuf = &bImageBuffer2[wStartCol][wStartRowByte];

			// shift x zeros into the upper byte, where x is
			// the number of rows to clear
//			uByteAccess.wShift = 0xff00 << (bEndBit - bStartBit + 1);

			// fill the lower byte as the bottom of the mask
//			uByteAccess.bShift.bLow = 0xff;

			// shift y ones into the upper byte, where y is
			// the number of rows to keep
			// note, bit 0 is for row 0, bit 7 is for row 7
//			uByteAccess.wShift <<= bStartBit;

			// shift x zeros into the upper byte, where x is
			// the number of rows to clear
			uByteAccess.wShift = 0xff00 << (bEndBit - bStartBit + 1);

			// fill the lower byte as the bottom of the mask
			uByteAccess.bShift.bLow = 0xff;

			// shift y ones into the upper byte, where y is
			// the number of rows to keep
			// note, bit 0 is for row 7, bit 7 is for row 0
			uByteAccess.wShift <<= (7 - bEndBit);

			// take the upper byte as the mask for the clearing
			// the bits for the row
			bStartMask = uByteAccess.bShift.bHigh;
			for (wCol = 0; wCol < wMaxCols; wCol++)
			{
				*pbBuf &= bStartMask;
				pbBuf += IMAGE_ROW_BYTES;
			}
		}
		else
		{
			wBytes = wEndRowByte - wStartRowByte + 1;
//			bStartMask = 0xff >> (8 - bStartBit);
//			bEndMask = 0xff << (bEndBit + 1);
//			for (wCol = wStartCol; wCol < wStartCol + wMaxCols; wCol++)
//			{
//				pbBuf = &bImageBuffer2[wCol][wStartRowByte];
//				*pbBuf++ &= bStartMask;
//				for (wRow = 2; wRow < wBytes; wRow++)
//				{
//					*pbBuf++ = 0;
//				}
//				*pbBuf &= bEndMask;
//			}

			bStartMask = 0xff << (8 - bStartBit);
			bEndMask = 0xff >> (bEndBit + 1);
			for (wCol = wStartCol; wCol < wStartCol + wMaxCols; wCol++)
			{
				pbBuf = &bImageBuffer2[wCol][wStartRowByte];
				*pbBuf++ &= bStartMask;
				for (wRow = 2; wRow < wBytes; wRow++)
				{
					*pbBuf++ = 0;
				}
				*pbBuf &= bEndMask;
			}
		}
	}
	else
	{
		// If the area is the full height, do it as a block clear
		// Else, clear it by columns
		if (wEndRow == MAX_ROWS)
			wBytes = (wMaxRows + (IMAGE_ROWS - MAX_ROWS + 7)) / 8;
		else
			wBytes = wMaxRows / 8;

		if ((wStartRow == 0) && (wBytes == IMAGE_ROW_BYTES))
		{
			lBytes = wBytes * wMaxCols;
			(void)memset(&bImageBuffer2[wStartCol][0], 0, lBytes);
		}
		else
		{
			wStartRowByte = wStartRow / 8;
			for (wCol = wStartCol; wCol < wStartCol + wMaxCols; wCol++)
				(void)memset(&bImageBuffer2[wCol][wStartRowByte], 0, wBytes);
		}
	}

	// For possible need in the future
	wLastStartCol = wStartCol;
	wLastStartRow = wStartRow;
	wLastEndCol = wStartCol + wMaxCols;
	wLastEndRow = wStartRow + wMaxRows;

	return (lErrCode);
}


//unsigned long IGU_LoadBackground(const unsigned char *pbImage, unsigned short wBytes,
unsigned long IGU_DecodeBin(const unsigned char *pbImage, unsigned short wBytes,
								 const unsigned short wStartCol, const unsigned short wStartRow,
								 const unsigned short wWidth, const unsigned short wHeight,
								 const unsigned short wMaxCols, const unsigned short wMaxRows)
{
	unsigned char	*pbBuf;
	unsigned long	lErrCode = IG_NO_ERROR;

	unsigned short	wCompHLeft;		// number of row bytes left to load for the graphic
	unsigned short	wCompWidth;		// number of columns in the graphic
	unsigned short	wCompWLeft;		// number of columns left to do on the current row
	unsigned short	wRowByte;		// starting/current row for loading the graphic

	unsigned char	bCmdByte;		// current command byte
	unsigned char	bCompCount = 0;	// number of compression bytes to load
	unsigned char	bCompShift;		// number of bits to shift the data to get it to start
									// on the proper row.
	unsigned char	bCompState;		// compression state
	unsigned char	bDataByte;		// current data byte

	char			pLogBuf[50];


	// make sure the background graphic has a valid size and that it
	// isn't bigger than the max size of the region
	if ((wWidth == 0) || (wHeight == 0) ||
		(wWidth > wMaxCols) || (wHeight > wMaxRows))
	{
		(void)sprintf(pLogBuf, "Graphic width = %02X", wWidth);
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
		(void)sprintf(pLogBuf, "Graphic max width = %02X", wMaxCols);
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
		(void)sprintf(pLogBuf, "Graphic height = %02X", wHeight);
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
		(void)sprintf(pLogBuf, "Graphic max height = %02X", wMaxRows);
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
		return (IG_INVALID_SIZE);
	}

	// make sure the data was provided
	if ((pbImage == NULL) || (wBytes == 0))
		return (IG_INVALID_IMAGE);

	// clear the region where the background is being loaded
	// only clear as wide as the actual graphic
	lErrCode = IGU_ClearRegion2(wStartCol, wStartRow, wWidth, wHeight);

	// if no problems clearing the region, load the background graphic
	if (lErrCode == IG_NO_ERROR)
	{
		// set up the initial data for loading the graphic
		wCompHLeft = (wHeight / 8) + 1;
		bCompShift = wStartRow % 8;
		wCompWidth = wWidth;
		wCompWLeft = wCompWidth;
		wRowByte = wStartRow / 8;

		pbBuf = &bImageBuffer2[wStartCol][wRowByte];
		bCompState = DE_NEED_CMD;

		/*
		 *	For each byte in the input so long as there isn't a problem
		 */

		while((wBytes != 0) && (lErrCode == IG_NO_ERROR))
		{
			switch (bCompState)
			{
			case DE_NEED_CMD :
				/*
				 *	If we're waiting for a command, set it up.
				 *	This will take care of adjusting bCompState appropriately.
				 *
				 *	If the command byte is a zero, this is an "end of line" command.
				 */

				bCmdByte = *pbImage;
				if (bCmdByte == 0)
				{
					/*
					 *	PaintNextLine handles going to a new line.
					 *	If PaintNextLinereturns successfully, we're great.
					 */

					PaintNextLine2(&lErrCode, &wCompHLeft, &wCompWLeft, wCompWidth, wStartCol, &wRowByte, &pbBuf);
					if (lErrCode != IG_NO_ERROR)
					{
						/*
						 *	OK, PaintNextLine failed.  This means that there's not enough
						 *	space in the buffer for another line of data.  There is a
						 *	special case to worry about, however.  If the last line of
						 *	graphics goes right up to the edge of the graphics buffer,
						 *	and if the very last command in the data is an "end of line"
						 *	with nothing following it, we can let it slide.  (Since no
						 *	data will follow, "no harm, no foul".  The "bytesLeft" parameter
						 *	tells us how many compressed graphics bytes remain so that
						 *	we can make this decision.)
						 *
						 *	To protect against anything else being loaded, we make sure
						 *	the "space left" variables are set to zero, which will force
						 *	us back through here (or through PaintNextLine) if another block
						 *	is processed.
						 */

						wCompWLeft = 0;
						wCompHLeft = 0;

						if (wBytes == 1)
							lErrCode = IG_NO_ERROR;

					}
				}
				else
				{
					/*
					 *	If we get here, we have a non-zero data byte.  The top bit
					 *	tells us what type of command it is.  (bit set: repeat command
					 *	bit clear: literal command)
					 */

					if (bCmdByte & DE_REP_BIT)
						bCompState = DE_REPEAT;
					else
						bCompState = DE_LITERAL;

						/*
						 *	The count is in the rest of the byte.  The count should be non-zero
						 *	because a "repeat" of zero bytes is not valid.  (A "literal" of
						 *	zero bytes is treated as an "end of line", and was handled above)
						 */

					bCompCount = bCmdByte & DE_CNT_MASK;
					if (bCompCount == 0)
						lErrCode = IG_INVALID_COMPRESSION;

					/*
					 *	The command byte itself gets skipped over when pbImage is
					 *	incremented at the end of the switch.
					 */
				}

				break;

			case DE_REPEAT:
				/*
				 *	We need to repeat this byte. The repeat count is in bCompCount.
				 */

				bDataByte = *pbImage;
				bDataByte = ByteMirror[bDataByte];

				/*
				 *	While we have data left to do.....
				 */

				while (bCompCount)
				{
					/*
					 *	If we've run out of room on this row, we need to wrap
					 *	to the next row.  PaintNextLine will fail if we run
					 *	off the edge of the graphics buffer, which is a bad
					 *	thing.  This test needs to be done before the byte
					 *	is painted, rather than after, to avoid a boundary
					 *	condition when the graphic finishes exactly at the
					 *	end of the buffer.
					 */

					if (wCompWLeft == 0)
					{
						PaintNextLine2(&lErrCode, &wCompHLeft, &wCompWLeft, wCompWidth, wStartCol, &wRowByte, &pbBuf);
						if (lErrCode != IG_NO_ERROR)
							break;
					}

					/*
					 *	If we get here, we've got space, so put out this byte.
					 *	If necessary, shift the contents of the byte.
					 *	Move the pointer to the same row in the next column,
					 *	and decrement the "space left" and "left to do" counts.
					 */

					if (bCompShift == 0)
					{
						*pbBuf = bDataByte;
					}
					else
					{
//?? will need to change this if the bytes are flipped back
//??
//						uByteAccess.wShift = bDataByte << bCompShift;

						uByteAccess.wShift = bDataByte << (8 - bCompShift);

						*pbBuf		|= uByteAccess.bShift.bHigh;
						*(pbBuf+1)	|= uByteAccess.bShift.bLow;
					}

					pbBuf += IMAGE_ROW_BYTES;
					wCompWLeft--;
					bCompCount--;
				}

				/*
				 *	At this point we've exhausted this command.  pbImage is still
				 *	pointing at the data byte, but it will be incremented
				 *	after the switch.  Now we need a command.
				 */

				bCompState = DE_NEED_CMD;
				break;

			case DE_LITERAL:
				/*
				 *	We need to copy this byte "as is" out.
				 */

				bDataByte = *pbImage;
				bDataByte = ByteMirror[bDataByte];

				/*
				 *	If we've run out of room on this row, we need to wrap
				 *	to the next row.  PaintNextLine will fail if we run
				 *	off the edge of the graphics buffer, which is a bad
				 *	thing.  This test needs to be done before the byte
				 *	is painted, rather than after, to avoid a boundary
				 *	condition when the graphic finishes exactly at the
				 *	end of the buffer.
				 */

				if (wCompWLeft == 0)
				{
					PaintNextLine2(&lErrCode, &wCompHLeft, &wCompWLeft, wCompWidth, wStartCol, &wRowByte, &pbBuf);
					if (lErrCode != IG_NO_ERROR)
						break;
				}

				/*
				 *	If we get here, we've got space, so put out this byte.
				 *	If necessary, shift the contents of the byte.
				 *	Move the pointer to the same row in the next column,
				 *	and decrement the "space left" and "left to do" counts.
				 */

				if (bCompShift == 0)
				{
					*pbBuf = bDataByte;
				}
				else
				{
//?? will need to change this if the bytes are flipped back
//??
//					uByteAccess.wShift = bDataByte << bCompShift;

					uByteAccess.wShift = bDataByte << (8 - bCompShift);

					*pbBuf		|= uByteAccess.bShift.bHigh;
					*(pbBuf+1)	|= uByteAccess.bShift.bLow;
				}

				pbBuf += IMAGE_ROW_BYTES;
				wCompWLeft--;
				bCompCount--;

				/*
				 *	If that was the last byte to be copied, we're back to
				 *	needing a command.  pbImage is still pointing at the byte
				 *	to be copied, but it will be incremented after the switch.
				 */

				if (bCompCount == 0)
					bCompState = DE_NEED_CMD;
				break;

			default:
				break;
			}

			/*
			 *	If we've successfully gotten through the switch, we've disposed
			 *	of one character (command or data byte).
			 */

			pbImage++;
			wBytes--;
		}
	}

	return (lErrCode);
}




