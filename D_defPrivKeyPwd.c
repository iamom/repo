/*************************************************************************
*   PROJECT:        Horizon CSD2-3
*	MODULE NAME:	D_defPrivKeyPwd.c
*
*	DESCRIPTION:	This file contains the obfuscated default private key password.
*
* ----------------------------------------------------------------------
*               Copyright (c) 2017 Pitney Bowes Inc.
*                    37 Executive drive
*                   Danbury, Connecticut  06810
* ----------------------------------------------------------------------
*
*************************************************************************/

const unsigned char defPrivKeyPwd3[] = {0xFF, 0xAD, 0x5C, 0x52, 0x6B, 0xCB, 0x6E, 0x0B};
const unsigned char defPrivKeyRand1[] = {0x52, 0x44, 0x3C, 0x17, 0x49, 0x6D, 0xAB, 0x06};
