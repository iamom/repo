/************************************************************************
   PROJECT:     Horizon CSD2-3
   MODULE NAME: UTILS.C
   
   DESCRIPTION:   General purpose utility functions for public use.
----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
----------------------------------------------------------------------
*************************************************************************/

#include <string.h> 
#include <stdio.h>  
#include <stdlib.h>
#include <math.h>
#include <ctype.h>

#include "thread_control.h"
#include "ossetup.h"
#include "lcdirect.h"
#include "bob.h"
#include "bobutils.h"
#include "clock.h"
#include "custdat.h"
#include "datdict.h"
#include "errcode.h"
#include "fwrapper.h"
#include "global.h"                                   
//#include "oirateservice.h"
#include "pbtarget.h"
#include "oit.h"
#include "nucleus.h"
#include "sys.h"
#include "unistring.h"
#include "utils.h"
#include "zlib.h"
#include "oifields.h"
#include "api_status.h"
#include "flashutil.h"
#include "ibuttoniolow.h"
#include "sysdata.h"
#include "api_temporary.h"
#include "sysdatadefines.h"
#include "pbptask.h"

#include "trm.h"

VOID UTF8ToUnicode(UINT16 *, UINT16, char *, size_t);


// ----------------------------------------------------------------------
//  External Function Prototypes:
// ----------------------------------------------------------------------


// ----------------------------------------------------------------------
//  External Variables:
// ----------------------------------------------------------------------

// From custdat.c:
extern ErrorLog     CMOSErrorLog;

extern TASK_DEFINITION Tasks [];

// From mcplib.c:
#if !defined(JANUS) || defined(G900)
extern const BaseModelToPartNumXref mmcModel2PartNum[];
#endif


extern BOOL     		fUploadInProgress;
// ----------------------------------------------------------------------
//  Local macro definitions:
// ----------------------------------------------------------------------

#define ERR_GET_ACCESS_CODE 1

#define BCD_BUFFER_SIZE    84  /* Should be large enough for any digit string  */


// ----------------------------------------------------------------------
//  Local function prototypes:
// ----------------------------------------------------------------------
//TODO - determine if this group static or not
UINT16 PBJisToUnicode( UINT16 PBJis );
BOOL fnGetLongAccessCode( char *pAccessCode );
BOOL fnGetDemoAccessCode ( char *pAccessCode );
void fnDouble2BinThree( UINT8 *pDest, double dblValue);


// ----------------------------------------------------------------------
//  Local Variables:
// ----------------------------------------------------------------------

// ----------------------------------------------------------------------
// This is a temp place to store strings to send to the System Log via
//  the function fnDumpStringToSystemLog.  This is to replace pLogBuf, 
//  which is a global defined in usbfunc.c, but used as an extern in  
//  other files.  It may also be used to replace a number of static and 
//  stack buffers (also named pLogBuf) that are used for the same purpose.
// The idea is to create one buffer that is used by many files, thereby 
//  saving memory.  The drawback is that the integetrity of this buffer
//  is not guaranteed for any calling function.  Even though the data has
//  a short life, interrupts may cause the data to be corrupted before it
//  is used.   For that reason, some tasks may wish to keep their local
//  buffers.  This was given a different name to try to distinguish the
//  global from all the locals.
// 
// JUST DON'T MAKE ANOTHER GLOBAL ONE!!
//
// BTW, the extern for this is in pbos.h, because the prototype for
//  fnDumpStringToSystemLog is there. 
//  
char pDumpLogBuff[ MAX_TEMPLOGBUFF_SIZE +1 ];


// ----------------------------------------------------------------------
//  Local Constants:
// ----------------------------------------------------------------------

static const char BcdChar[] = "0123456789ABCDEF";

/*********************************CRC Table**********************************/
static const unsigned char ByteCRCTbl[256] =
{
        0x00, 0x9B, 0xAD, 0x36, 0xC1, 0x5A, 0x6C, 0xF7,
        0x19, 0x82, 0xB4, 0x2F, 0xD8, 0x43, 0x75, 0xEE,
        0x32, 0xA9, 0x9F, 0x04, 0xF3, 0x68, 0x5E, 0xC5,
        0x2B, 0xB0, 0x86, 0x1D, 0xEA, 0x71, 0x47, 0xDC,
        0x64, 0xFF, 0xC9, 0x52, 0xA5, 0x3E, 0x08, 0x93,
        0x7D, 0xE6, 0xD0, 0x4B, 0xBC, 0x27, 0x11, 0x8A,
        0x56, 0xCD, 0xFB, 0x60, 0x97, 0x0C, 0x3A, 0xA1,
        0x4F, 0xD4, 0xE2, 0x79, 0x8E, 0x15, 0x23, 0xB8,
        0xC8, 0x53, 0x65, 0xFE, 0x09, 0x92, 0xA4, 0x3F,
        0xD1, 0x4A, 0x7C, 0xE7, 0x10, 0x8B, 0xBD, 0x26,
        0xFA, 0x61, 0x57, 0xCC, 0x3B, 0xA0, 0x96, 0x0D,
        0xE3, 0x78, 0x4E, 0xD5, 0x22, 0xB9, 0x8F, 0x14,
        0xAC, 0x37, 0x01, 0x9A, 0x6D, 0xF6, 0xC0, 0x5B,
        0xB5, 0x2E, 0x18, 0x83, 0x74, 0xEF, 0xD9, 0x42,
        0x9E, 0x05, 0x33, 0xA8, 0x5F, 0xC4, 0xF2, 0x69,
        0x87, 0x1C, 0x2A, 0xB1, 0x46, 0xDD, 0xEB, 0x70,
        0x0B, 0x90, 0xA6, 0x3D, 0xCA, 0x51, 0x67, 0xFC,
        0x12, 0x89, 0xBF, 0x24, 0xD3, 0x48, 0x7E, 0xE5,
        0x39, 0xA2, 0x94, 0x0F, 0xF8, 0x63, 0x55, 0xCE,
        0x20, 0xBB, 0x8D, 0x16, 0xE1, 0x7A, 0x4C, 0xD7,
        0x6F, 0xF4, 0xC2, 0x59, 0xAE, 0x35, 0x03, 0x98,
        0x76, 0xED, 0xDB, 0x40, 0xB7, 0x2C, 0x1A, 0x81,
        0x5D, 0xC6, 0xF0, 0x6B, 0x9C, 0x07, 0x31, 0xAA,
        0x44, 0xDF, 0xE9, 0x72, 0x85, 0x1E, 0x28, 0xB3,
        0xC3, 0x58, 0x6E, 0xF5, 0x02, 0x99, 0xAF, 0x34,
        0xDA, 0x41, 0x77, 0xEC, 0x1B, 0x80, 0xB6, 0x2D,
        0xF1, 0x6A, 0x5C, 0xC7, 0x30, 0xAB, 0x9D, 0x06,
        0xE8, 0x73, 0x45, 0xDE, 0x29, 0xB2, 0x84, 0x1F,
        0xA7, 0x3C, 0x0A, 0x91, 0x66, 0xFD, 0xCB, 0x50,
        0xBE, 0x25, 0x13, 0x88, 0x7F, 0xE4, 0xD2, 0x49,
        0x95, 0x0E, 0x38, 0xA3, 0x54, 0xCF, 0xF9, 0x62,
        0x8C, 0x17, 0x21, 0xBA, 0x4D, 0xD6, 0xE0, 0x7B
};

static char 
pLastAccessCode[10] = { '1', '2', '3', '4', '5', '6', 0 };


/*********************************CRC Table**********************************/


/* **********************************************************************
// FUNCTION NAME: fnDisplayErrorDirect
// DESCRIPTION:  Displays an error message on the LCD.  Processing is done 
//                  on the thread of the calling task- not the LCD task thread.
//                  Error text is displayed on all lines except the bottom one,
//                  which is reserved for the error & task IDs.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pString - Pointer to the error message ASCII text string.  This 
//                      function will only display as many characters as 
//                      will fit on the LCD in the standard font, not counting
//                      the bottom line.  
//
//          lErrorCode - Signed error code number.  The error code is displayed
//                          on the bottom line of the LCD.
//
//          sTask - Task ID number.  This is displayed on the bottom line
//                  of the LCD after the error code.  There is no guarantee
//                  that both numbers will fit if they are large or the
//                  LCD width is inadequate.
// **********************************************************************/
void fnDisplayErrorDirect( const char *pString, SINT32 lErrorCode, SINT16 sTask )
{
// for Janus we want to log the error and try to continue
    {
//        short sErrorCode;
//        unsigned char   bHighByte;
//        unsigned char   bLowByte;
		unsigned int	uiSize;
		char			cTempString[40];


		// create the second part of the syslog string
		(void)sprintf(cTempString, " Code=0x%04X, Task=0x%02X", (UINT)lErrorCode, sTask);
		uiSize = MAX_TEMPLOGBUFF_SIZE - strlen(cTempString) - 1;		// be sure to leave space for the null terminator

		// make sure the passed in string isn't too big
		if (strlen(pString) > uiSize)
		{
			(void)strncpy(pDumpLogBuff, pString, uiSize);
			pDumpLogBuff[uiSize] = 0;
			(void)strcat(pDumpLogBuff, cTempString);
		}
		else
		{
			(void)sprintf(pDumpLogBuff, "%s%s", pString, cTempString);
		}

        fnDumpStringToSystemLog( pDumpLogBuff );

//        sErrorCode = (short) lErrorCode;    /* convert the passed in error code from a long
//                                                to a short.  this may be a lossy conversion if
//                                                an error code is too big. */

        /* now take the 2 bytes of the short, and dump them into the log along with a special
            type code for fnDisplayErrorDirect errors.  ordinarily we would use a class and code
            byte, but since display error direct existed long before this error format, we have
            to attempt the best possible conversion. */
//        bHighByte = (unsigned char) (sErrorCode >> 8);
//        bLowByte = (unsigned char) (sErrorCode & 0xFF);
//        fnLogError(ERR_TYPE_DISPLAY_DIRECT, bHighByte, bLowByte);

        // to provide more info and to not confuse things w/ the other error classes,
        // load the LSB of the error code as the error type,
        // load the error type as the error class so the Display Direct errors will become their own error class, and
        // load the task ID as the error code.
		fnLogError((unsigned char)lErrorCode, ERR_TYPE_DISPLAY_DIRECT, (unsigned char)sTask);
    }

}

/* **********************************************************************
// FUNCTION NAME: baseEventAllowed
// DESCRIPTION: Determines whether or not the error should be sent to the 
//					 tablet as a base event.
//
// INPUTS:  bErrorClass, bErrorCode- the usual class/code means of identifying 
//												 an error as with fnReportMeterError
// OUTPUTS: BOOL - TRUE = send baseEvent to tablet
//						 FALSE = do not send baseEvent to tablet
// *************************************************************************/
static BOOL baseEventAllowed( UINT8 errorClass, UINT8 errorCode )
{
	// don't send baseEvent for error 1828 - tablet will get Data Centre Communication Failure response
	if ((errorClass == ERROR_CLASS_PBP) && (errorCode == (PBPS_DC_NO_METER - XML_USER_ERROR_BASE)))
		return FALSE;
	
	return TRUE;
}
	 
/* **********************************************************************
// FUNCTION NAME: fnLogError
// DESCRIPTION: Logs an error in the circular CMOS error log.
//
// INPUTS:  bErrorType- one of the error types defined in errcode.h (e.g. regular, fatal, etc.)
//          bErrorClass, bErrorCode- the usual class/code means of identifying an error
//                                      as with fnReportMeterError
//
//
// AUTHOR: Joe Mozdzer
//
// OUTPUTS: none
// *************************************************************************/
void fnLogError( UINT8 bErrorType, UINT8 bErrorClass, UINT8 bErrorCode )
{
    unsigned char   bIndex;
    unsigned char   bPreviousIndex;
    DATETIME        rDateTimeStamp;


    /* get the system date/time so that this can be recorded along with the error */
    GetSysDateTime(&rDateTimeStamp, ADDOFFSETS, GREGORIAN);

    char *szTaskName = (OSGetCurrentTask() == 0xFF) ? "?HISR?" :
    		((Tasks[(INT)OSGetCurrentTask()].pControlBlock != NULL) ? Tasks[OSGetCurrentTask()].szTaskName : "?") ;

    (void)dbgTrace(DBG_LVL_ERROR, "****SYSTEM ERROR**** %02d/%02d/%02d %02d:%02d:%02d****Task: %02X[%s] ErrCode: %02X%02X**\r\n",
             rDateTimeStamp.bMonth , rDateTimeStamp.bDay , rDateTimeStamp.bYear,
             rDateTimeStamp.bHour, rDateTimeStamp.bMinutes, rDateTimeStamp.bSeconds,
			 (INT)OSGetCurrentTask(), szTaskName,
             bErrorClass, bErrorCode );

	//TODO: Remove me after 253D Error debugging
	if((bErrorClass == ERROR_CLASS_JBOB_TASK) && (bErrorCode == JSCM_IPSD_HANDSHAKE_FAILURE))
	{
		extern UINT8 DebugIBtnDriver[];
		extern UINT8 DebugIBtnDriverCtr;
		extern IBUTTON_DEVICE iButtonDevice;

        uint32_t i;
        uint32_t j;
		char dbgBuf[128];
		volatile unsigned short *regFIFOLvl = (volatile unsigned short *)(0x44E09064);
		volatile unsigned short *txFIFOLvl = (volatile unsigned short *)(0x44E09068);
		volatile unsigned short *mdr = (volatile unsigned short *)(0x44E09020);
		volatile unsigned short *pc_u0_rx = (volatile unsigned short *)(0x44E10970);
		volatile unsigned short *pc_u0_tx = (volatile unsigned short *)(0x44E10974);
		volatile unsigned short *pc_usb1_drvbus = (volatile unsigned short *)(0x44E10A34);
		volatile unsigned short *pc_mii1_col = (volatile unsigned short *)(0x44E10908);
		volatile unsigned short *pc_mii1_rxdv = (volatile unsigned short *)(0x44E10918);
		volatile UINT32         *gpio_oe = (volatile unsigned short *)(0x481Ae134);

		dbgTrace(DBG_LVL_ERROR, "253D Info RxFifoLvl: 0x%x, TxFifoLvl: 0x%x, mdr: 0x%x\r\n",
			     *regFIFOLvl, *txFIFOLvl, *mdr);
		dbgTrace(DBG_LVL_ERROR, "253D Info pc_u0_rx: 0x%x, pc_u0_tx: 0x%x, pc_usb1_drvbus: 0x%x\r\n",
			     *pc_u0_rx, *pc_u0_tx, *pc_usb1_drvbus);
		dbgTrace(DBG_LVL_ERROR, "253D Info pc_mii1_col: 0x%x, pc_mii1_rxdv: 0x%x, gpio_oe: 0x%x\r\n",
			     *pc_mii1_col, *pc_mii1_rxdv, *gpio_oe);
		dbgTrace(DBG_LVL_ERROR, "253D Info Last Op:%02d, last Error:%02d, dbgCtr:0x%02X\r\n",
			     iButtonDevice.Status.lastSuccessfulOp, iButtonDevice.Status.lastError, DebugIBtnDriverCtr);

	    for (j=0; j<16; j++)
        {
			for (i=0; i<16; i++)
			{
				sprintf(&dbgBuf[i * 3], "%02X ", DebugIBtnDriver[i + (j*16)]);
			}
			dbgBuf[i*3] = '\0'; // null terminate it
			dbgTrace(DBG_LVL_ERROR, "253D Info %s\r\n", dbgBuf);
		}
	}

	if ( baseEventAllowed(bErrorClass, bErrorCode) )
	{
		char pLogBuf[80];
		
		cJSON *parameters = cJSON_CreateObject();
		
		(void)sprintf(pLogBuf, "%02X%02X", bErrorClass, bErrorCode);

		cJSON_AddStringToObject(parameters, "Error", pLogBuf);
		SendBaseEventToTablet(BASE_EVENT_ERROR, parameters);

		//Check for upload in progress
		if (fnIsDisabledStatusSet(DCOND_UPLOAD_IN_PROGRESS) == TRUE)
		{
			fUploadInProgress = FALSE;
			sendUploadProgress(UPLOAD_EVENT_TRX_UPLOAD_FAILED, 0, 0);
			trmSetUploadState(TRM_NONE);
			SendBaseEventToTablet(BASE_EVENT_TX_UPLOAD_FAILED, NULL);
		}
	}
	
    bIndex = CMOSErrorLog.LogIndex;
    if (bIndex == 0)
        bPreviousIndex = ERR_LOG_SZ - 1;
    else
        bPreviousIndex = bIndex - 1;

    /* check if the previous error entry is the same as this one.  we will avoid logging the same
        error consecutive times to prevent other log information from being
        wiped out in the event of some sort of recurring error. */
    if ((CMOSErrorLog.ErrorTable[bPreviousIndex].bErrorClass == bErrorClass) &&
        (CMOSErrorLog.ErrorTable[bPreviousIndex].bErrorCode == bErrorCode) &&
        (CMOSErrorLog.ErrorTable[bPreviousIndex].bErrorType == bErrorType))
    {
        /* just bump up the repeat counter if this is the same error as last time.  avoid
            flipping the number from 255 to 0. */
        if ((CMOSErrorLog.ErrorTable[bPreviousIndex].bRepeatCount) < 255)
            CMOSErrorLog.ErrorTable[bPreviousIndex].bRepeatCount++;
    }
    else
    {
        /* add the entry to the log */
        (void)memcpy(&CMOSErrorLog.ErrorTable[bIndex].rDateTime, &rDateTimeStamp, sizeof(DATETIME));
        CMOSErrorLog.ErrorTable[bIndex].bErrorType = bErrorType;
        CMOSErrorLog.ErrorTable[bIndex].bErrorClass = bErrorClass;
        CMOSErrorLog.ErrorTable[bIndex].bErrorCode = bErrorCode;
        CMOSErrorLog.ErrorTable[bIndex].bRepeatCount = 0;

        /* bump up the index to the next available slot, rolling over if necessary */
        CMOSErrorLog.LogIndex++;
        if (CMOSErrorLog.LogIndex >= ERR_LOG_SZ)
            CMOSErrorLog.LogIndex = 0;
    }

    return;
}

/*
 * Name
 *   fnResetSystemErrorLog
 *
 * Description
 *   This function clears out the System Error Log and resets the 'next'
 *   pointer to the beginning of the log.
 *
 * Inputs
 *   None.
 *
 * Returns
 *   Nothing.
 *
 * Caveats
 *   This function destructively resets the CMOS System Error Log.  In addition,
 *     the 'next' pointer is reset to the first element.
 *
 * Revision History
 *   04/12/2002 Joseph P. Tokarski - Initial Revision.
 */
void fnResetSystemErrorLog(void)
{
    int i;
    SystemErrorLogEntry sysErrorLogData;

    (void)memset(&sysErrorLogData, 0, sizeof(SystemErrorLogEntry));

    for (i=0;i<SYS_ERR_LOG_SZ;i++)
    {
        /* write the data to the log */
        (void)fnfSetCMOSPackedByteParam((i + CMOS_PB_SYS_FIRST_LOG_ENTRY), 
                                        (unsigned char *)&sysErrorLogData, 
                                        sizeof(SystemErrorLogEntry));
    }

    (void)fnfSetCMOSByteParam(CMOS_BP_SYS_ERR_LOG_INDEX, 0); // index at which to store the next entry      
}

/*
 * Name
 *   fnGetSystemLogAreaID
 *
 * Description
 *   This function retrieves the 'next' location where a System Error can
 *   be logged.  It is assumed that the retrieval of the next System Error Log
 *   location is immediately succeeded by the logging of an System Error.
 *
 *   In addition to retrieving the 'next' (i.e., current) System Error Log
 *   location, this function handles incrementing the Location variable,
 *   ensuring correct bounds checking. 
 *
 * Inputs
 *   None.
 *
 * Returns
 *   an unsigned integer representing the 'next' log location in which to
 *     store an System Error Log report
 *
 * Caveats
 *   This function performs the necessary 'wrap around' bounds checking for
 *     the Log Index.
 *
 *   This function also ensures that the 'EXTENDED_CMOS_ID_MASK' bit is set
 *     in the return value.
 *
 * Revision History
 *   07/16/2001 Joseph P. Tokarski (CSS, LLC) - Initial Revision.
 */
unsigned short fnGetSystemLogAreaID(void)
{
    unsigned short uwSysLogIndex;
    unsigned char  ucNewLogIndex;

    /* retrieve the value of the System Log Index from CMOS */
    uwSysLogIndex = (unsigned short)fnfGetCMOSByteParam(CMOS_BP_SYS_ERR_LOG_INDEX);

    /* compute the new Log Index to write back */
    ucNewLogIndex = (unsigned char)(uwSysLogIndex + 1) % SYS_ERR_LOG_SZ;

    (void)fnfSetCMOSByteParam(CMOS_BP_SYS_ERR_LOG_INDEX, ucNewLogIndex);

    /* adjust the value to be relative to the beginning of the Extended
     * Packed Byte Parameters.
     */
    uwSysLogIndex += CMOS_PB_SYS_FIRST_LOG_ENTRY;

    return(uwSysLogIndex);
}

// ********************************************************************************
// NAME:        fnUtilLogEarlyError  
// PURPOSE:     
//  If a call is made to fnLogSystemError before the CMOS has been preserved,
//  then this function is called to store the arguments in a ram array.  
//  After CMOSpreservation is done, the function fnUtilReLogEarlyErrors is 
//  called to call fnLogSystemError with the stored arguments.
//  This array is cleared out on powerup, so the early errors will only be
//  available in the CMOS log if we got past the preservation (or if we did
//  not need to do preserve.)
// INPUTS:
//      The same 5 arguments that are passed to fnLogSystemError.
// RETURNS: 
//      Nothing.
// OUTPUTS:
//      The 5 arguments are stored in the next empty entry in
//      pUtilEarlySysErrorLog, unless it is full already.
// HISTORY:
//  2008.12.05 Clarisa Bellamy - Copied from Mega.
// NOTES:
//  1)  At this time, the log can contain 10 early errors.
//-----------------------------------------------------------------------------

#define MAX_EARLY_ERR   10

// These should be static, but we will want to be able to see this with an emulator.
UINT8 pUtilEarlySysErrorLog[ MAX_EARLY_ERR ][ 5 ];
UINT8 bUtilEarlySysErrorCount;

static void fnUtilLogEarlyError( UINT8 bArg1,
                                 UINT8 bArg2,
                                 UINT8 bArg3,
                                 UINT8 bArg4,
                                 UINT8 bArg5 )
{
    UINT8   *pThisError;

    if( bUtilEarlySysErrorCount < MAX_EARLY_ERR )
    {
        pThisError = pUtilEarlySysErrorLog[ bUtilEarlySysErrorCount ];
        pThisError[ 0 ] = bArg1;
        pThisError[ 1 ] = bArg2;
        pThisError[ 2 ] = bArg3;
        pThisError[ 3 ] = bArg4;
        pThisError[ 4 ] = bArg5;
        bUtilEarlySysErrorCount++;
    }
    return;
}

// *************************************************************************
// NAME:             fnUtilReLogEarlyErrors
// DESCRIPTION:
//   This function writes any System Errors that were recorded in RAM prior to 
//    CMOSPreservation into the CMOS area.  It should be called after the CMOS
//    Preservation is done.
// INPUTS:
//   None  - Uses the data stored in pUtilEarlySysErrorLog.
// RETURNS:
//   Nothing.
// HISTORY:
//  2008.12.05 Clarisa Bellamy - Copied from Mega.
// NOTES:
// ---------------------------------------------------------------------------
void fnUtilReLogEarlyErrors( void )
{
    UINT8  *pThisError;
    UINT8   i = 0;

    for( i = 0; i < bUtilEarlySysErrorCount; i++ )
    {
        pThisError = pUtilEarlySysErrorLog[i];
        fnLogSystemError( pThisError[0], pThisError[1], pThisError[2],
                          pThisError[3], pThisError[4] );
    }
    // In case this gets called a second time, it won't do anything if this is 0.
    bUtilEarlySysErrorCount = 0;
    return;
}

// *************************************************************************
// NAME:             fnLogSystemError
// DESCRIPTION:
//   This function writes a System Error to the non-volatile (i.e., CMOS) Log.
// INPUTS:
//   sysErrClass - the System Error class, e.g., ERROR_CLASS_PSD1, ERROR_CLASS_MCS, etc.
//   errData1    - System Error Class-specific data, byte 1
//   errData2    - System Error Class-specific data, byte 2
//   errData3    - System Error Class-specific data, byte 3
//   errData4    - System Error Class-specific data, byte 4
// RETURNS:
//   Nothing.
// NOTES:
// 1)  This function writes an error log record into the System Error Log section of
//     the CMOS.
// HISTORY:
//   07/16/2001 Joseph P. Tokarski (CSS, LLC) - Initial Revision.
// ---------------------------------------------------------------------------
void fnLogSystemError( UINT8 sysErrClass,
                       UINT8 errData1,
                       UINT8 errData2,
                       UINT8 errData3,
                       UINT8 errData4 )
{
    SystemErrorLogEntry sysErrorLogData;
    UINT16 uwLogIndex;


    // Make sure we are not trying to write to CMOS before it is "preserved."
    if( fnCMOSOkToWriteIt() == FALSE )
    {
        // If CMOS is not ready, store the parameters in RAM to record later.
        fnUtilLogEarlyError( sysErrClass, errData1, errData2, errData3, errData4 );
        return;
    }

    /* get the 'next' log area to write into */
    uwLogIndex = fnGetSystemLogAreaID();

    /* get the system date/time */
    (void)GetSysDateTime(&(sysErrorLogData.rDateTime), ADDOFFSETS, GREGORIAN);

    /* load the error data into the MCS Error Log Structure */
    sysErrorLogData.bSysErrorClass = sysErrClass;
    sysErrorLogData.bErrorInfo1    = errData1;
    sysErrorLogData.bErrorInfo2    = errData2;
    sysErrorLogData.bErrorInfo3    = errData3;
    sysErrorLogData.bErrorInfo4    = errData4;

    /* write the data to the log */
    (void)fnfSetCMOSPackedByteParam(uwLogIndex, 
                                    (unsigned char *)&sysErrorLogData, 
                                    sizeof(SystemErrorLogEntry));   

}


/*
 * Name
 *   fnGetDownloadListLoc
 *
 * Description
 *   This function specifies the location where a Download File request can
 *   be inserted.
 *
 *   In addition to retrieving the Download File Request location, this
 *   function handles incrementing the Location variable, ensuring correct
 *   bounds checking. 
 *
 * Inputs
 *   None.
 *
 * Returns
 *   an integer representing the location at which to store a Download File
 *     request, if successful (currently 0 - (MAX_DOWNLOAD_FILES - 1))
 *
 *   -1 if the download file request list is full
 *
 * Caveats
 *
 *   This function ensures that the 'EXTENDED_CMOS_ID_MASK' bit is set
 *     in the return value.
 *
 * Revision History
 *   04/24/2002 Joseph P. Tokarski - Initial Revision.
 */
static short fnGetDownloadListLoc(void)
{
    unsigned short uwDownloadFileLoc;
    unsigned short uwNewDownloadFileLoc;

    /* retrieve the value of the Download File Index from CMOS */
    uwDownloadFileLoc = (unsigned short)fnfGetCMOSWordParam(CMOS_WP_DOWNLOAD_FILE_COUNT);

    if (uwDownloadFileLoc == CMOS_MAX_DOWNLOAD_FILES)
    {
        // the list is full; return the failure condition
        return(-1);
    }

    /* compute the new Log Index to write back */
    uwNewDownloadFileLoc = (unsigned char)(uwDownloadFileLoc + 1);

    (void)fnfSetCMOSWordParam(CMOS_WP_DOWNLOAD_FILE_COUNT, uwNewDownloadFileLoc);

    /* adjust the value to be relative to the beginning of the Extended
     * Packed Byte Parameters.
     */
    uwDownloadFileLoc += CMOS_PB_DOWNLOAD_FIRST_FILE_ENTRY;

    return(uwDownloadFileLoc);
}

/*
 * Name
 *   isFileAlreadyRequested
 *
 * Description
 *   This function determines if the requested file already exists in the
 *   requested files list.
 *
 * Inputs
 *   downloadFilename - name of the requested file
 *   downloadFileType - type of requested file
 *
 * Returns
 *   TRUE  - if the file already exists in the list
 *   FALSE - if the file doesn't already exist in the list
 *
 * Caveats
 *   This function 'cheats' and directly accesses the CMOS data structure.
 *
 * Revision History
 *   05/01/2002 Joseph P. Tokarski - Initial Revision.
 */
static BOOL isFileAlreadyRequested(char *downloadFilename,
                                   char downloadFileType)
{
    extern FILES_TO_DOWNLOAD CMOSFilesToDownload;

    int i;

    // determine if there are entries in the list; if not return FALSE
    if (CMOSFilesToDownload.DownloadFileCount == 0)
    {
        // nothing exists in the list...
        return(FALSE);
    }

    // loop through the entries looking for a match
    for (i = 0; i < CMOSFilesToDownload.DownloadFileCount; i++)
    {
        if (CMOSFilesToDownload.DownloadFileEntry[i].DownloadFileType == downloadFileType &&
            strcmp(CMOSFilesToDownload.DownloadFileEntry[i].DownloadFileName, downloadFilename) == 0)
        {
            // we have an exact match; return TRUE
            return(TRUE);
        }
    }

    // if we got here, then we exhausted the list and did not find a match
    return(FALSE);
}


/*
 * Name
 *   fnRequestFileForDownload
 *
 * Description
 *   This function makes an entry in the CMOS Download Filename list which
 *   contains the set of files which need to be downloaded for 'normal' UIC
 *   operation.  Examples of such files are the PHC Snippets, Midjet Motion
 *   Profiles, and the Printer Accounting Report Templates.
 *
 *   This function takes care of updating the CMOS variables in a safe manner
 *   to prevent the caller from needing to know the intimate details.
 *
 * Inputs
 *   downloadFilename - filename to be added to the list
 *   downloadFileType - type of file to be downloaded, e.g., SNIPPET, PROFILE, etc
 *
 * Returns
 *   TRUE  - if the file was successfully added to the list
 *   FALSE - if the file could not be added to the download file list (i.e., list full)
 *
 * Caveats
 *   This function alters the contents of CMOS.
 *
 * Revision History
 *   04/24/2002 Joseph P. Tokarski - Initial Revision.
 */
BOOL fnRequestFileForDownload( char *downloadFilename,
                               char  downloadFileType )
{
    unsigned short uwFileIndex;
    DOWNLOAD_FILE_ENTRY downloadFileData;

    // determine if the file already exists in the list
    if (isFileAlreadyRequested(downloadFilename, downloadFileType) == TRUE)
    {
        return(TRUE);
    }

    /* get the 'next' log area to write into */
    if ((uwFileIndex = fnGetDownloadListLoc()) == (unsigned short)-1)
    {
        // there's no room left at the inn....
        //
        return(FALSE);
    }

    /* load the Download File data into the DOWNLOAD_FILE Structure */
    downloadFileData.DownloadFileType = downloadFileType;
    (void)strcpy(&downloadFileData.DownloadFileName[0], downloadFilename);

    /* save the request */
    (void)fnfSetCMOSPackedByteParam(uwFileIndex, 
                                    (unsigned char *)&downloadFileData, 
                                    sizeof(downloadFileData));  

    return(TRUE);
}


/*
 * Name
 *   fnClearAllFilesToDownload
 *
 * Description
 *   This function clears all pending files to be downloaded via the infrastructure
 *   from the CMOS variables.
 *
 * Inputs
 *   None.
 *
 * Returns
 *   Nothing.
 *
 * Caveats
 *   This function clears any pending files to be downloaded from the CMOS
 *     'FilesToDownload' area.
 *
 * Revision History
 *   06/03/2002 Joseph P. Tokarski - Initial Revision.
 */
void fnClearAllFilesToDownload(void)
{
    unsigned short uwFileIndex;
    DOWNLOAD_FILE_ENTRY downloadFileData;
    unsigned short fileID;  

    // first, set the number of files to download to '0'
    uwFileIndex = 0;

    (void)fnfSetCMOSWordParam(CMOS_WP_DOWNLOAD_FILE_COUNT, uwFileIndex);

    // pre-load the data structure with 'null' data
    downloadFileData.DownloadFileType = 0;
    (void)memset(&downloadFileData.DownloadFileName[0], 0, MAX_DOWNLOAD_FILENAME_LEN + 1);

    // loop through the set of Download File Entries, and reset each one
    for (fileID = 0; fileID < CMOS_MAX_DOWNLOAD_FILES; fileID++)
    {
        // adjust the offset to the correct location within the download
        // file entries
        uwFileIndex = fileID + CMOS_PB_DOWNLOAD_FIRST_FILE_ENTRY;

        /* save the empty record */
        (void)fnfSetCMOSPackedByteParam(uwFileIndex, 
                                        (unsigned char *)&downloadFileData, 
                                        sizeof(downloadFileData));  
    }
}

#if !defined(JANUS) || defined(G900)
/*
 * Name
 *   fnLocatePartNumForModel
 *
 * Description
 *   This function locates a Model Number/Part Number cross-reference record
 *   for a given Base Model Number.  This is to allow Base Firmware Upgrades
 *   to occur by determining the download firmware part number for a specified
 *   base Model Number (e.g., "DM400").
 *
 * Inputs
 *   modelNum - a NULL-terminated Model Number to look up the corresponding
 *                part number record for
 *
 * Returns
 *   a pointer to a BaseModelToPartNumXref record for the specified Base Model,
 *     if successful
 *
 *   NULL if no corresponding Base Model Number record was located.
 *
 * Caveats
 *   None.
 *
 * Revision History
 *   06/12/2002 Joseph P. Tokarski - Initial Revision.
 */
BaseModelToPartNumXref *fnLocatePartNumForModel(unsigned char *modelNum)
{
    BaseModelToPartNumXref *curEntry;

    curEntry = (BaseModelToPartNumXref *)&mmcModel2PartNum[0];

    while (strcmp((const char *)curEntry->baseModelNum, "") != 0)
    {
        // check the model number
        if (strcmp((const char *)curEntry->baseModelNum, (const char *)modelNum) == 0)
        {
            // matched the model number; return the pointer
            return(curEntry);
        }

        // advance the pointer to the next entry
        curEntry++;
    }

    return((BaseModelToPartNumXref *)NULL);
}
#endif

/*
 * Name
 *   fnActivePartNumRecordExists
 *
 * Description
 *   This function attempts to locate a record in the CMOSActiveVersionData
 *   data structure corresponding to a provided Part Number string.
 *
 * Inputs
 *   partNum - null-terminated ASCII string containing the Part Number to find
 *
 * Returns
 *   the record 'index' if the record already exists (0 - (CMOS_MAX_ACTIVE_VERSION_ENTRIES - 1))
 *   -1 if the record does not exist in the archive
 *
 * Caveats
 *   None.
 *
 * Revision History
 *   06/13/2002 Joseph P. Tokarski - Initial Revision.
 */
static short fnActivePartNumRecordExists(unsigned char *partNum)
{
    extern CMOSACTIVEVERSIONDATA CMOSActiveVersionData; 

    unsigned long i;
    unsigned long recCount;
    ActiveVersionEntry *curEntry;
    
    // make sure there are records to process
    if ((recCount = CMOSActiveVersionData.activeVersionEntryCount) == 0)
    {
        // there are NO records to check
        return(-1);
    } 

    // set the initial record
    curEntry = &CMOSActiveVersionData.activeVersionData[0];

    // loop through the valid set of records, looking for a part number match
    for (i = 0; i < recCount; i++, curEntry++)
    {
        if (strcmp((const char *)partNum, (const char *)curEntry->activeBasePartNumber) == 0)
        {
            // record already exists
            return(i);
        }
    }

    // no match found
    return(-1);
}

/*
 * Name
 *   fnUpdateBaseDownloadEntry
 *
 * Description
 *   This function updates the Active Base Download Entry corresponding
 *   to the provided 'recordID', using the specified MMC Version, MMC SMR,
 *   and FFS Filename values.
 *
 * Inputs
 *   recordID    - ID of the Active Base Download record to be updated
 *   mmcVersion  - 'new' value of the MMC Version String
 *   mmcSMR      - 'new' value of the MMC SMR value needed to support the upgrade
 *   ffsFilename - 'new' value of the FFS Filename contianing the base firmware
 *
 * Returns
 *   Nothing.
 *
 * Caveats
 *   This function updates the CMOS 'Active Base Data' structure record with the
 *     specified Version, SMR, and FFS Filename values.
 *
 * Revision History
 *   06/13/2002 Joseph P. Tokarski - Initial Revision.
 */
static void fnUpdateBaseDownloadEntry(short recordID, 
                                      unsigned char *mmcVersion,
                                      unsigned char *mmcSMR,
                                      unsigned char *ffsFilename)
{
    extern CMOSACTIVEVERSIONDATA CMOSActiveVersionData; 

    ActiveVersionEntry *activeEntry;

    // set the record based upon the recordID
    activeEntry = &CMOSActiveVersionData.activeVersionData[recordID];

    // copy in the updated data
    (void)strcpy((char *)&activeEntry->activeBaseMMCMinVersion[0], (const char *)mmcVersion);
    (void)strcpy((char *)&activeEntry->activeBaseMinSMR[0], (const char *)mmcSMR);
    (void)strcpy((char *)&activeEntry->activeBaseFirmwareFilename[0], (const char *)ffsFilename);

    // all done
}

/*
 * Name
 *   fnAddBaseDownloadEntry
 *
 * Description
 *   This function adds an entry into the Active Base Download structure using
 *   the provided Firmware Part Number (of the base software), the Version
 *   associated with the Firmware, the SMR of the hardware supporting the
 *   firmware, and the FFS Filename of the file containing the firmware.
 *
 * Inputs
 *   firmwarePartNum - part number of the firmware record
 *   mmcVersion      - version associated with the firmware
 *   mmcSMR          - SMR the base must be at (minimally) to support the firmware
 *   ffsFilename     - filename of the FFS file containing the firmware
 *
 * Returns
 *   TRUE  - if the record was added successfully
 *   FALSE - if an error occurred (i.e., the table is full)
 *
 * Caveats
 *   This function changes the contents of the CMOSACTIVEBASEDATA structure.
 *
 * Revision History
 *   06/13/2002 Joseph P. Tokarski - Initial Revision.
 */
static BOOL fnAddBaseDownloadEntry(unsigned char *firmwarePartNum,
                                   unsigned char *mmcVersion,
                                   unsigned char *mmcSMR,
                                   unsigned char *ffsFilename)
{
    extern CMOSACTIVEVERSIONDATA CMOSActiveVersionData; 

    unsigned short recId;
    ActiveVersionEntry *newEntry;

    // determine if there's room for the record
    if ((recId = CMOSActiveVersionData.activeVersionEntryCount) == (CMOS_MAX_ACTIVE_VERSION_ENTRIES - 1))
    {
        // there is NO ROOM for the new record
        return(FALSE);
    } 

    // set the record to be added
    newEntry = &CMOSActiveVersionData.activeVersionData[recId];

    // copy in the new data
    (void)strcpy((char *)&newEntry->activeBasePartNumber[0], (const char *)firmwarePartNum);
    (void)strcpy((char *)&newEntry->activeBaseMMCMinVersion[0], (const char *)mmcVersion);
    (void)strcpy((char *)&newEntry->activeBaseMinSMR[0], (const char *)mmcSMR);
    (void)strcpy((char *)&newEntry->activeBaseFirmwareFilename[0], (const char *)ffsFilename);

    // increment the record count
    CMOSActiveVersionData.activeVersionEntryCount = CMOSActiveVersionData.activeVersionEntryCount + 1;

    // the record was successfully added
    return(TRUE);
}

/*
 * Name
 *   fnAddUpdateBaseDownloadEntry
 *
 * Description
 *   This function adds an entry in the CMOSActiveVersionData structure,
 *   if no entry exists, or it updates the existing entry.  The lookup
 *   is performed based upon the Firmware Part Number.
 *
 *   An update is performed on an existing record ONLY IF the data has
 *   changed.
 *
 * Inputs
 *   firmwarePartNum - the PB Part Number associated with the 'ffsFilename'
 *                       containing the COMET Base Firmware
 *   mmcVersion      - the Version of the Base Firmware in the downloaded file
 *   mmcSMR          - the SMR that the Base must be at in order to apply the
 *                       new firmware
 *   ffsFilename     - name of the file in the Flash File System containing the
 *                       new firmware to be downloaded
 *
 * Returns
 *   TRUE  - if the record was successfully added/updated
 *   FALSE - if the record couldn't be added (i.e., table full)
 *
 * Caveats
 *   This function changes the contents of the CMOS table CMOSActiveVersionData
 *     with either a new Base Download record or an updated Base Download
 *     record.
 *
 * Revision History
 *   06/13/2002 Joseph P. Tokarski
 */
BOOL fnAddUpdateBaseDownloadEntry(unsigned char *firmwarePartNum,
                                  unsigned char *mmcVersion,
                                  unsigned char *mmcSMR,
                                  unsigned char *ffsFilename)
{
    short recordID;
    BOOL result = TRUE; 

    // determine if the record already exists
    if ((recordID = fnActivePartNumRecordExists(firmwarePartNum)) >= 0)
    {
        // update the existing record
        fnUpdateBaseDownloadEntry((short)recordID, mmcVersion, mmcSMR, ffsFilename);
    }
    else
    {
        result = fnAddBaseDownloadEntry(firmwarePartNum,
                                        mmcVersion, mmcSMR, ffsFilename);
    }

    return(result);
}

/*
 * Name
 *   fnGetBaseFirmwareEntry
 *
 * Description
 *   This function locates a base Firmware Entry record, using the
 *   Firmware Part Number as the key.
 *
 * Inputs
 *   partNum - character pointer containing the Firmware Part Number to locate
 *               the record for
 *
 * Returns
 *   a pointer to an ActiveVersionEntry structure containing the base firmware data,
 *     if successful,
 *   NULL if the record could not be found
 *
 * Caveats
 *   None.
 *
 * Revision History
 *   06/13/2002 Joseph P. Tokarski - Initial Revision.
 */
ActiveVersionEntry * fnGetBaseFirmwareEntry(unsigned char *partNum)
{
    extern CMOSACTIVEVERSIONDATA CMOSActiveVersionData; 

    unsigned long i;
    unsigned long recCount;
    ActiveVersionEntry *curEntry;
    
    // make sure there are records to locate
    if ((recCount = CMOSActiveVersionData.activeVersionEntryCount) == 0)
    {
        // there are NO records to check
        return((ActiveVersionEntry *)NULL);
    } 

    // set the initial record
    curEntry = &CMOSActiveVersionData.activeVersionData[0];

    // loop through the valid set of records, looking for a part number match
    for (i = 0; i < recCount; i++, curEntry++)
    {
        if (strcmp((const char *)partNum, (const char *)curEntry->activeBasePartNumber) == 0)
        {
            // record already exists
            return(curEntry);
        }
    }

    // firmware record not found
    return((ActiveVersionEntry *)NULL); 
}

/*
 * Name
 *   fnGetMostRecentBaseFirmwareEntry
 *
 * Description
 *   This function locates the most recent base Firmware Entry record, using the
 *   Firmware Part Number as the key.
 *
 * Inputs
 *   partNum - character pointer containing the Firmware Part Number to locate
 *               the record for
 *
 * Returns
 *   a pointer to an ActiveVersionEntry structure containing the base firmware data,
 *     if successful,
 *   NULL if the record could not be found
 *
 * Caveats
 *   None.
 *
 * Revision History
 *   07/26/2006 Derek DeGennaro - Initial Revision.
 */
ActiveVersionEntry * fnGetMostRecentBaseFirmwareEntry(unsigned char *partNum)
{
    extern CMOSACTIVEVERSIONDATA CMOSActiveVersionData; 

    unsigned long i;
    unsigned long recCount;
    ActiveVersionEntry *curEntry;
    ActiveVersionEntry *mostRecentEntry = NULL;
    
    // make sure there are records to locate
    if ((recCount = CMOSActiveVersionData.activeVersionEntryCount) == 0)
    {
        // there are NO records to check
        return((ActiveVersionEntry *)NULL);
    } 

    // set the initial record
    curEntry = &CMOSActiveVersionData.activeVersionData[0];

    // loop through the valid set of records, looking for a part number match
    for (i = 0; i < recCount; i++, curEntry++)
    {
        if (strcmp((const char *)partNum, (const char *)curEntry->activeBasePartNumber) == 0)
        {
            // entry exists for this part number
            if (!mostRecentEntry)   // this  is the first
                mostRecentEntry = curEntry;
            else if (strcmp((const char *)curEntry->activeBaseMMCMinVersion,
               (const char *)mostRecentEntry->activeBaseMMCMinVersion) >= 0)
                mostRecentEntry = curEntry;
        }
    }

    // firmware record not found
    return(mostRecentEntry);    
}

/* **********************************************************************
// FUNCTION NAME: fnSwapBytesForLongs
// DESCRIPTION: Swaps bytes that come in LSB -> MSB to MSB -> LSB.
// AUTHOR: Wes Kirschner
// **********************************************************************/
void fnSwapBytesForLongs( UINT8 *bBytesToSwap )
{
   unsigned char    Temp[ 4 ] ;

   (void)memcpy( Temp, bBytesToSwap, 4 ) ;

   *bBytesToSwap = *(Temp + 3);
   *(bBytesToSwap + 1) = *(Temp + 2);
   *(bBytesToSwap + 2) = *(Temp + 1);
   *(bBytesToSwap + 3) = *Temp;
}   


/* **********************************************************************
// FUNCTION NAME: fnIsAsciiString
// DESCRIPTION: Returns true if all chars are ascii or katakana.  False otherwise.
//              Takes a null terminated string as an argument.
// AUTHOR: Wes Kirschner
// **********************************************************************/
BOOL fnIsAsciiString( char *pString )
{
    if (*pString == 0)
        return (false) ;

    do
    {
        if (!(isalnum(*pString))) /* if not ascii/numeric */
        {
            if (!((* (unsigned char *)pString >= (unsigned char) 0xA0) && (* (unsigned char *) pString <= (unsigned char) 0xDF))) /* and if not katakana */
            {
                return (false); /*return error */
            }           
        }
        pString++;
    }
    while (*pString != 0);

    return (true) ;
}

/* **********************************************************************
// FUNCTION NAME: fnIsBCDDigit
// DESCRIPTION: Returns true if a BCD digit, false otherwise.
// AUTHOR: Wes Kirschner
// **********************************************************************/
BOOL fnIsBCDDigit( UINT8 bDigit )
{
    char szTempString[5], *p;   /* Variables used in this routine */

    /* Convert it to a hex string */
    (void)sprintf(szTempString, "%x", bDigit);

    /* Set a pointer to the top of the string */
    p = szTempString;

    /* While not at the end of the string */
    while (*p != 0)
    {
        /* Make certain it is a digit */
        if (!(isdigit(*p)))
        {
            return (false);
        }
        /* Go to next character */
        p++;
    }

    /* things are cool ! */
    return (true) ;
}


/* *************************************************************************
// FUNCTION NAME: fnCheckDoubleRange
// DESCRIPTION: Checks to see if a five byte number will fit in a double.
// AUTHOR: Wes Mozdzer
//
// INPUTS:  dblValue - the value being translated
//
// RETURNS: true if range is OK, false otherwise
//
// *************************************************************************/
BOOL fnCheckDoubleRange (double dblValue)
{
    unsigned long lwHighByte;

    lwHighByte = dblValue / 4294967296.0;
    if (lwHighByte > 255)
        return (false) ;
    else
        return (true) ;
}

// *************************************************************************
//  FUNCTION NAME:  fnConvertLongToDouble
//      
//  DESCRIPTION:
//      This function accepts 2 inputs and produces one output. It converts
//      a long value, with it's given unsigned char decimal position, and
//      returns a double value.
//  INPUTS:
//      A long value (lwValue), and it's unsigned char decimal position (bDecimalPos).
//  RETURNS:
//      A double value (dblValue).
//  NOTES:
//
//  MODIFICATION HISTORY:
// *************************************************************************
double fnConvertLongToDouble( SINT32 lValue, 
                              UINT8 bDecimalPos )
{
    double          dblValue = 0 ;
    UINT16          i ;

    dblValue = (double) lValue ;

    for( i = 0 ; i < bDecimalPos ; i++ )
    {
        dblValue /= 10 ; 
    }

    return ( dblValue ) ;
}
 //end fnConvertLongToDouble

// *************************************************************************
//  FUNCTION NAME:  fnConvertDoubleToLong
//      
//  DESCRIPTION:
//      This function accepts 2 inputs and produces two outputs. It converts
//      converts a double value, with it's given unsigned char max decimal
//      position, and returns a long value.
//      IMPORTANT NOTE: This function requires the (maximum) number of
//          decimal positions, as an input, that the returned long value
//          should have in it.
//  INPUTS:
//      dblValue - the double value to convert.
//      lValue - a pointer to a long value to be returned.
//      bMaxDecimalPos - the decimal position.
//  RETURNS:
//      SUCCESSFUL, always
//  NOTES:
//
//  MODIFICATION HISTORY:
// *************************************************************************/
BOOL fnConvertDoubleToLong( double dblValue, SINT32 *lValue, 
                            UINT8 bMaxDecimalPos )
{
    SINT16          i ;
    UINT8           bPV[20] ;

    (void) memset(bPV, 0, sizeof(bPV));

    for ( i = 0 ; i < bMaxDecimalPos ; i++ )
    {
        dblValue *= 10 ; 
    }

    (void)sprintf( (char*)bPV, "%f", dblValue ) ;
    *lValue = atol( (char*)bPV ) ;

    return (BOOL) SUCCESSFUL ;
}
 //end fnConvertDoubleToLong

/* *************************************************************************
// FUNCTION NAME: fnBinFive2Double
// DESCRIPTION: Converts a five byte binary value from unsigned char array format
//              to a double.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pBin5 - pointer to 5 byte unsigned character array containing the
//                  binary value.  the first byte is the most significant
//
// RETURNS: converted value is returned in a double
//
// *************************************************************************/
double fnBinFive2Double( UINT8 *pBin5 )
{
    double          dblRetVal;
    unsigned long   lwLow4;

    //TODO JAH changed to endian copy, but not sure how it will affect other things (void)memcpy(&lwLow4, pBin5 + 1, sizeof(unsigned long));
    EndianAwareCopy(&lwLow4, pBin5 + 1, sizeof(unsigned long));
    dblRetVal = (double) lwLow4;
    dblRetVal += (4294967296.0 * (double)(*pBin5) );

    return( dblRetVal );
}

/* *************************************************************************
// FUNCTION NAME: fnDouble2BinFive
// DESCRIPTION: Converts a whole number from double format to a five byte binary 
//              unsigned char array.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pDest - destination to place the 5 byte binary output
//          dblValue - the value being translated
//
// RETURNS: converted value is returned at address specified by pDest
//
// *************************************************************************/
void fnDouble2BinFive( UINT8 *pDest, double dblValue, BOOL swap )
{
    double			fVal;
    unsigned long   lwHighByte;
    unsigned long   lwLow4;
    char scl;

    lwHighByte = dblValue / 4294967296.0;
    if (lwHighByte > 255)
        fnDisplayErrorDirect("Bin 5 overflow", 0, OSGetCurrentTask());

    fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *) &scl);

    fVal = fmod(dblValue, 4294967296.0);
    switch( scl )
    {
        case 0:
            lwLow4 = (unsigned long) ( fVal + 0.5L);
            break;
        case 1:
            lwLow4 = (unsigned long) ( fVal + 0.05L);
            break;
        case 2:
            lwLow4 = (unsigned long) ( fVal + 0.005L);
            break;
        case 3:
            lwLow4 = (unsigned long) ( fVal + 0.0005L);
            break;
        default:
            lwLow4 = (unsigned long) fVal;
    }
    *pDest = (unsigned char) lwHighByte;

    if (swap == TRUE)
    	(void)EndianAwareCopy(pDest+1, &lwLow4, sizeof(unsigned long));
    else
    	(void)memcpy(pDest+1, &lwLow4, sizeof(unsigned long));

    return;
}


/* *************************************************************************
// FUNCTION NAME: fnDouble2BinSix
// DESCRIPTION: Converts a whole number from double format to a six byte binary 
//              unsigned char array. There is no rounding this is not used for 
//              money.
// AUTHOR: Joe Mozdzer/ Mike Richardson
//
// INPUTS:  pDest - destination to place the 6 byte binary output
//          dblValue - the value being translated
//
// RETURNS: converted value is returned at address specified by pDest
//
// *************************************************************************/
void fnDouble2BinSix( UINT8 *pDest, double dblValue )
{
//    double          dblRetVal;
    double			fVal;
 //   char scl;
    unsigned short  wHigh2;
    unsigned long   lwHighByte;
    unsigned long   lwLow4;

    lwHighByte = dblValue / 4294967296.0;       // divide by 
    if (lwHighByte > 65535)
        fnDisplayErrorDirect("Bin 6 overflow", 0, OSGetCurrentTask());

    fVal = fmod(dblValue, 4294967296.0);
    lwLow4 = (unsigned long) ( fVal );
    wHigh2 =  (unsigned short)lwHighByte;

    (void)memcpy(pDest, &wHigh2, sizeof(unsigned short));
    (void)memcpy(pDest+2, &lwLow4, sizeof(unsigned long));

    return;
}

/* *************************************************************************
// FUNCTION NAME: fnDouble2BinThree
// DESCRIPTION: Converts a whole number from double format to a three byte binary 
//                              unsigned char array.
// AUTHOR: Mostly borrowed from Joe Mozdzer
//
// INPUTS:      pDest - destination to place the 3 byte binary output
//                      dblValue - the value being translated
//
// RETURNS:     converted value is returned at address specified by pDest
//
// *************************************************************************/
void fnDouble2BinThree( UINT8 *pDest, double dblValue )
{
//    double          dblRetVal;
    double			fVal;
    unsigned long   lwHighByte;
    unsigned short  lwLow2;
    char scl;

    lwHighByte = dblValue / 65536.0;
    if (lwHighByte > 255)
        fnDisplayErrorDirect("Bin 3 overflow", 0, OSGetCurrentTask());

    fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *) &scl);

    fVal = fmod(dblValue, 65536.0);

    switch( scl )
    {
        case 0:
            lwLow2 = (unsigned long) ( fVal + 0.5L);
            break;
        case 1:
            lwLow2 = (unsigned long) ( fVal + 0.05L);
            break;
        case 2:
            lwLow2 = (unsigned long) ( fVal + 0.005L);
            break;
        case 3:
            lwLow2 = (unsigned long) ( fVal + 0.0005L);
            break;
        default:
            lwLow2 = (unsigned long) fVal;
    }

    *pDest = (unsigned char) lwHighByte;
    (void)memcpy(pDest+1, &lwLow2, sizeof(unsigned short));

}

/* *************************************************************************
// FUNCTION NAME: fnDouble2BinFour
// DESCRIPTION: Converts a whole number from double format to a four byte binary 
//                              unsigned char array.
// AUTHOR: Mostly borrowed from Joe Mozdzer
//
// INPUTS:      pDest - destination to place the 4 byte binary output
//                      dblValue - the value being translated
//
// RETURNS:     converted value is returned at address specified by pDest
//
// *************************************************************************/
void fnDouble2BinFour( UINT8 *pDest, double dblValue )
{
//    double                  dblRetVal;
    unsigned long   lwFourBytes;

    lwFourBytes = (unsigned long) dblValue;

    (void)memcpy(pDest, &lwFourBytes, sizeof(unsigned long));

    return;
}



/* *************************************************************************
// FUNCTION NAME: fnAsciiPadSpace
// DESCRIPTION: Adds spaces to the beginning of a null-terminated ascii string.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pStr - pointer to the string being padded.  the memory allocated
//                  to the string must be large enough to accomodate wLenNeeded
//                  characters plus a null terminator.
//          wLenNow - current length of the string being padded (not counting
//                      the null terminator)
//          wLenNeeded - desired length of the string after padding (not
//                          counting the null terminator)
//
// *************************************************************************/
void fnAsciiPadSpace( char *pStr, UINT16 wLenNow, UINT16 wLenNeeded )
{
    unsigned int    i;

    if (wLenNow < wLenNeeded)
    {
        (void)memmove(pStr+(wLenNeeded-wLenNow), pStr, wLenNow + 1);
        for (i = 0; i < (wLenNeeded-wLenNow); i++)
            pStr[i] = (char) 0x20;
    }
    
    return;
}       



/**************************************************************************
// FUNCTION NAME: fnAddMoneyParameters
// DESCRIPTION: Takes two money parameters, adds them, and stores the result
// AUTHOR: Wes Kirschner
//
// INPUTS:  pResult - pointer to the result of the monetary figure you are adding to.
//          pFirstMoneyToAdd - pointer to the first monetary parameter to add.
//          pSecondMoneyToAdd - pointer to the second monetary parameter to add.
//  LIMITATION - Assumes the last byte will never overflow.
// *************************************************************************/
void fnAddMoneyParameters( UINT8 *pResult, UINT8 *pFirstMoneyToAdd, UINT8 *pSecondMoneyToAdd )
{
    unsigned char *pFirst, *pSecond;
    unsigned char i;
    unsigned short wResult = 0;

    pFirst = pFirstMoneyToAdd + SPARK_MONEY_SIZE - 1;
    pSecond = pSecondMoneyToAdd + SPARK_MONEY_SIZE - 1;
    pResult = pResult + SPARK_MONEY_SIZE - 1;
    for (i = 0 ; i < SPARK_MONEY_SIZE ; i++, pFirst--, pSecond--, pResult--)
    {
      wResult = *pFirst + *pSecond + (wResult >> 8);
      *pResult = (unsigned char) wResult;
    }
}


/**************************************************************************
// FUNCTION NAME: fnCompareMoneyParameters
// DESCRIPTION: Takes two money parameters, compares them.
// AUTHOR: Wes Kirschner
// RETURNS  > 0 if First parameter is greater
//          < 0 if Second parameter is greater
//          0 if both are equal
// INPUTS:  pMoneyParam1, pMoneyParam2 - parameters to be compared.
// *************************************************************************/
SINT8 fnCompareMoneyParameters( UINT8 *pMoneyParam1, UINT8 *pMoneyParam2)
{
    UINT8   i ;

    for (i = 0 ; i < SPARK_MONEY_SIZE; i++, pMoneyParam1++, pMoneyParam2++)
    {
       if (*pMoneyParam1 > *pMoneyParam2)
         return (1);
       if (*pMoneyParam1 < *pMoneyParam2)
         return (-1) ;
    }
    return (0) ;
}


/**************************************************************************/
/********************                                 *********************/
/********************     UNICODE UTILITY ROUTINES    *********************/
/********************                                 *********************/
/**************************************************************************/




/* *************************************************************************
// FUNCTION NAME: fnUnicodeLen
// DESCRIPTION: Returns the length of a null terminated Unicode string.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pUnicode - pointer to the source Unicode string.
// RETURNS: the length of the string (in characters, not bytes), not
//          counting the null terminator
//          note: maximum value returned is 4096
// *************************************************************************/
UINT32 fnUnicodeLen(unsigned char *pUnicode)
{
	UINT32   i = 0;

    while (*pUnicode != 0 || *(pUnicode+1) != 0)
    {
        i++;
        pUnicode += 2;
        if (i >= 4096)
            break;
    }
    return (i);
}


/* *************************************************************************
// FUNCTION NAME: fnUnicodeNCopy
// DESCRIPTION: Copies a unicode string from a source that may not be on
//      an even boundary.  Copy is bounded.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pDest - Pointer to a unicode destination.  On the correct boundary.
//          pSrc - Pointer to a uchar, but really a unicode string that may 
//                  not be on an even boundary.
//          wMaxUnichars to copy, not including the null-terminator.
//
// RETURNS: the length of the string (in characters, not bytes), not
//          counting the null terminator
//
//  NOTES:  The destination must be at least (wMaxUnichars +1) *2 bytes long.
//       Maximum value returned/copied is 4096
//      A null-terminator is added at the end.
//
// *************************************************************************/
UINT16 fnUnicodeNCopy( UNICHAR *pDest, const UINT8 *pSrc, UINT16 wMaxLen )
{
    UINT16  wLen = 0;

    if( pSrc )
    {
        // This function gets the correct length, even if the unistring is
        //  not on an even boundary.
        wLen = (UINT16)fnUnicodeLen( (unsigned char *)pSrc );
        if( wLen > wMaxLen )
            wLen = wMaxLen;
        (void)memcpy( pDest, pSrc, (wLen * sizeof(unichar)) );
        pDest[ wLen ] = 0;
    }
    return( wLen );
}



/* *************************************************************************
// FUNCTION NAME: fnAscii2Unicode
// DESCRIPTION: Converts an ascii string to unicode.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pSource - pointer to the source ascii string.
//          pDest - pointer to the unicode destination.  (must be pre-malloc'ed)
//          bLen - length of the ascii string to convert.
//
// *************************************************************************/
void fnAscii2Unicode(char *pSource, unsigned short *pDest, unsigned short bLen)
{
    unsigned long   i;

    for (i = 0; i < bLen; i++)
        *(pDest + i) = (unsigned short) *((unsigned char *) pSource + i);

    return;
}
/* *************************************************************************
// FUNCTION NAME: fnAscii2UnicodeWithNul
// DESCRIPTION: Converts an ascii string to unicode with a nul code at the end.
// AUTHOR: Barbara Poscich
//
// INPUTS:  pSource - pointer to the source ascii string.
//          pDest - pointer to the unicode destination.  (must be pre-malloc'ed)
//          wLen - length of the ascii string to convert including NUL code terminator.
//
// *************************************************************************/
void fnUnicode2AsciiWithNul( UINT16 *pSource, char *pDest, UINT16 wLen )
{
    fnUnicode2Ascii(pSource, pDest, wLen) ;
    pDest[wLen] = 0 ;
}


/* *************************************************************************
// FUNCTION NAME: fnBCD2Unicode
// DESCRIPTION: Converts a BCD string to Unicode.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pBCD - pointer to the BCD string.  this should be in packed format,
//                  with one digit per nibble, or 2 digits per byte.  the output
//                  of the function is undefined if the input string contains
//                  any nibbles greater than 9.
//          pDest - pointer to the unicode destination.  (must be pre-malloc'ed)
//          bLen - length, in digits, of the BCD string to convert.  note
//                  that if this is an odd number, the high nibble of the
//                  first input byte is ignored.
//
// *************************************************************************/
void fnBCD2Unicode(unsigned char *pBCD, unsigned short *pDest, unsigned char bLen)
{
    unsigned char   bDigit;

    while (bLen)
    {
        /* pull a digit out of the string */
        if (bLen % 2)
        {
            /* process odd nibble */
            bDigit = *pBCD++;
            bDigit &= 0x0F;
        }
        else
        {
            /* process even nibble */
            bDigit = *pBCD;
            bDigit = (bDigit & 0xF0) >> 4;
        }

        /* convert digit to ascii */
        bDigit += '0';

        /* put it in the unicode output string */
        *pDest++ = (unsigned short) bDigit;

        bLen--;
    }

    return;
}

/* *************************************************************************
// FUNCTION NAME: fnUnicode2BCD
// DESCRIPTION: Converts a Unicode string to BCD.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pUnicode - pointer to the Unicode string being converted.  Should
//                      contain all digits or else and error will be reported.
//          pDest - pointer to the BCD destination.  (must be pre-malloc'ed)
//          bLen - length, in digits, of the Unicode string to convert.  note
//                  that if this is an odd number, the high nibble of the
//                  first output byte will be 0.
//
// *************************************************************************/
void fnUnicode2BCD(unsigned short *pUnicode, unsigned char *pDest, unsigned char bLen)
{
    unsigned char   bBCDByte = 0;
    unsigned char   bBCDDigit;
    unsigned short  wUniChar;
    unsigned long   i = 0;
    
    while (i < bLen)
    {
        wUniChar = *pUnicode++;
        if (fnIsUnicodeDigit(wUniChar) == FALSE)
            fnDisplayErrorDirect("Attempt to translate non-digit Unicode to BCD", 0, OSGetCurrentTask());

        /* translate Unicode to BCD */
        bBCDDigit = ((unsigned char) wUniChar) - '0';   


        if ((bLen - i) % 2)
            /* if we are doing the low nibble, add it to the local byte and stuff in the
                destination address.  (also move the dest pointer and reset the local byte) */
        {
            bBCDByte = bBCDByte | bBCDDigit;
            *pDest++ = bBCDByte;
            bBCDByte = 0;
        }
        else
            /* if we are doing the high nibble of a byte, stick it in a local byte */
            bBCDByte = bBCDByte | (bBCDDigit << 4);
        
        i++;
    }

    return;

}

/* *************************************************************************
// FUNCTION NAME: fnIsUnicodeHex()
// DESCRIPTION: Checks if a Unicode character is a hexadecimal digit.
// AUTHOR: George Monroe
//
// INPUTS:  bUnicodeChar - the Unicode character
//
// OUTPUTS: true - character is a hexadecimal digit
//          false - character is not a digit
// *************************************************************************/
BOOL fnIsUnicodeHex(unsigned short bUnicodeChar)
{

    if (((bUnicodeChar >= UNICODE_ZERO)    && (bUnicodeChar <= UNICODE_NINE)) ||
        ((bUnicodeChar >= UNICODE_ALPHA_A) && (bUnicodeChar <= UNICODE_ALPHA_F)) ||
        ((bUnicodeChar >= UNICODE_ALPHA_a)  && (bUnicodeChar <= UNICODE_ALPHA_f)))
        return true;
    else
        return false;
}

/* *************************************************************************
// FUNCTION NAME: fnUnicode2Hex
// DESCRIPTION: Converts a Unicode string to a Packed Hex Value.
// AUTHOR: George Monroe
//
// INPUTS:  pHex - pointer to the Hex string.  this should be in packed format,
//                  with one digit per nibble, or 2 digits per byte.  the output
//                  of the function is undefined if the input string contains
//                  any nibbles not 0-9 and A-F
//                                      a-f is converted to A-F
//          pDest - pointer to the unicode destination.  (must be pre-malloc'ed)
//          bLen - length, in digits, of the Hex string to convert.  note
//                  that if this is an odd number, the high nibble of the
//                  first input byte is ignored.
//
// *************************************************************************/
void fnUnicode2Hex(unsigned short *pUnicode, unsigned char *pDest, unsigned char bLen)
{
    unsigned char   bHexByte = 0;
    unsigned char   bHexDigit;
    unsigned short  wUniChar;
    unsigned long   i = 0;
    
    while (i < bLen)
    {
        wUniChar = *pUnicode++;
        if (fnIsUnicodeHex(wUniChar) == FALSE)
            fnDisplayErrorDirect("Attempt to translate non-hexdigit Unicode to Hex",
                         0, OSGetCurrentTask());

        /* translate Unicode to BCD */
        if(wUniChar >= UNICODE_ALPHA_a && wUniChar <= UNICODE_ALPHA_f)
            wUniChar &= UNICODE_LOWER_UPPER_MASK;
        if(wUniChar >= UNICODE_ALPHA_A && wUniChar <= UNICODE_ALPHA_F)
            bHexDigit = ((unsigned char) wUniChar) - 'A' + 10;  
        else
            bHexDigit = ((unsigned char) wUniChar) - '0';   


        if ((bLen - i) % 2)
            /* if we are doing the low nibble, add it to the local byte and stuff in the
                destination address.  (also move the dest pointer and reset the local byte) */
        {
            bHexByte = bHexByte | bHexDigit;
            *pDest++ = bHexByte;
            bHexByte = 0;
        }
        else
            /* if we are doing the high nibble of a byte, stick it in a local byte */
            bHexByte = bHexByte | (bHexDigit << 4);
        
        i++;
    }

    return;

}

/* *************************************************************************
// FUNCTION NAME: fnUnicode2Ascii
// DESCRIPTION: Converts a unicode string to ascii.  THIS FUNCTION CAN ONLY
//              HANDLE UNICODE STRINGS THAT ARE ENTIRELY NUMERIC.  If this
//              is not the case, garbage will be returned at *pDest.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pSource - pointer to the source unicode string.
//          pDest - pointer to the ascii destination.  (must be pre-malloc'ed)
//          bLen - length of the unicode string to convert.
//
// RETURNS:  SUCCESSFUL if the Unicode string is entirely numeric.
//           FAILURE otherwise
//
// *************************************************************************/
BOOL fnUnicode2Ascii(unsigned short *pSource, char *pDest, unsigned short wLen)
{
    unsigned long   i;
    BOOL            fRetVal;

    fRetVal = SUCCESSFUL;

    for (i = 0; i < wLen; i++)
    {
        if (fnIsUnicodeDigit(pSource[i]) == false)
            fRetVal = FAILURE;
        *(pDest + i) = (char) *(pSource + i);
    }

    return (fRetVal);
}


/* *************************************************************************
// FUNCTION NAME: fnUnicodeStr2AsciiStr
// DESCRIPTION: Converts a unicode string to ascii str.
//
// INPUTS:  pSource - pointer to the source unicode string.
//          pDest - pointer to the ascii destination.  (must be pre-malloc'ed)
//          bLen - length of the unicode string to convert.
//
// RETURNS:  SUCCESSFUL if the Unicode string is entirely numeric.
//           FAILURE otherwise
//
// *************************************************************************/
BOOL fnUnicodeStr2AsciiStr(unsigned short *pSource, char *pDest, unsigned short wLen)
{
    unsigned long   i;
    BOOL            fRetVal;

    fRetVal = SUCCESSFUL;

    for (i = 0; i < wLen; i++)
    {
        *(pDest + i) = (char) *(pSource + i);
    }

    return (fRetVal);
}

/* *************************************************************************
// FUNCTION NAME: fnUnicode2Bin
// DESCRIPTION: Converts a unicode string to a binary long word.  The function
//              will search and find the first numeric string within the 
//              input Unicode string and use that for the translation.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pSource - pointer to the source unicode string.
//          pBinary - pointer to where the binary output should be returned (valid
//                      only if SUCCESSFUL is returned by the function)
//          bLen - length of the unicode string to convert
//
// RETURNS:  SUCCESSFUL if the Unicode string was successfully converted.
//           FAILURE if there are no number in the Unicode string or there
//              are more than 9 consecutive digits.
//
// *************************************************************************/
BOOL fnUnicode2Bin(unsigned short *pSource, UINT32 *pBinary, unsigned char bLen)
{
    unsigned long   i, j;
    BOOL            fRetVal;
    char            pCharArray[10];     /* biggest long word possible is 4294967295, so array is 10 digits */


    fRetVal = SUCCESSFUL;
    
    /* initialize the char array to spaces */
    for (i = 0; i < 10; i++)
        pCharArray[i] = ' ';
    
    /* find the first digit in the Unicode string starting from the left and working to the right */
    for (i = 0; i < bLen; i++)
    {
        if (fnIsUnicodeDigit(pSource[i]) == true)
            break;
    }

    /* if we didn't find a digit the function fails */
    if (i == bLen)
    {
        fRetVal = FAILURE;
        goto xit;
    }

    /* find the last digit in the Unicode string */
    j = i;
    while ((j < bLen) && (fnIsUnicodeDigit(pSource[j]) == true))
        j++;

    /* if more than 9 digits long it is a failure because we can't guarantee it will fit in a long */
    if (j - i > 9)
    {
        fRetVal = FAILURE;
        goto xit;
    }

    /* translate the unicode numeric string to ascii */
    fnUnicode2Ascii(pSource + i, pCharArray, j - i);

    *pBinary = (UINT32) atoi(pCharArray);
    
xit:return (fRetVal);
}

/* *************************************************************************
// FUNCTION NAME: fnUnicodeHex2Bin
// DESCRIPTION: Converts a unicode hex byte to a binary long word.
// AUTHOR: Mark D. Zamary
//
// INPUTS:  wUniHexChar - the source unicode hex byte.
//          pBinary - pointer to where the binary output should be returned
//                      (valid only if SUCCESSFUL is returned by the function)
//
// RETURNS:  SUCCESSFUL if the unicode hex byte was successfully converted.
//           FAILURE if there is not a number in the unicode hex byte.
//
// *************************************************************************/
BOOL fnUnicodeHex2Bin(unsigned short wUniHexChar, UINT32 *pBinary)
{
    BOOL    fRetVal = SUCCESSFUL;

    if ( fnIsUnicodeHex(wUniHexChar) == FALSE )
        fRetVal = FAILURE;

    if ( wUniHexChar >= UNICODE_ALPHA_a && wUniHexChar <= UNICODE_ALPHA_f )
        wUniHexChar &= UNICODE_LOWER_UPPER_MASK;
    if ( wUniHexChar >= UNICODE_ALPHA_A && wUniHexChar <= UNICODE_ALPHA_F )
        *pBinary = ((unsigned char) wUniHexChar) - 'A' + 10;    
    else
        *pBinary = ((unsigned char) wUniHexChar) - '0';
                
    return (fRetVal);
}

/* *************************************************************************
// FUNCTION NAME: fnBin2Unicode
// DESCRIPTION: Converts a binary value to a decimal unicode string.  There 
//              are no leading zeroes in the output string, unless the value 
//              is exactly 0.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pDest - pointer to the unicode destination.  (must be pre-malloc'ed)
//          lwBinary - binary value to convert
//
// *************************************************************************/
unsigned char fnBin2Unicode(unsigned short *pDest, UINT32 lwBinary)
{
//    unsigned short  i;
    char            pStr[16];   /* max an unsigned long can be is 4294967295, so 16 chars is more than enough */
    int             iStrLen;


    iStrLen = sprintf(pStr, "%u", lwBinary);
    fnAscii2Unicode(pStr, pDest, iStrLen);
    return ((unsigned char) iStrLen);
}

/* *************************************************************************
// FUNCTION NAME: fnSignBin2Unicode
// DESCRIPTION: Converts a binary value to a decimal unicode string.  There 
//              are no leading zeroes in the output string, unless the value 
//              is exactly 0.
// AUTHOR: Joe Mozdzer + Craig DeFilippo
//
// INPUTS:  pDest - pointer to the unicode destination.  (must be pre-malloc'ed)
//          lwBinary - binary value to convert
//
// *************************************************************************/
unsigned char fnSignBin2Unicode(unsigned short *pDest, int lwBinary)
{
//    unsigned short  i;
    char            pStr[16];   /* max an unsigned long can be is 4294967295, so 16 chars is more than enough */
    int             iStrLen;


    iStrLen = sprintf(pStr, "%d", lwBinary);
    fnAscii2Unicode(pStr, pDest, iStrLen);
    return ((unsigned char) iStrLen);
}

/* *************************************************************************
// FUNCTION NAME: fnIsUnicodeDigit()
// DESCRIPTION: Checks if a Unicode character is a digit.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  bUnicodeChar - the Unicode character
//
// OUTPUTS: true - character is a digit
//          false - character is not a digit
// *************************************************************************/
BOOL fnIsUnicodeDigit( UINT16 bUnicodeChar )
{
    if ((bUnicodeChar >= UNICODE_ZERO) && (bUnicodeChar <= UNICODE_NINE))
        return true;
    else
        return false;
}



/* *************************************************************************
// FUNCTION NAME: fnUnicodePadSpace
// DESCRIPTION: Adds spaces to the beginning of a null-terminated Unicode string.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pStr - pointer to the string being padded.  the memory allocated
//                  to the string must be large enough to accomodate wLenNeeded
//                  characters plus a null terminator.
//          wLenNow - current length of the string being padded (not counting
//                      the null terminator)
//          wLenNeeded - desired length of the string after padding (not
//                          counting the null terminator)
//
// *************************************************************************/
void fnUnicodePadSpace(unsigned short *pStr, unsigned short wLenNow, unsigned short wLenNeeded)
{
    unsigned int    i;

    if (wLenNow < wLenNeeded)
    {
		// make sure the string is null-terminated 
		pStr[wLenNow] = 0;
		
		// move all the characters and the null-terminator
        (void)memmove(pStr+(wLenNeeded-wLenNow), pStr, (wLenNow + 1) * sizeof(unsigned short));
		
		// fill in the necessary spaces
        for (i = 0; i < (wLenNeeded-wLenNow); i++)
            pStr[i] = UNICODE_SPACE;
    }
}       

/* *************************************************************************
// FUNCTION NAME: 
//      fnUnicodeCenterAligned
//
// DESCRIPTION: 
//      Adds spaces to the beginning and tail of a null-terminated Unicode 
//      string to make the string center aligned
//
// INPUTS:
//      pStr - pointer to the string being padded.  the memory allocated
//                  to the string must be large enough to accomodate wLenNeeded
//                  characters plus a null terminator.
//      wLenNow - current length of the string being padded (not counting
//                      the null terminator)
//      wLenNeeded - desired length of the string after padding (not
//                          counting the null terminator)
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY
//  02/16/2006  Vincent Yi      Initial version
//
// *************************************************************************/
void fnUnicodeCenterAligned (unsigned short *pStr,
                             unsigned short wLenNow,
                             unsigned short wLenNeeded)
{
    if (wLenNow < wLenNeeded)
    {
        unsigned short wLenSpaces = (wLenNeeded - wLenNow) / 2;
        unsigned short wIndex;
    
        (void)memmove(pStr+wLenSpaces, pStr, (wLenNow + 1) * sizeof(unsigned short));

        // Pad Spaces in the beginning
        for (wIndex = 0; wIndex < wLenSpaces; wIndex++)
        {
            pStr[wIndex] = UNICODE_SPACE;
        }
		
        // Pad Spaces at the tail
        for (wIndex += wLenNow; wIndex < wLenNeeded; wIndex++)
        {
            pStr[wIndex] = UNICODE_SPACE;
        }
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnUnicodeRemoveSpace
//
// DESCRIPTION: 
//      Removes spaces from the left aligned unicode string.
//
// INPUTS:
//      pStr - pointer to the string
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY
//  07/25/2007  Vincent Yi      Initial version
//
// *************************************************************************/
void fnUnicodeRemoveSpace (unsigned short *pStr)
{
    UINT8       ucLen;


    ucLen = unistrlen (pStr);
    while (ucLen>0 && (*(pStr+ucLen-1) == UNICHR_SPACE))
    {
        ucLen--;
    }
	
    *(pStr+ucLen) = (UINT16) NULL;
}

#ifdef JANUS
/* *************************************************************************
// FUNCTION NAME: fnUnicodePadChar
// DESCRIPTION: Adds unicode char to the beginning/ending of a null-terminated Unicode string.
// AUTHOR: Tim Zhang
//
// INPUTS:  pStr - pointer to the string being padded.  the memory allocated
//                  to the string must be large enough to accomodate wLenNeeded
//                  characters plus a null terminator.
//          wLenNow - current length of the string being padded (not counting
//                      the null terminator)
//          wLenNeeded - desired length of the string after padding (not
//                          counting the null terminator)
//
// *************************************************************************/
void fnUnicodePadChar(unsigned short *pStr, unsigned short wCharToPad, unsigned short wLenNow
                            , unsigned short wLenNeeded, unsigned char padType)
{
    unsigned int    i;

    
    if (wLenNow < wLenNeeded)
    {
        if ( padType == 0 ) // pad to the left/beginning of the string.
        {
			// make sure the string is null-terminated 
			pStr[wLenNow] = 0;
		
			// move all the characters and the null-terminator
            (void)memmove(pStr+(wLenNeeded-wLenNow), pStr, (wLenNow + 1) * sizeof(unsigned short));
			
			// fill in the necessary character
            for (i = 0; i < (wLenNeeded-wLenNow); i++)
                pStr[i] = wCharToPad;
        }
        else
        {
            for (i = wLenNow; i < wLenNeeded; i++)
                pStr[i] = wCharToPad;
        }
    }
}       
#endif

/* *************************************************************************
// FUNCTION NAME: fnUnicodeCompare
// DESCRIPTION: Compares 2 unicode strings.  This function returns the number
//              of characters that match between s1 and s2 until the first
//              non-matching character is found.  E.g. if string 1 is "Alaska"
//              and string 2 is "Alabama" this function would return a 3.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pStr1 - pointer to string 1 (can be a unsigned char *, short align
//                  is not necessary). 
//          pStr2 - pointer to string 2 (can be a unsigned char *, short align
//                  is not necessary).
//          wMaxLen - maximum number of characters to compare (must be >= 1). 
//                      note that the comparison automatically ends when a null terminator
//                      is found in either string.
//          fIgnoreCase - if set to TRUE, upper or lower case of letters is ignored
//                          when making the comparison
//
// OUTPUTS: returns the number of characters that matched before the first
//          non matching character was discovered
//
// *************************************************************************/
unsigned short fnUnicodeCompare(unsigned char *pStr1, unsigned char *pStr2, unsigned short wMaxLen,
                                BOOL fIgnoreCase)
{
    unsigned short  wMatchingChars = 0;
    unsigned long   i = 0;
    unsigned short  wUnicode1;
    unsigned short  wUnicode2;

    while (i < wMaxLen)
    {
        /* grab the next unicode character of each string */
        (void)memcpy(&wUnicode1, pStr1 + (i * sizeof(unsigned short)), sizeof(unsigned short));       
        (void)memcpy(&wUnicode2, pStr2 + (i * sizeof(unsigned short)), sizeof(unsigned short));       
        /* if either is a null, then we are done */
        if ((wUnicode1 == (unsigned short) NULL) || (wUnicode2 == (unsigned short) NULL))
            break;

        /* if we are ignoring the case, convert all letters to lower case before comparing */
        if (fIgnoreCase == TRUE)
        {
            if ((wUnicode1 >= 0x0041) && (wUnicode1 <= 0x005A))
                wUnicode1 += 0x20;      /* convert to lower case if this is an upper case letter */
            if ((wUnicode2 >= 0x0041) && (wUnicode2 <= 0x005A))
                wUnicode2 += 0x20;      /* convert to lower case if this is an upper case letter */
        }

        if (wUnicode1 != wUnicode2)
            break;
        
        /* otherwise proceed to the next character */
        wMatchingChars++;
        i++;
    } 

    return (wMatchingChars);
}       

/* *************************************************************************
// FUNCTION NAME: fnUnicode2Money
// DESCRIPTION:  Converts a Unicode value to money.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pUnicode - pointer to the null-terminated Unicode string being translated.  
//                      the string can have any number of leading non-digit, 
//                      non-decimal characters, followed by an optional whole number 
//                      portion, followed by an optional decimal point character, 
//                      followed by an optional decimal number portion.  examples of valid
//                      input strings (assume in null-terminated Unicode):
//                      "$ 1500.123", ".223 DM", "0", "Greenbacks $300."
//
// RETURNS: double containing whole-number value, normalized to the number of
//          money decimal places for the given PCN.  (e.g. "$1" is translated
//          to double "1000.0" if money decimal places is 3.)  If nothing
//          is translatable in the input string, 0.0 is returned.
// *************************************************************************/
double fnUnicode2Money(unsigned short *pUnicode)
{

    unsigned char   bMoneyDecimals; 
    char            pCharArray[255];  /* made this big to be safe.  we should never have a string this big */
    long            i, j, k;
    double          dblWhole, dblDecimal, dblMoney;
    unsigned long   lwDivisor;
    unsigned short  wUnicodeDecimalChar;


    /* initialize a pointer to the number of decimal places */
    if (fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *) &bMoneyDecimals) != BOB_OK)
    {
        fnProcessSCMError();
        dblMoney = 0.0;
        goto xit;
    }

    /* break the Unicode string down into a whole number and a decimal number part */
    i = 0;
    dblWhole = dblDecimal = 0.0;

    /* find the first Unicode digit or decimal point character */
    wUnicodeDecimalChar = fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR);

    while ((pUnicode[i] != (unsigned short) NULL) && (fnIsUnicodeDigit(pUnicode[i]) == false) &&
            (pUnicode[i] != wUnicodeDecimalChar))
        i++;

    if (fnIsUnicodeDigit(pUnicode[i]) == true)
    {
        /* extract the whole number portion of the unicode string and convert to a double */
        j = i + 1;
        while (fnIsUnicodeDigit(pUnicode[j]) == true)
            j++;
        /* at this point, i is the index to the start of the whole number part and j is one
            character past the end */
        fnUnicode2Ascii(pUnicode + i, pCharArray, j - i);   /* convert to ascii */
        pCharArray[j - i] = (char) NULL;                    /* make it null-terminated */
        dblWhole = strtod(pCharArray, (char **) NULL);      /* convert to double */
        i = j;
    }

    if (pUnicode[i] == wUnicodeDecimalChar)
    {
        /* extract the decimal portion of the unicode string and convert to a double */
        i++;
        j = i;
        while (fnIsUnicodeDigit(pUnicode[j]) == true)
            j++;
        /* at this point, i is the index to the start of the whole number part and j is one
            character past the end. if i = j, there are no digits after the decimal point */
        if (j > i)
        {
            fnUnicode2Ascii(pUnicode + i, pCharArray, j - i);   /* convert to ascii */
            for (k = j - i; k < bMoneyDecimals; k++)            /* pad with trailing zeroes if there */
                pCharArray[k] = '0';                            /*  are less digits than money decimal places */
            /* if there were more digits than money decimal places, we need to truncate the 
                string.  the user interface should never allow a string to be typed in that
                has more digits after the decimal point than there are money decimal places,
                so hopefully this should never happen */
            if (k > bMoneyDecimals)                             
                k = bMoneyDecimals;             
            pCharArray[k] = (char) NULL;                        /* make the string null-terminated */
            dblDecimal = strtod(pCharArray, (char **) NULL);    /* convert to a double */
        }
    }

    /* calculate 10 to the *pMoneyDecimals power */
    lwDivisor = 1;
    for (i = 0; i < bMoneyDecimals; i++)
        lwDivisor = lwDivisor * 10;

    dblMoney = (dblWhole * (double) lwDivisor) + dblDecimal;

xit:
    return (dblMoney);  
}


//*************************************************************************
// FUNCTION NAME: fnMoney2UnicodeUnv
// DESCRIPTION:  Converts a money value to Unicode. 
//
// AUTHOR: Craig DeFilippo, Clarisa Bellamy
//
// INPUTS:  pDest - pointer to the destination unicode string. memory allocated
//                  to the string should be large enough to accomodate the largest
//                  possible translation
//
//          dblGreenbacks - monetary value to convert.  this is a double floating
//                          point value that should be a whole number, since  
//                          any decimal places implied in the value are derived from
//                          a global PCN parameter.  E.g.- a value of 50025 with
//                          decimal places = 2 would mean 500.25 currency units.
//                          if dblGreenbacks is not a whole number, the fractional
//                          part is ignored.
//
//          bShowDecimals - indicates whether or not to include the decimal portion
//                          of the monetary value in the output string.  
//                      UTIL_DCML_NO_SHOW      "500"
//                      UTIL_DCML_SHW_POINT    "500.25"
//                      UTIL_DCML_SHW_NOPOINT  "50025"
//
//          bMinWhole - minimum number of whole integer digits to be returned.  this is
//                      used to pad the integer portion of the string with leading
//                      zeroes, if necessary.
//
//          bMinDecimal - minimum number of decimal digits to be returned.  this is
//                          used to pad the decimal portion of the string with trailing
//                          zeroes, if necessary.
//          fRemPadding - if True trailing zeros are removed
//
// RETURNS: length, in words, of the Unicode string
// -------------------------------------------------------------------------------
#ifndef JANUS
unsigned char fnMoney2UnicodeUnv( unsigned short *pDest, double dblGreenbacks, 
                                  unsigned char bMinWhole, unsigned char bMinDecimal, 
                                  unsigned char bShowDecimals, BOOL fRemPadding )
{
    unsigned char   bMoneyDecimals = 0; // Number of assumed decimals in money value.
                                        // Use 0 until the PSD is detected.
    unsigned long   lwDivisor;      // 10 to the *bMoneyDecimals power
    double          dblWhole;       // Whole portion of money
    int             iWholeLen;      // Length unichars of whole portion of unistring,
    double          dblDecimal;     // Fractional portion of money.
    int             iDecLen;        // Length in unichars of Decimal portion of unistring.
    unsigned short  wUnicodeDecimalChar;  // Character to print between whole number and fraction.
    unsigned char   bUniLen;        // Length in unichars of entire resultant unistring.
    long            i;              
    char            pAsciiMoney[32];    // Temp buffer for conversion of dbl to ascii


    // Read the configuration parameter: number of decimal places.
    if( fnSYSIsPSDDetected() == TRUE )      
    {
        // Since it is stored IN the Myko PSD, must check that PSD is detected first.
        //  Otherwise, fnValidData will fail, and we do NOT want to trigger a MeterError
        //  at THIS point if we can't detect the PSD.
        if( fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *) &bMoneyDecimals) != BOB_OK )
        {
            // If the PSD IS detected, and we still can't read the value, ERROR!
            fnProcessSCMError();
            bUniLen = 0;
            goto xit;
        }
    }

    // Calculate 10 to the *bMoneyDecimals power
    lwDivisor = 1;
    for( i = 0; i < bMoneyDecimals; i++ )
        lwDivisor = lwDivisor * 10;

    // Load the integer part of the value into dblWhole.
    (void)modf(dblGreenbacks / ((double) lwDivisor), &dblWhole);
    // Load the decimal part of the value into dblDecimal.
    dblDecimal = fmod(dblGreenbacks, (double) lwDivisor);

    // Print the integer part in the temp buffer and get its length.
    iWholeLen = sprintf(pAsciiMoney, "%.0f", dblWhole);

    // If sprintf returned a single 0, we change it to a null string instead.  sprintf
    //  does not allow us to specify a zero precison for doubles, so this is a patch for that.
    //  If this was not done, then if bMinWhole = 0 the function would not work correctly.
    if( (iWholeLen == 1) && (pAsciiMoney[ 0 ] == '0') )
        iWholeLen = 0;


    // If the minimum number of whole digits is more than we have from the 
    // initial conversion, we need to pad with left side zeros.
    if( bMinWhole > iWholeLen )
    {
        // Don't overwrite the array bounds
        if( bMinWhole > 31 )
            bMinWhole = 31;

        // Shift digits to the right to make some room (may not be null-terminated now.)
        (void)memmove( pAsciiMoney + (bMinWhole - iWholeLen), pAsciiMoney, iWholeLen );
        // Stick in the leading zeroes.
        for( i = 0; i < (bMinWhole - iWholeLen); i++ )
            pAsciiMoney[ i ] = '0';
        // The length has increased.
        iWholeLen = bMinWhole;
    }

    // Stick the whole integer portion into the unicode string.
    fnAscii2Unicode( pAsciiMoney, pDest, iWholeLen );
    bUniLen = (unsigned char) iWholeLen;

    // If we want to show the decimal portion...
    if( bShowDecimals != UTIL_DCML_NO_SHOW )
    {
        // Print the decimal portion to the temp string as an integer.
        iDecLen = sprintf(pAsciiMoney, "%.0f", dblDecimal);

        // Pad on the left side with zeroes to ensure that the value ends up in the
        //  correct columns... e.g. if moneydecimals is 3 & dblDecimal = 5, it should be
        //  displayed as .005, so add two left side zeroes
        if( bMoneyDecimals > iDecLen )
        {   
            // Don't overwrite array 
            if( bMoneyDecimals > 31 )
               bMoneyDecimals = 31;

            // Shift digits to the right to make some room (may not be null-terminated now.)
            (void)memmove(pAsciiMoney + (bMoneyDecimals - iDecLen), pAsciiMoney, iDecLen);
            // Stick in the left-side zeroes.
            for( i = 0; i < (bMoneyDecimals - iDecLen); i++ )
                pAsciiMoney[ i ] = '0';
            // The length has increased.
            iDecLen = bMoneyDecimals;
        }

        // Trailing zeroes can be removed; e.g. .250 can be shortened to .25, .0000 can be 
        //  shortened to a null string
        if( fRemPadding == TRUE )
        {
            i = iDecLen - 1;
            while( (i >= 0) && (pAsciiMoney[ i ] == '0') )
            {
                // If the last digit is '0', decrease the length by 1 and check again.
                iDecLen--;
                i--;
            }
        }   

        // If the minimum number of decimal digits is greater than we currently
        //  have, pad on the right-hand side with zeroes.
        if( bMinDecimal > iDecLen )
        {
            // Don't overwrite the array bounds
            if( bMinDecimal > 31 )
                bMinDecimal = 31;

            for( i = iDecLen; i < bMinDecimal; i++ )
                pAsciiMoney[ i ] = '0';
            // Legnth has increased.    
            iDecLen = bMinDecimal;
        }

        // Finally, if there is at least one decimal digit, add the decimal portion 
        //  the unicode string.
        if( iDecLen )
        {
            // Do we show the decimal point or not?
            if( bShowDecimals == UTIL_DCML_SHW_POINT )
            {
                // If we show the "decimal point", we need to read the proper one 
                //  from Flash (luckily it should be in unicode.)
                wUnicodeDecimalChar = fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR);
                pDest[iWholeLen] = wUnicodeDecimalChar;  
                bUniLen++;              
            }
            // After possibly adding the decimal point, add unicode version of the decimal string.
            fnAscii2Unicode( pAsciiMoney, (pDest + bUniLen), iDecLen );

            bUniLen += (unsigned char) iDecLen;
        }
    }
xit:
    return( bUniLen );
}
#endif


//********************************************************************************/
// NAME:    fnMoney2AsciiUnv               
// AUTHOR:  craig defilippo, Clarisa Bellamy               
// DESCRIPTION: Translate a 5-UINT8 money value to Double.  Then, using the flash
//              parameters BP_MIN_PRINTED_INTEGERS and BP_MIN_PRINTED_DECIMALS
//              convert the double to unicode, from unicode to ASCII.
//
// INPUTS:      pLen - Ptr to place to store length.
//              pAscii - Ptr to  place to store resulting ascii string
//              pMoney - Ptr to 5-char array that contains the money value to 
//                       convert.
//              fRemPadding - If TRUE, remove any trailing zeros in the decimal
//                      (fractional) portion of the string.
//              bShowDecimals - 
//                      UTIL_DCML_NO_SHOW       "500"
//                      UTIL_DCML_SHW_POINT    "500.25"
//                      UTIL_DCML_SHW_NOPOINT  "50025"
// OUTPUT: Return SUCCESSFUL.  Store null-terminated ASCII string at pAscii and 
//                              length of ASCII string in pLen.
//
//            
// NOTE:  It is NOT recommended to set fRemPadding to TRUE if 
//        bShowDecimals == UTIL_DCML_SHW_NOPOINT 
// -------------------------------------------------------------------------------
#ifndef JANUS
unsigned char fnMoney2AsciiUnv( unsigned short *pLen, char *pAscii, unsigned char *pMoney, 
                                unsigned char bShowDecimals, BOOL fRemPadding )
{
    unsigned char   retcode = SUCCESSFUL;
    unsigned char   bMinWhole,bMinDec;
    double          dblMoney;
    unsigned short  pUnicode[24];

    pUnicode[0] = (unsigned short) NULL;

    // Translate MONEY to DOUBLE
    dblMoney = fnBinFive2Double( pMoney );

    // Get min # of integers and decimals from FLASH
    bMinWhole = fnFlashGetByteParm( BP_MIN_PRINTED_INTEGERS );
    bMinDec   = fnFlashGetByteParm( BP_MIN_PRINTED_DECIMALS );

    // Translate MONEY from DOUBLE format to UNICODE 
    *pLen = fnMoney2UnicodeUnv( pUnicode, dblMoney, bMinWhole, bMinDec, 
                                bShowDecimals, fRemPadding );

    // Now translate UNICODE to ASCII.
    (void)fnUnicode2Ascii( pUnicode, pAscii, *pLen );

    pAscii[*pLen] = 0 ;

    return( retcode );
}
#endif

// *************************************************************************
// FUNCTION NAME: fnMoney2UnicodeFmt
// DESCRIPTION:  Converts a money value to Unicode.
// AUTHOR: Joe Mozdzer, Clarisa Bellamy
//
// INPUTS:  pDest - pointer to the destination unicode string. memory allocated
//                  to the string should be large enough to accomodate the largest
//                  possible translation
//
//          dblGreenbacks - monetary value to convert.  this is a double floating
//                          point value that should be a whole number, since  
//                          any decimal places implied in the value are derived from
//                          a global PCN parameter.  E.g.- a value of 50025 with
//                          decimal places = 2 would mean 500.25 currency units.
//                          if dblGreenbacks is not a whole number, the fractional
//                          part is ignored.
//
//          fShowDecimals - indicates whether or not to include the decimal portion
//                          of the monetary value in the output string.  (If
//                          set to false, Unicode "500" would be returned in
//                          the above example.  If true, "500.25")
//
//          bMinWhole - minimum number of whole integer digits to be returned.  this is
//                      used to pad the integer portion of the string with leading
//                      zeroes, if necessary.
//
//          bMinDecimal - minimum number of decimal digits to be returned.  this is
//                          used to pad the decimal portion of the string with trailing
//                          zeroes, if necessary.
//          bRemPadding - if True trailing zeros are removed
//
// RETURNS: length, in words, of the Unicode string
// NOTE:  If the resulting string includes a decimal portion, then a decimal 
//          character (WP_DISP_DECIMAL_POINT_CHAR) IS embedded in the string.
// -------------------------------------------------------------------------------
#if 0
// Handled by macro in global.h for Mega - cb
// Not used by Janus
unsigned char fnMoney2UnicodeFmt( unsigned short *pDest, double dblGreenbacks, 
                                  BOOL fShowDecimals, unsigned char bMinWhole, 
                                  unsigned char bMinDecimal, BOOL bRemPadding )
{
    unsigned char bShowDecimals;
    unsigned char retcode;

    bShowDecimals = (fShowDecimals ? UTIL_DCML_SHW_POINT : UTIL_DCML_NO_SHOW);
    retcode =  fnMoney2UnicodeUnv( pDest, dblGreenbacks, bMinWhole, bMinDecimal, 
                                   bShowDecimals, bRemPadding );
    return( retcode );
}
#endif

// *************************************************************************
// FUNCTION NAME: fnMoney2Unicode
// DESCRIPTION:  Converts a money value to Unicode.
// AUTHOR: Joe Mozdzer, Clarisa Bellamy
//
// INPUTS:  pDest - pointer to the destination unicode string. memory allocated
//                  to the string should be large enough to accomodate the largest
//                  possible translation
//
//          dblGreenbacks - monetary value to convert.  this is a double floating
//                          point value that should be a whole number, since  
//                          any decimal places implied in the value are derived from
//                          a global PCN parameter.  E.g.- a value of 50025 with
//                          decimal places = 2 would mean 500.25 currency units.
//                          if dblGreenbacks is not a whole number, the fractional
//                          part is ignored.
//
//          fShowDecimals - indicates whether or not to include the decimal portion
//                          of the monetary value in the output string.  (If
//                          set to false, Unicode "500" would be returned in
//                          the above example.  If true, "500.25")
//
//          bMinWhole - minimum number of whole integer digits to be returned.  this is
//                      used to pad the integer portion of the string with leading
//                      zeroes, if necessary.
//
//          bMinDecimal - minimum number of decimal digits to be returned.  this is
//                          used to pad the decimal portion of the string with trailing
//                          zeroes, if necessary.
//
// RETURNS: length, in words, of the Unicode string
// NOTE: Does NOT remove extra zeros in the decimal portion.
// -------------------------------------------------------------------------------
#if 0 
// Handled by macro in global.h for Mega - cb
// Not used by Janus
unsigned char fnMoney2Unicode( unsigned short *pDest, double dblGreenbacks, 
                               BOOL fShowDecimals, unsigned char bMinWhole, 
                               unsigned char bMinDecimal )
{
    unsigned char bShowDecimals;
    unsigned char retcode;

    bShowDecimals = (fShowDecimals ? UTIL_DCML_SHW_POINT : UTIL_DCML_NO_SHOW);
    retcode =  fnMoney2UnicodeUnv( pDest, dblGreenbacks, bMinWhole, bMinDecimal, 
                                   bShowDecimals, TRUE );
    return( retcode );
}
#endif

//**************************************************************************
// FUNCTION NAME: fnIntMoney2UnicodeFmt
// DESCRIPTION:  Converts a money value to Unicode. 
//              NOTE ** NO DECIMAL CHARACTER IS EMBEDDED. **
//
// AUTHOR: Craig DeFilippo, Clarisa Bellamy
//
// INPUTS:  pDest - pointer to the destination unicode string. memory allocated
//                  to the string should be large enough to accomodate the largest
//                  possible translation
//
//          dblGreenbacks - monetary value to convert.  this is a double floating
//                          point value that should be a whole number, since  
//                          any decimal places implied in the value are derived from
//                          a global PCN parameter.  E.g.- a value of 50025 with
//                          decimal places = 2 would mean 500.25 currency units.
//                          if dblGreenbacks is not a whole number, the fractional
//                          part is ignored.
//
//          bMinWhole - minimum number of whole integer digits to be returned.  this is
//                      used to pad the integer portion of the string with leading
//                      zeroes, if necessary.
//
//          bMinDecimal - minimum number of decimal digits to be returned.  this is
//                          used to pad the decimal portion of the string with trailing
//                          zeroes, if necessary.
//
// RETURNS: length, in words, of the Unicode string
// NOTES:   Trailing zeros in the decimal portion are NOT removed.
//          Decimal point is NOT embedded in the string.
// -------------------------------------------------------------------------------
#if 0 
// Handled by macro in global.h for Mega- cb
// Not used by Janus
unsigned char fnIntMoney2UnicodeFmt( unsigned short *pDest, double dblGreenbacks, 
                                     unsigned char bMinWhole, unsigned char bMinDecimal )
{
    unsigned char retcode;

    retcode =  fnMoney2UnicodeUnv( pDest, dblGreenbacks, bMinWhole, bMinDecimal, 
                                   UTIL_DCML_SHW_NOPOINT, FALSE );
    return( retcode );
}
#endif

//********************************************************************************/
// NAME:  fnMoney2AsciiFmt            
// AUTHOR: BMP , Clarisa Bellamy     
// DESCRIPTION: Translate a 5-char money value to Double.  Then, using the flash 
//              parameters BP_MIN_PRINTED_INTEGERS and BP_MIN_PRINTED_DECIMALS
//              convert the double to unicode, from unicode to ASCII.
//
// INPUTS:      pLen - Ptr to place to store length.
//              pAscii - Ptr to  place to store resulting ascii string
//              pMoney - Ptr to 5-char array that contains the money value to 
//                       convert.
//              fRemPadding - If TRUE, remove any trailing zeros in the decimal
//                      (fractional) portion of the string.
//
// OUTPUT: Return SUCCESSFUL.  Store null-terminated ASCII string at pAscii and 
//                              length of ASCII string in pLen.
//            
// NOTE:  If the resulting string includes a decimal portion, then a decimal 
//          character (WP_DISP_DECIMAL_POINT_CHAR) IS embedded in the string.
// -------------------------------------------------------------------------------
#if 0
// Apparently not used anywhere
unsigned char fnMoney2AsciiFmt(unsigned short *pLen, char * pAscii,unsigned char * pMoney , BOOL bRemPadding )
{
    unsigned char retcode;

    retcode = fnMoney2AsciiUnv( pLen, pAscii, pMoney, UTIL_DCML_SHW_POINT, bRemPadding );

    return( retcode );
}
#endif
//*******************************************************************************/
// NAME:    fnIntMoney2Ascii       
// AUTHOR:  craig defilippo, Clarisa Bellamy     
// DESCRIPTION: Translate a 5-char money value to Double.  Then, using the flash 
//              parameters BP_MIN_PRINTED_INTEGERS and BP_MIN_PRINTED_DECIMALS
//              convert the double to unicode, from unicode to ASCII.
//
// INPUTS:      pLen - Ptr to place to store length.
//              pAscii - Ptr to  place to store resulting ascii string
//              pMoney - Ptr to 5-char array that contains the money value to 
//                       convert.
//
// OUTPUT: Return SUCCESSFUL.  Store null-terminated ASCII string at pAscii and 
//                              length of ASCII string in pLen.
//            
// NOTE:  Trailing zeros in the decimal portion are NOT removed.
//        Decimal point is NOT embedded in the string. 
// -------------------------------------------------------------------------------
#if 0
// Handled by macro in global.h for Mega - cb
// Not used by Janus
unsigned char fnIntMoney2Ascii(unsigned short *pLen, char * pAscii,unsigned char * pMoney )
{
    unsigned char retcode;

    retcode = fnMoney2AsciiUnv( pLen, pAscii, pMoney, UTIL_DCML_SHW_NOPOINT, FALSE );

    return( retcode );
}
#endif

//*******************************************************************************
// NAME:        fnMoney2Ascii       
// AUTHOR:      BMP, Carisa Bellamy  
// DESCRIPTION: Translate a 5-char money value to Double.  Then, using the flash 
//              parameters BP_MIN_PRINTED_INTEGERS and BP_MIN_PRINTED_DECIMALS
//              convert the double to unicode, from unicode to ASCII.
//
// INPUTS:      pLen - Ptr to place to store length.
//              pAscii - Ptr to  place to store resulting ascii string
//              pMoney - Ptr to 5-char array that contains the money value to 
//                       convert.
//
// OUTPUT: Return SUCCESSFUL.  Store null-terminated ASCII string at pAscii and 
//                              length of ASCII string in pLen.
//            
// NOTE:  Trailing zeros in the decimal portion are removed.
//        Decimal point is embedded in the string. 
// -------------------------------------------------------------------------------
#if 0 
// Handled by macro in global.h for Mega - cb
// Not used by Janus
unsigned char fnMoney2Ascii(unsigned short *pLen, char * pAscii,unsigned char * pMoney)
{
    unsigned char retcode;

    retcode = fnMoney2AsciiUnv( pLen, pAscii, pMoney, UTIL_DCML_SHW_POINT, TRUE );

    return( retcode );
}
#endif

#ifdef JANUS
// Replaced by shorter functions above for Mega.

/* *************************************************************************
// FUNCTION NAME: fnMoney2UnicodeFmt
// DESCRIPTION:  Converts a money value to Unicode.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pDest - pointer to the destination unicode string. memory allocated
//                  to the string should be large enough to accomodate the largest
//                  possible translation
//
//          dblGreenbacks - monetary value to convert.  this is a double floating
//                          point value that should be a whole number, since  
//                          any decimal places implied in the value are derived from
//                          a global PCN parameter.  E.g.- a value of 50025 with
//                          decimal places = 2 would mean 500.25 currency units.
//                          if dblGreenbacks is not a whole number, the fractional
//                          part is ignored.
//
//          fShowDecimals - indicates whether or not to include the decimal portion
//                          of the monetary value in the output string.  (If
//                          set to false, Unicode "500" would be returned in
//                          the above example.  If true, "500.25")
//
//          bMinWhole - minimum number of whole integer digits to be returned.  this is
//                      used to pad the integer portion of the string with leading
//                      zeroes, if necessary.
//
//          bMinDecimal - minimum number of decimal digits to be returned.  this is
//                          used to pad the decimal portion of the string with trailing
//                          zeroes, if necessary.
//          bRemPadding - if True trailing zeros are removed
//
// RETURNS: length, in words, of the Unicode string
// *************************************************************************/
unsigned char fnMoney2UnicodeFmt( unsigned short *pDest, double dblGreenbacks, BOOL fShowDecimals,
                 unsigned char bMinWhole, unsigned char bMinDecimal,
                 BOOL bRemPadding)
{
    unsigned char   bMoneyDecimals = 0;
    char            pAsciiMoney[32];
    int             iDecLen, iWholeLen;
    unsigned long   lwDivisor;
    long            i;
    unsigned char   bUniLen;
    double          dblWhole, dblDecimal;
    unsigned short  wUnicodeDecimalChar;


    /* initialize a pointer to the number of decimal places */
    if (fnSYSIsPSDDetected() == TRUE)   /* do not make the bob call if the psd is not available.
                                            it will fail and trigger an meter error when we try
                                            to display the postage field */
        if (fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *) &bMoneyDecimals) != BOB_OK)
        {
            fnProcessSCMError();
            bUniLen = 0;
            goto xit;
        }

    /* calculate 10 to the *pMoneyDecimals power */
    lwDivisor = 1;
    for (i = 0; i < bMoneyDecimals; i++)
        lwDivisor = lwDivisor * 10;

    (void)modf(dblGreenbacks / ((double) lwDivisor), &dblWhole);  /* load the integer part of the value into dblWhole */
    dblDecimal = fmod(dblGreenbacks, (double) lwDivisor);

    iWholeLen = sprintf(pAsciiMoney, "%0.0f", dblWhole);

    /* if sprintf returned a single 0, we change it to a null string instead.  sprintf
        does not allow us to specify a zero precison for doubles, so this is a patch for that.
        if this was not done, then if bMinWhole = 0 the function would not work correctly. */
    if ((iWholeLen == 1) && (pAsciiMoney[0] == '0'))
        iWholeLen = 0;


    /* if the minimum number of whole digits is more than we have from the 
    initial conversion, we need to pad with left side zeros. */
    if (bMinWhole > iWholeLen)
    {
        /* Don't overwrite the array bounds */
        if (bMinWhole > 31)
            bMinWhole = 31;

        /* make some room */
        (void)memmove(pAsciiMoney + (bMinWhole - iWholeLen), pAsciiMoney, iWholeLen);
        /* stick in the zeroes */
        for (i = 0; i < (bMinWhole - iWholeLen); i++)
            pAsciiMoney[i] = '0';
        /* the length has increased */
        iWholeLen = bMinWhole;
    }

    /* stick the whole integer portion into the unicode string */
    fnAscii2Unicode(pAsciiMoney, pDest, iWholeLen);
    bUniLen = (unsigned char) iWholeLen;

    
    if (fShowDecimals == true)
    {
        iDecLen = sprintf(pAsciiMoney, "%0.0f", dblDecimal);

        /* pad on the left side with zeroes to ensure that the value ends up in the
            correct columns... e.g. if moneydecimals is 3 & lwDecimal = 5, it should be
            displayed as .005, so add two left side zeroes */
        if (bMoneyDecimals > iDecLen)
        {   
            /* Don't overwrite array */ 
            if (bMoneyDecimals > 31)
               bMoneyDecimals = 31;

            /* make room */
            (void)memmove( pAsciiMoney + (bMoneyDecimals - iDecLen), pAsciiMoney, iDecLen );
            /* stick in the zeroes */           
            for (i = 0; i < (bMoneyDecimals - iDecLen); i++)
                pAsciiMoney[i] = '0';
            iDecLen = bMoneyDecimals;
        }

        /* trailing zeroes can be removed; e.g. .250 can be shortened to .25, .0000 can be 
            shortened to a null string */
        if(bRemPadding == TRUE)
          {
            i = iDecLen - 1;
            while ((i >= 0) && (pAsciiMoney[i] == '0'))
              {
            iDecLen--;
            i--;
              }
          }


        /* if the minimum number of decimal digits is greater than we currently
            have, pad on the right hand side with zeroes */
        if (bMinDecimal > iDecLen)
        {
            /* Don't overwrite the array bounds */
            if (bMinDecimal > 31)
                bMinDecimal = 31;

            for (i = iDecLen; i < bMinDecimal; i++)
                pAsciiMoney[i] = '0';                           
            iDecLen = bMinDecimal;
        }

        /* if there is at least one decimal digit, add the decimal point character
            followed by the decimal digits to the unicode string */
        if (iDecLen)
        {
            wUnicodeDecimalChar = fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR);
            pDest[iWholeLen] = wUnicodeDecimalChar;  
            bUniLen++;

            // If dblGreenbacks was negative then the sprintf conversion above would
            // have prepended a '-' in front of the decicmal digits in pAsciiMoney. 
            // Get rid of it, we don't want "-ww.-dd" as our end result. 
            if(*pAsciiMoney == '-')
                fnAscii2Unicode(pAsciiMoney+1, (pDest + bUniLen), iDecLen);
            else
                fnAscii2Unicode(pAsciiMoney, (pDest + bUniLen), iDecLen);


            bUniLen += (unsigned char) iDecLen;
        }
    }
xit:
    return (bUniLen);
}

#ifdef JANUS
/**************************************************************************
// FUNCTION NAME: fnIntMoney2UnicodeFmt
// DESCRIPTION:  Converts a money value to Unicode. 
//              NOTE ** NO DECIMAL CHARACTER IS EMBEDDED. **
//
// AUTHOR: Craig DeFilippo
//
// INPUTS:  pDest - pointer to the destination unicode string. memory allocated
//                  to the string should be large enough to accomodate the largest
//                  possible translation
//
//          dblGreenbacks - monetary value to convert.  this is a double floating
//                          point value that should be a whole number, since  
//                          any decimal places implied in the value are derived from
//                          a global PCN parameter.  E.g.- a value of 50025 with
//                          decimal places = 2 would mean 500.25 currency units.
//                          if dblGreenbacks is not a whole number, the fractional
//                          part is ignored.
//
//          fShowDecimals - indicates whether or not to include the decimal portion
//                          of the monetary value in the output string.  (If
//                          set to false, Unicode "500" would be returned in
//                          the above example.  If true, "50025")
//
//          bMinWhole - minimum number of whole integer digits to be returned.  this is
//                      used to pad the integer portion of the string with leading
//                      zeroes, if necessary.
//
//          bMinDecimal - minimum number of decimal digits to be returned.  this is
//                          used to pad the decimal portion of the string with trailing
//                          zeroes, if necessary.
//          bRemPadding - if True trailing zeros are removed
//
//          bNumDecimals - if equal to USE_VAULT_DECIMALS then call bob to get the number of
//                         implied decimals, otherwise use passed value 
//
// RETURNS: length, in words, of the Unicode string
// *************************************************************************/
unsigned char fnIntMoney2UnicodeFmt(unsigned short *pDest, double dblGreenbacks, BOOL fShowDecimals,
                 unsigned char bMinWhole, unsigned char bMinDecimal,
                 BOOL bRemPadding, unsigned char ucNumDecimals)
{
    unsigned char   bMoneyDecimals = 0;
    char            pAsciiMoney[32];
    int             iDecLen, iWholeLen;
    unsigned long   lwDivisor;
    long            i;
    unsigned char   bUniLen;
    double          dblWhole, dblDecimal;
//    unsigned short  wUnicodeDecimalChar;


    /* initialize a pointer to the number of decimal places */
    if (fnSYSIsPSDDetected() == TRUE)   /* do not make the bob call if the psd is not available.
                                            it will fail and trigger an meter error when we try
                                            to display the postage field */
    {

        if (ucNumDecimals == USE_VAULT_DECIMALS)
        {
            if (fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *) &bMoneyDecimals) != BOB_OK)
            {
                fnProcessSCMError();
                bUniLen = 0;
                goto xit;
            }
        }
        else
        {
            bMoneyDecimals = ucNumDecimals;
        }
    }

    /* calculate 10 to the *pMoneyDecimals power */
    lwDivisor = 1;
    for (i = 0; i < bMoneyDecimals; i++)
        lwDivisor = lwDivisor * 10;

    (void)modf(dblGreenbacks / ((double) lwDivisor), &dblWhole);  /* load the integer part of the value into dblWhole */
    dblDecimal = fmod(dblGreenbacks, (double) lwDivisor);

    iWholeLen = sprintf(pAsciiMoney, "%0.0f", dblWhole);

    /* if sprintf returned a single 0, we change it to a null string instead.  sprintf
        does not allow us to specify a zero precison for doubles, so this is a patch for that.
        if this was not done, then if bMinWhole = 0 the function would not work correctly. */
    if ((iWholeLen == 1) && (pAsciiMoney[0] == '0'))
        iWholeLen = 0;


    /* if the minimum number of whole digits is more than we have from the 
    initial conversion, we need to pad with left side zeros. */
    if (bMinWhole > iWholeLen)
    {
        /* Don't overwrite the array bounds */
        if (bMinWhole > 31)
            bMinWhole = 31;

        /* make some room */
        (void)memmove(pAsciiMoney + (bMinWhole - iWholeLen), pAsciiMoney, iWholeLen);
        /* stick in the zeroes */
        for (i = 0; i < (bMinWhole - iWholeLen); i++)
            pAsciiMoney[i] = '0';
        /* the length has increased */
        iWholeLen = bMinWhole;
    }

    /* stick the whole integer portion into the unicode string */
    fnAscii2Unicode(pAsciiMoney, pDest, iWholeLen);
    bUniLen = (unsigned char) iWholeLen;

    
    if (fShowDecimals == true)
    {
        iDecLen = sprintf(pAsciiMoney, "%0.0f", dblDecimal);

        /* pad on the left side with zeroes to ensure that the value ends up in the
            correct columns... e.g. if moneydecimals is 3 & lwDecimal = 5, it should be
            displayed as .005, so add two left side zeroes */
        if (bMoneyDecimals > iDecLen)
        {   
            /* Don't overwrite array */ 
            if (bMoneyDecimals > 31)
               bMoneyDecimals = 31;

            /* make room */
            (void)memmove(pAsciiMoney + (bMoneyDecimals - iDecLen), pAsciiMoney, iDecLen);
            /* stick in the zeroes */           
            for (i = 0; i < (bMoneyDecimals - iDecLen); i++)
                pAsciiMoney[i] = '0';
            iDecLen = bMoneyDecimals;
        }

        /* trailing zeroes can be removed; e.g. .250 can be shortened to .25, .0000 can be 
            shortened to a null string */
        if(bRemPadding == TRUE)
          {
            i = iDecLen - 1;
            while ((i >= 0) && (pAsciiMoney[i] == '0'))
              {
            iDecLen--;
            i--;
              }
          }


        /* if the minimum number of decimal digits is greater than we currently
            have, pad on the right hand side with zeroes */
        if (bMinDecimal > iDecLen)
        {
            /* Don't overwrite the array bounds */
            if (bMinDecimal > 31)
                bMinDecimal = 31;

            for (i = iDecLen; i < bMinDecimal; i++)
                pAsciiMoney[i] = '0';                           
            iDecLen = bMinDecimal;
        }

        /* if there is at least one decimal digit, add the decimal point character
            followed by the decimal digits to the unicode string */
        if (iDecLen)
        {
            //wUnicodeDecimalChar = fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR);
            //pDest[iWholeLen] = wUnicodeDecimalChar;    
            //bUniLen++;

            fnAscii2Unicode(pAsciiMoney, (pDest + bUniLen), iDecLen);

            bUniLen += (unsigned char) iDecLen;
        }
    }
xit:
    return (bUniLen);
}
#endif

/* *************************************************************************
// FUNCTION NAME: fnMoney2Unicode
// DESCRIPTION:  Converts a money value to Unicode.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pDest - pointer to the destination unicode string. memory allocated
//                  to the string should be large enough to accomodate the largest
//                  possible translation
//
//          dblGreenbacks - monetary value to convert.  this is a double floating
//                          point value that should be a whole number, since  
//                          any decimal places implied in the value are derived from
//                          a global PCN parameter.  E.g.- a value of 50025 with
//                          decimal places = 2 would mean 500.25 currency units.
//                          if dblGreenbacks is not a whole number, the fractional
//                          part is ignored.
//
//          fShowDecimals - indicates whether or not to include the decimal portion
//                          of the monetary value in the output string.  (If
//                          set to false, Unicode "500" would be returned in
//                          the above example.  If true, "500.25")
//
//          bMinWhole - minimum number of whole integer digits to be returned.  this is
//                      used to pad the integer portion of the string with leading
//                      zeroes, if necessary.
//
//          bMinDecimal - minimum number of decimal digits to be returned.  this is
//                          used to pad the decimal portion of the string with trailing
//                          zeroes, if necessary.
//
// RETURNS: length, in words, of the Unicode string
// *************************************************************************/
unsigned char fnMoney2Unicode(unsigned short *pDest, double dblGreenbacks, BOOL fShowDecimals,
                                unsigned char bMinWhole, unsigned char bMinDecimal)
{
  return(fnMoney2UnicodeFmt(pDest,dblGreenbacks,fShowDecimals,
             bMinWhole, bMinDecimal , TRUE));
}

/********************************************************************************/
/* NAME:  fnMoney2AsciiFmt                                                      */
/* AUTHOR: BMP                                                                  */
/* INPUT: ptr to Money in FLASH,pDest is empty                                  */
/* DESCRIPTION: Retrieve Money Parameter from Flash wrapper                     */
/*              translate to Double, from Double to unicode, from uncode        */
/*              to ASCII                                                        */
/* OUTPUT: return SUCCESSFUL and pDest, which holds ASCII translation of        */
/*         ptr to MONEY                                                         */
/********************************************************************************/
unsigned char fnMoney2AsciiFmt(unsigned short *pLen, char * pAscii,unsigned char * pMoney , BOOL bRemPadding )
{
    unsigned char retcode = SUCCESSFUL, bMinWhole,bMinDec;
    double dblMoney;
    unsigned short pUnicode[24];

    pUnicode[0] = (unsigned short) NULL;

    /* Translate MONEY to DOUBLE */
    dblMoney = fnBinFive2Double(pMoney);

    /* Get min # of integers and decimals from FLASH */
    bMinWhole = fnFlashGetByteParm(BP_MIN_PRINTED_INTEGERS);
    bMinDec   = fnFlashGetByteParm(BP_MIN_PRINTED_DECIMALS);

    /* Translate MONEY from DOUBLE format to UNICODE  */
    *pLen = fnMoney2UnicodeFmt( pUnicode, dblMoney, true,bMinWhole, bMinDec, bRemPadding);

    /* Now translate UNICODE to ASCII. */
    (void) fnUnicode2Ascii(pUnicode, pAscii, *pLen);

    pAscii[*pLen] = 0 ;

    return(retcode);
}

#ifdef JANUS
/********************************************************************************/
/* NAME:  fnIntMoney2Ascii                                                      */
/* AUTHOR: craig defilippo                                                      */
/* INPUT: ptr to Money in FLASH,pDest is empty                                  */
/* DESCRIPTION: Retrieve Money Parameter from Flash wrapper                     */
/*              translate to Double, from Double to unicode, from uncode        */
/*              to ASCII                                                        */
/* OUTPUT: return SUCCESSFUL and pDest, which holds ASCII translation of        */
/*         ptr to MONEY                                                         */
/* NOTE: ** NO DECIMAL CHARACTER IS EMBEDDED. **                                */
/*       ** NO TRAILING ZEROS ARE REMOVED **                                    */  
/********************************************************************************/
unsigned char fnIntMoney2Ascii(unsigned short *pLen, char * pAscii,unsigned char * pMoney, unsigned char ucNumDecimals)
{
    unsigned char retcode = SUCCESSFUL, bMinWhole,bMinDec;
    double dblMoney;
    unsigned short pUnicode[24];

    pUnicode[0] = (unsigned short) NULL;

    /* Translate MONEY to DOUBLE */
    dblMoney = fnBinFive2Double(pMoney);

    /* Get min # of integers and decimals from FLASH */
    bMinWhole = fnFlashGetByteParm(BP_MIN_PRINTED_INTEGERS);
    bMinDec   = fnFlashGetByteParm(BP_MIN_PRINTED_DECIMALS);

    /* Translate MONEY from DOUBLE format to UNICODE  */
    *pLen = fnIntMoney2UnicodeFmt( pUnicode, dblMoney, true, bMinWhole, bMinDec , FALSE, ucNumDecimals);

    /* Now translate UNICODE to ASCII. */
    (void) fnUnicode2Ascii(pUnicode, pAscii, *pLen);

    pAscii[*pLen] = 0 ;

    return(retcode);
}
#endif

/********************************************************************************/
/* NAME:  fnMoney2Ascii                                                         */
/* AUTHOR: BMP                                                                  */
/* INPUT: ptr to Money in FLASH,pDest is empty                                  */
/* DESCRIPTION: Retrieve Money Parameter from Flash wrapper                     */
/*              translate to Double, from Double to unicode, from uncode        */
/*              to ASCII                                                        */
/* OUTPUT: return SUCCESSFUL and pDest, which holds ASCII translation of        */
/*         ptr to MONEY                                                         */
/********************************************************************************/
unsigned char fnMoney2Ascii(unsigned short *pLen, char * pAscii,unsigned char * pMoney)
{
    unsigned char retcode = SUCCESSFUL, bMinWhole,bMinDec;
    double dblMoney;
    unsigned short pUnicode[24];

    pUnicode[0] = (unsigned short) NULL;

    /* Translate MONEY to DOUBLE */
    dblMoney = fnBinFive2Double(pMoney);

    /* Get min # of integers and decimals from FLASH */
    bMinWhole = fnFlashGetByteParm(BP_MIN_PRINTED_INTEGERS);
    bMinDec   = fnFlashGetByteParm(BP_MIN_PRINTED_DECIMALS);

    /* Translate MONEY from DOUBLE format to UNICODE  */
    *pLen = fnMoney2UnicodeFmt( pUnicode, dblMoney, true,bMinWhole, bMinDec , TRUE);

    /* Now translate UNICODE to ASCII. */
    (void) fnUnicode2Ascii(pUnicode, pAscii, *pLen);

    pAscii[*pLen] = 0 ;

    return(retcode);
}
// End functions replaced by shorter ones for Mega. - cb
#endif

/* *************************************************************************
// FUNCTION NAME: fnAscii2Number
// DESCRIPTION: This function converts ASCII string to numeric 
//               string where numbers are separated by decimal point.  
// AUTHOR: Richard Peng
// 
// For example: "AT%%NC%sE0&K0S10=2X4&C1" is converted to 
//              "65.84.37.37.78.67.37.115.69.48.38.75.48.83.49.48.61.50.88.52.38.67.49"
//
// INPUTS:  pSource - pointer to the source ASCII string.
//          pDest - pointer to the number destination string.
//          bLen - length of the ascii string to convert.
// *************************************************************************/
void fnAscii2Number(char *pSource, char *pDest, unsigned short bLen)
{
    long    i;      
    long    j = 0;
    char    ch = 0;
#ifndef JANUS
    char    chSeparator ;

    // fetch unicode 'decimal' mapping for this UIC configuration
    chSeparator = (char) fnFlashGetWordParm( WP_DISP_DECIMAL_POINT_CHAR );
#endif

    for  (i=0; i < bLen; i++)
    {
        ch = *(pSource + i);                                /*get the ASCII character*/
#ifndef JANUS
        (void)sprintf (&pDest[j], "%d%c", ch, chSeparator);   /*convert and append to pDest*/
#else
        (void)sprintf (&pDest[j], "%d.", ch); /*convert and append to pDest*/
#endif
        j = strlen(pDest);                                  /*get the length of pDest*/
    }
    pDest[j - 1] = 0 ;  /* Null terminator replaces the last period in the string.*/
    
    return;
    
}

/* *************************************************************************
// FUNCTION NAME: fnNumber2Ascii
// DESCRIPTION: This function converts Number string to ASCII string. 
// AUTHOR: Richard Peng
//
//For example: "65.84.37.37.78.67.37.115.69.48.38.75.48.83.49.48.61.50.88.52.38.67.49" 
//              is converted to "AT%%NC%sE0&K0S10=2X4&C1". 
//
// INPUTS:  pSource - pointer to the source number string.
//          pDest - pointer to the ASCII destination string.
//          bLen - length of the number string to convert.
// *************************************************************************/
void fnNumber2Ascii(char *pSource, char *pDest, unsigned short bLen)
{
    char *pToken;
	char *pRest = NULL;
    long i = 0;
#ifdef JANUS
    char chSeps[]   = ".";
#else
    char chSeps[2] ;

    // fetch unicode 'decimal' mapping for this UIC configuration
    // create string for library function call
    chSeps[0] = (char) fnFlashGetWordParm( WP_DISP_DECIMAL_POINT_CHAR );
    chSeps[1] = (char) NULL ;
#endif

   /* Establish string and get the first token: */
   pToken = strtok_r( pSource, chSeps, &pRest);
   while( pToken != NULL )
   {
      /* While there are tokens in "string" */
      *(pDest + i) = atoi(pToken);
      i++;
      /* Get next token: */
      pToken = strtok_r( NULL, chSeps, &pRest);
    }
    
    /*add null to the end*/
    *(pDest + i) = 0;   
    return;
}
 
 

/* *************************************************************************
// FUNCTION NAME: fnMoney2BCD
// DESCRIPTION:  Converts a Money Value to Unpacked BCD
// AUTHOR: Wes Kirschner
//
// INPUTS:  unsigned char *pMoney -> 5 byte binary money
//          Destination Buffer
//
// RETURNS: an array of BCD bytes
// NOTE : Currently does not support decimal places.
// *************************************************************************/
BOOL fnMoney2BCD( const UINT8 *pMoney, UINT8 *pDest, const UINT8 bLen,
                  BOOL fbPackedBCD)
{
    double TempMonetarySum;             /* Stores the money in double format    */
    char MoneyString[32];               /* String version of the money          */
    unsigned char bMoneyLength;         /* Length of the money string           */
    unsigned char bRemainingDigits;     /* Number of digits to pad              */
    char *pStringMoney;                 /* Pointer to the money                 */
    char TempMoneyString[32];           /* Temporary money string               */
    unsigned char Temp[32];             /* Temporary character array            */
    unsigned short wUnpackedLen;        /* Unpacked Byte Length                 */


    /* Store the money in a double */
    TempMonetarySum = fnBinFive2Double((UINT8 *)pMoney) ;

    /* Convert the double into a string */
    (void)sprintf (MoneyString, "%.0f", TempMonetarySum) ;

    /* Get the string length */
    bMoneyLength = strlen (MoneyString) ;

    if (bMoneyLength < bLen)
    {
        /* How many digits are to the right of the decimal point? */
        bRemainingDigits = bLen - bMoneyLength;

        /* pad them in */
        (void)memset (TempMoneyString, '0', bRemainingDigits) ;

        /* Put in the rest of the string */
        (void)strcpy (TempMoneyString + bRemainingDigits, MoneyString);

        /* Set pointer to the right amt of money */
        pStringMoney = &TempMoneyString[0];
    }
    else
    {
        /* Just point to the money string */
        pStringMoney = &MoneyString[0];
    }

    /* Convert the string to Unpacked BCD */
    wUnpackedLen = fnAsciiFloatToUnpackedBcd(Temp, bLen, pStringMoney, 0);

    if (wUnpackedLen == 0xffff)
        return (false);

    if (fbPackedBCD == true)
    {
        /* Convert to packed BCD */
        fnUnpackedBcdToPackedBcd(pDest, Temp, wUnpackedLen);
    }
    else
    {
        (void)memcpy (pDest, Temp, bLen) ;
    }

    return (true) ;

}

#ifndef JANUS
/****************************************************************************
**                                                                          **
**   FUNCTION NAME       :  fnCalculateAccessCode                           **
**   FUNCTION DESCRIPTION:  Takes a peek at the refill type and gets        **
**                          the Access Code.                                **
**   AUTHOR              :  Wes Kirschner                                   **
**   DATE                :  April 21, 1999                                  **
**   PRECONDITIONS       :                                                  **
**   POSTCONDITIONS      :                                                  **
**   UNIT GLOBALS USED   :                                                  **
**   SYSTEM GLOBALS USED :                                                  **
**                                                                          **
****************************************************************************/
BOOL fnCalculateAccessCode (char *pAccessCode)
{
    unsigned char bRefillMethod;

    if (fnValidData(BOBAMAT0, VLT_PURSE_REFILL_METHOD, (void *) &bRefillMethod) == BOB_OK)
    {
        if (bRefillMethod == LONG_REG_RMRS)
        {
            return (fnGetLongAccessCode(pAccessCode));
        }
        else if (bRefillMethod == DEMO_RMRS)
        {
            return (fnGetDemoAccessCode(pAccessCode));
        }
        else
        {
            return (false);
        } 
    }
    else /* Couldn't get Bob information */
    {
        fnProcessSCMError();
        return (false);
    }
}
#endif

/****************************************************************************
**                                                                          **
**   FUNCTION NAME       :  fnRetrieveLastAccessCode                        **
**   FUNCTION DESCRIPTION:  Get the last access code that was calculated    **
**   AUTHOR              :  Wes Kirschner                                   **
**   DATE                :  April 21, 1999                                  **
**   PRECONDITIONS       :                                                  **
**   POSTCONDITIONS      :                                                  **
**   UNIT GLOBALS USED   :                                                  **
**   SYSTEM GLOBALS USED :  pLastAccessCode                                 **
**   NOTE                :  The second argument allows this fcn to be       **
**                          called by the report driver - so the PC Lint    **
**                          warning can be ignored.                         **
****************************************************************************/
unsigned char fnRetrieveLastAccessCode (void *pAccessCode, unsigned short wField)
{
         

    if (fnFlashGetByteParm(BP_ACCESS_CODE_AVAIL))
    {
        if (pLastAccessCode[0] != (char) NULL) 
        {   
            (void)strcpy (pAccessCode, pLastAccessCode);
        }
        else
        {
            return(ERR_GET_ACCESS_CODE);
        }       
    }
    else
        (void)strcpy((char * )pAccessCode, "???????" ); 


    return(SUCCESSFUL);
}

/***************** The following was stolen from FOX O_BCD.C ****************/

/****************************************************************************/
/* FUNCTION NAME         :      PackedBcdToUnpackedBcd                      */
/* FUNCTION DESCRIPTION  :      This routine converts from packed bcd       */
/*                              (2 nibbles / byte) into a binary array      */
/* AUTHOR                :      KDH                                         */
/* DATE                  :      25-Apr-94                                   */
/* CALLING SEQUENCE      :      -                                           */
/* PRECONDITIONS         :      -                                           */
/* POSTCONDITIONS        :      -                                           */
/* UNIT GLOBALS USED     :      -                                           */
/* SYSTEM GLOBALS USED   :      -                                           */
/****************************************************************************/

void fnPackedBcdToUnpackedBcd(unsigned char *pDest, const unsigned char *pSrc, unsigned short srcLen)
{
    while(srcLen != 0)
    {
        *(pDest++) = *pSrc >> 4;
        *(pDest++) = *(pSrc++) & 0x0F;
        srcLen--;
    }
}

/****************************************************************************/
/* FUNCTION NAME         :      PackedBcdToAsciiBcd                         */
/* FUNCTION DESCRIPTION  :      This routine converts from a packed BCD     */
/*                              array (2 nibbles / byte) into an alpha array*/
/* AUTHOR                :      KDH                                         */
/* DATE                  :      25-Apr-94                                   */
/* CALLING SEQUENCE      :      -                                           */
/* PRECONDITIONS         :      -                                           */
/* POSTCONDITIONS        :      -                                           */
/* UNIT GLOBALS USED     :      -                                           */
/* SYSTEM GLOBALS USED   :      -                                           */
/****************************************************************************/

void fnPackedBcdToAsciiBcd(char *pDest, const unsigned char *pSrc, unsigned short srcLen)
{
    while(srcLen-- != 0)
    {
        *(pDest++) = BcdChar[*pSrc >> 4];
        *(pDest++) = BcdChar[*(pSrc++) & 0x0F];
    }
}

/***********************************************************************************************/
/* FUNCTION NAME         :      fnRjPackedBcdToAsciiBcd                                        */
/* FUNCTION DESCRIPTION  :      This routine converts from a right justified packed BCD        */
/*                              array (2 nibbles / byte) into an alpha array                   */
/*                              Ex: size = 5 digits  pSrc = 01 23 45                           */
/*                              returns: 12345 instead of 01234.  Comprende? *******************/
/* AUTHOR                :      WFB                                         */
/* DATE                  :      9-Jun-00                                    */
/* CALLING SEQUENCE      :      -                                           */
/* PRECONDITIONS         :      -                                           */
/* POSTCONDITIONS        :      -                                           */
/* UNIT GLOBALS USED     :      -                                           */
/* SYSTEM GLOBALS USED   :      -                                           */
/****************************************************************************/

void fnRjPackedBcdToAsciiBcd(char *pDest, const unsigned char *pSrc, unsigned short srcLen)
{
    if ((srcLen != 0) && (srcLen % 2)) {
        *(pDest++) = BcdChar[*(pSrc++) & 0x0F];
        srcLen--;
    }
    
    while(srcLen-- != 0)
    {
        *(pDest++) = BcdChar[*pSrc >> 4];
        *(pDest++) = BcdChar[*(pSrc++) & 0x0F];
    }
}

/****************************************************************************/
/* FUNCTION NAME         :      PackedBcdToAsciiFloat                       */
/* FUNCTION DESCRIPTION  :      This routine converts from a packed BCD     */
/*                              array (2 nibbles / byte) into an ASCII      */
/*                              string with embedded decimal point.         */
/* AUTHOR                :      KDH                                         */
/* DATE                  :      08-Jul-94                                   */
/* CALLING SEQUENCE      :      -                                           */
/* PRECONDITIONS         :      -                                           */
/* POSTCONDITIONS        :      -                                           */
/* UNIT GLOBALS USED     :      -                                           */
/* SYSTEM GLOBALS USED   :      -                                           */
/****************************************************************************/

BOOL fnPackedBcdToAsciiFloat(char *pDest, const unsigned char *pSrc,
                    unsigned short srcDig, unsigned short srcDec, BOOL fbAddDecimalPoint)
{
    unsigned char nibble;
    unsigned short realDec;
    
    if (srcDec == 0x0F)
        realDec = 0;
    else
        realDec = srcDec;

    while(srcDig != 0)
    {
        if ((srcDig == realDec) && (fbAddDecimalPoint == true))
        {
            *(pDest++) = '.';
        }

        if (srcDig & 0x01)
        {
            nibble = *(pSrc++) & 0x0F;
        }
        else
        {
            nibble = (*pSrc >> 4) & 0x0F;
        }

        if (nibble >= 10)
        {
            return(false);
        }

        *(pDest++) = BcdChar[nibble];
        srcDig--;
    }

    if ((srcDec == 0) && (fbAddDecimalPoint == true))
        *(pDest++) = '.';

    *pDest = '\0';
    return(true);
}

/****************************************************************************/
/* FUNCTION NAME         :      UnpackedBcdToPackedBcd                      */
/* FUNCTION DESCRIPTION  :      This routine converts from a binary array   */
/*                              into packed bcd (2 nibbles / byte)          */
/* AUTHOR                :      KDH                                         */
/* DATE                  :      25-Apr-94                                   */
/* CALLING SEQUENCE      :      -                                           */
/* PRECONDITIONS         :      -                                           */
/* POSTCONDITIONS        :      -                                           */
/* UNIT GLOBALS USED     :      -                                           */
/* SYSTEM GLOBALS USED   :      -                                           */
/****************************************************************************/

void fnUnpackedBcdToPackedBcd(unsigned char *pDest, const unsigned char *pSrc, unsigned short srcLen)
{
    if (srcLen == 0)
    {
        return;
    }

    if (srcLen & 0x01)
    {
        *(pDest++) = *(pSrc++);
        srcLen--;
    }

    while(srcLen != 0)
    {
        *(pDest++) = (unsigned char)((*pSrc << 4) + (*(pSrc+1) & 0x0F));
        pSrc += 2;
        srcLen -= 2;
    }
}

/****************************************************************************/
/* FUNCTION NAME         :      UnpackedBcdToAsciiBcd                       */
/* FUNCTION DESCRIPTION  :      This routine converts from a binary array   */
/*                              into an alpha array.                        */
/* AUTHOR                :      KDH                                         */
/* DATE                  :      25-Apr-94                                   */
/* CALLING SEQUENCE      :      -                                           */
/* PRECONDITIONS         :      -                                           */
/* POSTCONDITIONS        :      -                                           */
/* UNIT GLOBALS USED     :      -                                           */
/* SYSTEM GLOBALS USED   :      -                                           */
/****************************************************************************/

void fnUnpackedBcdToAsciiBcd(char *pDest, const unsigned char *pSrc, unsigned short srcLen)
{
    while(srcLen-- != 0)
    {
        *(pDest++) = BcdChar[*(pSrc++) & 0x0F];
    }
}

/****************************************************************************/
/* FUNCTION NAME         :      AsciiBcdToUnpackedBcd                       */
/* FUNCTION DESCRIPTION  :      This routine converts from a char array     */
/*                              into a binary array.                        */
/* AUTHOR                :      KDH                                         */
/* DATE                  :      25-Apr-94                                   */
/* CALLING SEQUENCE      :      -                                           */
/* PRECONDITIONS         :      -                                           */
/* POSTCONDITIONS        :      -                                           */
/* UNIT GLOBALS USED     :      -                                           */
/* SYSTEM GLOBALS USED   :      -                                           */
/****************************************************************************/

short fnAsciiBcdToUnpackedBcd(unsigned char *pDest, unsigned short destLen, const char *pSrc, BOOL allowAlpha)
{
    short outputCount;

    outputCount = 0;

    while(*pSrc != '\0')
    {
        if (outputCount >= destLen)
        {
            return(-1);
        }

        switch(*pSrc)
        {
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            *(pDest++) = (unsigned char)(*(pSrc++) - '0');
            break;

        case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
            if (!allowAlpha)
            {
                return(-1);
            }
            *(pDest++) = (unsigned char)(*(pSrc++) - 'A' + 10);
            break;

        case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
            if (!allowAlpha)
            {
                return(-1);
            }
            *(pDest++) = (unsigned char)(*(pSrc++) - 'a' + 10);
            break;

        default:
            return(-1);
        }

        outputCount++;
    }

    return(outputCount);
}

/****************************************************************************/
/* FUNCTION NAME         :      AsciiBcdToPackedBcd                         */
/* FUNCTION DESCRIPTION  :      This routine converts from a char array     */
/*                              into a packed BCD (2 nibbles/byte) array.   */
/* AUTHOR                :      KDH                                         */
/* DATE                  :      25-Apr-94                                   */
/* CALLING SEQUENCE      :      -                                           */
/* PRECONDITIONS         :      -                                           */
/* POSTCONDITIONS        :      -                                           */
/* UNIT GLOBALS USED     :      -                                           */
/* SYSTEM GLOBALS USED   :      -                                           */
/* Modified Date         :  10 April 2000                                   */
/* Modified by           :  Edward J. Greene                                */
/* Modifieds             :  1. Increased bcdBuffer from 16 to 32 bytes      */
/*                             "BCD_BUFFER_SIZE" see above                  */
/*                       :  2. Added clearing of bcdBuffer before use       */
/****************************************************************************/


short fnAsciiBcdToPackedBcd(unsigned char *pDest, unsigned short destLen,
                                const char *pSrc, BOOL allowAlpha)
{
    unsigned char bcdBuffer[ BCD_BUFFER_SIZE ];
    short         unpackLen;
    short         packLen;


    /* Always initialize local Buffer to all Nulls, before using               */
    (void)memset( bcdBuffer, (unsigned char) NULL, BCD_BUFFER_SIZE );
    

    /* Now convert the input ASCII string into a Binary, unpacked BCD, string  */
    unpackLen = fnAsciiBcdToUnpackedBcd( bcdBuffer, BCD_BUFFER_SIZE,
                                            pSrc, allowAlpha);

    /* If unpacked Binary string (Input Buffer) is -1, then conversion error   */
    if (unpackLen <= 0)
    {
        return(unpackLen);
    }

    /* Convert unpacked BCD Digit Length to Packed BCD Digit Length            */
    /*  Always round up for an odd length                                      */
    packLen = (unpackLen + 1) / 2;
    
    /* If Packed BCD Digit Length > ASCII Digit Length, then Error             */
    /*  This should be an Impossible case!                                     */
    if (packLen > destLen)
    {
        return(-1);
    }

    /*Otherwise ready for final conversion of unpacked BCD to Packed BCD Digits*/
    fnUnpackedBcdToPackedBcd(pDest, bcdBuffer, unpackLen);

    return(packLen);  /* Return the final packed BCD Digits Length             */
}

/****************************************************************************/
/* FUNCTION NAME         :      AsciiFloatToUnpackedBcd                     */
/* FUNCTION DESCRIPTION  :      This routine converts from a char array     */
/*                              with embedded decimal point into a binary   */
/*                              array.                                      */
/* AUTHOR                :      KDH                                         */
/* DATE                  :      25-Apr-94                                   */
/* CALLING SEQUENCE      :      -                                           */
/* PRECONDITIONS         :      -                                           */
/* POSTCONDITIONS        :      -                                           */
/* UNIT GLOBALS USED     :      -                                           */
/* SYSTEM GLOBALS USED   :      -                                           */
/****************************************************************************/

short fnAsciiFloatToUnpackedBcd(unsigned char *pDest, unsigned short destLen, const char *pSrc, unsigned char *pNumDec)
{
    short outputCount=0;
    short decimalPlaces=0;

    while(*pSrc != '\0' && *pSrc != '.')
    {
        if (outputCount >= destLen)
        {
            return(-1);
        }

        if (*pSrc < '0' || *pSrc > '9')
        {
            return(-1);
        }

        *(pDest++) = (unsigned char)(*(pSrc++) - '0');
        outputCount++;
    }

    if (*pSrc == '\0')
    {
        return((int)outputCount);
    }

    pSrc++;

    while(*pSrc != '\0')
    {
        if (outputCount >= destLen)
        {
            return(-1);
        }

        if (*pSrc < '0' || *pSrc > '9')
        {
            return(-1);
        }

        *(pDest++) = (unsigned char)(*(pSrc++) - '0');
        outputCount++;
        decimalPlaces++;
    }
    if (pNumDec != 0)
       *pNumDec = decimalPlaces;    
    return(outputCount);
}

/*********************** C6014 Routines *************************************/

/***************************************************************************/
/*                                                                         */
/*                           fnC6014ToMultiByteBin                         */
/* Author: Rick Rudolph                                                    */
/* Description: Convert a C6014 multi bcd number to a multibyte binary     */
/*              number.                                                    */
/***************************************************************************/

unsigned char fnC6014ToMultiByteBin( unsigned char *SourceC6014Bytes, unsigned char *DestBytes, unsigned char NumSourceBytes, 
                            BOOL fMoney ) 
{
//unsigned char BCDByte;
char MoneyString[20];
unsigned char DecimalPlaces = 0;
//unsigned char DigPos;
unsigned short bNibbleLen;
unsigned short bSrcDec;
//unsigned char *pBytePtr;
//unsigned char bByteLen;
unsigned char *EndPtr;
//unsigned short wMeterStatusBytes;
//unsigned char VltMaxDigits[2];
//UNUSED_PARAMETER(pBytePtr);


double TempMonetarySum;
int i;

    bNibbleLen = ( *(SourceC6014Bytes) >> 4 );
//    bByteLen = ( (( *(SourceC6014Bytes) >> 4) / 2 ) + ( ( *(SourceC6014Bytes) >> 4) % 2 ) );
    bSrcDec = (*(SourceC6014Bytes) & 0x0F);
//    pBytePtr = (unsigned char *) ( (SourceC6014Bytes + 1) + (bByteLen - 1) );

    /* Needed for mfg mode */
    if ((fMoney == true) && (bSrcDec == 0x0F))
        bSrcDec = 0;

    fnPackedBcdToAsciiFloat( MoneyString, (SourceC6014Bytes+1), bNibbleLen, bSrcDec, false );
    TempMonetarySum = strtod( MoneyString, (char **)&EndPtr );

    /* Put in the number of decimal places */
    if (fMoney == true) {
        if( fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *)&DecimalPlaces) != BOB_OK)
        {
            fnProcessSCMError();
            return (false);
        }

        if( (bSrcDec == 0xf) )
            bSrcDec = bNibbleLen;
                    
        if( DecimalPlaces > bSrcDec ) {
        /* Put in the decimal point */
            for ( i = 0 ; i < (DecimalPlaces-bSrcDec); i++)
                TempMonetarySum *= 10 ;
            bSrcDec = DecimalPlaces;
        }
    }

    switch( NumSourceBytes ) 
    {
            case 3:
                fnDouble2BinThree( DestBytes, TempMonetarySum );
                break;
                        
            case 4:
                fnDouble2BinFour( DestBytes, TempMonetarySum );
                break;

            case 5:
                /* convert to five byte binary */
                if (fnCheckDoubleRange (TempMonetarySum) == true)
                {
                    fnDouble2BinFive( DestBytes, TempMonetarySum, TRUE);
                    break;
                }
                else
                    return (false);

            default:
                return (false);
    }
    return(true);
}


/***************************************************************************/
/*                                                                         */
/*                           fnMultiByteBinToC6014                         */
/* Author: Rick Rudolph                                                    */
/* Description: Convert a multi-byte binary number to a C6014 multi bcd    */
/*              number.                                                    */
/***************************************************************************/

unsigned char fnMultiByteBinToC6014( unsigned char *SourceBytes, unsigned char *DestC6014Bytes, unsigned char NumBytes, 
                   BOOL fMoney )
{
 double 		TempMonetarySum;                     /* Money that is in five byte format gets converted to a double */
 unsigned char 	DecimalPlacesPtr;          /* Pointer to the decimal places */
 char 			MoneyString[31];
 //char			TempMoneyString[31] ; /* Ascii Float value of the money */
 char bNumDigits;                            /* Number of Digits */
 unsigned long Low4Bytes = 0;                            /* Used for conversions */
 unsigned long High4Bytes = 0;                           /* Used for conversions */
 unsigned short Low2Bytes = 0;                           /* Used for conversions */
 unsigned char TempC6014Buffer[20];                      /* Temporary C6014 Buffer */
 unsigned char DecimalPlaces=0;                          /* Decimal Places */
 unsigned char i;                            /* Loop Control Variable */
 //unsigned char bMinDisplayIntegers;          /* Minimum displayed integers */
 //unsigned char bRemainingDigits;             /* Remaining Digits to display */
 char *pMoney = &MoneyString[0];             /* Pointer to the money */


 switch( NumBytes ) {
        case 3:
            (void)memcpy( &Low2Bytes, (unsigned char *)(SourceBytes + 1), sizeof(unsigned short));
            TempMonetarySum = (double) Low2Bytes;
            TempMonetarySum += (65536.0 * ((double) *SourceBytes));
            break;
                        
        case 4:
            (void)memcpy( &Low4Bytes, (unsigned char *)(SourceBytes), sizeof(unsigned long)); 
            TempMonetarySum = (double) Low4Bytes;
            break;

        case 5:
            TempMonetarySum = fnBinFive2Double(SourceBytes) ;
            break;
            
        case 8:
            (void)memcpy( &Low4Bytes, (unsigned char *)(SourceBytes), sizeof(unsigned long));
            TempMonetarySum = (double) Low4Bytes;
            (void)memcpy( &High4Bytes, (unsigned char *)(SourceBytes+4), sizeof(unsigned long));
            TempMonetarySum += (4294967296.0 * ((double) High4Bytes));
            break;

        default:
            return(0);

    }
    

    /* Put in the number of decimal places */
    if (fMoney == true) {
        if( fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, &DecimalPlacesPtr) ) 
            DecimalPlaces = DecimalPlacesPtr;

        /* Put in the decimal point */
        for ( i = 0 ; i < DecimalPlaces; i++) {
            TempMonetarySum = TempMonetarySum / 10 ;
        }

    }

    /* Get the money string and length */
    (void)sprintf (MoneyString, "%-.*f", DecimalPlaces, TempMonetarySum) ;
    bNumDigits = strlen (MoneyString) ;

    /* One character is the decimal point */
    if (DecimalPlaces > 0)
       bNumDigits--;

    /* May need to zero pad the stuff if dealing with money */
    if (fMoney == true) 
    {
        *DestC6014Bytes = (bNumDigits * 16) + DecimalPlaces;
    }
    else
    {
        *DestC6014Bytes = (bNumDigits * 16) + 0xF;
    }

    /* Go from a string to BCD using the FOX routines */
    fnAsciiFloatToUnpackedBcd(TempC6014Buffer, bNumDigits, pMoney, &DecimalPlaces);
    fnUnpackedBcdToPackedBcd(DestC6014Bytes+1, TempC6014Buffer, bNumDigits);
    
    /* Return the number of bytes, but adjust for an odd number of digits */
    if (bNumDigits & 0x1)
        bNumDigits = (bNumDigits + 1) / 2;
    else
        bNumDigits = bNumDigits / 2;


    return(bNumDigits + 1);
}

/***************************************************************************/
/*                                                                         */
/*                               fnAsciiToC6014                            */
/*                                                                         */
/***************************************************************************/

unsigned char fnAsciiToC6014( unsigned char *AsciiBytes, unsigned char *DestBuff, unsigned char bNumBytes ) {
int i,z;
unsigned char NibbleCount = 0;
unsigned char bByteCount = 0;

    for( i = 0, z = 1; i < bNumBytes; ) {
        if( bNumBytes%2 && z==1 ) DestBuff[z] = (unsigned char) 0 ;
        else {
            if (!(isascii(AsciiBytes[i])))
                return (0);
            DestBuff[z] = ( (AsciiBytes[i++] & 0x0F ) * 16 );                                                                       
        }
        if (!(isascii(AsciiBytes[i])))
            return (0) ;
        DestBuff[z++] += (AsciiBytes[i++] & 0x0F );                                                                     
        ++bByteCount;
        NibbleCount += 2;
    }

    DestBuff[0] = (NibbleCount * 16) + 0x0F ;

    return(bByteCount + 1);
}



/* THE FOLLOWING IS PORTED FROM M_C6014 From the fox program. */

/************************************************************************/
/* FUNCTION NAME         :  cCommMgrBase::fnValidateC6014               */
/* FUNCTION DESCRIPTION  :  This routine checks to make sure a C6014    */
/*                          message is internally and externally        */
/*                          consistant.                                 */
/* AUTHOR                :  KDH                                         */
/* DATE                  :  12-Apr-94                                   */
/* PRECONDITIONS         :  None                                        */
/* POSTCONDITIONS        :  None                                        */
/* UNIT GLOBALS USED     :  None                                        */
/* SYSTEM GLOBALS USED   :  None                                        */
/************************************************************************/

BOOL fnValidateC6014(unsigned char *pMsg, unsigned short nBytes, unsigned char maxDig, unsigned char maxDec, BOOL fDecReq)
{
    unsigned char nDig;
    unsigned char nDec;
    unsigned short i;

    nDig = ((*pMsg) >> 4) & 0x0F;   /* extract # digits from upper nibble */
    nDec = *pMsg & 0x0F;            /* extract # of digits from lower nibble */

    if ((nBytes < 2) ||             /* need at least a format byte and a data byte */
        (nDig == 0) ||              /* zero digits is never valid */
        (fDecReq && nDec != 0x0F))  /* verify nDec is F if F is required */
    {
        return(FALSE);
    }

    if (nDec == 0x0F)               /* once past above, F is the same as 0 */
    {
        nDec = 0;
        if (!fDecReq)               /* if the F isn't required,  */
            *pMsg &= 0xF0;          /* clear the lower nibble in the passed format */
    }

    if ((nBytes != ((nDig+1)/2)+1) ||   /* verify total message length    */
        (nDig > maxDig) ||              /* can't have too many total digits  */
        (nDec > maxDec) ||              /* can't have too many decimals      */
        (nDec > nDig) ||                /* can't have more decimals than digits */
        (nDig-nDec > maxDig-maxDec))    /* can't have too many integer digits   */
    {
        return(FALSE);
    }

    /*  None of the nibbles can be greater than 9  */

    for (i = 1; i < nBytes; i++)
    {
        if (((pMsg[i] & 0xF0) > 0x90) ||
            ((pMsg[i] & 0x0F) > 0x09))
        {
            return(FALSE);
        }
    }

    /*  If nDig is odd, upper nibble of first data byte must be zero */

    if (nDig & 0x01)
    {
        if (pMsg[1] > 0x09)
        {
            return(FALSE);
        }
    }

    return(TRUE);
}


/************************************************************************/
/* FUNCTION NAME         :  cCommMgrBase::fnLongToC6014                 */
/* FUNCTION DESCRIPTION  :  This routine converts a UINT32 value into   */
/*                          a C6014 value, complete with header.        */
/*                          It returns the number of BCD bytes generated*/
/* AUTHOR                :  KDH                                         */
/* DATE                  :  08-Mar-94                                   */
/* PRECONDITIONS         :  None                                        */
/* POSTCONDITIONS        :  None                                        */
/* UNIT GLOBALS USED     :  None                                        */
/* SYSTEM GLOBALS USED   :  None                                        */
/************************************************************************/

#define MAX_LONG_NIB_PAIRS  6

unsigned char fnLongToC6014(unsigned char *pDest, UINT32 val)
{
    unsigned char retval;
    int i;
    unsigned char nibPairs[MAX_LONG_NIB_PAIRS];

    nibPairs[0] = 0;    /* special initialization in case of zero value */

    for (i = 0; i < MAX_LONG_NIB_PAIRS && val != 0; i++)
    {
        nibPairs[i] = (unsigned char)(val % 100);
        val /= 100;
    }

    if (i == 0)         /* special case for value of zero - use the zero */
    {                   /* crammed into nibPairs[0], and pretend there's */
        i = 1;          /* one digit */
    }

    /*  At this point, 'i' contains the number of BCD bytes that will   */
    /*  be generated.  Thus, the last BCD byte is nibPairs[i-1].  That  */
    /*  byte needs to be checked to see if we're generating an even or  */
    /*  odd number of nibbles.                                          */

    if (nibPairs[i-1] >= 10)
    {
        *(pDest++) = (unsigned char)(((i*2) << 4) | 0x0F);
    }
    else
    {
        *(pDest++) = (unsigned char)(((i*2-1) << 4) | 0x0F);
    }

    /*  We output one more than 'i' bytes, because of the format byte   */
    /*  we just put out above.                                          */

    retval = (unsigned char)(i + 1);

    /*  At the beginning of this loop, nibPairs[i-1] is the next byte   */
    /*  that needs to be 'bcd-ized'.  This is why 'i' is decremented    */
    /*  *before* the output byte is generated.                          */

    while(i > 0)
    {
        i--;
        *(pDest++) = (unsigned char)(((nibPairs[i] / 10) << 4) + (nibPairs[i] % 10));
    }

    return(retval);
}

/************************************************************************/
/* FUNCTION NAME         :  cCommMgrBase::fnStringToC6014                   */
/* FUNCTION DESCRIPTION  :  This routine converts a string into a C6014 */
/*                          value, complete with header. It returns     */
/*                          the number of BCD bytes generated.          */
/* AUTHOR                :  KDH                                         */
/* DATE                  :  08-Mar-94                                   */
/* PRECONDITIONS         :  None                                        */
/* POSTCONDITIONS        :  None                                        */
/* UNIT GLOBALS USED     :  None                                        */
/* SYSTEM GLOBALS USED   :  None                                        */
/************************************************************************/

unsigned char fnStringToC6014(unsigned char *pDest, char *pString, BOOL allowHex)
{
    char retval;
    unsigned char temp[MAX_SCALE_VALUE_LEN];
    char isThisOdd;
    
    isThisOdd = strlen(pString);  // we need to know the digit count cause this next function fibs a little on odd lengths.
    retval = (char)(fnAsciiBcdToPackedBcd(temp, MAX_SCALE_VALUE_LEN,
                                        pString, allowHex));
    if (retval <= 0)
    {
        return(0);
    }

    *pDest = (unsigned char)(((retval * 2) << 4) | 0x0F);
//  if ((temp[0] & 0xF0) == 0)      //this was bogus - it messes up on numbers with even digits and leading zero
    if (isThisOdd % 2)
        *pDest -= 0x10;     /* upper nibble of byte is clear so make the */
                            /* total digit count odd in the format byte  */

    pDest++;
    (void)memcpy(pDest, temp, (unsigned char)retval);
    return((unsigned char)(retval+1));
}


/************************************************************************/
/* FUNCTION NAME         :  cCommMgrBase::fnC6014ToAsciiFloat           */
/* FUNCTION DESCRIPTION  :  This routine converts a C6014 value to a    */
/*                          decimal string.                             */
/* AUTHOR                :  KDH                                         */
/* DATE                  :  08-Mar-94                                   */
/* PRECONDITIONS         :  The source data can't contain hex digits.   */
/* POSTCONDITIONS        :  None                                        */
/* UNIT GLOBALS USED     :  None                                        */
/* SYSTEM GLOBALS USED   :  None                                        */
/************************************************************************/

void fnC6014ToAsciiFloat(char *pDest, unsigned char *pSrc)
{
    unsigned char nDig;
    unsigned char nDec;

    nDec = *pSrc & 0x0F;
    nDig = (*pSrc >> 4) & 0x0F;

    pSrc++;
    (void)fnPackedBcdToAsciiFloat(pDest, pSrc, nDig, nDec, true);

}


/*********************** Stolen from N_UTIL.CPP *****************************/

/****************************************************************************
**                                                                          **
**   FUNCTION NAME       :  fnBytesToUnsignedLong                           **
**   FUNCTION DESCRIPTION:  Converts an array of bytes to an integer.       **
**                          First element of the array is LSByte            **
**   AUTHOR              :  Ilya Y. Shnayder                                **
**   DATE                :  February 28, 1994                               **
**   PRECONDITIONS       :  -                                               **
**   POSTCONDITIONS      :  -                                               **
**   UNIT GLOBALS USED   :  -                                               **
**   SYSTEM GLOBALS USED :  -                                               **
**                                                                          **
****************************************************************************/

UINT32 fnBytesToUnsignedLong( const unsigned char *pBuff, unsigned char BuffSz )
{
    register const unsigned char *pByte;
    register UINT32 retVal=0;

    if ( pBuff != NULL )
    {
        if (BuffSz > sizeof(unsigned long))
        {
            pByte = pBuff + sizeof(unsigned long) ;
        }
        else
        {
            pByte = pBuff + BuffSz ;
        }

        while( pByte > pBuff )
        {
            retVal<<=8;
            pByte--;
            retVal += *pByte;
        }
    }
    return(retVal);
}

/****************************************************************************
**                                                                          **
**   FUNCTION NAME       :  fnUnsignedLongToBytes                           **
**   FUNCTION DESCRIPTION:  Converts unsigned long to aray of bytes.  LSByte**
**                          is first element of the array                   **
**   AUTHOR              :  Ilya Y. Shnayder                                **
**   DATE                :  February 28, 1994                               **
**   CALLING SEQUENCE    :  -                                               **
**   PRECONDITIONS       :  -                                               **
**   POSTCONDITIONS      :  -                                               **
**   UNIT GLOBALS USED   :  -                                               **
**   SYSTEM GLOBALS USED :  -                                               **
**                                                                          **
****************************************************************************/

void fnUnsignedLongToBytes( unsigned char *pBuff, unsigned char BuffSz, register UINT32 Value )
{
    unsigned char i ;

    if (pBuff != NULL )
    {
        for( i=0; i < BuffSz; i++ )
        {
            *pBuff=(unsigned char)Value;
            Value >>= 8;
            pBuff++;
        }
    }
}


/****************************************************************************
**                                                                          **
**   FUNCTION NAME       :  fnCalcCRCByte                                   **
**   FUNCTION DESCRIPTION:  Calculates 8 bits CRC                           **
**   AUTHOR              :  Ilya Y. Shnayder                                **
**   DATE                :  February 28, 1994                               **
**   PRECONDITIONS       :  -                                               **
**   POSTCONDITIONS      :  -                                               **
**   UNIT GLOBALS USED   :  -                                               **
**   SYSTEM GLOBALS USED :  -                                               **
**                                                                          **
****************************************************************************/

unsigned char CalcCRCByte( const void *pBuff, unsigned short BuffSize )
{
    register unsigned char remainder = 0xFF;
    register const unsigned char *pByte = (const unsigned char *)pBuff;

    if ( pBuff != NULL )
    {
        for( ; BuffSize > 0; BuffSize--, pByte++ )
            remainder = *pByte ^ ByteCRCTbl[remainder];
    }
    return( remainder );
}

/*********************  Stolen from N_RCHUTIL.CPP ******************************/

/****************************************************************************
**                                                                          **
**   FUNCTION NAME       :  fnPackDig                                       **
**   FUNCTION DESCRIPTION:  Converts array of Digits to PackedDig           **                                  
**   AUTHOR              :  Ilya Y. Shnayder                                **
**   DATE                :  April 12, 1994                                  **
**   PRECONDITIONS       :  Output buffer is large enough to hold           **
**                          CEILING( InDigits / 2 ) bytes.                  **                      
**   POSTCONDITIONS      :  Output buffer is updated.                       **                      
**   UNIT GLOBALS USED   :  -                                               **
**   SYSTEM GLOBALS USED :  -                                               **
**                                                                          **
****************************************************************************/

BOOL fnPackDig( unsigned char *pPackedOut, const unsigned char *pIn, unsigned char InDigits )
{
    if ((pPackedOut == NULL) || (pIn == NULL)) return (false);
   
    /*  If number of input digits is odd, fill MSNibble with 0            */
    if( InDigits & 0x01 )
    {
        if ( *pIn > 0xF ) return (false) ;
        *( pPackedOut++ ) = *( pIn++ );
        InDigits--;
    }

    for( ; InDigits > 0; InDigits -=2, pPackedOut++, pIn++ )
    {
        /*  InDigits is even                                              */
        if (((InDigits & 0x01) != 0 ) || (*pIn > 0xF)) return (false) ;
        *pPackedOut = ((unsigned char )(*pIn << 4));
        pIn++;
        if ( *pIn > 0xF )
           return (false) ;
        *pPackedOut |= *pIn;
    }
    return (true) ;
}

/****************************************************************************
**                                                                          **
**   FUNCTION NAME       :  UnPackDig                                       **
**   FUNCTION DESCRIPTION:  Converts array of PackedDig to Digits           **                                  
**   AUTHOR              :  Ilya Y. Shnayder                                **
**   DATE                :  April 12, 1994                                  **
**   PRECONDITIONS       :  Output buffer is large enough to hold           **
**                          ( InDigits * 2 ) bytes.                         **                  
**   POSTCONDITIONS      :  Output buffer is updated.                       **                      
**   UNIT GLOBALS USED   :  -                                               **
**   SYSTEM GLOBALS USED :  -                                               **
**                                                                          **
****************************************************************************/

BOOL fnUnPackDig( unsigned char *pOut, const unsigned char *pPackedIn, unsigned char InDigits )
{

    if (( pOut==NULL ) || (pPackedIn == NULL)) return (false) ;
    for( ; InDigits > 0; InDigits--, pOut++, pPackedIn++ )
    {
        *pOut=*pPackedIn >> 4;
        pOut++;
        *pOut=*pPackedIn & 0xF;
    }
    return (true);
}



/****************************************************************************
**                                                                          **
**   FUNCTION NAME       :  Str2BCDDig                                      **
**   FUNCTION DESCRIPTION:  Converts char string to a string of BCD digits  **
**                          char string should have the following format:   **
**                          [iiiiii][.][ffff]'\0'.  Returns total number of **
**                          digits in converted BCD string. If number of    **
**                          fractional digits in original string is smaller **
**                          then FracDigs, 0 are appended an the end of the **
**                          converted BCD string to make up the differences.**
**                          If number of fractional digits in original      **
**                          string is larger then FracDigs, or original     **
**                          string length is larger then MaxDigs            **
**                          ( trancation ), the function                    **
**                          returns negated number of converted digits      **
**                          until the error.                                **
**   AUTHOR              :  Ilya Y. Shnayder                                **
**   DATE                :  April 12, 1994                                  **
**   PRECONDITIONS       :  Characters in the string should be in the       **
**                          range '0' - '9' or '.'; string should be '\0'   **
**                          terminated. Size of the output buffer should be **
**                          large enouh to hold MaxDigs bytes.              **
**   POSTCONDITIONS      :  String is converted.                            **
**   UNIT GLOBALS USED   :  -                                               **
**   SYSTEM GLOBALS USED :  -                                               **
**                                                                          **
****************************************************************************/

UINT32 fnStr2BCDDig( unsigned char *pDig, const char *pStr, unsigned char MaxDigs, unsigned char FracDigs )
{
    register int totalCnt;
    register int fracCnt;

    if ((pDig==NULL ) || (pStr == NULL))
       return (0);

    totalCnt = 0;
    fracCnt = 0;

    /*  Convert Integer part of the string                                */
    for( ; *pStr!='\0' && *pStr!='.'; pStr++, pDig++, totalCnt++ )
    {
        if( totalCnt >= MaxDigs )
            return( 0 );
        if( !isdigit( *pStr ) )
            return( 0 );
        *pDig= ( unsigned char )( *pStr - '0' );
    }

    /*  Next, handle decimal point                                        */
    if( *pStr == '.' )
    {
        pStr++;                 /*  Ignore '.'                            */
    
        /*  Now, convert fractional part of the string                        */
        for( ; *pStr!='\0'; pStr++, pDig++, totalCnt++, fracCnt++ )
        {
            if( totalCnt >= MaxDigs )
                return( 0 );
            if( fracCnt >= FracDigs )
                return( 0 );
            if( !isdigit( *pStr ) )
                return( 0 );
            *pDig= ( unsigned char )( *pStr - '0' );
        }
    }

    /*  Now, append zeroes if fracCnt is less then FracDigs               */
    if ( *pStr!='\0' )
       return (0) ;

    for( ; fracCnt < FracDigs; pDig++, totalCnt++, fracCnt++ )
        *pDig = 0;

    return( (UINT32) totalCnt );
}
        
        
/****************************************************************************
**                                                                          **
**   FUNCTION NAME       :  BCDDig2Str                                      **
**   FUNCTION DESCRIPTION:  Converts string of BCD digits to a char string  **
**                          of the followinf format: [iiiiii][.][ffff]'\0'. **
**   AUTHOR              :  Ilya Y. Shnayder                                **
**   DATE                :  April 12, 1994                                  **
**   PRECONDITIONS       :  Character string buffer should be long enough   **
**                          to hold TotalDigs+1 bytes if FracDigs is 0,     **
**                          and TotalDigs+2 bytes otherwise.                **
**                          TotalDigs should be grater or equal then        **
**                          FracDigs.                                       **
**   POSTCONDITIONS      :  String is converted.                            **                  
**   UNIT GLOBALS USED   :  -                                               **
**   SYSTEM GLOBALS USED :  -                                               **
**                                                                          **
****************************************************************************/

BOOL fnBCDDig2Str( char *pStr, const unsigned char *pDig, unsigned char TotalDigs, unsigned char FracDigs )
{
    register long cnt;

    if ( (pDig==NULL ) || (pStr == NULL))
       return (false) ;

    /*  Convert integer part.                                            */
    cnt = TotalDigs - FracDigs;
    for( ; cnt > 0; cnt--, pDig++, pStr++ )
    {
        if ( *pDig >= 10 )
           return (false) ;

        *pStr = ( char )( *pDig+'0' );
    }
    /*  If FracDigs > 0, Convert Fractional part                          */
    if( FracDigs > 0 )
    {
        *pStr = '.';
        pStr++;
        for( cnt = FracDigs; cnt > 0; cnt--, pDig++, pStr++ ) 
        {
            if ( *pDig >= 10 )
               return (false);
            *pStr=( char )( *pDig+'0' );
        }
    }
    *pStr='\0';

    return (true);
}

/******************************************************************************
   FUNCTION NAME: fnBinFive2DoubleAdjustedMoney
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Convert a five byte binary money quantity to a real valued 
                : double.  Uses the vault's decimal significance.
   PARAMETERS   : none
   RETURN       : double money value
   NOTES        : none
******************************************************************************/
double  fnBinFive2DoubleAdjustedMoney(uchar* ptrBinFive)
{
    double  dblMoney = 0;
    uchar   ucDecimals;

    dblMoney = fnBinFive2Double(ptrBinFive);
    if (fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *) &ucDecimals) == BOB_OK)
    {
//      while(ucDecimals--)
//      {
//          dblMoney /= 10.0;
//      }       
        dblMoney /= pow(10,ucDecimals); 
    }

    return(dblMoney);
}

/******************************************************************************
   FUNCTION NAME: fnLong2DoubleAdjustedMoney
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Convert a long money quantity to a real valued 
                : double.  Uses the vault's decimal significance.
   PARAMETERS   : none
   RETURN       : double money value
   NOTES        : none
******************************************************************************/
double  fnLong2DoubleAdjustedMoney(SINT32 lMoney)
{
    double  dblMoney = 0;
    uchar   ucDecimals;

    dblMoney = (double) lMoney;
    if (fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *) &ucDecimals) == BOB_OK)
    {
//      while(ucDecimals--)
//      {
//          dblMoney /= 10.0;
//      }       
        dblMoney /= pow(10,ucDecimals); 
    }

    return(dblMoney);
}

/******************************************************************************
   FUNCTION NAME: fnDouble2DoubleAdjustedMoney
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Convert a double money quantity to a real valued 
                : double.  Uses the vault's decimal significance.
   PARAMETERS   : none
   RETURN       : double money value
   NOTES        : none
******************************************************************************/
double  fnDouble2DoubleAdjustedMoney(double dblMoney)
{
    uchar   ucDecimals;

    if (fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *) &ucDecimals) == BOB_OK)
    {
//      while(ucDecimals--)
//      {
//          dblMoney /= 10.0;
//      }       
        dblMoney /= pow(10,ucDecimals); 
    }

    return(dblMoney);
}

/******************************************************************************
   FUNCTION NAME: fnDoubleAdjustedMoney2Double
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Convert a real value double money quantity to a currency units 
                : double.  Uses the vault's decimal significance.
   PARAMETERS   : none
   RETURN       : double money value
   NOTES        : none
******************************************************************************/
double  fnDoubleAdjustedMoney2Double(double dblMoney)
{
    uchar   ucDecimals;

    if (fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *) &ucDecimals) == BOB_OK)
    {
        while(ucDecimals--)
        {
            dblMoney *= 10.0;
        }       
    }

    return(dblMoney);
}

/*********************************** Ported from N_RCHRG.CPP **************************************/

#ifndef JANUS
/****************************************************************************
**                                                                          **
**   FUNCTION NAME       :  fnGetLongAccessCode                             **
**   FUNCTION DESCRIPTION:  Calculates an Access Code.  This function       **
**                          provides specific implementation of the         **
**                          service for derived class.  For details         **
**                          see base class service.                         **
**   AUTHOR              :  Ilya Y. Shnayder                                **
**   DATE                :  April 11, 1994                                  **
**   PRECONDITIONS       :                                                  **
**   POSTCONDITIONS      :                                                  **
**   UNIT GLOBALS USED   :                                                  **
**   SYSTEM GLOBALS USED :                                                  **
**                                                                          **
****************************************************************************/
BOOL fnGetLongAccessCode( char *pAccessCode )
{
    char i;
    unsigned char AccessCodeBuffer[14];                     /* Access Code Buffer           */
    unsigned char *pAccessCodeString;                       /* Pointer to Access Code String*/
    unsigned char AccessCodeString[10];                     /* Access Code String           */
    unsigned char DescendingRegister[SPARK_MONEY_SIZE] ;    /* Descending Register Value    */
    unsigned char ControlSum[SPARK_MONEY_SIZE] ;            /* Control Sum Value            */
    unsigned short wRechargeCount;                          /* Recharge Count for the Vault */
    unsigned char crcOfAccessCodeBuffer;                    /* CRC of access code buffer    */
    unsigned char PBPSerialNumber[4];                       /* PBP Serial Number            */


    if ( pAccessCode == NULL )
        return (false) ;

    /* Read out the descending register, successful recharge count, serial number, and ControlSum */
    if (fnValidData (BOBAMAT0, GET_VLT_DESCENDING_REG, (void *) DescendingRegister) != BOB_OK)
    {
        fnProcessSCMError();
        return (false) ;
    }

    if (fnValidData (BOBAMAT0, GET_VLT_CONTROL_SUM, (void *) ControlSum) != BOB_OK)
    {
        fnProcessSCMError();
        return (false) ;
    }

    if (fnValidData (BOBAMAT0, VLT_CVA_COMBO_COUNTER, (void *) &wRechargeCount) != BOB_OK)
    {
        fnProcessSCMError();
        return (false) ;
    }
    else    /* Only fifteen attempts per try are done */
        wRechargeCount = wRechargeCount % 0x10;

    if (fnValidData (BOBAMAT0, VLT_INDI_FILE_PBP_SERIAL, (void *) PBPSerialNumber) != BOB_OK)
    {
        fnProcessSCMError();
        return (false) ;
    }

    /*  
    **  convert DR from Money to a string of BCD digits (10 digits in DR)
    **  discard three Least Significant Digits and put seven
    **  remaining digits in Dig0-Dig6 of the AccessCodeString (MSD in Dig0)
    */
    if (fnMoney2BCD ( DescendingRegister, AccessCodeString, 10, false ) == false)
        return (false) ;

    /*                                                                             
    **  put Successful Recharge Count (SRC) in Dig7 of the AccessCodeString
    **  and mask Dig7 of the AccessCodeString with 0x01
    */
    AccessCodeString[7] = wRechargeCount & 0x1;

    /*  
    **  starting with Dig6 of the AccessCodeString, and moving left, check if any of 
    **  the digits is less than 8.  The first digit that is less than 8 is
    **  to be exclusively ored with the three most significant bits of the
    **  SRC.  The check is completed when either a digit is found, or all 
    **  digits in Dig0-Dig6 have been checked
    */
    for( i = 6; i >= 0; i-- )
    {
        if( AccessCodeString[i] < 8 )
        {
            AccessCodeString[i] ^= wRechargeCount >> 1;
            break;
        }
    }
    /*  
    **  convert Dig0-Dig7 of the AccessCodeString to PackedBCD string (4 bytes)
    **  put converted PackedBCD string to Byte0-Byte3 of the AccessCodeBuffer;
    **  convert CS from cBigNum to PackedBCD string (12 digits in CS)
    **  put the control sum in Byte4-Byte9 of the AccessCodeBuffer;
    **  put the Serial Number in Byte10-Byte13 of the AccessCodeBuffer;
    */
    fnPackDig( AccessCodeBuffer, AccessCodeString, 8 );

    if (fnMoney2BCD(ControlSum, AccessCodeBuffer + 4, 12, true ) == false)
        return (false);

    (void)memcpy( AccessCodeBuffer + 10, PBPSerialNumber, 4 );

    /*  
    **  calculate CRC of the AccessCodeBuffer (14 bytes) and check for weak NVM.
    **  If NVM is weak, complement the CRC.
    */

    crcOfAccessCodeBuffer = CalcCRCByte( AccessCodeBuffer, sizeof( AccessCodeBuffer ) );

    /*  
    **  Move the CRC to Dig7-Dig9 of the AccessCodeString in the following way:
    **      CRC bits 7-6 are put in digit 7 bits 2-1
    **      CRC bits 5-3 are put in digit 8 bits 2-0
    **      CRC bits 2-0 are put in digit 9 bits 2-0
    */
    AccessCodeString[7] = AccessCodeString[7] | ( ( crcOfAccessCodeBuffer >> 5 ) & 0x06 );
    AccessCodeString[8] = ( crcOfAccessCodeBuffer >> 3 ) & 0x07;
    AccessCodeString[9] = crcOfAccessCodeBuffer & 0x07;

    /*  
    **  discard leading zeros of the AccessCodeString but must keep at least 3 digits;
    **  convert AccessCodeString from string of BCD digits to string of char
    */
    for( pAccessCodeString = AccessCodeString, i = 10; *pAccessCodeString == 0 && i > 3; pAccessCodeString++, i-- )
        ;

    fnBCDDig2Str ( pAccessCode, pAccessCodeString, i, 0 );

    (void)strcpy (pLastAccessCode, pAccessCode);

    return( true );
}




/****************************************************************************\
**                                                                          **
**   FUNCTION NAME       :  fnGetDemoAccessCode                             **
**   FUNCTION DESCRIPTION:  Calculates an Access Code.  This function       **
**                          provides specific implementation of the         **
**                          service for derived class.  For details         **
**                          see base class service.                         **
**   AUTHOR              :  Ilya Y. Shnayder                                **
**   DATE                :  April 11, 1994                                  **
**   PRECONDITIONS       :                                                  **
**   POSTCONDITIONS      :                                                  **
**   UNIT GLOBALS USED   :                                                  **
**   SYSTEM GLOBALS USED :                                                  **
**                                                                          **
\****************************************************************************/
BOOL fnGetDemoAccessCode ( char *pAccessCode )
{
    int i;
    unsigned char  AccessCodeBuffer[3];                     /* Access Code Buffer               */
    unsigned char *pAccessCodeString;                       /* Pointer to the AccessCodeString  */
    unsigned char AccessCodeString[10];                     /* Access Code String               */
    unsigned char crcOfAccessCodeBuffer;                    /* Access Code buffer CRC           */
    unsigned char ControlSum[SPARK_MONEY_SIZE] ;            /* Control Sum Value                */
    unsigned short wRechargeCount;                          /* Recharge Count for the Vault     */
    unsigned char PBPSerialNumber[4];                       /* PBP Serial Number                */


    if ( pAccessCode == NULL )
       return (false) ;


    /*  Read Control Sum, Successful Recharges, and Serial Number out of Vault.*/
    if (fnValidData (BOBAMAT0, GET_VLT_CONTROL_SUM, (void *) ControlSum) != BOB_OK)
    {
        fnProcessSCMError();
        return (false) ;
    }

    if (fnValidData (BOBAMAT0, VLT_CVA_COMBO_COUNTER, (void *) &wRechargeCount) != BOB_OK)
    {
        fnProcessSCMError();
        return (false) ;
    }
    else    /* Only fifteen attempts per try are done */
        wRechargeCount = wRechargeCount % 0xA;

    if (fnValidData (BOBAMAT0, VLT_INDI_FILE_PBP_SERIAL, (void *) PBPSerialNumber) != BOB_OK)
    {
        fnProcessSCMError();
        return (false) ;
    }

    /*  
    **  convert CS from cBigNum to a string of BCD digits (10 digits in CS)
    **  discard six Least Significant Digits and put four (digits 6-9)
    **  remaining digits in Dig0-Dig3 of the AccessCodeString (MSD in Dig0)
    */

    if (fnMoney2BCD (ControlSum, AccessCodeString, 10, false ) == false)
        return(false);
    /*
    **  put Successful Recharge Count (SRC) in Dig4 and Dig5 of the AccessCodeString;
    **  shift Dig4 one bit right and mask Dig5 of the AccessCodeString with 0x01
    */
    AccessCodeString[4] = wRechargeCount >> 1;
    AccessCodeString[5] = wRechargeCount & 0x1;

    /*
    **  convert Dig0-Dig5 of the AccessCodeString to PackedBCD string (3 bytes)
    **  put converted PackedBCD string to Byte0-Byte2 of the AccessCodeBuffer;
    */
    fnPackDig( AccessCodeBuffer, AccessCodeString, 6 );
    /*
    **  calculate CRC of the AccessCodeBuffer (3 bytes) and check for weak NVM.
    **  If NVM is weak, complement the CRC.
    */
    crcOfAccessCodeBuffer = CalcCRCByte( AccessCodeBuffer, sizeof( AccessCodeBuffer ) );

    /*
    **  Move the CRC to Dig5-Dig7 of the AccessCodeString in the following way:
    **      CRC bits 7-6 are put in digit 5 bits 2-1
    **      CRC bits 5-3 are put in digit 6 bits 2-0
    **      CRC bits 2-0 are put in digit 7 bits 2-0
    */
    AccessCodeString[5] = AccessCodeString[5] | ( ( crcOfAccessCodeBuffer >> 5 ) & 0x06 );
    AccessCodeString[6] = ( crcOfAccessCodeBuffer >> 3 ) & 0x07;
    AccessCodeString[7] = crcOfAccessCodeBuffer & 0x07;

    /*
    **  discard leading zeros of the AccessCodeString but must keep at least 3 digits;
    **  convert AccessCodeString from string of BCD digits to string of char.
    */
    for( pAccessCodeString = AccessCodeString, i = 8; *pAccessCodeString == 0 && i > 3; pAccessCodeString++, i-- )
        ;
    fnBCDDig2Str ( pAccessCode, pAccessCodeString, i, 0 );

    (void)strcpy (pLastAccessCode, pAccessCode);

    return( true );
}
#endif

/* **********************************************************************
// FUNCTION NAME: AreWeSpark2
// DESCRIPTION:  Returns TRUE if running on spark2 hardware
//                  
//
// AUTHOR: George Monroe
//
// INPUTS:  
// **********************************************************************/

BOOL AreWeSpark2()
{
  return(FALSE);
}

/* **********************************************************************
// FUNCTION NAME: fnParseFilename
// PURPOSE:  Takes a filename that is passed in and converts it to a format
//           that has characters.extension.   This allows the other devices to
//           use filenames that have the UIC Filename with an additional period and
//           other data afterwards.
//
// AUTHOR: Wes Kirschner
//
// INPUTS: pFileName - pointer to an ASCII filename
//
// OUTPUTS: modifies the contents of pFileName to be in the new format.
// **********************************************************************/


void fnParseFilename (char *pFileName)
{
    char *p;    /* Character pointer used to traverse the filename */

    if (pFileName != NULL)
    {
        /* Look for the first period */
        p = strchr (pFileName, '.');

        /* If the first period is found..if not, name is OK, no extension. */
        if (p != NULL)
        {
            /* Blow past the first period */
            p = p + 1;

            /* Find the second period */
            p = strchr (p, '.');

            /* If there is a second period...if not, name is OK - no additional stuff */
            if (p != NULL)
            {
                /* Get rid of the rest of the name by stuffing a null character over the period. */
                *p = 0;
            }
          }
     }
}   

/* *************************************************************************
// FUNCTION NAME: UnZipBlock
//
// DESCRIPTION:
//              Decompresses a single compressed array of zlib'ed data.  
//              This function handles just the data in a single block
//
// INPUTS:      pZippedData  - pointer to a compressed data buffer.
//              ZipDataLen   - number of bytes in the compressed data buffer
//              pUnzipBuffer - pointer to buffer for uncompressed data
//              UnzipDataLen - length of uncompressed data buffer.
//              piError      - Pointer to variable to get the return conde of 
//                             inflate operation.
//
// RETURNS:     the the number of bytes that were inflated int the pUnzipBuffer
//              0 on an error.
//
// WARNINGS/NOTES:  
//          None
//
// REVISION HISTORY:
//  Mark Harris  - Initial version
//
// *************************************************************************/
UINT32 UnZipBlock(UINT8 * pZippedData, 
                  UINT32 ZipDataLen, 
                  UINT8 * pUnzipBuffer, 
                  UINT32 UnzipDataLen,
                  int *piError)
{
    int iError;

    z_stream Data;

    Data.next_in = pZippedData;
    Data.avail_in = ZipDataLen;
    Data.total_in = 0;
    Data.next_out = pUnzipBuffer;
    Data.avail_out = UnzipDataLen;
    Data.total_out = 0;
    Data.zalloc = Z_NULL;
    Data.zfree = Z_NULL;
    Data.opaque = ( void *) Z_NULL;
    Data.data_type = Z_BINARY;

    iError = inflateInit( &Data );
    if( iError == 0 )
      {
        iError = inflate( &Data , Z_SYNC_FLUSH );
        (void)inflateEnd(&Data);
      }
    if( iError )
    {
      Data.total_out = 0;
    }
    *piError = iError;
    return( Data.total_out );
}

/* **********************************************************************
// FUNCTION NAME: fnZLIBUncompress
// DESCRIPTION:  Decompresses an array of zlib'ed data.  This function handles
//                  all of the piecemeal block decompression that is required
//                  to inflate an array that was created with the zcomp.exe 
//                  command line utility.
//                  
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pDest- pointer to the location at which to uncompress the data.
//          pDestLen- number of bytes that have been allocated at pDest must
//                      be passed in by reference.  the function will return
//                      the actual number of bytes that were uncompressed
//                      into pDest at this address, provided there was not
//                      an error.
//          pSource- pointer to zlib'ed data that is being uncompressed
//
// RETURNS: the error code returned from the zlib library (0 for success)
// **********************************************************************/
SINT32 fnZLIBUncompress(unsigned char *pDest, UINT32 *pDestLen, const unsigned char *pSource)
{
	UINT32   		lwNumBlocks;
    unsigned char   *pCompressedData;
    UINT32   		lwCompressedBlockSize;
    unsigned char   *pDecompressedData;
    UINT32   		lwDestSpaceAvailable;
    z_stream        rZLIBData;
    long            lRetval = -1;
    UINT32   		lwTotalDecompressedBytes = 0;

    pCompressedData = (unsigned char *) pSource;
    pDecompressedData = pDest;
    lwDestSpaceAvailable = *pDestLen;


    /* extract the number of blocks from the header of the compressed file;
         they are the first 4 bytes, in little-endian format */
    (void)memcpy(&lwNumBlocks, pCompressedData, sizeof(lwNumBlocks));
//    fnSwapBytesForLongs((unsigned char *) &lwNumBlocks);

    pCompressedData += sizeof(lwNumBlocks);


    while (lwNumBlocks && lwDestSpaceAvailable)
    {
        /* the beginning of each block contains its compressed size (4 bytes little endian) */
        (void)memcpy(&lwCompressedBlockSize, pCompressedData, sizeof(lwCompressedBlockSize));
//        fnSwapBytesForLongs((unsigned char *) &lwCompressedBlockSize);
        pCompressedData += sizeof(lwCompressedBlockSize);

        rZLIBData.next_in = pCompressedData;
        rZLIBData.avail_in = lwCompressedBlockSize;
        rZLIBData.next_out = pDecompressedData;
        rZLIBData.avail_out = lwDestSpaceAvailable;
        rZLIBData.total_in = 0;
        rZLIBData.total_out = 0;
        rZLIBData.zalloc = Z_NULL;
        rZLIBData.zfree = Z_NULL;
        rZLIBData.opaque = ( void *) Z_NULL;
        rZLIBData.data_type = Z_BINARY;

        lRetval = inflateInit(&rZLIBData);
        if (lRetval == 0)
        {
            lRetval = inflate(&rZLIBData, Z_SYNC_FLUSH);
            inflateEnd(&rZLIBData);
        }
        if (lRetval)
            break;

        /* adjust pointers and sizes */
        pCompressedData += lwCompressedBlockSize;
        pDecompressedData += rZLIBData.total_out;
        lwDestSpaceAvailable -= rZLIBData.total_out;    /* zlib won't decompress more than will fit, so this 
                                                            can't go negative */
        lwTotalDecompressedBytes += rZLIBData.total_out;
        lwNumBlocks--;
    }

    if (lRetval == 0)
        *pDestLen = lwTotalDecompressedBytes;

    return (lRetval);
}


/* **********************************************************************
// FUNCTION NAME: fnZLIBCalcMaxUnzipSize
// DESCRIPTION:  Calcuates the maximum amount of memory needed to decompress
//                  the zlib'ed data stored at a given location.  This function can be used
//                  to determine the size of a memory block to allocate for
//                  decompression purposes.
//                  
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pCompressedData- pointer to zlib'ed source data
//          lwBlockSize- size, in bytes, of each zlib block.  Example:  if zlib
//              file was created with "zcomp -64 file.bin file.zlb", you would
//              pass in 0x10000 for this parameter
//          
// **********************************************************************/
unsigned long fnZLIBCalcMaxUnzipSize(unsigned char *pCompressedData, unsigned long lwBlockSize)
{
    unsigned long   lwNumBlocks;

    /* extract the number of blocks from the header of the compressed file;
         they are the first 4 bytes, in little-endian format */
    (void)memcpy(&lwNumBlocks, pCompressedData, sizeof(lwNumBlocks));
    fnSwapBytesForLongs((unsigned char *) &lwNumBlocks);
    return (lwNumBlocks * lwBlockSize);
}

/*****************************************************************************/
// FUNCTION NAME: PBUnicodeToUnicode 
// DESCRIPTION:   This function translates the name strings into Japanese code
//                at a time. This translation depends on an EMD parameter from Flash.  
// AUTHOR:  Bob Li
//
// INPUTS:  usConvString,   to be translated .
// RETURNS: None.
// NOTES:
//
// MODIFICATION HISTORY:
//  09-14-05    Bob Li  Create this function according to Lan's design.
/*****************************************************************************/
void PBUnicodeToUnicode(UINT16* usConvString)
{
//    UINT32  ulStrLen = 0;
//    UINT32  j = 0;
//    UINT8   ucCtyCode = 0;
//    ucCtyCode =
    		fnFlashGetByteParm(BP_DEFAULT_RATING_COUNTRY_CODE);
/*
    if (ucCtyCode == JAPAN_COUNTRY_CODE)
    {
        ulStrLen = fnUnicodeLen( (UINT8*)usConvString );
        for( j = 0; j< ulStrLen; j++)
        {
            usConvString[j] =  PBJisToUnicode(usConvString[j]);
        }
     }  
*/
     return;
}

//Sunny merge 10/08
UINT16 PBJisToUnicode( UINT16 PBJis )
{
    if(   (PBJis >= 0x0a1) 
       && (PBJis <= 0xdf) )
    {
        return( PBJis + 0xfec0 );
    }
    else
    {
        return( PBJis );
    }
}

/* *************************************************************************
// FUNCTION NAME:  RawIPToUniIPStr
//
// DESCRIPTION: 
//      convert ip address from 4 byte format to unicode string 
//
// INPUTS:
//      ipadr - pointer to the byte array
//
// OUTPUTS:
//      ipstr - the unicode ip string
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      05/23/2007   Joey Cui   Merged from Mega
// *************************************************************************/
void RawIPToUniIPStr(UINT8 * ipadr, 
                     UINT16 * ipstr, 
                     UINT16 uniStrLen)
{
    char tmpStr[20];
    (void)memset(tmpStr, 0x00, 20);
    (void)sprintf((char *)tmpStr, "%d.%d.%d.%d", (INT)ipadr[0], (INT)ipadr[1], (INT)ipadr[2], (INT)ipadr[3]);

    (void)UTF8ToUnicode(ipstr, uniStrLen, tmpStr, strlen((char *)tmpStr));
}


/* *************************************************************************
// FUNCTION NAME:  UniIPStrToRawIP
//
// DESCRIPTION: 
//      convert ip address from unicode to 4 byte format 
//
// INPUTS:
//      ipstr - the unicode ip string
//
// OUTPUTS:
//      ipadr - pointer to the byte array
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      05/23/2007   Joey Cui   Merged from Mega
// *************************************************************************/
void UniIPStrToRawIP(UINT16 * ipstr, UINT8 * ipadr) 
{
	char tmpStr[20], *ptr;

    (void)UnicodeToUTF8(tmpStr, 20, ipstr, unistrlen(ipstr));   //convert it to utf8
    ipadr[0] 	= (UINT8)atoi((char *)tmpStr);
    ptr 		= (char *)strchr((char *)tmpStr, '.');
    ipadr[1] 	= (UINT8)atoi((char *)++ptr);
    ptr 		= (char *)strchr((char *)ptr, '.');
    ipadr[2] 	= (UINT8)atoi((char *)++ptr);
    ptr 		= (char *)strchr((char *)ptr, '.');
    ipadr[3] 	= (UINT8)atoi((char *)++ptr);
}

// ****************************************************************************
// FUNCTION NAME:   fnConvertInchFractionToMM
//
// DESCRIPTION:     Converts a value in fractions of inches to millimeters or tenths
//                  of millimeters
//                  Primarily used to convert imperial mailpiece dimensions to
//                  metric.
//
// INPUTS:
//      unsigned short *pValue, location containing table of values to be converted
//      bSourceUnits:   INCH1000, INCH100, INCH10; i.e 1/1000, 1/100, 1/10 inch respectively
//      bDestUnits:     MM1, MM10; i.e. Millimeters, 1/10 millimeter respectively
//      usCount:        number of entries in table pValue
//
// RETURNS: Nothing
//
// OUTPUT:
//      returns converted values in table containing original value
//
// NOTES:
// 1.   Mailpiece dimensions in tenths of inches and millimeters expected to fit
//      in an unsigned short.
// ****************************************************************************
 void fnConvertInchFractionToMM(UINT16 *pValue, 
                                UINT16 usSourceUnits, 
                                UINT16 usDestUnits, 
                                UINT16 usCount )
 
//Conversion factor to make input 1/1000 inch
//#define INCH1000  1        // 1/1000 inch
//#define INCH100   10       // 1/100 inch
//#define INCH10    100      // 1/10 inch
//conversion factor to millimeters or tenths of millimeters
//#define MM1       1000    //mm
//#define MM10TH      100     //.1 mm
{
    UINT16  i ;                                 // working variables
    float   flWorkingValue ;

    for( i = 0 ; i < usCount ; i++ )
    {
        // convert number fraction of inch to .001"  * millimeters per inch  / dest factor
        flWorkingValue = (float) (*(pValue+i) * usSourceUnits) * (float) (MM_PER_INCH / usDestUnits) ;

        // integer part of millimeter count
        *(pValue+i) = (UINT16) flWorkingValue ;
    }
}
//end fnConvertInchFractionToMM

// ****************************************************************************
// FUNCTION NAME:   fnSortUShortArray
//
// DESCRIPTION:     Sorts a simple array of unsigned short integer values
//
// INPUTS:
//      unsigned short *pArray, address of array to be sorted
//      unsigned short usCount, length in bytes of array elements
//
// RETURNS: Nothing
//
// OUTPUT:
//      returns array in sorted order
//
// NOTES:
// 1.   Uses bubble sort which is OK for < 100 elements.
//      (Borrowed from oirating.c Country Code sort.)
//
// MODIFICATION HISTORY:
//  11-MAR-06   ebc     Added for Dimensional Rating, V16.00
// ****************************************************************************
void fnSortUShortArray( unsigned short *pArray,
                        unsigned short usCount )
{
    int             i ;                     // loop index
    int             j ;                     // loop index
    unsigned short  usTemp ;                // swap location

    if( usCount > 0 )
    {
        // last-to-first method  : percolate largest to end of array first
        // repeat for usCount-1 passes
        for( i=0 ; i < usCount-1 ; i++ )
        {
            for( j=0 ; j < (usCount-1)-i; j++ )
            {
                if( pArray[j+1] < pArray[j] )
                {
                    usTemp = pArray[j] ;
                    pArray[j] = pArray[j+1] ;
                    pArray[j+1] = usTemp ;
                }
            }
        }
    }

    // else, skip sort on zero count
}
 //end fnSortUShortArray

// ****************************************************************************
// FUNCTION NAME:   fnRemoveUShortDuplicates
//
// DESCRIPTION:     Removes duplicate values from a sorted array of
//                  unsigned short values
//
// INPUTS:
//      unsigned short *pArray, address of array to be sorted
//      unsigned short usCount, length in bytes of array elements
//
// RETURNS:
//      new count, in case any duplicates were removed
//
// OUTPUT:
//      returns modified array if any duplicates eliminated
//
// NOTES:
// 1.   Array must have been previously sorted !!!
//
// MODIFICATION HISTORY:
//  11-MAR-06   ebc     Added for Dimensional Rating, V16.00
// ****************************************************************************
unsigned short fnRemoveUShortDuplicates(unsigned short *pArray,
                                        unsigned short usCount )
{
    int             i ;             // loop index
    int             j ;             // index to overwritten element

    // skip filter on zero count
    if( usCount == 0 )
    {
        return 0 ;
    }

    for( i = 0, j = 1 ; i < usCount - 1 ; i++ )
    {
        if( pArray[i] != pArray[i+1] )
        {
            if( j != i+1 )
            {
                // this element is changing position
                pArray[j] = pArray[i+1] ;
            }
            // in any case : move on to next from/to positions
            j++ ;
        }

        // else a duplicate in the original array has been found : skip it
        // i++ points to next element in original array
            // don't increment "to" position yet
    }

    // j tracked number of values kept in original array
    return (unsigned short) j ;
}
 //end fnRemoveUShortDuplicates

// **********************************************************************
// FUNCTION NAME:       fnAsciiIsItBlank
//
// PURPOSE:     Just checks to see if the first n characters, or all of
//               an ascii string is only blanks.
//
//  AUTHOR:  C.Bellamy  June 1, 2007
//
//  INPUTS:     pString - Pointer to the string to check.
//              ulLen - Length to check.  If 0, check the whole string.
//
//  OUTPUTS:    TRUE if blank, or empty, or null_ptr.
//              FALSE if it finds any non-blank characters in the first
//                      n chars of the string.
//
//  LIMITATIONS:
//
//  NOTES:
//
//---------------------------------------------------------------------
BOOL fnAsciiIsItBlank( const char *pString, UINT32 ulLen )
{
    BOOL    fRetval = TRUE;
    UINT32  i;

    if( pString  )
    {
        if( ulLen == 0 )
        {
            ulLen = strlen( pString );
        }

        for( i = 0; i < ulLen; i++ )
        {
            // If a length was passed in, the string may be shorter.
            if( pString[i] == 0 )
            {
                i = ulLen;
            }
            else if( pString[i] != ' ' )
            {
                fRetval = FALSE;
                i = ulLen;
            }
        }
    }

    return( fRetval );

}  // end  fnAsciiIsItBlank

//****************************************************************************
// FUNCTION NAME:       fnReverseIt
// DESCRIPTION:   
//      Reverse a byte array of length n, in place.
// ARGUMENTS:   
//      pSrc - Pointer to source/destination
//      wSize - size of array.                  
// RETURN:   
//      None
// NOTES:
//
// HISTORY:
//  2008.09.30  Clarisa Bellamy - Move this function from glob2bob.c file to 
//                      utils.c file.
//  2003 - Clarisa Bellamy - Initial   
//-----------------------------------------------------------------------------
void fnReverseIt( UINT8 *pSrc, UINT16 wSize )
{                            
    UINT16  bInx;
    UINT16  wLoopCount = (wSize +1) /2;
    UINT8   ucSave;

    for( bInx = 0; bInx < wLoopCount; bInx++ )
    {
        ucSave = pSrc[ bInx ];
        pSrc[ bInx ] = pSrc[ (wSize -1) -bInx ];
        pSrc[ (wSize -1) -bInx ] = ucSave;
    }
    return;
}

/* *************************************************************************
// FUNCTION NAME: fnUnicode2HexStr
//
// DESCRIPTION: Converts a Unicode string to a Hex String.
//
// AUTHOR: Andy
//
// INPUTS:  pUnicode - pointer to the Unicode string.  
//          pDest - pointer to the Hex string destination. 
//          wLen- Length of the Unicode
//
// MODIFICATION HISTORY:
//      08/07/2008   Joey Cui   Merged from Janus
// *************************************************************************/
void fnUnicode2HexStr( UINT16 *pUnicode, UINT8 *pDest, UINT16 wLen )
{
    UINT16   i,j;
    UINT16  a;
    UINT16  wTemp;
    UINT8   pTemp[5]={0};

    for( i=0; i<wLen; i++ )
    {
        wTemp = pUnicode[i];
        j = 0;
        (void)memset( pTemp, 0, sizeof(pTemp) ); 
        do 
        {
            if( (a = (wTemp % 16)) < 10 ) 
            {
                pTemp[j++] = a + '0';
            }
            else 
            {
                pTemp[j++] = a + '7';
            }
            wTemp /= 16;
        }while( j < 4 );

        fnReverseIt( pTemp, 4 );
        (void)strcat( (char*)pDest, (char*)pTemp );
    }
    return;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnAsciiAddMoneySign
//
// DESCRIPTION:  
//      Adds the currency sign to the beginning or end of a ascii string.
//      The location of the currency sign depends on a flash PCN 
//      parameter.  If the currency sign goes before the value, the value 
//      is shifted to the right to make room for it.  If is goes
//      after the value, it is tacked on immediately to the right
//      of the last value character.
//
// INPUTS:  
//      pMoneyString - pointer to a ascii string.  it must be pre-malloced
//                          to allow room for the currency sign to be added.
//
//      bMoneyLen - Initial length of the ascii string.
//
// RETURNS: 
//      length, in bytes, of the ascii string after the currency sign
//      has been added
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//      by Craig Defilippo based on Joe Mozdzer's unicode version
//      
// *************************************************************************/
UINT8 fnAsciiAddMoneySign(UINT8 *pMoneyString, 
                            UINT8 bMoneyLen)
{
    size_t  bSymbolLen;
    UINT8  *pAsciiCurrencySymbol;
    UINT8   bCurrencySymbolPlacement;


    /* get the currency symbol and location out of flash */
    pAsciiCurrencySymbol = (UINT8 *) fnFlashGetAsciiStringParm(ASP_REPORTS_CURRENCY_SYM); 
    if (pAsciiCurrencySymbol == NULL_PTR)
    {//nothing to do but return
    	return bMoneyLen;
    }

    bSymbolLen = strlen((const char *) pAsciiCurrencySymbol);
    if ((bSymbolLen == 0) || (bSymbolLen > MAX_CURRENCY_LEN))
    {//either no symbol or symbol is too big
    	return bMoneyLen;
    }

    bCurrencySymbolPlacement = fnFlashGetByteParm(BP_CURRENCY_SYM_PLACEMENT);

    /* check if the currency symbol goes before or after the money amount */
    if (bCurrencySymbolPlacement == 0)
    {
        /* Placement = BEFORE */
        /* make room for the symbol by shifting the money string to the right */
        (void)memmove(pMoneyString + bSymbolLen, pMoneyString, (bMoneyLen * sizeof(UINT8)));

        /* copy the symbol into the space we just opened up */
        (void)memcpy(pMoneyString, pAsciiCurrencySymbol, bSymbolLen * sizeof(UINT8));
    }
    else
    {
        /* Placement = AFTER */
        /* copy the symbol into the area immediately after the money string */
        (void)memcpy(pMoneyString + bMoneyLen, pAsciiCurrencySymbol, (bSymbolLen * sizeof(UINT8)));
    }
    
    return (bMoneyLen + bSymbolLen);    
}
/* *************************************************************************
// FUNCTION NAME: fnIsUnicodeHebrew()
// DESCRIPTION: Checks if a Unicode character is a Hebrew char.
// AUTHOR: Jingwei,Li
//
// INPUTS:  bUnicodeChar - the Unicode character
//
// OUTPUTS: true - character is a Hebrew char
//          false - character is not a Hebrew char
// MODIFICATION HISTORY:
//      Jingwei,Li 06/01/2010  Initial version
// *************************************************************************/
BOOL fnIsUnicodeHebrew( UINT16 bUnicodeChar )
{
    BOOL bRet = FALSE;
    if ((bUnicodeChar >= HEBREW_LETTER_ALEF) && (bUnicodeChar <= HEBREW_LETTER_TAV))
    {
        bRet = TRUE;
    }
    else
    {
        bRet = FALSE;
    }
    return bRet;
}

/* *************************************************************************
// FUNCTION NAME: fnBinSix2Double
// DESCRIPTION: Converts a Six byte binary value from unsigned char array format
//              to a double.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pBin5 - pointer to 6 byte unsigned character array containing the
//                  binary value.  the first 2 bytes is the most significant
//
// RETURNS: converted value is returned in a double
//
// *************************************************************************/
double fnBinSix2Double( UINT8 *pBin6 )
{
    double          dblRetVal;
    unsigned long   lwLow4;
    unsigned short	uhigh2;

    //TODO JAH changed to endian copy, but not sure how it will affect other things (void)memcpy(&lwLow4, pBin5 + 1, sizeof(unsigned long));
    EndianAwareCopy(&lwLow4, pBin6 + 2, sizeof(unsigned long));
    dblRetVal = (double) lwLow4;
    EndianAwareCopy(&uhigh2, pBin6, sizeof(unsigned short));
    dblRetVal += (4294967296.0 * uhigh2 );

    return( (double)dblRetVal );
}

/* *************************************************************************
// FUNCTION NAME: fnTrim
// DESCRIPTION: returns a pointer to the beginning of a string after
//              removing trailing spaces and tabs
// AUTHOR: John Wronski
//
// INPUTS:  strData - pointer to a null terminated string
//
// RETURNS: pointer to the beginning of the string after leading spaces and tabs
//
// *************************************************************************/
char* fnTrim(char* strData)
{
	int i;
	int len = strlen(strData);
	char* ptr = strData;

	if(len < 0xFFFF)
	{
		//shorten the end of the string with zeros
		for(i=len-1; i>0; i--)
		{
			if((*(ptr+i) == ' ') || (*(ptr+i) == '\t'))
					*(ptr+i) = 0;
			else
				break;
		}

		//move the start of the string to the first char
		for(i=0; i<len; i++)
		{
			if((*ptr == ' ') || (*ptr == '\t'))
					ptr++;
			else
				break;
		}
	}
	return ptr;
}

/* *************************************************************************
// FUNCTION NAME: fnTrim
// DESCRIPTION: copies all chars from the source buffer to the dest buffer
//              except the specified character
//
// INPUTS:  pDest - the buffer to copy to
//          pSource - the data to copy from
//          c - the character to be removed
//
// RETURNS: void
//
// *************************************************************************/
void fnRemoveCharFromString(unsigned char *pDest, unsigned char *pSource, char c)
{
    // Strip out the char from source copy to the destination...
    while (*pSource)
    {
        if (*pSource != c)
            *(pDest++) = *pSource;       // Copy everything except  c
        ++pSource;
    }
    *pDest = '\0';
}

/* *************************************************************************
// FUNCTION NAME: fnGetSubString
// DESCRIPTION: copy the buffer starting with the first start char and
//              continue up to and including the specified end char
//
// INPUTS:  pDest - the buffer to copy to
//          pSource - the data to copy from
//          startChar - the search start character
//          endChar - the search end character
//          maxLength - the max chars to search
//          startOccurrence - the occurrence of when to start coping the chars
//          endOccurrence - the occurrence of when to stop coping the chars
//
// RETURNS: one if success, zero if not
//
// *************************************************************************/
UINT32 fnGetSubString(unsigned char *pDest, unsigned char *pSource, char startChar, char endChar,
					int maxLength, int startOccurrence, int endOccurrence)
{
	UINT32 result = 0;
	int count = 0;
	int flag = 0;
    *pDest = '\0';

    if(startChar == '\0')
    	flag++;
    while (*pSource)
    {
        if ((*pSource == startChar) && (*(pSource+1) != '?'))
        {
        	startOccurrence--;
        	if(startOccurrence < 1)
        		flag++;
        }
    	if(flag > 0)
    	{
    		*(pDest++) = *pSource;
    		if (*pSource == endChar)
    		{
    			endOccurrence--;
    			if(endOccurrence < 1)
    			{
    				result = 1;
    				break;
    			}
    		}
    	}
        else if(count >= maxLength)
        	break;
        else if (*pSource == '\0')
        	break;
        ++pSource;
        count++;
    }
    *pDest = '\0';
    return result;
}

/* *************************************************************************
// FUNCTION NAME: fnAsciiStr2Uint
// DESCRIPTION: This function converts ASCII string to uint32_t
// *************************************************************************/
void fnAsciiStr2Uint(char *pSource, uint32_t *pDest, unsigned short bLen)
{
    long    i;
    uint32_t data = 0;


    for  (i=0; i < bLen; i++)
    {
    	data = ((*(pSource + i)) - '0') << (i*8);                                /*get the ASCII character*/
    }

    *pDest = data;

    return;

}

char gInfoStr[17];

//---------------------------------------------------------------------
//	MakeIntoAscii
//---------------------------------------------------------------------
char *MakeIntoAscii( uchar *pData, uchar type )
{
    uchar   x, j, i = 0;
    char    d;

    (void)memset( gInfoStr, 0, sizeof( gInfoStr ) );

    switch (type)
    {
        case EIGHT_BYTES_INTO_ASCII:
            j = 8;
            break;

        case FOUR_GVER_BYTES_INTO_ASCII:
            j = 4;
            pData += 8;
            break;

        case FIVE_BYTES_INTO_ASCII:
            j = 5;
            break;

        case THREE_BYTES_INTO_ASCII:
            j = 3;
            break;

        default:
            j = 0;
            break;
    }

    for (x = 0; x < j; x++)
    {
        d = (char)(*pData / 16);
        if (d < 10)
            gInfoStr[i++] = d + 0x30;
        else
            gInfoStr[i++] = d + 0x57;

        d = (char)(*pData & 0x0f);
        if (d < 10)
            gInfoStr[i++] = d + 0x30;
        else
            gInfoStr[i++] = d + 0x57;

        pData++;
    }

    return (gInfoStr);
}


//---------------------------------------------------------------------
//	HexToAscii
//		Inputs - number, 5 char buffer for output
//---------------------------------------------------------------------
char *HexToAscii( ushort hexVal, char *fCode )
{
    int i, v, hn;


    for (i = 0, v = 1000; i < 4; i++, v /= 10)
    {
        hn = hexVal / v;
        fCode[i] = (char)hn + 0x30;
        hexVal -= (unsigned short)(hn * v);
    }

    fCode[4] = 0;
    return (fCode);
}

/*********************************************************************************
 * Function:	UnicodeToUTF8
 *				Converts Unicode string to UTF8 encoded byte stream
 * Input:		pUTF8Buffer - UTF8 buffer
 *				usUTF8BufLen - Length of UTF8 buffer
 *				pUnicodeBuffer - Buffer w/ unicode string
 *				usUnicodeCharCnt - Length of unicode string
 *
 * Returns:		Number of characters converted
 **********************************************************************************/
 unsigned short UnicodeToUTF8(char *pUTF8Buffer, unsigned short usUTF8BufLen, 
	                 unichar *pUnicodeBuffer, unsigned short usUnicodeCharCnt)
{
    unsigned short i, j;
    unsigned short UnicodeChar;
    j = 0;
    
    if(usUnicodeCharCnt == 0 || usUTF8BufLen == 0)
        return 0;
    
    memset(pUTF8Buffer,0,usUTF8BufLen);

    for( i = 0; i < usUnicodeCharCnt; i++, pUnicodeBuffer++)
    {
        //UnicodeChar = PBUnicodeToUnicode(*pUnicodeBuffer);    //JY TEMP. Same function name in global.h & utils.c
        UnicodeChar = *pUnicodeBuffer; 
        if(UnicodeChar < 0x80 )
        {
            if((j + 1) < usUTF8BufLen)          // Check for and prevent overrun
            {
                *pUTF8Buffer++ = (UnicodeChar & 0x7F);
                j++;
            }
            else
                break;
        }
        else if(UnicodeChar < 0x800)
        {
            if((j + 2) < usUTF8BufLen)          // Check for and prevent overrun
            {
                *pUTF8Buffer++ = 0xC0 + ((UnicodeChar & 0x7C0) >> 6);
                j++;
                *pUTF8Buffer++ = 0x80 +  (UnicodeChar & 0x03F);
                j++;
            }
            else
                break;
        }
        else if ( (UnicodeChar >= 0x800 && UnicodeChar < 0xD800) ||
                  (UnicodeChar >= 0xE000))                      // && UnicodeChar <= 0xFFFF) )
        {
            if((j + 3) < usUTF8BufLen)          // Check for and prevent overrun
            {
                *pUTF8Buffer++ = 0xE0 + ((UnicodeChar & 0xF000) >> 12);
                j++;
                *pUTF8Buffer++ = 0x80 +  ((UnicodeChar & 0x0FC0) >> 6);
                j++;
                *pUTF8Buffer++ = 0x80 +  (UnicodeChar & 0x003F);
                j++;
            }
            else
                break;

        }
        else
        {
            // Do We Need Surrogates???
            return 0;
        }
    }
    return j;
}


#define NUM_ESCAPE_CHARS 5

static const struct {
	unsigned char *pName;
	unsigned char xChar;
} EscapeSeq[] = {
	{(unsigned char *)"&amp;", 0x26},
	{(unsigned char *)"&lt;", 0x3C},
	{(unsigned char *)"&gt;", 0x3E},
	{(unsigned char *)"&apos;", 0x27},
	{(unsigned char *)"&quot;", 0x22}
};

//static short fnFormatterSendParsedCharData(XMLFormatter *p, XmlChar *pData, unsigned short wDataLen)
short fnXmlConvertEscapeCharInString(char *pOutString, unsigned short *wOutLen, char *pInString, unsigned short wInLen)
{
	short iResult = SUCCESS;
	unsigned short		i,j,k,l;
	int iEscape = 0;

	/* for each character */
	l = 0;
	for (i=0; i < wInLen && iResult == SUCCESS; i++)
	{
		if(l >= *wOutLen)
		{
			iResult = -1;
			break;
		}

		/* check to see if it is markup */
		iEscape = 0;
		j = 0;
		while (j < NUM_ESCAPE_CHARS && iEscape == 0)
		{
			if (pInString[i] == EscapeSeq[j].xChar)
				iEscape = 1;
			else
				j++;
		}
		if (iEscape == 1 && j < NUM_ESCAPE_CHARS)
		{
			/* the character is markup, sends its
			escape sequence. */
			k = 0;
			while (EscapeSeq[j].pName[k] != 0 && iResult == SUCCESS)
			{
				if(l >= *wOutLen)
				{
					iResult = -2;
					break;
				}
				pOutString[l] = EscapeSeq[j].pName[k];
				k++;
				l++;
			}
		}
		else
		{
			if(l >= *wOutLen)
			{
				iResult = -3;
				break;
			}
			/* the character is NOT markup, just output it */
			pOutString[l] = pInString[i];
			l++;
		}
	}

	*wOutLen = l;

	return iResult;
}


