/************************************************************************
 PROJECT:        Future Phoenix
 COPYRIGHT:      2005, Pitney Bowes, Inc.
 AUTHOR:         
 MODULE NAME:    OiFunctfp.c

 DESCRIPTION:    Contains global variable definitions created by the
                 screen generation tool.

 ----------------------------------------------------------------------
 MODIFICATION HISTORY:
*	
* 02-May-15 sa002pe on FPHX 02.12 shelton branch
	Changed the screen versions to 02.12.31.

*	For Enh Req 229336:
*	1. Changed EServiceConfirmationOptions to add an item for displaying the number of tracking IDs left in the meter.
*	2. Added screen EServiceTrackingIDsRemaining & function fnfDisplayRemainingTrackingIDs.
*
*	For Fraca 229143: Changed OptionCorrectDrift so the "<Accept" field is filled in
*	after the field w/ the drift time. 
	
 24-Mar-15 sa002pe on FPHX 02.12 shelton branch
 	For CMOS version 0x293:
	Added usBarcodeWarning to the GTS_CUST_SETUP so the warning value can
	be 499 for Brazil, so had to change SetupTrackingBarcodeWarning so the
	entered value can be 3 digits and so the max value can be variable.

  15-JAN-2015 Renhao on FPHX 02.12 cienet branch
    Added function fnhkWithdrawalClearHandler() for fraca 222172.
    
 13-JAN-2015 Renhao on FPHX 02.12 cienet branch
    Added function fnhkFormatClearHandler() for Enh Req 223064.

 23-May-2014 Rong Junyu on FPHX 02.12 cienet branch
    Added function fnhkUpdateFromEntryBufferOnEditScreen() to fix fraca 225258.

 21-Apr-14 sa002pe on FPHX 02.12 shelton branch
 	Changed the screen versions to 02.12.21.

	Changed ServModeExtMemFormat to add a warning about how formatting
	the memory stick will turn off accounting.

 	For Enh Req 224995:
	1. Changed PmcDiagPrintTestPat, PmcDiagPrintTestPatError, PmcMaintPurge,
		 PmcMaintPurgeComplete to:
		a. Remove all the function keys and the Sleep event so we
			have some control over when the test print cycle is exited.
	2. Changed TestPrintPatAcceptable to
		a. Remove all the function keys and the Sleep event so we
			have some control over when the test print cycle is exited.
		b. Call fnhkExitTestPrint when "Yes" is selected.

 10-Apr-14 sa002pe on FPHX 02.12 shelton branch
 	Changed PBPDcapUploadDue to be able to also show the next required time when appropriate.

 07-Apr-14 sa002pe on FPHX 02.12 shelton branch
 	Changed the screen versions to 02.12.20.
	
	Merged in the following changes for U.K. EIB from Janus:
	1. Changed PBPDcapUploadComplete to be able to also show the next upload time when
		appropriate. Also to have the date field be right-justified instead of left-justified
		now that the time might be shown.

 03-April-14 Renhao on FHHX 02.12 cienet branch
    Modified screen RateErrRMNotInitialized, added screen ErrAdvanceManual,ErrRatesExpire
    for G922 Rates Expiration new feature.

 21-Mar-14 Wang Biao on FHHX 02.12 cienet branch
     Modified screen HostedAbacusNetworkProxyPortEdit to fix fraca 224729.

 19-Mar-14 sa002pe on FPHX 02.12 shelton branch
 	Changed the screen versions to 02.12.10.
	
 	For Enh Req 222165:
	1. Added ServModeCMOSDate & ServModePSDDate screens.
	2. Changed ServModeSystemInformation screen to have the
		"PSD Manufacture Date" & "CMOS Manufacture Date" options.
 
 10-Mar-14 sa002pe on FPHX 02.12 shelton branch
 	Related to Janus Fraca 224510:
	1. Added AbacusAccountingDisabled screen and the
		fnfInViewDisableError & fnhkClearAbacusProblem functions.

 09-Jan-14 Wang Biao on FPHX 02.12 cienet branch
    Created below screens to support Network Proxy Settings.
           HostedAbacusProxySettings
           HostedAbacusProxyHttpSettings
           HostedAbacusNetworkProxyAddrEdit
           HostedAbacusNetworkProxyAddrEdit2
           HostedAbacusNetworkProxyPortEdit
           HostedAbacusNetworkProxyPwdEdit
           HostedAbacusNetworkProxyUserEdit
           HostedAbacusNetworkProxyUserEdit2
           HostedAbacusProxyExclusionSettings

 18-Mar-13 sa002pe on FPHX 02.10 shelton branch
	Changed the screen tree version to 02.10.50.
	
 	For Janus Fraca 219851: Changed AbacusMXEnterMetricWeight to export the screen ID.
	
	For Fraca 219088:
	1. Changed InfraInstalling screen to handle the new EVENT_UNPACKING_ARCHIVE_COMPLETE event.
	2. Changed PowerOn screen to handle the new EVENT_ARCHIVE_NEEDS_UNPACKING event.
	
	For Enh Req 220395:
	1. Changed ServModeMainDiag to have a "Logging Features" option.
	2. Added ServDiagLogging screen.

 24-Feb-13 sa002pe on FPHX 02.10 integration branch
 	Fraca 206539: On the EServiceSetting screen, center the "Display Following During" string.
 
 	Merging in changes from the 01.10 branch for Brazil that didn't get merged back to the main branch.
----
	Per Clarisa: Changed MMEnterPostage to add the fnpEnterAmountTapePre pre-function to make
		sure the postage value is valid before allowing the operator to enter the number of tapes.
		The related changes in oipostage.c were done for Brazil, but she never got around to adding
		the pre-function to the screen.
 
	22-Jun-09 xuefei.xu on FPHX 01.10 cienet branch
		To fix fraca 164131: a misspell in the screen PBPBalancesSuccessful, "reciept" ---> "receipt".
	
	12-May-09 jingwei.li & yueming.shen on FPHX 01.10 cienet branch
		Added support for Balance Inquiry report/receipt for Brazil/France.
		Added PBPBalancesSuccessful screen, which uses fnhkOnlinePrintPbPInquiryReceipt function.
		Changed InfraContactPB screen to add PBPBalancesSuccessful screen to the end of the list of
			next screens for processing the message from the Distributor task.
		Changed EServicePrintManifestOnline, ReportAttachedPrinterMsg, ReportPHPrintError,
			ReportPHPrinting, ReportPHPrintReady, ReportPHTapePrinting, ReportPrepareNextGroup and
			ReportPrepareNextTapeGroup to add strings to the top two fields for use by
			fnfReportOption1 and fnfReportOption2 field functions.
		Changed ReportMenu to add a string for the Balance Inquiry Report to the menu option fields,
			and to add a new hard key operation for the report which uses fnhkReportBalanceInquiry.
----

 18-Feb-13 sa002pe on FPHX 02.10 integration branch
	Enh Req 219480:
	1. Changed ServModeNonPBApprovedManufacturer so the warning message is done w/
		field data instead of fixed text so the various text variations can be handled
		by the screen based on a new EMD parameter.

	Enh Req 218816:
	1. Changed DistributorParametersEdit to have an option to clear the entire string.

  2012.10.26  Jane Li  - FPHX 2.10 cienet branch
   - Changed the screen PmcDiagTestPatPrinting to fix the fraca 112676: 
     1. Added new event function fnevTestPatternPrinted() for the event EVENT_MAIL_JOB_COMPLETED
     2. Added the screen PmcDiagPrintTestPat to next screen list for the event EVENT_MAIL_JOB_COMPLETED

  2012.10.18  John Gao  - FPHX 2.10 cienet branch
   - Added new field function fnfScaleVibrationChangeTo() to display the scale stabilizer option
    to make sure the current setting is stable.
    
  2012.10.16  Bob Li - FPHX 2.10
   - Added function fnhkSetPlatformfilter(), and fnpPrepPlatformfilter() for fixing fraca 218621.
   - Changed the screen version to 2.10.30.

  CL501BE Screen Change Description   2012.10.02   FPHX 02.10
	Change screen version to 2.10.30.
	Change the ServDiagFeeder screen to allow testing of different feeder speeds in the new profile. 

	- For each of the 4 menu slot lines, add two new lines to the Field Text table (1-based index) : 
	       	[12] =  "Set Thruput Test" 
		[13] = "Clear Thruput Test" . 
	- For the status line (with function fnfFeederDiagStatus), add 2 new lines to the Field Text (1-based index) : 
		[3] = "Thruput Test Off" 
		[4] = "Thruput Test "
	- Added 2 new hardkey entries:
	  Key:                    B                                  C
	  State:                  Down                            Down
	  Function:             fnhkSetThruputTest        fnhkClearThruputTest    
	  Format:               2                                   2
	  Operation:            0-No operation                0-No operation
	  NumNextScrns:     1                                    1
	   NextScreens:      ServDiagFeeder             ServDiagFeeder     

	NOTES: 
	1. Corresponding changes are made in file FPHX_SADev\fpapp\source\oidiagnostic.c
	2. Code released to the field must NOT allow this functionality!!
	This new functionality will only show up when the macro ALLOW_THRUPUT_TEST is defined.
	This is located in file cmpublic.h.  
    
  2012.09.21  Bob Li - FPHX 2.10
   - Added function fnfSMenuNumOrLabelSlot1-4() for fixing fraca 118645.
   - Added function fnhkToggleScaleVibration(),  fnfScaleVibrationStatus() 
   		for Enh Req 196699 Scale stabilizer changes.
   - Added fnpPrepPlatCalibWait, fnpPrepPlatCalibWait2, and fnpPostPlatCalibWait 
   		for fixing fraca 218057.
   - Added fnfPlatformVersion, and fnfScaleSerial for Enh Req 190651.
   - Changed the screen version to 2.10.20.

  2012.09.11  Jonh   - FPHX 2.10
   - Add event function fnevOOBPlatformInstall to detect if W&M scale is attached.
  2012.08.14  Bob Li - FPHX 2.10
   - Added two new screen ServModeNonPBApprovedManufacturer and ServModePBApprovedManufacturer
      for supporting the ink tank type checking.   	
  2011.08.16  Clarisa Bellamy - FPHX 2.7 - Fraca fixes for G910, release 02.07.20:
   - On screen RatePromptValue, add prefunction fnpEnterRatingValue. 
   - On screen EserviceEditModeZipEntry, add 1 more entry to HardKey list:  
     All Keyboard + Shift, exactly the same as the All Keyboard  (without the shift) entry.
   - On screen MMPostageCorrectionEnterAmount, add screen ErrInsufficientFunds to the 
     Next Screens List for Soft4 key, because that key is supposed to act the same as 
     the Enter key on this screen. 
   - On screen ServModeExtMemoryUtil, add the Format2 function fnhkServExtMemCheck, 
     and the Next Screens List, consisting of one screen, ServModeExtMemeoryUtil, 
     to the Soft2 key.
   - Updated versions to 2.7.20.

    06/30/2011      Joe Zhang
    Adapted from Janus 16.80 to fix double refills issue
    Added a new screen InfraCallErrGeneralRefillSuccess
    
    09/19/2008      Bob Li      Added new function fnhkProcessManifestLimitReached to fix fraca 148781.
    07/24/2008      Bob Li      Added new function fnfPresetRatingStatus and fnhkTogglePresetRating for
                                screen OptionPresetRatingToggle.
    06/05/2008      Bob Li      Added some new functions for screen RateSelectRvKIPClass.
    04/26/2007      Created new screens to support Date correction and Manifest modes.
    04/25/2007      Added field function fnfInitPostageCorrectionAmount() for MMPostageCorrectionEnterAmount
    04/19/2007      Andy Mo     Created below screens to support Postage Correction
                                MMPostageCorrectionReady
                                MMPostageCorrectionError
                                MMPostageCorrectionEnterAmount
                                MMPostageCorrectionPrinting
                                MMPostageCorrectionTapePrinting
                                MMPostageCorrectionPrintingComplete

*   07/07/2006      Dicky Sun
*   Include oigts.h for GTS support. 

*   03/23/2006      Raymond Shen
*   Added some field functions for system information screen. 
*
*   03/21/2006      Raymond Shen
*   Added some functions for Feature Code. 
*
*   12/27/2005      Vincent Yi      Initial version

    OLD PVCS REVISION HISTORY
* 21-Oct-05 sa002pe on Janus 11.00 shelton branch
*   Bumped screen version to 0520.
*
* \main\jnus11.00_i1_shelton\10 cx17598 18 oct 2005
*
*   Replace the hard key function fnhkModeSetManual using fnhkModeSetDefault, add a new hardkey
*   function fnhkModeSetManual on rate rule update required screen which sets the mode to key in
*   postage mode indeed.

*************************************************************************/


/**********************************************************************
        Include Header Files
**********************************************************************/
//#include "oiAbacus1.h"
//#include "oiAbacus2.h"
//#include "oiAbacus3.h"
//#include "oiAbacus4.h"
//#include "oiAbacus5.h"
//#include "oiajout.h"
//#include "oiasr.h"
#include "oiclock.h"
#include "oicmenucontrol.h"
#include "oicommon.h"
//#include "oideptaccount.h"
//#include "oidiagnostic.h"
#include "oientry.h"
#include "oierrhnd.h"
//#include "oifpimage.h"
#include "oifpinfra.h"
//#include "oifplock.h"
#include "oifpmain.h"
//#include "oifpmenu.h"
#include "oifpprint.h"
//#include "oifpsetup.h"
//#include "oifpsuprcode.h"
#include "oifunctfp.h"
//#include "oioob.h"
#include "oipostag.h"
#include "oipwrup.h"
//#include "oirating.h"
//#include "oirateservice.h"
//#include "oirefill.h"
//#include "oireport.h"
#include "oisleep.h"
#include "oitpriv.h"
//#include "oiweight.h"
//#include "oipreset.h"
//#include "oigts.h"
#include "oi_IntelliLink.h"
//#include "Oiscanner.h"
//#include "oiwow.h"

/* beginning of screen tool created portion ... */
/*SCREEN_GEN_SOURCE_START*/
pHKeyFn aHKeyFns[] = {
(pHKeyFn)	fnHKeyDoNothing
};
pHKeyFn2ndFormat aKeyFnsInFormat2[] = {
	fnHKeyDoNothing,
	(pHKeyFn2ndFormat) 0xFFFFFFFF
};
pPreFn aPreFns[] = {
	fnPreDoNothing,
	fnpClearBatteryWarnings,
	fnpDataCenterOptionsMenu,
	fnpEnterAmountTapePre,
	fnpEnterPowerDown,
	fnpEnterSleep,
	fnpHighPostageWarning,
	fnpIndiciaError,
	fnpIndiciaPrinting,
	fnpIndiciaReadyCheck,
	fnpIndiciaTapePrinting,
	fnpIndiciaTransient,
	fnpInfraGeneralErr,
	fnpPreErrInsufficientFunds,
	fnpPrepFeederError,
	fnpPrepInkOutError,
	fnpPrepInkTankError,
	fnpPrepInkTankExpiring,
	fnpPrepJamLeverOpen,
	fnpPrepMidnightAdvDate,
	fnpPrepNoInkTankError,
	fnpPrepNoPrintHeadError,
	fnpPrepPaperError,
	fnpPrepPheadError,
	fnpPrepPrinterFault,
	fnpPrepTopCoverOpen,
	fnpPrepWasteTankFull,
	fnpPrepWasteTankNearFull,
	fnpPreSystemRebootNeeded
};
pPostFn aPostFns[] = {
	fnPostDoNothing,
	fnpExitSleep,
	fnpPostDataCenterOptionsMenu,
	fnpPostGeneralErrorScreen,
	fnpPostIndiciaError,
	fnpPostIndiciaPrinting,
	fnpPostIndiciaReady,
	fnpPostIndiciaTapePrinting,
	fnpPostMissingImage
};
pInitFn aInitFns[] = {
	fnInitDoNothing,
	fnfAccountingJobIDMain,
	fnfAccountingSurchargeValue,
	fnfAddDriftUserAmPm,
	fnfAmPmToggleFieldInit,
	fnfAmPmToggleFieldRefresh,
	fnfAscReg,
	fnfAutoDateAdvAmPm,
	fnfAutoDateAdvInit,
	fnfAutoDateAdvStringSelect,
	fnfBlindDialingSetting,
	fnfBufferEmptyStringSelect,
	fnfBufferFullStringSelect,
	fnfBufferPartialStringSelect,
	fnfClearField,
	fnfClearTransportCaption,
	fnfControlSum,
	fnfCurrencySignAfter,
	fnfCurrencySignFront,
	fnfCurrentLocationCode,
	fnfDataCaptureDate,
	fnfDataUploadCurDetailAMPM,
	fnfDataUploadCurDetailDate,
	fnfDataUploadCurDetailTime,
	fnfDataUploadLogAMPM1,
	fnfDataUploadLogAMPM2,
	fnfDataUploadLogAMPM3,
	fnfDataUploadLogAMPM4,
	fnfDataUploadLogDate1,
	fnfDataUploadLogDate2,
	fnfDataUploadLogDate3,
	fnfDataUploadLogDate4,
	fnfDataUploadLogTime1,
	fnfDataUploadLogTime2,
	fnfDataUploadLogTime3,
	fnfDataUploadLogTime4,
	fnfDataUploadNum1,
	fnfDataUploadNum2,
	fnfDataUploadNum3,
	fnfDataUploadNum4,
	fnfDatCapNextCallAMPM,
	fnfDatCapNextCallTime,
	fnfDatCapReqAMPM,
	fnfDatCapReqTime,
	fnfDcapNextUploadDate,
	fnfDcapNextUploadPlanned,
	fnfDCAPUploadReqDate,
	fnfDecimalChar,
	fnfDefValueEntryInit,
	fnfDescReg,
	fnfDHOnTimeAmPm,
	fnfDHOnTimeInit,
	fnfDiagError,
	fnfDialingPhoneNumberInit,
	fnfDialingPrefixInit,
	fnfDialingPrefixOutput,
	fnfDialTonePulse,
	fnfDiplayPrintingModeText,
	fnfDisplayDecimalMoney,
	fnfDisplayHighPostage,
	fnfDisplayInstructionLine1,
	fnfDisplayInstructionLine2,
	fnfDisplayInstructionLine3,
	fnfDisplayInstructionLine4,
	fnfDisplayMaxAdvDays,
	fnfDriftAddTimeRange,
	fnfDriftSubTimeRange,
	fnfDSTActive,
	fnfEntryErrorInfo,
	fnfFeederErrorsCaption,
	fnfFeederErrorSKIndicator,
	fnfFencingLimitHighValue,
	fnfFencingLimitLowValue,
	fnfFundsOrCreditShow,
	fnfGraphicErrorHex,
	fnfGTSStoreIndicator,
	fnfHighValueWarningAmt,
	fnfHighValueWarningInit,
	fnfHomeErrorCounter,
	fnfHomeErrorStrings,
	fnfHomeWarningCounter,
	fnfHomeWarningStrings,
	fnfILinkEffDateForActiveNPending1,
	fnfILinkEffDateForActiveNPending2,
	fnfILinkEffDateForActiveNPending3,
	fnfILinkEffDateForActiveNPending4,
	fnfILinkNewRatesDescription1,
	fnfILinkNewRatesDescription2,
	fnfILinkNewRatesDescription3,
	fnfILinkNewRatesEffectiveDate1,
	fnfILinkNewRatesEffectiveDate2,
	fnfILinkNewRatesEffectiveDate3,
	fnfILinkNewRatesEffectiveDate4,
	fnfILinkNewRatesPendingDate,
	fnfIndiciaPrintingStatus,
	fnfIndiciaSerial,
	fnfIndiciaTapePrintingStatus,
	fnfIndiciaTapePrintingStatus1,
	fnfInfDataUploadErrCode,
	fnfInfDataUploadErrString,
	fnfInfraContactPBCaption,
	fnfInfraDialingErrorInfo,
	fnfInfraDialingInfo1,
	fnfInfraDialingInfo2,
	fnfInfraDownloadComplete,
	fnfInfraDownloadFileCountCurrent,
	fnfInfraDownloadFileCountTotal,
	fnfInfraDownloadPercentage,
	fnfInfraDownloadProgressBar,
	fnfInfraDownloadRemainTime,
	fnfInfraGenErrorCaption,
	fnfInfraPbpError,
	fnfInfraTimeoutClockValue,
	fnfInitPostageCorrectionAmount,
	fnfInViewDisableError,
	fnfJamLeverOpenIndication,
	fnfKIPEnteredPostageAmt,
	fnfLANFirewallAddTrustedHostEntryInit,
	fnfLANFirewallPingSetting,
	fnfLANFirewallTrustAllIndicator,
	fnfLANFirewallTrustedHostEnterCaption,
	fnfLANFirewallTrustedHostSetting,
	fnfLANFirewallTrustedHostToDelete,
	fnfLANFirewallTrustHostOnlyIndicator,
	fnfLANFirewallTrustNoneIndicator,
	fnfLowFundsWarningAmt,
	fnfLowFundsWarningInit,
	fnfMaxTapeNumber,
	fnfMeterErrorHex,
	fnfMeterStamp,
	fnfMidnightPrintedDate,
	fnfModemStringInit,
	fnfMoneySign,
	fnfNormalPresetTimerInit,
	fnfOOBDialingPrefixInit,
	fnfPaperErrorsCaption,
	fnfPaperErrorSKIndicator,
	fnfPBPSerial,
	fnfPCN,
	fnfPercentageComplete,
	fnfPercentageCompleteTitle,
	fnfPieceCount,
	fnfPostageTapeAmtPrompt,
	fnfPostageValue,
	fnfPreViewPrintedDate,
	fnfPrintDateTimeOnOff,
	fnfPrintedDate,
	fnfPrintedDateWithoutDuck,
	fnfPrintMode,
	fnfPrintPositionInit,
	fnfPrintPositionRange,
	fnfProgressBar,
	fnfPSDState,
	fnfRTCTime,
	fnfRTCTimeAmPm,
	fnfScrollableEntryEditOption,
	fnfScrollableEntryInit,
	fnfScrollableEntryModemStr,
	fnfScrollableEntryRefresh,
	fnfSelectedItemIndicator1,
	fnfSelectedItemIndicator2,
	fnfSelectedItemIndicator3,
	fnfSelectedItemIndicator4,
	fnfSetPostagePrompt,
	fnfSetTapesPrompt,
	fnfShow12HourClockText,
	fnfShowChangeDateMenu,
	fnfShowFooterMsgInAdDateTimeDisabled,
	fnfShowIndicationOfBaseType,
	fnfShowTextAdDateTimeCaption,
	fnfSleepModeTimerInit,
	fnfSMenuDistrParamShown1,
	fnfSMenuDistrParamShown2,
	fnfSMenuDistrParamShown3,
	fnfSMenuDistrParamShown4,
	fnfSMenuLabelSlot1,
	fnfSMenuLabelSlot2,
	fnfSMenuLabelSlot3,
	fnfSMenuLabelSlot4,
	fnfSMenuNumberSlot1,
	fnfSMenuNumberSlot2,
	fnfSMenuNumberSlot3,
	fnfSMenuNumberSlot4,
	fnfSMenuNumberSlot5,
	fnfSMenuNumOrLabelSlot1,
	fnfSMenuNumOrLabelSlot2,
	fnfSMenuNumOrLabelSlot3,
	fnfSMenuNumOrLabelSlot4,
	fnfSMenuValueSlot1Init,
	fnfSMenuValueSlot2Init,
	fnfSMenuValueSlot3Init,
	fnfSMenuValueSlot4Init,
	fnfSoftKeyIndicator1,
	fnfSoftKeyIndicator2,
	fnfSoftKeyIndicator3,
	fnfSoftKeyIndicator4,
	fnfSoftKeyIndicator5,
	fnfStdDistrParamEntryInit,
	fnfStdDistrParamPartShown,
	fnfStdEntryInit,
	fnfStdEntryRefresh,
	fnfSubDriftUserAmPm,
	fnfSystemDate,
	fnfSystemRebootTimeoutClockValue,
	fnfTapeErrorsCaption,
	fnfTapeErrorSKIndicator,
	fnfTHostSoftKeyIndicator1,
	fnfTHostSoftKeyIndicator2,
	fnfTHostSoftKeyIndicator3,
	fnfTHostSoftKeyIndicator4,
	fnfUICExtVersion,
	fnfUICSerial,
	fnfUploadCompleteStringSelect1,
	fnfUploadCompleteStringSelect2,
	fnfUploadCompleteStringSelect3,
	fnfUserAmPm,
	fnfUserCorrectDriftAmPm,
	fnfUserCorrectDriftTime,
	fnfUserTime,
	fnfVersion,
	fnfViewPrintedDate,
	fnfWaitForEnvTimeoutInit,
	fnfWOWErrorsCaption,
	fnfWOWErrorSKIndicator,
	fnfWOWErrorsTitle,
	fnfWOWSBRErrorsCaption,
	fnfZWZPTimeoutInit
};
pRefreshFn aRefreshFns[] = {
	fnRefreshDoNothing,
	fnfAccountingJobIDMain,
	fnfAccountingSurchargeValue,
	fnfAddDriftUserAmPm,
	fnfAmPmToggleFieldRefresh,
	fnfAscReg,
	fnfAutoDateAdvStringSelect,
	fnfBlindDialingSetting,
	fnfBufferEmptyStringSelect,
	fnfBufferFullStringSelect,
	fnfBufferPartialStringSelect,
	fnfClearField,
	fnfClearTransportCaption,
	fnfControlSum,
	fnfCurrencySignAfter,
	fnfCurrencySignFront,
	fnfCurrentLocationCode,
	fnfDataCaptureDate,
	fnfDataUploadCurDetailAMPM,
	fnfDataUploadCurDetailDate,
	fnfDataUploadCurDetailTime,
	fnfDataUploadLogAMPM1,
	fnfDataUploadLogAMPM2,
	fnfDataUploadLogAMPM3,
	fnfDataUploadLogAMPM4,
	fnfDataUploadLogDate1,
	fnfDataUploadLogDate2,
	fnfDataUploadLogDate3,
	fnfDataUploadLogDate4,
	fnfDataUploadLogTime1,
	fnfDataUploadLogTime2,
	fnfDataUploadLogTime3,
	fnfDataUploadLogTime4,
	fnfDataUploadNum1,
	fnfDataUploadNum2,
	fnfDataUploadNum3,
	fnfDataUploadNum4,
	fnfDatCapNextCallAMPM,
	fnfDatCapNextCallTime,
	fnfDatCapReqAMPM,
	fnfDatCapReqTime,
	fnfDcapNextUploadDate,
	fnfDcapNextUploadPlanned,
	fnfDCAPUploadReqDate,
	fnfDefValueEntryRefresh,
	fnfDescReg,
	fnfDiagError,
	fnfDialingPhoneNumberInit,
	fnfDialingPrefixOutput,
	fnfDialTonePulse,
	fnfDiplayPrintingModeText,
	fnfDisplayDecimalMoney,
	fnfDisplayHighPostage,
	fnfDisplayInstructionLine1,
	fnfDisplayInstructionLine2,
	fnfDisplayInstructionLine3,
	fnfDisplayInstructionLine4,
	fnfDisplayMaxAdvDays,
	fnfDriftAddTimeRange,
	fnfDriftSubTimeRange,
	fnfDSTActive,
	fnfEntryErrorInfo,
	fnfFeederErrorsCaption,
	fnfFeederErrorSKIndicator,
	fnfFencingLimitHighValue,
	fnfFencingLimitLowValue,
	fnfFundsOrCreditShow,
	fnfGraphicErrorHex,
	fnfGTSStoreIndicator,
	fnfHideEntryRefresh,
	fnfHighValueWarningAmt,
	fnfHomeErrorCounter,
	fnfHomeErrorStrings,
	fnfHomeWarningCounter,
	fnfHomeWarningStrings,
	fnfILinkEffDateForActiveNPending1,
	fnfILinkEffDateForActiveNPending2,
	fnfILinkEffDateForActiveNPending3,
	fnfILinkEffDateForActiveNPending4,
	fnfILinkNewRatesDescription1,
	fnfILinkNewRatesDescription2,
	fnfILinkNewRatesDescription3,
	fnfILinkNewRatesEffectiveDate1,
	fnfILinkNewRatesEffectiveDate2,
	fnfILinkNewRatesEffectiveDate3,
	fnfILinkNewRatesEffectiveDate4,
	fnfILinkNewRatesPendingDate,
	fnfIndiciaPrintingStatus,
	fnfIndiciaSerial,
	fnfIndiciaTapePrintingStatus,
	fnfIndiciaTapePrintingStatus1,
	fnfInfDataUploadErrCode,
	fnfInfDataUploadErrString,
	fnfInfraContactPBCaption,
	fnfInfraDialingErrorInfo,
	fnfInfraDialingInfo1,
	fnfInfraDialingInfo2,
	fnfInfraDownloadComplete,
	fnfInfraDownloadFileCountCurrent,
	fnfInfraDownloadFileCountTotal,
	fnfInfraDownloadPercentage,
	fnfInfraDownloadProgressBar,
	fnfInfraDownloadRemainTime,
	fnfInfraGenErrorCaption,
	fnfInfraPbpError,
	fnfInfraScrollEntryRefresh,
	fnfInfraTimeoutClockValue,
	fnfInViewDisableError,
	fnfJamLeverOpenIndication,
	fnfKIPEnteredPostageAmt,
	fnfLANFirewallPingSetting,
	fnfLANFirewallTrustAllIndicator,
	fnfLANFirewallTrustedHostEnterCaption,
	fnfLANFirewallTrustedHostSetting,
	fnfLANFirewallTrustedHostToDelete,
	fnfLANFirewallTrustHostOnlyIndicator,
	fnfLANFirewallTrustNoneIndicator,
	fnfLowFundsWarningAmt,
	fnfMaxTapeNumber,
	fnfMeterErrorHex,
	fnfMeterStamp,
	fnfMidnightPrintedDate,
	fnfModemStringInit,
	fnfMoneySign,
	fnfPaperErrorsCaption,
	fnfPaperErrorSKIndicator,
	fnfPBPSerial,
	fnfPCN,
	fnfPercentageComplete,
	fnfPercentageCompleteTitle,
	fnfPieceCount,
	fnfPostageTapeAmtPrompt,
	fnfPostageValue,
	fnfPreViewPrintedDate,
	fnfPrintDateTimeOnOff,
	fnfPrintedDate,
	fnfPrintedDateWithoutDuck,
	fnfPrintMode,
	fnfPrintPositionRange,
	fnfProgressBar,
	fnfPSDState,
	fnfRTCTime,
	fnfRTCTimeAmPm,
	fnfScrollableEntryEditOption,
	fnfScrollableEntryRefresh,
	fnfScrollableEntryRefresh2,
	fnfScrollableHideEntryRefresh,
	fnfScrollableHideEntryRefreshProxyPwd,
	fnfSelectedItemIndicator1,
	fnfSelectedItemIndicator2,
	fnfSelectedItemIndicator3,
	fnfSelectedItemIndicator4,
	fnfSetPostagePrompt,
	fnfSetTapesPrompt,
	fnfShow12HourClockText,
	fnfShowChangeDateMenu,
	fnfShowFooterMsgInAdDateTimeDisabled,
	fnfShowIndicationOfBaseType,
	fnfShowTextAdDateTimeCaption,
	fnfSMenuDistrParamShown1,
	fnfSMenuDistrParamShown2,
	fnfSMenuDistrParamShown3,
	fnfSMenuDistrParamShown4,
	fnfSMenuLabelSlot1,
	fnfSMenuLabelSlot2,
	fnfSMenuLabelSlot3,
	fnfSMenuLabelSlot4,
	fnfSMenuNumberSlot1,
	fnfSMenuNumberSlot2,
	fnfSMenuNumberSlot3,
	fnfSMenuNumberSlot4,
	fnfSMenuNumberSlot5,
	fnfSMenuNumOrLabelSlot1,
	fnfSMenuNumOrLabelSlot2,
	fnfSMenuNumOrLabelSlot3,
	fnfSMenuNumOrLabelSlot4,
	fnfSMenuValueSlot1Refresh,
	fnfSMenuValueSlot2Refresh,
	fnfSMenuValueSlot3Refresh,
	fnfSMenuValueSlot4Refresh,
	fnfSoftKeyIndicator1,
	fnfSoftKeyIndicator2,
	fnfSoftKeyIndicator3,
	fnfSoftKeyIndicator4,
	fnfSoftKeyIndicator5,
	fnfStdDistrParamPartShown,
	fnfStdEntryRefresh,
	fnfSubDriftUserAmPm,
	fnfSystemDate,
	fnfSystemRebootTimeoutClockValue,
	fnfTapeErrorsCaption,
	fnfTapeErrorSKIndicator,
	fnfTHostSoftKeyIndicator1,
	fnfTHostSoftKeyIndicator2,
	fnfTHostSoftKeyIndicator3,
	fnfTHostSoftKeyIndicator4,
	fnfUICExtVersion,
	fnfUICSerial,
	fnfUploadCompleteStringSelect1,
	fnfUploadCompleteStringSelect2,
	fnfUploadCompleteStringSelect3,
	fnfUserAmPm,
	fnfUserCorrectDriftAmPm,
	fnfUserCorrectDriftTime,
	fnfUserTime,
	fnfVersion,
	fnfViewPrintedDate,
	fnfWOWErrorsCaption,
	fnfWOWErrorSKIndicator,
	fnfWOWErrorsTitle,
	fnfWOWSBRErrorsCaption
};
pEventFn aEventFns[] = {
(pEventFn)	fnEventDoNothing,
(pEventFn)	fnevAlphaEnterMoveCursor,
(pEventFn)	fnevAlphaEnterWithSwitch,
(pEventFn)	fnevCheckCMStatusBeforePrint ,
(pEventFn)	fnevCheckDelayedRpt,
(pEventFn)	fnevCheckScreenReentry,
(pEventFn)	fnevClearJamLeverOpen,
(pEventFn)	fnevCMErrorNotCleared,
(pEventFn)	fnevContinueToPrint,
(pEventFn)	fnevContinueToPrintMail,
(pEventFn)	fnevContinueToPrintTape,
(pEventFn)	fnevInfraTimeoutClockTick,
(pEventFn)	fnevInvokeErrScreen,
(pEventFn)	fnevJamLeverErrCleared,
(pEventFn)	fnevMailJobCompleted,
(pEventFn)	fnevMailPieceCompleted,
(pEventFn)	fnevMidnightComeInPrintingMail,
(pEventFn)	fnevMidnightComeInPrintingTape,
(pEventFn)	fnevPaperCleared,
(pEventFn)	fnevPaperErrorClearedReturn,
(pEventFn)	fnevPowerUpComplete,
(pEventFn)	fnevProcessDistrMsg,
(pEventFn)	fnevRebootUIC,
(pEventFn)	fnevRefillSuccessfulTimeout,
(pEventFn)	fnevRetFromErrScreen,
(pEventFn)	fnevSendRateReady,
(pEventFn)	fnevSendStartRequest,
(pEventFn)	fnevSetMarginFailed,
(pEventFn)	fnevSystemRebootTimeoutClockTick,
(pEventFn)	fnevTapeJobCompleted,
(pEventFn)	fnevTapePieceCompleted,
(pEventFn)	fnevTopCoverClosed,
(pEventFn)	fnevUpdateErrorMessages,
(pEventFn)	fnevUpdatePrinterState,
(pEventFn)	fnevUpdateWarningMessages,
(pEventFn)	fnevValidateEdmPostage,
(pEventFn)	fnevWaitPSOCDisablingConditionsClear
};

const size_t    OITEventIDMax = ARRAY_LENGTH( aEventFns );

pEventFn2ndFormat aEventFnsInFormat2[] = {
	fnEventDoNothing,
	fnevAlphaEnterMoveCursor,
	fnevCheckCMStatusBeforePrint ,
	fnevCheckDelayedRpt,
	fnevCheckScreenReentry,
	fnevClearJamLeverOpen,
	fnevCMErrorNotCleared,
	fnevContinueToPrint,
	fnevContinueToPrintMail,
	fnevContinueToPrintTape,
	fnevInfraTimeoutClockTick,
	fnevInvokeErrScreen,
	fnevJamLeverErrCleared,
	fnevMailJobCompleted,
	fnevMailPieceCompleted,
	fnevMidnightComeInPrintingMail,
	fnevMidnightComeInPrintingTape,
	fnevPaperCleared,
	fnevPaperErrorClearedReturn,
	fnevPowerUpComplete,
	fnevProcessDistrMsg,
	fnevRebootUIC,
	fnevRefillSuccessfulTimeout,
	fnevRetFromErrScreen,
	fnevSendRateReady,
	fnevSendStartRequest,
	fnevSetMarginFailed,
	fnevSystemRebootTimeoutClockTick,
	fnevTapeJobCompleted,
	fnevTapePieceCompleted,
	fnevTopCoverClosed,
	fnevUpdateErrorMessages,
	fnevUpdatePrinterState,
	fnevUpdateWarningMessages,
	fnevWaitPSOCDisablingConditionsClear,
	(pEventFn2ndFormat) 0xFFFFFFFF
};
pGraphicInitFn aGraphicInitFns[] = {
	fngInitDoNothing,
	fngDiagError,
	fngEntryErrorInfo,
	fngHomeErrorDisplayed,
	fngHomeWarningDisplayed,
	fngSMenuBottomLine,
	fngSMenuScrollBar
};
pGraphicRefreshFn aGraphicRefreshFns[] = {
	fngRefreshDoNothing,
	fngDiagError,
	fngEntryErrorInfo,
	fngHomeErrorDisplayed,
	fngHomeWarningDisplayed,
	fngSMenuBottomLine,
	fngSMenuScrollBar
};
/*SCREEN_GEN_SOURCE_STOP*/
/* end screen tool created portion ... */
