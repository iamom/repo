/*************************************************************************
*   PROJECT:        Horizon CSD
*  MODULE NAME:     imagegen.c  $
*       
*   DESCRIPTION:    Image generation functions
*
* -----------------------------------------------------------------------
*               Copyright (c) 2016 Pitney Bowes Inc.
*                    37 Executive Drive
*                   Danbury, Connecticut  06810
* -----------------------------------------------------------------------
*
*   REVISION HISTORY:

 17-Apr-18 sa002pe on FPHX 02.12 shelton branch
 	Removed everything for text ads.  Never implemented.
	Making sure everything for German Entgelt Bezahlt is commented out.

 27-Apr-17 sa002pe on FPHX 02.12 shelton branch
 	Commented out use of IGU_LoadTextAd.  Feature was never implemented.

 	Merging in changes from K7:
----*
	* 21-Oct-15 sa002pe on K7 tips shelton branch
	*	Changed IG_LoadVariableVCRs to add a syslog message indicating when the size
	*	of the 2D barcode region is too small.
----*
*************************************************************************/

#include <stdio.h>
#include <string.h>

#include "imagegen.h"       // must be first
#include "imageutl.h"
#include "ImageErrs.h"
#include "bobutils.h"       // must come before bob.h
#include "bob.h"
#include "datdict.h"
#include "DmBcPublic.h"
#include "features.h"
#include "fwrapper.h"
#include "report1.h"
#include "tealeavs.h"
#include "unistring.h"
#include "lodepng.h"
#include "UTILS.h"
#include "aiservic.h"

#ifdef G900
#include "oit.h"
#endif
//TODO - ensure all endian issues of EMD reads are addressed
#include "flashutil.h"




#ifdef IG_DEMO_AD
extern unsigned char DemoAdHeader[42];
extern unsigned char DemoAdData[13901];
#endif

#ifdef IG_TRY_TEST_PATTERN
extern unsigned char TestPatternHeader[16];
extern unsigned char TestPatternData[27544];
#endif

#ifdef IG_TRY_FUNDS_REPORT
extern IG_BLOB_DATA FundsReportBlob;
extern unsigned char FundsReportItems;
extern unsigned char FundsReportHeader[16];
extern unsigned char FundsReportData[21885];
#endif

#ifdef IG_TRY_REFILL_REPORT
extern IG_BLOB_DATA RefillReportBlob;
extern unsigned char RefillReportItems;
extern unsigned char RefillReportHeader[16];
extern unsigned char RefillReportData[21481];
#endif

#ifdef IG_TEST_DATA
extern IG_FONT_DATA FakeFontData;
extern IG_BLOB_DATA FakeStaticBlob;
extern IG_BLOB_DATA FakeVariableBlob;
extern unsigned char NumFakeFonts;
extern unsigned char FakeStaticItems;
extern unsigned char FakeVariableItems;
extern unsigned char FakeDebitData[89];
extern unsigned char TextAdHeader[42];
extern unsigned char TextAdData[354];
#endif

extern CMOSDownloadCtrlBuf  CmosDownloadCtrlBuf;
extern EXP_MTR_CMOS_DATA exp_mtr_cmos;


// Globals that may have to be moved elsewhere
unsigned char bImageBuffer[MAX_COLS][IMAGE_ROW_BYTES];
unsigned char bImageBuffer2[MAX_COLS][IMAGE_ROW_BYTES];
unsigned char bmpImageBuffer[IMAGE_ROW_BYTES][MAX_COLS];

// Internal contants

// Ad Types
#define GRAPHIC  0
#define TEXT     1

// Image Buffer States
#define RELOAD      FALSE
#define LOADED      TRUE

// Indicia Types
#define NORMAL      0
#define LOW         1

// Indicia Components being used
#define NORMAL_LOW          FALSE
#define LEADING_LAGGING     TRUE

// Maximum sizes
#define MAX_FIELDS      20      // max register groups per graphic
#define MAX_IMAGES      2       // max indicia-type graphics loaded from flash
                                // at any given time
#define MAX_TCS         2       // max town circle graphics loaded from flash
                                // at any given time
#define MAX_LEAD_IMAGES 6       // max number of leading indicia graphics
#define MAX_LAG_IMAGES  2       // max number of lagging indicia graphics


// Schema version numbers
#define FONT_SCHEMA			0		// original schema
#define FONT_SCHEMA_4BYTE	1		// font schema where the bitmap size is 4 bytes
#define GRAPHIC_SCHEMA  0
#define BARCODE_SCHEMA  0
#define IG_TEXT_AD_SCHEMA   0

// General record sizes
#define GRAPHIC_DATA_RECORD_SIZE    (2+2+1+1+2+2)       // starting at the width element
#define BARCODE_SYMBOL_RECORD_SIZE  (1+1+1+1+2)
#define TEXT_AD_RECORD_SIZE         (1+1+42)
#define REGION_RECORD_SIZE          (1+1+2+2+2+2)
#define REGISTER_GROUP_RECORD_SIZE  (2+1+1+2+2+1+1)
#define VCR_RECORD_SIZE             (2+2)

// Offsets to data in the Font component
#define FONT_TEXT_AD_FONT       (4+4)
#define FONT_SCHEMA_OFFSET      (FONT_TEXT_AD_FONT+1)
#define FONT_BITMAP_OFFSET      (FONT_SCHEMA_OFFSET+1)
#define FONT_SIZE               (FONT_BITMAP_OFFSET+2)

// Offsets to data in the Graphics component header
#define GRAPHIC_SCHEMA_OFFSET   (4+4+1)
#define GRAPHIC_NUM_NAMES       (GRAPHIC_SCHEMA_OFFSET+1+2+2+1)
#define GRAPHIC_NAME_DESC       (GRAPHIC_NUM_NAMES+1)

// Offsets to data in the Public Key Graphics component data area
#define GRAPHIC_WIDTH           (8+12)
#define GRAPHIC_VCRS_OFFSET     (GRAPHIC_WIDTH+GRAPHIC_DATA_RECORD_SIZE)
#define GRAPHIC_IMAGE_OFFSET    (GRAPHIC_VCRS_OFFSET+2)
#define GRAPHIC_FIELDS          (GRAPHIC_IMAGE_OFFSET+2)

// Offsets to data in the Barcode Graphics component data area
#define BARCODE_SYMBOLS_OFFSET  (GRAPHIC_WIDTH+BARCODE_DATA_RECORD_SIZE)

// Offsets to data in the Barcode Graphics component data area
#define TEXT_LINES_OFFSET       (1+1)

// Indices into the DirtyStuff array
#define INDICIA_DIRTY       0
#define PERMIT_DIRTY        1
#define TAX_DIRTY           2
#define AD_DIRTY            3
#define INSCR_DIRTY         4
#define TC_DIRTY            5
#define EB_DIRTY            6
#define REGIONS_DIRTY       7
#define REGISTERS_DIRTY     8
#define TEXT_ENTRY_DIRTY    9
#define DATE_TIME_DIRTY     10
#define MISC_DIRTY          11
#define BARCODE_DIRTY		12
#define MAX_DIRTY			13

// Internal variables and structures
unsigned long   lLastErrCode;       // the last error code that caused fbImageGenFailed
                                    // to be set to TRUE

unsigned short  wNewStartCol;       // starting column of the next possible region
unsigned short  wNewStartRow;       // starting row of the next possible region
unsigned short  wNextVCR;           // array ID of the next available VCR in sPrintedVCRs
unsigned short  wReqCols;           // number of columns that must be printed
unsigned short  wTotalVCRs;         // Total number of VCRs for a particular print area
unsigned short  wIGPermitName[MAX_PERMIT_UNICHARS+1];   // Name of the currently selected permit graphic
unsigned short  wIGTownCircleName[MAX_TC_UNICHARS+1];   // Name of the TC graphic currently being used

unsigned char   bAdID;              // ID of the currently loaded ad
unsigned char   bBarcodeRegion;     // index into sPrintedRegionMap to get to the barcode region
unsigned char   bCurrentTC;         // which sTCData structure is currently being used
unsigned char   bDateState;         // date ducking state
unsigned char   bImageType;         // Type of overall image in the print area
unsigned char   bIndiciaComponent;  // indicia components being used (normal/low or leading/lagging or unknown)
unsigned char   bIndiciaType;       // type of indicia graphic to use (normal or low or unknown)
unsigned char   bInscrID;           // ID of the currently loaded inscription
unsigned char   bNumFlashRegions[MAX_TYPES];    // number of regions in each of the flash region maps
unsigned char   bNumOfFonts;        // number of fonts in Flash
unsigned char   bNumOfImages;       // number of sIndiciaData structures that have been loaded
unsigned char   bNumOfTCs;          // number of sTCData structures that have been loaded
unsigned char   bPermitType;        // permit component type ID
unsigned char   bPrintedRegions;    // number of regions currently in sPrintedRegionMap
unsigned char   bTotalGroups;       // total number of register groups for a particular print area
unsigned char   bTotalRegions;      // total number of records in the RegionMap array in Flash

unsigned char   bNormalIndiciaComponent;
unsigned char	ucTCFondId;

BOOL	fb2dBarcodeState;			// 2D barcode ducking state
BOOL    fbAdHasVCRs;                // indicates if the currently selected ad has VCRs
BOOL    fbBarcodeCleared;           // barcode area has been cleared
BOOL    fbBatchCountState[MAX_TYPES];   // Printed Batch Count ducking state
BOOL    fbDirtyStuff[MAX_DIRTY];    // indicates which parts of the print area need to be reloaded
BOOL    fbEBState;                  // EB ducking state
BOOL    fbHasBarcode;               // print area contains a barcode
BOOL    fbMiscGraphicState;         // Misc. Graphic ducking state
BOOL    fbImageBufferState;         // state (loaded or reload) of the image buffer
BOOL    fbImageGenFailed;           // Image Generator isn't working properly
BOOL    fbLaggingRequired;          // A lagging indicia is required with the leading indicia
BOOL    fbPINState;                 // PIN ducking state
BOOL    fbTCState;                  // Town line-circle ducking state
#if IG_TEST_DUCKING
BOOL    fbTesting = FALSE;
#endif
BOOL    fbTextEntryState;           // Text entry ducking state
BOOL    fbTimeState;                // Time ducking state

FONT_DATA sFontData[IG_MAX_FONTS];

typedef struct
{
    unsigned char   *psVCRs;        // can't be sure the VCR definitions will start on an even-byte
                                    // boundary
    unsigned char   *psFields;      // can't be sure the register group definitions will start on an
                                    // even byte boundary
    unsigned char   *pbImage;
    unsigned short  wWidth;         // Keep this and the next 5 items in the same order as in flash
    unsigned short  wHeight;
    unsigned char   bNumOfGroups;
    unsigned char   bPad1;
    unsigned short  wNumOfVCRs;
    unsigned short  wSize;
    unsigned char   bComponentType;
    BOOL            fbLoaded;
} GRAPHIC_DATA;

GRAPHIC_DATA sAdData;
GRAPHIC_DATA sDateTimeData;
GRAPHIC_DATA sEBData;
GRAPHIC_DATA sIndiciaData[MAX_IMAGES];
GRAPHIC_DATA sInscrData;
GRAPHIC_DATA sMiscData;
GRAPHIC_DATA sPermitData;
GRAPHIC_DATA sReportTestData;
GRAPHIC_DATA sTaxData;
GRAPHIC_DATA sTextEntryData;

TOWN_CIRCLE_DATA sTCData[MAX_TCS];

IG_BARCODE_DATA sBarCodeData;

REGION_MAP sFlashRegionMap[MAX_TYPES][MAX_NUM_REGIONS_PER_AREA];
REGION_MAP sPrintedRegionMap[MAX_NUM_REGIONS_PER_AREA];

REGISTER_GROUPS sRegisterGroups[IG_MAX_GROUPS];

PRINTED_VCRS sPrintedVCRs[IG_MAX_VCRS];


// prototypes for internal subroutines
void        ClearBuffers(void);

unsigned long ClearRestOfIgStuff(void);

void        ClearVariables(void);

void        ClearVCRs(const unsigned short wStartVCR, const unsigned short wEndVCR);

unsigned long GetBarcode(const unsigned char bCompType,
                          const unsigned char *pbHeaderAddr, const unsigned char *pbDataAddr);

unsigned long GetFonts(const unsigned char bNumFonts, const IG_FONT_DATA *psFontData);

unsigned long GetImage(const unsigned char bType,
                       const unsigned char *pbHeaderAddr, unsigned char *pbDataAddr,
                       const BOOL fbSkipLoad, const BOOL fbIsReport);

unsigned long GetTC(const unsigned char bType, const BOOL fbSkipLoad,
                    const unsigned char *pbHeaderAddr, unsigned char *pbDataAddr);

void        LoadRegionData(const REGION_MAP *psFlashRegionMap);

unsigned long LoadRegisterData(const unsigned char *pbData1, const unsigned char bNumOfGroups,
                               const unsigned char *pbData2, const unsigned short wNumOfVCRs,
                               const unsigned short wStartCol, const unsigned short wStartRow);

unsigned long LoadRest(const REGION_MAP *psFlashRegionMap);

BOOL        NoTownCircleVCRs(void);

unsigned long SearchStructure(const unsigned char bType, const unsigned char bNumComponents,
                              const IG_COMPONENT_DATA *psCompData, unsigned char *pbCompInx);

unsigned long SearchStructureForPermit(const unsigned char bType, const unsigned char bNumComponents,
                            const IG_COMPONENT_DATA *psCompData, unsigned char *pbCompInx);
                            
/*****************************************************************************
*****************************************************************************/

// check if things can be repositioned into the ad slogan region.
BOOL fnIsReposAllowed(void)
{
#ifdef K700
    return (TRUE);
#else
    return (fnFlashGetByteParm(BP_OVERWRITE_AD_REGION_ALLOWED));
#endif
}


// get the min print length fudge margin.
unsigned short fnGetMinPrintMargin(void)
{
    unsigned short  wTemp = 0;
#ifndef K700
    extern unsigned short wMinPrintMargin;
    extern unsigned short wBuildTestMode;

    // if in test mode, use the value in CMOS.
    if (wBuildTestMode == IN_BUILD_TEST_MODE)
    {
        wTemp = wMinPrintMargin;
    }
    // else, use the EMD parameter value.
    // if the value in CMOS is zero and the parameter value isn't zero,
    //  set the value in CMOS to the parameter value so the value in CMOS will start
    //  out the same as the parameter value.
    else
    {
        wTemp = fnFlashGetWordParm(WP_MIN_PRINTED_COLUMNS_OFFSET);
        if ((wMinPrintMargin == 0) && (wTemp != 0))
            wMinPrintMargin = wTemp;
    }
#endif
    return (wTemp);
}

// check if the graphics should be included in the min print length.
BOOL fnIncludeGraphicInMinPrint(unsigned short usGraphicBitMask)
{
	unsigned short usParamVal = fnFlashGetWordParm(WP_MIN_PRINT_LENGTH_ADJUSTMENTS);
	BOOL retval = FALSE;


	switch(usGraphicBitMask)
	{
#ifdef G900
		case INCLUDE_MODE_AJOUT_INSCR:
	    {
	    	unsigned char bPrintMode = fnOITGetPrintMode();

			if (((bPrintMode == PMODE_AJOUT_PLATFORM) ||
				 (bPrintMode == PMODE_AJOUT_MANUAL_WGT_ENTRY)) &&
				(usParamVal & usGraphicBitMask))
				retval = TRUE;
		}
#endif

		case INCLUDE_INSCR_ALWAYS:
		case INCLUDE_AD_ALWAYS:
		case INCLUDE_MISC_GRAPHIC_ALWAYS:
		case INCLUDE_TEXT_MSG_ALWAYS:
		default:
			if (usParamVal & usGraphicBitMask)
				retval = TRUE;
			break;
	}

	return(retval);
}


/*****************************************************************************
//                           PUBLIC FUNCTIONS
*****************************************************************************/
/*****************************************************************************
//                           GENERAL FUNCTIONS
*****************************************************************************/
/*****************************************************************************
// FUNCTION NAME: IG_Powerup
// DESCRIPTION: Initializes up all the buffers, structures & variables for the
//              Image Generator
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bNumFonts = number of fonts described in IG_FONT_DATA structure
//         bNumRecords = number of records in the region map in Flash
//         bNumComponents = number of components described in IG_COMPONENT_DATA structure
//         psFontData = pointer to an IG_FONT_DATA structure, which has the IDs
//                      and addresses for all the fonts
//         pbRegionMap = pointer to the region map in Flash
//         psCompData = pointer to an IG_COMPONENT_DATA structure, which has the IDs
//                      and addresses for all the necessary components
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_Powerup(const unsigned char bNumFonts, const unsigned char bNumRecords,
                         const unsigned char bNumComponents, const IG_FONT_DATA *psFontData,
                         const unsigned char *pbRegionMap, const IG_COMPONENT_DATA *psCompData)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned char   bInx;


    // Make sure the print area is empty
    (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));

    // Initialize the variables that only have to be initialized on powerup
    bAdID = 0;
    bDateState = VLT_DATE_DAY_UNDUCKED;
    bImageType = UNKNOWN;
    bIndiciaType = UNKNOWN;
    bInscrID = 0;
    bPermitType = UNKNOWN;

    bNormalIndiciaComponent = IMG_ID_NORMAL_INDICIA;

	fb2dBarcodeState = UNDUCKED;
    fbAdHasVCRs = FALSE;
    fbBarcodeCleared = FALSE;

    for (bInx = 0; bInx < MAX_TYPES; bInx++)
        fbBatchCountState[bInx] = UNDUCKED;
    fbEBState = DUCKED;
    fbMiscGraphicState = UNDUCKED;
    fbPINState = UNDUCKED;
    fbTCState = UNDUCKED;
    fbTextEntryState = DUCKED;
    fbTimeState = UNDUCKED;

    wIGPermitName[0] = 0;
    wIGTownCircleName[0] = 0;

    (void)memset(sPrintedRegionMap, 0, sizeof(sPrintedRegionMap));
    (void)memset(sRegisterGroups, 0, sizeof(sRegisterGroups));
    (void)memset(sPrintedVCRs, 0, sizeof(sPrintedVCRs));

    // Get the fonts, region maps, and as many of the graphic components
    // as possible from Flash
#ifdef IG_TEST_DATA
    lErrCode = IG_Reload(NumFakeFonts, bNumRecords, bNumComponents, &FakeFontData,
                         pbRegionMap, psCompData);
#else
    lErrCode = IG_Reload(bNumFonts, bNumRecords, bNumComponents, psFontData,
                         pbRegionMap, psCompData);
#endif

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_Reload
// DESCRIPTION: Reloads the data for the fonts, the region maps, and as many
//              of the graphic components as possible.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bNumFonts = number of fonts described in IG_FONT_DATA structure
//         bNumRecords = number of records in the region map in Flash
//         bNumComponents = number of components described in IG_COMPONENT_DATA structure
//         psFontData = pointer to an IG_FONT_DATA structure, which has the IDs
//                      and addresses for all the fonts
//         pbRegionMap = pointer to the region map in Flash
//         psCompData = pointer to an IG_COMPONENT_DATA structure, which has the IDs
//                      and addresses for all the necessary components
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_Reload(const unsigned char bNumFonts, const unsigned char bNumRecords,
                         const unsigned char bNumComponents, const IG_FONT_DATA *psFontData,
                         const unsigned char *pbRegionMap, const IG_COMPONENT_DATA *psCompData)
{
    REGION_MAP      *psRegionMap, *psFlashRegionMap;
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned char   bInx1, bInx2;
    unsigned char   bCompInx;
    unsigned char   bCompType;
    unsigned char   bEnd = 0;
    unsigned char   bType[MAX_TYPES];
    unsigned char   bStatus;
    unsigned char   bIndex;
    char            pLogBuf[50];
    BOOL    fbFound = FALSE;
    BOOL    fbEnabled;


/*
#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "Start IG_Reload");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
*/

    // Initialize variables that need to be initialized both at powerup and on reloads
    wNextVCR = 0;
    wTotalVCRs = 0;

    bCurrentTC = 0;
    bIndiciaComponent = UNKNOWN;
    bNumOfFonts = bNumFonts;
    bNumOfImages = 0;
    bNumOfTCs = 0;
    bPrintedRegions = 0;
    bTotalGroups = 0;

    fbImageBufferState = RELOAD;
    fbImageGenFailed = FALSE;
    fbLaggingRequired = fnFlashGetByteParm(BP_LAGGING_BAR_REQUIRED);

    for (bInx1 = 0; bInx1 < MAX_DIRTY; bInx1++)
        fbDirtyStuff[bInx1] = TRUE;

    // Clear out the font data
    (void)memset(sFontData, 0, sizeof(sFontData));

    // Get the fonts from Flash
    lErrCode = GetFonts(bNumFonts, psFontData);
    if (lErrCode != IG_NO_ERROR)
    {
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
        return (lErrCode);
    }

    // Clear out the ad structures
    (void)memset(&sAdData, 0, sizeof(sAdData));
    sAdData.fbLoaded = FALSE;

    // Get the ad if necessary
    if (bAdID != 0)
    {
        lErrCode = SearchStructure(IMG_ID_AD_SLOGAN, bNumComponents, psCompData, &bCompInx);
        if (lErrCode == IG_NO_ERROR)
            lErrCode = GetImage(IMG_ID_AD_SLOGAN, psCompData->pbHeaderAddr[bCompInx], 
                                psCompData->pbDataAddr[bCompInx], FALSE, FALSE);
    }

    if (lErrCode != IG_NO_ERROR)
    {
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
        return (lErrCode);
    }

    // Clear out the inscription structure
    (void)memset(&sInscrData, 0, sizeof(sInscrData));
    sInscrData.fbLoaded = FALSE;

    // Get the inscription if necesssary
    if (bInscrID != 0)
    {
        lErrCode = SearchStructure(IMG_ID_INSCRIPTION, bNumComponents, psCompData, &bCompInx);
        if (lErrCode == IG_NO_ERROR)
            lErrCode = GetImage(IMG_ID_INSCRIPTION, psCompData->pbHeaderAddr[bCompInx], 
                                    psCompData->pbDataAddr[bCompInx], FALSE, FALSE);
    }

    if (lErrCode != IG_NO_ERROR)
    {
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
        return (lErrCode);
    }

    // Initialize bNumFlashRegions
    (void)memset(bNumFlashRegions, 0, MAX_TYPES);

    // Clear out the region maps
    (void)memset(sFlashRegionMap, 0, sizeof(sFlashRegionMap));

    // To satisfy lint, clear the bType array
    (void)memset(bType, 0, sizeof(bType));

    // Get the number of records in the RegionMap array in Flash
    bTotalRegions = bNumRecords;

    // Get the address of the RegionMap array in Flash
    psRegionMap = (REGION_MAP *)pbRegionMap;

    // Find the type and number of records for each print area
    for (bInx1 = 0, bInx2 = 0;
         (bInx1 < bTotalRegions) && (lErrCode == IG_NO_ERROR) && (bInx2 < MAX_TYPES-1);
         bInx1++, psRegionMap++)
    {
        if (psRegionMap->bAreaID != bInx2)
        {
            if (fbFound == TRUE)
            {
                if (bInx1 == bEnd)
                {
                    lErrCode = IG_INVALID_REGION_MAP;
                    continue;
                }

                bNumFlashRegions[bType[bInx2]] = bInx1 - bEnd;
                bEnd = bInx1;
                fbFound = FALSE;
                bInx2++;
            }
            else
            {
                lErrCode = IG_INVALID_REGION_MAP;
                continue;
            }
        }

        switch (psRegionMap->bComponentType)
        {
        case IMG_ID_NORMAL_INDICIA:
            if(fbFound == FALSE)
            {
                fbFound = TRUE;
                bType[bInx2] = INDICIA;
                if (bIndiciaComponent != LEADING_LAGGING)
                {
                    bIndiciaComponent = NORMAL_LOW;
                    bNormalIndiciaComponent = IMG_ID_NORMAL_INDICIA;
                }
                else
                    lErrCode = IG_INVALID_REGION_MAP;
            }
            else
            {
                lErrCode = IG_INVALID_REGION_MAP;
            }
            break;

        case IMG_BAR_LEADING_IMAGE:
        case IMG_BAR_LAGGING_IMAGE:
            if(fbFound == FALSE)
            {
                fbFound = TRUE;
                bType[bInx2] = INDICIA;
                if (bIndiciaComponent != NORMAL_LOW)
                    bIndiciaComponent = LEADING_LAGGING;
                else
                    lErrCode = IG_INVALID_REGION_MAP;
            }
            else
            {
                if (bType[bInx2] != INDICIA)
                    lErrCode = IG_INVALID_REGION_MAP;
            }
            break;

        case IMG_ID_PERMIT1_PE:
            if(fbFound == FALSE)
            {
                fbFound = TRUE;
                bType[bInx2] = PERMIT;
            }
            else
            {
                lErrCode = IG_INVALID_REGION_MAP;
            }
            break;

        case IMG_ID_TAX_GRAPHIC:
            if(fbFound == FALSE)
            {
                fbFound = TRUE;
                bType[bInx2] = TAX;
            }
            else
            {
                lErrCode = IG_INVALID_REGION_MAP;
            }
            break;

        case IMG_ID_DATE_TIME:
            if(fbFound == FALSE)
            {
                fbFound = TRUE;
                bType[bInx2] = DATE_TIME;
            }
            else
            {
                lErrCode = IG_INVALID_REGION_MAP;
            }
            break;

        default:
            break;
        }
    }

    if (fbFound == TRUE)
    {
        if (bInx1 == bEnd)
        {
            lErrCode = IG_INVALID_REGION_MAP;
        }
        else
        {
            bNumFlashRegions[bType[bInx2]] = bInx1 - bEnd;
            bInx2++;
        }
    }

    if (lErrCode != IG_NO_ERROR)
    {
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
        return (lErrCode);
    }

    // Get the address of the RegionMap array in Flash
    psRegionMap = (REGION_MAP *)pbRegionMap;

    // copy the data from flash into the appropriate region map
    bEnd = bInx2;
    for (bInx1 = 0; bInx1 < bEnd; bInx1++)
    {
        switch (bType[bInx1])
        {
        case INDICIA:
            psFlashRegionMap = &sFlashRegionMap[INDICIA][0];
            break;

        case PERMIT:
            psFlashRegionMap = &sFlashRegionMap[PERMIT][0];
            break;

        case TAX:
            psFlashRegionMap = &sFlashRegionMap[TAX][0];
            break;

        default:
            psFlashRegionMap = &sFlashRegionMap[DATE_TIME][0];
            break;
        }

        for (bInx2 = 0; bInx2 < bNumFlashRegions[bType[bInx1]];
             bInx2++, psFlashRegionMap++, psRegionMap++)
        {
            (void)memcpy((unsigned char *)psFlashRegionMap, (unsigned char *)psRegionMap,
                    REGION_RECORD_SIZE);
        }
    }

    // if permit mode isn't enabled, pretend that the permit region map doesn't exist.
    // this must be done AFTER the region maps are copied because if there is another
    // region map after the permit region map, we need to copy the permit region map
    // to get to the proper address for the next region map.
    if (IsFeatureEnabled(&fbEnabled, PERMIT_MODE_SUPPORT, &bStatus, &bIndex))
    {
        if (!fbEnabled)
            bNumFlashRegions[PERMIT] = 0;
    }

    // Clear out the data structures for the various graphics
    (void)memset(&sBarCodeData, 0, sizeof(sBarCodeData));
    sBarCodeData.fbLoaded = FALSE;

    (void)memset(&sDateTimeData, 0, sizeof(sDateTimeData));
    sTaxData.fbLoaded = FALSE;

    (void)memset(&sEBData, 0, sizeof(sEBData));
    sEBData.fbLoaded = FALSE;

    (void)memset(sIndiciaData, 0, sizeof(sIndiciaData));
    for (bInx1 = 0; bInx1 < MAX_IMAGES; bInx1++)
        sIndiciaData[bInx1].fbLoaded = FALSE;

    (void)memset(&sMiscData, 0, sizeof(sMiscData));
    sMiscData.fbLoaded = FALSE;

    (void)memset(&sPermitData, 0, sizeof(sPermitData));
    sPermitData.fbLoaded = FALSE;

    (void)memset(&sReportTestData, 0, sizeof(sInscrData));
    sInscrData.fbLoaded = FALSE;

    (void)memset(&sTaxData, 0, sizeof(sTaxData));
    sTaxData.fbLoaded = FALSE;

    (void)memset(sTCData, 0, sizeof(sTCData));
    for (bInx1 = 0; bInx1 < MAX_TCS; bInx1++)
        sTCData[bInx1].fbLoaded = FALSE;

    (void)memset(&sTextEntryData, 0, sizeof(sTextEntryData));
    sTextEntryData.fbLoaded = FALSE;

    // Get the graphic component data as needed from Flash
    for (bInx1 = 0; (bInx1 < MAX_TYPES) && (lErrCode == IG_NO_ERROR); bInx1++)
    {
        if (bNumFlashRegions[bInx1] == 0)
            continue;

        switch (bInx1)
        {
        case INDICIA:
            psFlashRegionMap = &sFlashRegionMap[INDICIA][0];
            break;

        case PERMIT:
            psFlashRegionMap = &sFlashRegionMap[PERMIT][0];
            break;

        case TAX:
            psFlashRegionMap = &sFlashRegionMap[TAX][0];
            break;

        default:
            psFlashRegionMap = &sFlashRegionMap[DATE_TIME][0];
            break;
        }

        for (bInx2 = 0; (bInx2 < bNumFlashRegions[bInx1]) && (lErrCode == IG_NO_ERROR);
             bInx2++, psFlashRegionMap++)
        {
            bCompType = psFlashRegionMap->bComponentType;
            switch (bCompType)
            {
            case IMG_ID_AD_SLOGAN:
            case IMG_ID_INSCRIPTION:
                break;

            case IMG_ID_NORMAL_INDICIA:
                switch (bIndiciaType)
                {
                case NORMAL:
                    lErrCode = SearchStructure(IMG_ID_NORMAL_INDICIA, bNumComponents,
                                                psCompData, &bCompInx);
                    if (lErrCode == IG_NO_ERROR)
                        lErrCode = GetImage(IMG_ID_NORMAL_INDICIA,
                                            psCompData->pbHeaderAddr[bCompInx], 
                                            psCompData->pbDataAddr[bCompInx],
                                            FALSE, FALSE);
                    break;

                case LOW:
                    lErrCode = SearchStructure(IMG_ID_LOW_INDICIA, bNumComponents,
                                                psCompData, &bCompInx);
                    if (lErrCode == IG_NO_ERROR)
                        lErrCode = GetImage(IMG_ID_LOW_INDICIA,
                                            psCompData->pbHeaderAddr[bCompInx], 
                                            psCompData->pbDataAddr[bCompInx],
                                            FALSE, FALSE);
                    break;

                default:
                    break;
                }
                break;

            case IMG_BAR_LEADING_IMAGE:
                {
                unsigned char   bLeadInx;

                fbFound = FALSE;
                for (bLeadInx = 0; 
                     (bLeadInx < MAX_LEAD_IMAGES) && (fbFound == FALSE);
                     bLeadInx++)
                {
                    switch (bLeadInx)
                    {
                    case 0:
                        lErrCode = SearchStructure(IMG_BAR_LEADING_IMAGE, bNumComponents,
                                                    psCompData, &bCompInx);
                        if (lErrCode == IG_NO_ERROR)
                        {
                            fbFound = TRUE;
                            lErrCode = GetImage(IMG_BAR_LEADING_IMAGE,
                                                psCompData->pbHeaderAddr[bCompInx], 
                                                psCompData->pbDataAddr[bCompInx],
                                                FALSE, FALSE);
                        }
                        break;

                    case 1:
                        lErrCode = SearchStructure(IMG_ID_INTERNAT_LEAD, bNumComponents,
                                                    psCompData, &bCompInx);
                        if (lErrCode == IG_NO_ERROR)
                        {
                            fbFound = TRUE;
                            lErrCode = GetImage(IMG_ID_INTERNAT_LEAD,
                                                psCompData->pbHeaderAddr[bCompInx], 
                                                psCompData->pbDataAddr[bCompInx],
                                                FALSE, FALSE);
                        }
                        break;

                    case 2:
                        lErrCode = SearchStructure(IMG_ID_LEADING_IMAGE_3, bNumComponents,
                                                    psCompData, &bCompInx);
                        if (lErrCode == IG_NO_ERROR)
                        {
                            fbFound = TRUE;
                            lErrCode = GetImage(IMG_ID_LEADING_IMAGE_3,
                                                psCompData->pbHeaderAddr[bCompInx], 
                                                psCompData->pbDataAddr[bCompInx],
                                                FALSE, FALSE);
                        }
                        break;

                    case 3:
                        lErrCode = SearchStructure(IMG_ID_LEADING_IMAGE_4, bNumComponents,
                                                    psCompData, &bCompInx);
                        if (lErrCode == IG_NO_ERROR)
                        {
                            fbFound = TRUE;
                            lErrCode = GetImage(IMG_ID_LEADING_IMAGE_4,
                                                psCompData->pbHeaderAddr[bCompInx], 
                                                psCompData->pbDataAddr[bCompInx],
                                                FALSE, FALSE);
                        }
                        break;

                    case 4:
                        lErrCode = SearchStructure(IMG_ID_LEADING_IMAGE_5, bNumComponents,
                                                    psCompData, &bCompInx);
                        if (lErrCode == IG_NO_ERROR)
                        {
                            fbFound = TRUE;
                            lErrCode = GetImage(IMG_ID_LEADING_IMAGE_5,
                                                psCompData->pbHeaderAddr[bCompInx], 
                                                psCompData->pbDataAddr[bCompInx],
                                                FALSE, FALSE);
                        }
                        break;

                    case 5:
                        lErrCode = SearchStructure(IMG_ID_LEADING_IMAGE_6, bNumComponents,
                                                    psCompData, &bCompInx);
                        if (lErrCode == IG_NO_ERROR)
                        {
                            fbFound = TRUE;
                            lErrCode = GetImage(IMG_ID_LEADING_IMAGE_6,
                                                psCompData->pbHeaderAddr[bCompInx], 
                                                psCompData->pbDataAddr[bCompInx],
                                                FALSE, FALSE);
                        }
                        break;

                    default:
                        break;
                    }

                    if (fbFound == TRUE)
                        break;
                }

                // if we didn't find a leading indicia graphic, declare an error.
                if (fbFound == FALSE)
				{
				    (void)sprintf(pLogBuf, "Reload: Missing leading graphic");
				    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
					lErrCode = IG_MISSING_COMPONENT;
				}
                }
                break;

            case IMG_BAR_LAGGING_IMAGE:
                {
                unsigned char   bLeadInx;

                fbFound = FALSE;
                for (bLeadInx = 0; 
                     (bLeadInx < MAX_LAG_IMAGES) && (fbFound == FALSE);
                     bLeadInx++)
                {
                    switch (bLeadInx)
                    {
                    case 0:
                        lErrCode = SearchStructure(IMG_BAR_LAGGING_IMAGE, bNumComponents,
                                                    psCompData, &bCompInx);
                        if (lErrCode == IG_NO_ERROR)
                        {
                            fbFound = TRUE;
                            lErrCode = GetImage(IMG_BAR_LAGGING_IMAGE,
                                                psCompData->pbHeaderAddr[bCompInx], 
                                                psCompData->pbDataAddr[bCompInx],
                                                FALSE, FALSE);
                        }
                        break;

                    case 1:
                        lErrCode = SearchStructure(IMG_ID_INTERNAT_LAG, bNumComponents,
                                                    psCompData, &bCompInx);
                        if (lErrCode == IG_NO_ERROR)
                        {
                            fbFound = TRUE;
                            lErrCode = GetImage(IMG_ID_INTERNAT_LAG,
                                                psCompData->pbHeaderAddr[bCompInx], 
                                                psCompData->pbDataAddr[bCompInx],
                                                FALSE, FALSE);
                        }
                        break;


                    default:
                        break;
                    }

                    if (fbFound == TRUE)
                        break;
                }

                // if we didn't find a lagging indicia graphic, check if we have to have one.
                if (fbFound == FALSE)
                {
                    // if a lagging graphic is required, declare an error.
                    // else, clear any error.
                    if (fbLaggingRequired == LAG_ALWAYS_REQUIRED)
					{
					    (void)sprintf(pLogBuf, "Reload: Missing lagging graphic");
					    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
						lErrCode = IG_MISSING_COMPONENT;
					}
                    else
                        lErrCode = IG_NO_ERROR;
                }
                }
                break;

            case IMG_ID_PERMIT1_PE:
                if (bPermitType != UNKNOWN)
                {
                    lErrCode = SearchStructureForPermit(bPermitType, bNumComponents,
                                                        psCompData, &bCompInx);
                    if (lErrCode == IG_NO_ERROR)
                        lErrCode = GetImage(bPermitType,
                                            psCompData->pbHeaderAddr[bCompInx], 
                                            psCompData->pbDataAddr[bCompInx],
                                            FALSE, FALSE);
                }
                break;

            case IMG_ID_BAR_GRAPHIC:
                lErrCode = SearchStructure(bCompType, bNumComponents,
                                            psCompData, &bCompInx);
                if (lErrCode == IG_NO_ERROR)
					lErrCode = GetBarcode(IMG_ID_BAR_GRAPHIC,
											psCompData->pbHeaderAddr[bCompInx],
											psCompData->pbDataAddr[bCompInx]);
                break;

            case IMG_ID_TEXT_ENTRY:
                // Check if the component is in the list.
                // If it isn't, check if the text entry messages feature is enabled.
                //    If it isn't, clear the error.
                //    Else, leave the error code as is.
                // Else, attempt to get the component data.
                lErrCode = SearchStructure(bCompType, bNumComponents,
                                            psCompData, &bCompInx);
                if (lErrCode != IG_NO_ERROR)
                {
                    if (IsFeatureEnabled(&fbEnabled, MANUAL_TEXT_MSGS_SUPPORT, &bStatus, &bIndex))
                    {
                        if (!fbEnabled)
                            lErrCode = IG_NO_ERROR;
                    }
                }
                else
                    lErrCode = GetImage(bCompType, psCompData->pbHeaderAddr[bCompInx], 
                                        psCompData->pbDataAddr[bCompInx], TRUE, FALSE);
                break;

            case IMG_ID_TOWN_CIRCLE:
            case IMG_ID_TOWN_CIRCLE_2:
                lErrCode = SearchStructure(bCompType, bNumComponents,
                                            psCompData, &bCompInx);
                if (lErrCode == IG_NO_ERROR)
                    lErrCode = GetTC(bCompType, TRUE,
                                        psCompData->pbHeaderAddr[bCompInx],
                                        psCompData->pbDataAddr[bCompInx]);
                break;

            default:
                lErrCode = SearchStructure(bCompType, bNumComponents,
                                            psCompData, &bCompInx);
                if (lErrCode == IG_NO_ERROR)
                    lErrCode = GetImage(bCompType, psCompData->pbHeaderAddr[bCompInx], 
                                        psCompData->pbDataAddr[bCompInx], TRUE, FALSE);
                break;
            }
        }
    }

    // Clear out or initialize data as appropriate based on the currently selected image type
    switch(bImageType)
    {
    case TEST_PRINT:
    case REPORT:
        // make sure any previous image is erased
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        break;

    default:
        break;
    }

    if (lErrCode != IG_NO_ERROR)
    {
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }

/*
#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "End IG_Reload");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
*/
    return (lErrCode);
}

/*****************************************************************************
//                           PRINT AREA FUNCTIONS
*****************************************************************************/
/****************************************************************************
// FUNCTION NAME: IG_LoadAdInscr
// DESCRIPTION: Loads an ad and/or inscription into the print area. The various
//              parts of the print area are only loaded as necessary.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: psBlob = pointer to an IG_BLOB_DATA structure which has the data
//                  for the static VCRs
//         bNumBlobItems = number of items in the IG_BLOB_DATA structure
//
// OUTPUTS: Error code
//          *pbPrintBuf = starting address of the print area
//          wTotalCols = total number of columns for the printed image
//          wMinCols = same as wTotalCols
*****************************************************************************/

unsigned long IG_LoadAdInscr(unsigned char **pbPrintBuf, unsigned short *pwTotalCols,
                                unsigned short *pwMinCols,
                                const IG_BLOB_DATA *psBlob, const unsigned char bNumBlobItems)
{
    REGION_MAP      *psFlashRegionMap;
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned char   bInx, bRegionInx;
    unsigned char   bNumOfRegions;


    *pbPrintBuf = &bImageBuffer[0][0];
    *pwTotalCols = 0;
    *pwMinCols = 0;
    
    if (fbImageGenFailed == TRUE)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (IG_FAILED);
    }

    if (bNumFlashRegions[INDICIA] == 0)
        return (IG_NO_INDICIA_MAP);

    // if the graphics data have been reloaded or the print area doesn't currently
    // contain an ad/inscription combination, redo the entire print area
    if ((fbImageBufferState == RELOAD) || (bImageType != AD_INSCR))
    {
        for (bInx = 0; bInx < MAX_DIRTY; bInx++)
            fbDirtyStuff[bInx] = TRUE;
    }
    
    ClearVariables();
    psFlashRegionMap = &sFlashRegionMap[INDICIA][0];
    bImageType = AD_INSCR;
    bNumOfRegions = bNumFlashRegions[INDICIA];

    if ((fbDirtyStuff[AD_DIRTY] == TRUE) || (fbDirtyStuff[INSCR_DIRTY] == TRUE))
    {
        ClearBuffers();
    }

    for (bRegionInx = 0; (bRegionInx < bNumOfRegions) && (lErrCode == IG_NO_ERROR);
         bRegionInx++, psFlashRegionMap++)
    {
        switch (psFlashRegionMap->bComponentType)
        {
        case IMG_ID_AD_SLOGAN:
        case IMG_ID_INSCRIPTION:
        case IMG_ID_TEXT_ENTRY:
            lErrCode = LoadRest(psFlashRegionMap);
            break;

        default:
            break;
        }
    }

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (lErrCode);
    }

    // Load the static VCRs
    if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
    {
        ClearVCRs(0, wTotalVCRs);
        lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_STATIC, FALSE, NULL);
    }
    else
        lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_STATIC, TRUE, NULL);

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (lErrCode);
    }
    
    // Clear out the rest of the image buffer, the printed region map, the
    // register groups, and the VCR definitions
    lErrCode = ClearRestOfIgStuff();
    
    // Indicate that the printed region map, the
    // register groups, and the VCR definitions are OK
    fbDirtyStuff[REGIONS_DIRTY] = FALSE;
    fbDirtyStuff[REGISTERS_DIRTY] = FALSE;

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
    }
    else
    {
        // load the actual number of columns used
        if (wNewStartCol == 0)
        {
            *pwTotalCols = 1 + END_OFFSET;
        }
        else
        {
            *pwTotalCols = wNewStartCol + END_OFFSET;
        }

        *pwMinCols = *pwTotalCols;
    }

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_LoadDateTime
// DESCRIPTION: Loads a Date/Time print into the print area. The various
//              parts of the print area are only loaded as necessary.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: psBlob = pointer to an IG_BLOB_DATA structure which has the data
//                  for the static VCRs
//         bNumBlobItems = number of items in the IG_BLOB_DATA structure
//
// OUTPUTS: Error code
//          *pbPrintBuf = starting address of the print area
//          wTotalCols = total number of columns for the printed image
//          wMinCols = same as wTotalCols
*****************************************************************************/

unsigned long IG_LoadDateTime(unsigned char **pbPrintBuf, unsigned short *pwTotalCols,
                                unsigned short *pwMinCols,
                                const IG_BLOB_DATA *psBlob, const unsigned char bNumBlobItems)
{
    REGION_MAP      *psFlashRegionMap;
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wRemains;
    unsigned char   bInx, bRegionInx;
    unsigned char   bNumOfRegions;
	char			pLogBuf[50];


    *pbPrintBuf = &bImageBuffer[0][0];
    *pwTotalCols = 0;
    *pwMinCols = 0;
    
    if (fbImageGenFailed == TRUE)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (IG_FAILED);
    }

    if (bNumFlashRegions[DATE_TIME] == 0)
        return (IG_NO_DATE_TIME_MAP);

    // if the graphics data have been reloaded or the print area doesn't currently
    // contain a date/time graphic, redo the entire print area
    if ((fbImageBufferState == RELOAD) || (bImageType != DATE_TIME))
    {
        for (bInx = 0; bInx < MAX_DIRTY; bInx++)
            fbDirtyStuff[bInx] = TRUE;
    }
    
    ClearVariables();
    psFlashRegionMap = &sFlashRegionMap[DATE_TIME][0];
    bImageType = DATE_TIME;
    bNumOfRegions = bNumFlashRegions[DATE_TIME];

    if (fbDirtyStuff[DATE_TIME_DIRTY] == TRUE)
    {
        ClearBuffers();
    }
    
    for (bRegionInx = 0; (bRegionInx < bNumOfRegions) && (lErrCode == IG_NO_ERROR);
         bRegionInx++, psFlashRegionMap++)
    {
        switch (psFlashRegionMap->bComponentType)
        {
        case IMG_ID_DATE_TIME:
            // if the component loaded at the current spot in the region map isn't the
            // same as the component we want to load, the rest of the regions have to
            // be redone
            if (sPrintedRegionMap[bPrintedRegions].bComponentType != IMG_ID_DATE_TIME)
            {
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }
                        
            if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
            {
                LoadRegionData(psFlashRegionMap);               

                // Load the date/time graphic
                lErrCode = IGU_LoadBackground(sDateTimeData.pbImage, sDateTimeData.wSize,
                                            sPrintedRegionMap[bPrintedRegions].wStartCol,
                                            sPrintedRegionMap[bPrintedRegions].wStartRow, 
                                            sDateTimeData.wWidth, sDateTimeData.wHeight,
                                            sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                            sPrintedRegionMap[bPrintedRegions].wMaxRows);

				if (lErrCode == IG_INVALID_SIZE)
				{
					(void)sprintf(pLogBuf, "Bad Date/Time graphic");
					fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
				}

                if (lErrCode != IG_NO_ERROR)
                    break;
            }
                        
            if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
            {
                lErrCode = LoadRegisterData(sDateTimeData.psFields, sDateTimeData.bNumOfGroups,
                                            sDateTimeData.psVCRs, sDateTimeData.wNumOfVCRs,
                                            sPrintedRegionMap[bPrintedRegions].wStartCol,
                                            sPrintedRegionMap[bPrintedRegions].wStartRow);
            }
            else
            {
                bTotalGroups += sDateTimeData.bNumOfGroups;
                wTotalVCRs += sDateTimeData.wNumOfVCRs;
            }
            
            wRemains = sPrintedRegionMap[bPrintedRegions].wStartCol +
                        sDateTimeData.wWidth;
                        
            // make sure not to go backwards on the number of columns
            if (wRemains > wNewStartCol)
                wNewStartCol = wRemains;
                
            wNewStartRow = sPrintedRegionMap[bPrintedRegions].wStartRow +
                            sDateTimeData.wHeight;
            bPrintedRegions++;
            wNextVCR = wTotalVCRs;
            break;

        case IMG_ID_AD_SLOGAN:
        case IMG_ID_TEXT_ENTRY:
            lErrCode = LoadRest(psFlashRegionMap);
            break;

        default:
            lErrCode = IG_INVALID_COMPONENT;            
            break;
        }
    }

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
        return (lErrCode);
    }

    // Load the static VCRs
    if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
    {
        ClearVCRs(0, wTotalVCRs);
        lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_STATIC, FALSE, NULL);
    }
    else
        lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_STATIC, TRUE, NULL);

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
        return (lErrCode);
    }
    
    // Clear out the rest of the image buffer, the printed region map, the
    // register groups, and the VCR definitions
    lErrCode = ClearRestOfIgStuff();
    
    // Indicate that the date/time print area, the printed region map, the
    // register groups, and the VCR definitions are OK
    fbDirtyStuff[DATE_TIME_DIRTY] = FALSE;
    fbDirtyStuff[REGIONS_DIRTY] = FALSE;
    fbDirtyStuff[REGISTERS_DIRTY] = FALSE;

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }
    else
    {
        // load the actual number of columns used
        *pwTotalCols = wNewStartCol + END_OFFSET;
        *pwMinCols = *pwTotalCols;
    }

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_LoadIndicia
// DESCRIPTION: Loads an indicia print into the print area. The various
//              parts of the print area are only loaded as necessary.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: psBlob = pointer to an IG_BLOB_DATA structure which has the data
//                  for the static VCRs
//         bNumBlobItems = number of items in the IG_BLOB_DATA structure
//
// OUTPUTS: Error code
//          *pbPrintBuf = starting address of the print area
//          wTotalCols = total number of columns for the printed image
//          wMinCols = number of columns for the indicia, barcode and town circle
*****************************************************************************/

unsigned long IG_LoadIndicia(unsigned char **pbPrintBuf, unsigned short *pwTotalCols,
                                unsigned short *pwMinCols,
                                const IG_BLOB_DATA *psBlob, const unsigned char bNumBlobItems)
{
    REGION_MAP      *psFlashRegionMap;
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wRemains;
    unsigned char   bInx, bRegionInx;
    unsigned char   bImageInx;
    unsigned char   bNumOfRegions;
    unsigned char   bDesiredComponent;
	char			pLogBuf[50];
    BOOL            fbLoadIndiciaGraphic;


#ifdef IG_TRY_TEST_PATTERN
    lErrCode = IG_LoadTestPrint(pbPrintBuf, pwTotalCols, pwMinCols,
                                TestPatternHeader, TestPatternData);
    return (lErrCode);
#endif

#ifdef IG_TRY_FUNDS_REPORT
    lErrCode = IG_LoadReport(pbPrintBuf, pwTotalCols, pwMinCols,
                                &FundsReportBlob, FundsReportItems,
                                FundsReportHeader, FundsReportData);
    return (lErrCode);
#endif

#ifdef IG_TRY_REFILL_REPORT
    lErrCode = IG_LoadReport(pbPrintBuf, pwTotalCols, pwMinCols,
                                &RefillReportBlob, RefillReportItems,
                                RefillReportHeader, RefillReportData);
    return (lErrCode);
#endif

#ifdef IG_TRY_AD_INSCR
    if ((bAdID != 0) || (bInscrID != 0))
    {
        lErrCode = IG_LoadAdInscr(pbPrintBuf, pwTotalCols, pwMinCols,
                                    psBlob, bNumBlobItems);
        return (lErrCode);
    }
#endif

#if IG_TEST_DUCKING
    fbTesting = TRUE;
    switch (bAdID & 0x03)
    {
    case 0:
        (void)IG_DuckDate(VLT_DATE_DAY_UNDUCKED);
        (void)IG_DuckTC(VLT_CIRCLE_UNDUCKED);
        break;

    case 1:
        (void)IG_DuckDate(VLT_DUCKED_OUT_DATE);
        (void)IG_DuckTC(VLT_CIRCLE_UNDUCKED);
        break;

    case 2:
        (void)IG_DuckDate(VLT_DATE_DAY_UNDUCKED);
        (void)IG_DuckTC(VLT_DUCKED_OUT_CIRCLE);
        break;

    default:
        (void)IG_DuckDate(VLT_DUCKED_OUT_DATE);
        (void)IG_DuckTC(VLT_DUCKED_OUT_CIRCLE);
        break;
    }

    fbTesting = FALSE;
#endif

    *pbPrintBuf = &bImageBuffer[0][0];
    *pwTotalCols = 0;
    *pwMinCols = 0;
    
    if (fbImageGenFailed == TRUE)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (IG_FAILED);
    }

    if (bNumFlashRegions[INDICIA] == 0)
        return (IG_NO_INDICIA_MAP);

    if (bIndiciaType == UNKNOWN)
        return (IG_NO_INDICIA_SELECTED);

    // if the graphics data have been reloaded or the print area doesn't currently
    // contain an indicia, redo the entire print area
    if ((fbImageBufferState == RELOAD) || (bImageType != INDICIA))
    {
        for (bInx = 0; bInx < MAX_DIRTY; bInx++)
            fbDirtyStuff[bInx] = TRUE;
    }
    
    ClearVariables();
    psFlashRegionMap = &sFlashRegionMap[INDICIA][0];
    bImageType = INDICIA;
    bNumOfRegions = bNumFlashRegions[INDICIA];
    
    if (fbDirtyStuff[INDICIA_DIRTY] == TRUE)
    {
        ClearBuffers();
    }
    
    for (bRegionInx = 0; (bRegionInx < bNumOfRegions) && (lErrCode == IG_NO_ERROR);
         bRegionInx++, psFlashRegionMap++)
    {
        switch (psFlashRegionMap->bComponentType)
        {
        case IMG_ID_NORMAL_INDICIA:
        case IMG_BAR_LEADING_IMAGE:
        case IMG_BAR_LAGGING_IMAGE:
            // start with the first set of data in the indicia data array
            bImageInx = 0;

            // default to loading the indicia graphic
            fbLoadIndiciaGraphic = TRUE;

            // if a normal/low region map, look for a normal or low-value indicia graphic
            if (bIndiciaComponent == NORMAL_LOW)
            {
                if (bIndiciaType == NORMAL)
                    bDesiredComponent = bNormalIndiciaComponent;
                else
                    bDesiredComponent = IMG_ID_LOW_INDICIA;

                if (sIndiciaData[bImageInx].bComponentType != bDesiredComponent)
				{
					(void)sprintf(pLogBuf, "LoadIndicia: Missing indicia, ID = %u", bDesiredComponent);
					fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
					bImageInx = bNumOfImages;
				}
            }
            // else, look for a leading indicia graphic or, if necessary, a lagging indicia graphic
            else
            {
                switch (psFlashRegionMap->bComponentType)
                {
                case IMG_BAR_LEADING_IMAGE:
                    // check if the desired component is in the first set of indicia data
                    switch (sIndiciaData[bImageInx].bComponentType)
                    {
                    case IMG_BAR_LEADING_IMAGE:
                    case IMG_ID_INTERNAT_LEAD:
                    case IMG_ID_LEADING_IMAGE_3:
                    case IMG_ID_LEADING_IMAGE_4:
                    case IMG_ID_LEADING_IMAGE_5:
                    case IMG_ID_LEADING_IMAGE_6:
                        // if so, do nothing
                        break;

                    default:
                        // increment to the next set
                        bImageInx++;

                        // if there is more data,
                        // check if the desired component is in the second set of indicia data
                        if (bImageInx < bNumOfImages)
                        {
                            switch (sIndiciaData[bImageInx].bComponentType)
                            {
                            case IMG_BAR_LEADING_IMAGE:
                            case IMG_ID_INTERNAT_LEAD:
                            case IMG_ID_LEADING_IMAGE_3:
                            case IMG_ID_LEADING_IMAGE_4:
                            case IMG_ID_LEADING_IMAGE_5:
                            case IMG_ID_LEADING_IMAGE_6:
                                // if so, do nothing
                                break;

                            default:
                                // increment to indicate the data wasn't found
								(void)sprintf(pLogBuf, "LoadIndicia: Missing leading indicia");
								fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
                                bImageInx++;
                                break;
                            }
                        }
                        break; // break out of default case for first set of data
                    }
                    break;  // break out of leading image case

                case IMG_BAR_LAGGING_IMAGE:
                default:
                    // check if the desired component is in the first set of indicia data
                    switch (sIndiciaData[bImageInx].bComponentType)
                    {
                    case IMG_BAR_LAGGING_IMAGE:
                    case IMG_ID_INTERNAT_LAG:
					case IMG_ID_SPEC_INDICIA_1:
					case IMG_ID_SPEC_INDICIA_2:
					case IMG_ID_SPEC_INDICIA_3:
					case IMG_ID_SPEC_INDICIA_4:
                        // if so, do nothing
                        break;

                    default:
                        // increment to the next set
                        bImageInx++;

                        // if there is more data,
                        // check if the desired component is in the second set of indicia data
                        if (bImageInx < bNumOfImages)
                        {
                            switch (sIndiciaData[bImageInx].bComponentType)
                            {
                            case IMG_BAR_LAGGING_IMAGE:
                            case IMG_ID_INTERNAT_LAG:
							case IMG_ID_SPEC_INDICIA_1:
							case IMG_ID_SPEC_INDICIA_2:
							case IMG_ID_SPEC_INDICIA_3:
							case IMG_ID_SPEC_INDICIA_4:
                                // if so, do nothing
                                break;

                            default:
                                // if lagging is required, increment to indicate the data wasn't found
                                if (fbLaggingRequired == LAG_ALWAYS_REQUIRED)
								{
									(void)sprintf(pLogBuf, "LoadIndicia: Missing lagging indicia");
									fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
									bImageInx++;
								}
                                // else, indicate that the graphic doesn't need to be loaded
                                else
                                    fbLoadIndiciaGraphic = FALSE;
                                break;
                            }
                        }
                        else
                        {
							// if lagging isn't required, decrement to avoid the data not found check
                            // and indicate that the graphic doesn't need to be loaded
                            if (fbLaggingRequired != LAG_ALWAYS_REQUIRED)
                            {
                                bImageInx--;
                                fbLoadIndiciaGraphic = FALSE;
                            }
                        }
                        break; // break out of default case for first set of data
                    }
                    break;  // break out of lagging image case
                }
            }


            if (bImageInx >= bNumOfImages)
            {
                lErrCode = IG_MISSING_COMPONENT;
                break;
            }

            if (fbLoadIndiciaGraphic)
            {
                // if the component loaded at the current spot in the region map isn't the
                // same as the component we want to load, the rest of the regions have to
                // be redone
                if (sPrintedRegionMap[bPrintedRegions].bComponentType != psFlashRegionMap->bComponentType)
                {
                    fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                    fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
                }

                if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
                {
                    LoadRegionData(psFlashRegionMap);               

                    // Load the indicia background graphic
                    lErrCode = IGU_LoadBackground(sIndiciaData[bImageInx].pbImage,
                                                sIndiciaData[bImageInx].wSize,
                                                sPrintedRegionMap[bPrintedRegions].wStartCol,
                                                sPrintedRegionMap[bPrintedRegions].wStartRow, 
                                                sIndiciaData[bImageInx].wWidth,
                                                sIndiciaData[bImageInx].wHeight,
                                                sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                                sPrintedRegionMap[bPrintedRegions].wMaxRows);

					if (lErrCode == IG_INVALID_SIZE)
					{
						(void)sprintf(pLogBuf, "Bad Indicia graphic, ID = %u", sIndiciaData[bImageInx].bComponentType);
						fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
					}

                    if (lErrCode != IG_NO_ERROR)
                        continue;
                }
            
                if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
                {
                    lErrCode = LoadRegisterData(sIndiciaData[bImageInx].psFields,
                                                sIndiciaData[bImageInx].bNumOfGroups,
                                                sIndiciaData[bImageInx].psVCRs,
                                                sIndiciaData[bImageInx].wNumOfVCRs,
                                                sPrintedRegionMap[bPrintedRegions].wStartCol,
                                                sPrintedRegionMap[bPrintedRegions].wStartRow);
                }
                else
                {
                    bTotalGroups += sIndiciaData[bImageInx].bNumOfGroups;
                    wTotalVCRs += sIndiciaData[bImageInx].wNumOfVCRs;
                }

                wRemains = sPrintedRegionMap[bPrintedRegions].wStartCol +
                            sIndiciaData[bImageInx].wWidth;
                        
                // make sure not to go backwards on the number of columns
                if (wRemains > wNewStartCol)
                    wNewStartCol = wRemains;

                if (wRemains > wReqCols)
                    wReqCols = wRemains;

                wNewStartRow = sPrintedRegionMap[bPrintedRegions].wStartRow +
                            sIndiciaData[bImageInx].wHeight;
                bPrintedRegions++;
                wNextVCR = wTotalVCRs;
            }
            else
            {
                // if the component loaded at the current spot in the region map is the
                // same as the component we want to skip, the rest of the regions have to
                // be redone
                if (sPrintedRegionMap[bPrintedRegions].bComponentType == psFlashRegionMap->bComponentType)
                {
                    fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                    fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
                }
            }
            break;

        case IMG_ID_AD_SLOGAN:
        case IMG_ID_BAR_GRAPHIC:
//        case IMG_ID_ENTGELT:
        case IMG_ID_INSCRIPTION:
        case IMG_ID_MISC_GRAPHIC:
        case IMG_ID_TEXT_ENTRY:
        case IMG_ID_TOWN_CIRCLE:
        case IMG_ID_TOWN_CIRCLE_2:
            lErrCode = LoadRest(psFlashRegionMap);
            break;

        default:
            lErrCode = IG_INVALID_COMPONENT;            
            break;
        }
    }

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
        return (lErrCode);
    }

    // Load the static VCRs
#ifdef IG_TEST_DATA
    if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
    {
        ClearVCRs(0, wTotalVCRs);
        lErrCode = IGU_LoadVCRs(&FakeStaticBlob, FakeStaticItems, IG_STATIC, FALSE, NULL);
    }
    else
        lErrCode = IGU_LoadVCRs(&FakeStaticBlob, FakeStaticItems, IG_STATIC, TRUE, NULL);
#else
    if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
    {
        ClearVCRs(0, wTotalVCRs);
        lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_STATIC, FALSE, NULL);
    }
    else
        lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_STATIC, TRUE, NULL);
#endif

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
        return (lErrCode);
    }
    
    // Clear out the rest of the image buffer, the printed region map, the
    // register groups, and the VCR definitions
    lErrCode = ClearRestOfIgStuff();
    
    // Indicate that the indicia print area, the printed region map, the
    // register groups, and the VCR definitions are OK
    fbDirtyStuff[INDICIA_DIRTY] = FALSE;
    fbDirtyStuff[REGIONS_DIRTY] = FALSE;
    fbDirtyStuff[REGISTERS_DIRTY] = FALSE;

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }
    else
    {
        // load the minimum number of columns needed
        *pwMinCols = wReqCols + END_OFFSET + fnGetMinPrintMargin();

        // load the actual number of columns used
#if defined(IG_TEST_CODE128_1D) || defined(IG_TEST_CODE39_1D) || defined(IG_TEST_CODE25_INTERLEAVED_1D)
        *pwTotalCols = MAX_COLS;
#else
        *pwTotalCols = wNewStartCol + END_OFFSET;
        if (*pwTotalCols < *pwMinCols)
            *pwTotalCols = *pwMinCols;
#endif
    }

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_LoadPermit
// DESCRIPTION: Loads a permit print into the print area. The various parts
//              of the print area are only loaded as necessary.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: psBlob = pointer to an IG_BLOB_DATA structure which has the data
//                  for the static VCRs
//         bNumBlobItems = number of items in the IG_BLOB_DATA structure
//
// OUTPUTS: Error code
//          *pbPrintBuf = starting address of the print area
//          wTotalCols = total number of columns for the printed image
//          wMinCols = number of columns for the permit, barcode and town circle
*****************************************************************************/

unsigned long IG_LoadPermit(unsigned char **pbPrintBuf, unsigned short *pwTotalCols,
                                unsigned short *pwMinCols,
                                const IG_BLOB_DATA *psBlob, const unsigned char bNumBlobItems)
{
    REGION_MAP      *psFlashRegionMap;
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wRemains;
    unsigned char   bInx, bRegionInx;
    unsigned char   bNumOfRegions;
	char			pLogBuf[50];


    *pbPrintBuf = &bImageBuffer[0][0];
    *pwTotalCols = 0;
    *pwMinCols = 0;
    
    if (fbImageGenFailed == TRUE)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (IG_FAILED);
    }

    if (bNumFlashRegions[PERMIT] == 0)
        return (IG_NO_PERMIT_MAP);

    if (bPermitType == UNKNOWN)
        return (IG_NO_PERMIT_SELECTED);

    // if the graphics data have been reloaded or the print area doesn't currently
    // contain a permit, redo the entire print area
    if ((fbImageBufferState == RELOAD) || (bImageType != PERMIT))
    {
        for (bInx = 0; bInx < MAX_DIRTY; bInx++)
            fbDirtyStuff[bInx] = TRUE;
    }
    
    ClearVariables();
    psFlashRegionMap = &sFlashRegionMap[PERMIT][0];
    fbImageBufferState = LOADED;
    bImageType = PERMIT;
    bNumOfRegions = bNumFlashRegions[PERMIT];

    if (fbDirtyStuff[PERMIT_DIRTY] == TRUE)
    {
        ClearBuffers();
    }
    
    for (bRegionInx = 0; (bRegionInx < bNumOfRegions) && (lErrCode == IG_NO_ERROR);
         bRegionInx++, psFlashRegionMap++)
    {
        switch (psFlashRegionMap->bComponentType)
        {
        case IMG_ID_PERMIT1_PE:
            // if the component loaded at the current spot in the region map isn't the
            // same as the component we want to load, the rest of the regions have to
            // be redone
            if (sPrintedRegionMap[bPrintedRegions].bComponentType != psFlashRegionMap->bComponentType)
            {
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }

            if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
            {
                LoadRegionData(psFlashRegionMap);               

                // Load the permit background graphic
                lErrCode = IGU_LoadBackground(sPermitData.pbImage, sPermitData.wSize,
                                            sPrintedRegionMap[bPrintedRegions].wStartCol,
                                            sPrintedRegionMap[bPrintedRegions].wStartRow, 
                                            sPermitData.wWidth, sPermitData.wHeight,
                                            sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                            sPrintedRegionMap[bPrintedRegions].wMaxRows);

				if (lErrCode == IG_INVALID_SIZE)
				{
					(void)sprintf(pLogBuf, "Bad Permit graphic");
					fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
				}

                if (lErrCode != IG_NO_ERROR)
                    continue;
            }

            if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
            {
                lErrCode = LoadRegisterData(sPermitData.psFields, sPermitData.bNumOfGroups,
                                            sPermitData.psVCRs, sPermitData.wNumOfVCRs,
                                            sPrintedRegionMap[bPrintedRegions].wStartCol,
                                            sPrintedRegionMap[bPrintedRegions].wStartRow);
            }
            else
            {
                bTotalGroups += sPermitData.bNumOfGroups;
                wTotalVCRs += sPermitData.wNumOfVCRs;
            }
            
            wRemains = sPrintedRegionMap[bPrintedRegions].wStartCol +
                        sPermitData.wWidth;
                        
            // make sure not to go backwards on the number of columns
            if (wRemains > wNewStartCol)
                wNewStartCol = wRemains;
                
            if (wRemains > wReqCols)
                wReqCols = wRemains;

            wNewStartRow = sPrintedRegionMap[bPrintedRegions].wStartRow +
                            sPermitData.wHeight;
            bPrintedRegions++;
            wNextVCR = wTotalVCRs;
            break;

        case IMG_ID_AD_SLOGAN:
        case IMG_ID_BAR_GRAPHIC:
        case IMG_ID_INSCRIPTION:
        case IMG_ID_MISC_GRAPHIC:
        case IMG_ID_TEXT_ENTRY:
        case IMG_ID_TOWN_CIRCLE:
        case IMG_ID_TOWN_CIRCLE_2:
            lErrCode = LoadRest(psFlashRegionMap);
            break;

        default:
            lErrCode = IG_INVALID_COMPONENT;            
            break;
        }
    }

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
        return (lErrCode);
    }

    // Load the static VCRs
    if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
    {
        ClearVCRs(0, wTotalVCRs);
        lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_STATIC, FALSE, NULL);
    }
    else
        lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_STATIC, TRUE, NULL);

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
        return (lErrCode);
    }
    
    // Clear out the rest of the image buffer, the printed region map, the
    // register groups, and the VCR definitions
    lErrCode = ClearRestOfIgStuff();
    
    // Indicate that the permit print area, the printed region map, the
    // register groups, and the VCR definitions are OK
    fbDirtyStuff[PERMIT_DIRTY] = FALSE;
    fbDirtyStuff[REGIONS_DIRTY] = FALSE;
    fbDirtyStuff[REGISTERS_DIRTY] = FALSE;
    
    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }
    else
    {
        // load the actual number of columns used
        *pwTotalCols = wNewStartCol + END_OFFSET;
        *pwMinCols = wReqCols + END_OFFSET;
    }

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_LoadReport
// DESCRIPTION: Loads the first page of a report into the print area.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: psBlob = pointer to an IG_BLOB_DATA structure which has the data
//                  for the report VCRs
//         bNumBlobItems = number of items in the IG_BLOB_DATA structure
//         pbHeaderAddr = starting address of the header section for the report
//         pbDataAddr = starting address of the data section (after the hash
//                      signature) for the report
//
// OUTPUTS: Error code
//          *pbPrintBuf = starting address of the print area
//          wTotalCols = total number of columns for the printed image
//          wMinCols = same as wTotalCols
*****************************************************************************/

unsigned long IG_LoadReport(unsigned char **pbPrintBuf, unsigned short *pwTotalCols,
                                unsigned short *pwMinCols,
                                const IG_BLOB_DATA *psBlob, const unsigned char bNumBlobItems,
                                const unsigned char *pbHeaderAddr, unsigned char *pbDataAddr)
{
    return (IG_LoadReportWithBarcode(pbPrintBuf, pwTotalCols, pwMinCols, psBlob, bNumBlobItems,
                                    pbHeaderAddr, pbDataAddr, NULL, 0xFFFF));
}


/****************************************************************************
// FUNCTION NAME: IG_LoadReportWithBarcode
// DESCRIPTION: Loads a report that contains a 2D barcode into the print area.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: psBlob = pointer to an IG_BLOB_DATA structure which has the data
//                  for the report VCRs
//         bNumBlobItems = number of items in the IG_BLOB_DATA structure
//         pbHeaderAddr = starting address of the header section for the report
//         pbDataAddr = starting address of the data section (after the hash
//                      signature) for the report
//         pbBarcodeData = starting address of the 2D barcode input data
//         usBarcodeDataLen = length of the 2D barcode input data
//
// OUTPUTS: Error code
//          *pbPrintBuf = starting address of the print area
//          wTotalCols = total number of columns for the printed image
//          wMinCols = same as wTotalCols
*****************************************************************************/

unsigned long IG_LoadReportWithBarcode( unsigned char **pbPrintBuf, unsigned short *pwTotalCols,
                                unsigned short *pwMinCols,
                                const IG_BLOB_DATA *psBlob, const unsigned char bNumBlobItems,
                                const unsigned char *pbHeaderAddr, unsigned char *pbDataAddr,
                                const unsigned char *pbBarcodeData, const unsigned short usBarcodeDataLen)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  usTempLen;
    char            pLogBuf[50];


/*
#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "Start IG_LoadReport");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
*/

    // Initialize output variables
    *pbPrintBuf = &bImageBuffer[0][0];
    *pwTotalCols = 0;
    *pwMinCols = 0;

    lErrCode = GetImage(UNKNOWN, pbHeaderAddr, pbDataAddr, FALSE, TRUE);
    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
        return (lErrCode);
    }

    // Initialize variables
    fbImageBufferState = LOADED;
    bImageType = REPORT;

    // Set the barcode data length
    usTempLen = sBarCodeData.sFlashBarcodeData.wNumberOfDataBytes;
    if (usBarcodeDataLen == 0xFFFF)
        SetNumDataBytes(usTempLen);
    else
        SetNumDataBytes(usBarcodeDataLen);

    // Clear out the print buffer, the printed region map,
    // the register groups, and the VCR data
    (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
    (void)memset(sPrintedRegionMap, 0, sizeof(sPrintedRegionMap));
    (void)memset(sRegisterGroups, 0, sizeof(sRegisterGroups));
    (void)memset(sPrintedVCRs, 0, sizeof(sPrintedVCRs));

    // Load the one and only printed region
    bPrintedRegions = 1;
    sPrintedRegionMap[0].bComponentType = sReportTestData.bComponentType;
    sPrintedRegionMap[0].wMaxCols = IMAGE_COLS;
    sPrintedRegionMap[0].wMaxRows = MAX_ROWS;
    sPrintedRegionMap[0].wStartCol = START_OFFSET;
    sPrintedRegionMap[0].wStartRow = 0;

    // Load the report background graphic
    lErrCode = IGU_LoadBackground(sReportTestData.pbImage, sReportTestData.wSize,
                                    sPrintedRegionMap[0].wStartCol, sPrintedRegionMap[0].wStartRow, 
                                    sReportTestData.wWidth, sReportTestData.wHeight,
                                    sReportTestData.wWidth, sReportTestData.wHeight);

/*
#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "Return from IGU_LoadBackground");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
*/

	if (lErrCode == IG_INVALID_SIZE)
	{
		(void)sprintf(pLogBuf, "Bad Report graphic");
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
	}

    if (lErrCode == IG_NO_ERROR)
    {
	    bTotalGroups = 0;
	    wNextVCR = 0;
	    wTotalVCRs = 0;
    
	    // Load the register group and VCR data from the flash
	    lErrCode = LoadRegisterData(sReportTestData.psFields, sReportTestData.bNumOfGroups,
	                                sReportTestData.psVCRs, sReportTestData.wNumOfVCRs,
	                                sPrintedRegionMap[0].wStartCol, sPrintedRegionMap[0].wStartRow);
    }

/*
#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "Return from LoadRegisterData");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
*/

    if (lErrCode == IG_NO_ERROR)
    {
        // Load the VCRs
        lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_ALL, FALSE, pbBarcodeData);
    }

/*
#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "Return from IGU_LoadVCRs");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
*/

    if (lErrCode == IG_NO_ERROR)
    {
        // load the actual number of columns used
        wNewStartCol = sReportTestData.wWidth + START_OFFSET;
        *pwTotalCols = wNewStartCol + END_OFFSET;
        *pwMinCols = *pwTotalCols;
    }
    else
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }

    // Restore the indicia barcode data length
    SetNumDataBytes(usTempLen);

/*
#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "End IG_LoadReport");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
*/

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_LoadTaxGraphic
// DESCRIPTION: loads a tax graphic print into the print area. The various
//              parts of the print area are only loaded as necessary.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: psBlob = pointer to an IG_BLOB_DATA structure which has the data
//                  for the static VCRs
//         bNumBlobItems = number of items in the IG_BLOB_DATA structure
//
// OUTPUTS: Error code
//          *pbPrintBuf = starting address of the print area
//          wTotalCols = total number of columns for the printed image
//          wMinCols = number of columns for the tax graphic, barcode and town circle
*****************************************************************************/

unsigned long IG_LoadTaxGraphic(unsigned char **pbPrintBuf, unsigned short *pwTotalCols,
                                unsigned short *pwMinCols,
                                const IG_BLOB_DATA *psBlob, const unsigned char bNumBlobItems)
{
    REGION_MAP      *psFlashRegionMap;
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wRemains;
    unsigned char   bInx, bRegionInx;
    unsigned char   bNumOfRegions;
	char			pLogBuf[50];


    *pbPrintBuf = &bImageBuffer[0][0];
    *pwTotalCols = 0;
    *pwMinCols = 0;
    
    if (fbImageGenFailed == TRUE)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (IG_FAILED);
    }

    if (bNumFlashRegions[TAX] == 0)
        return (IG_NO_TAX_MAP);

    // if the graphics data have been reloaded or the print area doesn't currently
    // contain a permit, redo the entire print area
    if ((fbImageBufferState == RELOAD) || (bImageType != TAX))
    {
        for (bInx = 0; bInx < MAX_DIRTY; bInx++)
            fbDirtyStuff[bInx] = TRUE;
    }
    
    ClearVariables();
    psFlashRegionMap = &sFlashRegionMap[TAX][0];
    bImageType = TAX;
    bNumOfRegions = bNumFlashRegions[TAX];
    
    if (fbDirtyStuff[TAX_DIRTY] == TRUE)
    {
        ClearBuffers();
    }
    
    for (bRegionInx = 0; (bRegionInx < bNumOfRegions) && (lErrCode == IG_NO_ERROR);
         bRegionInx++, psFlashRegionMap++)
    {
        switch (psFlashRegionMap->bComponentType)
        {
        case IMG_ID_TAX_GRAPHIC:
            // if the component loaded at the current spot in the region map isn't the
            // same as the component we want to load, the rest of the regions have to
            // be redone
            if (sPrintedRegionMap[bPrintedRegions].bComponentType != psFlashRegionMap->bComponentType)
            {
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }

            if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
            {
                LoadRegionData(psFlashRegionMap);               

                // Load the tax graphic background graphic
                lErrCode = IGU_LoadBackground(sTaxData.pbImage, sTaxData.wSize,
                                            sPrintedRegionMap[bPrintedRegions].wStartCol,
                                            sPrintedRegionMap[bPrintedRegions].wStartRow, 
                                            sTaxData.wWidth, sTaxData.wHeight,
                                            sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                            sPrintedRegionMap[bPrintedRegions].wMaxRows);

				if (lErrCode == IG_INVALID_SIZE)
				{
					(void)sprintf(pLogBuf, "Bad Tax graphic");
					fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
				}

                if (lErrCode != IG_NO_ERROR)
                    continue;
            }
            
            if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
            {
                lErrCode = LoadRegisterData(sTaxData.psFields, sTaxData.bNumOfGroups,
                                            sTaxData.psVCRs, sTaxData.wNumOfVCRs,
                                            sPrintedRegionMap[bPrintedRegions].wStartCol,
                                            sPrintedRegionMap[bPrintedRegions].wStartRow);
            }
            else
            {
                bTotalGroups += sTaxData.bNumOfGroups;
                wTotalVCRs += sTaxData.wNumOfVCRs;
            }
            
            wRemains = sPrintedRegionMap[bPrintedRegions].wStartCol +
                        sTaxData.wWidth;
                        
            // make sure not to go backwards on the number of columns
            if (wRemains > wNewStartCol)
                wNewStartCol = wRemains;
                
            if (wRemains > wReqCols)
                wReqCols = wRemains;

            wNewStartRow = sPrintedRegionMap[bPrintedRegions].wStartRow +
                            sTaxData.wHeight;
            bPrintedRegions++;
            wNextVCR = wTotalVCRs;
            break;

        case IMG_ID_AD_SLOGAN:
        case IMG_ID_BAR_GRAPHIC:
        case IMG_ID_INSCRIPTION:
        case IMG_ID_MISC_GRAPHIC:
        case IMG_ID_TEXT_ENTRY:
        case IMG_ID_TOWN_CIRCLE:
        case IMG_ID_TOWN_CIRCLE_2:
            lErrCode = LoadRest(psFlashRegionMap);
            break;

        default:
            lErrCode = IG_INVALID_COMPONENT;            
            break;
        }
    }

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
        return (lErrCode);
    }

    // Load the static VCRs
    if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
    {
        ClearVCRs(0, wTotalVCRs);
        lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_STATIC, FALSE, NULL);
    }
    else
        lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_STATIC, TRUE, NULL);

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
        return (lErrCode);
    }
    
    // Clear out the rest of the image buffer, the printed region map, the
    // register groups, and the VCR definitions
    lErrCode = ClearRestOfIgStuff();
    
    // Indicate that the permit print area, the printed region map, the
    // register groups, and the VCR definitions are OK
    fbDirtyStuff[TAX_DIRTY] = FALSE;
    fbDirtyStuff[REGIONS_DIRTY] = FALSE;
    fbDirtyStuff[REGISTERS_DIRTY] = FALSE;

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }
    else
    {
        // load the actual number of columns used
        *pwTotalCols = wNewStartCol + END_OFFSET;
        *pwMinCols = wReqCols + END_OFFSET;
    }

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_LoadTestPrint
// DESCRIPTION: Loads a test print into the print area.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: pbHeaderAddr = starting address of the header section for the test pattern
//         pbDataAddr = starting address of the data section (after the hash
//                      signature) for the test pattern
//
// OUTPUTS: Error code
//          *pbPrintBuf = starting address of the print area
//          wTotalCols = total number of columns for the printed image
//          wMinCols = same as wTotalCols
*****************************************************************************/

unsigned long IG_LoadTestPrint( unsigned char **pbPrintBuf, unsigned short *pwTotalCols,
                                unsigned short *pwMinCols,
                                const unsigned char *pbHeaderAddr, unsigned char *pbDataAddr)
{
    unsigned long   lErrCode = IG_NO_ERROR;
	char			pLogBuf[50];


    // Initialize output variables
    *pbPrintBuf = &bImageBuffer[0][0];
    *pwTotalCols = 0;
    *pwMinCols = 0;

    lErrCode = GetImage(IMG_ID_TEST_PRINT, pbHeaderAddr, pbDataAddr, FALSE, FALSE);
    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (lErrCode);
    }

    // Initialize variables
    fbImageBufferState = LOADED;
    bImageType = TEST_PRINT;

    // Clear out the print buffer, the printed region map,
    // the register groups, and the VCR data
    (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
    (void)memset(sPrintedRegionMap, 0, sizeof(sPrintedRegionMap));

    // Load the one and only printed region
    bPrintedRegions = 1;
    sPrintedRegionMap[0].bComponentType = sReportTestData.bComponentType;
    sPrintedRegionMap[0].wMaxCols = IMAGE_COLS;
    sPrintedRegionMap[0].wMaxRows = MAX_ROWS;
    sPrintedRegionMap[0].wStartCol = START_OFFSET;
    sPrintedRegionMap[0].wStartRow = 0;

    // Load the report background graphic
    lErrCode = IGU_LoadBackground(sReportTestData.pbImage, sReportTestData.wSize,
                                    sPrintedRegionMap[0].wStartCol, sPrintedRegionMap[0].wStartRow, 
                                    sReportTestData.wWidth, sReportTestData.wHeight,
                                    sReportTestData.wWidth, sReportTestData.wHeight);

	if (lErrCode == IG_INVALID_SIZE)
	{
		(void)sprintf(pLogBuf, "Bad Report graphic");
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
	}

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
    }
    else
    {
        // load the actual number of columns used
        wNewStartCol = sReportTestData.wWidth + START_OFFSET;
        *pwTotalCols = wNewStartCol + END_OFFSET;
        *pwMinCols = *pwTotalCols;
    }

    return (lErrCode);
}

/*****************************************************************************
//                           SET FUNCTIONS
*****************************************************************************/
/****************************************************************************
// FUNCTION NAME: IG_SetAd
// DESCRIPTION: Sets up the variables for the graphic ad and gets the 
//              graphic ad component.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bID = ad ID
//         pbHeaderAddr = starting address of the header section for the graphic ad
//         pbDataAddr = starting address of the data section (after the hash
//                      signature) for the graphic ad
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_SetAd(const unsigned char bID,
                        const unsigned char *pbHeaderAddr, unsigned char *pbDataAddr)
{
    unsigned long   lErrCode = IG_NO_ERROR;


    if (bAdID == bID)
        return (lErrCode);

    if (bID != 0)
        lErrCode = GetImage(IMG_ID_AD_SLOGAN, pbHeaderAddr, pbDataAddr, FALSE, FALSE);

    if (lErrCode == IG_NO_ERROR)
    {
        bAdID = bID;
        fbDirtyStuff[AD_DIRTY] = TRUE;
    }
    else
    {
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_SetBarcodeType
// DESCRIPTION: Sets up the variables for the 2D barcode and gets the 
//              2D barcode component.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bCompType = barcode graphic component type
//         pbHeaderAddr = starting address of the header section for the 2D barcode
//         pbDataAddr = starting address of the data section (after the hash
//                      signature) for the 2D barcode
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_SetBarcodeType(const unsigned char bCompType,
                                const unsigned char *pbHeaderAddr, unsigned char *pbDataAddr)
{
    unsigned long   lErrCode = IG_NO_ERROR;


    // if setting the same barcode component as before, do nothing
    if (bCompType == sBarCodeData.bComponentType)
        return (lErrCode);

    lErrCode = GetBarcode(bCompType, pbHeaderAddr, pbDataAddr);

    if (lErrCode == IG_NO_ERROR)
    {
        fbBarcodeCleared = FALSE;
        fbDirtyStuff[REGIONS_DIRTY] = TRUE;
    }
    else
    {
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }

    return (lErrCode);
}


/****************************************************************************
// FUNCTION NAME: IG_SetIndiciaType
// DESCRIPTION: Sets up variables for the indicia and gets the indicia
//              component(s).
//
// AUTHOR: Sandra Peterson
//
// INPUTS: fbNormal =   TRUE if normal (or lead/lag) indicia to be used
//                      FALSE if low value indicia to be used
//         bNumComponents = number of components described in IG_COMPONENT_DATA structure
//         psCompData = pointer to an IG_COMPONENT_DATA structure, which has the ID(s)
//                      and addresses for all the necessary component(s)
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_SetIndiciaType(const BOOL fbNormal,
                                const unsigned char bNumComponents, const IG_COMPONENT_DATA *psCompData)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned char   bCompInx = 0;
	char			pLogBuf[50];
    BOOL            fbGotLeading = FALSE;
    BOOL            fbGotLagging = FALSE;


    // if no components were given,
    // return an error because there should be at least 1 component.
    if (!bNumComponents)
	{
		(void)sprintf(pLogBuf, "SetIndiciaType: No components given");
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
		return (IG_MISSING_COMPONENT);
	}

    // if the region map indicated that we're using a normal/low indicia configuration,
    // look for a normal/low indicia graphic
    if (bIndiciaComponent == NORMAL_LOW)
    {
        // if more than 1 component was given,
        // return an error because there should only be 1 component.
        if (bNumComponents != 1)
            return (IG_TOO_MANY_IMAGES);

        if (fbNormal == FALSE)  // want to set to low-value indicia
        {
            // if already set to low-value indicia, just return
            if (bIndiciaType == LOW)
                return (lErrCode);

            // else, clear out the previous indicia data
            bNumOfImages = 0;
            (void)memset(sIndiciaData, 0, sizeof(sIndiciaData));

            switch(psCompData->bType[0])
            {
                case IMG_ID_LOW_INDICIA:
                    lErrCode = GetImage(IMG_ID_LOW_INDICIA,
                                         psCompData->pbHeaderAddr[bCompInx], 
                                         psCompData->pbDataAddr[bCompInx],
                                         FALSE, FALSE);
                    break;

                default:
					(void)sprintf(pLogBuf, "SetIndiciaType: Missing Low Indicia");
					fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
                    lErrCode = IG_MISSING_COMPONENT;
                    break;
            }

            if (lErrCode == IG_NO_ERROR)
                bIndiciaType = LOW;
        }
        else    // want to set to normal indicia
        {
            // if already set to proper normal indicia, just return
			if ((bIndiciaType == NORMAL) &&
				(bNormalIndiciaComponent == psCompData->bType[0]))
				return (lErrCode);

			// else, clear out the previous indicia data
			bNumOfImages = 0;
			(void)memset(sIndiciaData, 0, sizeof(sIndiciaData));

			switch(psCompData->bType[0])
			{
				case IMG_ID_NORMAL_INDICIA:
					lErrCode = GetImage(IMG_ID_NORMAL_INDICIA,
										psCompData->pbHeaderAddr[bCompInx], 
										psCompData->pbDataAddr[bCompInx],
										FALSE, FALSE);
	                break;

                case IMG_ID_LITE_INDICIA_IMAGE:
                    lErrCode = GetImage(IMG_ID_LITE_INDICIA_IMAGE,
                                         psCompData->pbHeaderAddr[bCompInx], 
                                         psCompData->pbDataAddr[bCompInx],
                                         FALSE, FALSE);
                    break;

                case IMG_ID_EXTRA_LITE_INDICIA_IMAGE:
                    lErrCode = GetImage(IMG_ID_EXTRA_LITE_INDICIA_IMAGE,
                                         psCompData->pbHeaderAddr[bCompInx], 
                                         psCompData->pbDataAddr[bCompInx],
                                         FALSE, FALSE);
                    break;

                default:
					(void)sprintf(pLogBuf, "SetIndiciaType: Missing Normal Indicia");
					fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
                    lErrCode = IG_MISSING_COMPONENT;
                    break;
            }

            if (lErrCode == IG_NO_ERROR)
            {
                bIndiciaType = NORMAL;
                bNormalIndiciaComponent = psCompData->bType[0];
            }
        }
    }
    // else, we're using a leading/lagging indicia configuration,
    // so look for the leading/lagging indicia graphic(s)
    else
    {
        // if more than 2 components were given,
        // return an error because there should be no more than 2.
        if (bNumComponents > 2)
            return (IG_TOO_MANY_IMAGES);

        // if a lagging indicia is required, but 2 components weren't given,
        // return an error.
        if ((fbLaggingRequired == LAG_ALWAYS_REQUIRED) && (bNumComponents != 2))
		{
			(void)sprintf(pLogBuf, "SetIndiciaType: Missing Leading/Lagging");
			fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
			return (IG_MISSING_COMPONENT);
		}

        // if the indicia components given are the same as what we already have, just return
        if (bNumComponents == bNumOfImages)
        {
            for ( ; bCompInx < bNumOfImages; bCompInx++)
            {
                if (sIndiciaData[bCompInx].bComponentType != psCompData->bType[bCompInx])
                    break;
            }
        }

        // if we're getting the same graphics we already think we have, and we've successfully
        // made it through at least once, just return
        if ((bCompInx == bNumOfImages) && (bIndiciaType != UNKNOWN))
            return (lErrCode);

        // else, clear out the previous indicia data
        bNumOfImages = 0;
        (void)memset(sIndiciaData, 0, sizeof(sIndiciaData));

        // get the indicia data
        for (bCompInx = 0; bCompInx < bNumComponents; bCompInx++)
        {
            // make sure it's a leading/lagging type of indicia component
            // if not, return an error
            switch (psCompData->bType[bCompInx])
            {
            case IMG_BAR_LEADING_IMAGE:
            case IMG_ID_INTERNAT_LEAD:
            case IMG_ID_LEADING_IMAGE_3:
            case IMG_ID_LEADING_IMAGE_4:
            case IMG_ID_LEADING_IMAGE_5:
            case IMG_ID_LEADING_IMAGE_6:
                if (fbGotLeading)
                {
                    lErrCode = IG_TOO_MANY_IMAGES;
                }
                else
                {
                    fbGotLeading = TRUE;
                    lErrCode = GetImage(psCompData->bType[bCompInx],
                                        psCompData->pbHeaderAddr[bCompInx], 
                                        psCompData->pbDataAddr[bCompInx],
                                        FALSE, FALSE);
                }
                break;

            case IMG_BAR_LAGGING_IMAGE:
            case IMG_ID_INTERNAT_LAG:
			case IMG_ID_SPEC_INDICIA_1:
			case IMG_ID_SPEC_INDICIA_2:
			case IMG_ID_SPEC_INDICIA_3:
			case IMG_ID_SPEC_INDICIA_4:
                if (fbGotLagging)
                {
                    lErrCode = IG_TOO_MANY_IMAGES;
                }
                else
                {
                    fbGotLagging = TRUE;
                    lErrCode = GetImage(psCompData->bType[bCompInx],
                                        psCompData->pbHeaderAddr[bCompInx], 
                                        psCompData->pbDataAddr[bCompInx],
                                        FALSE, FALSE);
                }
                break;

            default:
				(void)sprintf(pLogBuf, "SetIndiciaType:Got bad component,ID=%u", psCompData->bType[bCompInx]);
				fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
                lErrCode = IG_MISSING_COMPONENT;
                break;
            }

            if (lErrCode != IG_NO_ERROR)
                break;
        }

        if (lErrCode == IG_NO_ERROR)
        {
            // make sure we got a leading indicia and, if necessary, a lagging indicia
            if (!fbGotLeading || ((fbLaggingRequired == LAG_ALWAYS_REQUIRED) && !fbGotLagging))
			{
				if (!fbGotLeading)
				{
					(void)sprintf(pLogBuf, "SetIndiciaType: Missing Leading Indicia");
					fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
				}

				if ((fbLaggingRequired == LAG_ALWAYS_REQUIRED) && !fbGotLagging)
				{
					(void)sprintf(pLogBuf, "SetIndiciaType: Missing Req'd Lagging");
					fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
				}

				lErrCode = IG_MISSING_COMPONENT;
			}
            else
                bIndiciaType = NORMAL;  // just so it's set to something other than UNKNOWN
        }
    }

    if (lErrCode == IG_NO_ERROR)
    {
        fbDirtyStuff[INDICIA_DIRTY] = TRUE;
        if (bImageType == INDICIA)
        {
            fbDirtyStuff[REGIONS_DIRTY] = TRUE;
            fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
        }
    }
    else
    {
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }

#ifdef IG_DEMO_AD
    lErrCode = IG_SetAd(2, DemoAdHeader, DemoAdData);
#endif

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_SetInscr
// DESCRIPTION: Sets up the variables for the inscription and gets the
//              inscription component.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bID = inscription ID
//         pbHeaderAddr = starting address of the header section for the inscription
//         pbDataAddr = starting address of the data section (after the hash
//                      signature) for the inscription
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_SetInscr(const unsigned char bID,
                            const unsigned char *pbHeaderAddr, unsigned char *pbDataAddr)
{
    unsigned long   lErrCode = IG_NO_ERROR;


    if (bInscrID == bID)
        return (lErrCode);

    if (bID != 0)
        lErrCode = GetImage(IMG_ID_INSCRIPTION, pbHeaderAddr, pbDataAddr, FALSE, FALSE);

    if (lErrCode == IG_NO_ERROR)
    {
        bInscrID = bID;
        fbDirtyStuff[INSCR_DIRTY] = TRUE;
    }
    else
    {
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_SetPermit
// DESCRIPTION: Sets up variables for the permit and gets the permit component.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bComponentType = permit graphics component ID (permit1, permit2, or permit3)
//         pbHeaderAddr = starting address of the header section for the permit
//         pbDataAddr = starting address of the data section (after the hash
//                      signature) for the permit
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_SetPermit(unsigned char bComponentType,
                            const unsigned char *pbHeaderAddr, unsigned char *pbDataAddr)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wTempName[MAX_PERMIT_UNICHARS+1];
    unsigned char   bByte1, bByte2;
    unsigned char   bInx1 = GRAPHIC_NAME_DESC+2;            // get past the 2 ID bytes
    unsigned char   bInx2 = 0;


    if (bComponentType == 0xFF)
    {
        bComponentType = UNKNOWN;
    }
    else
    {
        // get the name descriptor.
        // can't do a word copy because we can't count on the address
        // being on an even-word boundary.
        for ( ;bInx2 < MAX_PERMIT_UNICHARS; bInx2++)
        {
            bByte1 = pbHeaderAddr[bInx1];
            bByte2 = pbHeaderAddr[bInx1+1];
            if ((bByte1 == 0) && (bByte2 == 0))
                break;
            else
            {
                wTempName[bInx2] = (bByte1 * 16) + bByte2;
                bInx1 += 2;
            }
        }
    }

    wTempName[bInx2] = 0;

    if ((bPermitType == bComponentType) && (!unistrcmp(wIGPermitName, wTempName)))
        return (lErrCode);
    
    if (bComponentType == UNKNOWN)
        sPermitData.fbLoaded = FALSE;
    else
        lErrCode = GetImage(bComponentType, pbHeaderAddr, pbDataAddr, FALSE, FALSE);

    if (lErrCode == IG_NO_ERROR)
    {
        bPermitType = bComponentType;
        (void)unistrcpy(wIGPermitName, wTempName);
        fbDirtyStuff[PERMIT_DIRTY] = TRUE;
        if (bImageType == PERMIT)
        {
            fbDirtyStuff[REGIONS_DIRTY] = TRUE;
            fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
        }
    }
    else
    {
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }

    return (lErrCode);
}

/*****************************************************************************
//                           LOAD FUNCTIONS
*****************************************************************************/
/****************************************************************************
// FUNCTION NAME: IG_ClearBarcode
// DESCRIPTION: If a barcode region exists, it's cleared.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: None
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_ClearBarcode(void)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    char            pLogBuf[50];


#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "Start IG_ClearBarcode");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

    if (fbImageGenFailed == TRUE)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (IG_FAILED);
    }

    if (fbHasBarcode == TRUE)
    {
        if (fbImageBufferState == RELOAD)
        {
            (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
	        bImageType = UNKNOWN;
	        return (IG_NEED_RELOAD);
	    }

	    switch(bImageType)
	    { 
	        case INDICIA:
	        case PERMIT:
	        case TAX:
	            break;

	        default:
	            return (IG_WRONG_STATE);
	    }

        if (fbBarcodeCleared != TRUE)
	    {
	        // clear out the barcode region
			// if the barcode is in a VCR, clear the whole region because it's a fake region.
			// else, only clear as much as the actual width of the barcode because if a meter has more than 1
			// 2D barcode, the region is the size of the biggest barcode. We only want to clear the space we
			// need for the barcode we're going to generate.
			switch(sBarCodeData.sFlashBarcodeData.bBarcodeType & GET_BARCODE_TYPE)
			{
				case DM_VCR_ASCII_BINARY_ENCODING:
			    case DM_VCR_C40_ONLY_ENCODING:
					lErrCode = IGU_ClearRegion(sPrintedRegionMap[bBarcodeRegion].wStartCol,
												sPrintedRegionMap[bBarcodeRegion].wStartRow,
												sPrintedRegionMap[bPrintedRegions].wMaxCols,
												sPrintedRegionMap[bBarcodeRegion].wMaxRows);
					break;

				default:
		            lErrCode = IGU_ClearRegion(sPrintedRegionMap[bBarcodeRegion].wStartCol,
		                                      sPrintedRegionMap[bBarcodeRegion].wStartRow,
		                                      sBarCodeData.wWidth,
		                                      sPrintedRegionMap[bBarcodeRegion].wMaxRows);
					break;
			}

			if (lErrCode == IG_INVALID_SIZE)
			{
				(void)sprintf(pLogBuf, "Bad 2D Barcode area");
				fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
			}

	        if (lErrCode != IG_NO_ERROR)
	        {
	                fbBarcodeCleared = FALSE;
	        }
	        else
	        {
	            fbBarcodeCleared = TRUE;
	        }
        }

        if (lErrCode == IG_NO_ERROR)
            lErrCode = StartGenDMBarcode();
    }

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }

#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "End IG_ClearBarcode");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_LoadNextReportPage
// DESCRIPTION: Loads the next page of the report.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: psBlob = pointer to an IG_BLOB_DATA structure which has the data
//                  for the report VCRs
//         bNumBlobItems = number of items in the IG_BLOB_DATA structure
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_LoadNextReportPage(const IG_BLOB_DATA *psBlob, const unsigned char bNumBlobItems)
{
    unsigned long   lErrCode = IG_NO_ERROR;

/*
#ifdef IG_TIME_DEBUG
    char            pLogBuf[50];

    (void)sprintf(pLogBuf, "Start IG_LoadNextReportPage");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
*/

    if (fbImageGenFailed == TRUE)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (IG_FAILED);
    }

    if (fbImageBufferState == RELOAD)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (IG_NEED_RELOAD);
    }

    if (bImageType != REPORT)
        return (IG_WRONG_STATE);

    // Load the VCRs
    lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_ALL, TRUE, NULL);
    if (lErrCode != IG_NO_ERROR)
    {
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }
/*
#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "End IG_LoadNextReportPage");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
*/

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_LoadOnlyVariableVCRs
// DESCRIPTION: Loads all the variable VCRs in the print area.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: psBlob = pointer to an IG_BLOB_DATA structure which has the data
//                  for the variable VCRs
//         bNumBlobItems = number of items in the IG_BLOB_DATA structure
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_LoadOnlyVariableVCRs(const unsigned char *pbDebitData,
                                      const IG_BLOB_DATA *psBlob, const unsigned char bNumBlobItems)
{
    unsigned long   lErrCode = IG_NO_ERROR;

#ifdef IG_TIME_DEBUG
    char            pLogBuf[50];


    (void)sprintf(pLogBuf, "Start Load Only Variables");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

    if (fbImageGenFailed == TRUE)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (IG_FAILED);
    }

    if (fbImageBufferState == RELOAD)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (IG_NEED_RELOAD);
    }

    switch(bImageType)
    { 
        case AD_INSCR:      // Also used for Text Ad.
        case DATE_TIME:
        case INDICIA:
        case PERMIT:
        case TAX:
            break;

        default:
            return (IG_WRONG_STATE);
    }

    // Load the variable VCRs
#if defined (IG_TEST_DATA)
    lErrCode = IGU_LoadVCRs(&FakeVariableBlob, FakeVariableItems, IG_VARIABLE, TRUE, pbDebitData);
#else
    lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_VARIABLE, TRUE, pbDebitData);
#endif

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }

#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "End IG_LoadOnlyVariableVCRs");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_LoadStaticVCRs
// DESCRIPTION: Loads all the static VCRs in the print area.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: psBlob = pointer to an IG_BLOB_DATA structure which has the data
//                  for the static VCRs
//         bNumBlobItems = number of items in the IG_BLOB_DATA structure
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_LoadStaticVCRs(const IG_BLOB_DATA *psBlob, const unsigned char bNumBlobItems)
{
    unsigned long   lErrCode = IG_NO_ERROR;


    if (fbImageGenFailed == TRUE)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (IG_FAILED);
    }

    if (fbImageBufferState == RELOAD)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (IG_NEED_RELOAD);
    }

    switch(bImageType)
    { 
        case AD_INSCR:
        case DATE_TIME:
        case INDICIA:
        case PERMIT:
        case TAX:
            break;

        default:
            return (IG_WRONG_STATE);
    }

    // Clear all the VCRs
    ClearVCRs(0, wTotalVCRs);

    // Load the static VCRs
    lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_STATIC, FALSE, NULL);
    if (lErrCode != IG_NO_ERROR)
    {
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_LoadVariableVCRs
// DESCRIPTION: Loads all the variable VCRs in the print area.  If a barcode
//              region exists, also generates the barcode and loads it into
//              the print area.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: pbDebitData = pointer to the data from the returned debit message
//         psBlob = pointer to an IG_BLOB_DATA structure which has the data
//                  for the variable VCRs
//         bNumBlobItems = number of items in the IG_BLOB_DATA structure
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_LoadVariableVCRs(const unsigned char *pbDebitData,
                                  const IG_BLOB_DATA *psBlob, const unsigned char bNumBlobItems)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned char   *pbBuf;
    unsigned char   bRowByte;
	unsigned char	ucBarcodeType;
    char            pLogBuf[50];


#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "Start Load Variables");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

#ifdef IG_TRY_TEST_PATTERN
    return (lErrCode);
#endif

#ifdef IG_TRY_FUNDS_REPORT
    return (lErrCode);
#endif

#ifdef IG_TRY_REFILL_REPORT
    return (lErrCode);
#endif

    if (fbImageGenFailed == TRUE)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (IG_FAILED);
    }

    if (fbImageBufferState == RELOAD)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        return (IG_NEED_RELOAD);
    }

    switch(bImageType)
    { 
        case AD_INSCR:      // Also used for Text Ads.
        case DATE_TIME:
        case INDICIA:
        case PERMIT:
        case TAX:
            break;

        default:
            return (IG_WRONG_STATE);
    }

    // Load the variable VCRs
#ifdef IG_TEST_DATA
    lErrCode = IGU_LoadVCRs(&FakeVariableBlob, FakeVariableItems, IG_VARIABLE, TRUE, pbDebitData);
#else
    lErrCode = IGU_LoadVCRs(psBlob, bNumBlobItems, IG_VARIABLE, TRUE, pbDebitData);
#endif

    if ((lErrCode == IG_NO_ERROR) && (fbHasBarcode == TRUE))
    {

#ifdef IG_TIME_DEBUG
        (void)sprintf(pLogBuf, "Start Clear Barcode During Load VCRs");
        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
		ucBarcodeType = sBarCodeData.sFlashBarcodeData.bBarcodeType & GET_BARCODE_TYPE;
        if (fbBarcodeCleared != TRUE)
            // clear out the barcode region
            lErrCode = IG_ClearBarcode();

        // If there were no previous problems and the barcode isn't rendered in a VCR,
        // generate the barcode and load it into the barcode region
        if ((lErrCode == IG_NO_ERROR) &&
			(ucBarcodeType != DM_VCR_ASCII_BINARY_ENCODING) &&
            (ucBarcodeType != DM_VCR_C40_ONLY_ENCODING))
            {
                // make sure the barcode element has a valid size and that it
                // isn't bigger than the max size of the region
                if ((sBarCodeData.wWidth == 0) || 
                    (sBarCodeData.wHeight == 0) ||
                    (sBarCodeData.wWidth > sPrintedRegionMap[bBarcodeRegion].wMaxCols) || 
                    (sBarCodeData.wHeight > sPrintedRegionMap[bBarcodeRegion].wMaxRows))
            {
                lErrCode = IG_INVALID_SIZE;
                (void)sprintf(pLogBuf, "2D Barcode bigger than region size");
                fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
            }

            if (lErrCode == IG_NO_ERROR)
            {
                bRowByte = (unsigned char)(sPrintedRegionMap[bBarcodeRegion].wStartRow / 8);
                pbBuf = &bImageBuffer[sPrintedRegionMap[bBarcodeRegion].wStartCol][bRowByte];

                    // Place the barcode into the print area
#ifdef IG_TEST_DATA
                lErrCode = FinishGenDMBarcode(pbBuf, IMAGE_ROW_BYTES, FakeDebitData);
#else
                lErrCode = FinishGenDMBarcode(pbBuf, IMAGE_ROW_BYTES, pbDebitData);
#endif
                fbBarcodeCleared = FALSE;

#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "End Blow Up");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
                }

            if ((lErrCode != IG_NO_ERROR) && (fbBarcodeCleared != TRUE))
            {
                // clear out the barcode region w/o affecting the error code
                (void)IG_ClearBarcode();
            }
        }
    }

    if (lErrCode != IG_NO_ERROR)
    {
        (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
        bImageType = UNKNOWN;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }

#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "End IG_LoadVariableVCRs");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_LoadVariableVCRsAndBCDataLen
// DESCRIPTION: Possibly changes the number of data bytes to be encoded in
//              the barcode. Loads all the variable VCRs in the print area.
//              If a barcode region exists, also generates the barcode and
//              loads it into the print area.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: pbDebitData = pointer to the data from the returned debit message
//         usBarcodeDataLen = number of bytes pointed to by pbDebitData. If
//                     set to 0xFFFF the length from the barcode graphic
//                     component will be used.
//         psBlob = pointer to an IG_BLOB_DATA structure which has the data
//                  for the variable VCRs
//         bNumBlobItems = number of items in the IG_BLOB_DATA structure
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_LoadVariableVCRsAndBCDataLen(  const unsigned char *pbDebitData, const unsigned short usBarcodeDataLen,
                                    const IG_BLOB_DATA *psBlob, const unsigned char bNumBlobItems)
{
    if (usBarcodeDataLen == 0xFFFF)
        SetNumDataBytes(sBarCodeData.sFlashBarcodeData.wNumberOfDataBytes);
    else
        SetNumDataBytes(usBarcodeDataLen);

    return(IG_LoadVariableVCRs(pbDebitData, psBlob, bNumBlobItems));
}

/*****************************************************************************
//                           DUCK FUNCTIONS
*****************************************************************************/

/****************************************************************************
// FUNCTION NAME: IG_AustriaDuckMiscGraphic
// DESCRIPTION: Sets up for the ducking/unducking of the miscellaneous
//				graphic in the print area.
//              Austria version, don't verify type of image durind load indicia
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bDuckState = ducked or unducked
//
// OUTPUTS:	Error code
*****************************************************************************/

unsigned long IG_AustriaDuckMiscGraphic(const BOOL fbDuckState)
{
	unsigned long	lErrCode = IG_NO_ERROR;


	// if the state isn't changing, just return
	if (fbDuckState == fbMiscGraphicState)
		return (lErrCode);

	if (fbImageGenFailed == TRUE)
		return (IG_FAILED);

	fbMiscGraphicState = fbDuckState;
	fbDirtyStuff[MISC_DIRTY] = TRUE;

	return (lErrCode);
}


/****************************************************************************
// FUNCTION NAME: IG_Duck2DBarcode
// DESCRIPTION: Ducks/Unducks the 2D barcode in the print area when the 2D
//              barcode is in it's own print region.
//
//              THIS FUNCTION HAS NO AFFECT ON THE PRINT OF 2D BARCODES IN A VCR!
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bDuckedState = ducked or unducked
//
// OUTPUTS:	Error code
*****************************************************************************/

unsigned long IG_Duck2DBarcode(const BOOL fbDuckState)
{
	unsigned long	lErrCode = IG_NO_ERROR;
	unsigned char	ucBarcodeType;


	ucBarcodeType = sBarCodeData.sFlashBarcodeData.bBarcodeType & GET_BARCODE_TYPE;

	// if the state isn't changing, just return
	// if the 2D barcode is in a VCR, just return
	if ((fbDuckState == fb2dBarcodeState) ||
		(ucBarcodeType == DM_VCR_ASCII_BINARY_ENCODING) ||
		(ucBarcodeType == DM_VCR_C40_ONLY_ENCODING))
		return (lErrCode);

	if (fbImageGenFailed == TRUE)
		return (IG_FAILED);

	fb2dBarcodeState = fbDuckState;
	fbDirtyStuff[BARCODE_DIRTY] = TRUE;

	return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_DuckBatchCount
// DESCRIPTION: Ducks/Unducks the Printed Batch Count digits in the print area.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bDuckedState = ducked or unducked
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_DuckBatchCount(const BOOL fbDuckState)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wStartVCR, wEndVCR, wDesiredField;
    unsigned char   bInx;


    if (fbImageGenFailed == TRUE)
        return (IG_FAILED);

    switch(bImageType)
    { 
        case INDICIA:
            wDesiredField = VCR_PRINTED_IBC;
            break;

        case PERMIT:
            wDesiredField = VCR_PRINTED_PBC;
            break;

        case TAX:
            wDesiredField = VCR_PRINTED_TBC;
            break;

        default:
            return (IG_WRONG_STATE);
    }

    if (fbDuckState == fbBatchCountState[bImageType])
        return (lErrCode);

    fbBatchCountState[bImageType] = fbDuckState;

    for (bInx = 0; bInx < bTotalGroups; bInx++)
    {
        if (sRegisterGroups[bInx].wVariableID == wDesiredField)
    {
        wStartVCR = sRegisterGroups[bInx].wStartRegID;
        wEndVCR = wStartVCR + sRegisterGroups[bInx].bLengthOfField;

        if (fbBatchCountState[bImageType] == DUCKED)
        {
            // load blanks for all the printed batch count VCRS and
            // set the state of all the printed batch count VCRs to "dirty"
            ClearVCRs(wStartVCR, wEndVCR);
        }
        else
        {
            // the printed batch count data will be loaded when IG_LoadVariableVCRs is called
            // force the state of all the printed batch count VCRs to "dirty"
            for (; wStartVCR < wEndVCR; wStartVCR++)
                sPrintedVCRs[wStartVCR].fbDirty = TRUE;
        }

            sRegisterGroups[bInx].fbDirty = TRUE;
        }
    }

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_DuckDate
// DESCRIPTION: Ducks/Unducks the date in the print area.
//              Also Ducks/Unducks the time if it exists.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bDuckState = ducked, day duck, or unducked (using the #defines in bob.h)
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_DuckDate(const unsigned char bDuckState)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wStartVCR, wEndVCR;
    unsigned char   bInx;


#if IG_TEST_DUCKING
    if ((bDateState != VLT_DATE_DAY_UNDUCKED) && (fbTesting == FALSE))
        return (lErrCode);
#endif

    // if the state isn't changing, just return
    if (bDuckState == bDateState)
        return (lErrCode);

    if (fbImageGenFailed == TRUE)
        return (IG_FAILED);

    switch(bImageType)
    { 
        case INDICIA:
        case PERMIT:
        case TAX:
            break;

        default:
            return (IG_WRONG_STATE);
    }

    bDateState = bDuckState;

    for (bInx = 0; bInx < bTotalGroups; bInx++)
    {
        if (sRegisterGroups[bInx].wVariableID == VCR_PRINTED_DATE)
    {
        wStartVCR = sRegisterGroups[bInx].wStartRegID;
        wEndVCR = wStartVCR + sRegisterGroups[bInx].bLengthOfField;

        if (bDateState == VLT_DUCKED_OUT_DATE)
        {
                // load blanks for all the VCRS and
                // set the state of all the VCRs to "dirty"
            ClearVCRs(wStartVCR, wEndVCR);
        }
        else
        {
                // the data will be loaded when IG_LoadVariableVCRs is called.
                // force the state of all the VCRs to "dirty"
            for (; wStartVCR < wEndVCR; wStartVCR++)
                sPrintedVCRs[wStartVCR].fbDirty = TRUE;
        }

            sRegisterGroups[bInx].fbDirty = TRUE;
        }
    }

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_DuckMiscGraphic
// DESCRIPTION: Sets up for the ducking/unducking of the miscellaneous
//              graphic in the print area.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bDuckState = ducked or unducked
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_DuckMiscGraphic(const BOOL fbDuckState)
{
    unsigned long   lErrCode = IG_NO_ERROR;


    // if the state isn't changing, just return
    if (fbDuckState == fbMiscGraphicState)
        return (lErrCode);

    if (fbImageGenFailed == TRUE)
        return (IG_FAILED);

    switch(bImageType)
    { 
        case INDICIA:
        case PERMIT:
        case TAX:
            break;

        default:
            return (IG_WRONG_STATE);
    }

    fbMiscGraphicState = fbDuckState;
    fbDirtyStuff[MISC_DIRTY] = TRUE;

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_DuckPIN
// DESCRIPTION: Ducks/Unducks the PIN digits in the print area.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bDuckedState = ducked or unducked
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_DuckPIN(const BOOL fbDuckState)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wStartVCR, wEndVCR;
    unsigned char   bInx;


    // if the state isn't changing, just return
    if (fbDuckState == fbPINState)
        return (lErrCode);

    if (fbImageGenFailed == TRUE)
        return (IG_FAILED);

    switch(bImageType)
    { 
        case INDICIA:
        case PERMIT:
        case TAX:
            break;

        default:
            return (IG_WRONG_STATE);
    }

    fbPINState = fbDuckState;

    for (bInx = 0; bInx < bTotalGroups; bInx++)
    {
        if (sRegisterGroups[bInx].wVariableID == VCR_PIN)
    {
        wStartVCR = sRegisterGroups[bInx].wStartRegID;
        wEndVCR = wStartVCR + sRegisterGroups[bInx].bLengthOfField;

        if (fbPINState == DUCKED)
        {
            // load blanks for all the PIN VCRS and
            // set the state of all the PIN VCRs to "dirty"
            ClearVCRs(wStartVCR, wEndVCR);
        }
        else
        {
            // the PIN data will be loaded when IG_LoadVariableVCRs is called
            // force the state of all the PIN VCRs to "dirty"
            for (; wStartVCR < wEndVCR; wStartVCR++)
                sPrintedVCRs[wStartVCR].fbDirty = TRUE;
        }

            sRegisterGroups[bInx].fbDirty = TRUE;
        }
    }

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_DuckTC
// DESCRIPTION: Ducks/Unducks the town strip, if it exists, or sets up the
//              ducking/unducking of the town circle.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bDuckedState = ducked or unducked
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_DuckTC(const BOOL fbDuckState)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wStartVCR = 0;
    unsigned short  wEndVCR = 0;
    unsigned char   bInx1, bInx2;
    unsigned char   bNumOfRegions = bNumFlashRegions[bImageType];


#if IG_TEST_DUCKING
    if ((fbTCState == DUCKED) && (fbTesting == FALSE))
        return (lErrCode);
#endif

    // if the state isn't changing, just return
    if (fbDuckState == fbTCState)
        return (lErrCode);

    if (fbImageGenFailed == TRUE)
        return (IG_FAILED);

    switch(bImageType)
    { 
        case INDICIA:
        case PERMIT:
        case TAX:
            break;

        default:
            return (IG_WRONG_STATE);
    }

    fbTCState = fbDuckState;

    // see if there is a town line filed or a zip code VCR field.
    // if there is, set it up appropriately
    for (bInx1 = 0; bInx1 < bTotalGroups; bInx1++)
    {
        if ((sRegisterGroups[bInx1].wVariableID == VCR_TOWN_LINE) ||
            (sRegisterGroups[bInx1].wVariableID == VCR_ZIP_CODE))
        {
            wStartVCR = sRegisterGroups[bInx1].wStartRegID;
            wEndVCR = wStartVCR + sRegisterGroups[bInx1].bLengthOfField;

            if (fbTCState == DUCKED)
            {
                // load blanks for all the VCRS and
                // set the state of all the VCRs to "dirty"
                ClearVCRs(wStartVCR, wEndVCR);
            }
            else
            {
                // the data will be loaded with the rest of the static VCRs
                //  in either IG_LoadIndicia or IG_LoadPermit or IG_LoadTaxGraphic.
                // force the state of all the VCRs to "dirty"
                for (; wStartVCR < wEndVCR; wStartVCR++)
                    sPrintedVCRs[wStartVCR].fbDirty = TRUE;
            }

            sRegisterGroups[bInx1].fbDirty = TRUE;
        }
    }

    // see if there is a town circle region
    for (bInx2 = 0; bInx2 < bNumOfRegions; bInx2++)
        if ((sFlashRegionMap[bImageType][bInx2].bComponentType == IMG_ID_TOWN_CIRCLE) ||
            (sFlashRegionMap[bImageType][bInx2].bComponentType == IMG_ID_TOWN_CIRCLE_2))
            break;

    // if there is a town circle region, indicate that it has to be updated
    if (bInx2 != bNumOfRegions)
        fbDirtyStuff[TC_DIRTY] = TRUE;

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_DuckTextEntry
// DESCRIPTION: Sets up for the ducking/unducking of the text entry
//              graphic in the print area.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bDuckState = ducked or unducked
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_DuckTextEntry(const BOOL fbDuckState)
{
    unsigned long   lErrCode = IG_NO_ERROR;


    // if the state isn't changing, just return
    if (fbDuckState == fbTextEntryState)
        return (lErrCode);

    if (fbImageGenFailed == TRUE)
        return (IG_FAILED);

    fbTextEntryState = fbDuckState;
    fbDirtyStuff[TEXT_ENTRY_DIRTY] = TRUE;

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: IG_DuckTime
// DESCRIPTION: Ducks/Unducks the time in the print area.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bDuckState = ducked, or unducked
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long IG_DuckTime(const unsigned char bDuckState)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wStartVCR, wEndVCR;
    unsigned char   bInx;


    // if the state isn't changing, just return
    if (bDuckState == fbTimeState)
        return (lErrCode);

    if (fbImageGenFailed == TRUE)
        return (IG_FAILED);

    switch(bImageType)
    { 
        case DATE_TIME:
        case INDICIA:
        case PERMIT:
        case TAX:
            break;

        default:
            return (IG_WRONG_STATE);
    }

    fbTimeState = bDuckState;

    for (bInx = 0; bInx < bTotalGroups; bInx++)
    {
        if (sRegisterGroups[bInx].wVariableID == VCR_PRINTED_TIME)
        {
            wStartVCR = sRegisterGroups[bInx].wStartRegID;
            wEndVCR = wStartVCR + sRegisterGroups[bInx].bLengthOfField;

            if (fbTimeState == DUCKED)
        {
            // load blanks for all the time VCRS and
            // set the state of all the time VCRs to "dirty"
            ClearVCRs(wStartVCR, wEndVCR);
        }
        else
        {
            // the time data will be loaded when IG_LoadVariableVCRs is called
            // force the state of all the time VCRs to "dirty"
            for (; wStartVCR < wEndVCR; wStartVCR++)
                sPrintedVCRs[wStartVCR].fbDirty = TRUE;
            }

            sRegisterGroups[bInx].fbDirty = TRUE;
        }
    }

    return (lErrCode);
}

/*****************************************************************************
//                           GET FUNCTIONS
*****************************************************************************/
/*****************************************************************************
//                           SUBROUTINES
*****************************************************************************/
/*****************************************************************************
//                  FUNCTIONS FOR GETTING DATA FROM FLASH
*****************************************************************************/
/*****************************************************************************
// FUNCTION NAME: GetBarcode
// DESCRIPTION: Sets up the sBarCodeData structure
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bCompType = barcode graphic component type
//         pbHeaderAddr = starting address of the header section for the barcode
//         pbDataAddr = starting address of the data section (after the hash
//                      signature) for the barcode
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long GetBarcode(const unsigned char bCompType,
						const unsigned char *pbHeaderAddr, const unsigned char *pbDataAddr)
{
    IG_BARCODE_DATA *psBarCodeData = &sBarCodeData;
    unsigned long   lErrCode = IG_NO_ERROR;
//  unsigned char   bInx;


    // Check that the schema is correct
    if (pbHeaderAddr[GRAPHIC_SCHEMA_OFFSET] != BARCODE_SCHEMA)
    {
        lLastErrCode = IG_SCHEMA_ERROR;
        fbImageGenFailed = TRUE;
        return (IG_SCHEMA_ERROR);
    }

    // Get the necessary data
    pbDataAddr += GRAPHIC_WIDTH;
    EndianAwareCopy((unsigned char *)&psBarCodeData->wWidth, pbDataAddr, 2);
    pbDataAddr += 2;
    EndianAwareCopy((unsigned char *)&psBarCodeData->wHeight, pbDataAddr, 2);
    pbDataAddr += 2;
    psBarCodeData->sFlashBarcodeData.bBarcodeType = *pbDataAddr++;
    psBarCodeData->sFlashBarcodeData.bNumberSymbols = *pbDataAddr++;
    psBarCodeData->sFlashBarcodeData.bQuietZones = *pbDataAddr++;
    psBarCodeData->sFlashBarcodeData.bHorizontalExp = *pbDataAddr++;
    psBarCodeData->sFlashBarcodeData.bVerticalExp = *pbDataAddr++;
    psBarCodeData->sFlashBarcodeData.bMaxRowThinning = *pbDataAddr++;
    psBarCodeData->sFlashBarcodeData.bMaxColumnThinning = *pbDataAddr++;
    psBarCodeData->sFlashBarcodeData.bFileID1 = *pbDataAddr++;
    psBarCodeData->sFlashBarcodeData.bFileID2 = *pbDataAddr++;
    psBarCodeData->sFlashBarcodeData.bSpare = *pbDataAddr++;
    EndianAwareCopy((unsigned char *)&psBarCodeData->sFlashBarcodeData.wNumberOfDataBytes, pbDataAddr, 2);
    pbDataAddr += 2;
    EndianAwareCopy((unsigned char *)&psBarCodeData->sFlashBarcodeData.wExtraPixelsAbove, pbDataAddr, 2);
    pbDataAddr += 2;
    EndianAwareCopy((unsigned char *)&psBarCodeData->sFlashBarcodeData.wExtraPixelsBelow, pbDataAddr, 2);
    pbDataAddr += 2;
    EndianAwareCopy((unsigned char *)&psBarCodeData->sFlashBarcodeData.wBarcodeAreaHeight, pbDataAddr, 2);
    pbDataAddr += 2;
    EndianAwareCopy((unsigned char *)&psBarCodeData->sFlashBarcodeData.wBarcodeAreaWidth, pbDataAddr, 2);
    pbDataAddr += 2;

//  for (bInx = 0; bInx < psBarCodeData->sFlashBarcodeData.bNumberSymbols; bInx++)
//  {
        (void)memcpy((unsigned char *)&psBarCodeData->sFlashBarcodeData.sBarcodeSymbols,
                pbDataAddr, BARCODE_SYMBOL_RECORD_SIZE);
        pbDataAddr += BARCODE_SYMBOL_RECORD_SIZE;
//  }

    psBarCodeData->bComponentType = bCompType;
    psBarCodeData->fbLoaded = TRUE;

    // Setup the barcode data in the barcode generator
    lErrCode = SetupDMBarcode(&(sBarCodeData.sFlashBarcodeData));
    if (lErrCode != IG_NO_ERROR)
    {
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }

    return (lErrCode);
}

/*****************************************************************************
// FUNCTION NAME: GetFonts
// DESCRIPTION: Sets up the array of sFontData structures
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bNumFonts = number of fonts described in IG_FONT_DATA structure
//         psFontData = pointer to an IG_FONT_DATA structure, which has the IDs
//                      and addresses for all the fonts
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long GetFonts(const unsigned char bNumFonts, const IG_FONT_DATA *psFontData)
{
    FONT_DATA       *psInternalFontData;
    unsigned char   *pHeader, *pData;
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned char   bID;
    unsigned char   bInx;
	unsigned char	ucFontSchema;
	unsigned char	ucFontSize[4];


    for (bInx = 0; bInx < bNumFonts; bInx++)
    {
		// Get the font ID and set up the necessary pointers
        bID = psFontData->bFontID[bInx];
        pHeader = psFontData->pbHeaderAddr[bInx];
        pData = psFontData->pbDataAddr[bInx];
        psInternalFontData = &sFontData[bInx];

		// Get some of the font data
		ucFontSchema = pHeader[FONT_SCHEMA_OFFSET];
		psInternalFontData->fbTextAdFont = pHeader[FONT_TEXT_AD_FONT];
		psInternalFontData->bFontID = bID;
		psInternalFontData->pbBitmap = pData;
		pHeader += FONT_SIZE;

		// Check that the schema is correct.
		// If so, get the bitmap size
		switch(ucFontSchema)
		{
			case FONT_SCHEMA:
				(void)memset(ucFontSize, 0, 2);
				(void)memcpy(&ucFontSize[2], pHeader, 2);
				(void)EndianAwareCopy((unsigned char *)&psInternalFontData->ulSize, ucFontSize, 4);
				pHeader += 2;
				break;

			case FONT_SCHEMA_4BYTE:
				//TODO may need endian swap here
				(void)memcpy((unsigned char *)&psInternalFontData->ulSize, pHeader, 4);
				pHeader += 4;
				break;

			default:
				lErrCode = IG_SCHEMA_ERROR;
				lLastErrCode = lErrCode;
				fbImageGenFailed = TRUE;
				break;
		}

		// If everything is OK, get the rest of the data
		if (lErrCode == IG_NO_ERROR)
		{
	        (void)EndianAwareCopy((unsigned char *)&psInternalFontData->wHeight, pHeader, 2);
	        pHeader += 2;
	        (void)EndianAwareCopy((unsigned char *)&psInternalFontData->wWidth, pHeader, 2);
	        pHeader += 2;
	        (void)EndianAwareCopy((unsigned char *)&psInternalFontData->wNumChars, pHeader, 2);
	        pHeader += 2;
	        EndianAwareArray16Copy(psInternalFontData->wCharList, pHeader,
								   (unsigned short)(psInternalFontData->wNumChars * 2));
			
		}
		else
		{
			// something is wrong w/ one of the fonts, so exit the for loop
			break;
		}
    }

    return (lErrCode);
}

/*****************************************************************************
// FUNCTION NAME: GetImage
// DESCRIPTION: Sets up one of the following structures: sAdData, sEBData,
//              sIndiciaData[bNumOfImages], sInscrData, sMiscData, sPermitData,
//              sTaxData, sReportTestData.
//
// AUTHOR: Sandra Peterson
//
// GLOBALS USED: bNumOfImages
//
// INPUTS: bType = graphics component type - ignored for reports
//         pbHeaderAddr = starting address of the header section for the town circle
//         pbDataAddr = starting address of the data section (after the hash
//                      signature) for the town circle
//         fbSkipLoad = TRUE if data should be retrieved only if it already
//                      isn't loaded, FALSE otherwise
//         fbIsReport = TRUE if a report image is desired, FALSE otherwise
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long GetImage(const unsigned char bType,
                        const unsigned char *pbHeaderAddr, unsigned char *pbDataAddr,
                        const BOOL fbSkipLoad, const BOOL fbIsReport)
{
    GRAPHIC_DATA    *psGraphicData;
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wOffset;
    BOOL            fbIncrementImage = FALSE;

/*
#ifdef IG_TIME_DEBUG
    char            pLogBuf[50];

    (void)sprintf(pLogBuf, "Start IG GetImage");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
*/

    switch(bType)
    {
    case IMG_ID_AD_SLOGAN:
        if ((fbSkipLoad == TRUE) && (sAdData.fbLoaded == TRUE))
            return (lErrCode);

        psGraphicData = &sAdData;
        break;

    case IMG_ID_DATE_TIME:
        if ((fbSkipLoad == TRUE) && (sDateTimeData.fbLoaded == TRUE))
            return (lErrCode);

        psGraphicData = &sDateTimeData;
        break;

/*
    case IMG_ID_ENTGELT:
        if ((fbSkipLoad == TRUE) && (sEBData.fbLoaded == TRUE))
            return (lErrCode);

        psGraphicData = &sEBData;
        break;
*/

    case IMG_ID_NORMAL_INDICIA:
    case IMG_ID_LOW_INDICIA:
    case IMG_ID_ACCEPTANCE_INDICIA:
    case IMG_BAR_LEADING_IMAGE:
    case IMG_BAR_LAGGING_IMAGE:
    case IMG_ID_INTERNAT_LEAD:
    case IMG_ID_INTERNAT_LAG:
    case IMG_ID_LEADING_IMAGE_3:
    case IMG_ID_LEADING_IMAGE_4:
    case IMG_ID_LEADING_IMAGE_5:
    case IMG_ID_LEADING_IMAGE_6:
    case IMG_ID_SPEC_INDICIA_1:
    case IMG_ID_SPEC_INDICIA_2:
    case IMG_ID_SPEC_INDICIA_3:
    case IMG_ID_SPEC_INDICIA_4:
        if (bNumOfImages < MAX_IMAGES)
        {
            if ((fbSkipLoad == TRUE) &&
                (sIndiciaData[bNumOfImages].fbLoaded == TRUE) &&
                (sIndiciaData[bNumOfImages].bComponentType == bType))
            {
                bNumOfImages++;
                return (lErrCode);
            }

            fbIncrementImage = TRUE;
            psGraphicData = &sIndiciaData[bNumOfImages];
        }
        else
        {
            lLastErrCode = IG_TOO_MANY_IMAGES;
            fbImageGenFailed = TRUE;
            return (IG_TOO_MANY_IMAGES);
        }
        break;

    case IMG_ID_INSCRIPTION:
        if ((fbSkipLoad == TRUE) && (sInscrData.fbLoaded == TRUE))
            return (lErrCode);

        psGraphicData = &sInscrData;
        break;

    case IMG_ID_MISC_GRAPHIC:
        if ((fbSkipLoad == TRUE) && (sMiscData.fbLoaded == TRUE))
            return (lErrCode);

        psGraphicData = &sMiscData;
        break;

    case IMG_ID_PERMIT1_PE:
    case IMG_ID_PERMIT2_PE:
    case IMG_ID_PERMIT3_PE:
        if ((fbSkipLoad == TRUE) && (sPermitData.fbLoaded == TRUE))
            return (lErrCode);

        psGraphicData = &sPermitData;
        break;

    case IMG_ID_TAX_GRAPHIC:
        if ((fbSkipLoad == TRUE) && (sTaxData.fbLoaded == TRUE))
            return (lErrCode);

        psGraphicData = &sTaxData;
        break;

    case IMG_ID_TEST_PRINT:
        if ((fbSkipLoad == TRUE) && (sReportTestData.fbLoaded == TRUE))
            return (lErrCode);

        psGraphicData = &sReportTestData;
        break;

    case IMG_ID_TEXT_ENTRY:
        if ((fbSkipLoad == TRUE) && (sTextEntryData.fbLoaded == TRUE))
            return (lErrCode);

        psGraphicData = &sTextEntryData;
        break;

    default:
        if (fbIsReport == TRUE)
        {
            if ((fbSkipLoad == TRUE) && (sReportTestData.fbLoaded == TRUE))
                return (lErrCode);

            psGraphicData = &sReportTestData;
        }
        else
        {
            lLastErrCode = IG_INVALID_COMPONENT;
            fbImageGenFailed = TRUE;
            return (IG_INVALID_COMPONENT);
        }
        break;
    }

    // Check that the schema is correct
    if (pbHeaderAddr[GRAPHIC_SCHEMA_OFFSET] != GRAPHIC_SCHEMA)
    {
        lErrCode = IG_SCHEMA_ERROR;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }
    else
    {
        // Get the necessary data
        (void)EndianAwareCopy((unsigned char *)&wOffset, pbDataAddr+GRAPHIC_VCRS_OFFSET, 2);
        psGraphicData->psVCRs = pbDataAddr + wOffset;
        (void)EndianAwareCopy((unsigned char *)&wOffset, pbDataAddr+GRAPHIC_IMAGE_OFFSET, 2);
        psGraphicData->pbImage = pbDataAddr + wOffset;
        psGraphicData->psFields = pbDataAddr + GRAPHIC_FIELDS;
        pbDataAddr += GRAPHIC_WIDTH;
        (void)EndianAwareCopy((unsigned char *)&psGraphicData->wWidth, pbDataAddr, 2);
        pbDataAddr += 2;
        (void)EndianAwareCopy((unsigned char *)&psGraphicData->wHeight, pbDataAddr, 2);
        pbDataAddr += 2;
        psGraphicData->bNumOfGroups = *pbDataAddr++;
        pbDataAddr ++;                          // skip bPad1
        (void)EndianAwareCopy((unsigned char *)&psGraphicData->wNumOfVCRs, pbDataAddr, 2);
        pbDataAddr += 2;
        (void)EndianAwareCopy((unsigned char *)&psGraphicData->wSize, pbDataAddr, 2);

        psGraphicData->bComponentType = bType;
        psGraphicData->fbLoaded = TRUE;

        if (fbIncrementImage == TRUE)
            bNumOfImages++;
    }
/*
#ifdef IG_TIME_DEBUG
    (void)sprintf(pLogBuf, "End IG GetImage");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
*/

    return (lErrCode);
}

/*****************************************************************************
// FUNCTION NAME: GetTC
// DESCRIPTION: Sets up the sTCGraphicData structure
//
// AUTHOR: Sandra Peterson
//
// INPUTS: pbHeaderAddr = starting address of the header section for the town circle
//         pbDataAddr = starting address of the data section (after the hash
//                      signature) for the town circle
//
// OUTPUTS: Error code
*****************************************************************************/

unsigned long GetTC(const unsigned char bType, const BOOL fbSkipLoad,
                    const unsigned char *pbHeaderAddr, unsigned char *pbDataAddr)
{
    TOWN_CIRCLE_DATA    *psTCData;
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wOffset;
    unsigned short  wInx1, wInx2;
    unsigned char   bByte1, bByte2;


    // Check that the schema is correct
    if (pbHeaderAddr[GRAPHIC_SCHEMA_OFFSET] != GRAPHIC_SCHEMA)
    {
        lErrCode = IG_SCHEMA_ERROR;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }
    else
    {
        if (bNumOfTCs < MAX_TCS)
        {
            if (fbSkipLoad == TRUE)
            {
                for (wInx1 = 0; wInx1 <= bNumOfTCs; wInx1++)
                {
                    if ((sTCData[wInx1].fbLoaded == TRUE) &&
                        (sTCData[wInx1].bComponentType == bType))
                    {
                        bNumOfTCs++;
                        return (lErrCode);
                    }
                }
            }

            psTCData = &sTCData[bNumOfTCs];
        }
        else
        {
            lLastErrCode = IG_TOO_MANY_IMAGES;
            fbImageGenFailed = TRUE;
            return (IG_TOO_MANY_IMAGES);
        }

        // Get the necessary data
        (void)EndianAwareCopy((unsigned char *)&wOffset, pbDataAddr+GRAPHIC_VCRS_OFFSET, 2);
        psTCData->psVCRs = pbDataAddr + wOffset;
        psTCData->psFields = pbDataAddr + GRAPHIC_FIELDS;
        (void)EndianAwareCopy((unsigned char *)&wOffset, pbDataAddr+GRAPHIC_IMAGE_OFFSET, 2);
        psTCData->pbImage = pbDataAddr + wOffset;
        pbDataAddr += GRAPHIC_WIDTH;
        (void)EndianAwareCopy((unsigned char *)&psTCData->wWidth, pbDataAddr, 2);
        pbDataAddr += 2;
        (void)EndianAwareCopy((unsigned char *)&psTCData->wHeight, pbDataAddr, 2);
        pbDataAddr += 2;
        psTCData->bNumOfGroups = *pbDataAddr++;
        pbDataAddr ++;                          // skip bPad1
        (void)EndianAwareCopy((unsigned char *)&psTCData->wNumOfVCRs, pbDataAddr, 2);
        pbDataAddr += 2;
        (void)EndianAwareCopy((unsigned char *)&psTCData->wSize, pbDataAddr, 2);

        // if there is a name descriptor, load it.
        // can't do a word copy because we can't count on the address
        // being on an even word boundary.
        wInx1 = GRAPHIC_NAME_DESC + 2;      // get past the 2 ID bytes
        wInx2 = 0;
        if (*(pbHeaderAddr+GRAPHIC_NUM_NAMES))
        {
            for ( ; wInx2 < MAX_TC_UNICHARS; wInx2++)
            {
                bByte1 = pbHeaderAddr[wInx1];
                bByte2 = pbHeaderAddr[wInx1+1];
                if ((bByte1 == 0) && (bByte2 == 0))
                    break;
                else
                {
                    psTCData->wNameDescriptor[wInx2] = (bByte1 * 16) + bByte2;
                    wInx1 += 2;
                }
            }
        }

        psTCData->wNameDescriptor[wInx2] = 0;

        psTCData->bComponentType = bType;
        psTCData->fbLoaded = TRUE;
        bNumOfTCs++;
    }

    return (lErrCode);
}

/****************************************************************************
//  FUNCTIONS FOR CLEARING/LOADING THE REGIONS, REGISTER GROUPS, VCR DATA   
*****************************************************************************/
/****************************************************************************
// FUNCTION NAME: ClearBuffers
// DESCRIPTION: Clears a bunch of common buffers and associated flags
//
// AUTHOR: Sandra Peterson
//
// INPUTS: 
//
// OUTPUTS: None
*****************************************************************************/

void ClearBuffers(void)
{
    fbDirtyStuff[REGIONS_DIRTY] = TRUE;
    fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
    fbHasBarcode = FALSE;
    (void)memset(bImageBuffer, 0, sizeof(bImageBuffer));
    (void)memset(sPrintedRegionMap, 0, sizeof(sPrintedRegionMap));
    (void)memset(sRegisterGroups, 0, sizeof(sRegisterGroups));
    (void)memset(sPrintedVCRs, 0, sizeof(sPrintedVCRs));
}


/****************************************************************************
// FUNCTION NAME: ClearRestOfIgStuff
// DESCRIPTION: Clears the rest of the image buffer, the printed region map, the
//              register groups, and the VCR definitions
//
// AUTHOR: Sandra Peterson
//
// INPUTS: 
//
// OUTPUTS: None
*****************************************************************************/

unsigned long ClearRestOfIgStuff(void)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wRemains;
	char			pLogBuf[50];


#if !defined(IG_TEST_CODE128_1D) && !defined(IG_TEST_CODE39_1D) && !defined(IG_TEST_CODE25_INTERLEAVED_1D)
    wRemains = IMAGE_COLS + START_OFFSET - wNewStartCol;
    lErrCode = IGU_ClearRegion(wNewStartCol, 0, wRemains, MAX_ROWS);

	if (lErrCode == IG_INVALID_SIZE)
	{
		(void)sprintf(pLogBuf, "Bad remaining area calculation");
		fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
	}
#endif
    
    wRemains = (MAX_NUM_REGIONS_PER_AREA - bPrintedRegions) * sizeof(sPrintedRegionMap[0]);
    (void)memset(&sPrintedRegionMap[bPrintedRegions], 0, wRemains);
    
    wRemains = (IG_MAX_GROUPS - bTotalGroups) * sizeof(sRegisterGroups[0]);
    (void)memset(&sRegisterGroups[bTotalGroups], 0, wRemains);
    
    wRemains = (unsigned short)((IG_MAX_VCRS - wTotalVCRs) * sizeof(sPrintedVCRs[0]));
    (void)memset(&sPrintedVCRs[wTotalVCRs], 0, wRemains);

    return(lErrCode);
}


/****************************************************************************
// FUNCTION NAME: ClearVariables
// DESCRIPTION: Clears a bunch of common variables
//
// AUTHOR: Sandra Peterson
//
// INPUTS: 
//
// OUTPUTS: None
*****************************************************************************/

void ClearVariables(void)
{
    wNewStartCol = 0;
    wNewStartRow = 0;
    wNextVCR = 0;
    wReqCols = 0;
    wTotalVCRs = 0;
    bPrintedRegions = 0;
    bTotalGroups = 0;
    fbImageBufferState = LOADED;
}


/****************************************************************************
// FUNCTION NAME: ClearVCRs
// DESCRIPTION: Sets the given VCRs to spaces and indicates that they need
//              to be reloaded
//
// AUTHOR: Sandra Peterson
//
// INPUTS: 
//
// OUTPUTS: None
*****************************************************************************/

void ClearVCRs(const unsigned short wStartVCR, const unsigned short wEndVCR)
{
    unsigned short  wInx;

    for (wInx = wStartVCR; wInx < wEndVCR; wInx++)
    {
        sPrintedVCRs[wInx].wValue = 0x0020;
        sPrintedVCRs[wInx].fbDirty = TRUE;
    }

    if ((wStartVCR == 0) && (wEndVCR == wTotalVCRs))
    {
        for (wInx = 0; wInx < bTotalGroups; wInx++)
            sRegisterGroups[wInx].fbDirty = TRUE;
    }
}

/****************************************************************************
// FUNCTION NAME: LoadRegionData
// DESCRIPTION: Loads a region map record from the given region map into the
//              printed region map
//
// AUTHOR: Sandra Peterson
//
// GLOBALS USED: sPrintedRegionMap, bPrintedRegions
//
// INPUTS: psFlashRegionMap = address of the record in the source region map
//
// OUTPUTS: None
*****************************************************************************/

void LoadRegionData(const REGION_MAP *psFlashRegionMap)
{
    // Fight the endians (void)memcpy((unsigned char *)&sPrintedRegionMap[bPrintedRegions],
    //        (unsigned char *)psFlashRegionMap, REGION_RECORD_SIZE);
	sPrintedRegionMap[bPrintedRegions].bAreaID = psFlashRegionMap->bAreaID;
	sPrintedRegionMap[bPrintedRegions].bComponentType = psFlashRegionMap->bComponentType;
    EndianAwareCopy(&sPrintedRegionMap[bPrintedRegions].wStartCol, &psFlashRegionMap->wStartCol, sizeof(psFlashRegionMap->wStartCol));
    EndianAwareCopy(&sPrintedRegionMap[bPrintedRegions].wStartRow, &psFlashRegionMap->wStartRow, sizeof(psFlashRegionMap->wStartRow));
    EndianAwareCopy(&sPrintedRegionMap[bPrintedRegions].wMaxCols, &psFlashRegionMap->wMaxCols, sizeof(psFlashRegionMap->wMaxCols));
    EndianAwareCopy(&sPrintedRegionMap[bPrintedRegions].wMaxRows, &psFlashRegionMap->wMaxRows, sizeof(psFlashRegionMap->wMaxRows));
	
    if (wNewStartCol != 0)
    {
        if (sPrintedRegionMap[bPrintedRegions].wStartCol != 0)
        {
            // if loading the data for the ad slogan region and nothing is allowed to
            // be repositioned into the ad slogan region,
            // change the max columns to include any space between the starting
            // column number from the flash region map and the current next column for
            // loading data
            if ((sPrintedRegionMap[bPrintedRegions].bComponentType == IMG_ID_AD_SLOGAN) &&
                !fnIsReposAllowed())
            {
                sPrintedRegionMap[bPrintedRegions].wMaxCols += 
                    sPrintedRegionMap[bPrintedRegions].wStartCol - wNewStartCol;
            }

            // set the starting column number to the next column for loading data
            sPrintedRegionMap[bPrintedRegions].wStartCol = wNewStartCol;
        }
    }
    else
        sPrintedRegionMap[bPrintedRegions].wStartCol = wNewStartCol + START_OFFSET;

    if (wNewStartRow != 0)
        if (sPrintedRegionMap[bPrintedRegions].wStartRow != 0)
            sPrintedRegionMap[bPrintedRegions].wStartRow = wNewStartRow;
}

/****************************************************************************
// FUNCTION NAME: LoadRegisterData
// DESCRIPTION: Loads the register groups and the VCR data for a given print
//              region into sRegisterGroups & sPrintedVCRs
//
// AUTHOR: Sandra Peterson
//
// GLOBALS USED: sRegisterGroups, bTotalGroups, wNextVCR,
//              sPrintedVCRs, wTotalVCRs
//
// INPUTS: pbData1 = pointer to the register groups for the print region
//         bNumOfGroups = number of register groups for the print region
//         psData2 = pointer to the VCR data for the print region
//         bNumOfGroups = number of VCRs for the print region
//         wStartCol = starting column for the print region
//         wStartRow = starting row for the print region
//
// OUTPUTS: Error code
//          bTotalGroups & wTotalVCRs updated
*****************************************************************************/

unsigned long LoadRegisterData(const unsigned char *pbData1, const unsigned char bNumOfGroups,
                               const unsigned char *pbData2, const unsigned short wNumOfVCRs,
                               const unsigned short wStartCol, const unsigned short wStartRow)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wNum = 0;
    unsigned short  wInx2 = 0;
    unsigned char   bInx1;
    
    
    for (bInx1 = 0; bInx1 < bNumOfGroups; bInx1++, bTotalGroups++)
    {
        // copy the register group data to sRegisterGroups
        (void)memcpy((unsigned char *)&sRegisterGroups[bTotalGroups], pbData1,
                REGISTER_GROUP_RECORD_SIZE);
		//Fix the endianism
		sRegisterGroups[bTotalGroups].wStartRegID = EndianAwareGet16(&sRegisterGroups[bTotalGroups].wStartRegID);
    	sRegisterGroups[bTotalGroups].wVariableID = EndianAwareGet16(&sRegisterGroups[bTotalGroups].wVariableID);
		sRegisterGroups[bTotalGroups].wFunctionID = EndianAwareGet16(&sRegisterGroups[bTotalGroups].wFunctionID);
 
        // adjust the starting register ID if necessary
        if (wNextVCR != 0)
            sRegisterGroups[bTotalGroups].wStartRegID += wNextVCR;

        // add the number of registers to the running count
        wNum += sRegisterGroups[bTotalGroups].bLengthOfField;

        // advance to the next register group record
        pbData1 += REGISTER_GROUP_RECORD_SIZE;

        for ( ; (wInx2 < wNum) && (wTotalVCRs < IG_MAX_VCRS);
             wInx2++, wTotalVCRs++)
        {
            // copy the VCR data to sPrintedVCRs
            (void)memcpy((unsigned char *)&sPrintedVCRs[wTotalVCRs], pbData2,
                    VCR_RECORD_SIZE);
			//Fix the endianism
			sPrintedVCRs[wTotalVCRs].wCol = EndianAwareGet16(&sPrintedVCRs[wTotalVCRs].wCol);
			sPrintedVCRs[wTotalVCRs].wRow = EndianAwareGet16(&sPrintedVCRs[wTotalVCRs].wRow);
 
            // adjust the starting col & row based on the starting
            // col & row for the region
            sPrintedVCRs[wTotalVCRs].wCol += wStartCol;
            sPrintedVCRs[wTotalVCRs].wRow += wStartRow;

            // load the font for the VCRs based on the font for the
            // register group
            sPrintedVCRs[wTotalVCRs].bFontID = sRegisterGroups[bTotalGroups].bFontID;

            // advance to the next VCR record
            pbData2 += VCR_RECORD_SIZE;
        }

        sRegisterGroups[bTotalGroups].fbDirty = TRUE;
    }

    // if the running count doesn't equal the number of VCRs for this region OR
    // we didn't load all the VCRs but we've loaded the allowed max number of VCRs,
    //  it's an error
    if ((wNum != wNumOfVCRs) ||
        ((wInx2 != wNumOfVCRs) && (wTotalVCRs == IG_MAX_VCRS)))
    {
        lErrCode = IG_INVALID_FIELD_SIZE;
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: LoadRest
// DESCRIPTION: Loads the rest of the print area aside from the indicia,
//              permit, or tax graphic region.  This function is used for the
//              regions that can be in multiple region maps.
//
// AUTHOR: Sandra Peterson
//
// GLOBALS USED: sPrintedRegionMap, bPrintedRegions,
//              sRegisterGroups, bTotalGroups,
//              sPrintedVCRs, wTotalVCRs,
//              wNewStartCol, wNewStartRow, wNextVCR
//
// INPUTS: psFlashRegionMap = address of the record in the source region map
//
// OUTPUTS: error code
//          sPrintedRegionMap & bPrintedRegions updated
//          sRegisterGroups & bTotalGroups updated
//          sPrintedVCRs & wTotalVCRs updated
//          wNewStartCol & wNewStartRow updated
//          wNextVCR updated
*****************************************************************************/

unsigned long LoadRest(const REGION_MAP *psFlashRegionMap)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wRemains;
    unsigned char   bStatus;
    unsigned char   bIndex;
	unsigned char	ucBarcodeType;
	char			pLogBuf[50];
    BOOL    fbEnabled;


    switch (psFlashRegionMap->bComponentType)
    {
    case IMG_ID_AD_SLOGAN:
        if (bAdID == 0)
        {
            if (fnIsReposAllowed())
            {
                if (sPrintedRegionMap[bPrintedRegions].bComponentType == IMG_ID_AD_SLOGAN)
                {
                    fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                    fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
                }

                if (fbAdHasVCRs == TRUE)
                    fbAdHasVCRs = FALSE;
            }
            else
            {
                if (sPrintedRegionMap[bPrintedRegions].bComponentType != IMG_ID_AD_SLOGAN)
                {
                    fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                    fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
                }

                if (fbAdHasVCRs == TRUE)
                {
                    fbAdHasVCRs = FALSE;
                    fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
                }

                // Reload the ad region data if necessary.
                if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
                    LoadRegionData(psFlashRegionMap);

                // clear out the ad region
                // since an ad may not have been loaded yet, clear as wide as the maximum
                lErrCode = IGU_ClearRegion(sPrintedRegionMap[bPrintedRegions].wStartCol,
                                            sPrintedRegionMap[bPrintedRegions].wStartRow,
                                            sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                            sPrintedRegionMap[bPrintedRegions].wMaxRows);

				if (lErrCode == IG_INVALID_SIZE)
				{
					(void)sprintf(pLogBuf, "Bad Ad region");
					fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
				}

                if (lErrCode != IG_NO_ERROR)
                    break;

                wRemains = sPrintedRegionMap[bPrintedRegions].wStartCol +
                            sPrintedRegionMap[bPrintedRegions].wMaxCols;

                // make sure not to go backwards on the number of columns
                if (wRemains > wNewStartCol)
                    wNewStartCol = wRemains;
            
				if (fnIncludeGraphicInMinPrint(INCLUDE_AD_ALWAYS) == TRUE)
				{
					if (wRemains > wReqCols)
						wReqCols = wRemains;
				}

                wNewStartRow = sPrintedRegionMap[bPrintedRegions].wStartRow +
                                sPrintedRegionMap[bPrintedRegions].wMaxRows;
                bPrintedRegions++;
            }
        }
        else
        {
            // if the component loaded at the current spot in the region map isn't the
            // same as the component we want to load, the rest of the regions have to
            // be redone
            if (sPrintedRegionMap[bPrintedRegions].bComponentType != IMG_ID_AD_SLOGAN)
            {
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }

            // Reload the ad region data if necessary.
            if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
                LoadRegionData(psFlashRegionMap);               

            if (fbDirtyStuff[AD_DIRTY] == TRUE)
            {
                // if nothing is allowed to be in the ad area except for the ad,
                // make sure the whole ad area is clear.
                if (!fnIsReposAllowed())
                {
                    // clear out the ad region
                    // since an ad may not have been loaded yet, clear as wide as the maximum
                    lErrCode = IGU_ClearRegion(sPrintedRegionMap[bPrintedRegions].wStartCol,
                                                sPrintedRegionMap[bPrintedRegions].wStartRow,
                                                sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                                sPrintedRegionMap[bPrintedRegions].wMaxRows);

					if (lErrCode == IG_INVALID_SIZE)
					{
						(void)sprintf(pLogBuf, "Bad Ad region");
						fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
					}

                    if (lErrCode != IG_NO_ERROR)
                        break;
                }

                if (sAdData.wNumOfVCRs != 0)
                {
                    fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
                    fbAdHasVCRs = TRUE;
                }
                else
                {
                    if (fbAdHasVCRs == TRUE)
                        fbDirtyStuff[REGISTERS_DIRTY] = TRUE;

                    fbAdHasVCRs = FALSE;
                }

                // Load the ad background graphic
#if defined(IG_TEST_CODE128_1D) || defined(IG_TEST_CODE39_1D) || defined(IG_TEST_CODE25_INTERLEAVED_1D)
                lErrCode = IGU_ClearRegion(sPrintedRegionMap[bPrintedRegions].wStartCol,
                                            sPrintedRegionMap[bPrintedRegions].wStartRow,
                                            sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                            sPrintedRegionMap[bPrintedRegions].wMaxRows);

                if (lErrCode == IG_INVALID_SIZE)
                {
                    (void)sprintf(pLogBuf, "Bad 1D barcode area");
                    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
                }

#else
                lErrCode = IGU_LoadBackground(sAdData.pbImage, sAdData.wSize,
                                            sPrintedRegionMap[bPrintedRegions].wStartCol,
                                            sPrintedRegionMap[bPrintedRegions].wStartRow, 
                                            sAdData.wWidth, sAdData.wHeight,
                                            sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                            sPrintedRegionMap[bPrintedRegions].wMaxRows);

                if (lErrCode == IG_INVALID_SIZE)
                {
                    (void)sprintf(pLogBuf, "Bad Ad graphic");
                    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
                }
#endif

                if (lErrCode != IG_NO_ERROR)
                    break;

                if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
                {
                    lErrCode = LoadRegisterData(sAdData.psFields, sAdData.bNumOfGroups,
                                                sAdData.psVCRs, sAdData.wNumOfVCRs,
                                                sPrintedRegionMap[bPrintedRegions].wStartCol,
                                                sPrintedRegionMap[bPrintedRegions].wStartRow);
                }
                else
                {
                    bTotalGroups += sAdData.bNumOfGroups;
                    wTotalVCRs += sAdData.wNumOfVCRs;
                }

                wNextVCR = wTotalVCRs;
 
                // since we changed the ad, the rest of the regions have to be
                // reloaded because the actual size of the new ad may be different
                // from what it was before
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }
            else
            {
                if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
                {
                    // if nothing is allowed to be in the ad area except for the ad,
                    // make sure the whole ad area is clear.
                    if (!fnIsReposAllowed())
                    {
                        // clear out the ad region
                        // since an ad may not have been loaded yet, clear as wide as the maximum
                        lErrCode = IGU_ClearRegion(sPrintedRegionMap[bPrintedRegions].wStartCol,
                                                    sPrintedRegionMap[bPrintedRegions].wStartRow,
                                                    sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                                    sPrintedRegionMap[bPrintedRegions].wMaxRows);

						if (lErrCode == IG_INVALID_SIZE)
						{
							(void)sprintf(pLogBuf, "Bad Ad region");
							fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
						}

                        if (lErrCode != IG_NO_ERROR)
                            break;
                    }

                    // Load the ad background graphic
                    lErrCode = IGU_LoadBackground(sAdData.pbImage, sAdData.wSize,
                                                sPrintedRegionMap[bPrintedRegions].wStartCol,
                                                sPrintedRegionMap[bPrintedRegions].wStartRow, 
                                                sAdData.wWidth, sAdData.wHeight,
                                                sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                                sPrintedRegionMap[bPrintedRegions].wMaxRows);

                    if (lErrCode == IG_INVALID_SIZE)
                    {
                        (void)sprintf(pLogBuf, "Bad Ad graphic");
                        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
                    }

                    if (lErrCode != IG_NO_ERROR)
                        break;
                }

                if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
                {
                    lErrCode = LoadRegisterData(sAdData.psFields, sAdData.bNumOfGroups,
                                                sAdData.psVCRs, sAdData.wNumOfVCRs,
                                                sPrintedRegionMap[bPrintedRegions].wStartCol,
                                                sPrintedRegionMap[bPrintedRegions].wStartRow);
                }
                else
                {
                    bTotalGroups += sAdData.bNumOfGroups;
                    wTotalVCRs += sAdData.wNumOfVCRs;
                }

                wNextVCR = wTotalVCRs;
            }

            // if loading a text ad or nothing is allowed to be repositioned into the
            // ad region, use the max size of the ad area.
            // else, use the actual size of the graphic ad.
            if (!fnIsReposAllowed())
            {
                wRemains = sPrintedRegionMap[bPrintedRegions].wStartCol +
                            sPrintedRegionMap[bPrintedRegions].wMaxCols;
            
                wNewStartRow = sPrintedRegionMap[bPrintedRegions].wStartRow +
                                sPrintedRegionMap[bPrintedRegions].wMaxRows;
            }
            else
            {
                wRemains = sPrintedRegionMap[bPrintedRegions].wStartCol +
                            sAdData.wWidth;
            
                wNewStartRow = sPrintedRegionMap[bPrintedRegions].wStartRow +
                                sAdData.wHeight;
            }
                        
            // make sure not to go backwards on the number of columns
            if (wRemains > wNewStartCol)
                wNewStartCol = wRemains;

			if (fnIncludeGraphicInMinPrint(INCLUDE_AD_ALWAYS) == TRUE)
			{
				if (wRemains > wReqCols)
					wReqCols = wRemains;
			}

            bPrintedRegions++;
        }

        fbDirtyStuff[AD_DIRTY] = FALSE;
        break;

    case IMG_ID_BAR_GRAPHIC:
		ucBarcodeType = sBarCodeData.sFlashBarcodeData.bBarcodeType & GET_BARCODE_TYPE;

		// if the 2D barcode is supposed to be ducked and the 2D barcode isn't in a VCR,
		//     if the 2D barcode is loaded at the current spot in the region map,
		//         the rest of the regions have to be redone.
		if ((fb2dBarcodeState == DUCKED) &&
			(ucBarcodeType != DM_VCR_ASCII_BINARY_ENCODING) &&
			(ucBarcodeType != DM_VCR_C40_ONLY_ENCODING))
		{
			fbHasBarcode = FALSE;
			fbBarcodeCleared = FALSE;
			if (sPrintedRegionMap[bPrintedRegions].bComponentType == IMG_ID_BAR_GRAPHIC)
			{
				fbDirtyStuff[REGIONS_DIRTY] = TRUE;
				fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
			}
		}
		else
		{
	        fbHasBarcode = TRUE;
	        bBarcodeRegion = bPrintedRegions;

	        // if the component loaded at the current spot in the region map isn't the
	        // same as the component we want to load, the rest of the regions have to
	        // be redone
	        if (sPrintedRegionMap[bPrintedRegions].bComponentType != IMG_ID_BAR_GRAPHIC)
	        {
	            fbDirtyStuff[REGIONS_DIRTY] = TRUE;
	            fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
	        }
                        
			// Reload the 2D barcode region data if necessary
	        if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
	        {
	            LoadRegionData(psFlashRegionMap);               
			}

			// if we think the barcode area might not be clear, clear it
			if ((fbDirtyStuff[REGIONS_DIRTY] == TRUE) ||
				(fbDirtyStuff[BARCODE_DIRTY] == TRUE))
			{
				fbBarcodeCleared = FALSE;

				lErrCode = IG_ClearBarcode();
				if (lErrCode != IG_NO_ERROR)
					break;		// break out of the IMG_ID_BAR_GRAPHIC case
	        }
            
			// if the barcode is in a VCR, we want to add in the width of the region.
			// else, only add in the actual width of the barcode because if a meter has more than 1
			// 2D barcode, the region is the size of the biggest barcode. We only want to add the space we
			// need for the barcode we're going to generate.
			switch(ucBarcodeType)
			{
				case DM_VCR_ASCII_BINARY_ENCODING:
			    case DM_VCR_C40_ONLY_ENCODING:
					wRemains = sPrintedRegionMap[bPrintedRegions].wStartCol +
								sPrintedRegionMap[bPrintedRegions].wMaxCols;
					break;

				default:
		            wRemains = sPrintedRegionMap[bPrintedRegions].wStartCol +
		            			sBarCodeData.wWidth;
					break;
			}

	        // make sure not to go backwards on the number of columns
	        if (wRemains > wNewStartCol)
	            wNewStartCol = wRemains;
                
	        if (wRemains > wReqCols)
	            wReqCols = wRemains;

			// if the barcode is in a VCR, we want to add in the height of the region.
			// else, only add in the actual height of the barcode because if a meter has more than 1
			// 2D barcode, the region is the size of the biggest barcode. We only want to add the space we
			// need for the barcode we're going to generate.
			switch(ucBarcodeType)
			{
				case DM_VCR_ASCII_BINARY_ENCODING:
			    case DM_VCR_C40_ONLY_ENCODING:
					wNewStartRow = sPrintedRegionMap[bPrintedRegions].wStartRow +
									sPrintedRegionMap[bPrintedRegions].wMaxRows;
					break;

				default:
		            wNewStartRow = sPrintedRegionMap[bPrintedRegions].wStartRow +
		            				sBarCodeData.wHeight;
					break;
			}

	        bPrintedRegions++;
		}

		fbDirtyStuff[BARCODE_DIRTY] = FALSE;
		break;

/*
    case IMG_ID_ENTGELT:
        // if the EB graphic is supposed to be ducked, but it's loaded at the current
        // spot in the region map, the rest of the regions have to be redone
        if (fbEBState == DUCKED)
        {
            if (sPrintedRegionMap[bPrintedRegions].bComponentType == IMG_ID_ENTGELT)
            {
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }
        }
        else
        {
            // if the component loaded at the current spot in the region map isn't the
            // same as the component we want to load, the rest of the regions have to
            // be redone
            if (sPrintedRegionMap[bPrintedRegions].bComponentType != IMG_ID_ENTGELT)
            {
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }
                        
            // Reload the entgelt bezahlt region data if necessary.
            if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
                LoadRegionData(psFlashRegionMap);               
    
            if (fbDirtyStuff[EB_DIRTY] == TRUE)
            {
                // Load the entgelt bezahlt background graphic
                lErrCode = IGU_LoadBackground(sEBData.pbImage, sEBData.wSize,
                                            sPrintedRegionMap[bPrintedRegions].wStartCol,
                                            sPrintedRegionMap[bPrintedRegions].wStartRow, 
                                            sEBData.wWidth, sEBData.wHeight,
                                            sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                            sPrintedRegionMap[bPrintedRegions].wMaxRows);
            }
            else
            {
                if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
                {
                    // Load the entgelt bezahlt background graphic
                    lErrCode = IGU_LoadBackground(sEBData.pbImage, sEBData.wSize,
                                                sPrintedRegionMap[bPrintedRegions].wStartCol,
                                                sPrintedRegionMap[bPrintedRegions].wStartRow, 
                                                sEBData.wWidth, sEBData.wHeight,
                                                sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                                sPrintedRegionMap[bPrintedRegions].wMaxRows);
                }
            }
                
            if (lErrCode != IG_NO_ERROR)
                break;

            wRemains = sPrintedRegionMap[bPrintedRegions].wStartCol +
                        sEBData.wWidth;

            // make sure not to go backwards on the number of columns
            if (wRemains > wNewStartCol)
                wNewStartCol = wRemains;

			if (fnIncludeGraphicInMinPrint(INCLUDE_EB_ALWAYS) == TRUE)
			{
				if (wRemains > wReqCols)
					wReqCols = wRemains;
			}

            wNewStartRow = sPrintedRegionMap[bPrintedRegions].wStartRow +
                            sEBData.wHeight;
            bPrintedRegions++;
        }
            
        fbDirtyStuff[EB_DIRTY] = FALSE;
        break;
*/

    case IMG_ID_INSCRIPTION:
        if (bInscrID == 0)
        {
            if (sPrintedRegionMap[bPrintedRegions].bComponentType == IMG_ID_INSCRIPTION)
            {
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }
        }
        else
        {
            // if the component loaded at the current spot in the region map isn't the
            // same as the component we want to load, the rest of the regions have to
            // be redone
            if (sPrintedRegionMap[bPrintedRegions].bComponentType != IMG_ID_INSCRIPTION)
            {
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }
                        
            // Reload the inscription region data if necessary.
            if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
                LoadRegionData(psFlashRegionMap);               

            if (fbDirtyStuff[INSCR_DIRTY] == TRUE)
            {
                // Load the inscription background graphic
                lErrCode = IGU_LoadBackground(sInscrData.pbImage, sInscrData.wSize,
                                            sPrintedRegionMap[bPrintedRegions].wStartCol,
                                            sPrintedRegionMap[bPrintedRegions].wStartRow, 
                                            sInscrData.wWidth, sInscrData.wHeight,
                                            sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                            sPrintedRegionMap[bPrintedRegions].wMaxRows);
 
                // since we changed the inscription, the rest of the regions have to be
                // reloaded because the actual size of the new inscription may be different
                // from what it was before
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }
            else
            {
                if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
                {
                    // Load the inscription background graphic
                    lErrCode = IGU_LoadBackground(sInscrData.pbImage, sInscrData.wSize,
                                                sPrintedRegionMap[bPrintedRegions].wStartCol,
                                                sPrintedRegionMap[bPrintedRegions].wStartRow, 
                                                sInscrData.wWidth, sInscrData.wHeight,
                                                sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                                sPrintedRegionMap[bPrintedRegions].wMaxRows);
                }
            }

			if (lErrCode == IG_INVALID_SIZE)
			{
				(void)sprintf(pLogBuf, "Bad Inscription graphic");
				fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
			}

            if (lErrCode != IG_NO_ERROR)
                break;
                
            wRemains = sPrintedRegionMap[bPrintedRegions].wStartCol +
                        sInscrData.wWidth;

            // make sure not to go backwards on the number of columns
            if (wRemains > wNewStartCol)
                wNewStartCol = wRemains;

			if ((fnIncludeGraphicInMinPrint(INCLUDE_MODE_AJOUT_INSCR) == TRUE) ||
				(fnIncludeGraphicInMinPrint(INCLUDE_INSCR_ALWAYS) == TRUE))
            {
                if (wRemains > wReqCols)
                    wReqCols = wRemains;
            }

            wNewStartRow = sPrintedRegionMap[bPrintedRegions].wStartRow +
                            sInscrData.wHeight;
            bPrintedRegions++;
        }
            
        fbDirtyStuff[INSCR_DIRTY] = FALSE;
        break;
    
    case IMG_ID_MISC_GRAPHIC:
        // if the misc graphic is supposed to be ducked, but it's loaded at the current
        // spot in the region map, the rest of the regions have to be redone
        if (fbMiscGraphicState == DUCKED)
        {
            if (sPrintedRegionMap[bPrintedRegions].bComponentType == IMG_ID_MISC_GRAPHIC)
            {
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }
        }
        else
        {
	        // if the component loaded at the current spot in the region map isn't the
	        // same as the component we want to load, the rest of the regions have to
	        // be redone
	        if (sPrintedRegionMap[bPrintedRegions].bComponentType != IMG_ID_MISC_GRAPHIC)
	        {
	            fbDirtyStuff[REGIONS_DIRTY] = TRUE;
	            fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
	        }
                        
	            // Reload the misc graphic region data if necessary.
	        if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
	        {
	            LoadRegionData(psFlashRegionMap);               

                // make sure we know to reload the VCRs
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;

	            // Load the misc. background graphic
	            lErrCode = IGU_LoadBackground(sMiscData.pbImage, sMiscData.wSize,
	                                        sPrintedRegionMap[bPrintedRegions].wStartCol,
	                                        sPrintedRegionMap[bPrintedRegions].wStartRow, 
	                                        sMiscData.wWidth, sMiscData.wHeight,
	                                        sPrintedRegionMap[bPrintedRegions].wMaxCols,
	                                        sPrintedRegionMap[bPrintedRegions].wMaxRows);

				if (lErrCode == IG_INVALID_SIZE)
				{
					(void)sprintf(pLogBuf, "Bad Misc graphic");
					fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
				}

	            if (lErrCode != IG_NO_ERROR)
	                break;
	        }
                        
            if (fbDirtyStuff[MISC_DIRTY] == TRUE)
            {
                // If changing the state, the VCRs will have to be reloaded
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }
                            
	        if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
	        {
	            lErrCode = LoadRegisterData(sMiscData.psFields, sMiscData.bNumOfGroups,
	                                        sMiscData.psVCRs, sMiscData.wNumOfVCRs,
	                                        sPrintedRegionMap[bPrintedRegions].wStartCol,
	                                        sPrintedRegionMap[bPrintedRegions].wStartRow);
	        }
	        else
	        {
	            bTotalGroups += sMiscData.bNumOfGroups;
	            wTotalVCRs += sMiscData.wNumOfVCRs;
	        }
            
	        wRemains = sPrintedRegionMap[bPrintedRegions].wStartCol +
	                    sMiscData.wWidth;
                        
	        // make sure not to go backwards on the number of columns
	        if (wRemains > wNewStartCol)
	            wNewStartCol = wRemains;
                
			if (fnIncludeGraphicInMinPrint(INCLUDE_MISC_GRAPHIC_ALWAYS) == TRUE)
			{
				if (wRemains > wReqCols)
					wReqCols = wRemains;
			}
	
	        wNewStartRow = sPrintedRegionMap[bPrintedRegions].wStartRow +
	                        sMiscData.wHeight;
	        bPrintedRegions++;
	        wNextVCR = wTotalVCRs;
        }

        fbDirtyStuff[MISC_DIRTY] = FALSE;
        break;

    case IMG_ID_TEXT_ENTRY:
        // If the text entry messages feature isn't enabled, don't load the graphic.
        // Else, check to make sure that we got the graphic.
        //    If we don't have the graphic, give an error.
        if (IsFeatureEnabled(&fbEnabled, MANUAL_TEXT_MSGS_SUPPORT, &bStatus, &bIndex))
        {
            if (fbEnabled)
            {
                if (sTextEntryData.fbLoaded == FALSE)
                {
					(void)sprintf(pLogBuf, "LoadRest: Missing Text Graphic");
					fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
                    lErrCode = IG_MISSING_COMPONENT;
                    fbDirtyStuff[TEXT_ENTRY_DIRTY] = FALSE;
                    break;
                }
                // else, attempt to load the graphic
            }
            else
            {
                fbDirtyStuff[TEXT_ENTRY_DIRTY] = FALSE;
                break;
            }
        }
        // else, since we can't tell if the feature is enabled, assume that it is and
        // attempt to load the graphic.

        if (fbTextEntryState == DUCKED)
        {
            if (sPrintedRegionMap[bPrintedRegions].bComponentType == IMG_ID_TEXT_ENTRY)
            {
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }
        }
        else
        {
            // if the component loaded at the current spot in the region map isn't the
            // same as the component we want to load, the rest of the regions have to
            // be redone
            if (sPrintedRegionMap[bPrintedRegions].bComponentType != IMG_ID_TEXT_ENTRY)
            {
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }
                        
            // Reload the text entry region data if necessary.
            if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
                LoadRegionData(psFlashRegionMap);               

            if (fbDirtyStuff[TEXT_ENTRY_DIRTY] == TRUE)
            {
                // Load the text entry background graphic
                lErrCode = IGU_LoadBackground(sTextEntryData.pbImage, sTextEntryData.wSize,
                                            sPrintedRegionMap[bPrintedRegions].wStartCol,
                                            sPrintedRegionMap[bPrintedRegions].wStartRow, 
                                            sTextEntryData.wWidth, sTextEntryData.wHeight,
                                            sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                            sPrintedRegionMap[bPrintedRegions].wMaxRows);
            }
            else
            {
                if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
                {
                    // Load the text entry background graphic
                    lErrCode = IGU_LoadBackground(sTextEntryData.pbImage, sTextEntryData.wSize,
                                                sPrintedRegionMap[bPrintedRegions].wStartCol,
                                                sPrintedRegionMap[bPrintedRegions].wStartRow, 
                                                sTextEntryData.wWidth, sTextEntryData.wHeight,
                                                sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                                sPrintedRegionMap[bPrintedRegions].wMaxRows);
                }
            }
                
			if (lErrCode == IG_INVALID_SIZE)
			{
				(void)sprintf(pLogBuf, "Bad Text Entry graphic");
				fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
			}

            if (lErrCode != IG_NO_ERROR)
                break;

            if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
            {
                lErrCode = LoadRegisterData(sTextEntryData.psFields, sTextEntryData.bNumOfGroups,
                                            sTextEntryData.psVCRs, sTextEntryData.wNumOfVCRs,
                                            sPrintedRegionMap[bPrintedRegions].wStartCol,
                                            sPrintedRegionMap[bPrintedRegions].wStartRow);
            }
            else
            {
                bTotalGroups += sTextEntryData.bNumOfGroups;
                wTotalVCRs += sTextEntryData.wNumOfVCRs;
            }

            wRemains = sPrintedRegionMap[bPrintedRegions].wStartCol +
                        sTextEntryData.wWidth;

            // make sure not to go backwards on the number of columns
            if (wRemains > wNewStartCol)
                wNewStartCol = wRemains;

			if (fnIncludeGraphicInMinPrint(INCLUDE_TEXT_MSG_ALWAYS) == TRUE)
			{
				if (wRemains > wReqCols)
					wReqCols = wRemains;
			}

            wNewStartRow = sPrintedRegionMap[bPrintedRegions].wStartRow +
                            sTextEntryData.wHeight;
            bPrintedRegions++;
            wNextVCR = wTotalVCRs;
        }
            
        fbDirtyStuff[TEXT_ENTRY_DIRTY] = FALSE;
        break;

    case IMG_ID_TOWN_CIRCLE:
    case IMG_ID_TOWN_CIRCLE_2:
        // determine which town circle structure to use
        for (bCurrentTC = 0; bCurrentTC < bNumOfTCs; bCurrentTC++)
        {
            if (sTCData[bCurrentTC].bComponentType == psFlashRegionMap->bComponentType)
                break;
        }

        if (bCurrentTC == bNumOfTCs)
        {
			(void)sprintf(pLogBuf, "LoadRest: Missing TC Graphic, ID = %u", psFlashRegionMap->bComponentType);
			fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
            lErrCode = IG_MISSING_COMPONENT;
            break;
        }

        // If a new variable town name has been downloaded withe the postal config message
        // the redraw of the town circle region must be forced to take this new information
        // into account in the printed indicia
        if (CmosDownloadCtrlBuf.tcNameDownloaded == TRUE)
        {
            fbDirtyStuff[TC_DIRTY] = TRUE;
            CmosDownloadCtrlBuf.tcNameDownloaded = FALSE;
        }
       
        // copy the town circle name if it's different from the last known name
        if (unistrcmp(wIGTownCircleName, sTCData[bCurrentTC].wNameDescriptor))
            (void)unistrcpy(wIGTownCircleName, sTCData[bCurrentTC].wNameDescriptor);

        if ((fbTCState == DUCKED) && (NoTownCircleVCRs() == TRUE))
        {
            if (sPrintedRegionMap[bPrintedRegions].bComponentType == psFlashRegionMap->bComponentType)
            {
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                if (sTCData[bCurrentTC].wNumOfVCRs)
                    fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }
        }
        else            // we're ducked and have VCRs, or we're unducked
        {
            // if the component loaded at the current spot in the region map isn't the
            // same as the component we want to load, the rest of the regions have to
            // be redone
            if (sPrintedRegionMap[bPrintedRegions].bComponentType != psFlashRegionMap->bComponentType)
            {
                fbDirtyStuff[REGIONS_DIRTY] = TRUE;
                if (sTCData[bCurrentTC].wNumOfVCRs)
                    fbDirtyStuff[REGISTERS_DIRTY] = TRUE;
            }
                                    
            // Reload the town circle region data if necessary.
            if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
                LoadRegionData(psFlashRegionMap);               

            if (fbDirtyStuff[TC_DIRTY] == TRUE)
            {
                // If changing the state of the town circle ducking, reload the VCRs
                if (sTCData[bCurrentTC].wNumOfVCRs)
                    fbDirtyStuff[REGISTERS_DIRTY] = TRUE;

                if (fbTCState == DUCKED)
                {
                    // clear out the town circle region
                    // only clear as wide as the actual graphic
                    lErrCode = IGU_ClearRegion(sPrintedRegionMap[bPrintedRegions].wStartCol,
                                                sPrintedRegionMap[bPrintedRegions].wStartRow,
                                                sTCData[bCurrentTC].wWidth,
                                                sPrintedRegionMap[bPrintedRegions].wMaxRows);

					if (lErrCode == IG_INVALID_SIZE)
					{
						(void)sprintf(pLogBuf, "Bad TC region");
						fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
					}
                }
                else
                {
                    // Load the town circle background graphic
                    lErrCode = IGU_LoadBackground(sTCData[bCurrentTC].pbImage, sTCData[bCurrentTC].wSize,
                                                sPrintedRegionMap[bPrintedRegions].wStartCol,
                                                sPrintedRegionMap[bPrintedRegions].wStartRow, 
                                                sTCData[bCurrentTC].wWidth, sTCData[bCurrentTC].wHeight,
                                                sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                                sPrintedRegionMap[bPrintedRegions].wMaxRows);
                                                
					if (lErrCode == IG_INVALID_SIZE)
					{
						(void)sprintf(pLogBuf, "Bad TC graphic, ID = %u", sTCData[bCurrentTC].bComponentType);
						fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
					}

                    /*****************************************************************************
                    //                     NEW TOWN CIRCLE MANAGEMENT 
                     *****************************************************************************/
                    // New Town Circle management if one of the 2 town names is defined
                    if ((exp_mtr_cmos.town_name_1[0] != UNICODE_NULL) ||
						(exp_mtr_cmos.town_name_2[0] != UNICODE_NULL))
                    {
                        ucTCFondId = fnFlashGetByteParm(BP_TC_FONT_ID);
                        IGU_DrawTownCircle(exp_mtr_cmos.town_name_1, ucTCFondId,
                                           exp_mtr_cmos.town_name_2, ucTCFondId,
                                           &sPrintedRegionMap[bPrintedRegions], &sTCData[bCurrentTC]);
                    }
                    /*****************************************************************************
                    //                   END OF NEW TOWN CIRCLE MANAGEMENT 
                     *****************************************************************************/
                }

                if (lErrCode != IG_NO_ERROR)
                    break;
            }
            else
            {
                if (fbDirtyStuff[REGIONS_DIRTY] == TRUE)
                {
                    if (fbTCState == DUCKED)
                    {
                        // clear out the town circle region
                        // only clear as wide as the actual graphic
                        lErrCode = IGU_ClearRegion(sPrintedRegionMap[bPrintedRegions].wStartCol,
                                                    sPrintedRegionMap[bPrintedRegions].wStartRow,
                                                    sTCData[bCurrentTC].wWidth,
                                                    sPrintedRegionMap[bPrintedRegions].wMaxRows);

						if (lErrCode == IG_INVALID_SIZE)
						{
							(void)sprintf(pLogBuf, "Bad TC region");
							fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
						}
                    }
                    else
                    {
                        // Load the town circle background graphic
                        lErrCode = IGU_LoadBackground(sTCData[bCurrentTC].pbImage, sTCData[bCurrentTC].wSize,
                                                    sPrintedRegionMap[bPrintedRegions].wStartCol,
                                                    sPrintedRegionMap[bPrintedRegions].wStartRow, 
                                                    sTCData[bCurrentTC].wWidth, sTCData[bCurrentTC].wHeight,
                                                    sPrintedRegionMap[bPrintedRegions].wMaxCols,
                                                    sPrintedRegionMap[bPrintedRegions].wMaxRows);

						if (lErrCode == IG_INVALID_SIZE)
						{
							(void)sprintf(pLogBuf, "Bad TC graphic, ID = %u", sTCData[bCurrentTC].bComponentType);
							fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
						}
                                                    
                        /*****************************************************************************
                        //                     NEW TOWN CIRCLE MANAGEMENT 
                         *****************************************************************************/
                        // New Town Circle management if one of the 2 town names is defined
                        if ((exp_mtr_cmos.town_name_1[0] != UNICODE_NULL) ||
							(exp_mtr_cmos.town_name_2[0] != UNICODE_NULL))
                        {
                            ucTCFondId = fnFlashGetByteParm(BP_TC_FONT_ID);
                            IGU_DrawTownCircle(exp_mtr_cmos.town_name_1, ucTCFondId,
                                               exp_mtr_cmos.town_name_2, ucTCFondId,
                                               &sPrintedRegionMap[bPrintedRegions], &sTCData[bCurrentTC]);
                        }
                        /*****************************************************************************
                        //                   END OF NEW TOWN CIRCLE MANAGEMENT 
                         *****************************************************************************/
                    }

                    if (lErrCode != IG_NO_ERROR)
                        break;
                }
            }

            if (fbDirtyStuff[REGISTERS_DIRTY] == TRUE)
            {
                lErrCode = LoadRegisterData(sTCData[bCurrentTC].psFields, sTCData[bCurrentTC].bNumOfGroups,
                                            sTCData[bCurrentTC].psVCRs, sTCData[bCurrentTC].wNumOfVCRs,
                                            sPrintedRegionMap[bPrintedRegions].wStartCol,
                                            sPrintedRegionMap[bPrintedRegions].wStartRow);
            }
            else
            {
                bTotalGroups += sTCData[bCurrentTC].bNumOfGroups;
                wTotalVCRs += sTCData[bCurrentTC].wNumOfVCRs;
            }
                
            wRemains = sPrintedRegionMap[bPrintedRegions].wStartCol +
                        sTCData[bCurrentTC].wWidth;

            // make sure not to go backwards on the number of columns
            if (wRemains > wNewStartCol)
                wNewStartCol = wRemains;
            
            if (wRemains > wReqCols)
                wReqCols = wRemains;

            wNewStartRow = sPrintedRegionMap[bPrintedRegions].wStartRow +
                            sTCData[bCurrentTC].wHeight;
            bPrintedRegions++;
            wNextVCR = wTotalVCRs;
        }
        
        fbDirtyStuff[TC_DIRTY] = FALSE;
        break;

    default:
        lErrCode = IG_INVALID_COMPONENT;            
        break;
    }

    if (lErrCode != IG_NO_ERROR)
    {
        lLastErrCode = lErrCode;
        fbImageGenFailed = TRUE;
    }

    return (lErrCode);
}

/****************************************************************************
// FUNCTION NAME: NoTownCircleVCRs
// DESCRIPTION: Checks if the town circle graphic has any VCRs, and, if it does,
//              if the VCRs are all supposed to be ducked.
//              This routine is only called when ducking the town circle
//
// AUTHOR: Sandra Peterson
//
// INPUTS: None
//
// OUTPUTS: TRUE, if no VCRs need to be printed
//          FALSE, otherwise
*****************************************************************************/

BOOL NoTownCircleVCRs(void)
{
    REGISTER_GROUPS sTempRegisters;
    unsigned char *pBuf;
    unsigned char bInx;
    BOOL fbRetCode = TRUE;


    if (sTCData[bCurrentTC].wNumOfVCRs != 0)
    {
        pBuf = sTCData[bCurrentTC].psFields;
        for (bInx = 0; (bInx < sTCData[bCurrentTC].bNumOfGroups) && (fbRetCode == TRUE); bInx++)
        {
            (void)memcpy((unsigned char *)&sTempRegisters, pBuf, REGISTER_GROUP_RECORD_SIZE);
            switch (sTempRegisters.wVariableID)
            {
            case VCR_PIN:
                // if the PIN is unducked, still have to load TC graphic.
                if (fbPINState == UNDUCKED)
                    fbRetCode = FALSE;
                break;

            case VCR_PRINTED_DATE:
                // if the date isn't fully ducked, still have to load TC graphic.
                if (bDateState != VLT_DUCKED_OUT_DATE)
                    fbRetCode = FALSE;
                break;

            case VCR_PRINTED_IBC:
                // if the printed indicia batch count is unducked, still have to load TC graphic.
                if (fbBatchCountState[INDICIA] == UNDUCKED)
                    fbRetCode = FALSE;
                break;

            case VCR_PRINTED_PBC:
                // if the printed permit batch count is unducked, still have to load TC graphic.
                if (fbBatchCountState[PERMIT] == UNDUCKED)
                    fbRetCode = FALSE;
                break;

            case VCR_PRINTED_TBC:
                // if the printed tax batch count is unducked, still have to load TC graphic.
                if (fbBatchCountState[TAX] == UNDUCKED)
                    fbRetCode = FALSE;
                break;

            case VCR_PRINTED_TIME:
                // if the date is fully unducked, still have to load TC graphic.
                if (fbTimeState == UNDUCKED)
                    fbRetCode = FALSE;
                break;

            case VCR_TOWN_LINE:
            case VCR_ZIP_CODE:
                // the town line (which may or may not include the zip code)
                // and the zip code automatically get ducked with the town circle
                break;

            default:
                // if it's a field that isn't duckable, still have to load TC graphic.
                fbRetCode = FALSE;
                break;
            }

            pBuf += REGISTER_GROUP_RECORD_SIZE;
        }
    }

    return (fbRetCode);
}

/****************************************************************************
// FUNCTION NAME: SearchStructure
// DESCRIPTION: Searches a given IG_COMPONENT_DATA structure for a given
//              component type
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bType = component type to look for in the component structure
//         bNumComponents = number of components described in the component structure
//         psCompData = pointer to an IG_COMPONENT_DATA structure, which has the ID(s)
//                      and addresses for all the necessary component(s)
//
// OUTPUTS: error code
//          *pbCompInx = if no error, index into the component structure where
//                      to find the data for the given component type
*****************************************************************************/

unsigned long SearchStructure(const unsigned char bType, const unsigned char bNumComponents,
                            const IG_COMPONENT_DATA *psCompData, unsigned char *pbCompInx)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned char   bInx;
	char	pLogBuf[50];


    for (bInx = 0; bInx < bNumComponents; bInx++)
    {
        if (psCompData->bType[bInx] == bType)
        {
            *pbCompInx = bInx;
            return (lErrCode);
        }
    }

	(void)sprintf(pLogBuf, "SearchStruct:Missing Graphic,ID= %u", bType);
	fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
    return (IG_MISSING_COMPONENT);
}


/****************************************************************************
// FUNCTION NAME: SearchStructureForPermit
// DESCRIPTION: Searches a given IG_COMPONENT_DATA structure for a given
//              permit component type
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bType = component type to look for in the component structure
//         bNumComponents = number of components described in the component structure
//         psCompData = pointer to an IG_COMPONENT_DATA structure, which has the ID(s)
//                      and addresses for all the necessary component(s)
//
// OUTPUTS: error code
//          *pbCompInx = if no error, index into the component structure where
//                      to find the data for the given component type
*****************************************************************************/

unsigned long SearchStructureForPermit(const unsigned char bType, const unsigned char bNumComponents,
                            const IG_COMPONENT_DATA *psCompData, unsigned char *pbCompInx)
{
    unsigned char   *pbData;
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned short  wTempName[MAX_PERMIT_UNICHARS+1];
    unsigned char   bByte1, bByte2, bInx1, bInx2, bInx3;
	char	pLogBuf[50];


    for (bInx3 = 0; bInx3 < bNumComponents; bInx3++)
    {
        if (psCompData->bType[bInx3] == bType)
        {
            // get the name descriptor.
            // can't do a word copy because we can't count on the address
            // being on an even-word boundary.
            pbData = psCompData->pbHeaderAddr[bInx3];
            bInx1 = GRAPHIC_NAME_DESC + 2;      // get past the 2 ID bytes
            for (bInx2 = 0 ; bInx2 < MAX_PERMIT_UNICHARS; bInx2++)
            {
                bByte1 = pbData[bInx1];
                bByte2 = pbData[bInx1+1];
                if ((bByte1 == 0) && (bByte2 == 0))
                    break;
                else
                {
                    wTempName[bInx2] = (bByte1 * 16) + bByte2;
                    bInx1 += 2;
                }
            }

            wTempName[bInx2] = 0;


            if (!unistrcmp(wIGPermitName, wTempName))
            {
                *pbCompInx = bInx3;
                return (lErrCode);
            }
        }
    }

	(void)sprintf(pLogBuf, "PmtSearchStruct:Missing Graphic,ID= %u", bType);
	fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
    return (IG_MISSING_COMPONENT);
}

unsigned long binImgDecode(unsigned char * pBuffer, long bufSize, short width, short height)
{
	unsigned long   lErrCode = IG_NO_ERROR;;
	unsigned bmpos;
	unsigned newpos;
	unsigned short x, y, xt, yt;
	unsigned char byte;
	unsigned char* bmp = &bImageBuffer2[0][0];


	if(bufSize < (width * height * 4))
		lErrCode = 20;

	for( y = 0; y < width; y++)
	{
		for( x = 0; x < height  ; x++)
		{
			//pixel start byte position in the BMP
			bmpos =  (y * IMAGE_ROW_BYTES) + x/8;

			//pixel start byte position in the new raw image
			//Need rotate graphic while generating raw image
			yt = x;
			xt = (width - 1) - y;
			newpos = 4 * yt * width  + 4 * xt;

			byte = bmp[bmpos];
	
			pBuffer[newpos + 0] = (byte & (1 << (7- (x%8))))? 0 : 0xFE; //R
			pBuffer[newpos + 1] = (byte & (1 << (7- (x%8))))? 0 : 0xFE; //G
			pBuffer[newpos + 2] = (byte & (1 << (7- (x%8))))? 0 : 0xFE; //B
			pBuffer[newpos + 3] = 255; //A
		}
	}

	return lErrCode;
}


unsigned long IGU_DecodeBin(const unsigned char *pbImage, unsigned short wBytes,
								 const unsigned short wStartCol, const unsigned short wStartRow,
								 const unsigned short wWidth, const unsigned short wHeight,
								 const unsigned short wMaxCols, const unsigned short wMaxRows);

bool ConvertBinToBmp(unsigned char *binBuffer, long binSize, unsigned short  pxlWidth, unsigned short  pxlHight, unsigned char *bmpBuffer);

unsigned long BuildPngFile(char *pGraphicName, unsigned char **pPng, size_t **pPngSize)
{
    unsigned long   lErrCode = IG_NO_ERROR;
    unsigned char* pImageData = NULL;
    GRAPHIC_DATA    GraphicsData;
    GRAPHIC_DATA    *psGraphicData = &GraphicsData;
    unsigned short  wOffset;
    long imagebufsize;
    unsigned char* image = NULL;
	unsigned char* png = NULL;
	size_t pngsize;
	UINT8   gfName[ GRFX_UNINAME_MAX_COMPARE_LEN ];
	short len;
	GFENT *pDirEntry;
	UINT16 dirIndex = 0;
	UINT8 *pLinkPtr;
	uchar *pGraphicDataAfterSig = NULL_PTR;


	len = fnAsciiBcdToPackedBcd(gfName, sizeof(gfName), pGraphicName, TRUE);
	if(len > 0)
	{
		gfName[len] = 0;
		gfName[len+1] = 0;

		pLinkPtr = fnGFGetDirInxByUniName(&dirIndex, ANY_COMPONENT_TYPE, (UNICHAR *)gfName, len, GRFX_ENABLED_ONLY);
		if(pLinkPtr != NULL)
		{
			pDirEntry = fnGFGetDirPtrByDirInx(dirIndex);
			if(pDirEntry != NULL)
			{
				if((pDirEntry->gfType != AD_COMP_TYPE) && (pDirEntry->gfType != INSCR_COMP_TYPE))
				{
					 lErrCode = IG_INVALID_COMPONENT;
				}
				else
				{
					pImageData = fnGrfxGetGrfkPtrByGenID( pDirEntry->gfType, pDirEntry->gfID, GRFX_ANY_MATCH );
					if(pImageData == NULL_PTR) 
					{
						lErrCode = IG_MISSING_COMPONENT;
					}
					else
					{
						if (!fnFlashGetGraphicMemberAddr(pImageData, GRAPHIC_DATA_AREA_AFTER_SIG ,(void**)&pGraphicDataAfterSig) )
						{
							lErrCode = IG_MISSING_COMPONENT;
						}

					}

				}
			}
			else
			{
				lErrCode = IG_MISSING_COMPONENT;
			}
		}
		else
		{
			lErrCode = IG_INVALID_IMAGE;
		}
	}
	else
	{
		lErrCode = IG_INVALID_IMAGE;
	}

	if(lErrCode == IG_NO_ERROR)
	{
		pImageData = pGraphicDataAfterSig;
		memset(psGraphicData, 0, sizeof(GRAPHIC_DATA));

		(void)EndianAwareCopy((unsigned char *)&wOffset, pImageData+GRAPHIC_VCRS_OFFSET, 2);
		psGraphicData->psVCRs = pImageData + wOffset;
		(void)EndianAwareCopy((unsigned char *)&wOffset, pImageData+GRAPHIC_IMAGE_OFFSET, 2);
		psGraphicData->pbImage = pImageData + wOffset;
		psGraphicData->psFields = pImageData + GRAPHIC_FIELDS;
		pImageData += GRAPHIC_WIDTH;
		(void)EndianAwareCopy((unsigned char *)&psGraphicData->wWidth, pImageData, 2);
		pImageData += 2;
		(void)EndianAwareCopy((unsigned char *)&psGraphicData->wHeight, pImageData, 2);
		pImageData += 2;
		psGraphicData->bNumOfGroups = *pImageData++;
		pImageData ++;                          // skip bPad1
		(void)EndianAwareCopy((unsigned char *)&psGraphicData->wNumOfVCRs, pImageData, 2);
		pImageData += 2;
		(void)EndianAwareCopy((unsigned char *)&psGraphicData->wSize, pImageData, 2);

		lErrCode =  IGU_DecodeBin(psGraphicData->pbImage, psGraphicData->wSize,
    									 0, 0,
    									 psGraphicData->wWidth, psGraphicData->wHeight,
    									 MAX_COLS, IMAGE_ROWS);
	}

    if(lErrCode == IG_NO_ERROR)
    {
		imagebufsize  = psGraphicData->wWidth * IMAGE_ROWS * 4;
		image = ( unsigned char*)malloc(imagebufsize);
		if(image != NULL)
		{
			lErrCode = binImgDecode(image, imagebufsize, psGraphicData->wWidth, IMAGE_ROWS);
			if(lErrCode == IG_NO_ERROR)
				lErrCode = lodepng_encode32(&png, &pngsize, image, psGraphicData->wWidth, psGraphicData->wHeight);

			*pPng = png;
			*pPngSize = pngsize;
		}
    }

	free(image);

	return lErrCode;
}





