/************************************************************************
 PROJECT:        Future Phoenix
 COPYRIGHT:      2005, Pitney Bowes, Inc.
 AUTHOR:         
 MODULE NMAE:    OiSleep.c

 DESCRIPTION:    This file contains all types of screen interface functions 
                 (hard key, event, pre, post, field init and field refresh)
                 and global variables pertaining to sleep mode.
//-----------------------------------------------------------------------------
 MODIFICATION HISTORY:

 01-May-15 sa002pe on FPHX 02.12 shelton branch
	In order to make sure all the timers get set up properly when the clock is resynced:
	1. Changed fnpExitSleep to call fnResyncTimers

  2014.04.03 Renhao            Fphx 02.12 cienet branch
   Modified function fnpExitSleep() to Reset the rates expired warning displayed flag 
   for G922 Rates Expiration  new feature.
   
  2012.10.10 Clarisa Bellamy   Fphx 02.10 
  - Fix fraca 215576 - If an update tag is received when not an a screen that 
    can handle it, handle it the next time one of those screens is reached.
     - Modify function fnpEnterSleep to check for a pending abacus host update, 
       and if found, set the launched-from-sleep flag and start the update.

    08/13/2008  Joey Cui        Merged stuffs from Janus to support PC proxy feature.
    07/18/2008  Raymond Shen    Modified fnpExitSleep() for battery life checking feature.
    11/29/2007  Andy Mo         Modified fnpExitSleep() to fix fraca133739&133684
    10/18/2007  Oscar Wang      Modified function fnhkCheckDelayedRpt() and fnevCheckDelayedRpt()
                                to fix FRACA GMSE00131327: Upload should prompt prior to 
                                preset loading.
    07/19/2007  Vincent Yi      Fixed lint errors
    06-June-07 I. Le Goff    In functions fnhkCheckDelayedRpt() and fnevCheckDelayedRpt()
                             exit mode ajout unless we have started to print
    09-May-07 de200ko           on FPHX 01.05 Secap branch
                                Changed fnhkCheckDelayedRpt() and fnevCheckDelayedRpt()
                                to avoid to restore normal preset when in ajout mode
    04/09/2007  Raymond Shen    Modified fnpEnterSleep to add operator log off action.
* 03-Apr-07 sa002pe on FPHX 01.04 shelton branch
*   Changed fnpEnterSleep to make sure the period is archived before doing the auto-upload
*   and to clear the meter lock flag when we setup to do an unattended
*   upload so the auto-upload will take precedence over the meter lock.
*
    03/27/2007  Oscar Wang      Modified function fnpExitSleep to check rate reminder workflow
    02/12/2007  Kan Jiang       Modified function fnhkCheckDelayedRpt and fnevCheckDelayedRpt
                                for Fraca GMSE00114529
    02/09/2007  Kan Jiang       Modified function fnhkCheckDelayedRpt and fnevCheckDelayedRpt
                                for Fraca GMSE00114529
    08/30/2006  Kan Jiang       Modified function fnhkCheckDelayedRpt and fnevCheckDelayedRpt
                                to fix Fraca 102926 and 103043.

    07/18/2006  Vincent Yi      Temporarily comment out the code on Data Capture
                                in fnpEnterSleep

    OLD PVCS REVISION HISTORY
 *    Rev 1.20   22 Jul 2005 16:59:22   HO501SU
 * Re-calculate the midnight event time and 
 * restart the timer, after each re-sync with
 * PSD.

*************************************************************************/


/**********************************************************************
        Include Header Files
**********************************************************************/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "oit.h"
#include "oisleep.h"
#include "global.h"
#include "nucleus.h"
#include "fwrapper.h"
#include "oitpriv.h"
#include "lcd.h"
#include "oifields.h"
#include "cmos.h"
#include "bob.h"
#include "bobutils.h"
#include "lcd.h"
#include "aiservic.h"
#include "errcode.h"
//#include "oipreset.h"
#include "oiclock.h"
//#include "deptaccount.h"
#include "clock.h"
#include "oiscreen.h"
#include "oipostag.h"
#include "custdat.h"
#include "suprcode.h"
#include "oipwrup.h"
#include "oifpmain.h"
#include "cwrapper.h"
#include "dcapi.h"
//#include "oiAbacus3.h"
#include "sys.h"
#include "ajout.h"
//#include "oigts.h"
//#include "oireport.h"

/**********************************************************************
        Local Function Prototypes
**********************************************************************/
static  BOOL    fSleepEnable = FALSE;
static DATETIME GMTtimeFallInSleep;



/**********************************************************************
        Local Defines, Typedefs, and Variables
**********************************************************************/



/**********************************************************************
        Global Functions Prototypes
**********************************************************************/



/**********************************************************************
        Global Variables
**********************************************************************/

//extern BOOL     fLowInkWarningShown;
//extern BOOL     fInspectionWarningShown;
//extern BOOL     fInspectionRequiredShown;
//extern IMAGE_SELECTIONS     rNewImageSelections;
//extern MemKeyTbl            CMOSMemKeys;
//extern BOOL     fMeterLocked;
//extern BOOL     fDCAPUploadDueShown;
//extern BOOL   fIsFromMMEnterNumber;  /*If it is from MMEnterNumber screen to EnterRefill screen,
//                                                      it is true. else it is false.*/
//extern SetupParams                 CMOSSetupParams;
extern BOOL     fDiffWeighPieceNeeded;
//extern BOOL     fOITDCAPDoUnattendedUpload;

//extern BOOL fInkLowWarned;

//extern BOOL fLowFundsWarningDisplayed;
//extern BOOL fLowPieceCountWarningDisplayed;

//extern ENTRY_BUFFER     rGenPurposeEbuf;
//extern unsigned short       usDcapWriteStatus;

BOOL            fDifferentialSleep = FALSE;
BOOL            fValidTimeOutValue = TRUE;

extern BOOL bRateExpHasBeenPrompted;
//extern BOOL CMOSRatesExpired;


/**********************************************************************
        Public Functions
**********************************************************************/
/* Utility functions */

/* **********************************************************************
// FUNCTION NAME: fnSleepTimerExpire
// DESCRIPTION:  Timer expiration routine that occurs when system has been
//                  inactive for the fixed duration.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none used
// **********************************************************************/
void fnSleepTimerExpire(unsigned long lwArg)
{
    /* put a message in the OIT queue indicating that the timer expired */
//    OSSendIntertask (OIT, OIT, OIT_SLEEP_TIMER_EXPIRED, NO_DATA, NULL, 0);

    /* stop this timer so it doesn't automatically retrigger */
    OSStopTimer(SLEEP_TIMER);

    return;
}

/* **********************************************************************
// FUNCTION NAME: fnEnableSleep
// DESCRIPTION:  Starts the sleep timer
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none used
// **********************************************************************/
void fnEnableSleep(void)
{
    /* start the sleep timer */
    OSStartTimer(SLEEP_TIMER);
    fSleepEnable = TRUE;
    return;
}

/* **********************************************************************
// FUNCTION NAME: fnDisableSleep
// DESCRIPTION:  Stops the sleep timer
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none used
// **********************************************************************/
void fnDisableSleep(void)
{
    /* stop the sleep timer */
    OSStopTimer(SLEEP_TIMER);
    fSleepEnable = FALSE;
    return;
}

/* **********************************************************************
// FUNCTION NAME: fnResetSleepTimer
// DESCRIPTION:  Resets the sleep timer to its full duration
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none used
// **********************************************************************/
void fnResetSleepTimer(void)
{
    /* if the sleep timer is enabled, turn the timer off then on again to
        reset it.  if the timer is not enabled, we don't want to do this
        because it would inadvertently reenable the timer */
    if (fSleepEnable == TRUE)
    {
        OSStopTimer(SLEEP_TIMER);
        OSStartTimer(SLEEP_TIMER);
    }
    return;
}

//******************************************************************************
// Function Name: fnSetSleepTimeout                                            *
// Description:  Sets the sleep timer duration                                 *
//                                                                             *
// AUTHOR: Joe Mozdzer, Ed Greene                                              *
//                                                                             *
// INPUTS:  none used                                                          *
//                                                                             *
// Modifications:                                                              *
// 1. Replaced  "if (fSleepEnable = TRUE)"  with  "if (fSleepEnable == TRUE)"  *
//    As the else branch would never execute!                                  *
//******************************************************************************

void fnSetSleepTimeout(unsigned long lwMilliseconds)
{


    // change the timer duration
    OSChangeTimerDuration(SLEEP_TIMER, lwMilliseconds);

    // if we were enabled, reset.  if we weren't enabled, make sure it didn't
    // start as a side effect of the os call.
    if (fSleepEnable == TRUE)
    {
        fnResetSleepTimer();
    }
    // Otherwise, put sleeper to pernament sleep
    else
    {
        OSStopTimer(SLEEP_TIMER);
    }

    // All Done, Bye-Bye
    return;
}




/* *************************************************************************
// FUNCTION NAME: 
//      fnEnableSleepTimeout 
//
// DESCRIPTION: 
//      Sets the sleep timer and enable it if base is attached
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      05/19/2006    John Gao      Initial version
// *************************************************************************/
void fnEnableSleepTimeout(void) 
{
    SetupParams *   pCMOSSetupParams = fnCMOSSetupGetCMOSSetupParams();
    
    if ((pCMOSSetupParams->wSleepTimeout) && (fnSYSIsBaseAttached() == TRUE))
    {
        /* adjust minutes to milliseconds */
        fnSetSleepTimeout(pCMOSSetupParams->wSleepTimeout * 60 * 1000); 
        fnEnableSleep();
    }
}


/* Pre/post functions */
/* *************************************************************************
// FUNCTION NAME:           fnpEnterSleep
// DESCRIPTION: 
//      Pre function that puts us in sleep mode.
//
// INPUTS:      Standard pre function inputs.
// OUTPUTS:     Go to next screen.
// RETURNS:     PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  2012.10.10 Clarisa Bellamy - Add check for Abacus Host Update pending. If an
//                          update is pending, go to the mHostedAbacusUploadProgress 
//                          screen to launch the update.
//  08/13/2008  Joey Cui        Release the Meter and send sleep message to PC
//  04/09/2007  Raymond Shen    Add code for Operator log off.
//  07/18/2006  Vincent Yi      Temporarily comment out the code on Data Capture
//  11/10/2005  Dicky Sun       Code clearup
//              Joe Mozdzer     Initial version
//
// *************************************************************************/
SINT8 fnpEnterSleep( void (** pContinuationFcn)(),
                     UINT32 *pMsgsAwaited,
                     UINT16 *pNextScr )
{

    DC_UPLOAD_STATUS_T  DCAPUploadStatus;
    // DATETIME  localtimeFallInSleep;        Test - to be deleted
        
//  fIsFromMMEnterNumber = FALSE;

    /* Note:  HF does NOT want the normal preset restored on the sleep timer like we did on 
        Spark, even as a temporary measure.  Eventually it will have its own timer. */

    if (fnOITGetPrintMode() == PMODE_DIFFERENTIAL_WEIGH)
    {
        fDifferentialSleep = TRUE;
        fDiffWeighPieceNeeded = TRUE;
    }

    /* If Operators in Use - Auto Logout Operators - Need to Check if Auto Log Off is Enabled MNH*/

//    if (fnGetAccountingType() != ACT_SYS_TYPE_NONE &&
//        fnGetAccountingType() != ACT_SYS_TYPE_INTERNAL_ON &&
//        fnAutoLogoutEnabled())
//    {
//        fnLogoutOperator();
//    }

    // If Abacus Host asked for an update while meter was on a screen that doesn't
    //  handle that, now is a good time to do the update.
/*    if( fnGetAccountingType() == ACT_SYS_TYPE_HOSTED_ABACUS )
    {
        // This will catch pending host updates and pending transaction updates.

        if( IsHostUpdateRequestPending() > 0 )
        {
            *pNextScr = GetScreen( SCREEN_HOSTED_ABACUS_UPLOAD_PROGRESS ); 
            // Chose NOT to clear the meterlock flag at this time, because we 
            //  want the meter to lock again after the update is done.
            SetUpdateLaunchedFromSleep();
            return( PREPOST_COMPLETE );
        }

    }*/

    /* turn off the LCD and backlight and change LED color */
    fnLCDOff();
    fnLEDSetColor(LED_AMBER);

    SET_PAGEUP_LED_OFF();
    SET_PAGEDOWN_LED_OFF();

    //  Remember the time going to sleep to be used in RTC resync when
    //  going out of sleep
    GetSysDateTime(&GMTtimeFallInSleep, FALSE, TRUE);

        /*---------- Test - to be deleted
        GetSysDateTime(&localtimeFallInSleep, TRUE, TRUE);

        sprintf(tmpBuf,"FallinSleep GMT Time %02d/%02d/%02d %02d:%02d:%02d", 
            GMTtimeFallInSleep.bMonth, GMTtimeFallInSleep.bDay, GMTtimeFallInSleep.bYear,
            GMTtimeFallInSleep.bHour,GMTtimeFallInSleep.bMinutes, GMTtimeFallInSleep.bSeconds);
        putString(tmpBuf, __LINE__);

        sprintf(tmpBuf,"FallinSleep Local Time %02d/%02d/%02d %02d:%02d:%02d", 
            localtimeFallInSleep.bMonth, localtimeFallInSleep.bDay, localtimeFallInSleep.bYear,
            localtimeFallInSleep.bHour,localtimeFallInSleep.bMinutes, localtimeFallInSleep.bSeconds);
        putString(tmpBuf, __LINE__);
        Test - to be deleted -----------*/

    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpExitSleep
//
// DESCRIPTION: 
//      Post function that takes us out of sleep mode.
//
// INPUTS:
//      Standard pre function inputs.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  04/03/2014  Renhao         Reset the rates expired warning displayed flag 
//                             for G922 Rates Expiration  new feature.
//  07/18/2008  Raymond Shen    Changed checking of PSD battery low to checking
//                              of PSD/CMOS battery status.
//  03/27/2007  Oscar Wang                     Check rate reminder workflow
//  11/10/2005  Dicky Sun                      Code clearup
//              Joe Mozdzer, Clarisa Bellamy   Initial version
//
// *************************************************************************/
SINT8 fnpExitSleep( void (** pContinuationFcn)(),
                   UINT32 *pMsgsAwaited )
{
    long            lDateTimeDiff = 0;
    long            lDateTimeDiff2 = 0;
    long            tmpVal = 0;
    ulong           lwTodSecs = 0;
    uchar           ucDummyData = 0;
    DATETIME        localTimeWakeUp, GMTTimeWakeUp, PsdGmtRtc, PsdAdjRtc; 


    // Get the time now to compare with the time when fell in sleep
    GetSysDateTime(&GMTTimeWakeUp, FALSE, TRUE); 
    GetSysDateTime(&localTimeWakeUp, TRUE, TRUE); 

    // Get current RTC from i-button
    if(fnGetPsdDateTime(&PsdGmtRtc, &PsdAdjRtc))
    {
        CalcDateTimeDifference(&GMTtimeFallInSleep, &PsdGmtRtc, &lDateTimeDiff);
        CalcDateTimeDifference(&GMTTimeWakeUp, &PsdGmtRtc, &lDateTimeDiff2);

        // Check to see whether time adjustment will accross date boundary
        lwTodSecs = fnTodToSecs(&localTimeWakeUp);

        // Make sure resync will not cross date boundary
        // and will not go backwards (lDateTimeDiff < 0)
        tmpVal = lwTodSecs + lDateTimeDiff2;

        if(    lDateTimeDiff > 0  // make sure it does not go backwards beyond the time falling in sleep
            && (   (lDateTimeDiff2 > 0 && tmpVal < 86400)    // make sure it does not move forward to next date
                || (lDateTimeDiff2 < 0 && tmpVal > 0)   ) )  // make sure it does not go backwards to previous date
        {
            if(fnValidData(BOBAMAT0, BOBID_IPSD_INIT_CLOCK, &ucDummyData) != BOB_OK)
            {
                fnProcessSCMError();
            }
            else
            {
				// Recalculate the alarm timers
				fnResyncTimers();
            }
        }

        /*----------- Test - to be deleted
        sprintf(tmpBuf,"Wakeup GMT Time %02d/%02d/%02d %02d:%02d:%02d", 
            GMTTimeWakeUp.bMonth, GMTTimeWakeUp.bDay, GMTTimeWakeUp.bYear,
            GMTTimeWakeUp.bHour,GMTTimeWakeUp.bMinutes, GMTTimeWakeUp.bSeconds);
        putString(tmpBuf, __LINE__);

        sprintf(tmpBuf,"Wakeup Local Time %02d/%02d/%02d %02d:%02d:%02d", 
            localTimeWakeUp.bMonth, localTimeWakeUp.bDay, localTimeWakeUp.bYear,
            localTimeWakeUp.bHour,localTimeWakeUp.bMinutes, localTimeWakeUp.bSeconds);
        putString(tmpBuf, __LINE__);
        Test - to be deleted -----------*/

    }

    fnResetAllWarningCheckFlags();

	
    /* reset the rates expired warning displayed flag */
    bRateExpHasBeenPrompted = FALSE;

    /* bring the lcd back up */
    fnLCDInit();
    fnLEDSetColor( LED_GREEN );

//    fnSetSourceComingFrom(0);
    // If the lock code is enabled, lock the meter, and force the user 
    //  to enter the lock code to access the meter.
    if( fnCheckLockCodeEnabled() )
//        fMeterLocked = TRUE;

    // check PSD/CMOS battery status and store it in an OIT variable during power up.
    fnOITSetBatteryStatus(fnBobCheckBatteryStatus());

//    fnUpdateNewRatesNormalPresetFlag();

    return( PREPOST_COMPLETE );
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpEnterPowerDown
//
// DESCRIPTION: 
//      Pre function that puts us in power down mode.
//
// INPUTS:
//      Standard pre function inputs.
//
// OUTPUTS:
//      Go to next screen.
//
// RETURNS:
//      PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Joe Mozdzer         Initial version
//  06/06/2006  Raymond Shen        Code clearup
//
// *************************************************************************/
SINT8 fnpEnterPowerDown( void (** pContinuationFcn)(),
                     UINT32 *pMsgsAwaited,
                     UINT16 *pNextScr )
{
//  fIsFromMMEnterNumber = FALSE;

    /* Note:  HF does NOT want the normal preset restored on the sleep timer like we did on 
        Spark, even as a temporary measure.  Eventually it will have its own timer. */

    if (fnOITGetPrintMode() == PMODE_DIFFERENTIAL_WEIGH)
    {
        fDifferentialSleep = TRUE;
        fDiffWeighPieceNeeded = TRUE;
    }

    /* turn off the LCD and backlight and change LED color */
    fnLCDOff();
    fnLEDSetColor(LED_OFF);

    SET_PAGEUP_LED_OFF();
    SET_PAGEDOWN_LED_OFF();

    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}

/* Field functions */

/* Hard key functions */

/* *************************************************************************
// FUNCTION NAME: fnhkCheckDelayedRpt
//
// DESCRIPTION: 
//      Hard key function that checks if we need to print an
//      outstanding report (such as a refill receipt or a delivery
//      confirmation report) and branches to the appropriate screen
//      if so.
//
// INPUTS:  
//      Standard hard key function inputs.
//
// RETURNS:
//      next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//    10/18/2007  Oscar Wang    Fix FRACA GMSE00131327: Upload should prompt 
//                              prior to preset loading.
//    06/14/2007  I. Le Goff  Exit mode ajout unless we have started to print
//    05/09/2007  D. Kohl       Don't restore normal preset if in ajout mode
//    02/12/2007  Kan Jiang     Add the invoking of fnPowerUpRestoreNormalPreset
//    02/09/2007  Kan Jiang     Modified for Fraca GMSE00114529
//    08/30/2006  Kan Jiang     Modified to fix Fraca 102926 and 103043
//      Joe Mozdzer/Ramprasad   Initial version 
// *************************************************************************/
T_SCREEN_ID fnhkCheckDelayedRpt(UINT8        bKeyCode,
                                UINT8        bNumNext,
                                T_SCREEN_ID *pNextScreens)
{
     T_SCREEN_ID usNext = pNextScreens[0];

   if( ! fnAreWeInModeAjout() )
   {
      // We are not in mode ajout: restore normal preset
	   //TODO: this follinwing function does not yet exist....
      //fnPowerUpRestoreNormalPreset();
   }
   else if( ! fnIsAjoutPrintingInProgress() )
   {
      // We are in mode ajout, but we haven't started to print: exit mode ajout
      fnExitModeAjout();
   }
   
   // TODO: this function does not yet exist...
    // fnSetRestorePresetFlag();
    
     return (usNext);
}


/* Event functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fnevCheckDelayedRpt
//
// DESCRIPTION: 
//      Event function that checks if we need to print an
//      outstanding report (such as a refill receipt or a delivery
//      confirmation report) and branches to the appropriate screen
//      if so.
//
// INPUTS:
//      Standard event function inputs.
//
// OUTPUTS:
//      Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//    10/18/2007  Oscar Wang    Fix FRACA GMSE00131327: Upload should prompt 
//                              prior to preset loading.
//    06/14/2007  I. Le Goff  Exit mode ajout unless we have started to print
//    05/09/2007  D. Kohl       Don't restore normal preset if in ajout mode
//    02/12/2007  Kan Jiang     Add the invoking of fnPowerUpRestoreNormalPreset
//    02/09/2007  Kan Jiang     Modified for Fraca GMSE00114529
//    08/30/2006  Kan Jiang     Modified to fix Fraca 102926 and 103043
//      Joe Mozdzer/Ramprasad       Initial version
// *************************************************************************/
T_SCREEN_ID fnevCheckDelayedRpt(UINT8        bNumNext, 
                                T_SCREEN_ID *pNextScreens, 
                                INTERTASK   *pIntertask)
{
     T_SCREEN_ID usNext = pNextScreens[0];
     
//     fnSetRestorePresetFlag();

     return (usNext);
}



/* Variable graphic functions */



/**********************************************************************
        Private Functions
**********************************************************************/


