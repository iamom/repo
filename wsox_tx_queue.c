
#define __wsox_tx_queue_c__
#include "pbos.h"
#include "thread_control.h"
#include "debugTask.h"
#include "wsox_tx_queue.h"

extern int verbosity;

// low level function to add entry to queue without logging
static STATUS addRawEntryToTxQueue(WSOX_Queue_Entry *entry, cJSON *req_msg, UNSIGNED uSusMode)
{
    STATUS status;

    if (entry->entry_type == WSOX_QET_CJSON || entry->entry_type == WSOX_QET_CJSON_BROADCAST)
    {
        if (req_msg != NULL && cJSON_HasObjectItem(req_msg, "SeqNum") == 1)
        {
            // There is an SeqNum in the req_msg. Copy that
            cJSON_AddStringToObject(entry->i.cjson_info.msg, "SeqNum", cJSON_GetObjectItem(req_msg, "SeqNum")->valuestring);
        }
        if (req_msg != NULL && cJSON_HasObjectItem(req_msg, "TranID") == 1)
        {
            // There is an TranID in the req_msg. Copy that
            cJSON_AddStringToObject(entry->i.cjson_info.msg, "TranID", cJSON_GetObjectItem(req_msg, "TranID")->valuestring);
        }
    }

    status = NU_Send_To_Queue(&Queue_wsox_tx, entry, sizeof(WSOX_Queue_Entry)/sizeof(UNSIGNED), uSusMode);
    if(status != NU_SUCCESS)
    {
    	if(entry->i.cjson_info.msg)
    		cJSON_Delete(entry->i.cjson_info.msg);
    }
    return status;
}

// Public function to be called from HISRs and very high priority tasks
// - no suspension and very simple logging
void addEntryToTxQueueFast(WSOX_Queue_Entry *entry, cJSON *req_msg)
{
    STATUS status;

    status = addRawEntryToTxQueue(entry, req_msg, NU_NO_SUSPEND);
    if(status != NU_SUCCESS)
    {
    	fnDumpStringToSystemLogWithNum("addEntryToTxQueueFast: NU_Send_To_Queue() returned: ", status);
    }

}


/* ---------------------------------------------------------------------------------------------[addEntryToTxQueue]-- */
void addEntryToTxQueue(WSOX_Queue_Entry *entry, cJSON *req_msg, char *logMsgFmt)
{
    STATUS status;

    // if we are in an HISR then do not suspend the add to queue operation!
    if (!OSRunningInTask())
    {//HISR
    	return addEntryToTxQueueFast(entry, req_msg);
    }

    status = addRawEntryToTxQueue(entry, req_msg, NU_SUSPEND);
    if(status != NU_SUCCESS)
    {
    	dbgTrace(DBG_LVL_ERROR, "addEntryToTxQueue: NU_Send_To_Queue() returned %d \r\n", status);
    }

    if (verbosity > 3)
    	dbgTrace(DBG_LVL_INFO, logMsgFmt, status);
}


/* eof */

