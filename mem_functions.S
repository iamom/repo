/*
*   Optimized memset routine.
*/

// .set INSTRUMENTED_MEMFUNCTIONS, 1

/* --------------------------------------------------------------------------------------------------------[MEMSET]-- */
/*
*   Optimized memset routine.
*
*   parameters:
*      r0 = destination addr
*      r1 = fill value
*      r2 = byte count
*/

	.arm
.ifdef INSTRUMENTED_MEMFUNCTIONS
	.section .text.memset_asm,"ax",%progbits
	.globl memset_asm
	.type	memset_asm, %function
.else
	.section .text.memset,"ax",%progbits
	.globl memset
	.type	memset, %function
.endif


align_start:                        @ write the memset value to the first few bytes
    subs	r2, r2, #4		        @ until we are on a multiple of 4 byte address
    blt	    memset_3_or_less_bytes  @ if not enough bytes, we can do the tail end memset
    cmp	    r3, #2
    strltb	r1, [r0], #1
    strleb	r1, [r0], #1
    strb	r1, [r0], #1
    add	    r2, r2, r3	            @ Adjust length (r2 = r2 - (4 - r3))
    /*
    * The pointer is now aligned and the length is adjusted.  Try doing the
    * memset again.
    */

.ifdef INSTRUMENTED_MEMFUNCTIONS
memset_asm:
.else
memset:
.endif

    ands	r3, r0, #3      @ r3 now contains lowest 2 bits of addr
    bne	    align_start	    @ align start if necessary 

    /*
     * we know that the pointer in r0 is aligned to a uint32 boundary.
     */
    orr	r1, r1, r1, lsl #8  @ memset value now in lower 2 bytes
    orr	r1, r1, r1, lsl #16 @ memset value now in all 4 bytes
    mov	r3, r1
    cmp	r2, #16
    blt	memset_8_bytes

    /*
    * We need an extra register for this loop - save the return address and
    * use the LR
    */
    str	lr, [sp, #-4]!
    mov	ip, r1
    mov	lr, r1

memset_64_bytes:	
    subs	r2, r2, #64
    stmgeia	r0!, {r1, r3, ip, lr}	@ 64 bytes at a time.
    stmgeia	r0!, {r1, r3, ip, lr}
    stmgeia	r0!, {r1, r3, ip, lr}
    stmgeia	r0!, {r1, r3, ip, lr}
    bgt	memset_64_bytes
    ldmeqfd	sp!, {pc}		@ if no more bytes to do, return to caller (return address was pushed onto stack!)

    /*
    * Now <64 bytes to go. No need to correct the count; we're only testing bits from now on
    */
    tst	r2, #32
    stmneia	r0!, {r1, r3, ip, lr}
    stmneia	r0!, {r1, r3, ip, lr}
    tst	r2, #16
    stmneia	r0!, {r1, r3, ip, lr}

    /* restore lr register */
    ldr	lr, [sp], #4

memset_8_bytes:
    tst	r2, #8
    stmneia	r0!, {r1, r3}
    tst	r2, #4
    strne	r1, [r0], #4

/*
* When we get here, we've got less than 4 bytes to set.  We
* may have an unaligned pointer as well.
*/
memset_3_or_less_bytes:
    tst	r2, #2
    strneb	r1, [r0], #1
    strneb	r1, [r0], #1
    tst	r2, #1
    strneb	r1, [r0], #1
    mov	pc, lr
    bx lr

/* -------------------------------------------------------------------------------------------------------[WMEMSET]-- */
/*
*   Optimized wmemset routine.
*
*   parameters:
*      r0 = destination addr
*      r1 = fill value (16 bit value)
*      r2 = halfword count (# of 16 bit values)
*/

	.arm
.ifdef INSTRUMENTED_MEMFUNCTIONS
	.section .text.wmemset_asm,"ax",%progbits
	.globl wmemset_asm
	.type	wmemset_asm, %function
.else
	.section .text.wmemset,"ax",%progbits
	.globl wmemset
	.type	wmemset, %function
.endif

    /* r3 == lowest 2 bits of destination address */
align_start_w:                       @ write the wmemset value to the first few bytes
                                     @ until we are on a multiple of 4 byte address

    subs	r2, r2, #4		         @ first subtract 4 from byte count
    blt	    wmemset_3_or_less_bytes  @ if not enough bytes, we can do the tail end memset
    cmp	    r3, #2
    strltb	r1, [r0], #1             @ store if r3 == 1 (1st of 3 bytes)
    strleh	r1, [r0], #2             @ store if r3 == 1 or 2 (write 2 bytes)
    cmp	    r3, #3
    streqb	r1, [r0], #1             @ store if r3 == 3 (1 byte)
    add	    r2, r2, r3	             @ Adjust length (r2 = r2 - (4 - r3))
    /*
    * The pointer is now aligned and the length is adjusted.  Try doing the
    * memset again.
    */
    b	    wmemset_restart




.ifdef INSTRUMENTED_MEMFUNCTIONS
wmemset_asm:
.else
wmemset:
.endif
    lsl	    r2,r2,#1	    @ r2 is now a count in bytes instead of in 16-bit words 
wmemset_restart:
    ands	r3, r0, #3      @ r3 now contains lowest 2 bits of addr
    bne	    align_start_w	    @ align start if necessary 

    /*
     * we know that the pointer in r0 is aligned to a uint32 boundary.
     */
    orr	r1, r1, r1, lsl #16 @ wmemset value now in all 4 bytes
    mov	r3, r1              @ wmemset value now in r1 & r3
    cmp	r2, #16
    blt	wmemset_8_bytes

    /*
    * We need an extra register for this loop - save the return address and
    * use the LR
    */
    str	lr, [sp, #-4]!
    mov	ip, r1
    mov	lr, r1

wmemset_64_bytes:	
    subs	r2, r2, #64
    stmgeia	r0!, {r1, r3, ip, lr}	@ 64 bytes at a time.
    stmgeia	r0!, {r1, r3, ip, lr}
    stmgeia	r0!, {r1, r3, ip, lr}
    stmgeia	r0!, {r1, r3, ip, lr}
    bgt	wmemset_64_bytes
    ldmeqfd	sp!, {pc}		@ if no more bytes to do, return to caller (return address was pushed onto stack!)

    /*
    * Now <64 bytes to go. No need to correct the count; we're only testing bits from now on
    */
    tst	r2, #32
    stmneia	r0!, {r1, r3, ip, lr}
    stmneia	r0!, {r1, r3, ip, lr}
    tst	r2, #16
    stmneia	r0!, {r1, r3, ip, lr}

    /* restore lr register */
    ldr	lr, [sp], #4

wmemset_8_bytes:
    tst	r2, #8
    stmneia	r0!, {r1, r3}
    tst	r2, #4
    strne	r1, [r0], #4

/*
* When we get here, we've got less than 4 bytes to set.  We
* may have an unaligned pointer as well.
*/
wmemset_3_or_less_bytes:
    tst	r2, #2
    strneh	r1, [r0], #2
    tst	r2, #1
    strneb	r1, [r0], #1
    mov	pc, lr
    bx lr


/* --------------------------------------------------------------------------------------------------------[MEMCPY]-- */
/*
*   Optimized memcpy routine.
*
*   parameters:
*      r0 = destination addr
*      r1 = source address
*      r2 = byte count
*/


	.arm
    .ifdef INSTRUMENTED_MEMFUNCTIONS
        .section .text.memcpy_asm,"ax",%progbits
        .globl memcpy_asm
        .type	memcpy_asm, %function
    .else
        .section .text.memcpy,"ax",%progbits
        .globl memcpy
        .type	memcpy, %function
    .endif


.ifdef INSTRUMENTED_MEMFUNCTIONS
memcpy_asm:
.else
memcpy:
.endif

    cmp     r2, #8
    bcc     memcpy_unaligned    @ if <8 bytes --> do simple unaligned copy

    ands	r3, r0, #3          @ r3 now contains lowest 2 bits of dst addr
    bne	    memcpy_unaligned    @ do an unalign copy if dst addr isn't on a 4 byte boundry 
    ands	r3, r1, #3          @ r3 now contains lowest 2 bits of src addr
    bne	    memcpy_unaligned    @ do an unalign copy if src addr isn't on a 4 byte boundry 

    /*
     * we know that both src and dst is aligned to a uint32 boundary.
     */
    /*
    * We need extra registers for this loop - use r4 .. r5
    * we will also use r12 (aka ip register) since that doesn't need to be saved
    * we also save r0 since we need to return the dst address on return
    */
    stmdb   sp!, {r0,r4,r5}

    cmp     r2, #16
    blt	memcpy_8_bytes


memcpy_64_bytes:	
    subs	r2, r2, #64
    ldmgeia r1!, {r3, r4, r5, r12}
    stmgeia r0!, {r3, r4, r5, r12}   @ 16 bytes at a time.
    ldmgeia r1!, {r3, r4, r5, r12}
    stmgeia r0!, {r3, r4, r5, r12}   @ 16 bytes at a time.
    ldmgeia r1!, {r3, r4, r5, r12}
    stmgeia r0!, {r3, r4, r5, r12}   @ 16 bytes at a time.
    ldmgeia r1!, {r3, r4, r5, r12}
    stmgeia r0!, {r3, r4, r5, r12}   @ 16 bytes at a time.
    bgt     memcpy_64_bytes

    /*
    * Now <64 bytes to go. No need to correct the count; we're only testing bits from now on
    */
memcpy_32_bytes:	
    tst     r2, #32
    ldmneia r1!, {r3, r4, r5, r12}
    stmneia r0!, {r3, r4, r5, r12}   @ 16 bytes at a time.
    ldmneia r1!, {r3, r4, r5, r12}
    stmneia r0!, {r3, r4, r5, r12}   @ 16 bytes at a time.

memcpy_16_bytes:	
    tst	r2, #16
    ldmneia r1!, {r3, r4, r5, r12}
    stmneia r0!, {r3, r4, r5, r12}   @ 16 bytes at a time.

memcpy_8_bytes:
    tst	r2, #8
    ldmneia r1!, {r3, r4}
    stmneia r0!, {r3, r4}           @ 8 bytes at a time.

    tst	r2, #4
    ldrne   r3, [r1], #4
    strne   r3, [r0], #4

/*
* When we get here, we've got less than 4 bytes to set.
*/
memcpy_3_or_less_bytes:
    tst     r2, #2
    ldrneh  r3, [r1], #2
    strneh	r3, [r0], #2

    tst	    r2, #1
    ldrneb  r3, [r1], #1
    strneb  r3, [r0], #1


    ldmia   sp!, {r0,r4,r5}
    bx      lr


memcpy_unaligned:
    cmp     r2, #0
    beq     memcpy_exit
    stmdb   sp!, {r0}

memcpy_unaligned_loop:
    ldrb    r3, [r1], #1
    strb    r3, [r0], #1
    subs    r2, r2, #1
    bne     memcpy_unaligned_loop

    ldmia   sp!, {r0}
memcpy_exit:
    bx lr

