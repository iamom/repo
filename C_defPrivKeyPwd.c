/*************************************************************************
*   PROJECT:        Horizon CSD2-3
*	MODULE NAME:	C_defPrivKeyPwd.c
*
*	DESCRIPTION:	This file contains the obfuscated default private key password.
*
* ----------------------------------------------------------------------
*               Copyright (c) 2017 Pitney Bowes Inc.
*                    37 Executive drive
*                   Danbury, Connecticut  06810
* ----------------------------------------------------------------------
*
*************************************************************************/

const unsigned char defPrivKeyRand3[] = {0xA5, 0xC3, 0x1D, 0x61, 0x19, 0x8D, 0x1C, 0x72};
const unsigned char defPrivKeyPwd4[] = {0x8A, 0x50, 0xFE, 0x88, 0xFF, 0xE3, 0xC2, 0x43};
