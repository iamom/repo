/************************************************************************
    PROJECT:		Future Phoenix
    COPYRIGHT:		2005, Pitney Bowes, Inc.
    AUTHOR:			
    MODULE NMAE:	OifpPrint.c

    DESCRIPTION:    This file contains all types of screen interface functions 
                    (hard key, event, pre, post, field init and field refresh)
                    and related global variables that are applicable to
                    printer/motion control handling for FP.

//-----------------------------------------------------------------------------
    MODIFICATION HISTORY:

  01-Jun-17 Jennifer Hao on FPHX 02.12 cienet branch         
    Commented out functions fnhkSwitchModeAdOnly, fnhkSwitchModeDateTimeOnly because  they aren't used by G9.  
    
  22-May-17 Jennifer Hao on FPHX 02.12 cienet branch
     Commented out functions  fnModeSetPermit, fnExeAlterNativeMode, fnhkResetAltermode, because they aren't used by G9.    

	
 09-Apr-15 sa002pe on FPHX 02.12 shelton branch
 	For Fraca 229144: Changed fnProcessMailJobCompleted to go to the "Another Piece" screen
	for Brazil Tracking Services if the GTS prompts are turned off.
	
	Also changed fnProcessMailJobCompleted to use the fnIsDWPiecePrinted subroutine instead
	of doing the same checks the subroutine does.
	
    04/09/2015  Way Zhang	    Modified function fnOITSetPrintMode() to fix fraca 229109 .
    05/04/2014  Renhao          Modified function fnhkModeSetDefault() to fix fraca 225148.
    02/11/2014  Wang Biao       Modified function fnCheckPostCorrectionVal() to fix fraca 224400.
    11/01/2012  John Gao        Modified function fnStartToPrintTape, don't set up abacus static mailrun information for report printing
	09/17/2012  Bob Li			Modified function fnOITSetPrintMode() to make sure the MWE mode not displayed if the MWE feature is disabled.
    05/10/2010  Deborah Kohl    Modified all functions testing (bFieldCtrl & ALIGN_MASK) 
                                to add padding characters either when right aligned and
                                in LTR mode or left aligned and in RTL mode    
                                Modified fnfIndiciaTapePrintingStatus() to have  1/2 Printing in RTL mode
    02/28/2010  Deborah Kohl    Modified fnfPrintPositionRange() to manage Right to Left writing
    02/24/2010  Raymond Shen    Modified fnStartToPrintMail() and fnStartToPrintTape() to fix the problem that report can't be printed out
                                if there is any GTS error.
    12/14/2009  Raymond Shen    Added function fnfIndiciaTapePrintingStatus1() to fix fraca 173754.
    11/04/2009  Raymond Shen    Modified fnevTapeJobCompleted() to fix GMSE00173925 (G910/DM300c - Meter should display "Tape 
                                Printing Paused" screen when press stop key in Date Correction Mode.).
    10/16/2009  Raymond Shen    Remove all the changes for GMSE00173492 in previous version because there is a better solution in oiabacus2.c.
    10/15/2009  Raymond Shen    Modified fnhkValidateTapeCountToPrint(), fnhkKIPValidateTapeCountToPrint() and fnSetMultiTapeFlag()
                                to fix GMSE00173492 (G910/DM300c - Meter is not recording any transactions done with a tape strip
                                when set "Ask For Tape Quantity").
    10/12/2009  Jingwei,Li      Modified fnProcessMailJobCompleted() and ()fnhkSwitchModeManifest() to fix fraca for CPS.
    09/24/2009  Jingwei,Li      Modified fnhkSwitchModeManifest() and fnhkSwitchModePostageCorrection() to fix fraca 
                                GMSE00170880 and GMSE00171003.
    09/11/2009  Raymond Shen    Modified fnProcessMailJobCompleted() to fix a Canada package service problem on the condition
                                that all the confirmation prompts are turned off.
    11/28/2008  Joe Qu          Modified fnfIndiciaTapePrintingStatus() to fix fraca GMSE00126761,
                                clean up the display when displayed mail sequence number exceeds the buffer length
    07/29/2009  Jingwei,Li      Modified fnhkCancelPrintingTape() for Canada Package service.
    07/28/2009  Raymond Shen    Modified fnevTapeJobCompleted() to support "Revert to WOW" after printing Tape for DM475C.
    07/17/2009  Raymond Shen    Added fnhkResumePrintingWithWOWError() for WOW error handling.
    06/25/2009  Jingwei,Li      Modified fnOITSetPrintMode() and fnhkCheckHybridMode() to save/restore Master context if change mode between
                                WOW/W1P and platform mode.
    06/24/2009  Raymond Shen    Modified fnhkResumePrintingWithFeeder() to remove Jingwei's previous modification because do
                                check the status of sensor W2 and W3 when receiving the event that CM error is cleared.
    06/23/2009  Jingwei,Li      Modified fnhkResumePrintingWithFeeder() for resume printing with mail jam.
    05/18/2009  Raymond Shen    Modified fnhkResumePrintingFeederCover() and fnevContinueToPrintMail() for wow error handling.
    05/20/2009  Jingwei,Li      Modified fnhkCheckHybridMode() to avoid 2003 error.
                                Modified fnIsWOWDREnabled() to whether length calibration is done.
    05/18/2009  Raymond Shen    Added fnhkResumePrintingWOWMailJammed() and modified fnfIndiciaPrintingStatus(), fnhkResumePrintingFeederCover(),
                                fnhkCancelPrintingFeederCover()for WOW.
    05/13/2009  Fred Xu         Added fnSetMultiTapeFlag and fnIsMultiTapeOK and a local variable fMultiTapeOK to fix FRACA GMSE00161089.
    04/30/2009  Jingwei Li      Added fnIsWOWDREnabled() and modified fnfPrintMode() for WOW+SBR.
    01/22/2009  Raymond Shen    Added fnevUpdatePrinterState() and modified fnfIndiciaPrintingStatus() for WOW.
    10/24/2008  Raymond Shen    Added code to support Brazil KIP Indicia HRT selection.
    10/06/2008  Oscar Wang      Modified fnProcessMailJobCompleted for Hollands GTS problem: sometimes 
                                meter doesn't display EServiceCMOSLogFull screen when the number of cmos 
                                log reaches 100.
    12/14/2007  Raymond Shen    Added code to clear "mail piece completed" flag in function
                                fnStartToPrintMail() and fnStartToPrintTape().
    11/20/2007  Oscar Wang      Modified functions to handle GTS interruption cases.
    11/14/2007  Vincent Yi      Modified fnStartToPrintMail and fnStartToPrintTape, they should
                                return 0 if there is any error after calling fnGTS_StartMailRun
    11/06/2007  Vincent Yi      Modified function fnStartToPrintMail and fnStartToPrintTape, 
                                added code to set up abacus static info before running
    11/06/2007  Oscar Wang      Modified function fnhkResumePrintingTape()to fix FRACA GMSE00131947: 
                                for GTS mail, if debit is done and paper error happens, we can't resume
                                printing even for tape since barcode is already used.
    10/24/2007  Oscar Wang      Modified function fnProcessMailJobCompleted() to fix FRACA GMSE00131603:
                                Replace fnhkModeSetDefault() with fnRatingSetDefault().
                                Modified function fnhkModeSetDefault() to clear retained dest info for 
                                clear key at home screen.
    10/18/2007  D.Kohl          FPHX 1.09 (Swiss adaptations) 
                                Modified fnOITSetPrintMode() for SWISS KIP
    10/10/2007  Oscar Wang      Modified fnhkModeSetDefault() for Diff Wt mode.
    09/25/2007  Vincent Yi      Modified fnProcessMailJobCompleted()
                                to add condition-check on fnIsGTSPieceProcessed()
    09/19/2007  Oscar Wang      Modified functions fnStartToPrintMail and fnStartToPrintTape: Check 
                                GTS error conditions before starting printing.
    09/18/2007  Oscar Wang      Modified functions fnStartToPrintMail and fnStartToPrintTape: For 
                                report printing, don't care GTS_NO_TRACKING_IDS error.                                
    09/10/2007  Oscar Wang      Modified function fnhkValidateTapeCountToPrint to set flag 
                                fBarcodeNoneReset for out of barcode case to retain class.
    09/07/2007  Oscar Wang      For tape printing, added "no enough barcode to print tape" screen.
                                Added function fnhkValidateRemainingGTSTapeCount.
                                Modified function fnhkValidateTapeCountToPrint.
    08/31/2007  Joey Cui        Modified fnevSendRateReady() to re check rerate
 	08/31/2007  Vincent Yi      Fixed fraca 128604, 
 	                            Renamed functions fnClearFlagDWPiecePrintedForDM400C()
 	                            to fnClearFlagDWPiecePrinted(), fnIsDWPiecePrintedForDM400C()
 	                            to fnIsDWPiecePrinted()
 	                            Added functions fnProcessMailPieceCompleted()
 	                            fnProcessMailJobCompleted() into both mail and tape printing
 	                            process
    08/27/2007  Oscar Wang      Update fnevMailJobCompleted and fnevTapeJobCompleted for case ONLY 
                                manifest prompt is ON
    8/23/2007   Vincent Yi      Updated fnevTapePrintingComplete for GTS + diff. wt
    8/23/2007   Vincent Yi      Fixed fraca 128096, modified function fnhkKIPValidateTapeCountToPrint()
                                in GTS single piece mode or diff. wt mode, only one tape printed out
    08/22/2007  Joey Cui        Modified fnhkSwitchModeTextAdDateTime() and fnhkSwitchModePermit()
                                to clear rate info before mode switch, fixed FRACA GMSE00128014, 
    08/22/2007  Oscar Wang      Modified fnhkCheckTapeConfig() to fix FRACA GMSE00128011.
                                For normal GTS (not fast mode), only print 1 tape.
    08/21/2007  Adam Liu        Modified fnOITSetPrintMode() to allow zero postage KIP mode on 
                                Canadian large meter, fix fraca GMSE00127939.
    08/20/2007  Andy  Mo        Modified fnTapeBufferValid() to fix fraca 127845
    08/15/2007  Oscar Wang      Modified tape mail job complete event function to handle error for 
                                Germany GTS Non-Stop mode, added function fnSendStopRequest.
    07/19/2007  Vincent Yi      Fixed lint errors
    07/05/2007  I. Le Goff In fnhkResumePrintingWithFeeder(),  and fnhkResumePrintingFeederCover() 
                            modify so we don't resume the print in mode ajout or when incident reports
                            are enabled.
    06/29/2007  I.. Le Goff In fnhkResumePrintingTape(), Modifiy so we don't resume the print 
                            in mode ajout or when incident reports are enabled.
    06/29/2007  Raymond Shen    Modified fnhkSwitchModePostageCorrection(), fnhkSwitchModeManifest(),
                                to fix fraca 122018.
    06/27/2007  Raymond Shen    Modified fnOITSetPrintMode() to forbid KIP mode on Canadian large meter.
    06/12/2007  Vincent Yi      Fix fraca 121244, added functions fnhkResumePrintingFeederCover()
                                fnhkCancelPrintingFeederCover()
  05/28/2007  I. Le Goff    In fnevTapePrintingComplete(), exit ajout if we are done 
                            printing all the tapes.
    15 May 07 Bill Herring  fnevTapePrintingComplete() now checks and exits ajout mode.
    05 May 2007	B Herring   Removed unnecessary ZWZP code from fnevTapePrintingComplete().
   10 May 2007  Simon Fox   Fixed a bug in the previos fix which was preventing
                            any use of KIP in countries where KIP is limited to
                            zero (currently only France).
   01 May 2007  Simon Fox   fnOITSetPrintMode - Don't select KIP mode by
                            default if KIP is limited to 0 and password
                            protected.
	04/27/2007  Andy Mo		Modified fnhkKIPValidateTapeCountToPrint()to display invalid message if the entered 
							tape quantity is invalid. 
	04/26/2007	Andy Mo		1.Modified fnhkSwitchModeManifest(),removed unused stuff which was merged from k700
							2.Modified in fnhkSwitchModeDateCorrection()by adding fnClearDateCorrection()
	04/20/2007	Andy Mo		Added fnhkSwitchModePostageCorrection() to switch mode to
							Postage Correction.
* 17-Apr-07 sa002pe on FPHX 01.04 shelton branch
*	Added bAlternMode & dbTempManifestPostage.
*	Changed fnOITSetPrintMode to add a case for PMODE_MANIFEST.
*	Added fnhkSwitchModeDateCorrection, fnhkSwitchModeManifest, fnSetAlterNativeMode,
*	fnExeAlterNativeMode, fnhkResetAltermode & fnCheckPostCorrectionVal.
*
* 06-Apr-07 sa002pe on FPHX 01.04 shelton branch
*	Changed the use of BP_KEY_IN_POSTAGE_MODE to clear the Dcap-related bit to get the
*	true KIP mode value.
*
    30 Mar 2007 Bill Herring    Modified fnhkModeSetManual() to check additional
                                KIP password type (zero postage only).
    03/05/2007  I. Le Goff    In fnfPrintMode(), fnevTapePrintingComplete() make
                            modifications for mode ajout
	01/08/2007	Vincent Yi		Fixed fraca 112591 
								Modified functions fnevTapePrintingComplete() 
								fnevMidnightComeInPrintingMail(),
								fnevMidnightComeInPrintingTape()  
    12/15/2006  Raymond Shen    Implemented ZWZP feature: 
                                Added fnevCheckScreenReentry(), fnSetScreenReentryCondition(),
                                fnClearScreenReentryCondition(), fnGetScreenReentryCondition();
                                Modified fnOITSetPrintMode().
                                
	12/14/2006  Oscar Wang      Modified function fnOITSetPrintMode() for KIP 
	                            class
	12/13/2006	Vincent Yi		Added/modified functions for printing paused when
								midnight crossing								 
	12/06/2006	Vincent Yi		Fixed fraca 108507, modified functions
								fnevTapePrintingComplete()
								fnhkStopPrintingTape()
	12/04/2006	Vincent Yi		Fixing fraca 108507, 
								added two functions in screen WrnTapePrintingPaused.
								fnhkContinueToPrintTape()
								fnhkPauseToStopPrintingTape()
								modified funtion fnhkStopPrintingTape()
    11/17/2006  Adam Liu       Fix fraca GMSE00106326 by changing the 
                               "printing..." animator to use screen ticks.
                               Modified fnfIndiciaPrintingStatus().    
    11/16/2006  Oscar Wang      Fixed FRACA GMSE00107891, Modified function 
                                fnhkExitTextAdDateTime and 
                                fnhkSwitchModeTextAdDateTime to save and restore
                                bob duck setting.
    09/08/2006	Vincent Yi		Added function fnevMidnightComingInPrinting()
    09/04/2006  Oscar Wang      Modified function fnOITSetRightMargin to fix 
                                FRACA GMSE00103523
    09/04/2006  Kan Jiang       Modified function fnhkExitTextAdDateTime for 
                                Fraca 103424

    08/30/2006  Oscar Wang      Updated function fnhkModeSetDefault for Auto
                                Scale OFF
    08/22/2006	Vincent Yi		Added function fnGetResumingPrintEvent()
    08/21/2006	Vincent Yi		Added code into fnOITSetPrintMode() to initialize 
                                variables for manual weight	mode
    08/17/2006  Oscar Wang      Modified function fnOITSetPrintMode() to fix 
                                FRACA GMSE00102554.
    08/10/2006  Oscar Wang      Modified function fnhkModeSetDefault to fix 
                                manual weight problem.
    08/02/2006  Raymond Shen    To use the new EMD parameter PBP_RIGHT_MARGIN_ADJUST_RANGE
                                to do "Right Margin Adjust", modified fnfPrintPositionRange(), 
                                fnOITSetRightMargin(), fnhkValidatePrintPosition(),
                                and added fnIsRightMarginAdjustable().
    07/31/2006  Dicky Sun       Update function fnhkCheckTapeConfig for GTS tape printing.
    07/31/2006  Oscar Wang      Modified function fnhkCheckTapeConfig for DWM 
                                tape printing.
    07/24/2006	Vincent Yi		Added function fnSetResumingPrintFlow(), modified
                                fnStartToPrintMail(), fnStartToPrintTape(), 
                                fnhkResumePrintingEnvelopeOut(), fnevContinueToPrintMail(),
                                fnevContinueToPrint() for resuming print flow
    07/17/2006  Clarisa Bellamy - In fnhkExitTextAdDateTime(), add call to 
                                fnSYSAllowRepeatOIEvent so that bobtask will 
                                get the Gen Static Image request, even though 
                                we never left the print ready state.  This allows
                                the first print run, after we return to normal 
                                indicia print mode, to print correctly.
    07/12/2006  Oscar Wang      Added Diff Weighing support for tape printing.
    05/12/2006  Steve Terebesi  Updated mode variables
    05/11/2006  Steve Terebesi  Replaced calls to fnGetScreenModeById with
                                fnGetScreenEventById
    05/10/2006  Steve Terebesi  Merged from rearchitecture branch		
                                Added calls to fnGetSysMode instead of to OIT                                
    02/15/2006	Vincent Yi		Initial version
                                        
//-----------------------------------------------------------------------------
    OLD PVCS REVISION HISTORY
 * \main\jnus11.00_i1_shelton\17	cx17598	18 oct 2005
 *
 *	Added a new hard key function fnhkModeSetManual which will set the print mode to
 *	key in postage mode in deed.
 *

*************************************************************************/


/**********************************************************************
        Include Header Files
**********************************************************************/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bob.h"
#include "bobutils.h"
#include "cm.h"
#include "cmos.h"
#include "cmpublic.h"
#include "cwrapper.h"
#include "datdict.h"
#include "fdrcm.h"
#include "fwrapper.h"
#include "oicmenucontrol.h"
#include "oicommon.h"
//#include "oideptaccount.h"
#include "oientry.h"
#include "oierrhnd.h"
#include "oifields.h"
//#include "oigts.h"
//#include "oifpimage.h"
#include "oifpmain.h"
#include "oifpprint.h"
#include "oipostag.h"
#include "oiscreen.h"
#include "oit.h"
#include "oitcm.h"
#include "unistring.h"
//#include "oiweight.h"
#include "nucleus.h"
#include "pbplatform.h"
//#include "rateutils.h"
#include "sys.h"        // fnSYSAllowRepeatOIEvent()
#include "sysdata.h"
#include "syspriv.h"
//#include "oirateservice.h"
#include "ajout.h"//for ajout api
//#include "Deptaccount.h"
#include "features.h"
#include "flashutil.h"
#include "utils.h"

/**********************************************************************
        Local Function Prototypes
**********************************************************************/
static void fnSendStartMailRequest (UINT8	ucPages);
static void fnSendStartTapeRequest (UINT8	ucTapes);
void fnSendStopMailRequest (void);
static void fnSendStopTapeRequest (void);

static void fnProcessMailPieceCompleted (void);
static T_SCREEN_ID fnProcessMailJobCompleted(void);

/**********************************************************************
        Local Defines, Typedefs, and Variables
**********************************************************************/

#define DEF_TAPE_NUMBER					1

#define SIZE_TAPE_COUNTER				10

// Status during Tape Print
#define TAPE_PRINTING					0
#define TAPE_PRINT_COMPLETED			1

#define PRINT_POS_RANGE_STR_LEN			25

static UINT8	ucPrintMediaType = NO_MEDIA;

static UINT8	ucTotalPagesToPrint = 1;// Total number of the pages to print
static UINT8    ucPagesToPrint = 0;
static UINT8	ucPrintPageSeqNo = 1;	// Sequence number of page to print

static BOOL     fPiecesNumAppointed = FALSE; // F: No appointed piece number, run until empty.
                                             // T: the input ucTotalPieces is appointed as processed
                                             //	   piece count, e.g, print multi-page report
// Most of time, ucTotalTapesToPrint and ucTapesToPrint have the same value,
// but when Tape out occurs and continue to print, ucTapesToPrint will be the 
// value of the tapes remained
static UINT8	ucTotalTapesToPrint = 1;// Total number of the tapes to print
static UINT8	ucTapesToPrint = 1; 	
static UINT8	ucPrintTapeSeqNo = 1;	// Sequence number of the tape to print

static UINT8	bTapePrintStatus = TAPE_PRINTING;// status during Tape Print
static UINT8	ucTapeShortTimerCount;		

//static UINT16   usTmpRightMarginPos = PO_RIGHT_MARGIN_OFFS_DEF;   //Right Margin: 4mm Right Margin Offset: 5mm
//static UINT16   usOldRightMarginPos = PO_RIGHT_MARGIN_OFFS_DEF;   //store the old value after successfully set new value
static UINT16   usTmpRightMarginPos = 0;   //Right Margin: 4mm Right Margin Offset: 0mm
static UINT16   usOldRightMarginPos = 0;   //store the old value after successfully set new value
static BOOL		fMarginPosInit = FALSE;

static BOOL		fResumePrint = FALSE;

static BOOL		fFeederPaperAdded = FALSE;

BOOL		fMultiPagePrintingStopped = FALSE;

static UINT8	cMultiTapePrintingStopped = MULTI_TAPE_NOT_STOPPED;

static UINT8	ucResumePrintEvent = OIT_PRINTING;
static UINT16         usDuckBits = 0;
static UINT8	bAlternMode;
// used to check if mail piece is printed under DWM mode of DM400
static BOOL     fDWPiecePrinted = FALSE;
static UINT8    bHybridRestoreMode = PMODE_MANUAL;
static UINT8    bCurrentPrintState = PSTATE_PREPARING_TO_PRINT;

static BOOL			fMultiTapeOK =  FALSE;

/**********************************************************************
        Global Functions Prototypes
**********************************************************************/

extern void fnClearDateCorrection(void);

/**********************************************************************
        Global Variables
**********************************************************************/
extern unsigned char bDiffWeigh1stClassSelection;
extern KIPPWInfo*                  pKIPPasswordInfo;
extern BOOL bFromRemoveItem;
extern double dbTempManifestPostage;

extern BOOL fForceReRate;
extern BOOL fBarcodeNoneReset;
extern ulong CMOSTrmUploadDueReqd;
BOOL		 			fValidValue = TRUE;   

BOOL					fDiffWeighPieceNeeded = FALSE;	/* used to trigger the 'remove one mailpiece' prompt */
BOOL					fDiffWModePending = FALSE;		/* differential weighing mode selected but we are waiting
                                                            for valid weight available on scale before changing modes */
UINT8 	bCurrentPrintMode    = PMODE_SEAL_ONLY;
UINT8     bPreviousNonSealMode = PMODE_MANUAL;   /* used to keep track of the last mode used before                                                                  seal only so that a toggle can be implemented */    
BOOL      fHybridMode = FALSE;
extern BOOL fNeedStartMailRun;	
extern BOOL fUpdateSelectedClassByModeChange;
extern BOOL bGoToReady;
extern BOOL fSkipPaperInTranport;

void fnChangeScreen(unsigned short wScreenNumber, BOOL fCallPreFn, BOOL fCallPostFn);

/********************************************************************
        Public Functions
**********************************************************************/
/* Utility functions */

/* *************************************************************************
// FUNCTION NAME: 
//		fnStartToPrintMail
//
// DESCRIPTION: 
// 		Utility function that handles the Start key pressing in ready mode
//		to run mail on DM400C 
//
// PRE-CONDITIONS:
//		The base type has been checked before calling this function, only for
//		DM400C
//
// INPUTS:
//		ucTotalPieces	- total piece count
//		bNumNext		- number of next screens
//		pNextScreens	- pointer to the next screen list
//		fInit			- TRUE, initialize the value of ucTotalPagesToPrint
//								and ucPrintPageSeqNo
//		fAppointedPieces- TRUE, the input ucTotalPieces is appointed as processed
//								piece count
//						  FALSE, no appointed piece number, run until empty.
//
// RETURNS:
//		Next screen:
//		if in Ready mode, no screen change
//		if in not Ready mode, to ready transient screen.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/24/2010  Raymond Shen    For report printing, on condition that any error is caught,
//                              do not change the next screen id. Thus fixed the problem
//                              that report can't be printed out if there is any GTS error.
//  12/14/2007  Raymond Shen    Clear mail peice completed flag for further use.
//  11/06/2007  Vincent Yi      Added code to set up abacus static info before running
//  09/19/2007  Oscar Wang      Check GTS error conditions before starting printing.
//  09/18/2007  Oscar Wang      For report printing, don't care GTS_NO_TRACKING_IDS 
//                              error.
// 	07/24/2006	Vincent Yi		Renamed fPrintReports to fAppointedPieces for more
//								general uses
//								Added ucResumePrintEvent to make sure the previous 
//								print flow can be resumed correctly
//	07/11/2006	Derek DeGennaro Updated to support GTS/DWM/Single Piece Mode.
//								Piece count sent to CM for normal mail runs
//								is now zero.  (Zero indicates run until empty.)
//								Non-zero counts indicate the actual number of
//								pieces to process.
//  03/15/2006  Vincent Yi      Initial version
// *************************************************************************/
T_SCREEN_ID fnStartToPrintMail(UINT8			ucTotalPieces,
                               UINT8			bNumNext, 
                               T_SCREEN_ID *	pNextScreens,
                               BOOL				fInit,
                               BOOL             fAppointedPieces)
{
    T_SCREEN_ID	usNext = 0;
    UINT8		ucMode = 0;
    UINT8		ucSubMode = 0;
    UINT8       ucEvent;

    fnSetMailPieceCompleted(FALSE);
    
    // Set up abacus static mailrun information
//    if (fnOiAbSetupStaticMailRunInfo() == FALSE)
//        {   // If not success, return home screen to display error message
//        return (GetScreen (SCREEN_MM_INDICIA_READY));
//    }
    
/*	if (fAppointedPieces == TRUE)
    {	// The input ucTotalPieces is appointed as processed piece count
        ucPagesToPrint = ucTotalPieces;
    }
    else
    {	// no appointed piece number, run until empty.
        ucPagesToPrint = 0;
    }
*/
    // If fAppointedPieces is FALSE, the input ucTotalPiece should be 0
    ucPagesToPrint = ucTotalPieces;
    if (fInit == TRUE)
    {
        ucTotalPagesToPrint = ucTotalPieces;
        ucPrintPageSeqNo = 1;	
        fPiecesNumAppointed = fAppointedPieces;
    }

    fnGetSysMode(&ucMode, &ucSubMode);

    if (ucMode == SYS_PRINT_READY)
    {
        fnSendStartMailRequest(ucPagesToPrint);
    }
    else if (ucMode == SYS_PRINT_NOT_READY)
    {	// In a not-ready screen, when "Start" pressed on DM400C, OIT should 
        // first change to ready mode , then send Start Mail request to SYS to 
        // change to printing mode.
        // "MM...ReadyTransient" is a transient screen with ready mode, in which
        // the request will be send to SYS. 

        fnGetScreenEventById(fnGetPreviousScreenID(), &ucEvent);

        if (ucEvent == OIT_LEFT_PRINT_RDY 		|| 
            ucEvent == OIT_ERROR_METER			||
            ucEvent == OIT_ENTER_POWERUP		||
            ucEvent == OIT_GOTO_SLEEP			||
            ucEvent == OIT_ENTER_POWERDOWN		||
            ucEvent == OIT_ENTER_DIAG_MODE)
        {
            ucEvent = ucResumePrintEvent;
        }

        // Clean last print-resume event
        ucResumePrintEvent = 0xFF;

        switch (ucEvent)
        {
//		case OIT_INDICIA:
        case OIT_ENTER_PRINT_RDY:
        case OIT_PRINTING:
            usNext = GetScreen (SCREEN_INDICIA_READY_TRANS);
            break;

//		case OIT_PERMIT:
        case OIT_PRINT_PERMIT:
        case OIT_RUNNING_PERMIT:
            usNext = GetScreen (SCREEN_PERMIT_READY_TRANS);
            break;

//		case OIT_REPORT:
        case OIT_PREP_FOR_RPT_PRINTING:
        case OIT_RUNNING_REPORT:
            usNext = GetScreen (SCREEN_REPORT_READY_TRANS);
            break;

//		case OIT_TEST_PATTERN:
        case OIT_PRINT_ENV_TEST:
        case OIT_RUNNING_TEST:
            usNext = GetScreen (SCREEN_TEST_PATTERN_READY_TRANS);
            break;

//		case OIT_SEAL_ONLY:
        case OIT_PRINT_NO_INDICIA:
        case OIT_RUNNING_SEAL:
            usNext = GetScreen (SCREEN_SEAL_ONLY_READY_TRANS);
            break;
    
        default:
            usNext = GetScreen (SCREEN_INDICIA_READY_TRANS);
            break;
        }	
        ucPrintMediaType = MEDIA_MAIL;
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnStartToPrintTape
//
// DESCRIPTION: 
// 		Utility function that handles the Tape key pressing in ready mode
//		to run tape.
// 
// INPUTS:
//		ucTotalTapes	- total tape number
//		bNumNext		- number of next screens
//		pNextScreens	- pointer to the next screen list
//		fInit			- TRUE, initialize the value of ucTotalTapesToPrint
//								and ucPrintTapeSeqNo	
//
// RETURNS:
//		Next screen:
//		if in Ready mode, no screen change
//		if in not Ready mode, to ready transient screen.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/24/2010  Raymond Shen    For report printing, on condition that any error is caught,
//                              do not change the next screen id. Thus fixed the problem
//                              that report can't be printed out if there is any GTS error.
//  12/14/2007  Raymond Shen    Clear mail peice completed flag for further use.
//  11/06/2007  Vincent Yi      Added code to set up abacus static info before running
//  09/19/2007  Oscar Wang      Check GTS error conditions before starting printing.
//  09/18/2007  Oscar Wang      For report printing, don't care GTS_NO_TRACKING_IDS
//                              error.
// 	07/24/2006	Vincent Yi		Added ucResumePrintEvent to make sure the previous 
//								print flow can be resumed correctly
//	03/07/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnStartToPrintTape(UINT8			ucTotalTapes,
                               UINT8			bNumNext, 
                               T_SCREEN_ID *	pNextScreens,
                               BOOL				fInit)
{
    T_SCREEN_ID	usNext = 0;
    UINT8		ucMode = 0;
    UINT8		ucSubMode = 0;
    UINT8       ucEvent;

    fnSetMailPieceCompleted(FALSE);

    // for report printing, don't need to Set up abacus static mailrun information.
/*
    if((fnIsInReportMode() == TRUE)
        || (fnGetResumingPrintEvent() == OIT_PRINT_RPT_TO_TAPE))
    {
        usNext = 0;
    }
    else
    {
        // Set up abacus static mailrun information
//        if (fnOiAbSetupStaticMailRunInfo() == FALSE)
//        {   // If not success, return home screen to display error message
//            return (GetScreen (SCREEN_MM_INDICIA_READY));
//        }
    }
*/

    ucTapesToPrint = ucTotalTapes;
    if (fInit == TRUE)
    {
        ucTotalTapesToPrint = ucTotalTapes;
        ucPrintTapeSeqNo = 1;	
    }

    fnGetSysMode(&ucMode, &ucSubMode);

    if (ucMode == SYS_PRINT_READY)
    {
        fnSendStartTapeRequest(ucTotalTapes);
    }
    else if (ucMode == SYS_PRINT_NOT_READY)
    {	// In a not-ready screen, when "Tape" pressed, OIT should first 
        // change to ready mode , then send Start Mail request to SYS to 
        // change to printing mode.
        // "MM...ReadyTransient" is a transient screen with ready mode, in which
        // the request will be send to SYS. 

        fnGetScreenEventById(fnGetPreviousScreenID(), &ucEvent);

        if (ucEvent == OIT_LEFT_PRINT_RDY 		|| 
            ucEvent == OIT_ERROR_METER			||
            ucEvent == OIT_ENTER_POWERUP		||
            ucEvent == OIT_GOTO_SLEEP			||
            ucEvent == OIT_ENTER_POWERDOWN		||
            ucEvent == OIT_ENTER_DIAG_MODE)
        {
            ucEvent = ucResumePrintEvent;
        }

        // Clean last print-resume event
        ucResumePrintEvent = 0xFF;

        switch (ucEvent)
        {
//		case OIT_INDICIA:
        case OIT_ENTER_PRINT_RDY:
        case OIT_PRINT_TAPE:
            usNext = GetScreen (SCREEN_INDICIA_READY_TRANS);
            break;

//		case OIT_PERMIT:
        case OIT_PRINT_PERMIT:
        case OIT_PRINT_TAPE_PERMIT:
            usNext = GetScreen (SCREEN_PERMIT_READY_TRANS);
            break;

//		case OIT_REPORT:
        case OIT_PREP_FOR_RPT_PRINTING:
        case OIT_PRINT_RPT_TO_TAPE:
            usNext = GetScreen (SCREEN_REPORT_READY_TRANS);
            break;

//		case OIT_TEST_PATTERN:
        case OIT_PRINT_ENV_TEST:
        case OIT_PRINT_TAPE_TEST:
            usNext = GetScreen (SCREEN_TEST_PATTERN_READY_TRANS);
            break;

//		case OIT_SEAL_ONLY:
//			usNext = GetScreen (SCREEN_SEAL_ONLY_READY_TRANS);
//			break;
    
        default:
            usNext = GetScreen (SCREEN_INDICIA_READY_TRANS);
            break;
        }	
        ucPrintMediaType = MEDIA_TAPE;
    }
    
    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnOITGetPrintMode
//
// DESCRIPTION: 
// 		Returns the current comet print mode (i.e. manual, wow, wow 1st
//		piece, etc).  There are constants in oit.h that enumerate the choices.
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Current Print Mode.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 				Joe Mozdzer, Victor Li	Initial version
//				Vincent Yi		Rename it from fnOITGetJanusPrintMode to 
//								fnOITGetPrintMode, and move it from oijmain 
//								here.
// 
// *************************************************************************/
UINT8 fnOITGetPrintMode(void)
{
    return (bCurrentPrintMode);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnOITSetPrintMode
//
// DESCRIPTION: 
// 		Sets the print mode.
//
// INPUTS:
//		bPrintMode - Print Mode to set to.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 09/17/2012 	Bob Li			Make sure the MWE mode can't be displayed if the feature is disabled.
// 06/25/2009   Jingwei,Li      Save master context info if change mode from WOW/W1P to Platform
//                              mode by weight update.
// 05/05/2009   Jingwei,Li      Save master context info inf need.
// 11/19/2008   Jingwei,Li      Add WOW/W1st piece logic.
// 18 Oct 2007  D.Kohl          Test swiss case in case on PMODE_MANUAL_
//  08/21/2007  Adam Liu        Allow zero postage KIP mode on Canadian large meter, fix fraca GMSE00127939.
//  06/27/2007  Raymond Shen    Don't select KIP mode on Canadian large meter.
// 1 May 2007   Simon Fox       Don't select KIP mode by default if KIP is
//                              limited to 0 and password protected.
// 12/14/2006   Oscar Wang      Modified KIP class work flow.
// 08/21/2006	Vincen Yi		Added code to initialize variables when switch 
//								from manual weight mode.
// 08/17/2006	Oscar Wang      Fix FRACA GMSE00102554.
//				Vincent Yi		Rename it from fnOITSetJanusPrintMode to 
//								fnOITSetPrintMode, and move it from oijmain 
//								here.
// 				Mark D. Zamary, Victor Li	Initial version
// 
// *************************************************************************/
void fnOITSetPrintMode(UINT8 bPrintMode)
{
    BOOL fSetNonRatingContext = FALSE;
    UINT8 bKIPType, bKIPCarType;
    BOOL bNeedSaveMasterContext=  FALSE;   
	BOOL		fEnabled = FALSE;
	unsigned char	bFeatureState;
	unsigned char	bFeatureIndex;


	(void)IsFeatureEnabled(&fEnabled, MANUAL_WEIGHT_ENTRY_SUPPORT, &bFeatureState, &bFeatureIndex);
    
    /* save the previous mode if needed */
    //if (bCurrentPrintMode != PMODE_SEAL_ONLY)
    //	bPreviousNonSealMode = bCurrentPrintMode;
    bKIPType = fnFlashGetByteParm(BP_KEY_IN_POSTAGE_MODE) & ~KIP_USE_MDS_TOKENS_MASK;

    /* do not allow to set to key in postage mode if ... */
    if (bPrintMode == PMODE_MANUAL &&
        /* KIP is not allowed */
        (bKIPType == KIP_NOT_ALLOWED || 
        /* Or KIP requires a password and the user hasn't just entered the correct password */
         (bKIPType == KIP_ZERO_ONLY_W_PASSWORD &&
          (pKIPPasswordInfo->ucPasswordSelectPurpose != KIPW_POSTAGE_ENTER_REQUIRED || 
           pKIPPasswordInfo->ucPasswordStatus != KIP_PW_COMPLETE)) ))
    {
        if ( fnPlatIsAttached() == TRUE )
        {
            // use platform mode if a platform is presented
            bPrintMode = PMODE_PLATFORM;
        } 
        else if(fEnabled == TRUE)
        {
            // we use manual weight entry mode instead
            bPrintMode = PMODE_MANUAL_WGT_ENTRY;
        }
        else
        {
            bPrintMode = PMODE_SEAL_ONLY;
        }
    }

    // do not allow to set to platform mode if no scale is attached.
    if ((bPrintMode == PMODE_PLATFORM) 	&& 
        (fnPlatIsAttached() == FALSE))
    {
        //if MWE feature is enabled.
        if(fEnabled == TRUE)
        {
            // we use manual weight entry mode instead
            bPrintMode = PMODE_MANUAL_WGT_ENTRY; 
        }
        else
        {
            bPrintMode = PMODE_SEAL_ONLY;
        }
    }

    if (bPrintMode == PMODE_MANUAL_WGT_ENTRY && !fEnabled)
    {
        bPrintMode = PMODE_SEAL_ONLY;
    }
        
    /* print mode change will happen */
    if (bCurrentPrintMode != bPrintMode)
    {
        // adjust weighing mode
        if ( bCurrentPrintMode == PMODE_DIFFERENTIAL_WEIGH )
        {
            // make sure that we set the platform mode to normal when
            // quit the diff weighing mode.
//            fnSetPlatMode( PLAT_MODE_NORMAL);
			
            // force the platform to send a weight update to the OIT
            fnPlatForceUpdate();

            // Clear this flag here what ever the new mode will be.
//            bDiffWeigh1stClassSelection = FALSE;
        }
        else if ( bPrintMode == PMODE_DIFFERENTIAL_WEIGH )
        {
//            fnSetPlatMode( PLAT_MODE_DIFFERENTIAL );
        }

        // Clear the KIP Indicia information if needed.
        if((fnFlashGetByteParm(BP_KEY_IN_POSTAGE_MODE) & ~KIP_USE_MDS_TOKENS_MASK) == KIP_ALLOWED_HRT_STRINGS)
        {
//            (void) fnRateClearKIPIndiciaInfo();
        }
        
        OSSendIntertask (OIT, OIT, OIT_PRINT_MODE_CHANGE, NO_DATA, NULL, 0);
    }

    /*if the mode change from PMODE_MANUAL_WGT_ENTRY / PMODE_AJOUT_MANUAL_WGT_ENTRY to any other mode,
    clear the manual weight and the manual weight display flag. added by Kemble*/
    if( ((fnOITGetPrintMode() == PMODE_MANUAL_WGT_ENTRY ) &&
        (bPrintMode != PMODE_MANUAL_WGT_ENTRY)) ||
        ((fnOITGetPrintMode() == PMODE_AJOUT_MANUAL_WGT_ENTRY ) &&
        (bPrintMode != PMODE_AJOUT_MANUAL_WGT_ENTRY)))
    {
//        fnEnableDisplayManualWeight (FALSE);
		
		//Fix FRACA#229109 , back up manual weight before clear it.
//		fnBackupManualWeight();
//        fnClearManualWeight();
    }

    /* force a rerate for WOW/W1P */
    if ((bPrintMode == PMODE_WOW) || (bPrintMode == PMODE_WEIGH_1ST) )
    {
         fForceReRate = TRUE;
    }
    if(((bCurrentPrintMode == PMODE_WOW)||(bCurrentPrintMode == PMODE_WEIGH_1ST))&&
       ((bPrintMode == PMODE_MANUAL)||(bPrintMode == PMODE_PLATFORM)))
    {
        bNeedSaveMasterContext = TRUE;
    }

    /* set the mode global */
    bCurrentPrintMode = bPrintMode;
    
    //fDiffWModePending = FALSE;

    switch( bPrintMode )
    {
        case PMODE_POSTAGE_CORRECTION:
        case PMODE_DATE_CORRECTION:
        case PMODE_MANIFEST:
            fSetNonRatingContext = TRUE;
            break;
        
        case PMODE_MANUAL:
            // get KIP Carrier type
            bKIPCarType = fnFlashGetByteParm(BP_KEY_IN_POSTAGE_CLASS);

            // KIP Carrier ?    
            // If Swiss meter (test by "KIP_FROM_FEE_LIST_ONLY"),
            // and KIP CLASS alowedreturn false.
 /*           if ( (    (bKIPCarType != KIP_CLASS_NOT_ALLOWED)
		           && (fnRateIsCurrentCarrierBranded()== RSSTS_CARRIER_KIP_CARRIER))
              || (    (bKIPCarType != KIP_CLASS_NOT_ALLOWED)
                   && (bKIPType == KIP_FROM_FEE_LIST_ONLY) ) )
            {
                fSetNonRatingContext = FALSE;
            }
            else
            {
                fSetNonRatingContext = TRUE;
            }
*/            break;

        default:
            fSetNonRatingContext = FALSE;
            break;
    }
    
    if(bNeedSaveMasterContext == TRUE)
    {
//        fnSaveMasterContexForRevertWOW();
    }

    if( fSetNonRatingContext )
    {
//        (void)fnRateSetNonRateMode();
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnOITGetJanusPrintOption
//
// DESCRIPTION: 
// 		Wrapper for fnOITGetPrintMode
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Current Print Mode.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 				???				Initial version
//				Vincent Yi		Move it from oijmain to here
//
// *************************************************************************/
UINT8 fnOITGetJanusPrintOption(void)
{
    return (fnOITGetPrintMode());
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnModeSetManualWgtEntry
//
// DESCRIPTION: 
//      Utility function that sets the print mode global to manual weight entry.
//
// INPUTS:
//      None
//
// OUTPUTS:
//      None
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Tim Zhang       Initial version
//  11/10/2005  Dicky Sun       Code cleanup
//
// *************************************************************************/
void fnModeSetManualWgtEntry(void)
{
    fnOITSetPrintMode( PMODE_MANUAL_WGT_ENTRY );
    
 // To fix fraca 14890 and 14941. ZWZP Disabling Condition should be FALSE in manual weight entry mode. -David  
//    fnClearZWDisablingCondition();
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnOITGetCometPrintMode
//
// DESCRIPTION: 
//      Returns the current comet print mode (i.e. manual, wow, wow 1st
//      piece, etc).  There are constants in oit.h that enumerate the choices.
//
// INPUTS:
//      None
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Current Comet print mode
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Joe Mozdzer     Initial version
//  11/10/2005  Dicky Sun       Code cleanup
//
// *************************************************************************/

UINT8 fnOITGetCometPrintMode(void)
{
    return ( bCurrentPrintMode );
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnModeSetDefault
//
// DESCRIPTION: 
//      Set the current print mode to a default mode that the system
//      allows. It is key in postage mode or a weighing mode.
//
// INPUTS:
//      None
//
// OUTPUTS:
//      None
//
// RETURNS:
//      TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Victor Li       Initial version
//  11/10/2005  Dicky Sun       Code cleanup
//
// *************************************************************************/

BOOL fnModeSetDefault(void)
{
    // set to the key in postage mode if key in postage is allowed and with no any restriction only */
    if( ((fnFlashGetByteParm(BP_KEY_IN_POSTAGE_MODE) & ~KIP_USE_MDS_TOKENS_MASK) == KIP_ALLOWED) ||
            ((fnFlashGetByteParm(BP_KEY_IN_POSTAGE_MODE) & ~KIP_USE_MDS_TOKENS_MASK) == KIP_ALLOWED_HRT_STRINGS) )
    {
        fnOITSetPrintMode(PMODE_MANUAL);
    }
    else if (fnOITGetPrintMode() != PMODE_PLATFORM)
    {
        fnOITSetPrintMode(PMODE_PLATFORM);
    }

    // unselect the class
    //fnSetRatingStatus( RATE_INITIAL_STATUS );

//    (void)fnClearPostageAndAllRateInfo();

    return (TRUE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnTapePrintingInit
//
// DESCRIPTION: 
// 		Utility function to initialize all variables related to tape printing.
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		bTapePrintStatus, ucPrintTapeSeqNo, ucTapeShortTimerCount have been 
//		initialized
//
// RETURNS:
//		None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	11/30/2005	Vincent Yi		Initial version
//
// *************************************************************************/
void fnTapePrintingInit(void)
{
    bTapePrintStatus = TAPE_PRINTING;
//	ucPrintTapeSeqNo = 1;
    ucTapeShortTimerCount = 0;
	cMultiTapePrintingStopped = MULTI_TAPE_NOT_STOPPED;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnGetMaximumTapeNumber
//
// DESCRIPTION: 
// 		Utility function to retrieve the maximum of tape number configured in EMD
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		The maximum of tape number.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	12/1/2005	Vincent Yi		Initial version
//
// *************************************************************************/
UINT16 fnGetMaximumTapeNumber (void)
{
    return (fnFlashGetByteParm(BP_MAX_TAPES_TO_PRINT));
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnTapePrintCompleteToReady
//
// DESCRIPTION: 
// 		Utility function to check whether screen can transition from Tape 
//		printing screen to Home ready screen, this guarantee there are 0.5 sec 
//		between "Complete!" and "Home Ready" 
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		SUCCESSFUL or FAILURE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	12/1/2005	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnTapePrintCompleteToReady (void)
{
    BOOL fRet = FAILURE;

    if (bTapePrintStatus == TAPE_PRINT_COMPLETED)
    {
        ucTapeShortTimerCount++;
        if (ucTapeShortTimerCount >= 5)
        {
            fRet = SUCCESSFUL;	
        }
        OSStartTimer(SHORT_TICK_TIMER);
    }
    if (bTapePrintStatus == TAPE_PRINTING)
    {
        ucTapeShortTimerCount = 0;
    }

    return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnOITSetRightMargin
//
// DESCRIPTION: 
// 		Utility function that sets the print right margin.
//
// INPUTS:
//		ucTapeNum.
//
// OUTPUTS:
//		wMarginPosition - the absolute value for print right margin .
//
// RETURNS:
//		SUCCESSFUL or FAILURE.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  09/04/2006  Oscar Wang      Fix FRACA GMSE00103523.
//  08/02/2006  Raymond Shen    Right Margin parameters are retrieved from EMD
//	2/07/2006	Vincent Yi		Code cleanup
// 				Henry Liu		Initial version
//
// *************************************************************************/
BOOL fnOITSetRightMargin(UINT16 usMarginPosition)
{

    UINT8 	pMsgData[4];
    ST_PO_RIGHT_MARGIN       *pRightMargin = 0;

    pRightMargin = (ST_PO_RIGHT_MARGIN *) fnFlashGetPackedByteParm(PBP_RIGHT_MARGIN_ADJUST_RANGE);

    //for UK: max and min range are 0, do not allow adjustment.
    if( (pRightMargin->maxRange == 0) &&
       (pRightMargin->minRange == 0) )
    {
        usMarginPosition = pRightMargin->defaultValue;
    }
    else if ( (usMarginPosition > pRightMargin->maxRange) || 
        (usMarginPosition < pRightMargin->minRange) )
    {
        return(FAILURE);
    }
    
    pMsgData[0] = (UINT8) PO_RIGHT_MARGIN; //right margin
    pMsgData[1] = (UINT8) usMarginPosition - PO_RIGHT_MARGIN;	//right margin offset
    pMsgData[2] = (UINT8) PO_TOP_MARGIN; //top margin
    pMsgData[3] = (UINT8) PO_TOP_MARGIN_OFFS_DEF;

    /* request control manager status */
    if( OSSendIntertask(CM, OIT, OC_SET_MARGIN, BYTE_DATA, pMsgData, sizeof(pMsgData)) 
                != SUCCESSFUL)
    {
        return(FAILURE);
    }

    /* Set the value to CMOS */
    fnCmosSetRightMargin(usMarginPosition);

    return(SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnSetResumePrintFlag
//
// DESCRIPTION: 
// 		Sets or clears the resume print flag.
//
// INPUTS:
//		fResume - TRUE or FALSE.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	03/17/2006	Vincent Yi		Initial version.
// 
// *************************************************************************/
void fnSetResumePrintFlag(BOOL fResume)
{
    fResumePrint = fResume;	
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnGetResumePrintFlag
//
// DESCRIPTION: 
// 		Gets value of the resume print flag.
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	03/17/2006	Vincent Yi		Initial version.
// 
// *************************************************************************/
BOOL fnGetResumePrintFlag (void)
{
    return fResumePrint;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnUpdatePrintPageSequenceNo
//
// DESCRIPTION: 
// 		Utility function that updates the value of ucPrintPageSeqNo.
//
// INPUTS:
//		ucPageNo
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		None.
//
// WARNINGS/NOTES:
//		None.
//
// MODIFICATION HISTORY:
//	04/04/2006	Vincent Yi		Initial version.
// 
// *************************************************************************/
void fnUpdatePrintPageSequenceNo (UINT8 ucPageNo)
{
    ucPrintPageSeqNo = ucPageNo;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnUpdatePrintTapeSequenceNo
//
// DESCRIPTION: 
// 		Utility function that updates the value of ucPrintTapeSeqNo.
//
// INPUTS:
//		ucTapeNo
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		None.
//
// WARNINGS/NOTES:
//		None.
//
// MODIFICATION HISTORY:
//	04/04/2006	Vincent Yi		Initial version.
// 
// *************************************************************************/
void fnUpdatePrintTapeSequenceNo (UINT8 ucTapeNo)
{
    ucPrintTapeSeqNo = ucTapeNo;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnFeederPaperAdded
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		fAdded	- TRUE or FALSE
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		None.
//
// WARNINGS/NOTES:
//		None.
//
// MODIFICATION HISTORY:
//	05/12/2006	Vincent Yi		Initial version.
// 
// *************************************************************************/
void fnFeederPaperAdded (BOOL fAdded)
{
    fFeederPaperAdded = fAdded;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnTapeBufferValid
//
// DESCRIPTION: 
// 		Utility function that checks if the tape count found in	the current \
//		entry buffer is a legal one.
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Joe Mozdzer		Initial version
// 	05/30/2006	Vincent Yi		Adapted for FP
//
// *************************************************************************/
BOOL fnTapeBufferValid(void)
{
    UNICHAR 		*pEntry;
    UNICHAR			uniDecimalChar;

    BOOL 			fStatus;
    BOOL			fRetval = FALSE;
    UINT16			usMaxTape = fnGetMaximumTapeNumber();
    UINT32			ulTapeCount;
    
    fnEntryBufferSnapshotCapture();
    /* if there is a decimal point in the string, it is automatically considered an invalid # of tapes */
    uniDecimalChar = fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR);
    pEntry = fnGetCurrentEntryBuffer()->pBuf;
    while ((*pEntry != (UNICHAR) NULL) && ((*pEntry) != uniDecimalChar))
    {
        pEntry++;
    }
    if (*pEntry == uniDecimalChar)
    {
        return FALSE;
    }

    /* since there is no decimal point, convert the number entered to binary 
        and make sure the amount doesn't exceed the maximum allowable number 
        of tapes */
    fStatus = fnUnicode2Bin(fnGetGenPurposeEbuf()->pBuf, &ulTapeCount, 
                            fnBufferLen());
    if ((fStatus == SUCCESSFUL) && (ulTapeCount > 0) && 
        (ulTapeCount <= usMaxTape))
    {
        fRetval = TRUE;
    }

    return (fRetval);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnSetResumingPrintFlow
//
// DESCRIPTION: 
//      Store current mode for later resuming print flow.
//
// INPUTS:
//      ucEvent - 
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/24/2006  Vincent Yi      Initial version.
// 
// *************************************************************************/
void fnSetResumingPrintFlow(UINT8 ucEvent)
{
    ucResumePrintEvent = ucEvent; 
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetResumingPrintEvent
//
// DESCRIPTION: 
//      Get last printing event for later resuming print flow.
//
//      None.
// :
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      Last event.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/22/2006  Vincent Yi      Initial version.
// 
// *************************************************************************/
UINT8 fnGetResumingPrintEvent(void)
{
    return ucResumePrintEvent; 
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsRightMarginAdjustable
//
// DESCRIPTION: 
//      Utility function that tells whether right margin is adjustable.
//
// INPUTS:
//      None 
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      TRUE    - if is adjustable;
//      FALSE   - if is not adjustable;
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/01/2006  Raymond Shen      Initial version.
// 
// *************************************************************************/
BOOL fnIsRightMarginAdjustable(void)
{
    BOOL    fStatus = TRUE;
    ST_PO_RIGHT_MARGIN       *pRightMargin = 0;

    pRightMargin = (ST_PO_RIGHT_MARGIN *) fnFlashGetPackedByteParm(PBP_RIGHT_MARGIN_ADJUST_RANGE);

    if( (pRightMargin->maxRange ==0) 
      && (pRightMargin->minRange == 0) )
    {
        fStatus = FALSE;
    }

    return (fStatus);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnMultiTapePrintingStopped
//
// DESCRIPTION: 
//		None.
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		value of cMultiTapePrintingStopped
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	12/06/2006	Vincent Yi		Initial version
// 
// *************************************************************************/
UINT8 fnMultiTapePrintingStopped (void)
{
	return cMultiTapePrintingStopped;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnSetMultiTapePrintingStopped
//
// DESCRIPTION: 
//      None.
//
// INPUTS:
//      fStopped - 
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/06/2006  Vincent Yi      Initial version.
// 
// *************************************************************************/
void fnSetMultiTapePrintingStopped (UINT8 cStopped)
{
	cMultiTapePrintingStopped = cStopped;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnIsMultiPagePrintingStopped
//
// DESCRIPTION: 
//		None.
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	12/13/2006	Vincent Yi		Initial version
// 
// *************************************************************************/
BOOL fnIsMultiPagePrintingStopped (void)
{
	return fMultiPagePrintingStopped;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnSetMultiPagePrintingStopped
//
// DESCRIPTION: 
//      None.
//
// INPUTS:
//      fStopped - 
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/13/2006  Vincent Yi      Initial version.
// 
// *************************************************************************/
void fnSetMultiPagePrintingStopped (BOOL fStopped)
{
	fMultiPagePrintingStopped = fStopped;
}


/* *************************************************************************
// FUNCTION NAME: 
//		fnClearFlagDWPiecePrinted
//
// DESCRIPTION: 
//		None.
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  06/21/2007  Vincent Yi  Initial version
// 
// *************************************************************************/
void fnClearFlagDWPiecePrinted (void)
{
    fDWPiecePrinted = FALSE;
}   

/* *************************************************************************
// FUNCTION NAME: 
//		fnIsDWPiecePrinted
//
// DESCRIPTION: 
//		None.
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  06/21/2007  Vincent Yi  Initial version
// 
// *************************************************************************/
BOOL fnIsDWPiecePrinted (void)
{
    return (fnOITGetPrintMode() == PMODE_DIFFERENTIAL_WEIGH &&
            fDWPiecePrinted == TRUE);
}

/* Pre/post functions */


/* *************************************************************************
// FUNCTION NAME: 
//		fnpSetupMultipleTapes
//
// DESCRIPTION: 
// 		Pre function that sets up the menu items for Tape Quantity screen.
//
// INPUTS:
//		Standard screen pre function inputs.
//
// OUTPUTS:
//		*pNextScr = 0
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	12/1/2005	Vincent Yi		Initial version 
//
// *************************************************************************/
SINT8 fnpSetupMultipleTapes( void 		(** pContinuationFcn)(), 
                             UINT32 	  *	pMsgsAwaited,
                             T_SCREEN_ID  *	pNextScr )
{
    UNICHAR	pUniTempString[3] = {0};
    UINT8	ucStrLen;

    *pNextScr = 0;

    // Set entry message to no error
    fnSetEntryErrorCode (ENTRY_NO_ERROR);

    // Set the default value of Tape number to 1
    ucStrLen = fnBin2Unicode (pUniTempString, DEF_TAPE_NUMBER);
    pUniTempString[ucStrLen] = (UNICHAR)NULL; 
    fnStoreDefaultValueString (pUniTempString);

    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpMenuPrintingMode
//
// DESCRIPTION: 
// 		Pre function that sets up the menu items for Printing Mode menu screen. 
//
// INPUTS:
//		Standard screen pre function inputs.
//
// OUTPUTS:
//		*pNextScr = 0
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//		Menu possibilities:
//		1. No Printing - Seal Only
//		2. Text-Ad-Date-Time
//      3. Print Permits
//		
// MODIFICATION HISTORY:
//	12/06/2005	Vincent Yi		Initial version 
//  05/12/2006  Adam Liu        Combined Text-Ad-DateTime screen 
// *************************************************************************/
SINT8 fnpMenuPrintingMode( void 		(** pContinuationFcn)(), 
                           UINT32 	  	*	pMsgsAwaited,
                           T_SCREEN_ID 	*	pNextScr )
{
    UINT32		ulSlotIndex = 0;
    UINT32		ulNextSlotIndex;

    static S_MENU_SETUP_ENTRY   pPrintingModeMenuInitTable[] = 
    {
        // force menu table reset before defining new slots
        {   CLEAR_MENU_TABLE,   (UINT8) 0,  (UINT8) 0,  FALSE,  NULL_PTR },

        // Display number           Key           Label        Active   Conditional 
        // offset,			        assignment,	  string id,   flag,	function
                                    
        /* No Printing - Seal Only */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 1,    (UINT8) 1,   TRUE,    NULL_PTR },
        /* Text-Ad-Date-Time */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 2,    (UINT8) 2,   TRUE,    fnCheckTextAdDateTimePrintingMode },
        /* Print Permits */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 3,   TRUE,    fnCheckPermitPrintingMode },

        {   END_MENU_TABLE_MARKER,  (UINT8) 0,    (UINT8) 0,   FALSE,   NULL_PTR }  
    } ;

    // invoke generic slot initialization function with this menu initializer table
    ulNextSlotIndex = fnSetupMenuSlots( pPrintingModeMenuInitTable, ulSlotIndex, 
                                        (UINT8) 4 ) ;
    
    // Set the total number of entries which will be use for displaying 
    // scroll bar
    fnSetMenuEntriesTotalNumber (ulNextSlotIndex - ulSlotIndex);

    // Update LED indicator
    fnUpdateLEDPageIndicator();
    *pNextScr = 0;      /* leave next screen unchanged */

    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpPostMenuPrintingMode
//
// DESCRIPTION: 
//		Post function that is called when leaving the Printing Mode Menu screen
//
// INPUTS:
//		Standard screen post function inputs.
//
// OUTPUTS:
//		None
//
// RETURNS:
//		Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	12/06/2005	Vincent Yi		Initial version 
//
// *************************************************************************/
SINT8 fnpPostMenuPrintingMode (void	   	(** pContinuationFcn)(), 
                               UINT32 	* 	pMsgsAwaited)
{
    SET_MENU_LED_OFF();

    return (BOOL)PREPOST_COMPLETE;
}


/* Field functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fnfPrintPositionInit
//
// DESCRIPTION: 
//      Init print position entry field by replaying the string
//      into the entry buffer.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  11/10/2005  Dicky Sun       Code cleanup
//              Henry Liu       Initial version
//
// *************************************************************************/

BOOL fnfPrintPositionInit (UINT16 *pFieldDest,
                           UINT8 bLen,
                           UINT8 bFieldCtrl, 
                           UINT8 bNumTableItems,
                           UINT8 *pTableTextIDs, 
                           UINT8 *pAttributes)
{
    UINT8     bNLen = 0;
    UINT16    i = 0;
    unichar   uniPos[3];

    (void)fnfStdEntryInit(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, pAttributes);

    /* Get the value to CMOS */
    usTmpRightMarginPos = fnCmosGetRightMargin();
    // Store the value for restore.
    usOldRightMarginPos = usTmpRightMarginPos;
    bNLen = fnBin2Unicode(uniPos, usTmpRightMarginPos);
    uniPos[bNLen] = (UNICHAR)NULL;

    while( (uniPos[i]) && (i < bNLen) )
    {
        fnBufferUnicode(uniPos[i++]);
    }

    fMarginPosInit = TRUE;

    return(fnfStdEntryRefresh(pFieldDest, bNLen, bFieldCtrl, bNumTableItems, pTableTextIDs, pAttributes));
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfIndiciaPrintingStatus
//
// DESCRIPTION: 
// 		Field function for displaying printing status in Indicia Printing screen
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  01/04/2009  Raymond Shen    Adapted it for WOW printing.
//  11/17/2006  Adam Liu       Fix fraca GMSE00106326 by changing the 
                               "printing..." animator to use screen ticks.
// 	11/16/2005	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnfIndiciaPrintingStatus(UINT16 *pFieldDest, 
                              UINT8	 bLen, 	
                              UINT8	 bFieldCtrl, 
                              UINT8	 bNumTableItems, 
                              UINT8  *pTableTextIDs,
                              UINT8  *pAttributes)
{
    UINT8   ucStringNum = 0;
    UINT8   ucMode = fnOITGetPrintMode();

    if( ((ucMode == PMODE_WEIGH_1ST) || (ucMode == PMODE_WOW))
            && (bNumTableItems > 4))
    {
        switch (bCurrentPrintState)
        {
            case PSTATE_PRINTING:
                ucStringNum = 4;
                break;
            case PSTATE_PREPARING_TO_PRINT:
                ucStringNum = 5;
                break;
            case PSTATE_REZEROING_WOW:
                ucStringNum = 6;
                break;
            case PSTATE_PERFORMING_MAINT:
                ucStringNum = 7;
                break;
            default:
                ucStringNum = 4;
                break;
        }

        if (bNumTableItems > ucStringNum)
        {
            fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                       pTableTextIDs, ucStringNum);
        }
    }
    else
    {
        // Currently, always display "Printing..."
        //fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
        //                                                pTableTextIDs, 0);

        UINT8 ucCount = fnGetAnimatorCounter();
        if (ucCount < bNumTableItems)
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                       pTableTextIDs, ucCount);
    }
    

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfIndiciaTapePrintingStatus
//
// DESCRIPTION: 
// 		Field function for displaying printing status in Indicia Printing screen
//
// PRE-CONDITIONS:
//		ucPrintTapeSeqNo and ucTotalTapesToPrint have been updated 
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/10/2010  Deborah Kohl Modified to have 1/2 printing instead of printing 1/2 in RTL mode
//  12/03/2008  Joe Qu  Modified fnfIndiciaTapePrintingStatus() to make sure unused field in pFieldDest 
                        are padded with spaces
//  11/28/2008  Joe Qu  Modified to fix fraca GMSE00126761, remove the displayed digit at the end 
//                      of the field if sequence number length exceeds the buffer length.
// 	11/30/2005	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnfIndiciaTapePrintingStatus(UINT16   *pFieldDest, 
                                  UINT8		bLen, 	
                                  UINT8		bFieldCtrl, 
                                  UINT8		bNumTableItems, 
                                  UINT8    *pTableTextIDs,
                                  UINT8    *pAttributes)
{
    char	pcTempStr[SIZE_TAPE_COUNTER] = {0};
    UNICHAR	pUniTempStr[SIZE_TAPE_COUNTER] = {0};
//	UNICHAR	pUniTempStr2[SIZE_TAPE_COUNTER] = {0};
    UINT8   bWritingDir;
    
    SET_SPACE_UNICODE(pFieldDest, bLen);

    bWritingDir = fnGetWritingDir();        

    if (bTapePrintStatus == TAPE_PRINTING)
    {
        UINT8	bStrLen;
        UINT8	bStr2Len;
        UINT16	usTextID;

        bStrLen = sprintf (pcTempStr, "%d/%d", ucPrintTapeSeqNo, 
                                            ucTotalTapesToPrint	);
        pcTempStr[bStrLen] = (UNICHAR)NULL;
        fnAscii2Unicode (pcTempStr, pUniTempStr, bStrLen+1);

        // Get "Printing " from field text table
        
        EndianAwareCopy(&usTextID, pTableTextIDs + (0 * sizeof(UINT16)), sizeof(UINT16));
        if(bWritingDir == RIGHT_TO_LEFT)
        {
            bStr2Len = fnCopyTableTextForRTL(pFieldDest, usTextID, bLen);
        }
        else
        {        
            bStr2Len = fnCopyTableText(pFieldDest, usTextID, bLen);
        }
        pFieldDest[bStr2Len] = (UNICHAR)NULL;

        if (bStrLen > (bLen - bStr2Len))
        {	// If no enough room to show tape count, display "*/*"
        	EndianAwareCopy(&usTextID, pTableTextIDs + (2 * sizeof(UINT16)), sizeof(UINT16));
            bStrLen = fnCopyTableText(pUniTempStr, usTextID, SIZE_TAPE_COUNTER);
            pUniTempStr[bStrLen] = (UNICHAR)NULL;
        }
        // Combine the string "Printing " with pUniTempStr
        if(bWritingDir == RIGHT_TO_LEFT)
        {
            // move the string and the NULL char to insert the tape numbers
            (void)memmove(pFieldDest+(bStrLen+1),pFieldDest,(bStr2Len+1) * sizeof(UINT16));
            (void)memcpy(pFieldDest, pUniTempStr, bStrLen*sizeof(UINT16)); 
            pFieldDest[bStrLen] = UNICODE_SPACE;
            bStrLen = unistrlen (pFieldDest);
        }
        else
        {        
            (void)unistrcat (pFieldDest, pUniTempStr );
            bStrLen = unistrlen (pFieldDest);
            if (bStrLen<bLen)
            {
                UINT8 i;
                for(i=bStrLen; i<bLen; i++)
                {
                    pFieldDest[i] = UNICODE_SPACE;
                }
            }
        }
        
        /* if field is right-aligned, pad left of string with spaces */
        if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
            ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
        {
            // when field right-aligned, we need to pad only if we are not in RTL mode, 
            // because right aligned in LTR mode gives left aligned in RTL mode
            // when field left-aligned, we need to pad only if we are in RTL mode, 
            // because left aligned in LTR mode gives right aligned in RTL mode
            pFieldDest[bStrLen] = (UINT16) NULL_VAL;
            fnUnicodePadSpace(pFieldDest, bStrLen, bLen);
        }
    }

    if (bTapePrintStatus == TAPE_PRINT_COMPLETED)
    {	// "Complete!"
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                            pTableTextIDs, 1);
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfIndiciaTapePrintingStatus1
//
// DESCRIPTION: 
// 		Field function for displaying printing status in Printing screens
//     except normal Indicia Mode.
//
// PRE-CONDITIONS:
//		ucPrintTapeSeqNo and ucTotalTapesToPrint have been updated 
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/13/2009  Raymond Shen    Initial version
//
// *************************************************************************/
BOOL fnfIndiciaTapePrintingStatus1(UINT16   *pFieldDest, 
                                  UINT8		bLen, 	
                                  UINT8		bFieldCtrl, 
                                  UINT8		bNumTableItems, 
                                  UINT8    *pTableTextIDs,
                                  UINT8    *pAttributes)
{
    UINT8   ucLength;
    UINT8   i;
    UINT8   ucRightAdjustPosition;
    
    (void) fnfIndiciaTapePrintingStatus(pFieldDest, bLen, bFieldCtrl,
                                                        bNumTableItems, pTableTextIDs, pAttributes);

    ucLength = (UINT8) unistrlen(pFieldDest);

    // Find out the none space string length.
    while(pFieldDest[ucLength - 1] == UNICODE_SPACE)
    {
        ucLength --;
    }

    // Center align the display buffer.
    if(ucLength < bLen)
    {
        // Calculate position shift.
        ucRightAdjustPosition = (bLen - ucLength) / 2;

        // Move the string right.
        for(i = ucLength; i > 0; i--)
        {
            pFieldDest[ ucRightAdjustPosition + i -1] = pFieldDest[i -1 ];
        }

        // Fill spaces before the string.
        for(i = 0; i < ucRightAdjustPosition; i++)
        {
            pFieldDest[i] = UNICODE_SPACE;
        }
    }
    
    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfMaxTapeNumber
//
// DESCRIPTION: 
// 		Field function for displaying the maximum of tape number configured in 
//		EMD
//
// PRE-CONDITIONS:
//		None
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/10/2010  Deborah Kohl    Add padding characters either when 
//                              right aligned andin LTR mode or 
//                              left aligned and in RTL mode
// 	12/1/2005	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnfMaxTapeNumber(UINT16   *pFieldDest, 
                      UINT8		bLen, 	
                      UINT8		bFieldCtrl, 
                      UINT8		bNumTableItems, 
                      UINT8    *pTableTextIDs,
                      UINT8    *pAttributes)
{
    UINT8 	bStrLen;
    UINT8   bWritingDir;

    SET_SPACE_UNICODE(pFieldDest, bLen);

    bStrLen = fnBin2Unicode(pFieldDest, fnGetMaximumTapeNumber());

    bWritingDir = fnGetWritingDir();

    /* if field is right-aligned, pad left of string with spaces */
    if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
        fnUnicodePadSpace(pFieldDest, bStrLen, bLen);
    }
    pFieldDest[bLen] = (UINT16) NULL;

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfPrintMode
//
// DESCRIPTION: 
//      Output field function for display current printing mode
//
// PRE-CONDITIONS:
//      None
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//      Menu possibilities:
//       1. Key In Postage
//       2. Permit
//       3. Manual Weight
//       4. Scale Weight
//       5. Diff. Weight
//
// MODIFICATION HISTORY:
//  05/05/2009   Jingwie,Li      Updated for W1P+SBR.
//  04/30/2009   Jingwei,Li      Updated for WOW+SBR.
//  03/05/2007   I. Le Goff      Add modifications for mode ajout
//  08/30/2006   Oscar Wang      Updated for Auto Scale OFF
//  02/23/2006   Vincent Yi      Initial version
// *************************************************************************/
BOOL fnfPrintMode(UINT16    *pFieldDest, 
                  UINT8     bLen,   
                  UINT8     bFieldCtrl, 
                  UINT8     bNumTableItems, 
                  UINT8     *pTableTextIDs,
                  UINT8     *pAttributes)
{
    UINT8 	bFieldIndex = bNumTableItems;
    UINT8	ucPrintMode = fnOITGetPrintMode();  

    SET_SPACE_UNICODE(pFieldDest, bLen);
    
    switch (ucPrintMode)
    {
    case PMODE_MANUAL:
        bFieldIndex = 0;	// Key In Postage
        break;

    case PMODE_PERMIT:
        bFieldIndex = 1;	// Permit
        break;

    case PMODE_MANUAL_WGT_ENTRY:
    case PMODE_AJOUT_MANUAL_WGT_ENTRY:
        bFieldIndex = 2;	// manual Weight
        break;
    
    case PMODE_PLATFORM:
    case PMODE_AJOUT_PLATFORM:
        bFieldIndex = 3;	// Scale Weight
        break;

    case PMODE_DIFFERENTIAL_WEIGH:
        bFieldIndex = 4;	// Diff. Weight
        break;

    case PMODE_WOW:
        if(fnIsWOWDREnabled()==TRUE)
        {
                bFieldIndex = 7;// WOW + SBR
        }
        else
        {
            bFieldIndex = 6;	// WOW
        }
        break;

    case PMODE_WEIGH_1ST:
        if(fnIsWOWDREnabled()==TRUE)
        {
                bFieldIndex = 9;//W1P + SBR
        }
        else
        {
            bFieldIndex = 8;	// W1P
        }
        break;
    
    default:
        break;
    } // switch (ucPrintMode)
    
    if (bFieldIndex < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                            pTableTextIDs, bFieldIndex);
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnfPrintPositionRange
//
// DESCRIPTION: 
// 		Output field function to display the print position range.
//
// INPUTS:
//		Standard field function inputs.
//
// OUTPUTS:
//		pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//		Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  04/28/2010  Deborah Kohl    Add RTL management
//  08/02/2006  Raymond Shen    Right Margin parameters are retrieved from EMD
// 	05/31/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnfPrintPositionRange (UINT16      *pFieldDest, 
                            UINT8 		bLen, 
                            UINT8 		bFieldCtrl,
                            UINT8 		bNumTableItems, 
                            UINT8       *pTableTextIDs,
                            UINT8       *pAttributes)
{
    UINT8       ucStrLen, ucWritingDir; 
    char		pcTempStr[PRINT_POS_RANGE_STR_LEN] = {0};
    ST_PO_RIGHT_MARGIN       *pRightMargin = 0;
    
    SET_SPACE_UNICODE(pFieldDest, bLen);
    
    ucWritingDir = fnGetWritingDir();

    pRightMargin = (ST_PO_RIGHT_MARGIN *) fnFlashGetPackedByteParm(PBP_RIGHT_MARGIN_ADJUST_RANGE);
    
    if(ucWritingDir == RIGHT_TO_LEFT)
    {
        ucStrLen = sprintf (pcTempStr, "(%d mm - %d)", 
                            pRightMargin->maxRange, 
                            pRightMargin->minRange);
    }
    else
    {
        ucStrLen = sprintf (pcTempStr, "(%d - %d mm)", 
                            pRightMargin->minRange, 
                            pRightMargin->maxRange);
    }

    pcTempStr[ucStrLen] = (char)NULL;
    fnAscii2Unicode (pcTempStr, pFieldDest, ucStrLen+1);
    fnUnicodeCenterAligned (pFieldDest, ucStrLen, bLen);

    return ((BOOL) SUCCESSFUL);
}


/* Hard key functions */

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkCheckTapeConfig
//
// DESCRIPTION: 
// 		Hard key function to check the tape configuration before tape printing
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Next Screen:
//			Next screen 1 - SetupMultipleTapes, if Tape configured to multi 
//							tapes printing.
//			no screen change, if Tape configured to signle tape printing.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/22/2007  Oscar Wang      Update for GTS tape printing to fix FRACA
//                              GMSE00128011
//  07/31/2006  Dicky Sun       For GTS, suppress multitape printing
//  07/31/2006  Oscar Wang      Modified for DWM tape printing.
//  07/12/2006  Oscar Wang      Added support for Diff Weighing
//	11/30/2005	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkCheckTapeConfig(UINT8 	   		bKeyCode, 
                                UINT8			bNumNext, 
                                T_SCREEN_ID *	pNextScreens)
{
    T_SCREEN_ID		usNext = 0;

    if (fnIsCMOSMultiTapes() == TRUE)
    {
        usNext = pNextScreens[0];	// Tape Quantity screen
    }
    else
    {
        usNext = fnStartToPrintTape (1, bNumNext, pNextScreens, TRUE);
    }
    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkValidateTapeCountToPrint
//
// DESCRIPTION: 
// 		Hard key function that validates and stores the number of tapes to 
//		print. Modified from Mega 
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		If entered value is valid, to next screen 1
//		Else, no screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  10/16/2009  Raymond Shen    Remove previous change for GMSE00173492 because
//                              there is a better solution in oiabacus2.c
//  10/15/2009  Raymond Shen    Set multi-tape flag to fix GMSE00173492.
//  09/10/2007  Oscar Wang      Set flag fBarcodeNoneReset for out of barcode
//                              case to retain class.
//  09/07/2007  Oscar Wang      For tape printing, added "no enough barcode 
//                              to print tape" screen.
//	12/1/2005	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkValidateTapeCountToPrint(UINT8 	   	bKeyCode, 
                                         UINT8			bNumNext, 
                                         T_SCREEN_ID *	pNextScreens)
{
    T_SCREEN_ID		usNext = 0;
    UINT16			usMaxTape = fnGetMaximumTapeNumber();
    UINT32			ulTapeCount;
    BOOL			fStatus;

    fStatus = fnUnicode2Bin(fnGetGenPurposeEbuf()->pBuf, &ulTapeCount, 
                            fnBufferLen());
    if ((fStatus == SUCCESSFUL) && (ulTapeCount > 0) && 
        (ulTapeCount <= usMaxTape))
    {
        usNext = fnStartToPrintTape ((UINT8)ulTapeCount, bNumNext,
                                        pNextScreens, TRUE);
    }
    else
    {	// Invalid entry

        // Clear the entry buffer
        fnClearEntryBuffer();
        // Set the entry message to invalid entry, no screen change
        fnSetEntryErrorCode (ENTRY_INVALID_VALUE);
    }

    return (usNext);
}


/* *************************************************************************
// FUNCTION NAME: 
//		fnhkValidateRemainingGTSTapeCount
//
// DESCRIPTION: 
// 		Hard key function that validates and stores the remaing GTS barcode
//      number of tapes to print. 
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Next screen..
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	09/07/2007	Oscar Wang    Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkValidateRemainingGTSTapeCount(UINT8 	   	bKeyCode, 
                                         UINT8			bNumNext, 
                                         T_SCREEN_ID *	pNextScreens)
{
    return (0);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkStartToPrintMail
//
// DESCRIPTION: 
// 		Hard key function that handles the Start key pressing in ready mode
//		to run mail on DM400C, it can be used in most of the printing mode
//		except the multi-page printing like OIT_REPORT 
//
// PRE-CONDITIONS:
//		The base type has been checked before calling this function, only for
//		DM400C
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Next screen:
//		if in Ready mode, no screen change
//		if in not Ready mode, to ready transient screen.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	1/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkStartToPrintMail(UINT8 	   		bKeyCode, 
                                 UINT8			bNumNext, 
                                 T_SCREEN_ID *	pNextScreens)
{
    return fnStartToPrintMail (0, bNumNext, pNextScreens, TRUE, FALSE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkStartToRunMail
//
// DESCRIPTION: 
// 		Hard key function that handles the Start key pressing in ready mode
//		to run mail in seal-only mode on DM400C 
//
// PRE-CONDITIONS:
//		The base type has been checked before calling this function, only for
//		DM400C
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Next screen:
//		if in Ready mode, no screen change
//		if in not Ready mode, to ready transient screen.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	1/17/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkStartToRunMail(UINT8 	   		bKeyCode, 
                               UINT8			bNumNext, 
                               T_SCREEN_ID *	pNextScreens)
{
    return fnStartToPrintMail (0, bNumNext, pNextScreens, TRUE, FALSE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkStartToPrint1Tape
//
// DESCRIPTION: 
// 		Hard key function that prints one tape
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		no screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	2/08/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkStartToPrint1Tape(UINT8 	   	bKeyCode, 
                                  UINT8			bNumNext, 
                                  T_SCREEN_ID *	pNextScreens)
{
    return fnStartToPrintTape (1, bNumNext, pNextScreens, TRUE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkStopPrintingMail
//
// DESCRIPTION: 
// 		Hard key function that requests us to stop the current mail print 
//		job. If successful, the mail job complete event should occur. 
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		no screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	1/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkStopPrintingMail(UINT8 	   		bKeyCode, 
                                 UINT8			bNumNext, 
                                 T_SCREEN_ID *	pNextScreens)
{
    T_SCREEN_ID	usNext = 0;

    fnSendStopMailRequest ();
    
    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkStopPrintingTape
//
// DESCRIPTION: 
// 		Hard key function that requests us to stop the current tape print 
//		job. If successful, the mail job complete event should occur. 
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		no screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	1/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkStopPrintingTape(UINT8 	   		bKeyCode, 
                                 UINT8			bNumNext, 
                                 T_SCREEN_ID *	pNextScreens)
{
    T_SCREEN_ID	usNext = 0;

	if (ucTotalTapesToPrint > 1)
	{	// Multi-tape, pause the printing;
		// for last tape, stop is disabled
		if (ucTotalTapesToPrint > ucPrintTapeSeqNo)
		{
			cMultiTapePrintingStopped = MULTI_TAPE_MANUAL_STOPPED;		
		    fnSendStopTapeRequest();
		}
	}
	else
	{	// Single tape
	    fnSendStopTapeRequest();
	}
    
    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkSwitchModeSealOnly
//
// DESCRIPTION: 
// 		Hard key function that set the seal only mode status to true.
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Always to Next screen 1 - Seal only ready screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	12/06/2005	Vincent Yi		Code cleanup
//    12/01/2008   Jingwei,Li        Set Hybrid mode flag to false.
//
// *************************************************************************/
T_SCREEN_ID fnhkSwitchModeSealOnly(UINT8 	   		bKeyCode, 
                                   UINT8			bNumNext, 
                                   T_SCREEN_ID *	pNextScreens)
{
    fnOITSetPrintMode(PMODE_SEAL_ONLY);
    fHybridMode = FALSE;

    return pNextScreens[0];
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkSwitchModeTextAdDateTime
//
// DESCRIPTION: 
// 		Hard key function that set the TextAdDateTime mode status to true.
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Always to Next screen 1 - DateTime only ready screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//    12/01/2008   Jingwei,Li        Set Hybrid mode flag to false.
//  08/22/2007  Joey Cui 		Fixed FRACA GMSE00128014, clear rate info first
//  11/16/2006  Oscar Wang 		Fixed FRACA GMSE00107891, save bob duck setting
//                              before entering into TextAdDateTime screen.
//			    Adam Liu		Initial version
//	
//
// *************************************************************************/
T_SCREEN_ID fnhkSwitchModeTextAdDateTime   (UINT8 	   		bKeyCode, 
                                            UINT8			bNumNext, 
                                            T_SCREEN_ID *	pNextScreens)
{
    T_SCREEN_ID usNext = 0;

    /* First save Bob's settings */
    if (fnValidData(BOBAMAT0, GET_INDICIA_PRINT_FLAGS, &usDuckBits) != BOB_OK)
    {
        fnProcessSCMError();
        return usNext;
    }

    //set datetime flag on by default if date-time feature is enabled
    fnSetPrintDateTimeFlag (fnCheckDateTimeOnlyPrintingMode()==TRUE);
        
    fnOITSetPrintMode(PMODE_AD_TIME_STAMP);
    fHybridMode = FALSE;

    fnClearPostageAndAllRateInfo();
    
    return pNextScreens[0];
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkSwitchModePermit
//
// DESCRIPTION: 
// 		Hard key function that set the Permit mode status to true.
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Always to Next screen 1 - Permit ready screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//    12/01/2008   Jingwei,Li        Set Hybrid mode flag to false.
//  08/22/2007  Joey Cui    Fixed FRACA GMSE00128014, clear rate info first
//			   Adam Liu		Initial version
//	
//
// *************************************************************************/
T_SCREEN_ID fnhkSwitchModePermit(UINT8 	   		bKeyCode, 
                                       UINT8			bNumNext, 
                                       T_SCREEN_ID *	pNextScreens)
{
    fnOITSetPrintMode(PMODE_PERMIT);
    fHybridMode = FALSE;

    fnClearPostageAndAllRateInfo();

    return pNextScreens[0];
}



/* *************************************************************************
 FUNCTION NAME:     fnhkSwitchModeDateCorrection
 DESCRIPTION:       hard key function that is set the print mode to date correction
  
 AUTHOR:           Sandra Peterson

 INPUTS:            Standard hard key function inputs.  
 MODIFICATION HISTORY:
    12/01/2008   Jingwei,Li        Set Hybrid mode flag to false.
 *************************************************************************/
T_SCREEN_ID fnhkSwitchModeDateCorrection(UINT8 	   		bKeyCode, 
                                       UINT8			bNumNext, 
                                       T_SCREEN_ID *	pNextScreens)
{
    fnOITSetPrintMode(PMODE_DATE_CORRECTION);
    fHybridMode = FALSE;
	fnClearDateCorrection();
    return ((bNumNext> 0) ? pNextScreens[0] : 0);
}
/* *************************************************************************
 FUNCTION NAME:     fnhkSwitchModeManifest
 DESCRIPTION:       hard key function that is set the print mode to Manifest
  
 AUTHOR:           Sandra Peterson

 INPUTS:            Standard hard key function inputs.  
 MODIFICATION HISTORY:
    10/12/2009   Jingwei,Li        Don't switch to manifest mode if RM not initialized.
    09/24/2009   Jingwei,Li        Don't allow to select accout if only job ID requried.
                                   (Fraca GMSE00170880)
    09/18/2009   Jingwei,Li        Don't allow to print manifest if RM not initialized
                                   (Fraca GMSE00171003).
    12/01/2008   Jingwei,Li        Set Hybrid mode flag to false.
 *************************************************************************/
T_SCREEN_ID fnhkSwitchModeManifest(UINT8 	   		bKeyCode, 
                                       UINT8			bNumNext, 
                                       T_SCREEN_ID *	pNextScreens)
{   
    T_SCREEN_ID usNext = 0;

    if(fnRateRMInitialized()==FALSE)
    {
        usNext = pNextScreens[2];	//key in postage file error screen.
    }
    else
    {
        fnOITSetPrintMode(PMODE_MANIFEST);
        fHybridMode = FALSE;
/*
        if(fnGetAccountDisabledStatus() != ACT_NO_ACCOUNT_SELECTED)
        {	
            usNext = pNextScreens[0];	   //Manifest enter amount screen
        }
        else if(fnGetAccountingType() == ACT_SYS_TYPE_INTERNAL_ON)
        {
            // AcctSelection screen
            usNext = fnhkSelectAcct(bKeyCode, bNumNext-1, &pNextScreens[1]);
        }
        else
        {
            // To AbacusAccountIndexSelcet or HostedAbacusAccountIndexSelect
            // screen according to accounting type.
            usNext = fnhkAccountKeyMainScreen(bKeyCode, pNextScreens[1], pNextScreens[1]);
        }
*/
    }
    return (usNext);
}

/* *************************************************************************
 FUNCTION NAME:     fnhkSwitchModePostageCorrection
 DESCRIPTION:       hard key function that is set the print mode to Manifest
  
 AUTHOR:           Sandra Peterson

 INPUTS:            Standard hard key function inputs.  
  MODIFICATION HISTORY:
    09/24/2009   Jingwei,Li        Don't allow to select accout if only job ID requried.
                                   (Fraca GMSE00170880)
    12/01/2008   Jingwei,Li        Set Hybrid mode flag to false.
 *************************************************************************/
T_SCREEN_ID fnhkSwitchModePostageCorrection(UINT8 	   		bKeyCode, 
                                       		UINT8			bNumNext, 
                                       		T_SCREEN_ID *	pNextScreens)
{   
    T_SCREEN_ID usNext = 0;
    if(fnFlashGetByteParm( BP_ADD_POSTAGE_MODE )== ADD_POSTAGE_ALLOWED_FENCINGLIMIT)
    {
    	if ((fnRateRMInitialized()==FALSE)|| fnRateGetFencingLimitStatus() != SUCCESS)
        {
            usNext = pNextScreens[1];	//key in postage file error screen.
		}
		else
		{	
			fnOITSetPrintMode(PMODE_POSTAGE_CORRECTION);
			fHybridMode = FALSE;
            
/*
            if(fnGetAccountDisabledStatus() != ACT_NO_ACCOUNT_SELECTED)
            {
                usNext = pNextScreens[0];	   //Postage Correction enter amount screen
            }
            else if(fnGetAccountingType() == ACT_SYS_TYPE_INTERNAL_ON)
            {
                // AcctSelection screen
                usNext = fnhkSelectAcct(bKeyCode, bNumNext-2, &pNextScreens[2]);
            }
            else
            {
                // To AbacusAccountIndexSelcet or HostedAbacusAccountIndexSelect
                // screen according to accounting type.
                usNext = fnhkAccountKeyMainScreen(bKeyCode, pNextScreens[2], pNextScreens[2]);
            }
*/
		}
    }
    
	return(usNext);
}



/* *************************************************************************
 FUNCTION NAME:     fnSetAlterNativeMode
 DESCRIPTION:      
                    
 AUTHOR:           Tom Zhao

 INPUTS:            Standard hard key function inputs.  
 *************************************************************************/

void fnSetAlterNativeMode(unsigned char  bPrintMode)
{   
    bAlternMode = bPrintMode;
}


/* *************************************************************************
 FUNCTION NAME:     fnCheckPostCorrectionVal
 DESCRIPTION:       check the value if in fencing limit
                0 -- postage is valid
                1 -- not valid
                2 -- RM not initialized
                    
 AUTHOR:           Tom Zhao

 INPUTS:            Standard hard key function inputs.  
 MODIFICATION HISTORY:
    02-11-14    Wang Biao   Modified to fixed fraca 224400.
    05-22-06    Bob Li      replace fnGetLowFencingLimit() 
                                with fnRateGetLowFencingLimit(),
                            replace fnGetHighFencingLimit()
                                with fnRateGetHighFencingLimit(),
                            replace  fnGetFencingLimitStatus()
//                              with fnRateGetFencingLimitStatus()
    02-17-06    Bob Li      make fnRateRMInitialized() == true not SUCCESS
 *************************************************************************/
unsigned char fnCheckPostCorrectionVal(double dblAmtEntered)
{
    unsigned char   bIsLegal;
    double dbHighFlimit, dbLowFlimit;
    unsigned long lwPV;
    
    //get fencing limit 
   // if(fnRMInitialized())
/*   if(fnRateRMInitialized() == true)
    {
        if(fnRateGetFencingLimitStatus() == SUCCESS)
        {
            dbLowFlimit = fnRateGetLowFencingLimit();
            dbHighFlimit = fnRateGetHighFencingLimit();
        
            if( dblAmtEntered <= dbHighFlimit  && dblAmtEntered >= dbLowFlimit)
                return PosInValid;
            else 
                return PosValid;
        }
    }*/
    return RMnotInitialized;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkExitTextAdDateTime
//
// DESCRIPTION: 
//      hard key function that set the TextAdDateTime mode status to false.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      Next screen 1, Home Ready screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  2006.11.16  Oscar Wang      Fix FRACA GMSE00107891, restore bob duck setting
//  2006.09.04  Kan Jiang       Add code to reset Datatime flag to fix Fraca 103424.
//  2006.07.18  Clarisa Bellamy     Add call to fnSYSAllowRepeatOIEvent so that
//              the normal Enter Print Ready process will complete.  This will
//              allow the new image to be downloaded and the first print run, 
//              after the change, to print correctly.

//              Adam Liu       Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkExitTextAdDateTime(UINT8          bKeyCode, 
                                   UINT8          bNumNext, 
                                   T_SCREEN_ID *  pNextScreens)
{
    T_SCREEN_ID usNext = 0;
    UINT16   usTempBobWord = 0;

    /* Fix FRACA GMSE00107891: Restore bob duck setting */
    if (fnWriteData(BOBAMAT0, SET_INDICIA_PRINT_FLAGS, &usDuckBits) 
                    != SUCCESSFUL)
    {
        fnProcessSCMError();
        return usNext;
    }

    /* Set the print mode to a default mode, which is either key in 
        postage mode or a weighing mode. */
    (void)fnModeSetDefault();
    
    if (bNumNext > 0)
    {
        usNext = pNextScreens[0];
    }

    // This allows the Enter Print Ready event to be sent to SYS, even if it was the last 
    //  event sent to sys from OI, so that the new static image will be loaded by the 
    //  "screen change" even when we don't ever leave the print ready state.
    // The current screen must be in the next screen list for this to work, by the way.
    fnSYSAllowRepeatOIEvent( OIT_ENTER_PRINT_RDY );
    
    return (usNext);
}
/* *************************************************************************
// FUNCTION NAME: 
//      fnModeSetDefault
//
// DESCRIPTION: 
//      Sets the print mode global to manual and clear the postage.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None
//
// RETURNS:
//      always goes to next screen 1
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/01/2008  Jingwei,Li     Set Hybrid mode flag to false.
//  30 Mar 2007 Bill Herring    Additional KIP password type checked
//  11/10/2005  Dicky Sun       Code cleanup
//  11/10/2005  Dicky Sun       Code cleanup
//              Tim Zhang       Initial version
//
// *************************************************************************/

T_SCREEN_ID fnhkModeSetManual(UINT8 bKeyCode, UINT8 bNumNext, UINT16 *pNextScreens)
{
    UINT16  wNext = 0;
    UINT8   bKIPMode;

    bKIPMode = fnFlashGetByteParm(BP_KEY_IN_POSTAGE_MODE) & ~KIP_USE_MDS_TOKENS_MASK;

    if (bKIPMode != KIP_NOT_ALLOWED) 
    {
        /* set the print mode to key in postage mode */
        fnOITSetPrintMode(PMODE_MANUAL);
	 fHybridMode = FALSE;
        if (bNumNext)
            wNext = pNextScreens[0];

        /* set the password required */
        if ( (bKIPMode == KIP_ALLOWED_PASSWORD) || (bKIPMode == KIP_ZERO_ONLY_W_PASSWORD) )
        {
            pKIPPasswordInfo->ucPasswordStatus = KIP_PW_FROM_PRESET ;
            pKIPPasswordInfo->ucPasswordSelectPurpose = KIPW_PRESET_ENTER_REQUIRED ;
            pKIPPasswordInfo->lwPostageValue = 0;
        }
        /* maybe we need to handle other value of the EMD param BP_KEY_IN_POSTAGE_MODE later. */
        // else
    }

    return (wNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkValidatePrintPosition
//
// DESCRIPTION: 
//      Hard key function that validate the print position
//
// INPUTS:
//      Standard hard key function inputs.
//
// OUTPUTS:
//      Next screen id
//
// RETURNS:
//      Always go to next screen 1
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/02/2006  Raymond Shen    Right Margin parameters are retrieved from EMD
//  11/10/2005  Dicky Sun       Code cleanup
//              Henry Liu       Initial version
//
// *************************************************************************/

T_SCREEN_ID fnhkValidatePrintPosition( UINT8 bKeyCode,
                                       UINT8 bNumNext, 
                                       T_SCREEN_ID *pNextScreens )
{

    T_SCREEN_ID usWhichNext = 0;
    UINT32	  	ulPos = 0;
    SINT16		sPosOffset = 0;
    ST_PO_RIGHT_MARGIN       *pRightMargin = 0;

    pRightMargin = (ST_PO_RIGHT_MARGIN *) fnFlashGetPackedByteParm(PBP_RIGHT_MARGIN_ADJUST_RANGE);

    //If no char entered, we should return.
    if(fnCharsBuffered() == 0)
    {
         return usWhichNext;
    }

    /* read the value in the entry buffer, translating to binary */
    (void)fnUnicode2Bin(fnGetGenPurposeEbuf()->pBuf, &ulPos, fnBufferLen());
    sPosOffset = ulPos;

    if ((ulPos > pRightMargin->maxRange) 	|| 
        (ulPos < pRightMargin->minRange))
    {
        // Clear the entry buffer
        fnClearEntryBuffer();
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE);
    }
    else
    {
        usTmpRightMarginPos = (UINT16)sPosOffset;
        (void)fnOITSetRightMargin (usTmpRightMarginPos);
    }

    return (usWhichNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkModeSetDefault
//
// DESCRIPTION: 
// 		Sets the print mode global to a default print mode which the system
// 			allows. It is either key in postage mode or a weighing mode.
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		Always to Next screen 1
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/04/2014  Renhao          Clear Date duck for fraca 225148.
//  10/24/2007  Oscar Wang      Clear retained dest info for clear key at home screen.
//  10/10/2007  Oscar Wang      Update for Diff. Wt mode
//  08/30/2006  Oscar Wang      Updated function for Auto Scale Off
//  08/28/2006  Oscar Wang      Added DWM flow as per new UI Spec.
//	08/21/2006	Vincent Yi		Moved manual weight code to fnOITSetPrintMode 
//  08/10/2006  Oscar Wang      Fix manual weight problem.
//	12/06/2005	Vincent Yi		Code cleanup
//				Victor Li		Initial version
// *************************************************************************/
T_SCREEN_ID fnhkModeSetDefault(UINT8 	   		bKeyCode, 
                               UINT8			bNumNext, 
                               T_SCREEN_ID *	pNextScreens)
{
    T_SCREEN_ID		usNext = 0;
    UINT8           bCurrentPrintMode = fnRSGetPrintMode( ) ;

    if(bCurrentPrintMode == PMODE_DIFFERENTIAL_WEIGH)
    {        
        usNext = fnDWMClearFromHomeScreen();
    }
    else
    {
        /* go to next screen 1 as long as such a choice exists. */
        if (bNumNext > 0)
        {
            usNext = pNextScreens[0];
        }
    }

    // set rating to default.
    fnRatingSetDefault();
    
    // clear retained dest info.
    fnRateResetSavedDestInfo();

    //Added for fraca 225148, clear Date duck for Business Reply Mail Class
	fnDateDuckOff();

    // Clear the KIP Indicia information if needed.
    if((fnFlashGetByteParm(BP_KEY_IN_POSTAGE_MODE) & ~KIP_USE_MDS_TOKENS_MASK) == KIP_ALLOWED_HRT_STRINGS)
    {
        (void) fnRateClearKIPIndiciaInfo();
    }
    
    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkPrintPosBufferKeyClear1st
//
// DESCRIPTION: 
//      Hard key function that works like fnhkBufferKeyClear1st (the key is 
//		buffered, clearing initial value of the buffer if it is the first key)
//
// INPUTS:
//      Standard hard key function inputs.
//
// OUTPUTS:
//
// RETURNS:
//      No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Henry Liu       Initial version
//	2/07/2005	Vincent Yi		Modified for FP
//
// *************************************************************************/
T_SCREEN_ID fnhkPrintPosBufferKeyClear1st( UINT8 bKeyCode,
                                           UINT8 bNumNext, 
                                           T_SCREEN_ID *pNextScreens )
{
    T_SCREEN_ID		usNext = 0;

    if (fMarginPosInit == TRUE)
    {
        fMarginPosInit = FALSE;
        fnClearEntryBuffer();
    }

    fnBufferKey(bKeyCode);

    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkResumePrinting
//
// DESCRIPTION: 
// 		Hard key function that resume printing after error cleared. 
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		no screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	03/20/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkResumePrinting( UINT8 bKeyCode,
                                UINT8 bNumNext, 
                                T_SCREEN_ID *pNextScreens )
{
    fResumePrint = TRUE;
    return fnhkClearPaperErrors (bKeyCode, bNumNext, pNextScreens);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkResumePrintingTape
//
// DESCRIPTION: 
// 		Hard key function that resume printing tape after error cleared. 
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		no screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  11/06/2007  Oscar Wang    Fix FRACA GMSE00131947: for GTS mail, if debit 
//                            is done and paper error happens, we can't resume 
//                            printing even for tape since barcode is already 
//                            used.
//  06/29/2007  I. Le Goff    Modifiy so we don't resume the print in mode ajout or
//                            when incident reports are enabled.
//	03/21/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkResumePrintingTape( UINT8 bKeyCode,
                                    UINT8 bNumNext, 
                                    T_SCREEN_ID *pNextScreens )
{
    if (fnAreWeInModeAjout() == TRUE || fnFlashGetByteParm(BP_DCAP_INDICIA_PWR_FAIL_REPORT) == TRUE)
    {
        return 0;
    }
    else
    {
        fResumePrint = TRUE;
        return fnhkClearTapeErrors (bKeyCode, bNumNext, pNextScreens);
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkResumePrintingEnvelopeOut
//
// DESCRIPTION: 
// 		Hard key function that resume printing after envelopes loaded in feeder. 
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		no screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	05/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkResumePrintingEnvelopeOut( UINT8 bKeyCode,
                                           UINT8 bNumNext, 
                                           T_SCREEN_ID *pNextScreens )
{
    T_SCREEN_ID		usNext = 0;

    if (fFeederPaperAdded == TRUE)
    {	// For feed no mail error, mail added
        fFeederPaperAdded = FALSE;
        fnClearCMPaperErrorStatus();
        
        if (fPiecesNumAppointed == TRUE)
        {	// Should be appointed piece number
            if (ucTotalPagesToPrint >= ucPrintPageSeqNo)
            {
                usNext = fnStartToPrintMail (ucTotalPagesToPrint - ucPrintPageSeqNo + 1, 
                                         bNumNext, pNextScreens, FALSE, TRUE);
            }
            else
            {
                usNext = fnRetFromErrScreen (bNumNext, pNextScreens);
            }
        }
    }
    
    return usNext;
}


/* *************************************************************************
// FUNCTION NAME: 
//		fnhkResumePrintingWithFeeder
//
// DESCRIPTION: 
// 		Hard key function that resume printing after envelopes loaded in feeder. 
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		no screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  06/23/2009  Raymond Shen  Removed Jingwei's previous modification for we do check the
//                            status of sensor W2 and W3 when receiving the event that
//                            CM error is cleared.
//  06/22/2009  Jingwei,Li    If sensor blocked, go to mail jam error screen
//  07/05/2007  I. Le Goff    Disable resume printing when we are in mode ajout or when
//                            power fail reports are enabled
//	05/12/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkResumePrintingWithFeeder( UINT8 bKeyCode,
                                          UINT8 bNumNext, 
                                          T_SCREEN_ID *pNextScreens )
{
    if (fnAreWeInModeAjout() == TRUE || fnFlashGetByteParm(BP_DCAP_INDICIA_PWR_FAIL_REPORT) == TRUE)
    {
        return 0;
    }
    else
    {
        fResumePrint = TRUE;
    
        return fnhkClearFeederErrors (bKeyCode, bNumNext, pNextScreens);
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkResumePrintingFeederCover
//
// DESCRIPTION: 
//      Hard key function that resume printing after feeder cover closed. 
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/05/2007  I. Le Goff    Disable resume printing when we are in mode ajout or when
//                            power fail reports are enabled
//  06/12/2007  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkResumePrintingFeederCover( UINT8        bKeyCode,
                                           UINT8        bNumNext, 
                                           T_SCREEN_ID *pNextScreens )
{
    T_SCREEN_ID		usNext = 0;

    if (fnAreWeInModeAjout() == TRUE || fnFlashGetByteParm(BP_DCAP_INDICIA_PWR_FAIL_REPORT) == TRUE )
    {
        return 0;
    }
    else
    { 
        if (fnIsCmFeederCoverOpen() == FALSE)
        {
            if (fnIsCmPaperError() == TRUE  ||
                fnIsCmTapeError() == TRUE   ||
                fnIsCmFeederError() == TRUE)
            {
                fnClearCMPaperErrorStatus();
            }

            if( fnIsWOWMailJammed() == TRUE )
            {
                usNext = pNextScreens[1];
            }
            else
            {
                if (fPiecesNumAppointed == TRUE)
                {   // appointed piece number
                    if (ucTotalPagesToPrint >= ucPrintPageSeqNo)
                    {
                        usNext = fnStartToPrintMail (ucTotalPagesToPrint - ucPrintPageSeqNo + 1, 
                                                 bNumNext, pNextScreens, FALSE, TRUE);
                    }
                    else
                    {
                        usNext = fnRetFromErrScreen (bNumNext, pNextScreens);
                    }
                }
                else
                {   // no appointed piece number
                    usNext = fnStartToPrintMail (0, bNumNext, pNextScreens, FALSE, FALSE);
                }
            }
        }

        return usNext;
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkResumePrintingWOWMailJammed
//
// DESCRIPTION: 
//      Hard key function that resume printing from WOWMailJammed screen. 
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/15/2009  Raymond Shen    Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkResumePrintingWOWMailJammed( UINT8        bKeyCode,
                                           UINT8        bNumNext, 
                                           T_SCREEN_ID *pNextScreens )
{
    T_SCREEN_ID		usNext = 0;

    if (fnAreWeInModeAjout() == TRUE || fnFlashGetByteParm(BP_DCAP_INDICIA_PWR_FAIL_REPORT) == TRUE )
    {
        return 0;
    }
    else
    { 
        if( fnIsWOWMailJammed() == FALSE )
        {
            if (fPiecesNumAppointed == TRUE)
            {   // appointed piece number
                if (ucTotalPagesToPrint >= ucPrintPageSeqNo)
                {
                    usNext = fnStartToPrintMail (ucTotalPagesToPrint - ucPrintPageSeqNo + 1, 
                                             bNumNext, pNextScreens, FALSE, TRUE);
                }
                else
                {
                    usNext = fnRetFromErrScreen (bNumNext, pNextScreens);
                }
            }
            else
            {   // no appointed piece number
                usNext = fnStartToPrintMail (0, bNumNext, pNextScreens, FALSE, FALSE);
            }
        }

        return usNext;
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkResumePrintingWithWOWError
//
// DESCRIPTION: 
// 		Hard key function that resume printing on WOW Error screen. 
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		no screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/17/2009  Raymond Shen  Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkResumePrintingWithWOWError( UINT8 bKeyCode,
                                          UINT8 bNumNext, 
                                          T_SCREEN_ID *pNextScreens )
{
    T_SCREEN_ID usNext = 0;
    
    if(fnIsWOWNeedCalibrationError() == FALSE)
    {
        usNext = fnhkResumePrintingWithFeeder(bKeyCode, bNumNext, pNextScreens);
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkCancelPrinting
//
// DESCRIPTION: 
// 		Hard key function that cancel printing after error cleared. 
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		no screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	03/20/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkCancelPrinting( UINT8 bKeyCode,
                                UINT8 bNumNext, 
                                T_SCREEN_ID *pNextScreens )
{
    fResumePrint = FALSE;
    return fnhkClearPaperErrors (bKeyCode, bNumNext, pNextScreens);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkCancelPrintingTape
//
// DESCRIPTION: 
// 		Hard key function that cancel printing tape after error cleared. 
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		no screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/17/2009  Jingwei,Li      Scrap Label Reminder if Canada package service tracking on.
//	03/21/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkCancelPrintingTape( UINT8 bKeyCode,
                                    UINT8 bNumNext, 
                                    T_SCREEN_ID *pNextScreens )
{
    T_SCREEN_ID usRet = 0;
	fResumePrint = FALSE;
    usRet = fnhkClearTapeErrors (bKeyCode, bNumNext, pNextScreens);
    return usRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkCancelPrintingEnvelopeOut
//
// DESCRIPTION: 
// 		Hard key function that cacel printing after envelopes loaded in feeder. 
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		no screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	05/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkCancelPrintingEnvelopeOut( UINT8 bKeyCode,
                                           UINT8 bNumNext, 
                                           T_SCREEN_ID *pNextScreens )
{
    return fnhkPaperErrorClearedReturn (bKeyCode, bNumNext, pNextScreens);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkCancelPrintingWithFeeder
//
// DESCRIPTION: 
// 		Hard key function that cacel printing after envelopes loaded in feeder. 
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		no screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	05/12/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkCancelPrintingWithFeeder( UINT8 bKeyCode,
                                          UINT8 bNumNext, 
                                          T_SCREEN_ID *pNextScreens )
{
    fResumePrint = FALSE;
    
    return fnhkClearFeederErrors (bKeyCode, bNumNext, pNextScreens);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkCancelPrintingFeederCover
//
// DESCRIPTION: 
//      Hard key function that cancel printing after feeder cover closed. 
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  06/12/2007  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkCancelPrintingFeederCover( UINT8        bKeyCode,
                                           UINT8        bNumNext, 
                                           T_SCREEN_ID *pNextScreens )
{
    T_SCREEN_ID		usNext = 0;

    usNext = fnRetFromErrScreen(bNumNext, pNextScreens);

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkKIPValidateTapeCountToPrint
//
// DESCRIPTION: 
// 		Hard key function that validates and stores the number of tapes to 
//		print in KIP screen.
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//		If entered value is valid, to next screen 1
//		Else, no screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  10/16/2009  Raymond Shen    Remove all the changes for GMSE00161089 because
//                              there is a better solution in oiabacus2.c
//  10/15/2009  Raymond Shen    Polished Fred's fix for GMSE00161089 to fix the problem 
//                              that will still happen when GTS tracking service is on.
//  5/13/2009   Fred Xu         Fix FRACA GMSE00161089
//  8/23/2007   Vincent Yi      Fixed fraca 128096,in GTS single piece mode 
//                              or diff. wt mode, only one tape printed out
//	12/1/2005	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkKIPValidateTapeCountToPrint(UINT8 	   	bKeyCode, 
                                            UINT8			bNumNext, 
                                            T_SCREEN_ID *	pNextScreens)
{
    T_SCREEN_ID		usNext = 0;
//	UINT32			ulDisablingConditions = fnOITGetDisablingConditions();
//	UINT32			ulDisablingConditions2 = fnOITGetDisablingConditions2();

//    if (ulDisablingConditions == 0	&& ulDisablingConditions2 == 0)
    if (fnGetDisabled1() == 0	&& fnGetDisabled2() == 0)
    {
        if (fnTapeBufferValid() == TRUE)
        {
            usNext = fnhkValidateTapeCountToPrint (bKeyCode, bNumNext, pNextScreens);
        }
		else //tape quantity is invalid
			if( fnGetKIPMode()== KIP_NOT_ALLOWED)
			{	
				fnEntryBufferSnapshotRestore();		    	
				// Clear the entry buffer
				fnClearEntryBuffer();
				// Set the entry message to invalid entry, no screen change
				fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
	    	}
    }
    else
    {	// with disabling conditions, return to Home error screen
        usNext = pNextScreens[0];	// MMIndiciaReady
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkContinueToPrintTape
//
// DESCRIPTION: 
// 		Hard key function that continues to print tape.
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	11/27/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkContinueToPrintTape(UINT8 	   	bKeyCode, 
                                    UINT8			bNumNext, 
                                    T_SCREEN_ID *	pNextScreens)
{
    T_SCREEN_ID		usNext = 0;

	// Check CM status before resuming printing
	usNext = fnCheckCMStatusInPrinting();

	if (usNext == 0)
	{
	    if (ucTotalTapesToPrint >= ucPrintTapeSeqNo)
	    {
	        usNext = fnStartToPrintTape (ucTotalTapesToPrint - ucPrintTapeSeqNo + 1, 
	                                     bNumNext, pNextScreens, FALSE);
	    }
	    else
	    {
	        usNext = fnRetFromErrScreen(bNumNext, pNextScreens);
	    }
	}
	
	return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnhkPauseToStopPrintingTape
//
// DESCRIPTION: 
// 		Hard key function that cancel tape printing when printing paused
//
// INPUTS:
//		Standard hard key function inputs (format 2).
//
// RETURNS:
//
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	11/27/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkPauseToStopPrintingTape(UINT8 	   	bKeyCode, 
	                                    UINT8			bNumNext, 
	                                    T_SCREEN_ID *	pNextScreens)
{
    T_SCREEN_ID		usNext = 0;

	// Check CM status before resuming printing
	usNext = fnCheckCMStatusInPrinting();

	if (usNext == 0)
	{
		usNext = fnhkRetFromErrScreen (bKeyCode, bNumNext, pNextScreens);
	}

	return usNext;
}
/* *************************************************************************
// FUNCTION NAME: 
//      fnhkModeSetWOW
//
// DESCRIPTION: 
//          Sets the print mode global to wow.
//
// INPUTS:  
//      Standard hard key function inputs (format 2).
//
// NEXT SCREEN:   
//          always goes to next screen 1.
//
//
// MODIFICATION HISTORY:
//      11/18/2008 Jingwei,Li  Initial version
//      
// *************************************************************************/
UINT16 fnhkModeSetWOW(UINT8 bKeyCode, 
                      UINT8 bNumNext, 
                      UINT16 *pNextScreens)
{
    UINT16  usNext = 0;
    UINT8   bNoneCarClass = 0; 
    
    fnOITSetPrintMode(PMODE_WOW);
    fHybridMode = FALSE;
    
    bNoneCarClass  = fnModeChangeNeedClassScreen();     // if kip car mode return 0,1,2
    
    if (bNumNext >= bNoneCarClass )
    {
        usNext = pNextScreens[bNoneCarClass];
    }
    
    else if (bNumNext)
    {
            usNext = pNextScreens[0];
    }

    return (usNext);

}
/* *************************************************************************
// FUNCTION NAME: 
//      fnhkModeSetWeigh1st
//
// DESCRIPTION: 
//          Sets the print mode global to weigh first piece.
//
// INPUTS:  
//      Standard hard key function inputs (format 2).
//
// NEXT SCREEN:   
//          always goes to next screen 1.
//
//
// MODIFICATION HISTORY:
//      11/18/2008 Jingwei,Li  Initial version
//      
// *************************************************************************/
UINT16 fnhkModeSetWeigh1st(UINT8 bKeyCode, 
                      UINT8 bNumNext, 
                      UINT16 *pNextScreens)
{
    UINT16  usNext = 0;
    UINT8   bNoneCarClass = 0; 
    
    fnOITSetPrintMode(PMODE_WEIGH_1ST);
    fHybridMode = FALSE;
    
    bNoneCarClass  = fnModeChangeNeedClassScreen();     // if kip car mode return 0,1,2
    
    if (bNumNext >= bNoneCarClass )
    {
        usNext = pNextScreens[bNoneCarClass];
    }
    
    else if (bNumNext)
    {
            usNext = pNextScreens[0];
    }
    return (usNext);

}
/* *************************************************************************
// FUNCTION NAME: 
//      fnhkCheckHybridMode
//
// DESCRIPTION: 
//         Checks if we are currently in a hybrid print mode and restores
//         the previous mode if we are.
//
// INPUTS:  
//      Standard hard key function inputs (format 2).
//
// NEXT SCREEN: always goes to next screen 1 
//
// NOTES:
// 1.   V9.03 (cristeb): add new argument to fnSetWOWRateWeight() call to specify
//      top-level entry to function
// 2.   Post V9.23(cristeb): Reset envelope count to known 'safe' state to accommodate
//      any changes forced by hybrid print mode which will prevent W1P mode from working
//      properly (eg, KIP with  password which sets envelope count to a fixed limit of
//      prints before password is required again).
//
// MODIFICATION HISTORY:
//      06/25/2009    Jingwei,Li    If we have updated selected class token by Mode 
//                                  change(WOW/W1P to Scale mode),restore it.
//      05/20/2009    Jingwei,Li    Resovle System Fault 2003 issue.
//      05/03/2009    Jingwei,Li    Restore last valid rating context for revert wow.
//      12/01/2008    Jingwei,Li    Initial version.
// *************************************************************************/
UINT16 fnhkCheckHybridMode(UINT8 bKeyCode, 
                         				        UINT8 bNumNext, 
                                                        UINT16 *pNextScreens)
{
    UINT16  usNext = 0;
    BOOL    fLowValue;
    UINT8   bPrintIdType;

    if ((fHybridMode == TRUE))//Just for DM475
    {
        UINT32  ulPostageValue;
        UINT16    usEServiceType;

        fHybridMode = FALSE;
        bCurrentPrintMode = bHybridRestoreMode;
        bPreviousNonSealMode = bCurrentPrintMode;

        /* if we are attempting to restore to wow/wfp modes (which is the normal way
            hybrid modes are used, we need to rerate for WOW */
        if ((bHybridRestoreMode == PMODE_WOW) || (bHybridRestoreMode == PMODE_WEIGH_1ST))
        {
            SINT8   chRateStatus;
            UINT16  usDummy;
            //if we have updated the selected class token by Mode change(WOW/W1P to Scale mode),
            // restore it.
//            if(fUpdateSelectedClassByModeChange ==TRUE)
//            {
//               fnRestoreMasterContexForRevertWOW();
//               fUpdateSelectedClassByModeChange = FALSE;
//            }

//            chRateStatus = fnRateWOWWeight( &usNext) ;
            /* if the rerate fails, restore the last rating configuration and go
                back to the main screen (should use next screen 2 instead of GetScreen
                in a future release).  The "select class" disabling condition should
                come up. */
/*
             if(chRateStatus != (UINT8) RSTATUS_OK)
            {
                fnRestoreMasterContexForRevertWOW();
                fForceReRate = TRUE;
                fNeedStartMailRun = TRUE;
                usNext = GetScreen(SCREEN_MM_INDICIA_READY);
                return( usNext );
            }
*/

        }
        /* postage value may have changed, and this function may cause us to go directly
            to a printing screen, therefore we must make sure the correct indicia is
            selected */
        if( fnValidData((UINT16)BOBAMAT0, (UINT16)VAULT_DEBIT_VALUE, 
                        (void *) &ulPostageValue) != (UINT8)BOB_OK )
        {
            fnProcessSCMError();
            return( usNext );
        }
        
    }
    return( usNext );

}

/* Event functions */
/* *************************************************************************
// FUNCTION NAME: fnevMailJobCompleted
// DESCRIPTION: Event function that is triggered after the mail printing finished.
//
// INPUTS:  Standard event function inputs format 2.
// NEXT SCREEN:  MMIndiciaReady.
//
// MODIFICATION HISTORY:
//  08/27/2007  Oscar Wang      Update for case ONLY manifest prompt is ON
//  08/23/2007  Vincent Yi      Updated for GTS + diff. wt mode
//  08/15/2007  Oscar Wang      Check if any error occurs for Germany GTS Non-Stop mode, 
//                              if so, go to corresponding screen.
//  06/21/2007  Oscar Wang      Fix FRACA GMSE00121606. Added flag fDWPiecePrintedForDM400C to 
//                              see if mail piece is really printed for DM400.
//              Tim Zhang / Victor Li      Initial version
//

// *************************************************************************/
T_SCREEN_ID fnevMailJobCompleted (UINT8          bNumNext, 
                                  T_SCREEN_ID  * pNextScreens, 
                                  INTERTASK    * pIntertask)
{
    UINT8   pPlatformWeight[LENWEIGHT] ;
    UINT8   pZeroWeight[LENWEIGHT] = {0};
    UINT8   bPlatformStatus;


    //fnDateDuckOff();
    T_SCREEN_ID usNext = pNextScreens[ 1 ];	// MMIndiciaReady



	if (fnIsMultiPagePrintingStopped() == TRUE)
	{
		// Multi-page printing paused due to midnight crossing,
		fnSetMultiPagePrintingStopped (FALSE);
		
		// to ErrMidnightAdvDateConfirm screen
		usNext = fnevInvokeErrScreen (bNumNext, pNextScreens, pIntertask);
	}
	else
	{
	    usNext = fnProcessMailJobCompleted();
	}
	
    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnevMailPieceCompleted 
//
// DESCRIPTION: 
//      Event function to handle EVENT_MAIL_PIECE_COMPLETE, specially for Diff. 
//      Weighing
//
// INPUTS:
//      Standard event function inputs. (format 2)
//
// RETURNS:
//      No screen change/Home screen.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/23/2007  Vincent Yi      Updated for GTS + diff. wt
//  06/21/2007  Oscar Wang      Fix FRACA GMSE00121606. Added flag fDWPiecePrintedForDM400C to 
//                              see if mail piece is really printed for DM400.
//	07/26/2006	Derek DeGennaro	Fix for Diff Weigh Mode on DM400C (see comments at top of file).
//  07/18/2006  Oscar Wang      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevMailPieceCompleted (UINT8          bNumNext, 
                                     T_SCREEN_ID  * pNextScreens, 
                                     INTERTASK    * pIntertask)
{
    fnProcessMailPieceCompleted();
    return 0;   // Should be no screen transition
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnevTapePieceCompleted
//
// DESCRIPTION: 
// 		Event function to update the tape counter once the event 
//		EVENT_MAIL_PIECE_COMPLETED  occurs.
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		ucPrintTapeSeqNo	- sequence number of the tape to print
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	11/11/2005	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevTapePieceCompleted(UINT8 			bNumNext, 
                                   T_SCREEN_ID  *	pNextScreens, 
                                   INTERTASK 	*	pIntertask)
{   
    // Update tape count
    if (ucPrintTapeSeqNo < ucTotalTapesToPrint)
    {
        ucPrintTapeSeqNo++;
    }

    fnProcessMailPieceCompleted();

    return 0;   // Should be no screen transition
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnevTapeJobCompleted
//
// DESCRIPTION: 
// 		Event function when the event EVENT_MAIL_PIECE_COMPLETED  occurs.
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  11/04/2009  Raymond Shen    Fixed GMSE00173925 (G910/DM300c - Meter should display "Tape 
//                              Printing Paused" screen when press stop key in Date Correction Mode.).
//  07/28/2009  Raymond Shen    Added code to support "Revert to WOW" after printing Tape for DM475. 
//  08/27/2007  Oscar Wang      Update for case ONLY manifest prompt is ON
//  08/15/2007  Oscar Wang      Check if any error occurs for Germany GTS Non-Stop mode, 
//                              if so, go to corresponding screen.
//  05/28/2007  I. Le Goff      Exit ajout if we are done printing all the tapes.
//  15 May 2007 Bill Herring    Exit ajout if in ajout mode
//  10 Apr 2007 Bill Herring    Removed unnecessary ZWZP code
//  03/05/2007  I. Le Goff      Add modifications for mode ajout
//	01/08/2007	Vincent Yi		Moved ZWZP code into real printing-complete branch
//  12/22/2006  Raymond Shen    Added support for ZWZP
//  07/12/2006  Oscar Wang      Added support for Diff Weighing
// 	11/11/2005	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevTapeJobCompleted (UINT8 			bNumNext, 
                                      T_SCREEN_ID  *	pNextScreens, 
                                      INTERTASK 	*	pIntertask)
{
    T_SCREEN_ID   usNext = pNextScreens[2]; // Home
    UINT8   pPlatformWeight[LENWEIGHT] ;
    UINT8   pZeroWeight[LENWEIGHT] = {0};
    UINT8   bPlatformStatus;
    UINT8   ucCurrentPrintMode = fnOITGetPrintMode();


    // Revert back to WOW if needed.
    (void)fnhkCheckHybridMode(0, bNumNext, pNextScreens);
    


	if (cMultiTapePrintingStopped == MULTI_TAPE_MANUAL_STOPPED)
	{
		// Multi-tape printing paused
		cMultiTapePrintingStopped = MULTI_TAPE_NOT_STOPPED;
		
		// to WrnTapePrintingPaused screen
		pNextScreens[1] = pNextScreens[0];
		usNext = fnevInvokeErrScreen (bNumNext-1, pNextScreens+1, pIntertask);
	}
	else if (cMultiTapePrintingStopped == MULTI_TAPE_MIDNIGHT_STOPPED)
	{
		// Multi-tape printing paused
		cMultiTapePrintingStopped = MULTI_TAPE_NOT_STOPPED;
		
		// to ErrMidnightAdvDateConfirm screen
		usNext = fnevInvokeErrScreen (bNumNext-1, pNextScreens+1, pIntertask);
	}
	else if ( (bNumNext > 3) &&
	                ( (ucCurrentPrintMode == PMODE_DATE_CORRECTION) ||
	                (ucCurrentPrintMode == PMODE_MANIFEST) ||
	                (ucCurrentPrintMode == PMODE_POSTAGE_CORRECTION)))
	{
	    // Tape printing is successfully completed in correction/ manifest mode. 
	    usNext = pNextScreens[3]; // MMCorrectionPrintingComplete
	}
	else
	{
	    usNext = fnProcessMailJobCompleted();
	}

	if (usNext == GetScreen (SCREEN_MM_INDICIA_READY))
	{
	    usNext = 0;
//	    fnRateMailJobCompleteCheck();
		
		fnSetMailPieceCompleted(FALSE); 
	    bTapePrintStatus = TAPE_PRINT_COMPLETED;
	    OSStartTimer(SHORT_TICK_TIMER);
	    ucTapeShortTimerCount = 0;
	}

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnevSetMarginFailed
//
// DESCRIPTION: 
// 		Event function when the event EVENT_SET_MARGIN_FAILED  occurs.
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	2/07/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevSetMarginFailed (UINT8 			bNumNext, 
                                 T_SCREEN_ID  *	pNextScreens, 
                                 INTERTASK 	*	pIntertask)
{
    T_SCREEN_ID	usNext = 0;

    // Restore the old value
    usTmpRightMarginPos = usOldRightMarginPos;
    fnCmosSetRightMargin(usTmpRightMarginPos);
    // Clear the entry buffer
    fnClearEntryBuffer();
    fnSetEntryErrorCode(ENTRY_INVALID_VALUE);

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnevSendStartRequest
//
// DESCRIPTION: 
// 		Event function that send Start request to SYS in ReadyTransient screen.
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	02/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevSendStartRequest (UINT8 			bNumNext, 
                                  T_SCREEN_ID  *	pNextScreens, 
                                  INTERTASK 	*	pIntertask)
{
    T_SCREEN_ID	usNext = 0;

    if (ucPrintMediaType == MEDIA_MAIL)
    {
        fnSendStartMailRequest(ucPagesToPrint);	
    }
    else if (ucPrintMediaType == MEDIA_TAPE)
    {
        fnSendStartTapeRequest(ucTapesToPrint);
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnevSendRateReady
//
// DESCRIPTION: 
// 		Event function that send Rate ready request to oit in Ready screen
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/31/2007  Joey Cui        Check the rerate flag, if it is true, rerate
// 	03/23/2006	Sunny Liao		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevSendRateReady (UINT8 			bNumNext, 
                                  T_SCREEN_ID  *	pNextScreens, 
                                  INTERTASK 	*	pIntertask)
{
    T_SCREEN_ID	usNext = 0;
    UINT8	bPrintMode;
    
    bPrintMode = fnOITGetPrintMode();  
    
//    if ((bPrintMode == PMODE_DIFFERENTIAL_WEIGH)&&
//                    (bFromRemoveItem == TRUE) &&
//                   (fnIsClassSelected() == true))
//    {
//        //Send message to CM when Rate is ready.
//         OSSendIntertask(CM, OIT, OC_RATE_READY, NO_DATA, NULL, 0);
//        //	 fnSetMessageTimeout(CM, CO_RATE_READY_RSP, STANDARD_INTERTASK_TIMEOUT);
//         bFromRemoveItem = FALSE;
//
//    }

    /* in some cases, weight update(which will set rerate flag) works later than
       checking rarate flag in the pre function, and weight update event is ignored
       during print mode switch (so, event function is ignored too).
        1. power up
        2. change weight during sleep
       so, we re check flag here, if needed, rerate */
    if ((fForceReRate)
            && (bPrintMode == PMODE_PLATFORM))
    {
//        fnRerateDelayedWeight(&usNext);
        fForceReRate = false;
    }
        
    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnevContinueToPrintMail
//
// DESCRIPTION: 
// 		Event function that continues to print mail.
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		If ucTotalPagesToPrint >= ucPrintPageSeqNo, to ready transient screen
//		else, to next screen 1 
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	03/16/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevContinueToPrintMail (UINT8 			bNumNext, 
                                     T_SCREEN_ID  *	pNextScreens, 
                                     INTERTASK 	*	pIntertask)
{
    T_SCREEN_ID		usNext = 0;

    if (fnIsCmPaperError() == TRUE 	||
        fnIsCmTapeError() == TRUE	||
        fnIsCmFeederError() == TRUE)
    {
        fnClearCMPaperErrorStatus();
    }
    
    if (fPiecesNumAppointed == TRUE)
    {	// appointed piece number
        if (ucTotalPagesToPrint >= ucPrintPageSeqNo)
        {
            if( fnIsWOWMailJammed() == TRUE )
            {
                usNext = pNextScreens[1];
            }

            usNext = fnStartToPrintMail (ucTotalPagesToPrint - ucPrintPageSeqNo + 1, 
                                     bNumNext, pNextScreens, FALSE, TRUE);
        }
        else
        {
            usNext = fnRetFromErrScreen (bNumNext, pNextScreens);
        }
    }
    else
    {	// no appointed piece number
        if( fnIsWOWMailJammed() == TRUE )
        {
            usNext = pNextScreens[1];
        }
        else
        {
            usNext = fnStartToPrintMail (0, bNumNext, pNextScreens, FALSE, FALSE);
        }
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnevContinueToPrintTape
//
// DESCRIPTION: 
// 		Event function that continues to print tape.
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		If ucTotalTapesToPrint >= ucPrintTapeSeqNo, to ready transient screen
//		else, to next screen 1 
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	03/07/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevContinueToPrintTape (UINT8 			bNumNext, 
                                     T_SCREEN_ID  *	pNextScreens, 
                                     INTERTASK 	*	pIntertask)
{
    T_SCREEN_ID		usNext = 0;

    if (fnIsCmPaperError() == TRUE 	||
        fnIsCmTapeError() == TRUE	||
        fnIsCmFeederError() == TRUE)
    {
        fnClearCMPaperErrorStatus();
    }
    
    
    if (ucTotalTapesToPrint >= ucPrintTapeSeqNo)
    {
        usNext = fnStartToPrintTape (ucTotalTapesToPrint - ucPrintTapeSeqNo + 1, 
                                     bNumNext, pNextScreens, FALSE);
    }
    else
    {
        usNext = fnRetFromErrScreen(bNumNext, pNextScreens);
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnevContinueToPrint
//
// DESCRIPTION: 
// 		Event function that continues to print mail or tape according to previous
//		screen's mode.
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		 
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	07/24/2006	Vincent Yi		Added ucResumePrintEvent to make sure the previous 
//								print flow can be resumed correctly
// 	03/17/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevContinueToPrint (UINT8 			bNumNext, 
                                 T_SCREEN_ID  *	pNextScreens, 
                                 INTERTASK 	*	pIntertask)
{
    T_SCREEN_ID		usNext = 0;	
//	UINT8			ucMode = 0;
//	UINT8			ucSubMode = 0;
    UINT8           ucEvent;

//	fnGetScreenModeById (fnGetPreviousScreenID(), &ucMode, &ucSubMode);
    fnGetScreenEventById(fnGetPreviousScreenID(), &ucEvent);

    if (ucEvent == OIT_LEFT_PRINT_RDY 		|| 
        ucEvent == OIT_ERROR_METER			||
        ucEvent == OIT_ENTER_POWERUP		||
        ucEvent == OIT_GOTO_SLEEP			||
        ucEvent == OIT_ENTER_POWERDOWN		||
        ucEvent == OIT_ENTER_DIAG_MODE)
    {
        ucEvent = ucResumePrintEvent;
    }

//	if (ucMode == OIT_RUNNING)
    if ((ucEvent == OIT_PRINTING)       || 
        (ucEvent == OIT_RUNNING_PERMIT) || 
        (ucEvent == OIT_RUNNING_SEAL)   || 
        (ucEvent == OIT_RUNNING_REPORT) ||
        (ucEvent == OIT_RUNNING_TEST))
    {
        usNext = fnevContinueToPrintMail(bNumNext, pNextScreens, pIntertask);	
    }
//	else if (ucMode == OIT_PRINTING_TAPE)
    else if ((ucEvent == OIT_PRINT_TAPE)        ||
             (ucEvent == OIT_PRINT_TAPE_PERMIT)	|| 
             (ucEvent == OIT_PRINT_TAPE_TEST)   || 
             (ucEvent == OIT_PRINT_RPT_TO_TAPE))
    {
        usNext = fnevContinueToPrintTape(bNumNext, pNextScreens, pIntertask);
    }
    else
    {
        // ??? impossible
    }

    return 	usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnevMidnightComeInPrintingMail
//
// DESCRIPTION: 
// 		Event function that during printing mail, midnight crossover will happen soon.
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		 
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	01/08/2007	Vincent Yi		Removed the conditions for stopping print flow
// 	12/13/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevMidnightComeInPrintingMail (UINT8 			bNumNext, 
                                          	T_SCREEN_ID *	pNextScreens, 
                                          	INTERTASK 	*	pIntertask)
{
    T_SCREEN_ID	usNext = 0;
	BOOL		fStop = FALSE;

	fMultiPagePrintingStopped = TRUE;		
    fnSendStopMailRequest();

/*	
	if (fnOITGetBaseType() == OIT_BASE_400C)
	{	// DM400C
		fStop = TRUE;
		if (ucTotalPagesToPrint > 1	&& 
			ucTotalPagesToPrint == ucPrintPageSeqNo)
		{	
			fStop = FALSE;
		}
		if (fStop == TRUE)
		{
			fMultiPagePrintingStopped = TRUE;		
		    fnSendStopMailRequest();
		}
	}
	else
	{	// DM300C
		if (ucTotalPagesToPrint > 1)
		{
			if (ucTotalPagesToPrint > ucPrintPageSeqNo)
			{
				fMultiPagePrintingStopped = TRUE;		
			    fnSendStopMailRequest();
			}
		}
		else
		{
			fMultiPagePrintingStopped = TRUE;		
		    fnSendStopMailRequest();
		}
	}
*/

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnevMidnightComeInPrintingTape
//
// DESCRIPTION: 
// 		Event function that during printing tape, midnight crossover will happen soon.
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		 
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	01/08/2007	Vincent Yi		Changed the conditions for stopping print flow
// 	12/13/2006	Vincent Yi		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevMidnightComeInPrintingTape (UINT8 			bNumNext, 
                                          	T_SCREEN_ID *	pNextScreens, 
                                          	INTERTASK 	*	pIntertask)
{
    T_SCREEN_ID	usNext = 0;
	BOOL		fStop = TRUE;

	if (ucTotalTapesToPrint > 1					&&
		ucTotalTapesToPrint == ucPrintTapeSeqNo	&&
		fnIsMailPieceCompleted() == TRUE)
	{
		fStop = FALSE;
	}

	if (fStop == TRUE)
	{
		cMultiTapePrintingStopped = MULTI_TAPE_MIDNIGHT_STOPPED;		
	    fnSendStopTapeRequest();
	}

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnevCheckScreenReentry
//
// DESCRIPTION: 
// 		Event function that check Main screen reentry condition.
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		If condition satisfied, enter into Main screen again.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	12/15/2006	Raymond Shen		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevCheckScreenReentry (UINT8 			bNumNext, 
                                  T_SCREEN_ID  *	pNextScreens, 
                                  INTERTASK 	*	pIntertask)
{
    T_SCREEN_ID	usNext = 0;
    
    if( bNumNext > 0 )
    {
        usNext = pNextScreens[0];
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnevUpdatePrinterState
//
// DESCRIPTION: 
// 		Event function that update current printer's state to OIT.
//
// INPUTS:
//		Standard event function inputs.	(format 2)
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Do not change screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	11/28/2008	Raymond Shen		Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevUpdatePrinterState (UINT8 			bNumNext, 
                                  T_SCREEN_ID  *	pNextScreens, 
                                  INTERTASK 	*	pIntertask)
{
    UINT16  usNext = 0;
    UINT8   bPaperHandlingStatus;

    bPaperHandlingStatus = pIntertask->IntertaskUnion.bByteData[OIT_PROG_MAILRUN_STATUS];

    /* set the mode global */
    switch (bPaperHandlingStatus)
    {
        case WOW_REZERO_DONE:
            bCurrentPrintState = PSTATE_PREPARING_TO_PRINT;
            break;
        case IS_RUNNING:
            bCurrentPrintState = PSTATE_PRINTING;
            break;
        case WOW_REZEROING:
            bCurrentPrintState = PSTATE_REZEROING_WOW;
            break;
        case PERFORM_MAINTENANCE:
            bCurrentPrintState = PSTATE_PERFORMING_MAINT;
            break;
        case WAITING_FOR_MAIL:
            bCurrentPrintState = PSTATE_WAITING_FOR_ENV;
            break;
        default:
            break;
    }

    return 0;
}

/* Variable graphic functions */



/**********************************************************************
        Private Functions
**********************************************************************/

/* *************************************************************************
// FUNCTION NAME: 
//		fnSendStartMailRequest
//
// DESCRIPTION: 
// 		Send request message to SYS to start mail run, only for DM400C
//
// INPUTS:
//		ucPages	- 	total pages to print
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	2/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static void fnSendStartMailRequest (UINT8	ucPages)
{
    UINT8		pMsgData[2];

    // Send SYS the message to request to start mail run
    pMsgData[0] = MEDIA_MAIL;
    pMsgData[1] = ucPages;

    OSSendIntertask (SYS, OIT, OIT_START_MAIL_REQ, BYTE_DATA, pMsgData, 
                        sizeof(pMsgData));
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnSendStartTapeRequest
//
// DESCRIPTION: 
// 		Send request message to SYS to start tape run
//
// INPUTS:
//		ucTapes	- 	total tapes to print.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	2/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static void fnSendStartTapeRequest (UINT8	ucTapes)
{
    UINT8		pMsgData[2];

    // Send SYS the message to request to start tape run
    pMsgData[0] = MEDIA_TAPE;
    pMsgData[1] = ucTapes;

    OSSendIntertask (SYS, OIT, OIT_START_MAIL_REQ, BYTE_DATA, pMsgData, 
                        sizeof(pMsgData));
}	

/* *************************************************************************
// FUNCTION NAME: 
//		fnSendStopMailRequest
//
// DESCRIPTION: 
// 		Send request message to SYS to stop mail run
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	2/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
void fnSendStopMailRequest (void)
{
    UINT8		pMsgData[1];

    // Send SYS the message to request to stop mail run
    pMsgData[0] = MEDIA_MAIL;

    OSSendIntertask (SYS, OIT, OIT_STOP_MAIL_REQ, BYTE_DATA, pMsgData, 
                        sizeof(pMsgData));
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnSendStopTapeRequest
//
// DESCRIPTION: 
// 		Send request message to SYS to stop tape run
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	2/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static void fnSendStopTapeRequest (void)
{
    UINT8		pMsgData[1];

    // Send SYS the message to request to stop mail run
    pMsgData[0] = MEDIA_TAPE;

    OSSendIntertask (SYS, OIT, OIT_STOP_MAIL_REQ, BYTE_DATA, pMsgData, 
                        sizeof(pMsgData));
}


/* *************************************************************************
// FUNCTION NAME: 
//		fnSendStopRequest
//
// DESCRIPTION: 
// 		Send request message to SYS to stop mail/tape run
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	08/14/2007	Oscar Wang		Initial version
//
// *************************************************************************/
void fnSendStopRequest(void)
{
    UINT8		ucMode = 0;
    UINT8		ucSubMode = 0;

    fnGetSysMode(&ucMode, &ucSubMode);
    
    // Send SYS the message to request to stop mail run
    if(ucMode == SYS_RUNNING)
    {
        fnSendStopMailRequest();
    }
    else if(ucMode == SYS_PRINTING_TAPE)
    {
        fnSendStopTapeRequest();
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnProcessMailPieceCompleted
//
// DESCRIPTION: 
// 		Utility function to process after mail piece completed
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	08/31/2007	Vincent Yi		Initial version
//
// *************************************************************************/
static void fnProcessMailPieceCompleted (void)
{
    if (fnOITGetPrintMode() == PMODE_DIFFERENTIAL_WEIGH)
    {
        fDWPiecePrinted = TRUE;    
    }

    //To Stop mail run
    if((CMOSTrmUploadDueReqd & TRM_UPLOAD_REQUIRED) == TRM_UPLOAD_REQUIRED)
    {
    	bGoToReady = FALSE;
    	fnChangeScreen(GetScreen (SCREEN_MM_INDICIA_READY), TRUE, TRUE);
    }
}


/* *************************************************************************
// FUNCTION NAME: 
//		fnProcessMailJobCompleted
//
// DESCRIPTION: 
// 		Utility function to process after mail job completed
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  10/12/2009  Jingwei,LI      Clear label printed flag if mail job completed.
//  00/11/2009  Raymond Shen    Fix a Canada package service problem on the condition that 
//                              all the confirmation prompts are turned off.
//  10/06/2008  Oscar Wang      Fix Holland problem.
//  10/24/2007  Oscar Wang      Replace fnhkModeSetDefault() with fnRatingSetDefault().
//  09/25/2007  Vincent Yi      Added condition-check on fnIsGTSPieceProcessed()
// 	08/31/2007	Vincent Yi		Initial version
//
// *************************************************************************/
static T_SCREEN_ID fnProcessMailJobCompleted(void)
{
    T_SCREEN_ID usNext = GetScreen (SCREEN_MM_INDICIA_READY);
    T_SCREEN_ID usNextPrompt;
	unsigned short usCountry = fnGetDefaultRatingCountryCode();
    UINT8       ucCurrentPrintMode = fnOITGetPrintMode();


	// When a mailpiece is inserted at the same time as the transport running times out,
	// ignore "paper in transport" error to not report jam error on tablet and not go to 
	// screen SCREEN_MM_INDICIA_ERROR on base until maintenance is finished 
    if (fnIsCmPaperInTransport() == TRUE && fSkipPaperInTranport == FALSE)
    {
    	bGoToReady = FALSE;
    	fnClearCMPaperErrorStatus();
    	usNext = GetScreen (SCREEN_MM_INDICIA_ERROR);

    }


    if (fnIsDWPiecePrinted() == TRUE)
    {
		// Normal diff wt mode
        usNext = GetScreen (SCREEN_MM_INDICIA_READY);
    }

    if (fnIsDWPiecePrinted() == TRUE)
    {
		// Any diff. wt mode, no matter with GTS or not
        fnDWPostMailPiecePrinted();
        fDWPiecePrinted = FALSE;
//        fnRateMailJobCompleteCheck();
    }
    
    return usNext;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnModeChangeNeedClassScreen
//
// DESCRIPTION: 
//          Checks if the next screen from a weighing mode should be 
//          the select class screen. This is used when KIP Carrier is
//          running and the class may be invalid.
//
// INPUTS:      
//           None
//
// RETURNS:
//          Returns True if next screen = class
//          else false     
//
// NOTES: 
//
//
// MODIFICATION HISTORY:
//     11/18/2008   Jingwei,Li    Reused from mega.
//      
// *************************************************************************/
UINT8 fnModeChangeNeedClassScreen(void)
{
    UINT8   bResult = 0;
    UINT8   ucKIPClassControl;
    
    ucKIPClassControl = fnFlashGetByteParm( BP_KEY_IN_POSTAGE_CLASS );    
    if( ucKIPClassControl != KIP_CLASS_NOT_ALLOWED )
    {
/*
        if (fnRateIsCurrentCarrierBranded()== RSSTS_CARRIER_KIP_CARRIER)          // Carrier KIP Carrier ?
        {
            bResult = 1;                                    // 1 = carrier

            if (fnFlashGetByteParm(BP_DEFAULT_CARRIER_OR_CLASS))
            {
                bResult = 2;                                // 2 = class
            }
        }
*/
    }
    return (bResult);
}
/* *************************************************************************
// FUNCTION NAME: 
//      fnOITSetPrintModeHybrid
//
// DESCRIPTION: 
//         Sets the print mode as a hybrid mode.  A hybrid mode means that
//         we have changed to a new mode, but may do an automatic mode
//         change to another mode when the start key is pressed.  This function
//         saves all necessary data needed to do this.
//
// INPUTS:      
//          bPrintMode - Print Mode to set to.
//
// RETURNS:
//          None
//
//   NOTES: 
//
//
// MODIFICATION HISTORY:
//      05/03/2009    Jingwie,Li         Modified for revert WOW.
//      12/01/2008    Jingwei,Li            Initial version.
//      
// *************************************************************************/
void fnOITSetPrintModeHybrid(UINT8 bPrintMode)
{
	if ( fnCMOSIsHybridModeRevertToWOW() == TRUE )
	{

		/* save the previous mode if needed */
		if ( (bCurrentPrintMode != PMODE_SEAL_ONLY) 
		&& (bCurrentPrintMode != PMODE_TIME_STAMP) )
		{
			bPreviousNonSealMode = bCurrentPrintMode;
		}

		if ((bCurrentPrintMode == PMODE_WOW) || (bCurrentPrintMode == PMODE_WEIGH_1ST))
		{
			fHybridMode = TRUE;
			bHybridRestoreMode = bCurrentPrintMode;
		}

		//bCurrentPrintMode = bPrintMode;
		fDiffWModePending = FALSE;
		fnOITSetPrintMode(bPrintMode);

	}
	else  // No Revert to WOW Set Mode Permanently - MNH 8/2004
	{
		fnOITSetPrintMode( bPrintMode );
	}
	return;
}
/* *************************************************************************
// FUNCTION NAME: 
//      fnIsWOWDREnabled
//
// DESCRIPTION: 
//     check if currently select class is auto-switched class
//
// INPUTS:      
//      void.
//
// RETURNS:
//          TRUE, if current class is auto-switched
//          FALSE, otherwise
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      05/20/2009  Jingwei,Li    Check whether length calibration is done.
//      04/30/2009  Jingwei,Li    Initial version
// *************************************************************************/
BOOL fnIsWOWDREnabled( void)
{
    BOOL bRet = FALSE;
/*    if(( fnIsWOWInstalled()== TRUE) &&
       (fnIsLengthCalibrateDone() == TRUE) &&
       (fnRateIsCurrentClassAutoSwitch()== RSSTS_AUTO_SWITCH_CLASS))
    {
        bRet = TRUE;
    }*/
    return bRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnSetMultiTapeFlag
//
// DESCRIPTION: 
// 		Set Multi-Tape flag to fix FRACA GMSE00161089
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	10/15/2009	Raymond Shen	Add print mode restriction before setting the flag.
// 	05/13/2009	Fred Xu		Initial version
//
// *************************************************************************/
void fnSetMultiTapeFlag(BOOL fOK)
{
    UINT8   ucPrintMode = fnOITGetPrintMode();

    // Only following mode need abacus static mailrun information
    if ((ucPrintMode == PMODE_MANUAL)||
            (ucPrintMode == PMODE_WOW) ||
            (ucPrintMode == PMODE_WEIGH_1ST) ||
            (ucPrintMode == PMODE_MANUAL_WGT_ENTRY) ||
            (ucPrintMode == PMODE_PLATFORM) ||
            (ucPrintMode == PMODE_POSTAGE_CORRECTION) ||
            (ucPrintMode == PMODE_DIFFERENTIAL_WEIGH) ||
            (ucPrintMode == PMODE_AJOUT_MANUAL_WGT_ENTRY) ||
            (ucPrintMode == PMODE_AJOUT_PLATFORM) ||
            (ucPrintMode == PMODE_MANIFEST) )
    {
        fMultiTapeOK = fOK;
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnIsMultiTapeOK
//
// DESCRIPTION: 
// 		Get Multi-Tape flag to fix FRACA GMSE00161089
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	05/13/2009	Fred Xu		Initial version
//
// *************************************************************************/
BOOL fnIsMultiTapeOK()
{
    return fMultiTapeOK;
}


