//=====================================================================
//
//	FileName:	HAL_Feeder.c
//
//	Description: Hardware abstraction layer implementation for Feeder interface
//=====================================================================
#include "commontypes.h"
#include "stdio.h"
#include "stddef.h"
#include "hal.h"
#include "am335x_pbdefs.h"
#include "HAL_Feeder.h"
#include "nucleus_gen_cfg.h"

extern void HALInitGPIOPins(P_GPIO_INFO pGPIOTable, unsigned numPinsUsed);

// Global variables

// The order of the entries in the following table must match the order of the entries in
// the GPIO_INFO enum in HAL_Feeder.h
// After a reset, a pin configured for output will output a 0.

// pins should match schematic
static GPIO_INFO CSD_GPIO_Table[] = {
		{AM335X_CTRL_PADCONF_GPMC_A8, AM335X_GPIO_BIT_24, GPIO_BANK_1, DIR_INPUT, GPIO_PULL_UP}, // FS1
		{AM335X_CTRL_PADCONF_GPMC_A6, AM335X_GPIO_BIT_22, GPIO_BANK_1, DIR_INPUT, GPIO_PULL_UP}, // FS2
		{AM335X_CTRL_PADCONF_GPMC_CSN1, AM335X_GPIO_BIT_30, GPIO_BANK_1, DIR_INPUT, GPIO_PULL_UP}, // FEEDER_COVER
		{AM335X_CTRL_PADCONF_GPMC_A9, AM335X_GPIO_BIT_25, GPIO_BANK_1, DIR_OUTPUT, GPIO_PULL_OFF} // FEEDM_ENB
};
static const unsigned numPinsUsed = sizeof(CSD_GPIO_Table) / sizeof(GPIO_INFO);
static P_GPIO_INFO pGPIOTable = &CSD_GPIO_Table[0];




/* **********************************************************************
// DESCRIPTION: Init Feeder Interface.
//
// INPUTS:
// RETURNS:
// **********************************************************************/
void HALInitFeederInterface(void)
{
	//Init GPIO pins used to interface with Panel
	HALInitGPIOPins(pGPIOTable, numPinsUsed);

	// default output of 0 for FEEDM_ENB disables feeder motor

	//TODO - PWM0 init

	//TODO - EQEP1 init

	//TODO - Feeder temperature input init

	//TODO - interrupt init GPIO inputs
}

//TODO - Feeder control functions

