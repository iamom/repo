/*************************************************************************
   PROJECT:        Janus
   COMPANY:        Pitney Bowes
   AUTHOR :        Sandra Peterson
   MODULE NAME:    permits.c
       
   DESCRIPTION:    Permit array functions

* ----------------------------------------------------------------------
*               Copyright (c) 1998 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
 REVISION HISTORY:

 2008.05.16  Clarisa Bellamy    FPHX 1.11
  Changes for graphics module update:
  - Modified DeleteMissingPermits to use one of the new Grfx routines directly
    instead of calling a private bob function.

* -----------------------------------------------------------------------
*   REVISION HISTORY:
*   $Log:   H:/group/SPARK/ARCHIVES/DM150/uic_app/permits.c_v  $
 * 
 *    Rev 1.4   12 May 2004 14:06:36   CR002DE
 * 1. added function DeleteMissingPermits.
 * 
 *    Rev 1.3   07 May 2004 14:19:54   SA002PE
 * Fixed the check of the permit index in several places.
 * 
 *    Rev 1.2   21 Apr 2004 18:59:06   SA002PE
 * Fixed 4 calls to unistrcpy that are supposed to be
 * calls to unistrcmp.
 * 
 *    Rev 1.1   21 Apr 2004 18:23:08   SA002PE
 * Changed ClearPermit, GetPermitByName & 
 * SetPermitType to do the default processing
 * for a null pointer instead of a null string.
 * 
 *    Rev 1.0   20 Apr 2004 20:00:44   SA002PE
 * Permit array access functions
* 
*************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "custdat.h"
#include "permits.h"
#include "unistring.h"
#include "bobutils.h"

extern PERMIT_ARRAY CMOSPermitArray;

// Internal contants

// Internal variables and structures

// prototypes for internal subroutines


/*****************************************************************************
// FUNCTION NAME: ClearPermitBatch
// DESCRIPTION: Clears the batch count associated with the given permit name.
//              If a null pointer is given, the batch count for the
//              currently selected permit will be cleared
//
// AUTHOR: Sandra Peterson
//
// INPUTS: pwPermitName = pointer to the permit name
//
// OUTPUTS: TRUE = OK
//          FALSE = permit doesn't have a batch count
*****************************************************************************/

BOOL ClearPermitBatch(unsigned short *pwPermitName)
{
    unsigned char bInx;
    BOOL    fbReturnValue = TRUE;


    // if a non-null pointer was given and a name was given, 
    // see if the name is in the array
    if (pwPermitName)
    {
        if (pwPermitName[0])
        {
            for (bInx = 0; bInx < CMOSPermitArray.bMaxPermitsByPcn; bInx++)
            {
                if (!unistrcmp(CMOSPermitArray.sPermitArray[bInx].wPermitName, pwPermitName))
                    break;
            }

            // if not in the array, return an error
            // else, clear the batch count
            if (bInx == CMOSPermitArray.bMaxPermitsByPcn)
                fbReturnValue = FALSE;
            else
                CMOSPermitArray.sPermitArray[bInx].lPermitBatchCount = 0;
        }
        else
            fbReturnValue = FALSE;
    }
    else    // working with the currently selected permit
    {
        // the index should be OK, but check just to make sure.
        // if it isn't, someone forgot to call SetPermitIndex
        if ((CMOSPermitArray.bIndex > 0) && 
            (CMOSPermitArray.bIndex <= CMOSPermitArray.bMaxPermitsByPcn))
            CMOSPermitArray.sPermitArray[CMOSPermitArray.bIndex-1].lPermitBatchCount = 0;
        else
            fbReturnValue = FALSE;
    }

    return(fbReturnValue);
}


/*****************************************************************************
// FUNCTION NAME: DeletePermit
// DESCRIPTION: Deletes the indicated permit from the array
//              The caller should first call GetPermitDataByName to see if the
//              permit has a batch count, and whether or not it equals zero
//
// AUTHOR: Sandra Peterson
//
// INPUTS: pwPermitName = pointer to the permit name
//
// OUTPUTS: None
*****************************************************************************/

void DeletePermit(unsigned short *pwPermitName)
{
    unsigned char bInx;


    // if a non-null pointer was given and a name was given, 
    // see if the name is in the array
    if ((pwPermitName) && (pwPermitName[0]))
    {
        for (bInx = 0; bInx < CMOSPermitArray.bMaxPermitsByPcn; bInx++)
        {
            if (!unistrcmp(CMOSPermitArray.sPermitArray[bInx].wPermitName, pwPermitName))
                break;
        }

        // if it is, clear the name and the batch count
        if (bInx != CMOSPermitArray.bMaxPermitsByPcn)
        {
            CMOSPermitArray.sPermitArray[bInx].wPermitName[0] = 0;
            CMOSPermitArray.sPermitArray[bInx].lPermitBatchCount = 0;
        }
    }
}

/*****************************************************************************
// FUNCTION NAME: DeleteMissingPermits
// DESCRIPTION: Deletes the all permits that have a batch counter assigned but
//              are not available in the graphics list anymore.
//
// AUTHOR: Craig DeFilippo
//
// INPUTS: none
//
// OUTPUTS: None
*****************************************************************************/

void DeleteMissingPermits(void)
{
    unsigned char bInx;
    UINT16  uwGenId;

    // scan through the permits and delete the ones that are no longer available
    for (bInx = 0; bInx < CMOSPermitArray.bMaxPermitsByPcn; bInx++)
    {
        uwGenId = fnGrfxGetPermitIdAndTypeByName( NULL_PTR, CMOSPermitArray.sPermitArray[bInx].wPermitName );
        if( uwGenId == 0 )
            DeletePermit( CMOSPermitArray.sPermitArray[bInx].wPermitName );
    }           
}


/*****************************************************************************
// FUNCTION NAME: GetNumberOfPermits
// DESCRIPTION: Returns the number of active permit batch counts in the array
//
// AUTHOR: Sandra Peterson
//
// INPUTS: None
//
// OUTPUTS: number of active permit batch counts
*****************************************************************************/

unsigned char GetNumberOfPermits(void)
{
    unsigned char bInx;
    unsigned char bCnt = 0;


    // count all the allowed array entries that have a name
    for (bInx = 0; bInx < CMOSPermitArray.bMaxPermitsByPcn; bInx++)
        if (CMOSPermitArray.sPermitArray[bInx].wPermitName[0])
            bCnt++;

    return (bCnt);
}


/*****************************************************************************
// FUNCTION NAME: GetPermitDataByName
// DESCRIPTION: Get the batch count for the given permit name.
//              If a null pointer is given, the batch count for the
//              currently selected permit will be retrieved
//
// AUTHOR: Sandra Peterson
//
// INPUTS: pwPermitName = pointer to the permit name
//         plCount = pointer where to store the batch count
//
// OUTPUTS: TRUE = OK
//          FALSE = permit doesn't have a batch count
*****************************************************************************/

BOOL GetPermitDataByName(unsigned short *pwPermitName, unsigned long *plCount)
{
    unsigned char bInx;
    BOOL    fbReturnValue = TRUE;


    // if a non-null pointer was given and a name was given, 
    // see if the name is in the array
    if (pwPermitName)
    {
        if (pwPermitName[0])
        {
            for (bInx = 0; bInx < CMOSPermitArray.bMaxPermitsByPcn; bInx++)
            {
                if (!unistrcmp(CMOSPermitArray.sPermitArray[bInx].wPermitName, pwPermitName))
                    break;
            }

            // if it isn't, set the returned batch count to zero & return an error
            // else, return the actual batch count
            if (bInx == CMOSPermitArray.bMaxPermitsByPcn)
            {
                *plCount = 0;
                fbReturnValue = FALSE;
            }
            else
                *plCount = CMOSPermitArray.sPermitArray[bInx].lPermitBatchCount;
        }
        else
            fbReturnValue = FALSE;
    }
    else    // working with the currently selected permit
    {
        // the index should be OK, but check just to make sure.
        // if it isn't, someone forgot to call SetPermitIndex
        if ((CMOSPermitArray.bIndex > 0) && 
            (CMOSPermitArray.bIndex <= CMOSPermitArray.bMaxPermitsByPcn))
            *plCount = CMOSPermitArray.sPermitArray[CMOSPermitArray.bIndex-1].lPermitBatchCount;
        else
        {
            *plCount = 0;
            fbReturnValue = FALSE;
        }
    }

    return(fbReturnValue);
}


/*****************************************************************************
// FUNCTION NAME: GetPermitDataByNum
// DESCRIPTION: Gets the batch count & permit name for the given index
//              if index is invalid or no permit data is stored at the given
//              index, the batch count is 0 and the name is a null terminator
//
// AUTHOR: Sandra Peterson
//
// INPUTS: bNum = index+1 into the array
//                  0 = currently selected permit
//                  1 thru MAX_PERMITS = specific batch count in the array
//         pwPermitName = pointer to a buffer of size MAX_PERMIT_UNICHARS+1
//
// OUTPUTS: batch count
*****************************************************************************/

unsigned long GetPermitDataByNum(unsigned char bNum, unsigned short *pwPermitName)
{
    unsigned long lCount;


    // if a number was given, make sure it's valid
    // if it is, return the actual batch count & name
    // else, return a zero batch count & a null name
    if (bNum)
    {
        if (bNum <= MAX_PERMITS)
        {
            unistrcpy(pwPermitName, CMOSPermitArray.sPermitArray[bNum-1].wPermitName);
            lCount = CMOSPermitArray.sPermitArray[bNum-1].lPermitBatchCount;
        }
        else
        {
            pwPermitName[0] = 0;
            lCount = 0;
        }
    }
    else    // working with the currently selected permit
    {
        // the index should be OK, but check just to make sure.
        // if it isn't, someone forgot to call SetPermitIndex
        if ((CMOSPermitArray.bIndex > 0) && 
            (CMOSPermitArray.bIndex <= CMOSPermitArray.bMaxPermitsByPcn))
        {
            unistrcpy(pwPermitName, CMOSPermitArray.sPermitArray[CMOSPermitArray.bIndex-1].wPermitName);
            lCount = CMOSPermitArray.sPermitArray[CMOSPermitArray.bIndex-1].lPermitBatchCount;
        }
        else
        {
            pwPermitName[0] = 0;
            lCount = 0;
        }
    }

    return (lCount);
}


/*****************************************************************************
// FUNCTION NAME: GetPermitPieceCount
// DESCRIPTION: Gets the permit piece count for the life of the mete
//              
// AUTHOR: Sandra Peterson
//
// INPUTS: None
//         
// OUTPUTS: permit piece count
*****************************************************************************/

unsigned long GetPermitPieceCount(void)
{
    return (CMOSPermitArray.lPermitPieceCount);
}

/*****************************************************************************
// FUNCTION NAME: IncrementPermitBatch
// DESCRIPTION: Increments the permit piece count and the batch count for
//              the currently selected permit.
//
// AUTHOR: Sandra Peterson
//
// INPUTS: None
//
// OUTPUTS: None
*****************************************************************************/

void IncrementPermitBatch(void)
{
    CMOSPermitArray.lPermitPieceCount++;

    // the index should be OK, but check just to make sure.
    // if it isn't, someone forgot to call SetPermitIndex
    if ((CMOSPermitArray.bIndex > 0) && 
        (CMOSPermitArray.bIndex <= CMOSPermitArray.bMaxPermitsByPcn))
    {
        CMOSPermitArray.sPermitArray[CMOSPermitArray.bIndex-1].lPermitBatchCount++;
    }
}


/*****************************************************************************
// FUNCTION NAME: SetPermitIndex
// DESCRIPTION: Sets the permit array index based on the given permit name
//
// AUTHOR: Sandra Peterson
//
// INPUTS: pwPermit = pointer to the permit name
//
// OUTPUTS: TRUE = OK
//          FALSE = permit doesn't have a batch count and there's no more
//                  room in the array
*****************************************************************************/

BOOL SetPermitIndex(unsigned short *pwPermitName)
{
    unsigned char bInx;
    unsigned char bAvailable = 0xff;
    BOOL    fbReturnValue = TRUE;


    // if a non-null pointer was given and a name was given, 
    // see if the name is in the array
    if (pwPermitName)
    {
        if (pwPermitName[0])
        {
            for (bInx = 0; bInx < CMOSPermitArray.bMaxPermitsByPcn; bInx++)
            {
                // if the array element has a name, check if it matches the given name
                if (CMOSPermitArray.sPermitArray[bInx].wPermitName[0])
                {
                    if (!unistrcmp(CMOSPermitArray.sPermitArray[bInx].wPermitName, pwPermitName))
                        break;
                }
                else    // found an available slot
                {
                    // if we haven't already found an available slot,
                    // save this one as the first available slot
                    if (bAvailable == 0xff)
                        bAvailable = bInx;
                }
            }

            // if we found the name, set the array index
            // else,
            //      if there is a slot available, assign this permit to the slot
            //      else, return an error
            if (bInx == CMOSPermitArray.bMaxPermitsByPcn)
            {
                if (bAvailable != 0xff)
                {
                    // set the array index to this slot, make sure the batch count is clear,
                    // and copy the given name into the array element
                    CMOSPermitArray.bIndex = bAvailable+1;
                    CMOSPermitArray.sPermitArray[bAvailable].lPermitBatchCount = 0;
                    unistrcpy(CMOSPermitArray.sPermitArray[bAvailable].wPermitName, pwPermitName);
                }
                else
                    fbReturnValue = FALSE;
            }
            else
                CMOSPermitArray.bIndex = bInx+1;
        }
        else
            fbReturnValue = FALSE;
    }
    else    // change to no permit being selected
        CMOSPermitArray.bIndex = 0;

    return(fbReturnValue);
}
