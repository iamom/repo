/************************************************************************
  PROJECT:        FPHX - Future Phoenix
  MODULE NAME:    platform.c
  
 DESCRIPTION:    Contains all functions that support the Janus platform task.
 
 ----------------------------------------------------------------------
               Copyright (c) 2003 Pitney Bowes Inc.
                    35 Waterview Drive
                   Shelton, Connecticut  06484
 ----------------------------------------------------------------------
 REVISION HISTORY:
 
 16-Mar-15 sa002pe on FPHX 02.12 int branch
 	Changed fnPlatformLocalizationTimerExpire to comment out the message put in the syslog
	because it ends up filling up the syslog w/ the same message.
	
 05-May-14 sa002pe on FPHX 02.10 shelton branch
 	For Fraca 225121: Changed fnPlatGetPrecision to adjust the precision value for metric W&M platforms.
	
	For various issues that have come up w/ trying to connect a USB platform, changed the
	"Unknown Task Message Source" error to have PLAT as the task instead of EMD and to have
	the ID of the source task as the error code.
*
* 24-Feb-13 sa002pe on FPHx 02.10 shelton branch
*	Re-arranged the listing of weight limit values so all the lb/oz values are together
*	in numerical order and all the kg values are together in numerical order so it's
*	easier to see what has been defined.
*	Changed fnGetPlatformLimit to make sure the platform units variable has been filled
*	in. Was causing problems if the System Setup Report was the first thing done after
*	power up.
*
* 04-Nov-12 sa002pe on FPHx 02.10 shelton branch
*	Fraca 218853:
*	1. Changed fnPlatformTask & fnsReportDisconnect to initialize ulRangeIncrement.
*	2. Changed fnmProcessPlatMessage to fill in ulRangeIncrement.
*	3. Changed fnPlatGetPrecision to take no arguments and to get the actual precison of
*		a USB W&M platform.
*
*	Merged in change from Janus so fnmProcessPlatMessage copies the message to a local buffer
*	so the message buffer can be freed up for use w/ another incoming message.

 2012.10.26 Bob Li      FPHX 02.10 cienet branch
    - Fixed fraca 218374 via setting the 'S' command retry times to 1 and reducing the timeout value.
 
 2012.10.18 Bob Li      FPHX 02.10 cienet branch
    - Removed the 3 seconds sleep since 'C' platform is not required by FP.

 2012.10.12 John Gao    FPHX 02.10 cienet branch
    - Fixed fraca GMSE00218654 - The differential weighing mode does not work
    
 2012.09.25 Bob Li 		FPHX 02.10 cienet branch
 	-Updated function fnsRetryPlatSMessage(), fnPlatformTask(), and fnIsCalibrationSupported()
	to support the serial platform calibration and make sure it work normally with new calibration logic.
 	-Modified fnPlatGetExtendedSWVersion to get the meter type.
	
 2012.09.21 John Gao
	-Added new functions fnPlatRestartFilter, fnPlatGetFilteringSetting, fnPlatSetFilteringSetting, 
	 fnPlatSetFilteringCount,
	 Modified fnPlatBuildUpdateMessage, to support platform message filtering.	
   	-Added fnsRetryPlatMessage to try to get the Serial Number from Serial Platform
   	-Fixed fraca 218374, If trying to establish communications w/ the platform, ignore any status/count messages
   	 Reactive the scale if it is not in a correct state
	-Set fSentSerialNumberRequest to FALSE when serial number received

 2012.09.14 Bob Li
   Added new function fnIsSetLocationCodeSupported() to filter out the W&M scale
   when setting location code.
   
 2012.09.11 John Gao
   Merge code from Janus to support W&M platform, No weight rounding for W&M scale,
   added part of the filtering code,  implement Enh Req 184995
  
 2012.09.10 Bob Li
  - Add function fnIsCalibrationSupported() to Check if the attached platform is a 
    non-W&M USB platform.
    
 2012.08.30 John Gao
   Merge code from Janus to support W&M platform, added code fix from Sandra Peterson

 2012.08.01  Clarisa Bellamy
  - Moved the event flag from the CM_EVENT_GROUP to the PLAT_EVENT_GROUP 
    (which already exists) and changed the flag name to PLAT_OK_TO_SEND_UPDATE.
  - Added syslog entries for clearing/setting and checking the event flag.

 12-July 2012 Liu Jupeng     
    Modified the function fnsReportUpdate() and fnPlatformTask() for fixing the 
    issue that the meter prints the empty evelope.

 2009.10.28     Michael Lepore  Fixes to address powerup problems when PLATFORM set to 
                                normal preset.
        - PLAT_ACTIVATE_PORT not really needed. This was used by Janus to multiplex the port
        - Set initial state to PLAT_STATE_DISCONNECTED 
        - fnPlatIsAttached() - removed check of PLAT_STATE_ESTABLISHING_COMM
  
 2008.09.30     Clarisa Bellamy     FPHX 1.11 branch
  - In fnPlatGetSWVersion, make sure that the function still works even if the 
    SWVersion string is not null-terminated.

 2008.08.05     Clarisa Bellamy     FPHX 1.07 branch
  Fix a bunch of compiler and LINT warnings:
  - Change these function defintions to take no arguments, and don't pass any
    arguments to them:
        fnRateGetWeightUnit, fnPlatForceUpdate
  - Change these function defintions to return no value:
        fnPlatProcessMsg, fnConfigurePlatCommunications, fnPlatSendCommSpecial, 
        fnPlatLogEvent.
  - In fnPlatformTask, initialize local variable, ulPingTime to 0.
  - Add missing include files, and missing prototypes.
  - Remove unused extern.
  - Add some comments
  
 04 Jan 2007 Bill Herring    Modified fnPlatformTask() so that the internal scale
                               is part re initialised if necessary when the external
                               is disconnected (fraca GMSE00134357).
 
 17-May-2007 - Michael Lepore    - In fct fnPlatBuildUpdateMessage() qualified (pPlat == pActivePlatform) around 
                                     fForcedUpdatePending = FALSE; Same check made around call to fnPlatForceUpdate(); 
                                     This ensure that the serial platform doesn't interfere with USB platform when both 
                                     connected. The USB platform takes precedence. 
                                     William J Herring found this while looking into Fraca 119985. This change should also 
                                     fix Fraca 120292.
  
  Victor 16 SEP 2005
  Added a field to the platform structure which is used to determine if we are to apply
  the graviational constant adjustment to the weight. Currently we only do this with the
  serial platforms and not with the usb
  Add function fnClearDSRState().
 
* ----------------------------------------------------------------------
* PRE-CLEARCASE REVISION HISTORY:
*
 *    Rev 1.51   04 May 2005 14:28:50   HO501SU
 * Commented out a log message so it won't 
 * overwrite useful system log.
 * 
 *    Rev 1.50   25 Jan 2005 17:24:50   HO501SU
 * Fixed zero weight problem for last piece
 * in differential weighing.
 * 
 * 
 *    Rev 1.49   Jan 11 2005 11:21:32   cx17598
 * Added a utility function for checking platform enabled/disabled feature. -Victor
 * 
 *    Rev 1.48   02 Nov 2004 15:44:56   HO501SU
 * Fixed weighing unit setting bug,
 * 
 *    Rev 1.47   22 Oct 2004 13:37:22   HO501SU
 * Corrected lint warnings.
 * 
 *    Rev 1.46   Sep 22 2004 18:27:08   HO501SU
 * Reconfigure UART when no response to
 * polling command. This fixed the EMC failure
 * problem.  Files checked in: platform.c, 
 * platform.h
 * 
 *    Rev 1.45   03 Sep 2004 11:41:20   HO501SU
 * Moved events setting to the bottom of 
 * the connection complete function.
 * 
 *    Rev 1.44   Aug 26 2004 16:15:26   cx17598
 * set the platform connect or disconnect event flag when the platform is connected or disconnected. -Victor
 * 
 *    Rev 1.43   24 Aug 2004 12:51:02   HO501SU
 * Send PLAT_DISCONNECT if platform is 
 * not attached when activating port
 * 
 *    Rev 1.42   19 Aug 2004 16:13:30   HO501SU
 * Added PLAT_DISCONNECT when relinquishing
 * port to modem.
 * 
 *    Rev 1.41   18 Aug 2004 14:00:18   HO501SU
 * Force weight update when scale port 
 * is activated.
 * 
 *    Rev 1.40   03 Aug 2004 14:43:12   HO501SU
 * Shortened reset time when localizing JB76
 * platform, automatically localize JB76 when
 * local code is changed. Check-in files are
 * platform.h, platform.c, TSKS.C, oioob.c.
 * 
 * 
 *    Rev 1.39   28 Jul 2004 18:12:16   HO501SU
 * Fixed empty reading after zeroing the scale. 
 * 
 *    Rev 1.38   28 Jul 2004 15:13:02   HO501SU
 * Bug fix when over 100 lbs.
 * 
 *    Rev 1.36   27 Jul 2004 18:17:38   HO501SU
 * Added defensive code when writing presets.
 * 
 *    Rev 1.35   26 Jul 2004 14:12:34   HO501SU
 * Fixed dead platform problem after saving 
 * preset to FFS.
 * 
 *    Rev 1.33   Jul 22 2004 22:00:34   HO501SU
 * bug fix.
 * 
 * 
 *    Rev 1.32   Jul 22 2004 20:33:40   HO501SU
 * Fixed a bug that platform task did not 
 * relinquish UART port to modem when 
 * requested by OI.
 * 
 *    Rev 1.31   16 Jul 2004 18:00:04   HO501SU
 * Bug fixes.
 * 
 *    Rev 1.30   16 Jul 2004 16:31:30   HO501SU
 * Supports JB76 large capacity scale. Other 
 * files are: LCDUTILS.C, TSKS.C, download.c,
 * OSSETUP.H, OS.H, platform.h, lcdutiils.c.
 * 
 * 
 * 
 *    Rev 1.28   15 Jul 2004 18:44:02   HO501SU
 * Supports JB76 large capacity scale. Other 
 * files are: LCDUTILS.C, TSKS.C, download.c,
 * OSSETUP.H, OS.H, platform.h, lcdutiils.c. 
 * Most of changes in other files are timer 
 * related.
 * 
 *    Rev 1.27   03 Jun 2004 15:47:20   HO501SU
 * Fixed for warning weight screen when 
 * piece weight and total weight are the same.
 * 
 *    Rev 1.26   23 Apr 2004 17:25:34   HO501SU
 * Bug fix when adding weight.
 * 
 *    Rev 1.25   23 Apr 2004 13:09:44   HO501SU
 * Added interface to OIT for mode change.
 * 
 *    Rev 1.25   22 Apr 2004 16:43:42   HO501SU
 * Added interface to OIT for mode change.
 * 
 *    Rev 1.24   06 Apr 2004 13:31:44   HO501SU
 * Added differential weighing mechanism, but
 * needs screen support for full functioning.
 * 
 *    Rev 1.23   Mar 08 2004 09:48:48   NEX0GVE
 * Added function for seting platform forcing update flag.
 * 
 *    Rev 1.22   Dec 12 2003 16:57:06   mozdzjm
 * Improved platform communication loss recovery.  Removed flashing red LED when recovery occurs.
 * 
 *    Rev 1.21   Dec 12 2003 10:54:12   mozdzjm
 * Corrected placement of wait time variables in main message loop.
 * 
 *    Rev 1.20   Dec 09 2003 16:00:24   mozdzjm
 * Added logic to log error information and reconfigure UART if we are in the weighing state and do
 * not receive a platform message within the deadman timeout period.
 * 
 *    Rev 1.19   Dec 03 2003 12:06:28   mozdzjm
 * Removed temporary hardcoded enabling of 10 lbs.
 * 
 *    Rev 1.18   Nov 26 2003 13:24:52   mozdzjm
 * Temporarily enable 10lbs for platform regardless of feature table.
 * 
 *    Rev 1.17   Nov 20 2003 11:26:20   mozdzjm
 * Added feature checking for 2, 5 and 10 lb limits.
 * 
 *    Rev 1.16   Nov 18 2003 09:16:38   mozdzjm
 * Added over-capacity reporting when the weight sent to us from the platform
 * exceeds the feature enabled weight limit.
 * 
 *    Rev 1.15   Sep 03 2003 11:11:10   mozdzjm
 * Added polling of DSR line to allow quick detection of hot plugged/unplugged platform.
 * 
 *    Rev 1.14   Sep 02 2003 14:45:06   mozdzjm
 * Corrected fnPlatIsAttached, which was not dealing with PLAT_STATE_DISCONNECTED properly.
 * 
 *    Rev 1.13   Aug 28 2003 10:44:34   mozdzjm
 * Added fnPlatIsAttached().
 * Uncommented BROADCAST_SUPPORTED to make the newer platforms the default.
 * 
 *    Rev 1.12   Aug 20 2003 14:17:42   mozdzjm
 * Added support for localization and metric weighing.
 * 
 *    Rev 1.11   Aug 19 2003 13:51:52   mozdzjm
 * Added support for calibration abort command.
 * 
 *    Rev 1.10   Aug 18 2003 15:27:46   mozdzjm
 * Added basic support for calibration.
 * 
 *    Rev 1.9   01 Aug 2003 16:01:02   BR507HA
 * Added call to OSReceiveIntertask in while loop for PLATFORM_TERMINAL condition.
 * This keeps the msg queue from filling up.
 * 
 *    Rev 1.8   Jul 25 2003 16:10:06   mozdzjm
 * Added call to reset IRQs to platform after ACTIVATE_PORT command is received.
 * 
 *    Rev 1.7   Jul 23 2003 16:58:30   mozdzjm
 * Baseline support for talking to platform that supports broadcast mode.
 * Uncomment BROADCAST_SUPPORTED to work with these platforms.
 * 
 *    Rev 1.6   Jul 10 2003 16:23:12   mozdzjm
 * Added timeout handling for zero.
 * 
 *    Rev 1.5   Jul 09 2003 17:06:58   mozdzjm
 * Added message timeout/retry logic.
 * Added reporting of motion timeout status.
 * Improved conversion and rounding algorithm.
 * Added some direct access function library routines.
 * 
 *    Rev 1.4   Jul 07 2003 15:48:52   mozdzjm
 * Added support for building message data to OIT in english lbs/ozs format.
 * Platform task can now handle basic weighing operations including zeroing.
 * 
 *    Rev 1.3   Jul 01 2003 14:56:28   mozdzjm
 * Intermediate check-in.  Contains basic message & event handling engine.  This version will respond to some of the 
 * OIT commands however PLAT_UPDATE messages still contain no data.
 * 
 *    Rev 1.2   28 May 2003 13:30:22   BR507HA
 * Rename platform event group
 * 
 *    Rev 1.1   May 05 2003 09:58:12   mozdzjm
 * Added comm driver initialization call and basic poll loop.
 * 
 *    Rev 1.0   Apr 30 2003 14:56:38   mozdzjm
 * Initial revision
 *************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/select.h>

#include "commglob.h"
#include "commintf.h"
#include "custdat.h"
#include "features.h"
#include "global.h"
#include "lcd.h"
#include "lcdirect.h"
#include "lcdutils.h"   // putString()
#include "pb232.h"
#include "pbioext.h"
#include "pbos.h"
#include "platuart.h"
#include "pbplatform.h"
//#include "RateSvcPublic.h"  // fnRateGetWeightUnit(), fnRateGetWeightUnit()
#include "utils.h"          // fnDisplayErrorDirect()

//---------------------------------------------------------------------
//      External Function Prototypes:
//  that don't exist in an include file yet.
//---------------------------------------------------------------------


//---------------------------------------------------------------------
//      Local Macro Definitions:
//---------------------------------------------------------------------

//#define PLAT_DBG_MESSAGES    1        /* uncomment to enable direct platform writes to screen */
#define BROADCAST_SUPPORTED            // only for standard platform

#define NON_WM_PLATFORM		FALSE
#define WM_PLATFORM			TRUE

// The following are used when platform message filtering is turned on.
// For the following, the actual consecutive messages needed before the update msg
// will be sent to the display is one more than the value.

//USB Platform
#define SAME_WEIGHT_REPEAT_COUNT_U		3 
#define SAME_STATUS_REPEAT_COUNT_U		14

//Serial Platform
#define SAME_WEIGHT_REPEAT_COUNT_S		3 
#define SAME_STATUS_REPEAT_COUNT_S		8


//---------------------------------------------------------------------
//      External Variable Declarations:
//---------------------------------------------------------------------

extern unsigned char    fCMOSScaleLocationCodeInited;
extern unsigned char    bCMOSScaleLocationCode;
extern unsigned char    fCMOSScaleLocallyCalibrated;
extern BOOL             CMOSPlatformFilteringOn;

extern unsigned long lwRXISRIn;
extern unsigned long lwRXISROut;
extern unsigned long lwTXISRIn;
extern unsigned long lwTXISROut;


/* private function prototypes */
static BOOL fnPlatSendMessage(PLATFORMDEVICE * , const char *pMsg);
static BOOL fnPlatProcessSTEvent(PLATFORMDEVICE * ,unsigned char bEvent);
static BOOL fnPlatMessageHandler(PLATFORMDEVICE * ,PLAT_MSG_TABLE_ENTRY *pMsgTable, INTERTASK *pMsg);
static unsigned char fnPlatBuildUpdateMessage(PLATFORMDEVICE *, unsigned char *pDest, BOOL fForceUpdate);
static BOOL fnPlatConvertCountToWeight(PLATFORMDEVICE *, long lwCount, long *pWeight);
static BOOL fnPlatConvertStringToWeight(PLATFORMDEVICE *,unsigned char *pStr, long *pWeight);
static double fnPlatLocalizeCount(PLATFORMDEVICE * ,unsigned long lwCount);
static BOOL fnPlatRestartMotionTimer(BOOL fStateDependentCheck);
static BOOL fnPlatCheckOverCapacity(PLATFORMDEVICE * ,unsigned long lwWeight);
static void fnPlatPollWeight(PLATFORMDEVICE * , int );
static BOOL fnGetCalibrationWeight(unsigned long *pWeight);
static BOOL fnGetScaleLocationCode(unsigned long *pLocCode);
static BOOL fnSetScaleLocationCode(unsigned long locCode);

/* state transition functions */
static void fnsStopPolling( PLATFORMDEVICE * );
static void fnsActivatePort( PLATFORMDEVICE * );
static void fnsFinishConnection( PLATFORMDEVICE * );
static void fnsStopBroadcast( PLATFORMDEVICE * );
static void fnsRequestCount( PLATFORMDEVICE * );
static void fnsRequestCalib( PLATFORMDEVICE * );
static void fnsRequestVersion( PLATFORMDEVICE * );
static void fnsRequestSerialNumber(PLATFORMDEVICE *);
static void fnsUserZeroCfm( PLATFORMDEVICE * );
static void fnsUserRefCfm( PLATFORMDEVICE * );
static void fnsUserCalibAbort( PLATFORMDEVICE * );
static void fnsReportUpdate( PLATFORMDEVICE * );
static void fnsReportMotionTO( PLATFORMDEVICE * );
static void fnsReportZeroPrompt( PLATFORMDEVICE * );
static void fnsReportRefPrompt( PLATFORMDEVICE * );
static void fnsStoreCalibData( PLATFORMDEVICE * );
static void fnsRetryPlatBMessage( PLATFORMDEVICE * );
static void fnsRetryPlatHMessage( PLATFORMDEVICE * );
static void fnsRetryPlatVMessage( PLATFORMDEVICE * );
static void fnsRetryPlatZMessage( PLATFORMDEVICE * );
static void fnsRetryPlatSMessage(PLATFORMDEVICE * );
static void fnsRetryPlatMessage(PLATFORMDEVICE * );
static void fnsScheduleZero( PLATFORMDEVICE * );
static unsigned long fnsPerformScheduledOp( PLATFORMDEVICE * );
static void fnsReportDisconnect( PLATFORMDEVICE * );
static void fnsReconfigureUART( PLATFORMDEVICE * );
static void fnsGoSleep( PLATFORMDEVICE * );
static void fnsWakeup( PLATFORMDEVICE * );
static void fnsDummyGoSleep( PLATFORMDEVICE * );
static void fnsDummyWakeup( PLATFORMDEVICE * );
static void fnsScheduleLocalize( PLATFORMDEVICE * );


/* message handling functions */
static BOOL fnmProcessPlatMessage(PLATFORMDEVICE * ,INTERTASK *pIntertask);
static BOOL fnmProcessOITZeroReq(PLATFORMDEVICE * ,INTERTASK *pIntertask);
static BOOL fnmProcessOitModeReq(PLATFORMDEVICE * ,INTERTASK *pIntertask);
static BOOL fnmProcessTimerMessage(PLATFORMDEVICE * ,INTERTASK *pIntertask);

static unsigned char    pollingRetry = 0;           
static unsigned long    lwIntertaskWaitTime = 0;
static BOOL             fForcedUpdatePending = FALSE;

static PLATFORMDEVICE *pActivePlatform = NULL_PTR;

extern UINT8 fnRateGetWeightUnit(void);

#ifdef PLATFORM_TRACING

typedef struct _wchange
{
  long lwWeight;
  long lwPrevWeight;
  long lwLastCount;
  long lwPrevCount;
  unsigned long ulTime;
  unsigned long ulT1;
  unsigned long ulT2;
  unsigned long ulT3;
}WCHANGE;

#define MAXWTRK 1024

WCHANGE weightTrack[ MAXWTRK ];
unsigned long wTrkIdx = 0;

typedef struct _platstrace
{
  unsigned long ulEvent;
  unsigned long ulCurrState;
  unsigned long ulNextState;
  unsigned long ulTime;
}PLATSTATETRACE;

#define MAXSTATETRACE 1024
PLATSTATETRACE PlatStateTrace[ MAXSTATETRACE ];
unsigned long ulPTraceIdx = 0;

#endif

char   tmpBuf[MAX_WT_STR_LEN];
BOOL   platDbg = TRUE; // FALSE; 
PLATFORMDEVICE sPlatforms[ MAX_PLATFORMS ];

static char				cLastMessageSent = 'S';
static BOOL fSentSerialNumberRequest = FALSE;
/*-------  intertask message table  -------*/
static const PLAT_MSG_TABLE_ENTRY pPlatMsgTable[] = {
  /* task ID        message ID               byte data [0]       byte data [1]      event (FF means call fn)        fn pointer          */
  { OIT,        PLAT_ACTIVATE_PORT,        PLAT_DONT_CARE,        PLAT_DONT_CARE,       PLAT_EV_ACTIVATE_PORT,        NULL            },
  { OIT,        PLAT_RELINQUISH_PORT,      PLAT_DONT_CARE,        PLAT_DONT_CARE,       PLAT_EV_RELINQUISH_PORT,        NULL            },
  { OIT,        PLAT_ZERO_REQ,            PLAT_DONT_CARE,        PLAT_DONT_CARE,       PLAT_EV_ZERO_REQ,            NULL },
  { OIT,        PLAT_CALIB_REQ,            PLAT_DONT_CARE,        PLAT_DONT_CARE,       PLAT_EV_CALIB_REQ,            NULL             },
  { OIT,        PLAT_CALIB_ZERO_CFM,    PLAT_DONT_CARE,        PLAT_DONT_CARE,       PLAT_EV_CALIB_USER_0_RCVD,    NULL             },
  { OIT,        PLAT_CALIB_REF_CFM,        PLAT_DONT_CARE,        PLAT_DONT_CARE,       PLAT_EV_CALIB_USER_REF_RCVD,    NULL             },
  { OIT,        PLAT_CALIB_ABORT,        PLAT_DONT_CARE,        PLAT_DONT_CARE,       PLAT_EV_CALIB_USER_ABORT,    NULL             },
  { OIT,        PLAT_MODE_REQ,            PLAT_DONT_CARE,        PLAT_DONT_CARE,       0xFF,                        fnmProcessOitModeReq },
  { OIT,        PLAT_LOCALIZE_REQ,        PLAT_DONT_CARE,        PLAT_DONT_CARE,       PLAT_EV_LOCALIZE_REQ,       NULL, },
  { OIT,        PLAT_GO_SLEEP_REQ,        PLAT_DONT_CARE,        PLAT_DONT_CARE,       PLAT_EV_GO_SLEEP,            NULL },
  { OIT,        PLAT_WAKEUP_REQ,        PLAT_DONT_CARE,        PLAT_DONT_CARE,         PLAT_EV_WAKEUP,              NULL },
  { PLAT,       PLAT_MOTION_TIMER_EXPIRED, PLAT_DONT_CARE,     PLAT_DONT_CARE,       PLAT_EV_MOTION_TIMER_EXP,    NULL             },
  { PLAT,       PLAT_POLLING_TIMER_EXPIRED, PLAT_DONT_CARE,    PLAT_DONT_CARE,       0xFF,                        fnmProcessTimerMessage },
  { PLAT,       PLAT_ACTIVATE_PORT,     PLAT_DONT_CARE,     PLAT_DONT_CARE,    PLAT_EV_ACTIVATE_PORT,       NULL            },
  { PLAT,       PLAT_RELINQUISH_PORT,   PLAT_DONT_CARE,     PLAT_DONT_CARE,    PLAT_EV_RELINQUISH_PORT,     NULL            },
  { PLAT_END_OF_TABLE, 0, 0, 0, 0 } };



/* **********************************************************************
// FUNCTION NAME:
// PURPOSE:
//
// AUTHOR: George Monroe
//
// INPUTS:
// RETURNS:
// **********************************************************************/
void fnPlatSetWaitTime( PLATFORMDEVICE *pPlat , unsigned long wtime )
{
  if(pPlat == pActivePlatform)
    lwIntertaskWaitTime = wtime;
}
/* **********************************************************************
// FUNCTION NAME:
// PURPOSE:
//
// AUTHOR: George Monroe
//
// INPUTS:
// RETURNS:
// **********************************************************************/

void fnPlatChangeState( PLATFORMDEVICE *pPlat , unsigned char bEvent , unsigned long ulNewState )
{
#ifdef PLATFORM_TRACING
  PlatStateTrace[ ulPTraceIdx ].ulEvent = bEvent;
  PlatStateTrace[ ulPTraceIdx ].ulCurrState = pPlat->ulCurrState;
  PlatStateTrace[ ulPTraceIdx ].ulNextState = ulNewState;
  PlatStateTrace[ ulPTraceIdx ].ulTime = PBSysTime();
  ulPTraceIdx++;
  if( ulPTraceIdx == MAXSTATETRACE )
    ulPTraceIdx = 0;
#endif
  pPlat->ulPrevState = pPlat->ulCurrState;
  pPlat->ulCurrState = ulNewState;
}

void fnPlatLogEvent( unsigned long bEvent , unsigned arg )
{
#ifdef PLATFORM_TRACING
  PlatStateTrace[ ulPTraceIdx ].ulEvent = bEvent;
  PlatStateTrace[ ulPTraceIdx ].ulCurrState = 0x44444444;
  PlatStateTrace[ ulPTraceIdx ].ulNextState = arg;
  PlatStateTrace[ ulPTraceIdx ].ulTime = PBSysTime();
  ulPTraceIdx++;
  if( ulPTraceIdx == MAXSTATETRACE )
    ulPTraceIdx = 0;
#endif
}
/* **********************************************************************
// FUNCTION NAME:
// PURPOSE:
//
// AUTHOR: George Monroe
//
// INPUTS:
// RETURNS:
// **********************************************************************/

void fnPLAT_STATE_PORT_NA( PLATFORMDEVICE *pPlat , unsigned char bEvent )
{
  switch(bEvent)
    {
    case PLAT_EV_ACTIVATE_PORT:
      fnsActivatePort(pPlat);
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_ESTABLISHING_COMM );
      if(platDbg) putString("Platform Activated", __LINE__);
      break;
    case PLAT_EV_VERSION_RCVD:
    case PLAT_EV_COUNT_RCVD:
    case PLAT_EV_STATUS_RCVD:
    case PLAT_EV_CALIB_C_RCVD:
    case PLAT_EV_CALIB_F_RCVD:
    case PLAT_EV_ZERO_REQ:
    case PLAT_EV_RELINQUISH_PORT:
    case PLAT_EV_MOTION_TIMER_EXP:
    case PLAT_EV_MESSAGE_TIMEOUT:
    case PLAT_EV_QUIT_BROADCAST_RCVD:
    case PLAT_EV_CALIB_REQ:
    case PLAT_EV_CALIB_USER_0_RCVD:
    case PLAT_EV_CALIB_USER_REF_RCVD:
    case PLAT_EV_CALIB_USER_ABORT:
    case PLAT_EV_CONNECTED:
    case PLAT_EV_DISCONNECTED:
    case PLAT_EV_GO_SLEEP:
    case PLAT_EV_WAKEUP:
    case PLAT_EV_LOCALIZE_REQ:
      break;
    default:
      break;
    }
}


unsigned long ulNumOfRptsRecved;

void fnPLAT_STATE_ESTABLISHING_COMM( PLATFORMDEVICE *pPlat , unsigned char bEvent )
{
    switch(bEvent)
    {
    case PLAT_EV_VERSION_RCVD:
        fnsFinishConnection(pPlat);
        ulNumOfRptsRecved = 0;
        fnPlatChangeState( pPlat , bEvent , PLAT_STATE_WEIGHING );
        if(platDbg) putString("Platform Connected", __LINE__);
        fnPlatSetWaitTime( pPlat , 0 );
        break;
    case PLAT_EV_RELINQUISH_PORT:
    case PLAT_EV_QUIT_BROADCAST_RCVD:
        fnsStopPolling( pPlat );
        fnPlatChangeState( pPlat , bEvent ,  PLAT_STATE_PORT_NA );
        fnPlatSetWaitTime( pPlat , 0 );
        break;
    case PLAT_EV_MESSAGE_TIMEOUT:
        if (pPlat->fLastScaleConnectState)
        {
            fnsRetryPlatMessage( pPlat );
        }
        else
        {
            fnPlatSetWaitTime( pPlat , PLAT_V_RESPONSE_TIME );
        }
        break;
    case PLAT_EV_DISCONNECTED:
        fnsReportDisconnect(pPlat);
        fnPlatChangeState( pPlat , bEvent , PLAT_STATE_DISCONNECTED );
        fnPlatSetWaitTime( pPlat , 0 );
        break;
    case PLAT_EV_CONNECTED:
        if (pPlat == pActivePlatform && pPlat->fLastScaleConnectState)
        {
            fnsRequestSerialNumber(pPlat);
        }
        break;

    case PLAT_EV_SN_RCVD:
        if (pActivePlatform && (pPlat == pActivePlatform) && (pPlat->fLastScaleConnectState))
	    {
			fSentSerialNumberRequest = FALSE;

			fnsRequestVersion( pPlat );

//	        pPlat->ucUnits = (unsigned char)fnRateGetWeightUnit();
	        fnPlatForceUpdate();
	    }
	    break;

    case PLAT_EV_STATUS_RCVD:
	case PLAT_EV_COUNT_RCVD:
	case PLAT_EV_ZERO_REQ:
	case PLAT_EV_CALIB_F_RCVD:
	case PLAT_EV_CALIB_C_RCVD:
	case PLAT_EV_ACTIVATE_PORT:
	case PLAT_EV_MOTION_TIMER_EXP:
	case PLAT_EV_CALIB_REQ:
	case PLAT_EV_CALIB_USER_0_RCVD:
	case PLAT_EV_CALIB_USER_REF_RCVD:
	case PLAT_EV_CALIB_USER_ABORT:
	case PLAT_EV_GO_SLEEP:
	case PLAT_EV_WAKEUP:
	case PLAT_EV_LOCALIZE_REQ:
	    break;

	default:
	    break;
    }
}
void fnPLAT_STATE_DISCONNECTED( PLATFORMDEVICE *pPlat , unsigned char bEvent )
{
  
  switch(bEvent)
    {
    case PLAT_EV_RELINQUISH_PORT:
      fnsStopPolling( pPlat );
      fnPlatChangeState( pPlat , bEvent ,  PLAT_STATE_PORT_NA );
      break;
    case PLAT_EV_CONNECTED:
    case PLAT_EV_ACTIVATE_PORT:
      fnsActivatePort(pPlat);
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_ESTABLISHING_COMM );
      break;
    case PLAT_EV_VERSION_RCVD:
    case PLAT_EV_COUNT_RCVD:
    case PLAT_EV_STATUS_RCVD:
    case PLAT_EV_CALIB_C_RCVD:
    case PLAT_EV_CALIB_F_RCVD:
    case PLAT_EV_ZERO_REQ:
    case PLAT_EV_MOTION_TIMER_EXP:
    case PLAT_EV_MESSAGE_TIMEOUT:
    case PLAT_EV_QUIT_BROADCAST_RCVD:
    case PLAT_EV_CALIB_REQ:
    case PLAT_EV_CALIB_USER_0_RCVD:
    case PLAT_EV_CALIB_USER_REF_RCVD:
    case PLAT_EV_CALIB_USER_ABORT:
    case PLAT_EV_DISCONNECTED:
    case PLAT_EV_GO_SLEEP:
    case PLAT_EV_WAKEUP:
    case PLAT_EV_LOCALIZE_REQ:
      break;
    default:
      break;
    }
}
void fnPLAT_STATE_WEIGHING( PLATFORMDEVICE *pPlat , unsigned char bEvent )
{
  
  switch(bEvent)
    {
    case PLAT_EV_COUNT_RCVD:
    case PLAT_EV_STATUS_RCVD:
      fnsReportUpdate( pPlat );
      ulNumOfRptsRecved++;
      if(ulNumOfRptsRecved > 2 )
    fnPlatSetWaitTime( pPlat , 0 );
      else
    fnsRetryPlatBMessage( pPlat );
      break;
    case PLAT_EV_ZERO_REQ:
      fnsScheduleZero( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_WEIGHING_SCH);
      fnPlatSetWaitTime( pPlat , 0 );
      break;
    case PLAT_EV_RELINQUISH_PORT:
      fnsStopPolling( pPlat );
      fnPlatChangeState( pPlat  , bEvent,  PLAT_STATE_PORT_NA );
      if(platDbg) putString("Platform Relinquish", __LINE__);
      break;
    case PLAT_EV_LOCALIZE_REQ:
      fnsScheduleLocalize( pPlat );
        // (ML) - need to go back to weighing
        fnPlatChangeState( pPlat  , bEvent,  PLAT_STATE_WEIGHING_SCH);      
      // fnPlatChangeState( pPlat  , bEvent,  PLAT_STATE_PORT_NA );
      break;
    case PLAT_EV_MOTION_TIMER_EXP:
      fnsReportMotionTO( pPlat );
      break;
    case PLAT_EV_CALIB_REQ:
      fnsRequestCalib(pPlat);
      fnPlatChangeState( pPlat  , bEvent,  PLAT_STATE_CAL_WAIT_PLAT_0 );
      break;
    case PLAT_EV_DISCONNECTED:
      fnsReportDisconnect( pPlat );
      fnPlatChangeState( pPlat  , bEvent, PLAT_STATE_DISCONNECTED);
      if(platDbg) putString("Platform Disconnected", __LINE__);
      break;
    case PLAT_EV_VERSION_RCVD:
    case PLAT_EV_MESSAGE_TIMEOUT:
      fnsRetryPlatBMessage( pPlat );
      break;
    case PLAT_EV_QUIT_BROADCAST_RCVD:
    case PLAT_EV_CALIB_USER_0_RCVD:
    case PLAT_EV_CALIB_USER_REF_RCVD:
    case PLAT_EV_CALIB_USER_ABORT:
    case PLAT_EV_CONNECTED:
    case PLAT_EV_CALIB_C_RCVD:
    case PLAT_EV_CALIB_F_RCVD:
    case PLAT_EV_GO_SLEEP:
    case PLAT_EV_WAKEUP:
    case PLAT_EV_ACTIVATE_PORT:
    default:
      break;
    }
}
void fnPLAT_STATE_WEIGHING_SCH( PLATFORMDEVICE *pPlat , unsigned char bEvent )
{
  unsigned long ulNextState;
  switch(bEvent)
    {
    case PLAT_EV_STATUS_RCVD:
    case PLAT_EV_COUNT_RCVD:
      ulNextState = fnsPerformScheduledOp( pPlat );
      fnPlatChangeState( pPlat , bEvent , ulNextState );
      break;
    case PLAT_EV_RELINQUISH_PORT:
      fnsStopPolling( pPlat );
      fnPlatChangeState( pPlat  , bEvent,  PLAT_STATE_PORT_NA );
      break;
    case PLAT_EV_DISCONNECTED:
      fnsReportDisconnect( pPlat );
      fnPlatChangeState( pPlat  , bEvent, PLAT_STATE_DISCONNECTED);
      break;
    case PLAT_EV_GO_SLEEP:
      fnsGoSleep( pPlat );
      break;
    case PLAT_EV_WAKEUP:
      fnsWakeup( pPlat );
      break;
    case PLAT_EV_ACTIVATE_PORT:
    case PLAT_EV_VERSION_RCVD:
    case PLAT_EV_CALIB_C_RCVD:
    case PLAT_EV_CALIB_F_RCVD:
    case PLAT_EV_ZERO_REQ:
    case PLAT_EV_MOTION_TIMER_EXP:
    case PLAT_EV_MESSAGE_TIMEOUT:
    case PLAT_EV_QUIT_BROADCAST_RCVD:
    case PLAT_EV_CALIB_REQ:
    case PLAT_EV_CALIB_USER_0_RCVD:
    case PLAT_EV_CALIB_USER_REF_RCVD:
    case PLAT_EV_CALIB_USER_ABORT:
    case PLAT_EV_CONNECTED:
    case PLAT_EV_LOCALIZE_REQ:
    default:
      break;
    }
}
void fnPLAT_STATE_ZEROING( PLATFORMDEVICE *pPlat , unsigned char bEvent )
{
  
  switch(bEvent)
    {
    case PLAT_EV_GO_SLEEP:
      fnsGoSleep( pPlat );
      break;
    case PLAT_EV_WAKEUP:
      fnsWakeup( pPlat );
      break;
    case PLAT_EV_RELINQUISH_PORT:
      fnsStopPolling( pPlat );
      fnPlatChangeState( pPlat  , bEvent,  PLAT_STATE_PORT_NA );
      fnPlatSetWaitTime( pPlat , 0 );
      break;
    case PLAT_EV_MESSAGE_TIMEOUT:
      fnsRetryPlatZMessage( pPlat );
      break;
    case PLAT_EV_COUNT_RCVD:
    case PLAT_EV_STATUS_RCVD:
      fnPlatSetWaitTime( pPlat , 0 );
      fnsReportUpdate( pPlat );
      fnPlatChangeState( pPlat  , bEvent,  PLAT_STATE_WEIGHING );
      break;
    case PLAT_EV_DISCONNECTED:
      fnsReportDisconnect( pPlat );
      fnPlatChangeState( pPlat  , bEvent , PLAT_STATE_DISCONNECTED);
      fnPlatSetWaitTime( pPlat , 0 );
      break;
    case PLAT_EV_ACTIVATE_PORT:
    case PLAT_EV_VERSION_RCVD:
    case PLAT_EV_CALIB_C_RCVD:
    case PLAT_EV_CALIB_F_RCVD:
    case PLAT_EV_ZERO_REQ:
    case PLAT_EV_MOTION_TIMER_EXP:
    case PLAT_EV_QUIT_BROADCAST_RCVD:
    case PLAT_EV_CALIB_REQ:
    case PLAT_EV_CALIB_USER_0_RCVD:
    case PLAT_EV_CALIB_USER_REF_RCVD:
    case PLAT_EV_CALIB_USER_ABORT:
    case PLAT_EV_CONNECTED:
    case PLAT_EV_LOCALIZE_REQ:
      break;
    default:
      break;
    }
}
void fnPLAT_STATE_CAL_WAIT_PLAT_0( PLATFORMDEVICE *pPlat , unsigned char bEvent )
{
  
  switch(bEvent)
    {
    case PLAT_EV_CALIB_C_RCVD:
      fnsReportZeroPrompt( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_CAL_WAIT_USER_0 );
      break;
    case PLAT_EV_RELINQUISH_PORT:
      fnsStopPolling( pPlat );
      fnPlatChangeState( pPlat , bEvent ,  PLAT_STATE_PORT_NA );
      break;
    case PLAT_EV_DISCONNECTED:
      fnsReportDisconnect( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_DISCONNECTED);
      break;
    case PLAT_EV_CALIB_USER_ABORT:
      fnsUserCalibAbort( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_REBOOTING);
      break;
    case PLAT_EV_MOTION_TIMER_EXP:
    case PLAT_EV_MESSAGE_TIMEOUT:
    case PLAT_EV_QUIT_BROADCAST_RCVD:
    case PLAT_EV_CALIB_REQ:
    case PLAT_EV_CALIB_USER_0_RCVD:
    case PLAT_EV_CALIB_USER_REF_RCVD:
    case PLAT_EV_ACTIVATE_PORT:
    case PLAT_EV_VERSION_RCVD:
    case PLAT_EV_COUNT_RCVD:
    case PLAT_EV_STATUS_RCVD:
    case PLAT_EV_CALIB_F_RCVD:
    case PLAT_EV_ZERO_REQ:
    case PLAT_EV_CONNECTED:
    case PLAT_EV_GO_SLEEP:
    case PLAT_EV_WAKEUP:
    case PLAT_EV_LOCALIZE_REQ:
      break;
    default:
      break;
    }
}
void fnPLAT_STATE_CAL_WAIT_USER_0( PLATFORMDEVICE *pPlat , unsigned char bEvent )
{
  
  switch(bEvent)
    {
    case PLAT_EV_RELINQUISH_PORT:
      fnsStopPolling( pPlat );
      fnPlatChangeState( pPlat , bEvent ,  PLAT_STATE_PORT_NA );
      break;
    case PLAT_EV_CALIB_USER_0_RCVD:
      fnsUserZeroCfm( pPlat );
      fnPlatChangeState( pPlat , bEvent ,PLAT_STATE_CAL_WAIT_PLAT_REF ); 
      break;
    case PLAT_EV_CALIB_USER_ABORT:
      fnsUserCalibAbort( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_REBOOTING);
      break;
    case PLAT_EV_DISCONNECTED:
      fnsReportDisconnect( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_DISCONNECTED);
      break;
    case PLAT_EV_MOTION_TIMER_EXP:
    case PLAT_EV_MESSAGE_TIMEOUT:
    case PLAT_EV_QUIT_BROADCAST_RCVD:
    case PLAT_EV_CALIB_REQ:
    case PLAT_EV_ACTIVATE_PORT:
    case PLAT_EV_VERSION_RCVD:
    case PLAT_EV_COUNT_RCVD:
    case PLAT_EV_STATUS_RCVD:
    case PLAT_EV_CALIB_C_RCVD:
    case PLAT_EV_CALIB_F_RCVD:
    case PLAT_EV_ZERO_REQ:
    case PLAT_EV_CALIB_USER_REF_RCVD:
    case PLAT_EV_CONNECTED:
    case PLAT_EV_GO_SLEEP:
    case PLAT_EV_WAKEUP:
    case PLAT_EV_LOCALIZE_REQ:
      break;
    default:
      break;
    }
}
void fnPLAT_STATE_CAL_WAIT_PLAT_REF( PLATFORMDEVICE *pPlat , unsigned char bEvent )
{
  
  switch(bEvent)
    {
    case PLAT_EV_CALIB_F_RCVD:
      fnsReportRefPrompt(pPlat);
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_CAL_WAIT_USER_REF);
      break;
    case PLAT_EV_RELINQUISH_PORT:
      fnsStopPolling( pPlat );
      fnPlatChangeState( pPlat , bEvent ,  PLAT_STATE_PORT_NA );
      break;
    case PLAT_EV_CALIB_USER_ABORT:
      fnsUserCalibAbort( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_REBOOTING);
      break;
    case PLAT_EV_DISCONNECTED:
      fnsReportDisconnect( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_DISCONNECTED);
      break;
    case PLAT_EV_ZERO_REQ:
    case PLAT_EV_ACTIVATE_PORT:
    case PLAT_EV_VERSION_RCVD:
    case PLAT_EV_COUNT_RCVD:
    case PLAT_EV_STATUS_RCVD:
    case PLAT_EV_CALIB_C_RCVD:
    case PLAT_EV_MOTION_TIMER_EXP:
    case PLAT_EV_MESSAGE_TIMEOUT:
    case PLAT_EV_QUIT_BROADCAST_RCVD:
    case PLAT_EV_CALIB_REQ:
    case PLAT_EV_CALIB_USER_0_RCVD:
    case PLAT_EV_CALIB_USER_REF_RCVD:
    case PLAT_EV_CONNECTED:
    case PLAT_EV_GO_SLEEP:
    case PLAT_EV_WAKEUP:
    case PLAT_EV_LOCALIZE_REQ:
      break;
    default:
      break;
    }
}
void fnPLAT_STATE_CAL_WAIT_USER_REF( PLATFORMDEVICE *pPlat , unsigned char bEvent )
{
  
  switch(bEvent)
    {
    case PLAT_EV_RELINQUISH_PORT:
      fnsStopPolling( pPlat );
      fnPlatChangeState( pPlat , bEvent ,  PLAT_STATE_PORT_NA );
      break;
    case PLAT_EV_DISCONNECTED:
      fnsReportDisconnect( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_DISCONNECTED);
      break;
    case PLAT_EV_CALIB_USER_ABORT:
      fnsUserCalibAbort( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_REBOOTING);
      break;
    case PLAT_EV_CALIB_USER_REF_RCVD:
      fnsUserRefCfm( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_CAL_WAIT_PLAT_CMP );
      break;
    case PLAT_EV_ACTIVATE_PORT:
    case PLAT_EV_VERSION_RCVD:
    case PLAT_EV_COUNT_RCVD:
    case PLAT_EV_STATUS_RCVD:
    case PLAT_EV_CALIB_C_RCVD:
    case PLAT_EV_CALIB_F_RCVD:
    case PLAT_EV_ZERO_REQ:
    case PLAT_EV_MOTION_TIMER_EXP:
    case PLAT_EV_MESSAGE_TIMEOUT:
    case PLAT_EV_QUIT_BROADCAST_RCVD:
    case PLAT_EV_CALIB_REQ:
    case PLAT_EV_CALIB_USER_0_RCVD:
    case PLAT_EV_CONNECTED:
    case PLAT_EV_GO_SLEEP:
    case PLAT_EV_WAKEUP:
    case PLAT_EV_LOCALIZE_REQ:
      break;
    default:
      break;
    }
}
void fnPLAT_STATE_CAL_WAIT_PLAT_CMP( PLATFORMDEVICE *pPlat , unsigned char bEvent )
{
  
  switch(bEvent)
    {
    case PLAT_EV_CALIB_C_RCVD:
      fnsReportZeroPrompt(pPlat);
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_CAL_WAIT_USER_CMP );
      break;
    case PLAT_EV_RELINQUISH_PORT:
      fnsStopPolling( pPlat );
      fnPlatChangeState( pPlat , bEvent ,  PLAT_STATE_PORT_NA );
      break;
    case PLAT_EV_DISCONNECTED:
      fnsReportDisconnect( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_DISCONNECTED);
      break;
    case PLAT_EV_CALIB_USER_ABORT:
      fnsUserCalibAbort( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_REBOOTING);
      break;
    case PLAT_EV_CALIB_F_RCVD:
    case PLAT_EV_ZERO_REQ:
    case PLAT_EV_ACTIVATE_PORT:
    case PLAT_EV_VERSION_RCVD:
    case PLAT_EV_COUNT_RCVD:
    case PLAT_EV_STATUS_RCVD:
    case PLAT_EV_MOTION_TIMER_EXP:
    case PLAT_EV_MESSAGE_TIMEOUT:
    case PLAT_EV_QUIT_BROADCAST_RCVD:
    case PLAT_EV_CALIB_REQ:
    case PLAT_EV_CALIB_USER_0_RCVD:
    case PLAT_EV_CALIB_USER_REF_RCVD:
    case PLAT_EV_CONNECTED:
    case PLAT_EV_GO_SLEEP:
    case PLAT_EV_WAKEUP:
    case PLAT_EV_LOCALIZE_REQ:
      break;
    default:
      break;
    }
}
void fnPLAT_STATE_CAL_WAIT_USER_CMP( PLATFORMDEVICE *pPlat , unsigned char bEvent )
{
  
  switch(bEvent)
    {
    case PLAT_EV_RELINQUISH_PORT:
      fnsStopPolling( pPlat );
      fnPlatChangeState( pPlat , bEvent ,  PLAT_STATE_PORT_NA );
      break;
    case PLAT_EV_CALIB_USER_0_RCVD:
      fnsStoreCalibData(pPlat);
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_REBOOTING );
      break;
    case PLAT_EV_DISCONNECTED:
      fnsReportDisconnect( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_DISCONNECTED);
      break;
    case PLAT_EV_CALIB_USER_ABORT:
      fnsUserCalibAbort( pPlat );
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_REBOOTING);
      break;      
    case PLAT_EV_MOTION_TIMER_EXP:
    case PLAT_EV_MESSAGE_TIMEOUT:
    case PLAT_EV_QUIT_BROADCAST_RCVD:
    case PLAT_EV_CALIB_REQ:
    case PLAT_EV_ACTIVATE_PORT:
    case PLAT_EV_VERSION_RCVD:
    case PLAT_EV_COUNT_RCVD:
    case PLAT_EV_STATUS_RCVD:
    case PLAT_EV_CALIB_C_RCVD:
    case PLAT_EV_CALIB_F_RCVD:
    case PLAT_EV_ZERO_REQ:
    case PLAT_EV_CALIB_USER_REF_RCVD:
    case PLAT_EV_CONNECTED:
    case PLAT_EV_GO_SLEEP:
    case PLAT_EV_WAKEUP:
    case PLAT_EV_LOCALIZE_REQ:
      break;
    default:
      break;
    }
}
void fnPLAT_STATE_REBOOTING( PLATFORMDEVICE *pPlat , unsigned char bEvent )
{
  
  switch(bEvent)
    {
    case PLAT_EV_RELINQUISH_PORT:
      fnsStopPolling( pPlat );
      fnPlatChangeState( pPlat , bEvent ,  PLAT_STATE_PORT_NA );
      break;
    case PLAT_EV_MESSAGE_TIMEOUT:
      fnsRequestSerialNumber(pPlat);
      fnPlatChangeState( pPlat , bEvent , PLAT_STATE_ESTABLISHING_COMM );
      break;
    case PLAT_EV_DISCONNECTED:
    case PLAT_EV_MOTION_TIMER_EXP:
    case PLAT_EV_ACTIVATE_PORT:
    case PLAT_EV_VERSION_RCVD:
    case PLAT_EV_COUNT_RCVD:
    case PLAT_EV_STATUS_RCVD:
    case PLAT_EV_CALIB_C_RCVD:
    case PLAT_EV_CALIB_F_RCVD:
    case PLAT_EV_ZERO_REQ:
    case PLAT_EV_QUIT_BROADCAST_RCVD:
    case PLAT_EV_CALIB_REQ:
    case PLAT_EV_CALIB_USER_0_RCVD:
    case PLAT_EV_CALIB_USER_REF_RCVD:
    case PLAT_EV_CALIB_USER_ABORT:
    case PLAT_EV_CONNECTED:
    case PLAT_EV_GO_SLEEP:
    case PLAT_EV_WAKEUP:
    case PLAT_EV_LOCALIZE_REQ:
      break;
    default:
      break;
    }
}

void fnPlatHandleEvent( PLATFORMDEVICE *pPlat , unsigned char bEvent )
{

#ifdef PLATFORM_TRACING
  PlatStateTrace[ ulPTraceIdx ].ulEvent = 0x80000000 | bEvent;
  PlatStateTrace[ ulPTraceIdx ].ulCurrState = pPlat->ulCurrState;
  PlatStateTrace[ ulPTraceIdx ].ulNextState = pPlat->ulPrevState;
  PlatStateTrace[ ulPTraceIdx ].ulTime = PBSysTime();
  ulPTraceIdx++;
  if( ulPTraceIdx == MAXSTATETRACE )
    ulPTraceIdx = 0;
#endif
  switch( pPlat->ulCurrState )
    {
    case PLAT_STATE_PORT_NA:
      fnPLAT_STATE_PORT_NA( pPlat , bEvent );
      break;
    case PLAT_STATE_ESTABLISHING_COMM:
      fnPLAT_STATE_ESTABLISHING_COMM( pPlat ,bEvent );
      break;
    case PLAT_STATE_DISCONNECTED:
      fnPLAT_STATE_DISCONNECTED( pPlat , bEvent );
      break;
    case PLAT_STATE_WEIGHING:
      fnPLAT_STATE_WEIGHING( pPlat , bEvent );
      break;
    case PLAT_STATE_WEIGHING_SCH:
      fnPLAT_STATE_WEIGHING_SCH( pPlat , bEvent );
      break;
    case PLAT_STATE_ZEROING:
      fnPLAT_STATE_ZEROING( pPlat , bEvent );
      break;
    case PLAT_STATE_CAL_WAIT_PLAT_0:
      fnPLAT_STATE_CAL_WAIT_PLAT_0( pPlat , bEvent );
      break;
    case PLAT_STATE_CAL_WAIT_USER_0:
      fnPLAT_STATE_CAL_WAIT_USER_0( pPlat , bEvent );
      break;
    case PLAT_STATE_CAL_WAIT_PLAT_REF:
      fnPLAT_STATE_CAL_WAIT_PLAT_REF( pPlat , bEvent );
      break;
    case PLAT_STATE_CAL_WAIT_USER_REF:
      fnPLAT_STATE_CAL_WAIT_USER_REF( pPlat , bEvent );
      break;
    case PLAT_STATE_CAL_WAIT_PLAT_CMP:
      fnPLAT_STATE_CAL_WAIT_PLAT_CMP( pPlat , bEvent );
      break;
    case PLAT_STATE_CAL_WAIT_USER_CMP:
      fnPLAT_STATE_CAL_WAIT_USER_CMP( pPlat , bEvent );
      break;
    case PLAT_STATE_REBOOTING:
      fnPLAT_STATE_REBOOTING( pPlat , bEvent );
      break;
    default:
      break;
    }
}

/*---------------------------------------*/
/*   State transition functions (fns*)   */
/*---------------------------------------*/
/* **********************************************************************
// FUNCTION NAME: fnsStopPolling
// DESCRIPTION: State transition function that does the following:
//                    Stop polling if JB 76, for others do nothing
//                This allows an entry in the table to exist without calling
//                a function that does any work.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsStopPolling(PLATFORMDEVICE *pPlat )
{
  if(pPlat == pActivePlatform)
    {
      if(pPlat->cPlatformVersion[0] == 'C') // 70 lb scale, do polling
    {
      OSStopTimer(PLAT_POLLING_TIMER); 
      OSStopTimer(PLAT_LOCALIZATION_TIMER); 
      // pPlat->cPlatformVersion[0] = 0;
      // platLocalized = NOT_LOCALIZED;
      if(platDbg) putString("Stop POLLING & LOCALIZATION", __LINE__);
    }
      // set the platform disconnect event flag
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_DISCONNECT_EVT, OS_OR);
      // set the disconnect message to OIT
      OSSendIntertask(OIT, PLAT, PLAT_DISCONNECT, NO_DATA, NULL, 0);
    }
  return;
}


/* **********************************************************************
// FUNCTION NAME: fnsActivatePort
// DESCRIPTION: State transition function that does the following:
//                    Takes control of the serial port and attempts
//                    to establish communications with the platform.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  either PLAT_STATE_ESTABLISH_COMM or PLAT_STATE_DISCONNECTED,
//                    depending on if DSR is detected
// *************************************************************************/
static void fnsActivatePort(PLATFORMDEVICE *pPlat )
{
  unsigned short     bLen;
  unsigned char    pCfgbuf[32];

  if(pPlat == pActivePlatform)
    {
	  // start out by indicating that the scale isn't locally calibrated
	  fCMOSScaleLocallyCalibrated = FALSE;
      /* temporarily missing DSR check... for now, assume platform is there */
      if (pPlat->fLastScaleConnectState)
    {
		  if (fSentSerialNumberRequest == FALSE)
		  {
		    fSentSerialNumberRequest = TRUE;
            pPlat->ucPlatformRetries = 0;
            fnPlatSetWaitTime( pPlat , PLAT_S_RESPONSE_TIME );
            cLastMessageSent = 'S';
            (void)fnPlatSendMessage( pPlat , "S");
		  }
		  else
		  {
		    fSentSerialNumberRequest = FALSE;
		  }

//          pPlat->ucUnits = (unsigned char)fnRateGetWeightUnit();
          fnPlatForceUpdate();
    }
      else
    {
      // set the platform disconnect event flag
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_DISCONNECT_EVT, OS_OR);
      //OSSendIntertask(OIT, PLAT, PLAT_DISCONNECT, NO_DATA, NULL, 0);
    }
    }
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsFinishConnection
// DESCRIPTION: State transition function that does the following:
//                    1. Sends a PLAT_CONNECT message to the OIT to
//                        inform it that we are now connected.
//                    2. Starts requests a weight from the platform.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  next state is not modified- table state is used.
// *************************************************************************/
static void fnsFinishConnection(PLATFORMDEVICE *pPlat )
{
  fnPlatSetWaitTime( pPlat , PLAT_CONNECT_RESPONSE_TIME );

  // Only inform everyone if we are the active platform
  if(pPlat == pActivePlatform )
    {
#ifndef BROADCAST_SUPPORTED  // Only for standard platform
      fnPlatSendMessage( pPlat ,"H");
#else
      if(pPlat->cPlatformVersion[0] != 'C') // 70 lb scale, do polling
    {
      fnPlatSendMessage( pPlat ,"B");
    }
#endif
      
      /* all transitions to the weighing state require the motion timer to be started */
      fnPlatRestartMotionTimer(FALSE);
      
      // set the platform connect event flag
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_CONNECT_EVT, OS_OR);
      // send the plat connect message to OIT
      OSSendIntertask(OIT, PLAT, PLAT_CONNECT, NO_DATA, NULL, 0);
    }
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsStopBroadcast
// DESCRIPTION: State transition function that does the following:
//                    1. Tells the platform to stop broadcasting.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  next state is not modified- table state is used.
// *************************************************************************/
static void fnsStopBroadcast(PLATFORMDEVICE *pPlat )
{
  fnPlatSetWaitTime( pPlat , PLAT_Q_RESPONSE_TIME );
  fnPlatSendMessage( pPlat ,"Q");
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsRequestCount
// DESCRIPTION: State transition function that does the following:
//                    1. Requests calibrated counts/status from the platform.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  next state is not modified- table state is used.
// *************************************************************************/
static void fnsRequestCount(PLATFORMDEVICE *pPlat )
{
  fnPlatSetWaitTime( pPlat , PLAT_H_RESPONSE_TIME );
  fnPlatSendMessage( pPlat ,"H");

  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsRequestVersion     
// DESCRIPTION: State transition function that does the following:
//                    1. Requests version from the platform.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  next state is not modified- table state is used.
// *************************************************************************/
static void fnsRequestVersion(PLATFORMDEVICE *pPlat )
{
  fnPlatSetWaitTime( pPlat , PLAT_V_RESPONSE_TIME );
  cLastMessageSent = 'V';
  fnPlatSendMessage( pPlat ,"V");
}

/* **********************************************************************
// FUNCTION NAME: fnsRequestSerialNumber     
// DESCRIPTION: State transition function that does the following:
//                    1. Requests serial number from the platform.
//
// AUTHOR: Sandra Peterson
//
// NEXT STATE:  next state is not modified- table state is used.
// *************************************************************************/
static void fnsRequestSerialNumber(PLATFORMDEVICE *pPlat )
{
	if (pPlat)
	{
		if (fSentSerialNumberRequest == FALSE)
		{

#ifdef PLAT_DBG
			putString("fnsRequestSerialNumber: Req SN ", __LINE__);
#endif

			fSentSerialNumberRequest = TRUE;

			fnPlatSetWaitTime( pPlat , PLAT_S_RESPONSE_TIME );
			cLastMessageSent = 'S';
			(void)fnPlatSendMessage( pPlat ,"S");
		}
		else
		{
			fSentSerialNumberRequest = FALSE;
		}
	}
}

/* **********************************************************************
// FUNCTION NAME: fnsRequestCount
// DESCRIPTION: State transition function that does the following:
//                    1. Requests calibrated counts/status from the platform.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  next state is not modified- table state is used.
// *************************************************************************/
static void fnsRequestCalib(PLATFORMDEVICE *pPlat )
{
  fnPlatSetWaitTime( pPlat , PLAT_C_RESPONSE_TIME );
  fnPlatSendMessage( pPlat ,"C");

  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsUserZeroCfm
// DESCRIPTION: State transition function that does the following:
//                    1. Sends Z command to platform indicating there is
//                        no weight currently on the platform.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  next state is not modified- table state is used.
// *************************************************************************/
static void fnsUserZeroCfm(PLATFORMDEVICE *pPlat )
{
  fnPlatSetWaitTime( pPlat , PLAT_Z_RESPONSE_TIME );
  if(platDbg) putString("Zero SCALE", __LINE__);
  fnPlatSendMessage( pPlat ,"Z");
  fnPlatForceUpdate();

  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsUserRefCfm
// DESCRIPTION: State transition function that does the following:
//                    1. Sends F command to platform indicating that
//                        the calibration weight is currently on the platform.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  next state is not modified- table state is used.
// *************************************************************************/
static void fnsUserRefCfm(PLATFORMDEVICE *pPlat )
{
  fnPlatSetWaitTime( pPlat , PLAT_F_RESPONSE_TIME );
  fnPlatSendMessage( pPlat ,"F");

  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsUserCalibAbort
// DESCRIPTION: State transition function that does the following:
//                    1. Sends A command to platform indicating that
//                        we are aborting calibration.  The platform
//                        will auto-reboot after this command is issued.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  next state is not modified- table state is used.
// *************************************************************************/
static void fnsUserCalibAbort(PLATFORMDEVICE *pPlat )
{
  /* stop the platform message timer so that we don't receive any extraneous intertask messages.
     we want to wait for the intertask timer to expire after 10 seconds which will trigger us
     to start talking to the platform again */
  OSStopTimer(PLAT_MESSAGE_TIMER);
  fnPlatSetWaitTime( pPlat , 10000 );

  /* this will force a platform update message once we start talking to the platform after it reinitializes */
  pPlat->lwPrevCountReceived = pPlat->lwLastCountReceived;
  pPlat->ucPrevStatusReceived = pPlat->ucLastStatusReceived;
  pPlat->lwLastCountReceived = 0xFFFFFFFF;
  pPlat->ucLastStatusReceived = 0;

  fnPlatSendMessage( pPlat ,"A");

  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsReportUpdate
// DESCRIPTION: State transition function that does the following:
//                    1. Checks last status/count information received from
//                        platform and uses it to generate a platform 
//                        update message to the OIT.
//                    2. Sends another request for calibrated counts/status
//                        from the platform.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  next state is not modified- table state is used.
// MODIFICATION HISTORY:
//  07/12/2012  Liu Jupeng      Added code to fix the issue that the meter prints 
//                              the empty evelope.
// *************************************************************************/
static void fnsReportUpdate(PLATFORMDEVICE *pPlat )
{
  unsigned char     pMsg[PLAT_MAX_UPDATE_DATA_SIZE];
  unsigned char    bDataSize;
  ulong lwCurrentEvent = 0;

  // Track platform state reguardless of it being the active platform
  bDataSize =    fnPlatBuildUpdateMessage( pPlat , pMsg, fForcedUpdatePending);
  if(pPlat == pActivePlatform)
    {
      if (bDataSize)
    {
#ifdef PLATFORM_TRACING
      weightTrack[ wTrkIdx ].lwLastCount = pPlat->lwCount;
      weightTrack[ wTrkIdx ].lwPrevCount = pPlat->lwPrevCountReceived;
      weightTrack[ wTrkIdx ].lwWeight = pPlat->lwWeight;
      weightTrack[ wTrkIdx ].lwPrevWeight = pPlat->lwPrevWeightReceived;
      weightTrack[ wTrkIdx ].ulTime = PBSysTime();
      weightTrack[ wTrkIdx ].ulT1 = 0;
      weightTrack[ wTrkIdx ].ulT2 = 0;
      weightTrack[ wTrkIdx ].ulT3 = 0x99999999;
      wTrkIdx++;
      if( wTrkIdx == MAXWTRK )
        wTrkIdx = 0;
#endif
      {
        /*************************************************************************************************
         1. When the PC_AUTOSTART msg is rx'd by the CM, it clears this event 
            flag, to prevent the PLAT task from sending update messages to the 
            OI, because they might kick off a screen change, which would cause
            fnpIndiciaReady to run, which would access the IPSD, and might
            interfere with the debiting process, producing a blank envelope. 
            (Hey! It happened.) 
         2. The flag will be set when the S3 message is sent from the CM.  Then
            the platform update messages can again be sent to the OI.
         3. So if the flag is clear, suspend until it is set.
        *******************************************************************************************************/
        if(platDbg)
			putString( "PlatTsk: Suspend until PLAT_OK_TO_SEND_UPDATE", __LINE__);
        (void)OSReceiveEvents( PLAT_EVENT_GROUP, PLAT_OK_TO_SEND_UPDATE, OS_SUSPEND, OS_AND, &lwCurrentEvent );
		
        // When the flag is set, it's ok to send the update...
        if(platDbg)
			putString("Sending Weight Update", __LINE__);
        OSSendIntertask(OIT, PLAT, PLAT_UPDATE, BYTE_DATA, pMsg, bDataSize);
      }
    }
      fnPlatSetWaitTime( pPlat , PLAT_BROADCAST_DEADMAN_TIME );
#ifndef BROADCAST_SUPPORTED        // only for standard platform
      fnPlatSendMessage( pPlat ,"H");
#endif
      
      /* if we already received a platform update, there's no need to 
     send a "B" command since we must already be in broadcast mode */
    }
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsReportMotionTO
// DESCRIPTION: State transition function that does the following:
//                    1. Sends an update message to the OIT indicating
//                        a platform motion timeout has occurred.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  next state is not modified- table state is used.
// *************************************************************************/
static void fnsReportMotionTO(PLATFORMDEVICE *pPlat )
{
  unsigned char     pMsg[PLAT_MAX_UPDATE_DATA_SIZE];

  if(pPlat == pActivePlatform)
    {
      memset(pMsg, 0, 12);
      pMsg[0] = PLAT_MOTION_TIMEOUT;
      pMsg[1] = pPlat->ucUnits;
      pMsg[2] = pPlat->ucMode;
      if(platDbg) putString("Reporting Motion", __LINE__);
      OSSendIntertask(OIT, PLAT, PLAT_UPDATE, BYTE_DATA, pMsg, 12);
    }
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsReportZeroPrompt
// DESCRIPTION: State transition function that does the following:
//                    1. Sends a calibration zero prompt message to the
//                        OIT.  This tells it to instruct the user to remove
//                        weight from the platform and send a message back when done.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  next state is not modified- table state is used.
// *************************************************************************/
static void fnsReportZeroPrompt(PLATFORMDEVICE *pPlat )
{
  if(pPlat == pActivePlatform)
    {
      if(platDbg) putString("Zero Prompt", __LINE__);
      OSSendIntertask(OIT, PLAT, PLAT_CALIB_ZERO_PROMPT, NO_DATA, NULL, 0);
    }
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsReportRefPrompt
// DESCRIPTION: State transition function that does the following:
//                    1. Sends a calibration reference prompt message to the
//                        OIT.  This tells it to instruct the user to put the
//                        designated amount of calibration weight on the
//                        platform and send a message back when done.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  next state is not modified- table state is used.
// *************************************************************************/
static void fnsReportRefPrompt(PLATFORMDEVICE *pPlat )
{
  unsigned char         pMsg[5];
  unsigned long    lwCalibWeight;

  if(pPlat == pActivePlatform)
    {
	  if (fnGetCalibrationWeight(&lwCalibWeight) != TRUE)
	  {
		  lwCalibWeight = 0x00040000; // hard coded default fixed at 4.0 kilograms.
	  }

      pMsg[0] = METRIC;
      memcpy(&pMsg[1], &lwCalibWeight, sizeof(lwCalibWeight));
      if(platDbg) putString("Reference Prompt", __LINE__);
      //TODO - send to tablet instead of OIT
      OSSendIntertask(OIT, PLAT, PLAT_CALIB_REF_PROMPT, BYTE_DATA, pMsg, 5);
    }
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsStoreCalibData
// DESCRIPTION: State transition function that does the following:
//                    1. Sends the Y command to the platform to get it
//                        to store calibration values in its NVM.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  next state is not modified- table state is used.
// *************************************************************************/
static void fnsStoreCalibData(PLATFORMDEVICE *pPlat )
{

  if(pPlat == pActivePlatform)
    {
      /* stop the platform message timer so that we don't receive any extraneous intertask messages.
     we want to wait for the intertask timer to expire after 10 seconds which will trigger us
     to start talking to the platform again */
      OSStopTimer(PLAT_MESSAGE_TIMER);
      fnPlatSetWaitTime( pPlat , 10000 );
      
      /* this will force a platform update message once we start talking to the platform after it reinitializes */
      pPlat->lwPrevCountReceived = pPlat->lwLastCountReceived;
      pPlat->ucPrevStatusReceived = pPlat->ucLastStatusReceived;
      pPlat->lwLastCountReceived = 0xFFFFFFFF;
      pPlat->ucLastStatusReceived = 0;
      
      fCMOSScaleLocallyCalibrated = TRUE;
      fnPlatSendMessage( pPlat ,"Y");
    }
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsRetryPlatHMessage
// DESCRIPTION: State transition function that does the following:
//                    1. Retries sending the platform "H" message.  If
//                        maximum retry count already reached, we change
//                        to the disconnected state and inform the OIT.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  PLAT_STATE_DISCONNECTED if max retry count already reached
//                table state is used otherwise
// *************************************************************************/
static void fnsRetryPlatHMessage(PLATFORMDEVICE *pPlat )
{

  if(pPlat == pActivePlatform)
    {
      if (pPlat->ucPlatformRetries < PLAT_MAX_MESSAGE_RETRIES)
    {
      pPlat->ucPlatformRetries++;
      fnPlatSetWaitTime( pPlat , PLAT_H_RESPONSE_TIME );
#ifndef BROADCAST_SUPPORTED        // only for standard platform
      fnPlatSendMessage( pPlat ,"H");
#endif
    }
      else
    {
      pPlat->ucPlatformRetries = 0;
      if(pPlat->cPlatformVersion[0] == 'C') // 70 lb scale, do polling
        {
          // pPlat->cPlatformVersion[0] = 0;
          // platLocalized = NOT_LOCALIZED;
          OSStopTimer(PLAT_POLLING_TIMER);
          OSStopTimer(PLAT_LOCALIZATION_TIMER); 
          if(platDbg) putString("Stop POLLING", __LINE__);
        }
      // set the platform disconnect event flag
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_DISCONNECT_EVT, OS_OR);
      // set the disconnect message to OIT
      OSSendIntertask(OIT, PLAT, PLAT_DISCONNECT, NO_DATA, NULL, 0);
    }
    }

  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsReconfigureUART
// DESCRIPTION: State transition function that does the following:
//                    1. Log information that will help us detect the root
//                        cause for the broadcasts stopping.
//                    2. Reconfigures the UART so that messages hopefully
//                        resume coming in from the platform.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  table state is used
// *************************************************************************/
static void fnsReconfigureUART(PLATFORMDEVICE *pPlat )
{
  char    buf[60];
  unsigned short    wAsicScr2;
  unsigned short     bLen;
  unsigned char    pCfgbuf[32];
    
  if(pPlat == pActivePlatform)
    {
      if(pPlat->cPlatformVersion[0] == 'C') 
    {
      OSStopTimer(PLAT_POLLING_TIMER);
      if(platDbg) putString("Stop POLLING", __LINE__);
    }
      
      /* log debug information */
//TODO - move into HAL and delete here
#if 0  // HAL? not sure if this even belongs there
      wAsicScr2 = ASICSCR2;
      sprintf(buf, "Plat broadcast timeout...ASICSCR2:%04X", wAsicScr2);
#endif
      fnSystemLogEntry(SYSLOG_TEXT, buf, 38);
      sprintf(buf, "RX in: %08lX  RX out: %08lX", lwRXISRIn, lwRXISROut);
      fnSystemLogEntry(SYSLOG_TEXT, buf, 33);
      sprintf(buf, "TX in: %08lX  TX out: %08lX", lwTXISRIn, lwTXISROut);
      fnSystemLogEntry(SYSLOG_TEXT, buf, 33);
      
      //    fnLEDSetStatus(PANEL_RED_LED, PANEL_LED_BLINK);
      //    fnLEDSetStatus(PANEL_GREEN_LED, PANEL_LED_OFF);
      
      /* DSR check */
      if (pPlat->fLastScaleConnectState)
    {
      fnPlatSetWaitTime( pPlat , PLAT_BROADCAST_DEADMAN_TIME );
      if(pPlat->cPlatformVersion[0] == 'C') // 70 lb scale, do polling
        {
          OSStartTimer(PLAT_POLLING_TIMER);
          if(platDbg) putString("Start POLLING", __LINE__);
        }
      else
        {
          fnPlatSendMessage( pPlat ,"B");
        }
      
      // all transitions to the weighing state require the motion timer to be started 
      // fnPlatRestartMotionTimer(FALSE);
    }
    }
  return;
  
}

/* **********************************************************************
// FUNCTION NAME: fnsRetryPlatBMessage
// DESCRIPTION: State transition function that does the following:
//                    1. Retries sending the platform "B" message.  If
//                        maximum retry count already reached, we change
//                        to the disconnected state and inform the OIT.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  PLAT_STATE_DISCONNECTED if max retry count already reached
//                table state is used otherwise
// *************************************************************************/
static void fnsRetryPlatBMessage(PLATFORMDEVICE *pPlat )
{

  if(pPlat == pActivePlatform)
    {
      if (pPlat->ucPlatformRetries < PLAT_MAX_MESSAGE_RETRIES)
    {
      pPlat->ucPlatformRetries++;
      fnPlatSetWaitTime( pPlat , PLAT_BROADCAST_DEADMAN_TIME );
      fnPlatSendMessage( pPlat ,"B");
      if(platDbg) putString("Retry Broadcast", __LINE__);
    }
    }
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsRetryPlatVMessage
// DESCRIPTION: State transition function that does the following:
//                    1. Retries sending the platform "V" message.  If
//                        maximum retry count already reached, we change
//                        to the disconnected state and inform the OIT.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  PLAT_STATE_DISCONNECTED if max retry count already reached
//                table state is used otherwise
// *************************************************************************/
static void fnsRetryPlatVMessage(PLATFORMDEVICE *pPlat )
{

  if(pPlat == pActivePlatform)
    {
      if (pPlat->ucPlatformRetries < PLAT_MAX_MESSAGE_RETRIES)
    {
      pPlat->ucPlatformRetries++;
      fnPlatSetWaitTime( pPlat , PLAT_V_RESPONSE_TIME );
      cLastMessageSent = 'V';
      fnPlatSendMessage( pPlat ,"V");
      if(platDbg) putString("Retry Ver", __LINE__);
    }
      else
    {
      cLastMessageSent = 'S';			// reset to default power-up value
      pPlat->ucPlatformRetries = 0;
      if(pPlat->cPlatformVersion[0] == 'C') // 70 lb scale, do polling
        {
          // pPlat->cPlatformVersion[0] = 0;
          // platLocalized = NOT_LOCALIZED;
          OSStopTimer(PLAT_POLLING_TIMER);
          OSStopTimer(PLAT_LOCALIZATION_TIMER); 
          if(platDbg) putString("Stop POLLING", __LINE__);
        }
      // set the platform connect event flag
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_DISCONNECT_EVT, OS_OR);
      // set the disconnect message to OIT
      OSSendIntertask(OIT, PLAT, PLAT_DISCONNECT, NO_DATA, NULL, 0);
    }
    }
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsRetryPlatSMessage
// DESCRIPTION: State transition function that does the following:
//              1. Retries sending the platform "S" message.  If
//                 maximum retry count already reached, we change
//                 to requesting the platform version.
//
// AUTHOR: Sandra Peterson
//
// NEXT STATE:  unchanged
// MODIFICATION HISTORY:
//	09-25-2012	Bob Li		Updated this function to fix the issue about serial platform calibration error.
// *************************************************************************/
static void fnsRetryPlatSMessage(PLATFORMDEVICE *pPlat )
{
  if(pActivePlatform && (pPlat == pActivePlatform))
  {
      if (pPlat->ucPlatformRetries < PLAT_MAX_S_MESSAGE_RETRIES)
      {
          fSentSerialNumberRequest = TRUE;
          pPlat->ucPlatformRetries++;
          fnPlatSetWaitTime( pPlat , PLAT_S_RESPONSE_TIME );
		  cLastMessageSent = 'S';
          (void)fnPlatSendMessage( pPlat ,"S");
      }
      else
      {
          fSentSerialNumberRequest = FALSE;
          pPlat->ucPlatformRetries = 0;
		  pPlat->cPlatformSN[0] = 0;

          fnsRequestVersion( pPlat );
      }
  }
}

/* **********************************************************************
// FUNCTION NAME: fnsRetryPlatMessage
// DESCRIPTION: Depending on the last message that has been sent to the 
//              scale, this function will invoke fnsRetryPlatVMessage
//              (for "V" message) or fnsRetryPlatSMessage (for "S" message)
//
// AUTHOR: Sandra Peterson
//
// NEXT STATE:  See called functions
// *************************************************************************/
static void fnsRetryPlatMessage(PLATFORMDEVICE *pPlat)
{
  if(pActivePlatform && (pPlat == pActivePlatform))
  {
    if (cLastMessageSent == 'S')
    {
		fnsRetryPlatSMessage(pPlat);
    }
    else
    {
		fnsRetryPlatVMessage(pPlat);
    }
  }
}


/* **********************************************************************
// FUNCTION NAME: fnsRetryPlatZMessage
// DESCRIPTION: State transition function that does the following:
//                    1. Retries sending the platform "Z" message.  If
//                        maximum retry count already reached, we change
//                        to the disconnected state and inform the OIT.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  PLAT_STATE_DISCONNECTED if max retry count already reached
//                table state is used otherwise
// *************************************************************************/
static void fnsRetryPlatZMessage(PLATFORMDEVICE *pPlat )
{

  if(pPlat == pActivePlatform)
    {
      if (pPlat->ucPlatformRetries < PLAT_MAX_MESSAGE_RETRIES)
    {
      pPlat->ucPlatformRetries++;
      fnPlatSetWaitTime( pPlat , PLAT_Z_RESPONSE_TIME );
      fnPlatSendMessage( pPlat ,"Z");
          fnPlatForceUpdate();
      if(platDbg) putString("Zero SCALE", __LINE__);
    }
      else
    {
      pPlat->ucPlatformRetries = 0;
      if(pPlat->cPlatformVersion[0] == 'C') // 70 lb scale, do polling
        {
          // pPlat->cPlatformVersion[0] = 0;
          // platLocalized = NOT_LOCALIZED;
          OSStopTimer(PLAT_POLLING_TIMER);
          OSStopTimer(PLAT_LOCALIZATION_TIMER); 
          if(platDbg) putString("Stop POLLING", __LINE__);
        }
      // set the platform disconnect event flag
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_DISCONNECT_EVT, OS_OR);
      // set the disconnect message to OIT
      OSSendIntertask(OIT, PLAT, PLAT_DISCONNECT, NO_DATA, NULL, 0);
    }
    }

  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsReportDisconnect
// DESCRIPTION: State transition function that does the following:
//                    1. Sends a PLAT_DISCONNECT message to the OIT.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  table state is used
// *************************************************************************/
static void fnsReportDisconnect(PLATFORMDEVICE *pPlat )
{
  if(pPlat == pActivePlatform)
    {
      OSStopTimer(PLAT_MESSAGE_TIMER);
      
      if(pPlat->cPlatformVersion[0] == 'C') // 70 lb scale, do polling
    {
      pPlat->platLocalized = NOT_LOCALIZED;
      OSStopTimer(PLAT_POLLING_TIMER);
      OSStopTimer(PLAT_LOCALIZATION_TIMER); 
      if(platDbg) putString("Stop POLLING", __LINE__);
    }
 
	 pPlat->cPlatformSN[0] = 0;
	 pPlat->cPlatformModel[0] = 0;  
     pPlat->cPlatformVersion[0] = 0;
     pPlat->fPlatformVersionValid = FALSE;
     pPlat->ulPlatformLimit = 0;
	 fSentSerialNumberRequest = FALSE;
	 cLastMessageSent = 'S';
	 pPlat->ulRangeIncrement = 0;
      
     if(platDbg) putString("Reporting Disconnect", __LINE__);
      // set the platform disconnect event flag
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_DISCONNECT_EVT, OS_OR);
      // set the disconnect message to OIT
      OSSendIntertask(OIT, PLAT, PLAT_DISCONNECT, NO_DATA, NULL, 0);
    }
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsScheduleZero
// DESCRIPTION: State transition function that does the following:
//                    1. Stores the zeroing operation in the scheduling variable
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  next state is not modified- table state is used.
// *************************************************************************/
static void fnsScheduleZero(PLATFORMDEVICE *pPlat )
{
  if(pPlat == pActivePlatform)
    {
      pPlat->ucScheduledOp = PLAT_OP_ZERO;
    }
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnsPerformScheduledOp
// DESCRIPTION: State transition function that does the following:
//                    1. Performs the operation as stored in the 
//                        scheduling variable.
//                    2. Changes next state to the one that goes with
//                        the designated operation.
//
//                Operations get "scheduled" because we don't want to
//                screw up the platform by sending two consecutive commands
//                without waiting for a response in between.  In other words,
//                during the normal weighing poll cycle, we are pretty much
//                always waiting for count or status message from the platform
//                and one of these must be received before we issue a command
//                such as zero or calibrate that is received from the OIT.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  PLAT_STATE_ZEROING if a zero operation was pending
//                PLAT_STATE_CALIBRATING if a calibration operation was pending
// *************************************************************************/
static unsigned long fnsPerformScheduledOp(PLATFORMDEVICE *pPlat )
{
    unsigned long ret = PLAT_DONT_CHANGE_STATE;   // No state change

    if(pPlat == pActivePlatform)
    {
        switch (pPlat->ucScheduledOp)
        {
            case PLAT_OP_ZERO:
            if(   ((pPlat->platLocalized == LOCALIZED) && (pPlat->cPlatformVersion[0] == 'C') )
                || (pPlat->cPlatformVersion[0] != 'C') )
            {
              fnPlatSetWaitTime( pPlat , PLAT_Z_RESPONSE_TIME );
              fnPlatSendMessage( pPlat ,"Z");
                  fnPlatForceUpdate();
              if(platDbg) putString("Zero SCALE", __LINE__);
              pPlat->ucScheduledOp = PLAT_OP_NONE;
              ret = PLAT_STATE_ZEROING;
              return(ret);
            }
            break;
      
            case PLAT_OP_LOCALIZE:
            // (ML) if(pPlat->cPlatformVersion[0] == 'C' && fnPlatIsAttached(pPlat))
            // Do it for any platform...fnPlatLocalize() will handle various platform checks..
            if(fnPlatIsAttached())
            {
              pPlat->platLocalized = NOT_LOCALIZED;
              OSStopTimer(PLAT_POLLING_TIMER);
              OSStopTimer(PLAT_LOCALIZATION_TIMER); 
              
              if(platDbg)
                putString("LOCALIZE START", __LINE__);
              fnPlatLocalize(PLAT_LOCALIZE_START, NULL);
              // (ML)
              ret = PLAT_STATE_WEIGHING;
              return(ret);
            }
            break;
              
            default:
                break;
        }
    }
            
    return (ret);
}

/* **********************************************************************
// FUNCTION NAME: fnsGoSleep
// DESCRIPTION: State function that stops broadcasting 
//              
//
// AUTHOR: Howell Sun 
//
// INPUTS:    pIntertask- intertask message that is being processed
// *************************************************************************/
static void fnsGoSleep(PLATFORMDEVICE *pPlat )
{
  unsigned char    sleepRsp[2];

  if(pPlat == pActivePlatform)
    {
      switch(pPlat->cPlatformVersion[0])
    {
    case 'D': // For standard platform
    case 'E': // For standard platform
    case 'F': // For standard platform
      fnPlatSendMessage( pPlat ,"Q"); // Wait for response from platform to set event
      break;
      
    case 'C': // For large platform
      OSStopTimer(PLAT_POLLING_TIMER); 
      OSStopTimer(PLAT_LOCALIZATION_TIMER); 
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_IN_SLEEP_EVT, OS_OR);
      if(platDbg) putString("Set PLAT_IN_SLEEP_EVT, stopped poll", __LINE__);
      break;
      
    default:  // For other platforms
      // Respond to OI immediately for other platforms
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_IN_SLEEP_EVT, OS_OR);
      if(platDbg) putString("Set PLAT_IN_SLEEP_EVT", __LINE__);
      break;
    }
    }
} // fnsGoSleep()


/* **********************************************************************
// FUNCTION NAME: fnsWakeup
// DESCRIPTION: Message handling function that starts broadcasting
//
// AUTHOR: Howell Sun 
//
// INPUTS:    pIntertask- intertask message that is being processed
// *************************************************************************/
static void fnsWakeup(PLATFORMDEVICE *pPlat )
{
  unsigned char    wakeupRsp[2];

  if(pPlat == pActivePlatform)
    {
      switch(pPlat->cPlatformVersion[0])
    {
    case 'D': // For standard usb platform
    case 'E': // For standard usb platform
    case 'F': // For standard usb platform
    fnPlatSetWaitTime( pPlat , PLAT_BROADCAST_DEADMAN_TIME );
      fnPlatSendMessage( pPlat ,"B");
      if(platDbg) putString("Set PLAT_WAKE_UP_EVT, started broadcast", __LINE__);
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_WAKE_UP_EVT, OS_OR);
      break;
      
    case 'C': // For large platform
      if(platDbg) putString("Set PLAT_WAKE_UP_EVT, started poll", __LINE__);
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_WAKE_UP_EVT, OS_OR);
      fnPlatPollWeight(pPlat , __LINE__);
      break;
      
    default:  // For other platforms
      // Respond to OI immediately for other platforms
      if(platDbg) putString("Set PLAT_WAKE_UP_EVT", __LINE__);
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_WAKE_UP_EVT, OS_OR);
      break;
      
    }
    }
} // fnsWakeup()



/* **********************************************************************
// FUNCTION NAME: fnsDummyGoSleep
// DESCRIPTION: State function that sets needed(faked) event to OI 
//
// AUTHOR: Howell Sun 
//
// INPUTS:    pIntertask- intertask message that is being processed
// *************************************************************************/
static void fnsDummyGoSleep(PLATFORMDEVICE *pPlat )
{
  if(pPlat == pActivePlatform)
    {
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_IN_SLEEP_EVT, OS_OR);
      if(platDbg) putString("Set PLAT_IN_SLEEP_EVT, dummy", __LINE__);
    }
}


/* **********************************************************************
// FUNCTION NAME: fnsDummyWakeup
// DESCRIPTION: State function that sets needed(faked) event to OI 
//
// AUTHOR: Howell Sun 
//
// INPUTS:    pIntertask- intertask message that is being processed
// *************************************************************************/
static void fnsDummyWakeup(PLATFORMDEVICE *pPlat )
{
  if(pPlat == pActivePlatform)
    {
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_WAKE_UP_EVT, OS_OR);
      if(platDbg) putString("Set PLAT_WAKE_UP_EVT, dummy", __LINE__);
    }
}



/*---------------------------------------*/
/*   Message handling functions (fnm*)   */
/*---------------------------------------*/
/* **********************************************************************
// FUNCTION NAME: fnmProcessPlatMessage
// DESCRIPTION: Message handling function that examines the data received
//                    from the platform, stores it in the proper locations,
//                    and generates the appropriate state transition event.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    pIntertask- intertask message that is being processed
// *************************************************************************/
static BOOL fnmProcessPlatMessage(PLATFORMDEVICE *pPlat ,INTERTASK *pIntertask)
{
    PB232BUFFER     *pPB232Msg;         /* Points to a PB232 message buffer.  This
                     is the structure returned by the driver
                     even though this is not a PB232 message. */
    unsigned long   msgLen;
	PB232BUFFER		localPB232Msg;
    char *			pStr;
    char            pLogBuf[50];
    unsigned char	ucLen;
    BOOL fIsFilteringOn = FALSE;


	// get appropriate pointer to message
    if (pIntertask->bMsgType == PART_PTR_DATA)
    {
        pPB232Msg = ( PB232BUFFER *) pIntertask->IntertaskUnion.PointerData.pData;
        msgLen = pIntertask->IntertaskUnion.PointerData.lwLength;
    }
    else
    {
        pPB232Msg = ( PB232BUFFER *) pIntertask->IntertaskUnion.CommData.bByteData;
        msgLen = pIntertask->IntertaskUnion.CommData.bLength;
    }
    // copy to local buffer
    (void)memcpy((unsigned char *)&(localPB232Msg.bData), (unsigned char *)pPB232Msg, MAXPB232MESS);
    localPB232Msg.bData[msgLen] = 0; // Add NULL at the end of string

    pollingRetry = 0;

#ifdef PLATFORM_TRACING
      weightTrack[ wTrkIdx ].lwLastCount = 0;
      weightTrack[ wTrkIdx ].lwPrevCount = 0;
      weightTrack[ wTrkIdx ].lwWeight = 0;
      weightTrack[ wTrkIdx ].lwPrevWeight = 0;
      weightTrack[ wTrkIdx ].ulTime = PBSysTime();
      weightTrack[ wTrkIdx ].ulT1 = pPlat->ulCurrState;
      weightTrack[ wTrkIdx ].ulT2 = localPB232Msg.bData[0];
      weightTrack[ wTrkIdx ].ulT3 = 0x99aa99aa;
      wTrkIdx++;
      if( wTrkIdx == MAXWTRK )
        wTrkIdx = 0;
#endif

  /* decode the received message */
  if (localPB232Msg.bData[0] == '?')
    {
      // If trying to establish communications w/ the platform, ignore any status messages
      if (pPlat->ulCurrState == PLAT_STATE_ESTABLISHING_COMM)
          goto xit;

      pPlat->ucPlatformRetries = 0;
      switch (localPB232Msg.bData[1])
        {
    case 'C':
    
      // set up to restart the filtering of the status/count messages from the platform
      fnPlatRestartFilter(pPlat);

      fnPlatProcessSTEvent(pPlat , PLAT_EV_CALIB_C_RCVD);
      break;

    case 'F':
    
      // set up to restart the filtering of the status/count messages from the platform
      fnPlatRestartFilter(pPlat);
    
      fnPlatProcessSTEvent(pPlat , PLAT_EV_CALIB_F_RCVD);
      break;

    case 'Q':
    
      // set up to restart the filtering of the status/count messages from the platform
      fnPlatRestartFilter(pPlat);
      
      // Do not need process for now, as Platform only send Q command when going sleep
      // fnPlatProcessSTEvent(PLAT_EV_QUIT_BROADCAST_RCVD);
      // sleepRsp[0] = 0;
      // sleepRsp[1] = 0;
      // OSSendIntertask(OIT, PLAT, PLAT_GO_SLEEP_RSP, BYTE_DATA, sleepRsp, sizeof(sleepRsp));
      OSSetEvents(PLAT_EVENT_GROUP, PLAT_IN_SLEEP_EVT, OS_OR);
      if(platDbg) putString("Set PLAT_IN_SLEEP_EVT", __LINE__);
      break;

    default:
      /* must be a status byte.*/  
      if((pPlat->platLocalized != LOCALIZED) && pPlat->cPlatformVersion[0] == 'C') 
        {
          // set up to restart the filtering of the status/count messages from the platform
          fnPlatRestartFilter(pPlat);
        
          fnPlatLocalize( PLAT_LOCALIZE_FAILED, localPB232Msg.bData);
        }
      else
        {
          //we ride out any status changes
          pPlat->ucPrevStatusReceived = pPlat->ucLastStatusReceived;
          pPlat->ucLastStatusReceived = localPB232Msg.bData[1];
          if (!(pPlat->ucLastStatusReceived & IN_MOTION))
            fnPlatRestartMotionTimer(TRUE);
			
          fnPlatProcessSTEvent(pPlat , PLAT_EV_STATUS_RCVD);
        }

      break;

        } // switch
    } // if (localPB232Msg.bData[0] == '?')

  /* if first character is a letter, this is either a version response or a localization response */
  if (isalpha(localPB232Msg.bData[0]))
    {
    
      // set up to restart the filtering of the status/count messages from the platform
      fnPlatRestartFilter(pPlat);
    
      OSWakeAfter(100);   // Adding 100ms delay so platform has more time to respond
      if( (localPB232Msg.bData[0]  == 'G') && (pPlat->cPlatformVersion[0] == 'C') ) // gravity localization response
        {
      fnPlatLocalize(PLAT_LOCALIZE_RSP, localPB232Msg.bData);
        }
      else // version response
        {
      pPlat->ucPlatformRetries = 0;

      fIsFilteringOn = fnPlatGetFilteringSetting();

      // Check the platform type, start polling for large scale
      switch(localPB232Msg.bData[0])
      {
        case 'C': // 70 lb serial scale
			pPlat->ucScaleType = NON_WM_PLATFORM;

			// if switching from a W&M platform, need to decide if the location code
			// has already been entered
			if (fCMOSScaleLocationCodeInited == WM_INIT_BEFORE)
				fCMOSScaleLocationCodeInited = FALSE;
			else if (fCMOSScaleLocationCodeInited == WM_INIT_AFTER)
				fCMOSScaleLocationCodeInited = TRUE;

			fnPlatSetFilteringCount(fIsFilteringOn, pPlat);
	            
			// blindly copy the version so we'll have a version
			(void)strncpy( pPlat->cPlatformVersion, (char *)localPB232Msg.bData, PLAT_MAX_VERSION_SIZE );
			pPlat->cPlatformVersion[ PLAT_MAX_VERSION_SIZE ] = 0;
			pPlat->fPlatformVersionValid = TRUE;
			fnPlatProcessSTEvent(pPlat , PLAT_EV_VERSION_RCVD);

			// We'll need to figure out later what the platform model is
			pPlat->cPlatformModel[0] = 0;

			if(    fCMOSScaleLocationCodeInited && (bCMOSScaleLocationCode < 32) 
				 && (!fCMOSScaleLocallyCalibrated) && (pPlat->platLocalized == NOT_LOCALIZED) ) 
			{
				if(platDbg) putString("LOCALIZE START", __LINE__);
				fnPlatLocalize(PLAT_LOCALIZE_START, NULL);
			}
			else
			{
				OSStartTimer(PLAT_POLLING_TIMER); 
				if(platDbg) putString("Start POLLING", __LINE__);
			}
			break;

        case 'B': // bigfoot serial scale
			pPlat->ucScaleType = NON_WM_PLATFORM;

            // if switching from a W&M platform, need to decide if the location code
            // has already been entered
            if (fCMOSScaleLocationCodeInited == WM_INIT_BEFORE)
                fCMOSScaleLocationCodeInited = FALSE;
            else if (fCMOSScaleLocationCodeInited == WM_INIT_AFTER)
                fCMOSScaleLocationCodeInited = TRUE;

            fnPlatSetFilteringCount(fIsFilteringOn, pPlat);
            
			// blindly copy the version so we'll have a version
			(void)strncpy( pPlat->cPlatformVersion, (char *)localPB232Msg.bData, PLAT_MAX_VERSION_SIZE );
			pPlat->cPlatformVersion[ PLAT_MAX_VERSION_SIZE ] = 0;
			pPlat->fPlatformVersionValid = TRUE;
            fnPlatProcessSTEvent(pPlat , PLAT_EV_VERSION_RCVD);

			// We'll need to figure out later what the platform model is
			pPlat->cPlatformModel[0] = 0;
	        break;

        case 'D': // For a standard usb platform
        case 'E': // For standard usb platform
        case 'F': // For standard usb platform
    		if (localPB232Msg.bData[1] == '4')
			{
			    pPlat->ucScaleType = WM_PLATFORM;

				// W&M platforms are calibrated via their external displays
				fCMOSScaleLocallyCalibrated = TRUE;

				// Switching to a W&M platform, need to indicate if the W&M platform was
				// attached before or after the scale location code was initialized w/ a
				// non-W&M platform	
				if (fCMOSScaleLocationCodeInited == TRUE)
					fCMOSScaleLocationCodeInited = WM_INIT_AFTER;
				else if (fCMOSScaleLocationCodeInited == FALSE)
					fCMOSScaleLocationCodeInited = WM_INIT_BEFORE;
			}
			else
			{
			    pPlat->ucScaleType = NON_WM_PLATFORM;

				// Switching from a W&M platform, need to decide if the location code
				// has already been entered
				if (fCMOSScaleLocationCodeInited == WM_INIT_BEFORE)
				    fCMOSScaleLocationCodeInited = FALSE;
				else if (fCMOSScaleLocationCodeInited == WM_INIT_AFTER)
					fCMOSScaleLocationCodeInited = TRUE;
			}

			fnPlatSetFilteringCount(fIsFilteringOn, pPlat);
            
			// blindly copy the version so we'll have a version
			(void)strncpy( pPlat->cPlatformVersion, (char *)localPB232Msg.bData, PLAT_MAX_VERSION_SIZE );
			pPlat->cPlatformVersion[ PLAT_MAX_VERSION_SIZE ] = 0;
			pPlat->fPlatformVersionValid = TRUE;

            ucLen = (unsigned char)strlen(pPlat->cPlatformVersion);
            
            if (ucLen <= (msgLen - 4))
            {
                pPlat->ulPlatformLimit = (localPB232Msg.bData[ucLen+1] << 24) +
                                         (localPB232Msg.bData[ucLen+2] << 16) +
                                         (localPB232Msg.bData[ucLen+3] << 8) +
                                          localPB232Msg.bData[ucLen+4] ;
            }   
            else
            {
                (void)sprintf(pLogBuf, "ByteCnt=%02X,MsgCnt=%08X,OutLimit=%08X", ucLen, (unsigned int) msgLen, (unsigned int) pPlat->ulPlatformLimit);
                fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
            }
            

			  
			// find the platform model
			pStr = strstr(pPlat->cPlatformVersion, "-");
			if (pStr)
			{
				// increment past the dash
				pStr++;
				(void)strcpy(pPlat->cPlatformModel, pStr);
			}
			  

            fnPlatProcessSTEvent(pPlat , PLAT_EV_VERSION_RCVD);
            if(pPlat->platLocalized == NOT_LOCALIZED)
                fnPlatLocalize(PLAT_LOCALIZE_START, NULL);

            break;

        case 'W': // For Weights & Measures serial platform
			pPlat->ucScaleType = WM_PLATFORM;

			// need to indicate if the W&M platform was attached before or after the
			// scale location code was initialized w/ a non-W&M platform	
			if (fCMOSScaleLocationCodeInited == TRUE)
				fCMOSScaleLocationCodeInited = WM_INIT_AFTER;
			else if (fCMOSScaleLocationCodeInited == FALSE)
				fCMOSScaleLocationCodeInited = WM_INIT_BEFORE;

            fnPlatSetFilteringCount(fIsFilteringOn, pPlat);
            
			// blindly copy the version so we'll have a version
			(void)strncpy((char *)pPlat->cPlatformVersion, (char *)localPB232Msg.bData, PLAT_MAX_VERSION_SIZE);
			pPlat->cPlatformVersion[PLAT_MAX_VERSION_SIZE] = 0;
			pPlat->fPlatformVersionValid = TRUE;
			(void)fnPlatProcessSTEvent(pPlat , PLAT_EV_VERSION_RCVD);

			// We'll need to figure out later what the platform model is
			pPlat->cPlatformModel[0] = 0;
	        break;  

		case 'S': // Serial Number
			// copy the serial number if there is one
			if (localPB232Msg.bData[1] != 0)
			{
				(void)strncpy(pPlat->cPlatformSN, (char *)&localPB232Msg.bData[1], PLAT_MAX_SN_SIZE);
				pPlat->cPlatformSN[PLAT_MAX_SN_SIZE] = 0;
			}

			(void)fnPlatProcessSTEvent(pPlat , PLAT_EV_SN_RCVD);
			break;

        default:  
            break;
            }
        }

      goto xit;
    }

  /* if first character is a digit, this must be a count message for a stable weight */
  if (isdigit(localPB232Msg.bData[0]))
    {
	  // If trying to establish communications w/ the platform, ignore any count messages
	  if (pPlat->ulCurrState == PLAT_STATE_ESTABLISHING_COMM)
	  	goto xit;
	  	
      if(pPlat->cPlatformVersion[0] == 'C')
      {
          strncpy( (char *)pPlat->ucLastStringWeight, (char *)localPB232Msg.bData, MAX_WT_STR_LEN );
          strncpy( (char *)pPlat->ucLastStringWeight, (char *)localPB232Msg.bData, MAX_WT_STR_LEN );
          pPlat->ucLastStringWeight[ MAX_WT_STR_LEN ] = 0;
      }
      else
      {
      	  pPlat->lwPrevCountReceived = pPlat->lwLastCountReceived;
      	  pPlat->ucPrevStatusReceived = pPlat->ucLastStatusReceived;
		  for (ucLen = 0; ucLen < msgLen; ucLen++)
		  {
		  	  if (localPB232Msg.bData[ucLen] == ',')
			  {
			      // replace the comma w/ a null terminator
				  localPB232Msg.bData[ucLen++] = 0;
			      break;
              }
		  }
		  

      	  pPlat->lwLastCountReceived = (unsigned long) atol((char *)localPB232Msg.bData);
		  if (ucLen == msgLen)
			  pPlat->ulRangeIncrement = 0;
		  else
			  pPlat->ulRangeIncrement = (unsigned long)atol((char *)&localPB232Msg.bData[ucLen]);
      }

      pPlat->ucLastStatusReceived = 0;    /* means last thing received was a count, not a status */
      pPlat->ucPlatformRetries = 0;

      /* when we receive a stable weight we are not in motion, so restart the motion timer */
      fnPlatRestartMotionTimer(TRUE);

      fnPlatProcessSTEvent(pPlat , PLAT_EV_COUNT_RCVD);
      goto xit;
    }

 xit:
  return (SUCCESSFUL);
}


/* **********************************************************************
// FUNCTION NAME: fnmProcessTimerMessage
// DESCRIPTION: Message handling function that send polling message to 
//                    platfrom 
//
// AUTHOR: Howell Sun 
//
// INPUTS:    pIntertask- intertask message that is being processed
// *************************************************************************/
static BOOL fnmProcessTimerMessage(PLATFORMDEVICE *pPlat , INTERTASK *pIntertask)
{
  if( fCMOSScaleLocationCodeInited  && (bCMOSScaleLocationCode < 32) && (!fCMOSScaleLocallyCalibrated)
      && (pActivePlatform->platLocalized == NOT_LOCALIZED) && (pActivePlatform->cPlatformVersion[0] == 'C') )
    {
      if(platDbg)
    putString("LOCALIZE START", __LINE__);
      fnPlatLocalize(PLAT_LOCALIZE_START, NULL);
    }
  else
    {
      // Need this for disconnected scale, to clear pending messages in the queue
      if( pActivePlatform->cPlatformVersion[0] == 'C' &&
      fnPlatIsAttached() &&
      pActivePlatform->platLocalized == LOCALIZED )
    {
      if(platDbg)
        putString("Start fnPlatPollWeight", __LINE__);
      fnPlatPollWeight(pActivePlatform , __LINE__);
      pollingRetry++;
    }
      else
    {
      if(platDbg)
        putString("Stop POLLING", __LINE__);
      OSStopTimer(PLAT_POLLING_TIMER); 
    }
    }
  return (SUCCESSFUL);

} // fnmProcessPlatMessage()


/* **********************************************************************
// FUNCTION NAME: fnmProcessOitLocalizeReq
// DESCRIPTION: Message handling function that starts localization
//
// AUTHOR: Howell Sun 
//
// INPUTS:    pIntertask- intertask message that is being processed
// *************************************************************************/
static void fnsScheduleLocalize(PLATFORMDEVICE *pPlat )
{
  pPlat->ucScheduledOp = PLAT_OP_LOCALIZE;
  return;
}



/* **********************************************************************/
// FUNCTION NAME: fnmProcessOitModeReq
// DESCRIPTION: Message handling function that processes a mode change
//                    request from the OIT.
//
// AUTHOR: Howell Sun

/* **********************************************************************/
// FUNCTION NAME: fnmProcessOitModeReq
// DESCRIPTION: Message handling function that processes a mode change
//                    request from the OIT.
//
// AUTHOR: Howell Sun
//
// INPUTS:    pIntertask- intertask message that is being processed
// *************************************************************************/
static BOOL fnmProcessOitModeReq(PLATFORMDEVICE *pPlat ,INTERTASK *pIntertask)
{
  unsigned char    modeRsp[2];

  fnPlatForceUpdate();
  switch(pIntertask->IntertaskUnion.bByteData[0])
    {
    case PLAT_MODE_DIFFERENTIAL:
      pActivePlatform->ucMode = PLAT_MODE_DIFFERENTIAL;
      pActivePlatform->lwPrevDiffWgtCount = pActivePlatform->lwCount; 
      pActivePlatform->lwPrevDiffWgtWgt = pActivePlatform->lwWeight;
      break;

    default:
      pActivePlatform->ucMode = PLAT_MODE_NORMAL;
      pActivePlatform->lwPrevDiffWgtCount = 0; 
      pActivePlatform->lwPrevDiffWgtWgt = 0;
      break;
    }

  modeRsp[0] = ( unsigned char ) pActivePlatform->ucMode;
  modeRsp[0] = 0;

  OSSendIntertask(OIT, PLAT, PLAT_MODE_RSP, BYTE_DATA, modeRsp, sizeof(modeRsp));
    
  return (SUCCESSFUL);

} // fnmProcessOitModeReq




/* **********************************************************************
// FUNCTION NAME: fnmProcessOITZeroReq
// DESCRIPTION: Message handling function that processes a zero request
//                    from the OIT.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    pIntertask- intertask message that is being processed
// *************************************************************************/
static BOOL fnmProcessOITZeroReq(PLATFORMDEVICE *pPlat ,INTERTASK *pIntertask)
{
  /* this command is only valid if we are in the weighing state.  if
     we are not, it is ignored. */
  //    if (pPlat->ucPlatformTaskState == PLAT_STATE_WEIGHING)
  //    {
        

  //    }

  fnPlatProcessSTEvent(pActivePlatform , PLAT_EV_ZERO_REQ);
  return (SUCCESSFUL);
}

/*-----------------------*/
/*   Utility functions   */
/*-----------------------*/
/* **********************************************************************
// FUNCTION NAME: fnPlatProcessSTEvent
// DESCRIPTION: Processes a platform state transition event.  This involves doing
//                the following:
//                1) Find the entry in the state table that goes with the
//                    current state and the event being processed.
//                2) Call the appropriate state transition function.
//                3) Update the next state.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    bEvent - the event being processed.  the list of events is in
//                        platform.h
//
// RETURNS: TRUE if the state transition event was handled (i.e. there was
//                a table entry for the event in the current state)
//            FALSE if not
//            
//            note also that the platform system state may get updated.
// *************************************************************************/
static BOOL fnPlatProcessSTEvent(PLATFORMDEVICE *pPlat , unsigned char bEvent)
{
  BOOL fRetval = TRUE;

  fnPlatHandleEvent( pPlat , bEvent );

  return (fRetval);
}

/* **********************************************************************
// FUNCTION NAME: fnPlatMessageHandler
// DESCRIPTION: Private message processing engine for the platform task.
//                This function takes an intertask message and a lookup table
//                as input, and either calls a message processing function or
//                directly generates a state transition event, depending on the entry
//                in the table.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    pMsgTable- The message table that maps an input message to either
//                        a function call or a state transition event.
//            pMsg- The message being processed.
//
// OUTPUT:  returns TRUE if the message found in the lookup table and
//                processed
//            FALSE is returned if there was not a table entry for this message
//
// *************************************************************************/
static BOOL fnPlatMessageHandler(PLATFORMDEVICE *pPlat , PLAT_MSG_TABLE_ENTRY *pMsgTable, INTERTASK *pMsg)
{
  register unsigned long    i = 0;
  unsigned char            bTableTaskID;
  BOOL                    fRetval = FALSE;

  bTableTaskID = pMsgTable[i].bMsgSource;

  while (bTableTaskID != PLAT_END_OF_TABLE)
    {
      if ((bTableTaskID == (unsigned short) pMsg->bSource) &&
      (pMsgTable[i].wMsgID == pMsg->bMsgId))
        {
      if (((pMsgTable[i].wByte0 == PLAT_DONT_CARE) ||
           (pMsgTable[i].wByte0 == (unsigned short) pMsg->IntertaskUnion.bByteData[0])) &&
          ((pMsgTable[i].wByte1 == PLAT_DONT_CARE) ||
           (pMsgTable[i].wByte1 == (unsigned short) pMsg->IntertaskUnion.bByteData[1])))
            {
          fRetval = TRUE;

          /* either call the message function or directly generate the event */
          if (pMsgTable[i].bEvent == 0xFF)
        pMsgTable[i].pfnMsgFunction(pPlat, pMsg);
          else
        fnPlatProcessSTEvent(pPlat, pMsgTable[i].bEvent);

          break;
            }
        }
      bTableTaskID = pMsgTable[++i].bMsgSource;
    }

  return (fRetval);
}


/* **********************************************************************
// FUNCTION NAME: fnPlatSendMessage
// DESCRIPTION: Sends a message to the platform.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    pMsg- The message to send.  Must be NULL terminated.
//
// OUTPUT:  TRUE if message sent successfully, FALSE otherwise
// *************************************************************************/
static BOOL fnPlatSendMessage( PLATFORMDEVICE *pPlat ,const char *pMsg)
{
  unsigned long    lwSize;
  BOOL            fRetval = SUCCESSFUL;

#ifdef PLATFORM_TRACING
      weightTrack[ wTrkIdx ].lwLastCount = 0;
      weightTrack[ wTrkIdx ].lwPrevCount = 0;
      weightTrack[ wTrkIdx ].lwWeight = 0;
      weightTrack[ wTrkIdx ].lwPrevWeight = 0;
      weightTrack[ wTrkIdx ].ulTime = PBSysTime();
      weightTrack[ wTrkIdx ].ulT1 = pPlat->ulCurrState;
      weightTrack[ wTrkIdx ].ulT2 = *pMsg;
      weightTrack[ wTrkIdx ].ulT3 = 0x11223344;
      wTrkIdx++;
      if( wTrkIdx == MAXWTRK )
        wTrkIdx = 0;
#endif
  lwSize = strlen(pMsg);
  if (lwSize > PLAT_MAX_XMIT_MSG_SIZE)
    fRetval = FAILURE;
  else
    {// send via either platform comm task or tablet
	  if(pPlat == &sPlatforms[ MAINPLATFORM ])
	  {
		  fRetval = fnTransmitPlatformLinkLayerComm(pMsg);
	  }
	  else
	  {
	      //TODO - send to tablet instead of OIT
	      OSSendIntertask(OIT, PLAT, PLAT_MESSAGE, BYTE_DATA, (void *)pMsg, lwSize);
	  }
    }

  return (fRetval);        
}

/* **********************************************************************
// FUNCTION NAME: fnPlatBuildUpdateMessage
// DESCRIPTION: Builds the data portion of the platform update message.
//                This involves checking the last status/count messages
//                received from the platform, determining what changed,
//                and formatting the data correctly.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    pDest- Pointer to the location at which to build the message.
//                    There must be sufficient memory declared at this address
//                    to hold the entire message.
//
//            fForceUpdate- if set to TRUE, update message will always be
//                            built even if the status/count last received
//                            from the platform has not changed
//
// OUTPUT:  returns the length of the message built
//            0 is returned if there was no change in status/count that needs
//             to be reported
// *************************************************************************/
static unsigned char fnPlatBuildUpdateMessage( PLATFORMDEVICE *pPlat , unsigned char *pDest, BOOL fForceUpdate)
{
    unsigned char        bStatus = pPlat->ucLastDerivedStatus;
    long        lwPieceCount = 0;
    long        lwPieceWeight = 0;
    long        lwDeltaCount = 0;


    BOOL                fNeedToSendMessage = FALSE;
    unsigned char        retVal = 12; // retVal must be 12 to be able to update weight to OIT 

    if(fForceUpdate)
        fnPlatRestartFilter(pPlat);

    /* if last status received was a 0, this means the last thing we got from
    the platform was actually a stable count */
    if (pPlat->ucLastStatusReceived == 0) // stable weight
    {
        if(fForceUpdate)
        {
            if(pPlat->cPlatformVersion[0] == 'C') // 70 lb scale, do polling
            {
                if(isdigit(*pPlat->ucLastStringWeight) )
                {               

                    fnPlatConvertStringToWeight(pPlat , pPlat->ucLastStringWeight, &pPlat->lwWeight);
                    fNeedToSendMessage = TRUE;
#ifdef PLATFORM_TRACING

                    weightTrack[ wTrkIdx ].lwLastCount = 0;
                    weightTrack[ wTrkIdx ].lwPrevCount = 0;
                    weightTrack[ wTrkIdx ].lwWeight = pPlat->lwWeight;
                    weightTrack[ wTrkIdx ].lwPrevWeight = pPlat->lwPrevWeightReceived;
                    weightTrack[ wTrkIdx ].ulTime = PBSysTime();
                    wTrkIdx++;
                    if( wTrkIdx == MAXWTRK )
                        wTrkIdx = 0;
#endif
               }

               // strcpy((char *)pPlat->ucPreStringWeight, (char *)pPlat->ucLastStringWeight);
            }
            else
            {
                    fNeedToSendMessage = TRUE;

                    pPlat->lwCount = pPlat->lwLastCountReceived;
#ifdef PLATFORM_TRACING
                    weightTrack[ wTrkIdx ].lwLastCount = pPlat->lwLastCountReceived;
                    weightTrack[ wTrkIdx ].lwPrevCount = pPlat->lwPrevCountReceived;
                    weightTrack[ wTrkIdx ].lwWeight = pPlat->lwWeight;
                    weightTrack[ wTrkIdx ].lwPrevWeight = pPlat->lwPrevWeightReceived;
                    weightTrack[ wTrkIdx ].ulTime = PBSysTime();
                    wTrkIdx++;
                    if( wTrkIdx == MAXWTRK )
                        wTrkIdx = 0;
#endif
                    fnPlatConvertCountToWeight(pPlat , pPlat->lwLastCountReceived, &pPlat->lwWeight);
            }

        }

        else //Not fForceUpdate, do the fltering
        {
    		// if going from any non-stable weight to a stable weight, restart the repeat count
    		// set to zero, because it will be incremented farther down.
            if (pPlat->ucLastStatusReceived != pPlat->ucPrevStatusReceived)
            {
    			pPlat->ucStatusRepeatCount = 0;
    		}
           
            if(pPlat->cPlatformVersion[0] == 'C') // 70 lb scale, do polling
            {
                // if the last weight received == previous weight received
                if(strcmp((char *)pPlat->ucLastStringWeight, (char *)pPlat->ucPreStringWeight) == 0) 
                {
                    if(isdigit(*pPlat->ucLastStringWeight)  )
                    {
                    
                        // if we've gotten the same weight for the required number of consequtive times,
                        // send the update message to the OI Task
                        if (pPlat->ucWeightRepeatCount == pPlat->ucMaxWeightRepeatCount)
                        {
                            fnPlatConvertStringToWeight(pPlat , pPlat->ucLastStringWeight, &pPlat->lwWeight);
                            fNeedToSendMessage = TRUE;
                            
                            // increment the repeat count, so we won't send the message again until something changes
                            pPlat->ucWeightRepeatCount++;
                            
                            // set the status repeat count so we'll send hopefully the message
                            pPlat->ucStatusRepeatCount = pPlat->ucMaxStatusRepeatCount;
#ifdef PLATFORM_TRACING

                            weightTrack[ wTrkIdx ].lwLastCount = 0;
                            weightTrack[ wTrkIdx ].lwPrevCount = 0;
                            weightTrack[ wTrkIdx ].lwWeight = pPlat->lwWeight;
                            weightTrack[ wTrkIdx ].lwPrevWeight = pPlat->lwPrevWeightReceived;
                            weightTrack[ wTrkIdx ].ulTime = PBSysTime();
                            wTrkIdx++;
                            if( wTrkIdx == MAXWTRK )
                                wTrkIdx = 0;
#endif
                        }
                        else
                        {
                            if (pPlat->ucWeightRepeatCount < pPlat->ucMaxWeightRepeatCount)
                            {
                                pPlat->ucWeightRepeatCount++;
                            }
                        }
                    }
                    //else ignore the message
                }
                
                // else, not the same value, so restart the repeat count
                else
                {
                    pPlat->ucWeightRepeatCount = 1;
                
                    // set the status repeat count to zero, because it will be incremented farther down.
                    pPlat->ucStatusRepeatCount = 0;
                }
                
                //strcpy((char *)pPlat->ucPreStringWeight, (char *)pPlat->ucLastStringWeight);
            }
            else
            {
                // if the last weight received == previous weight received
                if((pPlat->lwLastCountReceived == pPlat->lwPrevCountReceived))
                {
                    // if we've gotten the same weight for the required number of consequtive times,
                    // send the update message to the OI Task
                    if (pPlat->ucWeightRepeatCount == pPlat->ucMaxWeightRepeatCount)
                    {
                        fNeedToSendMessage = TRUE;


#ifdef PLATFORM_TRACING
                        weightTrack[ wTrkIdx ].lwLastCount = pPlat->lwLastCountReceived;
                        weightTrack[ wTrkIdx ].lwPrevCount = pPlat->lwPrevCountReceived;
                        weightTrack[ wTrkIdx ].lwWeight = pPlat->lwWeight;
                        weightTrack[ wTrkIdx ].lwPrevWeight = pPlat->lwPrevWeightReceived;
                        weightTrack[ wTrkIdx ].ulTime = PBSysTime();
                        wTrkIdx++;
                        if( wTrkIdx == MAXWTRK )
                            wTrkIdx = 0;
#endif
                        fnPlatConvertCountToWeight(pPlat , pPlat->lwLastCountReceived, &pPlat->lwWeight);

					    // increment the repeat count, so we won't send the message again until something changes
					    pPlat->ucWeightRepeatCount++;

					    // set the status repeat count so we'll send the message
					    pPlat->ucStatusRepeatCount = pPlat->ucMaxStatusRepeatCount;
						// save the last value as the comparison value for Diff weight
                        pPlat->lwCount = pPlat->lwLastCountReceived;
                    }
                    
                    // else, increment the repeat count as long as it's less than the required number
                    else
                    {
                        if (pPlat->ucWeightRepeatCount < pPlat->ucMaxWeightRepeatCount)
                        {
                            pPlat->ucWeightRepeatCount++;
                        }
                    }
                }                
                // else, not the same value, so restart the repeat count
                else
                {
                    pPlat->ucWeightRepeatCount = 1;
                
                    // set the status repeat count to zero, because it will be incremented farther down.
                    pPlat->ucStatusRepeatCount = 0;
                }
            }
        }

        if(fNeedToSendMessage)
        {
            if(fnPlatCheckOverCapacity(pPlat , pPlat->lwWeight))
            {
                bStatus = PLAT_OVER_CAPACITY;
            }
            else
            {
                if (pPlat->lwWeight == 0)
                    bStatus = PLAT_STABLE_ZERO;
                else
                    bStatus = PLAT_STABLE;
                          
                fForcedUpdatePending = FALSE;
            }
            pPlat->ucLastDerivedStatus = bStatus;
        }

        if(    ((!fCMOSScaleLocationCodeInited) && (!fCMOSScaleLocallyCalibrated))
            || ((pPlat->platLocalized == NOT_LOCALIZED) && pPlat->cPlatformVersion[0] == 'C' ) )
        {
            bStatus = PLAT_LOCAL_REQUIRED;

            // set the status repeat count so we'll send the message the first time we get this condition
            if (pPlat->ucStatusRepeatCount < pPlat->ucMaxStatusRepeatCount)
			    pPlat->ucStatusRepeatCount = pPlat->ucMaxStatusRepeatCount;

            // set the weight repeat count so it won't cause more messages
			if (pPlat->ucWeightRepeatCount < pPlat->ucMaxWeightRepeatCount)
				pPlat->ucWeightRepeatCount = pPlat->ucMaxWeightRepeatCount + 1;

			pPlat->ucLastDerivedStatus = bStatus;
 
        }

    } // stable weight
    else // other than stable weight like error handling
    {
        //restart the repeat count
        pPlat->ucWeightRepeatCount = 1;

        if (pPlat->ucLastStatusReceived != pPlat->ucPrevStatusReceived)
        {
            pPlat->ucStatusRepeatCount = 1;
			// if the last weight received from the platform was a consistent weight,
			// save it for use during the Diff Wgt calculations.
			if ((pPlat->ucWeightRepeatCount > pPlat->ucMaxWeightRepeatCount) ||
				(pPlat->lwLastCountReceived == 0xFFFFFFFF))
            	pPlat->lwCount = pPlat->lwLastCountReceived;
        }

        if (pPlat->ucLastStatusReceived & IN_MOTION)
        {
            bStatus = PLAT_MOTION;
            //if (pPlat == pActivePlatform)
            //{
            //    fnPlatForceUpdate();
            //}
            pPlat->lwWeight = 0;
        }

        else if (pPlat->ucLastStatusReceived & OUTSIDE_ZERO)
        {
            bStatus = PLAT_OUTSIDE_ZERO_CAPTURE;
            pPlat->lwWeight = 0;
        }

        else if (pPlat->ucLastStatusReceived & CENTER_ZERO)
        {
            bStatus = PLAT_CENTER_OF_ZERO;
            pPlat->lwWeight = 0;
        }

        else if (pPlat->ucLastStatusReceived & OVER_WEIGHT)
        {
            bStatus = PLAT_OVER_CAPACITY;
            pPlat->lwWeight = 0;
        }

        else if (pPlat->ucLastStatusReceived & UNDER_ZERO)
        {
            bStatus = PLAT_UNDER_ZERO;
            pPlat->lwWeight = 0;
        }

        fNeedToSendMessage = TRUE;
       
    }

    if(fForceUpdate)
    {
        fNeedToSendMessage = TRUE;
    }
	else 
	{
    	if (pPlat->ucLastDerivedStatus == bStatus)
    	{
    		// if we've gotten the same status for the required number of consequtive times,
    		// send the update message to the OI Task
    		if (pPlat->ucStatusRepeatCount == pPlat->ucMaxStatusRepeatCount)
    		{
                fNeedToSendMessage = TRUE;

    			// increment the repeat count, so we won't send the message again until something changes
    			pPlat->ucStatusRepeatCount++;
    		}
    		// else, increment the repeat count as long as it's less than the required number
    		else
    		{
    			if (pPlat->ucStatusRepeatCount < pPlat->ucMaxStatusRepeatCount)
    			{
    				pPlat->ucStatusRepeatCount++;
    			}

    			// indicate that we don't want to send the message after all
                fNeedToSendMessage = FALSE;
    		}
    	}
    	// not the same status
    	else
    	{
    		// restart the repeat count
    		// we only want the full status repeat count when the platform is in motion.
    		// Otherwise, we want the same repeat count as the weight (as long as it's smaller than the
    		// full status repeat count), but since we're checking against the status repeat limit, we
    		// need to start at a value that is "weight repeat count" short of the full status repeat count.
    		if ((bStatus != PLAT_MOTION) && (pPlat->ucMaxStatusRepeatCount > pPlat->ucMaxWeightRepeatCount))	//lint !e506 !e774
    			pPlat->ucStatusRepeatCount = 1 + pPlat->ucMaxStatusRepeatCount - pPlat->ucMaxWeightRepeatCount;
    		else
    			pPlat->ucStatusRepeatCount = 1;

    		// indicate that we don't want to send a message yet
    		fNeedToSendMessage = FALSE;

    		// save the status
    		pPlat->ucLastDerivedStatus = bStatus;
    	}
	}

    if (fNeedToSendMessage)
    {
        memset(pDest, 0, 12);
        pDest[0] = bStatus;
        pDest[1] = pPlat->ucUnits;
        pDest[2] = pPlat->ucMode;
        /* byte 3 is for weight increase/decrease flag in differential weighing */

        if(pPlat->ucMode == PLAT_MODE_NORMAL || bStatus == PLAT_MOTION)
        {
            lwPieceWeight = pPlat->lwWeight;

            /* piece weight and platform weight always the same for normal mode */
            memcpy(&pDest[4], &lwPieceWeight, sizeof(lwPieceWeight));
            memcpy(&pDest[8], &lwPieceWeight, sizeof(lwPieceWeight));
        }
        else
        {
            lwDeltaCount = pPlat->lwPrevDiffWgtCount - pPlat->lwCount;

            pDest[3] = FALSE;

            if(pPlat->lwPrevDiffWgtWgt > 0 && pPlat->lwWeight == 0) // last piece
                {
                lwPieceWeight = pPlat->lwPrevDiffWgtWgt;
                // if(platDbg) putString("DW# last piece", 0); 
            }
            else if(lwDeltaCount < 0)  // New pieces are added to the scale 
                {
                pDest[3] = TRUE;
                lwPieceWeight = pPlat->lwWeight;
                // lwPieceWeight = 0;
                // if(platDbg) putString("DW# new weight", 0);
            }
            else if(lwDeltaCount == 0)
            {
                if(pPlat->lwWeight == 0)   // nothing on platform
                    {
                    lwPieceWeight = 0;
                    // if(platDbg) putString("DW# no weight"); 
                }
                else                  // no weight change
                    {
                    lwPieceWeight = 0;
                    retVal = 0;        // do not bother OIT
                    // if(platDbg) putString("DW# no weight change"); 
                }

            }
            else // Piece taken out
                {
                lwPieceCount = lwDeltaCount;
                fnPlatConvertCountToWeight( pPlat , lwPieceCount, &lwPieceWeight);
                // if(platDbg) putString("DW# normal"); 
            }

            pPlat->lwPrevDiffWgtCount = pPlat->lwCount;
            pPlat->lwPrevDiffWgtWgt = pPlat->lwWeight;

            memcpy(&pDest[4], &lwPieceWeight, sizeof(lwPieceWeight));
            memcpy(&pDest[8], &pPlat->lwWeight, sizeof(pPlat->lwWeight));
        }

        return (retVal);
    }

    retVal = 0;
    return (retVal);
}

/* **********************************************************************
// FUNCTION NAME: fnPlatConvertCountToWeight
// DESCRIPTION: Examines the a count value received from the
//                platform and converts it to a valid 4 byte weight
//                value formatted in the proper units.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    pDest- Pointer to the location at which to build the message.
//                    There must be sufficient memory declared at this address
//                    to hold the entire message.
//
// OUTPUT:  SUCCESSFUL - this is always the return value for now
// *************************************************************************/
static BOOL fnPlatConvertCountToWeight(PLATFORMDEVICE *pPlat , long lwCount, long *pWeight)
{
  double    dblGrams;
  double    dblAdjustedCount;

  // temp for debug !!!
  char                scratchpad[20];

  /* adjust count for location code */
  dblAdjustedCount = fnPlatLocalizeCount(pPlat, lwCount);

  /* first convert the count to grams */
  dblGrams = (dblAdjustedCount) / 10.0;
    
//  pPlat->ucUnits = fnRateGetWeightUnit();

  if (pPlat->ucUnits == AVOIRDUPOIS)
    {
      double             dblFracLbs;
      double             dblFracOzs;
      unsigned char    bLbs;
      unsigned char    bWholeOzs;
      unsigned char    bTenthsOzs;
      unsigned char    bBCDLbs;
      unsigned char    bBCDWholeOzs;
      unsigned char    bBCDTenthsOzs;

      dblFracLbs = dblGrams / 453.592;
      bLbs = (unsigned char) dblFracLbs;                    // e.g. 8.2453
      dblFracOzs = (fmod(dblFracLbs, 1.0)) * 16.0;        // .2453 * 16 = 3.9248
      bWholeOzs = (unsigned char) dblFracOzs;                // 3
      bTenthsOzs = (fmod(dblFracOzs, 1.0)) * 10;            // .9248 * 10 = 9.248 == 9

      /* for weight classifier rounding, always round fractional portion up to next division */
      if (fmod(dblFracOzs, 1.0) > 0.0)    
        {
      bTenthsOzs++;
      if (bTenthsOzs == 10)
            {
          bTenthsOzs = 0;
          bWholeOzs++;

          if (bWholeOzs == 16)
                {
          bWholeOzs = 0;
          bLbs++;
                }
            }
        }

      // temp for debug !!!
#ifdef PLAT_DBG_MESSAGES
      sprintf(scratchpad, "%d lb %d.%d oz ", bLbs, bWholeOzs, bTenthsOzs);
      fnLCDPutStringDirect((uchar *)scratchpad, 3, 7, 0);
#endif

      bBCDLbs = ((bLbs / 10) << 4) | (bLbs % 10);
      bBCDWholeOzs = ((bWholeOzs / 10) << 4) | (bWholeOzs % 10);
      bBCDTenthsOzs = bTenthsOzs << 4;     /* hundreths of ozs not used */

      *pWeight = (bBCDLbs << 16) | (bBCDWholeOzs << 8) | bBCDTenthsOzs;
    }

  else // if (bUnits == METRIC)
    {
      unsigned char    bWholeKgs;
      unsigned short    wGrams;
      unsigned char    bBCDWholeKgs;
      unsigned char    bBCD100s10sGrams;
      unsigned char    bBCD1sGrams;
      unsigned long    lGrams = (unsigned long)dblGrams;
                
      bWholeKgs = (unsigned char)(lGrams / 1000);
      wGrams = lGrams % 1000;

      /* weight classifier rounding- round fractional portion up to next division */
      if (fmod(dblGrams, 1.0) > 0)
        wGrams++;

      if (wGrams >= 1000)
      {
        bWholeKgs++;
        wGrams -= 1000;
      }

      /* if weight is > 2.5 kg, round up to nearest 2g increment */
      // but only if not a W&M scale. Weights from W&M scales are supposed to be used "as is"
      // because the W&M scales do the rounding before sending the weight to the meter.
      if(pPlat->ucScaleType == NON_WM_PLATFORM)
      {
          if ((bWholeKgs > 2) || ((bWholeKgs == 2) && (wGrams > 500)))
          {
            if (wGrams % 2)
            {
              wGrams++;
              if (wGrams >= 1000)
              {
                bWholeKgs++;
                wGrams -= 1000;
              }
            }
          }
      }

      /* convert to BCD */
      bBCDWholeKgs = ((bWholeKgs / 10) << 4) | (bWholeKgs % 10);
      bBCD100s10sGrams = ((wGrams / 100) << 4) | ((wGrams % 100) / 10);
      bBCD1sGrams = (wGrams % 10) << 4;    /* tenths of grams not used */

      *pWeight = (bBCDWholeKgs << 16) | (bBCD100s10sGrams << 8) | bBCD1sGrams;
    }

  return (SUCCESSFUL);
}

/* **********************************************************************
// FUNCTION NAME: fnPlatLocalizeCount
// DESCRIPTION: Adjusts a platform count for a given location.  This function
//                compensates for the gravity differences between where the
//                platform was calibrated and where the customer site is.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    lwCount- count being adjusted
//
// OUTPUT:  returns the adjusted count.  if a location code is not available,
//            this will be equal to the input count provided in lwCount.
// *************************************************************************/
static double fnPlatLocalizeCount(PLATFORMDEVICE *pPlat , unsigned long lwCount)
{
  const double pGravityTable[32] = 
    {
      977.039,    /* location code 00 */
      977.239,    /* location code 01 */
      977.439,    /* location code 02 */
      977.639,    /* location code 03 */
      977.839,    /* location code 04 */
      978.039,    /* location code 05 */
      978.239,    /* location code 06 */
      978.439,    /* location code 07 */
      978.639,    /* location code 08 */
      978.839,    /* location code 09 */
      979.039,    /* location code 10 */
      979.239,    /* location code 11 */
      979.439,    /* location code 12 */
      979.639,    /* location code 13 */
      979.839,    /* location code 14 */
      980.039,    /* location code 15 */
      980.239,    /* location code 16 */
      980.439,    /* location code 17 */
      980.639,    /* location code 18 */
      980.839,    /* location code 19 */
      981.039,    /* location code 20 */
      981.239,    /* location code 21 */
      981.439,    /* location code 22 */
      981.639,    /* location code 23 */
      981.839,    /* location code 24 */
      982.039,    /* location code 25 */
      982.239,    /* location code 26 */
      982.439,    /* location code 27 */
      982.639,    /* location code 28 */
      982.839,    /* location code 29 */
      983.039,    /* location code 30 */
      983.239        /* location code 31 */
    };

  const double    dblCalibrationGravity = 978.771;    /* Hong Kong gravity */
  double            dblRetval;

    {
      if (pPlat->fPerformGravityAdjustment == TRUE && 
      (fCMOSScaleLocationCodeInited) &&
      (bCMOSScaleLocationCode < 32) &&
      (!fCMOSScaleLocallyCalibrated) &&
	  (pPlat->ucScaleType == NON_WM_PLATFORM))
    dblRetval = ((double) lwCount) * (dblCalibrationGravity / pGravityTable[bCMOSScaleLocationCode]);
      else
    dblRetval = (double) lwCount;
    }
  return ((double) dblRetval);
}

  
  const unsigned long    lwEnglish2lb =    0x00020000;
  const unsigned long    lwEnglish5lb =    0x00050000;
  const unsigned long    lwEnglish10lb =   0x00100000;
  const unsigned long    lwEnglish15lb =   0x00150000;
  const unsigned long    lwEnglish30lb =   0x00300000;
  const unsigned long    lwEnglish35lb =   0x00350000;
  const unsigned long    lwEnglish70lb =   0x00700000;
  const unsigned long    lwEnglish100lb =  0x01000000;
  const unsigned long    lwEnglish150lb =  0x01500000;


  const unsigned long    lwMetric1kg =     0x00010000;
  const unsigned long    lwMetric2p5kg =   0x00025000;
  const unsigned long    lwMetric5kg =     0x00050000;
  const unsigned long    lwMetric6p9kg =   0x00069000;
  const unsigned long    lwMetric7kg   =   0x00070000;
  const unsigned long    lwMetric12kg  =   0x00120000;
  const unsigned long    lwMetric13p6kg =  0x00136000;
  const unsigned long    lwMetric15kg  =   0x00150000;
  const unsigned long    lwMetric17p5kg =  0x00175000;
  const unsigned long    lwMetric30kg  =   0x00300000;
  const unsigned long    lwMetric35kg =    0x00350000;
  const unsigned long    lwMetric45kg  =   0x00450000;
  const unsigned long    lwMetric50kg =    0x00500000;
  const unsigned long    lwMetric60kg  =   0x00600000;
  const unsigned long    lwMetric65kg =    0x00650000;
  const unsigned long    lwMetric70kg  =   0x00700000;

/* **********************************************************************
// FUNCTION NAME: fnGetPlatformLimit
// DESCRIPTION: return the limit of the platform in BCD
// feature settings
//
// AUTHOR: George Monroe
//
// INPUTS: 
//
// OUTPUT:  returns the limit
//          returns FALSE if not
// *************************************************************************/
unsigned long fnGetPlatformLimit(PLATFORMDEVICE *pPlat)
{
  BOOL                fRetval = FALSE;
  unsigned long       lwWeightLimit = 0;
  unsigned char       bFeatureState;
  unsigned char       bFeatureIndex;
  BOOL                fHighEnabled;
  BOOL                fMediumEnabled;
  BOOL                fLowEnabled;


  // make sure the weight units has been set up
//  pPlat->ucUnits = fnRateGetWeightUnit();

  IsFeatureEnabled(&fHighEnabled, PLATFORM_HIGH_CAPACITY_SUPPORT, &bFeatureState, &bFeatureIndex);
  IsFeatureEnabled(&fMediumEnabled, PLATFORM_MED_CAPACITY_SUPPORT, &bFeatureState, &bFeatureIndex);
  IsFeatureEnabled(&fLowEnabled, PLATFORM_LOW_CAPACITY_SUPPORT, &bFeatureState, &bFeatureIndex);
  
  //NOTE******
  //The USB Platform return their weight in units of 1/10 gram
  if (fLowEnabled)
  {
    switch(pPlat->cPlatformVersion[0])
    {
    case 'B':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish2lb : lwMetric1kg;
      }
      break;
    case 'C':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish100lb : lwMetric45kg;
      }
      break;
    case 'D':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish15lb : lwMetric7kg;
      }
      break;
    case 'E':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish70lb : lwMetric30kg;
      }
      break;
    case 'F':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish70lb : lwMetric30kg;
      }
      break;
    default:
      break;
    }
  }
  
  if (fMediumEnabled)
  {
    switch(pPlat->cPlatformVersion[0] )
    {
    case 'B':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish5lb : lwMetric2p5kg;
      }
      break;
    case 'C':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish100lb : lwMetric45kg; 
      }
      break;
    case 'D':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish30lb : lwMetric12kg;
      }
      break;
    case 'E':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish100lb : lwMetric45kg;
      }
      break;
    case 'F':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish100lb : lwMetric45kg;
      }
      break;
    default:
      break;
    }
  }
  
  if (fHighEnabled)
  {
    switch(pPlat->cPlatformVersion[0] )
    {
    case 'B':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish10lb : lwMetric5kg;
      }
      break;
    case 'C':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish100lb : lwMetric45kg; 
      }
      break;
    case 'D':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish30lb : lwMetric15kg;
      }
      break;
    case 'E':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish100lb : lwMetric45kg;
      }
      break;
    case 'F':
      {
        lwWeightLimit = (pPlat->ucUnits == AVOIRDUPOIS) ? lwEnglish100lb : lwMetric45kg;
      }
      break;
    default:
      break;
    }
  }

  if(fnPlatIsUSBPlatform() == TRUE)
  {
    if (pPlat->ulPlatformLimit < lwWeightLimit)
        lwWeightLimit = pPlat->ulPlatformLimit;
  }

  return(lwWeightLimit);
}


/* **********************************************************************
// FUNCTION NAME: fnGetPlatCapacity
// DESCRIPTION: Get the platform capacity
//
// AUTHOR: Sunny Liao
//
// INPUTS:    lwWeight- the weight being checked
//
// OUTPUT:  returns TRUE if the passed in weight exceeds the platform capacity
//            returns FALSE if not
// *************************************************************************/
void fnGetPlatCapacity(unsigned char *pWeight)
{
  unsigned long lwWeightLimit = 0;

  lwWeightLimit = fnGetPlatformLimit( pActivePlatform );
  memcpy( pWeight , ( char *) &lwWeightLimit , sizeof(lwWeightLimit));
  return;   
}

/* **********************************************************************
// FUNCTION NAME: fnPlatCheckOverCapacity
// DESCRIPTION: Checks if a given weight exceeds the designated platform
//                capacity.  The platform capacity that we use is controlled
//                by feature codes and therefore may be less than the physical
//                capacity of the hardware.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    lwWeight- the weight being checked
//
// OUTPUT:  returns TRUE if the passed in weight exceeds the platform capacity
//            returns FALSE if not
// *************************************************************************/
static BOOL fnPlatCheckOverCapacity(PLATFORMDEVICE *pPlat , unsigned long lwWeight)
{
  unsigned long lwWeightLimit;
  BOOL fRetval = FALSE;

  lwWeightLimit = fnGetPlatformLimit( pPlat );

  if (lwWeight > lwWeightLimit)
    fRetval = TRUE;
  //Hard code here for the platform feature is not supported for now.2/7/06 Sunny
  //    fRetval = false;

  return (fRetval);
   
}

/* **********************************************************************
// FUNCTION NAME: fnPlatRestartMotionTimer
// DESCRIPTION: If applicable, restarts the timer
//                 used to detect perpetual motion for PLAT_MOTION_TIMEOUT_TIME.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    fStateDependentCheck- if set to true, timer will only be reset
//                                    if in the weighing state.  if false
//                                    timer will be reset regardless of state.
//
// OUTPUT:  TRUE if the timer was restarted, FALSE if not
// *************************************************************************/
static BOOL fnPlatRestartMotionTimer(BOOL fStateDependentCheck)
{
  BOOL fRetval;

  if (!fStateDependentCheck || ( pActivePlatform->ulCurrState == PLAT_STATE_WEIGHING))
    {
      OSStopTimer(PLAT_MESSAGE_TIMER);
      // OSChangeTimerDuration(PLAT_MESSAGE_TIMER, PLAT_MOTION_TIMEOUT_TIME);
      OSStartTimer(PLAT_MESSAGE_TIMER);
      fRetval = TRUE;
    }
  else
    fRetval = FALSE;

  return (fRetval);
}

/*---------------------*/
/*   Public functions  */
/*---------------------*/
void fnPlatProcessCommMsg( PLATFORMDEVICE *pPlat , INTERTASK *pMsg )
{

#ifdef PLATFORM_TRACING
  weightTrack[ wTrkIdx ].lwLastCount = 0;
  weightTrack[ wTrkIdx ].lwPrevCount = 0;
  weightTrack[ wTrkIdx ].lwWeight = 0;
  weightTrack[ wTrkIdx ].lwPrevWeight = 0;
  weightTrack[ wTrkIdx ].ulTime = PBSysTime();
  weightTrack[ wTrkIdx ].ulT1 = pPlat->ulCurrState;
  weightTrack[ wTrkIdx ].ulT2 = pMsg->bMsgId;
  weightTrack[ wTrkIdx ].ulT3 = 0x44444444;
  wTrkIdx++;
  if( wTrkIdx == MAXWTRK )
    wTrkIdx = 0;
#endif
  switch (pMsg->bMsgId)
    {
    case  COMM_RECEIVE_MESSAGE:
      fnmProcessPlatMessage(pPlat , pMsg);
      break;
    case  COMM_IDENTIFY:
      // If it's a reset after localization, the platform is still attached
      // The serial scale needs additional time after it issues the DTR  
      //remove the following 3 second sleep since only 'C' platform needs it
      //but FP doesn't require 'C' platform.
      pPlat->fLastScaleConnectState = TRUE;
      pPlat->ucScheduledOp = PLAT_OP_NONE;
      pPlat->lwLastCountReceived = 0xFFFFFFFF;
      pPlat->lwLastWeightReceived = 0xFFFFFFFF;
      pPlat->lwPrevCountReceived = 0xFFFFFFFF;
      pPlat->lwPrevWeightReceived = 0xFFFFFFFF;
      pPlat->ucLastStatusReceived = 0;
      pPlat->ucPrevStatusReceived = 0;
      pPlat->ucUnits = AVOIRDUPOIS;
      pPlat->ucMode = PLAT_MODE_NORMAL;
      pPlat->ucPlatformRetries = 0;
      pPlat->fForceUpdatePending = FALSE;
      pPlat->platLocalized = NOT_LOCALIZED;
      if(pPlat == pActivePlatform)
    {
      if(fnPlatLocalize(PLAT_CONNECTED, NULL) == FALSE)
        {
          fnPlatProcessSTEvent(pPlat , PLAT_EV_CONNECTED);
        }
    }
      break;
    case  COMM_UNIDENTIFY:
      if(pPlat->fLastScaleConnectState == TRUE)
    {
      // If it's a reset after localization, the platform is still attached
      if(fnPlatLocalize(PLAT_DISCONNECTED, NULL) == FALSE)
        {
          fnPlatProcessSTEvent(pPlat , PLAT_EV_DISCONNECTED);
        }
      pPlat->fLastScaleConnectState = FALSE;
    }
      break;
    default:
      break;
    }
}

void fnPlatProcessMsg( PLATFORMDEVICE *pPlat, INTERTASK *pMsg )
{

  switch (pMsg->bSource)
    {
      // message received from pb232 port
    case CPLAT:
      fnPlatProcessCommMsg( pPlat , pMsg );
      break;
    case CM:
      break;
	  
      // response from Oit - User Interface Task
      //All OIT/PLAT messages go to the currently active platform
    case  OIT:
    case PLAT:
      fnPlatMessageHandler( pActivePlatform , (PLAT_MSG_TABLE_ENTRY *)pPlatMsgTable, pMsg);
      break;
	  
    case  SYS:
      break;

	//TODO - add handling for Tablet messages using commented code below as template, if necessary based on rating info location.
	/* handle messages from Tablet API Server - Source ID TBD
	 * case  TBD:
	 * 	switch(pMsg->bMsgId)
	 * 	{
	 * 	   case COMM_RECEIVE_MESSAGE:
	 * 	      fnPlatProcessCommMsg( &sPlatforms[ ALTPLATFORM ]  , pMsg );
	 * 	      break;
	 * 	   case USB_PLATFORM_CONNECT:
	 * 	      if(pActivePlatform != &sPlatforms[ ALTPLATFORM ] )
				{
				  pActivePlatform = &sPlatforms[ ALTPLATFORM ];
				  pActivePlatform->fLastScaleConnectState = 1;
				  pActivePlatform->ulPlatformLimit = 0;

				  //automatically activate the port
				  fnPlatHandleEvent( pActivePlatform , PLAT_EV_ACTIVATE_PORT);
				}
	 *
	 * 	      break;
	 * 	   case USB_PLATFORM_DISCONNECT:
	 * 	      fnPlatProcessSTEvent(pActivePlatform , PLAT_EV_DISCONNECTED);
			  pActivePlatform = &sPlatforms[ MAINPLATFORM ];
			  if (sPlatforms[ MAINPLATFORM ].lwLastCountReceived == 0xFFFFFFFF)
			  {
				  fnPlatChangeState(pActivePlatform, 0, PLAT_STATE_PORT_NA );
				  fnPlatHandleEvent( pActivePlatform , PLAT_EV_ACTIVATE_PORT);
			  }
			  else if( pActivePlatform->fLastScaleConnectState != 0 &&
					   pActivePlatform->ulCurrState == PLAT_STATE_WEIGHING)
			  {
				fnPlatForceUpdate();
			  }
			  else
			  {
				if(fnPlatLocalize(PLAT_CONNECTED, NULL) == FALSE)
				{
				  fnPlatProcessSTEvent(pActivePlatform , PLAT_EV_CONNECTED);
				}
			  }
	 *
	 * 	      break;
	 *
	 * 	}
	 */
// error -- message unknown
    default:
      fnDisplayErrorDirect("Unknown Task Message Source", pMsg->bSource, PLAT);
      break;
    }   // End of Switch (IT Msg Source)
}


#if 0
/* **********************************************************************
// FUNCTION NAME: fnPlatformTask
// DESCRIPTION: This is the task entry point for the platform task.
//                Contains highest level processing and the main message loop.
//
// AUTHOR: Joe Mozdzer
// MODIFICATION HISTORY:
//	09/25/2012	Bob Li			Fixed the issue about serial platform calibration error.
//  07/12/2012  Liu Jupeng      Added code to fix the issue that the meter prints 
//                              the empty evelope.
// *************************************************************************/
void fnPlatformTask(unsigned long argc, void *argv)
{
  INTERTASK interTaskMsg;
  NU_EVENT_GROUP *pEvent;

  /****************************************************************************
   1. When the PLAT task is first started, nothing should be trying to debit, 
      so set this flag, which allows platform updates to be sent to the OI.
   2. The CM will will clear the flag between the envelope reaching S1 and S3, 
      so that platform updates will not interfere with debiting, which can 
      result in a blank envelope.
  *****************************************************************************/

  if(platDbg)
  	putString( "PlatTsk: Init PLAT_OK_TO_SEND_UPDATE", __LINE__);
  OSSetEvents( PLAT_EVENT_GROUP, (unsigned long)PLAT_OK_TO_SEND_UPDATE, OS_OR );


  pEvent = PBCreateSignalEvent();
  if(pEvent)
    PBSetTaskEvent(( unsigned long *) pEvent);

  // Set up the main platform
  sPlatforms[ MAINPLATFORM ].ulCurrState = PLAT_STATE_DISCONNECTED;
  sPlatforms[ MAINPLATFORM ].ulPrevState = PLAT_STATE_DISCONNECTED;

  sPlatforms[ MAINPLATFORM ].ucScheduledOp = PLAT_OP_NONE;
  sPlatforms[ MAINPLATFORM ].fPlatformVersionValid = FALSE;
  sPlatforms[ MAINPLATFORM ].lwLastCountReceived = 0xFFFFFFFF;
  sPlatforms[ MAINPLATFORM ].lwLastWeightReceived = 0xFFFFFFFF;
  sPlatforms[ MAINPLATFORM ].lwPrevCountReceived = 0xFFFFFFFF;
  sPlatforms[ MAINPLATFORM ].lwPrevWeightReceived = 0xFFFFFFFF;
  sPlatforms[ MAINPLATFORM ].ucLastStatusReceived = 0;
  sPlatforms[ MAINPLATFORM ].ucPrevStatusReceived = 0;
  sPlatforms[ MAINPLATFORM ].ucUnits = AVOIRDUPOIS;
  sPlatforms[ MAINPLATFORM ].ucMode = PLAT_MODE_NORMAL;
  sPlatforms[ MAINPLATFORM ].ucPlatformRetries = 0;
  sPlatforms[ MAINPLATFORM ].fForceUpdatePending = FALSE;
  sPlatforms[ MAINPLATFORM ].fLastScaleConnectState = FALSE;
  sPlatforms[ MAINPLATFORM ].platLocalized = NOT_LOCALIZED;
  sPlatforms[ MAINPLATFORM ].fPerformGravityAdjustment = TRUE;
  
  sPlatforms[ MAINPLATFORM ].ucScaleType = NON_WM_PLATFORM;
  sPlatforms[ MAINPLATFORM ].cPlatformVersion[0] = 0;
  sPlatforms[ MAINPLATFORM ].cPlatformSN[0] = 0;
  sPlatforms[ MAINPLATFORM ].cPlatformModel[0] = 0;
  sPlatforms[ MAINPLATFORM ].ulPlatformLimit = 0;
  sPlatforms[ MAINPLATFORM ].ulRangeIncrement = 0;
  
  sPlatforms[ MAINPLATFORM ].ucStatusRepeatCount = 0;
  sPlatforms[ MAINPLATFORM ].ucWeightRepeatCount = 0;
  sPlatforms[ MAINPLATFORM ].ucLastDerivedStatus = 0xFF;
  sPlatforms[ MAINPLATFORM ].ucMaxStatusRepeatCount = 0;
  sPlatforms[ MAINPLATFORM ].ucMaxWeightRepeatCount = 0;

  // Set up the alternate platform
  sPlatforms[ ALTPLATFORM ].ulCurrState = PLAT_STATE_ESTABLISHING_COMM;
  sPlatforms[ ALTPLATFORM ].ulPrevState = PLAT_STATE_ESTABLISHING_COMM;

  sPlatforms[ ALTPLATFORM ].ucScheduledOp = PLAT_OP_NONE;
  sPlatforms[ ALTPLATFORM ].fPlatformVersionValid = FALSE;
  sPlatforms[ ALTPLATFORM ].lwLastCountReceived = 0xFFFFFFFF;
  sPlatforms[ ALTPLATFORM ].lwLastWeightReceived = 0xFFFFFFFF;
  sPlatforms[ ALTPLATFORM ].lwPrevCountReceived = 0xFFFFFFFF;
  sPlatforms[ ALTPLATFORM ].lwPrevWeightReceived = 0xFFFFFFFF;
  sPlatforms[ ALTPLATFORM ].ucLastStatusReceived = 0;
  sPlatforms[ ALTPLATFORM ].ucPrevStatusReceived = 0;
  sPlatforms[ ALTPLATFORM ].ucUnits = AVOIRDUPOIS;
  sPlatforms[ ALTPLATFORM ].ucMode = PLAT_MODE_NORMAL;
  sPlatforms[ ALTPLATFORM ].ucPlatformRetries = 0;
  sPlatforms[ ALTPLATFORM ].fForceUpdatePending = FALSE;
  sPlatforms[ ALTPLATFORM ].fLastScaleConnectState = FALSE;
  sPlatforms[ ALTPLATFORM ].platLocalized = NOT_LOCALIZED;
  sPlatforms[ ALTPLATFORM ].fPerformGravityAdjustment = FALSE;

  sPlatforms[ ALTPLATFORM ].ucScaleType = NON_WM_PLATFORM;
  sPlatforms[ ALTPLATFORM ].cPlatformVersion[0] = 0;
  sPlatforms[ ALTPLATFORM ].cPlatformSN[0] = 0;
  sPlatforms[ ALTPLATFORM ].cPlatformModel[0] = 0;
  sPlatforms[ ALTPLATFORM ].ulPlatformLimit = 0;
  sPlatforms[ ALTPLATFORM ].ulRangeIncrement = 0;
  
  sPlatforms[ ALTPLATFORM ].ucStatusRepeatCount = 0;
  sPlatforms[ ALTPLATFORM ].ucWeightRepeatCount = 0;
  sPlatforms[ ALTPLATFORM ].ucLastDerivedStatus = 0xFF;
  sPlatforms[ ALTPLATFORM ].ucMaxStatusRepeatCount = 0;
  sPlatforms[ ALTPLATFORM ].ucMaxWeightRepeatCount = 0;

  pActivePlatform = &sPlatforms[ MAINPLATFORM ];

  while (1)
  {
      if( OSReceiveIntertask( PLAT, &interTaskMsg, OS_SUSPEND) == SUCCESSFUL )
      {
          fnPlatProcessMsg( pActivePlatform, &interTaskMsg );

#ifdef PLATFORM_TRACING
          weightTrack[ wTrkIdx ].lwLastCount = 0;
          weightTrack[ wTrkIdx ].lwPrevCount = 0;
          weightTrack[ wTrkIdx ].lwWeight = 0;
          weightTrack[ wTrkIdx ].lwPrevWeight = 0;
          weightTrack[ wTrkIdx ].ulTime = PBSysTime();
          weightTrack[ wTrkIdx ].ulT1 = pActivePlatform->ulCurrState;
          weightTrack[ wTrkIdx ].ulT2 = 0;
          weightTrack[ wTrkIdx ].ulT3 = cnt;
          wTrkIdx++;
          if( wTrkIdx == MAXWTRK )
            wTrkIdx = 0;
#endif
      	if(interTaskMsg.bMsgType == PART_PTR_DATA)
          {
            OSReleaseMemory( interTaskMsg.IntertaskUnion.PointerData.pData);
          }
      }
	  
	  
  }
}

#endif

/* **********************************************************************
// FUNCTION NAME: fnPlatMessageExpire
// DESCRIPTION:  Timer expiration routine for the platform message timer.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    none used
// **********************************************************************/
void fnPlatMessageExpire(unsigned long lwArg)
{
  /* queue a message to ourself which will generate the appropriate event */
  OSSendIntertask(PLAT, PLAT, PLAT_MOTION_TIMER_EXPIRED, NO_DATA, NULL, 0);
  return;
}



/* **********************************************************************
// FUNCTION NAME: fnPlatSupportDiffWeigh
// DESCRIPTION: Return FALSE if it's a JB76 platform otherwise TRUE
//
// AUTHOR: Howell Sun
//
// *************************************************************************/
BOOL fnPlatSupportDiffWeigh(void)
{
  if ((pActivePlatform->cPlatformVersion[0] == 'C') || // 70 lb scale, do polling
	  (pActivePlatform->ucScaleType == WM_PLATFORM))
    {
      return (FALSE);
    }
  else
    {
      return (TRUE);
    }
} // fnPlatformSupportDiffWeighing



  /* **********************************************************************
  // FUNCTION NAME: fnPlatGetSWVersion
  // DESCRIPTION: Returns the platform software version number.
  //
  // AUTHOR: Joe Mozdzer
  //
  // INPUTS:    pVersion- the version string is returned here.  the caller
  //                        must allocate PLAT_MAX_VERSION_SIZE+1 bytes
  //                        at this address before making the call.
  //                      note that if the platform is not currently in a 
  //                        connected state (or the platform port is not
  //                        activated), this function will return the last
  //                        version number that has been received from an
  //                        attached platform if it is available.
  //
  // RETURNS: SUCCESSFUL if a version number was available
  //            FAILURE if not (platform may never have been successfully initialized)
  // *************************************************************************/
BOOL fnPlatGetSWVersion(char *pVersion)
{
  BOOL fRetval = FAILURE;
	unsigned char ucInx;


    if (pActivePlatform && (pActivePlatform->fPlatformVersionValid))    
    {
		for (ucInx = 0; ucInx < PLAT_MAX_VERSION_SIZE; ucInx++)
		{
			if (pActivePlatform->cPlatformVersion[ucInx] == '-')
				break;
			else
				pVersion[ucInx] = pActivePlatform->cPlatformVersion[ucInx];
		}

        pVersion[ ucInx ] = 0;
        fRetval = SUCCESSFUL;
    }
	else
	{
		pVersion[0] = 0;  // just in case
	}

	return fRetval;
}

/* **********************************************************************
// FUNCTION NAME: fnPlatGetMode
// DESCRIPTION: Returns the current weighing mode being used by the platform.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    none
//
// RETURNS: current mode of the platform.  if the platform is not attached/
//            activated this function returns the last mode used or the
//            default setting.
// *************************************************************************/
unsigned char fnPlatGetMode()
{
  return (pActivePlatform->ucMode);
}

/* **********************************************************************
// FUNCTION NAME: fnPlatGetUnits
// DESCRIPTION: Returns the current weighing units being used by the platform.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    none
//
// RETURNS: current units setting for the platform.  if the platform is not attached/
//            activated this function returns the last units used or the
//            default setting.
// *************************************************************************/
unsigned char fnPlatGetUnits()
{
  return (pActivePlatform->ucUnits);
}

/* **********************************************************************
// FUNCTION NAME: fnPlatSetUnits
// DESCRIPTION: Sets the current weighing units being used by the platform.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    bWeighUnits    - the desired units to use for future messages from the platform
//                            valid values are:
//                                METRIC
//                                AVOIRDUPOIS
//                                AVOIRDUPOIS_FRAC
//                            if any other value is provided, this function does nothing
//
// *************************************************************************/
void fnPlatSetUnits(unsigned char bWeighUnits)
{
  if ((bWeighUnits == METRIC) || (bWeighUnits == AVOIRDUPOIS) ||
      (bWeighUnits == AVOIRDUPOIS_FRAC))
    pActivePlatform->ucUnits = bWeighUnits;
	
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnPlatGetPrecision
// DESCRIPTION: Returns the current precision of a weight provided by the
//                platform.
//
// AUTHOR: Joe Mozdzer & Sandra Peterson
//
// INPUTS:    None
//
// RETURNS: one of the PLAT_PREC_* constants defined in platform.h
// *************************************************************************/
UINT32 fnPlatGetPrecision(void)
{
	UINT32    lwRetval = PLAT_PREC_NOT_AVAIL;


    if (pActivePlatform && (pActivePlatform->fPlatformVersionValid))    
	{
        if (pActivePlatform->cPlatformVersion[0] == 'D' ||
        	pActivePlatform->cPlatformVersion[0] == 'E' ||
        	pActivePlatform->cPlatformVersion[0] == 'F')
	   	{
			// if it's a W&M platform, need to get the precision based on the weight range precision specified by the platform.
			// else, can determine the precision based on the weight units.
			switch (fnRateGetWeightUnit())
			{
		    	case METRIC:
					if (fnIsWMPlatform() == TRUE)
				   	{
						if (pActivePlatform->ulRangeIncrement < 0x00000100)
					        lwRetval = PLAT_PREC_MET_1_G;
						else
					        lwRetval = PLAT_PREC_MET_10_G;
					}
				   	else
					{
			        	lwRetval = PLAT_PREC_MET_1_G;
					}
					break;

				case AVOIRDUPOIS:
				case AVOIRDUPOIS_FRAC:
					if (fnIsWMPlatform() == TRUE)
				   	{
						if (pActivePlatform->ulRangeIncrement < 0x00000100)
					        lwRetval = PLAT_PREC_ENG_TENTH_OZ;
						else
					        lwRetval = PLAT_PREC_ENG_1_OZ;
					}
				   	else
					{
				        lwRetval = PLAT_PREC_ENG_TENTH_OZ;
					}
					break;

				default:
					break;
		    }
		}
	   	else
		{
			// if it's a W&M platform, need to get the precision based on the weight range precision specified by the platform.
			// else, can determine the precision based on the weight units.
			switch (fnRateGetWeightUnit())
			{
		    	case METRIC:
					// for now, we don't know of any special weight precisions for the metric W&M platforms
//					if (pPlatformVersion[0] == 'W')
//				   	{
//			        	lwRetval = PLAT_PREC_MET_1_G;
//					}
//				   	else
					{
			        	lwRetval = PLAT_PREC_MET_1_G;
					}
					break;

				case AVOIRDUPOIS:
				case AVOIRDUPOIS_FRAC:
					// for now, we don't know of any special weight precisions for the non-metric W&M platforms
//					if (pPlatformVersion[0] == 'W')
//				   	{
//				        lwRetval = PLAT_PREC_ENG_TENTH_OZ;
//					}
//				   	else
					{
				        lwRetval = PLAT_PREC_ENG_TENTH_OZ;
					}
					break;

				default:
					break;
			}
		}
	}
	
	return (lwRetval);
}

/* **********************************************************************
// FUNCTION NAME: fnPlatIsAttached
// DESCRIPTION: Used to determine if a platform is attached.
//                
//
// AUTHOR: Joe Mozdzer
//
// RETURNS: TRUE if a platform is attached and communicating.
//            FALSE if not.  (note that false is returned if the platform task
//                            does not currently have control over the shared plat/modem uart).
// *************************************************************************/
BOOL fnPlatIsAttached(void)
{
  BOOL     fRetval = FALSE;

  // RD - traced intermittent crash to a null value in pActivePlatform
  //		not sure how or why it is but we should check anyway.
  if(pActivePlatform == NULL)
	  fRetval = FALSE;
  else if ((pActivePlatform->ulCurrState == PLAT_STATE_PORT_NA) ||
		   (pActivePlatform->ulCurrState == PLAT_STATE_ESTABLISHING_COMM) ||
		   (pActivePlatform->ulCurrState == PLAT_STATE_REBOOTING) ||
		   (pActivePlatform->ulCurrState == PLAT_STATE_DISCONNECTED))
    fRetval = FALSE;
  else
    fRetval = TRUE;

  return (fRetval);
}

/* **********************************************************************
// FUNCTION NAME: fnIsPlatformEnabled
// DESCRIPTION: Check the platform enabled/disabled feature, the platform
//          is enabled if any one of the three feature is enabled.
//                
// AUTHOR: Victor Li
//
// RETURNS: TRUE if the platform feature is enabled, otherwise FALSE.
// *************************************************************************/
BOOL fnIsPlatformEnabled(void)
{
  unsigned char       bFeatureState;
  unsigned char       bFeatureIndex;
  BOOL                fHighEnabled;
  BOOL                fMediumEnabled;
  BOOL                fLowEnabled;

  IsFeatureEnabled(&fHighEnabled, PLATFORM_HIGH_CAPACITY_SUPPORT, &bFeatureState, &bFeatureIndex);
  IsFeatureEnabled(&fMediumEnabled, PLATFORM_MED_CAPACITY_SUPPORT, &bFeatureState, &bFeatureIndex);
  IsFeatureEnabled(&fLowEnabled, PLATFORM_LOW_CAPACITY_SUPPORT, &bFeatureState, &bFeatureIndex);
    
  //Hard code here for ccd file doesn't support platform feature./2/7/06 Sunny
  //fHighEnabled = true;

  return (fHighEnabled || fMediumEnabled || fLowEnabled) ? TRUE : FALSE;
}

/* **********************************************************************
// FUNCTION NAME: fnPlatForceUpdate
// DESCRIPTION: Set a pending flag to force the platform send a weight update message to OIT.
//                
//
// AUTHOR: Victor Li
//
// RETURNS: no return code
// *************************************************************************/
void fnPlatForceUpdate( void )
{
  fForcedUpdatePending = TRUE;
}


/* **********************************************************************
// FUNCTION NAME: fnPlatRestartFilter
// DESCRIPTION: Set up to restart the filtering of the status/count messages from the platform.
//                
//
// AUTHOR: Sandra Peterson
//
// RETURNS: no return code
// *************************************************************************/
void fnPlatRestartFilter(PLATFORMDEVICE *pPlat )
{
	pPlat->ucStatusRepeatCount = 1;
	pPlat->ucWeightRepeatCount = 1;
	pPlat->ucLastDerivedStatus = 0xFF;
}

/* **********************************************************************
// FUNCTION NAME: fnPlatformPollTimerExpire
// DESCRIPTION: Set polling message to platform for weight
//                
//
// AUTHOR: Howell Sun
//
// RETURNS: no return code
// *************************************************************************/
void fnPlatformPollTimerExpire(unsigned long inputVar)
{
  // Send message to itslef to send polling message to avoid NU_INVALID_SUSPEND error
  OSSendIntertask(PLAT, PLAT, PLAT_POLLING_TIMER_EXPIRED, NO_DATA, NULL, 0);
        
  // Timer is restarted after getting previous response or re-connected
  // OSStopTimer(PLAT_POLLING_TIMER); 
}


/* **********************************************************************
// FUNCTION NAME: fnPlatformLocalizationTimerExpire
// DESCRIPTION: Set localization to complete if DSR does not go high after
//                 getting the response from platform
//                
//
// AUTHOR: Howell Sun
//
// RETURNS: no return code
// *************************************************************************/
void fnPlatformLocalizationTimerExpire( unsigned long inputVar)
{
  if(pActivePlatform->platLocalized == WAIT_PLAT_RESET)
    {
      pActivePlatform->ucPlatLocalized = LOCALIZED;
      fnPlatForceUpdate();
    }
  else
    {
      pActivePlatform->ucPlatLocalized = NOT_LOCALIZED;
    }

  if(pActivePlatform->ulPlatStatus & PLAT_IS_ACTIVE)
    {
      OSStopTimer(PLAT_LOCALIZATION_TIMER); 
      OSSendIntertask(PLAT, PLAT, PLAT_POLLING_TIMER_EXPIRED, NO_DATA, NULL, 0);
    }
	
//  if(platDbg)
//    putString("SCALE Localization time out", __LINE__);
}


/* **********************************************************************
// FUNCTION NAME: fnPlatLocalize
// DESCRIPTION: Localize the platform by setting gravity code
//                
//
// AUTHOR: Howell Sun
//
// RETURNS: no return code
// *************************************************************************/
BOOL fnPlatLocalize(int opCode, uchar *rsp)
{
    int     rspLen = 0,err;
    BOOL    retVal = FALSE;
    unsigned long loc;

    // This array maps the Janus gravity code to Mega so JB76 platform can recognize it
    static const char *gravityCodeMap[32][2] = 
    {
      {"0", "2"}, {"1", "7"}, {"1", "0"}, {"1", "3"}, {"2", "8"}, {"2", "1"}, {"3", "9"}, {"3", "2"}, 
      {"3", "5"}, {"4", "3"}, {"4", "6"}, {"5", "0"}, {"5", "3"}, {"5", "9"}, {"6", "1"}, {"6", "4"},
      {"7", "2"}, {"7", "5"}, {"7", "8"}, {"8", "4"}, {"8", "7"}, {"9", "5"}, {"9", "8"}, {"9", "1"}, 
      {"0", "6"}, {"0", "9"}, {"0", "4"}, {"2", "4"}, {"4", "9"}, {"6", "7"}, {"8", "0"}, {"1", "1"}
    };

    switch(opCode)
    {
    case PLAT_LOCALIZE_START: // Ask for gravity setting grant
        OSStopTimer(PLAT_POLLING_TIMER); 
        if(platDbg)
            putString("Stop POLLING", __LINE__);
			
        pActivePlatform->platLocalized = LOCALIZE_STARTED;
		
        //If it is a USB platform
        if(pActivePlatform->cPlatformVersion[0] == 'D' ||
           pActivePlatform->cPlatformVersion[0] == 'E' ||
           pActivePlatform->cPlatformVersion[0] == 'F')
        {
      	  if (fnGetScaleLocationCode(&loc) == TRUE)
      	  {
                if ((bCMOSScaleLocationCode != loc) && (pActivePlatform->ucScaleType == NON_WM_PLATFORM))
                {
                	if (fnSetScaleLocationCode(bCMOSScaleLocationCode) == TRUE)
                    {
                        pActivePlatform->platLocalized = LOCALIZED;
                    }
                }
                else
                    pActivePlatform->platLocalized = LOCALIZED;
            }
            else
                pActivePlatform->platLocalized = LOCALIZED;

        }
        else
        {
            fnPlatSendMessage( pActivePlatform ,"G");
            OSStartTimer(PLAT_LOCALIZATION_TIMER); 
        }
        break;

    case PLAT_LOCALIZE_RSP:
        rspLen = strlen((char *)rsp);
        sprintf(tmpBuf, "%s", rsp);
        if(platDbg)
            putString(tmpBuf, __LINE__);

        switch(rspLen)
        {
        case 1:
            if(fCMOSScaleLocationCodeInited && (bCMOSScaleLocationCode < 32) && (!fCMOSScaleLocallyCalibrated)  )
            {
                fnPlatSendMessage( pActivePlatform ,gravityCodeMap[bCMOSScaleLocationCode][0]);
            }
            break;

        case 2:
            if(rsp[1] == gravityCodeMap[bCMOSScaleLocationCode][0][0])
            {
                fnPlatSendMessage( pActivePlatform ,gravityCodeMap[bCMOSScaleLocationCode][1]);
            }
            break;

        case 3:
            if(    rsp[1] == gravityCodeMap[bCMOSScaleLocationCode][0][0]
                && rsp[2] == gravityCodeMap[bCMOSScaleLocationCode][1][0]  )
            {
                pActivePlatform->platLocalized = WAIT_PLAT_RESET;
            }
            else 
            {
                pActivePlatform->platLocalized = NOT_LOCALIZED;
          
                // Poll the weight anyway, the error of localization will be reported together with weight
                fnPlatPollWeight(pActivePlatform , __LINE__); 
                if(platDbg)
                    putString("Start fnPlatPollWeight", __LINE__);
            }
            break;

        default: 
            break;
      }
      break;

    case PLAT_DISCONNECTED: // Platform is resetting
        if( (pActivePlatform->platLocalized == WAIT_PLAT_RESET) &&
            (pActivePlatform->cPlatformVersion[0] == 'C') )
			retVal = TRUE;
        break;

    case PLAT_CONNECTED: // Platform software is up and running
        if( (pActivePlatform->platLocalized == WAIT_PLAT_RESET) &&
            (pActivePlatform->cPlatformVersion[0] == 'C') )
        {
            retVal = TRUE;
            pActivePlatform->platLocalized = LOCALIZED;
            fnPlatForceUpdate();

            OSStopTimer(PLAT_LOCALIZATION_TIMER); 

            OSWakeAfter(4000); // Wait for 4 seconds for platform to be up running
            if(pActivePlatform->ucScheduledOp == PLAT_OP_ZERO) // Run pending command ("Zero" for now)
            {
                fnPlatSetWaitTime( pActivePlatform , PLAT_Z_RESPONSE_TIME );
                fnPlatSendMessage( pActivePlatform ,"Z");
                fnPlatForceUpdate();
                if(platDbg)
                    putString("Zero SCALE", __LINE__);
					
                pActivePlatform->ucScheduledOp = PLAT_OP_NONE;
            }

            if(platDbg)
                putString("Start POLLING", __LINE__);

            // Poll the weight
            OSStartTimer(PLAT_POLLING_TIMER);  // Give time for platform to respond
        }
        break;

    case PLAT_LOCALIZE_FAILED:
        // If SW1 is on, it prohibits gravity code setting, which will treat it as normal
        if(rsp[1] & GRAVITY_NOT_ALLOWED)
        {
            pActivePlatform->platLocalized = LOCALIZED;
            fnPlatForceUpdate();
            OSStopTimer(PLAT_LOCALIZATION_TIMER); 
        }
        else
        {
            pActivePlatform->platLocalized = NOT_LOCALIZED;
        }

        // Poll the weight 
        fnPlatPollWeight(pActivePlatform , __LINE__); 
        if(platDbg)
            putString("Start fnPlatPollWeight", __LINE__);
        break;

    default: // Unknown response, treated as error
        // Poll the weight anyway, the error of localization will be reported together with weight
        fnPlatPollWeight(pActivePlatform , __LINE__); 
        if(platDbg)
            putString("Start fnPlatPollWeight", __LINE__);
        break;
    }

    return retVal;
}


/* **********************************************************************
// FUNCTION NAME: fnPlatPollWeight
// DESCRIPTION: Send polling message to platform for weight
//                
//
// AUTHOR: Howell Sun
//
// RETURNS: no return code
// *************************************************************************/
void fnPlatPollWeight(PLATFORMDEVICE *pPlat , int lineNum)
{
  if(pPlat->ucUnits == AVOIRDUPOIS)
    {
      fnPlatSendMessage( pPlat ,"W");
    }
  else
    {
      fnPlatSendMessage( pPlat ,"D");
    }
     
  OSStopTimer(PLAT_POLLING_TIMER); 
  OSStartTimer(PLAT_POLLING_TIMER); 
}


/* **********************************************************************
// FUNCTION NAME: fnPlatConvertStringToWeight
// DESCRIPTION: Examines the a count value received from the
//                platform and converts it to a valid 4 byte weight
//                value formatted in the proper units.
//
// AUTHOR: Howell Sun
//
// INPUTS:    pDest- Pointer to the location at which to build the message.
//                    There must be sufficient memory declared at this address
//                    to hold the entire message.
//
// OUTPUT:  SUCCESSFUL - this is always the return value for now
// *************************************************************************/
static BOOL fnPlatConvertStringToWeight(PLATFORMDEVICE *pPlat , unsigned char *pStr, long *pWeight)
{
  unsigned char    tmpStr[MAX_WT_STR_LEN];
  unsigned char    *tmpP, *tmpP2;

  // temp for debug !!!
  char                scratchpad[20];


//  pPlat->ucUnits = fnRateGetWeightUnit();

  if (pPlat->ucUnits == AVOIRDUPOIS)
    {
      double           dblFracLbs = 0;
      double           dblFracOzs = 0;
      unsigned char    bLbs = 0;
      unsigned char    bWholeOzs = 0;
      unsigned char    bTenthsOzs = 0;
      unsigned char    bBCD100Lbs = 0;
      unsigned char    bBCDLbs = 0;
      unsigned char    bBCDWholeOzs = 0;
      unsigned char    bBCDTenthsOzs = 0;

      strcpy((char *)tmpStr, (char *)pStr);

      /* Look for "lb" */
      tmpP = &tmpStr[0];
      tmpP2 = (uchar *)strstr((char *)tmpP, STR_LB);
      if(tmpP2 == NULL) return FAILURE; // Wrong format
      *tmpP2 = 0;
      bLbs = atoi((char *)tmpP);

      // look for period
      tmpP = tmpP2 + 2; // Pass NULL character and "b "
      tmpP2 = (uchar *)strchr((char *)tmpP, CHAR_PERIOD);
      if(tmpP2 == NULL) return FAILURE; // Wrong format
      *tmpP2 = 0;
      bWholeOzs = atoi((char *)tmpP);

      // look for "oz"
      tmpP2++;
      tmpP = tmpP2; 
      tmpP2 = (uchar *)strstr((char *)tmpP, STR_OZ);
      if(tmpP2 == NULL) return FAILURE; // Wrong format
      *tmpP2 = 0;
      bTenthsOzs = atoi((char *)tmpP);

      // temp for debug !!!
#ifdef PLAT_DBG_MESSAGES
      sprintf(scratchpad, "%d lb %d.%d oz ", bLbs, bWholeOzs, bTenthsOzs);
      fnLCDPutStringDirect((uchar *)scratchpad, 3, 7, 0);
#endif

      bBCD100Lbs = bLbs / 100;
      bLbs = bLbs % 100;

      bBCDLbs = ((bLbs / 10) << 4) | (bLbs % 10);
      bBCDWholeOzs = ((bWholeOzs / 10) << 4) | (bWholeOzs % 10);
      bBCDTenthsOzs = bTenthsOzs << 4;     /* hundreths of ozs not used */

      *pWeight = (bBCD100Lbs << 24) | (bBCDLbs << 16) | (bBCDWholeOzs << 8) | bBCDTenthsOzs;
    }

  else // if (pPlat->ucUnits == METRIC)
    {
      unsigned char    bWholeKgs = 0;
      unsigned short    wGrams = 0;
      unsigned char    bBCDWhole100Kgs = 0;
      unsigned char    bBCDWholeKgs = 0;
      unsigned char    bBCD100s10sGrams = 0;
      unsigned char    bBCD1sGrams = 0;

      strcpy((char *)tmpStr, (char *)pStr);

      /* Look for the period */
      tmpP = &tmpStr[0];
      tmpP2 = (uchar *)strchr((char *)tmpP, CHAR_PERIOD);
      if(tmpP2 == NULL) return FAILURE; // Wrong format
      *tmpP2 = 0;
      bWholeKgs = atoi((char *)tmpP);

      // look for "kg"
      tmpP2++; // Pass NULL character
      tmpP = tmpP2;
      tmpP2 = (uchar *)strstr((char *)tmpP, STR_KG);
      if(tmpP2 == NULL) return FAILURE; // Wrong format
      *tmpP2 = 0;
      wGrams = atoi((char *)tmpP);

      /* convert to BCD */
      bBCDWhole100Kgs = bBCDWholeKgs / 100;
      bBCDWholeKgs = bBCDWholeKgs % 100;

      bBCDWholeKgs = ((bWholeKgs / 10) << 4) | (bWholeKgs % 10);
      bBCD100s10sGrams = ((wGrams / 100) << 4) | ((wGrams % 100) / 10);
      bBCD1sGrams = (wGrams % 10) << 4;    /* tenths of grams not used */

      *pWeight = (bBCDWhole100Kgs << 24) | (bBCDWholeKgs << 16) | (bBCD100s10sGrams << 8) | bBCD1sGrams;
    }

  return (SUCCESSFUL);
}

/* **********************************************************************
// FUNCTION NAME: fnClearDSRState
// DESCRIPTION: Clear the status to force platform to get the scale version
//          when OIT activates the port again.
//          This is for fixing the Fraca 15023.
//
// AUTHOR: Howell Sun, Victor Li
//
// INPUTS: 
//
// OUTPUT: 
// *************************************************************************/
void fnClearDSRState()
{
  pActivePlatform->fLastScaleConnectState = TRUE;
}

void debugMsg(char *msg, int lineNumber)
{
  char  dbg_buf[50];

  if(platDbg == TRUE)
    {
      sprintf(dbg_buf,"%s#%d",msg, lineNumber); 
      fnSystemLogEntry(SYSLOG_TEXT, msg, strlen(dbg_buf));
    }
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnActivePlatform  
//
// DESCRIPTION: 
//      To active the platform if it is enabled
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      05/19/2006    John Gao      Initial version
// *************************************************************************/
void fnActivePlatform(void)
{
  UINT32            ulEvents;
  /* check the platform enable/disable feature before activating it */
    
  if (fnIsPlatformEnabled())
    {
      /* active the platform here */
      OSSendIntertask(PLAT, OIT, PLAT_ACTIVATE_PORT, NO_DATA, NULL, 0);
    }
 }
 

/* *************************************************************************
// FUNCTION NAME: fnIsWMPlatform
// DESCRIPTION: Check if the attached platform is a W&M platform
// AUTHOR: John Gao
//
// INPUTS:	none
// RETURN: true if W&M platform
// *************************************************************************/
BOOL fnIsWMPlatform(void)
{
    BOOL fRetV = FALSE;

    if (pActivePlatform && (pActivePlatform->fPlatformVersionValid))    
    {
        switch (pActivePlatform->cPlatformVersion[0])
        {
        
        case 'D': // For a standard usb platform
        case 'E': // For standard usb platform
        case 'F': // For standard usb platform
    		if (pActivePlatform->ucScaleType == WM_PLATFORM)
    			fRetV = TRUE;
    	    break;
    	    
        case 'W':
            fRetV = TRUE;
            break;
            
        default:
            break;
        }
    }
    return fRetV;
}

/* *************************************************************************
// FUNCTION NAME: fnIsCalibrationSupported
// DESCRIPTION: Check if the attached platform supports calibration.
// AUTHOR: Bob Li
//
// INPUTS:	none
// RETURN: true if non-W&M USB platform
// MODIFICATION HISTORY:
//	2012-09-25	Bob Li		Updated the codes to support the serial platform calibration.
//	2012-09-10	Bob Li		Init ver for usb scale calibation.
// *************************************************************************/
BOOL fnIsCalibrationSupported(void)
{
    BOOL fRetV = FALSE;
	
    if( fnPlatIsAttached() ==TRUE
		//&& fnPlatIsUSBPlatform() == TRUE
		&& fnIsWMPlatform() == FALSE )
    {
    	fRetV=TRUE;
    }

    return fRetV;
}

/* *************************************************************************
// FUNCTION NAME: fnIsSetLocationCodeSupported
// DESCRIPTION: Check if the attached platform supports set location code.
// AUTHOR: Bob Li
//
// INPUTS:	none
// RETURN: true if non-W&M platform
// MODIFICATION HISTORY:
//	2012-09-14	Bob Li		Init ver for scale location code setting.
// *************************************************************************/
BOOL fnIsSetLocationCodeSupported(void)
{
    BOOL fRetV = FALSE;
	
    if( fnPlatIsAttached() ==TRUE
		&& fnIsWMPlatform() == FALSE )
    {
    	fRetV=TRUE;
    }

    return fRetV;
}

/* **********************************************************************
// FUNCTION NAME: fnPlatGetPlatSerialNumber
// DESCRIPTION: Get the serial number of the platform
//
// AUTHOR: John Gao modified from Janus code
//
// INPUTS:  pointer to buffer for the serial number
//
// OUTPUT:  none
// *************************************************************************/
void fnPlatGetPlatSerialNumber(char *pPlatSN)
{
	pPlatSN[0] = 0;  // just in case

    if (pActivePlatform)
	{
		(void)strncpy( pPlatSN, pActivePlatform->cPlatformSN, PLAT_MAX_SN_SIZE );
		pPlatSN[ PLAT_MAX_SN_SIZE ] = 0;
	}
}


/* **********************************************************************
// FUNCTION NAME: fnPlatSendPostage
// DESCRIPTION: Send the postage value to the platform
//
// AUTHOR: Sandra Peterson
//
// INPUTS:  pointer to the buffer containing the nul-terminated ASCII string
//          containing the postage value and monetary sign.
//
// OUTPUT:  none
// *************************************************************************/
void fnPlatSendPostage(char *pPlatPostage)
{
}


/* **********************************************************************
// FUNCTION NAME: fnPlatGetExtendedSWVersion
// DESCRIPTION: Get a platform software version that also includes the
//				platform model.
//
// AUTHOR: John Gao modified from Janus code
//
// INPUTS:  pointer to buffer for the version + model number
//
// OUTPUT:  none
//
// MODIFICATION HISTORY:
//	09-26-2012	Bob Li		Fixed an issue that missing check the hardware version.
// *************************************************************************/
BOOL fnPlatGetExtendedSWVersion(char* pVersion)
{
    BOOL fRetval = FAILURE;
    
    UINT8         bHardwareMajorVersion;
    UINT8         bDontCare;
    
    if (pActivePlatform && (pActivePlatform->fPlatformVersionValid))
    {
    	switch(pActivePlatform->cPlatformVersion[0])
    	{
    		case 'B':    		    
    		    (void)strcpy(pVersion, pActivePlatform->cPlatformVersion);

   			    (void)strcat(pVersion, "-MP4D/F"); //DM475
    			fRetval = SUCCESSFUL;
    			break;

            case 'D': // For a standard usb platform
            case 'E': // For standard usb platform
            case 'F': // For standard usb platform
    	
                (void)strncpy( pVersion, pActivePlatform->cPlatformVersion, PLAT_MAX_VERSION_SIZE );
                pVersion[ PLAT_MAX_VERSION_SIZE ] = 0;
    	        fRetval = SUCCESSFUL;
    			break;

    		case 'W':   // Not support for G9
    		case 'C':	// don't care about the model name for the JB7x platforms
    		default:
    			break;
    	}
    }
	else
	{
		pVersion[0] = 0;  // just in case
	}

    return fRetval;
}


/* **********************************************************************
// FUNCTION NAME: fnIsWMWeightInKg
// DESCRIPTION: Check if the weight from the W&M platform should be
//				shown only in kg.
// AUTHOR: John Gao modified from Janus code
//
// INPUTS:	none
// RETURN: true if weight only shown in kg, false if only shown in g.
// *************************************************************************/
BOOL fnIsWMWeightInKg(void)
{
    BOOL fRetV = FALSE;

    if (pActivePlatform && (pActivePlatform->fPlatformVersionValid))    
	{
        //If its is a USB platform
        if(pActivePlatform->cPlatformVersion[0] == 'D' ||
        pActivePlatform->cPlatformVersion[0] == 'E' ||
        pActivePlatform->cPlatformVersion[0] == 'F')
        {
            // If not an MP3C platform, the weight is always shown in kilograms
            // Otherwise, the weight is always shown in grams
	        if (strcmp(pActivePlatform->cPlatformModel, "MP3C") != 0)
	        {
	            fRetV = TRUE;
	        }
	    }

	    // for serial W&M platforms, the weight is always displayed in grams
	}


    return fRetV;
}

/* **********************************************************************
// FUNCTION NAME: fnPlatIsUSBPlatform
// DESCRIPTION: Check if the active platform is USB platfor,
// AUTHOR: John Gao
//
// INPUTS:	none
// RETURN: true if it is USB platform, otherwise false.
// *************************************************************************/
BOOL  fnPlatIsUSBPlatform(void)
{
    BOOL fRetV = FALSE;

    if (pActivePlatform && (pActivePlatform->fPlatformVersionValid))    
	{
        if(pActivePlatform->cPlatformVersion[0] == 'D' ||
        pActivePlatform->cPlatformVersion[0] == 'E' ||
        pActivePlatform->cPlatformVersion[0] == 'F')
        {
	        fRetV = TRUE;
	    }
	}

    return fRetV;
}


/* **********************************************************************
// FUNCTION NAME: fnPlatGetFilteringSetting
// DESCRIPTION: Get whether or not platform message filtering is on or off
//
// AUTHOR: Sandra Peterson
//
// INPUTS:  none
//
// OUTPUT:  TRUE, if filtering is on.  FALSE, otherwise.
// *************************************************************************/
BOOL fnPlatGetFilteringSetting(void)
{
	if (CMOSPlatformFilteringOn == TRUE)
		return(TRUE);
	else
		return(FALSE);
}

/* **********************************************************************
// FUNCTION NAME: fnPlatSetFilteringSetting
// DESCRIPTION: Set whether or not platform message filtering should be done.
//
// AUTHOR: John Gao modified from Janus code
//
// INPUTS:  TRUE, if filtering should be done.  FALSE, if it shouldn't be done.
//
// OUTPUT:  none
// *************************************************************************/
void fnPlatSetFilteringSetting(BOOL fNewSetting)
{   
    PLATFORMDEVICE *pPlat;
    BOOL fIsUSBPlat = FALSE;

    CMOSPlatformFilteringOn = fNewSetting;

    // change the max repeat counts  
    pPlat = &sPlatforms[ MAINPLATFORM ];
    fnPlatSetFilteringCount(fNewSetting, pPlat);

    pPlat = &sPlatforms[ ALTPLATFORM ];
    fnPlatSetFilteringCount(fNewSetting, pPlat);   
}


/* **********************************************************************
// FUNCTION NAME: fnPlatSetFilteringCount
// DESCRIPTION: Set whether or not platform message filtering should be done.
//
// AUTHOR: John Gao modified from Janus code
//
// INPUTS:  
//
// OUTPUT:  none
// *************************************************************************/
void fnPlatSetFilteringCount(BOOL fIsFilteringOn, PLATFORMDEVICE *pPlat)
{
    BOOL fIsUSBPlat = FALSE;
    
    if(pPlat)
    {
    
        if(pPlat->cPlatformVersion[0] == 'D' ||
            pPlat->cPlatformVersion[0] == 'E' ||
            pPlat->cPlatformVersion[0] == 'F')
        {
            fIsUSBPlat = TRUE;
        }

        
        pPlat->ucStatusRepeatCount = 1;
        pPlat->ucWeightRepeatCount = 1;

        
        if (fIsFilteringOn == TRUE)
        {
        
            // change the max repeat counts        
            if(fIsUSBPlat) //USB platform
            {
                pPlat->ucMaxStatusRepeatCount = SAME_STATUS_REPEAT_COUNT_U;
                pPlat->ucMaxWeightRepeatCount = SAME_WEIGHT_REPEAT_COUNT_U;
            }
            else    //Serial platform
            {           
                pPlat->ucMaxStatusRepeatCount = SAME_STATUS_REPEAT_COUNT_S;
                pPlat->ucMaxWeightRepeatCount = SAME_WEIGHT_REPEAT_COUNT_S;
            }
        }
        else
        {
            // change the max repeat counts
            pPlat->ucMaxStatusRepeatCount = 2;
            pPlat->ucMaxWeightRepeatCount = 2;
        }
    }    
}

static BOOL fnGetCalibrationWeight(unsigned long *pWeight)
{
	BOOL retVal = TRUE;
	//TODO - serial scale: figure out if this weight has to come from scale (maybe it can be cached from initial call on startup)
	//TODO - USB scale: call API on tablet synchronously
	return (retVal);
}

static BOOL fnGetScaleLocationCode(unsigned long *pLocCode)
{
	BOOL retVal = TRUE;
	//TODO - call API on tablet synchronously
	return (retVal);
}

static BOOL fnSetScaleLocationCode(unsigned long locCode)
{
	BOOL retVal = TRUE;
	//TODO - call API on tablet sync or async
	return (retVal);

}
