/************************************************************************
*	PROJECT:		Abacus
*	MODULE NAME:	$Workfile:   wowutils.c  $
*	REVISION:		$Revision:   1.3  $
*	DESCRIPTION:	WOW Utilies
*
*----------------------------------------------------------------------
*              Copyright (c) 2004 Pitney Bowes Inc.
*                   35 Waterview Drive
*                  Shelton, Connecticut  06484
*----------------------------------------------------------------------
*
*			REVISION HISTORY:
*
*$Log:   H:/group/SPARK/ARCHIVES/UIC/wowutils.c_v  $
* 
*    Rev 1.3   Dec 17 2004 18:05:12   DE009DE
* Fixed Fraca 13169.
* The Last 10 Weights screen was displaying the weights
* out of order.
* 
*    Rev 1.2   Nov 18 2004 13:27:10   DE009DE
* Updates to the following:
* Service Mode Verify WOW Test
* Portrait Mode
* 
*    Rev 1.1   Oct 27 2004 11:14:32   DE009DE
* Minor fixes to weight logging.
* 
*    Rev 1.0   Oct 26 2004 15:02:16   DE009DE
* WOW Utilities. Initial Check In.
************************************************************************/

/*lint -e46  */ /* field type should be int warning */

#include <string.h>
#include <math.h>
#include "custdat.h"
#include "wowutils.h"

// Mail Piece Weight Log Information *******************************************
extern unsigned char bCMOSWeightLogHead;
extern unsigned char bCMOSWeightLogNumEntries;
extern unsigned short wCMOSWeightLog[MAIL_PIECE_WEIGHT_LOG_NUM_ENTRIES];

PIP_DIM_HISTORY sPIPDimensionalData;
// *****************************************************************************

/*
 * Name
 *   fnClearMailPieceWeightLog
 *
 * Description
 *   
 *   This function removes all data from the mail piece weight log
 *   and initializes the log.
 *
 * Inputs
 *   Nothing.
 *
 * Returns
 *   Nothing.
 *
 */
void fnClearMailPieceWeightLog(void)
{
	bCMOSWeightLogNumEntries = 0;
	bCMOSWeightLogHead = 0;
	(void)memset((void*)wCMOSWeightLog,0,sizeof(wCMOSWeightLog));
}

/*
 * Name
 *   fnLogMailPieceWeight
 *
 * Description
 *   
 *   This function adds a weight to the mail piece weight log.
 *   The weight log is a circular buffer containing the last N mail
 *	 piece weights.
 *
 * Inputs
 *   wMailPieceWeight - Weight from scale.  The units are scale counts.
 *
 * Returns
 *   Nothing.
 *
 */
void fnLogMailPieceWeight(const unsigned short wMailPieceWeight)
{
	wCMOSWeightLog[bCMOSWeightLogHead] = wMailPieceWeight;
	bCMOSWeightLogHead = (bCMOSWeightLogHead + 1) % MAIL_PIECE_WEIGHT_LOG_NUM_ENTRIES;
	if (bCMOSWeightLogNumEntries < MAIL_PIECE_WEIGHT_LOG_NUM_ENTRIES)
		bCMOSWeightLogNumEntries++;
}

/*
 * Name
 *   fnGetMailPieceWeightLogNumEntries
 *
 * Description
 *   
 *   The function retrieves the number of mail piece weight
 *   log entries currently in the log.
 *	 
 *
 * Inputs
 *   Nothing.
 *
 * Returns
 *   Number of Mail Piece Log Entries in the log.
 *
 */
unsigned char fnGetMailPieceWeightLogNumEntries(void)
{
	return bCMOSWeightLogNumEntries;
}

/*
 * Name
 *   fnGetMailPieceWeightLogEntries
 *
 * Description
 *   
 *   This function retrieves a set of weight entries from the 
 *   mail piece weight log.  The caller must allocate a buffer
 *	 to hold the requested weight entries.  If the caller 
 *   requests too many entries then the function returns an error.
 *   
 *
 * Inputs
 *   bNumEntries - The number of mail piece weight entries requested.
 *	 pwEntries - Pointer to buffer where the most recent bNumEntries weight 
 *					entries are returned.
 *					This MUST be allocated by the caller. Each entry returned
 *					is the weight in scale counts.
 *
 * Returns
 *   TRUE if succesfull, FALSE otherwise
 *
 */
BOOL fnGetMailPieceWeightLogEntries(const unsigned char bNumEntries, unsigned short *pwEntries)
{
	unsigned char i, bEntriesToCopy;
	if (bNumEntries <= bCMOSWeightLogNumEntries && pwEntries != NULL)
	{
		// start at the oldest entry
		if (bCMOSWeightLogNumEntries == MAIL_PIECE_WEIGHT_LOG_NUM_ENTRIES)
			i = bCMOSWeightLogHead;
		else			
			i = (bCMOSWeightLogHead + (MAIL_PIECE_WEIGHT_LOG_NUM_ENTRIES-bCMOSWeightLogNumEntries))%MAIL_PIECE_WEIGHT_LOG_NUM_ENTRIES;
		
		// copy the requested number of entries	
		bEntriesToCopy = bNumEntries;		
		while (bEntriesToCopy-- > 0)
		{
			*pwEntries++ = wCMOSWeightLog[i];
			i = (i+1)%MAIL_PIECE_WEIGHT_LOG_NUM_ENTRIES;
		}
		return TRUE;
	}
	else
		return FALSE;
}

BOOL fnCaclulateWeightStdDev(const unsigned char bNumEntries, const unsigned int *pwEntries, double *pdStdDev)
{
	unsigned char i;
	unsigned int wSum, wAvg;
	long lTmp, lSum2;
	
	if (bNumEntries == 0 || pwEntries == NULL || pdStdDev == NULL)
		return FALSE;
		
	// Calculate the mean
	wSum = 0;
	for(i=0;i < bNumEntries; i++)
		wSum += pwEntries[i];
	wAvg = wSum / (unsigned short)bNumEntries;	//lint !e795
		
	// Calulate Sum((X[i] - Mean)^2)
	lSum2 = 0;
	for(i=0;i < bNumEntries; i++)
	{
		lTmp = (long)pwEntries[i] - (long)wAvg;
		lTmp *= lTmp;
		lSum2 += lTmp;
	}
	
	// Divide by (N-1)
	lSum2 /= ((long)bNumEntries - 1);
	
	// Take the sqare root
	*pdStdDev = sqrt((double)lSum2);		
		
	return TRUE;
}

BOOL fnCaclulateWeightAverage(const unsigned char bNumEntries, const unsigned int *pwEntries, double *pdAverage)
{
	unsigned char i;
	unsigned int wSum, wAvg;
	
	if (bNumEntries == 0 || pwEntries == NULL || pdAverage == NULL)
		return FALSE;
		
	// Calculate the mean
	wSum = 0;
	for(i=0;i < bNumEntries; i++)
		wSum += pwEntries[i];
	wAvg = wSum / (unsigned short)bNumEntries;	//lint !e795
	
	*pdAverage = (double)wAvg;
		
	return TRUE;
}

/*
 * Name
 *   fnClearPIPDimensionHistory
 *
 * Description
 *   
 *   This function removes all data from the Dimenion History log
 *   and initializes the log.
 *
 * Inputs
 *   Nothing.
 *
 * Returns
 *   Nothing.
 *
 */
void fnClearPIPDimensionHistory(void)
{
	sPIPDimensionalData.usPIPHistoryHead = 0;
	sPIPDimensionalData.usPIPHistoryNumEntries = 0;
	memset(&sPIPDimensionalData.PIPDimData, 0, sizeof(DIM_DATA)*MAX_PIP_DIM_HISTORY);
}
/*
 * Name
 *   fnGetPIPDimensionNumEntries
 *
 * Description
 *   
 *   The function retrieves the number of PIP Dimension
 *   log entries currently in the log.
 *	 
 *
 * Inputs
 *   Nothing.
 *
 * Returns
 *   Number of Mail Piece Log Entries in the log.
 *
 */
unsigned short fnGetPIPDimensionNumEntries(void)
{
	return sPIPDimensionalData.usPIPHistoryNumEntries;
}

/*
 * Name
 *   fnLogPIPDimension
 *
 * Description
 *   
 *   This function adds a the Dimensional Data to the Dimensional Data log.
 *   The weight log is a circular buffer containing the last N mail
 *	 piece weights.
 *
 * Inputs
 *   wMailPieceWeight - Weight from scale.  The units are scale counts.
 *
 * Returns
 *   Nothing.
 *
 */
void fnLogPIPDimension(unsigned short usWeight, unsigned short usThickness, unsigned short usWidth, unsigned short usLength)
{
	unsigned short i;
	i = sPIPDimensionalData.usPIPHistoryHead;
	sPIPDimensionalData.PIPDimData[i].usWeight = usWeight;
	sPIPDimensionalData.PIPDimData[i].usThickness = usThickness;
	sPIPDimensionalData.PIPDimData[i].usWidth = usWidth;
	sPIPDimensionalData.PIPDimData[i].usLength = usLength;

	sPIPDimensionalData.usPIPHistoryHead = (sPIPDimensionalData.usPIPHistoryHead + 1) % MAX_PIP_DIM_HISTORY;
	if (sPIPDimensionalData.usPIPHistoryNumEntries < MAX_PIP_DIM_HISTORY)
		sPIPDimensionalData.usPIPHistoryNumEntries++;
}


/*
 * Name
 *   fnGetMailPieceWeightLogEntries
 *
 * Description
 *   
 *   This function retrieves a set of PIP Dimensional Data entries.
 *   
 *
 * Inputs
 *   bNumEntries - The number of PIP Dimensional Data entries requested.
 *	 pwEntries - Pointer to buffer where the most recent bNumEntries PIP Dim 
 *					entries are returned.
 *					This MUST be allocated by the caller. Each entry returned
 *					is the contents of an entry of DIM_DATA in scale counts.
 *
 * Returns
 *   TRUE if succesfull, FALSE otherwise
 *
 */
BOOL fnGetPIPDimensionalEntries(const unsigned short usNumEntries, DIM_DATA *pwEntries)
{
	unsigned short i, usEntriesToCopy;
	if (usNumEntries <= sPIPDimensionalData.usPIPHistoryNumEntries && pwEntries != NULL)
	{
		// start at the oldest entry
		if (sPIPDimensionalData.usPIPHistoryNumEntries == MAX_PIP_DIM_HISTORY)
			i = sPIPDimensionalData.usPIPHistoryHead;
		else			
			i = (sPIPDimensionalData.usPIPHistoryHead + (MAX_PIP_DIM_HISTORY-sPIPDimensionalData.usPIPHistoryNumEntries))%MAX_PIP_DIM_HISTORY;
		
		// copy the requested number of entries	
		usEntriesToCopy = usNumEntries;		
		while (usEntriesToCopy-- > 0)
		{
			memcpy(pwEntries, &sPIPDimensionalData.PIPDimData[i], sizeof(DIM_DATA));
			++pwEntries;
			i = (i+1)%MAX_PIP_DIM_HISTORY;
		}
		return TRUE;  
	}
	else
		return FALSE;
}

/*
 * Name
 *   fnGetPIPLast256LogAddress
 *
 * Description
 *   
 *   The function retrieves pointer to last 256 piece log
 *	 SBR only
 *
 * Inputs
 *   Nothing.
 *
 * Returns
 *   pointer to  sPIPDimensionalData
 *
 */
PIP_DIM_HISTORY * fnGetPIPLast256LogAddress(void)
{
	return &sPIPDimensionalData;
}
