/************************************************************************
*       PROJECT:        Mega/Net
*       MODULE NAME:    $Workfile:   target.c  $
*       REVISION:       $Revision:   1.14.1.1  $
*
*       DESCRIPTION:    Handles the network parameters initialization
*
* ----------------------------------------------------------------------
*               Copyright (c) 2003 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
* MODIFICATION HISTORY:
* 
* 2013.08.26 Clarisa Bellamy
* - Move the new app function pointers to a new struct, NucNetAppFuncPtrs, of
*   a new type, T_NUCNETAPPFUNCS, and restore NUCNETCONFIG structure type.
*
* 2013.08.26 Clarisa Bellamy
*  To be more backwards compatible, restore the NUCNETCONFIG to its original 
*  structure, and add a new structure, NucNetAppFuncPtrs, which only contains
*  function pointers.       
*  Add a new function to set the function pointers, fnNetSetAppPtrs.
* 
* 2013.08.05 Clarisa Bellamy
*  To improve communications:
*  - In fnNetInitializeNetworkDefaults(), change the default RTO from 1/4 second 
*    to 1 second per the TCP protocol spec: RFC 6298.  Since, after it is 
*    defaulted, it is quickly set to the value in CMOS by the 
*    fnNetUpdateNetworkConfiguration(), modifications were also made to cmos.c 
*    to complete this change.
*  To allow net library to put entries into syslog:
*  - Two new members were added to the NUCNETCONFIG structure: 
*           pFnDumpToSystemLog  and pFnDumpToSystemLog;        
*    These are initialized to NULL_PTR in fnNetInitializeNetworkDefaults(), 
*    and set to the values determined by the application in 
*    fnNetUpdateNetworkConfiguration().
*
* ----------------------------------------------------------------------
*  PRE-CLEARCASE REVISION HISTORY:
*       $Log:   H:/group/SPARK/ARCHIVES/UIC/target.c_v  $
* 
*    Rev 1.14.1.1   May 12 2004 09:25:32   GE002MO
* Added Initialization string for pulse dialing
* 
*    Rev 1.14.1.0   May 07 2004 13:56:00   GE002MO
* Initializes new Members to NetworkConfig
* maxConnectTime The Maximum time Dial will wait for connection ( 100 Seconds )
* disableModemChecks will allow for turning off modem connection checking ( Checking On )
* carrierLossThreshold number of consecutive loss carrier indications before connection is terminated ( 1 )
* 
*    Rev 1.14   28 Jul 2003 12:37:16   CX08856
* fiexed hangup string
* 
*    Rev 1.13   Jul 15 2003 23:57:44   bellamyc
* Add initialization of Socket Modem Init String.
* For now, sets it the same as the extmodem Init String.
* 
* **********************************************************************/

#include "nucleus.h"
#include "pbtarget.h"
#include "externs.h"
#include "stdlib.h"
#include "string.h"
#include "ptypes.h"

NUCNETCONFIG        NucNetworkConfig;
T_NUCNETAPPFUNCS    NucNetAppFuncPtrs;


/* **********************************************************************
// FUNCTION NAME:               fnNetInitializeNetworkDefaults()
// PURPOSE: Sets valid default network parameters
//          these parameters can be over written with flash values
//
// INPUTS:  None
// RETURNS:
// HISTORY:
//  2013.08.05 Clarisa Bellamy - Change the minrto from 1/4 to 1 second per
//                  TCP spec RFC 6298.
// AUTHOR: George Monroe
// **********************************************************************/

void fnNetInitializeNetworkDefaults()
{
  unsigned char defIP[] = {192,168,0,10};
  unsigned char defGW[] = {192,168,0,1};
  unsigned char defPDNS[] = {192,168,0,1};
  unsigned char defSDNS[] = {192,168,0,1};
  unsigned char defMASK[] = {255,255,255,0};

  // Initialize the NetworkConfig structure:
  NucNetworkConfig.maxRto = ((UINT32)(240 * TICKS_PER_SECOND));  /* Maximum retransmit timeout.*/
  NucNetworkConfig.minRto = ((UINT32)(TICKS_PER_SECOND));   /* Min. retransmit timeout. */
  NucNetworkConfig.arpTimeout = ((UINT32)(1 * TICKS_PER_SECOND));    /* ARP timeout. */
  NucNetworkConfig.cacheTimeout = ((UINT32)(400 * TICKS_PER_SECOND));  /* ARP cache timeout. */
  NucNetworkConfig.waitTime = ((UINT32)(2 * TICKS_PER_SECOND));    /* Length of time to wait before reusing a port. */
  NucNetworkConfig.probeTime = (TICKS_PER_SECOND << 1);
  NucNetworkConfig.swsOverride = (TICKS_PER_SECOND >> 2) ; 
  NucNetworkConfig.dnsWait = TICKS_PER_SECOND * 5;   /* Time between retries on DNS lookups */
  NucNetworkConfig.maxConnectTime = 100;
  NucNetworkConfig.bModemType = MODEM_TYPE_USB;
  NucNetworkConfig.bEnableFTPServer = 0;   /* Enable FTP Server */
  NucNetworkConfig.staticIPFlag = FALSE;
  memcpy( NucNetworkConfig.staticIP , defIP , sizeof(NucNetworkConfig.staticIP));
  memcpy( NucNetworkConfig.staticSubnet , defMASK , sizeof(NucNetworkConfig.staticSubnet));
  memcpy( NucNetworkConfig.defaultGateway , defGW , sizeof(NucNetworkConfig.defaultGateway));
  memcpy(NucNetworkConfig.primaryDnsServer, defPDNS,sizeof(NucNetworkConfig.primaryDnsServer));
  memcpy(NucNetworkConfig.secondaryDnsServer, defSDNS,sizeof(NucNetworkConfig.secondaryDnsServer));
  strcpy(NucNetworkConfig.hostName , "comet.pb.com" );
  strcpy(NucNetworkConfig.dial_prefix , "ATDT");
  strcpy(NucNetworkConfig.pulse_dial_prefix , "ATDP");
  strcpy(NucNetworkConfig.attention , "AT^M" );
  strcpy(NucNetworkConfig.reset,"ATZ^M");
  strcpy(NucNetworkConfig.ignore_calls , "ATS0=0^M");
  strcpy(NucNetworkConfig.hangup_string , "~~~+++~~~ATH0^M");
  strcpy(NucNetworkConfig.accept_call , "ATS0=2^M");
  strcpy(NucNetworkConfig.modemInit,"ATm1S0=0V1X4&K3^M");
  memset(NucNetworkConfig.preInit1,0,sizeof(NucNetworkConfig.preInit1));
  memset(NucNetworkConfig.preInit2,0,sizeof(NucNetworkConfig.preInit2));
  memset(NucNetworkConfig.postInit1,0,sizeof(NucNetworkConfig.postInit1));
  memset(NucNetworkConfig.postInit2,0,sizeof(NucNetworkConfig.postInit2));

  // Initialize the AppFunctionPtr structure:
  //  A Null-ptr means that the app has not provided a ptr to that function.
  NucNetAppFuncPtrs.pFnDumpToSystemLog = NULL_PTR;
  NucNetAppFuncPtrs.pFnDumpToSystemLogWithNum = NULL_PTR;

  return;
}

//=====================================================================
//  Function Name:  fnNetUpdateNetworkConfiguration( NUCNETCONFIG *pConfig )
//
//  Description: This function allows for any application to set the network
//      parameters. This call should be made prior to calling the
//              initialize network function.
//
//  Parameters: pConfig - pointer to the Netconfig to update from
//      
//  Returns: none
//
//  Preconditions: none
//
//=====================================================================
void fnNetUpdateNetworkConfiguration( NUCNETCONFIG *pConfig )
{
  NucNetworkConfig.maxRto = pConfig->maxRto;  /* Maximum retransmit timeout.*/
  NucNetworkConfig.minRto = pConfig->minRto;   /* Min. retransmit timeout. */
  NucNetworkConfig.arpTimeout = pConfig->arpTimeout;    /* ARP timeout. */
  NucNetworkConfig.cacheTimeout = pConfig->cacheTimeout;  /* ARP cache timeout. */
  NucNetworkConfig.waitTime = pConfig->waitTime;    /* Length of time to wait before reusing a port. */
  NucNetworkConfig.probeTime = pConfig->probeTime;
  NucNetworkConfig.swsOverride = pConfig->swsOverride; 
  NucNetworkConfig.dnsWait = pConfig->dnsWait;   /* Time between retries on DNS lookups */
  NucNetworkConfig.maxConnectTime = pConfig->maxConnectTime;
  NucNetworkConfig.bModemType = pConfig->bModemType;   /* The type of modem or whether we are using a lan connection */
  NucNetworkConfig.bEnableFTPServer = pConfig->bEnableFTPServer;   /* Enable FTP Server */
  NucNetworkConfig.staticIPFlag = pConfig->staticIPFlag;
  memcpy( NucNetworkConfig.staticIP , pConfig->staticIP , sizeof(NucNetworkConfig.staticIP));
  memcpy( NucNetworkConfig.staticSubnet , pConfig->staticSubnet , sizeof(NucNetworkConfig.staticSubnet));
  memcpy( NucNetworkConfig.defaultGateway , pConfig->defaultGateway , sizeof(NucNetworkConfig.defaultGateway));
  strncpy(NucNetworkConfig.hostName,pConfig->hostName,sizeof(NucNetworkConfig.hostName) );
  strncpy(NucNetworkConfig.dial_prefix,pConfig->dial_prefix,sizeof(NucNetworkConfig.dial_prefix) );
  strncpy(NucNetworkConfig.pulse_dial_prefix ,pConfig->pulse_dial_prefix ,sizeof(NucNetworkConfig.pulse_dial_prefix ));
  strncpy(NucNetworkConfig.attention ,pConfig->attention ,sizeof(NucNetworkConfig.attention ) );
  strncpy(NucNetworkConfig.reset,pConfig->reset,sizeof(NucNetworkConfig.reset));
  strncpy(NucNetworkConfig.ignore_calls ,pConfig->ignore_calls ,sizeof(NucNetworkConfig.ignore_calls ));
  strncpy(NucNetworkConfig.hangup_string ,pConfig->hangup_string ,sizeof(NucNetworkConfig.hangup_string ));
  strncpy(NucNetworkConfig.accept_call ,pConfig->accept_call ,sizeof(NucNetworkConfig.accept_call ));
  strncpy(NucNetworkConfig.country ,pConfig->country ,sizeof(NucNetworkConfig.country ));
  strncpy(NucNetworkConfig.modemInit,pConfig->modemInit,sizeof(NucNetworkConfig.modemInit));
  memcpy(NucNetworkConfig.primaryDnsServer,pConfig->primaryDnsServer,sizeof(NucNetworkConfig.primaryDnsServer));
  memcpy(NucNetworkConfig.secondaryDnsServer,pConfig->secondaryDnsServer,sizeof(NucNetworkConfig.secondaryDnsServer));
  strncpy(NucNetworkConfig.attLoginAccountAndUserId,pConfig->attLoginAccountAndUserId,sizeof(NucNetworkConfig.attLoginAccountAndUserId));
  strncpy(NucNetworkConfig.attPassword,pConfig->attPassword,sizeof(NucNetworkConfig.attPassword));
  strncpy(NucNetworkConfig.preInit1,pConfig->preInit1,sizeof(NucNetworkConfig.preInit1));
  strncpy(NucNetworkConfig.postInit1,pConfig->postInit1,sizeof(NucNetworkConfig.postInit1));
  strncpy(NucNetworkConfig.preInit2,pConfig->preInit2,sizeof(NucNetworkConfig.preInit2));
  strncpy(NucNetworkConfig.postInit2,pConfig->postInit2,sizeof(NucNetworkConfig.postInit2));

  return;
}

//=====================================================================
//  Function Name:      fnNetSetAppPtrs
//  Description: 
//      This function allows for an application to set one or more 
//      pointers in the NucNetAppFuncPtrs structure.
//  INPUTS:
//      pAppFuncs - pointer to the T_NUCNETAPPFUNCS structure containing
//              pointers to the apps functions.
//  OUTPUTS:
//      None.
//  NOTES:
//  1. If a pointer in the app structure is blank, do not copy it.
//
// HISTORY:
// 2013.08.26 Clarisa Bellamy - Initial
//=====================================================================
void fnNetSetAppPtrs( T_NUCNETAPPFUNCS *pAppFuncs )
{
    // Save each non-null function pointer.
    if( pAppFuncs->pFnDumpToSystemLog != NULL_PTR )
    {
        NucNetAppFuncPtrs.pFnDumpToSystemLog          = pAppFuncs->pFnDumpToSystemLog;
    }
    if( pAppFuncs->pFnDumpToSystemLogWithNum != NULL_PTR )
    {
        NucNetAppFuncPtrs.pFnDumpToSystemLogWithNum   = pAppFuncs->pFnDumpToSystemLogWithNum;
    }

  return;
}

//=====================================================================
//
//  Function Name:  fnNetUpdateModemInitializationString( unsigned char pNewStr )
//
//  Description: This function allows for any application to update
//              just the modem initialization string.
//      This eliminates the need to reinitialize
//              the entire structure when we switch modems
//
//  Parameters: pNewStr - pointer to the new modem initialization string
//      
//      
//      
//  Returns: none
//      
//
//  Preconditions: none
//                  
//
//=====================================================================

void fnNetUpdateModemInitializationString( unsigned char *pNewStr )
{
  strncpy(NucNetworkConfig.modemInit, ( char *) pNewStr , sizeof(NucNetworkConfig.modemInit));
  return;
}

//=====================================================================
//
//  Function Name:  fnNetUpdateModemType( unsigned char pNewStr )
//
//  Description: This function allows for any application to update
//              just the modem type and initialization string
//
//  Parameters: bNewType -- The new modem type 
//          bNewStr --  The Modem initialization string
//      
//      
//  Returns: none
//      
//
//  Preconditions: none
//                  
//
//=====================================================================

void fnNetUpdateModemType( unsigned char bNewType , unsigned char *pNewStr )
{
  NucNetworkConfig.bModemType = bNewType;
  strncpy(NucNetworkConfig.modemInit, ( char *) pNewStr , sizeof(NucNetworkConfig.modemInit));
  return;
}

//=====================================================================
//
//  Function Name:  fnNetSetPCConnectStatus( unsigned char bStatus )
//
//  Description: This function allows for any application to update
//              the status pretaining to whether we are connected to a PC
//
//  Parameters: bStatus - The new status 
//      <> 0 connected else 0 = disconnected
//      
//      
//  Returns: none
//      
//
//  Preconditions: none
//                  
//
//=====================================================================

void fnNetSetPCConnectStatus( unsigned char bStatus )
{
  NucNetworkConfig.bPCConnectStatus = bStatus;
  return;
}

//=====================================================================
//
//  Function Name:  fnNetSetToneDialing( unsigned char pSetTone )
//
//  Description: This function allows for any application to update
//              the dialing mode. non-zero for tone dialing else pulse
//
//  Parameters: bSetTone - Dialing mode
//      
//      
//      
//  Returns: none
//      
//
//  Preconditions: none
//                  
//
//=====================================================================

void fnNetSetToneDialing( unsigned char bSetTone )
{
  NucNetworkConfig.ToneDialing = bSetTone;
  return;
}


//=====================================================================
//
//  Function Name:  fnNetSetStaticConfiguration
//
//  Description: This function allows for setting a static IP address
//      without having to send all of the network parameters
//
//  Parameters: staticFlag -- specifies whether to enable static IP's
//      staticIP -- points to the array containing the static IP to assign
//      staticSubnet -- points to the subnet mask 
//      defaultGateway -- points to the IP address for the default gateway
//
//  Returns:NONE
//      
//
//  Preconditions:
//                  
//
//=====================================================================
void fnNetSetStaticConfiguration( unsigned char staticFlag , unsigned char *staticIP,
                  unsigned char *staticSubnet , unsigned char *defaultGateway)
{
  NucNetworkConfig.staticIPFlag = staticFlag;
  memcpy( NucNetworkConfig.staticIP , staticIP , sizeof(NucNetworkConfig.staticIP));
  memcpy( NucNetworkConfig.staticSubnet , staticSubnet , sizeof(NucNetworkConfig.staticSubnet));
  memcpy( NucNetworkConfig.defaultGateway , defaultGateway , sizeof(NucNetworkConfig.defaultGateway));
}
