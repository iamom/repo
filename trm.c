


#include <errno.h>
#include <posix.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

#include "trmdefs.h"
#include "trm.h"
#include "trmstrg.h"

#include "datdict.h"
#include "pbos.h"
#include "clock.h"
#include "dir_defs.h"
#include "pcdisk.h"
#include "ipsd.h"
#include "zlib.h"
#include "bkgndmgr.h"
#include "sysdatadefines.h"
#include "api_funds.h"
#include "api_status.h"
#include "oit.h"
#include "networkmonitor.h"
#include "api_temporary.h"
#include "ossetup.h"
#include "errcode.h"
#include "custdat.h"

extern BOOL trm_log_level;

extern DCB_PARAMETERS_t *pParameters;

extern NU_MEMORY_POOL    	*pSystemMemoryCached;

//extern BOOL 			  gIntellilinkNotBusy;
//static NU_SEMAPHORE trmSemaphore, *pTrmSemaphore = NULL;
extern BOOL     		fDCAPUploadReqShown;
extern BOOL     		fDCAPUploadDueShown;
extern BOOL     		fUploadInProgress;
extern required_fields_t trm_required_fields;

extern char 			*trmFileList[TRM_MAX_NUMBER_OF_FILES];
extern DATETIME 		trmPkgEndDateTime;
extern BOOL     		 fTrmFatalError;
extern uint32_t 		gTrmFatalError;
extern BOOL fnGetUpdateNow (UINT8 bServiceCode, INT32 value, INT32 value2);
void CleanDCAPBuffer(void);
BOOL fnFolderExists(CHAR *folderName);
VOID fnFilePath(CHAR* outPath, CHAR* , CHAR* fileName);

DATETIME trmTimeStampOfLatestRec;

//int ReadPSDRegisterLog(const int psdID, const REG_LOG_MSG_TYPE *regLog, psd_reg_log_str_t *psdRegLog, int *status)
//{
//#if 0
//	sPsd_reg_log_t stempPSDRegLog;
//
//	if(m_psdRegLog == NULL)
//	{
//		m_psdRegLog = new CPSDRegisterLog;
//	}
//
//	m_psdRegLog->RestorePSDLog(psdID);
//
//	memset(&stempPSDRegLog, 0x0, sizeof(stempPSDRegLog));
//	status = m_psdRegLog->GetLastLogEntry(psdID, stempPSDRegLog);
//	if(status == SUCCESS)
//	{
//		memcpy(&psdRegLog, (sPsd_reg_log_t)&stempPSDRegLog, sizeof(stempPSDRegLog) );
//	}
//#endif
//	PSD_ERR_CODE errCode;
//    int rc = FM_SUCCESS;
//
//    unsigned long outSize = 0;
//    uint16_t numOfEntries = 0;
//    unsigned char outputBuff[1024];
//    unsigned char *poutputBuff = NULL;
//    GET_LOG_MSG_TYPE getLogs;
//
//    memset(outputBuff, 0x0, sizeof(outputBuff));
//    poutputBuff = &outputBuff[0];
//
//    CFundsMgrPSDInterface fundMgrPSDInterface(psdID);
//    outSize = sizeof(outputBuff);
//
//    memcpy(&getLogs, &regLog, sizeof(getLogs));
//
//    rc = fundMgrPSDInterface.GetLogs(getLogs, outSize, poutputBuff, numOfEntries, errCode);
//
//	if(rc == SUCCESS && errCode == SUCCESS)
//	{
//        memcpy(&psdRegLog, (psd_reg_log_str_t *)poutputBuff, outSize);
//
//    	psdRegLog.piece_cnt = ENDIAN_BE32((uint32_t)psdRegLog.piece_cnt);
//    	psdRegLog.zero_piece_cnt  = ENDIAN_BE32(psdRegLog.zero_piece_cnt );
//    	ChangeEndianFormat(psdRegLog.asc_reg, sizeof(psdRegLog.asc_reg));
//    	ChangeEndianFormat(psdRegLog.desc_reg, sizeof(psdRegLog.desc_reg));
//
//        status = SUCCESS;
//        dbgTrace(DBG_LVL_INFO,
//	            "TRM - ReadPSDRegisterLog:psdRegLog.sRecoveryData.jobid=%d, psdRegLog.sRecoveryData.transaction_id= %d, psdRegLog.piece_cnt= %d, outSize =%ld"
//        		,psdRegLog.sRecoveryData.jobid
//        		,psdRegLog.sRecoveryData.transaction_id
//        		,psdRegLog.piece_cnt
//        		,outSize);
//	}
//	else
//	{
//		status = errCode;
//	}
//	return status;
//}


//Not Required Shadow registers are in cmos
//void InitializePSDRegLog(psd_reg_log_str_t *psdRegLog)
//{
//	memset(psdRegLog.desc_reg, 0x0, sizeof(psdRegLog.desc_reg));
//	memset(psdRegLog.asc_reg, 0x0, sizeof(psdRegLog.asc_reg));
//	memset(psdRegLog.control_sum, 0x0, sizeof(psdRegLog.control_sum));
//	psdRegLog.operation = 0;
//	psdRegLog.piece_cnt = 0;
//	psdRegLog.postage = 0;
//	psdRegLog.submission_date = 0;
//	memset(psdRegLog.log_time, 0x0, sizeof(psdRegLog.log_time));
//	psdRegLog.zero_piece_cnt = 0;
//
//	//Recovery Data
//	psdRegLog.sRecoveryData.jobid = 0;
//	psdRegLog.sRecoveryData.dimension_index = 0;
//	psdRegLog.sRecoveryData.transaction_id = 0;
//	psdRegLog.sRecoveryData.raw_weight = 0;
//	psdRegLog.sRecoveryData.track_Id_table_Index = 0;
//	psdRegLog.sRecoveryData.backup_counter = 0;
//	memset(psdRegLog.sRecoveryData.reserved, 0x0, sizeof(psdRegLog.sRecoveryData.reserved));
//	memset(psdRegLog.sRecoveryData.psd_no_debit_counter, 0x0, sizeof(psdRegLog.sRecoveryData.psd_no_debit_counter));
//}


void ConvertDtBCDToYYYYMMDD(uint32_t uDate, char *pDateSubmission)
{
	uint32_t tmp = 0;
	int i;
	char DateOfSubmission[DATE_LEN];

	memset(DateOfSubmission, 0x0, sizeof(DateOfSubmission));

	for ( i = 7; i>= 0; i--)
	{
        tmp = (uDate)>>((7 - i)*4);
        tmp = tmp & 0xF;

        DateOfSubmission[i] = (unsigned char)(tmp + '0');
    }

	char *p_src = &DateOfSubmission[0];
    char *p_dst = &pDateSubmission[0];

    /* DateOfSumission is "YYYYMMDD" */
    /* date_of_submission needs to become "YYYY-MM-DD" */

    *p_dst++ = *p_src++; *p_dst++ = *p_src++; *p_dst++ = *p_src++; *p_dst++ = *p_src++;
    *p_dst++ = '-';
    *p_dst++ = *p_src++; *p_dst++ = *p_src++;
    *p_dst++ = '-';
    *p_dst++ = *p_src++; *p_dst++ = *p_src++;
    *p_dst = 0;

}

//TODO
//int BuildRecoveryTRec(mp_id_t mp_id, ShadowDebitLogEntry *psdRegLog, uint32_t *currTransID)
//{
//	int result = 0;
//	unsigned char buffer[2048];
//
//	CreateTR(mp_id);
//
//	dbgTrace(DBG_LVL_INFO,
//	            "TRM - BuildRecoveryTRec:p_trm_mem->trans_rec[mp_id].transaction_id =%d and p_trm_mem->header.next_transaction_id = %d ", p_trm_mem->trans_rec[mp_id].transaction_id, p_trm_mem->header.next_transaction_id);
//
//
//	//if Retained Batch count is active then set the populated flag.
////	if(p_trm_mem->header.retained_cnt.bActive)
////	{
////		p_trm_mem->trans_rec[mp_id].retained_cnt = psdRegLog->.sRecoveryData.backup_counter;
////		p_trm_mem->PostRetainedBC(mp_id);
////	}
//
//	psdRegLog->submission_date = ENDIAN_BE32(psdRegLog->submission_date);
//
//
//	ConvertDtBCDToYYYYMMDD(psdRegLog->submission_date, p_trm_mem->header.date_of_submission);
//
//	dbgTrace(DBG_LVL_INFO,
//	            "TRM - BuildRecoveryTRec:p_trm_mem->header.date_of_submission =%s"
//				,p_trm_mem->header.date_of_submission
//				);
//
//
//	//Put Registers from PSD Log
//	p_trm_mem->PutPSDRegisters(mp_id
//			psdRegLog->lDebitPieceCount, //psdRegLog.piece_cnt
//			psdRegLog->lZeroPieceCount, //psdRegLog.zero_piece_cnt
//			psdRegLog->bAR, //psdRegLog.asc_reg
//			psdRegLog->bDR, //psdRegLog.desc_reg
//			p_trm_mem->header.num_dec_places_in_PSD);
//
//
//	//Selected Graphics List
//	graphic_items_v vecPrintedGraphic;
//	graphic_item_t PrintedItems;
//	vecPrintedGraphic.clear();
//	int i = 0;
//	for(i = 0; i < userSelectedGrf.length; i++)
//	{
//		strncpy(PrintedItems.className, "", sizeof(PrintedItems.className));
//
//		int size = strlen(m_.GraphicElement[i].GrfFileName);
//		strncpy(PrintedItems.printFName, userSelectedGrf.GraphicElement[i].GrfFileName,  size);
//
//		vecPrintedGraphic.push_back(PrintedItems);
//	}
//
//
//
// 	//Get Power Failure Info from MCP services
//   	sPowerFailureInfo pfInfo;
//   	MCPServices mcpsvcs;
//
//   	pfInfo.transactionId = 0;
//   	pfInfo.printLengthColumns = 0;
//   	result = mcpsvcs.GetPowerFailureInfo(pfInfo);
//   	if(result == SUCCESS)
//   	{
//   		if(p_trm_mem->trans_rec[mp_id].transaction_id < pfInfo.transactionId )
//   		{
//   			p_trm_mem->trans_rec[mp_id].PF_piece_status_exited = true;
//   		}
//   		else if (p_trm_mem->trans_rec[mp_id].transaction_id > pfInfo.transactionId )
//   		{
//
//   			p_trm_mem->trans_rec[mp_id].PF_piece_status_debited = true;
//   		}
//   		else if(pfInfo.transactionId == p_trm_mem->trans_rec[mp_id].transaction_id)
//   		{
//   			if(pfInfo.printLengthColumns != 0)
//   			{
//   				p_trm_mem->SetColumnPrinted(mp_id, pfInfo.printLengthColumns);
//   			}
//   			else
//   			{
//   				p_trm_mem->trans_rec[mp_id].PF_piece_status_debited = true;
//   			}
//   		}
//   	}
//
//   	//Populate mac if available
//   	mac_data_t mac_data;
//   	result = GetMACInfoFromDS(p_trm_mem->trans_rec[mp_id].transaction_id, mac_data);
//   	if(result == SUCCESS)
//   	{
//   		p_trm_mem->SetMACData(mp_id, mac_data.macdata);
//   	}
//
//   	if(p_trm_mem->header.b_rate_required == true)
//	{
//		CRatingServicesInterface rating;
//		sMailPieceDimensionsResponse Dimensions;
//
//		dbgTrace(DBG_LVL_INFO,
//		            "TRM - BuildRecoveryTRec:psdRegLog.sRecoveryData.raw_weight =%ld and psdRegLog.sRecoveryData.dimension_index = %d "
//					,psdRegLog.sRecoveryData.raw_weight,
//					psdRegLog.sRecoveryData.dimension_index);
//
//
//		CTrackingServicesData trackingSrvs;
//
//		dbgTrace(DBG_LVL_INFO,
//		            "TRM - BuildRecoveryTRec:psdRegLog.sRecoveryData.track_Id_table_Index =%d"
//					,psdRegLog.sRecoveryData.track_Id_table_Index);
//		if(trackingSrvs.isTrackingServiceActive())
//		{
//			trackingSrvs.SetTrackingIndex(psdRegLog.sRecoveryData.track_Id_table_Index);
//		}
//
//		unsigned long dataSize = 0;
//		result = rating.RequestRating(buffer,
//								sizeof(buffer),
//			                    dataSize,
//			                    psdRegLog.sRecoveryData.raw_weight,
//			                    psdRegLog.sRecoveryData.dimension_index);
//
//		if(result == MAILPIECE_OKAY)
//		{
//			result = SUCCESS;
//			p_trm_mem->TR_Rated(mp_id, (char * const)buffer, dataSize);
//
//			uint16_t thickness = 0;
//			rating.GetMaxDimensionData(buffer,
//									Dimensions.dimensions.length,
//	    							Dimensions.dimensions.width,
//	    							thickness);
//
//			Dimensions.dimensions.thickness = thickness;
//			Dimensions.dimensions.wowWeight = psdRegLog.sRecoveryData.raw_weight;
//
//			p_trm_mem->UpdateMediaInfo(mp_id, Dimensions.dimensions);
//
//			sMailPieceLengthResponse Length;
//			Length.length = Dimensions.dimensions.length;
//			p_trm_mem->SetLength(mp_id, Length.length);
//
//
//			//Get AD Graphic file from Rating
//			eRateDataType type = eUndefinedType;
//            uint32_t  data_size = 0;
//            char pRateData[128];
//
//			uint8_t bAutoGraphic_flag = 0;
//
//			memset(pRateData, 0x0,sizeof(pRateData));
//
//			rating.GetRateData(eAutoInscriptionFlag, 0, buffer,
//										&bAutoGraphic_flag, sizeof(bAutoGraphic_flag), type, data_size);
//
//
//			if(bAutoGraphic_flag == 1)
//			{
//
//				rating.GetRateData(eAutoInscription,0, buffer,
//						pRateData, sizeof(pRateData), type, data_size);
//
//				strncpy(PrintedItems.className, "", sizeof(PrintedItems.className));
//
//				int size = strlen(pRateData);
//				strncpy(PrintedItems.printFName, pRateData,  size);
//				vecPrintedGraphic.push_back(PrintedItems);
//			}
//		}
//	}
//
//
//	p_trm_mem->SetPrintedGraphics(mp_id,vecPrintedGraphic);
//
//	dbgTrace(DBG_LVL_INFO,
//            "TRM - BuildRecoveryTRec:Request Rating status =%d", result);
//
//	currTransID = p_trm_mem->trans_rec[mp_id].transaction_id;
//	p_trm_mem->SetTRecRecreated(mp_id);
//	return result;
//}






////////////////////////////////////////////////////////////////////////////////////
static char    *pXml = NULL;
static int32_t lXmlLength;
static int32_t lXmlBufLength;

static char    *pDayMpXml = NULL;
static int32_t lDayMpXmlLength;
static int32_t lDayMpXmlBufLength;

static char    *pMpXml = NULL;
static int32_t lMpXmlLength;
static int32_t lMpXmlBufLength;


static trm_mem_t  RAMTrmArea;

//static BOOL bNMR;

#define MAX_XML_SIZE_PER_MAIL_PIECE	1500
#define MAX_XML_SIZE_HDR			1500

char* trmAllocateMPBuffer( int recordCount)
{
	if(lMpXmlBufLength < MAX_XML_SIZE_PER_MAIL_PIECE*recordCount)
	{
		if(pMpXml != NULL)
			NU_Deallocate_Memory(pMpXml);

		lMpXmlBufLength = MAX_XML_SIZE_PER_MAIL_PIECE*recordCount;
		pMpXml = NULL;

		NU_Allocate_Memory(pSystemMemoryCached, ( void *) &pMpXml, lMpXmlBufLength, NU_NO_SUSPEND);
		if(pMpXml != NULL)
			dbgTrace(DBG_LVL_INFO, "trmAllocateXmlBuffer buffer length:%d, record count:%d \n", lMpXmlBufLength, recordCount);
		else
			dbgTrace(DBG_LVL_INFO, "trmAllocateXmlBuffer  failed: buffer length:%d, record count:%d \n", lMpXmlBufLength, recordCount);
	}

	return pMpXml;
}

char* trmAllocateDayMPBuffer( int recordCount)
{
	if(lDayMpXmlBufLength < MAX_XML_SIZE_PER_MAIL_PIECE*recordCount)
	{
		if(pDayMpXml != NULL)
			NU_Deallocate_Memory(pDayMpXml);

		lDayMpXmlBufLength = MAX_XML_SIZE_PER_MAIL_PIECE*recordCount;
		pDayMpXml = NULL;

		NU_Allocate_Memory(pSystemMemoryCached, ( void *) &pDayMpXml, lDayMpXmlBufLength, NU_NO_SUSPEND);
		if(pDayMpXml != NULL)
			dbgTrace(DBG_LVL_INFO, "trmAllocateDayMPBuffer buffer length:%d, record count:%d \n", lDayMpXmlBufLength, recordCount);
		else
			dbgTrace(DBG_LVL_INFO, "trmAllocateDayMPBuffer  failed: buffer length:%d, record count:%d \n", lDayMpXmlBufLength, recordCount);
	}

	return pMpXml;
}

char* trmAllocateXmlBuffer( void)
{
	if( (lXmlBufLength < (MAX_XML_SIZE_HDR + lDayMpXmlLength)) ||
		(pXml == NULL) )
	{
		if(pXml != NULL)
		{
			NU_Deallocate_Memory(pXml);
			pXml = NULL;
		}

		lXmlBufLength = (MAX_XML_SIZE_HDR + lDayMpXmlLength);

		NU_Allocate_Memory(pSystemMemoryCached, ( void *) &pXml, lXmlBufLength, NU_NO_SUSPEND);

		if(pXml != NULL)
			dbgTrace(DBG_LVL_INFO, "trmAllocateXmlBuffer buffer length:%d, lDayMpXmlLength xml Lenght:%d \n", lXmlBufLength, lDayMpXmlLength);
		else
			dbgTrace(DBG_LVL_INFO, "trmAllocateXmlBuffer  failed: buffer length:%d, lDayMpXmlLength xml Lenght:%d \n", lXmlBufLength, lDayMpXmlLength);
	}

	return pXml;
}


void trmReleaseMemory(void)
{
	if(pXml != NULL)
		NU_Deallocate_Memory(pXml);

	if(pDayMpXml != NULL)
			NU_Deallocate_Memory(pDayMpXml);

	if(pMpXml != NULL)
		NU_Deallocate_Memory(pMpXml);

	pXml = NULL;
	lXmlBufLength = 0;
	lXmlLength = 0;

	pDayMpXml = NULL;
	lDayMpXmlLength = 0;
	lDayMpXmlBufLength = 0;

	pMpXml = NULL;
	lMpXmlLength = 0;
	lMpXmlBufLength = 0;

	dbgTrace(DBG_LVL_ERROR, "trmReleaseMemory: ");
}

void trmReleaseDayMPBuffers(void)
{
	if(pDayMpXml != NULL)
			NU_Deallocate_Memory(pDayMpXml);

	if(pMpXml != NULL)
		NU_Deallocate_Memory(pMpXml);

	pDayMpXml = NULL;
	lDayMpXmlLength = 0;
	lDayMpXmlBufLength = 0;

	pMpXml = NULL;
	lMpXmlLength = 0;
	lMpXmlBufLength = 0;

	dbgTrace(DBG_LVL_ERROR, "trmReleaseDayMPBuffers: ");
}


STATUS generate_day_xml(char *p_dest, int32_t max_xml_length, int32_t *p_xml_len, DATETIME *pDateTime, pkg_info_t *pDayInfo, bool last)
{
	STATUS status;
    void *temp_buf = NULL;
	char *p_wr = p_dest;
	char *p_limit = p_dest + max_xml_length;
    uint64_t register_val;

    #define add_to_xml(str) p_wr = str_n_cat(p_wr, (str), p_limit - p_wr)

    status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &temp_buf, CFG_MAX_DAY_ENTRY_SIZE, NU_NO_SUSPEND);
    if(temp_buf != NULL)
	{

		sprintf(temp_buf, "<Day>");
		add_to_xml(temp_buf);

		//GetSysDateTime(&rDateTimeStamp, ADDOFFSETS, GREGORIAN);
		snprintf(temp_buf, 48, "<Date>%02d%02d-%02d-%02d</Date>",
								pDateTime->bCentury,
								pDateTime->bYear,
								pDateTime->bMonth,
								pDateTime->bDay);

		//sprintf(temp_buf, "<Date>&s</Date>", temp_buf);
		add_to_xml(temp_buf);

		//if processing Transaction file use rate versin from file header otherwise use rate version from FRAM header
		if(strncmp( RAMTrmArea.header.rate_ver, (char*)CMOSTrmCommon.rate_ver, sizeof(CMOSTrmCommon.rate_ver))  < 0)
		{
			snprintf(temp_buf, 48, "<RateVer>%s</RateVer>", CMOSTrmCommon.rate_ver);
			dbgTrace(DBG_LVL_INFO, "generate_day_xml: CMOSTrmCommon Rate version :%s ",CMOSTrmCommon.rate_ver);
		}
		else
		{
			snprintf(temp_buf, 48, "<RateVer>%s</RateVer>", RAMTrmArea.header.rate_ver);
			dbgTrace(DBG_LVL_INFO, "generate_day_xml: RAMTrmArea.header.rate_ver:%s,  CMOSTrmCommon.rate_ver:%s ",RAMTrmArea.header.rate_ver, CMOSTrmCommon.rate_ver);
		}

		add_to_xml(temp_buf);

		if(last)
		{
			sprintf(temp_buf, "<Last>true</Last>");
			add_to_xml(temp_buf);
		}

		register_val = LE_byte_array_to_uint64_t( &pDayInfo->start_ar, sizeof( pDayInfo->start_ar));
		//register_val = fnBinFive2Double( p_trm->trans_rec[start_idx].ar);
		snprintf(temp_buf, 32, "<SAR>%llu</SAR>", register_val);
		add_to_xml(temp_buf);


		//
		register_val = LE_byte_array_to_uint64_t(&pDayInfo->start_dr, sizeof(pDayInfo->start_dr));
		//register_val = fnBinFive2Double( p_trm->trans_rec[start_idx].dr);
		snprintf(temp_buf, 32, "<SDR>%llu</SDR>", register_val);
		add_to_xml(temp_buf);

		snprintf(temp_buf, 32, "<SPC>%u</SPC>",pDayInfo->start_pc);
		add_to_xml(temp_buf);

		register_val = LE_byte_array_to_uint64_t(&pDayInfo->end_ar, sizeof(pDayInfo->end_ar));
		//register_val = fnBinFive2Double( p_trm->trans_rec[end_idx].ar);
		snprintf(temp_buf, 32, "<EAR>%llu</EAR>", register_val);
		add_to_xml(temp_buf);

		register_val = LE_byte_array_to_uint64_t(&pDayInfo->end_dr, sizeof(pDayInfo->end_dr));
		//register_val = fnBinFive2Double(p_trm->trans_rec[end_idx].dr);
		snprintf(temp_buf, 32, "<EDR>%llu</EDR>", register_val);
		add_to_xml(temp_buf);

		snprintf(temp_buf, 32, "<EPC>%u</EPC>", pDayInfo->end_pc);
		add_to_xml(temp_buf);

		*p_xml_len = p_wr - p_dest;

		NU_Deallocate_Memory(temp_buf);
	}

    return status;
}

void trmDATETIMEToDateStr(DATETIME *pDateTime, char* pDateStr, int size)
{

	snprintf(pDateStr, size, "%2d%02d%02d%02d%02d%02d%02d",
								pDateTime->bCentury,
								pDateTime->bYear,
								pDateTime->bMonth,
								pDateTime->bDay,
								pDateTime->bHour,
								pDateTime->bMinutes,
								pDateTime->bSeconds);
}


STATUS generate_day_mp_xml(int filesToProcess, DATETIME *pStartDate, DATETIME *pEndDate, BOOL *pNMR)
{
	STATUS status = SUCCESS;
	char path[64];
	//char path2[64];
	int cnt;
	int count;
	int idx;
	char *ptemp = NULL;
	int trxFileCount = 0;
	pkg_info_t prev_day;
	pkg_info_t cur_day;
	DATETIME curRecDateTime;
	DATETIME dateTimeProcessing;
	BOOL bValidDate;
    long lDateDiffEnd;
    char *pDayXmlLimit = NULL;
	int32_t day_xml_len = 0;
	int32_t day_tot_xml_len = 0;

	int32_t mp_xml_len = 0;

	uint16_t file_xml_len = 0;
	int readCount = 0;

	char *pTmpDayXml = NULL;
	char *pTmpMpXml = NULL;
	mp_id_t cur_mp_id = 0;
	BOOL bEndOfPeriodReached = false;
	DATETIME dummy;
	BOOL last = false;
	int cntValid = 0;
	TXREC_FILE_INFO txFileInfo;
	TXREC_FILE_INFO_EX txFileInfoEx;

	*pNMR = true;

	int length = sizeof(RAMTrmArea);
	fnFilePath(path, "trs", "");

	//Clear Uploaded file list
	memset(&CMOSTrmFileList, 0, sizeof(CMOSTrmFileList));
	memset(&dateTimeProcessing, 0, sizeof(dateTimeProcessing));
	memset(&dummy, 0, sizeof(dummy));

	memcpy(&curRecDateTime, pEndDate, sizeof(DATETIME));

	//Prepare Mail piece buffer
	if( (trmAllocateMPBuffer ( (filesToProcess + 1)* CFG_NUM_RECORDS ) != NULL) &&
			(trmAllocateDayMPBuffer ( (filesToProcess + 1)* CFG_NUM_RECORDS ) != NULL) )
	{

		pDayXmlLimit = pDayMpXml + lDayMpXmlBufLength;

		//Copy starting package info
		memcpy(&prev_day, &CMOSTrmPkgInfoPrev, sizeof(CMOSTrmPkgInfoPrev));
		memcpy(&cur_day, &CMOSTrmPkgInfoCur, sizeof(CMOSTrmPkgInfoPrev));

		dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml-FileCount: %d, Start Date: %02d%02d-%02d-%02d %02d:%02d:%02d, End Date: %02d%02d-%02d-%02d %02d:%02d:%02d",
														trxFileCount,
														pStartDate->bCentury,
														pStartDate->bYear,
														pStartDate->bMonth,
														pStartDate->bDay,
														pStartDate->bHour,
														pStartDate->bMinutes,
														pStartDate->bSeconds,
														//
														pEndDate->bCentury,
														pEndDate->bYear,
														pEndDate->bMonth,
														pEndDate->bDay,
														pEndDate->bHour,
														pEndDate->bMinutes,
														pEndDate->bSeconds);

		//Prepare list of Transaction record files and sort it
		//cntValid = trxFileCount;
		status = trmPrepareFileNamesListEx(trmFileList, &cntValid);
		if((status == SUCCESS) && (cntValid > 0))
		{

			trxFileCount = cntValid;

			//Copy Date part of the file
			//strncpy(curDate, (char*)trmFileList[0] + 13, 8);
			pTmpDayXml = pDayMpXml + lDayMpXmlLength;
			day_xml_len = 0;
			day_tot_xml_len = 0;

			count = 0;
			sendUploadProgress(UPLOAD_EVENT_TRX_PREPARING_PKG, count, trxFileCount);

			do{
				readCount = 0;
				dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml: First File: %s File Count: %d/%d ", trmFileList[count], count+1, trxFileCount);
				//if read fails try 3 times then declare as ssytem error
				do{
					if(readCount++ > 2)
						break;

					if(strstr(trmFileList[count], "TRD-") == 0)
						status = trmReadTxFile("trs", trmFileList[count], (uint8_t*)&RAMTrmArea, &length, &txFileInfo);
					else
						status = trmReadTxFileEx("trs", trmFileList[count], (uint8_t*)&RAMTrmArea, &length, &txFileInfoEx);

					if(status != SUCCESS)
						dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml: trmReadTxFile failed status:%d, attempt:%d, File: %s ", status, readCount, trmFileList[count]);

				}while(status != SUCCESS);

				if(status == SUCCESS)
				{
					//Upload MAX_FILE_LIST at a time
					if(CMOSTrmMaxFilesToUpload > MAX_FILES_TO_UPLOAD_EX)
						CMOSTrmMaxFilesToUpload = MAX_FILES_TO_UPLOAD_EX;

					for(; (count < trxFileCount) && (count < CMOSTrmMaxFilesToUpload) && (bEndOfPeriodReached == false); count++)
					{
						sendUploadProgress(UPLOAD_EVENT_TRX_PREPARING_PKG, count, trxFileCount);

						dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml-File-Processing:%s, records in use:%d",
													trmFileList[count], RAMTrmArea.header.num_records_in_use);


						pTmpMpXml = pMpXml + lMpXmlLength;
						cur_mp_id = 0;
						file_xml_len = 0;
						mp_xml_len = 0;

						for(cnt =  0; cnt < CFG_NUM_RECORDS; cnt++)
						{

							//start with record after current mail piece
							idx = get_mp_idx(cur_mp_id + cnt);

							if( (RAMTrmArea.trans_rec[idx].rec_state != TRM_UNUSED) )
								//This is to process valid records only to prevent non erased record if it happens
								//(RAMTrmArea.header.num_records_in_use > 0) )
							{

								//date time of current record
								memcpy(&trmTimeStampOfLatestRec, &RAMTrmArea.trans_rec[idx].start_time, sizeof(curRecDateTime));

								bValidDate = CalcDateTimeDifference( &trmTimeStampOfLatestRec, pEndDate, &lDateDiffEnd );

								if(cnt == 0 || trm_log_level)
									dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml-curRecDateTime Record: %d: %02d%02d-%02d-%02d %02d:%02d:%02d, bValidDate: %d, lDateDiffEnd: %d ",
																					idx,
																					trmTimeStampOfLatestRec.bCentury,
																					trmTimeStampOfLatestRec.bYear,
																					trmTimeStampOfLatestRec.bMonth,
																					trmTimeStampOfLatestRec.bDay,
																					trmTimeStampOfLatestRec.bHour,
																					trmTimeStampOfLatestRec.bMinutes,
																					trmTimeStampOfLatestRec.bSeconds,
																					bValidDate,
																					lDateDiffEnd);
								//dbgTrace(DBG_LVL_INFO, "TRM - generate_day_mp_xml:bValidDate: %d, lDateDiffEnd: %d ", bValidDate, lDateDiffEnd);

								if((bValidDate == FALSE) || (lDateDiffEnd > 0))
								{
									//set once
									if( (RAMTrmArea.trans_rec[idx].start_time.bCentury >= 20 ) &&
										(dateTimeProcessing.bCentury == 0 ) )
									{
										memcpy(&dateTimeProcessing, &RAMTrmArea.trans_rec[idx].start_time, sizeof(trmTimeStampOfLatestRec));
										if(trm_log_level)
										{
											dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml-dateTimeProcessing: %02d%02d-%02d-%02d %02d:%02d:%02d ",
																	dateTimeProcessing.bCentury,
																	dateTimeProcessing.bYear,
																	dateTimeProcessing.bMonth,
																	dateTimeProcessing.bDay,
																	dateTimeProcessing.bHour,
																	dateTimeProcessing.bMinutes,
																	dateTimeProcessing.bSeconds);
										}
									}

									//Clear NMR flag if mail pieces are processed
									if(*pNMR)
										*pNMR =  false;

									RAMTrmArea.trans_rec[idx].rec_state = TRM_PENDING_UPLOAD;

									//Add transaction record to the xml buffer
									status = generate_tr_xml(&RAMTrmArea.trans_rec[idx],
														pTmpMpXml + file_xml_len,
														lMpXmlBufLength - file_xml_len,
														&mp_xml_len,
														&trm_required_fields);
									if(status != SUCCESS)
									{
										dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml-Failed processing transaction record: %d, status:%d", idx, status);
										break;
									}

									//Update Currnet package info incase of debit required (indicia and succesfull cases only)
									if( (RAMTrmArea.trans_rec[idx].debit_required == true) &&
										(RAMTrmArea.trans_rec[idx].pc != 0)	)
									{
										//update current package registers
										trmUpdateDcapCurPackageInfo(false, RAMTrmArea.trans_rec[idx].mp_id, RAMTrmArea.trans_rec[idx].pc, &RAMTrmArea.trans_rec[idx].ar, &RAMTrmArea.trans_rec[idx].dr );
										memcpy(&cur_day.end_ar, &RAMTrmArea.trans_rec[idx].ar, sizeof(cur_day.end_ar));
										memcpy(&cur_day.end_dr, &RAMTrmArea.trans_rec[idx].dr , sizeof(cur_day.end_dr));
										memcpy(&cur_day.end_mp_id, &RAMTrmArea.trans_rec[idx].mp_id, sizeof(cur_day.end_mp_id));
										cur_day.end_pc = RAMTrmArea.trans_rec[idx].pc;

									}
									file_xml_len += mp_xml_len;

									if(trm_log_level)
									{
										dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml:mp_id=%d xml_len =%d, total:%d ", RAMTrmArea.trans_rec[idx].mp_id, mp_xml_len, file_xml_len);
									}

									//dbgTrace(DBG_LVL_INFO, tr_xml);
									RAMTrmArea.trans_rec[idx].rec_state = TRM_PENDING_UPLOAD_CONFIRMATION;
									//RAMTrmArea.header.num_records_in_use--;


								}
								else
								{
                                    // if the difference in date is not greater than zero, it means that the current date/period
                                    //   close date is not greater than the day of the currently processing record. 
									dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml- miss MP %d as lDateDiffEnd: %d!!! Record time: %02d%02d-%02d-%02d %02d:%02d:%02d, EndDate: %02d%02d-%02d-%02d %02d:%02d:%02d ",
											RAMTrmArea.trans_rec[idx].mp_id, lDateDiffEnd, 
											trmTimeStampOfLatestRec.bCentury,trmTimeStampOfLatestRec.bYear,trmTimeStampOfLatestRec.bMonth,
										    trmTimeStampOfLatestRec.bDay,trmTimeStampOfLatestRec.bHour,trmTimeStampOfLatestRec.bMinutes, trmTimeStampOfLatestRec.bSeconds,
											pEndDate->bCentury,pEndDate->bYear,pEndDate->bMonth,pEndDate->bDay,pEndDate->bHour,pEndDate->bMinutes,pEndDate->bSeconds);

									dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml: End time reached");
									//no need of processing this file any more,
									//rest of the pieces may belongs to next period
									bEndOfPeriodReached = true;
									break;
								}
							}
							else
							{
								if(trm_log_level)
								{
									dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml: Rec state: %d", RAMTrmArea.trans_rec[idx].rec_state);
								}
							}

						}//end for

						if(status != SUCCESS)
						{
							dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml-Failed processing file: %s, status:%d", trmFileList[count], status);
							break;
						}

						lMpXmlLength += file_xml_len;

						//No more records in file then add this file to processed file list
						//if records are still left then file might have records that belongs to next period

						//Update processed file list
						strncpy(CMOSTrmFileList.trm_file_list[CMOSTrmFileList.fileCount].trmFileName, trmFileList[count], MAX_FILE_NAME_SIZE);
						CMOSTrmFileList.trm_file_list[CMOSTrmFileList.fileCount].file_state = TRM_FILE_PENDING_UPLOAD;

						CMOSTrmFileList.fileCount++;
						dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml: Marked for Delete %s, count %d \n",
								CMOSTrmFileList.trm_file_list[CMOSTrmFileList.fileCount].trmFileName, CMOSTrmFileList.fileCount);

						////////////////////////////////////////////////////////////////////////////////
						//Read next file
						do{
							if( ( (count+1) <= (trxFileCount -1)) && (trmFileList[count+1] != NULL) )
							{

								dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml: Next File: %s, File Count: %d/%d ", trmFileList[count+1], count+2, trxFileCount);
								readCount = 0;
								do{
									if(readCount++ >2)
										break;

									if(strstr(trmFileList[count+1], "TRD-") == 0)
										status = trmReadTxFile("trs", trmFileList[count+1], (uint8_t*)&RAMTrmArea, &length, &txFileInfo);
									else
										status = trmReadTxFileEx("trs", trmFileList[count+1], (uint8_t*)&RAMTrmArea, &length, &txFileInfoEx);

									if(status != SUCCESS)
										dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml: trmReadTxFile failed status:%d, attempt:%d, File: %s ", status, readCount, trmFileList[count+1]);

								}while(status != SUCCESS);

								if(status == SUCCESS)
								{
									for(cnt =  0; cnt < CFG_NUM_RECORDS; cnt++)
									{

										//start with record after current mail piece
										idx = cnt;

										if (RAMTrmArea.trans_rec[idx].rec_state != TRM_UNUSED)
											//This is to process valid records only to prevent non erased record if it happens
										{

											//date time of current record
											memcpy(&trmTimeStampOfLatestRec, &RAMTrmArea.trans_rec[idx].start_time, sizeof(trmTimeStampOfLatestRec));
											if(trm_log_level)
											{
												dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml-Next File trmTimeStampOfLatestRec Record: %d: %02d%02d-%02d-%02d %02d:%02d:%02d \n",
																		idx,
																		trmTimeStampOfLatestRec.bCentury,
																		trmTimeStampOfLatestRec.bYear,
																		trmTimeStampOfLatestRec.bMonth,
																		trmTimeStampOfLatestRec.bDay,
																		trmTimeStampOfLatestRec.bHour,
																		trmTimeStampOfLatestRec.bMinutes,
																		trmTimeStampOfLatestRec.bSeconds);
											}
											break;
										}
									}


								}
								else
								{
									CMOSTrmFailedReads++;
									dbgTrace(DBG_LVL_INFO, "Failed to read Next File: %s status %d , CMOSTrmFailedReads:%d", trmFileList[count+1], status, CMOSTrmFailedReads);
									fnLogError( ERR_TYPE_FATAL, ERROR_CLASS_TRM, E_DATA_READ_FAILED );
//									if( gTrmFatalError == 0)
//										gTrmFatalError = E_DATA_READ_FAILED;
//
//									fTrmFatalError =  TRUE;
//									fnPostDisabledStatus(DCOND_TRM_FATAL_ERROR);
//									SendBaseEventToTablet(BASE_EVENT_TRM_FATAL_ERROR, NULL);

									fnFilePath(path, "trs", trmFileList[count+1]);
									status = NU_Delete(path); //purposefully ignored return value
									if(status != SUCCESS)
									{
										fnCheckFileSysCorruption(status);
										CMOSTrmFailedDeletes++;
										dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml 1 : Failure to delete CMOSTrmFailedReads:%d, CMOSTrmFailedDeletes:%d ", CMOSTrmFailedReads, CMOSTrmFailedDeletes);
									}
									count++;

								}


							}
							else
							{
								status = 1;
								dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml: No more files to process ");
								break;
							}

							//File read is failed goto next file and continue until all files are processed
						}while( (status != SUCCESS) && (count < trxFileCount) && (count < CMOSTrmMaxFilesToUpload) );
						////////////////////////////////////////////////////////////////////////////////

						bValidDate = CalcDateTimeDifference( &trmTimeStampOfLatestRec, pEndDate, &lDateDiffEnd );
						if(trm_log_level)
						{
							dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml: CurRec:%02d%02d-%02d-%02d %02d:%02d:%02d, DayProc:%02d%02d-%02d-%02d %02d:%02d:%02d, bValidDate:%d,lDateDiffEnd:%d ",
																			trmTimeStampOfLatestRec.bCentury,
																			trmTimeStampOfLatestRec.bYear,
																			trmTimeStampOfLatestRec.bMonth,
																			trmTimeStampOfLatestRec.bDay,
																			trmTimeStampOfLatestRec.bHour,
																			trmTimeStampOfLatestRec.bMinutes,
																			trmTimeStampOfLatestRec.bSeconds,
																			/////
																			dateTimeProcessing.bCentury,
																			dateTimeProcessing.bYear,
																			dateTimeProcessing.bMonth,
																			dateTimeProcessing.bDay,
																			dateTimeProcessing.bHour,
																			dateTimeProcessing.bMinutes,
																			dateTimeProcessing.bSeconds,
																			//
																			bValidDate,
																			//
																			lDateDiffEnd);
						}
						//Check for end of the day if day is changed
						//or all files are processed
						if( (status != SUCCESS) || //Close day if there are no files to process
							////Check for day change
							( (trmTimeStampOfLatestRec.bCentury != 0) &&
							  (memcmp(&trmTimeStampOfLatestRec, &dateTimeProcessing, offsetof(DATETIME, bDayOfWeek)) != 0)) ||
							//Check for cur day is greater than Period end date
							(lDateDiffEnd < 0) ||
							//Check all available files are processed, if processed close day
							(count+1 >= CMOSTrmMaxFilesToUpload) || (count+1 >= trxFileCount) )
						{
							//if date changed then set last to false.
							if(memcmp(&trmTimeStampOfLatestRec, &dateTimeProcessing, offsetof(DATETIME, bDayOfWeek)) != 0)
								last = TRUE;

							//this is to avoid empty date
							if(dateTimeProcessing.bDay == 0)
							{
								memcpy(&dateTimeProcessing, &trmTimeStampOfLatestRec, sizeof(DATETIME));

								//if the time stamp of first valid record in the first file is greater than cur day, 
								// then the record is ignored and the initial value of dateTimeProcessing is not updated.  
								dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml: dateTimeProcessing is empty. Last is set to TRUE\n");
							}

							if( pMpXml != NULL)
							{
								//Append Day
								pTmpDayXml = pDayMpXml + lDayMpXmlLength;
								status = generate_day_xml(pTmpDayXml,
															lDayMpXmlBufLength - lDayMpXmlLength,
															&day_xml_len,
															&dateTimeProcessing,
															&cur_day,
															last);


								memcpy(&dateTimeProcessing, &trmTimeStampOfLatestRec, sizeof(DATETIME));
								last = FALSE;

								day_tot_xml_len += day_xml_len;

								//copy mail piece xml
								memcpy(pTmpDayXml + day_tot_xml_len, pMpXml, lMpXmlLength);
								day_tot_xml_len += lMpXmlLength;

								//Append day end
								ptemp = pTmpDayXml + day_tot_xml_len;
								str_n_cat(ptemp, "</Day>", pDayXmlLimit - ptemp);
								day_tot_xml_len += strlen("</Day>");

								lDayMpXmlLength += day_tot_xml_len;
								dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml Day xml-Closed Day:day_xml_len:%d, lMpXmlLength:%d, day_tot_xml_len:%d, lDayMpXmlLength:%d \n",  day_xml_len, lMpXmlLength, day_tot_xml_len, lDayMpXmlLength);

								/////////////////////////////////
								//Reset day lengths for next day
								day_xml_len = 0;
								day_tot_xml_len = 0;
								pTmpDayXml = pDayMpXml + lDayMpXmlLength;

								//copy current day to prev day
								memcpy(&prev_day, &cur_day, sizeof(cur_day));

								//Copy Prev end counters to cur start counters
								memcpy(&cur_day.start_ar, &cur_day.end_ar, sizeof(cur_day.end_ar));
								memcpy(&cur_day.start_dr, &cur_day.end_dr, sizeof(cur_day.end_dr));
								memcpy(&cur_day.start_mp_id, &cur_day.end_mp_id, sizeof(cur_day.end_mp_id));
								cur_day.start_pc = cur_day.end_pc;

								lMpXmlLength = 0;

								//memcpy(&dateTimeProcessing, &trmTimeStampOfLatestRec, sizeof(DATETIME));
							}
							else
							{
								dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml-pMpXml is NULL: %s, count:%d \n", trmFileList[count], count);
							}
						}
						else
						{
							dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml Same Day \n");
						}

					}//end for

				}//end status = trmReadTxFile()
				else
				{
					CMOSTrmFailedReads++;

//					if(gTrmFatalError == 0)
//						gTrmFatalError = E_DATA_READ_FAILED;
//					fTrmFatalError =  TRUE;
//					fnPostDisabledStatus(DCOND_TRM_FATAL_ERROR);
//					SendBaseEventToTablet(BASE_EVENT_TRM_FATAL_ERROR, NULL);

					dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml : Failure to read First file status:%d, count:%d, File:%s, CMOSTrmFailedReads:%d \n", status, count+1, trmFileList[count], CMOSTrmFailedReads);
					 fnLogError( ERR_TYPE_FATAL, ERROR_CLASS_TRM, E_DATA_READ_FAILED );

					fnFilePath(path, "trs", trmFileList[count]);
					status = NU_Delete(path);
					if(status != SUCCESS)
					{
						fnCheckFileSysCorruption(status);
						CMOSTrmFailedDeletes++;
						dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml 2: Failure to delete CMOSTrmFailedReads:%d, CMOSTrmFailedDeletes:%d ", CMOSTrmFailedReads, CMOSTrmFailedDeletes);
					}
					//move to the next file
					count++;
					//break;
				}

			}while( (count < trxFileCount) && (count < CMOSTrmMaxFilesToUpload) && (bEndOfPeriodReached == false) );

		} //trmPrepareFileNamesList
		else
		{
			uint32_t pc;
			memcpy(&pc, pIPSD_pieceCount, sizeof(pc));

			dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml: trmPrepareFileNamesList  nothing to process \n");

			trmUpdateDcapCurPackageInfo(true, get_cur_mp_id(), pc, &m5IPSD_ascReg, &mStd_DescendingReg);
			trmUpdateDcapCurPackageInfo(false, get_cur_mp_id(), pc, &m5IPSD_ascReg, &mStd_DescendingReg);

			memcpy( &cur_day, &CMOSTrmPkgInfoCur, sizeof(CMOSTrmPkgInfoCur));

			//Append Day
			GetSysDateTime(&curRecDateTime, ADDOFFSETS, GREGORIAN);

			pTmpDayXml = pDayMpXml + lDayMpXmlLength;
			status = generate_day_xml(pTmpDayXml,
										lDayMpXmlBufLength - lDayMpXmlLength,
										&day_xml_len,
										&curRecDateTime,
										&cur_day,
										false);
			day_tot_xml_len += day_xml_len;
			dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml Day Xml: day_xml_len:%d,  day_tot_xml_len:%d \n", day_xml_len, day_tot_xml_len );

			//Append day end
			ptemp = pTmpDayXml + day_tot_xml_len;
			str_n_cat(ptemp, "</Day>", pDayXmlLimit - ptemp);
			day_tot_xml_len += strlen("</Day>");
			dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml Closed Day \n");

			lDayMpXmlLength += day_tot_xml_len;

		}

		//Clear File list
		trmReleaseFileList();

	}
	else
	{
		dbgTrace(DBG_LVL_INFO, "generate_day_mp_xml allocation failed:\n");
	}
	return status;
}


STATUS trmGenerateXMLFromFiles(DATETIME *pStartDate, DATETIME *pEndDate, BOOL *pNMR)
{
	STATUS status  = SUCCESS;

	int32_t xml_len = 0;
	int32_t tot_xml_len = 0;
    char *pLimit = NULL;
    char *ptemp;

    //Prepare Mailpiece xml
    //Set package start info from Prev Pckage
    trmUpdateDcapCurPackageInfo(true, CMOSTrmPkgInfoPrev.end_mp_id + 1, CMOSTrmPkgInfoPrev.end_pc, &CMOSTrmPkgInfoPrev.end_ar, &CMOSTrmPkgInfoPrev.end_dr);

   	status = generate_day_mp_xml(CMOSTrmMaxFilesToUpload, pStartDate, pEndDate, pNMR);
   	if(status != SUCCESS)
   	{
   		dbgTrace(DBG_LVL_INFO, "trmGenerateXMLFromFiles-generate_day_mp_xml failed: status:%d \n", status);
   		return 1;
   	}
   	else
   	{
   		dbgTrace(DBG_LVL_INFO, "trmGenerateXMLFromFiles-generate_day_mp_xml success: day_mp xml length %d \n", lDayMpXmlLength);
   	}

    if(status == SUCCESS)
    {
    	if(trmAllocateXmlBuffer() != NULL)
    	{
    		pLimit = pXml + lXmlBufLength;
			//Append Header
			xml_len = lXmlBufLength;
			status = generate_hdr_xml(pXml, lXmlBufLength, &xml_len);
			if(status != SUCCESS)
			{
				dbgTrace(DBG_LVL_INFO, "generate_hdr_xml failed: status:%d \n", status);
			}
			else
			{
				tot_xml_len += xml_len;
				dbgTrace(DBG_LVL_INFO, "generate_hdr_xml: xml_len:%d, tot_xml_len:%d \n", xml_len, tot_xml_len );
			}

			//copy Day mailpiece xml
			if( (pDayMpXml != NULL) && (lDayMpXmlLength != 0))
			{
				memcpy(pXml + tot_xml_len, pDayMpXml, lDayMpXmlLength);
				tot_xml_len += lDayMpXmlLength;
				dbgTrace(DBG_LVL_INFO, "Appended mail piece xml: lMpXmlLength:%d, tot_xml_len:%d \n", lDayMpXmlLength, tot_xml_len );
			}

			//Release Day and mail piece buffers
			trmReleaseDayMPBuffers();
    	}
    	else
    	{
    		dbgTrace(DBG_LVL_INFO, "trmAllocateXmlBuffer - allocation  failed \n");
    	}
    }

	//Add Pkg end
	ptemp = pXml + tot_xml_len;
	str_n_cat(ptemp, "</Pkg>", pLimit - ptemp );
	tot_xml_len += strlen("</Pkg>");

	dbgTrace(DBG_LVL_INFO, "TRM - GenerateXMLFromFiles finished ");

	lXmlLength = tot_xml_len;

	//trmGeneratetDcapFileName(bNMR);

	return status;
}


STATUS trmCompressXmlFromBuffer(char * uncompBuffer, uint32_t uncompLen, BOOL NMR)
{
	STATUS status = SUCCESS;
	char FileName[TRM_MAX_FILE_NAME_SIZE];
	char CompFileName[TRM_MAX_FILE_NAME_SIZE];// = "TRAN0001.zip";
	int len = sizeof(FileName);

	trmGetDcapFileName(FileName, &len, false);
	trmGetDcapFileName(CompFileName, &len, true);

	dbgTrace(DBG_LVL_INFO, "trmCompressXmlFromBuffer-fileName:%s, uncompLen:%d, compressed:%s, ", FileName, uncompLen, CompFileName );
	status = compressWithHeader ((const Bytef *)uncompBuffer, uncompLen, (Bytef *)FileName, strlen(FileName), (Bytef *)CompFileName );

	return status;
}

STATUS trmSaveXML(void)
{
	STATUS status = SUCCESS;
	int writelength = 0;
	int  fd;
	char fileName[TRM_MAX_FILE_NAME_SIZE]; // = "TRAN0001.xml";
	int len = sizeof(fileName);
	char path[64];

	fnFilePath(path, "upld", NULL);

	if(!fnFolderExists(path))
	{
		status = NU_Make_Dir(path);
		if(status != 0)
		{
			fnCheckFileSysCorruption(status);
			dbgTrace(DBG_LVL_ERROR, "trmSaveXML: Failed to create folder upld");
			return 1;
		}
	}

	trmGetDcapFileName(fileName, &len, false);

	fnFilePath(path, "upld", fileName);

	fd = NU_Open(path, (PO_TEXT|PO_RDWR|PO_CREAT|PO_TRUNC), (PS_IWRITE | PS_IREAD));
	if(fd < 0)
	{
		fnCheckFileSysCorruption(fd);
		dbgTrace(DBG_LVL_ERROR, "trmSaveXML: Failed to open xml file %s \r\n", fileName);
		status = fd;
	}
	else
	{
		if((pXml != NULL) && (lXmlLength != 0) )
		{
			writelength = NU_Write(fd, (void*)pXml, lXmlLength);
			if ((writelength < 0) || (writelength != lXmlLength))
			{
				fnCheckFileSysCorruption(writelength);
				dbgTrace(DBG_LVL_ERROR, "trmSaveXML: Failed to write XML file: %s, writelength: %0x / %0x \r\n", fileName, writelength, lXmlLength);
				status = writelength;
			}
			else
				dbgTrace(DBG_LVL_ERROR, "trmSaveXML: Succes XML file: %s, writelength: %0x / %0x \r\n", fileName, writelength, lXmlLength);
		}

		fnCheckFileSysCorruption(NU_Flush(fd));
		fnCheckFileSysCorruption(NU_Close(fd));
	}

	return status;
}


STATUS trmUpdateDcapUploadSuccess(void)
{
	STATUS status = SUCCESS;
	dbgTrace(DBG_LVL_ERROR, "trmUpdateDcapUploadSuccess: \r\n" );
	//CMOSTrmUploadState = TRM_STATE_UPLOAD_CONFIRMED;
	trmSetUploadState(TRM_STATE_UPDATE_PREV_PKG_INFO);

	MoveCurPkgInfoToPrevPkgInfo();

	trmSetUploadState(TRM_STATE_INCREMENT_COUNTERS);
	//If uload is succesful incremetn package seq count
	trmIncrementDcapFileSeqCount();

	trmSetUploadState(TRM_STATE_DELETE_FILES);
	//Removes transaction record files that are uploaded
	status = trmDeleteUploadedTxFiles();

	//Cleanup
	trmReleaseMemory();

	//bNMR = true;
	trmClearDcapFileName();
	trmSetUploadState(TRM_NONE);

	//Clear upload pending state
	if(status == SUCCESS)
	{

		dbgTrace(DBG_LVL_ERROR, "trmUpdateDcapUploadSuccess:  Success\r\n" );
	}
	else
	{
		dbgTrace(DBG_LVL_ERROR, "trmUpdateDcapUploadSuccess: Failed to cleanup\r\n" );
	}

	return status;
}


void trmResetForMidnight(void)
{
	STATUS status = SUCCESS;
	//Update Current package registers
	//trmInitializeDcapPackageInfo();

	//Move all TRs to Flash
	status = trmWriteTrmBucket(trmGetActiveBucket());
	if(status != SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "trmResetForMidnight-failed: %d ", status);
	}
	//Reset DCAP File Sequence count
	CMOSTrmDcapFileSeqCount = 1;

	//This sets mp id to 1
	reset_cur_mp_id();

}


//This function recovers Tx record upload incase of failure.

void trmSetState(ulong trmState)
{
	CMOSTRMState = trmState;
}

ulong trmGetState(void)
{
	return CMOSTRMState;
}

//
//TODO: Call this at powerup incase of powerfailure
STATUS trmProcessTxPendingWrite(void)
{
	if(trmGetState() == TRM_STATE_PENDING_TX_FILE_SAVE)
	{
		trmSetState(TRM_STATE_SAVING_TX_FILE);
		dbgTrace(DBG_LVL_ERROR, "trmProcessTxPendingWrite: performing pending write %d \r\n", CMOSTrmPendingBucketToUpload );
//		if(CMOSTrmActiveBucket == CFG_BUCKET0)
//			fnWriteTrxFile (CFG_BUCKET1, CFG_NUM_RECORDS, 0);
//		else if(CMOSTrmActiveBucket == CFG_BUCKET1)
//			fnWriteTrxFile (CFG_BUCKET0, CFG_NUM_RECORDS, 0);
		if(CMOSTrmPendingBucketToUpload != -1)
		{
			fnWriteTrxFile (CMOSTrmPendingBucketToUpload, CFG_NUM_RECORDS, 0);
		}
		else
			dbgTrace(DBG_LVL_ERROR, "trmProcessTxPendingWrite: Invalid Bucket %d \r\n", CMOSTrmPendingBucketToUpload );
	}

	return SUCCESS;
}

void trmSetUploadState(long state)
{
	CMOSTrmUploadState = state;
}

long trmGetUploadState(void)
{
	return CMOSTrmUploadState;
}

STATUS trmInitPSDInfo(void)
{
	strcpy((char*)CMOSTrmPSDInfo.psdPCN, "");
	strcpy((char*)CMOSTrmPSDInfo.indiciaSN, "");
	strcpy((char*)CMOSTrmPSDInfo.pbi_serial_number, "");
	strcpy((char*)CMOSTrmPSDInfo.orgCountry, "");
	strcpy((char*)CMOSTrmPSDInfo.sourceZipCode, "");
	dbgTrace(DBG_LVL_INFO, "trmInitPSDInfo: PSD Info initialized\r\n");
	return SUCCESS;
}

STATUS trmUpdatePSDInfo(BOOL bForce)
{
	STATUS status = SUCCESS;

	dbgTrace(DBG_LVL_INFO, "pIPSD_PCN:%s \r\n", pIPSD_PCN);
	dbgTrace(DBG_LVL_INFO, "pIPSD_indiciaSN:%s \r\n", pIPSD_indiciaSN);
	dbgTrace(DBG_LVL_INFO, "pIPSD_PBISerialNum:%s\r\n", pIPSD_PBISerialNum);
	dbgTrace(DBG_LVL_INFO, "pIPSD_originCountry:%s \r\n", pIPSD_originCountry);
	dbgTrace(DBG_LVL_INFO, "pIPSD_zipCode:%s \r\n", pIPSD_zipCode);

	if( (strcmp((char*)pIPSD_PCN, "") == 0) ||
			(strcmp((char*)pIPSD_indiciaSN, "") == 0) ||
			(strcmp((char*)pIPSD_PBISerialNum, "") == 0) ||
			(strcmp((char*)pIPSD_originCountry, "") == 0) ||
			(strcmp((char*)pIPSD_zipCode, "") == 0) )
	{

		status = 1; //invalid PSD info
		dbgTrace(DBG_LVL_INFO, "trmUpdatePSDInfo: PSD Info Null\r\n");
	}
	else
	{
		if ( (strncmp(CMOSTrmPSDInfo.pbi_serial_number, "", sizeof(pIPSD_PBISerialNum)) == 0) ||
				(bForce == TRUE))
		{
			memcpy(CMOSTrmPSDInfo.psdPCN, pIPSD_PCN, sizeof(pIPSD_PCN));
			memcpy(CMOSTrmPSDInfo.indiciaSN, pIPSD_indiciaSN, sizeof(pIPSD_indiciaSN));
			memcpy(CMOSTrmPSDInfo.pbi_serial_number, pIPSD_PBISerialNum, sizeof(pIPSD_PBISerialNum));
			memcpy(CMOSTrmPSDInfo.orgCountry, pIPSD_originCountry, sizeof(pIPSD_originCountry));
			memcpy(CMOSTrmPSDInfo.sourceZipCode, pIPSD_zipCode, sizeof(pIPSD_zipCode));
			dbgTrace(DBG_LVL_INFO, "trmUpdatePSDInfo: PSD Info initialized\r\n");
		}
		else if(strncmp(CMOSTrmPSDInfo.pbi_serial_number, (char*)pIPSD_PBISerialNum, sizeof(pIPSD_PBISerialNum)) != 0)
		{
			//there is difference in PBI Serial number due to PSD change
			if(CMOSTrmUploadState != TRM_NONE)
			{
				//TODO: trigger force upload if necessary
				dbgTrace(DBG_LVL_INFO, "trmUpdatePSDInfo: Upload required\r\n");
				status = 0xFF;
				fnPostDisabledStatus(DCOND_DCAP_AUTO_UPLOAD_REQ);
			}
			else
			{
				//Nothing to upload go ahead and update PSD info in case of PSD change
				memcpy(CMOSTrmPSDInfo.psdPCN, pIPSD_PCN, sizeof(pIPSD_PCN));
				memcpy(CMOSTrmPSDInfo.indiciaSN, pIPSD_indiciaSN, sizeof(pIPSD_indiciaSN));
				memcpy(CMOSTrmPSDInfo.pbi_serial_number, pIPSD_PBISerialNum, sizeof(pIPSD_PBISerialNum));
				memcpy(CMOSTrmPSDInfo.orgCountry, pIPSD_originCountry, sizeof(pIPSD_originCountry));
				memcpy(CMOSTrmPSDInfo.sourceZipCode, pIPSD_zipCode, sizeof(pIPSD_zipCode));
				dbgTrace(DBG_LVL_INFO, "trmUpdatePSDInfo: PSD Change detected and updating Info initialized\r\n");
			}
		}
	}

	return status;
}

/* *************************************************************************
// FUNCTION NAME:
//		fnWriteTrxFile
//
// DESCRIPTION:
//  	Funtion to write trx files
//
// INPUTS:
//
//
// OUTPUTS:
//		None
//
// RETURNS:
//		None
//
// WARNINGS/NOTES:
// 		None
//
//
// *************************************************************************/
BOOL fnWriteTrxFile (UINT8 bucket, INT32 value, INT32 value2)
{
	BOOL msgStat;
	UINT8 	ucServiceMsg[MAX_INTERTASK_BYTES] = {0};

	ucServiceMsg[0] = bucket;
	memcpy(&ucServiceMsg[1], &value, sizeof(INT32));
	memcpy(&ucServiceMsg[1+sizeof(INT32)], &value2, sizeof(INT32));

	msgStat = OSSendIntertask( BKGRND, CM, (unsigned char)WRITE_TRX_FILES,
							BYTE_DATA, ucServiceMsg, MAX_INTERTASK_BYTES );
	return msgStat;
}

BOOL fnUploadTrxPkgFile (UINT8 period, INT32 bucket, INT32 value2)
{
	BOOL msgStat;
	UINT8 	ucServiceMsg[MAX_INTERTASK_BYTES] = {0};

	fDCAPUploadDueShown = TRUE;

	ucServiceMsg[0] = period;
	memcpy(&ucServiceMsg[1], &bucket, sizeof(INT32));
	memcpy(&ucServiceMsg[1+sizeof(INT32)], &value2, sizeof(INT32));

	msgStat = OSSendIntertask( BKGRND, OIT, (unsigned char)PREPARE_TRX_PKG_FILE,
							BYTE_DATA, ucServiceMsg, MAX_INTERTASK_BYTES );
	return msgStat;
}

BOOL trmSendMsgToBackground (unsigned char bMsgID, INT32 value1, INT32 value2, INT32 value3)
{
	BOOL msgStat;
	UINT8 	ucServiceMsg[MAX_INTERTASK_BYTES] = {0};

	ucServiceMsg[0] = value1;
	memcpy(&ucServiceMsg[1], &value2, sizeof(INT32));
	memcpy(&ucServiceMsg[1+sizeof(INT32)], &value3, sizeof(INT32));

	msgStat = OSSendIntertask( BKGRND, CM, (unsigned char)bMsgID,
							BYTE_DATA, ucServiceMsg, MAX_INTERTASK_BYTES );
	return msgStat;
}

//TODO: Statemachine / Powerfail recovery
STATUS trmProcessTRUpload(DATETIME *pStartDate, DATETIME *pEndDate)
{
	STATUS status = SUCCESS;
	enum_trm_sates trmState;
	BOOL bNMR = false;
	BOOL done = FALSE;
	DATETIME curDate;
	//int len = 0;

	while(done == FALSE)
	{
		trmState = trmGetUploadState();
		switch(trmState)
		{
		case TRM_NONE:
		case TRM_STATE_UPLOAD_REQUIRED:
			//dbgTrace(DBG_LVL_INFO, "trmProcessTRUpload - TRM_NONE ");
			//trmSetUploadState(TRM_STATE_SAVE_TX_FILES);
			//trmSetUploadState(TRM_STATE_CREATE_XML);
		case TRM_STATE_CREATE_XML:
			dbgTrace(DBG_LVL_INFO, "trmProcessTRUpload - TRM_STATE_CREATE_XML ");
			sendUploadProgress(UPLOAD_EVENT_TRX_PREPARING_PKG, 0, 0);
			//Create XML, Save XML if necessary and compress
			status = trmGenerateXMLFromFiles(pStartDate, pEndDate, &bNMR);
			if(status != SUCCESS)
			{
				dbgTrace(DBG_LVL_ERROR, "trmProcessTRUpload: GenerateXML failed status:%d\r\n", status );
				done = TRUE;
			}
			else
			{
				dbgTrace(DBG_LVL_ERROR, "trmProcessTRUpload: GenerateXML Success \r\n");
				//Changed to use current date to create XML package name.
				//Using End date/Period End date causing conflict or duplicate file names
				//HORCSD-4848
				GetSysDateTime(&curDate, ADDOFFSETS, GREGORIAN);
				//trmGeneratetDcapFileName(&curDate, bNMR);
				trmGeneratetDcapFileName(&CMOSTrmTimeStampOfMPIDReset, bNMR);

				dbgTrace(DBG_LVL_ERROR, "trmProcessTRUpload: Saving Xml \r\n" );

				//Create XML file on request
				if(trm_log_level)
				{
					status = trmSaveXML();
					if(status != SUCCESS)
						dbgTrace(DBG_LVL_ERROR, "trmProcessTRUpload: trmSaveXML failed status:%d\r\n", status );
					else
						dbgTrace(DBG_LVL_ERROR, "trmProcessTRUpload: trmSaveXML Success \r\n");
				}
				trmSetUploadState(TRM_STATE_XML_COMPRESSED);
			}

			break;
		case TRM_STATE_XML_COMPRESSED:
			dbgTrace(DBG_LVL_INFO, "trmProcessTRUpload - TRM_STATE_XML_COMPRESSED: %d ", lXmlLength);
			sendUploadProgress(UPLOAD_EVENT_TRX_COMPRESSING_PKG, 0, 0);
			status = trmCompressXmlFromBuffer(pXml, lXmlLength, bNMR);
			if(status != SUCCESS)
			{
				dbgTrace(DBG_LVL_ERROR, "trmProcessTRUpload: Compression failed status:%d\r\n", status );
				done = TRUE;
			}
			else
			{
				dbgTrace(DBG_LVL_INFO, "trmProcessTRUpload: Compression Success \r\n");

				trmReleaseMemory();
				trmSetUploadState(TRM_STATE_UPLOAD_PENDING);

				//Once we reach this point lock the package
				CMOSTrmDcapFileSeqCount++;
				CMOS2TrmDcapFileToBeUploaded = TRUE;
				CMOSTrmTRCFilesToBeRemoved = TRUE;
				CMOSTrmMailPiecesPackaged = CMOSTrmMailPiecesToUpload;
				CMOSTrmMailPiecesToUpload = 0;
				//XMP package is created, Remove all TRC files.
				status = trmCheckForUploadCleanup();

			}
			return SUCCESS;
			break;

		default:
			dbgTrace(DBG_LVL_INFO, "trmProcessTRUpload - default: we should never see this %d", CMOSTrmUploadState);
			done = TRUE;
			break;
		}

	}

	if(status != SUCCESS)
	{
		trmUploadFailed(status);
	}

	return status;
}


STATUS trmProcessTRUploadConfirmation(void)
{
	STATUS status = SUCCESS;
	enum_trm_sates trmState;
	BOOL done  = FALSE;
	int fileCount = 0;

	//clear DCAP memory
	CleanDCAPBuffer();

	while(done == FALSE)
	{
		trmState = CMOSTrmUploadState;
		switch(trmState)
		{
		//case TRM_STATE_WAITING_FOR_UPLOAD_CONFIRMATION:
		case TRM_STATE_UPLOAD_CONFIRMED:
		//case TRM_STATE_UPDATE_PREV_PKG_INFO:
			dbgTrace(DBG_LVL_INFO, "trmProcessTRUploadConfirmation - TRM_STATE_UPLOAD_CONFIRMED: %d ", trmState);
			sendUploadProgress(UPLOAD_EVENT_TRX_CLEANUP_AFTER_UPLOAD, 0, 0);

			//Set Preupload Date
			memcpy(&CMOSTrmPrevUploadDate, &trmPkgEndDateTime, sizeof(DATETIME));

			//TODO
			MoveCurPkgInfoToPrevPkgInfo();

			 CMOSTrmUploadState = TRM_STATE_DELETE_FILES;
			break;
		case TRM_STATE_DELETE_FILES:
			dbgTrace(DBG_LVL_INFO, "trmProcessTRUploadConfirmation - TRM_STATE_DELETE_FILES ");
			//Clean Transaction record files
			status = trmDeleteUploadedTxFiles();
			if(status == SUCCESS)
				dbgTrace(DBG_LVL_INFO, "trmProcessTRUploadConfirmation - Deleted uploaded trx files ");
			else
				dbgTrace(DBG_LVL_INFO, "trmProcessTRUploadConfirmation - Deleting uploaded trx files failed:%d ", status);

			//Clean older xml and zip files
			status = trmDeleteUploadedDcapFiles();
			if(status == SUCCESS)
				dbgTrace(DBG_LVL_INFO, "trmProcessTRUploadConfirmation - Deleted Dcap files ");
			else
				dbgTrace(DBG_LVL_INFO, "trmProcessTRUploadConfirmation - Deleting Dcacp files failed :%d", status);

			 CMOSTrmUploadState = TRM_STATE_INCREMENT_COUNTERS;
			break;
		case TRM_STATE_INCREMENT_COUNTERS:
			dbgTrace(DBG_LVL_INFO, "trmProcessTRUploadConfirmation - TRM_STATE_INCREMENT_COUNTERS ");
			//trmIncrementDcapFileSeqCount();
			 CMOSTrmUploadState = TRM_STATE_CLEANUP;

			break;
		case TRM_STATE_CLEANUP:
			dbgTrace(DBG_LVL_INFO, "trmProcessTRUploadConfirmation - TRM_STATE_CLEANUP ");
			sendUploadProgress(UPLOAD_EVENT_TRX_UPLOAD_SUCCESS, 0, 0);
			(void) OSStopTimer(TRM_UPLOAD_TIMEOUT_TIMER);

			CMOSTrmMailPiecesPackaged = 0;
			trmClearDcapFileName();

			//Clear Mailer ID after successful upload
			memset( CMOSTrmCommon.mailer_id, 0, sizeof(CMOSTrmCommon.mailer_id));
			CMOSTrmCommon.flags.is_populated.mailer_id = false;

			 CMOSTrmUploadState = TRM_NONE;
			done = TRUE;
			break;

		case TRM_NONE:
		default:
			dbgTrace(DBG_LVL_INFO, "trmProcessTRUploadConfirmation - TRM_NONE: Nothing to cleanup %d", CMOSTrmUploadState);
			done = TRUE;
			break;


		}

	}

	trmReleaseMemory();
	//clear upload required / due flags
	fUploadInProgress = FALSE;

	//Clear all the necessary flags if all the files are processed
	status = trmGetTrxFileCount(&fileCount);
	if(fileCount == 0)
	{
		fDCAPUploadReqShown = FALSE;
		fDCAPUploadDueShown = FALSE;

		CMOSTrmUploadDueReqd = TRM_UPLOAD_NONE;
		//clear oldest mp date time
		memset(&CMOSTrmTimeStampOfOldestMP, 0, sizeof(CMOSTrmTimeStampOfOldestMP));
		dbgTrace(DBG_LVL_INFO, "trmProcessTRUploadConfirmation: No more to upload");
	}
	else
	{
		//if there are some more files to process set the latest record time as oldest mp
		if(trmTimeStampOfLatestRec.bDay != 0)
			memcpy(&CMOSTrmTimeStampOfOldestMP, &trmTimeStampOfLatestRec, sizeof(CMOSTrmTimeStampOfOldestMP));

		dbgTrace(DBG_LVL_INFO, "trmProcessTRUploadConfirmation: Upload Required File count:%d", fileCount);
	}

	SendBaseEventToTablet(BASE_EVENT_PRINTER_STATUS_UPDATED, NULL);

	return status;
}


//int  ReCreateTRecForPwrFail(const uint32_t start_transID, uint32_t *failed_transID, int *status)
//{
//
//	uint16_t uStart_tran_ID = start_transID;
////	uint16_t uTempStart_tranID = start_transID;
//	//unsigned char jobID = atoi(p_trm_mem->header.job_id);
//	//sPsd_reg_log_t psdRegLog;
//	bool bFoundStartID = false;
//	uint32_t currTransID = 0;
//	unsigned long TransID = start_transID;
//	ShadowDebitLogEntry sDebitLogEntry;
//	uint16_t LastTrasID;
//
//	int i = 0;
//	int Index = 0;
//
//
//	int uPsdID = 0;
//
//	uPsdID = p_trm_mem->header.psd_id;
//	status = RestorePSDLog(uPsdID);
//	if(status != SUCCESS)
//	{
//		failed_transID = start_transID;
//		dbgTrace(DBG_LVL_INFO,
//					            "TRM - ReCreateTRecForPwrFail: CPSDRegisterLog()Failed Transaction ID = %d and GetLog status=%d", failed_transID, status);
//		return status;
//	}
//
//	//Get the Last Transaction ID
//	//GetLastLogEntry(uPsdID, psdRegLog);
//	fnGetLastLogEntry(&sDebitLogEntry);
//
//	//copy the last transaction ID
//	LastTrasID = psdRegLog.sRecoveryData.transaction_id;
//
//	//check if the start transaaction ID is the last Transaction ID
//	if (uStart_tran_ID == LastTrasID)
//	{
//		//check for JobID
//		if(jobID != psdRegLog.sRecoveryData.jobid)
//		{
//			status = E_INVALID_JOBID;
//			failed_transID = start_transID;
//			dbgTrace(DBG_LVL_INFO,
//						            "TRM - ReCreateTRecForPwrFail: failed on JOBID= %d and psdRegLog.sRecoveryData.jobid =%d"
//					,jobID
//					,psdRegLog.sRecoveryData.jobid);
//			return status;
//
//		}
//
//		//only one record to upload do the upload and return;
//		//Build the Recovery Transaction record
//		p_trm_mem->header.next_transaction_id = start_transID;
//		status = BuildRecoveryTRec((mp_id_t)0, psdRegLog, currTransID);
//		if(status == SUCCESS)
//		{
//			GenerateXMLAndUpload(0);
//			status = SUCCESS;
//		}
//		else
//		{
//			status = E_INVALID_TREC_ID;
//			failed_transID = start_transID;
//			dbgTrace(DBG_LVL_INFO,
//						            "TRM - ReCreateTRecForPwrFail: BuildRecoveryTRec() Start Trans ID is  Last Trans ID : failed_transID = %d", failed_transID);
//		}
//		return status;
//	}
//
//	while(bFoundStartID ==false && uStart_tran_ID != LastTrasID)
//	{
//
//		memset(&sDebitLogEntry, 0x0, sizeof(psdRegLog));
//		if(fnGetLogEntry(uStart_tran_ID, &sDebitLogEntry))
//		{
//			bFoundStartID = true;
//			break;
//		}
//		else
//		{
//			uStart_tran_ID++;
//			TransID++;
//			dbgTrace(DBG_LVL_INFO,
//									"TRM - ReCreateTRecForPwrFail: uStart_tran_ID= %d", uStart_tran_ID);
//		}
//
//	}
//	if(bFoundStartID)
//	{
//		if (uStart_tran_ID == LastTrasID)
//		{
//			//only one record to upload do the upload and return;
//			//Build the Recovery Transaction record
//			p_trm_mem->header.next_transaction_id = TransID;
//			status = BuildRecoveryTRec((mp_id_t)0, psdRegLog, currTransID);
//			if(status == SUCCESS)
//			{
//				GenerateXMLAndUpload(0);
//				status = SUCCESS;
//			}
//			else
//			{
//				status = E_INVALID_TREC_ID;
//				failed_transID = TransID;
//				dbgTrace(DBG_LVL_INFO,
//							            "TRM - ReCreateTRecForPwrFail: BuildRecoveryTRec() failed_transID = %d and status =%d", failed_transID, status);
//			}
//			return status;
//
//		}
//
//		do{
//			dbgTrace(DBG_LVL_INFO,
//						            "TRM - ReCreateTRecForPwrFail: while loop before Build Recovery and Upload uStart_tran_ID= %d LastTrasID =%d", uStart_tran_ID, LastTrasID);
//
//			if(fnGetLogEntry(&Index, &sDebitLogEntry))
//			{
//				//Build the Recovery Transaction record
//				p_trm_mem->header.next_transaction_id = TransID;
//				status = BuildRecoveryTRec((mp_id_t)i, psdRegLog, currTransID);
//				if(status == SUCCESS)
//				{
//					GenerateXMLAndUpload(i);
//					i++;-
//					Index++;
//				}
//				else
//				{
//					status = E_TRANS_REC_NOT_CREATED;
//					failed_transID = currTransID;
//					dbgTrace(DBG_LVL_INFO,
//								            "TRM - ReCreateTRecForPwrFail: BuildRecoveryTRec() failed: failed_transID = %d and status =%d", failed_transID, status);
//					break;
//				}
//				//do the upload
//			}
//			else
//			{
//				dbgTrace(DBG_LVL_INFO,
//							            "TRM - PSD DOES NOT HAVE AN ENTRY WITH TRANSACTION ID = %ld and Job ID = %d", TransID, jobID);
//
//			}
//			TransID++;
//
//		}while(uStart_tran_ID++ != LastTrasID);
//	}
//	else
//	{
//		status = E_INVALID_TREC_ID;
//		failed_transID = start_transID;
//		dbgTrace(DBG_LVL_INFO,
//					            "TRM - ReCreateTRecForPwrFail: Failed Transaction ID = %d  jobID=%d", failed_transID, jobID);
//
//	}
//
//	return status;
//}

#ifdef TEST_TRM

int TestTrm(int numRecords)
{
	error_t error_code;
	static char bInit = false;
	char pbi_sn[10] = "12345678";
	static uint8_t ar[5];
	static uint8_t dr[5];
	static uint32 pc;

	uint8_t debit_cert[CFG_MAX_DEBIT_CERT_SIZE];
	char graph[] = "SAMPLE.GRA";
	int cnt;
	char AcntName[] = "Maine Education CU";
	char Oper[] = "super";
	char MailClass[] = "FC";
	DATETIME	dateTime;
	uint64_t dRgValue;
	mp_id_t mp_id;

	if(!bInit){
		memcpy(ar, CMOSTrmPkgInfoPrev.end_ar, sizeof(CMOSTrmPkgInfoPrev.end_ar));
		memcpy(dr, CMOSTrmPkgInfoPrev.end_dr, sizeof(CMOSTrmPkgInfoPrev.end_dr));
		pc = CMOSTrmPkgInfoPrev.end_pc;
		bInit = true;
	}

	dRgValue = LE_byte_array_to_uint64_t(ar, sizeof(ar));
	dRgValue = dRgValue + 1;
	memcpy(ar, &dRgValue, sizeof(ar));
	dRgValue = LE_byte_array_to_uint64_t(dr, sizeof(dr));
	dRgValue = dRgValue - 1;
	memcpy(dr, &dRgValue, sizeof(dr));
	pc++;



	//Create TR
	for(cnt = 0; cnt < numRecords; cnt++)
	{

		//error_code = trmCreateTR(get_new_mp_id());
		CreateXR();
		mp_id = get_cur_mp_id();

		//mode
//		error_code = trm_set_mode(mp_id, MODE_EXTERNAL_SCALE);
//		if(error_code)
//			while(1);

		//weight
//		error_code = trm_set_weight(mp_id , 0x1234);
//		if(error_code)
//			while(1);


//		error_code = trmSetPrintedGraphics(mp_id, graph, sizeof(graph));
//		if(error_code)
//			while(1);

//		error_code = trmSetAccount(mp_id, 65, AcntName, 1);
//		if(error_code)
//			while(1);

//		error_code = trmSetOper(mp_id, Oper);
//		if(error_code)
//			while(1);

//		error_code = trmSetClass(mp_id, "First Class", "ABCD");
//		if(error_code)
//			while(1);

//		error_code = trmSetCat(mp_id, 4);
//		if(error_code)
//			while(1);

//		error_code = trmSetCarrier(mp_id, "USPS");
//		if(error_code)
//			while(1);

//		error_code = trmSetPostage(mp_id, 100);
//		if(error_code)
//			while(1);


		error_code = trmSetPSDRegisters(mp_id
									,mp_id//pc
									,0		//zpc
									,ar
									,dr
									,3);
		if(error_code)
			while(1);

		memset(debit_cert, mp_id, sizeof(debit_cert));
		error_code = trmDebitCompleted(mp_id, debit_cert, sizeof(debit_cert));
		if(error_code)
			while(1);

		//Debit time
		GetSysDateTime(&dateTime, ADDOFFSETS, GREGORIAN);
//		trm_set_debitTime(mp_id, &dateTime);
//		if(error_code)
//			while(1);

		error_code = trmStartPrint(mp_id);
		if(error_code)
			while(1);

		//Update TR print complete
		error_code = trmEndPrint(mp_id);
		if(error_code)
			while(1);

		NU_Sleep(100);
	}

	//Update TR Complete

	//Create Flash File

	//Recover TR file

	//Remove TRs from FRAM

	//Upload

	//Confirm Upload

	//Delete Flash File

	//Generate XML
	//GenerateXMLAndUpload();

	return 0;
}

//int AddTR(void)
//{
//	error_t error_code;
//	char pbi_sn[10] = "12345678";
//	uint8_t ar[5] = {0x11, 0x22, 0x33, 0x44, 0x55};
//	uint8_t dr[5] = {0x11, 0x22, 0x33, 0x44, 0x55};
//	uint8_t debit_cert[CFG_MAX_DEBIT_CERT_SIZE];
//	char graph[] = "SAMPLE.GRA";
//
//	ulong mp_id ;
//	//Init
//	//STATUS status = InitTrm();
//
//	//Create TR
//	//error_code = trmCreateTR(get_new_mp_id());
//	CreateXR();
//
//	mp_id = get_cur_mp_id();
//
//	//Update TR
//
//	//Weight
//	//mode
//	error_code = trm_set_mode(mp_id, "Indicia");
//	if(error_code)
//		while(1);
//
//	//weight
//	error_code = trm_set_weight(mp_id, 0x1234);
//	if(error_code)
//		while(1);
//
//
//	error_code = trmSetPrintedGraphics(mp_id, graph, sizeof(graph));
//	if(error_code)
//		while(1);
//
//
//	error_code = trmPutPSDRegisters(mp_id, CMOSTrmPkgInfoCur.
//									,mp_id 	//pc
//									,0		//zpc
//									,ar
//									,dr
//									,3);
//	if(error_code)
//		while(1);
//
//	//Update TR Debit record
//	memset(debit_cert, mp_id, sizeof(debit_cert));
//	error_code = trmDebitCompleted(mp_id, debit_cert, sizeof(debit_cert));
//	if(error_code)
//		while(1);
//
//	//Update TR print started
//	error_code = trmStartPrint(mp_id);
//	if(error_code)
//		while(1);
//
//	//Update TR print complete
//	error_code = trmEndPrint(mp_id);
//	if(error_code)
//		while(1);
//
//
//	error_code = trmMPExit(mp_id, false);
//	if(error_code)
//		while(1);
//
//	//Update TR Complete
//
//	//Create Flash File
//
//	//Recover TR file
//
//	//Remove TRs from FRAM
//
//	//Upload
//
//	//Confirm Upload
//
//	//Delete Flash File
//
//	//Generate XML
//	//GenerateXMLAndUpload();
//
//}

#endif


/* **********************************************************************
// FUNCTION NAME: trmUploadTimerExpired
// DESCRIPTION:  Timer expiration routine
//
//
// AUTHOR: Sathish Kalidindi
//
// INPUTS:  none used
// **********************************************************************/
void trmUploadTimerExpired(unsigned long lwArg)
{
//	static enum_trm_sates trmState = 0;

//	if(trmState == 0)
//		trmSetUploadState(TRM_NONE);
//	//Check for uploads if it is not already in process
//	dbgTrace(DBG_LVL_ERROR, "trmUploadTimerExpired - uploadState: %d ", trmGetUploadState());
//
//	trmState = trmGetUploadState();
//	if(TRM_NONE == trmState)
//	{
//		trmSetUploadState(TRM_STATE_UPLOAD_REQUIRED);
//		trmSendMsgToBackground (CHECK_FOR_UPLOAD_ACTIVITY, 0, 0, 0);
//	}

//	trmSendMsgToBackground (CHECK_FOR_UPLOAD_DUE_REQUIRED, 0, 0, 0);

    return;
}


/* **********************************************************************
// DESCRIPTION:  Upload timeout expiration routine
//
// **********************************************************************/
void trmUploadTimeOut(unsigned long lwArg)
{
	// If timer expired, upload never completed so cancel upload and notify tablet
	BOOL msgStat;

	msgStat = trmSendMsgToBackground (UPLOAD_TIMEOUT, 0, 0, 0);
	if (msgStat == FAILURE)
	{
		dbgTrace(DBG_LVL_ERROR,"Error: Internal error sending upload timeout message to background manager");
	 }
}

/* **********************************************************************
// DESCRIPTION:  Upload failure cleanup
//
// **********************************************************************/
void trmCleanupUpload(upoad_event_t upEvent)
{
	fUploadInProgress = FALSE;
	sendUploadProgress(upEvent, 0, 0);
	SendBaseEventToTablet(BASE_EVENT_TX_UPLOAD_FAILED, NULL);
}

/* **********************************************************************
// DESCRIPTION:  Initiates upload of package
//
// **********************************************************************/
void trmInitiatePackageUpload(void)
{
	api_status_t  distStat;
	BOOL msgStat;
    BOOL osStat;
    //int len = 0;

	dbgTrace(DBG_LVL_INFO, "trmInitiatePackageUpload - TRM_STATE_UPLOAD_PENDING ");
	sendUploadProgress(UPLOAD_EVENT_TRX_UPLOAD_PROGRESS, 0, 0);
	//Delay
	NU_Sleep(100);

	if(trmGetUploadState() != TRM_STATE_UPLOAD_PENDING)
	{
		return trmCleanupUpload(UPLOAD_EVENT_TRX_UPLOAD_FAILED);
	}

	if (!connectedToInternet())
	{
		return trmCleanupUpload(UPLOAD_EVENT_TRX_NO_CONNECTIVITY);
	}

    distStat = lockDistributor("UploadTrxPkg");
    if (distStat != API_STATUS_SUCCESS)
    {
		return trmCleanupUpload(UPLOAD_EVENT_TRX_DISTRIBUTOR_BUSY);
    }

	dbgTrace(DBG_LVL_INFO, "trmInitiatePackageUpload: fnGetUpdateNow ");
	msgStat = fnGetUpdateNow(DCAP_UPLOAD_SERVICE, 0, 0);
	if (msgStat == FALSE)
	{// release locks and return
    	(void) releaseDistributor(TRUE, "UploadTrxPkgReq");
		return trmCleanupUpload(UPLOAD_EVENT_TRX_UPLOAD_FAILED);
	}

	trmSetUploadState(TRM_STATE_WAITING_FOR_UPLOAD_CONFIRMATION);
	osStat = OSStartTimer(TRM_UPLOAD_TIMEOUT_TIMER); // If this expires, upload will be cancelled
	if (osStat == (BOOL) FAILURE)
	{
		dbgTrace(DBG_LVL_ERROR, "UPLOAD_TRX_PKG_FILE Error: Cannot start upload timer");
	}



}

void trmUploadFailed(STATUS status)
{
	if(fUploadInProgress == TRUE)
	{
		char strTmp[20];
		cJSON *parameters = cJSON_CreateObject();

		fUploadInProgress = FALSE;

		trmSetUploadState(TRM_NONE);
		sendUploadProgress(UPLOAD_EVENT_TRX_UPLOAD_FAILED, 0, 0);

		// RD SendBaseEvent expects a string let's not disappoint...
		snprintf(strTmp, 20, "%d", (INT)status) ;
		cJSON_AddStringToObject(parameters, "Error", strTmp);

		(void) OSStopTimer(TRM_UPLOAD_TIMEOUT_TIMER);
		SendBaseEventToTablet(BASE_EVENT_TX_UPLOAD_FAILED, parameters);
	}


	trmReleaseMemory();
	//trmClearDcapFileName();
}


STATUS trmPrepareXMLPackageFile(int periodIdx)
{
	//this prepares XML/compresses and saves to flash file system
	STATUS status = SUCCESS;
	DATETIME startDateTime;
	DATETIME dateTimeStamp;
	BOOL bValidDate = false;
	long lDateDiff;

	//trmSetUploadState(TRM_STATE_UPLOAD_REQUIRED);
	dbgTrace(DBG_LVL_INFO, "trmPrepareXMLPackageFile : Upload Period: %d ", periodIdx);

	GetSysDateTime(&dateTimeStamp, ADDOFFSETS, GREGORIAN);

	//handle one period at a time
	if(periodIdx == 0)
	{
		memset(&startDateTime, 0, sizeof(startDateTime));
		memcpy(&trmPkgEndDateTime, &(pParameters->Period.periodList[0]), sizeof(trmPkgEndDateTime));
	}
	else if(periodIdx == 1)
	{
		memcpy(&startDateTime, &(pParameters->Period.periodList[0]), sizeof(startDateTime));
		memcpy(&trmPkgEndDateTime, &(pParameters->Period.periodList[1]), sizeof(trmPkgEndDateTime));
	}
	else if(periodIdx == 2)
	{
		memcpy(&startDateTime, &(pParameters->Period.periodList[1]), sizeof(startDateTime));
		memcpy(&trmPkgEndDateTime,&(pParameters->Period.periodList[2]), sizeof(trmPkgEndDateTime));
	}
	else if(periodIdx == 3)
	{
		memcpy(&startDateTime, &(pParameters->Period.periodList[2]), sizeof(startDateTime));
		memcpy(&trmPkgEndDateTime, &(pParameters->Period.periodList[3]), sizeof(trmPkgEndDateTime));
	}
	else
	{
		memset(&startDateTime, 0, sizeof(startDateTime));
		GetSysDateTime(&trmPkgEndDateTime, ADDOFFSETS, GREGORIAN);
	}

	bValidDate = CalcDateTimeDifference( &dateTimeStamp, &trmPkgEndDateTime, &lDateDiff );

	if( (bValidDate == false) || (lDateDiff > 0)  )
	{
		memcpy(&trmPkgEndDateTime, &dateTimeStamp, sizeof(dateTimeStamp));

		//Adjust the time to end of the day
		trmPkgEndDateTime.bHour = 23;
		trmPkgEndDateTime.bMinutes = 59;
		trmPkgEndDateTime.bSeconds = 59;
	}

	dbgTrace(DBG_LVL_INFO, "trmPrepareXMLPackageFile - Upload Period:%d, Start: %02d%02d-%02d-%02d %02d:%02d:%02d, End: %02d%02d-%02d-%02d %02d:%02d:%02d",
							periodIdx,
							startDateTime.bCentury,
							startDateTime.bYear,
							startDateTime.bMonth,
							startDateTime.bDay,
							startDateTime.bHour,
							startDateTime.bMinutes,
							startDateTime.bSeconds,
							//////
							trmPkgEndDateTime.bCentury,
							trmPkgEndDateTime.bYear,
							trmPkgEndDateTime.bMonth,
							trmPkgEndDateTime.bDay,
							trmPkgEndDateTime.bHour,
							trmPkgEndDateTime.bMinutes,
							trmPkgEndDateTime.bSeconds);

	fUploadInProgress = TRUE;
	status = trmProcessTRUpload(&startDateTime, &trmPkgEndDateTime);
	//status = 1; //To Stop uploading after creating XML package
	if(status == SUCCESS)
	{
		trmInitiatePackageUpload();
	}
	else
	{
		trmUploadFailed(status);
	}

	return status;
}
