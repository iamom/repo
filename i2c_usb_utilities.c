#include                <stdio.h>
#include 				<ctype.h>

#include                "nucleus.h"
#include				"pbos.h"
#include                "kernel/nu_kernel.h"
#include                "connectivity/nu_connectivity.h"
#include				"debugTask.h"
#include				"HAL_AM335x_GPIO.h"
#include				"HAL_AM335x_I2C.h"
#include 				"i2c_usb_utilities.h"

// Firmware def files generated from Cypress issued .cyacd (type from me made all files here cyad in name)
// files converted by using windows cmd line call:
// xxd -i <filename to convert> > <destination name>
//
//#include 				"ccg2_cyad_1.def"
//#include 				"ccg2_cyad_2.def"
//#include 				"ccg2_cyad_3.def"
//#include 				"ccg2_cyad_4.def"
//#include 				"ccg2_cyad_5.def"
//#include 				"ccg2_cyad_6.def"
//#include 				"ccg2_cyad_7.def"
		// WARNING!!! release 8 bricks the PDM Chip, do not use.
//#include 				"ccg2_cyad_9.def"
//#include				"ccg2_cyad_10_V2_4.def"
//#include				"ccg2_cyad_11_V3_0.def"
#include				"ccg2_cyacd_12_V3_1.def"

// identify start and length of firmware data.
//const UINT8 			*globalptrEmbeddedFirmwareStart 	= ccg2_cyad_7;
//const UINT 				globalEmbeddedFirmwareSize	= sizeof(ccg2_cyad_7);
//const UINT8 			*globalptrEmbeddedFirmwareStart 	= ccg2_cyad_9;
//const UINT 				globalEmbeddedFirmwareSize	= sizeof(ccg2_cyad_9);
//const UINT8 			*globalptrEmbeddedFirmwareStart 	= ccg2_cyad_10_V2_4;
//const UINT 				globalEmbeddedFirmwareSize	= sizeof(ccg2_cyad_10_V2_4);

//const UINT8 			*globalptrEmbeddedFirmwareStart 	= ccg2_cyad_11_V3_0;
//const UINT 				globalEmbeddedFirmwareSize	= sizeof(ccg2_cyad_11_V3_0);
const UINT8 			*globalptrEmbeddedFirmwareStart 	= ccg2_cyacd_12_V3_1;
const UINT 				globalEmbeddedFirmwareSize	= sizeof(ccg2_cyacd_12_V3_1);

const UINT8				globalI2CPDMAddresses[]				= {PDM_I2C_ADDRESS, PDM_I2C_ADDRESS_ALT};
const UINT				globalI2CPDMAddressCount 			= 2;
static UINT8			globalI2CPDMIndex 					= 0;
VOID                 	(*old_gpio3_lisr)(INT);
OS_HISR 				globalPDM_HISR;
UINT					globalI2CPDMFirmwareLoadActive 		= 0 ;
PB_PDM_AllVersions		globalPDMVersionCache ;

/***********************************************************************
*
*     FUNCTION
*
*         getNextPDMAddress
*
*     DESCRIPTION
*     Cypress went through a bad patch where they changed the hardware I2C address of their PDM
*     chip from 0x08 to 0x44 at random during the firmware upload operation. This routine is used
*     to combat this by allowing the caller to get an alternate address to try, globalI2CPDMAddresses
*     is an array of the possible addresses to try and globalI2CPDMAddressCount is the max number
*     of potential addresses available.
*
*     INPUTS
*
*
*     OUTPUTS
*
*
***********************************************************************/
UINT8 getNextPDMAddress()
{
	UINT8 res = globalI2CPDMAddresses[globalI2CPDMIndex];
	globalI2CPDMIndex++ ;
	globalI2CPDMIndex %= globalI2CPDMAddressCount;
	return res;
}

UINT8 cmdHubCmdStartConfig[]	= {0xFF, 0x01, 0x02};
UINT8 cmdHubCmdStartConfig1[]	= {0xFF, 0x01, 0x00};
UINT8 cmdHubCmdStopConfig[]		= {0xFF, 0x01, 0x05};

UINT8 cmdVID[]					= {0x00, 0x02, 0x24, 0x04};
UINT8 cmdPID[]					= {0x02, 0x02, 0x13, 0x25};
UINT8 cmdDID[]					= {0x04, 0x02, 0xB3, 0x0B};

//UINT8 cmdCFG1[]					= {0x06, 0x01, 0x92};	// 1001 0010 - works
//UINT8 cmdCFG1[]					= {0x06, 0x01, 0x12};	// 0001 0010 - make bus powered - does not work!
UINT8 cmdCFG1[]					= {0x06, 0x01, 0x93};	// 1001 0011 - make individual port power - works

UINT8 cmdCFG2[]					= {0x07, 0x01, 0x20};	// 0010 0000 - works
//UINT8 cmdCFG2[]					= {0x07, 0x01, 0xA0};	// 0010 0000  - add dynamic power - does not work!
UINT8 cmdCFG3[]					= {0x08, 0x01, 0x02};	// 0000 0010

UINT8 cmdNonRemovable[]			= {0x09, 0x01, 0x00};
UINT8 cmdPortsDisableSelf[]		= {0x0A, 0x01, 0x00};
UINT8 cmdPortsDisableBus[]		= {0x0B, 0x01, 0x00};
UINT8 cmdPortsMaxPowerSelf[]	= {0x0C, 0x01, 0x32};
UINT8 cmdPortsMaxPowerBus[]		= {0x0D, 0x01, 0x32};
UINT8 cmdPortsMaxCurrentSelf[]	= {0x0E, 0x01, 0x32};
UINT8 cmdPortsMaxCurrentBus[]	= {0x0F, 0x01, 0x32};
UINT8 cmdPowerOnTime[]			= {0x10, 0x01, 0x01};
UINT8 cmdEnableCharging[]		= {0xD0, 0x01, 0x1E};
UINT8 cmdPortMap12[]			= {0xFB, 0x01, 0x21};
UINT8 cmdPortMap34[]			= {0xFC, 0x01, 0x43};

/***********************************************************************
*
*     FUNCTION
*
*         configureUSBHub
*
*     DESCRIPTION
*		set up the USB hub component taken from the doc:
*		http://ww1.microchip.com/downloads/en/DeviceDoc/00001692C.pdf
*
*     INPUTS
*     	NU_MEMORY_POOL	pointer to nucleus memory pool, required for initializing the I2C interface.
*
*
*     OUTPUTS
*
*
***********************************************************************/
STATUS configureUSBHub(NU_MEMORY_POOL *pMem )
{
	STATUS 			status = NU_SUCCESS;
	PB_I2C 			pbI2C ;
	PB_I2C_NODE		HUBSlaveNode = {{HUB_I2C_ADDRESS, I2C_DEFAULT_ADDRESS_TYPE, I2C_SLAVE_NODE}, 0, 0};

	pbInitializeI2C(&pbI2C, pMem);

    int iStage = 0 ;
    while(status == NU_SUCCESS)
    {
		if((status = pbI2CWrite(&pbI2C, &HUBSlaveNode, cmdVID, 				(UINT)sizeof(cmdVID)) 				!= NU_SUCCESS)) 	break;
		iStage = 1 ;
		if((status = pbI2CWrite(&pbI2C, &HUBSlaveNode, cmdPID, 				(UINT)sizeof(cmdPID)) 				!= NU_SUCCESS)) 	break;
		iStage = 2 ;
		if((status = pbI2CWrite(&pbI2C, &HUBSlaveNode, cmdDID, 				(UINT)sizeof(cmdDID)) 				!= NU_SUCCESS)) 	break;
		iStage = 3 ;
		if((status = pbI2CWrite(&pbI2C, &HUBSlaveNode, cmdCFG1, 			(UINT)sizeof(cmdCFG1)) 				!= NU_SUCCESS)) 	break;
		iStage = 4 ;
		if((status = pbI2CWrite(&pbI2C, &HUBSlaveNode, cmdCFG2, 			(UINT)sizeof(cmdCFG2)) 				!= NU_SUCCESS)) 	break;
		iStage = 5 ;
		if((status = pbI2CWrite(&pbI2C, &HUBSlaveNode, cmdCFG3, 			(UINT)sizeof(cmdCFG3)) 				!= NU_SUCCESS)) 	break;
		iStage = 6 ;
		if((status = pbI2CWrite(&pbI2C, &HUBSlaveNode, cmdEnableCharging, 	(UINT)sizeof(cmdEnableCharging)) 	!= NU_SUCCESS)) 	break;
		iStage = 7 ;
		if((status = pbI2CWrite(&pbI2C, &HUBSlaveNode, cmdNonRemovable, 	(UINT)sizeof(cmdNonRemovable)) 		!= NU_SUCCESS)) 	break;
		iStage = 8 ;
		if((status = pbI2CWrite(&pbI2C, &HUBSlaveNode, cmdHubCmdStopConfig, (UINT)sizeof(cmdHubCmdStopConfig)) 	!= NU_SUCCESS)) 	break;
		iStage = 9 ;
		break;
    }


	if((status != NU_SUCCESS) || (iStage < 9))
	{
		debugDisplayOnly("\r\nERROR loading USB Hub I2C configuration : returned status = %d [stage %d]\r\n", (INT)status, iStage);
		status = NU_RETURN_ERROR;
	}

	pbDestroyI2C(&pbI2C);

	return status;
}


/***********************************************************************
*
*     FUNCTION
*
*         initializePDMOverCurrentPin
*
*     DESCRIPTION
*     Set up GPIO3:15 as an input ready for use as an interrupt detector for the PDM chip.
*
*     INPUTS
*
*
*     OUTPUTS
*
*
***********************************************************************/
void initializeGPIO3_15PDMOverCurrentPin()
{
	// setup PDM Sitara input pin functionality...
	// set up interrupt monitoring pin for USB C over current condition...
	// initGPIO(3) ; let's hope this has been done...

	UINT32 *pOverCurrentIP = addressGPIOPin(3, 15);
	clearGPIOPinAsOutput(3, 15);
	*pOverCurrentIP = 0x7 | (1 << 4) | (1 << 5) | (1 << 6);
	// set up complete...
}

// reference all-version data in our original board:
// {0x82, 0x03, 0x02, 0x10, 0x56, 0x41, 0x00, 0x01, 0x82, 0x03, 0x02, 0x10, 0x56, 0x41, 0x00, 0x01};

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
// CCG2 command set per rskv-212_3049453_V manual p9
//
UINT8 cmdReadDeviceModeReg[]	= {0x00};
UINT8 cmdReadBootModeReasonReg[]= {0x01};
UINT8 cmdReadSiliconID[]		= {0x02};
UINT8 cmdReadLoaderLastRow[]	= {0x04};
UINT8 cmdReadIntrReg[]			= {0x06};
UINT8 cmdClearIntrReg[]			= {0x06, 0x01};
UINT8 cmdJumpToBoot[]			= {0x07, 'J'};
UINT8 cmdHardReset[]			= {0x08, 'R', 0x01};
UINT8 cmdI2CReset[]				= {0x08, 'R', 0x00};
UINT8 cmdValidateFirmware[]		= {0x0B, 0x01};
UINT8 cmdEnterFlashingMode[]	= {0x0A, 'P'};
UINT8 cmdFlashReadWrite[]		= {0x0C, 'F', 0x00, 0x00, 0x00};
UINT8 cmdReadAllVersion[]		= {0x10};
UINT8 cmdPDCtrlPortDisable[]	= {0x28, 0x11};
UINT8 cmdCtrlPortHardReset[]	= {0x28, 0x0D};
UINT8 cmdCtrlPortSoftReset[]	= {0x28, 0x0E};
UINT8 cmdReadPDStatus[]			= {0x2C};
UINT8 cmdReadTypeCStatus[]		= {0x30};
UINT8 cmdReadEventMask[]		= {0x48};
UINT8 cmdReadResponseRegister[]	= {0x7E};
UINT8 cmdReadPowerStatus[]		= {0x2C};
UINT8 cmdTriggerDataRoleSwap[]	= {0x28, 0x05};
UINT8 cmdTriggerPowerRoleSwap[]	= {0x28, 0x06};
UINT8 cmdVCONNOn[]				= {0x28, 0x07};
UINT8 cmdVCONNOff[]				= {0x28, 0x08};
UINT8 cmdPortDisable[]			= {0x28, 0x11};
UINT8 cmdDataObjects[]			= {0x34};
UINT8 cmdPDOMasks[]				= {0x24};

UINT8 cmdReadSourcePDO[]		= {0x26};
UINT8 cmdReadSinkPDO[]			= {0x27};
UINT8 cmdSetSourcePDO4[]		= {0x26, 0x10};


STATUS sendCommandToPDM(NU_MEMORY_POOL *pnuMemPool, UINT8 *cmd, size_t sz)
{
	PB_I2C 					pbI2C ;
	PB_I2C_NODE				PDMSlaveNode = {{PDM_I2C_ADDRESS, I2C_DEFAULT_ADDRESS_TYPE, I2C_SLAVE_NODE}, 0, 0};
	STATUS					status = NU_SUCCESS;

	pbInitializeI2C(&pbI2C, pnuMemPool);
	clearIntrReg(&pbI2C, &PDMSlaveNode) ;
	status = pbI2CWrite(&pbI2C, &PDMSlaveNode, cmd, sz);
	NU_Sleep(10) ;
	waitForIntr(&pbI2C, 	&PDMSlaveNode);
	handleResponse(&pbI2C,	&PDMSlaveNode);
	clearIntrReg(&pbI2C, 	&PDMSlaveNode) ;
	pbDestroyI2C(&pbI2C);
	return status ;
}

#define PDM_MAXBUF_SIZE			256
STATUS displayPDMRegisters(NU_MEMORY_POOL *pnuMemPool, UINT8 cmd, size_t sz)
{
	PB_I2C 					pbI2C ;
	PB_I2C_NODE				PDMSlaveNode = {{PDM_I2C_ADDRESS, I2C_DEFAULT_ADDRESS_TYPE, I2C_SLAVE_NODE}, 0, 0};
	STATUS					status = NU_RETURN_ERROR;
	UINT8					buf[PDM_MAXBUF_SIZE];

	if(sz < PDM_MAXBUF_SIZE)
	{
		pbInitializeI2C(&pbI2C, pnuMemPool);
		clearIntrReg(&pbI2C, &PDMSlaveNode) ;
		if((status = pbI2CWrite(&pbI2C, &PDMSlaveNode, &cmd, 1) == NU_SUCCESS))
		{
			status = pbI2CRead(&pbI2C, &PDMSlaveNode, buf, sz);
			if(status == NU_SUCCESS)
			{
				debugDisplayOnly("\r\n");
				dumpHex(buf, sz, cmd);
				debugDisplayOnly("\r\n");
			}
		}
		pbDestroyI2C(&pbI2C);
	}
	return status ;
}


/***********************************************************************
*
*     FUNCTION
*		readPDOMasks
*
*     DESCRIPTION
*
*     INPUTS
*		PB_I2C			I2C instance pointer
*		PB_I2C_NODE		I2C Target node pointer
*		PB_PDO_MASKS	Pointer to structure to hold PDO mask qyery response
*
*     OUTPUTS
*
*
***********************************************************************/
STATUS readPDOMasks(PB_I2C *pbI2C, PB_I2C_NODE *node, pPB_PDO_MASKS pDOM)
{
	STATUS status = 0 ;

    memset(pDOM, 0, sizeof(PB_PDO_MASKS)) ;


	status = pbI2CWrite(pbI2C, node, cmdPDOMasks, (UINT)sizeof(cmdPDOMasks));
	if(status == NU_SUCCESS)
	{
		status = pbI2CRead(pbI2C, node, (UINT8 *)pDOM, sizeof(PB_PDO_MASKS));
	}
	if(status != NU_SUCCESS)
		debugDisplayOnly("\r\nERROR - Unable to read current PDO Masks\r\n") ;
	return status ;
}


/***********************************************************************
*
*     FUNCTION
*		readPDMPowerStatus
*
*     DESCRIPTION
*
*     INPUTS
*		PB_I2C					I2C instance pointer
*		PB_I2C_NODE				I2C Target node pointer
*		PB_PDM_POWER_STATUS		Pointer to structure to hold PDM Power status query response
*
*     OUTPUTS
*
*
***********************************************************************/
STATUS readPDMPowerStatus(PB_I2C *pbI2C, PB_I2C_NODE *node, pPB_PDM_POWER_STATUS pPwrStatus)
{
	STATUS status = 0 ;

	status = pbI2CWrite(pbI2C, node, cmdReadPowerStatus, (UINT)sizeof(cmdReadPowerStatus));
	if(status == NU_SUCCESS)
	{
		status = pbI2CRead(pbI2C, node, (UINT8 *)pPwrStatus, sizeof(PB_PDM_POWER_STATUS));
	}

	if(status != NU_SUCCESS)
	{
		debugDisplayOnly("!! ERROR failed to read PDM power status") ;
	}

	return status ;
}

/***********************************************************************
*
*     FUNCTION
*		readCurrentDataObjects
*
*     DESCRIPTION
*
*     INPUTS
*		PB_I2C						I2C instance pointer
*		PB_I2C_NODE					I2C Target node pointer
*		PB_PDM_CURRENT_DATA_OBJECTS	Pointer to structure to hold PDM Data Object Query response
*
*     OUTPUTS
*
*
***********************************************************************/
STATUS readCurrentDataObjects(PB_I2C *pbI2C, PB_I2C_NODE *node, pPB_PDM_CURRENT_DATA_OBJECTS pDO)
{
	STATUS status = 0 ;

    memset(pDO, 0, sizeof(PB_PDM_CURRENT_DATA_OBJECTS)) ;

	status = pbI2CWrite(pbI2C, node, cmdDataObjects, (UINT)sizeof(cmdDataObjects));
	if(status == NU_SUCCESS)
	{
		status = pbI2CRead(pbI2C, node, (UINT8 *)pDO, sizeof(PB_PDM_CURRENT_DATA_OBJECTS));
	}
	if(status != NU_SUCCESS)
		debugDisplayOnly("\r\nERROR - Unable to read current PDM Data Objects\r\n") ;
	return status ;
}


/***********************************************************************
*
*     FUNCTION
*		printPDMPowerStatus
*
*     DESCRIPTION
*
*     INPUTS
*		char *					pointer to a preamble string to be applied prior to the formatted output
*		PB_PDM_POWER_STATUS		pointer to the Power Status structure to display
*		char *					pointer to a postamble string to be applied after the formatted output
*
*     OUTPUTS
*
*
***********************************************************************/
void printPDMPowerStatus(char *strPreamble, pPB_PDM_POWER_STATUS pPwrStatus, char *strPostamble)
{
	debugDisplayOnly("%sPDM Status - %08lX  --  %02X\r\n", strPreamble, pPwrStatus->bytes.pd_status, pPwrStatus->bytes.c_status) ;
	printCTypeStatus(&pPwrStatus->bits.c_status) ;
	printPDStatus(&pPwrStatus->bits.pd_status) ;
	debugDisplayOnly("%s", strPostamble);
}

/***********************************************************************
*
*     FUNCTION
*		printPDMCurrentDataObjects
*
*     DESCRIPTION
*
*     INPUTS
*		char *							pointer to a preamble string to be applied prior to the formatted output
*		PB_PDM_CURRENT_DATA_OBJECTS		pointer to the PDM Data Object structure to display
*		char *							pointer to a postamble string to be applied after the formatted output
*
*     OUTPUTS
*
*
***********************************************************************/
void printPDMCurrentDataObjects(char *strPreamble, pPB_PDM_CURRENT_DATA_OBJECTS pPDMDO, char *strPostamble)
{
	debugDisplayOnly("%s", strPreamble) ;
	printPDMPDOStatus("PDO  - ", &pPDMDO->bits.PDO, "\r\n") ;
	printPDMPDOStatus("RDO  - ", &pPDMDO->bits.RDO, "\r\n") ;
	printPDMPDOStatus("VDO  - ", &pPDMDO->bits.VDO, "\r\n") ;
	debugDisplayOnly("%s", strPostamble) ;
}

/***********************************************************************
*
*     FUNCTION
*		printPDMPDOMasks
*
*     DESCRIPTION
*
*     INPUTS
*		char *					pointer to a preamble string to be applied prior to the formatted output
*		PB_PDO_MASKS			pointer to the PDM Masks structure to display
*		char *					pointer to a postamble string to be applied after the formatted output
*
*     OUTPUTS
*
*
***********************************************************************/
void printPDMPDOMasks(char *strPreamble, pPB_PDO_MASKS pDOM, char *strPostamble)
{
	debugDisplayOnly("%sSource PDO Mask = 0X%02X [", strPreamble, pDOM->source) ;
	dumpAsBinary(pDOM->source);
	debugDisplayOnly("]\r\n  Sink PDO Mask = 0X%02X [", pDOM->sink) ;
	dumpAsBinary(pDOM->sink);
	debugDisplayOnly("]%s", strPostamble) ;
}

/***********************************************************************
*
*     FUNCTION
*		printPDMPDOStatus
*
*     DESCRIPTION
*
*     INPUTS
*		char *					pointer to a preamble string to be applied prior to the formatted output
*		PDO						pointer to the PDM Power Delivery Object structure to display
*		char *					pointer to a postamble string to be applied after the formatted output
*
*     OUTPUTS
*
*
***********************************************************************/
void printPDMPDOStatus(char *strPreamble, pPB_PDO pPDO, char *strPostamble)
{
	debugDisplayOnly("%s Max Cur:%4d mA, ", 	strPreamble, pPDO->maxCurrent * 10) ;
	debugDisplayOnly("Volt:%5d mV, ", 			pPDO->voltage*50) ;
	debugDisplayOnly("Peak Cur:%X, ", 			pPDO->peakCurrent);
	debugDisplayOnly("%sData Role Swap, ", 		pPDO->dataRoleSwap == 0 ? "NO " : "   ");
	debugDisplayOnly("%sUSB comms, ", 			pPDO->USBCapable == 0 ? "NO " : "   ") ;
	debugDisplayOnly("%sPowered, ", 			pPDO->externallyPowered == 0 ? "Internally " : "Externally ");
	debugDisplayOnly("%sUSB Suspend support, ", pPDO->usbSuspend == 0 ? "NO " : "   " ) ;
	debugDisplayOnly("%s-Role Power, ", 		pPDO->dualRolePower == 0 ? " Single" : "   Dual") ;

	switch(pPDO->fixedSupply)
	{
		case 0:
			debugDisplayOnly("   FIXED ");
			break ;
		case 1:
			debugDisplayOnly(" BATTERY ");
			break ;
		case 2:
			debugDisplayOnly("VARIABLE ");
			break ;
		default:
			debugDisplayOnly(" UNKNOWN ");
			break ;
	}
	debugDisplayOnly("Supply [%08X]  %s", pPDO->u32Val, strPostamble) ;
}

/***********************************************************************
*
*     FUNCTION
*
*         printPDMPowerDeliveryStatus
*
*     DESCRIPTION
*     	Get the USB C active PDM status and display it
*
*     INPUTS
*     	NU_MEMORY_POOL					we require the memory pool to initialize the I2C interface.
*     	PB_PDO_MASKS					structure to hold the data object mask info
*     	PB_PDM_POWER_STATUS				structure to hold the power status info
*     	PB_PDM_CURRENT_DATA_OBJECTS		structure to hold the Data object info.
*
*
*     OUTPUTS
*
*
***********************************************************************/
void printPDMPowerDeliveryStatus(NU_MEMORY_POOL *pnuMemPool, pPB_PDO_MASKS pDOM, pPB_PDM_POWER_STATUS pPwrStatus, pPB_PDM_CURRENT_DATA_OBJECTS pDO)
{
	STATUS						status = NU_SUCCESS;
	PB_I2C 						pbI2C;
	PB_I2C_NODE 				node = {{PDM_I2C_ADDRESS, I2C_DEFAULT_ADDRESS_TYPE, I2C_SLAVE_NODE}, 0, 0};
	PB_PDO_MASKS				masks;
	PB_PDM_POWER_STATUS			powerStat;
	PB_PDM_CURRENT_DATA_OBJECTS	pdmDO;

	pbInitializeI2C(&pbI2C, pnuMemPool);

	status = readPDOMasks(&pbI2C, &node, &masks);
	if(status == NU_SUCCESS)
		status = readPDMPowerStatus(&pbI2C, &node, &powerStat);
	if(status == NU_SUCCESS)
		status = readCurrentDataObjects(&pbI2C, &node, &pdmDO);
	if(status == NU_SUCCESS)
	{
		printPDMPDOMasks("\r\nPower Deliver Module Masks:\r\n", &masks, "\r\n");
		printPDMCurrentDataObjects("\r\nUSB C Connection state:\r\n", &pdmDO, "\r\n");
		printPDMPowerStatus("\r\nPower Delievery Module Status:\r\n", &powerStat, "\r\n");
	}
	else
	{
		debugDisplayOnly("\r\nERROR - unable to read PDM USB C Power Status\r\n") ;
	}

	pbDestroyI2C(&pbI2C);
}


/***********************************************************************
*
*     FUNCTION
*         checkFirmwareAndUpdate
*
*     DESCRIPTION
*		check the supplied firmware against the contents of the PDM and:
*			if the ForceIfDifferent parameter is true the PDM will be updated if the PDM and supplied
*			versions do not match.
*			if the ForceIfDifferent is not set then the PDM will be updated only if the supplied firmware
*			is a newer version.
*
*     INPUTS
*     	NU_MEMORY_POOL					we require the memory pool to initialize the I2C interface.
*		UINT8 *							pointer to memory block containing new firmware
*		UINT							size of supplied firmware
*		BOOL							ForceIfDifferent, if TRUE will update if PDM and new
*										firmware versions do not match
*
*     OUTPUTS
*		STATUS							return NU_SUCCESS if all ok
*
***********************************************************************/
//STATUS checkFirmwareAndUpdate(NU_MEMORY_POOL *pnuMemPool, UINT8 *pFirmwareStart, UINT uFirmwareSize, BOOL bForceIfDifferent)
STATUS checkFirmwareAndUpdate(NU_MEMORY_POOL *pnuMemPool, BOOL bForceIfDifferent)
{
	PB_I2C 					pbI2C ;
	PB_I2C_NODE				PDMSlaveNode = {{PDM_I2C_ADDRESS, I2C_DEFAULT_ADDRESS_TYPE, I2C_SLAVE_NODE}, 0, 0};
	STATUS					status = NU_SUCCESS;
	PB_PDM_Version			newFW;

	pbInitializeI2C(&pbI2C, pnuMemPool);

	if((status = getPDMAllVersions(&pbI2C, &PDMSlaveNode, &globalPDMVersionCache)) == NU_SUCCESS)
	{
		printfAllPDMVersion( &globalPDMVersionCache);
		status = getFirmwareVersionFromFile(&newFW, (UINT8 *)globalptrEmbeddedFirmwareStart, globalEmbeddedFirmwareSize);
		printPDMVersion(" FILE:",&newFW ,"\r\n\r\n");

		if(status == NU_SUCCESS)
		{
			if(bForceIfDifferent)
			{
				if( (newFW.AppMajorVersion == globalPDMVersionCache.Firmware.AppMajorVersion) &&
				    (newFW.AppMinorVersion == globalPDMVersionCache.Firmware.AppMinorVersion) )
				{
					debugDisplayOnly("USB PDM - New Firmware matches existing Firmware - no operation\r\n");
				}
				else
				{
					if(loadFirmwareIntoPDM(&pbI2C, &PDMSlaveNode) == NU_SUCCESS)
						debugDisplayOnly("USB PDM - New Firmware loaded successfully\r\n");
					else
						debugDisplayOnly("ERROR USB PDM - Error detected loading new firmware\r\n");
				}
			}
			else
			{
				// only update if the new version is greater than the old version
				BOOL bDoIt = FALSE;
				bDoIt = (newFW.AppMajorVersion > globalPDMVersionCache.Firmware.AppMajorVersion);
				if(!bDoIt && (newFW.AppMajorVersion == globalPDMVersionCache.Firmware.AppMajorVersion))
				{
					bDoIt = (newFW.AppMinorVersion > globalPDMVersionCache.Firmware.AppMinorVersion);
				}
				if(bDoIt)
				{
					dbgTrace(DBG_LVL_INFO,"Replacing PDM fw %02d:%02d with %02d:%02d",
							globalPDMVersionCache.Firmware.AppMajorVersion,
							globalPDMVersionCache.Firmware.AppMinorVersion,
							newFW.AppMajorVersion,
							newFW.AppMinorVersion
							);
					if(loadFirmwareIntoPDM(&pbI2C, &PDMSlaveNode) == NU_SUCCESS)
					{
						dbgTrace(DBG_LVL_INFO,"PDM new Firmware loaded successfully\r\n");
					}
					else
					{
						dbgTrace(DBG_LVL_INFO,"PDM - ERROR loading new firmware\r\n");
					}
				}
				else
				{
					debugDisplayOnly("Existing PDM Firmware is up to date\r\n");
				}
			}
		}
	}

	pbDestroyI2C(&pbI2C);
	resetPDMISR();
	return status;
}

/***********************************************************************
*
*     FUNCTION
*		getFirmwareVersionFromFile
*
*     DESCRIPTION
*     	Populate the firmware version structure from the supplied cyacd file def data
*     	this exists on line 3 of the data
*     	This current version only operates on the compiled in file however by having the
*     	structure start and size it is simple to extend this to handle dynamic data.
*
*     INPUTS
*		PB_PDM_Version		pointer to PDM Version structure to plage the data in
*
*     OUTPUTS
*     	STATUS				NU_SUCCESS if all ok else NU_RETURN_ERROR
*
*
***********************************************************************/
UINT8 getFirmwareVersionFromFile(pPB_PDM_Version pAV, UINT8 *pFirmwareStart, UINT uFirmwareSize)
{
	STATUS				status = NU_SUCCESS;
	UINT8 				bufRow[200] ;
	UINT 				rowLength ;
	UINT8				*pLineStart 	= (UINT8 *)pFirmwareStart;
	UINT8 				*pNextLine 		= pLineStart;
	pstructFirmwareLine pFL 			= (pstructFirmwareLine)bufRow;
	UINT8 				chkSum ;
	UINT8				bufCCG[0x81] ;
	INT					iPass 			= 0 ;
	pstructFlashRW 		sFRW 			= (pstructFlashRW)cmdFlashReadWrite ;
	UINT8				*pFirmwareEnd 	= (UINT8 *)(pLineStart + uFirmwareSize);


	while((status == NU_SUCCESS) && (pNextLine < pFirmwareEnd))
	{
		chkSum =  readFirmwareLine(pLineStart, pFirmwareEnd, bufRow, &rowLength, &pNextLine);
		if(chkSum != 0)
		{
			debugDisplayOnly("CHECKSUM ERROR [%02X]\r\n", chkSum) ;
			status = NU_RETURN_ERROR;
		}
		// increment line count...
		iPass++ ;
		// write data to transfer area buffer (shift up on, the address to write to is in the first byte)...
		memcpy(bufCCG+1, &(pFL->row.data), SWAP16_INPLACE(pFL->row.rowLen)) ;

		if(iPass == 3)
		{
			// firmware version info is in the first 8 bytes of the 3rd packet according to the manual
			memcpy(pAV, bufCCG+1, sizeof(PB_PDM_Version));
			break ;
		}

		// now issue burn flash notice....
		sFRW->Row = SWAP16_INPLACE(pFL->row.rowNum) ;

		pLineStart = pNextLine ;
	}

	return status;
}


/***********************************************************************
*
*     FUNCTION
*		loadFirmwareIntoPDM
*
*     DESCRIPTION
*     	load the cyacd data structure into the PDM chip
*
*     INPUTS
*		PB_I2C		pointer to I2C instance
*		PB_I2C_NODE	pointer to I2C target node
*
*     OUTPUTS
*     	STATUS		NU_SUCCESS if all ok else NU_RETURN_ERROR
*
*
***********************************************************************/
UINT8 loadFirmwareIntoPDM(PB_I2C *pbI2C, PB_I2C_NODE *node)
{
	UINT						status			= NU_SUCCESS;
	UINT8 						bufRow[200] ;
	PB_PDM_AllVersions			pdmAllVersions;
	UINT 						rowLength ;
	INT							iPass ;
	UINT8						*pLineStart 	= (UINT8 *)globalptrEmbeddedFirmwareStart;
	UINT8 						*pNextLine 		= pLineStart;
	pstructFirmwareLine 		pFL 			= (pstructFirmwareLine)bufRow;
	UINT8						response ;
	UINT8						*pFirmwareEnd 	= (UINT8 *)(pLineStart + globalEmbeddedFirmwareSize);

	globalI2CPDMFirmwareLoadActive = 0 ;
	debugDisplayOnly("\r\nInstalling firmware into USB Power Module\r\n") ;

	// clear any pending reset/restart interrupt notification.
	clearIntrReg(pbI2C, node) ;

	// reset the chip
	// Note the delay after issuing the reset, the chip needs time to settle else I2C comms hrows error.
	debugDisplayOnly("\r\nChip reset ... ");
	pbI2CWrite(pbI2C, node, cmdHardReset, sizeof(cmdHardReset));
	NU_Sleep(10) ;
	waitForIntr(pbI2C, node) ;
	response = readResponse(pbI2C, node) ;
	if(response == 0x80)
		debugDisplayOnly("Reset complete, %s", responseCodeToText(response)) ;
	else
	{
		status = NU_RETURN_ERROR;
		debugDisplayOnly("Incorrect reset response received [%02X]\r\n", response) ;
	}
	clearIntrReg(pbI2C, node) ;

	if(status == NU_SUCCESS)
	{
		// display silicon ID and firmware details
		debugDisplayOnly("Cypress CCG2 - Silicon ID = %04X\r\n", readSiliconId(pbI2C, node));
		getPDMAllVersions(pbI2C, node, &pdmAllVersions);
		printfAllPDMVersion(&pdmAllVersions) ;

		// check device mode and report...
		if((readDeviceModeReg(pbI2C, node) & 1) == 0)
			debugDisplayOnly("Device is in bootloader mode, reason = %02X\r\n", readBootModeReasonReg(pbI2C, node));
		else
			debugDisplayOnly("Device is in normal mode\r\n");
	}

	if(status == NU_SUCCESS)
	{
		//try putting chip into bootloader mode. Issue Jump then check mode...
		debugDisplayOnly("Jump to boot mode ...\r\n");
		pbI2CWrite(pbI2C, node, cmdJumpToBoot, sizeof(cmdJumpToBoot));
		NU_Sleep(10) ;
		status = -1;
		for(iPass=0 ; (iPass < globalI2CPDMAddressCount+1) && (status == -1) ; iPass++)
		{
			status = waitForIntrTimeout(pbI2C, node, 2) ;
			if(status == -1)
			{
				node->node.i2c_slave_address = getNextPDMAddress() ;
				debugDisplayOnly("\r\nTrying PDM I2C address %02X\r\n", node->node.i2c_slave_address) ;
			}
		}

		if(status == -1)
		{
			debugDisplayOnly("\r\nBoot mode failed with I2C error\r\n") ;
		}
		else
		{
			handleResponse(pbI2C, node);
			clearIntrReg(pbI2C, node) ;

			if((readDeviceModeReg(pbI2C, node) & 1) == 0)
				debugDisplayOnly("Device now in bootloader mode\r\n");
			else
			{
				status = NU_RETURN_ERROR;
				debugDisplayOnly("ERROR Device not in bootloader mode\r\n");
			}
		}
	}

	UINT8 			chkSum ;
	UINT8			bufCCG[0x81] ;
	pstructFlashRW 	sFRW 	= (pstructFlashRW)cmdFlashReadWrite ;
	UINT8 			res		= 0x0 ;

	iPass = 0 ;
	if(status == NU_SUCCESS)
	{
		// issue enter flashing mode signature
		debugDisplayOnly("Issue Enter Flashing mode signature ... ");
		pbI2CWrite(pbI2C, node, cmdEnterFlashingMode, sizeof(cmdEnterFlashingMode));
		waitForIntr(pbI2C, node);
		handleResponse(pbI2C, node) ;
		clearIntrReg(pbI2C, node) ;
		debugDisplayOnly("\r\n");

		// Ready to load firmware....
		debugDisplayOnly("\r\nIssue Flash Read for rows...\r\n");

		// we accumulate the data to write in our buffer however the first byte is reserved for the address to write to 0x80...
		bufCCG[0] = 0x80 ;
		clearIntrReg(pbI2C, node);

		// set to write mode...
		sFRW->rw = 1 ;
		while((status == NU_SUCCESS) && (pNextLine < pFirmwareEnd))
		{
			chkSum =  readFirmwareLine(pLineStart, pFirmwareEnd, bufRow, &rowLength, &pNextLine);
			if(chkSum != 0)
			{
				debugDisplayOnly("CHECKSUM ERROR [%02X]\r\n", chkSum) ;
				status = NU_RETURN_ERROR;
			}

			// increment line count...
			iPass++ ;

			// write data to transfer area buffer (shift up on, the address to write to is in the first byte)...
			memcpy(bufCCG+1, &(pFL->row.data), SWAP16_INPLACE(pFL->row.rowLen)) ;
			pbI2CWrite(pbI2C, node, bufCCG, SWAP16_INPLACE(pFL->row.rowLen)+1);

			// now issue burn flash notice....
			sFRW->Row = SWAP16_INPLACE(pFL->row.rowNum) ;

			if(((iPass % 16) == 0) || (iPass == 1))
				debugDisplayOnly("\r\nProgramming line %3d - Row %04X  ", iPass, sFRW->Row) ;

			pbI2CWrite(pbI2C, node, cmdFlashReadWrite, sizeof(cmdFlashReadWrite));
			status = waitForIntrTimeout(pbI2C, node, 5);
			if(status == NU_SUCCESS)
			{
				res = readResponse(pbI2C, node) ;
				if(res != 0x02)
				{
					debugDisplayOnly(" ABORTING process \r\n") ;
					status = NU_RETURN_ERROR;
					break ;
				}
				else
					debugDisplayOnly(".") ;
			}
			else
			{
				debugDisplayOnly("ERROR - Timeout waiting for INTR to be set - response %02X\r\n", readResponse(pbI2C, node)) ;
				status = NU_RETURN_ERROR;
				break ;
			}
			clearIntrReg(pbI2C, node);
			pLineStart = pNextLine ;
		}
	}


	if(status == NU_SUCCESS)
	{
		// if all went ok we should be able to validate the FW
		pbI2CWrite(pbI2C, node, cmdValidateFirmware, sizeof(cmdValidateFirmware));
		NU_Sleep(10) ;
		status = waitForIntrTimeout(pbI2C, node, 50);
		if(status == NU_SUCCESS)
		{
			response = readResponse(pbI2C, node) ;
		}
		else
		{
			debugDisplayOnly("ERROR timeout validating Firmware\r\n");
		}
		clearIntrReg(pbI2C, node);
	}

	if((status == NU_SUCCESS) && (response == 0x02))
	{
		debugDisplayOnly("\r\nFirmware bootload completed successfully, reseting\r\n");
		//
		status = -1;
		for(iPass=0 ; (iPass < globalI2CPDMAddressCount+1) && (status == -1) ; iPass++)
		{
			status = pbI2CWrite(pbI2C, node, cmdHardReset, sizeof(cmdHardReset));
			if(status == -1)
			{
				node->node.i2c_slave_address = getNextPDMAddress() ;
				debugDisplayOnly("\r\nTrying PDM I2C address %02X\r\n", node->node.i2c_slave_address) ;
			}
		}
		NU_Sleep(10) ;
		status = -1 ;
		for(iPass=0 ; (iPass < globalI2CPDMAddressCount+1) && (status == -1) ; iPass++)
		{
			status = waitForIntrTimeout(pbI2C, node, 2) ;
			if(status == -1)
			{
				node->node.i2c_slave_address = getNextPDMAddress() ;
				debugDisplayOnly("\r\nTrying PDM I2C address %02X\r\n", node->node.i2c_slave_address) ;
			}
		}

		// reset the chip
		response = readResponse(pbI2C, node) ;
		if(response == 0x80)
			debugDisplayOnly("Reset complete, %s\r\n", responseCodeToText(response)) ;
		else
		{
			debugDisplayOnly("Incorrect reset response received [%02X]\r\n", response) ;
		}
		clearIntrReg(pbI2C, node) ;
		debugDisplayOnly("Delay 10 seconds for firmware upgrade \r\n") ;
		for(iPass=0 ; iPass < 10 ; iPass++)
		{
			debugDisplayOnly(".");
			NU_Sleep(100) ;
		}

		// display silicon ID and firmware details
		debugDisplayOnly("\r\nCypress CCG2 - Silicon ID = %04X\r\n", readSiliconId(pbI2C, node));

		// since the system may have changed ensure we update the cached version information...
		getPDMAllVersions(pbI2C, node, &globalPDMVersionCache);
		printfAllPDMVersion(&globalPDMVersionCache) ;
	}
	else
	{
		debugDisplayOnly("\r\nERROR -- BOOTLOADING FAILED -- ERROR\r\n") ;
		status = -1 ;
	}
	globalI2CPDMFirmwareLoadActive = 0 ;
	return status ;
}

/***********************************************************************
*
*     FUNCTION
*
*         loadPDMAllVersionsCache
*
*     DESCRIPTION
*     	This call is intended to startup the I2C interface, get the version information and shut down the I2C service
*
*     INPUTS
*
*
*     OUTPUTS
*     	STATUS		NU_SUCCESS if all ok else NU_RETURN_ERROR
*
*
***********************************************************************/
STATUS loadPDMAllVersionsCache(NU_MEMORY_POOL 	*pnuMemPool)
{
	STATUS					status = NU_SUCCESS;
	PB_I2C 					pbI2C ;
	PB_I2C_NODE				node = {{PDM_I2C_ADDRESS, I2C_DEFAULT_ADDRESS_TYPE, I2C_SLAVE_NODE}, 0, 0};

	pbInitializeI2C(&pbI2C, pnuMemPool);
	status = getPDMAllVersions(&pbI2C, &node, &globalPDMVersionCache);
	if(status != NU_SUCCESS)
		dbgTrace(DBG_LVL_INFO, "ERROR Unable to load PDM Version cache information - has Over Current detection been tripped?\r\n");
	pbDestroyI2C(&pbI2C);
	return status ;
}

/***********************************************************************
*
*     FUNCTION
*
*         getPDVersion
*
*     DESCRIPTION
*     Get the complete version block from the PDM chip (16 bytes)
*
*     INPUTS
*		PB_I2C						pointer to I2C session to use
*		PB_I2C_NODE					pointer to node to use
*		pPB_PDM_ReadAllVersion		pointer to structure to place response into
*
*
*     OUTPUTS
*     	STATUS		NU_SUCCESS if all ok else NU_RETURN_ERROR
*
*
***********************************************************************/
STATUS getPDMAllVersions(PB_I2C *pbI2C, PB_I2C_NODE *node, pPB_PDM_AllVersions pRAV)
{
	STATUS status	= NU_SUCCESS ;

	memset(pRAV, 0x00, sizeof(PB_PDM_AllVersions));
	if((status = pbI2CWrite(pbI2C, node, cmdReadAllVersion, (UINT)sizeof(cmdReadAllVersion))) == NU_SUCCESS)
	{
		status = pbI2CRead(pbI2C, node, (UINT8 *)pRAV, sizeof(PB_PDM_AllVersions));
	}

	return status;
}


/***********************************************************************
*
*     FUNCTION
*		getPDFirmwareVersion
*
*     DESCRIPTION
*     	place key information from the cached PDM firmware version data into the supplied
*
*     INPUTS
*
*
*     OUTPUTS
*     	STATUS		NU_SUCCESS if all ok else NU_RETURN_ERROR
*
*
***********************************************************************/
STATUS getPDFirmwareVersion(char *chBuff, size_t szBuffSize)
{
	STATUS					status = (szBuffSize >= 6) ? NU_SUCCESS : NU_RETURN_ERROR;

	snprintf(chBuff, szBuffSize, "%02d.%02d", globalPDMVersionCache.Firmware.AppMajorVersion, globalPDMVersionCache.Firmware.AppMinorVersion);
	return status;
}

/***********************************************************************
*
*     FUNCTION
*
*         printfAllPDMVersion
*
*     DESCRIPTION
*     	print a formatted output of both the bootloader and firmware version structures
*
*     INPUTS
*		pPB_PDM_AllVersions		structure containint the PDM full version information
*
*     OUTPUTS
*
*
***********************************************************************/
void printfAllPDMVersion(pPB_PDM_AllVersions pVer)
{
	printPDMVersion("\r\n   BL:", &(pVer->BootLoader), "") ;
	printPDMVersion("\r\n   FW:", &(pVer->Firmware), "\r\n") ;
}

/***********************************************************************
*
*     FUNCTION
*
*         printfPDMVersion
*
*     DESCRIPTION
*     	print a formatted output of the PDM Version structure, format is:
*     	<preamble> Build 02BB  AppMajor:AppMinor 00:00  Name:md  ExtVer 00 Major:Minor 01:00  Patch 00 <postamble>
*
*     INPUTS
*     char *				preamble, printed prior to the structure data
*     pPB_PDM_Version		the structure of data to format and output.
*     char *				postamble, printed after the structure data.
*
*
*     OUTPUTS
*
*
***********************************************************************/
void printPDMVersion(char *strPreamble, pPB_PDM_Version pVer, char *strPostAmble)
{
	debugDisplayOnly("%s Build %04X  AppMajor:AppMinor %02X:%02X  Name:%c%c  ExtVer %02X Major:Minor %02X:%02X  Patch %02X%s",
			strPreamble,
			pVer->BuildNumber,
			pVer->AppMajorVersion,
			pVer->AppMinorVersion,
			(UINT8)(pVer->ApplicationName>>8),
			(UINT8)(pVer->ApplicationName&0xFF),
			pVer->ExternalCircuitSpecificVersion,
			pVer->MajorVersion,
			pVer->MinorVersion,
			pVer->PatchVersion,
			strPostAmble);
}



/***********************************************************************
*
*     FUNCTION
*		waitForIntrTimeout
*
*     DESCRIPTION
*     	PDM Helper function, await for the acknowledgement interrupt from the PDM
*     	chip but with a timeout return in case of no response.
*
*     INPUTS
*		PB_I2C						pointer to I2C session to use
*		PB_I2C_NODE					pointer to node to use
*		UINT						timeout count in approx 100mS intervals.
*
*     OUTPUTS
*     	UINT						will return NU_SUCCESS if completed else NU_RETURN_ERROR
*     								if system timed out.
*
*
***********************************************************************/
UINT waitForIntrTimeout(PB_I2C *pbI2C, PB_I2C_NODE *node, UINT uCount)
{
	UINT8 res ;
	UINT uLimit = 0 ;
	UINT ret = NU_SUCCESS;
	do
	{
		res = readIntrReg(pbI2C, node) ;
		if(res != 1)
		{
			NU_Sleep(10) ;
			debugDisplayOnly("@");
		}
	}while((res != 1) && (uLimit++ < uCount));
	if(uLimit >= uCount)
	{
		debugDisplayOnly("??");
		ret = NU_RETURN_ERROR;
	}

	return ret ;
}


/***********************************************************************
*
*     FUNCTION
*		waitForIntr
*
*     DESCRIPTION
*     	PDM Helper function, will return once the acknowledgemnt interrupt has
*     	been detected.
*
*     INPUTS
*		PB_I2C						pointer to I2C session to use
*		PB_I2C_NODE					pointer to node to use
*
*     OUTPUTS
*
*
***********************************************************************/
void waitForIntr(PB_I2C *pbI2C, PB_I2C_NODE *node)
{
	UINT8 res ;
	do
	{
		res = readIntrReg(pbI2C, node) ;
		if(res != 1)
		{
			NU_Sleep(10) ;
			debugDisplayOnly("@");
		}
	}while(res != 1);
}

/***********************************************************************
*
*     FUNCTION
*		clearIntrReg
*
*     DESCRIPTION
*     	Helper function, will clear any outstanding acknowledgements.
*
*     INPUTS
*		PB_I2C						pointer to I2C session to use
*		PB_I2C_NODE					pointer to node to use
*
*     OUTPUTS
*
*
***********************************************************************/
void clearIntrReg(PB_I2C *pbI2C, PB_I2C_NODE *node)
{
	STATUS s;
	UINT8 res = 0;

	do
	{
		s = pbI2CWrite(pbI2C, node, cmdClearIntrReg, (UINT)sizeof(cmdClearIntrReg));
		if(s != NU_SUCCESS)
			debugDisplayOnly("ERROR! EZI2C CLEAR_INTR_REGISTER - response %d\r\n", s) ;
		res = readIntrReg(pbI2C, node) ;
		if(res == 1)
		{
			NU_Sleep(10) ;
			debugDisplayOnly("#");
		}
	} while(res == 1);
}

/***********************************************************************
*
*     FUNCTION
*		readIntrReg
*
*     DESCRIPTION
*     	Read the acknowledgemnent byte and return it.
*
*     INPUTS
*		PB_I2C						pointer to I2C session to use
*		PB_I2C_NODE					pointer to node to use
*
*     OUTPUTS
*		UINT8						PDM acknowledgement byte
*
***********************************************************************/
UINT8 readIntrReg(PB_I2C *pbI2C, PB_I2C_NODE *node)
{
	UINT8 res = -1 ;
	STATUS s = pbI2CWrite(pbI2C, node, cmdReadIntrReg, (UINT)sizeof(cmdReadIntrReg));
	if(s != NU_SUCCESS)
	{
		debugDisplayOnly("ERROR! EZI2C READ_INTR_REGISTER write op - response %d\r\n", s) ;
	}
	else
	{
		s = pbI2CRead(pbI2C, node, &res, 1);
		if(s != NU_SUCCESS)
			debugDisplayOnly("ERROR! EZI2C READ_INTR_REGISTER read op - response %d\r\n", s) ;
	}
	return res;
}


/***********************************************************************
*
*     FUNCTION
*		readDeviceModeReg
*
*     DESCRIPTION
*     	Read the PDM Device mode byte and return it.
*
*     INPUTS
*		PB_I2C						pointer to I2C session to use
*		PB_I2C_NODE					pointer to node to use
*
*     OUTPUTS
*		UINT8						PDM device mode byte
*
***********************************************************************/
UINT8 readDeviceModeReg(PB_I2C *pbI2C, PB_I2C_NODE *node)
{
	UINT8 res ;
	STATUS s = pbI2CWrite(pbI2C, node, cmdReadDeviceModeReg, (UINT)sizeof(cmdReadDeviceModeReg));
	if(s != NU_SUCCESS)
		debugDisplayOnly("ERROR! EZI2C READ_INTR_REGISTER - response %d\r\n", s) ;

	s = pbI2CRead(pbI2C, node, &res, 1);
	if(s != NU_SUCCESS)
		debugDisplayOnly("ERROR! EZI2C READ_INTR_REGISTER - response %d\r\n", s) ;

	return res;
}

/***********************************************************************
*
*     FUNCTION
*		readBootModeReasonReg
*
*     DESCRIPTION
*     	Read the PDM Boot mode reason flag byte and return it.
*
*     INPUTS
*		PB_I2C						pointer to I2C session to use
*		PB_I2C_NODE					pointer to node to use
*
*     OUTPUTS
*		UINT8						PDM boot mode reason byte
*
***********************************************************************/
UINT8 readBootModeReasonReg(PB_I2C *pbI2C, PB_I2C_NODE *node)
{
	UINT8 res ;
	STATUS s = pbI2CWrite(pbI2C, node, cmdReadBootModeReasonReg, (UINT)sizeof(cmdReadBootModeReasonReg));
	if(s != NU_SUCCESS)
		debugDisplayOnly("ERROR! EZI2C READ_INTR_REGISTER - response %d\r\n", s) ;

	s = pbI2CRead(pbI2C, node, &res, 1);
	if(s != NU_SUCCESS)
		debugDisplayOnly("ERROR! EZI2C READ_INTR_REGISTER - response %d\r\n", s) ;

	return res;
}

/***********************************************************************
*
*     FUNCTION
*		handleResponse
*
*     DESCRIPTION
*     	Read the PDM response byte and display an interpreted output
*
*     INPUTS
*		PB_I2C						pointer to I2C session to use
*		PB_I2C_NODE					pointer to node to use
*
*     OUTPUTS
*
***********************************************************************/
void handleResponse(PB_I2C *pbI2C, PB_I2C_NODE *node)
{
	UINT8 buf[0x400];
	memset(buf, 0x00, 0x400);

	STATUS s = pbI2CWrite(pbI2C, node, cmdReadResponseRegister, (UINT)sizeof(cmdReadResponseRegister));
	if(s != NU_SUCCESS)
		debugDisplayOnly("ERROR! EZI2C READ_RESPONSE_REGISTER - response %d\r\n", s) ;

	s = pbI2CRead(pbI2C, node, buf, sizeof(structResponseRegister));
	if(s != NU_SUCCESS)
		debugDisplayOnly("ERROR! EZI2C READ_RESPONSE_REGISTER - response %d\r\n", s) ;

	pstructResponseRegister pRR = (pstructResponseRegister)buf;
	debugDisplayOnly("[%02X] %s %s", buf[0], pRR->Type == 1 ? "Event/Asynch" : "Response", responseCodeToText(buf[0]));

	if(pRR->Length > 0)
	{
		debugDisplayOnly("Additional response data -\r\n") ;
		s = pbI2CRead(pbI2C, node, buf, pRR->Length);
		dumpHex(buf, pRR->Length, 0x0) ;

	}
}

/***********************************************************************
*
*     FUNCTION
*		readResponse
*
*     DESCRIPTION
*     	Read the PDM response byte and return it
*
*     INPUTS
*		PB_I2C						pointer to I2C session to use
*		PB_I2C_NODE					pointer to node to use
*
*     OUTPUTS
*     	UINT8						PDM response byte value
*
***********************************************************************/
UINT8 readResponse(PB_I2C *pbI2C, PB_I2C_NODE *node)
{
	UINT8 buf[0x400];
	memset(buf, 0x00, 0x400);

	STATUS s = pbI2CWrite(pbI2C, node, cmdReadResponseRegister, (UINT)sizeof(cmdReadResponseRegister));
	if(s != NU_SUCCESS)
		debugDisplayOnly("ERROR! EZI2C READ_RESPONSE_REGISTER - response %d\r\n", s) ;

	s = pbI2CRead(pbI2C, node, buf, sizeof(structResponseRegister));
	if(s != NU_SUCCESS)
		debugDisplayOnly("ERROR! EZI2C READ_RESPONSE_REGISTER - response %d\r\n", s) ;

	return buf[0] ;
}


/***********************************************************************
*
*     FUNCTION
*		readSiliconId
*
*     DESCRIPTION
*     	Read the PDM Silicon ID word and return it
*
*     INPUTS
*		PB_I2C						pointer to I2C session to use
*		PB_I2C_NODE					pointer to node to use
*
*     OUTPUTS
*     	UINT16						PDM silicon ID value
*
***********************************************************************/
UINT16 readSiliconId(PB_I2C *pbI2C, PB_I2C_NODE *node)
{
	UINT16 res ;
	STATUS s = pbI2CWrite(pbI2C, node, cmdReadSiliconID, (UINT)sizeof(cmdReadSiliconID));
	if(s != NU_SUCCESS)
		debugDisplayOnly("ERROR! EZI2C READ_RESPONSE_REGISTER - response %d\r\n", s) ;

	s = pbI2CRead(pbI2C, node, (UINT8 *)&res, sizeof(res));
	if(s != NU_SUCCESS)
		debugDisplayOnly("ERROR! EZI2C READ_RESPONSE_REGISTER - response %d\r\n", s) ;
	return res;
}

/***********************************************************************
*
*     FUNCTION
*		readFirmwareLine
*
*     DESCRIPTION
*     	Read a line of the formatted ASCII text version of the hex file
*     	and translate it into binary data
*
*     INPUTS
*		UINT8*			pointer to current position in data file.
*		UINT8*			pointer to end of data file
*		UINT8*			pointer to buffer in which to place the result
*		UINT*			pointer to a UINT in which to place the amount of data translated
*		UINT8 **		pointer the end of the line translated.
*
*     OUTPUTS
*		UINT8			The checksum value for the returned line.
*
***********************************************************************/
UINT8 readFirmwareLine(UINT8 *pSrc, UINT8 *pEnd, UINT8 *buf, UINT *uResCount, UINT8 **nextLine)
{
	// extract data
	UINT uMSB = 1;
	UINT8 chkSum = 0 ;
	*uResCount = 0 ;

	// skip to first delimiter ':' character.
	for( ; (pSrc < pEnd) && (*pSrc != 0x3A) ; pSrc++)
		;
	// skip over then start parsing the line...
	pSrc++ ;
	for(  ; (*pSrc != 0x3A) && (pSrc < pEnd) ; pSrc++)
	{
		if((*pSrc != 0x0A) && (*pSrc != 0x0D))
		{
			if(uMSB)
			{
				buf[*uResCount] = ASCIIToHex(*pSrc) << 4;
				uMSB = 0 ;
			}
			else
			{
				buf[*uResCount] |= ASCIIToHex(*pSrc) ;
				chkSum += buf[*uResCount] ;
				(*uResCount)++;
				uMSB = 1;
			}
		}
	}

	*nextLine = pSrc ;

	return chkSum ;
}


/***********************************************************************
*
*     FUNCTION
*		responseCodeToText
*
*     DESCRIPTION
*		translate the supplied PDM response code into text.
*		NOTE!! this can be a dynamic process so the returned value is not
*		guaranteed past the next call to this routine.
*
*     INPUTS
*		UINT8		PDM response code to translate
*
*     OUTPUTS
*		char*		pointer to string representation of supplied code
*
***********************************************************************/
#define PRIVATE_RESPONSECODETEXT_SIZE	25
static char privateResponseCodeText[PRIVATE_RESPONSECODETEXT_SIZE+1];
char *responseCodeToText(UINT8 code)
{
	char *p = " UNKNOWN\r\n";

	switch(code)
	{
		case 0x00:
			p = "No Response. No outstanding command, event or asynchronous message\r\n";
		break;
		case 0x02:
			p = "Success. Command handled successfully.\r\n";
			break;
		case 0x03:
			p = "Flash Data Available. Flash row data requested by EC is available in Data Memory. 2.2.6.1\r\n";
		break;
		case 0x05:
			p = "Invalid Command. Partial, unaligned register write or invalid command.\r\n";
		break;
		case 0x07:
			p = "Flash Update Failed. Flash write operation failed.\r\n";
		break;
		case 0x08:
			p = "Invalid FW. FW validity check failed. Please refer VALIDATE_FW command. 4.2.4\r\n";
		break;
		case 0x09:
			p = "Invalid Arguments. Command handling failed due to invalid arguments.\r\n";
		break;
		case 0x0A:
			p = "Not Supported. Command not supported in the current mode.\r\n";
		break;
		case 0x0C:
			p = "Transaction Failed. GOOD_CRC not received for the PD message transmitted.\r\n";
		break;
		case 0x0D:
			p = "PD Command Failed. CCG1 is not able to handle the command issued 2.2.6.2\r\n";
		break;
		case 0x80:
			p = "Reset Complete. Device underwent a reset and is back in operation mode.\r\n";
		break;
		case 0x81:
			p = "Message Queue Overflow. Message queue overflow detected.\r\n";
		break;
		case 0x82:
			p = "Over Current Detected.\r\n";
		break;
		case 0x83:
			p = "Over Voltage Detected.\r\n";
		break;
		case 0x84:
			p = "Type C Port Connect Detected.\r\n";
		break;
		case 0x85:
			p = "Type C Port Disconnect Detected.\r\n";
		break;
		case 0x86:
			p = "PD Contract Negotiation Complete. 2.2.6.4\r\n";
		break;
		case 0x87:
			p = "SWAP Complete. 2.2.6.5\r\n";
		break;
		case 0x8A:
			p = "PS_RDY Message Received.\r\n";
		break;
		case 0x8B:
			p = "GotoMin Message Received.\r\n";
		break;
		case 0x8C:
			p = "Accept Message Received.\r\n";
		break;
		case 0x8D:
			p = "Reject Message Received.\r\n";
		break;
		case 0x8E:
			p = "Wait Message Received.\r\n";
		break;
		case 0x8F:
			p = "Hard Reset Received.\r\n";
		break;
		case 0x90:
			p = "VDM Received. 5.\r\n";
		break;
		case 0x91:
			p = "Source Capabilities Message Received. 2.2.6.6\r\n";
		break;
		case 0x92:
			p = "Sink Capabilities Message Received. 2.2.6.7\r\n";
		break;
		case 0x93:
			p = "Display Port Alternate Mode entered 2.2.6.8\r\n";
		break;
		case 0x94:
			p = "Display Port device connected at UFP_U. 2.2.6.8\r\n";
		break;
		break;
		case 0x95:
			p = "Display port device not connected at UFP_U. 2.2.6.8\r\n";
		break;
		case 0x96:
			p = "Display port SID not found in Discover SID process 2.2.6.8\r\n";
		break;
		case 0x97:
			p = "Multiple SVIDs discovered along with Display port SID 2.2.6.8\r\n";
		break;
		case 0x98:
			p = "DP Functionality not supported by Cable 2.2.6.8\r\n";
		break;
		case 0x99:
			p = "Display Port Configuration not supported by UFP 2.2.6.8\r\n";
		break;
		case 0x9A:
			p = "Hard Reset Sent to Port Partner\r\n";
		break;
		case 0x9B:
			p = "Soft Reset Sent to Port Partner\r\n";
		break;
		case 0x9C:
			p = "Cable Reset Sent to EMCA\r\n";
		break;
		case 0x9D:
			p = "Source Disabled State Entered\r\n";
		break;
		case 0x9E:
			p = "Sender Response Timer Timeout\r\n";
		break;
		case 0x9F:
			p = "No VDM Response Received\r\n";
		break;
		case 0xA0:
			p = "Unexpected Voltage on Vbus 2.2.6.9\r\n";
		break;
		case 0xA1:
			p = "Type-C ErrorRecovery 2.2.6.10\r\n";
		break;
		case 0xA6:
			p = "EMCA Detected 2.2.6.11\r\n";
		break;
		case 0xAA:
			p = "Rp change detected\r\n";
		break;

		default:
			snprintf(privateResponseCodeText, PRIVATE_RESPONSECODETEXT_SIZE,"UNKNOWN [%02X]\r\n", code) ;
			p = privateResponseCodeText;
			break;
	}
	return p;
}

/***********************************************************************
*
*     FUNCTION
*		printCTypeStatus
*
*     DESCRIPTION
*		display a translation of the PDM USB Status register
*
*     INPUTS
*		structTypeCStatus		pointer to PDM USB C Status
*
*     OUTPUTS
*
*
***********************************************************************/
void printCTypeStatus(pstructTypeCStatus pS)
{
	debugDisplayOnly("%s to partner, ", pS->conStatus == 1 ? "Connected" : "NOT Connected") ;
	debugDisplayOnly("CC%s, ", pS->ccPolarity == 0 ? "1" : "2") ;

	switch(pS->devType)
	{
		case 0:
			debugDisplayOnly("Nothing            ") ;
			break ;
		case 1:
			debugDisplayOnly("Upstream Facing    ") ;
			break ;
		case 2:
			debugDisplayOnly("Downstream Facing  ") ;
			break ;
		case 3:
			debugDisplayOnly("Debug Accessory    ") ;
			break ;
		case 4:
			debugDisplayOnly("Audio Accessory    ") ;
			break ;
		case 5:
			debugDisplayOnly("Powered Accessory   ") ;
			break ;
		default:
			debugDisplayOnly("Not Supported Device") ;
			break ;
	}
	debugDisplayOnly(" attached, ") ;

	debugDisplayOnly("CCG does %sdetect Ra, ", pS->raStatus == 0 ? "NOT " : "") ;

	debugDisplayOnly("Current level:") ;
	switch(pS->currentLevel)
	{
		case 0:
			debugDisplayOnly("Default");
			break ;
		case 1:
			debugDisplayOnly("1.5A");
			break ;
		case 2:
			debugDisplayOnly("3A");
			break ;
		default:
			debugDisplayOnly("unsupported");
			break ;
	}

	debugDisplayOnly("\r\n") ;
}

/***********************************************************************
*
*     FUNCTION
*		pstructPDStatus
*
*     DESCRIPTION
*		display a translated output of the power delivery structure information.
*
*     INPUTS
*		structPDStatus		pointer to power delivery status structure
*
*     OUTPUTS
*
*
***********************************************************************/
void printPDStatus(pstructPDStatus pPD)
{
	debugDisplayOnly("Port Role:") ;
	switch(pPD->dataRole)
	{
		case 0:
			debugDisplayOnly("UFP,      ");
			break ;
		case 1:
			debugDisplayOnly("DFP,      ");
			break ;
		case 2:
			debugDisplayOnly("DRP (%s), ", pPD->drpDataRole == 0 ? "UFP" : "DFP");
			break ;
		default:
			debugDisplayOnly("UNKNOWN,  ");
			break ;
	}

	debugDisplayOnly("Def Power Role:");
	switch(pPD->powerRole)
	{
		case 0:
			debugDisplayOnly("Sink,        ") ;
			break ;
		case 1:
			debugDisplayOnly("Source,      ");
			break ;
		case 2:
			debugDisplayOnly("Dual [%s], ", pPD->drPowerRole == 0 ? "Sink " : "Source") ;
			break ;
		default:
			debugDisplayOnly("UNKNOWN,     ") ;
			break ;
	}

	debugDisplayOnly("Current DATA Role:%s, ", pPD->curDataRole == 0 ? "UFP" : "DFP") ;

	debugDisplayOnly("Current POWER Role:%s, ", pPD->curPowerRole == 0 ? "Sink" : "Source") ;

	debugDisplayOnly("Contract state:%s, ", pPD->contractState == 0 ? "NONE" : "EXISTS") ;

	if(pPD->emcaPresent == 1)
		debugDisplayOnly("EMCA Exists, ");

	if(pPD->vconnStatus == 1)
	{
		debugDisplayOnly("VCONN Supplier - CCG is %ssourcing VCONN ", pPD->vconnSupplier == 0 ? "NOT " : "") ;
	}

	debugDisplayOnly("\r\n") ;


}



/***********************************************************************
*
*     FUNCTION
*
*         readPDMOverCurrentFlag
*
*     DESCRIPTION
*     Read the state of (currently)GPIO3:15 which is connected to the PDM over current
*     output pin. This pin goes low when an overcurrent condition occurs.
*
*     INPUTS
*
*
*     OUTPUTS
*		returns 1 if NO over current condition is present else 0.
*
***********************************************************************/
UINT readPDMOverCurrentFlag()
{
	return readSimpleGPIOPin(3, 15) ;
}


////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
/***********************************************************************
*
*     FUNCTION
*
*         prepPDMISRHandling
*
*     DESCRIPTION
*		Initialize the GPIO 3 - 15 IRQ for active low operation. This is the USB-c PDM over-current
*		detection alarm from Cypress. We disable active high and edge detection and setup nucleus for the
*		appropriate IRQ handler then clear any pending IRQs and enable this one.
*
*     INPUTS
*
*
*     OUTPUTS
*
*
***********************************************************************/
void prepPDMISRHandling()
{

	OSCreateHISR(&globalPDM_HISR, &safePDMOverCurrentNotification, 1000, "hisrPDMOC"); //

	// load up the ISR handler
	NU_Register_LISR(ESAL_PR_INT_IRQ62_GPIOINT3A, isr_PDM_OverCurrent, &old_gpio3_lisr);

	// Set interrupt to trigger on a low level
	ESAL_GE_MEM_WRITE32 (AM335X_GPIO3_BASE + AM335X_GPIO_LEVELDETECT0, (ESAL_GE_MEM_READ32 (AM335X_GPIO3_BASE + AM335X_GPIO_LEVELDETECT0)) | PDM_OC_GPIO_BIT_MASK);

    // clear interrupt triggering on 1 level
    ESAL_GE_MEM_WRITE32 (AM335X_GPIO3_BASE + AM335X_GPIO_LEVELDETECT1, (ESAL_GE_MEM_READ32(AM335X_GPIO3_BASE + AM335X_GPIO_LEVELDETECT1)) &  ~PDM_OC_GPIO_BIT_MASK);

    // clear interrupt triggering on rising edge   
	ESAL_GE_MEM_WRITE32 (AM335X_GPIO3_BASE + AM335X_GPIO_RISINGDETECT, (ESAL_GE_MEM_READ32(AM335X_GPIO3_BASE + AM335X_GPIO_RISINGDETECT)) &  ~PDM_OC_GPIO_BIT_MASK);

    // clear interrupt triggering on falling edge...
    ESAL_GE_MEM_WRITE32 (AM335X_GPIO3_BASE + AM335X_GPIO_FALLINGDETECT, (ESAL_GE_MEM_READ32(AM335X_GPIO3_BASE + AM335X_GPIO_FALLINGDETECT)) &  ~PDM_OC_GPIO_BIT_MASK);

    // Clear any pending IRQ on the OC line
    ESAL_GE_MEM_WRITE32 (AM335X_GPIO3_BASE + AM335X_GPIO_IRQSTATUS_0, PDM_OC_GPIO_BIT_MASK);

	// enable the interrupt feature for this pin
    ESAL_GE_MEM_WRITE32 (AM335X_GPIO3_BASE + AM335X_GPIO_IRQSTATUS_SET_0, PDM_OC_GPIO_BIT_MASK);

    // enable Nucleus
	ESAL_GE_INT_Enable(ESAL_PR_INT_IRQ62_GPIOINT3A,0,0x5);
}

/***********************************************************************
*
*     FUNCTION
*
*         resetPDMISR
*
*     DESCRIPTION
*     Since some actions can falsely trigger the PDM over current line (such as firmware upload)
*     this routine will allow the interrupt system to be reset.
*     NOTE: this will only work if the chip over current output is safe (high)
*
*     INPUTS
*
*
*     OUTPUTS
*
*
***********************************************************************/
void resetPDMISR()
{
    UINT32 valIRQStat = ESAL_GE_MEM_READ32 (AM335X_GPIO3_BASE + AM335X_GPIO_IRQSTATUS_SET_0);

    // only reset if the pin is high and the IRQ has been disabled
	if((readSimpleGPIOPin(3, 15) == 1) && ((valIRQStat & PDM_OC_GPIO_BIT_MASK) == 0))
	{
		// Clear any pending IRQ on the OC line
		ESAL_GE_MEM_WRITE32 (AM335X_GPIO3_BASE + AM335X_GPIO_IRQSTATUS_0, PDM_OC_GPIO_BIT_MASK);
		// enable the interrupt feature for this pin
		ESAL_GE_MEM_WRITE32 (AM335X_GPIO3_BASE + AM335X_GPIO_IRQSTATUS_SET_0, PDM_OC_GPIO_BIT_MASK);

		debugDisplayOnly("\r\nPDM Over current ISR has been reset to operational mode\r\n");
	}
	else if(readSimpleGPIOPin(3, 15) == 0)
	{
		debugDisplayOnly("\r\nUnable to reset PDM Over current ISR - PDM chip has registered an event.\r\n");
	}

}

/***********************************************************************
*
*     FUNCTION
*
*         isr_PDM_OverCurrent
*
*     DESCRIPTION
*		ISR for the GPIO3-15 USB-c PDM over current detection.
*		execute any prior IRQ present prior to installing this one.
*		Check if it is actually for us by verifying that the pin level is low (over-current detected).
*		Clear the interrupt.
*		Disable the interrupt (there is no way to make cypress raise the level).
*		Call the associated HISR, this allows more flexibility in handling than this LISR
*
*     INPUTS
*
*		ID		Interrupt ID that caused this call.
*
*     OUTPUTS
*
*
***********************************************************************/
void isr_PDM_OverCurrent(int id)
{
	if(old_gpio3_lisr != 0)
		old_gpio3_lisr(id);

	// check that it is GPIO3:15 that has been taken low....
	if((ESAL_GE_MEM_READ32 (AM335X_GPIO3_BASE + AM335X_GPIO_IRQSTATUS_0) & PDM_OC_GPIO_BIT_MASK) != 0)
	{
		// If this is the OC interrupt we cannot clear the pin condition so clear then disable the interrupt
	    ESAL_GE_MEM_WRITE32 (AM335X_GPIO3_BASE + AM335X_GPIO_IRQSTATUS_0, PDM_OC_GPIO_BIT_MASK);

	    ESAL_GE_MEM_WRITE32 (AM335X_GPIO3_BASE + AM335X_GPIO_IRQSTATUS_CLR_0,  PDM_OC_GPIO_BIT_MASK);
		OSActivateHISR(&globalPDM_HISR);
	}
}

/***********************************************************************
*
*     FUNCTION
*
*         safePDMOverCurrentNotification
*
*     DESCRIPTION
*		This is the PDM Over-current detection HISR routine, called when the PDM OC interrupt is triggered.
*		Note this is called only once. A power cycle is required to clear this condition.
*
*     INPUTS
*
*
*     OUTPUTS
*
*
***********************************************************************/
void safePDMOverCurrentNotification()
{
	// place your HISR safe code here
	debugQueueString("\r\n\r\n ** !!BANG!! ** \r\nUSB C Over-current detected - system is now unusable until POWER is cycled, reset will NOT fix this\r\n\r\n");
}
