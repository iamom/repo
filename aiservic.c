/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    AiServic.c
   
   DESCRIPTION:    Routines for retrieving information from graphics.
        Contains global functions with these prefixes: 
            fnGrfx - These functions can be used to retrived graphics data from  
                     both or either:
                     1. graphics in the EMD Linked Graphics List (LGL) 
                     2. graphics in the Graphics File (GF).
            fnLGL  - These functions are used to retrieve information from 
                     graphics in the EMD Linked Graphics List.  The calling  
                     function knows the component type, and that type is stored  
                     in the LGL.
            fnAI   - These functions do or may call fnValidData or fnWriteData 
                     to get or set an image selection, and then maybe retreive  
                     more information about that graphic.

            Exceptions:
                fnPutAILinkHeader
                fnGetAILinkHeader
                fnAIAddSavedGraphics -  Doesn't access bob variables. After an upgrade from 
                                        pre-GraphicsFile software rev. to a Post-GraphicsFile
                                        software rev., this adds any custom graphics that were
                                        saved in .grap files.

 ----------------------------------------------------------------------
 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
 REVISION HISTORY:

 28-Jul-15 sa002pe on FPHX 02.12 shelton branch
 	Merging in changes from Janus tips:
----*
	* 22-Jul-15 sa002pe on Janus tips shelton branch
	*	Changed fnGrfxFindByDataNameVer to only validate the length passed in for graphics
	*	in the GAR file instead of being hard-coded to validate the entire length.
	*
	*	Commented out include files, declarations & functions that are only used by Mega.
	*	Fixed some lint warnings.
----*

*************************************************************************/

// This file, and grapfile.c make up the "graphics module".
#define GRFX_MODULE

#include <stdlib.h>
#include <string.h>
#include "aiservic.h"
#include "bob.h"
#include "bobutils.h"
#include "custdat.h"
#include "datdict.h"
#include "fcntl.h"
#include "features.h"
#include "fnames.h"
#include "fstub.h"
#include "fwrapper.h"
#include "global.h"     // fnUnicodeNCopy() 
#include "grapext.h"
#include "lcdirect.h"   // fnLCDPutLineDirect(), fnLCDRefresh(). 
#include "oit.h"
#include "sys/stat.h"
#include "dirent.h"
#include "commontypes.h"
#include "unistring.h"
#include "utils.h"      // fnAscii2Unicode(), fnAsciiIsItBlank()
#include "flashutil.h"
#include "pbio.h"


//---------------------------------------------------------------------
//      Local Definitions:
//---------------------------------------------------------------------

#define INITIALBUFSIZE 0x8000 /* Initial buffer size used by saving downloaded graphics */



//---------------------------------------------------------------------
//      External Variable Declarations:
//---------------------------------------------------------------------

// From fwrapper.c:
extern UINT32 Current_Top_Of_Flash;


//---------------------------------------------------------------------
//      Local Constants:
//---------------------------------------------------------------------

// These are the component types that are in the graphics file.
//  Any other component types are in the EMD.
const UINT8 pNonEmdCompTypes[ ] = 
{
    IMG_ID_AD_SLOGAN,
    IMG_ID_INSCRIPTION,
    IMG_ID_PERMIT1_PE,
    IMG_ID_PERMIT2_PE,
    IMG_ID_PERMIT3_PE,
    IMG_ID_TOWN_CIRCLE,
    IMG_ID_TOWN_CIRCLE_2,
};


//---------------------------------------------------------------------
//      Private Variables:
//---------------------------------------------------------------------



//---------------------------------------------------------------------
//      Declaration of Local Functions:
//---------------------------------------------------------------------

static UINT8  * fnLGLGetNextGraphLnkFltrd( const UINT8 *pCurrentLink, UINT16 uwFlagTest );
static UINT16   fnLGLGetGenIdListByCompType( UINT8 bCompType, UINT16 *pDestIDList, 
                                             UINT16 wMaxReturn, UINT16 uwFlagTest );
static UINT16 fnGrfxGetGenIdListByCompType( UINT8 bCompType, UINT16 *pDestIDList, 
                                            UINT16 wMaxReturn, UINT16 uwFlagTest );
static UINT16   fnLGLGetListAllGraphics( UINT8 *pDestCompTypes, UINT16 *pDestGenIDs, 
                                         UINT16 wMaxReturn, UINT16 uwFlagTest );
static UINT16 fnGrfxGetListAllGFFirst( UINT8 *pDestCompTypes, UINT16 *pDestGenIDs, 
                                       UINT16 wMaxReturn, UINT16 uwFlagTest );
static int      fnGrfxCompareGenID( const void *idp2 , const void *idp1 );



// ********************************************************************************
// FUNCTION NAME: fnIsNonEmdCompType()                              
// PURPOSE:     Check if this component type is stored in the EMD Linked Graphic List
//              or in the Graphics File.
//
// AUTHOR:      CBellamy
// INPUTS:      bCompType - Component type to check. UINT8 
// OUTPUTS:     Return TRUE if this type is stored in the Graphics File.
//
// NOTES:  This function uses table pNonEmdCompTypes, which must be updated if 
//          any other component types are to be stored in the Graphics File.
//      Current design dictates that each component type can only exist in the 
//      graphics location dictated by its type; there are no types that can exist 
//      in either location.
//---------------------------------------------------------------------
BOOL fnGrfxIsNonEmdCompType( UINT8 bCompType )
{
    UINT8 i;

    for( i = 0; i < ARRAY_LENGTH( pNonEmdCompTypes ); i++ )
    {
        if( pNonEmdCompTypes[ i ] == bCompType )
            return( TRUE );
    }
    return( FALSE );
}

// This returns the opposite of the function above.
BOOL fnGrfxIsEmdCompType( UINT8 bCompType )
{
    BOOL retval = TRUE;

    if( fnGrfxIsNonEmdCompType( bCompType ) == TRUE )
    {
        retval = FALSE;
    }

    return( retval );
}

// ********************************************************************************
// FUNCTION NAME: fnGrfxGetGraphic()                              
// PURPOSE:     Given a pointer to the link header of a graphic, return a pointer 
//          to the graphic WITHOUT the link header.    
//
// AUTHOR:      CBellamy
// INPUTS:      LnkPtr - Pointer to the graphic, starting with the link header. 
// OUTPUTS:     Return Pointer to the same graphic, WITHOUT the link header.
//
// NOTES:  This function should work with either graphic File  graphics or Linked 
//      graphics, since the graphics in the graphic file have a link header
//      even though they are not linked.
//---------------------------------------------------------------------
UINT8 *fnGrfxGetGraphic( const UINT8 *LnkPtr )
{
    UINT8 *GrfxPtr = NULL_PTR;

    if( LnkPtr != NULL_PTR )
    {
        GrfxPtr = (UINT8 *)LnkPtr + COMPONENTGRAPHIC_OFFSET;
    }
    return( GrfxPtr );
}

// ********************************************************************************
// FUNCTION NAME: fnGrfxGetGenID()                              
// PURPOSE:     Given a pointer to the link header of a graphic, pull out and 
//          return the Generated ID from the Link Header.    
//
// AUTHOR:      CBellamy
// INPUTS:      LnkPtr - Pointer to the graphic, starting with the link header. 
// OUTPUTS:     Return the 16-bit generated ID from the link header.
//
// NOTES:  This function should work with either graphic File  graphics or Linked 
//      graphics, since the graphics in the graphic file have a link header
//      even though they are not linked.
//---------------------------------------------------------------------
UINT16 fnGrfxGetGenID( const UINT8 *LnkPtr )
{
    UINT16  wGenID;

    EndianAwareCopy( (UINT8 *)&wGenID, LnkPtr + COMPONENTID_OFFSET, sizeof(UINT16) );
    return( wGenID );
}

// ********************************************************************************
// FUNCTION NAME: fnGrfxGetCompType()                               
// PURPOSE:     Given a pointer to the link header of a graphic, pull out and 
//          return the Component Type from the graphic.    
//
// AUTHOR:      CBellamy
// INPUTS:      LnkPtr - Pointer to the graphic, starting with the link header. 
// OUTPUTS:     Return the 8-bit Component type from the graphic.
//
// NOTES:  This function should work with either graphic-File-graphics or linked 
//      graphics, since it pulls the component type from the actual graphic.
//---------------------------------------------------------------------
UINT8 fnGrfxGetCompType( UINT8 *LnkPtr )
{
    UINT8  bCompType = TURN_OFF_IMAGE_BYTE;

    (void)fnFlashGetGCcomponentType( LnkPtr + COMPONENTGRAPHIC_OFFSET , &bCompType ); 
    return( bCompType );
}

// ********************************************************************************
// FUNCTION NAME: fnGrfxGetDataNameVerFromGraphic()                             
// PURPOSE:     Returns a pointer to the DataName and Ver of a graphic    
//
// AUTHOR:      CBellamy
// INPUTS:      pMyGraphic - Pointer to the graphic, WITHOUT the link header.
// OUTPUTS:     Return pointer to the 8-byte DataName, which is followed by
//                      the 4-byte Version, in the graphic.
//
// NOTES:  This function should work with either graphic-File-graphics or linked 
//      graphics, since it pulls the component type from the actual graphic.
//---------------------------------------------------------------------
UINT8 *fnGrfxGetDataNameVerPtrFromGraphic( const UINT8 *pMyGraphic )
{
    UINT8 * myDataName = NULL_PTR;
#ifndef JANUS
    BOOL    fStat;
    char *  mySDRptr;
#endif

    if( pMyGraphic != NULL_PTR )
    {
#ifndef JANUS
        fStat = fnFlashGetGCsignedData( pMyGraphic, &mySDRptr );
        if( fStat ) 
            myDataName = fnGetHashSDRfieldPtr( SDR_DATA_NAME, mySDRptr );
#else
        (void)fnFlashGetGraphicMemberAddr( (void *)pMyGraphic, GRAPHIC_DATA_NAME_REV, (void**)&myDataName );
#endif        
    }

    return( myDataName );    
}

// ********************************************************************************
// FUNCTION NAME: fnGrfxCopyDataNameVerFromGraphic()                                
// PURPOSE:     
//      Copies the DataName, and maybe Ver, of a graphic to the destination.   
// INPUTS:      
//      pMyGraphic - Pointer to the graphic, WITHOUT the link header.
//      pDestDataNameVer -  Pointer to buffer that is long enough for the 
//                          requested data (8 or 12 bytes.)
//      fWithVer - BOOL - IF TRUE, copy dataName AND version (12 bytes total)
//                      if FALSE, only copy dataName (8 bytes).
// OUTPUTS:     
//      Returns BOOL:   TRUE    if the copy was performed.
//                      FALSE   if pMyGraphic is a NULL_PTR or can't get the 
//                              dataNamePtr for some reason.
// NOTES:  
//  1. This function should work with either graphic-File-graphics or linked 
//      graphics, since the subroutine that it calls pulls the component type 
//      from the actual graphic.
// HISTORY:
//  2009.06.11  Clarisa Bellamy - LINT: move the memcpy into the conditional
//                      area so that the check for a valid source pointer has 
//                      been done directly.
// AUTHOR:      CBellamy
//---------------------------------------------------------------------
BOOL fnGrfxCopyDataNameVerFromGraphic( const UINT8 *pMyGraphic, UINT8 *pDestDataNameVer, BOOL fWithVer )
{
    UINT8 * pDataName = NULL_PTR;
    BOOL    bRetval = FALSE;
    UINT16  wCopySize = GRFX_DATANAME_NOVER_LEN;

    // Decide how many bytes the caller wants to get.
    if( fWithVer == GRFX_DATANAME_WITHVER )
        wCopySize = GRFX_DATANAME_ANDVER_LEN;

    // Set pointer to point to the dataname in the graphic.
    pDataName = fnGrfxGetDataNameVerPtrFromGraphic( pMyGraphic );
    if( pDataName != NULL_PTR )
    {
        bRetval = TRUE;
        // If the caller's pointer is legitimate, copy to the destination.
        if( pDestDataNameVer != NULL_PTR )
    {
        memcpy( pDestDataNameVer, pDataName, wCopySize );           
    }    
    }

    return( bRetval );    
}

// ********************************************************************************
// FUNCTION NAME: fnGrfxGetUniNamePtr()                             
// PURPOSE:     Given a pointer to the link header of a graphic, calculate and 
//          return the pointer to the Unicode Name from the graphic.    
//
// AUTHOR:      CBellamy
// INPUTS:      LnkPtr - Pointer to the graphic, starting with the link header. 
// OUTPUTS:     Return the pointer to the unicode name in the graphic.
//          It should be on an even boundary, but it is not guaranteed.
//
// NOTES:  This function should work with either graphic-File graphics or linked 
//      graphics, since the uninames are in the "graphics" part of the graphic.
//
//  The return type is a pointer to void because the name may not be on 
//   an even boundary. 
//---------------------------------------------------------------------
void *fnGrfxGetUniNamePtr( const UINT8 *LnkPtr, UINT8 language )
{
    UINT8 * GrfxPtr = NULL_PTR;
    UINT8 * pUnicodeByte = NULL_PTR;
    UINT8   bNumberOfNames;
    UINT8   bThisLang = 0;


    // If language is out of range, default to the FIRST language.
    if( fnFlashGetNumLanguages() <= language )
        language = 0;

    if( LnkPtr != NULL_PTR )
    {
        GrfxPtr = fnGrfxGetGraphic( LnkPtr );
        // number of unicode names in graphic
        (void)fnFlashGetGCnamesCnt( GrfxPtr, &bNumberOfNames );
        // if at least one name then transfer it to Destination Unicode String.
        if( bNumberOfNames )
        {
            (void)fnFlashGetGCunikodes( GrfxPtr, &pUnicodeByte );
   
        // At this point, pUnicodeByte points to the block of Unicode strings for all
        // of the languages stored in flash.

            // If we don't have enough languages in the graphic, use the first language.
            if( bNumberOfNames <= language )
                language = 0;

            // Adjust the pointer to the first record to point to actual 
            //  string within the record.  Each unicode string in the block
            //  starts with 2 bytes of "ID" (garbage: 0xFF, 0x00).
            pUnicodeByte += SCREEN_DESC_UNICODE_OFFSET;

            // If caller did not ask for language 0, then we must walk through the
            //  unichars to find the end of the current name.  Keep doing it until
            //  the name is found in the requested language.
            // There is a chance that the unicodes may not be on an even boundary, 
            //   so we must look byte by byte..
            while( bThisLang < language )
            {
                if( (*pUnicodeByte ==0)  &&  (*(pUnicodeByte+1) == 0) )
                {
                    // End of this string.
                    bThisLang++;
                    // Skip the null terminator, then the extra bytes between names, and 
                    // point to the next name.
                    pUnicodeByte += 2;
                    pUnicodeByte += SCREEN_DESC_UNICODE_OFFSET;
                }
                else
                    // Check the next unichar.
                    pUnicodeByte += 2;
            }
        }
    }
    
    return( pUnicodeByte );
}

// ********************************************************************************
// FUNCTION NAME: fnGrfxCpyUniName()                                
// PURPOSE:     Given a pointer to the link header of a graphic, copy the  
//          Unicode Name from the graphic into the destination buffer.
//
// AUTHOR:      CBellamy
// INPUTS:
//      pDest - Pointer to destination unichar buffer.   
//      LnkPtr - Pointer to the graphic, starting with the link header. 
//      language - 8-bit value indicating which language to get the name in.
//              language/pcn context.
//      wMaxSize - This is the max length of unichar string we can store in 
//              the destination.
// OUTPUTS:     Returns the length of the string OR if the string is longer
//              than MaxSize, returns MaxSize.
//
// NOTES:  This function should work with either graphic-File graphics or linked 
//      graphics, since the uninames are in the "graphics" part of the graphic.
//
//  This will only copy the first wMaxSize unicodes to the buffer.
//
//   The buffer must be AT LEAST (wMaxLen +1) * 2 byte long!
//
//---------------------------------------------------------------------
UINT16 fnGrfxCpyUniName( UNICHAR *pDest, const UINT8 *LnkPtr, UINT8 language, UINT16 wMaxLen )
{
    UINT16      wUniLen;
    UINT8 *     pUniName;  // Might NOT be on an even boundary.

    if( wMaxLen == 0 )
        return( 0 );

    pUniName = fnGrfxGetUniNamePtr( LnkPtr, language );
    wUniLen = fnUnicodeNCopy( pDest, pUniName, wMaxLen );
    return( wUniLen );
}





// **********************************************************************
// FUNCTION NAME:   fnLGLGetNextGraphLnkFltrd
//
// PURPOSE:     Returns the address of the NEXT link in the linked graphics
//              component list in the EMD that meets the filter requirements.
//          For now, the filter applies to the flags in the directory entries.
//          Since the linked graphics don't have directory entries, each filter 
//          is either TRUE or FALSE for ALL the linked graphics.
//          IF the current-Link pointer is a NULL_PTR, then the address of 
//          the FIRST link in the list is returned.
//                  
//          A NULL_PTR return indicates that the passed link is at the end
//          of the linked list.
//                                      
// AUTHOR:  R. Arsenault, C.Bellamy
//
// INPUTS:  pCurrentLink - Pointer to graphic (starts with link header) 
//                      in tne linked list.     
//                      If it is null, get the address of the FIRST 
//                      graphic of the linked list.
// RETURNS: Pointer to NEXT graphic in linked list, or pointer to FIRST
//          graphic in Linked list, OR NULL_PTR (if at the end of the list.)
//
//  NOTES:  Was fnFlashGetNextAnyLink.
//          A value of 0 or -1 for the offset to the next link indicates 
//          that THIS linked element is empty and is the last link in the list. 
//---------------------------------------------------------------------
static UINT8 *fnLGLGetNextGraphLnkFltrd( const UINT8 *pCurrentLink, UINT16 uwFlagTest ) 
{
    UINT8  *pNextLink;         
    UINT32  ulOffsetToNextLink;
    BOOL    fCanBeInList = TRUE;
            
    // Default to indicate the end.
    pNextLink = NULL_PTR;

    switch( uwFlagTest )
    {
        case GRFX_AUTOGRFX_ONLY:
            fCanBeInList = FALSE;  // Don't bother looking.
            break;

        default:
            break;
    }

    if( fCanBeInList ==TRUE )
    {
        if( pCurrentLink == NULL_PTR ) 
        {   // If passed in ptr is 0, then start at the beginning.
            pNextLink = (UINT8 *)fnFlashGetAddress( LINKED_IMAGES_DC );
        }
        else 
        {
            // Get offset to next link.
        	EndianAwareCopy((void*) &ulOffsetToNextLink, (void*)pCurrentLink + COMPONENT_OFFSET, sizeof(UINT32) );

            // Test offset to next link for end of file.  IF not, set linkAddr to point to next link.   
            if(    (ulOffsetToNextLink != 0) 
                && (ulOffsetToNextLink != (UINT32)(-1)) )
            {
                pNextLink = (UINT8 *)(Current_Top_Of_Flash) + ulOffsetToNextLink;       
            }
        }
    }

    return( pNextLink );
}

// This is the unfiltered version of the function above. It is called from other modules.
UINT8 *fnLGLGetNextGraphLnk( const UINT8 *pCurrentLink ) 
{
    return( fnLGLGetNextGraphLnkFltrd( pCurrentLink, GRFX_ANY_FLAGS ) );
}


//* **********************************************************************
//
// FUNCTION NAME:   fnGrfxGetFirstLnkPtrByType
//
// PURPOSE:     Searches either starting from the start of the linked 
//              list in the EMD or starting at the first directory entry
//              in the graphics file, and finds the first entry with the 
//              matching component type.  Returns a pointer to the link 
//              header of that graphic.
//                                      
// AUTHOR:  Clarisa Bellamy Apr. 28, 2005
//
// INPUTS:  bCompType - Component type we are looking for.
//          uwFlagTest - If not zero, ignores graphics that do not pass the 
//                      flag test.  The flags do not apply to Linked graphics.
// 
// OUTPUT: Returns a pointer to the found graphic WITH the link header.

//------------------------------------------------------------------------
void *fnGrfxGetFirstLnkPtrByType( UINT8 bCompType, UINT16 uwFlagTest )
{
  UINT8 *LinkPtr = NULL_PTR;
  UINT8 *NextLinkPtr = NULL_PTR;
  UINT8 *FoundLnkPtr = NULL_PTR;  
    
    if( fnGrfxIsNonEmdCompType( bCompType ) )
    {
        FoundLnkPtr = fnGFGetFirstLnkPtrByCompType( bCompType, uwFlagTest ); 
    }
    else
    {
        // Get the start of the linked list in the EMD.
        LinkPtr = fnLGLGetNextGraphLnkFltrd( NULL_PTR, uwFlagTest );
        // IF there IS a linked list, check that the first entry is real.
        if( LinkPtr )
            NextLinkPtr = fnLGLGetNextGraphLnkFltrd( LinkPtr, uwFlagTest );
        // For each valid graphic inthe linked list...
        while( NextLinkPtr )
        {
            //  Check the component type.  Stop when the first match found.
            if( fnGrfxGetCompType( LinkPtr ) == bCompType )
            {   // Found IT!    Stop looking.
                FoundLnkPtr = LinkPtr;
                NextLinkPtr = NULL_PTR;
            }
            else
            {   // Not a match, check the next link in the list.
                LinkPtr = NextLinkPtr;
                NextLinkPtr = fnLGLGetNextGraphLnkFltrd( LinkPtr, uwFlagTest );
            }                
        }
    }

    return( FoundLnkPtr );
}

//* **********************************************************************
//
// FUNCTION NAME:   fnGrfxGetLnkPtrByGenID
//
// PURPOSE:     Searches either the linked list in the EMD or the graphics 
//          file, and finds the entry with the matching bCompType and GenID.
//                                      
// AUTHOR:  Clarisa Bellamy Apr. 28, 2005
//
// INPUTS:  bCompType - Component type we are looking for, UINT8.
//          wGenID - Generated ID of component we are looking for, UINT16.
//          uwFlagTest - If not zero, ignores graphics that do not pass the 
//                      flag test.  The flags do not apply to Linked graphics.
// 
// OUTPUT: Returns a pointer to the found graphic WITH the link header.
//           Returns NULL_PTR if no match found.
// NOTES:
//      EMD graphics are always enabled/purchased.
//------------------------------------------------------------------------
void *fnGrfxGetLnkPtrByGenID( UINT8 bCompType, UINT16 wGenID, UINT16 uwFlagTest )
{
  UINT8 *LinkPtr = NULL_PTR;
  UINT8 *NextLinkPtr = NULL_PTR;
  UINT8 *FoundLnkPtr = NULL_PTR;  
    
    if( wGenID )
    {
        if( fnGrfxIsNonEmdCompType( bCompType ) )
        {
            FoundLnkPtr = fnGFGetLnkPtrByID( bCompType, wGenID, uwFlagTest ); 
        }
        else
        {
            // Get the start of the linked list in the EMD.
            LinkPtr = fnLGLGetNextGraphLnkFltrd( NULL_PTR, uwFlagTest );
            // IF there IS a linked list, check that the first entry is real.
            if( LinkPtr )
                NextLinkPtr = fnLGLGetNextGraphLnkFltrd( LinkPtr, uwFlagTest );
            // For each grphic in the linked list...
            while( NextLinkPtr )
            {
                // Check the component type and Generated ID, both must match.
                if(   (fnGrfxGetCompType( LinkPtr ) == bCompType) 
                   && (fnGrfxGetGenID( LinkPtr ) == wGenID) )   
                {   // Found IT!    Stop looking.
                    FoundLnkPtr = LinkPtr;
                    NextLinkPtr = NULL_PTR;
                }
                else
                {   // Not a match, check the next link in the list.
                    LinkPtr = NextLinkPtr;
                    NextLinkPtr = fnLGLGetNextGraphLnkFltrd( LinkPtr, uwFlagTest );
                }                
            }           
        }
    }

    return( FoundLnkPtr );
}


//* **********************************************************************
//
// FUNCTION NAME:   fnGrfxGetGenIdOfFirstByType
//
// PURPOSE:     Searches either starting from the start of the linked 
//      list in the EMD or starting at the first directory entry in the 
//      graphics file, and finds the first entry with the matching 
//      component type.  
//      Returns the Generated ID of that that graphic.
//                                      
// AUTHOR:  Clarisa Bellamy Apr. 28, 2005
//
// INPUTS:  bCompType - Component type we are looking for.
//          uwFlagTest - Ignores graphics that do not pass the flag test.  
//                      The flags do not apply to Linked graphics.
//                      0 means there is no test, include all matching graphics.
// 
// OUTPUT: Returns a pointer to the found graphic WITH the link header.
//  NOTES:
//      The generated ID of the first graphic found MIGHT NOT be the 
//      lowest generated ID of all graphics of that type.
//  Returns 0 if no graphics of this type pass the flag test.  
//------------------------------------------------------------------------
UINT16 fnGrfxGetGenIdOfFirstByType( UINT8 bCompType, UINT16 uwFlagTest )
{
    UINT8 * LnkPtr =  NULL_PTR;
    UINT16  wGenId = 0;

    LnkPtr = fnGrfxGetFirstLnkPtrByType( bCompType, uwFlagTest ); 
    if( LnkPtr )
        wGenId = fnGrfxGetGenID( LnkPtr );

    return( wGenId );    
}


// **********************************************************************
// FUNCTION NAME:   fnGrfxGetGrfkPtrByGenID
// PURPOSE:     
//      Finds the graphic with the matching component type and GenID,
//      and returns a pointer to the graphic WITHOUT the link header.
//                                      
// INPUTS:  bCompType - Component type we are looking for.
//          wGenID - Generated ID we are looking for.
//          uwFlagTest - Ignores graphics that do not pass the flag test.  
//                      The flags do not apply to Linked graphics.
//                      0 means there is no test, include all matching graphics.
// OUTPUT: 
//      Returns a pointer to the found graphic WITHOUT the link header.
//  
//  NOTES:  Replaced fnAIGetGraphicPtr
//------------------------------------------------------------------------
void *fnGrfxGetGrfkPtrByGenID( UINT8 bCompType, UINT16 wGenID, UINT16 uwFlagTest )
{
  UINT8 *LinkPtr;
  void  *GrfxPtr;

    // Only retrieve it if it is "purchased".
    LinkPtr = fnGrfxGetLnkPtrByGenID( bCompType, wGenID, uwFlagTest );
    GrfxPtr = fnGrfxGetGraphic( LinkPtr );
    return( GrfxPtr );            
}

//* **********************************************************************
//
// FUNCTION NAME:   fnGrfxGetFirstGrfkPtrByType
//
// PURPOSE:     Finds the first graphic with the matching component type,
//        and returns a pointer to the graphic WITHOUT the link header.
//                                      
// AUTHOR:  Clarisa Bellamy Apr. 28, 2005
//
// INPUTS:  bCompType - Component type we are looking for.
//          uwFlagTest - Ignores graphics that do not pass the flag test.  
//                      The flags do not apply to Linked graphics.
//                      0 means there is no test, include all matching graphics.
// 
// OUTPUT: Returns a pointer to the found graphic WITHOUT the link header.
//  
//  NOTES:  Replaced fnAIGetGraphicPtr
//
//------------------------------------------------------------------------
void *fnGrfxGetFirstGrfkPtrByType( UINT8 bCompType, UINT16 uwFlagTest )
{
  UINT8 *GrfxPtr = NULL_PTR;
  UINT8 *LinkPtr;

    // Get pointer to the link header of the first graphic with the
    //  matching type.  Only retrieve a "purchased" one.
    LinkPtr = fnGrfxGetFirstLnkPtrByType( bCompType, uwFlagTest );
    GrfxPtr = fnGrfxGetGraphic( LinkPtr );

    return( GrfxPtr );
}


//* **********************************************************************
//
// FUNCTION NAME:   fnGrfxGetUniNameByGenID
//
// PURPOSE:     Searches either the linked list in the EMD or the graphics 
//          file, and finds the entry with the matching bCompType and GenID.
//          If found, returns a pointer to the unicode names area
//                                      
// AUTHOR:  Clarisa Bellamy Apr. 28, 2005
//
// INPUTS:  bCompType - Component type we are looking for.
//          wGenID - Generated ID of component we are looking for.
//          uwFlagTest - Ignores graphics that do not pass the flag test.  
//                      The flags do not apply to Linked graphics.
//                      0 means there is no test, include all matching graphics.
// 
// OUTPUT: Returns a pointer to the first unicode string, which can be 
//      used on a display for selection by the user.
// NOTES:  DOES check if graphic is "purchased" or not.
//  Does not handle multiple languages.
//------------------------------------------------------------------------
void *fnGrfxGetUniNameByGenID( UINT8 bCompType, UINT16 wGenID, UINT16 uwFlagTest )
{
  UINT8 *   LinkPtr;
  void  *   pUnicode;

    LinkPtr = fnGrfxGetLnkPtrByGenID( bCompType, wGenID, uwFlagTest );
    // Just get pointer to the unicode string for the FIRST language.
    pUnicode = fnGrfxGetUniNamePtr( LinkPtr, 0 );
    
    return( pUnicode );
}

//* **********************************************************************
//
// FUNCTION NAME:   fnGrfxGetThumbnail
//
// PURPOSE:     Looks inside the graphic and returns a pointer to the start
//              of the thumbnail (bitmap) for display on the screen.  Also 
//              loads the size, if the caller requested it.
//                                      
// AUTHOR:  Clarisa Bellamy Apr. 28, 2005
//
// INPUTS:  pGraphic - Ptr to graphic, without link header.
//          pImageSize - Ptr to destination to store size of bitmap.  If this 
//                      is a NULPTR, then the caller doesn't need the size.
// 
// OUTPUT: Starting address of the thumbnail.  NULLPTR is returned if there is
//          no thumbnail.
//  If graphic is not "purchased" this returns NULL_PTR.
//------------------------------------------------------------------------
#ifndef JANUS		// only Mega has thumbnail graphics
void *fnGrfxGetThumbnail( UINT8 *LinkPtr, UINT16 *pImageSize )
{
  UINT8  *  pThumbnail = NULL_PTR;
  UINT8  *  pGraphic = NULL_PTR;
  UINT16    wTempImageSize;
  UINT16 *  pTempImgSzPtr = &wTempImageSize;
  BOOL  fStat = FALSE;

    pGraphic = fnGrfxGetGraphic( LinkPtr );
    if( pGraphic )
    {
#ifndef JANUS     
        if( fnFlashGetGCPKSzOfBitmapType( pGraphic, pTempImgSzPtr ) )
            fStat = fnFlashGetGCPKBitmapType( pGraphic, &pThumbnail );
#else
        if( fnFlashGetGraphicMember( pGraphic, BITMAPSIZE, pImageSize ) )
            fStat = fnFlashGetGraphicMemberAddr( pGraphic, BITMAP, (void **)&pTempImgSzPtr );
#endif
    }
    
    if( fStat )
    {
        // If the caller asked for the size, save it.
        if( (pThumbnail != NULL_PTR)  &&  (pImageSize != NULL_PTR) )
            *pImageSize = wTempImageSize;

        // If the size of the bitmap is zero, return a NULL_PTR.
        if( wTempImageSize == 0 )
            pThumbnail = NULL_PTR;          
    }
    
    return( pThumbnail );
}
#endif

//******************************************************************************
// FUNCTION NAME: fnPutAILinkHeader();                                         *
//                                                                             *
// PURPOSE: Copies the LinkHeader component structure to                       *
// the Graphic Component of the Image at the address specified.                *
//                                                                             *
// AUTHOR: George Monroe                                                       *
// INPUTS: ImageAddress in Flash, pointer to Component structure, flag to      *
//         swap endianess
//                                                                             *
// OUTPUTS: Graphic Component Structure                                        *
//                                                                             *
//******************************************************************************
void fnPutAILinkHeader( const struct LinkHeader *pHeader , UINT8 *LinkPtr, BOOL swap )
{

	if(swap == TRUE)
	{
		EndianAwareCopy( LinkPtr + COMPONENT_OFFSET,         // Destination
					   &pHeader->OffsetToNextGraphic,        // Source
					   sizeof(UINT32) );                     // String Length

		EndianAwareCopy( LinkPtr + COMPONENTID_OFFSET,       // Destination
					   &pHeader->GeneratedID,                // Source
					   sizeof(UINT16) );                     // String Length

		EndianAwareCopy( LinkPtr + COMPONENTSIZE_OFFSET,     // Destination
					   &pHeader->SizeOfThisLinkGraphic,      // Source
					   sizeof(UINT32) );                     // String Length
	}
	else
	{
		memcpy( LinkPtr + COMPONENT_OFFSET,           // Destination
				&pHeader->OffsetToNextGraphic,        // Source
				sizeof(UINT32) );                     // String Length

		memcpy( LinkPtr + COMPONENTID_OFFSET,         // Destination
				&pHeader->GeneratedID,                // Source
				sizeof(UINT16) );                     // String Length

		memcpy( LinkPtr + COMPONENTSIZE_OFFSET,       // Destination
				&pHeader->SizeOfThisLinkGraphic,      // Source
				sizeof(UINT32) );                     // String Length
	}

  *(LinkPtr + COMPONENTCONTEXT_OFFSET) = pHeader->Context;
  *(LinkPtr + COMPONENTFEATURE_OFFSET) = pHeader->FeatureCode;
}

/* **********************************************************************
 FUNCTION NAME: fnGetAILinkHeader();

 PURPOSE: Copies the Graphic Component of the Image at the address
          specified to the LinkHeader component structure pointer. 
                                        
 AUTHOR: Phil Jacques

 INPUTS: ImageAddress in Flash, pointer to Component structure

OUTPUTS: Graphic Component Structure
**************************************************************************/
void fnGetAILinkHeader( const void *LinkPtr, struct LinkHeader *pHeader )
{
  memcpy( &pHeader->OffsetToNextGraphic,    (char *)LinkPtr + COMPONENT_OFFSET,     sizeof(UINT32) );
  memcpy( &pHeader->SizeOfThisLinkGraphic,  (char *)LinkPtr + COMPONENTSIZE_OFFSET, sizeof(UINT32) );
  memcpy( &pHeader->GeneratedID,            (char *)LinkPtr + COMPONENTID_OFFSET,   sizeof(UINT16) );
  pHeader->Context = *((UINT8 *)LinkPtr + COMPONENTCONTEXT_OFFSET);
  pHeader->FeatureCode = *((UINT8 *)LinkPtr + COMPONENTFEATURE_OFFSET);
  return;
}

/* **********************************************************************

 FUNCTION NAME: fnGetAIGraphicHeader();

 PURPOSE: Copies the Graphic Component of the Image at the address
          specified to the GraphicHeader component structure pointer. 
                                        
 AUTHOR: George Monroe

 INPUTS: ImageAddress in Flash, pointer to Graphic Component structure

OUTPUTS: Graphic Component Structure Header
**************************************************************************/
#ifndef JANUS
void *fnGetAIGraphicHeader (void *gcPtr, struct gcHeader *gcHeader)
{
  memcpy(&gcHeader->ComponentSum, ( char *) gcPtr + COMPONENTSUM_OFF , SZ_COMPONENTSUM) ;
  memcpy(&gcHeader->ComponentSize, ( char *) gcPtr + COMPONENTSIZE_OFF , SZ_COMPONENTSIZE);
  memcpy(&gcHeader->DownloadSize, ( char *) gcPtr + DOWNLOADSIZE_OFF , SZ_DOWNLOADSIZE );
  memcpy(&gcHeader->DownloadAddr, ( char *) gcPtr + DOWNLOADADDR_OFF , SZ_DOWNLOADADDR );
  memcpy(&gcHeader->Columns, ( char *) gcPtr + COLUMNS_OFF , SZ_COLUMNS );
  memcpy(&gcHeader->Hash, ( char *) gcPtr + HASH_OFF , SZ_HASH );
  memcpy(&gcHeader->Signature, ( char *) gcPtr + SIGNATURE_OFF , SZ_SIGNATURE );
  memcpy(&gcHeader->DlChecksum, ( char *) gcPtr + DLCHECKSUM_OFF , SZ_DLCHECKSUM );
  memcpy(&gcHeader->Unikodes, ( char *) gcPtr + UNIKODES_OFF , SZ_UNIKODES );
  memcpy(&gcHeader->Fonts, ( char *) gcPtr + FONTS_OFF , SZ_FONTS );
  memcpy(&gcHeader->Registers, ( char *) gcPtr + REGISTERS_OFF , SZ_REGISTERS );
  memcpy(&gcHeader->Dl_Reserve, ( char *) gcPtr + DL_RESERVE_OFF , SZ_DL_RESERVE );
  gcHeader->Visibility = *((char *)gcPtr + VISIBILITY_OFF);
  gcHeader->FontsCnt = *((char *)gcPtr + FONTSCNT_OFF);
  gcHeader->RegFieldCnt = *((char *)gcPtr + REGFIELDCNT_OFF);
  gcHeader->NamesCnt = *((char *)gcPtr + NAMESCNT_OFF);
  gcHeader->ComponentType = *((char *)gcPtr + COMPONENTTYPE_OFF);
  gcHeader->CurrencyType = *((char *)gcPtr + CURRENCYTYPE_OFF);
  return;
}
#endif


// **********************************************************************
// FUNCTION NAME:   fnLGLGetGenIdListByCompType
//
// PURPOSE:     Searches the EMD Linked Graphics List to find as many 
//              matches as it can.  If the pointer to the destination 
//              list is NULL_PTR, it just counts them and returns the
//              number.  Otherwise, it fills up the destination list 
//              with the Generated IDs, stopping when bMaxList entries
//              have been found and copied.
//      All graphics are "installed".
//                                      
//  AUTHOR:  C.Bellamy
//
//  INPUTS:     bCompType - byte, Component Type to look for.
//              pDestIDList - Pointer to pre-initialized list of UINT16s, to 
//                          store the found IDs in.  
//                          If pointer is NULL_PTR, jut count ID's don't copy them.
//              wMaxReturn - UINT16, max number of matches to look for.
//                      IF this is 0, and we have a NULL_PTR for pDestIDlist, 
//                      then look for and count ALL the matching entries.
//
//  OUTPUTS:    Fills the pDestIDList, if pointer is not NULL_PTR and wMaxList > 0.
//              Returns the number found.
//
// With fnGrfxGetGenIdListByCompType, Replaces fnAIGetInstalledIdList
// 
//---------------------------------------------------------------------
static UINT16 fnLGLGetGenIdListByCompType( UINT8 bCompType, UINT16 *pDestIDList, 
                                           UINT16 wMaxReturn, UINT16 uwFlagTest )
{
    UINT8 *   pThisLinkedGraphic;     // Pointer to the linkheader of the current graphic
    UINT8 *   pNextLinkedGraphic = NULL_PTR;     // Pointer to the linkheader of the next graphic.
      
    BOOL    forever = TRUE;           // Loop stopper
    UINT16  wNumFound = 0;            // Number found so far.
      
    // Start at the beginning of the linked list...
    pThisLinkedGraphic = fnLGLGetNextGraphLnkFltrd( NULL_PTR, uwFlagTest );
    // IF there IS a linked list, check that the first entry is real.
    if( pThisLinkedGraphic )
        pNextLinkedGraphic = fnLGLGetNextGraphLnkFltrd( pThisLinkedGraphic, uwFlagTest );
    
    while( forever &&  pNextLinkedGraphic )
    {
        if( fnGrfxGetCompType( pThisLinkedGraphic ) == bCompType )
        {
            if( pDestIDList )
            {
                pDestIDList[ wNumFound ] = fnGrfxGetGenID( pThisLinkedGraphic );
            }
            wNumFound++;
            
            // Check for count limiter.  If none, or we are still in bounds, increment.
            if( (wMaxReturn == 0)  ||  (wNumFound < wMaxReturn) )
                    ;// Keep looking.
            else 
                forever = FALSE;
        }

        // Check the next graphic in the linked list.
        pThisLinkedGraphic = pNextLinkedGraphic;
        pNextLinkedGraphic = fnLGLGetNextGraphLnkFltrd( pThisLinkedGraphic, uwFlagTest );
    }
   
    return( wNumFound );
}

// **********************************************************************
// FUNCTION NAME:   fnGrfxGetGenIdListByCompType
//
// PURPOSE:     Based on type, searches either the EMD Linked Graphics List 
//              or the Resident graphics (RAM copy of graphics file) to 
//              find as many matches as it can.  If the pointer to the
//              destination list is NULL_PTR, it just counts them and 
//              returns the number.  Otherwise, it fills up the destination
//              list with the Generated IDs, stopping when bMaxList entries
//              have been found and copied.
//                                      
//  AUTHOR:  C.Bellamy
//
//  INPUTS:     bCompType - byte, Component Type to look for.
//              pDestIDList - Pointer to pre-initialized list of UINT16s, to 
//                          store the found IDs in.  
//                          If pointer is NULL_PTR, jut count ID's don't copy them.
//              wMaxReturn - UINT16, max number of entries to look for.
//                      IF this is 0, and we have a NULL_PTR for pDestIDlist, 
//                      then look for and count ALL the matching entries.
//          uwFlagTest - Ignores graphics that do not pass the flag test.  
//                      The flags do not apply to Linked graphics.
//                      0 means there is no test, include all matching graphics.
//
//  OUTPUTS:    Fills the pDestIDList, if pointer is not NULL_PTR and wMaxList > 0.
//              Returns the number found.
//
// Replaces fnAIGetInstalledIdList
// 
//---------------------------------------------------------------------
static UINT16 fnGrfxGetGenIdListByCompType( UINT8 bCompType, UINT16 *pDestIDList, 
                                            UINT16 wMaxReturn, UINT16 uwFlagTest )
{
    UINT16  wNumFound = 0;            // Number found so far.

    if( fnGrfxIsNonEmdCompType( bCompType ) )
    {
        wNumFound = fnGFGetGenIDListByCompType( bCompType, pDestIDList, wMaxReturn, uwFlagTest );
    }
    else
    {
        if( uwFlagTest != GRFX_AUTOGRFX_ONLY )
        {
            // No filtering on LGL graphics yet. All EMD graphics are always enabled and not autographic.
            wNumFound = fnLGLGetGenIdListByCompType( bCompType, pDestIDList, 
                                                     wMaxReturn, uwFlagTest );            
        }
    }

    return( wNumFound );
}

// **********************************************************************
// FUNCTION NAME:   fnLGLGetListAllGraphics
//
// PURPOSE:     Searches the EMD Linked Graphics List to find as many graphics
//          as it can.  
//              A pointer arg allows the caller to ask for a list of compTypes
//          and a list of GenIDs.  If either pointer is NULL_PTR, just don't 
//          get that list. 
//          Stops counting and/or filling when bMaxReturn graphics have been 
//          found.  (counting only the results, if filtered.)
//                                      
//  AUTHOR:  C.Bellamy
//
//  INPUTS:     pDestCompTypes - Pointer to the pre-initialized list of UINT8s,
//                          place to store the compTypes of the found graphics.
//              pDestIDList - Pointer to pre-initialized list of UINT16s, to 
//                          store the found IDs in.  
//                          If pointer is NULL_PTR, jut count ID's don't copy them.
//              wMaxReturn - UINT16, max number of entries to look for.
//                      IF this is 0, and we have a NULL_PTR for pDestIDlist, 
//                      then look for and count ALL the matching entries.
//          uwFlagTest - Ignores graphics that do not pass the flag test.  
//                      The flags do not apply to Linked graphics.
//                      0 means there is no test, include all graphics.
//
//  OUTPUTS:    Fills the pDestCompTypes, pDestIDList, if pointers are not 
//              NULL_PTR.
//              Returns the number found.
//              Limited by wMaxReturn, unless wMaxReturn is 0, then no limit.
//
// 
//---------------------------------------------------------------------
static UINT16 fnLGLGetListAllGraphics( UINT8 *pDestCompTypes, UINT16 *pDestGenIDs, 
                                       UINT16 wMaxReturn, UINT16 uwFlagTest )
{
    UINT16  wNumFound = 0;            // Number found so far.
    UINT8 * pThisLinkedGraphic = NULL_PTR;
    UINT8 * pNextLinkedGraphic = NULL_PTR;
    UINT16  wGenId;
    UINT8   bCompType;
    
    // All EMD graphics are always enabled.
    // Start at the beginning of the linked list...
    pThisLinkedGraphic = fnLGLGetNextGraphLnkFltrd( NULL_PTR, uwFlagTest );
    // IF there IS a linked list, check that the first entry is real.
    if( pThisLinkedGraphic )
        pNextLinkedGraphic = fnLGLGetNextGraphLnkFltrd( pThisLinkedGraphic, uwFlagTest );
    
    while( pNextLinkedGraphic )
    {
        // If caller wants types, save it.
        if( pDestCompTypes != NULL_PTR )
        {
            bCompType = fnGrfxGetCompType( pThisLinkedGraphic );
            pDestCompTypes[ wNumFound ] = bCompType;        
        }
        // If caller wants IDs, save it.
        if( pDestGenIDs != NULL_PTR )
        {
            wGenId = fnGrfxGetGenID( pThisLinkedGraphic );
            pDestGenIDs[ wNumFound ] = wGenId;
        }

        // Increment the number found.
        wNumFound++;
        // Check for count limiter.  If none, or we are still in bounds.
        if( (wMaxReturn == 0)  ||  (wNumFound < wMaxReturn) )
        {
            // Check the next graphic in the linked list.
            pThisLinkedGraphic = pNextLinkedGraphic;
            pNextLinkedGraphic = fnLGLGetNextGraphLnkFltrd( pThisLinkedGraphic, uwFlagTest );
        }
        else // List is full, stop looking.
            pNextLinkedGraphic = NULL_PTR;
    }

    return( wNumFound );
}

// **********************************************************************
// FUNCTION NAME:   fnGrfxGetListAllGFFirst
//
// PURPOSE:     Searches both the Resident graphics (RAM copy of graphics 
//          file) and the EMD Linked Graphics List to find as many graphics
//          as it can.  An argument lets the caller filter out certain 
//          characteristics, such as autographics, or non-deletable.
//              A pointer arg allows the caller to ask for a list of compTypes
//          and a list of GenIDs.  If either pointer is NULL_PTR, just don't 
//          get that list. 
//          Stops counting and/or filling when bMaxReturn graphics have been 
//          found.  (counting only the results, if filtered.)
//                                      
//  AUTHOR:  C.Bellamy
//
//  INPUTS:     pDestCompTypes - Pointer to the pre-initialized list of UINT8s,
//                          place to store the compTypes of the found graphics.
//              pDestIDList - Pointer to pre-initialized list of UINT16s, to 
//                          store the found IDs in.  
//                          If pointer is NULL_PTR, jut count ID's don't copy them.
//              wMaxReturn - UINT16, max number of entries to look for.
//                      IF this is 0, and we have a NULL_PTR for pDestIDlist, 
//                      then look for and count ALL the matching entries.
//          uwFlagTest - Ignores graphics that do not pass the flag test.  
//                      The flags do not apply to Linked graphics.
//                      0 means there is no test, include all graphics.
//
//  OUTPUTS:    Fills the pDestCompTypes, pDestIDList, if pointers are not 
//              NULL_PTR.
//              Returns the number found.
//              Limited by wMaxReturn, unless wMaxReturn is 0, then no limit.
//
// Replaces fnAIGetInstalledIdList
// 
//---------------------------------------------------------------------
static UINT16 fnGrfxGetListAllGFFirst( UINT8 *pDestCompTypes, UINT16 *pDestGenIDs, 
                                       UINT16 wMaxReturn, UINT16 uwFlagTest )
{
    UINT16  wNumFound = 0;            // Number found so far.

    
    // Find all the graphics in the graphics file.
    wNumFound = fnGFGetGraphicsListAllTypes( pDestCompTypes, pDestGenIDs, 
                                             wMaxReturn, uwFlagTest );
    
    // Check for count limiter.  If none, or we are still in bounds...
    // Find all the graphics in the EMD linked graphics list, and concatenate.
    if( (wMaxReturn == 0)  ||  (wNumFound < wMaxReturn) )
    {
        // Update Component and GenID destination pointers.  
        // If they are NULL_PTRs, leave them be.
        if( pDestCompTypes )
            pDestCompTypes += wNumFound;
        if( pDestGenIDs )
            pDestGenIDs += wNumFound;
        // Update max return for second call.
        if( wMaxReturn )
            wMaxReturn -= wNumFound;

        wNumFound += fnLGLGetListAllGraphics( pDestCompTypes, pDestGenIDs, 
                                              wMaxReturn, uwFlagTest );
    }

    return( wNumFound );
}

// **********************************************************************
// FUNCTION NAME:   fnGrfxGetListAllEMDFirst
//
// PURPOSE:     Searches both the Resident graphics (RAM copy of graphics 
//          file) and the EMD Linked Graphics List to find as many graphics
//          as it can.  An argument lets the caller choose to include, or 
//          not include, graphics that are not "purchased".
//              A pointer arg allows the caller to ask for a list of compTypes
//          and a list of GenIDs.  If either pointer is NULL_PTR, just don't 
//          get that list. 
//          Stops counting and/or filling when bMaxReturn graphics have been 
//          found.  (of the purchased-only or purchased and non-purchased types.)
//                                      
//  AUTHOR:  C.Bellamy
//
//  INPUTS:     pDestCompTypes - Pointer to the pre-initialized list of UINT8s,
//                          place to store the compTypes of the found graphics.
//              pDestIDList - Pointer to pre-initialized list of UINT16s, to 
//                          store the found IDs in.  
//                          If pointer is NULL_PTR, jut count ID's don't copy them.
//              wMaxReturn - UINT16, max number of entries to look for.
//                      IF this is 0, and we have a NULL_PTR for pDestIDlist, 
//                      then look for and count ALL the matching entries.
//          uwFlagTest - Ignores graphics that do not pass the flag test.  
//                      The flags do not apply to Linked graphics.
//                      0 means there is no test, include all matching graphics.
//
//  OUTPUTS:    Fills the pDestCompTypes, pDestIDList, if pointers are not 
//              NULL_PTR..
//              Returns the number found.
//              Limited by wMaxReturn, unless wMaxReturn is 0, then no limit.
//
// Replaces fnAIGetInstalledIdList
// 
//---------------------------------------------------------------------
UINT16 fnGrfxGetListAllEMDFirst( UINT8 *pDestCompTypes, UINT16 *pDestGenIDs, 
                                UINT16 wMaxReturn, UINT16 uwFlagTest )
{
    UINT16  wNumFound = 0;            // Number found so far.
    
    // All LGL graphics are NOT autographic.
    if( uwFlagTest != GRFX_AUTOGRFX_ONLY )
    {
        wNumFound = fnLGLGetListAllGraphics( pDestCompTypes, pDestGenIDs, 
                                             wMaxReturn, uwFlagTest );
    }
    
    // Check for count limiter.  If none, or we are still in bounds.
    if( (wMaxReturn == 0)  ||  (wNumFound < wMaxReturn) )
    {
        // Update Component and GenID destination pointers.  
        // If they are NULL_PTRs, leave them be.
        if( pDestCompTypes )
            pDestCompTypes += wNumFound;
        if( pDestGenIDs )
            pDestGenIDs += wNumFound;
        // Update max return for second call.  If 0, leave it be.
        if( wMaxReturn )
            wMaxReturn -= wNumFound;


        wNumFound += fnGFGetGraphicsListAllTypes( pDestCompTypes, pDestGenIDs, 
                                                  wMaxReturn, uwFlagTest );
    }

    return( wNumFound );
}

//************************************************************************
//
// FUNCTION NAME:   fnGrfxGetAvailableGenID
//
// PURPOSE:     Returns the first available Generated ID for the given 
//              component type.  0 is not a valid GenID for any componnet type.
//
// AUTHOR:  Clarisa Bellamy Apr. 28, 2005
//
// INPUTS:  bCompType - 8-bit component type, defined in fwrapper.h.
// OUTPUT:  16-bit GenID that is not assigned for this compType yet.
//  NOTES:  
//          !!!   FOR NOW - Max value for GenID is 254.
// Replaced:   fnAIGetAvailabeGenID
//                                      
//------------------------------------------------------------------------
UINT16 fnGrfxGetAvailableGenID( UINT8 bCompType )
{
    UINT16  wTryThisOne = 1;
    UINT16  curIDList[ MAX_GRAPHIC_COMPONENTS ];
    UINT16  i;
    UINT16  numIDs;           // Total number of gen IDs found of the particular type.

    UINT16  wNextID = 1;      // Default to 1 if this is the first of its type.

    memset( curIDList , 0 , sizeof(curIDList) );

    numIDs = fnGrfxGetGenIdListByCompType( bCompType, curIDList, 
                                           MAX_GRAPHIC_COMPONENTS, GRFX_ANY_FLAGS );
    if( numIDs )
    {
        // If we already have some, default to 0 (none left), just in case.
        wNextID = 0;
        // Use utility to sort in increasing order
        qsort( (void *)curIDList, numIDs, sizeof(UINT16), fnGrfxCompareGenID );

        // Find the first available GenID, starting with 1
        i = 0;
        while( wTryThisOne <= GRFX_MAX_GENID )
        {
            if(( curIDList[ i ] > wTryThisOne ) ||( i >= numIDs ))
            {
                wNextID = wTryThisOne;
                break;
            }
            else if( curIDList[ i ] == wTryThisOne )
                wTryThisOne++;
            else
                i++;
        }
    }

    if( wTryThisOne >= GRFX_MAX_GENID )
        wNextID = 0;

    return( wNextID );    
}

/* **********************************************************************
// FUNCTION NAME: fnGrfxCompareGenID( void *id2 , void *id1 )
// PURPOSE: compare two generated IDs
//
// AUTHOR: George Monroe
//
// INPUTS: id2,id1 generated ID
//
//
// RETURNS:
//
// **********************************************************************/
static int fnGrfxCompareGenID( const void *idp2 , const void *idp1 )
{
  int res;
  UINT16 *id2 = (UINT16 *)idp2;
  UINT16 *id1 = (UINT16 *)idp1;

    if( *id2 < *id1 )
        res = -1;
    else
    {
        if( *id2 > *id1 )
            res = 1;
        else
            res = 0;
    }
    return( res );
}

/* **********************************************************************
// FUNCTION NAME: fnGrfxGetNextGraphicID()
// PURPOSE: returns the next available generated graphic ID
//   Get the list of all graphics in the system and sort them
// in increasing order returning one more than the highest graphic found
// AUTHOR: George Monroe
//
// INPUTS:
// RETURNS: next ID to assign
// **********************************************************************/
UINT16 fnGrfxGetNextGraphicID( void )
{
  UINT16    curIDList[ MAX_GRAPHIC_COMPONENTS ];
  UINT16    nextID = 0;
  UINT16    numIDs = 0;

    memset( curIDList, 0, sizeof(curIDList) );
    
    numIDs = fnGrfxGetListAllGFFirst( NULL_PTR, curIDList, MAX_GRAPHIC_COMPONENTS, GRFX_ANY_FLAGS );
    // Any graphics ?
    if( numIDs )
    {
        // sort in increasing order
        qsort( (void *)curIDList, numIDs, sizeof(UINT16), fnGrfxCompareGenID );
      // return the next available
      nextID = curIDList[ numIDs - 1 ] + 1;
    }
    else
    {
      // No graphics in the system so this is a good starting point
      nextID = 1;
    }
    return( nextID );
}


// **********************************************************************
// FUNCTION NAME:   fnGrfxGetNumberFltrd
//
// PURPOSE:     Based on type and filter, searches either the EMD Linked 
//          Graphics List or the Resident graphics (RAM copy of graphics 
//          file) to find as many matches as it can.  
//                                      
//  AUTHOR:  C.Bellamy  May 8, 2005
//
//  INPUTS:     
//      bCompType - byte, Component Type to look for, may be ANY_COMPONENT_TYPE.
//      uwFlagTest - word: low byte are the bits to test.
//                        high byte are the required values of the bits to test.        
//                  0 includes everything.
//
//  OUTPUTS:    Number of enabled graphics found of the particular compType 
//              and filter.
//
//  NOTES:  bCompType may indicate ANY_COMPONENT_TYPE.
// 
//---------------------------------------------------------------------
UINT16 fnGrfxGetNumberFltrd( UINT8 bCompType, UINT16 uwFlagTest )
{
    UINT16 wNumFound = 0;

    if( bCompType ==  ANY_COMPONENT_TYPE )
        wNumFound = fnGrfxGetListAllEMDFirst( NULL_PTR, NULL_PTR, 0, uwFlagTest );    

    else
        wNumFound = fnGrfxGetGenIdListByCompType( bCompType, NULL_PTR, 0, uwFlagTest );

    return( (UINT8)wNumFound );
}


/* **********************************************************************
 FUNCTION NAME: fnAISetActive

 PURPOSE: Sets the image specified by the input arguments to the current
          active image.
                                        
 AUTHOR: Phil Jacques

 INPUTS: Graphic Component Type, Component Id

 OUTPUT: SUCCESS/FAILURE
 *************************************************************************/
BOOL fnAISetActive( UINT8 bCompType , UINT16 wImageId )
{
    BOOL        fRetval;
    UINT8       ucStatus, ucIndex;
    BOOL        fEnabled = FALSE;
    UINT16      usPermitType;
    UINT16      bSelection = wImageId;


    if( !bSelection ) 
        bSelection = TURN_OFF_IMAGE;    // if caller asks for id = 0, turn of the image
  
    switch (bCompType)
    {
        case IMG_ID_AD_SLOGAN:
            fRetval = fnWriteData( BOBAMAT0, SET_CURRENT_AD_SELECTION, &bSelection );
            if( fRetval != BOB_OK )
                fnProcessSCMError();

            // Invoke the Ad selection logic in case that BOB will not get
            // a BOB_GEN_STATIC_IMAGE_REQ message when there is any disabling codition.
            fRetval = fnValidData( BOBAMAT0, BOBID_UPDATE_AND_READ_AD_SELECT, &bSelection );
            if( fRetval != BOB_OK )
                fnProcessSCMError();
        break;
      
        case IMG_ID_INSCRIPTION:
            fRetval = fnWriteData( BOBAMAT0, SET_CURRENT_INSCR_SELECT, &bSelection );
            if( fRetval != BOB_OK )
                fnProcessSCMError();

            // Invoke the inscription selection logic in case that BOB will not get
            // a BOB_GEN_STATIC_IMAGE_REQ message. This can fix GMSE00168317:
            // G900 DM475 Unable to select the inscription when the select
            // class is highlighted.
            fRetval = fnValidData( BOBAMAT0, BOBID_UPDATE_AND_READ_INSCR_SELECT, &bSelection );
            if( fRetval != BOB_OK )
                fnProcessSCMError();
        break;

        case IMG_ID_TEXT_ENTRY:
            if( IsFeatureEnabled(&fEnabled, MANUAL_TEXT_MSGS_SUPPORT, &ucStatus, &ucIndex) && fEnabled )
            {
                fRetval = fnWriteData( BOBAMAT0, SET_CURRENT_TEXTENTRY_SELECTION, &bSelection );
                if( fRetval != BOB_OK )
                    fnProcessSCMError();
            }
            else
            {
                fRetval = FAILURE; 
            }
        break;

#ifdef JANUS
        case IMG_ID_PERMIT1_PE:
        case IMG_ID_PERMIT2_PE:
        case IMG_ID_PERMIT3_PE:
            if(   IsFeatureEnabled(&fEnabled, PERMIT_MODE_SUPPORT, &ucStatus, &ucIndex) 
               && fEnabled )
            {
                // Convert the 8-bit compType to a 16-bit permit type, but clear the type
                //  if the ID is 0.
                if( (UINT8)wImageId == TURN_OFF_IMAGE_BYTE )
                    usPermitType = TURN_OFF_IMAGE_BYTE;
                else
                    usPermitType = bCompType;

                // All graphics are referenced with a type and ID, so BOB must 
                //  know the permit type also.
                fRetval = fnWriteData( BOBAMAT0, SET_CURRENT_PERMIT_TYPE_SELECTION, &usPermitType );
                // If setting the permit type was successful, set the permit ID.
                if(fRetval == BOB_OK)
                {
                    fRetval = fnWriteData( BOBAMAT0, SET_CURRENT_PERMIT_SELECTION, &bSelection );
                }

                if( fRetval != BOB_OK )
                    fnProcessSCMError();
            }
            else
            {
                fRetval = FAILURE;
            }
        break;
#endif
      
        default:
            fRetval = FAILURE;
    } // End of switch

    return( fRetval );
}

/* **********************************************************************

 FUNCTION NAME: fnAIGetActive

 PURPOSE: Gets the currently active image specified by the input
          arguments.
                                        
 AUTHOR: Phil Jacques

 INPUTS: Graphic Component Type, Component Id

 OUTPUT: SUCCESS/FAILURE
 *************************************************************************/
BOOL fnAIGetActive( UINT8 bCompType , UINT16 *pImageID)
{
#ifndef JANUS
    UINT8       bSelection;
#else
    UINT16      bSelection;
#endif

    BOOL        fRetval;

    switch( bCompType )
    {
        case IMG_ID_AD_SLOGAN:
            fRetval = fnValidData( BOBAMAT0, READ_CURRENT_AD_SELECTION, &bSelection );
            if( (UINT8)bSelection == TURN_OFF_IMAGE_BYTE )
                *pImageID = 0;
            else
                *pImageID = (UINT16)bSelection;
        break;
      
        case IMG_ID_INSCRIPTION:
            fRetval = fnValidData( BOBAMAT0, READ_CURRENT_INSCR_SELECT, &bSelection );
            if( (UINT8)bSelection == TURN_OFF_IMAGE_BYTE )
                *pImageID = 0;
            else
                *pImageID = (UINT16)bSelection;
        break;

        case IMG_ID_TEXT_ENTRY:
            fRetval = fnValidData( BOBAMAT0, READ_CURRENT_TEXTENTRY_SELECTION, &bSelection );
            if( (UINT8)bSelection == TURN_OFF_IMAGE_BYTE )
                *pImageID = 0;
            else
                *pImageID = (UINT16)bSelection;
            break;

#ifdef JANUS
        case IMG_ID_PERMIT1_PE:
        case IMG_ID_PERMIT2_PE:
        case IMG_ID_PERMIT3_PE:
            fRetval = fnValidData( BOBAMAT0, READ_CURRENT_PERMIT_SELECTION, &bSelection );
            if( (UINT8)bSelection == TURN_OFF_IMAGE_BYTE )
                *pImageID = 0;
            else
                *pImageID = (UINT16)bSelection;
        break;
#endif
      
        default:
            fRetval = FAILURE;
    }

    return( fRetval );
}


/******************************************************************************
   FUNCTION NAME: fnGrfxGetGraphicIdByTypeAndName
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Get the generated id of the graphic component whose type and 
                : unicode name match the passed parameters.
   PARAMETERS   : component type and pointer to unicode string name
   RETURN       : Graphic Generated ID, 0 if not found
   NOTES        : none
 HISTORY:
  2009.06.11 Clarisa Bellamy - Bug fix: initialize uwDirInx to 0.
******************************************************************************/
UINT16 fnGrfxGetGraphicIdByTypeAndName( UINT8 ubCompType, const UNICHAR *pUniName )
{
    UINT16  uwId = 0;
    UINT8  *LnkPtr;
    UINT16  uwDirInx = 0;

    // This function is not needed for EMD type graphics. 
    if( fnGrfxIsNonEmdCompType( ubCompType ) )
    {
        LnkPtr = fnGFGetDirInxByUniName( &uwDirInx, ubCompType, pUniName, 
                                         MAX_INSCR_NAME, GRFX_ANY_FLAGS );
        if( LnkPtr )
        {
            uwId = fnGFGetGenIDByDirInx( uwDirInx );
        }
    }
    return( uwId );
}

/******************************************************************************
   FUNCTION NAME: fnGrfxGetPermitIdAndTypeByName
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : Get the generated id and component type of the permit component 
                : whose unicode name matches the name pointed to by the passed 
                : parameter.
   PARAMETERS   : Ptr to destination of component type and pointer to unicode string name
   RETURN       : Graphic Generated ID, 0 if not found
   NOTES        : none
   HISTORY:  
   2008.05.14 Clarisa Bellamy modified from fnAIGetGraphicIdByName.
******************************************************************************/
UINT16 fnGrfxGetPermitIdAndTypeByName( UINT8 *pCompType, const UNICHAR *pUniName )
{
    UINT8  *LnkPtr;
    UINT16  uwDirInx = 0;
    UINT16  uwPermitId = 0;
    UINT8   ubPermitType;

    ubPermitType = IMG_ID_PERMIT1_PE;
    LnkPtr = fnGFGetDirInxByUniName( &uwDirInx, ubPermitType, pUniName, 
                                     MAX_PERMIT_UNICHARS, GRFX_ANY_FLAGS );
    if( LnkPtr == NULL_PTR )
    {
        ubPermitType = IMG_ID_PERMIT2_PE;
        LnkPtr = fnGFGetDirInxByUniName( &uwDirInx, ubPermitType, pUniName, 
                                         MAX_PERMIT_UNICHARS, GRFX_ANY_FLAGS );
        if( LnkPtr == NULL_PTR )
        {
            ubPermitType = IMG_ID_PERMIT3_PE;
            LnkPtr = fnGFGetDirInxByUniName( &uwDirInx, ubPermitType, pUniName, 
                                             MAX_PERMIT_UNICHARS, GRFX_ANY_FLAGS );
        }
    }
    // If we found a match, get the GenId, else set the type to TURN_OFF_IMAGE.
    if( LnkPtr )
    {
        uwPermitId = fnGFGetGenIDByDirInx( uwDirInx );
    }
    else
    {
        ubPermitType = TURN_OFF_IMAGE_BYTE;
    }

    // Save the component type, if calling function needs it.
    if( pCompType != NULL_PTR )
    {
        *pCompType = ubPermitType;
    }

    return( uwPermitId );
}

// *************************************************************************
// FUNCTION NAME: 
//         fnAIGetActiveAd
//
// DESCRIPTION:
//         This function gets the GenID of the currently selected Ad 
//          from the bobtask.  It also calls fnProcessSCMError if there is a
//          problem retrieving this info from the bobtask.
//
// INPUTS:
//         pImageID - Pointer to the Component Id.
//
// RETURNS:
//         TRUE - if the currently active Ad image ID returns successfully.
//         FALSE - if failed.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      04/24/2006    James Wang     Initial version  
//
//---------------------------------------------------------------------------
BOOL fnAIGetActiveAd (UINT16 *pImageID)
{
    BOOL fRetv = TRUE;

    if (fnAIGetActive(AD_COMP_TYPE, pImageID) != SUCCESSFUL)
    {
        fnProcessSCMError();
        fRetv = FALSE;
    }

    return fRetv;
}

// *************************************************************************
// FUNCTION NAME: 
//         fnAIGetActiveIns
//
// DESCRIPTION:
//         This function gets the GenID of the of the selected Inscription,
//          from the bobtask. It also calls fnProcessSCMError if there is a
//          problem retrieving this info from the bobtask.
//
// INPUTS:
//         pImageID - Pointer to the Component Id.
//
// RETURNS:
//         TRUE - if the currently active Inscription image ID returns successfully.
//         FALSE - if failed.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      04/24/2006    James Wang     Initial version  
//
//---------------------------------------------------------------------------
BOOL fnAIGetActiveIns (UINT16 *pImageID)
{
    BOOL fRetv = TRUE;

    if (fnAIGetActive(INSCR_COMP_TYPE, pImageID) != SUCCESSFUL)
    {
        fnProcessSCMError();
        fRetv = FALSE;
    }

    return fRetv;
}

// *************************************************************************
// FUNCTION NAME: 
//         fnAIGetActivePermit
//
// DESCRIPTION:
//         This function gets the CompType AND GenID of the selected Permit
//          from the bobtask.  It also calls fnProcessSCMError if there is a
//          problem retrieving this info from the bobtask.
//
// INPUTS: pCompType - Pointer to destination Component Type (maybe NULL_PTR if caller doesn't care.)
//         pImageID - Pointer to destination Generated Id.
//
// RETURNS:
//         TRUE - if had no problems reading the current Permit selection from bob.
//         FALSE - if failed.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//  2009.05.19  Clarisa Bellamy -  Initial version  
//
//---------------------------------------------------------------------------
BOOL fnAIGetActivePermit( UINT8 *pCompType, UINT16 *pImageID )
{
    BOOL    fRetv = TRUE;
    UINT16  wPermitType = TURN_OFF_IMAGE;
    UINT8   bCompType = TURN_OFF_IMAGE_BYTE;

    if( fnValidData(BOBAMAT0, READ_CURRENT_PERMIT_TYPE_SELECTION, &wPermitType) != BOB_OK )
    {
        fnProcessSCMError();
        fRetv = FALSE;
    }
    else if( wPermitType == TURN_OFF_IMAGE )
    {
        *pImageID = 0;
    }
    else
    {   // A Valid permit type may be selected
        bCompType = (UINT8)(wPermitType & 0x00FF);
        if( fnAIGetActive( bCompType, pImageID ) != SUCCESSFUL )
        {
            fnProcessSCMError();
            fRetv = FALSE;
        }
    }
    // If we had no bob errors, and the caller wants the CompType too, save it.
    if(   (fRetv == TRUE) 
       && (pCompType != NULL_PTR) )
    {
        *pCompType = bCompType;
    }

    return( fRetv );
}

// *************************************************************************
// FUNCTION NAME:   fnGrfxAppendSortedPermitList
//
// DESCRIPTION:     Called by fnGrfxGetSortedPermitList.
//          This function will find all permit graphics, and using the same index, 
//          put the GenID and CompType in one list, and the speedCode and the ptr 
//          to the graphic unicode name in the SORTED list.
//
// INPUTS:
//      bCompType           - The Component Type of graphic to find (IMG_ID_PERMIT1_PE, IMG_ID_PERMIT2_PE, etc.)
//      pSortedPermitInfo   - pointer to the Sorted Permit Info struct;
//      usArraySize         - Max size of the sorted list;
//      uwFlagTest          - Filter, may indicate that the list should not include
//                          autographics, or non-deletable graphics, etc.
//      langInx             - Language index.  The functions that retrieve a name 
//                          will retrieve the first language if the language 
//                          index is out of range, or if there is only 1 language
//                          in the graphic.
//  
// RETURNS:     The number of graphics in the list.
//
// WARNINGS/NOTES:  
//    Similar to the fnGrfxGetSortedList function, except that this uses a structure 
//      type that includes the Component type, because there is more than 1 type of permit.
//    This function is local.  External modules use the wrapper function fnGrfxGetSortedPermitList
//
// MODIFICATION HISTORY:
// 2008.04.29  Clarisa Bellamy - Initial version, which is a consolidation of 
//          previous functions.
//---------------------------------------------------------------------------
static UINT16 fnGrfxAppendSortedPermitList( UINT16 bCompType, T_SORTED_PERMIT_INFO *pSortedPermitInfo, 
                                            UINT16 uwArraySize, UINT16 uwFlagTest, UINT8 langInx )  
{
    UINT16  uwCount;        // The index into the destination arrays
    UINT16  uwDirInx = 0;   // Index into GF directory of match found or next one to check.
    UINT32  ulSpeedCode;    // The speed code, stored in the sorted array
    UINT8   *LnkPtr;        // Ptr to a graphic that matches the criteria: type and flags.
    void    *pUniName;      // Ptr to the name in the graphic (not necessarily on an even boundary.)
    
    uwCount = pSortedPermitInfo->uwNumFound;  // This allows us to append.
    // The code is the index into the array, plus the offset passed in.
    ulSpeedCode = uwCount + pSortedPermitInfo->uwOffset;            
    uwDirInx = 0;
    // Get the directory Index of the first match in the graphics file.
    LnkPtr = fnGFGetNextDirInxByCompType( &uwDirInx, bCompType, uwFlagTest );
    // If we have a match, and there is room in the array...
    while(   (LnkPtr != NULL_PTR)
          && (uwCount < uwArraySize) )
    {
        // Copy the UniNamePtr, SpeedCode, and Gen ID into their respective places...
        pUniName = fnGrfxGetUniNamePtr( LnkPtr, langInx );
        pSortedPermitInfo->pSortedByCode[ uwCount ].pUniName = pUniName;
        pSortedPermitInfo->pSortedByCode[ uwCount ].ulSpeedCode = ulSpeedCode;
        pSortedPermitInfo->pPermitInfo[ uwCount ].usPermitID = fnGFGetGenIDByDirInx( uwDirInx );
        pSortedPermitInfo->pPermitInfo[ uwCount ].usPermitType = bCompType;
        // Increment the count and the speedCode 
        uwCount++;
        ulSpeedCode++;      
        // Get the directory Index of the NEXT match in the graphics file.
        uwDirInx++;
        LnkPtr = fnGFGetNextDirInxByCompType( &uwDirInx, bCompType, uwFlagTest );
    }

    pSortedPermitInfo->uwNumFound = uwCount;
    return( uwCount );
}

// *************************************************************************
// FUNCTION NAME:   fnGrfxGetSortedPermitList
//
// DESCRIPTION:     The sorted list allows the user to display the permits 
//          in alphabetical order or in "numerical" order.
//          The number is called the SpeedCode.   
//          The numerical order is just the order that they currently appear 
//          in the graphics file directory, sorted by type first.
//          The SpeedCode saved is just the index into the destination plus an 
//          offset. This is done so that the calling function can use the 
//          SpeedCode as the DisplayNumber in the menu structure. 
//          The sorted list structure is used by many other modules for different
//          things, and we are improvising for the graphics module.
//          This function will find all permit graphics, and using the same index, 
//          put the GenID and CompType in one list, and the speedCode and the ptr 
//          to the graphic unicode name in the SORTED list.
//          Most of the work is done by the subroutine fnGrfxAppendSortedPermitList,
//          which finds all the graphcis of a single type, and appends them to
//          the current lists. 
//
// INPUTS:
//      pSortedPermitInfo   - pointer to the Sorted Permit Info struct;
//      usArraySize         - Max size of the sorted list;
//      uwFlagTest          - Filter, may indicate that the list should not include
//                          autographics, or non-deletable graphics, etc.
//      langInx             - Language index.  The functions that retrieve a name 
//                          will retrieve the first language if the language 
//                          index is out of range, or if there is only 1 language
//                          in the graphic.
//
// RETURNS:     The number of matching graphics found/listed.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
// 2008.05.15  Clarisa Bellamy - Initial version.
//---------------------------------------------------------------------------
UINT16 fnGrfxGetSortedPermitList( T_SORTED_PERMIT_INFO *pSortedPermitInfo, 
                                  UINT16 uwArraySize, UINT16 uwFlagTest, UINT8 langInx )  
{
    UINT16  uwCount = 0;    // The number of matches we have found (and the index into the destination arrays.)
    
    pSortedPermitInfo->uwNumFound = 0;
     
    uwCount = fnGrfxAppendSortedPermitList( IMG_ID_PERMIT1_PE, pSortedPermitInfo, 
                                            uwArraySize, uwFlagTest, langInx );            
    uwCount = fnGrfxAppendSortedPermitList( IMG_ID_PERMIT2_PE, pSortedPermitInfo, 
                                            uwArraySize, uwFlagTest, langInx );            
    uwCount = fnGrfxAppendSortedPermitList( IMG_ID_PERMIT3_PE, pSortedPermitInfo, 
                                            uwArraySize, uwFlagTest, langInx );            
    return( uwCount );
}

// *************************************************************************
// FUNCTION NAME:   fnGrfxGetSortedList
//
// DESCRIPTION:     The sorted list allows the user to display the ads or 
//          inscriptions in alphabetical order or in "numerical" order.
//          The number is called the SpeedCode.   
//          The numerical order is just the order that they currently appear 
//          in the graphics file directory.   The SpeedCode saved is just the 
//          index into the destination plus an offset. This is done so that 
//          the calling function can use the SpeedCode as the DisplayNumber in
//          the menu structure. 
//          The sorted list structure is used by many other modules for different
//          things, and we are improvising for the graphics module.
//          This function will go through, find all graphics of a particular
//          type, and using the same index, put the GenID in one list, and the 
//          speedCode and the ptr to the graphic unicode name in the SORTED list.
//
// INPUTS:
//      bCompType       - The type of graphics to find (Ads, or Inscriptins, or...)
//      pGenIDList      - Pointer to destination Gen ID List.
//      offset          - Value to add to the index to calculate the "number"
//      pSortedGrfxInfo - pointer to the Sorted Grahpics Info struct;
//      usArraySize     -   Max size of the sorted list;
//      uwFlagTest      - Filter, may indicate that the list should not include
//                          autographics, or non-deletable graphics, etc.
//      langInx         - Language index.  The functions that retrieve a name 
//                          will retrieve the first language if the language 
//                          index is out of range, or if there is only 1 language
//                          in the graphic.
//  
//
// RETURNS:     The number of matching graphics found/listed.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
// 2008.04.29  Clarisa Bellamy - Initial version, which is a consolidation of 
//          previous functions.
//---------------------------------------------------------------------------
UINT16 fnGrfxGetSortedList( UINT8 bCompType, T_SORTED_GRFX_INFO *pSortedGrfxInfo, 
                            UINT16 uwArraySize, UINT16 uwFlagTest, UINT8 langInx )  
{
    UINT16  uwCount = 0;    // The number of matches we have found (and the index into the destination arrays.)
    UINT16  uwDirInx = 0;   // The directory entry to check.
    UINT32  ulSpeedCode = 0;     // The speed code, stored in the sorted array
    UINT8   *LnkPtr;        // Ptr to a graphic that matches the criteria: type and flags.
    void    *pUniName;      // Ptr to the name in the graphic (not necessarily on an even boundary.)
    
    // The code is the index into the array, plus the offset passed in.
    ulSpeedCode = uwCount + pSortedGrfxInfo->uwOffset;            
    // Get the directory Index of the first match in the graphics file.
    LnkPtr = fnGFGetNextDirInxByCompType( &uwDirInx, bCompType, uwFlagTest );
    // If we have a match, and there is room in the array...
    while(   (LnkPtr != NULL_PTR)
          && (uwCount < uwArraySize) )
    {
        // Copy the UniNamePtr, SpeedCode, and Gen ID into their respective places...
        pUniName = fnGrfxGetUniNamePtr( LnkPtr, langInx );
        pSortedGrfxInfo->pSortedByCode[ uwCount ].pUniName = pUniName;
        pSortedGrfxInfo->pSortedByCode[ uwCount ].ulSpeedCode = ulSpeedCode;
        pSortedGrfxInfo->pGenIDs[ uwCount ] = fnGFGetGenIDByDirInx( uwDirInx );
        // Increment the count and th speedCode 
        uwCount++;
        ulSpeedCode++;      
        // Get the directory Index of the NEXT match in the graphics file.
        uwDirInx++;
        LnkPtr = fnGFGetNextDirInxByCompType( &uwDirInx, bCompType, uwFlagTest );
    }
    pSortedGrfxInfo->uwNumFound = uwCount;
    
    return( uwCount );
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnAIGetSelectedName
//
// DESCRIPTION:
//         This function uses fnAIGetActive to get the currently selected 
//          Ad, Inscription, Permit, etc. from bob, then writes the GenID
//          into the caller's destination.  It then copies the uniname
//          of that graphic into the caller's unistring destination.  
//          If there is no graphic selected, then it writes an empty unistring
//          to the destination, and sets the GenID to 0.  
//          If there is a selection, but the uniname is empty, then it writes "*"
//          to the destination unistring.  In all cases, it also writes the length
//          of the unistring that was copied or written to the destination.
//
// INPUTS:
//      bCompType - Component Type of which to get the current selection.
//      bLanguage - If the graphic has uninames in multiple languages, this
//                  indicates which language we want.
//      pName - Pointer to unistring buffer to write the name to.  This must
//              be on an even boundary.
//      pImageID - Destination location to store the GenId of the selected component.
//      uwLen - Number of unichars the destination buffer can hold. This should
//              NOT count the null-terminator that will be added at the end by 
//              this function.
// OUTPUTS:
//    Returns FALSE: 
//      bobtask request failed.  Nothing is set.
//    Returns TRUE:  
//          If no graphic of that type is selected, sets ImageID to 0, and
//            length to 0, and sets pName to an empty unistring.
//          If a graphic is selected, set the ImageID to the genID of the 
//              selected graphic.
//              If the selected graphic's name is blank (empty unistring), set
//                  pName to "*", set length to 1.              
//              IF name is fine, copy the first MAX_INSCR_NAME unichars of the 
//                  uniname into pName, set the length to the shorter of the 
//                  actual length or MAX_INSCR_NAME.
//
// WARNINGS/NOTES:  
//   Even though the uniname in the graphci may not be on an even boundary, the 
//   destination unistring, pName, must be on an even boundary.
//
// MODIFICATION HISTORY:
//  2008.05.13  Clarisa Bellamy - Modified to be more generic, and use the existing
//                  graphics subroutines. 
//      06/04/2007    Joey Cui       Get rid of the hard-coded string "None"
//      04/24/2006    James Wang     Initial version  
//
// *************************************************************************/
BOOL fnAIGetSelectedName( UINT8 bCompType, UINT8 bLanguage, UINT16 *pName, 
                        UINT16 *pImageID, UINT16 *uwLen )
{
    BOOL    fRetval = TRUE;
    UINT8   *LnkPtr;        // Ptr to the currently selected graphic of type bCompType.
    UINT16  uwFlagTest = GRFX_ANY_FLAGS; 

    // Get the graphic that is currently selected from Bob.
    if( fnAIGetActive(bCompType, pImageID) != SUCCESSFUL )
    {
        fnProcessSCMError();
        fRetval = FAILURE;
    }
    else
    {
        // If a component is selected, copy the first MAX_INSCR_NAME unichars 
        //  of the uniName into the destination.
        if(   (*pImageID != 0)
           && (*pImageID != TURN_OFF_IMAGE) ) 
        {
             // Get a ptr to the graphic-with-link-header
             LnkPtr = fnGFGetLnkPtrByID( bCompType, *pImageID, uwFlagTest );
            
            if( LnkPtr != NULL_PTR )
            {
                *uwLen = fnGrfxCpyUniName( pName, LnkPtr, bLanguage, MAX_INSCR_NAME );
                // We have a graphic without a name.  This is not good, and so
                //  we'll display a star to indicate that it is not correct.
                if( *uwLen == 0 )
                {
                    *pName = (UINT16)'*';
                    pName[1] = 0;
                    *uwLen = 1;                    
                }
            }
            else // A graphic is selected, but cannot be found.  This is an error.
            {
                // De-select the graphic and proceed as if nothing was selected
                (void)fnAISetActive( bCompType, *pImageID );    
                *pName = 0;
                *uwLen = 0;
                *pImageID = 0;
            }
        }
        else // If none is selected, clear the name.
        {
            *pImageID = 0;
            *pName = 0;
            *uwLen = 0;
        }
    }
    return( fRetval );
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnAIGetActiveTxt
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets the value 
//         of the selected Text's Id.
//
// INPUTS:
//         pTxtID - Pointer to the active Text ID.
//
// RETURNS:
//         TRUE - if the currently active Text ID returns successfully.
//         FALSE - if failed.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/18/2006    James Wang     Initial version  
//
// *************************************************************************/
BOOL fnAIGetActiveTxt(UINT16 *pTxtID)
{
    BOOL fRetv = TRUE;

    if (fnAIGetActive(TEXT_COMP_TYPE, pTxtID) != SUCCESSFUL)
    {
        fnProcessSCMError();
        fRetv = FALSE;
    }

    return fRetv;
}


// **********************************************************************
// FUNCTION NAME:   fnGrfxCheckDataNameVer
//
// PURPOSE:     Compare the DataNameVer in the graphic with the Reference
//          dataNameVer pointed to by second argument.  If they are the 
//          same, return TRUE, else FALSE.
//                                      
//  AUTHOR:  C.Bellamy  May 8, 2005
//
//  INPUTS:     pLnkPtr - Ptr to link header of graphic.
//              pRefDataNameVer - Pointer to dataName and Version array to match.
//
//  OUTPUTS:    TRUE if there is a match between the DataNameVer and the 
//          one in the graphic.
//
//  NOTES:  None
// 
//---------------------------------------------------------------------
BOOL fnGrfxCheckDataNameVer( const UINT8 *LnkPtr, const UINT8 *pRefDataNameVer, UINT8 bLen )
{
    BOOL    retval = FALSE;
    UINT8 * pMyGraphic;
    UINT8 * pThisDataNameVer;

    if( LnkPtr )
    {
        pMyGraphic = fnGrfxGetGraphic( LnkPtr );
        pThisDataNameVer = fnGrfxGetDataNameVerPtrFromGraphic( pMyGraphic );
        if( pThisDataNameVer )
        {
            if( memcmp( pRefDataNameVer, pThisDataNameVer, bLen ) == 0 )
                // Found it!
                retval = TRUE;        
        }
    }
    
    return( retval );    
}

// **********************************************************************
// FUNCTION NAME:   fnGrfxFindByDataNameVer
//
// PURPOSE:     Based on type, searches either the EMD Linked Graphics List 
//              or the Resident graphics (RAM copy of graphics file) or both
//              (if type is ANY) to find a match with the reference DataNameVer 
//              array.  This can now find a partial match.
//      If a match is found, return pointer to graphic, with link header.
//      If no match found, return NULL_PTR.
//                                      
//  AUTHOR:  C.Bellamy  May 8, 2005
//      C.Bellamy - Modified to allow finding match to first bLen chars of 
//                  the DataNameVer array.  Will only find the FIRST match.
//
//  INPUTS:     bCompType - byte, Component Type to look for, may be ANY_COMPONENT_TYPE.
//              pRefDataNameVer - Pointer to dataName and Version array to match.
//              bLen - Number of UINT8s in the reference DataNameVer to check for a match.
//
//  OUTPUTS:    LnkPtr - if match found, return a pointer to the link header
//                  of the matching graphic.
//
//  NOTES:  bCompType may indicate ANY_COMPONENT_TYPE.
//    Does NOT check the "purchased" bit.
//---------------------------------------------------------------------
UINT8 * fnGrfxFindByDataNameVer( UINT8 bCompType, const UINT8 *pRefDataNameVer, UINT8 bLen )
{
    UINT16  wDirInx = 0;
    UINT8 * LnkPtr = NULL_PTR;
    UINT8 * pNextLinkedGraphic = NULL_PTR;


    // Start with the graphics file.. 
    if( (bCompType == ANY_COMPONENT_TYPE)  || fnGrfxIsNonEmdCompType( bCompType ) )
    {
        // Gets the next entry with matching type, and dataNameVer, whether enabled or not.
        wDirInx = 0;
        LnkPtr = fnGFGetDirInxByDataNameVer( &wDirInx, bCompType, pRefDataNameVer, 
                                             bLen, GRFX_ANY_FLAGS );
        if( LnkPtr != NULL_PTR )
        {
                // Found it!
                return( LnkPtr );                
        }        
    }

    // If no match found in Graphics file, check EMD...
    // This func returns FALSE for bCompType = ANY_COMPONENT_TYPE.
    if( fnGrfxIsNonEmdCompType( bCompType ) == FALSE )
    {
        // Start at the beginning of the linked list...
        LnkPtr = fnLGLGetNextGraphLnkFltrd( NULL_PTR, GRFX_ANY_FLAGS );
        // IF there IS a linked list, check that the first entry is real.
        if( LnkPtr )
            pNextLinkedGraphic = fnLGLGetNextGraphLnkFltrd( LnkPtr, GRFX_ANY_FLAGS );
    
        while( pNextLinkedGraphic )
        {
            // For each graphic of this comptype in the graphics file... 
            // check the DataNameVer
            if( fnGrfxCheckDataNameVer( LnkPtr, pRefDataNameVer, bLen ) )
            {
                // Found it!
                return( LnkPtr );                
            }
            // Check Next entry in Graphics file directory.
            LnkPtr = pNextLinkedGraphic;
            pNextLinkedGraphic = fnLGLGetNextGraphLnkFltrd( LnkPtr, GRFX_ANY_FLAGS );            
        }
    }
    // If we get here, it is because we didn't find a match.
    return( NULL_PTR );
}


// **********************************************************************
// FUNCTION NAME:   fnGrfxDoWeHaveThisGraphic
//
// PURPOSE:     Compares the data name and rev in the SDR of the 
//      incoming graphic to the data name and rev in the sdr of all the 
//      graphics stored in the graphics file and/or the EMD. 
//                  
//  AUTHOR:  C.Bellamy  May 8, 2005
//
//  INPUTS:     bCompType - byte, Component Type to look for, may be ANY_COMPONENT_TYPE.
//              LnkPtr - Pointer linkHeader of graphic to compare with. 
//
//  OUTPUTS:    Returns TRUE if an exact match is found and false otherwise.
//
//  NOTES:  This is to replace fnIsThisArtInstalled, which does repetative work
//      instead of calling existing routines.
// 
//---------------------------------------------------------------------
BOOL fnGrfxDoWeHaveThisGraphic( UINT8 bCompType, const UINT8 *pMyGraphic ) 
{
  UINT8 *   pMyDataNameVer;
  UINT8 *   pFoundLnkPtr;
  BOOL      retval = FALSE;

    //Get pointer to reference data name and ver.
    pMyDataNameVer = fnGrfxGetDataNameVerPtrFromGraphic( pMyGraphic  );
    pFoundLnkPtr = fnGrfxFindByDataNameVer( bCompType, pMyDataNameVer, GRFX_DATANAME_ANDVER_LEN );

    if( pFoundLnkPtr != NULL_PTR )
        retval = TRUE;
    
    return( retval );
}

// ********************************************************************************
// FUNCTION NAME: fnGrfxMakeDataNameVerIntoAscii()                             
// PURPOSE:     Converts the binary data in the graphic DataNameVersion to a
//              null-terminated ASCII string, containing either the DataName or 
//              the DataVersion.
//
// AUTHOR:      CBellamy
// INPUTS:      
//      pData - Pointer to the DataNameVer binary array.
//      bConvert - Indicates whether to convert the DataName or the DataVersion.
//      pDest - Pointer to destination string at least 17 bytes long.
// OUTPUTS:     
//      Fills the destination array with either a 16-char or 8-char NULL-TERMINATED 
//      string representation of the DataName or DataVersion.
//  Return:     The length of the string.
//
// NOTES:
// HISTORY:
//---------------------------------------------------------------------
UINT16 fnGrfxMakeDataNameVerIntoAscii( UINT8 *pData, UINT8 bConvert, char *pDest )
{
    UINT16   uwLen = 0;
    
    switch( bConvert )
    {
        case GRFXDATANAMEANDVERSION:      // Convert the first 12 bytes (DataNameAndVer)
            uwLen = GRFX_DATANAME_ANDVER_LEN;
            break;

        case GRFXDATANAME:      // Convert the first 8 bytes (DataName)
            uwLen = GRFX_DATANAME_NOVER_LEN;
            break;

        case GRFXDATAVERSION:   // Convert the last 4 bytes (DataVersion)
            pData += GRFX_DATANAME_NOVER_LEN;
            uwLen = GRFX_DATAVERSION_LEN;
            break;

        default:
            *pDest = 0;
            break;
    }

    if( uwLen )
    {
        // This converts a binary array to an ASCII string, null-terminated.
        (void)fnPleaseConvertThis2Ascii( (char *)pData, uwLen, PBCD_TO_STRING, pDest );
    }
    // The string is twice as long as the binary source.
    return( uwLen *2 );
}


