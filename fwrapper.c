/************************************************************************
    PROJECT:        Horizon CSD
    MODULE NAME:    FWRAPPER.C
       
    DESCRIPTION:    
            Wrapper functions for accessing flash data.
*
*	Fake parameters go in SubstPCNParameters table.

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
 REVISION HISTORY:
 17-Apr-18 sa002pe on FPHX 02.12 shelton branch
	Making sure everything for German Entgelt Bezahlt is commented out.
 
*************************************************************************/

#define SCREEN_GENERATOR_411


#include <string.h>
#include <stdlib.h>

#include "aiservic.h"       // fnGrfxGetFirstLnkPtrByType()
#include "bob.h"
#include "bobutils.h"
#include "datdict.h"
#include "dcapi.h"          //
#include "errcode.h"
#include "fcntl.h"
#include "fnames.h"
#include "fstub.h"
#include "fwrapper.h"
#include "global.h"
//#include "grapfile.c"
#include "oierrhnd.h"   // fnProcessSCMError
#include "rateadpt.h"
#include "rateadpt.h"   // fnIsDCapEngineAvailable()
#include "utils.h"      // fnBinFive2Double()
#include "version.h"
#include "glob2bob.h"
#include "report1.h"
#include "oit.h"
#include "hal.h"
#include "flashutil.h"

#include "dir_defs.h"
#include "pcdisk.h"
#include "zlib.h"
#include "megaemd.h"
#include "clock.h"
#include "oifpmain.h"



#ifdef JANUS
char transferBuff[1024];
#endif

void set_RTC();

FILE *fp_log = NULL;

#define NO_SECS_ADJUST 0
#define PLUS_1_SEC     1

#define MAX_FILENAME_SIZE 256
#define LOG_PATH  "A:\\logs\\BaseInstall.Log"
#define DEFAULT_LOG_DATE "2017-01-01T00:00:00"

#define CONV_2_ASCII_DEC_CHARS_TO_INT(str,offset) ((str[offset]-'0')*10 + (str[offset+1]-'0'))

// It is assumed UpdateBaseInstallLog() will not be called concurrently - it is not re-entrant.
static char sz_starting_timestamp[sizeof(DEFAULT_LOG_DATE)+1];
static char install_str[700];

uint8_t *pEMD_RAM = NULL;
uint32_t emdRamSize = 0;
extern BOOL RamEMDPresent;

VOID fnFilePath(CHAR* outPath, CHAR* path, CHAR* fileName);
extern BOOL validSysClock(void);

uint8_t* GetEmdRam(void)
{
	return pEMD_RAM;
}

uint32_t GetEmdRamSize(void)
{
	return emdRamSize;
}





/* ----------------------------------------------------------------------------------------------------[update_RTC]-- */
static void update_RTC(char *log_time_stamp, int seconds_to_adjust)
{
    DATETIME dt;

    // sample log_time_stamp: 2017_03_18_17_53_29

    dt.bCentury  = CONV_2_ASCII_DEC_CHARS_TO_INT(log_time_stamp,0);  /* The Century */
    dt.bYear     = CONV_2_ASCII_DEC_CHARS_TO_INT(log_time_stamp,2);  /* The Year    */
    dt.bMonth    = CONV_2_ASCII_DEC_CHARS_TO_INT(log_time_stamp,5);  /* The Month   */
    dt.bDay      = CONV_2_ASCII_DEC_CHARS_TO_INT(log_time_stamp,8);  /* The day     */
    dt.bHour     = CONV_2_ASCII_DEC_CHARS_TO_INT(log_time_stamp,11); /* The current hour */
    dt.bMinutes  = CONV_2_ASCII_DEC_CHARS_TO_INT(log_time_stamp,14); /* The Current minute */
    dt.bSeconds  = CONV_2_ASCII_DEC_CHARS_TO_INT(log_time_stamp,17); /* Got a second? */
    dt.bDayOfWeek= 1; // this must be valid (1 - 7) for RTC to be set properly

    if (seconds_to_adjust != 0)
    {
        int32_t tot_secs = (dt.bHour * 60 + dt.bMinutes) * 60 + dt.bSeconds;
        int     days_to_adjust = 0;

        tot_secs += seconds_to_adjust;

        while(tot_secs >= 86400)
        {
            days_to_adjust++;
            tot_secs -= 86400;
        }

        while(tot_secs < 0)
        {
            days_to_adjust--;
            tot_secs += 86400;
        }

        dt.bSeconds = tot_secs % 60;
        tot_secs = tot_secs / 60;
        dt.bMinutes = tot_secs % 60;
        tot_secs = tot_secs / 60;
        dt.bHour = tot_secs % 24;

        if (days_to_adjust != 0)
        {
            long jd = MdyToJulian(&dt);
            jd += days_to_adjust;
            JulianToMdy(jd, &dt);
        }
    }
    HALSetRTC(&dt);
}

/* -------------------------------------------------------------------------------------[get_install_log_timestamp]-- */
char *get_install_log_timestamp(char *timestamp_str)
{
    static char sz_str[sizeof(DEFAULT_LOG_DATE)+1];
    DATETIME dt;

    HALGetRTC(&dt);

    dt.bCentury = 20;  /* because the RTC doesn't have a centuries register */

    if (timestamp_str == NULL)
        timestamp_str = sz_str;
    sprintf(timestamp_str, "%02d%02d-%02d-%02dT%02d:%02d:%02d",
                            dt.bCentury,
                            dt.bYear,
                            dt.bMonth,
                            dt.bDay,
                            dt.bHour,
                            dt.bMinutes,
                            dt.bSeconds);
    return timestamp_str;
}

/* ----------------------------------------------------------------------------------[init_RTC_from_BaseInstallLog]-- */
void init_RTC_from_BaseInstallLog(FILE *fp, UINT32 fileSize)
{
    bool updated_RTC = false;
    char *last_block = NULL;
    int len;

    if(validSysClock() == FALSE)
    {
        if (fp)
        {
            long block_size;
            long idx;

            /* extract last 1K from log and find last entry and use that to set the RTC */
            block_size = (fileSize < 1024 ? fileSize : 1024);
            last_block = malloc(block_size+1);
            fnCheckFileSysCorruption(fseek(fp, fileSize - block_size, SEEK_SET));
            len = fread(last_block, block_size, 1, fp);
            if(len < 1)
                fnCheckFileSysCorruption(-1);

            last_block[block_size] = 0; /* make sure it is null terminated */

            /* we are looking go find the last "\n20xx-xx-xxTxx:xx:xx," so we search
            * backward for \n starting strlen("\n20xx-xx-xxTxx:xx:xx," from the end
            */
            idx = block_size - sizeof(DEFAULT_LOG_DATE);
            do
            {
                while(--idx >=0 && last_block[idx] != '\n')
                {
                    ;
                }

                if (idx >= 0)
                {
                    char *test = "dddd-dd-ddTdd:dd:dd";
                    int  failed_test = false;
                    char *c_to_test = &last_block[idx+1];

                    while (*test != 0 && !failed_test)
                    {
                        if (*test == 'd')
                        {
                            if (!isdigit(*c_to_test))
                                failed_test = true;
                        }
                        else if (*c_to_test != *test)
                            failed_test = true;

                        test++;
                        c_to_test++;
                    }

                    if (!failed_test)
                    {
                        update_RTC(&last_block[idx+1], PLUS_1_SEC);
                        updated_RTC = true;
                    }
                }
            }while(!updated_RTC && idx > 0);
        }

        if (!updated_RTC)
            update_RTC(DEFAULT_LOG_DATE, NO_SECS_ADJUST);

        if(last_block != NULL){
            free(last_block);
        }
    }

    get_install_log_timestamp(sz_starting_timestamp);    // so we have an accurate starting timestamp for the start entry in the log */
}
/* **********************************************************************
 DESCRIPTION: Appends the input status to the base install log file.
 * INPUTS: name of the file being installed, install status.
 * NOTES:
 * 1. This is not re-entrant.
 * 2. If there is a power failure or crash before file is closed, the update
 * is lost but the original file is OK (not corrupted).
 *************************************************************************/

int UpdateBaseInstallLog(char *filename, char *installStatus){

	FILE *ptr_file;
	char *fileEnd = "FileInstallEnd";
	char *fileStart = "FileInstallStart";
	char *logName = LOG_PATH;
	UINT32 fileSize;
	int ret = SUCCESS;

	fileSize = fsGetFileSize(logName);
	if (fileSize == 0)
	{
		dbgTrace(DBG_LVL_ERROR, "Error: Base Install log does not exist or is empty\n");
		return 1;
	}

	memset(install_str, 0x0, sizeof(install_str));

	ptr_file = fopen(logName,"a+");
	if (!ptr_file){
		fnCheckFileSysCorruption(-1);
		dbgTrace(DBG_LVL_ERROR, "Failed to open %s\n", logName);
		return 1;
	}

	init_RTC_from_BaseInstallLog(ptr_file, fileSize);

	// Go to the end
	fnCheckFileSysCorruption(fseek(ptr_file, 0, SEEK_END));

	//Add FileInstallStart
	sprintf(install_str, "%s,%s\n", sz_starting_timestamp, fileStart);

	//Insert installed file
	sprintf(install_str + strlen(install_str), "%s,%s,Installed,%s\n", sz_starting_timestamp,filename, installStatus);

	//Add FileInstallEnd
	sprintf(install_str + strlen(install_str), "%s,%s\n", sz_starting_timestamp, fileEnd);
	ret = fputs(install_str, ptr_file);

	fnCheckFileSysCorruption(fclose(ptr_file));

	dbgTrace(DBG_LVL_INFO, "copied %s to ms/dld folder :%s\r\n", filename, installStatus);

	return ret;

}




/* **********************************************************************
 DESCRIPTION: Appends the input status to the base install log file.
 * INPUTS: name of the file being installed, install status.
 * NOTES:
 * 1. This is not re-entrant.
 * 2. If there is a power failure or crash before file is closed, the update
 * is lost but the original file is OK (not corrupted).
 *************************************************************************/

int UpdateGraphicsBaseInstallLog(GFINSTALLENTRY *pGraphicList, int  numGraphics)
{

	FILE *ptr_file;
	char *fileEnd = "FileInstallEnd";
	char *fileStart = "FileInstallStart";
    char *successStatus = "SUCCESS";
    char *failStatus = "FAILED";
    char installStatus[8];
	char *logName = LOG_PATH;
	UINT32 fileSize;
	int ret = SUCCESS;
	char *pMemBuff = NULL;
	GFINSTALLENTRY *pEntry;
	int i;

	fileSize = fsGetFileSize(logName);
	if (fileSize == 0)
	{
		dbgTrace(DBG_LVL_ERROR, "Error: Base Install log does not exist or is empty\n");
		return 1;
	}

	ptr_file = fopen(logName,"a+");
	if (!ptr_file){
		fnCheckFileSysCorruption(-1);
		dbgTrace(DBG_LVL_ERROR, "Failed to open %s\n", logName);
		return 1;
	}

	init_RTC_from_BaseInstallLog(ptr_file, fileSize);

	// Go to the end
	fnCheckFileSysCorruption(fseek(ptr_file, 0, SEEK_END));

	pEntry = pGraphicList;
	
    for(i = 0; i < numGraphics; i ++)
    {
        if(pEntry->failStatus == FALSE)
            strcpy(installStatus, successStatus);
        else
            strcpy(installStatus, failStatus);
        size_t needed = snprintf(NULL, 0, "%s,%s\n%s,%s,Installed,%s\n%s,%s\n", sz_starting_timestamp, fileStart, sz_starting_timestamp,pEntry->fileName, installStatus,  sz_starting_timestamp, fileEnd) + 1;
        char  *pMemBuff = malloc(needed);
        if(pMemBuff == NULL)
        {
            fnCheckFileSysCorruption(fclose(ptr_file));
            return 1;
        }
        memset(pMemBuff, 0, needed);
        sprintf(pMemBuff, "%s,%s\n%s,%s,Installed,%s\n%s,%s\n", sz_starting_timestamp, fileStart, sz_starting_timestamp,pEntry->fileName, installStatus,  sz_starting_timestamp, fileEnd);
        int fputsRet = 0;
        fputsRet = fputs(pMemBuff, ptr_file);
        if(fputsRet < 0)
            ret = FAIL;

        if(pMemBuff!= NULL)
            free(pMemBuff);

        pEntry++;
    }

	dbgTrace(DBG_LVL_INFO, "UpdateGraphicsBaseInstallLog:fputs return %d \n", ret);
	
	fnCheckFileSysCorruption(fclose(ptr_file));

	dbgTrace(DBG_LVL_INFO, "Updated %d graphics files \r\n", numGraphics);

	return ret;

}

STATUS fnGARLookForNewGAR()
{
    STATUS status = NU_SUCCESS;

    NU_MEMORY_POOL *mem_pool;
    unsigned char *outAddrp = 0;
    int  offset = 0;
    UINT32 filesize;
    DSTAT statobj;
    char pattern[MAX_FILENAME_SIZE];
    char path[MAX_FILENAME_SIZE];
    char old_path_fileName[MAX_FILENAME_SIZE];
    char new_path_fileName[MAX_FILENAME_SIZE];

    memset(pattern, 0x0, sizeof(pattern));
    memset(&statobj, 0x0, sizeof(DSTAT));
    memset(path, 0x0, sizeof(path));

    fnFilePath(path, INSTALL_PATH, "");
    sprintf(pattern, "%s\\*.GAR", path);


    status = NU_Get_First(&statobj, pattern);
    if(status == NU_SUCCESS){

        strncpy(new_path_fileName, statobj.lfname, sizeof(new_path_fileName));
        filesize = statobj.fsize;
        (void) NU_Done(&statobj);

        dbgTrace(DBG_LVL_INFO, "GAR file name in install folder :%s \r\n", new_path_fileName);
        status = NU_System_Memory_Get(&mem_pool, NU_NULL);
        if (status != NU_SUCCESS)
        {
            dbgTrace(DBG_LVL_ERROR, "fnGARLookForNewGAR: Error retrieving system memory pool\r\n");
            return status;
        }

        status = NU_Allocate_Memory(mem_pool, (VOID **) &outAddrp, filesize, NU_NO_SUSPEND);
        if (status != NU_SUCCESS)
        {
            dbgTrace(DBG_LVL_ERROR, "fnGARLookForNewGAR: Error allocating %lu bytes\r\n", filesize);
            return status;
        }

        status = fnFileRead(INSTALL_PATH, new_path_fileName, (char *) outAddrp,  offset, filesize );
        if(status == SUCCESS)
        {// Now find original file and replace it
            sprintf(pattern, "A:\\*.GAR");
            //re-using object so clear it
            memset(&statobj, 0x0, sizeof(DSTAT));

            status = NU_Get_First(&statobj, pattern);
            if(status == SUCCESS)
            {
                strncpy(old_path_fileName, statobj.lfname, sizeof(old_path_fileName));
                (void) NU_Done(&statobj);

                status = fnFileDelete("", old_path_fileName);
                if(status == SUCCESS)
                {
                    OSWakeAfter(0); // relinquish control to OS so it can properly update file system
                }
                else
                {
                    dbgTrace(DBG_LVL_ERROR, "fnGARLookForNewGAR: Failed to delete old GAR file: %d. Will attempt overwriting. \r\n", status);
                }
            }
            else
            {
                fnCheckFileSysCorruption(status);;
            }

            status = fnFileCreate("", new_path_fileName, (char *) outAddrp,  filesize);
            if(status == SUCCESS){
                status = fnFileDelete(INSTALL_PATH, new_path_fileName);
                if(status != SUCCESS)
                {
                    dbgTrace(DBG_LVL_ERROR, "fnGARLookForNewGAR: Failed to delete GAR file in install directory: %d. \r\n", status);
                }
                //need to log in install BaseInstall.log
                char installStatus[]= "SUCCESS";
                UpdateBaseInstallLog(new_path_fileName, installStatus);
            }
            else
            {
                dbgTrace(DBG_LVL_ERROR, "fnGARLookForNewGAR: Failed to create GAR file in dld folder  %d \r\n", status);
                char installStatus[]= "FAILED";
                UpdateBaseInstallLog(new_path_fileName, installStatus);
            }

        }
        else
        {
            dbgTrace(DBG_LVL_ERROR, "fnGARLookForNewGAR: Failed to read GAR file in Install folder %d \r\n", status);
            char installStatus[]= "FAILED";
            UpdateBaseInstallLog(new_path_fileName, installStatus);

        }

    }
    else
    {
        dbgTrace(DBG_LVL_ERROR, "fnGARLookForNewGAR: GAR file not found in Install folder\r\n");
        fnCheckFileSysCorruption(status);
    }

    if(outAddrp != NULL)
            NU_Deallocate_Memory(outAddrp);

    return status;
}


STATUS fnZmdLookForNewZmd()
{
	STATUS status = NU_SUCCESS;

	NU_MEMORY_POOL *mem_pool;
    unsigned char *outAddrp = 0;
    int  offset = 0;
    UINT32 filesize;
	DSTAT statobj;
	char pattern[MAX_FILENAME_SIZE];
	char path[MAX_FILENAME_SIZE];
    char old_path_fileName[MAX_FILENAME_SIZE];
    char new_path_fileName[MAX_FILENAME_SIZE];

	memset(pattern, 0x0, sizeof(pattern));
	memset(&statobj, 0x0, sizeof(DSTAT));
	memset(path, 0x0, sizeof(path));

	fnFilePath(path, INSTALL_PATH, "");
	sprintf(pattern, "%s\\*.ZMD", path);


	status = NU_Get_First(&statobj, pattern);
    if(status == NU_SUCCESS){
    	strncpy(new_path_fileName, statobj.lfname, sizeof(new_path_fileName));
    	filesize = statobj.fsize;
		(void) NU_Done(&statobj);

		dbgTrace(DBG_LVL_INFO, "ZMD file name in install folder :%s \r\n", new_path_fileName);
		status = NU_System_Memory_Get(&mem_pool, NU_NULL);
		if (status != NU_SUCCESS)
		{
			dbgTrace(DBG_LVL_ERROR, "fnZmdLookForNewZmd: Error retrieving system memory pool\r\n");
			return status;
		}

		status = NU_Allocate_Memory(mem_pool, (VOID **) &outAddrp, filesize, NU_NO_SUSPEND);
		if (status != NU_SUCCESS)
		{
			dbgTrace(DBG_LVL_ERROR, "fnZmdLookForNewZmd: Error allocating %lu bytes\r\n", filesize);
			return status;
		}

		status = fnFileRead(INSTALL_PATH, new_path_fileName, (char *) outAddrp,  offset, filesize );
		if(status == SUCCESS)
		{// Now find original file and replace it
			sprintf(pattern, "A:\\*.ZMD");
			//re-using object so clear it
			memset(&statobj, 0x0, sizeof(DSTAT));

			status = NU_Get_First(&statobj, pattern);
			if(status == SUCCESS)
            {
		    	strncpy(old_path_fileName, statobj.lfname, sizeof(old_path_fileName));
				(void) NU_Done(&statobj);

				status = fnFileDelete("", old_path_fileName);
				if(status == SUCCESS)
				{
	                OSWakeAfter(0); // relinquish control to OS so it can properly update file system
				}
				else
				{
			    	dbgTrace(DBG_LVL_ERROR, "fnZmdLookForNewZmd: Failed to delete old ZMD file: %d. Will attempt overwriting. \r\n", status);
				}
            }
            else
            {
                fnCheckFileSysCorruption(status);
            }

			status = fnFileCreate("", new_path_fileName, (char *) outAddrp,  filesize);
			if(status == SUCCESS){
				status = fnFileDelete(INSTALL_PATH, new_path_fileName);
				if(status != SUCCESS)
				{
			    	dbgTrace(DBG_LVL_ERROR, "fnZmdLookForNewZmd: Failed to delete ZMD file in install directory: %d. \r\n", status);
				}
				//need to log in install BaseInstall.log
				char installStatus[]= "SUCCESS";
				UpdateBaseInstallLog(new_path_fileName, installStatus);
			}
		    else
		    {
		    	dbgTrace(DBG_LVL_ERROR, "fnZmdLookForNewZmd: Failed to create new ZMD file %d \r\n", status);
		    	char installStatus[]= "FAILED";
		    	UpdateBaseInstallLog(new_path_fileName, installStatus);

		    }

		}
		else
		{
			dbgTrace(DBG_LVL_ERROR, "fnZmdLookForNewZmd: Failed to read new ZMD file  %d \r\n", status);
			char installStatus[]= "FAILED";
			UpdateBaseInstallLog(new_path_fileName, installStatus);

		}

    }
    else
    {
    	dbgTrace(DBG_LVL_ERROR, "fnZmdLookForNewZmd: ZMD file not found in Install folder\r\n");
        fnCheckFileSysCorruption(status);
    }

    if(outAddrp != NULL)
			NU_Deallocate_Memory(outAddrp);

	return status;
}

STATUS fnUpdateZmd()
{
    unsigned char *inAddrp = 0;
    unsigned char *outAddrp = 0;

    int len;

	STATUS status = NU_SUCCESS;
	INT fd;
	uint8_t *ptemp;
	DSTAT statobj;
	CHAR  pattern[6] = "*.ZMD";
	int readlength = 0;
	NU_MEMORY_POOL *mem_pool;

	unsigned long maxSize = 0x190000;

	// Check to see if a compressed ZMD file exists
	status = NU_Get_First(&statobj, pattern);
    if(status == NU_SUCCESS)
	{
		//Open and Read
		fd = NU_Open(statobj.lfname, (PO_BINARY | PO_RDONLY ), PS_IREAD);
		if(fd < 0)
		{
			dbgTrace(DBG_LVL_ERROR, "UpdateZMD: Failed to open zmd file %s \r\n", statobj.lfname);
			status = fd;
			fnCheckFileSysCorruption(status);
		}
		else
		{
			dbgTrace(DBG_LVL_ERROR, "ZMD file name :%s \r\n", statobj.lfname);
			// we have a ZMD file, so see if it's OK
 			status = NU_System_Memory_Get(&mem_pool, NU_NULL);
			if (status != NU_SUCCESS)
			{
				dbgTrace(DBG_LVL_ERROR, "UpdateZMD: Error retrieving system memory pool\r\n");
				fnCheckFileSysCorruption(NU_Close(fd));
				(void) NU_Done(&statobj);
				return status;
			}
			status = NU_Allocate_Memory(mem_pool, &inAddrp, statobj.fsize, NU_NO_SUSPEND);
			if (status != NU_SUCCESS)
			{
				dbgTrace(DBG_LVL_ERROR, "UpdateZMD: Error allocating %lu bytes\r\n", statobj.fsize);
				fnCheckFileSysCorruption(NU_Close(fd));
				(void) NU_Done(&statobj);
				return status;
			}
			status = NU_Allocate_Memory(mem_pool, &outAddrp, maxSize, NU_NO_SUSPEND);
			if (status != NU_SUCCESS)
			{
				dbgTrace(DBG_LVL_ERROR, "UpdateZMD: Error allocating %lu bytes\r\n", maxSize);

				if(inAddrp != NULL)
						NU_Deallocate_Memory(inAddrp);
				fnCheckFileSysCorruption(NU_Close(fd));
				(void) NU_Done(&statobj);
				return status;
			}

			if (inAddrp)
			{
				readlength = NU_Read(fd, (void*)inAddrp, statobj.fsize);
				if ((readlength < 0) || (readlength != statobj.fsize))
				{
					dbgTrace(DBG_LVL_ERROR, "UpdateZMD: Failed to read zmd file %s read length: %0x / %0x \r\n", statobj.lfname, readlength, statobj.fsize);
					status = readlength;
					fnCheckFileSysCorruption(status);
					if(inAddrp != NULL)
						NU_Deallocate_Memory(inAddrp);
					fnCheckFileSysCorruption(NU_Close(fd));
				}
				else
				{
					status = fnZLIBUncompress(outAddrp, &maxSize, inAddrp);
					if (status == Z_OK)
					{
						//Check for same emdRelease original and start using new emd
						if(pEMD_RAM != NULL)
						{

							if(memcmp(pEMD_RAM, (void*)outAddrp, maxSize) == 0)
							{
								emdRamSize = maxSize;
								dbgTrace(DBG_LVL_INFO, "UpdateZMD: New zmd is same as old %s \r\n", statobj.lfname);
								NU_Deallocate_Memory(outAddrp);
								NU_Deallocate_Memory(inAddrp);
								fnCheckFileSysCorruption(NU_Close(fd));
							}
							else
							{
								ptemp = pEMD_RAM;
								pEMD_RAM = outAddrp;
								outAddrp = NULL;
								if(ptemp!=NULL)
										NU_Deallocate_Memory(ptemp);
								emdRamSize = maxSize;
							}
						}
						else
						{
								pEMD_RAM = outAddrp;
								emdRamSize = maxSize;
						}

						RamEMDPresent = TRUE;
					}
					else
					{
						// failure to unzip a block
						status = EMDUNCOMPERROR;
						if(inAddrp != NULL)
							NU_Deallocate_Memory(inAddrp);
						if(outAddrp != NULL)
							NU_Deallocate_Memory(outAddrp);
						fnCheckFileSysCorruption(NU_Close(fd));
					}
				}
			}

		}
        (void) NU_Done(&statobj);
	}
    else
    {

    	dbgTrace(DBG_LVL_INFO, "UpdateZMD: No zmd file \r\n");
    	//TODO need to remove UpdateEMD() method when everyone transitions to zmd file.
    	UpdateEMD();
    }
	if(inAddrp != NULL)
		NU_Deallocate_Memory(inAddrp);

	fnCheckFileSysCorruption(NU_Close(fd));
	return status;
}


STATUS UpdateEMD(void)
{
	STATUS status = NU_SUCCESS;
	INT fd;
	uint8_t *ptemp;
	uint8_t *pEmdTemp = NULL;
	DSTAT statobj;
	CHAR  pattern[6] = "*.EMD";
	//char filename[MAX_SFN + MAX_EXT];
	int readlength = 0;
	NU_MEMORY_POOL *mem_pool;

	//Find EMD file
	status = NU_Get_First(&statobj, pattern);
	if(status == NU_SUCCESS)
	{

		emdRamSize = statobj.fsize;

		//Open and Read
		fd = NU_Open(statobj.lfname, (PO_TEXT | PO_RDONLY ), PS_IREAD);
		if(fd < 0)
		{
			dbgTrace(DBG_LVL_ERROR, "UpdateEMD: Failed to open emd file %s \r\n", statobj.lfname);
			status = fd;
			fnCheckFileSysCorruption(status);
		}
		else
		{
			if(emdRamSize <= 0)
			{
				dbgTrace(DBG_LVL_ERROR, "UpdateEMD: Failed to Seek file: %0x / %0x \r\n", statobj.lfname,  statobj.fsize, emdRamSize);
				status = emdRamSize;
			}
			if(status == 0)
			{
				status = NU_System_Memory_Get(&mem_pool, NU_NULL);
				if (status != NU_SUCCESS)
				{
					dbgTrace(DBG_LVL_ERROR, "UpdateEMD: Error retrieving system memory pool\r\n");
					(void) NU_Done(&statobj);
					return status;
				}
				status = NU_Allocate_Memory(mem_pool, &pEmdTemp, (emdRamSize + 16), NU_NO_SUSPEND);
				if (status != NU_SUCCESS)
				{
					dbgTrace(DBG_LVL_ERROR, "UpdateEMD: Error allocating %lu bytes\r\n", emdRamSize);
					(void) NU_Done(&statobj);
					return status;
				}

				readlength = NU_Read(fd, (void*)pEmdTemp, emdRamSize);
				if ((readlength < 0) || (readlength != emdRamSize))
				{
					dbgTrace(DBG_LVL_ERROR, "UpdateEMD: Failed to read emd file %s read length: %0x / %0x \r\n", statobj.lfname, readlength, emdRamSize);
					status = readlength;
					NU_Deallocate_Memory(pEmdTemp);
					fnCheckFileSysCorruption(status);
				}
				else
				{
					//Check for same emdRelease original and start using new emd
					if(pEMD_RAM != NULL)
					{
						if(memcmp(pEMD_RAM, (void*)pEmdTemp, emdRamSize) == 0)
						{
							dbgTrace(DBG_LVL_INFO, "UpdateEMD: New emd is same as old %s \r\n", statobj.lfname);
							NU_Deallocate_Memory(pEmdTemp);
						}
						else
						{
							ptemp = pEMD_RAM;
							pEMD_RAM = pEmdTemp;
							pEmdTemp = NULL;
							if(ptemp!=NULL)
								NU_Deallocate_Memory(ptemp);
						}
					}
					else
					{
						pEMD_RAM = pEmdTemp;
					}

					RamEMDPresent = TRUE;

				}
			}
			fnCheckFileSysCorruption(NU_Close(fd));
		}

        (void) NU_Done(&statobj);
	}
	else
	{
		fnCheckFileSysCorruption(status);
		//No EMD file
		dbgTrace(DBG_LVL_INFO, "UpdateEMD: No emd file \r\n");
//		//Use hard coded EMD
//		emdRamSize = 0x500000; //G900EMD_UNC_len;
//
//		status = NU_System_Memory_Get(&mem_pool, NU_NULL);
//		if (status != NU_SUCCESS)
//		{
//			dbgTrace(DBG_LVL_ERROR, "UpdateEMD: Error retrieving system memory pool\r\n");
//			return status;
//		}
//		status = NU_Allocate_Memory(mem_pool, &pEmdTemp, (emdRamSize), NU_NO_SUSPEND);
//		if (status != NU_SUCCESS)
//		{
//			dbgTrace(DBG_LVL_ERROR, "UpdateEMD: Error allocating %lu bytes\r\n", emdRamSize);
//			return status;
//		}
//
//		if(pEmdTemp == NULL)
//		{
//			dbgTrace(DBG_LVL_ERROR, "UpdateEMD: Failed to allocate memory\r\n");
//			status = NU_NO_MEMORY;
//		}
//		else
//		{
//
//			memcpy(pEmdTemp, G900EMD_UNC, G900EMD_UNC_len);
//
//			ptemp = pEMD_RAM;
//			pEMD_RAM = pEmdTemp;
//			pEmdTemp = NULL;
//			if(ptemp!=NULL)
//				free(ptemp);
//
//			if(status == NU_SUCCESS)
//			{
//				strcpy(statobj.lfname, "A:\\G900EMD.EMD");
//				fd = NU_Open(statobj.lfname, (PO_TEXT | PO_RDWR | PO_CREAT), PS_IREAD| PS_IWRITE);
//				if(fd < 0)
//				{
//					dbgTrace(DBG_LVL_ERROR, "UpdateEMD: Failed to open emd file %s \r\n", statobj.lfname);
//					status = fd;
//				}
//				else
//				{
//					readlength = NU_Write(fd, (void*)pEMD_RAM, G900EMD_UNC_len);
//					if ((readlength < 0) || (readlength != emdRamSize))
//					{
//						dbgTrace(DBG_LVL_ERROR, "UpdateEMD: Failed to write emd file %s read length: %0x / %0x \r\n", statobj.lfname, readlength, G900EMD_UNC_len);
//						status = readlength;
//					}
//
//					NU_Close(fd);
//				}
//			}
//		}

	}

	return status;
}

//-------------------------------------------------------------------
// Prototypes for external functions 
//  that are not yet prototyped in an include file.
//-------------------------------------------------------------------

//-------------------------------------------------------------------
// Declarations of external data:
//-------------------------------------------------------------------
 
#ifdef GET_SCREENS_FROM_FLASH
// From systask.c:
extern BOOL NoEMDPresent;
#endif
extern BOOL RamEMDPresent;

// From oiflashfp.c:
#ifndef GET_SCREENS_FROM_FLASH  /* when not getting screens from flash, this data comes from oiflash.c */
extern unsigned char    bstubNumLangs;
#ifndef SCREEN_GENERATOR_411
extern const char       cArrayScreenHdr[];
extern const char       cArrayLangHdr[];
extern const char       cArrayLang2Hdr[];
extern const char       cArrayLang3Hdr[];
#else
extern const char       cArrayScreen[];
extern const char       cArrayLang[];
extern const char       cArrayLang2[];
extern const char       cArrayLang3[];
#endif
extern unsigned short   pstubLang1Name[];
extern unsigned short   pstubLang2Name[];
extern unsigned short   pstubLang3Name[];
extern char             *pstubLangFontTable[MAX_STUB_LANGS][MAX_STUB_FONTS_PER_LANG]; 
extern char             *pstubLangFontTableDM475[MAX_STUB_LANGS][MAX_STUB_FONTS_PER_LANG]; 
#endif


//extern FFS *FlashFFS;
//extern  CMOSRATECOMPSMAP CMOSRateCompsMap;


//-------------------------------------------------------------------
// Declarations of Local data:
//-------------------------------------------------------------------

DIRECTORYCOMP   pDirectory;                             /* derived FLASH directory, in which the offsets    */
                                                        /* given in the FLASH resident directory have       */
                                                        /* been changed to addresses                        */

unsigned long   lwFlashMainDirectory = 0;           /* This is a pointer to the Real Directory in Flash */
unsigned char   *pParamTypeIndex;           /* This is a pointer to the active context param. index */
unsigned long   Current_Top_Of_Flash;
unsigned long   lwCurrentContext = 0;       /* the currently selected context.  since this is used in a
                                                    lot of places, i made this a long instead of a byte so
                                                    that we don't need to do sign extensions all over the
                                                    place */

//----------------------------------------------------------------------------
//  EMD parameter substitution:
//----------------------------------------------------------------------------
// This ifdef is to make sure this stuff is only included in development builds.
//#ifndef GET_SCREENS_FROM_FLASH
#define PCN_PARM_SUBSTITUTION

BOOL fnGetSubstitutePCNParm(unsigned short wParameterID, unsigned short wParameterType, void *pParmValue);
#define TYPE_BP     0   // Byte Parameter
#define TYPE_WP     1   // Word Parameter
#define TYPE_LP     2   // Long Parameter
#define TYPE_MP     3   // Money parameter
#define TYPE_ASP    4   // ASCII string parameter
#define TYPE_PB     5   // Packed byte parameter
#define TYPE_MK     6   // Memory Key parameters
#define TYPE_USP    7   // Unicode string parameter
#define TYPE_MPB    8   // Multi-packed byte parameter
#define TYPE_IBP    9   // iButton Byte Parameter
#define TYPE_IWP    10  // iButton Word Parameter
#define TYPE_ILP    11  // iButton Long Parameter
#define TYPE_IMP    12  // iButton Money parameter
#define TYPE_ISP    13  // iButton ASCII string parameter
#define TYPE_IPB    14  // iButton Packed byte parameter
#define TYPE_IUSP   15  // iButton Unicode string parameter
#define TYPE_IMPB   16  // iButton Multi-packed byte parameter

#define END_OF_PARM_TABLE   (USHORT)0xFFFE

//#endif  // Screens from FFS.

#define APP_HEADER_LEN      3   /* app file header len - 3 four-byte blocks */

//----------------------------------------------------------------------------
//  End of EMD Parameter Substitution macro definitions.
//----------------------------------------------------------------------------


/* ***************************************************************************/
//----------------------------------------------------------------------------
//              FUNCTIONS:
//----------------------------------------------------------------------------
/* ***************************************************************************/

/* NOTES: These functions make the assumption that all components and all indexes
    begin on a 4-byte boundary.
*/

/* **********************************************************************
// FUNCTION NAME: fnFlashSelectContext
// DESCRIPTION: Selects the current flash context.  
// AUTHOR: Joe Mozdzer
//
// INPUTS:  bNewContext - New context number.
//
// OUTPUTS: SUCCESSFUL - if the bNewContext was a valid value,
//          FAILURE - otherwise
// *************************************************************************/
BOOL fnFlashSelectContext(unsigned char bNewContext)
{
    BOOL            fRetval = SUCCESSFUL;
    unsigned char   bContextConstant;


    /* check that the new context is a valid one */
    bContextConstant = *(GET_BYTE_CONSTANT(COFFSET_CONTEXTS_CNT));

    if (bNewContext > bContextConstant)
        fRetval = FAILURE;
    else
        lwCurrentContext = (unsigned long) bNewContext;

    return (fRetval);
}
/* **************************************************************************\
** FUNCTION NAME: fnEmdFlashInit                                                **
**   DESCRIPTION: Initializes the FlashMainDirectory pointer, the           **
**                parameter type index pointer for the active context, and  **
**                the Directory Component structure with the pointers to    **
**                the various components.  This allows the software to use  **
**                pointers instead of using the offsets contained in flash  **
**        AUTHOR: KME                                                       **
**                                                                          **
**        INPUTS: None                                                      **
**                                                                          **
**       OUTPUTS: None                                                      **
\* **************************************************************************/
int fnEmdFlashInit( void)
{
    unsigned long   *gPtr;
    unsigned long   *pID;
    unsigned long   *pDC;
    unsigned char   *pContextIndex;
    unsigned char   *aPtr;

    int ec = -1;
    unsigned long   contextIndex;
    unsigned long billy;
    unsigned short  i;
    //unsigned char fname[64];

  
  /*
   * Initialize the flash main directory pointer.  Many of the offsets in 
   * flash are offset from this address.
   * Initialize the directory structure.  Note that the checksum, component
   * size and number of free sectors are not pointers but are actually values.  
   * Copy the values for size and checksum into the proper elements.  For the 
   * pointers, get the offset stored in flash and add it to the base pointer.
   */
  
    if (RamEMDPresent == TRUE)
    {
        // indicate that everything is OK
        ec = 0;

        aPtr = (unsigned char *) GetEmdRam(); //HALGetEMDRAMBase();   /* get ptr to top of flash */
        Current_Top_Of_Flash = (unsigned long) GetEmdRam(); //HALGetEMDRAMBase();
        gPtr =  (unsigned long *) GetEmdRam(); //HALGetEMDRAMBase();  /* get ptr of main dir offset (at tof) */
        aPtr += EndianAwareGet32(gPtr); /* add offset to top of flash */
        pID = (void *)aPtr;                         /* get ptr to main directory */
    
        EndianAwareCopy((char *)(&lwFlashMainDirectory), aPtr, sizeof(long));  /* copy main directory address  */
        EndianAwareCopy((char *)(&pDirectory.CheckSum), pID++, sizeof(long));  /* and the directory checksum   */
        EndianAwareCopy((char *)(&billy), pID++, sizeof(long));                /* and size                     */

        pDirectory.ComponentSize = billy;
        pDC = (unsigned long *)(&pDirectory.Constants); /* create a moving pointer to   */
                                                                /* system directory             */
        for (i = CONSTANTS_DC; i < EMDCOMPATIBILITY; i++)
        {
        	EndianAwareCopy((char *)(pDC), (char *)(pID), sizeof(long)); /*handle the offsets in this loop */
            *pDC += Current_Top_Of_Flash;
            pID++;
            pDC++;
        }

        // Move over the EMD Version with adjusting for Top Of Flash
        EndianAwareCopy( pDC, pID, sizeof(long) ); /*handle the offsets in this loop */

        // Verify EMD Version
//TODO Do we need EMD compatibility
		//if(pDirectory.EmdCompatibility == EMD_FLASH_COMPATIBILITY )
        {
            /*
             * Calculate the pointer to the active context by adding the active context
             * (offset) to the context Parm pointer.  Copy the byte offset for the 
             * parameter index from flash.  Add the offset to the flash main directory
             * pointer to get the pointer to the parameter type index.
             */
            pContextIndex = (unsigned char *)(pDirectory.ContextParms + lwCurrentContext);
            EndianAwareCopy(&contextIndex, pContextIndex, sizeof(unsigned long));
            pParamTypeIndex = contextIndex + ((unsigned char *)(Current_Top_Of_Flash));
        }
    }
  return(ec);
}

/* **************************************************************************\
** FUNCTION NAME: fnFlashGetAddress                                         **
**   DESCRIPTION: Returns the address for the requested component from the  **
**                Main directory.  If the component ID is invalid, the Null **
**                pointer will be returned. Note the checksum and size      **
**                components are not in the directory structure such that   **
**                the pointers have to be derived.                          **
**        AUTHOR: KME                                                       **
**                                                                          **
**        INPUTS: Directory Component ID (defined in fstub.h)               **
**                                                                          **
**       OUTPUTS: Directory Component address                               **
\* **************************************************************************/
unsigned long *fnFlashGetAddress(unsigned short wComponentID)
{
  ulong *pDC,
    *poffset,
    *pmd;
  uchar *ptof;
  
  /*
   * If the component id is out of range return the null pointer.  Otherwise
   * if the component address must be calculated, do it, else get the address
   * stored in the directory.
   */
  if (wComponentID > MAX_DIRECTORY_COMPONENT)
    return( NULL_PTR );
  else
    if (wComponentID == CHECKSUM_DC 
    || wComponentID == COMPONENT_SIZE_DC)
      {  
    /*
     * Get the pointer to top of flash.  Get the pointer to the main
     * directory offset (which is at the top of flash).  Add the offset
     * to top of flash to get the address of the main directory.  Add
     * the component offset to get the address of the component.
     */
    ptof = (unsigned char *)(Current_Top_Of_Flash);
    poffset = (ulong *) (Current_Top_Of_Flash);
    ptof += EndianAwareGet32(poffset);
    pmd = (void *) ptof;
    return (pmd + wComponentID);
      }
    else
      {
    pDC = (unsigned long *) (&pDirectory);
    return ((unsigned long *) *(pDC + wComponentID));   
      }
}
/* **************************************************************************\
** FUNCTION NAME: fnGetStartofSBLink()                                          **
**   DESCRIPTION: retrieves the address of the first link in the supplemental   **
         binary linked list by reading the directory component holding the  **
         offset and converting it to a physical flash address               **
**                                                                          **
**    AUTHOR: WFB                                                           **
**                                                                          **
**   INPUTS: none                                                           **
**                                                                          **
**   OUTPUTS: returns void pointer to start of linked list                  **
** **************************************************************************/
#if 0 //TODO - not called, confirm this is not needed.
void *fnGetStartofSBLink()
{
    void *StartofLink;

    StartofLink = fnFlashGetAddress(SUPP_BIN_FILES_DC);
    return(StartofLink);
}
#endif

/* **********************************************************************

 FUNCTION NAME: fnFlashGetSBinaryPtr;

 PURPOSE: Returns a ptr to the link header for the supplemental binary 
 in Flash with the passed component ID 
                                        
 AUTHOR: Bill Bailey

 INPUTS: Supplemental Binary Type
 
OUTPUT: Starting address of specified binary or NULL if not found           
 *************************************************************************/
#if 0 //TODO - not called, confirm this is not needed.
void *fnFlashGetSBinaryPtr(unsigned short CompId)
{
  char *LinkPtr;
  struct LinkHeader SBLinkHeader;
  struct LinkHeader *pSBHeader = &SBLinkHeader;
  BOOL forever = TRUE;
  
    LinkPtr = ( char *) fnGetStartofSBLink();
    if( LinkPtr != NULL_PTR )
    {
      do
      {
        fnGetAILinkHeader(LinkPtr, pSBHeader);
        if (SBLinkHeader.GeneratedID == CompId )
        {
          forever = FALSE;
        }
        else
        {
          if(   (SBLinkHeader.OffsetToNextGraphic != NULL_VAL)
             && (SBLinkHeader.OffsetToNextGraphic != (unsigned long)(-1)) )
              LinkPtr = (void *)( Current_Top_Of_Flash
                                + SBLinkHeader.OffsetToNextGraphic);
          else
          {
              forever = FALSE;
              LinkPtr = NULL_PTR;
          }
        }
      } while (forever);
    }
    return(LinkPtr);
}
#endif

/* **************************************************************************\
** FUNCTION NAME: fnFlashGetSuppBinaryAddress                               **
**   DESCRIPTION: retrieve supplemental binary SBinID, if it exists         **
**                                                                          **
**    AUTHOR: WFB                                                           **
**                                                                          **
**   INPUTS: SBinID  supplemental binary type id                            **
**                                                                          **
**   OUTPUTS: Sets SBinPtr = to address of SBinID element                   **
**   Returns TRUE if successful, FALSE if not                               **
** **************************************************************************/
#if 0 //TODO - not called, confirm this is not needed.
BOOL    fnFlashGetSuppBinaryAddress(unsigned short SBinID, void **SBinPtr) {
uchar           *pSB;
short           retval = TRUE;

 pSB = fnFlashGetSBinaryPtr(SBinID);       /* get the binary component */
 
 if( pSB != NULL_PTR )
   // NOTE: Added extra 8 bytes to header offset to get at the RAW BINARY DATA
   //       - J. Tokarski
   *SBinPtr = (void *) (pSB + FLASHLINKHEADERSIZE + ZBINHEADERSIZE);          /* add offset to the code if needed */
 else
        retval = FALSE;               /* sbinary type not found */
 return (retval);
}
#endif

/* **************************************************************************\
** FUNCTION NAME: fnFlashGetDirectorySizeAndCS                              **
**   DESCRIPTION: Returns the Size and checksum for the Main directory.     **
**        AUTHOR: KME                                                       **
**                                                                          **
**        INPUTS: None                                                      **
**                                                                          **
**       OUTPUTS: Directory Component Size and checksum                     **
\* **************************************************************************/
void fnFlashDirectorySizeAndCS(unsigned long *pSize, unsigned long *pCS)
{
    *pSize = pDirectory.ComponentSize;
    *pCS = pDirectory.CheckSum;
    return;
}


/* **************************************************************************\
** FUNCTION NAME: fnFlashGetComponentSizeAndCS                              **
**   DESCRIPTION: Returns the Size and checksum for the component.          **
**        AUTHOR: KME                                                       **
**                                                                          **
**        INPUTS: None                                                      **
**                                                                          **
**       OUTPUTS: Directory Component Size and checksum                     **
\* **************************************************************************/
void fnFlashComponentSizeAndCS( UINT8 *pAddr, UINT32 *pSize, UINT32 *pCS)
{
	EndianAwareCopy( pCS, pAddr, sizeof(ulong) );
    pAddr += sizeof( UINT32 );
    EndianAwareCopy( pSize, pAddr, sizeof(ulong) );
    return;
}


/* **************************************************************************\
** FUNCTION NAME: fnFlashVerifyCheckSum                                     **
**   DESCRIPTION: Verifies the checksum for the area specified.             **
**                Note the checksum is the sum of four byte blocks.  If     **
**                the size of the area is not a multiple of 4, the missing  **
**                bytes will be filled with zeros to obtain the checksum.   **
**        AUTHOR: KME                                                       **
**                                                                          **
**        INPUTS: Pointer to area (doesn't include checksum), Size,         **
**                Checksum                                                  **
**                                                                          **
**       OUTPUTS: Pass = 1, Fail = 0                                        **
\* **************************************************************************/
BOOL fnFlashVerifyCheckSum( void *pAddress, 
                            UINT32 Size,
                            UINT32 CheckSum )
{
    BOOL        fResult = FALSE;        /* The result */
    UINT32      CalculatedCS = 0;    /* The calculated check sum */
    UINT32      value;
    UINT32      Blocks;             /* The number of four byte blocks */
    UINT32      i;
    UINT16      wRemainder;         /* The number of bytes remaining */
    UINT16      j = 0;
    UINT8       bLastBlockCopy[4] = {0,0,0,0};
    UINT8       *pLastBlock;        // Ptr to next 4-byte block to add to checksum.

    /*
     * First, subtract the size of the checksum from the area size, as the 
     * checksum itself is not included in the checksumming.
     * Get the number of four byte blocks.  Get the number of bytes remaining 
     * in the last block.  Calculate the checksum for the blocks, copying the 
     * blocks bytewise since they may not be on a long word boundary.  Then copy
     * the last block filling the missing bytes with zeros (initialized that
     * way).  Add the last block to the checksum.  If the calculated check sum
     * is equal to the checksum passed in return true.  Otherwise return false.
     */

    Size -= 4;
    Blocks = Size / 4;
    wRemainder = Size % 4;

    pLastBlock = pAddress;
    // Copy each block of 4 bytes into a long, then add it to the checksum.
    for (i = 0; i < Blocks; i++)
    {
    	EndianAwareCopy( &value, pLastBlock, sizeof(ulong));
        pLastBlock += sizeof( UINT32 );
        CalculatedCS += value;
    }

    if (wRemainder) 
    {
        // pLastBlock points to the remaining partial block of data.
        for( i = (5-wRemainder); i < 5; i++ )
        {
            bLastBlockCopy[j] = *pLastBlock++;
            j++;
        }

        pLastBlock = bLastBlockCopy;
        EndianAwareCopy( &value, pLastBlock, sizeof(ulong));
        CalculatedCS += value;
    }

    if( CalculatedCS == CheckSum )
        fResult = TRUE;

    return( fResult );
        
}

/* **************************************************************************\
** FUNCTION NAME: fnFlashVerifySumsIndexCheckSums                           **
**   DESCRIPTION: Verifies the checksums for the components in the Sums     **
**                index list.  Flash must be initialized before calling     ** 
**                this function.                                            **
**        AUTHOR: KME                                                       **
**                                                                          **
**        INPUTS:                                                           **
**                                                                          **
**       OUTPUTS: Pass = 1, Fail = 0                                        **
\* **************************************************************************/
BOOL fnFlashVerifySumsIndexCheckSums(void)
{
    BOOL            fResult = TRUE;     /* The result */
    UINT32          i; 
    UINT32          size, cs;
    UINT32          *psumsindex;
    UINT8           *pconstants;
    UINT8           *paddr;     // Ptr to 4-byte values that MAY not be on a 4-byte boundary.

    /*
     * First verify the checksum of the main directory.  Get the number of 
     * components in the sums index list from the constants directory component.
     * Get the Sums index component list and verify the checksums in the 
     * components.  Increment the sums index address past the checksum, as it
     * is not included in the check summing itself.
     */
    fnFlashDirectorySizeAndCS(&size, &cs);
    fResult = fnFlashVerifyCheckSum( fnFlashGetAddress(COMPONENT_SIZE_DC), size, cs );

    if( fResult ) 
    {
        pconstants = (uchar *)fnFlashGetAddress(CONSTANTS_DC);
        i = EndianAwareGet32(pconstants + COFFSET_SUMS_CNT);
        psumsindex = fnFlashGetAddress(SUMS_INDEX_DC);

        while (fResult && i)
        {
            paddr = (UINT8 *)(EndianAwareGet32(psumsindex) + Current_Top_Of_Flash);
            psumsindex++;
            fnFlashComponentSizeAndCS( paddr, &size, &cs );
            fResult = fnFlashVerifyCheckSum( (paddr + sizeof(UINT32)), size, cs );
            i--;
        }
    }

    return (fResult);
}


/* **************************************************************************\
** FUNCTION NAME: fnFlashGetReport                                          **
**   DESCRIPTION: Gets the a pointer to the requested report for the active **
**                context out of flash.                                     **
**        AUTHOR: KME                                                       **
**                                                                          **
**        INPUTS: Report ID (defined in fstub.h)                            **
**                                                                          **
**       OUTPUTS: The pointer to the report method and address. The NULL    **
**                pointer will be returned for invalid report IDs.          **
\* **************************************************************************/
void *fnFlashGetReport(unsigned short wReportID)
{
    char    *pReports;
    unsigned short  wIndexx;
    unsigned long   toReport;
    unsigned char   ucNumReportsPerLang;

    /*
     * If the report ID is invalid return the NULL pointer.  Otherwise, get the
     * offset to the array of report methods and addresses.  Return the address
     * for the report (method & address) selected, for the active context by
     * adding the offset to the flash base address.
     */

    ucNumReportsPerLang = fnFlashGetNumReports();

    if ((wReportID >= MAX_REPORT) || (wReportID >= ucNumReportsPerLang))
        return( NULL_PTR );
    else
    {
        // Get offset to array: Reports[ NumLangs ][ NumReports ]
        pReports = (char *)pDirectory.Reports;
		
        // Calculate offset to location in Reports[][] array where the offset  
        //  for this report is stored.  Each entry is 6 bytes, the report offset 
        //  offset is stored last 4 bytes of the entry.
        wIndexx = (CMOSSetupParams.Language * (ucNumReportsPerLang * REPORT_INDEX_RECORD_SIZ)) 
             + (wReportID * REPORT_INDEX_RECORD_SIZ) + REPORT_METHOD_SIZ + REPORT_PAD_SIZ;

        // Copy the 4 bytes from the array into toReport.
        EndianAwareCopy((char *)(&toReport), pReports + wIndexx, sizeof(long));
		
        // If the offset is 0, this report does not exist in this EMD.
		// otherwise, get the address of the report
        if( toReport > 0 )
        {
            // Calculate the report address
            toReport += (long)Current_Top_Of_Flash;
            return( (void *)toReport );
        }
    }

    // If you get here, then this report is not in this EMD.
    return( NULL_PTR );
}


#ifdef TESTFLASH
/* **************************************************************************\
** FUNCTION NAME: fnFlashGetIndicia                                         **
**   DESCRIPTION: Gets the requested indicia pointer for the active context **
**                out of flash.                                             **
**        AUTHOR: KME                                                       **
**                                                                          **
**        INPUTS: Indicia ID (defined in fstub.h)                           **
**                                                                          **
**       OUTPUTS: The pointer to the indicia.                               **
\* **************************************************************************/
void *fnFlashGetIndicia( UINT16 wIndiciaPEID )
{
    void            *pIndicias;

    pIndicias = fnGrfxGetFirstGrfkPtrByType(wIndiciaPEID, GRFX_ANY_MATCH);
    return( pIndicias );
}
#endif

/* **********************************************************************
// FUNCTION NAME: fnFlashGetByteParm
// DESCRIPTION: Gets a byte-sized context parameter out of flash.
//                NOTE: It is the responsibility of the caller to range     **
//                check the paramater id, as this routine does none.        **
// AUTHOR: Joe Mozdzer
//
// INPUTS:  wParmID - ID number of the byte parameter.  The ID's are
//                      defined in datdict.h
//
// OUTPUTS: The byte parameter value is returned.
// *************************************************************************/
unsigned char fnFlashGetByteParm(unsigned short wParmID) {
    unsigned char   *pParamByteIndex,
                    *pParamBytes;
    unsigned long   paramByteIndex;

#ifdef PCN_PARM_SUBSTITUTION
    unsigned char   bTempValue=0;

    if( fnGetSubstitutePCNParm(wParmID, TYPE_BP, (void *)&bTempValue) == TRUE )
         return( bTempValue );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * byte parameter offset.  Copy the offset and add it to the parameter
     * type index to get the pointer to the byte parameters.  Use the parameter
     * id as an offset to return the value of the parameter requested.
     */
    if(pParamTypeIndex)
    {
    	pParamByteIndex = pParamTypeIndex + BYTE_PARAM_OFFSET * sizeof(unsigned long);
    	EndianAwareCopy(&paramByteIndex, pParamByteIndex, sizeof(unsigned long));
    	pParamBytes = paramByteIndex + pParamTypeIndex;
    	return (pParamBytes[wParmID]);
    }
    else
    	return 0;


}

/* **************************************************************************\
** FUNCTION NAME: fnFlashGetPackedByteParm                                  **
**   DESCRIPTION: Gets a Packed byte context pcn parameter out of flash.    **
**                This assumes that the active context has been initialized.**
**                NOTE: It is the responsibility of the caller to range     **
**                check the paramater id, as this routine does none.        **
**        AUTHOR: KME                                                       **
**                                                                          **
**        INPUTS: wParmID - ID number of the Packed byte parameter.  The    **
**                ID's are defined in datdict.h                             **
**                                                                          **
**       OUTPUTS: The pointer to the Packed byte parameter is returned.     **
\* **************************************************************************/
unsigned char *fnFlashGetPackedByteParm(unsigned short  wParmID)
{
    unsigned char   *pParamPackedByteIndex,
                    *pPackedByteIndex,
                    *pTablePackedByteIndex,
                    *pParamPackedBytes = NULL_PTR;
    unsigned long   tablePackedByteIndex,
                    packedByteIndex,
                    paramPackedByteIndex;

#ifdef PCN_PARM_SUBSTITUTION
    if( fnGetSubstitutePCNParm(wParmID, TYPE_PB, (void *) &pParamPackedBytes) == TRUE )
        return( pParamPackedBytes );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * packed byte parameter offset and the packed byte table offset.
     * Get the offset for the requested parameter from the packed byte table
     * by adding the table offset to the param type pointer plus the parameter
     * offset.  Copy the packed byte offset from the table and add it
     * to the pointer to the start of the packed bytes to get the pointer
     * to the requested packed byte parameter.
     */
    if(wParmID < MAX_PACKED_BYTE_PARM)
    {
        pParamPackedByteIndex = pParamTypeIndex + PACKED_BYTE_PARAM_OFFSET * sizeof(unsigned long); 
        EndianAwareCopy(&paramPackedByteIndex, pParamPackedByteIndex, sizeof(unsigned long));
        pTablePackedByteIndex = pParamTypeIndex + PACKED_BYTE_TABLE_OFFSET * sizeof(unsigned long); 
        EndianAwareCopy(&tablePackedByteIndex, pTablePackedByteIndex, sizeof(unsigned long));
        pPackedByteIndex = pParamTypeIndex + tablePackedByteIndex + wParmID * sizeof(unsigned long);    
        EndianAwareCopy(&packedByteIndex, pPackedByteIndex, sizeof(unsigned long));
        pParamPackedBytes = pParamTypeIndex + paramPackedByteIndex;

        //The pointer to the packed byte offset table should be less than the pointer to the start of the packed bytes
        if( pPackedByteIndex >= pParamPackedBytes )
            pParamPackedBytes = NULL_PTR;
        else
            pParamPackedBytes = (unsigned char *) (pParamPackedBytes + packedByteIndex);
    }
    return (pParamPackedBytes);
}

/* **********************************************************************
// FUNCTION NAME: fnFlashGetWordParm
// DESCRIPTION: Gets a word-sized context parameter out of flash.
//                NOTE: It is the responsibility of the caller to range     **
//                check the paramater id, as this routine does none.        **
// AUTHOR: Joe Mozdzer
//
// INPUTS:  wParmID - ID number of the word parameter.  The ID's are
//                      defined in datdict.h
//
// OUTPUTS: The word parameter value is returned.
// *************************************************************************/
unsigned short fnFlashGetWordParm(unsigned short wParmID)
{
    unsigned char   *pParamWordIndex,
                    *pParamWords;
    unsigned long   paramWordIndex;
    unsigned short  wordParam;

#ifdef PCN_PARM_SUBSTITUTION
    if( fnGetSubstitutePCNParm(wParmID, TYPE_WP, (void *) &wordParam) == TRUE )
         return( wordParam );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * word parameter offset.  Copy the offset and add it to the parameter
     * type index to get the pointer to the word parameters.  Use the parameter
     * id as an offset to return the value of the parameter requested.
     */
    pParamWordIndex = pParamTypeIndex + WORD_PARAM_OFFSET * sizeof(unsigned long);  
    EndianAwareCopy(&paramWordIndex, pParamWordIndex, sizeof(unsigned long));
    pParamWords = paramWordIndex + pParamTypeIndex;
    EndianAwareCopy(&wordParam, pParamWords + (wParmID * sizeof (unsigned short)), sizeof (unsigned short));
    return (wordParam);
}

/* **********************************************************************
// FUNCTION NAME: fnFlashGetLongParm
// DESCRIPTION: Gets a long-sized context parameter out of flash.
//                NOTE: It is the responsibility of the caller to range     **
//                check the paramater id, as this routine does none.        **
// AUTHOR: Joe Mozdzer
//
// INPUTS:  wParmID - ID number of the long parameter.  The ID's are
//                      defined in datdict.h
//
// OUTPUTS: The long parameter value is returned.
// *************************************************************************/
unsigned long fnFlashGetLongParm(unsigned short wParmID)
{
    unsigned char   *pParamLongIndex,
                    *pParamLongs;
    unsigned long   paramLongIndex,
                    longParam;

#ifdef PCN_PARM_SUBSTITUTION
    if( fnGetSubstitutePCNParm(wParmID, TYPE_LP, (void *) &longParam) == TRUE )
         return( longParam );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * long parameter offset.  Copy the offset and add it to the parameter
     * type index to get the pointer to the long parameters.  Use the parameter
     * id as an offset to return the value of the parameter requested.
     */
    pParamLongIndex = pParamTypeIndex + LONG_PARAM_OFFSET * sizeof(unsigned long);  
    EndianAwareCopy(&paramLongIndex, pParamLongIndex, sizeof(unsigned long));
    pParamLongs = paramLongIndex + pParamTypeIndex;
    EndianAwareCopy(&longParam, pParamLongs + (wParmID * sizeof (unsigned long)), sizeof (unsigned long));
    return (longParam);
}

/* **********************************************************************
// FUNCTION NAME: fnFlashGetMoneyParm
// DESCRIPTION: Gets a money parameter out of flash.
//                NOTE: It is the responsibility of the caller to range     **
//                check the paramater id, as this routine does none.        **
// AUTHOR: Joe Mozdzer
//
// INPUTS:  wParmID - ID number of the money parameter.  The ID's are
//                      defined in datdict.h
//
// OUTPUTS: A pointer to the 5-byte money parameter is returned.  The calling
//          routine must use a memcpy if it wishes to save a local copy of
//          the parameter value.
// *************************************************************************/
unsigned char * fnFlashGetMoneyParm(unsigned short wParmID)
{
    unsigned char   *pParamMoneyIndex,
                    *pParamMoneys = NULL_PTR;
    unsigned long   paramMoneyIndex;

#ifdef PCN_PARM_SUBSTITUTION
    if( fnGetSubstitutePCNParm(wParmID, TYPE_MP, (void *) &pParamMoneys) == TRUE )
         return( pParamMoneys );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * money parameter offset.  Copy the offset and add it to the parameter
     * type index to get the pointer to the money parameters.  Use the parameter
     * id as an offset to return the pointer to the parameter requested.
     */
    if(wParmID < MAX_MONETARY_PARM)
    {
        pParamMoneyIndex = pParamTypeIndex + MONETARY_PARAM_OFFSET * sizeof(unsigned long); 
        EndianAwareCopy(&paramMoneyIndex, pParamMoneyIndex, sizeof(unsigned long));
        pParamMoneys = paramMoneyIndex + pParamTypeIndex + wParmID * SPARK_MONEY_SIZE;
    }
    return (pParamMoneys);
}

/* **********************************************************************
// FUNCTION NAME: fnFlashGetAsciiStringParm
// DESCRIPTION: Gets an ascii string parameter out of flash.
//                NOTE: It is the responsibility of the caller to range     **
//                check the paramater id, as this routine does none.        **
// AUTHOR: Joe Mozdzer
//
// INPUTS:  wParmID - ID number of the string parameter.  The ID's are
//                      defined in datdict.h
//
// OUTPUTS: A pointer to the string parameter is returned.  The calling
//          routine is responsible for knowing how to derive the length.
//          In some cases the string is null terminated; in others, it may
//          be a fixed length.
// *************************************************************************/
unsigned char * fnFlashGetAsciiStringParm(unsigned short wParmID)
{
    unsigned char   *pParamAsciiStrIndex,
                    *pAsciiStrIndex,
                    *pTableAsciiStrIndex,
                    *pParamAsciiStrs = NULL_PTR;
    unsigned long   tableAsciiStrIndex,
                    asciiStrIndex,
                    paramAsciiStrIndex;

#ifdef PCN_PARM_SUBSTITUTION
    if( fnGetSubstitutePCNParm(wParmID, TYPE_ASP, (void *) &pParamAsciiStrs) == TRUE )
         return( pParamAsciiStrs );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * Ascii string parameter offset and the Ascii string table offset.
     * Get the offset for the requested parameter from the ascii string table
     * by adding the table offset to the param type pointer plus the parameter
     * offset.  Copy the ascii string offset from the table and add it
     * to the pointer to the start of the ascii strings to get the pointer
     * to the requested ascii string parameter.
     */
    if(wParmID < MAX_ASCII_STR_PARM)
    {
        pParamAsciiStrIndex = pParamTypeIndex + ASCII_STRING_PARAM_OFFSET * sizeof(unsigned long);  
        EndianAwareCopy(&paramAsciiStrIndex, pParamAsciiStrIndex, sizeof(unsigned long));

        pTableAsciiStrIndex = pParamTypeIndex + ASCII_STRING_TABLE_OFFSET * sizeof(unsigned long);  
        EndianAwareCopy(&tableAsciiStrIndex, pTableAsciiStrIndex, sizeof(unsigned long));

        pAsciiStrIndex 		= pParamTypeIndex + tableAsciiStrIndex + wParmID * sizeof(unsigned long);
        EndianAwareCopy(&asciiStrIndex, pAsciiStrIndex, sizeof(unsigned long));

        pParamAsciiStrs 	= pParamTypeIndex + paramAsciiStrIndex;

        //The pointer to the ascii string offset table should be less than 
        //the pointer to the start of the ascii strings
        if( pAsciiStrIndex >= pParamAsciiStrs )
            pParamAsciiStrs = NULL_PTR;
        else
            pParamAsciiStrs 	= (unsigned char *) (pParamAsciiStrs + asciiStrIndex);
    }
    return (pParamAsciiStrs);
}

/* **********************************************************************
// FUNCTION NAME: fnFlashGetUnicodeStringParm
// DESCRIPTION: Gets a unicode string parameter out of flash.
//                NOTE: It is the responsibility of the caller to range     **
//                check the paramater id, as this routine does none.        **
// AUTHOR: Joe Mozdzer
//
// INPUTS:  wParmID - ID number of the string parameter.  The ID's are
//                      defined in datdict.h
//
// OUTPUTS: A pointer to the string parameter is returned.  The calling
//          routine is responsible for knowing how to derive the length.
//          In some cases the string is null terminated; in others, it may
//          be a fixed length.
//
// NOTE:  The function assumes that all of the Unicode string parameters
//          start on a word boundary.
// *************************************************************************/
unsigned short * fnFlashGetUnicodeStringParm(unsigned short wParmID)
{
    unsigned char   *pParamUnicodeStrIndex,
                    *pUnicodeStrIndex,
                    *pTableUnicodeStrIndex,
                    *pParamUnicodeStrs = NULL_PTR;
    unsigned long   tableUnicodeStrIndex,
                    unicodeStrIndex,
                    paramUnicodeStrIndex;

#ifdef PCN_PARM_SUBSTITUTION
    if( fnGetSubstitutePCNParm(wParmID, TYPE_USP, &pParamUnicodeStrs) == TRUE )
        return( (unsigned short *)pParamUnicodeStrs );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * Unicode string parameter offset and the Unicode string table offset.
     * Get the offset for the requested parameter from the Unicode string table
     * by adding the table offset to the param type pointer plus the parameter
     * offset.  Copy the unicode string offset from the table and add it
     * to the pointer to the start of the unicode strings to get the pointer
     * to the requested unicode string parameter.
     */
    if(wParmID < MAX_UNICODE_PARM)
    {
        pParamUnicodeStrIndex = pParamTypeIndex + UNICODE_STRING_PARAM_OFFSET * sizeof(unsigned long);  
        EndianAwareCopy(&paramUnicodeStrIndex, pParamUnicodeStrIndex, sizeof(unsigned long));
        pTableUnicodeStrIndex = pParamTypeIndex + UNICODE_STRING_TABLE_OFFSET * sizeof(unsigned long);  
        EndianAwareCopy(&tableUnicodeStrIndex, pTableUnicodeStrIndex, sizeof(unsigned long));
        pUnicodeStrIndex = pParamTypeIndex + tableUnicodeStrIndex + wParmID * sizeof(unsigned long);    
        EndianAwareCopy(&unicodeStrIndex, pUnicodeStrIndex, sizeof(unsigned long));
        pParamUnicodeStrs = pParamTypeIndex + paramUnicodeStrIndex;

        //The pointer to the unicode string offset table should be less than
        //the pointer to the start of the unicode strings
        if( pUnicodeStrIndex >= pParamUnicodeStrs )
            pParamUnicodeStrs = NULL_PTR;
        else
            pParamUnicodeStrs = (unsigned char *) (pParamUnicodeStrs + unicodeStrIndex);
    }
    return ((unsigned short *) pParamUnicodeStrs);
}



/* **********************************************************************
// FUNCTION NAME: fnJFontGetFontCompAddr
// DESCRIPTION: Gets a pointer to a janus printing font component.
// AUTHOR: Craig DeFilippo
//
// INPUTS:  ppFontComponent - Location where the pointer is returned.
//          bFontID - The id number of the font.  The EMD's id number.
//
// OUTPUTS: SUCCESSFUL - if the font id is valid.  The pointer is returned
//                          at the address specified by ppDOFFont.
//          FAILURE - if the flash does not contain the requested font.
//
// *************************************************************************/
BOOL fnJFontGetFontCompAddr(void ** ppJFontComponent, unsigned char bFontID)
{
    void            **pFontIndex;
    BOOL            fRetval = SUCCESSFUL;
    unsigned char   bFontConstant;
    uchar           *pAbsAddr, *pFontMap;
    ushort          usFontIndex;

    /* get the total number of fonts */
    bFontConstant = *(GET_BYTE_CONSTANT(COFFSET_JFONTS_CNT));

    pFontMap = (uchar*) fnFlashGetAddress(FONT_MAP_DC);
    usFontIndex = 0;
    while(usFontIndex < bFontConstant)
    {
        if(*(pFontMap+usFontIndex) == bFontID )
            break;  
        usFontIndex++;
    }

    if(usFontIndex < bFontConstant)
    {
        /* get a pointer to the array of font offsets */
        pFontIndex = (void **) pDirectory.FontIndex;

        /* translate the requested font's offset into an absolute address   */
        EndianAwareCopy(&pAbsAddr, &pFontIndex[usFontIndex], sizeof(char *));
        pAbsAddr += (unsigned long)Current_Top_Of_Flash;
        *ppJFontComponent = (void **)pAbsAddr;
    }
    else
    {
        fRetval = FALSE;
    }

    return (fRetval);
}
    
/* **********************************************************************
// FUNCTION NAME: fnFlashGetVersionID
// DESCRIPTION: Gets a pointer to the character string for the flash 
//              version ID.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none
//
// OUTPUTS: returns a pointer to the flash version ID ascii string.
//
// *************************************************************************/
char * fnFlashGetVersionID()
{
    char *  pRetval;

    pRetval = ((char *) pDirectory.Constants + COFFSET_VERSION_ID);

    return (pRetval);
}

/* **********************************************************************
// FUNCTION NAME: fnFlashGetPCN
// DESCRIPTION: Gets a pointer to the character string for the PCN
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none
//
// OUTPUTS: returns a pointer to the flash PCN string.
//
// *************************************************************************/
char * fnFlashGetPCN()
{
    char *  pRetval;

    pRetval = ((char *) pDirectory.Constants + COFFSET_PCN);

    return (pRetval);
}

/* **************************************************************************
// FUNCTION NAME: fnJFontGetFontBitmapSize
// 
// OUTPUTS: Returns the size of the bitmap portion of the font component.
// *************************************************************************/
 
unsigned short  fnJFontGetFontBitmapSize(void *pJFontComponent)
{
    unsigned short  wRetval;

    EndianAwareCopy(&wRetval, ((unsigned char *) pJFontComponent) + JFCOFFSET_BITMAP_SIZE, sizeof(unsigned short));

    return (wRetval);
}

/* **************************************************************************
// FUNCTION NAME: fnJFontGetFontBitmapAddr
// 
// OUTPUTS: Returns the address where the bit map portion of the font
//          component is located.
// *************************************************************************/
unsigned char * fnJFontGetFontBitmapAddr(void *pJFontComponent)
{
    unsigned char * pRetval;
    unsigned short usBitmapOffset;

    EndianAwareCopy(&usBitmapOffset, ((unsigned char *) pJFontComponent) +
                                    JFCOFFSET_BITMAP_OFFSET, sizeof(unsigned short));
    pRetval = ((unsigned char *) pJFontComponent) + usBitmapOffset;

    return (pRetval);
}

/* **************************************************************************
// FUNCTION NAME: fnJFontGetCharHeight
// 
// OUTPUTS: Returns the character height of the J font component.
// *************************************************************************/
/*
unsigned short fnJFontGetCharHeight(void *pJFontComponent)
{
    unsigned short  wRetval;

    memcpy(&wRetval, ((unsigned char *) pJFontComponent) + JFCOFFSET_CHAR_HEIGHT, sizeof(unsigned short));

    return (wRetval);
}
*/
/* **************************************************************************
// FUNCTION NAME: fnJFontGetCharWidth
// 
// OUTPUTS: Returns the character width of the J font component.
// *************************************************************************/
/*
unsigned short fnJFontGetCharWidth(void *pJFontComponent)
{
    unsigned short  wRetval;

    memcpy(&wRetval, ((unsigned char *) pJFontComponent) + JFCOFFSET_CHAR_WIDTH, sizeof(unsigned short));

    return (wRetval);
}
*/

/* **************************************************************************
// FUNCTION NAME: fnJFontGetNumChar
// 
// OUTPUTS: Returns the number of characters in the J font component.
// *************************************************************************/
unsigned short fnJFontGetNumChar(void *pJFontComponent)
{
    unsigned short  wRetval;

    EndianAwareCopy(&wRetval, ((unsigned char *) pJFontComponent) + JFCOFFSET_NUM_CHAR, sizeof(unsigned short));

    return (wRetval);
}

/* **************************************************************************
// FUNCTION NAME: fnJFontGetHashAndSigAddr
// 
// OUTPUTS: Returns the address of the font's hash and signature.
// *************************************************************************/
unsigned char * fnJFontGetHashAndSigAddr(void *pJFontComponent, ushort* pHashAndSigSize)
{
    unsigned char * pRetval;
    unsigned char * pHss;

    pHss = ((unsigned char *) pJFontComponent) + JFCOFFSET_UNI_CHAR_CODES + (fnJFontGetNumChar(pJFontComponent) * sizeof(ushort)) + JFONT_DISPLAY_NAME_SZ + JFONT_GRAPHIC_DATA_NAME_SZ;

    EndianAwareCopy( (uchar*)pHashAndSigSize, pHss, sizeof(unsigned short));

    pRetval = pHss + JFONT_SZ_OF_HASH_AND_SIG_SZ;

    return (pRetval);
}


/* **************************************************************************
// Author : craig defilippo
// FUNCTION NAME: fnFlashGetNumFonts
// 
// OUTPUTS: Returns the number of printing fonts.
// *************************************************************************/
unsigned char fnFlashGetNumFonts(void)
{
    return(*GET_BYTE_CONSTANT(COFFSET_JFONTS_CNT));
}
/* **************************************************************************
// Author : craig defilippo
// FUNCTION NAME: fnFlashGetNumRegionMapRecs
// 
// OUTPUTS: Returns the number of region maps.
// *************************************************************************/
unsigned char fnFlashGetNumRegionMapRecs(void)
{
    return(*GET_BYTE_CONSTANT(COFFSET_REGION_MAPS_CNT));
}

/* **************************************************************************
// Author : sandra peterson
// FUNCTION NAME: fnFlashGetNumReports
// 
// OUTPUTS: Returns the number of reports.
// *************************************************************************/
unsigned char fnFlashGetNumReports(void)
{
    unsigned char ucCount = *(GET_BYTE_CONSTANT(COFFSET_REPORT_TYPES_CNT));


    // until GCS starts loading the value in the EMD,
    // use the number of reports defined in the software.
    if (!ucCount)
        ucCount = REPORT_IDS;

    return(ucCount);
}


/* ----- Screen-related flash functions ----- */

/* **********************************************************************
// FUNCTION NAME: fnFlashGetNumLanguages
// DESCRIPTION: Returns the number of display languages available.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none
//
// OUTPUTS: returns the number of languages available
// *************************************************************************/
unsigned char fnFlashGetNumLanguages()
{
#ifdef GET_SCREENS_FROM_FLASH
  if( NoEMDPresent == TRUE )
    return(1);
  else
    return( fnFlashGetByteParm(BP_NUM_DISPLAY_LANG_AVAIL) );
#else
    return( bstubNumLangs );
#endif
}

/* **********************************************************************
// FUNCTION NAME: fnFlashGetTree
// DESCRIPTION: Returns a pointer to the start of the screen tree component.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none
//
// OUTPUTS: pointer to the tree component is returned
// *************************************************************************/
void * fnFlashGetTree()
{
    unsigned char   *pTree;

#ifdef GET_SCREENS_FROM_FLASH
    pTree = (unsigned char *) fnFlashGetAddress(SCREEN_TREE_DC);
    /* for now, skip over the checksum and size inserted by GCS */
    pTree += 8;
#else

#ifndef SCREEN_GENERATOR_411
    pTree = (unsigned char *) cArrayScreenHdr;
#else
    pTree = (unsigned char *) cArrayScreen;
#endif

#endif
    
    pTree += 20;    //temp fix for screen tool/oitmsg.c compression incompatibility

    return ((void *) pTree);
}

/* **********************************************************************
// FUNCTION NAME: fnFlashGetLanguageName
// DESCRIPTION: Returns a pointer to the start of the Unicode string that
//              describes the name of a language.
// AUTHOR: Joe Mozdzer, Craig DeFilippo
//
// INPUTS:  bLangID- the language for which the name is being requested.  0 is
//                      the first language, 1 the second, ...
//
// OUTPUTS: pointer to the null-terminated Unicode string.
// *************************************************************************/
void * fnFlashGetLanguageName(unsigned char bLangID)
{
    void            *pRetval;
    unsigned short  *pNamePtr;

#ifdef GET_SCREENS_FROM_FLASH
    /* make sure caller is not requesting name of a language that doesn't exist */
    if (bLangID >= *(GET_BYTE_CONSTANT(COFFSET_LANGUAGES_CNT)))
    {
        pRetval = NULL_PTR;
        goto xit;
    }

    /* get a pointer to the language descriptions index.  At this address is an array of unicode strings 
     each of length SZ_LANGUAGE_NAME. The array will be dimensioned according to the number of languages */
    pNamePtr = (unsigned short *) fnFlashGetAddress(LANG_DESC_DC);
    // index to the name requested
    pNamePtr += (bLangID * SZ_LANGUAGE_NAME);

    pRetval = (void *)pNamePtr;

#else
    if (bLangID >= bstubNumLangs)
        pRetval = NULL_PTR;
    else
        switch (bLangID)
        {
            case 0:
                pRetval = (void *) pstubLang1Name;
                break;

            case 1:
                pRetval = (void *) pstubLang2Name;
                break;

            case 2:
                pRetval = (void *) pstubLang3Name;
                break;

            default:
                pRetval = NULL_PTR;
        }
#endif

#ifdef GET_SCREENS_FROM_FLASH
xit:
#endif
    return (pRetval);
}


/* **********************************************************************
// FUNCTION NAME: fnFlashGetLanguage
// DESCRIPTION: Returns a pointer to the start of a language component.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  bLangID- the language component being requested.  0 is the
//                      first language, 1 the second, ...
//
// OUTPUTS: pointer to the language component.  null is returned if an illegal
//          language ID was passed in.
// *************************************************************************/
void * fnFlashGetLanguage(unsigned char bLangID)
{
    UINT8       *pRetval = NULL_PTR;
    UINT8       *pCh;
    UINT32      ulVal = 0;

#ifdef GET_SCREENS_FROM_FLASH
    // If language is out of range, use the first language.
    if( bLangID >= *(GET_BYTE_CONSTANT(COFFSET_LANGUAGES_CNT)) )
    {
        bLangID = 0;
    }

    pCh = (UINT8 *)fnFlashGetAddress( SCREENS_DC );
    if( pCh != NULL_PTR )
    {
        pCh += ( bLangID * sizeof( UINT32 ));
        EndianAwareCopy( &ulVal , pCh , sizeof(UINT32) );
        pRetval = ((UINT8 *)Current_Top_Of_Flash + ulVal);
        pRetval += 8;   /* skip over the GCS generated size and checksum */
    }
#else
    if( bLangID >= bstubNumLangs )
    {
        bLangID = 0;
    }
    switch (bLangID)
    {
#ifndef SCREEN_GENERATOR_411
        case 0:
            pRetval = (UINT8 *)cArrayLangHdr;
            break;

        case 1:
            pRetval = (UINT8 *)cArrayLang2Hdr;
            break;

        case 2:
            pRetval = (UINT8 *)cArrayLang3Hdr;
            break;
#else
        case 0:
            pRetval = (UINT8 *)cArrayLang;
            break;

        case 1:
            pRetval = (UINT8 *)cArrayLang2;
            break;

        case 2:
            pRetval = (UINT8 *)cArrayLang3;
            break;
#endif
        default:
            pRetval = NULL_PTR;
    }
#endif

//xit:

    if( pRetval != NULL_PTR )
        pRetval += 20;  //temp fix for screen tool/oitmsg.c compression incompatibility

    return( pRetval );
}

/* **********************************************************************
// FUNCTION NAME: fnFlashGetFont
// DESCRIPTION: Returns a pointer to the start of a font component.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  bLangID- the language currently being displayed.  0 is the
//                      first language, 1 the second, and so on.  the
//                      language is necessary because the font ID is relative
//                      to it (e.g. language 0's font 2 may be different than
//                      language 1's font 2)
//
//          bFontID- the font being requested.  see above comment.
//
// OUTPUTS: pointer to the font component.  null is returned if an illegal 
//          font or language ID was passed in.
// *************************************************************************/
void * fnFlashGetFont(unsigned char bLangID, unsigned char bFontID)
{
    unsigned char   *pRetval = NULL_PTR;
    unsigned long   *pFontOffset;
    unsigned char   bMaxScrFontsPerLang;
    UINT8           bHardwareMajorVersion;
    UINT8           bDontCare;
    UINT8           i;
    struct  sRemap
    {
        unsigned char   bDM475FontID;
        unsigned char   bDM300FontID;
    } rFontRemapTable[] =   // new hw ID        old hw ID
                            {   { 0,                3 },
                                { 1,                4 },
                                { 2,                5 },
                                { 0xFF,             3 } };  // 0xFF is used for the default entry

//#ifndef JANUS
#ifdef GET_SCREENS_FROM_FLASH

    bMaxScrFontsPerLang = *(GET_BYTE_CONSTANT(COFFSET_SCRFONTS_CNT));

    if ((bLangID >= *(GET_BYTE_CONSTANT(COFFSET_LANGUAGES_CNT))) ||
        (bFontID >= bMaxScrFontsPerLang))
    {
        pRetval = NULL_PTR;
        goto xit;
    }

    /* for each language, there are bMaxScrFontsPerLang pointers (for the language's
        font0, font1, ..., font[bMaxScrFontsPerLang-1].  if a language uses less
        than bMaxScrFontsPerLang fonts, a null pointer is used.  the clump of code
            below uses the language and font selection to find the appropriate pointer. */
    pFontOffset = (unsigned long *) fnFlashGetAddress(SCREENS_FONT_INDEX_DC);
#ifndef G900    // (ML) 12/20/06 - Fonts same for all Langs in Future Phoenix! 
    pFontOffset += (bLangID * bMaxScrFontsPerLang);
#endif

    pFontOffset += bFontID;
    if (EndianAwareGet32(pFontOffset) != NULL_VAL)
    {
        pRetval = ((unsigned char *) Current_Top_Of_Flash + EndianAwareGet32(pFontOffset));

        /* for now, we skip over the checksum and size put in by GCS */
        pRetval += 8;
    }
    else
        pRetval = NULL_PTR;
#else
    if ((bLangID > MAX_STUB_LANGS - 1) || (bFontID > MAX_STUB_FONTS_PER_LANG - 1))
        pRetval = NULL_PTR;
    else
    {
        fnGetHardwareVersion(&bHardwareMajorVersion, &bDontCare);
        if (bHardwareMajorVersion <= FP_MAIN_BD_HW_VERSION1)
            pRetval = (unsigned char *) pstubLangFontTable[bLangID][bFontID];
        else
            pRetval = (unsigned char *) pstubLangFontTableDM475[bLangID][bFontID];
    }
#endif
//#endif

#ifdef GET_SCREENS_FROM_FLASH
xit:
#endif
    return ((void *) pRetval);

}



/*****************************************************************************
    wrappers for I-Button Parameters
*****************************************************************************/
/******************************************************************************
   FUNCTION NAME: fnFlashGetIBByteParam
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Gets a byte-sized I- Button context parameter out of flash
   PARAMETERS   : wParmID - ID number of the IB byte parameter.  The ID's are
                : defined in datdict.h
   RETURN       : the byte parameter value
   NOTES        : It is the responsibility of the caller to range check the
                : paramater id, as this routine does not.
******************************************************************************/
unsigned char fnFlashGetIBByteParm(unsigned short   wParmID)
{
    unsigned char   *pIBParamByteIndex,
                    *pIBParamBytes;
    unsigned long   ulIBParamByteIndex;

#ifdef PCN_PARM_SUBSTITUTION
    unsigned char   bTempValue=0;

    if( fnGetSubstitutePCNParm(wParmID, TYPE_IBP, (void *) &bTempValue) == TRUE )
         return( bTempValue );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * IB byte parameter offset.  Copy the offset and add it to the parameter
     * type index to get the pointer to the IB byte parameters.  Use the parameter
     * id as an offset to return the value of the parameter requested.
     */
    pIBParamByteIndex = pParamTypeIndex + IB_BYTE_PARAM_OFFSET * sizeof(unsigned long);
    EndianAwareCopy(&ulIBParamByteIndex, pIBParamByteIndex, sizeof(unsigned long));
    pIBParamBytes = ulIBParamByteIndex + pParamTypeIndex;
    return (pIBParamBytes[wParmID]);
}

/******************************************************************************
   FUNCTION NAME: fnFlashGetIBWordParam
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Gets a word-sized I- Button context parameter out of flash
   PARAMETERS   : wParmID - ID number of the IB word parameter.  The ID's are
                : defined in datdict.h
   RETURN       : the word parameter value
   NOTES        : It is the responsibility of the caller to range check the
                : paramater id, as this routine does none.
******************************************************************************/
unsigned short fnFlashGetIBWordParm(unsigned short wParmID)
{
    unsigned char   *pIBParamWordIndex,
                    *pIBParamWords;
    unsigned long   ulIBParamWordIndex;
    unsigned short  usWordParam;

#ifdef PCN_PARM_SUBSTITUTION
    if( fnGetSubstitutePCNParm(wParmID, TYPE_IWP, (void *) &usWordParam) == TRUE )
         return( usWordParam );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * IB word parameter offset.  Copy the offset and add it to the parameter
     * type index to get the pointer to the IB word parameters.  Use the parameter
     * id as an offset to return the value of the parameter requested.
     */
    pIBParamWordIndex = pParamTypeIndex + IB_WORD_PARAM_OFFSET * sizeof(unsigned long); 
    EndianAwareCopy(&ulIBParamWordIndex, pIBParamWordIndex, sizeof(unsigned long));
    pIBParamWords = ulIBParamWordIndex + pParamTypeIndex;
    EndianAwareCopy(&usWordParam, pIBParamWords + (wParmID * sizeof (unsigned short)), sizeof (unsigned short));
    return (usWordParam);
}

/******************************************************************************
   FUNCTION NAME: fnFlashGetIBLongParam
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Gets a long-sized I- Button context parameter out of flash
   PARAMETERS   : wParmID - ID number of the IB long parameter.  The ID's are
                : defined in datdict.h
   RETURN       : the long parameter value
   NOTES        : It is the responsibility of the caller to range check the
                : paramater id, as this routine does none.
******************************************************************************/
unsigned long fnFlashGetIBLongParm(unsigned short wParmID)
{
    unsigned char   *pIBParamLongIndex,
                    *pIBParamLongs;
    unsigned long   ulIBParamLongIndex,
                    ulLongParam;
#ifdef PCN_PARM_SUBSTITUTION
    if( fnGetSubstitutePCNParm(wParmID, TYPE_ILP, (void *) &ulLongParam) == TRUE )
         return( ulLongParam );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * IB long parameter offset.  Copy the offset and add it to the parameter
     * type index to get the pointer to the IB long parameters.  Use the parameter
     * id as an offset to return the value of the parameter requested.
     */
    pIBParamLongIndex = pParamTypeIndex + IB_LONG_PARAM_OFFSET * sizeof(unsigned long); 
    EndianAwareCopy(&ulIBParamLongIndex, pIBParamLongIndex, sizeof(unsigned long));
    pIBParamLongs = ulIBParamLongIndex + pParamTypeIndex;
    EndianAwareCopy(&ulLongParam, pIBParamLongs + wParmID * sizeof (unsigned long), sizeof (unsigned long));
    return (ulLongParam);
}

/******************************************************************************
   FUNCTION NAME: fnFlashGetIBMoneyParam
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Gets an I-Button money parameter out of flash.
   PARAMETERS   : wParmID - ID number of the IB money parameter.  The ID's are
                : defined in datdict.h
   RETURN       : A pointer to the 5-byte money parameter is returned.  The calling
                : routine must use a memcpy if it wishes to save a local copy of
                : the parameter value.
   NOTES        : It is the responsibility of the caller to range
                : check the paramater id, as this routine does none.
******************************************************************************/
unsigned char * fnFlashGetIBMoneyParm(unsigned short wParmID)
{
    unsigned char   *pIBParamMoneyIndex,
                    *pIBParamMoneys = NULL_PTR;
    unsigned long   ulIBParamMoneyIndex;

#ifdef PCN_PARM_SUBSTITUTION
    if( fnGetSubstitutePCNParm(wParmID, TYPE_IMP, (void *) &pIBParamMoneys) == TRUE )
         return( pIBParamMoneys );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * IB money parameter offset.  Copy the offset and add it to the parameter
     * type index to get the pointer to the IB money parameters.  Use the parameter
     * id as an offset to return the pointer to the parameter requested.
     */
    if(wParmID < MAX_IMONEY_PARM)     //lint !e568 !e685   MAX_IMONEY_PARM may be zero.
    {
        pIBParamMoneyIndex = pParamTypeIndex + IB_MONETARY_PARAM_OFFSET * sizeof(unsigned long);    
        EndianAwareCopy(&ulIBParamMoneyIndex, pIBParamMoneyIndex, sizeof(unsigned long));
        pIBParamMoneys = ulIBParamMoneyIndex + pParamTypeIndex + wParmID * SPARK_MONEY_SIZE;
    }
    return (pIBParamMoneys);
}

/******************************************************************************
   FUNCTION NAME: fnFlashGetIBStringParam
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Gets an I-Button ascii string parameter out of flash.
   PARAMETERS   : wParmID - ID number of the IB string parameter.  The ID's are
                : defined in datdict.h
   RETURN       : A pointer to the IB string parameter is returned.  The calling
                : routine is responsible for knowing how to derive the length.
                : In some cases the string is null terminated; in others, it may
                : be a fixed length.
   NOTES        : It is the responsibility of the caller to range
                : check the paramater id, as this routine does none.
******************************************************************************/
unsigned char * fnFlashGetIBAsciiStringParm(unsigned short wParmID)
{
    unsigned char   *pIBParamAsciiStrIndex,
                    *pIBAsciiStrIndex,
                    *pIBTableAsciiStrIndex,
                    *pIBParamAsciiStrs = NULL_PTR;
    unsigned long   ulIBTableAsciiStrIndex,
                    ulIBAsciiStrIndex,
                    ulIBParamAsciiStrIndex;

#ifdef PCN_PARM_SUBSTITUTION
    if( fnGetSubstitutePCNParm(wParmID, TYPE_ISP, (void *) &pIBParamAsciiStrs) == TRUE )
         return( pIBParamAsciiStrs );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * IB Ascii string parameter offset and the IB Ascii string table offset.
     * Get the offset for the requested parameter from the IB ascii string table
     * by adding the IB table offset to the IB param type pointer plus the parameter
     * offset.  Copy the IB ascii string offset from the table and add it
     * to the pointer to the start of the IB ascii strings to get the pointer
     * to the requested IB ascii string parameter.
     */
    if(wParmID < MAX_ISTRING_PARM)      //lint !e568 !e685   MAX_ISTRING_PARM may be zero.
    {
        pIBParamAsciiStrIndex = pParamTypeIndex + IB_ASCII_STRING_PARAM_OFFSET * sizeof(unsigned long); 
        EndianAwareCopy(&ulIBParamAsciiStrIndex, pIBParamAsciiStrIndex, sizeof(unsigned long));
        pIBTableAsciiStrIndex = pParamTypeIndex + IB_ASCII_STRING_TABLE_OFFSET * sizeof(unsigned long); 
        EndianAwareCopy(&ulIBTableAsciiStrIndex, pIBTableAsciiStrIndex, sizeof(unsigned long));
        pIBAsciiStrIndex = pParamTypeIndex + ulIBTableAsciiStrIndex + wParmID * sizeof(unsigned long);  
        EndianAwareCopy(&ulIBAsciiStrIndex, pIBAsciiStrIndex, sizeof(unsigned long));
        pIBParamAsciiStrs = pParamTypeIndex + ulIBParamAsciiStrIndex;
        pIBParamAsciiStrs = (unsigned char *) (pIBParamAsciiStrs + ulIBAsciiStrIndex);
    }
    return (pIBParamAsciiStrs);
}

/******************************************************************************
   FUNCTION NAME: fnFlashGetIBSUnicodedStringParam
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Gets an I-Button unicode string parameter out of flash.
                : 
   PARAMETERS   : wParmID - ID number of the IB unicode string parameter.  The ID's are
                : defined in datdict.h
   RETURN       : A pointer to the IB unicode string parameter is returned.  The calling
                : routine is responsible for knowing how to derive the length.
                : In some cases the string is null terminated; in others, it may
                : be a fixed length.
   NOTES        : It is the responsibility of the caller to range
                :  check the paramater id, as this routine does none.
                : The function assumes that all of the Unicode string parameters
                :  start on a word boundary.
******************************************************************************/
unsigned short * fnFlashGetIBUnicodeStringParm(unsigned short wParmID)
{
    unsigned char   *pIBParamUnicodeStrIndex,
                    *pIBUnicodeStrIndex,
                    *pIBTableUnicodeStrIndex,
                    *pIBParamUnicodeStrs = NULL_PTR;
    unsigned long   ulIBTableUnicodeStrIndex,
                    ulIBUnicodeStrIndex,
                    ulIBParamUnicodeStrIndex;

#ifdef PCN_PARM_SUBSTITUTION
    if( fnGetSubstitutePCNParm(wParmID, TYPE_IUSP, (void *) &pIBParamUnicodeStrs) == TRUE )
        return( (unsigned short *)pIBParamUnicodeStrs );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * IB Unicode string parameter offset and the IB Unicode string table offset.
     * Get the offset for the requested parameter from the IB Unicode string table
     * by adding the IB table offset to the IB param type pointer plus the parameter
     * offset.  Copy the IB unicode string offset from the table and add it
     * to the pointer to the start of the IB unicode strings to get the pointer
     * to the requested IB unicode string parameter.
     */
    if(wParmID < MAX_IUNICODE_PARM)         //lint !e568 !e685   MAX_IUNICODE_PARM may be zero.
    {
        pIBParamUnicodeStrIndex = pParamTypeIndex + IB_UNICODE_STRING_PARAM_OFFSET * sizeof(unsigned long); 
        EndianAwareCopy(&ulIBParamUnicodeStrIndex, pIBParamUnicodeStrIndex, sizeof(unsigned long));
        pIBTableUnicodeStrIndex = pParamTypeIndex + IB_UNICODE_STRING_TABLE_OFFSET * sizeof(unsigned long); 
        EndianAwareCopy(&ulIBTableUnicodeStrIndex, pIBTableUnicodeStrIndex, sizeof(unsigned long));
        pIBUnicodeStrIndex = pParamTypeIndex + ulIBTableUnicodeStrIndex + wParmID * sizeof(unsigned long);  
        EndianAwareCopy(&ulIBUnicodeStrIndex, pIBUnicodeStrIndex, sizeof(unsigned long));
        pIBParamUnicodeStrs = pParamTypeIndex + ulIBParamUnicodeStrIndex;
        pIBParamUnicodeStrs = (unsigned char *) (pIBParamUnicodeStrs + ulIBUnicodeStrIndex);
    }
    return ((unsigned short *) pIBParamUnicodeStrs);
}

/******************************************************************************
   FUNCTION NAME: fnFlashGetIBPackedByteParam
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Gets a IB Packed byte context pcn parameter out of flash.
                : This assumes that the active context has been initialized.
   PARAMETERS   : wParmID - ID number of the IB Packed byte parameter.  The
                : ID's are defined in datdict.h
   RETURN       : The pointer to the Packed byte parameter is returned.
   NOTES        : It is the responsibility of the caller to range
                : check the paramater id, as this routine does none.
******************************************************************************/
unsigned char *fnFlashGetIBPackedByteParm(unsigned short wParmID)
{
    unsigned char   *pIBParamPackedByteIndex,
                    *pIBPackedByteIndex,
                    *pIBTablePackedByteIndex,
                    *pIBParamPackedBytes = NULL_PTR;
    unsigned long   ulIBTablePackedByteIndex,
                    ulIBPackedByteIndex,
                    ulIBParamPackedByteIndex;

#ifdef PCN_PARM_SUBSTITUTION
    if( fnGetSubstitutePCNParm(wParmID, TYPE_IPB, (void *) &pIBParamPackedBytes) == TRUE )
        return( (unsigned char *)pIBParamPackedBytes );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * IB packed byte parameter offset and the IB packed byte table offset.
     * Get the offset for the requested parameter from the IB packed byte table
     * by adding the table offset to the param type pointer plus the parameter
     * offset.  Copy the IB packed byte offset from the table and add it
     * to the pointer to the start of the IB packed bytes to get the pointer
     * to the requested packed byte parameter.
     */
    if(wParmID < MAX_IPACKED_PARM)
    {
        pIBParamPackedByteIndex = pParamTypeIndex + IB_PACKED_BYTE_PARAM_OFFSET * sizeof(unsigned long);    
        EndianAwareCopy(&ulIBParamPackedByteIndex, pIBParamPackedByteIndex, sizeof(unsigned long));
        pIBTablePackedByteIndex = pParamTypeIndex + IB_PACKED_BYTE_TABLE_OFFSET * sizeof(unsigned long);    
        EndianAwareCopy(&ulIBTablePackedByteIndex, pIBTablePackedByteIndex, sizeof(unsigned long));
        pIBPackedByteIndex = pParamTypeIndex + ulIBTablePackedByteIndex + wParmID * sizeof(unsigned long);  
        EndianAwareCopy(&ulIBPackedByteIndex, pIBPackedByteIndex, sizeof(unsigned long));
        pIBParamPackedBytes = pParamTypeIndex + ulIBParamPackedByteIndex;

        //The pointer to the IB packed byte offset table should be less than 
        //the pointer to the start of the IB packed bytes
        if( pIBPackedByteIndex >= pIBParamPackedBytes )
            pIBParamPackedBytes = NULL_PTR;
        else
            pIBParamPackedBytes = (unsigned char *) (pIBParamPackedBytes + ulIBPackedByteIndex);
    }
    return (pIBParamPackedBytes);
}



/******************************************************************************
   FUNCTION NAME: fnFlashGetIBMultiPackedByteParam
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Gets a IB Multi Packed byte context pcn parameter out of flash.
                : This assumes that the active context has been initialized.
   PARAMETERS   : wParmID - ID number of the IB Packed byte parameter.  The
                : ID's are defined in datdict.h
   RETURN       : The pointer to the Multi Packed byte parameter is returned.
   NOTES        : It is the responsibility of the caller to range
                : check the paramater id, as this routine does none.
******************************************************************************/
unsigned char *fnFlashGetIBMPackedByteParm(unsigned short wParmID)
{
    unsigned char   *pIBParamMPackedByteIndex,
                    *pIBMPackedByteIndex,
                    *pIBTableMPackedByteIndex,
                    *pIBParamMPackedBytes = NULL_PTR;
    unsigned long   ulIBTableMPackedByteIndex,
                    ulIBMPackedByteIndex,
                    ulIBParamMPackedByteIndex;

#ifdef PCN_PARM_SUBSTITUTION
    if( fnGetSubstitutePCNParm(wParmID, TYPE_IMPB, (void *) &pIBParamMPackedBytes) == TRUE )
        return( (unsigned char *)pIBParamMPackedBytes );
#endif

    /*
     * Using the parameter type index pointer, calculate the pointer to the 
     * IB multi packed byte parameter offset and the IB multi packed byte table offset.
     * Get the offset for the requested parameter from the IB multi packed byte table
     * by adding the table offset to the param type pointer plus the parameter
     * offset.  Copy the IB multi packed byte offset from the table and add it
     * to the pointer to the start of the IB multi packed bytes to get the pointer
     * to the requested multi packed byte parameter.
     */
    if(wParmID < MAX_IMULTIPACKED_PARM)
    {
        pIBParamMPackedByteIndex = pParamTypeIndex + IB_MULTI_PACKED_BYTE_PARAM_OFFSET * sizeof(unsigned long); 
        EndianAwareCopy(&ulIBParamMPackedByteIndex, pIBParamMPackedByteIndex, sizeof(unsigned long));
        pIBTableMPackedByteIndex = pParamTypeIndex + IB_MULTI_PACKED_BYTE_TABLE_OFFSET * sizeof(unsigned long); 
        EndianAwareCopy(&ulIBTableMPackedByteIndex, pIBTableMPackedByteIndex, sizeof(unsigned long));
        pIBMPackedByteIndex = pParamTypeIndex + ulIBTableMPackedByteIndex + wParmID * sizeof(unsigned long);    
        EndianAwareCopy(&ulIBMPackedByteIndex, pIBMPackedByteIndex, sizeof(unsigned long));
        pIBParamMPackedBytes = pParamTypeIndex + ulIBParamMPackedByteIndex;

        //The pointer to the IB multi packed byte offset table should be less than 
        //the pointer to the start of the IB multi packed bytes
        if( pIBMPackedByteIndex >= pIBParamMPackedBytes )
            pIBParamMPackedBytes = NULL_PTR;
        else
            pIBParamMPackedBytes = (unsigned char *) (pIBParamMPackedBytes + ulIBMPackedByteIndex);
    }
    return (pIBParamMPackedBytes);
}

/******************************************************************************
   FUNCTION NAME: fnFlashGetIBPNumFields
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Return the number of fields in a IBP record.
   PARAMETERS   : wRecID = Record Type to search
   RETURN       : number of fields in the record
   NOTES        : Can be used on IPB_GET_INDICIA_DEF_REC or
                : IPB_CREATE_INDICIA_DEF_REC, or IPB_BUILD_BC_DEF_REC
******************************************************************************/
ushort fnFlashGetIBPNumFields(unsigned short wRecID)
{
    ushort usNumFields = 0;
    uchar* pRecord;

    pRecord = fnFlashGetIBPackedByteParm(wRecID);

    if(pRecord)
    	EndianAwareCopy(&usNumFields, pRecord, sizeof(ushort));

    return (usNumFields);
}

/******************************************************************************
   FUNCTION NAME: fnFlashGetIBPRecVar
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Find a field record with an ID that matches the one passed in.
                : Will search the type of record ID passed in.
   PARAMETERS   : wRecID = Record Type to search
                : wVarID = Varaible Type to search for
                : pass back a stucture with the data record
   RETURN       : FALSE if no matching variable ID found
   NOTES        : Can be used to search IPB_GET_INDICIA_DEF_REC or
                : IPB_CREATE_INDICIA_DEF_REC
******************************************************************************/
BOOL fnFlashGetIBPRecVar(IPSD_DEF_REC* pFieldRec, unsigned short wRecID, unsigned short wVarID)
{
    BOOL fRetVal = FALSE;
    uchar* pRecord;
    ushort i, usFieldID, usNumFields;

    pRecord = fnFlashGetIBPackedByteParm(wRecID);

    EndianAwareCopy(&usNumFields, pRecord, sizeof(ushort));
    pRecord += sizeof(ushort);

    for (i = 0; i<usNumFields; i++)
    {
    	EndianAwareCopy(&usFieldID, pRecord, sizeof(ushort));
        if(usFieldID == wVarID)
        {
        	EndianAwareCopy(&pFieldRec->usFieldID, pRecord + IBP_DCR_FLD_ID_OFF, sizeof(ushort));
        	EndianAwareCopy(&pFieldRec->usFieldOff, pRecord + IBP_DCR_FLD_OFF_OFF, sizeof(ushort));
        	EndianAwareCopy(&pFieldRec->usFieldLen, pRecord + IBP_DCR_FLD_LEN_OFF, sizeof(ushort));
            memcpy(&pFieldRec->ucFieldOp, pRecord + IBP_DCR_FLD_OP_OFF, sizeof(uchar));
            //memcpy(&pFieldRec->ucSpare, pRecord + IBP_DCR_FLD_SPR_OFF, sizeof(uchar));
            fRetVal = TRUE;
            break;
        }
        pRecord += IBP_DCR_SIZE;
    } 

    return (fRetVal);
}

/******************************************************************************
   FUNCTION NAME: fnFlashGetIBPBcDefRecVar
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Find a field record with an ID that matches the one passed in.
                : Will search the type of record ID passed in.
   PARAMETERS   : wRecID = Record Type to search
                : wVarID = Varaible Type to search for
                : pass back a stucture with the data record
   RETURN       : FALSE if no matching variable ID found
   NOTES        : Can be used to search IPB_BUILD_BC_DEF_REC and like types
******************************************************************************/
BOOL fnFlashGetIBPBcDefRecVar(IPSD_BC_DEF_REC* pFieldRec, unsigned short wRecID, unsigned short wVarID)
{
    BOOL fRetVal = FALSE;
    uchar* pRecord;
    ushort i, usFieldID, usNumFields;

    pRecord = fnFlashGetIBPackedByteParm(wRecID);

    EndianAwareCopy(&usNumFields, pRecord, sizeof(ushort));
    pRecord += sizeof(ushort);

    for (i = 0; i<usNumFields; i++)
    {
    	EndianAwareCopy(&usFieldID, pRecord, sizeof(ushort));
        if(usFieldID == wVarID)
        {
        	EndianAwareCopy(&pFieldRec->usFieldID, pRecord + IBP_DCR_FLD_ID_OFF, sizeof(ushort));
        	EndianAwareCopy(&pFieldRec->usFieldOff, pRecord + IBP_DCR_FLD_OFF_OFF, sizeof(ushort));
        	EndianAwareCopy(&pFieldRec->usFieldLen, pRecord + IBP_DCR_FLD_LEN_OFF, sizeof(ushort));
        	memcpy(&pFieldRec->ucFieldOp, pRecord + IBP_DCR_FLD_OP_OFF, sizeof(uchar));
        	EndianAwareCopy(&pFieldRec->usFieldFn, pRecord + IBP_DCR_FLD_FN_OFF, sizeof(ushort));
            memcpy(&pFieldRec->ucFieldPad, pRecord + IBP_DCR_FLD_PAD_OFF, sizeof(uchar));
            memcpy(&pFieldRec->ucFieldJust, pRecord + IBP_DCR_FLD_OP_OFF, sizeof(uchar));
            fRetVal = TRUE;
            break;
        }
        pRecord += IBP_BC_REC_SIZE;
    } 

    return (fRetVal);
}

/******************************************************************************
   FUNCTION NAME: fnFlashGetIBPBcDefRecVarOrdered
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Find the nth field record in a IPSD_BC_DEF_REC type record
                : Will search the type of record ID passed in.
   PARAMETERS   : wRecID = Record Type to search
                : wID = Ordinal number of record to get (starting at 0)
                : pass back a stucture with the data record
   RETURN       : FALSE if ordinal is out of range
   NOTES        : Can be used to IPB_BUILD_BC_DEF_REC
******************************************************************************/
BOOL fnFlashGetIBPBcDefRecVarOrdered(IPSD_BC_DEF_REC* pFieldRec, unsigned short wRecID, unsigned short wID)
{
    BOOL fRetVal = FALSE;
    uchar* pRecord;
    ushort usNumFields;

    pRecord = fnFlashGetIBPackedByteParm(wRecID);

    EndianAwareCopy(&usNumFields, pRecord, sizeof(ushort));
    pRecord += sizeof(ushort);

    if (wID < usNumFields)
    {
        pRecord += (IBP_BC_REC_SIZE *  wID);
        EndianAwareCopy(&pFieldRec->usFieldID, pRecord + IBP_DCR_FLD_ID_OFF, sizeof(ushort));
        EndianAwareCopy(&pFieldRec->usFieldOff, pRecord + IBP_DCR_FLD_OFF_OFF, sizeof(ushort));
        EndianAwareCopy(&pFieldRec->usFieldLen, pRecord + IBP_DCR_FLD_LEN_OFF, sizeof(ushort));
        memcpy(&pFieldRec->ucFieldOp, pRecord + IBP_DCR_FLD_OP_OFF, sizeof(uchar));
        EndianAwareCopy(&pFieldRec->usFieldFn, pRecord + IBP_DCR_FLD_FN_OFF, sizeof(ushort));
        memcpy(&pFieldRec->ucFieldPad, pRecord + IBP_DCR_FLD_PAD_OFF, sizeof(uchar));
        memcpy(&pFieldRec->ucFieldJust, pRecord + IBP_DCR_FLD_JUST_OFF, sizeof(uchar));
        fRetVal = TRUE;
    } 

    return (fRetVal);
}

/******************************************************************************
   FUNCTION NAME: fnFlashGetMainDirEmdVersion
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Get the EMD version from the main directory. Systems populates
                : this with EMD.PCNattributes.FlashCompatibility.  Designed to 
                : allow EMD and screen binaries compatibility checking.
   PARAMETERS   : None
   RETURN       : EMD version
   NOTES        : None
******************************************************************************/
unsigned long fnFlashGetMainDirEmdVersion(void)
{
    return(pDirectory.EmdCompatibility);
}


/******************************************************************************
   FUNCTION NAME: fnFlashVerifyIndividualCheckSum
   AUTHOR       : Howell Sun
   DESCRIPTION  : Modified from fnFlashVerifyCheckSum().Calculate the checksum 
                  and compare it with the pre-calculated one. Note this is 
                  only for manufacture build. For emulator build, it will 
                  return TRUE.

   PARAMETERS   : type   0: application, others not supported yet

   RETURN       : FALSE: Checksum does not match
                  TRUE:  Checksum matches
   NOTES        : Application used checksum while RM uses CRC16
******************************************************************************/
BOOL fnFlashVerifyIndividualCheckSum( uchar        type,          //(i) type 
                                      ulong        *calculatedCS) //(o) calcuated checksum  
{
    extern const unsigned long lRFDOffset;
    extern const unsigned long lRFDSize;
    extern const unsigned long lRFDChecksum;

    ulong   *pAddress;
    UINT16  *pHdrCrc;
	
	unsigned long	Size = 0;
	unsigned long	pctCheckSum = 0;
	unsigned long	value;
	unsigned long	Blocks;				/* The number of four byte blocks */
	unsigned long	ulInx1;

	unsigned short	wRemainder;			/* The number of bytes remaining */

	char	cLogBuf[50];
    BOOL    retVal = FALSE;

//    unsigned long   ii;
//    unsigned short  jj = 0;
//    unsigned char   bLastBlockCopy[4] = {0,0,0,0},
//                    *pLastBlock;
//    char buf[ 2 * SYSLOG_MAX_DATA +1];   // The buffer is only big enough for 2 lines.
    
                    
	// initialize the calculated checksum
	*calculatedCS = 0;
	
    switch(type)
    {
        case CHECKSUM_APP:
            pAddress = (ulong *)&lRFDOffset + APP_HEADER_LEN;  //lint !e416    Yeah, it looks out of range.
            Size = lRFDSize;
            pctCheckSum = lRFDChecksum;

			(void)sprintf(cLogBuf, "*** App Addr = %08lX", (unsigned long)pAddress);
			fnDumpStringToSystemLog(cLogBuf);
			(void)sprintf(cLogBuf, "*** App Size = %08lX", Size);
			fnDumpStringToSystemLog(cLogBuf);
			(void)sprintf(cLogBuf, "*** App Checksum =  %08lX", pctCheckSum);
			fnDumpStringToSystemLog(cLogBuf);
           
            // If the application size and checksum are non-zero
            if (pctCheckSum != 0 && Size != 0)  
            {
	            /*
	            * No need to subtract the size of the checksum header from the area size
	            * as the stored size has already had the header size subtracted.
	            * Get the number of four byte blocks.  Get the number of bytes remaining 
	            * in the last block.  Calculate the checksum for the blocks, copying the 
	            * blocks bytewise since they may not be on a long word boundary.  Then copy
	            * the last block filling the missing bytes with zeros (initialized that
	            * way).  Add the last block to the checksum.  If the calculated check sum
	            * is equal to the checksum passed in return true.  Otherwise return false.
	            */

	            Blocks = Size / 4;
	            wRemainder = Size % 4;

				(void)sprintf(cLogBuf, "*** App Blocks = %08lX", Blocks);
				fnDumpStringToSystemLog(cLogBuf);
				(void)sprintf(cLogBuf, "*** App Leftover = %08X", wRemainder);
				fnDumpStringToSystemLog(cLogBuf);

				for (ulInx1 = 0; ulInx1 < Blocks; ulInx1++)
	            {
	                (void)memcpy( &value, pAddress, sizeof(ulong) );      //lint !e670 
	                pAddress++;         //lint !e662    It looks out of bounds, but it shouldn't be.
	                *calculatedCS += value;
	            }

				if (wRemainder) 
				{
					value = 0;
					(void)memcpy( &value, pAddress, wRemainder);
					*calculatedCS += value;
				}

				(void)sprintf(cLogBuf, "*** Calc Checksum = %08lX", *calculatedCS);
				fnDumpStringToSystemLog(cLogBuf);

	            if (*calculatedCS == pctCheckSum)
	            {
	                retVal = TRUE;
	            }
	            else
	            {
	                retVal = FALSE;
	            }
            }
            else
            {
				// probably running on the emulator
                retVal = TRUE;
            }
			
            break; // CHECKSUM_APP

        // Else, unknown checksum type, so return the default status of FALSE.
        default:
			(void)sprintf(cLogBuf, "!!! Invalid Cksum Type = %u", type);
			fnDumpStringToSystemLog(cLogBuf);
            break;
    }

    return (retVal);

} // fnFlashVerifyIndividualCheckSum()


/* *************************************************************************
// FUNCTION NAME: 
//         fnIsADSloganRequired
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function checks 
//         whether AD slogan is required.
//
// INPUTS:
//         None
//
// RETURNS:
//         TRUE  - if AD slogan is required.
//         FALSE - if it is not.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      04/24/2006    James Wang     Initial version  
//
// *************************************************************************/
BOOL fnIsADSloganRequired(void)
{
    return (fnFlashGetByteParm(BP_AD_SLOGAN_REQUIRED) != 0)? TRUE : FALSE;
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetDecimalChar
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets the 
//         decimal point character from flash.
//
// INPUTS:
//         None
//
// RETURNS:
//         The decimal point character.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      02/28/2006    James Wang     Initial version  
//
// *************************************************************************/
UINT16 fnGetDecimalChar(void)
{
    return fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR);
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnIsKIPAllowed
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function checks 
//         if KIP mode is allowed.
//
// INPUTS:
//         None
//
// RETURNS:
//         TRUE - if KIP is allowed.
//         FALSE - if it is not.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//  2011.01.27 Clarisa Bellamy - Use existing macro, for consistency, to mask
//                      the flags off of the control value before it is 
//                      compared.
//      02/28/2006    James Wang     Initial version  
//
// *************************************************************************/
BOOL fnIsKIPAllowed (void)
{
    BOOL    fKipAllowed = TRUE;

    if( fnFlashGetKIPModeControlVal() == KIP_NOT_ALLOWED )
    {
//        fKipAllowed == FALSE;
    }

    return( fKipAllowed );
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetTxtmsgMaxAllowed
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets 
//         text message max allowed number from flash.
//
// INPUTS:
//         None
//
// RETURNS:
//         text message max allowed number.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/18/2006    James Wang     Initial version  
//
// *************************************************************************/
UINT8 fnGetTxtmsgMaxAllowed(void)
{
    return fnFlashGetByteParm(BP_TXTMSG_MAX_ALLOWED);
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetTxtmsgMaxCharsAllowed
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets 
//         text message max characters number allowed in one line from flash.
//
// INPUTS:
//         None
//
// RETURNS:
//         The text message max characters number allowed in one line.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/18/2006    James Wang     Initial version  
//
// *************************************************************************/
UINT8 fnGetTxtmsgMaxCharsAllowed(void)
{
    return fnFlashGetByteParm(BP_TXTMSG_MAXCHARS_LINE_ALLOWED);
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetMaxADSloganNum
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets 
//         max AD slogan number from flash.
//
// INPUTS:
//         None
//
// RETURNS:
//         The max AD slogan number.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/18/2006    James Wang     Initial version  
//
// *************************************************************************/
UINT8 fnGetMaxADSloganNum(void)
{
    return fnFlashGetByteParm(BP_MAX_AD_SLOGAN);
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetInscSupportedNum
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets 
//         supported Inscription number from flash.
//
// INPUTS:
//         None
//
// RETURNS:
//         The supported Inscription number.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/18/2006    James Wang     Initial version  
//
// *************************************************************************/
UINT8 fnGetInscSupportedNum(void)
{
    return fnFlashGetByteParm(BP_INSC_SUPPORTED);
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetMaxPermitAllowedNum
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets 
//         allowed max Permit number from flash.
//
// INPUTS:
//         None
//
// RETURNS:
//         The max Permit allowed number.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/18/2006    James Wang     Initial version  
//
// *************************************************************************/
UINT8 fnGetMaxPermitAllowedNum(void)
{
    return fnFlashGetByteParm(BP_PERMIT_MAX_ALLOWED);
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetMinDisplayedIntegers
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets 
//         the minimum displayed integers from flash.
//
// INPUTS:
//         None
//
// RETURNS:
//         The minimum displayed integers.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/18/2006    James Wang     Initial version  
//
// *************************************************************************/
UINT8 fnGetMinDisplayedIntegers(void)
{
    return fnFlashGetByteParm(BP_MIN_DISPLAYED_INTEGERS);
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetMinDisplayedDecimals
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets 
//         the minimum displayed decimals from flash.
//
// INPUTS:
//         None
//
// RETURNS:
//         The minimum displayed decimals.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/18/2006    James Wang     Initial version  
//
// *************************************************************************/
UINT8 fnGetMinDisplayedDecimals(void)
{
    return fnFlashGetByteParm(BP_MIN_DISPLAYED_DECIMALS);
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetKIPMode
//
// DESCRIPTION:
//         This function gets KIP mode from flash.  
//          It masks off the flags to leave only the KIP control mode.
//
// INPUTS:
//         None
//
// RETURNS:
//         KIP control mode, can be one of the following value:
//              KIP_NOT_ALLOWED
//              KIP_ALLOWED
//              KIP_ALLOWED_PASSWORD
//              KIP_ALLOWED_VALIDATION
//              KIP_FROM_FEE_LIST_ONLY
//              KIP_ALLOWED_FENCINGLIMIT
//              KIP_ALLOWED_USE_MDS_TOKENS
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//  2011.01.27 Clarisa Bellamy - Replace body, which only masked one of the flags,
//                   with existing macro, to be more consistent.
//      05/18/2006    James Wang     Initial version  
//
// *************************************************************************/
UINT8 fnGetKIPMode(void)
{
    return( fnFlashGetKIPModeControlVal() );
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnIsLockCodeDisableAllowed
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function checks 
//         whether a user is allowed to turn on/off the use of the 
//         locking code feature in the UIC.
//
// INPUTS:
//         None
//
// RETURNS:
//         TRUE - if allowed.
//         FALSE - if it is not.
//
// WARNINGS/NOTES:  
//         This flash variable is actually FALSE if disabling IS allowed.
//
// MODIFICATION HISTORY:
//      05/19/2006    James Wang     Initial version  
//
// *************************************************************************/
BOOL fnIsLockCodeDisableAllowed(void)
{
    return (fnFlashGetByteParm(BP_ALLOW_LOCK_CODE_DISABLE) == FALSE)? TRUE : FALSE;
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnIsSuperPasswordDisableAllowed
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function checks 
//         whether a user is allowed to turn on/off the use of the 
//         supervisor password feature in the UIC.
//
// INPUTS:
//         None
//
// RETURNS:
//         TRUE - if allowed.
//         FALSE - if it is not.
//
// WARNINGS/NOTES:  
//         This flash variable is actually FALSE if disabling IS allowed.
//
// MODIFICATION HISTORY:
//      05/19/2006    James Wang     Initial version  
//
// *************************************************************************/
BOOL fnIsSuperPasswordDisableAllowed(void)
{
    return (fnFlashGetByteParm(BP_ALLOW_SUPER_PASSWORD_DISABLE) == FALSE)? TRUE : FALSE;
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetEMDVersion
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets 
//         the EMD version from flash.
//
// INPUTS:
//         None
//
// RETURNS:
//         The EMD version.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/18/2006    James Wang     Initial version  
//
// *************************************************************************/
UINT16 fnGetEMDVersion(void)
{

#ifdef GET_SCREENS_FROM_FLASH
	if (NoEMDPresent == TRUE)
#else
    if (RamEMDPresent == FALSE)
#endif
		return 0;
	else
		return fnFlashGetWordParm(WP_UIC_EMD_PARM_VER);
}
 

/* *************************************************************************
// FUNCTION NAME: 
//         fnGetUicPsdMarriedState
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets 
//         the UIC/PSD marriage state from flash.
//
// INPUTS:
//         None
//
// RETURNS:
//         The UIC/PSD marriage state.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/18/2006    James Wang     Initial version  
//
// *************************************************************************/
UINT8 fnGetUicPsdMarriedState(void)
{
    return fnFlashGetByteParm(BP_SAME_VAULT_REQUIRED);
}

/* *************************************************************************
// FUNCTION NAME: fnIsMemKeyAcctingEnabled
//
// DESCRIPTION: 
//
// INPUTS:
//      None.
//
// RETURNS:
//      TRUE or FALSE
//
// WARNINGS/NOTES:
//          
// MODIFICATION HISTORY:
//      Phil Jacques   Initial version
// *************************************************************************/
BOOL fnIsMemKeyAcctingEnabled(void)
{
    BOOL fStatus;

    if (fnFlashGetByteParm(BP_MEM_KEYS_USE_ACCT_NUM) == TRUE)
    {
        fStatus = TRUE;
    }
    else
    {
        fStatus = FALSE;
    }

    return (fStatus);
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnGetTCType
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets the 
//         type of TC based on the given print mode.
//
// INPUTS:
//         ucMode = print mode
//
// RETURNS:
//         The type of TC -         TC_TYPE_NONE
//                                  TC_TYPE_CIRCLE
//                                  TC_TYPE_STRIP.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/18/2006    John Gao    Initial version  
//
// *************************************************************************/
UINT8 fnGetTCType(unsigned char ucMode)
{
	switch (ucMode)
	{
		case PMODE_PERMIT:
		    return fnFlashGetByteParm(BP_PERMIT_TC_TYPE);
			break;

		default:
		    return fnFlashGetByteParm(BP_TC_TYPE);
			break;
	}
	//TODO: fix return to correct value.
	return 0;
}


/* *************************************************************************
// FUNCTION NAME: fnIsDateDuckingAllowed
//
// DESCRIPTION: 
//    check if the date ducking is allowed based on the given print mode.
//
// INPUTS:
//         ucMode = print mode
//
// RETURNS:
//      TRUE or FALSE
//
// WARNINGS/NOTES:
//          
// MODIFICATION HISTORY:
//      05/18/2006    John Gao   Initial version
// *************************************************************************/
BOOL fnIsDateDuckingAllowed(unsigned char ucMode)
{
    BOOL fStatus;

	switch (ucMode)
	{
		case PMODE_PERMIT:
		    if (fnFlashGetByteParm(BP_PERMIT_DATE_DUCKING_ALLOWED) != 0)
		    {
		        fStatus = TRUE;
		    }
		    else
		    {
		        fStatus = FALSE;
		    }
			break;

		default:
		    if (fnFlashGetByteParm(BP_DATE_DUCKING_ALLOWED) != 0)
		    {
		        fStatus = TRUE;
		    }
		    else
		    {
		        fStatus = FALSE;
		    }
			break;
	}
	
    return (fStatus);
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnGetFactorySleepTimeout
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function gets the 
//         factory sleep time.
//
// INPUTS:
//         None
//
// RETURNS:
//         The factory setting of sleep time
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/19/2006    John Gao    Initial version  
//
// *************************************************************************/
UINT16 fnGetFactorySleepTimeout (void)
{
    return fnFlashGetWordParm(WP_FACTORY_SLEEP_TIMEOUT);
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnGetPBPTimeSyncEnable 
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function checks the PBP
//         TIem Sync Enable status.
//
// INPUTS:
//         None
//
// RETURNS:
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/22/2006    John Gao    Initial version  
//
// *************************************************************************/
UINT8 fnGetPBPTimeSyncEnable(void)
{
     return fnFlashGetByteParm(BP_PBP_TIME_SYNC_ENABLE);
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnGetDCAPIndiciaPwrFailRpt  
//
// DESCRIPTION:
//         Business rules encapsulate function.
//
// INPUTS:
//         None
//
// RETURNS:
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      05/22/2006    John Gao    Initial version  
//
// *************************************************************************/
UINT8 fnGetDCAPIndiciaPwrFailRpt(void)
{
    return fnFlashGetByteParm(BP_DCAP_INDICIA_PWR_FAIL_REPORT);
}


/* *************************************************************************
// FUNCTION NAME:           fnGetDefaultRatingCountryCode  
// DESCRIPTION:
//         Business rules encapsulate function.
// INPUTS:
//         None
// RETURNS:
// WARNINGS/NOTES:  
//         None
// MODIFICATION HISTORY:
//      07/14/2006    Oscar Wang    Initial version  
// *************************************************************************/
UINT16 fnGetDefaultRatingCountryCode(void)
{
    return fnFlashGetByteParm(BP_DEFAULT_RATING_COUNTRY_CODE );
}


/* *************************************************************************
// FUNCTION NAME:       fnIsDebitType 
// DESCRIPTION:
//         Check whether PBP Balance Type is Debit.
// INPUTS:
//         None
// RETURNS:
// WARNINGS/NOTES:  
//         None
// MODIFICATION HISTORY:
//      05/21/2007    Dan Zhang    Initial version  
// *************************************************************************/
BOOL  fnIsDebitType(void)
{
    UINT8       bBalanceType;

    bBalanceType = (UINT8)fnFlashGetByteParm(BP_ACCT_BALANCE_TYPE); 

    // 0: None 1: Debit 2: Credit 3: Both
    if (bBalanceType == 1 || bBalanceType == 3)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/* *************************************************************************
// FUNCTION NAME:       fnIsCreditType 
// DESCRIPTION:
//         Check whether PBP Balance Type is Credit.
// INPUTS:
//         None
// RETURNS:
// WARNINGS/NOTES:  
//         None
// MODIFICATION HISTORY:
//      05/21/2007    Dan Zhang    Initial version  
// *************************************************************************/
BOOL  fnIsCreditType(void)
{
    UINT8       bBalanceType;

    bBalanceType = (UINT8)fnFlashGetByteParm(BP_ACCT_BALANCE_TYPE); 

    // 0: None 1: Debit 2: Credit 3: Both
    if (bBalanceType == 2 || bBalanceType == 3)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

// ***************************************************************************
// FUNCTION:        fnFlashIsLowValueIndiciaSupported
// PURPOSE: 
//      To determine if this PCN supports a Low-Value Indicia of any kind.
//      Only EMD parameters and graphics are checked.
//      If the Low-value threshhold is > 0, or the EMD indicates to use 
//      the low value indicia for zero prints, then check to see if a low
//      value indicia graphic exists.  If one does NOT exist, but a Leading-bar 
//      graphic is required and a low-value leading-bar graphic exists, then
//      this PCN supports low-value indicia.       
//
// RETURNS: TRUE if low-value indicia is supported, else FALSE.     
// INPUTS:  None.         
// OUTPUTS: None.
//
// NOTES:       
//  1. Because only EMD parameters and EMD graphics are checked, the entire test
//     only needs to be run once after powerup.  So... 
//     Utilizes a static variable that is initialized on startup to a non-valid 
//     value to indicate that the entire test should be run.  Once the static  
//     variable has been set to a valid value, the function simply returns the 
//     value of that variable.
// HISTORY:
//  2008.07.31  Clarisa Bellamy - Initial Revision.
//---------------------------------------------------------------------------
static UINT8 bLowValueSupported = 0xFF;
BOOL fnFlashIsLowValueIndiciaSupported( void )
{
    UINT8   *pLowValueIndiciaThreshold;

    if( bLowValueSupported == 0xFF )
    {
        pLowValueIndiciaThreshold = fnFlashGetMoneyParm(MP_LOW_VALUE_INDICIA_THRESHOLD);

        if(    (   fnFlashGetByteParm(BP_USE_LOW_VALUE_INDICIA_FOR_0)
                || ( fnBinFive2Double(pLowValueIndiciaThreshold) > 0 ) )
           && (   (fnGrfxGetFirstLnkPtrByType(IMG_ID_LOW_INDICIA, GRFX_ANY_MATCH) != NULL_PTR) 
               || (   (fnFlashGetByteParm(BP_LEADING_BAR_REQUIRED) != 0) 
                   && (fnGrfxGetFirstLnkPtrByType(IMG_ID_LOWVAL_STD_LEADING_IMAGE, GRFX_ANY_MATCH) != NULL_PTR) ) ) )
        {
            bLowValueSupported = TRUE;                
        }
        else
        {
            bLowValueSupported = FALSE;                
        }
    }

    return( (BOOL)bLowValueSupported );
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnUpdateInscrSelection
//
// DESCRIPTION:
//         Business rules encapsulate function.  This function will force bob
//         to update the selected inscreption.
//
// INPUTS:
//         None
//
// RETURNS:
//         None
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      08/26/2009    Raymond Shen     Initial version  
//
// *************************************************************************/
void fnUpdateInscrSelection(void)
{
    BOOL    fRetval;
#ifndef JANUS  
    UINT8       bSelection;
#else
    UINT16      bSelection;
#endif

    fRetval = fnValidData( BOBAMAT0, BOBID_UPDATE_AND_READ_INSCR_SELECT, &bSelection );
    if( fRetval != BOB_OK )
    {
        fnProcessSCMError();
    }
}
// New wrapper functions belong ABOVE this line.

//----------------------------------------------------------------------------
//          Beginning of parameter substitution area.
//----------------------------------------------------------------------------

#ifdef PCN_PARM_SUBSTITUTION
//*****************************************************************************
// These functions are used to substitute temporary values for PCN parameters.
// They can be used for new parameters that do not exist in the EMD or to
// change existing parameters.  This function does not do any range checking.
// Be sure that all data arrays are null-terminated, if required.
//
//          THESE VALUES ARE ONLY USED IN ENGINEERING BUILDS
//*****************************************************************************
typedef struct {
    unsigned short  wParmID;        // Parameter enum from datadict.h
    unsigned char   wParmType;      // parameter types (TYPE_BP, TYPE_ASP etc) defined at the top of this file
    unsigned long   lwParmValue;    // Value to be returned
    void            *pParmPtr;      // Pointer to data to be returned
} PCN_PARM_TABLE_ENTRY;

// Constant arrays for parameter types larger than 4 bytes should be created here.
// The following two arrays are examples

/*
const SINT8 OneString[ 5 ] =  "1111";
const SINT8 BlankString[ 5 ] =  "    ";
const SINT8 EmptyString[ 5 ] =  "";

const SINT8 DcmPrefix[ 5 ] =  "G9US";

const unsigned short DefaultOneKeyMap[] = {  // Default key mapping table for '1' key
    0x0031, 0x002C, 0x002A, 0x002E, 0x003A, 0x0024, 0x002D, 0x003F, 
    0x002F, 0x005C, 0x0028, 0x0029, 0x0021, 0x0040, 0x002B, 0x0023,
    0x005E, 0x003D, 0x0026, 0x0025, 0x005F, 0x0022, 0x0000 };

const unsigned short DefaultNumKeyMap[] = {  // Default key mapping table for numeric keys 2-9
    0x0032, 0x0041, 0x0042, 0x0043, 0x0000, 0x0000, 0x0000, 0x0000,  // 2,A,B,C   
    0x0033, 0x0044, 0x0045, 0x0046, 0x0000, 0x0000, 0x0000, 0x0000,  // 3,D,E,F   
    0x0034, 0x0047, 0x0048, 0x0049, 0x0000, 0x0000, 0x0000, 0x0000,  // 4,G,H,I   
    0x0035, 0x004A, 0x004B, 0x004C, 0x0000, 0x0000, 0x0000, 0x0000,  // 5,J,K,L   
    0x0036, 0x004D, 0x004E, 0x004F, 0x0000, 0x0000, 0x0000, 0x0000,  // 6,M,N,O   
    0x0037, 0x0050, 0x0051, 0x0052, 0x0053, 0x0000, 0x0000, 0x0000,  // 7,P,Q,R,S 
    0x0038, 0x0054, 0x0055, 0x0056, 0x0000, 0x0000, 0x0000, 0x0000,  // 8,T,U,V   
    0x0039, 0x0057, 0x0058, 0x0059, 0x005A, 0x0000, 0x0000, 0x0000,  // 9,W,X,Y,Z 
    0x0032, 0x0061, 0x0062, 0x0063, 0x0000, 0x0000, 0x0000, 0x0000,  // 2,a,b,c   
    0x0033, 0x0064, 0x0065, 0x0066, 0x0000, 0x0000, 0x0000, 0x0000,  // 3,d,e,f   
    0x0034, 0x0067, 0x0068, 0x0069, 0x0000, 0x0000, 0x0000, 0x0000,  // 4,g,h,i   
    0x0035, 0x006A, 0x006B, 0x006C, 0x0000, 0x0000, 0x0000, 0x0000,  // 5,j,k,l   
    0x0036, 0x006D, 0x006E, 0x006F, 0x0000, 0x0000, 0x0000, 0x0000,  // 6,m,n,o   
    0x0037, 0x0070, 0x0071, 0x0072, 0x0073, 0x0000, 0x0000, 0x0000,  // 7,p,q,r,s 
    0x0038, 0x0074, 0x0075, 0x0076, 0x0000, 0x0000, 0x0000, 0x0000,  // 8,t,u,v   
    0x0039, 0x0077, 0x0078, 0x0079, 0x007A, 0x0000, 0x0000, 0x0000}; // 9,w,x,y,z 
*/

// For byte, word & long parameters, put the actual value of the parameter in the DATA element
// and a 0 in the POINTER element.
// Otherwise, put a 0 in the DATA element and the pointer to the definition of the parameter in
// the POINTER element. The definition of the parameter should be added immediately above.

const PCN_PARM_TABLE_ENTRY SubstPCNParameters[] =
{
//      PARAMETER ID                  TYPE      DATA   or   POINTER 

//	BP_AUTO_INSC_SUPPORTED,     	TYPE_BP,    0x04,       NULL_PTR,
//	WP_WOW_MAXIMUM_WEIGHT_LIMIT,    TYPE_WP,    0x01F4,  	NULL_PTR,
//	USP_1KEY_ALT_CHAR_KEYMAP,       TYPE_USP,   0,          DefaultOneKeyMap,
//	IBP_GEMINI_BATTERY_EOL,         TYPE_IBP,   0x60,       NULL_PTR,
//	IMPB_NUM_KEYS_ALT_CHAR_KEYMAP,  TYPE_IMPB,  0,          DefaultNumKeyMap,

    {END_OF_PARM_TABLE,              0,          0,          (VOID *)0x00} // End of table - DO NOT DELETE
};

//*****************************************************************************
//  fnGetSubstitutePCNParm():  Checks for substitute PCN parameter values
//  
//  Inputs: wParmID - Parameter ID defined in datdict.h
//          wParmType -  TYPE_BP, TYPE_WP, etc. defined at the top of this file
//          pParmValue - Pointer to address where parameter value is returned
//                       If the parameter is not found, this location is not
//                       modified.
//  Returns: TRUE if parameter found in table.
//           FALSE if parameter not found.
//
//*****************************************************************************
BOOL fnGetSubstitutePCNParm(unsigned short wParameterID, unsigned short wParameterType, void *pParmValue) {
    unsigned short  usIndex=0;
    unsigned long  *lwPtr;
    unsigned short *wPtr;
    unsigned char  *bPtr;
    unsigned char  **pbPtr;
    unsigned short **pwPtr;
    unsigned short  usTableSize = (sizeof(SubstPCNParameters)/sizeof(PCN_PARM_TABLE_ENTRY) );

    while ((SubstPCNParameters[usIndex].wParmID != END_OF_PARM_TABLE) && (usIndex <= usTableSize))
        {
        if ((SubstPCNParameters[usIndex].wParmID == wParameterID) &&
            (SubstPCNParameters[usIndex].wParmType == wParameterType))
            {
            switch (wParameterType)
                {
                case TYPE_MP:   // return pointer to unsigned char
                case TYPE_ASP:
                case TYPE_PB:
                case TYPE_MPB:
                case TYPE_IMP:
                case TYPE_ISP:
                case TYPE_IPB:
                case TYPE_IMPB:
                    pbPtr = (unsigned char **)pParmValue;
                    *pbPtr = (unsigned char *)SubstPCNParameters[usIndex].pParmPtr;
                    break;
                case TYPE_USP:  // returns pointer to unsigned short
                case TYPE_IUSP:
                    pwPtr = (unsigned short **)pParmValue;
                    *pwPtr = (unsigned short *)SubstPCNParameters[usIndex].pParmPtr;
                    break;
                case TYPE_LP:   // returns unsigned long
                case TYPE_ILP:
                    lwPtr = (unsigned long *)pParmValue;
                    *lwPtr = (unsigned long) SubstPCNParameters[usIndex].lwParmValue;
                    break;
                case TYPE_WP:   // returns unsigned short
                case TYPE_IWP:
                    wPtr = (unsigned short *)pParmValue;
                    *wPtr = (unsigned short) SubstPCNParameters[usIndex].lwParmValue;
                    break;
                case TYPE_BP:   // returns unsigned char
                case TYPE_IBP:
                default:
                    bPtr = (unsigned char *)pParmValue;
                    *bPtr = (unsigned char) SubstPCNParameters[usIndex].lwParmValue;
                    break;
                }
            return(TRUE);
            }
        usIndex++;
        } 
    return(FALSE);
}
#endif
//----------------------------------------------------------------------------
//  End of EMD Parameter Substitution constants and functions.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------------
// OK ...  Please don't put wrapper functions down here.  
//  Wrapper funcitons belong ABOVE the parameter substitution area.

/********************************************************************************/
/* TEST */

#ifdef TESTFLASH

void    TestFlash(void)
{
unsigned    short wwwordvar;

void        *indiciaIndex;

unsigned long   *pFA,
                Size, CS,
                longvar,
                normIndex,
                pGraphicComponent;
unsigned char   bcharvar,
                *pbchar,
                *pbconstants;

void            *pTmp,
                *pAddr;
char            pDest[100];

unsigned short  i =1,
                wwordvar,
                *pword;
BOOL            fResult;

    /*
     * i should be set to 1 to test the real flash.
     */

    if (i)
    {
        fnFlashInit();
        fnFlashDirectorySizeAndCS(&Size, &CS);
        fResult = fnFlashVerifyCheckSum( fnFlashGetAddress(COMPONENT_SIZE_DC), Size, CS );
    }
    /*
     * Validate Directory Offsets
     */

    pFA = fnFlashGetAddress(CONSTANTS_DC);
    pFA = fnFlashGetAddress(SUMS_INDEX_DC);
    pFA = fnFlashGetAddress(CONTEXT_PARMS_DC);
    pFA = fnFlashGetAddress(REPORTS_DC);
    pFA = fnFlashGetAddress(INDICIAS_DC);
    pFA = fnFlashGetAddress(CONTEXT_MAP_DC);
    pFA = fnFlashGetAddress(LANGUAGE_MAP_DC);
    pFA = fnFlashGetAddress(DOF_FONT_MAP_DC);
    pFA = fnFlashGetAddress(DOFINDEX_DC);
    pFA = fnFlashGetAddress(MODEM_CONTROL_DC);
    pFA = fnFlashGetAddress(SCREEN_TREE_DC);
    pFA = fnFlashGetAddress(SCREENS_DC);
    pFA = fnFlashGetAddress(SCREEN_FONT_XREF_DC);
    pFA = fnFlashGetAddress(LINKED_IMAGES_DC);
    pFA = fnFlashGetAddress(SERIAL_PRINTER_FONTS_DC);
    pFA = fnFlashGetAddress(REFILL_COMPONENT_DC);
    pFA = fnFlashGetAddress(PROFILE_DC);
    pFA = fnFlashGetAddress(LOOSE_ENDS_DC);
    pFA = fnFlashGetAddress(START_OF_CODE_DC);
    pFA = fnFlashGetAddress(START_OF_FREE_SPACE_DC);
    pFA = fnFlashGetAddress(SIZE_OF_FREE_SPACE_DC);
    pFA = fnFlashGetAddress(START_OF_FREE_SECTORS_DC);
    pFA = fnFlashGetAddress(NUMBER_OF_FREE_SECTORS_DC);
    pFA = fnFlashGetAddress(NUMBER_OF_FREE_SECTORS_DC + 1);
    pFA = fnFlashGetAddress(CHECKSUM_DC);

    /*
     * Validate Constants
     */
     
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_VERSION_ID);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_LANGUAGES_CNT);      
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_CONTEXTS_CNT);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_REPORT_TYPES_CNT);   
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_SUMS_CNT);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_MAX_SCR_FONTS_CNT);  
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_DOF_FONTS_CNT);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_INDICIA_TYPES_CNT);  
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_REFILL_COMPONENT_CNT);

    /*
     * Graphic Component
     */

    pFA = fnFlashGetAddress(INDICIAS_DC);
    EndianAwareCopy((void *)(&indiciaIndex), (void *)(pFA), 4);
    normIndex = (unsigned long)(indiciaIndex);
    pGraphicComponent = (unsigned long)Current_Top_Of_Flash;
    pGraphicComponent += normIndex;

    
    fResult = fnFlashGetGCcomponentSum((void *)pGraphicComponent, pDest);
    fResult = fnFlashGetGCcomponentSize((void *)pGraphicComponent, pDest);
    fResult = fnFlashVerifyCheckSum(fnFlashGetAddress(COMPONENT_SIZE_DC), Size, CS);

    fResult = fnFlashGetGCvisibility((void *)pGraphicComponent, pDest);
    fResult = fnFlashGetGCdownloadSize((void *)pGraphicComponent, pDest);
    fResult = fnFlashGetGCdownloadAddr((void *)pGraphicComponent, pDest);
    fResult = fnFlashGetGCcolumns((void *)pGraphicComponent, pDest);
    fResult = fnFlashGetGChash((void *)pGraphicComponent, pDest);
    fResult = fnFlashGetGCsignature((void *)pGraphicComponent, pDest);
    fResult = fnFlashGetGCdlchecksum((void *)pGraphicComponent, pDest);
    fResult = fnFlashGetGCfontsCnt((void *)pGraphicComponent, pDest);
    fResult = fnFlashGetGCregFieldCnt((void *)pGraphicComponent, pDest);
    fResult = fnFlashGetGCnamesCnt((void *)pGraphicComponent, pDest);
    fResult = fnFlashGetGCcomponentType((void *)pGraphicComponent, pDest);
    fResult = fnFlashGetGCunikodes((void *)pGraphicComponent, pDest);
    fResult = fnFlashGetGCfonts((void *)pGraphicComponent, pDest);
    fResult = fnFlashGetGCregisters((void *)pGraphicComponent, pDest);

    
    /*
     * Validate Report Indicies
     */

    pTmp = fnFlashGetReport(REFILL_RECEIPT_RPT);
    pTmp = fnFlashGetReport(REFILL_INSTR_RPT);
    pTmp = fnFlashGetReport(REFILL_STMT_RPT);
    pTmp = fnFlashGetReport(MULTI_ACCT_DETAIL_RPT);
    pTmp = fnFlashGetReport(MULTI_ACCT_SUM_RPT);
    pTmp = fnFlashGetReport(SINGLE_ACCT_RPT);
    pTmp = fnFlashGetReport(SHORT_SVC_RPT);
    pTmp = fnFlashGetReport(LONG_SVC_RPT);
    pTmp = fnFlashGetReport(REG_RPT);
    pTmp = fnFlashGetReport(CONFIG_RPT);
    pTmp = fnFlashGetReport(ERR_RPT);
    pTmp = fnFlashGetReport(PERMIT_RPT);
    pTmp = fnFlashGetReport(SETUP_RPT);
    pTmp = fnFlashGetReport(SETUP_RPT+1);
    pTmp = fnFlashGetReport(REFILL_RECEIPT_RPT);

    /*
     * Validate Report Indicies
     */

    pTmp = fnFlashGetIndicia(IMG_ID_NORMAL_INDICIA);
    pTmp = fnFlashGetIndicia(IMG_ID_LOW_INDICIA);
    pTmp = fnFlashGetIndicia(IMG_ID_TOWN_CIRCLE);
    pTmp = fnFlashGetIndicia(IMG_ID_SWISS_STAR);
    pTmp = fnFlashGetIndicia(IMG_ID_ENTGELT);
    pTmp = fnFlashGetIndicia(IMG_ID_PERMIT1_PE);
    pTmp = fnFlashGetIndicia(IMG_ID_PERMIT2_PE);
    pTmp = fnFlashGetIndicia(IMG_ID_PERMIT3_PE);
    pTmp = fnFlashGetIndicia(IMG_ID_PERMIT3_PE+1);
    pTmp = fnFlashGetIndicia(IMG_ID_NORMAL_INDICIA);

    bcharvar = fnFlashGetByteParm(BP_ACCESS_CODE_AVAIL);
    bcharvar = fnFlashGetByteParm(BP_MAX_INSC);
    bcharvar = fnFlashGetByteParm(BP_USE_LOW_VALUE_INDICIA_FOR_0);

    pword = fnFlashGetUnicodeStringParm(USP_DISP_CURRENCY_SYM);
    pword = fnFlashGetUnicodeStringParm(USP_SERVICE_MODE_ENTRY_PASSWORD);
    pword = fnFlashGetUnicodeStringParm(USP_SERVICE_MODE_EXIT_PASSWORD);

    pbchar = fnFlashGetPackedByteParm(PBP_AD_SLOGAN_LIST);
    pbchar = fnFlashGetPackedByteParm(PBP_FEATURE_LIST);
    pbchar = fnFlashGetPackedByteParm(PBP_INSC_LIST);

    wwordvar = fnFlashGetWordParm(WP_DISP_AM_FOLLOWER_CHAR);
    wwordvar = fnFlashGetWordParm(WP_FACTORY_SLEEP_TIMEOUT);
    wwordvar = fnFlashGetWordParm(WP_DISP_TIME_SEP_CHAR);

    longvar = fnFlashGetLongParm(LP_AD_SLOGAN_POSITION);
    longvar = fnFlashGetLongParm(LP_INITIAL_PERMIT1_COUNT);
    longvar = fnFlashGetLongParm(LP_TC_POSITION);

    pbchar = fnFlashGetMoneyParm(MP_FACTORY_POSTAGE);
    pbchar = fnFlashGetMoneyParm(MP_INITIAL_BATCH_VALUE);
    pbchar = fnFlashGetMoneyParm(MP_LOW_VALUE_INDICIA_THRESHOLD);

    pbchar = fnFlashGetAsciiStringParm(ASP_REPORTS_CURRENCY_SYM);
    pbchar = fnFlashGetAsciiStringParm(ASP_MODEM_INIT_STRING);
    pbchar = fnFlashGetAsciiStringParm(ASP_PSR_FILE_ID);
    
}
#endif

#ifdef JANUS_TESTFLASH

extern char fnInitImagGen(ushort ignore);
extern void fnInitSelCompList(void);
extern char fnReloadImagGen(ushort ignore);
extern char fnSetIGInscr(ushort ignore);
extern char fnSetIGAd(ushort ignore);
extern void fnAdd2SelCompList( UINT8 usCompType, UINT16 usCompID);
extern char fnLoadStaticVars( ushort fSendToIG );
extern char fnLoadDynamicVars(ushort ignore);

void    JanusTestFlash(void)
{
unsigned    short wwwordvar;

void        *indiciaIndex, *pJFontComponent;

unsigned long   *pFA,
                Size, CS,
                longvar,
                normIndex;
                
unsigned char   bcharvar,
                *pbchar,
                *pbconstants;

void            *pTmp,
                *pAddr,
                *pGraphicComponent;

char            dest[256];

unsigned short  i = 1,
                wwordvar,
                *pword,
                usHashAndSigSize;

BOOL            fResult;

    /*
     * i should be set to 1 to test the real flash.
     */

    if (i)
    {
        fnFlashInit();
        fnFlashDirectorySizeAndCS(&Size, &CS);
        fResult = fnFlashVerifyCheckSum(fnFlashGetAddress(COMPONENT_SIZE_DC), Size, CS);
    }

    /*
     * Validate Directory Offsets
     */


    pFA = fnFlashGetAddress(CHECKSUM_DC);
    pFA = fnFlashGetAddress(COMPONENT_SIZE_DC);
    pFA = fnFlashGetAddress(CONSTANTS_DC);
    pFA = fnFlashGetAddress(SUMS_INDEX_DC);
    pFA = fnFlashGetAddress(CONTEXT_PARMS_DC);
    pFA = fnFlashGetAddress(REPORTS_DC);
    pFA = fnFlashGetAddress(CONTEXT_MAP_DC);
    pFA = fnFlashGetAddress(RESERVED_1);
    pFA = fnFlashGetAddress(FONT_MAP_DC);
    pFA = fnFlashGetAddress(FONT_INDEX_DC);
    pFA = fnFlashGetAddress(SCREEN_TREE_DC);
    pFA = fnFlashGetAddress(LANG_DESC_DC);
    pFA = fnFlashGetAddress(SCREENS_DC);
    pFA = fnFlashGetAddress(RESERVED_2);
    pFA = fnFlashGetAddress(LINKED_IMAGES_DC);
    pFA = fnFlashGetAddress(REGION_MAP_DC);
    pFA = fnFlashGetAddress(RESERVED_3);
    pFA = fnFlashGetAddress(SUPP_BIN_FILES_DC);
    pFA = fnFlashGetAddress(RESERVED_4);
    pFA = fnFlashGetAddress(RESERVED_5);
    pFA = fnFlashGetAddress(RESERVED_6);
    pFA = fnFlashGetAddress(RESERVED_7);


    /*                                                              
     * Validate Constants                                           
     */                                                             
                                                                    
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_VERSION_ID);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_FLASH_DATE_TIME);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_GCS_FLASH_REV);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_PSR_FILE_ID);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_PCN);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_LANGUAGES_CNT);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_CONTEXTS_CNT);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_REPORT_TYPES_CNT);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_SUMS_CNT);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_REGION_MAPS_CNT);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_JFONTS_CNT);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_VERSION_STRING);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_NUM_OF_KEY_REFRESH);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_UNIQUE_ID_STRING);
    pbconstants =  GET_BYTE_CONSTANT(COFFSET_METER_FAMILY_STRING);

    /*
     * Graphic Component
     */
    
    //get address of second linked component
    pFA = fnFlashGetAddress(LINKED_IMAGES_DC);
    pbchar = (uchar*) pFA;
    pbchar += 12;
//  memcpy((uchar*)&longvar, (uchar *)(pFA), 4);
//  longvar += TOP_OF_FLASH;
//  pGraphicComponent = (void*) longvar;
    pGraphicComponent = (void*) pbchar;

    fResult = fnFlashGetGraphicMember(pGraphicComponent, COMPONENTSUM ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, COMPONENTSIZE ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, VISIBILITY ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, SCHEMA_VERSION ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, GRAPHIC_DATA_AREA_SIZE ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, GRAPHIC_DATA_AREA_ADDR ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, COMPONENTTYPE ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, NAMESCNT ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, UNIKODES ,&dest);  

    fResult = fnFlashGetGraphicMember(pGraphicComponent, SZOF_IBUTTON_HASHANDSIG ,&dest);
//  fResult = fnFlashGetGraphicMember(pGraphicComponent, IBUTTON_HASHANDSIG ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, CURRENCYTYPE ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, RESERVED ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, GRAPHIC_DATA_NAME_REV ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, WIDTH ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, HEIGHT ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, NUM_OF_REG_GROUPS ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, PAD ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, REGFIELDCNT ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, BITMAPSIZE ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, OFFSET_TO_REGFDS ,&dest);
    fResult = fnFlashGetGraphicMember(pGraphicComponent, OFFSET_TO_BITMAP ,&dest);

    fResult = fnFlashGetGraphicMemberAddr(pGraphicComponent, IBUTTON_HASHANDSIG ,(void**)&pAddr);
    fResult = fnFlashGetGraphicMemberAddr(pGraphicComponent, REGISTERS ,(void**)&pAddr);
    fResult = fnFlashGetGraphicMemberAddr(pGraphicComponent, REG_FIELD_DESC_RECS ,(void**)&pAddr);
    fResult = fnFlashGetGraphicMemberAddr(pGraphicComponent, BITMAP ,(void**)&pAddr);

    /*
     * Font Components
    */

    fResult = fnJFontGetFontCompAddr(&pJFontComponent, 0);
    wwordvar = fnJFontGetFontBitmapSize(pJFontComponent);
    pbchar = fnJFontGetFontBitmapAddr(pJFontComponent);
    //wwordvar = fnJFontGetCharHeight(void *pJFontComponent);
    //wwordvar = fnJFFontGetCharWidth(void *pJFontComponent);
    wwordvar = fnJFontGetNumChar(pJFontComponent);
    pbchar = fnJFontGetHashAndSigAddr(pJFontComponent, &usHashAndSigSize);

    /*
     * Validate Report Indicies
     */

    pTmp = fnFlashGetReport(REFILL_RECEIPT_RPT);
    pTmp = fnFlashGetReport(REFILL_INSTR_RPT);
    pTmp = fnFlashGetReport(REFILL_STMT_RPT);
    pTmp = fnFlashGetReport(MULTI_ACCT_DETAIL_RPT);
    pTmp = fnFlashGetReport(MULTI_ACCT_SUM_RPT);
    pTmp = fnFlashGetReport(SINGLE_ACCT_RPT);
    pTmp = fnFlashGetReport(SHORT_SVC_RPT);
    pTmp = fnFlashGetReport(LONG_SVC_RPT);
    pTmp = fnFlashGetReport(REG_RPT);
    pTmp = fnFlashGetReport(CONFIG_RPT);
    pTmp = fnFlashGetReport(ERR_RPT);
    pTmp = fnFlashGetReport(PERMIT_RPT);
    pTmp = fnFlashGetReport(SETUP_RPT);
    pTmp = fnFlashGetReport(SETUP_RPT+1);
    pTmp = fnFlashGetReport(REFILL_RECEIPT_RPT);

    /*
     * Validate Report Indicies
     */
    /*
    pTmp = fnFlashGetIndicia(IMG_ID_NORMAL_INDICIA);
    pTmp = fnFlashGetIndicia(IMG_ID_LOW_INDICIA);
    pTmp = fnFlashGetIndicia(IMG_ID_TOWN_CIRCLE);
    pTmp = fnFlashGetIndicia(IMG_ID_SWISS_STAR);
    pTmp = fnFlashGetIndicia(IMG_ID_ENTGELT);
    pTmp = fnFlashGetIndicia(IMG_ID_PERMIT1_PE);
    pTmp = fnFlashGetIndicia(IMG_ID_PERMIT2_PE);
    pTmp = fnFlashGetIndicia(IMG_ID_PERMIT3_PE);
    pTmp = fnFlashGetIndicia(IMG_ID_PERMIT3_PE+1);
    pTmp = fnFlashGetIndicia(IMG_ID_NORMAL_INDICIA);
    */

    bcharvar = fnFlashGetByteParm(BP_ACCESS_CODE_AVAIL);
    bcharvar = fnFlashGetByteParm(BP_MAX_INSC);
    bcharvar = fnFlashGetByteParm(BP_USE_LOW_VALUE_INDICIA_FOR_0);

    pword = fnFlashGetUnicodeStringParm(USP_DISP_CURRENCY_SYM);
    pword = fnFlashGetUnicodeStringParm(USP_SERVICE_MODE_ENTRY_PASSWORD);

    pbchar = fnFlashGetPackedByteParm(PBP_AD_SLOGAN_LIST);
    pbchar = fnFlashGetPackedByteParm(PBP_FEATURE_LIST);
    pbchar = fnFlashGetPackedByteParm(PBP_INSC_LIST);

    wwordvar = fnFlashGetWordParm(WP_DISP_AM_FOLLOWER_CHAR);
    wwordvar = fnFlashGetWordParm(WP_FACTORY_SLEEP_TIMEOUT);
    wwordvar = fnFlashGetWordParm(WP_DISP_TIME_SEP_CHAR);

    longvar = fnFlashGetLongParm(LP_AD_SLOGAN_POSITION);
    longvar = fnFlashGetLongParm(LP_INITIAL_PERMIT1_COUNT);
    longvar = fnFlashGetLongParm(LP_TC_POSITION);

    pbchar = fnFlashGetMoneyParm(MP_FACTORY_POSTAGE);
    pbchar = fnFlashGetMoneyParm(MP_INITIAL_BATCH_VALUE);
    pbchar = fnFlashGetMoneyParm(MP_LOW_VALUE_INDICIA_THRESHOLD);

    pbchar = fnFlashGetAsciiStringParm(ASP_REPORTS_CURRENCY_SYM);
    pbchar = fnFlashGetAsciiStringParm(ASP_MODEM_INIT_STRING);
    pbchar = fnFlashGetAsciiStringParm(ASP_PSR_FILE_ID);

    //I-Button

    bcharvar = fnFlashGetIBByteParm(IBP_REFILL_INCREMENT);
    bcharvar = fnFlashGetIBByteParm(IBP_SETTABLE_DIGITS);
    bcharvar = fnFlashGetIBByteParm(IBP_FIXED_ZEROS);

    wwordvar = fnFlashGetIBWordParm(IWP_SECDATA_INSPECT_WARN_PER);
    
    longvar = fnFlashGetIBLongParm(ILP_USERSET_LOW_POST_WARN);
    longvar = fnFlashGetIBLongParm(ILP_REFILL_FAIL_LIMIT);
    longvar = fnFlashGetIBLongParm(ILP_GMT_OFFSET);

    pbchar = fnFlashGetIBMPackedByteParm(IMPB_ROOT_KEY_REC);
    pbchar = fnFlashGetIBMPackedByteParm(IMPB_KEY_UPDATE_KEY_REC);
    pbchar = fnFlashGetIBMPackedByteParm(IMPB_VENDOR_KEY_REC);

    fnInitImagGen(0);
    fnInitSelCompList();
    fnReloadImagGen(0);
    fnSetIGInscr(0);
    fnSetIGAd(0);
    fnInitSelCompList();
    fnAdd2SelCompList(0, 0);
    fnLoadStaticVars(0);
    fnLoadDynamicVars(0);
}

#endif
