/*********************************************************************************************
**	PROJECT:		Mega Comet
*	MODULE NAME:	$Workfile:   CRC16x.C  $
*	REVISION:		$Revision:   1.1  $
*
*	DESCRIPTION:	Implementation file for CRC-16 Standard and
*					CRC-16C Generation library.
* ----------------------------------------------------------------------
*               Copyright (c) 2004 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*
*	REVISION HISTORY:
*	$Log:   H:/group/SPARK/ARCHIVES/UIC/CRC16x.C_v  $  
 * 
 *    Rev 1.1   Dec 08 2004 16:09:22   MA507HA
 * Make comments consistent with code.
 * 
 *    Rev 1.0   Dec 08 2004 12:07:26   MA507HA
 * Combined CRC16 Library Source - Standard
 * and CCITT compliant CRC16 generation.
************************************************************************************************/

#ifndef NULL
#define NULL	0
#endif
/***************************************************************************************
*       Table: CRC16C - Implementation
*
*
* Description: This is the CCITT table used to generate CRC16.
*              
***** ----------------------------------------------------------------------- *****
*****| This table is used to implement a CCITT CRC16 error detection scheme. |*****
*****| The process treats the bit stream as a single very large number and   |*****
*****| the result is the remainder you are left with when that number is     |*****
*****| divided by 0x11021.  It is this choice of numbers that makes it CCITT |*****
*****| compatiable.  The entries in this table are used by the macro to      |*****
*****| implement a highly optomised remainder function that looks at the     |*****
*****| bit stream a full byte at a time.                                     |*****
***** ----------------------------------------------------------------------- *****
*****************************************************************************************/
static const short CrcTable[256] = {
        0,   4129,   8258,  12387,  16516,  20645,  24774,  28903,
   -32504, -28375, -24246, -20117, -15988, -11859,  -7730,  -3601,
     4657,    528,  12915,   8786,  21173,  17044,  29431,  25302,
   -27847, -31976, -19589, -23718, -11331, -15460,  -3073,  -7202,
     9314,  13379,   1056,   5121,  25830,  29895,  17572,  21637,
   -23190, -19125, -31448, -27383,  -6674,  -2609, -14932, -10867,
    13907,   9842,   5649,   1584,  30423,  26358,  22165,  18100,
   -18597, -22662, -26855, -30920,  -2081,  -6146, -10339, -14404,
    18628,  22757,  26758,  30887,   2112,   6241,  10242,  14371,
   -13876,  -9747,  -5746,  -1617, -30392, -26263, -22262, -18133,
    23285,  19156,  31415,  27286,   6769,   2640,  14899,  10770,
    -9219, -13348,  -1089,  -5218, -25735, -29864, -17605, -21734,
    27814,  31879,  19684,  23749,  11298,  15363,   3168,   7233,
    -4690,   -625, -12820,  -8755, -21206, -17141, -29336, -25271,
    32407,  28342,  24277,  20212,  15891,  11826,   7761,   3696,
      -97,  -4162,  -8227, -12292, -16613, -20678, -24743, -28808,
   -28280, -32343, -20022, -24085, -12020, -16083,  -3762,  -7825,
     4224,    161,  12482,   8419,  20484,  16421,  28742,  24679,
   -31815, -27752, -23557, -19494, -15555, -11492,  -7297,  -3234,
      689,   4752,   8947,  13010,  16949,  21012,  25207,  29270,
   -18966, -23093, -27224, -31351,  -2706,  -6833, -10964, -15091,
    13538,   9411,   5280,   1153,  29798,  25671,  21540,  17413,
   -22565, -18438, -30823, -26696,  -6305,  -2178, -14563, -10436,
     9939,  14066,   1681,   5808,  26199,  30326,  17941,  22068,
    -9908, -13971,  -1778,  -5841, -26168, -30231, -18038, -22101,
    22596,  18533,  30726,  26663,   6336,   2273,  14466,  10403,
   -13443,  -9380,  -5313,  -1250, -29703, -25640, -21573, -17510,
    19061,  23124,  27191,  31254,   2801,   6864,  10931,  14994,
     -722,  -4849,  -8852, -12979, -16982, -21109, -25112, -29239,
    31782,  27655,  23652,  19525,  15522,  11395,   7392,   3265,
    -4321,   -194, -12451,  -8324, -20581, -16454, -28711, -24584,
    28183,  32310,  20053,  24180,  11923,  16050,   3793,   7920
};
/*********************************************************************************
*
*  Function: CRC16_ADD - Macro for adding a character to an existing CRC
*
*  Parameters:
*     ch  - character to be added to the CRC
*     crc - Existing CRC value (minimum 16 bits).  For new CRC, pass in zero.
*
*  Return Value: None
*
*  Design Notes:
*
*  Revision Notes:
*    Aquired 7/30/97
*
**********************************************************************************/
#define CRC16_ADD(ch, crc)                                                         \
   crc = (CrcTable[((crc>>8)^ch)&0xFF]^(crc<<8))
/*********************************************************************************
*
*     Function: crc16c - Will generate a CCITT crc16 for a buffer and data for buffer
*
*   Parameters:  crc - Existing CRC value (minimum 16 bits).  
*				 	   For new CRC, pass in zero.
*	  		    *buf - buffer of characters to CRC
*			     len - size of data in buffer
*			    
* Return Value: None
*
**********************************************************************************/
unsigned short crc16c(unsigned short crc, const unsigned char *buf, unsigned int len)
{
	unsigned int iBufferLen = len;

	while(iBufferLen--)
		CRC16_ADD((*buf++), crc);
	return crc;
}
/***************************************************************************************
*       Table: CRC16 - Implementation
*
*
* Description: This is the Standard(IEEE)  table used to generate CRC16.
*              
***** ----------------------------------------------------------------------- *****
*****| This table is used to implement a Standard(IEEE) CRC16 error detection|*****
*****| scheme.                                                               |*****
***** ----------------------------------------------------------------------- *****
*****************************************************************************************/
static const unsigned int CRCtbl[ 256 ] = {                                 
   0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,   
   0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,   
   0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,   
   0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,   
   0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,   
   0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,   
   0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,   
   0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,   
   0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,   
   0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,   
   0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,   
   0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,   
   0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,   
   0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,   
   0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,   
   0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,   
   0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,   
   0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,   
   0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,   
   0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,   
   0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,   
   0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,   
   0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,   
   0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,   
   0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,   
   0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,   
   0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,   
   0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,   
   0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,   
   0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,   
   0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,   
   0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040 }; 
                                                                     
/*********************************************************************************
*
*     Function: addCRC - Will generate a IEEE Standard crc16 for data in buffer
*
*   Parameters:  crc - Existing CRC value (minimum 16 bits).  
*				 	   For new CRC, pass in zero.
*	  		    *buf - buffer of characters to CRC
*			     len - size of data in buffer
*			    
* Return Value: None
*
**********************************************************************************/
static unsigned int addCRC( unsigned short CRC, unsigned char b )         
{                                                                 
   return ( CRC >> 8 ) ^ CRCtbl[ ( CRC & 0xFF ) ^ b ];               
}     
/*********************************************************************************
*
*     Function: _crc16 - Will generate a IEEE Standard crc16 for data in buffer
*
*   Parameters:  crc - Existing CRC value (minimum 16 bits).  
*				 	   For new CRC, pass in zero.
*	  		    *buf - buffer of characters to CRC
*			     len - size of data in buffer
*			    
* Return Value: None
*
**********************************************************************************/
unsigned int _crc16(unsigned int crc, const char *buf, unsigned long len)
{
	unsigned int iBufferLen = len;

	while(iBufferLen--)
		crc = addCRC(crc, (*buf++));
	return crc;
}
/*********************************************************************************
*
*     Function: crc16 - Will generate a IEEE Standard crc16 for data in buffer
*
*   Parameters:  *buf - buffer of characters to CRC
*			     len - size of data in buffer
*			    
* Return Value: None
*
**********************************************************************************/
#if 0 //the version in dummy.c is used
unsigned int crc16(char *buf, unsigned long len)
{
	unsigned int crc = 0;

	crc = _crc16(crc, buf, len);
	return crc;
}
#endif
