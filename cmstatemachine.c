/************************************************************************
*   PROJECT:        FuturePhoenix
*   COMPANY:        Pitney Bowes
*   AUTHOR :        
*   MODULE NAME:    $Workfile:   cmstatemachine.c  $
*       
*   DESCRIPTION:    Control Manager Task - Interfaces to Print Manager 
*                    Task
*
* ----------------------------------------------------------------------
*               Copyright (c) 2002 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*       REVISION HISTORY:

 05-Mar-13 sa002pe on FPHX 02.10 shelton branch
    Changed all the tables from static to const so they won't take up space in RAM.
*
* 2012.11.23 John Gao   FPHX 02.10 cienet branch
*  Fix fraca GMSE00219294, modify the state table so that the v31_PmRspInkErr can be
*  processed in Tape Run Pending state s67_TapeRunPnd
*
* 2010.07.16 Clarisa Bellamy     FPHX 02.02 branch
*  Fix for GMSE 00191990 - DCAP / AR Discrepancy:
*  - Add a small scratchpad buffer for assembling messages for the system log.
*  - Add a table of pointers to strings for writing text entries in the system
*    log with the description of the cmBobState value.
*  - Modify function fnSetBobState to write an entry to the system log everytime 
*    the cmBobState value is changed.
*
* 06/03/2009 Raymond Shen
*   Added "{s46_HdInkErr,fnHdInkErr}" for state s60_4MailRunPnd in table cmStateFuncTbl[]
*   to handle event v31_PmRspInkErr.
*
v13_PmCvrOpencmStateFuncTbl
*    Initial revision for FuturePhoenix 01/17/2006 H. Sun
* 
*************************************************************************/

#include "stdlib.h"
#include "stdio.h"
#include "commontypes.h"
#include "pbos.h"
#include "global.h"
#include "sys.h"
#include "clock.h"
#include "psoctask.h"
#include "cm.h"
#include "cmpm.h"
#include "cmpublic.h"
#include "cmerr.h"

tCmStateData     cmState;


/*
 * State Table generates the new state, 
 * given the coming event and the current state
 * The current state is in columns,
 * and the event is listed in rows
 */
const tCmEvtElmnt cmStateFuncTbl[EVENT_NUM][STATE_NUM] = {
    // row 0 and column 0 are invalid
  { INVALID,       INVALID,       INVALID,       INVALID,       INVALID,    
    INVALID,       INVALID,       INVALID,       INVALID,       INVALID,    
    INVALID,       INVALID,       INVALID,       INVALID,       INVALID,    
    INVALID,       INVALID,       INVALID,       INVALID,       INVALID,    
    INVALID,       INVALID,       INVALID,       INVALID,       INVALID,    
    INVALID,       INVALID,       INVALID,       INVALID,       INVALID,    
    INVALID,       INVALID,       INVALID,       INVALID,       INVALID,    
    INVALID,       INVALID,       INVALID,       INVALID,       INVALID,    
    INVALID,       INVALID,       INVALID,       INVALID,       INVALID,    
    INVALID,       INVALID,       INVALID,       INVALID,       INVALID,    
    INVALID,       INVALID,       INVALID,       INVALID,       INVALID,    
    INVALID,       INVALID,       INVALID,       INVALID,       INVALID,    
    INVALID,       INVALID,       INVALID,       INVALID,       INVALID,    
    INVALID,       INVALID,       INVALID,       INVALID,       INVALID },    
    
    // Auto Start Request, no purge, 
    // event v1_PmAtStrtReqNoMtnc = 1
    // hits s4_NmlIdle, s5_SlOnlyIdle, s20_PreCvr, s31_BobError, s36_PndgWrng
  { INVALID,       MISMATCH,      MISMATCH,      {s29_SysRspPnd,fnAutoReq_SysRspPnd},      
                                                                {s29_SysRspPnd,fnAutoReq_SysRspPnd},
    {s29_SysRspPnd,fnAutoReq_SysRspPnd},
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnAutoReq_Reset},      
                                                                MISMATCH,   
    {s30_NoAction,fnAutoReq_Reset},      
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      {s30_NoAction,fnAutoReq_Reset},      
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      {s29_SysRspPnd,fnAutoReq_SysRspPnd},
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Auto Start Request, before printing purge or power on purge 
    // event v2_PmAtStrtReqMtnc 
    // hits s4_NmlIdle, s20_PreCvr, s31_BobError, s36_PndgWrng
  { INVALID,       MISMATCH,      MISMATCH,      {s29_SysRspPnd,fnAutoReq_SysRspPnd},      
                                                                {s9_BfPrtMtnc,fnAutoReq_Mtnc},
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnAutoReq_Reset},      
                                                                MISMATCH,   
    {s30_NoAction,fnAutoReq_Reset},      
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      {s30_NoAction,fnAutoReq_Reset},
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      {s30_NoAction,fnAutoReq_Reset},      
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // v3_PmMotionSnsrStatus
    // hits s1_Uninit, s2_Init, s6_DiagIdle, s7_DisableIdle, and error cases
    // 
  { INVALID,       {s30_NoAction,fnMotionSnsrStts},      
                                  {s30_NoAction,fnMotionSnsrStts},      
                                                 {s30_NoAction,fnMotionSnsrStts},  
                                                                {s30_NoAction,fnMotionSnsrStts},
    CM_DONT_CARE,  CM_DONT_CARE,  {s30_NoAction,fnMotionSnsrStts},  
                                                 CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    {s30_NoAction,fnMotionSnsrStts},  
                   {s30_NoAction,fnMotionSnsrStts},  
                                  {s30_NoAction,fnMotionSnsrStts},  
                                                 {s30_NoAction,fnMotionSnsrStts},  
                                                                {s30_NoAction,fnMotionSnsrStts},
    {s30_NoAction,fnMotionSnsrStts},  
                   {s30_NoAction,fnMotionSnsrStts},  
                                  {s30_NoAction,fnMotionSnsrStts},  
                                                 {s30_NoAction,fnMotionSnsrStts},  
                                                                {s30_NoAction,fnMotionSnsrStts},
    {s30_NoAction,fnMotionSnsrStts},
                   {s30_NoAction,fnMotionSnsrStts},  
                                  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  {s30_NoAction,fnMotionSnsrStts},  
                                  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  {s30_NoAction,fnMotionSnsrStts},  
                                  CM_DONT_CARE,  CM_DONT_CARE,  {s30_NoAction,fnMotionSnsrStts},
    {s30_NoAction,fnMotionSnsrStts},
                   CM_DONT_CARE,  {s30_NoAction,fnMotionSnsrStts},  
                                                 {s30_NoAction,fnMotionSnsrStts},  
                                                                {s30_NoAction,fnMotionSnsrStts},
    {s30_NoAction,fnMotionSnsrStts}, 
                   {s30_NoAction,fnMotionSnsrStts},
                                  {s30_NoAction,fnMotionSnsrStts},
                                                 {s30_NoAction,fnMotionSnsrStts},
                                                                {s30_NoAction,fnMotionSnsrStts},
    {s30_NoAction,fnMotionSnsrStts},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    CM_DONT_CARE,  MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Run Response, 
    // event v4_PmRunRsp 
    // hits s4_NmlIdle, s5_SlOnlyIdle, s10_PrtModeStg, s19_SlOnlyRun, s31_BobError
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnRunRsp_SlIdle},
    {s30_NoAction,fnRunRsp_SlIdle},      
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s28_BobCommitRspPnd,fnCmDontCare},      
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s5_SlOnlyIdle,fnRunRsp_SlIdle},
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      {s31_BobError,fnEject},      
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    {s4_NmlIdle,fnRunRsp_SlIdle},      
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      {s30_NoAction,fnRunRsp_SlIdle},      
                                  MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s30_NoAction,fnFdrRun},      
                   MISMATCH,      MISMATCH,      MISMATCH,      {s4_NmlIdle,fnFdrRun},   
    MISMATCH,      MISMATCH,      {s30_NoAction,fnPmRunTape},      
                                                 MISMATCH,      MISMATCH },   

    // Print Response, 
    // event v5_PmPrtRsp
    // hits s13_Prt
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      {s4_NmlIdle,fnPrtRsp_NmlIdle},
                                                                MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      {s30_NoAction,fnHdInkErr},  
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Request Printer Maintenance, 
    // event v6_PmReqPrtMtnc
    // hits s4_NmlIdle, s5_SlIdle, s8_Sleep, 
    // s40_PrtJamErr, s42_EjtErr, s43_StgErr, s44_PrtErr, 
    // s45_SlRunErr, s46_HdInkErr, s47_MsgErr 
  { INVALID,       MISMATCH,      {s30_NoAction,fnInitRsp_Mtnc},      
                                                 MISMATCH,      {s3_Mtnc,fnMtncReq_Mtnc},
    {s3_Mtnc,fnMtncReq_Mtnc},  
                   MISMATCH,      {s30_NoAction,fnMtncReq_Mtnc},      
                                                 {s3_Mtnc,fnMtncReq_Mtnc},  
                                                                MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s16_CvrOpen,fnMtncReq_Mtnc},      
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s30_NoAction,fnMtncReq_Mtnc},      
                   MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnMtncReq_Mtnc},   
    MISMATCH,      MISMATCH,      {s2_Init,fnMtncReq_Mtnc},
                                                 MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnMtncReq_Mtnc},   
    MISMATCH,      {s30_NoAction,fnMtncReq_Mtnc},      
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    {s30_NoAction,fnMtncReq_Mtnc},
                   MISMATCH,      {s30_NoAction,fnMtncReq_Mtnc},      
                                                {s30_NoAction,fnMtncReq_Mtnc},      
                                                                {s30_NoAction,fnMtncReq_Mtnc},
    {s30_NoAction,fnMtncReq_Mtnc}, 
                   {s30_NoAction,fnMtncReq_Mtnc},      
                                  {s30_NoAction,fnMtncReq_Mtnc},      
                                                 MISMATCH,      MISMATCH,   

    {s30_NoAction,fnMtncReq_Mtnc},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s30_NoAction,fnMtncReq_Mtnc},      
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      {s30_NoAction,fnMtncReq_Mtnc},      
                                                 MISMATCH,      MISMATCH },   
    
    // Perform Printer Maintenance Response
    // event v7_PmMtncRsp 
    // hits s3_Mtnc, s9_BfPrtMtnc, s18_RplFnsh_5V_ON, s23_StCks
    // s40_PrtJamErr, s42_EjtErr, s43_StgErr, s44_PrtErr, 
    // s45_SlRunErr, s46_HdInkErr, s47_MsgErr 
  { INVALID,       MISMATCH,      {s22_Idle,fnPsocResync},
                                                 {s4_NmlIdle,fnSetModeRsp}, 
                                                                {s30_NoAction,fnMtncRsp_Idle},
    {s30_NoAction,fnCmDontCare},      
                   {s30_NoAction,fnCmDontCare},      
                                  {s30_NoAction,fnCmDontCare},      
                                                 {s30_NoAction,fnCmDontCare},      
                                                                {s29_SysRspPnd,fnMtncRsp_SysRspPnd},
    {s30_NoAction,fnCmDontCare},      
                   {s30_NoAction,fnCmDontCare},      
                                  {s30_NoAction,fnCmDontCare},      
                                                 {s30_NoAction,fnCmDontCare},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s22_Idle,fnMtncRsp_RplFin},
                                                                {s30_NoAction,NULL},
    {s30_NoAction,NULL}, 
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},          
                                                 {s22_Idle,fnMtncRsp_Oit},
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL}, 
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},          
                                                 {s30_NoAction,NULL},
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL}, 
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},          
                                                 {s30_NoAction,NULL},
                                                                {s4_NmlIdle,fnSetModeRsp},   
    {s30_NoAction,NULL}, 
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},          
                                                 {s30_NoAction,NULL},
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},
                   MISMATCH,      {s30_NoAction,NULL},      
                                                {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},
    {s30_NoAction,NULL}, 
                   {s30_NoAction,fnMtncRsp_RptIfIdle}, 
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},  

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s30_NoAction,fnStartMainTransport},  
                   CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,      
                                                                {s30_NoAction,fnRestoreMainRun},   
    CM_DONT_CARE,  CM_DONT_CARE,  {s30_NoAction,fnStartMainTransportForTapeRun},
                                                 CM_DONT_CARE,  CM_DONT_CARE },   
    
    // v8_PmNoPreRunMtnc
    // Hist s60_4MailRunPnd, s67_TapeRunPnd
  { INVALID,       CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    {s30_NoAction,fnStartMainTransport},
                   CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  {s30_NoAction,fnStartMainTransport},
                                                 CM_DONT_CARE,  CM_DONT_CARE },   
    
    // Initialize PM Response
    // event v9_PmInitRspNoMtnc
    // hits s2_Init, s4_NmlIdle, s5_SlOnlyIdle, s7_DisableIdle, 
    // s20_PreCvr, s24_JamOpen, s25_BothCvrOpen
  { INVALID,       MISMATCH,      {s30_NoAction,fnPsocResync},
                                                 MISMATCH,      {s30_NoAction,fnPsocResync},   
    {s30_NoAction,fnPsocResync},      
                   MISMATCH,      {s30_NoAction,fnPsocResync},      
                                                 MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s30_NoAction,fnPsocResync},      
                   MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnPsocResync},   
    {s30_NoAction,fnPsocResync},      
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Perform PM Diagnostic Response
    // event v10_PmDiagRsp 10
    // hits s6_DiagIdle 6
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      {s6_DiagIdle,fnDiagRsp_DiagIdle},
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Perform Replacement Response
    // event v11_PmRplRsp 11
    // hits s16_CvrOpen
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      {s15_PmRpl,fnRplRsp_PmRpl},
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Request Printer Sensor Data Response
    // v12_PmSnsrDataRsp 12
    // hits all states
  { INVALID,       {s30_NoAction,fnSnsrDataRsp},
                                  {s30_NoAction,fnSnsrDataRsp},      
                                                 {s30_NoAction,fnSnsrDataRsp},      
                                                                {s30_NoAction,fnSnsrDataRsp},   
    {s30_NoAction,fnSnsrDataRsp},      
                   {s30_NoAction,fnSnsrDataRsp},      
                                  {s30_NoAction,fnSnsrDataRsp},      
                                                 {s30_NoAction,fnSnsrDataRsp},      
                                                                {s30_NoAction,fnSnsrDataRsp},   
    {s30_NoAction,fnSnsrDataRsp},      
                   {s30_NoAction,fnSnsrDataRsp},      
                                  {s30_NoAction,fnSnsrDataRsp},      
                                                 {s30_NoAction,fnSnsrDataRsp},      
                                                                {s30_NoAction,fnSnsrDataRsp},   
    {s30_NoAction,fnSnsrDataRsp},      
                   {s30_NoAction,fnSnsrDataRsp},      
                                  {s30_NoAction,fnSnsrDataRsp},      
                                                 {s30_NoAction,fnSnsrDataRsp},      
                                                                {s30_NoAction,fnSnsrDataRsp},   
    {s30_NoAction,fnSnsrDataRsp},      
                   {s30_NoAction,fnSnsrDataRsp},      
                                  {s30_NoAction,fnSnsrDataRsp},      
                                                 {s30_NoAction,fnSnsrDataRsp},      
                                                                {s30_NoAction,fnSnsrDataRsp},   
    {s30_NoAction,fnSnsrDataRsp},      
                   {s30_NoAction,fnSnsrDataRsp},      
                                  {s30_NoAction,fnSnsrDataRsp},      
                                                 {s30_NoAction,fnSnsrDataRsp},      
                                                                {s30_NoAction,fnSnsrDataRsp},   
    {s30_NoAction,fnSnsrDataRsp},      
                   {s30_NoAction,fnSnsrDataRsp},      
                                  {s30_NoAction,fnSnsrDataRsp},      
                                                 {s30_NoAction,fnSnsrDataRsp},      
                                                                {s30_NoAction,fnSnsrDataRsp},   
    {s30_NoAction,fnSnsrDataRsp},      
                   {s30_NoAction,fnSnsrDataRsp},      
                                  {s30_NoAction,fnSnsrDataRsp},      
                                                 {s30_NoAction,fnSnsrDataRsp},      
                                                                {s30_NoAction,fnSnsrDataRsp},   
    {s30_NoAction,fnSnsrDataRsp},      
                   {s30_NoAction,fnSnsrDataRsp},      
                                  {s30_NoAction,fnSnsrDataRsp},      
                                                 {s30_NoAction,fnSnsrDataRsp},      
                                                                {s30_NoAction,fnSnsrDataRsp},   
    {s30_NoAction,fnSnsrDataRsp},      
                   {s30_NoAction,fnSnsrDataRsp},      
                                  {s30_NoAction,fnSnsrDataRsp},      
                                                 {s30_NoAction,fnSnsrDataRsp},      
                                                                {s30_NoAction,fnSnsrDataRsp},

    {s30_NoAction,fnSnsrDataRsp},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Cover Open
    // v13_PmCvrOpen 
    // hits s4_NmlIdle 4, s5_SlOnlyIdle 5, s15_PmRpl 15 for normal operations
    // hits other states too, since there is no way to prevent the user from
    // opening the top cover, no matter what the meter is doing
    // For s10_PrtModeStg, s12_Ejt, s13_Prt, s19_SlOnlyRun,s29_SysRspPnd 
    // the CM will hold them (NULL), instruct the PM to finish any pending jobs then
    // move to s20_PreCvr.
  { INVALID,       {s20_PreCvr,fnCvrOpen_PreCvr},  
                                  {s30_NoAction,fnInitRsp_CvrOpen},  
                                                 {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                                {s20_PreCvr,fnCvrOpen_PreCvr},
    {s20_PreCvr,fnCvrOpen_PreCvr},  
                   {s20_PreCvr,fnCvrOpen_PreCvr},  
                                  {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                                {s20_PreCvr,fnCvrOpen_PreCvr},
                                                                
    {s30_NoAction,NULL},
                   {s30_NoAction,NULL},
                                  {s30_NoAction,NULL}, 
                                                {s30_NoAction,NULL}, 
                                                                {s16_CvrOpen,fnCvrOpen_PreCvr},
                                                                
    CM_DONT_CARE,
                   CM_DONT_CARE,
                                  CM_DONT_CARE,
                                                CM_DONT_CARE,
                                                                CM_DONT_CARE,
    {s20_PreCvr,fnCvrOpen_PreCvr},  
                   {s20_PreCvr,fnCvrOpen_PreCvr},  
                                  {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                CM_DONT_CARE,
                                                                {s25_BothCvrOpen,fnBothCvrOpen},
    {s30_NoAction,NULL},
                   {s20_PreCvr,fnCvrOpen_PreCvr},  
                                  {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                                {s30_NoAction,NULL},
    {s20_PreCvr,fnCvrOpen_PreCvr},  
                   {s20_PreCvr,fnCvrOpen_PreCvr},  
                                  {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                                {s30_NoAction,fnCvrOpen_PreCvr},
                                                                
    {s20_PreCvr,fnCvrOpen_PreCvr},  
                   {s20_PreCvr,fnCvrOpen_PreCvr},  
                                  {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                                {s20_PreCvr,fnCvrOpen_PreCvr},
                                                                
    {s20_PreCvr,fnCvrOpen_PreCvr},  
                   {s20_PreCvr,fnCvrOpen_PreCvr},  
                                  {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                                {s20_PreCvr,fnCvrOpen_PreCvr},
                                                                
    {s20_PreCvr,fnCvrOpen_PreCvr},  
                   {s20_PreCvr,fnCvrOpen_PreCvr},  
                                  {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                                {s20_PreCvr,fnCvrOpen_PreCvr},
                                                                
    {s20_PreCvr,fnCvrOpen_PreCvr},  
                   {s20_PreCvr,fnCvrOpen_PreCvr},  
                                  {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                                {s20_PreCvr,fnCvrOpen_PreCvr},   

    {s20_PreCvr,fnCvrOpen_PreCvr},  
                   {s20_PreCvr,fnCvrOpen_PreCvr},  
                                  {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                                {s20_PreCvr,fnCvrOpen_PreCvr},   

    {s20_PreCvr,fnCvrOpen_PreCvr},  
                   {s20_PreCvr,fnCvrOpen_PreCvr},  
                                  {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                                {s20_PreCvr,fnCvrOpen_PreCvr}, 

    {s20_PreCvr,fnCvrOpen_PreCvr},  
                   {s20_PreCvr,fnCvrOpen_PreCvr},  
                                  {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                {s20_PreCvr,fnCvrOpen_PreCvr},  
                                                                {s20_PreCvr,fnCvrOpen_PreCvr},
},   
    
    // Cover Close
    // v14_PmCvrClose
    // hits s4_NmlIdle, s5_SlOnlyIdle, s7_DisableIdle, s15_PmRpl, s16_CvrOpen, 
    // s20_PreCvr, s25_BothCvrOpen
    // For s10_PrtModeStg, s12_Ejt, s13_Prt, s19_SlOnlyRun,s29_SysRspPnd 
    // the CM will hold them (NULL), instruct the PM to finish any pending jobs then
    // move to s20_PreCvr.
  { INVALID,       CM_DONT_CARE,  {s30_NoAction,fnPsocResync}, // fnCvrCls_Idle,  
                                                 CM_DONT_CARE, {s30_NoAction,fnPsocResync}, // fnCvrCls_Idle,
    {s30_NoAction,fnPsocResync}, // fnCvrCls_Idle,
                   {s30_NoAction,fnPsocResync}, // fnCvrCls_Idle, 
                                  {s30_NoAction,fnPsocResync}, // fnCvrCls_Idle,  
                                                 CM_DONT_CARE, CM_DONT_CARE,
    CM_DONT_CARE,
                   CM_DONT_CARE,
                                  CM_DONT_CARE,
                                                 CM_DONT_CARE,
                                                                CM_DONT_CARE,
    {s17_PmRplFnsh,fnCvrCls_PmRplFnsh},  
                   CM_DONT_CARE, //{s22_Idle,fnCvrCls_Idle}, going to idle will damage replacement processing  
                                  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    {s22_Idle, fnPsocResync}, // fnCvrCls_Idle,
                   CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  {s30_NoAction,fnPsocResync}, // {s24_JamOpen,fnCvrCls_JamOpen},   
    {s30_NoAction,fnPsocResync}, // {s24_JamOpen,fnCvrCls_JamOpen}, 
                   CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE },
    
    // Jam Open
    // v15_PmJamOpen 15
    // hits any state, since there is no way to prevent the user from
    // opening Jam lever, no matter what the meter is doing
    // For other states, the CM will instruct the PM to finish any pending jobs then
    // moves to s23_StCks
  { INVALID,       {s24_JamOpen,fnJamOpen}, 
                                  {s30_NoAction,fnInitRsp_JamOpen}, 
                                                 {s24_JamOpen,fnJamOpen}, 
                                                                {s24_JamOpen,fnJamOpen_Cap}, 
                                                                
    {s24_JamOpen,fnJamOpen_Cap}, 
                   {s24_JamOpen,fnJamOpen_Cap}, 
                                  {s24_JamOpen,fnJamOpen_Cap}, 
                                                 {s24_JamOpen,fnJamOpen}, 
                                                                {s24_JamOpen,fnJamOpen}, 
                                                                
    {s24_JamOpen,fnJamOpen}, 
                   {s24_JamOpen,fnJamOpen}, 
                                  {s24_JamOpen,fnJamOpen}, 
                                                 {s24_JamOpen,fnJamOpen}, 
                                                                {s24_JamOpen,fnJamOpen}, 
                                                                
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  {s24_JamOpen,fnJamOpen}, 
                                                                
    {s24_JamOpen,fnJamOpen}, 
                   {s24_JamOpen,fnJamOpen}, 
                                  {s24_JamOpen,fnJamOpen}, 
                                                 CM_DONT_CARE,
                                                                {s30_NoAction,NULL}, 
                                                                
    {s24_JamOpen,fnJamOpen}, 
                   {s24_JamOpen,fnJamOpen}, 
                                  {s24_JamOpen,fnJamOpen}, 
                                                 {s24_JamOpen,fnJamOpen}, 
                                                                {s24_JamOpen,fnJamOpen}, 
                                                                
    {s24_JamOpen,fnJamOpen}, 
                   {s24_JamOpen,fnJamOpen}, 
                                  {s24_JamOpen,fnJamOpen}, 
                                                 {s24_JamOpen,fnJamOpen}, 
                                                                {s30_NoAction,fnJamOpen}, 
                                                                
    {s24_JamOpen,fnJamOpen}, 
                   {s24_JamOpen,fnJamOpen}, 
                                  {s24_JamOpen,fnJamOpen}, 
                                                 {s24_JamOpen,fnJamOpen}, 
                                                                {s24_JamOpen,fnJamOpen}, 
                                                                
    {s24_JamOpen,fnJamOpen}, 
                   {s24_JamOpen,fnJamOpen}, 
                                  {s24_JamOpen,fnJamOpen}, 
                                                 {s24_JamOpen,fnJamOpen}, 
                                                                {s24_JamOpen,fnJamOpen}, 
                                                                
    {s24_JamOpen,fnJamOpen}, 
                   {s24_JamOpen,fnJamOpen}, 
                                  {s24_JamOpen,fnJamOpen}, 
                                                 {s24_JamOpen,fnJamOpen}, 
                                                                {s24_JamOpen,fnJamOpen}, 
                                                                
    {s24_JamOpen,fnJamOpen},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
                                                                
    // Jam Close
    // v16_PmJamClose
    // hits s24_JamOpen, s25_BothCvrOpen
  { INVALID,       MISMATCH,      {s30_NoAction,NULL}, 
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 CM_DONT_CARE,
                                                                {s22_Idle,fnJamCls},      
    {s20_PreCvr,fnJamCls_crvOpen},
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},
                                                                
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Clear Printer Error Response, this event should not include Jam error clearance response,
    //       which needs PM maintenance. The Jam error clearance response comes in the same message,
    //       but is convertd to event v25_PmClrJamErrRsp.
    // v17_PmClrPrtErrRsp 17 
    // s47_MsgErr and s49_FtlErr are not supported by this event.
    // hits following error states: 
    // s42_EjtErr 42, s43_StgErr 43, s44_PrtErr 44, s45_SlRunErr 45, 
    // s46_HdInkErr 46
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      {s4_NmlIdle,fnClrPmErrRsp_Nml}, 
                                                 {s4_NmlIdle,fnClrPmErrRsp_Nml}, 
                                                                {s4_NmlIdle,fnClrPmErrRsp_Nml}, 
    {s5_SlOnlyIdle,fnClrPmErrRsp_Sl},
                   {s22_Idle,fnClrPmHdInkErrRsp}, 
                                  MISMATCH,      MISMATCH,   
                                                                MISMATCH,   
                                                                
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Printer Shutdown Response
    // v18_PmStdwRsp 18 
    // hits s21_SftPwrOffPnd 21
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnShutDownRsp},   
    {s30_NoAction,fnShutDownRsp},      
                   {s30_NoAction,fnShutDownRsp},      
                                  {s30_NoAction,fnShutDownRsp}, 
                                                 {s30_NoAction,fnShutDownRsp},      
                                                                MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      {s30_NoAction,fnShutDownRsp},      
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    {s30_NoAction,fnShutDownRsp},      
                   {s27_SftPwrOff,fnShutDownRsp}, 
                                  MISMATCH,      MISMATCH,      {s30_NoAction,fnShutDownRsp},   
    {s30_NoAction,fnShutDownRsp},      
                   {s8_Sleep,fnShutDownRspAnd27VOff},     
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      CM_DONT_CARE,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      CM_DONT_CARE,
                                                                
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Get Print Head Data Response
    // v19_PmPrtHdDataRsp 19
    // hits all states
    // hist s3_Mtnc, s4_NmlIdle, s5_SlOnlyIdle for debugging purpose
  { INVALID,       {s30_NoAction,fnHdDataRsp}, 
                                  {s30_NoAction,fnHdDataRsp},      
                                                 {s30_NoAction,fnHdDataRsp}, 
                                                                {s30_NoAction,fnHdDataRsp},
    {s30_NoAction,fnHdDataRsp},
                   {s30_NoAction,fnHdDataRsp},      
                                  {s30_NoAction,fnHdDataRsp},      
                                                 {s30_NoAction,fnHdDataRsp}, 
                                                                {s30_NoAction,fnHdDataRsp},

    {s30_NoAction,fnHdDataRsp},
                   {s30_NoAction,fnHdDataRsp},      
                                  {s30_NoAction,fnHdDataRsp},      
                                                 {s30_NoAction,fnHdDataRsp}, 
                                                                {s30_NoAction,fnHdDataRsp},
    {s30_NoAction,fnHdDataRsp},
                   {s30_NoAction,fnHdDataRsp},      
                                  {s30_NoAction,fnHdDataRsp},      
                                                 {s30_NoAction,fnHdDataRsp}, 
                                                                {s30_NoAction,fnHdDataRsp},
    {s30_NoAction,fnHdDataRsp},
                   {s30_NoAction,fnHdDataRsp},      
                                  {s30_NoAction,fnHdDataRsp},      
                                                 {s30_NoAction,fnHdDataRsp}, 
                                                                {s30_NoAction,fnHdDataRsp},
    {s30_NoAction,fnHdDataRsp},
                   {s30_NoAction,fnHdDataRsp},      
                                  {s30_NoAction,fnHdDataRsp},      
                                                 {s30_NoAction,fnHdDataRsp}, 
                                                                {s30_NoAction,fnHdDataRsp},
    {s30_NoAction,fnHdDataRsp},
                   {s30_NoAction,fnHdDataRsp},      
                                  {s30_NoAction,fnHdDataRsp},      
                                                 {s30_NoAction,fnHdDataRsp}, 
                                                                {s30_NoAction,fnHdDataRsp},
    {s30_NoAction,fnHdDataRsp},
                   {s30_NoAction,fnHdDataRsp},      
                                  {s30_NoAction,fnHdDataRsp},      
                                                 {s30_NoAction,fnHdDataRsp}, 
                                                                {s30_NoAction,fnHdDataRsp},
    {s30_NoAction,fnHdDataRsp},
                   {s30_NoAction,fnHdDataRsp},      
                                  {s30_NoAction,fnHdDataRsp},      
                                                 {s30_NoAction,fnHdDataRsp}, 
                                                                {s30_NoAction,fnHdDataRsp},
    {s30_NoAction,fnHdDataRsp},
                   {s30_NoAction,fnHdDataRsp},      
                                  {s30_NoAction,fnHdDataRsp},      
                                                 {s30_NoAction,fnHdDataRsp}, 
                                                                {s30_NoAction,fnHdDataRsp},
                                                                
    {s30_NoAction,fnHdDataRsp},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Get Ink Tank Data Response
    // v20_PmInkTkRsp = 20
    // hits all states
    // hist s3_Mtnc, s4_NmlIdle, s5_SlOnlyIdle for debugging purpose
  { INVALID,       {s30_NoAction,fnInkTankDataRsp},      
                                  {s30_NoAction,fnInkTankDataRsp},      
                                                 {s30_NoAction,fnInkTankDataRsp},
                                                                {s30_NoAction,fnInkTankDataRsp},
    {s30_NoAction,fnInkTankDataRsp},
                   {s30_NoAction,fnInkTankDataRsp},      
                                  {s30_NoAction,fnInkTankDataRsp},      
                                                 {s30_NoAction,fnInkTankDataRsp},      
                                                                {s30_NoAction,fnInkTankDataRsp},
    {s30_NoAction,fnInkTankDataRsp},
                   {s30_NoAction,fnInkTankDataRsp},      
                                  {s30_NoAction,fnInkTankDataRsp},      
                                                 {s30_NoAction,fnInkTankDataRsp},      
                                                                {s30_NoAction,fnInkTankDataRsp},
    {s30_NoAction,fnInkTankDataRsp},
                   {s30_NoAction,fnInkTankDataRsp},      
                                  {s30_NoAction,fnInkTankDataRsp},      
                                                 {s30_NoAction,fnInkTankDataRsp},      
                                                                {s30_NoAction,fnInkTankDataRsp},
    {s30_NoAction,fnInkTankDataRsp},
                   {s30_NoAction,fnInkTankDataRsp},      
                                  {s30_NoAction,fnInkTankDataRsp},      
                                                 {s30_NoAction,fnInkTankDataRsp},      
                                                                {s30_NoAction,fnInkTankDataRsp},
    {s30_NoAction,fnInkTankDataRsp},
                   {s30_NoAction,fnInkTankDataRsp},      
                                  {s30_NoAction,fnInkTankDataRsp},      
                                                 {s30_NoAction,fnInkTankDataRsp},      
                                                                {s30_NoAction,fnInkTankDataRsp},
    {s30_NoAction,fnInkTankDataRsp},
                   {s30_NoAction,fnInkTankDataRsp},      
                                  {s30_NoAction,fnInkTankDataRsp},      
                                                 {s30_NoAction,fnInkTankDataRsp},      
                                                                {s30_NoAction,fnInkTankDataRsp},
    {s30_NoAction,fnInkTankDataRsp},
                   {s30_NoAction,fnInkTankDataRsp},      
                                  {s30_NoAction,fnInkTankDataRsp},      
                                                 {s30_NoAction,fnInkTankDataRsp},      
                                                                {s30_NoAction,fnInkTankDataRsp},
    {s30_NoAction,fnInkTankDataRsp},
                   {s30_NoAction,fnInkTankDataRsp},      
                                  {s30_NoAction,fnInkTankDataRsp},      
                                                 {s30_NoAction,fnInkTankDataRsp},      
                                                                {s30_NoAction,fnInkTankDataRsp},
    {s30_NoAction,fnInkTankDataRsp},
                   {s30_NoAction,fnInkTankDataRsp},      
                                  {s30_NoAction,fnInkTankDataRsp},      
                                                 {s30_NoAction,fnInkTankDataRsp},      
                                                                {s30_NoAction,fnInkTankDataRsp},
                                                                
    {s30_NoAction,fnInkTankDataRsp},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   

    
    // Get SW version Response
    // v21_PmSwVerRsp 
    // hits all states 
    // hist s3_Mtnc, s4_NmlIdle, s5_SlOnlyIdle for debugging purpose
  { INVALID,       {s30_NoAction,fnSwVerRsp},      
                                  {s30_NoAction,fnSwVerRsp},      
                                                 {s30_NoAction,fnSwVerRsp},
                                                                {s30_NoAction,fnSwVerRsp},
    {s30_NoAction,fnSwVerRsp},
                   {s30_NoAction,fnSwVerRsp},      
                                  {s30_NoAction,fnSwVerRsp},      
                                                 {s30_NoAction,fnSwVerRsp},
                                                                {s30_NoAction,fnSwVerRsp},
    {s30_NoAction,fnSwVerRsp},
                   {s30_NoAction,fnSwVerRsp},      
                                  {s30_NoAction,fnSwVerRsp},      
                                                 {s30_NoAction,fnSwVerRsp},
                                                                {s30_NoAction,fnSwVerRsp},
    {s30_NoAction,fnSwVerRsp},
                   {s30_NoAction,fnSwVerRsp},      
                                  {s30_NoAction,fnSwVerRsp},      
                                                 {s30_NoAction,fnSwVerRsp},
                                                                {s30_NoAction,fnSwVerRsp},
    {s30_NoAction,fnSwVerRsp},
                   {s30_NoAction,fnSwVerRsp},      
                                  {s30_NoAction,fnSwVerRsp},      
                                                 {s30_NoAction,fnSwVerRsp},
                                                                {s30_NoAction,fnSwVerRsp},
    {s30_NoAction,fnSwVerRsp},
                   {s30_NoAction,fnSwVerRsp},      
                                  {s30_NoAction,fnSwVerRsp},      
                                                 {s30_NoAction,fnSwVerRsp},
                                                                {s30_NoAction,fnSwVerRsp},
    {s30_NoAction,fnSwVerRsp},
                   {s30_NoAction,fnSwVerRsp},      
                                  {s30_NoAction,fnSwVerRsp},      
                                                 {s30_NoAction,fnSwVerRsp},
                                                                {s30_NoAction,fnSwVerRsp},
    {s30_NoAction,fnSwVerRsp},
                   {s30_NoAction,fnSwVerRsp},      
                                  {s30_NoAction,fnSwVerRsp},      
                                                 {s30_NoAction,fnSwVerRsp},
                                                                {s30_NoAction,fnSwVerRsp},
    {s30_NoAction,fnSwVerRsp},
                   {s30_NoAction,fnSwVerRsp},      
                                  {s30_NoAction,fnSwVerRsp},      
                                                 {s30_NoAction,fnSwVerRsp},
                                                                {s30_NoAction,fnSwVerRsp},
    {s30_NoAction,fnSwVerRsp},
                   {s30_NoAction,fnSwVerRsp},      
                                  {s30_NoAction,fnSwVerRsp},      
                                                 {s30_NoAction,fnSwVerRsp},
                                                                {s30_NoAction,fnSwVerRsp},
                                                                
    {s30_NoAction,fnSwVerRsp},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Set Mode Response
    // v22_PmSetModeRsp
    // hits s4_NmlIdle, s5_SlOnlyIdle, s6_DiagIdle, s7_DisableIdle, s36_PndgWrng
  { INVALID,       {s30_NoAction,fnSetModeRsp},      
                                  {s30_NoAction,fnSetModeRsp},      
                                                 {s30_NoAction,fnSetModeRsp}, 
                                                                {s4_NmlIdle,fnSetModeRsp},
    {s5_SlOnlyIdle,fnSetModeRsp},   
                   {s6_DiagIdle,fnSetModeRsp},
                                  {s7_DisableIdle,fnSetModeRsp}, //fnSetModeRsp_CvrStatus,               
                                                 {s30_NoAction,fnSetModeRsp},      
                                                                {s30_NoAction,fnSetModeRsp},   
    {s30_NoAction,fnSetModeRsp},      
                   {s30_NoAction,fnSetModeRsp},      
                                  {s30_NoAction,fnSetModeRsp},      
                                                 {s30_NoAction,fnSetModeRsp},      
                                                                {s30_NoAction,fnSetModeRsp},   
    {s30_NoAction,fnSetModeRsp},      
                   {s30_NoAction,fnSetModeRsp},      
                                  {s30_NoAction,fnSetModeRsp},      
                                                 {s30_NoAction,fnSetModeRsp},      
                                                                {s30_NoAction,fnSetModeRsp},   
    {s30_NoAction,fnSetModeRsp},      
                   {s30_NoAction,fnSetModeRsp},      
                                  {s30_NoAction,fnSetModeRsp},      
                                                 {s30_NoAction,fnSetModeRsp},      
                                                                {s30_NoAction,fnSetModeRsp},   
    {s30_NoAction,fnSetModeRsp},      
                   {s30_NoAction,fnSetModeRsp},      
                                  {s30_NoAction,fnSetModeRsp},      
                                                 {s30_NoAction,fnSetModeRsp},      
                                                                {s30_NoAction,fnSetModeRsp},   
    {s30_NoAction,fnSetModeRsp},      
                   {s30_NoAction,fnSetModeRsp},      
                                  {s30_NoAction,fnSetModeRsp},      
                                                 {s30_NoAction,fnSetModeRsp},      
                                                                {s4_NmlIdle,fnSetModeRsp},   
    {s30_NoAction,fnSetModeRsp},      
                   {s7_DisableIdle,fnSetModeRsp},      
                                  {s30_NoAction,fnSetModeRsp},      
                                                 {s30_NoAction,fnSetModeRsp},      
                                                                {s30_NoAction,fnSetModeRsp},   
    {s30_NoAction,fnSetModeRsp},      
                   {s30_NoAction,fnSetModeRsp},      
                                  {s30_NoAction,fnSetModeRsp},      
                                                 {s30_NoAction,fnSetModeRsp},      
                                                                {s30_NoAction,fnSetModeRsp},   
    {s30_NoAction,fnSetModeRsp},      
                   {s30_NoAction,fnSetModeRsp},      
                                  {s30_NoAction,fnSetModeRsp},      
                                                 {s30_NoAction,fnSetModeRsp},      
                                                                {s30_NoAction,fnSetModeRsp},
                                                                
    {s30_NoAction,fnSetModeRsp},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Eject Response
    // v23_PmEjtRsp = 23,
    // hits s12_Ejt 
  { INVALID,       MISMATCH,      MISMATCH,      {s30_NoAction,fnEjtRsp_Idle},      
                                                                {s4_NmlIdle,fnEjtRsp_Idle},
    MISMATCH,      MISMATCH,      {s30_NoAction,fnEjtRsp_Idle},      
                                                 MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      {s4_NmlIdle,fnEjtRsp_Idle},
                                                 MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      {s30_NoAction,fnHdInkErr},      
                                  MISMATCH,      MISMATCH,      MISMATCH,
                                                                
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // PM warning : Piece skipped
    // v24_PmWrng = 24
    // hits any state
  { INVALID,       {s30_NoAction,fnPmWrng},
                                  {s30_NoAction,fnPmWrng}, 
                                                 {s30_NoAction,fnPmWrng}, 
                                                                {s36_PndgWrng,fnPmWrng},
    {s36_PndgWrng,fnPmWrng}, 
                   {s30_NoAction,fnPmWrng}, 
                                  {s30_NoAction,fnPmWrng}, 
                                                 {s30_NoAction,fnPmWrng}, 
                                                                {s30_NoAction,fnPmWrng}, 
                                                                
    {s30_NoAction,fnPmWrng}, 
                   {s30_NoAction,fnPmWrng}, 
                                  {s30_NoAction,fnPmWrng}, 
                                                 {s30_NoAction,fnPmWrng}, 
                                                                {s30_NoAction,fnPmWrng}, 
                                                                
    {s30_NoAction,fnPmWrng}, 
                   {s30_NoAction,fnPmWrng}, 
                                  {s30_NoAction,fnPmWrng}, 
                                                 {s30_NoAction,fnPmWrng}, 
                                                                {s30_NoAction,fnPmWrng}, 
                                                                
    {s30_NoAction,fnPmWrng}, 
                   {s30_NoAction,fnPmWrng}, 
                                  {s30_NoAction,fnPmWrng}, 
                                                 {s30_NoAction,fnPmWrng}, 
                                                                {s30_NoAction,fnPmWrng}, 
                                                                
    {s30_NoAction,fnPmWrng}, 
                   {s30_NoAction,fnPmWrng}, 
                                  {s30_NoAction,fnPmWrng}, 
                                                 {s30_NoAction,fnPmWrng}, 
                                                                {s30_NoAction,fnPmWrng}, 
                                                                
    {s30_NoAction,fnPmWrng}, 
                   {s30_NoAction,fnPmWrng}, 
                                  {s30_NoAction,fnPmWrng}, 
                                                 {s30_NoAction,fnPmWrng}, 
                                                                {s30_NoAction,fnPmWrng}, 
                                                                
    {s30_NoAction,fnPmWrng}, 
                   {s30_NoAction,fnPmWrng}, 
                                  {s30_NoAction,fnPmWrng}, 
                                                 {s30_NoAction,fnPmWrng}, 
                                                                {s30_NoAction,fnPmWrng}, 
                                                                
    {s30_NoAction,fnPmWrng}, 
                   {s30_NoAction,fnPmWrng}, 
                                  {s30_NoAction,fnPmWrng}, 
                                                 {s30_NoAction,fnPmWrng}, 
                                                                {s30_NoAction,fnPmWrng}, 
                                                                
    {s30_NoAction,fnPmWrng}, 
                   {s30_NoAction,fnPmWrng}, 
                                  {s30_NoAction,fnPmWrng}, 
                                                 {s30_NoAction,fnPmWrng}, 
                                                                {s30_NoAction,fnPmWrng},
                                                                
    {s30_NoAction,fnPmWrng},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
                                                                
    
    // Clear Printer Jam Error Response
    // v25_PmClrJamErrRsp,
    // hits s40_PrtJamErr, s42_EjtErr, s43_StgErr, s44_PrtErr, s45_SlRunErr
  { INVALID,       {s30_NoAction,NULL}, 
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s3_Mtnc,fnClrPmJamErrRsp},
                   {s30_NoAction,NULL},      
                                  {s3_Mtnc,fnClrPmJamErrRsp},
                                                 {s3_Mtnc,fnClrPmJamErrRsp},
                                                                {s3_Mtnc,fnClrPmJamErrRsp},
    {s3_Mtnc,fnClrPmJamErrRsp},
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},
                                                                
    {s3_Mtnc,fnClrPmJamErrRsp},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Clear Printer Paper Error Response
    // v26_PmClrPprErrRsp6,
    // hits s40_PrtJamErr, s42_EjtErr, s43_StgErr, s44_PrtErr, s45_SlRunErr
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnClrPmErrRsp_Nml},   
    {s30_NoAction,fnClrPmErrRsp_Sl},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},   
    {s30_NoAction,NULL},      
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s22_Idle,fnClrNoPaperRsp},   
    {s4_NmlIdle,fnClrPmPprErrRsp_Nml},
                   {s30_NoAction,NULL},      
                                  {s4_NmlIdle,fnClrPmPprErrRsp_Nml},
                                                 {s4_NmlIdle,fnClrPmPprErrRsp_Nml},
                                                                {s4_NmlIdle,fnClrPmPprErrRsp_Nml},
    {s5_SlOnlyIdle,fnClrPmPprErrRsp_Sl},
                   {s30_NoAction,NULL},      
                                  {s30_NoAction,NULL},      
                                                 {s30_NoAction,NULL},      
                                                                {s30_NoAction,NULL},
                                                                
    {s4_NmlIdle,fnClrPmPprErrRsp_Nml},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Warning events occured in the middle of printing/maintenance
    // v27_PmKeepWrng
    // Hits s4_NmlIdle, s5_SlOnlyIdle, s6_DiagIdle, s7_DisableIdle, s18_RplFnsh_5V_ON
    // s13_Prt
  { INVALID,       CM_DONT_CARE,  {s30_NoAction,fnRptKeepWarnToOit},  
                                                 {s30_NoAction,fnRptKeepWarnToOit},  
                                                                {s30_NoAction,fnRptKeepWarnToOit},   
    {s30_NoAction,fnRptKeepWarnToOit},  
                   {s30_NoAction,fnRptKeepWarnToOit},  
                                  {s30_NoAction,fnRptKeepWarnToOit},  
                                                 CM_DONT_CARE,  {s22_Idle,fnRptKeepWarnToOit_Idle},
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  {s36_PndgWrng,fnPrtRsp_NmlIdle},
                                                                CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  {s18_RplFnsh_5V_ON,fnRptKeepWarnToOit},
                                                                CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE },  
    
    // Replacement complete response from PM, 5V head voltage is ON
    // v28_PmRplCmpltRsp
    // hits s17_PmRplFnsh
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      {s18_RplFnsh_5V_ON,fnPsocResync},    
                                                 MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
                                                                
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
  // Bob pending warning, an event activated after PM's print response
  // v29_BobPndWrng
  // Hits s13_Prt, s46_HdInkErr
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      {s36_PndgWrng,fnPrtRsp_NmlIdle},
                                                                MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      {s30_NoAction,fnHdInkErr},
                                  MISMATCH,      MISMATCH,      MISMATCH,
 
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Ink/tank errors reported in messages other than Mtnc Rsp & Run Rsp.
    // v30_InkErr, hits all states. But it is ignored during printing and 
    // reported after the last response (Print Rsp or Run Rsp for seal only)
  { INVALID,       {s46_HdInkErr,fnHdInkErr},
                                  {s30_NoAction,fnInitRsp_HeadErr},// Continue init
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                CM_DONT_CARE,
    CM_DONT_CARE,
                   CM_DONT_CARE,
                                  {s46_HdInkErr,fnCmDontCare},  
                                                 {s46_HdInkErr,NULL},  
                                                                {s46_HdInkErr,fnHdInkErr},
    {s46_HdInkErr,fnHdInkErr},
                   {s30_NoAction,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                CM_DONT_CARE,
                                                                
    {s30_NoAction,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
 
    {s46_HdInkErr,fnHdInkErr},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
                                                                
    // Ink / tank error that is reported in Maintenance response or Run response
    // v31_PmRspInkErr,  
    // hits any state 
  { INVALID,       {s46_HdInkErr,fnHdInkErr},
                                  {s30_NoAction,fnInitRsp_InkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr}, // {fnAutoReq_RunEject},
    {s46_HdInkErr,fnEject},
                   {s46_HdInkErr,fnEject},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s30_NoAction,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr_RplCmplt},
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s30_NoAction,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr_Oit},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr_SysModeRsp},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},
                                                 {s46_HdInkErr,fnHdInkErr},
                                                                {s46_HdInkErr,fnHdInkErr},
 
    {s46_HdInkErr,fnHdInkErr},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s46_HdInkErr,fnHdInkErr},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      {s46_HdInkErr,fnHdInkErr},      MISMATCH,      MISMATCH },   
                                                                
    // v32_TapeRunRsp
    // Hits 67_TapeRunPnd
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      {s4_NmlIdle,fnCmDontCare},      
                                                 MISMATCH,      MISMATCH },   
    
    // v33_StopTapeRunRsp, hits s4_NmlIdle and others
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,

    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE },
    
    // v34_StopRunRsp
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      CM_DONT_CARE,  MISMATCH,      MISMATCH,      CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   

    CM_DONT_CARE,  MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnMtncReq_Mtnc},   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // v35_PmAtStrtPauseMtnc
  { INVALID,       MISMATCH,      MISMATCH,      {s64_MtncPause,fnAutoReq_PauseMtnc},      
																					 {s64_MtncPause,fnAutoReq_PauseMtnc},   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // v36_PmAtStrtPauseCooling, 36
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      {s64_MtncPause,fnAutoReq_PauseMtnc},   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      CM_DONT_CARE,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // PSOC: 
    // v37_PsocCommitErr
    // Hist s18_RplFnsh_5V_ON, s32_PsocReseedRspPnd
  { INVALID,       {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocCommitRsp_Rpl},
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s35_RunEjt,fnAutoReq_RunEject},  
                                                 {s35_RunEjt,fnAutoReq_RunEject},
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},
 
    {s30_NoAction,fnPsocErrRsp},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   

    // PSOC:
    // v38_PsocReseedRsp
    // Hits s2_Init, s4_NmlIdle, s5_SlOnlyIdle, s7_DisableIdle, 
    // s20_PreCvr, s24_JamOpen, s25_BothCvrOpen, s18_RplFnsh_5V_ON, s29_SysRspPnd, s32_PsocReseedRspPnd
  { INVALID,       CM_DONT_CARE,  {s30_NoAction,fnPsocCommitSeed}, 
                                                 CM_DONT_CARE,  {s30_NoAction,fnPsocCommitSeed},   
    {s30_NoAction,fnPsocCommitSeed},  
                   {s30_NoAction,fnPsocCommitSeed},  
                                  {s30_NoAction,fnPsocCommitSeed},  
                                                 CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  {s30_NoAction,fnPsocCommitSeed},  
                                                                CM_DONT_CARE,   
    {s30_NoAction,fnPsocCommitSeed},  
                   CM_DONT_CARE,  {s30_NoAction,fnPsocCommitSeed},   // CM_DONT_CARE
                                                 CM_DONT_CARE,  {s30_NoAction,fnPsocCommitSeed},   
    {s30_NoAction,fnPsocCommitSeed},  
                   CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  {s30_NoAction,fnPsocCommitSeed},   
    CM_DONT_CARE,  CM_DONT_CARE,  {s30_NoAction,fnPsocCommitSeed}, // s33_PsocCommitRspPnd,fnPsocCommitSeed,
                                                 MISMATCH,      CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  {s30_NoAction,fnPsocCommitSeed},  
                                  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,

    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE },   
    
    // PSOC:
    // v39_PsocCommitRsp
    // Hits s2_Init, s4_NmlIdle, s5_SlOnlyIdle, s7_DisableIdle, 
    // s20_PreCvr, s24_JamOpen, s25_BothCvrOpen, s18_RplFnsh_5V_ON, 
    // s29_SysRspPnd, s32_PsocReseedRspPnd, s33_PsocCommitRspPnd
  { INVALID,       CM_DONT_CARE,  {s22_Idle,fnPsocCommitRsp_Init},  
                                                 CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  {s30_NoAction,fnPsocCommitRsp_Init},  
                                                 CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  {s30_NoAction,fnPsocCommitRsp_Rpl},  
                                                                CM_DONT_CARE,   
    {s30_NoAction,fnPsocCommitRsp_Init}, 
                   CM_DONT_CARE,  {s30_NoAction,fnCvrCls_Idle},
                                                 CM_DONT_CARE,  {s30_NoAction,fnPsocCommitRsp_Init},
    {s24_JamOpen,fnPsocCommitRsp_Init},   
                   CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  MISMATCH,      CM_DONT_CARE, 
                                                                CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  {s30_NoAction,fnPsocCommitRsp_Init},  
                                  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   

    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,   
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE },   
    
    // SYS: stage response, waiting for Bob's first response
    // v40_BobRspPnd
    // hits s29_SysRspPnd, s31_BobError
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH, 
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s28_BobCommitRspPnd,fnCmDontCare},
    MISMATCH,      {s31_BobError,fnEject},      
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Bob: Precreate or Commit error
    // v41_BobDebitCommitErr 
    // hits s28_BobCommitRspPnd, and other states
  { INVALID,       MISMATCH,      MISMATCH,      {s31_BobError,NULL},      
                                                                {s31_BobError,NULL},   
    {s31_BobError,NULL},      
                   {s31_BobError,NULL},      
                                  {s31_BobError,NULL},      
                                                 {s31_BobError,NULL},      
                                                                {s31_BobError,NULL},   
    {s31_BobError,NULL},      
                   {s31_BobError,NULL},      
                                  {s31_BobError,NULL},      
                                                 {s31_BobError,NULL},      
                                                                {s31_BobError,NULL},   
    {s31_BobError,NULL},      
                   {s31_BobError,NULL},      
                                  {s31_BobError,NULL},      
                                                 {s31_BobError,NULL},      
                                                                {s31_BobError,NULL},   
    {s31_BobError,NULL},      
                   {s31_BobError,NULL},      
                                  {s31_BobError,NULL},      
                                                 {s31_BobError,fnEject},
                                                                {s31_BobError,NULL},
    {s31_BobError,NULL},      
                   {s31_BobError,NULL},      
                                  {s31_BobError,NULL},      
                                                 {s31_BobError,fnEject},      
                                                                {s31_BobError,NULL},   
    {s31_BobError,NULL},      
                   {s31_BobError,NULL},      
                                  {s31_BobError,NULL},      
                                                 {s31_BobError,NULL},      
                                                                {s31_BobError,NULL},   
    {s31_BobError,NULL},      
                   {s31_BobError,NULL},      
                                  {s31_BobError,NULL},      
                                                 {s31_BobError,NULL},      
                                                                {s31_BobError,NULL},   
    {s31_BobError,NULL},      
                   {s31_BobError,NULL},      
                                  {s31_BobError,NULL},      
                                                 {s31_BobError,NULL},      
                                                                {s31_BobError,NULL},   
    {s31_BobError,NULL},      
                   {s31_BobError,NULL},      
                                  {s31_BobError,NULL},      
                                                 {s31_BobError,NULL},      
                                                                {s31_BobError,NULL},   
    {s31_BobError,NULL},      
                   {s31_BobError,NULL},      
                                  {s31_BobError,NULL},      
                                                 {s31_BobError,NULL},      
                                                                {s31_BobError,NULL},   
    {s31_BobError,NULL},      
                   {s31_BobError,NULL},      
                                  {s31_BobError,NULL},      
                                                 {s31_BobError,NULL},      
                                                                {s31_BobError,NULL},   
    {s31_BobError,NULL},      
                   {s31_BobError,NULL},      
                                  {s31_BobError,NULL},      
                                                 {s31_BobError,NULL},      
                                                                {s31_BobError,NULL},   
    {s31_BobError,NULL},      
                   {s31_BobError,NULL},      
                                  {s31_BobError,NULL},      
                                                 {s31_BobError,NULL},      
                                                                {s31_BobError,NULL} },
    // SYS: RUN response error
    // v42_SysRunRspErr
    // hits s29_SysRspPnd 
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnHandleSysRejectRun},// DSD - changed this for S1 Tease fix - {s35_RunEjt,fnAutoReq_RunEject},
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // SYS: STAGE response error
    // v43_SysSagRspErr
    // hits s29_SysRspPnd, s31_BobError 
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s4_NmlIdle,fnEject},
    MISMATCH,      {s31_BobError,fnEject},      
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // SYS: Print response error
    // v44_SysPrtRspErr
    // hits s29_SysRspPnd 
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s4_NmlIdle,fnEject}, 
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // DSD 02/26/2007 - Fix for ME 0112 caused by a jam condition combined with the jam lever being opened.

    // Enter Normal Mode
    // v45_SetNmlMode = 45,
    // hits s3_Mtnc, s4_NmlIdle, s5_SlOnlyIdle, s6_DiagIdle, s7_PassTruIdle
/*0*/         { INVALID,       MISMATCH,      MISMATCH,      {s30_NoAction,fnSetNmlMode},      
                                                                        {s34_SetModePnd,fnSetNmlMode},
/*5*/           {s34_SetModePnd,fnSetNmlMode},
                           {s34_SetModePnd,fnSetNmlMode},
                                          {s34_SetModePnd,fnSetNmlMode},
                                                         MISMATCH,      {s30_NoAction,fnSetModeRsp},   
/*10*/          {s30_NoAction,fnSetModeRsp},      
                           {s30_NoAction,fnSetModeRsp},      
                                          MISMATCH,      MISMATCH,      MISMATCH,   
/*15*/          MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
/*20*/          MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      
                                                                            {s34_SetModePnd,fnSetNmlMode},   // DSD 02/26/2007 - Fix for ME 0112 caused by a jam condition combined with the jam lever being opened.
/*25*/          MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnSetModeRsp},   
/*30*/          MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
/*35*/          MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
/*40*/          MISMATCH, //{s30_NoAction,fnSetModeRsp},
                           MISMATCH,      MISMATCH,      MISMATCH, //{s30_NoAction,fnSetModeRsp},
                                                                        MISMATCH,   
/*45*/          MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

/*50*/          MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
/*55*/          MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
/*60*/          MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
/*65*/          MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    

    // Enter Seal Only Mode
    // v46_SetSlMode = 46,
    // hits s4_NmlIdle 4, s5_SlOnlyIdle 5, s6_DiagIdle 6, s7_PassTruIdle 7
    // s8_Sleep 8
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      {s5_SlOnlyIdle,fnSetSlOnlyMode},
    {s30_NoAction,fnSetSlOnlyMode},
                   {s5_SlOnlyIdle,fnSetSlOnlyMode},   
                                  {s5_SlOnlyIdle,fnSetSlOnlyMode},   
                                                 MISMATCH, // {s5_SlOnlyIdle,fnWakeUpSetSlOnly}, 
                                                                MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      {s5_SlOnlyIdle,fnSetSlOnlyMode},      
                                  MISMATCH,      MISMATCH,      MISMATCH,
 
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Enter Diagnostic Mode
    // v47_SetDiagMode = 47,
    // hits s4_NmlIdle 4, s5_SlOnlyIdle 5, s6_DiagIdle 6, s7_PassTruIdle 7
    // s8_Sleep 8
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      {s6_DiagIdle,fnSetDiagMode},
    {s6_DiagIdle,fnSetDiagMode},  
                   {s30_NoAction,fnSetModeRsp},
                                  {s6_DiagIdle,fnSetDiagMode}, 
                                                 MISMATCH, // {s6_DiagIdle,fnWakeUpSetDiagMode}, 
                                                                MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH, // {s18_RplFnsh_5V_ON,fnOitRplCmplt},      
                                                 MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
 
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    // SYS: Disable mode
    // v48_SetDisableMode
    // hits s3_Mtnc, s4_NmlIdle, s5_SlOnlyIdle, s6_DiagIdle, s7_DisableIdle, s50_TapeErr
  { INVALID,       {s30_NoAction,fnSetDisableMode},      
                                  {s30_NoAction,fnSetDisableMode},      
                                                 {s7_DisableIdle,fnSetDisableMode},      
                                                                {s7_DisableIdle,fnSetDisableMode},   
    {s7_DisableIdle,fnSetDisableMode},      
                   {s7_DisableIdle,fnSetDisableMode},      
                                  {s30_NoAction,fnSetDisableMode},//{s30_NoAction,fnSetModeRsp_Dup},      
                                                 MISMATCH,      {s7_DisableIdle,fnSetDisableMode},   
    {s7_DisableIdle,fnSetDisableMode},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s30_NoAction,fnSetDisableMode},      
                  {s30_NoAction,fnSetDisableMode},      
                                  {s30_NoAction,fnSetDisableMode},      
                                                 {s30_NoAction,fnSetDisableMode},      
                                                                {s30_NoAction,fnSetDisableMode},   
    {s30_NoAction,fnSetDisableMode},      
                   {s30_NoAction,fnSetDisableMode},      
                                  {s30_NoAction,fnSetDisableMode},      
                                                 {s30_NoAction,fnSetDisableMode},      
                                                                {s30_NoAction,fnSetDisableMode},   
    {s30_NoAction,fnSetDisableMode},      
                   {s30_NoAction,fnSetDisableMode},      
                                  {s30_NoAction,fnSetDisableMode},      
                                                 {s30_NoAction,fnSetDisableMode},      
                                                                {s7_DisableIdle,fnForcedPrintDisable},
    {s30_NoAction,fnSetDisableMode},      
                   {s30_NoAction,fnSetDisableMode},      
                                  {s30_NoAction,fnSetDisableMode},      
                                                 {s30_NoAction,fnSetDisableMode},      
                                                                {s7_DisableIdle,fnSetDisableMode},   
    {s30_NoAction,fnSetDisableMode},      
                   {s30_NoAction,fnSetDisableMode},      
                                  {s30_NoAction,fnSetDisableMode},      
                                                 {s30_NoAction,fnSetDisableMode},      
                                                                {s7_DisableIdle,fnSetDisableMode}, 
    {s30_NoAction,fnSetDisableMode},      
                   {s30_NoAction,fnSetDisableMode},      
                                  {s30_NoAction,fnSetDisableMode},      
                                                 {s30_NoAction,fnSetDisableMode},      
                                                                {s30_NoAction,fnSetDisableMode},   
    {s30_NoAction,fnSetDisableMode},
                   {s7_DisableIdle,fnSetDisableMode},
                                  {s30_NoAction,fnSetDisableMode},
                                                 {s30_NoAction,fnSetDisableMode},
                                                                {s30_NoAction,fnSetDisableMode},
    {s30_NoAction,fnSetDisableMode},
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },


    // Replacement Start
    // v49_RplStrt = 49,
    // hits s20_PreCvr, s46_HdInkErr 
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      {s16_CvrOpen,fnOitRplStart_cvrOpen}, 
    {s16_CvrOpen,fnOitRplStart_cvrOpen},      
                   MISMATCH,      {s16_CvrOpen,fnOitRplStart_cvrOpen},      
                                                 MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s16_CvrOpen,fnOitRplStart_cvrOpen},
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      {s16_CvrOpen,fnOitRplStart_cvrOpen},      
                                  MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    // Soft Power Off
    // v50_SftPwrOff = 50,
    // hits s4_NmlIdle, s5_SlOnlyIdle, 
  { INVALID,       MISMATCH,      {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                 {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                                {s21_SftPwrOffPnd,fnSysPwrOff},
    {s21_SftPwrOffPnd,fnSysPwrOff},
                   {s21_SftPwrOffPnd,fnSysPwrOff},      
                                  {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                 {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                                {s21_SftPwrOffPnd,fnSysPwrOff},   
    {s21_SftPwrOffPnd,fnSysPwrOff},
                   {s21_SftPwrOffPnd,fnSysPwrOff},      
                                  {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                 {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                                {s21_SftPwrOffPnd,fnSysPwrOff},   
                                                                
    {s21_SftPwrOffPnd,fnSysPwrOff},
                   {s21_SftPwrOffPnd,fnSysPwrOff},      
                                  {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                 {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                                {s21_SftPwrOffPnd,fnSysPwrOff},   
    {s21_SftPwrOffPnd,fnSysPwrOff},
                   {s21_SftPwrOffPnd,fnSysPwrOff},      
                                  {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                 {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                                {s21_SftPwrOffPnd,fnSysPwrOff},   
    {s21_SftPwrOffPnd,fnSysPwrOff},
                   {s21_SftPwrOffPnd,fnSysPwrOff},      
                                  {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                 {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                                {s21_SftPwrOffPnd,fnSysPwrOff},   
    {s21_SftPwrOffPnd,fnSysPwrOff},
                   {s21_SftPwrOffPnd,fnSysPwrOff},      
                                  {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                 {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                                {s21_SftPwrOffPnd,fnSysPwrOff},   
    {s21_SftPwrOffPnd,fnSysPwrOff},
                   {s21_SftPwrOffPnd,fnSysPwrOff},      
                                  {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                 {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                                {s21_SftPwrOffPnd,fnSysPwrOff},   
    {s21_SftPwrOffPnd,fnSysPwrOff},
                   {s21_SftPwrOffPnd,fnSysPwrOff},      
                                  {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                 {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                                {s21_SftPwrOffPnd,fnSysPwrOff},   
    {s21_SftPwrOffPnd,fnSysPwrOff},
                   {s21_SftPwrOffPnd,fnSysPwrOff},      
                                  {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                 {s21_SftPwrOffPnd,fnSysPwrOff},      
                                                                {s21_SftPwrOffPnd,fnSysPwrOff},

    {s21_SftPwrOffPnd,fnSysPwrOff},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    // Enter Sleep
    // v51_GoSleep = 51,
    // hits s4_NmlIdle 4, s5_SlOnlyIdle 5, 
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      {s26_SlpPnd,fnSysGoSleep},
    {s26_SlpPnd,fnSysGoSleep},
                   MISMATCH,      {s26_SlpPnd,fnSysGoSleep},      
                                                 MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s26_SlpPnd,fnSysGoSleep},  // DSD 05-16-2007 Fix Fraca 119275   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Initialize Request
    // v52_InitReq = 52,
    // hits s1_Uninit , s8_Sleep, s27_SftPwrOff 
  { INVALID,       {s2_Init,fnSysInitReq},
                                  {s2_Init,fnSysInitReq},
                                                 {s2_Init,fnSysInitReq},
                                                                {s2_Init,fnSysInitReq},
    {s2_Init,fnSysInitReq},
                   {s2_Init,fnSysInitReq},
                                  {s2_Init,fnSysInitReq},
                                                 {s2_Init,fnSysInitReq27VOn},
                                                                {s2_Init,fnSysInitReq},
    {s2_Init,fnSysInitReq},      
                   {s2_Init,fnSysInitReq},      
                                  {s2_Init,fnSysInitReq},      
                                                 {s2_Init,fnSysInitReq},      
                                                                {s2_Init,fnSysInitReq},   
    {s2_Init,fnSysInitReq},      
                   {s2_Init,fnSysInitReq},      
                                  {s2_Init,fnSysInitReq},      
                                                 {s2_Init,fnSysInitReq},      
                                                                {s2_Init,fnSysInitReq},   
    {s2_Init,fnSysInitReq},      
                   {s2_Init,fnSysInitReq},      
                                  {s2_Init,fnSysInitReq},      
                                                 {s2_Init,fnSysInitReq},      
                                                                {s2_Init,fnSysInitReq},   
    {s2_Init,fnSysInitReq},      
                   {s2_Init,fnSysInitReq},      
                                  {s2_Init,fnSysInitReq},      
                                                 {s2_Init,fnSysInitReq},      
                                                                {s2_Init,fnSysInitReq},   
    {s2_Init,fnSysInitReq},      
                   {s2_Init,fnSysInitReq},      
                                  {s2_Init,fnSysInitReq},      
                                                 {s2_Init,fnSysInitReq},      
                                                                {s2_Init,fnSysInitReq},   
    {s2_Init,fnSysInitReq},      
                   {s2_Init,fnSysInitReq},      
                                  {s2_Init,fnSysInitReq},      
                                                 {s2_Init,fnSysInitReq},      
                                                                {s2_Init,fnSysInitReq},   
    {s2_Init,fnSysInitReq},      
                   {s2_Init,fnSysInitReq},      
                                  {s2_Init,fnSysInitReq},      
                                                 {s2_Init,fnSysInitReq},      
                                                                {s2_Init,fnSysInitReq},
    {s2_Init,fnSysInitReq},      
                   {s2_Init,fnSysInitReq},      
                                  {s2_Init,fnSysInitReq},      
                                                 {s2_Init,fnSysInitReq},      
                                                                {s2_Init,fnSysInitReq},

    {s2_Init,fnSysInitReq},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

    
    // Clear Error Request (from SYS/OIT)
    // No state transition for this event. State transition only occurs
    // when Clear Error Response is received from the PM
    // v53_ClrErrReq = 53,
    // s47_MsgErr and s49_FtlErr are not supported by this event.
    // hits following error states: s4_NmlIdle, s5_SlOnlyIdle
    // s40_PrtJamErr, s42_EjtErr, s43_StgErr, s44_PrtErr, 
    // s45_SlRunErr, s46_HdInkErr
  { INVALID,       MISMATCH,      MISMATCH,      {s30_NoAction,fnOitClrErrReq},      
                                                                {s30_NoAction, fnOitClrErrReq},   
    {s30_NoAction, fnOitClrErrReq},      
                   {s30_NoAction, fnOitClrErrReq},      
                                  {s30_NoAction, fnOitClrErrReq},      
                                                 {s30_NoAction,fnOitClrErrReq},      
                                                                {s30_NoAction,fnOitClrErrReq},
    {s30_NoAction, fnOitClrErrReq},      
                   {s30_NoAction, fnOitClrErrReq},      
                                  {s30_NoAction, fnOitClrErrReq},      
                                                 {s30_NoAction,fnOitClrErrReq},      
                                                                {s30_NoAction,fnOitClrErrReq},
    {s30_NoAction, fnOitClrErrReq},      
                   {s30_NoAction, fnOitClrErrReq},      
                                  {s30_NoAction, fnOitClrErrReq},      
                                                 {s30_NoAction,fnOitClrErrReq},      
                                                                {s30_NoAction,fnOitClrErrReq},
    {s30_NoAction, fnOitClrErrReq},      
                   {s30_NoAction, fnOitClrErrReq},      
                                  {s22_Idle,fnOitClrErrReq},      
                                                 {s22_Idle,fnOitClrErrReq},
                                                                {s30_NoAction, fnOitClrErrReq},   
    {s30_NoAction, fnOitClrErrReq},      
                   {s30_NoAction, fnOitClrErrReq},      
                                  {s30_NoAction, fnOitClrErrReq},      
                                                 {s30_NoAction, fnOitClrErrReq},      
                                                                {s30_NoAction, fnOitClrErrReq},   
    {s30_NoAction, fnOitClrErrReq},      
                   {s30_NoAction, fnOitClrErrReq},      
                                  {s30_NoAction, fnOitClrErrReq},      
                                                 {s30_NoAction, fnOitClrErrReq},      
                                                                {s30_NoAction, fnOitClrErrReq},   
    {s30_NoAction, fnOitClrErrReq},      
                   {s30_NoAction, fnOitClrErrReq},      
                                  {s30_NoAction, fnOitClrErrReq},      
                                                 {s30_NoAction, fnOitClrErrReq},      
                                                                {s30_NoAction, fnOitClrErrReq},   
    {s30_NoAction,fnOitClrErrReq},      
                   {s30_NoAction,fnOitClrErrReq},      
                                  {s30_NoAction,fnOitClrErrReq},
                                                 {s30_NoAction,fnOitClrErrReq},  
                                                                {s30_NoAction,fnOitClrErrReq},
    {s30_NoAction,fnOitClrErrReq},   
                   {s30_NoAction,fnOitClrErrReq},   
                                  {s30_NoAction,fnOitClrErrReq},      
                                                 {s30_NoAction,fnOitClrErrReq},      
                                                                {s30_NoAction,fnOitClrErrReq},

    {s30_NoAction,fnOitClrErrReq},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    // Replacement Complete from OI
    // v54_RplCmplt = 54
    // hits s17_PmRplFnsh 
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s17_PmRplFnsh,fnOitRplCmplt},      
                   MISMATCH,      {s30_NoAction,fnOitRplCmplt},  
                                                 MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // 
    // Reserved 55
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Get sensor data, print head, or ink tank data
    // v56_GetData 
    // hits s4_NmlIdle, s5_SlOnlyIdle, s6_DiagIdle
  { INVALID,       {s30_NoAction,fnGetData},      
                                  {s30_NoAction,fnGetData},      
                                                 {s30_NoAction,fnGetData},      
                                                                {s30_NoAction,fnGetData},    
    {s30_NoAction,fnGetData},      
                   {s30_NoAction,fnGetData},      
                                  {s30_NoAction,fnGetData},      
                                                 {s30_NoAction,fnGetData},      
                                                                {s30_NoAction,fnGetData},   
    {s30_NoAction,fnGetData},      
                   {s30_NoAction,fnGetData},      
                                  {s30_NoAction,fnGetData},      
                                                 {s30_NoAction,fnGetData},      
                                                                {s30_NoAction,fnGetData},   
    {s30_NoAction,fnGetData},      
                   {s30_NoAction,fnGetData},      
                                  {s30_NoAction,fnGetData},      
                                                 {s30_NoAction,fnGetData},      
                                                                {s30_NoAction,fnGetData},   
    {s30_NoAction,fnGetData},      
                   {s30_NoAction,fnGetData},      
                                  {s30_NoAction,fnGetData},      
                                                 {s30_NoAction,fnGetData},      
                                                                {s30_NoAction,fnGetData},   
    {s30_NoAction,fnGetData},      
                   {s30_NoAction,fnGetData},      
                                  {s30_NoAction,fnGetData},      
                                                 {s30_NoAction,fnGetData},      
                                                                {s30_NoAction,fnGetData},   
    {s30_NoAction,fnGetData},      
                   {s30_NoAction,fnGetData},      
                                  {s30_NoAction,fnGetData},      
                                                 {s30_NoAction,fnGetData},      
                                                                {s30_NoAction,fnGetData},   
    {s30_NoAction,fnGetData},      
                   {s30_NoAction,fnGetData},      
                                  {s30_NoAction,fnGetData},      
                                                 {s30_NoAction,fnGetData},      
                                                                {s30_NoAction,fnGetData},   

    {s30_NoAction,fnGetData},      
                   {s30_NoAction,fnGetData},      
                                  {s30_NoAction,fnGetData},      
                                                 {s30_NoAction,fnGetData},      
                                                                {s30_NoAction,fnGetData},   

    {s30_NoAction,fnGetData},      
                   {s30_NoAction,fnGetData},      
                                  {s30_NoAction,fnGetData},      
                                                 {s30_NoAction,fnGetData},      
                                                                {s30_NoAction,fnGetData},   

    {s30_NoAction,fnGetData},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

    // Perform diagnostic actions
    // v57_DiagActn 
    // hits s6_DiagIdle
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      {s30_NoAction,fnOitDiagActn},      
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // OIT: Perform maintenance 
    // v58_OitReqMtnc
    // hits s4_NmlIdle, s5_SlOnlyIdle
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,     {s23_StCks,fnOitMtnc},
    {s23_StCks,fnOitMtnc},
                   {s23_StCks,fnOitMtnc},      
                                  {s23_StCks,fnOitMtnc},      
                                                 MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s23_StCks,fnOitMtnc}, 
                   MISMATCH,      MISMATCH,      MISMATCH,      {s23_StCks,fnOitMtnc},   
    {s23_StCks,fnOitMtnc},      
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    // SYS: Run response in Normal Mode
    // v59_SysPrtModeRunRsp 
    // hits s29_SysRspPnd, s31_BobError
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      CM_DONT_CARE,  MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s10_PrtModeStg,fnSysPrtModeRunRsp},
    MISMATCH,      {s35_RunEjt,fnAutoReq_RunEject},      
                                  MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // SYS: Run response in Seal Only Mode
    // v60_SysSlOnlyRunRsp 
    // hits s29_SysRspPnd
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s19_SlOnlyRun,fnSysSlOnlyRunRsp},
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      {s19_SlOnlyRun,fnSysSlOnlyRunRsp},      
                                  MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

    
    // Reserved 61
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Reserved 62
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // Bob: Debit & image commit 
    // v63_BobDebitCommitRsp 
    // hits s20_PreCvr, s28_BobCommitRspPnd
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s13_Prt,fnDebitCommitRsp},
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      {s13_Prt,fnDebitCommitRsp},      
                                                                MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // OIT: Eject mail piece request
    // v64_EjtReq 
    // hits s10_PrtModeStg, s11_ImgGen, s29_SysRspPnd
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s12_Ejt,fnEject},      
                   {s12_Ejt,fnEject}, 
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s12_Ejt,fnEject},   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    // OIT: Get status request 
    // v65_GetSttsReq 
    // hits any states 
  { INVALID,       {s30_NoAction,fnOitGetSttsRsp}, 
                                  {s30_NoAction,fnOitGetSttsRsp},      
                                                {s30_NoAction,fnOitGetSttsRsp},      
                                                                {s30_NoAction,fnOitGetSttsRsp},   
    {s30_NoAction,fnOitGetSttsRsp},       
                   {s30_NoAction,fnOitGetSttsRsp},      
                                  {s30_NoAction,fnOitGetSttsRsp},      
                                                {s30_NoAction,fnOitGetSttsRsp},      
                                                                {s30_NoAction,fnOitGetSttsRsp},   
    {s30_NoAction,fnOitGetSttsRsp},       
                   {s30_NoAction,fnOitGetSttsRsp},      
                                  {s30_NoAction,fnOitGetSttsRsp},      
                                                {s30_NoAction,fnOitGetSttsRsp},      
                                                                {s30_NoAction,fnOitGetSttsRsp},   
    {s30_NoAction,fnOitGetSttsRsp},       
                   {s30_NoAction,fnOitGetSttsRsp},      
                                  {s30_NoAction,fnOitGetSttsRsp},      
                                                {s30_NoAction,fnOitGetSttsRsp},      
                                                                {s30_NoAction,fnOitGetSttsRsp},   
    {s30_NoAction,fnOitGetSttsRsp},       
                   {s30_NoAction,fnOitGetSttsRsp},      
                                  {s30_NoAction,fnOitGetSttsRsp},      
                                                {s30_NoAction,fnOitGetSttsRsp},      
                                                                {s30_NoAction,fnOitGetSttsRsp},   
    {s30_NoAction,fnOitGetSttsRsp},       
                   {s30_NoAction,fnOitGetSttsRsp},      
                                  {s30_NoAction,fnOitGetSttsRsp},      
                                                {s30_NoAction,fnOitGetSttsRsp},      
                                                                {s30_NoAction,fnOitGetSttsRsp},   
    {s30_NoAction,fnOitGetSttsRsp},       
                   {s30_NoAction,fnOitGetSttsRsp},      
                                  {s30_NoAction,fnOitGetSttsRsp},      
                                                {s30_NoAction,fnOitGetSttsRsp},      
                                                                {s30_NoAction,fnOitGetSttsRsp},   
    {s30_NoAction,fnOitGetSttsRsp},       
                   {s30_NoAction,fnOitGetSttsRsp},      
                                  {s30_NoAction,fnOitGetSttsRsp},      
                                                {s30_NoAction,fnOitGetSttsRsp},      
                                                                {s30_NoAction,fnOitGetSttsRsp},   
    {s30_NoAction,fnOitGetSttsRsp},       
                   {s30_NoAction,fnOitGetSttsRsp},      
                                  {s30_NoAction,fnOitGetSttsRsp},      
                                                {s30_NoAction,fnOitGetSttsRsp},      
                                                                {s30_NoAction,fnOitGetSttsRsp},   
    {s30_NoAction,fnOitGetSttsRsp},       
                   {s30_NoAction,fnOitGetSttsRsp},      
                                  {s30_NoAction,fnOitGetSttsRsp},      
                                                {s30_NoAction,fnOitGetSttsRsp},      
                                                                {s30_NoAction,fnOitGetSttsRsp},   

    {s30_NoAction,fnOitGetSttsRsp},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    // init request (DM400C)
    // v66_4InitReq 
  { INVALID,       {s65_4FdrInit,fnFdrInit},     
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      {s65_4FdrInit,fnFdrInit},      
                                                                MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s30_NoAction,fnFdrInit},      
                    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // SYS: start mail run
    // v67_4StartMailRun
    // Hits s4_NmlIdle,s5_SlOnlyIdle
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      {s60_4MailRunPnd,fnCheckPmMtnc},
    {s60_4MailRunPnd,fnCheckPmMtnc},      
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   
    
    // SYS: Start tape run
    // v68_StartTapeRun
    // hits s4_NmlIdle
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      {s67_TapeRunPnd,fnCheckPmMtnc},   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH }, 
    
    // Head/tank errors reported in messages other than Mtnc Rsp & Run Rsp.
    // v69_HdInkErr, hits all states. But it is ignored during printing and 
    // reported after the last response (Print Rsp or Run Rsp for seal only)
  { INVALID,       {s46_HdInkErr,fnHdInkErr},
                                  {s30_NoAction,fnInitRsp_HeadErr},// Continue init
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                CM_DONT_CARE,
    CM_DONT_CARE,
                   CM_DONT_CARE,
                                  {s46_HdInkErr,fnCmDontCare},  
                                                 {s46_HdInkErr,NULL},  
                                                                {s46_HdInkErr,fnHdInkErr},
    {s46_HdInkErr,fnHdInkErr},
                   {s30_NoAction,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                CM_DONT_CARE,
                                                                
    {s30_NoAction,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},

    {s46_HdInkErr,fnHdInkErr},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
                                                                
    
    // PSOC:
    // v70_PsocReseedErr
    // Hits s4_NmlIdle, s18_RplFnsh_5V_ON, s32_PsocReseedRspPnd, s33_PsocCommitRspPnd 
  { INVALID,       {s30_NoAction,fnPsocErrRsp}, 
                                  {s30_NoAction,fnPsocErrRsp},
                                                 {s30_NoAction,fnPsocErrRsp},
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocCommitSeed},  
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s35_RunEjt,fnAutoReq_RunEject},  
                                                 {s35_RunEjt,fnAutoReq_RunEject},   
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},
    {s30_NoAction,fnPsocErrRsp},
                   {s30_NoAction,fnPsocErrRsp},  
                                  {s30_NoAction,fnPsocErrRsp},  
                                                 {s30_NoAction,fnPsocErrRsp},  
                                                                {s30_NoAction,fnPsocErrRsp},

    {s30_NoAction,fnPsocErrRsp},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    // v71_PmPprErr
    // No Paper error (not an error in PB side)
    // hits s10_PrtModeStg, s13_Prt, s19_SlOnlyRun 
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s39_PprErr,fnNoPaper},      
                   MISMATCH,      MISMATCH,      {s39_PprErr,fnNoPaper},      
                                                                MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s39_PprErr,fnNoPaper},   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s39_PprErr,fnNoPaper},   
    {s30_NoAction,fnNoPaper},      
                   MISMATCH,      {s30_NoAction,fnNoPaper},      
                                                 {s30_NoAction,fnNoPaper},      
                                                                {s30_NoAction,fnNoPaper},   
    {s30_NoAction,fnNoPaper},      
                   {s30_NoAction,fnNoPaper},      
                                  {s30_NoAction,fnNoPaper},      
                                                 {s30_NoAction,fnNoPaper},      
                                                                {s30_NoAction,fnNoPaper},   

    {s30_NoAction,fnNoPaper},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    // reserved 72
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    // Jam error: reported when executing printing related commands like 
    // <Print>, <Run> or <Eject>
    // v73_PmRspJamErr = 73,  
    // hits s13_Prt 13, s10_PrtModeStg 
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s40_PrtJamErr,fnPmJamErr},      
                   MISMATCH,      MISMATCH,      {s40_PrtJamErr,fnPmJamErr},
                                                                MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s40_PrtJamErr,fnPmJamErr},
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnPmJamErr},   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s40_PrtJamErr,fnPmJamErr},
                   MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnPmJamErr},   
    {s30_NoAction,fnPmJamErr},
                   MISMATCH,      {s30_NoAction,fnPmJamErr},      
                                                 {s30_NoAction,fnPmJamErr},      
                                                                {s30_NoAction,fnPmJamErr},   
    {s30_NoAction,fnPmJamErr},
                   {s30_NoAction,fnPmJamErr},      
                                  {s30_NoAction,fnPmJamErr},      
                                                 {s30_NoAction,fnPmJamErr},      
                                                                {s30_NoAction,fnPmJamErr},   

    {s30_NoAction,fnPmJamErr},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s40_PrtJamErr,fnPmJamErr},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    // Paper Error(Skew,No Paper,Too Short)
    // reported when executing transport commands like 
    // <Print>, <Run> or <Eject>
    // v74_PmRspPprErr = 74, 
    // hits s10_PrtModeStg, s12_Ejt, s13_Prt, s19_SlOnlyRun 
  { INVALID,       {s44_PrtErr,fnPmPprErr},
                                  {s44_PrtErr,fnPmPprErr}, 
                                                 {s44_PrtErr,fnPmPprErr},      
                                                                {s44_PrtErr,fnPmPprErr},   
    {s44_PrtErr,fnPmPprErr},      
                   {s44_PrtErr,fnPmPprErr},      
                                  {s44_PrtErr,fnPmPprErr},      
                                                 {s44_PrtErr,fnPmPprErr},      
                                                                {s44_PrtErr,fnPmPprErr},   
    {s43_StgErr,fnPmPprErr},
                   {s44_PrtErr,fnPmPprErr},      
                                  {s42_EjtErr,fnPmPprErr},  
                                                 {s44_PrtErr,fnPmPprErr},
                                                                {s44_PrtErr,fnPmPprErr},
    {s44_PrtErr,fnPmPprErr},      
                   {s44_PrtErr,fnPmPprErr},      
                                  {s44_PrtErr,fnPmPprErr},      
                                                 {s44_PrtErr,fnPmPprErr},      
                                                                {s45_SlRunErr,fnPmPprErr},
    {s44_PrtErr,fnPmPprErr},      
                   {s44_PrtErr,fnPmPprErr},      
                                  {s44_PrtErr,fnPmPprErr},      
                                                 {s44_PrtErr,fnPmPprErr},      
                                                                {s44_PrtErr,fnPmPprErr},
    {s44_PrtErr,fnPmPprErr},      
                   {s44_PrtErr,fnPmPprErr},      
                                  {s44_PrtErr,fnPmPprErr},      
                                                 {s44_PrtErr,fnPmPprErr},      
                                                                {s44_PrtErr,fnPmPprErr},
    {s44_PrtErr,fnPmPprErr},      
                   {s44_PrtErr,fnPmPprErr},      
                                  {s44_PrtErr,fnPmPprErr},      
                                                 {s44_PrtErr,fnPmPprErr},      
                                                                {s44_PrtErr,fnPmPprErr},
    {s44_PrtErr,fnPmPprErr},      
                   {s44_PrtErr,fnPmPprErr},      
                                  {s44_PrtErr,fnPmPprErr},      
                                                 {s44_PrtErr,fnPmPprErr},      
                                                                {s44_PrtErr,fnPmPprErr},
    {s44_PrtErr,fnPmPprErr},      
                   {s30_NoAction,fnPmPprErr},      
                                  {s44_PrtErr,fnPmPprErr},      
                                                 {s44_PrtErr,fnPmPprErr},      
                                                                {s44_PrtErr,fnPmPprErr},
    {s44_PrtErr,fnPmPprErr},      
                   {s44_PrtErr,fnPmPprErr},      
                                  {s44_PrtErr,fnPmPprErr},      
                                                 {s44_PrtErr,fnPmPprErr},      
                                                                {s44_PrtErr,fnPmPprErr},

    {s44_PrtErr,fnPmPprErr},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    // Print Head/Ink Tank Error that is reported in Maintenance response or Run response
    // v75_PmRspHdErr,  
    // hits any state 
  { INVALID,       {s46_HdInkErr,fnHdInkErr},
                                  {s30_NoAction,fnInitRsp_HeadErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr}, // {fnAutoReq_RunEject},
    {s46_HdInkErr,fnEject},
                   {s46_HdInkErr,fnEject},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s30_NoAction,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s30_NoAction,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr_Oit},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr_SysModeRsp},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},  
                                                 {s46_HdInkErr,fnHdInkErr},  
                                                                {s46_HdInkErr,fnHdInkErr},
                                                                
    {s46_HdInkErr,fnHdInkErr},
                   {s46_HdInkErr,fnHdInkErr},
                                  {s46_HdInkErr,fnHdInkErr},
                                                 {s46_HdInkErr,fnHdInkErr},
                                                                {s46_HdInkErr,fnHdInkErr},

    {s46_HdInkErr,fnHdInkErr},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
                                                                
    
    // Diagnostic Error
    // v76_PmRspDiagErr = 76,  
    // hits s6_DiagIdle 6
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      {s6_DiagIdle,fnDiagErrRsp},
                                  MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    // PM Fatal error
    // v77_PmFtlErr = 77,   
    // hits any state
  { INVALID,       {s49_FtlErr,fnFtlErr},
                                  {s30_NoAction,fnInitRsp_FtlErr},
                                                 {s49_FtlErr,fnFtlErr},
                                                                {s49_FtlErr,fnFtlErr},
    
    {s49_FtlErr,fnFtlErr},
                   {s49_FtlErr,fnFtlErr},
                                  {s49_FtlErr,fnFtlErr},
                                                 {s49_FtlErr,fnFtlErr},
                                                                {s49_FtlErr,fnFtlErr},
    
    {s49_FtlErr,fnFtlErr},
                   {s49_FtlErr,fnFtlErr},
                                  {s49_FtlErr,fnFtlErr},
                                                 {s49_FtlErr,fnFtlErr},
                                                                {s49_FtlErr,fnFtlErr},
    
    {s49_FtlErr,fnFtlErr},
                   {s49_FtlErr,fnFtlErr},
                                  {s49_FtlErr,fnFtlErr},
                                                 {s49_FtlErr,fnFtlErr},
                                                                {s49_FtlErr,fnFtlErr},
    
    {s49_FtlErr,fnFtlErr},
                   {s49_FtlErr,fnFtlErr},
                                  {s49_FtlErr,fnFtlErr},
                                                 {s49_FtlErr,fnFtlErr},
                                                                {s49_FtlErr,fnFtlErr},
    
    {s49_FtlErr,fnFtlErr},
                   {s49_FtlErr,fnFtlErr},
                                  {s49_FtlErr,fnFtlErr},
                                                 {s49_FtlErr,fnFtlErr},
                                                                {s49_FtlErr,fnFtlErr},
    
    {s49_FtlErr,fnFtlErr},
                   {s49_FtlErr,fnFtlErr},
                                  {s49_FtlErr,fnFtlErr},
                                                 {s49_FtlErr,fnFtlErr},
                                                                {s49_FtlErr,fnFtlErr},
    
    {s49_FtlErr,fnFtlErr},
                   {s49_FtlErr,fnFtlErr},
                                  {s49_FtlErr,fnFtlErr},
                                                 {s49_FtlErr,fnFtlErr},
                                                                {s49_FtlErr,fnFtlErr},
    {s49_FtlErr,fnFtlErr},
                   {s49_FtlErr,fnFtlErr},
                                  {s49_FtlErr,fnFtlErr},
                                                 {s49_FtlErr,fnFtlErr},
                                                                {s49_FtlErr,fnFtlErr},
    
    {s49_FtlErr,fnFtlErr}, {s49_FtlErr,fnFtlErr},
                                  {s49_FtlErr,fnFtlErr},
                                                 {s49_FtlErr,fnFtlErr},
                                                                {s49_FtlErr,fnFtlErr},

    {s49_FtlErr,fnFtlErr},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    // CM message error
    // v78_PmMsgErr = 78,   
    // hits any state
  { INVALID,       {s30_NoAction,fnPmMsgErr},  
                                  {s30_NoAction,fnPmMsgErr},
                                                 {s30_NoAction, fnPmMsgErr},
                                                                {s30_NoAction, fnPmMsgErr},
    {s30_NoAction,fnPmMsgErr},
                   {s30_NoAction,fnPmMsgErr},
                                  {s30_NoAction,fnPmMsgErr},
                                                 {s30_NoAction, fnPmMsgErr},
                                                                {s30_NoAction, fnPmMsgErr},

    {s30_NoAction,fnPmMsgErr},
                   {s30_NoAction,fnPmMsgErr},
                                  {s30_NoAction,fnPmMsgErr},
                                                 {s30_NoAction, fnPmMsgErr},
                                                                {s30_NoAction, fnPmMsgErr},

    {s30_NoAction,fnPmMsgErr},
                   {s30_NoAction,fnPmMsgErr},
                                  {s30_NoAction,fnPmMsgErr},
                                                 {s30_NoAction, fnPmMsgErr},
                                                                {s30_NoAction, fnPmMsgErr},

    {s30_NoAction,fnPmMsgErr},
                   {s30_NoAction,fnPmMsgErr},
                                  {s30_NoAction,fnPmMsgErr},
                                                 {s30_NoAction, fnPmMsgErr},
                                                                {s30_NoAction, fnPmMsgErr},
    
    {s30_NoAction,fnPmMsgErr},
                   {s30_NoAction,fnPmMsgErr},
                                  {s30_NoAction,fnPmMsgErr},
                                                 {s30_NoAction, fnPmMsgErr},
                                                                {s30_NoAction, fnPmMsgErr},

    {s30_NoAction,fnPmMsgErr},
                   {s30_NoAction,fnPmMsgErr},
                                  {s30_NoAction,fnPmMsgErr},
                                                 {s30_NoAction, fnPmMsgErr},
                                                                {s30_NoAction, fnPmMsgErr},

    {s30_NoAction,fnPmMsgErr},
                   {s30_NoAction,fnPmMsgErr},
                                  {s30_NoAction,fnPmMsgErr},
                                                 {s30_NoAction, fnPmMsgErr},
                                                                {s30_NoAction, fnPmMsgErr},

    {s30_NoAction,fnPmMsgErr},
                   {s30_NoAction,fnPmMsgErr},
                                  {s30_NoAction,fnPmMsgErr},
                                                 {s30_NoAction, fnPmMsgErr},
                                                                {s30_NoAction, fnPmMsgErr},
    {s30_NoAction,fnPmMsgErr},
                   {s30_NoAction,fnPmMsgErr},
                                  {s30_NoAction,fnPmMsgErr},
                                                 {s30_NoAction, fnPmMsgErr},
                                                                {s30_NoAction, fnPmMsgErr},

    {s30_NoAction, fnPmMsgErr},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },
    
    
    // PM init fatal error
    // v79_PmInitErr = 79                 
    // hits any state
  { INVALID,        {s30_NoAction,fnPmInitErr},
                                  {s30_NoAction,fnInitRsp_FtlErr},
                                                 {s30_NoAction, fnPmInitErr},
                                                                {s30_NoAction, fnPmInitErr},
    {s30_NoAction,fnPmInitErr},
                    {s30_NoAction,fnPmInitErr},
                                  {s30_NoAction,fnPmInitErr},
                                                 {s30_NoAction, fnPmInitErr},
                                                                {s30_NoAction, fnPmInitErr},
                                                                
    {s30_NoAction,fnPmInitErr},
                    {s30_NoAction,fnPmInitErr},
                                  {s30_NoAction,fnPmInitErr},
                                                 {s30_NoAction, fnPmInitErr},
                                                                {s30_NoAction, fnPmInitErr},
                                                                
    {s30_NoAction,fnPmInitErr},
                    {s30_NoAction,fnPmInitErr},
                                  {s30_NoAction,fnPmInitErr},
                                                 {s30_NoAction, fnPmInitErr},
                                                                {s30_NoAction, fnPmInitErr},
                                                                
    {s30_NoAction,fnPmInitErr},
                    {s30_NoAction,fnPmInitErr},
                                  {s30_NoAction,fnPmInitErr},
                                                 {s30_NoAction, fnPmInitErr},
                                                                {s30_NoAction, fnPmInitErr},
                                                                
    {s30_NoAction,fnPmInitErr},
                    {s30_NoAction,fnPmInitErr},
                                  {s30_NoAction,fnPmInitErr},
                                                 {s30_NoAction, fnPmInitErr},
                                                                {s30_NoAction, fnPmInitErr},
                                                                
    {s30_NoAction,fnPmInitErr},
                    {s30_NoAction,fnPmInitErr},
                                  {s30_NoAction,fnPmInitErr},
                                                 {s30_NoAction, fnPmInitErr},
                                                                {s30_NoAction, fnPmInitErr},
                                                                
    {s30_NoAction,fnPmInitErr},
                    {s30_NoAction,fnPmInitErr},
                                  {s30_NoAction,fnPmInitErr},
                                                 {s30_NoAction, fnPmInitErr},
                                                                {s30_NoAction, fnPmInitErr},
                                                                
    {s30_NoAction,fnPmInitErr},
                    {s30_NoAction,fnPmInitErr},
                                  {s30_NoAction,fnPmInitErr},
                                                 {s30_NoAction, fnPmInitErr},
                                                                {s30_NoAction, fnPmInitErr},
                                                                
    {s30_NoAction,fnPmInitErr},
                    {s30_NoAction,fnPmInitErr},
                                  {s30_NoAction,fnPmInitErr},
                                                 {s30_NoAction, fnPmInitErr},
                                                                {s30_NoAction, fnPmInitErr},

    {s30_NoAction, fnPmInitErr},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

  //
  // v80_FdrErr
  // Continue init so at least tape can be printed
  // 
  { INVALID,       {s41_FdrErr,fnRptFdrErr},
                                  {s41_FdrErr,fnRptFdrErr},
                                                 {s41_FdrErr,fnRptFdrErr},
                                                                {s41_FdrErr,fnRptFdrErr},
    {s41_FdrErr,fnRptFdrErr},
                   {s41_FdrErr,fnRptFdrErr},
                                  {s41_FdrErr,fnRptFdrErr},
                                                 {s41_FdrErr,fnRptFdrErr},
                                                                CM_DONT_CARE,
    CM_DONT_CARE,
                   CM_DONT_CARE,
                                  {s41_FdrErr,fnCmDontCare},  
                                                 {s41_FdrErr,fnRptFdrErr},  
                                                                {s41_FdrErr,fnRptFdrErr},
    {s41_FdrErr,fnRptFdrErr},
                   {s30_NoAction,fnRptFdrErr},
                                  {s41_FdrErr,fnRptFdrErr},  
                                                 {s41_FdrErr,fnRptFdrErr},  
                                                                CM_DONT_CARE,
                                                                
    {s30_NoAction,fnRptFdrErr},
                   {s41_FdrErr,fnRptFdrErr},
                                  {s41_FdrErr,fnRptFdrErr},  
                                                 {s41_FdrErr,fnRptFdrErr},  
                                                                {s41_FdrErr,fnRptFdrErr},
                                                                
    {s41_FdrErr,fnRptFdrErr},
                   {s41_FdrErr,fnRptFdrErr},
                                  {s41_FdrErr,fnRptFdrErr},  
                                                 {s41_FdrErr,fnRptFdrErr},  
                                                                {s41_FdrErr,fnRptFdrErr},
                                                                
    {s41_FdrErr,fnRptFdrErr},
                   {s41_FdrErr,fnRptFdrErr},
                                  {s41_FdrErr,fnRptFdrErr},  
                                                 {s41_FdrErr,fnRptFdrErr},  
                                                                {s41_FdrErr,fnRptFdrErr},
                                                                
    {s41_FdrErr,fnRptFdrErr},
                   {s41_FdrErr,fnRptFdrErr},
                                  {s41_FdrErr,fnRptFdrErr},  
                                                 {s41_FdrErr,fnRptFdrErr},  
                                                                {s41_FdrErr,fnRptFdrErr},
                                                                
    {s41_FdrErr,fnRptFdrErr},
                   {s41_FdrErr,fnRptFdrErr},
                                  {s41_FdrErr,fnRptFdrErr},  
                                                 {s41_FdrErr,fnRptFdrErr},  
                                                                {s41_FdrErr,fnRptFdrErr},
                                                                
    {s41_FdrErr,fnRptFdrErr},
                   {s41_FdrErr,fnRptFdrErr},
                                  {s41_FdrErr,fnRptFdrErr},  
                                                 {s41_FdrErr,fnRptFdrErr},  
                                                                {s41_FdrErr,fnRptFdrErr},

    {s41_FdrErr,fnRptFdrErr},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s30_NoAction,fnSysInitReq},      
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

  // v81_TapeErr
  // Hits s67_TapeRunPnd and others
  { INVALID,       {s50_TapeErr,fnStopMainTransport},      
                                  {s50_TapeErr,fnStopMainTransport},      
                                                 {s50_TapeErr,fnStopMainTransport},      
                                                                {s50_TapeErr,fnStopMainTransport},
    {s50_TapeErr,fnStopMainTransport},      
                   {s50_TapeErr,fnStopMainTransport},      
                                  {s50_TapeErr,fnStopMainTransport},      
                                                 {s50_TapeErr,fnStopMainTransport},      
                                                                {s50_TapeErr,fnStopMainTransport},
    {s50_TapeErr,fnStopMainTransport},      
                   {s50_TapeErr,fnStopMainTransport},      
                                  {s50_TapeErr,fnStopMainTransport},      
                                                 {s50_TapeErr,fnStopMainTransport},      
                                                                {s50_TapeErr,fnStopMainTransport},
    {s50_TapeErr,fnStopMainTransport},      
                   {s50_TapeErr,fnStopMainTransport},      
                                  {s50_TapeErr,fnStopMainTransport},      
                                                 {s50_TapeErr,fnStopMainTransport},      
                                                                {s50_TapeErr,fnStopMainTransport},
    {s50_TapeErr,fnStopMainTransport},      
                   {s50_TapeErr,fnStopMainTransport},      
                                  {s50_TapeErr,fnStopMainTransport},      
                                                 {s50_TapeErr,fnStopMainTransport},      
                                                                {s50_TapeErr,fnStopMainTransport},
    {s50_TapeErr,fnStopMainTransport},      
                   {s50_TapeErr,fnStopMainTransport},      
                                  {s50_TapeErr,fnStopMainTransport},      
                                                 {s50_TapeErr,fnStopMainTransport},      
                                                                {s50_TapeErr,fnStopMainTransport},
    {s50_TapeErr,fnStopMainTransport},      
                   {s50_TapeErr,fnStopMainTransport},      
                                  {s50_TapeErr,fnStopMainTransport},      
                                                 {s50_TapeErr,fnStopMainTransport},      
                                                                {s50_TapeErr,fnStopMainTransport},
    {s50_TapeErr,fnStopMainTransport},      
                   {s50_TapeErr,fnStopMainTransport},      
                                  {s50_TapeErr,fnStopMainTransport},      
                                                 {s50_TapeErr,fnStopMainTransport},      
                                                                {s50_TapeErr,fnStopMainTransport},
    {s50_TapeErr,fnStopMainTransport},      
                   {s50_TapeErr,fnStopMainTransport},      
                                  {s50_TapeErr,fnStopMainTransport},      
                                                 {s50_TapeErr,fnStopMainTransport},      
                                                                {s50_TapeErr,fnStopMainTransport},
    {s50_TapeErr,fnStopMainTransport},      
                   {s50_TapeErr,fnStopMainTransport},      
                                  {s50_TapeErr,fnStopMainTransport},      
                                                 {s50_TapeErr,fnStopMainTransport},      
                                                                {s50_TapeErr,fnStopMainTransport},
    {s50_TapeErr,fnStopMainTransport},      
                   {s50_TapeErr,fnStopMainTransport},      
                                  {s50_TapeErr,fnStopMainTransport},      
                                                 {s50_TapeErr,fnStopMainTransport},      
                                                                {s50_TapeErr,fnStopMainTransport},
    {s50_TapeErr,fnStopMainTransport},      
                   {s50_TapeErr,fnStopMainTransport},      
                                  {s50_TapeErr,fnStopMainTransport},      
                                                 {s50_TapeErr,fnStopMainTransport},      
                                                                {s50_TapeErr,fnStopMainTransport},
    {s50_TapeErr,fnStopMainTransport},      
                   {s50_TapeErr,fnStopMainTransport},      
                                  {s50_TapeErr,fnStopMainTransport},      
                                                 {s50_TapeErr,fnStopMainTransport},      
                                                                {s50_TapeErr,fnStopMainTransport},
    {s50_TapeErr,fnStopMainTransport},      
                   {s50_TapeErr,fnStopMainTransport},      
                                  {s50_TapeErr,fnStopMainTransport},      
                                                 {s50_TapeErr,fnStopMainTransport},      
                                                                {s50_TapeErr,fnStopMainTransport} },

  // v82_PmRspSkewErr (skew error from Run Response)
  // Hits s10_PrtModeStg
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      {s12_Ejt,fnEject},
    {s12_Ejt,fnEject}, 
                   {s12_Ejt,fnEject},
                                  {s12_Ejt,fnEject},
                                                 {s12_Ejt,fnEject},
                                                                {s44_PrtErr,fnCmDontCare},
    CM_DONT_CARE, 
                   {s12_Ejt,fnEject},
                                  {s22_Idle,fnPmPprErr},
                                                 {s12_Ejt,fnEject},
                                                                {s12_Ejt,fnEject},
    {s12_Ejt,fnEject}, 
                   {s12_Ejt,fnEject},
                                  {s12_Ejt,fnEject},
                                                 {s12_Ejt,fnEject},
                                                                {s12_Ejt,fnEject},
    {s12_Ejt,fnEject}, 
                   {s12_Ejt,fnEject},
                                  {s12_Ejt,fnEject},
                                                 {s12_Ejt,fnEject},
                                                                {s12_Ejt,fnEject},
    {s12_Ejt,fnEject}, 
                   {s12_Ejt,fnEject},
                                  {s12_Ejt,fnEject},
                                                 {s12_Ejt,fnEject},
                                                                {s12_Ejt,fnEject},
    {s12_Ejt,fnEject}, 
                   {s12_Ejt,fnEject},
                                  {s12_Ejt,fnEject},
                                                 {s12_Ejt,fnEject},
                                                                {s12_Ejt,fnEject},
    {s12_Ejt,fnEject}, 
                   {s12_Ejt,fnEject},
                                  {s12_Ejt,fnEject},
                                                 {s12_Ejt,fnEject},
                                                                {s12_Ejt,fnEject},
    {s12_Ejt,fnEject}, 
                   {s12_Ejt,fnEject},
                                  {s12_Ejt,fnEject},
                                                 {s12_Ejt,fnEject},
                                                                {s12_Ejt,fnEject},
    {s12_Ejt,fnEject}, 
                   {s12_Ejt,fnEject},
                                  {s12_Ejt,fnEject},
                                                 {s12_Ejt,fnEject},
                                                                {s12_Ejt,fnEject},
    {s12_Ejt,fnEject}, 
                   {s12_Ejt,fnEject},
                                  {s12_Ejt,fnEject},
                                                 {s12_Ejt,fnEject},
                                                                {s12_Ejt,fnEject},
    {s12_Ejt,fnEject}, 
                   {s12_Ejt,fnEject},
                                  {s12_Ejt,fnEject},
                                                 {s12_Ejt,fnEject},
                                                                {s12_Ejt,fnEject},
    {s12_Ejt,fnEject}, 
                   {s12_Ejt,fnEject},
                                  {s12_Ejt,fnEject},
                                                 {s12_Ejt,fnEject},
                                                                {s12_Ejt,fnEject},
    {s12_Ejt,fnEject}, 
                   {s12_Ejt,fnEject},
                                  {s12_Ejt,fnEject},
                                                 {s12_Ejt,fnEject},
                                                                {s12_Ejt,fnEject} },

  // Resereved 83
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

  // Resereved 84
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

  // Resereved 85
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

  // Resereved 86
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

  // Resereved 87
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

  // Resereved 88
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

  // Resereved 89
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

    // Reserved 90
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

  // v91_FdrInitRsp
  // Hits s65_4FdrInit
  // 
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    {s2_Init,fnSysInitReq},
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

// Reserved 92
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

  // v93_FdrStatus 
  // Unsolicited feeder status
  { INVALID,       {s30_NoAction,fnFdrStatus},   
                                  {s30_NoAction,fnFdrStatus},      
                                                 {s30_NoAction,fnFdrStatus},      
                                                                {s30_NoAction,fnFdrStatus},
    {s30_NoAction,fnFdrStatus},
                   {s30_NoAction,fnFdrStatus},
                                  {s30_NoAction,fnFdrStatus},
                                                 {s30_NoAction,fnFdrStatus},
                                                                {s30_NoAction,fnFdrStatus},
    {s30_NoAction,fnFdrStatus},
                   {s30_NoAction,fnFdrStatus},
                                  {s30_NoAction,fnFdrStatus},
                                                 {s30_NoAction,fnFdrStatus},
                                                                {s30_NoAction,fnFdrStatus},
    {s30_NoAction,fnFdrStatus},
                   {s30_NoAction,fnFdrStatus},
                                  {s30_NoAction,fnFdrStatus},
                                                 {s30_NoAction,fnFdrStatus},
                                                                {s30_NoAction,fnFdrStatus},
    {s30_NoAction,fnFdrStatus},
                   {s30_NoAction,fnFdrStatus},
                                  {s30_NoAction,fnFdrStatus},
                                                 {s30_NoAction,fnFdrStatus},
                                                                {s30_NoAction,fnFdrStatus},
    {s30_NoAction,fnFdrStatus},
                   {s30_NoAction,fnFdrStatus},
                                  {s30_NoAction,fnFdrStatus},
                                                 {s30_NoAction,fnFdrStatus},
                                                                {s30_NoAction,fnFdrStatus},
    {s30_NoAction,fnFdrStatus},
                   {s30_NoAction,fnFdrStatus},
                                  {s30_NoAction,fnFdrStatus},
                                                 {s30_NoAction,fnFdrStatus},
                                                                {s30_NoAction,fnFdrStatus},
    {s30_NoAction,fnFdrStatus},
                   {s30_NoAction,fnFdrStatus},
                                  {s30_NoAction,fnFdrStatus},
                                                 {s30_NoAction,fnFdrStatus},
                                                                {s30_NoAction,fnFdrStatus},
    {s30_NoAction,fnFdrStatus},
                   {s30_NoAction,fnFdrStatus},
                                  {s30_NoAction,fnFdrStatus},
                                                 {s30_NoAction,fnFdrStatus},
                                                                {s30_NoAction,fnFdrStatus},
    {s30_NoAction,fnFdrStatus},
                   {s30_NoAction,fnFdrStatus},
                                  {s30_NoAction,fnFdrStatus},
                                                 {s30_NoAction,fnFdrStatus},
                                                                {s30_NoAction,fnFdrStatus},
    {s30_NoAction,fnFdrStatus},
                   {s30_NoAction,fnFdrStatus},
                                  {s30_NoAction,fnFdrStatus},
                                                 {s30_NoAction,fnFdrStatus},
                                                                {s30_NoAction,fnFdrStatus},
    {s30_NoAction,fnFdrStatus},
                   {s30_NoAction,fnFdrStatus},
                                  {s30_NoAction,fnFdrStatus},
                                                 {s30_NoAction,fnFdrStatus},
                                                                {s30_NoAction,fnFdrStatus},
    {s30_NoAction,fnFdrStatus},
                   {s30_NoAction,fnFdrStatus},
                                  {s30_NoAction,fnFdrStatus},
                                                 {s30_NoAction,fnFdrStatus},
                                                                {s30_NoAction,fnFdrStatus},
    {s30_NoAction,fnFdrStatus},
                   {s30_NoAction,fnFdrStatus},
                                  {s30_NoAction,fnFdrStatus},
                                                 {s30_NoAction,fnFdrStatus},
                                                                {s30_NoAction,fnFdrStatus} },

  // v94_FdrRunRsp
  // hits s60_4MailRunPnd
  // 
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      CM_DONT_CARE,
    CM_DONT_CARE,  MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    {s22_Idle,fnStCks_Idle},      
                   MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

  // v95_FdrStopRunRsp
  // Hits s4_NmlIdle, s5_SlOnlyIdle and others
  { INVALID,       MISMATCH,      MISMATCH,      {s30_NoAction,fnStopMainTransport},      {s30_NoAction,fnStopMainTransport},
    {s30_NoAction,fnStopMainTransport},  
                   CM_DONT_CARE,  {s30_NoAction,fnStopMainTransport},  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    {s30_NoAction,fnStopMainTransport},  
                    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  {s30_NoAction,fnStopMainTransport},
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  {s30_NoAction,fnStopMainTransport},  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  {s30_NoAction,fnStopMainTransport},  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,

    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  {s30_NoAction,fnStopMainTransport},
    CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE,  CM_DONT_CARE },

  // Feeder Enter Sleep
  // v96_FdrGoSleep = 96
  // hits s4_NmlIdle 4, s5_SlOnlyIdle 5, 
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnSysGoSleep},
    {s30_NoAction,fnSysGoSleep},
                   MISMATCH,      {s30_NoAction,fnSysGoSleep},      
                                                 MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   

  // Resereved 97
  // Feeder Set Mode Response
    // v97_FdrModeRsp
    // hits s4_NmlIdle, s5_SlOnlyIdle, s6_DiagIdle, s7_DisableIdle, s36_PndgWrng
  { INVALID,       {s30_NoAction,fnFdrSetModeRsp},      
                                  {s30_NoAction,fnFdrSetModeRsp},      
                                                 {s30_NoAction,fnFdrSetModeRsp}, 
                                                                {s4_NmlIdle,fnFdrSetModeRsp},
    {s5_SlOnlyIdle,fnFdrSetModeRsp},   
                   {s6_DiagIdle,fnFdrSetModeRsp},
                                  {s7_DisableIdle,fnFdrSetModeRsp}, //fnSetModeRsp_CvrStatus,                
                                                 {s30_NoAction,fnFdrSetModeRsp},      
                                                                {s30_NoAction,fnFdrSetModeRsp},   
    {s30_NoAction,fnFdrSetModeRsp},      
                   {s30_NoAction,fnFdrSetModeRsp},      
                                  {s30_NoAction,fnFdrSetModeRsp},      
                                                 {s30_NoAction,fnFdrSetModeRsp},      
                                                                {s30_NoAction,fnFdrSetModeRsp},   
    {s30_NoAction,fnFdrSetModeRsp},      
                   {s30_NoAction,fnFdrSetModeRsp},      
                                  {s30_NoAction,fnFdrSetModeRsp},      
                                                 {s30_NoAction,fnFdrSetModeRsp},      
                                                                {s30_NoAction,fnFdrSetModeRsp},   
    {s30_NoAction,fnFdrSetModeRsp},      
                   {s30_NoAction,fnFdrSetModeRsp},      
                                  {s30_NoAction,fnFdrSetModeRsp},      
                                                 {s30_NoAction,fnFdrSetModeRsp},      
                                                                {s30_NoAction,fnFdrSetModeRsp},   
    {s30_NoAction,fnFdrSetModeRsp},      
                   {s30_NoAction,fnFdrSetModeRsp},      
                                  {s30_NoAction,fnFdrSetModeRsp},      
                                                 {s30_NoAction,fnFdrSetModeRsp},      
                                                                {s30_NoAction,fnFdrSetModeRsp},   
    {s30_NoAction,fnFdrSetModeRsp},      
                   {s30_NoAction,fnFdrSetModeRsp},      
                                  {s30_NoAction,fnFdrSetModeRsp},      
                                                 {s30_NoAction,fnFdrSetModeRsp},      
                                                                {s4_NmlIdle,fnFdrSetModeRsp},   
    {s30_NoAction,fnSetModeRsp},      
                   {s7_DisableIdle,fnSetModeRsp},      
                                  {s30_NoAction,fnSetModeRsp},      
                                                 {s30_NoAction,fnSetModeRsp},      
                                                                {s30_NoAction,fnSetModeRsp},   
    {s30_NoAction,fnSetModeRsp},      
                   {s30_NoAction,fnSetModeRsp},      
                                  {s30_NoAction,fnSetModeRsp},      
                                                 {s30_NoAction,fnSetModeRsp},      
                                                                {s30_NoAction,fnSetModeRsp},   
    {s30_NoAction,fnFdrSetModeRsp},      
                   {s30_NoAction,fnFdrSetModeRsp},      
                                  {s30_NoAction,fnFdrSetModeRsp},      
                                                 {s30_NoAction,fnFdrSetModeRsp},      
                                                                {s30_NoAction,fnFdrSetModeRsp},
                                                                
    {s30_NoAction,fnFdrSetModeRsp},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },   

  // v98_SetDiffWeighMode
  // hits s4_NmlIdle, s7_DisableIdle
/*state 0*/ { INVALID,      MISMATCH,      MISMATCH,      MISMATCH,     /*s4_NmlIdle*/
                                                                        /*
                                                                        DSD 1/15/2007
                                                                        Update state transition for event v98_SetDiffWeighMode 
                                                                        in state s4_NmlIdle.  This fixes an issue where a feeder 
                                                                        jam occurs, the user hits the resume key to re-start 
                                                                        the run, but the CM doesn't start the mail run because 
                                                                        the PM was not put into normal mode (it was still disabled).
                                                                         -> old entry {s30_NoAction,fnAckSysModeChange}
                                                                         -> new entry {s4_NmlIdle,fnSetDiffWeighMode}
                                                                        */
                                                                        {s4_NmlIdle,fnSetDiffWeighMode},
/*state 5*/ MISMATCH,       MISMATCH,      /*s7_DisableIdle*/
                                          {s4_NmlIdle,fnSetDiffWeighMode},      
                                                          MISMATCH,      MISMATCH,

/*state 10*/ MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
/*state 15*/ MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
/*state 20*/ MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
/*state 25*/ MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
/*state 30*/ MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
/*state 35*/ MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
/*state 40*/ MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
/*state 45*/ MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
/*state 50*/ MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
/*state 55*/ MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
/*state 60*/ MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
/*state 65*/ MISMATCH,      MISMATCH,      MISMATCH},

    // Feeder Jam error
    // v99_FdrJam = 99,  
    // hits s13_Prt 13, s10_PrtModeStg 
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      {s40_PrtJamErr,fnFdrJamErr},   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s40_PrtJamErr,fnFdrJamErr},      
                   MISMATCH,      MISMATCH,      {s40_PrtJamErr,fnFdrJamErr},
                                                                MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s40_PrtJamErr,fnFdrJamErr},
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnFdrJamErr},   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s40_PrtJamErr,fnFdrJamErr},
                   MISMATCH,      MISMATCH,      MISMATCH,      {s30_NoAction,fnFdrJamErr},   
    {s30_NoAction,fnFdrJamErr},
                   MISMATCH,      {s30_NoAction,fnFdrJamErr},      
                                                 {s30_NoAction,fnFdrJamErr},      
                                                                {s30_NoAction,fnFdrJamErr},   
    {s30_NoAction,fnFdrJamErr},
                   {s30_NoAction,fnFdrJamErr},      
                                  {s30_NoAction,fnFdrJamErr},      
                                                 {s30_NoAction,fnFdrJamErr},      
                                                                {s30_NoAction,fnFdrJamErr},   

    {s30_NoAction,fnFdrJamErr},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    {s40_PrtJamErr,fnFdrJamErr},      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,   
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },


  // Resereved 100
  { INVALID,       MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,

    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,
    MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH,      MISMATCH },

}; // end of cmStateTbl



/*
 * PM message processing funtions
 * Each function corresponds to one intertask message coming from the PM
 */ 
const FN_PROCESS_INT_TASK_MSG fnPmMsgHandlerTbl[] = {
    //PC_AUTOSTART 0x80,        PC_RUN_RSP 0x81             PC_PRINT_RSP 0x82
    fnPrcsAutoReq,              fnPrcsRunRsp,               fnPrcsPrtRsp,
    
    //PC_REQ_PRT_MTNC 0x83      PC_PRT_MTNC_RSP 0x84        PC_SET_MODE_RSP 0x85
    fnPrcsMtncReq,              fnPrcsMtncRsp,              fnPrcsSetModeRsp,
    
    // PC_INIT_PM_RSP 0x86      PC_PM_DIAG_RSP 0x87         PC_RPL_RSP 0x88
    fnPrcsInitRsp,              fnPrcsDiagRsp,              fnPrcsRplRsp,

    // PC_SENSOR_DATA_RSP 0x89  PC_TOP_COVER_STATUS 0x8A    PC_CLR_ERR_RSP 0x8B
    fnPrcsSnsrDataRsp,          fnPrcsTopCvrStts,           fnPrcsClrErrRsp,
    
    // PC_PRT_SHUTDOWN_RSP 0x8C PC_PRTHEAD_DATA_RSP 0x8D    PC_INKTANK_DATA_RSP 0x8E
    fnPrcsShutdownRsp,          fnPrcsPrtHdRsp,             fnPrcsInkTankRsp,
    
    // PC_PM_VER_RSP 0x8F       PC_PRT_STATUS 0x90          PC_EJECT_RSP 0x91
    fnPrcsSwVerRsp,             fnPrcsUnsolicit,            fnPrcsEjtRsp,
    
    // PC_MOT_SENSOR_STATUS x92 PC_JAM_LEVER_STATUS 0x93    PC_RESET_RSP 0x94
    fnPrcsMotionSnsrStts,       fnPrcsJamLvrStts,           fnPrcsResetRsp,

    // PC_PM_NVM_DATA_RSP 0x95  PC_PM_NVM_UPDATE_RSP 0x96   PC_RPL_COMPLETE_RSP 0x97
    fnPrcsNvmDataRsp,           fnPrcsNvmUpdateRsp,         fnPrcsRplCmpltRsp,
    
    // PC_STOP_RUN_RSP 0x98     PC_RUN_TAPE_RSP 0x99        PC_STOP_TAPE_RUN_RSP 0x9A
    fnPrcsStopRunRsp,           fnPrcsTapeRunRsp,           fnPrcsStopTapeRunRsp,

    // PC_PRINT_READY 0x9B
    fnPrcsPrintReady
}; // end of fnPmMsgHandlerTbl[] 



/*
 * SYS message process funtion pointer arrary
 * Each function corresponds to one intertask message coming from the SYS
 */ 
const FN_PROCESS_INT_TASK_MSG fnSysMsgHandlerTbl[] = {
    // Init req (0)     // Mode req(1)          // Run Response(2)  // Start Mail Run(3)
    // SC_INIT_REQ      SC_MODE_REQ             SC_RUN_RSP          SC_START_MAILRUN
    fnPrcsSysInitReq,   fnPrcsSysModeRequest,   fnPrcsSysRunRsp,    fnPrcsSysStartMailRun,
    
    // Stop Mail Run        // Fatal Error Notification
    // SC_STOP_MAILRUN      SC_NOTIFY_FATAL_ERROR
    fnPrcsSysStopMailRun,   fnPrcsSysFatalError

}; // end of fnSysMsgHandlerTbl[]


/*
 * OIT message process funtion pointer arrary
 * Each function corresponds to one intertask message coming from the OIT
 */ 
const FN_PROCESS_INT_TASK_MSG fnOitMsgHandlerTbl[] = {
    
    // Get Data Req (0)  // Get Status Req (1)  // Printer Mtnc (2) // Rate ready (3)
    // OC_GET_DATA_REQ      OC_GET_STATUS_REQ   OC_MTNC_REQ         OC_RATE_READY
    fnPrcsOitGetDataReq, fnPrcsOitGetStatusReq, fnPrcsOitPmMtnc,    fnPrcsOitRateReady,

    // Rate not ready (4) // Reserved           // Diag Action (6) // Clear error req (7)
    // OC_RATE_NOT_READY    OC_RESET_LOG_REQ    OC_DIAG_ACTN_REQ        OC_CLR_ERR_REQ
    fnPrcsOitRateNotReady, NULL,                 fnPrcsOitDiagActn,  fnPrcsOitClrErrReq,

    // Eject (8)         // Replacement start(9)// Replacement complete(10) // Set margin (11)
    // OC_EJECT_REQ         OC_RPL_START        OC_RPL_CMPLT        OC_SET_MARGIN
    fnPrcsOitEject,      fnPrcsOitRplStart,     fnPrcsOitRplCmplt,  fnPrcsOitSetMargin,

    // Feeder Diag Req (12)
    // OC_FDR_DIAG_REQ      OC_GET_NVM_DATA_REQ     OC_CLR_WASTE_CNTS_REQ
    fnPrcsOitFdrDiagReq,    fnPrcsPmNvmReadReq,     fnPrcsClearWasteCounts


}; // end of fnOitMsgHandlerTbl[]
 

/*
 * Feeder message process funtion pointer arrary
 * Each function corresponds to one intertask message coming from the feeder task
 */ 
const FN_PROCESS_INT_TASK_MSG fnFdrMsgHandlerTbl[] = {
    // Init rsp(80)     Mode rsp(81)            Run Response(82)    Stop Run Response(83)
    // FC_INIT_RSP      FC_SET_MODE_RSP         FC_RUN_RSP          FC_STOP_RUN_RSP
    fnPrcsFdrInitRsp,   fnPrcsFdrModeRsp,       fnPrcsFdrRunRsp,    fnPrcsFdrStopRunRsp,
    
    // Get status rsp(84),    Diagnostic rsp (85) Unsolicited msg (86)
    // FC_GET_STATUS_RSP      FC_DIAG_CMD_RSP     FC_UNSLCT_STATUS
    fnPrcsFdrStatusRsp,       fnPrcsFdrDiagCmdRsp,fnPrcsFdrUnslctStatus

}; // end of fnFdrMsgHandlerTbl[]


/*
 * BOB message processing funtions
 * Each function corresponds to one intertask message coming from the BOB
 */ 
const FN_PROCESS_INT_TASK_MSG fnBobMsgHandlerTbl[] = {

    // Commit transaction
    fnPrcsBobDebitCommitRsp

}; // fnBobMsgHandlerTbl


/*
 * PSOC message processing funtions
 * Each function corresponds to one intertask message coming from the PSOC
 */ 
const FN_PROCESS_INT_TASK_MSG fnPsocMsgHandlerTbl[] = {
    // Reseed response      // Commit response  
    fnPrcsPsocReseedRsp,    fnPrcsPsocCommitRsp
}; // fnPsocMsgHandlerTbl

const FN_PROCESS_INT_TASK_MSG fnCMMsgHandlerTbl[] = {
    // clear transport timer expired                Stop the system gracefully
    // CM_TRANSPORT_CLEAR_TIMER_EXPIRED             CM_STOP_MAILRUN_MSG
    fnPrcsTransportClearTimerExpire,                fnPrcsCmStopSystemReq,

    // CM_MAINTENANCE_STOP_TIMER_EXPIRED
    fnPrcsMaintStopReq
};

/***************************************************************************
// FUNCTION NAME: fnCmNewStateByEvt
//
// DESCRIPTION:   This task accepts input event, applied to current state,
//                then returns new state and the index to be called.
//                The new state will not take effective until fnCmSetState()
//                function is called.
//
// AUTHOR: H.Sun    12/05/02
//
// Returns:         Event element
//
// *************************************************************************/
tCmEvtElmnt fnCmNewStateByEvt(
    emCmStateEvent         inputEvt)        // (I) input event 
{
    int     currState = (int)cmState.eCurrentState;
    static const tCmEvtElmnt invalidEvtElmnt = { s30_NoAction, NULL };

    if (currState >= CM_FIRST_EXTENDED_STATE_VALUE)
        return fnCmNewStateByEvt2(inputEvt);
    else if (currState < CM_FIRST_EXTENDED_STATE_VALUE  && inputEvt < CM_START_EXTENDED_EVENT_DEFINITIONS)
        return cmStateFuncTbl[inputEvt][cmState.eCurrentState];
    else
        return invalidEvtElmnt; // DSD 05-16-2007 Fix Fraca 119446
                                //  One issue found during the fix for this
                                //  fraca was that all paths through this
                                //  function did not return a valid return
                                //  value.  The scenario is that if the CM
                                //  is in a non-extended state but an extended
                                //  event is being processed.  The function
                                //  was fixed to return the "do nothing" state entry.
                
        
}// end of fnCmNewStateByEvt()


tCmEvtElmnt fnCmNewStateByEvt2(
    emCmStateEvent         inputEvt)        // (I) input event 
{
    emCmState currState = cmState.eCurrentState;
    tCmEvtElmnt evtElmnt = { s30_NoAction, NULL };

    switch (currState) {
    case s512_Running:
        evtElmnt = fnRunningStateHandler(inputEvt);
        break;
    case s256_StopFdr:
        evtElmnt = fnStopFeederStateHandler(inputEvt);
        break;
    case s257_DisablePm:
        evtElmnt = fnDisablePmStateHandler(inputEvt);
        break;
    case s258_ClearTransport:
        evtElmnt = fnClearTransportStateHandler(inputEvt);
        break;
    case s259_StopTransport:
        evtElmnt = fnStopTransportStateHandler(inputEvt);
        break;
    case s260_StopDoneResetMode:
        evtElmnt = fnStopResetModeStateHandler(inputEvt);
        break;
    case s261_ImmediateStopRecovery:
        evtElmnt = fnImmediateStopRecoveryStateHandler(inputEvt);
        break;
    case s262_StopTape:
        evtElmnt = fnStopTapeStateHandler(inputEvt);
        break;
    default:
        break;
    };
    
    return evtElmnt;            
}// end of fnCmNewStateByEvt2()

/******************************************************************************
// FUNCTION NAME:   fnCmCommitNewState
//
// DESCRIPTION:     Set the current state to the input state
//
// AUTHOR: H.Sun    12/05/02
//
// Returns:         0 for success, others are error
//
// ***************************************************************************/
int fnCmCommitNewState(
    emCmStateEvent       inputEvt,      // (I) input event
    emCmState            newState)      // (I) new state
{
    if (cmState.eCurrentState != newState)
    {
        cmState.ePreviousState = cmState.eCurrentState;
        cmState.eCurrentState = newState;
        cmState.ePreEvt = inputEvt;
    }

    return OK;

} // end of fnCmCommitNewState() 




/*****************************************************************************
// FUNCTION NAME:   fnCmGetCrntState
//
// DESCRIPTION:     Get the current state
//
// AUTHOR: H.Sun    12/05/02
//
// Returns:         Current state
//
// ***************************************************************************/
emCmState fnCmGetCrntState(void)    
{
    return cmState.eCurrentState;

} // end of fnCmGetCrntState() 



/*****************************************************************************
// FUNCTION NAME:   fnCmGetPreState
//
// DESCRIPTION:     Get the previous state
//
// AUTHOR: H.Sun    12/05/02
//
// Returns:         Previous state
//
// ***************************************************************************/
emCmState fnCmGetPreState(void)    
{
    return cmState.ePreviousState;

} // end of fnCmGetPreState 



/*****************************************************************************
// FUNCTION NAME:   fnCmGetPreEvent
//
// DESCRIPTION:     Get the previous event that triggered the transtion to 
//                  current state.
//
// AUTHOR: H.Sun    12/05/02
//
// Returns:         Previous event
//
// ***************************************************************************/
emCmStateEvent fnCmGetPreEvent(void)    
{
    return cmState.ePreEvt;
}


/******************************************************************************
// FUNCTION NAME:   fnMsgState
//
// DESCRIPTION:     Message request / response pair management state
//                  This is to manage the 
//
// AUTHOR: H.Sun    1/19/06
//
// Returns:         TRUE / FALSE
//
// ***************************************************************************/
int fnMsgState(
    emOpcode    eOpcode,        // (I) SET_STATE, MATCH
    uchar       bTaskID,        // (I) task ID is used to differentiate PM and FDR
    emMsgState  newState)       // (I/O) current state
{
    static emMsgState pmMsgState;
    static emMsgState fdrMsgState;

           BOOL retCode = FALSE;
           emMsgState   *tmpPtr = &pmMsgState;

//TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    if( ( fnGetMeterModel() == CSD2 ) && bTaskID == FDR)
    {
         retCode =  TRUE;
    }

    else if(eOpcode == MATCH) 
    {
         if(    ((pmMsgState == fdrMsgState) && (pmMsgState == newState))
            ||  ( fnGetMeterModel() == CSD2 ) )
         {
             retCode =  TRUE;
         }
    }
    else // SET_STATE
    {
        retCode =  TRUE;

        if(bTaskID == FDR) 
        {
             tmpPtr = &fdrMsgState;
        }

        *tmpPtr = newState;
    }

    return retCode;

} // end of fnMsgState() 

//------------------------------------------------------------------------------
// Strings for adding the state to the system log:
//  I Would love to figure out a way to just put code in the system log, and 
//  have the parser print this out.  But I don't know where that code even is.
const char     *cmbobStates[] =
{
   
    "Idle",         // 90
    "PreCreStrtd",  // 91
    "Precreated" ,  // 92
    "CmitStrtd",    // 93
    "Committed",    // 94
    "Error",        // 95
};

// Bob state from CM perspective
static emBobState cmBobState;

// Local buffer for assembling the string for the system log:
static char    pCmScratchpad[50]; // Max line length in system log is 40 chars.

emBobState fnSetBobState( emBobState newState )       // (I) event
{
    cmBobState = newState;
    //Log the current state in systemLog:
//    sprintf( pCmScratchpad, "NewCmBobState %s", cmbobStates[ newState - bob_Idle ] );
//    fnDumpStringToSystemLog( pCmScratchpad );
    return( cmBobState );
} // fnSetBobState


emBobState fnGetBobState(void)
{
    return( cmBobState );
} // fnGetBobState


/*****************************************************************************
// FUNCTION NAME:   fnCmGetMsgFunc
//
// DESCRIPTION:     Get the element from PM task message processing function table
//                  Using the msg src and ID as the index
//
// AUTHOR: H.Sun    12/15/02
//
// Returns:         Function pointer for processing the message
//
// ***************************************************************************/
FN_PROCESS_INT_TASK_MSG fnCmGetMsgHandler(
    uchar       bMsgSrc,        // (I)  Incoming message source
    uchar       bMsgId)         // (I)  Incoming message ID     
{
    int     sysHndlTblSize = SYS_MSG_HNDL_TABLE_SIZE;
    int     pmHndlTblSize = PM_MSG_HNDL_TABLE_SIZE;
    int     oitHndlTblSize = OIT_MSG_HNDL_TABLE_SIZE;
    int     bobHndlTblSize = BOB_MSG_HNDL_TABLE_SIZE;
    int     psocHndlTblSize = PSOC_MSG_HNDL_TABLE_SIZE;
    int     fdrHndlTblSize = FDR_MSG_HNDL_TABLE_SIZE;
    
    switch(bMsgSrc)
    {
        case SYS:
             if(bMsgId < 0 || bMsgId >= sysHndlTblSize)
             {
                break;
             }
             else
             {
                return fnSysMsgHandlerTbl[bMsgId];
             }

        case BJCTRL:
        case BJSTATUS: // PM
             bMsgId -= PM_MSGID_OFST;
             if(bMsgId < 0 || bMsgId >= pmHndlTblSize)
             {
                break;
             }
             else
             {
                return fnPmMsgHandlerTbl[bMsgId];
             }

        case OIT: // OIT
             if(bMsgId < 0 || bMsgId >= oitHndlTblSize)
             {
                break;
             }
             else
             {
                return fnOitMsgHandlerTbl[bMsgId];
             }

        case SCM: // BOB
             if(bMsgId < 0 || bMsgId >= bobHndlTblSize)
             {
                break;
             }
             else
             {
                return fnBobMsgHandlerTbl[bMsgId];
             }

        case PSOC: // PSOC
             bMsgId -= PSOC_GEN_SEED_RSP;
             if(bMsgId < 0 || bMsgId >= psocHndlTblSize)
             {
                break;
             }
             else
             {
                return fnPsocMsgHandlerTbl[bMsgId];
             }

        case FDR: // FDRMCPTASK
             bMsgId -= FC_INIT_RSP;
             if(bMsgId < 0 || bMsgId >= fdrHndlTblSize)
             {
                break;
             }
             else
             {
                return fnFdrMsgHandlerTbl[bMsgId];
             }

        case CM:
            return fnCMMsgHandlerTbl[bMsgId];
            
        default:
             // PPP: Error processing
             return fnPrcsUnknownMsg;
    } // end of switch

    return fnPrcsUnknownMsg;

} // end of fnCmGetMsgHandler()


