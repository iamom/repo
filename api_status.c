/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    api_status.c

   DESCRIPTION:    API implementations for status messages

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
 MODIFICATION HISTORY:
 
 2018.02.08 CWB - CSD-Sr
 - Added check for new Disabling condition, DCOND_ACCESSING_PM_NVM, in fnUpdateJsonErrorStatus().

*************************************************************************/
#include "commontypes.h"
#include "cJSON.h"
#include "wsox_tx_queue.h"
#include "sysdatadefines.h"
#include "syspriv.h"
#include "sys.h"
#include "wsox_services.h"
#include "cmpublic.h"
#include "psocutil.h"
#include "cwrapper.h"
#include "hal.h"
#include "HAL_AM335x_I2C.h"
#include "i2c_usb_utilities.h"
#include "cm.h"
#include "version.h"
#include "aiservic.h"
#include "grapfile.h"
#include "grapext.h"
#include "utils.h"
#include "networkmonitor.h"
#include "cfi.h"
#define __define_variables_apphdr_h__
#include "apphdr.h"
#include "btldr_fram.h"

#include "oifpmain.h" //TODO remove from oi

#define __API_STATUS_DEFINE_TABLES__
#include "api_status.h"

#define  CRLF   "\r\n"

/////////////////////////////////////////////////////////////////////////
//TODO JAH get rid of these externs
extern BOOL     fNoInkTank;
extern BOOL     fNoPrintHead;
void fnUpdateDisablingWarningConditions (void);
void fnClearDisablingWarningConditions(void);

// From dummy.c:
extern unsigned long fnReadPMVersion( void );

void GetUICStates(char *state)
{

    UINT8   ucMode;
    UINT8   ucSubMode;
    char rspStrBuff[50];
    size_t rspLen = 0;


    //fnGetOITMode (&ucMode, &ucSubMode);
    fnGetSysMode(&ucMode, &ucSubMode);

    memset(rspStrBuff, 0x0, sizeof(rspStrBuff));
    switch(ucMode){

    case SYS_POWERUP:
        sprintf(rspStrBuff+rspLen,   "Powerup");

    	break;
    case SYS_PRINT_NOT_READY:
        sprintf(rspStrBuff+rspLen,   "NotReady");

    	break;

    case SYS_PRINT_READY:
         sprintf(rspStrBuff+rspLen,   "Ready");

     	break;
    case SYS_RUNNING:
        sprintf(rspStrBuff+rspLen,   "Running");

        break;
    case SYS_SLEEP:
        sprintf(rspStrBuff+rspLen,   "Sleep");

        break;
    case SYS_MANUFACTURING:
        sprintf(rspStrBuff+rspLen,   "Manufacturing");

        	break;
    case SYS_DIAGNOSTICS:
        sprintf(rspStrBuff+rspLen,   "Diagnostics");
        	break;
    case SYS_METER_ERROR:
        sprintf(rspStrBuff+rspLen,   "Error");
         	break;

    case SYS_PRINTING_TAPE:
    	sprintf(rspStrBuff+rspLen,   "TapePrinting");
    	        	break;
    default:
    	//Do nothing for now
    	sprintf(rspStrBuff+rspLen,   "Undefined");
    	break;
    }
    rspLen = strlen(rspStrBuff);
    memcpy(state, rspStrBuff, rspLen + 1);

}

cJSON *CreateWarningItem(CHAR *warnStr)
{
    cJSON *item = cJSON_CreateObject();
    cJSON_AddStringToObject(item, "Warn", warnStr);
    return item;
}


static void fnUpdateJsonWarningStatus( cJSON *warningCondRsp)
{
    if (fnIsWarningStatusSet(WCOND_INSPECTION_DUE) == TRUE)
    {
    	cJSON_AddItemToArray(warningCondRsp, CreateWarningItem("Inspection Due"));
    }

    if (fnIsWarningStatusSet(WCOND_LOW_FUNDS) == TRUE)
    {
    	cJSON_AddItemToArray(warningCondRsp, CreateWarningItem("Low Funds"));
    }

    if (fnIsWarningStatusSet(WCOND_LOW_INK ) == TRUE)
    {
    	cJSON_AddItemToArray(warningCondRsp, CreateWarningItem("Low Ink"));
    }

    if (fnIsWarningStatusSet(WCOND_INKTANK_EXPIRING) == TRUE)
    {
    	cJSON_AddItemToArray(warningCondRsp, CreateWarningItem("Ink Expiring Soon"));
    }

    if (fnIsWarningStatusSet(WCOND_WASTETANK_NEAR_FULL) == TRUE)
    {
    	cJSON_AddItemToArray(warningCondRsp, CreateWarningItem("Waste Tank Near Full"));
    }

    if (fnIsWarningStatusSet(WCOND_LOW_BATTERY_PSD) == TRUE)
    {
    	cJSON_AddItemToArray(warningCondRsp, CreateWarningItem("Low PSD Battery"));
    }

    if (fnIsWarningStatusSet(WCOND_TRANS_LOG_NEAR_FULL) == TRUE)
    {
    	cJSON_AddItemToArray(warningCondRsp, CreateWarningItem("Transaction Log Near Full"));
    }

    if (fnIsWarningStatusSet(WCOND_END_OF_LIFE_WARNING) == TRUE)
    {
    	cJSON_AddItemToArray(warningCondRsp, CreateWarningItem("End Of Life Warning"));
    }

    if (fnIsWarningStatusSet(WCOND_DCAP_UPLOAD_DUE) == TRUE)
	{
		cJSON_AddItemToArray(warningCondRsp, CreateWarningItem("Upload Due"));
	}

    if (fnIsWarningStatusSet(WCOND_TIME_SYNC_NEGATIVE) == TRUE)
	{
		cJSON_AddItemToArray(warningCondRsp, CreateWarningItem("Sync Time With Data Center"));
	}


}


cJSON *CreateDisblingItem(base_event_t base_event)
{
    cJSON *item = cJSON_CreateObject();
    cJSON_AddStringToObject(item, "Disable", base_event_to_string_tbl[base_event]);
    return item;
}


void fnUpdateJsonErrorStatus(cJSON *disableCondRsp)
{

/*    UINT8   ucPrintMode = fnOITGetPrintMode();

    // Check all error conditions, if occur, set the element which has the
    // same index in pErrorsStatus as the field index to TRUE
    if(ucPrintMode == PMODE_MANUAL ||
        ucPrintMode == PMODE_TIME_STAMP ||
        ucPrintMode == PMODE_POSTAGE_CORRECTION  ||
        ucPrintMode == PMODE_DATE_CORRECTION ||
        ucPrintMode == PMODE_MANUAL_WGT_ENTRY)*/
    if(fnCMOSSetupGetCMOSDiagnostics()->ucWhyDoingOOB == TRUE)
    {

        if (fnIsDisabledStatusSet(DCOND_INSUFFICIENT_FUNDS) == TRUE)
        {
        	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_INSUFFICIENT_FUNDS));
        }

        if (fnIsDisabledStatusSet(DCOND_INSPECTION_REQUIRED) == TRUE)
        {
        	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_INSPECTION_REQUIRED));
        }

        if (fnIsDisabledStatusSet(DCOND_AUTHORIZATION_REQ) == TRUE)
        {
        	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_PSD_AUTH_REQ));
        }

        if (fnIsDisabledStatusSet(DCOND2_PSD_DISABLED) == TRUE)
        {   //PSD IS DIABLED BY INFRASTRUCTURE
        	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_PSD_DISABLED));
        }

        if (fnIsDisabledStatusSet(DCOND_OUT_OF_SERVICE) == TRUE)
        {
        	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_PSD_OUT_OF_SERVICE));
        }

        if (fnIsDisabledStatusSet(DCOND2_PSD_MISMATCH) == TRUE)
        {
        	//PSD is married to UIC
        	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_PSD_MISMATCH));
        }

        if (fnIsDisabledStatusSet(DCOND2_PIECE_COUNT_EOL) == TRUE)
        {
        	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_PSD_EOL));
        }

        if (fnIsDisabledStatusSet(DCOND2_TIME_SYNC) == TRUE)

    	{
    		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_DC_TIME_SYNC));
    	}


    }


    if (fnIsDisabledStatusSet(DCOND_INK_OUT) == TRUE)
    {
    	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_INK_OUT));
    }

	// There may (in the future) be other conditions that cause this same generic 
	//  event to be sent to the tablet.	 The tablet doesn't care why it's busy.
    if (fnIsDisabledStatusSet(DCOND_ACCESSING_PM_NVM) == TRUE)
	{
		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_BUSY));
	}

    if (fnIsDisabledStatusSet(DCOND_WASTE_TANK_FULL) == TRUE)
    {
    	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_WASTE_TANK_FULL));
    }

    if (fnIsDisabledStatusSet(DCOND_PAPER_ERROR) == TRUE)
    {
    	//Paper is Jam or Skew
    	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_PAPER_ERROR));
    }

    if (fnIsDisabledStatusSet(DCOND_PAPER_IN_TRANSPORT) == TRUE)
    {
    	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_PAPER_IN_TRANSPORT));
    }

    if (fnIsDisabledStatusSet(DCOND_TRANSPORT_OPEN) == TRUE)
    {
    	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_TRANSPORT_OPEN));
    }

    if (fnIsDisabledStatusSet(DCOND_PRINTER_FAULT) == TRUE)
    {
    	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_PRINTER_FAULT));
    }


    if (fnIsDisabledStatusSet(DCOND_PRINTHEAD_ERROR) == TRUE)
    {
    	if (fNoPrintHead == TRUE)
    		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_NO_PRINT_HEAD));
    	else
    		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_PRINT_HEAD_ERROR));
    }

    if (fnIsDisabledStatusSet(DCOND_INKTANK_ERROR) == TRUE)
    {
    	if (fNoInkTank == TRUE)
    		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_NO_INK_TANK));
    	else
    		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_INK_TANK_ERROR));
    }


    if (fnIsDisabledStatusSet(DCOND_COVER_OPEN) == TRUE)
    {   // top cover open, warning, can be cleared.
    	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_TOP_COVER_OPEN));
    }

    if (fnIsDisabledStatusSet(DCOND2_FEEDER_COVER_OPEN) == TRUE)
    {
    	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_FEEDER_COVER_OPEN));
    }


    if (fnIsDisabledStatusSet(DCOND2_FEEDER_FAULT) == TRUE)
    {
    	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_FEEDER_FAULT));
    }

    if (fnIsDisabledStatusSet(DCOND2_INFRA_UPDATE_REQUIRED) == TRUE)
    {
    	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_INF_UPDATE_REQ));
    }

    if (fnIsDisabledStatusSet(DCOND_DCAP_AUTO_UPLOAD_REQ) == TRUE)
    {
    	cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_UPLOAD_REQ));
    }

    if (fnIsDisabledStatusSet(DCOND_DCAP_MANUAL_UPLOAD_REQ) == TRUE)
	{
		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_UPLOAD_REQ));
	}

    if (fnIsDisabledStatusSet(DCOND_NO_PSD) == TRUE)
	{
		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_NO_PSD));
	}

    if (fnIsDisabledStatusSet(DCOND_UPLOAD_IN_PROGRESS) == TRUE)
   	{
   		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_UPLOAD_PROGRESS));
   	}

    if (fnIsDisabledStatusSet(DCOND_NO_TABLET_CONNECTION) == TRUE)
   	{
   		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_NO_TABLET_CONNECTION));
   	}

    if (fnIsDisabledStatusSet(DCOND_NO_EMD) == TRUE)
	{
		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_EMD_NOT_LOADED));
	}

    if (fnIsDisabledStatusSet(DCOND_NO_GAR) == TRUE)
	{
		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_GAR_NOT_LOADED));
	}

    if (fnIsDisabledStatusSet(DCOND_TRM_FATAL_ERROR) == TRUE)
	{
		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_TRM_FATAL_ERROR));
	}
	if (fnIsDisabledStatusSet(DCOND_MIDNIGHT_OCCUR) == TRUE)
	{
		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_MIDNIGHT));
	}

	if (fnIsDisabledStatusSet(DCOND_FILE_SYS_CORRUPTION) == TRUE)
	{
		cJSON_AddItemToArray(disableCondRsp, CreateDisblingItem(BASE_EVENT_FILE_SYS_CORRUPTION));
	}	

}

static void BaseMessage(cJSON *rspMsg)
{
    char sState[50];
    char sStatus[50];
    cJSON   *warningCond, *disableCond;

    memset(sState, 0x0, sizeof(sState));
    memset(sStatus, 0x0, sizeof(sStatus));

    fnClearDisablingWarningConditions();

    //Set all Disabling and warning conditions
    fnUpdateDisablingWarningConditions();

    //Get Base state
    GetUICStates(sState);
    cJSON_AddStringToObject(rspMsg, "BaseState", sState );

//    cJSON_AddStringToObject(rspMsg, "ErrorCode", pLogBuf );

    //Get Warning conditions
    cJSON_AddItemToObject(rspMsg, "Warning", warningCond = cJSON_CreateArray());
    fnUpdateJsonWarningStatus(warningCond);

    //Get Disable conditions
    cJSON_AddItemToObject(rspMsg, "Disabling", disableCond = cJSON_CreateArray());
    fnUpdateJsonErrorStatus(disableCond);

    //Add packaged mail pieced
    cJSON_AddNumberToObject(rspMsg, "MailPiecesPackaged", CMOSTrmMailPiecesPackaged);

    //Add Mail pieced to be uploaded
    cJSON_AddNumberToObject(rspMsg, "MailPiecesToUpload", CMOSTrmMailPiecesToUpload);
}


void on_GetBaseStatusReq(UINT32 handle, cJSON *root)
{

    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetBaseStatusRsp");

    BaseMessage(rspMsg);

    addEntryToTxQueue(&entry, root, "  GetBaseStatusReq: Added GetBaseStatusRsp to tx queue. status=%d" CRLF);
}


cJSON *CreateGraphicsItem(UINT8 graphType, UINT16 graphId, UINT8 visibility, char *pName, char* pDataNameRev, char* pUrl)
{
	UINT8 autoSelect = 0;
	UINT8 permanent = 0;
	
	if((visibility & GRFXFLG_AUTOGRFX) == GRFXFLG_AUTOGRFX)
	{
		autoSelect = 1;
	}
	
	if((visibility & GRFXFLG_PERMANENT) == GRFXFLG_PERMANENT)
	{
		permanent = 1;
	}

    cJSON *item = cJSON_CreateObject();
    cJSON_AddNumberToObject(item,        "Type", graphType);
    cJSON_AddNumberToObject(item,          "ID", graphId);
    cJSON_AddNumberToObject(item,  "AutoSelect", autoSelect);
    cJSON_AddNumberToObject(item,   "Permanent", permanent);
    cJSON_AddStringToObject(item,        "Name", pName);
    cJSON_AddStringToObject(item, "DataNameRev", pDataNameRev);
    cJSON_AddStringToObject(item,         "URL", pUrl);
    return item;
}


void on_GetGraphicsListReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();
    cJSON *graphics; //, *ads, *inscriptions;
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
	UINT16 curIDList[ MAX_GRAPHIC_COMPONENTS ];
	UINT8  curTypeList[ MAX_GRAPHIC_COMPONENTS ];
	UINT16 numIDs = 0;
	UINT8 *pLinkPtr = NULL;
	char name[96];
	char dataNameRev[25];
	INT32 i;
	unsigned char* pUni;
	UINT32 uniLen;
	GFENT *   pDirEntry;
	char url[256];
    char addr_str[IP_ADDR_STR_LEN];

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetGraphicsListRsp");

	memset( curIDList, 0, sizeof(curIDList) );
	memset( curTypeList, 0, sizeof(curTypeList) );
	memset( dataNameRev, 0, sizeof(dataNameRev) );
 
    numIDs = fnGFGetGraphicsListAllTypes( curTypeList, curIDList, MAX_GRAPHIC_COMPONENTS, GRFX_ANY_FLAGS );

    cJSON_AddItemToObject(rspMsg, "Graphics", graphics = cJSON_CreateArray());

	for( i = 0; i < numIDs; i++)
	{
		pDirEntry = fnGFGetDirPtrByID( curTypeList[i], curIDList[i], GRFX_ANY_FLAGS );
		if(pDirEntry != NULL)
		{
			pLinkPtr = fnGFGetLnkPtrByDirPtr( pDirEntry );
			if(pLinkPtr != NULL)
			{
				
				pUni = fnGrfxGetUniNamePtr(pLinkPtr, 0);
				if(pUni != NULL)
				{
					uniLen = fnUnicodeLen(pUni);
					fnPackedBcdToAsciiBcd(name, pUni, uniLen*2);
				}
				else
				{
					memset(name, 0x30, 4);
					uniLen = 1;
				}
				name[uniLen*4] = 0;
				name[(uniLen*4)+1] = 0;

				(void) fnGrfxMakeDataNameVerIntoAscii(pDirEntry->gfDataName, GRFXDATANAMEANDVERSION, dataNameRev );

                #ifdef USE_HTTP_URI
				    sprintf(url, "http://%s/ms/graphics/%s.png",
				            get_local_ip_addr_str_from_wsox_handle(handle, addr_str, "192.168.10.246"), name);
                #else
				    sprintf(url, "https://base.sendpro.pb.com/ms/graphics/%s.png", name);
                #endif

				cJSON_AddItemToArray(graphics, CreateGraphicsItem(curTypeList[i], curIDList[i], pDirEntry->gfFlags, name, dataNameRev, url ));
			}
		}
 	}

	addEntryToTxQueue(&entry, root, "  GetGraphicsListReq: Added GetGraphicsListRsp to tx queue. status=%d" CRLF);
}


void on_GetDateTimeReq(UINT32 handle, cJSON *root)
{
	char sLocalDateTime[50]= {0}, sGMTDateTime[50]={0};
    cJSON *rspMsg = cJSON_CreateObject();

	WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetDateTimeRsp");

    fnGetGMT_LOCAL_DateTime(sGMTDateTime, sLocalDateTime);

    cJSON_AddStringToObject(rspMsg, "Local", sLocalDateTime);
    cJSON_AddStringToObject(rspMsg, "GMT", sGMTDateTime);

    addEntryToTxQueue(&entry, root, "  GetDateTimeReq: Added GetDateTimeRsp to tx queue. status=%d" CRLF);
}


void SendBaseEventToTablet(base_event_t base_event, cJSON *eventData)
{

	//NOTE: We are only want to update the printer and not notify the tablet for further processing of Paper JAM and SKew
/*
    if(base_event == BASE_EVENT_PAPER_CLEARED){

    	fnClearDisablingWarningConditions();
    	//Set all Disabling and warning conditions
    	fnUpdateDisablingWarningConditions();
    }
    else
*/
    {
        cJSON *rspMsg = cJSON_CreateObject();
        cJSON_AddStringToObject(rspMsg, "MsgID", "BaseEvent");
        cJSON_AddStringToObject(rspMsg, "Event", base_event_to_string_tbl[base_event] );

    	BaseMessage(rspMsg);

		if (eventData != NULL)
		{

			// RD Protect against a NULL value string and while we are at it check the eventData child object....
			if( eventData->child && eventData->child->valuestring && (strcmp(eventData->child->string, "Error") == 0))
			{

				cJSON_AddStringToObject(rspMsg, "ErrorCode", eventData->child->valuestring);
				cJSON_Delete(eventData);    /* since ownership of eventData doesn't transfer to rspMsg in this case */
			}
			else
				cJSON_AddItemToObject(rspMsg, "EventData", eventData);
		}

		Send_JSON(rspMsg,BROADCAST_MASK);
    }
	dbgTrace(DBG_LVL_INFO, "BaseEvent: %s" CRLF , base_event_to_string_tbl[base_event] );
}


extern const char UicVersionString[];


void on_GetBaseVersionsReq(UINT32 handle, cJSON *root)
{
	char *pTemp, *pTempPH;
	char osVer[20];
    CMOS_Signature *sig;
    int status;
    char strTemp[40];
    char strPHSerialNumber[16] = {0};
    unsigned char strVerStr[PRNTR_SN_LEN+1] = {0};
    long lPhSn;
    char boot_loader_version[40] = {0};
    long PMSoftwareVer = 0;
    char strPHSoftwareVer[10] = {0};
    char sEMDVer[10] = {0};
    char flash_id[80];

	cJSON *rspMsg = cJSON_CreateObject();

	WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetBaseVersionsRsp");

    sig = fnCMOSSetupGetCMOSSignature();

    //Get Base PCN
    cJSON_AddStringToObject(rspMsg, "BasePCN", sig->bUicPcn);

    //Get Base serial number
    cJSON_AddStringToObject(rspMsg, "BaseSerialNum", sig->bUicSN);


    // Ram updated to include Base serial number and eth0 MAC address.
    MACBuf tempMAC;
    tempMAC.u64Val = 0;
    getSitaraMACAddress(MAC_ADDSOFFSET_ETH0, &tempMAC);
    snprintf(strTemp, 20,"%08X%04X", tempMAC.u32Val, tempMAC.u16LVal);
    cJSON_AddStringToObject(rspMsg, "Ethernet MAC", strTemp);


    //Nucleus OS version
    memset(osVer, 0x0, sizeof(osVer));
    sprintf(osVer, "%d.%02d.%02d", NUCLEUS_RELEASE_MAJOR_VERSION, NUCLEUS_RELEASE_MINOR_VERSION, NUCLEUS_RELEASE_PATCH_VERSION);
    cJSON_AddStringToObject(rspMsg, "OS", osVer);
    cJSON_AddStringToObject(rspMsg, "OSBuildRefName", OS_BUILD_REF_NAME);
    cJSON_AddStringToObject(rspMsg, "OSGitCommitSHA", OS_COMMIT_SHA);

    //Base Software version
    cJSON_AddStringToObject(rspMsg, "BaseSW", UicVersionString);
    cJSON_AddStringToObject(rspMsg, "BaseSWLong", CSD_VERSION_STRING_FULL);

    //build details
    cJSON_AddStringToObject(rspMsg, "BuildTimestamp", BUILD_DATE_TIME);
    cJSON_AddStringToObject(rspMsg, "BuildMachine", BUILD_MACHINE);
    cJSON_AddStringToObject(rspMsg, "BuildUsername", BUILD_USERNAME);
    cJSON_AddStringToObject(rspMsg, "BuildRefName", BUILD_REF_NAME);
    cJSON_AddStringToObject(rspMsg, "GitCommitSHA", COMMIT_SHA);

    //Base Hardware version
    cJSON_AddNumberToObject(rspMsg, "HW Version", BoardInfo.HW_Version);
    cJSON_AddStringToObject(rspMsg, "HW Type", fnGetMeterModel() == CSD2 ? "CSD2" : "CSD3");

    sprintf(boot_loader_version, "%02.2d.%02.2d %s", BoardInfo.Bootloader_Ver_Major, 
                                                     BoardInfo.Bootloader_Ver_Minor, 
                                                     bldr_info_build_type_to_str(strTemp));
    cJSON_AddStringToObject(rspMsg, "BootLoaderVersion", boot_loader_version);

    cJSON_AddStringToObject(rspMsg, "CFI Library Version", cfi_get_lib_version());
    sprintf(flash_id, "Manf-Id=0x%X DevId-0=0x%X DevId-1=0x%X DevId-2=0x%X", 
            cfi.manufacturer_id, cfi.device_id[0], cfi.device_id[1], cfi.device_id[2]);
    cJSON_AddStringToObject(rspMsg, "Flash ID" , flash_id);

    //Read MCB PSOC version
    pTemp = fnReadPSOCVersion(MCB_PSOC);
    cJSON_AddStringToObject(rspMsg, "MCB PSOC", pTemp);

    //Read PH PSOC version
    pTempPH = fnReadPSOCVersion(PH_PSOC);
    cJSON_AddStringToObject(rspMsg, "PH PSOC", pTempPH);

    memset(strTemp, 0x0, sizeof(strTemp));
    status = getPDFirmwareVersion(strTemp, 20);
    cJSON_AddStringToObject(rspMsg, "USB PD FW", strTemp);

    lPhSn = fnGetPrintHeadSerialNumber();
    sprintf(strPHSerialNumber, "%08lx", lPhSn);
    //TODO
    cJSON_AddStringToObject(rspMsg, "Print Head Number",strPHSerialNumber);

    fnGetPrintSerialNumber((uchar *)strVerStr);
    cJSON_AddStringToObject(rspMsg, "Printer Serial Number",strVerStr);

    PMSoftwareVer = fnReadPMVersion();
    sprintf(strPHSoftwareVer, "%x", (unsigned int)PMSoftwareVer);
    cJSON_AddStringToObject(rspMsg, "Print Manager",strPHSoftwareVer);


    UINT16 uEMDVer = fnGetEMDVersion();
    sprintf(sEMDVer, "%x", uEMDVer);
    cJSON_AddStringToObject(rspMsg, "EMD",sEMDVer);

    addEntryToTxQueue(&entry, root, "  GetBaseVersionsReq: Added GetBaseVersionsRsp to tx queue. status=%d" CRLF);

    UNUSED_PARAMETER(status);
}


void on_DeleteGraphicReq(UINT32 handle, cJSON *root)
{
	UINT8   gfName[ GRFX_UNINAME_MAX_COMPARE_LEN ];
	short len;
	GFENT *pDirEntry;
	UINT16 dirIndex = 0;
	UINT8 *pLinkPtr;
	BOOL passed = FALSE;
	int  ec = 0;

    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    cJSON_AddStringToObject(rspMsg, "MsgID", "DeleteGraphicRsp");

	cJSON *dataName = cJSON_GetObjectItem(root, "Graphic Name");
    if (dataName)
    {
		passed = FALSE;
		len = fnAsciiBcdToPackedBcd(gfName, sizeof(gfName), dataName->valuestring, TRUE);
		if(len > 0)
		{
			gfName[len] = 0;
			gfName[len+1] = 0;

			pLinkPtr = fnGFGetDirInxByUniName(&dirIndex, ANY_COMPONENT_TYPE, (UNICHAR *)gfName, len, GRFX_ENABLED_ONLY);
			if(pLinkPtr != NULL)
			{
				pDirEntry = fnGFGetDirPtrByDirInx(dirIndex);
				if(pDirEntry != NULL)
				{
					if((pDirEntry->gfFlags & GRFXFLG_PERMANENT) != GRFXFLG_PERMANENT)
					{
						ec = fnGFDeleteByDirInx(dirIndex, TRUE);
						if(ec == 0)
						{
							passed = TRUE;
						}
					}
				}
			}
		}
	}

	if(passed == FALSE)
	{
		cJSON_AddStringToObject(rspMsg, "Graphic Name", dataName->valuestring);
		cJSON_AddStringToObject(rspMsg, "Status", "Not Accepted");
	}
	else
	{
		cJSON_AddStringToObject(rspMsg, "Status", "Accepted");
	}

    addEntryToTxQueue(&entry, root, "  DeleteGraphicReq: Added DeleteGraphicRsp to tx queue. status=%d" CRLF);
}

void on_InstallGraphicReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    cJSON_AddStringToObject(rspMsg, "MsgID", "InstallGraphicRsp");

    fnInstallGraphics();

	cJSON_AddStringToObject(rspMsg, "Status", "Accepted");

    addEntryToTxQueue(&entry, root, "  InstallGraphicReq: Added InstallGraphicRsp to tx queue. status=%d" CRLF);
}



