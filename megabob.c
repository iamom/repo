/* TAB SETTING = 4 (e.g. 5, 9, 13, 17...)   */
/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    megabob.c 
   
   DESCRIPTION:    
       Intertask interface between application and local devices
       (phc, psd, card slot).  This file was originally version 1.6 of 
       the Spark bobTask.c file.

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
CHANGE HISTORY:

 2018.07.07 CWB 
 - Modified fnDeviceMEssage() to store error flags in syslog when an error is 
   returned from the device, and to add an entry in the syslog if there is a 
   bob error.  Added local scratch buffer for creating these messages. 
 - Added a new flag, fBobUberDebugDevMessage, that can be used by the functions 
   in cbobpriv.c to turn on detailed debugging for specific messages.  The flag
   is set in the pre-function and cleared in the post-function of the message 
   that we want the details for.
 - Modified fnDeviceMessage() to add some additional message details to the syslog 
   if the fBobUberDebugDevMessage flag is set. 

*************************************************************************/


// #define  COMET_PHC_NOT_PRESENT   1   // do not define this for JANUS

/* #define  COMET_PSD_NOT_PRESENT   1   */
#define SPARK_VLT_NOT_PRESENT   1
#define EARLY_COMET 1

#include <stdio.h>
#include <string.h>
#include "global.h"
#include "ossetup.h"
#include "pbos.h"
#include "bob.h"
#include "tealeavs.h"
#include "junior.h"
#include "bobutils.h"
#include "clock.h"
#include "fwrapper.h"
#include "fstub.h"
#include "datdict.h"
#include "cmos.h"
#include "commontypes.h"
#include "errcode.h"
#include "oit.h"
#include <string.h>
#include "sys.h"
#include "cm.h"
#include "cjunior.h"
#include "cmerr.h"
#include "ipsd.h"
#include "bwrapper.h"
#include "utils.h"

// ----------------------------------------------------------------------
//              Declaration of External variables:
// ----------------------------------------------------------------------

//#define PSD_FLYING_ALLOWED    1
BOOL    getFlightRecord;

extern struct   systemDeviceStates rPowerUpStatus;

extern  unsigned char    bInMfgMode;


// Message Statuses 
extern  uchar   phStatus[2];                                // status of last print head message            


// ----------------------------------------------------------------------
//              Protypes for External functions :
// ----------------------------------------------------------------------

extern void     fnInitTeaLeaves();


// ----------------------------------------------------------------------
//              Declaration of Global variables:
// ----------------------------------------------------------------------
// DEBUG !!!!
// This is a flag used to turn on targetted uber-debugging.
//  If you want very detailed info in the syslog on handling of a particular 
//  message, then set and clear this flag in cbobpriv.c.
//  In this file, use it to turn on putting extra messages in the syslog.
BOOL	fBobUberDebugDevMessage = FALSE;
// !!!!


// ----------------------------------------------------------------------
//              Declaration of Local variables:
// ----------------------------------------------------------------------

// For debugging.. \/.. CB
ushort  wCallingScriptID = 0;
ushort  wCallingScriptIdx = 0;
ushort  wNowScriptID = 0;
ushort  wNowScriptIdx = 0;

#define MAX_IPSD_MSG_HSTY_IDX   0x80
typedef struct 
{
    ushort  wMsgId;         // ID of IPSD message.
    uchar   bITaskMsgId;    // ID of intertask message.
    uchar   bITaskSrc;      // ID of task that sent the message.
    ushort  wScriptID;      // ID of script which sends the message.
    ushort  wScriptIdx;     // Index of script line which sends the message.
    ushort  wParentScrID;   // ID of calling script.
    ushort  wParentScrIdx;  // Index of line in calling script.
    ushort  wMsgData;       // Mem Access ID or Perform Script ID
    ushort  pad;
} tIPSD_MSG_HISTORY_LMNT;

tIPSD_MSG_HISTORY_LMNT pIpsdMsgHistory[ MAX_IPSD_MSG_HSTY_IDX +1 ];
ushort wIpsdMsgHistoryIdx = 0;
void fnUpdateIpsdMsgHistory( bMsgId )
{
    tIPSD_MSG_HISTORY_LMNT * pHistoryElement;

    pHistoryElement = &pIpsdMsgHistory[ wIpsdMsgHistoryIdx ];
    pHistoryElement->wMsgId = bMsgId;
    pHistoryElement->bITaskMsgId = bobsCommand.bMsgId;
    pHistoryElement->bITaskSrc = bobsCommand.bSource;
    pHistoryElement->wScriptID = wNowScriptID;
    pHistoryElement->wScriptIdx = wNowScriptIdx;
    pHistoryElement->wParentScrID = wCallingScriptID;
    pHistoryElement->wParentScrIdx = wCallingScriptIdx;
    pHistoryElement->wMsgData = 0;
    pHistoryElement->pad = 1;
    if(   (bobsCommand.bMsgType == MEM_ACCESS)
       || (bobsCommand.bMsgType == PERFORM_SCRIPT) )    
    {
        pHistoryElement->bITaskMsgId = bobsCommand.bMsgType;
        pHistoryElement->wMsgData = bobsCommand.IntertaskUnion.wShortData[0];
    }
    

    // Adjust for next call... and clear old data.
    if( ++wIpsdMsgHistoryIdx > MAX_IPSD_MSG_HSTY_IDX )
        wIpsdMsgHistoryIdx = 0;
    pIpsdMsgHistory[ wIpsdMsgHistoryIdx ].pad = 0x8888;
    return;
}

// For debugging.. (CB)
uchar ucErrorFlags = 0;


BOOL    freezeErrorHandling;
BOOL    fBobReplied;

// Local functions
static void fnSetCMReplyStatus(uchar ucMsgId);

// Convert Destination ID to a Device name 
const char *pDefaultDevName = "Unknown"; 
typedef struct 
{
	 int		destID;
	 const char	*devName;	
} tDEV_NAME_TABLE;

static const  tDEV_NAME_TABLE  sDeviceNameTable[] =
{
 { 	IPSD, 		"IPSD" },
 { 	IPSD_BL, 	"IPSD_BL" },
};


// For Handling Special Device reply Lengths    


#ifdef  DEBUG_TRACE
#define TRACE_SIZE          42000
#define TRACE_MARGIN        39000
#define INTER_TRACE_SIZE    4000
#define INTER_TRACE_MARG    3968
    ushort  traceOff;
    char    *tracer;
    uchar   traceBuffer[TRACE_SIZE];
    ushort  interTraceOff;
    char    *interTracer;
    uchar   interTraceBuf[INTER_TRACE_SIZE];
    uchar   *whereIsTrace;

//extern    ushort  phcTraceOff;
//extern    uchar   phcTraceBuffer[];


#endif

// For putting things into the syslog:
char        pLocalScratchPad150[ 150 ];


#define BUTILS_EVENT2_GROUP         BUTILS_EVENT_GROUP

T_BOB_EVENT_MASK eventMaskTable[ MAX_TASKS_HANDLED_BY_BOB ] = {
{   0x00000001, 0x00000002, BUTILS1_EVENT_GROUP  },
{   0x00000004, 0x00000008, BUTILS1_EVENT_GROUP  },
{   0x00000010, 0x00000020, BUTILS1_EVENT_GROUP  },
{   0x00000040, 0x00000080, BUTILS1_EVENT_GROUP  },
{   0x00000100, 0x00000200, BUTILS1_EVENT_GROUP  },
{   0x00000400, 0x00000800, BUTILS1_EVENT_GROUP  },
{   0x00001000, 0x00002000, BUTILS1_EVENT_GROUP  },
{   0x00004000, 0x00008000, BUTILS1_EVENT_GROUP  },
{   0x00010000, 0x00020000, BUTILS1_EVENT_GROUP  },
{   0x00040000, 0x00080000, BUTILS1_EVENT_GROUP  },
{   0x00100000, 0x00200000, BUTILS1_EVENT_GROUP  },
{   0x00400000, 0x00800000, BUTILS1_EVENT_GROUP  },
{   0x01000000, 0x02000000, BUTILS1_EVENT_GROUP  },
{   0x04000000, 0x08000000, BUTILS1_EVENT_GROUP  },
{   0x10000000, 0x20000000, BUTILS1_EVENT_GROUP  },
{   0x40000000, 0x80000000, BUTILS1_EVENT_GROUP  },

{   0x00000001, 0x00000002, BUTILS2_EVENT_GROUP },
{   0x00000004, 0x00000008, BUTILS2_EVENT_GROUP },
{   0x00000010, 0x00000020, BUTILS2_EVENT_GROUP },
{   0x00000040, 0x00000080, BUTILS2_EVENT_GROUP },
{   0x00000100, 0x00000200, BUTILS2_EVENT_GROUP },
{   0x00000400, 0x00000800, BUTILS2_EVENT_GROUP },
{   0x00001000, 0x00002000, BUTILS2_EVENT_GROUP },
{   0x00004000, 0x00008000, BUTILS2_EVENT_GROUP },
{   0x00010000, 0x00020000, BUTILS2_EVENT_GROUP },
{   0x00040000, 0x00080000, BUTILS2_EVENT_GROUP },
{   0x00100000, 0x00200000, BUTILS2_EVENT_GROUP },
{   0x00400000, 0x00800000, BUTILS2_EVENT_GROUP },
{   0x01000000, 0x02000000, BUTILS2_EVENT_GROUP },
{   0x04000000, 0x08000000, BUTILS2_EVENT_GROUP },
{   0x10000000, 0x20000000, BUTILS2_EVENT_GROUP },
{   0x40000000, 0x80000000, BUTILS2_EVENT_GROUP },

{   0x00000001, 0x00000002, BUTILS3_EVENT_GROUP },
{   0x00000004, 0x00000008, BUTILS3_EVENT_GROUP },
{   0x00000010, 0x00000020, BUTILS3_EVENT_GROUP },
{   0x00000040, 0x00000080, BUTILS3_EVENT_GROUP },
{   0x00000100, 0x00000200, BUTILS3_EVENT_GROUP },
{   0x00000400, 0x00000800, BUTILS3_EVENT_GROUP },
{   0x00001000, 0x00002000, BUTILS3_EVENT_GROUP },
{   0x00004000, 0x00008000, BUTILS3_EVENT_GROUP },
{   0x00010000, 0x00020000, BUTILS3_EVENT_GROUP },
{   0x00040000, 0x00080000, BUTILS3_EVENT_GROUP },
{   0x00100000, 0x00200000, BUTILS3_EVENT_GROUP },
{   0x00400000, 0x00800000, BUTILS3_EVENT_GROUP },
{   0x01000000, 0x02000000, BUTILS3_EVENT_GROUP },
{   0x04000000, 0x08000000, BUTILS3_EVENT_GROUP },
{   0x10000000, 0x20000000, BUTILS3_EVENT_GROUP },
{   0x40000000, 0x80000000, BUTILS3_EVENT_GROUP },
};


T_BOB_EVENT_MASK eventMaskTableNonTask[ MAX_NONTASKS_HANDLED_BY_BOB ] = 
{
    {   0x00000001, 0x00000002, BUTILSX_EVENT_GROUP  },
    {   0x00000004, 0x00000008, BUTILSX_EVENT_GROUP  },
    {   0x00000010, 0x00000020, BUTILSX_EVENT_GROUP  },
    {   0x00000040, 0x00000080, BUTILSX_EVENT_GROUP  },
    {   0x00000100, 0x00000200, BUTILSX_EVENT_GROUP  },
    {   0x00000400, 0x00000800, BUTILSX_EVENT_GROUP  },
    {   0x00001000, 0x00002000, BUTILSX_EVENT_GROUP  },
    {   0x00004000, 0x00008000, BUTILSX_EVENT_GROUP  },
    {   0x00010000, 0x00020000, BUTILSX_EVENT_GROUP  },
    {   0x00040000, 0x00080000, BUTILSX_EVENT_GROUP  },
    // I don't think we need more than 10 for now.
    // The last one is not availabe because it is used for the BOB_INACTIVE event flag.
};

UINT16 uwFlagsNonTaskEventsAvailable    = 0xFFFF;



/*************************************************************************

 FUNCTION NAME:     void fnGetEventMask( uchar caller,  T_BOB_EVENT_MASK *pDestMasks )

 PURPOSE:           
      Using the caller as the index into the eventMaskTable, copies the event-mask 
    struct from the table to the destination.  The structure contains an event 
    mask for success, one for failure, and an event ID.  Each bit is only 
    used for one caller (task).  The caller will wait for one of their own masked 
    bits to be set.

 INPUTS:  
   caller - Task ID of the task that is making the request.
   pDestMasks - Pointer to a structure where the info will be copied by this function.
                    
 AUTHOR:            R. Arsenault
************************************************************************/
BOOL fnGetEventMask( uchar caller, T_BOB_EVENT_MASK *pDestMasks, UINT16 *pEventPairID )
{
    BOOL    retval;

    if( caller < MAX_TASKS_HANDLED_BY_BOB ) 
    {
        *pDestMasks = eventMaskTable[ caller ];
        *pEventPairID = 0;
        retval = BOB_OK;
    }
    else
    {
        retval = BOB_SICK;
        //retval = fnGetNonTaskEventMask( pDestMasks, pEventPairID );
    }    
    return( retval );
}


//****************************************************************************
// FUNCTION NAME:   fnGetNonTaskEventMask
// DESCRIPTION:   
//      This function copies an event-mask struct from the eventMaskTableNonTask
//      table to the destination.  The structure contains an event mask for success, 
//      one for failure, and an event group ID.
//      If the contents of pEventPairID is 0, then the function should look for 
//      the first available entry and assign it.  If the contents are a valid
//      non-task eventPairID, then it should simply retrieve the corresponding
//      entry.  If neither is true (a non-zero value, out of range) then the 
//      function returns BOB_SICK.
//       
// ARGUMENTS:   
//      pDestMasks - Pointer For now, this is equal to the Flag that will be set in
// RETURN:   
//      TRUE if an available Event Pair was found.
// OUTPUTS:
//      pDestMask - Structure which is loaded with the Event Group and the good and 
//                  bad event masks by this function.
//      pEventPairID - If this is zero, it is loaded with the Availability flag 
//                      of the pair that is assigned.  If it is non-zero, it is 
//                      used to indicate which entry to retrieve.
//
// NOTES:
//  1.  If a non-task calls fnValidData() or fnWriteData(), and that function 
//      then calls fnScheduleBob(), then an event pair is assigned from bob's
//      non-task event pair pool.  When it is used, its availability flag is
//      cleared.  When it is done being used, the availability flag is cleared.
//  2.  The EventPairID for an entry in the table, IS the availability flag mask
//      for that entry. 
//-----------------------------------------------------------------------------
BOOL fnGetNonTaskEventMask( T_BOB_EVENT_MASK *pDestMasks, UINT16 *pEventPairID )
{
    BOOL    retval = BOB_SICK;
    UINT8   ubIndex;        // The index into uwFlagsNonTaskEventsAvailable
    UINT16  uwTestFlags;    // A copy of uwFlagsNonTaskEventsAvailable that can be shifted for bit testing.
    UINT16  uwFoundFlag = 0;

    uwTestFlags = *pEventPairID;
    // If the EventPairID is zero, then we want to ASSIGN an available event pair.
    if( *pEventPairID == 0 )
    {
        // The first bit set in the Available mask with be the first available mask pair ID:
        uwTestFlags = uwFlagsNonTaskEventsAvailable;
    }

    // Look for the first set bit in the uwTestFlags, and adjust the ubIndex accordingly.
    ubIndex = 0;
    while(   ((uwTestFlags & 0x01) != 0)
          && (ubIndex < MAX_NONTASKS_HANDLED_BY_BOB) )
    {
        ubIndex++;
        uwTestFlags >>= 1;
    }
 
    // See if we found a set bit.
    if( ubIndex < MAX_NONTASKS_HANDLED_BY_BOB )
    {
        // Retrieve the data for the found pair:
        *pDestMasks = eventMaskTableNonTask[ ubIndex ];

        // If we are assigning this pair now...
        if( *pEventPairID == 0 )
        {
            // Set the found flag to the availability flag mask.  This is the EventPairID.
            uwFoundFlag = 0x01 << ubIndex;
            *pEventPairID = uwFoundFlag;
            // Clear the available flag for this pair:
            uwFlagsNonTaskEventsAvailable &= ~( uwFoundFlag );
        }
        retval = BOB_OK;
    }
    else
    {
        // No flags set in range:
        retval = BOB_SICK;
    }
 
    return( retval );
    
}

//****************************************************************************
// FUNCTION NAME:   fnFreeNonTaskEvenMask
// DESCRIPTION:   
//      This function clears the availability flag.
// ARGUMENTS:   
//      uwEventPairID - For now, this is equal to the Flag that will be set in
//                      uwFlagsNonTaskEventsAvailable.  If it is zero, then 
//                      nothing needs to be done.
// RETURN:   
//      None.
//
// NOTES:
//  1.  If a non-task calls fnValidData() or fnWriteData(), and that function 
//      then calls fnScheduleBob(), then an event pair is assigned from bob's
//      non-task event pair pool.  When it is used, its availability flag is
//      cleared.  When it is done being used, the availability flag needs to 
//      be set so that it can be used again in the future.
//-----------------------------------------------------------------------------
void fnFreeNonTaskEvenMask( UINT16 uwEventPairID )
{
    UINT16  uwFlag = uwEventPairID;
    
    uwFlagsNonTaskEventsAvailable |= uwFlag;
    return; 
}

/*************************************************************************
    opCode tables for fnAllowedPsdPhcMoods
    Note: make sure there are entries for each case in  fnAllowedPsdPhcMoods
        and if new case are added, update all tables
************************************************************************/

const char aPsdMoodStatForOp[PSD_OP_MAX][PSD_MOOD_CNT] = {
    {//PSD_OP_VERIFY_GRAPHICS OP
    {FALSE},    //0x0010:    Manufacturing
    {FALSE},    //0x0011:    Manufacturing - Challenge Provided
    {TRUE},     //0x0020:    Serial Number Lock - Full Postal 
    {FALSE},    //0x0021:    Serial Number Lock - Awaiting Authorization        <<< note: AWAITING AUTHORIZATION
    {FALSE},    //0x0022:    Serial Number Lock - Challenge Provided
    {FALSE},    //0x0023:    Serial Number Lock - Awaiting Postage Value Download
    {FALSE},    //0x0030:    Inspection Lockout
    {FALSE},    //0x0031:    Inspection Lockout  - Awaiting Postage Value Download
    {FALSE},    //0x0040:    Key Lockout
    {TRUE},     //0x0050:    Ascending Register Lockout
    {TRUE},     //0x0060:    Refill Failure Lockout
    {FALSE},    //0x0070:    Out of Service Lockout
    {FALSE},    //0x0080:    Withdrawal From Service
    {FALSE},    //0x0081:    Withdrawal From Service - Challenge Provided
    {FALSE},    //0x0090:    End of Life
    {FALSE},    //0x00A0:    Disabled From Data Center - Challenge Provided
    {FALSE},    //0x00A1:    Disabled From Data Center
    {FALSE},    //0x00F0:    Hard Fatal
    {FALSE},    //0x00F1:    Soft Fatal
    }
};


/***********************************************************************

 FUNCTION NAME:     void fnBobInterfaceTask (unsigned long argc, void *argv)

 PURPOSE:           Waits for incoming intertask command messages.
                    Instigates performance in response to the message.
                    Updates any system globals effected by the command.
                    Instigates a reply to the commanding task, describe the
                    result of performing the command.
                    
 AUTHOR:            R. Arsenault

 INPUTS:            Standard main() arguments... I'm not yet sure why.
 ************************************************************************/
void fnBobInterfaceTask( unsigned long argc, void *argv ) 
{
    fnSysLogTaskStart( "BobTask", SCM );  

#ifdef DEBUG_TRACE
    traceOff = 0;
    memset(traceBuffer, 0, TRACE_SIZE);
    interTraceOff = 0;
    memset(interTraceBuf, 0, INTER_TRACE_SIZE);
#endif

#ifndef ORBIT_H
    fnInitTeaLeaves();
#endif

    ErrClassFrombob = 0;
    ErrCodeFrombob = 0;
    extCardStatus[0] = 0;                                       //
    extCardStatus[1] = 0;                                       //
    extCardState = XCARD_DEVICE_NOT_THERE;                      //

    fnSysLogTaskLoop( "BobTask", SCM );  
    while( 1 ) 
    {
        // Until a message is received, mark that bob is inactive.
        OSSetEvents( BUTILSX_EVENT_GROUP, BOB_INACTIVE, OS_OR );
//        fnDumpStringToSystemLog( "BobInactive" );
          
        // Wait for an intertask message.
        if( OSReceiveIntertask (SCM, &bobsCommand, OS_SUSPEND) == SUCCESSFUL ) 
        { 
            // When we recieve a message, first mark bob as ACTIVE.
            OSSetEvents( BUTILSX_EVENT_GROUP, BOB_ACTIVE, OS_AND );
//            fnDumpStringToSystemLog( "BobActive" );
            // Clear the Replied flag, it will be set when this message is replied to.
            fBobReplied = FALSE;

#ifdef  DEBUG_TRACE
            if (interTraceOff > INTER_TRACE_MARG) 
                interTraceOff = 0;
            memset(&interTraceBuf[interTraceOff], 0, 16);
            if( (bobsCommand.bMsgType == MEM_ACCESS) || (bobsCommand.bMsgType == PERFORM_SCRIPT) ) 
            {
                memcpy(&interTraceBuf[interTraceOff], &bobsCommand.IntertaskUnion.bByteData[0], 2);
            }
            else 
                interTraceBuf[interTraceOff] = bobsCommand.bMsgId;                 // copy the intertask command to the inter-trace line
            whereIsTrace = &traceBuffer[traceOff];
            memcpy( &interTraceBuf[interTraceOff+3], &whereIsTrace, 4 );    // copy the starting tracebuffer address to the inter-trace line

            interTraceBuf[interTraceOff+15] = bobsCommand.bMsgType;
            interTraceOff += 16;
            memset(&interTraceBuf[interTraceOff], 0xFF, 16);
            interTracer = (char *)(&interTraceBuf[interTraceOff]);
#endif
            freezeErrorHandling = FALSE;

            fnBobInterTaskHandler();

            //fnAllowError(0, BOB_OK);      // clear all devices error status containers

            if( bobsCommand.bMsgType == PART_PTR_DATA ) 
                OSReleaseMemory(bobsCommand.IntertaskUnion.PointerData.pData);
        }
    }
}

void fnBobInterTaskHandler( void ) 
{
    BOOL            bobStat = BOB_OK;                                                   // the result of responding to the command      
    BOOL            specialMessage;
    uchar           ucEventGroupId;         //Should not be used if it is not first set.  
    ulong           lwGoodEvent;                                                // used to signal status to bobutilts           
    ulong           lwBadEvent;
    ushort          realID;
    unsigned char   caller;                 // callers task ID                              
    ulong           lwEventsReceived;       // copy of event group after waiting for event  
    T_BOB_EVENT_MASK    stCallerEvents;     // Has the bits and event ID associated with the calling task.
    UINT16          uwEventPairID = 0;      // Comes from the message. Should be zero if caller is in range.

    // preset to standard message handling
    bobState = STATE_OK;                                                    
    specialMessage = FALSE;         // Local, used to indicate the message has already been processed.
    errorAlreadyHandled = FALSE;    // Set global indicator...

    getFlightRecord = FALSE;

    // Handle special messages first...                                                                         

    switch( bobsCommand.bSource ) 
    {
        case SYS:                           // SPECIAL MESSAGES FROM SYS TASK
            switch( bobsCommand.bMsgId )    // --------------------------------
            {   
                case SCRIPT_INIT_SCM:
                    // Perform the script that is associated with the 
                    //  initialization message, and do normal error handling for now.
                	bobStat = fnDoScript( bobsCommand.bMsgId );

                	bobStat = fnBobPublishPowerupJanus( bobStat );
                    fnBobErrorStandards( bobStat );                                                     
                    // Set the flag, indicating that initialization was completed.
                    OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_SCM_INIT_COMPLETE, OS_OR);
                    OSSendIntertask(SYS, SCM, SCM_INITIALIZE_RSP, NO_DATA, NULL_PTR, 0);
                    // and mark that this mesage is already processed.
                    specialMessage = TRUE;
                    break;

                default:
                    break;
            }
            break;

        default:
            break;
    }

    if( !specialMessage ) 
    {
        switch( bobsCommand.bMsgType ) 
        {
            // IF FROM BOB UTILITIES (fnValidData, fnWriteData)
            // --------------------------------------------------
            case PERFORM_SCRIPT:                                                
            case MEM_ACCESS:
			case IBUTTON_ACCESS:
                caller = bobsCommand.bSource;
                // If a non-task called this, we need the non-task-event-pair ID.
                if( caller > MAX_TASKS_HANDLED_BY_BOB )
                    memcpy( &uwEventPairID, &bobsCommand.IntertaskUnion.bByteData[ BOBMSG_OFFS_EVNTID ], 2 );                                       
                // Retrieve the event masks and event group ID, based on either 
                //  the callerID or the nontask EvenPairID.
                if( fnGetEventMask(caller, &stCallerEvents, &uwEventPairID) == BOB_OK ) 
                {               
                    // Load the event group ID and the event masks to use
                    lwGoodEvent = stCallerEvents.ulSuccessMask;
                    lwBadEvent = stCallerEvents.ulErrorMask;
                    ucEventGroupId = stCallerEvents.ucEventGroupId;
                    
                    // Retrieve the ID of the script to run or the device msg to send out.
                    realID = bobsCommand.IntertaskUnion.wShortData[0];              
                                                                                
                    if( bobsCommand.bMsgType == MEM_ACCESS ) 
                    {                       
                        // if the request is for a single message, and it is valid...
                        if( realID < DEVICE_MSG_CNT ) 
                        {   // perform the message                 
                            bobStat = fnDeviceMessage( realID );                        
                        }               
                        else bobStat = BOB_SICK;
                    }

                    if( bobsCommand.bMsgType == PERFORM_SCRIPT) 
                    {                   
                        // if the request to perform a script, and it is valid...
                        if( realID < INVALID_BOB_MSG ) 
                        {   // summon the script handler
                            bobStat = fnDoScript( realID );                         
                        }
                        else bobStat = BOB_SICK;
                    }

					if( bobsCommand.bMsgType == IBUTTON_ACCESS) 
                    {                   
                        // if IBUTTON access, and it is valid...
                        if( realID < INVALID_IBUTTON_ACCESS_MSG ) 
                        {   // summon the script handler
                            bobStat = fnAccessIButton( realID );                         
                        }
                        else bobStat = BOB_SICK;
                    }

                    fnBobErrorStandards( bobStat );
                    if( bobStat != BOB_OK ) 
                    {   // if failure, post a bad event to the bob utility
                        OSSetEvents( ucEventGroupId, lwBadEvent, OS_OR );
                    }   
                    else  // or, if success, post a good event to the bob utility
                        OSSetEvents( ucEventGroupId, lwGoodEvent, OS_OR ); 
                }
                else 
                    bobStat = BOB_SICK;
                break;

            // IF AN INTERTASK REQUEST TO PERFORM A SCRIPT
            // -------------------------------------------
            default:                                                            
                if( bobsCommand.bMsgId < INVALID_BOB_MSG ) 
                {   // if the request is valid
                    switch( bobsCommand.bMsgId ) 
                    {   // performance is based on the request

                        // For An Image Multi-Script Request
                        case BOB_SELECT_IMAGE_REQ:
                            // Note that fnMultiScript handles it's own task replies.
                            bobStat = fnMultiScript( &bobsCommand );                        
                            break;                                                          
                                                                                                                                    
                        default:                                                     
                            // perform the script that is associated with the
                            //  requested operation, and upon completion of 
                            // the sciprt (or upon encountering an error),
                            // transmit a reply to the calling task.
                            bobStat = fnDoScript( bobsCommand.bMsgId );                  
                            fnBobErrorStandards(bobStat);           
                            // transmit a reply to the calling task                 
                            bobReply( bobsCommand.bSource, bobsCommand.bMsgId, bobStat);
                            break;
                    } // end of switch(bMsgID)
                }// end of if bMsgId is valid
                else 
                {   
                    // INVALID INTERTASK MESSAGE ERROR
                    bobState = BOB_NG_TASK_MESSAGE;                                 
                    bobStat = BOB_SICK;
                    fnBobErrorStandards( bobStat );
                }
                break;  // End of default case processing.
        }  // End of switch(bMsgType)

#ifdef  PSD_FLYING_ALLOWED
        if( getFlightRecord ) 
            fnDeviceMessage( PSD_GET_FLIGHT_RECORD );
#endif

    }   // End of if(!specialMessage)
    return;
}


/***********************************************************************

 FUNCTION NAME:     BOOL fnMultiScript(INTERTASK *rCommand) {

 PURPOSE:           Performs a series of message scripts... 
                    each script is a list of records that define messages that
                    must be communicated to periferals in order to complete 
                    an application operation.
                    
 AUTHOR:            R. Arsenault

 INPUTS:            Pointer to an intertask message received by the bob task.
 ************************************************************************/
BOOL fnMultiScript( INTERTASK *interMsg ) 
{
    BOOL    retval;
    uchar   scriptCount;
    uchar   scriptIndex;
    uchar   imageType;
    uchar   imageID;
    uchar   msgIndex;
    BOOL    sendReply;


    retval = BOB_OK;
    if( fBobReplied == FALSE )
    sendReply = TRUE;
    msgIndex = FIRST_SELECTION_BYTE_LOC;        // initialize an index into the intertask message   

    switch( interMsg->bMsgId )                  // operation is based on the intertask message ID   
    {   
        
        case BOB_SELECT_IMAGE_REQ:              // ignore without error for now.
            retval = BOB_OK;                    // waiting for the OI to figure out what it wants to do
        break;

        default:                                // INVALID INTERTASK MESSAGE                        
            retval = BOB_SICK;
        break;

    } // endof switch( bMsgID )
    fnBobErrorStandards( retval );
    if( sendReply ) 
    {
        // perform the multi-script and when all scripts are complete (or on encountering an error)
        bobReply(   bobsCommand.bSource, bobsCommand.bMsgId, retval );
    }
    return( retval );
}

/******************************************************************************
 FUNCTION NAME:         fn78LastMsgMarker
 PURPOSE:           
    Marks the last succesful message sent to each device.
 INPUTS:            
    ID of last message and status of last message.
 AUTHOR:            R. Arsenault
 ----------------------------------------------------------------------------*/
void fn78LastMsgMarker( UINT8 bMsgId, BOOL bobStat ) 
{
    struct msgControl   controlRec;

    if( bobStat == BOB_OK ) 
    {
        // If the last message was successful, store the ID of the message in 
        //  the appropriate device-specific, global variable.  Each device has
        //  its own last successful message ID.
        fnTeaGetMsgControl( bMsgId, &controlRec );
        switch( controlRec.device ) 
        {                       
            case IPSD_BL:       lastIpsdBlMessage   = bMsgId; break;
            case IPSD:          lastIpsdMessage     = bMsgId; break;
            default: break;
        }
    }
    // if not successful, don't do anything.
    return;
}



/***********************************************************************

 FUNCTION NAME:     BOOL fnMsgDateTime(uchar msgID)

 PURPOSE:           Loads date and/or time values into a specified message's structure.
                    Returns TRUE only if loading is successful.
                    
 AUTHOR:            R. Arsenault

 INPUTS:            ID of device message being built.  
************************************************************************/

BOOL fnMsgDateTime(ushort msgID) {
    struct  DTdepends dependenceRecord;
    struct  DTdepends exceptionRecord;
    short   destID;
    BOOL    retval, foundit;
    ushort  destItem, sourceItem;
    uchar   rekords, rekordsEnd;
    char    *dest = NULL_PTR;
    char    *source = NULL_PTR;
    uchar   sizz = 0;


    retval = TRUE;
    foundit = FALSE;
    if( fnTeaGetDependence( msgID, &dependenceRecord ) ) 
    {
        fnTeaGetConstant( MAXSYSDATEREFERENCE, &rekordsEnd );

        // If a valid System date/time can be retrieved,
        if (GetSysDateTime (&msgSysTime, ADDOFFSETS, GREGORIAN)) 
        {   // For each entry in the msgSysDates table, convert the *src to BCD and store at dest.
            for( rekords = 0; rekords < rekordsEnd; rekords++ ) 
            {               
                fnTeaGetSysDateRef( rekords, &destItem, &sourceItem );
                dest = bobaVarAddresses[destItem].addr;                                             
                source = bobaVarAddresses[sourceItem].addr;
                *dest = BinToBCD( *source );
            }

            // Try to retrieve a valid Printed date too.
            if( GetPrintedDate( &msgPrintTime, GREGORIAN) ) 
            {   // and for each entry in the msgPrintDates cross-reference table, convert the *src 
                //  to BCD and store at dest.
                fnTeaGetConstant(MAXPRINTDATEREFERENCES, &rekordsEnd);
                for( rekords = 0; rekords < rekordsEnd; rekords++ ) 
                {
                    fnTeaGetPrintDateRef( rekords, &destItem, &sourceItem );
                    dest = bobaVarAddresses[destItem].addr;     
                    source = bobaVarAddresses[sourceItem].addr;
                    *dest = BinToBCD( *source );
                }
                // Also convert the printed date to the MailDate format (ipsd).  This is 
                //  number of seconds since IPSD_epoch.

                //The maildate is calculated using julian date algorithms. It is only significant to the year month and day,
                // it is always normalized to 12:00 noon on a given day, so all day long we use the same debit date.
                //The German debit algorithm outputs the debit time rather than the input maildate like other algorithms.
                //The ibutton can backdate mail because it caches the output debit time and only updates it if the input
                // maildate changes, so for Germany we will vary the input maildate slightly print to print to force the 
                // cache to update.
                //However don't add in the minutes and seconds if we are set to max advanced date, we get 2015 error otherwise   
                if (  (bIPSD_indiciaType == IPSD_IND_TY_GERMANY) &&
                      (GetPrintedDateAdvanceDays() != bIPSD_dateAdvanceLimitDaze) )
                {
                    msgPrintTime.bMinutes = msgSysTime.bMinutes;
                    msgPrintTime.bSeconds = msgSysTime.bSeconds;
                }
                else
                {
                    msgPrintTime.bMinutes = 0;
                    msgPrintTime.bSeconds = 0;
                }
                fnSysTimeToIPSD( bobaVarAddresses[JDATAMAILDATE].addr, &msgPrintTime );
                
            }                                                                   
            else retval = FALSE;                                                

            dest = bobaVarAddresses[dependenceRecord.destID].addr;                  // store the destination, source, and size  
            source = bobaVarAddresses[dependenceRecord.sourceID].addr;              // of the date-time fields to be applied    
            sizz = bobaVarAddresses[dependenceRecord.destID].siz;
            foundit = TRUE;

        }
        // if can't get current time, skip everything else.
        else retval = FALSE;                            
                                
        if( retval && foundit ) 
        {   
            switch(dependenceRecord.xceptionCase) 
            {   // If there are possible exceptions to the table settings, determine if an
                // exception is in effect.          
    
/* For now, no date ducking in Janus...  Data is not yet stored anywhere.

                case PREDEBIT_XCEPTION:                                         
                case UPDATE_EXCEPTION:
                case SESSION_EXCEPTION:
                case SELKEY_EXCEPTION:
                    switch( jdataVltDebitOptsFile.dateDuckFlag[0] ) 
                    {  // if an exception is in effect, switch to appropriate exception table

                        case VLT_DUCKED_OUT_DATE:
                            retval = fnTeaGetexception(msgID, &exceptionRecord);
                            if( retval ) 
                            {
                                dest = bobaVarAddresses[exceptionRecord.destID].addr;
                                source = bobaVarAddresses[exceptionRecord.sourceID].addr;
                                sizz = bobaVarAddresses[exceptionRecord.destID].siz;                    // defined in the exception table           
                            }
                        break;
    
                        case VLT_DUCKED_OUT_DAY:
                        case VLT_DATE_DAY_UNDUCKED:
                        default:
                        break;
                    }
                    break;
  */    
                default:
                case NO_EXCEPTION:
                break;
            }       
        }        
    }

    if( retval && foundit && (dest != NULL_PTR) && (source != NULL_PTR) ) 
        EndianAwareCopy( dest, source, sizz );                       // update the fields

    return( retval );
}



/***********************************************************************

 FUNCTION NAME:         fnDeviceMessage
 PURPOSE:           
    Instigates a single device message.
 INPUTS:            
    ID of device message to be handled/performed.
 HISTORY:
  2009.02.17  Clarisa Bellamy - Add a case for IPSD_BL.  In the first switch,
                    it falls through, and uses the same driver as IPSD.
                    In the error cases, it does the same thing for now as 
                    IPSD, but will eventually have its own errors codes
 AUTHOR:            R. Arsenault
 ************************************************************************/
BOOL fnDeviceMessage( ushort    bMsgId ) 
{                                               
    BOOL    retval;                         // status returned to caller                                
    char    bEventity;                      // status received when checking events                     
    ulong   lwEventsReceived;               // copy of event group after waiting for event              
    int  	idx;							// Loop counter/index, less than 8-bits needed. 
    struct  msgInfo messyInfo;
    struct  msgInfo getresInfo;
    uchar   *statusPlace;
    uchar   whichDriver = 0;
    uchar   howToHandleMsg;
    uchar   PsdSwDownloadPacketRetryCount = 0;
    uchar   applicationRetryCnt = 0;
    ushort  internalError = JMEGABOB_STATE_OK;
    ulong   appliedTimeout = 0;
    UINT8   ubItMsg;                 // InterTask Message Command to driver.
	const char *pDeviceName = pDefaultDevName;

    // Setting this ahead of time, allows the IPSD_BL case to fall-through and 
    //  reuse most of the IPSD code.
    ubItMsg = TRANSMIT_COMMAND;
     
    retval = BOB_OK;
    memset( juniorBuildMsg, 0xFF, BUILD_BUF_SIZ );      // clean the transmit message buffer
    memset( juniorReplyMsg, 0xFF, REPLY_BUF_SIZ );      // clean the message reply buffer

    if( !(fnMsgDateTime(bMsgId)) ) 
    {   // report an error if unable to get date/time
        internalError = BOB_DATE_TIME_ACCESS_ERROR;
        retval = BOB_SICK;
    }
    
    // partially fill the structure that will be used to direct building of 
    //  the message...
    messyInfo.msgID = bMsgId;                                                
    messyInfo.xmitBuf = &juniorBuildMsg[0];                                 
    messyInfo.recBuf =  &juniorReplyMsg[0];
    messyInfo.xferStruc = &transferStruct;
    if( retval == BOB_OK ) 
    {
        // build the message and examine the results
        howToHandleMsg = fnTeaBuildMessage( &messyInfo );                       
        if( howToHandleMsg == BOB_BUILD_MSG_ERROR ) 
        {
            internalError = BOB_MESSAGE_BUILD_ERROR;
            retval = BOB_SICK;                                              
        }
        if( howToHandleMsg == BOB_USB_JUST_ACCUMULATE_MSG ) 
            // continue accumulating the message, or the normal
            return( BOB_OK ); 
  
		// This is for UberDebug Info:
		if( fBobUberDebugDevMessage == TRUE )
		{
			for( idx = 0; idx < ARRAY_LENGTH(sDeviceNameTable); idx++ )
			{
				if( sDeviceNameTable[idx].destID == messyInfo.device )
					pDeviceName = sDeviceNameTable[idx].devName;
			}
		}
        
        // If cannot talk to IPSD, then just skip this message.    
        if(   (messyInfo.device == IPSD) 
           && (bPsdType == PSDT_NA) )
        {
            return( BOB_OK );
        }

        // Trap Error conditions ----------------------------
        // If Asteroid, cannot talk to IPSD_BL, then just skip this message.    
        if( messyInfo.device == IPSD_BL )
        {
            // If Asteroid, cannot talk to IPSD_BL, then just skip this message.    
            if( bIPSD_ProgMode == IPSD_MODE_ASTEROID )
            {
                fnLogError( ERR_TYPE_NORMAL, ERROR_CLASS_JBOB_TASK, BOB_IPSD_BL_2_ASTEROID );
                fnSetBobInternalErr( BOB_IPSD_BL_2_ASTEROID );
                return( BOB_SICK );     // Not sure if this is correct.
            }
        }
        else if( messyInfo.device == IPSD )
        {
            // If IPSD App message, make sure we are NOT in Boot Loader mode.
            if( bIPSD_ProgMode == IPSD_MODE_BOOT_LOADER )
            {
                fnLogError( ERR_TYPE_NORMAL, ERROR_CLASS_JBOB_TASK, BOB_APP_CMD_2_GEMINI_IN_BL );
                fnSetBobInternalErr( BOB_APP_CMD_2_GEMINI_IN_BL );
                return( BOB_SICK );     // Not sure if this is correct.
            }
        }
        // --------------------------------------------------

  
#ifdef DEBUG_TRACE                                                          // emulator builds (for development) maintain a
        fnStoreDeviceXmit();                                                // flight recorder of transmitted and received device messages
#endif

        switch(messyInfo.device)
        {                           // Determine The Driver, Event Timeout, and App Level Retries
                                    // ----------------------------------------------------------
            case IPSD_BL:      
                ubItMsg = TRANSMIT_BL_COMMAND;
                // Fallthrough   
            
            case IPSD:                                                      // 
// For debugging.. \/.. CB
                fnUpdateIpsdMsgHistory( bMsgId );
// For debugging.. /\.. CB

                // Keep track of whether we are in the precreated state.
                // This is a bit crude, because it only keeps track of if we should be,
                //  (the message was sent) not whether the message was successful.
                // It also may set to FALSE, even for some messages that don't change the
                //  state, like ??
                if(   (bMsgId == IPSD_PRECREATE_INDICIUM)
                   || (bMsgId == IPSD_GERMAN_PRECREATE_INDICIUM)
                   || (bMsgId == IPSD_PRECREATE_INDICIUM_WITH_MSG_TYPE)
                   || (bMsgId == IPSD_PRECREATE_INDICIUM_FLEX_DEBIT)  )
                {
                    fIpsdIndiPrecreatedState = TRUE;
                }
                else
                {
                    fIpsdIndiPrecreatedState = FALSE;
                }
                // ---
                // Set up stuff to send the message...
                appliedTimeout = messyInfo.timeout;                             // retries are handled by the driver
                appliedTimeout += messyInfo.timeout * transferStruct.retries;   // therefore the timeout will be multiplied by the
                applicationRetryCnt = 0;                                        // allowed retries
                whichDriver = CIBUTTON;                                         
				
				// DEBUG !!! 
				if( fBobUberDebugDevMessage == TRUE )
				{
					sprintf( pLocalScratchPad150, "%s DeviceMessage: Msg Timeout= %d, .retries= %d", 
							 pDeviceName, appliedTimeout, transferStruct.retries );
					fnDumpStringToSystemLog(pLocalScratchPad150);
				}
				// !!!!
            break;

            default:
                internalError = BOB_UNKNOWN_DEVICE_ID;
                retval = BOB_SICK;
            break;
        }

        if( retval == BOB_OK ) 
        {
                // Interface To Drivers
                // -------------------------------------
//          do {                                                                    // start of psd sw download retry loop
                OSSetEvents( BOB_DRIVERS_EVENTS, WAITING_FOR_T0, OS_AND );          // all drivers handshake to bob through an event register
                                                                                    // reset the register
                // drivers are driven by intertask messages, and need the transfer control 
                //  structure that was filled by fnTeaBuildMessage
                OSSendIntertask( whichDriver, SCM, ubItMsg, GLOBAL_PTR_DATA, 
                                 &transferStruct, sizeof(struct bobDriverIface) );              

                // after starting the driver, suspend until the event handshake occurs
                //  (or until timeout)
                bEventity = OSReceiveEvents( BOB_DRIVERS_EVENTS, T0_SUCCESS | T0_ERROR, 
                                             appliedTimeout, OS_OR, &lwEventsReceived );
                
				// DEBUG !!! 
				if( fBobUberDebugDevMessage == TRUE )
				{
					sprintf( pLocalScratchPad150, "%s DeviceMessage-RxEvent: bEventity= %d, lwEventsReceived= %08X", 
							 pDeviceName, bEventity, (UINT32)lwEventsReceived );
					fnDumpStringToSystemLog(pLocalScratchPad150);
				}
				// !!!!

                statusPlace = messyInfo.statusPlace;                                
                
                if( bEventity == SUCCESSFUL ) 
                {
                    // when the handshake arrives, 
                    if( lwEventsReceived & T0_SUCCESS ) 
                    {                           // if a handshake occurred
						// DEBUG !!! 
						if( fBobUberDebugDevMessage == TRUE )
						{
							sprintf( pLocalScratchPad150, "%s DeviceMessage-RxEvent: BOB_OK", pDeviceName );
							fnDumpStringToSystemLog( pLocalScratchPad150 );
						}
						// !!!!
                        retval = BOB_OK;
                    }
                    else if (lwEventsReceived & T0_ERROR)
                    {
                        // Low-level Error: 
                        ucErrorFlags = transferStruct.errorFlags;
                        retval = BOB_SICK;
						sprintf( pLocalScratchPad150, "%s DeviceMessage-RxEvent: LowLevelError. ErrFlags= %02X", 
								 pDeviceName, transferStruct.errorFlags );
						fnDumpStringToSystemLog( pLocalScratchPad150 );

                        switch( messyInfo.device )
                        {
                            case IPSD_BL:
                                internalError = BOB_IPSD_BL_COM_TIMEOUT_FAILURE;
                                break;

                            case IPSD:
                            default:
                                internalError = BOB_IPSD_COM_TIMEOUT_FAILURE;
                                break;
                        }
                    }
                    else 
                    {
						// Make sure error info is in syslog.
						sprintf( pLocalScratchPad150, "*s DeviceMessage-RxEvent: BobComFailed. ErrFlags= %02X", 
								 pDeviceName, transferStruct.errorFlags );
						fnDumpStringToSystemLog( pLocalScratchPad150 );
                        ucErrorFlags = transferStruct.errorFlags;   
                        getFlightRecord = TRUE;
                        switch( messyInfo.device ) 
                        {
                            case IPSD:      
                                internalError = BOB_IPSD_COM_FAILURE;   
                            break;

                            case IPSD_BL:      
                                internalError = BOB_IPSD_BL_COM_FAILURE;   
                            break;
                            
                            default:        
                                internalError = BOB_UNKNOWN_COM_FAILURE;
                            break;
                        }
                        if( applicationRetryCnt > 1 ) 
                        {                           // are application level retries allowed?
                            applicationRetryCnt--;
                            fnSetBobInternalErr( BOB_MESSAGE_RESEND );
                        }
                        retval = BOB_SICK;
                    }
                }                                                       
                else 
                {
                    if(applicationRetryCnt > 1) 
                    {                                   // are application level retries allowed?
                        --applicationRetryCnt;
                        fnSetBobInternalErr( BOB_MESSAGE_RESEND );
                    }
                    else 
                    {
                        getFlightRecord = TRUE;
                        switch( messyInfo.device ) 
                        {
                            case IPSD:
                                if( bEventity == OS_NO_EVENT_MATCH )
                                    internalError = BOB_IPSD_COM_OS_NO_EVENT_MATCH;
                                else if( bEventity == OS_TIMEOUT )
                                    internalError = BOB_IPSD_COM_OS_TIMEOUT;
                                else
                                    internalError = BOB_IPSD_HANDSHAKE_FAILURE;
                            break;

                            case IPSD_BL:
                                if( bEventity == OS_NO_EVENT_MATCH )
                                    internalError = BOB_IPSD_BL_COM_OS_NO_EVENT_MATCH;
                                else if( bEventity == OS_TIMEOUT )
                                    internalError = BOB_IPSD_BL_COM_OS_TIMEOUT;
                                else
                                    internalError = BOB_IPSD_BL_HANDSHAKE_FAILURE;
                            break;
                            
                            default:        
                                internalError = BOB_UNKNOWN_HANDSHAKE_FAILURE;  
                            break;
                        }
	
						// Note this in the syslog:
						sprintf( pLocalScratchPad150, "%s DeviceMessage-RxEvent: BobInternalErr= %02X", 
								 pDeviceName, internalError );
						fnDumpStringToSystemLog( pLocalScratchPad150 );
 
                        retval = BOB_SICK;
                    }
                }
                #ifdef DEBUG_TRACE
                // emulator builds (for development) maintain a flight recorder
                // of transmitted and received device messages
                fnStoreDeviceReply();  
                #endif                                                          
        
//          } while( PsdSwDownloadPacketRetryCount );
        }
        if( retval == BOB_OK ) 
        {   
            if( !transferStruct.errorFlags ) 
            {
                if( transferStruct.expectedReply ) 
                {
                    // Both IPSD and IPSD_BL replies need to be checked for 
                    // minimum length and format before extracting data.
                    internalError = fnTeaCheckReplyStatus( &messyInfo );
                    if( internalError == JMEGABOB_STATE_OK )
                    {   
                        // Messages from the IPSD are handled differently.  Look for status first.
                        if( (messyInfo.flags[0] & VREPLY_MASK) == RPLY_IPSD )
                        {
                            // If a reply message from the IPSD contained a status, 
                            // check the status.  If bad, don't process any more packets
                            // or attempt to distribute the reply.
                            if( fnTeaExamineReply( &messyInfo ) == FALSE ) 
                            {
                               // There is already a device error to post
                               internalError = DO_NOT_POST_BOB_ERROR;
                               // If this message might have parsable data, and is longer than just
                               //  the error status, parse the data.
                               if(   ((messyInfo.flags[0] & TRY_TO_DISTRIB) == TRY_TO_DISTRIB)
                                  && (transferStruct.replyLen[0] > 2) )
                               {
                                    fnTeaDistribReply( &messyInfo );
                                }
                                retval = BOB_SICK;
                            }
                            else
                            {   // Otherwise distribute the reply ...                                                          
                                retval = fnTeaDistribReply( &messyInfo );
                                if( retval )
                                    retval = BOB_OK;
                            }
                        }
                        else
                        {   // If NOT from an IPSD, distribute reply, THEN look at status.                                                          
                            retval = fnTeaDistribReply( &messyInfo );
                            if( retval )
                            {
                                retval= fnTeaExamineReply( &messyInfo );
                                if( retval == TRUE ) 
                                    retval = BOB_OK;
                                else 
                                {   // there is already a device error to post
                                    internalError = DO_NOT_POST_BOB_ERROR;
                                    retval = BOB_SICK;
                                }
                            }
                        }
                    }
                    else    //  fnTeaCheckReplyStatus returned an error status.                       
                        retval = BOB_SICK;
                } //end if(expectedReply)
            }
            else    // transferStruct.errorFlags 
            {
// For debugging..\/.. CB   
// So the error flags can be easily seen by the emulator at a LATER breakpoint.
                ucErrorFlags = transferStruct.errorFlags;
// For debugging../\.. CB
				// Make sure error is in syslog:
				sprintf( pLocalScratchPad150, "%s DeviceMessage-RxEvent: BOB_OK. ErrorFlags= %02X", 
						 pDeviceName, transferStruct.errorFlags );
				fnDumpStringToSystemLog( pLocalScratchPad150 );
            
                retval = BOB_SICK;
            }
        }
    }   
                                                                
    if( retval == BOB_OK ) 
        // if the message was successful, this will mark it as the last sent.
        fn78LastMsgMarker( bMsgId, retval );                    
    else 
    {
        if( internalError ) 
            fnSetBobInternalErr( internalError );
    }
  
	 // DEBUG !!! 
	 if( fBobUberDebugDevMessage == TRUE )
	 {
		// To find the MsgID definitions, search for NOT_A_MESSAGE in orbit.h
	 	sprintf( pLocalScratchPad150, "%s DeviceMessage: MsgID = %d, returning retval=%d %s", 
	 			pDeviceName, (int)bMsgId, retval, (retval == BOB_OK ? "BOB_OK": "Not BOB_OK") );
	 	fnDumpStringToSystemLog( pLocalScratchPad150 );
	 }
	 // !!!!

    return( retval );                                                                   
}
        
        

void fnStoreDeviceReply() {
    ushort  j;

#ifdef DEBUG_TRACE
    if (traceOff > TRACE_MARGIN) traceOff = 0;
    memset(&traceBuffer[traceOff],0xEE, 16);
    traceOff += 16;
    memcpy(&traceBuffer[traceOff],                                      // this section stores the device reply
            transferStruct.pDataRecv[0],                                // in a flight recorder...
            transferStruct.replyLen[0]);                                // it has no product function and is
    traceOff += transferStruct.replyLen[0];                             // not included in manufacturing
                                                                        // builds.
    j = 0;                                                              //
    while ((traceOff % 16) != 0) {                                      // traceBuffer is a very large circular
        traceBuffer[traceOff] = 0;                                      // buffer that lists the device
        traceOff++;                                                     // messages that have been transmitted
        j++;                                                            // and received
        if (j > 15) break;                                              //
    }                                                                   //
    tracer = (char *)(&traceBuffer[traceOff]);
#endif
    return;
}

void fnStoreDeviceXmit() {
    ushort  j;

#ifdef DEBUG_TRACE
    if (traceOff > TRACE_MARGIN) traceOff = 0;
    memset(&traceBuffer[traceOff],0xFF, 16);
    traceOff += 16;
    memcpy( &traceBuffer[traceOff],                                     // this section stores the message being
            juniorBuildMsg,                                             // transmitted in a flight recorder...
            transferStruct.bMsgLen);                                    // it has no product function and is
    traceOff += transferStruct.bMsgLen;                                 // not included in manufacturing
    j = 0;                                                              // builds.
    while ((traceOff % 16) != 0) {                                      //
        traceBuffer[traceOff] = 0;                                      // traceBuffer is a very large circular
        traceOff++;                                                     // buffer that lists the device
        j++;                                                            // messages that have been transmitted
        if (j > 15) break;                                              // and received
    }                                                                   //
    tracer = (char *)(&traceBuffer[traceOff]);
#endif
    return;
}
    

/***********************************************************************

 FUNCTION NAME:     BOOL fnDoScript(uchar msgId)

 PURPOSE:           Performs a message script... a list of records that define messages that
                    must be communicated to periferals in order to complete an application operation.
                    
 AUTHOR:            R. Arsenault

 INPUTS:            Pointer to an intertask message received by the bob task.
MODS:
 2009.01.17 Clarisa Bellamy - Add case for IPSD_BL device.  Same as IPSD for this switch.
 2008.06.12 Clarisa Bellamy - Redirect functions return a 16-bit value, instead of an 
                        8-bit value. (Requires concurrent changes in cbobpriv.c and bob.h)
 ************************************************************************/
BOOL fnDoScript(ushort msgId) 
{
    BOOL    lowerStat;                          // status after implementing each message in the script                     
    T_SCRIPTQUE que[MAX_SCRIPT_NEST];           // storage for nested scripts                                               
    char    qIndex;                             // index into script que                                                    
    char    preStat;                            // status returned from a pre-message task                                  
    char    postStat;                           // status returned from a post-message task                                 
    UINT16  redirect;                           // Script ID of redirect subscript.
    BOOL    keepOnChuggin;                      // cleared when the entire multi-script is aborted                          
    T_SCRIPT scriptRecord;
    ushort  maybeLastScriptIndex = 0;
    BOOL    postError;
    redirectFunc *redirFuncPtr;                 // Pointer to redirect function.

    postError = FALSE;

    repeatCommandFlag = TRUE;
    alreadyRepeated = FALSE;

    while( repeatCommandFlag ) 
    {  
        repeatCommandFlag = FALSE;
        keepOnChuggin = TRUE;
        qIndex = 0;
        lowerStat = BOB_OK;                     // initialize to no error condition
        que[qIndex].scriptIndex = 0;            // initialize an index into the script
        // Convert script ID (msgID) to the one used for the current PSD, and store it in the queue. 
        que[qIndex].scriptID = fnConvertIDMyko2PsdX( msgId, CONV_SCRIPTID );
        // Load the script...
        if( !fnTeaLoadScriptRec(que[qIndex].scriptID, 0, &scriptRecord) ) 
        {   
            keepOnChuggin = FALSE;               // if script loading fails, end with an error
            lowerStat = BOB_SICK;
            fnSetBobInternalErr((ushort)(BOB_BAD_SCRIPT_ID));
        }
        else 
        {
            fnTeaGetScriptSiz(  que[qIndex].scriptID, 
                                &que[qIndex].scriptSize);
            maybeLastScriptIndex = que[qIndex].scriptID;
        }

        // FOR EACH RECORD IN THE SCRIPT
        while(   keepOnChuggin 
              && (   (que[0].scriptIndex < que[0].scriptSize) 
                  || (qIndex > 0) ) )
       {
            // The destination is used to determine how to handle this line.
            switch( scriptRecord.dest ) 
            {                                                               //
                case TASKONLY:                                              // for script records that do not require messages
                lowerStat = scriptRecord.preOp(scriptRecord.Id);            // perform the pre function and (if successful) the
                if (lowerStat == BOB_OK) {                                  // the post function
                    lowerStat = scriptRecord.postOp(scriptRecord.Id, 
                                                    lowerStat);
                }

                switch( lowerStat ) 
                {
                    case BOB_SICK:
                    case BOB_ABORT:
                    case NOT_AVAILABLE_YET:
                    lowerStat = BOB_SICK;
                    break;

                    case BOB_OK:
                    case BOB_ALLOW:
                    lowerStat = BOB_OK; 
                    break;

                    case BOB_SKIP: 
                    lowerStat = BOB_OK; 
                    break;

                    case BOB_SKIP_ALL:
                    keepOnChuggin = FALSE;
                    lowerStat = BOB_OK; 
                    break;

                    case BOB_REPEAT:
                    que[qIndex].scriptIndex--;
                    lowerStat = BOB_OK; 
                    break;

                    case BOB_EXIT_SUBSCRIPT:
                    que[qIndex].scriptIndex = que[qIndex].scriptSize;
                    lowerStat = BOB_OK;
                    break;

                    case BOB_ALLOW_BUT_POST:
                    fnBobErrorStandards(BOB_SICK);
                    freezeErrorHandling = TRUE;
                    postError = TRUE;
                    lowerStat = BOB_OK;
                    break;

                    default:
                    lowerStat = BOB_SICK;
                    break;

                }
                if( lowerStat == BOB_OK ) 
                {
                    que[qIndex].scriptIndex++;
                    fnTeaLoadScriptRec(que[qIndex].scriptID, que[qIndex].scriptIndex, &scriptRecord);
                }
                break;
    
    
                case REDIRECT:
                // The redirect function has a different return value than a normal post-op.
                //  The normal post-op functions only return a signed char (up to 127)
                //  But now there are more than 127 scripts.
                redirFuncPtr = (redirectFunc *)scriptRecord.preOp;
                redirect = redirFuncPtr( scriptRecord.Id );
    
                switch( redirect ) 
                {
                    case DUMMY_SCRIPT:
                    que[qIndex].scriptIndex++;
                    fnTeaLoadScriptRec(que[qIndex].scriptID, que[qIndex].scriptIndex, &scriptRecord);
                    lowerStat = BOB_OK;
                    break;
    
    
                    case BAD_SCRIPT:
                    lowerStat = BOB_SICK;                                   // if the local task err'd, exit with error set     
                    break;
    
    
                    default:
                    if (qIndex + 1 >= MAX_SCRIPT_NEST) {                    // control is redirected by changing the passed     
                            lowerStat = BOB_SICK;                           // if the script queue is full, abort with an error 
                            break;                                          // condition                                        
                    }                                                       //                                                  
                    que[qIndex].scriptIndex++;                              // then advance the script index in the original    
                    qIndex++;                                               // queue record and advance the queue record index  
                    que[qIndex].scriptIndex = 0;                            // initialize the next available queue slot...      
                    que[qIndex].scriptID = fnConvertIDMyko2PsdX( redirect, CONV_SCRIPTID );
                    fnTeaGetScriptSiz(que[qIndex].scriptID, &que[qIndex].scriptSize);
                    fnTeaLoadScriptRec(que[qIndex].scriptID, que[qIndex].scriptIndex, &scriptRecord);
                    break;
    
                }
    
                break;
    
    
                case SUBSCRIPT:                                             // for subscripts:                                  
                preStat = scriptRecord.preOp(scriptRecord.Id);              //                                                  
                switch(preStat) {                                           // if the prerequiste is met, attempt to nest the   
                                                                            // specified script                                 
                    case BOB_EXIT_SUBSCRIPT:
                    que[qIndex].scriptIndex = que[qIndex].scriptSize;
                    lowerStat = BOB_OK;
                    break;
    
                    case BOB_OK:                                            //                                                  
                    if( qIndex + 1 >= MAX_SCRIPT_NEST ) 
                    {                                                   
                        lowerStat = BOB_SICK;                               // if the script queue is full, abort with an error 
                        break;                                              // condition                                        
                    }       
// For debugging..\/.. CB
                    // Update calling script ID and Index, in case
                    //  anyone is looking.
                    wCallingScriptID = que[qIndex].scriptID;
                    wCallingScriptIdx = que[qIndex].scriptIndex;                                                //                                                  
// For debugging../\.. CB
                    que[qIndex + 1].scriptIndex = 0;                        // initialize the next available queue slot...      
                    que[qIndex + 1].scriptID = fnConvertIDMyko2PsdX( scriptRecord.Id, CONV_SCRIPTID );
                    que[qIndex].scriptIndex++;                              // then advance the script index in the original    
                    qIndex++;                                               // queue record and advance the queue record index  
                    fnTeaGetScriptSiz(que[qIndex].scriptID, &que[qIndex].scriptSize);
                    fnTeaLoadScriptRec(que[qIndex].scriptID, que[qIndex].scriptIndex, &scriptRecord);
                    break;                                                  //                                                  
                                                                            //                                                  
                    case BOB_SICK:                                          //                                                  
                    case BOB_ABORT:                                         // if the prerequesite was not met, and it is       
                    lowerStat = BOB_SICK;                                   // indicated that the current operation should      
                    break;                                                  // abort, mark an error condition                   
                                                                            //                                                  
                    case BOB_SKIP:                                          // if the prerequesite indicates to continue but    
                    que[qIndex].scriptIndex++;                              // without performing the nested script, don't      
                    fnTeaLoadScriptRec(que[qIndex].scriptID, que[qIndex].scriptIndex, &scriptRecord);
                    break;                                                  // install the script in the queue, just            
                                                                            // advance the script index in the original         
                    
                    case BOB_SKIP_ALL:
                    lowerStat = BOB_OK;
                    keepOnChuggin = FALSE;
                    break;
    
                    case BOB_REPEAT:
                    default:
                    lowerStat = BOB_SICK;
                    fnSetBobInternalErr((short)(BOB_BAD_SCRIPT_ID));
                    break;
                }
                break;
    
// !!!cb No non-IPSD devices yet.
/*  
                case PHC_DIRECT:
                case VAULT:
                case PRINT_HEAD:
                case CARD_SLOT:
                case PSD:   
  */
                case IPSD_BL:   // fallthrough
                case IPSD:      //
                preStat = scriptRecord.preOp(scriptRecord.Id);
                switch( preStat ) 
                {
                    case BOB_EXIT_SUBSCRIPT:
                    que[qIndex].scriptIndex = que[qIndex].scriptSize;
                    lowerStat = BOB_OK;
                    break;
    
                    case BOB_ABORT:
                    case BOB_SICK:
                    lowerStat = BOB_SICK;
                    break;
    
                    case BOB_SKIP:
                    break;
    
                    case BOB_SKIP_ALL:
                    lowerStat = BOB_OK;
                    keepOnChuggin = FALSE;
                    break;
    
                    case BOB_REPEAT:
                    que[qIndex].scriptIndex--;
                    break;
    
                    default:
                    fnSetBobInternalErr((short)(BOB_BAD_SCRIPT_ID));
                    lowerStat = BOB_SICK;
                    break;
                    
                    case BOB_OK:

#ifdef COMET_PHC_NOT_PRESENT
        if (scriptRecord.dest == PRINT_HEAD) return(BOB_OK);
#endif
// For debugging.. \/.. CB
                    // While debugging, fnDeviceMessage will call 
                    //  fnUpdateIPSDMsgHistory() for all IPSD messages, 
                    //  so...
                    //      Make sure calling script ID and Index are up to date.
                    if( qIndex > 0 )
                    {
                        wCallingScriptID = que[qIndex -1].scriptID;
                        wCallingScriptIdx = que[qIndex -1].scriptIndex;
                        if( wCallingScriptIdx )
                            wCallingScriptIdx -= 1;
                    }
                    else
                    {
                        wCallingScriptID = 0;
                        wCallingScriptIdx = 0;
                    }
                    //      Store current script ID and index.
                    wNowScriptID = que[qIndex].scriptID;
                    wNowScriptIdx = que[qIndex].scriptIndex;
// For debugging.. /\.. CB
                    lowerStat = fnDeviceMessage(scriptRecord.Id);
                    postStat = scriptRecord.postOp(scriptRecord.Id, lowerStat);
                    switch( postStat ) 
                    {
                        case BOB_ALLOW_BUT_POST:
                        fnBobErrorStandards(BOB_SICK);
                        freezeErrorHandling = TRUE;
                        postError = TRUE;
                        lowerStat = BOB_OK;
                        break;
    
                        case BOB_EXIT_SUBSCRIPT:
                        que[qIndex].scriptIndex = que[qIndex].scriptSize;
                        lowerStat = BOB_OK;
                        break;
    
                        case BOB_ALLOW:
                        megabobStat[0] = 0;
                        megabobStat[1] = 0;
                        lowerStat = BOB_OK;
                        break;
    
                        case BOB_SICK:
                        lowerStat = BOB_SICK;
                        break;
    
                        case BOB_OK:
                        break;
    
                        case BOB_SKIP:
                        lowerStat = BOB_SICK;
                        fnSetBobInternalErr((ushort)(BOB_BELATED_SCRIPT_SKIP));
                        break;
    
                        case BOB_SKIP_ALL:
                        lowerStat = BOB_OK;
                        keepOnChuggin = FALSE;
                        break;
    
                        case BOB_REPEAT:
                        que[qIndex].scriptIndex--;
                        break;
    
    
                        case BOB_ABORT:
                        default:
                        lowerStat = BOB_SICK;
                        break;
                    }
                    break;

                } // End of switch( PreStat )                               
                
                // advance the script index and get the next script.                        
                que[qIndex].scriptIndex++;
                fnTeaLoadScriptRec(que[qIndex].scriptID, que[qIndex].scriptIndex, &scriptRecord);
                break;

                default:    // Unknown device or script operation/destination.
                    lowerStat = BOB_SICK;
                break;

            } // End of switch( scriptRecord.dest )
                                                                            // after each script record                         
                                                                            // -------------------------------------------------
            if (lowerStat != BOB_OK) break;                                 // upon any message error, abort remaining scripts  
            while(qIndex) {                                                 // upon completion of a nested script back up the   
                if( (que[qIndex].scriptIndex >=                          // queue to the hosting script...                   
                     que[qIndex].scriptSize) && qIndex ) 
                {                                                   
                    qIndex--;
                    fnTeaLoadScriptRec(que[qIndex].scriptID, que[qIndex].scriptIndex-1, &scriptRecord);
                                                                    //                                                  
                    postStat = scriptRecord.postOp(scriptRecord.Id, lowerStat);
    
                    switch( postStat ) 
                    {
                        case BOB_EXIT_SUBSCRIPT:
                        que[qIndex].scriptIndex = que[qIndex].scriptSize;
                        lowerStat = BOB_OK;
                        break;
    
                        case BOB_ALLOW_BUT_POST:
                        fnBobErrorStandards(BOB_SICK);
                        freezeErrorHandling = TRUE;
                        postError = TRUE;
                        lowerStat = BOB_OK;
                        break;
    
                        case BOB_ALLOW:
                        megabobStat[0] = 0;
                        megabobStat[1] = 0;
                        lowerStat = BOB_OK;
                        break;
    
                        case BOB_OK:
                        break;
    
                        case BOB_ABORT:
                        case BOB_SICK:                                  //                                                  
                        lowerStat = BOB_SICK;
                        break;
    
                        case BOB_SKIP:
                        lowerStat = BOB_SICK;
                        fnSetBobInternalErr((ushort)(BOB_BELATED_SCRIPT_SKIP));
                        break;
    
                        case BOB_SKIP_ALL:
                        lowerStat = BOB_OK;
                        keepOnChuggin = FALSE;
                        break;
    
                        case BOB_REPEAT:
                        qIndex++;                                       // repeat the nested script that was just finished  
                        que[qIndex].scriptIndex = 0;
                        fnTeaLoadScriptRec(que[qIndex].scriptID, que[qIndex].scriptIndex, &scriptRecord);
                        break;
    
                        default:
                        fnSetBobInternalErr((short)(BOB_BAD_SCRIPT_ID));
                        lowerStat = BOB_SICK;
                        break;
                    }
                    fnTeaLoadScriptRec(que[qIndex].scriptID, que[qIndex].scriptIndex, &scriptRecord);
                }
                else break;
            }
            if (lowerStat != BOB_OK) break;                                 // upon any message error, abort all scripts        
        }
    }
    if (postError) lowerStat = BOB_SICK;
    if (lowerStat == BOB_OK) lastScriptPerformed = maybeLastScriptIndex;
    return(lowerStat);                                                  // return the results to the caller                 
}


/***********************************************************************

 FUNCTION NAME:     void bobReply(uchar caller, ushort bMsgId, BOOL stat)

 PURPOSE:           Updates global statuses and instigates intertask replies
                    based on the results of a completed are failed message script.
                    
 AUTHOR:            R. Arsenault

 INPUTS:            The data portion of the reply messages are composed from a reply map
                    that is referenced in table rMsgStrategy.  For example, for the message
                    SCM_PREPARE_FOR_MAILRUN, the rMsgStrategy table references a map called 
                    bobToPMCtask as the reply map to use.

 ************************************************************************/
void bobReply( uchar caller, ushort bMsgId, BOOL stat ) 
{
    uchar   responseStuff[MAX_INTERTASK_BYTES];             // intertask reply data                         
    uchar   *myReplyMap;
    short   j;
    char    *myBuf;
    uchar   replyMsgID;
    BOOL    skipTheRest;
    char    *myPtr;
    uchar   dummyByte;
    short   mapItemSiz;
    short   varID;
    ushort  bigMsgID;
    uchar   replyMsgType;
//  ulong   alongval;
    uchar   *aptrval;
    BOOL    doDerek;
    struct  TransferControlStructure *myXferStruc;

    // If we have already replied to the current message, 
    //  don't reply again.
    if( fBobReplied == TRUE )
        return; 

    skipTheRest = FALSE;
    doDerek = FALSE;
    memset( responseStuff, 0, sizeof(responseStuff) );

    bigMsgID = bMsgId;
    if( !fnTeaGetItaskReply(bigMsgID, &replyMsgID, &myReplyMap) ) 
    {
        replyMsgID = BOB_RESPONSE;
        myReplyMap = NULL_PTR;
    }
                                                            // BUILD GENERAL PORTION OF REPLY                   
    responseStuff[GENERAL_RESULT] = BOB_OK;                 // ---------------------------------                
    if (stat != BOB_OK) 
    {
        responseStuff[GENERAL_RESULT] = BOB_SICK;
    }
    switch( bMsgId ) 
    {                                       // BUILD SPECIFIC PORTION OF REPLY                  

        case BOB_GEN_STATIC_IMAGE_REQ:
            if(bobErrFlag)
            {
                fnProcessSCMError();
            }
            break;
             
        case SCM_PREPARE_FOR_MAILRUN:
        case SCM_MAIL_IN_PROGRESS:
            //check bob error class and code, and format members for rCMReply struct
            fnSetCMReplyStatus(bMsgId);
            OSSendIntertask( caller, SCM, replyMsgID, GLOBAL_PTR_DATA, 
                             (char *)(&rCMReply), sizeof(rCMReply));
            skipTheRest = TRUE;
            break;


        //for these cases:
        // The associated script is one line and the command to the ibutton is not packetized (not using fnWhileMoreIPSDPackets)
        // the ibutton command status regardless of success or failure is available in pIPSD_4ByteCmdStatusResp
        // and it must be returned to PBP via the deviceReplyStatus[0] member of transfer structure they gave us
        case GET_AUDIT_RECORD:
        case GET_PVD_RECORD:            
        case GET_REFUND_RECORD:
        case PSD_EDC_RECORD:
        case PSD_EDC_NONCE:
            myXferStruc = (struct TransferControlStructure *)(bobsCommand.IntertaskUnion.PointerData.pData),
            memcpy(&myXferStruc->deviceReplyStatus[0], pIPSD_4ByteCmdStatusResp, 4);            
            doDerek = TRUE;
            replyMsgType = BYTE_DATA; 
            break;

        //for these cases:
        // The associated script is multiple lines and the command to the ibutton is  packetized (is using fnWhileMoreIPSDPackets)
        // the ibutton command status regardless of success or failure is available in pIPSD_savedStatus
        // and it must be returned to PBP via the deviceReplyStatus[0] member of transfer structure they gave us
        case PSD_KEY_RECORD:
        case POSTAL_CONFIG_RECORD:
        case AUTH_REQ_RECORD:
        case AUDIT_RSP_RECORD:
        case PVD_RSP_RECORD:
        case REFUND_RESPONSE_RECORD:
            myXferStruc = (struct TransferControlStructure *)(bobsCommand.IntertaskUnion.PointerData.pData),
            memcpy(&myXferStruc->deviceReplyStatus[0], pIPSD_savedStatus, 4);           
            doDerek = TRUE;
            replyMsgType = BYTE_DATA; 
            break;

        default:
            break;
    
    }
    if (doDerek) {
        OSSendIntertask ( caller, SCM, replyMsgID, replyMsgType, responseStuff, 
                          sizeof(responseStuff) );  
    skipTheRest = TRUE;
    }

    if( !skipTheRest ) 
    {
        if( myReplyMap != NULL_PTR ) 
        {
            j = 0;
            myBuf = (char *)(&responseStuff[DETAIL_RESULT]);
            while( myReplyMap[j] ) 
            {
                mapItemSiz = myReplyMap[j];
                memcpy(&varID, &myReplyMap[j+1], 2);
                memcpy(myBuf, bobaVarAddresses[varID].addr, mapItemSiz);
                myBuf += mapItemSiz;
                j += 3;
            }
        }
        OSSendIntertask( caller, SCM, replyMsgID, BYTE_DATA, responseStuff, 
                        sizeof(responseStuff) );    
    }
    fBobReplied = TRUE;
    return;
}

/******************************************************************************
   FUNCTION NAME: fnSetCMReplyStatus
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : format the status bytes that go back to the CM and OI
   PARAMETERS   : None
   RETURN       : None
   NOTES        : None
******************************************************************************/
void fnSetCMReplyStatus(uchar ucMsgId)
{
    rCMReply.bBobOIPrintError = SCM_SUCCESSFUL_STATUS;
    rCMReply.fBobPrintStatus = CM_SUCCESS;
    rCMReply.bReqMsgId = ucMsgId;
    rCMReply.fDebitOK = fBobDebitOK;    
            
    if(bobErrFlag)
    {
        rCMReply.fBobPrintStatus = CM_FAILURE;

        //pass on bobs current error class and code
        // the CM should call fnProcessSCMError which will display the error and clear the code
        rCMReply.wBobErrorClassCode = (ErrClassFrombob<<8) + (ErrCodeFrombob);

        //determine the correct OI error to throw if any
        switch(ErrClassFrombob) 
        {
            case ERROR_CLASS_JBOB_TASK:
                switch(ErrCodeFrombob) 
                {
                    case BOB_ACCOUNT_OUTTA_FUNDS: 
                        rCMReply.bBobOIPrintError = ACCT_ACCOUNT_OUTTA_FUNDS;
                        break;
                    case BOB_ACCOUNT_OUTTA_PEACE: 
                        rCMReply.bBobOIPrintError = ACCT_ACCOUNT_OUTTA_PEACE;
                        break;
                    case BOB_UNKNOWN_ACCOUNT_ERR:
                        rCMReply.bBobOIPrintError = ACCT_UNKNOWN_ACCOUNT_ERR;
                        break;
                    case BOB_PC_EOL_ERROR:
                        rCMReply.bBobOIPrintError = SCM_PC_END_OF_LIFE_ERR;
                        break;
                    default:
                        rCMReply.bBobOIPrintError = LOGGED_PRINT_ERR;
                        break;
                }
                break;

            case ERROR_CLASS_IPSD1:
            case ERROR_CLASS_IPSD2:
                switch(ErrCodeFrombob) 
                {
                    case IPSDERR_POSTAGEVAL_OUTOFRANGE:                         // too high or too low
                        rCMReply.bBobOIPrintError = SCM_POSTAGE_OUT_OF_RANGE;
                        break;

                    case IPSDERR_INSUFFICIENT_FUNDS:
                        rCMReply.bBobOIPrintError = SCM_INSUFFICIENT_FUNDS; 
                        fnDumpStringToSystemLog( "InsuffFunds SCM flag set in fnSetCMReplyStatus 1" );
                        break;

                    case IPSDERR_INSPECTION_OVERDUE:
                        rCMReply.bBobOIPrintError = SCM_METER_INSPECT_LOCK;
                        break;

                    default:
                        rCMReply.bBobOIPrintError = SCM_SECURITY_ERROR;
                        break;                  
                    }
                break;

            default:
                rCMReply.bBobOIPrintError = SCM_SECURITY_ERROR;
                break;
        }
    }
    // if there are no other errors after the debit...
    //   then evaluate the low funds situation and inform the OI if need be 
    else if (ucMsgId == SCM_MAIL_IN_PROGRESS)
    {
        unsigned long ulLowFundsThreshold, ulCurrentDescReg, ulCurrentPostageVal;

        ulLowFundsThreshold = fnfGetCMOSLongParam(CMOS_LP_LOW_FUNDS_WARNING);
        EndianAwareCopy(&ulCurrentDescReg, m4IPSD_descReg, sizeof(ulong));
        EndianAwareCopy(&ulCurrentPostageVal, bobsPostageValue, sizeof(ulong));
                    
        // if there is not enough money in the descending register to frank the
        // next mail piece at the current postage value just send the insufficient funds error
        if( ulCurrentPostageVal > ulCurrentDescReg )
        {
            rCMReply.bBobOIPrintError = SCM_INSUFFICIENT_FUNDS;
            fnDumpStringToSystemLog( "InsuffFunds SCM flag set in fnSetCMReplyStatus 2" );
        }
        else if( ulCurrentDescReg <= ulLowFundsThreshold )
        {
            rCMReply.bBobOIPrintError = SCM_LOW_FUNDS_WARNING;
            fnDumpStringToSystemLog( "LowFundsWarn SCM flag set in fnSetCMReplyStatus" );
        }
        
        // give precedence to low funds warning if it has been reached
        // otherwise check for piece count end of life warning here
        if(rCMReply.bBobOIPrintError == SCM_SUCCESSFUL_STATUS)
        {
            uchar ucEolStat;
             
            ucEolStat = fnCheckPieceCountEndOfLife();
            if(ucEolStat == PSD_EOL_WARNING)
            {
                rCMReply.bBobOIPrintError = SCM_PC_END_OF_LIFE_WARNING;
            }
        }       
    }
}

/***********************************************************************

 FUNCTION NAME:     uchar fnPrintXceptionInfo() 

 PURPOSE:           Returns a pointer to information on why a print operation
                    was aborted.
                    
 AUTHOR:            R. Arsenault

 INPUTS:            none.
                    
 *************************************************************************/
uchar fnPrintXceptionInfo() {

    return(bobSessionState);
}


/***********************************************************************

 FUNCTION NAME:     BOOL fnBobErrorInfo(uchar *pClass, uchar *pCode) 

 PURPOSE:           Copies into the passed locations the most recent device message 
                    status information... information that is compatible with the 
                    central error handler.
                    
                    
 AUTHOR:            R. Arsenault

 INPUTS:            location to write Error Class Code and location to write Error ID Code.
                    
 *************************************************************************/
BOOL fnBobErrorInfo(uchar *pClass, uchar *pCode) {
    BOOL    retCode;

    retCode = bobErrFlag;
    *pClass = ErrClassFrombob;
    *pCode = ErrCodeFrombob;
    bobErrFlag = FALSE;
    return(retCode);
}



/******************************************************************************
 FUNCTION NAME:     void fnBobErrorStandards(BOOL stat) 
 DESCRIPTION: 
    If stat == BOB_OK or freezeErrorHandling is set, then do nothing.
    Else...          
      This goes through the ErrorDirectory in tealeavs.  For each entry, it  
      calls fnTeaGetErrorDirectoryEntry() to get the data from that entry,  
      then calls fnTeaFindDeviceError() to convert the value in the status  
      place referenced by that entry into an errorcode, using the Error 
      Conversion table referenced by that entry.  
      If the converted error code is non-zero, then...
       1. Set the values of ErrClassFrombob, ErrCodeFrombob to the converted 
           code.
       2. Set bobErrFlag to TRUE.
       3. Clear the status place.
       4. Copy the error code (without error class) to the State referenced 
           by the Error Directory Entry.
       5. Stop looking for errors.
     If no non-zero error was found, when looking through the table, then the
     error code is set to BOB_INTERNAL_ERROR (2509) 

 INPUTS:  
    stat - BOB_OK (1) if no errors to look for.
           BOB_SICK (0) if there was a problem of some kind.
 OUTPUTS:
    RETURNS:    Nothing
    If stat != BOB_OK, then bobErrFlag will be set to TRUE and some non-zero 
    value will be loaded into these global variables:
        ErrClassFrombob 
        ErrCodeFrombob
 
 HISTORY:   
 2009.06.16 - Rewritten by Clarisa Bellamy to simplify.
 AUTHOR:            R. Arsenault
 *************************************************************************/
void fnBobErrorStandards( BOOL stat ) 
{
    UINT8                   bErrDirIndex;
    T_TEA_ERROR_DIR_ENTRY   sErrDirEntry;
    T_TEA_ERROR_CONVERTED_DATA sConvertedErrorData;
    BOOL                    fGotAnEntry;

    // This is set if we have found an error and don't want to overwrite it.
    if( !freezeErrorHandling )  
    {
        // If bob does not think there is an error, we don't look for one.
        if( stat != BOB_OK ) 
        {
            // Check each entry in the Error Directory...
            bErrDirIndex = 0;
            fGotAnEntry = fnTeaGetErrorDirectoryEntry( &sErrDirEntry, bErrDirIndex );
            while( fGotAnEntry )
            {
                // This function will convert the value in the Status Place using the
                //  Error Table pointed to by the Directory Entry.  If the converted
                //  error code is non-zero, it returns TRUE.
                if( fnTeaFindDeviceError( &sErrDirEntry, &sConvertedErrorData ) == TRUE )
                {
                    // Found a match with a non-zero error code.  Update these 'global' values.
                    ErrClassFrombob = sConvertedErrorData.bErrClass;
                    ErrCodeFrombob = sConvertedErrorData.bErrCode;
                    bobErrFlag = TRUE;
                    // After we found an error, erase it:
                    memset( sErrDirEntry.pStatus, 0, sErrDirEntry.bStatusSize );
                    // Copy the converted Error code (without the class) into the Device's State.
                    *sErrDirEntry.pState = ErrCodeFrombob;
                    break;
                }
                // Check the next entry in the Error Directory, if there is one...
                bErrDirIndex++;
                fGotAnEntry = fnTeaGetErrorDirectoryEntry( &sErrDirEntry, bErrDirIndex );
            }

            if( !bobErrFlag ) 
            {   
                // This is the infamous 2509 (or 0509) error code - unidentified BobTask problem.
                // stat is not BOB_OK, but no error is listed in any of the 
                //  command status places (including MegaBob)
                ErrClassFrombob = ERROR_CLASS_JBOB_TASK;                    
                ErrCodeFrombob = BOB_INTERNAL_ERROR;                       
                bobErrFlag = TRUE;
            }
        }
    }
    return;
}


/***********************************************************************

 FUNCTION NAME:     short lookUpErrorIndex(T_DEV_STATE_LOOKUP *tableAddr, short tableSize, uchar *statusAddr) 

 PURPOSE:           Returns the index into the passed stateLookup table that matches the 
                    2 byte status at the passed status variable address.
                    
 AUTHOR:            R. Arsenault

 INPUTS:            tableAddr:  address of the stateLookup table
                    tableSize:  size of the stateLookup table
                    statusAddr: address of the 2 byte status to be indexed
                    
 *************************************************************************/
short lookUpErrorIndex( T_DEV_STATE_LOOKUP *tableAddr, short tableSize, uchar *statusAddr ) 
{
    short   anIndex;

    anIndex = 0;
    while( anIndex < tableSize ) 
    {
        if( tableAddr[anIndex].byte1 == statusAddr[0] ) 
        {
            if( tableAddr[anIndex].byte2 == statusAddr[1] ) 
            {
                break;
            }
        }
        anIndex++;
    }
    if( anIndex >= tableSize ) 
        anIndex = tableSize;
    return( anIndex );
}

void fnSetBobInternalErr( ushort errCode ) 
{

    if( errCode != DO_NOT_POST_BOB_ERROR ) 
    {
		// Must be stored in megabobStat with High byte first so that
		//  the comparison with the error table works.
        EndianAwareCopy(&megabobStat[0], (char *)(&errCode), 2);
        fnLogSystemError(   ERROR_CLASS_JBOB_TASK,
                            errCode, bobsCommand.bMsgId, 
                            bobsCommand.IntertaskUnion.bByteData[0],
                            bobsCommand.IntertaskUnion.bByteData[1]
                            );
    }
    return;
}

void fnSetIGBCErr( ulong errCode ) 
{

    if( errCode != DO_NOT_POST_BOB_ERROR ) 
    {
        IGBCStatus = errCode & 0x000000FF;
        fnLogSystemError(   ((errCode >> 8) & 0x000000FF),
                            IGBCStatus, bobsCommand.bMsgId, 
                            bobsCommand.IntertaskUnion.bByteData[0],
                            bobsCommand.IntertaskUnion.bByteData[1]
                            );
		IGBCStatus = EndianAwareGet16(&IGBCStatus);
    }
    return;
}
