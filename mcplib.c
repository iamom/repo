/**********************************************************************
 PROJECT    :   Common
 COPYRIGHT  :   2006, Pitney Bowes, Inc.  
 AUTHOR     :   Derek DeGennaro
 MODULE     :   MCPLib.h
 
 DESCRIPTION:
    Implementation of the MCP Library.

 MODIFICATION HISTORY:
    04/24/2009  Raymond Shen    Added code to read/write NVM info in MCP instance.
    02/25/2009  Raymond Shen    Added fnMCPGetCurrMcpPCNInfo() for wow diagnostic mode.
    1/19/2006	 Derek DeGennaro	Initial Version
      .           .                       .
    <Date>	 <Developer_Name>	<Modification_Description>
 **********************************************************************/

/********************************************************/
// Turn off a bunch of unnessesary lint warnings 
// and errors for files that are non-mcp related.
/********************************************************/
//lint -e46
//lint -e612
//lint -e783
//lint -e537
//lint -e726
//lint -e760
//lint -e525
//lint -e539
//lint -e725
/********************************************************/
#include <nucleus.h>
#include <stdio.h>
#include "pbos.h"
#include "fnames.h"
#include "pbos.h"
#include "mcpcfg.h"
#include "mmcintf.h"
#include "pb232.h"
#include "mmcimjet.h"
#include "nu_usb.h"
#include "MCPLib.h"
#include "clock.h"
#ifndef lint
#include "MCPDIntf.h"
#endif

 // MCP Library Private Data
#define INCLUDE_MCP_LIB_PRIVATE_DATA
#include "MCPLibPriv.h"
#undef INCLUDE_MCP_LIB_PRIVATE_DATA

/********************************************************/
// Re-enable lint warnings
/********************************************************/
//lint +e46
//lint +e612
//lint +e783
//lint +e537
//lint +e726
//lint +e760
/********************************************************/
extern unsigned long fnMMCLoadProfile(void);

static const MCP_INIT_FCN_t fcnMCPInitSteps[MCP_NUM_INIT_STEPS] = {
	fnMCPLibRequestConfigInfo,			/* MCP_REQ_CONFIG_INIT_STEP 		*/
	fnMCPLibRequestStopAndResetFlags,	/* MCP_STOP_AND_RESET_FLAGS_STEP	*/
	fnMCPLibRequestPcnInfo,				/* MCP_REQ_PCN_INFO_STEP			*/
	fnMCPLibRequestNVMInfo				/* MCP_REQ_NVM_INFO_STEP			*/
};

static BOOL fPassThroughModeOn = FALSE;
static int iPassThroughDestination = 0;


unsigned long fnMCPLibCreate(void **pMcp, 
							const unsigned char bMcpId, 
							unsigned char bTaskId, 
							const unsigned char bMcpType,
							const BOOL fContinuousLogging,
							const MCP_REPORT_CONTROL_DATA *pReportInfo)
{
	MCP_INSTANCE_t *pNew = NULL;

	MCPLIB_ASSERT(pMcp,pMcp);	//lint !e506

	if (pMcp==NULL)
		return MCP_LIB_INVALID_HANDLE;

	pNew = (MCP_INSTANCE_t *)calloc(1,sizeof(MCP_INSTANCE_t));	//lint !e718 !e746
	if (pNew == NULL)
	{
		return MCP_LIB_OUT_OF_MEMORY;
	}
	else
	{
		pNew->bMcpId = bMcpId;
		pNew->bTaskId = bTaskId;
		pNew->bMcpType = bMcpType;
		pNew->fContinuousLogging = fContinuousLogging;
		pNew->pReportInfo = pReportInfo;
		*pMcp = pNew;
		return MCP_LIB_SUCCESS;
	}
}

unsigned long fnMCPLibReset(void *pMcp)
{
	MCP_INSTANCE_t *pMcpInst = NULL;

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506

	if (pMcp==NULL)
		return MCP_LIB_INVALID_HANDLE;

	pMcpInst = (MCP_INSTANCE_t*)pMcp;

	// release resources

	if (pMcpInst->sProfileDwnldInfo.binFileHandle != NULL)
	{
		// close the profile
		fclose(pMcpInst->sProfileDwnldInfo.binFileHandle);
		pMcpInst->sProfileDwnldInfo.binFileHandle = NULL;
	}

	pMcpInst->pDnldTaskStack = NULL;

	// unhook the callbacks
	pMcpInst->pfcnMsgRspCb = NULL;
	pMcpInst->pfcnMcpConnectCb = NULL;
	pMcpInst->pfcnMcpDisconnectCb = NULL;
	pMcpInst->pfcnMcpProfDnldCompleteCb = NULL;
	pMcpInst->pfcnInitComplCb = NULL;

	// go back to the idle state
	fnMCPLibSetState(pMcpInst,MCP_LIB_NO_STATE);

	return MCP_LIB_SUCCESS;
}
unsigned long fnMCPLibDestroy(void *pMcp)
{
	unsigned long lwStatus = fnMCPLibReset(pMcp);

	if (lwStatus == MCP_LIB_SUCCESS)
		free(pMcp);	//lint !e718 !e746

	return lwStatus;
}

unsigned long fnMCPInitialize(void *pMcp, MCP_INIT_COMPLETE_CB_t pfcnInitCompleteCb)
{
	unsigned long lwStatus = MCP_LIB_SUCCESS;

	MCP_INSTANCE_t *pMcpInst = NULL;

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	MCPLIB_ASSERT(pMcp,pfcnInitCompleteCb!=NULL); //lint !e506

	if (pMcp==NULL)
		return MCP_LIB_INVALID_HANDLE;

	pMcpInst = (MCP_INSTANCE_t*)pMcp;

	MCPLIB_ASSERT(pMcp,pMcpInst->lwState == MCP_LIB_NO_STATE);	//lint !e506
	if (pMcpInst->lwState != MCP_LIB_NO_STATE)
		return MCP_LIB_INVALID_OP_IN_CURR_STATE;

//	if (!fnMCPIsDevPresent(pMcpInst->bMcpId))
//		return MCP_LIB_NOT_CONNECTED_ERR;

	(void)memset((void*)pMcpInst->bMcpVersion,0,sizeof(pMcpInst->bMcpVersion));
	(void)memset((void*)pMcpInst->bProfileVersion,0,sizeof(pMcpInst->bProfileVersion));
	(void)memset((void*)&pMcpInst->pcnInfo,0,sizeof(pMcpInst->pcnInfo));

	pMcpInst->pfcnInitComplCb = pfcnInitCompleteCb;
	fnMCPLibSetState(pMcpInst,MCP_LIB_INITIALIZING_STATE);
	pMcpInst->lwCurrInitStep = MCP_REQ_CONFIG_INIT_STEP;
	lwStatus = fnMCPLibExecuteNextInitStep(pMcpInst);	

	return lwStatus;
}

char* fnMCPGetCurrProfileVersionString(void *pMcp)
{
	MCP_INSTANCE_t *pMcpInst = NULL;

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	if (pMcp==NULL)
		return NULL;
	pMcpInst = (MCP_INSTANCE_t*)pMcp;
	MCPLIB_ASSERT(pMcp,pMcpInst->fInitComplete); //lint !e506
	if (!pMcpInst->fInitComplete)
		return NULL;

	return pMcpInst->bProfileVersion;
}
char* fnMCPGetCurrMcpVersionString(void* pMcp)
{
	MCP_INSTANCE_t *pMcpInst = NULL;

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	if (pMcp==NULL)
		return NULL;
	pMcpInst = (MCP_INSTANCE_t*)pMcp;
	MCPLIB_ASSERT(pMcp,pMcpInst->fInitComplete); //lint !e506
	if (!pMcpInst->fInitComplete)
		return NULL;

	return pMcpInst->bMcpVersion;
}

MCP_PCN_REC* fnMCPGetCurrMcpPCNInfo(void *pMcp)
{
	MCP_INSTANCE_t *pMcpInst = NULL;

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	if (pMcp==NULL)
		return NULL;
	pMcpInst = (MCP_INSTANCE_t*)pMcp;
	MCPLIB_ASSERT(pMcp,pMcpInst->fInitComplete); //lint !e506
	if (!pMcpInst->fInitComplete)
		return NULL;

	return &(pMcpInst->pcnInfo);
}

unsigned long fnMCPGetCurrMcpNVMMachineInfo(UINT16 usRequestID, void *pMcp, void **pOutputInfo)
{
	unsigned long lwStatus = MCP_LIB_SUCCESS;
	MCP_INSTANCE_t *pMcpInst = NULL;

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	if (pMcp==NULL)
		return MCP_LIB_INVALID_PARAM;
	pMcpInst = (MCP_INSTANCE_t*)pMcp;
	MCPLIB_ASSERT(pMcp,pMcpInst->fInitComplete); //lint !e506
	if (!pMcpInst->fInitComplete)
		return MCP_LIB_INVALID_OP_IN_CURR_STATE;

	switch(usRequestID){
	case MCP_NVM_ALL:
		*pOutputInfo = (void *) &(pMcpInst->wowNvmInfo);
		break;
		
	case MCP_NVM_LENGTH_CALIBRATION_INFO:
		*pOutputInfo = (void *) &(pMcpInst->wowNvmInfo.lenCalibrated);
		break;
		
	default:
		break;
	}
	return lwStatus;
}

unsigned long fnMCPSetCurrMcpNVMMachineInfo(UINT16 usRequestID, void *pMcp,  void *pInputInfo)
{
	unsigned long lwStatus = MCP_LIB_SUCCESS;
	MCP_INSTANCE_t *pMcpInst = NULL;

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506

	if (pMcp == NULL || pInputInfo == NULL)
		return MCP_LIB_INVALID_PARAM;

	pMcpInst = (MCP_INSTANCE_t*)pMcp;

	switch(usRequestID){
	case MCP_NVM_ALL:
		memcpy((void *)&pMcpInst->wowNvmInfo, pInputInfo, sizeof(MMC_WOW_NVM_MACHINE_INFO));
		break;
		
	case MCP_NVM_LENGTH_CALIBRATION_INFO:
		if((*(UINT8 *)pInputInfo != TRUE) && (*(UINT8 *)pInputInfo != FALSE) )
		{
			lwStatus = MCP_LIB_INVALID_PARAM;
		}
		else
		{
			pMcpInst->wowNvmInfo.lenCalibrated = *((UINT8 *)pInputInfo);
		}
		break;
		
	default:
		break;
	}

	return lwStatus;
}

static void fnMCPLibSetState(MCP_INSTANCE_t *pMcp, const unsigned long lwState)
{
	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506

	if (pMcp)
	{
		// stop timers

		// reset the callback functions
		//pMcp->pfcnMsgRspCb = NULL;
		//pMcp->pfcnMcpConnectCb = NULL;
		//pMcp->pfcnMcpDisconnectCb = NULL;

		// set new state
		pMcp->lwPrevState = pMcp->lwState;
		pMcp->lwState = lwState;
	}
}

void fnMCPSendSimpleRequest(void *pMcp,
								   const unsigned char ucMmcMsgID,
								   const unsigned char ucMmcRspMsgID,
								   MCP_MSG_RSP_CB_t pfcnMsgRspCb)
{
	unsigned char mmcMsg[MMC_MJ_SIMPLE_MSG_LEN];
	MCP_INSTANCE_t *pMcpInst = NULL;
	STATUS status = NU_SUCCESS;

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	
	if (pMcp == NULL)
		return;

	pMcpInst = (MCP_INSTANCE_t*)pMcp;

	MCPLIB_ASSERT(pMcp,pMcpInst->lwState == MCP_LIB_NO_STATE);	//lint !e506
	if (pMcpInst->lwState != MCP_LIB_NO_STATE)
		return;
	MCPLIB_ASSERT(pMcpInst,pMcpInst->fInitComplete);	//lint !e506
	if (!pMcpInst->fInitComplete)
		return;

	/* construct the 'simple' message structure using the provided data */
	mmcMsg[MMC_MSG_CLASS_OFFSET]   = MOTION_CLASS_CODE;
	mmcMsg[MMC_MSG_SOURCE_OFFSET]  = pMcpInst->bTaskId;
	mmcMsg[MMC_MSG_DEST_OFFSET]    = pMcpInst->bMcpId;
	mmcMsg[MMC_MSG_COMMAND_OFFSET] = ucMmcMsgID;

	/* send the message to the MMC */
	status = fnMCPSendMsg(mmcMsg, MMC_MJ_SIMPLE_MSG_LEN);
	if (status != NU_SUCCESS)
	{
		// the send failed, check to see if there is a callback
		if (pfcnMsgRspCb != NULL)
		{
			// there is a callback - notify the caller that the send failed
			pfcnMsgRspCb(pMcp,MCP_LIB_SEND_MSG_ERR,NULL,0);
			return;
		}
	}
	

	/* determine if we should be awaiting a reply message, because if
	 * we're not, DON'T SET THE TIMER!!!  We can make this determination
	 * by checking the value of the ucMmcRspMsgId.  If it has been set
	 * to 'MMC_NO_RESPONSE', then don't set the timer...
	 */
	if (ucMmcRspMsgID != MMC_NO_RESPONSE && pfcnMsgRspCb != NULL)
	{
		fnMCPLibSetState(pMcpInst,MCP_LIB_WAIT_RSP_STATE);
		pMcpInst->bWaitRspMsg = ucMmcRspMsgID;
		pMcpInst->pfcnMsgRspCb = pfcnMsgRspCb;

		/* set the MMC Response timer, waiting for the specified response
		 * message ID
		 */
		 // TODO: fix this
		//fnSetMCPCommTimer(ucMsgDest, MOTION_CLASS_CODE, ucMmcRspMsgID);
	}
}

void fnMCPSendRequest(void *pMcp, 
							 const unsigned char *payload,
							 const unsigned char payloadLen,
							 const unsigned char ucMmcMsgID,
							 const unsigned char ucMmcRspMsgID,
							 MCP_MSG_RSP_CB_t pfcnMsgRspCb)
{
	unsigned char mmcMsg[MMC_MAX_MSG_LEN];
	MCP_INSTANCE_t *pMcpInst = NULL;
	STATUS status = NU_SUCCESS;

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	
	if (pMcp == NULL)
		return;

	pMcpInst = (MCP_INSTANCE_t*)pMcp;

	MCPLIB_ASSERT(pMcp,pMcpInst->lwState == MCP_LIB_NO_STATE);	//lint !e506
	if (pMcpInst->lwState != MCP_LIB_NO_STATE)
		return;
	MCPLIB_ASSERT(pMcpInst,pMcpInst->fInitComplete);	//lint !e506
	if (!pMcpInst->fInitComplete)
		return;

	/* construct the message header using the provided data */
	mmcMsg[MMC_MSG_CLASS_OFFSET]   = MOTION_CLASS_CODE;
	mmcMsg[MMC_MSG_SOURCE_OFFSET]  = pMcpInst->bTaskId;
	mmcMsg[MMC_MSG_DEST_OFFSET]    = pMcpInst->bMcpId;
	mmcMsg[MMC_MSG_COMMAND_OFFSET] = ucMmcMsgID;

	/* load the data into the 'payload' section' */
	(void)memcpy((void *)&mmcMsg[MMC_MSG_PAYLOAD_OFFSET], (void *)payload, (size_t)payloadLen);	//lint !e718 !e746

	/* send the message to the MMC */
	/* send the message to the MMC */
	status = fnMCPSendMsg(mmcMsg,MMC_MJ_SIMPLE_MSG_LEN + payloadLen);
	if (status != NU_SUCCESS)
	{
		// the send failed, check to see if there is a callback
		if (pfcnMsgRspCb != NULL)
		{
			// there is a callback - notify the caller that the send failed
			pfcnMsgRspCb(pMcp,MCP_LIB_SEND_MSG_ERR,NULL,0);
			return;
		}
	}

	/* determine if we should be awaiting a reply message, because if
	 * we're not, DON'T SET THE TIMER!!!  We can make this determination
	 * by checking the value of the ucMmcRspMsgId.  If it has been set
	 * to 'MMC_NO_RESPONSE', then don't set the timer...
	 */
	if (ucMmcRspMsgID != MMC_NO_RESPONSE && pfcnMsgRspCb != NULL)
	{
		fnMCPLibSetState(pMcpInst,MCP_LIB_WAIT_RSP_STATE);
		pMcpInst->bWaitRspMsg = ucMmcRspMsgID;
		pMcpInst->pfcnMsgRspCb = pfcnMsgRspCb;

		/* set the MMC Response timer, waiting for the specified response
		 * message ID
		 */
		 // TODO: fix this
		//fnSetMCPCommTimer(ucMsgDest, MOTION_CLASS_CODE, ucMmcRspMsgID);
	}
}

/*
 * Name
 *   fnMMCIlocateProfileGroup
 *
 * Description
 *   This function attempts to locate a Profile Group, using the specified
 *   Profile Group ID.  This is accomplished by walking through the list of
 *   saved Profile Groups, and attempting to match on the Profile Group ID.
 *
 * Inputs
 *   mcpID          - ID of the MCP for which we are trying to locate the group
 *   profileGroupID - Profile Group ID to find the data for
 *
 * Returns
 *   a pointer to an MMCProfileGroup structure, containing the Profile Group
 *     data, if successful
 *   NULL if the Profile Group could not be located
 *
 * Caveats
 *   The mcpID should be used to index into the pMCPStateInfo array so we get
 *   the control block for the appropriate MCP
 *
 * Revision History
 *   05/11/2001 Joseph P. Tokarski (CSS, LLC) - Initial Revision.
 *
 *   01/27/2004 Cheryl M. Tokarski (CSS, LLC) - Modified as part of supporting
 *												multiple MCPs.  Use the mcpID to
 *												index into the array of control blocks.
 */
static MMCProfileGroup *fnMMCIlocateProfileGroup(void *pMcp, unsigned char profileGroupID)
{
	MMCProfileGroup *curProfGrp;
	MCP_INSTANCE_t *pMcpInst = NULL;

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	
	if (pMcp == NULL)
		return NULL;

	pMcpInst = (MCP_INSTANCE_t*)pMcp;	

	/* set the current profile group to the first profile group in the list */
	curProfGrp = *(pMcpInst->sProfileDwnldInfo.profGroups);

	/* loop through the set of Profile Groups, looking for a match on the
	 * Profile Group ID
	 */
	while (curProfGrp != (MMCProfileGroup *)NULL)
	{

		if (curProfGrp->groupId == profileGroupID)
		{
			/* we have a match; return the reference to the current profile
			 * group structure
			 */
			return(curProfGrp);
		}		

		/* advance the profile group to the 'next' one, since we didn't
		 * match 
		 */
		curProfGrp = curProfGrp->next;
	}

	/* if we arrived here, then we were unable to locate the requested
	 * profile group.  Return NULL, and let the calling function handle
	 * the error.
	 */
	return((MMCProfileGroup *)NULL);
}
/*
 * Name
 *   fnMCPStartProfile
 *
 * Description
 *   This function starts the specified profile for the number of iterations
 *   provided, or attempts to locate and start the Profile Group identified
 *   by the 'profileId' for the specified mcpID
 *
 *   In the event that the specified profile ID represents a Profile Group,
 *   the Group's definition is looked up.  In the event that the Group cannot
 *   be located, an error is returned.
 *
 * Inputs
 *   mcpID      - ID of the MCP which we should start the profile for
 *   profileId  - unique profile or profile group Identifier
 *   iterations - number of iterations to execute the profile for
 *
 * Returns
 *   TRUE  - if the Profile/Profile group was successfully started
 *   FALSE - if an error occurred attempting to start the profile
 *
 * Caveats
 *   This function starts the specified profile or profile group on the specified MCP.
 *
 *   The 'iterations' parameter is only signficant when the 'profileId'
 *     parameter references a single Profile, rather than a Profile Group.
 *
 * Revision History
 *   01/27/2004 Cheryl M. Tokaraki (CSS, LLC) - Adapted the original fnMMCStartProfile
 *												to act on the specified MCP.  fnMMCStartProfile
 *												has been modified to call this function with the 
 *												MMC MCPId.
 */
BOOL fnMCPStartProfile(void *pMcp,
							  unsigned char profileId,
							  unsigned char iterations)
{
	MMCProfileGroup *profileGroup;
	unsigned char profileData[MMC_PROFILE_DATA_LEN];
	
	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	
	if (pMcp == NULL)
		return FALSE;

	MCPLIB_ASSERT(pMcp,((MCP_INSTANCE_t*)pMcp)->fInitComplete);	//lint !e506
	if (!((MCP_INSTANCE_t*)pMcp)->fInitComplete)
		return FALSE;

	/* determine if the specified profileId represents a Profile Group */
	if (profileId > MAX_PROFILE_ID)
	{

		/* find the Profile Group, and start it */
		if ((profileGroup = 
			   fnMMCIlocateProfileGroup(pMcp, profileId)) == (MMCProfileGroup *)NULL)
		{
			/* we were unable to locate the Profile Group; return the error
			 * condition.
			 */
			return(FALSE);
		}

		/* Issue the request to start the profile group */
		fnMCPSendRequest(pMcp,profileGroup->startProfiles, profileGroup->startSize,
						 MMC_START_PROFILE_REQ,	
						 MMC_NO_RESPONSE,NULL);
	}
	else
	{
		/* format the buffer to send containing the Profile ID and 
		 * Interations 
		 */
		profileData[MMC_PROFILE_OFFSET] = profileId;
		profileData[MMC_PROFILE_ITER]   = iterations;

		fnMCPSendRequest(pMcp, profileData, MMC_PROFILE_DATA_LEN,
						MMC_START_PROFILE_REQ, MMC_NO_RESPONSE, NULL);
	}

	/* the start request was issued */
	return(TRUE);

}
/*
 * Name
 *   fnMCPCancelGroup
 *
 * Description
 *   This function cancels the specified profile, or locates and cancels the
 *   Profile Group identified by the 'profileId' for the specified mcpID.
 *
 *   In the event that the specified profile ID represents a Profile Group,
 *   the Group's definition is looked up.
 *
 * Inputs
 *   mcpID      - ID of the device on which the group is to be cancelled.
 *   profileId  - unique profile or profile group Identifier
 *
 * Returns
 *   Nothing.
 *
 * Caveats
 *   This function cancels the specified profile or profile group on the specified MCP.
 *
 * Revision History
 *   01/27/2004 Cheryl M. Tokaraki (CSS, LLC) - Adapted the original fnMMCCancelGroup
 *												to act on the specified MCP.  fnMMCCancelGroup
 *												has been modified to call this function with the 
 *												MMC MCPId.
 */
void fnMCPCancelGroup(void *pMcp, unsigned char profileId)
{
	MMCProfileGroup *profileGroup;
	unsigned char profileData[MMC_PROFILE_DATA_LEN];
	
	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	
	if (pMcp == NULL)
		return;

	MCPLIB_ASSERT(pMcp,((MCP_INSTANCE_t*)pMcp)->fInitComplete);	//lint !e506
	if (!((MCP_INSTANCE_t*)pMcp)->fInitComplete)
		return;

	/* determine if the specified profileId represents a Profile Group */
	if (profileId > MAX_PROFILE_ID)
	{

		/* find the Profile Group, and start it */
		if ((profileGroup = 
			   fnMMCIlocateProfileGroup(pMcp, profileId)) == (MMCProfileGroup *)NULL)
		{
			/* we were unable to locate the Profile Group; just return */
			return;
		}

		/* Issue the request to cancel the profile group; note that the
		 * 'Cancel Group' command is used instead of the 'Cancel Profile'
		 * command
		 */
		fnMCPSendRequest(pMcp,
						 profileGroup->cancelProfiles, profileGroup->cancelSize,
						 MMC_CANCEL_GROUP_REQ,	
						 MMC_NO_RESPONSE,NULL);
	}
	else
	{
		/* format the buffer to send containing the Profile ID and 
		 * Interations 
		 */
		profileData[MMC_PROFILE_OFFSET] = profileId;
		profileData[MMC_PROFILE_OFFSET + 1] = 0xFF;

		fnMCPSendRequest(pMcp,
						profileData, MMC_CANCEL_PROFILE_DATA_LEN +1,
						MMC_CANCEL_PROFILE_REQ, MMC_NO_RESPONSE, NULL);
	}
}

/*
 * Name
 *   fnMCPSetRemoteFlag
 *
 * Description
 *   This function sets an MCP flag to either MMC_FLAG_SET or MMC_FLAG_CLEAR.
 *
 * Inputs
 *   mcpID      - ID of the MCP to which we should send the message
 *   mcpFlagID  - MCP Flag ID to be operated upon
 *   mcpFlagVal - value to set the MCP Flag ID to
 *
 * Returns
 *   Nothing.
 *
 * Caveats
 *   This function changes the value of a Flag within the MCP, changing the
 *     behavior of the MCP during operation.
 *
 *   There is no response message expected.
 *
 * Revision History
 *   05/11/2001 Joseph P. Tokarski (CSS, LLC) - Initial Revision.
 *
 *   01/27/2004 Cheryl M. Tokaraki (CSS, LLC) - Adapted the original fnMMCSetRemoteFlag
 *												to act on the specified MCP.  fnMMCSetRemoteFlag
 *												has been modified to call this function with the 
 *												MMC MCPId.
 */
void fnMCPSetRemoteFlag(void *pMcp,
							   unsigned char mcpFlagID,
							   unsigned char mcpFlagVal)
{
	static unsigned char mcpFlagData[MMC_FLAG_DATA_LEN];

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	
	if (pMcp == NULL)
		return;

	MCPLIB_ASSERT(pMcp,((MCP_INSTANCE_t*)pMcp)->fInitComplete);	//lint !e506
	if (!((MCP_INSTANCE_t*)pMcp)->fInitComplete)
		return;

	/* load the flag data in the data array to be issued */
	mcpFlagData[MMC_FLAG_ID_OFFSET]  = mcpFlagID;
	mcpFlagData[MMC_FLAG_VAL_OFFSET] = mcpFlagVal;
	mcpFlagData[MMC_FLAG_TERM_OFFSET] = MMC_FLAG_TERMINATOR;

	/* issue the request to set the MCP 'Remote' Flag */
	fnMCPSendRequest(pMcp,
					 &mcpFlagData[0], MMC_FLAG_DATA_LEN,
					 MMC_SET_REMOTE_FLAG_REQ, MMC_NO_RESPONSE, NULL);
}

void fnMCPSetCounterValue(void *pMcp, const unsigned char bCounterId, const unsigned short wCounterValue)
{
	static unsigned char mmcCounterData[MMC_COUNTER_DATA_LEN];

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	
	if (pMcp == NULL)
		return;

	MCPLIB_ASSERT(pMcp,((MCP_INSTANCE_t*)pMcp)->fInitComplete);	//lint !e506
	if (!((MCP_INSTANCE_t*)pMcp)->fInitComplete)
		return;
	
    /* load the flag data in the data array to be issued */
    mmcCounterData[MMC_COUNTER_ID_OFFSET]  = bCounterId;

    memcpy((void *)&mmcCounterData[MMC_COUNTER_VAL_OFFSET],
            (void *)&wCounterValue, sizeof(wCounterValue));


    /* issue the request to set the MMC 'Remote' Flag */
	fnMCPSendRequest(pMcp,
					 &mmcCounterData[0], MMC_COUNTER_DATA_LEN,
					 MMC_SET_COUNTER_REQ, MMC_NO_RESPONSE, NULL);
}

/*
 * Name
 *   fnMCPSetMultipleRemoteFlags
 *
 * Description
 *   This function performs the same operation as the function
 *   'fnMMCSetRemoteFlag' except that it operates on several remote
 *   flags within a single message.  This function formats the buffer
 *   to be issued to the MMC using the flag array and the flag count
 *   to load the flags and values into the message, ensuring that a
 *   terminator is appended.
 *
 * Inputs
 *   flagArray - pointer to an unsigned character array containing the
 *                 flagId and flagValue to be set
 *   flagCount - number of flags present in the array
 *
 * Returns
 *   Nothing.
 *
 * Caveats
 *   This function changes the value of a Flag within the MMC, changing the
 *     behavior of the MMC during operation.
 *
 *   There is no response message expected.
 *
 * Revision History
 *   09/20/2002 Joseph P. Tokarski - Initial Revision.
 *
 *   01/28/2004 Cheryl M. Tokarski - Right now this function is only called for setting flags
 *									 for the MMC.  If this changes, we will need to indicate the 
 *									 destination MCP id
 * 
 *   06/07/2004 Cheryl M. Tokarski - Adapted the original fnMMCSetMultipleRemoteFlags
 *									 to act on the specified MCP.  fnMMCSetMultipleRemoteFlags
 *									 has been modified to call this function with the 
 *									 MMC MCPId.
 */
void fnMCPSetMultipleRemoteFlags(void *pMcp,
										unsigned char *flagArray,
										unsigned char  flagCount)
{
	static unsigned char mmcFlagData[MMC_MAX_MSG_LEN];
	unsigned char i;
	unsigned char *curFlag;
	unsigned char *curFlagEntry;

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	
	if (pMcp == NULL)
		return;

	MCPLIB_ASSERT(pMcp,((MCP_INSTANCE_t*)pMcp)->fInitComplete);	//lint !e506
	if (!((MCP_INSTANCE_t*)pMcp)->fInitComplete)
		return;

	curFlag = flagArray;
	curFlagEntry = &mmcFlagData[0];

	// load the flags and values
	for (i = 0; i < flagCount; i++)
	{
		// load the flag ID
		*curFlagEntry++ = *curFlag++;

		// load the flag value
		*curFlagEntry++ = *curFlag++;
	}

	// curFlagEntry points to the next place to write into
	*curFlagEntry = MMC_FLAG_TERMINATOR;


	// NOTE: the message size '(flagCount * 2) + 1' specifies the number
	//       of payload bytes - 1 for each flagId, 1 for each flag value
	//       (flagCount * 2), and 1 for the Flag Terminator
	//
	/* issue the request to set the MMC 'Remote' Flags */
	fnMCPSendRequest(pMcp,
					 (const unsigned char*)&mmcFlagData[0], (flagCount * 2) + 1, 
					 MMC_SET_REMOTE_FLAG_REQ, MMC_NO_RESPONSE, NULL);

}

void fnMCPResetAllFlags(void *pMcp)
{
	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	
	if (pMcp == NULL)
		return;

	MCPLIB_ASSERT(pMcp,((MCP_INSTANCE_t*)pMcp)->fInitComplete);	//lint !e506
	if (!((MCP_INSTANCE_t*)pMcp)->fInitComplete)
		return;

	fnMCPSendSimpleRequest(pMcp, MMC_RESET_ALL_FLAGS_REQ,
								   MMC_NO_RESPONSE,NULL);
}

void fnMCPStopWaitingForRsp(void *pMcp)
{
}

void fnMCPSetUnsolicitedMsgCb(void *pMcp, MCP_UNSOLICITED_MSG_CB_t pfcnUnsolicitedMsgCb)
{
	MCP_INSTANCE_t *pMcpInst = NULL;
	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	
	if (pMcp == NULL)
		return;

	pMcpInst = (MCP_INSTANCE_t*)pMcp;

	MCPLIB_ASSERT(pMcpInst,pMcpInst->fInitComplete);	//lint !e506
	if (!pMcpInst->fInitComplete)
		return;

	pMcpInst->pfcnMcpUnsolicitedMsgCb = pfcnUnsolicitedMsgCb;
}
static unsigned long fnMCPLibSaveConfigInfo(MCP_INSTANCE_t *pMcpInst, unsigned char *pPtr, unsigned long lwLength)
{
	unsigned char tmpVers[MMC_MAX_VERSION_INFO_LEN];
    unsigned char *lclptr;
	char *pRest = NULL;
	pPtr += MMC_MSG_HEADER_LEN;
	lwLength -= MMC_MSG_HEADER_LEN;
	if (lwLength > 0)
	{
		(void)strcpy((char*)tmpVers,(char*)pPtr);

	    // parse the version string, using spaces as the delimiting character
	    // the first 'strtok' operation should replace the first space with a
	    // NULL character, leaving the string 'Vers'; we need the second component
	    // which will be the version number, e.g., "04.02.00"
	    //
	    lclptr = (unsigned char *)strtok_r((char *)tmpVers, " ", &pRest);
	    lclptr = (unsigned char *)strtok_r((char *)NULL, " ", &pRest);

	    // versionLoc should now contain the numeric version string embedded within
	    // the MMCVersion string
	    //
	    (void)strcpy((char *)pMcpInst->bMcpVersion, (const char *)lclptr); //lint !e668 - Joe/Gary/Cheryl should know what they are doing.

		return MCP_LIB_SUCCESS;
	}
	else
		return MCP_LIB_MSG_FORMAT_ERR;	
}

static unsigned long fnMCPLibSavePCNInfo(MCP_INSTANCE_t *pMcpInst, unsigned char *pPtr, unsigned long lwLength)
{	
	pPtr += MMC_MSG_HEADER_LEN;
	lwLength -= MMC_MSG_HEADER_LEN;
	if (lwLength >= (SMR_OFFSET + MMC_SMR_LEN + 1))
	{
		pPtr++;	// skip the nvm type
		(void)memset((void*)&pMcpInst->pcnInfo,0,sizeof(pMcpInst->pcnInfo));
		(void)strncpy((char*)pMcpInst->pcnInfo.szModel,(char *)&pPtr[MODEL_NUM_OFFSET], MMC_MODEL_NUM_LEN);
		(void)strncpy((char*)pMcpInst->pcnInfo.szPCN,(char *)&pPtr[PCN_OFFSET], MMC_PCN_LEN);
		(void)strncpy((char*)pMcpInst->pcnInfo.szSerialNumber,(char *)&pPtr[SERIAL_NUM_OFFSET], MMC_SERIAL_NUMBER_LEN);
		(void)strncpy((char*)pMcpInst->pcnInfo.szSMR,(char *)&pPtr[SMR_OFFSET], MMC_SMR_LEN);
		return MCP_LIB_SUCCESS;
	}
	else
		return MCP_LIB_MSG_FORMAT_ERR;
}

static unsigned long fnMCPLibSaveNVMMachineInfo(MCP_INSTANCE_t *pMcpInst, unsigned char *pPtr, unsigned long lwLength)
{
	pPtr += MMC_MSG_HEADER_LEN;
	lwLength -= MMC_MSG_HEADER_LEN;
	if (lwLength >= (SMR_OFFSET + MMC_SMR_LEN + 1))
	{
		(void)memcpy((void*)&(pMcpInst->wowNvmInfo),pPtr,sizeof(MMC_WOW_NVM_MACHINE_INFO));
		return MCP_LIB_SUCCESS;
	}
	else
		return MCP_LIB_MSG_FORMAT_ERR;
}

static unsigned long fnMCPLibExecuteNextInitStep(MCP_INSTANCE_t *pMcpInst)
{
	MCPLIB_ASSERT(pMcpInst,pMcpInst->lwState == MCP_LIB_INITIALIZING_STATE);	//lint !e506
	if (fnMCPLibIsInitComplete(pMcpInst))
	{
		MCPLIB_ASSERT(pMcpInst,pMcpInst->pfcnInitComplCb);	//lint !e506
		fnMCPLibSetState(pMcpInst,MCP_LIB_NO_STATE);
		pMcpInst->fInitComplete = TRUE;
		if (pMcpInst->pfcnInitComplCb)
		{
			pMcpInst->pfcnInitComplCb((void*)pMcpInst,MCP_LIB_SUCCESS);
			return MCP_LIB_SUCCESS;
		}
		else
			return MCP_LIB_INVALID_PARAM;
	}
	else
	{
		return fcnMCPInitSteps[pMcpInst->lwCurrInitStep++](pMcpInst);
	}
}
static BOOL fnMCPLibIsInitComplete(const MCP_INSTANCE_t *pMcpInst)
{
	return (pMcpInst->lwCurrInitStep == MCP_NUM_INIT_STEPS ? TRUE : FALSE);
}
static unsigned long fnMCPLibGetInitResult(const MCP_INSTANCE_t *pMcpInst)
{
	return pMcpInst->lwInitStatus;
}
static void fnMCPLibSetInitResult(MCP_INSTANCE_t *pMcpInst, const unsigned long lwStatus)
{
	pMcpInst->lwInitStatus = lwStatus;
}

static unsigned long fnMCPLibRequestConfigInfo(const MCP_INSTANCE_t *pMcpInst)
{
	STATUS status = NU_SUCCESS;
	unsigned long lwStatus = MCP_LIB_SUCCESS;
	unsigned char mcpMsg[MMC_MJ_SIMPLE_MSG_LEN] = {0};

	/* construct the 'simple' message */
	mcpMsg[MMC_MSG_CLASS_OFFSET]   = MOTION_CLASS_CODE;
	mcpMsg[MMC_MSG_SOURCE_OFFSET]  = pMcpInst->bTaskId;
	mcpMsg[MMC_MSG_DEST_OFFSET]    = pMcpInst->bMcpId;
	mcpMsg[MMC_MSG_COMMAND_OFFSET] = MMC_CONFIGURATION_REQ;

	status = fnMCPSendMsg(mcpMsg,MMC_MJ_SIMPLE_MSG_LEN);
	if (status != NU_SUCCESS)
	{
		lwStatus = MCP_LIB_SEND_MSG_ERR;	
	}

	return lwStatus;
}

static unsigned long fnMCPLibRequestPcnInfo(const MCP_INSTANCE_t *pMcpInst)
{
	STATUS status = NU_SUCCESS;	
	unsigned char mcpMsg[MMC_MJ_SIMPLE_MSG_LEN] = {0};

	/* construct the 'simple' message */
	mcpMsg[MMC_MSG_CLASS_OFFSET]   = MOTION_CLASS_CODE;
	mcpMsg[MMC_MSG_SOURCE_OFFSET]  = pMcpInst->bTaskId;
	mcpMsg[MMC_MSG_DEST_OFFSET]    = pMcpInst->bMcpId;
	mcpMsg[MMC_MSG_COMMAND_OFFSET] = MMC_NVM_PCN_INFO_REQ;

	/* send the message */
	status = fnMCPSendMsg(mcpMsg,MMC_MJ_SIMPLE_MSG_LEN);
	if (status == NU_SUCCESS)
	{
		return MCP_LIB_SUCCESS;
	}
	else
	{
		return MCP_LIB_SEND_MSG_ERR;
	}
}

static unsigned long fnMCPLibRequestStopAndResetFlags(MCP_INSTANCE_t *pMcpInst)
{
	STATUS status = NU_SUCCESS;	
	unsigned long lwStatus = MCP_LIB_SUCCESS;
	unsigned char mcpMsg[MMC_MJ_SIMPLE_MSG_LEN] = {0};

	/* construct the 'simple' message */
	mcpMsg[MMC_MSG_CLASS_OFFSET]   = MOTION_CLASS_CODE;
	mcpMsg[MMC_MSG_SOURCE_OFFSET]  = pMcpInst->bTaskId;
	mcpMsg[MMC_MSG_DEST_OFFSET]    = pMcpInst->bMcpId;
	mcpMsg[MMC_MSG_COMMAND_OFFSET] = MMC_EMERGENCY_STOP_REQ;

	/* send the message */
	status = fnMCPSendMsg(mcpMsg,MMC_MJ_SIMPLE_MSG_LEN);
	if (status != NU_SUCCESS)
	{
		return MCP_LIB_SEND_MSG_ERR;
	}

	/* construct the 'simple' message */
	mcpMsg[MMC_MSG_CLASS_OFFSET]   = MOTION_CLASS_CODE;
	mcpMsg[MMC_MSG_SOURCE_OFFSET]  = pMcpInst->bTaskId;
	mcpMsg[MMC_MSG_DEST_OFFSET]    = pMcpInst->bMcpId;
	mcpMsg[MMC_MSG_COMMAND_OFFSET] = MMC_RESET_ALL_FLAGS_REQ;

	/* send the message */
	status = fnMCPSendMsg(mcpMsg,MMC_MJ_SIMPLE_MSG_LEN);
	if (status != NU_SUCCESS)
	{
		return MCP_LIB_SEND_MSG_ERR;
	}	

	lwStatus = fnMCPLibExecuteNextInitStep(pMcpInst);

	return lwStatus;
}

static unsigned long fnMCPLibRequestNVMInfo(MCP_INSTANCE_t *pMcpInst)
{
	STATUS status = NU_SUCCESS;	
	unsigned long lwStatus = MCP_LIB_SUCCESS;
	unsigned char mcpMsg[MMC_MJ_SIMPLE_MSG_LEN] = {0};

	/* construct the 'simple' message */
	mcpMsg[MMC_MSG_CLASS_OFFSET]   = MOTION_CLASS_CODE;
	mcpMsg[MMC_MSG_SOURCE_OFFSET]  = pMcpInst->bTaskId;
	mcpMsg[MMC_MSG_DEST_OFFSET]    = pMcpInst->bMcpId;
	mcpMsg[MMC_MSG_COMMAND_OFFSET] = MMC_NVM_MACHINE_INFO_RECORD_REQ;

	/* send the message */
	status = fnMCPSendMsg(mcpMsg,MMC_MJ_SIMPLE_MSG_LEN);
	if (status != NU_SUCCESS)
	{
		return MCP_LIB_SEND_MSG_ERR;
	}	
}

static void fnMCPLibHandleSensorLogDataRsp(MCP_INSTANCE_t *pMcpInst, unsigned char *pPtr, unsigned long lwLength)
{
	if (lwLength > 7)
	{
		pPtr += MMC_MSG_PAYLOAD_OFFSET;
		lwLength -= MMC_MSG_PAYLOAD_OFFSET;

		if (pMcpInst->fContinuousLogging)
		{
			unsigned short wSensorLogNumRecs = 0;
			unsigned char *pLogEntry = NULL;
			unsigned char bSensorLogRecSize = 0;
			bSensorLogRecSize = *pPtr++;
			(void)memcpy(&wSensorLogNumRecs,pPtr,sizeof(unsigned short));
			pPtr+=2;
			lwLength-=3;
			if (!pMcpInst->pSensorLogBuff)
			{
				pMcpInst->bSensorLogRecSize = bSensorLogRecSize;								
				pMcpInst->pSensorLogBuff = (unsigned char*)calloc(1, 
											pMcpInst->bSensorLogRecSize*MCP_NUM_SENSOR_LOG_ENTRIES);
				if (pMcpInst->pSensorLogBuff)
				{
					pMcpInst->lwSensorLogIndx = 0;
					pMcpInst->lwSensorLogCount = 0;
				}
			}
			if (pMcpInst->pSensorLogBuff)
			{
				while(wSensorLogNumRecs-- > 0)
				{
					pLogEntry = pMcpInst->pSensorLogBuff + (pMcpInst->lwSensorLogIndx * pMcpInst->bSensorLogRecSize);
					(void)memcpy((void*)pLogEntry,pPtr,pMcpInst->bSensorLogRecSize);
					pPtr += pMcpInst->bSensorLogRecSize;
					pMcpInst->lwSensorLogIndx = (pMcpInst->lwSensorLogIndx+1)%MCP_NUM_SENSOR_LOG_ENTRIES;				
					if (pMcpInst->lwSensorLogCount < MCP_NUM_SENSOR_LOG_ENTRIES)
						pMcpInst->lwSensorLogCount++;
				}
			}
		}
	}
}
static void fnMCPLibHandleProfileHistLogDataRsp(MCP_INSTANCE_t *pMcpInst, unsigned char *pPtr, unsigned long lwLength)
{
	if (lwLength > 7)
	{
		pPtr += MMC_MSG_PAYLOAD_OFFSET;
		lwLength -= MMC_MSG_PAYLOAD_OFFSET;

		if (pMcpInst->fContinuousLogging)
		{
			unsigned short wTraceLogNumRecs = 0;
			unsigned char *pLogEntry = NULL;
			unsigned char bTraceLogRecSize = 0;
			bTraceLogRecSize = *pPtr++;
			(void)memcpy(&wTraceLogNumRecs,pPtr,sizeof(unsigned short));
			pPtr+=2;
			lwLength-=3;
			if (!pMcpInst->pProfileHistLogBuff)
			{
				pMcpInst->bProfileHistLogRecSize = bTraceLogRecSize;
				pMcpInst->pProfileHistLogBuff = (unsigned char*)calloc(1, 
											pMcpInst->bProfileHistLogRecSize*MCP_NUM_PROF_HIST_LOG_ENTRIES);
				if (pMcpInst->pProfileHistLogBuff)
				{
					pMcpInst->lwProfileHistLogIndx = 0;
					pMcpInst->lwProfileHistLogCount = 0;
				}
			}
			if (pMcpInst->pProfileHistLogBuff)
			{
				while(wTraceLogNumRecs-- > 0)
				{
					pLogEntry = pMcpInst->pProfileHistLogBuff + (pMcpInst->lwProfileHistLogIndx * pMcpInst->bProfileHistLogRecSize);
					(void)memcpy((void*)pLogEntry,pPtr,pMcpInst->bProfileHistLogRecSize);
					pPtr += pMcpInst->bProfileHistLogRecSize;
					pMcpInst->lwProfileHistLogIndx = (pMcpInst->lwProfileHistLogIndx+1)%MCP_NUM_PROF_HIST_LOG_ENTRIES;				
					if (pMcpInst->lwProfileHistLogCount < MCP_NUM_PROF_HIST_LOG_ENTRIES)
						pMcpInst->lwProfileHistLogCount++;
				}
			}
		}
	}
}

unsigned long fnMCPGetSensorLog(void* pMcp, MCP_LOG_DATA_REQ_COMBPLETE_CB_t pfcnLogDataReqCb)
{
	MCP_INSTANCE_t *pMcpInst = NULL;
	STATUS status = NU_SUCCESS;	
	unsigned char mcpMsg[MMC_MJ_SIMPLE_MSG_LEN + 1] = {0};
	
	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	if (pMcp==NULL)
		return MCP_LIB_INVALID_PARAM;
	pMcpInst = (MCP_INSTANCE_t*)pMcp;
	MCPLIB_ASSERT(pMcp,pMcpInst->lwState == MCP_LIB_NO_STATE);	//lint !e506
	if (pMcpInst->lwState != MCP_LIB_NO_STATE)
		return MCP_LIB_INVALID_OP_IN_CURR_STATE;
	MCPLIB_ASSERT(pMcpInst,pMcpInst->fInitComplete);	//lint !e506
	if (!pMcpInst->fInitComplete)
		return MCP_LIB_INVALID_OP_IN_CURR_STATE;

	fnMCPLibSetState(pMcpInst,MCP_LIB_UPLOADING_LOG_DATA_STATE);

	pMcpInst->pfcnLogDataComplCb = pfcnLogDataReqCb;
	
	/* construct the 'simple' message */
	mcpMsg[MMC_MSG_CLASS_OFFSET]   = MOTION_CLASS_CODE;
	mcpMsg[MMC_MSG_SOURCE_OFFSET]  = pMcpInst->bTaskId;
	mcpMsg[MMC_MSG_DEST_OFFSET]    = pMcpInst->bMcpId;
	mcpMsg[MMC_MSG_COMMAND_OFFSET] = MMC_EXAMINE_SENSOR_LOG_REQ;
	mcpMsg[MMC_MSG_PAYLOAD_OFFSET] = pMcpInst->fContinuousLogging;	// set continuous logging param


	/* send the message */
	status = fnMCPSendMsg(mcpMsg,sizeof(mcpMsg));
	if (status == NU_SUCCESS)
	{
		return MCP_LIB_SUCCESS;
	}
	else
	{
		return MCP_LIB_SEND_MSG_ERR;
	}
}
unsigned long fnMCPGetProfileHistoryLog(void* pMcp, MCP_LOG_DATA_REQ_COMBPLETE_CB_t pfcnLogDataReqCb)
{
	MCP_INSTANCE_t *pMcpInst = NULL;
	STATUS status = NU_SUCCESS;	
	unsigned char mcpMsg[MMC_MJ_SIMPLE_MSG_LEN+1] = {0};
	
	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	if (pMcp==NULL)
		return MCP_LIB_INVALID_PARAM;
	pMcpInst = (MCP_INSTANCE_t*)pMcp;
	MCPLIB_ASSERT(pMcp,pMcpInst->lwState == MCP_LIB_NO_STATE);	//lint !e506
	if (pMcpInst->lwState != MCP_LIB_NO_STATE)
		return MCP_LIB_INVALID_OP_IN_CURR_STATE;
	MCPLIB_ASSERT(pMcpInst,pMcpInst->fInitComplete);	//lint !e506
	if (!pMcpInst->fInitComplete)
		return MCP_LIB_INVALID_OP_IN_CURR_STATE;

	fnMCPLibSetState(pMcpInst,MCP_LIB_UPLOADING_LOG_DATA_STATE);

	pMcpInst->pfcnLogDataComplCb = pfcnLogDataReqCb;
	
	/* construct the 'simple' message */
	mcpMsg[MMC_MSG_CLASS_OFFSET]   = MOTION_CLASS_CODE;
	mcpMsg[MMC_MSG_SOURCE_OFFSET]  = pMcpInst->bTaskId;
	mcpMsg[MMC_MSG_DEST_OFFSET]    = pMcpInst->bMcpId;
	mcpMsg[MMC_MSG_COMMAND_OFFSET] = MMC_GET_PROFILE_HIST_LOG_REQ;
	mcpMsg[MMC_MSG_PAYLOAD_OFFSET] = pMcpInst->fContinuousLogging;	// set continuous logging param

	/* send the message */
	status = fnMCPSendMsg(mcpMsg,sizeof(mcpMsg));
	if (status == NU_SUCCESS)
	{
		return MCP_LIB_SUCCESS;
	}
	else
	{
		return MCP_LIB_SEND_MSG_ERR;
	}
}

static void fnMCPHandleMotionClassMsg(void *pMcp, const INTERTASK *pMsg)
{
	MCP_INSTANCE_t *pMcpInst = NULL;
	MCP_PROFILE_DNLD_COMPLETE_CB_t pfcnDnldCb = NULL;
	MCP_MSG_RSP_CB_t pfcnMsgRspCb = NULL;
	MCP_LOG_DATA_REQ_COMBPLETE_CB_t pfcnLogDataCb = NULL;
	unsigned char bMsg = 0;
	unsigned char bPollRspData = 0;
	unsigned char bDest = 0;
	unsigned char *pPtr = NULL;
	unsigned long lwLength = 0;
	unsigned long lwStatus = MCP_LIB_SUCCESS;
	int destination;

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	MCPLIB_ASSERT(pMcp,pMsg!=NULL);	//lint !e506

	if (pMcp==NULL || pMsg==NULL)
		return;

	pMcpInst = (MCP_INSTANCE_t*)pMcp;

	fnMCPLibGetMotionClassMsgPointerAndLength((void**)&pPtr,&lwLength,pMsg);	//lint !e605
	
	MCPLIB_ASSERT(pMcp,pPtr);	//lint !e506

	if (!pPtr)
		return;

	bDest = pPtr[MMC_MSG_DEST_OFFSET];
	bMsg = pPtr[MMC_MSG_COMMAND_OFFSET];

	// handle these messages all the time!
	if (bMsg == MMC_SENSOR_LOG_DATA_RSP)
	{
		fnMCPLibHandleSensorLogDataRsp(pMcpInst, pPtr, lwLength);
	}
	else if (bMsg == MMC_PROFILE_HIST_DATA_RSP)
	{
		fnMCPLibHandleProfileHistLogDataRsp(pMcpInst, pPtr, lwLength);
	}
	else if (bMsg == MMC_DATA_LOG_RSP)
	{
	}


	switch (pMcpInst->lwState) {
	case MCP_LIB_WAIT_RSP_STATE:
		if (bMsg == pMcpInst->bWaitRspMsg)
		{
			pfcnMsgRspCb = pMcpInst->pfcnMsgRspCb;
			fnMCPLibSetState(pMcpInst,MCP_LIB_NO_STATE);
			if (pfcnMsgRspCb)
			{
				pfcnMsgRspCb = pMcpInst->pfcnMsgRspCb;				
				pMcpInst->pfcnMsgRspCb = NULL;	// TODO: this must also be cleared when the timeout occurs
				pfcnMsgRspCb(pMcp,MCP_LIB_SUCCESS,pPtr,lwLength);
			}					
		}
		else
		{
			if (pMcpInst->pfcnMcpUnsolicitedMsgCb)
			{
				pMcpInst->pfcnMcpUnsolicitedMsgCb(pMcp,pPtr,lwLength);
			}
		}
		break;
	case MCP_LIB_NO_STATE:
		if (pMcpInst->pfcnMcpUnsolicitedMsgCb)
		{
			pMcpInst->pfcnMcpUnsolicitedMsgCb(pMcp,pPtr,lwLength);
		}
		break;
	case MCP_LIB_UPLOADING_LOG_DATA_STATE:
		switch (bMsg) {
		case MMC_SENSOR_LOG_DATA_RSP:
			pfcnLogDataCb = pMcpInst->pfcnLogDataComplCb;
			fnMCPLibSetState(pMcpInst,MCP_LIB_NO_STATE);
			if (pfcnLogDataCb)
			{
				pfcnLogDataCb(pMcp,MCP_LIB_SUCCESS,pMcpInst->pSensorLogBuff,pMcpInst->bSensorLogRecSize*MCP_NUM_SENSOR_LOG_ENTRIES);
			}		
			break;
		case MMC_PROFILE_HIST_DATA_RSP:
			pfcnLogDataCb = pMcpInst->pfcnLogDataComplCb;
			fnMCPLibSetState(pMcpInst,MCP_LIB_NO_STATE);
			if (pfcnLogDataCb)
			{
				pfcnLogDataCb(pMcp,MCP_LIB_SUCCESS,pMcpInst->pProfileHistLogBuff,pMcpInst->bProfileHistLogRecSize*MCP_NUM_PROF_HIST_LOG_ENTRIES);
			}
			break;
		};		
		break;
	case MCP_LIB_INITIALIZING_STATE:
		if (bMsg == MMC_CONFIGURATION_RSP)
		{
			(void)fnMCPLibSaveConfigInfo(pMcpInst,pPtr,lwLength);
			lwStatus = fnMCPLibExecuteNextInitStep(pMcpInst);
		}
		else if (bMsg == MMC_NVM_PCN_INFO_RSP)
		{
			(void)fnMCPLibSavePCNInfo(pMcpInst,pPtr,lwLength);
			lwStatus = fnMCPLibExecuteNextInitStep(pMcpInst);			
		}
		else if(bMsg == MMC_NVM_MACHINE_INFO_RECORD_RSP)
		{
			(void)fnMCPLibSaveNVMMachineInfo(pMcpInst,pPtr,lwLength);
			lwStatus = fnMCPLibExecuteNextInitStep(pMcpInst);	
		}
		break;
	default:
		break;
	};



}
void fnMCPLibGetMotionClassMsgPointerAndLength(void **ptr, unsigned long *lwLength, void *pMsg)
{
	GetIntertaskPointerAndLength(ptr,lwLength,pMsg);
	if (((INTERTASK *)pMsg)->bMsgId == COMM_RECEIVE_MESSAGE)
	{
		PB232BUFFER *pPB232Ptr = (PB232BUFFER*)(*ptr);
		*ptr = (void*)pPB232Ptr->bData;
		*lwLength = (unsigned long)pPB232Ptr->wLength;
	}
}

static STATUS fnMCPSendMsg(const unsigned char *pData, const unsigned long lwLength)
{
	STATUS status = NU_SUCCESS;

//TODO - send intertask message

	return status;
}
static void fnMCPHandleBootModeEnumeration(void *pMcp, const INTERTASK *pMsg)
{
	MCP_INSTANCE_t *pMcpInst = (MCP_INSTANCE_t*)pMcp;
	UINT8	bHardwareMajorVersion;
	UINT8	bDontCare;

	pMcpInst->fInBootMode = TRUE;
	if (pMcpInst->fPerformFirmwareUpdate)
	{
		pMcpInst->fPerformFirmwareUpdate = FALSE; 
		fnMCPLibSetState(pMcp,MCP_LIB_FIRMWARE_UPDATING_STATE);
			// attfp
		// choose the driver to use for the firmware update depending on the hardware version of the
		//	main board.  we may want to consider looking at the attached MCP instead
		fnGetHardwareVersion(&bHardwareMajorVersion, &bDontCare);
/*
		if (bHardwareMajorVersion <= FP_MAIN_BD_HW_VERSION1)
			NU_USBH_H8S_USB_BOOT_MODE_DRVR_DownloadFeederFirmware((const char*)pMcpInst->firmwareUpdateData.bFirmwareImageFileName);
		else
			NU_USBH_1663_USB_BOOT_MODE_DRVR_DownloadFeederFirmware((const char*)pMcpInst->firmwareUpdateData.bFirmwareImageFileName);
*/
	}
}
static void fnMCPHandleBootModeDeEnumeration(void *pMcp, const INTERTASK *pMsg)
{
	MCP_INSTANCE_t *pMcpInst = (MCP_INSTANCE_t*)pMcp;
	pMcpInst->fInBootMode = FALSE;
	if (pMcpInst->fPerformFirmwareUpdate)
	{
		pMcpInst->fPerformFirmwareUpdate = FALSE;
		fnMCPLibSetState(pMcpInst,MCP_LIB_NO_STATE); 
	}
}

static void fnMCPHandleFirmwareUpdateComplete(void *pMcp, const INTERTASK *pMsg)
{
	MCP_INSTANCE_t *pMcpInst = (MCP_INSTANCE_t*)pMcp;		
	pMcpInst->fInBootMode = FALSE;
	pMcpInst->lwLastFirmwareUpdateStatus = pMsg->IntertaskUnion.lwLongData[0];
	fnMCPLibSetState(pMcp,MCP_LIB_FIRMWARE_UPDATE_WAIT_DEV_REENUM_STATE);
	pMcpInst->firmwareUpdateData.fcnExitBootModeCb();
}
static void fnMCPHandleMcpUsbEvent(void *pMcp, const INTERTASK *pMsg)
{
	unsigned char *pPtr=NULL;
	unsigned long lwLength = 0;
	MCP_INSTANCE_t *pMcpInst = (MCP_INSTANCE_t*)pMcp;

	fnMCPLibGetMotionClassMsgPointerAndLength((void**)&pPtr,&lwLength,pMsg);	//lint !e605

	MCPLIB_ASSERT(pMcp,pPtr!=NULL);	//lint !e506
	if (pPtr == NULL)
		return;

	if (pPtr[0] == MCP_CONNECTED
		&& pMcpInst->lwState == MCP_LIB_FIRMWARE_UPDATE_WAIT_DEV_REENUM_STATE)
	{	
		fnMCPLibSetState(pMcp,MCP_LIB_NO_STATE);
		pMcpInst->firmwareUpdateData.fcnUpdateCompleteCb(pMcpInst->lwLastFirmwareUpdateStatus);
	}
}
void fnMCPHandleUSBDriverMsg(void *pMcp, const INTERTASK *pMsg)
{
	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	MCPLIB_ASSERT(pMcp,pMsg!=NULL);	//lint !e506

	if (pMcp==NULL || pMsg==NULL)
		return;

	if (pMsg->bSource == MCP_CLASS_DRVR && pMsg->bMsgId == COMM_RECEIVE_MESSAGE)
		fnMCPHandleMotionClassMsg(pMcp, pMsg);				  
	else if (pMsg->bSource == USB_DIRECT_CLASS_DRVR && 
			pMsg->bMsgId == USB_BUS_CHANGE_EVENT &&
			pMsg->IntertaskUnion.bByteData[0] == MCP_USB_DIRECT_DEV_CONNECT)
		fnMCPHandleBootModeEnumeration(pMcp,pMsg);
	else if (pMsg->bSource == USB_DIRECT_CLASS_DRVR && 
			pMsg->bMsgId == USB_BUS_CHANGE_EVENT &&
			pMsg->IntertaskUnion.bByteData[0] == MCP_USB_DIRECT_DEV_DISCONNECT)
		fnMCPHandleBootModeDeEnumeration(pMcp,pMsg);
	else if (pMsg->bSource == USB_DIRECT_CLASS_DRVR && 
			pMsg->bMsgId == MCP_USB_DIRECT_DEV_UPDATE_COMPLETE)
		fnMCPHandleFirmwareUpdateComplete(pMcp,pMsg);
	else if (pMsg->bSource == MCP_CLASS_DRVR && 
		pMsg->bMsgId == USB_BUS_CHANGE_EVENT)
		fnMCPHandleMcpUsbEvent(pMcp,pMsg);
}

unsigned short mcpCurSendPosIndx;
unsigned short mcpCurRecvPosIndx;

unsigned short mcpCurTraceRefId;

static const char pclGotoRowCol[]="\33&a%uR\33&a%uC";

static const char pclReset[] = {
	0x1b,'E',					// Reset Printer
	0x1b,'&', 'l','0','H',		// Paper source is default (current page)
	0x1b,'&', 'l','0','O',		// Print in portrait mode
	0x00
};

static const char pclSetUpRasterGraphics[]={
	0x1b,'E',					// Reset Printer
	0x1b,'&', 'l','0','H',		// Paper source is default (current page)
	0x1b,'&', 'l','0','O',		// Print in portrait mode
	0x1b,'&', 'l','1','X',		// One copy
	0x1b,'*','r','0','F',			// Orientation is logical page
	0x1b,'*','t','7','5','R',		// Resolution is 75 dpi
	0x1b,'*','r','2','4','0','T',	// Raster height is 240
	0x1b,'*','r','3','2','0','S',	// Raster width is 320
	0x1b,'*','r','0','A',0x00			// Start raster graphics
};

static const char pclTransferRasterData[] = { // send as preamble for each row of 320 bits
	0x1b,'*','b','4','0','W',0x00		// Send 40 bytes per row
};

static const char pclEndRasterGraphics[] = {
	0x1b,'*','r','C',0x0c,0x00			// End Raster Graphics and form feed
};

static const char pclLinePrinter[]= {
	0x1b, 0x45, 0x1b, 0x26, 0x6c, 0x30, 0x48, 0x1b,
	0x26, 0x6c, 0x30, 0x4f, 0x1b, 0x26, 0x6c, 0x31,
	0x58, 0x1b, 0x2a, 0x76, 0x54, 0x1b, 0x28, 0x30,
	0x4e, 0x1b, 0x26, 0x6c, 0x38, 0x44, 0x1b, 0x28,
	0x73, 0x30, 0x74, 0x30, 0x62, 0x30, 0x73, 0x30,
	0x38, 0x2e, 0x35, 0x30, 0x76, 0x31, 0x36, 0x2e,
	0x36, 0x36, 0x68, 0x30, 0x50, 0x00};

DATETIME rptDateTime;


#pragma pack1
typedef struct SENSOR_HISTORY_st {
		unsigned short wClockTickH;
		unsigned short wClockTickL;
		unsigned short fwDigitalSensors;
		unsigned short fwActuators;
		} SENSOR_HISTORY;

typedef struct TRACE_HISTORY_st {
        unsigned short wClockTickH;
        unsigned short wClockTickL;
		unsigned char bProfile;
		unsigned char bSegment;
		} TRACE_HISTORY;
#pragma unpack

static void PrintSensorLog (MCP_INSTANCE_t *pMcpInst, short numrecs)
{
//TODO - determine where this is supposed to go: file, tablet or elsewhere
#if 0
	extern unsigned char *fbSensorHistory;

   	unsigned int                i;
	unsigned short             mask;
	unsigned short           hrs;
	unsigned short           min;
	unsigned short           sec;
	unsigned short           msec;
	unsigned long			 ms;
	unsigned int mtrspace = 7;

    unsigned int first_rec,rec_num;
    SENSOR_HISTORY * s;
    SENSOR_HISTORY * sensor_log;
    SENSOR_HISTORY * last;

    unsigned short Achange;
    unsigned short Dchange;
    unsigned short Mchange;

   short row, col, page=0;
   char LineBuff[255];
   char buff[80];
   char done = 0;
   char date_time[80];
   char profile_ver[80];

	FILE * pFile;


   sprintf(profile_ver, pclGotoRowCol,80,0);
 
	/*
		Add the Profile filename and version here
	*/
   strcat(profile_ver, (const char *)pMcpInst->bProfileVersion);


   sprintf(date_time, pclGotoRowCol,80, 100);

	/* get the current system date and time */
    (void) GetSysDateTime (&rptDateTime, ADDOFFSETS, GREGORIAN);  

	sprintf(buff, "DATE: %d/%d/%d%2.2d  TIME: %d:%2.2d", rptDateTime.bMonth, rptDateTime.bDay, 
												   		   rptDateTime.bCentury, rptDateTime.bYear,
												   		   rptDateTime.bHour, rptDateTime.bMinutes);

	strcat(date_time, buff);


    i = 0;	first_rec = 0;

	sensor_log = (SENSOR_HISTORY *)pMcpInst->pSensorLogBuff;//  fbSensorHistory;

      if ((sensor_log[numrecs-1].wClockTickH) || (sensor_log[numrecs-1].wClockTickL))
      {
         /* We have wrap around so we have to find the earliest record */
         for (s=sensor_log+1, i=1; i < numrecs; i++,s++)
         {
            if (((s-1)->wClockTickH >= s->wClockTickH) && ((s-1)->wClockTickL > s->wClockTickL))
            {
               first_rec = i;
               break;
            }
         }
      }

	 // Find first non zero record
	 for (i=0; i<numrecs; i++, first_rec++)
		if ((sensor_log[first_rec].wClockTickH) || (sensor_log[first_rec].wClockTickL))
			break;

	 if (i==numrecs) // If we have nothing to print just exit
		return;

      rec_num = first_rec;
      s = &sensor_log[first_rec];


      last = &sensor_log[first_rec-1];
      page = 0;

	pFile = fopen("/dev/LprPCL00","wb");
	if (pFile == NULL)
		return;

	fwrite (pclLinePrinter,1,strlen(pclLinePrinter), pFile);	// Setup the printer

      while (!done) {
      // new page

         for (i=0,row=0, col=10; pMcpInst->pReportInfo->pszSensorLogHeader[i] != NULL; i++,row++)
         {
            sprintf(buff,pclGotoRowCol,row,col);
            fprintf(pFile, "%s%s", buff, pMcpInst->pReportInfo->pszSensorLogHeader[i]);
         }

         while (row < 77)
         {
	         if (done = (s->wClockTickH == 0) && (s->wClockTickL == 0))	//lint !e720   // End if data
               break;

 
			Achange = 0;
			Dchange = 0;
			Mchange = 0;
			//if (pMcpInst->pReportInfo->fReportASensors)
			//	Achange = (s->fwAnalogSensors  ^ last->fwAnalogSensors) & pMcpInst->pReportInfo->wASensorMask;
			if (pMcpInst->pReportInfo->fReportDSensors)
	            Dchange = (s->fwDigitalSensors ^ last->fwDigitalSensors)& pMcpInst->pReportInfo->wDSensorMask;
			if (pMcpInst->pReportInfo->fReportMSensors)
	            Mchange = (s->fwActuators      ^ last->fwActuators)     & pMcpInst->pReportInfo->wMSensorMask;
            if (Achange || Dchange || Mchange)
            {
               ms = (s->wClockTickH << 16) + (s->wClockTickL);	/* convert ticks to ms */
               hrs = ms / 3600000L;	/* ms per hour */
               ms %= 3600000L;
		       min = ms / 60000L;	/* ms per min */
		       ms %= 60000L;
		       sec = ms / 1000;	/* ms per sec */
		       msec = ms % 1000;

               last = s;

               sprintf(buff,pclGotoRowCol,row++,col);
               sprintf(LineBuff,"%s%4u:%02u:%02u.%03u ",buff,hrs,min,sec,msec);
		         mask = 1;
		         for (i=0; i<16; i++)
               {
				  //if (pMcpInst->pReportInfo->fReportASensors)
                  // sprintf(buff,"%c ",
                  //          (s->fwAnalogSensors & mask) ? pMcpInst->pReportInfo->pszAState1[i] :
                  //                                        pMcpInst->pReportInfo->pszAState0[i]);
				  //else
				   sprintf(buff,"%c ",pMcpInst->pReportInfo->pszAState0[i]);
				      mask <<= 1;
                  strcat(LineBuff, buff);

			      }
               strcat(LineBuff,"   ");
               mask = 1;

		         for (i=0; i<16; i++)
               {
				  if (pMcpInst->pReportInfo->fReportDSensors)
                   sprintf(buff,"%c ",
                            (s->fwDigitalSensors & mask) ? pMcpInst->pReportInfo->pszDState1[i] :
                                                          pMcpInst->pReportInfo->pszDState0[i]);
				  else
				   sprintf(buff,"%c ",pMcpInst->pReportInfo->pszDState0[i]);

                  mask <<= 1;
                  strcat(LineBuff,buff);
		         }
               strcat(LineBuff,"   ");

               mask = 1;
               for (i=0; i<16; i++)
               {
				  if (pMcpInst->pReportInfo->fReportMSensors)
                   sprintf(buff,"%c ",
                            (s->fwActuators & mask) ? pMcpInst->pReportInfo->pszMState1[i] :
                                                          pMcpInst->pReportInfo->pszMState0[i]);
				  else
				   sprintf(buff,"%c ",pMcpInst->pReportInfo->pszMState0[i]);

                  mask <<= 1;
                  strcat(LineBuff,buff);
                  if (i == mtrspace)
                     strcat(LineBuff,"   ");
		         }

               fwrite(LineBuff,1,strlen(LineBuff),pFile);
            }

            if (++rec_num == numrecs)
            {
               rec_num = 0;
               s = sensor_log;
            } else
               s++;
            if (done = (rec_num == first_rec))	//lint !e720
               break;
      } // End of Page

      fwrite(profile_ver, 1, strlen(profile_ver), pFile);
      fwrite(date_time,   1, strlen(date_time),   pFile);

      sprintf(buff, pclGotoRowCol,80,65);
      sprintf(LineBuff,"%s- %u -\f", buff, ++page);  // End of Page

      fwrite(LineBuff, 1, strlen(LineBuff), pFile);

   } // End of Data

	fclose(pFile);
#endif
}

static void PrintProfileHistory (MCP_INSTANCE_t *pMcpInst, short numrecs)
{
//TODO - determine where this is supposed to go: file, tablet or elsewhere
#if 0
   extern unsigned char *pTraceHistory;

   TRACE_HISTORY * pHist;
   TRACE_HISTORY * pTrace;

   unsigned short hrs;
   unsigned short min;
   unsigned short sec;
   unsigned short msec;
   unsigned long  ms;
   int            first_rec, rec_num, size;
   int            i;
   char LineBuff[255];
   char buff[80];
   short row, col, page;
   char done = 0;
   char date_time[80];
   char profile_ver[80];
   FILE * lpt;


   sprintf(profile_ver, pclGotoRowCol,80,0);
 
	/*
		Add the Profile filename and version here
	*/
   strcat(profile_ver, (const char *)pMcpInst->bProfileVersion);


   sprintf(date_time, pclGotoRowCol,80, 100);

	/* get the current system date and time */
    (void) GetSysDateTime (&rptDateTime, ADDOFFSETS, GREGORIAN);  

	sprintf(buff, "DATE :%d/%d/%d%2.2d  TIME: %d:%2.2d", rptDateTime.bMonth, rptDateTime.bDay, 
												   		   rptDateTime.bCentury, rptDateTime.bYear,
												   		   rptDateTime.bHour, rptDateTime.bMinutes);

	strcat(date_time, buff);

/*
   ProcessField("DATE", buff);
   strcat(date_time,buff);
   strcat(date_time,"  ");
   ProcessField("TIME", buff);
   strcat(date_time, buff);
*/

	lpt = fopen("/dev/LprPCL00","wb");
	if (lpt == NULL)
		return;

	fwrite( pclLinePrinter, 1, strlen(pclLinePrinter), lpt );

    pTrace = (TRACE_HISTORY *)pMcpInst->pProfileHistLogBuff;//  pTraceHistory;

 
    if (pTrace->wClockTickH || pTrace->wClockTickL) {
       first_rec = 0;
       pHist = pTrace;
    } else {
       first_rec = 1;
       pHist = pTrace+1;
    }

    if (pTrace[numrecs-1].wClockTickL || pTrace[numrecs-1].wClockTickH)
      {
         /* We have wrap around so we have to find the earliest record */
         for (pHist=pTrace+1, i=1; i < numrecs; i++,pHist++)
         {
            if (((pHist-1)->wClockTickH >= pHist->wClockTickH) &&
				((pHist-1)->wClockTickL > pHist->wClockTickL))
            {
               first_rec = i;
               break;
            }
         }
      }

    pHist = &pTrace[first_rec];
    rec_num = first_rec;

    page = 0;


    while (!done) {
      // New Page
      sprintf(buff,pclGotoRowCol,0,51);
      sprintf(LineBuff, "%s Profile Execution Trace",buff);
      fwrite(LineBuff, 1, strlen(LineBuff), lpt);

      for (col = 0; (col < 3*45) && !done; col+= 45) {
         // New Col
         row = 2;
         sprintf(buff,pclGotoRowCol,row,col);
	       sprintf(LineBuff,"%s      Time            Hex         Decimal",buff);
         fwrite(LineBuff, 1, strlen(LineBuff), lpt);

         sprintf(buff,pclGotoRowCol,++row,col);
         sprintf(LineBuff,"%shhhh:mm:ss.mmm     Prof [Seg]   Prof [Seg]",buff);
         fwrite(LineBuff, 1, strlen(LineBuff), lpt);

         for (row = 5; (row < 79) && !done; row++) {

	         if (done = ((pHist->wClockTickH == 0) && (pHist->wClockTickL == 0))) //lint !e720	// End if data
               break;
            ms = (pHist->wClockTickH << 16) + pHist->wClockTickL;	/* 1 ms per tick */
            hrs = ms / 3600000L;	/* ms per hour */
            ms %= 3600000L;
		      min = ms / 60000L;	/* ms per min */
		      ms %= 60000L;
		      sec = ms / 1000;	/* ms per sec */
		      msec = ms % 1000;

            sprintf(buff,pclGotoRowCol,row,col);
		       sprintf(LineBuff,"%s%4u:%02u:%02u.%03u     0x%02x [0x%02x]   %3u [%3u]  %c",
                 buff,hrs,min,sec,msec,
                 pHist->bProfile, pHist->bSegment, pHist->bProfile, pHist->bSegment,
                 (col) == 90 ? ' ' : '|');
             fwrite(LineBuff, 1, strlen(LineBuff), lpt);

       	   if (++rec_num == numrecs)
            {
               rec_num = 0;
               pHist = pTrace;
            } else
            pHist++;
            done = (rec_num == first_rec);
         } // End of row
      } // End of Col
      // End of Page

      fwrite(profile_ver, 1, strlen(profile_ver), lpt);
      fwrite(date_time,   1, strlen(date_time),   lpt);

      sprintf(buff, pclGotoRowCol,80,65);
      sprintf(LineBuff,"%s- %u -\f", buff, ++page);  // End of Page

      fwrite(LineBuff, 1, strlen(LineBuff), lpt);
 
    }
	fclose(lpt);
#endif
}

void fnMCPPrintProfileHistory(void *pMcp)
{
	MCP_INSTANCE_t *pMcpInst = NULL;

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	
	if (pMcp == NULL)
		return;

	pMcpInst = (MCP_INSTANCE_t*)pMcp;

	MCPLIB_ASSERT(pMcp,pMcpInst->lwState == MCP_LIB_NO_STATE);	//lint !e506
	if (pMcpInst->lwState != MCP_LIB_NO_STATE)
		return;
	MCPLIB_ASSERT(pMcpInst,pMcpInst->fInitComplete);	//lint !e506
	if (!pMcpInst->fInitComplete)
		return;

	if (pMcpInst->pProfileHistLogBuff && pMcpInst->lwProfileHistLogCount)
		PrintProfileHistory(pMcpInst,(short)pMcpInst->lwProfileHistLogCount);
}
void fnMCPPrintSensorLog(void *pMcp)
{
	MCP_INSTANCE_t *pMcpInst = NULL;

	MCPLIB_ASSERT(pMcp,pMcp!=NULL);	//lint !e506
	
	if (pMcp == NULL)
		return;

	pMcpInst = (MCP_INSTANCE_t*)pMcp;

	MCPLIB_ASSERT(pMcp,pMcpInst->lwState == MCP_LIB_NO_STATE);	//lint !e506
	if (pMcpInst->lwState != MCP_LIB_NO_STATE)
		return;
	MCPLIB_ASSERT(pMcpInst,pMcpInst->fInitComplete);	//lint !e506
	if (!pMcpInst->fInitComplete)
		return;
	
	if (pMcpInst->pSensorLogBuff && pMcpInst->lwSensorLogCount)
		PrintSensorLog(pMcpInst,(short)pMcpInst->lwSensorLogCount);
}

static char bMcpLibAssertFailureText[256];

static void fnMCPLibHandleAssertionFailure(const MCP_INSTANCE_t *pMcp,
										const char *pExpr, 
										const char *pFileName,
										const unsigned long lwLine)
{
	if (pMcp && pExpr && pFileName)
	{
		unsigned char tmpMsg[8];

		// put entry in the mcp buffer
		tmpMsg[0] = 0xDE;
		tmpMsg[1] = pMcp->bMcpId;
		tmpMsg[2] = pMcp->bTaskId;
		tmpMsg[3] = (unsigned char)pMcp->lwState;
		(void)memcpy((void*)&tmpMsg[4],(void*)&lwLine,4);	//lint !e718 !e746

		// put an entry in some other logs...
		(void)sprintf(bMcpLibAssertFailureText, "McpLib Assert Failure: %s, Line: %d", pExpr,lwLine);
		fnDumpStringToSystemLog(bMcpLibAssertFailureText);	//lint !e718 !e746

	}
}

/******************************************************************************
 *																			  *
 *				   		Local Utility Functions								  *
 *																			  *
 *****************************************************************************/

/*
 * Name
 *	 swapWordBytes
 *
 * Description
 *   This function performs a byte swap on an unsigned short integer.
 *
 * Inputs
 *   uwValue - the unsigned short value to be byte-swapped
 *
 * Returns
 *   the byte-swapped version of the unsigned short integer passed in
 *
 * Caveats
 *   None.
 *
 * Revision History
 *   02/27/2001 Joseph P. Tokarski (CSS, LLC) - Initial Revision.
 */
static unsigned short swapWordBytes(unsigned short uwValue)
{
	unsigned short uwSwapped;

	/* move the low byte to the high byte of the result */
	uwSwapped = (unsigned short)(uwValue << 8);

	/* move the high byte to the low byte, and add it to the result */
	uwSwapped += (unsigned short)(uwValue >> 8);

	/* return the result */
	return(uwSwapped);
}

/*
 * Name
 *	 swapLongBytes
 *
 * Description
 *   This function performs a byte swap on an unsigned long integer.
 *
 * Inputs
 *   ulValue - the unsigned long value to be byte-swapped
 *
 * Returns
 *   the byte-swapped version of the unsigned long integer passed in
 *
 * Caveats
 *   None.
 *
 * Revision History
 *   02/27/2001 Joseph P. Tokarski (CSS, LLC) - Initial Revision.
 */
static unsigned long swapLongBytes(unsigned long ulValue)
{
	unsigned long ulSwapped;

	/* swap the low word, and make it the high word of the result */
	ulSwapped = (unsigned long)swapWordBytes((unsigned short)(ulValue & 0x0000FFFF));
	ulSwapped = (unsigned long)(ulSwapped << 16);

	/* swap the high word, and make it the low byte of the result 
	 * (by adding it to the result) */
	ulSwapped += (unsigned long)swapWordBytes((unsigned short)(ulValue >> 16));

	/* return the result */
	return(ulSwapped);
}


const BaseModelToPartNumXref mmcModel2PartNum[] = {
	{"DM400C", "G900009", "10.01.01"},	
	{"DM475", "G900097", "01.00.00"},	
	{"",      "",        ""},
};


void fnMCPLibSetPassThroughMode(const BOOL fPassThrough, const int destination)
{
	fPassThroughModeOn = fPassThrough;
	iPassThroughDestination = destination;
}
BOOL fnMCPLibGetPassThroughMode(void)
{
	return fPassThroughModeOn;
}
int fnMCPLibGetPassThroughModeDest(void)
{
	return iPassThroughDestination;
}
void ProcessMotionClassMessage( int requestor, unsigned char *pbData, unsigned short pbDataLen)
{
	UINT8 bSrc=0;
	UINT8 bDest=0;
	if (fPassThroughModeOn)
	{
		bDest = pbData[MMC_MSG_DEST_OFFSET];
		bSrc = pbData[MMC_MSG_SOURCE_OFFSET];

		switch (bDest) {
		case FDR_MCP_ID :
        case DM475C_MCP_ID:
			(void)fnMCPSendMsg((unsigned char*)pbData,(unsigned long)pbDataLen);
			break;
		case FDRMCPTASK :			
			break;
		};
		
	}	
}


/**********************************************************************/
// 
//	OS Stuff to Port
//
/**********************************************************************/

// OSSendIntertask
// OSReceiveIntertask
// OSResumeTask

// memset
// memcpy

/**********************************************************************/
//	END OS Stuff to Port
/**********************************************************************/
