//=====================================================================
//
//	FileName:	HAL_AM335x_RTC.c
//
//	Description: Hardware abstraction layer implementation for AM335x Real Time Clock initialization
//=====================================================================
#include "commontypes.h"
#include "bsp/csd_2_3_defs.h"
#include "nucleus.h"


static void HALWriteProtectRTC(BOOL enable)
{
	if (enable)
	{
		/* Writing a random value into KICK registers.*/
		ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_KICK0R, 0);
		ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_KICK1R, 0);
	}
	else
	{
	    /* Writing a specific value into KICK registers.*/
	    ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_KICK0R, AM335X_RTC_KICK0R_KEY);
	    ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_KICK1R, AM335X_RTC_KICK1R_KEY);
	}
}

static void HALEnableRTC(BOOL enable)
{
	unsigned temp;

    /* Update RTC_CTRL_REG */
    temp = ESAL_GE_MEM_READ32(AM335X_RTCSS_BASE + AM335X_RTCSS_RTC_CTRL_REG) & ~(AM335X_RTCSS_CTRL_RTC_EN_DIS);

	if (enable)
	{
	    ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_RTC_CTRL_REG, temp | AM335X_RTCSS_CTRL_RTC_ENABLE);
	}
	else
	{
	    ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_RTC_CTRL_REG, temp | AM335X_RTCSS_CTRL_RTC_DISABLE);
	}
}

static void HALRunRTC(BOOL enable)
{
	unsigned temp;

    /* Update RTC_CTRL_REG */
    temp = ESAL_GE_MEM_READ32(AM335X_RTCSS_BASE + AM335X_RTCSS_RTC_CTRL_REG) & ~(AM335X_RTCSS_CTRL_STOP_RTC);

    if (enable)
	{
	    /* Setting the RUN bit in CTRL register.*/
	    ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_RTC_CTRL_REG, temp | AM335X_RTCSS_CTRL_STOP_RTC_RUN);
	}
	else
	{
	      /* Clearing the RUN bit in CTRL register. */
	    ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_RTC_CTRL_REG, temp | AM335X_RTCSS_CTRL_STOP_RTC_FREEZE);

	    /* Waiting for RUN bit in STATUS register to indicate stopped  */
	    while((ESAL_GE_MEM_READ32(AM335X_RTCSS_BASE + AM335X_RTCSS_RTC_STATUS_REG) & AM335X_RTCSS_STATUS_RUN) ==
	    		AM335X_RTCSS_STATUS_RUN_YES);
	}
}

static void HALInitRTCClocks(void)
{
    /* Writing to MODULEMODE field of AM335X_CM_RTC_RTC_CLKCTRL register. */
    ESAL_GE_MEM_WRITE32(AM335X_CM_RTC_BASE + AM335X_CM_RTC_RTC_CLKCTRL, AM335X_CM_CLKCTRL_MODULEMODEEN);

    /* Waiting for MODULEMODE field  */
    while((ESAL_GE_MEM_READ32(AM335X_CM_RTC_BASE + AM335X_CM_RTC_RTC_CLKCTRL) & AM335X_CM_CLKCTRL_MODULEMODE) !=
    		AM335X_CM_CLKCTRL_MODULEMODEEN);

    /* Writing to CLKTRCTRL field of AM335X_CM_RTC_CLKSTCTRL register. */
    ESAL_GE_MEM_WRITE32(AM335X_CM_RTC_BASE + AM335X_CM_RTC_CLKSTCTRL, AM335X_CM_CLKCTRL_SWWKUP);

    /* Waiting for CLKTRCTRL field  */
    while((ESAL_GE_MEM_READ32(AM335X_CM_RTC_BASE + AM335X_CM_RTC_CLKSTCTRL) & AM335X_CM_CLKCTRL_CLKTRCTRL) !=
    		AM335X_CM_CLKCTRL_SWWKUP);

    /* Waiting for IDLEST field in AM335X_CM_RTC_RTC_CLKCTRL register */
    while((ESAL_GE_MEM_READ32(AM335X_CM_RTC_BASE + AM335X_CM_RTC_RTC_CLKCTRL) & AM335X_CM_CLKCTRL_IDLEST) !=
                             AM335X_CM_CLKCTRL_IDLEST_FUNC);

    /* Waiting for AM335X_CM_RTC_L4_RTC_GCLK active  */
    while((ESAL_GE_MEM_READ32(AM335X_CM_RTC_BASE + AM335X_CM_RTC_CLKSTCTRL) & AM335X_CM_RTC_L4_RTC_GCLK) !=
    		AM335X_CM_RTC_L4_RTC_GCLK_ACT);

    /* Waiting for AM335X_CM_RTC_RTC_32K_CLK active */
    while((ESAL_GE_MEM_READ32(AM335X_CM_RTC_BASE + AM335X_CM_RTC_CLKSTCTRL) & AM335X_CM_RTC_RTC_32K_CLK) !=
    		AM335X_CM_RTC_RTC_32K_CLK_ACT);

}

static void HALInitRTCClockSource(void)
{
	unsigned temp;

    /* Selecting Internal Clock source for RTC. */
    temp = ESAL_GE_MEM_READ32(AM335X_RTCSS_BASE + AM335X_RTCSS_RTC_OSC_REG);

    temp &= ~(AM335X_RTCSS_OSC_RES_SELECT |
    		AM335X_RTCSS_OSC_SEL_32KCLK_SRC |
    		AM335X_RTCSS_OSC_OSC32K_GZ |
    		AM335X_RTCSS_OSC_EN_32KCLK);

    temp |= (AM335X_RTCSS_OSC_RES_SELECT_INT |
    		AM335X_RTCSS_OSC_SEL_32KCLK_SRC_INT |
    		AM335X_RTCSS_OSC_OSC32K_GZ_DIS |
    		AM335X_RTCSS_OSC_EN_32KCLK_EN);

    ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_RTC_OSC_REG, temp);

}

static BOOL validDateTime(DATETIME *pBinaryClockSettings)
{
	// Century: ignore because it does not go into RTC

	// Year: 0 - 99
	if (pBinaryClockSettings->bYear > 99)
	{
		return FALSE;
	}

	// Month: 1 - 12
	if ((pBinaryClockSettings->bMonth > 12) || (pBinaryClockSettings->bMonth < 1))
	{
		return FALSE;
	}

	// Day of Week: 0 - 6
	if (pBinaryClockSettings->bDayOfWeek > 6)
	{
		return FALSE;
	}

	// Day: 1 - 31
	if ((pBinaryClockSettings->bDay > 31) || (pBinaryClockSettings->bDay < 1))
	{
		return FALSE;
	}

	// Day of Month: 31
	if ((pBinaryClockSettings->bDay == 31) && ( (pBinaryClockSettings->bMonth == 2) ||
												(pBinaryClockSettings->bMonth == 4) ||
												(pBinaryClockSettings->bMonth == 6) ||
												(pBinaryClockSettings->bMonth == 9) ||
												(pBinaryClockSettings->bMonth == 11) ))
	{
		return FALSE;
	}

	// Day of Month: 30
	if ((pBinaryClockSettings->bDay == 30) && (pBinaryClockSettings->bMonth == 2))
	{
		return FALSE;
	}

	// Day of Month: 29
	if ((pBinaryClockSettings->bDay == 29) && (pBinaryClockSettings->bMonth == 2) && ((pBinaryClockSettings->bYear % 4) != 0))
	{
		return FALSE;
	}

	// Hour: 0 - 23
	if (pBinaryClockSettings->bHour > 23)
	{
		return FALSE;
	}

	// Minute: 0 - 59
	if (pBinaryClockSettings->bMinutes > 59)
	{
		return FALSE;
	}

	// Second: 0 - 59
	if (pBinaryClockSettings->bSeconds > 59)
	{
		return FALSE;
	}

    return TRUE;
}

static void HALSetRawRTCValues(DATETIME *pBinaryClockSettings)
{
	unsigned char hiDigit, loDigit;

	// Seconds
	loDigit = pBinaryClockSettings->bSeconds % 10;
	hiDigit = pBinaryClockSettings->bSeconds / 10;

    ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_SECONDS_REG,
    		AM335X_RTCSS_SECONDS_SEC1_TO_FIELD(hiDigit) +
    		AM335X_RTCSS_SECONDS_SEC0_TO_FIELD(loDigit));

	// Minutes
	loDigit = pBinaryClockSettings->bMinutes % 10;
	hiDigit = pBinaryClockSettings->bMinutes / 10;

    ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_MINUTES_REG,
    		AM335X_RTCSS_MINUTES_MIN1_TO_FIELD(hiDigit) +
    		AM335X_RTCSS_MINUTES_MIN0_TO_FIELD(loDigit));

	// Hours
	loDigit = pBinaryClockSettings->bHour % 10;
	hiDigit = pBinaryClockSettings->bHour / 10;

    ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_HOURS_REG,
    		AM335X_RTCSS_HOURS_HOUR1_TO_FIELD(hiDigit) +
    		AM335X_RTCSS_HOURS_HOUR0_TO_FIELD(loDigit));

	// Days
	loDigit = pBinaryClockSettings->bDay % 10;
	hiDigit = pBinaryClockSettings->bDay / 10;

    ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_DAYS_REG,
    		AM335X_RTCSS_DAYS_DAY1_TO_FIELD(hiDigit) +
    		AM335X_RTCSS_DAYS_DAY0_TO_FIELD(loDigit));

	// Months
	loDigit = pBinaryClockSettings->bMonth % 10;
	hiDigit = pBinaryClockSettings->bMonth / 10;

    ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_MONTHS_REG,
    		AM335X_RTCSS_MONTHS_MONTH1_TO_FIELD(hiDigit) +
    		AM335X_RTCSS_MONTHS_MONTH0_TO_FIELD(loDigit));

	// Years
	loDigit = pBinaryClockSettings->bYear % 10;
	hiDigit = pBinaryClockSettings->bYear / 10;

    ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_YEARS_REG,
    		AM335X_RTCSS_YEARS_YEAR1_TO_FIELD(hiDigit) +
    		AM335X_RTCSS_YEARS_YEAR0_TO_FIELD(loDigit));

	// Day of Week
	loDigit = pBinaryClockSettings->bDayOfWeek;

    ESAL_GE_MEM_WRITE32(AM335X_RTCSS_BASE + AM335X_RTCSS_WEEKS_REG,
    		AM335X_RTCSS_WEEKS_WEEK0_TO_FIELD(loDigit));


}

// RTC sub system init without setting of date/time and without running
void HALInitRTCSS(void)
{
	HALInitRTCClocks();
	HALWriteProtectRTC(FALSE);
	HALInitRTCClockSource();
	HALEnableRTC(TRUE);
	HALRunRTC(FALSE);
	HALWriteProtectRTC(TRUE);
}

// RTC setting of date and time
//  - RTC is stopped during update
BOOL HALSetRTC(DATETIME *pBinaryClockSettings)
{
	int old_level;
	DATETIME localSettings;

	if (pBinaryClockSettings == NULL)
	{
		return FALSE;
	}

	// Create local copy of input which has values adjusted to match RTC
	memcpy(&localSettings, pBinaryClockSettings, sizeof(DATETIME));
	// Adjust day of week to be 0 based
	localSettings.bDayOfWeek--;

	if (!validDateTime(&localSettings))
	{
		return FALSE;
	}
	//disable interrupts so that stoppage of RTC is minimized
    old_level = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
	HALWriteProtectRTC(FALSE);
	// Stop RTC to set values and then restart
	HALRunRTC(FALSE);

	// Set values
	HALSetRawRTCValues(&localSettings);

	HALRunRTC(TRUE);

	HALWriteProtectRTC(TRUE);
    NU_Local_Control_Interrupts(old_level);

    return TRUE;
}

// RTC getting of date and time
BOOL HALGetRTC(DATETIME *pBinaryClockSettings)
{
	unsigned readVal;
	unsigned char hiDigit, loDigit;

	if (pBinaryClockSettings == NULL)
	{
		return FALSE;
	}

	//Read values - must start with Seconds register
	// Seconds
    readVal = ESAL_GE_MEM_READ32(AM335X_RTCSS_BASE + AM335X_RTCSS_SECONDS_REG);

    loDigit = (readVal & AM335X_RTCSS_SECONDS_SEC0_TO_FIELD(AM335X_RTCSS_SEC0_MASK));
    pBinaryClockSettings->bSeconds = AM335X_RTCSS_SECONDS_SEC0_FROM_FIELD(loDigit);

    hiDigit = readVal & AM335X_RTCSS_SECONDS_SEC1_TO_FIELD(AM335X_RTCSS_SEC1_MASK);
    pBinaryClockSettings->bSeconds += 10 * AM335X_RTCSS_SECONDS_SEC1_FROM_FIELD(hiDigit);

    // Minutes
    readVal = ESAL_GE_MEM_READ32(AM335X_RTCSS_BASE + AM335X_RTCSS_MINUTES_REG);

    loDigit = (readVal & AM335X_RTCSS_MINUTES_MIN0_TO_FIELD(AM335X_RTCSS_MIN0_MASK));
    pBinaryClockSettings->bMinutes = AM335X_RTCSS_MINUTES_MIN0_FROM_FIELD(loDigit);

    hiDigit = readVal & AM335X_RTCSS_MINUTES_MIN1_TO_FIELD(AM335X_RTCSS_MIN1_MASK);
    pBinaryClockSettings->bMinutes += 10 * AM335X_RTCSS_MINUTES_MIN1_FROM_FIELD(hiDigit);

    // Hours
    readVal = ESAL_GE_MEM_READ32(AM335X_RTCSS_BASE + AM335X_RTCSS_HOURS_REG);

    loDigit = (readVal & AM335X_RTCSS_HOURS_HOUR0_TO_FIELD(AM335X_RTCSS_HOUR0_MASK));
    pBinaryClockSettings->bHour = AM335X_RTCSS_HOURS_HOUR0_FROM_FIELD(loDigit);

    hiDigit = readVal & AM335X_RTCSS_HOURS_HOUR1_TO_FIELD(AM335X_RTCSS_HOUR1_MASK);
    pBinaryClockSettings->bHour += 10 * AM335X_RTCSS_HOURS_HOUR1_FROM_FIELD(hiDigit);

    // Days
    readVal = ESAL_GE_MEM_READ32(AM335X_RTCSS_BASE + AM335X_RTCSS_DAYS_REG);

    loDigit = (readVal & AM335X_RTCSS_DAYS_DAY0_TO_FIELD(AM335X_RTCSS_DAY0_MASK));
    pBinaryClockSettings->bDay = AM335X_RTCSS_DAYS_DAY0_FROM_FIELD(loDigit);

    hiDigit = readVal & AM335X_RTCSS_DAYS_DAY1_TO_FIELD(AM335X_RTCSS_DAY1_MASK);
    pBinaryClockSettings->bDay += 10 * AM335X_RTCSS_DAYS_DAY1_FROM_FIELD(hiDigit);

    // Months
    readVal = ESAL_GE_MEM_READ32(AM335X_RTCSS_BASE + AM335X_RTCSS_MONTHS_REG);

    loDigit = (readVal & AM335X_RTCSS_MONTHS_MONTH0_TO_FIELD(AM335X_RTCSS_MONTH0_MASK));
    pBinaryClockSettings->bMonth = AM335X_RTCSS_MONTHS_MONTH0_FROM_FIELD(loDigit);

    hiDigit = readVal & AM335X_RTCSS_MONTHS_MONTH1_TO_FIELD(AM335X_RTCSS_MONTH1_MASK);
    pBinaryClockSettings->bMonth += 10 * AM335X_RTCSS_MONTHS_MONTH1_FROM_FIELD(hiDigit);

    // Years
    readVal = ESAL_GE_MEM_READ32(AM335X_RTCSS_BASE + AM335X_RTCSS_YEARS_REG);

    loDigit = (readVal & AM335X_RTCSS_YEARS_YEAR0_TO_FIELD(AM335X_RTCSS_YEAR0_MASK));
    pBinaryClockSettings->bYear = AM335X_RTCSS_YEARS_YEAR0_FROM_FIELD(loDigit);

    hiDigit = readVal & AM335X_RTCSS_YEARS_YEAR1_TO_FIELD(AM335X_RTCSS_YEAR1_MASK);
    pBinaryClockSettings->bYear += 10 * AM335X_RTCSS_YEARS_YEAR1_FROM_FIELD(hiDigit);

    // Day of Week
    readVal = ESAL_GE_MEM_READ32(AM335X_RTCSS_BASE + AM335X_RTCSS_WEEKS_REG);

    // RD - software is 1 based and chip is 0 based so add 1 to chip value....
    loDigit = (readVal & AM335X_RTCSS_WEEKS_WEEK0_TO_FIELD(AM335X_RTCSS_WEEK0_MASK));
    pBinaryClockSettings->bDayOfWeek = AM335X_RTCSS_WEEKS_WEEK0_FROM_FIELD(loDigit) + 1;

    pBinaryClockSettings->bCentury  = 20;  // return correct value (for the next 80 years anyway) regardless of input
    return TRUE;
}


