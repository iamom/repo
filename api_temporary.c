/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    api_temporary.c

   DESCRIPTION:    API implementations for temporary messages

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------

*************************************************************************/
#include "commontypes.h"
#include "cJSON.h"
#include "wsox_tx_queue.h"
#include "pbos.h"
#include "events.h"
#include "oit.h"
#include "wsox_services.h"
#include "networkmonitor.h"
#include "csd_sim.h"
#include "oifpinfra.h"
#include "bobutils.h"

#include "api_funds.h"
#include "api_utils.h"
#include "settingsmgr.h"


#include "trmdefs.h"
#include "trmstrg.h"
#include "trm.h"
#include "bkgndmgr.h"
#include "pcdisk.h"

#include "zutil.h"
#include "clock.h"
#include "ossetup.h"
#include "..\include\exception.h"

#define __UPLOAD_DEFINE_TABLES__
#include "api_temporary.h"
#include "btldr_fram.h"
#include "cm.h"
#include "errcode.h"
#include "utils.h"
#include "sysdatadefines.h"
#include "api_status.h"
#include "datdict.h"
#include "custdat.h"

#define THISFILE "api_temporary.c"

#define  CRLF   "\r\n"

extern SetupParams CMOSSetupParams;
extern uchar 			  CMOSDcapGuid[];
extern BOOL     		  fUploadInProgress;

extern DCB_PARAMETERS_t *pParameters;
extern NU_MEMORY_POOL    	*pSystemMemoryCached;


extern NU_PIPE wsox_tablet_usb_devices_rsp_pipe;
extern NU_PIPE wsox_tablet_usb_write_test_rsp_pipe;
extern NU_PIPE wsox_tablet_versions_rsp_pipe;
extern NU_PIPE wsox_tablet_status_rsp_pipe;
extern NU_MEMORY_POOL    *pSystemMemoryCached;
extern unsigned long EndianSwap32(const unsigned long inval);
extern const char *api_status_to_string_tbl[];
extern long graceSeconds;
extern char uploadWaitCount;
extern bool trmTransactionLogActive;

extern CM_SYSTEM_STATUS_t sSystemStatus;
extern uint32_t gTrmFatalError;
extern BOOL     fTrmFatalError;
// prototypes


BOOL fnGetUpdateNow (UINT8 bServiceCode, INT32 value, INT32 value2);
STATUS trmCompletePendingWrites(void);
STATUS trmPrepareXMLPackageFile(int periodIdx);
int trmGetNextPeriod(void);
extern bool image_file_received;

/* --------------------------------------------------------------------------------------------[on_InstallUpdatesReq]-- */
void on_InstallUpdatesReq(UINT32 handle, cJSON *root)
{
	int 			delay = 500;
	char 			sState[50];
    cJSON *rsp = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rsp}}};

    if (cJSON_HasObjectItem(root, "SeqNr"))
        cJSON_AddNumberToObject(rsp, "SeqNr", cJSON_GetObjectItem(root, "SeqNr")->valueint);
    if (cJSON_HasObjectItem(root, "Delay"))
        delay = cJSON_GetObjectItem(root, "Delay")->valueint;

    GetUICStates(sState);
    cJSON_AddStringToObject(rsp, "BaseState", sState);

    if (is_bldr_info_initialized() && verify_bldr_info_crc() && image_file_received)
    {
        /* let bootloader know to process the install directory on next boot */
        bldr_info_set_flags(BI_FLAG_PROCESS_INSTALL_DIR | BI_FLAG_REBOOT_WHEN_DONE);
    }
    
    if(strcmp(sState,"Running") == 0)
    {
		generateResponse(&entry, rsp, "InstallUpdates", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
    }
    else
    {
		generateResponse(&entry, rsp, "InstallUpdates", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
		//give the web socket time to respond
		NU_Sleep(delay);
		//reboot and install the files
		if (delay != 13)
			RebootSystem();
   }

}

/* --------------------------------------------------------------------------------------------[on_SetVerbosityReq]-- */
void on_SetVerbosityReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "SetVerbosityRsp");

    cJSON *level = cJSON_GetObjectItem(root, "Level");
    if (level)
    {
        if (level->valueint >= MIN_VERBOSITY && level->valueint <= MAX_VERBOSITY)
            verbosity = level->valueint;
        else
            cJSON_AddStringToObject(rspMsg, "ErrorStr", "Invalid verbosity level. verbosity not changed!");
    }

    cJSON_AddNumberToObject(rspMsg, "Level", verbosity);

    addEntryToTxQueue(&entry, root, "  SetVerbosityReq: Added SetVerbosityRsp to tx queue. status=%d" CRLF);

}

/* -------------------------------------------------------------------------------------------[on_SetRatingInfoReq]-- */
required_fields_tbl_t required_fields_tbl_SetRatingInfoReq = 
    {
    "SetRatingInfoReq", { "Postage"
                         ,"Weight"
                         , NULL } 
    };

void on_SetRatingInfoReq(UINT32 handle, cJSON *root)
{
    cJSON *reqParms;
    cJSON *rspParms;
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "SetRatingInfoRsp");

    reqParms = cJSON_GetObjectItem(root, "Parameters");
    if (reqParms)
    {
        if (RequiredFieldsArePresent(reqParms, rspMsg, &required_fields_tbl_SetRatingInfoReq))
        {
            cJSON_AddItemToObject(rspMsg, "Parameters", rspParms = cJSON_CreateObject());

            cJSON_AddStringToObject(rspParms,    "Postage", cJSON_GetObjectItem(reqParms, "Postage")->valuestring);
            cJSON_AddStringToObject(rspParms,    "Carrier", "USPS");
            cJSON_AddStringToObject(rspParms,      "Class", "First Class");
            cJSON_AddStringToObject(rspParms, "ClassToken", "1234");
            cJSON_AddStringToObject(rspParms,     "Weight", cJSON_GetObjectItem(reqParms, "Weight")->valuestring);

        }
        /* else an error has been added to rspMsg by RequiredFieldsArePresent() */
    }
    else
        AddError(rspMsg, "SetRatingInfoReq", "Missing Parameters in SetRatingInfoReq");

    addEntryToTxQueue(&entry, root, "  SetRatingInfoReq: Added SetRatingInfoRsp to tx queue. status=%d" CRLF);
}

/* ------------------------------------------------------------------------------------[on_GetNetworkInterfacesReq]-- */
void on_GetNetworkInterfacesReq(UINT32 handle, cJSON *root)
{
    //NU_IOCTL_OPTION ioctl_opt;
    //STATUS      status = STATUS_FAILURE;
    cJSON *rspMsg = cJSON_CreateObject();
    cJSON *intf;
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetNetworkInterfacesRsp");
    if (cJSON_HasObjectItem(root, "SeqNr"))
        cJSON_AddNumberToObject(rspMsg, "SeqNr", cJSON_GetObjectItem(root, "SeqNr")->valueint);

    cJSON_AddItemToObject(rspMsg, "Interfaces", intf = cJSON_CreateArray());

    /* Interface Name Index  */
    struct if_nameindex *NI;

    /* will use this to free Name Index */
    struct if_nameindex *NI_tmp;

    NI = NU_IF_NameIndex();
    NI_tmp = NI;

    while (NI->if_name != NU_NULL)
    {
    	// do not include the loopback port in the list
    	if(strcmp(NI->if_name, "loopback")!= 0)
    	{

    		cJSON_AddItemToArray(intf, cJSON_CreateString(NI->if_name));
    		cJSON *pObj = getLANConfigAsJSONObject(NI->if_name);
    		if(pObj)
    		{
    			cJSON_AddItemToObject(rspMsg, NI->if_name, pObj) ;
    		}
    	}


        /* next interface */
        NI = (struct if_nameindex *)((CHAR *)NI +
                sizeof(struct if_nameindex) + DEV_NAME_LENGTH);

    }

    /* Free name index */
    NU_IF_FreeNameIndex(NI_tmp);

    addEntryToTxQueue(&entry, root, "  GetNetworkInterfacesReq: Added GetNetworkInterfacesRsp to tx queue. status=%d" CRLF);

}

#ifdef MEMORYTEST
required_fields_tbl_t required_fields_tbl_MemoryTestReq =
    {
    "MemoryTestReq", { "MemoryType"
                       ,"TestType"
                       ,"TestCount"
                       ,"Address"
                       ,"Size"
                       , NULL }
    };

void on_RunMemoryTestReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_MemoryTestReq))
    {
        ev_data_t ev_data;

        // extract field values

        ev_data.type = EVDT_MEMORY_TEST;

        memset(&ev_data.d.memory_test_data, 0, sizeof(ev_data.d.memory_test_data));
        ev_data.d.memory_test_data.memory_test_req.memory_type = cJSON_GetObjectItem(root, "MemoryType")->valueint;
		ev_data.d.memory_test_data.memory_test_req.test_type = cJSON_GetObjectItem(root, "TestType")->valueint;

		if(cJSON_HasObjectItem(root, "TestDelay"))
			ev_data.d.memory_test_data.memory_test_req.delay = cJSON_GetObjectItem(root, "TestDelay")->valueint;
		else
			ev_data.d.memory_test_data.memory_test_req.delay = 100;

		if(cJSON_HasObjectItem(root, "TestCount"))
			ev_data.d.memory_test_data.memory_test_req.count = cJSON_GetObjectItem(root, "TestCount")->valueint;
		else
			ev_data.d.memory_test_data.memory_test_req.count = 1;

		ev_data.d.memory_test_data.memory_test_req.address = cJSON_GetObjectItem(root, "Address")->valueint;
		ev_data.d.memory_test_data.memory_test_req.size= cJSON_GetObjectItem(root, "Size")->valueint;

		if(cJSON_HasObjectItem(root, "Value32"))
			ev_data.d.memory_test_data.memory_test_req.value32 = cJSON_GetObjectItem(root, "Value32")->valueint;

		if(cJSON_HasObjectItem(root, "Value16"))
			ev_data.d.memory_test_data.memory_test_req.value16 = cJSON_GetObjectItem(root, "Value16")->valueint;

		if(cJSON_HasObjectItem(root, "Value8"))
			ev_data.d.memory_test_data.memory_test_req.value8 = cJSON_GetObjectItem(root, "Value8")->valueint;

        ev_raise_event(EV_START_TEST_REQ, &ev_data);

       	generateResponse(&entry, rspMsg, "MemoryTest", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
    }
    /* else an error has been added to rspMsg by RequiredFieldsArePresent() */
    else
    	generateResponse(&entry, rspMsg, "MemoryTest", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);

}
#endif /* MEMORYTEST */


/* ----------------------------------------------------------------------------------------[on_TestPrintReq]-- */
void on_TestPrintReq(UINT32 handle, cJSON *root)
{
    UINT8     pMsgData[2];    /* send 2 bytes- 1st is event, 2nd is 0 */

    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

	generateResponse(&entry, rspMsg, "TestPrint", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);

    // Initiate test print
    pMsgData[0] = OIT_PRINT_ENV_TEST;
    pMsgData[1] = 0;
    (void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);

}

//TODO JAH Remove me when real indicia printing interface is ready
/* --------------------------------------------------------------------------------------------[on_PrintReq]-- */
void on_PrintReq(UINT32 handle, cJSON *root)
{
    UINT8     pMsgData[2];    /* send 2 bytes- 1st is event, 2nd is 0 */
    unsigned int pv = 0;

    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON *postageVal = cJSON_GetObjectItem(root, "PostageVal");
    if (postageVal)
    {
        pv = postageVal->valueint;
    }

    cJSON_AddNumberToObject(rspMsg, "PostageVal", pv);

	generateResponse(&entry, rspMsg, "Print", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);

    // Initiate test print
    (void)smgr_set_setting_data(SETTING_ITEM_POSTAGE, &pv, sizeof(UINT32));

    pMsgData[0] = OIT_ENTER_PRINT_RDY;
    pMsgData[1] = 0;
    (void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);
#ifdef NO_PRINTER
	OSWakeAfter(620);
	pMsgData[0] = 0;
	(void)OSSendIntertask(CM, BJCTRL, 0x80, BYTE_DATA, pMsgData, 2);
#endif
}

/* ----------------------------------------------------------------------------------------[on_SetNetworkConfigReq]-- */
required_fields_tbl_t required_fields_tbl_SetNetworkConfigReq =
    {
    "SetNetworkConfigReq", { "InterfaceName"
                           , "UseDhcp"
                           , NULL }
    };

required_fields_tbl_t required_fields_tbl_SetNetworkConfigReq_Static_IP=
    {
    "SetNetworkConfigReq", { "IP"
                           , "NetMask"
                           , NULL }
    };

void on_SetNetworkConfigReq(UINT32 handle, cJSON *root)
{
    cJSON           *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    STATUS           status;

    cJSON_AddStringToObject(rspMsg, "MsgID", "SetNetworkConfigRsp");

    if (cJSON_HasObjectItem(root, "SeqNr"))
        cJSON_AddNumberToObject(rspMsg, "SeqNr", cJSON_GetObjectItem(root, "SeqNr")->valueint);

    if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_SetNetworkConfigReq))
    {
        if (cJSON_GetObjectItem(root, "UseDhcp")->valueint == 0)
        {
            // request to use static ip. make sure we have all the info
            if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_SetNetworkConfigReq_Static_IP))
            {
                UINT8 ip_addr[4];
                UINT8 netmask[4];
                CHAR  *ip_addr_str = cJSON_GetObjectItem(root, "IP")->valuestring;
                CHAR  *netmask_str = cJSON_GetObjectItem(root, "NetMask")->valuestring;
                CHAR  errStr[100];

                status = NU_Inet_PTON(NU_FAMILY_IP, ip_addr_str, ip_addr);

                if (status != NU_SUCCESS)
                {
                    sprintf(errStr, "Error parsing IP [%s]", ip_addr_str);
                    AddError(rspMsg, "SetNetworkConfigReq", errStr);
                }
                else
                {
                    status = NU_Inet_PTON(NU_FAMILY_IP, netmask_str, netmask);
                    if (status != NU_SUCCESS)
                    {
                        sprintf(errStr, "Error parsing NetMask [%s]", netmask_str);
                        AddError(rspMsg, "SetNetworkConfigReq", errStr);
                    }
                    else
                    {
                        // now change network stack to use fixed IP
                        dbgTrace(DBG_LVL_INFO, " TODO: change network stack to use static ip [%s] and netmask [%s]\b", ip_addr_str, netmask_str);
                    }
                }

            }
            /* else an error has been added to rspMsg by RequiredFieldsArePresent() */

        }
    }
    /* else an error has been added to rspMsg by RequiredFieldsArePresent() */

    addEntryToTxQueue(&entry, root, "  MemoryTestReq: Added MemoryTestRsp to tx queue. status=%d" CRLF);
}


///////////
required_fields_tbl_t required_fields_tbl_SetDefaulRouteReq_Static_IP=
    {
    "SetDefaultRouteReq", { "IP"
                            , NULL }
    };
#define MAX_RESPONSE_SIZE		100
void on_SetDefaultRouteReq(UINT32 handle, cJSON *root)
{
    cJSON           	*rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry 	entry  	=  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    STATUS           	status;
    CHAR				strResp[MAX_RESPONSE_SIZE];
    INT					iPass;

    cJSON_AddStringToObject(rspMsg, "MsgID", "SetDefaultRouteRsp");

    if (cJSON_HasObjectItem(root, WS_KEY_SEQNUMBER))
        cJSON_AddNumberToObject(rspMsg, WS_KEY_SEQNUMBER, cJSON_GetObjectItem(root, WS_KEY_SEQNUMBER)->valueint);

    if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_SetDefaulRouteReq_Static_IP))
    {
    	UINT8 ip[MAX_ADDRESS_SIZE] ;
    	CHAR *ptrIP 	= cJSON_GetObjectItem(root, KEY_DEFROUTE_IP)   ? cJSON_GetObjectItem(root, KEY_DEFROUTE_IP)  ->valuestring : "";
    	status = NU_RETURN_ERROR;
    	memset(ip, 0, MAX_ADDRESS_SIZE);

    	// if the user selects DEFAULT use a fixed value for the gateway address
    	if(stricmp(ptrIP, VALUE_TABLET_IP_DEFAULT) == 0)
    	{
    		ptrIP = DEFAULT_FIXED_TABLET_IP ;
    	}

		// put the text ip into our array, assume IP4 used index 3,2,1,0
    	status = stringToIP4Array(ip, ptrIP);

    	if(status == NU_SUCCESS)
    	{
    		status = setDefaultGateway(ip) ;
    	}

    	if(status != NU_SUCCESS)
    	{
    		snprintf(strResp, MAX_RESPONSE_SIZE, "Unable to set requested default route value %s [%d]", ptrIP, status);
    		cJSON_AddStringToObject(rspMsg, "Error",  strResp);
    	}

    	// set any dns server addresses supplied in an array key DNS
    	cJSON *array ;
    	if((status == NU_SUCCESS) && ((array = cJSON_GetObjectItem(root, KEY_DEFROUTE_DNS)) != NULL))
		{
    		for(iPass = 0 ; iPass < cJSON_GetArraySize(array) ; iPass++)
    		{
    			CHAR *ptrDNS = cJSON_GetArrayItem(array, iPass)->valuestring ;
    	    	if((status == NU_SUCCESS) && (ptrDNS != 0) && ((status = stringToIP4Array(ip, ptrDNS)) == NU_SUCCESS))
    	    	{
					// delete it if present, no worries if it is not...
					NU_Delete_DNS_Server2((UINT8 *)&ip, NU_FAMILY_IP);

					status = NU_Add_DNS_Server2(ip, DNS_ADD_TO_END, NU_FAMILY_IP);
					if(status != NU_SUCCESS)
					{
						snprintf(strResp, MAX_RESPONSE_SIZE, "Unable to set requested DNS%d value %s [%d]", iPass, ptrDNS, status);
						cJSON_AddStringToObject(rspMsg, "Error",  strResp);
					}
    	    	}
    		}
		}

    	if(status == NU_SUCCESS)
    	{
    		cJSON_AddStringToObject(rspMsg, "Status",  "Success");
    	}
    }
    addEntryToTxQueue(&entry, root, "  SetDefaultRouteReq: Added SetDefaultRouteRsp to tx queue. status=%d" CRLF);
}



/* ---------------------------------------------------------------------------------------------[num_tasks_waiting]-- */
UNSIGNED num_tasks_waiting(NU_PIPE *pipe)
{
    CHAR pipe_name[8];
    VOID *start_address;
    UNSIGNED pipe_size;
    UNSIGNED available;
    UNSIGNED messages;
    OPTION message_type;
    UNSIGNED message_size;
    OPTION suspend_type;
    UNSIGNED tasks_suspended;
    NU_TASK *first_task;
    STATUS status;

    status = NU_Pipe_Information(pipe, pipe_name, &start_address, &pipe_size, &available, &messages, &message_type, &message_size, &suspend_type, &tasks_suspended, &first_task);


    if (status != NU_SUCCESS)
        tasks_suspended = 0;

    return tasks_suspended;
}


/* -----------------------------------------------------------------------------------[Send_GetTabletUSBDevicesReq]-- */
NU_PIPE *Send_GetTabletUSBDevicesReq()
{
    cJSON *req = cJSON_CreateObject();
    cJSON_AddStringToObject(req, "MsgID", "GetTabletUSBDevicesReq");
    Send_JSON(req,BROADCAST_MASK);
    return &wsox_tablet_usb_devices_rsp_pipe;
}

/* ------------------------------------------------------------------------------------[Send_TabletUsbWriteTestReq]-- */
NU_PIPE *Send_TabletUsbWriteTestReq(uint8_t deviceID, size_t size)
{
    cJSON *req = cJSON_CreateObject();
    cJSON_AddStringToObject(req, "MsgID", "TabletUsbWriteTestReq");
    cJSON_AddNumberToObject(req, "deviceID", deviceID);
    cJSON_AddNumberToObject(req, "writeSize", size);
    Send_JSON(req,BROADCAST_MASK);
    return &wsox_tablet_usb_write_test_rsp_pipe;
}

/* -------------------------------------------------------------------------------------[Send_GetTabletVersionsReq]-- */
NU_PIPE *Send_GetTabletVersionsReq()
{
    cJSON *req = cJSON_CreateObject();
    cJSON_AddStringToObject(req, "MsgID", "GetTabletVersionsReq");
    Send_JSON(req,BROADCAST_MASK);
    return &wsox_tablet_versions_rsp_pipe;
}

/* ---------------------------------------------------------------------------------------[Send_GetTabletStatusReq]-- */
NU_PIPE *Send_GetTabletStatusReq()
{
    cJSON *req = cJSON_CreateObject();
    cJSON_AddStringToObject(req, "MsgID", "GetTabletStatusReq");
    Send_JSON(req,BROADCAST_MASK);
    return &wsox_tablet_status_rsp_pipe;
}


/* -------------------------------------------------------------------------------------[on_GetTabletUSBDevicesRsp]-- */
void on_GetTabletUSBDevicesRsp(UINT32 handle, cJSON *root)
{
    cJSON *Devices;
    int i;
    char rspStr[1024];
    size_t rspLen = 0;

    if (!cJSON_HasObjectItem(root, "Devices"))
    {
        dbgTrace(DBG_LVL_INFO, "on_GetTabletUSBDevicesRsp: Missing Devices field!\r\n");
        return;
    }

    Devices = cJSON_GetObjectItem(root, "Devices");

    rspStr[0] = '\0';

    if (verbosity > 2)
    {
        dbgTrace(DBG_LVL_INFO, "on_GetTabletUSBDevicesRsp:\r\n");
        dbgTrace(DBG_LVL_INFO, "  ID  VID     PID     Class  SubClass\r\n");
        dbgTrace(DBG_LVL_INFO, "  --  ------  ------  -----  --------\r\n");
    }
    for( i=0; i < cJSON_GetArraySize(Devices); i++)
    {
        cJSON *Device = cJSON_GetArrayItem(Devices, i);

        cJSON *ID, *VID, *PID, *Class, *SubClass;

        ID       = cJSON_GetObjectItem(Device, "ID"      );
        VID      = cJSON_GetObjectItem(Device, "VID"     );
        PID      = cJSON_GetObjectItem(Device, "PID"     );
        Class    = cJSON_GetObjectItem(Device, "Class"   );
        SubClass = cJSON_GetObjectItem(Device, "SubClass");
        if (verbosity > 2)
            dbgTrace(DBG_LVL_INFO, "  %2.2d  0x%04X  0x%04X  0x%02X   0x%02X\r\n",
                    ID       -> valueint,
                    VID      -> valueint,
                    PID      -> valueint,
                    Class    -> valueint,
                    SubClass -> valueint
                );

        sprintf(rspStr+rspLen,   "%d,0x%04X,0x%04X,0x%02X,0x%02X;",
                ID       -> valueint,
                VID      -> valueint,
                PID      -> valueint,
                Class    -> valueint,
                SubClass -> valueint
              );
        rspLen = strlen(rspStr);
    }

    /* check to see if there are one or more tasks waiting on the pipe */
    if (num_tasks_waiting(&wsox_tablet_usb_devices_rsp_pipe) > 0)
    {
        if (verbosity > 1)
            dbgTrace(DBG_LVL_INFO, "on_GetTabletUSBDevicesRsp: sending response to pipe\r\n");
        NU_Send_To_Pipe(&wsox_tablet_usb_devices_rsp_pipe, rspStr, rspLen+1, NU_SUSPEND);
    }

}

/* -----------------------------------------------------------------------------------[on_GetTabletUsbWriteTestRsp]-- */
required_fields_tbl_t required_fields_tbl_GetTabletUsbWriteTestRsp =
    {
    "GetTabletUsbWriteTestRsp", { "StatusCode"
                                , "StatusString"
                                , NULL }
    };

void on_GetTabletUsbWriteTestRsp(UINT32 handle, cJSON *root)
{
    if (!RequiredFieldsArePresent(root, NU_NULL, &required_fields_tbl_GetTabletUsbWriteTestRsp))
    {
        /* an error is printed to serial debug port */
        return;
    }

    dbgTrace(DBG_LVL_INFO, "on_GetTabletUsbWriteTestRsp:\r\n"
           "  StatusCode   = %d\r\n"
           "  StatusString = %s\r\n",
           cJSON_GetObjectItem(root, "StatusCode"   )->valueint,
           cJSON_GetObjectItem(root, "StatusString" )->valuestring);

    /* check to see if there are one or more tasks waiting on the pipe */
    if (num_tasks_waiting(&wsox_tablet_usb_write_test_rsp_pipe) > 0)
    {
        tablet_usb_write_test_rsp_msg_t rspMsg;
        rspMsg.write_test_result_code = cJSON_GetObjectItem(root, "StatusCode"   )->valueint;

        if (verbosity > 1)
            dbgTrace(DBG_LVL_INFO, "on_GetTabletUsbWriteTestRsp: sending response to pipe\r\n");
        NU_Send_To_Pipe(&wsox_tablet_usb_write_test_rsp_pipe, &rspMsg, sizeof(rspMsg), NU_SUSPEND);
    }

}

/* ---------------------------------------------------------------------------------------[on_GetTabletVersionsRsp]-- */
required_fields_tbl_t required_fields_tbl_GetTabletVersionsRsp =
    {
    "GetTabletVersionsRsp", { "Versions"
                             , NULL }
    };

void on_GetTabletVersionsRsp(UINT32 handle, cJSON *root)
{
    char *versions;
    if (!RequiredFieldsArePresent(root, NU_NULL, &required_fields_tbl_GetTabletVersionsRsp))
    {
        /* an error is printed to serial debug port */
        return;
    }

    versions = cJSON_GetObjectItem(root, "Versions" )->valuestring;
    dbgTrace(DBG_LVL_INFO, "on_GetTabletVersionsRsp:\r\n"
           "  Versions = %s\r\n", versions );

    /* check to see if there are one or more tasks waiting on the pipe */
    if (num_tasks_waiting(&wsox_tablet_versions_rsp_pipe) > 0)
    {
        if (verbosity > 1)
            dbgTrace(DBG_LVL_INFO, "on_GetTabletVersionsRsp: sending response to pipe\r\n");
        NU_Send_To_Pipe(&wsox_tablet_versions_rsp_pipe, versions, strlen(versions)+1, NU_SUSPEND);
    }

}

/* -----------------------------------------------------------------------------------------[on_GetTabletStatusRsp]-- */
required_fields_tbl_t required_fields_tbl_GetTabletStatusRsp =
    {
    "GetTabletStatusRsp", { "StatusString"
                             , NULL }
    };

void on_GetTabletStatusRsp(UINT32 handle, cJSON *root)
{
    char *status;
    if (!RequiredFieldsArePresent(root, NU_NULL, &required_fields_tbl_GetTabletStatusRsp))
    {
        /* an error is printed to serial debug port */
        return;
    }

    status = cJSON_GetObjectItem(root, "StatusString" )->valuestring;

    dbgTrace(DBG_LVL_INFO, "on_GetTabletStatusRsp:\r\n"
           "  StatusString = %s\r\n", status);

    /* check to see if there are one or more tasks waiting on the pipe */
    if (num_tasks_waiting(&wsox_tablet_status_rsp_pipe) > 0)
    {
        if (verbosity > 1)
            dbgTrace(DBG_LVL_INFO, "on_GetTabletStatusRsp: sending response to pipe\r\n");
        NU_Send_To_Pipe(&wsox_tablet_status_rsp_pipe, status, strlen(status)+1, NU_SUSPEND);
    }
}


/* --------------------------------------------------------------------------------------------------[SetBaseState]-- */
const char *baseState = "Idle";

void SetBaseState(const char *state)
{
    baseState = state;
}


/* --------------------------------------------------------------------------------------------[on_SetBaseStateReq]-- */
required_fields_tbl_t required_fields_tbl_SetBaseStateReq= 
    {
    "SetBaseStateReq", { "State"
                       , NULL } 
    };


typedef enum
{
    BSTATE_INVALID = -1,
    BSTATE_IDLE = 0,
    BSTATE_READY_TO_PRINT,
    BSTATE_MAINTENANCE_IN_PROGRESS,
    BSTATE_NOT_READY,
    BSTATE_ERROR,
} bstate_t;


void on_SetBaseStateReq(UINT32 handle, cJSON *root)
{
    cJSON           *rspMsg          = cJSON_CreateObject();
    WSOX_Queue_Entry entry           = { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    const char      *valid_states [] = { "Idle", "ReadyToPrint", "MaintInProgress", "NotReady", "Error"};
    char            *requested_state;
    bool             valid_state     = false;
    bstate_t         state_id        = BSTATE_INVALID;

    if (!RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_SetBaseStateReq))
    {
    	generateResponse(&entry, rspMsg, "SetBaseState", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);

    }
    else
    {
        int  i;

        requested_state = cJSON_GetObjectItem(root, "State")->valuestring;
        for(i=0; i<NELEMENTS(valid_states) && !valid_state; i++)
        {
            if (0 == NCL_Stricmp(valid_states[i], requested_state))
            {
                state_id = i;
                valid_state = true;
            }
        }

        if(valid_state)
        	generateResponse(&entry, rspMsg, "SetBaseState", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
        else
        	generateResponse(&entry, rspMsg, "SetBaseState", api_status_to_string_tbl[API_STATUS_UNRECOGNIZED_STATE], root);
    }


    if (valid_state)
    {
        SetBaseState(valid_states[state_id]);

        switch(state_id)
        {
            case BSTATE_READY_TO_PRINT:
                {

                }
                break;

            default:
                {
                }
                break;
        }

        
    }
}


//this is for testing purpose
//int TestTrm(int numRecords);
//trm_mem_t *get_trm_mem(void);
//extern char tr_xml[];
char printbuf[512];
//void on_GetTRsReq(UINT32 handle, cJSON *root)
//{
//	STATUS status = SUCCESS;
//	int32_t xml_len = 0;
//	int remain_buff_len = 0;
//	int offset = 0;
//	int StartID = 0;
//	int Count = 0;
//	BOOL msgStat = TRUE;
//	int cnt;
//    required_fields_t rqd_fields;
//
//    trm_mem_t *p_trm_mem ;
//
//	cJSON *rspMsg = cJSON_CreateObject();
//	cJSON *invalid = cJSON_CreateArray();
//
//	WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
//
//	static const char *valid_fieldnames [] =
//	                { "StartID", "Count", "AddTR", "InitTRM"};
//
//	static required_fields_tbl_t required_fields_tbl =
//	    {
//	    	"GetTRsReq", {
//							"StartID",
//							"Count",
//							NULL }
//	};
//
//	p_trm_mem = get_trm_mem();
//
//	if (!RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl))
//	{
//		//Add missing required item to invalid list
//		cJSON_AddItemToArray(invalid, CreateStringItem(valid_fieldnames[0], ""));
//		msgStat = FALSE;
//	}
//
//	if(msgStat == TRUE)
//	{
//		// validate fields - StartID
//		if (cJSON_HasObjectItem(root, valid_fieldnames[3]))
//		{
//			Count = cJSON_GetObjectItem(root, valid_fieldnames[0])->valueint;
//			InitTrmMem();
//		}
//
//		if (cJSON_HasObjectItem(root, valid_fieldnames[0]))
//		{
//			StartID = cJSON_GetObjectItem(root, valid_fieldnames[0])->valueint;
//		}
//
//
//		//Add TR if required
//		if (cJSON_HasObjectItem(root, valid_fieldnames[2]))
//		{
//			Count = cJSON_GetObjectItem(root, valid_fieldnames[2])->valueint;
//			TestTrm(Count);
//
//		}
//
//		// validate fields - Count
//		if (cJSON_HasObjectItem(root, valid_fieldnames[1]))
//		{
//			Count = cJSON_GetObjectItem(root, valid_fieldnames[1])->valueint;
//		}
//
//		memset(&rqd_fields, 0xFF, sizeof(rqd_fields));
//		memset(&tr_xml[0], 0, TRM_XML_BUFFER_SIZE);
//
//		remain_buff_len = TRM_XML_BUFFER_SIZE;
//
//		for(cnt = 0; cnt < Count ; cnt ++)
//		{
//			xml_len = 0;
//			if(p_trm_mem->trans_rec[StartID + cnt].rec_state == TRM_UNUSED)
//				p_trm_mem->trans_rec[StartID + cnt].rec_state = TRM_POPULATING;
//
//			status = generate_tr_xml(&p_trm_mem->trans_rec[StartID + cnt],
//											tr_xml + offset,
//											remain_buff_len,
//											&xml_len,
//											rqd_fields);
//
//			remain_buff_len -= xml_len;
//			offset += xml_len;
//		}
//
//	}
//
//	cJSON_AddStringToObject(rspMsg, "MsgID", "GetTRsRsp");
//	cJSON_AddStringToObject(rspMsg, "Status", "Accepted");
//	if(remain_buff_len < TRM_XML_BUFFER_SIZE)
//	{
//
//		tr_xml[offset] = '\0';
//		xml_len = offset;
//		offset = 0;
//
//		while(xml_len > 0)
//		{
//			memset(printbuf, 0, sizeof(printbuf));
//			snprintf(printbuf, 512, "%s", tr_xml + offset );
//
//			dbgTrace(DBG_LVL_INFO, printbuf);
//
//			offset += 512;
//			xml_len -= 512;
//		}
//
//		//cJSON_AddStringToObject(rspMsg, "xml", tr_xml );
//	}
//
//	addEntryToTxQueue(&entry, "GetTRsReq: Added GetTRsReq to tx queue. status=%d" CRLF);
//}


BOOL strDateTimeToDATETIME(DATETIME *dateTimePtr, char *asciiDatePtr)
{
	char scratchPad[32];

    if(!dateTimePtr || !asciiDatePtr)
        throwException("NULL pointer");

    memset(dateTimePtr, 0, sizeof(DATETIME));    //clean

    memset(scratchPad, 0, sizeof(scratchPad));
    memcpy(scratchPad, asciiDatePtr, 2);
    dateTimePtr->bCentury = atoi(scratchPad);

    memset(scratchPad, 0, sizeof(scratchPad));
    memcpy(scratchPad, asciiDatePtr + 2, 2);
    dateTimePtr->bYear = atoi(scratchPad);

    memset(scratchPad, 0, sizeof(scratchPad));
    memcpy(scratchPad, asciiDatePtr + 4, 2);
    dateTimePtr->bMonth = atoi(scratchPad);

    memset(scratchPad, 0, sizeof(scratchPad));
    memcpy(scratchPad, asciiDatePtr + 6, 2);
    dateTimePtr->bDay = atoi(scratchPad);

    memset(scratchPad, 0, sizeof(scratchPad));
	memcpy(scratchPad, asciiDatePtr + 8, 2);
	dateTimePtr->bHour = atoi(scratchPad);

	memset(scratchPad, 0, sizeof(scratchPad));
	memcpy(scratchPad, asciiDatePtr + 10, 2);
	dateTimePtr->bMinutes = atoi(scratchPad);

	memset(scratchPad, 0, sizeof(scratchPad));
	memcpy(scratchPad, asciiDatePtr + 12, 2);
	dateTimePtr->bSeconds = atoi(scratchPad);

    dateTimePtr->bDayOfWeek = 1;

    return fnValidDate(dateTimePtr);
}

//#define TEST_TRM

#ifdef TEST_TRM

//TODO : this is only for testing
static char *gp_tr_xml = NULL;
//static int32_t reqd_buf_len;
//static int32_t g_xml_len;


extern unsigned long fsm_calccrc32(unsigned long  dwcrc, const void *vbuf, unsigned long  dwlen);
extern trm_mem_t  RAMTrmArea;
void on_TRMReq(UINT32 handle, cJSON *root)
{
	STATUS status = SUCCESS;


	//int remain_buff_len = 0;
	int offset = 0;
	//int StartID = 0;
	int Count = 0;
	BOOL msgStat = TRUE;
	Byte bGenXML = false;
	BOOL bSaveXML = false;
	char bClean = 0;
	//BOOL bCompress = false;
	//BOOL bCompress2 = false;
	BOOL bInitClean = false;
	BOOL bCompressWithHeader = false;
	BOOL bProcessTx = false;
	BOOL bCompressFRAM = false;
	BOOL bSaveFRAM = false;
	BOOL bReadTxFile = false;
	BOOL bDeleteTxFiles = false;
	BOOL bPrepareMidnight = false;
	BOOL bXmlFromFiles = false;
	BOOL bDeleteUploadedFiles = false;
	BOOL UploadedSuccess = false;
	BOOL ClearDcapGUID = false;
	BOOL bMidnight = false;
	uint8_t bGetTrminfo = 0;
	ulong uploadTimeSec = 0;
	BOOL bTmrEnable = false;
	int UploadWaitSec = 0;
	int uploadPeriod = 0;
	static DATETIME prevTime;
	DATETIME curTime;
	int len;
	int uploadCount = 0;
	char UploadWaitCount = 2;
	BOOL passed = false;
	//int cnt;
    required_fields_t rqd_fields;
    int start_idx, end_idx;
    char fileName[TRM_MAX_FILE_NAME_SIZE];

    pkg_info_t pkg_info;

    DATETIME dateTimePeriod;
    BOOL ClearPrevUploadDate = FALSE;

    //trm_mem_t *p_trm_mem ;

	cJSON *rspMsg = cJSON_CreateObject();

	WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

	//cJSON_AddStringToObject(rspMsg, "MsgID", "TRMRsp");

	static const char *valid_fieldnames [] =

	                {"InitTRM",					//0
	                "StartID", 					//1
					"Count", 					//2
					"AddTR", 					//3
					"SaveXML", 					//4
					"GenXML", 					//5
					"Upload", 					//6
					"Clean", 					//7
					"ProcessTx", 			//8
					"CompressWithHeader",		//9
					"CompressFRAM",				//10
					"SaveFRAM",					//11
					"ReadTxFile",				//12
					"DeleteTxFiles",			//13
					"PrepareMidnight",			//14
					"XmlFromFiles",				//15
					"DeleteUploadedFiles",		//16
					"UploadedSuccess",			//17
					"GetTrmInfo",				//18
					"UploadTime",				//19
					"TimerEnable",				//20
					"UploadPeriod",				//21
					"UploadDateTime",			//22
					"UploadWaitSec",			//23
					"UploadWaitCount",			//24
					"ClearPrevUploadDate",		//25
					"ClearDcapGUID",			//26
					"UploadFileName",			//27
					"Midnight"					//28
	                };

	static required_fields_tbl_t required_fields_tbl =
	    {
	    	"TRMReq", {
							NULL }
	};

	//p_trm_mem = get_trm_mem();

	if(msgStat == TRUE)
	{
		// validate fields - "InitTRM",
		if (cJSON_HasObjectItem(root, valid_fieldnames[0]))
		{
			bInitClean = true;
			Count = cJSON_GetObjectItem(root, valid_fieldnames[0])->valueint;
			//InitTrmMem();

			//trmUpdateDcapPackageInfo();
		}

		//StartID
//		if (cJSON_HasObjectItem(root, valid_fieldnames[1]))
//		{
//			StartID = cJSON_GetObjectItem(root, valid_fieldnames[1])->valueint;
//		}

		// validate fields - Count
		if (cJSON_HasObjectItem(root, valid_fieldnames[2]))
		{
			Count = cJSON_GetObjectItem(root, valid_fieldnames[2])->valueint;
		}

		//Add TR if required
		if (cJSON_HasObjectItem(root, valid_fieldnames[3]))
		{
			int Count = cJSON_GetObjectItem(root, valid_fieldnames[3])->valueint;
			TestTrm(Count);

		}

		// validate fields - SaveXML
		if (cJSON_HasObjectItem(root, valid_fieldnames[4]))
		{
			bSaveXML = cJSON_GetObjectItem(root, valid_fieldnames[4])->valueint;
		}

		// validate fields - GenXML
		if (cJSON_HasObjectItem(root, valid_fieldnames[5]))
		{
			bGenXML = cJSON_GetObjectItem(root, valid_fieldnames[5])->valueint;
		}

		// validate fields - Upload
		if (cJSON_HasObjectItem(root, valid_fieldnames[6]))
		{
			uploadCount = cJSON_GetObjectItem(root, valid_fieldnames[6])->valueint;

		}

		// validate fields - Upload
		if (cJSON_HasObjectItem(root, valid_fieldnames[7]))
		{
			bClean = cJSON_GetObjectItem(root, valid_fieldnames[7])->valueint;

		}

		//ProcessTx
		if (cJSON_HasObjectItem(root, valid_fieldnames[8]))
		{
			bProcessTx = cJSON_GetObjectItem(root, valid_fieldnames[8])->valueint;

		}

		//CompressWithHeader
		if (cJSON_HasObjectItem(root, valid_fieldnames[9]))
		{
			bCompressWithHeader = cJSON_GetObjectItem(root, valid_fieldnames[9])->valueint;
		}

		//CompressFRAM
		if (cJSON_HasObjectItem(root, valid_fieldnames[10]))
		{
			bCompressFRAM = cJSON_GetObjectItem(root, valid_fieldnames[10])->valueint;
		}

		//SaveFRAM
		if (cJSON_HasObjectItem(root, valid_fieldnames[11]))
		{
			bSaveFRAM = cJSON_GetObjectItem(root, valid_fieldnames[11])->valueint;
		}

		//ReadTxFile
		if (cJSON_HasObjectItem(root, valid_fieldnames[12]))
		{
			bReadTxFile = cJSON_GetObjectItem(root, valid_fieldnames[12])->valueint;
		}

		//"DeleteTxFiles",			//13
		if (cJSON_HasObjectItem(root, valid_fieldnames[13]))
		{
			bDeleteTxFiles = cJSON_GetObjectItem(root, valid_fieldnames[13])->valueint;
		}

		//"PrepareMidnight",			//14
		if (cJSON_HasObjectItem(root, valid_fieldnames[14]))
		{
			bPrepareMidnight = cJSON_GetObjectItem(root, valid_fieldnames[14])->valueint;
		}
		//"XmlFromFiles",			//15
		if (cJSON_HasObjectItem(root, valid_fieldnames[15]))
		{
			bXmlFromFiles = cJSON_GetObjectItem(root, valid_fieldnames[15])->valueint;
		}
		//bDeleteUploadedFiles			//15
		if (cJSON_HasObjectItem(root, valid_fieldnames[16]))
		{
			bDeleteUploadedFiles = cJSON_GetObjectItem(root, valid_fieldnames[16])->valueint;
		}

		//bSetUploadedSuccess
		if (cJSON_HasObjectItem(root, valid_fieldnames[17]))
		{
			UploadedSuccess = cJSON_GetObjectItem(root, valid_fieldnames[17])->valueint;
		}
		//GetTrmInfo
		if (cJSON_HasObjectItem(root, valid_fieldnames[18]))
		{
			bGetTrminfo = cJSON_GetObjectItem(root, valid_fieldnames[18])->valueint;
		}

		//"UploadTime"				//19
		if (cJSON_HasObjectItem(root, valid_fieldnames[19]))
		{
			uploadTimeSec = cJSON_GetObjectItem(root, valid_fieldnames[19])->valueint;
			dbgTrace(DBG_LVL_INFO, "TrmReq - UploadTime sec: %d ", uploadTimeSec);
		}
		//TimerEnable
		if (cJSON_HasObjectItem(root, valid_fieldnames[20]))
		{
			bTmrEnable = cJSON_GetObjectItem(root, valid_fieldnames[20])->valueint;
			dbgTrace(DBG_LVL_INFO, "TrmReq - TimerEnable: %d ", bTmrEnable);
		}
		//"UploadPeriod",				//21
		if (cJSON_HasObjectItem(root, valid_fieldnames[21]))
		{
			uploadPeriod = cJSON_GetObjectItem(root, valid_fieldnames[21])->valueint;

		}

		//"UploadDateTime",			//22
		if (cJSON_HasObjectItem(root, valid_fieldnames[22]))
		{
			char pTempDate[32];
			cJSON *jsonUploadTime = cJSON_GetObjectItem(root, valid_fieldnames[22]);
			if(jsonUploadTime)
			{
				memset(pTempDate, 0x0, sizeof(pTempDate));
				memcpy(pTempDate, jsonUploadTime->valuestring, strlen(jsonUploadTime->valuestring));
				passed = strDateTimeToDATETIME(&dateTimePeriod, pTempDate);

			}
		}

		//UploadWaitSec
		if (cJSON_HasObjectItem(root, valid_fieldnames[23]))
		{
			UploadWaitSec = cJSON_GetObjectItem(root, valid_fieldnames[23])->valueint;
		}

		//"UploadWaitCount"			//24
		if (cJSON_HasObjectItem(root, valid_fieldnames[24]))
		{
			UploadWaitCount = cJSON_GetObjectItem(root, valid_fieldnames[24])->valueint;
		}

		//ClearPrevUploadDate	25
		if (cJSON_HasObjectItem(root, valid_fieldnames[25]))
		{
			ClearPrevUploadDate = cJSON_GetObjectItem(root, valid_fieldnames[25])->valueint;
		}

		//ClearDcapGUID			//26
		if (cJSON_HasObjectItem(root, valid_fieldnames[26]))
		{
			ClearDcapGUID = cJSON_GetObjectItem(root, valid_fieldnames[26])->valueint;
		}

		//UploadFileName		//27
		if (cJSON_HasObjectItem(root, valid_fieldnames[27]))
		{
			cJSON *jsonUploadFileName = cJSON_GetObjectItem(root, valid_fieldnames[27]);
			memset(CMOS2TrmDcapFileName, 0, sizeof(fileName));
			memcpy(CMOS2TrmDcapFileName, jsonUploadFileName->valuestring, strlen(jsonUploadFileName->valuestring));

			dbgTrace(DBG_LVL_ERROR, "TrmReq: CMOS2TrmDcapFileName : %s ", CMOS2TrmDcapFileName);
		}

		//Midnight
		if (cJSON_HasObjectItem(root, valid_fieldnames[28]))
		{

			dbgTrace(DBG_LVL_ERROR, "TrmReq: Midnight Event : ");
			bMidnight = TRUE;
		}

		memset(&rqd_fields, 0xFF, sizeof(rqd_fields));


		//remain_buff_len = TRM_XML_BUFFER_SIZE;

	    memset(&pkg_info, 0, sizeof(pkg_info));

	    if(bInitClean >= 1)
	    {
	    	//InitTrmMem();
	    	trmSetUploadState(TRM_STATE_WAITING_FOR_UPLOAD_CONFIRMATION);
	    	trmProcessTRUploadConfirmation();
	    	trmDeleteAllTxFiles();
	    	trmDeleteUploadedDcapFiles();
	    	InitTrmMem();

//	    	if(Count == 0xFF)
//	    		InitTrmMem();
//	    	else if (Count > 1)
//	    		//This removes TR flash files
//	    		trmProcessTransactionRecords(true);
//	    	else
//	    		trmProcessTRUploadConfirmation();
	    }


	    if(bGenXML)
	    {
	    	if(prevTime.bCentury != 0)
	    		  ReadRTClock(&prevTime);

	    	ReadRTClock(&curTime);


	    	if(bGenXML == 1)
	    	{
//	    		//this is to stop multiple clicks on tablet
//	    		if( (trmGetUploadState() == TRM_NONE) ||
//	    				(trmGetUploadState() == TRM_STATE_UPLOAD_REQUIRED) ||
//	    				((curTime.bSeconds - prevTime.bSeconds) > 5) ||
//	    				((curTime.bMinutes - prevTime.bMinutes) > 1) ||
//						((curTime.bHour - prevTime.bHour) > 1) )
//	    		{
//	    			//trmSetUploadState(TRM_STATE_UPLOAD_REQUIRED);
//	    			dbgTrace(DBG_LVL_ERROR, "TrmReq : Upload starting \r\n");
//	    			status = trmProcessTRUpload();
//
//	    			memcpy(&prevTime, &curTime, sizeof(curTime));
//	    		}
//	    		else
//	    		{
//	    			dbgTrace(DBG_LVL_ERROR, "TrmReq : Waiting for upload to finish - state:%d,  Time-%02d:%02d:%02d \r\n", trmGetUploadState(), curTime.bHour, curTime.bMinutes, curTime.bSeconds);
//	    			dbgTrace(DBG_LVL_ERROR, "TrmReq : Waiting for upload to finish - state:%d,  Time-%02d:%02d:%02d \r\n", trmGetUploadState(), prevTime.bHour, prevTime.bMinutes, prevTime.bSeconds);
//	    			NU_Sleep(500);
//	    		}

//	    		trmSetUploadState(TRM_STATE_UPLOAD_REQUIRED);
//				dbgTrace(DBG_LVL_ERROR, "TrmReq : Upload starting n");
//				status = trmProcessTRUpload();

//	    		if (!connectedToInternet())
//				{
//					return generateResponse(&entry, rspMsg, "TRM", api_status_to_string_tbl[API_STATUS_NO_INTERNET], root);
//				}

				//Send request to the background
				fnUploadTrxPkgFile (0, 0, 0);

				//return generateResponse(&entry, rspMsg, "TRM", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
	    	}
	    	else
	    	{
				//status = trmProcessTransactionRecords(true);

				if(status == SUCCESS)
					dbgTrace(DBG_LVL_ERROR, "TrmReq: trmProcessTransactionRecords : Success %d ", status);
				else
				{
					//Donot upload
					uploadCount = 0;
					dbgTrace(DBG_LVL_ERROR, "TrmReq: trmProcessTransactionRecords : failed %d \r\n", status);
				}
	    	}

	    }

	    if(UploadedSuccess)
	    {
			if(UploadedSuccess == 1)
			{
				trmProcessTRUploadConfirmation();
			}
			else if(UploadedSuccess > 1)
				trmUpdateDcapUploadSuccess();
	    }

		if(bSaveFRAM)
		{
			//status = trmWriteFramTxRecords(true);
			trmWriteTrmBucket(0);
		}

		if(bDeleteTxFiles)
		{
			if(bDeleteTxFiles > 2)
				trmDeleteAllTxFiles();

			trmDeleteUploadedDcapFiles();
		}

		if(bDeleteUploadedFiles > 1)
		{
			dbgTrace(DBG_LVL_ERROR, "TrmReq: trmDeleteUploadedTxFiles " );
			trmDeleteUploadedTxFiles();
		}
		if(bPrepareMidnight)
		{
			trmResetForMidnight();
		}

		if(bMidnight)
		{
			trmSendMsgToBackground (MIDNIGHT_ACTIVITY, 0, TRUE, 0);
		}

	    if(uploadCount > 1)
		{
			//Delay
			NU_Sleep(300);
			trmReleaseMemory();

			//strncpy(trmFileName, "1H00-9300029-1H00-9300029-20170804-050-01", TRM_MAX_FILE_NAME_SIZE);
			//trncpy(trmFileName, "1H00-1150986-1H00-1150986-20170804-050-01", TRM_MAX_FILE_NAME_SIZE);


			//trmSetUploadState(TRM_STATE_UPLOAD_PENDING);
			dbgTrace(DBG_LVL_INFO, "TrmReq: Setting state: TRM_STATE_UPLOAD_PENDING ");
	    	//msgStat = fnGetUpdateNow(DCAP_UPLOAD_SERVICE, 0, 0);
		    // check if connected to Internet first
		    if (!connectedToInternet())
		    {
		    	return generateResponse(&entry, rspMsg, "TRM", api_status_to_string_tbl[API_STATUS_NO_INTERNET], root);
		    }

			trmSetUploadState(TRM_STATE_UPLOAD_PENDING);

			dbgTrace(DBG_LVL_ERROR,"TrmReq- trmSstate:%d Package:%d", trmGetUploadState(), CMOS2TrmDcapFileName);
			trmInitiatePackageUpload();

//	        if(trmGetUploadState() == TRM_STATE_UPLOAD_PENDING)
//	        {
//	        	dbgTrace(DBG_LVL_INFO, "TrmReq: fnGetUpdateNow ");
//	        	msgStat = fnGetUpdateNow(DCAP_UPLOAD_SERVICE, 0, 0);
//	    	}
//		    trmSendMsgToBackground (UPLOAD_TRX_PKG_FILE, 0, 0, 0);

//	        if(trmGetUploadState() == TRM_STATE_UPLOAD_PENDING)
//	        {
//	        	dbgTrace(DBG_LVL_INFO, "TrmReq: fnGetUpdateNow ");
//	        	msgStat = fnGetUpdateNow(DCAP_UPLOAD_SERVICE, 0, 0);
//	    	}
		}

	    //bGetTrminfo
	    if(bGetTrminfo)
	    {

	    	if(bGetTrminfo == 1)
	    	{
	    		cJSON_AddNumberToObject(rspMsg, "MailPieceID", CMOSTrmMailPieceID);
	    		cJSON_AddNumberToObject(rspMsg, "ActiveBucket", CMOSTrmActiveBucket);
	    		cJSON_AddNumberToObject(rspMsg, "TrmState", CMOSTRMState);
	    		cJSON_AddNumberToObject(rspMsg, "UploadState", CMOSTrmUploadState);
	    		cJSON_AddNumberToObject(rspMsg, "UploadFileCount", CMOSTrmDcapFileSeqCount);

	    	}
	    }

	}

//	if(uploadTimeSec)
//	{
//		DATETIME stGMTDateTime;
//		int i;
//
//		dbgTrace(DBG_LVL_ERROR, "TrmReq - UploadTime min: %d ", uploadTimeSec);
//		if(pParameters != NULL)
//		{
//			///////////////////////////////////////////////////////
//			//TODO:Remove this once period mgmt is working
//			dbgTrace(DBG_LVL_INFO, "TRMReq:");
//			for(  i = 0; i < MAX_NEXT_UPLOAD_DATES; i++)
//			{
//				dbgTrace(DBG_LVL_INFO, "PeriodMgmt: %d: %02d%02d-%02d-%02d %02d:%02d:%02d",
//						i+1,
//						pParameters->Period.periodList[i].bCentury,
//						pParameters->Period.periodList[i].bYear,
//						pParameters->Period.periodList[i].bMonth,
//						pParameters->Period.periodList[i].bDay,
//						pParameters->Period.periodList[i].bHour,
//						pParameters->Period.periodList[i].bMinutes,
//						pParameters->Period.periodList[i].bSeconds);
//			}
//			pParameters->Period.ucPeriodListIndex = 0;
//			dbgTrace(DBG_LVL_ERROR, "TrmReq - ucPeriodListIndex: %d ", pParameters->Period.ucPeriodListIndex);
//			memcpy(&pParameters->GraceDaysRef, &stGMTDateTime, sizeof(stGMTDateTime));
//			pParameters->Period.ucType = PT_PERIOD_LIST;
//		}
//
//
//
//		if(bTmrEnable == 0)
//			OSStopTimer(TRM_UPLOAD_TIMER);
//		else
//			OSStartTimer(TRM_UPLOAD_TIMER);
//
//		if(uploadTimeSec > 5)
//			OSChangeTimerDuration (TRM_UPLOAD_TIMER, (uploadTimeSec * 1000 ));
//
//	}

	//this is to control random time btwn midnight and 3 hrs after midnight
	if(UploadWaitCount)
	{
		uploadWaitCount = UploadWaitCount;
	}

	if(UploadWaitSec)
	{
		//this is to test Period limits
		graceSeconds = UploadWaitSec;
	}

	//Set period and upload period
	if(passed)
	{
	    if ((uploadPeriod < 0) || (uploadPeriod > 3))
	    	uploadPeriod = 0;

	    memcpy(&(pParameters->Period.periodList[uploadPeriod]), &dateTimePeriod, sizeof(dateTimePeriod));

	    dbgTrace(DBG_LVL_INFO, "UploadPeriod: %d: %02d%02d-%02d-%02d %02d:%02d:%02d",
	    						uploadPeriod,
	    						pParameters->Period.periodList[uploadPeriod].bCentury,
	    						pParameters->Period.periodList[uploadPeriod].bYear,
	    						pParameters->Period.periodList[uploadPeriod].bMonth,
	    						pParameters->Period.periodList[uploadPeriod].bDay,
	    						pParameters->Period.periodList[uploadPeriod].bHour,
	    						pParameters->Period.periodList[uploadPeriod].bMinutes,
	    						pParameters->Period.periodList[uploadPeriod].bSeconds);

	    pParameters->Period.ucPeriodListIndex = uploadPeriod;
	    dbgTrace(DBG_LVL_ERROR, "TrmReq - ucPeriodListIndex: %d ", pParameters->Period.ucPeriodListIndex);
	}

	if(ClearPrevUploadDate)
	{
		memset(&CMOSTrmPrevUploadDate, 0, sizeof(CMOSTrmPrevUploadDate));
	}

	if(ClearDcapGUID)
	{
		memset(&CMOSDcapGuid[0], 0, GUID_SIZE);
	}

	//clean if necessary
	if(bClean > 0 && gp_tr_xml != NULL)
	{
		NU_Deallocate_Memory(gp_tr_xml);

		trmClearSemaphore();
	}

	if (msgStat == SUCCESSFUL)
	{
		generateResponse(&entry, rspMsg, "TRM", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
	}
	else
	{
		generateResponse(&entry, rspMsg, "TRM", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
	}

}

#else
void on_TRMReq(UINT32 handle, cJSON *root)
{
	cJSON *rspMsg = cJSON_CreateObject();

	WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

	generateResponse(&entry, rspMsg, "TRM", api_status_to_string_tbl[API_STATUS_NOT_ACCEPTED], root);
}

#endif //TEST_TRM


void send_TrmRsp(api_status_t status, double bucket, double recordidx, double mpid, double pc, char *fileName)
{
    cJSON *rsp = cJSON_CreateObject();
    cJSON_AddStringToObject(rsp, "MsgID", "TRMReqRsp");
	cJSON_AddStringToObject(rsp, "Status", api_status_to_string_tbl[status]);
    if (status == API_STATUS_SUCCESS)
    {
		cJSON_AddNumberToObject(rsp, "Bucket", bucket);
		cJSON_AddNumberToObject(rsp, "RecordIdx", recordidx);
		cJSON_AddNumberToObject(rsp, "MPID", mpid);
		cJSON_AddNumberToObject(rsp, "PC", pc);
		cJSON_AddStringToObject(rsp, "FileName", fileName);
    }

    Send_JSON(rsp,BROADCAST_MASK);
}

/////////////////////////////////////////////////////////////////////////////

static required_fields_tbl_t required_fields_tbl_SetDcapRequiredFieldsReq =
    {
    	"TrmSetRequiredDcapFieldsReq", {
    			            NULL }
    };

void on_SetDcapRequiredFieldsReq(UINT32 handle, cJSON *root)
{
	BOOL msgStat = TRUE;

	cJSON *rspMsg = cJSON_CreateObject();
	cJSON *invalid = cJSON_CreateArray();
	WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    if (!RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_SetDcapRequiredFieldsReq))
	{
		//Add missing required item to invalid list
		cJSON_AddItemToArray(invalid, CreateStringItem((char *)required_fields_tbl_SetDcapRequiredFieldsReq.fieldnames[0], ""));
		msgStat = FALSE;
	}
    else
    {
		if (smgr_set_values(root) != NU_SUCCESS)
		{
			msgStat = FALSE;
			cJSON_AddItemToArray(invalid, settingsMgrErrorItem);
		}
    }

	if(msgStat == TRUE)
	{
		cJSON_Delete(invalid);
		generateResponse(&entry, rspMsg, "SetDcapRequiredFields", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
	}
	else
	{
		cJSON_AddItemToObject(rspMsg, "Invalid", invalid);
		generateResponse(&entry, rspMsg, "SetDcapRequiredFields", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
	}

}

/////////////////////////////////////////////////////////////////////////////////

void on_SetDcapParametersReq(UINT32 handle, cJSON *root)
{
	//STATUS status = SUCCESS;
	BOOL msgStat = TRUE;

	cJSON *rspMsg = cJSON_CreateObject();
	cJSON *invalid = cJSON_CreateArray();

	WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

	static char *valid_fieldnames [] =
	                {"DcapDevID"};

	static required_fields_tbl_t required_fields_tbl =
	    {
	    	"SetDcapParametersReq", {
							NULL }
	};

	//p_trm_mem = get_trm_mem();

	if (!RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl))
	{
		//Add missing required item to invalid list
		cJSON_AddItemToArray(invalid, CreateStringItem(valid_fieldnames[0], ""));
		msgStat = FALSE;
	}

	if(msgStat == TRUE)
	{
		// validate fields - "InitTRM",
		if (cJSON_HasObjectItem(root, valid_fieldnames[0]))
		{
			ushort dcapDevID = cJSON_GetObjectItem(root, valid_fieldnames[0])->valueint;
			trmSetDcapDeviceID(dcapDevID);
		}

	}

	if (msgStat == SUCCESSFUL)
	{
		generateResponse(&entry, rspMsg, "SetDcapParameters", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
	}
	else
	{
		generateResponse(&entry, rspMsg, "SetDcapParameters", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
	}

}



void on_UploadTrxPkgReq(UINT32 handle, cJSON *root)
{
	api_status_t  tranStat;
	STATUS status = SUCCESS;
	char *pTemp = "";
	short           systemErrorLog = 0;
	int i = 0;
	char 			sState[50];
	int 			uploadPeriod = 0;
	int 			fileCount = 0;
	static DATETIME	dtPrev = {0};
	DATETIME		dtCur;
	long 			lDateDiff;
	bool 			bValidDate;
	char 			DummyFileName[64] = {0};


	cJSON *rsp = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rsp}}};


    if (cJSON_HasObjectItem(root, "SeqNr"))
        cJSON_AddNumberToObject(rsp, "SeqNr", cJSON_GetObjectItem(root, "SeqNr")->valueint);

    //optional flag to include the system error log
    if (cJSON_HasObjectItem(root, "SystemErrorLog"))
    	systemErrorLog = (strcmp("true", cJSON_GetObjectItem(root, "SystemErrorLog")->valuestring) == 0) ? 1 : 0;

    GetUICStates(sState);
    cJSON_AddStringToObject(rsp, "BaseState", sState);

    if (cJSON_HasObjectItem(root, "UploadPeriod") )
    	uploadPeriod = cJSON_GetObjectItem(root, "UploadPeriod")->valueint;

    //Do not accept if  running,
    if (strcmp(sState,"Running") == 0)
    {
    	dbgTrace(DBG_LVL_INFO, "UploadTrxPkgReq-not accepted; Transport Running");
		return generateResponse(&entry, rsp, "UploadTrxPkg", api_status_to_string_tbl[API_STATUS_TRANSPORT_RUNNING], root);
    }

    //Do not accept if  state is ready,
	if (strcmp(sState,"Ready") == 0)
	{
		dbgTrace(DBG_LVL_INFO, "on_UploadTrxPkgReq-not accepted; Ready");
		return generateResponse(&entry, rsp, "UploadTrxPkg", api_status_to_string_tbl[API_STATUS_NOT_ACCEPTED], root);
	}

    // get transaction ID, if any, and store it
    if (cJSON_HasObjectItem(root, "TranID") == 1)
    {
        // There is a TranID in the req_msg. Copy that
    	pTemp = cJSON_GetObjectItem(root, "TranID")->valuestring;
    }
    tranStat = setTransactionID(pTemp, "UploadTrxPkgReq");
    if (tranStat != API_STATUS_SUCCESS)
    {
    	return generateResponse(&entry, rsp, "UploadTrxPkg", api_status_to_string_tbl[tranStat], root);
    }

    //if running or stopping wait until it is completed
	while ( (sSystemStatus.fRunning == TRUE) || (sSystemStatus.fStopping == TRUE) )
	{
		  NU_Sleep(20);
		  i++;
		  if(i ==150) //approx 2 sec wait for completion
				 break;
	}

	uploadPeriod = trmGetNextPeriod();

    status = trmGetTrxFileCount(&fileCount);
    if( ((CMOSTrmUploadDueReqd & TRM_UPLOAD_AVAILABLE) != TRM_UPLOAD_AVAILABLE) &&
    	(fileCount == 0) && (CMOSTrmMailPiecesToUpload == 0) && (CMOSTrmMailPiecesPackaged == 0))
    {
    	dbgTrace(DBG_LVL_ERROR,"UploadTrxPkg Status:%d, fileCount:%d", status, fileCount);
    	generateResponse(&entry, rsp, "UploadTrxPkg", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);

    	OSWakeAfter(100);
    	return sendUploadProgress(UPLOAD_EVENT_TRX_NO_RECORDS_TO_UPLOAD, 0, 0);
    }
    else
    {

    	//Reject this message if upload already in progress and next msg is within 2 min
		GetSysDateTime(&dtCur, ADDOFFSETS, GREGORIAN);
		bValidDate = CalcDateTimeDifference( &dtPrev, &dtCur, &lDateDiff );
		if( (fUploadInProgress == TRUE) && ((lDateDiff <= 120) && bValidDate) )
		{
			dbgTrace(DBG_LVL_INFO, "UploadTrxPkgReq-Upload in progress trmState:%d, lDateDiff:%d, bValidDate:%d ", trmGetUploadState(), lDateDiff, bValidDate);

			return generateResponse(&entry, rsp, "UploadTrxPkg", api_status_to_string_tbl[API_TRANSACTION_IN_PROGRESS], root);
		}
		else
		{
			fUploadInProgress = TRUE;
			dbgTrace(DBG_LVL_INFO, "UploadTrxPkgReq-: trmState:%d, lDateDiff:%d, bValidDate:%d ", trmGetUploadState(), lDateDiff, bValidDate);
			memcpy(&dtPrev, &dtCur, sizeof(dtCur));
		}

    	//if state is waiting for confirmation just try to upload same package again.
		//
		if( (CMOS2TrmDcapFileToBeUploaded == TRUE) &&
			(strncmp(CMOS2TrmDcapFileName, DummyFileName, sizeof(DummyFileName)) != 0) )
		{
			trmSetUploadState(TRM_STATE_UPLOAD_PENDING);

			dbgTrace(DBG_LVL_ERROR,"UploadTrxPkg Same Package trmSstate:%d ", trmGetUploadState());
			trmInitiatePackageUpload();
		}
		else
		{
			dbgTrace(DBG_LVL_ERROR,"UploadTrxPkg New package trmSstate:%d ", trmGetUploadState());
			fnUploadTrxPkgFile (uploadPeriod, CMOSTrmActiveBucket, 0);
		}

    }

    (void) getTransactionID(NULL, 0, "UploadTrxPkgReq", true);

	return generateResponse(&entry, rsp, "UploadTrxPkg", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);

}



void sendUploadProgress(upoad_event_t upload_event, int fileInProgress, int totFiles)
{
	api_status_t  tranStat;
	bool endTransaction = false;
    char tmpString[TRAN_ID_BUF_SIZE];
    cJSON *rsp = cJSON_CreateObject();

    cJSON_AddStringToObject(rsp, "MsgID", "UploadStatus");
    cJSON_AddStringToObject(rsp, "Event", upload_events_to_string_tbl[upload_event] );
    if((fileInProgress != 0) && (totFiles != 0))
    {
    	snprintf(tmpString, sizeof(tmpString), "%d out of  %d ", fileInProgress, totFiles);
    	cJSON_AddStringToObject(rsp, "Progress: ", tmpString );
    }

    switch (upload_event)
    { // end transaction on success or failure
		case UPLOAD_EVENT_TRX_UPLOAD_SUCCESS:
		case UPLOAD_EVENT_TRX_UPLOAD_FAILED:
		case UPLOAD_EVENT_TRX_NO_CONNECTIVITY:
		case UPLOAD_EVENT_TRX_FILE_CREATION_FAILED:
		case UPLOAD_EVENT_TRX_DISTRIBUTOR_BUSY:
		case UPLOAD_EVENT_TRX_NO_RECORDS_TO_UPLOAD:
			endTransaction = true;
			break;
		default:
			break;
    }

    tranStat = getTransactionID(tmpString, sizeof(tmpString), "UploadStatus", endTransaction);
    if (tranStat != API_STATUS_SUCCESS)
    { // internal error so do not send anything to tablet
		dbgTrace(DBG_LVL_ERROR,"Error: Internal error %d getting TranID in %s", tranStat, "UploadStatus");
    	return;
    }
	// add transaction ID to response
    cJSON_AddStringToObject(rsp, "TranID", tmpString);


    Send_JSON(rsp,BROADCAST_MASK);
}


