/*************************************************************************
*   PROJECT:        Horizon CSD2-3
*	MODULE NAME:	B_defPrivKeyPwd.c
*
*	DESCRIPTION:	This file contains the obfuscated default private key password.
*
* ----------------------------------------------------------------------
*               Copyright (c) 2017 Pitney Bowes Inc.
*                    37 Executive drive
*                   Danbury, Connecticut  06810
* ----------------------------------------------------------------------
*
*************************************************************************/

const unsigned char defPrivKeyPwd1[] = {0x08, 0x11, 0x6D, 0x23, 0x1B, 0x21, 0xDB, 0x61};
const unsigned char defPrivKeyRand2[] = {0x25, 0x9B, 0xDE, 0xC0, 0x4A, 0x0D, 0xB0, 0x4F};
