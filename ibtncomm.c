/************************************************************************
 PROJECT:        Horizon CSD
 MODULE NAME:    ibtncomm.c
 
 DESCRIPTION:    
        Ibutton serial communication task, init routines, LISR and 
        HISR routines.  Contains the state machine functions that 
        convert a single application-layer message into the DS2480
        CMD messages, Data Rom Messages, and 

               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810

*************************************************************************/

#include <stdio.h>
#include <string.h>     // sprintf()
#include <stdlib.h>     // malloc()
#include "ossetup.h"
#include "pbos.h"
#include "bob.h"
#include "sac.h"
#include "ibuttoniolow.h"

#define IBUTTON_MFG_TEST  // define this to buld the stand-alone manufacturing 
                            // routine iButtonMfgTest()

void fnSystemLogEntry( UINT8 bType, char *pData, UINT32 lwLen );
//----------------------------------------------------------------------------
//      External variable declarations:

extern unsigned char ucDS2480Mode;      // holds the current mode (command or data) of Dallas DS2480 chip
extern UARTCONTROL NSDUart;		// Nucleus Serial Device

extern UINT8    iButtonDiagBuf[ SIZEOF_IBTNDIAGBUF ];
extern unsigned short iButtonDiagBufIndex;


//----------------------------------------------------------------
//  Local function prototypes: 

UINT8   ubBLReplyLen    = 56;

// Remove this to talk to Bob
//#define NO_BOB_TEST

#ifdef NO_BOB_TEST
static void iButtonAdHocTest( void); 
#endif

static void iButtonUartTxRxIrqLISR( int id );
static int iButtonDeviceInit ( IBUTTON_DEVICE *dev , 
                               UINT16 * BaseAddr,
                               unsigned char InComingId ,
                               unsigned char OutGoingId,
                               unsigned char EventId,
                               unsigned RxVectorNum,
                               unsigned TxVectorNum,
                               void (*Rxhandler)(int),
                               void (*Txhandler)(int),
                               void (*hisrHandler)(void),
                               char *hisrName,
                               BOOL fbActive,
                               UARTCONTROL *uartFuncs,
                               unsigned PwrModeDoneVectorNum,
                               void (*PwrDonehandler)(int)
                             );

// iButton LISR prototypes 
static void iButtonCompuDoneIrqLISR( int id );

// iButton HISR state handler prototypes 
static void iButtonIdleStateHandler( unsigned long *iButtonEvent);
static void DS2480InitPendingStateHandler( unsigned long *iButtonEvent );
static void iButtonInitPendingStateHandler( unsigned long *iButtonEvent);
static void iButtonReadyStateHandler( unsigned long *iButtonEvent);
static void iButtonXmitPendingStateHandler( unsigned long *iButtonEvent);
static void iButtonRcvPendingStateHandler( unsigned long *iButtonEvent);

//----------------------------------------------------------------------------
//      Global variables created here...

UARTCHANNEL iButtonChannel = { (UINT16 *)PSDUART,
								PSDUARTRX,
								PSDUARTRX,
                                &iButtonUartTxRxIrqLISR,
                                &iButtonUartTxRxIrqLISR,
                                &NSDUart };


// Used for scratch pad for entries to system Log.
char   iBtnSysLogBuff[ 100 ];


//----------------------------------------------------------------
//  Local variables: 

// Almost all of the low-level stuff is here.
// !!! Not declared static for debugging purposes.
IBUTTON_DEVICE iButtonDevice;

// !!! Not declared static for debugging purposes.
BOBS_DRIVER_INTERFACE *iButtonBobIfPtr; // defines a pointer to a transfer structure

static UINT16   calcCRC;          // CRC value returned by generate command

// Static HISR state handler initialization
//  (Note that the state is represented in the upper 16 bits of a 32-bit word) 
static IB_STATE_S iButtonHISRState = 
{
    IB_IDLE,                        // current state
    // State handler functions for each state,  State,              State value 
    iButtonIdleStateHandler,               // IB_IDLE             0x000000
    DS2480InitPendingStateHandler,         // DS2480_INIT_PENDING 0x010000     
    iButtonInitPendingStateHandler,        // IB_INIT_PENDING     0x020000
    iButtonReadyStateHandler,              // IB_READY            0x030000
    iButtonXmitPendingStateHandler,        // IB_XMIT_PENDING     0x040000
    iButtonRcvPendingStateHandler          // IB_RCV_PENDING      0x050000
};

// DEBUG
BOBS_DRIVER_INTERFACE iButtonBobIf;		// Bob test structure

BOOL fLogIbutDrvMsgs = TRUE;    // log the ibutton-UIC traffic in sys log

//TODO: JAH Remove em: Driver debuggung
UINT8 DebugIBtnDriver[256];
UINT8 DebugIBtnDriverCtr = 0;
BOOL DebugIBtnDriverInit = FALSE;


//**********************************************************************
//      ----------------------------------------------------------------
//                  FUNCTIONS: 


//*****************************************************************************
// FUNCTION NAME:       fnIbuttonClrFlagOverrun
// DESCRIPTION:
//      Clears the overrun flag.
// ARGUMENTS:
//      pDev - Pointer to the device structure, from which all low-level data 
//              can be found.  (Not sure why we pass this all over the place, 
//              since there is only one, but all the functions that need it,
//              pass it.)
// RETURNS:
//      None 
// NOTES: 
//---------------------------------------------------------------------------*/
void fnIbuttonClrFlagOverrun( IBUTTON_DEVICE *pDev )
{
    // Clear the overrun flag.
    pDev->Link.iButtonFlags &= ~IBTNF_OVERRUN_EXT_READ;
    return;
}

//*****************************************************************************
// FUNCTION NAME:       fnIbuttonSetFlagOverrun
// DESCRIPTION:
//      Sets the overrun flag.
// ARGUMENTS:
//      pDev - Pointer to the device structure, from which all low-level data 
//              can be found.  (Not sure why we pass this all over the place, 
//              since there is only one, but all the functions that need it,
//              pass it.)
// RETURNS:
//      None 
// NOTES: 
//---------------------------------------------------------------------------*/
void fnIbuttonSetFlagOverrun( IBUTTON_DEVICE *pDev )
{
    // Set the overrun flag.
    pDev->Link.iButtonFlags |= IBTNF_OVERRUN_EXT_READ;
    return;
}


//*****************************************************************************
// FUNCTION NAME:       fnIbuttonGetFlagBootMsg
// DESCRIPTION:
//      Checks the flag indicating if this message is intended for the bootloader.
// ARGUMENTS:
//      pDev - Pointer to the device structure, from which all low-level data 
//              can be found.  (Not sure why we pass this all over the place, 
//              since there is only one, but all the functions that need it,
//              pass it.)
// RETURNS:
//      TRUE if the current message from the bobtask is a Boot Loader message,
//      else FALSE.
// NOTES: 
//---------------------------------------------------------------------------*/
BOOL fnIbuttonGetFlagBootMsg( const IBUTTON_DEVICE *pDev )
{
    BOOL    fRetVal;

    // Check if the flag is set.
    fRetVal = (pDev->Link.iButtonFlags & IBTNF_BOOT_LAYER_CMD) ? TRUE : FALSE;

    return( fRetVal );
}



//*****************************************************************************
// FUNCTION NAME:       fnIbuttonGetFlagWriteCBExt
// DESCRIPTION:
//      Checks the flag indicating if this message should use the WriteCBExtended
//      command instead of the WriteIPR command.
// ARGUMENTS:
//      pDev - Pointer to the device structure, from which all low-level data 
//              can be found.  (Not sure why we pass this all over the place, 
//              since there is only one, but all the functions that need it,
//              pass it.)
// RETURNS:
//      TRUE if the Extended Write CB flag is set.
//      else FALSE.
// NOTES: 
//---------------------------------------------------------------------------*/
BOOL fnIbuttonGetFlagWriteCBExt( const IBUTTON_DEVICE *pDev )
{
    BOOL    fRetVal;

    // Check if the flag is set.
    fRetVal = (pDev->Link.iButtonFlags & IBTNF_EXT_WRITE_CB) ? TRUE : FALSE;

    return( fRetVal );
}

//*****************************************************************************
// FUNCTION NAME:       fnIbuttonGetFlagReadCBExt
// DESCRIPTION:
//      Checks the flag indicating if this message should use the ReadCBExtended
//      command instead of the ReadIPR command.
// ARGUMENTS:
//      pDev - Pointer to the device structure, from which all low-level data 
//              can be found.  (Not sure why we pass this all over the place, 
//              since there is only one, but all the functions that need it,
//              pass it.)
// RETURNS:
//      TRUE if the Extended Read CB flag is set.
//      else FALSE.
// NOTES: 
//---------------------------------------------------------------------------*/
BOOL fnIbuttonGetFlagReadCBExt( const IBUTTON_DEVICE *pDev )
{
    BOOL    fRetVal;

    // Check if the flag is set.
    fRetVal = (pDev->Link.iButtonFlags & IBTNF_EXT_READ_CB) ? TRUE : FALSE;

    return( fRetVal );
}

//*****************************************************************************
// FUNCTION NAME:       iButtonGetHISRCompleteState
// DESCRIPTION:
//      Encapsulates the HISR state.
// ARGUMENTS:
//      None
// RETURNS:
//      32-bit HISR complete state.
// NOTES: 
//---------------------------------------------------------------------------*/
UINT32 fnIbuttonGetHISRCompleteState( void )
{
    UINT32  ulRetVal;

    ulRetVal = iButtonHISRState.currentState;
    return( ulRetVal );
}

//*****************************************************************************
// FUNCTION NAME:       iButtonGetHISRState
// DESCRIPTION:
//      Encapsulates the HISR state.
// ARGUMENTS:
//      None
// RETURNS:
//      16-bit HISR state.
// NOTES: 
//---------------------------------------------------------------------------*/
UINT16 fnIbuttonGetHISRState( void )
{
    UINT32  ulTemp;
    UINT16  uwRetVal;

    ulTemp = iButtonHISRState.currentState >> 16;
    uwRetVal = (UINT16)ulTemp;
    return( uwRetVal );
}

//*****************************************************************************
// FUNCTION NAME:       iButtonGetHISRSubState
// DESCRIPTION:
//      Encapsulates the HISR substate.
// ARGUMENTS:
//      None
// RETURNS:
//      16-bit HISR Sub-state.
// NOTES: 
//---------------------------------------------------------------------------*/
IB_SUB_STATES_E fnIbuttonGetHISRSubState( void )
{
    UINT32  ulTemp;
    IB_SUB_STATES_E  uwRetVal;

    ulTemp = iButtonHISRState.currentState & SUB_STATE_MASK;
    uwRetVal = (IB_SUB_STATES_E)ulTemp;
    return( uwRetVal );
}

// **********************************************************************
// FUNCTION NAME:  iButtonUartTxRxIrqLISR
// PURPOSE: 
//          
// AUTHOR: 
//
// INPUTS: 
// RETURNS: none
// **********************************************************************/
static void iButtonUartTxRxIrqLISR( int id )
{
    unsigned char bInterruptFlags;

    bInterruptFlags = (unsigned char)(*iButtonChannel.uartControl->fnReadInterruptRequest)( iButtonDevice.pbUartBase);

    if ( (( bInterruptFlags & DATA_RECEIVED_MASK ) == DATA_RECEIVED_MASK) || 
		 (*(iButtonDevice.pbUartBase + AM3X_UARTLSR_OFFSET) & AM3X_UARTLSR_FIFO_ERROR) )
    {
#ifdef IBUTTON_TEST_BUFFER
        iButtonBuf[iButtonBufIdx++] = 'r';
        iButtonBufIdx %= IBUTTON_TEST_BUFFER_SIZE;
#endif
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 50;
        iButton_Rx_ISR( &iButtonDevice );
    }

    if ( (*(iButtonDevice.pbUartBase + AM3X_UARTIER_OFFSET) &  AM3X_UARTIER_TXHR) && 
		 (( bInterruptFlags & THR_EMPTY_MASK ) == THR_EMPTY_MASK ))
    {
#ifdef IBUTTON_TEST_BUFFER
        iButtonBuf[iButtonBufIdx++] = 't';
        iButtonBufIdx %= IBUTTON_TEST_BUFFER_SIZE;
#endif
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 51;
        iButton_Tx_ISR( &iButtonDevice );
     }

}


// **********************************************************************
// FUNCTION NAME: iButtonCompuDoneIrqLISR
//
// PURPOSE: Calls ISR
//
// AUTHOR: Mike Ernandez
//
// INPUTS:
// RETURNS:
// **********************************************************************/
static void iButtonCompuDoneIrqLISR( int id )
{
    iButton_Compu_Done_ISR( &iButtonDevice );
}

// **********************************************************************
// FUNCTION NAME:  iButtonDMACompIrqLISR
// PURPOSE: 
//          
// AUTHOR: 
//
// INPUTS: 
// RETURNS: none
// **********************************************************************/
void iButtonDMACompIrqLISR( int id )
{
  iButton_DMA_ISR( &iButtonDevice );
}

// **********************************************************************/
// FUNCTION NAME: iButtonIrqHISR
//
// PURPOSE: Invokes the next state of the iButton driver state machine
//          and passes it the active event flags, if any.
//          States are defined as follows (note that the state is represented
//          in the upper 16 bits of a 32-bit word):
//              IB_IDLE             0x000000
//              DS2480_INIT_PENDING 0x010000
//              IB_INIT_PENDING     0x020000
//              IB_READY            0x030000
//              IB_XMIT_PENDING     0x040000
//              IB_RCV_PENDING      0x050000
//          
//          The following structure contains the current state and substate
//          along with an array of pointers to the state handlers:
//              
//          typedef struct _iButtonState {
//              unsigned long   currentState;   // State and sub-state
//              void (*fnStateHandler[ IB_NUM_STATES])( unsigned long *iButtonEvent);
//          } IB_STATE_S;
//  
//          and is initialized as follows:
//              
//          static IB_STATE_S iButtonHISRState = {
//              IB_IDLE,                    // current state
//              iButtonIdleStateHandler,
//              DS2480InitPendingStateHandler,              
//              iButtonInitPendingStateHandler,
//              iButtonReadyStateHandler,
//              iButtonXmitPendingStateHandler,
//              iButtonRcvPendingStateHandler
//          };
//
//
// AUTHOR: Mike Ernandez
//
// INPUTS:
// RETURNS:
// **********************************************************************/
void iButtonIrqHISR( void)
{

		DebugIBtnDriver[DebugIBtnDriverCtr++] = 53;
    // Invoke the state machine - the upper 2 bytes contain the state 
    iButtonHISRState.fnStateHandler[ iButtonHISRState.currentState>>16]( &iButtonDevice.lwActivateEvent);

}

/****************************************/
/* iButton HISR state service functions */
/****************************************/
// **********************************************************************
// FUNCTION NAME: iButtonIdleStateHandler
//
// PURPOSE: The idle state handler processes the INIT_IBUTTON_COMMAND message
//          which comes from the BOB task and is sent once at power-up.
//
// AUTHOR: Mike Ernandez/Tom Dometios
//
// INPUTS:
// RETURNS:
// **********************************************************************/
static void iButtonIdleStateHandler( unsigned long *iButtonEvent )
{
    unsigned long ival;
    IBUTTON_STATUS iButtonStatus; 
    IBUTTON_DEVICE *dev = &iButtonDevice;

	//TODO: JAH Remove em: Driver debuggung
	if(DebugIBtnDriverInit == FALSE)
	{
		memset(DebugIBtnDriver, 0, sizeof(DebugIBtnDriver));
		DebugIBtnDriverInit = TRUE;
	}

	DebugIBtnDriver[DebugIBtnDriverCtr++] = 1;
    // Idle only processes the EV_IB_INIT events - others are ignored
    if( *iButtonEvent & EV_IB_INIT) 
    {
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 2;
       // Clear the event
        ival = iButtonDisableInts( &iButtonDevice );
        *iButtonEvent &= ~EV_IB_INIT;
        iButtonRestoreInts( &iButtonDevice, ival);

        // Set the mode to start the initialization.  The 2480 has 2 modes: command and data mode
        // Command mode tells the 2480 that the data is not to passed through to the iButton, but 
        // is instead, a command for the 2480.  Data mode tells the 2480 to pass the data straight
        // through to the iButton.
        ucDS2480Mode = DS2480_CMD_MODE; 

        // Send a UART BREAK to perform a Master Reset Cycle on the DS2480.  This is equivalent
        // to a power cycle. Note that no response will be generated by the DS2480 or iButton for 
        // this command.  The LISR knows this and will set the EV_IB_RCV_DONE event so that we
        // can transition to the DS2480_INIT_PENDING state.
        iButtonStatus = iButtonGenerateCmd( DS2480_CMD, SEND_BREAK, &calcCRC, &iButtonDevice );
        if( iButtonStatus  == IBUTTON_OK ) 
        {
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 3;
           dev->Link.iButtonNoResponse = TRUE; // tell the TxRx ISR not to wait for a response
                    
            // Set state
            iButtonHISRState.currentState = DS2480_INIT_PENDING | DS2480_SEND_BREAK_PEND;
           
			// Write the next byte
            iButtonPutTxFIFO( NO_START, &iButtonDevice); 
        }
    }

			DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xDD;
			DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) (iButtonHISRState.currentState >> 16);
			DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) (iButtonHISRState.currentState);
}

// **********************************************************************
// FUNCTION NAME: DS2480InitPendingStateHandler
//
// PURPOSE: This handler waits for the DS2480 Master Reset Cycle (UART BREAK)
//          to complete, then handles the DS2480_SEND_BREAK_PEND and
//          DS2480_RESET_PEND sub-states.
//
// AUTHOR: Tom Dometios
//
// INPUTS:
// RETURNS:
// **********************************************************************/
static void DS2480InitPendingStateHandler( unsigned long *iButtonEvent )
{
    unsigned long ival;
    IBUTTON_STATUS iButtonStatus; 
    IBUTTON_DEVICE *dev = &iButtonDevice;
    
    // The only event of interest here is the receive done
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 4;
   if( *iButtonEvent & EV_IB_RCV_DONE ) 
    {
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 5;
       ival = iButtonDisableInts( &iButtonDevice );
        *iButtonEvent &= ~EV_IB_RCV_DONE;
        // Clear the events processed here
        iButtonRestoreInts( &iButtonDevice, ival);
    
    
        switch( iButtonHISRState.currentState & SUB_STATE_MASK) 
        {
            case DS2480_SEND_BREAK_PEND:
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 6;
               dev->Link.iButtonNoResponse = FALSE;   // clear the no response flag
        
                // WAIT FOR XMIT SHIFT REGISTER TO BE EMPTY BEFORE CHANGING THE BAUD RATE BACK!!!
                (*iButtonChannel.uartControl->fnWaitXmitDone)( dev->pbUartBase);


                (*iButtonChannel.uartControl->fnSetBaudRate)( dev->pbUartBase , BRRRESET);
                
                // Send the RESET command to the DS2480.  This first one is for timing purposes and is not
                // passed on to the iBUTTON.
                iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_RESET, &calcCRC, &iButtonDevice );
                if( iButtonStatus == IBUTTON_OK ) 
                {
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 7;
                   dev->Link.iButtonNoResponse = TRUE;   // tell the TxRx ISR not to wait for a response

                    // Set state
                    iButtonHISRState.currentState &= ~DS2480_SEND_BREAK_PEND;
                    iButtonHISRState.currentState |= DS2480_RESET_PEND;
                
                    // Write the reset
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 8;
              break;
           
            case DS2480_RESET_PEND:
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 9;
                dev->Link.iButtonNoResponse = FALSE;   // clear the no response flag
                
                // Put out the slow 1-wire reset to start, next validate the presence
                
                iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET, &calcCRC, &iButtonDevice );
                if( iButtonStatus  == IBUTTON_OK ) 
                {
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 10;

                    // Set state
                    iButtonHISRState.currentState = IB_INIT_PENDING | PRESENCE_VAL;

					// Write the reset
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 11;
               break;
            
            default:
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 12;
                break;
        }
    }
			DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xDD;
			DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) (iButtonHISRState.currentState >> 16);
			DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) (iButtonHISRState.currentState);
}

/* **********************************************************************
// FUNCTION NAME: iButtonInitPendingStateHandler
//
// PURPOSE: This handler processes the following sub-states to complete                                             
//          the DS2480 and iButton initialization:
//              PRESENCE_VAL 
//              DS2480_FAST_BAUD_PEND
//              DS2480_TEST_BAUD_PEND
//              OD_RESET_PENDING
//              OD_PRESENCE_VAL
//              READ_ROM_VAL
//
// AUTHOR: Mike Ernandez/Tom Dometios
//
// INPUTS:
// RETURNS:
// **********************************************************************/
static void iButtonInitPendingStateHandler( unsigned long *iButtonEvent)
{
    unsigned long ival, l;
    IBUTTON_STATUS  iButtonStatus;
    IBUTTON_DEVICE *dev = &iButtonDevice;

    // The only event of interest here is the receive done
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 13;
    if( *iButtonEvent & EV_IB_RCV_DONE ) 
    {
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 14;
       ival = iButtonDisableInts( &iButtonDevice );
        *iButtonEvent &= ~EV_IB_RCV_DONE;
        // Clear the events processed here
        iButtonRestoreInts( &iButtonDevice, ival);
    
        switch( iButtonHISRState.currentState & SUB_STATE_MASK) 
        {
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 15;
           // check to see if the 2480 detected an iButton after a slow reset
            case PRESENCE_VAL:  
                iButtonGetResponseData( &iButtonDevice );
                if(fLogIbutDrvMsgs)
                {
                    // add a log entry with the msg payload
                    fnSystemLogEntry(SYSLOG_IBUTTON, (char *)iButtonDevice.Link.iButtonRxResponseBuff, 1);
                }
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 16;
               if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                { 
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 17;
                   // Otherwise note it and move on
                    iButtonDevice.Status.lastSuccessfulOp = IBUTTON_SLOW_RESET;
                    // Move the sub-state, clear the current, set the new
                    iButtonHISRState.currentState &= ~PRESENCE_VAL;
                    iButtonHISRState.currentState |= DS2480_FAST_BAUD_PEND;
    
                    // At this point we are going to put the DS2480 into OVERDRIVE mode which causes it 
                    // to operate at 115Kbaud.  Sending this command causes the DS2480 to generate a response.
                    // However, there is a potential race condtion in receiving this command...here's why:
                    // The command is sent at 9600 baud.  The 2480 receives the command and sends a response 
                    // back at 115Kbaud.  However, the ASIC UART is in the process of being switched to 115K
                    // but the response my come back before this is complete.  For this reason, the receiver is
                    // being disabled so as to deliberately ignore the response. Once the UART speed is switched to
                    // 115K, then the receiver will be re-enabled.
                    (*iButtonChannel.uartControl->fnDisableReceive)( dev->pbUartBase);

                    (void)iButtonGenerateCmd( DS2480_CMD, DS2480_FAST_BAUD, &calcCRC, &iButtonDevice );
                    dev->Link.iButtonNoResponse = TRUE;   // tell the TxRx ISR not to wait for a response
                    
                    // Write the command
                    iButtonPutTxFIFO(NO_START, &iButtonDevice); 
                }
                else 
                {
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 18;
                   // Set note the error 
                    iButtonDevice.Status.lastError = IBUTTON_RESET_FAIL;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_RESET_FAIL;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
                break;

            // change the ASIC UART to 115 Kbaud to match the DS2480 overdrive mode
            case DS2480_FAST_BAUD_PEND:
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 19;
               // WAIT FOR XMIT SHIFT REGISTER TO BE EMPTY BEFORE CHANGING THE BAUD RATE to overdrive
                (*iButtonChannel.uartControl->fnWaitXmitDone)( dev->pbUartBase);

                for (l=0; l < 10000000; l++);           

                (*iButtonChannel.uartControl->fnSetBaudRate)( dev->pbUartBase , BRROVERDRIVE);
                
                iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_TEST_BAUD, &calcCRC, &iButtonDevice );
                if( iButtonStatus == IBUTTON_OK ) 
                {
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 20;
                   // Move the sub-state, clear the current, set the new
                    iButtonHISRState.currentState &= ~DS2480_FAST_BAUD_PEND;
                    iButtonHISRState.currentState |= DS2480_TEST_BAUD_PEND;
                    dev->Link.iButtonNoResponse = FALSE;   // clear the no response flag
    
                    // Re-enable the UART receiver...it is now operating at 115K and can receive commands from the
                    // DS2480 at overdrive speed.
					*(dev->pbUartBase + AM3X_UARTFCR_OFFSET) = (AM3X_UARTFCR_RXTRIG_1 | AM3X_UARTFCR_FIFOEN | AM3X_UARTFCR_RX_FIFO_CLEAR); //TODO: JAH temp change to try for 253D Fix
                    (*iButtonChannel.uartControl->fnEnableReceive)( dev->pbUartBase);
   
                    // Write the reset
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 21;
               break;
                
            // check the response to see if DS2480 is in overdrive (fast baud rate) mode.  If it is, then
            // issue the OVERDRIVE_SKIP_ROM command to the iButton.  This tells the iButton to switch to
            // overdrive mode without the host having to provide the 64-bit ROM code.  Since this is a 
            // single-drop bus, the ROM code is not needed to address the device.   
            case DS2480_TEST_BAUD_PEND:
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 22;
               iButtonGetResponseData( &iButtonDevice );
                if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_FAST_BAUD_OK ) 
                {
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 23;
                   iButtonStatus = iButtonGenerateCmd( IBUTTON_CMD, OVERDRIVE_SKIP_ROM, 
                                                        &calcCRC, &iButtonDevice );
                    if( iButtonStatus == IBUTTON_OK ) 
                    {
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 24;
                       // Move the sub-state, clear the current, set the new
                        iButtonHISRState.currentState &= ~DS2480_TEST_BAUD_PEND;
                        iButtonHISRState.currentState |= OD_RESET_PENDING;
                        // Write the reset
                        iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                    }
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 25;
               }    
                else 
                {
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 26;
                    // Set note the error 
                    iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_OD_RESET_FAIL;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
                break;
                
            // Now issue an overdrive reset to the iButton to see if it is alive in overdrive mode  
            case OD_RESET_PENDING:
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 27;
                iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET_OD, 
                                                    &calcCRC, &iButtonDevice );
                if( iButtonStatus == IBUTTON_OK ) 
                {
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 28;
                    // Move the sub-state, clear the current, set the new
                    iButtonHISRState.currentState &= ~OD_RESET_PENDING;
                    iButtonHISRState.currentState |= OD_PRESENCE_VAL;
                    // Write the reset
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 29;
                break;
            
            // check to see if iButton is alive and send the READ_ROM command to the iButton
            // to get the family code and serial number.
            case OD_PRESENCE_VAL:   
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 30;
               iButtonGetResponseData( &iButtonDevice );
                if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                 { 
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 31;
                    
                    // Otherwise note it and move on
                    iButtonDevice.Status.lastSuccessfulOp = IBUTTON_OD_RESET;
                    // Move the sub-state, clear the current, set the new
                    iButtonHISRState.currentState &= ~OD_PRESENCE_VAL;
                    iButtonHISRState.currentState |= READ_ROM_VAL;
                    // Send the READ_ROM cmd
                    (void)iButtonGenerateCmd( IBUTTON_CMD, READ_ROM, &calcCRC, &iButtonDevice );
                    // Write the command
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }
                else 
                {
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 32;
                    // Set note the error 
                    iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /*  BOB_*/IBUTTON_OD_RESET_FAIL;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
                break;

            // Get the family code and serial number of the iButton
            case READ_ROM_VAL:  
 	DebugIBtnDriver[DebugIBtnDriverCtr++] = 33;
               iButtonGetResponseData( &iButtonDevice );
                if(fLogIbutDrvMsgs)
                {
                    // add a log entry with the msg payload
                    fnSystemLogEntry(SYSLOG_IBUTTON, (char *)iButtonDevice.Link.iButtonRxResponseBuff, 8);
                }
                iButtonDevice.Status.familyCode = iButtonDevice.Link.iButtonRxResponseBuff[ 0 ];
                memcpy( iButtonDevice.Status.serial,&iButtonDevice.Link.iButtonRxResponseBuff[ 1 ], 6);
                // Tell the Bob Task
                iButtonBobIfPtr->replyLen[ 0] = 8;
                memcpy( iButtonBobIfPtr->pDataRecv[ 0], 
                        &iButtonDevice.Link.iButtonRxResponseBuff[ 0 ], 
                        8 );

                OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->okMask, OS_OR);
                // Move the state to ready
                iButtonHISRState.currentState = IB_READY;

                break;

            default:
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 34;
                break;
        }
    }
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 35;
			DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xDD;
			DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) (iButtonHISRState.currentState >> 16);
			DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) (iButtonHISRState.currentState);

}

// **********************************************************************
// FUNCTION NAME: iButtonReadyStateHandler
//
// PURPOSE: The ready state handler processes the TRANSMIT_COMMAND message
//          which comes from the BOB task.  Once the iButton initialization
//          has taken place, this message will be sent to begin all iButton
//          transactions. Note that every iButton command will begin with
//          a DS2480_OWRESET_OD command.  This tells the DS2480 to issue a
//          1-wire reset to the iButton in overdrive mode.  This ensures 
//          that the iButton's internal 1-wire interface is in a known reset
//          state before sending the actual command.  There is one caveat
//          here: in order to send the reset command, if the DS2480 is not
//          in command mode, it has to be switched to command mode in order
//          to issue it the DS2480_OWRESET_OD command. Note that the RESET MICRO
//          command will not suffice since it resets the iButton's micro 
//          but no it's 1-wire interface.
//
// AUTHOR: Mike Ernandez/Tom Dometios
//
// INPUTS:
// RETURNS:
// **********************************************************************/
static void iButtonReadyStateHandler( unsigned long *iButtonEvent)
{
    unsigned long ival;
    IBUTTON_STATUS  iButtonStatus;

		DebugIBtnDriver[DebugIBtnDriverCtr++] = 36;
    if( *iButtonEvent & EV_IB_XMIT_DATA ) 
    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 37;
        // Clear the events processed here
        ival = iButtonDisableInts( &iButtonDevice );
        *iButtonEvent &= ~EV_IB_XMIT_DATA;
        iButtonRestoreInts( &iButtonDevice, ival );

        iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET_OD, &calcCRC, &iButtonDevice );
        if( iButtonStatus == IBUTTON_OK ) 
        {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 38;
            // Move the sub-state, clear the current, set the new
            iButtonHISRState.currentState = IB_XMIT_PENDING | READY_RESET_PEND;
            // Write the reset
            iButtonPutTxFIFO( NO_START, &iButtonDevice ); 
        }
    }
			DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xDD;
			DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) (iButtonHISRState.currentState >> 16);
			DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) (iButtonHISRState.currentState);
}

// **********************************************************************
// FUNCTION NAME: iButtonXmitPendingStateHandler
//
// PURPOSE: This handler processes the following sub-states that send
//          a command to the iButton. The WRITE_START_VAL sub-state 
//          sends a start bit to the iButton.  This causes the iButton
//          to start it's compute cycle which processes the command/data
//          in the IPR register.
//
//          READY_RESET_PEND
//          READ_STATUS_PEND_RESET
//          READ_STATUS_PEND
//          READ_STATUS_VAL_RESET
//          READ_STATUS_VAL
//          WRITE_IO_VAL
//          WRITE_IPR_PEND_RESET
//          WRITE_IPR_PEND
//          WRITE_IPR_VAL_RESET
//          WRITE_IPR_VAL
//          WRITE_STATUS_VAL
//          WRITE_START_VAL_RESET
//          WRITE_START_VAL
//          START_BIT_PEND
//          START_BIT_DONE
//
// AUTHOR: Mike Ernandez/Tom Dometios
//
// INPUTS:
// RETURNS:
// **********************************************************************/
static void iButtonXmitPendingStateHandler( unsigned long *iButtonEvent)
{
    unsigned long ival;
    IBUTTON_STATUS  iButtonStatus;
    IBUTTON_DEVICE *dev = &iButtonDevice;
    UINT8   *pCurrSeg;
	P_GPIO_REGLAYOUT pGPIOReg = (P_GPIO_REGLAYOUT) dev->puPort;


    // The only event of interest here is the receive done
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 39;
    if( *iButtonEvent & EV_IB_RCV_DONE ) 
    {
        ival = iButtonDisableInts( &iButtonDevice );
        *iButtonEvent &= ~ EV_IB_RCV_DONE;
        iButtonRestoreInts( &iButtonDevice, ival);
    
        // Process the sub-state
        		DebugIBtnDriver[DebugIBtnDriverCtr++] = 40;
		switch( iButtonHISRState.currentState & SUB_STATE_MASK) 
        {
            case READY_RESET_PEND:    
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 41;
                // Check for iButton presence & alive after OD reset...
                iButtonGetResponseData( &iButtonDevice );
                if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                 { 
                    		DebugIBtnDriver[DebugIBtnDriverCtr++] = 42;
 					iButtonDevice.Status.lastSuccessfulOp = IBUTTON_OD_RESET;
                    
                    // Initialize variables
                    iButtonDevice.Trans.iButtonRxCurSegment = 0;
                    
                    iButtonHISRState.currentState &= ~READY_RESET_PEND;
                    iButtonHISRState.currentState |= READ_STATUS_PEND_RESET;

                    (void)iButtonGenerateCmd( IBUTTON_CMD, RESET_MICRO, &calcCRC, &iButtonDevice );
                    // Write the command
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }
                else 
                {
                    // Set note the error 
                    iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_OD_RESET_FAIL;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
                break;
                
            case READ_STATUS_PEND_RESET:    
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 43;
                iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET_OD,
                                                    &calcCRC, &iButtonDevice );
                if( iButtonStatus == IBUTTON_OK ) 
                {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 44;
					iButtonHISRState.currentState &= ~READ_STATUS_PEND_RESET;
                    iButtonHISRState.currentState |= READ_STATUS_PEND; 
                        
                    // Write the command
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }
                break;
                    
            case READ_STATUS_PEND:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 45;
                // Check for iButton presence & alive after OD reset...
                iButtonGetResponseData( &iButtonDevice );
                if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                { 
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 46;
                   iButtonDevice.Status.lastSuccessfulOp = IBUTTON_OD_RESET;
                    
                    iButtonHISRState.currentState &= ~READ_STATUS_PEND;
                    iButtonHISRState.currentState |= READ_STATUS_VAL_RESET;

                    // Create and send a Get Status message
                    (void)iButtonGenerateCmd( IBUTTON_CMD, READ_STATUS, &calcCRC, &iButtonDevice );
                    // Write the command
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                    // Advance the state
                }
                else 
                {
                    // Set note the error 
                    iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_OD_RESET_FAIL;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
               break;
                
            case READ_STATUS_VAL_RESET: 
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 47;
               iButtonGetResponseData( &iButtonDevice );
                iButtonStatus = iButtonCheckCRC( READ_STATUS, calcCRC, &iButtonDevice);
                if( iButtonStatus == IBUTTON_OK ) 
                {
                    // ROM-message: SkipROM-overdrive (no ROM ID required, there's 
                    //  only 1 iBtn device.)
                    iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET_OD, 
                                                        &calcCRC, &iButtonDevice ); 
                    if( iButtonStatus == IBUTTON_OK ) 
                    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 48;
                        // Clear the current  state
                        iButtonHISRState.currentState &= ~READ_STATUS_VAL_RESET;
                        // Check if we are supposed to use the Write CB Extended command. 
                        //  (Boot Loader messages do.)
                        if( fnIbuttonGetFlagWriteCBExt( &iButtonDevice ) == TRUE )
                        {
                            // Write CB Extended command skips the write I/O step. 
                            iButtonHISRState.currentState |= WRITE_CB_EXTENDED_PEND;
                        }
                        else if( fnIbuttonGetFlagBootMsg( &iButtonDevice ) == TRUE )
                        {
                            // If its a Boot Loader message, but not using extended write,
                            //  just skip the I/O and goto WriteIPR 
                            iButtonHISRState.currentState |= WRITE_IPR_PEND;
                        }
                        else
                        {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 49;
                            iButtonHISRState.currentState |= READ_STATUS_VAL;
                        }

                        // Seed the transmit
                        iButtonPutTxFIFO( NO_START, &iButtonDevice);
                    }
                }
                else 
                {
                    // Set the error 
                    iButtonDevice.Status.lastError = READ_STATUS_ERROR;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/READ_STATUS_ERROR;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
                break;

            case READ_STATUS_VAL:
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 60;
               // Check for iButton presence & alive after OD reset...
                iButtonGetResponseData( &iButtonDevice );
                if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                { 
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 61;
                    iButtonDevice.Status.lastSuccessfulOp = IBUTTON_OD_RESET;
                    
                    iButtonHISRState.currentState &= ~READ_STATUS_VAL;
                    iButtonHISRState.currentState |= WRITE_IO_VAL;

                    // Create and send a Get Status message
                    (void)iButtonGenerateCmd( IBUTTON_CMD, WRITE_IO_BUFFER, &calcCRC, &iButtonDevice );
                    // Write the command
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                    // Advance the state
                }
                else 
                {
                    // Set note the error 
                    iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_OD_RESET_FAIL;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
               break;

            case WRITE_IO_VAL:  
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 62;
                iButtonGetResponseData( &iButtonDevice );
                if( iButtonCheckCRC( WRITE_IO_BUFFER, calcCRC, &iButtonDevice) == IBUTTON_OK) 
                {
  		DebugIBtnDriver[DebugIBtnDriverCtr++] = 63;
                  // If ok advance the state
                    iButtonHISRState.currentState &= ~WRITE_IO_VAL;
                    iButtonHISRState.currentState |= WRITE_IPR_PEND_RESET;
                    // Send the WRITE_IO_BUFFER Release sequence
                    iButtonGenerateReleaseSeq( WRITE_IO_BUFFER_REL, &iButtonDevice);
                    // Seed the transmit
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }
                else 
                {
                    // Set the error 
                    iButtonDevice.Status.lastError = WRITE_IO_ERROR;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/WRITE_IO_ERROR;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
                break;

            case WRITE_IPR_PEND_RESET:
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 64;
               iButtonGetResponseData( &iButtonDevice );
                // Do not log release sequence response from IButton
                //if(fLogIbutDrvMsgs)
                //{
                     // add a log entry with the msg payload
                     //fnSystemLogEntry(SYSLOG_IBUTTON, (char *)iButtonDevice.Link.iButtonRxResponseBuff, 1);
                //}
                if ( iButtonDevice.Link.iButtonRxResponseBuff[0] != 0 )
                {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 65;
                    // Set the error 
                    iButtonDevice.Status.lastError = WRITE_IO_ERROR;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/WRITE_IO_ERROR;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
                else if( ( iButtonStatus = iButtonGenerateCmd( DS2480_CMD, 
                                                          DS2480_OWRESET_OD, &calcCRC, &iButtonDevice ) ) == 
                                                         IBUTTON_OK) 
                {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 66;
                    iButtonHISRState.currentState &= ~WRITE_IPR_PEND_RESET;
                    iButtonHISRState.currentState |= WRITE_IPR_PEND;
                    // Seed the transmit
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }
                break;

            case WRITE_IPR_PEND:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 67;
                iButtonGetResponseData( &iButtonDevice );
                if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                { 
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 68;
                   iButtonHISRState.currentState &= ~WRITE_IPR_PEND;
                    iButtonHISRState.currentState |= WRITE_IPR_VAL_RESET;
                    (void)iButtonGenerateCmd( IBUTTON_CMD, WRITE_IPR_REGISTER, &calcCRC, &iButtonDevice );
                    // Seed the transmit
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }
                else 
                {
                    // Set note the error 
                    iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_OD_RESET_FAIL;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
                break;

            case WRITE_CB_EXTENDED_PEND:
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 69;
               iButtonGetResponseData( &iButtonDevice );
                if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                { 
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 70;
                   iButtonHISRState.currentState &= ~WRITE_CB_EXTENDED_PEND;
                    iButtonHISRState.currentState |= WRITE_CB_EXTENDED_VAL;
                    (void)iButtonGenerateCmd( IBUTTON_CMD, EXTENDED_WRITE_CB, &calcCRC, &iButtonDevice );
                    // Seed the transmit
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }
                else 
                {
                    // Set note the error 
                    iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_OD_RESET_FAIL;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
                break;

            case WRITE_CB_EXTENDED_VAL:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 71;
                pCurrSeg = &(iButtonDevice.Trans.iButtonTxCurSegment);
                (*pCurrSeg)++; 
                if( *pCurrSeg < iButtonDevice.Trans.iButtonTxSegCount )
                {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 72;
                   // State stays the same, until all segments have been sent.
                    // Send the next block, updating the calculated CRC.
                    (void)fniButtonContinueExtendedWrite( &iButtonDevice, &calcCRC );
                    // Seed the transmit
                    iButtonPutTxFIFO( NO_START, &iButtonDevice);
                }
                else // Sent the last block last time, should have received the CRC now...
                {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 73;
                    iButtonGetResponseData( &iButtonDevice );
                    if( iButtonCheckCRC( EXTENDED_WRITE_CB, calcCRC, &iButtonDevice ) == IBUTTON_OK ) 
                    {
                       iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET_OD, 
                                                           &calcCRC, &iButtonDevice );
                        if( iButtonStatus == IBUTTON_OK ) 
                        {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 74;
                            // If ok advance the state
                            iButtonHISRState.currentState &= ~WRITE_CB_EXTENDED_VAL;
                            iButtonHISRState.currentState |= WRITE_IPR_VAL;
                            // Seed the transmit
                            iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                        }
                    }
                    else  // CRC did not check out.
                    {
                        // Set the error           
                        iButtonDevice.Status.lastError = WRITE_IPR_ERROR;
                        // Tell the Bob Task
                        iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/WRITE_IPR_ERROR;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set the state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                }
                break;

            case WRITE_IPR_VAL_RESET:
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 75;
               iButtonGetResponseData( &iButtonDevice );
                if( iButtonCheckCRC( WRITE_IPR_REGISTER, calcCRC, &iButtonDevice) == IBUTTON_OK) 
                {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 76;
                    if( ( iButtonStatus = iButtonGenerateCmd( DS2480_CMD, 
                                                              DS2480_OWRESET_OD, &calcCRC, &iButtonDevice ) ) == 
                                                         IBUTTON_OK) 
                    {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 77;
                       // If ok advance the state
                        iButtonHISRState.currentState &= ~WRITE_IPR_VAL_RESET;
                        iButtonHISRState.currentState |= WRITE_IPR_VAL;
                        // Seed the transmit
                        iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                    }
                }
                else 
                {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 78;
                   // Set the error           
                    iButtonDevice.Status.lastError = WRITE_IPR_ERROR;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/WRITE_IPR_ERROR;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
                break;

            case WRITE_IPR_VAL:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 79;
                iButtonGetResponseData( &iButtonDevice );
                if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                { 
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 80;
                   (void)iButtonGenerateCmd( IBUTTON_CMD, WRITE_STATUS, &calcCRC, &iButtonDevice );
                    iButtonHISRState.currentState &= ~WRITE_IPR_VAL;
                    iButtonHISRState.currentState |= WRITE_STATUS_VAL;
                    // Seed the transmit
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }
                else 
                {
                    // Set note the error 
                    iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_OD_RESET_FAIL;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
                break;
                
            case WRITE_STATUS_VAL:
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 81;
				iButtonGetResponseData( &iButtonDevice );
                if( iButtonCheckCRC( WRITE_STATUS, calcCRC, &iButtonDevice) == IBUTTON_OK) 
                {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 82;
                    // If ok advance the state
                    iButtonHISRState.currentState &= ~WRITE_STATUS_VAL;
                    iButtonHISRState.currentState |= WRITE_START_VAL_RESET;
                    // ...and generate the release sequence
                    iButtonGenerateReleaseSeq( WRITE_STATUS_REL, &iButtonDevice);
                    // Seed the transmit
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }
                else 
                {
                    // Set the error 
                    iButtonDevice.Status.lastError = WRITE_STATUS_ERROR;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/WRITE_STATUS_ERROR;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
                break;

            case WRITE_START_VAL_RESET:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 83;
                iButtonGetResponseData( &iButtonDevice );
                // Do not log release sequence response from IButton
                //if(fLogIbutDrvMsgs)
                //{
                    // add a log entry with the msg payload
                    //fnSystemLogEntry(SYSLOG_IBUTTON, (char *)iButtonDevice.Link.iButtonRxResponseBuff, 1);
                //}
                if ( iButtonDevice.Link.iButtonRxResponseBuff[0] != 0 )
                {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 84;
                    // Set the error 
                    iButtonDevice.Status.lastError = WRITE_IO_ERROR;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/WRITE_IO_ERROR;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
                else if( ( iButtonStatus = iButtonGenerateCmd( DS2480_CMD, 
                                                          DS2480_OWRESET_OD, &calcCRC, &iButtonDevice ) ) == 
                                                         IBUTTON_OK) 
                {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 85;
                    iButtonHISRState.currentState &= ~WRITE_START_VAL_RESET;
                    iButtonHISRState.currentState |= WRITE_START_VAL;
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }       
                break;
                    
            case WRITE_START_VAL:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 86;
                iButtonGetResponseData( &iButtonDevice );
                if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                { 
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 87;
                    iButtonHISRState.currentState &= ~WRITE_START_VAL;
                    iButtonHISRState.currentState |= START_BIT_PEND;
                    // ..and generate the START PROGRAM command with the release sequence
                    (void)iButtonGenerateCmd( IBUTTON_CMD, START_PROGRAM, &calcCRC, &iButtonDevice );
                    // Seed the transmit
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                }
                else 
                {
                    // Set note the error 
                    iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                    // Tell the Bob Task
                    iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_OD_RESET_FAIL;
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                    // Set the state to Idle
                    iButtonHISRState.currentState = IB_IDLE;
                }
                break;

            case START_BIT_PEND:
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 88;
               // Do the release for the start
                iButtonHISRState.currentState &= ~START_BIT_PEND;
                iButtonHISRState.currentState |= START_BIT_DONE;
                
                (void)iButtonGenerateCmd( DS2480_CMD, DS2480_SINGLE_BIT_WRITE_0, &calcCRC, &iButtonDevice );
                
                iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                break;
                
            case START_BIT_DONE:
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 89;
               /// !!!!!!! Test
                iButtonGetResponseData( &iButtonDevice );
                /// !!!!!!! Test

                iButtonHISRState.currentState = IB_RCV_PENDING | RCV_DONE_PEND;
                pGPIOReg->GPIO_SETDATAOUT = IBTN_INT_ENB; // enable the current sense flip-flop by releasing the 'clr'  
                break;
                
            default:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 90;
                break;
        }
    } // End of event = EV_IB_RCV_DONE
			DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xDD;
			DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) (iButtonHISRState.currentState >> 16);
			DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) (iButtonHISRState.currentState);
}

// **********************************************************************
// FUNCTION NAME: iButtonRcvPendingState:Handler
//
// PURPOSE: This handler processes the following sub-states that retrieve
//          the response data from the iButton:  
//              RECEIVE_HANDLER_RESET_PEND
//              READ_STATUS_PEND
//              READ_IO_BUFFER_PEND
//              READ_IO_BUFFER_RESET_PEND
//              READ_IO_BUFFER_VAL
//              READ_IPR_PARMS_PEND_RESET
//              READ_IPR_PARMS_PEND
//              READ_IPR_DATA_PEND_RESET
//              READ_IPR_DATA_PEND
//              WRITE_STATUS_PEND
//              READ_CONTINUE_PROGRAM_RESET
//              READ_CONTINUE_PROGRAM
//              READ_CONTINUE_PROGRAM_PEND
//              READ_CONTINUE_PROGRAM_DONE
//              READ_CB_EXTENDED_PEND,      
//              READ_CB_EXT_RESET_PEND,     
//              READ_CB_EXT_BUFFER_LEN,     
//              READ_CB_EXTENDED_VAL,              
//
//          There are different several scenarios that can occur here:
//          1.  The entire command that was sent by the host was <= 128 bytes
//              meaning that no more data has to be sent by the host.
//          2.  The entire command is > 128 bytes.  Since the iButton can
//              only process 128 bytes at a time, more data will have to
//              be sent by the host after the compute cycle is over.  The
//              iButton knows how much remains to be sent from the header
//              info. that was sent in the WRITE_IO_VAL sub-state. In this
//              case when the host sends the READ_STATUS cmd, the iButton
//              will return a status of MSG_INCOMPLETE which will cause
//              the stat machine to return to the IB_XMIT_PENDING state to
//              send another block of data.  
//          3.  The iButton response <= 128 bytes.
//              Once all of the host's data has been sent, the host will read 
//              the iButton's I/O buffer to see how much data the iButton has 
//              to send back. The host will then read the iButton's IPR register
//              to get the data.  At this point, the host will re-assemble the
//              APDU and send the data back to the BOB task.
//          4.  The iButton response > 128 bytes.
//              Once all of the host's data has been sent, the host will read 
//              the iButton's I/O buffer to see how much data the iButton has 
//              to send back. The host will then read the iButton's IPR register
//              to get the data.  At this point, the host will check the status
//              that was returned from the iButton and find that the status
//              indicates to CONTINUE.  The host will ultimately send a 
//              CONTINUE command to the iButton, kicking off another compute
//              cycle during which the iButton will put more data in the IPR
//              register to be read by the host.  At the end of the compute
//              cycle, the state machine will return to the IB_RCV_PENDING
//              state to repeat this process until the iButton has sent all
//              of the data.  
  
// AUTHOR: Mike Ernandez/Tom Dometios
//
// INPUTS:
// RETURNS:
// **********************************************************************/
static void iButtonRcvPendingStateHandler( unsigned long *iButtonEvent)
{
    IBUTTON_STATUS  iButtonStatus;
    IBUTTON_DEVICE *dev = &iButtonDevice;
    UINT32      ival;
    UINT16      uwRxLen;
	P_GPIO_REGLAYOUT pGPIOReg = (P_GPIO_REGLAYOUT) dev->puPort;
	UINT32 i;

    // Process the sub-state
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 91;
    if( *iButtonEvent & (EV_IB_COMP_DONE)) 
    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 92;
		dev->Link.iButtonNoResponse = FALSE;   // clear the no response flag
    
        ival = iButtonDisableInts( &iButtonDevice );
        *iButtonEvent &= ~EV_IB_COMP_DONE;
        iButtonRestoreInts( &iButtonDevice, ival);
    
        iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET_OD, 
                                            &calcCRC, &iButtonDevice);
        if( iButtonStatus == IBUTTON_OK )
        {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 93;
            iButtonHISRState.currentState &= ~RCV_DONE_PEND;
            iButtonHISRState.currentState |= RECEIVE_HANDLER_RESET_PEND;
            iButtonPutTxFIFO( NO_START, &iButtonDevice ); 
        }
    }
    else 
    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 94;
        // The only event of interest here is the receive done
        if( *iButtonEvent & EV_IB_RCV_DONE ) 
        {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 95;
           ival = iButtonDisableInts( &iButtonDevice );
            *iButtonEvent &= ~EV_IB_RCV_DONE;
            iButtonRestoreInts( &iButtonDevice, ival );
        
            switch( iButtonHISRState.currentState & SUB_STATE_MASK) 
            {
                case RECEIVE_HANDLER_RESET_PEND:
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 96;
                   iButtonGetResponseData( &iButtonDevice );
                    if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                    { 
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 97;
                       iButtonHISRState.currentState &= ~RECEIVE_HANDLER_RESET_PEND;
                        iButtonHISRState.currentState |= READ_STATUS_PEND;
                        (void)iButtonGenerateCmd( IBUTTON_CMD, READ_STATUS, &calcCRC, &iButtonDevice );
                        iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                    }
                    else 
                    {
                        // Set note the error 
                        iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                        // Tell the Bob Task
                        iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_OD_RESET_FAIL;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set the state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                    break;
                    
                case READ_STATUS_PEND:
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 98;
                   // Check the CRC16 return
                    iButtonGetResponseData( &iButtonDevice );
                    iButtonStatus = iButtonCheckCRC( READ_STATUS, calcCRC, &iButtonDevice); 
                    if( iButtonStatus == IBUTTON_OK )
                    {
   		DebugIBtnDriver[DebugIBtnDriverCtr++] = 99;
						if ( (iButtonDevice.Status.iButtonContinue & IBTN_OWMS_STATUS_MASK) == STANDBY)
                        {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 100;
                            iButtonDevice.Status.lastError = READ_STATUS_ERROR;
                            // Tell the Bob Task
                            iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/READ_STATUS_ERROR;
                            OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                            iButtonHISRState.currentState = IB_IDLE;
                        }
                        else
                        {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 101;
                           // Save the status to determine whether there is more data
                            iButtonDevice.Status.iButtonContinue = 
                                    iButtonDevice.Link.iButtonRxResponseBuff[ 2];
                            if( fnIbuttonGetFlagReadCBExt( &iButtonDevice ) == TRUE )
                            {
                                // If it's a boot loader message, goto ReadCBExtended
                                iButtonHISRState.currentState &= ~READ_STATUS_PEND;
                                iButtonHISRState.currentState |= READ_CB_EXTENDED_PEND;
                                
                                // Generate the Read Status release command
                                iButtonGenerateReleaseSeq( READ_STATUS_REL, &iButtonDevice);
                                // Seed the transmit
                                iButtonPutTxFIFO( NO_START, &iButtonDevice);
                            }
                            else
                            {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 102;
                                // Get the Read I/O params length from the status
                                iButtonDevice.Trans.iButtonRxSegLength = 
                                    iButtonDevice.Link.iButtonRxResponseBuff[ 1];   
                                // Advance the state
                                iButtonHISRState.currentState &= ~READ_STATUS_PEND;
                                iButtonHISRState.currentState |= READ_IO_BUFFER_PEND;

                                // Generate the Read Status release command
                                iButtonGenerateReleaseSeq( READ_STATUS_REL, &iButtonDevice);
                                // Seed the transmit
                                iButtonPutTxFIFO( NO_START, &iButtonDevice);
                            } 
                        }
                    }
                    else 
                    {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 103;
                       // Set the error 
                        iButtonDevice.Status.lastError = READ_STATUS_ERROR;
                        // Tell the Bob Task
                        iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/READ_STATUS_ERROR;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set the state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                    break;    
                                    
                case READ_IO_BUFFER_PEND:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 104;
                    iButtonGetResponseData( &iButtonDevice );

                    // Do NOT log release sequence response from IButton
                    // DO NOT CHECK Release Sequence Status because it NEVER is "success" ( 0 ).
                    // Dallas doesn't even define a Read Status Release Sequence, however, if we
                    //  skip it, then the whole thing fails. Don't know why though!!!

                    // Message incomplete indicates a multi-block Tx
                    if( iButtonDevice.Status.iButtonContinue == MSG_INCOMPLETE) 
                    {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 105;
                       iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET_OD, 
                                                            &calcCRC, &iButtonDevice );
                        if( iButtonStatus == IBUTTON_OK ) 
                        {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 106;
                            iButtonDevice.Trans.iButtonTxCurSegment++;
                            iButtonHISRState.currentState = IB_XMIT_PENDING | READ_STATUS_VAL;
                        }
                    }
                    // 2.29.04 B. Hannigan added second type of continue status to make
                    // the Transport Unlock command work for Version 1.5 ibuttons.
                    else if((iButtonDevice.Status.iButtonContinue & IBTN_OWMS_STATUS_MASK )== CONTINUE_STATUS2) 
                    {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 107;
                       // Generate a WRITE_STATUS
                        (void)iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET_OD, &calcCRC, &iButtonDevice );
                        // Advance the state
                        iButtonHISRState.currentState &= ~READ_IO_BUFFER_PEND;
                        iButtonHISRState.currentState |= READ_CONTINUE_PROGRAM;
                        if(fLogIbutDrvMsgs)
                        {
                            fnSystemLogEntry(SYSLOG_TEXT, "Error Code 0x3F", 15);
                        }
                    }
                    else // Go onto reading the reply...
                    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 108;
                        iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET_OD, 
                                                            &calcCRC, &iButtonDevice );
                        if( iButtonStatus == IBUTTON_OK ) 
                        {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 109;
                            iButtonHISRState.currentState &= ~READ_IO_BUFFER_PEND;
                            if( fnIbuttonGetFlagBootMsg( &iButtonDevice ) == TRUE )
                            {   // Skip IO buffer read, if this is a boot loader message.
                                iButtonHISRState.currentState |= READ_IPR_PARMS_PEND;
                                iButtonDevice.Trans.iButtonRxSegLength = ubBLReplyLen;
                            }
                            else
                            {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 110;
                               iButtonHISRState.currentState |= READ_IO_BUFFER_RESET_PEND;
                            }
                        }
                    }
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 111;
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                    break;

                case READ_IO_BUFFER_RESET_PEND:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 112;
                    iButtonGetResponseData( &iButtonDevice );
                    if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                    { 
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 113;
                        iButtonHISRState.currentState &= ~READ_IO_BUFFER_RESET_PEND;
                        iButtonHISRState.currentState |= READ_IO_BUFFER_VAL;
                        (void)iButtonGenerateCmd( IBUTTON_CMD, READ_IO_BUFFER, &calcCRC, &iButtonDevice );
                        iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                    }    
                    else 
                    {
                        // Set note the error 
                        iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                        // Tell the Bob Task
                        iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_OD_RESET_FAIL;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set the state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                   break;
                    
                case READ_IO_BUFFER_VAL:                    
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 114;
                    // Check the CRC16 return
                    iButtonGetResponseData( &iButtonDevice );
                    iButtonStatus = iButtonCheckCRC( READ_IO_BUFFER, calcCRC, &iButtonDevice );
                    if( iButtonStatus == IBUTTON_OK ) 
                    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 115;
                        // Save the length and other data(??)
                        iButtonDevice.Trans.iButtonRxSegLength = 
                                iButtonDevice.Link.iButtonRxResponseBuff[ 1 ];
                        // If ok, advance the state
                        iButtonHISRState.currentState &= ~READ_IO_BUFFER_VAL;
                        iButtonHISRState.currentState |= READ_IPR_PARMS_PEND_RESET;
                        // Generate the Read IO release command
                        iButtonGenerateReleaseSeq( READ_IO_BUFFER_REL, &iButtonDevice);
                        // Seed the transmit
                        iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                    }
                    else 
                    {
                        // Set the error 
                        iButtonDevice.Status.lastError = READ_IO_ERROR;
                        // Tell the Bob Task
                        iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/READ_IO_ERROR;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set the state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                    break;

                case READ_IPR_PARMS_PEND_RESET:
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 116;
                   iButtonGetResponseData( &iButtonDevice );
                    // Do not log release sequence response from IButton
                    //if(fLogIbutDrvMsgs)
                    //{
                        // add a log entry with the msg payload
                        //fnSystemLogEntry(SYSLOG_IBUTTON, (char *)iButtonDevice.Link.iButtonRxResponseBuff, 1);
                    //}
                    if ( iButtonDevice.Link.iButtonRxResponseBuff[0] != 0 )
                    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 117;
                        // Set the error 
                        iButtonDevice.Status.lastError = WRITE_IO_ERROR;
                        // Tell the Bob Task
                        iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/WRITE_IO_ERROR;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set the state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                    else 
                    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 118;
                        iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET_OD,  
                                                            &calcCRC, &iButtonDevice);
                        if( iButtonStatus == IBUTTON_OK ) 
                        {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 119;
                            iButtonHISRState.currentState &= ~READ_IPR_PARMS_PEND_RESET;
                            iButtonHISRState.currentState |= READ_IPR_PARMS_PEND;
                            iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                        }
                    }
                    break;
                
                case READ_IPR_PARMS_PEND:
                    		DebugIBtnDriver[DebugIBtnDriverCtr++] = 120;
					// Advance the state
                    iButtonGetResponseData( &iButtonDevice );
                    if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                    { 
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 121;
                        iButtonHISRState.currentState &= ~READ_IPR_PARMS_PEND;
                        iButtonHISRState.currentState |= READ_IPR_DATA_PEND_RESET;
                        // Generate the Read I/O command
                        (void)iButtonGenerateCmd( IBUTTON_CMD, READ_IPR_REGISTER, &calcCRC, &iButtonDevice );
                        // Seed the transmit
                        iButtonPutTxFIFO( NO_START, &iButtonDevice ); 
                    }
                    else 
                    {
                        // Set note the error 
                        iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                        // Tell the Bob Task
                        iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_OD_RESET_FAIL;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set the state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                    break;

                case READ_IPR_DATA_PEND_RESET:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 122;
                    iButtonGetResponseData( &iButtonDevice );
                    iButtonStatus = iButtonCheckCRC( READ_IPR_REGISTER, calcCRC, &iButtonDevice );
                    if( iButtonStatus == IBUTTON_OK ) 
                    {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 123;
                       // Append the data to the Bob structure 
                        iButtonReassembleAPDU( &iButtonDevice, iButtonBobIfPtr);
                        if( iButtonDevice.Status.iButtonContinue == CONTINUE_STATUS) 
                        {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 124;
                           if( ( iButtonStatus = iButtonGenerateCmd( DS2480_CMD, 
                                                                      DS2480_OWRESET_OD, &calcCRC, &iButtonDevice ) ) == 
                                                                     IBUTTON_OK ) 
                            {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 125;
                                iButtonHISRState.currentState &= ~READ_IPR_DATA_PEND_RESET;
                                iButtonHISRState.currentState |= READ_IPR_DATA_PEND;
                                iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                            }
                        }    
                        else 
                        {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 126;
                            // Tell the Bob Task
                            OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->okMask, OS_OR);
                            if( fLogIbutDrvMsgs )
                            {
                                // add a log entry with the msg payload
                                fnSystemLogEntry(SYSLOG_IBUTTON, (char *)iButtonBobIfPtr->pDataRecv[0], iButtonBobIfPtr->replyLen[0]);
                            }
                            // If ok, reset the state
                            iButtonHISRState.currentState  = IB_READY;
                            iButtonDevice.Trans.iButtonRxCurSegment = 0;
                        }
                    }
                    else 
                    {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 127;
                       // Set the error 
                        iButtonDevice.Status.lastError = READ_IPR_ERROR;
                        // Tell the Bob Task
                        iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/READ_IPR_ERROR;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set the state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                    
                    break;
                
                case READ_IPR_DATA_PEND:
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 128;
                   iButtonGetResponseData( &iButtonDevice );
                    if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                    { 
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 129;
                        (void)iButtonGenerateCmd( IBUTTON_CMD, WRITE_STATUS, &calcCRC, &iButtonDevice );
                        // Advance the state
                        iButtonHISRState.currentState &= ~READ_IPR_DATA_PEND;
                        iButtonHISRState.currentState |= WRITE_STATUS_PEND;
                        iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                    }
                    else 
                    {
                        // Set note the error 
                        iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                        // Tell the Bob Task
                        iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_OD_RESET_FAIL;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set the state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                    break;

                case WRITE_STATUS_PEND:
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 130;
                   iButtonGetResponseData( &iButtonDevice );
                    iButtonStatus = iButtonCheckCRC( WRITE_STATUS, calcCRC, &iButtonDevice);
                    if( iButtonStatus == IBUTTON_OK ) 
                    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 131;
                        // If ok advance the state
                        iButtonHISRState.currentState &= ~WRITE_STATUS_PEND;
                        iButtonHISRState.currentState |= READ_CONTINUE_PROGRAM_RESET;
                        // ...and generate the release sequence
                        iButtonGenerateReleaseSeq( WRITE_STATUS_REL, &iButtonDevice);
                        // Seed the transmit
                        iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                    }
                    else 
                    {
                        // Set the error 
                        iButtonDevice.Status.lastError = WRITE_STATUS_ERROR;
                        // Tell the Bob Task
                        iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/WRITE_STATUS_ERROR;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set the state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                    break;
                        
                case READ_CB_EXTENDED_PEND:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 132;
                    // Just sent the ReadStatus Release Sequence.  This one, we ignore the response.
                    iButtonGetResponseData( &iButtonDevice );
                    // Extended read commands are only used with boot loader commands, which 
                    //  should not ever have the Msg Incomplete condition.
                    // If the i-button requested more time to process the last input data.
                    if( (iButtonDevice.Status.iButtonContinue & IBTN_OWMS_STATUS_MASK) == CONTINUE_STATUS2 ) 
                    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 133;
                        // Generate a WRITE_STATUS
                        (void)iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET_OD, &calcCRC, &iButtonDevice );
                        // Advance the state
                        iButtonHISRState.currentState &= ~READ_CB_EXTENDED_PEND;
                        iButtonHISRState.currentState |= READ_CONTINUE_PROGRAM;
                        if( fLogIbutDrvMsgs )
                        {
                            fnSystemLogEntry( SYSLOG_TEXT, "Error Code 0x3F", 15 );
                        }
                    }
                    else 
                    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 134;
                        iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET_OD,
                                                            &calcCRC, &iButtonDevice );
                        if( iButtonStatus == IBUTTON_OK ) 
                        {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 135;
                            iButtonHISRState.currentState &= ~READ_CB_EXTENDED_PEND;
                            iButtonHISRState.currentState |= READ_CB_EXT_RESET_PEND; 
                        }
                    }
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 136;
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                    break;

                case READ_CB_EXT_RESET_PEND:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 137;
                    // Sent a 2480 OD Reset, check response is OK, then send Extended Read Command.
                    iButtonGetResponseData( &iButtonDevice );
                    if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                    { 
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 138;
                        iButtonHISRState.currentState &= ~READ_CB_EXT_RESET_PEND;
                        iButtonHISRState.currentState |= READ_CB_EXT_BUFFER_LEN;
                        (void)iButtonGenerateCmd( IBUTTON_CMD, EXTENDED_READ_CB, &calcCRC, &iButtonDevice );
                        iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                    }    
                    else 
                    {
                        // Set note the error 
                        iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                        // Tell the Bob Task
                        iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_OD_RESET_FAIL;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set the state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                   break;
                    
                case READ_CB_EXT_BUFFER_LEN:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 139;
                    // Sent a Read CB Extended command.  Read the length from the i-button,
                    //  and then get the rest of the message.
                    iButtonGetResponseData( &iButtonDevice );
                    // Save the length (rx'd low byte first), and reset the Rx buffer.
                    uwRxLen = iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] +
                              (0x100 * iButtonDevice.Link.iButtonRxResponseBuff[ 1 ]);
#ifdef IBUTTON_LOG
                    fnIbuttonLogEntry( SYSLOG_IBTN_DRV, SL_IBTN_LEN, SL_IBTN_DRVR, 
                                       EXTENDED_READ_CB, (char *)&uwRxLen, 2 ); 
#endif
                    if( uwRxLen == 0 )
                    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 140;
                        // Set note the error 
                        iButtonDevice.Status.lastError = WRITE_STATUS_ERROR;
                        // Tell the Bob Task
                        iButtonBobIfPtr->errorFlags = /* BOB_*/WRITE_STATUS_ERROR;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set the state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                    else
                    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 141;
                        // The length does not include the crc, so add that.
                        uwRxLen += 2;
                        if( uwRxLen > EXT_RX_BUFF_LEN )
                        {
                            fnIbuttonSetFlagOverrun( &iButtonDevice );
                            // add a log entry with the msg payload
                            sprintf( iBtnSysLogBuff, "Ibtn BL response too long, cmd= %d, len= %d", 
                                     iButtonBobIfPtr->pMessage[ IBTN_BL_CMD_OFFSET ], uwRxLen );
                            fnDumpStringToSystemLog( iBtnSysLogBuff );
                            // Set note the error 
                            iButtonDevice.Status.lastError = EXTENDED_READ_ERROR;
                            // Tell the Bob Task
                            iButtonBobIfPtr->errorFlags = /* BOB_*/EXTENDED_READ_ERROR;
                            OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                            // Set the state to Idle
                            iButtonHISRState.currentState = IB_IDLE;
                        }
                        else
                        {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 142;
                            // Set the total number of bytes left to receive.
                            iButtonDevice.Link.iButtonExtCmdRxLenRemaining = uwRxLen;
                            // Start storing the rx'd message at index 0.
                            fniButtonResetResponseData( &iButtonDevice );
                            iButtonHISRState.currentState &= ~READ_CB_EXT_BUFFER_LEN;
                            iButtonHISRState.currentState |= READ_CB_EXTENDED_VAL;
                            fniButtonContinueExtendedRead( &iButtonDevice );
                            // Read the data
                            iButtonPutTxFIFO( NO_START, &iButtonDevice ); 
                        }
                    }
                    break;

                case READ_CB_EXTENDED_VAL:
                    // Reading the Extended CB data...
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 143;
                    fniButtonGetExtendedResponse( &iButtonDevice );
                    if( iButtonDevice.Link.iButtonExtCmdRxLenRemaining > 0 )
                    {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 144;
                       // Read more bytes, and stay in this state until 
                        //  we've read them all!
                        fniButtonContinueExtendedRead( &iButtonDevice );
                        iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                        break;
                    }
                    else
                    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 145;
                        // We've read all the bytes, so check the CRC
                        iButtonStatus = iButtonCheckCRC( EXTENDED_READ_CB, 0, &iButtonDevice ); 
                        if( iButtonStatus == IBUTTON_OK ) 
                        {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 146;
                            // Append the data to the Bob structure 
                            (void)iButtonReassembleAPDU( &iButtonDevice, iButtonBobIfPtr );
                            if( iButtonDevice.Status.iButtonContinue == CONTINUE_STATUS ) 
                            {
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 147;
                               // If the status is CONTINUE, then we need to do a write status,
                                //  release sequence, and CONTINUE sequence (like a START sequence)
                                //  to allow the i-button another time-slice, before reading again. 
                                iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET_OD, 
                                                                    &calcCRC, &iButtonDevice );
                                if( iButtonStatus == IBUTTON_OK ) 
                                {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 148;
                                    iButtonHISRState.currentState &= ~READ_CB_EXTENDED_VAL;
                                    iButtonHISRState.currentState |= READ_IPR_DATA_PEND;
                                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                                }
                            }
                            else 
                            {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 149;
                                // Tell the Bob Task
                                OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->okMask, OS_OR );
                                if( fLogIbutDrvMsgs )
                                {
                                    // add a log entry with the msg payload
                                    fnSystemLogEntry( SYSLOG_IBUTTON, 
                                                      (char *)iButtonBobIfPtr->pDataRecv[0], 
                                                      iButtonBobIfPtr->replyLen[0] );
                                }
                                // If ok, reset the state
                                iButtonHISRState.currentState  = IB_READY;
                                iButtonDevice.Trans.iButtonRxCurSegment = 0;
                            }
                        } // End of good CRC check.
                        else 
                        {
                            // Set the error 
                            iButtonDevice.Status.lastError = EXTENDED_READ_ERROR;
                            // Tell the Bob Task
                            iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/EXTENDED_READ_ERROR;
                            OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                            // Set the state to Idle
                            iButtonHISRState.currentState = IB_IDLE;
                        }
                    }// End of all bytes have been received
                    break;
                
                case READ_CONTINUE_PROGRAM_RESET:
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 150;
                   iButtonGetResponseData( &iButtonDevice );
                    // Do not log release sequence response from IButton
                    //if(fLogIbutDrvMsgs)
                    //{
                        // add a log entry with the msg payload
                        //fnSystemLogEntry(SYSLOG_IBUTTON, (char *)iButtonDevice.Link.iButtonRxResponseBuff, 1);
                    //}
                    if ( iButtonDevice.Link.iButtonRxResponseBuff[0] != 0 )
                    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 151;
                        // Set the error 
                        iButtonDevice.Status.lastError = WRITE_STATUS_ERROR;
                        // Tell the Bob Task
                        iButtonBobIfPtr->errorFlags = /* BOB_IBUTTON_*/WRITE_STATUS_ERROR;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set the state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                    else
                    {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 152;
                        iButtonStatus = iButtonGenerateCmd( DS2480_CMD, DS2480_OWRESET_OD,
                                                            &calcCRC, &iButtonDevice );
                        if( iButtonStatus == IBUTTON_OK ) 
                        {
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 153;
                            iButtonHISRState.currentState &= ~READ_CONTINUE_PROGRAM_RESET;
                            iButtonHISRState.currentState |= READ_CONTINUE_PROGRAM;
                            iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                        }
                    }
                    break;
                
                case READ_CONTINUE_PROGRAM:
 		DebugIBtnDriver[DebugIBtnDriverCtr++] = 154;
                   // Advance the state
                    iButtonGetResponseData( &iButtonDevice );
                    if( iButtonDevice.Link.iButtonRxResponseBuff[ 0 ] == DS2480_RESET_RESPONSE_OK ) 
                    { 
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 155;
                        iButtonHISRState.currentState &= ~READ_CONTINUE_PROGRAM;
                        iButtonHISRState.currentState |= READ_CONTINUE_PROGRAM_PEND;                                                    
                        // ..and generate the CONTINUE PROGRAM command with the release sequence
                        (void)iButtonGenerateCmd( IBUTTON_CMD, CONTINUE_PROGRAM, &calcCRC, &iButtonDevice );
                        // Seed the transmit
                        iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                    }
                    else 
                    {
                        // Set note the error 
                        iButtonDevice.Status.lastError = IBUTTON_OD_RESET_FAIL;
                        // Tell the Bob Task
                        iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_OD_RESET_FAIL;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set the state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                    break;
            
                case READ_CONTINUE_PROGRAM_PEND:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 156;
                    // Do the release for the start
                    (void)iButtonGenerateCmd( DS2480_CMD, DS2480_SINGLE_BIT_WRITE_0, &calcCRC, &iButtonDevice );
                    iButtonHISRState.currentState &= ~READ_CONTINUE_PROGRAM_PEND;
                    iButtonHISRState.currentState |= READ_CONTINUE_PROGRAM_DONE;                                                    
                    iButtonPutTxFIFO( NO_START, &iButtonDevice); 
                    break;
                
                case READ_CONTINUE_PROGRAM_DONE:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 157;
                    pGPIOReg->GPIO_SETDATAOUT = IBTN_INT_ENB; // enable the current sense flip-flop by releasing the 'clr'
                    iButtonHISRState.currentState = IB_RCV_PENDING | RCV_DONE_PEND;
                    break;
                    
                case RCV_ERROR:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 158;
                    // Set the error code
                    // Tell the Bob Task
                    // Reset the State to Ready
                    break;
                default:
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 159;
                    break;
            }
        } // End of event = EV_IB_RCV_DONE
    }
			DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xDD;
			DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) (iButtonHISRState.currentState >> 16);
			DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) (iButtonHISRState.currentState);
}

// **********************************************************************
// FUNCTION NAME: fnIButtonCommTask
//
// PURPOSE: This function is the entry point for the iButton 
//          communications task. This task pends on one of three intertask
//          messages:  INIT_IBUTTON_COMMAND
//                     TRANSMIT_COMMAND 
//                     TRANSMIT_BL_COMMAND 
//          All commands will activate the iButton HISR which is basically
//          a finite state machine made up of states and substates.
//          The 'next state' and 'next substate' is determined by:
//              'present state' and possibly:
//                  a)result (response) of previous iButton command 
//                  b)HISR and LISR generated events
//
// AUTHOR: Mike Ernandez/Tom Dometios
//
// INPUTS:
// RETURNS:
// **********************************************************************/
extern IBUTTON_STATUS iButtonPullUp( UINT16 OnOff , volatile UINT8 *puPort ,
                                     UINT8 puMask, UINT8 stMask);
#if 0 //FIXME JAH Remove me once IButton hardware is working
unsigned char dummyIButtonSuccess[] = { 0x90, 0x00};
unsigned char dummyGetDevice[] = {0x64, 0x13, 0x15, 0x00, 0x00, 0x00, 0x00, 0x70, 0x90, 0x00};
unsigned char dummyGetVersionBl[] =  {0x4A, 0x52, 0x42, 0x80, 0x00, 0x00, 0x00, 0x00,
								   'M', 'A', 'X', 'Q', '1', '9', '5', '9', ' ', 'V', 'e', 'r', 's', 'i', 'o', 'n', ' ', 'A', 'M', 0x00,
									0x90, 0x00 };
unsigned char dummyGetVersion[] =  {0x38, 0x4d, 0x41, 0x58, 0x51, 0x31, 0x39, 0x35, 0x39, 0x20, 0x50, 0x42, 0x2d, 0x50, 0x53, 
									0x44, 0x20, 0x2d, 0x20, 0x46, 0x69, 0x72, 0x6d, 0x77, 0x61, 0x72, 0x65, 0x20, 0x52, 0x65, 
									0x76, 0x69, 0x73, 0x69, 0x6f, 0x6e, 0x20, 0x30, 0x35, 0x2e, 0x30, 0x31, 0x2e, 0x30, 0x31, 
									0x20, 0x44, 0x65, 0x63, 0x20, 0x32, 0x35, 0x20, 0x32, 0x30, 0x31, 0x35, 0x00,
									0x90, 0x00 };
unsigned char dummyGetState0[] = {0x00, 0x00, 0x00, 0x07, 0x90, 0x00};
unsigned char dummyGetState1[] = {0x00, 0x00, 0x00, 0x0B, 0x90, 0x00};
unsigned char dummyGetModuleStatus[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x90, 0x00};
unsigned char dummyGetParamList[] = {0x0E,
									'P', 'B',
									'1', 'A',
									'0', '6', '2', '6', '9', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
										0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
										0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
									0x11,
									0x11, 0x22, 0x33, 0x44,
									0x00, 0x11, 0x11, 0x33,
									0x55, 0x66, 0x44, 0x33,
									0x00, 0x00, 0x04, 0xE8,
									0x00, 0x00, 0x00, 0x32,
									'1', '1', '2', '2', '3', '3', '4', '4', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
									'1', 'W', '0', '0', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
									'1', '1', '2', '2', '3', '3', '4', '4', 0x00, 0x00,
									0x00, 
									0x02,
									0x02,
									0x00, 0x00, 0x00, 0x01, 
									0x00, 0x02, 0xA3, 0x00,
									0x00, 0x01, 0x51, 0x80,
									0x00, 0x00, 0x00, 0x00, 0x01,
									0xFF, 0xFF, 0xFF, 0xFF, 0xF0,
									0xFF, 0xFF, 0xFF, 0xA0,
									0x02,
									0x02, 0x01,
									0x02,
									0x00,
									0x00,
									0x00, 0x00, 0x00, 0x10,
									'A', 'B', 'C',
									0x00, 0x01, 0x00, 0x01, 0x00, 0x02,
									0x00, 0x00, 0x00, 0x01,
									0x00, 0x00, 0x00, 0x04,
									0x00,
									0x00,
									0x00,
									0x00,
									0x00, 0x00, 0x00, 0x10,
									0x00, 0x00,
									0x01,
									0x00,
									0x00,
									0x01,
									0x00, 0x00, 0x00, 0x01,
									0x01,
									0x00,
									0x00,
									0x02,
									0x00, 0x01,
									0x00, 0x00, 0x00, 0x01,
									0x90, 0x00};
unsigned char dummyGetChallenge[] = {0xD9, 0xF7, 0x2B, 0x16, 0xD1, 0xFB, 0x80, 0x98, 0x90, 0x00};
unsigned char dummyGetRTC[] = {0x00, 0x02, 0x03, 0x04, 0x90, 0x00};
unsigned stateCount = 0;
#endif

void fnIButtonCommTask( unsigned long argc, void *argv )
{
    UINT8       flags = IBTNF_STANDARD_MSG;
    INTERTASK   msgIn;        // pointer to a received intertask message
    unsigned long ival;

unsigned char *pMsg;
unsigned long msgSize = 0;

//TODO	fnSysLogTaskStart("iBtnCommTask", CIBUTTON );
    // Setup  iButton Device
    iButtonDeviceInit( &iButtonDevice, iButtonChannel.uartBase, 0,0,0,
                       iButtonChannel.uartRxInt, iButtonChannel.uartTxInt,
                       iButtonChannel.RxIrqHandler, iButtonChannel.TxIrqHandler,
                       iButtonIrqHISR, "iButtnHISR", true, iButtonChannel.uartControl, 
                       PSDPWRMODEDONE, iButtonCompuDoneIrqLISR );   

    // Init the CRC table
    GenCRC16Table();

#ifdef NO_BOB_TEST
    // Test in lieu of Bob....
    iButtonAdHocTest() ;
#else

    // Wait for bob to send a request
    while( 1) {

        flags = IBTNF_STANDARD_MSG;

        if( OSReceiveIntertask( CIBUTTON, &msgIn, OS_SUSPEND) == SUCCESSFUL ) 
        {
            // Extract the data structure from the message
            iButtonBobIfPtr = msgIn.IntertaskUnion.PointerData.pData;   
            // Process the request        
            switch( msgIn.bMsgId) 
            {
#if 1 //TODO FIXME JAH Ibutton code stubbed out until hardware is ready
				case INIT_IBUTTON_COMMAND:  
                    // Check state is idle
                    if( iButtonHISRState.currentState == IB_IDLE) 
                    {
#ifdef IB_DRIVER_DEBUG
                        // Set the buffer to all spaces.
                        memset( iButtonDiagBuf, ' ', sizeof( iButtonDiagBuf ) );
                        iButtonDiagBufIndex = 0;
#endif
                        // Start the initialization
                        iButtonDevice.lwActivateEvent = EV_IB_INIT;
                        (void)OSActivateHISR( (OS_HISR *)&iButtonDevice.devHisr );
								DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xDC;
								DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) iButtonDevice.lwActivateEvent;


                    }
                    else 
                    {
                        // Incorrect state - set the error and tell Bob
                        iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_STATE_ERROR;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);

                        // Set state to Idle
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                    break;

                case RESET_IBUTTON_COMMAND:  
                    // Make sure the pullup is not active.
                    (void)iButtonPullUp( PULLUP_OFF, iButtonDevice.puPort, 
                                         iButtonDevice.puMask, iButtonDevice.stMask );
                    iButtonDevice.Link.iButtonNoResponse = FALSE;   // clear the no response flag
                    // Make sure the event is clear.
                    ival = iButtonDisableInts( &iButtonDevice );
                    iButtonDevice.lwActivateEvent = 0;
                    iButtonRestoreInts( &iButtonDevice, ival );

#ifdef IB_DRIVER_DEBUG
                    if( iButtonDiagBufIndex < 1016 ) // dont' bother if there isn't at least one more line left
                    {
                        iButtonDiagBuf[ iButtonDiagBufIndex ] = 'T';
                        iButtonDiagBufIndex++;  
                        iButtonDiagBuf[ iButtonDiagBufIndex ] = 'O';
                        iButtonDiagBufIndex++;  
                        iButtonDiagBuf[ iButtonDiagBufIndex ] = 'U';
                        iButtonDiagBufIndex++;
                        iButtonDiagBuf[ iButtonDiagBufIndex ] = 'T';
                        iButtonDiagBufIndex++;
                        iButtonDiagBuf[ iButtonDiagBufIndex ] = 'R';
                        iButtonDiagBufIndex++;
                        iButtonDiagBuf[ iButtonDiagBufIndex ] = 'S';
                        iButtonDiagBufIndex++;
                        iButtonDiagBuf[ iButtonDiagBufIndex ] = 'E';
                        iButtonDiagBufIndex++;
                        iButtonDiagBuf[ iButtonDiagBufIndex ] = 'T';
                        iButtonDiagBufIndex++;
                    }
#endif
                    // Tell the Bob Task
                    iButtonBobIfPtr->replyLen[ 0] = 0;
                    iButtonDevice.Trans.iButtonRxCurSegment = 0;
                    // Set the state to be able to initialize again.
                    iButtonHISRState.currentState = IB_IDLE; 
                    OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->okMask, OS_OR );
 								DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xDC;
								DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) iButtonDevice.lwActivateEvent;
                   break;


                case TRANSMIT_BL_COMMAND:
                    if( iButtonDevice.Status.familyCode != IPSD_GEMINI_FAMILYCODE )
                    {
                        // Should not get BootLoader messages when we have an Asteroid...
                        // Return error event to Bob
                        iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_ERR_BL_TO_ASTEROID;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        break;
                    }

                    // Check for state to be Ready
                    if( iButtonHISRState.currentState & IB_READY) 
                    {
                        // Save that this is a boot loader command...
                        flags = IBTNF_BOOT_LAYER_CMD;
                        // Geminis can use the Extended Read/Write CB commands
                        flags |= IBTNF_EXT_READ_CB;
                        flags |= IBTNF_EXT_WRITE_CB;
                    }
                    // Fallthrough:

                case TRANSMIT_COMMAND:  
                    // Check for state to be Ready                   
                    if( iButtonHISRState.currentState & IB_READY) 
                    {
#ifdef IB_DRIVER_DEBUG
                        memset( iButtonDiagBuf, ' ', sizeof( iButtonDiagBuf ) );
                        iButtonDiagBufIndex = 0;
#endif
                        // Save the flags in the link layer structure.
                        iButtonDevice.Link.iButtonFlags = flags;

                        // Make sure we clear the lengths, because one of the subroutines
                        //  just adds to the existing length.
                        iButtonBobIfPtr->replyLen[0] = iButtonBobIfPtr->replyLen[ 1 ] = iButtonBobIfPtr->replyLen[ 2 ] = 0;        
                        // Set-up data in the Tx segment from Bob interface structure
                        (void)iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr );

                        if(fLogIbutDrvMsgs)
                        {
                            // add a log entry with the msg payload
                            fnSystemLogEntry(SYSLOG_IBUTTON, (char *)iButtonBobIfPtr->pMessage, iButtonBobIfPtr->bMsgLen);
                        }
                        // Initiate the Tx
                        iButtonHISRState.currentState = IB_READY;
                        iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
                        (void)OSActivateHISR( (OS_HISR *)&iButtonDevice.devHisr );
								DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xDC;
								DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) iButtonDevice.lwActivateEvent;
                    }
                    // State not ready
                    else 
                    {
                        // Return error event to Bob
                        iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_STATE_ERROR;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        // Set state to idle and wait for reset command if timeout has elapsed
                        iButtonHISRState.currentState = IB_IDLE;
                    }
                    break;
#else //JAH
					case INIT_IBUTTON_COMMAND:  
                    // Check state is idle
#ifdef IB_DRIVER_DEBUG
                        // Set the buffer to all spaces.
                        memset( iButtonDiagBuf, ' ', sizeof( iButtonDiagBuf ) );
                        iButtonDiagBufIndex = 0;
#endif
                        // Start the initialization
                        iButtonDevice.lwActivateEvent = EV_IB_INIT;
                        iButtonBobIfPtr->replyLen[0] = iButtonBobIfPtr->replyLen[ 1 ] = iButtonBobIfPtr->replyLen[ 2 ] = 0;        
 						memcpy( &iButtonBobIfPtr->pDataRecv[ 0 ][ iButtonBobIfPtr->replyLen[ 0] ], 
								dummyGetDevice,   
								sizeof(dummyGetDevice)); 
						// Update the Data Length
						iButtonBobIfPtr->replyLen[ 0] += 2;
						iButtonDevice.Status.familyCode = *dummyGetDevice;
						memcpy( iButtonDevice.Status.serial,dummyGetDevice+1, 6);
            			OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->okMask, OS_OR );
						break;

					case RESET_IBUTTON_COMMAND:  
						iButtonDevice.Link.iButtonNoResponse = FALSE;   // clear the no response flag
						// Make sure the event is clear.
						iButtonDevice.lwActivateEvent = 0;
						 // Tell the Bob Task
						iButtonBobIfPtr->replyLen[ 0] = 0;
						iButtonDevice.Trans.iButtonRxCurSegment = 0;
						// Set the state to be able to initialize again.
						iButtonHISRState.currentState = IB_IDLE; 
						OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->okMask, OS_OR );
                    break;

					case TRANSMIT_BL_COMMAND:
                    if( iButtonDevice.Status.familyCode != IPSD_GEMINI_FAMILYCODE )
                    {
                        // Should not get BootLoader messages when we have an Asteroid...
                        // Return error event to Bob
                        iButtonBobIfPtr->errorFlags = /* BOB_*/IBUTTON_ERR_BL_TO_ASTEROID;
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->errMask, OS_OR);
                        break;
                    }
					//fall through
  
                case TRANSMIT_COMMAND:  
                        // Save the flags in the link layer structure.
                        //iButtonDevice.Link.iButtonFlags = flags;

                        // Make sure we clear the lengths, because one of the subroutines
                        //  just adds to the existing length.
                        iButtonBobIfPtr->replyLen[0] = iButtonBobIfPtr->replyLen[ 1 ] = iButtonBobIfPtr->replyLen[ 2 ] = 0;        
                        // Set-up data in the Tx segment from Bob interface structure
                       // (void)iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr );

                        if(fLogIbutDrvMsgs)
                        {
                            // add a log entry with the msg payload
                            fnSystemLogEntry(SYSLOG_IBUTTON, (char *)iButtonBobIfPtr->pMessage, iButtonBobIfPtr->bMsgLen);
                        }
                        
						//TODO JAH Stub out response and send response back see iButtonReassembleAPDU
						// Append the received data to bob's Rx buffer, without the CRC,
						//  and update bob's reply length.

						switch(iButtonBobIfPtr->pMessage[1])
						{
						case 0x00:
							pMsg = dummyGetVersionBl;
							msgSize = sizeof(dummyGetVersionBl);
							break;

						case 0x03:
							if(stateCount == 0)
							{
								stateCount++;
							pMsg = dummyGetState0;
							msgSize = sizeof(dummyGetState0);
							}
							else
							{
							pMsg = dummyGetState1;
							msgSize = sizeof(dummyGetState1);
							}
							break;

						case 0x12:
							pMsg = dummyGetModuleStatus;
							msgSize = sizeof(dummyGetModuleStatus);
						break;

						case 0x13:
							pMsg = dummyGetChallenge;
							msgSize = sizeof(dummyGetChallenge);
						break;

						case 0x1A:
							pMsg = dummyGetParamList;
							msgSize = sizeof(dummyGetParamList);
							break;

						case 0x95:
							if((iButtonBobIfPtr->pMessage[2] == 0x01) && (iButtonBobIfPtr->pMessage[3] == 0x00))
							{
								pMsg = dummyGetVersion;
								msgSize = sizeof(dummyGetVersion);
							}
							else if((iButtonBobIfPtr->pMessage[2] == 0x01) && (iButtonBobIfPtr->pMessage[3] == 0x0C))
							{
								pMsg = dummyGetRTC;
								msgSize = sizeof(dummyGetRTC);
							}
							else
							{
								pMsg = dummyIButtonSuccess;
								msgSize = sizeof(dummyIButtonSuccess);
							}
							break;

						default:
							pMsg = dummyIButtonSuccess;
							msgSize = sizeof(dummyIButtonSuccess);
						}
						memcpy( &iButtonBobIfPtr->pDataRecv[ 0 ][ iButtonBobIfPtr->replyLen[ 0] ], 
								pMsg,   
								msgSize); 
						// Update the Data Length
						iButtonBobIfPtr->replyLen[ 0] += msgSize;
						// Tell the Bob Task
                        OSSetEvents( iButtonBobIfPtr->eventID, iButtonBobIfPtr->okMask, OS_OR );
                    break;

#endif //JAH
                
                default:    
                    break;
            }
        
        }
        else
        {
            ;/* not successful, display error ? */
        }
    }   
#endif
}

/******************************************************************************/
/*                                                                            */
/*  Function Name: iButtonAdHocTest                                           */
/*                                                                            */
/*  Function Description:                                                     */
/*                                                                            */
/*                                                                            */
/*  Inputs:                                                                   */
/*                                                                            */
/*  Outputs:                                                                  */
/*                                                                            */
/*                                                                            */
/*  Returns:                                                                  */
/*                                                                            */
/******************************************************************************/
#ifdef NO_BOB_TEST
static void iButtonAdHocTest( void) 
{
    BOOL fStatus;

    // Test commands
    const unsigned char getFreeRAM[] = {0xd0,0x95,0x01,0x01};
    const unsigned char getPSDKeyData[] = {0x80,0x1B,0x00,0x00,0x00};
    const unsigned char getPSDParamData[] = {0x80,0x1A,0x00,0x00,0x00};
    const unsigned char getRTCData[] = {0xd0,0x95,0x01,0x0c,0x00};
    const unsigned char getFirmVerData[] = {0xd0,0x95,0x01,0x00,0x00};
    const unsigned char getModuleStatusData[] = {0x80,0x12,0x00,0x00,0x00};
    const unsigned char getChallengeData[] = {0x80,0x13,0x00,0x00,0x00};
    const unsigned char userPasswordData[ 20] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                                    0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38};
    const unsigned char userLoginData[ 30] = {0x80,0x14,0x00,0x00,0x14};
    const unsigned char createIndicumData[] = 
            {0x80,0x0D,0x00,0x00,0x0C,0x00,0x00,0x00,0x01,0x06,0x20,0xD9,0x00,0x00,0x00,0x00,0x00}; 
    const unsigned char verifyHashP1Data[] = 
        {   0x80,0x16,0x01,0x00,0xF5,0x0D,0x41,0x73,0x69,0x6D,0x20,0x69,0x73,0x20,0x61,0x20,
            0x63,0x6F,0x6F,0x6C,0x20,0x64,0x75,0x64,0x65,0x2E,0x05,0x24,0x8E,0xE1,0xA0,0x55, 
            0xC7,0xB8,0x0B,0xF7,0x30,0xFC,0x30,0x24,0x3C,0x3C,0x69,0x6C,0x27,0xF1,0x06,0x79, 
            0xC0,0x1D,0xF4,0x2C,0xF3,0x24,0xE7,0x89,0x4C,0xCE,0x08,0x49,0xF8,0xC8,0x11,0x59, 
            0x02,0x5F,0x00,0x80,0x00,0x00,0x00,0x14,0x00,0x80,0x00,0x80,0x00,0x94,0x00,0x80, 
            0x01,0x14,0xE0,0x97,0xE9,0xA1,0xAB,0xDF,0xDC,0x4F,0xC5,0xD4,0xBE,0x74,0xE8,0x30, 
            0x3D,0x1F,0x1A,0xB7,0xBA,0xE0,0xA3,0x34,0xDB,0xC0,0xC1,0xDC,0x2B,0xB0,0x96,0x1C, 
            0x32,0x56,0x54,0x6B,0x9F,0x0F,0xB7,0x74,0xFF,0xF6,0x73,0x67,0x3F,0x09,0xCA,0x00, 
            0xCF,0xFF,0x6E,0x93,0xDB,0x3B,0xD1,0xFE,0x55,0x7A,0xBB,0x36,0x49,0x20,0xBE,0xC8, 
            0x03,0x1A,0x46,0x10,0x34,0x35,0x46,0x9A,0xBA,0xB3,0x3F,0x0D,0x16,0x74,0xEE,0x85, 
            0x9B,0xB0,0x76,0x86,0xCE,0x4F,0xAA,0xD0,0x3A,0xF8,0x60,0xD4,0x33,0xA6,0x67,0x8D, 
            0x28,0x04,0x31,0xC4,0xD5,0xA5,0x0E,0x14,0x70,0x63,0x42,0xB7,0xF8,0xB7,0x3D,0xF9, 
            0xEC,0x49,0xCB,0xE6,0xBF,0xB1,0xFF,0xB9,0xF5,0x67,0x1F,0x72,0xEC,0xD5,0xC2,0x96, 
            0x36,0x61,0xD1,0xCE,0xEC,0xB8,0xF5,0x36,0x24,0xBF,0x05,0x2F,0xF2,0xE6,0xD0,0x84, 
            0xE6,0x03,0xFB,0x3A,0x22,0xC3,0x24,0xE7,0x87,0xAE,0x0C,0x62,0x85,0x38,0x26,0xEB, 
            0x43,0x22,0xB4,0x1D,0x7F,0xA2,0xBC,0xD9,0xE6,0x84,0x00 
        };
    const unsigned char verifyHashP2Data[] = 
        {   0x80,0x16,0x02,0x00,0xF5,0x87,0x9D,0x7C,0x05,0x1E,0xE7,0xAF,0x7F,0x80,0xEB,0xC7, 
            0x86,0x30,0xB9,0xBC,0x05,0x20,0xA9,0xCD,0x8E,0x5E,0x0B,0x89,0x82,0x14,0xB8,0x5E, 
            0x1E,0x4B,0x06,0x30,0xE4,0x61,0x51,0xFA,0xD4,0x3A,0x8E,0xFB,0x80,0x81,0x26,0xDD, 
            0xFC,0x1B,0xC5,0x85,0x2E,0x36,0x33,0x10,0x88,0x47,0xB5,0xE4,0x81,0x2F,0x98,0xFB, 
            0x61,0x3A,0xB3,0x8E,0x3E,0x3C,0xB7,0xE7,0x52,0x69,0x94,0x8F,0x3B,0x6B,0xF2,0x7D, 
            0x5F,0x52,0xFE,0x1D,0xD0,0x91,0xD9,0x34,0x5E,0x27,0xF0,0x75,0x5A,0x5F,0x19,0x6C, 
            0xC1,0x0E,0x0C,0xE6,0x5F,0x36,0xFF,0x4A,0x5F,0x0A,0x48,0xDF,0x91,0xDA,0x2B,0x1E, 
            0xC8,0x00,0x36,0xF4,0x02,0x5D,0xEE,0x7E,0x7D,0x03,0x1C,0xDB,0x22,0xC9,0x25,0x01, 
            0x15,0xEC,0xA7,0x8B,0x52,0x6A,0x0D,0x39,0x10,0xA7,0x9E,0xA2,0x0F,0x38,0x26,0x3D, 
            0x51,0x5B,0xF4,0x7A,0xF9,0x0C,0xFC,0xD4,0xBE,0x18,0x79,0x98,0xEA,0x50,0x37,0x69, 
            0x11,0x4A,0xC5,0x81,0x06,0xB5,0x7D,0x5F,0x44,0x45,0xAB,0x0A,0x87,0xD8,0x2B,0xC8, 
            0xE5,0xBF,0x1B,0xBA,0xE7,0x0E,0x91,0xB4,0xF0,0xE7,0x75,0x28,0x2A,0x0A,0x5D,0xBF, 
            0x6A,0x4D,0x15,0x39,0xB1,0xBF,0xA9,0xFC,0x01,0xCF,0x70,0x45,0xBC,0x07,0x6F,0x6E, 
            0x9D,0x86,0x7C,0x46,0x21,0x87,0xC4,0xD9,0xD9,0xF8,0x34,0xE1,0xC7,0x46,0x84,0x6C, 
            0xB5,0x1E,0xA8,0xF5,0xCB,0x65,0x6D,0xD2,0x70,0x74,0xA0,0xEC,0x89,0x66,0x18,0x01, 
            0x84,0x66,0xF6,0x2B,0x46,0x1C,0xAA,0xDE,0xCF,0x87
        };
    const unsigned char verifyHashP3Data[] = 
        {   0x80,0x16,0x03,0x00,0x1F,0xFB,0x4C,0x12,0x32,0x4D,0xC6,0x88,0x99,0x56,0x29,0x2C, 
            0xCF,0xBB,0xDA,0x56,0xDA,0xB4,0xFF,0x26,0x15,0x6C,0xF0,0x83,0x9B,0x13,0xEF,0x77, 
            0x14,0xFB,0x14,0x5E,0x00
        };

    // Simulate what would be passed by Bob
    // Get Tx and Rx Data pointers and initialize
    iButtonBobIfPtr = &iButtonBobIf;
    iButtonBobIf.pMessage = (unsigned char *)malloc( 256);
    iButtonBobIf.pDataRecv[ 0] = (unsigned char *)malloc( 2048);

    // Wake up the state machine
    iButtonDevice.lwActivateEvent = EV_IB_INIT;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
								DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xDC;
								DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) iButtonDevice.lwActivateEvent;

    // Wait until init is complete
    while( !(iButtonHISRState.currentState & IB_READY))
        OSWakeAfter(100);

    // Do the APDU tests here...

    // VerifyHash - Multi Tx Test 
    // Packet 1
#if 0
    memcpy( iButtonBobIf.pMessage,verifyHashP1Data, sizeof(verifyHashP1Data)); 
    iButtonBobIf.bMsgLen = sizeof(verifyHashP1Data);
    iButtonBobIf.replyLen[ 0] = 0;
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
								DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xDC;
								DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) iButtonDevice.lwActivateEvent;
    
    // Wait until previous cmd is complete
    OSWakeAfter(1000);  //Wait a crapload for this to start
    while( !(iButtonHISRState.currentState & IB_READY))
        OSWakeAfter(100);

    // Packet 2
    memcpy( iButtonBobIf.pMessage,verifyHashP2Data, sizeof(verifyHashP2Data)); 
    iButtonBobIf.bMsgLen = sizeof(verifyHashP2Data);
    iButtonBobIf.replyLen[ 0] = 0;
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    
    // Wait until previous cmd is complete
    OSWakeAfter(1000);  //Wait a crapload for this to start
    while( !(iButtonHISRState.currentState & IB_READY))
        OSWakeAfter(100);

    // Packet 3
    memcpy( iButtonBobIf.pMessage,verifyHashP3Data, sizeof(verifyHashP3Data)); 
    iButtonBobIf.bMsgLen = sizeof(verifyHashP3Data);
    iButtonBobIf.replyLen[ 0] = 0;
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    
    // Wait until previous cmd is complete
    OSWakeAfter(1000);  //Wait a crapload for this to start
    while( !(iButtonHISRState.currentState & IB_READY))
        OSWakeAfter(100);

    // GetFreeRAM
    memcpy( iButtonBobIf.pMessage,getFreeRAM, sizeof(getFreeRAM)); 
    iButtonBobIf.bMsgLen = sizeof(getFreeRAM);
    iButtonBobIf.replyLen[ 0] = 0;
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    
    // Wait until previous cmd is complete
    OSWakeAfter(1000);  //Wait a crapload for this to start
    while( !(iButtonHISRState.currentState & IB_READY))
        OSWakeAfter(100);

    //***** Debit Test *************
    //***** Debit Test *************

    //****************************
    // Get Module Status - to check registers
    //****************************
    memcpy( iButtonBobIf.pMessage,getModuleStatusData, sizeof(getModuleStatusData)); 
    iButtonBobIf.bMsgLen = sizeof(getModuleStatusData);
    iButtonBobIf.replyLen[ 0] = 0;
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);
    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    // Wait until previous cmd is complete
    OSWakeAfter(1000);  //Wait a crapload for this to start
    while( !(iButtonHISRState.currentState & IB_READY))
        OSWakeAfter(100);


    //****************************
    // Get Challenge
    //****************************
    memcpy( iButtonBobIf.pMessage,getChallengeData, sizeof(getChallengeData)); 
    iButtonBobIf.bMsgLen = sizeof(getChallengeData);
    iButtonBobIf.replyLen[ 0] = 0;
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);
    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    // Wait until previous cmd is complete
    OSWakeAfter(1000);  //Wait a crapload for this to start
    while( !(iButtonHISRState.currentState & IB_READY))
        OSWakeAfter(100);

    //****************************
    // compute SHA-1 hash from UserPassword + 8 random bytes from Get Challenge
    //****************************
// Need the Sha1 stuff that is NOT in the build...
    memcpy( &userPasswordData[ 0], iButtonBobIf.pDataRecv[ 0], 8);

    //-------> calculate SHA-1 hash
//  SHA1Reset(&sha);
//  SHA1Input(&sha, userPasswordData,16);
//  SHA1Result(&sha, hash);         

    //****************************
    // User Login + 20 Hash bytes from above
    //****************************
    //-------> append hash to user login data
    memcpy( &userLoginData[ 5], hash, 20);
    memcpy( iButtonBobIf.pMessage,userLoginData, 25); 
    iButtonBobIf.bMsgLen = 25;
    iButtonBobIf.replyLen[ 0] = 0;
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);
    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    // Wait until previous cmd is complete
    OSWakeAfter(1000);  //Wait a crapload for this to start
    while( !(iButtonHISRState.currentState & IB_READY))
        OSWakeAfter(100);

    //****************************
    // Create Indicum 
    //****************************
    memcpy( iButtonBobIf.pMessage,createIndicumData, sizeof(createIndicumData)); 
    iButtonBobIf.bMsgLen = sizeof(createIndicumData);
    iButtonBobIf.replyLen[ 0] = 0;
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);
    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    // Wait until previous cmd is complete
    OSWakeAfter(1000);  //Wait a crapload for this to start
    while( !(iButtonHISRState.currentState & IB_READY))
        OSWakeAfter(100);

    //****************************
    // Get Module Status - to check registers
    //****************************
    memcpy( iButtonBobIf.pMessage,getModuleStatusData, sizeof(getModuleStatusData)); 
    iButtonBobIf.bMsgLen = sizeof(getModuleStatusData);
    iButtonBobIf.replyLen[ 0] = 0;
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);
    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    // Wait until previous cmd is complete
    OSWakeAfter(1000);  //Wait a crapload for this to start
    while( !(iButtonHISRState.currentState & IB_READY))
        OSWakeAfter(100);

    // ******* End Debit Test *******
    // ******* End Debit Test *******

    // GetPSDParamData
    memcpy( iButtonBobIf.pMessage,getPSDParamData, sizeof(getPSDParamData)); 
    iButtonBobIf.bMsgLen = sizeof(getPSDParamData);
    iButtonBobIf.replyLen[ 0] = 0;

    // Load the Tx segment
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);

    // Wait until previous cmd is complete
    OSWakeAfter(1000);  //Wait a crapload for this to start
    while( !(iButtonHISRState.currentState & IB_READY))
        OSWakeAfter(100);

    // GetModuleStatus
    memcpy( iButtonBobIf.pMessage,getModuleStatusData, sizeof(getModuleStatusData)); 
    iButtonBobIf.bMsgLen = sizeof(getModuleStatusData);
    iButtonBobIf.replyLen[ 0] = 0;
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    
    // Wait until previous cmd is complete
    OSWakeAfter(1000);  //Wait a crapload for this to start
    while( !(iButtonHISRState.currentState & IB_READY))
        OSWakeAfter(100);

    // Get Firmware version
    memcpy( iButtonBobIf.pMessage,getFirmVerData, sizeof(getFirmVerData)); 
    iButtonBobIf.bMsgLen = sizeof(getFirmVerData);
    iButtonBobIf.replyLen[ 0] = 0;

    // Load the Tx segment
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);

    // Wait until previous cmd is complete
    OSWakeAfter(1000);  //Wait a crapload for this to start
    while( !(iButtonHISRState.currentState & IB_READY))
        OSWakeAfter(100);

//************GetRTC Test Loop 
//  while( 1) {
   //   for( i=0; i<5; i++) {
        // Set-up the structure 
        memcpy( iButtonBobIf.pMessage,getRTCData, sizeof(getRTCData)); 
        iButtonBobIf.bMsgLen = sizeof(getRTCData);
        iButtonBobIf.replyLen[ 0] = 0;
        iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);
    
        // Initiate the request
        iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
        fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);

    
        // Wait until previous cmd is complete
        OSWakeAfter(1000);  //Wait a crapload for this to start
        while( !(iButtonHISRState.currentState & IB_READY))
            OSWakeAfter(100);
        // Save the data
        memcpy( &rtc[ i], iButtonBobIf.pDataRecv[0]+3, 4);

        // Wait 5 second
        OSWakeAfter( 5000);

  //    }           
#endif 
//*********** end GetRTC Loop
#if 0
    // GetFreeRAM
    memcpy( iButtonBobIf.pMessage,getFreeRAM, sizeof(getFreeRAM)); 
    iButtonBobIf.bMsgLen = sizeof(getFreeRAM);
    iButtonBobIf.replyLen[ 0] = 0;
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    
    // Wait until previous cmd is complete
    OSWakeAfter(1000);  //Wait a crapload for this to start
    while( !(iButtonHISRState.currentState & IB_READY))
        OSWakeAfter(100);

    //GetPSDKeyData
    memcpy( iButtonBobIf.pMessage,getPSDKeyData, sizeof(getPSDKeyData)); 
    iButtonBobIf.bMsgLen = sizeof(getPSDKeyData);
    iButtonBobIf.replyLen[ 0] = 0;

    // Load the Tx segment
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
#endif
    while(1)
        OSWakeAfter(100);

}
#endif


//***********************************************************************/
//                                                                       */
// FUNCTION NAME:  iButtonDeviceInit                                     */
//                                                                       */
// FUNCTION DESCRIPTION: */
//                                                                       */
//                                                                       */
// AUTHOR:                                                   */
//                                                                       */
// DATE:                                                         */
//                                                                       */
// INPUT PARAMETERS:    none                                             */
//                                                                       */
// LOCAL VARIABLES:    none                                              */
//                                                                       */
// UNIT GLOBALS:                                                         */
//    name        data type    description                               */
//    ----        ---------    -----------                               */
//                                                                       */
// SYSTEM GLOBALS:    none                                               */
//                                                                       */
// RETURNED VALUES:    none                                              */
//                                                                       */
// LIMITIATIONS AND                                                      */
// RESTRICTIONS:    none                                                 */
//                                                                       */
// PSEUDOCODE:                                                           */
//                                                                       */
// Setup pointers for device drivers to use.  The pointers are pass      */
// in the ioparams structure.                                            */
// Initialize each serial port.                                          */
// Enable communications.                                                */
//                                                                       */
// REVISION HISTORY:                                                     */
//    rev #   name        date    reason                                 */
//    ----    ----        ----    ------                                 */
//                                                                       */
//***********************************************************************/
// This is only initialized from the Application.
static int iButtonDeviceInit ( IBUTTON_DEVICE *dev , 
                               UINT16 * BaseAddr,
                               unsigned char InComingId ,
                               unsigned char OutGoingId,
                               unsigned char EventId,
                               unsigned RxVectorNum,
                               unsigned TxVectorNum,
                               void (*Rxhandler)(int),
                               void (*Txhandler)(int),
                               void (*hisrHandler)(void),
                               char *hisrName,
                               BOOL fbActive,
                               UARTCONTROL *uartFuncs,
                               unsigned PwrModeDoneVectorNum,
                               void (*PwrDonehandler)(int)
                              )

{
    unsigned char i;
    int         iStatus = SUCCESSFUL;
    UINT8       majorHwVer;
    UINT8       minorHwVer;
    unsigned char bMask = 0;
	P_GPIO_REGLAYOUT pGPIOReg;

	// Copy the vectors into the device structure
    dev->pbUartBase = (unsigned char *)BaseAddr;
    dev->RxlisrHandler = Rxhandler;
    dev->TxlisrHandler = Txhandler;
    dev->CompuDoneHandler = PwrDonehandler;
    dev->hisrHandler = hisrHandler;
    dev->lwDevRxIrqVectorNum = RxVectorNum;
    dev->lwDevTxIrqVectorNum = TxVectorNum;
    dev->lwDevDoneIrqVectorNum = PwrModeDoneVectorNum;
    dev->Link.iButtonNoResponse = FALSE;
	dev->puPort = (unsigned char *)AM335X_GPIO3_BASE;

	pGPIOReg 	= (P_GPIO_REGLAYOUT) dev->puPort;
 	UINT32 temp32;

	*(volatile UINT16 *)(AM335X_CTRL_MODULE_BASE + AM335X_CTRL_PADCONF_UART0_TXD)	 = 0x0048;
	*(volatile UINT16 *)(AM335X_CTRL_MODULE_BASE + AM335X_CTRL_PADCONF_UART0_RXD)	 = 0x0070;
	*(volatile UINT16 *)(AM335X_CTRL_MODULE_BASE + AM335X_CTRL_PADCONF_USB1_DRVVBUS) = 0x004f;
	*(volatile UINT16 *)(AM335X_CTRL_MODULE_BASE + AM335X_CTRL_PADCONF_MII1_COL)	 = 0x0067;
	*(volatile UINT16 *)(AM335X_CTRL_MODULE_BASE + AM335X_CTRL_PADCONF_MII1_RX_DV)	 = 0x004f;
	temp32 = *(volatile UINT32 *)(AM335X_GPIO3_BASE + AM335X_GPIO_OE);
	*(volatile UINT32 *)(AM335X_GPIO3_BASE + AM335X_GPIO_OE) = temp32 & (~0x00002010);
	*(volatile UINT32 *)(AM335X_GPIO3_BASE + AM335X_GPIO_IRQSTATUS_CLR_0) = 0x00002011;
	*(volatile UINT32 *)(AM335X_GPIO3_BASE + AM335X_GPIO_IRQSTATUS_CLR_1) = 0x00002011;
	*(volatile UINT32 *)(AM335X_GPIO3_BASE + AM335X_GPIO_IRQSTATUS_0) = 0x00000001;
	*(volatile UINT32 *)(AM335X_GPIO3_BASE + AM335X_GPIO_IRQSTATUS_1) = 0x00000001;
	*(volatile UINT32 *)(AM335X_GPIO3_BASE + AM335X_GPIO_CLEARDATAOUT) = 0x00002010;

  
    //********************
    // Allocate data buffer(s) - for transport and link layers
    //********************
    // Tx Buffer - contains 1Wire Image put to UART
    dev->Link.iButtonTxBuff = (UINT8 *)malloc( TX_BUFF_LEN ); 
    // Rx Buffer - 1Wire Image get from UART
    dev->Link.iButtonRxDataBuff = (UINT8 *)malloc( RX_BUFF_LEN ); 
    // Rx Data post conversion
    dev->Link.iButtonRxResponseBuff = (UINT8 *)malloc( EXT_RX_BUFF_LEN ); 
  
    // Allocate the transport layer Tx & Rx - an array of each to handle multi-packet messages.
    // Tx Buffer(s)
    for( i=0; i<MAX_TX_SEGMENTS; i++) 
    {
        dev->Trans.iButtonTxSegBuff[ i] = (UINT8 *)malloc( 128 ); 
        dev->Trans.iButtonTxSegHdr[ i] = (IBUTTON_TX_HDR *)malloc( sizeof(IBUTTON_TX_HDR)); 
    }

    // Rx Buffer(s) 
    for( i=0; i<MAX_RX_SEGMENTS; i++)
    {
        dev->Trans.iButtonRxSegBuff[ i] = (unsigned char *)malloc( 256); 
    }

    //********************
    // Initialize the other variables
    //********************
    // Tx Buffer Length - Total Length      
    dev->Link.iButtonTxLength = 0;
    // Tx Remaining Length - Left to Tx         
    dev->Link.iButtonTxUnsentLength = 0;
    // Tx Current index
    dev->Link.iButtonTxCurrentIndex = 0;
    // Rx Buffer Length         
    dev->Link.iButtonRxDataIndex = 0;   
    // Rx Data offset        
    dev->Link.iButtonRxResponseOffset = 0;

    // Transport layer data structs
    dev->Trans.iButtonTxSegCount = 1;   // Until multi-packet
    dev->Trans.iButtonRxSegCount = 1;

    // Instantiate the HISR and the LISRs
    iStatus = OSCreateHISR(  ( OS_HISR *) &dev->devHisr , hisrHandler , HISRSTACKSIZE*2 , hisrName );
   
    // Register the LISR Interrupts for serial port
    if( Rxhandler )
        (void)OSSetIrqVector( RxVectorNum, Rxhandler );
        
    if( Txhandler )         
        (void)OSSetIrqVector( TxVectorNum, Txhandler );        

    if( PwrDonehandler )         
	// Register the LISR Interrupt the power done interrupt
    (void)OSSetIrqVector( PwrModeDoneVectorNum, PwrDonehandler );      

	// Register DMA Channel 4 Interrupt
//TODO: JAH Enable once we do DMA for Pullup    (void)OSSetIrqVector( DMACH4IRQVECT , iButtonDMACompIrqLISR );

    // Initialize default baud of 9600 
    iButtonInitUart( dev, BRRRESET );

    pGPIOReg->GPIO_CLEARDATAOUT = IBTN_PULLUP;   // disable/turnoff strong pull up (SPU)
    pGPIOReg->GPIO_CLEARDATAOUT = IBTN_INT_ENB;  // disable/inhibit the current sense flip-flop by clearing it

    
	//iButton Done interrupt is 62 + 2
   	ESAL_GE_MEM_WRITE32(ESAL_PR_INT_CNTRL_BASE_ADDR + 0x1F8, 0x14);  //Try 62 
	ESAL_GE_MEM_WRITE32(ESAL_PR_INT_CNTRL_BASE_ADDR + 0xA8, 0x40000000);

	pGPIOReg->GPIO_IRQSTATUS_0 = IBTN_DONE; //Clears any interrupt request
    pGPIOReg->GPIO_IRQSTATUS_1 = IBTN_DONE; //Clears any interrupt request

    pGPIOReg->GPIO_IRQSTATUS_SET_0 = IBTN_DONE; 
    pGPIOReg->GPIO_IRQSTATUS_SET_1 = IBTN_DONE;
	pGPIOReg->GPIO_RISINGDETECT |= IBTN_DONE;

    *(volatile UINT16 *)(dev->pbUartBase + AM3X_UARTFCR_OFFSET) = (AM3X_UARTFCR_RXTRIG_1 | AM3X_UARTFCR_FIFOEN | AM3X_UARTFCR_RX_FIFO_CLEAR);
    *(volatile UINT16 *)(dev->pbUartBase + AM3X_UARTIER_OFFSET) |= AM3X_UARTIER_RXHR;

    return( iStatus);
}



/****************************************/
/* iButton Manufacturing Test function  */
/****************************************/
#ifdef IBUTTON_MFG_TEST

// Return codes
#define     IBTN_MFG_INIT_FAIL          0x7000
#define     IBTN_MFG_GETVER_FAIL        0x7100
#define     IBTN_MFG_GETSTATE_FAIL      0x7200
#define     IBTN_MFG_GETSPEED_FAIL      0x7300
#define     IBTN_MFG_GETRTC_FAIL        0x7400
#define     IBTN_MFG_GETRAM_FAIL        0x7500
#define     IBTN_MFG_GETPOR_FAIL        0x7600
#define     IBTN_MFG_SELFTEST_FAIL      0x7700

/******************************************************************************/
/*                                                                            */
/*  Function Name: iButtonMfgTest                                             */
/*                                                                            */
/*  Function Description:  Test routine for functional test of I-button.      */ 
/*      This routine tests the i-button and its interface.  The I-button      */
/*      is assumed to be in Transport State, so only commands that work       */
/*      in Transport State can be tested.                                     */
/*                                                                            */
/*  Inputs:  Pointer to buffer to store test commands and results.            */
/*                                                                            */
/*  Outputs: Data returned by I-button is stored in the output array          */
/*                                                                            */
/*  Returns: 0 if all tests pass                                              */
/*           I-button error code from last failed test                        */
/*                                                                            */
/******************************************************************************/

unsigned short iButtonMfgTest(uchar *results) 
{
    unsigned short i, index=0;
    unsigned short returnStatus = 0x9000;
    BOOL fStatus;

    // Test commands
    const unsigned char getFirmVerData[] =      {0xd0,0x95,0x01,0x00,0x00};
    const unsigned char getStateData[] =        {0x80,0x03,0x00,0x00,0x00};
    const unsigned char getSpeed[] =            {0x80,0x1E,0x00,0x00,0x00};
    const unsigned char getRTCData[] =          {0xd0,0x95,0x01,0x0c,0x00};
    const unsigned char getFreeRAM[] =          {0xd0,0x95,0x01,0x01};
    const unsigned char getPORCount[] =         {0xd0,0x95,0x02,0x00};
    const unsigned char runSelfTest[] =         {0x80,0x11,0x00,0x00,0x00};

// First do the stuff the IbuttonCommTask normally does
    // Setup  iButton Device
    iButtonDeviceInit( &iButtonDevice, iButtonChannel.uartBase, 0,0,0,
                       iButtonChannel.uartRxInt, iButtonChannel.uartTxInt,
                       iButtonChannel.RxIrqHandler, iButtonChannel.TxIrqHandler,
                       iButtonIrqHISR, "iButtnHISR", true, iButtonChannel.uartControl, 
                       PSDPWRMODEDONE, iButtonCompuDoneIrqLISR );    

    // Init the CRC table
    GenCRC16Table();

	// Simulate what would be passed by Bob
    // Get Tx and Rx Data pointers and initialize
    iButtonBobIfPtr = &iButtonBobIf;
    iButtonBobIf.pMessage = (unsigned char *)malloc( 256);
    iButtonBobIf.pDataRecv[ 0] = (unsigned char *)malloc( 2048);

    // Wake up the state machine
    iButtonDevice.lwActivateEvent = EV_IB_INIT;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);

    OSWakeAfter(100);   // Delay so HISR can set current state to IB_RCV_PENDING
    i = 0;
    while(((iButtonHISRState.currentState & (unsigned long)IB_READY) != (unsigned long)IB_READY) &&
         (i < 40))      // 2 second timeout if no data received
    {
        OSWakeAfter(50);   // Wait until cmd is complete
        i++;
    }
    if( !(iButtonHISRState.currentState & IB_READY))
        return(IBTN_MFG_INIT_FAIL); // If ibutton is not ready, return an error

    // ***************** Store ROM data into results array ****************
    // The ROM data is stored in the buffer by the init routine
    *(results+index++) = (uchar) iButtonBobIf.replyLen[0];  // reply length
    memcpy(results+index, iButtonBobIf.pDataRecv[0], iButtonBobIf.replyLen[0]); // reply data
    index += iButtonBobIf.replyLen[0];
	
	// Start sending commands
    // *********************** Get Firmware version ***********************
    memcpy( iButtonBobIf.pMessage,getFirmVerData, sizeof(getFirmVerData)); 
    iButtonBobIf.bMsgLen = sizeof(getFirmVerData);
    iButtonBobIf.replyLen[ 0] = 0;
    // copy cmd into results buffer
    *(results+index++) = (uchar) iButtonBobIf.bMsgLen;                  // cmd length
    memcpy(results+index, iButtonBobIf.pMessage, iButtonBobIf.bMsgLen); // cmd data
    index += iButtonBobIf.bMsgLen;

    // Load the Tx segment
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);

    OSWakeAfter(100);   // Delay so HISR can set current state to IB_RCV_PENDING
    i = 0;
    while(((iButtonHISRState.currentState & (unsigned long)IB_READY) != (unsigned long)IB_READY) &&
         (i < 40))      // 2 second timeout if no data received
    {
        OSWakeAfter(50);   // Wait until cmd is complete
        i++;
    }
    // If ibutton is not ready or response length is zero, return an error
    if( !(iButtonHISRState.currentState & IB_READY) ||(iButtonBobIf.replyLen[0] ==0))
        return(IBTN_MFG_GETVER_FAIL | iButtonBobIfPtr->errorFlags); 

    // Save reply data in results buffer
    *(results+index++) = (uchar) iButtonBobIf.replyLen[0];  // reply length
    memcpy(results+index, iButtonBobIf.pDataRecv[0], iButtonBobIf.replyLen[0]); // reply data
    index += iButtonBobIf.replyLen[0];

    // Check for ibutton status = JIB_PSD_SUCCESS = 9000, if not, return error code
    if ((*(results+index-2) != 0x90) || (*(results+index-1) != 0x00)) 
        returnStatus = *(results+index-2)<<8 | *(results+index-1); 

    // *********************** GetState ***********************
    memcpy( iButtonBobIf.pMessage,getStateData, sizeof(getStateData)); 
    iButtonBobIf.bMsgLen = sizeof(getStateData);
    // copy cmd into results buffer
    *(results+index++) = (uchar) iButtonBobIf.bMsgLen;                  // cmd length
    memcpy(results+index, iButtonBobIf.pMessage, iButtonBobIf.bMsgLen);
    index += iButtonBobIf.bMsgLen;

    iButtonBobIf.replyLen[ 0] = 0;
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    
    OSWakeAfter(100);   // Delay so HISR can set current state to IB_RCV_PENDING
    i = 0;
    while(((iButtonHISRState.currentState & (unsigned long)IB_READY) != (unsigned long)IB_READY) &&
         (i < 40))      // 2 second timeout if no data received
    {
        OSWakeAfter(50);   // Wait until cmd is complete
        i++;
    }
    // If ibutton is not ready or response length is zero, return an error
    if( !(iButtonHISRState.currentState & IB_READY) ||(iButtonBobIf.replyLen[0] ==0))
        return(IBTN_MFG_GETSTATE_FAIL | iButtonBobIfPtr->errorFlags);   

    // Save reply data in results buffer
    *(results+index++) = (uchar) iButtonBobIf.replyLen[0];  // reply length
    memcpy(results+index, iButtonBobIf.pDataRecv[0], iButtonBobIf.replyLen[0]);
    index += iButtonBobIf.replyLen[0];

    // Check for ibutton status = JIB_PSD_SUCCESS = 9000, if not, return error code
    if ((*(results+index-2) != 0x90) || (*(results+index-1) != 0x00)) 
        returnStatus = *(results+index-2)<<8 | *(results+index-1); 

#if 0 // GetSpeed is not allowed in all ibutton states - take it out to avoid errors
    // *********************** Get Speed ***********************
    memcpy( iButtonBobIf.pMessage,getSpeed, sizeof(getSpeed)); 
    iButtonBobIf.bMsgLen = sizeof(getSpeed);
    iButtonBobIf.replyLen[ 0] = 0;
    // copy cmd into results buffer
    *(results+index++) = (uchar) iButtonBobIf.bMsgLen;                  // cmd length
    memcpy(results+index, iButtonBobIf.pMessage, iButtonBobIf.bMsgLen);
    index += iButtonBobIf.bMsgLen;

    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    
    OSWakeAfter(100);   // Delay so HISR can set current state to IB_RCV_PENDING
    i = 0;
    while(((iButtonHISRState.currentState & (unsigned long)IB_READY) != (unsigned long)IB_READY) &&
         (i < 20))      // 2 second timeout if no data received
    {
        OSWakeAfter(50);   // Wait until cmd is complete
        i++;
    }
    // If ibutton is not ready or response length is zero, return an error
    if( !(iButtonHISRState.currentState & IB_READY) ||(iButtonBobIf.replyLen[0] ==0))
        return(IBTN_MFG_GETSPEED_FAIL | iButtonBobIfPtr->errorFlags);   

    // Save reply data in results buffer
    *(results+index++) = (uchar) iButtonBobIf.replyLen[0];  // reply length
    memcpy(results+index, iButtonBobIf.pDataRecv[0], iButtonBobIf.replyLen[0]);
    index += iButtonBobIf.replyLen[0];

    // Check for ibutton status = JIB_PSD_SUCCESS = 9000, if not, return error code
    if ((*(results+index-2) != 0x90) || (*(results+index-1) != 0x00)) 
        returnStatus = *(results+index-2)<<8 | *(results+index-1); 
#endif

    // *********************** GetRTC ***********************   
    // Set-up the structure 
    memcpy( iButtonBobIf.pMessage,getRTCData, sizeof(getRTCData)); 
    iButtonBobIf.bMsgLen = sizeof(getRTCData);
    iButtonBobIf.replyLen[ 0] = 0;
    // copy cmd into results buffer
    *(results+index++) = (uchar) iButtonBobIf.bMsgLen;                  // cmd length
    memcpy(results+index, iButtonBobIf.pMessage, iButtonBobIf.bMsgLen);
    index += iButtonBobIf.bMsgLen;

    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);
    
    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);

    OSWakeAfter(100);   // Delay so HISR can set current state to IB_RCV_PENDING
    i = 0;
    while(((iButtonHISRState.currentState & (unsigned long)IB_READY) != (unsigned long)IB_READY) &&
         (i < 40))      // 2 second timeout if no data received
    {
        OSWakeAfter(50);   // Wait until cmd is complete
        i++;
    }
    // If ibutton is not ready or response length is zero, return an error
    if( !(iButtonHISRState.currentState & IB_READY) ||(iButtonBobIf.replyLen[0] ==0))
        return(IBTN_MFG_GETRTC_FAIL | iButtonBobIfPtr->errorFlags); 

    // Save reply data in results buffer
    *(results+index++) = (uchar) iButtonBobIf.replyLen[0];  // reply length
    memcpy(results+index, iButtonBobIf.pDataRecv[0], iButtonBobIf.replyLen[0]);
    index += iButtonBobIf.replyLen[0];

    // Check for ibutton status = JIB_PSD_SUCCESS = 9000, if not, return error code
    if ((*(results+index-2) != 0x90) || (*(results+index-1) != 0x00)) 
        returnStatus = *(results+index-2)<<8 | *(results+index-1); 

    // *********************** GetFreeRAM ***********************
    memcpy( iButtonBobIf.pMessage,getFreeRAM, sizeof(getFreeRAM)); 
    iButtonBobIf.bMsgLen = sizeof(getFreeRAM);
    iButtonBobIf.replyLen[ 0] = 0;
    // copy cmd into results buffer
    *(results+index++) = (uchar) iButtonBobIf.bMsgLen;                  // cmd length
    memcpy(results+index, iButtonBobIf.pMessage, iButtonBobIf.bMsgLen);
    index += iButtonBobIf.bMsgLen;

    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    
    OSWakeAfter(100);   // Delay so HISR can set current state to IB_RCV_PENDING
    i = 0;
    while(((iButtonHISRState.currentState & (unsigned long)IB_READY) != (unsigned long)IB_READY) &&
         (i < 40))      // 2 second timeout if no data received
    {
        OSWakeAfter(50);   // Wait until cmd is complete
        i++;
    }
    // If ibutton is not ready or response length is zero, return an error
    if( !(iButtonHISRState.currentState & IB_READY) ||(iButtonBobIf.replyLen[0] ==0))
        return(IBTN_MFG_GETRAM_FAIL | iButtonBobIfPtr->errorFlags); 

    // Save reply data in results buffer
    *(results+index++) = (uchar) iButtonBobIf.replyLen[0];  // reply length
    memcpy(results+index, iButtonBobIf.pDataRecv[0], iButtonBobIf.replyLen[0]);
    index += iButtonBobIf.replyLen[0];

    // Check for ibutton status = JIB_PSD_SUCCESS = 9000, if not, return error code
    if ((*(results+index-2) != 0x90) || (*(results+index-1) != 0x00)) 
        returnStatus = *(results+index-2)<<8 | *(results+index-1); 

    // *********************** PORCount ***********************
    memcpy( iButtonBobIf.pMessage,getPORCount, sizeof(getPORCount)); 
    iButtonBobIf.bMsgLen = sizeof(getPORCount);
    iButtonBobIf.replyLen[ 0] = 0;
    // copy cmd into results buffer
    *(results+index++) = (uchar) iButtonBobIf.bMsgLen;                  // cmd length
    memcpy(results+index, iButtonBobIf.pMessage, iButtonBobIf.bMsgLen);
    index += iButtonBobIf.bMsgLen;

    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    
    OSWakeAfter(100);   // Delay so HISR can set current state to IB_RCV_PENDING
    i = 0;
    while(((iButtonHISRState.currentState & (unsigned long)IB_READY) != (unsigned long)IB_READY) &&
         (i < 40))      // 2 second timeout if no data received
    {
        OSWakeAfter(50);   // Wait until cmd is complete
        i++;
    }
    // If ibutton is not ready or response length is zero, return an error
    if( !(iButtonHISRState.currentState & IB_READY) ||(iButtonBobIf.replyLen[0] ==0))
        return(IBTN_MFG_GETPOR_FAIL | iButtonBobIfPtr->errorFlags); 

    // Save reply data in results buffer
    *(results+index++) = (uchar) iButtonBobIf.replyLen[0];  // reply length
    memcpy(results+index, iButtonBobIf.pDataRecv[0], iButtonBobIf.replyLen[0]);
    index += iButtonBobIf.replyLen[0];

    // Check for ibutton status = JIB_PSD_SUCCESS = 9000, if not, return error code
    if ((*(results+index-2) != 0x90) || (*(results+index-1) != 0x00)) 
        returnStatus = *(results+index-2)<<8 | *(results+index-1); 

    // *********************** Run Self Tests ***********************
    // This test is run last so that its failure code will overwrite any others
    memcpy( iButtonBobIf.pMessage,runSelfTest, sizeof(runSelfTest)); 
    iButtonBobIf.bMsgLen = sizeof(runSelfTest);
    // copy cmd into results buffer
    *(results+index++) = (uchar) iButtonBobIf.bMsgLen;                  // cmd length
    memcpy(results+index, iButtonBobIf.pMessage, iButtonBobIf.bMsgLen);
    index += iButtonBobIf.bMsgLen;

    iButtonBobIf.replyLen[ 0] = 0;
    iButtonSegmentAPDU( &iButtonDevice, iButtonBobIfPtr);

    // Initiate the request
    iButtonDevice.lwActivateEvent = EV_IB_XMIT_DATA;
    fStatus = OSActivateHISR( ( OS_HISR *)&iButtonDevice.devHisr);
    
    OSWakeAfter(100);   // Delay so HISR can set current state to IB_RCV_PENDING
    i = 0;
    while(((iButtonHISRState.currentState & (unsigned long)IB_READY) != (unsigned long)IB_READY) &&
         (i < 200))         // 10 second timeout if no data received
    {
        OSWakeAfter(50);    // Wait until cmd is complete
        i++;
    }
 
    // If ibutton is not ready or response length is zero, return an error
    if( !(iButtonHISRState.currentState & (unsigned long)IB_READY) ||(iButtonBobIf.replyLen[0] ==0))
        return(IBTN_MFG_SELFTEST_FAIL | iButtonBobIfPtr->errorFlags);   

    // Save reply data in results buffer
    *(results+index++) = (uchar) iButtonBobIf.replyLen[0];  // reply length
    memcpy(results+index, iButtonBobIf.pDataRecv[0], iButtonBobIf.replyLen[0]); // reply data
    index += iButtonBobIf.replyLen[0];

    // Check for ibutton status = JIB_PSD_SUCCESS = 9000, if not, return error code
    if ((*(results+index-2) != 0x90) || (*(results+index-1) != 0x00)) 
        returnStatus = *(results+index-2)<<8 | *(results+index-1); 
    return(returnStatus);
}


#endif

