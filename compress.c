/* $TOG: compress.c /main/1 1998/02/04 14:33:10 kaleb $ */
/* compress.c -- compress a memory buffer
 * Copyright (C) 1995-1998 Jean-loup Gailly.
 * For conditions of distribution and use, see copyright notice in zlib.h 
 */

#include "commontypes.h"
#include "zlib.h"
#include "zutil.h"
#include "clock.h"
#include "kernel/plus_common.h"
#include "pcdisk.h"
#include "pbos.h"

extern NU_MEMORY_POOL    *pSystemMemoryCached;
BOOL fnFolderExists(CHAR *folderName);
VOID fnFilePath(CHAR* outPath, CHAR* path, CHAR* fileName);
/* ===========================================================================
     Compresses the source buffer into the destination buffer. The level
   parameter has the same meaning as in deflateInit.  sourceLen is the byte
   length of the source buffer. Upon entry, destLen is the total size of the
   destination buffer, which must be at least 0.1% larger than sourceLen plus
   12 bytes. Upon exit, destLen is the actual size of the compressed buffer.

     compress2 returns Z_OK if success, Z_MEM_ERROR if there was not enough
   memory, Z_BUF_ERROR if there was not enough room in the output buffer,
   Z_STREAM_ERROR if the level parameter is invalid.
*/
int EXPORT compress2 (dest, destLen, source, sourceLen, level)
    Bytef *dest;
    uLongf *destLen;
    const Bytef *source;
    uLong sourceLen;
    int level;
{
    z_stream stream;
    int err;

    stream.next_in = (Bytef*)source;
    stream.avail_in = (uInt)sourceLen;
#ifdef MAXSEG_64K
    /* Check for source > 64K on 16-bit machine: */
    if ((uLong)stream.avail_in != sourceLen) return Z_BUF_ERROR;
#endif
    stream.next_out = dest;
    stream.avail_out = (uInt)*destLen;
    if ((uLong)stream.avail_out != *destLen) return Z_BUF_ERROR;

    stream.zalloc = (alloc_func)0;
    stream.zfree = (free_func)0;
    stream.opaque = (voidpf)0;

    err = deflateInit(&stream, level);
    if (err != Z_OK) return err;

    err = deflate(&stream, Z_FINISH);
    if (err != Z_STREAM_END) {
        deflateEnd(&stream);
        return err == Z_OK ? Z_BUF_ERROR : err;
    }
    *destLen = stream.total_out;

    err = deflateEnd(&stream);
    return err;
}

/* ===========================================================================
 */
int EXPORT compress (dest, destLen, source, sourceLen)
    Bytef *dest;
    uLongf *destLen;
    const Bytef *source;
    uLong sourceLen;
{
    return compress2(dest, destLen, source, sourceLen, Z_DEFAULT_COMPRESSION);
}


int EXPORT compressWithHeader (source, sourceLen, fileName, fileNameLen, compFileName )
	const Bytef *source;
	uLong sourceLen;
	Bytef *fileName;
	int fileNameLen;
	Bytef *compFileName;

{
	ulg status = SUCCESS;
	DATETIME        rDateTimeStamp;
	ulg crc = 0;
	ulg compBufLen = 0;
	ulg adjCompBufLen = 0;
	uch* compBuffer = NULL;
	int hdrLen = 0;
	uch* compHeader = NULL;
	INT fd;

	Uzp_File_Hdr recHdr;
	Uzp_cdir_Rec cdRec;
	Uzp_end_cdir_Rec ecdRec;
	ulg size;
	char path[64];

	ulg signature;

	compBufLen =  sourceLen + sizeof(Uzp_File_Hdr) + sizeof(Uzp_cdir_Rec) + sizeof(Uzp_end_cdir_Rec) + 500;
	status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &compBuffer, compBufLen, NU_NO_SUSPEND);
	if(compBuffer != NULL)
	{
		status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &compHeader, 256, NU_NO_SUSPEND);
		if(compHeader != NULL)
		{
			//Create upld folder if necessary
			fnFilePath(path, "upld", NULL);

			if(!fnFolderExists(path))
			{
				status = NU_Make_Dir(path);
				if(status != 0)
				{
					dbgTrace(DBG_LVL_INFO, "compressWithHeader: Failed to create folder upld \r\n");
					fnCheckFileSysCorruption(status);
					status = -100;
				}
			}

			if(status == SUCCESS)
			{
				memset(compBuffer, 0, compBufLen);
				status = compress (compBuffer, &compBufLen, source, sourceLen);
				if(status == SUCCESS)
				{
					dbgTrace(DBG_LVL_INFO, "compressWithHeader: Success uncompress length: %d, compress length: %d \r\n", sourceLen, compBufLen);

					GetSysDateTime(&rDateTimeStamp, ADDOFFSETS, GREGORIAN);

					//Header
					signature = 0x04034b50; //0x504B0304;
					memset(&recHdr, 0, sizeof(recHdr));
					memcpy(&recHdr.signature, &signature, sizeof(signature));

					recHdr.version_needed_to_extract[0] = 0x0A;
					recHdr.version_needed_to_extract[1] = 0x00;
					recHdr.general_purpose_bit_flag[0] = 0;
					recHdr.general_purpose_bit_flag[1] = 0;
					recHdr.compression_method = 8;

					//recHdr.last_mod_dos_datetime = 0;
					recHdr.time.sec = rDateTimeStamp.bSeconds/2;
					recHdr.time.min =  rDateTimeStamp.bMinutes;
					recHdr.time.hr = rDateTimeStamp.bHour;

					recHdr.date.day = rDateTimeStamp.bDay;
					recHdr.date.mon = rDateTimeStamp.bMonth;
					recHdr.date.yr = rDateTimeStamp.bYear;

					crc = crc32(0, source, sourceLen);
					memcpy(&recHdr.crc32, &crc, sizeof(crc));


					//Remove adler at the end
					adjCompBufLen = compBufLen - 4; //no need of adler in case of header
					dbgTrace(DBG_LVL_ERROR, "compressWithHeader: compBuffer Len:%d, adjustedBufLen:%d \r\n", compBufLen, adjCompBufLen);
					//Removing zlib header length 2 bytes
					adjCompBufLen -= 2;
					dbgTrace(DBG_LVL_ERROR, "compressWithHeader: After removing zlib header, adjustedBufLen:%d \r\n", adjCompBufLen);

					recHdr.csize = adjCompBufLen;
					recHdr.ucsize = sourceLen;

					recHdr.filename_length = fileNameLen;
					recHdr.extra_field_length = 0;


					//CDR
					signature = 0x02014B50; //0x504B0102;
					memset(&cdRec, 0, sizeof(cdRec));
					memcpy(&cdRec.signature, &signature, sizeof(signature));
					cdRec.version = 0;
					cdRec.version_needed_to_extract[0] = 0x0A;
					cdRec.version_needed_to_extract[1] = 0x00;
					cdRec.general_purpose_bit_flag = 0;
					cdRec.compression_method = 8;

					//cdRec.last_mod_dos_datetime = 0;
		//			cdRec.time.sec = rDateTimeStamp.bSeconds;
		//			cdRec.time.min =  rDateTimeStamp.bMinutes;
		//			cdRec.time.hr = rDateTimeStamp.bHour;
					memcpy(&cdRec.time, &recHdr.time, sizeof(recHdr.time));

		//			cdRec.date.day = rDateTimeStamp.bDay;
		//			cdRec.date.mon = rDateTimeStamp.bMonth;
		//			cdRec.date.yr = rDateTimeStamp.bYear;
					memcpy(&cdRec.date, &recHdr.date, sizeof(recHdr.date));

					memcpy(&cdRec.crc32, &crc, sizeof(crc));

					cdRec.ucsize = recHdr.ucsize;
					cdRec.csize = recHdr.csize;

					cdRec.filename_length = fileNameLen;
					cdRec.extra_field_length = 0;
					cdRec.file_comment_length = 0;
					cdRec.disk_number_start = 0;
					cdRec.internal_file_attributes = 0;
					cdRec.external_file_attributes = 0;
					cdRec.relative_offset_local_header = 0;


					//ecdr
					signature = 0x06054B50; //0x504B0506;
					memset(&ecdRec, 0, sizeof(ecdRec));
					memcpy(&ecdRec.signature, &signature, sizeof(signature));

					ecdRec.disk_num = 0;
					ecdRec.disk_num_wcd = 0;
					ecdRec.disk_entries = 1;
					ecdRec.total_entries = 1;
					ecdRec.cd_size = sizeof(cdRec) + fileNameLen;
					ecdRec.offset_cd = 0;
					ecdRec.cmnt_len = 0;

					//Create file
					if(status == SUCCESS)
					{
						fnFilePath(path, "upld", compFileName);

						fd = NU_Open(path, (PO_TEXT|PO_RDWR|PO_CREAT|PO_TRUNC), (PS_IWRITE | PS_IREAD));
						if(fd < 0)
						{
							dbgTrace(DBG_LVL_ERROR, "compressWithHeader: Failed to open file %s \r\n", compFileName);
							status = fd;
							fnCheckFileSysCorruption(status);
						}
						else
						{
							//Write Header
							memcpy(compHeader, &recHdr, sizeof(recHdr));
							hdrLen = sizeof(recHdr);
//							size = NU_Write(fd, (void*)&recHdr, sizeof(recHdr));
//							if(size != sizeof(recHdr))
//							{
//								dbgTrace(DBG_LVL_ERROR, "compressWithHeader: Failed to write file: %s, writelength: %0x / %0x \r\n", compFileName, size, sizeof(recHdr));
//								status = size;
//							}

							//Write Name
							memcpy(compHeader + hdrLen, (void*)fileName, fileNameLen);
							hdrLen += fileNameLen;

							size = NU_Write(fd, (void*)compHeader, hdrLen);
							if(size != hdrLen)
							{
								dbgTrace(DBG_LVL_ERROR, "compressWithHeader: Failed to write file Name: %s, %s,  writelength: %0x / %0x \r\n", compFileName, fileName, size, hdrLen);
								fnCheckFileSysCorruption(size);
								status = -200;
							}
							else
							{
								//Add compressed data
								size = NU_Write(fd, (void*)compBuffer + 2, adjCompBufLen );
								if ((size < 0) || (size != adjCompBufLen))
								{
									dbgTrace(DBG_LVL_ERROR, "compressWithHeader: Failed to write Commpressed data: %s, writelength: %0x / %0x \r\n", compFileName, adjCompBufLen, compBufLen);
									fnCheckFileSysCorruption(size);
									status = -300;
								}
								else
								{

									//Update oofset in the ECDR
									if((ecdRec.offset_cd = NU_Seek(fd, 0,PSEEK_CUR)) < 0)
										fnCheckFileSysCorruption(ecdRec.offset_cd);

									memset(compHeader, 0 , 256);
									hdrLen = 0;

									//Write Cetnral Driectory Record
									memcpy(compHeader, (void*)&cdRec, sizeof(cdRec));
									hdrLen =  sizeof(cdRec);
		//							size = NU_Write(fd, (void*)&cdRec, sizeof(cdRec));
		//							if(size != sizeof(cdRec))
		//							{
		//								dbgTrace(DBG_LVL_ERROR, "compressWithHeader: Failed to write CDR: %s, writelength: %0x / %0x \r\n", compFileName, size, sizeof(cdRec));
		//								status = size;
		//							}

									//Write Name
									memcpy(compHeader + hdrLen, (void*)fileName, fileNameLen);
									hdrLen +=  fileNameLen;
		//							size = NU_Write(fd, (void*)fileName, fileNameLen);
		//							if(size != fileNameLen)
		//							{
		//								dbgTrace(DBG_LVL_ERROR, "compressWithHeader: Failed to write file Name: %s, %s, writelength: %0x / %0x \r\n", compFileName, fileName, size, fileNameLen);
		//								status = size;
		//							}

									//Write End Directory Record
									memcpy(compHeader + hdrLen, (void*)&ecdRec, sizeof(ecdRec));
									hdrLen +=   sizeof(ecdRec);
									size = NU_Write(fd, (void*)compHeader, hdrLen);
									if(size !=  hdrLen)
									{
										dbgTrace(DBG_LVL_ERROR, "compressWithHeader: Failed to write ECDR: %s, writelength: 0x%0x / 0x%0x \r\n", compFileName, size, hdrLen);
										fnCheckFileSysCorruption(size);
										status = -400;
									}
								}
							}

							fnCheckFileSysCorruption(NU_Flush(fd));
							fnCheckFileSysCorruption(NU_Close(fd));
						}
					}
					else
					{
						dbgTrace(DBG_LVL_INFO, "compressWithHeader: Compression Failed Status:%d, uncompress length: %d, compress length: %d \r\n", status, sourceLen, compBufLen);
					}
				}
			}

			NU_Deallocate_Memory(compHeader);
		}
		NU_Deallocate_Memory(compBuffer);
	}
	else
	{
		status = -500;
		dbgTrace(DBG_LVL_INFO, "compressWithHeader: Failed to allocate Status:%d ", status);
	}
	return status;
}

//int EXPORT compressWithHeaderEx (dest, destLen, source, sourceLen, fileName, fileNameLen, compFileName )
//	Bytef *dest;
//    uLongf *destLen;
//    const Bytef *source;
//	uLong sourceLen;
//	Bytef *fileName;
//	int fileNameLen;
//	Bytef *compFileName;
//{
//	ulg status = SUCCESS;
//	DATETIME        rDateTimeStamp;
//	ulg crc = 0;
//	ulg compBufLen = 0;
//	ulg adjCompBufLen = 0;
//	uch* compBuffer = NULL;
//	int hdrLen = 0;
//	uch* compHeader = NULL;
//	INT fd;
//
//	Uzp_File_Hdr recHdr;
//	Uzp_cdir_Rec cdRec;
//	Uzp_end_cdir_Rec ecdRec;
//	ulg size;
//	char path[64];
//
//	ulg signature;
//
//	compBufLen =  sourceLen + sizeof(Uzp_File_Hdr) + sizeof(Uzp_cdir_Rec) + sizeof(Uzp_end_cdir_Rec) + 500;
//
//	if(*destLen >= compBufLen)
//	{
//		status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &compBuffer, compBufLen, NU_NO_SUSPEND);
//		if(compBuffer != NULL)
//		{
//			status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &compHeader, 256, NU_NO_SUSPEND);
//			if(compHeader != NULL)
//			{
//				memset(compBuffer, 0, compBufLen);
//				status = compress (compBuffer, &compBufLen, source, sourceLen);
//				if(status == SUCCESS)
//				{
//					dbgTrace(DBG_LVL_INFO, "compressWithHeaderEx: Success uncompress length: %d, compress length: %d \r\n", sourceLen, compBufLen);
//
//					GetSysDateTime(&rDateTimeStamp, ADDOFFSETS, GREGORIAN);
//
//					//Header
//					signature = 0x04034b50; //0x504B0304;
//					memset(&recHdr, 0, sizeof(recHdr));
//					memcpy(&recHdr.signature, &signature, sizeof(signature));
//
//					recHdr.version_needed_to_extract[0] = 0x0A;
//					recHdr.version_needed_to_extract[1] = 0x00;
//					recHdr.general_purpose_bit_flag[0] = 0;
//					recHdr.general_purpose_bit_flag[1] = 0;
//					recHdr.compression_method = 8;
//
//					//recHdr.last_mod_dos_datetime = 0;
//					recHdr.time.sec = rDateTimeStamp.bSeconds/2;
//					recHdr.time.min =  rDateTimeStamp.bMinutes;
//					recHdr.time.hr = rDateTimeStamp.bHour;
//
//					recHdr.date.day = rDateTimeStamp.bDay;
//					recHdr.date.mon = rDateTimeStamp.bMonth;
//					recHdr.date.yr = rDateTimeStamp.bYear;
//
//					crc = crc32(0, source, sourceLen);
//					memcpy(&recHdr.crc32, &crc, sizeof(crc));
//
//
//					//Remove adler at the end
//					adjCompBufLen = compBufLen;// - 4; //no need of adler incase of header
//					dbgTrace(DBG_LVL_ERROR, "compressWithHeaderEx: compBuffer Len:%d, adjustedBufLen:%d \r\n", compBufLen, adjCompBufLen);
//					//Removing zlib header length 2 bytes
//					adjCompBufLen -= 2;
//					dbgTrace(DBG_LVL_ERROR, "compressWithHeaderEx: After removing zlib header, adjustedBufLen:%d \r\n", adjCompBufLen);
//
//					recHdr.csize = adjCompBufLen;
//					recHdr.ucsize = sourceLen;
//
//					recHdr.filename_length = fileNameLen;
//					recHdr.extra_field_length = 0;
//
//
//					//CDR
//					signature = 0x02014B50; //0x504B0102;
//					memset(&cdRec, 0, sizeof(cdRec));
//					memcpy(&cdRec.signature, &signature, sizeof(signature));
//					cdRec.version = 0;
//					cdRec.version_needed_to_extract[0] = 0x0A;
//					cdRec.version_needed_to_extract[1] = 0x00;
//					cdRec.general_purpose_bit_flag = 0;
//					cdRec.compression_method = 8;
//
//					//cdRec.last_mod_dos_datetime = 0;
//		//			cdRec.time.sec = rDateTimeStamp.bSeconds;
//		//			cdRec.time.min =  rDateTimeStamp.bMinutes;
//		//			cdRec.time.hr = rDateTimeStamp.bHour;
//					memcpy(&cdRec.time, &recHdr.time, sizeof(recHdr.time));
//
//		//			cdRec.date.day = rDateTimeStamp.bDay;
//		//			cdRec.date.mon = rDateTimeStamp.bMonth;
//		//			cdRec.date.yr = rDateTimeStamp.bYear;
//					memcpy(&cdRec.date, &recHdr.date, sizeof(recHdr.date));
//
//					memcpy(&cdRec.crc32, &crc, sizeof(crc));
//
//					cdRec.ucsize = recHdr.ucsize;
//					cdRec.csize = recHdr.csize;
//
//					cdRec.filename_length = fileNameLen;
//					cdRec.extra_field_length = 0;
//					cdRec.file_comment_length = 0;
//					cdRec.disk_number_start = 0;
//					cdRec.internal_file_attributes = 0;
//					cdRec.external_file_attributes = 0;
//					cdRec.relative_offset_local_header = 0;
//
//
//					//ecdr
//					signature = 0x06054B50; //0x504B0506;
//					memset(&ecdRec, 0, sizeof(ecdRec));
//					memcpy(&ecdRec.signature, &signature, sizeof(signature));
//
//					ecdRec.disk_num = 0;
//					ecdRec.disk_num_wcd = 0;
//					ecdRec.disk_entries = 1;
//					ecdRec.total_entries = 1;
//					ecdRec.cd_size = sizeof(cdRec) + fileNameLen;
//					ecdRec.offset_cd = 0;
//					ecdRec.cmnt_len = 0;
//
//					//Write Header
//					memcpy(dest, &recHdr, sizeof(recHdr));
//					hdrLen = sizeof(recHdr);
//
//					//Write Name
//					memcpy(dest + hdrLen, (void*)fileName, fileNameLen);
//					hdrLen += fileNameLen;
//
//					////////////////////////////////////////////
//					//Add compressed data
//					memcpy(dest + hdrLen, compBuffer + 2, adjCompBufLen);
//					hdrLen += adjCompBufLen;
//
//					//Update oofset in the ECDR
//					ecdRec.offset_cd = hdrLen; //NU_Seek(fd, 0,PSEEK_CUR);
//
////					memset(compHeader, 0 , 256);
////					hdrLen = 0;
//
//					//Write Cetnral Driectory Record
//					memcpy(dest + hdrLen, (void*)&cdRec, sizeof(cdRec));
//					hdrLen +=  sizeof(cdRec);
//
//					//Write Name
//					memcpy(dest + hdrLen, (void*)fileName, fileNameLen);
//					hdrLen += fileNameLen;
//
//					//Write End Directory Record
//					memcpy(dest + hdrLen, (void*)&ecdRec, sizeof(ecdRec));
//					hdrLen +=   sizeof(ecdRec);
//
//					*destLen = hdrLen;
//
//				}
//
//				NU_Deallocate_Memory(compHeader);
//			}
//			NU_Deallocate_Memory(compBuffer);
//		}
//		else
//		{
//			status = -500;
//			dbgTrace(DBG_LVL_INFO, "compressWithHeaderEx: Failed to allocate Status:%d ", status);
//		}
//	}
//	else
//	{
//		status = -600;
//		dbgTrace(DBG_LVL_INFO, "compressWithHeaderEx: FInvalid destination lengths:%d ", *destLen);
//	}
//	return status;
//}
//
//int compress_overhead(void)
//{
//	return sizeof(Uzp_File_Hdr) + sizeof(Uzp_cdir_Rec) + sizeof(Uzp_end_cdir_Rec) + 500 ;
//}
