#include <stdlib.h>
#include <ctype.h>
#include "kernel/esal_extr.h"
#include "HAL_AM335x_I2C.h"
#include "thread_control.h"
#include "reg_api.h"
#include "power_core.h"
#include "nu_drivers.h"
#include "usbf_dv_interface.h"
#include "nucleus.h"
#include "error_management.h"
#include "pbioext.h"
#include "pcdisk.h"
#include "pbos.h"
#include "global.h"
#include "sig.h"
#include "networking/nu_networking.h"
#include "connectivity/nu_usb.h"
#include "networking/http/inc/http_lite_svr.h"
#include "networking/fc_extr.h"
#include "os/networking/websocket/wsox_int.h"

#include <wolfssl/wolfcrypt/settings.h>
#include <wolfssl/internal.h>
#include <wolfssl/error-ssl.h>

#include "custdat.h"
#include "keypad.h"
#include "pbos.h"
#include "utils.h"
#include "features.h"
#include "grapext.h"
#include "settingsmgr.h"
#include "cwrapper.h"
#include "ossetup.h"
#include "hal.h"
#include "oit.h"
#include "lcd.h"
#include "cm.h"
#include "cJSON.h"
#include "oitcm.h"
#include "flashutil.h"
#include "feeder_timer.h"
#include "feeder_pwm.h"
#include "feeder_qep.h"
#include "HAL_AM335x_GPIO.h"
#include "networkmonitor.h"
#include "wsox_server.h"
#include "i2c_usb_utilities.h"
#include "memory_stats.h"
#include "debugTask.h"
#include "btldr_fram.h"
#include "counters/counters.h"
#include "counters/fram_log.h"
#include "storage/file/fs/hcc/src/media-drv/ftl/common/ftldrv.h"
#include "storage/file/fs/hcc/src/api/api_fat.h"
#include "storage/file/fs/hcc/src/fat/common/fat_common.h"
#include "ccdapi.h"
#include "exception.h"
#include "datdict.h"

static INT					globalDebugInitialized	= 0 ;
INT 						globalServerDebugSocket 	= -1 ;
INT							globalClientDebugSocket[CSD_DEBUG_MAX_CONNECTIONS] ;
INT							globalDebugConnectionCount = 0 ;
struct addr_struct 			globalDebugAddress ;
struct addr_struct			globalTransientDebugAddress ;
BOOL                        trace_wsox_ping_pong_enabled = FALSE;
BOOL                        trm_log_level = FALSE;
UINT16						globalMemBuffersUsedMax = 0;

extern INTERTASK_LOG_ENTRY 	pIntertaskMsgLog[INTERTASK_LOG_SIZE];
extern int					verbosity;
extern tCmStatus        	cmStatus;               // Current status of CM/PM
extern PB_PDM_AllVersions	globalPDMVersionCache ;
extern const UINT8 			*globalptrEmbeddedFirmwareStart ;
extern const UINT 			globalEmbeddedFirmwareSize ;
extern const unsigned int 	GPIOAddr[];
extern UINT8 globalDefaultIPv4Gateway[MAX_ADDRESS_SIZE] ;
extern UINT8 globalDefaultIPv6Gateway[MAX_ADDRESS_SIZE] ;
extern TQ_EVENT DHCP_Renew;
extern TQ_EVENT DHCP_Rebind;	// rebind
extern TQ_EVENT DHCP_New_Lease;	// new lease
extern TQ_EVENT DHCP_Release;
extern NLOG_ENTRY NLOG_Entry_List[NLOG_MAX_ENTRIES];
extern FeatureTbl           CMOSFeatureTable __attribute__ ((section ("cmos")));
extern unsigned long    	CMOSExtUpdatedToVersion __attribute__ ((section ("cmos")));
extern HTTP_SVR_DATA 		*HTTP_SVR_Data ;
extern CMOS_Signature      	CMOSSignature ;
extern bool 				heartbeat_trace_inhibit_override;
extern UINT32 				globalHBRemaining[10] ;
extern UINT32				globalTotalHB ;
extern BOOL					globalErrorWritingInProgress;

extern void RebootSystem(void);

extern VOID    TCC_Unhandled_Interrupt(INT unhandled_vector);
extern INT HTTP_Get_Listen_Port(HTTP_SVR_DATA *server_data, INT port);
extern VOID HTTP_Lite_Close_Listen_Port(HTTP_SVR_LISTEN_PORT *port);
extern STATUS HTTP_Configure_Listen_Port(INT port_index, struct addr_struct *listen_addr,NU_HTTP_SSL_STRUCT *ssl);;
extern WOLFSSL_DEBUG_LEVELS	globalWolfDebugCbLevelFilter;
extern const char *api_status_to_string_tbl[];
extern char *log_names[];
HBLogStructure globalHBLogStructure __attribute__ ((section ("fram_hblog_area"))) ;

extern char last_hb_rsp_seqnum_attempted_to_send[35];
extern char last_hb_rsp_seqnum_sent_success[35];
extern char last_hb_rsp_seqnum_sent_failed[35];
extern UINT8 globalDHCPProcessActive;
extern NAT_TRANSLATION_TABLE   NAT_Translation_Table;
extern SEMAPHORE_DEFINITION Semaphores[];

const DebugCommands globalDefaultTraceCmds[] =
					{{"d", 					"Log Snapshot Time"},
					 {"hb", 				"Heartbeat Remaining Time Stats"},
					 {"nb", 				"Network buffer status"},
					 {"usb", 				"USB Network Data Stats"},
					 {"h",   				"Network Interfaces"},
					 {"nlog",  				"Dump Netlog"},
					 {"n",   				"Network Connection state dump"},
					 {"nis",   				"Network Information Structure"},
					 {"skt google.com",   	"Connectivity check including dns to google.com"},
					 {"bi",   				"Board Info Structure dump"},
					 {"mp",   				"Memory Pool Dump"},
					 {"tcpnat",             "Dump NAT table"},
//					 {"mp cJSONmp",			"Memory Pool Dump for JSON messages"},
					 {"t",   				"Task Stats list"},
					 {"th",   				"HISR Task Stats list"},
					 {"v",   				"Version Information"},
					 {"wstats",   			"Websocket Stats"},
					 {0, 0}};



//extern PB_I2C					globalPBI2C ;
// op modes defined in enum in cm.h
const char *kopModeDescriptions[] =
{
	"NORMAL",
	"SEALONLY",
	"DIAG",
	"SLEEPING",
	"SOFT_PWROFF",
	"DISABLE",
	"SIMULATION"
};


// PM status descriptions
const char *kStatusDescriptions[] =
{
		" OK ",
		"WARNING",
		"PAPER_ERR",
		"PHEAD_ERR",
		"INK_OR_TANK_ERR",
		"MSG_ERR",
		"FATAL_ERR",
		"TAPE_ERR"
};
const size_t kStatusDescriptionsSize = sizeof(kStatusDescriptions) ;

const UINT8 kMinBJCTRLRespID = BJRESP_REQAUTOSTART;
const UINT8 kMaxBJCTRLRespID = BJRESP_MAINTCHECK;


const char*kBJCTRLRespIDStrings[] =
{
		"REQAUTOSTART",			// 0x80
		"RUN",
		"PRINT",
		"REQMAINTENANCE",
		"MAINTENANCE",
		"SETMODE",				// 0x85
		"INIT",
		"DIAGNOSTIC",
		"REPLACEMENT",
		"SENSORDATA",
		"TOPCOVER",				// 0x8A
		"CLEARPRINTERERROR",
		"PRINTERSHUTDOWN",
		"PRINTHEADDATA",
		"INKTANKDATA",
		"PMSWVER",
		"UNSOLICITED",			// 0x90
		"EJECT",
		"MOTIONSENSOR",
		"JAMLEVER",
		"RESET",
		"NVMDATA",				// 0x95
		"NVMUPDATE",
		"INKTANKREPLACEMENT",
		"STOPRUN",
		"RUNTAPE",
		"STOPRUNTAPE",			// 0x9A
		"PRINTREADY",
		"MAINTCHECK",
};
const size_t kBJCTRLRespIDStringsSize = sizeof(kBJCTRLRespIDStrings) ;


/***********************************************************************
 * debugErrorNotification
 *
 * Use this as the user error handler in nucleus' error handling. Just
 * to let someone know that something has stopped working.
 * It designed to handle a modified system package error notification, if
 * the error type is between -5000 and -6000 it is assumed to be an error type
 * otherwise it is assumed to be the address of an error control block containing
 * details about the error that has occurred.
 * If it is an error control block we try restarting the thread that generated the error.
 **********************************************************************/
STATUS debugSystemErrorNotification(INT i)
{
	// careful what goes in here, this may be called from an HISR
	// if the number passed is within our memory pool area we will assume the data passed is an
	// error control block otherwise handle it like a normal error type.
	if(((UINT32)i <  (UINT32)ESAL_DP_MEM_Region_Data[8].virtual_start_addr) ||
	   ((UINT32)i >= (UINT32)ESAL_DP_MEM_Region_Data[9].virtual_start_addr))
	{
		char strTmp[70];
		snprintf(strTmp, 70, "Entering Nucleus System ERROR Loop - error %d\r\n", i) ;
		return debugQueueString(strTmp) ;
	}
	// if here the passed in parameter should be a pointer to an error control block...
	ERC_CB *ercb = (ERC_CB *)i;
	NU_TASK *pnuTask = ercb->user_error.thread;

	// dump information about the error to the syslog...
	char strTmp[100];
	snprintf(strTmp, 100, "SYSERROR Restarting System - Error in Thread %s: Err %d",
			pnuTask->tc_name, ercb->user_error.error_type);
	fnDumpStringToSystemLog(strTmp) ;
	snprintf(strTmp, 100, "SYSERROR - err adds 0x%08X",
			(UINT32)ercb->user_error.read_write_address);
	fnDumpStringToSystemLog(strTmp) ;
	snprintf(strTmp, 100, "SYSERROR - ret adds 0x%08X",
			(UINT32)ercb->user_error.return_address);
	fnDumpStringToSystemLog(strTmp) ;
	
	RebootSystem();

	return SUCCESS;
}

/***********************************************************************
*
*     FUNCTION
*
*         spawnDebugTerminalAcceptor
*
*     DESCRIPTION
*
*         Create the acceptor needed by the debug terminal to connect via telnet
*
*     INPUTS
*			none
*
*     OUTPUTS
*
*         none
*
***********************************************************************/
STATUS spawnDebugTerminalAcceptor()
{
	STATUS status = NU_SUCCESS;
    char  semstat;

    semstat = OSAcquireSemaphore (DEBUG_PORT_SEM_ID, OS_SUSPEND);
    if (semstat != SUCCESSFUL)
    {
        dbgTrace(DBG_LVL_ERROR,"Error: Failed to acquire Debug port semaphore in spawnDebugTerminalAcceptor: %d\n", semstat);
        return NU_RETURN_ERROR;
    }

    if(globalServerDebugSocket >= 0)
	{
		NU_Close_Socket(globalServerDebugSocket);
		globalServerDebugSocket = -1 ;
	}

#if (INCLUDE_IPV6 == 1)
	globalServerDebugSocket = NU_Socket(NU_FAMILY_IP6, NU_TYPE_STREAM, NU_NONE ) ;
#else
	globalServerDebugSocket = NU_Socket(NU_FAMILY_IP, NU_TYPE_STREAM, NU_NONE ) ;
#endif

	if(globalServerDebugSocket >= 0)
	{
		memset(&globalDebugAddress, 0, sizeof(struct addr_struct)) ;
#if (INCLUDE_IPV6 == 1)
		globalTransientDebugAddress.family 	= NU_FAMILY_IP6; // NU_FAMILY_IP;
#else
		globalTransientDebugAddress.family 	= NU_FAMILY_IP;
#endif
		globalTransientDebugAddress.port		= CSD_DEBUG_PORT ;
		if (CMOSglobalNetworkInformationStructure.eth0WsoxFlag == 0)
        {
            // eth0 not allowed --> bind to 192.168.10.110
            stringToIP4Array(globalTransientDebugAddress.id.is_ip_addrs, "192.168.10.110");
        }
		// else leave ip at 0, look on all interfaces...

		globalDebugAddress = globalTransientDebugAddress;

		NU_Fcntl(globalServerDebugSocket, NU_SETFLAG, NU_NO_BLOCK);

		status = NU_Bind(globalServerDebugSocket, &globalDebugAddress, 0) ;
		status = NU_Listen(globalServerDebugSocket, 1) ;
		semstat = OSReleaseSemaphore(DEBUG_PORT_SEM_ID);
	    if (semstat != SUCCESSFUL)
	    {
	        dbgTrace(DBG_LVL_ERROR,"Error: Failed to release Debug port semaphore in spawnDebugTerminalAcceptor: %d\n", semstat);
	    }
	}
	return status;
}

/***********************************************************************
*
*     FUNCTION
*
*         debugTask
*         associated listener task = DBGLSTNR
*
*     DESCRIPTION
*
*         main Nucleus task for managing Dubug tracing socket connections.
*         Main loop sits on the acceptor socket looking for new connections
*
*     INPUTS
*
*         argc - not used
*         argv - not used.
*
*     OUTPUTS
*
*         none
*
***********************************************************************/
void debugTask(unsigned long argc, void *argv)
{
	int 			iPass 	= 0 ;
	int 			handled = FALSE ;
	STATUS 			status 	= 0 ;
	int 			tmpSocket;
    NU_MEMORY_POOL 	*pnuMemPool = (NU_MEMORY_POOL *)argc;
    UNUSED_PARAMETER(pnuMemPool);
	UNUSED_PARAMETER(status);
	BOOL			bErrorLogged = FALSE ;
    char            semstat;


	struct addr_struct		tmpSocketStruct ;
	//
	if(!globalDebugInitialized)
		debugInitialize();
	NU_Register_System_Error_Handler(debugSystemErrorNotification) ;

	// create the listener socket for debugterminal operation...
	spawnDebugTerminalAcceptor();

	OSResumeTask(DEBUGLSTNR);
	fnSysLogTaskStart("debugTask", DEBUGTASK);
	while(1)
	{
		handled = FALSE;
		tmpSocket = NU_Accept(globalServerDebugSocket, &tmpSocketStruct, 0) ;
		if(tmpSocket >= 0)
		{
			// reset our acceptor error logging flag, this is an attempt to stop the logs filling with dup msgs.
			bErrorLogged = FALSE ;
			semstat = OSAcquireSemaphore (DEBUG_PORT_SEM_ID, OS_SUSPEND);
		    if (semstat != SUCCESSFUL)
		    {
		        dbgTrace(DBG_LVL_ERROR,"Error: Failed to acquire Debug port semaphore in debugTask, opening socket anyway: %d\n", semstat);
		    }
			for(iPass = 0 ; (iPass < CSD_DEBUG_MAX_CONNECTIONS) && (handled == 0) ; iPass++)
			{
				if(globalClientDebugSocket[iPass] == -1)
				{
					globalClientDebugSocket[iPass] = tmpSocket ;
					handled = TRUE ;
					globalDebugConnectionCount++;
					NU_Fcntl(tmpSocket, NU_SETFLAG, NU_NO_BLOCK);
				}
			}
			semstat = OSReleaseSemaphore(DEBUG_PORT_SEM_ID);
		    if (semstat != SUCCESSFUL)
		    {
		        dbgTrace(DBG_LVL_ERROR,"Error: Failed to release Debug port semaphore in debugTask, continuing anyway: %d\n", semstat);
		    }

			if(!handled)
			{
				NU_Close_Socket(tmpSocket) ;
			}
			else
			{
				char 			strInUse[50];
				debugDisplayOnly("CSD DEBUG Socket %2u connection successful - %u of %u connections in use [%s] - type ? for help.\r\n", (UINT)tmpSocket, debugConnectionsInUse(strInUse, 50), CSD_DEBUG_MAX_CONNECTIONS, strInUse) ;
			}
		}
		else
		{
			// NU_Accept error
			status = NU_RETURN_ERROR;
			NU_Sleep(1);
			if(memcmp(&globalDebugAddress, &globalTransientDebugAddress, sizeof(struct addr_struct)) != 0)
			{
				debugDisplayOnly("Detected request to alter debug interface settings\r\n") ;
				//
				INT iTmpSkt = -1;
				iTmpSkt = NU_Socket(NU_FAMILY_IP, NU_TYPE_STREAM, NU_NONE ) ;
				if(iTmpSkt >= 0)
				{
					NU_Fcntl(iTmpSkt, NU_SETFLAG, NU_NO_BLOCK);
					NU_Close_Socket(globalServerDebugSocket);
					killSocketsWithMatchingPorts(globalTransientDebugAddress.port);

					globalServerDebugSocket = iTmpSkt ;
					status = NU_Bind(globalServerDebugSocket, &globalTransientDebugAddress, 0) ;
					status = NU_Listen(globalServerDebugSocket, 1) ;
					if(status == NU_SUCCESS)
					{
						debugDisplayOnly("Successfully transitioned debug interface to new address\r\n");
					}
					else
					{
						debugDisplayOnly("ERROR Unable to transition debug interface to requested address\r\n");
					}

				}

				//
				globalDebugAddress = globalTransientDebugAddress;
			}
			else if(tmpSocket != NU_WOULD_BLOCK)
			{
				// we have an unexpected error on debug channel acceptor....
				if(!bErrorLogged)
				{
					dbgTrace(DBG_LVL_INFO, "Debug channel accept returned error %d\r\n", tmpSocket);
					bErrorLogged = TRUE ;
				}
			}
		}
	}
}

NU_QUEUE 		globalDebugOutputQueue;
#define			DEBUG_QUEUE_MSGSIZE		DEBUG_WORKBUFFER_SIZE
#define			DEBUG_QUEUE_MSGCOUNT	2

/***********************************************************************
*
*     FUNCTION
*
*         debugTaskListener
*
*     DESCRIPTION
*
*         associated listener task = DBGLSTNR
*         designed to listen to any sockets opened by the debug task above.
*         It cleans up the input data an calls a command handler function.
*
*     INPUTS
*
*         argc - not used
*         argv - not used.
*
*     OUTPUTS
*
*         none
*
***********************************************************************/
VOID debugTaskListener(unsigned long argc, void *argv)
{
	int 			iPass 	= 0 ;
	STATUS 			status 	= 0 ;
    char			readBuf[DEBUG_READBUFFER_SIZE];
    char			workBuf[DEBUG_WORKBUFFER_SIZE];
//    char			*ptrIn = readBuf ;
//    size_t			szInCount = 0 ;
    NU_MEMORY_POOL 	*pnuMemPool = (NU_MEMORY_POOL *)argc;
    char            semstat;

    fnSysLogTaskStart("debugTaskListener", DEBUGLSTNR);

    VOID *pointer;
    status = NU_Allocate_Memory(pnuMemPool, &pointer, DEBUG_QUEUE_MSGSIZE * DEBUG_QUEUE_MSGCOUNT, NU_NO_SUSPEND);
	status |= NU_Create_Queue(&globalDebugOutputQueue,
						"DBGLSTQ",
						pointer,
						(DEBUG_QUEUE_MSGSIZE * DEBUG_QUEUE_MSGCOUNT)/sizeof(UNSIGNED),
						NU_VARIABLE_SIZE ,
						DEBUG_QUEUE_MSGSIZE/sizeof(UNSIGNED),
						NU_FIFO);

	*readBuf = *workBuf = 0 ;

	while(1)
	{
		int		weHaveData 	= FALSE ;

		if(globalDebugConnectionCount > 0)
		{
			semstat = OSAcquireSemaphore (DEBUG_PORT_SEM_ID, OS_SUSPEND);
		    if (semstat != SUCCESSFUL)
		    {
		        dbgTrace(DBG_LVL_ERROR,"Error: Failed to acquire Debug port semaphore in debugTaskListener, reading socket anyway: %d\n", semstat);
		    }
			globalMemBuffersUsedMax = MAX(globalMemBuffersUsedMax, MEM_Buffers_Used) ;

			for(iPass=0 ; (iPass < CSD_DEBUG_MAX_CONNECTIONS) && !weHaveData ; iPass++)
			{
				if(globalClientDebugSocket[iPass] == -1)
				{
					NU_Sleep(1) ;
				}
				else
				{
					status = NU_Recv(globalClientDebugSocket[iPass], readBuf, DEBUG_READBUFFER_SIZE-1, NU_NULL);
					if(status >= 0)
					{
						if(status > DEBUG_READBUFFER_SIZE)
							status = DEBUG_READBUFFER_SIZE;

						// null terminate the input
						readBuf[status] = 0;
						strncat(workBuf, readBuf, DEBUG_WORKBUFFER_SIZE) ;
						// remove leading CR/LF
						char *p1;
						char *p2;
						while((workBuf[0] == DEBUG_LF) || (workBuf[0] == DEBUG_CR))
						{
							p1 = workBuf;
							p2 = workBuf+1;
							for( ; *p1; p1++, p2++)
								*p1 = *p2;

						}
						// handle the BS character
						p1 = p2 = workBuf;
						for( ; *p2 ; p1++)
						{
							if(*p1 == DEBUG_BS)
							{
								p2--;
							}
							else
							{
								*p2 = *p1;
								p2++;
							}
						}
						// terminating CR/LF means stop input and process data
						if((workBuf[strlen(workBuf)-1] == DEBUG_LF) || (workBuf[strlen(workBuf)-1] == DEBUG_LF))
						{
							weHaveData = TRUE;
							// remove any trailing CR/LF
							for(p1 = workBuf ; *p1 ; p1++)
							{
								if((*p1 == DEBUG_LF) || (*p1 == DEBUG_CR))
								{
									*p1 = 0 ;
									break;
								}
							}
						}
					}
					else if(status < 0)
					{
						// any error other than would block terminates the socket...
						if(status != NU_WOULD_BLOCK)
						{
							NU_Close_Socket(globalClientDebugSocket[iPass]);
							globalClientDebugSocket[iPass] = -1;
							if(globalDebugConnectionCount > 0) globalDebugConnectionCount--;
							break ;
						}
						else
						{
							NU_Sleep(1);
						}
					}
				}
			}
			semstat = OSReleaseSemaphore(DEBUG_PORT_SEM_ID);
		    if (semstat != SUCCESSFUL)
		    {
		        dbgTrace(DBG_LVL_ERROR,"Error: Failed to release Debug port semaphore in debugTaskListener, continuing anyway: %d\n", semstat);
		    }
		}
		else
		{

			*workBuf = 0;
			NU_Sleep(1) ;
		}

		// delay handling the data until we have cleared the semaphore....
		if(weHaveData)
		{
			// not sure why but this print seems to un-clog/speed-up  the debug terminal execution...
			debugDisplayOnly("\r\n");
			debugHandleInput(pnuMemPool, workBuf);
			*workBuf = 0 ;
		}
		else
		{
			// poll our asynch input queue for anything to display.
			debugCheckQueue();
		}
	}
	dbgTrace(DBG_LVL_INFO, "HELP should not be here\r\n");
}

/***********************************************************************
*
*     FUNCTION
*
*         debugCheckQueue
*
*     DESCRIPTION
*
*		Check the input queue for activity and if present, display it.
*		this queue allows us to handle printing from HISR routines.
*
*     INPUTS
*
*
*     OUTPUTS
*
*         none
*
***********************************************************************/
void debugCheckQueue()
{
	UNSIGNED 		msgLen;
    char			readBuf[DEBUG_QUEUE_MSGSIZE];

	STATUS status =  NU_Receive_From_Queue(&globalDebugOutputQueue, readBuf, DEBUG_QUEUE_MSGSIZE/sizeof(UNSIGNED), (UNSIGNED *)&msgLen, NU_NO_SUSPEND);
	if((status == NU_SUCCESS) && (msgLen > 0))
	{
//		debugSendString(readBuf);
		dbgTrace(DBG_LVL_INFO, readBuf);
	}
}

/***********************************************************************
*
*     FUNCTION
*
*         debugQueueString
*
*     DESCRIPTION
*		Place the supplied null terminated string on a queue for asynch display. This allows printing of info
*		from inside HISR routines.
*
*     INPUTS
*
*		str		standard null terminated string, max length allowed is DEBUG_QUEUE_MSGSIZE
*				(500 bytes at time of writing).
*
*     OUTPUTS
*
*         STATUS
*
***********************************************************************/
STATUS debugQueueString(char *str)
{
	STATUS status = -1;

	// since nucleus uses a count of the UNSIGNED size we add one to ensure the null termination makes it through the system.
	size_t sz = strlen(str) + sizeof(unsigned);
	if(sz < DEBUG_QUEUE_MSGSIZE)
	{
		status = NU_Send_To_Queue(&globalDebugOutputQueue, str, sz/sizeof(unsigned), NU_NO_SUSPEND);
	}

	return status;
}

static char bufPreviousLine[DEBUG_READBUFFER_SIZE+1];
/***********************************************************************
*
*     FUNCTION
*
*         debugHandleInput
*
*     DESCRIPTION
*
*         process any input from the debug terminals
*
*     INPUTS
*
*         *buf		input data buffer.
*
*     OUTPUTS
*
*         none
*
***********************************************************************/
VOID debugHandleInput(NU_MEMORY_POOL 	*pnuMemPool, char *buf)
{
	BOOL bHandled = FALSE ;

	// repeat last command if user types ! or '
	if((*buf == MBCMD_REPEATLASTCMD) || (*buf == MBCMD_REPEATLASTCMD2))
	{
		strncpy(buf, bufPreviousLine, DEBUG_READBUFFER_SIZE);
		debugDisplayOnly("Repeating: %s", buf) ;
	}
	else
		strncpy(bufPreviousLine, buf, DEBUG_READBUFFER_SIZE);

	// try single char if appropriate, if not handled by that then move on to multi char.
	if(strlen(buf) == 1)
		bHandled = debugHandleInputSingleChar(pnuMemPool, *buf);

	if(!bHandled)
		bHandled = debugHandleInputMultiChar(pnuMemPool, buf);

	if(!bHandled)
		debugDisplayOnly("\r\nUnknown command [%s]\r\n", buf);
}

/***********************************************************************
 * stackCrasher
 *
************************************************************************/
UINT globalStackIterator = 0 ;
void stackCrasher(NU_TASK *preThr, void *prevSP)
{
	NU_TASK *pCurThr		= NU_Current_Task_Pointer();
	void *current_sp = ESAL_GE_RTE_SP_READ();
	NU_Sleep(1);
	globalStackIterator++;
	stackCrasher(pCurThr, current_sp);
}

extern UINT8 cmdHardReset[3];
extern UINT8 cmdVCONNOn[2];
extern UINT8 cmdVCONNOff[2];
extern UINT8 cmdCtrlPortHardReset[2];
extern UINT8 cmdCtrlPortSoftReset[2];
extern UINT8 cmdPortDisable[2];
extern UINT8 cmdReadSinkPDO0[2];

// DEBUG RD - structure to try and monitor activity on USB network lines
extern struct _USBNetworkStats
{
	UINT32 NDIScalls;
	UINT32 NDISbytes;
	UINT32 NDISErrors;
	UINT32 CDCcalls;
	UINT32 CDCbytes;
	UINT32 CDCErrors;
} USBNETWORKStats[2];
// DEBUG RD - end of stats monitoringextern UINT8 cmdTriggerDataRoleSwap[2];
UINT16 globalHspClkDiv = 0 ;
UINT16 globalClkDiv = 0 ;

static uint32_t m_w = 521288629;
static uint32_t m_z = 362436069;

static void set_seed(uint32_t u, uint32_t v)
{
    if (u != 0) m_w = u;
    if (v != 0) m_z = v;
}

static void reseed_from_timer()
{
    uint32_t t = get_32kHz_tick_count();
    set_seed((t >> 16), t);
}

static uint32_t get_rand()
{
    m_z = 36969 * (m_z & 65535) + (m_z >> 16);
    m_w = 18000 * (m_w & 65535) + (m_w >> 16);
    return (m_z << 16) + m_w;
}

static uint32_t get_rand_range(uint32_t M, uint32_t N)
{
    return M + get_rand() / (UINT32_MAX / (N - M + 1) +1);
}

/* ---------------------------------------------------------------------------------------------[get_cmd_parameter]-- */
char *get_cmd_parameter(char **pPos)
{
    char *p_par;

    p_par = skipSpaces(*pPos) ;
    *pPos  = skipNonspaces(p_par) ;
    if (**pPos != 0)
    {
        **pPos = 0;   /* null terminate the parameter */
        (*pPos)++;
    }
    return p_par;
}

/***********************************************************************
*
*     FUNCTION
*
*         debugHandleInputMultiChar
*
*     DESCRIPTION
*
*         process string commands
*
*     INPUTS
*
*		 pnuMemPool	- Nucleus memory pool
*        str		- the command string to interpret
*
*     OUTPUTS
*
*         none
*
***********************************************************************/
extern const char image_len_adjust[];

//TODO - With new Nucleus release, confirm access of internal structures is still valid and check whether it can be replaced with API call
extern  WSOX_CONTEXT_LIST   WSOX_Connection_List;
extern  WSOX_CONTEXT_LIST   WSOX_Listener_List;

UINT32  globalUSBFAddress=0;
static UINT iFeederState = 0 ;
BOOL debugHandleInputMultiChar(NU_MEMORY_POOL 	*pnuMemPool, char *str)
{
	UNUSED_PARAMETER(pnuMemPool);

	BOOL bHandled = TRUE ;
	char 	strCommand[MAX_COMMAND_SIZE+1];
	char 	*pPos 		= getString(str, strCommand, MAX_COMMAND_SIZE) ;
	size_t 	szCmdLen 	= strlen(strCommand) ;

	if((szCmdLen == strlen(MBCMD_SYSLOG)) && strncmp(strCommand, MBCMD_SYSLOG, strlen(MBCMD_SYSLOG)) == 0)
	{
		handleSyslogDump(strCommand, pPos) ;
	}
	else if((szCmdLen == strlen(MBCMD_WSVERBOSITY)) && strncmp(strCommand, MBCMD_WSVERBOSITY, strlen(MBCMD_WSVERBOSITY)) == 0)
	{
		char result[50];
		verbosity = atoi(pPos) ;
		snprintf(result, 50, "Websocket verbosity now set to %d\r\n", verbosity);
		debugSendString(result) ;
	}
	else if((szCmdLen == strlen(MBCMD_TASKSTATS)) && strncmp(strCommand, MBCMD_TASKSTATS, strlen(MBCMD_TASKSTATS)) == 0)
	{
//		printTaskStats(pnuMemPool, pPos) ;
		printTaskStats(pPos) ;
	}
	else if((szCmdLen == strlen(MBCMD_HISRSTATS)) && strncmp(strCommand, MBCMD_HISRSTATS, strlen(MBCMD_HISRSTATS)) == 0)
	{
//		printHISRStats(pnuMemPool, pPos) ;
		printHISRStats(pPos) ;
	}
	else if((szCmdLen == strlen(MBCMD_TASKPRIORITY)) && strncmp(strCommand, MBCMD_TASKPRIORITY, strlen(MBCMD_TASKPRIORITY)) == 0)
	{
		// SetTaskPriority, format  tp <taskname> <priority>
		char *pName = pPos;
		pPos = skipNonspaces(pPos) ;
		int namelen = pPos - pName;
		pPos = skipSpaces(pPos) ;
		INT iPriority = atoi(pPos) ;
		*(pName+namelen) = 0; /* make sure it is null terminated */
		setTaskPriority(pnuMemPool, pName, iPriority) ;
	}

	else if((szCmdLen == strlen(MBCMD_DUMPMACADDRESSES)) && strncmp(strCommand, MBCMD_DUMPMACADDRESSES, strlen(MBCMD_DUMPMACADDRESSES)) == 0)
	{
		updateNetworkMACAddsFromSITARA(FALSE) ; // do not update the hardware.
	}
	else if((szCmdLen == strlen(MBCMD_DUMPMEMORYPOOLS)) && strncmp(strCommand, MBCMD_DUMPMEMORYPOOLS, strlen(MBCMD_DUMPMEMORYPOOLS)) == 0)
	{
		printMemoryPoolStats(pnuMemPool, pPos) ;
	}
	else if((szCmdLen == strlen(MBCMD_SOCKETCONNECT)) && strncmp(strCommand, MBCMD_SOCKETCONNECT, strlen(MBCMD_SOCKETCONNECT)) == 0)
	{
		// socket connect test
		testSocketConnect(pPos, NULL) ;
	}
	else if((szCmdLen == strlen(MBCMD_SMGR_SETTINGS)) && strncmp(strCommand, MBCMD_SMGR_SETTINGS, strlen(MBCMD_SMGR_SETTINGS)) == 0)
    {
        smgr_dump_settings();
    }
	else if((szCmdLen == strlen(MBCMD_BOARD_INFO_STRUCTURE)) && strncmp(strCommand, MBCMD_BOARD_INFO_STRUCTURE, strlen(MBCMD_BOARD_INFO_STRUCTURE)) == 0)
    {
        displayBoardInfoStructure();
    }
	else if((szCmdLen == strlen(MBCMD_CYCLE_BOOTMODE)) && strncmp(strCommand, MBCMD_CYCLE_BOOTMODE, strlen(MBCMD_CYCLE_BOOTMODE)) == 0)
    {
        debugDisplayOnly("\nChanged boot mode from %s ", boot_mode_to_str(BoardInfo.BootMode));
        switch (BoardInfo.BootMode)
        {
            case START_PB_APP:         BoardInfo.BootMode = START_CANON_TEST_APP;  break;
            case START_CANON_TEST_APP: BoardInfo.BootMode = USB_DOWNLOAD_MODE;     break;
            case USB_DOWNLOAD_MODE:    BoardInfo.BootMode = SERIAL_DOWNLOAD_MODE;  break;
            case SERIAL_DOWNLOAD_MODE: BoardInfo.BootMode = 0;                     break;
            default:                   BoardInfo.BootMode = START_PB_APP;          break;
        }
        debugDisplayOnly("to %s\n", boot_mode_to_str(BoardInfo.BootMode));
    }
 
	else if((szCmdLen == strlen(MBCMD_DUMPNIS)) && strncmp(strCommand, MBCMD_DUMPNIS, strlen(MBCMD_DUMPNIS)) == 0)
    {
		debugDisplayNetworkInformationStructure(&CMOSglobalNetworkInformationStructure);
    }
	else if((szCmdLen == strlen(MBCMD_SOCKETKILL)) && strncmp(strCommand, MBCMD_SOCKETKILL, strlen(MBCMD_SOCKETKILL)) == 0)
	{
		INT iSktNum = atoi(pPos) ;
		STATUS status ;
		if(iSktNum >= 0)
		{
			status = NU_Close_Socket(iSktNum);
			if(status == NU_SUCCESS)
				debugDisplayOnly("Successfully closed socket %d\r\n", iSktNum);
			else
				debugDisplayOnly("ERROR Unable to close socket %d [%d]\r\n", iSktNum, status);
		}
		else
		{
			debugDisplayOnly("ERROR Invalid socket ID %d\r\n", iSktNum) ;
		}
	}
	else if((szCmdLen == strlen(MBCMD_DHCP)) && strncmp(strCommand, MBCMD_DHCP, strlen(MBCMD_DHCP)) == 0)
	{
		INT val = - 1;
		if(strncmp(pPos, "ren", 3) == 0) val = DHCP_Renew;	// renew
		if(strncmp(pPos, "reb", 3) == 0) val = DHCP_Rebind;	// rebind
		if(strncmp(pPos, "new", 3) == 0) val = DHCP_New_Lease;	// new lease
		if(strncmp(pPos, "rel", 3) == 0) val = DHCP_Release;	// release

		if(val > 0)
		{
			if (TQ_Timerset(val, 3, 5 /* * SCK_Ticks_Per_Second */, 0) != NU_SUCCESS)
			{
				debugDisplayOnly("ERROR Unable to initiate a dhcp %s [%d]\r\n", pPos, val);
			}
			else
			{
				debugDisplayOnly("DHCP %s [%d] will begin in 5 seconds\r\n", pPos, val) ;
			}
		}
	}
	else if((szCmdLen == strlen(MBCMD_NLOGDUMP)) && strncmp(strCommand, MBCMD_NLOGDUMP, strlen(MBCMD_NLOGDUMP)) == 0)
	{
		// dump the nlog array
		int iPass ;
		char str[NLOG_MAX_BUFFER_SIZE];

	    int iTimeLen = snprintf(str,NLOG_MAX_BUFFER_SIZE, "\r\nStart Time  %02d/%02d/%02d  %02d:%02d:%02d\r\n",
	    		globalNetlogStartTime.bMonth, globalNetlogStartTime.bDay, globalNetlogStartTime.bYear,
				globalNetlogStartTime.bHour, globalNetlogStartTime.bMinutes, globalNetlogStartTime.bSeconds);
	    if(iTimeLen > 0)
	    	debugDisplayOnly(str);

		for(iPass = 0 ; iPass < NLOG_MAX_ENTRIES ; iPass++)
		{
			if(strlen(NLOG_Entry_List[iPass].log_msg) > 0)
			{
				char *ptr;
				int iPass1 ;
				strcpy(str, NLOG_Entry_List[iPass].log_msg);
				str[NLOG_MAX_BUFFER_SIZE] = 0 ;
				for(iPass1 = 0, ptr = str; (iPass1 < NLOG_MAX_BUFFER_SIZE) && *ptr ; iPass1++, ptr++)
				{
					if(*ptr == DEBUG_CR) *ptr = '\t' ;
					if(*ptr == DEBUG_LF) *ptr = ' ' ;
				}
				debugDisplayOnly("%2d %s\r\n", iPass, str);
			}
		}
	}
	else if((szCmdLen == strlen(FORCE_ERROR)) && strncmp(strCommand, FORCE_ERROR, strlen(FORCE_ERROR)) == 0)
	{
		if(strncmp(pPos, "mem", 3) == 0)
		{
			debugDisplayOnly("Forcing memory error\r\n");
			UINT32 *ptr = (UINT32 *)0x60000000;
			*ptr = 10 ;
		}
		else
		{
			debugDisplayOnly("Let's crash the stack!\r\n");
			stackCrasher(NULL, NULL) ;
		}
	}
	else if((szCmdLen == strlen(FEEDER_SETUP)) && strncmp(strCommand, FEEDER_SETUP, strlen(FEEDER_SETUP)) == 0)
	{
		initializeFeederControl();				// enable the elements of the feeder control system
		setPWM0ScalingFactors(0,0) ;			// set PWM scaling to 0
		setPWM0ZeroScalingTBFreq(10000) ;		// run the PWM at 10KHz pulse frequency
		setQEPUnitTimerFrequency(2000);			// set the Encoder sampling frequency to 2KHz (500uS)
		setRequiredQEPEncoderRate(0) ;
		enableFeeder();							// turn power onto the feeder system
		debugDisplayOnly("Feeder ON\r\n");
	}
	else if((szCmdLen == strlen(FEEDER_QEPTEST)) && strncmp(strCommand, FEEDER_QEPTEST, strlen(FEEDER_QEPTEST)) == 0)
	{
		disableQEPUintPeriodInt();
	}
	else if((szCmdLen == strlen(FEEDER_TEST)) && strncmp(strCommand, FEEDER_TEST, strlen(FEEDER_TEST)) == 0)
	{
		// modify the required encoder rate (note: rate is number of counts per encoder sampling period)
		switch((iFeederState++) % 10)
		{
			case 0:
				enableFeeder();
				setRequiredQEPEncoderRate(20) ;
				break ;
			case 1:
				setRequiredQEPEncoderRate(30) ;
				break ;
			case 2:
				setRequiredQEPEncoderRate(40) ;
				break ;
			case 3:
				setRequiredQEPEncoderRate(50) ;
				break ;
			case 4:
				setRequiredQEPEncoderRate(60) ;
				break ;
			case 5:
				setRequiredQEPEncoderRate(70) ;
				break ;
			case 6:
				setRequiredQEPEncoderRate(80) ;
				break ;
			case 7:
				setRequiredQEPEncoderRate(90) ;
				break ;
			default:
				disableFeeder();
				setRequiredQEPEncoderRate(0) ;
				break ;

		}
	}
	else if((szCmdLen == strlen(GAR_FILE)) && strncmp(strCommand, GAR_FILE, strlen(GAR_FILE)) == 0)
	{
		// handle onboard GAR file.
		GFENT *pDirEntry = NULL;
		INT iCnt = 0 ;
		GFRESIDENT *pgrfx = getResidentGFGrfx();
		if(pgrfx)
		{
			debugDisplayOnly("\r\nReading GAR File - %d entries\r\n Idx        GraphicName          Offset     Size         Type  FeatID  Flags  genID  FixVer  Source\r\n", pgrfx->rgfHdr.gfDirSize);
			for(pDirEntry = pgrfx->rgfEntries, iCnt =  0 ; iCnt < pgrfx->rgfHdr.gfDirSize ; pDirEntry++, iCnt++)
			{
				if(pDirEntry->gfSize > 0)
				{
					debugDisplayOnly("%3d - %02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X  %08X - %5d bytes  : %02X     %02X     %02X      %02X     %02X      %02X\r\n",
						iCnt,
						pDirEntry->gfDataName[0],pDirEntry->gfDataName[1],pDirEntry->gfDataName[2],pDirEntry->gfDataName[3],pDirEntry->gfDataName[4],pDirEntry->gfDataName[5],
						pDirEntry->gfDataName[6],pDirEntry->gfDataName[7],pDirEntry->gfDataName[8],pDirEntry->gfDataName[9],pDirEntry->gfDataName[10],pDirEntry->gfDataName[11],
						pDirEntry->gfOffset,
						pDirEntry->gfSize,
						pDirEntry->gfType,
						pDirEntry->gfFeatID,
						pDirEntry->gfFlags,
						pDirEntry->gfID,
						pDirEntry->gfFixVer,
						pDirEntry->gfSource
						);
				}
			}
		}
	}
	else if((szCmdLen == strlen(MBCMD_USBSERIAL_STOP)) && strncmp(strCommand, MBCMD_USBSERIAL_STOP, strlen(MBCMD_USBSERIAL_STOP)) == 0)
	{
		UINT8 *usbf0Reg = (UINT8 *)0x47401400;
		UINT8 *usbf1Reg = (UINT8 *)0x47401c00;
		UINT8 usbVal = usbf1Reg[1];
		//UINT8 pdmCmd[2] = {0x28, 0};

		debugDisplayOnly("\r\nOriginal USBF0/1 8 bit data registers %01X : %01X\r\n", usbf0Reg[1], usbf1Reg[1]);

		// disable USB port
		usbf1Reg[1] = (usbVal & 0xBF);
		debugDisplayOnly("\r\nUSBF0/1 8 bit data registers %01X : %01X\r\n", usbf0Reg[1], usbf1Reg[1]);
	}
	else if((szCmdLen == strlen(MBCMD_USBSERIAL_START)) && strncmp(strCommand, MBCMD_USBSERIAL_START, strlen(MBCMD_USBSERIAL_START)) == 0)
	{
		UINT8 *usbf0Reg = (UINT8 *)0x47401400;
		UINT8 *usbf1Reg = (UINT8 *)0x47401c00;
		UINT8 usbVal 	= usbf1Reg[1];
		//UINT8 pdmCmd[2] = {0x28, 0};

		debugDisplayOnly("\r\nOriginal USBF0/1 8 bit data registers %01X : %01X\r\n", usbf0Reg[1], usbf1Reg[1]);


		usbf1Reg[1] = (usbVal | 0x40);
		debugDisplayOnly("\r\nFinal USBF0/1 8 bit data registers %01X : %01X\r\n", usbf0Reg[1], usbf1Reg[1]);
	}
	else if((szCmdLen == strlen(MBCMD_USBSERIAL_STATS)) && strncmp(strCommand, MBCMD_USBSERIAL_STATS, strlen(MBCMD_USBSERIAL_STATS)) == 0)
	{
		// print usb stats
		debugDisplayOnly("\r\nNU_USBF_NET_Xmit_Packet:\r\n");
		debugDisplayOnly("net0     %8d CDCbytes   %8d NDISbytes    %5d CDCErrors   %5d NDISErrors - called  %5d (CDC) : %5d (NDIS)\r\n",
				USBNETWORKStats[0].CDCbytes, USBNETWORKStats[0].NDISbytes, USBNETWORKStats[0].CDCErrors, USBNETWORKStats[0].NDISErrors, USBNETWORKStats[0].CDCcalls, USBNETWORKStats[0].NDIScalls);
		debugDisplayOnly("net1     %8d CDCbytes   %8d NDISbytes    %5d CDCErrors   %5d NDISErrors - called  %5d (CDC) : %5d (NDIS)\r\n",
				USBNETWORKStats[1].CDCbytes, USBNETWORKStats[1].NDISbytes, USBNETWORKStats[1].CDCErrors, USBNETWORKStats[1].NDISErrors, USBNETWORKStats[1].CDCcalls, USBNETWORKStats[1].NDIScalls);
	}
	else if((szCmdLen == strlen(MBCMD_FRAMLOG)) && strncmp(strCommand, MBCMD_FRAMLOG, strlen(MBCMD_FRAMLOG)) == 0)
    {
        framlog_get_context_t ctx;
        fram_log_entry_hdr_t hdr;
        //uint16_t seq_nr;
        //uint32_t timestamp;
        //uint16_t len;
        char *p_msg;

        debugDisplayOnly("\r\nFram log\r\n");
        debugDisplayOnly("ctx    seq #  len    repeat  timestamp  msg\r\n");
        debugDisplayOnly("-----  -----  -----  ------  ---------  ------------------------------------------------------------------------------------------\r\n");

        if (Framlog_GetFirstEntry(&ctx, &hdr, (void **)&p_msg))
        {
            do
            {
                debugDisplayOnly("%5X  %5d  %5d  %6u  %9u  %s\r\n", ctx.idx, hdr.seq_nr, hdr.len, hdr.repeat_count, hdr.timestamp * 10, p_msg);
            } while(Framlog_GetNextEntry(&ctx, &hdr, (void **)&p_msg));
        }
        debugDisplayOnly("\r\nFram log end\r\n");
    }
	else if((szCmdLen == strlen(MBCMD_HCCSTATS)) && strncmp(strCommand, MBCMD_HCCSTATS, strlen(MBCMD_HCCSTATS)) == 0)
    {
        ctrs_count_id_t cid;
        ctrs_value_id_t vid;
        uint32_t cnt_this_run, cnt_all_runs;
        ctrs_value_stats_t val_this_run, val_all_runs;
        const char *name;
        const char tbl_divider_counts[] = "  +----------------------+----------+----------+\r\n";
        const char tbl_divider_values[] = "  +---------------------------------+----------+----------+----------+----------+------------++----------+----------+----------+----------+------------+\r\n";

        debugDisplayOnly("\r\nHCC file system statistics\r\n");
        debugDisplayOnly(tbl_divider_counts);
        debugDisplayOnly("  | %-20s | this run | all runs |\r\n", "function");
        debugDisplayOnly(tbl_divider_counts);
        for(cid=0; cid < NUM_OF_COUNT_ITEMS; cid++)
        {
            if (Ctrs_get_count(cid, &cnt_this_run, &cnt_all_runs, &name))
                debugDisplayOnly("  | %-20s | %8d | %8d |\r\n", name, cnt_this_run, cnt_all_runs);
        }
        debugDisplayOnly(tbl_divider_counts);

        debugDisplayOnly(
                     "\r\n  +---------------------------------+---------------------- this run ------------------------++---------------------- all runs ------------------------+\r\n");
        debugDisplayOnly("  | %-31s |  # calls | min size | avg size | max size | total size ||  # calls | min size | avg size | max size | total size |\r\n", "function");


        debugDisplayOnly(tbl_divider_values);
        for(vid=0; vid < NUM_OF_VALUE_ITEMS; vid++)
        {
            if (Ctrs_get_value(vid, &val_this_run, &val_all_runs, &name))
                debugDisplayOnly("  | %-31s | %8u | %8u | %8u | %8u | %10llu |"
                                           "| %8u | %8u | %8u | %8u | %10llu |\r\n", 
                        name, 
                        val_this_run.num_vals, val_this_run.val_min, val_this_run.val_avg, val_this_run.val_max, val_this_run.val_tot, 
                        val_all_runs.num_vals, val_all_runs.val_min, val_all_runs.val_avg, val_all_runs.val_max, val_all_runs.val_tot);
        }
        debugDisplayOnly(tbl_divider_values);

        /* retrieve the ftl stats */
        {
            uint32_t    ftl_ret;
            t_ftl_stats stats;

            ftl_ret = ftl_stats(0, &stats);
            if (ftl_ret == 0)
            {
                debugDisplayOnly("\n\rFlash Translation Layer Stats:\r\n");
                debugDisplayOnly("  blks_total    = %3d   blks_data = %3d   blks_map = %3d   page/blk   = %3d\r\n", stats.n_blocks_total    , stats.n_blocks_data, stats.n_blocks_map, stats.page_per_block);
                debugDisplayOnly("  blks_reserved = %3d   blks_mgmt = %3d   blks_bad = %3d   bytes/page = %3d\r\n", stats.n_blocks_reserved , stats.n_blocks_mgmt, stats.n_blocks_bad, stats.bytes_per_page);
            }
            else
                debugDisplayOnly("\n\rFlash Translation Layer Stats: error calling ftl_stats(). retval = %d\r\n", ftl_ret);
        }

        /* retrieve FAT freespace information */
        {
            int     result;
            F_SPACE space_info;

            result = f_getfreespace( 0, &space_info);
            if (result == 0)
            {
                debugDisplayOnly("\n\r\r\nFAT space information for drive A(0) in bytes:\r\n");
                debugDisplayOnly("  total size = %lu   free = %lu   used = %lu   bad = %lu\r\n", space_info.total, space_info.free, space_info.used, space_info.bad);
            }
            else
            {
                debugDisplayOnly("\n\r\r\nFAT free space information: error calling f_getfreespace(). retval = %d\r\n", result);
                debugDisplayOnly("\r\nrandom data length for image length adjust:%s\r\n", strlen(image_len_adjust));
            }
        }
        /* now dump the g_multi structure */
        {
            int i;
            for(i=0; i<sizeof(g_multi)/sizeof(g_multi[0]); i++)
            {
                debugDisplayOnly("g_multi[%d] %s ID=0x%p", i, g_multi[i].used? "inuse" : "free", g_multi[i].ID);

                if (g_multi[i].ID)
                {
                    if (g_multi[i].ID->tc_id == 0x5441534bUL)
                    {
                        debugDisplayOnly(" tc_id=0x%08X task=%s\r\n", g_multi[i].ID->tc_id, g_multi[i].ID->tc_name);
                    }
                    else
                    {
                        debugDisplayOnly(" tc_id=0x%08X (invalid)\r\n", g_multi[i].ID->tc_id);
                    }
                }
                else
                {
                    debugDisplayOnly(" (invalid)\r\n");
                }
            }
        }
        debugDisplayOnly("\r\n");
    }
	else if((szCmdLen == strlen(MBCMD_HCC_BLK_INFO)) && strncmp(strCommand, MBCMD_HCC_BLK_INFO, strlen(MBCMD_HCC_BLK_INFO)) == 0)
    {
        uint32_t    ftl_ret;
        t_ftl_stats stats;

        ftl_ret = ftl_stats(0, &stats);
        if (ftl_ret == 0)
        {
            t_ftl_blockinfo blk_info;
            uint32_t blk_idx;

            debugDisplayOnly("\r\nData block information. lba=Logical Block Address   pba=Physical Block Address     wear=#erase cycles done\r\n\r\n");
            for(blk_idx=0; blk_idx<10; blk_idx++) debugDisplayOnly("  lba/pba/wear");
            debugDisplayOnly("\r\n");
            /* first retrieve all data block information */
            for(blk_idx=0; blk_idx < stats.n_blocks_data; blk_idx++)
            {

                ftl_ret = ftl_get_blockinfo(0, true, blk_idx, 1, &blk_info);
                if(ftl_ret == 0)
                {
                    debugDisplayOnly("  %3d/%3d/%-4d", blk_info.lba, blk_info.pba, blk_info.wear);
                }

                if (0 == (blk_idx+1)%10)
                    debugDisplayOnly("\r\n");
            }
            if (0 != blk_idx%10)
                debugDisplayOnly("\r\n");
                
            debugDisplayOnly("\r\nMgmt block information. pba=Physical Block Address     wear=#erase cycles done\r\n\r\n");
            for(blk_idx=0; blk_idx<10; blk_idx++) debugDisplayOnly("  pba/wear");
            debugDisplayOnly("\r\n");
            /* first retrieve all data block information */
            int col=0;
            for(blk_idx=0; blk_idx < stats.n_blocks_data; blk_idx++)
            {

                ftl_ret = ftl_get_blockinfo(0, false, blk_idx, 1, &blk_info);
                if(ftl_ret == 0)
                {
                    col++;
                    debugDisplayOnly("  %3d/%4d", blk_info.pba, blk_info.wear);
                    if (col >= 10)
                    {
                        col = 0;
                        debugDisplayOnly("\r\n");
                    }
                }

            }
            if (col > 0)
                debugDisplayOnly("|\r\n");
                
        }

    }
	else if((szCmdLen == strlen(MBCMD_CCDFILE)) && strncmp(strCommand, MBCMD_CCDFILE, strlen(MBCMD_CCDFILE)) == 0)
	{
		char ccdFileName[100];
		ccdFileName[0] = 0 ;
	    strcat( ccdFileName, CMOSSignature.bUicPcn );
	    strcat( ccdFileName, CMOSSignature.bUicSN );
	    // RD change extension for now...
	    strcat( ccdFileName, CCD_DEFAULT_EXTENSION );

	    (void) loadCCDFile(ccdFileName);
		rLogCcdFileInfo(NULL) ;
	}
	else if((szCmdLen == strlen(MBCMD_FEATURETABLE)) && strncmp(strCommand, MBCMD_FEATURETABLE, strlen(MBCMD_FEATURETABLE)) == 0)
	{
		// dump the CMOS feature table content
		CHAR strLine[MAX_TEMP_LINE_LENGTH+1] ;
		CHAR strTemp[10];
		INT iPass ;
		strncpy(strLine, "      ", MAX_TEMP_LINE_LENGTH);
		for(iPass = 0 ; iPass < 10 ; iPass++)
		{
			snprintf(strTemp, 10, " %03d |", iPass);
			strncat(strLine, strTemp, MAX_TEMP_LINE_LENGTH);
		}
		debugDisplayOnly(strLine);
		debugDisplayOnly("\r\n------------------------------------------------------------------");
		strLine[0] = 0 ;

		for(iPass = 0 ; iPass < MAX_FEATURES ; iPass++)
		{
			if((iPass % 10) == 0)
			{
				if(iPass != 0)
				{
					debugDisplayOnly(strLine) ;
				}
				snprintf(strTemp, 10, "\r\n%3d| ", iPass) ;
				strncpy(strLine, strTemp, MAX_TEMP_LINE_LENGTH);
			}



			switch(CMOSFeatureTable.fTbl[iPass])
			{
				case NOT_PURCHASED:
					strncpy(strTemp, "  NP  ", 10) ;
					break ;
				case PERMDISBLD:
					strncpy(strTemp, " PDSBL", 10) ;
					break ;
				case PERMENBLD:
					strncpy(strTemp, " PENBL", 10) ;
					break ;
				case ENBLD:
					strncpy(strTemp, " ENBL ", 10) ;
					break ;
				case DISBLD:
					strncpy(strTemp, " DSBL ", 10) ;
					break ;
				case REDISBLD:
					strncpy(strTemp, "ReDsbl", 10) ;
					break ;
				case DELETED:
					strncpy(strTemp, " Dltd ", 10) ;
					break ;
				default:
					strncpy(strTemp, "UNKnwn", 10) ;
					break ;
			}
//			snprintf(strTemp, 10, "%3d, ",CMOSFeatureTable.fTbl[iPass]);
			strncat(strLine, strTemp, MAX_TEMP_LINE_LENGTH);
		}
		debugDisplayOnly(strLine) ;
		debugDisplayOnly("\r\n");
		if(strcmp(pPos, "clear") == 0)
		{
			CMOSExtUpdatedToVersion = 0;
			debugDisplayOnly("CMOS Version flag cleared, restart base to load EMD\r\n");
		}
	}
	else if((szCmdLen == strlen(MBCMD_WEBSOCKET_SETTINGS)) && strncmp(strCommand, MBCMD_WEBSOCKET_SETTINGS, strlen(MBCMD_WEBSOCKET_SETTINGS)) == 0)
	{
	    struct addr_struct  servaddr;            /* Server address structure */
		int port_index;
		STATUS status = 0;
		// manipulate websocket connectivity
		// if param is C then only allow connectivity via C port
		if(toupper(*pPos) == 'C')
		{
			debugDisplayOnly("Configuring websocket server for tablet interface only\r\n");
		    servaddr.id.is_ip_addrs[0] 	= 192;
		    servaddr.id.is_ip_addrs[1] 	= 168;
		    servaddr.id.is_ip_addrs[2] 	= 10;
		    servaddr.id.is_ip_addrs[3] 	= 246;
		    servaddr.family 			= NU_FAMILY_IP;
		    servaddr.port   			= 80;

		}
		else if(toupper(*pPos) == 'A')
		{
			debugDisplayOnly("Configuring websocket server for all interfaces\r\n");
		    servaddr.id.is_ip_addrs[0] 	= 0;
		    servaddr.id.is_ip_addrs[1] 	= 0;
		    servaddr.id.is_ip_addrs[2] 	= 0;
		    servaddr.id.is_ip_addrs[3] 	= 0;
		    servaddr.family 			= NU_FAMILY_IP;
		    servaddr.port   			= 80;
		}

	    if (status == NU_SUCCESS)
	    {
	        port_index = HTTP_Get_Listen_Port(HTTP_SVR_Data, 80);
	        if(port_index >= 0)
	        {
	        	// if there is a change then we need to close the existing listener, kill all sockets with the
	        	// same port (otherwise the nucleus reuse port trap hits us) then rebind the new address info.
	        	HTTP_Lite_Close_Listen_Port(&(HTTP_SVR_Data->listen_ports[port_index]));
	        	debugDisplayOnly("Killed %d sockets using port 80\r\n",killSocketsWithMatchingPorts(80));
	        	status = HTTP_Lite_Bind(&servaddr, NULL);
	        }
	        else
	        	status = -1;

            if(status == NU_SUCCESS)
            	debugDisplayOnly("Successfully configured websocket listening port\r\n");
            else
            {
            	if(status == NU_INVALID_PORT)
            		debugDisplayOnly("ERROR while configuring websocket listening port [%d] NU_INVALID_PORT\r\n", status);
            	else
            		debugDisplayOnly("ERROR while configuring websocket listening port [%d]\r\n", status);
            }
	    }
	    else
	    {
	    	debugDisplayOnly("ERROR - unable to acquire HTTP Lite semaphore [%d]\r\n", status);
	    }
	}
	else if((szCmdLen == strlen(MBCMD_WEBSOCKET_STATS)) && strncmp(strCommand, MBCMD_WEBSOCKET_STATS, strlen(MBCMD_WEBSOCKET_STATS)) == 0)
    {
        WSOX_CONTEXT_STRUCT *p_ctx;
    	int                 old_level;
    	UINT32				user_handle;
    	UINT32				ping_tx_count;
    	UINT32				pong_rx_count;

        debugDisplayOnly("handle|    type   | ping tx | pong rx \r\n");
        debugDisplayOnly("------|-----------|---------|---------\r\n");

        /* first show active connections */

//TODO - With new Nucleus release, confirm access of internal structures is still valid and check whether it can be replaced with API call
	    // take snaphsot of values
	    old_level = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
        p_ctx = WSOX_Connection_List.head;
        if (p_ctx)
        {
        	user_handle = p_ctx->user_handle;
            ping_tx_count = p_ctx->ping_tx_count;
            pong_rx_count = p_ctx->pong_rx_count;
        }
	    NU_Local_Control_Interrupts(old_level);

	    while(p_ctx)
        {
            debugDisplayOnly(" %4d | connected |%8d |%8d \r\n", user_handle, ping_tx_count, pong_rx_count);
    	    // take next snaphsot of values
    	    old_level = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
            p_ctx = p_ctx->flink;
            if (p_ctx)
            {
            	user_handle = p_ctx->user_handle;
                ping_tx_count = p_ctx->ping_tx_count;
                pong_rx_count = p_ctx->pong_rx_count;
            }
    	    NU_Local_Control_Interrupts(old_level);
        }

        /* then show listeners */

	    // take snaphsot of values
	    old_level = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
        p_ctx = WSOX_Listener_List.head;
        if (p_ctx)
        {
        	user_handle = p_ctx->user_handle;
            ping_tx_count = p_ctx->ping_tx_count;
            pong_rx_count = p_ctx->pong_rx_count;
        }
	    NU_Local_Control_Interrupts(old_level);

        while(p_ctx)
        {
            debugDisplayOnly(" %4d | listener  |%8d |%8d \r\n", user_handle, ping_tx_count, pong_rx_count);
    	    // take next snaphsot of values
    	    old_level = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
            p_ctx = p_ctx->flink;
            if (p_ctx)
            {
            	user_handle = p_ctx->user_handle;
                ping_tx_count = p_ctx->ping_tx_count;
                pong_rx_count = p_ctx->pong_rx_count;
            }
    	    NU_Local_Control_Interrupts(old_level);
        }

    }
	else if((szCmdLen == strlen(MBCMD_WEBSOCKET_TRACE)) && strncmp(strCommand, MBCMD_WEBSOCKET_TRACE, strlen(MBCMD_WEBSOCKET_TRACE)) == 0)
    {
        BOOL old_val = trace_wsox_ping_pong_enabled;
        
        if (strcmp(pPos, "on" ) == 0) trace_wsox_ping_pong_enabled = TRUE;
        if (strcmp(pPos, "off") == 0) trace_wsox_ping_pong_enabled = FALSE;

        heartbeat_trace_inhibit_override = trace_wsox_ping_pong_enabled;

        debugDisplayOnly("wstrace %s %s\r\n", old_val != trace_wsox_ping_pong_enabled ? "changed to" : "is", trace_wsox_ping_pong_enabled ? "enabled" : "disabled");

    }
	else if((szCmdLen == strlen(MBCMD_DIR)) && strncmp(strCommand, MBCMD_DIR, strlen(MBCMD_DIR)) == 0)
    {
        DSTAT statobj;
        char *default_pattern = "*";

        if (*pPos == 0)
        {
            pPos = default_pattern;
        }

        debugDisplayOnly("dir %s\r\n", pPos);

        /* A simple directory list subroutine. */
        if (NU_Get_First(&statobj, pPos) == NU_SUCCESS)
        {
            debugDisplayOnly("%40s  %8s  %10s\r\n", "Name", "Size", "Attributes");
            debugDisplayOnly("%40s  %8s  %10s\r\n", "----------------------------------------", "--------", "----------");

            while(1)
            {
                char fileattributes[50];
                char dirlink[256];

                sprintf(fileattributes, "%s%s%s%s%s%s (%u)",
                        (statobj.fattribute & ARDONLY) ? "<RO>"  : "",
                        (statobj.fattribute & AHIDDEN) ? "<HID>" : "",
                        (statobj.fattribute & ASYSTEM) ? "<SyS>" : "",
                        (statobj.fattribute & AVOLUME) ? "<VOL>" : "",
                        (statobj.fattribute & ADIRENT) ? "<DIR>" : "",
                        (statobj.fattribute & ARCHIVE) ? "<ARCHIVE>" : "",
                        statobj.fattribute);

                debugDisplayOnly("%40s  %8u  %s\r\n", statobj.lfname, (unsigned int)statobj.fsize, fileattributes);

                if (NU_Get_Next(&statobj) != NU_SUCCESS)
                {
                    (void) NU_Done(&statobj);
                    break;
                }
            }
        } 
        debugDisplayOnly("end dir\r\n");
    }
	else if((szCmdLen == strlen(MBCMD_COPY)) && strncmp(strCommand, MBCMD_COPY, strlen(MBCMD_COPY)) == 0)
    {
        char *p_src_path = get_cmd_parameter(&pPos);
        char *p_dst_path = get_cmd_parameter(&pPos);

        int copy_result = fsCopyFile(p_src_path, p_dst_path);

        if (copy_result != 0)
        {
            /* failure */
            debugDisplayOnly("Failed to copy %s to %s. result=%d\r\n", p_src_path, p_dst_path, copy_result);
        }
        else
        {
            debugDisplayOnly("Copied %s to %s.\r\n", p_src_path, p_dst_path);
        }
    }
	else if((szCmdLen == strlen(MBCMD_RENAME)) && strncmp(strCommand, MBCMD_RENAME, strlen(MBCMD_RENAME)) == 0)
    {
        char *p_src_path = get_cmd_parameter(&pPos);
        char *p_dst_path = get_cmd_parameter(&pPos);

        STATUS rename_result = NU_Rename( p_src_path, p_dst_path );

        if (rename_result != 0)
        {
            /* failure */
            debugDisplayOnly("Failed to rename %s to %s. result=%d\r\n", p_src_path, p_dst_path, rename_result);
        }
        else
        {
            debugDisplayOnly("Renamed %s to %s.\r\n", p_src_path, p_dst_path);
        }
    }
	else if((szCmdLen == strlen(MBCMD_DELETE)) && strncmp(strCommand, MBCMD_DELETE, strlen(MBCMD_DELETE)) == 0)
    {
        char *p_src_path = get_cmd_parameter(&pPos);

        STATUS delete_result = NU_Delete( p_src_path );

        if (delete_result != 0)
        {
            /* failure */
            debugDisplayOnly("Failed to delete %s. result=%d\r\n", p_src_path, delete_result);
        }
        else
        {
            debugDisplayOnly("Deleted %s.\r\n", p_src_path);
        }
    }
	else if((szCmdLen == strlen(MBCMD_MKDIR)) && strncmp(strCommand, MBCMD_MKDIR, strlen(MBCMD_MKDIR)) == 0)
    {
        char *p_src_path = get_cmd_parameter(&pPos);

        STATUS mkdir_result = NU_Make_Dir( p_src_path );

        if (mkdir_result != 0)
        {
            /* failure */
            debugDisplayOnly("Failed to create %s. result=%d\r\n", p_src_path, mkdir_result);
        }
        else
        {
            debugDisplayOnly("Created %s.\r\n", p_src_path);
        }
    }
	else if((szCmdLen == strlen(MBCMD_RMDIR)) && strncmp(strCommand, MBCMD_RMDIR, strlen(MBCMD_RMDIR)) == 0)
    {
        char *p_src_path = get_cmd_parameter(&pPos);

        STATUS rmdir_result = NU_Remove_Dir( p_src_path );

        if (rmdir_result != 0)
        {
            /* failure */
            debugDisplayOnly("Failed to remove %s. result=%d\r\n", p_src_path, rmdir_result);
        }
        else
        {
            debugDisplayOnly("Removed %s.\r\n", p_src_path);
        }
    }
	else if((szCmdLen == strlen(DEBUG_INTERFACE)) && strncmp(strCommand, DEBUG_INTERFACE, strlen(DEBUG_INTERFACE)) == 0)
	{
		UINT i1,i2,i3,i4,p;
		sscanf(pPos, "%u.%u.%u.%u:%u", &i1, &i2, &i3, &i4, &p);
		memset(&globalTransientDebugAddress, 0, sizeof(struct addr_struct)) ;
		globalTransientDebugAddress.family 	= NU_FAMILY_IP;

		globalTransientDebugAddress.id.is_ip_addrs[0] = (UINT8)i1;
		globalTransientDebugAddress.id.is_ip_addrs[1] = (UINT8)i2;
		globalTransientDebugAddress.id.is_ip_addrs[2] = (UINT8)i3;
		globalTransientDebugAddress.id.is_ip_addrs[3] = (UINT8)i4;
		globalTransientDebugAddress.port 			  = (UINT16)p;
		//
		debugDisplayOnly("Change debug interface port to %d.%d.%d.%d:%d\r\n",
				globalTransientDebugAddress.id.is_ip_addrs[0],
				globalTransientDebugAddress.id.is_ip_addrs[1],
				globalTransientDebugAddress.id.is_ip_addrs[2],
				globalTransientDebugAddress.id.is_ip_addrs[3],
				globalTransientDebugAddress.port
				);
	}
	else if((szCmdLen == strlen(MBCMD_TRM_WRITE_SIM)) && strncmp(strCommand, MBCMD_TRM_WRITE_SIM, strlen(MBCMD_TRM_WRITE_SIM)) == 0)
    {
        STATUS status = SUCCESS;
        char path[64];
        uint32_t sourceLen = 0;
        uint32_t start_tick, end_tick;
        uint8_t * buffer = NULL;
        char trfilename[32];
        int file_idx;
        
		// cmd format: trm-write-sim <dirname> <#files>
		char *p_dir_name = pPos;
		pPos = skipNonspaces(pPos) ;
		int name_len = pPos - p_dir_name;
		pPos = skipSpaces(pPos) ;
		INT num_files = atoi(pPos);
		*(p_dir_name+name_len) = 0; /* make sure it is null terminated */

        debugDisplayOnly("going to create %d files in %s\r\n", num_files, p_dir_name);
        status = NU_Allocate_Memory(MEM_Cached, ( void *) &buffer, 1800, NU_NO_SUSPEND);
        if (status != NU_SUCCESS)
        {
            debugDisplayOnly("Failed to Allocate 1800 byte Buffer\n");
        }
        else
        {
            file_idx = 1;
            do
            {
                start_tick = get_32kHz_tick_count();
                fnFilePath(path, p_dir_name, NULL);

                if(!fnFolderExists(path))
                {
                    status = NU_Make_Dir(path);
                    if(status != 0)
                    {
                        debugDisplayOnly("failed to create dir [%s]\r\n", path);
                        break;
                    }
                }


                trmGetTRFileName(trfilename, true);

                fnFilePath(path, p_dir_name, trfilename);
                sourceLen = get_rand_range(1588, 1757);
                debugDisplayOnly("going to write %d bytes to %s (%d/%d)...", sourceLen, trfilename, file_idx, num_files);

                {
                    uint32 i;
                    for(i=0; i < sourceLen; i+=4)
                    {
                        *((uint32_t *)&buffer[i]) = get_rand();
                    }
                    status = fsAppend(path, (CHAR*)buffer, sourceLen);
                    if(status == SUCCESS)
                    {
                        end_tick = get_32kHz_tick_count();
                        debugDisplayOnly("OK  (%u ms)\r\n", (end_tick - start_tick) >> 5); /* since there are 32000 ticks per second */
                        /* wait until atleast 1 second has passed */
                        while((end_tick - start_tick) < 33000) 
                        {
                            end_tick = get_32kHz_tick_count();
                        }
                    }
                    else
                    {
                        debugDisplayOnly("FAILED! status=%d\r\n", status);
                        break;
                    }
                }

                file_idx++;
            }while(file_idx <= num_files);

            if(buffer != NULL)
                NU_Deallocate_Memory(buffer);
        }        
        debugDisplayOnly("Done\r\n");
    }
	else if((szCmdLen == strlen(MBCMD_SSLDEBUG)) && strncmp(strCommand, MBCMD_SSLDEBUG, strlen(MBCMD_SSLDEBUG)) == 0)
	{
		// usage ssldebug [off | 0 | 1 | 2 | 3]  - if the char after the command is non digit we turn interface off
		if(pPos && isdigit(*pPos))
		{
		    wolfSSL_Debugging_ON();
			switch(*pPos)
			{
				case '0': globalWolfDebugCbLevelFilter = WOLFDEBUG_HIGH 	; break ;
				case '1': globalWolfDebugCbLevelFilter = WOLFDEBUG_MEDIUM 	; break ;
				case '2': globalWolfDebugCbLevelFilter = WOLFDEBUG_LOW 		; break ;
				default : globalWolfDebugCbLevelFilter = WOLFDEBUG_ALL 		; break ;
			}
		}
		else
		{
		    wolfSSL_Debugging_OFF();
		}
	}
	else if((szCmdLen == strlen(MBCMD_TRM_LOG)) && strncmp(strCommand, MBCMD_TRM_LOG, strlen(MBCMD_TRM_LOG)) == 0)
	{
		int old_val = trm_log_level;

		if (strcmp(pPos, "on" ) == 0) trm_log_level = TRUE;
		if (strcmp(pPos, "off") == 0) trm_log_level = FALSE;

		debugDisplayOnly("trmlog level %s %s\r\n", old_val != trm_log_level ? "changed to" : "is", trm_log_level ? "enabled" : "disabled");

	}
	else if((szCmdLen == strlen(MBCMD_TRM_COUNTERS)) && strncmp(strCommand, MBCMD_TRM_COUNTERS, strlen(MBCMD_TRM_COUNTERS)) == 0)
	{
		debugDisplayOnly("\r\nTransaction manager information\r\n");
		debugDisplayOnly(" Item                   |  value    \r\n");
		debugDisplayOnly(" -----------------------|------------ \r\n");
		debugDisplayOnly(" MailPieceID            |  %ld  \r\n", CMOSTrmMailPieceID);
		debugDisplayOnly(" ActiveBucket           |  %ld \r\n", CMOSTrmActiveBucket);
		debugDisplayOnly(" FailedWrites           |  %ld \r\n", CMOSTrmFailedWrites);
		debugDisplayOnly(" FailedReads            |  %ld \r\n", CMOSTrmFailedReads);
		debugDisplayOnly(" FailedDeletes          |  %ld \r\n", CMOSTrmFailedDeletes);
		debugDisplayOnly(" ZeroLengthFiles        |  %ld \r\n", CMOSTrmZeroLengthFiles);
		debugDisplayOnly(" ZeroLenDeletFails      |  %ld \r\n", CMOSTrmZeroLenDeletFails);
		debugDisplayOnly(" DcapDeviceID           |  %ld \r\n", CMOSTrmDcapDeviceID);
		debugDisplayOnly(" FlashFileSeqCount      |  %ld \r\n", CMOSTrmFlashFileSeqCount);
		debugDisplayOnly(" Version                |  %ld \r\n", CMOSTrmVersion);
		debugDisplayOnly(" State                  |  %ld \r\n", CMOSTRMState);
		debugDisplayOnly(" UploadState            |  %ld \r\n", CMOSTrmUploadState);
		debugDisplayOnly(" DcapFileSeqCount       |  %ld \r\n", CMOSTrmDcapFileSeqCount);
		debugDisplayOnly(" UploadDueReqd          |  %ld \r\n", CMOSTrmUploadDueReqd);

		debugDisplayOnly(" MidnightFlag           |  %ld \r\n", CMOSTrmMidnightFlag);
		debugDisplayOnly(" MaxFilesToUpload       |  %ld \r\n", CMOSTrmMaxFilesToUpload);
		debugDisplayOnly(" MailPiecesToUpload     |  %ld \r\n", CMOSTrmMailPiecesToUpload);
		debugDisplayOnly(" PendingBucketToUpload  |  %ld \r\n", CMOSTrmPendingBucketToUpload);
		debugDisplayOnly(" FatalError             |  %ld \r\n", CMOSTrmFatalError);
		debugDisplayOnly(" TRCFilesToBeRemoved    |  %ld \r\n", CMOSTrmTRCFilesToBeRemoved);
		debugDisplayOnly(" MailPiecesPackaged     |  %ld \r\n", CMOSTrmMailPiecesPackaged);
		debugDisplayOnly(" UploadDueReqd          |  %ld \r\n", CMOSTrmUploadDueReqd);

		debugDisplayOnly(" PrevUploadDate         |  %02d%02d%02d%02d-%02d%02d%02d \r\n",
															CMOSTrmPrevUploadDate.bCentury,
															CMOSTrmPrevUploadDate.bYear,
															CMOSTrmPrevUploadDate.bMonth,
															CMOSTrmPrevUploadDate.bDay,
															CMOSTrmPrevUploadDate.bHour,
															CMOSTrmPrevUploadDate.bMinutes,
															CMOSTrmPrevUploadDate.bSeconds );
		debugDisplayOnly(" TimeStampOfOldestMP    |  %02d%02d%02d%02d-%02d%02d%02d \r\n",
															CMOSTrmTimeStampOfOldestMP.bCentury,
															CMOSTrmTimeStampOfOldestMP.bYear,
															CMOSTrmTimeStampOfOldestMP.bMonth,
															CMOSTrmTimeStampOfOldestMP.bDay,
															CMOSTrmTimeStampOfOldestMP.bHour,
															CMOSTrmTimeStampOfOldestMP.bMinutes,
															CMOSTrmTimeStampOfOldestMP.bSeconds );
		debugDisplayOnly(" TimeStampOfMPIDReset   |  %02d%02d%02d%02d-%02d%02d%02d \r\n",
															CMOSTrmTimeStampOfMPIDReset.bCentury,
															CMOSTrmTimeStampOfMPIDReset.bYear,
															CMOSTrmTimeStampOfMPIDReset.bMonth,
															CMOSTrmTimeStampOfMPIDReset.bDay,
															CMOSTrmTimeStampOfMPIDReset.bHour,
															CMOSTrmTimeStampOfMPIDReset.bMinutes,
															CMOSTrmTimeStampOfMPIDReset.bSeconds );
		debugDisplayOnly(" DcapFileName           |  %s \r\n", CMOS2TrmDcapFileName );
		debugDisplayOnly(" Guid                   |  %s \r\n", CMOSDcapGuid );

	}
	else if((szCmdLen == strlen(MBCMD_PEERCERT)) && strncmp(strCommand, MBCMD_PEERCERT, strlen(MBCMD_PEERCERT)) == 0)
	{
	    WSOX_CONTEXT_STRUCT *p_ctx = NULL;
        WOLFSSL 			*p_ssl = NULL ;
    	int                 old_level;
	    INT 				socketd;
	    BOOL 				useSSL = FALSE;
	    BOOL 				gotCert = FALSE;
	    char  				certName[ASN_NAME_MAX];

	    memset(certName, 0, ASN_NAME_MAX);

//TODO - With new Nucleus release, confirm access of internal structures is still valid and check whether it can be replaced with API call
	    // take snaphsot of values
	    old_level = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
        p_ctx = WSOX_Connection_List.head;
        if (p_ctx)
        {
        	socketd = p_ctx->socketd;
        	useSSL = (p_ctx->ssl != NULL);
        	if (useSSL)
        	{
                p_ssl = (WOLFSSL *)p_ctx->ssl;
        		gotCert = ((p_ssl->peerCert.issuer.fullName.fullName != NULL) && (p_ssl->peerCert.issuer.fullName.fullNameLen > 0));
        		if (gotCert)
        		{
        			memcpy(certName, p_ssl->peerCert.issuer.fullName.fullName, MIN(p_ssl->peerCert.issuer.fullName.fullNameLen, ASN_NAME_MAX - 1));
        		}
        	}
        }
	    NU_Local_Control_Interrupts(old_level);

	    if(p_ctx)
	    {
	        while(p_ctx != NULL)
	        {
	            debugDisplayOnly("Websocket socket connection %d uses%s SSL with %s certificate\r\n", socketd, (useSSL ? "":" no"), (gotCert ? certName:"no"));
	    	    // clear local values and take next snaphsot of values
	    	    memset(certName, 0, ASN_NAME_MAX);
	    	    gotCert = FALSE;

	    	    old_level = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
	            p_ctx = p_ctx->flink;
	            if (p_ctx)
	            {
	            	socketd = p_ctx->socketd;
	            	useSSL = (p_ctx->ssl != NULL);
	            	if (useSSL)
	            	{
	                    p_ssl = (WOLFSSL *)p_ctx->ssl;
	            		gotCert = ((p_ssl->peerCert.issuer.fullName.fullName != NULL) && (p_ssl->peerCert.issuer.fullName.fullNameLen > 0));
	            		if (gotCert)
	            		{
	            			memcpy(certName, p_ssl->peerCert.issuer.fullName.fullName, MIN(p_ssl->peerCert.issuer.fullName.fullNameLen, ASN_NAME_MAX - 1));
	            		}
	            	}
	            }
	    	    NU_Local_Control_Interrupts(old_level);
	        }
	    }
	    else
	    {
	        debugDisplayOnly("No Websocket connections found\r\n");
	    }
	}
	else if((szCmdLen == strlen(MBCMD_HBSTATS)) && strncmp(strCommand, MBCMD_HBSTATS, strlen(MBCMD_HBSTATS)) == 0)
	{
		debugDisplayOnly("\r\nHB Rsp info: last attempted=[%s] last success=[%s] last failed=[%s]\r\n\r\n",
                                                last_hb_rsp_seqnum_attempted_to_send,
                                                last_hb_rsp_seqnum_sent_success,
                                                last_hb_rsp_seqnum_sent_failed   );

		debugDisplayOnly("                     :            Time Remaining in Seconds        \r\n");
		debugDisplayOnly("Total   ] Disconnect :   0    0.5   1.0   1.5   2.0   2.5   3.0   3.5   4.0   4.5   5.0   5.5   6.0   6.5   7.0   7.5   8.0  \r\n");
		debugDisplayOnly("-----------------------------------------------------------------------------------------------------------------------------\r\n");
		debugDisplayOnly("%7d ]     %5d  : %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d \r\n",
				globalTotalHB,
				globalHBRemaining[0],
				globalHBRemaining[1],
				globalHBRemaining[2],
				globalHBRemaining[3],
				globalHBRemaining[4],
				globalHBRemaining[5],
				globalHBRemaining[6],
				globalHBRemaining[7],
				globalHBRemaining[8],
				globalHBRemaining[9],
				globalHBRemaining[10],
				globalHBRemaining[11],
				globalHBRemaining[12],
				globalHBRemaining[13],
				globalHBRemaining[14],
				globalHBRemaining[15],
				globalHBRemaining[16],
				globalHBRemaining[17]) ;
		// now dump the hb store information
		if(*pPos == 'c')
		{
			// clear the hb structure.....
			debugDisplayOnly("\r\n!! Clearing HB timeout structure !!\r\n");
			clearHBStore();
		}
		else
		{
			INT iIndex = (INT)getHBStoreRawIndex();
			char strOut[150];

			if(iIndex > 0)
			{
				debugDisplayOnly("\r\n\r\nHB Timeout Disconnect log - %d total timeouts, last %d displayed\r\n", iIndex, HB_STORE_MAX_COUNT);
				// if index is >0 but < max array size then
				if(iIndex < HB_STORE_MAX_COUNT)
				{
					for(--iIndex; iIndex != (UINT)-1 ; iIndex--)
					{
						getHBStoreRecord(iIndex, strOut,  150);
						debugDisplayOnly("%s\r\n",strOut) ;
					}
				}
				else
				{
					// if index is larger than the array size then we have wrapped around so we need to print the entire array
					// backwards...hmmmm
					INT iIdx = (iIndex % HB_STORE_MAX_COUNT) + HB_STORE_MAX_COUNT;
					for(--iIdx ; iIdx >= (iIndex % HB_STORE_MAX_COUNT) ; iIdx--)
					{
						// no worries the get function applies a modulo....
						getHBStoreRecord(iIdx, strOut,  150);
						debugDisplayOnly("%s\r\n",strOut) ;
					}
				}
			}
			else
			{
				debugDisplayOnly("\r\n\r\n-- No HB Timeout information to display --\r\n");
			}
		}
	}
	else if((szCmdLen == strlen(DEBUG_TOFILE)) && strncmp(strCommand, DEBUG_TOFILE, strlen(DEBUG_TOFILE)) == 0)
	{
		debugDisplayOnly("DIRECTING DEBUG TO FILE\r\n");

		debugTerminalDumpToFile();

		debugDisplayOnly("DEBUG DIRECTED TO SCREEN AGAIN\r\n");
	}
	else if((szCmdLen == strlen(MBCMD_NETWORKBUFFERS)) && strncmp(strCommand, MBCMD_NETWORKBUFFERS, strlen(MBCMD_NETWORKBUFFERS)) == 0)
	{
		debugDisplayOnly("MEM Buffers Used = %u  - Max since last clearing %u - MAX_BUFFERS = %d\r\n", MEM_Buffers_Used, globalMemBuffersUsedMax, MAX_BUFFERS);
		if(*pPos == 'c')
			globalMemBuffersUsedMax = 0;
	}
    else if ((szCmdLen == strlen("tl-dump")) && strncmp(strCommand, "tl-dump", strlen("tl-dump")) == 0)
    {
        char hdr_line[50];
        char sz_log_line[128]; 
        DATETIME rDateTimeStamp;

        GetSysDateTime(&rDateTimeStamp, ADDOFFSETS, GREGORIAN);

        sprintf(hdr_line, "Tasklog created on %02d%02d%02d%02d-%02d%02d%02d\r\n",
                    rDateTimeStamp.bCentury,
                    rDateTimeStamp.bYear,
                    rDateTimeStamp.bMonth,
                    rDateTimeStamp.bDay,
                    rDateTimeStamp.bHour,
                    rDateTimeStamp.bMinutes,
                    rDateTimeStamp.bSeconds  );

        
        tasklog_dump_to_file(hdr_line, sz_log_line);
        debugDisplayOnly(sz_log_line);
    }
    else if ((szCmdLen == strlen(MBCMD_TCPNAT)) && strncmp(strCommand, MBCMD_TCPNAT, strlen(MBCMD_TCPNAT)) == 0)
    {
    	UINT uCount = 0 ;
    	// Dump the TCP NAT table,
    	debugDisplayOnly("TCP NAT table has %d slots in it:\r\n",NAT_MAX_TCP_CONNS);
    	debugDisplayOnly("Idx       Src IP       Port       Dst IP         Port    Time Active     State\r\n");
    	// check for null pointer in nat table prior to running.
    	for(int iPass=0 ; (NAT_Translation_Table.nat_tcp_table) && (iPass < NAT_MAX_TCP_CONNS) ; iPass++)
    	{
			NAT_TCP_ENTRY *pNE = &(NAT_Translation_Table.nat_tcp_table->nat_tcp_entry[iPass]);
			UINT32 uNow = NU_Retrieve_Clock();
    		if(pNE && (pNE->nat_timeout > 0))
    		{
    			UINT32 uTime = (uNow - pNE->nat_timeout)/100; // seconds
				UINT8 *pSrc  = (UINT8 *)&pNE->nat_internal_source_ip;
				UINT8 *pDst  = (UINT8 *)&pNE->nat_destination_ip;
				UINT  hrs    = uTime/(60*60);
				UINT  mins   = uTime % (60*60);
				UINT  secs   = mins % 60;
				mins /= (60);
				debugDisplayOnly("%3d - %3d.%3d.%3d.%3d:%5d --> %3d.%3d.%3d.%3d:%5d     %02d:%02d:%02d S - %s\r\n",
						iPass,
						pSrc[3], pSrc[2], pSrc[1], pSrc[0], pNE->nat_internal_source_port,
						pDst[3], pDst[2], pDst[1], pDst[0], pNE->nat_destination_port,
						hrs, mins, secs,
						NATStateToString(pNE->nat_state));
				uCount++;
    		}
    	}
    	debugDisplayOnly("%d entries located\r\n\r\n", uCount);

    }
    else if ((szCmdLen == strlen(MBCMD_IRQ)) && strncmp(strCommand, MBCMD_IRQ, strlen(MBCMD_IRQ)) == 0)
    {
    	// Nucleus adds 2 to Sitara IRQ numbers
    	debugDisplayOnly("\r\nSitara IRQ Handler Addresses:\r\nUnhandled interrupt routine at 0x%08X:\r\n", (UINT32)TCC_Unhandled_Interrupt);
    	for(int iPass=2 ; iPass < 129 ; iPass++)
    	{
    		if(((iPass-2) % 5) == 0) debugDisplayOnly("\r\n");

    		debugDisplayOnly("IRQ%03d=0x%08X : ", iPass-2,(UINT32)ESAL_GE_ISR_Interrupt_Handler[iPass]);
    	}
    	debugDisplayOnly("\r\n");
    }
    else if ((szCmdLen == strlen(MBCMD_SEMAPHORES)) && strncmp(strCommand, MBCMD_SEMAPHORES, strlen(MBCMD_SEMAPHORES)) == 0)
    {
    	// display sempahore status...
    	for(int iPass=0 ; iPass < MAX_UIC_SEMAPHORES ; iPass++)
    	{
    		debugDisplayOnly("%d ID %d [%s] initial %d  type %s : name %s  owner %08X  tasks waiting %d  sem count %d  suspend Type %d\r\n",
    				iPass,
					(UINT)Semaphores[iPass].pSemaphore->sm_id,
					Semaphores[iPass].szSemaphoreName,
					(UINT)Semaphores[iPass].lwInitialCount,
					Semaphores[iPass].type == NU_FIFO ? "   FIFO    " : "NU_PRIORITY",
					Semaphores[iPass].pSemaphore->sm_name,
					(UINT32)Semaphores[iPass].pSemaphore->sm_semaphore_owner,
					(UINT)Semaphores[iPass].pSemaphore->sm_tasks_waiting,
					(UINT)Semaphores[iPass].pSemaphore->sm_semaphore_count,
					(UINT)Semaphores[iPass].pSemaphore->sm_suspend_type
					);
    	}
    }

	else
		bHandled = FALSE ;

	return bHandled;
}

int globalPowerFlag = 0;
/***********************************************************************
*
*     FUNCTION
*
*         debugHandleInputSingleChar
*
*     DESCRIPTION
*
*         process single char commands
*
*     INPUTS
*
*		 pnuMemPool	- Nucleus memory pool used for I2C initialization.
*        ch			- the character to interpret
*
*     OUTPUTS
*
*         none
*
***********************************************************************/
INT iPowerCount = 0 ;
#define DEBUG_TEMP_STRINGLENGTH		500
BOOL debugHandleInputSingleChar(NU_MEMORY_POOL 	*pnuMemPool, char ch)
{
//	uchar 					uc[2];
	BOOL					bHandled = TRUE ;
	char					strTemp[DEBUG_TEMP_STRINGLENGTH+1];
//	STATUS					status;

	switch(ch)
	{
		case 'c':
			dumpCmStatusStructure(&cmStatus);
			break ;

		case 'd':
			{
				DATETIME rTime;
				// display current date time
			    (void)GetSysDateTime(&rTime, ADDOFFSETS, GREGORIAN);
				globalNetlogStartTime = rTime;
				debugDisplayOnly("System Time  %02d/%02d/%02d  %02d:%02d:%02d\r\n",
								rTime.bMonth, rTime.bDay, rTime.bYear, rTime.bHour, rTime.bMinutes, rTime.bSeconds);
			}
			break;
		case 'h':
			{
				char lan_names[100];
				char *lan_name;
				size_t lan_names_size = sizeof(lan_names);
				unsigned char res = fnGetLANNames(lan_names, &lan_names_size);
				debugDisplayOnly("\r\n\n GetLANNames: result = %d, lan_names=[%s] - operating in %s mode - DHCP %s\r\n",
						res,
						lan_names,
						(CMOSglobalNetworkInformationStructure.sourceFlag == NIS_SOURCE_WIRED) ? "Wired" : "Tablet WiFi",
						(globalDHCPProcessActive != 0) ? "Processing" : "Idle"
						);

				char *pRest = NULL;
				lan_name = strtok_r(lan_names, ",", &pRest);
				while (lan_name != NULL)
				{
					debugPrintGetLANConfig(lan_name);
					lan_name = strtok_r(NULL, ",", &pRest);
				}
				debugDisplayIP4DNSServers();
				displayProxySettings();
			}
			break;

		case 'i':
			{
				if(connectedToInternet())
				{
					debugDisplayOnly("\r\nInternet connection exists!\r\n");
					break ;
				}
				debugDisplayOnly("NOT CONNECTED TO INTERNET\r\n");
				break;
			}

		case 'n':
			printNUSocketInformation();
			break;

		case 'p':
			iPowerCount++;
			if(iPowerCount == 8) iPowerCount = 0 ;

			debugDisplayOnly("27volt - %s   :", (iPowerCount & 1) == 0 ? "ON " : "OFF") ;
			HALSetVM27VPowerState((iPowerCount & 1) == 0) ;

			debugDisplayOnly("   5VS - %s   :", (iPowerCount & 2) == 0 ? "ON " : "OFF") ;
			HALSetS5VSPowerState((iPowerCount & 2) == 0) ;

			debugDisplayOnly("   5volt - %s%s", (iPowerCount & 4) == 0 ? "ON " : "OFF", (iPowerCount == 0) ? " - All domains now ON, normal operation\r\n": "\r\n" ) ;
			HALSetS5VPowerState((iPowerCount & 2) == 0) ;

			break;

		case 's':
//			checkFirmwareAndUpdate(pnuMemPool, (UINT8 *)globalptrEmbeddedFirmwareStart, globalEmbeddedFirmwareSize, TRUE);
			checkFirmwareAndUpdate(pnuMemPool, TRUE);
			break;

		case 'v':
			// print all firmware information
			// start with PDM App and firmware
			debugDisplayOnly("USB Power Delivery Chip Application and Firmware versions:");
			printfAllPDMVersion(&globalPDMVersionCache);

			getPDFirmwareVersion(strTemp, DEBUG_TEMP_STRINGLENGTH);
			debugDisplayOnly("\r\nAPI format PD Firmware Version = \'%s\'\r\n", strTemp);

			debugDisplayOnly("-------------------------------------------------------------------------\r\n");

			// now lets output the CSD2/3 identification pin value, this is the feeder present ident pin (GPIO1(8))
			debugDisplayOnly(HALIsFeederPresent() ? "This is a CSD3 system\r\n" : "This is a CSD2 System\r\n");
			debugDisplayOnly("-------------------------------------------------------------------------\r\n");
			debugDisplayOnly("Feeder cover is %s  Feed Platform is %s  Print Head Cover is %s  - [%08lX]\r\n",
						(readSimpleGPIOPin(1, 30) == 1) ? "OPEN" : "CLOSED",
						(readSimpleGPIOPin(1, 24) == 1) ? "OCCUPIED" : "CLEAR",
						(readSimpleGPIOPin(1, 22) == 1) ? "OPEN" : "CLOSED",
						(UINT32)ESAL_GE_MEM_READ32(GPIOAddr[1] + AM335X_GPIO_DATAIN));
			resetPDMISR();
			debugDisplayOnly("PDM Overcurrent detect flag is : %s\r\n", (readPDMOverCurrentFlag() == 1) ? "OK" : "Overcurrent state triggered") ;
			debugDisplayOnly("-------------------------------------------------------------------------\r\n");

			PB_PDO_MASKS pdmMasks;
			PB_PDM_POWER_STATUS pdmStatus;
			PB_PDM_CURRENT_DATA_OBJECTS pdmDO;
			printPDMPowerDeliveryStatus(pnuMemPool, &pdmMasks,  &pdmStatus,  &pdmDO);

			break;

		case 'r':
	    	taskException("Test Exception!", "Debug", __LINE__);
			break;

		case 'z':
		{
			INT 	i1, i2, i3 ;
			float 	f1 ;
			CHAR 	strTxt[20];
			INT		iN1, iN2, iN3, iN4, iN5;

			// test for sscanf - temporary, do not leave in
			CHAR *strRef = "1234  ThisIsText 12    0000123400 12.34";
			CHAR *strFmt = "%d%n%s%n%d%n%d%n%f%n" ;
			CHAR *strFmt1 = "%d %n %s %n %d %n %d %n %f %n" ;
			INT iCount = sscanf(strRef, strFmt, &i1, &iN1, strTxt, &iN2, &i2, &iN3, &i3, &iN4, &f1, &iN5);

			debugDisplayOnly("\r\nFound %d fields : %d [%d] %s [%d] %d [%d] %d  [%d] %5.2f [%d]\r\n",
					iCount, i1, iN1, strTxt, iN2, i2, iN3, i3, iN4, f1, iN5);

			iCount = sscanf(strRef, strFmt1, &i1, &iN1, strTxt, &iN2, &i2, &iN3, &i3, &iN4, &f1, &iN5);

			debugDisplayOnly("\r\nFound %d fields : %d [%d] %s [%d] %d [%d] %d  [%d] %5.2f [%d]\r\n",
					iCount, i1, iN1, strTxt, iN2, i2, iN3, i3, iN4, f1, iN5);

		}
			break;

		case '?':
			// print out a help screen
			debugDisplayOnly(" Command Help\r\n");
			debugDisplayOnly(" bi  . . . . . . . . . . . . . . . Dump contents of the BoardInfo structure\r\n");
			debugDisplayOnly(" c . . . . . . . . . . . . . . . . Dump contents of the cmStatus structure\r\n");
			debugDisplayOnly(" cbm . . . . . . . . . . . . . . . Cycle BootMode\r\n");
			debugDisplayOnly(" ccd . . . . . . . . . . . . . . . Read CCD file and output contents to screen\r\n");
			debugDisplayOnly(" cft . [clear] . . . . . . . . . . Dump Customer Feature Table content - if 'clear' then clear the stored version forcing update on restart\r\n");
			debugDisplayOnly(" d   . . . . . . . . . . . . . . . Display current date and time\r\n");
			debugDisplayOnly(" de  x.x.x.x:pp  . . . . . . . . . Change the debug interface to the requested IP address  x.x.x.x:pp\r\n");
			debugDisplayOnly(" dhcp  [renew |  . . . . . . . . . initiate a dhcp operation in 5 seconds\r\n");
			debugDisplayOnly("        rebind\r\n");
			debugDisplayOnly("        new\r\n");
			debugDisplayOnly("        release]\r\n");
			debugDisplayOnly(" err . [mem] . . . . . . . . . . . Force a stack overflow or [mem] = illegal memory access to 0x60000000\r\n");
			debugDisplayOnly(" fs  . . . . . . . . . . . . . . . Intitialize the feeder test\r\n");
			debugDisplayOnly(" fq  . . . . . . . . . . . . . . . Disable the QEP interrupt, this will terminate the feeder testing\r\n");
			debugDisplayOnly(" f   . . . . . . . . . . . . . . . Step to next feeder speed value in feeder test\r\n");
			debugDisplayOnly(" framlog . . . . . . . . . . . . . Dump persistent log located in fram\r\n");
			debugDisplayOnly(" gar . . . . . . . . . . . . . . . Read resident GAR file\r\n");
			debugDisplayOnly(" h . . . . . . . . . . . . . . . . Get LAN names and state\r\n");
			debugDisplayOnly(" hb. . . . . . . . . . . . . . . . Display heartbeat timing stats and timeout log\r\n");
			debugDisplayOnly(" hb    c . . . . . . . . . . . . . Clear the heartbeat timeout log\r\n");
			debugDisplayOnly(" hcc . . . . . . . . . . . . . . . Show HCC file system stats\r\n");
			debugDisplayOnly(" hcc-blk-info  . . . . . . . . . . Show block wear and address info for HCC file system\r\n");
			debugDisplayOnly(" i . . . . . . . . . . . . . . . . Show if a connection to the internet exists\r\n");
			debugDisplayOnly(" irq . . . . . . . . . . . . . . . Show the address of all IRQ handlers\r\n");
			debugDisplayOnly(" l . . . . . . . . . . . . . . . . Dump Syslog information\r\n");
			debugDisplayOnly("        -inc <text>. . . . . . . . Only include syslog lines with <text>\r\n");
			debugDisplayOnly("        -exc <text>. . . . . . . . Exclude any syslog lines with <text>\r\n");
			debugDisplayOnly("              example: l -inc >>CM-inc TEXT: -exc 48\r\n");
			debugDisplayOnly(" macs  . . . . . . . . . . . . . . Display the allocated MAC addresses for this SITARA chip.\r\n");
			debugDisplayOnly(" mp  . . . . . . . . . . . . . . . Dump out the linked list of memory pools.\r\n");
			debugDisplayOnly("        [poolname] . . . . . . . . Optional name, if present will dump the allocated blocks from this pool\r\n");
			debugDisplayOnly(" n . . . . . . . . . . . . . . . . Display networking information\r\n");
			debugDisplayOnly(" nb  c[lear] . . . . . . . . . . . Display network buffers in use this includes a max tracking value\r\n");
			debugDisplayOnly(" nis . . . . . . . . . . . . . . . Display persisted networking information structure\r\n");
			debugDisplayOnly(" nlog  . . . . . . . . . . . . . . Dump the contents of the nucleus network log\r\n");
			debugDisplayOnly(" p . . . . . . . . . . . . . . . . Toggle the 27v, 5vS and 5v power rails on and off in binary order\r\n");
			debugDisplayOnly(" r . . . . . . . . . . . . . . . . Generate a task exception which goes into Exception.log\r\n");
			debugDisplayOnly(" s . . . . . . . . . . . . . . . . Check and load USB PDM Firmware\r\n");
			debugDisplayOnly(" sem . . . . . . . . . . . . . . . Display the state of system semaphores\r\n");
			debugDisplayOnly(" skt  [ip adds]. <noping>. . . . . try to open a socket connection (port 80) to supplied address (or google by default)\r\n");
			debugDisplayOnly(" sktkill  <skt num>  . . . . . . . Close the specified socket number\r\n");
			debugDisplayOnly(" ssldebug  [off | 0 | 1 | 2 | 3] . Turn on or off ssl debug trace information, 3=All info 0=HiPriority only\r\n");
			debugDisplayOnly(" sm  . . . . . . . . . . . . . . . Dump out the settingsmanager\r\n");
			debugDisplayOnly(" t . . . . . . . . . . . . . . . . Get stats for requested task name\r\n");
			debugDisplayOnly("                                   With no parameter this will display all tasks\r\n");
			debugDisplayOnly("        <name>                     name of the task to return stats for\r\n");
			debugDisplayOnly(" tcpnat  . . . . . . . . . . . . . Dump the contents of the system TCP NAT Table\r\n");
			debugDisplayOnly(" th  . . . . . . . . . . . . . . . Get stats for requested HISR name\r\n");
			debugDisplayOnly("                                   With no parameter this will display all HISRs\r\n");
			debugDisplayOnly("        <name>                     name of the task to return stats for\r\n");
			debugDisplayOnly(" tp <name> <priority>  . . . . . . Set the priority (0-255) of the given task name\r\n");
			debugDisplayOnly(" trmlog [0 | 1 | 2]  . . . . . . . Show or set transaction manager log\r\n");
			debugDisplayOnly(" trm-write-sim dirname #files  . . Simulate the file writes the trm does. sample: trm-write-sim trs2 21\r\n");
			debugDisplayOnly(" usbstop . . . . . . . . . . . . . Stop usb interface\r\n");
			debugDisplayOnly(" usbstart. . . . . . . . . . . . . Start usb interface\r\n");
			debugDisplayOnly(" usb . . . . . . . . . . . . . . . Display USB Networking stats\r\n");
			debugDisplayOnly(" v . . . . . . . . . . . . . . . . Get Version information and system state info\r\n");
			debugDisplayOnly(" w <n> . . . . . . . . . . . . . . Set WebSocket verbosity level to <n> (0 - 4)\r\n");
			debugDisplayOnly(" ws [A | C]  . . . . . . . . . . . Set the websocket port to [A]ll or type-[C] only\r\n");
			debugDisplayOnly(" wstats  . . . . . . . . . . . . . Show websocket statistics\r\n");
			debugDisplayOnly(" wstrace [on | off]  . . . . . . . Show or set websocket trace state\r\n");
			debugDisplayOnly(" (copy, delete, rename)  . . . . . File commands e.g. copy file1 tmp\\file2   or   delete file1  (no wildcards)\r\n");
			debugDisplayOnly(" (dir, mkdir, rmdir) . . . . . . . Directory commands. Only dir has wildcard support e.g. dir install\\*\r\n");
			debugDisplayOnly(" tr . . . . . . .  . . . . . . . . Transaction manager counters\\*\r\n");
			break ;

		default:
			bHandled = FALSE ;
			break ;
	}
	return bHandled;
}

/***********************************************************************
*
*     FUNCTION
*
*         testSocketConnect
*
*     DESCRIPTION
*
*         Try to open a socket connection to the supplied IP address (IPv4)
*         if successful it reports success and closes the socket. If failed then
*         a failure is reported along with the error code.
*         If no ip is supplied it will try connecting to a google address
*         Currently we have no option to alter the port to connect to (80)
*
*     INPUTS
*
*		 pIP		- string representation of IP address eg "192.168.10.110"
*
*     OUTPUTS
*
*         none
*
***********************************************************************/
void testSocketConnect(char *pIP, cJSON *rspMsg)
{
	BOOL	bNoPing	= FALSE;
	STATUS status 	= NU_SUCCESS;
	INT itempSkt 	= NU_Socket(NU_FAMILY_IP, NU_TYPE_STREAM, NU_NONE ) ;
//	char strTemp[DEBUG_TEMP_STRINGLENGTH+1];
	char strJSON[DEBUG_TEMP_STRINGLENGTH+1];

	pIP = skipSpaces(pIP);				// start of IP address
	char *pEOA = skipNonspaces(pIP); 	// End of address
	char *pOpt = skipSpaces(pEOA);		// start of options
	*pEOA = 0;							// null terminate the end of the address.

	if((*pOpt != NULL) && (tolower(*pOpt) == 'n'))
		bNoPing = TRUE;


	struct addr_struct addrTemp ;
	if(itempSkt >= 0)
	{
		memset(&addrTemp, 0, sizeof(struct addr_struct)) ;
		addrTemp.family 	= NU_FAMILY_IP;
		addrTemp.port		= 80;
		if(!pIP || *pIP == 0)
		{
			addrTemp.id.is_ip_addrs[0] = 216;
			addrTemp.id.is_ip_addrs[1] = 58;
			addrTemp.id.is_ip_addrs[2] = 217;
			addrTemp.id.is_ip_addrs[3] = 110;
		}
		else
		{
			if(isdigit(*pIP))
				stringToIP4Array(addrTemp.id.is_ip_addrs, pIP);
			else
			{
				NU_HOSTENT *hentry;
				UINT8 *addr_list;
				hentry = NU_Get_IP_Node_By_Name(pIP, NU_FAMILY_IP, 0, &status);
				if(status == NU_SUCCESS)
				{
					// take the first found value
					addr_list = (UINT8 *)(*hentry->h_addr_list);
					addrTemp.id.is_ip_addrs[0] = addr_list[0];
					addrTemp.id.is_ip_addrs[1] = addr_list[1];
					addrTemp.id.is_ip_addrs[2] = addr_list[2];
					addrTemp.id.is_ip_addrs[3] = addr_list[3];
					if(rspMsg)
					{
						snprintf(strJSON, DEBUG_TEMP_STRINGLENGTH, "DNS Lookup found IP %d.%d.%d.%d for URL %s",
							addr_list[0],addr_list[1],addr_list[2],addr_list[3],pIP);
						cJSON_AddStringToObject(rspMsg, WS_KEY_CONNECT_DNSSTAT, strJSON);
					}
					else
					{
						debugDisplayOnly("\r\nDNS Lookup found IP %d.%d.%d.%d for URL %s\r\n",
							addr_list[0],addr_list[1],addr_list[2],addr_list[3],pIP);
					}

					NU_Free_Host_Entry(hentry) ;
				}
				else
				{
					if(rspMsg)
					{
						snprintf(strJSON, DEBUG_TEMP_STRINGLENGTH, "DNS Lookup failed for %s", pIP);
						cJSON_AddStringToObject(rspMsg, WS_KEY_CONNECT_DNSSTAT, strJSON);
					}
					else
						debugDisplayOnly("\r\nDNS Lookup failed for %s\r\n", pIP);
				}
			}
		}

		if(*((UINT32 *)addrTemp.id.is_ip_addrs) == 0)
		{
			if(rspMsg)
			{
				cJSON_AddStringToObject(rspMsg, WS_KEY_CONNECT_STAT, "Unable to connect to address 0.0.0.0 - DNS Failure?");
			}
			else
				debugDisplayOnly("Unable to connect to address 0.0.0.0 - DNS Failure?\r\n");
		}
		else
		{
			if(bNoPing || (NU_Ping(addrTemp.id.is_ip_addrs, 200) == NU_SUCCESS))
			{
				if(bNoPing)
					debugDisplayOnly("Not pinging address prior to test connection\r\n");

				status = NU_Connect(itempSkt, &addrTemp, 0) ;
				if(status >= 0)
				{
					if(rspMsg)
					{
						snprintf(strJSON, DEBUG_TEMP_STRINGLENGTH, "Test connection to %d.%d.%d.%d successful",
								addrTemp.id.is_ip_addrs[0],
								addrTemp.id.is_ip_addrs[1],
								addrTemp.id.is_ip_addrs[2],
								addrTemp.id.is_ip_addrs[3]);
						cJSON_AddStringToObject(rspMsg, WS_KEY_CONNECT_STAT, strJSON);
					}
					else
						debugDisplayOnly("Test connection to %d.%d.%d.%d successful\r\n",
							addrTemp.id.is_ip_addrs[0],
							addrTemp.id.is_ip_addrs[1],
							addrTemp.id.is_ip_addrs[2],
							addrTemp.id.is_ip_addrs[3]);


					NU_Close_Socket(itempSkt);
				}
				else
				{
					if(rspMsg)
					{
						snprintf(strJSON, DEBUG_TEMP_STRINGLENGTH, "ERROR Test connection to %d.%d.%d.%d failed [%d]",
								addrTemp.id.is_ip_addrs[0],
								addrTemp.id.is_ip_addrs[1],
								addrTemp.id.is_ip_addrs[2],
								addrTemp.id.is_ip_addrs[3],
								status);
						cJSON_AddStringToObject(rspMsg, WS_KEY_CONNECT_STAT, strJSON);
					}
					else
						debugDisplayOnly("ERROR Test connection to %d.%d.%d.%d failed [%d]\r\n",
							addrTemp.id.is_ip_addrs[0],
							addrTemp.id.is_ip_addrs[1],
							addrTemp.id.is_ip_addrs[2],
							addrTemp.id.is_ip_addrs[3],
							status);
				}
			}
			else
			{
				if(rspMsg)
				{
					cJSON_AddStringToObject(rspMsg, WS_KEY_CONNECT_STAT, "PING failed to this address, connection will not be attempted");
				}
				else
					debugDisplayOnly("PING failed to this address [%d.%d.%d.%d], connection will not be attempted\r\n",
						addrTemp.id.is_ip_addrs[0],
						addrTemp.id.is_ip_addrs[1],
						addrTemp.id.is_ip_addrs[2],
						addrTemp.id.is_ip_addrs[3]);
			}
		}
	}
	else // if skt < 0
	{
		//
		if(rspMsg)
		{
			snprintf(strJSON, DEBUG_TEMP_STRINGLENGTH, "Failed to create socket, nucleus returned %d",itempSkt);
			cJSON_AddStringToObject(rspMsg, WS_KEY_CONNECT_STAT, strJSON);
		}
		debugDisplayOnly("Failed to create socket, nucleus returned %d\r\n",itempSkt);
	}
}



/***********************************************************************
*
*     FUNCTION
*
*         debugConnectionsInUse
*
*     DESCRIPTION
*
*         return the number of valid debug socket connections, this routine also formats a string with the details of the sockets opened.
*
*     INPUTS
*
*         *str		string in whiuch to format the response
*
*     OUTPUTS
*
*         none
*
***********************************************************************/
UINT debugConnectionsInUse(char *str, size_t szLen)
{
	UINT inUse = 0, uPass ;
	for(uPass=0 ; uPass < CSD_DEBUG_MAX_CONNECTIONS ; uPass++)
		if(globalClientDebugSocket[uPass] != -1)
		{
			size_t szIntLen = snprintf(str, szLen, "%d, ", (int)globalClientDebugSocket[uPass]);
			szLen -= szIntLen ;
			str += szIntLen ;
			inUse++;
		}
	return inUse;
}

/***********************************************************************
*
*     FUNCTION
*
*         debugInitialize
*
*     DESCRIPTION
*
*         Initialize arrays etc for the debugTrace operation. Note that this routine tracks its use and will only allow execution once.
*
*     INPUTS
*
*         none
*
*     OUTPUTS
*
*         none
*
***********************************************************************/
VOID debugInitialize()
{
	int iPass;
	for(iPass=0 ; iPass < CSD_DEBUG_MAX_CONNECTIONS ; iPass++)
	{
		globalClientDebugSocket[iPass] = -1 ;
	}

	globalDebugInitialized = -1 ;
}

/***********************************************************************
 * debugTerminalDump
 *
 *
***********************************************************************/
void debugTerminalDumpToFile()
{
	const int MAX_BUFFER_SIZE = 0x7FFF;
	BOOL bAllocated = FALSE;
	char *bufferPtr;
	STATUS status;
    size_t length;
    char *path = "Logs";

    if(NU_Allocate_Memory(MEM_Cached, (void **)&bufferPtr, MAX_BUFFER_SIZE, NU_NO_SUSPEND) == NU_SUCCESS)
	{
    	bAllocated = TRUE;
    	*bufferPtr = 0 ;
		addDiagnosticsLog(bufferPtr, MAX_BUFFER_SIZE);
	}
	else
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  debugTerminalDump: %s\r\n", api_status_to_string_tbl[API_OUT_OF_MEMORY]);
        bufferPtr = "debugTerminalDump unable to allocate required memory\r\n";
    }

	length = strlen(bufferPtr);
	debugDisplayOnly("debugTerminalDumpToFile resulted in %u bytes of data\r\n", (UINT32)length);

	 if (length > 0 && length < MAX_BUFFER_SIZE)
			status = fnFileAppend(path, (CHAR *)log_names[LOG_DIAGNOSTICS], bufferPtr, length, true);

	//release memory
	if(bAllocated && ((status = NU_Deallocate_Memory(bufferPtr)) != NU_SUCCESS))
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  debugTerminalDump: %s\r\n", api_status_to_string_tbl[API_STATUS_DEALLOC_FAILED]);
    }

}

/***********************************************************************
 * on_DebugTerminalDumpReq
 *
 *
***********************************************************************/
void on_DebugTerminalDumpReq(UINT32 handle, cJSON *root)
{
    cJSON 				*rspMsg		=  cJSON_CreateObject();
    WSOX_Queue_Entry 	entry  		=  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "DebugTerminalDumpRsp");

    debugTerminalDumpToFile();

	addEntryToTxQueue(&entry, root, "  on_DebugTerminalDumpReq: Added response to tx queue.\r\n");
}

/***********************************************************************
 * addDiagnosticsLog
 *
 * Create the terminal dump file and then place the contents of that file
 * into the supplied buffer. Buffer size needs to be large enough to allow
 * it all in. Note that due to the odd requirements for this function
 * the supplied buffer may already have data within it so we have to find the
 * end of it and take this into account with the supplied file size.
 ***********************************************************************/
void addDiagnosticsLog(char *bufferPtr, UINT bufferSize)
{
#if 1
    NU_MEMORY_POOL 		*pnuMemPool = getMemoryPool(MEMSTATS_SYSTEMMEMORYPOOL);
	directDebugToFile(bufferPtr, bufferSize);
    autoDebugDump(NULL, pnuMemPool);
    directDebugToScreen();
#else
    strcat(bufferPtr, "Temporarily disabled\r\n");
#endif
}

/***********************************************************************
 * autoDebugDump
 *
 * run a user provided set of debugTerminal commands if provided or,
 * if none provided execute a default set.
***********************************************************************/
static INT globalAutoDebugDumpIter = 0;
void autoDebugDump(PDebugCommands p, NU_MEMORY_POOL *pool)
{
	// stop recursion if possible...
	if(globalAutoDebugDumpIter++ == 0)
	{
		PDebugCommands pCmds = (p == NULL) ? (PDebugCommands)globalDefaultTraceCmds : p ;
		while(pCmds->cmd)
		{
			BOOL bHandled = 0 ;
			debugDisplayOnly("\r\n CMD:%s  - %s\r\n", pCmds->cmd, pCmds->description);
			if(strlen(pCmds->cmd) == 1)
				bHandled = debugHandleInputSingleChar(pool, pCmds->cmd[0]);

			if(!bHandled)
				bHandled = debugHandleInputMultiChar(pool, pCmds->cmd);

			debugDisplayOnly("\r\n---------------------------------------------------------------------------------------------------------------\r\n");
			pCmds++;
		}
		globalAutoDebugDumpIter = 0 ;
	}
}

INT	globalDebugOutputSize = -1;
char *globalDebugOutputPtr = NULL;

/***********************************************************************
 * directDebugToFile
 *
 * Call this to redirect the debugDisplayOutput data to a file instead
 * of to the telnet session.
***********************************************************************/
STATUS directDebugToFile(char *bufferPtr, UINT bufferSize)
{
	STATUS status = NU_SUCCESS;
	char  semstat;

	semstat = OSAcquireSemaphore (DEBUG_PORT_SEM_ID, DEBUGTASK_SEMAPHORETIMEOUT);
	if(semstat == SUCCESSFUL)
	{
		globalDebugOutputSize = bufferSize;
		globalDebugOutputPtr = bufferPtr;
		semstat = OSReleaseSemaphore(DEBUG_PORT_SEM_ID);
		if(semstat != SUCCESSFUL)
		{
	        dbgTrace(DBG_LVL_ERROR,"Error: Failed to release Debug port semaphore in directDebugToFile: continuing anyway %d\n", semstat);
		}
	}
	else
	{
        dbgTrace(DBG_LVL_ERROR,"Error: Failed to acquire Debug port semaphore in directDebugToFile: %d\n", semstat);
		status = NU_RETURN_ERROR;
	}
	return status;
}

/***********************************************************************
 * directDebugToScreen
 *
 * Call this to return redirected debug output back to the socket form.
***********************************************************************/
void directDebugToScreen()
{
	char  semstat;

	if(globalDebugOutputSize != -1)
	{
		semstat = OSAcquireSemaphore (DEBUG_PORT_SEM_ID, DEBUGTASK_SEMAPHORETIMEOUT);
		if(semstat == SUCCESSFUL)
		{
			globalDebugOutputSize = -1 ;
			globalDebugOutputPtr = NULL;
			semstat = OSReleaseSemaphore(DEBUG_PORT_SEM_ID);
			if(semstat != SUCCESSFUL)
			{
		        dbgTrace(DBG_LVL_ERROR,"Error: Failed to release Debug port semaphore in directDebugToScreen: continuing anyway %d\n", semstat);
			}
		}
		else
		{
	        dbgTrace(DBG_LVL_ERROR,"Error: Failed to acquire Debug port semaphore in directDebugToScreen: %d\n", semstat);
		}
	}
}

/***********************************************************************
*
*     FUNCTION
*
*         debugSendString
*
*     DESCRIPTION
*
*         This is the output routine for the managed debug socket connections
*
*     INPUTS
*
*         char *data	string (null terminated) to be output to serial connection
*
*     OUTPUTS
*
*         status of result, NU_SUCCESS means no errors detected. This routine can fail if
*         the debug task is not initialized or if the semaphore is not available.
*
***********************************************************************/
STATUS debugSendString(char *data)
{
	STATUS 	status 		= NU_SUCCESS;
	int 	handled 	= 0;
	int 	iPass 		= 0 ;
	size_t	szToSend 	= 0 ;
	char 	*pData;
	char    semstat;

	// if not initialized return an error, debug task has not been started.
	if(!globalDebugInitialized)
		return NU_RETURN_ERROR ;

	// if the error task is writing we will not wait on a semaphore. instead we set the semstat flag to true and move on regardless
	if(!globalErrorWritingInProgress)
	{
		semstat = OSAcquireSemaphore (DEBUG_PORT_SEM_ID, DEBUGTASK_SEMAPHORETIMEOUT);
	}
	else
	{
		semstat = TRUE;
	}
	if(semstat == SUCCESSFUL)
	{
		if(globalDebugOutputSize != -1)
		{
			if ((globalDebugOutputPtr != NULL) && (globalDebugOutputSize > strlen(data)))
			{
				strcat(globalDebugOutputPtr, data);
				globalDebugOutputSize -= strlen(data);
			}
			handled++;
		}
		else
		{
			for(iPass=0 ; iPass < CSD_DEBUG_MAX_CONNECTIONS ; iPass++)
			{
				if(globalClientDebugSocket[iPass] != -1)
				{
					handled++ ;
					szToSend = strlen(data) ;
					pData = data;
					while(szToSend > 0)
					{

						INT iTimeout = 0 ;
						do
						{
							status = NU_Send(globalClientDebugSocket[iPass], pData, szToSend, 1) ;
							if(status == NU_WOULD_BLOCK)
							{
								NU_Sleep(1);
								iTimeout++;
							}
						} while((status == NU_WOULD_BLOCK) && (iTimeout < DEBUG_BLOCKSTALL_TIMEOUT));

						// if we have timedout try closing the socket an allowing user to restart.
						if((status < 0) || (iTimeout >= DEBUG_BLOCKSTALL_TIMEOUT))
						{
							if(status == NU_INVALID_TASK)
							{
								NU_Sleep(1);
							}
							else
							{
								NU_Close_Socket(globalClientDebugSocket[iPass]) ;
								globalClientDebugSocket[iPass] = -1 ;
								if(globalDebugConnectionCount > 0) globalDebugConnectionCount--;
								break ;
							}
						}
						else
						{
							szToSend -= (size_t)status ;
							if (szToSend > 0)
							{
								pData += (size_t)status;
								// must be some reason why all the data was not sent so wait a little while and try again.
								NU_Sleep(1);
							}
						}
					}
				}
			} // end of for
		}

		// only release semaphore if we are not in the error task crash dump mode
		if(!globalErrorWritingInProgress)
		{
			semstat = OSReleaseSemaphore(DEBUG_PORT_SEM_ID);
		}

		if(semstat != SUCCESSFUL)
		{
			fnDumpStringToSystemLogWithNum("Error: Failed to release Debug port semaphore in debugSendString", (int) semstat);
		}
	}
	else
	{
		fnDumpStringToSystemLogWithNum("Error: Failed to acquire Debug port semaphore in debugSendString", (int) semstat);
	}
	return (handled == 0) ? NU_RETURN_ERROR : status ;
}

/***********************************************************************
*
*     FUNCTION
*
*         debugPrintGetLANConfig
*
*     DESCRIPTION
*
*         Print the current network configuration for the supplied service name
*
*     INPUTS
*
*         char *if_name	 name of the service to print (ie "eth0" "net1")
*
*     OUTPUTS
*
*
***********************************************************************/
//extern ROUTE_NODE *RTAB4_Default_Route;

VOID debugPrintGetLANConfig(char *if_name)
{
    LanInfo lan_info;

    unsigned char res = fnGetLANConfig(if_name, &lan_info);

    debugDisplayOnly("\r\n GetLANConfig(\"%s\"):      result = %d\r\n", if_name, res);
    debugDisplayOnly("                            if_up  = %s\r\n", lan_info.if_up ? "true" : "false");
// display the required value rather than the actual value since the actual one has problems when the interface is not up.
//    debugDisplayOnly("                              dhcp = %s\r\n", NU_Ifconfig_Get_DHCP4_Enabled(if_name) == 1 ? "Enabled" : "Disabled");
    debugDisplayOnly("                              dhcp = %s\r\n", CMOSglobalNetworkInformationStructure.dhcpFlag == NIS_DHCP_ACTIVE ? "Enabled" : "Disabled");

    debugDisplayOnly("                           ip_addr = %d.%d.%d.%d\r\n", lan_info.ip_addr[0]
            , lan_info.ip_addr[1]
            , lan_info.ip_addr[2]
            , lan_info.ip_addr[3]);

    if(lan_info.if6_up == true)
    {
        debugDisplayOnly("                        ip_v6_addr = %02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X\r\n",
        		lan_info.ipv6_addr[0],lan_info.ipv6_addr[1],
        		lan_info.ipv6_addr[2],lan_info.ipv6_addr[3],
        		lan_info.ipv6_addr[4],lan_info.ipv6_addr[5],
        		lan_info.ipv6_addr[6],lan_info.ipv6_addr[7],
        		lan_info.ipv6_addr[8],lan_info.ipv6_addr[9],
        		lan_info.ipv6_addr[10],lan_info.ipv6_addr[11],
        		lan_info.ipv6_addr[12],lan_info.ipv6_addr[13],
        		lan_info.ipv6_addr[14],lan_info.ipv6_addr[15]);

        debugDisplayOnly("                      v6_link_addr = %02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X\r\n",
        		lan_info.link6_addr[0],lan_info.link6_addr[1],
        		lan_info.link6_addr[2],lan_info.link6_addr[3],
        		lan_info.link6_addr[4],lan_info.link6_addr[5],
        		lan_info.link6_addr[6],lan_info.link6_addr[7],
        		lan_info.link6_addr[8],lan_info.link6_addr[9],
        		lan_info.link6_addr[10],lan_info.link6_addr[11],
        		lan_info.link6_addr[12],lan_info.link6_addr[13],
        		lan_info.link6_addr[14],lan_info.link6_addr[15]);

        debugDisplayOnly("                        v6_gateway = %02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X\r\n",
        		globalDefaultIPv6Gateway[0], globalDefaultIPv6Gateway[1],
        		globalDefaultIPv6Gateway[2], globalDefaultIPv6Gateway[3],
        		globalDefaultIPv6Gateway[4], globalDefaultIPv6Gateway[5],
        		globalDefaultIPv6Gateway[6], globalDefaultIPv6Gateway[7],
        		globalDefaultIPv6Gateway[8], globalDefaultIPv6Gateway[9],
        		globalDefaultIPv6Gateway[10], globalDefaultIPv6Gateway[11],
        		globalDefaultIPv6Gateway[12], globalDefaultIPv6Gateway[13],
        		globalDefaultIPv6Gateway[14], globalDefaultIPv6Gateway[15]);

    }

    debugDisplayOnly("                           netmask = %d.%d.%d.%d\r\n", lan_info.netmask[0]
            , lan_info.netmask[1]
            , lan_info.netmask[2]
            , lan_info.netmask[3]);

    debugDisplayOnly("                      ipv4 gateway = %d.%d.%d.%d\r\n", globalDefaultIPv4Gateway[0]
            , globalDefaultIPv4Gateway[1]
            , globalDefaultIPv4Gateway[2]
            , globalDefaultIPv4Gateway[3]);

    if(RTAB4_Default_Route != NULL)
    {
		UINT8 *pAdds = NUCLEUS_RTAB4_ROUTE4 ;
		debugDisplayOnly("      RTAB Default Route (Gateway) = %d.%d.%d.%d\r\n",
				pAdds[3],
				pAdds[2],
				pAdds[1],
				pAdds[0]
				);
    }
    else
    {
		debugDisplayOnly("   NO RTAB Default Route (Gateway) = 0.0.0.0\r\n");
    }

    debugDisplayOnly("                           macaddr = %02X:%02X:%02X:%02X:%02X:%02X\r\n",
            lan_info.mac_addr[0]
            , lan_info.mac_addr[1]
            , lan_info.mac_addr[2]
            , lan_info.mac_addr[3]
            , lan_info.mac_addr[4]
            , lan_info.mac_addr[5]);

}



/***********************************************************************
*
*     FUNCTION
*
*         skipSpaces
*
*     DESCRIPTION
*
*         Take the supplied string and skipp all spaces up to either the end or the first non
*         white space char, return that position.
*
*     INPUTS
*
*         char *String the string to use
*
*     OUTPUTS
*		 char *val	the position of the first non-whitespace character
*
***********************************************************************/
char *skipSpaces(char *pStart)
{
	while((*pStart != 0) && ((*pStart == ' ') || (*pStart == '\t')) )
		pStart++;

	return pStart ;
}


/***********************************************************************
*
*     FUNCTION
*
*         skipNULLs
*
*     DESCRIPTION
*
*         Take the supplied memory pointer and skipp all NULL characters up to the point where
*         the supplied in pointer equals the supplied end pointer
*
*     INPUTS
*
*         char *String the string to use
*         char *pEnd the address of the last character in the memory block
*
*     OUTPUTS
*		 char *val	the position of the first non-NULL character
*
***********************************************************************/
char *skipNULLs(char *pStart, char *pEnd)
{
	while((pStart <= pEnd) && (*pStart == 0))
		pStart++;

	return pStart ;
}

/***********************************************************************
*
*     FUNCTION
*
*         skipNonspaces
*
*     DESCRIPTION
*
*         Skip all the non-whitespace characters (space tab and eol)
*
*     INPUTS
*
*         char *String  source string to use
*
*     OUTPUTS
*		char *Pos  the position of the first white space char or eol
*
***********************************************************************/
char *skipNonspaces(char *pStart)
{
	while((*pStart != 0) && (*pStart != ' ') && (*pStart != '\t'))
		pStart++;

	return pStart ;
}

/***********************************************************************
*
*     FUNCTION
*
*         skipToChar
*
*     DESCRIPTION
*
*         Skip all characters until the eol or the cupplied ch value is reached.
*
*     INPUTS
*
*         char *String  source string to use
*         char ch		value to look for
*
*     OUTPUTS
*		char *Pos  the position of the first match to supplied ch or eol
*
***********************************************************************/
char *skipToChar(char *pStart, char ch)
{
	while((*pStart != 0) && (*pStart != ch))
		pStart++;
	return pStart ;
}

/***********************************************************************
*
*     FUNCTION
*
*         skipToCommand
*
*     DESCRIPTION
*
*         Skip all the characters until eol or a '-' is detected
*
*     INPUTS
*
*         char *String  source string to use
*
*     OUTPUTS
*		char *Pos  the position of the first '-' or eol
*
***********************************************************************/
char *skipToCommand(char *pStart)
{
	while((*pStart != 0) && (*pStart != FILTER_COMMAND_CHAR))
		pStart++;

	return pStart ;
}

/***********************************************************************
*
*     FUNCTION
*
*         skipToCommand
*
*     DESCRIPTION
*
*         Skip all the characters until eol or the supplied character match is detected
*
*     INPUTS
*
*         char *String  source string to use
*
*     OUTPUTS
*		char *Pos  the position of the first '-' or eol
*
***********************************************************************/
char *findChar(char *str, char ch)
{
	while(str && (*str != 0) && (*str != ch))
		str++;
	return str;
}


/***********************************************************************
*
*     FUNCTION
*
*         getString
*
*     DESCRIPTION
*
*         Return the first complete word in the supplied string (ending in white space or eol)
*
*     INPUTS
*
*         pStart -  string to search
*         pStore -  space to store resultant word in
*         szMaxLen  space available in supplied store variable
*
*     OUTPUTS
*		char *		first character after found word in supplied start string.
*
***********************************************************************/
char *getString(char *pStart, char *pStore, size_t szMaxLen)
{
	*pStore = 0 ;
	pStart = skipSpaces(pStart) ;
	size_t szLen = 0;
	char *pEnd 	= skipNonspaces(pStart) ;
	char *pEndPad = skipSpaces(pEnd) ;

	if((szLen = (pEnd - pStart)) < szMaxLen)
	{
		strncpy(pStore, pStart, szLen);
		pStore[szLen]=0;
	}

	return pEndPad;
}


/***********************************************************************
*
*     FUNCTION
*
*         stripCRLF
*
*     DESCRIPTION
*
*         delete any CR/LF characters - WARNING this alters INPUT string
*
*     INPUTS
*
*         strS		String to strip CRLF characters from
*
*     OUTPUTS
*     		BOOL	true if come characters were deleted
*
***********************************************************************/
BOOL stripCRLF(char *strDest, char *strSource, size_t szMaxLen)
{
	BOOL bRes = FALSE;

	int iPass ;

	for(iPass=0 ; (iPass < szMaxLen) && (*strSource != 0) ; strSource++)
	{
		if((*strSource != DEBUG_CR) && (*strSource != DEBUG_LF))
		{
			*strDest++ = *strSource ;
			iPass++;
		}
		else
		{
			bRes = TRUE;
		}
	}
	*strDest = 0 ;
	return bRes ;
}

/***********************************************************************
*
*     FUNCTION
*
*         deUniCodeString
*
*     DESCRIPTION
*
*         oversimplistic routine to simply compress source by removing every other byte
*         rudimentary check on source to see if every other byte is 0 prior to operation
*
*     INPUTS
*
*         strSource	string to operate upon, result will overwrite this data.
*         szLen		pointer to inbound string size, this will return size of returned string
*
*     OUTPUTS
*
***********************************************************************/
void deUniCodeString(char *strSrc, size_t *szLen)
{
	char *strDest = strSrc ;
	BOOL bIsUniCode = TRUE ;
	size_t szPass;
	size_t szNewLen ;
	for(szPass=1 ; bIsUniCode && (szPass < *szLen) ; szPass+=2)
	{
		bIsUniCode &= (strSrc[szPass] == NULL);
		if((strSrc[szPass] == NULL) && (strSrc[szPass+1] == NULL))
			break ;
	}

	if(bIsUniCode)
	{
		for(szPass=0, szNewLen = 0 ; szPass < *szLen ; szPass+=2)
		{
			*strDest++ = strSrc[szPass];
			szNewLen++;
		}
		*strDest = 0 ;
		*szLen = szNewLen;
	}
}

/***********************************************************************
*
*     FUNCTION
*
*         handleSyslogDump
*
*     DESCRIPTION
*
*         Dump out the syslog information
*
*     INPUTS
*
*         strCommand	the command work
*         pPos			the remainder of the command line instigating this call.
*
*     OUTPUTS
*
***********************************************************************/
#define DOUBLE_SYSLOGSTRING_SIZE		MAX_SYSLOGSTRING_SIZE*2
#define	THROTTLE_PER_LINES				50
void handleSyslogDump(char *strCommand, char *pPos)
{
	char	strBuf[DOUBLE_SYSLOGSTRING_SIZE+1]; // strBuf holds strStr + more data so enlarge it.
	char	strStr[MAX_SYSLOGSTRING_SIZE+1];
	sSyslogFilters	structFilters ;
	INT iThrottle = 0;

	getSyslogFilters(&structFilters, pPos);

	int iPass ;
	size_t szLen;
	BOOL bBlock = FALSE ;

	for(iPass=0 ; (iPass < INTERTASK_LOG_SIZE) && (pIntertaskMsgLog[iPass].lwTimeStamp != 0xFFFFFFFF) ; iPass++)
	{
		INT iContinuations = 0 ;
		INT iSubPass;
		bBlock = FALSE ;
		// Allow block blocking here at a coding level, feel free to change this dependent upon what you are looking for

		// end of blocking section
		strBuf[0] = 0;
		strStr[0] = 0;
		BOOL bContinue = TRUE ;
		switch(pIntertaskMsgLog[iPass].bType)
		{
			case SYSLOG_TEXT:
				memcpy(strStr, (CHAR *)pIntertaskMsgLog[iPass].pUnionData, SYSLOG_PAYLOAD_BYTES);
				// force terminate after the payload limit.
				strStr[SYSLOG_PAYLOAD_BYTES] = 0;
				szLen =  SYSLOG_PAYLOAD_BYTES ;
				deUniCodeString(strStr, &szLen) ;

				for(iSubPass = iPass+1 ; bContinue && (iSubPass < (INTERTASK_LOG_SIZE-1)) ; iSubPass++)
				{
					if( (pIntertaskMsgLog[iSubPass].bType == SYSLOG_TEXT_CONTINUE))
					{
						memcpy(strStr + szLen, (CHAR *)pIntertaskMsgLog[iSubPass].pUnionData, SYSLOG_PAYLOAD_BYTES);
						szLen +=  SYSLOG_PAYLOAD_BYTES ;
						strStr[szLen]=0;
						deUniCodeString(strStr, &szLen) ;
						stripCRLF(strStr, strStr, szLen);
						szLen = strlen(strStr) ;
						iContinuations++;
					}
					else
						bContinue = FALSE;
				}
//				deUniCodeString(strStr, &szLen) ;
				stripCRLF(strStr, strStr, szLen);
				szLen = strlen(strStr) ;
				break;

			case SYSLOG_TEXT_CONTINUE:
				bBlock = TRUE;
				strStr[0]=0;
				break;

			case SYSLOG_ITM:
				if(pIntertaskMsgLog[iPass].bSourceID == BJCTRL)
				{
					decodeBJCTRLResponse(&(pIntertaskMsgLog[iPass]), strStr, MAX_SYSLOGSTRING_SIZE) ;
					break ;
				}
				// NO BREAK
			case SYSLOG_EVENT:
			case SYSLOG_IBTN_DRV:
			case SYSLOG_IBUTTON:
			case SYSLOG_EDMMSG:
				binaryToString(strStr, (UINT8 *)pIntertaskMsgLog[iPass].pUnionData, SYSLOG_PAYLOAD_BYTES);
				if(strlen(strStr)!= (SYSLOG_PAYLOAD_BYTES * 2))
					*strStr =  *(char *)pIntertaskMsgLog[iPass].pUnionData;
			break ;

			default:
				bBlock = TRUE;
//				strStr[0]='?';
//				memcpy(strStr+1, pIntertaskMsgLog[iPass].pUnionData, MAX_SYSLOGSTRING_SIZE) ;
				break;
		}

		if(!bBlock)
		{
			snprintf(strBuf, DOUBLE_SYSLOGSTRING_SIZE, "%010u %s %s >> %s %s  ..%1d  %s\r\n",
					(UINT32)EndianSwap32(pIntertaskMsgLog[iPass].lwTimeStamp),
					syslogTypeToString(pIntertaskMsgLog[iPass].bType),
					syslogSourceDestToString(pIntertaskMsgLog[iPass].bSourceID),
					syslogSourceDestToString(pIntertaskMsgLog[iPass].bDestID),
					msgIDToString(pIntertaskMsgLog[iPass].bMsgID),
					iContinuations,
					strStr);

			BOOL	bOutputIt = (strBuf[0] != 0) ;
			UINT	uFCount = 0;
			BOOL	bIntermediate ;
			// Only include lines that contains text included in any of the filters...
			if(structFilters.inCount != 0)
			{
				bIntermediate = FALSE ;
				for(uFCount=0 ; uFCount < structFilters.inCount ; uFCount++)
					 bIntermediate |= (strstr(strBuf, structFilters.FilterTextIn[uFCount]) != NULL) ;
				bOutputIt &= bIntermediate ;
			}

			// exclude any line that has text included in any of these filters.
			if(structFilters.outCount != 0)
			{
				BOOL bShouldBeExcluded = FALSE ;
				for(uFCount=0 ; uFCount < structFilters.outCount ; uFCount++)
					bShouldBeExcluded |= (strstr(strBuf, structFilters.FilterTextOut[uFCount]) != NULL) ;

				bOutputIt &= !bShouldBeExcluded ;
			}

			if(bOutputIt)
			{
				if((iThrottle++ % THROTTLE_PER_LINES) == 0)
				{
					NU_Sleep(10) ;
				}
				debugSendString(strBuf) ;
			}
		}
	}
}


/***********************************************************************
*
*     FUNCTION
*
*         decodeBJCTRLResponse
*
*     DESCRIPTION
*
*         linke printf but onl to our debug screen
*
*     INPUTS
*
*         structure like a normal printf statement
*
*     OUTPUTS
*
***********************************************************************/
BOOL decodeBJCTRLResponse(INTERTASK_LOG_ENTRY *pLogEntry, char *strStr, size_t sz)
{
	BOOL bResp = FALSE;
	strStr[0] = 0;
	pBJCTRLData bj = (pBJCTRLData)pLogEntry->pUnionData ;
	if((pLogEntry->bMsgID >= kMinBJCTRLRespID) && (pLogEntry->bMsgID <= kMaxBJCTRLRespID))
	{
		bResp = TRUE ;
		int iOffset = pLogEntry->bMsgID - kMinBJCTRLRespID;
		switch(pLogEntry->bMsgID)
		{
			case BJRESP_UNSOLICITED:
			case BJRESP_SENSORDATA:
			case BJRESP_PRINTHEADDATA:
			case BJRESP_INKTANKDATA:
				if(bj->u1 >= kStatusDescriptionsSize)
					bj->u1 = 0;
				snprintf(strStr, sz, "%s %s  Error %08X", kBJCTRLRespIDStrings[iOffset], kStatusDescriptions[bj->u1], bj->u2) ;
				break ;

			case BJRESP_PMSWVER:
				if(bj->u1 >= kStatusDescriptionsSize)
					bj->u1 = 0;
				snprintf(strStr, sz, "%s %s  Error %08X  Version %08X", kBJCTRLRespIDStrings[iOffset], kStatusDescriptions[bj->u1], bj->u2, bj->u3) ;
				break ;

			case BJRESP_INIT:
				if(bj->u1 >= kStatusDescriptionsSize)
					bj->u1 = 0;
				snprintf(strStr, sz, "%s %s  Error %08X  Maint %08X", kBJCTRLRespIDStrings[iOffset], kStatusDescriptions[bj->u1], bj->u2, bj->u3) ;
				break ;

			default:
				snprintf(strStr, sz, "UNPARSED %08X %08X %08X %08X", bj->u1, bj->u2, bj->u3, bj->u4) ;
				break ;
		}
	}

	return bResp ;
}



/***********************************************************************
*
*     FUNCTION
*
*         debugDisplayOnly
*
*     DESCRIPTION
*
*         linke printf but onl to our debug screen
*
*     INPUTS
*
*         structure like a normal printf statement
*
*     OUTPUTS
*
***********************************************************************/
void debugDisplayOnly(const char *fmt, ...)
{
	#define DBGTRACE_BUF_SIZE  1800
	char errStartBuf[DBGTRACE_BUF_SIZE];
	char *errBuf = errStartBuf;
	BOOL inHISR;

	inHISR = !OSRunningInTask();
	if(inHISR)
	{// assume in HISR for logging purposes
		strcpy(errStartBuf, HISR_ID_TAG);
		errBuf = errStartBuf + strlen(HISR_ID_TAG) ;
	}

	va_list argp;
	va_start(argp, fmt);
	vsnprintf(errBuf, DBGTRACE_BUF_SIZE ,fmt, argp);

	if(inHISR)
		debugQueueString(errStartBuf);
	else
		debugSendString(errStartBuf) ;

	va_end(argp);
}

/***********************************************************************
*
*     FUNCTION
*
*         msgIDToString
*
*     DESCRIPTION
*
*         Return a string representation of the Message ID value
*
*     INPUTS
*
*         id		syslog msg type
*
*
*     OUTPUTS
*     	*char		string representation the type or UNKNOWN if the type is not found.
*
***********************************************************************/
#define DEBUG_TYPESTRING_MAXSIZE	27
char globalTypeIDString[DEBUG_TYPESTRING_MAXSIZE+1] ;
char *msgIDToString(UINT8 id)
{
	char *pRes = globalTypeIDString ;
	switch(id)
	{
		case CF_SET_MODE:		pRes = "              CF_SET_MODE";			break ;
		case CP_SET_MODE:		pRes = "              CP_SET_MODE";			break ;
		case OIT_EVENT:			pRes = "                OIT_EVENT";			break ;
		case OIT_REC_EVENT:		pRes = "            OIT_REC_EVENT";			break ;
		case LCD_CLEAR_DISPLAY:	pRes = "        LCD_CLEAR_DISPLAY";			break ;

		default:
			snprintf(pRes, DEBUG_TYPESTRING_MAXSIZE, "                       %02X", (UINT)id) ;
	}
	return pRes;
}

/***********************************************************************
*
*     FUNCTION
*
*         syslogTypeToString
*
*     DESCRIPTION
*
*         Return a string representation of the source/destination byte types for the syslog
*
*     INPUTS
*
*         uc		syslog byte type
*
*
*     OUTPUTS
*     	*char		string representation the type or UNKNOWN if the type is not found.
*
***********************************************************************/
char globalSource2Dest[15];
char *syslogSourceDestToString(unsigned char uc)
{
	char *pRes 				 = globalSource2Dest ;

	switch(uc)
	{
		case SYS:				pRes = "SYS        "; 	break;
		case CM:				pRes = "CM         ";	break;
		case BJCTRL:			pRes = "BJCTRL     ";	break;
		case BJSTATUS:			pRes = "BJSTATUS   ";	break;
		case OIT:				pRes = "OIT        ";	break;
		case SCM:				pRes = "SCM        ";	break;
		case KEYTASK:			pRes = "KEYTASK    ";	break;
		case LCDTASK:			pRes = "LCDTASK    ";	break;
		case CLCD:				pRes = "CLCD       ";	break;
		case PSOC:				pRes = "PSOC       ";	break;
		case CIBUTTON:			pRes = "CIBUTTON   ";	break;
		case DISTTASK:			pRes = "DISTTASK   ";	break;
		case PLAT:				pRes = "PLAT       ";	break;
		case CPLAT:				pRes = "CPLAT      ";	break;
		case PBPTASK:			pRes = "PBPTASK    ";	break;
		case FDRMCPTASK:		pRes = "FFDRMCPTASK";	break;
//		case ILANTASK:			pRes = "ILANTASK   ";	break;
		case NETMONTASK:		pRes = "NETMONTASK ";	break;
		case WSRXSRVTASK:		pRes = "WSRXSRVTASK"; 	break;
		case DEBUGTASK:			pRes = "DEBUGTASK  ";	break;
		case WDTTASK:			pRes = "WDTASK     ";	break;
		case DEBUGLSTNR:		pRes = "DEBUGLSTNR "; 	break;
		case WSTXSRVTASK:		pRes = "WSTXSRVTASK"; 	break;
		case SL_IBTN_DRVR:		pRes = "IBTN_DRVR  "; 	break;
		case SL_IBTN_COMMAND:	pRes = "IBTN_CMD   "; 	break;
		case SL_IBTN_REL_SEQ:	pRes = "IBTN_RELSEQ"; 	break;
		case SL_IBTN_CRC_OK:	pRes = "IBTN_CRC_OK"; 	break;
		case SL_IBTN_CRC_FAIL:	pRes = "IBTNCRCFAIL"; 	break;
		case SL_IBTN_LEN:		pRes = "IBTNLEN    "; 	break;
		case MCP_CLASS_DRVR:	pRes = "MCPCLASSDRV";	break;

		default:
			snprintf(globalSource2Dest, 15, "UNKNOWN[%02X]", (INT)uc);
			break;
	}
	return pRes;
}

/***********************************************************************
*
*     FUNCTION
*
*         timerIDToNameString
*
*     DESCRIPTION
*
*         Return a string representation of the supplied timer ID
*
*     INPUTS
*
*         uc		timer byte type
*
*
*     OUTPUTS
*     	*char		string representation the TIMER
*
***********************************************************************/
char *timerIDToNameString(unsigned char uc)
{
	char *pRes = "UNKNOWN_TIMER" ;
	switch(uc)
	{
		case MAILRUN_CMPLT_TIMER4: 				pRes = "MAILRUN_CMPLT_TIMER4" ; 			break;
		case KEY_SCAN_TIMER: 					pRes = "KEY_SCAN_TIMER" ; 					break;
		case BJStatusTimerID:					pRes = "BJStatusTimerID" ; 					break;
		case SYS_MESSAGE_TIMER: 				pRes = "SYS_MESSAGE_TIMER" ; 				break;
		case SCREEN_CHANGE_TIMER: 				pRes = "SCREEN_CHANGE_TIMER" ; 				break;
		case USER_TICK_TIMER: 					pRes = "USER_TICK_TIMER" ;			 		break;
		case OIT_DEADMAN_TIMER: 				pRes = "OIT_DEADMAN_TIMER" ;		 		break;
		case OIT_MESSAGE_TIMER: 				pRes = "OIT_MESSAGE_TIMER" ; 				break;
		case SCREEN_TICK_TIMER: 				pRes = "SCREEN_TICK_TIMER" ; 				break;
		case MAILRUN_CMPLT_TIMER_ID: 			pRes = "MAILRUN_CMPLT_TIMER_ID" ; 			break;
		case PLAT_MESSAGE_TIMER: 				pRes = "PLAT_MESSAGE_TIMER" ; 				break;
		case PSOC_SIGNAL_TIMER: 				pRes = "PSOC_SIGNAL_TIMER" ; 				break;
		case NORMAL_PRESET_TIMER: 				pRes = "NORMAL_PRESET_TIMER" ; 				break;
		case SLEEP_TIMER: 						pRes = "SLEEP_TIMER" ; 						break;
		case ASIC_UART_TIMER: 					pRes = "ASIC_UART_TIMER" ;					break;
		case ALARM_TIMER: 						pRes = "ALARM_TIMER" ; 						break;
		case SYS_THRUPUT_TIMER_40: 				pRes = "SYS_THRUPUT_TIMER_40" ; 			break;
		case SYS_THRUPUT_TIMER_30: 				pRes = "SYS_THRUPUT_TIMER_30" ; 			break;
		case SYS_THRUPUT_TIMER_20: 				pRes = "SYS_THRUPUT_TIMER_20" ; 			break;
//		case DLS_FTP_TIMER: 					pRes = "DLS_FTP_TIMER" ; 					break;
		case PLAT_POLLING_TIMER: 				pRes = "PLAT_POLLING_TIMER" ;			 	break;
		case PLAT_LOCALIZATION_TIMER: 			pRes = "PLAT_LOCALIZATION_TIMER" ;		 	break;
		case SHORT_TICK_TIMER: 					pRes = "SHORT_TICK_TIMER" ; 				break;
		//case RATE_ZERO_WEIGHT_TIMER  (unsigned char) 23
		case TIME_SYNC_WAIT_TIMER: 				pRes = "TIME_SYNC_WAIT_TIMER" ; 			break;
		case FP_FEEDER_QUERY_SENSOR_TIMER: 		pRes = "FP_FEEDER_QUERY_SENSOR_TIMER" ; 	break;
		case FP_FEEDER_STOP_TIMER: 				pRes = "FP_FEEDER_STOP_TIMER" ; 			break;
		case MAILRUN_DELAY_TRANSPORT_STOP: 		pRes = "MAILRUN_DELAY_TRANSPORT_STOP" ; 	break;
		case FP_FEEDER_ROLLER_STOP_TIMER: 		pRes = "FP_FEEDER_ROLLER_STOP_TIMER" ; 		break;
		case FP_FEEDER_MAIL_INACTIVITY_TIMER: 	pRes = "FP_FEEDER_MAIL_INACTIVITY_TIMER" ;	break;
		case FP_MAINTENANCE_STOP_TIMER: 		pRes = "FP_MAINTENANCE_STOP_TIMER" ; 		break;
		case SYS_HEARTBEAT_TIMER: 				pRes = "SYS_HEARTBEAT_TIMER" ; 				break;
		case FP_FEEDER_START_TIMER: 			pRes = "FP_FEEDER_START_TIMER" ; 			break;
		case FP_FEEDER_MSG_TIMER: 				pRes = "FP_FEEDER_MSG_TIMER" ; 				break;
//		case TRM_UPLOAD_TIMER: 					pRes = "TRM_UPLOAD_TIMER" ; 				break;
//Removed by RAM not to display EMD ->EMD 000000000 in syslog
//		case EDM_STATUS_TIMER: 					pRes = "EDM_STATUS_TIMER" ; 				break;
	}
	return pRes ;
}




/***********************************************************************
*
*     FUNCTION
*
*         syslogTypeToString
*
*     DESCRIPTION
*
*         Return a string representation of the source/destination byte types for the syslog
*
*     INPUTS
*
*         uc		syslog byte type
*
*
*     OUTPUTS
*     	*char		string representation the type or UNKNOWN if the type is not found.
*
***********************************************************************/
char *syslogTypeToString(unsigned char uc)
{
	char *pRes 					 = "UNKNOWN         ";

	switch(uc)
	{
	case SYSLOG_TEXT:		    pRes = "SYSLOG_TEXT     "; 	break;
	case SYSLOG_ITM:		    pRes = "SYSLOG_ITM      ";	break;
	case SYSLOG_EVENT:		    pRes = "SYSLOG_EVENT    ";	break;
	case SYSLOG_IBTN_DRV:	    pRes = "SYSLOG_IBTN_DRV ";	break;
	case SYSLOG_IBUTTON:	    pRes = "SYSLOG_IBUTTON  ";	break;
	case SL_IBTN_DRVR:		    pRes = "SL_IBTN_DRVR    ";	break;
	case SL_IBTN_COMMAND:	    pRes = "SL_IBTN_COMMAND ";	break;
	case SL_IBTN_REL_SEQ:	    pRes = "SL_IBTN_REL_SEQ ";	break;
	case SL_IBTN_CRC_OK:	    pRes = "SL_IBTN_CRC_OK  ";	break;
	case SL_IBTN_CRC_FAIL:	    pRes = "SL_IBTN_CRC_FAIL";	break;
	case SL_IBTN_LEN:		    pRes = "SL_IBTN_LEN     ";	break;
	case SYSLOG_TEXT_CONTINUE:	pRes = "SYSLOG_TEXT>>   "; 	break;
	}
	return pRes;
}

/***********************************************************************
*
*     FUNCTION
*
*         binaryToString
*
*     DESCRIPTION
*
*         Fill the supplied strDest buffer with an ASCII representation of the supplied buffer
*
*     INPUTS
*
*         strDest		destination buffer, the user is responsible for ensuring it has enough space
*         pSrc			buffer of binary data to convert
*         szLen			size of the BINARY buffer (destination must be this *2 + 1)
*
*     OUTPUTS
*
***********************************************************************/
void binaryToString(char *strDest, UINT8 *pSrc, size_t szLen)
{
	size_t szPass;
	for(szPass=0 ; szPass < szLen ; szPass++, pSrc++)
	{
		*strDest++ = NIBBLETOASCII((*pSrc >> 4)) ;
		*strDest++ = NIBBLETOASCII((*pSrc & 0x0F));
	}

	*strDest = 0 ;
}


/***********************************************************************
*
*     FUNCTION
*
*         getSyslogFilters
*
*     DESCRIPTION
*
*         get the options to the syslog dump command and place the strings in the supplied structure
*         this routine keeps working until it gets to the end of the line
*
*     INPUTS
*
*         pFilters		Structure containing the string for inclusion and exclusion
*         pParams		pointer after command
*
*     OUTPUTS
*
***********************************************************************/
void getSyslogFilters(sSyslogFilters *pFilters, char *pParams)
{
	char strCmd[50];
	char *pEndCmd ;
	memset(pFilters, 0, sizeof(sSyslogFilters));
	while(*pParams != 0)
	{
		pParams = skipToCommand(pParams);
		pParams = getString(pParams, strCmd, 50) ;
		pParams = skipSpaces(pParams) ;
		pEndCmd = skipToCommand(pParams);
		if((strncmp(strCmd, FILTER_CMD_INCLUDE, strlen(FILTER_CMD_INCLUDE)) == 0) && (pFilters->inCount < FILTER_MAXELEMENTS))
		{
			strncpy(pFilters->FilterTextIn[pFilters->inCount], pParams, (size_t)(pEndCmd - pParams));
			pFilters->FilterTextIn[pFilters->inCount][pEndCmd - pParams] = 0;
			pFilters->inCount++;
		}
		else if((strncmp(strCmd, FILTER_CMD_EXCLUDE, strlen(FILTER_CMD_EXCLUDE)) == 0) && (pFilters->outCount < FILTER_MAXELEMENTS))
		{
			strncpy(pFilters->FilterTextOut[pFilters->outCount], pParams, (size_t)(pEndCmd - pParams));
			pFilters->FilterTextOut[pFilters->outCount][pEndCmd - pParams] = 0;
			pFilters->outCount++;
		}

		pParams = pEndCmd;
	}
}

/***********************************************************************
*
*     FUNCTION
*
*         dumpCmStatus
*
*     DESCRIPTION
*
*         Dump the contents of the global cmStatus variable, this contains a lot of run-time system relevant data
*
*     INPUTS
*
*
*     OUTPUTS
*
***********************************************************************/
void dumpCmStatusStructure(tCmStatus *pStruct)
{
	char str[MAX_SYSLOGSTRING_SIZE+1];

	snprintf(str, MAX_SYSLOGSTRING_SIZE, "\r\n>>>>>  Dump of the cmStatus structure --\r\n\r\n") ;
	debugSendString(str) ;

	snprintf(str, MAX_SYSLOGSTRING_SIZE, "MREventPend %02lX     BobPrevErr %02X            BobErr %02X              cmErr %02lX             testPrt %01X   pendingRpl %01X\r\n",
			pStruct->mailRunEvtPending,
			pStruct->bobPreErrCode,
			pStruct->bobErrCode,
			pStruct->cmErr,
			pStruct->tstPrt ? 1 : 0,
			pStruct->pendingRpl ? 1 : 0) ;
	debugSendString(str) ;

	snprintf(str, MAX_SYSLOGSTRING_SIZE, "   mtncType %02X      topMargin %02X   topMarginOffset %02X        rightMargin %02X   rightMarginOffset %02X   opMode %s\r\n",
			pStruct->mtncType,
			pStruct->topMargin,
			pStruct->topMarginOffset,
			pStruct->rightMargin,
			pStruct->rightMarginOffset,
			kopModeDescriptions[pStruct->opMode]) ;
	debugSendString(str) ;

	snprintf(str, MAX_SYSLOGSTRING_SIZE, "   cvrState %02X    jamLvrState %02X           pmSwVer %08lX\r\n",
			pStruct->cvrState,
			pStruct->jamLvrState,
			pStruct->pmSwVer) ;
	debugSendString(str) ;


	snprintf(str, MAX_SYSLOGSTRING_SIZE, "SysInitMode %02X%02X fdrErrStatus %02X            fdrErr %02X      fdrErrJamCode %02X             fdrMode %02X    tapesNum %02X\r\n",
			pStruct->sysInitMode[0],pStruct->sysInitMode[1],
			pStruct->fdrErrState.bStatus,pStruct->fdrErrState.bErr,pStruct->fdrErrState.bJamCode,
			pStruct->fdrMode,
			pStruct->tapesNum);
	debugSendString(str) ;

	snprintf(str, MAX_SYSLOGSTRING_SIZE, "totPieceCount %02X  stopPressed %02X         mediaType %02x         fIsRunning %s\r\n",
			pStruct->totalPieceCount,
			pStruct->stopPressed,
			pStruct->bMediaType,
			pStruct->fIsRunning ? "YES" : "NO");
	debugSendString(str) ;

	// emOpMode state
	snprintf(str, MAX_SYSLOGSTRING_SIZE, "\r\nError State -----------------------------\r\n") ;
	debugSendString(str) ;
	snprintf(str, MAX_SYSLOGSTRING_SIZE, "\r\n   Status   FatalErr PaperErr PHeadErr InkErr  InkTnkEr WasteErr TapeErr\r\n") ;
	debugSendString(str) ;
	snprintf(str, MAX_SYSLOGSTRING_SIZE, "%8lX %8lX %8lX %8lX %8lX %8lX %8lX %8lX\r\n\r\n",
			pStruct->pmErrState.lwStatus,
			pStruct->pmErrState.lwFatalErr,
			pStruct->pmErrState.lwPaperErr,
			pStruct->pmErrState.lwPheadErr,
			pStruct->pmErrState.lwInkErr,
			pStruct->pmErrState.lwInkTankErr,
			pStruct->pmErrState.lwWasteTankErr,
			pStruct->pmErrState.lwTapeErr) ;
	debugSendString(str) ;

// Sensor Response info (snsrState)
	snprintf(str, MAX_SYSLOGSTRING_SIZE, "\r\nSensor Data -----------------------------\r\n") ;
	debugSendString(str) ;
	snprintf(str, MAX_SYSLOGSTRING_SIZE, "\r\n  S1  S2  S3  Jam  Top  Cap  Pump  Print  PVolt  HeadVoltage %u\r\n", pStruct->snsrState.HeadVolt) ;
	debugSendString(str) ;
	snprintf(str, MAX_SYSLOGSTRING_SIZE,     "  %d   %d   %d    %d    %d    %d     %d     %d    %s    HeadTemp    %3.1f C\r\n\r\n",
			(pStruct->snsrState.wSensorData & 0x001) ? 1 : 0,
			(pStruct->snsrState.wSensorData & 0x002) ? 1 : 0,
			(pStruct->snsrState.wSensorData & 0x004) ? 1 : 0,
			(pStruct->snsrState.wSensorData & 0x008) ? 1 : 0,
			(pStruct->snsrState.wSensorData & 0x010) ? 1 : 0,
			(pStruct->snsrState.wSensorData & 0x020) ? 1 : 0,
			(pStruct->snsrState.wSensorData & 0x080) ? 1 : 0,
			(pStruct->snsrState.wSensorData & 0x100) ? 1 : 0,
			(pStruct->snsrState.wSensorData & 0x200) ? "ON" : "OFF",
			(double)(pStruct->snsrState.wHeadTemp)/10) ;
	debugSendString(str) ;

	snprintf(str, MAX_SYSLOGSTRING_SIZE, "\r\nPrintHead Data --------------------------\r\n") ;
	debugSendString(str) ;
	snprintf(str, MAX_SYSLOGSTRING_SIZE, "\r\n  ID      SNum   ManuDate   Update      PrintSerNum        Date    DotCount\r\n") ;
	debugSendString(str) ;
	//									       ID            SNum         ManuDate       Update         PrintSerNum                    Date             DotCount
	snprintf(str, MAX_SYSLOGSTRING_SIZE, "  %02X%02X  %02X%02X%02X%02X  %02X%02X%02X      %02X          %c%c%c%c%c%c%c%c%c       %02X%02X%02X   %02X%02X%02X%02X%02X%02X\r\n",
			pStruct->prtHeadData.bId[0], pStruct->prtHeadData.bId[1],
			pStruct->prtHeadData.bSerialNumber[0],pStruct->prtHeadData.bSerialNumber[1], pStruct->prtHeadData.bSerialNumber[2], pStruct->prtHeadData.bSerialNumber[3],
			pStruct->prtHeadData.bManufactureDate[0],pStruct->prtHeadData.bManufactureDate[1], pStruct->prtHeadData.bManufactureDate[2],
			pStruct->prtHeadData.bUpdate[0],
			pStruct->prtHeadData.bPrinterSerialNumber[0], pStruct->prtHeadData.bPrinterSerialNumber[1],pStruct->prtHeadData.bPrinterSerialNumber[2],
			pStruct->prtHeadData.bPrinterSerialNumber[3], pStruct->prtHeadData.bPrinterSerialNumber[4],pStruct->prtHeadData.bPrinterSerialNumber[5],
			pStruct->prtHeadData.bPrinterSerialNumber[6], pStruct->prtHeadData.bPrinterSerialNumber[7],pStruct->prtHeadData.bPrinterSerialNumber[8],
			pStruct->prtHeadData.bInstallDate[0],pStruct->prtHeadData.bInstallDate[1],pStruct->prtHeadData.bInstallDate[2],
			pStruct->prtHeadData.bDotCount[0],pStruct->prtHeadData.bDotCount[1],pStruct->prtHeadData.bDotCount[2],
			pStruct->prtHeadData.bDotCount[3],pStruct->prtHeadData.bDotCount[4],pStruct->prtHeadData.bDotCount[5]) ;
	debugSendString(str) ;

	snprintf(str, MAX_SYSLOGSTRING_SIZE, "\r\nInstallCount   TankColor  TankCount  WipeCount \r\n") ;
	debugSendString(str) ;
	snprintf(str, MAX_SYSLOGSTRING_SIZE, "    %02X            %02X          %02X       %02X%02X\r\n\r\n",
			pStruct->prtHeadData.bInstallCount[0],
			pStruct->prtHeadData.bTankColor[0],
			pStruct->prtHeadData.bTankCount[0],
			pStruct->prtHeadData.bWipeCount[0],pStruct->prtHeadData.bWipeCount[1]) ;
	debugSendString(str) ;

	// INK Tank Data...
	snprintf(str, MAX_SYSLOGSTRING_SIZE, "\r\nInkTank ---------------------------------\r\n") ;
	debugSendString(str) ;
	snprintf(str, MAX_SYSLOGSTRING_SIZE, "ID       SerNum   ManufDate   Update   GrossDotCount    NoInkDotCount     InkLowDotCount\r\n") ;
	debugSendString(str) ;
	//                                     ID               SNo           ManuDate         Update           GrossCount               NoInkCount                   InkLowCount
	snprintf(str, MAX_SYSLOGSTRING_SIZE, "%02X%02X    %02X%02X%02X%02X   %02X%02X%02X       %02X      %02X%02X%02X%02X%02X%02X     %02X%02X%02X%02X%02X%02X      %02X%02X%02X%02X%02X%02X\r\n\r\n",
			pStruct->inkTankData.bId[0],pStruct->inkTankData.bId[1],
			pStruct->inkTankData.bSerialNumber[0],pStruct->inkTankData.bSerialNumber[1],pStruct->inkTankData.bSerialNumber[2],pStruct->inkTankData.bSerialNumber[3],
			pStruct->inkTankData.bManufactureDate[0],pStruct->inkTankData.bManufactureDate[1],pStruct->inkTankData.bManufactureDate[2],
			pStruct->inkTankData.bUpdate[0],
			pStruct->inkTankData.bGrossDotCount[0],pStruct->inkTankData.bGrossDotCount[1],pStruct->inkTankData.bGrossDotCount[2],
			pStruct->inkTankData.bGrossDotCount[3],pStruct->inkTankData.bGrossDotCount[4],pStruct->inkTankData.bGrossDotCount[5],
			pStruct->inkTankData.bNoInkDotCount[0],pStruct->inkTankData.bNoInkDotCount[1],pStruct->inkTankData.bNoInkDotCount[2],
			pStruct->inkTankData.bNoInkDotCount[3],pStruct->inkTankData.bNoInkDotCount[4],pStruct->inkTankData.bNoInkDotCount[5],
			pStruct->inkTankData.bInkLowDotCount[0],pStruct->inkTankData.bInkLowDotCount[1],pStruct->inkTankData.bInkLowDotCount[2],
			pStruct->inkTankData.bInkLowDotCount[3],pStruct->inkTankData.bInkLowDotCount[4],pStruct->inkTankData.bInkLowDotCount[5]) ;
	debugSendString(str) ;

	snprintf(str, MAX_SYSLOGSTRING_SIZE, "\r\nPrinter Data ----------------------------\r\n") ;
	debugSendString(str) ;
	snprintf(str, MAX_SYSLOGSTRING_SIZE, "\r\nPrinterSerNo       InstallDate      DotCount     InstallCount    PurgeCount\r\n") ;
	debugSendString(str) ;
	//											PrintSerialNumber                InstallDate            DotCount                instCount           purgeCount
	snprintf(str, MAX_SYSLOGSTRING_SIZE, "%02X%02X%02X%02X%02X%02X%02X%02X%02X   %02X%02X%02X       %02X%02X%02X%02X%02X%02X        %02X             %02X\r\n\r\n",
			pStruct->inkTankData.bPrinterSerialNumber[0],pStruct->inkTankData.bPrinterSerialNumber[1],pStruct->inkTankData.bPrinterSerialNumber[2],
			pStruct->inkTankData.bPrinterSerialNumber[3],pStruct->inkTankData.bPrinterSerialNumber[4],pStruct->inkTankData.bPrinterSerialNumber[5],
			pStruct->inkTankData.bPrinterSerialNumber[6],pStruct->inkTankData.bPrinterSerialNumber[7],pStruct->inkTankData.bPrinterSerialNumber[8],
			pStruct->inkTankData.bInstallDate[0],pStruct->inkTankData.bInstallDate[1],pStruct->inkTankData.bInstallDate[2],
			pStruct->inkTankData.bDotCount[0],pStruct->inkTankData.bDotCount[1],pStruct->inkTankData.bDotCount[2],
			pStruct->inkTankData.bDotCount[3],pStruct->inkTankData.bDotCount[4],pStruct->inkTankData.bDotCount[5],
			pStruct->inkTankData.bInstallCount[0],
			pStruct->inkTankData.bPurgeCount[0]) ;
	debugSendString(str) ;

	snprintf(str, MAX_SYSLOGSTRING_SIZE, "\r\n>>>>> End of cmStatus structure dump\r\n\r\n") ;
	debugSendString(str) ;
}

char *boot_mode_to_str(uint32_t boot_mode)
{
    char *boot_mode_str;
    
    switch(BoardInfo.BootMode)
    {
        case START_PB_APP:          boot_mode_str = "START_PB_APP";         break;
        case START_CANON_TEST_APP:  boot_mode_str = "START_CANON_TEST_APP"; break;
        case USB_DOWNLOAD_MODE:     boot_mode_str = "USB_DOWNLOAD_MODE";    break;
        case SERIAL_DOWNLOAD_MODE:  boot_mode_str = "SERIAL_DOWNLOAD_MODE"; break;
        default:                    boot_mode_str = "Unknown";              break;
    }

    return boot_mode_str;
}

char *hw_type_to_str(uint8_t hw_type)
{
    return hw_type == 0 ? "CSD2" : "CSD3";
}

void displayBoardInfoStructure(void)
{
    debugDisplayOnly("\nBoard info (@%p):\n", &BoardInfo);
    debugDisplayOnly("      Boot mode    : 0x%08x (%s)\r\n", BoardInfo.BootMode,     boot_mode_to_str(BoardInfo.BootMode) );
    debugDisplayOnly("      Function Chk : %d\r\n",          BoardInfo.Function_Chk                                       );
    debugDisplayOnly("      HW_Type      : %d (%s)\r\n",     BoardInfo.HW_Type,      hw_type_to_str(BoardInfo.HW_Type)    );
    debugDisplayOnly("      HW_Version   : %d\r\n",          BoardInfo.HW_Version                                         );
    debugDisplayOnly("      BL_Ver_Major : %d\r\n",          BoardInfo.Bootloader_Ver_Major                               );
    debugDisplayOnly("      BL_Ver_Minor : %d\r\n",          BoardInfo.Bootloader_Ver_Minor                               );
}

/***********************************************************************
*
*     FUNCTION
*
*         dumpCmStatus
*
*     DESCRIPTION
*
*         Simulate a key down and up press and send it into the system (to OIT Task?)
*
*     INPUTS
*		strCommand		the command that called this routine
*		pParams			pointer to the remainder of the task line after the command portion.
*
*     OUTPUTS
*
***********************************************************************/
//									SRC   DST  MsgType    			DataType  	Delay  DataSize  Data
const msgSeqEle oitTestPrint1[] = {
								   {OIT, SYS, OIT_EVENT, 			BYTE_DATA, 	200, 	2, 		{0x01, 0x00}},
								   {OIT,  CM, OC_GET_STATUS_REQ, 	NO_DATA, 	200, 	0, 		{0x00, 0x00}},
								   {OIT, SYS, OIT_EVENT, 			BYTE_DATA, 	200, 	2, 		{0x05, 0x00}},
								   {OIT, SYS, OIT_START_MAIL_REQ,	BYTE_DATA, 	200, 	2, 		{0x00, 0x00}}
								  };

void sendMsgSequence(char *strCommand, char *pPos)
{
	char *pParams = skipSpaces(pPos) ;
	if(strncmp(pParams, "tp1", 3) == 0)
	{
		size_t szPass ;
		size_t szCount = sizeof(oitTestPrint1)/sizeof(msgSeqEle);
		for(szPass=0 ; szPass < szCount ; szPass++)
		{
		    (void)OSSendIntertask(oitTestPrint1[szPass].dst,
		    					  oitTestPrint1[szPass].src,
		    					  oitTestPrint1[szPass].msgType,
		    					  oitTestPrint1[szPass].dataType,
		    					  (UINT8 *)oitTestPrint1[szPass].data,
		    					  oitTestPrint1[szPass].dataSize);

		    debugDisplayOnly("[%2d] SEQ: %02X >> %02X  Type %02X\r\n",
		    						szPass+1,
		    						(UINT)oitTestPrint1[szPass].src,
		    						(UINT)oitTestPrint1[szPass].dst,
		    						(UINT)oitTestPrint1[szPass].msgType) ;
		    NU_Sleep(oitTestPrint1[szPass].delay) ;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// dumpHex
// dump supplied buffer in a conventional ASCII/Hex display mode
void dumpHex(UINT8 *buf, UINT size, UINT startAdds)
{
	int iPass=0;
	UINT8 bufHex[120];
	UINT8 bufASCII[120];
	UINT8 bufTmp[10];

	bufHex[0] = 0 ;
	bufASCII[0] = 0;
	sprintf((char *)bufHex, "%04X | ", startAdds);
	for (iPass =0 ; iPass < size ; iPass++)
	{
		if((iPass > 0) && ((iPass % 16) == 0))
		{
			dbgTrace(DBG_LVL_INFO,"%s   %s", bufHex, bufASCII) ;
			sprintf((char *)bufHex, "%04X | ", iPass + startAdds);
			bufASCII[0] = 0 ;
		}

		sprintf((char *)bufTmp, "%02X ", buf[iPass]);
		strcat ((char *)bufHex, (char *)bufTmp) ;
		//
		sprintf((char *)bufTmp, "%c ", isprint(buf[iPass]) ? buf[iPass] : '.') ;
		strcat ((char *)bufASCII, (char *)bufTmp);
	}
	int iTmp = 0 ;
	for(iTmp = iPass ; iTmp%16 != 0 ; iTmp++)
	{
		strcat((char *)bufHex, "   ");
	}

	debugDisplayOnly("%s   %s\r\n\r\n", bufHex, bufASCII);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// errorTypeToString
//
// return a string description of the Nucleus error value supplied
char *errorTypeToString(int iErr)
{
	char *strRes = "Unknown Error";
	switch(iErr)
	{
		case NU_ERROR_CREATING_TIMER_HISR:  strRes = "Error creating timer HISR"; break;
		case NU_STACK_OVERFLOW:  			strRes = "Stack overflow"; break;
		case NU_UNHANDLED_INTERRUPT:  		strRes = "Unhandled interrupt"; break;
		case NU_NOT_IN_SUPERVISOR_MODE:  	strRes = "Supervisor code run in user mode"; break;
		case NU_STACK_UNDERFLOW:  			strRes = "Stack underflow"; break;
		case NU_UNHANDLED_EXCEPTION:  		strRes = "Unhandled exception"; break;
		case NU_RUNLEVEL_INIT_ERROR:  		strRes = "Runlevel initialization error"; break;
		case NU_MPU_CONFIG_ERROR:  			strRes = "MPU confiugration error"; break;
		case NU_INVALID_LOCK_USAGE:  		strRes = "Invalid usage of lock"; break;
		case NU_ERROR_OBTAINING_CB:  		strRes = "Error obtaining control block"; break;
		case NU_ILLEGAL_INST:  				strRes = "Illegal instruction executed"; break;
		case NU_INVALID_MEM:  				strRes = "Invalid memory access"; break;
		case NU_FATAL_ERROR:  				strRes = "Fatal/unrecoverable error"; break;
	}
	return strRes;
}
