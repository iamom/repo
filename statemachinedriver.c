/*
** statemachinedriver.s
*/

#include <stdio.h>         /* for printf / sprintf prototype */
#include "externs.h"
#include "target.h"
#include "statemachine.h"
#include "varassert.h"
#include "logging.h"
#include "sys_log_demo.h"

/* ----------------------------------------[ Local Function prototypes ]-- */


/* ---------------------------------------------------[ Implementation ]-- */

void smafn_change_state_machine(sm_context_t *p_context, ev_t *p_ev, void *p_param)
{
   UNUSED_PARAMETER(p_ev);

   p_context->next_sm_tbl = p_param;
}   


void smafn_raise_event(sm_context_t *p_context, ev_t *p_ev, void *p_param)
{
   UNUSED_PARAMETER(p_context);
   UNUSED_PARAMETER(p_ev);
   varassert(p_param != NULL, "p_param should contain event id");
   ev_raise_event((event_id_t)p_param, NULL);
}
 

static ev_t state_changed_ev = {EV_STATE_ENTRY, {0}, 0, 0, { EVDT_NO_DATA, /* EV_INFO_NORMAL,*/ {{0, 0, 0, 0}} }};

static ev_data_t ev_chg_data = { EVDT_STATE_CHANGE_LOG, /* EV_INFO_NORMAL, */ {{0, 0, 0, 0}} };


void sm_handle_event(sm_context_t *p_context, ev_t *ev_ptr)
{
   sm_tbl_entry_t *p_sm_tbl_entry;
   uint8_t b_event_handled = FALSE; /* move inside the do loop if you want notification that the EV_STATE_ENTRY isn't handled */

   bool state_tbl_change_has_occurred;
   
   do
   {
        p_sm_tbl_entry = p_context->sm_tbl->p_tbl;


        while(p_sm_tbl_entry->afn)
        {
            if (p_sm_tbl_entry->event == ev_ptr->ev_id)
            {
                /* found matching event */
                p_sm_tbl_entry->afn(p_context, ev_ptr, (void *)p_sm_tbl_entry->p_param);
                b_event_handled = TRUE;
            }
            p_sm_tbl_entry++;
        }
      #ifndef SMD_DO_NOT_REPORT_UNHANDLED_EVENTS
      if (!b_event_handled)
      {
         char s_text[100];
         const char *event_name;
         event_name = ev_name(ev_ptr->ev_id);
         if (event_name)
         {
            sprintf(s_text, "SM %s: Unhandled event %s", 
                                p_context->sm_tbl->tbl_name, event_name);
         }
         else
         {
            sprintf(s_text, "SM %s: Unhandled event %d (unknown)", 
                                p_context->sm_tbl->tbl_name, ev_ptr->ev_id);
         }
         term_log(s_text);
      }
      #endif /* SMD_DO_NOT_REPORT_UNHANDLED_EVENTS */

      state_tbl_change_has_occurred = p_context->sm_tbl != p_context->next_sm_tbl;
      if (state_tbl_change_has_occurred)
      {
         #ifdef REPORT_STATE_CHANGES
            char mys_txt[100];
            switch (p_context->type)
            {

                default:
                sprintf(mys_txt, "context change %s->%s", 
                                        p_context->sm_tbl->tbl_name,
                                        p_context->next_sm_tbl->tbl_name);
                
                break;
            }
            term_log(mys_txt);
         #endif
         ev_chg_data.d.state_change.state_from    = p_context->sm_tbl;
         ev_chg_data.d.state_change.state_to      = p_context->next_sm_tbl;
         ev_chg_data.d.state_change.p_sm_context  = p_context;
         ev_chg_data.d.state_change.trigger_event = ev_ptr->ev_id;

         ev_create_log_event(EV_STATE_CHG, &ev_chg_data);

         sys_log_add_sprintf(LOG_SRC_STATE_MACHINE, "%s --> %s", p_context->sm_tbl->tbl_name, p_context->next_sm_tbl->tbl_name);
         p_context->sm_tbl = p_context->next_sm_tbl;
         ev_ptr = &state_changed_ev;
      }
   }while (state_tbl_change_has_occurred);
}   
