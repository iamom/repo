/************************************************************************
*   PROJECT:        Horizon CSD
*   COMPANY:        Pitney Bowes
*   MODULE NAME:    cmutils.c
*
*   DESCRIPTION:    Utility functions used by the Control Manager Task
*
* ----------------------------------------------------------------------
*               Copyright (c) 2016 Pitney Bowes Inc.
*                        37 Executive Dr
*                        Danbury, CT 06810
* ----------------------------------------------------------------------
*   MODIFICATION HISTORY:
 13-Jul-16 sa002pe on FPHX 02.12 shelton branch
	Merging in changes for Canada from the 02.07 branch
----*
	* 2013.10.10 Clarisa Bellamy        FPHX 02.07
	*  - Modified to add the "low-speed" option (70 LPM) for Canada.  This has already been
	*    added for non-Canadian countries on the 02.10 branch.  The code in this version
	*    of fnGetCurrentThruputValue() supercedes that of the 02.10 branch as it works
	*    for all countries now.
----*

*
* 2018.01.24 - CWB 
* - Modify fnCMClearWasteTankCounts() to use the new functions defined in the 
*   Horizon_req_HZ00002_CM-to_PM_Interface_Spec.docx specification.
*
* 2018.02.07 - CWB 
* - Remove the EVT_CM_PMNVM_AVAILABLE flag.  Acsess will only be 
*   controlled by the disabling condition DCOND_ACCESSING_PM_NVM. 
*
*************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "commontypes.h"
#include "pbos.h"
#include "global.h"
#include "sys.h"
#include "clock.h"
#include "psoctask.h"

#include "cmpublic.h"
#include "cmpm.h"
#include "cm.h"
#include "cmerr.h"
#include "errcode.h"

#include "OitCm.h"
#include "SysCm.h"
#include "tealeavs.h"
#include "oit.h"
#include "cjunior.h"

#include "cwrapper.h"

#include "custdat.h"
#include "pbplatform.h"       // PLAT_OK_TO_SEND_UPDATE

#include "sig.h"      // (ML) Remove when CMOS Uictype not needed for meterModel
#include "cmos.h"    // (ML) Remove when CMOS Uictype not needed for meterModel

#ifdef RD_DM400_PRINTDBG
#include "mcpdintf.h"
#endif

#include "pbos.h"

#include "features.h"

#include "syspriv.h"

#include "lcd.h"

//#include "RateSvcPublic.h"
//#include "RateService.h"
#include "Datdict.h"
#include "oifpprint.h"
#include "hal.h"
#include "FdrMcpTask.h"
//#include "oigts.h"

#include "api_status.h"
#include "api_diags.h"
#include "trmstrg.h"
#include "imagegen.h"
#include "imageutl.h"
#include "sysdatadefines.h"


#define DBG_CM          // enable PutString messages to syslog
static char     DbgBuf[MAX_DBG_BUF_LEN];

// #define ENABLE_MSG_ERR  1
#define MAILRUN_TIMER   1

//#define TEST_PAUSE_FOR_MAINTENANCE
#define CANON_COPY1_DATA_ADDRESS    0xB001FC00L
#define CANON_COPY2_DATA_ADDRESS    0xB001FD00L


// Internal State of CM PM NVM Access process:
#define CM_PM_NVM_ACC_DONE      0
#define CM_PM_NVM_ACC_READ_PND  1
#define CM_PM_NVM_ACC_WRITE_PND 2



UINT8           pmMtncCtrl = 0;   // Used for disabling 72 hour purge for automated test
UINT8           errCnt = 0; 
static UINT8    initCnt = 0;

UINT32                  *pmVerForScreen;
tCmStatus               cmStatus;               // Current status of CM/PM
ERROR_DESCRIPTOR        pmErrorDescriptor;      // Error discriptor - need by PM ... do not delete
unsigned char           aDummyReasonMsg[ERR_MSG_LEN];
INTERTASK               sDummyTaskMsg;
char                    cDummyTrademark[4] = {0};

static INT8             ejtFlg = FALSE;
static UINT8            subPrtMode = CM_SLEEP_MODE; // Init value. Don't change !
static PRINT_DATA       prtData;                // Image data
static emInitStatus     initRspFlg = INIT_COMPLETE;

static INT8             runDelayTime = 0;
static UINT8            psocStatus = 0;
static UINT32           initRspCnt = 0;    // Count for sending init response
unsigned char ucWasteInkTankStatus = 0;


UINT8 bLastDiagReq = 0;
BOOL fFeederDiag = FALSE;

extern BOOL             fSkipPaperInTranport;
extern UINT8            pIPSD_pieceCount[ 4 ];
extern tCmStateData     cmState;
extern Diagnostics      CMOSDiagnostics;

extern CMOS_Signature CMOSSignature;        // (ML) only temporary - remove when metermodel fixed


//-----------------------------------------------------------------------------
// Data for handling access to PM NVM data: 
//   This data should not be accessed directly, but should follow a call to the 
//   PM to retrieve a copy of the data.  Then the copy is read.  To change the 
//   data, change the copy, then call the update function.   
//-----------------------------------------------------------------------------

// Ptr to Client's NVM return data, including error info.
tPmNvmAccStatus *pNvmAccPtr;

// Internal state of the CMPM NVM Access process. 
UINT8   bPmNvmAccessState = CM_PM_NVM_ACC_DONE;

// The function to call after the PM has replied to either the NVMRead or the 
//  NVMUpdate request.
typedef void (*tpCallbackFn)(void); 
tpCallbackFn     pNvmCallbackFn;

// This is where a copy of the PM NVM data will be stored, unless the client wants 
//  their own copy.
NVRAM_DATA      sNvmCopy;
//-----------------------------------------------------------------------------



emThruput fnGetCurrentThruputValue(void);
int fnGrantPmMtnc(emMtncType  type);
extern BOOL fAllowThruputTesting;
extern BOOL fnIsFeederHardwarePresent(void);
extern BOOL bGoToReady;
extern ulong              CMOSTrmUploadDueReqd;
// DSD 3/31/2006 - Updated error handling.
enum eFDR_INIT_STATUS {
    FDR_INIT_NO_RESPONSE = 0,
    FDR_INIT_SUCCESS,
    FDR_INIT_ERROR
};

enum eFDR_INIT_STATUS eFdrInitStatus = FDR_INIT_NO_RESPONSE;
UINT8 bFdrInitizationResult = MCP_NO_ERROR;

CM_FEEDER_STATUS_t sFeederStatus = {
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    TRUE,
    0,
    0,
    0,
    0,
    0
};

CM_SYSTEM_STATUS_t sSystemStatus = {
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    0,
    0,
    0,
    NO_STOP_CAUSE
};

static UINT8 bMailRunTimerExpCnt = 0;

static int fnPrcsSysModeReqMain(uchar bNewMode, emCmStateEvent  *outputEvtP);
static int fnPrcsSysStopMailRunMain(uchar bMediaType,emCmStateEvent *outputEvtP);
static void fnSendOitGetSttsRsp(void);
static void fnSendModeRsp(void);
static void fnPrcsFeederHasStopped(void);
static void fnPrcsPmHasStopped(void);
static void fnStopAllCMTimers(void);
static void fnSetFdrJamErrInfo(const UINT8 bEvt, const UINT8 bStatus, const UINT8 bErrCode);
static void fnStartCMTimer(const UINT8 bTimerId, int iLineNum);
static void fnStopCMTimer(const UINT8 bTimerId, int iLineNum);
static tCmEvtElmnt fnStopCompleteReturnToIdle(emCmStateEvent inputEvt);
static void fnStartCMMailRunCompleteTimer(const int iStopType, const int iLineNum);
static void fnSendUpdatedFeederSensorStatus(void);
static void fnSaveFeederSensorStatus(const UINT16 sensorStatus);
static void fnCMCheckInkTankMfg(void);
static unsigned char fnDoAdditiveByteChecksum(const unsigned char *pData, const unsigned short usByteCount);
static void fnBeginToStopMailFlow(void);
static int fnFdrErrHandler(FN_PROCESS_INT_TASK_MSG msgHandler, INTERTASK *rMsgP, ulong errType, ulong errCode, emCmStateEvent *outputEvtP);



static unsigned char ucInkTankMfgType = UNKNOWN_MFG;
char            cActualInkTankTMString[50] = {0};   // actual trademark data in the ink tank.
static BOOL     fPendingPrtDisable = FALSE;

// static array used for blank image during simulation
static unsigned char blankImageBuffer[IMAGE_BYTES] = {0};

/**********************************************************************
FUNCTION NAME: 
    fnCMDM400CHandlePmMtncReq

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static BOOL fnCMDM400CHandlePmMtncReq(const UINT8 bMtncType, emCmStateEvent *outputEvtP)
{
    BOOL fRetval = FALSE; // default - let normal CM handle the request

#ifdef DBG_CM
    fnDumpStringToSystemLog("fnCMDM400CHandlePmMtncReq");
#endif

    *outputEvtP = v0_Invalid;

    if (sFeederStatus.fRunning || sFeederStatus.fStopping)
        fRetval = TRUE; // don't grant maintenance while the feeder is active

    return fRetval;
}

/**********************************************************************
FUNCTION NAME: 
    fnSendMailRunComplete

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static void fnSendMailRunComplete(void)
{
    UINT32       rspData[LEN_2];

    if (sSystemStatus.fSendMailRunComplete)
    {
        sSystemStatus.fSendMailRunComplete = FALSE;

        rspData[POS_0] = 0;
        rspData[POS_1] = 0;

        (void)OSSendIntertask(SYS, CM, CS_MAILRUN_CMPLT, LONG_DATA, rspData, sizeof(rspData));    
    }
}

/**********************************************************************
DESCRIPTION: Returns transport speed based on meter type, throughput and mail type

**********************************************************************/
static emSpeed fnGetTransportSpeed(void)
{
    emSpeed bIPS;
    emMeterModel eModelType = fnGetMeterModel();

    switch (eModelType)
    {
        case CSD3:
            switch (cmStatus.bMediaType)
            {
                case MEDIA_MAIL:
                    bIPS = IPS_24;      // CSD3 Mail / Envelopes
                    break;
                case MEDIA_TAPE:
                    bIPS = IPS_12_2;      // CSD3 Tape
                    break;
                default:    // invalid media type
                    bIPS = IPS_24; //No media, default to MEDIA_MAIL
                    break;
            }// media type
            break;
        case CSD2:
            switch (CMOSPrintParams.bSpeedToRunMachineAt)
            {
                case (unsigned char) PRINT_THRUPUT_LOW:
                    bIPS = IPS_7_5;
                    break;
                case (unsigned char) PRINT_THRUPUT_MEDIUM:
                    bIPS = IPS_9_4;
                    break;
                case (unsigned char) PRINT_THRUPUT_HIGH:
                    bIPS = IPS_12_2;
                    break;
                default:    // invalid throughput
                    bIPS = IPS_7_5; //No throughput set default to low speed
                    break;
            } // throughput
            break;
        default:   // invalid model type
            bIPS = IPS_0_0;
            break;
    }// model type

    return bIPS;
}

/**********************************************************************
DESCRIPTION: Stores throughput

**********************************************************************/
void fnSetThroughput(PRINT_THRUPUT thruput)
{
    CMOSPrintParams.bSpeedToRunMachineAt = (uchar) thruput;
}

/*****************************************************************************
*
* SYS messages processing functions
*
*****************************************************************************/

/*****************************************************************************
FUNCTION NAME:   fnPrcsSysInitReq
DESCRIPTION:     Process Init Request message from SYS

AUTHOR: H.Sun    12/16/02

Returns:         0 for success, others are error
****************************************************************************/
int fnPrcsSysInitReq(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    extern PRINT_HEAD_DATA  head_eeprom;
    extern INK_TANK_DATA    tank_eeprom;
    extern NVRAM_DATA       pm_nvram_data;

    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    emMeterModel eModelType = fnGetMeterModel();
    ST_PO_RIGHT_MARGIN       *pRightMargin = 0;
    int     count;


    initRspFlg = INIT_PENDING;

    // clear the error code that will be used by the Print Manager
    pmErrorDescriptor.lwErrorCode = 0;

    // set up the addresses of the buffers that will be used by the Print Manager
    pmErrorDescriptor.originalIntTaskMsgP = (unsigned char *)&sDummyTaskMsg;
    for (count = 0; count < ERR_MSG_LEN; count++)
        pmErrorDescriptor.reasonBufP[count] = aDummyReasonMsg;

    if(initCnt == 0) // Only once for power on
    {

        // CM init
        (void)memset(&cmStatus, 0, sizeof(tCmStatus));

        cmStatus.opMode = DISABLE_MODE;         // set within fnPmDftInit()
        cmStatus.mailRunEvtPending = 0; 
        cmStatus.pendingRpl = FALSE;
        cmStatus.cvrState = CVR_CLOSE;          
        cmStatus.jamLvrState = JAM_LVR_CLOSE;           
        cmStatus.mtncType = CAP_SPIT;
        cmStatus.tstPrt = FALSE;
        cmStatus.topMargin = DFLT_TOP_MARGIN;
        cmStatus.topMarginOffset = DFLT_TOP_OFFSET;
 
        cmStatus.rightMargin = DFLT_RIGHT_MARGIN;

        //Modified to fix fraca 141263,set the default right marginset by using the EMD parameter
        //instead of the hard code.
        pRightMargin = (ST_PO_RIGHT_MARGIN *) fnFlashGetPackedByteParm(PBP_RIGHT_MARGIN_ADJUST_RANGE);
        cmStatus.rightMarginOffset =  pRightMargin->defaultValue - DFLT_RIGHT_MARGIN;
        
        cmStatus.fOOBCompleteP = &CMOSDiagnostics.fOOBComplete;
        cmStatus.pmHeadDataP = &head_eeprom;
        cmStatus.pmTankDataP = &tank_eeprom;
        cmStatus.pmNVMDataP = &pm_nvram_data;

        pmVerForScreen = &cmStatus.pmSwVer;
    }

    // Needed for each init, no matter power up or sleep wake up
    memcpy(cmStatus.sysInitMode, rMsgP->IntertaskUnion.bByteData, sizeof(cmStatus.sysInitMode));

    if( eModelType >= CSD3 )
    {
        if (initCnt == 0)
        {
            *outputEvtP = v66_4InitReq;
        }
        else
        {
            *outputEvtP = v52_InitReq;
        }
    }
    else *outputEvtP = v52_InitReq;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x InitMode %d Time %ld state %d",
            rMsgP->bMsgId, rMsgP->IntertaskUnion.bByteData[POS_0], getMsClock(), cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    initCnt++;

    return OK;
} // end of fnPrcsSysInitReq()


/**********************************************************************
FUNCTION NAME: 
    fnSysInitReq

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSysInitReq(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    // Following code lines can fix GMSE00168143: G900_DM475 System stays 
    // in Please wait when access cover is open to awaken from sleep.
    // Need to turn 27v on not only for s8_Sleep state because if PM wakes up
    // with disabling conditinos, it will switch to another state.
    // turn 27v back on to the feeder board
    fnSystemLogEntry(SYSLOG_TEXT, "27 Volts ON ", 12);
//TODO - move into HAL and delete here
//    FDR_VM_CONTROL_PORT &= ~FDR_VM_CONTROL_BIT;     // For DE2 the logic was reversed for some unknown reason.
    HALFeederSet27Volts(TRUE);

    fnPmDftInit(cmStatus.sysInitMode);
    
    return OK;

} // fnSysInitReq


/**********************************************************************
FUNCTION NAME: 
    fnPrcsSysFatalError

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsSysFatalError(INTERTASK *rMsgP, emCmStateEvent *outputEvtP)
{
    (void)fnStopRun(STOP_ALL,PM_STOP_NOW,EMERGENCY_STOP);
    fnSetModeCommon();
    (void)fnFdrSetMode(CM_DISABLED_MODE);
    (void)fnPmSetMode(DISABLE_MODE, 0, 0);
    *outputEvtP = v0_Invalid;
    return OK;
}

/**********************************************************************
FUNCTION NAME: 
    fnPrcsSysModeReqMain

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static int fnPrcsSysModeReqMain(uchar bModeReq, emCmStateEvent  *outputEvtP)
{
    emMeterModel eModelType = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    uchar       previousSubPrtMode = subPrtMode;


    sSystemStatus.fSysModeChangePending = TRUE;
    sSystemStatus.fPmModeChangeNeeded = TRUE;
    sSystemStatus.bRequestedMode = bModeReq;

    if(subPrtMode == bModeReq) 
    {
        (void)fnMegaSetModeRsp(bModeReq);
        *outputEvtP = v0_Invalid;
    }
    else
    {
        subPrtMode = bModeReq;
        fPendingPrtDisable = FALSE;
        switch(bModeReq)
        {
        case CM_INDICIA_MODE:    // 0
        case CM_TEST_PAT_MODE:   // 2
        case CM_REPORT_MODE:     // 3
        case CM_PERMIT_MODE:     // 0x8
        case CM_SIMULATION_MODE: // 0x1E
             // FRACA 106774
             if (bModeReq == CM_REPORT_MODE)
             {
//                cmStatus.totalPieceCount = (uchar)fnGetCurrentReportTotalPages();
//                sFeederStatus.bTotalPieceCount = (uchar)fnGetCurrentReportTotalPages();
             }
             
             fnSetBobState(bob_Idle); 
             sSystemStatus.bLastRunningMode = NORMAL_MODE;
             sFeederStatus.bLastRunningMode = CM_INDICIA_MODE;
             *outputEvtP = v45_SetNmlMode;
             break;

        case CM_DIFF_WEIGH_MODE: // 0xC
             fnSetBobState(bob_Idle);
             if ( eModelType >= CSD3 )
                *outputEvtP = v98_SetDiffWeighMode;
             else
                 *outputEvtP = v45_SetNmlMode;
                 
             sSystemStatus.bLastRunningMode = NORMAL_MODE;
             sFeederStatus.bLastRunningMode = CM_DIFF_WEIGH_MODE;
             break;

        case CM_SEAL_ONLY_MODE: // 1
             *outputEvtP = v46_SetSlMode;
             sSystemStatus.bLastRunningMode = SEALONLY_MODE;
             sFeederStatus.bLastRunningMode = CM_INDICIA_MODE;
             break;

        case CM_DIAG_MODE: // 7
             *outputEvtP = v47_SetDiagMode;
             sSystemStatus.bLastRunningMode = DIAG_MODE;
             sFeederStatus.bLastRunningMode = CM_DIAG_MODE;
             break;

        case CM_SLEEP_MODE: // 0xA
             *outputEvtP = v51_GoSleep;
             break;

        case CM_SOFT_PWROFF_MODE: // 0xB
             *outputEvtP = v50_SftPwrOff;
             break;

        case CM_DISABLED_MODE: // 0x14
             if (cmState.eCurrentState == s1_Uninit)
             {
//               fnLogSystemError( ERROR_CLASS_CTRL_MGR, ECCM_DISABLE_WHILE_PRINTING, subPrtMode, (uchar)cmState.eCurrentState, 0 );
                 fnDumpStringToSystemLog("Trying to disable CM before init");
                 subPrtMode = previousSubPrtMode;
                 fPendingPrtDisable = TRUE;

                 // we know we aren't allowed to disable before we're initialized,
                 // so send the response just to make everybody happy.
                (void)fnMegaSetModeRsp(bModeReq);
             }

             *outputEvtP = v48_SetDisableMode;
             break;

        default:
             #ifdef DBG_CM
             sprintf(DbgBuf, "Bad Mode req %d St %d",bModeReq, cmState.eCurrentState);
             fnDumpStringToSystemLog(DbgBuf);
             #endif
             break;
             
        } // switch 
    }

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Evt %d stt %d", *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;
}

/**********************************************************************
FUNCTION NAME: 
    fnPrcsSysModeRequest

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsSysModeRequest(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    uchar       bModeReq = rMsgP->IntertaskUnion.bByteData[POS_0];


    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x ModeReq Mode %d state %d",
            rMsgP->bMsgId, rMsgP->IntertaskUnion.bByteData[POS_0], cmState.eCurrentState); 
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return fnPrcsSysModeReqMain(bModeReq,outputEvtP);

} // fnPrcsSysModeRequest

/**********************************************************************
FUNCTION NAME: 
    fnFdrSetMode

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnFdrSetMode(emOpMode bMode)  // (I) mode
{
    uchar       modeMsg[LEN_2];
    emMeterModel eModelType = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    modeMsg[POS_0] = bMode;
    modeMsg[POS_1] = THRUPUT_120;

    if( eModelType >= CSD3 )
    {
        if (sFeederStatus.bCurrMode != bMode)
        {
            if (!sFeederStatus.fModeChangePending)
            {
                sFeederStatus.fModeChangePending = TRUE;
                sFeederStatus.bDesiredMode = (UINT8)bMode;
                sFeederStatus.bCurrMode = (UINT8)bMode;
                (void)OSSendIntertask(FDR, CM, CF_SET_MODE, BYTE_DATA, modeMsg, sizeof(modeMsg));
                fnMsgState(SET_STATE, FDR, ss1_Sent);
            }
        }
        else
        {
            fnMsgState(SET_STATE, FDR, ss2_Responded);
        }
    }
    else
    {
        // even though the feeder isn't present, 
        // make the CM beleive that it is and that it responded.
        // this is necessary because the CM checks to see
        // if the feeder responded when it gets 
        // messages from the pm.
        fnMsgState(SET_STATE, FDR, ss2_Responded);
    }

    return OK;
}

/**********************************************************************
FUNCTION NAME: 
    fnSysGoSleep

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSysGoSleep(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    uchar   stdnMode = STDN_SLEEP;

    fnSetModeCommon();
    (void)fnFdrSetMode(CM_SLEEP_MODE);
    // send Sleep (shutdown) msg to PM
    (void)OSSendIntertask(BJCTRL, CM, CP_PRT_SHUTDOWN, BYTE_DATA, &stdnMode, sizeof(stdnMode));

    return OK;
} // fnSysGoSleep

/**********************************************************************
FUNCTION NAME: 
    fnSysPwrOff

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSysPwrOff(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    uchar   stdnMode = STDN_PWR_OFF;

    // send Sleep (shutdown) msg to PM
    (void)OSSendIntertask(BJCTRL, CM, CP_PRT_SHUTDOWN, BYTE_DATA, &stdnMode, sizeof(stdnMode));

    return OK;

} // fnSysPwrOff

/*****************************************************************************
*
* Feeder messages processing functions
*
*****************************************************************************/

/**********************************************************************
FUNCTION NAME: 
    fnPrcsFdrInitRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsFdrInitRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{

#ifdef NO_PRINTER //TODO JAH Remove when printer is working
    rMsgP->IntertaskUnion.bByteData[POS_0] = 0;
    rMsgP->IntertaskUnion.bByteData[POS_1] = 0;
#endif
    uchar status = rMsgP->IntertaskUnion.bByteData[POS_0];
    uchar errCode = rMsgP->IntertaskUnion.bByteData[POS_1];

    if(status == OK)
    {
        *outputEvtP = v91_FdrInitRsp;
        eFdrInitStatus = FDR_INIT_SUCCESS;
        bFdrInitizationResult = MCP_NO_ERROR;
        cmStatus.fdrErrState.bStatus = OK;
        cmStatus.fdrErrState.bErr = MCP_NO_ERROR;
    }
    else
    {
        //fnFdrErrHandler(fnPrcsFdrInitRsp, rMsgP, status, errCode, outputEvtP);
        eFdrInitStatus = FDR_INIT_ERROR;
        bFdrInitizationResult = errCode;
        cmStatus.fdrErrState.bStatus = status;
        cmStatus.fdrErrState.bErr = errCode;
        *outputEvtP = v91_FdrInitRsp;
    }

    return OK;

} // fnPrcsFdrInitRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsFdrModeRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsFdrModeRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
#ifdef NO_PRINTER //TODO JAH Remove when printer is working
    rMsgP->IntertaskUnion.bByteData[POS_0] = 0;
    rMsgP->IntertaskUnion.bByteData[POS_1] = 0;
    rMsgP->IntertaskUnion.bByteData[POS_2] = 0;
#endif
    uchar fdrMode = rMsgP->IntertaskUnion.bByteData[POS_0];
    uchar status = rMsgP->IntertaskUnion.bByteData[POS_1];
//    uchar errCode = rMsgP->IntertaskUnion.bByteData[POS_2];
    int currState = (int)fnCmGetCrntState();

    *outputEvtP = v0_Invalid;

    sFeederStatus.bCurrMode = fdrMode;
    sFeederStatus.fModeChangePending = FALSE;

    (void)fnMsgState(SET_STATE, FDR, ss2_Responded);

    if(status == OK)
    {
        if ((currState & CM_STOPPING_STATE_MASK) == CM_STOPPING_STATE_PREFIX)
        {
            if(fnMsgState(MATCH, BJCTRL, ss2_Responded) && fnMsgState(MATCH, FDR, ss2_Responded))
            {
                if (currState == s260_StopDoneResetMode)
                    *outputEvtP = v134_ModeResetComplete;
                else
                    *outputEvtP = v131_PmDisabled;
            }
        }
        else
            *outputEvtP = v97_FdrModeRsp;
    }
    else
    {
        //fnFdrErrHandler(fnPrcsFdrModeRsp, rMsgP, status, errCode, outputEvtP);
    }

    return OK;

} // fnPrcsFdrModeRsp

/**********************************************************************
FUNCTION NAME: 
    fnFdrSetModeRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int     fnFdrSetModeRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet
{
    // respond to SYS if pm and feeder responded also
    if(fnMsgState(MATCH, FDR, ss2_Responded) && fnMsgState(MATCH, BJCTRL, ss2_Responded) )
    {
        fnSendModeRsp();
    }
    return OK;
}

/**********************************************************************
FUNCTION NAME: 
    fnPrcsFdrRunRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsFdrRunRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
#ifdef NO_PRINTER //TODO JAH Remove when printer is working
    rMsgP->IntertaskUnion.bByteData[POS_0] = 0;
    rMsgP->IntertaskUnion.bByteData[POS_1] = 0;
#endif
    if(rMsgP->IntertaskUnion.bByteData[POS_0] == OK) 
    {
//TODO - confirm checking for WOW not necessary
#if 0
#ifdef  FAKE_WOW_ON_DM400C
        if(fnIsWOWMail() == TRUE)
        {
            (void)fnFakeWOWMailForDM400C();
        }
#endif
#endif
        *outputEvtP = v94_FdrRunRsp;
    }
    else
    {
        fnFdrErrHandler(fnPrcsFdrInitRsp, rMsgP, rMsgP->IntertaskUnion.bByteData[POS_0], 
            rMsgP->IntertaskUnion.bByteData[POS_1], outputEvtP);
    }

    return OK;

} // fnPrcsFdrRunRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsFdrStopRunRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsFdrStopRunRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
#ifdef NO_PRINTER //TODO JAH Remove when printer is working
    rMsgP->IntertaskUnion.bByteData[POS_0] = 0;
    rMsgP->IntertaskUnion.bByteData[POS_1] = 0;
#endif

    if(rMsgP->IntertaskUnion.bByteData[POS_0] == OK) 
    {
        if (rMsgP->IntertaskUnion.bByteData[POS_2] == (UINT8)MTNC_STOP)
        {
            sSystemStatus.stopCause = STOP_MAINT;
            fnStopAllCMTimers();
            fnStartCMTimer(FP_MAINTENANCE_STOP_TIMER,__LINE__);
        }
        else if (!sSystemStatus.fPieceInProgress)
        {
            sSystemStatus.stopCause = STOP_NORMAL;
            fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);   // can stop quickly, feeder off & nothing in printer
        }
        fnPrcsFeederHasStopped();
    }
    else
    {
        fnFdrErrHandler(fnPrcsFdrInitRsp, rMsgP, rMsgP->IntertaskUnion.bByteData[POS_0], 
            rMsgP->IntertaskUnion.bByteData[POS_1], outputEvtP);
    }

    return OK;

} // fnPrcsFdrStopRunRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsFdrStatusRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsFdrStatusRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
#ifdef NO_PRINTER //TODO JAH Remove when printer is working
    rMsgP->IntertaskUnion.bByteData[POS_0] = 0;
    rMsgP->IntertaskUnion.bByteData[POS_1] = 0;
#endif
    fnMsgState(SET_STATE, FDR, ss2_Responded);
    if(rMsgP->IntertaskUnion.bByteData[POS_0] == OK) 
    {
        *outputEvtP = v93_FdrStatus;
    }
    else
    {
        fnFdrErrHandler(fnPrcsFdrInitRsp, rMsgP, rMsgP->IntertaskUnion.bByteData[POS_0], 
            rMsgP->IntertaskUnion.bByteData[POS_1], outputEvtP);
    }

    return OK;

} // fnPrcsFdrStatusRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsFdrDiagCmdRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsFdrDiagCmdRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    if(rMsgP->bMsgType == BYTE_DATA)
    {
        (void)OSSendIntertask(OIT,CM,CO_FDR_DIAG_RSP,rMsgP->bMsgType,&rMsgP->IntertaskUnion.bByteData[0],sizeof(rMsgP->IntertaskUnion));
    }
    else if(rMsgP->bMsgType == GLOBAL_PTR_DATA)
    {

        (void)OSSendIntertask(OIT,CM,CO_FDR_DIAG_RSP,rMsgP->bMsgType,rMsgP->IntertaskUnion.PointerData.pData,rMsgP->IntertaskUnion.PointerData.lwLength);
    }

    return OK;
} // fnPrcsFdrDiagCmdRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsFdrUnslctStatus

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
HISTORY:
 2010.07.06 Clarisa Bellamy (&Raymond) - Add check for bEvt == FDR_PAPER_SKIPPED, 
                to fake a paper error wehn the envelope misses S3.  This is to 
                fix the DCAP/AR discrepancy issue.
                - Set the cmBobState to PrecreateStarted BEFORE the 
                SCM_PREPARE_FOR_MAILRUN message is sent to bob, instead of after.

**********************************************************************/
int fnPrcsFdrUnslctStatus(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
#ifdef NO_PRINTER //TODO JAH Remove when printer is working
    //rMsgP->IntertaskUnion.bByteData[POS_0] = 0;
    rMsgP->IntertaskUnion.bByteData[POS_1] = 0;
    rMsgP->IntertaskUnion.bByteData[POS_2] = 0;
#endif
    UINT8 bEvt = rMsgP->IntertaskUnion.bByteData[POS_0];
    UINT8 bStatus = rMsgP->IntertaskUnion.bByteData[POS_1];
    UINT8 bError = rMsgP->IntertaskUnion.bByteData[POS_2];

    *outputEvtP = v0_Invalid;

    fnSaveFeederSensorStatus((UINT16)(rMsgP->IntertaskUnion.bByteData[POS_3]) |
                                                        (UINT16)(rMsgP->IntertaskUnion.bByteData[POS_4] << 8));

    if(bStatus == OK) 
    {
        if (bEvt == FDR_MAIL_JOB_COMPLETE)
        {
            fnPrcsFeederHasStopped();
            //(void)fnCmCommitNewState(v130_FdrStopped,s512_Running);
            if (!sSystemStatus.fPieceInProgress)                // if nothing in printer then stop, otherwise the print rsp starts the timer
                fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);   // can stop quickly, feeder off & nothing in printer
            *outputEvtP = v130_FdrStopped;
        }
        else if( bEvt == FDR_PAPER_SKIPPED )
        {
            // Next envelope detected before current one passed S3. 
            // Assume S3 was missed, and fake PM paper error to stop printing.
            rMsgP->IntertaskUnion.lwLongData[POS_0] = PM_PAPER_ERR;
            rMsgP->IntertaskUnion.lwLongData[POS_1] = E_PAPER_SKIPPED;
            (void)fnPmErrHandler( fnPrcsUnsolicit, rMsgP, 
                                  rMsgP->IntertaskUnion.lwLongData[POS_0], 
                                  rMsgP->IntertaskUnion.lwLongData[POS_1], 
                                  outputEvtP );
        }
        else if (bEvt == FDR_TIMEOUT_PAUSE && !(sSystemStatus.fSysStopPending || sSystemStatus.fCmStopPending))
        {
            sSystemStatus.fPausePending = TRUE;
            fnStopAllCMTimers();
            fnStartCMTimer(FP_MAINTENANCE_STOP_TIMER,__LINE__);
            *outputEvtP = v0_Invalid;           
        }
        else if (bEvt == FDR_RESTART_REQ && !(sSystemStatus.fSysStopPending || sSystemStatus.fCmStopPending))
        {
            sSystemStatus.fRestartPending = TRUE;           
            if (!sSystemStatus.fStopping && !sSystemStatus.fPausePending)
            {
                if (!sSystemStatus.fRunning)
                    fnPmRun(RUN_START);
                else
                    (void)fnFdrRun(0,NULL,NULL);
            }
            *outputEvtP = v0_Invalid;
        }
        else if (bEvt == FDR_COVER_OPENED)  // DSD 06/06/2007 - Reworked the handling of the Feeder Cover Open (FRACAs 121244 & 1212335)
        {
            if ((sFeederStatus.fRunning && !sFeederStatus.fStopping) || 
                (sSystemStatus.fProcessingPieces && !sSystemStatus.fStopping))
            {
                (void)fnMailProgress(EVT_FDR_COVER_STATUS_UPDATE, IS_STOPPED, OK);
                (void)fnStopRun(STOP_ALL, PM_STOP_NOW, EMERGENCY_STOP);
                sSystemStatus.fCancelStart = TRUE;
                //sSystemStatus.stopCause = STOP_NORMAL;
                //fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);
            }               
            *outputEvtP = v93_FdrStatus;
        }
        else if(bEvt == FDR_WOW_REZEROING)
        {
            (void)fnMailProgress(EVT_MAILRUN_STATUS_UPDATE, WOW_REZEROING, OK);
            *outputEvtP = v0_Invalid;
        }
        else if(bEvt == FDR_WOW_REZERO_DONE)
        {
            (void)fnMailProgress(EVT_MAILRUN_STATUS_UPDATE, WOW_REZERO_DONE, OK);
            *outputEvtP = v0_Invalid;
        }

//TODO - confirm checking for WOW not necessary
#if 0
#ifdef  FAKE_WOW_ON_DM400C
        else if (bEvt == FDR_MAIL_PIECE_AT_EXIT)
        {
            (void)fnFakeWOWMailForDM400C();
            *outputEvtP = v0_Invalid;
        }
#endif
#endif
        else if(bEvt == FDR_WOW_MAIL_PIECE_DATA_READY)
        {
//            UINT8   bWOWWeightStatus;
//            UINT8   bWOWWeight[LENWEIGHT];

            //Query WOW mail piece weight status and take corresponding actions.
//            fnGetWOWWeight(bWOWWeight, &bWOWWeightStatus, FALSE);
/*
            if (bWOWWeightStatus == WEIGHT_OKAY)
            {
                //send a message "SCM_PREPARE_FOR_MAILRUN" to SCM to precreate Indicum.
                fnSetBobState(bob_PrecreateStarted);
                (void)OSSendIntertask(SCM, CM, SCM_PREPARE_FOR_MAILRUN, NO_DATA, NULL, 0);
            }
            else
            {
                //TBD: handle error condition accordingly.
            }   
            *outputEvtP = v0_Invalid;
*/
        }
        else
            *outputEvtP = v93_FdrStatus;
    }
    else if (bEvt == FDR_MAIL_JOB_COMPLETE)
    {
        fnPrcsFeederHasStopped();
        if (bStatus == FDR_JAM_STATUS)
        {
            if (bError == MCP_INTERLOCK_CVR_OPEN)   // DSD 06/06/2007 - Reworked the handling of the Feeder Cover Open (FRACAs 121244 & 1212335)
            {
                if ((sFeederStatus.fRunning && !sFeederStatus.fStopping) || 
                    (sSystemStatus.fProcessingPieces && !sSystemStatus.fStopping) ||
                    (subPrtMode == CM_DIFF_WEIGH_MODE))
                {
                    (void)fnMailProgress(EVT_FDR_COVER_STATUS_UPDATE, IS_STOPPED, OK);
                    (void)fnStopRun(STOP_ALL, PM_STOP_NOW, EMERGENCY_STOP);
                    sSystemStatus.fCancelStart = TRUE;
                    //sSystemStatus.stopCause = STOP_NORMAL;
                    //fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);
                    *outputEvtP = v93_FdrStatus;
                }               
                else
                    *outputEvtP = v99_FdrJam;
            }
            else
            {
                if (subPrtMode == CM_DIFF_WEIGH_MODE)
                    (void)fnStopRun(STOP_FDR_ONLY, 0, EMERGENCY_STOP);
                fnFdrErrHandler(fnPrcsFdrUnslctStatus, rMsgP, rMsgP->IntertaskUnion.bByteData[POS_1], rMsgP->IntertaskUnion.bByteData[POS_2], outputEvtP);
                fnSetFdrJamErrInfo(rMsgP->IntertaskUnion.bByteData[0],rMsgP->IntertaskUnion.bByteData[1],rMsgP->IntertaskUnion.bByteData[2]);   
                (void)fnRptPendingEvts();         
                (void)fnCmCommitNewState(v99_FdrJam,s512_Running);
                *outputEvtP = v99_FdrJam;
            }
        }
        else
        {           
            (void)fnCmCommitNewState(v130_FdrStopped,s512_Running);
            *outputEvtP = v130_FdrStopped;
        }
    }
    else if (bEvt == FDR_COVER_OPENED || bEvt == FDR_COVER_CLOSED)
    {
        *outputEvtP = v93_FdrStatus;
    }
    else
    {
        fnFdrErrHandler(fnPrcsFdrUnslctStatus, rMsgP, rMsgP->IntertaskUnion.bByteData[POS_0], 
            rMsgP->IntertaskUnion.bByteData[POS_1], outputEvtP);
    }

    return( OK );

} // fnPrcsFdrUnslctStatus

/**********************************************************************
FUNCTION NAME: 
    fnFdrStatus

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnFdrStatus(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
#ifdef NO_PRINTER //TODO JAH Remove when printer is working
    //rMsgP->IntertaskUnion.bByteData[POS_0] = 0;
    rMsgP->IntertaskUnion.bByteData[POS_1] = 0;
    rMsgP->IntertaskUnion.bByteData[POS_2] = 0;
#endif
    emFdrEvt    evt = 0;//rMsgP->IntertaskUnion.bByteData[POS_0];
    uchar       status = 0;//rMsgP->IntertaskUnion.bByteData[POS_1];
    uchar       errCode = 0;//rMsgP->IntertaskUnion.bByteData[POS_2];
    UINT16       sensorState = 0;//rMsgP->IntertaskUnion.bByteData[POS_3] | (rMsgP->IntertaskUnion.bByteData[POS_4] << 8);
    uchar       mailrunStatus = IS_RUNNING;
//    ushort      fdrSensors = (ushort)rMsgP->IntertaskUnion.bByteData[POS_3];

    if (rMsgP->bMsgId == FC_UNSLCT_STATUS)
    {
        evt = rMsgP->IntertaskUnion.bByteData[POS_0];
        status = rMsgP->IntertaskUnion.bByteData[POS_1];
        errCode = rMsgP->IntertaskUnion.bByteData[POS_2];
        sensorState = (UINT16)rMsgP->IntertaskUnion.bByteData[POS_3] |
                                    (UINT16)(rMsgP->IntertaskUnion.bByteData[POS_4] << 8);
        // clear sensor status
        cmStatus.snsrState.wSensorData &= ~(FTOPCVR_MASK|
                                                                                (FS2_MASK) |
                                                                                (W2_MASK << 8) |
                                                                                (W3_MASK << 8));
        // set sensor status
        cmStatus.snsrState.wSensorData |= sensorState; 
        // DM475C doesn't need to check S2 Sensor.
        if (evt != FDR_COVER_OPENED && evt != FDR_COVER_CLOSED && errCode != MCP_INTERLOCK_CVR_OPEN
                && evt != FDR_SENSOR_STATUS)    // DSD 06/07/2007
        {
            cmStatus.fdrErrState.bStatus |= status;
            cmStatus.fdrErrState.bErr |= errCode;
        }
        switch(evt)
        {
        case FDR_NO_MAIL:
             (void)fnMailProgress(EVT_FDR_NO_MAIL, IS_STOPPED, OK);
             break;
           
        case FDR_MAIL_LOADED:
             (void)fnMailProgress(EVT_FDR_MAIL_ADDED, IS_STOPPED, OK);
             break;
           
        case FDR_COVER_OPENED:
        case FDR_COVER_CLOSED:
             if (evt == FDR_COVER_CLOSED)   // DSD 06/06/2007 - Reworked the handling of the Feeder Cover Open (FRACAs 121244 & 1212335)
             {
                cmStatus.fdrErrState.bStatus = 0;
                cmStatus.fdrErrState.bErr = 0;
                cmStatus.fdrErrState.bJamCode = 0;              
             }
             else if (evt == FDR_COVER_OPENED)
             {
                mailrunStatus = IS_STOPPED;
                //sSystemStatus.stopCause = STOP_NORMAL;
                //fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);
             }
             (void)fnMailProgress(EVT_FDR_COVER_STATUS_UPDATE, mailrunStatus, OK);
             break;

        case FDR_ERROR:
             (void)fnMailProgress(EVT_FDR_ERR, IS_STOPPED, OK);
             break;

        case FDR_SENSOR_STATUS:
             (void)fnMailProgress(EVT_WOW_MAIL_JAM_STATUS_UPDATE, IS_STOPPED, OK);
             break;

        default: // ignore others for now
             break;

        } // switch     
    }
    else if (rMsgP->bMsgId == FC_GET_STATUS_RSP)
    {
        status = rMsgP->IntertaskUnion.bByteData[POS_0];
        errCode = rMsgP->IntertaskUnion.bByteData[POS_1];
        sensorState = (UINT16)rMsgP->IntertaskUnion.bByteData[POS_2] |
                                    (UINT16)(rMsgP->IntertaskUnion.bByteData[POS_3] << 8);
        // clear sensor status
        cmStatus.snsrState.wSensorData &= ~(FTOPCVR_MASK |
                                                                                (FS2_MASK) |
                                                                                (W2_MASK << 8) |
                                                                                (W3_MASK << 8));
        // set sensor status
        cmStatus.snsrState.wSensorData |= sensorState; 
        cmStatus.fdrErrState.bStatus |= status;
        cmStatus.fdrErrState.bErr |= errCode;

        fnSendOitGetSttsRsp();

        if(cmState.eCurrentState == s36_PndgWrng) *newStateP = fnRtnToIdle(__LINE__);       
    }

    return OK;

} // fnFdrStatus



/*****************************************************************************
*
* OIT messages processing functions
*
*****************************************************************************/
/**********************************************************************
FUNCTION NAME: 
    fnPrcsOitRplStart

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsOitRplStart(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    // Clear all errors
    (void)memset(&cmStatus.pmErrState, 0, sizeof(tPmErrStatus));

    *outputEvtP = v49_RplStrt;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Evt %d stt %d", *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsOitRplStart

/**********************************************************************
FUNCTION NAME: 
    fnOitRplStart

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnOitRplStart(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    char  semstat;

    // Get semaphore for PH PSOC - do not suspend, nobody else should be acquiring it
    semstat = OSAcquireSemaphore (PH_PSOC_SEM_ID, OS_NO_SUSPEND);
    if (semstat != SUCCESSFUL)
    {
        dbgTrace(DBG_LVL_ERROR,"Could not obtain semaphore for Print Head replacement: status %d\r\n", semstat);
    }

    // send Replacement Start msg to PM
    (void)OSSendIntertask(BJCTRL, CM, CP_PERFORM_RPL, NO_DATA, NULL, 0);
    cmStatus.pendingRpl = TRUE;

    return OK;

} // fnOitRplStart

/**********************************************************************
FUNCTION NAME: 
    fnOitRplStart_cvrOpen

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnOitRplStart_cvrOpen(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    char  semstat;

    // Get semaphore for PH PSOC - do not suspend, nobody else should be acquiring it
    semstat = OSAcquireSemaphore (PH_PSOC_SEM_ID, OS_NO_SUSPEND);
    if (semstat != SUCCESSFUL)
    {
        dbgTrace(DBG_LVL_ERROR,"Could not obtain semaphore for Cover Open Print Head replacement: status %d\r\n", semstat);
    }

    // send Replacement Start msg to PM
    (void)OSSendIntertask(BJCTRL, CM, CP_PERFORM_RPL, NO_DATA, NULL, 0);
    cmStatus.pendingRpl = TRUE;

    return OK;

} // fnOitRplStart_cvrOpen

/**********************************************************************
FUNCTION NAME: 
    fnPrcsOitRplCmplt

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsOitRplCmplt(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    *outputEvtP = v54_RplCmplt;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Evt %d stt %d", *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsOitRplCmplt

/**********************************************************************
FUNCTION NAME: 
    fnOitRplCmplt

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnOitRplCmplt(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    // Let PM to turn on 5V head voltage 
    (void)OSSendIntertask(BJCTRL, CM, CP_RPL_COMPLETE, NO_DATA, NULL, 0);

    return OK; 

} // fnOitRplCmplt

/**********************************************************************
FUNCTION NAME: 
    fnPrcsRplCmpltRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsRplCmpltRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
#ifdef NO_PRINTER //TODO JAH Remove when printer is working
    rMsgP->IntertaskUnion.lwLongData[POS_0] = 0;
    rMsgP->IntertaskUnion.lwLongData[POS_1] = 0;
#endif
    ulong       status = rMsgP->IntertaskUnion.lwLongData[POS_0]; 
    ulong       errCode = rMsgP->IntertaskUnion.lwLongData[POS_1];

    if(status == OK)
    {
        *outputEvtP = v28_PmRplCmpltRsp;
    }
    else 
    {
        (void)fnPmErrHandler(fnPrcsRplCmpltRsp, rMsgP, status, errCode, outputEvtP);
    }

    // fix for fnPsocResync() so CS_INIT_RSP won't be sent for tank replacement 
    if(initRspCnt == 0)
        initRspCnt++;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> initRspCnt %ld Evt %d stt %d", initRspCnt, *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsRplCmpltRsp

/**********************************************************************
FUNCTION NAME: 
    fnPsocResync

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPsocResync(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{

    // PSOC reset is managed by SYS for the first hardware power up,
    // No resync needed
    if(initRspCnt > 0)    //  || *newStateP == s18_RplFnsh_5V_ON) 
    {
        (void)OSSendIntertask(PSOC, CM, RESEED_MSG, NO_DATA, NULL, 0);
    }
    else 
    {
        (void)fnInitRsp_Idle(inputEvt, rMsgP, newStateP);
        if(initRspCnt == 0)
            initRspCnt++;
    }

    return OK;

} // fnPsocResync

/**********************************************************************
FUNCTION NAME: 
    fnPrcsResetRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsResetRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    *outputEvtP = v0_Invalid;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> ResetRsp Evt %d stt %d", *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;
} // fnPrcsResetRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsNvmDataRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
/*   This has been rewritten for CSD-Sr farther below.

int fnPrcsNvmDataRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    *outputEvtP = v0_Invalid;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Evt %d stt %d", *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsNvmDataRsp
 */


/**********************************************************************
FUNCTION NAME: 
    fnPrcsNvmUpdateRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
/*   This has been rewritten for CSD-Sr farther below.
int fnPrcsNvmUpdateRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    *outputEvtP = v0_Invalid;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Evt %d stt %d", *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsNvmUpdateRsp
 */


/**********************************************************************
FUNCTION NAME: 
    fnPrcsStopRunRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
HISTORY:
 2012.11.19 Jane Li - Fraca GMSE00116406: Set sSystemStatus.fRunning to FALSE to   
                avoid the dead loop when the error PM_FATAL_ERR occures  
**********************************************************************/
int fnPrcsStopRunRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    ulong       snsrData;
    short       motionSnsr;
    STOP_RUN_RESPONSE   *stopRunRsp = (STOP_RUN_RESPONSE *)rMsgP->IntertaskUnion.PointerData.pData;
//    int         currState = (int)fnCmGetCrntState();
//    emCmState   newState;
#ifdef NO_PRINTER //TODO JAH Remove when we have printer
    stopRunRsp->lwStatus = 0;
    stopRunRsp->lwErrorCode = 0;
    stopRunRsp->lwMotionSensorData = 0;
    stopRunRsp->lwInkDropsUsed = 0;
    stopRunRsp->bStopType = 0;
#endif //TODO JAH Remove when we have printer

    // Only pay attention to sensor data if response is valid and PM is OK
    if((stopRunRsp->lwStatus != PM_MSG_ERR) && (stopRunRsp->lwStatus != PM_FATAL_ERR))
    {
        snsrData = stopRunRsp->lwMotionSensorData;
        dbgTrace(DBG_LVL_INFO,"PM sensor status: Stop Run Response %x", cmStatus.snsrState.wSensorData);
        motionSnsr = cmStatus.snsrState.wSensorData & (S1_MASK | S2_MASK | S3_MASK);

        snsrData &= (S1_MASK | S2_MASK | S3_MASK); // Extract motion sensor bits

        if(motionSnsr != snsrData)
        {
            // clear motion sensor data bits
            cmStatus.snsrState.wSensorData &= ~(S1_MASK | S2_MASK | S3_MASK);

            // set sensor data
            cmStatus.snsrState.wSensorData |= snsrData;
        }
    }

    //if(stopRunRsp->lwStatus == OK &&
    //  (cmStatus.pmErrState.lwStatus & (IPM_MSG_ERR|IPM_TAPE_ERR|IPM_PAPER_ERR|IPM_PHEAD_ERR|IPM_FATAL_ERR)) == 0)// FRACA GMSE00102137 - TODO: make an error state with a new function
    if(stopRunRsp->lwStatus == OK)
    {
        if (sSystemStatus.stopCause == STOP_MAINT)
        {       
            //*outputEvtP = v34_StopRunRsp;
            *outputEvtP = v36_PmAtStrtPauseCooling;
        }
        else if (sSystemStatus.fRestartPending)
        {
            sSystemStatus.stopCause = NO_STOP_CAUSE;
            fnPmRun(RUN_START);
            *outputEvtP = v0_Invalid;
        }
        else if (sSystemStatus.stopCause == STOP_FDR_PAUSE)
        {
            sSystemStatus.stopCause = NO_STOP_CAUSE;
            fnPrcsPmHasStopped();       
            (void)OSSendIntertask(FDRMCPTASK,CM,CF_CM_PAUSED,NO_DATA,NULL,0);
            *outputEvtP = v0_Invalid;
        }
        else
/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
        {
            //*outputEvtP = v133_TransportStopped;                          
            fnPrcsPmHasStopped();
            (void) fnStopCompleteReturnToIdle(v34_StopRunRsp);
            *outputEvtP = v0_Invalid;
        }
    //}
    //else if (stopRunRsp->lwStatus == OK && currState == s259_StopTransport)
    //{
        //*outputEvtP = v133_TransportStopped;
    }
    else
    {
       //Fraca GMSE00116406: Set sSystemStatus.fRunning to FALSE to avoid the dead loop 
       //when the error PM_FATAL_ERR occures
       if (stopRunRsp->lwStatus == PM_FATAL_ERR)
            fnPrcsPmHasStopped();
            
        (void)fnPmErrHandler(fnPrcsStopRunRsp, rMsgP, 
            stopRunRsp->lwStatus, stopRunRsp->lwErrorCode, outputEvtP);
    }

    return OK;

} // fnPrcsStopRunRsp

/**********************************************************************
FUNCTION NAME: 
    fnSetModeCommon

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
void fnSetModeCommon(void)
{ 
    emMeterModel eModelType = fnGetMeterModel();

    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    if( eModelType >= CSD3 )
    {
        fnMsgState(SET_STATE, FDR, ss1_Sent);
        fnMsgState(SET_STATE, BJCTRL, ss1_Sent);
    }
    else
    {
        fnMsgState(SET_STATE, FDR, ss2_Responded);
    }
}

/**********************************************************************
FUNCTION NAME: 
    fnSetNmlMode

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSetNmlMode(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    MAIL_SIMULATION simuParmsCopy;

    cmStatus.opMode = NORMAL_MODE;
    
    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Set Normal Mode Evt %d state %d", inputEvt, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    if(subPrtMode == CM_SIMULATION_MODE && fnGetMailSimuPara(&simuParmsCopy))
    {
        (void)fnPmSetMode(SIMULATION_MODE, simuParmsCopy.TimeInterval, simuParmsCopy.NumTrips);
    }

    else
    {
        fnSetModeCommon();
        (void)fnPmSetMode(NORMAL_MODE, 0, 0);
        (void)fnFdrSetMode(CM_INDICIA_MODE);
    }

    return OK;

} // fnSetNmlMode

/**********************************************************************
FUNCTION NAME: 
    fnSetDisableMode

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSetDisableMode(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
//    emCmState currState = fnCmGetCrntState();

    cmStatus.opMode = DISABLE_MODE;
    
    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Set Disable Mode Evt %d state %d", inputEvt, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif
    
    fnSetModeCommon();
    (void)fnFdrSetMode(CM_DISABLED_MODE);
    (void)fnPmSetMode(DISABLE_MODE, 0, 0);

/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//
//    if ( cmStatus.fIsRunning == TRUE )
//  {
//      fnStopAllCMTimers();
//      if (currState != s20_PreCvr)    // if disabled because of the cover open then don't start the timer - it causes unhelpfull state transitions
//          fnStartCMTimer(MAILRUN_DELAY_TRANSPORT_STOP,__LINE__);
//  }
/////////////////////////////////////////////////////////////////

    return OK;

} // fnSetDisableMode

/**********************************************************************
FUNCTION NAME: 
    fnSetSlOnlyMode

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSetSlOnlyMode(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    cmStatus.opMode = SEALONLY_MODE;
    // cmStatus.tstPrt = FALSE;

    fnSetModeCommon();
    (void)fnFdrSetMode(CM_INDICIA_MODE);
    (void)fnPmSetMode(SEALONLY_MODE, 0, 0);

    return OK;

} // fnSetSlOnlyMode

/**********************************************************************
FUNCTION NAME: 
    fnSetDiagMode

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSetDiagMode(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    cmStatus.opMode = DIAG_MODE;

    // FRACA GMSE00102135
    fnSetModeCommon();              
    (void)fnFdrSetMode(CM_DIAG_MODE);
    (void)fnPmSetMode(DIAG_MODE, 0, 0);

    return OK;

} // fnSetDiagMode

/**********************************************************************
FUNCTION NAME: 
    fnSetDiffWeighMode

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSetDiffWeighMode(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    cmStatus.opMode = NORMAL_MODE;

    fnSetModeCommon();
    (void)fnPmSetMode(NORMAL_MODE, 0, 0);
    (void)fnFdrSetMode(CM_DIFF_WEIGH_MODE);

    return OK;
}   // fnSetDiffWeighMode

/**********************************************************************
FUNCTION NAME: 
    fnAckSysModeChange

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnAckSysModeChange(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    fnMegaSetModeRsp(sSystemStatus.bRequestedMode);
    return OK;
}   // fnAckSysModeChange

/**********************************************************************
FUNCTION NAME: 
    fnPrcsSysRunRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

NOTES:
    1, Bob updated this function is to fix the G9CA field issue that with the attached scale mode,
    when user takes a mail from scale and inserts it into the DM300c meter, the meter sometimes prints
    nothing on the envelope but the postage is debited. According to the analyzed syslog, after getting
    the stable zero message, the meter needs about 300 ms to do some Bob Task scripts (0x0E, 0x0F, and then 0x31)
    and then to set mode to OIT_LEFT_PRINT_RDY and then to generate static image before jumping to home screen.
    During the 300 ms, if the S1 sensor is covered, meter will start the mail printing process.
    Because the OIT_LEFT_PRINT_RDY mode is set, the meter's system mode indicates it is not ready to print mail.
    Meter didn't cath this special case and then caused this issue.
    So, meter needs checking the system mode before starting the mail run process. If the system mode is not ready,
    meter shouldn't start the mail run process.
    
HISTORY:
 2013.08.07 Bob Li - Fixed the G9CA blank envelope printing issue.
 2010.07.17 Clarisa Bellamy - Set the cmBobState to PrecreateStarted BEFORE the 
                SCM_PREPARE_FOR_MAILRUN message is sent to bob, instead of after.
**********************************************************************/
int fnPrcsSysRunRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
#ifdef NO_PRINTER //TODO JAH Remove when printer is working
    rMsgP->IntertaskUnion.lwLongData[POS_0] = 0;
#endif
    ulong lwCurrentEvent = 0;
    unsigned char ucMode = 0;
    unsigned char ucSubMode = 0;
//    unsigned int iTest;
    unsigned long lwStatus = 0;
    emMeterModel emModel = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    emBobState  tmpBobState = fnGetBobState();
    emCmState emCurrState = fnCmGetCrntState();
    emCmState emNewState = (emCmState)0;

// LMD      sSystemStatus.fSysRunReqPending = FALSE;

    // Firstly, find out what the system current mode is
    fnGetSysMode(&ucMode, &ucSubMode);

    lwStatus = rMsgP->IntertaskUnion.lwLongData[POS_0];

#ifdef S1_TEASE_DEBUG
    iTest = (unsigned int)rand();
        if ((iTest%5) == 0)
        lwStatus = 1;       // 20% of the time fail
#endif      

    //if(    rMsgP->IntertaskUnion.lwLongData[POS_0] == 0 )
    if (lwStatus==0)
    // && (cmState.ePreviousState == s9_BfPrtMtnc || cmState.ePreviousState == s4_NmlIdle || cmState.ePreviousState == s5_SlOnlyIdle) )
    {
        // For DM300C, start precreate for the first piece 
        // For DM400C, it's already done when starting mail run
        // For tape, it's already done when starting tape run
        if(    emModel == CSD2
            && subPrtMode != CM_SEAL_ONLY_MODE  
            && tmpBobState != bob_PrecreateStarted
            && tmpBobState != bob_Precreated ) 
        {
            //to fix the G9CA blank mail printing issue.
            //At this point, the meter has been set to not ready mode.
            //Meter shouldn't start the print process.
            if(ucMode != SYS_PRINT_NOT_READY && ucSubMode != SYS_NO_SUBMODE)
            {
                fnSetBobState(bob_PrecreateStarted);
                (void)OSSendIntertask(SCM, CM, SCM_PREPARE_FOR_MAILRUN, NO_DATA, NULL, 0);
            }
        }

        switch(cmStatus.opMode)
        {
        case NORMAL_MODE:
            //to fix the G9CA blank mail printing issue.
            //At this point, if the meter has been set to not ready mode,
            //meter shouldn't start the print process.
            if(ucMode != SYS_PRINT_NOT_READY && ucSubMode != SYS_NO_SUBMODE)
            {
                *outputEvtP = v59_SysPrtModeRunRsp;
                
                // DSD - check to see if this is really a good idea
                (void) OSReceiveEvents(OIT_EVENT_GROUP, MAIN_MENU_CHK_END_EVT, OS_SUSPEND, OS_AND, &lwCurrentEvent);
            }
            else
                *outputEvtP = v72_SysMsgErr;
                
            break;
         
        case SEALONLY_MODE:
             *outputEvtP = v60_SysSlOnlyRunRsp;
             break;

        default:
            *outputEvtP = v72_SysMsgErr;
             break;
        }
    }
    else 
    {
        //if(rMsgP->IntertaskUnion.lwLongData[POS_0] != 0) *outputEvtP = v42_SysRunRspErr;
        //if(lwStatus != 0) *outputEvtP = v42_SysRunRspErr;
        //else *outputEvtP = v72_SysMsgErr;
#ifdef S1_TEASE_DEBUG
        fnLEDSetColor(LED_RED);
#endif
        sSystemStatus.fSysRunRejected = TRUE;

        if (emModel == CSD2 && emCurrState == s29_SysRspPnd)
        {
            if (!sSystemStatus.fRunning)
            {
                // this is the first piece
                // simply go back to normal and don't 
                // allow the run to begin.
                //*outputEvtP = v128_NormalStopRunReq;
                //(void)fnCmCommitNewState(v128_NormalStopRunReq,s512_Running);
                *outputEvtP = v0_Invalid;
                emNewState = fnRtnToIdle(__LINE__);
                (void)fnCmCommitNewState(v0_Invalid,emNewState);
            }
            else
            {
                // this has occurred in the middle of a run.
                // treat this like a normal stop.
                // but, to be safe disable the pm.
                *outputEvtP = v128_NormalStopRunReq;
/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//              (void)fnPmSetMode(DISABLE_MODE, 0, 0);
/////////////////////////////////////////////////////////////////
                (void)fnCmCommitNewState(v128_NormalStopRunReq,s512_Running);
            }
        }
    }

#ifdef TEST_OIT_RATE_NOT_READY_MSG
    (void)OSSendIntertask(CM, OIT, OC_RATE_NOT_READY, NO_DATA, NULL, 0);
#endif

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x RunR x%x outEvt %d preS %d crntS %d",
            rMsgP->bMsgId, lwStatus, *outputEvtP, cmState.ePreviousState, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;
} // fnPrcsSysRunRsp

/**********************************************************************
FUNCTION NAME: 
    fnSysPrtModeRunRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSysPrtModeRunRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    fnPmRun(RUN_THRU_S3);

    return OK;

} // fnSysPrtModeRunRsp

/**********************************************************************
FUNCTION NAME: 
    fnSysSlOnlyRunRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSysSlOnlyRunRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    return fnPmRun(RUN_THRU_S3);

} // fnSysSlOnlyRunRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsSysStartMailRun

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
HISTORY:
 2010.07.17 Clarisa Bellamy - Set the cmBobState to PrecreateStarted BEFORE the 
                SCM_PREPARE_FOR_MAILRUN message is sent to bob, instead of after.
**********************************************************************/
int fnPrcsSysStartMailRun(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    uchar       bMediaType = rMsgP->IntertaskUnion.bByteData[POS_0];
    ulong       rspMsg[LEN_2];
    emMeterModel eModelType = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    rspMsg[POS_0] = 0;
    rspMsg[POS_1] = 0;

    // SYS sends this message for both mail run and tape run
    cmStatus.bMediaType = bMediaType;
    cmStatus.tapesNum = rMsgP->IntertaskUnion.bByteData[POS_1];
    cmStatus.totalPieceCount = rMsgP->IntertaskUnion.bByteData[POS_1];
    sFeederStatus.bTotalPieceCount = rMsgP->IntertaskUnion.bByteData[POS_1];
    sSystemStatus.fProcessingPieces = TRUE;     // DSD 05-14-2007 Track system running status better.  Fix Fraca 119889

    //initialize number of pieces weighed for Weigh First Piece handling
    fnSetNumPiecesWeighed(0);

    //TODO - confirm checking for WOW not necessary
// Do not start Indicium's pre-create here in WOW mode
//    if(fnIsWOWMail() == FALSE)
    if(subPrtMode != CM_SEAL_ONLY_MODE)
    {
        // Start precreate
        fnSetBobState(bob_PrecreateStarted);
        (void)OSSendIntertask(SCM, CM, SCM_PREPARE_FOR_MAILRUN, NO_DATA, NULL, 0);
    }

    // DSD 06/06/2007 - Reworked the handling of the Feeder Cover Open (FRACAs 121244 & 1212335)
    cmStatus.fdrErrState.bStatus = 0;
    cmStatus.fdrErrState.bErr = 0;
    cmStatus.fdrErrState.bJamCode = 0;

    if(bMediaType == MEDIA_MAIL) 
    {
        if( eModelType >= CSD3 )
        {
            *outputEvtP = v67_4StartMailRun;
            sSystemStatus.fSendMailRunComplete = TRUE;

            (void)OSSendIntertask(SYS, CM, CS_MAILRUN_STARTED, LONG_DATA, rspMsg, sizeof(rspMsg));
        }
        else
        {
            *outputEvtP = v0_Invalid;
        }
    }
    else
    {
#ifdef S1_TEASE_DEBUG
        fnLEDSetColor(LED_GREEN);
#endif
        *outputEvtP = v68_StartTapeRun;
        sSystemStatus.fSendMailRunComplete = TRUE;

        (void)OSSendIntertask(SYS, CM, CS_MAILRUN_STARTED, LONG_DATA, rspMsg, sizeof(rspMsg));
    }

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Sx%x Start Media %d Evt %d stt %d",
            rMsgP->bMsgId, bMediaType, *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsSysStartMailRun

/**********************************************************************
FUNCTION NAME: 
    fnPrcsSysStopMailRunMain

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static int  fnPrcsSysStopMailRunMain(
    uchar bMediaType,
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{   
    emMeterModel model = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    if (!sSystemStatus.fSysStopPending && !sSystemStatus.fCmStopPending)
    {
        sSystemStatus.fSysStopPending = TRUE;
        if (bMediaType == MEDIA_TAPE)
        {
            sSystemStatus.stopCause = STOP_NORMAL;
            sSystemStatus.fTapeStopPending = TRUE;  // in case the v33_StopTapeRunRsp isn't handled by state machine, it is checked again later...
            (void)fnStopRun(STOP_TAPE_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
        }
        else if ( (model >= CSD3) && sFeederStatus.fRunning )
        {
            sSystemStatus.stopCause = STOP_NORMAL;
            sSystemStatus.fRestartPending = FALSE;  // DSD 03/11/2007 - Cancel any restart
            (void)fnStopRun(STOP_FDR_ONLY, 0, GRACEFUL_STOP);
        }
        else if (model >= CSD3)
        {
            sSystemStatus.stopCause = STOP_NORMAL;
            sSystemStatus.fRestartPending = FALSE;  // DSD 03/11/2007 - Cancel any restart
            (void)fnStopRun(STOP_MAIN_ONLY,PM_STOP_NOW, GRACEFUL_STOP);
        }
        else
        {   // DM300C Envelopes
            sSystemStatus.stopCause = STOP_NORMAL;
            //fnStartCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);
            if (!sSystemStatus.fPieceInProgress)
                fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);
            else
                fnStartCMMailRunCompleteTimer(NORMAL_STOP,__LINE__);
        }
    }

    *outputEvtP = v0_Invalid;

    return OK;
}

/**********************************************************************
FUNCTION NAME: 
    fnPrcsSysStopMailRun

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsSysStopMailRun(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    uchar       bMediaType = rMsgP->IntertaskUnion.bByteData[POS_0];

    return fnPrcsSysStopMailRunMain(bMediaType,outputEvtP);

} // fnPrcsSysStopMailRun

/**********************************************************************
FUNCTION NAME: 
    fnCheckPmMtnc

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnCheckPmMtnc(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    // check PM whether maintenance is needed
    (void)OSSendIntertask(BJCTRL, CM, CP_MAINTENANCE_CHECK, NO_DATA, NULL, 0);

    return OK;

} // fnCheckPmMtnc

/**********************************************************************
FUNCTION NAME: 
    fnStartMainTransport

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnStartMainTransport(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    emMeterModel eModelType = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    if( eModelType >= CSD3 )
    {
        fnPmRun(RUN_START);
    }
    else  // DM300C starts transport when SYS says ok, in which case it goes to run to S3. 
    {
        fnAutoReq_SysRspPnd(inputEvt, rMsgP, newStateP);
        *newStateP = s29_SysRspPnd;
    }

    return OK;

} // fnStartMainTransport

/**********************************************************************
FUNCTION NAME: 
    fnStartMainTransportForTapeRun

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnStartMainTransportForTapeRun(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    fnPmRun(RUN_START);

    return OK;

} // fnStartMainTransportForTapeRun

/**********************************************************************
FUNCTION NAME: 
    fnStopMainTransport

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnStopMainTransport(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    emMeterModel eModelType = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    if (sSystemStatus.stopCause == STOP_MAINT || sSystemStatus.stopCause == STOP_FDR_PAUSE)
    {
        //return fnStopRun(STOP_MAIN_ONLY, PM_STOP_AT_S3, MTNC_STOP);
        if( eModelType >= CSD3 )
            return fnStopRun(STOP_MAIN_ONLY, PM_STOP_NOW, MTNC_STOP);
        else
            return fnStopRun(STOP_MAIN_ONLY, PM_STOP_AT_S3, MTNC_STOP);
    }
    else
        return fnStopRun(STOP_MAIN_ONLY, PM_STOP_NOW, GRACEFUL_STOP);

} // fnStopMainTransport

/**********************************************************************
FUNCTION NAME: 
    fnFdrRun

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnFdrRun(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    uchar    runMsg[LEN_3];

    runMsg[POS_0] = sFeederStatus.bTotalPieceCount;
    runMsg[POS_1] = fnGetCurrentThruputValue();
    //TODO - confirm checking for WOW not necessary
#if 1
    runMsg[POS_2] = 0; // dummy value
#else
    runMsg[POS_2] = fnGetWOWThruputValue();
#endif
    switch(subPrtMode)
    {
    //case CM_PERMIT_MODE:     // 0x8
    //case CM_SIMULATION_MODE: // 0x1E
    //case CM_REPORT_MODE:       // 3
    case CM_TEST_PAT_MODE:   // 2                   // Fix FRACA 106302
         cmStatus.totalPieceCount = 1;
         // fall through !!!
    case CM_DIFF_WEIGH_MODE:   // 0xC
         sFeederStatus.bTotalPieceCount = 1;      // Run one piece at a time
         runMsg[POS_0] = 1;
         break;

    default:
         break;
    }

#ifdef DBG_CM
    sprintf(DbgBuf, "CM>> fnFdrRun (%d)", sFeederStatus.bTotalPieceCount);
    fnDumpStringToSystemLog(DbgBuf);
#endif

    sFeederStatus.fRunning = TRUE;
    (void)OSSendIntertask(FDR, CM, CF_RUN, BYTE_DATA, runMsg, sizeof(runMsg));

    return(0);
} // fnFdrRun()

/**********************************************************************
FUNCTION NAME: 
    fnPmRunTape

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPmRunTape(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    uchar    runMsg[LEN_2];

    runMsg[POS_0] = IPS_12_2;
    runMsg[POS_1] = cmStatus.tapesNum;

    if (!sSystemStatus.fTapeStopPending)
    {
        sSystemStatus.fRunning = TRUE;
        sSystemStatus.fProcessingPieces = TRUE; // DSD 05-14-2007 Track system running status better.  Fix Fraca 119889
        sSystemStatus.fTapeRunning = TRUE;
        sSystemStatus.stopCause = NO_STOP_CAUSE;
        sSystemStatus.fTapeStopPending = FALSE;
        (void)OSSendIntertask(BJCTRL, CM, CP_RUN_TAPE, BYTE_DATA, runMsg, sizeof(runMsg));
        cmStatus.tapesNum = 0;
    }

    return OK;

} // fnPmRunTape

/**********************************************************************
FUNCTION NAME: 
    fnPrcsOitGetDataReq

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsOitGetDataReq(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    *outputEvtP = v56_GetData;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Sx%x GetData %d Evt %d stt %d",
            rMsgP->bMsgId, rMsgP->IntertaskUnion.bByteData[POS_0], *outputEvtP, cmState.eCurrentState); 
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsOitGetDataReq 

/**********************************************************************
FUNCTION NAME: 
    fnGetData

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnGetData(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    switch(rMsgP->IntertaskUnion.bByteData[POS_0])
    {
    case PRT_HEAD_DATA:
        (void)OSSendIntertask(BJCTRL, CM, CP_GET_PRTHEAD_DATA, GLOBAL_PTR_DATA, 
             &cmStatus.prtHeadData, sizeof(PRINT_HEAD_DATA));
        break;

    case INKTANK_DATA:
        (void)OSSendIntertask(BJCTRL, CM, CP_GET_INKTANK_DATA, GLOBAL_PTR_DATA, 
             &cmStatus.inkTankData, sizeof(INK_TANK_DATA));
        break;

    case SW_VER:
        (void)OSSendIntertask(BJCTRL, CM, CP_GET_PM_VER, NO_DATA, NULL, 0);
        break;

    default: // SNSR_DATA_REQ:
        cmStatus.snsrState.wSensorData = 0;
        (void)OSSendIntertask(BJCTRL, CM, CP_REQ_SENSOR_DATA, GLOBAL_PTR_DATA, 
             &cmStatus.snsrState, sizeof(SENSOR_RESPONSE));
        (void)OSWakeAfter(50);
        break;
    } // switch

    return OK;

} // fnGetData

/**********************************************************************
FUNCTION NAME: 
    fnPrcsOitGetStatusReq

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsOitGetStatusReq(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    int currState = (int)fnCmGetCrntState();

    // TODO: Patched by RD to get CSD3 to work, syslog indicatedc timeout due to no response to this message.
    if (( fnGetMeterModel() == CSD3 ) || ((currState & CM_STOPPING_STATE_MASK) == CM_STOPPING_STATE_PREFIX))
    {
        // in the stopping state we need to respond or else the OIT becomes unhappy...
        sSystemStatus.fSendOitSttsRsp = TRUE;
        fnSendOitGetSttsRsp();
    }

    *outputEvtP = v65_GetSttsReq;

    fnSetBobState(bob_Idle);

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Evt %d stt %d", *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsOitGetStatusReq

/**********************************************************************
FUNCTION NAME: 
    fnSendOitGetSttsRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static void fnSendOitGetSttsRsp(void)
{
    uchar       rspData[LEN_12];


    (void)memset(rspData, 0x0, LEN_12);

    // Fill transport status
    switch(cmState.eCurrentState)
    {
        case s3_Mtnc:
        case s9_BfPrtMtnc:
        case s10_PrtModeStg:
        //case s11_ImgGen:
        case s12_Ejt:
        case s13_Prt:
        case s19_SlOnlyRun:
        case s256_StopFdr:
        case s257_DisablePm:
        case s258_ClearTransport:
        case s259_StopTransport:
        case s260_StopDoneResetMode:
        case s261_ImmediateStopRecovery:
        case s262_StopTape:
            rspData[POS_0] = TRANSPORT_RUNNING_STATUS;

            if(cmState.eCurrentState == s3_Mtnc || cmState.eCurrentState == s9_BfPrtMtnc) 
            {
                rspData[POS_8] = PERFORM_MAINTENANCE;
            }
            else 
            {
                rspData[POS_8] = IS_RUNNING;
            }
            break;
        default:
            rspData[POS_0] = TRANSPORT_STOPPED_STATUS;
            rspData[POS_8] = IS_STOPPED;
            break;
    } // switch

    // Fill PM errors, bytes 1 to 7 AND PSOC byte 11
    (void)fillPmFdrErrState(rspData);

    rspData[POS_9] = cmStatus.snsrState.wSensorData & (S1_MASK | S2_MASK | S3_MASK | JAMLVR_MASK | TOPCVR_MASK | FTOPCVR_MASK | FS2_MASK );
    rspData[POS_5] = (cmStatus.snsrState.wSensorData >> 8) & 
                                        (W2_MASK | W3_MASK | WIDTH1_MASK | WIDTH2_MASK | WIDTH3_MASK);

    if (sSystemStatus.fSendOitSttsRsp)
    {
        sSystemStatus.fSendOitSttsRsp = FALSE;
        (void)OSSendIntertask (OIT, CM, CO_GET_STATUS_RSP, BYTE_DATA, rspData, LEN_12);
    }
}

/**********************************************************************
FUNCTION NAME: 
    fnOitGetSttsRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnOitGetSttsRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    emMeterModel eModelType = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    sSystemStatus.fSendOitSttsRsp = TRUE;
    if( eModelType >= CSD3 )
    {
        (void)fnMsgState(SET_STATE,FDR,ss1_Sent);
        (void)OSSendIntertask(FDRMCPTASK,CM,CF_GET_STATUS,NO_DATA,NULL,0);
        if (cmState.eCurrentState == s26_SlpPnd || cmState.eCurrentState == s8_Sleep || cmState.eCurrentState == s2_Init
            || ( (cmState.eCurrentState == s20_PreCvr) && (cmState.ePreviousState == s8_Sleep) ) 
            || cmState.eCurrentState == s24_JamOpen ) // DSD 02/26/2007 - Fix for ME 0112 caused by a jam condition combined with the jam lever being opened.
        {
            (void)fnMsgState(SET_STATE,BJCTRL,ss2_Responded);
        }
        else
        {
            (void)fnMsgState(SET_STATE,BJCTRL,ss1_Sent);
            cmStatus.snsrState.wSensorData = 0;
            (void)OSSendIntertask(BJCTRL, CM, CP_REQ_SENSOR_DATA, GLOBAL_PTR_DATA, 
                 &cmStatus.snsrState, sizeof(SENSOR_RESPONSE));
            (void)OSWakeAfter(50);
        }
    }
    else
    {
        (void)fnMsgState(SET_STATE,FDR,ss2_Responded);
        if (cmState.eCurrentState == s26_SlpPnd || cmState.eCurrentState == s8_Sleep || cmState.eCurrentState == s2_Init
            || ( (cmState.eCurrentState == s20_PreCvr) && (cmState.ePreviousState == s8_Sleep) ) 
            || cmState.eCurrentState == s24_JamOpen ) // DSD 02/26/2007 - Fix for ME 0112 caused by a jam condition combined with the jam lever being opened.
        {
            (void)fnMsgState(SET_STATE,BJCTRL,ss2_Responded);
            fnSendOitGetSttsRsp();
        }
        else
        {
            cmStatus.snsrState.wSensorData = 0;
            (void)fnMsgState(SET_STATE,BJCTRL,ss1_Sent);
            cmStatus.snsrState.wSensorData = 0;
            (void)OSSendIntertask(BJCTRL, CM, CP_REQ_SENSOR_DATA, GLOBAL_PTR_DATA, 
                 &cmStatus.snsrState, sizeof(SENSOR_RESPONSE));
            (void)OSWakeAfter(50);
        }
    }
    
    return OK;

} // fnOitGetSttsRsp

/**********************************************************************
FUNCTION NAME: 
    fnMailProgress

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnMailProgress(
    uchar   mailRunEvent,               // (I) mail run event
    uchar   mailRunStatus,              // (I) mail run status
    uchar   bobStatus)                  // (I) Bob status
{
    uchar       rspData[LEN_12];

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> To Oit: Mail Progress evt %d", mailRunEvent);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    (void)memset(rspData, 0x0, LEN_12);

    // convert FDR ERR
    if(mailRunEvent == EVT_FDR_ERR)
    {
        if(fnIsWOWDimRatingError() == TRUE)
        {
            mailRunEvent = EVT_WOW_DIM_RATING_ERR;
            
            // Clear WOW Dim Rating error flag
            fnSetWOWDimRatingErrorFlag(FALSE);
        }
        else if(fnIsMCPWOWError(cmStatus.fdrErrState.bJamCode) == TRUE)
        {
            mailRunEvent = EVT_WOW_SBR_ERR;
        }
    }

    // Fill mail run event
    rspData[POS_0] = mailRunEvent;

    // Fill PM & feeder errors, bytes 1 to 7 AND PSOC byte 11
    (void)fillPmFdrErrState(rspData);
    
    // Mail run status
    rspData[POS_8] = mailRunStatus;

    // Sensor status 
    rspData[POS_9] = cmStatus.snsrState.wSensorData & 
                     ( S1_MASK | S2_MASK | S3_MASK | JAMLVR_MASK | TOPCVR_MASK | 
                       FS1_MASK | FS2_MASK | FTOPCVR_MASK );
    rspData[POS_5] = (cmStatus.snsrState.wSensorData >> 8) & 
                     ( W2_MASK | W3_MASK | WIDTH1_MASK | WIDTH2_MASK | WIDTH3_MASK );

    // Bob status
    rspData[POS_10] = cmStatus.bobErrCode;

    // if(mailRunEvent == EVT_MAIL_PIECE_PROCESSED)
    // {
    //     cmStatus.bobPreErrCode = cmStatus.bobErrCode;
    // }

    // For Bob warning, only send once until Bob reports next time .
    // This condition check is needed as Bob error needs designated event in the mail progress message
    // But Bob warning does not
    if(!(cmStatus.mailRunEvtPending & BOB_EVT_MASK )) 
    {
        cmStatus.bobErrCode = 0;
    }

    (void)OSSendIntertask(OIT, CM, CO_MAIL_PROGRESS, BYTE_DATA, rspData, LEN_12);

    // Clear PENDING evevt already reported so no duplicate report next time
    // see fnPmErrHandler() && fnRptPendingEvts() for details
    switch(mailRunEvent)
    {
        case EVT_PM_PAPER_ERR:
             cmStatus.mailRunEvtPending &= ~PAPER_EVT_MASK;
             break;

        case EVT_PM_INK_ERR:
             cmStatus.mailRunEvtPending &= ~INK_EVT_MASK;
             break;

        case EVT_PM_INKTANK_ERR:
             cmStatus.mailRunEvtPending &= ~TANK_EVT_MASK;
             break;

        case EVT_PM_WASTETANK_ERR:
             cmStatus.mailRunEvtPending &= ~WTANK_EVT_MASK;
             break;

        case EVT_PM_PHEAD_ERR:
             cmStatus.mailRunEvtPending &= ~PHEAD_EVT_MASK;
             break;

        case EVT_PSOC_ERR:
             psocStatus = 0;
             break;

        case EVT_FDR_ERR:
        case EVT_WOW_SBR_ERR:
        case EVT_WOW_DIM_RATING_ERR:
             cmStatus.mailRunEvtPending &= ~FDR_EVT_MASK;
             break;

        case EVT_TAPE_ERR:
             cmStatus.mailRunEvtPending &= ~TAPE_EVT_MASK;
             break;

        default:     // do nothing
             break;
    } // end switch

    return OK;

} // fnMailProgress

/**********************************************************************
FUNCTION NAME: 
    fillPmFdrErrState

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
HISTORY:
 2010.07.17 Clarisa Bellamy - Handle the paper skipped error.

**********************************************************************/
int fillPmFdrErrState(
    uchar *stateData)                   // (O) Filled PM error state data, the address starts 
                                        //     from second array element bByteData[POS_1]
{
    ulong           idx = 0;
    ulong           tmpVal = 0;


    // stateData is initialized by caller

    // Parse internal error structure
    if(    cmStatus.pmErrState.lwStatus == OK
        && cmStatus.fdrErrState.bStatus == OK )
    {
        return OK;
    }

    if(cmStatus.fdrErrState.bStatus)
    {
        stateData[POS_1] |= FDR_ERR;
        stateData[POS_3] = cmStatus.fdrErrState.bErr;

        //cmStatus.fdrErrState.bStatus = 0;
        //cmStatus.fdrErrState.bErr = 0;
    }

    if(cmStatus.pmErrState.lwStatus & IPM_TAPE_ERR)
    {
        stateData[POS_1] |= TAPE_ERR;

        idx = TAPE_ERR_OFFSET;
        while(idx < SHIFT_LIMIT) 
        {
            tmpVal = 1 << idx;
            if(cmStatus.pmErrState.lwTapeErr & tmpVal) break;  
            idx++;
        }

        stateData[POS_3] = idx + TAPE_ERR_SHIFT;

        if(cmStatus.pmErrState.lwTapeErr == 0)
            cmStatus.pmErrState.lwStatus &= ~IPM_TAPE_ERR;

    }

    if(cmStatus.pmErrState.lwStatus & IPM_FATAL_ERR)
    {
        stateData[POS_1] |= FATAL_ERR;

        idx = FATAL_ERR_OFFSET;
        while(idx < SHIFT_LIMIT) 
        {
            tmpVal = 1 << idx;
            if(cmStatus.pmErrState.lwFatalErr & tmpVal)
                break;  
            idx++;
        }

        if(idx < SHIFT_LIMIT)
            stateData[POS_2] = (unsigned char)(idx + FATAL_ERR_SHIFT);

        #ifdef DBG_CM
        sprintf(DbgBuf, "CM>> fillPmErr POS_2 Fatal %x", stateData[POS_2]);
        fnDumpStringToSystemLog(DbgBuf);
        #endif
    }

    if(cmStatus.pmErrState.lwStatus & IPM_PAPER_ERR)
    {
        stateData[POS_1] |= PAPER_ERR;

        idx = PAPER_ERR_OFFSET;
        while(idx < SHIFT_LIMIT) 
        {
            tmpVal = 1 << idx;
            if(cmStatus.pmErrState.lwPaperErr & tmpVal)
                break;  
            idx++;
        }
        // First 5 bits (0-4) are used for paper errors, bits 5 ans 6 are used for Jams
        // bit 7 is another paper error.
        if( idx < 5 ) 
            stateData[POS_3] = idx + PAPER_ERR_SHIFT;
        else if( idx > 6 ) 
            stateData[POS_3] = idx + PAPER_ERR_SHIFT;
        else 
            stateData[POS_3] = idx + JAM_ERR_SHIFT;

        // Clear No Paper Error
        cmStatus.pmErrState.lwPaperErr &= ~(1 << (E_NO_PAPER - PAPER_ERR_SHIFT));
        cmStatus.pmErrState.lwPaperErr &= ~(1 << (E_NO_PRINT - PAPER_ERR_SHIFT));
        if(cmStatus.pmErrState.lwPaperErr == 0)
            cmStatus.pmErrState.lwStatus &= ~IPM_PAPER_ERR;

        #ifdef DBG_CM
        sprintf(DbgBuf, "CM>> fillPmErr POS_3 Paper %x", stateData[POS_3]);
        fnDumpStringToSystemLog(DbgBuf);
        #endif
    }

    if(cmStatus.pmErrState.lwStatus & IPM_PHEAD_ERR)
    {
        stateData[POS_1] |= PHEAD_ERR;

        idx = PHEAD_ERR_OFFSET;
        while(idx < SHIFT_LIMIT) 
        {
            tmpVal = 1 << idx;
            if(cmStatus.pmErrState.lwPheadErr & tmpVal) 
                break;
            idx++;
        }

        if(idx < SHIFT_LIMIT) 
            stateData[POS_4] = idx + PHEAD_ERR_SHIFT;

        #ifdef DBG_CM
        sprintf(DbgBuf, "CM>> fillPmErr POS_4 Phead %x", stateData[POS_4]);
        fnDumpStringToSystemLog(DbgBuf);
        #endif
    }

    if(cmStatus.pmErrState.lwStatus & IPM_INK_ERR)
    {
        stateData[POS_1] |= INK_ERR;

        if(cmStatus.pmErrState.lwInkErr == IE_NO_INK) 
            stateData[POS_6] = ERR_INK_OUT;

        #ifdef DBG_CM
        sprintf(DbgBuf, "CM>> fillPmErr POS_6 Ink %x", stateData[POS_6]);
        fnDumpStringToSystemLog(DbgBuf);
        #endif
    }

    if(cmStatus.pmErrState.lwStatus & IPM_INKTANK_ERR)
    {
        stateData[POS_1] |= INK_ERR;

        idx = INKTANK_ERR_OFFSET;
        while(idx < SHIFT_LIMIT) 
        {
            tmpVal = 1 << idx;
            if(cmStatus.pmErrState.lwInkTankErr & tmpVal) 
                break;  
            idx++;
        }

        stateData[POS_6] = idx + INKTANK_ERR_SHIFT;

        #ifdef DBG_CM
        sprintf(DbgBuf, "CM>> fillPmErr POS_6 InkTank %x", stateData[POS_6]);
        fnDumpStringToSystemLog(DbgBuf);
        #endif
    }

    #if ENABLE_MSG_ERR
    if(cmStatus.pmErrState.lwStatus & IPM_MSG_ERR)
    {
        stateData[POS_1] |= MSG_ERR;

        idx = MSG_ERR_OFFSET;
        while(idx < SHIFT_LIMIT)
        {
            tmpVal = 1 << idx;
            if(cmStatus.pmErrState.lwStatus & tmpVal)
                break;  
            idx++;
        }

        stateData[POS_7] = idx + MSG_ERR_SHIFT;

        // clear message error automatically
        cmStatus.pmErrState.lwStatus &= ~IPM_MSG_ERR;
        cmStatus.pmErrState.lwStatus &= tmpVal;

        #ifdef DBG_CM
        sprintf(DbgBuf, "CM>> fillPmErr POS_7 Msg %x", stateData[POS_7]);
        fnDumpStringToSystemLog(DbgBuf);
        #endif
    }
    #endif

    if(cmStatus.pmErrState.lwStatus & IPM_WARNING)
    {
        if(cmStatus.pmErrState.lwStatus & IW_INK_LOW)
        {
            stateData[POS_1] |= INK_ERR;
            stateData[POS_6] |= WRN_INK_LOW;

            #ifdef DBG_CM
            sprintf(DbgBuf, "CM>> fillPmErr POS_6 WrnInkLow %x", stateData[POS_6]);
            fnDumpStringToSystemLog(DbgBuf);
            #endif
        }

        if(cmStatus.pmErrState.lwStatus & IW_WTANK_NEAR_FULL)
        {
            stateData[POS_1] |= WASTETANK_ERR;
            stateData[POS_7] = WRN_WTANK_NEAR_FULL;

            #ifdef DBG_CM
            sprintf(DbgBuf, "CM>> fillPmErr POS_7 WrnWTankNearFull %x", stateData[POS_7]);
            fnDumpStringToSystemLog(DbgBuf);
            #endif
        }

        if(cmStatus.pmErrState.lwStatus & IW_WTANK_FULL)
        {
            stateData[POS_1] |= WASTETANK_ERR;
            stateData[POS_7] = WRN_WTANK_FULL;

            #ifdef DBG_CM
            sprintf(DbgBuf, "CM>> fillPmErr POS_7 WrnWTankFull %x", stateData[POS_7]);
            fnDumpStringToSystemLog(DbgBuf);
            #endif
        }

        if(   cmStatus.pmErrState.lwStatus & IW_INKTANK_MFTR_TIMEOUT_1
           || cmStatus.pmErrState.lwStatus & IW_INKTANK_MFTR_TIMEOUT_2)
        {
            stateData[POS_1] |= INK_ERR;
            stateData[POS_6] = WRN_INKTANK_MRTR_TIMEOUT;

            #ifdef DBG_CM
            sprintf(DbgBuf, "CM>> fillPmErr POS_6 WrnInkTank_MRTR %x", stateData[POS_6]);
            fnDumpStringToSystemLog(DbgBuf);
            #endif
        }

        if(   cmStatus.pmErrState.lwStatus & IW_INKTANK_INST_TIMEOUT_1
           || cmStatus.pmErrState.lwStatus & IW_INKTANK_INST_TIMEOUT_2)
        {
            stateData[POS_1] |= INK_ERR;
            stateData[POS_6] = WRN_INKTANK_INST_TIMEOUT;

            #ifdef DBG_CM
            sprintf(DbgBuf, "CM>> fillPmErr POS_6 WrnInkTank_INST %x", stateData[POS_6]);
            fnDumpStringToSystemLog(DbgBuf);
            #endif
        }

        if(cmStatus.pmErrState.lwStatus & IW_PIECE_SKIPPED)
        {
            stateData[POS_1] |= PAPER_ERR;
            stateData[POS_3] = WRN_PIECE_SKIPPED;

            cmStatus.pmErrState.lwStatus &= ~IW_PIECE_SKIPPED;

            #ifdef DBG_CM
            sprintf(DbgBuf, "CM>> fillPmErr POS_3 Paper %x", stateData[POS_3]);
            fnDumpStringToSystemLog(DbgBuf);
            #endif
        }

    } // IPM_WARNING

    if(cmStatus.pmErrState.lwStatus & IPSOC_ERR)
    {
            stateData[POS_1] |= PSOC_ERR;
            stateData[POS_11] = ERR_PSOC;
            
            #ifdef DBG_CM
            sprintf(DbgBuf, "CM>> fillPmErr POS_11 PSOC %x", stateData[POS_11]);
            fnDumpStringToSystemLog(DbgBuf);
            #endif
    }
    return OK;

} // fillPmFdrErrState


/*****************************************************************************
*
* BOB messages processing functions
*
*****************************************************************************/
/**********************************************************************
FUNCTION NAME: 
    fnPrcsBobDebitCommitRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsBobDebitCommitRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    active_mp_log_t *p_active_mp;
    uint32_t pc;
    uint32_t zpc;
    error_t err;
    JPRINT_IMAGE    *bobImage = (JPRINT_IMAGE *)rMsgP->IntertaskUnion.PointerData.pData; 
    emMeterModel eModelType = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here


    if(bobImage->bReqMsgId == SCM_PREPARE_FOR_MAILRUN)
    {
        if(bobImage->fBobPrintStatus != OK ||
           ((CMOSTrmUploadDueReqd | TRM_UPLOAD_REQUIRED) == TRM_UPLOAD_REQUIRED) )
        {
            cmStatus.bobErrCode = bobImage->bBobOIPrintError;

            *outputEvtP = v41_BobDebitCommitErr;
            cmStatus.mailRunEvtPending |= BOB_EVT_MASK;
            fnSetBobState(bob_Err);

            // (void)fnMailProgress(EVT_BOB_ERR, IS_RUNNING, bobImage->bBobOIPrintError);
            sSystemStatus.fEjectMailPiece = TRUE;
            sSystemStatus.fCmStopPending = TRUE;
            if (cmStatus.bMediaType == MEDIA_TAPE)
            {
                (void)fnStopRun(STOP_TAPE_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
            }
            else if( eModelType >= CSD3 )
            {
                (void)fnStopRun(STOP_FDR_ONLY, 0, GRACEFUL_STOP);
            }
            else
            {   // DM300C Envelopes
                fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);
            }
        }
        else
        {
        	// dont overwrite debit Bob error condition
        	if (cmStatus.bobErrCode == SCM_SUCCESSFUL_STATUS)
        		cmStatus.bobErrCode = bobImage->bBobOIPrintError;

           fnSetBobState(bob_Precreated);
           *outputEvtP = v0_Invalid;

//           //Set End Print
//           p_active_mp = get_active_mp();
//           trmEndPrint(p_active_mp->mp_id);
        }
    }
    else
    {
        // tmpCnt++; // TBD: testing code
        // if(tmpCnt % 2) cmStatus.bobErrCode = 0xC;  
        // else cmStatus.bobErrCode = 0;
         //Commit debit here

        cmStatus.bobErrCode = bobImage->bBobOIPrintError;

        if(bobImage->fDebitOK == 1)
        {
            p_active_mp = get_active_mp();
            memcpy(&pc, pIPSD_pieceCount, sizeof(pc));
            memcpy(&zpc, pIPSD_pieceCount, sizeof(zpc));
            dbgTrace(DBG_LVL_INFO, "fnPrcsBobDebitCommitRsp: pc:%d", pc);
            err = trmSetPSDRegisters(p_active_mp->mp_id,
                                pc,
                                zpc,
                                m5IPSD_ascReg,
                                mStd_DescendingReg,
                                0);

            err = trmDebitCompleted(p_active_mp->mp_id, pIPSD_barcodeData.pGlob, wIPSD_barcodeDataGlobSize);
            if(err)
            {
                sprintf( DbgBuf, "fnDebitCommitRsp:trmDebitCompleted TRM Error:%u,  id:%u\n", err, p_active_mp->mp_id );
                fnDumpStringToSystemLog( DbgBuf );
            }
        }

        if(   bobImage->fBobPrintStatus != OK || bobImage->wImageLength == 0
           || bobImage->wMinPrintLength == 0 || bobImage->pImage == NULL ||
           ((CMOSTrmUploadDueReqd | TRM_UPLOAD_REQUIRED) == TRM_UPLOAD_REQUIRED) )
        {
            *outputEvtP = v41_BobDebitCommitErr;
            cmStatus.mailRunEvtPending |= BOB_EVT_MASK;
            fnSetBobState(bob_Err);

            sSystemStatus.fEjectMailPiece = TRUE;
            sSystemStatus.fCmStopPending = TRUE;
            if (cmStatus.bMediaType == MEDIA_TAPE)
            {
                (void)fnStopRun(STOP_TAPE_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
            }
            else if( eModelType >= CSD3 )
            {
                (void)fnStopRun(STOP_FDR_ONLY, 0, GRACEFUL_STOP);
            }
            else
            {   // DM300C Envelopes
                fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);

            }

            // (void)fnMailProgress(EVT_BOB_ERR, IS_RUNNING, bobImage->bBobOIPrintError);
        }
        else 
        {
            *outputEvtP = v63_BobDebitCommitRsp;


        }

    }

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Bob: Ex%x iL %d evt %d %d",
            bobImage->bBobOIPrintError, 
            bobImage->wImageLength, *outputEvtP, cmState.eCurrentState); 
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    if((CMOSTrmUploadDueReqd | TRM_UPLOAD_REQUIRED) == TRM_UPLOAD_REQUIRED)
        SendBaseEventToTablet(BASE_EVENT_UPLOAD_REQ, NULL);

    return OK;

} // fnPrcsBobDebitCommitRsp

/**********************************************************************
FUNCTION NAME: 
    fnDebitCommitRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
HISTORY:
 2010.07.17 Clarisa Bellamy - Set the cmBobState to PrecreateStarted BEFORE the 
                SCM_PREPARE_FOR_MAILRUN message is sent to bob, instead of after.
**********************************************************************/
int fnDebitCommitRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    BOOL simulationWOInk = FALSE;
    MAIL_SIMULATION simuParmsCopy;

    JPRINT_IMAGE    *bobImage = (JPRINT_IMAGE *)rMsgP->IntertaskUnion.PointerData.pData;
    active_mp_log_t *p_active_mp;

    switch(subPrtMode)
    {
    case CM_INDICIA_MODE:
    case CM_DIFF_WEIGH_MODE:
    case CM_SIMULATION_MODE:
      // We are debiting the PSD: for the print manager to update the power
      // fail flag by setting it to Indicia mode
      prtData.bMode = CM_INDICIA_MODE;
      break;
    default:
      prtData.bMode = subPrtMode;
      break;
    }
    
    simulationWOInk = ((subPrtMode == CM_SIMULATION_MODE) && fnGetMailSimuPara(&simuParmsCopy) && (simuParmsCopy.blankImage == TRUE));

    prtData.lwImageLength = bobImage->wImageLength;
    prtData.lwMinPrintLength = bobImage->wMinPrintLength;
    if (simulationWOInk)
    { //use blank image
        prtData.pImage = &blankImageBuffer[0];
    }
    else
    {// use image from SCM
        prtData.pImage = bobImage->pImage;
    }


    (void)OSSendIntertask(BJCTRL, CM, CP_PRINT, GLOBAL_PTR_DATA, &prtData, sizeof(PRINT_DATA));


    //TODO - confirm checking for WOW not necessary
    // Do not start Indicium's pre-create here in WOW mode
    // if(fnIsWOWMail() == FALSE)
    // Start precreate for next piece

    fnSetBobState(bob_PrecreateStarted);
    (void)OSSendIntertask(SCM, CM, SCM_PREPARE_FOR_MAILRUN, NO_DATA, NULL, 0);

    // Information only 
    (void)OSSendIntertask(SYS, CM, CS_PRINT_STARTED, NO_DATA, NULL, 0);

    ////////////////////////////////////////////////////////////////
    //Update XR
    p_active_mp = get_active_mp();
    if(trmStartPrint(p_active_mp->mp_id))
    {
        sprintf( DbgBuf,  "trmStartPrint: mpid %d", p_active_mp->mp_id);
        fnDumpStringToSystemLog(DbgBuf);
    }
    ////////////////////////////////////////////////////////////////
    return OK;

} // fnbDebitCommitRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsOitPmMtnc

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsOitPmMtnc(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    *outputEvtP = v58_OitReqMtnc;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Evt %d stt %d", *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsOitPmMtnc

/**********************************************************************
FUNCTION NAME: 
    fnOitMtnc

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnOitMtnc(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    return fnGrantPmMtnc((emMtncType)rMsgP->IntertaskUnion.bByteData[POS_0]);
} // fnOitMtnc

/**********************************************************************
FUNCTION NAME: 
    fnPrcsOitDiagActn

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsOitDiagActn(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    *outputEvtP = v57_DiagActn;

    bLastDiagReq = rMsgP->bMsgId;
    fFeederDiag = FALSE;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Evt %d stt %d", *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsOitDiagActn 

/**********************************************************************
FUNCTION NAME: 
    fnOitDiagActn

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnOitDiagActn(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    if (!fFeederDiag)
    {
        // Send to PM
        (void)OSSendIntertask(BJCTRL, CM, CP_PERFORM_PM_DIAG, BYTE_DATA, rMsgP->IntertaskUnion.bByteData, LEN_2);
    }
    else
    {
        // Send to Feeder
        (void)OSSendIntertask(FDRMCPTASK, CM, CF_DIAG_CMD, rMsgP->bMsgType, &rMsgP->IntertaskUnion, sizeof(rMsgP->IntertaskUnion));
    }

    return OK;

} // fnOitDiagActn

/**********************************************************************
FUNCTION NAME: 
    fnPrcsOitClrErrReq

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsOitClrErrReq(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    ulong       rspData[LEN_2];
    emMeterModel eModelType = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    rspData[POS_0] = 0;
    rspData[POS_1] = 0;

    if( eModelType >= CSD3 )
    {
        // To deal with combination errors clear out the
        // feeder errors.  For example, a combination
        // error would be a feeder cover open error
        // combined with a PM jam.
        cmStatus.mailRunEvtPending &= ~FDR_EVT_MASK;
        cmStatus.fdrErrState.bStatus = 0;
        cmStatus.fdrErrState.bErr = FDR_OK;
        cmStatus.fdrErrState.bJamCode = 0;
    }

    if(rMsgP->IntertaskUnion.lwLongData[POS_0] == PM_MSG_ERR) 
    {
        (void)OSSendIntertask(OIT, CM, CO_CLR_ERR_RSP, LONG_DATA, rspData, LEN_2 * sizeof(ulong));
        *outputEvtP = v0_Invalid;
    }
    else if (rMsgP->IntertaskUnion.lwLongData[POS_0] == PM_PAPER_ERR &&
            rMsgP->IntertaskUnion.lwLongData[POS_1] == E_NO_PAPER)
    {
        cmStatus.pmErrState.lwPaperErr &= ~(1 << (E_NO_PAPER - PAPER_ERR_SHIFT));
        if (cmStatus.pmErrState.lwPaperErr == 0)
            cmStatus.pmErrState.lwStatus &= ~IPM_PAPER_ERR;
        (void)OSSendIntertask(OIT, CM, CO_CLR_ERR_RSP, LONG_DATA, rspData, LEN_2 * sizeof(ulong));
        *outputEvtP = v0_Invalid;
    }
    else if (rMsgP->IntertaskUnion.lwLongData[POS_0] == PM_PAPER_ERR &&
            rMsgP->IntertaskUnion.lwLongData[POS_1] == E_PAPER_SKEW)
    {
        cmStatus.pmErrState.lwPaperErr &= ~(1 << (E_PAPER_SKEW - PAPER_ERR_SHIFT));
        if (cmStatus.pmErrState.lwPaperErr == 0)
            cmStatus.pmErrState.lwStatus &= ~IPM_PAPER_ERR;
        (void)OSSendIntertask(OIT, CM, CO_CLR_ERR_RSP, LONG_DATA, rspData, LEN_2 * sizeof(ulong));
        *outputEvtP = v0_Invalid;
    }
    else if (rMsgP->IntertaskUnion.lwLongData[POS_0] == PM_PAPER_ERR &&
            rMsgP->IntertaskUnion.lwLongData[POS_1] == E_NO_PRINT)
    {
        cmStatus.pmErrState.lwPaperErr &= ~(1 << (E_NO_PRINT - PAPER_ERR_SHIFT));
        // clear motion sensor data bits
        cmStatus.snsrState.wSensorData &= ~(S1_MASK | S2_MASK | S3_MASK);

        if (cmStatus.pmErrState.lwPaperErr == 0)
            cmStatus.pmErrState.lwStatus &= ~IPM_PAPER_ERR;
        (void)OSSendIntertask(OIT, CM, CO_CLR_ERR_RSP, LONG_DATA, rspData, LEN_2 * sizeof(ulong));
        *outputEvtP = v53_ClrErrReq;
    }
    else if (rMsgP->IntertaskUnion.lwLongData[POS_0] == PM_PAPER_ERR &&
            rMsgP->IntertaskUnion.lwLongData[POS_1] == E_PIECE_TOO_SHORT)
    {
        cmStatus.pmErrState.lwPaperErr &= ~(1 << (E_PIECE_TOO_SHORT - PAPER_ERR_SHIFT));
        if (cmStatus.pmErrState.lwPaperErr == 0)
            cmStatus.pmErrState.lwStatus &= ~IPM_PAPER_ERR;
        (void)OSSendIntertask(OIT, CM, CO_CLR_ERR_RSP, LONG_DATA, rspData, LEN_2 * sizeof(ulong));
        *outputEvtP = v53_ClrErrReq;
    }
    else if (rMsgP->IntertaskUnion.lwLongData[POS_0] == PM_PAPER_ERR &&
            rMsgP->IntertaskUnion.lwLongData[POS_1] == E_JAM)
    {
        cmStatus.pmErrState.lwPaperErr &= ~(1 << (E_JAM - PAPER_ERR_SHIFT));
        if (cmStatus.pmErrState.lwPaperErr == 0)
            cmStatus.pmErrState.lwStatus &= ~IPM_PAPER_ERR;
        (void)OSSendIntertask(OIT, CM, CO_CLR_ERR_RSP, LONG_DATA, rspData, LEN_2 * sizeof(ulong));
        *outputEvtP = v53_ClrErrReq;
    }

    else if (rMsgP->IntertaskUnion.lwLongData[POS_0] == ERTYPE_FEEDER_ERR)
    {
        cmStatus.mailRunEvtPending &= ~FDR_EVT_MASK;
        cmStatus.fdrErrState.bStatus = 0;
        cmStatus.fdrErrState.bErr = FDR_OK;
        cmStatus.fdrErrState.bJamCode = 0;
        (void)OSSendIntertask(OIT, CM, CO_CLR_ERR_RSP, LONG_DATA, rspData, LEN_2 * sizeof(ulong));
        *outputEvtP = v53_ClrErrReq;
    } 

    else *outputEvtP = v53_ClrErrReq;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Ox%x ClrErr type %x code x%x Evt %d %d",
            rMsgP->bMsgId, rMsgP->IntertaskUnion.lwLongData[POS_0], 
            rMsgP->IntertaskUnion.lwLongData[POS_1], *outputEvtP, cmState.eCurrentState); 
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsOitClrErrReq 

/**********************************************************************
FUNCTION NAME: 
    fnOitClrErrReq

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnOitClrErrReq(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    emCmState   newState = fnRtnToIdle(__LINE__);

    ulong       errClrCode[LEN_2];

    errClrCode[POS_0] = rMsgP->IntertaskUnion.lwLongData[POS_0];
    errClrCode[POS_1] = rMsgP->IntertaskUnion.lwLongData[POS_1];

    if (errClrCode[POS_0] == ERTYPE_FEEDER_ERR)
    {
        *newStateP = newState;
    }
    else
    {
        fnCmClrIntErr(errClrCode[POS_0],errClrCode[POS_1]); 

        // send clear error request to PM
        (void)OSSendIntertask(BJCTRL, CM, CP_CLR_PRT_ERR, LONG_DATA, errClrCode, sizeof(errClrCode));
    }

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> PM told ClrErr x%x x%x %d",
             rMsgP->IntertaskUnion.lwLongData[POS_0], 
             rMsgP->IntertaskUnion.lwLongData[POS_1], cmState.eCurrentState); 
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnOitClrErrReq

/**********************************************************************
FUNCTION NAME: 
    fnClrPmErr

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
void fnClrPmErr(ulong errType, ulong errCode)
{
    ulong       errClrCode[LEN_2];

    errClrCode[POS_0] = errType;
    errClrCode[POS_1] = errCode;

    (void)fnCmClrIntErr(errClrCode[POS_0],errClrCode[POS_1]); 

    // send clear error request to PM
    (void)OSSendIntertask(BJCTRL, CM, CP_CLR_PRT_ERR, LONG_DATA, errClrCode, sizeof(errClrCode));

} // fnClrPmErr

/**********************************************************************
FUNCTION NAME: 
    fnPrcsOitEject

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsOitEject(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    // ejtFlg = !ejtFlg;
    *outputEvtP = v64_EjtReq;

    return OK;

} // fnPrcsOitEject

int     fnPrcsOitFdrDiagReq(
        INTERTASK *rMsgP,                   // (I) Received msg 
        emCmStateEvent *outputEvtP)             // (O) Embedded event in the msg
{
    *outputEvtP = v57_DiagActn;

    bLastDiagReq = rMsgP->IntertaskUnion.bByteData[0];
    fFeederDiag = TRUE;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Evt %d stt %d", *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;
} // fnPrcsOitFdrDiagReq

/**********************************************************************
FUNCTION NAME: 
    fnEject

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnEject(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    if(*newStateP == s31_BobError)
    {
        #ifdef DBG_CM
        sprintf(DbgBuf, "CM>> ###BOB error on Eject");
        fnDumpStringToSystemLog(DbgBuf);
        #endif

        *newStateP = s4_NmlIdle;
    }

    (void)OSSendIntertask(BJCTRL, CM, CP_EJECT, NO_DATA, NULL, 0);
    (void)fnStopRun(STOP_FDR_ONLY, 0, GRACEFUL_STOP);

    return OK;

} // fnEject

/**********************************************************************
FUNCTION NAME: 
    fnPrcsOitSetMargin

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsOitSetMargin(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    ulong   rspData[LEN_2];
    uchar   *byteData = rMsgP->IntertaskUnion.bByteData;

    rspData[POS_0] = 0;
    rspData[POS_1] = 0;
    cmStatus.rightMargin = byteData[POS_0];
    cmStatus.rightMarginOffset = byteData[POS_1];
    cmStatus.topMargin = byteData[POS_2];
    cmStatus.topMarginOffset = byteData[POS_3];

    (void)OSSendIntertask(OIT, CM, CO_SET_MARGIN_RSP, LONG_DATA, rspData, 2 * sizeof(ulong));

    return OK;

} // fnPrcsOitSetMargin

/**********************************************************************
FUNCTION NAME: 
    fnPrcsOitRateReady

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsOitRateReady(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    *outputEvtP = v0_Invalid;

    // for now just send the appropriate response.  
    // full support will come soon...
    (void)OSSendIntertask(OIT,CM,CO_RATE_READY_RSP,NO_DATA,NULL,0);

    return OK;

} // fnPrcsOitRateReady

/**********************************************************************
FUNCTION NAME: 
    fnPrcsOitRateNotReady

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsOitRateNotReady(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    emMeterModel eModelType = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    *outputEvtP = v0_Invalid;
    
    // for now just send the appropriate response.  
    // full support will come soon...
    (void)OSSendIntertask(OIT,CM,CO_RATE_NOT_READY_RSP,NO_DATA,NULL,0);
//  if (eModelType == CSD2)
//  {       
//      *outputEvtP = v48_SetDisableMode;
//  }
    
    // DSD 06/12/2007
    if (!sSystemStatus.fEjectMailPiece && !sSystemStatus.fCmStopPending && 
        sSystemStatus.fRunning && !sSystemStatus.fStopping)
    { 
        sSystemStatus.fEjectMailPiece = TRUE;
        sSystemStatus.fCmStopPending = TRUE;
        if (cmStatus.bMediaType == MEDIA_TAPE)
        {
            (void)fnStopRun(STOP_TAPE_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
        }
        else if( eModelType >= CSD3 )
        {
            (void)fnStopRun(STOP_FDR_ONLY, 0, GRACEFUL_STOP);
        }
        else
        {   // CSD2 Envelopes
            fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);
        }
    }
    
    return OK;

} // fnPrcsOitRateNotReady

/**********************************************************************
FUNCTION NAME: 
    fnStCks_Idle

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnStCks_Idle(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    *newStateP = fnRtnToIdle(__LINE__);

    return OK;

} // fnStCks_Idle

/**********************************************************************
FUNCTION NAME: 
    fnPrcsInitRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsInitRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{   
#ifdef NO_PRINTER //TODO Remove when we get a printer
    rMsgP->IntertaskUnion.lwLongData[POS_0] = 0;
    rMsgP->IntertaskUnion.lwLongData[POS_1] = 0;
    rMsgP->IntertaskUnion.lwLongData[POS_2] = 0;
#endif
    // TESTCODE
    /* -------------------
    int testFlg = 0;
    ulong dummyMsg[3];


    switch(testFlg)
    {
    case 0:
        dummyMsg[0] = PM_PHEAD_ERR;
        dummyMsg[1] = E_PHEAD_CHECKSUM;
        dummyMsg[2] = 0;
        (void)OSSendIntertask(CM, BJCTRL, PC_PRT_STATUS, LONG_DATA, dummyMsg, 3 * sizeof(ulong));
        break;

    case 1:
        // Test fatal error 
        rMsgP->IntertaskUnion.lwLongData[POS_0] = PM_FATAL_ERR;
        rMsgP->IntertaskUnion.lwLongData[POS_1] = F_PUMP_POS;
        break;

    }
     ------------------- */
    // TESTCODE

    if(rMsgP->IntertaskUnion.lwLongData[POS_0] == OK)
    {
        if(rMsgP->IntertaskUnion.lwLongData[POS_2])
        {
            *outputEvtP = v6_PmReqPrtMtnc;
            cmStatus.mtncType = (UINT8)rMsgP->IntertaskUnion.lwLongData[POS_2];
        }
        else
        {
            *outputEvtP = v9_PmInitRspNoMtnc;
        }

       
        if(initRspCnt ==0)
        {
            (void)OSSendIntertask(BJCTRL, CM, CP_GET_PRTHEAD_DATA, GLOBAL_PTR_DATA, &cmStatus.prtHeadData, sizeof(PRINT_HEAD_DATA));
            (void)OSSendIntertask(BJCTRL, CM, CP_GET_INKTANK_DATA, GLOBAL_PTR_DATA, &cmStatus.inkTankData, sizeof(INK_TANK_DATA));
            (void)OSSendIntertask(BJCTRL, CM, CP_GET_PM_VER, NO_DATA, NULL, 0);

        }

        #ifdef DBG_CM
        sprintf(DbgBuf, "CM>> Msg ID x%x InitR Err x%x mtnc %uld Time %uld Evt %d St %d",
            rMsgP->bMsgId,
            rMsgP->IntertaskUnion.lwLongData[POS_1], 
            rMsgP->IntertaskUnion.lwLongData[POS_2], 
            getMsClock(), *outputEvtP, cmState.eCurrentState);
        fnDumpStringToSystemLog(DbgBuf);
        #endif
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsInitRsp, rMsgP, rMsgP->IntertaskUnion.lwLongData[POS_0], rMsgP->IntertaskUnion.lwLongData[POS_1], outputEvtP);
    }

    return OK;
} // fnPrcsInitRsp

/**********************************************************************
FUNCTION NAME: 
    fnInitRsp_Idle

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnInitRsp_Idle(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    if(initRspFlg == INIT_PENDING)
    {
        // check the manufacturer type
        fnCMCheckInkTankMfg();
        
        // Send init response to SYS if no maintenance requested, or mtnc is completed
        (void)OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE, OS_OR);
        (void)OSSendIntertask(SYS, CM, CS_INIT_RSP, NO_DATA, NULL, 0);

        initRspFlg = INIT_COMPLETE;
    }

    if (fPendingPrtDisable == TRUE)
    {
        fPendingPrtDisable = FALSE;
        subPrtMode = CM_DISABLED_MODE;
        cmStatus.opMode = DISABLE_MODE;
        fnSetModeCommon();
        (void)fnFdrSetMode(CM_DISABLED_MODE);
        (void)fnPmSetMode(DISABLE_MODE, 0, 0);
    }

    // Report to OIT about the sensor change
    (void)fnMailProgress(EVT_TOP_COVER_STATUS_UPDATE, IS_STOPPED, OK);

    if(cmStatus.pmErrState.lwStatus & IPSOC_ERR) 
        (void)fnPsocErrRsp(inputEvt, rMsgP, newStateP);

    (void)fnRptPendingEvts();

    if(*newStateP != s46_HdInkErr) 
    {
        *newStateP = fnRtnToIdle(__LINE__);
    }

    return OK;
    
} // fnInitRsp_Idle()

/**********************************************************************
FUNCTION NAME: 
    fnInitRsp_FtlErr

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnInitRsp_FtlErr(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    if(initRspFlg == INIT_PENDING)
    {
        // Send init response to SYS if no maintenance requested, or mtnc is completed
        (void)OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE, OS_OR);
        (void)OSSendIntertask(SYS, CM, CS_INIT_RSP, NO_DATA, NULL, 0);

        initRspFlg = INIT_COMPLETE;
    }

    // Report to OIT about the fatal error. Do not report other events as fatal error
    // takes higher priority
    (void)fnMailProgress(EVT_PM_FATAL_ERR, IS_STOPPED, OK);

    return OK;
} // fnInitRsp_FtlErr()

/**********************************************************************
FUNCTION NAME: 
    fnInitRsp_CvrOpen

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnInitRsp_CvrOpen(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    if(initRspFlg == INIT_PENDING)
    {
        // check the manufacturer type
        fnCMCheckInkTankMfg();
        
        // Send init response to SYS if no maintenance requested, or mtnc is completed
        (void)OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE, OS_OR);
        (void)OSSendIntertask(SYS, CM, CS_INIT_RSP, NO_DATA, NULL, 0);

        initRspFlg = INIT_COMPLETE;
    }

    (void)fnMailProgress(EVT_TOP_COVER_STATUS_UPDATE, IS_STOPPED, OK);

    return OK;
} // fnInitRsp_CvrOpen()

/**********************************************************************
FUNCTION NAME: 
    fnInitRsp_JamOpen

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnInitRsp_JamOpen(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    if(initRspFlg == INIT_PENDING)
    {
        // check the manufacturer type
        fnCMCheckInkTankMfg();
        
        // Send init response to SYS if no maintenance requested, or mtnc is completed
        (void)OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE, OS_OR);
        (void)OSSendIntertask(SYS, CM, CS_INIT_RSP, NO_DATA, NULL, 0);

        initRspFlg = INIT_COMPLETE;
    }

    (void)fnMailProgress(EVT_JAM_LEVER_STATUS_UPDATE, IS_STOPPED, OK);

    return OK;
} // fnInitRsp_JamOpen()

/**********************************************************************
FUNCTION NAME: 
    fnInitRsp_HeadErr

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnInitRsp_HeadErr(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    if(initRspFlg == INIT_PENDING)
    {
        // Send init response to SYS if no maintenance requested, or mtnc is completed
        (void)OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE, OS_OR);
        (void)OSSendIntertask(SYS, CM, CS_INIT_RSP, NO_DATA, NULL, 0);

        initRspFlg = INIT_COMPLETE;
    }

    (void)fnHdInkErr(inputEvt, rMsgP, newStateP);

    return OK;
} // fnInitRsp_HeadErr()

/**********************************************************************
FUNCTION NAME: 
    fnInitRsp_Mtnc

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnInitRsp_Mtnc(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    int         rtn = OK;

    if(cmStatus.mtncType == NO_PURGE) 
    {
        *newStateP = fnRtnToIdle(__LINE__);
    }
    else 
    {
        rtn = fnGrantPmMtnc(cmStatus.mtncType);
    }

    return rtn;
    
} // fnInitRsp_Mtnc()

/**********************************************************************
FUNCTION NAME: 
    fnPrcsMtncRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsMtncRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    ulong                   rspData[LEN_2];
    MAINTENANCE_RESPONSE    *mtncRsp;

    mtncRsp = (MAINTENANCE_RESPONSE *)rMsgP->IntertaskUnion.PointerData.pData;

    #ifdef DBG_CM
    // TESTCODE
    /* ------------------
    if(mtncRsp->bMaintenanceType == BF_PRT_PURGE)
    {
        if(errCnt == 5)
        {
            mtncRsp->lwStatus = PM_INK_OR_TANK_ERR;
            mtncRsp->lwErrorCode = E_INKTANK_ID;
        }
        errCnt++;
    }
    -------------------- */
    // TESTCODE

    sprintf(DbgBuf, "Msg ID x%x MRsp Err x%x state %d",
            rMsgP->bMsgId, mtncRsp->lwErrorCode, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    if(    (cmState.eCurrentState == s18_RplFnsh_5V_ON || cmStatus.pendingRpl == TRUE) 
        && (mtncRsp->bMaintenanceType == RPL_PURGE) )
    {
        cmStatus.pendingRpl = FALSE;
        
        // copy the data from the Print Manager area to the Control Manager area
        (void)memcpy(&cmStatus.prtHeadData, cmStatus.pmHeadDataP, sizeof(PRINT_HEAD_DATA));
        (void)memcpy(&cmStatus.inkTankData, cmStatus.pmTankDataP, sizeof(INK_TANK_DATA));

        // check the manufacturer type as long as there wasn't some type of error
        switch (mtncRsp->lwStatus)
        {
            case OK:
            case PM_WARNING:
            case PM_MSG_ERR:
            case PM_PAPER_ERR:
            default:
                fnCMCheckInkTankMfg();
                break;

            case PM_PHEAD_ERR:
            case PM_INK_OR_TANK_ERR:
            case PM_FATAL_ERR:
                // since there is a print head or ink tank problem, set the manufacturer type
                // to unknown because the data in cmStatus could be for the previous print head
                // and/or ink tank
                ucInkTankMfgType = UNKNOWN_MFG;
                break;
        }

        // Send response to OIT
        rspData[POS_0] = mtncRsp->lwStatus;
        rspData[POS_1] = mtncRsp->lwErrorCode;
        (void)OSSendIntertask(OIT, CM, CO_RPL_CMPLT_RSP, LONG_DATA, rspData, 2 * sizeof(ulong));

        (void)OSSendIntertask(BJCTRL, CM, CP_GET_PRTHEAD_DATA, GLOBAL_PTR_DATA, &cmStatus.prtHeadData, sizeof(PRINT_HEAD_DATA));
        (void)OSSendIntertask(BJCTRL, CM, CP_GET_INKTANK_DATA, GLOBAL_PTR_DATA, &cmStatus.inkTankData, sizeof(INK_TANK_DATA));
    }

#ifdef TEST_PAUSE_FOR_MAINTENANCE
    if(TRUE)
#else
    if(mtncRsp->lwStatus == OK)
#endif
    {
        *outputEvtP = v7_PmMtncRsp;
        if (sSystemStatus.fSysStopPending)
        {
            (void) fnStopCompleteReturnToIdle(*outputEvtP);
            *outputEvtP = v0_Invalid;
        }
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsMtncRsp, rMsgP, mtncRsp->lwStatus, mtncRsp->lwErrorCode, outputEvtP);
    }

    SendBaseEventToTablet(BASE_EVENT_MAINTENANCE_COMPLETE, NULL);

	// The transport stop running and maintenance is finished after previous mail,  
	// restart to report paper in transport error
	fSkipPaperInTranport = FALSE;
    return OK;

} // end of fnPrcsMtncRsp()

/**********************************************************************
FUNCTION NAME: 
    fnMtncRsp_Stg

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnMtncRsp_Stg(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg 
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    return fnPmRun(RUN_THRU_S3);
} // fnMtncRsp_Stg()

/**********************************************************************
FUNCTION NAME: 
    fnMtncRsp_Idle

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnMtncRsp_Idle(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    *newStateP = fnRtnToIdle(__LINE__);

    return OK;

} // fnMtncRsp_Idle

/**********************************************************************
FUNCTION NAME:
    fnMtncRsp_RplFin

DESCRIPTION:

INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnMtncRsp_RplFin(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg
    emCmState           *newStateP)     // (I/O) New State, not committed yet
{
    *newStateP = fnRtnToIdle(__LINE__);
     SendBaseEventToTablet(BASE_EVENT_PH_REPLACE_DONE, NULL);

    return OK;

} // fnMtncRsp_RplFin

/**********************************************************************
FUNCTION NAME: 
    fnMtncRsp_Oit

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnMtncRsp_Oit(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    ulong                   rspData[LEN_2];
    MAINTENANCE_RESPONSE    *mtncRsp = rMsgP->IntertaskUnion.PointerData.pData;

    rspData[POS_0] = mtncRsp->lwStatus;
    rspData[POS_1] = mtncRsp->lwErrorCode;

    *newStateP = fnRtnToIdle(__LINE__);
    
    // copy the data from the Print Manager area to the Control Manager area
    (void)memcpy(&cmStatus.prtHeadData, cmStatus.pmHeadDataP, sizeof(PRINT_HEAD_DATA));
    (void)memcpy(&cmStatus.inkTankData, cmStatus.pmTankDataP, sizeof(INK_TANK_DATA));

    // check the manufacturer type as long as there wasn't some type of error
    switch (mtncRsp->lwStatus)
    {
        case OK:
        case PM_WARNING:
        case PM_MSG_ERR:
        case PM_PAPER_ERR:
        default:
            fnCMCheckInkTankMfg();
            break;

        case PM_PHEAD_ERR:
        case PM_INK_OR_TANK_ERR:
        case PM_FATAL_ERR:
            // since there is a print head or ink tank problem, set the manufacturer type
            // to unknown because the data in cmStatus could be for the previous print head
            // and/or ink tank
            ucInkTankMfgType = UNKNOWN_MFG;
            break;
    }

    (void)OSSendIntertask(OIT, CM, CO_MTNC_RSP, LONG_DATA, rspData, sizeof(rspData));

    if(cmStatus.cvrState || cmStatus.jamLvrState) 
        (void)fnGrantPmMtnc(CAP_SPIT);

    return OK;

} // fnMtncRsp_Oit

/*****************************************************************************
*
* PSOC messages processing functions
*
*****************************************************************************/

/**********************************************************************
FUNCTION NAME: 
    fnPrcsPsocReseedRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsPsocReseedRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    // psocRsp = rMsgP->bMsgId;
    psocStatus = rMsgP->IntertaskUnion.bByteData[POS_0];    

    if(psocStatus) 
    {   
        // report PSOC error either in CS_INIT_RSP or in CO_GET_STTS_RSP
        // *outputEvtP = v70_PsocReseedErr; 
        cmStatus.pmErrState.lwStatus |= IPSOC_ERR;
    }

    *outputEvtP = v38_PsocReseedRsp;

    #ifdef DBG_CM
    sprintf(DbgBuf, "PSOC_GEN_SEED_RSP Evt %d st %d", *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsPsocReseedRsp

/**********************************************************************
FUNCTION NAME: 
    fnPsocCommitSeed

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPsocCommitSeed(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    char semstat;
    // Regardless of seeding success, release semaphore acquired by CM when replacement started
    semstat = OSReleaseSemaphore(PH_PSOC_SEM_ID);
    if ((semstat != (char) SUCCESSFUL) && (semstat != (char) OS_DONT_OWN_SEMAPHORE)) // OK if trying to release not acquired semaphore
    {
        dbgTrace(DBG_LVL_ERROR,"Error: Could not release semaphore for Print Head replacement: status %d\r\n", semstat);
    }

    // Ask PSOC to commit seed
    (void)OSSendIntertask(PSOC, CM, COMSEED_MSG, NO_DATA, NULL, 0);
    #ifdef DBG_CM
    sprintf(DbgBuf, "COMSEED_MSG");
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPsocCommitSeed

/**********************************************************************
FUNCTION NAME: 
    fnPrcsPsocCommitRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsPsocCommitRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    // psocRsp = rMsgP->bMsgId;
    psocStatus = rMsgP->IntertaskUnion.bByteData[POS_0];    

    if ((cmStatus.pmErrState.lwStatus & IPSOC_ERR) == 0)
    {
        if(psocStatus) 
        {
            // report PSOC error either in CS_INIT_RSP or in CO_GET_STTS_RSP
            // *outputEvtP = v37_PsocCommitErr;
            cmStatus.pmErrState.lwStatus |= IPSOC_ERR;
        }
        else
        {
            cmStatus.pmErrState.lwStatus &= ~IPSOC_ERR;
        }
    }

    *outputEvtP = v39_PsocCommitRsp;

    #ifdef DBG_CM
    sprintf(DbgBuf, "PSOC_COM_SEED_RSP Evt %d st %d", *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif
    return OK;

} // fnPrcsPsocCommitRsp

/**********************************************************************
FUNCTION NAME: 
    fnPsocCommitRsp_Rpl

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPsocCommitRsp_Rpl(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    (void)fnGrantPmMtnc(RPL_PURGE);
    return OK;
} // fnPsocCommitRsp_Rpl

/**********************************************************************
FUNCTION NAME: 
    fnPsocErrRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPsocErrRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    // psocRsp = PSC_NEED_RESEED;
    // psocStatus = PSC_NEED_RESEED;
    (void)fnMailProgress(EVT_PSOC_ERR, IS_STOPPED, OK);

    return OK;
} // fnPsocCommitRsp

#ifdef TEST_PAUSE_FOR_MAINTENANCE
int iMtncTestCnt = 0;
#endif

/*****************************************************************************
*
* PM messages processing functions
*
*****************************************************************************/
/**********************************************************************
FUNCTION NAME: 
    fnPrcsAutoReq

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsAutoReq(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    uchar       mtncReq = rMsgP->IntertaskUnion.bByteData[POS_0];   
    uchar       mtncTime = rMsgP->IntertaskUnion.bByteData[POS_1];
    emMeterModel eModelType = fnGetMeterModel();    
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    ulong lwCurrentEvent = 0;

    if(bGoToReady == TRUE)
    {
        // Do not start mail run if CM status check in the pre function of Home screen hasn't been finished,
        // thus fixes GMSE00171558 (G900 - Loss of Postage reported by customer after No Paper message and error 2482).
        // Just check whether main menu check has been finished and do not need to suspend if the event doesn't match.
        //if(OSReceiveEvents(OIT_EVENT_GROUP, MAIN_MENU_CHK_END_EVT, OS_SUSPEND, OS_AND, &lwCurrentEvent) != SUCCESSFUL)
        //{
		//	  *outputEvtP = v0_Invalid;
		//	  return OK;
        //}
        // Change to suspend to wait for the event
        OSReceiveEvents(OIT_EVENT_GROUP, MAIN_MENU_CHK_END_EVT, OS_SUSPEND, OS_AND, &lwCurrentEvent);

        sSystemStatus.fSysRunRejected = FALSE;
        sSystemStatus.fPieceInProgress = TRUE;

        /*************************************************************************************************
         1. PC_AUTOSTART has been received, indicating an envelope at S1. Before
            starting the debiting and printing process, clear this flag to prevent
            the Platform task from sending update messages to the OI that could
            kick off a screen change and the fnpIndiciaReadyCheck function, which
            access the IPSD; interferes with the debiting process, and creates a
            blank envelope.
         2. If the PLAT task wants to send an update message, it must wait until
            this flag is set again.  (When CM gets the S3 message, or when the
            fnpIndiciaReadyCheck function is called.)
         *******************************************************************************************************/
        sprintf(DbgBuf, "Clear PLAT_OK_TO_SEND_UPDATE flag");
        fnDumpStringToSystemLog(DbgBuf);
        OSSetEvents( PLAT_EVENT_GROUP, ~(UINT32)PLAT_OK_TO_SEND_UPDATE, OS_AND );

        fnStopCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);
        fnStopCMTimer(MAILRUN_CMPLT_TIMER4,__LINE__);
        fnStopCMTimer(MAILRUN_REPORT_CMPLT_TIMER_ID,__LINE__);

    #ifdef TEST_PAUSE_FOR_MAINTENANCE
        if (++iMtncTestCnt >= 3)
        {
            mtncReq = HEAD_COOLING;
            mtncTime = 5;
            iMtncTestCnt = 0;
        }
    #endif

        //if (eModelType >= DM400C)
        //  OSSuspendTask(LCDTASK);          // DSD - check to see if this is really a good idea

        if (!sSystemStatus.fProcessingPieces)       // DSD 05-14-2007 Track system running status better.  Fix Fraca 119889
            sSystemStatus.fProcessingPieces = TRUE;

        sSystemStatus.fSendMailRunComplete = TRUE;

        #ifdef DBG_CM
        sprintf(DbgBuf, "CM>> Msg ID x%x AtSt MtTp Mtnc %d state %d", rMsgP->bMsgId, mtncReq, cmState.eCurrentState);
        fnDumpStringToSystemLog(DbgBuf);
       #endif

        if ( eModelType == CSD2 )
        {
            // only way on DM300c to tell if running "Envelopes" vs "Tape" is to see if the
            // media has been set to "Tape" by the time AutoStart happens
            // DM300c tape and DM400c tape and envelope are all done off hard keys that set the media type byte
            if ( cmStatus.bMediaType == NO_MEDIA )
            {
                cmStatus.bMediaType = MEDIA_MAIL;
            }
        }

        switch(mtncReq)
        {
        case BF_PRT_PURGE:
        case PWR_PURGE:
             if (eModelType == CSD2)
             {
                 cmStatus.mtncType = mtncReq;
                 sSystemStatus.stopCause = STOP_MAINT;
                 *outputEvtP = v2_PmAtStrtReqMtnc;
             }
             else
             {
                cmStatus.mtncType = mtncReq;
                sSystemStatus.stopCause = STOP_MAINT;
                (void)fnStopRun(STOP_FDR_ONLY, 0, MTNC_STOP);
                *outputEvtP = v1_PmAtStrtReqNoMtnc;     // continue printing until the feeder has stopped
             }
             break;

        case WIPE_PURGE:
             if (eModelType == CSD2)
             {
                cmStatus.mtncType = mtncReq;
                sSystemStatus.stopCause = STOP_MAINT;
                *outputEvtP = v35_PmAtStrtPauseMtnc;
             }
             else
             {
                cmStatus.mtncType = mtncReq;
                sSystemStatus.stopCause = STOP_MAINT;
                (void)fnStopRun(STOP_FDR_ONLY, 0, MTNC_STOP);
                *outputEvtP = v1_PmAtStrtReqNoMtnc;     // continue printing until the feeder has stopped
             }
             break;

        case HEAD_COOLING:
             if (eModelType == CSD2)
             {
                 cmStatus.mtncType = mtncReq;
                 sSystemStatus.stopCause = STOP_MAINT;
                 if(mtncTime > LEN_3) // > 3 seconds
                 {
                     *outputEvtP = v36_PmAtStrtPauseCooling;
                 }
                 else
                 {
                     *outputEvtP = v35_PmAtStrtPauseMtnc;
                 }
             }
             else
             {
                 cmStatus.mtncType = mtncReq;
                 //*outputEvtP = v36_PmAtStrtPauseCooling;
                 sSystemStatus.stopCause = STOP_MAINT;
                 (void)fnStopRun(STOP_FDR_ONLY, 0, MTNC_STOP);
                 *outputEvtP = v1_PmAtStrtReqNoMtnc;        // continue printing until the feeder has stopped
             }
             break;

        default: // others are ignored
             *outputEvtP = v1_PmAtStrtReqNoMtnc;
             break;
        }

        // S1 covered, set sensor data
        cmStatus.snsrState.wSensorData &= ~(S1_MASK | S2_MASK | S3_MASK);

        // Eject the mail piece if we are stopping the feeder and the ejection flag
        // is set on conditions such as bob debit error, Rate not ready, last MP, etc.
        if( (sFeederStatus.fStopping  == TRUE) && (sSystemStatus.fEjectMailPiece == TRUE) )
        {
            (void)OSSendIntertask(BJCTRL, CM, CP_EJECT, NO_DATA, NULL, 0);

            *outputEvtP = v0_Invalid;
        }
    }
    return OK;

} // fnPrcsAutoReq

/**********************************************************************
FUNCTION NAME: 
    fnAutoReq_SysRspPnd

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnAutoReq_SysRspPnd(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Src x%x Id x%x psocStts x%x", rMsgP->bSource, rMsgP->bMsgId, psocStatus);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    //Moved to display Running on Printer Status Update message
    (void)OSSendIntertask(SYS, CM, CS_RUN_REQ, NO_DATA, NULL, 0);

    (void)fnMailProgress(EVT_MAILRUN_STATUS_UPDATE, IS_RUNNING, OK);

// LMD  sSystemStatus.fSysRunReqPending = TRUE;

    return OK; 

} // fnAutoReq_SysRspPnd

/**********************************************************************
FUNCTION NAME: 
    fnAutoReq_RunEject

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnAutoReq_RunEject(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    (void)fnMailProgress(EVT_MAILRUN_STATUS_UPDATE, IS_RUNNING, OK);

    if(*newStateP == s22_Idle) 
        *newStateP = fnRtnToIdle(__LINE__);
    return fnPmRun(RUN_THRU_S3);

} // fnAutoReq_RunEject

/**********************************************************************
FUNCTION NAME: 
    fnAutoReq_Mtnc

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnAutoReq_Mtnc(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    // (void)fnMailProgress(EVT_MAILRUN_STATUS_UPDATE, IS_RUNNING, OK); will be sent after maintenance completed

    return fnGrantPmMtnc(rMsgP->IntertaskUnion.bByteData[POS_0]);

} // fnAutoReq_Mtnc

/**********************************************************************
FUNCTION NAME: 
    fnAutoReq_PauseMtnc

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnAutoReq_PauseMtnc(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    emMeterModel eModelType = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    // (void)fnMailProgress(EVT_MAILRUN_STATUS_UPDATE, IS_RUNNING, OK); will be sent after maintenance completed

    if( (eModelType >= CSD3) && sFeederStatus.fRunning)
    {
        sSystemStatus.stopCause = STOP_MAINT;
        (void)fnStopRun(STOP_FDR_ONLY, 0, MTNC_STOP);
    }
    else if ( eModelType >= CSD3 )
    {
        sSystemStatus.stopCause = STOP_MAINT;
        //(void)fnStopRun(STOP_MAIN_ONLY, PM_STOP_AT_S3, MTNC_STOP);
        (void)OSSendIntertask(BJCTRL, CM, CP_PERFORM_PRT_MTNC, BYTE_DATA, &cmStatus.mtncType, sizeof(cmStatus.mtncType));

        //Gather ink tank data
        (void)OSSendIntertask(BJCTRL, CM, CP_GET_INKTANK_DATA, GLOBAL_PTR_DATA, &cmStatus.inkTankData, sizeof(INK_TANK_DATA));
        (void)OSSendIntertask(BJCTRL, CM, CP_GET_PRTHEAD_DATA, GLOBAL_PTR_DATA, &cmStatus.prtHeadData, sizeof(PRINT_HEAD_DATA));
    }
    else // Stop immediately for CSD2
    {
        sSystemStatus.stopCause = STOP_MAINT;
        (void)fnStopRun(STOP_MAIN_ONLY, PM_STOP_AT_S3, MTNC_STOP);
        if(cmStatus.mtncType == WIPE_PURGE)
            (void)OSWakeAfter(500);

        (void)OSSendIntertask(BJCTRL, CM, CP_PERFORM_PRT_MTNC, BYTE_DATA, &cmStatus.mtncType, sizeof(cmStatus.mtncType));

        //Gather ink tank data
        (void)OSSendIntertask(BJCTRL, CM, CP_GET_INKTANK_DATA, GLOBAL_PTR_DATA, &cmStatus.inkTankData, sizeof(INK_TANK_DATA));
        (void)OSSendIntertask(BJCTRL, CM, CP_GET_PRTHEAD_DATA, GLOBAL_PTR_DATA, &cmStatus.prtHeadData, sizeof(PRINT_HEAD_DATA));
    }

    // Start mail idle timer 

    return OK;

} // fnAutoReq_PauseMtnc

/**********************************************************************
FUNCTION NAME: 
    fnAutoReq_Reset

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnAutoReq_Reset(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//    fnStartCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);
/////////////////////////////////////////////////////////////////

    (void)OSSendIntertask(BJCTRL, CM, CP_RESET, NO_DATA, NULL, 0);
    return OK;

} // fnAutoReq_Reset

/**********************************************************************
FUNCTION NAME: 
    fnHandleSysRejectRun

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnHandleSysRejectRun(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    // called in Sys Run Rsp Pending State.
    // sys has indicated that the run either must
    // not start, or it must end if it is in progress.

        
    return OK;
}

/**********************************************************************
FUNCTION NAME: 
    fnForcedPrintDisable

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnForcedPrintDisable(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{

    // Before staged, reset PM so it won't wait RUN command forever
    if(cmState.ePreviousState == s4_NmlIdle)
    {
        (void)OSSendIntertask(BJCTRL, CM, CP_RESET, NO_DATA, NULL, 0);
        if (sSystemStatus.fRunning)
        	(void)fnStopRun(STOP_ALL, PM_STOP_NOW, GRACEFUL_STOP);
    }

    // Already staged, eject the mail piece
    else 
    {
        (void)OSSendIntertask(BJCTRL, CM, CP_EJECT, NO_DATA, NULL, 0);
        (void)fnStopRun(STOP_FDR_ONLY, 0, GRACEFUL_STOP);    
    }

    (void)fnSetDisableMode(inputEvt, rMsgP, newStateP);

    return OK;

} // fnForcedPrintDisable

/**********************************************************************
FUNCTION NAME: 
    fnPrcsRunRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsRunRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    RUN_RESPONSE        *runRsp = (RUN_RESPONSE *)rMsgP->IntertaskUnion.PointerData.pData;
    emMeterModel        meterModel;

//    trm_mem_t *p_trm_mem = get_trm_bucket(trmGetActiveBucket());


#ifdef NO_PRINTER //TODO JAH Remove when we have printer
    runRsp->lwStatus = 0;
    runRsp->lwErrorCode = 0;
    runRsp->lwMotionSensorData = 7;
    runRsp->bRunType = 0;
#endif //TODO JAH Remove when we have printer

    meterModel = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    //set sensor data
    cmStatus.snsrState.wSensorData &= ~(S1_MASK | S2_MASK | S3_MASK);

//    if(runRsp->lwStatus == OK || runRsp->lwErrorCode == PM_MSG_ERR)
    if(runRsp->lwStatus == OK)
    {
        
        cmStatus.fIsRunning = TRUE;
        *outputEvtP = v4_PmRunRsp;

        if (!sSystemStatus.fCancelStart)
        {
            if (sSystemStatus.fRestartPending)
            {
                sSystemStatus.fRestartPending = FALSE;
                (void)OSSendIntertask(FDRMCPTASK,CM,CF_CM_RUNNING,NO_DATA,NULL,0);
            }

//            if (sSystemStatus.fEjectMailPiece || sSystemStatus.fSysStopPending)    
//- ML - Based on Email from Raymond 6/22/09 to try to handle Graceful stop and allow last piece to print.
            if (sSystemStatus.fEjectMailPiece)
            {
                *outputEvtP = v64_EjtReq;
                sSystemStatus.fPieceInProgress = FALSE;
                sSystemStatus.fCmStopPending = TRUE;
                if (cmStatus.bMediaType == MEDIA_TAPE)
                {
                    (void)fnStopRun(STOP_TAPE_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
                }
                else if ( (meterModel >= CSD3) && sFeederStatus.fRunning)
                {
                    (void)fnStopRun(STOP_FDR_ONLY, 0, GRACEFUL_STOP);
                }
                else if (meterModel >= CSD3)
                {
                    (void)fnStopRun(STOP_MAIN_ONLY,PM_STOP_NOW,GRACEFUL_STOP);
                }
                else
                {   // CSD2 Envelopes
                    fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);
                }
            }
            else
            {
                if(subPrtMode == CM_SEAL_ONLY_MODE)
                {
                    sSystemStatus.fPieceInProgress = FALSE;
                    if (meterModel == CSD2)
                        fnStartCMMailRunCompleteTimer(NORMAL_STOP,__LINE__);
                }
                if(runRsp->bRunType == RUN_THRU_S3 && subPrtMode != CM_SEAL_ONLY_MODE) //  && fnCmGetCrntState() != s60_4MailRunPnd 
                {
                    if( fnGetBobState() == bob_Precreated )
                    {
                        (void)OSSendIntertask(SCM, CM, SCM_MAIL_IN_PROGRESS, NO_DATA, NULL, 0);
                        fnSetBobState(bob_CommitStarted);
                    }
                    else
                    {
                        fnCmCommitNewState(v4_PmRunRsp,s31_BobError);

                        if ( fnGetBobState() != bob_Err )
                        {                                                   
                            #ifdef DBG_CM
                            sprintf(DbgBuf, "Precreate did not catch up");
                            fnDumpStringToSystemLog(DbgBuf);
                            #endif
                        }
                        else
                        {
                            #ifdef DBG_CM
                            sprintf(DbgBuf, "Precreate failed");
                            fnDumpStringToSystemLog(DbgBuf);
                            #endif
                        }
                    }
                }
            }
        }
        else
        {
            *outputEvtP = v0_Invalid;
        }
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsRunRsp, rMsgP, runRsp->lwStatus, runRsp->lwErrorCode, outputEvtP);

    }

    // Information only 
    if( (fnCmGetCrntState() != s60_4MailRunPnd)  && (fnCmGetCrntState() != s67_TapeRunPnd) )
    {
        (void)OSSendIntertask(SYS, CM, CS_S3_COVERED, NO_DATA, NULL, 0);

        //
//      if(p_trm_mem->header.num_records_in_use >= CFG_NUM_RECORDS)
//      {
//          //TODO: Write  S3 Covered
//          trmWriteTrmBucket(slot);
//      }

        /*************************************************************************************************
         1. Once the CS_S3_COVERED message is sent, the debiting/printing process 
            has advanced enough that the platform update message being sent to the
            OI will not interfere with the debit.  
        *******************************************************************************************************/
        sprintf(DbgBuf, "Set PLAT_OK_TO_SEND_UPDATE flag");
        fnDumpStringToSystemLog(DbgBuf);
        OSSetEvents( PLAT_EVENT_GROUP, (unsigned long)PLAT_OK_TO_SEND_UPDATE, OS_OR );
    }

    if(subPrtMode == CM_SEAL_ONLY_MODE) //  && meterModel == DM300C)  
    {
/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//      fnStartCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);
////////////////////////////////////////////////////////////////

        if(cmStatus.cvrState) 
            (void)fnGrantPmMtnc(CAP_SPIT);
    }

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x RunR Err x%x evt %d st %d",
            rMsgP->bMsgId, rMsgP->IntertaskUnion.lwLongData[POS_1], *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsRunRsp

/**********************************************************************
FUNCTION NAME: 
    fnRunRsp_SlIdle

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnRunRsp_SlIdle(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    (void)fnMailProgress(EVT_MAIL_PIECE_PROCESSED, WAITING_FOR_MAIL, OK);     

    if(cmStatus.cvrState)
    {
        *newStateP = s20_PreCvr;
    }

    // Send delayed events
    (void)fnRptPendingEvts();

    return OK;

} // fnRunRsp_SlIdle

/**********************************************************************
FUNCTION NAME: 
    fnPrcsPrtRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsPrtRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    static PRINT_RESPONSE   *prtRspP;
           BOOL             fLastPiece = FALSE;

    extern BOOL             fLowFundsWarningPending;
    extern BOOL             fLowFundsWarningDisplayed;
    extern BOOL             fLowPieceCountWarningPending;
    extern BOOL             fLowPieceCountWarningDisplayed;
    
    extern BOOL             fnPermitMailPrintComplete(void);
    extern BOOL             fnKipPasswdCountReached(void);
    extern BOOL             fnIsRateTrackable(void);
    emMeterModel            meterModel;
    active_mp_log_t *p_active_mp;
    short err = SUCCESS;

    meterModel = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    //if (meterModel >= CSD3)
    //  OSResumeTask(LCDTASK);           // DSD - check to see if this is really a good idea

    prtRspP = (PRINT_RESPONSE *)rMsgP->IntertaskUnion.PointerData.pData;
#ifdef NO_PRINTER //TODO JAH Remove when we have printer
    prtRspP->lwStatus = 0;
    prtRspP->lwErrorCode = 0;
//    unsigned long    lwMotionSensorData;
//    unsigned long    lwMailPieceLength;
//    unsigned long    lwPrintLength;
//    unsigned long    lwInkDropsUsed;
//    unsigned long    lwPrintTime;
#endif //TODO JAH Remove when we have printer


    //set sensor data
    cmStatus.snsrState.wSensorData &= ~(S1_MASK | S2_MASK | S3_MASK);

    // cmStatus.snsrState.wSensorData |= prtRspP->lwMotionSensorData & (S1_MASK | S2_MASK | S3_MASK);

    sSystemStatus.fPieceInProgress = FALSE;

    if(prtRspP->lwStatus == OK)
    {
        // update piece remaining count
        if (cmStatus.totalPieceCount > 0)
            cmStatus.totalPieceCount--;

        // check if the low funds warning is the first time or not
        // Does not need to check Bob error because it won't come to here, i.e., no print for Bob error
        // The bobErrCode will be cleared in fnMailProgess(EVT_MAIL_PIECE_PROCESSED)...
        // if(cmStatus.bobErrCode != 0 && cmStatus.bobPreErrCode != cmStatus.bobErrCode) *outputEvtP = v29_BobPndWrng;
        if(cmStatus.bobErrCode == SCM_LOW_FUNDS_WARNING && fLowFundsWarningPending == FALSE && fLowFundsWarningDisplayed == FALSE) 
        {
             *outputEvtP = v29_BobPndWrng;
        }
        else if(cmStatus.bobErrCode == SCM_PC_END_OF_LIFE_WARNING && fLowPieceCountWarningPending == FALSE && fLowPieceCountWarningDisplayed == FALSE) 
        {
             *outputEvtP = v29_BobPndWrng;
        }
/*
        else if(fnKipPasswdCountReached() || fnIsRateTrackable()) 
        {    
             // If KIP count reached for France or trackable piece for Germany,  
             // hold next mail piece so OI can set disable mode
             *outputEvtP = v27_PmKeepWrng;    
        }
*/
        else
            *outputEvtP = v5_PmPrtRsp;

        if(subPrtMode == CM_PERMIT_MODE)  
        { // REVISIT - Call function to report sucessful count
            (void)fnPermitMailPrintComplete();
        }

        //TODO - restore Bob code call
#if 1 //disable Bob call because of crash in bobReply when indicia is printed on DB model
        (void)OSSendIntertask( SCM, CM, SCRIPT_PRINT_COMPLETE, NO_DATA, NULL_PTR, 0 );
#endif
        if (subPrtMode == CM_REPORT_MODE)
        {
            fnSetMailProgressState(PF_PRINT_DONE); //Canon's PM will set it in new version 0132.0012
        }

        ///////////////////////////////////////////////////////////
        //Update XR: Printing complete
        p_active_mp = get_active_mp();
        err = trmEndPrint(p_active_mp->mp_id);
        if(err)
        {
    #ifdef DBG_CM
            sprintf(DbgBuf, "trmEndPrint failed: mpid: %d, err:%u", p_active_mp->mp_id, err);
            fnDumpStringToSystemLog(DbgBuf);
    #endif
        }
        //TODO: Handle multiple pieces in transport
        //clear_active_mp_log(0);
        //////////////////////////////////////////////////////////
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsPrtRsp, rMsgP, prtRspP->lwStatus, prtRspP->lwErrorCode, outputEvtP);
    }

    if (cmStatus.bMediaType == MEDIA_MAIL)
    {
        if (subPrtMode == CM_DIFF_WEIGH_MODE)
            fLastPiece = TRUE;
        else if (cmStatus.totalPieceCount == 0 && subPrtMode == CM_REPORT_MODE)
            fLastPiece = TRUE;
        else if (cmStatus.totalPieceCount == 0 && subPrtMode == CM_TEST_PAT_MODE)
            fLastPiece = TRUE;
        else if ( (meterModel >= CSD3) && !sFeederStatus.fRunning && !sFeederStatus.fStopping)
            fLastPiece = TRUE;
        else if ((meterModel == CSD2) && ((CMOSTrmUploadDueReqd & TRM_UPLOAD_REQUIRED) == TRM_UPLOAD_REQUIRED))
            fLastPiece = TRUE;
    }
    else if (cmStatus.bMediaType == MEDIA_TAPE)
    {
        if (subPrtMode == CM_DIFF_WEIGH_MODE)
            fLastPiece = TRUE;
        else if (cmStatus.totalPieceCount == 0)
            fLastPiece = TRUE;
        else if (!sSystemStatus.fTapeRunning && !sSystemStatus.fTapeStopping)
            fLastPiece = TRUE;
    }

    if (fLastPiece)
    {
        sSystemStatus.fCmStopPending = TRUE;
        if (cmStatus.bMediaType == MEDIA_TAPE)
        {
            sSystemStatus.stopCause = STOP_NORMAL;
            sSystemStatus.fTapeStopPending = TRUE;  // in case the v33_StopTapeRunRsp isn't handled by state machine, it is checked again later...
            (void)fnStopRun(STOP_TAPE_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
        }
        else if (meterModel >= CSD3)
        {
            sSystemStatus.stopCause = STOP_NORMAL;
            if (sFeederStatus.fRunning && !sFeederStatus.fStopping)
            {
                (void)fnStopRun(STOP_FDR_ONLY, 0, GRACEFUL_STOP);
            }
            else if (!sFeederStatus.fRunning && !sFeederStatus.fStopping)
            {
                // DSD 05-16-2007 Fix Fraca 119446
                //  Timing issue with short envelopes & single piece
                //  mode results in the Feeder reporting that it has
                //  stopped before the PM sends the PC_PRINT_RSP
                //  message.  In old code, when getting the PC_PRINT_RSP
                //  message the CM only stopped the feeder if it was
                //  running.  However, since the feeder was already stopped
                //  the CM never stopped the transport (it was waiting for
                //  the Feeder to say it was stopped).  The fix is to
                //  set the mailrun complete timer if we get the PRINT_RSP
                //  and the feeder has stopped.
                sSystemStatus.stopCause = STOP_NORMAL;
                fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);
            }               
        }
        else
        {   // CSD2 Envelopes
            sSystemStatus.stopCause = STOP_NORMAL;
            //fnStartCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);
            fnStartCMMailRunCompleteTimer(REPORT_STOP,__LINE__);
        }
        sSystemStatus.fEjectMailPiece = TRUE;           
    }
    else if (meterModel == CSD2)
    {
        fnStartCMMailRunCompleteTimer(NORMAL_STOP,__LINE__);
    }
    else if ( (meterModel >= CSD3) && !sFeederStatus.fRunning && !sFeederStatus.fStopping)
    {
        fnStartCMMailRunCompleteTimer(NORMAL_STOP,__LINE__);
    }

    (void)fnMailProgress(EVT_MAIL_PIECE_PROCESSED, WAITING_FOR_MAIL, OK);

    if(cmStatus.cvrState) 
    {
/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//      fnStartCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);
/////////////////////////////////////////////////////////////////

        (void)fnGrantPmMtnc(CAP_SPIT);
        if (meterModel >= CSD3)
        {
            // Stop feeder/tape and the main transport will stop when mail run timer expires
            (void)fnStopRun(STOP_FDR_ONLY, 0, EMERGENCY_STOP);//GRACEFUL_STOP);

            (void)fnStopRun(STOP_TAPE_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
        }
        else
        {
            (void)fnStopRun(STOP_MAIN_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
        }
    }

    /////////////////////////////
    //Complete any pending writes
    trmProcessTxPendingWrite();
    /////////////////////////////

    (void)fnRptPendingEvts();

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x PrtRsp Err x%x Ev %d %d",
            rMsgP->bMsgId, prtRspP->lwErrorCode, *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsPrtRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsTapeRunRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsTapeRunRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    // clear motion sensor data bits
    cmStatus.snsrState.wSensorData &= ~(S1_MASK | S2_MASK | S3_MASK);

    if(rMsgP->IntertaskUnion.lwLongData[POS_0] == OK) 
    {
        sSystemStatus.fTapeRunning = TRUE;
        *outputEvtP = v32_TapeRunRsp;
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsTapeRunRsp, rMsgP, rMsgP->IntertaskUnion.lwLongData[POS_0], 
            rMsgP->IntertaskUnion.lwLongData[POS_1], outputEvtP);
    }

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x TapeRunR Err x%x evt %d st %d",
            rMsgP->bMsgId, rMsgP->IntertaskUnion.lwLongData[POS_1], *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsTapeRunRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsStopTapeRunRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsStopTapeRunRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    ulong lwStatus = rMsgP->IntertaskUnion.lwLongData[POS_0];

    // Only pay attention to sensor data if response is valid and PM is OK
    if((lwStatus != PM_MSG_ERR) && (lwStatus != PM_FATAL_ERR))
    {
        ulong       snsrData = rMsgP->IntertaskUnion.lwLongData[POS_2];
        short       motionSnsr = cmStatus.snsrState.wSensorData & (S1_MASK | S2_MASK | S3_MASK);

        snsrData &= (S1_MASK | S2_MASK | S3_MASK); // Extract motion sensor bits

        if(motionSnsr != snsrData)
        {
            // clear motion sensor data bits
            cmStatus.snsrState.wSensorData &= ~(S1_MASK | S2_MASK | S3_MASK);

            // set sensor data
            cmStatus.snsrState.wSensorData |= snsrData;
        }
    }

    if(lwStatus == PM_SUCCESS)
    {
/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//      *outputEvtP = v33_StopTapeRunRsp;
        *outputEvtP = v0_Invalid;
        sSystemStatus.stopCause = STOP_NORMAL;
        if (!sSystemStatus.fPieceInProgress)
            fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);
////////////////////////////////////////////////////////////////
        sSystemStatus.fTapeRunning = FALSE;
        sSystemStatus.fTapeStopping = FALSE;
//      if (sSystemStatus.fTapeStopPending)
//      {
            //(void)fnStopRun(STOP_MAIN_ONLY, PM_STOP_NOW, GRACEFUL_STOP);          
            //fnStartCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__); 
/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//          fnStartCMTimer(MAILRUN_DELAY_TRANSPORT_STOP,__LINE__);
/////////////////////////////////////////////////////////////////
//      }
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsStopRunRsp, rMsgP, 
                lwStatus, rMsgP->IntertaskUnion.lwLongData[POS_1], outputEvtP);
    }

    return(0);
} // fnPrcsStopTapeRunRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsPrintReady

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsPrintReady(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    *outputEvtP = v0_Invalid;
    //TODO TRM State Print started

    return OK;

} // fnPrcsPrintReady

/**********************************************************************
FUNCTION NAME: 
    fnRptPendingEvts

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnRptPendingEvts(void)
{
    // Send delayed events, like Bob error / ink low/out in the middle of printing
    if(cmStatus.mailRunEvtPending & BOB_EVT_MASK)
    {
        (void)fnMailProgress(EVT_BOB_ERR, IS_STOPPED, cmStatus.bobErrCode);
        // cmStatus.bobPreErrCode = cmStatus.bobErrCode;
        cmStatus.bobErrCode = OK;
    }

    if(cmStatus.mailRunEvtPending & FDR_EVT_MASK)
    {
        (void)fnMailProgress(EVT_FDR_ERR, IS_STOPPED, OK);
    }

    if(cmStatus.mailRunEvtPending & INK_EVT_MASK)
    {
        (void)fnMailProgress(EVT_PM_INK_ERR, IS_STOPPED, OK);
    }

    if(cmStatus.mailRunEvtPending & TANK_EVT_MASK)
    {
        (void)fnMailProgress(EVT_PM_INKTANK_ERR, IS_STOPPED, OK);
    }

    if(cmStatus.mailRunEvtPending & WTANK_EVT_MASK)
    {
        (void)fnMailProgress(EVT_PM_WASTETANK_ERR, IS_STOPPED, OK);
    }

    if(cmStatus.mailRunEvtPending & PAPER_EVT_MASK)
    {
        (void)fnMailProgress(EVT_PM_PAPER_ERR, IS_STOPPED, OK);
    }

    if(cmStatus.mailRunEvtPending & PHEAD_EVT_MASK)
    {
        (void)fnMailProgress(EVT_PM_PHEAD_ERR, IS_STOPPED, OK);
    }

    if(cmStatus.mailRunEvtPending & CVR_EVT_MASK)
    {
        (void)fnMailProgress(EVT_TOP_COVER_STATUS_UPDATE, IS_STOPPED, OK);
    }

    if(cmStatus.mailRunEvtPending & JAMLVR_EVT_MASK)
    {
        (void)fnMailProgress(EVT_JAM_LEVER_STATUS_UPDATE, IS_STOPPED, OK);
    }

    if(cmStatus.mailRunEvtPending & MOTION_SNSR_EVT_MASK)
    {
        (void)fnMailProgress(EVT_SENSOR_STATUS_UPDATE, IS_STOPPED, OK);
    }

    cmStatus.mailRunEvtPending = 0;

    return OK;

} // fnRptPendingEvts

/**********************************************************************
FUNCTION NAME: 
    fnPrtRsp_NmlIdle

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrtRsp_NmlIdle(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    if(cmStatus.cvrState)
    {
        *newStateP = s20_PreCvr;
        (void)fnStopRun(STOP_ALL, PM_STOP_NOW, EMERGENCY_STOP);
        cmStatus.mailRunEvtPending |= CVR_EVT_MASK;
        (void)fnRptPendingEvts();       
    }
        
    return OK;
} // fnPrintRsp_NmlIdle

/**********************************************************************
FUNCTION NAME: 
    fnPrcsDiagRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsDiagRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    static DIAGNOSTIC_RESPONSE      *diagRspP;

    diagRspP = (DIAGNOSTIC_RESPONSE *)rMsgP->IntertaskUnion.PointerData.pData;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x DiagRsp Ex%x x%x %d %d",
            rMsgP->bMsgId, diagRspP->lwErrorCode,
            diagRspP->lwDiagnosticPerformed, diagRspP->lwDiagnosticData, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    if(diagRspP->lwStatus == OK)
    {
        *outputEvtP = v10_PmDiagRsp;
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsDiagRsp, rMsgP, diagRspP->lwStatus, diagRspP->lwErrorCode, outputEvtP);
        *outputEvtP = v76_PmRspDiagErr;
    }

    return OK;

} // fnPrcsDiagRsp

/**********************************************************************
FUNCTION NAME: 
    fnDiagErrRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnDiagErrRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    static      DIAG_RSP    diagRsp;

    diagRsp.lwStatus = rMsgP->IntertaskUnion.lwLongData[POS_0];
    diagRsp.lwErrorCode = rMsgP->IntertaskUnion.lwLongData[POS_1];

    (void)OSSendIntertask(OIT, CM, CO_DIAG_ACTN_RSP, GLOBAL_PTR_DATA, &diagRsp, sizeof(DIAG_RSP));

    return OK;

} // fnDiagErrRsp

/**********************************************************************
FUNCTION NAME: 
    fnDiagRsp_DiagIdle

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnDiagRsp_DiagIdle(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    static      DIAGNOSTIC_RESPONSE diagRsp;

    memcpy(&diagRsp, (DIAGNOSTIC_RESPONSE *)rMsgP->IntertaskUnion.PointerData.pData, sizeof(DIAGNOSTIC_RESPONSE));

    (void)OSSendIntertask(OIT, CM, CO_DIAG_ACTN_RSP, GLOBAL_PTR_DATA, &diagRsp, sizeof(DIAG_RSP));

    return OK;

} // fnDiagRsp_DiagIdle

/**********************************************************************
FUNCTION NAME: 
    fnPrcsRplRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsRplRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{

    if(rMsgP->IntertaskUnion.lwLongData[POS_0] == OK)
    {
        *outputEvtP = v11_PmRplRsp;

        // In normal case, cover is still open since replacement just started
        // If it is closed, it means the cover is closed right after being opened. 
        // So tell OI that replacement is done anyway.
        if(cmStatus.cvrState == CVR_CLOSE) 
            (void)fnMailProgress(EVT_TOP_COVER_STATUS_UPDATE, IS_STOPPED, OK);
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsRplRsp, rMsgP, rMsgP->IntertaskUnion.lwLongData[POS_0], rMsgP->IntertaskUnion.lwLongData[POS_1], outputEvtP);
    }

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> RplRsp Evt %d %d", *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;
} // fnPrcsRplRsp

/**********************************************************************
FUNCTION NAME: 
    fnRplRsp_PmRpl

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnRplRsp_PmRpl(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{

    // Send base event to tablet - ink tank in replacement position
    // Assumes PM cleared PH related warnings and errors, otherwise would not get here

    SendBaseEventToTablet(BASE_EVENT_PH_AT_REPLACE, NULL);

    return OK;

} // fnRplRsp_PmRpl

/**********************************************************************
FUNCTION NAME: 
    fnPrcsMtncReq

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsMtncReq(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    emMeterModel eModelType = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    *outputEvtP = v0_Invalid;

    if ( eModelType >= CSD3 )
    {
        *outputEvtP = v0_Invalid;
        if (fnCMDM400CHandlePmMtncReq(rMsgP->IntertaskUnion.bByteData[POS_0],outputEvtP))
            return OK;
        
    }
    cmStatus.mtncType = rMsgP->IntertaskUnion.bByteData[POS_0];

    if(cmStatus.mtncType && pmMtncCtrl == 0) *outputEvtP = v6_PmReqPrtMtnc;
    else 
    {
        *outputEvtP = v8_PmNoPreRunMtnc;
    }

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x MtncReq evt %d st %d",
            rMsgP->bMsgId, *outputEvtP, cmState.eCurrentState); 
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsMtncReq

/**********************************************************************
FUNCTION NAME: 
    fnMtncReq_Mtnc

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnMtncReq_Mtnc(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{

    if(cmStatus.mtncType == CAP_SPIT && *newStateP == s3_Mtnc)
    {
        //(void)OSSendIntertask(SYS, CM, CS_MAILRUN_CMPLT, LONG_DATA, rspData, sizeof(rspData));
///////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//      fnStartCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);        
///////////////////////////////////////////////////////////////////
    }

    cmStatus.pendingRpl = FALSE;

    if ( cmStatus.mtncType != CAP_SPIT )
    return fnGrantPmMtnc(cmStatus.mtncType);
    else
        return TRUE;
} // fnMtncReq_Mtnc

/********************************************************************************/
/* TITLE: fnCMCheckInkTankMfg                                                   */
/* AUTHOR: Sandra Peterson                                                      */
/* INPUT:  none                                                                 */
/* DESCRIPTION: Check if the trademark string in the installed ink tank is the  */
/*              proper one.                                                     */
/* OUTPUT: none                                                                 */
/********************************************************************************/
void fnCMCheckInkTankMfg(void)
{
    char    *pEMDTrademarkString = (char *)fnFlashGetAsciiStringParm(ASP_INK_TANK_TRADEMARK);
    BOOL    fbDoCheck = TRUE;


    // start by resetting the manufacturer type to non-PB-approved
    ucInkTankMfgType = SOME_OTHER_MFG;
    
    if (!cDummyTrademark[0])
    {
        if (!pEMDTrademarkString || !strlen(pEMDTrademarkString))
        {
            fbDoCheck = FALSE;
        }
    }

    // since there's a trademark string in the EMD (or we're using the dummy string),
    // check if the trademark string in the Canon CMOS area matches the EMD string.
    if (fbDoCheck == TRUE)
    {
        if (!cDummyTrademark[0])
        {
            cActualInkTankTMString[0] = (char)cmStatus.pmNVMDataP->tank_eeprom.bReserve0[0];
            cActualInkTankTMString[1] = (char)cmStatus.pmNVMDataP->tank_eeprom.bReserve0[1];
            cActualInkTankTMString[2] = (char)cmStatus.pmNVMDataP->tank_eeprom.bReserve1[0];
        }
        else
        {
            (void)strncpy(cActualInkTankTMString, cDummyTrademark, 3);
        }

        cActualInkTankTMString[3] = 0;

        if (strcmp(cActualInkTankTMString, pEMDTrademarkString) == 0)
        {
            ucInkTankMfgType = OUR_OEM_MFG;
        }
//        else
//        {
//            ucInkTankMfgType = SOME_OTHER_MFG;
//        }
    }
}

/**********************************************************************
FUNCTION NAME: 
    fnMtncRsp_RptIfIdle

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnMtncRsp_RptIfIdle(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    MAINTENANCE_RESPONSE    *mtncRsp;

    mtncRsp = (MAINTENANCE_RESPONSE *)rMsgP->IntertaskUnion.PointerData.pData;

    // Send delayed events
    if(mtncRsp->bMaintenanceType != BF_PRT_PURGE)
    {
        (void)fnRptPendingEvts();
    }

    return OK;

} // fnMtncRsp_RptIfIdle

/**********************************************************************
FUNCTION NAME: 
    fnPrcsSnsrDataRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsSnsrDataRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{

#ifdef NO_PRINTER //TODO Remove when we get a printer
    rMsgP->IntertaskUnion.lwLongData[POS_0] = 0;
    rMsgP->IntertaskUnion.lwLongData[POS_1] = 0;
#endif
    fnMsgState(SET_STATE, BJCTRL, ss2_Responded);

    if(rMsgP->IntertaskUnion.lwLongData[POS_0] == OK)
    {
        *outputEvtP = v12_PmSnsrDataRsp;
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsSnsrDataRsp, rMsgP, rMsgP->IntertaskUnion.lwLongData[POS_0], rMsgP->IntertaskUnion.lwLongData[POS_1], outputEvtP);
    }

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x SnsrDataRsp x%x x%x Evt %d %d",
            rMsgP->bMsgId, rMsgP->IntertaskUnion.lwLongData[POS_1],
            rMsgP->IntertaskUnion.lwLongData[POS_2], *outputEvtP, cmState.eCurrentState); 
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsSnsrDataRsp

/**********************************************************************
FUNCTION NAME: 
    fnSnsrDataRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSnsrDataRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    static DATA_RSP rspData = {0, 0, SNSR_DATA, NULL}; 

    // *newStateP = fnRtnToIdle(__LINE__);

    if (sSystemStatus.fSendOitSttsRsp)
    {
        if(fnMsgState(MATCH, FDR, ss2_Responded) && fnMsgState(MATCH, BJCTRL, ss2_Responded) )
        {
            fnSendOitGetSttsRsp();

            if(cmState.eCurrentState == s36_PndgWrng) *newStateP = fnRtnToIdle(__LINE__);
        }
    }
    else
    {
        // Forward sensor data to OIT
        rspData.pRspData = (uchar *)&cmStatus.snsrState;

        (void)OSSendIntertask(OIT, CM, CO_GET_DATA_RSP, GLOBAL_PTR_DATA, &rspData, sizeof(DATA_RSP));
    }

    return OK;
} // fnSnsrDataRsp_Idle

/**********************************************************************
FUNCTION NAME: 
    fnPrcsClrErrRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsClrErrRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    emMtncType  mtncType = rMsgP->IntertaskUnion.lwLongData[POS_2];
    ulong       rsp[2];

    if(rMsgP->IntertaskUnion.lwLongData[POS_0] == 0)
    {
        if(mtncType == BF_PRT_PURGE || mtncType == PWR_PURGE || mtncType == JAM_PURGE)
        {
            (void)fnGrantPmMtnc(mtncType);
            *outputEvtP = v25_PmClrJamErrRsp;
        }

        else *outputEvtP = v26_PmClrPprErrRsp;

        // clear motion sensor data bits
        cmStatus.snsrState.wSensorData &= ~(S1_MASK | S2_MASK | S3_MASK);
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsClrErrRsp, rMsgP, rMsgP->IntertaskUnion.lwLongData[POS_0], rMsgP->IntertaskUnion.lwLongData[POS_1], outputEvtP);
    }

    if(rMsgP->IntertaskUnion.lwLongData[POS_0] == PM_MSG_ERR && cmState.eCurrentState != s39_PprErr)
    {
        // there is a message error, don't get send this back to the OIT
        rsp[0] = 0;
        rsp[1] = 0;
        (void)OSSendIntertask(OIT, CM, CO_CLR_ERR_RSP, LONG_DATA, &rsp, sizeof(rsp));     
    }
    else if(cmState.eCurrentState != s39_PprErr)
    {
        (void)OSSendIntertask(OIT, CM, CO_CLR_ERR_RSP, LONG_DATA, rMsgP->IntertaskUnion.lwLongData, LEN_2 * sizeof(ulong));
    }

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x ClrErrRsp stts x%x err x%x MtncType %d outputEvtP %d %d",
            rMsgP->bMsgId, 
            rMsgP->IntertaskUnion.lwLongData[POS_0], 
            rMsgP->IntertaskUnion.lwLongData[POS_1], 
            rMsgP->IntertaskUnion.lwLongData[POS_2], *outputEvtP, cmState.eCurrentState); 
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return (OK);

} // fnPrcsClrErrRsp

/**********************************************************************
FUNCTION NAME: 
    fnClrPmErrRsp_Nml

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnClrPmErrRsp_Nml(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    return OK;

} // fnClrPmErrRsp_Nml

/**********************************************************************
FUNCTION NAME: 
    fnClrPmErrRsp_Sl

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnClrPmErrRsp_Sl(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    return OK;

} // fnClrPmErrRsp_Sl

/**********************************************************************
FUNCTION NAME: 
    fnClrPmHdInkErrRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnClrPmHdInkErrRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    *newStateP = fnRtnToIdle(__LINE__);

    return OK;

} // fnClrPmHdInkErrRsp

/**********************************************************************
FUNCTION NAME: 
    fnClrPmJamErrRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnClrPmJamErrRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    return OK;

} // fnClrPmJamErrRsp

/**********************************************************************
FUNCTION NAME: 
    fnClrPmPprErrRsp_Nml

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnClrPmPprErrRsp_Nml(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    return OK;

} // fnClrPmPprErrRsp_Nml

/**********************************************************************
FUNCTION NAME: 
    fnClrPmPprErrRsp_Sl

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnClrPmPprErrRsp_Sl(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    return OK;

} // fnClrPmPprErrRsp_Sl

/**********************************************************************
FUNCTION NAME: 
    fnNoPaper

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnNoPaper(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    ulong       errClrCode[LEN_2];

    errClrCode[POS_0] = PM_PAPER_ERR;
    errClrCode[POS_1] = E_NO_PAPER;

    // send notice to SYS
    (void)OSSendIntertask(SYS, CM, CS_JOB_CANCEL, LONG_DATA, errClrCode, sizeof(errClrCode));

    // send clear error request to PM
    // Uncomment this line for CFI version before 0127.0002
    // (void)OSSendIntertask(BJCTRL, CM, CP_CLR_PRT_ERR, LONG_DATA, errClrCode, sizeof(errClrCode));

    fnStopCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);

    if(subPrtMode == CM_REPORT_MODE) 
    {
        //(void)OSSendIntertask(SYS, CM, CS_MAILRUN_CMPLT, LONG_DATA, errClrCode, sizeof(errClrCode));
/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//      fnStartCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);
/////////////////////////////////////////////////////////////////
    }
    else
    {
        // Start timer to go to Ready screen
/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//      fnStartCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);
/////////////////////////////////////////////////////////////////
    }

    *newStateP = fnRtnToIdle(__LINE__);

    #ifdef DBG_CM
    sprintf(DbgBuf, "Clear No Paper Error");
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnNoPaper

/**********************************************************************
FUNCTION NAME: 
    fnClrNoPaperRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnClrNoPaperRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    *newStateP = fnRtnToIdle(__LINE__);

    return OK;

} // fnClrNoPaperRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsTopCvrStts

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsTopCvrStts(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    ulong       cvrStatus = rMsgP->IntertaskUnion.bByteData[POS_0];

    cmStatus.cvrState = cvrStatus; 
    cmStatus.snsrState.wSensorData &= ~TOPCVR_MASK;
    cmStatus.snsrState.wSensorData |= cvrStatus << POS_4;

    if(cvrStatus == CVR_OPEN) 
        *outputEvtP = v13_PmCvrOpen;
    else 
        *outputEvtP = v14_PmCvrClose;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x CvrStts %d Evt %d %d",
            rMsgP->bMsgId, cmStatus.cvrState, *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsTopCvrStts

/**********************************************************************
FUNCTION NAME: 
    fnCvrOpen_Save

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnCvrOpen_Save(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    // Save to report OIT later
    cmStatus.mailRunEvtPending |= CVR_EVT_MASK;

    return OK;

} // fnCvrOpen_Save

/**********************************************************************
FUNCTION NAME: 
    fnCvrOpen_CvrOpen

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnCvrOpen_CvrOpen(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    // Report to OIT about the sensor change
    if (cmStatus.cvrState != CVR_OPEN && cmStatus.jamLvrState != JAM_LVR_OPEN)
        (void)fnGrantPmMtnc(CAP_SPIT);
    (void)fnMailProgress(EVT_TOP_COVER_STATUS_UPDATE, IS_STOPPED, OK);

    return OK;

} // fnCvrOpen_CvrOpen

/**********************************************************************
FUNCTION NAME: 
    fnCvrOpen_PreCvr

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnCvrOpen_PreCvr(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    // Report to OIT about the sensor change
    if (cmStatus.cvrState != CVR_OPEN && cmStatus.jamLvrState != JAM_LVR_OPEN)
        (void)fnGrantPmMtnc(CAP_SPIT);
    (void)fnMailProgress(EVT_TOP_COVER_STATUS_UPDATE, IS_STOPPED, OK);
    // DSD 05-14-2007 Fix Fraca 119889.  A stop isn't required if not running.  
    // This unnecessary stop was causing issues whith the ink tank replacement
    // from the menu screens.
    if (sSystemStatus.fProcessingPieces)
        (void)fnStopRun(STOP_ALL, PM_STOP_NOW, EMERGENCY_STOP);

    return OK;

} // fnCvrOpen_PreCvr

/**********************************************************************
FUNCTION NAME: 
    fnCvrClose_Save

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnCvrClose_Save(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    if( (cmStatus.mailRunEvtPending & CVR_EVT_MASK) && cmStatus.cvrState == CVR_OPEN )
    {
        // Clear cover event if cover is closed before cover open event reported
        cmStatus.mailRunEvtPending &= ~CVR_EVT_MASK;
    }
    else 
    {
        // Save to report OIT later
        cmStatus.mailRunEvtPending |= CVR_EVT_MASK;
    }

    return OK;

} // fnCvrOpen_Save

/**********************************************************************
FUNCTION NAME: 
    fnCvrCls_Idle

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnCvrCls_Idle(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    if(initRspFlg == INIT_PENDING)
    {
        // check the manufacturer type
        fnCMCheckInkTankMfg();
        
        // Send init response to SYS if no maintenance requested, or mtnc is completed
        (void)OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE, OS_OR);
        (void)OSSendIntertask(SYS, CM, CS_INIT_RSP, NO_DATA, NULL, 0);

        initRspFlg = INIT_COMPLETE;
    }

    // Report to OIT about the sensor change
    (void)fnMailProgress(EVT_TOP_COVER_STATUS_UPDATE, IS_STOPPED, OK);

    *newStateP = fnRtnToIdle(__LINE__);

    return OK;

} // fnCvrCls_Idle

/**********************************************************************
FUNCTION NAME: 
    fnCvrCls_PmRplFnsh

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnCvrCls_PmRplFnsh(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    // Report to OIT about the sensor change
    (void)fnMailProgress(EVT_TOP_COVER_STATUS_UPDATE, IS_STOPPED, OK);

    return OK;

} // fnCvrCls_PmRplFnsh

/**********************************************************************
FUNCTION NAME: 
    fnCvrCls_JamOpen

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnCvrCls_JamOpen(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg
    emCmState           *newStateP)     // (I/O) New State, not committed yet
{
    // Report to OIT about the sensor change
    (void)fnMailProgress(EVT_TOP_COVER_STATUS_UPDATE, IS_STOPPED, OK);

    return OK;
} // fnCvrCls_JamOpen

/**********************************************************************
FUNCTION NAME: 
    fnPrcsJamLvrStts

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsJamLvrStts(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    ulong       lvrStatus = rMsgP->IntertaskUnion.bByteData[POS_0];

    *outputEvtP = v0_Invalid;
    if(lvrStatus != cmStatus.jamLvrState)
    {
        cmStatus.jamLvrState = lvrStatus;
        cmStatus.snsrState.wSensorData &= ~JAMLVR_MASK;
        cmStatus.snsrState.wSensorData |= lvrStatus << POS_3;

        if(lvrStatus == JAM_LVR_OPEN) 
            *outputEvtP = v15_PmJamOpen;
        else 
            *outputEvtP = v16_PmJamClose;

        // Report to OIT about the sensor change
        (void)fnMailProgress(EVT_JAM_LEVER_STATUS_UPDATE, IS_STOPPED, OK);
    }

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x JamLvrStts Evt %d %d",
            rMsgP->bMsgId, *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsJamLvrStts

/**********************************************************************
FUNCTION NAME: 
    fnJamOpen_Cap

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnJamOpen_Cap(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    (void)fnGrantPmMtnc(CAP_SPIT);
    return OK;

} // fnJamOpen_Cap

/**********************************************************************
FUNCTION NAME: 
    fnJamOpen

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnJamOpen(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    return OK;

} // fnJamOpen

/**********************************************************************
FUNCTION NAME: 
    fnJamCls

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnJamCls(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    *newStateP = fnRtnToIdle(__LINE__);
    return OK;
} // fnJamCls

/**********************************************************************
FUNCTION NAME: 
    fnJamCls_crvOpen

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnJamCls_crvOpen(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{

    if(cmState.ePreviousState == s20_PreCvr)
    {
        *newStateP = s20_PreCvr;
    }

    return OK;
} // fnJamCls_crvOpen

/**********************************************************************
FUNCTION NAME: 
    fnBothCvrOpen

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnBothCvrOpen(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg 
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    (void)fnGrantPmMtnc(CAP_SPIT);
    (void)fnMailProgress(EVT_TOP_COVER_STATUS_UPDATE, IS_STOPPED, OK);
    return OK;

} // fnBothCvrOpen

/**********************************************************************
FUNCTION NAME: 
    fnPrcsShutdownRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsShutdownRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{ 
    SHUTDOWN_RESPONSE   *shutDownRsp = (SHUTDOWN_RESPONSE *)rMsgP->IntertaskUnion.PointerData.pData; 

    (void)fnMsgState(SET_STATE, BJCTRL, ss2_Responded);
    
    if(shutDownRsp->lwStatus == OK)
    {
        *outputEvtP = v18_PmStdwRsp;
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsShutdownRsp, rMsgP, shutDownRsp->lwStatus, shutDownRsp->lwErrorCode, outputEvtP);
    }

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x ShutdownRsp Evt %d %d",
            rMsgP->bMsgId, *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsShutdownRsp

/**********************************************************************
FUNCTION NAME: 
    fnShutDownRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnShutDownRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{

    if(fnMsgState(MATCH, FDR, ss2_Responded) && fnMsgState(MATCH, BJCTRL, ss2_Responded) )
    {
        (void)fnSetModeRsp(inputEvt, rMsgP, newStateP);
        (void)fnRptPendingEvts();
    }

    return OK;

} // fnShutDownRsp

/**********************************************************************
FUNCTION NAME: 
    fnShutDownRspAnd27VOff

DESCRIPTION:    Does the same thing as fnShutDownRsp except that it also
                turns off 27 volts to the feeder board (to reduce power consumption.)
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnShutDownRspAnd27VOff(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{

    if(fnMsgState(MATCH, FDR, ss2_Responded) && fnMsgState(MATCH, BJCTRL, ss2_Responded) )
    {
        (void)fnSetModeRsp(inputEvt, rMsgP, newStateP);
        (void)fnRptPendingEvts();

        // turn off 27V to the feeder board
        fnSystemLogEntry(SYSLOG_TEXT, "27 Volts OFF", 12);
//TODO - move into HAL and delete here
//        FDR_VM_CONTROL_PORT |= FDR_VM_CONTROL_BIT;
        HALFeederSet27Volts(FALSE);

    }

    return OK;

} // fnShutDownRspAnd27VOff

/**********************************************************************
FUNCTION NAME: 
    fnSysInitReq27VOn

DESCRIPTION:    Same as fnSysInitReq, except that it also turns on 27 volts.
                This function would be used when waking up from sleep mode.
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSysInitReq27VOn(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
                                        //       May change if error occurs
{
    // turn 27v back on to the feeder board
    fnSystemLogEntry(SYSLOG_TEXT, "27 Volts ON ", 12);
//TODO - move into HAL and delete here
//    FDR_VM_CONTROL_PORT &= ~FDR_VM_CONTROL_BIT;     // For DE2 the logic was reversed for some unknown reason.
    HALFeederSet27Volts(TRUE);
    fnPmDftInit(cmStatus.sysInitMode);
    
    return OK;

} // fnSysInitReq27VOn

/**********************************************************************
FUNCTION NAME: 
    fnPrcsPrtHdRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsPrtHdRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
#ifdef NO_PRINTER //TODO JAH Remove when printer is working
    rMsgP->IntertaskUnion.lwLongData[POS_0] = 0;
    rMsgP->IntertaskUnion.lwLongData[POS_1] = 0;
#endif

    #ifdef DBG_CM
    /*-------
    sprintf(DbgBuf, "Msg ID x%x PrtHeadDataRsp %d x%x %d Print Header",
            rMsgP->bMsgId,
            rMsgP->IntertaskUnion.lwLongData[POS_0],
            rMsgP->IntertaskUnion.lwLongData[POS_1], cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);

    -------- */

    sprintf(DbgBuf, "Print Head: ID %.2x%.2x SN %.2x%.2x%.2x%.2x Mfg %.2x/%.2x/%.2x Update %x",
            cmStatus.prtHeadData.bId[POS_0],
            cmStatus.prtHeadData.bId[POS_1],
            cmStatus.prtHeadData.bSerialNumber[POS_0],
            cmStatus.prtHeadData.bSerialNumber[POS_1],
            cmStatus.prtHeadData.bSerialNumber[POS_2],
            cmStatus.prtHeadData.bSerialNumber[POS_3],
            cmStatus.prtHeadData.bManufactureDate[POS_0],
            cmStatus.prtHeadData.bManufactureDate[POS_1],
            cmStatus.prtHeadData.bManufactureDate[POS_2],
            cmStatus.prtHeadData.bUpdate[POS_0]);
    fnDumpStringToSystemLog(DbgBuf);


    sprintf(DbgBuf, "\tInstall %.2x/%.2x/%.2x Dots %.2x%.2x%.2x%.2x%.2x%.2x InstallCount %x",
            cmStatus.prtHeadData.bInstallDate[POS_0],
            cmStatus.prtHeadData.bInstallDate[POS_1],
            cmStatus.prtHeadData.bInstallDate[POS_2],
            cmStatus.prtHeadData.bDotCount[POS_0],
            cmStatus.prtHeadData.bDotCount[POS_1],
            cmStatus.prtHeadData.bDotCount[POS_2],
            cmStatus.prtHeadData.bDotCount[POS_3],
            cmStatus.prtHeadData.bDotCount[4],
            cmStatus.prtHeadData.bDotCount[5],
            cmStatus.prtHeadData.bInstallCount[POS_0]);
    fnDumpStringToSystemLog(DbgBuf);


    #endif

    if(rMsgP->IntertaskUnion.lwLongData[POS_0] == OK)
    {
        *outputEvtP = v19_PmPrtHdDataRsp;
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsPrtHdRsp, rMsgP, rMsgP->IntertaskUnion.lwLongData[POS_0], rMsgP->IntertaskUnion.lwLongData[POS_1], outputEvtP);
    }


    return OK;

} // fnPrcsPrtHdRsp

/**********************************************************************
FUNCTION NAME: 
    fnHdDataRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnHdDataRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    static DATA_RSP rspData = {0, 0, PRT_HEAD_DATA, NULL}; 
    static uchar count = 0;

    // *newStateP = fnRtnToIdle(__LINE__);

    // Forward head data to OIT
    rspData.pRspData = (uchar *)&cmStatus.prtHeadData;

    if(count != 0)
    {
        (void)OSSendIntertask(OIT, CM, CO_GET_DATA_RSP, GLOBAL_PTR_DATA, &rspData, sizeof(DATA_RSP));
        count++;
    }

    return OK;

} // fnHdDataRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsInkTankRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsInkTankRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
#ifdef NO_PRINTER //TODO JAH Remove when printer is working
    rMsgP->IntertaskUnion.lwLongData[POS_0] = 0;
    rMsgP->IntertaskUnion.lwLongData[POS_1] = 0;
#endif

    #ifdef DBG_CM
    /*------
    sprintf(DbgBuf, "Msg ID x%x InkTankRsp %d x%x %d InkTank",
            rMsgP->bMsgId,
            rMsgP->IntertaskUnion.lwLongData[POS_0],
            rMsgP->IntertaskUnion.lwLongData[POS_1], cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);

    ------- */

    sprintf(DbgBuf, "Tank: SN %.2x%.2x%.2x%.2x PRN SN %c%c%c%c%c%c%c%c%c",
            cmStatus.inkTankData.bSerialNumber[POS_0],
            cmStatus.inkTankData.bSerialNumber[POS_1],
            cmStatus.inkTankData.bSerialNumber[POS_2],
            cmStatus.inkTankData.bSerialNumber[POS_3],
            cmStatus.inkTankData.bPrinterSerialNumber[POS_0],
            cmStatus.inkTankData.bPrinterSerialNumber[POS_1],
            cmStatus.inkTankData.bPrinterSerialNumber[POS_2],
            cmStatus.inkTankData.bPrinterSerialNumber[POS_3],
            cmStatus.inkTankData.bPrinterSerialNumber[POS_4],
            cmStatus.inkTankData.bPrinterSerialNumber[POS_5],
            cmStatus.inkTankData.bPrinterSerialNumber[POS_6],
            cmStatus.inkTankData.bPrinterSerialNumber[POS_7],
            cmStatus.inkTankData.bPrinterSerialNumber[POS_8] );
    fnDumpStringToSystemLog(DbgBuf);


    sprintf(DbgBuf, "\tInkDots %.2x%.2x%.2x%.2x%.2x%.2x",
            cmStatus.inkTankData.bDotCount[POS_0],
            cmStatus.inkTankData.bDotCount[POS_1],
            cmStatus.inkTankData.bDotCount[POS_2],
            cmStatus.inkTankData.bDotCount[POS_3],
            cmStatus.inkTankData.bDotCount[POS_4],
            cmStatus.inkTankData.bDotCount[POS_5] );
    fnDumpStringToSystemLog(DbgBuf);


    #endif

    if(rMsgP->IntertaskUnion.lwLongData[POS_0] == OK)
    {
        *outputEvtP = v20_PmInkTkRsp;
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsInkTankRsp, rMsgP, rMsgP->IntertaskUnion.lwLongData[POS_0], rMsgP->IntertaskUnion.lwLongData[POS_1], outputEvtP);
    }

    return OK;

} // fnPrcsInkTankRsp

/**********************************************************************
FUNCTION NAME: 
    fnInkTankDataRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnInkTankDataRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    static DATA_RSP rspData = {0, 0, INKTANK_DATA, NULL}; 
    static uchar count = 0;

    if (!fnGetNVRAMOOBInkInstalled())
    {
        fnSetNVRAMOOBInkInstalled(TRUE);
    }

    // *newStateP = fnRtnToIdle(__LINE__);

    // Forward ink tank data to OIT
    rspData.pRspData = (uchar *)&cmStatus.inkTankData;

    if(count != 0)
    {
        (void)OSSendIntertask(OIT, CM, CO_GET_DATA_RSP, GLOBAL_PTR_DATA, &rspData, sizeof(DATA_RSP));
        count++;
    }

    return OK;

} // fnInkTankDataRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsSwVerRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsSwVerRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
#ifdef NO_PRINTER //TODO JAH Remove when printer is working
    rMsgP->IntertaskUnion.lwLongData[POS_0] = 0;
    rMsgP->IntertaskUnion.lwLongData[POS_1] = 0;
#endif

    #ifdef DBG_CM
    /*  sprintf(DbgBuf, "Msg ID x%x SwVerRsp x%x --SW Ver %.8x---- %d",
            rMsgP->bMsgId,
            rMsgP->IntertaskUnion.lwLongData[POS_1],
            rMsgP->IntertaskUnion.lwLongData[POS_2], cmState.eCurrentState); */
    sprintf(DbgBuf, "PM Version %.8x",rMsgP->IntertaskUnion.lwLongData[POS_2]);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    if(rMsgP->IntertaskUnion.lwLongData[POS_0] == OK)
    {
        *outputEvtP = v21_PmSwVerRsp;
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsSwVerRsp, rMsgP, rMsgP->IntertaskUnion.lwLongData[POS_0], rMsgP->IntertaskUnion.lwLongData[POS_1], outputEvtP);
    }

    return OK;

} // fnPrcsSwVerRsp

/**********************************************************************
FUNCTION NAME: 
    fnSwVerRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSwVerRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    static DATA_RSP rspData = {0, 0, SW_VER, NULL}; 
    static uchar count = 0;

    cmStatus.pmSwVer = rMsgP->IntertaskUnion.lwLongData[POS_2];

    // *newStateP = fnRtnToIdle(__LINE__);

    // Forward SW version data to OIT
    rspData.pRspData = (uchar *)&cmStatus.pmSwVer;

    if(count != 0)
    {
        (void)OSSendIntertask(OIT, CM, CO_GET_DATA_RSP, GLOBAL_PTR_DATA, &rspData, sizeof(DATA_RSP));
        count++;
    }

    return OK;

} // fnSwVerRsp

/**********************************************************************
FUNCTION NAME: 
    fnPrcsUnsolicit

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsUnsolicit(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{

    // Get error event
    (void)fnPmErrHandler(fnPrcsUnsolicit, rMsgP, rMsgP->IntertaskUnion.lwLongData[POS_0], rMsgP->IntertaskUnion.lwLongData[POS_1], outputEvtP);

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x Unsolicit Evt %d state %d",
            rMsgP->bMsgId, *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsUnsolicit

/**********************************************************************
FUNCTION NAME: 
    fnPrcsSetModeRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsSetModeRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
#ifdef NO_PRINTER //TODO Remove when we get a printer
    rMsgP->IntertaskUnion.lwLongData[POS_0] = 0;
    rMsgP->IntertaskUnion.lwLongData[POS_1] = 0;
#endif
    int currState = (int)fnCmGetCrntState();

    // If maintenance is needed, grant it. Mode request response will be sent when mtnc is done
    sSystemStatus.fPmModeChangeNeeded = FALSE;
    if(rMsgP->IntertaskUnion.lwLongData[POS_0] == OK)
    {
        fnMsgState(SET_STATE, BJCTRL, ss2_Responded); 

        if(rMsgP->IntertaskUnion.lwLongData[POS_2])
        {
            cmStatus.mtncType = (UINT8)rMsgP->IntertaskUnion.lwLongData[POS_2];
            *outputEvtP = v6_PmReqPrtMtnc;
        }
        else
        {
            if ((currState & CM_STOPPING_STATE_MASK) == CM_STOPPING_STATE_PREFIX)
            {
                if(fnMsgState(MATCH, BJCTRL, ss2_Responded) && fnMsgState(MATCH, FDR, ss2_Responded))
                {
                    if (currState == s260_StopDoneResetMode)
                        *outputEvtP = v134_ModeResetComplete;
                    else
                        *outputEvtP = v131_PmDisabled;
                }
            }
            else
                *outputEvtP = v22_PmSetModeRsp;
                
        }
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsSetModeRsp, rMsgP, rMsgP->IntertaskUnion.lwLongData[POS_0], rMsgP->IntertaskUnion.lwLongData[POS_1], outputEvtP);
    }

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x SetModRsp Evt %d state %d",
            rMsgP->bMsgId, *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsSetModeRsp

/**********************************************************************
FUNCTION NAME: 
    fnSetModeRsp_Dup

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSetModeRsp_Dup(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    fnSendModeRsp();

    return OK;
}

/**********************************************************************
FUNCTION NAME: 
    fnSendModeRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static void fnSendModeRsp(void)
{
    ulong       rspData[LEN_3];

    rspData[POS_0] = 0;
    rspData[POS_1] = subPrtMode;    //cmStatus.opMode;
    rspData[POS_1] = sSystemStatus.bCurrMode;
    rspData[POS_2] = 0;

    // respond to SYS if feeder responded also
    if(fnMsgState(MATCH, BJCTRL, ss2_Responded) && fnMsgState(MATCH, FDR, ss2_Responded))
    {
        sSystemStatus.fSysModeChangePending = FALSE;
        (void)OSSendIntertask(SYS, CM, CS_MODE_RSP, LONG_DATA, rspData, sizeof(rspData));
    }
}

/**********************************************************************
FUNCTION NAME: 
    fnSetModeRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnSetModeRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{   
    int         rtnVal = OK;
   
    if(fnMsgState(MATCH, BJCTRL, ss2_Responded) && fnMsgState(MATCH, FDR, ss2_Responded))
    { 
        fnSendModeRsp();
    }
    
    return rtnVal;

} // fnSetModeRsp

/**********************************************************************
FUNCTION NAME: 
    fnSetModeRsp_CvrStatus

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
/* Report top cover status to OI again after mode change caused by opened cover.
 * The reason is that OI ignores any reported events when it's in the middle of mode change
 */
int fnSetModeRsp_CvrStatus(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{

    // respond to SYS 
    // if(fnMsgState(MATCH, BJCTRL, ss2_responded)) diable mode won't pass to feeder
    fnSendModeRsp();

    // Report to OIT about top cover status
    (void)fnMailProgress(EVT_TOP_COVER_STATUS_UPDATE, IS_STOPPED, OK);

    return OK;

} // fnSetModeRsp_CvrStatus ()

/**********************************************************************
FUNCTION NAME: 
    fnMegaSetModeRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnMegaSetModeRsp(
    uchar       inputMode)          // Mode request 
{

    ulong       rspData[LEN_3];

    rspData[POS_0] = 0;
    rspData[POS_1] = inputMode;
    rspData[POS_2] = 0;

    // respond to SYS
    sSystemStatus.fSysModeChangePending = FALSE;
    (void)OSSendIntertask(SYS, CM, CS_MODE_RSP, LONG_DATA, rspData, sizeof(rspData));

    return OK;

} // fnMegaSetModeRsp

/**********************************************************************
FUNCTION NAME: 
    fnPmSetMode

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPmSetMode(
    emOpMode            mode,       // (I)   operation mode
    ulong               interval,   // (I)   interval (Simulation only)
    ulong               times)      // (I)   times (Simulation only)
{
    static SET_MODE_DATA modeReq; 
    emSpeed  bIPS = fnGetTransportSpeed();

    if (bIPS == IPS_0_0)
    {// error condition
        return ERR;
    }

    modeReq.bMode = mode;
    modeReq.bTransportSpeed = (uchar) bIPS;
    modeReq.lwInterval = interval;
    modeReq.lwTimes = times;

    sSystemStatus.bCurrMode = (UINT8)mode;

    (void)OSSendIntertask(BJCTRL, CM, CP_SET_MODE, GLOBAL_PTR_DATA, &modeReq, sizeof(SET_MODE_DATA));
    fnMsgState(SET_STATE, BJCTRL, ss1_Sent);

    return OK;

} // fnPmSetMode

/**********************************************************************
FUNCTION NAME: 
    fnPrcsEjtRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsEjtRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
//    ulong       rspData[LEN_2];

//    rspData[POS_0] = 0;
//    rspData[POS_1] = 0;

    //set sensor data
    cmStatus.snsrState.wSensorData &= ~(S1_MASK | S2_MASK | S3_MASK);

    sSystemStatus.fPieceInProgress = FALSE;

    if(rMsgP->IntertaskUnion.lwLongData[POS_0] == OK)
    {
        //*outputEvtP = v23_PmEjtRsp;
        *outputEvtP = v0_Invalid;
        fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);
    }
    else
    {
        (void)fnPmErrHandler(fnPrcsEjtRsp, rMsgP, rMsgP->IntertaskUnion.lwLongData[POS_0], rMsgP->IntertaskUnion.lwLongData[POS_1], outputEvtP);
    }

    (void)fnMailProgress(EVT_MAIL_PIECE_PROCESSED, WAITING_FOR_MAIL, OK);     

    if(    subPrtMode == CM_INDICIA_MODE || subPrtMode == CM_SIMULATION_MODE 
        || subPrtMode == CM_PERMIT_MODE || subPrtMode == CM_TEST_PAT_MODE )
    {
/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//      fnStartCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);
/////////////////////////////////////////////////////////////////
    }
    else
    {
        // (void)OSSendIntertask(SYS, CM, CS_MAILRUN_CMPLT, LONG_DATA, rspData, sizeof(rspData));
/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//      fnStartCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);
/////////////////////////////////////////////////////////////////
    }

    if(cmStatus.cvrState) 
        (void)fnGrantPmMtnc(CAP_SPIT);

    // Send delayed events
    (void)fnRptPendingEvts();

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x EjtRsp Evt %d state %d",
            rMsgP->bMsgId, *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsEjtRsp

/**********************************************************************
FUNCTION NAME: 
    fnEjtRsp_Idle

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnEjtRsp_Idle(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    (void)OSSendIntertask(OIT, CM, CO_EJECT_RSP, LONG_DATA, rMsgP->IntertaskUnion.lwLongData, LEN_2 * sizeof(ulong));

    if(cmStatus.cvrState)
    {
        *newStateP = s20_PreCvr;
    }

    return OK;

} // fnEjtRsp_Idle

/**********************************************************************
FUNCTION NAME: 
    fnPrcsMotionSnsrStts

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsMotionSnsrStts(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    *outputEvtP = v3_PmMotionSnsrStatus;

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Msg ID x%x MotionStts Evt %d state %d",
            rMsgP->bMsgId, *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsMotionSnsrStts

/**********************************************************************
FUNCTION NAME: 
    fnMotionSnsrStts

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnMotionSnsrStts(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    ulong lwStatus = rMsgP->IntertaskUnion.lwLongData[POS_0];

    // Only pay attention to sensor data if response is valid and PM is OK
    if((lwStatus != PM_MSG_ERR) && (lwStatus != PM_FATAL_ERR))
    {
        UINT16      snsrData = (UINT16)rMsgP->IntertaskUnion.lwLongData[POS_1];
        UINT16       motionSnsr = cmStatus.snsrState.wSensorData & (S1_MASK | S2_MASK | S3_MASK);

        snsrData &= (S1_MASK | S2_MASK | S3_MASK);

        if(motionSnsr != snsrData)
        {
            // clear motion sensor data bits
            cmStatus.snsrState.wSensorData &= ~(S1_MASK | S2_MASK | S3_MASK);

            // set sensor data
            cmStatus.snsrState.wSensorData |= snsrData;

            // Report to OIT about the sensor change
            (void)fnMailProgress(EVT_SENSOR_STATUS_UPDATE, IS_STOPPED, OK);

            #ifdef DBG_CM
            sprintf(DbgBuf, "Msg ID x%x MtnSsr status chg x%x sensor data x%x evt 3 st %d",
                rMsgP->bMsgId,
                rMsgP->IntertaskUnion.lwLongData[POS_0],
                rMsgP->IntertaskUnion.lwLongData[POS_1], cmState.eCurrentState);
            fnDumpStringToSystemLog(DbgBuf);
            #endif
        }
        else
        {
            fnDumpStringToSystemLog(DbgBuf);
        }
    }

    return OK;

} // fnMotionSnsrStts

/**********************************************************************
FUNCTION NAME: 
    fnCmClrIntErr

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnCmClrIntErr(
    ulong               errType,        // (I) Error Type
    ulong               errCode  )      // (I) Error Code
{

    switch(errType)
    {
        case PM_WARNING:
             cmStatus.pmErrState.lwStatus &= ~IPM_WARNING;
                // lint thinks the result on the right side below is signed, so it gives a warning
             cmStatus.pmErrState.lwStatus &= ~(1 << (errCode + WRN_SHIFT));     //lint !e502
             break;
             
        case PM_MSG_ERR:
             cmStatus.pmErrState.lwStatus &= ~IPM_MSG_ERR;
                // lint thinks the result on the right side below is signed, so it gives a warning
             cmStatus.pmErrState.lwStatus &= ~(1 << (errCode - MSG_ERR_SHIFT));     //lint !e502
             break;
             
        case PM_TAPE_ERR:
             cmStatus.pmErrState.lwStatus &= ~IPM_TAPE_ERR;
                // lint thinks the result on the right side below is signed, so it gives a warning
             cmStatus.pmErrState.lwTapeErr &= ~(1 << (errCode - TAPE_ERR_SHIFT));       //lint !e502

             if(cmStatus.pmErrState.lwTapeErr == 0)
                 cmStatus.pmErrState.lwStatus &= ~IPM_TAPE_ERR;
             break;

        case PM_PAPER_ERR:
             if(errCode == E_JAM)
             {
                // lint thinks the result on the right side below is signed, so it gives a warning
                cmStatus.pmErrState.lwPaperErr &= ~(1 << (errCode - JAM_ERR_SHIFT));    //lint !e502
             }
             else if(errCode == E_JAM_LEVER_OPEN)
             {
                // lint thinks the result on the right side below is signed, so it gives a warning
                 cmStatus.pmErrState.lwPaperErr &= ~(1 << (errCode - JAM_ERR_SHIFT));   //lint !e502
             }
             else
             {
                // lint thinks the result on the right side below is signed, so it gives a warning
                 cmStatus.pmErrState.lwPaperErr &= ~(1 << (errCode - PAPER_ERR_SHIFT)); //lint !e502
             }

             if(cmStatus.pmErrState.lwPaperErr == 0)
                 cmStatus.pmErrState.lwStatus &= ~IPM_PAPER_ERR;
             break;

        case PM_PHEAD_ERR:
                // lint thinks the result on the right side below is signed, so it gives a warning
             cmStatus.pmErrState.lwPheadErr &= ~(1 << (errCode - PHEAD_ERR_SHIFT));     //lint !e502

             if(cmStatus.pmErrState.lwPheadErr == 0)
                 cmStatus.pmErrState.lwStatus &= ~IPM_PHEAD_ERR;
             break;
             
        case PM_INK_OR_TANK_ERR:
             if(errCode == E_NO_INK) 
             {
                 cmStatus.pmErrState.lwInkErr &= ~BIT_16_MASK;

                 if(cmStatus.pmErrState.lwInkErr == 0)
                     cmStatus.pmErrState.lwStatus &= ~IPM_INK_ERR;
             }
             else 
             {
                // lint thinks the result on the right side below is signed, so it gives a warning
                 cmStatus.pmErrState.lwInkTankErr &= ~(1 << (errCode - INKTANK_ERR_SHIFT));     //lint !e502

                 if(cmStatus.pmErrState.lwInkTankErr == 0)
                     cmStatus.pmErrState.lwStatus &= ~IPM_INKTANK_ERR;
             }
             break;
             
        case PM_FATAL_ERR:
                // lint thinks the result on the right side below is signed, so it gives a warning
             cmStatus.pmErrState.lwFatalErr &= ~(1 << (errCode - FATAL_ERR_SHIFT));     //lint !e502

             if(cmStatus.pmErrState.lwFatalErr == 0)
                 cmStatus.pmErrState.lwStatus &= ~IPM_FATAL_ERR;
             break;

        default:
             break;
    } // switch

    #ifdef DBG_CM
    sprintf(DbgBuf, "Error in OIT clr Err message status x%x, err x%x", errType, errCode);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // end of fnCmClrIntErr()

/**********************************************************************
FUNCTION NAME: 
    fnFdrErrHandler

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static int fnFdrErrHandler(
    FN_PROCESS_INT_TASK_MSG
                        msgHandler,     // (I) Message handler
    INTERTASK           *rMsgP,         // (I) Inertask Message
    ulong               errType,        // (I) Error Type
    ulong               errCode,        // (I) Error Code
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{

    cmStatus.fdrErrState.bStatus = errType;
    cmStatus.fdrErrState.bErr = errCode;

    cmStatus.mailRunEvtPending |= FDR_EVT_MASK;
    if(msgHandler == fnPrcsFdrInitRsp)
    {
        *outputEvtP = v91_FdrInitRsp;
    }
    else  // For errors, continue init. OIT can only handle errors after init
    {
        // (void)fnMailProgress(EVT_FDR_ERR, IS_STOPPED, OK);
        *outputEvtP = v80_FdrErr;
    }

    return OK;

} // fnFdrErrHandler

/**********************************************************************
FUNCTION NAME: 
    fnRptFdrErr

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnRptFdrErr(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    cmStatus.mailRunEvtPending &= ~FDR_EVT_MASK;
    (void)fnMailProgress(EVT_FDR_ERR, IS_STOPPED, OK);

    return OK;

} // fnRptFdrErr

/**********************************************************************
FUNCTION NAME: 
    fnPmErrHandler

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPmErrHandler(
    FN_PROCESS_INT_TASK_MSG
                        msgHandler,     // (I) Message handler
    INTERTASK           *rMsgP,         // (I) Inertask Message
    ulong               errType,        // (I) Error Type
    ulong               errCode,        // (I) Error Code
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    ulong           errClrCode[LEN_2];
    uchar           mailRunEvt = 0;
    emCmStateEvent  inputEvt = *outputEvtP;
    BOOL            fStopRunRequired = FALSE;
    emMeterModel    meterModel;

    meterModel = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    // Ignore warnings
    if(errCode != E_NO_PAPER && errType != PM_WARNING)  
        fnStopCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);

    switch(errType)
    {
        case PM_WARNING:

             *outputEvtP = v24_PmWrng;
             switch(errCode)
             {
             case W_INK_LOW:
                  *outputEvtP = v27_PmKeepWrng;
                  mailRunEvt = EVT_PM_INK_ERR;
                  cmStatus.mailRunEvtPending |= INK_EVT_MASK;   
                  break;

             case W_INKTANK_MFTR_TIMEOUT_1:
             case W_INKTANK_MFTR_TIMEOUT_2:
                  /*------
                  *outputEvtP = v27_PmKeepWrng;
                  mailRunEvt = EVT_PM_INKTANK_ERR;
                  cmStatus.mailRunEvtPending |= TANK_EVT_MASK;  
                  errCode = W_INKTANK_MFTR_TIMEOUT_1;
                  --------*/
                  *outputEvtP = v0_Invalid;
                  inputEvt = *outputEvtP;
                  break;
             
             case W_INKTANK_INST_TIMEOUT_1:
             case W_INKTANK_INST_TIMEOUT_2:
                  /*------
                  *outputEvtP = v27_PmKeepWrng;
                  mailRunEvt = EVT_PM_INKTANK_ERR;
                  cmStatus.mailRunEvtPending |= TANK_EVT_MASK;  
                  errCode = W_INKTANK_INST_TIMEOUT_1;
                  --------*/
                  *outputEvtP = v0_Invalid;
                  inputEvt = *outputEvtP;
                  break;

             case W_WTANK_NEAR_FULL:
             case W_WTANK_FULL:
                  *outputEvtP = v27_PmKeepWrng;
                  mailRunEvt = EVT_PM_WASTETANK_ERR;
                  cmStatus.mailRunEvtPending |= WTANK_EVT_MASK; 
                   // save the status for use in the distributor messages
                   ucWasteInkTankStatus = (unsigned char)errCode;
                   break;
                                  
              case W_WTANK_ABOUT_FULL:
                   *outputEvtP = v27_PmKeepWrng;
                   mailRunEvt = EVT_PM_WASTETANK_ERR;
                   cmStatus.mailRunEvtPending |= WTANK_EVT_MASK; 
                   // save the status for use in the distributor messages
                   ucWasteInkTankStatus = (unsigned char)errCode;
                   // change the error code so it will be handled like a near full error
                   errCode = W_WTANK_NEAR_FULL;
                  
                  break;
                 
             case W_PIECE_SKIPPED:
                  *outputEvtP = v24_PmWrng;
                  mailRunEvt = EVT_PM_PAPER_ERR;
                  cmStatus.mailRunEvtPending |= PAPER_EVT_MASK;
                  break;

             default:
                  #ifdef DBG_CM
                  sprintf(DbgBuf, "Unknown Warning: 0x%x %d", errCode, cmState.eCurrentState);
                  fnDumpStringToSystemLog(DbgBuf);
                  #endif
                  *outputEvtP = v0_Invalid;
                  inputEvt = *outputEvtP;

                  break;

             } // switch

             if(*outputEvtP != v0_Invalid)
             {
                 cmStatus.pmErrState.lwStatus |= IPM_WARNING;
                 cmStatus.pmErrState.lwStatus |= (1 << (errCode + WRN_SHIFT));
             }
             break;
             
        case PM_MSG_ERR:
             #if ENABLE_MSG_ERR
             *outputEvtP = v78_PmMsgErr;
             mailRunEvt = EVT_PM_PAPER_ERR;
             cmStatus.pmErrState.lwStatus |= IPM_MSG_ERR;
             cmStatus.pmErrState.lwStatus |= (1 << (errCode - MSG_ERR_SHIFT));
             sprintf(DbgBuf, "Msg error ###");
             fnDumpStringToSystemLog(DbgBuf);
             #else
             *outputEvtP = v0_Invalid;
             inputEvt = *outputEvtP;
             #endif
             break;
             
        case PM_PAPER_ERR:
             mailRunEvt = EVT_PM_PAPER_ERR;
             cmStatus.pmErrState.lwStatus |= IPM_PAPER_ERR;
             fStopRunRequired = TRUE;
             if(errCode == E_JAM)
             {
                 *outputEvtP = v73_PmRspJamErr;
                 cmStatus.pmErrState.lwPaperErr |= (1 << (errCode - JAM_ERR_SHIFT));
             }
             else if(errCode == E_JAM_LEVER_OPEN)
             {
                 *outputEvtP = v15_PmJamOpen;
                 cmStatus.pmErrState.lwPaperErr |= (1 << (errCode - JAM_ERR_SHIFT));
             }
             else if(errCode == E_NO_PAPER)
             {
                 *outputEvtP = v71_PmPprErr;
                 //errClrCode[POS_0] = errType;
                 //errClrCode[POS_1] = errCode;
                 //(void)fnPmSetMode(DISABLE_MODE, 0, 0);
                 //(void)OSSendIntertask(BJCTRL, CM, CP_CLR_PRT_ERR, LONG_DATA, errClrCode, sizeof(errClrCode));
                 //(void)OSStartTimer(MAILRUN_DELAY_TRANSPORT_STOP);
                 cmStatus.pmErrState.lwPaperErr |= (1 << (errCode - PAPER_ERR_SHIFT));
             }
             else if(errCode == E_PAPER_SKEW)
             {
                 *outputEvtP = v82_PmRspSkewErr;
//Ram updated not to clear when Skew only clear Paper error when Jam lever open/close.
//                 fStopRunRequired = FALSE;
//                 errClrCode[POS_0] = errType;
//                 errClrCode[POS_1] = errCode;
//                 (void)OSSendIntertask(BJCTRL, CM, CP_CLR_PRT_ERR, LONG_DATA, errClrCode, sizeof(errClrCode));
                 //(void)OSStartTimer(MAILRUN_DELAY_TRANSPORT_STOP);
                 cmStatus.pmErrState.lwPaperErr |= (1 << (errCode - PAPER_ERR_SHIFT));
             }
             else
             {
                 *outputEvtP = v74_PmRspPprErr;
                 cmStatus.pmErrState.lwPaperErr |= (1 << (errCode - PAPER_ERR_SHIFT));
             }

             if(msgHandler == fnPrcsRunRsp)
             {
                 // SYS does not care about error code, but just in case
                 errClrCode[POS_0] = rMsgP->IntertaskUnion.lwLongData[POS_0]; 
                 errClrCode[POS_1] = rMsgP->IntertaskUnion.lwLongData[POS_1];

                 // send notice to SYS
                 (void)OSSendIntertask(SYS, CM, CS_JOB_CANCEL, LONG_DATA, errClrCode, sizeof(errClrCode));
/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//               fnStartCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);
/////////////////////////////////////////////////////////////////
             }

             break;

#ifndef NO_PRINTER //TODO JAH remove me after we get the printers
        case PM_PHEAD_ERR:
            fStopRunRequired = TRUE;
             if(msgHandler == fnPrcsMtncRsp || msgHandler == fnPrcsRunRsp) 
                 *outputEvtP = v75_PmRspHdErr;
             else 
                 *outputEvtP = v69_HdErr;

             mailRunEvt = EVT_PM_PHEAD_ERR;

             cmStatus.mailRunEvtPending |= PHEAD_EVT_MASK;  
             cmStatus.pmErrState.lwStatus |= IPM_PHEAD_ERR;
             cmStatus.pmErrState.lwPheadErr |= (1 << (errCode - PHEAD_ERR_SHIFT));
             break;
             
        case PM_INK_OR_TANK_ERR:
            fStopRunRequired = TRUE;
            if(msgHandler == fnPrcsMtncRsp || msgHandler == fnPrcsRunRsp) 
                *outputEvtP = v31_PmRspInkErr;
            else 
                *outputEvtP = v30_InkErr;

            if(errCode == E_NO_INK) 
            {
                mailRunEvt = EVT_PM_INK_ERR;
                cmStatus.mailRunEvtPending |= INK_EVT_MASK;    
                cmStatus.pmErrState.lwStatus |= IPM_INK_ERR;
                cmStatus.pmErrState.lwInkErr |= BIT_16_MASK;
            }
            else 
            {
                mailRunEvt = EVT_PM_INKTANK_ERR;
                cmStatus.mailRunEvtPending |= TANK_EVT_MASK;   
                cmStatus.pmErrState.lwStatus |= IPM_INKTANK_ERR;
                cmStatus.pmErrState.lwInkTankErr |= (1 << (errCode - INKTANK_ERR_SHIFT));
                // since there is a print head or ink tank problem, set the manufacturer type
                // to unknown because the data in cmStatus has been updated to reflect errors for the print head
                // and/or ink tank
                ucInkTankMfgType = UNKNOWN_MFG;
            }
            break;
#endif //TODO JAH remove me after we get the printers
             
        case PM_TAPE_ERR:
            mailRunEvt = EVT_TAPE_ERR;

            fStopRunRequired = TRUE;
            cmStatus.mailRunEvtPending |= TAPE_EVT_MASK;   
            cmStatus.pmErrState.lwStatus |= IPM_TAPE_ERR;
            cmStatus.pmErrState.lwTapeErr |= (1 << (errCode - TAPE_ERR_SHIFT));
            *outputEvtP = v81_TapeErr;
            break;

#ifndef NO_PRINTER //TODO JAH remove me after we get the printers
        case PM_FATAL_ERR:
            fStopRunRequired = TRUE;
            if(msgHandler == fnPrcsInitRsp) 
                *outputEvtP = v79_PmInitErr;
            else 
                *outputEvtP = v77_PmFtlErr;

            mailRunEvt = EVT_PM_FATAL_ERR;
            cmStatus.pmErrState.lwStatus |= IPM_FATAL_ERR;
            cmStatus.pmErrState.lwFatalErr |= (1 << (errCode - FATAL_ERR_SHIFT));
            break;
#endif //TODO JAH remove me after we get the printers

        default:
            #ifdef DBG_CM
            sprintf(DbgBuf, "PM Error: x%x, err x%x st %d",
                    errType, errCode, cmState.eCurrentState);
            fnDumpStringToSystemLog(DbgBuf);
            #endif
            *outputEvtP = v0_Invalid;
            inputEvt = *outputEvtP;
            break;

    } // switch

    sSystemStatus.stopCause = NO_STOP_CAUSE;
    if (( meterModel >= CSD3) && fStopRunRequired)
    {
        if (subPrtMode == CM_DIFF_WEIGH_MODE || (sFeederStatus.fRunning && !sFeederStatus.fStopping))
            (void)fnStopRun(STOP_FDR_ONLY, 0, EMERGENCY_STOP);
    }
    if (fStopRunRequired && sSystemStatus.fRunning)
    {       
        (void)fnCmCommitNewState(v129_PmErrStopRunReq,s512_Running);
        *outputEvtP = v129_PmErrStopRunReq;
        sSystemStatus.stopCause = STOP_ERROR;
    }
    else if (*outputEvtP == v82_PmRspSkewErr)
    {
        (void)fnCmCommitNewState(v135_StopDueToSkew,s512_Running);
        *outputEvtP = v135_StopDueToSkew;
        sSystemStatus.stopCause = STOP_ERROR;
    }

    // Report to OIT about the error or one time warning
    // The condition is to avoid stop in the middle of printing, the
    // warning will be sent out either when the system is idle, or right after printing 
    // is completed
    if(    *outputEvtP != v75_PmRspHdErr && *outputEvtP != v31_PmRspInkErr && *outputEvtP != v69_HdErr 
        && *outputEvtP != v30_InkErr && *outputEvtP != v79_PmInitErr && *outputEvtP != v27_PmKeepWrng 
        && *outputEvtP != inputEvt && *outputEvtP != v0_Invalid )
    {
        (void)fnMailProgress(mailRunEvt, IS_STOPPED, OK);
    }   
    
    #ifdef DBG_CM
    sprintf(DbgBuf, "Err msgId x%x Err x%x evt %d st %d",
          rMsgP->bMsgId, errCode, *outputEvtP, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);

    sprintf(DbgBuf, "PMErrH %1x %1x %1x",sFeederStatus.fRunning, sSystemStatus.fTapeRunning, sSystemStatus.fRunning);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // end of fnPmErrHandler()

/**********************************************************************
FUNCTION NAME: 
    fnRptKeepWarnToOit

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnRptKeepWarnToOit(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    (void)fnRptPendingEvts();
    return OK;

} // fnRptKeepWarnToOit

/**********************************************************************
FUNCTION NAME: 
    fnRptKeepWarnToOit_Idle

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnRptKeepWarnToOit_Idle(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    (void)fnRptPendingEvts();
    *newStateP = fnRtnToIdle(__LINE__);
    return OK;

} // fnRptKeepWarnToOit

/**********************************************************************
FUNCTION NAME: 
    fnFtlErr

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnFtlErr(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    uchar       stdnMode = STDN_PWR_OFF;

    #ifdef DBG_CM
    sprintf(DbgBuf, "Ftl %d x%x %d %d", rMsgP->bSource, rMsgP->bMsgId, inputEvt, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif


    if( sSystemStatus.fRunning == TRUE )
        (void)fnStopRun(STOP_ALL, PM_STOP_NOW, EMERGENCY_STOP); 

    // shutdown PM
    (void)OSSendIntertask(BJCTRL, CM, CP_PRT_SHUTDOWN, BYTE_DATA, &stdnMode, sizeof(stdnMode));

    return OK;

} // fnFtlErr

/**********************************************************************
FUNCTION NAME: 
    fnPmPprErr

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPmPprErr(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    // Stop feeder & tape
    (void)fnStopRun(STOP_ALL, PM_STOP_NOW, EMERGENCY_STOP); 

    if(*newStateP == s22_Idle) 
        *newStateP = fnRtnToIdle(__LINE__);

    #ifdef DBG_CM
    sprintf(DbgBuf, "Ppr x%x evt %d state %d", rMsgP->bMsgId, inputEvt, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPmPprErr

/**********************************************************************
FUNCTION NAME: 
    fnStopRun

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnStopRun(
    emStopType      stopType,           // (I) stop type
    emPmStopType    pmStopType,         // (I) PM main trapnsort stop mode 
    emFdrStopType   fdrStopType)        // (I) Feeder stop type
{
    uchar       fdrStopMsg = fdrStopType;
    uchar       pmStopRunMsg = pmStopType;
    emMeterModel eModelType = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    switch(stopType)
    {
    case STOP_TAPE_ONLY:
        //if (sSystemStatus.fTapeRunning)
        if(eModelType >= CSD3)
        {
            sSystemStatus.fStopping = TRUE;
            sSystemStatus.fTapeStopping = TRUE;
            (void)OSSendIntertask(BJCTRL, CM, CP_STOP_TAPE_RUN, NO_DATA, NULL, 0);
        }
        break;

    case STOP_FDR_ONLY:
        if(eModelType >= CSD3 && (subPrtMode == CM_DIFF_WEIGH_MODE || (sFeederStatus.fRunning && !sFeederStatus.fStopping)))
        {
            sFeederStatus.fStopping = TRUE;
            sFeederStatus.fStopTransport = TRUE;
            (void)OSSendIntertask(FDR, CM, CF_STOP_RUN, BYTE_DATA, &fdrStopMsg, sizeof(fdrStopMsg));
        }
        break;

    case STOP_MAIN_ONLY:
        //if (sSystemStatus.fRunning)
        //{
            sSystemStatus.fStopping = TRUE;
            sFeederStatus.fStopTransport = FALSE;
            (void)OSSendIntertask(BJCTRL, CM, CP_STOP_RUN, BYTE_DATA, &pmStopRunMsg, sizeof(pmStopRunMsg));
        //}
        break;

    case STOP_TAPE_AND_MAIN:
        //if (sSystemStatus.fTapeRunning)
        if(eModelType >= CSD3)
        {
            sSystemStatus.fStopping = TRUE;
            sSystemStatus.fTapeStopping = TRUE;
            (void)OSSendIntertask(BJCTRL, CM, CP_STOP_TAPE_RUN, NO_DATA, NULL, 0);
        }
        //if (sSystemStatus.fRunning)
        //{
            sSystemStatus.fStopping = TRUE;
            sFeederStatus.fStopTransport = FALSE;
            (void)OSSendIntertask(BJCTRL, CM, CP_STOP_RUN, BYTE_DATA, &pmStopRunMsg, sizeof(pmStopRunMsg));
        //}
        break;

    case STOP_FEEDR_AND_MAIN:
        if(eModelType >= CSD3 && (subPrtMode == CM_DIFF_WEIGH_MODE || (sFeederStatus.fRunning && !sFeederStatus.fStopping)))
        {
            sFeederStatus.fStopping = TRUE;
            sFeederStatus.fStopTransport = TRUE;
            (void)OSSendIntertask(FDR, CM, CF_STOP_RUN, BYTE_DATA, &fdrStopMsg, sizeof(fdrStopMsg));
        }
        //if (sSystemStatus.fRunning)
        //{
            sSystemStatus.fStopping = TRUE;
            sFeederStatus.fStopTransport = FALSE;
            (void)OSSendIntertask(BJCTRL, CM, CP_STOP_RUN, BYTE_DATA, &pmStopRunMsg, sizeof(pmStopRunMsg));
        //}
        break;

    default: // STOP_ALL
        if(eModelType >= CSD3 && sFeederStatus.fRunning && !sFeederStatus.fStopping)
        {
            sFeederStatus.fStopping = TRUE;
            sFeederStatus.fStopTransport = TRUE;
            (void)OSSendIntertask(FDR, CM, CF_STOP_RUN, BYTE_DATA, &fdrStopMsg, sizeof(fdrStopMsg));
        }
        //if (sSystemStatus.fTapeRunning)
        if(eModelType >= CSD3)
        {
            sSystemStatus.fStopping = TRUE;
            sSystemStatus.fTapeStopping = TRUE;
            (void)OSSendIntertask(BJCTRL, CM, CP_STOP_TAPE_RUN, NO_DATA, NULL, 0);
        }
        //if (sSystemStatus.fRunning)
        //{
            sSystemStatus.fStopping = TRUE;
            sFeederStatus.fStopTransport = FALSE;
            (void)OSSendIntertask(BJCTRL, CM, CP_STOP_RUN, BYTE_DATA, &pmStopRunMsg, sizeof(pmStopRunMsg));
        //}
        break;
    }

    return OK;

} // fnStopRun

/**********************************************************************
FUNCTION NAME: 
    fnHdInkErr

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnHdInkErr(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{

    #ifdef DBG_CM
    sprintf(DbgBuf, "Head/Ink x%x %d %d", rMsgP->bMsgId, inputEvt, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    // Send delayed events
    (void)fnRptPendingEvts();

    // Stop feeder & tape
    (void)fnStopRun(STOP_ALL, PM_STOP_NOW, EMERGENCY_STOP); 

    if(cmStatus.opMode == SEALONLY_MODE || subPrtMode == SEALONLY_MODE) 
    {
         *newStateP = s5_SlOnlyIdle;
    }

    return OK;

} // fnHdInkErr

/**********************************************************************
FUNCTION NAME: 
    fnHdInkErr_Oit

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnHdInkErr_Oit(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 

{
    (void)fnHdInkErr(inputEvt, rMsgP, newStateP);

    // Stop feeder & tape
    (void)fnStopRun(STOP_ALL, PM_STOP_NOW, EMERGENCY_STOP); 

    // send response to OI for screen transition
    return fnMtncRsp_Oit(inputEvt, rMsgP, newStateP);

}  // fnHdInkErr_Oit

/**********************************************************************
FUNCTION NAME: 
    fnHdInkErr_SysModeRsp

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnHdInkErr_SysModeRsp(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 

{
    // send mode response to SYS 
    (void)fnSetModeRsp(inputEvt, rMsgP, newStateP);

    // Stop feeder & tape
    (void)fnStopRun(STOP_ALL, PM_STOP_NOW, EMERGENCY_STOP); 

    return fnHdInkErr(inputEvt, rMsgP, newStateP);

}  // fnHdInkErr_SysModeRsp

/**********************************************************************
FUNCTION NAME: 
    fnHdInkErr_RplCmplt

DESCRIPTION:

INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnHdInkErr_RplCmplt(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg
    emCmState           *newStateP)     // (I/O) New State, not committed yet

{
// Even though there is an ink error, tell tablet PH replacement complete
    SendBaseEventToTablet(BASE_EVENT_PH_REPLACE_DONE, NULL);

    return fnHdInkErr(inputEvt, rMsgP, newStateP);

}  // fnHdInkErr_RplCmplt

/**********************************************************************
FUNCTION NAME:
    fnPmJamErr

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPmJamErr(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{

    #ifdef DBG_CM
    sprintf(DbgBuf, "Jam x%x %d %d",
         rMsgP->bMsgId, inputEvt, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    // Stop feeder & tape
    (void)fnStopRun(STOP_ALL, PM_STOP_NOW, EMERGENCY_STOP); 

    return OK;

} // fnPmJamErr

/**********************************************************************
FUNCTION NAME: 
    fnFdrJamErr

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnFdrJamErr(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    UINT8 bEvt=0,bStatus=0,bErr=0;

    #ifdef DBG_CM
    sprintf(DbgBuf, "FdrJam x%x %d %d",
         rMsgP->bMsgId, inputEvt, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    bEvt = rMsgP->IntertaskUnion.bByteData[0];
    bStatus = rMsgP->IntertaskUnion.bByteData[1];
    bErr = rMsgP->IntertaskUnion.bByteData[2];

    fnPrcsFeederHasStopped();
    (void)fnStopRun(STOP_MAIN_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
    if (bEvt != FDR_MAIL_JOB_COMPLETE || bStatus != FDR_JAM_STATUS || bErr != MCP_INTERLOCK_CVR_OPEN)   // DSD 06/07/2007
        fnSetFdrJamErrInfo(bEvt,bStatus,bErr);  
    (void)fnMailProgress(EVT_FDR_ERR, IS_STOPPED, OK);  

    return OK;

} // fnFdrJamErr

/**********************************************************************
FUNCTION NAME: 
    fnPmWrng

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPmWrng(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{

    #ifdef DBG_CM
    sprintf(DbgBuf, "Wrng Ex%x %d %d", rMsgP->bMsgId, inputEvt, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPmWrng

/**********************************************************************
FUNCTION NAME: 
    fnFdrInit

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnFdrInit(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    uchar         thruput = THRUPUT_120;

    if (sFeederStatus.fDfuReInitPending || !sFeederStatus.bInitReqCount)
    {
        sFeederStatus.fDfuReInitPending = FALSE;
        sFeederStatus.bInitReqCount++;
        (void)OSSendIntertask(FDR, CM, CF_INIT, BYTE_DATA, &thruput, sizeof(thruput));
#ifdef RD_DM400_PRINTDBG
        // looking through a DM400 test print log, we need a connection response from the MCP
        (void)OSSendIntertask(FDR, MCP_CLASS_DRVR, MCP_CONNECTED, NO_DATA, 0, 0);
#endif
    }

    return OK;
}

/**********************************************************************
FUNCTION NAME: 
    fnPmDftInit

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPmDftInit(
    uchar   *bModeP)                    // (I) Init Mode & Operation Mode
{
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    static INIT_PM_DATA     pmInitReq = { CSD2, POWER_UP, DISABLE_MODE,
                                    DFLT_RIGHT_MARGIN, DFLT_TOP_MARGIN,
                                    DFLT_RIGHT_OFFSET, DFLT_TOP_OFFSET, BEFORE_PRINTING_PURGE_STRONG_OFF };

    ulong msgSize = sizeof(INIT_PM_DATA);

    pmInitReq.bMeterModel = fnGetMeterModel();

    pmInitReq.bMaintenanceMode = fnGetBeforePrintPurgeStrong();

    pmInitReq.bInitMode = *bModeP;

    // SYS DISABLE MODE change: uncomment two lines below
    pmInitReq.bOperationMode = bModeP[POS_1];
    cmStatus.opMode = bModeP[POS_1];            

    // Send Init request to PM
    (void)OSSendIntertask(BJCTRL, CM, CP_INIT_PM, GLOBAL_PTR_DATA, &pmInitReq, msgSize);

    return OK;

} // fnPmDftInit

/**********************************************************************
FUNCTION NAME: 
    fnPmRun

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPmRun(uchar runType)
{
    uchar           runMsg[LEN_6];
    uchar           bRunMode;
    emSpeed         bIPS = fnGetTransportSpeed();

    if (bIPS == IPS_0_0)
    {// error condition
        return ERR;
    }

    runMsg[POS_0] = runType;

    if (cmStatus.opMode == SEALONLY_MODE)
    {
        bRunMode = RUNMODE_SEALONLY;
    }
    else if ( cmStatus.bMediaType == MEDIA_MAIL )
    {
        bRunMode = RUNMODE_ENVELOPE;      // Envelope
    }
    else
    {
        bRunMode = RUNMODE_TAPE;         // Tape
    }
    runMsg[POS_1] = bRunMode;

    runMsg[POS_2] = (uchar) bIPS;
    runMsg[POS_3] = cmStatus.rightMarginOffset; 
    runMsg[POS_4] = cmStatus.topMarginOffset;
    runMsg[POS_5] = runDelayTime;

    // send Run command
    sSystemStatus.fRunning = TRUE;
    sSystemStatus.stopCause = NO_STOP_CAUSE;
    sSystemStatus.fTapeStopPending = FALSE;
    (void)OSSendIntertask(BJCTRL, CM, CP_RUN, BYTE_DATA, runMsg, sizeof(runMsg));

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> Let PM Run Run Type %d Speed %d" ,runType, bIPS);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;
} // fnPmRun

/**********************************************************************
FUNCTION NAME: 
    fnPrtTstImg

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrtTstImg(ushort *imageBuf, ulong imageSize, ulong printLen)
{ 
    ulong        prtLen = 0;

    extern uchar        bDiagTestPrintPatternID;
    extern ulong        tp_generate(uchar, ushort *);

    if(ejtFlg == TRUE)
    {
        (void)OSSendIntertask(BJCTRL, CM, CP_EJECT, NO_DATA, NULL, 0);
        (void)fnStopRun(STOP_FDR_ONLY, 0, GRACEFUL_STOP);
    }
    else
    {
        prtLen = tp_generate(bDiagTestPrintPatternID, imageBuf);

        // PPP: Generate image
        #ifdef DBG_CM
        sprintf(DbgBuf, "CM>> TestPtn#%d col %d", bDiagTestPrintPatternID, prtLen);
        fnDumpStringToSystemLog(DbgBuf);
        #endif

        // Ask PM to Print
        prtData.pImage = (void *)imageBuf;
        prtData.lwImageLength = prtLen;  // imageSize;
        prtData.lwMinPrintLength = 1025; // printLen;

        (void)OSSendIntertask(BJCTRL, CM, CP_PRINT, GLOBAL_PTR_DATA, &prtData, sizeof(PRINT_DATA));

        // Information only 
        (void)OSSendIntertask(SYS, CM, CS_PRINT_STARTED, NO_DATA, NULL, 0);

    } // ejtFlg = FALSE;

    return OK;

} // fnPrtTstImg

/**********************************************************************
FUNCTION NAME: 
    fnGrantPmMtnc

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnGrantPmMtnc(
    emMtncType  type)           // (I) Maintenance type
{
    char            chEventStatus;
    unsigned long   lwCurrentEvents = 0;
    uchar           mtncType = type;
    int             rtn = OK;
    
    // Wait for PSOCs to be initialized before allowing power on maintenance; 30 second timeout
    chEventStatus = OSReceiveEvents(SYS_PWRUP_EVENT_GROUP, EV_PSOC_INIT_COMPLETE, 30000, OS_AND, &lwCurrentEvents);
    if (chEventStatus != SUCCESSFUL)
    { // CSD should already be put in error state by SYS task
        dbgTrace(DBG_LVL_ERROR,"Timed out waiting for PSOC initialization prior to Print Maintenance\r\n");
        rtn = ERR;
    }

    // Permit Maintenance
    (void)OSSendIntertask(BJCTRL, CM, CP_PERFORM_PRT_MTNC, BYTE_DATA, &mtncType, sizeof(mtncType));
    
    //Gather ink tank data
    (void)OSSendIntertask(BJCTRL, CM, CP_GET_INKTANK_DATA, GLOBAL_PTR_DATA, &cmStatus.inkTankData, sizeof(INK_TANK_DATA));
    (void)OSSendIntertask(BJCTRL, CM, CP_GET_PRTHEAD_DATA, GLOBAL_PTR_DATA, &cmStatus.prtHeadData, sizeof(PRINT_HEAD_DATA));
    
    return rtn;

} // fnGrantPmMtnc

/**********************************************************************
FUNCTION NAME: 
    fnRtnToIdle

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
emCmState   fnRtnToIdle(int lineNo)
{
    switch(cmStatus.opMode)
    {
    case NORMAL_MODE:
         return s4_NmlIdle;
         
    case SEALONLY_MODE:
         return s5_SlOnlyIdle;

    case DIAG_MODE:
         return s6_DiagIdle;

    case DISABLE_MODE:
         return s7_DisableIdle;

    default:
         #ifdef DBG_CM
         sprintf(DbgBuf, "Error opMode %d L%d", cmStatus.opMode, lineNo);
         fnDumpStringToSystemLog(DbgBuf);
         #endif

         return s4_NmlIdle;
    }

} // fnRtnToIdle

/**********************************************************************
FUNCTION NAME: 
    fnPrcsUnknownMsg

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsUnknownMsg(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{

    #ifdef DBG_CM
    sprintf(DbgBuf, "Unknown msg: src %d Id x%x %d", rMsgP->bSource, rMsgP->bMsgId, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnPrcsUnknownMsg

/**********************************************************************
FUNCTION NAME: 
    fnCmHandleInvalid

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnCmHandleInvalid(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{

    #ifdef DBG_CM
    sprintf(DbgBuf, "Inv %d x%x %d %d",
         rMsgP->bSource, rMsgP->bMsgId, inputEvt, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnCmHandleInvalid

/**********************************************************************
FUNCTION NAME: 
    fnCmHandleMismatch

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnCmHandleMismatch(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
#ifdef DBG_CM
    static char     *mismatchErrMsg[] = 
    {
        "Invalid state",                        // 0
        "Uninitialized",                        // 1
        "Being initialized",                    // 2
        "Waiting for Maintenance Response from PM",    // 3
        "Normal Idle",                          // 4
        "Seal Only Idle",                       // 5
        "Diagnostic Idle",                      // 6
        "Disable Idle",                         // 7
        "Sleeping",                             // 8
        "Before Print Maintenance",             // 9
        "Normal Mode Staging",                  // 10
        "Image generating",                     // 11
        "Ejecting",                             // 12
        "Printing",                             // 13
        "Replacement Prepare",                  // 14
        "PM Replacing",                         // 15
        "Cover is open",                        // 16
        "PM Replacement finish",                // 17
        "Replacement finish",                   // 18
        "Seal Only Mode: Running",              // 19
        "Pre Cover Open",                       // 20
        "Soft Power Off Pending",               // 21
        "Idle pending",                         // 22
        "Status check",                         // 23
        "Jam Lever is Open",                    // 24
        "Cover and Jam Lever are Open",         // 25
        "Sleep: Pending response from PM",      // 26
        "Soft Power Off",                       // 27
        "Bob response pending",                 // 28
        "SYS reponse pending",                  // 29
        "No Action",                            // 30
        "Bob error",                            // 31
        "PSOC Reseed response pending",         // 32
        "PSOC Commit response pending",         // 33
        "Reserved 34",                          // 34
        "Reserved 35",                          // 35
        "Reserved 36",                          // 36
        "Reserved 37",                          // 37
        "Reserved 38",                          // 38
        "Reserved 39",                          // 39
    
        // Error States
        "Printing Jam Error",                   // 40
        "Mismatch state",                       // 41
        "Eject Error",                          // 42
        "Staging Error",                        // 43
        "Printing Error",                       // 44
        "Seal Running Error",                   // 45
        "Print head / Ink Tank error",          // 46
        "Intertask message error",              // 47

        "Internal CM Errors",                   // 48
        "Fatal Errors"                          // 49

    };

    sprintf(DbgBuf, "Mismatch evt %d st %d %s",
            inputEvt, cmState.eCurrentState, ((cmState.eCurrentState < ARRAY_LENGTH(mismatchErrMsg)) ? mismatchErrMsg[cmState.eCurrentState] : "") );
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;
} // fnCmHandleMismatch

/**********************************************************************
FUNCTION NAME: 
    fnCmDontCare

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnCmDontCare(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM>> DontCare MsgID x%x evt %d state %d", rMsgP->bMsgId, inputEvt, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;

} // fnCmDontCare

/**********************************************************************
FUNCTION NAME: 
    fnPmMsgErr

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPmMsgErr(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{

    #ifdef DBG_CM
    sprintf(DbgBuf, "Msg err Id x%x evt %d %d", rMsgP->bMsgId, inputEvt, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;
} // fnPmMsgErr

/**********************************************************************
FUNCTION NAME: 
    fnPmInitErr

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPmInitErr(
    emCmStateEvent      inputEvt,       // (I)   Embedded event in the msg
    INTERTASK           *rMsgP,         // (I)   Received msg 
    emCmState           *newStateP)     // (I/O) New State, not committed yet 
{
    #ifdef DBG_CM
    sprintf(DbgBuf, "PM init err");
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    return OK;
} // fnPmInitErr

/**********************************************************************
FUNCTION NAME: 
    fnCMMailRunTimerExpire

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
void fnCMMailRunTimerExpire(ulong   inputVar)
{
    if (inputVar == MAILRUN_CMPLT_TIMER_ID)
    {
        fnStopCMTimer(MAILRUN_CMPLT_TIMER_ID,__LINE__);
        (void)OSSendIntertask(CM,CM,CM_STOP_MAILRUN_MSG,NO_DATA,NULL,0);
    }
    else if (inputVar == MAILRUN_CMPLT_TIMER4)
    {
        fnStopCMTimer(MAILRUN_CMPLT_TIMER4,__LINE__);
        (void)OSSendIntertask(CM,CM,CM_STOP_MAILRUN_MSG,NO_DATA,NULL,0);
    }
    else if (inputVar == MAILRUN_REPORT_CMPLT_TIMER_ID)
    {
        fnStopCMTimer(MAILRUN_REPORT_CMPLT_TIMER_ID,__LINE__);
        (void)OSSendIntertask(CM,CM,CM_STOP_MAILRUN_MSG,NO_DATA,NULL,0);
    }
    else if (inputVar == FP_MAINTENANCE_STOP_TIMER)
    {
        fnStopCMTimer(FP_MAINTENANCE_STOP_TIMER,__LINE__);
        (void)OSSendIntertask(CM,CM,CM_MAINTENANCE_STOP_TIMER_EXPIRED,NO_DATA,NULL,0);
    }
}

/**********************************************************************
FUNCTION NAME: 
    fnCMMailRunTimerExpire2

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
void fnCMMailRunTimerExpire2(ulong  inputVar)
{
    int currState = (int)fnCmGetCrntState();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    emMeterModel eModelType = fnGetMeterModel();

    #ifdef DBG_CM
    sprintf(DbgBuf, "CM Timer (%d) expired st %d", inputVar, cmState.eCurrentState);
    fnDumpStringToSystemLog(DbgBuf);
    #endif

    if (inputVar == MAILRUN_DELAY_TRANSPORT_STOP &&
        (currState & CM_STOPPING_STATE_MASK) == CM_STOPPING_STATE_PREFIX)
    {
        // stop the timer
        (void)fnStopCMTimer(MAILRUN_DELAY_TRANSPORT_STOP,__LINE__);
        // if we are stopping send a message to the CM task
        // to process the stop
        (void)OSSendIntertask(CM,CM,CM_TRANSPORT_CLEAR_TIMER_EXPIRED,NO_DATA,NULL,0);
        return;
    }
    else if (inputVar == FP_MAINTENANCE_STOP_TIMER)
    {
        // stop the timer
        (void)fnStopCMTimer(FP_MAINTENANCE_STOP_TIMER,__LINE__);
        (void)OSSendIntertask(CM,CM,CM_MAINTENANCE_STOP_TIMER_EXPIRED,NO_DATA,NULL,0);
        return;
    }
    else if (eModelType == CSD2 || cmStatus.bMediaType == MEDIA_TAPE || sSystemStatus.fTapeStopPending)
    {
        (void)OSSendIntertask(CM,CM,CM_STOP_MAILRUN_MSG,NO_DATA,NULL,0);
        return;     
    }
    // DSD 03/01/2007 - Feeder has stopped and no new mail has entered the system - so stop.
    //else if (eModelType >= CSD3 && sSystemStatus.stopCause == STOP_NORMAL)
    //{
    //  fnStopAllCMTimers();
    //
    //}
    else if (eModelType >= CSD3)
    {
        if (++bMailRunTimerExpCnt >= 90)
            (void)OSSendIntertask(CM,CM,CM_STOP_MAILRUN_MSG,NO_DATA,NULL,0);
        return;
    }


} // fnCMMailRunTimerExpire2

/**********************************************************************
FUNCTION NAME: 
    fnGetMargins

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
void    fnGetMargins(MARGIN_VALUE *marginValue)
{
    marginValue->topMarginOffset = cmStatus.topMarginOffset;
    marginValue->rightMarginOffset = cmStatus.rightMarginOffset;

} // fnGetMargins

/**********************************************************************
FUNCTION NAME: 
    fnSetMargins

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
void    fnSetMargins(MARGIN_VALUE *marginValue)
{
    cmStatus.topMarginOffset = marginValue->topMarginOffset;
    cmStatus.rightMarginOffset = marginValue->rightMarginOffset; 

} // fnSetMargins

//TODO - determine model number at runtime
static emMeterModel emFPModelNumber = CSD2;

/**********************************************************************
FUNCTION NAME: 
    fnInitModelNumber

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
void fnInitModelNumber(void)
{
    UINT8 majorHwVer, minorHwVer;
    BOOL fFdrHwPresent = FALSE;
    emMeterModel emModelNum = CSD2;
    
//TODO - confirm that for initial release, major and minor HW version numbers not used
    fnGetHardwareVersion(&majorHwVer, &minorHwVer);

        fFdrHwPresent = fnIsFeederHardwarePresent();
        if (fFdrHwPresent)
        {
            emModelNum = CSD3;
        }
        else
        {
            emModelNum = CSD2;    // no feeder present
        }

    CMOSSignature.bUicType = (UINT8)emModelNum;

    emFPModelNumber = CMOSSignature.bUicType;
}

/**********************************************************************
FUNCTION NAME: 
    fnGetMeterModel

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
emMeterModel fnGetMeterModel(void)
{   
    
#ifdef NO_PRINTER
    return CSD2;  // because board #4 has a CSD3 powersupply
#else
    return emFPModelNumber;
#endif

} // fnGetMeterModel

/**********************************************************************
FUNCTION NAME: 
    fnGetCurrentMediaType

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
UINT8 fnGetCurrentMediaType (void)
{
    return cmStatus.bMediaType;
}

/********************************************************************************/
/* TITLE: fnGetPrintHeadSerialNumber                                            */
/* AUTHOR: Craig DeFilippo                                                      */
/* INPUT:  none                                                                 */
/* DESCRIPTION: Frame the printhead serial number as a long and return it       */
/* OUTPUT: BCD Printhead serial number                                          */
/********************************************************************************/
long fnGetPrintHeadSerialNumber(void)
{
    long lPhSn;

    memcpy(&lPhSn, cmStatus.prtHeadData.bSerialNumber, sizeof(long));
    
    return(lPhSn);
}


/**********************************************************************
 FUNCTION NAME:          fnChangeThruputTestValue
 DESCRIPTION:
       Cycles the test thruput value through all the Thruput values so 
       that a specific value can be tested.
          
 INPUTS:     None        
 RETURNS:    None
 WARNINGS/NOTES:
  1. This should only ever run on test builds. 
  2. Each time it is called, it changes the bThruputTestingValue value to the
     "next" one.  After the highest value, change it back to 0, so that 
     normal thruput logic will control value.
  3. When thruput testing is on, the speed set by bThruputTestingValue is used
     to override all other logic for feeder speed, 

 HISTORY:
  2012.09.24 Clarisa Bellamy - Initial.
-----------------------------------------------------------------------------*/
// This can only be changed on the emulator or in test builds.
UINT8 bThruputTestingValue    =  0;
void fnChangeThruputTestValue( void )
{
    switch( bThruputTestingValue    )
    {
        // If its off, set it to the lowest value.
        case 0:
            bThruputTestingValue = (UINT8)THRUPUT_65;
            break;

        case THRUPUT_65:
            bThruputTestingValue = (UINT8)THRUPUT_70;
            break;

        case THRUPUT_70:
            bThruputTestingValue = (UINT8)THRUPUT_88_CANADA;
            break;

        case THRUPUT_88_CANADA:
            bThruputTestingValue = (UINT8)THRUPUT_90;
            break;

        case THRUPUT_90:
            bThruputTestingValue = (UINT8)THRUPUT_120;
            break;

        case THRUPUT_120:
        default:    // Set back to 0, so normal logic returns.
            bThruputTestingValue = 0;
            break;   
    }
    return;
}

/**********************************************************************
 FUNCTION NAME:          fnGetThruputTestValue
 DESCRIPTION:
       Retrieve the thruput test value.
 INPUTS:     None        
 RETURNS:    bThruputTestingValue
 WARNINGS/NOTES:
  1. returns type UINT8 not emThruput.  Zero value means thruput testing is off.

 HISTORY:
  2012.09.24 Clarisa Bellamy - Initial.
-----------------------------------------------------------------------------*/
UINT8 fnGetThruputTestValue( void )
{
    return( bThruputTestingValue );
}

/**********************************************************************
 FUNCTION NAME:          fnClearThruputTestValue
 DESCRIPTION:
       Clear the thruput test value, which turns thruput testing off.
 INPUTS:        None        
 RETURNS:       None
 WARNINGS/NOTES:    None
 HISTORY:
  2012.09.24 Clarisa Bellamy - Initial.
-----------------------------------------------------------------------------*/
void fnClearThruputTestValue( void )
{
    bThruputTestingValue = 0;
}


/**********************************************************************
FUNCTION NAME:          fnGetCurrentThruputValue
DESCRIPTION:
        Checks the country code, BP_DEFAULT_RATING_COUNTRY_CODE, and the enabled
        features to determine the appropriate thruput speed for the feeder.   
INPUTS:
        None        
RETURNS:
        emThruput values defined in cmpublic.h
WARNINGS/NOTES:
 1. Until version 02.10.00004, this did not look at the country code.  In order
    for the feeder to go slow enough in Canada (ECDSA) to work, the Feature for 
    Slow speed HAD to be enabled.
 2. After version 02.10.0004, the country code for Canada forces the slower speed
    of THRUPUT_88_CANADA (actually around ~85 LPM.)  For now, in Canada, features 
    are disregarded, because there is only one speed for Canada. 
 3. Starting with version 02.10.0006, for a non-Canadian country, check feature 
    codes for slow, medium, high and set throughput to the highest one that is 
    enabled.  If none are enabled, default is medium (90 LPM), because this has 
    been the default speed for 5 years.  

HISTORY:
 2012.09.24 Clarisa Bellamy - Change logic to look for the highest throughput 
                    feature enabled and select that.  (Canada is still the exception, 
                    always at "88" no matter what is selected.)
 2012.09.24 Clarisa Bellamy - Change so that when the BP_DEFAULT_RATING_COUNTRY_CODE 
                    is set to CANADA (1), we always select the "canadian" thruput.
                    (Used to use the LOW_THRUPUT feature code to determine this.) 
                    For all other countries, use default of medium (90 LPM) unless 
                    the feature code for HI_THRUPUT is set, then select 120 LPM.  
                    Else if feature code for Low_Thruput is set, select 70 LPM.

**********************************************************************/
emThruput fnGetCurrentThruputValue(void)
{
    BOOL            fEnabled;
    unsigned char   bFeatureState, bFeatureIndex;
//    unsigned char   currReport;
    emThruput eThruPut = THRUPUT_90;    // 90 lpm by default

    // Used for testing! on emulator
/*
    if(   (fAllowThruputTesting == TRUE) 
       && (bThruputTestingValue != 0) )
    {
        // Set to test value and return early !!!!
        eThruPut = (emThruput)bThruputTestingValue;
        return( eThruPut );
    }
*/

    // Check for highest speed that is feature-enabled: 
    if( IsFeatureEnabled(&fEnabled, HI_THRUPUT_SPEED_SUPPORT, &bFeatureState, &bFeatureIndex) && fEnabled )
    {
        eThruPut = THRUPUT_120;         // 120 lpm has been purchased
    }
    else if( IsFeatureEnabled(&fEnabled, MED_THRUPUT_SPEED_SUPPORT, &bFeatureState, &bFeatureIndex) && fEnabled )
    {
        // It's redundant, but clearer this way.  If both medium and low are set, use medium.
        eThruPut = THRUPUT_90;          // Feature set to 90 lpm
    }
    else if( IsFeatureEnabled(&fEnabled, LOW_THRUPUT_SPEED_SUPPORT, &bFeatureState, &bFeatureIndex) && fEnabled )
    {
        eThruPut = THRUPUT_70;          // Feature set to 70 lpm
    }

    // Limit speeds when necessary:

    //   First check Canadian limit
    if( fnGetDefaultRatingCountryCode() == CANADA )
    {
        // Canada cannot do the default 90 LPM because ECDSA takes too long.
        // There is currently only one speed for Canada, the Default Canadian Speed.
        // This was selected via a the LOW_THRUPUT feature code, but it is really 
        //  as fast as Canada can go. 
        if( eThruPut > THRUPUT_88_CANADA )
        {
            eThruPut = THRUPUT_88_CANADA;
        }
    }

    // Checking for the special (slow) german reports is done last so
    // that only the german reports reduce the throughput.
    // Only "slow" it down if it is going faster.
    else if( (subPrtMode == CM_REPORT_MODE) && (eThruPut > THRUPUT_90 )  )
    {
//TODO - implement fnGetCurrentReportType()
#if 0
        currReport = fnGetCurrentReportType();
        if (currReport == GERM_DOM_MANIFEST_RPT || currReport == GERM_INTL_MANIFEST_RPT)
            eThruPut = THRUPUT_90;
#endif
    }

    return eThruPut;
}

//TODO - confirm checking for WOW not necessary
#if 0
/**********************************************************************
FUNCTION NAME: 
    fnGetWOWThruputValue

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
emWOWThruput fnGetWOWThruputValue(void)
{
    BOOL            fEnabled;
    unsigned char   bFeatureState, bFeatureIndex, currReport;
    emWOWThruput eWOWThruPut = LOW_WOW_THRUPUT;    // 65 lpm by default for WOW
    
    if(IsFeatureEnabled(&fEnabled, HI_WOW_THRUPUT_SPEED_SUPPORT, &bFeatureState, &bFeatureIndex) && fEnabled)
    {
        eWOWThruPut = HI_WOW_THRUPUT;         // 90 lpm has been purchased
    }
    else if (IsFeatureEnabled(&fEnabled, MED_WOW_THRUPUT_SPEED_SUPPORT, &bFeatureState, &bFeatureIndex) && fEnabled)
    {
        eWOWThruPut = MED_WOW_THRUPUT;
    }
    else if (IsFeatureEnabled(&fEnabled, LOW_WOW_THRUPUT_SPEED_SUPPORT, &bFeatureState, &bFeatureIndex) && fEnabled)
    {
        eWOWThruPut = LOW_WOW_THRUPUT;
    }
    return eWOWThruPut;
}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////
// Start New CM Stopping Code
////////////////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************
FUNCTION NAME: 
    fnStartCMTimer

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static void fnStartCMTimer(const UINT8 bTimerId, int iLineNum)
{
    (void)OSStopTimer(bTimerId);
    bMailRunTimerExpCnt = 0;
    (void)OSStartTimer(bTimerId);
    sprintf(DbgBuf, ">> CM Start Timer %d", bTimerId);
    fnDumpStringToSystemLog(DbgBuf);
}

/**********************************************************************
FUNCTION NAME: 
    fnStartCMMailRunCompleteTimer

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static void fnStartCMMailRunCompleteTimer(const int iStopType, const int iLineNum)
{
    (void)OSStopTimer(MAILRUN_CMPLT_TIMER_ID);
    (void)OSStopTimer(MAILRUN_CMPLT_TIMER4);
    (void)OSStopTimer(MAILRUN_REPORT_CMPLT_TIMER_ID);
    if (iStopType == QUICK_STOP)
        (void)OSStartTimer(MAILRUN_CMPLT_TIMER4);
    else if (iStopType == REPORT_STOP)
    	(void)OSStartTimer(MAILRUN_REPORT_CMPLT_TIMER_ID);
    else
        (void)OSStartTimer(MAILRUN_CMPLT_TIMER_ID);
    sprintf(DbgBuf, "CM>> StartMailRunCmpTimer %d", iStopType);
    fnDumpStringToSystemLog(DbgBuf);
}

/**********************************************************************
FUNCTION NAME: 
    fnStopCMTimer

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static void fnStopCMTimer(const UINT8 bTimerId, int iLineNum)
{
    (void)OSStopTimer(bTimerId);
    sprintf(DbgBuf, "CM>> CM Stop Timer %d", bTimerId);
    fnDumpStringToSystemLog(DbgBuf);
}

/**********************************************************************
FUNCTION NAME: 
    fnStopAllCMTimers

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static void fnStopAllCMTimers(void)
{
    (void)OSStopTimer(MAILRUN_CMPLT_TIMER_ID);
    (void)OSStopTimer(MAILRUN_REPORT_CMPLT_TIMER_ID);
    (void)OSStopTimer(MAILRUN_CMPLT_TIMER4);
    (void)OSStopTimer(MAILRUN_DELAY_TRANSPORT_STOP);
    (void)OSStopTimer(FP_MAINTENANCE_STOP_TIMER);
    sprintf(DbgBuf, ">> CM Stop All Timers");
    fnDumpStringToSystemLog(DbgBuf);
}

/**********************************************************************
FUNCTION NAME: 
    fnSetFdrJamErrInfo

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static void fnSetFdrJamErrInfo(const UINT8 bEvt, const UINT8 bStatus, const UINT8 bErrCode)
{
    cmStatus.fdrErrState.bStatus = FDR_PAPER_ERR;
    if (bEvt == FDR_MAIL_JOB_COMPLETE &&
        bStatus == FDR_JAM_STATUS)
    {
        cmStatus.fdrErrState.bErr = ERR_FDR_JAM;
        cmStatus.fdrErrState.bJamCode = bErrCode;
    }
}

/**********************************************************************
FUNCTION NAME: 
    fnPrcsTransportClearTimerExpire

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsTransportClearTimerExpire(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    *outputEvtP = v132_TransportClear;
    return OK;

}

/**********************************************************************
FUNCTION NAME: 
    fnPrcsCmStopSystemReq

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsCmStopSystemReq(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    emMeterModel model = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    sSystemStatus.fCmStopPending = TRUE;
    if (model == CSD3 && sFeederStatus.fRunning && !sFeederStatus.fStopping)
    {
        sSystemStatus.stopCause = STOP_NORMAL;
        sSystemStatus.fRestartPending = FALSE;
        (void)fnStopRun(STOP_FDR_ONLY, 0, GRACEFUL_STOP);
    }
    else if (!sSystemStatus.fPieceInProgress)
    {
        (void)fnStopRun(STOP_MAIN_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
        (void)fnCmCommitNewState(v128_NormalStopRunReq,s259_StopTransport);
        *outputEvtP = v128_NormalStopRunReq;
    }
    else
    {
        fnStartCMMailRunCompleteTimer(NORMAL_STOP,__LINE__);
    }
    return OK;
}

/**********************************************************************
FUNCTION NAME: 
    fnStopStateSendDisableModeMsgs

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static void fnStopStateSendDisableModeMsgs(void)
{
    //cmStatus.opMode = DISABLE_MODE;
    (void)fnSetModeCommon();
    (void)fnFdrSetMode(CM_DISABLED_MODE);
    (void)fnPmSetMode(DISABLE_MODE, 0, 0);
}

/**********************************************************************
FUNCTION NAME: 
    fnResetModesToLastRunningValues

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
#if 0 //unused
static void fnResetModesToLastRunningValues(void)
{
    emMeterModel model = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    fnSetModeCommon();
    if (model >= CSD3 && sFeederStatus.bCurrMode != sFeederStatus.bLastRunningMode)
        (void)fnFdrSetMode(sFeederStatus.bLastRunningMode);
    else
        (void)fnMsgState(SET_STATE, FDR, ss2_Responded);
    if (sSystemStatus.bCurrMode != sSystemStatus.bLastRunningMode)
    {
        (void)fnMsgState(SET_STATE, BJCTRL, ss1_Sent);
        (void)fnPmSetMode(sSystemStatus.bLastRunningMode, 0, 0);  // FRACA GMSE00102137
    }
}
#endif
/**********************************************************************
FUNCTION NAME: 
    fnPrcsFeederHasStopped

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static void fnPrcsFeederHasStopped(void)
{
    sFeederStatus.fRunning = FALSE;
    sFeederStatus.fStopping = FALSE;
    sFeederStatus.fStopTransport = TRUE;
}

/**********************************************************************
FUNCTION NAME: 
    fnPrcsPmHasStopped

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static void fnPrcsPmHasStopped(void)
{
    sSystemStatus.fStopping = FALSE;
    sSystemStatus.fRunning = FALSE;
    cmStatus.fIsRunning = FALSE;
}

/**********************************************************************
FUNCTION NAME: 
    fnStopDoneIsModeResetRequired

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
#if 0 //unused
static BOOL fnStopDoneIsModeResetRequired(void)
{
    emMeterModel model = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    if (sSystemStatus.bCurrMode != sSystemStatus.bLastRunningMode ||
        (sFeederStatus.bCurrMode != sFeederStatus.bLastRunningMode && model >= CSD3))
        return TRUE;
    else
        return FALSE;
}
#endif

/**********************************************************************

DESCRIPTION:
     Is anything running that could prevent ink replacement?

**********************************************************************/

BOOL fnIsSomeSubSystemRunning(void)
{
    emMeterModel eModelType = fnGetMeterModel();
    if (eModelType == CSD2)
    {
        return (BOOL)(sSystemStatus.fRunning || sSystemStatus.fStopping);
    }
    else
    {
        return (BOOL)(sSystemStatus.fRunning || sSystemStatus.fStopping || sSystemStatus.fTapeRunning || sFeederStatus.fRunning);
    }
}

/**********************************************************************
FUNCTION NAME: 
    fnStopCompleteReturnToIdle

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static tCmEvtElmnt fnStopCompleteReturnToIdle(emCmStateEvent inputEvt)
{
    tCmEvtElmnt evtElmnt = { s30_NoAction, NULL };
    evtElmnt.state = fnRtnToIdle(__LINE__);
    evtElmnt.fnCmEvtHandler = NULL;
    sSystemStatus.fGotoDisableAfterStop = FALSE;
    sSystemStatus.stopCause = NO_STOP_CAUSE;
    sSystemStatus.fEjectMailPiece = FALSE;
    sSystemStatus.fPieceInProgress = FALSE;
    sSystemStatus.fSysStopPending = FALSE;
    sSystemStatus.fCmStopPending = FALSE;
    sSystemStatus.fTapeStopping = FALSE;
    sSystemStatus.fProcessingPieces = FALSE;
    sSystemStatus.fCancelStart = FALSE;
    cmStatus.bMediaType = NO_MEDIA;
    fnSendUpdatedFeederSensorStatus();
    (void)fnCmCommitNewState(inputEvt,evtElmnt.state);
    fnSendMailRunComplete();
    (void)fnRptPendingEvts();
    fnMsgState(SET_STATE, FDR, ss2_Responded);
    fnMsgState(SET_STATE, BJCTRL, ss2_Responded);
    fnSendModeRsp();
    //fnSendUpdatedFeederSensorStatus();

    return evtElmnt;
}

/**********************************************************************
FUNCTION NAME: 
    fnPrcsErrWhileStopping

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static tCmEvtElmnt fnPrcsErrWhileStopping(emCmStateEvent inputEvt)
{
    tCmEvtElmnt evtElmnt = { s30_NoAction, NULL };

    // flag that an error occurred
    sSystemStatus.fGotoDisableAfterStop = TRUE;
    sSystemStatus.stopCause = STOP_ERROR;

    // just to be safe - disable the sub-systems    
    //fnStopStateSendDisableModeMsgs(); // DSD - remove this for now...

    // stop all sub-systems
    (void)fnStopRun(STOP_ALL,PM_STOP_NOW,EMERGENCY_STOP);

    // go to the recovery state
    evtElmnt.state = s261_ImmediateStopRecovery;
    (void)fnCmCommitNewState(inputEvt,evtElmnt.state);

    return evtElmnt;
}

/**********************************************************************
FUNCTION NAME: 
    fnStopFeederStateHandler

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
tCmEvtElmnt fnStopFeederStateHandler(
    emCmStateEvent         inputEvt)        // (I) input event 
{
    tCmEvtElmnt evtElmnt = { s30_NoAction, NULL };

    switch (inputEvt) {
    case v130_FdrStopped:   // feeder stopped normally
    case v99_FdrJam:        // feeder stopped because of a jam
        evtElmnt.state = s257_DisablePm;
        evtElmnt.fnCmEvtHandler = NULL;
        fnPrcsFeederHasStopped();
        fnStopStateSendDisableModeMsgs();       
        (void)fnCmCommitNewState(inputEvt,evtElmnt.state);
        break;
    case v129_PmErrStopRunReq:
        evtElmnt = fnPrcsErrWhileStopping(inputEvt);
        break;

    default:
        // stop compiler warnings
        break;
    };

    return evtElmnt;
}

/**********************************************************************
FUNCTION NAME: 
    fnStopTapeStateHandler

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
tCmEvtElmnt fnStopTapeStateHandler(emCmStateEvent inputEvt)
{
    tCmEvtElmnt evtElmnt = { s30_NoAction, NULL };

    switch (inputEvt) {
    case v33_StopTapeRunRsp:
        evtElmnt.state = s257_DisablePm;
        evtElmnt.fnCmEvtHandler = NULL;
        fnStopStateSendDisableModeMsgs();       
        (void)fnCmCommitNewState(inputEvt,evtElmnt.state);
        break;
    case v129_PmErrStopRunReq:
        evtElmnt = fnPrcsErrWhileStopping(inputEvt);
        break;

    default:
        // stop compiler warnings
        break;

    };

    return evtElmnt;
}

/**********************************************************************
FUNCTION NAME: 
    fnDisablePmStateHandler

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
tCmEvtElmnt fnDisablePmStateHandler(
    emCmStateEvent         inputEvt)        // (I) input event 
{
    tCmEvtElmnt evtElmnt = { s30_NoAction, NULL };
    emMeterModel model = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    switch (inputEvt) {
    case v131_PmDisabled:
        evtElmnt.state = s258_ClearTransport;
        evtElmnt.fnCmEvtHandler = NULL;
        if (model >= CSD3)
        {
            // DM400C - wait another second until the piece
            // exits the system
            (void)OSStartTimer(MAILRUN_DELAY_TRANSPORT_STOP);
            (void)fnCmCommitNewState(inputEvt,evtElmnt.state);
        }
        else
        {
            // DM300C - per Rich's request stop quicker so that 
            // a piece can not be inserted while printing the 
            // previous piece and thus pulling in the second
            // piece too far.
            (void)OSSendIntertask(CM,CM,CM_TRANSPORT_CLEAR_TIMER_EXPIRED,NO_DATA,NULL,0);
            (void)fnCmCommitNewState(inputEvt,evtElmnt.state);
        }
        break;

    case v129_PmErrStopRunReq:
        evtElmnt = fnPrcsErrWhileStopping(inputEvt);
        break;

    default:
        // stop compiler warnings
        break;

    };

    return evtElmnt;
}

/**********************************************************************
FUNCTION NAME: 
    fnClearTransportStateHandler

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
tCmEvtElmnt fnClearTransportStateHandler(
    emCmStateEvent         inputEvt)        // (I) input event 
{
    tCmEvtElmnt evtElmnt = { s30_NoAction, NULL };

    switch (inputEvt) {
    case v132_TransportClear:
        sprintf(DbgBuf, "XptClr %1x %1x %1x",sFeederStatus.fRunning, sSystemStatus.fTapeRunning, sSystemStatus.fRunning);
        fnDumpStringToSystemLog(DbgBuf);
        evtElmnt.state = s259_StopTransport;
        evtElmnt.fnCmEvtHandler = NULL;
        (void)fnStopRun(STOP_MAIN_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
        (void)fnCmCommitNewState(inputEvt,evtElmnt.state);
        break;
    case v129_PmErrStopRunReq:
        evtElmnt = fnPrcsErrWhileStopping(inputEvt);
        break;

    default:
        // stop warnings
        break ;
    };

    return evtElmnt;
}

/**********************************************************************
FUNCTION NAME: 
    fnStopTransportStateHandler

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
tCmEvtElmnt fnStopTransportStateHandler(
    emCmStateEvent         inputEvt)        // (I) input event 
{
    tCmEvtElmnt evtElmnt = { s30_NoAction, NULL };

    switch (inputEvt) {
    case v133_TransportStopped:
//      if (!sSystemStatus.fGotoDisableAfterStop && sSystemStatus.stopCause != STOP_ERROR)
//      {
//          if (fnStopDoneIsModeResetRequired())
//          {
                // Return to nomal idle
//              evtElmnt.state = s260_StopDoneResetMode;
//              evtElmnt.fnCmEvtHandler  = NULL;        
//              fnResetModesToLastRunningValues();
//              (void)fnCmCommitNewState(inputEvt,evtElmnt.state);
//          }
//          else
//          {
//              evtElmnt = fnStopCompleteReturnToIdle(inputEvt);
//          }
//      }
//      else
//      {
            // Goto Disable mode (because of some type of error)
            evtElmnt = fnStopCompleteReturnToIdle(inputEvt);
//      }
//      fnPrcsPmHasStopped();
        break;
    case v129_PmErrStopRunReq:
        evtElmnt = fnPrcsErrWhileStopping(inputEvt);
        break;

    default:
        // stop warnings.
        break ;
    };

    return evtElmnt;
}

/**********************************************************************
FUNCTION NAME: 
    fnStopResetModeStateHandler

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
tCmEvtElmnt fnStopResetModeStateHandler(
    emCmStateEvent         inputEvt)        // (I) input event 
{
    tCmEvtElmnt evtElmnt = { s30_NoAction, NULL };

    switch (inputEvt) {
    case v134_ModeResetComplete:
        evtElmnt = fnStopCompleteReturnToIdle(inputEvt);
        break;

    default:
        // stop warnings.
        break ;
    };

    return evtElmnt;
}
/**********************************************************************
FUNCTION NAME: 
    fnImmediateStopRecoveryStateHandler

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
tCmEvtElmnt fnImmediateStopRecoveryStateHandler(emCmStateEvent inputEvt)
{
    tCmEvtElmnt evtElmnt = { s30_NoAction, NULL };
    BOOL fCheckDone = FALSE;

    switch (inputEvt) {
    case v130_FdrStopped:   // feeder stopped normally
    case v99_FdrJam:        // feeder stopped because of a jam
    case v133_TransportStopped: // transport stopped normally
    case v129_PmErrStopRunReq:  // the PM has stopped abnormally due to some error
    case v33_StopTapeRunRsp:    // the tape has stopped normally
        if (inputEvt == v133_TransportStopped)
        {
            fnPrcsPmHasStopped();       
        }
        if (inputEvt == v130_FdrStopped || inputEvt == v99_FdrJam)
        {
            fnPrcsFeederHasStopped();
        }
        fCheckDone = TRUE;
        break;

    default:
        // stop compiler warnings
        break;

    };

    // check to see if all the sub-systems have stopped
    if (fCheckDone && !sSystemStatus.fRunning && !sSystemStatus.fTapeRunning && !sFeederStatus.fRunning)    // all sub-systems have stopped
    {
        //if (!sSystemStatus.fGotoDisableAfterStop)
        //{
        //  evtElmnt.state = s260_StopDoneResetMode;
        //  evtElmnt.fnCmEvtHandler = NULL;         
        //  fnResetModesToLastRunningValues();
        //  (void)fnCmCommitNewState(inputEvt,evtElmnt.state);
        //}
        //else
        //{
            evtElmnt = fnStopCompleteReturnToIdle(inputEvt);
        //}
        //cmStatus.fIsRunning = FALSE;
        //(void)fnSetDisableMode(v0_Invalid,NULL,NULL); // force the system into a disabled mode                        
        //evtElmnt = fnStopCompleteReturnToIdle(v0_Invalid);
    }

    return evtElmnt;
}

/**********************************************************************
FUNCTION NAME: 
    fnRunningStateHandler

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
tCmEvtElmnt fnRunningStateHandler(
    emCmStateEvent         inputEvt)        // (I) input event 
{
    tCmEvtElmnt evtElmnt = { s30_NoAction, NULL };
    emMeterModel model = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    switch (inputEvt) {
    case v128_NormalStopRunReq: // A Normal (Gracefull) stop is being requested
    case v135_StopDueToSkew:    // A Normal stop is being requested due to skew
        fnStopAllCMTimers();

        if (inputEvt == v135_StopDueToSkew)
            sSystemStatus.fGotoDisableAfterStop = TRUE;
        sSystemStatus.stopCause = STOP_NORMAL;
        
        if (cmStatus.bMediaType == MEDIA_MAIL)  // envelopes
        {
            if (model >= CSD3)    // CSD3 - Stop the feeder first
            {
                if (sFeederStatus.fRunning && !sFeederStatus.fStopping)
                {
                    evtElmnt.state = s256_StopFdr;
                    evtElmnt.fnCmEvtHandler = NULL;
                    (void)fnPmSetMode(DISABLE_MODE, 0, 0);  // disable PM so no more mail is printed
                    (void)fnStopRun(STOP_FDR_ONLY, 0, GRACEFUL_STOP);
                    (void)fnCmCommitNewState(inputEvt,evtElmnt.state);              
                }
                else
                {
                    evtElmnt.state = s257_DisablePm;
                    evtElmnt.fnCmEvtHandler = NULL;
                    (void)fnCmCommitNewState(inputEvt,evtElmnt.state);
                    fnStopStateSendDisableModeMsgs();
                }
            }
            else    // CSD2 - No feeder Disable the PM
            {
                evtElmnt.state = s257_DisablePm;
                evtElmnt.fnCmEvtHandler = NULL;
                (void)fnCmCommitNewState(inputEvt,evtElmnt.state);
                fnStopStateSendDisableModeMsgs();
            }
        }
        else    // tapes
        {
            sSystemStatus.fTapeStopPending = FALSE;
            evtElmnt.state = s259_StopTransport;
            evtElmnt.fnCmEvtHandler = NULL;
            (void)fnStopRun(STOP_MAIN_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
            (void)fnCmCommitNewState(inputEvt,evtElmnt.state);          
        }       
        break;
    case v130_FdrStopped:
        sSystemStatus.stopCause = STOP_NORMAL;
/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//      fnStopAllCMTimers();
/////////////////////////////////////////////////////////////////
        evtElmnt.state = s257_DisablePm;
        evtElmnt.fnCmEvtHandler = NULL;
        fnStopStateSendDisableModeMsgs();       
        (void)fnCmCommitNewState(inputEvt,evtElmnt.state);
        break;
    case v129_PmErrStopRunReq:
    case v99_FdrJam:
        sSystemStatus.stopCause = STOP_ERROR;
/////////////////////////////////////////////////////////////////
// DSD 03/19/2007 Timer, Stop, & Mode Change Fixes
//      fnStopAllCMTimers();
//      (void)fnMailProgress(EVT_FDR_ERR, IS_STOPPED, OK);
/////////////////////////////////////////////////////////////////
        fnPrcsFeederHasStopped();
        evtElmnt = fnPrcsErrWhileStopping(inputEvt);
        break;

    default:
        // stop compiler warnings
        break;

    };

    return evtElmnt;
}
//case v73_PmRspJamErr:
//case v15_PmJamOpen:
//case v74_PmRspPprErr:
//case v81_TapeErr:
//case v77_PmFtlErr:
//case v82_PmRspSkewErr:


/**********************************************************************
FUNCTION NAME: 
    fnPrcsMaintStopReq

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
int fnPrcsMaintStopReq(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg

{

    *outputEvtP = v95_FdrStopRunRsp;
    if (sSystemStatus.fPausePending)
    {
        sSystemStatus.fPausePending = FALSE;
        sSystemStatus.stopCause = STOP_FDR_PAUSE;
    }
    else
        sSystemStatus.stopCause = STOP_MAINT;
    return OK;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// End New CM Stopping Code
////////////////////////////////////////////////////////////////////////////////////////////////////

/**********************************************************************
FUNCTION NAME: 
    fnGetPrintSerialNumber

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
UINT8 fnGetPrintSerialNumber(uchar *dest)
{
    if(cmStatus.pmNVMDataP == NULL)
        strncpy((char *)dest, "unknown", PRNTR_SN_LEN );
    else
        strncpy((char *)dest, (char *)cmStatus.pmNVMDataP->printer_serial_number, PRNTR_SN_LEN);

    dest[PRNTR_SN_LEN] = 0;
    return((uchar) strlen((const char*)dest));
}


/********************************************************************************/
/* TITLE: fnSetPrintSerialNumber                                                */
/* AUTHOR: Sandra Peterson                                                      */
/* INPUT:  Pointer to an array that is nul-terminated.                          */
/* DESCRIPTION: Set the Canon serial number string. Have to do it as an         */
/*              unsigned char to match what the EDM task has.                   */
/* OUTPUT: ASCII Canon serial number                                            */
/********************************************************************************/
BOOL fnSetPrintSerialNumber(const unsigned char *pSerialNum)
{
    BOOL    retVal = FALSE;


    if (strlen((const char *)pSerialNum) == PRNTR_SN_LEN)
    {
        NVRAM_DATA *pCanonStructure;


        // point to first copy of the Canon data
        pCanonStructure = (NVRAM_DATA *)CANON_COPY1_DATA_ADDRESS;

        // load the new value
        (void)memcpy((unsigned char *)(pCanonStructure->printer_serial_number), pSerialNum, PRNTR_SN_LEN);

        // recalculate the CRC
        pCanonStructure->check_sum = fnDoAdditiveByteChecksum((unsigned char *)pCanonStructure, 255);

        // point to second copy of the Canon data
        pCanonStructure = (NVRAM_DATA *)CANON_COPY2_DATA_ADDRESS;

        // load the new value
        (void)memcpy((unsigned char *)(pCanonStructure->printer_serial_number), pSerialNum, PRNTR_SN_LEN);

        // recalculate the CRC
        pCanonStructure->check_sum = fnDoAdditiveByteChecksum((unsigned char *)pCanonStructure, 255);

        if (initCnt)
        {
            // point to the RAM copy of the Canon data
            pCanonStructure = cmStatus.pmNVMDataP;

            // load the new value
            (void)memcpy((unsigned char *)(pCanonStructure->printer_serial_number), pSerialNum, PRNTR_SN_LEN);

            // recalculate the CRC
            pCanonStructure->check_sum = fnDoAdditiveByteChecksum((unsigned char *)pCanonStructure, 255);
        }

        retVal = TRUE;
    }

    return (retVal);
}


/********************************************************************************/
/* TITLE: fnDoAdditiveByteCRC                                                   */
/* AUTHOR: Sandra Peterson                                                      */
/* INPUT:  Pointer to an array of bytes.                                        */
/*          Number of bytes to calculate the checksum on.                       */
/* DESCRIPTION: Calculate an additive one-byte checksum                         */
/* OUTPUT: checksum byte                                                        */
/********************************************************************************/
unsigned char fnDoAdditiveByteChecksum(const unsigned char *pData, const unsigned short usByteCount)
{
    unsigned short usInx = 0;
    unsigned char ucCRC = 0;


    for ( ; usInx < usByteCount; usInx++)
    {
        ucCRC += pData[usInx];
    }

    return (ucCRC);
}



/********************************************************************************/
/* TITLE: fnCMGetWasteTankStatus                                                */
/* AUTHOR: Sandra Peterson                                                      */
/* INPUT:  none                                                                 */
/* DESCRIPTION: Get the current status of the waste ink tank.                   */
/* OUTPUT: status (i.e., full, near full, about near full, OK)                  */
/********************************************************************************/

unsigned char fnCMGetWasteTankStatus(void)
{
    return(ucWasteInkTankStatus);
}

/********************************************************************************/
/* TITLE: fnCMGetBoardBuildDate                                                 */
/* AUTHOR: Sandra Peterson                                                      */
/* INPUT:  Pointer to a DATETIME structure.                                     */
/* DESCRIPTION: Get the board build date.                                       */
/* OUTPUT: TRUE if could get the data, FALSE otherwise                          */
/********************************************************************************/
BOOL fnCMGetBoardBuildDate(DATETIME *pDate)
{
    if (initCnt)
    {
        pDate->bCentury = 20;
        pDate->bYear = cmStatus.pmNVMDataP->battery_install_year;
        pDate->bMonth = cmStatus.pmNVMDataP->battery_install_month;
        pDate->bDay = cmStatus.pmNVMDataP->battery_install_day;
        pDate->bDayOfWeek = 1;
        pDate->bHour = 0;
        pDate->bMinutes = 0;
        pDate->bSeconds = 0;
        return (TRUE);
    }
    else
        return (FALSE);
}


/********************************************************************************/
/* TITLE: fnCMSetBoardBuildDate                                                 */
/* AUTHOR: Sandra Peterson                                                      */
/* INPUT:  Pointer to a DATETIME structure.                                     */
/* DESCRIPTION: Set the board build date.                                       */
/* OUTPUT: none                                                                 */
/********************************************************************************/
void fnCMSetBoardBuildDate(const DATETIME *pDate)
{
    NVRAM_DATA *pCanonStructure;


    // point to first copy of the Canon data
    pCanonStructure = (NVRAM_DATA *)CANON_COPY1_DATA_ADDRESS;

    // load the new values
    pCanonStructure->battery_install_year  = pDate->bYear;
    pCanonStructure->battery_install_month = pDate->bMonth;
    pCanonStructure->battery_install_day = pDate->bDay;

    // recalculate the CRC
    pCanonStructure->check_sum = fnDoAdditiveByteChecksum((unsigned char *)pCanonStructure, 255);

    // point to second copy of the Canon data
    pCanonStructure = (NVRAM_DATA *)CANON_COPY2_DATA_ADDRESS;

    // load the new values
    pCanonStructure->battery_install_year  = pDate->bYear;
    pCanonStructure->battery_install_month = pDate->bMonth;
    pCanonStructure->battery_install_day = pDate->bDay;

    // recalculate the CRC
    pCanonStructure->check_sum = fnDoAdditiveByteChecksum((unsigned char *)pCanonStructure, 255);

    if (initCnt)
    {
        // point to the RAM copy of the Canon data
        pCanonStructure = cmStatus.pmNVMDataP;

        // load the new values
        pCanonStructure->battery_install_year  = pDate->bYear;
        pCanonStructure->battery_install_month = pDate->bMonth;
        pCanonStructure->battery_install_day = pDate->bDay;

        // recalculate the CRC
        pCanonStructure->check_sum = fnDoAdditiveByteChecksum((unsigned char *)pCanonStructure, 255);
    }
}


/********************************************************************************/
/* TITLE: fnCMGetInkDropCounts                                                  */
/* AUTHOR: Sandra Peterson                                                      */
/* INPUT:  Pointer to an array that is 6 bytes in size.                         */
/* DESCRIPTION: Get the ink drop counts.                                        */
/* OUTPUT: TRUE if could get the data, FALSE otherwise                          */
/********************************************************************************/
BOOL fnCMGetInkDropCounts(unsigned char *pData)
{
    unsigned char bInx;


    if (initCnt)
    {
        for (bInx = 0; bInx < 6; bInx++)
            pData[bInx] = cmStatus.pmNVMDataP->tank_eeprom.bDotCount[bInx];

        return (TRUE);
    }
    else
        return (FALSE);
}


/********************************************************************************/
/* TITLE: fnCMSetInkDropCounts                                                  */
/* AUTHOR: Sandra Peterson                                                      */
/* INPUT:  Pointer to an array that is 6 bytes in size.                         */
/* DESCRIPTION: Set the ink drop counts.                                        */
/* OUTPUT: none                                                                 */
/********************************************************************************/
void fnCMSetInkDropCounts(const unsigned char *pData)
{
    unsigned char bInx;


    if (initCnt)
    {
        for (bInx = 0; bInx < 6; bInx++)
            cmStatus.pmNVMDataP->tank_eeprom.bDotCount[bInx] = pData[bInx];
    }
}

/********************************************************************************/
/* TITLE: fnCMGetInkTankMfgType                                                 */
/* AUTHOR: Sandra Peterson                                                      */
/* INPUT:  none                                                                 */
/* DESCRIPTION: Determine who manufactured the ink tank.                        */
/* OUTPUT: manufacturer type                                                    */
/********************************************************************************/

unsigned char fnCMGetInkTankMfgType(void)
{
    return(ucInkTankMfgType);
}


/********************************************************************************/
/* TITLE: fnCMSetInkTankMfgType                                                 */
/* AUTHOR: Sandra Peterson                                                      */
/* INPUT:  none                                                                 */
/* DESCRIPTION: Sets the last known manufacturer type.                          */
/* OUTPUT: manufacturer type                                                    */
/********************************************************************************/

BOOL fnCMSetInkTankMfgType(const unsigned char ucNewType)
{
    char *  pEMDTrademarkString = 0;
    char    cDummyString[] = "000";
    char    cEmptyString[] = "";
    BOOL    fRetVal = TRUE;


    // First determine if we've gotten a valid type
    switch(ucNewType)
    {
        case OUR_OEM_MFG:
            pEMDTrademarkString = (char *)fnFlashGetAsciiStringParm(ASP_INK_TANK_TRADEMARK);
            if (!pEMDTrademarkString || !strlen(pEMDTrademarkString))
                fRetVal = FALSE;
            break;

        case SOME_OTHER_MFG:
            pEMDTrademarkString = cDummyString;
            break;

        case UNKNOWN_MFG:
            pEMDTrademarkString = cEmptyString;
            break;

        default:
            fRetVal = FALSE;
            break;
    }

    // if we haven't had a problem so far -AND-
    //  everything has been properly initialized -AND-
    //  there isn't a problem w/ the ink tank,
    // try to change the trademark data in the ink tank.
    if ((fRetVal == TRUE) && initCnt && (fnCMGetInkTankMfgType() != UNKNOWN_MFG))
    {
        (void)strncpy(cDummyTrademark, pEMDTrademarkString, 3);
        cDummyTrademark[3] = 0;
    }
    else
    {
        fRetVal = FALSE;
    }

    return(fRetVal);
}


/********************************************************************************/
/* TITLE: fnCMGetInkTankSerialNumber                                            */
/* AUTHOR: Sandra Peterson                                                      */
/* INPUT:  pointer to string buffer that has at least 9 bytes.                  */
/* DESCRIPTION: Get the serial number of the ink tank.                          */
/* OUTPUT: none                                                                 */
/********************************************************************************/

void fnCMGetInkTankSerialNumber(char *pString)
{
    pString[0] = (char)(cmStatus.pmNVMDataP->tank_eeprom.bSerialNumber[0] / 16) + 0x30;
    pString[1] = (cmStatus.pmNVMDataP->tank_eeprom.bSerialNumber[0] & 0x0F) + 0x30;
    pString[2] = (char)(cmStatus.pmNVMDataP->tank_eeprom.bSerialNumber[1] / 16) + 0x30;
    pString[3] = (cmStatus.pmNVMDataP->tank_eeprom.bSerialNumber[1] & 0x0F) + 0x30;
    pString[4] = (char)(cmStatus.pmNVMDataP->tank_eeprom.bSerialNumber[2] / 16) + 0x30;
    pString[5] = (cmStatus.pmNVMDataP->tank_eeprom.bSerialNumber[2] & 0x0F) + 0x30;
    pString[6] = (char)(cmStatus.pmNVMDataP->tank_eeprom.bSerialNumber[3] / 16) + 0x30;
    pString[7] = (cmStatus.pmNVMDataP->tank_eeprom.bSerialNumber[3] & 0x0F) + 0x30;
    pString[8] = 0;

    return;
}


/********************************************************************************/
/* TITLE: fnCMGetInkTankUpdateValue                                             */
/* AUTHOR: Sandra Peterson                                                      */
/* INPUT:  none                                                                 */
/* DESCRIPTION: Get the ink tank update value, which is in the lower nibble of  */
/*              the bUpdate byte in the tank_eeprom structure.                  */
/* OUTPUT: update value                                                         */
/********************************************************************************/

unsigned char fnCMGetInkTankUpdateValue(void)
{
    return(cmStatus.pmNVMDataP->tank_eeprom.bUpdate[0] & 0x0F);
}


/********************************************************************************/
/* TITLE: fnCMGetInkTankMfgDate                                                 */
/* AUTHOR: Sandra Peterson                                                      */
/* INPUT:  pointer to date/time structure.                                      */
/* DESCRIPTION: Get the manufacture date of the ink tank.                       */
/* OUTPUT: none                                                                 */
/********************************************************************************/

void fnCMGetInkTankMfgDate(DATETIME *pDate)
{
    pDate->bCentury = 20;
    pDate->bYear = (char)((cmStatus.pmNVMDataP->tank_eeprom.bManufactureDate[0] / 16) * 10) +
                    (cmStatus.pmNVMDataP->tank_eeprom.bManufactureDate[0] & 0x0F);
    pDate->bMonth = (char)((cmStatus.pmNVMDataP->tank_eeprom.bManufactureDate[1] / 16) * 10) +
                    (cmStatus.pmNVMDataP->tank_eeprom.bManufactureDate[1] & 0x0F);
    pDate->bDay = (char)((cmStatus.pmNVMDataP->tank_eeprom.bManufactureDate[2] / 16) * 10) +
                    (cmStatus.pmNVMDataP->tank_eeprom.bManufactureDate[2] & 0x0F);
    pDate->bDayOfWeek = 1;
    pDate->bHour = 0;
    pDate->bMinutes = 0;
    pDate->bSeconds = 0;

    return;
}

/********************************************************************************/
/* TITLE: fnCMGetInkTankInkType                                                 */
/* AUTHOR: Sandra Peterson                                                      */
/* INPUT:  none                                                                 */
/* DESCRIPTION: Get the ink tank color value, which is in the upper nibble of   */
/*              the bUpdate bytes in the tank_eeprom structure.                 */
/* OUTPUT: color value                                                          */
/********************************************************************************/

unsigned char fnCMGetInkTankInkType(void)
{
    return(cmStatus.pmNVMDataP->tank_eeprom.bUpdate[0] / 16);
}

/**********************************************************************
FUNCTION NAME: 
    fnSendUpdatedFeederSensorStatus

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static void fnSendUpdatedFeederSensorStatus(void)
{
    (void)fnMailProgress(EVT_SENSOR_STATUS_UPDATE, IS_STOPPED, OK);
}

/**********************************************************************
FUNCTION NAME: 
    fnSaveFeederSensorStatus

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static void fnSaveFeederSensorStatus(const UINT16 sensorStatus)
{
    UINT16 status = 0;
    UINT8   ucMode = fnOITGetPrintMode();
    
    if( (ucMode == PMODE_WOW) || (ucMode == PMODE_WEIGH_1ST) )
    {
        status  = sensorStatus & ( FTOPCVR_MASK | (W2_MASK << 8) | (W3_MASK << 8) );
    
        // clear sensor status
        cmStatus.snsrState.wSensorData &= ~(FTOPCVR_MASK | (W2_MASK << 8) | (W3_MASK << 8));
    }
    else
    {
        status  = sensorStatus & (FTOPCVR_MASK);// | FS1_MASK | FS2_MASK);
    
        // clear sensor status
        cmStatus.snsrState.wSensorData &= ~(FTOPCVR_MASK);// | FS1_MASK | FS2_MASK);
    }
    // set sensor status
    cmStatus.snsrState.wSensorData |= status;
}


// DSD 09/07/2007 - Refactor stopping
/**********************************************************************
FUNCTION NAME: 
    fnHasStoppingProcessBegun

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static BOOL fnHasStoppingProcessBegun(void)
{
    if (!sSystemStatus.fEjectMailPiece && !sSystemStatus.fCmStopPending && 
        sSystemStatus.fRunning && !sSystemStatus.fStopping && !sSystemStatus.fTapeStopPending)
        return FALSE;
    else
        return TRUE;
}

/**********************************************************************
FUNCTION NAME: 
    fnBeginToStopMailFlow

DESCRIPTION:
     
INPUTS:

RETURNS:

WARNINGS/NOTES:
**********************************************************************/
static void fnBeginToStopMailFlow(void)
{
    emMeterModel meterModel = fnGetMeterModel();
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    if (!fnHasStoppingProcessBegun())
    {
        sSystemStatus.fEjectMailPiece = TRUE;
        sSystemStatus.fCmStopPending = TRUE;
        if (cmStatus.bMediaType == MEDIA_TAPE)
        {
            sSystemStatus.stopCause = STOP_NORMAL;
            sSystemStatus.fTapeStopPending = TRUE;
            (void)fnStopRun(STOP_TAPE_ONLY, PM_STOP_NOW, GRACEFUL_STOP);
        }
        else if (meterModel >= CSD3 && sFeederStatus.fRunning && !sFeederStatus.fStopping)
        {
            sSystemStatus.stopCause = STOP_NORMAL;
            sSystemStatus.fRestartPending = FALSE;  // DSD 03/11/2007 - Cancel any restart
            (void)fnStopRun(STOP_FDR_ONLY, 0, GRACEFUL_STOP);
        }
        else if (meterModel >= CSD3 && !sFeederStatus.fRunning && !sFeederStatus.fStopping)
        {
            // DSD 05-16-2007 Fix Fraca 119446
            //  Timing issue with short envelopes & single piece
            //  mode results in the Feeder reporting that it has
            //  stopped before the PM sends the PC_PRINT_RSP
            //  message.  In old code, when getting the PC_PRINT_RSP
            //  message the CM only stopped the feeder if it was
            //  running.  However, since the feeder was already stopped
            //  the CM never stopped the transport (it was waiting for
            //  the Feeder to say it was stopped).  The fix is to
            //  set the mailrun complete timer if we get the PRINT_RSP
            //  and the feeder has stopped.
            sSystemStatus.stopCause = STOP_NORMAL;
            sSystemStatus.fRestartPending = FALSE;  // DSD 03/11/2007 - Cancel any restart
            fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);
        }
        else if (meterModel == CSD3 && sFeederStatus.fRunning)
        {
            sSystemStatus.stopCause = STOP_NORMAL;
            sSystemStatus.fRestartPending = FALSE;  // DSD 03/11/2007 - Cancel any restart
            (void)fnStopRun(STOP_FDR_ONLY, 0, GRACEFUL_STOP);
        }
        else if (meterModel == CSD3)
        {
            sSystemStatus.stopCause = STOP_NORMAL;
            sSystemStatus.fRestartPending = FALSE;  // DSD 03/11/2007 - Cancel any restart
            (void)fnStopRun(STOP_MAIN_ONLY,PM_STOP_NOW,GRACEFUL_STOP);
        }
        else
        {   // CSD2 Envelopes
            sSystemStatus.fRestartPending = FALSE;  // DSD 03/11/2007 - Cancel any restart
            if (!sSystemStatus.fPieceInProgress)
                fnStartCMMailRunCompleteTimer(QUICK_STOP,__LINE__);
            else
                fnStartCMMailRunCompleteTimer(NORMAL_STOP,__LINE__);
        }
    }
}



//-----------------------------------------------------------------------------
//  Functions to handle access to PM NVM data: 
//   That data should not be accessed directly, but should follow a call to the 
//   PM to retrieve a copy of the data.  Then the data is accessed.  
//-----------------------------------------------------------------------------


/**********************************************************************
* FUNCTION NAME:                        fnPrcsNvmDataRsp
* DESCRIPTION:   
*   PM has responded to an NvmRead request.
*   This may be part of a request from another task to change the PmNvm data.
*     
* INPUTS:   Standard inputs Message Handler function.         
*
* RETURNS:  OK
*
* NOTES:
* 1. This function either ends the CM side of the NVM Access if there is an 
*    error or 
* 2. Calls the callback function to go to the next step.
**********************************************************************/
int fnPrcsNvmDataRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    GEN_PM_RESPONSE   *pPmRsp;

    // Don't change the state based on this event.
    *outputEvtP = v0_Invalid;

    // Make sure we are waiting for this message: 
    if( bPmNvmAccessState != CM_PM_NVM_ACC_READ_PND )  
    {
        // Something has gone horribly wrong:
        // Put something in the syslog and cleanup. 
        dbgTrace(DBG_LVL_ERROR,"CM Received unexpected PmNvmDataResponse.\n" );
        bPmNvmAccessState = CM_PM_NVM_ACC_DONE;
        // Clear the Good and Bad events and set Available event. 
        //  Normally this is done by the client. But there is no client.
		fnClearDisabledStatus( DCOND_ACCESSING_PM_NVM ); 
        OSSetEvents( EVENT_GROUP_CMPM, ~(EVT_CM_PMNVM_BAD | EVT_CM_PMNVM_GOOD), OS_AND );        
        return( OK );
    }

    // Verify response is the correct type.
    if( rMsgP->bMsgType != LONG_DATA )
    {
        // Response format is incorrect. 
        dbgTrace(DBG_LVL_ERROR,"CM Received bad PmNvmDataResponse Type =%d.\n", rMsgP->bMsgType  );
        bPmNvmAccessState = CM_PM_NVM_ACC_DONE;
        // Set Bad event.
        OSSetEvents( EVENT_GROUP_CMPM, EVT_CM_PMNVM_BAD, OS_OR );        
        return( OK );
    }
    // Get ptr to Response data, and copy the status.
    pPmRsp = (GEN_PM_RESPONSE *)&(rMsgP->IntertaskUnion.lwLongData);
    pNvmAccPtr->lStatus = pPmRsp->lwStatus;

    // Test status:
    if( pPmRsp->lwStatus != PM_SUCCESS )
    {
        // Status is bad: 
        //  Copy error info from PM message to Client's Error Structure. 
        pNvmAccPtr->bCMErrorType = CM_PM_NVM_ERR_DURING_READ;
        pNvmAccPtr->lErrCode = pPmRsp->lwErrorCode;
        pNvmAccPtr->pDataCopy = NULL_PTR;
        dbgTrace( DBG_LVL_INFO, "CM: PmNvmDataRsp Error: Status=%d, Err=%d.\n", 
                 (int)pNvmAccPtr->lStatus, (int)pNvmAccPtr->lErrCode );

        //   Set internal state to done. Set Error Event.
        bPmNvmAccessState = CM_PM_NVM_ACC_DONE;  
        OSSetEvents( EVENT_GROUP_CMPM, EVT_CM_PMNVM_BAD, OS_OR );        
    }
    else
    {
        // Data has been read, examine client's msg to find out what is next.
        dbgTrace(DBG_LVL_INFO,"CM: PmNvmDataRsp Success.\n" );
        if( pNvmCallbackFn == NULL_PTR )
        {
            dbgTrace(DBG_LVL_INFO,"CM: Missing CallbackFunction.\n" );
        	pNvmAccPtr->bCMErrorType = CM_PM_NVM_ERR_DURING_READ;
        	pNvmAccPtr->lErrCode = 0xFFFFFF;
        	OSSetEvents( EVENT_GROUP_CMPM, EVT_CM_PMNVM_BAD, OS_OR );        
        }
        else
        {
            pNvmCallbackFn();
        }
    }

    return OK;
}


/**********************************************************************
* FUNCTION NAME:                        fnPrcsNvmUpdateRsp
* DESCRIPTION:   
*   PM has responded to an NvmUpdate request from the CM.
*     
* INPUTS:   Standard inputs Message Handler function.         
*
* RETURNS:  OK
*
* NOTES: 
**********************************************************************/
int fnPrcsNvmUpdateRsp(
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    GEN_PM_RESPONSE   *pPmRsp;

    // Don't change the state based on this event.
    *outputEvtP = v0_Invalid;

    // Make sure we are waiting for this message: 
    if( bPmNvmAccessState != CM_PM_NVM_ACC_WRITE_PND )  
    {
        // Something has gone horribly wrong:
        // Put something in the syslog and cleanup. 
        dbgTrace(DBG_LVL_ERROR,"CM Received unexpected PmNvmUpdateRsp.\n" );
        bPmNvmAccessState = CM_PM_NVM_ACC_DONE;
        // Clear the Good and Bad events and set Available event. 
        //  Normally this is done by the client. 
		// Post a disabling condition until the access is complete. 
		fnClearDisabledStatus( DCOND_ACCESSING_PM_NVM ); 
        OSSetEvents( EVENT_GROUP_CMPM, ~(EVT_CM_PMNVM_BAD | EVT_CM_PMNVM_GOOD), OS_AND );        
        return( OK );
    }

    // Verify response is the correct type.
    if( rMsgP->bMsgType != LONG_DATA )
    {
        // Response format is incorrect. 
        dbgTrace(DBG_LVL_ERROR,"CM Received bad PmNvmDataResponse Type =%d.\n", rMsgP->bMsgType  );
        bPmNvmAccessState = CM_PM_NVM_ACC_DONE;
        // Set Bad event.
        OSSetEvents( EVENT_GROUP_CMPM, EVT_CM_PMNVM_BAD, OS_OR );        
        return( OK );
    }
    // Get ptr to Response data, and copy the status.
    pPmRsp = (GEN_PM_RESPONSE *)&(rMsgP->IntertaskUnion.lwLongData);
    pNvmAccPtr->lStatus = pPmRsp->lwStatus;

    // Test status:
    if( pPmRsp->lwStatus != PM_SUCCESS )
    {
        // Status is bad: 
        //  Copy error info from PM message to Client's Error Structure. 
        pNvmAccPtr->bCMErrorType = CM_PM_NVM_ERR_DURING_WRITE;
        pNvmAccPtr->lErrCode = pPmRsp->lwErrorCode;
        //   Set internal state to done. Set Error Event.
        bPmNvmAccessState = CM_PM_NVM_ACC_DONE;  
        OSSetEvents( EVENT_GROUP_CMPM, EVT_CM_PMNVM_BAD, OS_OR );        
    }
    else
    {
        // Data has been updated, call appropriate function to finish.
        dbgTrace(DBG_LVL_INFO,"CM: PmNvmUpdateRsp Success.\n" );
        if( pNvmCallbackFn == NULL_PTR )
        {
            dbgTrace(DBG_LVL_INFO,"CM: Missing CallbackFunction.\n" );
        	pNvmAccPtr->bCMErrorType = CM_PM_NVM_ERR_DURING_WRITE;
        	pNvmAccPtr->lErrCode = 0xFFFFFF;
        	OSSetEvents( EVENT_GROUP_CMPM, EVT_CM_PMNVM_BAD, OS_OR );        
        }
        else
        {
            pNvmCallbackFn();
        }
    }

    return OK;
}

/**********************************************************************
* FUNCTION NAME:                        fnCallbackPmNvmSuccess
* DESCRIPTION:   
*   When a successful repsonse means that the service is finished.
*   Set internal state to done, reset callback function pointer and 
*   Set the GOOD event flag.  
* INPUTS:   None          
*
* RETURNS:  None
*
*
* NOTES:
**********************************************************************/
void fnCallbackPmNvmSuccess( void )
{
    // Set internal state to done. Set Success Event.
    bPmNvmAccessState = CM_PM_NVM_ACC_DONE;  
    pNvmCallbackFn = NULL_PTR;    
    OSSetEvents( EVENT_GROUP_CMPM, EVT_CM_PMNVM_GOOD, OS_OR );
    return;      
}

// This handles the Intertask message to read the NVM.
/**********************************************************************
* FUNCTION NAME:                        fnCheckStateForNvmAccessReq
* DESCRIPTION:   
*   This checks that the CM task is in an ok state to access the NVM data
*   (not printing?), and then sets bPmNvmAccessState and contents of 
*   pNvmAccPtr. 
* INPUTS:             
*
* RETURNS:  TRUE if the state is ok for NVM access, else FALSE.
*
*
* NOTES:
**********************************************************************/
BOOL fnCheckStateForNvmAccessReq( INTERTASK           *rMsgP  ) // (I) Received msg 
{
    // Get pointer to the client's Data structure.
    pNvmAccPtr = (tPmNvmAccStatus *)(rMsgP->IntertaskUnion.PointerData.pData);
    
    // Check CM current state to make sure we are idle or error state. 
    switch( cmState.eCurrentState )
    {
        // Idle and error states are valid states to perform this in.
        // Added init, because the first request for NVM seems to come during init.
        case s2_Init:
        case s4_NmlIdle: 
        case s5_SlOnlyIdle:      
        case s6_DiagIdle:        
        case s7_DisableIdle:     
        case s8_Sleep:           
        case s22_Idle:           
        case s39_PprErr:        
        case s40_PrtJamErr :     
        case s41_FdrErr:        
        case s42_EjtErr:        
        case s43_StgErr:        
        case s44_PrtErr:        
        case s45_SlRunErr:      
        case s46_HdInkErr:      
        case s47_MsgErr:        
        case s48_IntErr:        
        case s49_FtlErr:        
        case s50_TapeErr: 
            break;      

        default: 
            // If not in elligible state, set error type, set status to OK,  
            //  set ErrCode to the current state, and return FALSE.
            pNvmAccPtr->bCMErrorType = CM_PM_NVM_ERR_CM_INV_STATE;
            pNvmAccPtr->lStatus = 0;
            pNvmAccPtr->lErrCode = (UINT32)cmState.eCurrentState;
            pNvmAccPtr->pDataCopy = NULL_PTR;
            bPmNvmAccessState = CM_PM_NVM_ACC_DONE; 
            return( FALSE );        
    }

    // Set the status default to success. 
    pNvmAccPtr->bCMErrorType = CM_PM_NVM_SUCCESS;
    pNvmAccPtr->lErrCode = 0;
    pNvmAccPtr->lStatus = 0;

    bPmNvmAccessState = CM_PM_NVM_ACC_READ_PND; 
    return( TRUE );
}


/**********************************************************************
* FUNCTION NAME:                        fnPrcsPmNvmReadReq
* DESCRIPTION:   
*   This handles the Intertask message to read the NVM. 
* INPUTS:             
*
* RETURNS:  
*
*
* NOTES:
**********************************************************************/
int fnPrcsPmNvmReadReq(  
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    BOOL    fGoAhead;

    // Don't change the state based on this event.
    *outputEvtP = v0_Invalid;

    // Check state, set default or error return data, set Access state.
    fGoAhead = fnCheckStateForNvmAccessReq( rMsgP );

    
    if( fGoAhead == FALSE )
    {
        // The check function filled in the return error data and cleared the DCOND.
        OSSetEvents( EVENT_GROUP_CMPM, EVT_CM_PMNVM_BAD, OS_OR );
    }
    else 
    {
        // Allow client to supply RAM or not. 
        if( pNvmAccPtr->pDataCopy == NULL_PTR)
             pNvmAccPtr->pDataCopy = &sNvmCopy;  

        // When the Read Reply is successful, the access is complete.
        pNvmCallbackFn = fnCallbackPmNvmSuccess;    
        OSSendIntertask( BJCTRL, CM, CP_GET_PM_NVM_DATA, GLOBAL_PTR_DATA, pNvmAccPtr->pDataCopy, sizeof(NVRAM_DATA));
    }
    return( OK );
}


/**********************************************************************
* FUNCTION NAME:                        fnCallbackClearWasteTankCounts
* DESCRIPTION:   
*       When the client wants to clear the waste tank counts, this is called  
*        after the NVM Read succeeds. 
*
* INPUTS:             
*
* RETURNS:  
*
* NOTES:
**********************************************************************/
void fnCallbackClearWasteTankCounts( void )
{
    // Clear the waste tank counts,
    sNvmCopy.wast_ink_count_g = 0; 
    sNvmCopy.wast_ink_count_mg = 0; 
    sNvmCopy.wast_ink_count_ug = 0; 
    sNvmCopy.wast_ink_count_ng = 0; 

    // When write is successful, we are done.
    pNvmCallbackFn = fnCallbackPmNvmSuccess;
  
    // Clearing the waste counts should clear the Waste Tank Error Status 
    //  saved when the PM last sent an error message.
    ucWasteInkTankStatus = 0;
    if( pNvmAccPtr->pDataCopy == NULL_PTR )
    {
        // IF the pointer is bad, set the BAD event, and we are done.
        pNvmAccPtr->bCMErrorType = CM_PM_NVM_ERR_BAD_PTR;
        pNvmAccPtr->lStatus = 0;
        pNvmAccPtr->lErrCode = 0;
        pNvmCallbackFn = NULL_PTR;
        bPmNvmAccessState = CM_PM_NVM_ACC_DONE; 
        OSSetEvents( EVENT_GROUP_CMPM, EVT_CM_PMNVM_BAD, OS_OR );
    }
    else
    {
        // Set internal state to Write Pending. Send msg to PM.
        bPmNvmAccessState = CM_PM_NVM_ACC_WRITE_PND;
        OSSendIntertask( BJCTRL, CM, CP_PM_NVM_UPDATE, GLOBAL_PTR_DATA, &sNvmCopy, sizeof(NVRAM_DATA));
    }
    return;      
} 


/********************************************************************************/
// TITLE:                       fnPrcsClearWasteCounts                                              
// DESCRIPTION:     
//      Clear the Canon waste ink tank counts.
//      This is done by requesting a copy of the NVM data, overwriting the 4
//      long counts with 0, and then sending an update request to the PM.
//
// INPUT:  Standard CM message handler inputs: 
//      Ptr to ITMsg and Ptr to location to store the new event.
//      The message should contain a pointer to the client's tPmNvmAccStatus 
//      structure, where the result status will be saved.  
//                                                                 
// OUTPUT:  OK 
// NOTES: 
// 1. Since the first client that needs this is actually the WSRXSRVTASK (sending 
//    the ITM as if it came from the OIT), we cannot return a response.  So 
//    the System Events are used to signal the client that the process has finished.
// 2. The fnCheckStateForNvmAccessReq() gets the ptr to the clients Access structure.
// HISTORY: 
// 2018.01.29 CWBellamy - New 
/********************************************************************************/
int fnPrcsClearWasteCounts(  
    INTERTASK           *rMsgP,         // (I) Received msg 
    emCmStateEvent      *outputEvtP)    // (O) Embedded event in the msg
{
    BOOL    fGoAhead;

    // Don't change the state based on this event.
    *outputEvtP = v0_Invalid;

    // Check state, set default or error return data, set Access state.
    fGoAhead = fnCheckStateForNvmAccessReq( rMsgP );
    
    if( fGoAhead == FALSE )
    {
        // The check function filled in the return error data.
        OSSetEvents( EVENT_GROUP_CMPM, EVT_CM_PMNVM_BAD, OS_OR );
    }
    else
    {
        // Always use internal RAM. Write callback function writes it directly. 
        pNvmAccPtr->pDataCopy = &sNvmCopy;  

        // When the Read Reply is successful, the access is complete.
        pNvmCallbackFn = fnCallbackClearWasteTankCounts;    
        OSSendIntertask( BJCTRL, CM, CP_GET_PM_NVM_DATA, GLOBAL_PTR_DATA, pNvmAccPtr->pDataCopy, sizeof(NVRAM_DATA));
    }
    return( OK );
}

