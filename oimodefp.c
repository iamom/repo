/************************************************************************
	PROJECT:		Future Phoenix
	COPYRIGHT:		2005, Pitney Bowes, Inc.
	AUTHOR:			
	MODULE NMAE:	OiModefp.c

	DESCRIPTION:	This file contains OI mode tables for FP.

 	MODIFICATION HISTORY:
    05/11/2006  Steve Terebesi  Removed old structures and functions
	12/27/2005	Vincent Yi		Initial version

	OLD PVCS REVISION HISTORY
 * \main\jnus11.00_i1_shelton\4 cx17598 12 oct 2005
 *
 *	Put comments for previous check in in.
 *

*************************************************************************/


/**********************************************************************
		Include Header Files
**********************************************************************/
#include "global.h"
#include "oitpriv.h"
#include "oit.h"
#include "oifunctfp.h"



/**********************************************************************
		Local Function Prototypes
**********************************************************************/



/**********************************************************************
		Local Defines, Typedefs, and Variables
**********************************************************************/



/**********************************************************************
		Global Functions Prototypes
**********************************************************************/



/**********************************************************************
		Global Variables
**********************************************************************/
/*------------------ MODE TABLES FOR SPARK 2 -----------------------*/
/* Screen-to-mode map:  This table maps a screen to a mode/submode.  If we attempt to transition
	to a screen that is associated with a mode other than the current mode, permission is requested
	from the system controller before the transition is allowed.  All screens that can be reached
	(through user operations) from a screen of a different mode should appear in this table.  */
//const SCREEN_MODE_TABLE pScreenModeMap[] =
//{
//		{ mPowerOn,							OIT_POWERUP,					OIT_NO_SUBMODE },
//	  	{ mErrInitializingFailed,		 	OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//
//	  	{ mMMEnterPostage, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//
//	  	{ mMMIndiciaReady, 					OIT_PRINT_READY,				OIT_INDICIA },
//	  	{ mMMIndiciaReadyTransient,			OIT_PRINT_READY,				OIT_INDICIA },
//	  	{ mMMIndiciaPrinting, 				OIT_RUNNING,					OIT_INDICIA },
//	    { mMMIndiciaTapePrinting, 			OIT_PRINTING_TAPE, 				OIT_INDICIA },								
//	    { mMMIndiciaError,			 		OIT_PRINT_NOT_READY, 			OIT_NO_SUBMODE },							
//	  	{ mMMIndiciaTransient,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mMMFeesDetails,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	
//	  	{ mMMSealOnlyReady,					OIT_PRINT_READY,				OIT_SEAL_ONLY },
//	  	{ mMMSealOnlyReadyTransient,		OIT_PRINT_READY,				OIT_SEAL_ONLY },
//	    { mMMSealOnlyError,			 		OIT_PRINT_NOT_READY, 			OIT_NO_SUBMODE },							
//	  	{ mMMSealOnlyRunning, 				OIT_RUNNING,					OIT_SEAL_ONLY },
//
//	  	{ mMMDateTimeOnlyReady,				OIT_PRINT_READY,				OIT_INDICIA },
//	    { mMMDateTimeOnlyError,			 	OIT_PRINT_NOT_READY, 		    OIT_NO_SUBMODE },	
//	  	{ mMMDateTimeOnlyPrinting, 			OIT_RUNNING,					OIT_INDICIA },
//        { mMMDateTimeOnlyTapePrinting, 		OIT_PRINTING_TAPE, 				OIT_INDICIA },
//
//		{ mMMAdOnlyReady,					OIT_PRINT_READY,				OIT_INDICIA },
//	    { mMMAdOnlyError,			 		OIT_PRINT_NOT_READY, 		    OIT_NO_SUBMODE },
//		{ mMMAdOnlyPrinting,				OIT_RUNNING,					OIT_INDICIA },
//	  	{ mMMAdOnlyTapePrinting, 		    OIT_PRINTING_TAPE, 				OIT_INDICIA },
//	  	
//	  	{ mMMPermitReadyTransient,			OIT_PRINT_READY,				OIT_PERMIT },
//
//		{ mReportsMenu,						OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },	
//		{ mReportPHPrintReady, 				OIT_PRINT_READY,				OIT_REPORT },
//		{ mReportPHPrinting,				OIT_RUNNING,					OIT_REPORT },	
//		{ mReportPHTapePrinting,			OIT_PRINTING_TAPE,				OIT_REPORT },	
//		{ mReportPHPrintError,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },	
//		{ mReportPHPrintComplete,			OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },	
//	  	{ mReportPrintReadyTransient,		OIT_PRINT_READY,				OIT_REPORT },
//
//	  	{ mPmcDiagPrintTestPat,				OIT_PRINT_READY,		 		OIT_TEST_PATTERN },
//	  	{ mPmcDiagTestPatReadyTransient,	OIT_PRINT_READY,				OIT_TEST_PATTERN },
//	  	{ mPmcDiagPrintTestPatError,		OIT_PRINT_NOT_READY,		 	OIT_NO_SUBMODE },
//	  	{ mPmcDiagTestPatPrinting,			OIT_RUNNING,			 		OIT_TEST_PATTERN },
//	  	{ mPmcDiagTestPatTapePrinting,		OIT_PRINTING_TAPE,		 		OIT_TEST_PATTERN },
//	  	{ mTestPrintPatAcceptable,			OIT_PRINT_NOT_READY,		 	OIT_NO_SUBMODE },
//		
//	  	{ mAdSelection,						OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },	  	
//	  	{ mInscSelection,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },	  	
//	  	{ mPresetSelectCustomPreset,		OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mPresetNoCustomPresets,		OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mPresetInvalidSettings,		OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrFeederJam,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },	  	
//	  	{ mErrFeederNoPaper,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrHighPostageExceeded,			OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },	  	
//	  	{ mErrInkOut,						OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrInkTank,						OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrInkTankLidOpen,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrInsufficientFunds,			OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrInvalidPostage,		 		OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mErrJamLeverOpen,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrMeterError,					OIT_METER_ERROR,				OIT_NO_SUBMODE },
//	  	{ mErrMissingImage,					OIT_METER_ERROR,				OIT_NO_SUBMODE },  	
//	  	{ mErrMeterErrorPowerOff,			OIT_METER_ERROR,				OIT_NO_SUBMODE },
//	  	{ mErrMeterUnauthorized,			OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrMissingGraphic,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrNoInkTank,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrNoPrinthead,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrPaperErrors,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrPrinterFault,					OIT_METER_ERROR,				OIT_NO_SUBMODE },
//	  	{ mErrPrinthead,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrPSDBatteryLow, 				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//		{ mErrTapeErrors,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },	
//	  	{ mErrTopCoverOpen,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrWasteTankFull,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mInspectionRequired, 				OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mInspectionWarning, 				OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//		{ mManufacturingPSDDisabled,		OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//		{ mManufacturingPSDMisMatch,		OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	
//		{ mLockEnterLockCode, 				OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//		{ mLockEnterPassword, 				OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//
//	  	{ mSleep,		 					OIT_SLEEP,						OIT_NORMAL_SLEEP },
//
//	  	{ mFunds,		 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mMenuMainMenu, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mMenuPrintingMode,		 		OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mPrinterMaint,			 		OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mSetupMultipleTapes,		 		OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//
//	  	{ mRefill, 							OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mRefillEnterAmount, 				OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mRefillSuccessful, 				OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mInfraContactPB, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//
//	  	{ mWeightRateOptionMain,			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mRateErrNegativeWeight,			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mRateErrWeightOverCapacity,		OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//        { mRateSelectClass, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mRateServices, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },	  	
//	  	{ mRatePromptZipZone, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//        { mRateCountrySelection, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	  	  	  	
//	  	{ mTextSelectTextMessage,		 	OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//
//		{ mAcctSelection,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//                { mRateErrWeightOverCapacityDiffWeight,		OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mRateDiffWeightZero, 				OIT_PRINT_READY,				OIT_INDICIA },
//	  	{ mRateDiffRemoveItem, 				OIT_PRINT_READY,				OIT_INDICIA },
//	  	{ mRateDiffLastItem, 				OIT_PRINT_READY,				OIT_INDICIA },
//#if 0
//	  	{ mMMIndiciaReadyZeroWeight,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//		{ mMMIndiciaAddPostageReady,				OIT_PRINT_READY,			OIT_INDICIA },
//		{ mMMIndiciaAddPostagePrinting,				OIT_RUNNING,				OIT_INDICIA },
//		{ mMMIndiciaCorrectedDateReady,				OIT_PRINT_READY,			OIT_INDICIA },
//		{ mMMIndiciaCorrectedDatePrinting,			OIT_RUNNING,				OIT_INDICIA },
//		{ mMMIndiciaTimeStampReady,				OIT_PRINT_READY,			OIT_INDICIA },
//		{ mMMIndiciaTimeStampPrinting,				OIT_RUNNING,				OIT_INDICIA },
//		{ mMMIndiciaTimeStampTransient,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//		{ mMMIndiciaAdTimeStampReady,				OIT_PRINT_READY,			OIT_INDICIA },
//		{ mMMIndiciaAdTimeStampPrinting,			OIT_RUNNING,				OIT_INDICIA },
//		{ mMMIndiciaAdTimeStampNeedsAd,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//		{ mMMIndiciaAdTimeStampTransient,			OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//		{ mMMPermitReady,					OIT_PRINT_READY,			OIT_PERMIT },
//		{ mMMPermitPrinting,					OIT_RUNNING,				OIT_PERMIT },
//		{ mMMPermitReadyNeedsAcct,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mLockMeterLocked, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mLockPrompt, 						OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mLockCodeDisabled, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mPBPDcapBackup, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mPBPDcapUploadDue, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mPBPDcapUploadReq, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mPBPDcapUpdateReq, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mMeterOutOfService, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//		{ mManufacturingMode,					OIT_MANUFACTURING,			OIT_NO_SUBMODE },
//	  	{ mDownloadMain,		 			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mAcctMainMenu,		 			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mAdMenu,		 				OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mInscMenu,		 				OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mPrintOptionAddPostage,		 		OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mPrintOptionCorrectDate,		 		OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mPrintOptionHelpText,		 			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mServModePassword, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mServModeMain,					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mServModePmcDiagSelectTestPat,			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//
//	  	{ mOOBTestPrintAcceptable,				OIT_PRINT_NOT_READY,		 	OIT_NO_SUBMODE },
//	  	{ mRefill2CreditMeter,				OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },	  	
//	  	{ mErrPaperInTransport,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrPSOCError,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrPieceCountEOL,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrPieceCountLowWarning,			OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },  	
//	  	{ mErrKIPNotAllowed,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//		{ mErrBobPrintAreaExceeds,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//		{ mErrBobPrintAreaExceeds2,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mNotImplemented,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mRateEnterWeight, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mRateSelectClass, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mRateServices, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },	  	
//	  	{ mRatePromptZipZone, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mRateInternational, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },	  	
//	  	{ mOptionSetScaleCode, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mPresetMainMenu, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mScaleOptions, 					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mEServiceUploadConfirm, 				OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mOOBInstallNotComplete, 				OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mErrIncidentReportPrompt, 				OIT_PRINT_READY,		 	OIT_REPORT },
//	  	{ mScaleMeasuredWeightPositive, 			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mScaleMeasuredWeightZero, 				OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mScaleErrNegativeWeight,				OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mMMIndiciaEServiceReady, 				OIT_PRINT_READY,	 		OIT_INDICIA },
//	  	{ mEServiceAnotherItem,					OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mLockSleep,		 				OIT_SLEEP,				OIT_NORMAL_SLEEP },
//	  	{ mPowerDownPrompt,	 				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mPowerDown,		 				OIT_SLEEP,				OIT_SOFT_POWER_DOWN },
//		{ mPermitZeroCount,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//		{ mPermitReport,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//		{ mPermitFunds,						OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//		
//	  	{ mWgtDiffApplyLabel,		 			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mWgtDiffLastItem,		 			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mWgtDiffReplaceMail,		 			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mMMDiffNeedsAcct,		 			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE }, 	
//	  	{ mMMDiffNeedsClass,		 			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE }, 	
//	  	{ mMMDiffRemoveOnePiece,		 		OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE }, 	
//	  	{ mWgtDiffMailPrompt,		 			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE }, 	
//	  	{ mRateSelectCarrier,		 			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mRateApplyLabel,		 			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mRatePromptBarcode,		 			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mTextMenu,		 				OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mRateErrNoCarrierClassAvailable,		 	OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mRateErrPostageTooHigh,		 		OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mRateErrRMNotInitialized,		 		OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mLockEnterActPwd,		 			OIT_PRINT_NOT_READY,	 		OIT_NO_SUBMODE },
//	  	{ mKIPCheckPassword, 					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mFranceDateAdvancedPrompt, 				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mDaylightSavingTimeOccur, 				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrAppCheckSum, 					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mErrTimeSyncErr, 					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mWrnTimeSyncNegativeLockout, 				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mWrnInscOverride, 					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },
//	  	{ mGermanErrTaskEKPRequired,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },	  	
//	  	{ mPBPMeterMoveComplete,				OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },	  	
//	  	{ mPBPMeterMoveFailed,					OIT_PRINT_NOT_READY,			OIT_NO_SUBMODE },	  	
//#endif // #if 0
//		{ END_OF_SCREEN_MODE_TABLE        , 0                         , 0 			} 
//	  };


/* Mode-to-screen map:  This table attaches a screen ID to a mode/submode.  It indicates that the
	user interface should transition to the given screen ID# when a change mode command is received
	from the system controller.   There is one line in the table for every possible mode/submode
	combination.  The screen ID that is associated with a mode/submode can change or be set
	dynamically as the system runs, therefore all of the variables are not initialized here. */
//SCREEN_MODE_TABLE		pModeNextScreen[] =
//	{
//		{ 0, 								OIT_POWERUP, 				OIT_NO_SUBMODE },
//		{ 0, 								OIT_PRINT_NOT_READY, 	 	OIT_NO_SUBMODE },
//
//		{ 0, 								OIT_PRINT_READY,	 		OIT_INDICIA },
//		{ 0, 								OIT_PRINT_READY,	 		OIT_PERMIT },
//		{ 0,				 				OIT_PRINT_READY, 			OIT_SEAL_ONLY },
//		{ 0, 								OIT_PRINT_READY, 			OIT_TEST_PATTERN },
//		{ 0, 								OIT_PRINT_READY, 			OIT_REPORT },
//
//		{ mMMIndiciaPrinting, 				OIT_RUNNING, 	 			OIT_INDICIA },
//	    { mMMIndiciaTapePrinting, 			OIT_PRINTING_TAPE, 			OIT_INDICIA },								
//
//		{ mMMSealOnlyRunning, 				OIT_RUNNING, 	 			OIT_SEAL_ONLY },
//
//		{ mPmcDiagTestPatPrinting,			OIT_RUNNING, 	 			OIT_TEST_PATTERN },
//		{ mPmcDiagTestPatTapePrinting,		OIT_PRINTING_TAPE, 			OIT_TEST_PATTERN },
//
//		{ mReportPHPrinting, 				OIT_RUNNING, 	 			OIT_REPORT },
//		{ mReportPHTapePrinting, 			OIT_PRINTING_TAPE, 			OIT_REPORT },
//
//		{ mErrMeterError,					OIT_METER_ERROR, 			OIT_NO_SUBMODE },
//		{ 0, 								OIT_SLEEP,	 	 			OIT_NORMAL_SLEEP },
//		{ 0, 								OIT_SLEEP,	 	 			OIT_SOFT_POWER_DOWN },
//
//#if 0
//		{ mMMPermitPrinting, 				OIT_RUNNING, 	 			OIT_PERMIT },
//		{ mMMIndiciaEServicePrinting, 			OIT_RUNNING, 	 		OIT_INDICIA },
//		{ 0, 							OIT_MAINTENANCE, 		OIT_NO_SUBMODE },
//		{ mErrMissingImage,					OIT_METER_ERROR, 		OIT_NO_SUBMODE },		
//		{ mManufacturingMode,					OIT_MANUFACTURING,		OIT_NO_SUBMODE },		
//#endif // #if 0
//		{ END_OF_SCREEN_MODE_TABLE, 0, 0 } 
//	};


//const SCREEN_MODE_ADJ_TABLE	pModeAdjTable[] =
///* when we go to this screen...,		... map this mode/submode combo,	  			... to this screen */
//{
//	{ mMMIndiciaReady,					OIT_RUNNING,			OIT_INDICIA,			mMMIndiciaPrinting	},
//	{ mMMIndiciaReady,					OIT_PRINTING_TAPE,		OIT_INDICIA,			mMMIndiciaTapePrinting},
//
//	{ mMMAdOnlyReady,					OIT_RUNNING,			OIT_INDICIA,			mMMAdOnlyPrinting 	},
//    { mMMAdOnlyReady,					OIT_PRINTING_TAPE,		OIT_INDICIA,			mMMAdOnlyTapePrinting},
//	{ mMMDateTimeOnlyReady,				OIT_RUNNING,			OIT_INDICIA,			mMMDateTimeOnlyPrinting },
//    { mMMDateTimeOnlyReady,				OIT_PRINTING_TAPE,		OIT_INDICIA,			mMMDateTimeOnlyTapePrinting},
//
//#if 0
//#if 0
//	{ mInkInsertEnvelopeForTest,		OIT_PRINTING_ENVELOPE,		OIT_NO_SUBMODE,		mInkPerformingPrintHeadTest	},
//	{ mOOBPromptTestPrint,			OIT_PRINTING_ENVELOPE,		OIT_NO_SUBMODE,		mOOBPrintingTestPattern },
//	{ mMMIndiciaReadyLowFunds,		OIT_PRINTING_ENVELOPE,		OIT_NO_SUBMODE,		mMMIndiciaPrintingLowFunds },
//	{ mMMIndiciaReadyUSPS,			OIT_PRINTING_ENVELOPE,		OIT_NO_SUBMODE,		mMMIndiciaPrintingUSPS },
//	{ mMMSealOnlyReady,			OIT_PRINTING_ENVELOPE,		OIT_NO_SUBMODE,		mMMSealOnlyPrinting },
//#endif
//	{ mMMIndiciaEServiceReady,		OIT_RUNNING,			OIT_INDICIA,		mMMIndiciaEServicePrinting	},	
//	{ mMMIndiciaAddPostageReady,		OIT_RUNNING,			OIT_INDICIA,		mMMIndiciaAddPostagePrinting },
//	{ mMMIndiciaCorrectedDateReady,		OIT_RUNNING,			OIT_INDICIA,		mMMIndiciaCorrectedDatePrinting },
//	{ mMMIndiciaTimeStampReady,		OIT_RUNNING,			OIT_INDICIA,		mMMIndiciaTimeStampPrinting },
//	{ mMMIndiciaAdTimeStampReady,		OIT_RUNNING,			OIT_INDICIA,		mMMIndiciaAdTimeStampPrinting },
//#endif
//	{ END_OF_SCREEN_MODE_TABLE, 0, 0, 0 } 
//};

/* *************************************************************************
// FUNCTION NAME: 
//		fnGetScreenModeById
//
// DESCRIPTION: 
// 		Retrieve the mode and the submode in mode table by input screen ID.
//
// INPUTS:
//		usScreenID	- screen ID
//
// OUTPUTS:
//		pMode		- mode of the screen
//		pSubMode	- submode of the screen
//
// RETURNS:
//		Found or not
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	02/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
//BOOL fnGetScreenModeById (T_SCREEN_ID usScreenID, UINT8 * pMode, UINT8 *pSubMode)
//{
//	BOOL	fFound = FAILURE;
//	UINT16	usIndex = 0;
//
//	while (pScreenModeMap[usIndex].wScreenID != END_OF_SCREEN_MODE_TABLE)
//	{
//		if (pScreenModeMap[usIndex].wScreenID == usScreenID)
//		{
//			*pMode = pScreenModeMap[usIndex].bMode;
//			*pSubMode = pScreenModeMap[usIndex].bSubMode;
//
//			fFound = SUCCESSFUL;
//			break;
//		}
//		usIndex++;
//	}
//	
//	return fFound;
//}
