/*
 * settings manager
 *
 */

#include "nucleus.h"
#include "kernel/nu_kernel.h"
#include "commontypes.h"
#include "pbos.h"
#include "bobutils.h"
#include "pbtarget.h"
#include "wsox_tx_queue.h"
#include "debugtask.h"
#include "api_utils.h"

#define __declare_settings_mgr_variables__
#include "settingsmgr.h"
#undef __declare_settings_mgr_variables__


#ifndef CRLF
#define CRLF "\r\n"
#endif

typedef struct
{
    uint32_t     reader_count;				// protected by SET_MGR_RD_SEM_ID semaphore

} settings_t;


/* local prototypes - returned values are defined in osmsg.h */
static char smgr_increment_reader_count();
static char smgr_decrement_reader_count();
static char smgr_obtain_write_lock();
static char smgr_release_write_lock();


settings_t m_settings;
settings_in_ram_t m_settings_in_ram;
bool m_is_set[NUM_OF_SETTING_ITEMS];

// Error object for errors accumulated during input validation
cJSON *settingsMgrErrorItem = NULL;

/* -----------------------------------------------------------------------------------[smgr_increment_reader_count]-- */
static char smgr_increment_reader_count()
{
    char  semstat;

	semstat = OSAcquireSemaphore (SET_MGR_RD_SEM_ID, OS_SUSPEND);
    if (semstat != SUCCESSFUL)
    {
        dbgTrace(DBG_LVL_ERROR, "%s: failed to obtain SMGR_RD semaphore. error %d" CRLF, __FUNCTION__, semstat);
        return semstat;
    }

    if( ++m_settings.reader_count == 1)
    {
        // take the writer_semaphore to make sure a writer has to wait until all readers are done
    	semstat = smgr_obtain_write_lock();
        if (semstat != SUCCESSFUL)
        {
            dbgTrace(DBG_LVL_ERROR, "%s: failed to obtain SMGR_WR semaphore. error %d" CRLF, __FUNCTION__, semstat);
            return semstat;
        }
    }
    
    semstat = OSReleaseSemaphore(SET_MGR_RD_SEM_ID);
    if (semstat != SUCCESSFUL)
    {
        dbgTrace(DBG_LVL_ERROR, "%s: failed to release SMGR_RD semaphore. error %d" CRLF, __FUNCTION__, semstat);
    }

    return semstat;
}

/* -----------------------------------------------------------------------------------[smgr_decrement_reader_count]-- */
static char smgr_decrement_reader_count()
{
    char  semstat;

	semstat = OSAcquireSemaphore (SET_MGR_RD_SEM_ID, OS_SUSPEND);
    if (semstat != SUCCESSFUL)
    {
        dbgTrace(DBG_LVL_ERROR, "%s: failed to obtain SMGR_RD semaphore. error %d" CRLF, __FUNCTION__, semstat);
        return semstat;
    }

    if( --m_settings.reader_count == 0)
    {
        // release the writer_semaphore 
    	semstat = smgr_release_write_lock();
        if (semstat != SUCCESSFUL)
        {
            dbgTrace(DBG_LVL_ERROR, "%s: failed to release SMGR_WR semaphore. error %d" CRLF, __FUNCTION__, semstat);
            return semstat;
        }
    }
    
    semstat = OSReleaseSemaphore(SET_MGR_RD_SEM_ID);
    if (semstat != SUCCESSFUL)
    {
        dbgTrace(DBG_LVL_ERROR, "%s: failed to release SMGR_RD semaphore. error %d" CRLF, __FUNCTION__, semstat);
    }

    return semstat;
}


/* ----------------------------------------------------------------------------------------[smgr_obtain_write_lock]-- */
static char smgr_obtain_write_lock()
{
    // take the writer_semaphore 
    char  semstat;

	semstat = OSAcquireSemaphore (SET_MGR_WR_SEM_ID, OS_SUSPEND);
    if (semstat != SUCCESSFUL)
    {
        dbgTrace(DBG_LVL_ERROR, "%s: failed to obtain SMGR_WR semaphore. error %d" CRLF, __FUNCTION__, semstat);
    }
    return semstat;
}

/* ---------------------------------------------------------------------------------------[smgr_release_write_lock]-- */
static char smgr_release_write_lock()
{
    char  semstat;

    semstat = OSReleaseSemaphore(SET_MGR_WR_SEM_ID);
    if (semstat != SUCCESSFUL)
    {
        dbgTrace(DBG_LVL_ERROR, "%s: failed to release SMGR_WR semaphore. error %d" CRLF, __FUNCTION__, semstat);
    }

    return semstat;
}


#ifdef TEST_SETTINGS_MGR
void on_TestSettingsMgrReq(UINT32 handle, cJSON *root)
{

	cJSON *rspMsg = cJSON_CreateObject();

	WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "TestSettingsMgrRsp");

    smgr_set_values(root);

    cJSON_AddStringToObject(rspMsg, "Result", "Done");

    addEntryToTxQueue(&entry, root, "  on_TestSettingsMgrReq: Added TestSettingsMgrRsp to tx queue. status=%d" CRLF);
}
#endif /* TEST_SETTINGS_MGR */

/* -----------------------------------------------------------------------------------------------------[smgr_init]-- */
void smgr_init()
{

    (void) smgr_obtain_write_lock();

    m_settings.reader_count = 0;

    // Todo: initialize settings with defaults
    //

    memset(&m_settings_in_ram, 0, sizeof(m_settings_in_ram));
    memset(m_is_set, 0, sizeof(m_is_set));

    (void) smgr_release_write_lock();

}

/* -----------------------------------------------------------------------------------------------[smgr_invalidate]-- */
STATUS smgr_invalidate(setting_item_t item)
{
    STATUS status = NU_SUCCESS;
    char  semstat;
    
    if (item >= NUM_OF_SETTING_ITEMS)
    {
        status = NU_INVALID_ENTRY;  /* find/define better error code */
    }
    else
    {
    	semstat = smgr_obtain_write_lock();

        if (semstat == SUCCESSFUL)
        {
            m_is_set[item] = false;

            (void) smgr_release_write_lock();
        }
        else
        {
            status = NU_RETURN_ERROR;
        }
    }

    return status;
}

/* -------------------------------------------------------------------------------------------[smgr_invalidate_all]-- */
STATUS smgr_invalidate_all(void)
{
    STATUS status = NU_SUCCESS;
    char  semstat;
    
    semstat = smgr_obtain_write_lock();

    if (semstat == SUCCESSFUL)
    {
    	memset(&m_settings_in_ram, 0, sizeof(m_settings_in_ram));
        memset(m_is_set, 0, sizeof(m_is_set));
        (void) smgr_release_write_lock();
    }
    else
    {
        status = NU_RETURN_ERROR;
    }
    return status;
}

/* -----------------------------------------------------------------------------------------[smgr_get_setting_data]-- */
STATUS smgr_get_setting_data(setting_item_t item, void *dst, size_t size)
{
    STATUS status = NU_SUCCESS;
    char  semstat;

    if (dst == NULL)
    {
        status = NU_INVALID_POINTER;
    }
    else if (item >= NUM_OF_SETTING_ITEMS)
    {
        status = NU_INVALID_ENTRY;  /* find/define better error code */
    }
    else
    {
    	semstat = smgr_increment_reader_count();

        if (semstat == SUCCESSFUL)
        {

            if (m_is_set[item])
            {
                if (fn_legacy_getset_tbl[item] != NULL)
                {
                    status = fn_legacy_getset_tbl[item](FN_GET, dst, size);
                }
                else
                {
                    void *src = ((void *)&m_settings_in_ram) + settings_offset_tbl[item];
                    if ( (SIF_STR_32 == (item_flags[item] & SIF_STR_32)) ||
                    		(SIF_STR_64 == (item_flags[item] & SIF_STR_64)) ||
							(SIF_STR_256 == (item_flags[item] & SIF_STR_256)) )
                    {
                        strncpy(dst, src, size);
                    }
                    else
                    {
                        memcpy(dst, src, size);
                    }
                }
            }
            else
            {
                status = NU_NOT_PRESENT;    /* find/define better error code */
            }
            (void)smgr_decrement_reader_count();
        }
    }

    return status;
}

/* -----------------------------------------------------------------------------------------[smgr_set_setting_data]-- */
STATUS smgr_set_setting_data(setting_item_t item, void *src, size_t size)
{
    STATUS status = NU_SUCCESS;
    char  semstat;

    if (src == NULL)
    {
        status = NU_INVALID_POINTER;
    }
    else if (item >= NUM_OF_SETTING_ITEMS)
    {
        status = NU_INVALID_ENTRY;  /* find/define better error code */
    }
    else
    {
    	semstat = smgr_obtain_write_lock();

        if (semstat == SUCCESSFUL)
        {
            if (fn_legacy_getset_tbl[item] != NULL)
            {
                status = fn_legacy_getset_tbl[item](FN_SET, src, size);
            }
            else
            {
                void *dst = ((void *)&m_settings_in_ram) + settings_offset_tbl[item];
                if ((SIF_STR_32 == (item_flags[item] & SIF_STR_32)) ||
                		(SIF_STR_64 == (item_flags[item] & SIF_STR_64)) ||
						(SIF_STR_256 == (item_flags[item] & SIF_STR_256)) )
                {
                    strncpy(dst, src, size);
                }
                else
                {
                    memcpy(dst, src, size);
                }
            }
            m_is_set[item] = true;
            (void) smgr_release_write_lock();
        }
    }

    return status;
}

/* ---------------------------------------------------------------------------------------------[get_settings_enum]-- */
bool get_settings_enum(char *name, setting_item_t *p_settings_item)
{
    int idx = 0;

    if (name != NULL && *name != 0)
        for (idx=0; settings_json_fieldname[idx] != NULL; idx++)
        {
            if (NU_STRICMP(name, settings_json_fieldname[idx]) == 0)
            {
                *p_settings_item = idx;
                return true;
            }
        }

    return false;
}

/* --------------------------------------------------------------------------------------[smgr_alt_get_set_postage]-- */
STATUS smgr_alt_get_set_postage(bool get, void *val, size_t size)
{
    bool result;
    if (get)
        result = fnGetDebitValue(val);
    else
        result = fnSetDebitValue(val);

    if (result == false)
        return -1;
    else
        return NU_SUCCESS;
}

/* ---------------------------------------------------------------------------------------------------[walk_object]-- */
typedef STATUS (*fn_process_cJSON_item_t)(cJSON *item, void *p_context);

static STATUS walk_object(cJSON *item, fn_process_cJSON_item_t process_item, void *p_context)
{
	STATUS status = NU_SUCCESS;
	STATUS childStatus = NU_SUCCESS;

    if (!item) return status;

    status = process_item(item, p_context);

    cJSON *subitem = item->child;

    while(subitem)
    {
    	childStatus = walk_object(subitem, process_item, p_context);
        if (childStatus != NU_SUCCESS)
        {
        	status = childStatus; //inherit the child's status if failure
        }
        subitem = subitem->next;
    }

    return status;
}

/* --------------------------------------------------------------------------------------[fn_set_value_if_possible]-- */
// Note:  This only validates the type of the data is correct. It does not check for valid input values.
STATUS fn_set_value_if_possible(cJSON *item, void *p_context)
{
	STATUS status = NU_SUCCESS;
    setting_item_t setting_id;

    if (item == NULL) return status;

    /* check to see if item->name is a known settings name */
    if (get_settings_enum(item->string, &setting_id))
    {
        uint32_t sif_type = item_flags[setting_id] & SIF_TYPE_ENUM_MASK;
        switch(sif_type)
        {
            case SIF_STR_32:
                if (item->type == cJSON_String)
                {
                    status = smgr_set_setting_data(setting_id, item->valuestring, sizeof(string_32));
                	if (status != NU_SUCCESS)
                	{
                		cJSON_AddItemToArray(settingsMgrErrorItem, CreateStringItem(item->string, "Error setting value"));
                	}
                }
                else
                {
                    dbgTrace(DBG_LVL_ERROR, "%s: Expected a cJSON_Type %d (String) for %s but found a type %d" CRLF, __FUNCTION__, cJSON_String, item->string, item->type);
            		cJSON_AddItemToArray(settingsMgrErrorItem, CreateStringItem(item->string, "Value wrong type"));
            		status = NU_INVALID_ENTRY;
                }
                break;
            case SIF_STR_64:
                if (item->type == cJSON_String)
                {
                    status = smgr_set_setting_data(setting_id, item->valuestring, sizeof(string_64));
                	if (status != NU_SUCCESS)
                	{
                		cJSON_AddItemToArray(settingsMgrErrorItem, CreateStringItem(item->string, "Error setting value"));
                	}
                }
                else
                {
					dbgTrace(DBG_LVL_ERROR, "%s: Expected a cJSON_Type %d (String64) for %s but found a type %d" CRLF, __FUNCTION__, cJSON_String, item->string, item->type);
            		cJSON_AddItemToArray(settingsMgrErrorItem, CreateStringItem(item->string, "Value wrong type"));
            		status = NU_INVALID_ENTRY;
                }
				break;
            case SIF_STR_256:
				if (item->type == cJSON_String)
				{
					status = smgr_set_setting_data(setting_id, item->valuestring, sizeof(string_256));
					if (status != NU_SUCCESS)
					{
						cJSON_AddItemToArray(settingsMgrErrorItem, CreateStringItem(item->string, "Error setting value"));
					}
				}
				else
				{
					dbgTrace(DBG_LVL_ERROR, "%s: Expected a cJSON_Type %d (String256) for %s but found a type %d" CRLF, __FUNCTION__, cJSON_String, item->string, item->type);
					cJSON_AddItemToArray(settingsMgrErrorItem, CreateStringItem(item->string, "Value wrong type"));
					status = NU_INVALID_ENTRY;
				}
				break;
            case SIF_UINT16:
            case SIF_INT16:
                if (item->type == cJSON_Number)
                {
                    status = smgr_set_setting_data(setting_id, &item->valueint, sizeof(uint16_t));
                	if (status != NU_SUCCESS)
                	{
                		cJSON_AddItemToArray(settingsMgrErrorItem, CreateStringItem(item->string, "Error setting value"));
                	}
                }
                else
                {
                    dbgTrace(DBG_LVL_ERROR, "%s: Expected a cJSON_Type %d (Number) for %s but found a type %d" CRLF, __FUNCTION__, cJSON_Number, item->string, item->type);
            		cJSON_AddItemToArray(settingsMgrErrorItem, CreateStringItem(item->string, "Value wrong type"));
            		status = NU_INVALID_ENTRY;
                }
                break;

            case SIF_UINT32:
            case SIF_INT32:
                if (item->type == cJSON_Number)
                {
                    status = smgr_set_setting_data(setting_id, &item->valueint, sizeof(uint32_t));
                	if (status != NU_SUCCESS)
                	{
                		cJSON_AddItemToArray(settingsMgrErrorItem, CreateStringItem(item->string, "Error setting value"));
                	}
                }
                else
                {
                    dbgTrace(DBG_LVL_ERROR, "%s: Expected a cJSON_Type %d (Number) for %s but found a type %d" CRLF, __FUNCTION__, cJSON_Number, item->string, item->type);
            		cJSON_AddItemToArray(settingsMgrErrorItem, CreateStringItem(item->string, "Value wrong type"));
            		status = NU_INVALID_ENTRY;
                }
                break;

            default:
                dbgTrace(DBG_LVL_ERROR, "%s: Unhandled SIF_ type %u for %s" CRLF, __FUNCTION__, sif_type, item->string);
        		cJSON_AddItemToArray(settingsMgrErrorItem, CreateStringItem(item->string, "Value invalid type"));
        		status = NU_INVALID_ENTRY;
                break;
        }
    }
    return status;
#if 0
    else
    {
        dbgTrace(DBG_LVL_ERROR, "%s: %s is not a known settings field" CRLF, __FUNCTION__, item->string);
    }
#endif
}

/* -----------------------------------------------------------------------------------------------[smgr_set_values]-- */
STATUS smgr_set_values(cJSON *msg)
{
	STATUS status = NU_SUCCESS;

    //create new JSON error object, old one should have been deallocated already; no need for sync
    settingsMgrErrorItem = cJSON_CreateArray();

    status = smgr_invalidate_all();
	if (status != NU_SUCCESS)
	{
		cJSON_AddItemToArray(settingsMgrErrorItem, CreateStringItem("Setting Values", "Internal Error"));
		return status;
	}

	status = walk_object(msg, fn_set_value_if_possible, NULL);

	// only set PROD ID if all settings were correct
    if ((status == NU_SUCCESS) && (m_is_set[SETTING_ITEM_PRODID]))
    {
    	char prod_id[32];
    	status = smgr_get_setting_data(SETTING_ITEM_PRODID, prod_id, settings_size_tbl[SETTING_ITEM_PRODID]);
    	if (status == NU_SUCCESS)
    	{
        	fnSetRateCategory( prod_id);
    	}
    	else
    	{
    		cJSON_AddItemToArray(settingsMgrErrorItem, CreateStringItem("Setting Product ID", "Internal Error"));
    	}
    }

    if (status == NU_SUCCESS)
    {// release array object if no errors to provide to caller
        cJSON_Delete(settingsMgrErrorItem);
    }
    return status;
}

/* ------------------------------------------------------------------------------------[smgr_add_set_fields_to_rsp]-- */
void smgr_add_set_fields_to_rsp(cJSON *rspMsg)
{
    int idx;
    uint32_t buffer[sizeof(string_256)/sizeof(uint32_t)]; // this has to be larger in bytes than the max size in settings_size_tbl[]

    for (idx=0; settings_json_fieldname[idx] != NULL; idx++)
    {
        if (m_is_set[idx])
        {
            uint32_t sif_type = item_flags[idx] & SIF_TYPE_ENUM_MASK;
            STATUS status = smgr_get_setting_data(idx, buffer, settings_size_tbl[idx]);

            if (status == NU_SUCCESS)
            {
                switch(sif_type)
                {
                    case SIF_STR_32:
                    case SIF_STR_64:
                    case SIF_STR_256:
                        cJSON_AddStringToObject(rspMsg, settings_json_fieldname[idx], buffer);
                        break;

                    case SIF_UINT16:
                        cJSON_AddNumberToObject(rspMsg, settings_json_fieldname[idx], *(uint16_t *)buffer);
                        break;

                    case SIF_INT16:
                        cJSON_AddNumberToObject(rspMsg, settings_json_fieldname[idx], *(int16_t *)buffer);
                        break;

                    case SIF_UINT32:
                        cJSON_AddNumberToObject(rspMsg, settings_json_fieldname[idx], *(uint32_t *)buffer);
                        break;

                    case SIF_INT32:
                        cJSON_AddNumberToObject(rspMsg, settings_json_fieldname[idx], *(int32_t *)buffer);
                        break;
                }
            }
        }
    }
}

/* --------------------------------------------------------------------------------------------[smgr_dump_settings]-- */
void smgr_dump_settings(void)
{
    int idx;
    uint32_t buffer[sizeof(string_256)/sizeof(uint32_t)]; // this has to be larger in bytes than the max size in settings_size_tbl[]

    debugDisplayOnly("  #  set name           value" CRLF);
    debugDisplayOnly("---- --- -------------- -------------------------------------------------" CRLF);
    for (idx=0; settings_json_fieldname[idx] != NULL; idx++)
    {
        debugDisplayOnly(" %2.2d   %c  %-14s ", idx, m_is_set[idx]?'Y':'n', settings_json_fieldname[idx]);
        if (m_is_set[idx])
        {
            uint32_t sif_type = item_flags[idx] & SIF_TYPE_ENUM_MASK;
            STATUS status = smgr_get_setting_data(idx, buffer, settings_size_tbl[idx]);

            if (status != NU_SUCCESS)
            {
                debugDisplayOnly("*** Error retrieving setting. Error=%d ***" CRLF, status);
            }
            else
            {
                switch(sif_type)
                {
                    case SIF_STR_32:
                    case SIF_STR_64:
                    case SIF_STR_256:
                        debugDisplayOnly("%s" CRLF, buffer);
                        break;

                    case SIF_UINT16:
                        debugDisplayOnly("%u" CRLF, *(uint16_t *)buffer);
                        break;

                    case SIF_INT16:
                        debugDisplayOnly("%d" CRLF, *(int16_t *)buffer);
                        break;

                    case SIF_UINT32:
                        debugDisplayOnly("%u" CRLF, *(uint32_t *)buffer);
                        break;

                    case SIF_INT32:
                        debugDisplayOnly("%d" CRLF, *(int32_t *)buffer);
                        break;
                }
            }
        }
        else
        {
            debugDisplayOnly(CRLF);
        }
    }
}


void smgr_get_settings_info(int idx, uint16_t *length, uint32_t *type, bool *is_set)
{

	*length = settings_size_tbl[idx];
	*type = item_flags[idx] & SIF_TYPE_ENUM_MASK;
	*is_set = m_is_set[idx];
}
/* -----------------------------------------------------------------------------------------------------------[EOF]-- */
