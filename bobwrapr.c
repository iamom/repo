/************************************************************************
*   PROJECT:        Horizon CSD
*   MODULE NAME:    bobwrapr.c
*   
*   DESCRIPTION:    Access to tealeavs
*                   
*
* ----------------------------------------------------------------------
*               Copyright (c) 2016 Pitney Bowes Inc.
*                    37 Executive Drive
*                   Danbury, Connecticut  06810
* ----------------------------------------------------------------------
* CHANGE HISTORY:
*
* 2018.08.07 CWB - Fixed Endianess when copying BobID of Device Status from Bobs
*					Error Directory, in the Redirected cases of fnTeaDistribReply().
*					Added debug info to view the status code in the future. 
* 2018.07.16 CWB - Got rid of some warnings by: Fixed a bad sprintf. Fixed 
*				some variable types. Removed a function that has been commented 
*				out for years. 
* 
*************************************************************************/

#include    <string.h>
#include    <stdio.h>       // sprintf()
#include    "bob.h"
#include    "bobutils.h"
#include    "global.h"
#include    "junior.h"
#include    "errcode.h"
#include    "cjunior.h"
#include    "ipsd.h"
#include    "pbos.h"
#include    "tealeavs.h"
#include	"flashutil.h"
#include	"utils.h"

//---------------------------------------------------------------------
//      Declaration of external variables:
//---------------------------------------------------------------------

extern  BOOL    getFlightRecord;

extern  uchar   tealeaves[];                            /* first item in tealeaves              */

extern  char   pBobsLogScratchPad[ 150 ];

//---------------------------------------------------------------------
//      Declaration of local variables:
//---------------------------------------------------------------------
static ushort   wRxGlobSize;


/*
    Used to access information in the Constants Section of tealeaves
*/
ushort  teaConstAccess[][2] = {
/*      offset  size                                                                                    */
    {    0,     TEA_HEADER_SIZ      },          /* component size and checksum                          */
    {    8,     TEA_OFFSET_SIZE     },          /* offset to component directory                        */
    {   12,     2                   },          /* records in message control table                     */  
    {   14,     2                   },          /* number of groups for categorizing headers            */
    {   16,     2                   },          /* number of groups used to categorize appended data    */
    {   18,     2                   },          /* number of groups used to categorize the replies      */
    {   20,     1                   },          /* number of records in the date/time dependence table  */
    {   21,     1                   },          /* number of records in the date/time exception table   */
    {   22,     1                   },          /* number of records in the system date/time table      */
    {   23,     1                   },          /* number of records in the printer date/time reference */
    {   24,     1                   },          /* number of settings in the timeout index              */
    {   25,     1                   },          /* number of groups to categorize errors                */
    {   26,     1                   },          /* number of groups for megabob's intertask replies     */
    {   27,     1                   },          /* number of devices handled by megabob                 */
    {   28,     2                   },          /* number of scripts supplied for megabob               */
    {   30,     2                   },          /* number of fake messages                              */
};

const ulong teaAddress = (ulong)(&tealeaves[0]);/* address of the start of tealeaves                    */


/* **********************************************************************

 FUNCTION NAME:     void fnTeaGetConstant(short constantID, void *constDest) 

 PURPOSE:           Returns a specified value from the constants section of tealeaves.
                    
 AUTHOR:            R. Arsenault

 INPUTS:            The passed argument constantID specifies the value to be retrieved.

                    Argument constDest points to the location at which the caller wants the
                    value to be copied.  Note that it is the responsibility of the caller to
                    ensure that the destination is large enough to store the copy.

                    Table teaConstAccess contains the offsets and sizes of variables that are
                    contained in the constants section of tealeaves.
 *************************************************************************/
void fnTeaGetConstant(short constantID, void *constDest) {
    char    *constPtr;

    constPtr = (char *)(teaAddress + teaConstAccess[constantID][0]);    /* calculate the actual address of the value    */
    EndianAwareCopy(constDest, constPtr, teaConstAccess[constantID][1]);         /* copy the value to the caller's address       */
    return;
}

/* **********************************************************************

 FUNCTION NAME:     BOOL fnTeaGetMsgControl(short bMsgId, struct msgControl *controlRec)

 PURPOSE:           Fills the msgControl record pointed to by passed argument controlRec
                    with the control record for the passed device message ID.

                    The control record information si retrieved from MsgControlTable, a table containing
                    a control record for each device message.
                    
 AUTHOR:            R. Arsenault

 INPUTS:            bMsgId is the device message ID
                    controlRec is a pointer to a control record
 *************************************************************************/
BOOL    fnTeaGetMsgControl( UINT16 bMsgId, struct msgControl *controlRec )
{
    uchar   *directory;
    uchar   *msgTable;
    BOOL    retval;
    ushort  maxMsgID;

    fnTeaGetConstant(MESSAGETABLESIZE, &maxMsgID);                                  // get the table size
    if (bMsgId >= maxMsgID) retval = FALSE;                                         // if the reqested message ID is not in the
    else {                                                                          // range of the table, return an error
        retval = TRUE;                                                              // 
        fnTeaGetConstant(TOTEAS, &directory);                                       // if it is in range, access the tealeavs directory
        directory += teaAddress;                                                    // 
        EndianAwareCopy( &msgTable,                                                          // get the offset to the Message Control Table
						directory  + ( MSGCONTROLTABLE * TEA_OFFSET_SIZE),                  // record that defines the requested message
						TEA_OFFSET_SIZE);                                                   // and convert it into a real address
        msgTable += teaAddress + (MSGCONTROLRECLEN * bMsgId);                       // 
                                                                                    // 
        memcpy(controlRec->flags, msgTable + MSGCON_FLAGS, 2);                      // fill the passed control record with the
        EndianAwareCopy(&controlRec->replyTaly, msgTable + MSGCON_REPLYTALY, 2);             // fields in the message control record
        EndianAwareCopy(&controlRec->headID, msgTable + MSGCON_HEADER, TEA_IDS_SIZE);        // 
        EndianAwareCopy(&controlRec->appDataID, msgTable + MSGCON_APPEND, TEA_IDS_SIZE);     // 
        EndianAwareCopy(&controlRec->replDataID, msgTable + MSGCON_REPLY, TEA_IDS_SIZE);     // 
        controlRec->device = msgTable[MSGCON_DEVID];                                // 
        controlRec->msgCase = msgTable[MSGCON_CASE];                                // 
        controlRec->timeoutIndex = msgTable[MSGCON_TIME_INDEX];                     // and return
        controlRec->retryCode = msgTable[MSGCON_RETRYCODE];
    }
    return(retval);
}


/* **********************************************************************

 FUNCTION NAME:     uchar fnTeaBuildMessage(struct msgInfo *messyInfo)

 PURPOSE:           
   Of course, this is the function that builds a message to be transmitted.
   Starting with the message information that is passed, it retrieves all of the
   tables and records that pertain to building the message that is specified in
   the passed message information.  It then builds the message, and fills the
   transfer control structure that will (eventually) passed to the driver that
   transmits the message.

 MODS:
  2009.03.12 Clarisa Bellamy - Before copying the header, make sure it has at
                    least 1 byte.  Remove code that is not applicable to Janus-based
                    products.  Change some local variable names to indicate purpose.
                    Update some comments.                    
 AUTHOR:            R. Arsenault
 *************************************************************************/
typedef ushort sizeGetter( UINT16 );
UINT16  bRNGCommand = 0;

uchar fnTeaBuildMessage( struct msgInfo *messyInfo ) //FIX ME JAH may need to fight more endians here
{
    struct      msgControl controlRec;      // record that cross-references a message's header, appended data, and other info
    uchar       *myDirectory;               // pointer to the tealeaves main directory
//    uchar       *myDeviceTable;             // pointer to the device table for the device that will receive this message
    uchar       *myTimeoutTable;            // pointer to the timeout settings cross-reference
    uchar       *myHeaderIndex;             // pointer to the header index
    uchar       *myAppdataIndex;            // pointer to the appended data index
    uchar       *myErrorIndex;              // pointer to the error index
    uchar       *headerRec;                 // pointer to the message's header
    uchar       *appendRec;                 // pointer to the message's appended data record
    uchar       *mySpecialLengths;          // pointer to the pointer to the special lengths table
    ushort      ashort;                     // used here and there for holding a short variable
    BOOL        myStatus;                   // returned status of called functions
    uchar       timeoutIndex;               // index (in the timeout table) to the timeout to use for this message
    const UINT8 *pubSource;                 // used here and there as a pointer to a character
    ushort      bMsgLen;                    // length of the message to be transmitted (head and data) 
    ushort      inputSize = 0;                  // the number of bytes to be appended to the header as appended data
    ushort      strucIndex;                 // used to maneuver through the special lengths table
    ushort      apprecSniffer;              // used to maneuver through the appended data definition record
    uchar       appendedItems;              // number of appended data fields
    sizeGetter  *spLenFunc;                 // pointer to a function that is sometimes used for determining the length of appended data
    ushort      expectedReply;              // set this if there is a reply expected
    long        timeValue;                  // actual timeout setting in milliseconds
    uchar       errDirRecords;              // number of error tables
    BOOL        foundFlag;                  // set when the appropriate error table is found
    struct      TransferControlStructure    *redirectStruc; // for acquiring appended data that is passed from another operating system task
    uchar       howToHandleMsg;             // this is what is returned to the caller... it specifies whether or not to transmit
    short       thisHereShort;              // for determine a calculated packet size
//    uchar       *ptrAddress;                // a pointer to variable header arguments

    myStatus = TRUE;
    howToHandleMsg = BOB_PROCESS_MSG;                                       // 
    fnTeaGetConstant(TOTEAS, &myDirectory);                                 // preset the ret
    myDirectory += teaAddress;                                              // 
                                                                            // 
//    myDeviceTable =     (uchar *)(fnTeaGetTable(DEVICETABLE));              // 
    myTimeoutTable =    (uchar *)(fnTeaGetTable(TIMEOUTLIST));              // 
                                                                            // 
    if( fnTeaGetMsgControl(messyInfo->msgID, &controlRec) ) 
    {
        myHeaderIndex =     (uchar *)(fnTeaGetTable(HEADDATAINDEX));        // 
        myAppdataIndex =    (uchar *)(fnTeaGetTable(APPENDDATAINDEX));      // 
        mySpecialLengths =  (fnTeaGetTable(SPECIALLENGTHS));                // 
        myErrorIndex =      fnTeaGetTable(BOBSERRDIRECTORY);                // 
        EndianAwareCopy( &headerRec,                                                 // 
						myHeaderIndex + (controlRec.headID * TEA_OFFSET_SIZE),      // 
						TEA_OFFSET_SIZE);                                           // 
        headerRec += teaAddress;                                            // 
                                                                            // 
        EndianAwareCopy( &appendRec,                                                 // 
						myAppdataIndex + (controlRec.appDataID * TEA_OFFSET_SIZE),  // 
						TEA_OFFSET_SIZE);                                           // 
        appendRec += teaAddress;                                            // 
                                                                            // 
        timeoutIndex = controlRec.timeoutIndex;
        // The orbit database has the indices as 1-based instead of 0-based.
        // So adjust the index by 1.
        if( timeoutIndex > 0 )  
            timeoutIndex -= 1;                             // 
        myTimeoutTable += (sizeof(long) * timeoutIndex);                    // 
        EndianAwareCopy(&timeValue, myTimeoutTable, sizeof(long));                   // 
                                                                            // 
        messyInfo->timeout = timeValue;                                     // 
        messyInfo->device = controlRec.device;                              // 
        // This copies the 2 flag bytes into messyInfo->flags[0,1].
        memcpy( &messyInfo->flags[0], &controlRec.flags[0], 2 );            //
        // This copies the 16-bit replyTaly into messyInfo->flags[2,3].
        EndianAwareCopy( &messyInfo->flags[2], &controlRec.replyTaly, 2 );           //
                                                                            // 
        foundFlag = FALSE;                                                  // 
        fnTeaGetConstant(ERRORGROUPCOUNT, &errDirRecords);                  // determine the number of error tables and the
        while( errDirRecords ) 
        {                                              // 
            if( myErrorIndex[0] == controlRec.device ) 
            {                     // 
                EndianAwareCopy(&ashort, &myErrorIndex[9], sizeof(short));  // 
                messyInfo->statusPlace = bobaVarAddresses[ashort].addr;     // 
                foundFlag = TRUE;                                           // 
                break;                                                      // 
            }                                                               // 
            errDirRecords--;                                                // 
            myErrorIndex += ERR_DIREC_REK_SIZ;                              // 
        }                                                                   // 
        if( !foundFlag )
        {
            messyInfo->statusPlace = vaultStatus;               // 
        }
                                                                            // 
        if( (controlRec.flags[0] & VAPEND) )
        {
            appendedItems = appendRec[0];   // 
        }
        else
        {
            appendedItems = 0;                                             // 
        }
          
        // !!!!!!!!!!!!! TEST CODE !!!!!!!!!!!!!!
        switch( messyInfo->msgID )
        {
            case IPSD_INITIALIZE:
            case IPSD_LOAD_POSTAL_CFG_DATA:
            case IPSD_AUTHORIZE:
            case IPSD_MASTER_ERASE:
            case IPSD_PROCESS_PVD_MESSAGE:
            case IPSD_PROCESS_PVR_MESSAGE:
            case IPSD_PROCESS_AUDIT_RESPONSE:
            case IPSD_GENERATE_PSD_KEY:
            case IPSD_LD_SECRET_KEY:
            case IPSD_LOAD_PROVIDER_KEY:
            case IPSD_ENABLE:
            case IPSD_DISABLE:
                bRNGCommand++;
                break;

            default:
                break;
        }
        // !!!!!!!!!!!! End Test Code !!!!!!!!!!!!!!!  
                                                                            // 
        bMsgLen = 0;                                                        // 
        if( !(controlRec.flags[0] & VXNOT) ) 
        {
            // Start by copying any known header bytes from the Header definition
            //  record to the transmit buffer, accumulating the message length
            //  in a local variable.  Some messages may have a 0-length header,
            //  if the header is in the data redirected from the EDM task.
            bMsgLen = headerRec[HEAD_SIZE];
            if( bMsgLen )
            {
                memcpy( messyInfo->xmitBuf, &headerRec[HEAD_START], bMsgLen );
            }
                                                                           
            if( controlRec.flags[1] == PHCFID ) 
            {
                if(   (bMsgLen == 8) 
                   && (messyInfo->xmitBuf[5] == 1) ) 
                {
                    memset(&messyInfo->xmitBuf[bMsgLen], 0, 4);
                    bMsgLen += 4;
                    memcpy(&thisHereShort, &messyInfo->xmitBuf[6], sizeof(short));
                    thisHereShort += 4;
                    memcpy(&messyInfo->xmitBuf[6], &thisHereShort, sizeof(short));
                }
            }
            if( (controlRec.flags[0] & VAPEND) ) 
            {                           // 
                apprecSniffer = 0;                                          // 
                while( appendedItems ) 
                {
                    // From the Appended Data Definition Record, get the dataID,
                    //  use the variable table to convert the ID to the data Address.
                    EndianAwareCopy( &ashort, &appendRec[apprecSniffer + 1], sizeof(short) );
                    pubSource = bobaVarAddresses[ashort].addr;
                    // If the "size" is really an exception code...
                    if( appendRec[apprecSniffer + 3] == TABLE_EXCEPTION ) 
                    {
                        // Handle the exceptions...
                        switch( appendRec[apprecSniffer + 4] ) 
                        {
                            case SAME_AS_LE:                                    /* determine the number of bytes to be  */
                                while( bMsgLen < 5 ) 
                                {
                                    messyInfo->xmitBuf[bMsgLen] = *pubSource;
                                    pubSource++;
                                    bMsgLen++;
                                }
                                inputSize = messyInfo->xmitBuf[4];              /* appended to the header... usually    */
                            break;                                              /* this is listed in the Message        */

                            case LEN_XCEPTION_LIST:
                                strucIndex = 0;
                                while( (mySpecialLengths[strucIndex] + mySpecialLengths[strucIndex + 1]) != 0 ) 
                                {
                                    EndianAwareCopy(&ashort, &mySpecialLengths[strucIndex], sizeof(short));
                                    if( ashort == messyInfo->msgID ) 
                                    {
                                        EndianAwareCopy(&ashort, &mySpecialLengths[strucIndex+2], sizeof(short));
                                        spLenFunc = (sizeGetter *)(funcCrossRef[ashort]);
                                        inputSize = (spLenFunc)(messyInfo->msgID);
                                        break;
                                    }                                                       
                                    else
                                    {
                                        strucIndex += 4;                                       
                                    }
                                }
                            break;

                            case REDIRECTED_DATA:
                                // The address is for a TransferControl structure.
                                // The struct contains the address and length of
                                //  the data to append.
                                memcpy( &redirectStruc, pubSource, 4 );
                                pubSource = redirectStruc->ptrToDataToSendToDevice;
                                inputSize = redirectStruc->bytesInSource;
                                break;

                            case IPSD_PACKET_LEN:
                                // The data is variable-length and packetized for 
                                //  an I-button.  The length is stored in the 
                                //  packet info struct.
                                inputSize = rIPSD_packetInfo.rHdr.bPacketSize;
                            break;

                            default:
                            break;
                        }
                    }
                    else 
                    {
                        EndianAwareCopy( &inputSize, &appendRec[apprecSniffer + 3], sizeof(short) );
                    }

                    memcpy( &messyInfo->xmitBuf[bMsgLen], pubSource, inputSize );
                    bMsgLen += inputSize;
                    inputSize = 0;
                    appendedItems--;
                    apprecSniffer += 4;
                }
            }
        }
        // Save the length of the message in the xferStruct sent to the driver.
        messyInfo->xferStruc->bMsgLen = bMsgLen;


#ifdef BOB_USING_USB
        howToHandleMsg = fnAccumulateUSBmsg(messyInfo);
        if( howToHandleMsg == BOB_BUILD_MSG_ERROR )
        {
            myStatus = FALSE;
        }
#endif
        if (howToHandleMsg == BOB_PROCESS_MSG) 
        {
            expectedReply = 1;                                              /* now gets this value from tealeavs table */
            switch(controlRec.flags[0] & VREPLY_MASK)                       /* Calc The Total Expected Reply Length */
            {                                                               /* ------------------------------------ */
                case NOREPLY:                                               /* for type NOREPLY, the reply length   */
                    expectedReply = 0;                                      /* is zero (duh)                        */
                break;

#ifdef BOB_USING_USB
                default:
                break;
#endif
            }
            switch (controlRec.retryCode) 
            { 
                case NORETRIES:
                    messyInfo->xferStruc->retries = 1;
                break;

                case STDTRIES:
                default:
                    messyInfo->xferStruc->retries = STANDARD_RETRY;
                break;

                case IF_NOT_ALONE:
// !!!!cb Janus is ALWAYS on base ???
//  !!!cb           if (fnSYSIsBaseAttached()) 
                    messyInfo->xferStruc->retries = STANDARD_RETRY;
//  !!!cb           else myStatus = FALSE;
                break;
            }
            if( myStatus ) 
            {
                messyInfo->xferStruc->msg_number = messyInfo->msgID;            /* fill its transfer structure with the */
                messyInfo->xferStruc->device_type = controlRec.device;          /* message ID, mux setting, output      */
                messyInfo->xferStruc->pMessage = messyInfo->xmitBuf;            /* message pointer, length of message,  */
                messyInfo->xferStruc->pDataRecv[0] = messyInfo->recBuf;         /* address for the reply, message case, */
//              messyInfo->xferStruc->bMsgLen = bMsgLen;                        /* timeout setting, and length of       */
                messyInfo->xferStruc->bMsgCase = controlRec.msgCase;            /* expected reply... also clear the     */
                messyInfo->xferStruc->expectedReply = expectedReply;            /* the error flag and reply length      */
                messyInfo->xferStruc->errorFlags = 0;                           /* that the driver will fill...         */
                messyInfo->xferStruc->replyLen[0] = 0;                          /* also specify whether a get response  */
                messyInfo->xferStruc->replyLen[1] = 0;                          /* also specify whether a get response  */
                messyInfo->xferStruc->replyLen[2] = 0;                          /* also specify whether a get response  */
                messyInfo->xferStruc->replyCnt = 1;
                messyInfo->xferStruc->replyNum = 0;
                messyInfo->xferStruc->timeout = timeValue;                      /* message is required and mark the     */

                messyInfo->xferStruc->eventID = BOB_DRIVERS_EVENTS; /* NOTE!!!! these three lines must be changed to variables */
                messyInfo->xferStruc->errMask = T0_ERROR;
                messyInfo->xferStruc->okMask = T0_SUCCESS;
                messyInfo->xferStruc->getResponse = 0;                          /* not used in comet */
            }
        }
    }
    else 
    {
        howToHandleMsg = BOB_BUILD_MSG_ERROR;
    }
    return( howToHandleMsg );
}


/* **********************************************************************

 FUNCTION NAME:     

 PURPOSE:           
                    
 AUTHOR:            R. Arsenault

 INPUTS:
 MODS:
 2009.03.23  Clarisa Bellamy - If reply type is RPLY_IPSD, then allow 
                the IPSD_CMDRESP_RNG_REPEATED error, but record it.
                             
 *************************************************************************/
BOOL    fnTeaExamineReply( const struct msgInfo *messyInfo ) 
{
    struct      msgControl controlRec;
    BOOL        retval;                                         /* return value                     */
    UINT8       *statusPlace;                                   /* location of status reply storage */
//    UINT8       bMsgId;
    UINT8       *ReplyMsg; 
    UINT16      replyLen; 
    UINT8       Le;
    UINT16      wCmdStatus;

    retval = TRUE;

    if( fnTeaGetMsgControl(messyInfo->msgID, &controlRec) ) 
    {
        statusPlace = messyInfo->statusPlace;

//        bMsgId = messyInfo->msgID;
        ReplyMsg = messyInfo->recBuf; 
        replyLen = messyInfo->xferStruc->replyLen[0]; 
         // The length should be < 256.
         //  A non-zero high byte indicates a special case.
        Le = (UINT8)messyInfo->specialLen;

        // Based on the type of reply expected.  Determine if the received msg 
        //  is acceptable.
        switch(controlRec.flags[0] & VREPLY_MASK) 
        {             
            case PSD_STAT:
                if( (   *(statusPlace+0) 
                     +  *(statusPlace+1) 
                     +  *(statusPlace+2) 
                     +  *(statusPlace+3)) == 0 )
                {
                     retval = TRUE;
                }
                else retval = FALSE;
                break;

            case STAT4:
                retval = FALSE;
                if( messyInfo->recBuf[2] == 0xFF ) 
                {
                    *(statusPlace+0) = 0;
                    *(statusPlace+1) = 0;
                    *(statusPlace+2) = 0;
                    *(statusPlace+3) = 1;
                }
                if( (   *(statusPlace+0) 
                      + *(statusPlace+1) 
                      + *(statusPlace+2) 
                      + *(statusPlace+3)) == 0 )   // OK status 
                {                       
                        retval = TRUE;
                        break;
                }

                if( (   *(statusPlace+0)                                /* image already down-              */
                      + *(statusPlace+1)                                /* loaded... will be picked up by   */
                      + *(statusPlace+2)                                /* preMessage of the download script*/
                      + *(statusPlace+3)) == 0x61 )
                {
                    retval = TRUE;
                }
                break;

            case NOREPLY:                                       /* IF NO REPLY IS EXPECTED          */
                if( replyLen ) 
                {                                     /* if a reply was received,         */
                    retval = FALSE;                                 /* something is wrong... mark       */
                    statusPlace[0] = 0xFF;                          /* the status with the general      */
                    statusPlace[1] = 0xFF;                          /* error code                       */
                }
                else 
                {                                              /* if no reply was received,        */
                    statusPlace[0] = 0x90;                          /* assume everything ok , and mark  */
                    statusPlace[1] = 0x00;                          /* it with the general OK status    */
                }
                break;
        
            case DATA_ONLY:     /* WHEN RECEIVING DATA ONLY         */
                if( messyInfo->msgID != 0 ) 
                {
                    if( !replyLen ) 
                    {    // If length is zero, mark as an error.
                        retval = FALSE;
                        statusPlace[0] = 0xFF;
                        statusPlace[1] = 0xFF;
                    }
                    else 
                    {   // Otherwise mark as ok.
                        statusPlace[0] = 0x90;      
                        statusPlace[1] = 0x00;      
                    }
                }
                break;                                                      
        
            case DATA_STAT:     /* WHEN RECEIVING DATA PLUS STATUS      */
                if( replyLen < 2 )    // Must at least receive a status.                      
                {  
                    // If not, mark as general error.
                    retval = FALSE;                    
                    statusPlace[0] = 0xFF; 
                    statusPlace[1] = 0xFF; 
                }
                else 
                {   // If reply has status, copy the status to the device's status
                    //  storage location and examine the status to determine if the 
                    //  message was successful.                 
                    statusPlace[0] = ReplyMsg[replyLen - 2];        
                    statusPlace[1] = ReplyMsg[replyLen - 1];        
                    if(   (ReplyMsg[replyLen - 2] == 0x90) 
                       && (ReplyMsg[replyLen - 1] == 0)  )
                    {         
                        retval = TRUE;  
                    }
                    else
                    {   // If status in message is not SUCCESSFUL, return FALSE. 
                        retval = FALSE;     
                    }                   
                }
                break;
        
            case GET_RESP:                                      /* WHEN RECEIVING A RESPONSE TO A   */
                if (replyLen != 2) {                                /* MESSAGE THAT NEEDS GET RESPONSE  */
                    retval = FALSE;                                 /* if not a status message          */
                    statusPlace[0] = 0xFF;                          /* mark a general error             */
                    statusPlace[1] = 0xFF;                          /*                                  */
                }
                else {                                              /* if a status message, copy it     */
                    statusPlace[0] = ReplyMsg[0];                   /* ... and then test it:            */
                    statusPlace[1] = ReplyMsg[1];                   /*                                  */
                                                                    /* if a correct status for a message*/
                    if (ReplyMsg[0] == 0x61) {                      /* requiring Get Response but the   */
                        if (ReplyMsg[1] != Le) {                    /* length doesn't match the length  */
                            retval = FALSE;                         /* expected, mark an error          */
                        }
                    }                                               /* if not a correct status for a    */ 
                    else retval = FALSE;                            /* message that requires a get      */
                }                                                   /* response, mark an error for the  */
                break;                                              /* caller                           */
        
        
            case XXGET_RESP:                                    /* GET RESPONSE TYPE WITH RESPONSE  */
                if (replyLen != 2) {                                /* THAT CAN BE IGNORED              */
                    retval = FALSE;                                 /* if not a status message          */
                    statusPlace[0] = 0xFF;                          /* mark a general error             */
                    statusPlace[1] = 0xFF;                          /*                                  */
                }
                else {                                              /* if a status message, copy it     */
                    statusPlace[0] = ReplyMsg[0];                   /* ... and then test it:            */
                    statusPlace[1] = ReplyMsg[1];                   /*                                  */
                                                                    /* if a correct status for a Get    */
                    if (ReplyMsg[0] == 0x61) {                      /* Response Message, overide the    */
                        statusPlace[0] = 0x90;                      /* status to a standard success...  */
                        statusPlace[1] = 0;                         /* the pending response can be      */
                    }                                               /* ignored                          */
                    else retval = FALSE;
                }
                break;
        
            case REPLY_STAT:                                    /* if only a staus reply is     */
                retval = FALSE;                                     /* expected, just check that    */
                if (ReplyMsg[0] == 0x90) {                          /* an OK status was received    */
                    if (ReplyMsg[1] == 0) retval = TRUE;    
                }
                statusPlace[0] = ReplyMsg[0];
                statusPlace[1] = ReplyMsg[1];
                break;

            case RPLY_IPSD:
                retval = FALSE;
                wCmdStatus = (ushort)(statusPlace[0]) * 0x100;
                wCmdStatus += (ushort)(statusPlace[1]);
                // Check for Message OK or Packet OK...
                if(   (wCmdStatus == IPSD_CMDRESP_OK) 
                   || (wCmdStatus == IPSD_CMDRESP_PACKET_OK) )
                {
                    retval = TRUE;
                }
                else 
                {
                    // Check for errors that need special attention...
                    if( wCmdStatus == IPSD_CMDRESP_RNG_REPEATED ) 
                    {
                        // We don't want to act like it is an error, except to log it.
                        // Not sure this is a good place to put all this call, but its easy.
                        fnLogSystemError( ERROR_CLASS_IPSD1, IPSDERR_REPEATED_RNG, 
                                          messyInfo->msgID, 0, 0 );
                        fnLogError( ERR_TYPE_NORMAL, ERROR_CLASS_IPSD1, IPSDERR_REPEATED_RNG );
                        sprintf( pBobsLogScratchPad, 
                                "MsgID %d, RNG Repeated Error from IPSD.", 
                                messyInfo->msgID );
                        fnDumpStringToSystemLog( pBobsLogScratchPad );
                        retval = TRUE;
                    }
                    else
                    {
                        // If we get an error status, but still want to parse it, 
                        //  put a msg in the syslog.  (One example is when the PVD Response 
                        //  from Data Center has a Data Center error in it.)
                        if(   (replyLen > 2 ) // msg longer than just PSD status (2 bytes)
                           && ((messyInfo->flags[0] & TRY_TO_DISTRIB) == TRY_TO_DISTRIB) )
                        {   
                            sprintf( pBobsLogScratchPad, 
                                    "Parse IPSD err msg: ID %d, IPSD Status %02X",
                                    messyInfo->msgID, wCmdStatus );
                            fnDumpStringToSystemLog( pBobsLogScratchPad );
                            //Leave retval = FALSE
                        }
                    }
                }

                break;

            case RPLY_IPSD_BL:
                retval = FALSE;
                wCmdStatus = (ushort)(statusPlace[0]);
                // Check for Message OK or Packet OK...
                if( wCmdStatus == IPSDBL_NO_ERROR )
                    retval = TRUE;
                break;

            default:
            break;
        
        }
    }
    else retval = FALSE;
    return(retval);
}
    
/* ***************************************************************************
*
* FUNCTION NAME:    fnTeaCheckReplyStatus        
*
*       PURPOSE:    If the reply is from an IPSD, this reads and stores the status 
*                   at the end of the message.  If the status indicates an error,
*                   the rest of the message should not be read.
*
*        AUTHOR:    Clarisa Bellamy
*
*        INPUTS:    Pointer to MsgInfo struct, which has all pertinent info
*                   about the message sent to the driver.
*               
*        RETURNS:   SCM_BAD_REPLY_LENGTH if reply message is supposed to have 
*                   an IPSD statusResponseCode and does not. 
*                   Else returns JMEGABOB_STATE_OK.
*
*         NOTES:    If reply is not from IPSD, return JMEGABOB_STATE_OK.
*
*---------------------------------------------------------------------------*/
UINT16  fnTeaCheckReplyStatus( const struct msgInfo *messyInfo )	//FIX ME JAH may need to fight more endians here
{
    struct msgControl   controlRec;
    UINT16          retval;
    UINT8           *myDirectory;
    UINT8           *myErrorIndex;
    UINT16          wStatusSize, wMsgLen, wStatusOffset;
    UINT8           *pStatusSrc, *pStatusDest;
    UINT16          wVarIndex;  
    
    retval = JMEGABOB_STATE_OK;
    fnTeaGetConstant(TOTEAS, &myDirectory);
    myDirectory += teaAddress;

    if( fnTeaGetMsgControl( messyInfo->msgID, &controlRec ) ) 
    {
        myErrorIndex = fnTeaGetErrDirRecord( controlRec.device, ANY_ERR_MODE );
        //      Size = size of status for this device.
        wStatusSize = myErrorIndex[8];
        //      Get Variable ID for status of this device.
        EndianAwareCopy( &wVarIndex, &myErrorIndex[9], sizeof(short) );
        //      Convert ID to address of Status for this device.
        pStatusDest = bobaVarAddresses[ wVarIndex ].addr;

        // If the reply is from an IPSD, then the last two bytes are the status.  Read this first.
        if( (controlRec.flags[0] & VREPLY_MASK) == RPLY_IPSD )
        { 
            // Length of entire reply is set in transfer struct by driver.
            wMsgLen = messyInfo->xferStruc->replyLen[0];
            // cmdStatusResp is second variable in data, don't copy it to glob.
            if( wMsgLen >= wStatusSize )
            {
                // If the response is at least 2 bytes, the status is the last two bytes.
                // NOTE: we are copying the two byte IPSD status to the IPSD device status variable.
                // The device status variable is cleared by fnBobsErrorStandards if an error is present.
                wStatusOffset = wMsgLen - wStatusSize;
                pStatusSrc = &(messyInfo->recBuf[ wStatusOffset ]);
                memcpy( pStatusDest, pStatusSrc, wStatusSize );
                
                // Since the two byte version of the IPSD status will get cleared by fnBobsErrorStandards
                // we will also copy the status to a 4 byte status container that will persist
                memset(pIPSD_4ByteCmdStatusResp, 0, sizeof(pIPSD_4ByteCmdStatusResp));
                memcpy(&pIPSD_4ByteCmdStatusResp[2], pStatusSrc, 2);
            }
            else
            {
                // all messages should have a status... this is an error.
                retval = BOB_BAD_REPLY_LENGTH;
            }
        }

        // If the reply is from the IPSD Boot Loader, then the status is the 5th byte - rply[4].
        if( (controlRec.flags[0] & VREPLY_MASK) == RPLY_IPSD_BL )
        { 
            // Length of entire reply is set in transfer struct by driver.
            wMsgLen = messyInfo->xferStruc->replyLen[0];
            // All replies from the boot loader come with an 8-byte header.
            //  The first 3 bytes must be the BootLoader signature, 
            //   the 4th byte is the command
            //   the 5th byte is the status code.
            if( wMsgLen < IBTN_BL_REPLY_MINLEN )
            {
                // If the reply is less than 8-bytes, its an error.
                retval = BOB_IPSD_BL_REPLY_TOO_SHORT;
            }
            else if( strncmp( (char *)(messyInfo->recBuf), IBTN_BL_REPLY_SIGNATURE, IBTN_BL_REPLY_SIGLEN ) != 0 )
            {
                // If the reply does not start with the BootLoader Signature, it is an error.
                retval = BOB_IPSD_BL_BAD_REPLY_FORMAT;
            }
            else
            {
                // If the response header looks good, read the reply code.
                pStatusSrc = &(messyInfo->recBuf[ IBTN_BL_STATUS_OFFSET ]);
                memcpy( pStatusDest, pStatusSrc, wStatusSize );
                
                // Since the one-byte version of the IPSD status will get cleared by fnBobsErrorStandards
                // we will also copy the status to a 4 byte status container that will persist
                memset( pIPSD_4ByteCmdStatusResp, 0, sizeof(pIPSD_4ByteCmdStatusResp) );
                memcpy( &pIPSD_4ByteCmdStatusResp[3], pStatusSrc, 1 );
            }
        }
    }
    return( retval );   
}

/* **********************************************************************
 FUNCTION NAME:         fnTeaDistribReply    
 PURPOSE:   
    Uses the reply definition record, in the orbit database, that is
    listed in the msgControl table for this message, to distribute the
    data in the reply from the device to the appropriate variables/buffers,
    etc.    
 INPUTS:            
    messyInfo - Ptr to structure with information about this message.
 RETURN:
    BOOL - FALSE if this message is not in the message control table.
          else TRUE. 
 HISTORY:
 2009.03.13 Clarisa Bellamy - Add support for new size exception type, 
                    LEX_REDIRECTED_IPSD_BL, used for passthrough messages
                    from the EDM to the Boot Loader.                    
 2009.02.13 Clarisa Bellamy - Add support for new size exception type, 
                    LEX_SIMPLE_COPY, used to store the rx'd data, without
                    advancing the distrib pointer.
                    Also added some checking for null-pointers.                    
 AUTHOR: R. Arsenault
// --------------------------------------------------------------------------*/
typedef UINT16 replyFunct(struct msgInfo *);

BOOL fnTeaDistribReply( struct msgInfo *messyInfo ) //FIX ME JAH may need to fight more endians in here
{
    struct  msgControl controlRec;
    UINT8       *myDirectory;
    UINT8       *myReplyDataIndex;
    UINT8       *myErrorIndex;
    UINT8       *replyRec;
    UINT8       replyVarCnt;
    short       recordSniffer;
    short       ashort;
    short       destID;
    char        *dest;
    UINT16      copySize;
    UINT8       *recDeviceData;
    BOOL        retval;                 // This function's return value 
    BOOL        keepOnChuggin;
    struct      TransferControlStructure    *pRedirectStruc;
    BOOL        memberFlag;
    char        *MemberPtr = NULL_PTR;
    char        *MemberSourcePtr = NULL_PTR;
    UINT8       MemberSize = 0;
    UINT16      multiReplyIndex = 0;
    replyFunct  *spReplyFunc;   // for reply function exception
    UINT16      replyRetVal;    // for reply function exception
    UINT16      destSize;       // For actual size of destination,

#ifndef JANUS
    BOOL        floatFlag = FALSE;
#endif

    retval = TRUE;
    fnTeaGetConstant(TOTEAS, &myDirectory);
    myDirectory += teaAddress;

    if( (messyInfo->flags[0] & VREPLY_MASK) == RPLY_IPSD )
    {
        // For IPSD messages, the cmdStatusResp is saved and examined first.
        // If we get here, then it is current and either Message_OK or 
        // PACKET_OK.  A cmdStatusResp of Packet_OK, indicates that the last 
        // packet has not yet been sent.  
        // Do not process reply until all packets have been sent.
        if( (pIPSD_cmdStatusResp[0] == (IPSD_CMDRESP_PACKET_OK >> 8)) &&
            (pIPSD_cmdStatusResp[1] == (IPSD_CMDRESP_PACKET_OK & 0x0FF)) )
            return( TRUE );   
    }

    if (fnTeaGetMsgControl(messyInfo->msgID, &controlRec)) 
    {
        myErrorIndex = fnTeaGetErrDirRecord( controlRec.device, ANY_ERR_MODE );

        EndianAwareCopy( &myReplyDataIndex,
						myDirectory + (REPLYDATAINDEX * TEA_OFFSET_SIZE), 
						TEA_OFFSET_SIZE);
        myReplyDataIndex += teaAddress;

        keepOnChuggin = TRUE;
        memberFlag = FALSE;
        while (keepOnChuggin) 
        {
            keepOnChuggin = FALSE;
            EndianAwareCopy( &replyRec,
							myReplyDataIndex + (controlRec.replDataID * TEA_OFFSET_SIZE),
							TEA_OFFSET_SIZE);
            replyRec += teaAddress;         
        
            replyVarCnt = replyRec[0];
            recordSniffer = 1;
            recDeviceData = messyInfo->recBuf;


        
            while( replyVarCnt ) 
            {
                EndianAwareCopy(&destID, &replyRec[recordSniffer], sizeof(short));
                dest = bobaVarAddresses[destID].addr;
                destSize = bobaVarAddresses[destID].siz;
                EndianAwareCopy(&copySize, &replyRec[recordSniffer + 2], sizeof(short));
        
                if (replyRec[recordSniffer + 2] == TABLE_EXCEPTION) 
                {
                    switch (replyRec[recordSniffer + 3]) 
                    {       
                        case IPSD_GLOB_LEN:
                            // Reply is a variable length glob, plus a 2-byte cmdStatusResp.
                            // Length is set in transfer struct by driver.
                            copySize = messyInfo->xferStruc->replyLen[0];
                            // cmdStatusResp is second variable in data, don't copy it to glob.
                            if( copySize > 2 )
                            {
                                copySize -= 2;
                            }
                            else
                            {
                                copySize = 0;
                            }
                            // Save glob size.
                            wRxGlobSize = copySize;    

                            // Make sure we don't overwrite the destination.
                            if( copySize > destSize )
                            {
                                // Log that the destination is too small.  
                                // If we get here, we probably want to change the 
                                // variable size to have enough room to store the 
                                // whole glob.
                                // In the system log, store the destination's 
                                //  variable ID and size, and the glob size.
                                sprintf( pBobsLogScratchPad, "%s %s%d. %s%ud. %s%ud.", 
                                         "Glob array too small for rx'd data.",
                                         "BobID = ", destID, 
                                         "BobSize = ", destSize, 
                                         "GlobSz = ",  copySize );
                                fnDumpStringToSystemLog( pBobsLogScratchPad );
                                // In the system error log, store the Destination's
                                //  variable ID and the amount of space it is short.
                                fnLogSystemError( ERROR_CLASS_JBOB_TASK, 
                                                  BOB_GLOB_VAR_TOO_SMALL, 
                                                  (UINT8)(destID / 0x100), 
                                                  (UINT8)(destID % 0x100), 
                                                  (UINT8)(copySize - destSize) );       
                                // Make sure we don't overwrite the destination buffer.
                                // Otherwise, keep going.
                                copySize = destSize;
                            }
                            break;

                        case IPSD_GLOB_LEN_LEN:
                        // Copy the glob length, instead of message data into this
                        //  destination.
                        memcpy( dest, &wRxGlobSize, sizeof( wRxGlobSize ) );
                        // Keep the message pointer the same.  Go to next entry in rply record.
                        copySize = 0;
                        break;

                        case LEN_XCEPTION_LIST:
                        memcpy(&copySize, &msgPacketInfo[2], sizeof(short));
                        break;
                        
                        case SAME_AS_LC:
                        copySize = messyInfo->xmitBuf[4];
                        break;

                        case LEX_SIMPLE_COPY:     // Length Exception Code: Copy the message to a buffer.           
                            // This will copy the entire message into the destination, then 
                            //  allow the rest of the reply definition record to distribute it again.
                            copySize = messyInfo->xferStruc->replyLen[0];
                            if( copySize > destSize )
                                copySize = destSize;
                            memcpy( dest, recDeviceData, copySize );
                            // Reuse the existing GlobSize variable to save the size that we 
                            //  copied so it can be captured by the next GLOB_LEN_LEN entry 
                            //  in the reply definition record.
                            wRxGlobSize = copySize;
                            // This allows the next message to be distributed a second time.
                            copySize = 0;
                            break;

                        case LEX_REDIRECTED_IPSD_BL:
                        // This section copies the command and return code from the header into the 
                        //  first two bytes of the reply pointed to by the xfer structure.  Then it
                        //  sets dest to the 3rd byte, copysize to the length of the data portion of
                        //  the message (after the 8-byte header), and source to the data portion of
                        //  the message, so that, further down, the data 
                        //  can be copied from messyInfo->recBuf to the CallersDestBuffer pointed
                        //  to in the transfer control structure.
                        // It also sets the # of packets to 1, and the length of that packet
                        //  as the length of the reply.             
                            // The Variable ID in the in the reply record is of the redirect structure pointer.
                            memcpy( &pRedirectStruc, dest, 4 );
                            //  Get the destination address from the redirect structure.
                            dest = (char *)(pRedirectStruc->ptrToCallersDestBuffer);
                            // Length is set in transfer struct by driver.  Return the whole length.
                            copySize = messyInfo->xferStruc->replyLen[0];
                            // Store the length of the reply, and set the packets to 1.
                            if(   (dest != 0)
                               && (copySize > 4) )
                            {
                                // The EDM wants to see the command byte, the status byte, then 
                                //  the data.  So retrieve the command and status bytes first.
                                // Command byte is the 4th byte in the reply header.
                                *(dest++) = pIPSD_Bl_ReplyHeader[3];
                                *(dest++) = bIPSD_Bl_ReplyStatus;
                                if( copySize > 8 )
                                {  
                                    copySize -= 8;
                                }
                                else 
                                {   
                                    copySize = 0;
                                }
                                pRedirectStruc->bytesWrittenToDest[0] = copySize +2;
                            }
                            else
                            {
                                pRedirectStruc->bytesWrittenToDest[0] = 0;
                            }
                            pRedirectStruc->numberOfDeviceReplies = 1;

                            // Clear out error, because it's too big.
                            memset( pRedirectStruc->deviceReplyStatus, 0 , sizeof(pRedirectStruc->deviceReplyStatus) ); 
                        
                         // From bobsErrDirectory entry for this device ...
                         // This section sets the ptrs up so that further down, the device status can be
                         //  copied from the location defineed in the error directory entry for this device
                         //  to the transfer control structure.
                            //   Size = size of status for this device.
                            MemberSize = myErrorIndex[8];
                            // Get ID for status of this device.
                            EndianAwareCopy( &ashort, &myErrorIndex[9], sizeof(short) );
                            //  Convert ID to address of Status for this device.
                            MemberSourcePtr = bobaVarAddresses[ashort].addr;
                            MemberPtr = (char *)(&pRedirectStruc->deviceReplyStatus[(4 - MemberSize)]);
                            // After processing all the reply fields, copy the device status into the redirect structure.
                            memberFlag = TRUE;
                            break;

                        case REDIRECTED_DATA_IPSD:            
                            // The Variable ID in the in the reply record is of the redirect structure pointer.
                            memcpy( &pRedirectStruc, dest, 4 );
                            //  Get the destination address from the redirect structure.
                            dest = (char *)(pRedirectStruc->ptrToCallersDestBuffer);
                            // Length is set in transfer struct by driver.
                            copySize = messyInfo->xferStruc->replyLen[0];
                            // cmdStatusResp is last two bytes of the message.  If we
                            // want it, we will call it out in the next variable in 
                            // reply def, don't copy it to glob.
                            if( copySize > 2 )          copySize -= 2;
                            else                        copySize = 0;
                            if( dest != 0 )
                            {
                                pRedirectStruc->bytesWrittenToDest[0] = copySize;
                            }
                            else
                            {
                                pRedirectStruc->bytesWrittenToDest[0] = 0;
                            }
                            pRedirectStruc->numberOfDeviceReplies = 1;

                            // Clear out error, because it's too big.
                            memset( pRedirectStruc->deviceReplyStatus, 0 , sizeof(pRedirectStruc->deviceReplyStatus) ); 
                        
                         // From bobsErrDirectory entry for this device ...
                         // This section sets the ptrs up so that further down, the device status can be
                         //  copied from the location defineed in the error directory entry for this device
                         //  to the transfer control structure.
                            //   Size = size of status for this device.
                            MemberSize = myErrorIndex[8];
                            // Get ID for status of this device.
                            EndianAwareCopy( &ashort, &myErrorIndex[9], sizeof(short) );
                            //  Convert ID to address of Status for this device.
                            MemberSourcePtr = bobaVarAddresses[ashort].addr;
                            MemberPtr = (char *)(&pRedirectStruc->deviceReplyStatus[(4 - MemberSize)]);
                            // After processing all the reply fields, copy the device status into the redirect structure.
                            memberFlag = TRUE;
                            break;

                        // Not supported in Janus because this assumes REDIRECTED is 
                        //  only for Myko PSD.
                        //case REDIRECTED_DATA:
        
                        case NEST_XCEPT:
                            // NOTE: This must be the last entry in a reply record.
                            keepOnChuggin = TRUE;
                            memcpy(&controlRec.replDataID, &replyRec[recordSniffer], TEA_IDS_SIZE);
                            replyVarCnt = 0;
                            break;
        
                        /* cb not supported in Janus or FPHX:
                        case INIT_REPLY_PTR:
                        case PER_PSD_HEADER:
                        case IF_PHC_1ST_PACKET:
                        cb */

                        case USE_FUNCTION:     // Use a function to do special handling of the reply
                        spReplyFunc = (replyFunct *)funcCrossRef[ destID ];
                        replyRetVal = (spReplyFunc)(messyInfo);
                        if( replyRetVal != BOB_OK )
                            retval = FALSE;
                        copySize = 0;
                        break;

                        
                        default:
                        break;
                    }
                }
                if( !keepOnChuggin ) 
                {
                    if( copySize ) 
                    {
                        if (destID == FLOATINGREPLYPTR) dest = floatingReplyPtr;
                        if( dest == NULL_PTR )
                        {
                            // !!! Should log an error here:
                            // The reply engine wants to save the data. 
                            // but there is no destination.
                        }
                        else
                        {
                        	if((messyInfo->msgID == IPSD_GET_MODULE_STATUS_ALL) &&
                        	   ((destID == BOBID_IPSD_PIECECOUNT) ||
                        		(destID == BOBID_IPSD_ZEROPOSTAGEPIECECOUNT)))
                        	{
                        		EndianAwareCopy( dest, recDeviceData, copySize );
                        	}
                        	else
                        	{
                        		memcpy( dest, recDeviceData, copySize );
                        	}
                        }
                        if( destID == FLOATINGREPLYPTR ) 
                        {
                            floatingReplyPtr += copySize;
                            recDeviceData = &messyInfo->recBuf[multiReplyIndex];
                            multiReplyIndex += 300;
                        }
                        else recDeviceData += copySize;
                    }
                    if (replyVarCnt) replyVarCnt--;
                    recordSniffer += 4;
                }
            }
        }
        if( memberFlag && MemberPtr && MemberSourcePtr && MemberSize )
        {
            memcpy( MemberPtr, MemberSourcePtr, MemberSize );
			dbgTrace( DBG_LVL_INFO,  "BOB--fnTeaDistribReply: msgID=%d, edmTransferControl.deviceReplyStatus = %02X%02X%02X%02X, pIPSD_cmdStatusResp=%02X%02X", 
						messyInfo->msgID,
						edmXferStructPtr->deviceReplyStatus[0], edmXferStructPtr->deviceReplyStatus[1],
						edmXferStructPtr->deviceReplyStatus[2], edmXferStructPtr->deviceReplyStatus[3], 
						pIPSD_cmdStatusResp[0], pIPSD_cmdStatusResp[1] );
        }
    }
    else 
    {
        retval = FALSE;
    }

    return( retval );
}


/* **********************************************************************

 FUNCTION NAME:     

 PURPOSE:           
                    
 AUTHOR:            R. Arsenault

 INPUTS:            
 *************************************************************************/
BOOL    fnTeaGetDataMat( UINT16 groupID, UINT16 itemID, struct dataMatRec *recordDest )
{
    UINT8       *myDirectory;
    UINT8       *myDataMatIndex;
    UINT8       *myDataMat;
    BOOL        retval;
    UINT8       hiByte;
    UINT8       lowByte;
    UINT16      tableSize;
    UINT16      j;
    UINT16      ashort;

    retval = FALSE;
    fnTeaGetConstant(TOTEAS, &myDirectory);
    myDirectory += teaAddress;


    EndianAwareCopy( &myDataMatIndex,
					myDirectory + (DATAMAPINDEXER * TEA_OFFSET_SIZE), 
					TEA_OFFSET_SIZE);
    myDataMatIndex += teaAddress + (groupID * (TEA_OFFSET_SIZE + TEA_CNT_SIZE));

    EndianAwareCopy( &myDataMat, myDataMatIndex, TEA_OFFSET_SIZE);
    myDataMat += teaAddress;

    EndianAwareCopy( &tableSize, &myDataMatIndex[4], sizeof(short));

    hiByte = (itemID >> 8) & 0xFF;
    lowByte = itemID &0xFF;

    for (j = 0; j < tableSize; j++) {
        if (myDataMat[1] == lowByte) {
            if (myDataMat[0] == hiByte) {
                retval = TRUE;
                break;
            }
        }
        myDataMat += DATAMAT_REC_SIZ;
    }
    if (retval) {
        recordDest->variance = myDataMat[2];
        EndianAwareCopy( &ashort, &myDataMat[3], sizeof(UINT16) );
        recordDest->controlID = ashort;
        recordDest->conversion = myDataMat[5];
    }
    return(retval);
}

/* **********************************************************************

 FUNCTION NAME:     

 PURPOSE:   Searches through the whenAmI table for a record that has a 
            matching msgID.  Then loads the dest, source, and exception
            from that record into the recordDest            
                    
 AUTHOR:            R. Arsenault

 INPUTS:    msgID to look for, 
            recordDest, where to store the data from the matching record.
                    
 *************************************************************************/
BOOL    fnTeaGetDependence( UINT16 msgID, struct DTdepends *recordDest )
{
    UINT8       *myDirectory;
    UINT8       *myWhenAmI;
    BOOL        retval;
    UINT8       hiByte;
    UINT8       lowByte;
    UINT8       smallTableSize;
    UINT8       j;
    UINT16      ashort;

    retval = FALSE;
    fnTeaGetConstant(MAXDATEDEPENDENCE, &smallTableSize);
    fnTeaGetConstant(TOTEAS, &myDirectory);
    myDirectory += teaAddress;

    EndianAwareCopy( &myWhenAmI,
					myDirectory + (WHENAMI * TEA_OFFSET_SIZE), 
					TEA_OFFSET_SIZE);
    myWhenAmI += teaAddress;

    hiByte = (msgID>>8) & 0xFF;
    lowByte = msgID & 0xFF;

    for (j = 0; j < smallTableSize; j++) {
        if (myWhenAmI[1] == lowByte) {
            if (myWhenAmI[0] == hiByte) {
                retval = TRUE;
                break;
            }
        }
        myWhenAmI += DATE_DEPEND_REK_SIZ;
    }
    if (retval) {
        EndianAwareCopy( &ashort, &myWhenAmI[2], sizeof(UINT16) );
        recordDest->destID = ashort;
        EndianAwareCopy( &ashort, &myWhenAmI[4], sizeof(UINT16) );
        recordDest->sourceID = ashort;
        recordDest->xceptionCase = myWhenAmI[6];
    }
    return(retval);
}


/* **********************************************************************

 FUNCTION NAME:     

 PURPOSE:           
                    
 AUTHOR:            R. Arsenault

 INPUTS:            
 *************************************************************************/
BOOL    fnTeaGetexception( UINT16 msgID, struct DTdepends *recordDest )
{
    UINT8       *myDirectory;
    UINT8       *myXceptAmI;
    BOOL        retval;
    UINT8       hiByte;
    UINT8       lowByte;
    UINT8       smallTableSize;
    UINT8       j;
    UINT16       ashort;

    retval = FALSE;
    fnTeaGetConstant(MAXDATEEXCEPTION, &smallTableSize);
    fnTeaGetConstant(TOTEAS, &myDirectory);
    myDirectory += teaAddress;

    EndianAwareCopy( &myXceptAmI,
					myDirectory + (XCEPTAMI * TEA_OFFSET_SIZE), 
					TEA_OFFSET_SIZE);
    myXceptAmI += teaAddress;

    hiByte = (msgID>>8) & 0xFF;
    lowByte = msgID & 0xFF;

    for (j = 0; j < smallTableSize; j++) {
        if (myXceptAmI[1] == lowByte) {
            if (myXceptAmI[0] == hiByte) {
                retval = TRUE;
                break;
            }
        }
        myXceptAmI += DATE_DEPEND_REK_SIZ;
    }
    if (retval) {
        EndianAwareCopy( &ashort, &myXceptAmI[2], sizeof(UINT16) );
        recordDest->destID = ashort;
        EndianAwareCopy( &ashort, &myXceptAmI[4], sizeof(UINT16) );
        recordDest->sourceID = ashort;
        recordDest->xceptionCase = myXceptAmI[6];
    }
    return(retval);
}

/* **********************************************************************

 FUNCTION NAME:     

 PURPOSE:           
                    
 AUTHOR:            R. Arsenault

 INPUTS:            
 *************************************************************************/
BOOL fnTeaGetSysDateRef(ushort recnum, ushort *dest, ushort *source) 
{

    return(fnTeaGetaRef(recnum, dest, source, MSGSYSDATES));
}


/* **********************************************************************

 FUNCTION NAME:     

 PURPOSE:           
                    
 AUTHOR:            R. Arsenault

 INPUTS:            
 *************************************************************************/
BOOL fnTeaGetPrintDateRef(ushort recnum, ushort *dest, ushort *source) {
    return(fnTeaGetaRef(recnum, dest, source, MSGPRINTDATES));
}

/* **********************************************************************

 FUNCTION NAME:     

 PURPOSE:           
                    
 AUTHOR:            R. Arsenault

 INPUTS:            
 *************************************************************************/
BOOL fnTeaGetaRef( UINT16 recnum, UINT16 *dest, UINT16 *source, UINT16 dirIndex ) 
{
    UINT8       *myDirectory;
    UINT8       *myShortRefTable;

    fnTeaGetConstant(TOTEAS, &myDirectory);
    myDirectory += teaAddress;

    EndianAwareCopy( &myShortRefTable,
					myDirectory + (dirIndex * TEA_OFFSET_SIZE), 
					TEA_OFFSET_SIZE);
    myShortRefTable += teaAddress + (recnum * 2 * TEA_IDS_SIZE);

    EndianAwareCopy(dest, &myShortRefTable[0], sizeof(short));
    EndianAwareCopy(source, &myShortRefTable[2], sizeof(short));

    return(TRUE);
}


// ***************************************************************************
// FUNCTION NAME:       fnTeaGetErrorDirectoryEntry
// PURPOSE: 
//      Given an index into the tealeavs Error Directory, and a pointer to an 
//      Error Directory Entry structure, fill the structure with the data 
//      from the indexed entry in the Error Directory.
//
// INPUTS: 
//      pErrDirEntry    Ptr to Error Directory Structure (destination).
//      bIndex          Index into Error Directory table.
//    
// RETURNS:
//      BOOL  - TRUE if index is in range of table size, and destination
//                      structure was filled. 
//              FALSE if Index is too big.
// NOTES:
// HISTORY:
//  2009.06.11 Clarisa Bellamy - Created to make things simpler.         
//----------------------------------------------------------------------------
BOOL fnTeaGetErrorDirectoryEntry( T_TEA_ERROR_DIR_ENTRY *pErrDirEntry, UINT8 bIndex )
{
    BOOL    retVal = TRUE;
    UINT8 * pErrDirEntrySource;
    UINT8   bErrDirectoryLen;
    UINT16  wVariableID;

    // Get the Number of Error Directory Entries, and verify bIndex is in range.
    fnTeaGetConstant( ERRORGROUPCOUNT, &bErrDirectoryLen );
    if( bIndex < bErrDirectoryLen )
    {
        // Get address of ErrDirectory entry:
        pErrDirEntrySource = (UINT8 *)(fnTeaGetTable( BOBSERRDIRECTORY ));
        pErrDirEntrySource += ((UINT32)bIndex * ERR_DIREC_REK_SIZ);
    
        // Copy the data from the error directory entry in tealeaves to the 
        //  destination structure.
        //  Commented out lines are just to make it easier to see where the data
        //  is for multi-byte copies, which are done further down.  
        pErrDirEntry->bDeviceID   = pErrDirEntrySource[ 0 ];
        pErrDirEntry->bMode       = pErrDirEntrySource[ 1 ];
        //pErrDirEntry->pErrorTable = pErrDirEntrySource[ 2 thru 5  ] + teaAddress;
        //pErrDirEntry->wTableLen   = pErrDirEntrySource[ 6 - 7 ];
        pErrDirEntry->bStatusSize = pErrDirEntrySource[ 8 ];
        //pErrDirEntry->pStatus   = pErrDirEntrySource[ 9 -10 ] converted to an address;
        //pErrDirEntry->pState    = pErrDirEntrySource[ 11 - 12 ] converted to an address;
        pErrDirEntry->bRecSize    = pErrDirEntrySource[ 13 ];

        // Here we get the multi-byte members:
        //  Copy offset to error table, then convert it to an address.
        EndianAwareCopy( &pErrDirEntry->pErrorTable, 
						 &pErrDirEntrySource[ 2 ], 
						TEA_OFFSET_SIZE ); 
        pErrDirEntry->pErrorTable += teaAddress;
        //  Copy length of error table.
        EndianAwareCopy( &pErrDirEntry->wTableLen, 
						&pErrDirEntrySource[ 6 ], 
						sizeof( pErrDirEntry->wTableLen ) );
        // Convert Status ID to Ptr to status:
        EndianAwareCopy( &wVariableID, &pErrDirEntrySource[ 9 ], sizeof( wVariableID) ); 
        pErrDirEntry->pStatus = bobaVarAddresses[ wVariableID ].addr;
        // Convert State ID to Ptr to state:
        EndianAwareCopy( &wVariableID, &pErrDirEntrySource[ 11 ], sizeof( wVariableID) ); 
        pErrDirEntry->pState = bobaVarAddresses[ wVariableID ].addr;        
    }
    else
    {
        // Index is out of range.
        retVal = FALSE;
    }
    
    return( retVal );
}

// ***************************************************************************
// FUNCTION NAME:       fnTeaFindDeviceError
// PURPOSE: 
//      Starting with a ptr to an Error Directory Entry Structure, get a  
//      pointer to the Error Conversion Table and to the status place, both 
//      referenced by the Entry.  Compare the data in the status place with 
//      each entry in the Error Conversion Table to find a match.
//       If a match is NOT found, return FALSE.
//       If a match IS found...
//       1. Copy the conversion data into the destination structure.
//       2. If the converted error code is zero, return FALSE.
//          If the converted error code is > 0, ...
//          1. Log the error in the System Error Log
//          2. Return TRUE.
//
// INPUTS: 
//      pErrDirEntry    Ptr to error directory entry structure. (source)
//      pErrData        Ptr to error conversion structure. (destination)
//    
// RETURNS:
//      BOOL  - TRUE if value in status place converts to a non-zero error code.
//              FALSE if it doesn't match any entry in the table, or converts
//                      to an error code value of 0.
// NOTES:
// HISTORY:
//  2009.06.11 Clarisa Bellamy - Created to make things simpler.         
//----------------------------------------------------------------------------
BOOL fnTeaFindDeviceError( T_TEA_ERROR_DIR_ENTRY *pErrDirEntry, 
                           T_TEA_ERROR_CONVERTED_DATA *pErrData )
{
    BOOL    retVal = FALSE;     // Actual error (non-zero) found.
    UINT16  wIndex = 0;         // Index into Error Table.
    UINT16  wTableSize;         // Number of entries in this error table.
    UINT8   *pErrorEntry;       // Ptr to entry in Error Table.
    UINT8   bCommandStatSize;   // Size of Command Status from device.
    UINT8   *pCommandStatus;    // Ptr to dCommand Status from device.
    UINT8   byteInx;            // Index into command Status from device.
    UINT8   bRecSize;           // Size of Error Table Entry
    BOOL    errFound;           // Command Status matches Error Table Entry.

    wTableSize = pErrDirEntry->wTableLen;
    bCommandStatSize = pErrDirEntry->bStatusSize;
    pCommandStatus = pErrDirEntry->pStatus;
    bRecSize = pErrDirEntry->bRecSize;
    
    // Start at the beginning of the table.
    pErrorEntry = pErrDirEntry->pErrorTable;

    // For each entry in the Error Table...
    for( wIndex = 0; wIndex < wTableSize; wIndex++ )
    {
        // This may be the one!
        errFound = TRUE;       
        // Check each byte (words are not necessarily on word boundaries.)                                     
        for( byteInx = 0; byteInx < bCommandStatSize; byteInx++ )
        {
            if( pErrorEntry[ byteInx ] != pCommandStatus[ byteInx ] )
            {
                // Found a byte that doesn't match, skip to next entry in 
                //  error table.
                errFound = FALSE;
                break;
            }
        }

        if( errFound == TRUE )
        {
            // Found an error match...
            // Copy Error code and class from the entry.
            pErrData->bErrCode = pErrorEntry[ byteInx++ ];
            pErrData->bErrClass = pErrorEntry[ byteInx++ ];
            pErrData->bOiInfo = pErrorEntry[ byteInx++ ];

            // Log the found error:
            if( pErrData->bErrCode > 0 )
            {
                retVal = TRUE;

                // 01/10/2002 Joseph P. Tokarski - 
                //      Added logging to the System Error Log for
                //      PSDx and PHC Errors....only log non-zero errors.
                switch( pErrData->bErrClass )
                {
                  case ERROR_CLASS_PSD1:
                  case ERROR_CLASS_PSD2:
                  case ERROR_CLASS_PSD3:
                  case ERROR_CLASS_PSD4:
                    // make an entry into the system error log
                    fnLogSystemError( pErrData->bErrClass,
                                      pErrorEntry[0], pErrorEntry[1],
                                      pErrorEntry[2], pErrorEntry[3] );
                    getFlightRecord = TRUE;
                    break;

                  case ERROR_CLASS_PHC:
                    // extract the relevant PHC Status information from the 
                    // most recent phc status
                    fnLogSystemError( pErrData->bErrClass,
                                      phcAllStats[2], phcAllStats[3],
                                      phcAllStats[4], phcAllStats[5] );
                    break;

                  default:
                    // do nothing
                    break;
                }
            }
            // We've found a match, so stop looking.
            break;
        }
        else
        {
            // Haven't found a match yet, keep looking.
            pErrorEntry += bRecSize;    
        }
    }

    return( retVal );    
}


/* **********************************************************************

 FUNCTION NAME:     

 PURPOSE:           
                    
 AUTHOR:            R. Arsenault

 INPUTS:            
 *************************************************************************/
BOOL fnTeaGetItaskReply(ushort scriptID, uchar *replyIDdest, uchar **mapAddressDest) {
    uchar   *directory;
    uchar   *scriptDirectory;
    uchar   *myReplyMapIndex;
    uchar   *aPtr;
    ushort  ashort;
    BOOL    retval;

    retval = TRUE;
    fnTeaGetConstant(TOTEAS, &directory);
    directory += teaAddress;
    EndianAwareCopy(&scriptDirectory, (  char *)(directory) 
									+(  RMSGSTRATEGY * TEA_OFFSET_SIZE), 
										TEA_OFFSET_SIZE);
    scriptDirectory += teaAddress + (SCRIPTDIR_REC_SIZE * scriptID);
    *replyIDdest = scriptDirectory[8];

    EndianAwareCopy(&ashort, &scriptDirectory[6], sizeof(short));

    EndianAwareCopy(&myReplyMapIndex, (  char *)(directory) 
									+(  REPMAPSINDEX * TEA_OFFSET_SIZE), 
										TEA_OFFSET_SIZE);
    myReplyMapIndex += teaAddress + (ITASK_REPLYMAP_REK_SIZE  * ashort);

    EndianAwareCopy(&aPtr, myReplyMapIndex, sizeof(char *));
    aPtr += teaAddress;
    memcpy(mapAddressDest, &aPtr, sizeof(char *));

    return(retval);
}


/* **********************************************************************

 FUNCTION NAME:     

 PURPOSE:           
                    
 AUTHOR:            R. Arsenault

 INPUTS:            
 *************************************************************************/
BOOL    fnTeaGetScriptSiz( UINT16 scriptID, UINT16 *queSizeDest )
{
    UINT8   *directory;
    UINT8   *scriptDirectory;
    BOOL    retval;

    retval = TRUE;
    fnTeaGetConstant(TOTEAS, &directory);
    directory += teaAddress;
    EndianAwareCopy( &scriptDirectory, 
					directory + (RMSGSTRATEGY * TEA_OFFSET_SIZE), 
					TEA_OFFSET_SIZE );
    scriptDirectory += teaAddress + (SCRIPTDIR_REC_SIZE * scriptID);

    EndianAwareCopy( queSizeDest, &scriptDirectory[0], sizeof(short) );

    return(retval);
}


/* **********************************************************************

 FUNCTION NAME:     

 PURPOSE:           
                    
 AUTHOR:            R. Arsenault

 INPUTS:            
 *************************************************************************/
BOOL    fnTeaLoadScriptRec( UINT16 scriptID, UINT16 scriptIndex, T_SCRIPT *recDest )
{
    UINT8   *directory;
    UINT8   *scriptDirectory;
    UINT8   *myScript;
    UINT16  ashort;
    BOOL    retval;

    retval = TRUE;
    fnTeaGetConstant(TOTEAS, &directory);
    directory += teaAddress;
    EndianAwareCopy( &scriptDirectory, 
					directory  +  (RMSGSTRATEGY * TEA_OFFSET_SIZE), 
					TEA_OFFSET_SIZE );
    scriptDirectory += teaAddress + (SCRIPTDIR_REC_SIZE * scriptID);

    EndianAwareCopy( &myScript, &scriptDirectory[2], sizeof(char *) );
    myScript += teaAddress + (SCRIPT_REC_SIZE * scriptIndex);

    EndianAwareCopy( &ashort, &myScript[0], sizeof(ashort) );
    recDest->Id = ashort;
    recDest->dest = myScript[2];
    EndianAwareCopy( &ashort, &myScript[3], sizeof(ashort) );
    recDest->preOp = (void *)(funcCrossRef[ashort]);
    EndianAwareCopy( &ashort, &myScript[5], sizeof(ashort) );
    recDest->postOp = (void *)(funcCrossRef[ashort]);

    return(retval);
}
    

// ****************************************************************************
// FUNCTION NAME:         fnTeaGetTable 
// DESCRIPTION: 
//    Given a Table ID, returns a pointer to the table in tealeavs.
// INPUTS:  
//    tableName - Index into the tealeavs table directory:
//                defined in tealeavs.h:  enum teaDirectItems  
// OUTPUTS:
//    RETURNS:    Pointer to requested table in tealeavs.
// 
// HISTORY:   
// 2009.06.16 Clarisa Bellamy -Add function header.
// AUTHOR:            R. Arsenault
//----------------------------------------------------------------------------
void *fnTeaGetTable( uchar tableName ) 
{
    uchar       *myDirectory;
    uchar       *myTable;

    fnTeaGetConstant( TOTEAS, &myDirectory );
    myDirectory += teaAddress;

    EndianAwareCopy( &myTable,
            myDirectory + (tableName * TEA_OFFSET_SIZE), 
            TEA_OFFSET_SIZE);
    myTable += teaAddress;

    return( myTable );
}

// ****************************************************************************
// FUNCTION NAME:         fnTeaGetErrDirRecord 
// DESCRIPTION: 
//    Finds the Entry in the tealeavs Error Directory with the matching  
//    deviceID and error mode, and returns a pointer to the Entry.
// INPUTS:  
//    deviceID - The device ID to match.  Values are in bob.h (For example, IPSD.)
//    errMode  - Error Mode to match.  If ANY_ERR_MODE, match any.  Values  
//               are in orbit.h: STD_ERRS, BOB_INTERNAL, ANY_ERR_MODE.
// OUTPUTS:
//    RETURNS:    Pointer to first byte of matching Entry in Error Directory.
//                NULL_PTR if no match found.
// 
// HISTORY:   
// 2009.06.16 Clarisa Bellamy - Change variable names to match data, add 
//                            comments and function header.
// AUTHOR:            R. Arsenault
//----------------------------------------------------------------------------
UINT8   *fnTeaGetErrDirRecord( UINT8 deviceID, UINT8 errMode )
{
    BOOL        foundFlag;
    UINT8       bNumErrDirEntries;
    UINT8       *pMyErrorEntry;
    
    // Get a pointer to the first entry in the Error Directory table.
    pMyErrorEntry = (UINT8 *)(fnTeaGetTable(BOBSERRDIRECTORY));
    foundFlag = FALSE;
    // Get the number of entries in the table.
    fnTeaGetConstant( ERRORGROUPCOUNT, &bNumErrDirEntries );          
    // For each entry, check the deviceID and errMode 
    while( bNumErrDirEntries ) 
    {
        if( pMyErrorEntry[0] == deviceID ) 
        {
            if(   (pMyErrorEntry[1] == errMode) 
               || (errMode == ANY_ERR_MODE) ) 
            {
                // If we find a match, stop looking.
                foundFlag = TRUE;
                break;
            }
        }
        // If this entry does not match, check the next.
        bNumErrDirEntries--;
        pMyErrorEntry += ERR_DIREC_REK_SIZ;
    }

    // If no match found in table, return NULL_PTR;
    if( !foundFlag )
    {
        pMyErrorEntry = NULL_PTR;
    }
    return( pMyErrorEntry );
}

/* process multi-packet reply for PSD gen PSD key command   */
/* and Load Secret Key Message                              */

UINT8   fnGetPsdGenPsdKeyReply( struct msgInfo *messyInfo ) 
{
    UINT8     i;
    UINT16  packetSize;

    for( i=0; i < messyInfo->xferStruc->replyCnt; i++ )  
    {
        edmXferStructPtr->ptrToDeviceReplyData[i] = (messyInfo->xferStruc->pDataRecv[i])+8;          
        memcpy( &packetSize, (messyInfo->xferStruc->pDataRecv[i])+6, sizeof(UINT16) );
        if( i==0 ) 
        {
//not needed any more packetSize += 4;  //Account for the error code byes in the first buffer
            memcpy(edmXferStructPtr->deviceReplyStatus, (messyInfo->xferStruc->pDataRecv[i])+8, 4); 
        }
        edmXferStructPtr->bytesWrittenToDest[i] = packetSize;
    }
    edmXferStructPtr->numberOfDeviceReplies = messyInfo->xferStruc->replyCnt;

    return(1);
}

/* process multi-packet reply for PSD gen key exchange key command after long delay */

UINT8   fnGetPsdGenKeyXchngReply( struct msgInfo *messyInfo ) 
{
    UINT8   i;
    UINT16  packetSize;

    for( i=0; i < messyInfo->xferStruc->replyCnt; i++ )  
    {
        edmXferStructPtr->ptrToDeviceReplyData[i] = (messyInfo->xferStruc->pDataRecv[i])+8;
        memcpy( &packetSize, (messyInfo->xferStruc->pDataRecv[i])+6, sizeof(UINT16) );
        if( i==0 ) 
        {
//not needed any more   packetSize += 4;  //Account for the error code byes in the first buffer
            memcpy(edmXferStructPtr->deviceReplyStatus, (messyInfo->xferStruc->pDataRecv[i])+8, 4); 
        }
        edmXferStructPtr->bytesWrittenToDest[i] = packetSize;
    }
    edmXferStructPtr->numberOfDeviceReplies = messyInfo->xferStruc->replyCnt;
    STAT_EdmCommand = KEYX_COMPLETE;
                                                
    return(1);
}

// These are only used if we have a Myko-style PSD, which we don't on FPHX.
//uchar   USBaccumBuffer[2048+8];
//uchar   *USBaccumBufPtr;
//ushort  USBmessageID;
//ushort  USBaccumCharCnt;
//ushort  USBpacketCnt;

UINT8   fnAccumulateUSBmsg( const struct msgInfo *messyInfo ) 
{
    UINT8   retval;

    retval = BOB_PROCESS_MSG;
// !!!cb No PSD device on the USB.
    return( retval );
}


void fnTeaGetFakeReply( const struct msgInfo *messyInfo ) 
{
    uchar   *myFakeMsgIndex;
    uchar   *fakePtr;
    ushort  fakeMessages;
    
    myFakeMsgIndex = (uchar *)(fnTeaGetTable(BOBSFAKEMSGINDEX));
    fnTeaGetConstant(FAKEMESSAGECOUNT, &fakeMessages);          /* determine the number of error tables and the */
    while(fakeMessages) {
        EndianAwareCopy(&fakePtr, myFakeMsgIndex, 4);
        fakePtr += teaAddress;
        if (fakePtr[0] == messyInfo->xmitBuf[2]) {
            EndianAwareCopy(messyInfo->recBuf, &fakePtr[2], fakePtr[1]);
            messyInfo->xferStruc->replyLen[0] = fakePtr[1];
            break;
        }
        fakeMessages--;
        myFakeMsgIndex += 4;
    }
    return;
}

BOOL    fnIfPHCFID( UINT16 msgID ) 
{
    struct      msgControl controlRec;      // record that cross-references a message's header, appended data, and other info
    BOOL        retstat;

    retstat = FALSE;
    if( fnTeaGetMsgControl(msgID, &controlRec) ) 
    {
        if( controlRec.flags[1] == PHCFID ) 
            retstat = TRUE;
    }
    return(retstat);
}
