/*************************************************************************
*   PROJECT:        Horizon CSD2-3
*	MODULE NAME:	sslinterface.c
*
*	DESCRIPTION:	This file contains interface functions to perform 
*					secure connection based on the supplied url. 
*					
* ----------------------------------------------------------------------
*               Copyright (c) 2017 Pitney Bowes Inc.
*                    37 Executive drive
*                   Danbury, Connecticut  06810
* ----------------------------------------------------------------------
*
*************************************************************************/

#include "sys/stat.h"
#include "posix.h"
#include "datdict.h"    // CMOSSignature
#include "ssl.h"
#include "sslinterface.h"
#include "error-ssl.h"
#include "xmlparse.h"
#include "fnames.h"
#include "remotelog.h"
#include "novadriver.h"
#include "intellilink.h"
#include "nstask.h"
#include "pbptask.h"
#include "cometclock.h"     // SetSystemClock()
#include "flashutil.h"

extern void fnDumpRawLenToSystemLog(char *inStr, int Len);
extern void putString(char *msg, int lineNumber);
extern BOOL validSysClock(void);
extern BOOL strDateTimeToDATETIME(DATETIME *dateTimePtr, char *asciiDatePtr);

extern const unsigned char defaultEncryptedPrivateKey[];
extern const unsigned char defaultServerCert[];
extern const unsigned char defaultServerCertNotBeforeDateTime[];
extern const unsigned char defaultServerCertNotAfterDateTime[];

static char sslLog[SSL_LOG_SIZE];
static void fnDestroySSLConnection(SOCKET_DESCRIPTOR *pSocket);
static int fnCreateSSLConnection(SOCKET_DESCRIPTOR *pSocket);
static int fnVerifySSLCertificate(int iPreVerifyOk, X509_STORE_CTX *ctx);
static int fnLoadCertificatesFromFile(char *pFileName, SSL_DESCRIPTOR * sslDesc);
static void fnGenerateReportableError(int errCode);

#define CERT_DATE_SIZE 16 // concise UTC + null : this may be too short to show time but at least no garbage characters will appear in log

/************************************************************************
Description	: 	This function creates the ssl context and loads the cert list
Parameters	: 	cracked URL
Return Value: 	a pointer to an SSL_DESCRIPTOR structure if successful, otherwise NULL
*************************************************************************/
SSL_DESCRIPTOR * fnCreateSSLContext(CRACKED_URL * pURLInfo)
{
	BOOL ret;
	SSL_DESCRIPTOR * retSSLDesc = NULL;
	int sslret = SSL_SUCCESS;
    char CERTLIST_FILE_NAME[16] = "XXXXCERT.BIN";
    SSL_METHOD *sslMethod = NULL;

    fnDumpStringToSystemLog("fnCreateSSLContext start");

    ret = OSGetMemory ((void **) &retSSLDesc, sizeof(SSL_DESCRIPTOR));
    if (ret == FAILURE)
    {
    	dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Context failed to allocate sufficient memory");
    	fnGenerateReportableError(SSLIF_SSL_OUT_OF_MEMORY);
    	return NULL;
    }
	memset((void*)retSSLDesc, 0, sizeof(SSL_DESCRIPTOR));

	/* Initialize the certificate list file name.  The
    file name depends on the PCN of the UIC.  The
    file name is defined with the first 4 characters allocated
    for the PCN. */
    memcpy((void*)CERTLIST_FILE_NAME, (void*)CMOSSignature.bUicPcn, 4);

	/* create client method */
	sslMethod = TLSv1_2_client_method();
    if (sslMethod == NULL)
    {
    	dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Context failed to create TLS client method");
        (void) fnDestroySSLContext(retSSLDesc);
    	fnGenerateReportableError(SSLIF_TLS_CLIENT_METHOD_ERR);
    	return NULL;
    }

	retSSLDesc->sslContext = SSL_CTX_new(sslMethod);
    if (retSSLDesc->sslContext == NULL)
    {
    	dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Context failed to create SSL Context");
        (void) fnDestroySSLContext(retSSLDesc);
    	fnGenerateReportableError(SSLIF_UNABLE_TO_SETUP_SSL_CTX_ERR);
    	return NULL;
    }

    SSL_CTX_set_verify(retSSLDesc->sslContext, SSL_VERIFY_PEER, fnVerifySSLCertificate);
    // WolfSSL does not support SSL_CTX_set_info_callback: the function exists but does nothing
    // SSL_CTX_set_info_callback(sslContext, (void*)fnSSLInfoCallbackFunction);
    sslret = wolfSSL_CTX_UseSNI(retSSLDesc->sslContext, WOLFSSL_SNI_HOST_NAME, pURLInfo->host, strlen(pURLInfo->host));
    if (sslret != SSL_SUCCESS)
    {
		dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Context Use SNI failure: %d\n", sslret);
        (void) fnDestroySSLContext(retSSLDesc);
    	fnGenerateReportableError(SSLIF_TLS_USE_SNI_ERR);
    	return NULL;
    }

    //TODO - determine proper file location - for now it is in root dir
    sslret = fnLoadCertificatesFromFile(CERTLIST_FILE_NAME, retSSLDesc);
    if (sslret != SSLIF_SUCCESS)
    {
    	dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Context failed to load certificates: %d", sslret);
        (void) fnDestroySSLContext(retSSLDesc);
    	fnGenerateReportableError(sslret);
    	return NULL;
    }

    // check allowed cipher list
    if ( retSSLDesc->certListDesc.allowedCipherCount == 0 )
    {
    	dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Context failed to load any allowed cipher suites");
        (void) fnDestroySSLContext(retSSLDesc);
    	fnGenerateReportableError(SSLIF_NO_ALLOWED_CIPHERS);
    	return NULL;
    }

	sslret = wolfSSL_CTX_set_cipher_list(retSSLDesc->sslContext, (const char*) &(retSSLDesc->certListDesc.allowedCipherList[0]));
	if (sslret != SSL_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Context setting cipher list: %d\n", sslret);
        (void) fnDestroySSLContext(retSSLDesc);
    	fnGenerateReportableError(SSLIF_SET_CIPHERS_ERR);
    	return NULL;
	}

	dbgTrace(DBG_LVL_INFO,"Loaded into SSL Context cipher list: %s\n", &(retSSLDesc->certListDesc.allowedCipherList[0]));

	fnDumpStringToSystemLog("fnCreateSSLContext End");
    return retSSLDesc;
}

/************************************************************************
Description	: 	This function deletes the ssl context, deallocating memory as necessary
Parameters	: 	pointer to SSL_DESCRIPTOR structure
Return Value: 	integer indicating status
*************************************************************************/
int fnDestroySSLContext(SSL_DESCRIPTOR * sslDesc)
{
    if (sslDesc == NULL)
    {
    	return SSLIF_SUCCESS;
    }

    if (sslDesc->sslContext != NULL)
    {
        SSL_CTX_free(sslDesc->sslContext);
    }

    (void) OSReleaseMemory(sslDesc);

	return SSLIF_SUCCESS;

}

/************************************************************************
Description	: 	This function generates an error that is reported
Parameters	: 	SSLIF error code
Return Value: 	NULL, indicating error
*************************************************************************/
static void fnGenerateReportableError(int errCode)
{
	int pbpsErrorCode;

	// Convert error code into PBPS error code so that it can be reported to tablet and user
	switch (errCode)
	{
		case SSLIF_UNKNOWNURL_ERR:
			pbpsErrorCode = PBPS_UNKNOWNURL_ERR;
			break;
		case SSLIF_SOCKET_CREATE_ERR:
			pbpsErrorCode = PBPS_SOCKETSEND_ERR;
			break;
		case SSLIF_UNABLE_CONNECT_ERR:
			pbpsErrorCode = PBPS_UNABLE_CONNECT;
			break;
		case SSLIF_CERT_VERIFY_ERR:
			pbpsErrorCode = PBPS_SSL_VERIFY_CERT_ERROR;
			break;
		case SSLIF_CONNECT_UNTRUSTED_HOST_ERR:
			pbpsErrorCode = PBPS_CONNECT_UNTRUSTED_HOST_ERR;
			break;
		case SSLIF_CERT_LIST_ERR:
			pbpsErrorCode = PBPS_CERT_LIST_ERROR;
			break;
		case SSLIF_SSL_OUT_OF_MEMORY:
			pbpsErrorCode = PBPS_SSL_OUT_OF_MEMORY;
			break;
		case SSLIF_INVALID_BASE64_ENCODE_ERR:
			pbpsErrorCode = PBPS_CERT_LIST_BASE64_ERROR;
			break;
		case SSLIF_CERT_LIST_FILE_READ_ERR:
			pbpsErrorCode = PBPS_CERT_LIST_FILE_READ_ERROR;
			break;
		case SSLIF_CERT_LIST_FILE_SEEK_ERR:
			pbpsErrorCode = PBPS_CERT_LIST_FILE_SEEK_ERROR;
			break;
		case SSLIF_CERT_LIST_FILE_OPEN_ERR:
			pbpsErrorCode = PBPS_CERT_LIST_FILE_OPEN_ERROR;
			break;
		case SSLIF_CERT_LIST_FILE_SIG_VERIFY_ERR:
			pbpsErrorCode = PBPS_CERT_LIST_FILE_VERIFY_ERROR;
			break;
		case SSLIF_UNABLE_TO_SETUP_SSL_CTX_ERR:
			pbpsErrorCode = PBPS_SSL_CONTEXT_CREATE_ERROR;
			break;
		case SSLIF_XML_PARSER_ERR:
			pbpsErrorCode = PBPS_PARSE_ERR;
			break;
		case SSLIF_CRACK_URL_ERR:
			pbpsErrorCode = PBPS_UNKNOWNURL_ERR;
			break;
		case SSLIF_HOST_LOOKUP_ERR:
			pbpsErrorCode = PBPS_HOST_LOOKUP_ERROR;
			break;
		case SSLIF_CERT_LOAD_ERR:
			pbpsErrorCode = PBPS_CERT_LOAD_ERROR;
			break;
		case SSLIF_TLS_CLIENT_METHOD_ERR:
			pbpsErrorCode = PBPS_TLS_CLIENT_METHOD_ERROR;
			break;
		case SSLIF_TLS_USE_SNI_ERR:
			pbpsErrorCode = PBPS_TLS_USE_SNI_ERROR;
			break;
		case SSLIF_NO_ALLOWED_CIPHERS:
			pbpsErrorCode = PBPS_NO_ALLOWED_CIPHERS;
			break;
		case SSLIF_SET_CIPHERS_ERR:
			pbpsErrorCode = PBPS_SET_CIPHERS_ERROR;
			break;
		case SSLIF_CERT_LIST_FILE_UNAVAILABLE:
			pbpsErrorCode = PBPS_CERT_LIST_FILE_UNAVAILABLE;
			break;
		case SSLIF_GEN_FAILURE:
		case SSLIF_INVALID_INPUT:
		default:
			pbpsErrorCode = PBPS_GENERAL_FAILURE;
			break;
	}

	fnPBPSLogError(pbpsErrorCode);

}

/************************************************************************
Description	: 	This function does an SSL socket read
 	 	 	 	It changes socket to be non-blocking during read to avoid blocking forever
Parameters	: 	SSL object, read buffer
Return Value: 	number of bytes received, or negative value if error, or 0 if no data
*************************************************************************/
int fnSSLRead(SOCKET_DESCRIPTOR *pSocket, char *buffer, int bufsize)
{
	int readCount;
	int sslErr;
    STATUS osStat;
    int retval;
    int sockfd;
    SSL *sslConn;

    sockfd = pSocket->iSocketID;
    sslConn = pSocket->sslConnection;

	// change socket to non-blocking
	osStat = NU_Fcntl(sockfd, NU_SETFLAG, NU_NO_BLOCK);
	if (osStat != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR,"Error: SSL Read - failed to change to non-blocking: %d\n", osStat);
		return osStat;
	}

	readCount = SSL_read(sslConn, buffer, bufsize);
	sslErr = SSL_get_error(sslConn, readCount);
	switch (sslErr)
	{
		case SSL_ERROR_NONE:
			retval = readCount;
			break;

		case SSL_ERROR_WANT_READ:
			retval = 0;
			break;
		default:
			// Error so return value
			dbgTrace(DBG_LVL_ERROR,"Error: SSL Read - read error: %d\n", sslErr);
			retval = sslErr;
			break;
	}

	// change socket back to blocking
	osStat = NU_Fcntl(sockfd, NU_SETFLAG, NU_BLOCK);
	if (osStat != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR,"Error: SSL Read - failed to change back to blocking: %d\n", osStat);
		// don't change return value and keep going
	}

		return retval;
}

/************************************************************************
Description	: 	Connects to SSL enabled socket
 	 	 	 	Create SSL structure, Read BIO and Write BIO
 	 	 	 	Set up domain name checking
 	 	 	 	Finally do SSL connect
Parameters	: 	socket descriptor - ASSUMED TO BE VALID!
Return Value: 	boolean indicating success or failure
*************************************************************************/
BOOL fnSSLConnect(SOCKET_DESCRIPTOR *pSocket)
{
	BOOL retval = FALSE;
	int sslstat;

	pSocket->sslConnection = SSL_new(pSocket->sslDesc->sslContext);
	if (pSocket->sslConnection == NULL)
	{
		dbgTrace(DBG_LVL_ERROR,"Error: SSL structure creation failure\n");
		return retval;
	}

	//BIO based IO
	pSocket->sslWriteBIO = BIO_new_socket(pSocket->iSocketID, BIO_NOCLOSE);
	if (pSocket->sslWriteBIO == NULL)
	{
		dbgTrace(DBG_LVL_ERROR,"Error: SSL write BIO creation failure\n");
		return retval;
	}
	pSocket->sslReadBIO = BIO_new_socket(pSocket->iSocketID, BIO_NOCLOSE);
	if (pSocket->sslReadBIO == NULL)
	{
		dbgTrace(DBG_LVL_ERROR,"Error: SSL read BIO creation failure\n");
		return retval;
	}
	SSL_set_bio(pSocket->sslConnection, pSocket->sslReadBIO, pSocket->sslWriteBIO);

	// enable domain name checking
	sslstat = wolfSSL_check_domain_name(pSocket->sslConnection, (const char *) pSocket->urlInfo.host);
	if (sslstat != SSL_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR,"Error: SSL domain name checking setup failure: %d\n", sslstat);
		return retval;
	}

	pSocket->sslConnectStatus = SSL_connect(pSocket->sslConnection);
	if (pSocket->sslConnectStatus != SSL_SUCCESS)
	{
		sslstat = SSL_get_error(pSocket->sslConnection, pSocket->sslConnectStatus);
		dbgTrace(DBG_LVL_ERROR,"Error: SSL Connect failure: %d\n", sslstat);
		return retval;
	}
	else
	{
		retval = TRUE;
	}
	return retval;
}

/************************************************************************
Function	: 	fnInitializeSocket
Author		: 	Derek DeGennaro/Sam Thillaikumaran
Description	: 	This function initializes the a socket descriptor.
Parameters	: 	pSocket - Socket descriptor used to communicate with the
				server.
Return Value: 	SSLIF_SUCCESS if successfull, otherwise one of the error
				codes defined in sslinterface.h.
*************************************************************************/ 
int fnInitializeSocket(SOCKET_DESCRIPTOR *pSocket)
{
	if (pSocket == NULL)
        return SSLIF_INVALID_INPUT;
		
    pSocket->iSocketID = -1;
    memset((void*)&pSocket->urlInfo,0,sizeof(pSocket->urlInfo));
    memset((void*)&pSocket->Address,0,sizeof(pSocket->Address));

    memset((void*)&pSocket->serverURL,0,sizeof(pSocket->serverURL));
    pSocket->fUseSSL=FALSE;
    pSocket->fUseProxy=FALSE;

    pSocket->sslConnection=NULL;
    pSocket->sslWriteBIO=NULL;
    pSocket->sslReadBIO=NULL;
    pSocket->sslConnectStatus=0;
    memset((void*)pSocket->bCertificateBuff,0,sizeof(pSocket->bCertificateBuff));

    return SSLIF_SUCCESS;
}

/************************************************************************
Function	:	fnGetServerAddress
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function takes a service request and determines the
				necessary information regarding	the server that will
				perform the service. This information includes the fully
				decoded URL, the IP address, and the port number.
Parameters	:	pSocket - The socket descriptor that will be used to
				connect to the server. This function fills in the
				descriptor	with the parsed url information (protocol,
				host,port,path), the requested url, and the resolved ip
				address of the destination server.
Return Value:	SSLIF_SUCCESS - no errors
				SSLIF_INVALID_INPUT - invalid data passed to it
				SSLIF_CRACK_URL_ERR - Falied to crack the URL into its
				                      individual parts.
				SSLIF_HOST_LOOKUP_ERR - Falied DNS lookup of the host name
										(no IP address available).

*************************************************************************/
int fnGetServerAddress(SOCKET_DESCRIPTOR *pSocket, char *pUrl, char *pAddrName, unsigned char ucConnection)
{
    char *pHostAddrList=NULL;

	NU_HOSTENT hostEnt;
    STATUS      nu_err;
    char pLogBuf[200];
    unsigned char bHostIpAddr[4];

    
    if(pUrl == NULL || pSocket == NULL)	
	{
        /* Invalid Parameters. */
        (void)sprintf(pLogBuf, "fnGetServerAddress got NULL ptrs");
        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
        return SSLIF_INVALID_INPUT;
	}
	
	/* clear out the host entry struct and address */
	memset((void*)&hostEnt, 0, sizeof(hostEnt));

    hostEnt.h_addr_list = &pHostAddrList;

	// clear out the socket address data
	memset((void*)&pSocket->Address, 0, sizeof(pSocket->Address));
	
	strncpy(pSocket->serverURL, pUrl, URL_BUFFER_LEN);
	pSocket->serverURL[URL_BUFFER_LEN]=0;
		
	(void)sprintf(pLogBuf, "URL = %s", pSocket->serverURL);
	fnDumpStringToSystemLog(pLogBuf);
	
	/* url / ip is given in pMsg */
	if (crackUrl(&pSocket->urlInfo, pUrl) == NU_SUCCESS)
	{
	    if (strlen(pSocket->urlInfo.protocol))
	    {
	        (void)sprintf(pLogBuf, "Original URL Protocol = %s", pSocket->urlInfo.protocol);
	        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
	    }
		else
		{
	        (void)sprintf(pLogBuf, "Original URL Protocol not specified");
	        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
		}

	    if (strlen(pSocket->urlInfo.port))
	    {
	        (void)sprintf(pLogBuf, "Original URL Port = %s", pSocket->urlInfo.port);
	        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
	    }
		else
		{
	        (void)sprintf(pLogBuf, "Original URL Port not specified");
	        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
		}

        /* is the host specified as a name or ip address? */
        if (isIpAddress(pSocket->urlInfo.host) == FALSE)
        {
            /* get IP address using DNS lookup */
            nu_err = NU_Get_Host_By_Name(pSocket->urlInfo.host, &hostEnt);
            if (nu_err == NU_SUCCESS)
            {
                pSocket->Address.id.is_ip_addrs[0] = hostEnt.h_addr[0];
                pSocket->Address.id.is_ip_addrs[1] = hostEnt.h_addr[1];
                pSocket->Address.id.is_ip_addrs[2] = hostEnt.h_addr[2];
                pSocket->Address.id.is_ip_addrs[3] = hostEnt.h_addr[3];
            }
            else
            {
             	(void)sprintf(pLogBuf, "Couldn't resolve URL, NU err = %d", nu_err);
			    fnDumpStringToSystemLog(pLogBuf);
                return SSLIF_HOST_LOOKUP_ERR;
            }               
        }
        else
        {
            /* IP address is present in the url */
            dottedQuadToArray((unsigned char *)bHostIpAddr, pSocket->urlInfo.host);
            pHostAddrList = (char *)bHostIpAddr;
            pSocket->Address.id.is_ip_addrs[0] = hostEnt.h_addr[0];
            pSocket->Address.id.is_ip_addrs[1] = hostEnt.h_addr[1];
            pSocket->Address.id.is_ip_addrs[2] = hostEnt.h_addr[2];
            pSocket->Address.id.is_ip_addrs[3] = hostEnt.h_addr[3];
        }
	}
	else
	{
        (void)sprintf(pLogBuf, "Couldn't crack the URL for some unknown reason");
        fnDumpStringToSystemLog(pLogBuf);
	    return SSLIF_CRACK_URL_ERR;
	}

    (void)sprintf(pLogBuf, "DLA Host or File Server IP is %d.%d.%d.%d", 
			        (unsigned char)hostEnt.h_addr[0], (unsigned char)hostEnt.h_addr[1], 
			        (unsigned char)hostEnt.h_addr[2], (unsigned char)hostEnt.h_addr[3]);
	fnDumpStringToSystemLog(pLogBuf);

	/* set some common socket stuff */  
	pSocket->Address.family = NU_FAMILY_IP;
	pSocket->Address.name = pAddrName;		// the ".name" element stores the pointer to the name

#ifdef ALLOW_NON_SSL
	if (strlen(pSocket->urlInfo.protocol) > 0)
	{
        /* The distributor or DLA specified the protocol.
        	The protocol determines if SSL is used.  */
        if (strncmp(pSocket->urlInfo.protocol,"https",5) == 0)
        {
            pSocket->fUseSSL = TRUE;
        }
        else if (strncmp(pSocket->urlInfo.protocol,"http",4) == 0)
        {
            pSocket->fUseSSL = FALSE;
        }
        else
        {
            /* The distributor or DLA specified an unknown protocol.
            	Set the protocol to the default HTTP - don't use SSL. */
            (void)strcpy(pSocket->urlInfo.protocol,"http://");
            pSocket->fUseSSL = FALSE;
        }               
    }
	else
	{
        /* The distributor or DLA did not specify the protocol.
			By default SSL is not used. */
        (void)strcpy(pSocket->urlInfo.protocol,"http://");
        pSocket->fUseSSL = FALSE;
	}
#else
        pSocket->fUseSSL = TRUE;
        (void)strcpy(pSocket->urlInfo.protocol,"https://");
#endif

	if (strlen(pSocket->urlInfo.port) > 0)
	{
        /* The distributor or DLA specified the port.  */
        pSocket->Address.port = (unsigned short)atoi(pSocket->urlInfo.port);
	}
	else
	{
        /* The distributor or DLA did not specified the port.
        	The protocol determines the port. */
        if (pSocket->fUseSSL == TRUE)
		{
            pSocket->Address.port = 443;
        }
        else
        {
            pSocket->Address.port = 80;
        }
    }
	
	if (fnDoWeNeedToUseProxy(pSocket->urlInfo.protocol, ucConnection))	
	{
		pSocket->fUseProxy = 1;

        switch (fnSetupProxySocket(&pSocket->Address, pSocket->urlInfo.protocol))
        {
            case PROXY_SOCKET_ERR_CRACK_URL:
                return SSLIF_CRACK_URL_ERR;
            case PROXY_SOCKET_ERR_RESOLVE_HOST:
                return SSLIF_HOST_LOOKUP_ERR;
			default:
				break;
        }
	}

    if (pSocket->fUseSSL == TRUE)
    {
        (void)sprintf(pLogBuf, "Using SSL, Protocol = %s", pSocket->urlInfo.protocol);
        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
        (void)sprintf(pLogBuf, "Using SSL, Port = %u", pSocket->Address.port);
        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
    }
    else
    {
        (void)sprintf(pLogBuf, "Not using SSL, Protocol = %s", pSocket->urlInfo.protocol);
        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
        (void)sprintf(pLogBuf, "Not using SSL, Port = %u", pSocket->Address.port);
        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
    }
	
	return SSLIF_SUCCESS;
}

/************************************************************************
Function	:	fnCreateSocket
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function creates a handle to a socket descriptor by
				calling the network library socket API.
Parameters	:	pSocket - Socket descriptor used to communicate with the
				server.
Return Value:	SSLIF_SUCCESS if successfull, otherwise an error.
*************************************************************************/
int fnCreateSocket(SOCKET_DESCRIPTOR *pSocket)
{   
    int iReturnValue;
    char pLogBuf[200];


    if (pSocket == NULL)
        return SSLIF_INVALID_INPUT;

    /* create socket handle using NU_Socket */
    pSocket->iSocketID = NU_Socket(NU_FAMILY_IP, NU_TYPE_STREAM, NU_NONE);
    if (pSocket->iSocketID >= 0)
    {
        iReturnValue = SSLIF_SUCCESS;
    }
    else
    {
        sprintf(pLogBuf, "NU_Socket() ERR# %d", pSocket->iSocketID);
        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
        iReturnValue = SSLIF_SOCKET_CREATE_ERR;
    }

    return iReturnValue;    
}

/************************************************************************
Function	:	fnSocketSend
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function sends a series of bytes to the specified
				server. This function is a wrapper for the underlying
				network layer. If the UIC is using normall HTTP this
				function calls the network library socket API to send the
				data. If the UIC is using HTTP over SSL this function calls
				the SSL API to send the data.
Parameters	:	pSocket - The descriptor of the Socket being used to 
				communicate	with the server.
				pBuff - The buffer containig the data to be sent to the 
				server.
				iLength - The number of bytes in pBuff.
Return Value:	The number of bytes sent. If there is an error then this
				will not equal iLength.
*************************************************************************/
int fnSocketSend(SOCKET_DESCRIPTOR *pSocket, char *pBuff, int iLength)
{
    int iNucStatus, iSent = 0, iLen;


    if (pSocket == NULL || pBuff == NULL || iLength == 0)
        return 0;

    while (iSent < iLength)
    {
        if (pSocket->fUseSSL == FALSE)
            iNucStatus = NU_Send(pSocket->iSocketID, pBuff + iSent, (unsigned short)(iLength - iSent), 0);
        else
            iNucStatus = SSL_write(pSocket->sslConnection, (char*)(pBuff + iSent), iLength - iSent);

        if (fnGetNOVALoggingState())
        {
            if (iNucStatus > 0)
            {
                strcpy( sslLog, "Socket SENT:" );
//	            if (remoteLoggingIsOn())
//	                remoteLog( sslLog, NULL);
//	            else
	                fnDumpStringToSystemLog( sslLog );

	        	iLen = (iNucStatus < SSL_LOG_SIZE - 1 ? iNucStatus : SSL_LOG_SIZE - 1);
                memcpy( sslLog, (pBuff+iSent), iLen );
                sslLog[iLen] = 0;
            }
            else
            {
            	sprintf( sslLog, "Socket SENT: NU_Send Error=%d", iNucStatus );
            }

//            if (remoteLoggingIsOn())
//                remoteLog( sslLog, NULL);
//            else
                fnDumpStringToSystemLog( sslLog );
        }

        if (iNucStatus > 0)
            iSent += iNucStatus;
        else
            return iSent;
    }

    return iSent;
}

/************************************************************************
Function	:	fnSocketReceive
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function gets a series of bytes from the server.
				This function is a wrapper for the underlying network layer.
				If the UIC is using normall HTTP this function calls the
				network library socket API to send the data.  If the 
				UIC is using HTTP over SSL this function calls the SSL
				API to send the data.
Parameters	:	pSocket - The descriptor of the Socket being used to 
				communicate with the server.
				pBuff - The buffer to hold the incoming data
				iLength - The size (in bytes) of pBuff.
Return Value:	The number of bytes received (zero or greater is successfull
				less than zero is an error).
*************************************************************************/
int fnSocketReceive(SOCKET_DESCRIPTOR *pSocket, char *pBuff, int iLength)
{
    int iCount, iLen = 0;   


    if (pSocket == NULL || pBuff == NULL || iLength == 0)
        return 0;

    memset( pBuff, 0, iLength );
        
    if (pSocket->fUseSSL == FALSE)
        iCount = NU_Recv(pSocket->iSocketID, pBuff, (unsigned short)iLength, 0);
    else
        iCount = fnSSLRead(pSocket, pBuff, iLength);

    if (fnGetNOVALoggingState())
    {
		if (iCount > 0)
	    {
	        strcpy( sslLog, "Socket GOT:" );
//            if (remoteLoggingIsOn())
//                remoteLog( sslLog, NULL);
//            else
                fnDumpStringToSystemLog( sslLog );

	        iLen = (iCount < SSL_LOG_SIZE - 1 ? iCount : SSL_LOG_SIZE - 1);
            memcpy( sslLog, pBuff, iLen );
	        sslLog[iLen] = 0;
	    }
	    else
	    {
			if (iCount != NU_NOT_CONNECTED)
			{
		        sprintf( sslLog, "Socket GOT: NU_Recv Error=%d", iCount );
				iLen = strlen(sslLog);
			}
			else
			{
		        sprintf( sslLog, "Socket GOT: Connection closed (%d)", iCount );
				iLen = strlen(sslLog);
			}
	    }
		
//        if (remoteLoggingIsOn())
//            remoteLog( sslLog, NULL);
//        else
		{
            fnDumpStringToSystemLog( sslLog );
			if (strlen(sslLog) != iLen)
			{
				fnDumpRawLenToSystemLog(sslLog, iLen);
			}
		}
	}

    if (SSL_get_shutdown(pSocket->sslConnection) == SSL_RECEIVED_SHUTDOWN )
    {
        fnDumpStringToSystemLog("fnSocketReceive(): Alert received SSL_RECEIVED_SHUTDOWN flag.");
	}
	return iCount;
}

/************************************************************************
Function	:	fnDestroySocket
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function is called to release any socket resources
				after the UIC has disconnected from the server.
				If the UIC is using HTTP over SSL then this function
				releases the SSL resources as well.
Parameters	:	pSocket - The socket descriptor that needs to be freed.
Return Value:	None
*************************************************************************/
void fnDestroySocket(SOCKET_DESCRIPTOR *pSocket)
{
    if (pSocket == NULL)
        return;

    fnDestroySSLConnection(pSocket);        

    if (pSocket->iSocketID >= 0)
    {
	    if (fnGetNOVALoggingState())
	    {
			sprintf(sslLog, "call NU_Close_Socket");
		    fnDumpStringToSystemLog( sslLog );
		}
		 
        /* A socket was created. Release it. */
        (void)NU_Close_Socket(pSocket->iSocketID); 
        pSocket->iSocketID = -1;
    }
	 
    if (fnGetNOVALoggingState())
    {
		sprintf(sslLog, "call fnDestroySocket ended");
	    fnDumpStringToSystemLog( sslLog );
	}
}

/************************************************************************
Function	:	fnDestroySSLConnection
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function releases any SSL resources after the UIC 
				disconnects from the server.
Parameters	:	pSocket - The descriptor of the socket that was used to
				communicate with the server.
Return Value:	None 
*************************************************************************/
static void fnDestroySSLConnection(SOCKET_DESCRIPTOR *pSocket)
{   
    if (pSocket == NULL)
        return; 
        
	if (pSocket->sslConnection != NULL)
    {
		//Free the connection
	    if (fnGetNOVALoggingState())
	    {
			sprintf(sslLog, "call SSL_shutdown");
		    fnDumpStringToSystemLog( sslLog );
		}
		 
		SSL_shutdown(pSocket->sslConnection);        
	    if (fnGetNOVALoggingState())
	    {
			sprintf(sslLog, "call SSL_free");
		    fnDumpStringToSystemLog( sslLog );
		}
		 
		SSL_free(pSocket->sslConnection);
    }
	else
	{
		// Free the lio's.
		if (pSocket->sslWriteBIO != NULL)
		{
		    if (fnGetNOVALoggingState())
		    {
				sprintf(sslLog, "call OPENSSL_free(pSocket->sslWriteLIO)");
			    fnDumpStringToSystemLog( sslLog );
			}
		 
	        OPENSSL_free(pSocket->sslWriteBIO);
		}
	
	    if (pSocket->sslReadBIO != NULL)
		{
		    if (fnGetNOVALoggingState())
		    {
				sprintf(sslLog, "call OPENSSL_free(pSocket->sslReadLIO)");
			    fnDumpStringToSystemLog( sslLog );
			}
		 
	        OPENSSL_free(pSocket->sslReadBIO);
		}
	}
	
    // The method is not dynamically allocated.
    pSocket->sslConnection = NULL;
    pSocket->sslWriteBIO = NULL;
    pSocket->sslReadBIO = NULL;
    pSocket->sslConnectStatus = 0;

	if (pSocket->sslDesc != NULL)
	{
		(void) fnDestroySSLContext(pSocket->sslDesc);
	}

}

/************************************************************************
Function	:	fnConnectSocket
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function makes a connection to a server. If HTTP
				over SSL is used, then the function makes the SSL connection.
Parameters	:	pSocket - Socket descriptor for the socket. The descriptor
				contains parsed url info (protocol,host,port,and path), 
				the Network Library socket descriptor handle, and the 
				ip address/port of the Network Library socket. If using
				SSL, the descriptor also includes all the necessary socket
				information.
Return Value:	SSLIF_SUCCESS if successfull, an error otherwise.
*************************************************************************/
int fnConnectSocket(SOCKET_DESCRIPTOR *pSocket, INT16 socketBlockFlag, UINT8 ubConnectionType)
{
    STATUS iSocketStatus = NU_SUCCESS;


    if (pSocket != NULL)
    {
        /* connect the socket to the server using NU_Connect */
        iSocketStatus = NU_Connect(pSocket->iSocketID, &pSocket->Address,0);
        if (iSocketStatus >= 0)
        {
            /* set the socket to block. */
            if (NU_Fcntl(pSocket->iSocketID, NU_SETFLAG, socketBlockFlag) == NU_SUCCESS)
            {               
                if (pSocket->fUseSSL == FALSE)
                {
                    /* The socket has been connected and the UIC is using normal HTTP. */
                    return SSLIF_SUCCESS;
                }
                else
                {
                    /* The socket has been connected and the UIC is using HTTP over SSL (HTTPS). */
                    return fnCreateSSLConnection(pSocket);
                }                                   
            }
            else
            {   
				sprintf(sslLog, "Socket block error %d", SSLIF_UNABLE_CONNECT_ERR);
				putString(sslLog, __LINE__);
                return SSLIF_UNABLE_CONNECT_ERR;
            }
        }
        else
        {
			sprintf(sslLog, "Connect error %d", SSLIF_UNABLE_CONNECT_ERR);
			putString(sslLog, __LINE__);
            return SSLIF_UNABLE_CONNECT_ERR;
	    }
    }
    else
    {
        return SSLIF_INVALID_INPUT;
    }
}

/************************************************************************
Function	:	fnCreateSSLConnection
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function takes a socket descriptor and uses it to
				create an SSL connection to the server. If this function
				succeeds the UIC is securely connected to the server and
				can begin to exchange secure messages using HTTP over SSL.
Parameters	:	pSocket - Descriptor of the socket used to communicate
				with the server.
Return Value:	SSLIF_SUCCESS if successfull, an error otherwise.
*************************************************************************/
static int fnCreateSSLConnection(SOCKET_DESCRIPTOR *pSocket)
{
	int iStatus = SSLIF_UNABLE_CONNECT_ERR;

    if (pSocket->iSocketID >= 0)
    {
		if (fnGetNOVALoggingState())
		{
   		    fnDumpStringToSystemLog("fnCreateSSLConnection start");
		}

		// set up the SSL Context
		pSocket->sslDesc = fnCreateSSLContext(&(pSocket->urlInfo));
		if (pSocket->sslDesc != NULL)
		{
			if (fnSSLConnect(pSocket))
			{
				iStatus = SSLIF_SUCCESS;
			}
	    }
    }
    else
    {
        iStatus = SSLIF_INVALID_INPUT;
		sprintf(sslLog, "ssl connect error %d", iStatus);
		putString(sslLog, __LINE__);
    }

	if (fnGetNOVALoggingState())
	{
    	fnDumpStringToSystemLog("fnCreateSSLConnection End");
	}
	
    return iStatus;
}

/************************************
    SSL/Certificate functions
************************************/
/************************************************************************
Function	:	fnVerifySSLCertificate
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function is called by the SSL library after it has
				verified (either successfully or un-successfully) a
				certificate. If the function returns 1 then the SSL handshake
				continues regardless of the error. If the function returns 0 
				then the SSL handshake stops.
Parameters	:	iPreVerifyOk - Status of the previous certificate validation
				performed by the SSL library. Equals 1 if the certificate
				was verified successfully, otherwise 0.  The error that caused
				the failure can be extracted from the ctx parameter and the
				SSL library.
				ctx - A certificate store context. From this context it
				is possbile to get lots of information including the
				certificate that has been verified or failed verification.
Return Value:	1 If the certificate is ok and the SSL handshake can continue.
				0 If there is a problem and the UIC should not be allowed to
				connect to the server.
**********************************************************/
static int fnVerifySSLCertificate(int iPreVerifyOk, X509_STORE_CTX *ctx)
{
    int iStatus = 0;
    char tempDate[CERT_DATE_SIZE];

    if (CMOSDiagnostics.fOOBComplete == TRUE)
    {
        /* The UIC is not in OOB mode any more.  Set the status
        based on the SSL Library's findings. */
        if (iPreVerifyOk == 1)
            iStatus = 1;
    }
    else
    {
        /* The UIC is still in Out Of Box mode.  Don't bother
        checking the certificate, just let the SSL connection
        continue. */
        iStatus = 1;
    }
    
    if (iStatus == 0)
    {
		dbgTrace(DBG_LVL_ERROR,"Error: SSL cert verify: %d, domain: %s, error: %d",
				iPreVerifyOk,
				(ctx != NULL ? ctx->domain : "context null"),
				(ctx != NULL ? ctx->error : 0));
    }

    WOLFSSL_X509 *tempX509 = NULL;

    tempX509 = ctx->current_cert;
	if (tempX509 != NULL)
	{// log info on bad cert
	    char *issuer = wolfSSL_X509_NAME_oneline(wolfSSL_X509_get_issuer_name(tempX509), 0, 0);
	    char *subject = wolfSSL_X509_NAME_oneline(wolfSSL_X509_get_subject_name(tempX509), 0, 0);

	    dbgTrace(DBG_LVL_INFO,"Cert Issuer: %s\n", issuer);
		dbgTrace(DBG_LVL_INFO,"Cert Subject: %s\n", subject);

		memset(tempDate, 0, sizeof(tempDate));
		strncpy(tempDate, (const char *) wolfSSL_X509_notBefore(tempX509), CERT_DATE_SIZE - 1);
		dbgTrace(DBG_LVL_INFO,"Cert Valid From: %s\n", tempDate);

		strncpy(tempDate, (const char *) wolfSSL_X509_notAfter(tempX509), CERT_DATE_SIZE - 1);
		dbgTrace(DBG_LVL_INFO,"Cert Valid To: %s\n", tempDate);
	}

	if (CMOSNetworkConfig.bEnableSSLCertVerify)
	{
	    return iStatus;
	}
	else
	{//ignore errors
	    return 1;
	}
}

/************************************************************************
Function	:	fnSSLInfoCallbackFunction
Description	:	Not needed if verify certificate function above is used properly,
				moreover this callback is not supported by WolfSSL
**********************************************************/

/************************************************************************
Function	:	fnCertListParseStart
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function is a parser callback function. This function
				is called before parsing a trusted root certificate list file.
				It initializes some data.
Parameters	:	Standard Parser Parameters.
Return Value:	SSLIF_SUCCESS if successfull, otherwise an error.
*************************************************************************/ 
static short fnCertListParseStart(void * context)
{
	SSL_DESCRIPTOR * sslDesc = (SSL_DESCRIPTOR *) context;

    /* Called before starting to parse a certificate list. */
    
    /* Reset the temp certificate list version. */
	sslDesc->certListDesc.iCertListVer = 0;
	sslDesc->certListDesc.certCount = 0;

    /* Reset the number of trusted hosts. */
	sslDesc->certListDesc.lwTrustedHostListEntries = 0;
    
    // Reset allowed cipher list
	sslDesc->certListDesc.curAllowedCipher = &(sslDesc->certListDesc.allowedCipherList[0]);
	sslDesc->certListDesc.allowedCipherCount = 0;

    return SSLIF_SUCCESS;
}

/************************************************************************
Function	:	fnCertListParseEnd
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function is a parser callback function. This function
				is called after parsing a trusted root certificate list file.
				It releases some resources.
Parameters	:	Standard Parser Parameters.
Return Value:	SSLIF_SUCCESS if successfull, otherwise an error.
*************************************************************************/
static short fnCertListParseEnd(void * context)
{
    
    return SSLIF_SUCCESS;
}

/************************************************************************
Function	:	fnCertListSTag2
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function is a parser callback function. This function
				is called when it finds a start tag in the trusted root
				certificate list file.  If the tag is the certificate list
				start tag then the function verifies and stores the version.
				IF the tag is the certificate start tag it verifies the
				format is correct.
Parameters	:	Standard Parser Parameters.
Return Value:	SSLIF_SUCCESS if successfull, otherwise an error .
*************************************************************************/
static short fnCertListSTag2(SSL_DESCRIPTOR * sslDesc, XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    short iCurrAttr = 0, iStatus = SSLIF_SUCCESS;
    BOOL fFoundVersion = FALSE;
    
    if (strcmp((const char*)pName,"CertificateList") == 0)
    {
        /* Look For the Required version attribute. */
        for (iCurrAttr = 0; iCurrAttr < iNumAttr && fFoundVersion == FALSE; iCurrAttr++)
        {
            if (strcmp((const char*)pAttrList[iCurrAttr].pName,"version") == 0)
            {
            	sslDesc->certListDesc.iCertListVer = atoi((const char*)pAttrList[iCurrAttr].pValue);
                fFoundVersion = TRUE;
            }       
        }
        
        if (fFoundVersion == FALSE)
            iStatus = SSLIF_CERT_LIST_ERR;
    }
    
    return iStatus;
}
   
static short fnCertListSTag(void * context, XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    BOOL fCertFormatOk = FALSE;
    short iCurrAttr = 0;
	SSL_DESCRIPTOR * sslDesc = (SSL_DESCRIPTOR *) context;
    
    if (strcmp((char*)pName,"Certificate") == 0)
    {
        /* Make sure the certificate is in the correct format. */
        for (iCurrAttr = 0; iCurrAttr < iNumAttr && fCertFormatOk == FALSE; iCurrAttr++)
        {
            if (strcmp((char*)pAttrList[iCurrAttr].pName,"format") == 0 &&
                strcmp((char*)pAttrList[iCurrAttr].pValue,"DER") == 0)
            {
                /* At this point the UIC only accepts certificates in
                DER format. */
                fCertFormatOk = TRUE;
            }
        }
        
        if (fCertFormatOk == TRUE)
        {
            /* The format attribute is set correctly. */
            return SSLIF_SUCCESS;
        }
        else
        {
            /* Error - certificate is in an unkown format. */
            return SSLIF_CERT_LIST_ERR;
        }
    }
    else if (strcmp((char*)pName,"CertificateList") == 0)
    {
        return fnCertListSTag2(sslDesc, pName,pAttrList,iNumAttr);
    }
    else
    {
        /* unknown element in document, ignore it. */
        return SSLIF_SUCCESS;
    }
}

/************************************************************************
Description	:	Determines if cert loading error from wolfSSL should be ignored;
				Logs information about bad cert
Parameters	:
Return Value:	SSLIF_SUCCESS if successful, otherwise an error.
*************************************************************************/
static short fnHandleCertLoadError(int wolfStatus,  unsigned char *pCertBuff, unsigned long iLen)
{
    short iStatus = SSLIF_SUCCESS;
    WOLFSSL_X509 *tempX509 = NULL;
    char tempDate[CERT_DATE_SIZE];

	if (wolfStatus == ASN_AFTER_DATE_E)
	{
		dbgTrace(DBG_LVL_INFO,"Ignoring expired cert failure: %d\n", wolfStatus);
	}
	else
	{
		dbgTrace(DBG_LVL_ERROR,"Error: Load cert into SSL context failure: %d\n", wolfStatus);
		iStatus = SSLIF_CERT_LOAD_ERR;
	}

	tempX509 = wolfSSL_X509_d2i(&tempX509, pCertBuff, (int) iLen);
	if (tempX509 != NULL)
	{// log info on bad cert
	    char *issuer = wolfSSL_X509_NAME_oneline(wolfSSL_X509_get_issuer_name(tempX509), 0, 0);
	    char *subject = wolfSSL_X509_NAME_oneline(wolfSSL_X509_get_subject_name(tempX509), 0, 0);

	    dbgTrace(DBG_LVL_INFO,"Cert Issuer: %s\n", issuer);
		dbgTrace(DBG_LVL_INFO,"Cert Subject: %s\n", subject);

		memset(tempDate, 0, sizeof(tempDate));
		strncpy(tempDate, (const char *) wolfSSL_X509_notBefore(tempX509), CERT_DATE_SIZE - 1);
		dbgTrace(DBG_LVL_INFO,"Cert Valid From: %s\n", tempDate);

		strncpy(tempDate, (const char *) wolfSSL_X509_notAfter(tempX509), CERT_DATE_SIZE - 1);
		dbgTrace(DBG_LVL_INFO,"Cert Valid To: %s\n", tempDate);
        XFREE(tempX509, NULL, DYNAMIC_TYPE_X509);
	}

	return iStatus;
}

/************************************************************************
Function	:	fnCertListCharData
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function is a parser callback function. This function
				is called when it finds character data in the trusted root
				certificate list file. If the character data is part of
				the certificate element then it is stored in the base 64
				buffer where it is later operated on.
Parameters	:	Standard Parser Parameters.
Return Value:	SSLIF_SUCCESS if successfull, otherwise an error.
*************************************************************************/ 
static short fnCertListCharData(void * context, XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    ConversionResult cr = CONVERSION_OK;
    short iStatus = SSLIF_SUCCESS;
    unsigned long iTmpBase64Len = 0;
    XmlChar *pRealBase64 = pChars;
    unsigned char *pCertBase64Buff;
    unsigned long iCertBase64BuffLen;
    unsigned long iDecodeLen;
    int wolfStatus;
	SSL_DESCRIPTOR * sslDesc = (SSL_DESCRIPTOR *) context;
    
    if (strcmp((char*)pName,"Certificate") == 0)
    {               
        if (pChars != NULL && iLen > 0)
        {                                                   
            /* Remove leading whitespace. */
            while (iLen > 0 && *pRealBase64 != '\0' && isspace((int)(*pRealBase64)))
            {
                pRealBase64++;
                iLen--;
            }
            if (pRealBase64 != NULL && *pRealBase64 != '\0')
            {
                /* Figure out how big the binary data will be. */
                iTmpBase64Len = B64_UCHAR_CHARS(iLen);
                /* Allocate memory to hold the certificate list. */         
                pCertBase64Buff = (unsigned char*)malloc(iTmpBase64Len);
                if (pCertBase64Buff != NULL)
                {
                    iCertBase64BuffLen = iTmpBase64Len;
                    iDecodeLen = 0;
                    /* First Decode the certificate. */
                    cr = B64Decode((char*)pRealBase64,iLen,pCertBase64Buff,iCertBase64BuffLen,&iDecodeLen);
                    if (cr == CONVERSION_OK)
                    {//load certificate into SSL context
                    	wolfStatus = wolfSSL_CTX_load_verify_buffer(sslDesc->sslContext, pCertBase64Buff, (long) iDecodeLen, SSL_FILETYPE_ASN1);
                    	// Save load status of current cert
                    	sslDesc->certListDesc.certLoadStatus = wolfStatus;
                    	if (wolfStatus != SSL_SUCCESS)
                    	{
                    		iStatus = fnHandleCertLoadError(wolfStatus, pCertBase64Buff, iDecodeLen);
                    	}
                    	else
                    	{
                    		sslDesc->certListDesc.certCount++;
                    	}
                    }
                    else
                    {
                        /* Error - Invalid Base64 Encoding. */
                        iStatus = SSLIF_INVALID_BASE64_ENCODE_ERR;
                    }
                }
                else
                {
                    /* Error - Could not allocate buffer. */
                    iStatus = SSLIF_SSL_OUT_OF_MEMORY;
                }                       
            }           
        }                   
        
        /* Release the buffer. */
        if (pCertBase64Buff != NULL)
        {
            free(pCertBase64Buff);
        }
    }   
    else if (strcmp((char*)pName,"TrustedHost") == 0)
    {
        /* Another host has been parsed, can it be added to the list? */
        if (sslDesc->certListDesc.lwTrustedHostListEntries < MAX_TRUSTED_HOSTS)
        {
            /* There is enough room in the list.  Add the name, truncating it if necessary. */
            memset((void*)sslDesc->certListDesc.pTrustedHostList[sslDesc->certListDesc.lwTrustedHostListEntries],0,TRUSTED_HOST_LENGTH);
            strncpy(sslDesc->certListDesc.pTrustedHostList[sslDesc->certListDesc.lwTrustedHostListEntries],(const char*)pChars,TRUSTED_HOST_LENGTH - 1);
            sslDesc->certListDesc.lwTrustedHostListEntries++;
        }
    }
    else if (strcmp((char*)pName,"AllowedCipher") == 0)
    {// Add to colon separated list only if list can hold new entry
        if ( (sslDesc->certListDesc.curAllowedCipher - &(sslDesc->certListDesc.allowedCipherList[0])) + iLen + 1 < CIPHER_LIST_SIZE)
        {// Don't prepend colon on first entry
        	if (sslDesc->certListDesc.allowedCipherCount != 0)
        	{
				strncpy(sslDesc->certListDesc.curAllowedCipher, ":", 1);
				sslDesc->certListDesc.curAllowedCipher++;
        	}
        	strncpy(sslDesc->certListDesc.curAllowedCipher, (const char *) pChars, iLen);
        	sslDesc->certListDesc.curAllowedCipher += iLen;
        	sslDesc->certListDesc.allowedCipherCount++;
        }
        else
        {
			dbgTrace(DBG_LVL_ERROR,"Too many allowed ciphers, got %d, ignoring the rest", sslDesc->certListDesc.allowedCipherCount);
        }
    }
    
    return iStatus;
}

/************************************************************************
Function	:	fnCertListETag
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function is a parser callback function. This function
				is called when it finds an end tag in the trusted root
				certificate list file. If the tag is the certificate tag
				then the function releases some resources.
Parameters	:	Standard Parser Parameters.
Return Value:	SSLIF_SUCCESS if successfull, otherwise an error.
*************************************************************************/ 
static short fnCertListETag(void * context, XmlChar *pName)
{
    
    return SSLIF_SUCCESS;
}

/************************************************************************
Function	:	fnLoadCertificatesFromFile
Author		:	Derek DeGennaro/Sam Thillaikumaran
Description	:	This function loads the certificates that are contained in
				the trusted root certificate list file.  The signature of
				the file is verified before loading the certificates.
				If the file is corrupted then it is deleted.
Parameters	:	pFileName - The name of the file.
Return Value:	SSLIF_SUCCESS if successfull, otherwise an error.
*************************************************************************/ 
static int fnLoadCertificatesFromFile(char *pFileName, SSL_DESCRIPTOR * sslDesc)
{
    long lwDataOffset = 0, lwDataSize = 0;
    FILE *pCertFileHandle = NULL;
    short iParseResult = SSLIF_SUCCESS;
    int iStatus = SSLIF_SUCCESS;  
	struct stat fileInfo;
	int fileStatus;
	char  semstat;

	// get semaphore for file
	semstat = OSAcquireSemaphore (CERT_FILE_SEM_ID, 100);
	if (semstat != SUCCESSFUL)
	{//should never happen
	    return SSLIF_CERT_LIST_FILE_UNAVAILABLE;
	}

    if (pFileName != NULL && sslDesc != NULL)
    {               
        fileStatus = stat( pFileName, &fileInfo );
        if (fileStatus == POSIX_SUCCESS)
        {
        	lwDataSize = fileInfo.st_size;
            /* Open the file */                     
            pCertFileHandle = fopen(pFileName,"rb");
            if(pCertFileHandle != NULL)
            {               
                if( fseek(pCertFileHandle, lwDataOffset, SEEK_SET) == 0 )
                {
                    unsigned char *file_contents = NU_NULL;
                    BOOL osstat;

                    osstat = OSGetMemory((void **)&file_contents, lwDataSize);
                    if (osstat == FAILURE)
                    {
                        dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Server Context failed to allocate sufficient memory for Certificates file contents: %d", lwDataSize);

                        iStatus = SSLIF_SSL_OUT_OF_MEMORY;
                    }
                    else
                    {
                        // Read whole file into buffer
                        size_t readLen = fread((void*)file_contents, 1, lwDataSize, pCertFileHandle);

                        if (readLen < lwDataSize)
                        {
                            fnCheckFileSysCorruption(POSIX_ERROR);
                            iParseResult = SSLIF_CERT_LIST_FILE_READ_ERR;
                        }
                        else
                        {

                            /* take all the characters in file_contents and send them
                            to the parser.  The parser callback routines
                            are what set up the certificates in the 
                            certificate store. */

                            XmlChar *pXmlChar = (XmlChar *)file_contents;

                            iParseResult = XMLParserWithContextInit(&(sslDesc->certListDesc.dxmlLoadCert),     // parser
                                                        0,                      // # table entries
                                                        NULL,                   // table
                                                        fnCertListParseStart,   // parse start fcn
                                                        fnCertListParseEnd,     // parse end fcn
                                                        fnCertListSTag,         // default start element fcn
                                                        fnCertListETag,         // default end element fcn
                                                        fnCertListCharData,     // default chardata fcn
                                                        0,
                                                        (void *) sslDesc);
                                                        
                            while (iParseResult == XML_STATUS_OK && lwDataSize > 0)
                            {
                                iParseResult = XMLSendParserChar(&(sslDesc->certListDesc.dxmlLoadCert), *pXmlChar++);
                                lwDataSize -= (long)sizeof(XmlChar);
                            }

                            (void)XMLParserEndParse(&(sslDesc->certListDesc.dxmlLoadCert), iParseResult == XML_STATUS_OK);
                            if (iParseResult != XML_STATUS_OK)
                            {           
                                /* Log the offending error. */
                                sprintf(sslLog, "xml parse err# %d", iParseResult);
                                putString(sslLog, __LINE__);
                                iStatus = SSLIF_XML_PARSER_ERR;
                            }
                            else
                            {//
                            }
                        }

                        if (file_contents != NU_NULL)
                        {
                            (void) OSReleaseMemory(file_contents);
                        }
                    }
                }
                else
                {
                    fnCheckFileSysCorruption(POSIX_ERROR);
                    /* Couldn't go to data part of file. */                 
                    iStatus = SSLIF_CERT_LIST_FILE_SEEK_ERR;
                }
            }
            else
            {
                fnCheckFileSysCorruption(POSIX_ERROR);
                /* Couldn't open file. */
                iStatus = SSLIF_CERT_LIST_FILE_OPEN_ERR;
            }           
        }
        else
        {
            fnCheckFileSysCorruption(fileStatus);
            /* Coudn't find file. */
            iStatus = SSLIF_CERT_LIST_FILE_OPEN_ERR;
        }
    }
    else
    {
        /* Error - Invalid Arguments. */
        iStatus = SSLIF_INVALID_INPUT;
    }

    /* Close the open file. */
    if (pCertFileHandle != NULL)
    {
        fileStatus = fclose(pCertFileHandle);

        if (fileStatus != POSIX_SUCCESS)
            fnCheckFileSysCorruption(POSIX_ERROR);
    }
    
	(void) OSReleaseSemaphore(CERT_FILE_SEM_ID);

	dbgTrace(DBG_LVL_INFO,"SSL loading certificates from file, #certs %d, #hosts: %d, #cipher suites: %d",
			sslDesc->certListDesc.certCount,
			sslDesc->certListDesc.lwTrustedHostListEntries,
			sslDesc->certListDesc.allowedCipherCount);

	if (iStatus != SSLIF_SUCCESS)
    {           
		dbgTrace(DBG_LVL_ERROR,"Error: SSL loading certificates from file: %s, status: %d", pFileName, iStatus);
    }
	
    return iStatus;
}

/************************************************************************
Description	: 	This is a callback called by the SSL library during client authentication
Parameters	: 	client authentication status
				X509 Store context
Return Value: 	integer indicating status
*************************************************************************/
static int fnVerifyClientCertificate(int iPreVerifyOk, X509_STORE_CTX *ctx)
{
    char tempDate[CERT_DATE_SIZE];
    WOLFSSL_X509 *tempX509 = NULL;

    if (iPreVerifyOk == 0)
    {
		dbgTrace(DBG_LVL_ERROR,"Error Client Cert Verification Failure::     SSL Client Cert Domain: %s, Error: %d",
				(ctx != NULL ? ctx->domain : "context null"),
				(ctx != NULL ? ctx->error : 0));
    }

    tempX509 = ctx->current_cert;
	if (tempX509 != NULL)
	{// log info on bad cert
	    char *issuer = wolfSSL_X509_NAME_oneline(wolfSSL_X509_get_issuer_name(tempX509), 0, 0);
	    char *subject = wolfSSL_X509_NAME_oneline(wolfSSL_X509_get_subject_name(tempX509), 0, 0);

	    dbgTrace(DBG_LVL_INFO,"Error Client Cert Verification Failure::     SSL Client Cert Issuer: %s", issuer);
		dbgTrace(DBG_LVL_INFO,"Error Client Cert Verification Failure::    SSL Client Cert Subject: %s", subject);

		memset(tempDate, 0, sizeof(tempDate));
		strncpy(tempDate, (const char *) wolfSSL_X509_notBefore(tempX509), CERT_DATE_SIZE - 1);
		dbgTrace(DBG_LVL_INFO,"Error Client Cert Verification Failure:: SSL Client Cert Valid From: %s", tempDate+2);

		strncpy(tempDate, (const char *) wolfSSL_X509_notAfter(tempX509), CERT_DATE_SIZE - 1);
		dbgTrace(DBG_LVL_INFO,"Error Client Cert Verification Failure::   SSL Client Cert Valid To: %s", tempDate+2);
	}

	return iPreVerifyOk;
}

/************************************************************************
Description	: 	This function loads the server certificate into the SSL library
				If the file is not found then a hardcoded value is used.
				The hardcoded data should only be used to allow a valid file
				to be downloaded and then the file would be used after a reset.
Parameters	: 	pointer to SSL context, filename or BOOL forcing default
Return Value: 	boolean indicating success or failure
*************************************************************************/
BOOL loadServerCertificate(SSL_CTX *sslContext, char *filename, BOOL forceDefault)
{
	struct stat fileInfo;
	int sslret = SSL_SUCCESS;
	long buflen;    
	int fileStatus = POSIX_SUCCESS;

	if (forceDefault || ((fileStatus = stat(filename, &fileInfo)) != POSIX_SUCCESS))
	{
    	fnCheckFileSysCorruption(fileStatus);
    	errno = ENOERR; //clear errno
    	buflen = (long) (strlen((const char *)defaultServerCert) + 1);
    	sslret = wolfSSL_CTX_use_certificate_buffer(sslContext, defaultServerCert, buflen, SSL_FILETYPE_PEM);
    	if (sslret != SSL_SUCCESS)
    	{
    		dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Server Context load default server certificate failed: %d\n", sslret);
        	return FALSE;
    	}
	}
	else
    {
    	sslret = wolfSSL_CTX_use_certificate_file(sslContext, filename, SSL_FILETYPE_PEM);
    	if (sslret != SSL_SUCCESS)
    	{
    		dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Server Context load server certificate file %s: %d\n", filename, sslret);
        	return FALSE;
    	}
    }
    return TRUE;
}

/* **********************************************************************
// PURPOSE: Returns password to decrypt private key file
//
// **********************************************************************/
#define PWD_LEN 32
#define PWD_CHUNK_LEN 8
#define NUM_PWD_CHUNKS (PWD_LEN / PWD_CHUNK_LEN)
static unsigned char outputPWD[PWD_LEN+1];

extern unsigned char defPrivKeyRand1[];
extern unsigned char defPrivKeyRand2[];
extern unsigned char defPrivKeyRand3[];
extern unsigned char defPrivKeyRand4[];
extern unsigned char defPrivKeyPwd1[];
extern unsigned char defPrivKeyPwd2[];
extern unsigned char defPrivKeyPwd3[];
extern unsigned char defPrivKeyPwd4[];

// Clear password for security reasons
void clearPasswordinMemory(void)
{
	memset((void*)outputPWD, 0, sizeof(outputPWD));
}

char *getDefaultPrivateKeyPwd(void)
{
	int chidx;
	unsigned char *outptr = &outputPWD[0];
	unsigned char temp;

	clearPasswordinMemory();

	//Combine chunks
	for (chidx = 0; chidx < PWD_CHUNK_LEN; chidx++)
	{
		*outptr++ = (defPrivKeyRand1[chidx] ^ defPrivKeyPwd1[chidx]);
	}

	for (chidx = 0; chidx < PWD_CHUNK_LEN; chidx++)
	{
		*outptr++ = (defPrivKeyRand2[chidx] ^ defPrivKeyPwd2[chidx]);
	}

	for (chidx = 0; chidx < PWD_CHUNK_LEN; chidx++)
	{
		*outptr++ = (defPrivKeyRand3[chidx] ^ defPrivKeyPwd3[chidx]);
	}

	for (chidx = 0; chidx < PWD_CHUNK_LEN; chidx++)
	{
		*outptr++ = (defPrivKeyRand4[chidx] ^ defPrivKeyPwd4[chidx]);
	}
	*outptr = 0 ;

	// Swap back bytes
	temp = outputPWD[2];
	outputPWD[2] = outputPWD[27];
	outputPWD[27] = temp;

	temp = outputPWD[11];
	outputPWD[11] = outputPWD[23];
	outputPWD[23] = temp;

	return (char *) outputPWD;
}

extern unsigned char filePrivKeyRand1[];
extern unsigned char filePrivKeyRand2[];
extern unsigned char filePrivKeyRand3[];
extern unsigned char filePrivKeyRand4[];
extern unsigned char filePrivKeyPwd1[];
extern unsigned char filePrivKeyPwd2[];
extern unsigned char filePrivKeyPwd3[];
extern unsigned char filePrivKeyPwd4[];

char *getFilePrivateKeyPwd(void)
{
	int chidx;
	unsigned char *outptr = &outputPWD[0];
	unsigned char temp;

	clearPasswordinMemory();

	//Combine chunks
	for (chidx = 0; chidx < PWD_CHUNK_LEN; chidx++)
	{
		*outptr++ = (filePrivKeyRand1[chidx] ^ filePrivKeyPwd1[chidx]);
	}

	for (chidx = 0; chidx < PWD_CHUNK_LEN; chidx++)
	{
		*outptr++ = (filePrivKeyRand2[chidx] ^ filePrivKeyPwd2[chidx]);
	}

	for (chidx = 0; chidx < PWD_CHUNK_LEN; chidx++)
	{
		*outptr++ = (filePrivKeyRand3[chidx] ^ filePrivKeyPwd3[chidx]);
	}

	for (chidx = 0; chidx < PWD_CHUNK_LEN; chidx++)
	{
		*outptr++ = (filePrivKeyRand4[chidx] ^ filePrivKeyPwd4[chidx]);
	}
	*outptr = 0 ;

	// Swap back bytes
	temp = outputPWD[2];
	outputPWD[2] = outputPWD[27];
	outputPWD[27] = temp;

	temp = outputPWD[11];
	outputPWD[11] = outputPWD[23];
	outputPWD[23] = temp;

	return (char *) outputPWD;
}


/************************************************************************
Description	: 	This function loads takes an encrypted private key buffer
				and decrypts it into another buffer.  Then it loads the
				decrypted key into WolfSSL.
Parameters	: 	pointer to SSL context
				encrypted key buffer
				decrypted key buffer
				size of buffers (both the same)
				password for decryption
Return Value: 	int: size of decrypted key or negative for failure
*************************************************************************/
int loadPrivateKeyBuffer(SSL_CTX *sslContext, unsigned char *encbufptr, unsigned char *decbufptr, int bufsize, char * pwd)
{
	int sslret = SSL_SUCCESS;
	int numDERbytes;

	// convert provided key from PEM to DER and decrypt using password
	sslret = wolfSSL_KeyPemToDer((const unsigned char *) encbufptr, bufsize, decbufptr, bufsize, (const char *) pwd);
	if (sslret <= 0)
	{
		dbgTrace(DBG_LVL_ERROR,"Error: WolfSSL failed to convert PEM key to DER: %d\n", sslret);
    	return sslret;
	}
	numDERbytes = sslret;

	// provide key to WolfSSL
	sslret = wolfSSL_CTX_use_PrivateKey_buffer(sslContext, (const unsigned char*) decbufptr, (long) numDERbytes, SSL_FILETYPE_ASN1);
	if (sslret != SSL_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR,"Error: WolfSSL failed to load DER private key: %d\n", sslret);
    	return sslret;
	}

	return sslret;

}

/************************************************************************
Description	: 	This function loads the encrypted server private key into the SSL library
				If the file is not found then a hardcoded value is used.
				The hardcoded data should only be used to allow a valid file
				to be downloaded and then the file would be used after a reset.
Parameters	: 	pointer to SSL context, filename or BOOL forcing default
Return Value: 	boolean indicating success or failure
*************************************************************************/
BOOL loadEncryptedPrivateKey(SSL_CTX *sslContext, char *filename, BOOL forceDefault)
{
	struct stat fileInfo;
	long encBufLen;
	unsigned char *encbufptr;
	unsigned char *decbufptr;
	BOOL osstat;
	BOOL heapEncBuf = FALSE;
	int loadStat;
	char * pwd;
	int fileStatus = POSIX_SUCCESS;

	// Get encrypted key from hardcoded default or file
	if (forceDefault || ((fileStatus = stat(filename, &fileInfo)) != POSIX_SUCCESS))
    {
    	fnCheckFileSysCorruption(fileStatus);
    	errno = ENOERR; //clear errno
    	encBufLen = (long) (strlen((const char *)defaultEncryptedPrivateKey) + 1);
    	encbufptr = (unsigned char *) defaultEncryptedPrivateKey;
    	// Get password
    	pwd = getDefaultPrivateKeyPwd();
		dbgTrace(DBG_LVL_INFO,"Private key file not found. Default will be used.\n");
    }
	else
    {
		encBufLen = fileInfo.st_size;
		// Allocate buffer for encrypted private key file in PEM format
	    osstat = OSGetMemory ((void **) &encbufptr, encBufLen );
	    if (osstat == FAILURE)
	    {
	    	dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Server Context failed to allocate sufficient memory for private key: %d", encBufLen);
	    	return FALSE;
	    }

	    // Read whole file into buffer
	    loadStat = fsReadFile(filename, encbufptr, encBufLen);
		if (loadStat <= 0)
		{
		    (void) OSReleaseMemory(encbufptr);
    		dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Server Context reading private key file %s failed: %d\n", filename, loadStat);
        	return FALSE;
		}
		// Get file password
    	pwd = getFilePrivateKeyPwd();
		dbgTrace(DBG_LVL_INFO,"Private key file found and read.\n");
		// indicate encrypted buffer in heap so it can be freed
		heapEncBuf = TRUE;
    }

	// Allocate buffer for decrypted private key in binary format - size needed should be no more than encrypted key
	osstat = OSGetMemory ((void **) &decbufptr, encBufLen );
	if (osstat == FAILURE)
	{
		if (heapEncBuf)
			(void) OSReleaseMemory(encbufptr);
		dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Server Context failed to allocate sufficient memory for private key: %d", encBufLen);
		return FALSE;
	}

	// Decrypt private key and provide to WolfSSL
	loadStat = loadPrivateKeyBuffer(sslContext, encbufptr, decbufptr, encBufLen, pwd);

    // Clean up memory
	clearPasswordinMemory();
	if (heapEncBuf)
		(void) OSReleaseMemory(encbufptr);
    (void) OSReleaseMemory(decbufptr);

	if (loadStat <= 0)
	{
    	dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Server Context load private key buffer failed: %d\n", loadStat);
    	return FALSE;
	}

    return TRUE;

}

/************************************************************************
Description	: 	This function deletes the ssl server context, deallocating memory as necessary
Parameters	: 	pointer to SSL_DESCRIPTOR structure
Return Value: 	integer indicating status
*************************************************************************/
int fnDestroySSLServerContext(SSL_DESCRIPTOR * sslDesc)
{
    if (sslDesc == NULL)
    {
    	return SSLIF_SUCCESS;
    }

    if (sslDesc->sslContext != NULL)
    {
        SSL_CTX_free(sslDesc->sslContext);
    }

    (void) OSReleaseMemory(sslDesc);

	return SSLIF_SUCCESS;

}

/************************************************************************
Description	: 	This function creates the ssl context for a server
Parameters	:
Return Value: 	a pointer to an SSL_DESCRIPTOR structure if successful, otherwise NULL
*************************************************************************/
SSL_DESCRIPTOR * fnCreateSSLServerContext(void)
{
	BOOL ret;
	BOOL forceDefaultCert = FALSE;
	DATETIME      forcedDate;
	SSL_DESCRIPTOR * retSSLDesc = NULL;
	int sslret = SSL_SUCCESS;
    char CERTLIST_FILE_NAME[16] = "XXXXCERT.BIN";
    char PEERCERT_FILE_NAME[20] = "XXXXPEERCERT.PEM";
    char SERVCERT_FILE_NAME[20] = "XXXXSERVCERT.PEM";
    char SERVKEY_FILE_NAME[20]  = "XXXXSERVKEY.PEM";
    char SERVENCKEY_FILE_NAME[30]  = "XXXXSERVENCKEY.PEM";
    SSL_METHOD *sslMethod = NULL;

    fnDumpStringToSystemLog("fnCreateSSLServerContext start");

    ret = OSGetMemory ((void **) &retSSLDesc, sizeof(SSL_DESCRIPTOR));
    if (ret == FAILURE)
    {
    	dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Server Context failed to allocate sufficient memory");
    	return NULL;
    }
	memset((void*)retSSLDesc, 0, sizeof(SSL_DESCRIPTOR));

	/* Initialize the certificate list file name.  The
    file name depends on the PCN of the UIC.  The
    file name is defined with the first 4 characters allocated
    for the PCN. */
    memcpy((void*)CERTLIST_FILE_NAME, (void*)CMOSSignature.bUicPcn, 4);
    memcpy((void*)PEERCERT_FILE_NAME, (void*)CMOSSignature.bUicPcn, 4);
    memcpy((void*)SERVCERT_FILE_NAME, (void*)CMOSSignature.bUicPcn, 4);
    memcpy((void*)SERVKEY_FILE_NAME,  (void*)CMOSSignature.bUicPcn, 4);
    memcpy((void*)SERVENCKEY_FILE_NAME,  (void*)CMOSSignature.bUicPcn, 4);

    //Workaround to problem that system clock is not set if no EMD present (SCM init issue)
    // - must have valid system clock before loading certs & want to have valid context if no EMD,
    // - so set system clock to be start date of default cert and force usage of default cert
    if (validSysClock() == FALSE)
    {
    	forceDefaultCert = TRUE;
    	// set system clock to be start of default cert valid period
		(void) strDateTimeToDATETIME(&forcedDate, (char *) defaultServerCertNotBeforeDateTime);
    	SetSystemClock(&forcedDate);
        OSWakeAfter(0); // relinquish control to OS so it can properly update file system
    }

	/* create client method */
	sslMethod = TLSv1_2_server_method();
    if (sslMethod == NULL)
    {
    	dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Server Context failed to create TLS server method");
        (void) fnDestroySSLServerContext(retSSLDesc);
    	return NULL;
    }

	retSSLDesc->sslContext = SSL_CTX_new(sslMethod);
    if (retSSLDesc->sslContext == NULL)
    {
    	dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Server Context failed to create SSL Context");
        (void) fnDestroySSLServerContext(retSSLDesc);
    	return NULL;
    }

    // Any failure with loading root cert list file will result in default ciphers being used
    sslret = fnLoadCertificatesFromFile(CERTLIST_FILE_NAME, retSSLDesc);
    if (sslret != SSLIF_SUCCESS)
    {
    	dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Server Context failed to load root certificates: %d", sslret);
    	dbgTrace(DBG_LVL_INFO,"SSL Server Context using default cipher list");
    }
    else
    {
        // check allowed cipher list
        if ( retSSLDesc->certListDesc.allowedCipherCount == 0 )
        {
        	dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Server Context failed to load any allowed cipher suites");
        	dbgTrace(DBG_LVL_INFO,"SSL Server Context using default cipher list");
        }
        else
        {
        	sslret = wolfSSL_CTX_set_cipher_list(retSSLDesc->sslContext, (const char*) &(retSSLDesc->certListDesc.allowedCipherList[0]));
        	if (sslret != SSL_SUCCESS)
        	{
        		dbgTrace(DBG_LVL_ERROR,"Error: Create SSL Context setting cipher list: %d\n", sslret);
            	dbgTrace(DBG_LVL_INFO,"SSL Server Context using default cipher list");
        	}
        	else
        	{
            	dbgTrace(DBG_LVL_INFO,"Loaded into SSL Server Context cipher list: %s\n", &(retSSLDesc->certListDesc.allowedCipherList[0]));
        	}
        }
    }

	// Load server certificate
	ret = loadServerCertificate(retSSLDesc->sslContext, SERVCERT_FILE_NAME, forceDefaultCert);
    if (ret == FALSE)
    {
        (void) fnDestroySSLServerContext(retSSLDesc);
    	return NULL;
    }

	// Load private key
	ret = loadEncryptedPrivateKey(retSSLDesc->sslContext, SERVENCKEY_FILE_NAME, forceDefaultCert);
    if (ret == FALSE)
    {
        (void) fnDestroySSLServerContext(retSSLDesc);
    	return NULL;
    }

    // Delete cleartext private key file if any exists
    // If there are any errors with this, they get logged but they will not prevent context creation
	ret = fsDeleteFile(SERVKEY_FILE_NAME);
    if (ret == TRUE)
    {
		dbgTrace(DBG_LVL_INFO,"Cleartext private key file %s deleted\n", SERVKEY_FILE_NAME);
    }

    // If we are forcing the use of default certs, don't enable client authentication which may not work
    if (forceDefaultCert)
    {
		dbgTrace(DBG_LVL_ERROR,"No valid OS clock, default certificates will be used, peer verification will NOT be performed\n");
    }
    else
    {
        // Load peer verification certificate file - if file is not present then turn off peer verification,
    	// otherwise use the supplied certificate.
		sslret = wolfSSL_CTX_trust_peer_cert(retSSLDesc->sslContext, PEERCERT_FILE_NAME, SSL_FILETYPE_PEM);
		if (sslret == SSL_SUCCESS)
		{
			dbgTrace(DBG_LVL_ERROR,"SSL Trusted peer certificate file %s found, Peer verification WILL be performed\n", PEERCERT_FILE_NAME);
			// this is for client authentication - fail if client does not provide good cert
			SSL_CTX_set_verify(retSSLDesc->sslContext, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, fnVerifyClientCertificate);
		}
		else
		{
			dbgTrace(DBG_LVL_ERROR,"SSL No valid trusted peer certificate file %s found, peer verification will NOT be performed\n", PEERCERT_FILE_NAME);
		}
    }

	fnDumpStringToSystemLog("fnCreateSSLServerContext End");
    return retSSLDesc;
}

