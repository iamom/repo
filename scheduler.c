//************************************************************************
//   PROJECT:        MEGA, JANUS, FUTURE PHOENIX, K7
//   MODULE NAME:    scheduler.c
//   
//   DESCRIPTION:    
//      Routines for adding to, deleting from and maintaining the linked list
//      of Scheduled Events.   Also, routines for handling the current
//      Scheduled Event when it becomes due.
//
/************************************************************************
*	REVISION HISTORY:

 05-Feb-18 sa002pe on FPHX 02.12 shelton branch
 	Added/Changed some syslog messages so it's easier to see what is going
	on w/ the events.
*
* 2014.10.02 Clarisa Bellamy    FPHX 02.12 branch
*	Hopefully to fix Fracas 227431 & 227460:
*  - Modified function deleteNvmBlock() so that a "deleted" block's memory
*    is cleared BEFORE the block is made available, instead of afterwards.
*  - Modified function delAllScheduledEventsMatchingDescription so that if
*    an event is deleted, the next current Event ptr isn't set to 0, and
*    the rest of the linked list is checked instead of random memory.
*     And reset the index so that if a second match is found, the matching
*    event will be deleted and not the one after it.
*  - Added function headers to the two modified functions, and a bit more
*    of the standard file header above.

 04-Apr-14 sa002pe on FPHX 02.12 shelton branch
	Merging in changes from Janus tips for U.K. EIB:
	1. Added an entry for the Dcap Upload Date/Time event to callBacks[].
	2. Cleaned up callBacks[] so the common entries aren't in the "ifdef" parts.
	3. Changed eventScheduler to put messages in the syslog indicating that
		this is why the alarm timer went off and the address of the event
		handler.
	4. Changed scheduleEvent to allow the event to be a few seconds in the past
		to allow for any delay in scheduling the event.  The event time will be
		adjusted ahead by a few seconds so it will happen right away.
*
* 05-Apr-07 sa002pe on FPHX 01.04 shelton branch
*	Added definition of THISFILE.
*	Changed all calls for exception logging to use THISFILE instead of __FILE__.
*
* 	$Log:   H:/group/SPARK/ARCHIVES/UIC/scheduler.c_v  $
* 
*    Rev 1.20   29 Nov 2004 09:37:46   cx08856
* reinstated fnAddTimerEvent( ) for JANUS
* 
*    Rev 1.19   30 Jun 2004 13:19:36   SA002PE
* Added header block to capture the PVCS comments.
* Added "DST Auto Change" to the call back table for Janus.
*
*	Merged in the change from 1.15.1.0
*************************************************************************/
#ifndef SCHEDULER_C
#define SCHEDULER_C

#include "..\include\rftypes.h"
#include "..\include\clock.h"
#include "..\include\cometphc.h"
#include "..\include\scheduler.h"
#include "networking/externs.h"
#include "pbos.h"


#define THISFILE "scheduler.c"

//File scope scratchpad
static char scratchPad[256];


//
static void dummyFunction(EVENT_SCHEDULER_INFO *eventInfoPtr);
static void dummyFunction2(EVENT_SCHEDULER_INFO *eventInfoPtr);

extern void setScheduledEventsHeadPtr(EVENT_SCHEDULER_INFO *eventSchedulerInfo);
extern void fnClearChronEvent (unsigned short event);
//TODO JAH remove dcperiod.c extern void fnDCAPAutoUploadEventCallback(EVENT_SCHEDULER_INFO *eventInfoPtr);
extern void fnDSTAutoChangeCallback(EVENT_SCHEDULER_INFO *eventInfoPtr);

#ifndef K700
//	extern void fnAGDMajorPeriodAlarmCallback(EVENT_SCHEDULER_INFO *eventInfoPtr);
//	extern void fnAGDMidnightAlarmCallback(EVENT_SCHEDULER_INFO *eventInfoPtr);
#endif

#ifdef JANUS
	//TODO JAH remove dcperiod.c extern void fnDCAPUploadDateTimeEventCallback(EVENT_SCHEDULER_INFO *eventInfoPtr);
#else
	extern void oitUpdateEventCallback(EVENT_SCHEDULER_INFO *eventInfoPtr);
	extern void fnAccountPeriodAlarmCallback(EVENT_SCHEDULER_INFO *eventInfoPtr);
#endif


CALL_BACK_HASH callBacks[] = {{"dummyFunction", dummyFunction},
                              {"dummyFunction2", dummyFunction2},
                              {"anotherHandle", dummyFunction},
                              {"anotherHandle2", dummyFunction2},
							  //TODO JAH remove dcperiod.c {"dcapAutoUploadFcn",fnDCAPAutoUploadEventCallback},
							  {"DSTAutoChange",fnDSTAutoChangeCallback},
#ifndef K700
//							  {"abacusMajorPeriod",fnAGDMajorPeriodAlarmCallback},
//							  {"abacusMidnight",fnAGDMidnightAlarmCallback},
#endif

#ifdef JANUS
							  {"oitUpdatePending",dummyFunction},
							  {"deptAccountPeriod",dummyFunction},
							  //TODO JAH remove dcperiod.c {"dcapUploadDateTime",fnDCAPUploadDateTimeEventCallback},
#else
							  {"oitUpdatePending",oitUpdateEventCallback},
							  {"deptAccountPeriod",fnAccountPeriodAlarmCallback},
#endif
                              };

//This should be called at power up.
//What it does is to register the first event in the chain with the low level fnAddTimerEvent() function.
//If the time is already past it calls the scheduler right away
void initEventScheduler(void)
{
	fnDumpStringToSystemLog("=== Init Event Scheduler");
    
     //If there are any events...
    if(getScheduledEventsHeadPtr())
    {
	    DATETIME now;
	    long differenceInSeconds;


        if(!GetSysDateTime(&now, NOOFFSETS, GREGORIAN))     //Get the "now" time, note: NOOFFSETS is intentional
            taskException("GetSysDateTime() failure", THISFILE, __LINE__);      

        if(!CalcDateTimeDifference(&now, &getScheduledEventsHeadPtr()->eventTime, &differenceInSeconds))  // event time - now
            taskException("CalcDateTimeDifference() failure", THISFILE, __LINE__);       
                                                                 
        if(differenceInSeconds <= 0)    //If already expired
            eventScheduler();           //call eventScheduler
             
        else                            //register it
        {  
	        ChronEvent theEvent;
	        STATUS status;


            memset(&theEvent, '\0', sizeof(theEvent));
            theEvent.time = getScheduledEventsHeadPtr()->eventTime;
            theEvent.fn = eventScheduler;
            theEvent.eventType = GENERIC_EVENT;
//#ifdef JANUS    
//            status = 0;
//#else
            status = fnAddTimerEvent(&theEvent);
//#endif
            if(status)
            {
                sprintf(scratchPad, "fnAddTimerEvent() ERR# %d", status);     //Going to change this to return error
                taskException(scratchPad, THISFILE, __LINE__);
            }
        }
    }
}

void dummyFunction(EVENT_SCHEDULER_INFO *eventInfoPtr)
{
	STATUS status;


    putMsg("In dummy function 1", nextLine());
    
    putMsg(eventInfoPtr->eventDescription, nextLine());
    
    if((status = delEventSchedulerInfo(eventInfoPtr)) != NU_SUCCESS)
    {
        sprintf(scratchPad,"delEventSchedulerInfo() Error #%d", status); 
        taskException(scratchPad, THISFILE, __LINE__);    //Might change this to return error   
    }
}

void dummyFunction2(EVENT_SCHEDULER_INFO *eventInfoPtr)
{
	STATUS status;


    putMsg("In dummy function 2", nextLine());
    
    putMsg(eventInfoPtr->eventDescription, nextLine());
    
    if((status = delEventSchedulerInfo(eventInfoPtr)) != NU_SUCCESS)
    {
        sprintf(scratchPad,"delEventSchedulerInfo() Error #%d", status); 
        taskException(scratchPad, THISFILE, __LINE__);    //Might change this to return error   
    }
}

//Time to "Pop" an event and dispatch it...
//This gets called directly at initialization if an event was scheduled "While you where out"
void eventScheduler(void)
{
	EVENT_SCHEDULER_INFO *thisEvent;
	char pLogBuf[50];


    //First Make sure the head pointer is not NULL
    if(!getScheduledEventsHeadPtr())
        taskException("NULL Pointer", THISFILE, __LINE__);   
      
	fnDumpStringToSystemLog("=== Event Handler");

    while(getScheduledEventsHeadPtr()) //while there is an event... This logic also deals with when multiple events were scheduled "While you where out"
    {                                  
	    DATETIME now;
	    long differenceInSeconds;


        if(!GetSysDateTime(&now, NOOFFSETS, GREGORIAN))     //Get the "now" time, note: NOOFFSETS is intentional
            taskException("GetSysDateTime() failure", THISFILE, __LINE__);      

        if(!CalcDateTimeDifference(&now, &getScheduledEventsHeadPtr()->eventTime, &differenceInSeconds))  // event time - now
            taskException("CalcDateTimeDifference() failure", THISFILE, __LINE__);       
                                                                 
        if(differenceInSeconds <= 0)    //If already expired
        {                               //lookup and call the event handler function                 
	        int loopindex;                 
	        int foundFunction= 0;


            //Unlink the first entry in the chain
            thisEvent = getScheduledEventsHeadPtr();
            setScheduledEventsHeadPtr(thisEvent->nextEvent);

            for(loopindex = 0; loopindex < ARRAY_LENGTH(callBacks); loopindex++)
            {
                if(!strcmp(callBacks[loopindex].handle, thisEvent->registeredCallBackFunction))    //Search for a match of this event
                {
                    //found it
                    foundFunction = 1;
                
				 	(void)sprintf(pLogBuf, "Event Function Pointer = %08X", (unsigned long)callBacks[loopindex].function);
					fnDumpStringToSystemLog(pLogBuf);

                    //call it
                    callBacks[loopindex].function(thisEvent);        
                
                    //done
                    break;
                }
            }

            if(!foundFunction)
            {
                sprintf(scratchPad, "No Reg func: %s" , thisEvent->registeredCallBackFunction);    
                taskException(scratchPad, THISFILE, __LINE__);
            }
        
        }    
        else
            break;     
    }

    if(getScheduledEventsHeadPtr())     //if there is a future event
    {
	    ChronEvent theEvent;
	    STATUS status;


		/* the callback function may have scheduled an event with the low level timer, so clear it if so */
        fnClearChronEvent(GENERIC_EVENT);

        memset(&theEvent, '\0', sizeof(theEvent));
        theEvent.time = getScheduledEventsHeadPtr()->eventTime;
        theEvent.fn = eventScheduler;
        theEvent.eventType = GENERIC_EVENT;
        
//#ifdef JANUS    
//            status = 0;
//#else
            status = fnAddTimerEvent(&theEvent);
//#endif

        if(status)
        {
            sprintf(scratchPad, "fnAddTimerEvent() ERR# %d", status);     //Maybe change this to return error
            taskException(scratchPad, THISFILE, __LINE__);
        }
    }
}


void scheduleEvent(EVENT_SCHEDULER_INFO *thisEvent)
{
	DATETIME now;
	long differenceInSeconds;
	ChronEvent theEvent;
	STATUS status;
	int loopindex;                 
	int foundFunction= 0;
	char pLogBuf[50];


    //First make sure the pointer is not NULL
    if(!thisEvent)
        taskException("NULL Pointer", THISFILE, __LINE__);    //Going to change this to return error   
    
	fnDumpStringToSystemLog("=== Schedule Event");
    
    //Then check to be sure the handler is registered
    for(loopindex = 0; loopindex < ARRAY_LENGTH(callBacks); loopindex++)
    {
        if(!strcmp(callBacks[loopindex].handle, thisEvent->registeredCallBackFunction))    //Search for a match of this event
        {
            //found it
            foundFunction = 1;
        
            //done
            break;
         }
    }

    if(!foundFunction)
    {
        sprintf(scratchPad, "No Reg func: %s" , thisEvent->registeredCallBackFunction);    
        taskException(scratchPad, THISFILE, __LINE__);
    }

    //Then set the pointer to the next event to NULL
    thisEvent->nextEvent = NULL;

    //Now check the date and time to be sure it has not already passed 
    if(!GetSysDateTime(&now, ADDOFFSETS, GREGORIAN))
        taskException("GetSysDateTime() failure", THISFILE, __LINE__);    //Might change this to return error   

    if(!CalcDateTimeDifference(&now, &thisEvent->eventTime, &differenceInSeconds))  // event time - now
        taskException("CalcDateTimeDifference() failure", THISFILE, __LINE__);    //Might change this to return error   
                                                             
    sprintf(pLogBuf, "Event %02d-%02d-%02d %02d:%02d", 
            thisEvent->eventTime.bMonth, thisEvent->eventTime.bDay, thisEvent->eventTime.bYear, 
            thisEvent->eventTime.bHour, thisEvent->eventTime.bMinutes);
	fnDumpStringToSystemLog(pLogBuf);
	(void)sprintf(pLogBuf, "Event Function Ptr = %s", thisEvent->registeredCallBackFunction);
	fnDumpStringToSystemLog(pLogBuf);

    if (differenceInSeconds <= 0)
	{
		fnDumpStringToSystemLog("New Time in the past");

		// allow the difference to be a few seconds off
		// otherwise, throw an error
		if (differenceInSeconds < -10)
        	taskException("Invalid Time Specification", THISFILE, __LINE__);    //Going to change this to return error
		else
			differenceInSeconds = 5 - differenceInSeconds;
	}
	else
	{
		differenceInSeconds = 0;
	}
         
    //Now I need to convert the local time to GMT...
    fnTodSecsToDatetime((fnTodToSecs(&thisEvent->eventTime) + (unsigned long)differenceInSeconds), &thisEvent->eventTime, TRUE);	
        
    //register it with the low level timer if there are no events already pending
    if(!getScheduledEventsHeadPtr())
    {
        fnDumpStringToSystemLog("No events pending");

        memset(&theEvent, '\0', sizeof(theEvent));
        theEvent.time = thisEvent->eventTime;
        theEvent.fn = eventScheduler;
        theEvent.eventType = GENERIC_EVENT;

//#ifdef JANUS    
//            status = 0;
//#else
            status = fnAddTimerEvent(&theEvent);
//#endif

        if(status)
        {
            sprintf(scratchPad, "fnAddTimerEvent() ERR# %d", status);     //Going to change this to return error
            taskException(scratchPad, THISFILE, __LINE__);
        }
    }
    //Okee Dokee, now place it in the appropriate position in the list

    //empty list?
    if(!getScheduledEventsHeadPtr())
        setScheduledEventsHeadPtr(thisEvent);
        
    else    //Insert into the appropriate list position
    {
	    EVENT_SCHEDULER_INFO  *currentPtr = getScheduledEventsHeadPtr();
	    EVENT_SCHEDULER_INFO  *previousPtr = NULL;
	    int done = 0;


        while(currentPtr && !done)
        {
            sprintf(pLogBuf, "Current Event %02d/%02d/%02d %02d:%02d", 
                    currentPtr->eventTime.bMonth, currentPtr->eventTime.bDay, currentPtr->eventTime.bYear, 
                    currentPtr->eventTime.bHour, currentPtr->eventTime.bMinutes);
            fnDumpStringToSystemLog(pLogBuf);

            /* diff = new event time - current event time */
            if(!CalcDateTimeDifference(&currentPtr->eventTime, &thisEvent->eventTime, &differenceInSeconds))     
                taskException("CalcDateTimeDifference() failure", THISFILE, __LINE__);    //Might change this to return error   
    
            //If the event we are adding comes before (or at the same second!) as the one we are testing...
            if(differenceInSeconds <= 0)
            {
                fnDumpStringToSystemLog("New <= Current");

                if(currentPtr == getScheduledEventsHeadPtr())     //special case if dealing with head pointer
                {
                    fnDumpStringToSystemLog("New head of the list");

                    thisEvent->nextEvent = getScheduledEventsHeadPtr();
                    setScheduledEventsHeadPtr(thisEvent);
        
                    //Unregister the prevoius first event with the low level alarm
                    fnClearChronEvent(GENERIC_EVENT);
                    
                    //Register this one...
                    memset(&theEvent, '\0', sizeof(theEvent));
                    theEvent.time = thisEvent->eventTime;
                    theEvent.fn = eventScheduler;
                    theEvent.eventType = GENERIC_EVENT;
    
//#ifdef JANUS    
//            		status = 0;
//#else
            		status = fnAddTimerEvent(&theEvent);
//#endif

                    if(status)
                    {
                        sprintf(scratchPad, "fnAddTimerEvent() ERR# %d", status);     //Going to change this to return error
                        taskException(scratchPad, THISFILE, __LINE__);
                    }
                    
                    done = 1;
                }

                else
                {
                    fnDumpStringToSystemLog("Put in middle of list");

                    thisEvent->nextEvent = previousPtr->nextEvent;
                    previousPtr->nextEvent = thisEvent;
                    done = 1;
                }
            }   

            previousPtr = currentPtr;
            currentPtr = currentPtr->nextEvent;
    
        }
    
        if(!done)   //add to end
        {
            fnDumpStringToSystemLog("Put at end of list");

            previousPtr->nextEvent = thisEvent;
        }
    }
}


STATUS delScheduledEvent(int eventNumber)
{
int loopindex;
STATUS status;
EVENT_SCHEDULER_INFO *currentEventPtr = NULL;
EVENT_SCHEDULER_INFO *prevEventPtr = NULL;

    //Make sure the event is within range
    if(eventNumber > getNumberOfScheduledEvents())
        return NO_SUCH_EVENT;

    fnDumpStringToSystemLog("=== Delete Event by number");
    
    //Point to the first event
    currentEventPtr = getScheduledEventsHeadPtr();
    
    //iterate througe the linked list of events...
    for(loopindex = 1; loopindex < eventNumber; loopindex++)
    {
        prevEventPtr = currentEventPtr;
        currentEventPtr = currentEventPtr->nextEvent;
    }
    
    //If we are removing first event, the prevEventPtr will still 
    //be NULL and the the currentEventPtr will still point to the head
    if(!prevEventPtr)
    {
        //Unregister the event with the low level alarm
        fnClearChronEvent(GENERIC_EVENT);

        //Unlink the event...
        setScheduledEventsHeadPtr(currentEventPtr->nextEvent);

        //Register the next event if there is one...
        if(getScheduledEventsHeadPtr())
        {
        ChronEvent theEvent;
            
            memset(&theEvent, '\0', sizeof(theEvent));
            theEvent.time = getScheduledEventsHeadPtr()->eventTime;
            theEvent.fn = eventScheduler;
            theEvent.eventType = GENERIC_EVENT;
    
//#ifdef JANUS    
//            status = 0;
//#else
            status = fnAddTimerEvent(&theEvent);
//#endif

            if(status)
            {
                sprintf(scratchPad, "fnAddTimerEvent() ERR# %d", status);     //Going to change this to return error
                taskException(scratchPad, THISFILE, __LINE__);
            }
        }
    }
    else //Not the first event
    {
        //Unlink
        prevEventPtr->nextEvent = currentEventPtr->nextEvent;
    }

    //Clean up any data allocated for the event
    if(currentEventPtr->eventDataPtr)
    {
        //Free the NVM data
        if((status = deleteNvmBlock(currentEventPtr->eventDataPtr)) != NU_SUCCESS)
            {
            sprintf(scratchPad,"deleteNvmBlock() Error #%d", status); 
            taskException(scratchPad, THISFILE, __LINE__);    //Might change this to return error   
            }
    }

    //Free the info link
    if((status = delEventSchedulerInfo(currentEventPtr)) != NU_SUCCESS)
        {
        sprintf(scratchPad,"delEventSchedulerInfo() Error #%d", status); 
        taskException(scratchPad, THISFILE, __LINE__);    //Might change this to return error   
        }

    return NU_SUCCESS;

}

void delAllScheduledEvents(void)
{
int howManyscheduledEvents;
STATUS status;

    fnDumpStringToSystemLog("=== Delete All Events");
    
    for(howManyscheduledEvents = getNumberOfScheduledEvents(); howManyscheduledEvents > 0; howManyscheduledEvents--)
    {
        if((status = delScheduledEvent(howManyscheduledEvents)) != NU_SUCCESS)
        {
        sprintf(scratchPad,"delScheduledEvent() Error #%d", status); 
        taskException(scratchPad, THISFILE, __LINE__);
        }   
    }
}

STATUS deleteScheduledEvent(EVENT_SCHEDULER_INFO *thisEvent)
{
int loopindex;
EVENT_SCHEDULER_INFO *currentEventPtr = NULL;

    fnDumpStringToSystemLog("=== Delete Event by ptr");
    
    //Check for NULL pointer or no scheduled events
    if(!thisEvent || !getScheduledEventsHeadPtr())
        return NO_SUCH_EVENT;

    //Point to the first event
    currentEventPtr = getScheduledEventsHeadPtr();

    //iterate througe the linked list of events...
    for(loopindex = 1; loopindex <= getNumberOfScheduledEvents(); loopindex++)
    {
        if(currentEventPtr == thisEvent)
            break;

        currentEventPtr = currentEventPtr->nextEvent;
    }

    if(loopindex <= getNumberOfScheduledEvents())
        return delScheduledEvent(loopindex);

    else
        return NO_SUCH_EVENT;
}

STATUS delScheduledEventByHandle(unsigned long eventHandle)
{
int loopindex;
EVENT_SCHEDULER_INFO *currentEventPtr = NULL;

    //Check for no scheduled events
    if(!getScheduledEventsHeadPtr())
        return NO_SUCH_EVENT;

    fnDumpStringToSystemLog("=== Delete Event by Handle");
    
    //Point to the first event
    currentEventPtr = getScheduledEventsHeadPtr();

    //iterate througe the linked list of events...
    for(loopindex = 1; loopindex <= getNumberOfScheduledEvents(); loopindex++)
    {
        if(currentEventPtr->eventHandle == eventHandle)
            break;

        currentEventPtr = currentEventPtr->nextEvent;
    }

    if(loopindex <= getNumberOfScheduledEvents())
        return delScheduledEvent(loopindex);

    else
        return NO_SUCH_EVENT;
}

STATUS delScheduledEventByDescription(char *eventDescription)
{
int loopindex;
EVENT_SCHEDULER_INFO *currentEventPtr = NULL;

    //First make sure the pointer is not NULL
    if(!eventDescription)
        taskException("NULL Pointer", THISFILE, __LINE__);   
    
    //Check for no scheduled events
    if(!getScheduledEventsHeadPtr())
        return NO_SUCH_EVENT;

    fnDumpStringToSystemLog("=== Delete Event by description");
    
    //Point to the first event
    currentEventPtr = getScheduledEventsHeadPtr();

    //iterate througe the linked list of events...
    for(loopindex = 1; loopindex <= getNumberOfScheduledEvents(); loopindex++)
    {
        if(!strcmp(currentEventPtr->eventDescription, eventDescription))
            break;

        currentEventPtr = currentEventPtr->nextEvent;
    }

    if(loopindex <= getNumberOfScheduledEvents())
        return delScheduledEvent(loopindex);

    else
        return NO_SUCH_EVENT;
}

// ********************************************************************************
// FUNCTION NAME: delAllScheduledEventsMatchingDescription()                             
// PURPOSE:     Removes all of the events that an event description string that  
//              matches the input eventDescription string from the linked list
//              of waiting events.  For each one that is found, delScheduledEvent()
//              is called to actually remove the event from the list and deal with 
//              any necessary changes to the chrono timer list AND to free any Nvm
//              Block memory that may have been allocated for this event, to free
//              the NVM block data used to store this event, and to CLEAR the memory
//              in the EVENT_SCHEDULER_INFO structure.
//              
// INPUTS:      
//      eventDescription - Pointer to the string event name descriptor of the events
//                  we want to delete.  There can be only one string name per call.
// OUTPUTS:     
//      
//  Return:     Returns the number of events deleted from the linked list.
//
// NOTES:
//      1. When an event is "deleted" its memory is released for use by another 
//          event, AND that memory is also cleared to zeros.
// HISTORY:
//  2014.10.02 - Modified to NOT use currentEventPtr after it may have been cleared
//                  by the delScheduledEvent, and to check the entire list, and if
//                  a second event of the same type is found, delete it, and not the
//                  one after it.   Also added an entry in the syslog if the event
//                  ptr and the index are out of synch, which should NEVER happen now.
//---------------------------------------------------------------------
int delAllScheduledEventsMatchingDescription(char *eventDescription)
{
int loopindex;
EVENT_SCHEDULER_INFO *currentEventPtr = NULL;
  EVENT_SCHEDULER_INFO *pNextEventPtr = NULL;
  EVENT_SCHEDULER_INFO *checkEventPtr = NULL;
int numberOfDeletedEvents = 0;
  int                   numberOfEvents = 0;
  int                   iEventIndex;


    //First make sure the pointer is not NULL
    if(!eventDescription)
        taskException("NULL Pointer", THISFILE, __LINE__);   
    
    fnDumpStringToSystemLog("=== Delete All Events by description");
    //Check for no scheduled events
    if(!getScheduledEventsHeadPtr())
        return 0;

    //Point to the first event
    currentEventPtr = getScheduledEventsHeadPtr();

    //iterate througe the linked list of events...
    numberOfEvents = getNumberOfScheduledEvents();
    iEventIndex = 1;
    for(loopindex = 1; (loopindex <= numberOfEvents) && (currentEventPtr != NULL_PTR); loopindex++, iEventIndex++ )
    {
        checkEventPtr = getEventInfo( iEventIndex );
        if( checkEventPtr != currentEventPtr )
        {
            // The eventPtr and the index are out of synch now. This is a problem: 
            fnDumpStringToSystemLog( "LoopError in delAllScheduledEventsMatchingDescription()" );
            taskException( "LoopError", THISFILE, __LINE__);   
        }

        // Start prep for next loop:
        pNextEventPtr = currentEventPtr->nextEvent;

        if( !strcmp(currentEventPtr->eventDescription, eventDescription) )
        {
            // If a match is found, delete the event from the schedule, and free 
            //  its memory.
            delScheduledEvent( iEventIndex );
            numberOfDeletedEvents++;
			
            // Decrement the EventIndex, since the next event will now have the same
            //  index as the event that we just deleted. 
            iEventIndex--;
        }
		
        // Point to the next event in the list.
        currentEventPtr = pNextEventPtr;
    }

    return numberOfDeletedEvents;
}


//#ifndef JANUS	// By David , from #ifndef JANUS to allowing for Janus

EVENT_SCHEDULER_INFO *newEventSchedulerInfo(void)
{
EVENT_SCHEDULER_INFO *newEvent = newNvmBlock(sizeof(EVENT_SCHEDULER_INFO));    
    
    //Assign a unique handle
    if(newEvent)
        newEvent->eventHandle = getMsClock();

    return newEvent;
}
//#endif

STATUS delEventSchedulerInfo(void *eventSchedulerInfoToFree)
{
    fnDumpStringToSystemLog("=== Delete Event Schedule Info");
    return deleteNvmBlock(eventSchedulerInfoToFree);
}

//NVM heap related functions here
void initNvmHeap(void)
{
NVM_HEAP *nvmHeapPtrPtr = (NVM_HEAP *)getNvmHeapAddress();

	memset(nvmHeapPtrPtr, '\0', getSizeOfNvmHeap());

}
    
void *newNvmBlock(size_t howManyBytes)
{
void *returnPtr = NULL;

    if(howManyBytes <= (sizeof(NVM_SMALL_BLOCK) - 4))   
        returnPtr = getNvmSmallBlock();

    if(returnPtr)
        return returnPtr;
    
    if(howManyBytes <= (sizeof(NVM_MEDIUM_BLOCK) - 4))   
        returnPtr = getNvmMediumBlock();

    if(returnPtr)
        return returnPtr;
    
    if(howManyBytes <= (sizeof(NVM_LARGE_BLOCK) - 4))   
        returnPtr = getNvmLargeBlock();

    return returnPtr;
}


// ********************************************************************************
// FUNCTION NAME: deleteNvmBlock()                             
// PURPOSE:     
//    Looks through the heaps of nvmSmallBlocks, nvmMediumBlocksall, and nvmLargeBlocks 
//    to find one that has a matching ID (address of the block itself.) 
//      When found, all the data in that block is cleared AND the "allocated" indicator
//      is cleared, releasing this block for future use.
//              
// INPUTS:      
//      NvmBlockToFree - Block ID of the block to "delete" (release).  This is 
//                  in fact the starting address of the block. 
//
// OUTPUTS:     
//      
//  Return:     Returns NU_SUCCESS if it found the Nvm block to release.
//              Returns NU_INVALID_MEMORY if the block was not found.
//                                                                                BBl
// NOTES:
//  1.  Once the block with the matching ID is found, the routine does not check
//      any other blocks.
// HISTORY:
//  2014.10.02 - Modified to clear the block BEFORE releasing posession of it.
//---------------------------------------------------------------------
STATUS deleteNvmBlock(void *NvmBlockToFree)
{
int loopIndex;
NVM_HEAP *nvmHeapPtr = (NVM_HEAP *)getNvmHeapAddress();
    
    //First make sure the pointer is not NULL
    if(!NvmBlockToFree)
        taskException("NULL Pointer", THISFILE, __LINE__);    //Going to change this to return error   

    for(loopIndex = 0; loopIndex < ARRAY_LENGTH(nvmHeapPtr->nvmSmallBlocks); loopIndex++)
        if(NvmBlockToFree == nvmHeapPtr->nvmSmallBlocks[loopIndex].nvmSmallBlock)
            {
            memset(nvmHeapPtr->nvmSmallBlocks[loopIndex].nvmSmallBlock, '\0', sizeof(nvmHeapPtr->nvmSmallBlocks[loopIndex].nvmSmallBlock));
            nvmHeapPtr->nvmSmallBlocks[loopIndex].allocated = 0;
            return NU_SUCCESS;
            }

    for(loopIndex = 0; loopIndex < ARRAY_LENGTH(nvmHeapPtr->nvmMediumBlocks); loopIndex++)
        if(NvmBlockToFree == nvmHeapPtr->nvmMediumBlocks[loopIndex].nvmMediumBlock)
            {
            memset(nvmHeapPtr->nvmMediumBlocks[loopIndex].nvmMediumBlock, '\0', sizeof(nvmHeapPtr->nvmMediumBlocks[loopIndex].nvmMediumBlock));
            nvmHeapPtr->nvmMediumBlocks[loopIndex].allocated = 0;
            return NU_SUCCESS;
            }

    for(loopIndex = 0; loopIndex < ARRAY_LENGTH(nvmHeapPtr->nvmLargeBlocks); loopIndex++)
        if(NvmBlockToFree == nvmHeapPtr->nvmLargeBlocks[loopIndex].nvmLargeBlock)
            {
            memset(nvmHeapPtr->nvmLargeBlocks[loopIndex].nvmLargeBlock, '\0', sizeof(nvmHeapPtr->nvmLargeBlocks[loopIndex].nvmLargeBlock));
            nvmHeapPtr->nvmLargeBlocks[loopIndex].allocated = 0;
            return NU_SUCCESS;
            }

    return NU_INVALID_MEMORY;
}

void *getNvmSmallBlock(void)
{
int loopIndex;
NVM_HEAP *nvmHeapPtr = (NVM_HEAP *)getNvmHeapAddress();
	
    for(loopIndex = 0; loopIndex < ARRAY_LENGTH(nvmHeapPtr->nvmSmallBlocks); loopIndex++)
    {
        if(!nvmHeapPtr->nvmSmallBlocks[loopIndex].allocated)
        {
            nvmHeapPtr->nvmSmallBlocks[loopIndex].allocated = 1;
            return nvmHeapPtr->nvmSmallBlocks[loopIndex].nvmSmallBlock;
        }   
    }
        return NULL;    //All blocks allocated
}

void *getNvmMediumBlock(void)
{
int loopIndex;
NVM_HEAP *nvmHeapPtr = (NVM_HEAP *)getNvmHeapAddress();
	
    for(loopIndex = 0; loopIndex < ARRAY_LENGTH(nvmHeapPtr->nvmMediumBlocks); loopIndex++)
    {
        if(!nvmHeapPtr->nvmMediumBlocks[loopIndex].allocated)
        {
            nvmHeapPtr->nvmMediumBlocks[loopIndex].allocated = 1;
            return nvmHeapPtr->nvmMediumBlocks[loopIndex].nvmMediumBlock;
        }   
    }
        return NULL;    //All blocks allocated
}

void *getNvmLargeBlock(void)
{
int loopIndex;
NVM_HEAP *nvmHeapPtr = (NVM_HEAP *)getNvmHeapAddress();
	
    for(loopIndex = 0; loopIndex < ARRAY_LENGTH(nvmHeapPtr->nvmLargeBlocks); loopIndex++)
    {
        if(!nvmHeapPtr->nvmLargeBlocks[loopIndex].allocated)
        {
            nvmHeapPtr->nvmLargeBlocks[loopIndex].allocated = 1;
            return nvmHeapPtr->nvmLargeBlocks[loopIndex].nvmLargeBlock;
        }   
    }
        return NULL;    //All blocks allocated
}


//Returns the number of scheduled events
int getNumberOfScheduledEvents(void)
{
int numberOfScheduledEvents = 0;
EVENT_SCHEDULER_INFO *eventPtr = getScheduledEventsHeadPtr();

    while(eventPtr)
    {
        numberOfScheduledEvents++;
        eventPtr = eventPtr->nextEvent;    
    }
    
    return numberOfScheduledEvents;
}

//Retrieve a particular event in the chain 1 - X where 1 is the soonest
const EVENT_SCHEDULER_INFO *getEventInfo(int eventNumber)
{
int loopindex;
const EVENT_SCHEDULER_INFO *eventPtr;

    if(eventNumber > getNumberOfScheduledEvents())
        return NULL;

    eventPtr = getScheduledEventsHeadPtr();
    
    for(loopindex = 1; loopindex < eventNumber; loopindex++)
    {
        eventPtr = eventPtr->nextEvent;
    }

    return eventPtr;
}



//Test stuff down here

//Test clock thingy    
    
//This is just for a quick test and does not fully support day/month/year rollover
void addMinutes(DATETIME *time, int minutes)
{
    if(time->bMinutes + minutes < 60)
         time->bMinutes += minutes;
    
    else
    {
         time->bMinutes += minutes;
         time->bMinutes = time->bMinutes % 60;
         if(time->bHour < 23)
             time->bHour++;
         else 
         {
             time->bHour = 0;
             time->bDay++; //the hell with testing on midnight on the last day of the month!
         }
    }
}

#ifndef JANUS
//Test clock thingy    
void testScheduler(void)
{    
DATETIME now;

EVENT_SCHEDULER_INFO *theInfo;
EVENT_SCHEDULER_INFO *theInfo2;
EVENT_SCHEDULER_INFO *theInfo3;
EVENT_SCHEDULER_INFO *theInfo4;

    theInfo = newEventSchedulerInfo();
    if(!theInfo)
            taskException("newEventSchedulerInfo() failure", THISFILE, __LINE__);    //Might change this to return error   
        
    theInfo2 = newEventSchedulerInfo();
    if(!theInfo2)
            taskException("newEventSchedulerInfo() failure", THISFILE, __LINE__);    //Might change this to return error   
        
    theInfo3 = newEventSchedulerInfo();
    if(!theInfo3)
            taskException("newEventSchedulerInfo() failure", THISFILE, __LINE__);    //Might change this to return error   
        
    theInfo4 = newEventSchedulerInfo();
    if(!theInfo4)
            taskException("newEventSchedulerInfo() failure", THISFILE, __LINE__);    //Might change this to return error   
        
    sprintf(scratchPad, "Event scheduler = %d bytes.", sizeof(EVENT_SCHEDULER_INFO)); 
    putMsg(scratchPad, nextLine());

    sprintf(scratchPad, "Stack size = %u bytes.", NU_Check_Stack()); 
    putMsg(scratchPad, nextLine());
    
    //OSWakeAfter(30000);      //Need time to be sure RTC is up

    if(!GetSysDateTime(&now, ADDOFFSETS, GREGORIAN))
        taskException("GetSysDateTime() failure", THISFILE, __LINE__);    //Might change this to return error   

    addMinutes(&now, 3);
    
    theInfo->eventTime = now;
    memcpy(theInfo->eventDescription, "Some dumb event occurred!", strlen("Some dumb event occurred!"));
    memcpy(theInfo->registeredCallBackFunction, "dummyFunction", strlen("dummyFunction"));
    theInfo->eventDataPtr = NULL;
        
    scheduleEvent(theInfo);
    
    if(!GetSysDateTime(&now, ADDOFFSETS, GREGORIAN))
        taskException("GetSysDateTime() failure", THISFILE, __LINE__);    //Might change this to return error   

    addMinutes(&now, 5);
    
    theInfo2->eventTime = now;
    memcpy(theInfo2->eventDescription, "Another real dumb event occurred!", strlen("Another real dumb event occurred!"));
    memcpy(theInfo2->registeredCallBackFunction, "dummyFunction2", strlen("dummyFunction2"));
    theInfo2->eventDataPtr = NULL;
        
    scheduleEvent(theInfo2);
    
    if(!GetSysDateTime(&now, ADDOFFSETS, GREGORIAN))
        taskException("GetSysDateTime() failure", THISFILE, __LINE__);    //Might change this to return error   

    addMinutes(&now, 4);
    
    theInfo3->eventTime = now;
    memcpy(theInfo3->eventDescription, "An even dumber event occurred!", strlen("An even dumber event occurred!"));
    memcpy(theInfo3->registeredCallBackFunction, "anotherHandle", strlen("anotherHandle"));
    theInfo3->eventDataPtr = NULL;
        
    scheduleEvent(theInfo3);
    
    if(!GetSysDateTime(&now, ADDOFFSETS, GREGORIAN))
        taskException("GetSysDateTime() failure", THISFILE, __LINE__);    //Might change this to return error   

    addMinutes(&now, 6);
    
    theInfo4->eventTime = now;
    memcpy(theInfo4->eventDescription, "The dumbest event of all occurred!", strlen("The dumbest event of all occurred!"));
    memcpy(theInfo4->registeredCallBackFunction, "anotherHandle2", strlen("anotherHandle2"));
    theInfo4->eventDataPtr = NULL;
        
    scheduleEvent(theInfo4);
    
}    
    
#endif



    
//End Test clock thingies    








#endif    
    
