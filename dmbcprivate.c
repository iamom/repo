/************************************************************************
    PROJECT:        Horizon CSD
    MODULE NAME:    dmbcprivate.c 
    
    DESCRIPTION:    Private PreMessage and PostMessage functions for the bob task.
                    
 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
 
*************************************************************************/

#include <stdio.h>
#include <string.h>

#include "DmBcPublic.h"		// must be first
#include "DmBcPrivate.h"
#include "ImageErrs.h"
#include "pbos.h"


extern const unsigned char ByteMirror[256];

/* Maximum number of ECC words */
#define MAX_ECC_WORDS 0x44

/* Data Matrix Randomizer value */
#define RANDOMIZER_VALUE    149
#define DOUBLE_DIGIT_OFFSET 130
#define UPPER_DIGIT         0x39
#define LOWER_DIGIT         0x30

/* Data Matrix "Structured Append" mode overhead for Base 256 encoding */
#define DATAMATRIX_SINGLE_OVERHEAD  2

#define START_MASK       0x8000

/* Data Matrix Definitions */
#define PAD         129
#define LATCH_C40   230
#define LATCH_256   231
#define LATCH_ASCII 254

// structure for planning how to do the actual encoding
typedef struct
{
	BOOL	fbDoAscii;
	unsigned short	usByteCount;
} BARCODE_ENCODING_INSTRUCTIONS; 

#define MAX_SWITCHES	10
#define MAX_DATA_2DBC	174	   /* Maximum data size for a 48*48 2D BC          */

/* Structure to handle pre-defined Data Matrix barcode information */
typedef struct {
    UINT8   bSymbolRows;       /* Number of rows in symbol                     */
    UINT8   bSymbolColumns;    /* Number of columns in symbol                  */
    UINT8   bVerticalDataRegions;   /* Number of vert. data regions in symbol  */
    UINT8   bHorizontalDataRegions; /* Number of horiz. data regions in symbol */
    SINT8   bRowWrapOffset;    /* Adjustment factor for row when column wraps  */
    SINT8   bColWrapOffset;    /* Adjustment factor for column when row wraps  */
    UINT16  wDataCodeWords;    /* Number of data code words in the symbol      */
    UINT16  wEccWords;         /* Number of ECC words in the symbol            */
    UINT16  wBitmapSize;       /* Number of bytes used to store DM bitmap      */
	UINT16	wThinningMask[4];
} BARCODE_INFO;

/* Number of entries in the barcodeTable, this must change when the number */
/* of entries in the barcodeTable is changed */    
#define BARCODE_TABLE_ENTRIES	20

/* This table is used to define the various components needed to handle */
/* various DM barcode sizes.                                            */
const BARCODE_INFO sBarcodeTable[BARCODE_TABLE_ENTRIES] =
{
	/* row, col, vdr, hdr, rwo, cwo, dcw, ecw, bmpSize, thinning masks */ 
	{  8,  18,   1,   1,   0,  -2,   5,   7,  32, {0x9900, 0x0000, 0x0000, 0x0000}},
	{  8,  32,   1,   2,   0,  -2,  10,  11,  56, {0x9900, 0x0000, 0x0000, 0x0000}},
	{ 10,  10,   1,   1,   0,   0,   3,   5,  16, {0x8C40, 0x0000, 0x0000, 0x0000}},
	{ 12,  12,   1,   1,   2,   2,   5,   7,  20, {0x8610, 0x0000, 0x0000, 0x0000}},
	{ 12,  26,   1,   1,   0,   2,  16,  14,  48, {0x8610, 0x0000, 0x0000, 0x0000}},
	{ 12,  36,   1,   2,   0,   2,  22,  18,  64, {0x8610, 0x0000, 0x0000, 0x0000}},
	{ 14,  14,   1,   1,  -4,  -4,   8,  10,  24, {0x8304, 0x0000, 0x0000, 0x0000}},
	{ 16,  16,   1,   1,  -2,  -2,  12,  12,  28, {0x8181, 0x0000, 0x0000, 0x0000}},
	{ 16,  36,   1,   2,   0,  -2,  32,  24,  64, {0x8181, 0x0000, 0x0000, 0x0000}},
	{ 16,  48,   1,   2,  -4,  -2,  49,  28,  96, {0x8181, 0x0000, 0x0000, 0x0000}},
	{ 18,  18,   1,   1,   0,   0,  18,  14,  32, {0x80C0, 0x4000, 0x0000, 0x0000}},
	{ 20,  20,   1,   1,   2,   2,  22,  18,  72, {0x8060, 0x1000, 0x0000, 0x0000}},
	{ 22,  22,   1,   1,  -4,  -4,  30,  20,  80, {0x8030, 0x0400, 0x0000, 0x0000}},
	{ 24,  24,   1,   1,  -2,  -2,  36,  24,  88, {0x8018, 0x0100, 0x0000, 0x0000}},
	{ 26,  26,   1,   1,   0,   0,  44,  28,  96, {0x800C, 0x0040, 0x0000, 0x0000}},
	{ 32,  32,   2,   2,  -4,  -4,  62,  36, 112, {0x8001, 0x8001, 0x0000, 0x0000}},
	{ 36,  36,   2,   2,   0,   0,  86,  42, 128, {0x8000, 0x6000, 0x1000, 0x0000}},
	{ 40,  40,   2,   2,  -4,  -4, 114,  48, 216, {0x8000, 0x1800, 0x0100, 0x0000}},
	{ 44,  44,   2,   2,   0,   0, 144,  56, 240, {0x8000, 0x0600, 0x0010, 0x0000}},
	{ 48,  48,   2,   2,  -4,  -4, 174,  68, 264, {0x8000, 0x0180, 0x0001, 0x0000}}
};  

/* ECC Generator Polynomial Terms */

const UINT8  bPoly5[5]   = {  62, 111,  15,  48, 228 };

const UINT8  bPoly7[7]   = { 254,  92, 240, 134, 144,  68,  23};

const UINT8  bPoly10[10] = {  61, 110, 255, 116, 248, 223, 166, 185,  
                             24,  28 };

const UINT8  bPoly11[11] = { 120,  97,  60, 245,  39, 168, 194,  12,
                            205, 138, 175 };

const UINT8  bPoly12[12] = { 242, 100, 178,  97, 213, 142,  42,  61,
                             91, 158, 153,  41 };

const UINT8  bPoly14[14] = { 185,  83, 186,  18,  45, 138, 119, 157,
                              9,  95, 252, 192,  97, 156 };

const UINT8  bPoly18[18] = { 188,  90,  48, 225, 254,  94, 129, 109, 
                            213, 241,  61,  66,  75, 188,  39, 100,
                            195,  83 };

const UINT8  bPoly20[20] = { 172, 186, 174,  27,  82, 108,  79, 253,
                           145, 153, 160, 188,   2, 168,  71, 233, 
                              9, 244, 195,  15 };

const UINT8  bPoly24[24] = { 193,  50,  96, 184, 181,  12, 124, 254,
                            172,   5,  21, 155, 223, 251, 197, 155, 
                             21, 176,  39, 109, 205,  88, 190,  52 };

const UINT8  bPoly28[28] = { 255,  93, 168, 233, 151, 120, 136, 141, 
                            213, 110, 138,  17, 121, 249,  34,  75, 
                             53, 170, 151,  37, 174, 103,  96,  71,
                             97,  43, 231, 211 };

const UINT8  bPoly36[36] = { 112,  81,  98, 225,  25,  59, 184, 175, 
                             44, 115, 119,  95, 137, 101,  33,  68, 
                              4,   2,  18, 229, 182,  80, 251, 220, 
                            179,  84, 120, 102, 181, 162, 250, 130, 
                            218, 242, 127, 245 };

const UINT8  bPoly42[42] = {   5,   9,   5, 226, 177, 150,  50,  69,
                            202, 248, 101,  54,  57, 253,   1,  21, 
                            121,  57, 111, 214, 105, 167,   9, 100, 
                             95, 175,   8, 242, 133, 245,   2, 122, 
                            105, 247, 153,  22,  38,  19,  31, 137, 
                            193,  77 };

const UINT8  bPoly48[48] = {  19, 225, 253,  92, 213,  69, 175, 160,
                            147, 187,  87, 176,  44,  82, 240, 186, 
                            138,  66, 100, 120,  88, 131, 205, 170, 
                             90,  37,  23, 118, 147,  16, 106, 191, 
                             87, 237, 188, 205, 231, 238, 133, 238, 
                             22, 117,  32,  96, 223, 172, 132, 245 };

const UINT8  bPoly56[56] = {  46, 143,  53, 233, 107, 203,  43, 155, 
                             28, 247,  67, 127, 245, 137,  13, 164, 
                            207,  62, 117, 201, 150,  22, 238, 144,
                            232,  29, 203, 117, 234, 218, 146, 228, 
                             54, 132, 200,  38, 223,  36, 159, 150, 
                            235, 215, 192, 230, 170, 175,  29, 100,
                            208, 220,  17,  12, 238, 223,   9, 175 };

const UINT8  bPoly62[62] = { 204,  11,  47,  86, 124, 224, 166,  94,
                              7, 232, 107,   4, 170, 176,  31, 163, 
                             17, 188, 130,  40,  10,  87,  63,  51, 
                            218,  27,   6, 147,  44, 161,  71, 114, 
                             64, 175, 221, 185, 106, 250, 190, 197, 
                             63, 245, 230, 134, 112, 185,  37, 196, 
                            108, 143, 189, 201, 188, 202, 118,  39, 
                            210, 144,  50, 169,  93, 242 };

const UINT8  bPoly68[68] = { 186,  82, 103,  96,  63, 132, 153, 108,
                             54,  64, 189, 211, 232,  49,  25, 172,
                             52,  59, 241, 181, 239, 223, 136, 231,
                            210,  96, 232, 220,  25, 179, 167, 202,
                            185, 153, 139,  66, 236, 227, 160,  15, 
                            213,  93, 122,  68, 177, 158, 197, 234, 
                            180, 248, 136, 213, 127,  73,  36, 154, 
                            244, 147,  33,  89,  56, 159, 149, 251,
                             89, 173, 228, 220 };

/* Storage for barcode information */
#define BARCODE_MEMORY_SIZE     0x0800      /* 2048 */

static  UINT8	*pbJanusBitmapPtr;

static  BARCODE	sDmBarCode;
static 	BARCODE	*psBarCode = &sDmBarCode;    /* pointer to  barcode information */
static	BARCODE_SYMBOL *psSymbol = &(sDmBarCode.sBarcodeSymbols);       /* pointer to a barcode symbol */

static	UINT32	lwThinningMask[2];

static	UINT8   bDmMemory[BARCODE_MEMORY_SIZE];   /* Reserve memory for the barcode */
static	UINT8   *pbDmMemory = NULL;				// reset for each barcode generation

		UINT32  lwAlignData[2] = {0, 0};	/* Holds the module information for        */
											/* the alignment col. of barcode data      */
		UINT32  lwBlackData[2] = {0, 0};	/* Holds the module information for        */
											/* the all-black col. of barcode data      */

static  BOOL    fbBarCodeAvailable = FALSE;


/* Randomizer variables */
static UINT16  wRandom253;     /* 253-State Randomizer */
static UINT16  wRandom255;     /* 255-State Randomizer */

//****************************************
//****************************************
//****************************************
//#define	BARCODE_DEBUG	1

#ifdef BARCODE_DEBUG
/* Structure to handle pre-defined Data Matrix barcode information */
typedef struct {
	UINT32	lwEncodedData[2];
	UINT32	lwCurData[2];
} DEBUG_INFO;

/* This table is used to define the various components needed to handle */
/* various DM barcode sizes.                                            */
DEBUG_INFO sDebugTable[48];
#endif
//****************************************
//****************************************
//****************************************

// Internal minor/short/small subroutines
UINT32	AllocateBarcodeMemory(UINT8 **pbMemory, const UINT16 wSizeRequested, 
                                const BOOL fbOn8ByteBoundary);
void	FillPattern (void);
UINT32	FindBarcodeInfo(const UINT8 bNumRows, const UINT8 bNumCols,
						const BARCODE_INFO **ppsTableEntry);

#ifdef SUPPORT_PDF417
UINT16	fnECCAdd(const UINT16 wA, const UINT16 wB, const UINT8 bMode);
UINT16	fnECCComp(const UINT16 wA, const UINT8 bMode);
UINT16	fnECCMult(const UINT16 wA, const UINT16 wB, const UINT8 bMode);
UINT16	fnPDFmult(const UINT16 wA, const UINT16 wB);
#endif

UINT16	fnPolyMult_Fast(UINT8 bA, const UINT8 bB);
#define	IncRand253	((wRandom253 = (wRandom253 + RANDOMIZER_VALUE) % 253))
#define	IncRand255	((wRandom255 = (wRandom255 + RANDOMIZER_VALUE) % 255))

void	Module(INT8 bRow, INT8 bCol, const INT8 bNRow, const INT8 bNCol,
				INT8 **ppbArray);
void	PokeBits(UINT8 *pbTarget, const UINT16 wBitTarget, UINT8 bBitCount);
void	ZeroDataMatrixMemory(UINT16 *pwDMMemory, const UINT16 wDmMemSize);


// Internal major/long/large subroutines
void BitmapGen(const UINT8 bHorizontalExp, const UINT8 bVerticalExp, const UINT16 wNumModules,
			   const UINT16 *pwBitmapData, const UINT8 bVThinning, const UINT16 wSpaceAbove,
			   const UINT16 wSpaceBelow, const UINT8 bRowBytes, const BOOL fDoAnyThinning,
			   const UINT8 bWhiteSpace);
void BitPlace(const SINT8 bDmRow, const SINT8 bDmCol, const UINT8 bInputData);
void DMGenRotated(const UINT8 *pbPrintBuf, const UINT8 bRowBytes);
void CornerPlace(const SINT8 bCornerCase, const UINT8 bInputData);
void DoAlign(const UINT8 bNumRow, const SINT8 bExtraModules, UINT16 *pwBitmap);
void DoBlack(const UINT8 bNumRow, const SINT8 bExtraModules, UINT16 *pwBitmap);
void DoColumn(const UINT8 bColCnt, const UINT8 bNumRow, const UINT8 bRowsInArea,
               const SINT8 bExtraModules, const UINT16 *pwBitmapData, UINT16 *pwBitmap);
void DoRotatedColumn(const UINT8 bColCnt, const UINT8 bNumRow, const UINT8 bRowsInArea,
               const SINT8 bExtraModules, const UINT16 *pwBitmapData, UINT16 *pwBitmap,
			   const UINT8 bWordsInCol, const UINT8 bSourceRowNum);
void PlaceOnBitMap(const UINT16 wNumDataItems,
                   const UINT16 *pwData, const SINT8 *pbLookupEntry);


/*****************************************************************************
//                           MINOR SUBROUTINES
*****************************************************************************/
/**********************************************************************
DESCRIPTION:
The AllocateBarcodeMemory function allocates a section of the Barcode 
memory block if it will fit.
	 
INPUT PARAMS:
	pbMemory         - pointer to a pointer to place the address of the  
                      allocated memory.
	wSizeRequested   - size of the requested memory block.
    fbOn8ByteBoundary - If true the allocated memory needs to start on an 8 byte 
                      boundary.                      

RETURN VALUE:
    UINT32    - results of the barcode memory allocation.
**********************************************************************/

UINT32 AllocateBarcodeMemory(UINT8 **pbMemory, const UINT16 wSizeRequested,
							   const BOOL fbOn8ByteBoundary)
{
    UINT32 lError = IG_NO_ERROR;


    /* Check if allocated memory needs to start on an 8 byte boundary */
    if ((fbOn8ByteBoundary) && (((UINT32) pbDmMemory) & 0x07))
    {
        /* align on 8 byte boundary */
        pbDmMemory = (UINT8 *)(((((UINT32) pbDmMemory) / 8) + 1) * 8);
    }
    
    if (((UINT32) pbDmMemory + wSizeRequested) > 
        ((UINT32) bDmMemory + BARCODE_MEMORY_SIZE))
    {
        lError = NOT_ENOUGH_BARCODE_MEMORY;
    }
    else
    {
        /* Allocate the memory */
        *pbMemory = pbDmMemory;
        pbDmMemory += wSizeRequested;
    }
    
    return (lError);
}


/**********************************************************************
DESCRIPTION:
The BarCodeAvailable function returns the status of the fbBarCodeAvailable flag.
	 
INPUT PARAMS:
    None.

RETURN VALUE:
    BOOL - TRUE if barcode data is available, otherwise FALSE.
**********************************************************************/

BOOL BarCodeAvailable(void)
{
    return (fbBarCodeAvailable);
}


/**********************************************************************
DESCRIPTION:
The ChangeNumDataBytes function alters the number of data bytes that will be encoded.
	 
INPUT PARAMS:
    usBarcodeDataLen = number of data bytes to encode.

RETURN VALUE:
    None
**********************************************************************/

void ChangeNumDataBytes(const unsigned short usBarcodeDataLen)
{
	psBarCode->wNumberData = usBarcodeDataLen;
}


/**********************************************************************
DESCRIPTION:
The FillPattern function fills in unused area of Data Matrix bitmap with  
a fixed pattern.
	 
INPUT PARAMS:
    None.
        
RETURN VALUE:
    None.    
**********************************************************************/

void FillPattern (void)
{
    UINT16 *pwDM_Ptr; 
    UINT16 wSetBit;
    UINT8  bRow;
    UINT8  bRowPos;


    bRow = (psSymbol->bSymbolRows) - (UINT8)(2 * psSymbol->bHorizontalDataRegions);
          
    /* Set bit in last row, first column from the right */ 
    pwDM_Ptr = psSymbol->pwBitmap + ((bRow - 1) / 16);

    /* find last row */
    bRowPos = (bRow - 1) & 0x0f;
    wSetBit = 0x0001;
    wSetBit = wSetBit << bRowPos;

    *pwDM_Ptr |= wSetBit; /* set bit */

    /* set bit in 2nd to last row, 2nd column */
    /* move to 2nd column */
    pwDM_Ptr += ((bRow - 1) / 16) + 1;

    /* move to 2nd to last row */
    wSetBit /= 2;

    *pwDM_Ptr |= wSetBit; /* set bit */
    return;
}


/**********************************************************************
DESCRIPTION:
The FindBarcodeInfo function searches the barcode information table to
locate additional information needed on a symbol.  It will return the correct
table entry if the symbol is found.
	 
INPUT PARAMS:
	bNumRows      - Number of rows in the symbol.
	bNumCols      - Number of columns in the symbol.
	ppsTableEntry - Pointer to a pointer to a barcode info table entry that
                   is used to return the results of the search .

RETURN VALUE:
    UINT32    - results of the info table look up.
**********************************************************************/

UINT32 FindBarcodeInfo(const UINT8 bNumRows, const UINT8 bNumCols,
						 const BARCODE_INFO **ppsTableEntry)
{
    UINT32 lError = IG_NO_ERROR;
    UINT32   wIndex = 0;
    BOOL     fbDone = FALSE;

    
    /* Search the table for the matching size symbol */
    while ((wIndex < BARCODE_TABLE_ENTRIES) && (!fbDone))
    {
        if ((sBarcodeTable[wIndex].bSymbolRows == bNumRows) && 
            (sBarcodeTable[wIndex].bSymbolColumns == bNumCols))
            fbDone = TRUE;
        
        if (!fbDone)
            wIndex++;
    }
    
    if (!fbDone)
        lError = UNSUPPORTED_DM_SYMBOL_SIZE;
    else
        *ppsTableEntry = &sBarcodeTable[wIndex];    
    
    return (lError);
}




/**********************************************************
 * short fnPolyMult_Fast(char, char)
 * Fast version of the fnPolyMult Polynomial Multiply
 * function shown above
 **********************************************************/

UINT16 fnPolyMult_Fast(UINT8 bA, const UINT8 bB)
{
    UINT16 wTest;
    UINT16 wAB = 0;


	/* multiply */

    if (bA & 0x01)
		wAB = (bB * 256) ^ wAB;
	wAB /= 2;
    bA /= 2;
    if (bA & 0x01)
		wAB = (bB * 256) ^ wAB;
	wAB /= 2;
    bA /= 2;
    if (bA & 0x01)
		wAB = (bB * 256) ^ wAB;
	wAB /= 2;
    bA /= 2;
    if (bA & 0x01) 
		wAB = (bB * 256) ^ wAB;
	wAB /= 2;
    bA /= 2;
    if (bA & 0x01) 
		wAB = (bB * 256) ^ wAB;
	wAB /= 2;
    bA /= 2;
    if (bA & 0x01) 
		wAB = (bB * 256) ^ wAB;
	wAB /= 2;
    bA /= 2;
    if (bA & 0x01) 
		wAB = (bB * 256) ^ wAB;
	wAB /= 2;
    bA /= 2;
    if (bA & 0x01) 
		wAB = (bB * 256) ^ wAB;
	wAB /= 2;
    bA /= 2;

    /* divide */

    wTest = wAB & 0x8000;
    wAB *= 2;
    if (wTest) 
		wAB ^= 0x2d00;
	wTest = wAB & 0x8000;
    wAB *= 2;
    if (wTest) 
		wAB ^= 0x2d00;
	wTest = wAB & 0x8000;
    wAB *= 2;
    if (wTest) 
		wAB ^= 0x2d00;
	wTest = wAB & 0x8000;
    wAB *= 2;
    if (wTest) 
		wAB ^= 0x2d00;
	wTest = wAB & 0x8000;
    wAB *= 2;
    if (wTest) 
		wAB ^= 0x2d00;
	wTest = wAB & 0x8000;
    wAB *= 2;
    if (wTest) 
		wAB ^= 0x2d00;
	wTest = wAB & 0x8000;
    wAB *= 2;
    if (wTest) 
		wAB ^= 0x2d00;
	wTest = wAB & 0x8000;
    wAB *= 2;
    if (wTest) 
		wAB ^= 0x2d00;

	// the result is in the upper byte and the lower byte is zero.
	// need to move the upper byte to the lower byte.
    return (wAB / 0x100);
}


/************************** InitializeBitMap **************************/
void InitializeBitMap(void)
{
    UINT16	wLength;

    /* For the symbol in the Barcode, initialize its bitmap */
	/* Determine the length of the bitmap for this symbol */
	wLength = ((psSymbol->bSymbolRows) - 
					 (2 * psSymbol->bHorizontalDataRegions + 1)) / 16;
	wLength ++;
	wLength *= ((psSymbol->bSymbolColumns) - 
					  (2 * psSymbol->bVerticalDataRegions));

	/* Clear the bitmap */
	ZeroDataMatrixMemory(psSymbol->pwBitmap, wLength);
 
	/* Check if there if there are unused bits in the symbol, 
		if so fill with pattern */
	if ((((psSymbol->bSymbolRows - (2 * psSymbol->bHorizontalDataRegions)) * 
		  (psSymbol->bSymbolColumns - (2 * psSymbol->bVerticalDataRegions))) %
		 8) == 4)
		FillPattern();

	/* Initialize ECC Memory(0 to K-1) = 0 */
	ZeroDataMatrixMemory(psSymbol->pwECCWords, psSymbol->wEccWords);

	// Generate the alignment & all-black columns
	if (!(psBarCode->bBarcodeType & GET_ROTATE_FLAG))
	{
		// Doing normal barcode
	    DoAlign(psSymbol->bSymbolRows, psSymbol->bExtraSpace, (unsigned short *)lwAlignData);	//lint !e740
	    DoBlack(psSymbol->bSymbolRows, psSymbol->bExtraSpace, (unsigned short *)lwBlackData);	//lint !e740
	}
	else
	{
		// Doing rotated barcode
	    DoAlign(psSymbol->bSymbolColumns, psSymbol->bExtraSpace, (unsigned short *)lwAlignData);	//lint !e740
	    DoBlack(psSymbol->bSymbolColumns, psSymbol->bExtraSpace, (unsigned short *)lwBlackData);	//lint !e740
	}
}


/**********************************************************************
DESCRIPTION:
The Module function adds an entry in the bit look up table.
	 
INPUT PARAMS:
    bRow     - row number
    bCol     - column number
    bNRow    - Total rows in this Data Matrix symbol
    bNCol    - Total columns in this Data Matrix symbol
    ppbArray - Pointer to the pointer to the bit look up table

RETURN VALUE:
    None.
**********************************************************************/

void Module(INT8 bRow, INT8 bCol, const INT8 bNRow, const INT8 bNCol, 
			INT8 **ppbArray)
{   
    /* row and/or col may be outside the boundary, if so correct */
    if (bRow < 0)
    { 
        bRow += bNRow; 
        bCol += 4 - ((bNRow + 4) % 8); 
    }

    if (bCol < 0) 
    { 
        bCol += bNCol; 
        bRow += 4 - ((bNCol + 4) % 8); 
    }

    /* place the entry in the table */
    **ppbArray = bRow;
    *ppbArray += 1;
    **ppbArray = (bNCol - bCol - 1);
    *ppbArray += 1;
}


/************************** PlaceDMDataWords **************************/

void PlaceDMDataWords(void)
{
    /* For this symbol in the Barcode, place the code words into the bitmap */
    PlaceOnBitMap (psSymbol->wDataCodeWords, psSymbol->pwCodeWords,
					psSymbol->pbBitLookupTable);
}


/************************** PlaceDMECCWords ***************************/

void PlaceDMECCWords(void)
{
    /* For this symbol in the Barcode, place the ecc words into the bitmap */
    PlaceOnBitMap(psSymbol->wEccWords, psSymbol->pwECCWords,
					psSymbol->pbBitLookupTable + (psSymbol->wDataCodeWords * 2));
}


/***************************** PokeBits ********************************
DESCRIPTION:
    Inserts a chunk of "black" bits into an array at the specified bit position.
	 
INPUT PARAMS:
    pbTarget		- Target array being modified
    wBitTarget		- Bit position within this array where the bits
    					should be placed (e.g. 19 would be bit 4 (00010000) in pbTarget[2])
    bBitCount		- Number of bits to set starting at that position
    
RETURN VALUE:
    None.    
**********************************************************************/
void PokeBits(UINT8 *pbTarget, const UINT16 wBitTarget, UINT8 bBitCount)
{
	register UINT16	wByteIndex;
	register UINT8	bBitMask;
	register UINT8	bBitOffset;
	register UINT8	bBitsDone;

	wByteIndex = wBitTarget / 8;			// get the starting byte index
	bBitOffset = wBitTarget % 8;
	bBitMask = 0xFF >> bBitOffset;		// get the starting mask
	bBitsDone = 8 - bBitOffset;

	if (bBitCount >= bBitsDone)
	{
		// we have more than just the current byte to fill
		pbTarget[wByteIndex++] |= bBitMask;
		bBitCount -= bBitsDone;

		while (bBitCount >= 8)
		{
			pbTarget[wByteIndex++] = 0xFF;
			bBitCount -= 8;
		}

		if (bBitCount)
		{
			pbTarget[wByteIndex] = 0xFF << (8 - bBitCount);
		}
	}
	else
	{
		pbTarget[wByteIndex] |= bBitMask & (0xFF << (bBitsDone - bBitCount));
	}

	return;
}


/**********************************************************************
DESCRIPTION:
The ZeroDataMatrixMemory function clears memory for the data matrix
calculation.  In the case of the bitmap area, this causes the default
state of the bitmap to be all zeros, so only the ones have to be placed.
	 
INPUT PARAMS:
    pwDMBitmap - Pointer to the memory.
    wDmMemSize - Size of the Data Matrix area.
        
RETURN VALUE:
    None.    
**********************************************************************/

void ZeroDataMatrixMemory (UINT16 *pwDMMemory, const UINT16 wDmMemSize)
{
	register UINT16	wInx;


	for (wInx = 0; wInx < wDmMemSize; wInx++, pwDMMemory++)
		*pwDMMemory = 0;
}


/*****************************************************************************
//                           MAJOR SUBROUTINES
*****************************************************************************/
/**********************************************************************
DESCRIPTION:
    The BitmapGen function expands the barcode bitmap to actual printing
	size.
	 
INPUT PARAMS:
    bHorizontalExp - Horizontal Expansion factor.
    bVerticalExp   - Vertical Exapansion factor.
    wNumModules    - Number of modules in the column.
    pwBitmapData   - Pointer to the data for generating the bitmap 
    bVThinning     - Number of bits to be thinned in the vertical direction
    wSpaceAbove    - Space above in barcode in pixels.
    wSpaceBelow    - Space below in barcode in pixels.
	bRowBytes      - Number of bytes for rows in each column of the print buffer.
	fDoAnyThinning - Indicates if thinning should be done. Overrules bVThinning & bDoWhiteSpace.
	bDoWhiteSpace  - Indicates if thinning should be done between adjacent black modules.
In addition:    
    pbJanusBitmapPtr - Pointer to the area where the bitmap should be stored.

RETURN VALUE:
    None.    
**********************************************************************/

void BitmapGen(const UINT8 bHorizontalExp, const UINT8 bVerticalExp, const UINT16 wNumModules,
               const UINT16 *pwBitmapData, const UINT8 bVThinning, const UINT16 wSpaceAbove,
			   const UINT16 wSpaceBelow, const UINT8 bRowBytes, const BOOL fDoAnyThinning,
			   const UINT8 bDoWhiteSpace)
{
	register UINT16	wInx;


	{
		UINT16	wData;
		register UINT16	wModuleMask;
		register UINT16 wPixelIndex;
		UINT16	wPrevBitValue = 1;		// used for vertical thinning

		UINT8	bNonThinExp;
		UINT8	bWhichModuleWord;


		// set up some initial data
		bNonThinExp = bVerticalExp - bVThinning;

		/* generate the column of data */
		/* skip the white space */
		wPixelIndex = wSpaceAbove;

		/* setup for first module */
		wModuleMask = START_MASK;
		bWhichModuleWord = 0;
		wData = pwBitmapData[bWhichModuleWord];

		/* map each module to a pattern in the bitmap */
		for (wInx = 0; wInx < wNumModules; wInx++)
		{
			UINT16	wBitValue;
			BOOL	fDoThinning;			// indicates if thinning should be done for a particular module


			if (!wModuleMask)
			{
				wModuleMask = START_MASK;
				bWhichModuleWord++;
				wData = pwBitmapData[bWhichModuleWord];
			}

			/* get the data bit and load it into the bitmap */
			wBitValue = wData & wModuleMask;

			// if there is a certain amount of thinning that should be done
			if (bVThinning)
			{
				// Don't thin the start & end alignment modules
				// They are the first module, the last module and the two middle modules
				if (!wInx || (wInx == (wNumModules-1)) ||
					(wInx == (wNumModules/2)) ||
					(wInx == ((wNumModules/2)-1)))
					fDoThinning = FALSE;
				else
					fDoThinning = TRUE;

				// if thinning should be done for this module -AND-
				// thinning is allowed for this column
				if (fDoThinning && fDoAnyThinning)
				{
					// If both the previous bit and the current bit are
					// black -AND- we don't want white space between two
					// black modules, the vertical thinning doesn't need to be
					// done, so load black bits into the thinning area.
					//
					// Otherwise, the thinning needs to be done or one of the
					// bits is white. In either case, we'd be loading
					// zeros into a buffer that already contains zeros,
					// so skip the load.
					if (wPrevBitValue && wBitValue && !bDoWhiteSpace)
						PokeBits(pbJanusBitmapPtr, wPixelIndex, bVThinning);
				}
				// else, we're not doing the thinning for this module or
				// or column, so see if we need to actually load anything
				else
				{
					// If the bit is black, load the black bits.
					// Otherwise, we'd be loading zeros, so skip the load.
					if (wBitValue)
						PokeBits(pbJanusBitmapPtr, wPixelIndex, bVThinning);
				}

				wPixelIndex += bVThinning;
			}

			// If the bit value is non-zero, load "black" bits.
			// Else, just increment the pixel index because the
			// buffer already contains zeros, which means it isn't 
			// necessary to load zeros.
			if (wBitValue)
				PokeBits(pbJanusBitmapPtr, wPixelIndex, bNonThinExp);

			wPixelIndex += bNonThinExp;
			wModuleMask /= 2;
			wPrevBitValue = wBitValue;
		}
	}


	{
		register UINT8	*pbPrevCol, *pbCurrCol, *pbStartCol;
		register UINT16	wBytesPerColumn;
		register UINT8	bInx, bTemp;


		wBytesPerColumn = (UINT16)((wSpaceAbove + wSpaceBelow + (wNumModules * bVerticalExp) + 7) / 8);
		bInx = 1;
		bTemp = bRowBytes;
		pbPrevCol = pbJanusBitmapPtr;
		pbStartCol = pbPrevCol + bTemp;

		/* repeat column for horizontal expansion factor */
		for ( ; bInx < bHorizontalExp; bInx++)
		{
			pbCurrCol = pbStartCol;
			for (wInx = 0; wInx < wBytesPerColumn; wInx++, pbPrevCol++, pbCurrCol++)
				*pbCurrCol = *pbPrevCol;

			pbPrevCol = pbStartCol;
			pbStartCol += bTemp;
		}

		pbJanusBitmapPtr = pbStartCol;
	}

	return;
}


/**********************************************************************
DESCRIPTION:
The BitPlace function places each bit into the bitmap format. 
The codeword will be placed directly into the bitmap memory in the following 
pattern where 1 is the msb and 8 is the lsb.
                 ---------
                 \ 1 \ 2 \
                 -------------
                 \ 3 \ 4 \ 5 \
                 -------------
                 \ 6 \ 7 \ 8 \
                 -------------
	 
INPUT PARAMS:
    bDmRow     - The row position for the 1st bit
    bDmCol     - The column position for the 1st bit
    bInputData - The codeword to be placed
        
RETURN VALUE:
    None.    
**********************************************************************/

void BitPlace(const SINT8 bDmRow, const SINT8 bDmCol, const UINT8 bInputData)
{
    UINT16 *pwBitmapStart;   /* Pointer to the beginning of the bitmap */
    UINT16 *pwDM_Ptr;        /* Bitmap memory pointer            */
    UINT16 *pwLastDM_Ptr;    /* Previous bitmap memory pointer   */

    SINT32  lAlignHeight;    /* # of rows of used for alignment  */
    SINT32  lAlignWidth;     /* # of cols of used for alignment  */
    SINT32  lCol;
    SINT32  lColLength;      /* # of data units to make a column */
    SINT32  lColumnWrapping; /* column Wrap                      */
    SINT32  lRow;
    SINT32  lRowPos;         /* offset of current row position   */

    UINT16  wOutputData;     /* temp output data                 */
    UINT16  wSetBit;         /* mask identifying the  current bit*/

    UINT8   bBit345Wrap;     /* Second row wrap around flag      */ 
    UINT8   bBit678Wrap;     /* Third row wrap around flag       */
    UINT8   bSymbolRows;     /* Rows in the barcode symbol       */


    lRow = bDmRow;
    lCol = bDmCol;
    
    /* Determine # of Cols and rows used for alignment */
    lAlignWidth  = 2 * psSymbol->bHorizontalDataRegions;
    lAlignHeight = 2 * psSymbol->bVerticalDataRegions;
    
    pwBitmapStart = psSymbol->pwBitmap;
    bSymbolRows = psSymbol->bSymbolRows; 
    
    /* # of data units per column */
    lColLength = ((bSymbolRows - lAlignHeight - 1) / 16) + 1;
	lColumnWrapping = lColLength - 1 + (lColLength * psSymbol->bColWrapOffset);
    
    /* update the bitmap pointer */
    pwDM_Ptr = lCol * lColLength + pwBitmapStart + (lRow / 16);
    pwLastDM_Ptr = pwDM_Ptr;
   
    /* Clear the wrap flags */
    bBit345Wrap = 0;
    bBit678Wrap = 0;
   
    /* Determine if any rows will wrap around */
    if (lRow == (bSymbolRows - lAlignHeight - 1))
        bBit345Wrap = 1;
    else if (lRow == (bSymbolRows - lAlignHeight - 2))
        bBit678Wrap = 1;

    /* Determine the offset into the row */
    lRowPos = lRow & 0x0f;

    /*******************************************************/
    /* bit 1, 3, 6 placement                               */
    /*******************************************************/
    /* setup mask */
    wSetBit = 0x0001;

    /* Adjust mask to place bit in correct position */
    wSetBit <<= lRowPos;

    /* get bitmap memory so the bit can be placed */
    wOutputData = *pwDM_Ptr;

    /* Place input bit 1 */
    if (bInputData & 0x80)
        wOutputData |=  wSetBit;
    
    /* Check if the row will wrap to the top of the symbol */
    if (!bBit345Wrap)
    {         
        /* No wrap */
        wSetBit *= 2;  /* set mask to next bit */
        if (!wSetBit) /* check if we need to get next data unit */
        {
            wSetBit = 0x0001;        /* reset mask                   */
            *pwDM_Ptr = wOutputData;  /* store current data and read  */
            pwDM_Ptr++;              /* in next partial data unit    */
            wOutputData = *pwDM_Ptr;
        }
    }
    else
    {
        /* This row of codeword is wraping to the top of the symbol */
        wSetBit = 0x0001;        /* reset mask */
        *pwDM_Ptr = wOutputData;  /* store current data and read in   */
                                /* next partial data unit           */
        pwDM_Ptr -= lColumnWrapping;
        wOutputData = *pwDM_Ptr;
    }
     
    /* Place input_bit_3 */
    if (bInputData & 0x20)
        wOutputData |=  wSetBit;

    /* Check if the row will wrap to the top of the symbol */
    if (!bBit678Wrap)
    {
        /* No wrap */
        wSetBit *= 2;  /* set mask to next bit */   
        if (!wSetBit)	//lint !e774
        {
            wSetBit = 0x0001;       /* reset mask                   */
            *pwDM_Ptr = wOutputData; /* store current data and read  */
            pwDM_Ptr++;              /* in next partial data unit    */
            wOutputData = *pwDM_Ptr;
        }
    }
    else
    {
        /* This row of codeword is wraping to the top of the symbol */
         wSetBit = 0x0001;       /* reset mask */
         *pwDM_Ptr = wOutputData; /* store current data and read in   */
                                /* next partial data unit           */
         pwDM_Ptr -= lColumnWrapping;
         wOutputData = *pwDM_Ptr;
    }

     /* Place input_bit_6 */
    if (bInputData & 0x04)
        wOutputData |=  wSetBit;

     /* store current data */ 
    *pwDM_Ptr = wOutputData;

    /*********************************************************/
    /* bit 2, 4, 7 placement                                 */
    /*********************************************************/
    /* Move to next column in the symbol */
    pwDM_Ptr = pwLastDM_Ptr - lColLength;
    lCol--;

    /* if the pointer reaches the right end of the symbol, */
    /* flip it over to the left end of the symbol.         */
    if (lCol == -1)
    {
        /* update the bitmap pointer */
        lCol += psSymbol->bSymbolColumns - lAlignWidth;
        pwDM_Ptr = lCol * lColLength + pwBitmapStart + 
                  ((lRow + psSymbol->bRowWrapOffset) / 16);
        lRowPos = (lRow + psSymbol->bRowWrapOffset) & 0x0f;
    }

    /* save the bitmap pointer for the next column */
    pwLastDM_Ptr = pwDM_Ptr;

    /* setup mask */
    wSetBit = 0x0001;

    /* Adjust mask to place bit in correct position */
    wSetBit <<= lRowPos;

    /* get bitmap memory so the bit can be placed */
    wOutputData = *pwDM_Ptr;

    /* Place input_bit_2  */
    if (bInputData & 0x40)
         wOutputData |=  wSetBit;

    /* Check if the row will wrap to the top of the symbol */
    if (!bBit345Wrap)
    { 
        /* No wrap */
        wSetBit *= 2;  /* set mask to next bit */
        if (!wSetBit) /* check if we need to get next data unit */
        {
            wSetBit = 0x0001;        /* reset mask                   */
            *pwDM_Ptr = wOutputData;  /* store current data and read  */
            pwDM_Ptr++;              /* in next partial data unit    */
            wOutputData = *pwDM_Ptr;
        }
    }
    else
    {
        /* This row of codeword is wraping to the top of the symbol */
        wSetBit = 0x0001;        /* reset mask */
        *pwDM_Ptr = wOutputData;  /* store current data and read in   */
                                /* next partial data unit           */
        pwDM_Ptr -= lColumnWrapping;
        wOutputData = *pwDM_Ptr;
    }

    /* Place input_bit_4  */
    if (bInputData & 0x10)
        wOutputData |=  wSetBit;
  
    /* Check if the row will wrap to the top of the symbol */
    if (!bBit678Wrap)
    {
        /* No wrap */
        wSetBit *= 2;  /* set mask to next bit */
        if (!wSetBit) /* check if we need to get next data unit */	//lint !e774
        {
            wSetBit = 0x0001;        /* reset mask                   */
            *pwDM_Ptr = wOutputData;  /* store current data and read  */
            pwDM_Ptr++;              /* in next partial data unit    */
            wOutputData = *pwDM_Ptr;
        }
    }
    else
    {
        /* This row of codeword is wraping to the top of the symbol */
        wSetBit = 0x0001;        /* reset mask */
        *pwDM_Ptr = wOutputData;  /* store current data and read in   */
                                /* next partial data unit           */
        pwDM_Ptr -= lColumnWrapping;
        wOutputData = *pwDM_Ptr;
    }

    /* Place input_bit_7  */
    if (bInputData & 0x02)
        wOutputData |=  wSetBit;

     /* store current data */ 
    *pwDM_Ptr = wOutputData;

    /******************************************************/
    /* bit 5, 8 placement                                 */
    /******************************************************/
    /* Move to next column in the symbol */
    pwDM_Ptr = pwLastDM_Ptr - lColLength;
    lCol--;

    /* if the pointer reaches the right end of the symbol, */
    /* flip it over to the left end of the symbol.         */
    if (lCol == -1)
    {
        /* update the bitmap pointer */
        lCol  += psSymbol->bSymbolColumns - lAlignWidth;
        pwDM_Ptr = lCol * lColLength + pwBitmapStart + 
                  ((lRow + psSymbol->bRowWrapOffset) / 16);
        lRowPos = (lRow + psSymbol->bRowWrapOffset) & 0x0f;
    }

    /* setup mask */
    wSetBit = 0x0001;

    /* Adjust mask to place bit in correct position */
    wSetBit <<= lRowPos;

    /* get bitmap memory so the bit can be placed */
    wOutputData = *pwDM_Ptr;

    /* Still need to shift 1 more bit down, that is   */
    /* handled next                                   */

    /* Check if the row will wrap to the top of the symbol */
    if (!bBit345Wrap)
    {
        /* No wrap */
        wSetBit *= 2;  /* set mask to next bit */
        if (!wSetBit) /* check if we need to get next data unit */
        {
            wSetBit = 0x0001;        /* reset mask                   */
            *pwDM_Ptr = wOutputData;  /* store current data and read  */
            pwDM_Ptr++;              /* in next partial data unit    */
            wOutputData = *pwDM_Ptr;
        }
    }
    else
    {
        /* This row of codeword is wraping to the top of the symbol */
        wSetBit = 0x0001;        /* reset mask */
        *pwDM_Ptr = wOutputData;  /* store current data and read in   */
                               /* next partial data unit           */
        pwDM_Ptr -= lColumnWrapping;
        wOutputData = *pwDM_Ptr;
    }

     /* Place input_bit_5  */
    if (bInputData & 0x08)
        wOutputData |=  wSetBit;

    /* Check if the row will wrap to the top of the symbol */
    if (!bBit678Wrap)
    {
        /* No wrap */
        wSetBit *= 2;  /* set mask to next bit */
        if (!wSetBit) /* check if we need to get next data unit */	//lint !e774
        {
            wSetBit = 0x0001;        /* reset mask                   */
            *pwDM_Ptr = wOutputData;  /* store current data and read  */
            pwDM_Ptr++;              /* in next partial data unit    */
            wOutputData = *pwDM_Ptr;
        }
    }
    else
    {
        /* This row of codeword is wraping to the top of the symbol */
        wSetBit = 0x0001;        /* reset mask */
        *pwDM_Ptr = wOutputData;  /* store current data and read in   */
                                /* next partial data unit           */
        pwDM_Ptr -= lColumnWrapping;
        wOutputData = *pwDM_Ptr;
    }

    /* Place input_bit_8  */
    if (bInputData & 0x01)
       wOutputData |=  wSetBit;
 
    /* store current data */ 
    *pwDM_Ptr = wOutputData;
    return;
}


/**********************************************************************
DESCRIPTION:
The CornerPlace function handles the special corner cases when placing each  
bit into the bitmap format.  The codeword will be placed directly into the  
bitmap memory in the following patterns where 1 is the msb and 8 is the lsb.

Special Case 1:                    Special Case 2:
------------- ... ---------         ----- ... -----------------
\                 \ 4 \ 5 \         \         \ 4 \ 5 \ 6 \ 7 \
-                 ---------         -         -----------------
\                     \ 6 \         \                     \ 8 \
-                     -----         -                     -----
\                     \ 7 \         .                         .
-                     -----         .                         .
\                     \ 8 \         .                         .
-                     -----         -----                     -
.                         .         \ 1 \                     \
.                         .         -----                     -
.                         .         \ 2 \                     \
-------------                       -----                     -
\ 1 \ 2 \ 3 \             \         \ 3 \                     \
------------- ... ---------         ----- ... -----------------


Special Case 3:                    Special Case 4:
----- ....... -------------         ----- ... -----------------
\             \ 3 \ 4 \ 5 \         \                 \ 4 \ 5 \
-             -------------         -                 ---------
\             \ 6 \ 7 \ 8 \         \                     \ 6 \
-             -------------         -                     -----
\                         \         .                     \ 7 \
-                         -         .                     -----
\                         \         .                     \ 8 \
-                         -         -----                 -----
.                         .         \ 1 \                     .
.                         .         -----                     .
.                         .         \ 2 \                     .
-----                 -----         -----                     -
\ 1 \                 \ 2 \         \ 3 \                     \
----- ....... -------------         ----- ... -----------------

	 
INPUT PARAMS:
    bCornerCase - Corner condition
    bInputData  - The codeword to be placed
        
RETURN VALUE:
    None.    
**********************************************************************/
  
void CornerPlace(const SINT8 bCornerCase, const UINT8 bInputData)
{
    UINT16	*pwBitmapStart;   /* Pointer to the beginning of the bitmap      */
    UINT16  *pwDM_Ptr;       /* Bitmap memory pointer            */
    UINT16	*pwLastColumn;    /* Pointer to the beginning of the last column */

    SINT32  lAlignHeight;    /* # of rows of used for alignment  */
    SINT32  lAlignWidth;     /* # of cols of used for alignment  */
    SINT32  lColLength;      /* # of data units to make a column */
    SINT32  lNumCols;       /* # of Cols in symbol w/out align. */
    SINT32  lNumRows;       /* # of Rows in symbol w/out align. */
    SINT32  lTemp;

    UINT32  lRowPos;         /* offset of current row position   */

    UINT16  wSetBit;         /* mask identifying the  current bit*/
 
   
    /* Determine # of Cols and rows used for alignment */
    lAlignHeight = 2 * psSymbol->bVerticalDataRegions;
    lAlignWidth  = 2 * psSymbol->bHorizontalDataRegions;
   
    /* Determine # of Cols and rows used for data */
    lNumCols = psSymbol->bSymbolColumns - lAlignWidth;
    lNumRows = psSymbol->bSymbolRows - lAlignHeight;
    lTemp = lNumRows - 1;
    pwBitmapStart = psSymbol->pwBitmap;
   
    /* # of data units per column */
    lColLength = (lTemp / 16) + 1;
    pwLastColumn = ((lNumCols - 1) * lColLength) + pwBitmapStart + 
                  (lTemp / 16);

    /* reset mask */
    wSetBit = 0x0001;
    switch (-bCornerCase) /* Determine special corner condition */
    {
        case 1: /* Special Corner Condition 1 */
            /* Start at bottom row, last column from the right */
            lRowPos = (lNumRows - 1) & 0x0f; /* Start at bottom row */
            wSetBit <<= lRowPos;

            /* find last column */
            pwDM_Ptr = pwLastColumn;
            if (bInputData & 0x80)  /* set Bit 1 */
                *pwDM_Ptr |= wSetBit;

            pwDM_Ptr -= lColLength;  /* move to the 2nd to last column */
            if (bInputData & 0x40)  /* set Bit 2 */ 
                *pwDM_Ptr |= wSetBit;

            pwDM_Ptr -= lColLength;  /* move to the 3rd to last column */
            if (bInputData & 0x20)  /* set Bit 3 */
                *pwDM_Ptr |= wSetBit;

            /* move to top row, 2nd column from the right */
            wSetBit = 0x0001; 
            pwDM_Ptr = pwBitmapStart + lColLength;
            if (bInputData & 0x10)  /* set Bit 4 */
                *pwDM_Ptr |= wSetBit;

            pwDM_Ptr = psSymbol->pwBitmap;  /* move to the 1st column */
            if (bInputData & 0x08)  /* set Bit 5 */
                *pwDM_Ptr |= wSetBit;

            wSetBit *= 2; /* move to the 2nd row */
            if (bInputData & 0x04)  /* set Bit 6 */
                *pwDM_Ptr |= wSetBit;

            wSetBit *= 2; /* move to the 3rd row */
            if (bInputData & 0x02)  /* set Bit 7 */
                *pwDM_Ptr |= wSetBit;

            wSetBit *= 2; /* move to the 4th row */
            if (bInputData & 0x01)  /* set Bit 8 */
                *pwDM_Ptr |= wSetBit;

            break;

        case 2: /* Special Corner Condition 2 */
            /* Start in 3rd row from the bottom, last column from the right */
            lRowPos = (lNumRows - 3) & 0x0f; 
            wSetBit <<= lRowPos;

            /* Find last column from the right */
            pwDM_Ptr = pwLastColumn;
            if (bInputData & 0x80)  /* set Bit 1 */
                *pwDM_Ptr |= wSetBit;

            wSetBit *= 2; /* move to next row */
            if (!wSetBit) /* Need to check if next bit is in next word */
			{
                wSetBit = 0x0001;
                pwDM_Ptr++;
			}

            if (bInputData & 0x40)  /* set Bit 2 */
                *pwDM_Ptr |= wSetBit;
           
            wSetBit *= 2;  /* move to next row */
                                   /* Don't need to check if in next word, 
                                     always even */
            if (bInputData & 0x20)  /* set Bit 3  */
                *pwDM_Ptr |= wSetBit;

            /* move to top row, 4th column from the right */
            wSetBit = 0x0001;

            /* find 4th column */
            pwDM_Ptr = 3 * lColLength + pwBitmapStart;
            if (bInputData & 0x10)  /* set Bit 4 */
                *pwDM_Ptr |= wSetBit;

            pwDM_Ptr -= lColLength; /* move to 3rd column */
            if (bInputData & 0x08) /* set Bit 5 */
                *pwDM_Ptr |= wSetBit;

            pwDM_Ptr -= lColLength; /* move to 2nd column */
            if (bInputData & 0x04) /* set Bit 6 */
                *pwDM_Ptr |= wSetBit;

            pwDM_Ptr -= lColLength; /* move to 1st column */
            if (bInputData & 0x02) /* set Bit 7 */
                *pwDM_Ptr |= wSetBit;

            wSetBit++;             /* move to 2nd row */
            if (bInputData & 0x01) /* set Bit 8 */
                *pwDM_Ptr |= wSetBit;

            break;

        case 3:
            /* start in the last row, last column from the right */
            /* find last row */
            lRowPos = (lNumRows - 1) & 0x0f;
            wSetBit <<= lRowPos;

            /* find last column */
            pwDM_Ptr = pwLastColumn;
            if (bInputData & 0x80)  /* set Bit 1 */
                *pwDM_Ptr |= wSetBit;

            /* find first column from the right */
            pwDM_Ptr = psSymbol->pwBitmap + ((lNumRows - 1) / 16);
            if (bInputData & 0x40)  /* set Bit 2 */
                *pwDM_Ptr |= wSetBit;

            /* find 1st row, 1st column from the right */
            pwDM_Ptr = psSymbol->pwBitmap;
            wSetBit = 0x0001;
            if (bInputData & 0x08)  /* set Bit 5 */
                *pwDM_Ptr |= wSetBit;

            wSetBit *= 2; /* move to 2nd row */
            if (bInputData & 0x01)  /* set Bit 8 */
                *pwDM_Ptr |= wSetBit;

            pwDM_Ptr += lColLength;  /* move to 2nd column */
            if (bInputData & 0x02)  /* set Bit 7 */
                *pwDM_Ptr |= wSetBit;

            wSetBit /= 2; /* move to 1st row */
            if (bInputData & 0x10)  /* set Bit 4 */
                *pwDM_Ptr |= wSetBit;

            pwDM_Ptr += lColLength;  /* move to 3rd column */
            if (bInputData & 0x20)  /* set Bit 3 */
                *pwDM_Ptr |= wSetBit;

            wSetBit *= 2; /* move to 2nd row */
            if (bInputData & 0x04)  /* set Bit 6 */
                *pwDM_Ptr |= wSetBit;

            break;

        case 4:
            /* Start in 3rd row from the bottom, last column from the right */ 
            lRowPos = (lNumRows - 3) & 0x0f;
            wSetBit <<= lRowPos;

            /* find last column */
            pwDM_Ptr = pwLastColumn;
            if (bInputData & 0x80)  /* set Bit 1 */
                *pwDM_Ptr |= wSetBit;

            wSetBit *= 2; /* move to the 2nd row from the bottom */
            if (!wSetBit)  /* Need to check if next bit is in next word */
 			{
				wSetBit = 0x0001;
                pwDM_Ptr++;
			}

            if (bInputData & 0x40)  /* set Bit 2 */
                *pwDM_Ptr |= wSetBit;
           
            wSetBit *= 2;  /* Move to the last row */
                                   /* Don't need to check if in next word, 
                                      always even */
            if (bInputData & 0x20)  /* set Bit 3 */
                *pwDM_Ptr |= wSetBit;

            /* move to the top row, 2nd column from the right */
            wSetBit = 0x0001;       

            /* find 2nd column from the right */
            pwDM_Ptr = pwBitmapStart + lColLength;
            if (bInputData & 0x10) /* set Bit 4 */
                *pwDM_Ptr |= wSetBit;

            /* move to first column */
            pwDM_Ptr = pwBitmapStart;
            if (bInputData & 0x08)  /* set Bit 5 */
                *pwDM_Ptr |= wSetBit;

            wSetBit *= 2; /* move to 2nd row */
            if (bInputData & 0x04)  /* set Bit 6 */
                *pwDM_Ptr |= wSetBit;

            wSetBit *= 2; /* move to 3rd row */
            if (bInputData & 0x02)  /* set Bit 7 */
                *pwDM_Ptr |= wSetBit;

            wSetBit *= 2; /* move to 4th row */
            if (bInputData & 0x01)  /* set Bit 8 */
                *pwDM_Ptr |= wSetBit;

            break;

		default:
			break;
    }

    return;
}


/**********************************************************************
DESCRIPTION:
    The DoAlign function generates an alignment column for the current symbol.
    The pattern is placed at the location pointed to by pwBitmap.
	 
INPUT PARAMS:
    bNumRow        - Number of Data Matrix Rows.
    bExtraModules  - Add extra modules above or below this symbol.
    pwBitmap       - Pointer to the bitmap, which has already been cleared
    
RETURN VALUE:
    None.    
**********************************************************************/

void DoAlign(const UINT8 bNumRow, const SINT8 bExtraModules, UINT16 *pwBitmap)
{
	UINT16	wMask = 0x5555;
	UINT16	wData;
    UINT8   bInx = bNumRow;        /* loop index        */
	UINT8	bWhiteCnt = 0;


    /**************************************************/
    /* Generate the alignment column                  */
    /**************************************************/

	if (bInx >= (16 - bWhiteCnt))
	{
		// we have more than just the current word to fill
		*pwBitmap++ = wMask;
		bInx -= 16 - bWhiteCnt;

		wData = 0x5555;		// else, start each full word with a 0

		while (bInx >= 16)
		{
			*pwBitmap++ = wData;
			bInx -= 16;
		}

		if (bInx)
		{
			*pwBitmap = wData & (0xFFFF << (16 - bInx));
		}
	}
	else
	{
		*pwBitmap = wMask & (0xFFFF << (16 - bWhiteCnt - bInx));
	}
}


/**********************************************************************
DESCRIPTION:
    The DoBlack function generates an black column for the current symbol.
    The pattern is placed at the location pointed to by pwBitmap.
	 
INPUT PARAMS:
    bNumRow        - Number of Data Matrix Rows.
    bExtraModules  - Add extra modules above or below this symbol.
    pwBitmap       - Pointer to the bitmap, which has already been cleared
    
RETURN VALUE:
    None.    
**********************************************************************/

void DoBlack(const UINT8 bNumRow, const SINT8 bExtraModules, UINT16 *pwBitmap)
{
	UINT16	wMask = 0xFFFF;
    UINT8   bBlackCnt = bNumRow;		/* Black Fill count */
	UINT8	bWhiteCnt = 0;


    /**************************************************/
    /* Generate "black space" for a black column      */
    /**************************************************/

    /* Add the black space */
	if (bBlackCnt >= (16 - bWhiteCnt))
	{
		// we have more than just the current word to fill
		*pwBitmap++ = wMask;
		bBlackCnt -= 16 - bWhiteCnt;

		while (bBlackCnt >= 16)
		{
			*pwBitmap++ = 0xFFFF;
			bBlackCnt -= 16;
		}

		if (bBlackCnt)
		{
			*pwBitmap = 0xFFFF << (16 - bBlackCnt);
		}
	}
	else
	{
		*pwBitmap = wMask & (0xFFFF << (16 - bWhiteCnt - bBlackCnt));
	}
}


/**********************************************************************
DESCRIPTION:
    The DoColumn function generates a column of data for the current 
    symbol. The pattern is placed at the location pointed to by pwBitmap.

	NOTE: The data for the bitmap was generated so the MS bit is bit 0 of
			the 16-bit word.  For the actual bitmap, the MS bit needs to
			be bit 15.  So the data is read from low to high, but the
			bitmap is generated from high to low.
	 
INPUT PARAMS:
    bColCnt        - The current DM symbol column (starting at 0).
    bNumRow        - Number of Data Matrix Rows.
    bRowsInArea    - Rows between alignment patterns.
    bExtraModules  - Add extra modules above or below this symbol.
    pwBitmapData   - Pointer to the data for generating the bitmap
    pwBitmap       - Pointer to the bitmap, which has already been cleared
    
RETURN VALUE:
    None.    
**********************************************************************/

void DoColumn(const UINT8 bColCnt, const UINT8 bNumRow, const UINT8 bRowsInArea,
			  const SINT8 bExtraModules, const UINT16 *pwBitmapData, UINT16 *pwBitmap)
{
    UINT16  wBitmapData;     /* Bitmap data                                  */
    UINT16  wBitMask;        /* bit position in bitmap                       */
	UINT16	wTempData, wTempData2;
	UINT16	wPreviousBits = 0, wPreviousBits2 = 0;
	UINT16	wAlignBit = 0;
    UINT8   bDataIncluded;   /* Data included in data area                   */
	UINT8	bUpperData, bLowerData;
   

    /**************************************************/
    /* Generate one column of a Data Matrix Bitmap    */
    /**************************************************/

	// Get the first data word and reverse the bits.
	wTempData = *pwBitmapData++;
	bUpperData = wTempData >> 8;
	bLowerData = wTempData & 0xFF;
	wBitmapData = (UINT16)((ByteMirror[bLowerData] << 8) + ByteMirror[bUpperData]);

	if (wBitmapData & 0x01)
	{
		wPreviousBits = 0x8000;
	}

	// make room for the start alignment bit
	wBitmapData >>= 1;

	// insert the start alignment bit
	if (bColCnt & 0x01)
	{
		wAlignBit =	0x8000;		// for later use
		wBitmapData |= 0x8000;
	}

	// If a barcode has <= 26 rows, it only has 1 vertical region,
	// so the total number of rows = number of data rows + 2 alignment rows and
	// the data rows are contained within 2 words
	if (bNumRow == bRowsInArea + 2)
	{
		// if there are less than 15 actual data rows, the two alignment bits
		// plus the data bits will all fit in one word.
		if (bRowsInArea < 15)
		{
			// figure out where the end alignment bit should go in the word,
			// put it in, load the word in the bitmap & we're done
			*pwBitmap = wBitmapData | (0x0001 << (14 - bRowsInArea));
		}
		// else, a barcode never has an odd number of rows, so we don't have
		// to check for the number of data rows being 15.  We can safely
		// assume at this point that the number of data rows is at least 16,
		// but no more than 26.
		// If the number of data rows is 16, we already have everything we
		// need because the 16 data bits were in the first data word.
		else if (bRowsInArea == 16)
		{
			// store the first word of column data.
			*pwBitmap++ = wBitmapData;

			// combine the 16th data bit with the end alignment bit & store.
			*pwBitmap = wPreviousBits | 0x4000;
		}
		// else, the rest of the data bits plus the end alignment bit will
		// fit in a second word.
		// combine the 16th data bit with the second data word and the
		// end aligment bit, then store.
		else
		{
			// store the first word of column data.
			*pwBitmap++ = wBitmapData;

			// Get the second data word and reverse the bits.
			wTempData = *pwBitmapData;
			bUpperData = wTempData >> 8;
			bLowerData = wTempData & 0xFF;
			wBitmapData = (UINT16)((ByteMirror[bLowerData] << 8) + ByteMirror[bUpperData]);

			// shift the data to allow the 16th bit to be put in
			wBitmapData = (wBitmapData >> 1) | wPreviousBits;

			// figure out where the end alignment bit should go in the word,
			// put it in, load the word in the bitmap & we're done
			*pwBitmap = wBitmapData | (0x8000 >> (bRowsInArea - 15));
		}
	}
	// else, the barcode has two vertical regions of 32 to 48 rows.
	// this means that there are ((x / 2) - 2), or 14 - 22, data rows per region.
	else
	{
		// let's handle the case of 14 data rows separately because it works out
		// to even symetrical bytes.
		// (i.e., start bit, 14 data bits, end bit in each byte)
		if (bRowsInArea == 14)
		{
			// combine the first word of data w/ the end alignment bit & store.
			*pwBitmap++ = wBitmapData | 0x0001;

			// Get the second data word and reverse the bits.
			wTempData = *pwBitmapData;
			bUpperData = wTempData >> 8;
			bLowerData = wTempData & 0xFF;
			wBitmapData = (UINT16)((ByteMirror[bLowerData] << 8) + ByteMirror[bUpperData]);

			// shift the data to allow the start alignment bit & the 16th bit to be put in.
			// also put in the end alignment bit & store
			*pwBitmap = wAlignBit | (wPreviousBits >> 1) | (wBitmapData >> 2) | 0x0001;
		}
		// else, we know the first word of data is OK as is.
		// we just need to figure out how to adjust the data for the rest of the words. 
		else
		{
			// store the first word of barcode column data.
			*pwBitmap++ = wBitmapData;

			// Get the second data word and reverse the bits.
			wTempData = *pwBitmapData++;
			bUpperData = wTempData >> 8;
			bLowerData = wTempData & 0xFF;
			wBitmapData = (UINT16)((ByteMirror[bLowerData] << 8) + ByteMirror[bUpperData]);

			// Now we know that the end bit & the second start bit are somewhere in the
			// second word of column data. Based on the barcode types allowed, we know that
			// there are none with 15 rows of data in a vertical region.  So, we won't worry
			// about that case, which means the 16th bit from the previous data word can
			// be used as the MS bit of the second column data word.  Since the 2 alignment
			// bits must go in the second column data word, we must save the 3 MS bits of
			// the second column data word for use in the third column data word.
			wPreviousBits2 = (wBitmapData & 0x07) << 13;

			// shift the data to allow the 16th bit to be put in
			wBitmapData = (wBitmapData >> 1) | wPreviousBits;

			// now split up the word to get where everything goes
			bDataIncluded = bRowsInArea - 15;
			wBitMask = 0xFFFF << (16 - bDataIncluded);
			wTempData = wBitmapData & wBitMask;				// get the bits before the alignment bits
			wTempData2 = (wBitmapData & ~wBitMask) >> 2;	// get the bits after the alignment bits

			// store the second word of barcode column data.
			*pwBitmap++ = wTempData | (0x8000 >> bDataIncluded) | (wAlignBit >> (bDataIncluded + 1)) | wTempData2;

			// number of bits from the second vertical region included in the second column data word =
			// 16 - 2 alignment bits - number of bits from the first vertical region included
			// in the second column data word
			bDataIncluded = 16 - 2 - bDataIncluded;

			// Get the third data word and reverse the bits.
			wTempData = *pwBitmapData;
			bUpperData = wTempData >> 8;
			bLowerData = wTempData & 0xFF;
			wBitmapData = (UINT16)((ByteMirror[bLowerData] << 8) + ByteMirror[bUpperData]);

			// shift the data to allow the previous bits to be put in.
			// we know that the biggest barcode has 48 total rows, so the bits that are being
			// shifted out were padding.
			wBitmapData = (wBitmapData >> 3) | wPreviousBits2;

			// figure out where the end alignment bit should go in the word,
			// put it in, load the word in the bitmap & we're done
			// the shift for the end bit is done to cause fewer shifts the larger the barcode
			*pwBitmap = wBitmapData | (0x0001 << (15 - (bRowsInArea - bDataIncluded)));
		}
    }
}


/**********************************************************************
DESCRIPTION:
    The DoRotatedColumn function generates a column of data for the current 
    rotated symbol. The pattern is placed at the location pointed to by pwBitmap.

	NOTE: The data for the bitmap was generated so the MS bit is bit 0 of
			the 16-bit word.  For the actual bitmap, the MS bit needs to
			be bit 15.  So the data is read from low to high, but the
			bitmap is generated from high to low.
	 
INPUT PARAMS:
    bColCnt        - The current DM symbol column (starting at 0) in the rotated barcode.
    bNumRow        - Number of Data Matrix Rows in the rotated barcode.
    bRowsInArea    - Rows between alignment patterns in the rotated barcode.
    bExtraModules  - Add extra modules above or below this symbol.
    pwBitmapData   - Pointer to the data for generating the bitmap
    pwBitmap       - Pointer to the bitmap, which has already been cleared
    bWordsInCol    - Number of words of data for each column in the non-rotated barcode.
    bSourceRowNum  - Number of the row from the non-rotated barcode that has the column
	                 data for the rotated barcode
    
RETURN VALUE:
    None.    
**********************************************************************/

void DoRotatedColumn(const UINT8 bColCnt, const UINT8 bNumRow, const UINT8 bRowsInArea,
			  const SINT8 bExtraModules, const UINT16 *pwBitmapData, UINT16 *pwBitmap,
			  const UINT8 bWordsInCol, const UINT8 bSourceRowNum)
{
    UINT16  wBitmapData = 0;
    UINT16  wBitMask = 0x8000;
    UINT16  wSourceMask = 0x8000;
	UINT16	wTempData;
    UINT8   bInx;
    UINT8   bDataIncluded = 0;
    UINT8   bStatus = 1;
	UINT8	bUpperData, bLowerData;
	UINT8	bAlignBit = 0;
   

    /**************************************************/
    /* Generate one column of a rotated Data Matrix Bitmap    */
    /**************************************************/

	// First set up a few things.

	// determine the start alignment bit
	if (!(bColCnt & 0x01))
	{
		bAlignBit = 1;
	}

	// determine which byte and which bit to use
	pwBitmapData += bSourceRowNum / 16;
	wSourceMask >>= (bSourceRowNum % 16);

	// get all the bits for the column
	for (bInx = 0; bInx < bNumRow; bInx++)
	{
		switch (bStatus)
		{
		case 1:
			// need start alignment pattern bit
			wTempData = bAlignBit;
			bStatus--;
			break;

		case 2:
			// need end alignment pattern bit
			wTempData = 1;
			bStatus--;
			break;

		default:
			// need data bit
			// get the bit value
			wTempData = *pwBitmapData;
			bUpperData = wTempData >> 8;
			bLowerData = wTempData & 0xFF;
			wTempData = (UINT16)((ByteMirror[bLowerData] << 8) + ByteMirror[bUpperData]);
			wTempData &= wSourceMask;

			// indicate that we've done a bit
			bDataIncluded ++;

			// if done all the data bits between two alignment patterns,
			// setup for the next bit to be a start alignment pattern bit.
			if (bDataIncluded == bRowsInArea)
			{
				bDataIncluded = 0;
				bStatus = 2;
			}

		    /* advance the pointer to the next group of data */
		    pwBitmapData += bWordsInCol;
			break;
		}

		// insert the bit
		if (wTempData)
		{
			wBitmapData |= wBitMask;
		}

		// set up the mask for the next bit
		wBitMask >>= 1;

		// if no more bits left, store the data and
		// setup to do the next word of data
		if (!wBitMask)
		{
			// store the word of column data.
			*pwBitmap++ = wBitmapData;

			// setup for the next word of column data.
			wBitMask = 0x8000;
			wBitmapData = 0;
		}
	}

	// check if we have to store a partial word of column data
	if (wBitMask != 0x8000)
		*pwBitmap++ = wBitmapData;
}


/****************************** DMGen *********************************
 
 DESCRIPTION:
    This function converts Data Matrix Bitmaps to its full size while 
	inserting the alignment patterns and quiet zones. 
    Conversion steps:
    (1) Read one column of Data Matrix bitmap data.
    (2) Insert "white fill" at the top of the column.
    (3) Insert "alignment patterns" and "quiet zones" to data and apply 
        thinning between rows.
    (4) Insert "white fill" at the bottom of the column.
    (5) Apply thinning between columns.
    (7) Return to step 1 for the next column, until all columns are converted.
**********************************************************************/

void DMGen(const UINT8 *pbPrintBuf, const UINT8 bRowBytes)
{
    UINT16  *pwDM_Ptr;         /* bitmap memory pointers                  */
    UINT32  lwCurData[2];       /* Holds the module information for        */
                               /* the current col. of barcode data        */ 
    UINT32  lwPrevData[2] = {0xff, 0xff};
							   /* Holds the module information for        */
                               /* the previous col. of barcode data       */
                               /* for use when doing thinning             */
	register UINT32	*plwCurData = lwCurData;
	register UINT32	*plwPrevData = lwPrevData;

    register UINT8   bAlignment;        /* align pattern indicator                 */
    register UINT8   bColCnt;           /* loop index                              */
	register UINT8   bMaxColCnt;
    UINT8   bColsInDataArea;   /* # of columns between alignment patterns */
    UINT8   bDataIncluded;     /* # cols done in a data area              */
    UINT8   bRowsInDataArea;   /* # of rows between alignment patterns    */
    UINT8   bWordsInCol;       /* # of words in a column                  */
	UINT8	bHThinFactor;
	UINT8	bVThinFactor;
	UINT8	bNonThinExp;
	BOOL	fDoThinning;		// indicates if thinning should be done for a particular column

#ifdef	BARCODE_DEBUG
	UINT8	bDebugInx = 0;
#endif

//#ifdef IG_TIME_DEBUG
//	char			pLogBuf[50];
//#endif


	if (psBarCode->bBarcodeType & GET_ROTATE_FLAG)
	{
		// doing a rotated barcode
		DMGenRotated(pbPrintBuf, bRowBytes);
		return;
	}

    /* Initialization */
	bHThinFactor = psBarCode->bHThin;
	bVThinFactor = psBarCode->bVThin;
	pbJanusBitmapPtr = (UINT8 *)pbPrintBuf;
	bNonThinExp = psBarCode->bHorizontalExp-bHThinFactor;

    /* pre-calculate symbol information */
    pwDM_Ptr  = psSymbol->pwBitmap;
    bRowsInDataArea = (UINT8)(psSymbol->bSymbolRows - (UINT8)(2 * psSymbol->bVerticalDataRegions)) >> 
                     (psSymbol->bVerticalDataRegions / 2);

    bColsInDataArea = (UINT8)(psSymbol->bSymbolColumns - (UINT8)(2 * psSymbol->bHorizontalDataRegions)) >> 
                     (psSymbol->bHorizontalDataRegions / 2);

    bWordsInCol = ((psSymbol->bSymbolRows - (UINT8)(2 * psSymbol->bHorizontalDataRegions)) + 15) / 16;
	bMaxColCnt = psSymbol->bSymbolColumns;
    bAlignment = 1;
    bDataIncluded = 0;

    /* Generate the symbol */
    /* generate the current column */
    for (bColCnt = 0; bColCnt < bMaxColCnt; bColCnt++)
    {
        /* generate the module patterns for each symbol in the barcode col*/
		switch (bAlignment)
		{
        case 1:
			/* start aligment pattern is to be generated */
			plwCurData[0] = lwAlignData[0];
			plwCurData[1] = lwAlignData[1];
            bAlignment--;
			fDoThinning = FALSE;
			break;

        case 2:
			/* end aligment pattern is to be generated */
			plwCurData[0] = lwBlackData[0];
			plwCurData[1] = lwBlackData[1];
            bAlignment--;
			fDoThinning = FALSE;
			break;

        default:
			// clear the data buffer
	        plwCurData[0] = 0;
	        plwCurData[1] = 0;

			/* gather data for bitmap generation */
			/* need to be sure colCnt is ok for all symbols 
               since its used for the alignment */
            DoColumn(bColCnt, psSymbol->bSymbolRows, bRowsInDataArea, 
                      psSymbol->bExtraSpace, pwDM_Ptr, (unsigned short *)plwCurData);	//lint !e740

#ifdef	BARCODE_DEBUG
			(void)memcpy((unsigned char *)sDebugTable[bDebugInx].lwEncodedData,
						(unsigned char *)pwDM_Ptr, bWordsInCol*2);
			(void)memcpy((unsigned char *)sDebugTable[bDebugInx++].lwCurData,
						(unsigned char *)plwCurData, bWordsInCol*2);
#endif

            /* advance the bitmap pointer to the next column */
            pwDM_Ptr += bWordsInCol;

            /* Increment the columns included count */
            bDataIncluded++;
            
            /* Is it time to generate an end alignment pattern? */
            if (bDataIncluded == bColsInDataArea)
            {
                /* Setup so that next column is an end alignment pattern */
                bDataIncluded = 0;
                bAlignment = 2;
            }

			fDoThinning = TRUE;
			break;
        }

		// Check if thinning should be done at all
		if (bHThinFactor)
		{
			// if thinning should be done for this particular column,
			// adjust the previous data to get the thinning data that
			// should be put in the thinning area between columns.
			if (fDoThinning)
			{
				// AND the adjusted previous column data with the current column
				// data to get the 'thinned' data for the previous column
				plwPrevData[0] &= plwCurData[0];
				plwPrevData[1] &= plwCurData[1];

				// Load the thinned data
				BitmapGen(bHThinFactor, psBarCode->bVerticalExp,
						psBarCode->wBarcodeHeight, (unsigned short *)plwPrevData, bVThinFactor,	//lint !e740
						psBarCode->wExtraPixelsAbove, psBarCode->wExtraPixelsBelow,
						bRowBytes, fDoThinning, psBarCode->bFileID1);
			}
			// else, load the thinning area with the current data
			else
			{
				// Load the normal data
				BitmapGen(bHThinFactor, psBarCode->bVerticalExp,
						psBarCode->wBarcodeHeight, (unsigned short *)plwCurData, bVThinFactor,	//lint !e740
						psBarCode->wExtraPixelsAbove, psBarCode->wExtraPixelsBelow,
						bRowBytes, FALSE, psBarCode->bFileID1);
			}
		}

//#ifdef IG_TIME_DEBUG
//	sprintf(pLogBuf, "Done w/ Align, Black, & Column, Start Expand");
//	fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
//#endif

		/* Load the current column data */
		BitmapGen(bNonThinExp, psBarCode->bVerticalExp,
				psBarCode->wBarcodeHeight, (unsigned short *)plwCurData, bVThinFactor,	//lint !e740
				psBarCode->wExtraPixelsAbove, psBarCode->wExtraPixelsBelow,
				bRowBytes, fDoThinning, psBarCode->bFileID1);

//#ifdef IG_TIME_DEBUG
//	sprintf(pLogBuf, "End Expand");
//	fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
//#endif

        // If thinning is being done,
		// Make the current column the previous column used for thinning with some
		// adjustments to make sure the start & end alignment patterns aren't affected
		// Check if we need to put white space between adjacent horizontal black modules
		// If so, clear all the bits except the ones for the alignment modules.
		// Else, make sure the bits for the alignment modules are on in preparation for
		// the ANDing that is done above.
		if (bHThinFactor)
		{
			if (psBarCode->bFileID2)
			{
				plwPrevData[0] = lwThinningMask[0];
				plwPrevData[1] = lwThinningMask[1];
			}
			else
			{
				plwPrevData[0] = (plwCurData[0] | lwThinningMask[0]);
				plwPrevData[1] = (plwCurData[1] | lwThinningMask[1]);
			}
		}
    }

    return;
}


/************************** DMGenRotated ******************************
 
 DESCRIPTION:
    This function converts Data Matrix Bitmaps to its full size while 
	inserting the alignment patterns and quiet zones for a rotated barcode. 
    Conversion steps:
    (1) Read one column of Data Matrix bitmap data.
    (2) Insert "white fill" at the top of the column.
    (3) Insert "alignment patterns" and "quiet zones" to data and apply 
        thinning between rows.
    (4) Insert "white fill" at the bottom of the column.
    (5) Apply thinning between columns.
    (7) Return to step 1 for the next column, until all columns are converted.
**********************************************************************/

void DMGenRotated(const UINT8 *pbPrintBuf, const UINT8 bRowBytes)
{
#ifdef	BARCODE_DEBUG
    UINT16  *pwDM_Ptr;         /* bitmap memory pointers                  */
#endif

    UINT32  lwCurData[2];       /* Holds the module information for        */
                               /* the current col. of barcode data        */ 
    UINT32  lwPrevData[2] = {0xff, 0xff};
							   /* Holds the module information for        */
                               /* the previous col. of barcode data       */
                               /* for use when doing thinning             */
	register UINT32	*plwCurData = lwCurData;
	register UINT32	*plwPrevData = lwPrevData;

    register UINT8   bAlignment;        /* align pattern indicator                 */
    register UINT8   bColCnt;           /* loop index                              */
	register UINT8   bMaxColCnt;
    UINT8   bColsInDataArea;   /* # of columns between alignment patterns */
    UINT8   bDataIncluded;     /* # cols done in a data area              */
    UINT8   bRowsInDataArea;   /* # of rows between alignment patterns    */
    UINT8   bWordsInCol;       /* # of words in a column                  */
	UINT8	bSourceRowNum = 0;
	UINT8	bHThinFactor;
	UINT8	bVThinFactor;
	UINT8	bNonThinExp;
	BOOL	fDoThinning;		// indicates if thinning should be done for a particular column

#ifdef	BARCODE_DEBUG
	UINT8	bDebugInx = 0;
#endif

//#ifdef IG_TIME_DEBUG
//	char			pLogBuf[50];
//#endif


    /* Initialization */
	bHThinFactor = psBarCode->bHThin;
	bVThinFactor = psBarCode->bVThin;
	pbJanusBitmapPtr = (UINT8 *)pbPrintBuf;
	bNonThinExp = psBarCode->bHorizontalExp-bHThinFactor;

    /* pre-calculate symbol information */
#ifdef	BARCODE_DEBUG
    pwDM_Ptr  = psSymbol->pwBitmap;
#endif

    bRowsInDataArea = (UINT8)(psSymbol->bSymbolColumns - (UINT8)(2 * psSymbol->bHorizontalDataRegions)) >> 
                     (psSymbol->bHorizontalDataRegions / 2);

    bColsInDataArea = (UINT8)(psSymbol->bSymbolRows - (UINT8)(2 * psSymbol->bVerticalDataRegions)) >> 
                     (psSymbol->bVerticalDataRegions / 2);

    bWordsInCol = ((psSymbol->bSymbolRows - (UINT8)(2 * psSymbol->bHorizontalDataRegions)) + 15) / 16;
	bMaxColCnt = psSymbol->bSymbolRows;
    bAlignment = 2;
	bSourceRowNum = (UINT8)((psSymbol->bSymbolRows - (UINT8)(2 * psSymbol->bVerticalDataRegions)) - 1);
    bDataIncluded = 0;

    /* Generate the symbol */
    /* generate the current column */
    for (bColCnt = 0; bColCnt < bMaxColCnt; bColCnt++)
    {
        /* generate the module patterns for each symbol in the barcode col*/
		switch (bAlignment)
		{
        case 1:
			/* start aligment pattern is to be generated */
			plwCurData[0] = lwAlignData[0];
			plwCurData[1] = lwAlignData[1];
            bAlignment++;
			fDoThinning = FALSE;
			break;

        case 2:
			/* end aligment pattern is to be generated */
			plwCurData[0] = lwBlackData[0];
			plwCurData[1] = lwBlackData[1];
            bAlignment = 0;
			fDoThinning = FALSE;
			break;

        default:
			// clear the data buffer
	        plwCurData[0] = 0;
	        plwCurData[1] = 0;

			/* gather data for bitmap generation */
			/* need to be sure colCnt is ok for all symbols 
               since its used for the alignment */
            DoRotatedColumn(bColCnt, psSymbol->bSymbolColumns, bRowsInDataArea, 
                      psSymbol->bExtraSpace, psSymbol->pwBitmap, (unsigned short *)plwCurData,	//lint !e740
					  bWordsInCol, bSourceRowNum);
			bSourceRowNum--;

#ifdef	BARCODE_DEBUG
			(void)memcpy((unsigned char *)sDebugTable[bDebugInx].lwEncodedData,
						(unsigned char *)pwDM_Ptr, bWordsInCol*2);
			(void)memcpy((unsigned char *)sDebugTable[bDebugInx++].lwCurData,
						(unsigned char *)plwCurData, bWordsInCol*2);

            /* advance the bitmap pointer to the next column */
            pwDM_Ptr += bWordsInCol;
#endif

            /* Increment the columns included count */
            bDataIncluded++;
            
            /* Is it time to generate an end alignment pattern or a start alignment pattern? */
            if (bDataIncluded == bColsInDataArea)
            {
                /* Setup so that next column is a start alignment pattern */
                bDataIncluded = 0;

				// next column is a start alignment pattern column, so bAlignment must be 1
                bAlignment++;
           }

			fDoThinning = TRUE;
			break;
        }

		// Check if thinning should be done at all
		if (bHThinFactor)
		{
			// if thinning should be done for this particular column,
			// adjust the previous data to get the thinning data that
			// should be put in the thinning area between columns.
			if (fDoThinning)
			{
				// AND the adjusted previous column data with the current column
				// data to get the 'thinned' data for the previous column
				plwPrevData[0] &= plwCurData[0];
				plwPrevData[1] &= plwCurData[1];

				// Load the thinned data
				BitmapGen(bHThinFactor, psBarCode->bVerticalExp,
						psBarCode->wBarcodeWidth, (unsigned short *)plwPrevData, bVThinFactor,	//lint !e740
						psBarCode->wExtraPixelsAbove, psBarCode->wExtraPixelsBelow,
						bRowBytes, fDoThinning, psBarCode->bFileID1);
			}
			// else, load the thinning area with the current data
			else
			{
				// Load the normal data
				BitmapGen(bHThinFactor, psBarCode->bVerticalExp,
						psBarCode->wBarcodeWidth, (unsigned short *)plwCurData, bVThinFactor,	//lint !e740
						psBarCode->wExtraPixelsAbove, psBarCode->wExtraPixelsBelow,
						bRowBytes, FALSE, psBarCode->bFileID1);
			}
		}

//#ifdef IG_TIME_DEBUG
//	sprintf(pLogBuf, "Done w/ Align, Black, & Column, Start Expand");
//	fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
//#endif

		/* Load the current column data */
		BitmapGen(bNonThinExp, psBarCode->bVerticalExp,
				psBarCode->wBarcodeWidth, (unsigned short *)plwCurData, bVThinFactor,	//lint !e740
				psBarCode->wExtraPixelsAbove, psBarCode->wExtraPixelsBelow,
				bRowBytes, fDoThinning, psBarCode->bFileID1);

//#ifdef IG_TIME_DEBUG
//	sprintf(pLogBuf, "End Expand");
//	fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
//#endif

        // If thinning is being done,
		// Make the current column the previous column used for thinning with some
		// adjustments to make sure the start & end alignment patterns aren't affected
		// Check if we need to put white space between adjacent horizontal black modules
		// If so, clear all the bits except the ones for the alignment modules.
		// Else, make sure the bits for the alignment modules are on in preparation for
		// the ANDing that is done above.
		if (bHThinFactor)
		{
			if (psBarCode->bFileID2)
			{
				plwPrevData[0] = lwThinningMask[0];
				plwPrevData[1] = lwThinningMask[1];
			}
			else
			{
				plwPrevData[0] = (plwCurData[0] | lwThinningMask[0]);
				plwPrevData[1] = (plwCurData[1] | lwThinningMask[1]);
			}
		}
    }

    return;
}


/***********************************************************************
*   FUNCTION NAME: ECCEngine
*  DESCRIPTION:  Universal Error Correction Code Generator for
*                Data Matrix and PDF417 barcodes.
*                Major functions:
*                (1) Reads input data from Codeword_Memory word by word.
*                (2) Read coefficients of a specified "Generator Polynomial"
*                    G(x) from Generator_Memory.
*                (3) Generate Error Correction codewords.
*                     - The generator engine is universal.
*                       The Error correction codewords are the remainder
*                       after dividing the data codewords by a specified
*                       Polynominal G(x) used for Reed-Solomon codes.
*                     - But mathematic functions:
*                       addition, complement and multiplication
*                       are re-defined for each barcode.
*                (4) Store ECC codewords back to ECC_Memory word by word.
* 
*   PURPOSE:      (1) Two dimensional barcode symbols employ Reed-Solomon
*                     error correction.
*                 (2) The Error correction codewords are the remainder
*                     after dividing the data codewords by a specified
*                     Polynominal G(x) used for Reed-Solomon codes.
*                 (3) The arithematic functions used in the calculation
*                     are dependenent on the barcode type.
*                     Data Matrix:
*                     Data Matrix uses bit-wise modulo 2 arithmetic and
*                     byte-wise modulo 100101101, which represents the
*                     Galoris field's prime modules polynomial
*                           8   5    3    2
*                          x + x  + x  + x  + 1
*                     PDF417:
*                     All of the arithemetic in PDF417 is done modulo 929.
* 
*  AUTHOR:        John Hurd
* 
*  INPUT:         all input codewords, coefficients of the Generator Polynomial
*                 and parameters should be loaded into memory (ref: memory map)
*                 before the program is called.
* 
*                 The following parameters will be read in when this
*                 function is called:
*                 gN -- Total data codeword count in a symbol..
*                       Example: for a 36x36 DM, gN=86
*                 gK -- Total ECC codeword counts in a Data Matrix
*                       symbol.
*                       Example: For a 36X36 matrix, gK = 42    ?? gD = 86 - gD = 86 ??
*                 gMode -- barcode mode control,
*                          0=MODE_Data_Matrix, 1=MODE_PDF417
*                 pwGenPtr -- A pointer points to the first location
*                       of the Generator_Memory.
*                 pwCW_Ptr -- A pointer points to the first location
*                       of the Codeword_memory
*                 pwECCPtr -- A pointer points to the first location
*                       of the ECC_memory
*
*  OUTPUT:        ECC codewords will be placed diretly into the ECC memory
*                 word by word.
* 
************************************************************************/
void ECCEngine(void)
{
	UINT16	*pwTempEccPtr;
	UINT16	*pwTempGenPtr;
    UINT16	*pwCW_Ptr;        /* Codeword Memory pointer */

    register UINT16	wNumDataCw;
    register UINT16	wNumEccCw;
	UINT8	bInx;

	/*--------------------------------------------------------------*/
	/* Initialization                                               */
	/*--------------------------------------------------------------*/

    /* Read in and initialize parameters   */
    pwCW_Ptr   = psBarCode->sBarcodeSymbols.pwCodeWords;
    pwTempEccPtr  = psBarCode->sBarcodeSymbols.pwECCWords;
    pwTempGenPtr  = psBarCode->sBarcodeSymbols.pwECCPolyWords;
    wNumDataCw = psBarCode->sBarcodeSymbols.wDataCodeWords - 1;
	wNumEccCw  = psBarCode->sBarcodeSymbols.wEccWords - 1;
    
	for (bInx = 0; bInx < psBarCode->bNumberSymbols; bInx++)
	{
		register UINT16	wLoopData;

    
        /*--------------------------------------------------------------*/
        /* Main Function                                                */
        /*--------------------------------------------------------------*/

        /* ECC Calculation                           */
        for (wLoopData = 0; wLoopData <= wNumDataCw; wLoopData++)
        {
		    register UINT16	*pwECC_Ptr;       /* ECC Memory pointer      */
		    register UINT16	*pwGen_Ptr;       /* Generator Memory pointer*/

		    register UINT16	wLoopEcc;
		    register UINT16	wTempSum;


            /* Initialize Pointers                   */
            pwECC_Ptr = pwTempEccPtr;
            pwGen_Ptr = pwTempGenPtr;

            /* wTempSum = CW(wLoopData) + ECC(K-1)   */
            wTempSum = *pwCW_Ptr ^ *pwECC_Ptr;

            for (wLoopEcc = 0; wLoopEcc <= wNumEccCw; wLoopEcc++)
            {
			    register UINT16	wTemp1;
			    UINT16	wTemp2;


                /* Calculate wTemp1 = ECC(wLoopEcc-1) - (M * Gen(wLoopEcc) */
                wTemp1 = fnPolyMult_Fast((UINT8)wTempSum, (UINT8)*pwGen_Ptr);

                if (wLoopEcc == wNumEccCw)
                   /* The last ECC data is at location=NumEccCw   */
                   /* No valid data beyond that point             */
                    wTemp2 = 0;
                else
                    wTemp2 = *(pwECC_Ptr+1);

                wTemp1 = wTemp1 ^ wTemp2;

                /* If the last wLoopData, then take the complement */
                /* otherwise don't                   */
				*pwECC_Ptr = wTemp1;                                  

				pwECC_Ptr++;
                pwGen_Ptr++;
            }

            pwCW_Ptr++;
        }
	}
}


/**********************************************************************
DESCRIPTION:
The GenerateBitLookUpTable function generates a bitmap look up table used to 
place the code words in a Data Matrix symbol.
	 
INPUT PARAMS:
    None.
    
RETURN VALUE:
    None.
**********************************************************************/

void GenerateBitLookUpTable(void)
{   
    INT8    *pbArray;    /* Pointer to the bitmap look up Table      */
    UINT16  wInx;          /* counter                                  */
    UINT16  wLimit;      /* Number of entries in the table           */
    INT8    bNRow, bNCol; /* Number of rows/colums in the symbol      */
    INT8    bRow, bCol;   /* Current row/column in the symbol         */
    BOOL    fbSpecialCorner; /* True if special placement is required */


    /* Determine the number of rows and columns of data in the symbol */
    bNRow = (INT8)(psSymbol->bSymbolRows - (UINT8)(2 * psSymbol->bVerticalDataRegions));
    bNCol = (INT8)(psSymbol->bSymbolColumns - (UINT8)(2 * psSymbol->bHorizontalDataRegions));

    /* Set pointer to the location of the Placement Table */
    pbArray = (INT8*)psSymbol->pbBitLookupTable;

    /* Compute the number of entries in the table */
    wLimit = (UINT16)(bNRow * bNCol) / 8; 

    /* First, clear the table with invalid entries */ 
    for (wInx = 0; wInx < wLimit; wInx++) 
        for (bCol = 0; bCol < 2; bCol++)
            *pbArray++ = 0;

    /* Starting in the correct location for character #1,...  */
    /* The starting location of the least significant bit is  */
    /* always in column 0, row 4                              */

    bRow = 4;                /* Initialize the starting row          */
    bCol = 0;                /* Initialize the starting column       */
    fbSpecialCorner = FALSE;  /* Clear the found special corner flag  */
    pbArray = (INT8*)psSymbol->pbBitLookupTable;

    /* The algorithm to determine the placement of the data first checks    */
    /* for a special corner condition, then sweeps upward diagonally to the */
    /* right, placing starting locations until outside the boundaries and   */
    /* sweeps downward and to the left, placing starting locations until    */
    /* outside the boundaries.  This three step process continues until all */
    /* entries in the table are placed.                                     */

    do 
    {
        /* repeatedly check for one of the special corner condition, then... */
        /* check for corner condition 1 */
        if ((bRow == bNRow) && (!bCol))
        {
            /* special corner condition number 1 */
            *pbArray++ = -1;
            *pbArray++ = -1;
            fbSpecialCorner = TRUE;
        }

        /* check for corner condition 2 */
        if ((bRow == bNRow - 2) && (!bCol) && (bNCol % 4)) 
        {
            /* special corner condition number 2 */
            *pbArray++ = -2;
            *pbArray++ = -2;
            bRow -= 2;
            bCol += 2;
            fbSpecialCorner = TRUE;
        }

        /* check for corner condition 3 */
        if ((bRow == bNRow + 4) && (bCol == 2) && (!(bNCol % 8))) 
        {
            /* special corner condition number 3 */
            *pbArray++ = -3;
            *pbArray++ = -3;
            bRow -= 2;
            bCol += 2;
            fbSpecialCorner = TRUE;
        }

        /* check for corner condition 4 */
        if ((bRow == bNRow - 2) && (!bCol) && (bNCol % 8 == 4)) 
        {
            /* special corner condition number 4 */
            *pbArray++ = -4;
            *pbArray++ = -4;
            bRow -= 2;
            bCol += 2;
            fbSpecialCorner = TRUE;
        }

        /* sweep upward diagonally, inserting successive characters,... */
        do 
        {
            /* place the entry if it is valid and does not overwrite a  */
            /* a special corner condition.                              */
            if ((bRow < bNRow) && (bCol >= 0) && 
                !(fbSpecialCorner && (!bRow) && (bCol == bNCol - 2)))
            {
                /* bRow and bCol are the position for bit 8 (lsb), we want the */
                /* position of bit 1 (msb) for the placement table           */
                Module((bRow - 2), (bCol - 2), bNRow, bNCol, &pbArray);
            }

            bRow -= 2; 
            bCol += 2; 

        } while ((bRow >= 0) && (bCol < bNCol));  /* while still in the symbol */

        bRow += 1; 
        bCol += 3; /* setup to sweep downward */

        /* & then sweep downward diagonally, inserting successive characters,.*/
        do 
        {
            /* place the entry if it is valid and does not overwrite a  */
            /* a special corner condition.                              */
            if ((bRow >= 0) && (bCol < bNCol) && 
                !(fbSpecialCorner && (bRow == 1) && (bCol == bNCol - 1)))
            {
                Module((bRow - 2), (bCol - 2), bNRow, bNCol, &pbArray);
            }

            bRow += 2; 
            bCol -= 2;

        } while ((bRow < bNRow) && (bCol >= 0));  /* while still in the symbol */

        bRow += 3; 
        bCol += 1; /* setup to sweep upward */

    /* ... until the entire table is filled */
    } while ((bRow < bNRow) || (bCol < bNCol));
}


/**********************************************************************
DESCRIPTION:
The GenDMCodeWord function will generate ASCII followed by Base 256 or C40 encoded 
code words for a Data Matrix barcode.

INPUT PARAMS:
    pbdata    - pointer to the data to converted to codewords.
        
RETURN VALUE:
    UINT32 - status of the code word generation.

HISTORY:
    2011.08.26  Jane Li    Change the original function name to GenDMCodeWordForNonC40,
    					Keep the this function name to be called to decide the way
    					 to generate ASCII for a Data Matrix barcode. 
**********************************************************************/
UINT32 GenDMCodeWord(const UINT8 *pbData)
{
	UINT32 lError = IG_NO_ERROR; /* status of the codeword generation */
	unsigned char ucBarcodeType;


	ucBarcodeType = psBarCode->bBarcodeType & GET_BARCODE_TYPE;
	if ((ucBarcodeType == DM_C40_ONLY_ENCODING) ||
		(ucBarcodeType == DM_VCR_C40_ONLY_ENCODING))
	{
		lError = GenDMCodeWordForC40(pbData);
	}
	else
	{
		lError = GenDMCodeWordForNonC40(pbData);
	}

	return (lError);
}


/**********************************************************************
DESCRIPTION:
The GenDMCodeWordForNonC40 function will generate ASCII followed by Base 256 encoded 
code words for a Data Matrix barcode.
(1) The Data Matrix Randomizer converts a binary input at a given position to 
    a new randomized output codeword.
(2) Add header bytes to identify control mode.
(3) Separate input data into one or more Data Matrix symbols.
(4) Add "padding" codewords if not enough input data to fill the symbol(s).
 
Data Matrix Codeword Format is as follows:
    Append mode byte (multi-symbol only)
    file ID1  (multi-symbol only)
    file ID2  (multi-symbol only)
    sequence# (multi-symbol only)
    data codewords (ASCII encoded)
    mode byte (start base 256 encoding, if necessary) 
        length (if mode is base 256)
        .............
        data codewords
        ............
    pad (if necessary)
    ............
    ECC codewords                      
	 
INPUT PARAMS:
    pdata    - pointer to the data to converted to codewords.
        
RETURN VALUE:
    UINT32 - status of the code word generation.
**********************************************************************/

UINT32 GenDMCodeWordForNonC40(const UINT8 *pbData)
{
	BARCODE_ENCODING_INSTRUCTIONS	arBcEncodeInstr[MAX_SWITCHES+1];
	unsigned short	usTotalBytes;
	unsigned char	ucNumInstr;
    UINT32 lError = IG_NO_ERROR; /* status of the codeword generation */
    register UINT16  *pwCwPtr;        /* pointer to Codeword Memory           */
    register UINT8   *pbBytePtr;      /* pointer to Data Memory               */

    register UINT16  wByteCnt;        /* input byte count                     */
    register UINT16  wCwCnt;          /* code words per symbol                */
	unsigned short	usInx1;
	unsigned short	usInx2 = 0;
    UINT8   bData;           /* holder of data to be encoded         */
	unsigned char ucBarcodeType;
	char	pLogBuf[128];


    (void)sprintf(pLogBuf, "Doing other than C40 encoding");
	fnDumpStringToSystemLog(pLogBuf);

	ucBarcodeType = psBarCode->bBarcodeType & GET_BARCODE_TYPE;

    /* Initialization for determination of encoding instructions */
	(void)memset(arBcEncodeInstr, 0, sizeof(arBcEncodeInstr));
	usTotalBytes = 0;
	ucNumInstr = 0;
    pbBytePtr = (UINT8 *)pbData;                 /* Set pointer to data memory */	//lint !e605

    if ((ucBarcodeType == DM_ASCII_THEN_BINARY_ENCODING) || 
		(ucBarcodeType == DM_VCR_ASCII_BINARY_ENCODING))
	{
		unsigned char	ucInx3 = 0;
		unsigned char	ucNumConsecutiveNumbers = 0;
		BOOL		fbNeedToSwitch = FALSE;


		arBcEncodeInstr[ucNumInstr].fbDoAscii = TRUE;
		usInx1 = psBarCode->wNumberData;   /* Initialize byte counter    */
		while (usInx1 && (ucNumInstr < MAX_SWITCHES))
		{
			if  (arBcEncodeInstr[ucNumInstr].fbDoAscii == TRUE)
			{
				// doing ASCII
				while (usInx1 && (fbNeedToSwitch == FALSE))
				{
					// doing ASCII encoding
					if (*pbBytePtr > 0x7F)
					{
						// need to switch to binary
						fbNeedToSwitch = TRUE;
					}
					else
					{
						// keep counting for ASCII, but check for consecutive numbers so we can
						// see if it's worth changing to ASCII before the next change to binary
						if ((*pbBytePtr > 0x2F) && (*pbBytePtr < 0x3A))
						{
							// it's an ASCII number, so check if we need to account
							// for 2 consecutive ASCII numbers
							ucInx3++;
							if (ucInx3 >= 2)
							{
								ucNumConsecutiveNumbers++;
								ucInx3 = 0;
							}
						}
						else
						{
							// it's not an ASCII number
							ucInx3 = 0;
						}

						usInx2++;
						usInx1--;
						pbBytePtr++;
					}
				}
			}
			else		// doing binary encoding
			{
				while (usInx1 && (fbNeedToSwitch == FALSE))
				{
					if (*pbBytePtr < 0x80)
					{
						// need to switch to ASCII
						fbNeedToSwitch = TRUE;
					}
					else
					{
						// keep counting for binary
						usInx2++;
						usInx1--;
						pbBytePtr++;
					}
				}
			}

			if (fbNeedToSwitch == TRUE)
			{
				if (arBcEncodeInstr[ucNumInstr].fbDoAscii == FALSE)
				{
					// were doing binary.
					// need to switch to ASCII, so do it in any case.
					// store data for the current binary group.
					// since we could have backtracked to this binary group,
					// have to add usInx2 to the instruction byte count instead
					// of just loading it.
					arBcEncodeInstr[ucNumInstr].usByteCount += usInx2;
					usTotalBytes += usInx2;

					// set up for an ASCII group
					ucNumInstr++;
					arBcEncodeInstr[ucNumInstr].fbDoAscii = TRUE;
					usInx2 = 0;
					fbNeedToSwitch = FALSE;
					ucInx3 = 0;
					ucNumConsecutiveNumbers = 0;
				}
				else
				{
					// were doing ASCII
					// need to switch to binary.
					// check if we're past the first group
					if (!ucNumInstr  && !usInx2)
					{
						// we're still in the first group & there were no ASCII characters at the
						// start of the input data, so switch the first group to binary
						arBcEncodeInstr[ucNumInstr].fbDoAscii = FALSE;
						fbNeedToSwitch = FALSE;
						usTotalBytes += 2;
					}
					else
					{
						// we're either past the first group or there were ASCII characters at
						// the start of the input data.
						// check if the current group should be left as ASCII as long as it
						// isn't the first group
						if (ucNumInstr && (ucNumConsecutiveNumbers < 2))
						{
							// it isn't the first group and this group of ASCII doesn't have enough compression
							// to compensate for the 2 bytes that will have to be added to switch to binary,
							// so add this group to the previous binary group
							ucNumInstr--;
							arBcEncodeInstr[ucNumInstr].usByteCount += usInx2;
							usTotalBytes += usInx2;
							usInx2 = 0;
							fbNeedToSwitch = FALSE;
						}
						else
						{
							// we're processing the first group or a subsequent group of ASCII will
							// have enough compression to compensate for the 2 bytes that will have
							// to be added for this switch to binary.
							// store data for the ASCII group
							arBcEncodeInstr[ucNumInstr].usByteCount = usInx2;
							usTotalBytes += usInx2 - ucNumConsecutiveNumbers + 2;

							// set up for a binary group
							ucNumInstr++;
							arBcEncodeInstr[ucNumInstr].fbDoAscii = FALSE;
							usInx2 = 0;
							fbNeedToSwitch = FALSE;
						}
					}
				}
			}
			else
			{
				// need to load the data for the last group of bytes no matter what type.
				// since we could have backtracked to a binary group,
				// have to add usInx2 to the instruction byte count instead
				// of just loading it.
				arBcEncodeInstr[ucNumInstr].usByteCount += usInx2;
				usTotalBytes += usInx2;
				if (arBcEncodeInstr[ucNumInstr].fbDoAscii == TRUE)
				{
					usTotalBytes -= ucNumConsecutiveNumbers;
				}

				ucNumInstr++;
			}
		}
	}
	else		// set up the data for straight binary encoding
	{
		arBcEncodeInstr[ucNumInstr].fbDoAscii = FALSE;
		arBcEncodeInstr[ucNumInstr].usByteCount = psBarCode->wNumberData;
		usTotalBytes = psBarCode->wNumberData + 2;
		ucNumInstr++;
		usInx1 = 0;
	}

	if (usInx1)
	{
	    (void)sprintf(pLogBuf, "Ran out of encoding switches, can do %u switches, have %u bytes left", MAX_SWITCHES, usInx1);
	    fnDumpStringToSystemLog(pLogBuf);
        lError = TOO_MANY_ENCODING_SWITCHES;
	}
	else
	{
		/* Initialization for actual encoding */
		pbBytePtr = (UINT8 *)pbData;                 /* Reset pointer to data memory */	//lint !e605

		/* Code word generation */
		/* Generate codewords for one Data Matrix symbol */
		/* Initialize Randomizers */
		wRandom253 = RANDOMIZER_VALUE;
		wRandom255 = RANDOMIZER_VALUE;

		/* set pointer to codeWord memory */
		pwCwPtr = psSymbol->pwCodeWords; 

		/* Initialize Codeword Counters */
		wCwCnt = psSymbol->wDataCodeWords;

		if (usTotalBytes > wCwCnt)
		{
		    (void)sprintf(pLogBuf, "Too much data to encode, needed words = %u, allowed words = %u", usTotalBytes, wCwCnt);
		    fnDumpStringToSystemLog(pLogBuf);
			lError = TOO_MUCH_DATA_FOR_BARCODE;
		}
		else
		{
			for (usInx1 = 0; (usInx1 < ucNumInstr) && wCwCnt && (lError == IG_NO_ERROR); usInx1++)
			{
				wByteCnt = arBcEncodeInstr[usInx1].usByteCount;   /* Initialize byte counter    */
				if (arBcEncodeInstr[usInx1].fbDoAscii == TRUE)
				{
					while (wByteCnt && wCwCnt)
					{
						bData = *pbBytePtr;
						pbBytePtr++;
						if ((bData > UPPER_DIGIT) || (bData < LOWER_DIGIT))
						{
							wByteCnt--;
							bData++;     /* Add 1 to ASCII encode the data */
						}         
						else if (wByteCnt > 1)
						{
							if ((*pbBytePtr > UPPER_DIGIT) || (*pbBytePtr < LOWER_DIGIT))
							{
								/* Only 1 digit so encode as an ASCII character */
								wByteCnt--;
								bData++;     /* Add 1 to ASCII encode the data */
							}
							else
							{
								/* Have 2 digits in a row, combine them to get a value  */
								/* between 00 and 99 plus the DOUBLE_DIGIT_OFFSET or    */
								/* 0x82 and 0xE5                                        */
								bData = ((bData & 0x0f) * 10) + 
										   ((*pbBytePtr) & 0x0f) + DOUBLE_DIGIT_OFFSET;
								pbBytePtr++;
								wByteCnt -= 2;
							}
						}
						else
						{
							/* Only 1 digit so encode as an ASCII character */
							wByteCnt--;
							bData++;     /* Add 1 to ASCII encode the data */
						}
        
						*pwCwPtr = bData;
						pwCwPtr++;
						wCwCnt--;
						IncRand253;
						IncRand255;
					}

				}
				else
				{
					/* Switch to binary mode, and write randomized       */
					/* length of data (Base 256 codewords in the symbol) */
					if (wByteCnt > (wCwCnt - psBarCode->wOverhead))
					{
						usInx2 = 0;
					    (void)sprintf(pLogBuf, "Too much data left to encode, binary words left = %u, available space = %u",
										wByteCnt, (wCwCnt - psBarCode->wOverhead));
					    fnDumpStringToSystemLog(pLogBuf);
					    lError = TOO_MUCH_DATA_FOR_BARCODE;
					}
					else
						usInx2 = wByteCnt; /* Remaining data in the symbol is encoded in 256 mode */

					/* Write Base 256 Encodation mode */
					*pwCwPtr = LATCH_256;
					pwCwPtr++;
					wCwCnt--;
					IncRand253;
					IncRand255;

					*pwCwPtr = (UINT8)(wRandom255 + usInx2 + 1);
					pwCwPtr++;

					wCwCnt--;
					IncRand253;
					IncRand255;

					if (usInx2)
					{
						/* Overhead completed.  Encode the data in binary mode */
						while (wByteCnt && wCwCnt)
						{
							*pwCwPtr = (UINT8)(wRandom255 + *pbBytePtr + 1);
							pwCwPtr++;
							wCwCnt--;
							pbBytePtr++;
							wByteCnt--;
							IncRand253;
							IncRand255;
						}
					}
				}
			}

			// if everything is OK, check if we need to do any padding
			if (lError == IG_NO_ERROR)
			{
				if (wCwCnt)
				{
					// there are code words left to fill, so put in padding
					/* Write pad indicator byte to start padding */
					*pwCwPtr = PAD;
					pwCwPtr++;
					wCwCnt--;
					IncRand253;

					/* Fill the rest with randomized pads */
					while (wCwCnt)
					{
						usInx1 = wRandom253 + PAD + 1;
						if (usInx1 > 254)
							usInx1 -= 254;

						*pwCwPtr = (UINT8)usInx1;
						pwCwPtr++;

						wCwCnt--;
						IncRand253;
					}
				}
			}
		}
	}
    
    return (lError);
}


/**********************************************************************
DESCRIPTION:
The GenDMCodeWordForC40 function will generate C40 encoded 
code words for a Data Matrix barcode.
     
INPUT PARAMS:
    pbdata    - pointer to the data to converted to codewords.
        
RETURN VALUE:
    UINT32 - status of the code word generation.

HISTORY:
    2011.08.26  Jane Li    Merged GenDMCodeWord from AMITA to support C40 encode
**********************************************************************/
UINT32 GenDMCodeWordForC40(const UINT8 *pbData)
{
	char       *pwWork;
	const char *pbC40Table = " 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	const char *pbShift2   = "!\"#$%&'()*+,-./:;<=>?@[\\]_";
	const char *pbShift3   = "`abcdefghijklmnopqrstuvwxyz{|}~\177";

    UINT32 lError = IG_NO_ERROR; /* status of the codeword generation */

    register UINT16  *pwCwPtr;        /* pointer to Codeword Memory           */
    register UINT8   *pbBytePtr;      /* pointer to Data Memory               */

    register UINT16  wCwCnt;          /* code words per symbol                */

    unsigned short	usInx1 = 0;
    unsigned short  usInx2 = 0;
    unsigned short  usInx3 = 0;
    unsigned short  usBinCnt = 0;
    unsigned short  usBinaryLen;
	unsigned short	usEncodingLen;
	unsigned short	usTemp;
	unsigned short	wC40inx;

	char	pLogBuf[128];
	char	wC40out[6];

    unsigned char   ucEncodingType[MAX_DATA_2DBC];
    unsigned char   bCurrentChar;

	BOOL			fbC40End;


    (void)sprintf(pLogBuf, "Doing C40 encoding");
	fnDumpStringToSystemLog(pLogBuf);

	usEncodingLen = (psBarCode->wNumberData) + 1;

    /* Initialization for determination of encoding instructions */
	(void)memset(ucEncodingType, 'c', usEncodingLen-1);
    ucEncodingType[usEncodingLen-1] = 0;

    /* Initialization for actual encoding */
	pbBytePtr = (UINT8 *)pbData;                 /* Reset pointer to data memory */  //lint !e605

    /* set pointer to codeWord memory */
	pwCwPtr = psSymbol->pwCodeWords;

    /* Initialize Codeword Counters */
	wCwCnt = psSymbol->wDataCodeWords;

	usInx1 = 0;
	usInx2 = 0;
	while (usInx1 < (usEncodingLen -1)  && (lError == IG_NO_ERROR))
	{
		switch (ucEncodingType[usInx1])
        {
            case'a': //ASCII Encoding
                if (usInx1 != 0 && ucEncodingType[usInx1-1] == 'c')
                {
                    pwCwPtr[usInx2++] = LATCH_ASCII;
                }

//				Since it's only a byte value, it's always <= 0xFF
                if (pbBytePtr[usInx1] > 0x7F)
                {
                    pwCwPtr[usInx2++] = 235;
                    pwCwPtr[usInx2++] = (pbBytePtr[usInx1] - 127);
                    usInx1++;
                }
                else if ((pbBytePtr[usInx1] >= LOWER_DIGIT) && (pbBytePtr[usInx1] <= UPPER_DIGIT))
                {
                    if ((pbBytePtr[usInx1+1] >= LOWER_DIGIT) && (pbBytePtr[usInx1+1] <= UPPER_DIGIT) &&
                        (ucEncodingType[usInx1+1]  == 'a') && (usInx1+1 < usEncodingLen))
                    {
                        pwCwPtr[usInx2++] = (((pbBytePtr[usInx1]-'0') * 10) +
                        (pbBytePtr[usInx1+1]-'0')) + DOUBLE_DIGIT_OFFSET;
                        usInx1 += 2;
                    }
                    else
                    {
                        pwCwPtr[usInx2++] = (pbBytePtr[usInx1]) + 1;
                        usInx1++;
                    }
                }
                else
                {
                    pwCwPtr[usInx2++] = (pbBytePtr[usInx1]) + 1;
                    usInx1++;
                }
                break;

            case 'b': //Binary encoding
                if (( usInx1 == 0 && ucEncodingType[usInx1] == 'b') || (usInx1 != 0 && ucEncodingType[usInx1-1] != 'b'))
                {
                    pwCwPtr[usInx2++] = LATCH_256;
                }

                usInx3 = 0;
                usBinCnt = usInx1;
                while (ucEncodingType[usBinCnt++] == 'b')
                    usInx3++;

                usBinaryLen = usInx3;

                if ( wCwCnt - usInx2 < 250)
                {
					usTemp = usInx2 + 1;
                    pwCwPtr[usInx2++] = (usBinaryLen + (((149 * usTemp) % 255) + 1)) % 256 ;
                }
                else
                {
					usTemp = usInx2 + 1;
                    pwCwPtr[usInx2++] = (((usBinaryLen / 250) + 249 ) + (((149 * usTemp) % 255) + 1)) % 256 ;
					usTemp = usInx2 + 1;
                    pwCwPtr[usInx2++] = ((usBinaryLen % 250) + (((149 * usTemp) % 255) + 1)) % 256 ;
                }
   
                while ( ucEncodingType[usInx1] == 'b')
                {
					usTemp = usInx2 + 1;
                    pwCwPtr[usInx2++] = (pbBytePtr[usInx1] + (((149 * usTemp) % 255) + 1)) % 256 ;
                    usInx1++;
                }
                break;

            case 'c': //C40 encoding
                if (( usInx1 == 0 && ucEncodingType[usInx1] == 'c') || ( usInx1 != 0 && ucEncodingType[usInx1-1] != 'c'))
                {
                    pwCwPtr[usInx2++] = LATCH_C40; // C40 mode
                }

                wC40inx = 0;
                do
                {
                    fbC40End = FALSE;
                    bCurrentChar = pbBytePtr[usInx1++];
                    if (bCurrentChar & 0x80)
                    {   //extended char ( if bCurrentChar>128 )
                        bCurrentChar &= 0x7F;
                        wC40out[wC40inx++] = 1;
                        wC40out[wC40inx++] = 30;
                    }

                    pwWork = (char *)strchr (pbC40Table, bCurrentChar);
                    if ((pwWork) && (bCurrentChar != 0))
                    {
                        wC40out[wC40inx++] = ((pwWork - pbC40Table) + 3) % 40;
                    }
                    else
                    {
                        if (bCurrentChar < 32)
                        { // Shift 1
                            wC40out[wC40inx++] = 0;
                            wC40out[wC40inx++] = (char)bCurrentChar;
                        }
			 		    else
                        {
                            pwWork = (char *)strchr (pbShift2, bCurrentChar);
                            if (pwWork)
                            {  // Shift 2
                                wC40out[wC40inx++] = 1;
                                wC40out[wC40inx++] = (char)(pwWork - pbShift2);
                            }
                            else
                            {
                                pwWork = (char *)strchr (pbShift3, bCurrentChar);
                                if (pwWork)
                                {
                                    wC40out[wC40inx++] = 2;
                                    wC40out[wC40inx++] = (char)(pwWork - pbShift3);
                                } 
                                else
                                {
								    (void)sprintf(pLogBuf, "Invalid data for C40");
								    fnDumpStringToSystemLog(pLogBuf);
                                    lError = IG_INVALID_BARCODE_DATA;
									break;
                                }
                            }
                        }
                    }

                    if (((wC40inx == 2) && (usInx2 + 2 == wCwCnt) && (usInx1 == usEncodingLen)) ||
                       ((wC40inx == 2) && (ucEncodingType[usInx1+1] == 'a') && ( usInx1 < usEncodingLen - 2)))
                    {
                        wC40out[wC40inx++] = 0; // Shift 1 pad at end
                    }

                    while (wC40inx >= 3)
                    {
                        UINT16 v = (unsigned short)((wC40out[0] * 1600) + (wC40out[1] * 40) + (wC40out[2] + 1));


                        pwCwPtr[usInx2++] = (v >> 8);
                        pwCwPtr[usInx2++] = (v & 0xFF);
                        wC40inx -= 3;
                        wC40out[0] = wC40out[3];
                        wC40out[1] = wC40out[4];
                        wC40out[2] = wC40out[5];
                        fbC40End = TRUE;
                    }
                }
                while(ucEncodingType[usInx1] == 'c');		// end of do..while

                if ((fbC40End == FALSE) && (lError == IG_NO_ERROR))
                {
                    ucEncodingType[--usInx1] = 'a';
                }
                break;

            default:
                break;
        } // end switch

        // check if we can support another loop of encoding
        if (((usInx2-1) > psSymbol->wDataCodeWords) && (lError == IG_NO_ERROR))
        {
		    (void)sprintf(pLogBuf, "Too much data to encode, current index = %u, allowed size = %u", (usInx2-1), psSymbol->wDataCodeWords);
		    fnDumpStringToSystemLog(pLogBuf);
            lError = TOO_MUCH_DATA_FOR_BARCODE;
        }
    } // end while (still have stuff to encode && don't have an error)
  
    if (lError == IG_NO_ERROR)
    {
        //PADDING
        if (usInx2 < ((psSymbol->wDataCodeWords) -1 ) && ucEncodingType[usInx1-1] == 'c') // End of Data
            pwCwPtr[usInx2++] = LATCH_ASCII;

        if (usInx2 < ((psSymbol->wDataCodeWords) -1 )) // First padding code word
            pwCwPtr[usInx2++] = PAD;

        while (usInx2 < psSymbol->wDataCodeWords)
		{
			usTemp = usInx2 + 1;
            pwCwPtr[usInx2++] = (PAD + (((149 * usTemp) % 253) + 1)) % 254 ;
        }
    }

    return (lError);
}


/************************* InitializeBarCode **************************/

UINT32 InitializeBarCode(const EMD_BARCODE *psEMDBarcodeData)
{

    const BARCODE_INFO *psBarcodeTable;       /* Pointer to barcode           */
                                              /* initialization  table        */
    const EMD_BARCODE_SYMBOL  *psEMDSymbol;   /* Pointer to the initial       */
                                              /* barcode info from the host   */
	UINT32            lError = IG_NO_ERROR;   /* Error status                 */
	void *	pTempPtr;


    /* Re-initialize the memory buffer used to allocate the barcode memory */
    pbDmMemory = bDmMemory;

    /* Determine if can handle the number of symbols requested */    
    if (psEMDBarcodeData->bNumberSymbols == MAX_SYMBOLS)
    { 
        /* Start to initialize the barcode memory based on the information  */
        /* from the host.                                                   */
        psBarCode->bBarcodeType =		psEMDBarcodeData->bBarcodeType;
	    psBarCode->bNumberSymbols =		psEMDBarcodeData->bNumberSymbols;
	    psBarCode->bQuietZones =		psEMDBarcodeData->bQuietZones;
	    psBarCode->bHorizontalExp =		psEMDBarcodeData->bHorizontalExp;
	    psBarCode->bVerticalExp =		psEMDBarcodeData->bVerticalExp;
	    psBarCode->bHThin =				psEMDBarcodeData->bMaxRowThinning;
	    psBarCode->bVThin =				psEMDBarcodeData->bMaxColumnThinning;
	    psBarCode->bFileID1 =			psEMDBarcodeData->bFileID1;
	    psBarCode->bFileID2 =			psEMDBarcodeData->bFileID2;
	    psBarCode->bSpare =				psEMDBarcodeData->bSpare;
	    psBarCode->wNumberData =		psEMDBarcodeData->wNumberOfDataBytes;
        psBarCode->wExtraPixelsAbove =	psEMDBarcodeData->wExtraPixelsAbove;
        psBarCode->wExtraPixelsBelow =	psEMDBarcodeData->wExtraPixelsBelow;

        /* Only handling Base 256 encoding, so overhead is consistent. */
        /* If add aditional encoding methods, overhead may have to change. */
        psBarCode->wOverhead = DATAMATRIX_SINGLE_OVERHEAD;
        
        psEMDSymbol = &(psEMDBarcodeData->sBarcodeSymbols);

        /* Start to fill in information on the first symbol in the barcode */    
	    psSymbol->bSymbolRows = psEMDSymbol->bSymbolRows;
	    psSymbol->bSymbolColumns = psEMDSymbol->bSymbolColumns;
	    psSymbol->fbSymbolBelow = psEMDSymbol->fbSymbolBelow;

        /* Find the current symbol in the info table */
        lError = FindBarcodeInfo(psSymbol->bSymbolRows, psSymbol->bSymbolColumns, 
                                &psBarcodeTable);
    
	    if (lError == IG_NO_ERROR)
	    {
	        /* Update the symbol with the information from the info table */
			// ignore lint warning 644 about things not being initialized.
			// if there ware no errors, everything was initialized above.
		    psSymbol->bVerticalDataRegions = psBarcodeTable->bVerticalDataRegions;
		    psSymbol->bHorizontalDataRegions = psBarcodeTable->bHorizontalDataRegions;
		    psSymbol->wDataCodeWords = psBarcodeTable->wDataCodeWords;
		    psSymbol->wEccWords = psBarcodeTable->wEccWords;
		    psSymbol->bRowWrapOffset = psBarcodeTable->bRowWrapOffset;
		    psSymbol->bColWrapOffset = psBarcodeTable->bColWrapOffset;
			lwThinningMask[0] = (UINT32)((psBarcodeTable->wThinningMask[0] << 16) + psBarcodeTable->wThinningMask[1]);
			lwThinningMask[1] = (UINT32)((psBarcodeTable->wThinningMask[2] << 16) + psBarcodeTable->wThinningMask[3]);

	        /* Pad extra modules above or below the symbol */
			// ignore lint warning 644 about things not being initialized.
			// if there ware no errors, everything was initialized above.
/*        if (psEMDSymbol->bExtraModulesBelow)
		        psSymbol->bExtraSpace = (SINT8)(-psEMDSymbol->bExtraModulesBelow);
	        else
		        psSymbol->bExtraSpace = (SINT8)psEMDSymbol->bExtraModulesAbove;
*/
	        /* Start to allocate memory from the barcode memory block */
	        /* Pointer to the location of the CodeWords */
			pTempPtr = (void *)&psSymbol->pwCodeWords;
		    lError = AllocateBarcodeMemory((UINT8 **)pTempPtr,
	                                       (UINT16)(psSymbol->wDataCodeWords * 2),
	                                       TRUE);
	    }

		/* Pointer to the location of the ECCWords */
	    if (lError == IG_NO_ERROR)
		{
			pTempPtr = (void *)&psSymbol->pwECCWords;
		    lError = AllocateBarcodeMemory((UINT8 **)pTempPtr,
	                                       (UINT16)(psSymbol->wEccWords * 2),
										   TRUE);
		}

	    /* Pointer to the Bit placement table */
	    if (lError == IG_NO_ERROR)
		    lError = AllocateBarcodeMemory((UINT8 **) &psSymbol->pbBitLookupTable,
											(UINT16)((psSymbol->wDataCodeWords + psSymbol->wEccWords) * 2),
											FALSE);

		/* Pointer to the location of the bit mapping */
	    if (lError == IG_NO_ERROR)
		{
			pTempPtr = (void *)&psSymbol->pwBitmap;
		    lError = AllocateBarcodeMemory((UINT8 **)pTempPtr,
	                                       psBarcodeTable->wBitmapSize, FALSE);
		}

	    /* Allocate memory for the maximum ECC generation polynomial */
	    if (lError == IG_NO_ERROR)
		{
			pTempPtr = (void *)&psSymbol->pwECCPolyWords;
		    lError = AllocateBarcodeMemory((UINT8 **)pTempPtr,
	                                       (UINT16)(MAX_ECC_WORDS * 2),
										   TRUE);
		}
    
	    if (lError == IG_NO_ERROR)
	    {
	        /* Calculate the height of the barcode in modules */
        	psBarCode->wBarcodeHeight = psSymbol->bSymbolRows;
	        /* Calculate the width of the barcode in modules */
        	psBarCode->wBarcodeWidth = psSymbol->bSymbolColumns;

			/* Signal that the barcode stuff has been loaded */
			fbBarCodeAvailable = TRUE;
	    }
    }
    else
        lError = TOO_MANY_BARCODE_SYMBOLS;

    return (lError);
}	


/**********************************************************************
DESCRIPTION:
The PlaceOnBitMap function determines the starting position of each code 
word in the bitmap and then has the code word placed in the bitmap. 
	 
INPUT PARAMS:
    wNumDataItems - Number of data items to be placed
    pwData        - Pointer to the code words to be placed
    pbLookupEntry - Pointer to the location to place the code words
        
RETURN VALUE:
    None.    
**********************************************************************/

void PlaceOnBitMap(const UINT16 wNumDataItems,
                   const UINT16 *pwData, const SINT8 *pbLookupEntry)
{
    UINT16  wInx;        /* Counter                  */
    SINT8   bDmCol;      /* The 1st bit col position */
    SINT8   bDmRow;      /* The 1st bit row position */
  

    /* Place data in the bitmap */
    for (wInx = 0; wInx < wNumDataItems; wInx++)
    {
        /* Get the location for this data word */
        bDmRow = *pbLookupEntry++;
        bDmCol = *pbLookupEntry++;

        /* If bDmRow is negative, the data is to be placed in a */
        /* special corner condition                            */
        if (bDmRow >= 0)
            BitPlace (bDmRow, bDmCol, (UINT8) *pwData);
        else
            CornerPlace (bDmRow, (UINT8) *pwData);

        /* Point to next data word */
        pwData++;
    }
}


/************************** SetECCParameters **************************/

UINT32 SetECCParameters(const UINT16 wMode)
{
    UINT32     lError = IG_NO_ERROR;
    const UINT8  *pbPolynomial;
    UINT16       *pwTemp;
	UINT16       wInx;
	UINT16		wNumECCCodeWords = psSymbol->wEccWords;


    if (wMode == MODE_DM)
    {
        /* Find Generator Polynomial for Data Matrix ECC */
        switch (wNumECCCodeWords)
        {
            case 5:
                pbPolynomial = bPoly5;
                break;

            case 7:
                pbPolynomial = bPoly7;
                break;

            case 10:
                pbPolynomial = bPoly10;
                break;

            case 11:
                pbPolynomial = bPoly11;
                break;

            case 12:
                pbPolynomial = bPoly12;
                break;

            case 14:
                pbPolynomial = bPoly14;
                break;

            case 18:
                pbPolynomial = bPoly18;
                break;

            case 20:
            	pbPolynomial = bPoly20;
                break;

            case 24:
                pbPolynomial = bPoly24;
                break;

            case 28:
                pbPolynomial = bPoly28;
                break;

            case 36:
                pbPolynomial = bPoly36;
                break;

            case 42:
                pbPolynomial = bPoly42;
                break;

            case 48:
                pbPolynomial = bPoly48;
                break;

            case 56:
                pbPolynomial = bPoly56;
                break;

            case 62:
                pbPolynomial = bPoly62;
                break;

            case 68:
                pbPolynomial = bPoly68;
                break;

            default:
		        lError = INVALID_NUMBER_OF_ECC_CODEWORDS;
                break;
        }

	    if (lError == IG_NO_ERROR)
	    {
		    pwTemp = psSymbol->pwECCPolyWords;

	        /* Load ECC Polynomial Generator */
        	for (wInx = 0; wInx < wNumECCCodeWords; wInx++, pwTemp++)
	        {
				/* load the data as words in order to save half the time */
				// ignore lint warning 644 about pbPolynomial not being initialized.
	            *pwTemp = (UINT16)*pbPolynomial++;		//lint !e644
	        }
	    }
    }
    else 
        lError = INVALID_EEC_MODE;
    
    return (lError);
}
