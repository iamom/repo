/**********************************************************************
 PROJECT    :   Future Phoenix
 COPYRIGHT  :   2006, Pitney Bowes, Inc.  
 AUTHOR     :   Derek DeGennaro
 MODULE     :   FdrMcpTask.c
 
 DESCRIPTION:
    Implementation of the Feeder MCP Controller Task.

 ----------------------------------------------------------------------
 MODIFICATION HISTORY:

  2015.04.14 Way Zhang	   FPHX 02.10 cienet branch
   - Modified fnProcessSensorValuesIdleState() to fix fraca 229174.

  2012.11.13 Bob Li                 FPHX 02.10 cienet branch
   - Modified fnProcessInvalidReqForState(), fnFeederStoppedSendStopRsp() and
     fnSetSendStopRspFlag() to update the requested Feeder stop type in time.
     Fixed fraca 219120.
     
  2012.09.24 Clarisa Bellamy        FPHX 02.10 branch
   - Modified fnStartFeeder() function by replacing the if-else sequence that 
     set the non-wow speed flags to a switch statement.  Then added a new case 
     for the new speed, 70 LPM (SLOW).

  2010.07.25 Clarisa Bellamy             FPHX 02.07 branch
  - Modified to eliminate calls to fnGetCurrIndiciaType() which is an illegal
    function.  Bobtask internal data should never be accessed directly by other 
    tasks. 
    Orbit database, cjunior.h & .c, and cbobpriv.c have been modified to allow 
    access to indicia type via fnValidData.
    - Updated fnMailRunMailPieceDataHandler and fnCheckImageNeedsUpdating to
      get the indicia type (before and after postage change, respectively) by
      using fnValidData instead of fnGetCurrIndiciaType. 
  - Added code to fnCheckImageNeedsUpdating to check the ad also, but that is
    commented out until we actually allow that to change. (So far, the add is
    only changed based on the carrier type (PreSort Extra) which doesn't change
    on the fly.

  2010.07.07 Clarisa Bellamy & Raymond Shen     FPHX 02.02 branch
   Fix for GMSE 00191990 - DCAP / AR Discrepancy:
   - In function fnMailRunMailPieceDataHandler, before rating the next piece, 
     make sure the current piece has finished debiting.  If not, wait in a loop
     until it has, or until timeout. If paused, log how long the fdrTask was 
     paused for.  If timeout, fake a paper skipped feeder error.  
   - In function fnFeederQuerySensorTimerExpire, check if the fdrTask is paused.
     If so, stop the timer to these messages don't fill up the queue, log an 
     entry in the system log, and send a message to the fdrTask to start the 
     timer again (when the feeder is unpaused, it will get this message.) THEN
     send the msg to poll the sensors.
   - Added new function fnProcessPollSensorStartupReq, that starts the 
     poll-sensor timer (once the fdrTask is no longer paused.)

  2010.02.05 Bob Li - FPHX 2.00
    - Modified function fnMailRunMailPieceDataHandler() and fnCheckImageNeedsUpdating()
      for fixing the issue that system doesn't update the INDICIA type under WOW mode.

  2009.07.29 Clarisa Bellamy  - FPHX 2.00
    - To make the last changes more generic, added a new function, 
      fnCheckImageNeedsUpdating, which can be used to check for any change 
      in the static image that would require the feeder to send the 
      BOB_GEN_STATIC_IMAGE_REQ to bobtask.  This function can be expanded 
      in the future to check for a switch to/from the low-value indica.
    - Changed the order of the new functions, just because it made more sense to me.

  2009.07.28 Clarisa Bellamy  - FPHX 2.00
    For support of Auto Inscriptions in WOW:
    - Add include of bob.h and bobutils.h.
    - Move the end of the fnMailRunMailPieceDataHandler function into a new
      function, fnMailPieceDataHandlerContinue.  
    - Modify fnMailRunMailPieceDataHandler so that it saves the selected inscription,
      updates the inscription (after rating), then compares the new inscription with
      the current inscription.  If they are different, it sends a message to bobtask  
      to update the image.  If they are the same, call the new Continue function.
    - Add new function fnHandleGenStaticImageResponse that calls the Continue function 
      when the message is received from bobtask indicating the image has been updated.
    - Add new function, fnFeederMsgTimerExpire, which calls the continue function if the
      message is NOT received from the bobtask.

  07/24/2009  Raymond Shen    Modified fnMailRunProfileStatusHandler() to support MCP/profile error WOW_PIECE_TOO_LONG (0x3B).
  07/21/2009  Raymond Shen    1. Modified fnDMWWaitPieceStateGotoNewMode() to fix GMSE00166037 (G900 - DM475 Feeder cover open 
                              message is displayed after mail is removed off scale).
                              2. Modified fnFeederResetMCP(), fnMailRunMailPieceDataHandler(), fnIsWOWDimRatingError() 
                              and added fnSetWOWDimRatingErrorFlag() for WOW/SBR error handling.
  07/17/2009  Raymond Shen    Modified fnMailRunInfoLogErrorHandler(), fnMailRunMailPieceDataHandler(), 
                              fnIsMCPWOWError(), fnIsWOWDimRatingError(), and added fnMapWOWError() to handle WOW/SBR errors.
    06/24/2009  Raymond Shen    Modified fnProcessIdleStatePollSensorReq(): not to check the status of FS2 sensor bit.
    06/04/2009  Jingwei,Li      Added functions fnDiagWOWReadPCNInfo(),
                                                fnDiagWOWReadCycleCount(),
                                                fnDiagWOWReadJamCount(),
                                                fnDiagWOWResetJamCount(),
                                                fnHandleWOWCycleCountRsp(),
                                                fnHandleWOWJamCountRsp(),
                                and modified functions fnStartDiag(), fnLogSendIntertaskMsg() for 
                                Feeder/Wow Cycle/Jam counters.
    05/21/2009  Jingwei,Li      Added functions fnStartQuerySensorTimer() and fnStopQuerySensorTimer().
    05/21/2009  Raymond Shen    Modified fnProcessStartReq() to support restarting feeder from FDR_PAUSED_STATE in WOW mode.
    05/20/2009  Raymond Shen    Modified fnMailRunInfoLogErrorHandler(), and fnMailRunMailPieceDataHandler(), 
                                added fnIsMCPWOWError() and fnIsWOWDimRatingError() for wow error handling.
    05/19/2009  Raymond Shen    Added fnSetNumPiecesWeighed() and modified fnStartFeeder() for wow.
    05/18/2009  Raymond Shen    Modified fnProcessSensorValuesIdleState() for WOW.
    05/13/2009  Raymond Shen    Modified fnFeederDownloadDRData() and fnDiagWOWRetrieveParams() to get rid of the unused
                                DR status flags fMJDRCapable and fMJDRReady.
    05/12/2009  Jingwei,Li      Modified fnProcessStartReq() and fnStartFeeder()for WOW throughput feature.
    05/12/2009  Raymond Shen    Added fnGetNumPiecesWeighed() and modified fnStartFeeder() for Weigh First Piece mode.
    05/08/2009  Jingwei Li      Modified fnStartFeeder() to support WOW High/Medium/Low throughput speed.
    05/05/2009  Jingwei Li      Modified fnStartFeeder() to fix Weigh First Piece mode fraca.
    04/27/2009  Raymond Shen    Modified fnStopDiag() to fix the problem caused by stopping timer.
    04/24/2009  Raymond Shen    Added code to read/write NVM length calibration status in MCP instance and also
                                added code for setting width sensitivity.
    4/15/2009   Raymond Shen    Modified code for the new format of sensor status message between OI, CM, and FDR.
    4/13/2009   Raymond Shen    Modified fnDiagWOWLengthCalibrationSuccess(), fnDiagWOWLengthCalibrationFailure(), 
                                and fnDiagMailPieceFeed() for length calibration.
    4/09/2009   Raymond Shen    Added code for wow length calibration.
    3/31/2009   Raymond Shen    Added code to support wow motor speed test and modified fnHandleWOWRezeroStart() 
                                and fnHandleWOWRezeroDone() because FPHX is different to Midjet when doing wow rezero.
    3/26/2009   Raymond Shen    Modified fnMailRunMailPieceDataHandler() to get the proper length & width data for postage calculation.
    3/25/2009   Raymond Shen    Modified fnMailRunMailPieceDataHandler() to log mail piece weight and PIP dimension data;
                                Added fnReloadProfilesCompleteCB() and modified fnFeederLoadProfiles(), fnStartDiag()
                                for "Reload Profiles" option in feeder diagnostics menu.
                                Removed the unused function fnFeederGetMcpSerialNumber().
    3/19/2009   Raymond Shen    In service mode, "WOW Calibration", "Static Weight", "Set WOW Mode", 
                                "Re-zero WOW", and "Calibration Report" options work well now.
    1/25/2009   Raymond Shen    Added code for wow main printing flow and wow diagnostic mode
                                (Need to debug it later on the real DM475C which isn't ready for now).
    1/22/2009   Raymond Shen    Added some code for WOW.
    1/19/2006    Derek DeGennaro    Initial Version
      .           .                       .
    <Date>   <Developer_Name>   <Modification_Description>
 **********************************************************************/

/**********************************************************************
        Include Header Files
**********************************************************************/

/********************************************************/
// Turn off a bunch of unnessesary lint warnings 
// and errors for files that are non-feeder task related.
/********************************************************/
//lint -e46
//lint -e612
//lint -e783
//lint -e537
//lint -e726
//lint -e760
//lint -e725
//lint -e539
/********************************************************/
#include <stdio.h>
#include "bob.h"            // BOB_OK 
#include "bobutils.h"       // for BOBAMAT0, fnValidData(), 
#include "pbos.h"
#include "ossetup.h"
#include "mmcintf.h"
#include "mmcimjet.h"
#include "fdrcm.h"
#include "errcode.h"
#include "MCPDIntf.h"
#include "FdrMcpTask.h"
#include "cwrapper.h"
#include "utils.h"
//#include "rateservice.h"
#include "oit.h"
#include "hal.h"

// Feeder Private Data
#define INCLUDE_FEEDER_TASK_PRIVATE_DATA 1
#include "FdrMcpTaskPriv.h"
#undef INCLUDE_FEEDER_TASK_PRIVATE_DATA


static char pBOBACCESSERROR[] = "FdrWOW BobAccessError";


/********************************************************/
// Re-enable lint warnings
/********************************************************/
//lint +e46
//lint +e612
//lint +e783
//lint +e537
//lint +e726
//lint +e760
/********************************************************/


/**********************************************************************
        Local Function Prototypes
**********************************************************************/
// These are all located in fdrmcptaskpriv.h file

/**********************************************************************
        Functions
**********************************************************************/



/**********************************************************************

4.1.1   FeederMCPControllerTask
This is the Feeder MPC Controller Task entry point.

4.1.1.1 Scope
Public

4.1.1.2 Prototype
void FeederMCPControllerTask(unsigned long argc, void *argv);

4.1.1.3 Pre-Conditions
None.

4.1.1.4 Inputs
argc - Required to match task entry point function signature.  Unused.
argv - Required to match task entry point function signature.  Unused.

4.1.1.5 Processing

CALL InitializeFeederTaskData

REPEAT
  Wait to Receive an Intertask Message
  Log Intertask Message
  CALL fnProcessMsg
UNTIL Forever

**********************************************************************/
void FeederMCPControllerTask(unsigned long argc, void *argv)    //lint !e818
{
    INTERTASK               rMsg;                   // received intertask message

    if ( fnGetMeterModel() == CSD2 )
    	OSSuspendTask(FDRMCPTASK);
    
    fnSysLogTaskStart( "FeederMCPControllerTask", FDRMCPTASK );  
    fnInitializeFeederTaskData();   

    fnSysLogTaskLoop( "FeederMCPControllerTask", FDRMCPTASK );  
    while (FOREVER)
    {
        if( OSReceiveIntertask (FDRMCPTASK, &rMsg, OS_SUSPEND) == SUCCESSFUL )
        {
            fnLogRecvIntertaskMsg(&rMsg);
            fnProcessMsg(&rMsg);        
        }       
    }
}

/**********************************************************************

fnInitializeFeederTaskData
Initializes Feeder Task Private Data

Scope
Private

Prototype
static void fnInitializeFeederTaskData(void);

Pre-Conditions
None.

Inputs
None.

**********************************************************************/
static void fnInitializeFeederTaskData(void)
{
    unsigned long lwStatus = 0;
    UINT8 minorHwVer;
    
    (void)memset((void*)&sFdrFirmwareUpdateDetails,0,sizeof(sFdrFirmwareUpdateDetails));
    fnSetState(FDR_POWERUP_STATE);

    fnGetHardwareVersion(NULL, &minorHwVer);
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here

    if ( fnGetMeterModel() == CSD3 )
    {
//TODO - move into HAL and delete here
//        FDR_5V_CONTROL &= ~FDR_5V_CONTROL_BIT;       // turn on power to Feeder/WOW board
//        FDR_VM_CONTROL_PORT &= ~FDR_VM_CONTROL_BIT;  // turn on the 27v to Feeder/WOW board
        HALFeederSetPower(TRUE);
        HALFeederSet27Volts(TRUE);

    }

    // reset the feeder
    fnResetFeeder();
    // DM400c
    lwStatus = fnMCPLibCreate(&pG900FeederMcp,FDR_MCP_ID,FDRMCPTASK,FUTURE_PHOENIX_BASE_TYPE,TRUE,&sFeederReportInfo);
    FDRTASK_ASSERT(lwStatus == MCP_LIB_SUCCESS);//lint !e506
    FDRTASK_ASSERT(pG900FeederMcp);                 //lint !e506

#ifdef MIDJET_RIG_TESTING
    lwStatus = fnMCPLibCreate(&pMidjetWowMcp,MIDJET_WOW_MCP_ID,FDRMCPTASK,MIDJET_MCP,TRUE,&sFeederReportInfo);
    FDRTASK_ASSERT(lwStatus == MCP_LIB_SUCCESS);//lint !e506
    FDRTASK_ASSERT(pMidjetWowMcp);              //lint !e506
    pFeederMcp = pMidjetWowMcp;
#else
    pFeederMcp = pG900FeederMcp;
#endif

    FDRTASK_ASSERT(pFeederMcp);                 //lint !e506
    FDRTASK_ASSERT(lwStatus == MCP_LIB_SUCCESS);//lint !e506

    if (!pFeederMcp || lwStatus != MCP_LIB_SUCCESS)
        fnSetState(FDR_INACTIVE_STATE);
    

#ifdef FP_DUMMY_CM
    OSResumeTask(_CMTASK_);
#endif //FP_DUMMY_CM
}

/**********************************************************************
4.1.2   fnProcessMsg
This function handles an intertask message by dispatching it to the 
appropriate handler function based on the state of the Feeder Task.

4.1.2.1 Scope
Private

4.1.2.2 Prototype
static void fnProcessMsg(const INTERTASK *pMsg);

4.1.2.3 Pre-Conditions
pMsg != NULL

4.1.2.4 Inputs
pMsg - Pointer to an Intertask Message.

4.1.2.5 Processing

IF the message is from the MCP Class Driver THEN
  CALL fnMCPHandleUSBDriverMsg(FeederMcpInst, msg)
END IF

DISPATCH the message based on the current state, message id, and message contents

**********************************************************************/
static void fnProcessMsg(const INTERTASK *pMsg)
{
    unsigned long lwState = 0;

    FDRTASK_ASSERT(pMsg);       //lint !e506
    FDRTASK_ASSERT(pFeederMcp); //lint !e506

    if (!pMsg || !pFeederMcp)
        return;

    // first notify the MCP Instance of any USB events and Motion Class Messages
    if (pMsg->bSource == MCP_CLASS_DRVR || pMsg->bSource == USB_DIRECT_CLASS_DRVR)
    {
        fnMCPHandleUSBDriverMsg(pFeederMcp,pMsg);
    }

    lwState = fnGetState();
    switch (lwState) {
    case FDR_POWERUP_STATE:             fnDispatchMsgHandler(pMsg,
                                                sPowerUpStateMsgHandler,
                                                ARRAY_LENGTH(sPowerUpStateMsgHandler)); break;          
    case FDR_START_INIT_STATE:          fnDispatchMsgHandler(pMsg,
                                                sInitStateMsgHandler,
                                                ARRAY_LENGTH(sInitStateMsgHandler));    break;
    case FDR_WAIT_CONNECT_STATE:        fnDispatchMsgHandler(pMsg,
                                                sWaitConnectStateMsgHandler,
                                                ARRAY_LENGTH(sWaitConnectStateMsgHandler)); break;
    case FDR_IDLE_STATE:                fnDispatchMsgHandler(pMsg,
                                                sIdleStateMsgHandler,
                                                ARRAY_LENGTH(sIdleStateMsgHandler));    break;
    case FDR_STARTING_STATE:            fnDispatchMsgHandler(pMsg,
                                                sStartingStateMsgHandler,
                                                ARRAY_LENGTH(sStartingStateMsgHandler));    break;
    case FDR_RUNNING_STATE:             fnDispatchMsgHandler(pMsg,
                                                sRunningStateMsgHandler,
                                                ARRAY_LENGTH(sRunningStateMsgHandler)); break;
    case FDR_STOPPING_STATE:            fnDispatchMsgHandler(pMsg,
                                                sStoppingStateMsgHandler,
                                                ARRAY_LENGTH(sStoppingStateMsgHandler));    break;
    case FDR_INACTIVE_STATE:            fnDispatchMsgHandler(pMsg,
                                                sInactiveStateMsgHandler,
                                                ARRAY_LENGTH(sInactiveStateMsgHandler));    break;
    case FDR_SLEEP_STATE:               fnDispatchMsgHandler(pMsg,
                                                sSleepStateMsgHandler,
                                                ARRAY_LENGTH(sSleepStateMsgHandler));   break;
    case FDR_INIT_ERROR_STATE:          fnDispatchMsgHandler(pMsg,
                                                sInitErrorStateMsgHandler,
                                                ARRAY_LENGTH(sInitErrorStateMsgHandler));   break;
    case FDR_FIRMWARE_UPDATE_STATE:     fnDispatchMsgHandler(pMsg,
                                                sFirmwareUpdateStateMsgHandler,
                                                ARRAY_LENGTH(sFirmwareUpdateStateMsgHandler));  break;
    case FDR_DIAG_STATE:                fnDispatchMsgHandler(pMsg,
                                                sDiagStateMsgHandler,
                                                ARRAY_LENGTH(sDiagStateMsgHandler));    break;
    case FDR_DETECT_BOOT_MODE_DEV_STATE:    fnDispatchMsgHandler(pMsg,
                                                sDetectBootModeDevStateMsgHandler,
                                                ARRAY_LENGTH(sDetectBootModeDevStateMsgHandler));   break;
    case FDR_DIFF_WEIGH_WAIT_PIECE_STATE:   fnDispatchMsgHandler(pMsg,
                                                sDWMWaitPieceStateMsgHandler,
                                                ARRAY_LENGTH(sDWMWaitPieceStateMsgHandler));    break;



    case FDR_PAUSING_STATE:             fnDispatchMsgHandler(pMsg,
                                                sPausingStateMsgHandler,
                                                ARRAY_LENGTH(sPausingStateMsgHandler)); break;

    case FDR_PAUSED_STATE:              fnDispatchMsgHandler(pMsg,
                                                sPausedStateMsgHandler,
                                                ARRAY_LENGTH(sPausedStateMsgHandler));  break;


    default:
        break;
                                                
#if 0
    case FDR_MCP_DISCONNECTED_STATE:    fnMcpDisconnectedStateProcessMsg(pMsg); break;  
    case FDR_INIT_ERROR_STATE:          fnInitErrorStateProcessMsg(pMsg); break;
    case FDR_IDLE_STATE:                fnIdleStateProcessMsg(pMsg);    break;
    case FDR_STARTING_STATE:            fnStartingStateProcessMsg(pMsg); break;
    case FDR_STOPPING_STATE:            fnStoppingStateProcessMsg(pMsg); break;
    case FDR_RUNNING_STATE:             fnRunningStateProcessMsg(pMsg); break;
    case FDR_PAUSING_STATE:             fnPausingStateProcessMsg(pMsg); break;
    case FDR_pD_STATE:              fnPausedStateProcessMsg(pMsg); break;
    case FDR_DIFF_WEIGH_WAIT_PIECE_STATE: fnDiffWeighWaitPieceStateProcessMsg(pMsg); break;
    case FDR_FIRMWARE_UPDATE_STATE:     fnFirmwareUpdateStateProcessMsg(pMsg); break;
    case FDR_FIRMWARE_UPDATE_ERROR_STATE: fnFirmwareUpdateErrorStateProcessMsg(pMsg); break;
    case FDR_REINITIALIZE_STATE:        fnReinitializingStateProcessMsg(pMsg); break;
    case FDR_REINIT_ERROR_STATE:        fnReinitErrorStateProcessMsg(pMsg); break;
    case FDR_SLEEP_STATE:               fnSleepStateProcessing(pMsg); break;
    default:
        throwFeederException("Invalid Task State.");
#endif
    };  
}


/**********************************************************************
fnProcessInitReq
This function is called upon receiving an intertask message requesting
the feeder to initialize

Scope
Private

Prototype
static void fnProcessInitReq(const INTERTASK *pMsg);

Pre-Conditions
State = FDR_POWERUP_STATE
pMsg != NULL

Inputs
pMsg - Pointer to an Intertask Message.

Processing

  IF Feeder Hardware Present Signal = False THEN
    SEND InitRsp message
    SET State = FDR_INACTIVE_STATE
  ELSEIF Feeder MCP has enumerated THEN
    SET State = FDR_INITIALIZING_STATE
    CALL fnStartInitialization()
  ELSE
    SET State = FDR_WAIT_CONNECT_STATE
    SET ConnectTimeout Timer
  ENDIF
    

**********************************************************************/
static void fnProcessInitReq(const INTERTASK *pMsg)
{
    // NULL is a possible value when we are calling this routine because of 
    //  waking up from sleep, so don't assert and return anymore
    /*
    FDRTASK_ASSERT(pMsg!=NULL); //lint !e506
    if (pMsg==NULL)
        return;
    */

    // set flag so that a response is sent
    fSendInitRsp = TRUE;

    if (!fnIsFeederHardwarePresent())
    {       
        // inform CM that there is no Feeder
        fnSendInitRsp(FDR_MISC_ERR,MCP_FPFDR_NOT_PRESENT_ERR);
        // goto inactive state
        fnSetState(FDR_INACTIVE_STATE);
    }
#ifdef MIDJET_RIG_TESTING
    else if (fnMCPIsDevPresent(MIDJET_WOW_MCP_ID))
//#else
//    else if (( (fnGetMeterModel()==DM400C) && fnMCPIsDevPresent(FDR_MCP_ID) ) ||
//                ((fnGetMeterModel()==DM475C) && fnMCPIsDevPresent(DM475C_MCP_ID)) )
#endif
    {       
        // kick off the initialization
        fnSetState(FDR_START_INIT_STATE);
        fnStartInitialization();
    }
/*    else
    {
        // wait for the feeder to connect
        fnSetState(FDR_WAIT_CONNECT_STATE);
        (void)OSStartTimer(FP_FEEDER_ENUMERATE_TIMER);
    }*/
}

/**********************************************************************
fnStartInitialization
This function starts the feeder initialization process.

Scope
Private

Prototype
static void fnStartInitialization(void);

Pre-Conditions
State = INITIALIZING

Processing

START Downloading Profiles
IF Profile Download Couldn't be Started THEN
  Stop the Profile Download
  SET State = FDR_INACTIVE_STATE
  SEND InitRsp message
ELSE
  SET ProfileDownloadTimeout Timer
ENDIF

**********************************************************************/
static void fnStartInitialization(void)
{   
    // Start initializing
    unsigned long lwStatus = MCP_LIB_SUCCESS;
    UINT8 majorHwVer, minorHwVer;

    fnGetHardwareVersion(&majorHwVer, &minorHwVer);
    
    FDRTASK_ASSERT(lwFeederTaskState==FDR_START_INIT_STATE);    //lint !e506

    // Turn on feeder VM (motor power) control
    //TODO - move into HAL and delete here
#if 0
    if ( (majorHwVer == FP_MAIN_BD_HW_VERSION1) && (minorHwVer == FP_HW_VERSION_DE1_VAL) )
        FDR_VM_CONTROL_PORT |= FDR_VM_CONTROL_BIT;
    else
        FDR_VM_CONTROL_PORT &= ~FDR_VM_CONTROL_BIT;     // For DE2 the logic was reversed for some unknown reason.
#endif
    HALFeederSet27Volts(TRUE);

    lwStatus = fnMCPInitialize(pFeederMcp,fnFeederInitCompleteCb);
    if (lwStatus != MCP_LIB_SUCCESS)
    {
        // goto inactive state
        fnSetState(FDR_INACTIVE_STATE);
        // inform CM that the init failed
        fnSendInitRsp(FDR_MISC_ERR,(unsigned char)lwStatus);                    
    }

#ifdef RD_DM400_PRINTDBG
    // Send an OK init response
    fnSendInitRsp(0,0);
#endif

}

static void fnFeederInitCompleteCb(const void *pMcp, const unsigned long lwStatus)
{
// No profiles to download so do nothing
}

/**********************************************************************
fnProfileDnldCompleteCB
This callback function is called by the MCP Module after the MCP Module
has finished downloading profiles.
The profiles may have been downloaded successfully or 
unsuccessfully depending on the status passed back.

Scope
Private

Prototype
static void fnProfileDnldCompleteCB(const unsigned long lwInitStatus);

Inputs
lwStatus - Profile Download Status.  Valid Values are:
0 - Success
Other - Error

Processing

CALL fnProcessEvent(MCP_INIT_COMPLETE_EVENT, &lwInitStatus)

**********************************************************************/
static void fnProfileDnldCompleteCB(const void *pMcp, const unsigned long lwStatus)
{
    if (lwStatus == MCP_LIB_SUCCESS)
    {
        // goto the wait for sensor state
        fnSetState(FDR_SENSORS_INIT_STATE);
        // request the sensors status
        fnMCPSendSimpleRequest(pFeederMcp,
                                MMC_QUERY_SENSORS_REQ,
                                MMC_SENSOR_STATE_RSP,
                                fnInitQuerySensorRspCb);
    }
    else
    {
        // goto inactive state
        fnSetState(FDR_INACTIVE_STATE);
        // inform CM that the init failed
        fnSendInitRsp(FDR_MISC_ERR,MCP_FFS_PROFILE_NOT_LOADED);
    }
}


/**********************************************************************
fnProcessConnect
This function handles an MCP_CONNECT event when the Feeder Task is
waiting for the MCP to enumerate.

Scope
Private

Prototype
static void fnProcessConnect(const INTERTASK *pMsg);

Pre-Conditions
State = FDR_WAIT_CONNECT_STATE
pMsg!= NULL

Inputs
pMsg - Pointer to an Intertask Message.

Processing

SET State = INITIALIZING
CALL fnStartInitialization()

**********************************************************************/
static void fnProcessConnect(const INTERTASK *pMsg)
{

    FDRTASK_ASSERT(lwFeederTaskState==FDR_WAIT_CONNECT_STATE || lwFeederTaskState==FDR_DETECT_BOOT_MODE_DEV_STATE); //lint !e506
    FDRTASK_ASSERT(pMsg!=NULL);                                 //lint !e506

    if (pMsg==NULL)
        return; 

    if (lwFeederTaskState==FDR_WAIT_CONNECT_STATE)
    {
        // kick off the initialization
        fnSetState(FDR_START_INIT_STATE);
        fnStartInitialization();        
    }
}

void fnFeederStopTimerExpire(unsigned long lwTimerID)
{
    (void)OSStopTimer(FP_FEEDER_STOP_TIMER);
    (void)fnSendIntertask(FDRMCPTASK,FDRMCPTASK,FDR_STOP_TIMEOUT,NO_DATA,NULL,0);
}
void fnFeederStartTimerExpire(unsigned long lwTimerID)
{
    (void)OSStopTimer(FP_FEEDER_START_TIMER);
    (void)fnSendIntertask(FDRMCPTASK,FDRMCPTASK,FDR_START_TIMEOUT,NO_DATA,NULL,0);  
}

/**********************************************************************
fnInitQuerySensorRspCb
This function is called when the responds to a sensor query during
initialization.  The response is in the form of a MMC_SENSOR_STATE_RSP
motion class message.

Scope
Private

Prototype
static void fnInitQuerySensorRspCb(const void *pMcp, 
                            const unsigned long lwStatus, 
                            const void *pData, 
                            const unsigned long lwDataLength);

Inputs
pMcp - Pointer to the instance of the MCP that has sent the message.
lwStatus - Status of the message.  Indicates if the message has
    been received succesfully or not.
pData - Pointer to the Motion Class Message.
lwDataLength - Length of the Motion Class Message.

Processing

IF the response has been succesfully received THEN
  SET State = Idle
  SEND the Init Rsp (success)
ELSE
  SET State = Inactive
  SEND the Init Rsp (error)
END IF

**********************************************************************/
static void fnInitQuerySensorRspCb(const void *pMcp, const unsigned long lwStatus, const void *pData, const unsigned long lwDataLength)
{
    unsigned long lwFdrMcpFirmwareQueryStatus = MCP_LIB_SUCCESS;
    unsigned long lwState = 0;
    unsigned char bStartRspStatus = FDR_OK;
    char  dbgBuf[SYSLOG_MAX_DATA];

    FDRTASK_ASSERT(lwStatus == MCP_LIB_SUCCESS);    //lint !e506
    if (lwStatus == MCP_LIB_SUCCESS)
    {
        FDRTASK_ASSERT(pData && lwDataLength > MMC_MSG_HEADER_LEN); //lint !e506

        if (pData && lwDataLength > MMC_MSG_HEADER_LEN)
        {
            unsigned char *pPtr = (unsigned char *)pData;

            // store the current sensor values
            (void)memcpy((void*)&lwFeederSensorState,&pPtr[MMC_MSG_HEADER_LEN],sizeof(lwFeederSensorState));    //lint !e718 !e746
            // DM400c
            lwFeederSensorState &= FDR_SENSOR_MASK; // filter out some bits, like the floating S1 sensor bit
            lwLastFeederSensorState = lwFeederSensorState;
        }

        // get current state
        lwState = fnGetState();
        FDRTASK_ASSERT(lwState == FDR_SENSORS_INIT_STATE);  //lint !e506
        if (lwState == FDR_SENSORS_INIT_STATE)
        {
        }
        else
        {
            fnSendInitRsp(FDR_MISC_ERR,MCP_FPFDR_INVALID_OP_IN_STATE);
        }       
    }
    else
    {
        // goto inactive state
        fnSetState(FDR_INACTIVE_STATE);
        // inform CM that the init failed
        fnSendInitRsp(FDR_MISC_ERR,MCP_FFS_PROFILE_NOT_LOADED);
    }
}

/**********************************************************************
fnProcessDisconnect
This function handles an MCP_DISCONNECT event.

Scope
Private

Prototype
static void fnProcessDisconnect(const INTERTASK *pMsg);

Pre-Conditions
pMsg!=NULL

Inputs
pMsg - Pointer to an Intertask Message.

Processing

RESET the Feeder MCP Instance
SET State = FDR_INACTIVE_STATE
IF InitRsp Must Be Sent THEN
  CALL fnSendInitRsp
ENDIF

**********************************************************************/
static void fnProcessDisconnect(const INTERTASK *pMsg)
{
    unsigned long lwState = 0;
    
    FDRTASK_ASSERT(pMsg!=NULL); //lint !e506

    if (pMsg==NULL)
        return; 

    // DSD 05-11-2007 Modified the handling of the disconnect message.
    //      The addition of Abacus, USB LAN, ect has changed the timing
    //      so that the MCP_DISCONNECTED message arrives after
    //      the feeder task has transitioned to the init state.
    //      When this happens, just transition to the wait for connection
    //      state and set the enumeration timeout timer.

#ifdef RD_DM400_PRINTDBG
    // for debug purposes avoid the MCP handling issues.
    fnSetState(FDR_IDLE_STATE);
#endif

    lwState = fnGetState();
    if (lwState != FDR_START_INIT_STATE)
    {
        // reset the mcp instance
        (void)fnMCPLibReset(pFeederMcp);
        // goto inactive state
        fnSetState(FDR_INACTIVE_STATE);
        if (fSendInitRsp)
        {
            // inform CM that there is no Feeder
            fnSendInitRsp(FDR_MISC_ERR,0xFE);
        }
    }
    else
    {
        // During initialization, the feeder disconnected so we wait for it to
        // re-connect.
        fRecoverFromDisconnectDuringInit = TRUE;
        (void)fnMCPLibReset(pFeederMcp);            // reset the mcp instance
        fnSetState(FDR_WAIT_CONNECT_STATE);
    }
}

/**********************************************************************
fnSendInitRsp
This function sends an Init Response Message if one should be sent.

Scope
Private

Prototype
static void fnSendInitRsp(const unsigned char bStatus);

Inputs
bStatus - Status of the initialization.
0 - Success
Other - Error

Processing

IF InitRsp message requested THEN
  SEND FC_INIT_RSP(lwStatus)
ENDIF

**********************************************************************/
static void fnSendInitRsp(const unsigned char bStatus, const unsigned char bErrorCode)
{
    uchar               rsp[2] = { 0 };

    FDRTASK_ASSERT(fSendInitRsp);   //lint !e506

    if (fSendInitRsp)
    {
        if (bStatus != FDR_OK || bErrorCode != FDR_OK)
        {
            bLastFdrMeterErrCode = bErrorCode;
            fnLogSystemError(ERROR_CLASS_FP_FEEDER, 0xF0, bStatus, bErrorCode, 0);
            (void)sprintf(bFdrDbgMsgBuff,">FDR MERR(%02X %02X %02X %02X) %d<", 0xF0, bStatus,bErrorCode,0,__LINE__);
            fnFdrLogMsg(bFdrDbgMsgBuff);
        }

        rsp[0] = bStatus;
        rsp[1] = bErrorCode;        
#ifdef DM400C_TEST_FEEDER_LOAD_FIRMWARE_FIRST_TIME
        rsp[0] = FDR_MISC_ERR;
        rsp[1] = MCP_FPFDR_ENUM_TIMEOUT;
        bLastFdrMeterErrCode = MCP_FPFDR_ENUM_TIMEOUT;
#endif
        (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_INIT_RSP, BYTE_DATA, rsp, sizeof(rsp));
        fSendInitRsp = FALSE;
    }
}

/**********************************************************************
fnIdleStateValidateNewMode
This function checks to see if a change to the new mode is allowed
given the current state.

Scope
Private

Prototype
static BOOL fnIdleStateValidateNewMode(const unsigned char bNewMode);

Inputs
bNewMode - The new mode that is being requested.

Processing

IF In the current state a change to the new mode is valid THEN
  ACCEPT the mode change request
ENDIF

**********************************************************************/
static BOOL fnIdleStateValidateNewMode(const unsigned char bNewMode)
{
    BOOL fNewModeOk = FALSE;

    if (bNewMode == NORMAL_MODE || bNewMode == SLEEPING || 
        bNewMode == DISABLE_MODE || bNewMode == DIAG_MODE ||
        bNewMode == CM_DIFF_WEIGH_MODE)
        fNewModeOk = TRUE;

    return fNewModeOk;
}
/**********************************************************************
fnIdleStateGotoNewMode
This function performs actions when changing to a new mode.

Scope
Private

Prototype
static void fnIdleStateGotoNewMode(const unsigned char bNewMode);

Inputs
bNewMode - The new mode that is being entered.

**********************************************************************/
static void fnIdleStateGotoNewMode(const unsigned char bNewMode)
{
    if (bNewMode == SLEEPING)
    {
        const unsigned char bMcpSleepSensorMsg = 0x01;

        // DSD 01-17-2007
        // reset the mcp so that it will properly send the sleep
        // request to the feeder.
        (void)fnMCPLibReset(pFeederMcp);

        //TODO - confirm proper way to idle feeder
#if 1
        // turn off 5 volts to feeder
            fnSystemLogEntry(SYSLOG_TEXT, "5 Volts OFF", 11);
            //TODO - move into HAL and delete here
//            FDR_5V_CONTROL |= FDR_5V_CONTROL_BIT;
            HALFeederSetPower(FALSE);
#else
            // send message to feeder to go to sleep
            fnMCPSendRequest(pFeederMcp,
                            &bMcpSleepSensorMsg,
                            sizeof(bMcpSleepSensorMsg),
                            MMC_SLEEP_MODE_REQ,MMC_NO_RESPONSE,NULL);
#endif

        // goto sleep state
        fnSetState(FDR_SLEEP_STATE);
    }   
    else if (bNewMode == DIAG_MODE)
    {
        // set the unsolicited msg callback
        fnMCPSetUnsolicitedMsgCb(pFeederMcp, fnDiagStateMsgHandler);
        // goto diag state
        fnSetState(FDR_DIAG_STATE);
    }
    else if (bNewMode == CM_DIFF_WEIGH_MODE)
    {
        fnEnterDiffWeighMode();
    }
}
/**********************************************************************
fnSleepStateValidateNewMode
This function checks to see if a change to the new mode is allowed
given the current state.

Scope
Private

Prototype
static BOOL fnSleepStateValidateNewMode(const unsigned char bNewMode);

Inputs
bNewMode - The new mode that is being requested.

Processing

IF In the current state a change to the new mode is valid THEN
  ACCEPT the mode change request
ENDIF

**********************************************************************/
static BOOL fnSleepStateValidateNewMode(const unsigned char bNewMode)
{
    BOOL fNewModeOk = FALSE;

    if (bNewMode == NORMAL_MODE || bNewMode == DISABLE_MODE)
        fNewModeOk = TRUE;

    return fNewModeOk;
}

/**********************************************************************
fnSleepStateGotoNewMode
This function performs actions when changing to a new mode.

Scope
Private

Prototype
static void fnSleepStateGotoNewMode(const unsigned char bNewMode);

Inputs
bNewMode - The new mode that is being entered.

Caveats

1) The Feeder Task is assuming that the system does NOT transition
from the sleep mode directly to a running mode.  Thus, this function
causes the Feeder Task to enter the idle state.

**********************************************************************/
static void fnSleepStateGotoNewMode(const unsigned char bNewMode)
{
    const unsigned char bMcpWakeupFromSleepMsg = 0x00;  // DSD 01-17-2007 Fix copy & paste error

    //TODO - confirm proper way to wake feeder
#if 1
        // turn on 5 volts to feeder
        fnSystemLogEntry(SYSLOG_TEXT, "5 Volts ON ", 11);
        //TODO - move into HAL and delete here
//        FDR_5V_CONTROL &= ~FDR_5V_CONTROL_BIT;
        HALFeederSetPower(TRUE);


        // this will keep the mode change response message from going back to CM immediately.  this allows
        //  us to finish bringing up the MCP before sending this message, which is necessary because otherwise
        //  we end up on the main screen without really being ready to run and the feeder will hang if the
        //  user presses start too soon.
        fWakeupModeChangeToCMPending = true;

        // goto sleep state
        fnSetState(FDR_POWERUP_STATE);

        // fake an init request
        fnProcessInitReq(0);
#else
        // send message to feeder to go to sleep
        fnMCPSendRequest(pFeederMcp,
                        &bMcpWakeupFromSleepMsg,
                        sizeof(bMcpWakeupFromSleepMsg),
                        MMC_SLEEP_MODE_REQ,MMC_NO_RESPONSE,NULL);

        fnSetState(FDR_IDLE_STATE);
#endif
}

static BOOL fnInactiveStateValidateNewMode(const unsigned char bNewMode)
{
    BOOL fNewModeOk = FALSE;

    if (bNewMode == NORMAL_MODE || bNewMode == SLEEPING || bNewMode == DISABLE_MODE)
        fNewModeOk = TRUE;

    return fNewModeOk;  
}
static BOOL fnDiagStateValidateNewMode(const unsigned char bNewMode)
{
    BOOL fNewModeOk = FALSE;
    if (bNewMode == NORMAL_MODE || bNewMode == SLEEPING || bNewMode == DISABLE_MODE)
        fNewModeOk = TRUE;
    return fNewModeOk;
}
static void fnDiagStateGotoNewMode(const unsigned char bNewMode)
{
    fnSetState(FDR_IDLE_STATE);
}
static BOOL fnDMWWaitPieceStateValidateNewMode(const unsigned char bNewMode)
{
    BOOL fNewModeOk = FALSE;

    if (bNewMode == NORMAL_MODE || bNewMode == DISABLE_MODE || bNewMode == SLEEPING)
        fNewModeOk = TRUE;

    return fNewModeOk;
}
static void fnDMWWaitPieceStateGotoNewMode(const unsigned char bNewMode)
{
    fnExitDiffWeighMode();

    if (bNewMode == SLEEPING)
    {
        const unsigned char bMcpSleepSensorMsg = 0x01;

        //TODO - confirm proper way to idle feeder
#if 1
        // turn off 5 volts to feeder
        fnSystemLogEntry(SYSLOG_TEXT, "5 Volts OFF", 11);
        //TODO - move into HAL and delete here
//        FDR_5V_CONTROL |= FDR_5V_CONTROL_BIT;
        HALFeederSetPower(FALSE);
#else
            // send message to feeder to go to sleep
        fnMCPSendRequest(pFeederMcp,
                        &bMcpSleepSensorMsg,
                        sizeof(bMcpSleepSensorMsg),
                        MMC_SLEEP_MODE_REQ,MMC_NO_RESPONSE,NULL);
#endif
        // goto sleep state
        fnSetState(FDR_SLEEP_STATE);
    }   
    else if (bNewMode == DIAG_MODE)
    {
        // set the unsolicited msg callback
        fnMCPSetUnsolicitedMsgCb(pFeederMcp, fnDiagStateMsgHandler);
        // goto diag state
        fnSetState(FDR_DIAG_STATE);
    }
    else
    {
        fnSetState(FDR_IDLE_STATE);
    }
}

/**********************************************************************
fnProcessSetModeReq
This function is called in response to a set mode request.

Scope
Private

Prototype
static void fnProcessSetModeReq(const INTERTASK *pMsg);

Pre-Conditions
pMsg != NULL

Inputs
pMsg - Pointer to an Intertask Message.

Processing

IF requested Mode != current Mode THEN
  IF entry to requested Mode is valid THEN
    enter new mode
  ENDIF
ENDIF

**********************************************************************/
static void fnProcessSetModeReq(const INTERTASK *pMsg)
{
    unsigned char *pPtr = NULL;
    unsigned long lwLength = 0;
    unsigned long lwState = FDR_IDLE_STATE;
    unsigned char bSetModeStatus = MCP_FPFDR_INVALID_REQUEST;
    BOOL fModeValid = FALSE;
    emOpMode emNewMode = eSysOpMode;    

    FDRTASK_ASSERT(pMsg!=NULL);     //lint !e506

    GetIntertaskPointerAndLength((void**)&pPtr,&lwLength,pMsg); //lint !e605
    FDRTASK_ASSERT(pPtr);                                       //lint !e506

    if (pPtr)
    {
        fSendModeChangeRsp = TRUE;
        emNewMode = pPtr[POS_0];
        if (emNewMode == eSysOpMode)
        {
            fModeValid = TRUE;
        }
        else
        {
            // validate mode
            lwState = fnGetState();
            switch (lwState) {
            case FDR_IDLE_STATE:            
                fModeValid = fnIdleStateValidateNewMode(pPtr[POS_0]);
                break;
            case FDR_SLEEP_STATE:
                fModeValid = fnSleepStateValidateNewMode(pPtr[POS_0]);
                break;
            case FDR_INACTIVE_STATE:
                fModeValid = fnInactiveStateValidateNewMode(pPtr[POS_0]);
                break;
            case FDR_DIAG_STATE:
                fModeValid = fnDiagStateValidateNewMode(pPtr[POS_0]);
                break;
            case FDR_DIFF_WEIGH_WAIT_PIECE_STATE:
                fModeValid = fnDMWWaitPieceStateValidateNewMode(pPtr[POS_0]);
                break;
            default:
                break;
            };

            // change modes if the mode change is valid
            if (fModeValid)
            {
                // set the unsolicited msg callback
                fnMCPSetUnsolicitedMsgCb(pFeederMcp, NULL);

                switch (lwState) {
                case FDR_IDLE_STATE:                
                    fnIdleStateGotoNewMode(pPtr[POS_0]);
                    break;
                case FDR_SLEEP_STATE:
                    fnSleepStateGotoNewMode(pPtr[POS_0]);
                    break;
                case FDR_INACTIVE_STATE:                    
                    break;
                case FDR_DIAG_STATE:
                    fnDiagStateGotoNewMode(pPtr[POS_0]);
                    break;
                case FDR_DIFF_WEIGH_WAIT_PIECE_STATE:
                    fnDMWWaitPieceStateGotoNewMode(pPtr[POS_0]);
                    break;
                default:
                    break;
                };
            }
        }
    }
    
    if (fModeValid)
    {
        bSetModeStatus = MCP_NO_ERROR;  
        eSysOpMode = emNewMode;
    }

    // if this flag is set we need to wait until the MCP is finished initializing before sending the mode
    //  change response.
    if (fWakeupModeChangeToCMPending != true)
        fnSendSetModeRsp(bSetModeStatus);   
}

/**********************************************************************
fnSendSetModeRsp
This function sends the Set Mode Rsp message.

Scope
Private

Prototype
static void fnSendSetModeRsp(const unsigned char bStatus);

Pre-Conditions
None.

Inputs
bStatus - Status of the Set Mode operation that is sent in the 
    Set Mode Rsp message.

**********************************************************************/
static void fnSendSetModeRsp(const unsigned char bStatus)
{
    uchar               rsp[4] = { 0 };

    rsp[0] = (unsigned char)eSysOpMode;
    rsp[1] = bStatus == MCP_NO_ERROR ? FDR_OK : FDR_MISC_ERR;
    rsp[2] = bStatus;

    if (fSendModeChangeRsp)
    {
        fSendModeChangeRsp = FALSE;

        if (bStatus != MCP_NO_ERROR)
        {
            bLastFdrMeterErrCode = bStatus;
            fnLogSystemError(ERROR_CLASS_FP_FEEDER, 0xF1, FDR_MISC_ERR, bStatus, 0);
            (void)sprintf(bFdrDbgMsgBuff,">FDR MERR(%02X %02X %02X %02X) %d<", 0xF1, FDR_MISC_ERR, bStatus, 0,__LINE__);
            fnFdrLogMsg(bFdrDbgMsgBuff);
        }
        
        (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_SET_MODE_RSP, BYTE_DATA, rsp, sizeof(rsp));  
    }
}

/**********************************************************************
fnProcessStartReq
This function is called in response to a start feeder request.

Scope
Private

Prototype
static void fnProcessStartReq(const INTERTASK *pMsg);

Pre-Conditions
State = FDR_START_INIT_STATE
pMsg != NULL

Inputs
pMsg - Pointer to an Intertask Message.

Processing

IF State is IDLE THEN
  CALL fnStartFeeder
ELSE
  SEND Run Rsp Msg (error)
END IF

**********************************************************************/
static void fnProcessStartReq(const INTERTASK *pMsg)
{
    unsigned char *pPtr = NULL;
    unsigned long lwLength = 0;
    unsigned long lwState = 0;

    FDRTASK_ASSERT(pMsg!=NULL);                         //lint !e506
    FDRTASK_ASSERT(lwFeederTaskState==FDR_IDLE_STATE||lwFeederTaskState==FDR_PAUSED_STATE||lwFeederTaskState==FDR_DIFF_WEIGH_WAIT_PIECE_STATE); //lint !e506

    if (pMsg == NULL)
        return;

    fSendStartRsp = TRUE;

    lwState = fnGetState();
    if (lwState == FDR_IDLE_STATE || lwState == FDR_PAUSED_STATE || lwState == FDR_DIFF_WEIGH_WAIT_PIECE_STATE)
    {
        GetIntertaskPointerAndLength((void**)&pPtr,&lwLength,pMsg); //lint !e605

        FDRTASK_ASSERT(pPtr);   //lint !e506

        if (pPtr)
        {
            FDRTASK_ASSERT(eThroughput==pPtr[POS_1]);   //lint !e506    

            eThroughput = pPtr[POS_1];
            bTotalPieceCount = pPtr[POS_0];
            fMailRunInitiated = TRUE;

            fnStartFeeder();
        }
        else
        {
            fnSendStartRsp(MCP_FPFDR_INVALID_REQUEST);
        }
    }
    else
    {
        // the feeder can NOT be started in this state.
        fnSendStartRsp(MCP_FPFDR_INVALID_OP_IN_STATE);      
    }   
}

static void fnFeederResetMCP(void)
{
        // reset stop flag
        fFullStop = FALSE;
        fNormalPause = FALSE;

        // reset jam info
        bLastJamCode = 0;

        // reset timer info
        fnResetMailFlowTimers();

        // cancel currently running profiles
        fnMCPCancelGroup(pFeederMcp,MMC_ENVELOPE_PROFILE_GROUP);

        // reset the profile flags
        fnMCPResetAllFlags(pFeederMcp);

        // reset the MCP instance
        (void)fnMCPLibReset(pFeederMcp);

        // Clear WOW Dim Rating error flag
        fnSetWOWDimRatingErrorFlag(FALSE);
}

static void fnFeederDownloadDRData(void)
{
    // DM475C doesn't need to download the width break table,
    // just download the weight breaks
    fnMMCIDownloadWeightBreaks();
}

/******************************************************************************
Function Name: fnMMCIDownloadWidthBreaks
Existing: No
Scope: Public
Prototype: void fnMMCIDownloadWidthBreaks (void);
Pre-Conditions: None
Inputs: None
Return Value: None
Outputs: None.
Processing:

CALL fnRateFetchDimensionBreaks(��, &MMCWidthBrkTbl, ��)
SET WidthBrkMsgIndex = 0
SET NumWithBrksInMsg = 0
SET WidthBrk Array = [0, 0, �� , 0]
SET bCurrWidthBrkTbl Array = [0, 0, �� , 0]
SET bCurrNumWidthBrkTblEntries = 0
FOR Each Break In MMCWidthBrkTbl
  SET SensorIndex = Index of last sensor in sMJWowWidthTable whose distance is less than or
    equal to the current entry in MMCWidthBrkTbl
  SET WidthBrk[WidthBrkMsgIndex] = SensorIndex
  INCREMENT WidthBrkMsgIndex
END FOR
SORT WidthBrk Array In ASCENDING ORDER.
FORMAT Download Width Break Table MSG - WidthBrk Array is the data for this message
SET bCurrWidthBrkTbl = WidthBrk
SET bCurrNumWidthBrkTblEntries = WidthBrkMsgIndex
SEND Width Break Table MSG to WOW MCP
******************************************************************************/
static void fnMMCIDownloadWidthBreaks (void)
{
    UINT8   *pWidthBrkData;
    UINT8   ucConvWidth;
    UINT16  usWidthBrksToDownload;
    UINT8   ucWidthBrkEntryNum;
    UINT8   ucWidthBrkPkt;
    UINT8   ucNumWidthBrksInPkt, i;
    
//    fnRateFetchDimensionBreaks(&stDimBreaksInfo);

/*
    if (fnGetWOWClassChangedStatus() == TRUE && stDimBreaksInfo.stWidthBreaks.usCount > 0)
    {
        ucWidthBrkEntryNum = 0;
        ucWidthBrkPkt = 0;
        ucNumWidthBrksInPkt = 0;
        usWidthBrksToDownload = stDimBreaksInfo.stWidthBreaks.usCount;

        // set up the width break data
        (void)memset((void*)ucWidthBrkBuff,0,sizeof(ucWidthBrkBuff));
        pWidthBrkData = ucWidthBrkBuff + MMC_DNLD_WIDTHBRK_DATA_OFFSET;
        ucNumWidthBrksInPkt = 0;
        if (usWidthBrksToDownload > MMC_MAX_NUM_WIDTHBRKS_IN_PACKET)
        {
            ucNumWidthBrksInPkt = MMC_MAX_NUM_WIDTHBRKS_IN_PACKET;
        }
        else
        {
            ucNumWidthBrksInPkt = (UINT8)usWidthBrksToDownload;
        }
        
        i = 0;
        while (i < ucNumWidthBrksInPkt)
        {
            ucConvWidth = MMCMJGetWidthSensorIndexForMM(stDimBreaksInfo.stWidthBreaks.usBreaks[ucWidthBrkEntryNum++]);
            *pWidthBrkData++ = ucConvWidth;
            i++;
        }

        // populate the info part of the data
        ucWidthBrkBuff[MMC_DNLD_WIDTHBRK_NUM_IN_PKT_OFFSET] = ucNumWidthBrksInPkt;
        
        // send the message to the MCP
        fnMCPSendRequest(pFeederMcp, 
                            ucWidthBrkBuff,
                            MMC_DNLD_WIDTHBRK_DATA_OFFSET + (ucNumWidthBrksInPkt * sizeof(UINT8)),
                            MMC_DOWNLOAD_WIDTH_BREAK_TBL_REQ,
                            MMC_NO_RESPONSE,
                            NULL);
    }
*/
}

/******************************************************************************
 * Name
 *   fnMMCIDownloadWeightBreaks
 *
 * Description
 *   This function handles the download of the weight break table to the Midjet WOW module.
 *
 * Inputs
 *   None.
 *
 * Returns
 *   None.
 *
 * Caveats
 *   This function checks to see if the rates have been changed requiring an update
 *   of the weight break table and if they have, it downloads the information.
 *
 * Revision History
 *   08/09/2004 Cheryl M. Tokarski - Initial version.
 *   DSD 01/15/2008 - Fix a problem where the MMCI task would hang when more than 255 weight breaks are downloaded.
 *
******************************************************************************/
static void fnMMCIDownloadWeightBreaks(void)
{
    UINT8   *pWgtBrkData;
    UINT32  ulConvWeight;
    UINT16  usWgtBrksToDownload;
    UINT16  usWgtBrkEntryNum;    // DSD 01/15/2008
    UINT8   ucWgtBrkPkt;
    UINT8   ucNumWgtBrksInPkt, i;
    struct wow_table *pWOWTable;
    

/*
    if ((fnGetWOWClassChangedStatus() == TRUE) &&
//            (fnRateGetWOWWeightTable(&pWOWTable) == RSSTS_NO_ERROR) &&
            (pWOWTable->count > 0))
    {
        usWgtBrkEntryNum = 0;
        ucWgtBrkPkt = 0;
        ucNumWgtBrksInPkt = 0;
        usWgtBrksToDownload = pWOWTable->count;

        while (usWgtBrkEntryNum < pWOWTable->count)  // DSD 01/15/2008
        {
            // set up the weight break data
            (void)memset((void*)ucWgtBrkBuff,0,sizeof(ucWgtBrkBuff));
            pWgtBrkData = ucWgtBrkBuff + MMC_DNLD_WGTBRK_DATA_OFFSET;
            ucNumWgtBrksInPkt = 0;
            if (usWgtBrksToDownload > MMC_MAX_NUM_WGTBRKS_IN_PACKET)
            {
                ucNumWgtBrksInPkt = MMC_MAX_NUM_WGTBRKS_IN_PACKET;
            }
            else
            {
                ucNumWgtBrksInPkt = (UINT8)usWgtBrksToDownload;
            }
            i = 0;
            while (i < ucNumWgtBrksInPkt)
            {
                ulConvWeight = fnRatePackedBcdWeightToLong(pWOWTable->Wow_Table[usWgtBrkEntryNum++].weight);
                *pWgtBrkData++ = (UINT8)((ulConvWeight & 0x0000FF00) >> 8); // get hi byte
                *pWgtBrkData++ = (UINT8)(ulConvWeight & 0x000000FF);        // get lo byte
                i++;
            }
            usWgtBrksToDownload -= ucNumWgtBrksInPkt;

            // populate the info part of the data
            if (fnRateGetWeightUnit() == METRIC)
            {
                ucWgtBrkBuff[MMC_DNLD_WGTBRK_INFO_OFFSET] |= MMC_DNLD_WGTBRK_INFO_METRIC_BIT;
            }
            memcpy((void*)&ucWgtBrkBuff[MMC_DNLD_WGTBRK_TOTAL_NUM_OFFSET],&pWOWTable->count,2);
            ucWgtBrkBuff[MMC_DNLD_WGTBRK_SEQ_NUM_OFFSET] = ucWgtBrkPkt++;
            ucWgtBrkBuff[MMC_DNLD_WGTBRK_NUM_IN_PKT_OFFSET] = ucNumWgtBrksInPkt;

            // send the message to the MCP
            fnMCPSendRequest(pFeederMcp, 
                                ucWgtBrkBuff,
                                MMC_DNLD_WGTBRK_DATA_OFFSET + (ucNumWgtBrksInPkt * sizeof(UINT16)),
                                MMC_DOWNLOAD_WEIGH_BREAK_TBL_REQ,
                                MMC_DOWNLOAD_WEIGH_BREAK_TBL_RSP,
                                NULL);
        }
    }
    else
    {
        // Weight break table doesn't change, so we needn't download it again, just start feeder right now.
        fnStartFeeder();
    }
*/
}


/******************************************************************************
Function Name: MMCMJGetWidthSensorIndexForMM
Existing: No
Scope: Public
Prototype: unsigned char MMCMJGetWidthSensorIndexForMM(unsigned short)
Pre-Conditions: None
Inputs: usBreakMM = width break in MM
Return Value: The index of the last sensor before exceeding usBreakMM.
    If equal, next sensor index is returned
    If exceeds table width max, 0 (zero) is returned
Outputs: None.
Processing: Uses data structure sMJWowWidthTable, an array containing the
            distances from the wall for setting width breaks
******************************************************************************/
static UINT8 MMCMJGetWidthSensorIndexForMM(UINT16  usBreakMM)
{
    UINT8   ucSensorIndex=1, currIindex;
    BOOL    fIndexFound = FALSE;

    for (currIindex=1; currIindex < (MJWOW_NUM_WIDTH_SENSORS+1); currIindex++)
    {
        if (usBreakMM >= sMJWowWidthTable[currIindex].wWidthMM)
        {
            ucSensorIndex++;
        }
        else
        {
            fIndexFound = TRUE;
            break;
        }
    }
    if (fIndexFound == FALSE)
    {
        ucSensorIndex = 0;
    }

    return (ucSensorIndex);
}

/**********************************************************************
fnStartFeeder
This function sends the Set Mode Rsp message.

Scope
Private

Prototype
static void fnSendSetModeRsp(const unsigned long lwStatus);

Pre-Conditions
None.

Inputs
lwStatus - Status of the Set Mode operation that is sent in the 
    Set Mode Rsp message.
HISTORY:
 2012.09.24 Clarisa Bellamy - Replace if-else sequence for setting speed flag
                    with a switch statement.  Added one more case, for the new
                    70 LPM speed (SLOW).

**********************************************************************/
static void fnStartFeeder(void)
{
    unsigned long lwState = fnGetState();
    UINT8   ucMode = fnOITGetPrintMode();

    FDRTASK_ASSERT(lwState==FDR_IDLE_STATE||lwState==FDR_PAUSED_STATE||lwState==FDR_DIFF_WEIGH_WAIT_PIECE_STATE);   //lint !e506

    if (lwState == FDR_IDLE_STATE)
    {

        fnFeederResetMCP();

        // set the unsolicited msg callback
        fnMCPSetUnsolicitedMsgCb(pFeederMcp, fnMailRunMsgHandler);

        // change to the starting state
        fnSetState(FDR_STARTING_STATE);
        
        // set counters

        // is piece count non-zero? a zero piece count indicates we feed until empty
        if (bTotalPieceCount != 0)
        {
            // piece count is non-zero - only feed the specified number of pieces
            fSinglePieceMode = TRUE;
            fnMCPSetCounterValue(pFeederMcp,FP_TOTAL_FEED_COUNT,(UINT16)bTotalPieceCount);
            fnMCPSetRemoteFlag(pFeederMcp, MMC_SINGLE_PIECE_MODE_FLAG, MMC_FLAG_SET);
        }
        else
        {
            fSinglePieceMode = FALSE;
        }

        // start the profiles
        if (fnMCPStartProfile(pFeederMcp, MMC_ENVELOPE_PROFILE_GROUP, 0))
        {
            // Use the flags to tell the feeder the non-wow throughput:
            switch( eThroughput )
            {
                case THRUPUT_65:
                    fnMCPSetRemoteFlag( pFeederMcp, MMC_FP_SLOW_THROUGHPUT_FLAG, MMC_FLAG_SET );
                    break;

                case THRUPUT_120:
                    fnMCPSetRemoteFlag( pFeederMcp, MMC_FP_HI_THROUGHPUT_FLAG, MMC_FLAG_SET );
                    break;

                case THRUPUT_90:
                default:
                    // The default is 90, with no flags set.
                    break;
            }

            // set the start flag
            fnMCPSetRemoteFlag(pFeederMcp, MMC_START_MAIL_FLOW_FLAG, MMC_FLAG_SET);
            
            // start the start timer
            (void)OSStartTimer(FP_FEEDER_START_TIMER);  
        }
        else
        {
            // send start rsp
            fnSendStartRsp(MCP_PROFILE_GROUP_NOT_FOUND);
            // goto idle state
            fnSetState(FDR_IDLE_STATE);
        }   
    }
    else if (lwState == FDR_PAUSED_STATE)
    {
        // goto idle state
        fnSetState(FDR_RUNNING_STATE);
        // set the start flag
        fnMCPSetRemoteFlag(pFeederMcp,MMC_PAUSE_MAIL_FLOW_FLAG,MMC_FLAG_CLEAR);
        fnMCPSetRemoteFlag(pFeederMcp, MMC_START_MAIL_FLOW_FLAG, MMC_FLAG_SET);

        // start the start timer
        (void)OSStartTimer(FP_FEEDER_START_TIMER);
    }
    else if (lwState == FDR_DIFF_WEIGH_WAIT_PIECE_STATE)
    {
        fnStartDiffWeighRun();
        // send start rsp
        fnSendStartRsp(MCP_NO_ERROR);

        // start the start timer
        (void)OSStartTimer(FP_FEEDER_START_TIMER);
    }
}

static void fnEnterDiffWeighMode(void)
{
    fnSetState(FDR_DIFF_WEIGH_WAIT_PIECE_STATE);

    // cancel currently running profiles
    fnMCPCancelGroup(pFeederMcp,MMC_ENVELOPE_PROFILE_GROUP);

    // reset the profile flags
    fnMCPResetAllFlags(pFeederMcp);

    // reset the MCP instance
    (void)fnMCPLibReset(pFeederMcp);

    // set the unsolicited msg callback
    fnMCPSetUnsolicitedMsgCb(pFeederMcp, fnMailRunMsgHandler);

    // set counters

    // piece count is non-zero - only feed the specified number of pieces
    fSinglePieceMode = TRUE;
    fnMCPSetCounterValue(pFeederMcp,FP_TOTAL_FEED_COUNT,(UINT16)1);
    fnMCPSetRemoteFlag(pFeederMcp, MMC_SINGLE_PIECE_MODE_FLAG, MMC_FLAG_SET);

    // start the profiles
    if (!fnMCPStartProfile(pFeederMcp, MMC_ENVELOPE_PROFILE_GROUP, 0))
    {
            // send start rsp
        fnSendSetModeRsp(MCP_PROFILE_GROUP_NOT_FOUND);
        // goto idle state
        fnSetState(FDR_IDLE_STATE);
    }
}
static void fnExitDiffWeighMode(void)
{
    // clear the unsolicited msg callback
    fnMCPSetUnsolicitedMsgCb(pFeederMcp, NULL);

    // cancel currently running profiles
    fnMCPCancelGroup(pFeederMcp,MMC_ENVELOPE_PROFILE_GROUP);

    // reset the profile flags
    fnMCPResetAllFlags(pFeederMcp);

    // reset the MCP instance
    (void)fnMCPLibReset(pFeederMcp);
}
static void fnStartDiffWeighRun(void)
{
    UINT8 flags[4];
    
    fnSetState(FDR_STARTING_STATE);

    flags[0] = MMC_PAUSE_MAIL_FLOW_FLAG;
    flags[1] = MMC_FLAG_CLEAR;
    flags[2] = MMC_START_MAIL_FLOW_FLAG;
    flags[3] = MMC_FLAG_SET;
    
    fnMCPSetCounterValue(pFeederMcp,FP_TOTAL_FEED_COUNT,(UINT16)bTotalPieceCount);
    fnMCPSetMultipleRemoteFlags(pFeederMcp, &flags[0], 2);  
}
static void fnDiffWeighRunStopped(void)
{
    fnSetState(FDR_DIFF_WEIGH_WAIT_PIECE_STATE);
    fnSendMailJobComplete(MCP_NO_ERROR,MCP_NO_ERROR);
}

/**********************************************************************
fnSendStartRsp
This function sends the Start Rsp message.

Scope
Private

Prototype
static void fnSendStartRsp(const unsigned char bStatus);

Pre-Conditions
None.

Inputs
lwStatus - Status of the start operation that is sent in the 
    Start Rsp message.

**********************************************************************/
static void fnSendStartRsp(const unsigned char bStatus)
{
    uchar               rsp[4] = { 0 };

    FDRTASK_ASSERT(fSendStartRsp);      //lint !e506

    if (fSendStartRsp)
    {
        fSendStartRsp = FALSE;

        if (bStatus != MCP_NO_ERROR)
        {
            bLastFdrMeterErrCode = bStatus;
            fnLogSystemError(ERROR_CLASS_FP_FEEDER, 0xF2, FDR_MISC_ERR, bStatus, 0);
            (void)sprintf(bFdrDbgMsgBuff,">FDR MERR(%02X %02X %02X %02X) %d<", 0xF2,FDR_MISC_ERR,bStatus,0,__LINE__);
            fnFdrLogMsg(bFdrDbgMsgBuff);
        }

        rsp[0] = bStatus == MCP_NO_ERROR ? 0 : FDR_MISC_ERR;                
        rsp[1] = bStatus;

#ifdef RD_DM400_PRINTDBG
        // force good state for DM400 test print testing
        rsp[0] = 0;
        rsp[1] = 0;
#endif

        (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_RUN_RSP, BYTE_DATA, rsp, sizeof(rsp));
    }
}

/**********************************************************************
fnMailRunMsgHandler
This is the unsolicited message handler during the mail run states.
This function dispatches control to an appropriate handler
function.

Scope
Private

Prototype
static void fnMailRunMsgHandler(const void *pMcp, const void *pData, const unsigned long lwDataLength);

Pre-Conditions
Feeder Task is in one of the mail run states.

Inputs
pMcp - Pointer to an MCP Instance.  Unused - this will always be the Feeder MCP.
pData - Pointer to the Motion Class Message.
lwDataLength - Length of the Motion Class Message.

Processing

IF Motion Class Message is well-formed THEN
  Dispatch control to the handler for the Motion Class Command.
END IF

**********************************************************************/
static void fnMailRunMsgHandler(const void *pMcp, const void *pData, const unsigned long lwDataLength)
{
    if (pData && lwDataLength >= MMC_MSG_COMMAND_OFFSET)
    {
        const unsigned char *pPtr = (const unsigned char *)pData;
        const unsigned char bMsg = pPtr[MMC_MSG_COMMAND_OFFSET];
        unsigned char bId = 0;

        switch (bMsg) {
        case MMC_PROFILE_STATUS_RSP:
            fnMailRunProfileStatusHandler(pPtr[MMC_MSG_HEADER_LEN]);
            break;
        case MMC_ERROR_LOG_DATA_RSP:
            fnMailRunProfileErrorHandler(pMcp,pData,lwDataLength);
            break;
        case MMC_MAIL_PIECE_DATA_RSP:
        case MMC_CSPARK_MAIL_PIECE_DATA_RSP:
            bId = pPtr[MMC_MSG_PAYLOAD_OFFSET];
            //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
            if ( (fnGetMeterModel() == CSD3) && (bId == MMC_FP_FEEDER_MAIL_PIECE_DATA_ID) )
            {
                fnLogMailPieceData((const UINT8 *)&pPtr[MMC_MSG_PAYLOAD_OFFSET+1]);
                fnMailRunMailPieceDataHandler(pMcp,pData,lwDataLength);
            }
            break;
        case MMC_DATA_LOG_RSP:
            fnProcessLogData(pData,lwDataLength);
            break;
        case MMC_DOWNLOAD_WEIGH_BREAK_TBL_RSP:
            fnWeightBreakDwnldRspHandler(pData);
            break;
        default:
            break;
        };  
    }
}

static void fnLogMailPieceData(const UINT8 *pData)
{
    const FP_MAIL_PIECE_RECORD *pMailPieceData = (const FP_MAIL_PIECE_RECORD*)pData;
    (void)memcpy((void*)&feederMailPieceDataLog[feederMailPieceDataLogIndx],(void*)pMailPieceData,sizeof(FP_MAIL_PIECE_RECORD));
    feederMailPieceDataLogIndx = (feederMailPieceDataLogIndx+1)%NUM_MAILPIECE_DATA_LOG_ENTRIES;
}

static void fnProcessLogData(const UINT8 *pData, const UINT32 lwDataLength)
{
    UINT16 wRecSize = 0;
    UINT16 wNumRecs = 0;
    UINT16 i = 0;
    UINT8 *pLogEntry = NULL;
    UINT8 *pSrcLogEntry = NULL;

    wRecSize = (UINT16)pData[MMC_MSG_PAYLOAD_OFFSET];
    (void)memcpy((void*)&wNumRecs,(void*)&pData[MMC_MSG_PAYLOAD_OFFSET+1],sizeof(wNumRecs));
    if (!pFeederLogData)
    {
        pFeederLogData = (UINT8*)malloc(NUM_FEEDER_LOG_ENTRIES * wRecSize);
        if (pFeederLogData)
        {
            (void)memset((void*)pFeederLogData,0,NUM_FEEDER_LOG_ENTRIES * wRecSize);
            lwFeederLogDataNumEntries = 0;
            lwFeederLogDataIndex = 0;
            lwFeederLogDataRecSize = (UINT32)wRecSize;
        }                               
    }
    if (pFeederLogData && wRecSize == lwFeederLogDataRecSize)
    {
        pSrcLogEntry = &pData[MMC_MSG_PAYLOAD_OFFSET+3];
        for (i=0; i < wNumRecs; i++)
        {
            pLogEntry = pFeederLogData + (lwFeederLogDataIndex * lwFeederLogDataRecSize);
            (void)memcpy((void*)pLogEntry,(void*)pSrcLogEntry,lwFeederLogDataRecSize);
            pSrcLogEntry += lwFeederLogDataRecSize;
            lwFeederLogDataIndex = (lwFeederLogDataIndex+1)%NUM_FEEDER_LOG_ENTRIES;
            if (lwFeederLogDataNumEntries < NUM_FEEDER_LOG_ENTRIES)
                lwFeederLogDataNumEntries++;
        }
    }
}

static void fnWeightBreakDwnldRspHandler(const void *pData)
{
    if (*((UINT8 *)pData+MMC_DNLD_WGTBRK_RSP_STATUS_OFFSET) == 0) // zero is success
    {
        // the weight breaks have been downloaded succesfully - 
        // clear "wow class changed" flag and start mail flow.
//        fnSetWOWClassChanged(FALSE);
        fnStartFeeder();
    }
    else
    {
        // there was an error
        // log the error
        //fnMMCIReportMMCError(MCPID_MJWOW,MCP_WOW_WEIGHT_BREAK_DWNLD_SEQ_ERR);
        fnSendFeederError(MCP_WOW_WEIGHT_BREAK_DWNLD_SEQ_ERR);
        /* transition to the 'Idle' state */
        //fnMMCIstateIdle();
    }
}

UINT32 fnFeederGetLogDataBuff(void)
{
    return (UINT32)pFeederLogData;
}
UINT32 fnFeederGetLogDataLength(void)
{
    if (pFeederLogData)
        return (UINT32)(NUM_FEEDER_LOG_ENTRIES * lwFeederLogDataRecSize);
    else
        return 0;
}

/**********************************************************************
fnMailRunProfileErrorHandler
This function handles Error Log messages while the Feeder Task is
in the mail run states.

Scope
Private

Prototype
static void fnMailRunProfileErrorHandler(const void *pMcp, const void *pData, const unsigned long lwDataLength);

Pre-Conditions
Feeder Task is in one of the mail run states.

Inputs
pMcp - Pointer to an MCP Instance.  Unused - this will always be the Feeder MCP.
pData - Pointer to the Motion Class Message.
lwDataLength - Length of the Motion Class Message.

Processing

**********************************************************************/
static void fnMailRunProfileErrorHandler(const void *pMcp, const void *pData, const unsigned long lwDataLength)
{
    // TODO: Notify CM that we are stopping
    //  fnSendXXX();

    //fnStopFeeder();

    UINT8 *pPtr = ((UINT8*)pData) + MMC_MSG_PAYLOAD_OFFSET;
    UINT8 bErrClass = pPtr[MMC_MIDJET_LOG_ERROR_CLASS_OFFSET];
    UINT8 bErrCode = pPtr[MMC_MIDJET_LOG_ERROR_CODE_OFFSET];
    UINT8 bContext1 = pPtr[MMC_MIDJET_LOG_ERROR_CONTEXT1_OFFSET];
    UINT8 bContext2 = pPtr[MMC_MIDJET_LOG_ERROR_CONTEXT2_OFFSET];

    switch (bErrClass) {
    case MMC_LOG_ERROR_CLASS_FATAL:
        fnMailRunFatalLogErrorHandler(bErrCode,bContext1,bContext2);
        break;
    case MMC_LOG_ERROR_CLASS_SERVICE:
        fnMailRunServiceLogErrorHandler(bErrCode,bContext1,bContext2);
        break;
    case MMC_LOG_ERROR_CLASS_OPERATOR:
        fnMailRunOperatorLogErrorHandler(bErrCode,bContext1,bContext2);
        break;
    case MMC_LOG_ERROR_CLASS_INFO:
        fnMailRunInfoLogErrorHandler(bErrCode,bContext1,bContext2);
        break;
    };

    if (bLastJamCode)
    {
        eCurrStopType = EMERGENCY_STOP;
        fnSetState(FDR_STOPPING_STATE);
        fnFeederStopped();
    }
}

static void fnMailRunFatalLogErrorHandler(const UINT8 bErrCode, const UINT8 bContext1, const UINT8 bContext2)
{
    switch (bErrCode) {
    case MMC_MIDJET_FATAL_WATCHDOG_TIMEOUT:
        bLastJamCode = MCP_WATCHDOG_RESET;
        break;
    case MMC_MIDJET_FATAL_MCP_RESET:
        bLastJamCode = MCP_FATAL_RESET;
        break;
    case MMC_MIDJET_FATAL_BAD_FLASH_CHECKSUM:
        break;
    case MMC_MIDJET_FATAL_MCP_HOST_RESET:
        break;
    };
}
static void fnMailRunServiceLogErrorHandler(const UINT8 bErrCode, const UINT8 bContext1, const UINT8 bContext2)
{
}
static void fnMailRunOperatorLogErrorHandler(const UINT8 bErrCode, const UINT8 bContext1, const UINT8 bContext2)
{
}
static void fnMailRunInfoLogErrorHandler(const UINT8 bErrCode, const UINT8 bContext1, const UINT8 bContext2)
{
    switch (bErrCode) {
    case MMC_MIDJET_INFO_BAD_PROFILE_COMMAND:
        bLastJamCode = MCP_BAD_PROFILE_COMMAND;
        break;
    case MMC_MIDJET_INFO_INVALID_MOTOR_STATE:
        bLastJamCode = MCP_INVALID_MOTOR_STATE;
        break;
    case MMC_MIDJET_INFO_INVALID_POSITION_MOVE_STATE:
        bLastJamCode = MCP_INVALID_POSITION_MOVE;
        break;  
    case MMC_MIDJET_SERVO_POSITION_ERROR_FAULT:
    //  bLastJamCode = MCP_SERVO_POSITION_ERROR;
    // TODO: un-comment full servo error handling when we have a chance to debug the servo ...
        fnLogSystemError(ERROR_CLASS_FP_FEEDER, MCP_SERVO_POSITION_ERROR, bContext1, bContext2, 0);
        break;
    case MMC_WOW_MODULE_INFO_ERR:
        // map the WOW MCP error code to UIC error code and set the UIC errro code to jam code.
        bLastJamCode = fnMapWOWError(bContext1);
        fnLogSystemError(ERROR_CLASS_FP_FEEDER, MMC_WOW_MODULE_INFO_ERR, bContext1, bContext2, 0);
        break;
    };
}

static void fnProcessMailAtFeederExit(void)
{
    if (fSinglePieceMode)
    {
        if (--bTotalPieceCount == 0)
        {
            // clear the start mail flow flag so the feeder
            // profiles don't re-start
            fnMCPSetRemoteFlag(pFeederMcp, MMC_START_MAIL_FLOW_FLAG, MMC_FLAG_CLEAR);

            // since the profiles will automatically pause the 
            // mail flow, set the state to stopping.  when
            // the profiles stop the mail flow they send the
            // paused profile status - which is when we perform the
            // last step of the stop process
            fnSetState(FDR_STOPPING_STATE);
        }
        else if (lwFeederTaskState == FDR_RUNNING_STATE)
        {
            // DSD 05/30/2007
            // A piece has exited the feeder.
            // Enable the roller timeout in case the 
            // piece that just exited actually started
            // the mail run under FS2.  This could happen
            // if the previous mail run was single piece
            // mode. In this condition, after the 1st piece
            // of the mail run exits the feeder, the roller
            // timer hasn't started, and the system will not
            // timeout like is should.
            lwRollerTimerVal = 0;
            (void)OSStartTimer(FP_FEEDER_ROLLER_STOP_TIMER);
        }
    }
    else
    {
        // normal (not single piece mode)
        if (lwFeederTaskState == FDR_RUNNING_STATE)
        {
            // DSD 1/15/2007
            // A piece has exited the feeder.
            // Enable the roller timeout in case the 
            // piece that just exited actually started
            // the mail run under FS2.  This could happen
            // if the previous mail run was single piece
            // mode. In this condition, after the 1st piece
            // of the mail run exits the feeder, the roller
            // timer hasn't started, and the system will not
            // timeout like is should.
            lwRollerTimerVal = 0;
            (void)OSStartTimer(FP_FEEDER_ROLLER_STOP_TIMER);
        }
    }

}

/**********************************************************************
fnMailRunMailPieceDataHandler
This function handles MMC_MAIL_PIECE_DATA_RSP messages while the Feeder Task is
in the mail run states.

Scope
Private

Prototype
static void fnMailRunMailPieceDataHandler(const void *pMcp, const void *pData, const unsigned long lwDataLength);

Pre-Conditions
Feeder Task is in one of the mail run states.

Inputs
pMcp - Pointer to an MCP Instance.  Unused - this will always be the Feeder MCP.
pData - Pointer to the Motion Class Message.
lwDataLength - Length of the Motion Class Message.

NOTES:

MODS:
 2010.07.07 Clarisa Bellamy & Raymond Shen - Add a check for the bobState.  If bob has started the
                        precreate but not finished the debit, then we MUST not rate the next 
                        piece yet, because it will overwrite the rating data and DCAP data needed
                        to kick the buckets.   In that case, pause until the debit is finished
                        or count indicates we have timed out.  
                        In case the of timeout, (2.5 seconds.) fake a Paper Skipped (missed?) error 
                        from the feeder and exit the loop.
                        To ensure that the queue does not fill up while fdrTask is paused, set the
                        paused flag, and clear it when we exit the loop.


 2010.02.05 Bob Li      - Before setting the new postage, check the current indicia type firstly
                        so that system can check if it is necessary to update the indicia type.
 2009.07.28 Clarisa Bellamy - Removed the last part of the function and put it into 
                        a new function called fnMailPieceDataHandlerContinue.  At the end, this 
                        function now checks to see if the rating has modified the 
                        inscription, via auto inscriptions.  If so, it sends an intertask
                        msg to bobtask to update the image.  The function that handles 
                        the reply will call fnMailPieceDataHandlerContinue.  If the inscription has
                        not been modified, then this function calls fnMailPieceDataHandlerContinue.

**********************************************************************/
static void fnMailRunMailPieceDataHandler(const void *pMcp, const UINT8 *pData, const unsigned long lwDataLength)
{
    UINT16  usLength = 0;
    UINT16  usPieceWeight = 0;
    UINT16  usThickness = 0;
    UINT16  usWidth = 0;
//    MAIL_PIECE_DATA stMailPieceData;
    UINT32  ulPV;
    UINT8   ucResult;
    UINT8   ucResBob = BOB_OK;
//    SINT16  sRetStatus = RSSTS_NO_ERROR;
    struct rdim_output  *pWidthBreaksInfo;
    BOOL    fStaticChangeNeeded = FALSE;
    UINT16  usCurIndicia;           // For comparison with next indicia to see if it needs to be updated.
    UINT8   ucCurrentPrintMode;
    emBobState  tmpBobState;        // CM's view of bob's precreate/debit state.
    UINT32  ulCount;                // Number of ticks paused waiting for current envelope to finish debiting.
    UINT8   rsp[5];                 // Buffer for msg to CM to fake a paper error from the feeder.
    UINT16  usSensorValue;          // Current sensor values must be put into the msg faking the paper error.
    UINT8   fMissedEnvelope;        // Set when timed out waiting for the current envelope to finish debiting.
  

    ucCurrentPrintMode = fnOITGetPrintMode();

     // Clear WOW Dim Rating error flag
    fnSetWOWDimRatingErrorFlag(FALSE);

    if(    (pData[MMC_MSG_SOURCE_OFFSET] == FDR_MCP_ID)
       &&  (    (pData[MMC_WOW_MAIL_PIECE_DATA_ID_OFFSET] == MMC_WOW_MAIL_PIECE_DATA_ID)
             || (pData[MMC_WOW_MAIL_PIECE_DATA_ID_OFFSET] == MMC_PIP_WOW_MAIL_PIECE_DATA_ID) ) 
       &&  (    (ucCurrentPrintMode == PMODE_WOW) 
             || (ucCurrentPrintMode == PMODE_WEIGH_1ST)  )  )
    {
        // DSD 08-23-2004 - Added WOW mail piece processing
        // Got mail piece data from the WOW MCP while in WOW mode

        // First check to see if we are in Weigh First Piece Mode
        // and this is the first piece.
        if (ucCurrentPrintMode == PMODE_WEIGH_1ST && ulNumPiecesWeighed == 0)
        {
            ulNumPiecesWeighed++;
            fnMCPSetRemoteFlag(pMcp, MMC_WOW_DONE, MMC_FLAG_SET);
            fnMCPSetRemoteFlag(pMcp, MMC_WOW_MODE_FLAG, MMC_FLAG_CLEAR);
        }

        // If the current piece has started but not finished the debit, then 
        //  wait for it to finish before rating the next piece.  Otherwise 
        //  the data required at the end of the debit (DCAP, accounting, etc.)
        //  may be corrupted by this piece.
        tmpBobState = fnGetBobState();
        ulCount = 0;
        fMissedEnvelope = FALSE;
        while(   
                 (tmpBobState == bob_PrecreateStarted)
              || (tmpBobState == bob_Precreated)
              || (tmpBobState == bob_CommitStarted) )
        {
            if( ulCount == 0 )
            {
                fnDumpStringToSystemLog( "FDRWOW paused, weight too early." );
            }
            ulCount++;
            // Allow the Poll-sensor timer to stop itself
            fFdrPaused = TRUE;
            OSWakeAfter( (UINT32)FDR_PAUSE_TICK );
            tmpBobState = fnGetBobState();

            if( ulCount > 250 )
            {
                usSensorValue = fnMapCurrentSensorValues();
                rsp[0] = FDR_PAPER_SKIPPED;
                rsp[1] = 0;
                rsp[2] = 0;
                rsp[3] = (UINT8)usSensorValue;
                rsp[4] = (UINT8)(usSensorValue >> 8);

                (void)fnSendIntertask( _CMTASK_, FDRMCPTASK, FC_UNSLCT_STATUS, BYTE_DATA, rsp, sizeof(rsp) );
                fMissedEnvelope = TRUE;
                break;
            }
        }

        // If the previous piece finally debited (or there was no previous piece)...
        if( fMissedEnvelope == FALSE )
        {
            // If we paused, do un-pause stuff...
            if( ulCount > 0 )
            {
                // Prevent Poll-sensor timer from stopping.
                fFdrPaused = FALSE;

                // Log how long the the fdrTask was  paused.
                sprintf( pFdrScratchpad , "FDRWOW unpaused, delayed %d %d-counts", (int)ulCount, (int)FDR_PAUSE_TICK );
                fnDumpStringToSystemLog( pFdrScratchpad  );
            }

            // extract mail piece weight from message
            usPieceWeight = pData[MMC_WOW_MAIL_PIECE_WEIGHT_OFFSET] << 8;
            usPieceWeight |= pData[MMC_WOW_MAIL_PIECE_WEIGHT_OFFSET+1];

            // DSD 10-18-2004 Log mail piece weight
            // leave the weight in the log
            fnLogMailPieceWeight(usPieceWeight);
        
            ulPV = 0;
            if (usPieceWeight > 0)
            {
                // call rating to get the postage value for the given weight
                if (pData[MMC_WOW_MAIL_PIECE_DATA_ID_OFFSET] == MMC_PIP_WOW_MAIL_PIECE_DATA_ID)
                {
                    // DSD 3/22/1006 - PIP development
                    // extract the length from message
                    usLength = pData[MMC_PIP_WOW_MAIL_PIECE_LENGTH_OFFSET]<<8;
                    usLength |= pData[MMC_PIP_WOW_MAIL_PIECE_LENGTH_OFFSET+1];

                    // get width break table from Rate manager.
//                    sRetStatus = fnRateFetchWidthBreaks(&pWidthBreaksInfo);

                    // extract the width sensor value from the message and look up current width break in width break table.
/*
                    if( (sRetStatus == RSSTS_NO_ERROR) &&
                        (pData[MMC_PIP_WOW_MAIL_PIECE_WIDTH_BRK_OFFSET]   >= 0) )
                    {
                        if(pData[MMC_PIP_WOW_MAIL_PIECE_WIDTH_BRK_OFFSET] < pWidthBreaksInfo->numDimensions)
                        {
                            usWidth = pWidthBreaksInfo->Dimensions[pData[MMC_PIP_WOW_MAIL_PIECE_WIDTH_BRK_OFFSET]];
                        }
                        else
                        {
                            // let the width over size, we will handle it later in fnRateGetPostageForWeight().
                            usWidth = pWidthBreaksInfo->Dimensions[pWidthBreaksInfo->numDimensions - 1] + 10;
                        }
                    }
                    else
                    {
                        usWidth = 0;
                    }
                
*/
                    // DSD 04/26/2007 - Ignore width dimension if the width sensor isn't working
                    // properly and the user chooses to ignore it.
/*
                    if (fnGetIgnoreWidthSensorStatus() == TRUE)
                    {
                        usWidth = 0;
                    }
*/
                    // extract the thickness from the message
                    usThickness = pData[MMC_PIP_WOW_MAIL_PIECE_THICKNESS_OFFSET];

                    // log pip dimension data
                    fnLogPIPDimension(usPieceWeight,usThickness,usWidth,usLength);
                }
            
                //get the current indicia type before setting the new postage
                //fnGetCurrIndiciaType(&usCurIndicia);
                ucResBob = fnValidData( BOBAMAT0, BOBID_UPDATE_AND_READ_INDICIA_TO_PRINT, &usCurIndicia );
                if( ucResBob == BOB_OK )
                {
/*
                    stMailPieceData.usWOWWeight = usPieceWeight;
                    stMailPieceData.usWidth = usWidth;
                    stMailPieceData.usLength = usLength;
                    stMailPieceData.usThickness = usThickness;
*/
            
                    sprintf( pFdrScratchpad , "FdrWOW GetPostageForWeight" );
                    fnDumpStringToSystemLog( pFdrScratchpad  );
//                    ucResult = fnRateGetPostageForWeight(&ulPV, &stMailPieceData);
                   
                }
                else
                {
                    // Couldn't get critical info from bobtask.
                    sprintf( pFdrScratchpad , pBOBACCESSERROR );
                    fnDumpStringToSystemLogWithNum( pFdrScratchpad, __LINE__ );
                    fnMailPieceDataHandlerContinue( MCP_WOW_ERROR );  // !!! Needs a new Error code.
                }
            }
            else
            {
//                ucResult = WEIGHT_LIGHT;
            }

            if( ucResBob == BOB_OK )
            {
                // log and save weight
                //sprintf( pFdrScratchpad , "FdrWOW Log&SaveWeight, PV = %d", ulPV );
                //fnDumpStringToSystemLog( pFdrScratchpad  );
//                fnRateLongToPackedBcdWeight(fnRateGetWeightUnit(), ucWOWWorkingWeight, (UINT32)usPieceWeight);
//                fnStoreWOWData(ucResult, ucWOWWorkingWeight);

/*
                if (ucResult == WEIGHT_OKAY)
                {
                    if( fnCheckImageNeedsUpdating( &fStaticChangeNeeded, usCurIndicia ) == SUCCESS )
                    {
                        if( fStaticChangeNeeded )
                        {
                            // We need to update the image generator with a new image. 
                            //  We want to time out if we never get a response from bob.
                            (void)OSStartTimer( FP_FEEDER_MSG_TIMER );
                            OSSendIntertask( SCM, FDRMCPTASK, BOB_GEN_STATIC_IMAGE_REQ, NO_DATA, NULL, 0 );
                        }
                        else
                        {
                            // The current static image is fine, continue to handle the piece.
                            fnMailPieceDataHandlerContinue( 0 );  // No errorcode.
                        }
                    }
                    else // There was a problem doing the check.
                    {
                        fnMailPieceDataHandlerContinue( MCP_WOW_ERROR );  // !!! Needs a new Error code.
                    }
                }
                else
                {
                    // DSD 02/08/2005
                    // The Rates Manager has determined that this mail piece weight
                    // is not valid for the current Rates Carrier/Class/Fee.
                    // Eject the mail piece without printing any postage.
                    // The status value passed in to fnStoreWOWData will cause
                    // the OI to display the appropriate error screen.
                    fnMCPSetRemoteFlag(pMcp, MMC_START_MAIL_FLOW_FLAG, MMC_FLAG_CLEAR);
                    fnMCPSetRemoteFlag(pMcp, MMC_PRINT_FLAG, MMC_FLAG_CLEAR);
                    fnMCPSetRemoteFlag(pMcp, MMC_EJECT_MAILPIECE_FLAG, MMC_FLAG_SET);

                    // 05/09/2006 PIP dimension error detected, Restart Mail flow to eject the mailpiece after the error happened.
                    fnMCPSetRemoteFlag(pMcp, MMC_UIC_RESTART_WOW_MAIL_FLOW,MMC_FLAG_SET);
                    // Since the Print Flag is cleared and the Eject flag is set
                    // the normal method of sending the MAILPIECE_PROCESSED
                    // message to the OI doesn't work.  So manually send the
                    // message now.  Otherwise the OI never displays the appropriate
                    // error screen because it never thinks a piece was processed.
                    // TBD: do we need this line? Seems no.
                    //fnMMCIProcessedMailpiece();

                    // store the WEIGHT_HEAVY/WEIGHT_LIGHT status as Dim status for error info displayed on screen later.
                    fnRateSetMailpieceDimStatus( ucResult );
                    fnMailPieceDataHandlerContinue( MCP_WOW_ERROR );  // !!! Needs a new Error code
                } 
*/
            } // End of BOB_OK 
        }
        else
        {
            // Missed mailpiece...
            // Allow Poll-sensor timer to keep going once it starts again?
            fFdrPaused = FALSE;
            
            fnMCPSetRemoteFlag(pMcp, MMC_PRINT_FLAG, MMC_FLAG_CLEAR);
            fnDumpStringToSystemLog( "FdrWOW Error - Timeout waiting for resume" );
            fnMailPieceDataHandlerContinue( MCP_WOW_ERROR );  // !!! Needs a new Error code
        }
                
    } // End of msg is MAIL_PIECE_DATA and print mode is WOW or W1P mode

    return;
}

// !!! Start of stuff added to update the image in the middle of a WOW run.
// ***************************************************************************
// FUNCTION NAME:        fnMailPieceDataHandlerContinue
// PURPOSE:          
//   This is a continuation of the process started in fnMailRunMailPieceDataHandler.
//   All this stuff was in there, until we had to support Auto Inscriptions in WOW mode,
//   So here is all the stuff that happens AFTER the new image is generated (if a new
//   one needs to be generated.)
// INPUTS:           
//   errcode - THis is zero if we havne't had an error yet.
// OUTPUT:
//   None
// MODS:
// 2009.07.28  Clarisa Bellamy - New function                  
//-----------------------------------------------------------------------------
static void fnMailPieceDataHandlerContinue( UINT8 errcode )
{
    if( errcode == 0 )
    {
        // Tell the WOW to restart the piece that was just weighed.
        fnMCPSetRemoteFlag( pFeederMcp, MMC_UIC_RESTART_WOW_MAIL_FLOW, MMC_FLAG_SET );

/*
        if( fnRateGetMailpieceDimStatus( ) != MAILPIECE_OKAY )
        {
            // Set WOW Dim Rating error flag
            fnSetWOWDimRatingErrorFlag(TRUE);
            
            // set jam code and stop mail run immediately.
            bLastJamCode = MCP_WOW_ERROR;

            eCurrStopType = EMERGENCY_STOP;
            fnSetState(FDR_STOPPING_STATE);
            fnFeederStopped();
            fnDumpStringToSystemLog( "FdrWOW DimRatingError" );
        }
*/
    }
    else
    {
        // Set WOW Dim Rating error flag
        fnSetWOWDimRatingErrorFlag(TRUE);
        // set jam code and stop mail run immediately.
        bLastJamCode = errcode;
        eCurrStopType = EMERGENCY_STOP;
        fnSetState( FDR_STOPPING_STATE );
        fnFeederStopped();
        fnDumpStringToSystemLog( "FdrWOW RatingError" );
    }

    // send a message to tell CM that WOW mail piece data is ready.If status is WEIGHT_OKAY,
    // CM should precreate Indicum, otherwise CM should handle the error.
    fnSendFeederWOWMailPieceDataReady();

}

// ***************************************************************************
// FUNCTION NAME:        fnCheckImageNeedsUpdating
// PURPOSE:          
//   Checks to see if there is anything in the static image that has changed.
//   
// INPUTS:           
//   lwTimerId - Id of timer that expired.  We don't need this, because this function
//              is only used by one of the timers.
// OUTPUT:
//   BOOL   fStatus = SUCCESS (0) IF we did not run into any problems doing the check.
//                    FAILURE - If we COULD NOT do the check.
// NOTES:
//  1. The first use of this is for support of Auto Inscriptions that can change
//      during a WOW run.
//  2. In a country that uses low-value indicias, we will want to put a test here
//      to see if the indicia needs to changed.
//  3. If the current indicia type is different from the previous type, the indicia needs
//      to be changed.
// MODS:
// 2010.07.25  Clarisa Bellamy - Updated to fix the check for indicia type.  Bobtask 
//                      internal data should never be accessed directly by other tasks. 
//                      Changes were made to orbit database to allow access the indicia 
//                      type to be read the correct way (via fnValidData()). 
//                      - Added code to check the ad also, but that is commented out 
//                      until we actually allow that to change. (So far, the add is 
//                      only changed based on the carrier type (PreSort Extra) which
//                      doesn't change on the fly.             
// 2010.02.05  Bob Li          - Updated to check the indicia type.
// 2009.07.29  Clarisa Bellamy - New function                  
//-----------------------------------------------------------------------------
BOOL fnCheckImageNeedsUpdating( BOOL *pfChangeNeeded, UINT16 usPrevIndicia )
{
    UINT16  wCurrInsc;
    UINT16  wNewInsc ;
    BOOL    fBobStatus = BOB_OK;
    BOOL    fRetStatus = SUCCESS;
    BOOL    fChangeNeededLocal = FALSE;
    UINT16  usCurIndicia;

    // Test to see if Auto Inscriptions has changed the inscription:
    // First, get the current inscription...
    fBobStatus = fnValidData( BOBAMAT0, READ_CURRENT_INSCR_SELECT, &wCurrInsc );
    if( fBobStatus == BOB_OK )
    {
        // Now have bob update the inscription selection, and read it.
        fBobStatus = fnValidData( BOBAMAT0, BOBID_UPDATE_AND_READ_INSCR_SELECT, &wNewInsc );
        if( fBobStatus == BOB_OK )
        {
            if( wCurrInsc != wNewInsc )
            {
                //sprintf( pFdrScratchpad , "FdrWOW Inscription Changed" );
                //fnDumpStringToSystemLog( pFdrScratchpad  );
                fChangeNeededLocal = TRUE;
            }
        }
        else
        {
            sprintf( pFdrScratchpad , pBOBACCESSERROR );
            fnDumpStringToSystemLogWithNum( pFdrScratchpad, __LINE__ );
        }

    }

    // We don't currently support changing Ads on the fly (in wow mode), but when we do,
    //  uncomment this section to support it.  (PresortXtra is a psuedo carrier, so it won't
    //  change on the fly.)
    /*
    fBobStatus = fnValidData( BOBAMAT0, READ_CURRENT_AD_SELECT, &wCurrInsc );
    if( fBobStatus == BOB_OK )
    {
        // Now have bob update the ad selection, and read it.
        fBobStatus = fnValidData( BOBAMAT0, BOBID_UPDATE_AND_READ_AD_SELECT, &wNewInsc );
        if( fBobStatus == BOB_OK )
        {
            if( wCurrInsc != wNewInsc )
            {
                //sprintf( pFdrScratchpad , "FdrWOW Ad Changed" );
                //fnDumpStringToSystemLog( pFdrScratchpad  );
                fChangeNeededLocal = TRUE;
            }
        }
        else
        {
            sprintf( pFdrScratchpad , pBOBACCESSERROR );
            fnDumpStringToSystemLogWithNum( pFdrScratchpad, __LINE__ );
        }
    }
    */

    // If we need to check for anything else, do it here.... 
    if( fBobStatus == BOB_OK )
    {   
        // fnGetCurrIndiciaType should NOT be called from any function outside 
        //  of the bobTask.  It reads AND modifies private bob data.
        //fnGetCurrIndiciaType(&usCurIndicia);
        fBobStatus = fnValidData( BOBAMAT0, BOBID_UPDATE_AND_READ_INDICIA_TO_PRINT, &usCurIndicia );
        if( fBobStatus == BOB_OK )
        {
            //if the indicia type is changed, trigger the redrawing indicia event.
            if(usCurIndicia != usPrevIndicia)
            {
                //sprintf( pFdrScratchpad , "FdrWOW Indicia Changed" );
                //fnDumpStringToSystemLog( pFdrScratchpad  );
                fChangeNeededLocal= TRUE;
            }
        }
        else
        {
            sprintf( pFdrScratchpad , pBOBACCESSERROR );
            fnDumpStringToSystemLogWithNum( pFdrScratchpad, __LINE__ );
        }
    }
    if( fBobStatus != BOB_OK )
    {
        // Couldn't get critical information from Bobtask.
        fRetStatus = FAILURE;
    }
    
    // If our checks were successful, let the caller know the results.
    if( fRetStatus == SUCCESS )
    {
        *pfChangeNeeded = fChangeNeededLocal;
    }

    return( fRetStatus );
}

// ***************************************************************************
// FUNCTION NAME:        fnHandleGenStaticImageResponse
// PURPOSE:          
//   This is called when the feeder task receives the BOB_GEN_STATIC_IMAGE_RSP  
//  from the bobtask, which means that the image has been updated. This happens
//  if the inscription was automatically changed during a WOW run.
// INPUTS:           
//   pMsg - Pointer to the intertask message.  We don't actually use this, because 
//          this is only called when the correct message is recieved, and we don't
//          care about the data in the message.  
// OUTPUT:
//   None
// NOTES:
//  1. The function format is fixed.
//
// MODS:
// 2009.07.28  Clarisa Bellamy - New function                  
//-----------------------------------------------------------------------------
static void fnHandleGenStaticImageResponse( const INTERTASK *pMsg )
{   
    (void)OSStopTimer( FP_FEEDER_MSG_TIMER );
    fnMailPieceDataHandlerContinue( 0 );  // No error yet.
    }


// ***************************************************************************
// FUNCTION NAME:        fnFeederMsgTimerExpire
// PURPOSE:          
//   This is called when the feeder message timer expires.  This will only happen
//      if the Feeder task is waiting for a response from a message and it never 
//      arrives.
// INPUTS:           
//   lwTimerId - Id of timer that expired.  We don't need this, because this function
//              is only used by one of the timers.
// OUTPUT:
//   None
// NOTES:
//  1. The function format is fixed.
//
// MODS:
// 2009.07.28  Clarisa Bellamy - New function                  
//-----------------------------------------------------------------------------
// For now there is only one message that we are waiting for, the bob's generate image response.
void fnFeederMsgTimerExpire( UINT32 lwTimerId )
{
    (void)OSStopTimer( FP_FEEDER_MSG_TIMER );
    
    // Timed out waiting for bob to update the image.
    fnDumpStringToSystemLog( "FdrTsk timed out waiting for bob to update the image" );
    fnMailPieceDataHandlerContinue( MCP_WOW_ERROR );  // !!! Needs a new Error code
}
// End of stuff added to update the image in the middle of a WOW run. !!!!


/**********************************************************************
fnMailRunProfileStatusHandler
This function handles Profile Status messages while the Feeder Task is
in the mail run states.

Scope
Private

Prototype
static void fnMailRunProfileStatusHandler(const unsigned char bStatus);

Pre-Conditions
Feeder Task is in one of the mail run states.

Inputs
bStatus - Profile Status

Processing

IF Profile Status is recognized THEN
  Dispatch control to the handler for the status.
END IF

**********************************************************************/
static void fnMailRunProfileStatusHandler(const unsigned char bStatus)
{
    switch (bStatus) {
    case MMC_FEEDER_READY:
        fnFeederReady();
        break;
    case MMC_FEEDER_MOTOR_STARTED:
        fnFeederStarted();
        break;
    case MMC_FEEDER_PAUSED:
        fnFeederStopped();
        break;
    case MMC_MAIL_AT_FEEDER_EXIT:
        fnProcessMailAtFeederExit();
        break;
    case MMC_TAR2_EXIT_JAM:
    case MMC_TAR1_EXIT_JAM:
    case FP_FDR_WOW_MID_JAM:
    case FP_FDR_WOW_EXIT_JAM:
        fnHandleJam(bStatus);
        break;
    case WOW_PIECE_TOO_LONG:
        fnHandleJam(bStatus);
        fnFeederStopped();
        break;
    case MMC_INTERLOCK_CVR_OPEN:
        fnHandleJam(bStatus);
        fnFeederStopped();
        break;
    case MMC_MAIL_ON_FEEDER_DECK:
        fnHandleMailOnDeck();
        break;
    case MMC_OUT_OF_MAIL:
        fnHandleOutOfMail();
        break;
    case FP_FDR_ENTER_MOTOR_COOL_DOWN_MODE: // Motor Cool Down Mode Notifications
    case FP_FDR_EXIT_MOTOR_COOL_DOWN_MODE:
        break;
    case FP_FDR_BAD_MOTOR_THERMISTER_ERR:   // Motor Temp Errors
    case FP_FDR_MOTOR_OVER_TEMP_ERR:        
        fnStopFeederForProfileStatusError(bStatus);
        break;
    case MMC_MAIL_AT_SEALER_EXIT:       
        fnHandleMailAtSealerExit();
        break;
    case WOW_MAIL_AT_EXIT:
        fnHandleWOWMailAtExit();
        break;
    case WOW_PAUSED:
        fnHandleWOWPaused();
        break;
    case MMC_PAUSE_WOW:
    case MMC_WOW_LOW_SPEED:
    case MMC_WOW_RESTART:
    case MMC_WOW_STOP:
    case MMC_WOW_START:
        fnHandleWOWStateChange(bStatus);
        break;
    case MMC_WOW_REZERO_START:
        fnHandleWOWRezeroStart();
        break;
    case MMC_WOW_REZERO_DONE:
        fnHandleWOWRezeroDone();
        break;
    default:
        break;
    };
}

static void fnHandleJam(const UINT8 bJamCode)
{
    bLastJamCode = bJamCode;
    fnSetState(FDR_STOPPING_STATE); // the profiles take care of stopping.
                                    // when they're done the send the feeder paused message.
}

static void fnPauseFeederNormal(void)
{
    fFullStop = FALSE;
    fNormalPause = TRUE;

    fnSetState(FDR_PAUSING_STATE);

    // set stop profile flag
    fnMCPSetRemoteFlag(pFeederMcp,MMC_PAUSE_MAIL_FLOW_FLAG,MMC_FLAG_SET);
}
static void fnHandleOutOfMail(void)
{
    lwRollerTimerVal = 0;
    (void)OSStartTimer(FP_FEEDER_ROLLER_STOP_TIMER);
}
static void fnHandleMailOnDeck(void)
{
    unsigned long lwState = fnGetState();

    (void)sprintf(bFdrDbgMsgBuff,">FDR MailOnDeck %d<",(int)lwState);
    fnFdrLogMsg(bFdrDbgMsgBuff);

    if (lwState == FDR_PAUSED_STATE)
    {
        fMailRunInitiated = TRUE;
        fnSendFeederRestart();
    }
    else if (lwState == FDR_RUNNING_STATE)
    {
        // we are running, and mail has been put on deck
        // reset the timers so that they don't expire
        // by accident and terminate a valid mail run
        fnResetMailFlowTimers();
    }
}

static void fnHandleMailAtSealerExit(void)
{
    // Set the flag to tell the WOW module that the mail should be arriving
    fnMCPSetRemoteFlag(pFeederMcp, WOW_SEALER_HANDOFF_FLAG, MMC_FLAG_SET);
}

static void fnHandleWOWMailAtExit(void)
{
//    UINT8   WOWFlags[20];
//    UINT8   arrayOffset = 0;
//    UINT8   WOWFlagCount = 0;
    
    // Set the flag to tell the MMC the mail piece is exiting the WOW
    // Set the appropriate WOW Flag along with the flag to indicate that there
    // is a State Change at the WOW

    // Is not needed for FPHX
    //WOWFlags[arrayOffset++] = MMC_PRINTER_TRNSPRT_HANDOFF_FLAG;
    //WOWFlags[arrayOffset++] = MMC_FLAG_SET;
    //WOWFlagCount = 1;
    
    //if(sMMCIState.mailParam.mailMode == WOW_MODE)
//    if(fnOITGetPrintMode()== PMODE_WOW)
//    {
//        WOWFlags[arrayOffset++] = MMC_WOW_DONE;
//        WOWFlags[arrayOffset++] = MMC_FLAG_SET;
//        WOWFlagCount++;
//    }

	// RD - someone commented out the line below which removed the need for the vars WOWFlags etc so I commented them out to remove warnings.
    //fnMCPSetMultipleRemoteFlags(pFeederMcp, &WOWFlags[0], WOWFlagCount);
    
    fnMCPSetRemoteFlag(pFeederMcp, MMC_WOW_DONE, MMC_FLAG_SET);
}

static void fnHandleWOWPaused(void)
{
    // Set the flag to tell the WOW module that the WOW process is paused.
    fnMCPSetRemoteFlag(pFeederMcp, MMC_WOW_PAUSED_FLAG, MMC_FLAG_SET);
}

static void fnHandleWOWStateChange(UINT8 bStatus)
{
    UINT8   WOWFlags[20];
    UINT8   arrayOffset = 0;
    UINT8   WOWFlagCount = 0;

    // Set the appropriate WOW Flag along with the flag to indicate that there
    // is a State Change at the WOW
    WOWFlags[arrayOffset++] = WOW_STATE_CHANGE_FLAG;
    WOWFlags[arrayOffset++] = MMC_FLAG_SET;
    WOWFlags[arrayOffset++] = bStatus - MMC_MSG_TO_WOW_FLAG;
    WOWFlags[arrayOffset++] = MMC_FLAG_SET;
    WOWFlagCount = 2;
    fnMCPSetMultipleRemoteFlags(pFeederMcp, &WOWFlags[0], WOWFlagCount);
}

static void fnHandleWOWRezeroStart(void)
{
    // send a message to the CM that a rezero operation is ocurring
    fnSendFeederRezeroing();
    OSWakeAfter(0); // let the OIT display the message
}

static void fnHandleWOWRezeroDone(void)
{
    // send a message to the CM that the rezero is complete.
    fnSendFeederRezeroDone();
    OSWakeAfter(0); // let the OIT display the message
}
static void fnProcessCMRunning(const INTERTASK *pMsg)
{
    unsigned long lwState = fnGetState();

    fnResetMailFlowTimers();
    if (lwState == FDR_PAUSED_STATE && fMailRunInitiated == TRUE)
    {
        // clear stop profile flag so the mail flow starts again
        fnMCPSetRemoteFlag(pFeederMcp,MMC_PAUSE_MAIL_FLOW_FLAG,MMC_FLAG_CLEAR);
        fnMCPSetRemoteFlag(pFeederMcp, MMC_START_MAIL_FLOW_FLAG, MMC_FLAG_SET);

        // start running again.
        fnSetState(FDR_RUNNING_STATE);
    }
}

void fnFeederRollerTimerExpire(unsigned long lwTimerId)
{
    char bDebugMsg[64];
    if (++lwRollerTimerVal >= fnCMOSSetupGetCMOSSetupParams()->RollerTimeOut)   // DEFAULT_FEEDER_ROLLER_TIMEOUT_VALUE
    {
        (void)OSStopTimer(FP_FEEDER_ROLLER_STOP_TIMER);
        (void)sprintf(bDebugMsg,"FDR > Roller Timer Exp (%d)",lwFeederTaskState);
        fnFdrLogMsg(bDebugMsg);
        (void)OSSendIntertask(FDRMCPTASK,FDRMCPTASK,FDR_ROLLER_TIMEOUT,NO_DATA,NULL,0);
        lwRollerTimerVal = 0;
    }
    return;
}
void fnFeederInactivityTimerExpire(unsigned long lwTimerId)
{
    char bDebugMsg[64];
    if (++lwInactiveTimerVal >= fnCMOSSetupGetCMOSSetupParams()->ulMailInactivityTimeout)// DEFAULT_FEEDER_INACTIVITY_TIMEOUT_VALUE
    {
        (void)OSStopTimer(FP_FEEDER_MAIL_INACTIVITY_TIMER);
        (void)sprintf(bDebugMsg,"FDR > Inactive Timer Exp (%d)",lwFeederTaskState);
        fnFdrLogMsg(bDebugMsg);
        (void)OSSendIntertask(FDRMCPTASK,FDRMCPTASK,FDR_INACTIVITY_TIMEOUT,NO_DATA,NULL,0);     
        lwInactiveTimerVal = 0;
    }
}
static void fnProcessRollerTimeout(const INTERTASK *pMsg)
{
    if (fnCMOSSetupGetCMOSSetupParams()->ulMailInactivityTimeout > 0)
    {
        fnPauseFeederNormal();      // A paused mail run is restarted when mail is put on the deck.
    }
    else
    {
        fnStopFeeder();             // The mail run is complete and must be restarted.
    }
}
static void fnProcessInactivityTimeout(const INTERTASK *pMsg)
{
    fnSetState(FDR_STOPPING_STATE);
    fnFeederStopped();
    fSendMailJobComplete = TRUE;
    fnSendMailJobComplete(MCP_NO_ERROR,MCP_NO_ERROR);
}
static void fnProcessStopTimeout(const INTERTASK *pMsg)
{
    fnDumpStringToSystemLog("Feeder Stop Response Timeout!");
    eCurrStopType = EMERGENCY_STOP;
    fSendStopRsp = TRUE;
    fnSetState(FDR_STOPPING_STATE);
    bLastJamCode = MCP_FPFDR_NOT_PRESENT_ERR;
    fnFeederStopped();
}
static void fnProcessStartTimeout(const INTERTASK *pMsg)
{
    fnDumpStringToSystemLog("Feeder Start Response Timeout!");
    eCurrStopType = EMERGENCY_STOP;
    fSendStartRsp = TRUE;
    fnSendStartRsp(MCP_FPFDR_NOT_PRESENT_ERR);
    fnSetState(FDR_STOPPING_STATE);
    bLastJamCode = MCP_FPFDR_NOT_PRESENT_ERR;
    fnFeederStopped();
}
static void fnResetMailFlowTimers(void)
{
    (void)OSStopTimer(FP_FEEDER_ROLLER_STOP_TIMER);
    (void)OSStopTimer(FP_FEEDER_MAIL_INACTIVITY_TIMER);
    lwRollerTimerVal = 0;
    lwInactiveTimerVal = 0;
}

/**********************************************************************
fnFeederReady
This function handles the MMC_FEEDER_READY Profile Status message
 while the Feeder Task is in the mail run states.  This Profile Status
 indicates that the Feeder MCP Profiles are ready to start processing
 mail.

Scope
Private

Prototype
static void fnFeederReady(void);

Pre-Conditions
Feeder Task is in one of the mail run states.

Inputs


Processing

SEND Start Response Message

**********************************************************************/
static void fnFeederReady(void)
{
    // the feeder has started - stop the timer
    (void)OSStopTimer(FP_FEEDER_START_TIMER);

    fnSendStartRsp(MCP_NO_ERROR);

    // set the roller timer.
    // if no mail is ever put on the deck then when the
    // timer expires it will start the timeout sequence.
    lwRollerTimerVal = 0;
    (void)OSStartTimer(FP_FEEDER_ROLLER_STOP_TIMER);
}

/**********************************************************************
fnFeederReady
This function handles the MMC_FEEDER_MOTOR_STARTED Profile Status message
 while the Feeder Task is in the mail run states.  This Profile Status
 indicates that the Feeder MCP Profiles have started the motors and
 mail is now flowing through the system.

Scope
Private

Prototype
static void fnFeederStarted(void);

Pre-Conditions
Feeder Task is in one of the mail run states.

Inputs


Processing

SET State = RUNNING

**********************************************************************/
static void fnFeederStarted(void)
{
    unsigned long lwState = fnGetState();
    fnResetMailFlowTimers();
    if (lwState == FDR_STARTING_STATE)
    fnSetState(FDR_RUNNING_STATE);

}

/**********************************************************************
fnProcessStopReq
This function is called in response to a stop feeder request.

Scope
Private

Prototype
static void fnProcessStopReq(const INTERTASK *pMsg);

Pre-Conditions
pMsg != NULL

Inputs
pMsg - Pointer to an Intertask Message.

Processing

IF State is IDLE THEN
  CALL fnStartFeeder
ELSE
  SEND Run Rsp Msg (error)
END IF

**********************************************************************/
static void fnProcessStopReq(const INTERTASK *pMsg)
{
    unsigned char *pPtr = NULL;
    unsigned long lwLength = 0;
    emFdrStopType stopType;

    fMailRunInitiated = FALSE;

    FDRTASK_ASSERT(pMsg!=NULL); //lint !e506

    if (pMsg == NULL)
        return;

    GetIntertaskPointerAndLength((void**)&pPtr,&lwLength,pMsg); //lint !e605

    FDRTASK_ASSERT(pPtr);       //lint !e506

    if (pPtr)
    {
        stopType = pPtr[0];
        fSendStopRsp = TRUE;
        switch (stopType) {
        case GRACEFUL_STOP:
            eCurrStopType = stopType;
            fnStopFeeder();     // stop the feeder in a controlled fashion
            break;
        case EMERGENCY_STOP:
            eCurrStopType = stopType;
            fnSetState(FDR_STOPPING_STATE);
            fnFeederStopped();  // stop the feeder immediately
            break;
        case MTNC_STOP:
            eCurrStopType = stopType;
            fnPauseFeederForMaintenance();  // pause the feeder
            break;
        default:
            fnSendStopRsp(MCP_FPFDR_INVALID_REQUEST);
            break;
        };
    }
}
/**********************************************************************
fnStopFeeder
This function starts the process of stopping the Feeder.

Scope
Private

Prototype
static void fnStopFeeder(BOOL fNormal, BOOL fForce);

Pre-Conditions
Feeder Task is in one of the mail run states

Inputs


Processing


**********************************************************************/
static void fnStopFeeder()
{   
    // TODO: notify the CM if this is not a normal stop
    //if (!fNormal)     
    //  fnSendXXX();

    UINT32 lwState = fnGetState();

    fFullStop = TRUE;

    fnResetMailFlowTimers();

    if (lwState != FDR_PAUSING_STATE && lwState != FDR_PAUSED_STATE)
    {
        fnSetState(FDR_STOPPING_STATE);

        if (fSendStartRsp)
            fnSendStartRsp(MCP_NO_ERROR);   

        // set stop profile flag
        fnMCPSetRemoteFlag(pFeederMcp,MMC_PAUSE_MAIL_FLOW_FLAG,MMC_FLAG_SET);

        (void)OSStartTimer(FP_FEEDER_STOP_TIMER);
    }
    else if (lwState == FDR_PAUSED_STATE)
    {
        fnSetState(FDR_STOPPING_STATE);
        fnFeederStopped();      
    }
    else if (lwState == FDR_PAUSING_STATE)
    {
        fnSetState(FDR_STOPPING_STATE);     
    }
}
static void fnStopFeederForProfileStatusError(const unsigned char bStatus)
{
    // Used to stop the feeder due to the reception of
    // a profile status which indicates an error
    // condition.

    // This function assumes that the profiles have not
    // stopped the feeder on their own. This function
    // would have to modified to handle the profiles
    // stopping the feeder.

    // This function assumes that jams & profile status
    // errors can use the bLastJamCode variable to signal
    // the error.
    
    if (!bLastJamCode)              // if already jammed, don't overwrite the error.
        bLastJamCode = bStatus;     // use the jam code so the error is reported.
    fnStopFeeder();                 // stop the feeder as normal.
}
static void fnPauseFeederForMaintenance(void)
{
    // TODO: notify the CM if this is not a normal stop
    //if (!fNormal)     
    //  fnSendXXX();

    fFullStop = FALSE;

    fnResetMailFlowTimers();

    fnSetState(FDR_PAUSING_STATE);

    if (fSendStartRsp)
        fnSendStartRsp(MCP_NO_ERROR);   

    // set stop profile flag
    fnMCPSetRemoteFlag(pFeederMcp,MMC_PAUSE_MAIL_FLOW_FLAG,MMC_FLAG_SET);
    fnMCPSetRemoteFlag(pFeederMcp,MMC_START_MAIL_FLOW_FLAG,MMC_FLAG_CLEAR);

    (void)OSStartTimer(FP_FEEDER_STOP_TIMER);
}
/**********************************************************************
fnSetSendStopRspFlag
This function is called in response to a stop feeder request while the
Feeder Task is already in the process of stopping the Feeder.  This
function basically sets a flag so that a stop response message is sent.

Scope
Private

Prototype
static void fnSetSendStopRspFlag(const INTERTASK *pMsg);

Pre-Conditions
None.

Inputs
pMsg - Pointer to an Intertask Message.
MODIFICATION HISTORY:
    11/13/2012  Bob Li      Updated the current Feeder Stop type in time for fixing fraca 219120.  
**********************************************************************/
static void fnSetSendStopRspFlag(const INTERTASK *pMsg)
{
    unsigned char *pPtr = NULL;
    unsigned long lwLength = 0;

    GetIntertaskPointerAndLength((void**)&pPtr,&lwLength,pMsg); //lint !e605

    FDRTASK_ASSERT(pPtr);       //lint !e506

    if (pPtr)
    {
        eCurrStopType = pPtr[0];
    }

    fSendStopRsp = TRUE;
}

/**********************************************************************
fnFeederStopped
This function handles the MMC_FEEDER_PAUSED Profile Status message
 while the Feeder Task is in the mail run states.  This Profile Status
 indicates that the Feeder MCP Profiles have stopped the mail flow.

Scope
Private

Prototype
static void fnFeederStopped(void);

Pre-Conditions
Feeder Task is in the stopping state.

Inputs
None

Processing

Un-hook the unsolicited msg handler
Cancel the Profiles
Reset all Profile Flags
Reset the Feeder MCP instance
SEND Stop Response Msg
Clear Feeder Task Flags

**********************************************************************/
static void fnFeederStopped(void)
{
    unsigned long lwState = fnGetState();
    BOOL fForceStop = FALSE;

    FDRTASK_ASSERT(lwState==FDR_STOPPING_STATE||lwState == FDR_PAUSING_STATE);  //lint !e506

    (void)OSStopTimer(FP_FEEDER_STOP_TIMER);
    (void)OSStopTimer(FP_FEEDER_START_TIMER);
    fnResetMailFlowTimers();

    if (bLastJamCode)
    {       // TODO: clear bLastFdrMeterErrCode in the clear error msg processor
        fSendMailJobComplete = TRUE;
        bLastFdrMeterErrCode = bLastJamCode;    
        fnSendMailJobComplete(FDR_JAM_STATUS,bLastJamCode);
        bLastJamCode = 0;
        fSendStartRsp = FALSE;
        fSendStopRsp = FALSE;
        fForceStop = TRUE;      
    }
    else
        fSendMailJobComplete = TRUE;

    if (eSysOpMode == CM_DIFF_WEIGH_MODE && !fForceStop)
    {
        fnDiffWeighRunStopped();
    }
    else if (lwState == FDR_STOPPING_STATE)
    {
        // clear the unsolicited msg callback
        fnMCPSetUnsolicitedMsgCb(pFeederMcp, NULL);

        // cancel currently running profiles
        fnMCPCancelGroup(pFeederMcp,MMC_ENVELOPE_PROFILE_GROUP);

        // reset the profile flags
        fnMCPResetAllFlags(pFeederMcp);

        // reset the MCP instance
        (void)fnMCPLibReset(pFeederMcp);

        // goto idle state
        fnSetState(FDR_IDLE_STATE);

        // send stop rsp
        if (fSendStopRsp)
            fnSendStopRsp(MCP_NO_ERROR);
        else
            fnSendMailJobComplete(MCP_NO_ERROR,MCP_NO_ERROR);

        // clear flags
        fSendStartRsp = FALSE;
        fSendStopRsp = FALSE;
        fMailRunInitiated = FALSE;
    }
    else if (lwState == FDR_PAUSING_STATE)
    {
        // set state to paused
        fnSetState(FDR_PAUSED_STATE);

        // send stop rsp
        fnSendStopRsp(MCP_NO_ERROR);

        // handle the paused event
        fnFeederPaused();
    }

    fSendMailJobComplete = FALSE;
}

static void fnFeederPaused(void)
{
    if (fNormalPause)
    {
        fNormalPause = FALSE;
        lwInactiveTimerVal = 0;
        //(void)OSStartTimer(FP_FEEDER_MAIL_INACTIVITY_TIMER);
        fnSendFeederTimeoutPause();
    }
}

static void fnProcessCMPaused(const INTERTASK *pMsg)
{
    (void)OSStartTimer(FP_FEEDER_MAIL_INACTIVITY_TIMER);
}

static void fnSendFeederTimeoutPause(void)
{
    UINT8               rsp[5];
    UINT16  usSensorValue = fnMapCurrentSensorValues();
    
    rsp[0] = FDR_TIMEOUT_PAUSE;
    rsp[1] = 0;
    rsp[2] = 0;
    rsp[3] = (UINT8)usSensorValue;
    rsp[4] = (UINT8)(usSensorValue >> 8);

    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_UNSLCT_STATUS, BYTE_DATA, rsp, sizeof(rsp));
}
static void fnSendFeederRestart(void)
{
    UINT8               rsp[5];
    UINT16  usSensorValue = fnMapCurrentSensorValues();

    rsp[0] = FDR_RESTART_REQ;
    rsp[1] = 0;
    rsp[2] = 0;
    rsp[3] = (UINT8)usSensorValue;
    rsp[4] = (UINT8)(usSensorValue >> 8);

    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_UNSLCT_STATUS, BYTE_DATA, rsp, sizeof(rsp));
}

static void fnSendFeederRezeroing(void)
{
    UINT8               rsp[5];
    UINT16  usSensorValue = fnMapCurrentSensorValues();

    rsp[0] = FDR_WOW_REZEROING;
    rsp[1] = 0;
    rsp[2] = 0;
    rsp[3] = (UINT8)usSensorValue;
    rsp[4] = (UINT8)(usSensorValue >> 8);

    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_UNSLCT_STATUS, BYTE_DATA, rsp, sizeof(rsp));
}

static void fnSendFeederRezeroDone(void)
{
    UINT8               rsp[5];
    UINT16  usSensorValue = fnMapCurrentSensorValues();

    rsp[0] = FDR_WOW_REZERO_DONE;
    rsp[1] = 0;
    rsp[2] = 0;
    rsp[3] = (UINT8)usSensorValue;
    rsp[4] = (UINT8)(usSensorValue >> 8);

    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_UNSLCT_STATUS, BYTE_DATA, rsp, sizeof(rsp));
}

static void fnSendFeederWOWMailPieceDataReady(void)
{
    UINT8               rsp[5];
    UINT16  usSensorValue = fnMapCurrentSensorValues();

    rsp[0] = FDR_WOW_MAIL_PIECE_DATA_READY;
    rsp[1] = 0;
    rsp[2] = 0;
    rsp[3] = (UINT8)usSensorValue;
    rsp[4] = (UINT8)(usSensorValue >> 8);

    //fnDumpStringToSystemLog( "FdrWOW mailpieceReady" );
    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_UNSLCT_STATUS, BYTE_DATA, rsp, sizeof(rsp));
}

static void fnSendFeederError(UINT8 ucBaseError)
{
    UINT8               rsp[5];
    UINT16  usSensorValue = fnMapCurrentSensorValues();

    rsp[0] = FDR_ERROR;
    rsp[1] = 0;
    rsp[2] = ucBaseError;
    rsp[3] = (UINT8)usSensorValue;
    rsp[4] = (UINT8)(usSensorValue >> 8);

    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_UNSLCT_STATUS, BYTE_DATA, rsp, sizeof(rsp));
}

static void fnSendFeederMailPieceAtExit(void)
{
    UINT8               rsp[5];
    UINT16  usSensorValue = fnMapCurrentSensorValues();

    rsp[0] = FDR_MAIL_PIECE_AT_EXIT;
    rsp[1] = 0;
    rsp[2] = 0;
    rsp[3] = (UINT8)usSensorValue;
    rsp[4] = (UINT8)(usSensorValue >> 8);

    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_UNSLCT_STATUS, BYTE_DATA, rsp, sizeof(rsp));
}

/**********************************************************************
fnFeederStoppedSendStopRsp
This function is called in response to a stop feeder request
when the feeder is idle.

Scope
Private

Prototype
static void fnFeederStoppedSendStopRsp(const INTERTASK *pMsg);

Pre-Conditions
pMsg != NULL

Inputs
pMsg - Pointer to an Intertask Message.

Processing

SEND Stop Response Msg
MODIFICATION HISTORY:
    11/13/2012  Bob Li      Updated the current Feeder Stop type in time for fixing fraca 219120.
**********************************************************************/
static void fnFeederStoppedSendStopRsp(const INTERTASK *pMsg)
{
    unsigned char *pPtr = NULL;
    unsigned long lwLength = 0;

    GetIntertaskPointerAndLength((void**)&pPtr,&lwLength,pMsg); //lint !e605

    FDRTASK_ASSERT(pPtr);       //lint !e506

    if (pPtr)
    {
        eCurrStopType = pPtr[0];
    }

    fSendStopRsp = TRUE;
    fnSendStopRsp(MCP_NO_ERROR);
}
/**********************************************************************
fnSendStopRsp
This function sends a Stop Response message.

Scope
Private

Prototype
static void fnSendStopRsp(const unsigned char bStatus)

Inputs
bStatus - Status indicating whether the stop was executed successfully
or not.

**********************************************************************/
static void fnSendStopRsp(const unsigned char bStatus)
{
    uchar               rsp[4];

    FDRTASK_ASSERT(fSendStopRsp);   //lint !e506

#ifdef RD_DM400_PRINTDBG
    // set status to OK and stop type to normal, play games to get around const
    *(unsigned char *)&bStatus = 0 ;
    eCurrStopType = 0 ;
#endif

    if (fSendStopRsp)
    {
        rsp[0] = bStatus == MCP_NO_ERROR ? 0 : FDR_MISC_ERR;
        rsp[1] = bStatus;
        rsp[2] = (UINT8)eCurrStopType;

        if (bStatus != MCP_NO_ERROR)
        {
            bLastFdrMeterErrCode = bStatus;
            fnLogSystemError(ERROR_CLASS_FP_FEEDER, 0xF3, FDR_MISC_ERR, bStatus, 0);
            (void)sprintf(bFdrDbgMsgBuff,">FDR MERR(%02X %02X %02X %02X) %d<", 0xF3, FDR_MISC_ERR, bStatus, 0, __LINE__);
            fnFdrLogMsg(bFdrDbgMsgBuff);
        }

        (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_STOP_RUN_RSP, BYTE_DATA, rsp, sizeof(rsp));
        fSendStopRsp = FALSE;
    }
}
static void fnSendMailJobComplete(const unsigned char bStatus, const unsigned char bError)
{
    UINT8               rsp[5];
    UINT16  usSensorValue = fnMapCurrentSensorValues();

    if (fSendMailJobComplete)
    {
        fSendMailJobComplete = FALSE;
        rsp[0] = FDR_MAIL_JOB_COMPLETE;
        rsp[1] = bStatus;
        rsp[2] = bError;
        rsp[3] = (UINT8)usSensorValue;
        rsp[4] = (UINT8)(usSensorValue >> 8);

        (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_UNSLCT_STATUS, BYTE_DATA, rsp, sizeof(rsp));
    }
}

/**********************************************************************
fnFeederQuerySensorTimerExpire
This is an OS timer expiry function.  It is called when the Feeder
Sensor Poll Interval has expired.  The function simply sends an
intertask message to the Feeder Task to itself.  The purpose of this
intertask message is to serialize the Feeder Task processing.

Scope
Public

Prototype
void fnFeederQuerySensorTimerExpire(unsigned long lwTimerID)

Inputs
lwTimerID - ID of the timer. (unused - necessary for OS compatiblity)

Processing

SEND Intertask message to Feeder Task from itself

**********************************************************************/
void fnFeederQuerySensorTimerExpire(unsigned long lwTimerID)
{
    if( fFdrPaused )
    {
        // The feeder task is in a loop, not rx'ing messages, so don't keep sending them.
        // Stop the timer so these timeout messages don't clog the fdrtask's msg queue
        fnStopQuerySensorTimer();
        fnDumpStringToSystemLog( "FDRTSK Pause Fdr QuerySensor timer." );

        // Instead, send a message to the Fdr task to start the timer again when it gets this message.
        (void)fnSendIntertask( FDRMCPTASK, FDRMCPTASK, FDR_RESTART_POLL_SENSOR_TIMER, NO_DATA, NULL, 0 );
        // Then fall through to send the message to get the sensor data.  This message will
        //  be processed when we unpause, if it is received in the correct state.
    }
    
    // Send the feeder task a msg to poll the sensors again.
    (void)fnSendIntertask( FDRMCPTASK,FDRMCPTASK,FDR_POLL_SENSOR_TIMEOUT,NO_DATA,NULL,0);

}

/**********************************************************************
fnProcessIdleStatePollSensorReq
This function is called when the Feeder Task determines that it
should poll the Feeder Sensors in the idle state.  This function
is called after the Sensor Poll Timeout Timer expires and the
Feeder Task receives a FDR_POLL_SENSOR_TIMEOUT message from iteself.

Scope
Private

Prototype
static void fnProcessIdleStatePollSensorReq(const INTERTASK *pMsg);

Inputs
pMsg - Pointer to an intertask message.

Processing

Query the Feeder Sensors

**********************************************************************/
static void fnProcessIdleStatePollSensorReq(const INTERTASK *pMsg)
{
    unsigned long lwState = fnGetState();
    unsigned char motorEncoderDiag[MMC_ENCODER_DIAG_REQ_MSG_LEN];

    if (lwState != FDR_IDLE_STATE && lwState != FDR_DIAG_STATE) // make sure that we only deal with sensors in the idle state
        return; 

    FDRTASK_ASSERT(pMsg);   //lint !e506

    if (pMsg)
    {
        if (lwState == FDR_IDLE_STATE)
            // request the sensors status
            fnMCPSendSimpleRequest(pFeederMcp,
                                    MMC_QUERY_SENSORS_REQ,
                                    MMC_SENSOR_STATE_RSP,
                                    fnQuerySensorRspCb);
        else if (lwState == FDR_DIAG_STATE)
        {
/*
            switch (bLastFeederDiag) {
            case FDR_DIAG_READ_SENSORS:
                // request the sensors status
                fnMCPSendSimpleRequest(pFeederMcp,
                                        MMC_QUERY_SENSORS_REQ,
                                        MMC_SENSOR_STATE_RSP,
                                        fnQuerySensorRspCb);
                break;
            case FDR_DIAG_READ_ENCODER:          
            case WOW_DIAG_MOTOR_READ_ENCODER:            
                // load the header information for the speed test
                motorEncoderDiag[MMC_DIAG_TYPE_OFFSET] = MMC_RETRIEVE_ENCODER_COUNT;
                motorEncoderDiag[MMC_DIAG_MOTOR_ID_OFFSET] = ucCurrentDiagMotorID;

                fnMCPSendRequest(pFeederMcp,
                                 motorEncoderDiag, MMC_ENCODER_DIAG_REQ_MSG_LEN,
                                 MMC_START_DIAGNOSTIC_REQ,
                                 MMC_DIAGNOSTIC_STATUS_RSP,
                                 fnQueryEncoderRspCb);
                break;
            };
*/
        }
    }
}

/******************************************************************************
FUNCTION:           fnProcessPollSensorStartupReq
DESCRIPTION:        
    If the poll-sensor timer expires whle the feeder task is paused in a loop,
    it sends this message and stops the timer so it won't fill up the fdr 
    task's IT msg queue. 
    If we are processing this message, then the fdr task is not paused in
    the loop anymore, so it can poll the sensors and restart the timer.
INPUTS:
    pMsg - Pointer to the intertask message being processed.
Scope:  
    Private
Processing:
    Query the Feeder Sensors and start the poll-sensor timer.
HISTORY:
 2010.07.13 Clarisa Bellamy - Created.
------------------------------------------------------------------------------*/
static void fnProcessPollSensorStartupReq( const INTERTASK *pMsg )
{
    // Restart the timer.
    fnStartQuerySensorTimer();
    fnDumpStringToSystemLog( "FDRTSK Restart QuerySensor timer" ); 
}



static void fnProcessSensorValuesDiagState(const UINT32 lwSensorValues)
{
    UINT8 bRsp[FDR_DIAG_RSP_DATA_OFFSET+sizeof(UINT32)] = {0};
    UINT8  ucSensorValue = fnDiagMapCurrentSensorValues();

    bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
    bRsp[FDR_DIAG_OPERATION_OFFSET] = FDR_DIAG_READ_SENSORS;
    bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
    bRsp[FDR_DIAG_RSP_DATA_OFFSET]= ucSensorValue;

    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
}
static void fnProcessEncoderValueDiagState(const UINT16 lwEncoderValue)
{
    UINT8 bRsp[FDR_DIAG_RSP_DATA_OFFSET+sizeof(UINT16)] = {0};

    bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
//    bRsp[FDR_DIAG_OPERATION_OFFSET] = bLastFeederDiag;
    bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
    (void)memcpy((void*)&bRsp[FDR_DIAG_RSP_DATA_OFFSET],&lwEncoderValue,sizeof(lwEncoderValue));

    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
}
static void fnProcessSensorValuesIdleState(const UINT32 lwSensorValues)
{
    uchar               rsp[12] = {0};
    unsigned long lwPrevCvrState=0;
    unsigned long lwCurrCvrState=0;
    UINT16  usSensorValue = fnMapCurrentSensorValues();
    UINT8   ucMode = fnOITGetPrintMode();

    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    switch ( fnGetMeterModel() )
    {
        case CSD3:
            lwPrevCvrState = lwLastFeederSensorState & FDR_CVR_SNSR_BIT;
            lwCurrCvrState = lwSensorValues & FDR_CVR_SNSR_BIT;
            break;
            
        default:
            break;
    }

	//Check sensor status per meter type
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
	if ( ( (lwPrevCvrState &  FDR_CVR_SNSR_BIT) != (lwCurrCvrState & FDR_CVR_SNSR_BIT) )  && (fnGetMeterModel() == CSD3) )
    {
        // special cover sensor state change
        if (lwCurrCvrState)
            rsp[0] = FDR_COVER_OPENED;//FDR_COVER_CLOSED;
        else
            rsp[0] = FDR_COVER_CLOSED;//FDR_COVER_OPENED;
        rsp[1] = FDR_OK;
        rsp[2] = FDR_OK;
        rsp[3] = (UINT8)usSensorValue;
        rsp[4] = (UINT8)(usSensorValue >> 8);
        (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_UNSLCT_STATUS, BYTE_DATA, rsp, sizeof(rsp));
    }
    else if(lwPrevCvrState != lwCurrCvrState)
    {
        // special sensor state change
        rsp[0] = FDR_SENSOR_STATUS;
        rsp[1] = FDR_OK;
        rsp[2] = FDR_OK;
        rsp[3] = (UINT8)usSensorValue;
        rsp[4] = (UINT8)(usSensorValue >> 8);
        (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_UNSLCT_STATUS, BYTE_DATA, rsp, sizeof(rsp));
    }
    else
    {
        // generic sensor state change
    }
}
/**********************************************************************
fnQuerySensorRspCb
This function is called when the responds to a sensor query in the
idle state.  The response is in the form of a MMC_SENSOR_STATE_RSP
motion class message.

Scope
Private

Prototype
static void fnQuerySensorRspCb(const void *pMcp, 
                            const unsigned long lwStatus, 
                            const void *pData, 
                            const unsigned long lwDataLength);

Inputs
pMcp - Pointer to the instance of the MCP that has sent the message.
lwStatus - Status of the message.  Indicates if the message has
    been received succesfully or not.
pData - Pointer to the Motion Class Message.
lwDataLength - Length of the Motion Class Message.

Processing

SAVE the Sensor Values
IF the Sensor Values have changed THEN
  SEND an Unsolicited Status message
ENDIF

**********************************************************************/
static void fnQuerySensorRspCb(const void *pMcp, const unsigned long lwStatus, const void *pData, const unsigned long lwDataLength)
{
    UINT8 *pPtr = NULL;
    if (lwStatus == MCP_LIB_SUCCESS)
    {
        unsigned long lwState = fnGetState();

        if (lwState != FDR_IDLE_STATE && lwState != FDR_DIAG_STATE) // make sure that we only deal with sensors in the idle state
            return; 

        FDRTASK_ASSERT(pData && lwDataLength > MMC_MSG_HEADER_LEN); //lint !e506

        if (pData && lwDataLength > MMC_MSG_HEADER_LEN)
        {
            unsigned long lwSensorVal;
            pPtr = (UINT8*)pData;
            // store the current sensor values
            (void)memcpy((void*)&lwSensorVal,&pPtr[MMC_MSG_HEADER_LEN],sizeof(lwSensorVal));    //lint !e718 !e746
            // DM400c
            lwSensorVal &= FDR_SENSOR_MASK; // filter out some bits, like the floating S1 sensor bit
            lwFeederSensorState = lwSensorVal;
            if (lwFeederSensorState != lwLastFeederSensorState)
            {
                // tell CM the sensor state changed
                //fnSendUncolicitedStatus(); // removed temporarily because of suspicions that it causes a weird CM/SYS/OIT interaction related to disabling modes???
/*                if (lwState == FDR_DIAG_STATE && bLastFeederDiag == FDR_DIAG_READ_SENSORS)
                    fnProcessSensorValuesDiagState(lwFeederSensorState);
                else if (lwState == FDR_IDLE_STATE)
                    fnProcessSensorValuesIdleState(lwFeederSensorState);
                lwLastFeederSensorState = lwFeederSensorState;                  
 */           }
        }
    }       
}
static void fnQueryEncoderRspCb(const void *pMcp, const unsigned long lwStatus, const void *pData, const unsigned long lwDataLength)
{
    static UINT16 wLastEncoderValue = 0;
    UINT16 wEncoderValue = 0;
    UINT8 *pPtr = NULL;
    if (lwStatus == MCP_LIB_SUCCESS)
    {
        unsigned long lwState = fnGetState();

        if (lwState != FDR_IDLE_STATE && lwState != FDR_DIAG_STATE) // make sure that we only deal with sensors in the idle state
            return; 

        FDRTASK_ASSERT(pData && lwDataLength > MMC_MSG_HEADER_LEN); //lint !e506

        if (pData && lwDataLength > MMC_MSG_HEADER_LEN)
        {
            pPtr = (UINT8*)pData;
            pPtr += MMC_MSG_HEADER_LEN;
            // determine if an error occurred
            if (pPtr[MMC_DIAG_RSP_ERROR_CODE_OFFSET] == MMC_DIAG_RSP_NO_ERROR)
            {
                memcpy(&wEncoderValue,
                        &pPtr[MMC_ENCODER_COUNT_VALUE], sizeof(wEncoderValue));
            }
            if (wEncoderValue != wLastEncoderValue)
            {
                if (lwState == FDR_DIAG_STATE && 
                        (bLastFeederDiag == FDR_DIAG_READ_ENCODER ||
                          bLastFeederDiag == WOW_DIAG_MOTOR_READ_ENCODER))
                    fnProcessEncoderValueDiagState(wEncoderValue);              
                wLastEncoderValue = wEncoderValue;
            }
        }
    }   
}

/**********************************************************************
fnProcessGetStatusReq
This function processes a request for status.  The function sends
a status response message.

Scope
Private

**********************************************************************/
static void fnProcessGetStatusReq(const INTERTASK *pMsg)
{
    FDRTASK_ASSERT(pMsg);   //lint !e506

    if (pMsg)
    {
        fnSendStatusRsp();      
    }
}

/**********************************************************************
fnMapCurrentSensorValues
This function maps the Feeder's sensor values to the sensor values
specified in the status messages

Scope
Private

**********************************************************************/
static UINT16 fnMapCurrentSensorValues(void)
{
    UINT16  usSensorValue = 0;

#if !defined (MIDJET_RIG_TESTING)   // for now ignore the sensors...
    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    switch ( fnGetMeterModel() )
    {
        case CSD3:
            if (lwFeederSensorState & FDR_CVR_SNSR_BIT)
                usSensorValue |= FTOPCVR_MASK;
            if (lwFeederSensorState & FDR_FS2_SNSR_BIT)
                usSensorValue |= FS2_MASK;
            if (lwFeederSensorState & FDR_FS1_SNSR_BIT)
                usSensorValue |= FS1_MASK;
            break;
            
        default:
            break;
    }
#endif
    return usSensorValue;
}

/**********************************************************************
fnDiagMapCurrentSensorValues
In diagnostic mode, use this function to map the Feeder's sensor values
to the sensor values specified in the status messages

Scope
Private

**********************************************************************/
static UINT8 fnDiagMapCurrentSensorValues(void)
{
    UINT8   ucSensorValue = 0;

    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    switch ( fnGetMeterModel() )
    {
        case CSD3:
            if (lwFeederSensorState & FDR_CVR_SNSR_BIT)
                ucSensorValue |= DIAG_FTOPCVR_MASK;
            if (lwFeederSensorState & FDR_FS2_SNSR_BIT)
                ucSensorValue |= DIAG_FS2_MASK;
            if (lwFeederSensorState & FDR_FS1_SNSR_BIT)
                ucSensorValue |= DIAG_FS1_MASK;
            break;
            
        default:
            break;
    }
    
    return ucSensorValue;
}

/**********************************************************************
fnMapRunningState
This function maps the Feeder Task's state to the "Running Status" 
specified in the status messages

Scope
Private

**********************************************************************/
static unsigned char fnMapRunningState(void)
{
    unsigned char bRunningState = 0x20;
    unsigned long lwState = fnGetState();

    switch (lwState) {
    case FDR_STARTING_STATE:
    case FDR_STOPPING_STATE:
    case FDR_RUNNING_STATE:
    case FDR_PAUSING_STATE:
    case FDR_PAUSED_STATE:
    case FDR_DIFF_WEIGH_WAIT_PIECE_STATE:
        bRunningState = 0x10;
        break;
    default:
        bRunningState = 0x20;
        break;
    };

    return bRunningState;
}

/**********************************************************************
fnSendUncolicitedStatus
This function sends unsolicited status message.

Scope
Private

Prototype
static void fnSendUncolicitedStatus(void)

**********************************************************************/
static void fnSendUncolicitedStatus(void)
{
    uchar               rsp[12];
    UINT16  usSensorValue = fnMapCurrentSensorValues();

    (void)memset((void*)rsp,0,sizeof(rsp)); //lint !e718 !e746
    rsp[0] = 0;
    rsp[1] = MCP_NO_ERROR;
    rsp[2] = MCP_NO_ERROR;
    rsp[3] = (UINT8)usSensorValue;
    rsp[4] = (UINT8)(usSensorValue >> 8);

    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_UNSLCT_STATUS, BYTE_DATA, rsp, sizeof(rsp));
}

static void fnDiagHandleEncoderCountRequest(UINT8 ucMotorId)
{
    UINT8 bRsp[FDR_DIAG_RSP_STATUS_OFFSET+1] = {0};

    // save the current Motor ID
    ucCurrentDiagMotorID  = ucMotorId;

    bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_START;
//    bRsp[FDR_DIAG_OPERATION_OFFSET] = bLastFeederDiag;
    bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;

    (void)OSSendIntertask(_CMTASK_,FDRMCPTASK,FC_DIAG_CMD_RSP,BYTE_DATA,bRsp,sizeof(bRsp));
    (void)OSChangeTimerDuration(FP_FEEDER_QUERY_SENSOR_TIMER,FEEDER_DIAG_SENSOR_POLLING_INTERVAL);     
    (void)OSStartTimer(FP_FEEDER_QUERY_SENSOR_TIMER);
}

static void fnDiagWOWRezero()
{
    UINT8 bDiagInfoBuff[2];

    bDiagInfoBuff[0] = WOW_DIAG_REZERO_COMMAND;
    bDiagInfoBuff[1] =0;
    fnMCPSendRequest (pFeederMcp,
                        bDiagInfoBuff,
                        2,
                        MMC_START_DIAGNOSTIC_REQ,
                        MMC_DIAGNOSTIC_STATUS_RSP, fnDiagStatusRspCb);

}

static void fnDiagWOWCalibrateZero()
{
    UINT8 bDiagInfoBuff[2];

    bDiagInfoBuff[0] = WOW_DIAG_CALIBRATE_ZERO_COMMAND;
    bDiagInfoBuff[1] =0;
    fnMCPSendRequest (pFeederMcp,
                        bDiagInfoBuff,
                        2,
                        MMC_START_DIAGNOSTIC_REQ,
                        MMC_DIAGNOSTIC_STATUS_RSP, fnDiagStatusRspCb);
}

static void fnDiagWOWCalibrateFull()
{
    UINT8 bDiagInfoBuff[2];

    bDiagInfoBuff[0] = WOW_DIAG_CALIBRATE_FULL_COMMAND;
//    bDiagInfoBuff[1] =(UINT8) fnRateGetWeightUnit();
    fnMCPSendRequest (pFeederMcp,
                        bDiagInfoBuff,
                        2,
                        MMC_START_DIAGNOSTIC_REQ,
                        MMC_DIAGNOSTIC_STATUS_RSP, fnDiagStatusRspCb);
}

static void fnDiagWOWStaticWeight()
{
    UINT8 bDiagInfoBuff[2];

    bDiagInfoBuff[0] = WOW_DIAG_STATIC_WEIGH_COMMAND;
//    bDiagInfoBuff[1] =(UINT8) fnRateGetWeightUnit();
    fnMCPSendRequest (pFeederMcp,
                        bDiagInfoBuff,
                        2,
                        MMC_START_DIAGNOSTIC_REQ,
                        MMC_DIAGNOSTIC_STATUS_RSP, fnDiagStatusRspCb);
}

static void fnDiagWOWSetMode(UINT8 bParam)
{
    UINT8   bWeighMode[2];
    bWeighMode[0] = bParam;
    bWeighMode[1] = 1;  // make this permanent change - update wow NVM

    fnMCPSendRequest (pFeederMcp,
                        bWeighMode,
                        2,
                        MMC_SET_WEIGHING_MODE_REQ,
                        MMC_NO_RESPONSE, NULL);

    fnSendWOWSetModeRsp();
}

static void fnDiagWOWRetrieveParams(UINT8 bParam)
{
    bLastWowNvmRequestType = bParam;
    
    switch(bParam)
    {
        case WOW_PRECISION:
        case WOW_CALIBRATION_CONSTANTS:
        case WOW_WIDTH_SENSOR_CAL_VAL:
            fnMCPSendSimpleRequest (pFeederMcp,                      
                                MMC_NVM_MACHINE_INFO_RECORD_REQ,
                                MMC_NVM_MACHINE_INFO_RECORD_RSP, fnDiagMcpNVMInfoRspCb);
            break;
            
        case SERIAL_NUMBER:
            fnMCPGetSerialNumber();
        break;

        case WOW_LENGTH_CALIB_CLEAR:
            fnMCPSendSimpleRequest (pFeederMcp,                      
                                    MMC_NVM_MACHINE_INFO_RECORD_REQ,
                                    MMC_NVM_MACHINE_INFO_RECORD_RSP, fnDiagMcpNVMInfoRspCb);

            break;

        default:
            break;

    }

}

static void fnDiagWOWRequestDRThicknessSensor()
{
    UINT8 bDiagInfoBuff[2];

    bDiagInfoBuff[0] = WOW_READ_THICKNESS_ENCODER_COMMAND;
    bDiagInfoBuff[1] = WOW_DIAG_READ_SUBCOMMAND;
    fnMCPSendRequest (pFeederMcp,
                        bDiagInfoBuff,
                        2,
                        MMC_START_DIAGNOSTIC_REQ,
                        MMC_DIAGNOSTIC_STATUS_RSP, fnDiagStatusRspCb);
}

static void fnDiagWOWRequestDRWidthSensor()
{
    UINT8 bDiagInfoBuff[2];

    bDiagInfoBuff[0] = WOW_READ_WIDTH_SENSOR_COMMAND;
    bDiagInfoBuff[1] = WOW_DIAG_READ_SUBCOMMAND;
    fnMCPSendRequest (pFeederMcp,
                        bDiagInfoBuff,
                        2,
                        MMC_START_DIAGNOSTIC_REQ,
                        MMC_DIAGNOSTIC_STATUS_RSP, fnDiagStatusRspCb);
}

static void fnDiagWOWRezeroDRThicknessEncoder()
{
    UINT8 bDiagInfoBuff[2];
    
    fDRPauseSensorPolling = TRUE;

    bDiagInfoBuff[0] = WOW_READ_THICKNESS_ENCODER_COMMAND;
    bDiagInfoBuff[1] = WOW_DIAG_REZERO_SUBCOMMAND;
    fnMCPSendRequest (pFeederMcp,
                        bDiagInfoBuff,
                        2,
                        MMC_START_DIAGNOSTIC_REQ,
                        MMC_DIAGNOSTIC_STATUS_RSP, fnDiagStatusRspCb);

}

static void fnDiagWOWRezeroDRWidthSensor()
{
    UINT8 bDiagInfoBuff[2];

    fDRPauseSensorPolling = TRUE;
    
    bDiagInfoBuff[0] = WOW_READ_WIDTH_SENSOR_COMMAND;
    bDiagInfoBuff[1] = WOW_DIAG_REZERO_SUBCOMMAND;
    fnMCPSendRequest (pFeederMcp,
                        bDiagInfoBuff,
                        2,
                        MMC_START_DIAGNOSTIC_REQ,
                        MMC_DIAGNOSTIC_STATUS_RSP, fnDiagStatusRspCb);
}

static void fnDiagWOWReadPCNInfo()
{
    static UINT8 bPCNRsp[MCP_PCN_LENGTH + 4];
    MCP_PCN_REC *pMcpPCNInfo;
    memset(&bPCNRsp, 0, sizeof(bPCNRsp));
    pMcpPCNInfo = fnMCPGetCurrMcpPCNInfo(pFeederMcp);

    if(pMcpPCNInfo != NULL)
    {
        bPCNRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
        bPCNRsp[FDR_DIAG_OPERATION_OFFSET] = WOW_DIAG_READ_PCN_INFO;
        bPCNRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
        memcpy( &bPCNRsp[FDR_DIAG_RSP_DATA_OFFSET + 1], pMcpPCNInfo,  sizeof(MCP_PCN_REC));
        
        (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, GLOBAL_PTR_DATA, bPCNRsp, sizeof(bPCNRsp));

    }
    else
    {
        bPCNRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
        bPCNRsp[FDR_DIAG_OPERATION_OFFSET] = WOW_DIAG_READ_PCN_INFO;
        bPCNRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
        bPCNRsp[FDR_DIAG_RSP_DATA_OFFSET + 1] = 0;
        
        (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, GLOBAL_PTR_DATA, bPCNRsp, sizeof(bPCNRsp));
    }
}
static void fnDiagWOWReadCycleCount()
{
    fnMCPSendSimpleRequest( pFeederMcp, 
                            MMC_PIECE_COUNT_REQ, 
                            MMC_PIECE_COUNT_RSP, 
                            fnHandleWOWCycleCountRsp);
}
static void fnDiagWOWReadJamCount()
{
    fnMCPSendSimpleRequest( pFeederMcp, 
                            MMC_JAM_COUNTERS_REQ, 
                            MMC_JAM_COUNTERS_RSP, 
                            fnHandleWOWJamCountRsp);
}
static void fnDiagWOWResetJamCount()
{
    static WOW_JAM_COUNT_DATA wowJamCounters;
    // clear the jam counter data
    memset(&wowJamCounters, 0, sizeof(wowJamCounters));
    // issue the request to 'set' (i.e., clear) the Jam Counters; no response
    // is expected
    fnMCPSendRequest (pFeederMcp,
    (unsigned char *)&wowJamCounters, sizeof(wowJamCounters),
    MMC_JAM_COUNTERS_RSP,
    MMC_NO_RESPONSE, NULL);
}

static void fnSendWOWSetModeRsp ()
{
    UINT8 bRsp[4]; 
    
    bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
    bRsp[FDR_DIAG_OPERATION_OFFSET] = FDR_DIAG_WOW_SET_MODE;
    bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
    bRsp[FDR_DIAG_RSP_DATA_OFFSET]= TRUE;//means success
    
    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
}

static void fnDiagMcpNVMInfoRspCb(const void *pMcp, const UINT32 lwStatus, const void *pData, UINT32 lwDataLength)
{
    UINT8  tmpRecSz;
    UINT8   bRspConstant[10], bRspPrecision[5];  
    
    /* we've already verified the message's Class Code, the Command ID, etc.,
    * so we can simply bypass the header, and extract the data
    */
    UINT8 *pPtr = NULL;
    if (lwStatus == MCP_LIB_SUCCESS)
    {
        UINT32  lwState = fnGetState();

        if ( lwState != FDR_DIAG_STATE) // make sure that we only deal with it in the idle state
            return; 

        FDRTASK_ASSERT(pData && lwDataLength > MMC_MSG_HEADER_LEN); //lint !e506

        if (pData && lwDataLength > MMC_MSG_HEADER_LEN)
        {
            pPtr=pData;

            pPtr += MMC_MSG_HEADER_LEN;
            lwDataLength -= MMC_MSG_HEADER_LEN;

            // save the data into a local structure
            tmpRecSz = sizeof(MMC_WOW_NVM_MACHINE_INFO);

            memcpy(&wowNvmMachineInfo, &pPtr[0], tmpRecSz);

            switch (bLastWowNvmRequestType) 
            {
                case WOW_PRECISION:        
                    bRspPrecision [FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
                    bRspPrecision [FDR_DIAG_OPERATION_OFFSET] = FDR_DIAG_WOW_RETRIEVE_PARAMS;
                    bRspPrecision [FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
                    bRspPrecision [FDR_DIAG_RSP_DATA_OFFSET] = bLastWowNvmRequestType;
                    bRspPrecision [FDR_DIAG_RSP_DATA_OFFSET + 1] = wowNvmMachineInfo.wowPercisionMode;
                    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRspPrecision, sizeof(bRspPrecision));
                    break;
                case WOW_CALIBRATION_CONSTANTS:
                    bRspConstant [FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
                    bRspConstant [FDR_DIAG_OPERATION_OFFSET] = FDR_DIAG_WOW_RETRIEVE_PARAMS;
                    bRspConstant [FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
                    bRspConstant [FDR_DIAG_RSP_DATA_OFFSET] = bLastWowNvmRequestType;
                    memcpy((void*)& bRspConstant [FDR_DIAG_RSP_DATA_OFFSET + 1],&wowNvmMachineInfo.wowCalibrationZeroCounts[1],3);
                    memcpy((void*)& bRspConstant[FDR_DIAG_RSP_DATA_OFFSET + 4],&wowNvmMachineInfo.wowCalibrationSpanCounts[1],3);
                    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRspConstant, sizeof(bRspConstant));
                    break;
                case WOW_LENGTH_CALIB_CLEAR:
                    wowNvmMachineInfo.lenCalibrated = 0;      //Reset flag to Calibration
                    // issue the request to 'set' (i.e., clear) the NVM Machine Info Cycle
                    // Counters; no response is expected
                    fnMCPSendRequest (pFeederMcp,
                                    (UINT8 *) &wowNvmMachineInfo,
                                    tmpRecSz,
                                    MMC_NVM_MACHINE_INFO_RECORD_RSP,
                                    MMC_NO_RESPONSE, NULL);

                    // give the wow mcp some time to write the data to nvm
                    (void)OSWakeAfter(100);
                    break;
                case WOW_WIDTH_SENSOR_CAL_VAL:        
                    bRspPrecision [FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
                    bRspPrecision [FDR_DIAG_OPERATION_OFFSET] = FDR_DIAG_WOW_RETRIEVE_PARAMS;
                    bRspPrecision [FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
                    bRspPrecision [FDR_DIAG_RSP_DATA_OFFSET] = bLastWowNvmRequestType;
                    bRspPrecision [FDR_DIAG_RSP_DATA_OFFSET + 1] = wowNvmMachineInfo.widthSensitivity;
                    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRspPrecision, sizeof(bRspPrecision));

                    // inform the OIT that the length calib has been done
                    (void)OSSetEvents(FDR_WOW_DR_EVENT_GROUP, WOW_DR_WIDTH_SENSITIVITY_RECV_EVENT, OS_OR);

                    break;
                    
                default:
                    break;

            }
        }
    }
}

static void fnHandleWOWCycleCountRsp(const void *pMcp, const UINT32 lwStatus, const void *pData, UINT32 lwDataLength)
{
    static UINT8   bRspCycleCount[25];  
    MMC_WOW_NVM_MACHINE_INFO *pWOWNVMInfo;
    UINT8 *pPtr = NULL;
    memset(&bRspCycleCount, 0, sizeof(bRspCycleCount));
    if(fnMCPGetCurrMcpNVMMachineInfo(MCP_NVM_ALL, pFeederMcp, (void **)&pWOWNVMInfo) != MCP_LIB_SUCCESS)
    {
        pWOWNVMInfo = NULL;
    }
    /* we've already verified the message's Class Code, the Command ID, etc.,
    * so we can simply bypass the header, and extract the data
    */
    if (lwStatus == MCP_LIB_SUCCESS)
    {
        UINT32  lwState = fnGetState();

        // make sure that we only deal with it in the diag/idle state
        if ( (lwState != FDR_DIAG_STATE)&& (lwState != FDR_IDLE_STATE) )
        {
            return; 
        }
    
        FDRTASK_ASSERT(pData && lwDataLength > MMC_MSG_HEADER_LEN); //lint !e506

        if (pData && lwDataLength > MMC_MSG_HEADER_LEN)
        {
            pPtr=pData;

            pPtr += MMC_MSG_HEADER_LEN;
            lwDataLength -= MMC_MSG_HEADER_LEN;

            bRspCycleCount [FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
            bRspCycleCount [FDR_DIAG_OPERATION_OFFSET] = WOW_DIAG_READ_CYCLE_COUNT;
            bRspCycleCount [FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
            memcpy(&bRspCycleCount [FDR_DIAG_RSP_DATA_OFFSET+1], pPtr, sizeof(MMC_WOW_NVM_CYCLE_COUNT_DATA));
            //copy nvm info for power cycle/watchdog reboot/reset counter
            if(pWOWNVMInfo != NULL )
            {
                memcpy( &bRspCycleCount [FDR_DIAG_RSP_DATA_OFFSET+13], &(pWOWNVMInfo->wowPowerCycles), 2);
                memcpy( &bRspCycleCount [FDR_DIAG_RSP_DATA_OFFSET+15], &(pWOWNVMInfo->wowWatchDogTimerReboots),  2);
                memcpy( &bRspCycleCount [FDR_DIAG_RSP_DATA_OFFSET+17], &(pWOWNVMInfo->wowResetCounter),  2);
            }
            (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, GLOBAL_PTR_DATA, bRspCycleCount, sizeof(bRspCycleCount));
        }
    }

}

static void fnHandleWOWJamCountRsp(const void *pMcp, const UINT32 lwStatus, const void *pData, UINT32 lwDataLength)
{
    static UINT8   bRspJamCount[30];  
    
    /* we've already verified the message's Class Code, the Command ID, etc.,
    * so we can simply bypass the header, and extract the data
    */
    UINT8 *pPtr = NULL;
    memset(&bRspJamCount, 0, sizeof(bRspJamCount));
    if (lwStatus == MCP_LIB_SUCCESS)
    {
        UINT32  lwState = fnGetState();

        // make sure that we only deal with it in the diag/idle state
        if ( (lwState != FDR_DIAG_STATE)&& (lwState != FDR_IDLE_STATE) )
        {
            return; 
        }
    
        FDRTASK_ASSERT(pData && lwDataLength > MMC_MSG_HEADER_LEN); //lint !e506

        if (pData && lwDataLength > MMC_MSG_HEADER_LEN)
        {
            pPtr=pData;

            pPtr += MMC_MSG_HEADER_LEN;
            lwDataLength -= MMC_MSG_HEADER_LEN;

            bRspJamCount [FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
            bRspJamCount [FDR_DIAG_OPERATION_OFFSET] = WOW_DIAG_READ_JAM_COUNT;
            bRspJamCount [FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
            memcpy(&bRspJamCount [FDR_DIAG_RSP_DATA_OFFSET], pPtr, sizeof(WOW_JAM_COUNT_DATA));
            
            (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, GLOBAL_PTR_DATA, bRspJamCount, sizeof(bRspJamCount));
        }
    }

}

static void fnMCPGetSerialNumber()
{
    UINT8 bRsp[MMC_SERIAL_NUMBER_LEN + 4];
    MCP_PCN_REC *pMcpPCNInfo;
    
    pMcpPCNInfo = fnMCPGetCurrMcpPCNInfo(pFeederMcp);

    if(pMcpPCNInfo != NULL)
    {
        bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
        bRsp[FDR_DIAG_OPERATION_OFFSET] = FDR_DIAG_WOW_RETRIEVE_PARAMS;
        bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
        bRsp[FDR_DIAG_RSP_DATA_OFFSET] = SERIAL_NUMBER;
        memcpy( &bRsp[FDR_DIAG_RSP_DATA_OFFSET + 1], pMcpPCNInfo->szPCN,  MMC_SERIAL_NUMBER_LEN);
        
        (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));

    }
    else
    {
        bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
        bRsp[FDR_DIAG_OPERATION_OFFSET] = FDR_DIAG_WOW_RETRIEVE_PARAMS;
        bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
        bRsp[FDR_DIAG_RSP_DATA_OFFSET] = SERIAL_NUMBER;
        bRsp[FDR_DIAG_RSP_DATA_OFFSET + 1] = 0;
        
        (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
    }
}

static void fnHandleWowDiagRezeroRsp(UINT8 *data,
                                     UINT16 lwLength)
{
    // this rezero is being done from the service mode diagnostics
    UINT8 bRsp[4];
    
    bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
    bRsp[FDR_DIAG_OPERATION_OFFSET] = FDR_DIAG_WOW_REZERO;
    bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
    bRsp[FDR_DIAG_RSP_DATA_OFFSET] = data[MMC_DIAG_RSP_ERROR_CODE_OFFSET] ;
    
    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
}

static void fnHandleWowDiagCalibrateZeroRsp(UINT8 *data,
                                     UINT16 lwLength)
{
    // this rezero is being done from the service mode diagnostics
    UINT8 bRsp[4]; 
    
    bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
    bRsp[FDR_DIAG_OPERATION_OFFSET] = FDR_DIAG_WOW_CALIBRATE_ZERO;
    bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
    bRsp[FDR_DIAG_RSP_DATA_OFFSET] = data[MMC_DIAG_RSP_ERROR_CODE_OFFSET] ;
    
    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
}

static void fnHandleWowDiagCalibrateFullRsp(UINT8 *data,
                                     UINT16 lwLength)
{
    // this rezero is being done from the service mode diagnostics
    UINT8 bRsp[4]; 
    
    bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
    bRsp[FDR_DIAG_OPERATION_OFFSET] = FDR_DIAG_WOW_CALIBRATE_FULL;
    bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
    bRsp[FDR_DIAG_RSP_DATA_OFFSET] = data[MMC_DIAG_RSP_ERROR_CODE_OFFSET] ;
    
    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
}

static void fnHandleWowDiagStaticWeighRsp(UINT8 *data,
                                     UINT16 lwLength)
{
    // this rezero is being done from the service mode diagnostics
    UINT8 bRsp[4], bStaticWeightRsp[8]; 

    if (data[MMC_DIAG_RSP_ERROR_CODE_OFFSET] == 0)  //successfully to get the static weight
    {
        bStaticWeightRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
        bStaticWeightRsp[FDR_DIAG_OPERATION_OFFSET] = FDR_DIAG_WOW_STATIC_WEIGHT;
        bStaticWeightRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
        bStaticWeightRsp[FDR_DIAG_RSP_DATA_OFFSET] = data[MMC_DIAG_RSP_ERROR_CODE_OFFSET] ; //stWOWDiagStatus.fStaticWeight=0;
        memcpy(&bStaticWeightRsp [FDR_DIAG_RSP_DATA_OFFSET + 1], &data[MMC_DIAG_RSP_HEADER_LEN], sizeof(UINT32));
        (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bStaticWeightRsp, sizeof(bStaticWeightRsp));
    }
    else   //failed to get the static weight
    {
        bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
        bRsp[FDR_DIAG_OPERATION_OFFSET] = FDR_DIAG_WOW_STATIC_WEIGHT;
        bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
        bRsp[FDR_DIAG_RSP_DATA_OFFSET] = data[MMC_DIAG_RSP_ERROR_CODE_OFFSET] ; //stWOWDiagStatus.fStaticWeight = data[MMC_DIAG_RSP_ERROR_CODE_OFFSET];
        (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
    }
}

static void fnFDRSendDiagThicknessSensorStatusMsg()
{
    UINT8 bRsp[WOW_THICKNESS_SENSOR_LENGTH+3];   

    bRsp [FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
    bRsp [FDR_DIAG_OPERATION_OFFSET] = FDR_READ_DR_THICKNESS_ENCODER;
    bRsp [FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;

    // add the current thickness value to the message
    (void)memcpy((void*)& bRsp [FDR_DIAG_RSP_DATA_OFFSET],
                    &ucExtendedSensorStates[WOW_THICKNESS_SENSOR_OFFSET],
                    WOW_THICKNESS_SENSOR_LENGTH);

    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
}

static void fnHandleThicknessEncoderValueRsp(UINT8 *data,
                                     UINT16 lwLength)
{
    // this rezero is being done from the service mode diagnostics
    UINT8 bRsp[4];   

    if (fDRPauseSensorPolling)
    {
        fDRPauseSensorPolling = FALSE;
    }

    if (data[MMC_DIAG_RSP_ERROR_CODE_OFFSET] == 0)  // zero is success
    {
        // copy the thickness value to the current
        if (memcmp((void*)&data[WOW_DIAG_THICKNESS_ENCODER_OFFSET],
                    &ucExtendedSensorStates[WOW_THICKNESS_SENSOR_OFFSET],
                    WOW_THICKNESS_SENSOR_LENGTH) != 0)
        {
            // save the curr to the previous
            (void)memcpy(&ucPrevExtendedSensorStates[WOW_THICKNESS_SENSOR_OFFSET],
                    &ucExtendedSensorStates[WOW_THICKNESS_SENSOR_OFFSET],
                    WOW_THICKNESS_SENSOR_LENGTH);
            // save the curr
            (void)memcpy(&ucExtendedSensorStates[WOW_THICKNESS_SENSOR_OFFSET],
                    (void*)&data[WOW_DIAG_THICKNESS_ENCODER_OFFSET],
                    WOW_THICKNESS_SENSOR_LENGTH);

            // send the udpated sensor values
            fnFDRSendDiagThicknessSensorStatusMsg();
        }
    }
}

static void fnFDRSendDiagWidthSensorStatusMsg()
{
    UINT8 bRsp[WOW_WIDTH_SENSOR_LENGTH+3];   

    bRsp [FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
    bRsp [FDR_DIAG_OPERATION_OFFSET] = FDR_READ_DR_WIDTH_SENSOR;
    bRsp [FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;

    // add the current thickness value to the message
    (void)memcpy((void*)& bRsp[FDR_DIAG_RSP_DATA_OFFSET],
                    &ucExtendedSensorStates[WOW_WIDTH_SENSOR_OFFSET],
                    WOW_WIDTH_SENSOR_LENGTH);

    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
}

static void fnHandleWidthSensorRsp(UINT8 *data,
                                     UINT16 lwLength)
{
    // this rezero is being done from the service mode diagnostics
    UINT8 bRsp[4];   

    if (fDRPauseSensorPolling)
    {
        fDRPauseSensorPolling = FALSE;
    }

    if (!data[MMC_DIAG_RSP_ERROR_CODE_OFFSET])  // zero is success
    {
        // copy the thickness value to the current
        if (memcmp((void*)&data[WOW_DIAG_WIDTH_SENSOR_OFFSET],
                    &ucExtendedSensorStates[WOW_WIDTH_SENSOR_OFFSET],
                    WOW_WIDTH_SENSOR_LENGTH) != 0)
        {
            // save the curr to the previous
            (void)memcpy(&ucPrevExtendedSensorStates[WOW_WIDTH_SENSOR_OFFSET],
                    &ucExtendedSensorStates[WOW_WIDTH_SENSOR_OFFSET],
                    WOW_WIDTH_SENSOR_LENGTH);
            // save the curr
            (void)memcpy(&ucExtendedSensorStates[WOW_WIDTH_SENSOR_OFFSET],
                    (void*)&data[WOW_DIAG_WIDTH_SENSOR_OFFSET],
                    WOW_WIDTH_SENSOR_LENGTH);

            // send the udpated sensor values
           fnFDRSendDiagWidthSensorStatusMsg();

        }
    }
}

static void fnDiagStatusRspCb(const void *pMcp, const UINT32 lwStatus, const void *pData, UINT32 lwDataLength)
{
    UINT8  *ptr;

    /* we've already verified the message's Class Code, the Command ID, etc.,
     * so we can simply bypass the header, and extract the data
     */
    if (lwStatus == MCP_LIB_SUCCESS)
    {
        UINT32 lwState = fnGetState();

        if (lwState != FDR_IDLE_STATE && lwState != FDR_DIAG_STATE) // make sure that we only deal with sensors in the idle state
            return; 

        FDRTASK_ASSERT(pData && lwDataLength > MMC_MSG_HEADER_LEN); //lint !e506

        if (pData && lwDataLength > MMC_MSG_HEADER_LEN)
        {
            ptr = pData;
            ptr += MMC_MSG_HEADER_LEN;
            lwDataLength -= MMC_MSG_HEADER_LEN;

            /* process the response message based upon the received Diagnostic Response
             * Type
             */
            switch (ptr[MMC_DIAG_RSP_CODE_OFFSET])
            {
                case WOW_DIAG_REZERO_COMMAND:
                    fnHandleWowDiagRezeroRsp(ptr,lwDataLength);
                    break;
                case WOW_DIAG_CALIBRATE_ZERO_COMMAND:
                    fnHandleWowDiagCalibrateZeroRsp(ptr,lwDataLength);
                    break;
                case WOW_DIAG_CALIBRATE_FULL_COMMAND:
                    fnHandleWowDiagCalibrateFullRsp(ptr,lwDataLength);
                    break;
                case WOW_DIAG_STATIC_WEIGH_COMMAND:
                    fnHandleWowDiagStaticWeighRsp(ptr,lwDataLength);
                    break;

                //  PIP development
                case WOW_READ_THICKNESS_ENCODER_COMMAND:
                    fnHandleThicknessEncoderValueRsp(ptr,lwDataLength);
                    break;
                case WOW_READ_WIDTH_SENSOR_COMMAND:
                    fnHandleWidthSensorRsp(ptr,lwDataLength);
                    break;

                default:
                    /* unknown Diagnostic; log the error in the system log using '05' for
                     * the MCP Error Class, specifying switch case #xx, and providing
                     * the Diagnostic ID
                     */
                    fnReportMeterError(ERROR_CLASS_MCP, MCP_MMCI_INVALID_STATE_TRANSITION);
                    break;
            }
        }
    }

}
static void fnStartDiag(const UINT8 bCmd, const UINT8 bParam, INTERTASK *pMsg)
{
    unsigned char profGroup = 0;
    unsigned char arrayOffset = 0;
    UINT8 bRsp[FDR_DIAG_RSP_STATUS_OFFSET+1] = {0};

    bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_START;
    bRsp[FDR_DIAG_OPERATION_OFFSET] = bCmd;
    bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;

    diagFlagCount = 0;
    diagProfGroup = MMC_DIAG_PROFILE_GROUP;
//    bLastFeederDiag = bCmd;

    switch (bCmd) {
    case FDR_DIAG_RUN_MOTOR_25:
        diagFlags[arrayOffset++] = MMC_FEEDER_DIAG_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;

        diagFlags[arrayOffset++] = MMC_SPEED_25_PCT_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;
        break;
    case FDR_DIAG_RUN_MOTOR_50:
        diagFlags[arrayOffset++] = MMC_FEEDER_DIAG_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;

        diagFlags[arrayOffset++] = MMC_SPEED_50_PCT_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;
        break;
    case FDR_DIAG_RUN_MOTOR_75:
        diagFlags[arrayOffset++] = MMC_FEEDER_DIAG_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;

        diagFlags[arrayOffset++] = MMC_SPEED_75_PCT_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;
        break;
    case FDR_DIAG_RUN_MOTOR_100:
        diagFlags[arrayOffset++] = MMC_FEEDER_DIAG_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;

        diagFlags[arrayOffset++] = MMC_SPEED_100_PCT_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;
        break;
    case WOW_DIAG_RUN_XPT_MOTOR_25:
        diagFlags[arrayOffset++] = WOW_TRANSPORT_DIAG_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;

        diagFlags[arrayOffset++] = MMC_SPEED_25_PCT_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;
        break;
      case WOW_DIAG_RUN_XPT_MOTOR_50:
        diagFlags[arrayOffset++] = WOW_TRANSPORT_DIAG_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;

        diagFlags[arrayOffset++] = MMC_SPEED_50_PCT_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;
        break;
      case WOW_DIAG_RUN_XPT_MOTOR_75:
        diagFlags[arrayOffset++] = WOW_TRANSPORT_DIAG_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;

        diagFlags[arrayOffset++] = MMC_SPEED_75_PCT_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;
        break;
    case WOW_DIAG_RUN_XPT_MOTOR_100:
        diagFlags[arrayOffset++] = WOW_TRANSPORT_DIAG_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;

        diagFlags[arrayOffset++] = MMC_SPEED_100_PCT_FLAG;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;
        break;
    case FDR_DIAG_READ_ENCODER:
        fnDiagHandleEncoderCountRequest(FDR_MOTOR_ID);
        return;
        break;  //lint !e527
    case WOW_DIAG_MOTOR_READ_ENCODER:
        fnDiagHandleEncoderCountRequest(MMC_MIDJET_TRANSPORT_MOTOR_ID);
        return;
        break;  //lint !e527
    case FDR_DIAG_READ_SENSORS:
        (void)OSSendIntertask(_CMTASK_,FDRMCPTASK,FC_DIAG_CMD_RSP,BYTE_DATA,bRsp,sizeof(bRsp));
        (void)OSChangeTimerDuration(FP_FEEDER_QUERY_SENSOR_TIMER,FEEDER_DIAG_SENSOR_POLLING_INTERVAL);     
        (void)OSStartTimer(FP_FEEDER_QUERY_SENSOR_TIMER);
        return;
        break;  //lint !e527
    case DIAG_MAIL_FLOW:
        break;
    case FDR_DIAG_PRINT_LOGS:
        fnMCPPrintSensorLog(pFeederMcp);
        fnMCPPrintProfileHistory(pFeederMcp);
        fnSendDiagStatus(FDR_DIAG_OK);
        return;
        break;  //lint !e527
    case FDR_DIAG_WOW_REZERO:
        fnDiagWOWRezero();
        return;
        break; //lint !e527
    case FDR_DIAG_WOW_CALIBRATE_ZERO:
        fnDiagWOWCalibrateZero();
        return;
        break; //lint !e527
    case FDR_DIAG_WOW_CALIBRATE_FULL:
        fnDiagWOWCalibrateFull();
        return;
        break; //lint !e527
    case FDR_DIAG_WOW_STATIC_WEIGHT:
        fnDiagWOWStaticWeight();
        return;
        break; //lint !e527
    case FDR_DIAG_WOW_SET_MODE:
        fnDiagWOWSetMode(bParam);
        return;
        break; //lint !e527
    case FDR_DIAG_WOW_RETRIEVE_PARAMS:
        fnDiagWOWRetrieveParams(bParam);
        return;
        break; //lint !e527
    case FDR_READ_DR_THICKNESS_ENCODER:
        fnDiagWOWRequestDRThicknessSensor();
        return;
        break; //lint !e527
    case FDR_READ_DR_WIDTH_SENSOR:
        fnDiagWOWRequestDRWidthSensor();
        //Maybe start a certain timer.TBD
        //OSStartTimer(FP_FEEDER_QUERY_SENSOR_TIMER);
        return;
        break; //lint !e527
    case FDR_DIAG_REZERO_DR_THICKNESS_ENCODER:
        fnDiagWOWRezeroDRThicknessEncoder();
        return;
        break; //lint !e527
    case FDR_DIAG_SET_SENSITIVITY_DR_WIDTH_SENSOR:
        wowNvmMachineInfo.widthSensitivity = bParam;
                    fnMCPSendRequest (pFeederMcp,
                                    (UINT8 *) &wowNvmMachineInfo,
                                    sizeof(wowNvmMachineInfo),
                                    MMC_NVM_MACHINE_INFO_RECORD_RSP,
                                    MMC_NO_RESPONSE, NULL);
              return;
        break; //lint !e527
    case FDR_DIAG_REZERO_DR_WIDTH_SENSOR:
        fnDiagWOWRezeroDRWidthSensor();
        return;
        break; //lint !e527
    case WOW_DIAG_READ_PCN_INFO:
        fnDiagWOWReadPCNInfo();
        return;
        break; //lint !e527
    case WOW_DIAG_READ_CYCLE_COUNT:
        fnDiagWOWReadCycleCount();
        break;
    case WOW_DIAG_READ_JAM_COUNT:
        fnDiagWOWReadJamCount();
        break;
    case WOW_DIAG_RESET_JAM_COUNT:
        fnDiagWOWResetJamCount();
        break;
    case WOW_DIAG_LENGTH_CALIBRATION:
        wowNvmMachineInfo.lenCalibrated = 1;
        diagFlags[arrayOffset++] = MMC_WOW_LENGTH_CALIBRATION;
        diagFlags[arrayOffset++] = MMC_FLAG_SET;
        diagFlagCount += 1;
        break;
    default:
        break;
    };

    if (diagFlagCount)
    {
        fnMCPSetMultipleRemoteFlags(pFeederMcp, &diagFlags[0], diagFlagCount);

        (void)fnMCPStartProfile(pFeederMcp,diagProfGroup,0);
    }
    else
    {
        bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = 0xff;
        
    }
    (void)OSSendIntertask(_CMTASK_,FDRMCPTASK,FC_DIAG_CMD_RSP,BYTE_DATA,bRsp,sizeof(bRsp));
}
static void fnStopDiag(void)
{
    UINT8 bRsp[FDR_DIAG_RSP_STATUS_OFFSET+1] = {0};
    UINT8 bCommand[2] = {0};

    bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STOP;
//    bRsp[FDR_DIAG_OPERATION_OFFSET] = bLastFeederDiag;
    bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;

/*
    switch (bLastFeederDiag) {
    case FDR_DIAG_READ_ENCODER:
    case WOW_DIAG_MOTOR_READ_ENCODER:
        (void)OSChangeTimerDuration(FP_FEEDER_QUERY_SENSOR_TIMER,FEEDER_SENSOR_POLLING_INTERVAL);
        (void)OSStopTimer(FP_FEEDER_QUERY_SENSOR_TIMER);

        // The MCP may be still in "MCP_LIB_WAIT_RSP_STATE", which will cause MCP not accepting command,
        // so we must reset the MCP instance here.
        (void)fnMCPLibReset(pFeederMcp);
        break;

    case FDR_DIAG_READ_SENSORS:
        (void)OSChangeTimerDuration(FP_FEEDER_QUERY_SENSOR_TIMER,FEEDER_SENSOR_POLLING_INTERVAL);
        (void)OSStopTimer(FP_FEEDER_QUERY_SENSOR_TIMER);

        // The MCP may be still in "MCP_LIB_WAIT_RSP_STATE", which will cause MCP not accepting command,
        // so we must reset the MCP instance here.
        (void)fnMCPLibReset(pFeederMcp);
        break;
        
    case WOW_DIAG_LENGTH_CALIBRATION:
        wowNvmMachineInfo.lenCalibrated = 2;
        bCommand[0] = MMC_WOW_LENGTH_CALIBRATION;
        bCommand[1] = MMC_FLAG_SET;
        fnMCPSendRequest (pFeederMcp,
                            bCommand,
                            2,
                            MMC_START_DIAGNOSTIC_REQ,
                            MMC_NO_RESPONSE, NULL);
        break;
    default:
        break;
    };
*/

    fnMCPCancelGroup(pFeederMcp,diagProfGroup);
    fnMCPResetAllFlags(pFeederMcp);

//    bLastFeederDiag = FDR_DIAG_NONE;

    (void)OSSendIntertask(_CMTASK_,FDRMCPTASK,FC_DIAG_CMD_RSP,BYTE_DATA,bRsp,sizeof(bRsp));
}
static void fnProcessDiagReq(const INTERTASK *pMsg)
{
    UINT8 *pPtr=NULL;
    UINT32 lwLength = 0;
    UINT8 bCmd = 0;
    UINT8 bAction = 0;
    UINT8 bParam = 0;

    FDRTASK_ASSERT(pMsg);   //lint !e506
    FDRTASK_ASSERT(lwFeederTaskState == FDR_DIAG_STATE);    //lint !e506
    if (!pMsg || lwFeederTaskState != FDR_DIAG_STATE)
        return;
    GetIntertaskPointerAndLength((void**)&pPtr,&lwLength,pMsg); //lint !e605
    FDRTASK_ASSERT(pPtr);   //lint !e506
    if (!pPtr)
        return;

    bAction = pPtr[FDR_DIAG_ACTION_OFFSET];
    bCmd = pPtr[FDR_DIAG_OPERATION_OFFSET];
    bParam = pPtr[FDR_DIAG_REQ_DATA_OFFSET];

    if (bAction == FDR_DIAG_START)
    {
        fnStartDiag(bCmd,bParam,pMsg);
    }
    else if (bAction == FDR_DIAG_STOP)
    {
        fnStopDiag();
    }
}
static void fnDiagStateMsgHandler(const void *pMcp, const void *pData, const unsigned long lwDataLength)
{
    if (pData && lwDataLength >= MMC_MSG_COMMAND_OFFSET)
    {
        const unsigned char *pPtr = (const unsigned char *)pData;
        const unsigned char bMsg = pPtr[MMC_MSG_COMMAND_OFFSET];

        switch (bMsg) {
        case MMC_ERROR_LOG_DATA_RSP:
            fnDiagStateErrorLogDataHandler(pMcp,pData,lwDataLength);
            break;
        case MMC_PROFILE_STATUS_RSP:
            fnDiagProfileStatusHandler(pPtr[MMC_MSG_HEADER_LEN]);
            break;
        default:
            break;
        };  
    }
}
static void fnDiagStateErrorLogDataHandler(const void *pMcp, const void *pData, const unsigned long lwDataLength)
{
// Nothing do to for now
}

static void fnDiagProfileStatusHandler(const UINT8 bStatus)
{
    switch (bStatus)
    {
        case WOW_LENGTH_CALIBRATION_SUCCESS:
            fnDiagWOWLengthCalibrationSuccess();
            break;
        case WOW_LENGTH_CALIBRATION_FAILURE:
            fnDiagWOWLengthCalibrationFailure();
            break;
        case MMC_MAIL_PIECE_FEED:
            fnDiagMailPieceFeed();
            break;
        default:
            break;
    }
}
static void fnDiagWOWLengthCalibrationSuccess(void)
{
    UINT8 bRsp[4];

    //set length calibration status to MCP
    wowNvmMachineInfo.lenCalibrated = 0; // length calibration success

    //send length calibration status to CM
    bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
    bRsp[FDR_DIAG_OPERATION_OFFSET] = WOW_DIAG_LENGTH_CALIBRATION;
    bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
    bRsp[FDR_DIAG_RSP_DATA_OFFSET] = wowNvmMachineInfo.lenCalibrated;
    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
}
static void fnDiagWOWLengthCalibrationFailure(void)
{
    UINT8 bRsp[4];

    //set length calibration status to MCP
    wowNvmMachineInfo.lenCalibrated = 2; // length calibration fail

    //send length calibration status to CM
    bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
    bRsp[FDR_DIAG_OPERATION_OFFSET] = WOW_DIAG_LENGTH_CALIBRATION;
    bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
    bRsp[FDR_DIAG_RSP_DATA_OFFSET] = wowNvmMachineInfo.lenCalibrated;
    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
}
static void fnDiagMailPieceFeed(void)
{
    UINT8 bRsp[4];

    if(wowNvmMachineInfo.lenCalibrated == 1) // length calibration in process
    {
        bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
        bRsp[FDR_DIAG_OPERATION_OFFSET] = WOW_DIAG_LENGTH_CALIBRATION;
        bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;
        bRsp[FDR_DIAG_RSP_DATA_OFFSET] = wowNvmMachineInfo.lenCalibrated;
        
        (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
    }
}

static void fnSendDiagStatus(const unsigned char bStatus)
{
    UINT8 bRsp[FDR_DIAG_RSP_DATA_OFFSET] = {0};

    bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
//    bRsp[FDR_DIAG_OPERATION_OFFSET] = bLastFeederDiag;
    bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = bStatus;

    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
}


static void fnResetFeeder(void)
{
	//TODO - move into HAL and delete here
#if 0
    UINT8 minorHwVer;

    fnGetHardwareVersion(NULL_PTR, &minorHwVer);

    if (minorHwVer != FP_HW_VERSION_DE1_VAL)
    {
        // Hold the Feeder in Reset for a little while
        FDR_RESET_PORT &= ~FDR_RESET_PORT_BIT;
        NU_Sleep ((UNSIGNED) 500/MILLISECONDS_PER_TICK);
        // Take the Feeder out of Reset - it should re-enumerate as a boot mode device
        FDR_RESET_PORT |= FDR_RESET_PORT_BIT;
    }
#endif
    HALFeederReset();
}

BOOL fnIsFeederHardwarePresent(void)
{
    return HALIsFeederPresent();
}

/**********************************************************************
fnSendStatusRsp
This function sends a Get Status Response message.

Scope
Private

Prototype
static void fnSendStatusRsp(void)

**********************************************************************/
static void fnSendStatusRsp(void)
{
    uchar               rsp[12];
    UINT16  usSensorValue = fnMapCurrentSensorValues();
    
    (void)memset((void*)rsp,0,sizeof(rsp)); //lint !e718 !e746
    rsp[0] = MCP_NO_ERROR;
    rsp[1] = MCP_NO_ERROR;
    rsp[2] = (UINT8)usSensorValue;
    rsp[3] = (UINT8)(usSensorValue >> 8);
    rsp[4] = fnMapRunningState();

    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_GET_STATUS_RSP, BYTE_DATA, rsp, sizeof(rsp));
}

/**********************************************************************
fnProcessInvalidReqForState
This function handles a request that cannot be processed by the Feeder
Task in its current state.

Scope
Private

Prototype
static void fnProcessInvalidReqForState(const INTERTASK *pMsg);

Pre-Conditions
pMsg != NULL

Inputs
pMsg - Pointer to an Intertask Message.

MODIFICATION HISTORY:
    11/13/2012  Bob Li      Updated the current Feeder Stop type in time for fixing fraca 219120.  
**********************************************************************/
static void fnProcessInvalidReqForState(const INTERTASK *pMsg)
{
    unsigned char *pPtr = NULL;
    unsigned long lwLength = 0;

    if (pMsg)
    {
        switch (pMsg->bMsgId) {
        case CF_INIT:
            fSendInitRsp = TRUE;
            fnSendInitRsp(FDR_MISC_ERR,MCP_FPFDR_INVALID_OP_IN_STATE);
            break;
        case CF_SET_MODE:
            fnSendSetModeRsp(MCP_FPFDR_INVALID_OP_IN_STATE);
            break;
        case CF_RUN:
            fSendStartRsp = TRUE;
            fnSendStartRsp(MCP_FPFDR_INVALID_OP_IN_STATE);
            break;
        case CF_STOP_RUN:            
            GetIntertaskPointerAndLength((void**)&pPtr,&lwLength,pMsg); //lint !e605            
            FDRTASK_ASSERT(pPtr);       //lint !e506            
            if (pPtr)
            {
                eCurrStopType = pPtr[0];
            }
            fSendStopRsp = TRUE;
            fnSendStopRsp(MCP_FPFDR_INVALID_OP_IN_STATE);
            break;
        default:
            break;
        };
    }
}

/**********************************************************************
fnReloadProfilesCompleteCB
This callback function is called by the MCP Module after the MCP Module
has finished downloading profiles.
The profiles may have been downloaded successfully or 
unsuccessfully depending on the status passed back.

Scope
Private

Prototype
static void fnReloadProfilesCompleteCB(const unsigned long lwInitStatus);

Inputs
lwStatus - Profile Download Status.  Valid Values are:
0 - Success
Other - Error

Processing

**********************************************************************/
static void fnReloadProfilesCompleteCB(const void *pMcp, const unsigned long lwStatus)
{
    UINT8 bRsp[FDR_DIAG_RSP_DATA_OFFSET + 1] = {0};

    bRsp[FDR_DIAG_ACTION_OFFSET] = FDR_DIAG_STATUS;
//    bRsp[FDR_DIAG_OPERATION_OFFSET] = bLastFeederDiag;
    bRsp[FDR_DIAG_RSP_STATUS_OFFSET] = FDR_DIAG_OK;

    if (lwStatus == MCP_LIB_SUCCESS)
    {
        bRsp[FDR_DIAG_RSP_DATA_OFFSET]= 0;//means success
    }
    else
    {
        bRsp[FDR_DIAG_RSP_DATA_OFFSET]= 1;//means failure
    }

    (void)fnSendIntertask(_CMTASK_, FDRMCPTASK, FC_DIAG_CMD_RSP, BYTE_DATA, bRsp, sizeof(bRsp));
}

unsigned long fnFeederLoadProfiles(const char *pProfileFileName)
{
    unsigned long lwDnldStatus = fnMCPDownloadProfiles((void*)pFeederMcp,FALSE,fnReloadProfilesCompleteCB);

    return lwDnldStatus;
}

/**********************************************************************
4.1.8   fnDispatchMsgHandler
This function dispatches processing to the appropriate handler function
based on the feeder state, the message source, and the message ID.

4.1.8.1 Scope
Private

4.1.8.2 Prototype
static void fnDispatchMsgHandler(const INTERTASK *pMsg, 
                                const FDR_INTERTASK_MSG_TBL_ENTRY *pTbl, 
                                unsigned long lwNumTblEntries);

4.1.8.3 Pre-Conditions
pMsg != NULL AND pTbl != NULL

4.1.8.4 Inputs
pMsg - Pointer to an Intertask Message.
pTbl - Pointer to an Intertask Message Handler Map Table.
lwNumTblEntries - Number of entries in the pTbl Intertask Message 
    Handler Map Table.


4.1.8.5 Processing

FOR EACH entry in the handler map table
  IF the table entry source matches and the message ID matches THEN
    SAVE a reference to the function
  END IF
END FOR

IF a match was found THEN
  CALL the handler function
END IF

**********************************************************************/
static BOOL CompareSrc(const INTERTASK *pMsg,
                        const FDR_INTERTASK_MSG_TBL_ENTRY *pTbl)
{
    if ((pTbl->wSource == FDR_MSG_DONT_CARE || (unsigned char)pTbl->wSource == pMsg->bSource) &&
            (pTbl->wMsgId == FDR_MSG_DONT_CARE || (unsigned char)pTbl->wMsgId == pMsg->bMsgId))
        return TRUE;
    else
        return FALSE;   
}
static BOOL CompareData(const INTERTASK *pMsg, 
                        const unsigned char *pByteData,
                        const unsigned long lwByteDataLen, 
                        const FDR_INTERTASK_MSG_TBL_ENTRY *pTbl)
{
    BOOL fByteZeroOk = TRUE;
    BOOL fByteOneOk = TRUE;
    if (pByteData)
    {
        if (lwByteDataLen > 0)
        {
            fByteZeroOk = (pTbl->wByte0 == FDR_MSG_DONT_CARE || (unsigned char)(pTbl->wByte0 == pByteData[0]));
            if (lwByteDataLen > 1)
                fByteOneOk = (pTbl->wByte1 == FDR_MSG_DONT_CARE || (unsigned char)(pTbl->wByte1 == pByteData[1]));
        }
    }
    if (fByteZeroOk && fByteOneOk)
        return TRUE;
    else
        return FALSE;
}   
static void fnDispatchMsgHandler(const INTERTASK *pMsg, 
                                const FDR_INTERTASK_MSG_TBL_ENTRY *pTbl, 
                                unsigned long lwNumTblEntries)
{
    BOOL fSrcMatch = FALSE;
    BOOL fDataMatch = FALSE;
    unsigned long lwTblIndex = 0;
    FDR_MSG_HANDLER_FCN_t pHandlerFcn = NULL;
    unsigned char *pPtr = NULL;
    unsigned long lwLength = 0;

    FDRTASK_ASSERT(pTbl);   //lint !e506
    FDRTASK_ASSERT(pMsg);   //lint !e506

    if (pTbl && pMsg)
    {
        fnMCPLibGetMotionClassMsgPointerAndLength((void**)&pPtr,&lwLength,pMsg); //lint !e605
            for(lwTblIndex=0;lwTblIndex<lwNumTblEntries;lwTblIndex++)
            {
                fSrcMatch = CompareSrc(pMsg,pTbl);
            fDataMatch = CompareData(pMsg,pPtr,lwLength,pTbl);
                if (fSrcMatch == TRUE && fDataMatch == TRUE)
                {
                    pHandlerFcn = pTbl->pfcnHandler;
                    break;
                }
                pTbl++;
            }
        }

    if (pHandlerFcn)
        (*pHandlerFcn)(pMsg);
}

/**********************************************************************
fnGetState
Retreives the state of the Feeder Task.

Scope
Private

Prototype
static unsigned long fnGetState(void)

Pre-Conditions
None.

Inputs
None.

Return Value
The state of the Feeder Task.

**********************************************************************/
static unsigned long fnGetState(void)
{
    return lwFeederTaskState;
}


/**********************************************************************
fnSetState
Changes the state of the Feeder Task.

Scope
Private

Prototype
static void fnSetState(const unsigned long lwState)

Pre-Conditions
None.

Inputs
lwState - The new state of the Feeder Task.

**********************************************************************/
static void fnSetState(const unsigned long lwState)
{   
    lwPrevFeederTaskState = lwFeederTaskState;
    lwFeederTaskState = lwState;

    // if going to the error catch all state, clear this flag because it will block mode change
    //  messages from going to the CM.  there is a good chance we can't recover anyway, but after
    //  drawing this out with Derek it seemed like a good idea to clear this here.
    if (lwState == FDR_INACTIVE_STATE)
        fWakeupModeChangeToCMPending = false;

}

static unsigned long fnGetPrevState(void)
{
    return lwPrevFeederTaskState;
}

/**********************************************************************

fnSendIntertask
Send an Intertask Message. Wraps the actual function that sends the
message in order to log the message.

Scope
Private

Prototype
static BOOL fnSendIntertask (unsigned char bDestID, unsigned char bSourceID, unsigned char bMsgID, 
                        unsigned char bMessageType, void *pMsgBuf, unsigned long lwSize)

Pre-Conditions
None.

 INPUTS:    bDestID - Destination task ID
            bSourceID - Source task ID
            bMsgID - Message identifier
            bMessageType - Format of the message data union
            pMsgBuf - Pointer to message data
            sSize - Size of the message data


**********************************************************************/
static BOOL fnSendIntertask (unsigned char bDestID, unsigned char bSourceID, unsigned char bMsgID, 
                        unsigned char bMessageType, void *pMsgBuf, unsigned long lwSize)
{
    BOOL fRetval = OSSendIntertask(bDestID,bSourceID,bMsgID,bMessageType,pMsgBuf,lwSize);

    if (bSourceID == FDRMCPTASK && bMsgID == FDR_POLL_SENSOR_TIMEOUT)
        return fRetval;     

    return fRetval;
}

/**********************************************************************

fnLogRecvIntertaskMsg
Logs an Intertask Message when one is received

Scope
Private

Prototype
static void fnLogRecvIntertaskMsg(const INTERTASK *pMsg);

Pre-Conditions
None.

Inputs
pMsg - Pointer to the received Intertask Message

**********************************************************************/
static void fnLogRecvIntertaskMsg(const INTERTASK *pMsg)
{
    unsigned char tmpMsg[16] = {0};
    unsigned char *pPtr = NULL;
    unsigned long lwLength = 12;

    tmpMsg[0] = pMsg->bMsgId;
    tmpMsg[1] = pMsg->bSource;
    tmpMsg[2] = pMsg->bDest;
    tmpMsg[3] = pMsg->bMsgType; 
    
    switch (pMsg->bSource) {    
    case _CMTASK_:
    case SYS:
    case OIT:
    case SCM:
        fnMCPLibGetMotionClassMsgPointerAndLength((void**)&pPtr,&lwLength,pMsg); //lint !e605
        if (pPtr)
        {
            if (lwLength == 0 || lwLength > 12)
                lwLength = 12;
            (void)memcpy((void*)&tmpMsg[4],(void*)pPtr,lwLength);
        }
        break;
    default:
        break;
    };      
}

static char bFdrTskAssertFailureText[256];

static void fnFdrTskHandleAssertionFailure(const char *pExpr, 
                                        const char *pFileName,
                                        const unsigned long lwLine)
{
    if (pExpr && pFileName)
    {
        // throw an exception?

        // log the failure
        (void)sprintf(bFdrTskAssertFailureText, "FdrTsk Assert Failure: %s, Line: %d", pExpr,lwLine);
        fnDumpStringToSystemLog(bFdrTskAssertFailureText);
        fnFdrTskLogAssertionFailure(lwLine);
    }
}

static void fnFdrTskLogAssertionFailure(const unsigned long lwLine)
{
    unsigned char tmpMsg[8];

    // put entry in the mcp buffer
    tmpMsg[0] = 0xDD;
    tmpMsg[1] = FDRMCPTASK;
    tmpMsg[2] = (unsigned char)lwFeederTaskState;
    tmpMsg[3] = 0;
    (void)memcpy((void*)&tmpMsg[4],(void*)&lwLine,4);   //lint !e718 !e746

    // put an entry in some other logs...
    fnLogSystemError(ERROR_CLASS_FP_FEEDER,0xef,(unsigned char)((lwLine&0x00FF0000)>>16),(unsigned char)((lwLine&0x0000FF00)>>8),(unsigned char)(lwLine&0x000000FF));
}

static void fnFdrLogMsg(const char *pMsg)
{
    fnDumpStringToSystemLog((char*)pMsg);
}
UINT8 fnFeederGetLastMeterErrorCode(void)
{
    return bLastFdrMeterErrCode;
}
char* fnFeederGetProfileVersionString(void)
{
    FDRTASK_ASSERT(pFeederMcp); //lint !e506
    if (!pFeederMcp)
        return NULL;
    return fnMCPGetCurrProfileVersionString((void*)pFeederMcp);
}
char* fnFeederGetMcpVersionString(void)
{
    FDRTASK_ASSERT(pFeederMcp); //lint !e506
    if (!pFeederMcp)
        return NULL;
    return fnMCPGetCurrMcpVersionString((void*)pFeederMcp);
}
static void fnLogDataCompleteCb(const void *pMcp, const unsigned long lwStatus, const unsigned char *pLogDataBuff, const unsigned long lwLogDataBuffLen)
{
    pfcnLogDataCb(pMcp,lwStatus,pLogDataBuff,lwLogDataBuffLen);
    fUploadingLogData = FALSE;
}
static void fnProcessGetSensorLogReq(const INTERTASK *pMsg)
{
    if (!fUploadingLogData)
    {       
        MCP_LOG_DATA_REQ_COMBPLETE_CB_t pFcn = NULL;
        (void*)memcpy((void*)&pFcn,(void*)pMsg->IntertaskUnion.bByteData,sizeof(pFcn));
        pfcnLogDataCb = pFcn;
        fUploadingLogData = TRUE;
        (void)fnMCPGetSensorLog((void*)pFeederMcp,fnLogDataCompleteCb);
    }
}
static void fnProcessGetProfHistLogReq(const INTERTASK *pMsg)
{
    if (!fUploadingLogData)
    {       
        MCP_LOG_DATA_REQ_COMBPLETE_CB_t pFcn = NULL;
        (void*)memcpy((void*)&pFcn,(void*)pMsg->IntertaskUnion.bByteData,sizeof(pFcn));
        pfcnLogDataCb = pFcn;
        fUploadingLogData = TRUE;
        (void)fnMCPGetProfileHistoryLog((void*)pFeederMcp,fnLogDataCompleteCb);
    }
}
unsigned long fnFeederGetSensorLogData(MCP_LOG_DATA_REQ_COMBPLETE_CB_t pfcnLogDataCbFn)
{
    if (pfcnLogDataCbFn)
    {
        (void)fnSendIntertask(FDRMCPTASK, FDRMCPTASK, FDR_REQ_SENSOR_LOG_DATA, BYTE_DATA, &pfcnLogDataCbFn, sizeof(pfcnLogDataCbFn));
        return MCP_LIB_SUCCESS;
    }
    else
        return MCP_LIB_INVALID_PARAM;
}
unsigned long fnFeederGetProfileHistoryLog(MCP_LOG_DATA_REQ_COMBPLETE_CB_t pfcnLogDataCbFn)
{
    if (pfcnLogDataCbFn)
    {
        (void)fnSendIntertask(FDRMCPTASK, FDRMCPTASK, FDR_REQ_PROF_HIST_LOG_DATA, BYTE_DATA, &pfcnLogDataCbFn, sizeof(pfcnLogDataCbFn));
        return MCP_LIB_SUCCESS;
    }
    else
        return MCP_LIB_INVALID_PARAM;
}
static void fnPrintProfHistCb(const void *pMcp, const unsigned long lwStatus, const unsigned char *pLogDataBuff, const unsigned long lwLogDataBuffLen)
{
    fnMCPPrintProfileHistory(pFeederMcp);
}
static void fnPrintSensorLogCb(const void *pMcp, const unsigned long lwStatus, const unsigned char *pLogDataBuff, const unsigned long lwLogDataBuffLen)
{
    fnMCPPrintSensorLog(pFeederMcp);
    (void)fnFeederGetProfileHistoryLog((MCP_LOG_DATA_REQ_COMBPLETE_CB_t)fnPrintProfHistCb);
}
void fnFeederPrintReports(void)
{
    fnMCPPrintSendTraceBuffer();
    fnMCPPrintRecvTraceBuffer();
    (void)fnFeederGetSensorLogData((MCP_LOG_DATA_REQ_COMBPLETE_CB_t)fnPrintSensorLogCb);
}

void TestLogging(void)
{
    fnMCPSendSimpleRequest(pFeederMcp, 0xFF,
                                   MMC_NO_RESPONSE,NULL);
}

UINT32 fnFeederGetPointerToGapLog(void)
{
    return (UINT32)((UINT8*)feederMailPieceDataLog);
}
UINT32 fnFeederGetGapLogLength(void)
{
    return (UINT32)(sizeof(feederMailPieceDataLog));
}

BOOL fnFDRGetLenCalibCompleteStatus(BOOL *fLenCalibStatus)
{
    BOOL    fStatus = FALSE;
    BOOL    *fLengthCalibrated;
    
    if(fnMCPGetCurrMcpNVMMachineInfo(MCP_NVM_LENGTH_CALIBRATION_INFO, pFeederMcp, (void **)&fLengthCalibrated) == MCP_LIB_SUCCESS)
    {
        *fLenCalibStatus = *fLengthCalibrated;
        fStatus = TRUE;
    }

    return fStatus;
}

BOOL fnFDRSetLenCalibCompleteStatus(BOOL *fLenCalibStatus)
{
    BOOL    fStatus = FALSE;
    
    if(fnMCPSetCurrMcpNVMMachineInfo(MCP_NVM_LENGTH_CALIBRATION_INFO, pFeederMcp, (void *)fLenCalibStatus) == MCP_LIB_SUCCESS)
    {
        fStatus = TRUE;
    }

    return fStatus;
}

UINT32 fnGetNumPiecesWeighed()
{
    return ulNumPiecesWeighed;
}

void fnSetNumPiecesWeighed(UINT32 ulValue)
{
    ulNumPiecesWeighed = ulValue;
}

BOOL fnIsMCPWOWError(UINT8 ucErr)
{
    BOOL fStatus = FALSE;
    UINT8   i = 0;
    
    while(mmcWOWErrorMap[i].ucMappedErrorCode != MMC_WOW_ERROR_MAPPING_END)
    {
        if(ucErr == mmcWOWErrorMap[i].ucMappedErrorCode)
    {
        fStatus = TRUE;
            break;
        }
        
        i++;
    }

    return fStatus;
}

BOOL fnIsWOWDimRatingError()
{
    return fWOWDimRatingErrorFlag;
}

void fnSetWOWDimRatingErrorFlag(BOOL bStatus)
{
    fWOWDimRatingErrorFlag = bStatus;
}

static UINT8 fnMapWOWError(UINT8 ucMCPErrorCode)
{
    UINT8   ucUICErrorCode = MCP_WOW_ERROR; // default UIC Error code
    UINT8   i = 0;
    
    while(mmcWOWErrorMap[i].ucError != MMC_WOW_ERROR_MAPPING_END)
    {
        if(ucMCPErrorCode == mmcWOWErrorMap[i].ucError)
    {
            ucUICErrorCode = mmcWOWErrorMap[i].ucMappedErrorCode;
            break;
        }
        
        i++;
    }

    return ucUICErrorCode;
}

BOOL fnStopQuerySensorTimer()
{
    (void)OSStopTimer(FP_FEEDER_QUERY_SENSOR_TIMER);
    
    // The MCP may be still in "MCP_LIB_WAIT_RSP_STATE", which will cause MCP not accepting command,
    // so we must reset the MCP instance here.
    (void)fnMCPLibReset(pFeederMcp);
}

BOOL fnStartQuerySensorTimer()
{
	//TODO - restore when Feeder task is enabled again
#if 0  //prevent messages to Feeder queue to avoid queue full errors
    (void)OSChangeTimerDuration(FP_FEEDER_QUERY_SENSOR_TIMER,FEEDER_DIAG_SENSOR_POLLING_INTERVAL);     
    (void)OSStartTimer(FP_FEEDER_QUERY_SENSOR_TIMER);
#endif
}



static const char Astate0[] = "                ";
static const char Astate1[] = "                ";
static const char Dstate0[] = "XXXX............";
static const char Dstate1[] = "----............";
static const char Mstate0[] = "-               ";
static const char Mstate1[] = "X               ";

static const char * feeder_rpt_header[] = {
{"                           D M 4 0 0 C   F E E D E R   S E N S O R   L O G   R E P O R T "},
{"  Time                Analog Sensors                      Digital Sensors             Motor Enables  "},
{"               0 1 2 3 4 5 6 7 8 9 A B C D E F    0 1 2 3 4 5 6 7 8 9 A B C D E F    0 1 2 3 4 5 6 7 "},
{"              +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+  +-+-+-+-+-+-+-+-+"},
{"              | | | | | | | | | | | | | | | | |  |F| |F|F| | | | | | | | | | | | |  |F| | | | | | | |"},
{"              | | | | | | | | | | | | | | | | |  |D| |S|S| | | | | | | | | | | | |  |D| | | | | | | |"},
{"              | | | | | | | | | | | | | | | | |  |R| |2|1| | | | | | | | | | | | |  |R| | | | | | | |"},
{"              | | | | | | | | | | | | | | | | |  | | | | | | | | | | | | | | | | |  | | | | | | | | |"},
{"              | | | | | | | | | | | | | | | | |  |C| | | | | | | | | | | | | | | |  |M| | | | | | | |"},
{"              | | | | | | | | | | | | | | | | |  |V| | | | | | | | | | | | | | | |  |T| | | | | | | |"},
{"              | | | | | | | | | | | | | | | | |  |R| | | | | | | | | | | | | | | |  |R| | | | | | | |"},
{"              +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+  +-+-+-+-+-+-+-+-+"},
{NULL}
};

static const MCP_REPORT_CONTROL_DATA sFeederReportInfo = {
    (const char **)feeder_rpt_header,
    0x0000, // 0000 0000 0000 0000  Analog Sensors
    0x000D, // 0000 0000 0000 1101  Digital Sensors
    0x0001, // 0000 0000 0000 0001  Motors
    Astate0,
    Astate1,
    Dstate0,
    Dstate1,
    Mstate0,
    Mstate1,
    FALSE,  // report analog sensors
    TRUE,   // report digital sensors
    TRUE,   // report motor sensors
};


/**********************************************************************/
// 
//  OS Stuff to Port
//
/**********************************************************************/

// OSSendIntertask
// OSReceiveIntertask
// OSResumeTask

/**********************************************************************/
//  END OS Stuff to Port
/**********************************************************************/





/**********************************************************************/
#ifdef FP_DUMMY_CM

void TestStartFeeder()
{
    uchar    runMsg[LEN_2];

    runMsg[POS_0] = 0;
    runMsg[POS_1] = THRUPUT_120;

    (void)OSSendIntertask(FDRMCPTASK, _CMTASK_, CF_RUN, BYTE_DATA, runMsg, sizeof(runMsg));
}
void TestStopFeeder()
{
    
    uchar    fdrStopMsg[LEN_1];

    fdrStopMsg[POS_0] = GRACEFUL_STOP;

    (void)OSSendIntertask(FDR, _CMTASK_, CF_STOP_RUN, BYTE_DATA, fdrStopMsg, sizeof(fdrStopMsg));
}

/**********************************************************************
        Dummy Control Manager For Development
**********************************************************************/

static void fnFdrInitX()
{
    uchar         thruput = THRUPUT_120;

    (void)OSSendIntertask(FDRMCPTASK, _CMTASK_, CF_INIT, BYTE_DATA, &thruput, sizeof(thruput));
}

static void fnFdrSetModeX(emOpMode bMode)  
{
    uchar       modeMsg[LEN_2];

    modeMsg[POS_0] = bMode;
    modeMsg[POS_1] = THRUPUT_120;
    
    (void)OSSendIntertask(FDRMCPTASK, _CMTASK_, CF_SET_MODE, BYTE_DATA, modeMsg, sizeof(modeMsg));
}

static void fnFdrRunNormalX()
{
    uchar    runMsg[LEN_2];

    runMsg[POS_0] = 0;
    runMsg[POS_1] = THRUPUT_120;

    (void)OSSendIntertask(FDRMCPTASK, _CMTASK_, CF_RUN, BYTE_DATA, runMsg, sizeof(runMsg));

} // fnFdrRun()

static void fnFdrStopX()
{
    uchar    fdrStopMsg[LEN_1];

    fdrStopMsg[POS_0] = GRACEFUL_STOP;

    (void)OSSendIntertask(FDR, _CMTASK_, CF_STOP_RUN, BYTE_DATA, &fdrStopMsg, sizeof(fdrStopMsg));
}

BOOL fTestFeederMCPTask = FALSE;

void DummyCMTask(unsigned long argc, void *argv)
{
    INTERTASK               rMsg;

    OSWakeAfter(5000);


    while (!fTestFeederMCPTask)
    {
        OSWakeAfter(500);
    }

    // Tell the Feeder to initialize
    if (fTestFeederMCPTask)
        fnFdrInitX();

    while(1) 
    {
        if( OSReceiveIntertask (_CMTASK_, &rMsg, OS_SUSPEND) == SUCCESSFUL )
        {
            switch (rMsg.bMsgId) {
            case FC_INIT_RSP:
                {
                    OSWakeAfter(500);
                    fnFdrSetModeX(NORMAL_MODE);                 
                }
                break;
            case FC_SET_MODE_RSP:
                {
                    OSWakeAfter(500);
                    fnFdrRunNormalX();
                }
                break;
            case FC_RUN_RSP:
                {
                    OSWakeAfter(5000);
                    fnFdrStopX();
                    break;
                }
            };          
            
        }
    }
}






#endif //FP_DUMMY_CM
/**********************************************************************/
