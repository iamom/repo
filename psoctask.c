/************************************************************************
*   PROJECT:        DM150
*   COMPANY:        Pitney Bowes
*   AUTHOR :        
*   MODULE NAME:    PsocTask.c
*   REVISION:       
*       
*   DESCRIPTION:    PSOC Manager Task - Interfaces to Print Head PSOC 
*                   and Host PSOC
*
* ----------------------------------------------------------------------
*               Copyright (c) 2016 Pitney Bowes Inc.
*                        37 Executive Dr
*                      Danbury, CT 06810
* ----------------------------------------------------------------------
*************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "posix.h"
#include "sys/stat.h"
#include "services/reg_api.h"
#include "nucleus_gen_cfg.h"
#include "pbos.h"
#include "global.h"
#include "sys.h"
#include "psoctask.h"
#include "errcode.h"
#include "cwrapper.h"
#include "sysdatadefines.h"
#include "sysdata.h"
#include "hal.h"
#include "lcdutils.h"
#include "UTILS.h"


// external functions
extern void fnToggleLED0();
extern void HALWriteFPGAConfig(unsigned char *pConfigData, unsigned int numChars);
extern void HALInitFPGAInterface(void);
//extern void InitUsbmsDev(void);
extern STATUS Find_USBMS_Devices(VOID);

// external variables
extern char *psocImageVNum;
extern char verNum[2][PSOCVERLENGTH];

extern unsigned long  UIC_ID_TYPE;
extern uchar    failCode;

// local global variables
unsigned long MCBLoopCnt = 0 , PHLoopCnt = 0;
unsigned char mcbVersionMismatch = 0;
unsigned char phVersionMismatch = 0;
char pPsocDiagMsg[45];


// PSOC error messages
char PSOCIniting[]  = "PSOC Initializing";
char PSOCSuspending[]  = "PSOC Suspending";
char PSOCMCBVersionOK[] = "Have MCB Version"; 
char PSOCPHVersionOK[] = "Have PH Version"; 
char PSOCMCBProgramming[] = "Programming MCB"; 
char PSOCPHProgramming[] = "Programming MCB"; 
char PSOCSeeding[] = "Seeding PSOCs";
char PSOCSYSStartInitSeeding[] = "SYS Seeding Started";
char PSOCSYSInitSeeding[] = "SYS Seeding of PSOCs Initiated";
char PSOCCMInitSeeding[] = "CM Seeding of PSOCs Initiated";
char PSOCSeedingComplete[]  = "PSOC Seeding Complete";
char PSOCInitComplete[]  = "PSOC Init Complete";
char PSOCMCBNoResp[] = "Unable to Read MCB Version"; 
char PSOCPHNoResp[] = "Unable to Read PH Version"; 
char PSOCCommError[] = "Problem with either PH or MCB Psoc. Suspending Psoc Task"; 
char PSOCStartCommit[]  = "PSOC Issuing Commit";
char PSOCCommitComplete[]  = "PSOC Commit Complete";
char PSOCPHNoPower[]  = "PSOC PH Loss of Power";
char PSOCMCBVersionFail[] = "MCB Version Mismatch"; 
char PSOCPHVersionFail[] = "PH Version Mismatch"; 


/* **********************************************************************
// FUNCTION NAME: fnPreInitializePsocTask
// DESCRIPTION: 
//  This function is called very early in the system's initialization
//  process and before the PSOC task is running.  It gives the PSOC
//  a chance to initialize any data at a very early point in time
//  and before the task runs.
//
// AUTHOR:      Derek DeGennaro
//
// NOTES:
//  This function sets disabling conditions - make sure this is called
//  after the sys status api has been initialized (currently by fnInitSystemStatus).
//
// *************************************************************************/
void fnPreInitializePsocTask(void)
{
    // Set disabling conditions as part of an effort to make sure
    // that the system does not enter the print ready state
    // when the PSOC is programming or reseeding.  If that
    // occurs it is possible to get the dreaded "Red Block".
    fnPostDisabledStatus(DCOND_PSOC_PROGRAMMING_IN_PROG);
    fnPostDisabledStatus(DCOND_PSOC_RESEED_IN_PROG);
}


/* **********************************************************************
// FUNCTION NAME: PsocTask
// DESCRIPTION: This task is responsible for programming the PSOCs of both
//              the host card and the print head.  This task will instruct 
//              the PSOCs to generate new seeds upon each print event
//
// AUTHOR:      R.Day - Complete Software Solutions
//
// *************************************************************************/
void PsocTask( unsigned long argc, void *argv )
{
    int         err;
    unsigned long phResetErrors = 0;
    unsigned long phResetRetrys = 0;
    uchar       psocOk, reProg, status[2];
    INTERTASK   msg;
    char        dbgBuf[80];
    unsigned char wasProgrammed = 0;
    unsigned char phUp;
    unsigned char mcbUp;
    unsigned char ReSeedComplete = 0;
    unsigned long lwSleep;
    BOOL fStatus;
    STATUS stat;

    // ********************************* CODE ***********************************
  
    fnSysLogTaskStart( "PSOCTask", PSOC );  
    mcbVersionMismatch = phVersionMismatch = 0;

    fnSystemLogEntry( SYSLOG_TEXT, PSOCIniting , sizeof(PSOCIniting));

#ifdef CFG_NU_BSP_BEAGLEBONE_BLACK_ENABLE
    BOOLEAN         stdio_en = NU_FALSE;
    //TODO - remove - if stdio is on serial1, suspend this task so its GPIO pins can be used for Serial1
    stat = NU_Registry_Get_Boolean("/beaglebone_black/serial1/stdio", &stdio_en);

    /* Make the count 1 if the device is marked STDIO */
    if(stdio_en == NU_TRUE)
    {
    	OSSuspendTask( PSOC );      /* TBD no error reported ??? */
    }
#endif

    // Init PSOC interface
    HALInitPSOCInterface();
    fnInitPSOCState();

    phUp = mcbUp = 0;
    // SEE IF PRINT HEAD PSOC NEEDS TO BE PROGRAMMED
    // IF IT DOESNT RETURN ITS VERSION NUMBER THEN THE FLASH IS BLANK
    HALResetMCBAndPHLFSR();
    OSWakeAfter( 500 );

    /************************************* MCB PSOC *********************************************/
    /********************************************************************************************/
    //This loop will give the PSOC about 4.5 seconds to power up
    MCBLoopCnt = 0;
    do
    {
        psocOk = GetPsocVerNum( MCB_PSOC );
        if(psocOk != SUCCESSFUL && MCBLoopCnt++ < MAX_PSOC_CHECKS)
            OSWakeAfter(100);
    }while(psocOk != SUCCESSFUL && MCBLoopCnt++ < MAX_PSOC_CHECKS);

    if(psocOk == SUCCESSFUL)
    {
        /* able to communicate with MCB PSOC - check version to see if needs reprogramming */
        reProg = CheckImageVerNum( MCB_PSOC );
		dbgTrace(DBG_LVL_INFO,"MCB PSOC OK\r\n");
    }
 
    if (psocOk == FAILURE || reProg)
    {
        /* either didn't get a version from MCB PSOC or the version numbers didn't match - so program MCB PSOC */
        sprintf( dbgBuf, "\nProg PSOC CM with version: %s\n", psocImageVNum );
        putString(dbgBuf,0);
        err = ProgPsocFlash( MCB_PSOC );
        if(err == 0)
        {
            /* successful MCB PSOC program */
            err = VerifyPsocFlash( MCB_PSOC );
            if (err == 0)
            {
                /* successful MCB PSOC verify */
                err = LockPsocFlash( MCB_PSOC );
            }
        }

        if(err)     /* error case */
        {
            err = ProgPsocFlash( MCB_PSOC );        /* try to program MCB PSOC again */
            if( err == 0)
            {
                /* successful the second time */
                err = VerifyPsocFlash( MCB_PSOC );
                if ( err )
                {
                    /* failed to verify MCB PSOC */
                    LogPsocError( PSOC_PROG_FATAL, MCB_PSOC, 0, 0, "\nFailed again. Suspending PSOC task\n" );
                    putString("Controller PSOC programming failed\n\r", 0);
                    //OSSuspendTask( PSOC );      HOST PSOC COULD NOT BE PROGRAMMED SO SUSPEND IT
                }
                else
                {
                    LogPsocError( PSOC_PROG_FAILURE, MCB_PSOC, 0, 0, "\nFailed to program CM PSOC\n" );
                    
                    /* successful MCP PSOC verify */
                    err = LockPsocFlash( MCB_PSOC );
                }
            }
            else
            {
                /* failed to program MCB PSOC, a second time */
                LogPsocError( PSOC_PROG_FATAL, MCB_PSOC, 0, 0, "\nFailed again. Suspending PSOC task\n" );
                putString("Controller PSOC programming failed\n\r", 0);
            }
        }

        if( err == 0)
        {
            /* successful MCB PSOC lock */
        	fStatus = VerifyPsocChkSum( MCB_PSOC );
            if ( !fStatus )
            {
                /* failed to verify MCB PSOC checksum */
                LogPsocError( PSOC_PROG_FATAL, MCB_PSOC, 0, 0, "\nFailed again. Suspending PSOC task\n" );
                putString("Controller PSOC programming failed\n\r", 0);
            }
        }
        else
        {
            /* failed MCB PSOC lock */
            LogPsocError( PSOC_PROG_FATAL, MCB_PSOC, 0, 0, "\nFailed again. Suspending PSOC task\n" );
            putString("Controller PSOC programming failed\n\r", 0);
        }
        wasProgrammed++;     /* SUCCESSFULLY (RE)-PROGRAMMED */
		dbgTrace(DBG_LVL_INFO,"MCB PSOC Programmed\r\n");
    }
    else
        mcbUp = 1;
    /************************************* MCB PSOC *********************************************/
    /********************************************************************************************/

    /************************************* PH PSOC *********************************************/
    /********************************************************************************************/
    //This loop will give the PSOC about 4.5 seconds to power up
    PHLoopCnt = 0;
    do
    {
        psocOk = GetPsocVerNum( PH_PSOC );
        if(psocOk != SUCCESSFUL && PHLoopCnt++ < MAX_PSOC_CHECKS)
            OSWakeAfter(100);
    }while(psocOk != SUCCESSFUL && PHLoopCnt++ < MAX_PSOC_CHECKS);

    if(psocOk == SUCCESSFUL)
    {
        /* able to communicate with PH PSOC - check version to see if needs reprogramming */
        reProg = CheckImageVerNum( PH_PSOC );
		dbgTrace(DBG_LVL_INFO,"PH PSOC OK\r\n");
    }
  
    if (psocOk != SUCCESSFUL || reProg)
    {
        /* either didn't get a version from PH PSOC or the version numbers didn't match - so program PH PSOC */
        sprintf( dbgBuf, "\nProg PSOC PH with version: %s\n", psocImageVNum );
        putString(dbgBuf,0);
        err = ProgPsocFlash( PH_PSOC );
        if(err == 0)
        {
            /* successful PH PSOC program */
            err = VerifyPsocFlash( PH_PSOC );
            if (err == 0)
            {
                /* successful PH PSOC verify */
                err = LockPsocFlash( PH_PSOC );
            }
        }

        if(err)     /* error case */
        {
            err = ProgPsocFlash( PH_PSOC );     /* try to program PH PSOC again */
            if(err == 0)
            {
                /* successful the second time */
                err = VerifyPsocFlash( PH_PSOC );
                if (err)
                {
                    /* failed to verify PH PSOC */
                    LogPsocError( PSOC_PROG_FATAL, MCB_PSOC, 0, 0, "\nFailed again. Suspending PSOC task\n" );
                    //OSSuspendTask( PSOC );      HOST PSOC COULD NOT BE PROGRAMMED SO SUSPEND IT
                }
                else
                {
                    LogPsocError( PSOC_PROG_FAILURE, PH_PSOC, 0, 0, "\nFailed to program PH PSOC\n" );
                    
                    /* successful PH PSOC verify */
                    err = LockPsocFlash( PH_PSOC );
                }
            }
            else
            {
                /* failed to program PH PSOC, a second time */
                LogPsocError( PSOC_PROG_FATAL, PH_PSOC, 0, 0, "\nFailed again. Suspending PSOC task\n" );
                putString("PH PSOC programming failed\n\r", 0);
            }
        }

        if(err == 0 )
        {
            /* successful PH PSOC lock */
            err = VerifyPsocChkSum( PH_PSOC );        
            if ( err )
            {
                /* failed to verify PH PSOC checksum */
                LogPsocError( PSOC_PROG_FATAL, PH_PSOC, 0, 0, "\nFailed again. Suspending PSOC task\n" );
                putString("PH PSOC programming failed\n\r", 0);
            }
        }
        else
        {
            /* failed PH PSOC lock */
            LogPsocError( PSOC_PROG_FATAL, PH_PSOC, 0, 0, "\nFailed again. Suspending PSOC task\n" );
            putString("PH PSOC programming failed\n\r", 0);
        }
        wasProgrammed++;        /* SUCCESSFULLY (RE)-PROGRAMMED */
		dbgTrace(DBG_LVL_INFO,"PH PSOC Programmed\r\n");
    }
    else
        phUp = 1;
    /************************************* PH PSOC **********************************************/
    /********************************************************************************************/

    if(wasProgrammed)     /* either MCB or PH PSOC was just programmed */
    {
        // SEE IF PRINT HEAD PSOC NEEDS TO BE PROGRAMMED
        // IF IT DOESNT RETURN ITS VERSION NUMBER THEN THE FLASH IS BLANK
        HALResetMCBAndPHLFSR();
        OSWakeAfter( 500 );
        //This loop will give the PSOC about 4.5 seconds to power up
        MCBLoopCnt = 0;
        do
        {
            psocOk = GetPsocVerNum( MCB_PSOC );
            if(psocOk != SUCCESSFUL && MCBLoopCnt++ < MAX_PSOC_CHECKS)
                OSWakeAfter(100);
        }while(psocOk != SUCCESSFUL && MCBLoopCnt++ < MAX_PSOC_CHECKS);

        if(psocOk == SUCCESSFUL)
        {
            fnSystemLogEntry( SYSLOG_TEXT, PSOCMCBVersionOK , sizeof(PSOCMCBVersionOK));
            sprintf(dbgBuf,"MCB Controller PSOC Ver %s\n\r", verNum[MCB_PSOC]);
            fnSystemLogHeader(dbgBuf, 0);
            if( CheckImageVerNum( MCB_PSOC ) == 0 )
                mcbUp = 1;
            else
                mcbVersionMismatch = 1;
        }
        else
        {
            fnSystemLogEntry( SYSLOG_TEXT, PSOCMCBNoResp , sizeof(PSOCMCBNoResp));
        }

        //The Print head PSOC take longer to initialize do to the
        //fact it generates the initial random number seed at boot
        //We must be very patient waiting for the PH PSOC to come up
        //It appears that the PSOC may have an issue with convergence
        //on the random number so if first we don't succeed
        //reset the PH and try again
        phResetRetrys = 0;
        while( phResetRetrys < MAX_PH_PSOC_RESETS )
        {
            //This loop will give the PSOC about 4.5 seconds to power up
            PHLoopCnt = 0;
            do
            {
                psocOk = GetPsocVerNum( PH_PSOC );
                if(psocOk != SUCCESSFUL && PHLoopCnt++ < MAX_PSOC_CHECKS)
                    OSWakeAfter(100);
            }while(psocOk != SUCCESSFUL && PHLoopCnt++ < MAX_PSOC_CHECKS);

            if(psocOk == SUCCESSFUL)
                break;
            else
            {
                phResetErrors++;
                // SEE IF PRINT HEAD PSOC NEEDS TO BE PROGRAMMED
                // IF IT DOESNT RETURN ITS VERSION NUMBER THEN THE FLASH IS BLANK
                HALResetLFSR(PH_PSOC);
                OSWakeAfter( 500 );
            }
            phResetRetrys++;    /* TBD why incremented ? */
        }

        if(psocOk == SUCCESSFUL)
        {
            fnSystemLogEntry( SYSLOG_TEXT, PSOCPHVersionOK , sizeof(PSOCPHVersionOK));
            sprintf(dbgBuf,"Print Head PSOC Ver %s\n\r", verNum[PH_PSOC]);
            fnSystemLogHeader(dbgBuf, 0);
            if( CheckImageVerNum( PH_PSOC ) == 0 )
                phUp = 1;
            else
                phVersionMismatch = 1;
        }
        else
        {
            fnSystemLogEntry( SYSLOG_TEXT, PSOCPHNoResp , sizeof(PSOCPHNoResp));
        }
    }

    if(phUp && mcbUp)
    {
        fnSystemLogEntry( SYSLOG_TEXT, PSOCMCBVersionOK , sizeof(PSOCMCBVersionOK));
        sprintf(dbgBuf,"MCB Controller PSOC Ver %s\n\r", verNum[MCB_PSOC]);
        fnSystemLogHeader(dbgBuf, 0);
		
        fnSystemLogEntry( SYSLOG_TEXT, PSOCPHVersionOK , sizeof(PSOCPHVersionOK));
        sprintf(dbgBuf,"Print Head PSOC Ver %s\n\r", verNum[PH_PSOC]);
        fnSystemLogHeader(dbgBuf, 0);
		
        fnSystemLogEntry( SYSLOG_TEXT, PSOCSeeding , sizeof(PSOCSeeding));
		
        /* can ignore the following return values since if it fails twice during reseed or commit - a fatal error 
            will be thrown then AND there is no one to report the error to anyways ... sys really isn't waiting */
        (void)ReseedPsocs( RESEED_MSG );        // GENERATE INITIAL SEED VALUE      
        (void)ReseedPsocs( COMSEED_MSG );       // COMMIT INITIAL SEED VALUE        
        fnSystemLogEntry( SYSLOG_TEXT, PSOCInitComplete , sizeof(PSOCInitComplete));
        ReSeedComplete = 1;
    }
    else
    {
        /* either MCB PSOC or PH PSOC is not up and running */
        if(mcbVersionMismatch)
        {
            fnSystemLogEntry( SYSLOG_TEXT, PSOCMCBVersionFail , sizeof(PSOCMCBVersionFail));
            LogPsocError( PSOC_VER_MISMATCH, MCB_PSOC, 0, 0, "Mcb Version Mismatch Suspending PSOC task\n" );
        }
        else
        {
            if(phVersionMismatch)
            {
                fnSystemLogEntry( SYSLOG_TEXT, PSOCPHVersionFail , sizeof(PSOCMCBVersionFail));
                LogPsocError( PSOC_VER_MISMATCH, PH_PSOC, 0, 0, "Ph Version Mismatch Suspending PSOC task\n" );
            }
            else
            {
                fnSystemLogEntry( SYSLOG_TEXT, PSOCCommError , sizeof(PSOCCommError));
                LogPsocError( PSOC_COMM_TIMEOUT, MCB_PSOC, 0, 0,
                        "Either MCb or PH Psoc Failed. Suspending PSOC task\n" );
            }
        }
        OSSuspendTask( PSOC );      /* TBD no error reported ??? */
    }

    fnClearDisabledStatus(DCOND_PSOC_PROGRAMMING_IN_PROG);
    fnClearDisabledStatus(DCOND_PSOC_RESEED_IN_PROG);
    /* inform system controller we have made it through initialization */
    OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_PSOC_INIT_COMPLETE, OS_OR);
    fnFinalPSOCState();
    //  fnTempTest();
	

    //We will sleep for about 300ms between checks of PSOC seeds
	// there appears to be about a 20ms delay somewhere,
	// so subtracting the 20ms from the wait time.
    lwSleep = 280;

    fnSysLogTaskLoop( "PSOCTask", PSOC );  
    while (TRUE)
    {
        stat = OSReceiveIntertask( PSOC, &msg, lwSleep );
        if ( stat == SUCCESSFUL)
        {
            switch (msg.bSource)
            {
                case SYS:
                case SCM:
					sprintf(pPsocDiagMsg, "PSOC Task src = %02Xh, msg = %02Xh", msg.bSource, msg.bMsgId);
					fnSystemLogEntry( SYSLOG_TEXT, pPsocDiagMsg, strlen(pPsocDiagMsg));
					
                    // Currently in G900, I don't see that SYS or SCM are re-seeding the
                    // PSOC.  But just to be sure set, and clear the disabling conditions.
                    fnPostDisabledStatus(DCOND_PSOC_RESEED_IN_PROG); 
                    fnSystemLogEntry( SYSLOG_TEXT, PSOCSYSStartInitSeeding , sizeof(PSOCSYSStartInitSeeding));
						
                    fnSystemLogEntry( SYSLOG_TEXT, PSOCSYSInitSeeding , sizeof(PSOCSYSInitSeeding));
                    ReSeedComplete = 0;
                    status[0] = ReseedPsocs( msg.bMsgId );
                    fnClearDisabledStatus(DCOND_PSOC_RESEED_IN_PROG);
                    OSSendIntertask( msg.bSource, PSOC, PSOC_RSP, BYTE_DATA, status, 1 );   
                    break;

                case CM: 
                    switch(msg.bMsgId)
                    {
                        case RESEED_MSG:
                            fnPostDisabledStatus(DCOND_PSOC_RESEED_IN_PROG);
							
                            /* this will perform a reseed and commit */
                            ReSeedComplete = 0;
                            fnSystemLogEntry( SYSLOG_TEXT, PSOCCMInitSeeding , sizeof(PSOCCMInitSeeding));
							
                            HALResetLFSR(PH_PSOC);
                            OSWakeAfter( 500 );
                                
                            //This loop will give the PSOC about 4.5 seconds to power up
                            PHLoopCnt = 0;
                            do
                            {
                                psocOk = GetPsocVerNum( PH_PSOC );
                                if(psocOk != SUCCESSFUL && PHLoopCnt++ < MAX_PSOC_CHECKS)
                                    OSWakeAfter(100);
                            } while ((psocOk != SUCCESSFUL) && (PHLoopCnt++ < MAX_PSOC_CHECKS));

                            if(psocOk == SUCCESSFUL)
                            {
                                fnSystemLogEntry( SYSLOG_TEXT , PSOCPHVersionOK , sizeof(PSOCPHVersionOK));
                            }
                            else
                            {
                                fnSystemLogEntry( SYSLOG_TEXT , PSOCPHNoResp , sizeof(PSOCPHNoResp));
                                status[0] = PSOC_ERROR;
                            }

                            if(PHLoopCnt < 100)     /* TBD - isn't this always below 100 ??? */
                            {
                                /* Reseed */
                                status [0] = ReseedPsocs( RESEED_MSG );     
                                if ( status[0] == 0 )
                                {
                                    /* Commit */
                                    status[0]  = ReseedPsocs( COMSEED_MSG );
                                }
                            }
                            else
                                status[0] = PSOC_ERROR;

                            if(status[0] == 0)
                            {
                                ReSeedComplete = 1;
                            }
								
                            OSSendIntertask( msg.bSource, PSOC, PSOC_GEN_SEED_RSP, BYTE_DATA, status, 1 );
                            break;
          
	                    case COMSEED_MSG:
							if (ReSeedComplete == 1)
							{
		                        fnClearDisabledStatus(DCOND_PSOC_RESEED_IN_PROG);
						
		                        /* since the Commit is done in the "reseed" message */
		                        /* this always returns SUCCESSFUL */
		                        fnSystemLogEntry( SYSLOG_TEXT , PSOCStartCommit , sizeof(PSOCStartCommit));
		                        fnSystemLogEntry( SYSLOG_TEXT , PSOCCommitComplete , sizeof(PSOCCommitComplete));
						
		                        status[0] = 0;
							}
							else
							{
								status[0] = PSOC_ERROR;
							}
						
	                        OSSendIntertask( msg.bSource, PSOC, PSOC_COM_SEED_RSP, BYTE_DATA, status, 1 );
	                        break;

	                    default:
							sprintf(pPsocDiagMsg, "PSOC Task Unknown CM msg ID  = %02Xh", msg.bMsgId);
							fnSystemLogEntry( SYSLOG_TEXT, pPsocDiagMsg, strlen(pPsocDiagMsg));
	                        break;
	                }
					break;
          
                default:
					sprintf(pPsocDiagMsg, "PSOC Task Unknown msg src = %02Xh", msg.bSource);
					fnSystemLogEntry( SYSLOG_TEXT, pPsocDiagMsg, strlen(pPsocDiagMsg));
                    break;
            }
        }
        else
        {
            if(phUp && mcbUp)
            {
                /* both PH and MCP up and running */
                if(fnPrintHeadPowerOn())
                {
                    if(ReSeedComplete)
                    {
                        fStatus = fnPSOCVerifySync( FALSE );
                        if(fStatus == TRUE)
						{
                            fnToggleLED0();
						}
                    }
                }
                else
                {
                    if(ReSeedComplete)
                        fnSystemLogEntry( SYSLOG_TEXT, PSOCPHNoPower, strlen(PSOCPHNoPower));

                    ReSeedComplete = 0;
                }
            }
//			else
//			{
//				sprintf(pPsocDiagMsg, "PH up = %u, MCB up = %u", phUp, mcbUp);
//				fnSystemLogEntry( SYSLOG_TEXT, pPsocDiagMsg, strlen(pPsocDiagMsg));
//			}
        }
    }
}
