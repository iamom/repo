/************************************************************************
    PROJECT:        Horizon CSD
    MODULE NAME:    cbobPriv.c 
    
    DESCRIPTION:    Private PreMessage and PostMessage functions for the bob task.
                    
 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
* HISTORY: 
*				  
* 2018.08.23 CWB - 1H10 - Jira 8243	- Service Code in Barcode is 0.
* - Include file settingsmgr.h.
* -	Modify fnBinServTypeForPreDeb() for 1H10 AND rename it to 	
*    fnBinServCodeForPreDeb().  This function gets the ServiceCodeNumeric, 
*    formats it properly (for Canada) for input as RateCategory in the  
*	 CreateIndicia PSD message, and saves it as bobsBinaryServiceCode. The 
*    preDebit function will copy it from there to rcString based on the EMD
*    paraemeter BP_RATE_INFO_TYPE.  
*    G9 got the Service Code from Dcap Engine, but 1H gets it from the Tablet, 
*    and it is stored by the settings manager. 
*
* 2018.07.07 CWB
  - Fixed a syslog message that was ambiguous.
  - In fnPostGBLPassthroughFromEDM(), added to set the fGetMicroStatusAgain 
    flag for message ID IBTN_BLCMD_ENTER_APPLICATION_MODE, because this msg 
    will change the MicroStatus, and the code works better with this flag set.
    It has now been tested to make sure it doesn't cause a timing problem.  
  - Modified fnPreRunSelfTests() to set the default value of pIPSD_4ByteCmdStatusResp[] 
	differently for BOB_SKIP and BOB__OK.   Also to clear out pIPSD_cmdStatusResp[] 
	and megabobStat[] before sending the message to the ibutton.
  - Modified fnPostRunSelfTests() to handle the error condition better.
For future debugging:
  - Added extern for a new flag, fBobUberDebugDevMessage, that can be used to turn 
	on detailed debugging for very specific messages.  The flag is set in a pre-function 
	and cleared in a post-function of the message that we want the details for.  
	Megabob.c: fnDeviceMessage() will put more info in the syslog when this flag is set.
  - Modified fnPreRunSelfTests() to set the fBobUberDebugDevMessage flag and 
    fnPostRunSelfTests() to clear the fBobUberDebugDevMessage flag. This will turn on 
    extra logging of the self test message.  This is left in as a good example, since 
    this message is only usually called on startup or by PCT.
  - Added clear fBobUberDebugDevMessage flag to the Standard Post Message Task to 
    further limit the time this flag is set. 
*
 23-Mar-18 sa002pe on FPHX 02.12 shelton branch
 	Merging in changes from Janus:
----*
	* 19-Mar-18 sa002pe on Janus tips shelton branch
	*   For Fraca 236321: Changed fnGenStaticImageReqScriptSelect to not alter the indicia type selection.
	*
	*   Fixed some lint warnings.
----*

 02-Aug-17 sa002pe on FPHX 02.12 shelton branch
 	Changed fnFormatMailCreationDate so it can handle years later than 2027.
 
 02-Jun-16 sa002pe on FPHX 02.12 shelton branch
 	Cleaned up fnLoadAdInscOnly.
	
	Merging in changes from Janus 21.3x branch:
----*
	* 09-Feb-16 sa002pe on Janus 21.3x shelton branch
	*	For Fraca 227357: Changed fnDateTimeQuackers to not change currentTextEntrySelection to TURN_OFF_IMAGE.
----*

 30-Nov-15 sa002pe on FPHX 02.12 shelton branch
 	Merged in changes from the 02.13 branch:
----*
	 20-Nov-15 sa002pe on FPHX 02.13 shelton branch
	 	To make sure fnIndiciaQuackers doesn't try to load the indicia before
		everything has been set up (which results in an error being declared):
		1. Added fIndiciaTypeSet.
		2. Changed fnInitImagGen &  to set it to FALSE.
		3. Changed fnSetIGIndiciaType to set it to TRUE.
		4. Changed fnIndiciaQuackers to check it before calling fnLoadIndicia.
----*
*  
*************************************************************************/
#define BOB_CHECK_CURRENCY

/*lint -save -e641 -e715 -e713 -e732 -e545 -e569 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>          // isalnum()
#include "aiservic.h"
#include "ajout.h"
#include "api_mach_ctrl.h"
#include "asrapi.h"
#include "bob.h"
#include "bobutils.h"
#include "chasing_shadows.h"
#include "cjunior.h"
#include "clock.h"          // fnDayOfYear()
#include "cm.h"
#include "cmos.h"
#include "cometclock.h"     // SetSystemClock()
#include "datdict.h"
#include "dcapi.h"
#include "dummy.h"          // ConvertUnitoAscii()
#include "errcode.h"
#include "extreports.h"     // fnPadString()
#include "features.h"
#include "fstub.h"
#include "fwrapper.h"
#include "glob2bob.h"
#include "global.h"
#include "grapext.h"
#include "hash.h"
#include "imagegen.h"
#include "ipsd.h"
#include "junior.h"
//#include "oigts.h"          // fnGetTaskMgtFRANKIT(), fnRecordPostDebit()
#include "oit.h"
#include "pbos.h"
#include "permits.h"        // SetPermitIndex(), DeleteMissingPermits()
#include "report1.h"
#include "sys.h"
#include "tealeavs.h"
#include "commontypes.h"
#include "utils.h"
#include "flashutil.h"
#include "unistring.h"
#include "settingsmgr.h"
#include "trmstrg.h"

#define THISFILE "cbobpriv.c"


// ----------------------------------------------------------------------
//              Protypes for External functions :
// ----------------------------------------------------------------------
extern void fnIncrementKipCount(void);
extern BOOL fnIncrementPieceID(void);
extern unsigned char fnGetSupportedCountryID(void);
extern void fnDumpRawLenToSystemLog(char *inStr, int Len);
extern BOOL fnGetLocalLoggingState(void);
extern unichar *fnGetIndiciaString();
BOOL fnAccessIButton( ushort bMsgID);
extern void SysClockSetByPSD(void);

// ----------------------------------------------------------------------
//             Local definitions:
// ----------------------------------------------------------------------
#define PER_RUN     TRUE
#define PER_PIECE   FALSE

#define UPDATE_REGS_YES     TRUE
#define UPDATE_REGS_NO      FALSE

// For auto inscriptions and auto ads using 3rd gen data capture:
// The third Indicia string is the Auto Inscription selection.
// The fourth Indicia string is the Auto Ad selection.
#define DCI_HRT_AUTO_INSC_IDX   2
#define DCI_HRT_AUTO_AD_IDX     3

#define BOB_TIME_DEBUG

// ----------------------------------------------------------------------
//              Declaration of External variables:
// ----------------------------------------------------------------------

extern const            BobsByteTableType  BobsReportScriptLookup[];
extern PF_RECOVERY_LOG  CMOSpfDebitLogRec;  //CMOS power fail recovery log record
extern char             marriedVaultNumber[16];
extern UINT32   CMOSPieceID;
extern EuroConversion   cmosEuroConversion;


// From megabob.c:
extern uchar ucErrorFlags;
// When this flag is set, megabob will do targetted uber-logging: log more 
//  stuff than is normally practical.  So it is only turned on for the 
//  messages that we are currently debugging.  Since we rarely use RunSelfTests,
//  it is turned on permanently for that message, as an example.
extern BOOL	fBobUberDebugDevMessage;



// Defined in hash.c, Used to create the hash for Login to IPSD.
extern const HASH_DATA_TYPE initChainVariable;

// From CustDat.c:
extern  pfp_t   cmosDcapPfp;
extern DATETIME CMOSLastPrintedMailDate;



// ----------------------------------------------------------------------
//              Declaration of Global variables 
//              (used in creport.c, NOT by other modules):
// ----------------------------------------------------------------------

// Bit defintions for these print flag varaiables are in bob.h
ushort  usIndiciaPrintFlags = 0;
ushort  usPermitPrintFlags = 0;
ushort  usDateTimePrintFlags = 0;


// ----------------------------------------------------------------------
//              Declaration of Local variables:
// ----------------------------------------------------------------------

#define MAX_NUM_PRINT_MAPS  5

typedef struct rBlobIndex{
    ushort usRegGrpIndex;           //this will be the number of blob items
    ushort usUniCharIndex;
}IG_BLOB_INDEX;  

typedef struct rBobImageBlob{
    IG_BLOB_INDEX   rIGBlobIndex;   
    IG_BLOB_DATA    rIGBlob;
}BOB_IG_BLOB;

typedef struct bSelectedComponents{
    uchar       ucNumComponents;
    uchar       ucSelCompType[IG_MAX_COMPONENTS];   // Component type
    ushort      usSelCompID[IG_MAX_COMPONENTS];     // Generated ID for most graphics.  
                                                    //  But for reports, it is the componentType.

}BOB_SEL_COMP_LIST;

typedef struct rBobPrintAreaMaps{
    uchar   ucPrintMapType;                                 // Normal Indicia or Permit or Tax Graphic
    uchar   aRegionCompTypes[MAX_NUM_REGIONS_PER_AREA];     // component types per flash spec
}BOB_PRINT_AREA_MAP; 


static unichar indiciaTextLine[PCL_STRING_LENGTH];
static unichar supplementalTextLine[PCL_STRING_LENGTH];

//Semaphores for Germany Create Indicia and preCreate Indicia data elements
static BOOL    DP_IndiciaOtherDpInfoReadSem = FALSE;       // semaphore indicating fnGetProdConfigListInfoAscii executed
// These aren't used anymore.
//BOOL    DP_IndiciaLineTextReadSem = FALSE;      // semaphore indicating fnGetProdConfigListInfoAscii executed
//BOOL    DP_VasTextReadSem = FALSE;
//BOOL    DP_EkpOrTskNoReadSem = FALSE;


static char    lowWarningFlag;     // Set if already told the OI of the lowWarning condition.


// The number of pages should never exceed 60, but we need a word to be
// compatible with the return type of fnDetermineTotalPages().
static UINT8    ucBobsReportPageNbr = 0;
static UINT16   usBobsTotalPages;

// For Gemini:  This is set when a msg from the EDM may change the 
//              Gemini ProgMode.  It is cleared when we retrieve
//              the MicroStatus from the BootLoader.
/*static */ BOOL    fGetMicroStatusAgain = FALSE;

BOOL	fIndiciaTypeSet = FALSE;

// For Debugging..\/.. CB

// For Debugging ../\.. CB


// ----------------------------------------------------------------------
//              Protypes for Local functions :
// ----------------------------------------------------------------------
void fnInitSelCompList(void);
void fnAdd2SelCompList( UINT8 usCompType, UINT16 usCompID);
void fnInitPrintMaps( UINT8 ucNumRegionMapRecords, const UINT8 *pbRegionMap );
//BOOL fnCheckForComponentInPrintMap(uchar ucMapType, uchar ucCompType); moved to bob.h, called from cbobutil.c
static BOOL fnCheckIGImageParams(void);
char fnLoadNextPageReport( ushort ignore );

// Functions used in table tblIPSDPreDeb[].
static void fnRCFromSuppStr1_IBIChk( void );

static void fnSetSpecialIndicia(void);

// ----------------------------------------------------------------------
//              Declaration of MORE Local variables:
// ----------------------------------------------------------------------

// These ones are for debugging purpose only.
DATETIME    sysTimeFromIPSD;
//long        TempIPSDtime;

// If we need to set it as a constant instead, here is the value for 01/01/2000:
//const long    lIPSD_epoch_in_julian = 0x0256859;       // Set during powerup.
long    lIPSD_epoch_in_julian;       // Set during powerup.

// Set for redirected data once the localXfrCrtlStructPtr has been loaded.  
//  Should be cleared when the last prefunction is run before the message is sent.
BOOL    bGotXfrStructPtr = FALSE;

// This holds the Component Type of the indicia component to print.
UINT16  currentIndiToPrint = TURN_OFF_IMAGE;

uchar aMissingGraphic[MISSING_GRAPHIC_LIST_SZ]; // component type or graphic ID or report type of missing graphic

// utilities for Imag Gen 
IG_FONT_DATA        rIGFontData;
BOB_IG_BLOB         rStaticIGBlob;
BOB_IG_BLOB         rDynamicIGBlob;
IG_COMPONENT_DATA   rIGComponentData;
BOB_PRINT_AREA_MAP  rPrintMap[MAX_NUM_PRINT_MAPS];
BOB_SEL_COMP_LIST   rSelCompList;

// Keep Private !!!!
// This is always equal to the postage value that was in the most recently sent 
// Create Indicia Glob.  So when the debit takes place, this is the value 
// actually debited.  
// This is separate from bobsPostageValue because that may change between the 
//  precreate and the commit, but this will not.   
static UINT32  ulDebitedPostageValue = 0;

// Used to verify AR and DR after a hand-update.
static m5IPSD  m5Verify_AR[ 5 ];
static m4IPSD  m4Verify_DR[ 4 ];
static BOOL    fBobVerifyRegs = FALSE;



/* ***************************************************************************
*
* FUNCTION NAME:    fnEpochToJulian
*
* PURPOSE:          Calculate and set the Julian Epoch constant. 
*                   
* AUTHOR:           Clarisa Bellamy
*
* INPUTS:           none
*                   
*---------------------------------------------------------------------------*/
void fnEpochToJulian( void )
{
    DATETIME sysTime;

    sysTime.bCentury    = 20;
    sysTime.bYear       = 0;
    sysTime.bMonth      = 1;
    sysTime.bDay        = 1;
    sysTime.bDayOfWeek  = 0;
    sysTime.bHour       = 0;
    sysTime.bMinutes     = 0;
    sysTime.bSeconds    = 0;

    lIPSD_epoch_in_julian = MdyToJulian( &sysTime );
    return;
}


/* **********************************************************************

 FUNCTION NAME:  IPSDTimeToSys  

 PURPOSE:       Converts the time in seconds since epoch (01/01/2000) to 
                the systemTime format (DATETIME struct).   
                    
 AUTHOR:        Clarisa Bellamy 

 INPUTS:        IPSDtime is the number of seconds since epoch (01/01/2000 00:00:00)
                src    is pointer to a DATETIME structure, which is the type
                        used by the SetSystemClock and other functions.


 NOTES:     The Julian Epoch is set to the Julian value of the epoch (01/01/2000).
            The julian value of any date is just the number of days since 
            -4712 January 1,  (Julian proleptic Calendar).

 *************************************************************************/

void fnSysTimeToIPSD( long *IPSDtime, DATETIME *src )
{
    long    time;
    long    date;

    // Get days since epoch.
    date = MdyToJulian( src );
    date -= lIPSD_epoch_in_julian;  
    // Multiply by seconds/day to get seconds since epoch.
    date *= (60*60*24);
    // Get seconds since 12:00:00 this morning.
    time = fnTodToSecs( src );
    
    // Return seconds since epoch for this day, plus seconds so far today.
    *IPSDtime = time + date;
    return;
}

/* **********************************************************************

 FUNCTION NAME:  IPSDTimeToSys  

 PURPOSE:       Converts the time in seconds since epoch (01/01/2000) to 
                the systemTime format (DATETIME struct).   
                    
 AUTHOR:        Clarisa Bellamy 

 INPUTS:        dest    is pointer to a DATETIME structure, which is the type
                        used by the SetSystemClock and other functions.
                IPSDtime is the number of seconds since epoch (01/01/2000 00:00:00)

 NOTES:     The Julian Epoch is set to the Julian value of the epoch (01/01/2000).
            The julian value of any date is just the number of days since 
            -4712 January 1,  (Julian proleptic Calendar).

 *************************************************************************/

void fnIPSDTimeToSys( DATETIME *dest, UINT32 IPSDtime )
{
    UINT32    days;       // days since epoch
    SINT32    julianIPSD;  // julian value of IPSD date.
    UINT32    tod;        // TimeOfDay in seconds.


    days = IPSDtime / (60*60*24);
    tod = IPSDtime % (60*60*24);

    fnTodSecsToDatetime( tod, dest, FALSE );

    // Add the days since epoch to the Julian epoch to get the Julian value
    //  of the IPSD date.  
    julianIPSD = (SINT32)days + lIPSD_epoch_in_julian;

    // Convert the julian value of the IPSD date to Sys Time format.
    (void)JulianToMdy( julianIPSD, dest );

    return;    
}

/******************************************************************************
   FUNCTION NAME: fnPostConvertWatchdogTimer
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : After getting the watchdog timer convert it to a next due date
   PARAMETERS   : std post function
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnPostConvertWatchdogTimer(ushort ignore, BOOL msgStat) {
    char    retval;
    DATETIME rDateTime;
    long    lWatchDoggy;

    retval = msgStat;

    if(retval == BOB_OK)
    {
        EndianAwareCopy(&lWatchDoggy, pIPSD_watchDogTmr, sizeof(long));
        fnIPSDTimeToSys( &rDateTime, lWatchDoggy);
        pIPSD_InspectionDueDate[0] = rDateTime.bCentury;
        pIPSD_InspectionDueDate[1] = rDateTime.bYear;
        pIPSD_InspectionDueDate[2] = rDateTime.bMonth;
        pIPSD_InspectionDueDate[3] = rDateTime.bDay;
    }
    else
        retval = msgStat;

    return(retval);
}


// ----------------------------------------------------------------------
//              Global Functions:
// ----------------------------------------------------------------------

/* **********************************************************************

 DUMMY ROUTINES FOR SUPPORTING OPTIONS FOR SPECIAL HANDLING

 PURPOSE:           These functions are called by the device message script handler
                    if a device message does not require a preMessage or postMessage
                    operation.  They do not perform anything.  They exist because the
                    script tables require pre and post message function names.  

                    NOTE:  As a time optimization it would be okay to have the script 
                    handler test for addresses fnStdPreMsgTask and fnStdPostMsgTask, and
                    completely skip the calls.  However, these functions should remain...
                    don't replace the labels in the scripts with nulls... operable functions 
                    are required to ensure that no harm occurs if they are called.
                    
 AUTHOR:            blu

 INPUTS:            
                    
 *************************************************************************/
char fnStdPreMsgTask( ushort msgID ) 
{
/*    switch( msgID )
    {
        default:
            break;
    }
 */
    return( BOB_OK );
}

char fnStdPostMsgTask( ushort msgID, BOOL msgStat ) 
{
    // This is easier than adding functions in bobxfunc for testing:
/*    switch( msgID )
    {   
        default:
            break;
    }   
*/
	fBobUberDebugDevMessage = FALSE;
    return( msgStat );
}


/* ***************************************************************************
*
* FUNCTION NAME:    fnTestIPSDPreTask        fnTestIPSDPostTask
*
* PURPOSE:          Dummy TEST routines for breaking on. 
*                   
* AUTHOR:           Clarisa Bellamy
*
* INPUTS:           standard pre- post- message function inputs.
*                   
*---------------------------------------------------------------------------*/
char fnTestIPSDPreTask( ushort msgID ) 
{
    char retval = BOB_OK;

    // This is easier than adding functions in bobxfunc for testing:
    // Check which msg ID or scriptID is in the line that called this function.
    switch( msgID )
    {
        case SCRIPT_IPSD_PRECOMPUTE_R:
                retval = BOB_SKIP;
            break;

        default:
            break;
    }
    // This is just to put a break point on.
    return( retval );
}


char fnTestIPSDPostTask( ushort msgID, BOOL msgStat ) 
{
    char retval = msgStat;

    if( retval == BOB_OK )
        retval = BOB_EXIT_SUBSCRIPT;

    // This is easier than adding functions in bobxfunc for testing:
    switch( msgID )
    {
        case SCRIPT_TEST:
            break;

        default:
            break;
    }
    // This is just to put a break point on.
    return( retval );
}

// For Debugging..\/.. CB
// For Debugging../\.. CB

/* ***************************************************************************
//
// FUNCTION NAME:       fnIPSDSetupPowerup
// PURPOSE:          
//      Premessage function, to initialize the IPSD driver. 
// INPUTS:           
//   bMsgID  Not used here, required because the function is called indirectly.
// MODS:
// 2009.03.12 Clarisa Bellamy - Added support for Gemini Boot Loader. Check the
//                  family code to determine type of i-button.  If it is an
//                  Asteroid, set new variable bIPSD_ProgMode to ASTEROID, so 
//                  that we won't try to send Boot Loader messages to it. If it
//                  is a Gemini, set the ProgMode to GEMINI_UNKNOWN, until we 
//                  find out what mode it is operating in (LM, AM, SAM), and 
//                  change the PSD type to PSDT_GEMINI.
// AUTHOR: Clarisa Bellamy
*---------------------------------------------------------------------------*/
char fnIPSDSetupPowerup( ushort bMsgID )
{
    struct  msgInfo     messyInfo;
    ulong               appliedTimeout;
    BOOL                fEventity;                  // status received when checking events                     
    ulong               lwEventsReceived;           // copy of event group after waiting for event              
    uchar               *pRplyDataPtr;      // Local pointer to reply buffer.
    int                 i;                  // loop counter.
    BOOL                retval = BOB_OK;
    ushort          internalError = JMEGABOB_STATE_OK;
    
    // Make sure the Julian version of the IPSD Epoch 'constant' is set.
    fnEpochToJulian();

    //clear out the device id container
    memset(pIPSD_DeviceID, 0 , sizeof(pIPSD_DeviceID));

    // This does most of the stuff that would be done by megabob.c: fnDeviceMessage()
    // Reset the Tx and Rx buffers
    memset( juniorBuildMsg, 0xFF, BUILD_BUF_SIZ );
    memset( juniorReplyMsg, 0xFF, REPLY_BUF_SIZ );

    // partially fill the structure that will be used to direct building
    //  of the message.
    messyInfo.msgID = bMsgID;                                               
    messyInfo.xmitBuf = &juniorBuildMsg[0];                                 
    messyInfo.recBuf = &juniorReplyMsg[0];
    messyInfo.xferStruc = &transferStruct;

    // build the message ...
        messyInfo.timeout =     2000;                       // Set it to 2 seconds.
        messyInfo.device  =     IPSD;                       // 
        messyInfo.flags[0] =    RPLY_IPSD;
        messyInfo.flags[1] =    0;
        messyInfo.flags[2] =    0;
        messyInfo.flags[3] =    0;
        messyInfo.statusPlace = bobaVarAddresses[ BOBID_IPSD_CMDSTATUSRESP ].addr; 
        messyInfo.xferStruc->bMsgLen = 0;
        messyInfo.xferStruc->retries = 0;
        messyInfo.xferStruc->msg_number = messyInfo.msgID;      // fill its transfer structure with the 
        messyInfo.xferStruc->device_type = IPSD;                // message ID, mux setting, output      
        messyInfo.xferStruc->pMessage = messyInfo.xmitBuf;      // message pointer, length of message,  
        messyInfo.xferStruc->pDataRecv[0] = messyInfo.recBuf;   // address for the reply, message case, 
        messyInfo.xferStruc->bMsgCase = 0;                      // expected reply... also clear the     
        messyInfo.xferStruc->expectedReply = 8;                 // the error flag and reply length      
        messyInfo.xferStruc->errorFlags = 0;                    // that the driver will fill...         
        messyInfo.xferStruc->replyLen[0] = 0;                   // also specify whether a get response  
        messyInfo.xferStruc->replyLen[1] = 0;                   // also specify whether a get response  
        messyInfo.xferStruc->replyLen[2] = 0;                   // also specify whether a get response  
        messyInfo.xferStruc->replyCnt = 1;
        messyInfo.xferStruc->replyNum = 0;
        messyInfo.xferStruc->eventID = BOB_DRIVERS_EVENTS;  // NOTE!!!! these three lines must be changed to variables 
        messyInfo.xferStruc->errMask = T0_ERROR;
        messyInfo.xferStruc->okMask = T0_SUCCESS;
        messyInfo.xferStruc->getResponse = 0;               // not used in comet 

    appliedTimeout = messyInfo.timeout;                             

    // all drivers handshake to bob through an event register
    (void)OSSetEvents( BOB_DRIVERS_EVENTS, WAITING_FOR_T0, OS_AND );          

    // Send message to IPSD driver to initialize the I-button PSD.
    (void)OSSendIntertask( CIBUTTON, SCM, INIT_IBUTTON_COMMAND, GLOBAL_PTR_DATA, 
                           &transferStruct, sizeof(struct bobDriverIface) );
                            
    // Init message has been sent to I-button driver.  Suspend until the 
    //  reply is received.
    fEventity = OSReceiveEvents( BOB_DRIVERS_EVENTS, T0_SUCCESS | T0_ERROR, 
                                 appliedTimeout, OS_OR, &lwEventsReceived );

    if( fEventity == SUCCESSFUL ) 
    {   // when the handshake arrives, grab the address of the  device's message status storage location

        //PsdSwDownloadPacketRetryCount = 0;
        if( lwEventsReceived & T0_SUCCESS ) 
        {   // if a handshake occurred
            retval = BOB_OK;
        }
        else if (lwEventsReceived & T0_ERROR)
        {
            // timeout, ibutton is incommunicado
            internalError = BOB_IPSD_BL_COM_TIMEOUT_FAILURE;
            ucErrorFlags = transferStruct.errorFlags;
            memset(pIPSD_state, 0, sizeof(pIPSD_state));
            pIPSD_state[3] = eIPSDSTATE_INCOMMUNICADO;
            retval = BOB_SKIP_ALL;
        }
        else 
        {
            internalError = BOB_IPSD_COM_FAILURE;   
            ucErrorFlags = transferStruct.errorFlags;
            memset(pIPSD_state, 0, sizeof(pIPSD_state));
            pIPSD_state[3] = eIPSDSTATE_INCOMMUNICADO;
            retval = BOB_SICK;
        }
    }
    else
    {
        if( fEventity == OS_NO_EVENT_MATCH )
            internalError = BOB_IPSD_COM_OS_NO_EVENT_MATCH;
        else if( fEventity == OS_TIMEOUT )
            internalError = BOB_IPSD_COM_OS_TIMEOUT;
        else
            internalError = BOB_IPSD_HANDSHAKE_FAILURE;
        memset(pIPSD_state, 0, sizeof(pIPSD_state));
        pIPSD_state[3] = eIPSDSTATE_INCOMMUNICADO;
        retval = BOB_SICK;
    }

    #ifdef DEBUG_TRACE
    fnStoreDeviceReply();                                           // emulator builds (for development) maintain a
    #endif          

    // flight recorder of transmitted and received device messages
    if( retval == BOB_OK ) 
    { 
        if( !transferStruct.errorFlags ) 
        {
            //  Since we don't have tables for this message...
            pRplyDataPtr = transferStruct.pDataRecv[0];
    
            memcpy(pIPSD_DeviceID, pRplyDataPtr, sizeof(pIPSD_DeviceID));    // family code + sn + checksum
    
            // First byte is family code...
            bIPSD_familyCode = *pRplyDataPtr++;
            // Next bytes are the Serial number, LSB first.
            for( i = 0; i < IPSD_MFGSN_SZ; i++ )
            {
                pIPSD_MfgSNlsbf[ i ] = *pRplyDataPtr++;
            }
            // Last byte is the CRC, capture it
            bIPSD_MfgSNcrc = *pRplyDataPtr;
            // and remove it from the SN string
            *pRplyDataPtr = 0;

            // Indicate that we got a response from the I-button PSD.
            bPsdType = PSDT_IBUTTON;
            bIPSD_ProgMode = IPSD_MODE_UNKNOWN;
            // Check the family code to find out what type of IBUTTON it is.
            if( bIPSD_familyCode == IPSD_ASTEROID_FAMILYCODE )
            {
                bIPSD_ProgMode = IPSD_MODE_ASTEROID;
            }
            else if( bIPSD_familyCode == IPSD_GEMINI_FAMILYCODE )
            {
                bPsdType = PSDT_GEMINI;
                // Leave the mode as UNKNOWN.  The next step in the
                //  script will read the MicroStatus and determine 
                //  the mode.
                bIPSD_ProgMode = IPSD_MODE_GEMINI_UNKNOWN;
            }
        }
        else // transferStruct.errorFlags 
            retval = BOB_SICK;
    } // end of if BOB_OK
    else
    {
        if( internalError ) 
            fnSetBobInternalErr( internalError );       
    }

    return( retval );
}

/* ***************************************************************************
*
* FUNCTION NAME:    fnIPSDStorePowerup
*
* PURPOSE:          Postmessage function, run after an attempt to initialize
*                   the IPSD driver.   
*               Converts the Manufacturer's Serial Number, rx'd in Hex, LSB first,
*               to an ascii string of the hex number, MSB first, and stores that
*               in the MfgSNAscii array.
*                   
* AUTHOR:           Clarisa Bellamy
*
* INPUTS:           msgID and Status of pre-function and message sent.
*                   
*                   
*---------------------------------------------------------------------------*/
char fnIPSDStorePowerup( ushort msgID, BOOL msgStat )
{
    char                pMyString[ sizeof(puIPSD_MfgSNAscii) ];
    char                *pMyStringPtr;
    int                 convertIt;
    int                 i;
    UINT8               bLen;

    // If couldn't get Manufacturer's Serial Number, blank out the Ascii version of it.
    //  This also makes sure that the string is null-terminated.
    memset( pMyString, 0, sizeof( puIPSD_MfgSNAscii ) );
    if( msgStat == BOB_OK )
    {
        // Translate LittleEndian to BigEnd,Ascii string.  
        // Set pointer to destination
        pMyStringPtr = pMyString;
        // Set counter, start with MSB...
        i = IPSD_MFGSN_SZ;
        while( i )
        {
            // Get next byte, convert it to an int, then to a 2-byte hex string
            i--;
            convertIt = (int)pIPSD_MfgSNlsbf[ i ];                
            sprintf( pMyStringPtr, "%2.2X", convertIt );
            pMyStringPtr +=2;
        }
        bLen = (UINT8)strlen( pMyString );
        // Convert entire string to unichar string and store it.
        fnAscii2Unicode( pMyString, puIPSD_MfgSNAscii, bLen );
    }

    return( msgStat );
}

/* ***************************************************************************
* FUNCTION NAME:        fnPreCheckIPSDType
* PURPOSE:          
*   Premessage function, run before we send the Boot Loader message 
*    ReadMicroStatus, to determine if we are in boot loader mode.
* INPUTS:           
*       msgID of message to send.
* OUTPUT:
*       BOB_OK
* MODS:
* 2009.06.09 Clarisa Bellamy - Since this function should ONLY be called 
*               once, from SCRIPT_INIT_IPSD, we will set a flag indicating
*               that we are in that script.
* 2009.03.04 Clarisa Bellamy - New function                  
*---------------------------------------------------------------------------*/
char fnPreCheckIPSDType( ushort bMsgID )
{   
    if( bIPSD_ProgMode == IPSD_MODE_ASTEROID )
    {
        memset(  pIPSD_Bl_MicroStatusString, 0, sizeof( pIPSD_Bl_MicroStatusString ) );
        strcpy( (char *)pIPSD_Bl_MicroStatusString, "ASTEROID" );
        return( BOB_SKIP );
    }
/*    if( bIPSD_ProgMode == IPSD_MODE_GEMINI_EXITING )
    {
        // We have failed the self tests, and are trying to the ExitApp/RebootGemini
        //  sequence. 
        return( BOB_SKIP );
    } */
    
    memset(  pIPSD_Bl_ReplyRaw, 0, sizeof( pIPSD_Bl_ReplyRaw ) );
    memset(  pIPSD_Bl_MicroStatusString, 0, sizeof( pIPSD_Bl_MicroStatusString ) );
    return( BOB_OK );
}

/* ***************************************************************************
* FUNCTION NAME:        fnPostCheckGeminiMode
* PURPOSE:          
*   Postmessage function, run after we send the Boot Loader message 
*    ReadMicroStatus, to see if we are in boot loader mode.
* INPUTS:           
*   msgID and Status of pre-function and message sent.
* OUTPUT:
* NOTES:
*   1) We shouldn't even get here, if we have an Asteroid or nothing.
* MODS:
* 2009.03.04 Clarisa Bellamy - New function                  
* 2009.03.23 Clarisa Bellamy - If "AM" in MicroStatus, set ProgMode to UNTESTEDAPP.
*---------------------------------------------------------------------------*/
char fnPostCheckGeminiMode(  ushort msgID, BOOL msgStat )
{
    memset(  pIPSD_Bl_MicroStatusString, 0, sizeof( pIPSD_Bl_MicroStatusString ) );
    if( msgStat == BOB_OK )
    {
        strncpy( (char *)pIPSD_Bl_MicroStatusString,
                 (char *)&pIPSD_Bl_ReplyRaw[ IBTN_BL_HEADER_OFFSET ],
                 sizeof( pIPSD_Bl_MicroStatusString ) );
        pIPSD_Bl_MicroStatusString[ ISPD_BL_MICROSTATLEN ] = 0; 

        if( strstr( (char *)pIPSD_Bl_MicroStatusString, "MAXQ1959" ) != NULL_PTR )
        {
            if( strstr( (char *)pIPSD_Bl_MicroStatusString, "SAM" ) != NULL_PTR )
            {
                bIPSD_ProgMode = IPSD_MODE_SECURE_APP;
                return( BOB_EXIT_SUBSCRIPT );
            }
            else if ( strstr( (char *)pIPSD_Bl_MicroStatusString, "AM" ) != NULL_PTR )
            {
                bIPSD_ProgMode = IPSD_MODE_UNTESTEDAPP;
                // We will set to APPLICATION after the SelfTests are passed.
            }
            else if ( strstr( (char *)pIPSD_Bl_MicroStatusString, "LM" ) != NULL_PTR )
            {
                bIPSD_ProgMode = IPSD_MODE_BOOT_LOADER;
                pIPSD_state[3] = eIPSDSTATE_BOOT_LOADER;
				
                // Any other IPSD initialization that needs to be done for an i-button
                //  in boot loader mode, must be done here.  Because the application
                //  messages wont be responded to.
                return( BOB_SKIP_ALL );
            }
            else  // This case should not happen
            {
                //  It doesn't have a known state (SAM, AM, or LM).  
                bIPSD_ProgMode = IPSD_MODE_GEMINI_UNKNOWN;
                msgStat = BOB_SICK;
            }
        }
        else   // This case should also not happen
        {
            // We have a gemini (family code = x64) but it doesn't have MAXQ1959 
            //  in its MicroStatus string.   
            msgStat = BOB_SICK;
        }

        if( msgStat == BOB_SICK )
        {
            // If we ever get here, then we have a gemini (family code = x64) 
            //  but it doesn't have MAXQ1959 in its MicroStatus string, OR 
            //  It doesn't have a known state (SAM, AM, or LM) in its 
            //  MicroStatus string.  This should never happen. So.....
            //  We need to call Dallas/Maxim and ask them what the **** is going on.
            fnDumpStringToSystemLog( "Gemini with unknown mode:" );
            fnDumpStringToSystemLog( (char *)pIPSD_Bl_MicroStatusString );
            pIPSD_state[3] = eIPSDSTATE_BAD_GEMINI;
			
            // This will make it go to manufacturing mode screen and display the 
            //  "IPSD state" instead of going to Error screen and dislaying the 
            //  error code 2509.
            msgStat = BOB_SKIP_ALL;
			
            // Log the error.
            // fnLogSystemError( ERROR_CLASS_JBOB_TASK, BOB_IPSD_UNKNOWN_TYPE_OR_MODE, 0, 0, 0 );
            fnLogError( ERR_TYPE_NORMAL, ERROR_CLASS_JBOB_TASK, BOB_IPSD_UNKNOWN_TYPE_OR_MODE );
        }
    }
    else
    {
        // This is a problem because we don't know what type of IPSD this is.
        msgStat = BOB_SICK;
		
        // Log the error.
        // fnLogSystemError( ERROR_CLASS_JBOB_TASK, BOB_IPSD_UNKNOWN_TYPE_OR_MODE, 0, 0, 0 );
        fnLogError( ERR_TYPE_NORMAL, ERROR_CLASS_JBOB_TASK, BOB_IPSD_UNKNOWN_TYPE_OR_MODE );
    }
	
    return( msgStat );
}


/* **********************************************************************

 FUNCTION NAME:     char fnBobPublishPowerup(uchar stat) 

 PURPOSE:           Evaluates all of the conditions that were encountered during power up and 
                    determines what to admit to the system task
                    
 AUTHOR:            blu

 INPUTS:            
 *************************************************************************/
char fnBobPublishPowerupJanus( uchar stat ) 
{
    BOOL    processErr;
    
    processErr = FALSE;
    if( stat == BOB_SICK ) 
    {
        fnBobErrorStandards( stat );
        processErr = TRUE;
        // if error is from image generator, set vault initialized flag anyway
        if(ErrClassFrombob == ERROR_CLASS_IG_BC)
            fVltDataInitialized = TRUE; // IPSD variables are now accessible
    }
    if( stat == BOB_OK)
        fVltDataInitialized = TRUE;     // IPSD variables are now accessible

    if( processErr ) 
        fnProcessSCMError();

    bobsBeenInitialized = TRUE;                                 

    return( BOB_OK );
}


/* **********************************************************************

 FUNCTION NAME:  fnIPSDPostClockInit    

 PURPOSE:        After the RealTimeClock has been read from the IPSD.
                Calculate the current GMT, using the RTC and clock Offset
                read from the IPSD,then set the system clock.
                    
 AUTHOR:        Clarisa Bellamy 

 INPUTS:        bMsgID  not used here.

 NOTES:         Must use GetPSDParams command to get ClockOffset before 
                this function is run and use GetRTC command to get 
                RealTimeClock value JUST before this command is run.
        
 *************************************************************************/
char fnIPSDPostClockInit( ushort msgID, BOOL msgStat ) 
{
    DATETIME    sysTime;
    long        IPSDtime;
    long        lOffset;


    EndianAwareCopy( &IPSDtime, pIPSD_RealTimeClock, IPSD_SZ_TIME );
    memcpy( pIPSD_RealTimeClock, &IPSDtime, IPSD_SZ_TIME ); //swap endianess of the RTC value from the PSD
    EndianAwareCopy( &lOffset, pIPSD_clockOffset, IPSD_SZ_TIME );
    // Get the GMT time from the IPSD: GMT = RTC + ClockOffset
    IPSDtime += lOffset;

#if defined (JANUS) && !defined (K700)
    {
    extern long lDcapTimestampFudge;
    extern unsigned short wBuildTestMode;

    /* data capture time fudge is in seconds and it applies as long as we are in test mode */
    if (wBuildTestMode == IN_BUILD_TEST_MODE)
        IPSDtime += lDcapTimestampFudge;
    }
#endif

    // !!!cb Set global for debugging only.
    //TempIPSDtime = IPSDtime;

    // Convert GMT in IPSD format to GMT in system time format.
    fnIPSDTimeToSys( &sysTime, IPSDtime );
    // !!!cb Set global for debugging only.
    memcpy( (uchar *)&sysTimeFromIPSD, (uchar *)&sysTime, sizeof( DATETIME ) );

    //Set the system GMT to the IPSD GMT.
    (void)SetSystemClock( &sysTime );
	SysClockSetByPSD();

    return( msgStat );
}



/* **********************************************************************

 FUNCTION NAME:  fnIPSDcalcGMToffset    

 PURPOSE:        Adds all the offsets together to get the total GMT offset
                to send to the IPSD.    
                    
 AUTHOR:        Clarisa Bellamy 

 INPUTS:        bMsgID  not used here.
                    
 *************************************************************************/
char fnIPSDcalcGMToffset( ushort msgID ) 
{
    long gmtOffset = 0;
    long temp_pIPSD_GMTOffset = EndianAwareGet32(&pIPSD_GMTOffset);
    char retval = BOB_OK;

    // Add together the TimeZone offset, drift and (if activated) daylight 
    //  savings offset.

    // Timezone offset is in minutes, convert to seconds and add it.  
    gmtOffset += ((long)(CMOSSetupParams.ClockOffset.TimeZone) * 60); 

    // Drift correction is in minutes, convert to seconds and add it.
    gmtOffset += ((long)(CMOSSetupParams.ClockOffset.Drift) * 60); 

    // If daylight savings time is turned on, add it to the offset (always 1 hour)
    if( CMOSSetupParams.ClockOffset.DaylightSTactivated == TRUE )
        gmtOffset += ((long)(CMOSSetupParams.ClockOffset.DaylightSToff) * 60); 

    // GMT correction is in seconds
    gmtOffset += CMOSSetupParams.ClockOffset.GMTcorrection; 

    // skip setting the gmtoffset if it is already correct
    if(memcmp((char*) &gmtOffset, (char*) &temp_pIPSD_GMTOffset, sizeof(gmtOffset)) == 0)
    {
        retval = BOB_SKIP;
    }
    // skip setting the gmtoffset if the ibutton state won't allow it
    else if(    (pIPSD_state[3] == eIPSDSTATE_WITHDRAWALPENDING)      || 
                (pIPSD_state[3] == eIPSDSTATE_WITHDRAWN)              ||
                (pIPSD_state[3] == eIPSDSTATE_GERM_WITHDRAWALPENDING) || 
                (pIPSD_state[3] == eIPSDSTATE_GERM_WITHDRAWN)   )
    {
        retval = BOB_SKIP;
    }
    // if the values in CMOS are out of range...
    else if(fnCheckGMTOffset(gmtOffset) == OUT_OF_RANGE)
    {
        // try resetting the drift to it's default
        CMOSSetupParams.ClockOffset.Drift = 0;
        gmtOffset = ((long)(CMOSSetupParams.ClockOffset.TimeZone) * 60); 
        gmtOffset += ((long)(CMOSSetupParams.ClockOffset.Drift) * 60); 
        if( CMOSSetupParams.ClockOffset.DaylightSTactivated == TRUE )
            gmtOffset += ((long)(CMOSSetupParams.ClockOffset.DaylightSToff) * 60);

        // GMT correction is in seconds
        gmtOffset += CMOSSetupParams.ClockOffset.GMTcorrection;

        if(fnCheckGMTOffset(gmtOffset) == OUT_OF_RANGE)
        {
            // then try resetting the time zone to it's default
            CMOSSetupParams.ClockOffset.TimeZone = (short) fnFlashGetWordParm(WP_TIME_LOCALIZATION_OFFSET);
            gmtOffset = ((long)(CMOSSetupParams.ClockOffset.TimeZone) * 60); 
            gmtOffset += ((long)(CMOSSetupParams.ClockOffset.Drift) * 60); 
            if( CMOSSetupParams.ClockOffset.DaylightSTactivated == TRUE )
                gmtOffset += ((long)(CMOSSetupParams.ClockOffset.DaylightSToff) * 60);

            // GMT correction is in seconds
            gmtOffset += CMOSSetupParams.ClockOffset.GMTcorrection;

            if(fnCheckGMTOffset(gmtOffset) == OUT_OF_RANGE)
            {
                // then try resetting daylight savings to it's default 
                CMOSSetupParams.ClockOffset.DaylightSToff = (char) fnFlashGetByteParm(BP_FACTORY_DST_OFFSET);
                gmtOffset = ((long)(CMOSSetupParams.ClockOffset.TimeZone) * 60); 
                gmtOffset += ((long)(CMOSSetupParams.ClockOffset.Drift) * 60); 
                if( CMOSSetupParams.ClockOffset.DaylightSTactivated == TRUE )
                    gmtOffset += ((long)(CMOSSetupParams.ClockOffset.DaylightSToff) * 60);
                     
                // GMT correction is in seconds
                gmtOffset += CMOSSetupParams.ClockOffset.GMTcorrection;

                if(fnCheckGMTOffset(gmtOffset) == OUT_OF_RANGE)
                {
                    // then try resetting GMT correction
                    CMOSSetupParams.ClockOffset.GMTcorrection = 0;
                    gmtOffset = ((long)(CMOSSetupParams.ClockOffset.TimeZone) * 60); 
                    gmtOffset += ((long)(CMOSSetupParams.ClockOffset.Drift) * 60); 
                    if( CMOSSetupParams.ClockOffset.DaylightSTactivated == TRUE )
                        gmtOffset += ((long)(CMOSSetupParams.ClockOffset.DaylightSToff) * 60);
                         
                    // GMT correction is in seconds
                    gmtOffset += CMOSSetupParams.ClockOffset.GMTcorrection;
                }
            }
        }

        fnLogError(ERR_TYPE_NORMAL, ERROR_CLASS_JBOB_TASK, BOB_CMOS_GMTOFFSET_OUT_OF_RANGE);
    }

    if(retval == BOB_OK)
    {
        // Store where the bobTask will send it to the IPSD.
        EndianAwareCopy( pIPSD_GMTOffset, (char *)&gmtOffset, IPSD_SZ_TIME );
    }

    psdClockFlag = TRUE;

    return( retval ); 
}



/* **********************************************************************

 FUNCTION NAME:  fnIPSDcheckLoginState  

 PURPOSE:       Redirect function.  Checks if the IPSD
                is already logged in.  IF so, return dummy script.
                If not, returns the script to login.
                    
 AUTHOR:        Clarisa Bellamy 

 INPUTS:        bMsgID  Should be the login script id.
                    
 *************************************************************************/
UINT16 fnIPSDcheckLoginState( ushort msgID ) 
{
    uchar   state;
    UINT16      retVal = DUMMY_SCRIPT;

    //  This is really bad, but need to do something quick.
    state = pIPSD_state[3];

    switch (state)
    {
        case eIPSDSTATE_FULLPOSTAL:
        case eIPSDSTATE_GERM_FULLPOSTAL:
            retVal = msgID;
            // If the vault is in a full postal state, check to see of 
            // the UIC is suppose to marry the vault
            if (fnFlashGetByteParm( BP_SAME_VAULT_REQUIRED)) 
            {
                // if the UIC is to be married to a specific vault and it is not already married...
                if (marriedVaultNumber[0] == 0)
                {
                    memcpy( &marriedVaultNumber[0],
                            &pIPSD_indiciaSN[0], 
                            sizeof(marriedVaultNumber));
                }
            }
            break;

        case eIPSDSTATE_LOGGEDIN:
        case eIPSDSTATE_GERM_LOGGEDIN:
            // If we're playing on the emulator, we could end up not doing the marriage because
            // we're already in the logged-in state by the time this routine is called,
            // so just do the marriage without anything else
            retVal = DUMMY_SCRIPT;
            // If the vault is in a full postal state & is already logged in, check to see of 
            // the UIC is suppose to marry the vault
            if (fnFlashGetByteParm( BP_SAME_VAULT_REQUIRED)) 
            {
                // if the UIC is to be married to a specific vault and it is not already married...
                if (marriedVaultNumber[0] == 0)
                {
                    memcpy( &marriedVaultNumber[0],
                            &pIPSD_indiciaSN[0], 
                            sizeof(marriedVaultNumber));
                }
            }
            break;

        default:
            retVal = DUMMY_SCRIPT;
            break;

    }
    return (retVal);
}


/* **********************************************************************

 FUNCTION NAME: fnIPSDloginPre

 PURPOSE:       Create the User Login code byt getting the nonce, appending
                the userPassword, and creating a Hash.  
                    
 AUTHOR:        Clarisa Bellamy             

 INPUTS:        bMsgID  not used here.
                    
 *************************************************************************/
char fnIPSDloginPre( ushort msgID ) 
{
    BOOL    retval = BOB_OK;
    uchar    pHashSource[ IPSD_SZ_NONCE + IPSD_SZ_USER_PWD ];
    HASH_DATA_TYPE  sHashDest;   //temp storage of Hash value.
    uchar    *pEMD_UserPassword;
    
    // get the password pointer from the EMD
    pEMD_UserPassword = fnFlashGetIBPackedByteParm(IPB_PB_USER_PASSWORD);

    //  Copy nonce, then password into source buffer.
    memcpy( pHashSource, pIPSD_nonce, IPSD_SZ_NONCE );
    memcpy( &pHashSource[ IPSD_SZ_NONCE ], pEMD_UserPassword, IPSD_SZ_USER_PWD );

    // Compute Hash
    (void)ComputeSHA1Hash( (uchar *)pHashSource, &sHashDest, (UINT32)(IPSD_SZ_NONCE + IPSD_SZ_USER_PWD), 
                           &initChainVariable );

    // Flag an error at compile time if the dest array is set too small.
#if ( IPSD_SZ_LOGIN_DATA < 20 )
#error
#else
    // Copy Hash into login data buffer.
    memcpy( pIPSD_loginData, sHashDest.value, sizeof( HASH_DATA_TYPE ) );
	fnReverseIt( pIPSD_loginData, sizeof( HASH_DATA_TYPE ) );
#endif
    return( retval );
}


/* **********************************************************************

 FUNCTION NAME: fnIPSDloginPost

 PURPOSE:       Verify login.   
                    
 AUTHOR:        Clarisa Bellamy             

* INPUTS:           msgID and Status of pre-function and message sent.
                    
 *************************************************************************/
/*char fnIPSDloginPost( ushort msgID, BOOL msgStat ) 
{
    // Don't know if we need to do anything yet...
    return( msgStat );
}
*/


char UpdateControlSum(void)
{
	char msgStat = true;
	tIPSD_LOG_LMNT_REFILL* pRefillLog = NULL_PTR;
	uchar i, index = 0;
	long count, maxCount = 0;
	MONEY ascReg, descReg;


	msgStat = fnAccessIButton(I_BUTTON_REFILL_LOGS);

	if(msgStat)
	{
		for( i = 0; i < IPSD_LOG_MAX_REFILL; i++)
		{
			pRefillLog = (tIPSD_LOG_LMNT_REFILL*) &pIPSD_RefillLog[i * sizeof(tIPSD_LOG_LMNT_REFILL)];
	
			//Find the largest refill count
			EndianAwareCopy(&count, pRefillLog->pRefillCount, IPSD_SZ_REFILL_CNT );
			if(count > maxCount)
			{
				maxCount = count;
				index = i;
			}
		}

		pRefillLog = (tIPSD_LOG_LMNT_REFILL*) &pIPSD_RefillLog[index * sizeof(tIPSD_LOG_LMNT_REFILL)];

		// Convert Ascending Register to a 5-byte value, and store.
		memset( ascReg, 0, SPARK_MONEY_SIZE );
		memcpy( ascReg, pRefillLog->m5AR,  IPSD_SZ_ASC_REG );
 
		// Convert Descending Register to a 5-byte value, and store.
		memset( descReg, 0, SPARK_MONEY_SIZE );
		memcpy( &descReg[ SPARK_MONEY_SIZE - IPSD_SZ_DESC_REG ], pRefillLog->m4DR,  IPSD_SZ_DESC_REG );
    
		fnAddMoneyParameters( mStd_ControlSum, descReg, ascReg );
	}
	else
	{
        sprintf( pBobsLogScratchPad, "Cannot update Control sum from Refill Log" );
        fnDumpStringToSystemLog( pBobsLogScratchPad );
		msgStat = BOB_SICK;
	}

	return (msgStat);
}





/* **********************************************************************
 FUNCTION NAME: fnSaveRefillData

 PURPOSE:       Save latest refill data in CMOS structure   
                    
 AUTHOR:        Steve Terebesi  

* INPUTS:       None

* OUTPUTS:      None
 *************************************************************************/
void fnSaveRefillData(void)
{
//  Store AR and DR refill data into CMOS
    fnStoreRefillAR((MONEY *)m5IPSD_ascReg);
    fnStoreRefillDR((MONEY *)mStd_DescendingReg);
}



/***********************************************************************

 FUNCTION NAME:    fnComputeControlSum  

 PURPOSE:           Creates the 5-byte version of the descending register from the
                    4-byte version read from the IPSD, and creates the Control Sum 
                    (which used to be in the Myko PSD) from the Ascending and 
                    Descending registers.      
                    
 AUTHOR:            Clarisa Bellamy (and blu)
 INPUTS:            Standard Post-message function inputs.
 HISTORY:
 2010.07.13 Clarisa Bellamy - Add code to verify that the hand-updated register 
                        values match the values just read in, and to put an error
                        in the systemLog and the errorLog if they don't.  
                    
 *************************************************************************/
char fnComputeControlSum( ushort msgID, BOOL msgStat ) 
{
    UINT32  ulLclDR;
    UINT32  ulPsdDR;
    BOOL    fBadHandUpdate = FALSE;
	MONEY   controlSum;

    // Are registers flagged for verification.
    if( fBobVerifyRegs == TRUE )
    {
        // The LAST time this was called, the registers were updated by hand, 
        //  this just verifies that the values just read in match the values 
        //  that we used when we updated them by hand.  We are guessing that 
        //  both are out of range.
        if( memcmp( m5Verify_AR, m5IPSD_ascReg, 5 ) != 0 )
        {
            //Add an entry to the system log and exception log file.
            fnDumpStringToSystemLog( "fnComputeControlSum: AR out of synch!" );
            fBadHandUpdate = TRUE;
       }

       if( memcmp( m4Verify_DR, m4IPSD_descReg, 4 ) != 0 )
       {
            //Add an entry to the system log and exception log file.
            memcpy( &ulLclDR, m4Verify_DR, sizeof( ulLclDR ) );
            memcpy( &ulPsdDR, m4IPSD_descReg, sizeof( ulLclDR ) );
            sprintf( pBobsLogScratchPad, "DR out of synch! Lcl=%ld psd=%ld", (long)ulLclDR, (long)ulPsdDR );
            fnDumpStringToSystemLog( pBobsLogScratchPad );
            fBadHandUpdate = TRUE;
        }
        if( fBadHandUpdate == TRUE )
        {
            // Record it in the error log so we know if it happens.  But don't report an error
            //  this is a temporary problem that has already been fixed by the most recent 
            //  read of the register.
            fnLogError( ERR_TYPE_NORMAL, ERROR_CLASS_JBOB_TASK, BOB_BAD_REGISTER_HANDUPDATE );
       }
   }

    if( msgStat == BOB_OK )
    {
        // Convert Descending Register to a 5-byte value, and store.
        memset( mStd_DescendingReg, 0, SPARK_MONEY_SIZE );
        memcpy( &mStd_DescendingReg[ SPARK_MONEY_SIZE - IPSD_SZ_DESC_REG ], m4IPSD_descReg,  IPSD_SZ_DESC_REG );
        // Create and store the Control Sum
        fnAddMoneyParameters( controlSum, mStd_DescendingReg, m5IPSD_ascReg );

//      Compare current Checksum value to value calculated from Refill Log
		if(memcmp(controlSum, mStd_ControlSum, sizeof(controlSum)) != 0)
		{
//          If it fails, make sure the Refill Log checksum value is up to date
			msgStat = UpdateControlSum();

//          If good, compare the current CS to the Refill Log CS again
			if(msgStat == BOB_OK)
			{
				if(memcmp(controlSum, mStd_ControlSum, sizeof(controlSum)) != 0)
				{
//                  If the check fails again, compare the current CS value to
//                  CS value of the last refill data stored in CMOS
                    MONEY  lastCS;
                    SINT32 refillAmt;
                    MONEY  convAmt;
                    MONEY  newCS;
                    MONEY  lastAR;
                    MONEY  lastDR;

//                  Calculate the CS value of the last refill from CMOS
                    fnGetLastRefillAR(&lastAR);
                    fnGetLastRefillDR(&lastDR);
                    fnAddMoneyParameters((UINT8 *)&lastCS, lastAR, lastDR);

//                  Compare current CS value to last refill CS value
    				if (memcmp(controlSum, lastCS, sizeof(controlSum)) != 0)
                    {
//                      If they still don't match, last refill AR and DR values 
//                      may not have been updated yet.  Add the current refill 
//                      amount to last refill CS and compare the CS values again
                        fnGetLastRefillAmount(&refillAmt);

//                      Convert refill amount to MONEY format                    
                        fnDouble2BinFive((UINT8 *)convAmt, fnConvertLongToDouble(refillAmt, 0), TRUE);

//                      Calculate new CS value from old refill AR/DR and refill amount
                        fnAddMoneyParameters(newCS, lastCS, convAmt);

//                      Compare the new CS to the current CS        
        				if (memcmp(controlSum, newCS, sizeof(controlSum)) != 0)
                        {
//                          Values still do not match.  Set the error                      
    	    				fnSetBobInternalErr(BOB_BAD_REGISTER_HANDUPDATE);
    		    			msgStat = BOB_SICK;
                        }
                        else
                        {
//                          Control sum verified.  Replace bad value calculated from
//                          refill logs with the correct value
                            memcpy(mStd_ControlSum, newCS, sizeof(mStd_ControlSum));
                        }
                    }
                    else
                    {
//                      Control sum verified.  Replace bad value calculated from
//                      refill logs with the correct value
                        memcpy(mStd_ControlSum, lastCS, sizeof(mStd_ControlSum));
                    }
				}
			}
		}
    }

	if(msgStat != BOB_OK)
	{
        fnLogError( ERR_TYPE_NORMAL, ERROR_CLASS_JBOB_TASK, BOB_BAD_REGISTER_HANDUPDATE );
        sprintf( pBobsLogScratchPad, "Cannot verify Control Sum " );
	}

    // Make sure this reset before exiting.
    fBobVerifyRegs = FALSE;
    return( msgStat ); 
}


/* **********************************************************************

 FUNCTION NAME:    fnConvertPSDParamMoneys  

 PURPOSE:           Creates the 5-byte version of IPSD 4-byte money
                    values.
 AUTHOR:            Clarisa Bellamy (and blu)
 INPUTS:            Standard Post-message function inputs.
                    
 *************************************************************************/
char fnConvertPSDParamMoneys( ushort msgID, BOOL msgStat ) 
{
    if( msgStat == BOB_OK )
    {
        // Convert MaxDescReg to a 5-byte value, and store.
        memset( mStd_MaxDescReg, 0, SPARK_MONEY_SIZE );
        memcpy( &mStd_MaxDescReg[ SPARK_MONEY_SIZE - IPSD_SZ_DESC_REG ], m4IPSD_maxDescReg,  IPSD_SZ_DESC_REG );
        // Convert PostageMin to a 5-byte value, and store.
        memset( mStd_MinPostage, 0, SPARK_MONEY_SIZE );
        memcpy( &mStd_MinPostage[ SPARK_MONEY_SIZE - IPSD_SZ_MIN_POSTAGE ], m4IPSD_postageMin,  IPSD_SZ_MIN_POSTAGE );
    
        // Convert PostageMax to a 5-byte value, and store.
        memset( mStd_MaxPostage, 0, SPARK_MONEY_SIZE );
        memcpy( &mStd_MaxPostage[ SPARK_MONEY_SIZE - IPSD_SZ_MAX_POSTAGE ], m4IPSD_postageMax,  IPSD_SZ_MAX_POSTAGE );
    }

    return( msgStat ); 
    
}

/******************************************************************************
   FUNCTION NAME: fnConvertPSDParamDateLimits
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : After getting the PSD param convert the back date limit and
                : advanced date limit from seconds to days
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnConvertPSDParamDateLimits(ushort ignore, BOOL msgStat)
{
    long lTemp = 0;

    if( msgStat == BOB_OK )
    {
        memcpy(&lTemp, &pIPSD_dateAdvanceLimit, sizeof(long));
        bIPSD_dateAdvanceLimitDaze = 0x000000FF & (UINT32)(lTemp/SECONDS_IN_1DAY);    
        memcpy(&lTemp, &pIPSD_backdateLimit, sizeof(long));
        bIPSD_backdateLimitDaze = 0x000000FF & (UINT32)(lTemp/SECONDS_IN_1DAY);
    }

    return(msgStat);
}

/* **********************************************************************
 FUNCTION NAME:     fnStrFind3charMonth 
 PURPOSE:           
    Called by fnConvertFirmwareVersion to find the beginning of a date in a string.
    This function just finds the first 3-char match to the beginning of a month
    name.  It does not verify if it really starts a string.  

 INPUTS:            
    pInp    - Pointer to the string to check.
    pOutp   - Pointer to the place to store the address of the substring, if it
              was found.
 OUTPUTS:
    Pointer to the start of the substring, if found.  If not, this is set to NULL_PTR.
 RETURNS:
    TRUE if substring found, else FALSE.

 NOTES:
  1. The search is case-insensitive.
  2. Matches are only tested at the beginning of a word.
  2. This is super simple, so the calling function must verify if it really is 
     a date.
      For example ,"We march forward." will return TRUE, as will "Jane Doe".
      However "taught" will return FALSE, and "date 1Aug 11" will return FALSE.
//---------------------------------------------------------------------------*/
const char *apMonths[12] =
{
    "JAN","FEB","MAR","APR","MAY","JUN",
    "JUL","AUG","SEP","OCT","NOV","DEC"
};
BOOL fnStrFind3charMonth( const char *pInp, const char **pOutp )
{
	char		temp[128];
    BOOL        fFoundIt = FALSE;
	char		*pStr = NULL;
    int         num;
    UINT8       loop;
	int			i;

	num = strlen(pInp);
	if(num > sizeof(temp))
		num = sizeof(temp)-1;

	strncpy(temp, pInp, num);
	for(i = 0; i < num; i++)
    {
        temp[i] = (char)toupper(temp[i]);
    }

	for(loop = 0; loop < 12; loop++)
	{
		pStr = strstr( temp, apMonths[ loop ]);
		if(pStr != NULL)
		{
			fFoundIt = TRUE;
			num = pStr - temp;
			*pOutp = pInp + num;
			break;
		}
    }

    return( fFoundIt );
}


/* **********************************************************************
 FUNCTION NAME:    fnConvertFirmwareVersion 

 PURPOSE:           
    This function examines the firmware version string tha was retrieved from
     the IPSD.  If it is of the expected format, then it will make a smaller 
     string out of xxxxxxxxx, and store that in the char array pIPSD_FirmwareVerSubStr.
    Then it will attempt to convert that string to a number, and store that 
     number in lIPSD_FirmwareVerNum.
    Then it will examine the original string again, and attempt to retrive the
     date from it, expecting it in the form of Mon dd yyyy. Then it will store 
     the date string in pIPSD_FirmwareVerDateStr.

 INPUTS:            
    Standard Post-message function inputs.
 OUTPUTS:
    IF it can, it fills up      pIPSD_FirmwareVerSubStr
                                lIPSD_FirmwareVerNum
                                pIPSD_FirmwareVerDateStr
 RETURNS:
    msgStat (input argument unaltered)

 NOTES:
  1. The firmware revision string is assumed to have the format:
        * Firmware Revision version * Date
    Where * is zero or more characters, "Firmware Revision" is an exact match, 
    the version is of the format xx.xx.xx, and the Date is mon dd yyyy.
  2. Just found out that the first "character" in the string is really its length!

 AUTHOR:    Clarisa Bellamy (and blu)
//---------------------------------------------------------------------------*/
char fnConvertFirmwareVersion( ushort msgID, BOOL msgStat ) 
{
    const char  *pStart;        // Points to the start of a substring.
    const char  *pEnd;          // Points to the end of a substring.
    const char  *pInp;          // Traveling ptr.
    BOOL        foundIt = FALSE;
    int         num;            // dest for %n in sscanf (num chars read)
    int         count;          // number of sucsessful sscanf items copied.
    int         a, b, c;
    UINT16      uwLen = 0;

    if( msgStat == BOB_OK )
    {
        pStart = NULL_PTR;
        pEnd = NULL_PTR;
        foundIt = FALSE;
        pInp = (const char *)pIPSD_FirmwareVer +1;

        // Do a search for the starting delimiter: "Firmware Revision"
        pStart = strstr( pInp, "Firmware Revision" );
        if( pStart != NULL_PTR )
        {
            pInp = pStart + strlen( "Firmware Revision" );
                            foundIt =TRUE;
                        }

        // If starting delimiter found...            
        if( foundIt == TRUE )
        {
            num = 0;
            pStart = pInp;
            // Make adjustments in the starting pointer to remove any leading characters 
            //  that are not numbers or letters, (including spaces.) 
            while( *pStart )
            {
                if( isalnum( *pStart ) ) 
                    break;
                pStart++;
            }

            // Start of revision substring has been found.
            // Find the end of the version ... first white-space
            foundIt = 0;
                num = 0;
            // Make sure the string is at least 1-char long.
            if( *pStart )
            {
                foundIt = TRUE;
                pEnd = pStart + IPSD_MAXSZ_FWVER_SUB_LEN - 1;
            } // End of found a string that should be the version number. 
        }

        // Copy the string following "Firmware Revision" to the substring;
        if( pEnd && pStart )
        {
            // IF the substring is too long, just truncate to the length of the destination.
            uwLen = (UINT16)(pEnd - pStart);
            if( uwLen > IPSD_MAXSZ_FWVER_SUB_LEN )
                uwLen = IPSD_MAXSZ_FWVER_SUB_LEN;
			memset(pIPSD_FirmwareVerSubStr, 0, IPSD_MAXSZ_FWVER_SUB_LEN );
            strncpy( pIPSD_FirmwareVerSubStr, pStart, uwLen );
        }

        // -----------------------------
        // Convert the substring to an int, since current routines only look for a number;
        a = b = c = 0;      // Set default values, in case not found in scan.
        count = sscanf( pIPSD_FirmwareVerSubStr, "%4d.%4d.%4d", &a, &b, &c );
        lIPSD_FirmwareVerNum = (UINT16)a;
		lIPSD_FirmwareVerNum <<= 8;
		lIPSD_FirmwareVerNum += (UINT16)b;
		lIPSD_FirmwareVerNum <<= 8;
		lIPSD_FirmwareVerNum += (UINT16)c;
            
        // -----------------------------
		// Because the Indicia Type values for 3.x an 5.x were reused in later versions,
		//  check the version here, and set a flag.  This way we don't have to check it
		//  all over the place.
		if(   (   (lIPSD_FirmwareVerNum >= 0x00030000)
			   && (lIPSD_FirmwareVerNum <  0x00040000) )
		   || (   (lIPSD_FirmwareVerNum >= 0x00050000)
			   && (lIPSD_FirmwareVerNum <  0x00060000) )  )
		{
			fIPSD_FWVerModsIndiciaType = TRUE;
		}


        // Look for the date string. -------------------
        foundIt = FALSE;
        pStart = pEnd = NULL_PTR;
        uwLen = 0;
        while( foundIt == FALSE )
        {
            // Start looking for the month at the beginning of the version string.
            pInp = (char *)pIPSD_FirmwareVer +1;
            foundIt = fnStrFind3charMonth( pInp, &pStart );

            if( foundIt )
            {
                foundIt = FALSE;
                // Get past any non-alpha characters at the front...
                while( *pStart )
                {
                    if( isalpha( *pStart ) ) 
                        break;
                    pStart++;
				}
                // Find the end of the month string.
                pInp = pStart + 4;  //3 char month + space
                // If the month is followed by 2 more numbers, then it's the date. 
                count = sscanf( pInp, "%d %d%n", &a, &b, &num );
                if( count == 2 )
                {
                    foundIt = TRUE;
                    pEnd = pInp + 7; //2 character day + space + 4 character year
                    uwLen = (UINT32)(pEnd - pStart);    
                    break;
                }
            }
            else
            {
                // No 3-char month in the rest of the string. So we can't find it, stop looking.
                pStart = NULL_PTR;
                break;
            }
        }

        if( (pStart != NULL_PTR) && (uwLen > 0) )
        {
            if( uwLen >= IPSD_MAXSZ_FWVER_DATE_LEN )
                uwLen = IPSD_MAXSZ_FWVER_DATE_LEN;
            strncpy( pIPSD_FirmwareVerDateStr, pStart, uwLen );
            pIPSD_FirmwareVerDateStr[ uwLen ] = 0;
        }
        // End of looking for the date string.------

    } // end of if( msgState == BOB_OK )
    
    return( msgStat );
}


//**********************************************************************
// FUNCTION NAME:   fnPostIPSDGetParamList  
//
//       PURPOSE:   Calls the subroutine to parse the Parameter List glob 
//                  data using the data definition records in Flash, into 
//                  the ipsd variables that can be accessed using the 
//                  variable IDs and bobamat tables.
//          Then does some standard stuff to the data:
//          1) Converts any IPSD 4-byte money values into the 5-byte values
//             by the rest of the UIC.
//          2) Calculate the Derived Piece Count.
//                  
//        AUTHOR:   Clarisa Bellamy
//
//        INPUTS:   Standard PostMessage function inputs:
//                      msgID is ignored.
//                      msgStat - if not BOB_OK, skip all processing 
//        RETURN:   Standard PostMessage return values. 
//                      Same as msgStat input.
//         NOTES:   After all of the distribution is complete, a new step has been
//                  added.  For Backward compatibility, variables that are not availble
//                  from older iButtons, need to be set to a default value.  
//
//          2/1/05  The above note regarding setting of defaults is no longer true.
//                  The default(s) for backward compatibility in parameter lists can
//                  be set by the compiler, where required.  
//
//*************************************************************************
char fnPostIPSDGetParamList( ushort msgID, BOOL msgStat ) 
{
    if( msgStat == BOB_OK )
    {
        // Use the Definition record in flash to distribute the 
        //  variables in the parameter list glob.
        msgStat = fnIPSDDistribParamList();
        // Convert any 4-byte money values to 5-byte money values.
        msgStat = fnConvertPSDParamMoneys( msgID, msgStat );
        // Anything else to do with this data? Uh-huh
        msgStat = fnConvertPSDParamDateLimits(msgID, msgStat );
        // compute the derived piece count
        msgStat = fnComputeDerivedPieceCount( msgID, msgStat ); 
    }

    (void)fnSetBobState( bob_Idle );

    return( msgStat );
}

/******************************************************************************
   FUNCTION NAME: fnComputeDerivedPieceCount
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Compute the derived piece count based on PCN parameters
   PARAMETERS   : standard script post function
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnComputeDerivedPieceCount( ushort msgID, BOOL msgStat ) 
{
    if( msgStat == BOB_OK )
    {
        // the piece count shell game, watch closely
        if( fnFlashGetByteParm(BP_NO_ZPRINTS_IN_PCNT) == 0 )
        {
            //for most markets the derived pc is the same as the total pc
            memcpy( &ulDerivedPieceCount, pIPSD_pieceCount, sizeof(ulDerivedPieceCount) );
        }
        else
        {
            //master chocolatiers and watchmakers prefer derived pc = total pc - zero pc
            ulong ulTotalPc, ulZeroPc;

            memcpy( (void*)&ulTotalPc, pIPSD_pieceCount, sizeof(ulTotalPc) ); 
            memcpy( &ulZeroPc, pIPSD_ZeroPostagePieceCount, sizeof(ulZeroPc) );
            ulDerivedPieceCount = ulTotalPc - ulZeroPc;
        }      
    }
    return( msgStat );
}

/* **********************************************************************

 FUNCTION NAME:     char fnGetPrintPrep(ushort ignore) 

 PURPOSE:           During handling of the intertask message SCM_MAIL_IN_PROGRESS, 
                    this function redirects the script handler to an appropriate 
                    script based on the current print mode.
                    
 AUTHOR:            blu

 INPUTS:            Argument "ignore" is the device message ID of the message being
                    serviced.  In this function the argument is labeled as "ignore"
                    because the argument is not used.  It is only passed for
                    compatibility with the preMessage function prototype.
                    
 *************************************************************************/
UINT16 fnGetPrintPrep( UINT16 msgID ) 
{
    UINT16    retval = msgID;

    switch( bUICPrintMode )
    {

        case BOB_DEBIT_FUNDS:
        retval = SCRIPT_IPSD_DEBIT;
        break;

        case BOB_PERMIT_MODE:
        retval = SCRIPT_PERMIT_PRINT;
        break;

        case BOB_DATE_STAMP_MODE:
        case BOB_DATE_STAMP_ONLY_MODE:
        retval = SCRIPT_DATE_STAMP_PRINT;
        break;

        default:
        retval = DUMMY_SCRIPT;
        break;

    }
    return(retval);
}

//**************************************************************************
//  FUNCTION NAME:  fnGetIndiciaLine
//
//  DESCRIPTION:
//      Retrieve line 1 or 2 of the indicia text from rating app.
//  INPUTS:
//      ucLine  line 1 or 2
//  RETURNS:
//      bob's disposition
//  NOTES:          
//
//  MODIFICATION HISTORY:
//  10/24/2008  Raymond Shen      Added code to support Brazil KIP Indicia HRT selection.
//  07-10-06    Dicky Sun         Update for supporting new return type of 
//                                fnRateGetProdConfigListInfo.
//              Craig DeFilippo   Initial version
//**************************************************************************
unichar* fnGetIndiciaLine(uchar ucLine) 
{

//	char str[11];
//	memcpy(str, "Correction", 10);
//	str[11] = 0;
//	(void)unistrcpyFromCharStr(indiciaTextLine, str);

//	SET_SPACE_UNICODE(indiciaTextLine, PCL_STRING_LENGTH);    //This clears the field
	UNICHAR *pString;

	switch(ucLine){

	case 4:
		pString = fnGetIndiciaString();
		(void) unistrcpy(indiciaTextLine,  pString);
	break;

    default:
         (void) unistrset(indiciaTextLine, 0);
         break;

	}

#if 0 //TODO RAM remove rates
  // for K700 or G900 3rd Gen Dcap
    // Get the Indicia info from the Data Capture Engine

    const struct ind_output *pInfoOutput;
    SINT16  usRet;


//TODO RAM remove rates    usRet = fnRateGetIndiciaInfo(&pInfoOutput);

    if(fnRateIsKIPHrtSelected() == TRUE)
    {
        UNICHAR *pString;
        
        switch(ucLine)
        {
        case 1:
            (void)fnRateGetKIPIndiciaHrtRate( &pString );
            (void) unistrcpy(indiciaTextLine,  pString);
            break;
			
        case 4:
            (void)fnRateGetKIPIndiciaHrtFee1( &pString );
            (void) unistrcpy(indiciaTextLine,  pString);
            break;
			
        case 5:
            (void)fnRateGetKIPIndiciaHrtFee2( &pString );
            (void) unistrcpy(indiciaTextLine,  pString);
            break;
			
        default: 
            (void) unistrset(indiciaTextLine, 0); 
            break;
        }
    }
    else if ((usRet == 0)&&(ucLine > 0) && (ucLine < 6) )
    {
        switch(ucLine)
        {
        case 1:
            (void)unistrcpy( indiciaTextLine,  pInfoOutput->Indicia_HRT[0].String );
            break;
			
        case 4:
            (void)unistrcpy( indiciaTextLine,  pInfoOutput->Indicia_HRT[3].String );
            break;
			
        case 5:
            (void)unistrcpy( indiciaTextLine,  pInfoOutput->Indicia_HRT[4].String );
            break;
			
        case 2:
        default: 
            (void)unistrcpy( indiciaTextLine, pInfoOutput->Indicia_HRT[1].String ); 
            break;

        }
    }   
    else
    {
        (void)unistrset( indiciaTextLine, 0 );
    }
#endif //RAM remove rates
    return(indiciaTextLine);
}

//**************************************************************************
//  FUNCTION NAME:  fnGetSupplementalLine
//
//  DESCRIPTION:
//      Retrieve line 1 or 2 of the supplemental text from rating app.
//  INPUTS:
//      ucLine  line 1 or 2
//  RETURNS:
//      bob's disposition
//  NOTES:          
//      pointer to result in a unistring buffer
//  MODIFICATION HISTORY:
//  07-15-09    Bonnie Xu         Update for AUSTRIA
//  07-10-06    Dicky Sun         Update for supporting new return type of 
//                                fnRateGetProdConfigListInfo.
//              Craig DeFilippo   Initial version
//**************************************************************************
unichar* fnGetSupplementalLine(uchar ucLine) 
{
    (void)unistrset( supplementalTextLine, 0 );

    return(supplementalTextLine);
}


//**************************************************************************
//  FUNCTION NAME:  fnGetDCMIndiciaHRT
//
//  DESCRIPTION:
//      Retrieve line 3 or 4 of the HRT from DCM
//  INPUTS:
//      ucLine  line 3 or 4
//  RETURNS:
//      bob's disposition
//  NOTES:          
//      pointer to result in a unistring buffer
//  MODIFICATION HISTORY:
//   03/04/08      Andy Mo   Initial version
//**************************************************************************
/*        This function is not used on FPHX, for Brazil or anyone else. 
UINT16* fnGetDCMIndiciaHRT(UINT8 ucLine) 
{
    // Get the Tracking info from the Data Capture Engine
    const struct ind_output *pDCInfo;
    SINT16 sRet;
    UINT16 usLen;
    
    memset(supplementalTextLine,0,sizeof(supplementalTextLine));
    
    sRet = fnRateGetIndiciaInfo(&pDCInfo);
        
    if( sRet ==  RSSTS_NO_ERROR && pDCInfo )
    {
        if( pDCInfo->NumIndiciaStrings > 0 )
        {
            switch(ucLine)
            {
            case INDICIA_DCM_STRING4:            
                usLen = unistrlen( pDCInfo->Indicia_HRT[3].String );
                if(usLen>0)
                {
                    // return the string
                    (void)unistrcpy( supplementalTextLine, pDCInfo->Indicia_HRT[3].String );
                }
                break;
            case INDICIA_DCM_STRING5:
                usLen = unistrlen( pDCInfo->Indicia_HRT[4].String );                
                if(usLen>0)
                {
                    // return the string
                    (void)unistrcpy( supplementalTextLine, pDCInfo->Indicia_HRT[4].String );
                }
                break;
            default:
                break;
            }
        }
    }
    return(supplementalTextLine);
}
*/

/******************************************************************************
   FUNCTION NAME: fnGetVasBarcodes
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Ask GTS to generate all the possible tracking barcodes for 
             : this mailpiece, and save them to BOB's local buffers
   PARAMETERS   : standard preMessage
   RETURN       : bob's disposition
   NOTES         :
   1.  Intended for use in the high level debit and predebit scripts.
       This will ensure that BOB has retreived all the barcodes prior to formatting
       any predebit messages or rendering any data for the indicia.
       Callers should use fnGetLastUsedVasBarcodeAsciiText to get BOB's buffered 
       copy.
   2. On FPHX, this is only used in the debit, not the pre-debit script.
******************************************************************************/
char fnGetVasBarcodes( ushort msgID )
{
    char    retval = BOB_OK;

    return(retval);
}

//**************************************************************************
//  FUNCTION NAME:  fnGetVasBarcodeAsciiText
//
//  DESCRIPTION:
//      Ask GTS to generate the 1D barcode for this mailpiece, save and
//      pass back a pointer to it.
//  INPUTS:
//      barcode text to return, 1 (1D) or 2 (2D)
//  RETURNS:
//      pointer to unistring buffer, NULL_PTR if no PCL info or GTS
//  NOTES:          
//      None
//  MODIFICATION HISTORY:
//  07-10-06    Dicky Sun         Update for supporting new return type of 
//                                fnRateGetProdConfigListInfo.
//              Craig DeFilippo   Initial version
//**************************************************************************
//// for debug only ////////////////////////////////////////////////
//static uchar BC1DdummyDom[]={"WW000000000DE000"};   // changed this to "WW" to differentiate with the correct RR string
//static uchar BC1DdummyInt[]={"WW000000000DE"};  // changed this to "WW" to differentiate with the correct RR string
/*
uchar* fnGetVasBarcodeAsciiText(uchar ucBarcode) 
{
//// for debug only ////////////////////////////////////////////////
//  if(pcl_output->pPCLInfo->RecordType == PCL_OUT_REC_TYPE_DOM_VAS)
//      return(BC1DdummyDom);
//  else
//      return(BC1DdummyInt);   // for testing
////////////////////////////////////////////////////////////////////
}
*/

//**************************************************************************
//  FUNCTION NAME:  fnCheckGTSRecordTypeDomIntl
//
//  DESCRIPTION:
//      Pulled out of fnGetLastUsedVasBarcodeAsciiText. Checks the record 
//      type, not sure why.   
//  INPUTS:
//      None
//  RETURNS:
//      TRUE if the record type can be retrieved and is either 
//          international or domestic.
//      FALSE otherwise
//  NOTES:
//   1. Not sure if we really need this anymore, which is why I pulled it
//      out of the other function.          
//  MODIFICATION HISTORY:
//   2009.01.15  Clarisa Bellamy   Initial version
//**************************************************************************
BOOL fnCheckGTSRecordTypeDomIntl( void )
{
    BOOL fRetVal = FALSE;
#if 0 //TODO VAS
    // Get the Indicia info from the Data Capture Engine

    const struct ind_output *pDCInfo;
    SINT16  sRet;

//TODO RAM remove rates    sRet = fnRateGetIndiciaInfo( &pDCInfo );


    if(   (sRet == 0)
       && (pDCInfo != NULL_PTR) 
       && (   (pDCInfo->IndiciaRecordType == PCL_OUT_REC_TYPE_DOM_VAS) 
           || (pDCInfo->IndiciaRecordType == PCL_OUT_REC_TYPE_INTL_VAS) ) )
    {
        fRetVal = TRUE;  
    }

#endif

    return( fRetVal );
}



//**************************************************************************
//  FUNCTION NAME:  fnGetbobPCLInfo
//
//  DESCRIPTION:
//      
//  INPUTS:
//      
//  RETURNS:
//      
//  NOTES:          
//      
//  MODIFICATION HISTORY:
//  2009.09.21  Deborah Kohl    Remove call to fnGetVasBarcodes() that are no
//                              longer needed as fnGetVasBarcodes() is called
//                              now in fnIPSDLoadCreateIndiciaData()
//  2008.06.03  Clarisa Bellamy   Make sure pDCInfo is initialized, then make
//                              sure it is not a NULL_PTR before reading its
//                              data. (LINT)
//  07-14-06    Dicky Sun         Check pcl_output pointer before using.
//  07-10-06    Dicky Sun         Update for supporting new return type of 
//                                fnRateGetProdConfigListInfo.
//              blu               Initial version
//**************************************************************************
char fnGetbobPCLInfo(ushort bobVarID, uchar *dest)
{
    char    longChar[sizeof(long)];
    ulong   longVar;
	
    // start by filling any general retrieval stuctures that are needed
    switch (bobVarID)
    {
        // The following variables require information from Rates
        case BOBID_IPSD_GERMAN_VARPOSTALDATA:               // german variable postal data elements
        case BOBID_IPSD_GERMAN_PRODUCTKEY:                  // german product key
        case BOBID_IPSD_GERMAN_SERVICEID:                   // german service id
            break;

        default:
            break;
    }
	
    switch (bobVarID)
    {
        // The discount piece count it already set
        case BOBID_IPSD_GERMAN_PIECECOUNT:
            longVar = CMOSPieceID;
            longVar++;
            memcpy((char *)(&longChar[0]), (char *)(&longVar), sizeof(long));
            memcpy((char *)(&pIPSD_GermanPieceCount)[0], (char *)(&longChar[1]), IPSD_GERMAN_PIECECOUNT_SZ);
            break;

        // German Product Key
        case BOBID_IPSD_GERMAN_PRODUCTKEY:
            memset(dest,0,IPSD_GERMAN_PRODUCTKEY_SZ);
            break;

        // German Second Date. For now the german second date is zeroed
        case BOBID_IPSD_GERMAN_SECONDDATE:
            memset(dest, 0, IPSD_GERMAN_SECONDDATE_SZ);
            break;

        // German Service ID.  The german service ID comes from PCL info    
        case BOBID_IPSD_GERMAN_SERVICEID:
            memset(dest,0,IPSD_GERMAN_SERVICEID_SZ);
            break;

        // German Variable Postal Date must be build from several sources
        case BOBID_IPSD_GERMAN_VARPOSTALDATA:
            memset(dest, 0, IPSD_GERMAN_VARPOSTALDATA_SZ);
            break;

        default:
            break;
    } // end of switch (bobVarID)
    return(BOB_OK);
}

/******************************************************************************
   FUNCTION NAME: fnRefreshDebitInputInfo
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Refresh miscellaneous pieces of data that are used as inputs
             : to various debit algorithms.
   PARAMETERS   : Bobs variable id, pointer to destination bufferNone
   RETURN       : Bobs disposition
   NOTES         : none
   MODIFICATION HISTORY:
       2009.06.10 Clarisa Bellamy - For Canada support:  When checking certain 
                            Indicia types, check if the IPSDFirmwareVersion 
                            indicates that the Indicia Type has a different 
                            meaning.
      08/27/2008  Joe Qu  Merged changes from K700 for Netherlands NetSet2
                          Add support for cases BOBID_IPSD_NETSET_PRODUCTCODE,
                          BOBID_IPSD_NETSET_CLR for G900
      10/22/2007  Craig DeFilippo  Initial version
******************************************************************************/
char fnRefreshDebitInputInfo(ushort bobVarID, uchar *dest)
{
    uchar   strTempLong[sizeof(long)];
    uchar   *pTemp = 0;
    ulong   ulTempLong = 0;

#if defined (K700) || defined (G900)
    //const struct ind_output    *pDCInfo;
    //SINT16 sRet;
//TODO RAM remove rates    sRet = fnRateGetIndiciaInfo(&pDCInfo);
#endif

    switch (bobVarID)
    {
        // The discount piece count is the batch count
        case BOBID_IPSD_GERMAN_PIECECOUNT:
            //TODO remove batch count from base ulTempLong = GetBatchCount();
            ulTempLong++;               // getting the value before the debit, so need to increment.
            memcpy( &strTempLong[0], &ulTempLong, sizeof(long) );
            memcpy( dest, &strTempLong[1], IPSD_GERMAN_PIECECOUNT_SZ );
            break;

        // German Second Date. For now the german second date is zeroed
        case BOBID_IPSD_GERMAN_SECONDDATE:
            memset(dest, 0, IPSD_GERMAN_SECONDDATE_SZ);
            break;

        // The message type for precreate (and create) indicia with message type
        case BOBID_IPSD_CREATEINDICIA_MSGTYPE:
            // base it on the indicia type
            if(   (bIPSD_indiciaType == IPSD_IND_TY_NETHERLANDS) 
               && (fIPSD_FWVerModsIndiciaType == FALSE) )
            {
                memset( dest, IPSD_CREATE_IND_NETHERLANDS, IPSD_CREATEINDICIA_MSGTYPE_SZ );
            }
            else if( bIPSD_indiciaType == IPSD_IND_TY_SPAIN_GENERIC )
            {
                memset( dest, IPSD_CREATE_IND_GENERIC, IPSD_CREATEINDICIA_MSGTYPE_SZ );
            }
            else if( bIPSD_indiciaType == IPSD_IND_TY_FLEX_DEBIT )
            {
                memset( dest, IPSD_CREATE_IND_FLEX_DEBIT, IPSD_CREATEINDICIA_MSGTYPE_SZ );
            }

            break;

        // NetSet2 Configuration Layout Reference, from DCAP rules string 1
        case BOBID_IPSD_NETSET_CLR:
#if defined (K700) || defined (G900)
#if 0 //TODO RAM remove rates
             if (sRet == RSSTS_NO_ERROR)
             {
                memcpy( dest, pDCInfo->BarcodeData->ServiceCodeAlpha, IPSD_NETSET_CLR_SZ );
             }
             else
             {
                memset( dest, 0x30, IPSD_NETSET_CLR_SZ );
             }
#endif //TODO RAM remove rates
#else
#if 0 //TODO JAH Remove dcapi.c 
			pTemp = (uchar*) fnDCAPGetProductDescriptionLine(0);
            if( pTemp != NULL_PTR )
            {
                memcpy( dest, pTemp, IPSD_NETSET_CLR_SZ );
            }
            else
            {
                memset( dest, 0x30, IPSD_NETSET_CLR_SZ );
            }
#endif //TODO JAH Remove dcapi.c 
#endif
            break;

        // NetSet2 CUPU S31 Issuer code, from EMD postal authority code
        case BOBID_IPSD_NETSET_ISSUERCODE:
            pTemp = fnFlashGetIBPackedByteParm(IPB_POSTAL_AUTH_CODE);
            if( pTemp != NULL_PTR )
            {
                memcpy(dest, (uchar*)pTemp, IPSD_NETSET_ISSUERCODE_SZ);
            }
            else
            {
                memset(dest, 0x30, IPSD_NETSET_ISSUERCODE_SZ);
            }
            break;

        // NetSet2 Maildate, offset from production date
        case BOBID_IPSD_ADVANCEDDATEOFFSETASCII:
            ulTempLong = GetPrintedDateAdvanceDays();
            sprintf((char*)strTempLong,"%02ld",ulTempLong);
            memcpy(dest,(uchar*)strTempLong, IPSD_ADVANCEDDATEOFFSETASCII_SZ);
            break;

        // NetSet2 Security Code Version, set to '01'
        case BOBID_IPSD_NETSET_SECURITYCODEVER:
            //strcpy((char*) dest, "01");
            pTemp = fnFlashGetAsciiStringParm(ASP_SECURITY_CODE_VERSION);
            if(pTemp && (strlen((const char *) pTemp) > 0))
            {
                memcpy(dest,pTemp,IPSD_NETSET_SECURITYCODEVER_SZ);
            }
            else
            {
                memset(dest,0x30,IPSD_NETSET_SECURITYCODEVER_SZ);
            }
            break;

        // NetSet2 product code, format is PPnnnn, just use the nnnn portion
        // It comes from the bucket name
        case BOBID_IPSD_NETSET_PRODUCTCODE:
#if defined (K700) || defined (G900)
#if 0 //TODO RAM remove rates
            if( sRet == RSSTS_NO_ERROR )
            {
                memcpy( dest, pDCInfo->BarcodeData->ProductIDAlpha , IPSD_NETSET_PRODUCTCODE_SZ );
            }
            else
            {
                memset( dest, 0x30, IPSD_NETSET_PRODUCTCODE_SZ );
            }
#endif //TODO RAM remove rates
#else
#if 0 //TODO JAH Remove dcapi.c 
			pTemp = (uchar*)fnDCAPGetCurrentProductCode();
            if( pTemp )
            {
                memcpy( dest, pTemp+2, IPSD_NETSET_PRODUCTCODE_SZ );
            }
            else
            {
                memset( dest, 0x30, IPSD_NETSET_PRODUCTCODE_SZ );
            }
#endif //TODO JAH Remove dcapi.c 
#endif
            break;

        case BOBID_IPSD_FLEXDEBIT_VALKEY_DATA:
            pTemp = fnFlashGetIBPackedByteParm(IPB_FLEXDEBIT_VALUE_KEY_DATA);
            if( pTemp != NULL_PTR )
            {
                memcpy(dest, (uchar*)pTemp, IPSD_SZ_FLX_VALKEY);
            }
            else
            {
                memset(dest, 0x00, IPSD_SZ_FLX_VALKEY);
            }
            break;
             
        default:
            break;
    } // end of switch (bobVarID)
    return(BOB_OK);
}

/******************************************************************************
   FUNCTION NAME: fnBuildIndiciaFlexDebitData
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Build the flex debit data according to the
             : IPB_CREATE_FLEXDEBIT_DEF_REC record.
   PARAMETERS   : Bobs variable id, pointer to destination (for compatibitlity)
   RETURN       : Bobs disposition
   NOTES         : the structure of IPB_CREATE_FLEXDEBIT_DEF_REC is the same as
             :   IPB_BUILD_BC_DEF_REC
******************************************************************************/
char fnBuildIndiciaFlexDebitData( ushort bobVarID, uchar *ignore )
{
    char    retval;

    retval = fnBuildFlexDebitData( bobVarID, IPB_CREATE_FLEXDEBIT_DEF_REC );
    return( retval );  
}

/******************************************************************************
   FUNCTION NAME: fnBuildSOMFlexDebitData
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Build the flex debit data according to the
             : IPB_CREATE_FLEXDEBIT_DEF_REC_2 record.
   PARAMETERS   : Bobs variable id, pointer to destination (for compatibitlity)
   RETURN       : Bobs disposition
   NOTES         : the structure of IPB_CREATE_FLEXDEBIT_DEF_REC_2 is the same as
             :   IPB_BUILD_BC_DEF_REC
******************************************************************************/
/* This is not needed yet for FPHX... It is for India.
char fnBuildSOMFlexDebitData( ushort bobVarID, uchar *dest )
{
    return( fnBuildFlexDebitData( bobVarID, IPB_CREATE_FLEXDEBIT_DEF_REC_2 ) );
}
 */

/******************************************************************************
   FUNCTION NAME: fnBuildFlexDebitData
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Build the flex debit data according to the
             : passed FLEXDEBIT_DEF_REC record ID.
   PARAMETERS   : Bobs variable id, pointer to destination (both ignored,
                 :  included for possible future use)   
   RETURN       : Bobs disposition
   NOTES         : the structure of IPB_CREATE_FLEXDEBIT_DEF_REC is the same as
             :   IPB_BUILD_BC_DEF_REC
******************************************************************************/
char fnBuildFlexDebitData( ushort bobVarID, UINT16 usRecID )
{
    IPSD_FLEXDEBIT_DEF_REC rFieldRec;
    UINT16  usNumFields;
    UINT16  i, j;
    UINT16  usTempLen;
    UINT16  usSize;
    UINT8   *pGlob;
    UINT8   *pTempGlob;
	char pLogBuf[50];


    usNumFields = fnFlashGetIBPNumFields( usRecID );
    memset( pIPSD_flexDebitDataGlob, 0, sizeof(pIPSD_flexDebitDataGlob) );
    pGlob = pIPSD_flexDebitDataGlob;
    wIPSD_flexDebitDataSize = 0;

    // clear the temp glob which will capture one variable each iteration
    memset( pIPSD_flexDebitTempData, 0, sizeof(pIPSD_flexDebitTempData) );
    pTempGlob = pIPSD_flexDebitTempData;

    // scan through the IPB_CREATE_FLEXDEBIT_DEF_REC and fetch each variable
    for( i=0; i<usNumFields; i++ )
    {
        if( fnFlashGetIBPBcDefRecVarOrdered(&rFieldRec, usRecID, i) )
        {
            if( rFieldRec.usFieldLen != IBP_BC_DEF_REC_LEN_VAR )
            {
                // set entire field to pad by default
                memset( pGlob, rFieldRec.ucFieldPad, rFieldRec.usFieldLen );
            }

            usTempLen = fnCallReportTableFunction( rFieldRec.usFieldFn, (void*) pTempGlob, rFieldRec.usFieldID );
            if( usTempLen )
            {
                // write in the data left or right justified
                // in either case both source and destination are parsed from left or right
                //  according to the justification.  We'll see how long this convention lasts.
                if(   (rFieldRec.ucFieldJust == IBP_DCR_FLD_JUST_RIGHT) 
                   && (rFieldRec.usFieldLen != IBP_BC_DEF_REC_LEN_VAR) )
                {
                    if( rFieldRec.usFieldLen <= usTempLen )
                        usSize = rFieldRec.usFieldLen;
                    else
                        usSize = usTempLen;

                    for( j = 0; j < usSize; j++ )
                    {
                        *(pGlob + ( (rFieldRec.usFieldLen -1) -j )) = *(pTempGlob + ( (usTempLen -1) -j ));
                    }
                }
                // Left justification, no Center justification supported
                else
                {
					// if the field length isn't variable -AND-
					// what we want to copy is bigger than the field length,
					// use the field length for the copy
					//
					// else, use the length of what we want to copy for the copy
                    if ((rFieldRec.usFieldLen != IBP_BC_DEF_REC_LEN_VAR) &&
						(usTempLen > rFieldRec.usFieldLen))
					{
                        memcpy(pGlob, pTempGlob, rFieldRec.usFieldLen);
					}
                    else
					{
                        memcpy(pGlob, pTempGlob, usTempLen);
					}
                }
            }

            pGlob += rFieldRec.usFieldLen;
            wIPSD_flexDebitDataSize += rFieldRec.usFieldLen;
        }
    }

	(void)sprintf(pLogBuf, "flex debit size = %u", wIPSD_flexDebitDataSize);
	fnDumpStringToSystemLog(pLogBuf);
	pIPSD_flexDebitDataGlob[wIPSD_flexDebitDataSize] = 0;
	fnDumpStringToSystemLog((char *)pIPSD_flexDebitDataGlob);
	
	if (fnGetLocalLoggingState())
		fnDumpRawLenToSystemLog((char *)pIPSD_flexDebitDataGlob, wIPSD_flexDebitDataSize);

    return(BOB_OK);
}

/* **********************************************************************
 FUNCTION NAME:     fnConvertPDEreplyMailData

 PURPOSE:           populate the postal data element array DP_IBIdataElements as needed for German IBI
                    2d barcodes in reply style
                    
 AUTHOR:            blu... converted from a similar function written for Comet, by billy

 INPUTS:            pPDElementArray should point to DP_IBIdataElements[1[ or DP_IBIdataElements[9[ on entry 
                    depending on the value of f60 in barcode data
 *************************************************************************/
 void  fnConvertPDEreplyMailData(uchar * pPDElementArray) 
 {
//    char    tempString[40];
//    uchar   tmpPLZ[40] = {0,0};
//    long    PLZval;
//    UINT16  i;


    memset(pPDElementArray, 0, 8);
#if 0 //TODO ReplyMail Data
    if (fnIsReplyMailInUse(tempString))
    {
        for( i = 0; i < sizeof( tmpPLZ ); i++) {                    // Eliminate town name info if used
            if (isalpha(tempString[i])) break;
            tmpPLZ[i] = tempString[i];
        }
        PLZval = atol((char *) tmpPLZ );                            //convert ASCII PLZ to number
        memcpy((char *)tmpPLZ, (char *) &PLZval, sizeof(long));                     
        memcpy(pPDElementArray,(char *) &tmpPLZ[1], 3);             //pull out least signif 3 bytes
    }
#endif
}



//const struct varFuncRef createVarRef[] = {
const T_VAR_FUNC_REF createVarRef[] = {
//  Create Indicia Var ID       Matching bob ID                 Refresh Function
{   GLOBID_GERMANSERVICEID,     BOBID_IPSD_GERMAN_SERVICEID,    fnGetbobPCLInfo },
{   GLOBID_GERMANPRODUCTKEY,    BOBID_IPSD_GERMAN_PRODUCTKEY,   fnGetbobPCLInfo },
{   GLOBID_GERMANSECONDDATE,    BOBID_IPSD_GERMAN_SECONDDATE,   fnGetbobPCLInfo },
{   GLOBID_GERMANPIECECOUNT,    BOBID_IPSD_GERMAN_PIECECOUNT,   fnGetbobPCLInfo },
{   GLOBID_GERMANVARPOSTALDATA, BOBID_IPSD_GERMAN_VARPOSTALDATA,fnGetbobPCLInfo },
{   GLOBID_CREATEINDICIA_MSGTYPE,   BOBID_IPSD_CREATEINDICIA_MSGTYPE,   fnRefreshDebitInputInfo },      //6
{   GLOBID_NETSET_CLR,              BOBID_IPSD_NETSET_CLR,              fnRefreshDebitInputInfo },      //7
{   GLOBID_NETSET_ISSUERCODE,       BOBID_IPSD_NETSET_ISSUERCODE,       fnRefreshDebitInputInfo },      //8
{   GLOBID_ADVANCEDDATEOFFSET,      BOBID_IPSD_ADVANCEDDATEOFFSETASCII, fnRefreshDebitInputInfo },      //9
{   GLOBID_NETSET_PRODUCTCODE,      BOBID_IPSD_NETSET_PRODUCTCODE,      fnRefreshDebitInputInfo },      //10
{   GLOBID_NETSET_SECURITYCODEVER,  BOBID_IPSD_NETSET_SECURITYCODEVER,  fnRefreshDebitInputInfo },      //11
{   GLOBID_FLEXDEBIT_VALUEKEY_DATA, BOBID_IPSD_FLEXDEBIT_VALKEY_DATA,   fnRefreshDebitInputInfo },      //12
{   GLOBID_FLEXDEBIT_DATA,          BOBID_IPSD_FLEXDEBIT_DATAGLOB,      fnBuildIndiciaFlexDebitData },  //13
{   GLOBID_NETSET_PSTVERSION,       BOBID_IPSD_GERMAN_SERVICEID,        fnGetbobPCLInfo },              //14
};

// **********************************************************************
//
// FUNCTION NAME:   char fnRefreshCreateIndiInfo()
//
//     PURPOSE:     Based on the Create Indicia Definition Record, refreshes 
//                  data that will be used in the preCreate and Create messages 
//                  to the iButton.
//                   
//      AUTHOR:     
//
//      INPUTS:     none
//
//      NOTES:      This function can be used for both create Indicia and
//                  PRE-create Indicia messages, since they have the same
//                  appended data format.
//
//                  Table createVarRef specifies the refresh function that
//                  is required for each variable in the Create Indicia 
//                  Definition Record.
// *************************************************************************
char fnRefreshCreateIndiInfo()
{
    uchar       *pDefRekord;        // Traveling pointer through the Create Indicia Definition entries
    ushort      usDefCount;         // number of entries in record
    ushort      usRecCnt;           // running count of definitions processed
    ushort      varID;              // a variable ID extracted from the Create Indicia Definition Record
    char        retval;             // the returned status of this operation
    ushort      bobVarID;           // a bob variable ID cross-referenced from an Create Indicia variable ID
    uchar       *destAddress;       // the address of the bob variable 
    ushort      hunting;

    retval = BOB_OK;
//    DP_IndiciaLineTextReadSem = FALSE;
//    DP_VasTextReadSem = FALSE;
//    DP_EkpOrTskNoReadSem = FALSE;
    DP_IndiciaOtherDpInfoReadSem = FALSE;

    // Get a pointer to the Create Indicia Definition Record in FLASH
    pDefRekord = fnFlashGetIBPackedByteParm( IPB_CREATE_INDICIA_DEF_REC );


    // Get the number of entries in the record.     
    EndianAwareCopy( (char *)(&usDefCount), (char *)(pDefRekord), sizeof(short) );
    pDefRekord += sizeof(short);

    // If there are entries in the record...
    if( usDefCount ) 
    {
        // from each entry in the iPacked Byte Parameter...
        for( usRecCnt = 0; usRecCnt < usDefCount; usRecCnt++ ) 
        {
            // retrieve the variable ID
            EndianAwareCopy( (char *)(&varID), pDefRekord, sizeof(short) );

            // Table createIndexRef contains the IDs of variables 
            // that must be refreshed.  See if the variable ID is in the table
            for( hunting = 0; hunting < ARRAY_LENGTH(createVarRef); hunting++ )
            {
                if (varID == createVarRef[hunting].varID)
                {
                    // if the Create Indicia variable is listed in the table,
                    // call the associated table function, passing the address of
                    // the bob variable that is assigned to that Create Indicia variable
                    bobVarID = createVarRef[hunting].bobID;
                    destAddress = bobaVarAddresses[bobVarID].addr;
                    retval = (char)(createVarRef[hunting].func(bobVarID, destAddress));
                    break;
                }
            }
            // Goto next record.
            if (retval == BOB_OK)
            {
                pDefRekord +=  SIZE_IPSDDEFREC;
            }
        }
    }
    if (retval != BOB_OK)
    {
        fnSetBobInternalErr(BOB_PREDEB_PREP_ERR);
        retval = BOB_SICK;
    }
    return(retval);
}


// **********************************************************************
//
// FUNCTION NAME:   char fnIPSDLoadCreateIndiciaData( ushort msgID ) 
//
//     PURPOSE:     Based on the Create Indicia Definition Record, gather
//                  the data that will be appended to the preCreate or
//                  Create Indicia message.
// INPUTS:     none
//
// NOTES:      
//  1. This function can be used for both create Indicia and PRE-create Indicia 
//     messages, since they have the same appended data format.
// HISTORY:
//   2010.07.13 Clarisa Bellamy - Add a line to save the postage value that is sent 
//                      to the ipsd in the new static variable ulDebitedPostageValue, 
//                      because bobsPostageValue may be changed before the debit 
//                      script is called. 
//   2009.09.21 Deborah Kohl Add a call to fnGetVasBarcodes() to refresh
//              tracking ID in 1D and 2D barcode buffer
//   2008.09.22 Clarisa Belllamy - Set P2 according to the value of fIPSD_IbiLiteOn.
// *************************************************************************
char fnIPSDLoadCreateIndiciaData( ushort msgID ) 
{
    char    retval = BOB_SICK;
    ushort  len;
    uchar   *pMsgData;

    // NOTE: In the following call, the constant IPSD_PRECREATE_INDICIUM is used.
    // This is okay, as long as the various indicia don't diverge.
    // if in the future an indicia needs to use the system date rather than the printed date,
    // then the call should be corrected to use passed variable msgID instead.

    // Update date/time data with current date/time.
    (void)fnMsgDateTime( IPSD_PRECREATE_INDICIUM );

    // Set Rate/Class data
    fnIPSDRateClassForPreDeb();

    // Update buffer containing tracking ID for 1D and 2D barcode
    fnGetVasBarcodes(0);

    // Refresh bob variables that will be used to load the data
    retval = fnRefreshCreateIndiInfo();
    
    // Funds data is updated prior to getting here.

    if( retval == BOB_OK )
    {
        // Save the value that was actually sent in the precreate message, just in case 
        // some process changes bob's postage value before the debit is complete.
        memcpy( &ulDebitedPostageValue, bobsPostageValue, sizeof(ulDebitedPostageValue) ); //TODO JAH FIght Endians?
    
        // Use the Definition Record to load up the create indicia
        // glob before it is used in the message.
        retval = fnIPSDLoadCreateIndiciaGlob();
        if( retval == BOB_OK )
        {
            // The easiest way to send data of indeterminant size is to packetize it.
            // So setup the packetInfo
            len = wIPSD_createIndDataGlobSize;
            pMsgData = pIPSD_createIndDataGlob;
            rIPSD_packetInfo.wLeftToSend   = rIPSD_packetInfo.wTotalSize    = len;
            rIPSD_packetInfo.pCurrPcktData = rIPSD_packetInfo.pMsgData      = pMsgData;

            if (fnGetDefaultRatingCountryCode() == CANADA)
            {
                mach_ctrl_indiciatypes_t indiciaType = getIndiciaType();

                switch(indiciaType)
                {
                    case MACH_CTRL_INDICIATYPES_RETURN_POSTAGE_PREPAID:
                        rIPSD_packetInfo.rHdr.bP2 = IPSD_P2_CANADA_RETURN_POSTAGE_PREPAID_INDICIUM;
                        break;

                    case MACH_CTRL_INDICIATYPES_CA_DATE_CORRECTION:
                        rIPSD_packetInfo.rHdr.bP2 = IPSD_P2_CANADA_REDATE_INDICIUM;
                        break;

                    default:
                        rIPSD_packetInfo.rHdr.bP2 = IPSD_P2_NORMAL_INDICIA;
                        break;
                }
            }
            else
            {
                rIPSD_packetInfo.rHdr.bP2 = IPSD_P2_NORMAL_INDICIA;
            }
        }   
    }
    return( retval );
}

// **********************************************************************
//
// FUNCTION NAME:   char fnIsDebitMode( ushort msgID ) 
//
// PURPOSE:         If doing a normal mail piece, calls a function to load 
//                  up the appended data for the create-indicia message, 
//                  and returns its value. (If it returns an error
//                  call fnSetBobInternalErr().)
//                  Otherwise, return BOB_SKIP_ALL to abort the rest of the 
//                  script.
//                    
// AUTHOR:          blu
//
// INPUTS:          Argument "ignore" is the device message ID of the message being
//                  serviced.  In this function the argument is labeled as "ignore"
//                  because the argument is not used.  It is only passed for
//                  compatibility with the preMessage function prototype.
//                  
// ************************************************************************
char fnIsDebitMode( ushort msgID ) 
{
    char    retval = BOB_OK;
    uchar   ucBobError = 0;
    
    dirtyDebit = FALSE;     // reset to FALSE by any mode update
    fBobDebitOK = FALSE;

    switch( bUICPrintMode )
    {
        case BOB_DEBIT_FUNDS:
            break;

        case BOB_REPORT_MODE:
        case BOB_PERMIT_MODE:
        case BOB_TEST_PRINT_MODE:
        case BOB_AD_ONLY_MODE:
        case BOB_DATE_STAMP_MODE:
        case BOB_DATE_STAMP_ONLY_MODE:
        //fall thru
        case BOB_SEAL_MODE:
        case BOB_IDLE_PRINT_MODE:
            retval = BOB_SKIP_ALL;
            break;
        
        default:
            ucBobError = BOB_PRINT_MODE_ERROR;
            break;
    }

    if (ucBobError > 0)
    {
        fnSetBobInternalErr(ucBobError);
        retval = BOB_SICK;
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnCheckPCEndOfLife
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Check if the PC for End of Life lockout
   PARAMETERS   : std prefunction
   RETURN       : Bobs disposition
   NOTES        : none
******************************************************************************/
char fnCheckPCEndOfLife( ushort ignore ) 
{
    char retval = BOB_OK;
    uchar ucEolStat;

    ucEolStat = fnCheckPieceCountEndOfLife();
    if(ucEolStat == PSD_EOL_LOCKOUT)
    {
        fnSetBobInternalErr(BOB_PC_EOL_ERROR);
        retval = BOB_SICK;
    }
    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnValidateShadowLogs
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Validate the shadow logs against BOB's current copy of the
                : PSD register values
   PARAMETERS   : std prefunction
   RETURN       : Bobs disposition
   NOTES        : Turns out the shadow might not know
******************************************************************************/
char fnValidateShadowLogs( ushort ignore ) 
{
    char retval = BOB_OK;
    UINT32  ulPC;
    UINT32  ulZPC;
    BOOL    stat;

    memcpy( &ulPC, pIPSD_pieceCount, sizeof(ulPC) );
    memcpy( &ulZPC, pIPSD_ZeroPostagePieceCount, sizeof(ulZPC) );
    stat = fnCMOSValidateAgainstShadowLogs( ulPC, ulZPC, 
                                            (UINT8 *)&m5IPSD_ascReg, 
                                            (UINT8 *)&mStd_DescendingReg,
                                            (char *)pIPSD_PBISerialNum );
    if( stat == FALSE )
    {
        fnSetBobInternalErr( BOB_SHADOW_CORRECTION_FAILURE );
        retval = BOB_SICK;
    }
    return( retval );
}

/******************************************************************************
   FUNCTION NAME: fnInitShadowLogs
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Perform any initialization required for shadow logs
   PARAMETERS   : std prefunction
   RETURN       : Bobs disposition
   NOTES        : Should be called during PSD initialization
******************************************************************************/
char fnInitShadowLogs( ushort ignore ) 
{
    UINT8   bRefillType;
    UINT8   bRefundType;

// Comment out test, because FPHX does not allow Keypad refills.
//    if( fnFlashGetByteParm(BP_PBP_REFILL_MODE) == PBP_MODE_MODEM_ONLY )
    {
        bRefillType = bIPSD_indiciaType;
        bRefundType = bIPSD_indiciaType;
    }
//    else
//    {
//        bRefillType = KEYPAD_SHADOW_RECORD_TYPE;
//        bRefundType = KEYPAD_SHADOW_RECORD_TYPE;
//    }

    fnCMOSSetShadowRecordTypes( bIPSD_indiciaType, bRefillType, bRefundType );
    return( BOB_OK );
}


// ******************************************************************************
// This table coorelates component types to bob's current selection for that type
// ******************************************************************************
// bob and or the OI can change the values of these currentXXXSelection var's

const struct imageSelectRef bobsSelectRef[] = 
{   //  componentID,            selectionAddress,           selectionAddressType 
    //  Image ID (fwrappr.h)    dest loc of ID,             data stored in selection address  
    { IMG_ID_NORMAL_INDICIA,    &currentIndiToPrint,        SEL_ADR_IS_COMP_TYPE},
    { IMG_ID_BAR_GRAPHIC,       &currentBarCodeSelection,   SEL_ADR_IS_COMP_TYPE},
    { IMG_BAR_LEADING_IMAGE,    &currentPreBarSelection,    SEL_ADR_IS_COMP_TYPE},  
    { IMG_BAR_LAGGING_IMAGE,    &currentLagSelection,       SEL_ADR_IS_COMP_TYPE},
    { IMG_ID_TOWN_CIRCLE,       &currentTCSelection,        SEL_ADR_IS_NOT_USED},
    { IMG_ID_AD_SLOGAN,         &currentAdSelection,        SEL_ADR_IS_GRF_ID}, 
    { IMG_ID_INSCRIPTION,       &currentInscrSelection,     SEL_ADR_IS_GRF_ID},
    { IMG_ID_PERMIT1_PE,        &currentPermitSelection,    SEL_ADR_IS_GRF_ID},
    { IMG_ID_GDPC_GRAPHIC,      &currentGDPCSelection,      SEL_ADR_IS_NOT_USED},
    { IMG_ID_TOWN_CIRCLE_2,     &currentTCSelection,        SEL_ADR_IS_NOT_USED},
    { IMG_ID_TEXT_ENTRY,        &currentTextEntrySelection, SEL_ADR_IS_GRF_ID},
    { IMG_ID_DATE_TIME,         &currentDateTimeSelection,  SEL_ADR_IS_NOT_USED},
    //{ IMG_ID_ENTGELT,         &currentEBSelection,        SEL_ADR_IS_COMP_TYPE},
    { TURN_OFF_IMAGE_BYTE,      &currentAdSelection,        SEL_ADR_IS_GRF_ID},     //end of table marker
};

/******************************************************************************
   FUNCTION NAME: fnLoadMailSpecs
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Use Bobs Print Maps and the current print mode to populate
                : the selected component list
   PARAMETERS   : std prefunction
   RETURN       : Bobs disposition
   NOTES        : Janus version of this function is drastically different from
                : Mega's.
 HISTORY: 
  2008.05.28  Clarisa Bellamy - If a component is a permit type, get the actual
                 permit component type from currentPermitTypeSelection
******************************************************************************/
char fnLoadMailSpecs( ushort ignore ) 
{
    char  retval;
    uchar ucPrintMapType, ucCompType = TURN_OFF_IMAGE_BYTE;
    ushort i,j,k, usCurrentSelection = TURN_OFF_IMAGE;
    retval = BOB_OK;

    // clear the selected component list
    fnInitSelCompList();

    // determine the print map type to use based on our print mode
    switch(bUICPrintMode)
    {
        case BOB_AD_ONLY_MODE:
        case BOB_DEBIT_FUNDS:
            ucPrintMapType = IMG_ID_NORMAL_INDICIA;
            break;

        case BOB_PERMIT_MODE:
            ucPrintMapType = IMG_ID_PERMIT1_PE;
            break;

        case BOB_DATE_STAMP_MODE:
        case BOB_DATE_STAMP_ONLY_MODE:
            ucPrintMapType = IMG_ID_DATE_TIME;
            break;

        case BOB_TAX_MODE:
            ucPrintMapType = IMG_ID_TAX_GRAPHIC;
            break;

        default:
            ucPrintMapType = TURN_OFF_IMAGE_BYTE;
            break;
    }

    // scan through all the print maps and...
    for(i = 0; i < MAX_NUM_PRINT_MAPS; i++)
    {
        // find the appropriate one
        if(ucPrintMapType == rPrintMap[i].ucPrintMapType)
        {
            // for each region in this print map, 
            for (j=0; j < MAX_NUM_REGIONS_PER_AREA; j++)
            {
                if(rPrintMap[i].aRegionCompTypes[j] == TURN_OFF_IMAGE_BYTE)
                    break;
                //  get the component type and... 
                ucCompType = rPrintMap[i].aRegionCompTypes[j];
                k = 0;
                while( bobsSelectRef[k].componentID != TURN_OFF_IMAGE_BYTE) 
                {
                    if(bobsSelectRef[k].componentID == ucCompType)
                    {
                        switch(bobsSelectRef[k].selectionAddressType)
                        {
                            case SEL_ADR_IS_COMP_TYPE:
                                // for these components in the print map...
                                // the selection address contains the actual component type
                                // and for the selected component list, only the component type
                                // is relevant, not the graphic ID
                                ucCompType = *(bobsSelectRef[k].selectionAddress) & 0xFF;
                                usCurrentSelection = *(bobsSelectRef[k].selectionAddress);
                                break;

                            case SEL_ADR_IS_NOT_USED:
                                // for these components in the print map...
                                // the component type in the print map is correct
                                // and for the selected component list, only the component type
                                // is relevant, not the graphic ID
                                usCurrentSelection = ucCompType;
                                break;

                            case SEL_ADR_IS_GRF_ID:
                            default:
                                // for these components in the print map...
                                // the component type in the print map is correct
                                // and for the selected component list, both the component type
                                // and graphic ID are relevant
                                usCurrentSelection = *(bobsSelectRef[k].selectionAddress);
                                break;
                        }
                        break;
                    }
                    k++;                
                }
                // populate the selected component list
                if(ucCompType == TURN_OFF_IMAGE_BYTE)
                {
                    // no more regions to scan in this print map, exit loop
                    if(rPrintMap[i].aRegionCompTypes[j] == TURN_OFF_IMAGE_BYTE)
                    {
                        j = MAX_NUM_REGIONS_PER_AREA; 
                    }
                }
                else
                {
                    // If this component is a Permit, the print map would always specify IMG_ID_PERMIT1_PE
                    // but the real component type we need could be one of three permit component types
                    // so lets hope BOB knows which one to use...
                    if(   (ucCompType == IMG_ID_PERMIT1_PE) 
                       && (currentPermitTypeSelection != TURN_OFF_IMAGE) )
                    {
                        ucCompType = 0x00FF & currentPermitTypeSelection;
                    }
                    fnAdd2SelCompList(ucCompType, usCurrentSelection);
                }
            }
            break;
        }
    }

    return(retval);
}


// IPSD packet message support stuff
//****************************************************************************
// FUNCTION NAME:   fnSendIPSDPacket
//   DESCRIPTION:   Updates the packet data that changes with each packet.
//                  The data that is the same for each message should already 
//                  be loaded.  
//
//        AUTHOR:   Clarisa Bellamy, Craig DeFilippo
//
//     ARGUMENTS:   Std PreMsg
//        RETURN:   Std PreMsg
//
//         NOTES:   
//-----------------------------------------------------------------------------
char fnSendIPSDPacket( ushort ignore )
{
    ushort  len;
    uchar   P1;
    char retval;

    retval = BOB_OK;
    if( rIPSD_packetInfo.wLeftToSend )
    {
        // First set P1, and get the length of the packet to send...
        if( rIPSD_packetInfo.wLeftToSend == rIPSD_packetInfo.wTotalSize)
        {
            // This is the first packet...
            if( rIPSD_packetInfo.wLeftToSend > MAX_IPSD_PCKT_SZ )
            {
                len = MAX_IPSD_PCKT_SZ;
                P1 = IPSD_PCKTCODE_FIRST;
            }
            else 
            {
                len = rIPSD_packetInfo.wLeftToSend;
                P1 = IPSD_PCKTCODE_ONLY1;
            }
        }
        else
        {
            // This is NOT the first packet...
            if( rIPSD_packetInfo.wLeftToSend > MAX_IPSD_PCKT_SZ )
            {
                len = MAX_IPSD_PCKT_SZ;
                P1 = IPSD_PCKTCODE_MDL;
            }
            else 
            {
                len = rIPSD_packetInfo.wLeftToSend;
                P1 = IPSD_PCKTCODE_LAST;
            }
        }
        // Load the Packet Information Structure...
        memcpy( pIPSD_packet_data,  rIPSD_packetInfo.pCurrPcktData, len );
        rIPSD_packetInfo.rHdr.bPacketCode = P1;
        rIPSD_packetInfo.pCurrPcktData += len;
        rIPSD_packetInfo.wLeftToSend -= len;
        rIPSD_packetInfo.rHdr.bPacketSize = (char) len;
        retval = BOB_OK;
    }   
    else
        retval = BOB_SKIP;

    return( retval );
}                                     

//****************************************************************************
// FUNCTION NAME:   fnWhileMoreIPSDPackets
//   DESCRIPTION:     
//
//        AUTHOR:   Clarisa Bellamy, Craig DeFilippo
//
//     ARGUMENTS:   Std PostMsg
//        RETURN:   If this is not the last packet, return BOB_REPEAT.
//                  If it IS the last packet, return BOB_OK.
//
//         NOTES:   
//-----------------------------------------------------------------------------
char fnWhileMoreIPSDPackets(ushort ignore, BOOL msgStat)
{
    char    retval = msgStat;

    if( rIPSD_packetInfo.wLeftToSend && (msgStat == BOB_OK))
    {
        retval = BOB_REPEAT; 
    }
    else if(rIPSD_packetInfo.wLeftToSend == 0)
    {
        memcpy(pIPSD_savedStatus, pIPSD_4ByteCmdStatusResp, sizeof(pIPSD_savedStatus)); 
    }

    return( retval );
}

//****************************************************************************
// FUNCTION NAME:   fnWhileMoreIPSDPacketsAllowLPE
//   DESCRIPTION:   Packet transmission repeat postfunction.
//                  Will allow Last packet error to go unprocessed. Because...
//
//  The status of the last packet is the overall message status. If it is not
//  BOB_OK we did not get 9000 back from the i-button.  Normally this will
//  cause fnBobErrorStandards to put the error in ErrClassFrombob and
//  ErrCodeFrombob and set bobErrFlag true and the IPSD status variable will
//  be cleared to prevent a false (error) lookup hit on any subsequent call to
//  fnBobErrorStandards.  Also script processing will stop, this functiona allows
//  subsequent script processing to continue, and the error will still be posted.
//
//        AUTHOR:   Craig DeFilippo
//
//     ARGUMENTS:   Std PostMsg
//        RETURN:   If this is not the last packet, return BOB_REPEAT.
//                  If it IS the last packet, return BOB_ALLOW_BUT_POST or BOB_OK
//
//         NOTES:   
//-----------------------------------------------------------------------------
char fnWhileMoreIPSDPacketsAllowLPE(ushort ignore, BOOL msgStat)
{
    char    retval = msgStat;

    if( rIPSD_packetInfo.wLeftToSend && (msgStat == BOB_OK))
    {
        retval = BOB_REPEAT; 
    }
    else if(rIPSD_packetInfo.wLeftToSend == 0)
    {
        memcpy(pIPSD_savedStatus, pIPSD_4ByteCmdStatusResp, sizeof(pIPSD_savedStatus)); 

        if(msgStat != BOB_OK)
            retval = BOB_ALLOW_BUT_POST;
    }

    return( retval );
}

//****************************************************************************
// FUNCTION NAME:   fnLoadEdmXfrCtrlPtr
//   DESCRIPTION:   Loads the localXferCrtlStructPtr from the edmXferStructPtr
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   Std PreMsg
//        RETURN:   BOB_OK
//         NOTES:   This used in SCRIPTS called from the EDM through fnWriteData.
//-----------------------------------------------------------------------------
char fnLoadEdmXfrCtrlPtr( ushort msgID )
{
    if( bGotXfrStructPtr == FALSE )
    {
        localXferCrtlStructPtr = edmXferStructPtr;
        bGotXfrStructPtr = TRUE;            
    }
    return( BOB_OK );
}

//****************************************************************************
// FUNCTION NAME:   fnLoadXfrCtlPtr
//   DESCRIPTION:   If the bGotXfrStructPtr flag is FALSE, this will attempt
//                  to load the localXferCrtlStructPtr from the Intertask message.
//                  if the flag is TRUE, the localXferCrtlStructPtr MUST already
//                  be loaded.
//                  The bGotXfrStructPtr flag is always cleared when this function
//                  is exited.
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   Std PreMsg
//        RETURN:   BOB_OK
//
//-----------------------------------------------------------------------------
char fnLoadXfrCtlPtr( ushort msgID )
{
    // IF this Script is called from fnWriteData, then the flag will be set and the
    //  pointer is already loaded.
    if( bGotXfrStructPtr == FALSE )
    {
        localXferCrtlStructPtr = (struct TransferControlStructure *)(bobsCommand.IntertaskUnion.PointerData.pData);
        bGotXfrStructPtr = TRUE;                    
    }
    return( BOB_OK );
}

//****************************************************************************
// FUNCTION NAME:   fnIPSDLoadRedirectToPacket
//   DESCRIPTION:   This function is used to get info about data that is 
//                  being redirected FROM a Transfer control structure to 
//                  the IPSD.
//                  It loads up the packet info struct so that the standard 
//                  IPSD-packetized-data method can be used to send the data
//                  to the IPSD.
//
//        AUTHOR:   Clarisa Bellamy
//
//     ARGUMENTS:   Std PreMsg
//        RETURN:   BOB_OK
//
//         NOTES:   if the bGotXfrStructPtr flag is FALSE, this will attempt
//                  to load the localXferCrtlStructPtr from the Intertask message.
//                  if the flag is TRUE, the localXferCrtlStructPtr MUST already
//                  be loaded.
//-----------------------------------------------------------------------------
char fnIPSDLoadRedirectToPacket( ushort msgID )
{
    uchar    *pMsgData;
    ushort  len;

    (void)fnLoadXfrCtlPtr( msgID );
    // Clear the flag for the next call.
    bGotXfrStructPtr = FALSE;

    // Get all the necessary data from the XfrControlStruct...
    len = localXferCrtlStructPtr->bytesInSource;
    pMsgData = (uchar *)(localXferCrtlStructPtr->ptrToDataToSendToDevice);
    
    rIPSD_packetInfo.wLeftToSend   = rIPSD_packetInfo.wTotalSize    = len;
    rIPSD_packetInfo.pCurrPcktData = rIPSD_packetInfo.pMsgData      = pMsgData;

    return( BOB_OK );
}

//****************************************************************************
// FUNCTION NAME:   fnIPSDGetReplyXferCtrlStructPtr
//   DESCRIPTION:   Loads the localXferCrtlStructPtr with the address passed
//                  in the intertask message.  
//
//        AUTHOR:   
//
//     ARGUMENTS:   Std PreMsg
//        RETURN:   BOB_OK
//
//         NOTES:   If the bGotXfrStructPtr flag is set on entry, then the 
//                  transferControlStruct pointer is already loaded from a 
//                  calling script.  
//                  This should be the last prefunction run before the 
//                  message is sent.  So it clears the flag
//-----------------------------------------------------------------------------
char fnIPSDGetReplyXferCtrlStructPtr( ushort msgID )
{
    // Load the TransferControlStructurePointer from the Intertask message.
    (void)fnLoadXfrCtlPtr( msgID );
    // Clear the flag for the next call.
    bGotXfrStructPtr = FALSE;
    
    return( BOB_OK );
}

#if 0
//****************************************************************************
// FUNCTION NAME:   
//   DESCRIPTION:   
//
//        AUTHOR:   
//
//     ARGUMENTS:   Std PreMsg
//        RETURN:   Std PreMsg
//
//         NOTES:   
//-----------------------------------------------------------------------------
char fnIPSDLoadSecretExchgKey( ushort msgID )
{
    (void)fnIPSDLoadRedirectToPacket( msgID );
    rIPSD_packetInfo.rHdr.bP2 = IPSD_SKEY_TY_SECXCH;
    return( BOB_OK );
}

//****************************************************************************
// FUNCTION NAME:   
//   DESCRIPTION:   
//
//        AUTHOR:   
//
//     ARGUMENTS:   Std PreMsg
//        RETURN:   Std PreMsg
//
//         NOTES:   
//-----------------------------------------------------------------------------
char fnIPSDLoadKeypadRefillKey( ushort msgID )
{
    (void)fnIPSDLoadRedirectToPacket( msgID );
    rIPSD_packetInfo.rHdr.bP2 = IPSD_SKEY_TY_KEYPAD_REFILL;
    return( BOB_OK );
}

//****************************************************************************
// FUNCTION NAME:   
//   DESCRIPTION:   
//
//        AUTHOR:   
//
//     ARGUMENTS:   Std PreMsg
//        RETURN:   Std PreMsg
//
//         NOTES:   
//-----------------------------------------------------------------------------
char fnIPSDLoadKFabMA( ushort msgID )
{
    (void)fnIPSDLoadRedirectToPacket( msgID );
    rIPSD_packetInfo.rHdr.bP2 = IPSD_SKEY_TY_KFABMA;
    return( BOB_OK );
}

//****************************************************************************
// FUNCTION NAME:   
//   DESCRIPTION:   
//
//        AUTHOR:   
//
//     ARGUMENTS:   Std PreMsg
//        RETURN:   Std PreMsg
//
//         NOTES:   
//-----------------------------------------------------------------------------
char fnIPSDLoadKMA( ushort msgID )
{
    (void)fnIPSDLoadRedirectToPacket( msgID );
    rIPSD_packetInfo.rHdr.bP2 = IPSD_SKEY_TY_KMA;
    return( BOB_OK );
}
#endif

//****************************************************************************
// FUNCTION NAME:   
//   DESCRIPTION:   
//
//        AUTHOR:   
//
//     ARGUMENTS:   Std PreMsg
//        RETURN:   Std PreMsg
//
//         NOTES:   
//-----------------------------------------------------------------------------
char fnIPSDLoadEncrKey( ushort msgID )
{
    (void)fnIPSDLoadRedirectToPacket( msgID );
    rIPSD_packetInfo.rHdr.bP2 = bIPSDEncrKeyType;
    return( BOB_OK );
}

//****************************************************************************
// FUNCTION NAME:   fnIPSDPacketize2IButtonP20
//   DESCRIPTION:   
//
//        AUTHOR:   
//
//     ARGUMENTS:   Std PreMsg
//        RETURN:   Std PreMsg
//
//         NOTES:   
//-----------------------------------------------------------------------------
char fnIPSDPacketize2IButtonP20( ushort msgID )
{
    (void)fnIPSDLoadRedirectToPacket( msgID );
    rIPSD_packetInfo.rHdr.bP2 = 0;
    return( BOB_OK );
}

//****************************************************************************
// FUNCTION NAME:   fnPreIPSDVerifyHashSignature
//   DESCRIPTION:   Set up a xfer control structure to send the verify hash
//                  signature command.  The pointer to the verify hash
//                  signature data record is a bob variable that can be written
//                  to via fnWriteData.
//        AUTHOR:   Craig DeFilippo
//
//     ARGUMENTS:   Std PreMsg
//        RETURN:   Std PreMsg
//
//         NOTES:   
//-----------------------------------------------------------------------------
char fnPreIPSDVerifyHashSignature( ushort msgID )
{
    ushort usVhsRecSz = 0;

    localXferCrtlStructPtr = &localXferCrtlStruct;
    bGotXfrStructPtr = TRUE;                    
    memset( localXferCrtlStructPtr, 0, sizeof(localXferCrtlStruct) );

    localXferCrtlStruct.ptrToDataToSendToDevice = (UINT8 *)ulVerifyHashSignatureDataPtr;

    //check the record type, they have different sizes
    switch( *localXferCrtlStruct.ptrToDataToSendToDevice )
    {
        case IPSD_ECDSA_VHS_REC_TYPE:
            usVhsRecSz = IPSD_SZ_ECDSA_VERIFY_HASH_SIGNATURE;
            break;
        case IPSD_DSA_VHS_REC_TYPE:
        default:
            usVhsRecSz = IPSD_SZ_DSA_VERIFY_HASH_SIGNATURE;
            break;
    }

    localXferCrtlStruct.bytesInSource = usVhsRecSz;

    (void)fnIPSDLoadRedirectToPacket( msgID );

    rIPSD_packetInfo.rHdr.bP2 = 0;
    return( BOB_OK );
}

//
// Don't know what else needs to be done here.   ....
// 
//
//****************************************************************************
// FUNCTION NAME:   
//   DESCRIPTION:   
//
//        AUTHOR:   
//
//     ARGUMENTS:   Std PostMsg
//        RETURN:   Std PostMsg
//
//         NOTES:   
//-----------------------------------------------------------------------------
char fnReplyIPSDredirect(ushort ignore, BOOL msgStat)
{
    // Perhaps check the pIPSD_cmdStatusResp to see if it is OK.
    return( msgStat );
}

//****************************************************************************
// FUNCTION NAME:   fnIPSDGatherInit
//   DESCRIPTION:   Prefunction that runs before sending the INITIALIZE msg
//                  to the IPSD.  It puts the init data into a long message,
//                  then gets it ready to send out in packets.
//
//        AUTHOR:   Clarisa Bellamy    
//
//     ARGUMENTS:   Std PostMsg
//        RETURN:   BOB_OK, unless the total message length is too long
//                  to fit in the buffer.  We can't handle that yet.
//
//         NOTES:   If the total message length ever does get too long
//                  we'll need to rewrite a few things to check for
//                  more input before the last packet is sent out.
//-----------------------------------------------------------------------------
char fnIPSDGatherInit( ushort msgID )
{
    char    retval = BOB_OK;

    if( wIPSD_InitDataSize <= IPSD_MAX_UNCHUNKED_MSGDATA_LEN )
    {
        // Load the packet data
        rIPSD_packetInfo.wLeftToSend   = rIPSD_packetInfo.wTotalSize    = wIPSD_InitDataSize;
        rIPSD_packetInfo.pCurrPcktData = rIPSD_packetInfo.pMsgData      = pIPSD_InitDataGlob;
        // For initialize message, P2 is 0.
        rIPSD_packetInfo.rHdr.bP2 = 0;
    }
    else
    {
        // ?? Want to create and set a new bob error here, data is too long for internal buffer.    
        retval = BOB_SICK;
    }
    return( retval );
}

//****************************************************************************
// FUNCTION NAME:   fnIPSDSetDateTime
//   DESCRIPTION:   Calculate the difference between the IPSD RTC and the 
//                  system time which is set by MFG.  
//
//        AUTHOR:   Sam Thillaikumaran   
//
//     ARGUMENTS:   Std PostMsg
//        RETURN:   Std PostMsg
//
//         NOTES:   
//-----------------------------------------------------------------------------
char fnIPSDSetDateTime( ushort msgID, BOOL msgStat )
{
//    long IPSDtime;
//    long lTime;

    //Set the system clock to the time received from MFG
    (void)SetSystemClock( &IPSDSetimeclock ); 
	SysClockSetByPSD();
    //Convert systeime to the IPSD time format
    //fnSysTimeToIPSD( &IPSDtime,&IPSDSetimeclock);
    //memcpy( (char *)&lTime,pIPSD_RealTimeClock,IPSD_SZ_TIME );
    //Caluculate the differences between System time and the IPSD time
    //store this value (this value wil be the offset sent back to MFG).
       
    return( msgStat );
}                                     

/* **********************************************************************

 FUNCTION NAME:  fnIPSDGetDateTime  

 PURPOSE:       After the RealTimeClock has been read from the IPSD.
                Converts the IPSD Time to String.
                    
 AUTHOR:        Sam Thillaikumaran

 INPUTS:        bMsgID  not used here.

 NOTES:         use GetRTC command to get 
                RealTimeClock value JUST before this command is run.
        
 *************************************************************************/
char fnIPSDGetDateTime( ushort msgID, BOOL msgStat ) 
{
    DATETIME    sysTime;
    long        IPSDtime;
    long        lOffset;


    memcpy( (char *)&IPSDtime, pIPSD_RealTimeClock, IPSD_SZ_TIME );    //TODO does this need to be swapped?
    EndianAwareCopy( (char *)&lOffset, pIPSD_clockOffset, IPSD_SZ_TIME );
    // Get the GMT time from the IPSD: GMT = RTC + ClockOffset
    IPSDtime += lOffset;

#if defined (JANUS) && !defined (K700)
    {
    extern long lDcapTimestampFudge;
    extern unsigned short wBuildTestMode;

    /* data capture time fudge is in seconds and it applies as long as we are in test mode */
    if (wBuildTestMode == IN_BUILD_TEST_MODE)
        IPSDtime += lDcapTimestampFudge;
    }
#endif

    // Convert GMT in IPSD format to GMT in system time format.
    fnIPSDTimeToSys( &sysTime, IPSDtime );
    sprintf( (char *)IPSDTimerecord, "%02d%02d%02d%02d%02d%02d%02d", sysTime.bCentury, sysTime.bYear,
             sysTime.bMonth, sysTime.bDay, sysTime.bHour, sysTime.bMinutes, sysTime.bSeconds );

    return( msgStat );
}


/* **********************************************************************

 FUNCTION NAME:  fnIPSDGetClockOffset   

 PURPOSE:       After the RealTimeClock has been read from the IPSD.
                Converts the IPSD Time to String.
                    
 AUTHOR:        Sam Thillaikumaran

 INPUTS:        bMsgID  not used here.

 NOTES:         use GetRTC command to get 
                RealTimeClock value JUST before this command is run.
        
 *************************************************************************/
char fnIPSDGetClockOffset( ushort msgID, BOOL msgStat ) 
{
    long IPSDtime;
    long lTime;
    long lOffset;
    DATETIME    currentTime;

    // Get the current time (GMT) 
    (void)GetSysDateTime( &currentTime, NOOFFSETS, GREGORIAN );
    //Convert systeime to the IPSD time format
    fnSysTimeToIPSD( &IPSDtime,&currentTime);
    memcpy( (char *)&lTime,pIPSD_RealTimeClock,IPSD_SZ_TIME );
    //Caluculate the differences between System time and the IPSD time
    //store this value (this value wil be the offset sent back to MFG).
    lOffset = IPSDtime - lTime;
    memcpy( pEdmIPSDClockOffset,(char *)&lOffset,IPSD_SZ_TIME );
    return( msgStat );
}

/******************************************************************************
   FUNCTION NAME: fnHandUpdateRegsAndPC
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : Updates the registers and piece count by hand.  This is done
                    here, since we don't have time to read them from the IPSD 
                    after the debit and before they are used in the image.
                    They will be read and updated for real at the end of the run.
                    OR while the piece is printing.
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
   Modification History:
   2010.07.07 Clarisa Bellamy - Modified to use the debited postage value instead
                    of bobsPostageValue, because bobsPostageValue may have been 
                    changed between the precreate and the debit.
                    Also, added lines to save the hand-updated register value for 
                    later verification when the registers are actually read.  
                    If they don't match, an entry will be put in the error log. 
   2008-08-05    Joe Qu             call fnComputeDerivedPieceCount( )just before computing
                                    the control sum, to keep ulDerivedPieceCount and 
                                    pIPSD_pieceCount in sync
                                    
                 Clarisa Bellamy    Initial version
******************************************************************************/
char fnHandUpdateRegsAndPC(ushort ignore, BOOL msgStat) 
{
    char    retval = msgStat;
    MONEY   m5TempReg;
    MONEY   m5PostageVal;
    ulong   ulDescReg;
    ulong   ulPostageVal;
    ulong   ulPieceCount;
    ulong   ulZeroPieceCount;

    if( retval == BOB_OK )
    {
        // Convert a 4-byte char array to a ulong.
        EndianAwareCopy( (char *)&ulDescReg, m4IPSD_descReg, sizeof(UINT32) );
        ulPostageVal = EndianAwareGet32(&ulDebitedPostageValue);
        
		// Make sure there really is money in there.
		if( ulDescReg < ulPostageVal )
        {   
			char tempBuff[80];
			(void)sprintf( tempBuff, "Not enough funds DR 0x%08lX <  Postage 0x%08lX", 
							ulDescReg, ulPostageVal);
			fnDumpStringToSystemLog( tempBuff );
            
			fnSetBobInternalErr(BOB_BATCHUPDATE_ERR);
            return( BOB_SICK );
        }

        // Subtract the postage value from the Descending Register.
        // and restore it in the array version.
        ulDescReg -= ulPostageVal;
        EndianAwareCopy( m4IPSD_descReg, (char *)&ulDescReg, sizeof(ulong)  );
    
        // Add postage value to Ascending register:
        //      Convert postage value to MONEY type (5-bytes).
        memset( m5PostageVal, 0, SPARK_MONEY_SIZE );
        EndianAwareCopy( &m5PostageVal[ SPARK_MONEY_SIZE - 4 ], &ulPostageVal, sizeof(ulong) );
        //      Add 5-byte postage value to the ascending register.
        fnAddMoneyParameters( m5TempReg, m5PostageVal, m5IPSD_ascReg );
        memcpy( m5IPSD_ascReg, m5TempReg, SPARK_MONEY_SIZE );
    
        // Increment PieceCount
        memcpy( (char *)&ulPieceCount, pIPSD_pieceCount, sizeof(ulong) );
        ulPieceCount++;
        memcpy( pIPSD_pieceCount, (char *)&ulPieceCount, sizeof(ulong) );

        // Increment ZeroPieceCount if the Postage Value is 0.
        if( ulPostageVal == 0 )
        {
            memcpy( (char *)&ulZeroPieceCount, pIPSD_ZeroPostagePieceCount, sizeof(ulong) );
            ulZeroPieceCount++;
            memcpy( pIPSD_ZeroPostagePieceCount, (char *)&ulZeroPieceCount, sizeof(ulong) );
        }

        // At the end of the debit ( after the data has been sent to the printhead ), 
        // the ulDerivedPieceCount and Control Sum are updated from the i-button 
        // pIPSD_pieceCount, to keep them from getting out of synch. 
        retval = fnComputeDerivedPieceCount( ignore, BOB_OK );
        // Don't verify this time.
        fBobVerifyRegs = FALSE;
        retval = fnComputeControlSum( ignore, BOB_OK );

        // The next time we call fnComputeControlSum should be after we read the 
        //  registers from the ipsd.  So save the AR and DR values so that we can
        //  confirm they were correct, and set the verify flag.
        memcpy( m5Verify_AR, m5IPSD_ascReg, 5 );
        memcpy( m4Verify_DR, m4IPSD_descReg, 4 );
        fBobVerifyRegs = TRUE;

        // Update the piece count and zero piece count from bob's hand-updated
        // copies to the parameter list glob that we last read from the ipsd.  
        // The IBI record instructs the image generator to look in that glob
        //  for this information.   Time tests show that it takes < 1 msec.
        fnUpdatePSDParamListFromBob( GLOBID_PIECECOUNT );
        fnUpdatePSDParamListFromBob( GLOBID_ZEROPOSTAGEPC );

        sprintf( pBobsLogScratchPad, "Hand update registers done." );
        fnSystemLogEntry( SYSLOG_TEXT, pBobsLogScratchPad, strlen(pBobsLogScratchPad) );
    }
    return( retval );
}

/* **********************************************************************

 FUNCTION NAME:     char fnUpdateTheBatchSub(ushort ignore, BOOL msgStat) 

 PURPOSE:           
                    
 AUTHOR:            blu

 INPUTS:            Argument msgStat is the success/fail status of the device message
                    or message operation that has just been attempted.
 
                    Argument "ignore" is the device message ID of the message being
                    serviced.  In this function the argument is labeled as "ignore"
                    because the argument is not used.  It is only passed for
                    compatibility with the postMessage function prototype.
 HISTORY:
 2010.07.13 Clarisa Bellamy - Now it uses new static variable ulDebitedPostageValue, 
                    because bobsPostageValue may have been changed by another task 
                    between the precreate and the debit.  We have added code in other 
                    tasks to try to prevent them from changing bobsPostageValue during
                    that time, but we want to check here, and log an error in both
                    the system log and the errror log if it happens.
                    

 *************************************************************************/
char fnUpdateTheBatchSub(ushort ignore, BOOL msgStat, BOOL fHandUpdateRegs ) 
{
    char    retval;             
    ulong   ulBobsPostage;      // bobsPostageValue in ulong form for comparison with the debitvalue.
    ushort  localErr;

    
    retval = BOB_SICK;
    localErr = BOB_BATCHUPDATE_ERR;

    if (msgStat == BOB_OK)
    {
        retval = BOB_OK;
        fBobDebitOK = TRUE;

        // French power fail recovery
        fnIndiciaDebitPowerFailLog();

        // Convert bobsPostageValue to a long so we can detect if it got modified between the 
        // PreCreate and the Debit.
        memcpy( &ulBobsPostage, bobsPostageValue, sizeof( ulBobsPostage ) ); 
        if( ulDebitedPostageValue != ulBobsPostage )
        {
            // Log when bobsPostageValue got updated between the PreCreate and the Debit
            sprintf( pBobsLogScratchPad, 
                     "DEBIT Timing Issue: DebitedValue=%ld, bobsPostage=%ld",
                     (long)ulDebitedPostageValue,
                     (long)ulBobsPostage ); 
            fnDumpStringToSystemLog( pBobsLogScratchPad );
			
            // This should not happen, so log it, because if it is happening, 
            //  then it should be investigated.
            //fnLogError( ERR_TYPE_NORMAL, ERROR_CLASS_JBOB_TASK, BOB_BADTIME_BOBSPOSTAGEVAL_CHANGE );
        }
        

        // Do Batch register book keeping
        fnIncrementKipCount();

        // Do departmental accounting book keeping
    }
	
    // To save time we may not read the regs and piece counts until later, so update them by hand now.
    if( fHandUpdateRegs == UPDATE_REGS_YES )
        retval = fnHandUpdateRegsAndPC( ignore, msgStat );

    fnCmosUpdatePostageChangeCount();

    switch( fnOITGetPrintMode() )
    {
        case PMODE_AJOUT_MANUAL_WGT_ENTRY:
        case PMODE_AJOUT_PLATFORM:
//TODO is this needed          retval = fnUpdateAjoutGlobalCorrectionValue();
          break;

        default:
          break;
    }

    if (retval == BOB_SICK && msgStat == BOB_OK)
        fnSetBobInternalErr(localErr);

    // Save the printed date.  
    //  This is checked if we need to force an upload whenever the date has 
    //  changed.  (Some countries do.)
    // The variable is in CMOS because it may need to be checked on next powerup.
    // NOTE: In Janus this statement is in the fnBobShadowDebit, but I think 
    //      this is a more appropriate location. -cwb
    CMOSLastPrintedMailDate = msgPrintTime;

    return(retval);
}

/* **********************************************************************

 FUNCTION NAME:     char fnUpdateTheBatch(ushort ignore, BOOL msgStat) 

 PURPOSE:           
                    
 AUTHOR:            blu

 INPUTS:            Argument msgStat is the success/fail status of the device message
                    or message operation that has just been attempted.
 
                    Argument "ignore" is the device message ID of the message being
                    serviced.  In this function the argument is labeled as "ignore"
                    because the argument is not used.  It is only passed for
                    compatibility with the postMessage function prototype.
 *************************************************************************/
char fnUpdateTheBatch( ushort ignore, BOOL msgStat ) 
{
    char    retval;

    // Do not hand update the registers and piece counts, they are read from the IPSD 
    //  in subsequent script lines. 
    retval = fnUpdateTheBatchSub( ignore, msgStat, UPDATE_REGS_NO );

    return( retval );
}

/* **********************************************************************

 FUNCTION NAME:     char fnUpdateTheBatchAndRegs(ushort ignore, BOOL msgStat) 

 PURPOSE:           
                    
 AUTHOR:            blu

 INPUTS:            Argument msgStat is the success/fail status of the device message
                    or message operation that has just been attempted.
 
                    Argument "ignore" is the device message ID of the message being
                    serviced.  In this function the argument is labeled as "ignore"
                    because the argument is not used.  It is only passed for
                    compatibility with the postMessage function prototype.
 *************************************************************************/
char fnUpdateTheBatchAndRegs( ushort ignore, BOOL msgStat ) 
{
    char    retval;
    
    // Make sure the subroutine also hand updates the registers and piece counts.
    retval = fnUpdateTheBatchSub( ignore, msgStat, UPDATE_REGS_YES );

    return( retval );
}


//******************************************************************************
//   FUNCTION NAME: fnSetMailProgressState
//   AUTHOR       : Bill Bailey
//   DESCRIPTION  : Set CMOS stored mail run state to passed value
//   
//   PARAMETERS   : uchar state to value  PF_PRINT_DONE, PF_DEBIT_COMPLETE, or PF_PRINT_IN_PROGRESS
//   RETURN       : none
//   NOTES       : 
//*****************************************************************************
void fnSetMailProgressState(uchar bCurrState)
{
    *CMOSpfDebitLogRec.pfPmPrintStatePtr = bCurrState;
}


//******************************************************************************
//   FUNCTION NAME: fnGetMailProgressState
//   AUTHOR       : Bill Bailey
//   DESCRIPTION  : returns CMOS stored mail run state
//   
//   PARAMETERS   : none
//   RETURN       : uchar state of value  PF_PRINT_DONE, PF_DEBIT_COMPLETE, or PF_PRINT_IN_PROGRESS
//   NOTES       : 
//*****************************************************************************
uchar fnGetMailProgressState(void)
{
#if defined (K700) || defined (G900)
  return(*CMOSpfDebitLogRec.pfPmPrintStatePtr);
#else
    switch(*CMOSpfDebitLogRec.pfPmPrintStatePtr)
    {
    case PF_DEBIT_COMPLETE:
    case PF_TRANSPORT_START:
    case PF_PRINT_IN_PROGRESS:
        return( PF_PRINT_IN_PROGRESS );
    default:
        return( PF_PRINT_DONE );
    }
#endif
}


/* **********************************************************************

 FUNCTION NAME:     char fnCheckAccountFull(ushort ignore) 

 PURPOSE:           Determines if the current department account is still
                    okay to print mailpieces.  Only called in debit mode, and
                    is among the predebit messages that are used when responding
                    to a MAIL_IN_PROGRESS message.
                    
 AUTHOR:            blu

 INPUTS:            
 HISTORY:
  2010.07.13 Clarisa Bellamy - Use new static variable ulDebitedPostageValue instead
                    of bobsPostageValue, bobsPostageValue may have been changed between
                    the precreate and the debit.            
 *************************************************************************/
char fnCheckAccountFull(ushort ignore)
{

#if 0 //TODO JAH Remove guts but leave function
	ushort  usAccountID;            // current department account ID
    double  dwFunds;                // used to hold the postage setting
    ushort  acctStat;               // status returned when account is examined
    ushort  localErrorCode;         // references an error code that can be posted
    char    retval;                 // return value from this function
    ulong   localFunds;             // used to transfer the postage setting to a double

    retval = BOB_OK;
    localErrorCode = DO_NOT_POST_BOB_ERROR;
//    usAccountID = fnGetActiveAccount();                                         // determine the current department account
    if(usAccountID) {                                                           // and if an account is active,

        localFunds = ulDebitedPostageValue;                                     // load the current postage selection
        dwFunds = localFunds;

//        acctStat = fnCheckAccountLimit(usAccountID, dwFunds, 1L);               // see if the account is allowed one more
        switch(acctStat) {                                                      // mailpiece

   /*         case ACT_EXCEED_MAX_ACCT_VALUE:                                     // reasons for not being allowed include
            localErrorCode = BOB_ACCOUNT_OUTTA_FUNDS;                           // not being allocated enough funds
            retval = BOB_SICK;
            break;

            case ACT_EXCEED_MAX_ACCT_PIECE:                                     // and not be allocated enough pieces
            localErrorCode = BOB_ACCOUNT_OUTTA_PEACE;
            retval = BOB_SICK;
            break;

            case ACT_SUCCESS:
            break;

*/            default:
            localErrorCode = BOB_UNKNOWN_ACCOUNT_ERR;
            retval = BOB_SICK;
            break;
        }
    }

    if (retval != BOB_OK)
        fnSetBobInternalErr(localErrorCode);
//  else 
//  {
//      if (germanPCstate == DISCOUNT_PC_WILL_PRINT)
//          germanDisCountPiece++;
//  }
    return(retval);
#endif
    return(BOB_OK);
}

/* **********************************************************************

 FUNCTION NAME:     fnSetCurReport

 PURPOSE:           
                    
 AUTHOR:            Sam and Craig DeFilippo
                    
                    
 INPUTS:            Argument reportID is the device Message ID of the report 
                    being serviced.
                    
 NOTES:            This is called for the first page of a report.  
                    Bob should be calling fnLoadReport for the next pages.                   
 *************************************************************************/
char fnSetCurReport( uchar ucReportID )
{
#ifdef BOB_TIME_DEBUG
    char            pLogBuf[50];

    sprintf(pLogBuf, "Start Bob  fnSetCurReport");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

    currentRptSelection = ucReportID;
    ucBobsReportPageNbr = 1;
    fnSetCurrentReportPageNbr(1);

    // Do the necessary pre-function (if any).
//    if( ucPageNumber == 1 )
//    {
        switch (currentRptSelection)
        {
            case MULTI_ACCT_SUM_RPT:
                fnPreMultiAccountReport();
                break;

	        case RATES_DOWNLOAD_RPT:
	            fnPreRatesDnldReport();
	            break;

	        case RATES_SUMMARY_RPT:
	            fnPreRatesSumReport();
	            break;

            default:
	            break;
        }
     
        // Finds the total number of pages and sets the bob variable and calls 
        // fnSetTotalReportPages() to set the number that will be displayed on 
        // the screen. 
        (void)fnInitReportPageInfo( 0 );
//    }
#ifdef BOB_TIME_DEBUG
    sprintf(pLogBuf, "End Bob  fnSetCurReport");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
     
    return BOB_OK;  
}


/********************************************************************************/
/* TITLE: fnInitReportPageInfo                                                  */
/* AUTHOR: Craig DeFilippo                                                      */
/* INPUT:                                                                       */
/* DESCRIPTION: Set total number of pages               */
/* OUTPUT: returns void                                                         */
/********************************************************************************/
char fnInitReportPageInfo(ushort ignore)
{
#ifdef BOB_TIME_DEBUG
    char            pLogBuf[50];

    sprintf(pLogBuf, "Start Bob  fnInitReportPageInfo");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif

    usBobsTotalPages = fnDetermineTotalPages(currentRptSelection);
    fnSetTotalReportPages(usBobsTotalPages);

#ifdef BOB_TIME_DEBUG
    sprintf(pLogBuf, "End Bob  fnInitReportPageInfo");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
    
    return(BOB_OK);             
}

/* **********************************************************************

 FUNCTION NAME:     char fnAllowError(ushort ignore, BOOL msgStat) 

 PURPOSE:           
                    
 AUTHOR:            blu

 INPUTS:            Argument msgStat is the success/fail status of the device message
                    or message operation that has just been attempted.
 
                    Argument "ignore" is the device message ID of the message being
                    serviced.  In this function the argument is labeled as "ignore"
                    because the argument is not used.  It is only passed for
                    compatibility with the postMessage function prototype.
                    
 *************************************************************************/
char fnAllowError(ushort ignore, BOOL msgStat) {

    char    *myErrorDirectory;
    uchar   myErrGroups;
    short   myID;
    uchar   *statPlace;
    uchar   statSiz;

    myErrorDirectory = fnTeaGetTable(BOBSERRDIRECTORY);
    fnTeaGetConstant(ERRORGROUPCOUNT, &myErrGroups);

    while(myErrGroups)
    {
        if (myErrGroups) myErrGroups--;
        EndianAwareCopy((char *)(&myID), myErrorDirectory + 9, 2);
        statPlace = bobaVarAddresses[myID].addr;

        statSiz = myErrorDirectory[8];                          // erase the device stat after we've searched for the
        memset(statPlace, 0, statSiz);                          // error     
        myErrorDirectory += ERR_DIREC_REK_SIZ;
    }

    return(BOB_ALLOW);
}



/* **********************************************************************

 FUNCTION NAME:     Pre Debit Support Functions

 PURPOSE:           Determine the proper string to be used as the rate
                    class argument for an IPSD.

 AUTHOR:            Craig DeFilippo

 INPUTS:            
 *************************************************************************/
const uchar aRCAsciiZeros[BOBS_RATE_CLASS_SZ] = {0x30, 0x30, 0x30, 0x30};
const uchar aRCZeros[BOBS_RATE_CLASS_SZ] = {0,0,0,0};
const PRE_DEB_RATE_CLASS_TBL tblIPSDPreDeb[] =
{
    {RATE_INFO_TYPE_RM_RATE_CATEGORY,       fnRateClassForPreDeb,   bobsRateClass},  
    {RATE_INFO_TYPE_DCAP_ITEM_CATEGORY,     fnItemCatForPreDeb,     bobsItemCategory},
    {RATE_INFO_TYPE_ASCII_ZEROS,            NULL_PTR,               aRCAsciiZeros}, 
    {RATE_INFO_TYPE_ZEROS,                  NULL_PTR,               aRCZeros},
    {RATE_INFO_TYPE_DCAP_SERV_CODE_BIN,     fnBinServCodeForPreDeb, bobsBinaryServiceCode},
    {RATE_INFO_TYPE_DCAP_SERV_CODE_ASCII,   fnHRServTypeForPreDeb,  readableServType},  
    {RATE_INFO_TYPE_SUPPSTR1_IBICHK,        fnRCFromSuppStr1_IBIChk,    bobsRateClass},
    // Additions go above this line.
    {0xFF,   NULL_PTR,  aRCZeros} // table terminator.       
};

void fnIPSDRateClassForPreDeb(void) {

    ushort i=0;
    void (*pfunct)(void) = NULL_PTR;
    uchar   bRateInfoType = fnFlashGetByteParm(BP_RATE_INFO_TYPE);


    while(tblIPSDPreDeb[i].ucEMDmethod != 0xFF)
    {
        if(tblIPSDPreDeb[i].ucEMDmethod == bRateInfoType)
        {
            pfunct = tblIPSDPreDeb[i].pRCfunct;
            break;
        }
        i++;
    }

    if( pfunct != NULL_PTR )
    {
        pfunct();
    }
    
    memcpy( rcString, tblIPSDPreDeb[i].ptrResult, sizeof(rcString));
    
    return;
}

// ********************************************************************************
// FUNCTION NAME:   fnRCFromSuppStr1_IBIChk()                               
// AUTHOR:      CBellamy
// PURPOSE:     
//      Retrieve the rate class from Supplemental String 1.  Depending upon 
//      the indicia type currently selected, convert the rate class to ASCII 
//      or Binary. 
//      Copy the result to bobsRateClass.
// INPUTS:      None
// OUTPUTS:     
//      4-byte Rate Class data is in bobsRateClass, ready to be copied
//      into the create indicia message.
// Returns:     None
// NOTES: 
//  For this implementation, the Rate Info is an ASCII representation of a 
//  decimal numeric value.  A unicode version of the ASCII string is in 
//  "Supplemental String 1".  Once the unicode string is converted to ASCII...
//  If the indicia is not IBI-Lite, take ASCII string as is.  Unless it is 
//  not 4 chars.  If the string  is more than 4 chars, take the last 4 chars;
//  If it is less than 4 chars, pad the beginning with "0".
//  If the IBI-Lite indicia is currently selected, then we need to convert the 
//  number to a binary (hex), and take the least significant 4 bytes.
//  If there is an error, then the value 0 is passed.
//  See examples embedded below.
//-----------------------------------------------------------------------------
static void fnRCFromSuppStr1_IBIChk( void ) 
{
    UINT32  ulTempLong;
    SINT32  slRateVal = 0;                              // Numeric conversion of Ascii copy of IndiLine1.
    UINT16  bLen;                                   // 
    UINT16  offset = 0;
    char *  pCode;
    char   pTempAscii[ PCL_STRING_LENGTH +1 ];         // Ascii copy of IndiLine1
    char   pTempAsciiFmtd[ BOBS_RATE_CLASS_SZ +1 ];    // Last 4 bytes of Ascii copy of IndiLine1
    BOOL    fGotCode = FALSE;

    if(strncmp((char *)bobsRateClass, "0000", 4) == 0)
    	(void)strcpy(pTempAscii, "0000");   // just so it's init to something
    else
    	(void)strcpy(pTempAscii, (char *)bobsRateClass);   // just so it's init to something

    // Get the current postage value in a format that can be easily checked.
    // Use bobsPostageValue, because this function is called before bobsPostageValue is
    //  copied to ulDebitedPostageValue.
    (void)memcpy(&ulTempLong, bobsPostageValue, sizeof(ulTempLong));

    // do possible special processing for KIP & zero postage mail
    if (!ulTempLong)
    {
        pCode = (char *)fnFlashGetAsciiStringParm(ASP_ZERO_POST_2GEN_DCAP_PRODUCT_CODE);
        // if we can retrieve the product code and the product code isn't a null string,
        // copy the product code to the result buffer
        if (pCode && (strlen(pCode) > 0))
        {
            fGotCode = TRUE;
            (void)strncpy(pTempAscii, pCode, PCL_STRING_LENGTH);
        }
    }

    else if (fnOITGetPrintMode() == PMODE_MANUAL)
    {
        pCode = (char *)fnFlashGetAsciiStringParm(ASP_KIP_2GEN_DCAP_PRODUCT_CODE);
        // if we can retrieve the product code and the product code isn't a null string,
        // copy the product code to the result buffer
        if (pCode && (strlen(pCode) > 0))
        {
            fGotCode = TRUE;
            (void)strncpy(pTempAscii, pCode, PCL_STRING_LENGTH);
        }
    }

    // if we haven't already loaded the rate info,
    // get the Rate Info from the rating module
    if (fGotCode == FALSE)

#if defined (K700) || defined (G900)
    {
//        const struct ind_output    *pDCInfo;
//        SINT16 sRet = fnRateGetIndiciaInfo(&pDCInfo);


#if 0 //TODO RAM remove rates
        if(sRet == 0)
        {
            (void)strcpy(pTempAscii, pDCInfo->BarcodeData->ServiceCodeAlpha);
        }
#endif //TODO RAM remove rates
    }
#else           // must be Janus
    {
        UNICHAR     *pSuppleLine1;                             // Ptr to SuppleLine1 Unichar string.



        // Get a pointer to the unichar string called "Supplemental Line 1".
        pSuppleLine1 = fnGetSupplementalLine( 1 );

        // Convert it to ASCII
        ConvertUnitoAscii( pSuppleLine1, pTempAscii, PCL_STRING_LENGTH );
    }
#endif

    // Find the length.
    bLen = (unsigned short)strlen( pTempAscii );
    if( fIPSD_IbiLiteOn == TRUE )
    {
        // The IBILite indicia wants the 4 byte rate class input value to be 
        //  a BigEndian binary representation of the ascii string.  If there
        //  is any non-numeric data in SuppStr1, then that is the end of the 
        //  number.  i.e. "15ABC24" will be converted to 00 00 00 0F.
        slRateVal = atol( pTempAscii );
        (void)memcpy( bobsRateClass, &slRateVal, BOBS_RATE_CLASS_SZ );
    }
    else
    {   // Not IBI-Lite.  The rate class input needs to be an ASCII string.
        // Use the last 4 bytes of the string.
        //  i.e. If SuppStr1 = "15ABC24",  bobRateClass will be "BC24".
        //       If SuppStr1 = "36",       bobRateClass will be "0036"
        if( bLen < BOBS_RATE_CLASS_SZ )
        {
            (void)fnPadString( pTempAsciiFmtd, pTempAscii, BOBS_RATE_CLASS_SZ, BEFORE );
        }
        else 
        {
            offset = bLen - BOBS_RATE_CLASS_SZ;
            (void)strncpy( pTempAsciiFmtd, &pTempAscii[ offset ] , BOBS_RATE_CLASS_SZ );
            pTempAsciiFmtd[ BOBS_RATE_CLASS_SZ ] = 0;
        }

        (void)memcpy( bobsRateClass, pTempAsciiFmtd, BOBS_RATE_CLASS_SZ );
    }
    return;      
}


void fnItemCatForPreDeb(void)
{
    char    *pItemCategory;

    pItemCategory = fnGetFrenchItemCategory();
    memcpy(bobsItemCategory, (uchar*) pItemCategory, sizeof(bobsItemCategory));
    return;
}

void fnRateClassForPreDeb(void)
{
    return;
}

void fnProdCodeForPreDeb(void) 
{
        // not 3rd generation DCAP
    memset( bobsProductCode, ' ', sizeof(bobsProductCode) );
    return;
}

void fnHRServTypeForPreDeb( void ) 
{

    memset( readableServType, ' ', sizeof(readableServType) );
    return;
}
//----------------------------------------------------------------------------
// Used when EMD parameter BP_RATE_INFO_TYPE == RATE_INFO_TYPE_DCAP_SERV_CODE_BIN (04)
//  (as in Canada) to populate bobsBinaryServiceCode, which is later copied to 
//  rcString (RateCategory in the createIndicia input).
void fnBinServCodeForPreDeb( void ) 
{
    UINT32 ulServCode = 0;		// The Generic Service Code Numeric
    UINT16 usServCode = 0;		// The Canadian value to load into the predebit.
	STATUS status = NU_SUCCESS;

	// Default the value to 0.
	memset( bobsBinaryServiceCode, 0, sizeof(bobsBinaryServiceCode) );

	status = smgr_get_setting_data( SETTING_ITEM_SRVC_CODE_NUM, &ulServCode, sizeof( ulServCode ) );
    if( status == NU_SUCCESS )
    {
		// Convert the generic ServiceCodeNumeric, type UINT32-LE, to the  
		//  Canadian service code: type UINT16-BE.
    	usServCode = (UINT16)ulServCode;
    	usServCode = EndianAwareGet16( &usServCode );
		
		// For Canada, the 16-bit Service Code (BE) belongs in the TOP 2 bytes 
		//  of the 4-byte RateCategory.
		if( sizeof( usServCode ) <= sizeof( bobsBinaryServiceCode ) )
	    	memcpy( bobsBinaryServiceCode, &usServCode, sizeof(usServCode) );
		else
	    	memcpy( bobsBinaryServiceCode, &usServCode, sizeof(bobsBinaryServiceCode) );
    }
	else
	{
		dbgTrace( DBG_LVL_ERROR, "BOB: Could not retrieve ServiceCodeNumeric from SettingsMgr" ); 
	}
    return;
}

void fnBCServTypeForPreDeb( void ) 
{

    memset( bcServicetype, ' ', sizeof(bcServicetype) );
    return;
}


char *fnGetFrenchItemCategory(void) 
{
    static char cItemCategoryAscii[PCODE_SIZE];//To hold the ascii string

    memset( cItemCategoryAscii, 0, sizeof(cItemCategoryAscii) );//For safety

    return(cItemCategoryAscii);
}



/******************************************************************************
   FUNCTION NAME: fnInitSelCompList
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Erase the selected comp list.
                : 
   PARAMETERS   : None
   RETURN       : None
   NOTES        : None
******************************************************************************/
void fnInitSelCompList(void)
{
    memset( &rSelCompList, 0, sizeof(rSelCompList) );
    return;
}

/******************************************************************************
   FUNCTION NAME: fnAdd2SelCompList
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Populate the selected comp list.
                : 
   PARAMETERS   : component type (from graphic comp header)
                : component id (for ads and insc's its the link header graphic id
                :               for all others its the component type
   RETURN       : None
   NOTES        : None
******************************************************************************/
void fnAdd2SelCompList( UINT8 ubCompType, UINT16 usCompID )
{
    UINT16  i = 0;
    BOOL    found = FALSE;

    if(   (ubCompType != TURN_OFF_IMAGE_BYTE) 
       && (usCompID   != TURN_OFF_IMAGE) )
    {
        while(i < rSelCompList.ucNumComponents)
        {
            if(ubCompType == rSelCompList.ucSelCompType[i++])
            {
                found = TRUE;      //duplicate found
                break;
            }
        }
        
        if(!found)
        {
            rSelCompList.ucSelCompType[i] = ubCompType;
            rSelCompList.usSelCompID[i] = usCompID;
            rSelCompList.ucNumComponents++;
        }
    }
}

/******************************************************************************
   FUNCTION NAME: fnInitPrintMaps
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Initialize Bobs Print Maps
                : 
   PARAMETERS   : number of region map recs, pointer to first region map
   RETURN       : None
   NOTES        : None
******************************************************************************/
void fnInitPrintMaps( UINT8 ucNumRegionMapRecords, const UINT8 *pbRegionMap )
{
    ushort i,j,k;
    uchar ucAreaID, ucCompType;
    uchar ucStatus, ucIndex;
    BOOL enabled;

    // init to all FF's
    for(i=0; i < MAX_NUM_PRINT_MAPS; i++)
    {
        memset(&rPrintMap[i], TURN_OFF_IMAGE_BYTE, sizeof(BOB_PRINT_AREA_MAP));
    }

    ucAreaID = *pbRegionMap;
    
    // scan through the region maps in flash and populate BOB's print maps
    for(i=0, j=0, k=0; i < ucNumRegionMapRecords; i++)
    {
        if( ucAreaID != *(pbRegionMap) )
        {
            ucAreaID = *pbRegionMap;
            if(++j >= MAX_NUM_PRINT_MAPS)
                break;

            k = 0;
        }
        ucCompType = *(pbRegionMap + RMOFFS_COMP_TYPE);
        
        // classify the print map type
        switch(ucCompType)
        {
            case IMG_ID_NORMAL_INDICIA:
            case IMG_BAR_LEADING_IMAGE:
            case IMG_BAR_LAGGING_IMAGE:
            case IMG_ID_INTERNAT_LEAD:
            case IMG_ID_INTERNAT_LAG:
                rPrintMap[j].ucPrintMapType = IMG_ID_NORMAL_INDICIA;
                break;

            case IMG_ID_PERMIT1_PE:
            case IMG_ID_PERMIT2_PE:
            case IMG_ID_PERMIT3_PE:
                // Only load the map type if permit mode is enabled
                if (IsFeatureEnabled(&enabled, PERMIT_MODE_SUPPORT, &ucStatus, &ucIndex))
                {
                    if (enabled)
                        rPrintMap[j].ucPrintMapType = IMG_ID_PERMIT1_PE;
                }
                break;

            case IMG_ID_TAX_GRAPHIC:
                rPrintMap[j].ucPrintMapType = IMG_ID_TAX_GRAPHIC;
                break;

            case IMG_ID_DATE_TIME:
                rPrintMap[j].ucPrintMapType = IMG_ID_DATE_TIME;
                break;
        
            default:
                break;
        }
        rPrintMap[j].aRegionCompTypes[k++] = ucCompType;
        pbRegionMap += REGION_MAP_SIZ;
    }
}

/******************************************************************************
   FUNCTION NAME: fnCheckForComponentInPrintMap
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Check if a graphic component appears in a Print Map
   PARAMETERS   : map type, component type
   RETURN       : TRUE if component in print map, FALSE otherwise
   NOTES        : None
******************************************************************************/
BOOL fnCheckForComponentInPrintMap(uchar ucMapType, uchar ucCompType)
{
    ushort i,j;
    BOOL fReturn = FALSE;

    // find the correct map
    for(i=0; i < MAX_NUM_PRINT_MAPS; i++)
    {
        // then try to find the component type
        if(rPrintMap[i].ucPrintMapType == ucMapType)
        {
            for(j = 0;
                ((j < MAX_NUM_REGIONS_PER_AREA) && 
                 (rPrintMap[i].aRegionCompTypes[j] != TURN_OFF_IMAGE_BYTE)); 
                j++)
            {
                if(rPrintMap[i].aRegionCompTypes[j] == ucCompType)
                {
                    fReturn = TRUE;
                    break;
                }
            }
            break;
        }
    }

    return(fReturn);
}


/******************************************************************************
   FUNCTION NAME: fnLoadBarCodeIG
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : Get the info about the currently "selected" 2D-barcode, and call
                  IG_SetBarcodeType().
                : 
   PARAMETERS   : ucCompType - Component Type of the currently selected 2D-barcode.
   RETURN       : BOB_OK if graphic found, and ImageGen returned NO_ERROR, 
                  else BOB_SICK.
   NOTES        : None
//-----------------------------------------------------------------------------*/
char fnLoadBarCodeIG( UINT8 ucCompType )
{
	char    retval = BOB_SICK;
    UINT8   *pHeaderAddr = NULL_PTR;    // Address of graphic without LINK header.
    UINT8   *pDataAddr = NULL_PTR;      // Address of data after Sig
    BOOL    fStat = FALSE;              // Indicate Failure, unless we can get the graphic.
    UINT32  ulIGretval = IG_NO_ERROR;   // Error code returned from Image Generator call.

    pHeaderAddr = fnGrfxGetFirstGrfkPtrByType( ucCompType, TRUE );
    if( pHeaderAddr != NULL_PTR )
    {
        fStat = fnFlashGetGraphicMemberAddr( pHeaderAddr, 
                                             GRAPHIC_DATA_AREA_AFTER_SIG, 
                                             (void**)&pDataAddr );
    }
    if( fStat == TRUE )
    {
        // Tell the image Generator which barcode to use.
        ulIGretval = IG_SetBarcodeType( ucCompType, pHeaderAddr, pDataAddr );
        if( ulIGretval == IG_NO_ERROR )
        {
            // SUCCESS!!
            retval = BOB_OK;
        }
        else 
        {
            fnSetIGBCErr( ulIGretval );
        }
    }
    if( fStat != TRUE )
    {
        // If we could not get the graphic or its members, indicate an error.
        fnSetBobInternalErr( BOB_FLASH_GRAPHIC_ACCESS_ERROR );
    }

    return( retval );
}


/******************************************************************************
   FUNCTION NAME: fnInitGraphicBlob
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Erase the graphic Blob and initialize it's index pointers.
                : 
   PARAMETERS   : None
   RETURN       : None
   NOTES        : None
******************************************************************************/
void fnInitGraphicBlob(BOB_IG_BLOB* pBobIGBlob)
{
    memset(pBobIGBlob,0,sizeof(BOB_IG_BLOB));
}

/****************************************************************************** 
   FUNCTION NAME: fnAppendGraphicBlob                                           
   AUTHOR       : Craig DeFilippo                                               
   DESCRIPTION  : Append data to an Image Generator graphics blob.
                :                                                                   
   PARAMETERS   : field type form Reg descriptor, IG_Blob pointer, image ID type, CompID
   RETURN       : bob's disposition                                                         
   NOTES        : ucRdFldType = RD_FT_STATIC, RD_FT_VARIABLE, RD_FT_REPORT  
                : image ID Type is BY_GRAPHIC_ID (CompID = graphic id in the link header)
                :               or BY_COMP_TYPE (CompID = comp type in the component header)
                :               or BY_REPORT_ID (CompID = report id)
 HISTORY:  
  2008.05.28  Clarisa Bellamy - Mods for Graphics Module Update: 
                Add 5th argument, the Component type, since GenID alone is not enough to 
                identify a graphic.  Replace calls to old graphcis functions with calls 
                to new graphics functions. 
******************************************************************************/
char fnAppendGraphicBlob( UINT8 ucRdFldType, BOB_IG_BLOB* pBobIGBlob, UINT8 IDType,
                           UINT16 usComponentID, UINT8 ucCompType )
{
	char    retval = BOB_SICK;
    UINT8   ucNumReg;
    UINT8   *pRegGroup;
    UINT16  i=0;
    UINT16  usFnID;
    UINT16  *pUnichar;
    UINT16  usLen = 0;
    void    *pGCHeader = NULL_PTR; 
    IG_BLOB_HEADER *pBlobHeader;

/*
#ifdef BOB_TIME_DEBUG
    char            pLogBuf[50];

    sprintf(pLogBuf, "Start Bob  fnAppendGraphicBlob");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
*/
    memset(aMissingGraphic, 0, sizeof(aMissingGraphic));

    // Get a pointer to the graphics component header using the component type or graphic id as a key
    switch( IDType )
    {
        case BY_COMP_TYPE:
            pGCHeader = fnGrfxGetFirstGrfkPtrByType( ucCompType, GRFX_ANY_MATCH );
            break;

        case BY_REPORT_ID:
            pGCHeader = fnFlashGetReport( usComponentID );
            break;

        case BY_GRAPHIC_ID:
        default:
            pGCHeader = fnGrfxGetGrfkPtrByGenID( ucCompType, usComponentID, GRFX_ANY_MATCH );
            break;

    }

    // If a graphic was found...
    if( pGCHeader != NULL_PTR )
    {
        if(fnFlashGetGraphicMember(pGCHeader, NUM_OF_REG_GROUPS ,&ucNumReg))
        {
            //get a pointer to the register group arrays
            if(fnFlashGetGraphicMemberAddr(pGCHeader, REGISTERS ,(void**)&pRegGroup))
            {
                retval = BOB_OK;

                //loop through each register group record 
                for(i = 0; i<ucNumReg; i++)
                {
                    pBlobHeader = (IG_BLOB_HEADER*) &pBobIGBlob->rIGBlob.sBlobHeader[pBobIGBlob->rIGBlobIndex.usRegGrpIndex];
                    pUnichar = (ushort*) &pBobIGBlob->rIGBlob.wStrings[pBobIGBlob->rIGBlobIndex.usUniCharIndex];

                    //check the field type, ignore bit 3. IG uses it to distinguish a 1D barcode font
                    if( (*((uchar*)pRegGroup+RD_FLDTYP_OFF) & 0x07) == ucRdFldType)
                    {
                        //if the type matches the search key, fill in the blob header record
                        EndianAwareCopy(&pBlobHeader->wVariableID, (uchar*) pRegGroup+RD_VARID_OFF, SZ_VARID);
                        EndianAwareCopy(&usFnID, (uchar*) pRegGroup+RD_FNID_OFF, SZ_FNID);
                        
                        //  execute the function in the function ID, 
                        //  pass function the blob unichar pointer
                        //  have functions return unicode length written in number of unichars
                        usLen = fnCallReportTableFunction(usFnID, (ushort*) pUnichar, pBlobHeader->wVariableID);
                        
                        if( pBobIGBlob->rIGBlobIndex.usRegGrpIndex < IG_MAX_GROUPS - 1 )
                        {
                            //fill in the rest of the blob header
                            pBlobHeader->wLength = usLen;
                            pBlobHeader->wOffset = pBobIGBlob->rIGBlobIndex.usUniCharIndex;

                            //increment blob index pointers
                            pBobIGBlob->rIGBlobIndex.usRegGrpIndex++;           //now equals number of blob items
                            pBobIGBlob->rIGBlobIndex.usUniCharIndex += usLen;                           
                        }
                        else
                        {
                            retval = BOB_SICK;
                            break;
                        }
                    }
                    pRegGroup += RD_SIZE;
                }
            }
        }
    }
    else    // pGCHeader == NULL_PTR, could not locate the graphic.
    {    
        // This section here makes no sense.... (cb) 
        // If the Normal Indicia is missing, then it will still be 0.
        if( IDType == BY_COMP_TYPE )
            aMissingGraphic[0] = ucCompType;                // component type
        else if( IDType == BY_GRAPHIC_ID )
            aMissingGraphic[1] = (UINT8)usComponentID;      // Gen ID
        else if( IDType == BY_REPORT_ID )
            aMissingGraphic[2] = (UINT8)usComponentID;             // report ID
    }

/*
#ifdef BOB_TIME_DEBUG
    sprintf(pLogBuf, "End Bob  fnAppendGraphicBlob");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
*/
    return(retval);
}


/******************************************************************************
   FUNCTION NAME: fnGatherIGInitData
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Gather Image Generator initialization data.
                : Used to gather data for IG_Powerup and IG_Reload
   PARAMETERS   : ptr to number of fonts, ptr to number of region map recods,
                : ptr to FontData, ptr to ptr to Region Map (passed back)
   RETURN       : BOOL false if error, true otherwise
   NOTES        : None
******************************************************************************/
BOOL fnGatherIGInitData(uchar* pNumFonts, uchar* pNumRegionMapRecords, 
                        IG_FONT_DATA *pIGFontData, uchar **ppbRegionMap)
{
    uchar   ucNumFonts, *pFontMap;
    BOOL   fRetVal = TRUE;

    //get the number of fonts
    ucNumFonts =  fnFlashGetNumFonts();

    //get the number of region maps
    *pNumRegionMapRecords =  fnFlashGetNumRegionMapRecs();

    //get the address of the first region map
    *ppbRegionMap = (uchar*) fnFlashGetAddress(REGION_MAP_DC);

    //get the address of the font map
    pFontMap = (uchar*) fnFlashGetAddress(FONT_MAP_DC);

    //fill in the font data for all fonts
    *pNumFonts = 0;
    do
    {
        pIGFontData->bFontID[*pNumFonts] = *(pFontMap + *pNumFonts);
        if(fnJFontGetFontCompAddr((void**) &pIGFontData->pbHeaderAddr[*pNumFonts], pIGFontData->bFontID[*pNumFonts]) != SUCCESSFUL)
        {
            fRetVal = FALSE;
            break;
        }
        pIGFontData->pbDataAddr[*pNumFonts] = fnJFontGetFontBitmapAddr(pIGFontData->pbHeaderAddr[*pNumFonts]);
        (*pNumFonts)++;
    }while( (*pNumFonts < IG_MAX_FONTS) && (*pNumFonts < ucNumFonts) );

    return(fRetVal);
}

/******************************************************************************
   FUNCTION NAME: fnInitImagGen
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Initialize the Image generator module. 
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : None
******************************************************************************/
static uchar aReqGarItems[] = {IMG_ID_TOWN_CIRCLE, IMG_ID_TOWN_CIRCLE_2};

char fnInitImagGen(ushort ignore)
{
    char    retval = BOB_SICK;
    UINT8   ucNumFonts; 
    UINT8   ucNumRegionMapRecords = 0;
    UINT8   *pbRegionMap;
    UINT8   ucNumComponents = 0;
    UINT8   ucType;
    UINT8   i;
    IG_FONT_DATA* pIGFontData;
    IG_COMPONENT_DATA* pIGComponentData;
    BOOL    fResult;
    UINT8   *pLink = NULL_PTR;
    UINT8   *pNextLink = NULL_PTR;
    UINT8   *pGraphicComponent;
    UINT8   *pGraphicDataAfterSig;
    ulong   ulIGretval = IG_NO_ERROR; 

    // clear the missing graphic list
    memset(aMissingGraphic, 0, sizeof(aMissingGraphic));

    //clear the font data and component data struct
    pIGFontData = &rIGFontData;
    memset(pIGFontData,0,sizeof(IG_FONT_DATA));
    pIGComponentData = &rIGComponentData;
    memset(pIGComponentData,0,sizeof(IG_COMPONENT_DATA));

    fResult = fnGatherIGInitData(&ucNumFonts, &ucNumRegionMapRecords, 
                                        pIGFontData, &pbRegionMap);

    if(fResult)
    {
        fnInitPrintMaps(ucNumRegionMapRecords, pbRegionMap);

        // Start at the beginning of the linked list...
        pLink = fnLGLGetNextGraphLnk( NULL_PTR );
        // If there IS a linked list, check that the first entry is real.
        if( pLink )
            pNextLink = fnLGLGetNextGraphLnk( pLink );
        // scan the graphics linked list in the EMD and..
        while(   (pNextLink != NULL_PTR) 
              && (ucNumComponents < IG_MAX_COMPONENTS) ) 
        {
            // ... fill in the component data for all graphic component types
            //  that SHOULD be stored in the linked list. (IF it should be in the 
            //  Graphics File, then it will not be found here by usual routines.  
            pGraphicComponent = fnGrfxGetGraphic( pLink );
            if( fnFlashGetGCcomponentType(pGraphicComponent, &ucType) )
            {
                if( fnGrfxIsEmdCompType( ucType ) == TRUE )
                {
                    pIGComponentData->bType[ucNumComponents] = ucType;
                    pIGComponentData->pbHeaderAddr[ucNumComponents] = pGraphicComponent;
                    if( fnFlashGetGraphicMemberAddr(pGraphicComponent, GRAPHIC_DATA_AREA_AFTER_SIG, (void**)&pGraphicDataAfterSig) )
                    {
                        pIGComponentData->pbDataAddr[ucNumComponents] = pGraphicDataAfterSig;
                        ucNumComponents++;
                    }
                    else
                    {
                        fResult = FALSE;
                        break;
                    }
                }
            }
            else
            {
                fResult = FALSE;
                break;
            }
            // Check the next graphic in the list.
            pLink = pNextLink;
            pNextLink = fnLGLGetNextGraphLnk( pLink );
        }
        
        // Check the .GAR file for any grahpics of the types listed in aReqGarItems. 
        //  If found, add it to the pIGComponentData list.
        if( fResult )
        {
            for( i = 0; i < sizeof(aReqGarItems); i++ )
            {
                pGraphicComponent = fnGrfxGetFirstGrfkPtrByType( aReqGarItems[ i ], GRFX_ANY_MATCH );

                if( pGraphicComponent != NULL_PTR )      
                {
                    if( fnFlashGetGCcomponentType(pGraphicComponent, &ucType) )
                    {
                        pIGComponentData->bType[ucNumComponents] = ucType;
                        pIGComponentData->pbHeaderAddr[ucNumComponents] = pGraphicComponent;
                        if( fnFlashGetGraphicMemberAddr(pGraphicComponent, GRAPHIC_DATA_AREA_AFTER_SIG, (void**)&pGraphicDataAfterSig) )
                        {
                            pIGComponentData->pbDataAddr[ucNumComponents] = pGraphicDataAfterSig;
                            ucNumComponents++;
                        }
                        else
                        {
                            fResult = FALSE;
                            break;
                        }
                    }
                    else
                    {
                        fResult = FALSE;
                        break;
                    }
                }
            }
        }
    }

    if(fResult)
    {
        ulIGretval = IG_Powerup(ucNumFonts,ucNumRegionMapRecords,ucNumComponents, pIGFontData,pbRegionMap, pIGComponentData);
    }

    if(fResult && (ulIGretval == IG_NO_ERROR) ) 
	{ 
		fIndiciaTypeSet = FALSE;
        retval = BOB_OK;
	}
    else if(fResult && (ulIGretval != IG_NO_ERROR) )
    {  
        fnSetIGBCErr(ulIGretval);
        retval = BOB_SICK;
    }
    else if ( !fResult && (retval == BOB_CURRENCY_MISMATCH) )
    {
        fnSetBobInternalErr(retval);
        retval = BOB_SICK;
    }
    else if (!fResult)
    {
        fnSetBobInternalErr(BOB_FLASH_GRAPHIC_ACCESS_ERROR);
        retval = BOB_SICK;
    }

    return(retval);
}


/******************************************************************************
   FUNCTION NAME: fnReloadImagGen
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Initialize the Image generator module. 
   PARAMETERS   : None
   RETURN       : bob's disposition
 NOTES:
 1. WHY is this commented out?  Read this to understand (as much as I do.)
    This function is only used by the JanusTestFlash() function, which is only
    compiled in if we have TESTFLASH defined.  (Discovered by LINT, Oct. 2008)
    Former calls to it have been replaced by calls to fnInitImagGen(), because
    just calling this was apparently not adequate.
    So we are commenting it OUT for now. 
******************************************************************************/
/*
char fnReloadImagGen(ushort ignore)
{
    char    retval = BOB_SICK;
    UINT8   ucNumFonts;
    UINT8   ucNumRegionMapRecords;
    UINT8   *pbRegionMap;
    UINT8   ucNumComponents = 0;
    UINT8   ucType;
    IG_FONT_DATA* pIGFontData;
    IG_COMPONENT_DATA* pIGComponentData;
    BOOL    fResult;
    ushort  i;  
    UINT8   *pLink;
    UINT8   *pNextLink = NULL_PTR;
    UINT8   *pGraphicComponent;
    UINT8   *pGraphicDataAfterSig;
    ulong   ulIGretval = IG_NO_ERROR; 

    // clear the missing graphic list
    memset(aMissingGraphic, 0, sizeof(aMissingGraphic));

    //clear the font data and component data struct
    pIGFontData = &rIGFontData;
    memset(pIGFontData,0,sizeof(IG_FONT_DATA));
    pIGComponentData = &rIGComponentData;
    memset(pIGComponentData,0,sizeof(IG_COMPONENT_DATA));

    fResult = fnGatherIGInitData(&ucNumFonts, &ucNumRegionMapRecords, 
                                    pIGFontData, &pbRegionMap);

    if(fResult)
    {
        fnInitPrintMaps(ucNumRegionMapRecords, pbRegionMap);

        // Start at the beginning of the linked list...
        pLink = fnLGLGetNextGraphLnk( NULL_PTR );
        // If there IS a linked list, check that the first entry is real.
        if( pLink )
            pNextLink = fnLGLGetNextGraphLnk( pLink );
        // scan the graphics linked list in the EMD and..
        while(   (pNextLink != NULL_PTR) 
              && (ucNumComponents < IG_MAX_COMPONENTS) ) 
        {
            // ... fill in the component data for all graphic component types 
            //  that SHOULD be stored in the linked list. (IF it should be in the 
            //  Graphics File, then it will not be found here by usual routines.)  
            pGraphicComponent = fnGrfxGetGraphic( pLink );
            if( fnFlashGetGCcomponentType(pGraphicComponent, &ucType) )
            {
                if( fnGrfxIsEmdCompType( ucType ) == TRUE )
                {
                    pIGComponentData->bType[ucNumComponents] = ucType;
                    pIGComponentData->pbHeaderAddr[ucNumComponents] = pGraphicComponent;
                    if( fnFlashGetGraphicMemberAddr(pGraphicComponent, GRAPHIC_DATA_AREA_AFTER_SIG, (void**)&pGraphicDataAfterSig) )
                    {
                        pIGComponentData->pbDataAddr[ucNumComponents] = pGraphicDataAfterSig;
                        ucNumComponents++;
                    }
                    else
                    {
                        fResult = FALSE;
                        break;
                    }
                }
            }
            else
            {
                fResult = FALSE;
                break;
            }
            // Check the next graphic in the list.
            pLink = pNextLink;
            pNextLink = fnLGLGetNextGraphLnk( pLink );
        }
        
        // check for town circle, and misc graphic, in the GAR file
        if(fResult)
        {
            for(i = 0; i < sizeof(aReqGarItems); i++)
            {
                pGraphicComponent = fnGrfxGetFirstGrfkPtrByType( aReqGarItems[ i ], GRFX_ANY_MATCH );

                if( pGraphicComponent != NULL_PTR )      
                {
                    if( fnFlashGetGCcomponentType(pGraphicComponent, &ucType) )
                    {
                        pIGComponentData->bType[ucNumComponents] = ucType;
                        pIGComponentData->pbHeaderAddr[ucNumComponents] = pGraphicComponent;
                        if(fnFlashGetGraphicMemberAddr(pGraphicComponent, GRAPHIC_DATA_AREA_AFTER_SIG, (void**)&pGraphicDataAfterSig))
                        {
                            pIGComponentData->pbDataAddr[ucNumComponents] = pGraphicDataAfterSig;
                            ucNumComponents++;
                        }
                        else
                        {
                            fResult = FALSE;
                            break;
                        }
                    }
                    else
                    {
                        fResult = FALSE;
                        break;
                    }
                }
            }
        }
    }

    if(fResult)
        ulIGretval = IG_Reload(ucNumFonts,ucNumRegionMapRecords,ucNumComponents, pIGFontData,pbRegionMap, pIGComponentData);

    if(fResult && (ulIGretval == IG_NO_ERROR) )
        retval = BOB_OK;
    else if(fResult && (ulIGretval != IG_NO_ERROR) )
    {  
        fnSetIGBCErr(ulIGretval);
        retval = BOB_SICK;
    }
    else if (!fResult)
    {
        fnSetBobInternalErr(BOB_FLASH_GRAPHIC_ACCESS_ERROR);
        retval = BOB_SICK;
    }

    return(retval);
}
*/

/******************************************************************************
   FUNCTION NAME: fnLoadStaticVars
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Send static variables to image generator module
   PARAMETERS   : if false (0) , skip sending message to IG
   RETURN       : bob's disposition
   NOTES        : 
******************************************************************************/
char fnLoadStaticVars(ushort ignore)
{
    char retval = BOB_OK;
    ushort i;
    uchar ucIDType;
    ulong   ulIGretval; 

//    DP_IndiciaLineTextReadSem = FALSE;
//    DP_VasTextReadSem = FALSE;
//    DP_EkpOrTskNoReadSem = FALSE;
    DP_IndiciaOtherDpInfoReadSem = FALSE;

    fnInitGraphicBlob(&rStaticIGBlob);

    for(i = 0; i < rSelCompList.ucNumComponents; i++)
    {
        
        if(   (rSelCompList.ucSelCompType[i] == AD_COMP_TYPE) 
           || (rSelCompList.ucSelCompType[i] == INSCR_COMP_TYPE) 
           || (rSelCompList.ucSelCompType[i] == IMG_ID_PERMIT1_PE)
           || (rSelCompList.ucSelCompType[i] == IMG_ID_PERMIT2_PE)
           || (rSelCompList.ucSelCompType[i] == IMG_ID_PERMIT3_PE) )
        {
            // There may be more than one of each of these types.  
            //  So we need the GenID AND the CompType to identify the right graphic.
            ucIDType = BY_GRAPHIC_ID;
            retval = fnAppendGraphicBlob( RD_FT_STATIC, &rStaticIGBlob, ucIDType, 
                                          rSelCompList.usSelCompID[i], rSelCompList.ucSelCompType[i] );
        }
        else if(   (rSelCompList.ucSelCompType[i] == IMG_ID_BAR_GRAPHIC)
                || (rSelCompList.ucSelCompType[i] == IMG_ID_REDUCED_BC) )
        {
            retval = fnLoadBarCodeIG( rSelCompList.ucSelCompType[i] ); 
        }
        else 
        {
            // For these component types, just get the first graphic of that type, we
            //  don't need to know the GenId.
            ucIDType = BY_COMP_TYPE;
            retval = fnAppendGraphicBlob( RD_FT_STATIC, &rStaticIGBlob, ucIDType, 
                                          0, rSelCompList.ucSelCompType[i] );
        }

        if(retval != BOB_OK)
        {
            fnSetBobInternalErr(BOB_STATIC_IG_BLOB_ERROR);
            break;
        }
    }

    if( (retval == BOB_OK) && (ignore != FALSE) )
    {
        ulIGretval = IG_LoadStaticVCRs(&rStaticIGBlob.rIGBlob, (uchar) rStaticIGBlob.rIGBlobIndex.usRegGrpIndex & 0x0FF);
        if(ulIGretval != IG_NO_ERROR)
        {
            fnSetIGBCErr(ulIGretval);
            retval = BOB_SICK;
        }
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnAlmostLoadDynamicVars
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Send dynamic variables to image generator module
            This may be called before the actual debit, by BOB_GEN_STATIC_IMAGE_REQ.
            In that case, fPerRun will be set to PER_RUN, instead of PER_PIECE.
   PARAMETERS   : none
   RETURN       : bob's disposition
   NOTES   : Sometimes the modified barcode glob needs to be processed before
           : we format the other dynamic variables in the indicia graphic then
           : we have to make sure the barcode region appears to the right
           : of the indicia graphic.
******************************************************************************/
char fnAlmostLoadDynamicVars( BOOL fPerRun )
{
    uchar   *pBarcodeGlob = NULL_PTR;
    ulong   ulIGretval = 0;
    ushort  i;
    ushort  usBarcodeGlobLen = 0;
    char retval = BOB_OK;
    uchar   ucIDType;


    fnInitGraphicBlob(&rDynamicIGBlob);

    if(   (bUICPrintMode == BOB_DEBIT_FUNDS)
       && (fPerRun == PER_PIECE ) )    // FPHX: only do barcode stuff after debiting the vault.
    {
        //Sometimes we are sourcing the data for dynamic variables in the indicia graphic
        //  from the modified barcode glob, so it must be processed before them.  
        //  Therefore we have to scan the list once to find the barcode (because we don't
        //  want to do this if there is no barcode in this indicia) and then again to
        //  find all the other stuff.
        for(i = 0; i < rSelCompList.ucNumComponents; i++)
        {
            // If we have a 2D-barcode, we may need to modify the output from the IPSD a little.
            // fnModifyBarcodeDataGlob() will return the correct barcode data glob pointer and
            //  length whether it modifies the barcode or not.
            if( rSelCompList.ucSelCompType[i] == IMG_ID_BAR_GRAPHIC )
            {
                // For the normal barcode, use the Build Barcode Definition record 
                //  to determine (if or) how to modify the barcode data.
                usBarcodeGlobLen = fnModifyBarcodeDataGlob( &pBarcodeGlob, IPB_BUILD_BC_DEF_REC );
                break;
            }
            else if( rSelCompList.ucSelCompType[i] == IMG_ID_REDUCED_BC )
            {
                // For the reduced barcode (IBI-Lite), use the Build Barcode Definition 
                //  record to determine (if or) how to modify the barcode data.
                usBarcodeGlobLen = fnModifyBarcodeDataGlob( &pBarcodeGlob, IPB_BUILD_BC_DEF_REC_2 );
                break;
            }
        }

	    // Express meter change to create modified debit certificate regardless of the presence
	    // of the barcode in the indicium
	    if (usBarcodeGlobLen == 0 && fnFlashGetIBPNumFields(IPB_BUILD_BC_DEF_REC) > 0)
	    {
	        (void)fnModifyBarcodeDataGlob(&pBarcodeGlob, IPB_BUILD_BC_DEF_REC);
	    }
    }

    for(i = 0; i < rSelCompList.ucNumComponents; i++)
    {                
        if(   (rSelCompList.ucSelCompType[i] == AD_COMP_TYPE) 
           || (rSelCompList.ucSelCompType[i] == INSCR_COMP_TYPE)
           || (rSelCompList.ucSelCompType[i] == IMG_ID_PERMIT1_PE)
           || (rSelCompList.ucSelCompType[i] == IMG_ID_PERMIT2_PE)
           || (rSelCompList.ucSelCompType[i] == IMG_ID_PERMIT3_PE) )
        {
            ucIDType = BY_GRAPHIC_ID;
            retval = fnAppendGraphicBlob( RD_FT_VARIABLE, &rDynamicIGBlob, ucIDType, 
                                          rSelCompList.usSelCompID[i], rSelCompList.ucSelCompType[i] );
        }
        else if(   (rSelCompList.ucSelCompType[i] == IMG_ID_BAR_GRAPHIC)
                || (rSelCompList.ucSelCompType[i] == IMG_ID_REDUCED_BC) )
        {
            // Everything should have been taken care of above or in fnLoadStaticVars.
            ; 
        }
        else
        {
            // For these component types, just get the first graphic of that type, we
            //  don't need to know the GenId.
            ucIDType = BY_COMP_TYPE;
            retval = fnAppendGraphicBlob( RD_FT_VARIABLE, &rDynamicIGBlob, ucIDType, 
                                          0, rSelCompList.ucSelCompType[i]);
        }

        if(retval != BOB_OK)
        {
            fnSetBobInternalErr(BOB_DYNAMIC_IG_BLOB_ERROR);
            break;
        }
    }

    if(retval == BOB_OK)
    {
		if (pBarcodeGlob)
		{
			fnDumpStringToSystemLog("----- 2D Barcode Data");
			pBarcodeGlob[usBarcodeGlobLen] = 0;
			fnDumpStringToSystemLog((char *)pBarcodeGlob);

			if (fnGetLocalLoggingState())
				fnDumpRawLenToSystemLog((char *)pBarcodeGlob, usBarcodeGlobLen);
		}

        switch( bUICPrintMode )
        {
            case BOB_DEBIT_FUNDS:    
            case BOB_PERMIT_MODE:
            case BOB_DATE_STAMP_MODE:
            case BOB_DATE_STAMP_ONLY_MODE: 
            case BOB_TAX_MODE:
                if( fPerRun == PER_PIECE )
                {
                    // This loads any VCRs that are different from the ones that are currently loaded, or have not been loaded yet.
			        ulIGretval = IG_LoadVariableVCRsAndBCDataLen(pBarcodeGlob, usBarcodeGlobLen, &rDynamicIGBlob.rIGBlob, 
																 (uchar)rDynamicIGBlob.rIGBlobIndex.usRegGrpIndex & 0x0FF);
                }
                //lint -fallthrough
            case  BOB_AD_ONLY_MODE:         // Also used for Text Ads, which has some PER_RUN variable VCRs.
                if( fPerRun == PER_RUN )
                {
                    // This only loads the VCRs that are not likely to change per piece and can be found someplace other than the barcode data.
                    ulIGretval = IG_LoadOnlyVariableVCRs( NULL_PTR, &rDynamicIGBlob.rIGBlob, 
                                                          (UINT8)(rDynamicIGBlob.rIGBlobIndex.usRegGrpIndex & 0x0FF) );
                }

                if( ulIGretval != IG_NO_ERROR )
                {
                    fnSetIGBCErr(ulIGretval);
                    retval = BOB_SICK;
                }
                break;
        
            default:
            	break;
        }
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnLoadDynamicVars
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Send dynamic variables to image generator module
   PARAMETERS   : none
   RETURN       : bob's disposition
   NOTES        : None
******************************************************************************/
char fnLoadDynamicVars( ushort ignore )
{
    char retval = BOB_OK;
    
    // A slightly different subroutine is used if this is a per piece or a per run call.
    //  The per run call does not have access to barcode parameters yet. 
    retval = fnAlmostLoadDynamicVars( PER_PIECE );

    return( retval );
}

/******************************************************************************
   FUNCTION NAME: fnLoadKindofDynamicVars
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Send dynamic variables to image generator module
   PARAMETERS   : none
   RETURN       : bob's disposition
   NOTES        : None
******************************************************************************/
char fnLoadKindofDynamicVars( ushort ignore )
{
    char retval = BOB_OK;
    
    // A slightly different subroutine is used if this is a per piece or a per run call.
    //  The per run call does not have access to barcode parameters yet. 
    retval = fnAlmostLoadDynamicVars( PER_RUN );

    return( retval );
}

/******************************************************************************
   FUNCTION NAME: fnBobClearBC
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : Clears the IG_Barcode region so that it does not have to be done
                    during the debit cycle.
   PARAMETERS   : none
   RETURN       : bob's disposition
   NOTES        : None
******************************************************************************/
char fnBobClearBC( ushort ignore )
{
    char    retval = BOB_OK;
    ulong   ulIGretval;
    
  
    // ONLY clear the barcode region if we are in BOB_DEBIT_FUNDS mode.
    if( bUICPrintMode == BOB_DEBIT_FUNDS )
    {
        // Bob does this ahead of time, so the debit cycle is faster. 
        ulIGretval = IG_ClearBarcode();
        if( ulIGretval != IG_NO_ERROR )
        {
            fnSetIGBCErr( ulIGretval );
            retval = BOB_SICK;
        }
    }
        
    return( retval );
}


/******************************************************************************
   FUNCTION NAME: fnBobNextReportPage
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : If we are printing a multi-page report, this advances the page
                    number and reloads the "dynamic" information for the new page.
                    The functions in creport.c, which populate the report fields, 
                    use their variable wCurrentPageNumber, which is set by 
                    fnSetCurrentReportPageNbr.
   PARAMETERS   : none
   RETURN       : bob's disposition
   NOTES        : None
******************************************************************************/
char fnBobNextReportPage( ushort ignore )
{
    char    retval = BOB_OK;

#ifdef BOB_TIME_DEBUG
    char            pLogBuf[50];

    sprintf(pLogBuf, "Start Bob  fnBobNextReportPage");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
    
    // ONLY clear the barcode region if we are in BOB_DEBIT_FUNDS mode.
    if( bUICPrintMode  == BOB_REPORT_MODE )
    {
        // Do we need to set up for the next page of a report?
        if(    (bobsCommand.bMsgId == SCRIPT_PRINT_COMPLETE)
            && ((ushort)ucBobsReportPageNbr < usBobsTotalPages) )
        {
            fnSetCurrentReportPageNbr( ++ucBobsReportPageNbr );
//            retval = fnLoadReport( 0 );
            retval = fnLoadNextPageReport( 0 );
        }                
    }
#ifdef BOB_TIME_DEBUG
    sprintf(pLogBuf, "End Bob  fnBobNextReportPage");
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
#endif
        
    return( retval );
}

/******************************************************************************
   FUNCTION NAME: fnBobPrintComplete
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : Whatever we can do now for the next print, do it.
                In Debit Mode, clear the barcode date.
                In Report mode, load the next page.
   PARAMETERS   : none
   RETURN       : bob's disposition
   NOTES        : None
******************************************************************************/
char fnBobPrintComplete( ushort ignore )
{
    char    retval = BOB_OK;
    
    // If we are in BOB_DEBIT_FUNDS mode, clear the BarCode ahead of time, so the 
    //  debit cycle if faster.
    retval = fnBobClearBC( ignore );

    if( retval == BOB_OK ) 
    {
        // If we are in a multi-page report, load up the VCRs for the next 
        //  page of the report.
        retval = fnBobNextReportPage( ignore );
    }

    return( retval );
}

/******************************************************************************
   FUNCTION NAME: fnLoadNextPageReport
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : Send VCR report variables to image generator module
   PARAMETERS   : none
   RETURN       : bob's disposition
   NOTES        : Next page used to just call fnLoadReport again.  That took
                too much time for FPHX.
******************************************************************************/
char fnLoadNextPageReport( ushort ignore )
{
    char    retval = BOB_OK;
    ulong   ulIGretval;


    fnInitGraphicBlob( &rDynamicIGBlob );

    // When getting a report graphic, only the currentRptSelection is used.  
    //  This is actually a 16-bit version of the compType, so it is passed as the GenID arg. 
    if( fnAppendGraphicBlob( RD_FT_REPORT, &rDynamicIGBlob, BY_REPORT_ID, 
                             currentRptSelection, 0 ) != BOB_OK )
    {
        fnSetBobInternalErr( BOB_REPORT_IG_BLOB_ERROR );
        retval = BOB_SICK;
    }   

    if( retval == BOB_OK )
    {
        ulIGretval = IG_LoadNextReportPage( &rDynamicIGBlob.rIGBlob, 
                                            (uchar)(rDynamicIGBlob.rIGBlobIndex.usRegGrpIndex & 0x0FF)
                                           );

        if( ulIGretval != IG_NO_ERROR )
        {
            fnSetIGBCErr( ulIGretval );
            retval = BOB_SICK;
        }
        else if( !fnCheckIGImageParams() )
        {
            fnSetBobInternalErr( BOB_REPORT_NOT_FOUND );
            retval = BOB_SICK;          
        }
    }

    return( retval );
}



/******************************************************************************
   FUNCTION NAME: fnLoadReport
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Send report variables to image generator module
   PARAMETERS   : none
   RETURN       : bob's disposition
   NOTES        : None
******************************************************************************/
char fnLoadReport(ushort ignore)
{
	char   retval = BOB_OK;
    UINT8   *pbHeaderAddr = NULL_PTR;
    UINT8   *pbDataAddr = NULL_PTR;
    UINT32   ulIGretval;

    fnInitGraphicBlob( &rDynamicIGBlob );

    // When getting a report graphic, only the currentRptSelection is used.  
    //  This is actually a 16-bit version of the compType, so it is passed as the GenID arg. 
    if( fnAppendGraphicBlob( RD_FT_REPORT, &rDynamicIGBlob, BY_REPORT_ID, 
                             currentRptSelection, 0 ) != BOB_OK )
    {
        fnSetBobInternalErr(BOB_REPORT_IG_BLOB_ERROR);
        retval = BOB_SICK;
    }   

    if(retval == BOB_OK)
    {
        pbHeaderAddr = fnFlashGetReport(currentRptSelection);
        if( pbHeaderAddr == NULL_PTR )
        {
            // This report does not exist in the EMD:
            fnSetBobInternalErr( BOB_REPORT_NOT_FOUND );
            retval = BOB_SICK;
        }
        else
        {
	        (void)fnFlashGetGraphicMemberAddr( pbHeaderAddr, GRAPHIC_DATA_AREA_AFTER_SIG, (void**)&pbDataAddr );

	        ulIGretval = IG_LoadReport( (uchar**)&rCMReply.pImage, &rCMReply.wImageLength, &rCMReply.wMinPrintLength,
	                                &rDynamicIGBlob.rIGBlob, (uchar) rDynamicIGBlob.rIGBlobIndex.usRegGrpIndex & 0x0FF,                             
	                                pbHeaderAddr, pbDataAddr);

	        if(ulIGretval != IG_NO_ERROR)
	        {
	            fnSetIGBCErr(ulIGretval);
	            retval = BOB_SICK;
	        }
	        else if(!fnCheckIGImageParams())
	        {
	            fnSetBobInternalErr(BOB_REPORT_NOT_FOUND);
	            retval = BOB_SICK;          
	        }
        }
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnSwitchReportScript
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Redirect to appropriate script for current report selection
   PARAMETERS   : none
   RETURN       : bob's disposition
   NOTES        : None
******************************************************************************/
UINT16 fnSwitchReportScript( UINT16 ignore )
{
    UINT16 retval = DUMMY_SCRIPT;

    if( currentRptSelection < fnFlashGetNumReports() )
    {
        if( fnFlashGetReport(currentRptSelection) != NULL_PTR )
        {
            retval = BobsReportScriptLookup[currentRptSelection].bVarID;
        }
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnLoadTestPrint
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Send test print info to image generator module
   PARAMETERS   : none
   RETURN       : bob's disposition
   NOTES        : None
******************************************************************************/
char fnLoadTestPrint(ushort ignore)
{
	char   retval = BOB_OK;
    UINT16  wNumTestPrints;             // Number of test prints in the EMD Linked Graphic List.
    UINT8   *pbHeaderAddr = NULL_PTR;
    UINT8   *pbDataAddr = NULL_PTR;
    ulong   ulIGretval;

    fnInitGraphicBlob(&rDynamicIGBlob);

    wNumTestPrints = fnGrfxGetNumberFltrd( IMG_ID_TEST_PRINT, GRFX_ANY_FLAGS );
    if(   (wNumTestPrints >= 1)
       && (    (currentTestPrintSelection == 0)
            || (currentTestPrintSelection == TURN_OFF_IMAGE) ) )
    {
        // If we only have one test print image, or the user hasn't selected one, just select the
        //  first one we can find in the EMD.
        currentTestPrintSelection = fnGrfxGetGenIdOfFirstByType( IMG_ID_TEST_PRINT, GRFX_ANY_FLAGS );
    }
    // else ... Otherwise the OI has set the selection (GenID) using a call to fnWriteData( SET_CURRENT_TEST_PRINT )
    
    // Get a pointer to the selected test print, WITHOUT the link header.
    if( currentTestPrintSelection > 0 )
    {
        pbHeaderAddr = fnGrfxGetGrfkPtrByGenID( IMG_ID_TEST_PRINT, currentTestPrintSelection, GRFX_ANY_FLAGS );
    }

    // If the "selected" test print doesn't exist, get the first one that does.
    if( pbHeaderAddr == NULL_PTR )
    {
        pbHeaderAddr = fnGrfxGetFirstGrfkPtrByType( IMG_ID_TEST_PRINT, GRFX_ANY_FLAGS );
    }

    // If we found a test print...
    if( pbHeaderAddr != NULL_PTR )
    {
        (void)fnFlashGetGraphicMemberAddr( pbHeaderAddr, GRAPHIC_DATA_AREA_AFTER_SIG, (void**)&pbDataAddr );

        ulIGretval =  IG_LoadTestPrint( (uchar**)&rCMReply.pImage, &rCMReply.wImageLength, &rCMReply.wMinPrintLength,
                                        pbHeaderAddr, pbDataAddr);
        if(ulIGretval != IG_NO_ERROR)
        {
            fnSetIGBCErr(ulIGretval);
            retval = BOB_SICK;
        }
        else if(!fnCheckIGImageParams())
        {
            fnSetBobInternalErr(BOB_TEST_PRINT_NOT_FOUND);
            retval = BOB_SICK;          
        }

    }
    else
    {
        fnSetBobInternalErr(BOB_TEST_PRINT_NOT_FOUND);
        retval = BOB_SICK;
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnLoadIndicia
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Tell the IG to load the indicia and the static vars
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnLoadIndicia(ushort ignore)
{
    char retval = BOB_OK;
    ulong   ulIGretval;

    // Attempt to load the GTSBarcode Data buffers before we load static data.
    // Not sure this is really needed here, and this is why...
    //   1. It is called as part of the debit script now, so that the data is
    //      available before the LoadDynamic stuff is done.  
    //   2. If the data is needed for the precreate, then the specific function 
    //      that gets the precreate data should call fnGetVasBarcodes(). 
    //  Here, though, we DONT do the filtering that fnGetVasBarcodes() does before
    //   it calls the GTS routine.
    retval = fnLoadStaticVars(FALSE);

    if(retval == BOB_OK)
    {
        ulIGretval = IG_LoadIndicia((uchar**)&rCMReply.pImage, &rCMReply.wImageLength, &rCMReply.wMinPrintLength,
                    &rStaticIGBlob.rIGBlob, (uchar) rStaticIGBlob.rIGBlobIndex.usRegGrpIndex & 0x0FF);
        
        if(ulIGretval == IG_TOO_MANY_COLUMNS)
        {
            ucStaticImageStatus = BOB_STATIC_IMAGE_TOO_MANY_COLUMNS;
            retval = BOB_SKIP_ALL;
        }
        else if(ulIGretval != IG_NO_ERROR)
        {
            fnSetIGBCErr(ulIGretval);
            retval = BOB_SICK;
        }
        else if(!fnCheckIGImageParams())
        {
            fnSetBobInternalErr(BOB_INDICIA_NOT_FOUND);
            retval = BOB_SICK;          
        }

    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnCheckIndiciaCurrency
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Check if Indicia currency type is the same as psd currency type
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnCheckIndiciaCurrency(ushort ignore)
{
    void* pGraphicComponent = NULL_PTR;
    UINT8 ucCurType;
    char retval = BOB_OK;
    UINT8 ucIndiaType = IMG_ID_NORMAL_INDICIA;

#ifdef BOB_CHECK_CURRENCY
    if(fnCheckForComponentInPrintMap(IMG_ID_NORMAL_INDICIA, IMG_ID_NORMAL_INDICIA))
        ucIndiaType = IMG_ID_NORMAL_INDICIA;
    else if(fnCheckForComponentInPrintMap(IMG_ID_NORMAL_INDICIA, IMG_BAR_LEADING_IMAGE))
        ucIndiaType = IMG_BAR_LEADING_IMAGE;
    else if(fnCheckForComponentInPrintMap(IMG_ID_NORMAL_INDICIA, IMG_BAR_LAGGING_IMAGE))
        ucIndiaType = IMG_BAR_LAGGING_IMAGE;
    else if(fnCheckForComponentInPrintMap(IMG_ID_NORMAL_INDICIA, IMG_ID_INTERNAT_LEAD))
        ucIndiaType = IMG_ID_INTERNAT_LEAD;
    else if(fnCheckForComponentInPrintMap(IMG_ID_NORMAL_INDICIA, IMG_ID_INTERNAT_LAG))
        ucIndiaType = IMG_ID_INTERNAT_LAG;
    
    pGraphicComponent = fnGrfxGetFirstGrfkPtrByType( ucIndiaType, GRFX_ANY_MATCH );
    if( pGraphicComponent != NULL_PTR )
    {
        if(fnFlashGetGraphicMember(pGraphicComponent, CURRENCYTYPE ,&ucCurType))
        {
            if(bIPSD_currencyCode != ucCurType)
            {
                fnSetBobInternalErr(BOB_CURRENCY_MISMATCH);
                retval = BOB_SICK;
            }
        }
    }

#endif
    return(retval);
}


/******************************************************************************
   FUNCTION NAME: fnSetIGIndiciaType
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Set the Indicia type 
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : CompType is Normal or Low Value
******************************************************************************/
char fnSetIGIndiciaType(ushort ignore)
{
    char retval = BOB_OK;
    uchar ucNumComponents = 0, *pGraphicDataAfterSig;
    ushort i;
    BOOL    fIType = TRUE, fBreak;
    IG_COMPONENT_DATA* pIGComponentData;
    uchar   *pGraphicComponent;
    ulong   ulIGretval;

    pIGComponentData = &rIGComponentData;
    memset(pIGComponentData,0,sizeof(IG_COMPONENT_DATA));

    // Note: only those components relevent to the current print should be in the
    //       selected component list by the time we  get here.
    for(i = 0; i < rSelCompList.ucNumComponents; i++)
    {
        fBreak = FALSE;
        
        switch(rSelCompList.ucSelCompType[i])
        {
            case IMG_ID_LOW_INDICIA:
                fIType = FALSE;
                //lint -fallthrough
            case IMG_ID_NORMAL_INDICIA:
            case IMG_ID_CORRECTION_INDI:
            case IMG_ID_REDATE_INDICIA:
            case IMG_BAR_LEADING_IMAGE:
            case IMG_BAR_LAGGING_IMAGE:
            case IMG_ID_INTERNAT_LEAD:
            case IMG_ID_INTERNAT_LAG:
            case IMG_ID_LITE_INDICIA_IMAGE:
            case IMG_ID_EXTRA_LITE_INDICIA_IMAGE:
            {
                pGraphicComponent = fnGrfxGetFirstGrfkPtrByType( rSelCompList.ucSelCompType[i], GRFX_ANY_MATCH);
                if( pGraphicComponent != NULL_PTR )
                {
                    pIGComponentData->bType[ucNumComponents] = rSelCompList.ucSelCompType[i];
                    pIGComponentData->pbHeaderAddr[ucNumComponents] = pGraphicComponent;
                    if(fnFlashGetGraphicMemberAddr(pGraphicComponent, GRAPHIC_DATA_AREA_AFTER_SIG, (void**)&pGraphicDataAfterSig))
                    {
                        pIGComponentData->pbDataAddr[ucNumComponents] = pGraphicDataAfterSig;
                        if(++ucNumComponents >= IG_MAX_COMPONENTS)
                            fBreak = TRUE;
                    }
                }
                else
                {
                    fnSetBobInternalErr(BOB_INDICIA_NOT_FOUND);
                    retval = BOB_SICK;
                    fBreak = TRUE;
                }
            }
                break;

            default:
                break;       
        }

        if(fBreak)
            break;
    }
    
    if(retval == BOB_OK)
    {
        ulIGretval = IG_SetIndiciaType(fIType, ucNumComponents, pIGComponentData);
        
        if(ulIGretval != IG_NO_ERROR)
        {
            fnSetIGBCErr(ulIGretval);
            retval = BOB_SICK;
        }
        else
        {
            fIndiciaTypeSet = TRUE;
        }
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnReLoadAdInscOnly
   AUTHOR       : Sam Thillaikumaran
   DESCRIPTION  : Tell the IG to reload the ad and insc only
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnReLoadAdInscOnly(ushort ignore)
{
    char retval = BOB_OK;
    ulong   ulIGretval;
    
    retval = fnLoadStaticVars(FALSE);       //ignore = FALSE means skip sending IG_LoadStaticVCRs_
    if(retval == BOB_OK)
    {
        ulIGretval = IG_LoadAdInscr((uchar**)&rCMReply.pImage, &rCMReply.wImageLength, &rCMReply.wMinPrintLength,
                    &rStaticIGBlob.rIGBlob, (uchar) rStaticIGBlob.rIGBlobIndex.usRegGrpIndex & 0x0FF);
        
        if(ulIGretval == IG_TOO_MANY_COLUMNS)
        {
            ucStaticImageStatus = BOB_STATIC_IMAGE_TOO_MANY_COLUMNS;
            retval = BOB_SKIP_ALL;
        }
        if(ulIGretval != IG_NO_ERROR)
        {
            fnSetIGBCErr(ulIGretval);
            retval = BOB_SICK;
        }
        else if(!fnCheckIGImageParams())
        {
            fnSetBobInternalErr(BOB_INDICIA_NOT_FOUND);
            retval = BOB_SICK;          
        }
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnLoadAdInscOnly
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Tell the IG to load the ad and insc only
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnLoadAdInscOnly(ushort ignore)
{
    char retval = BOB_OK;
    ulong   ulIGretval;
    ulong ulIGretval1 = IG_NO_ERROR;
    uchar ucTextEntryDuckState = FALSE;
    BOOL  fReloadAdInsc = FALSE;
    
    retval = fnLoadStaticVars(FALSE);       //ignore = FALSE means skip sending IG_LoadStaticVCRs_
    if(retval == BOB_OK)
    {
        ulIGretval = IG_LoadAdInscr((uchar**)&rCMReply.pImage, &rCMReply.wImageLength, &rCMReply.wMinPrintLength,
                    &rStaticIGBlob.rIGBlob, (uchar) rStaticIGBlob.rIGBlobIndex.usRegGrpIndex & 0x0FF);
        
        if(ulIGretval == IG_TOO_MANY_COLUMNS)
        {
            ucStaticImageStatus = BOB_STATIC_IMAGE_TOO_MANY_COLUMNS;
            retval = BOB_SKIP_ALL;
        }
        if(ulIGretval != IG_NO_ERROR)
        {
            fnSetIGBCErr(ulIGretval);
            retval = BOB_SICK;
        }
        else if(!fnCheckIGImageParams())
        {
            fnSetBobInternalErr(BOB_INDICIA_NOT_FOUND);
            retval = BOB_SICK;          
        }

    }

    // if text entry is in the current print map
    if (retval == BOB_OK && fnCheckForComponentInPrintMap(IMG_ID_NORMAL_INDICIA, IMG_ID_TEXT_ENTRY) ) 
    {
        // if text messages are not allowed in ad only mode for this PCN, then duck the image
        // if the current text message selection is NONE, then duck the image
        if ((!fnFlashGetByteParm(BP_TXTMSG_ALLOWED_IN_AD_ONLY)) ||
        	(currentTextEntrySelection == TURN_OFF_IMAGE))
        {
            ucTextEntryDuckState = TRUE;
        }
			
        fReloadAdInsc = TRUE;
        ulIGretval1 = IG_DuckTextEntry(ucTextEntryDuckState);       
    }

    if(ulIGretval1 !=  IG_NO_ERROR)
    {
        fnSetIGBCErr(ulIGretval1);
        retval = BOB_SICK;
    }

    if( (retval == BOB_OK) &&  fReloadAdInsc )  
    {
        retval = fnReLoadAdInscOnly(0);
    }

    return(retval);
}


//**************************************************************************
//  FUNCTION NAME:  
//       fnCheckIfVASDisablesRegion
//  DESCRIPTION:
//       Check the EMD parameter and the rates data to determine if 
//       Ads, Inscs, or Text msgs are allowed.
//  INPUTS:
//       Component (Image) type = 
//       IMG_ID_AD_SLOGAN, or IMG_ID_TEXT_ENTRY, or IMG_ID_INSCRIPTION
//  RETURNS:
//       TRUE if compType is disabled for VAS classes, FALSE otherwise
//  NOTES:          
//       none
//  MODIFICATION HISTORY:
//  07-10-06    Dicky Sun         Update for supporting new return type of 
//                                fnRateGetProdConfigListInfo.
//              Craig DeFilippo   Initial version
//**************************************************************************
BOOL fnCheckIfVASDisablesRegion(uchar ucCompType)
{
    BOOL fRet = FALSE;

    return(fRet);
}

/******************************************************************************
   FUNCTION NAME: fnSetIGText
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Tell the image generator to duck Text if no text is selected
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : Only required in certain circumstances, like when not all 
                : images in a region map can be printed at the same time, we
                : might have to inform the IG to duck the Text message before
                : we call fnLoadIndicia. This function is intended to be called
                : on the fly after the OI calls BOB to set a text id.
******************************************************************************/
char fnSetIGText(ushort ignore)
{
    char retval = BOB_OK;
    ulong ulIGretval;

    if( currentTextEntrySelection == TURN_OFF_IMAGE )
    {
        ulIGretval = IG_DuckTextEntry(TRUE);

        if( ulIGretval != IG_NO_ERROR )
        {
            fnSetIGBCErr(ulIGretval);
            retval = BOB_SICK;
        }   
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnSetIGAd
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Tell the image generator to set the current ad 
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnSetIGAd(ushort ignore)
{
    char retval = BOB_OK;
    uchar *pGraphicDataAfterSig = NULL_PTR;
    uchar *pGraphicComponent = NULL_PTR;
    ushort usIgAdId = SET_NO_AD;
    ulong ulIGretval;
    BOOL  fSetAd = FALSE;
#ifdef K700
    ulong ulIGretval1 = IG_NO_ERROR;
#endif

    if(currentAdSelection == TURN_OFF_IMAGE)
    {
        usIgAdId = SET_NO_AD;
        fSetAd = TRUE;  
    }
    // check if VAS service selection diasables ad region
    // or if datetime only mode is selected
    else if( ((bUICPrintMode == BOB_DEBIT_FUNDS) && fnCheckIfVASDisablesRegion(IMG_ID_AD_SLOGAN)) ||
             (bUICPrintMode == BOB_DATE_STAMP_ONLY_MODE) )
    {
        usIgAdId = SET_NO_AD;
        fSetAd = TRUE;  
        currentAdSelection = TURN_OFF_IMAGE;
    }
    else
    {
        pGraphicComponent = fnGrfxGetGrfkPtrByGenID( IMG_ID_AD_SLOGAN, currentAdSelection, GRFX_ANY_MATCH );
        if(   (pGraphicComponent != NULL_PTR) 
           && fnFlashGetGraphicMemberAddr(pGraphicComponent, GRAPHIC_DATA_AREA_AFTER_SIG ,(void**)&pGraphicDataAfterSig) )
        {
            usIgAdId = currentAdSelection;
            fSetAd = TRUE;
        }
        else
        {
            fnSetBobInternalErr(BOB_AD_ACCESS_ERROR);
            retval = BOB_SICK;
        }
#ifdef K700
        //In K700, Text message entry and AD cannot be printed together.
        //So if the Ad is enabled turn off the text message entry.
        if(retval == BOB_OK)
        {
            // if text entry is in the current print map
            if( fnCheckForComponentInPrintMap(IMG_ID_NORMAL_INDICIA, IMG_ID_TEXT_ENTRY) ) 
            {
                currentTextEntrySelection = TURN_OFF_IMAGE;
                ulIGretval1 = IG_DuckTextEntry(TRUE);
            }
            if(ulIGretval1 !=  IG_NO_ERROR)
            {
                fnSetIGBCErr(ulIGretval1);
                retval = BOB_SICK;
            }
        }
#endif
    }

    if(fSetAd)
    {
        ulIGretval = IG_SetAd( (UINT8)usIgAdId, pGraphicComponent, pGraphicDataAfterSig );
        if(ulIGretval !=  IG_NO_ERROR)
        {
            fnSetIGBCErr(ulIGretval);
            retval = BOB_SICK;
        }
        // Udate Ad Slogan Register information
        //TODO JAH remove asrapi.c fnASRFindASRLogEntry(usIgAdId);
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnSetIGAdNone
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Tell the image generator to set no ad
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnSetIGAdNone(ushort ignore)
{
    char retval = BOB_OK;
    uchar   *pGraphicDataAfterSig = NULL_PTR;
    uchar   *pGraphicComponent = NULL_PTR;
    ulong ulIGretval;

    ulIGretval = IG_SetAd(SET_NO_AD, pGraphicComponent, pGraphicDataAfterSig);
    if(ulIGretval !=  IG_NO_ERROR)
    {
        fnSetIGBCErr(ulIGretval);
        retval = BOB_SICK;
    }   
    // Udate Ad Slogan Register information
    //TODO JAH remove asrapi.c fnASRFindASRLogEntry(SET_NO_AD);

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnGetAutoADIDViaRateInfo
   AUTHOR       : Raymond Shen
   DESCRIPTION  : Get the auto-AD name from the indicia string 
                : returned from the data capture engine for the currently selected 
                : rate class, then look up its graphic ID 
   PARAMETERS   : Ptr to the graphic ID
   RETURN       : passback the the graphic ID of the inscription in usGraphicID 
                : BOB_OK if auto inscription found or none is specified or
                : BOB_AUTO_AD_NOT_FOUND if rules indicate a graphic is
                :     required but the graphic could not be found
                : BOB_AUTO_AD_OUT_OF_RANGE if rules indicate a graphic
                :     name that is too long
   NOTES        : 
 HISTORY:  
  2010.08.03 Clarisa Bellamy - An empty string indicates no auto ad, same as "NO_AD_SLOGAN".
                                Update comments.  Use macro for string index. 
  2010.07.30 Clarisa Bellamy - Only look at the Ad string if the NumIndiciaStrings 
                            is large enough to have an ad string.
******************************************************************************/
char fnGetAutoAdIDViaRateInfo(ushort *ptrGraphicID)
{
//    unichar ADName[MAX_INSCR_NAME+1] = {0x00, 0x00};
//    uchar ucLen = 0;
    char retVal = BOB_OK;
//    const struct ind_output *pDCInfo;
//    SINT16 sRet;
//    const unichar NoADValue[] = { 'N','O','_','A','D','_','S','L','O','G','A','N','\0' };

    // Default to no auto ad.
    *ptrGraphicID = 0;  

//TODO RAM remove rates    sRet = fnRateGetIndiciaInfo( &pDCInfo );
    
    // If there is an Auto Ad, it's uniname will be in the 4th Indicia_HRT entry.
#if 0 //TODO remove me?
    if( pDCInfo && sRet ==  RSSTS_NO_ERROR )
    {
        if( pDCInfo->NumIndiciaStrings > DCI_HRT_AUTO_AD_IDX )
        {
            // If we have an Auto Ad string, check if it is an empty string or not.
            ucLen = (UINT8)unistrlen( pDCInfo->Indicia_HRT[DCI_HRT_AUTO_AD_IDX].String );

            if( (ucLen > 0) && (ucLen <= MAX_INSCR_NAME) )
            {
                // Copy the unistring name from the DCAP/rates data to our local buffer.
                (void)unistrcpy( ADName, pDCInfo->Indicia_HRT[DCI_HRT_AUTO_AD_IDX].String );
            
                // If the unistring is not empty, check if it is "NO_AD_SLOGAN"
                if( !unistrcmp( NoADValue, ADName ) )
                {
                    retVal = BOB_OK;
                }
                else
                {
                    // Find ad with matching uniname.
                    *ptrGraphicID = fnGrfxGetGraphicIdByTypeAndName( IMG_ID_AD_SLOGAN, ADName );

                    // If ad can't be found with this name, return error
                    if( *ptrGraphicID == 0 )
                    {
                        retVal = BOB_AUTO_AD_NOT_FOUND;
                    }
                }
            }
            else if( ucLen == 0 )
            {
                // In 3rd Gen, an empty string indicates no auto ad.
                retVal = BOB_OK;
            }
            else
            {
                // If specified name is too long, return error
                retVal = BOB_AUTO_AD_OUT_OF_RANGE;
            }
        }
    }
#endif
    return(retVal); 
}

/******************************************************************************
   FUNCTION NAME: fnDetermineAdSelection
   AUTHOR       : Raymond Shen
   DESCRIPTION  : Figure out which Ad selection is in use, the user's
                : selection or the auto selection.
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnDetermineAdSelection(ushort ignore)
{
    char    retval = BOB_OK;
    BOOL    fEnabled = FALSE;
    uchar   ucStatus, ucIndex;
    char    autoAdState;          // auto Ad requirement per pcn parameter
    ushort  usAutoAdID = 0;       // Ad feature code

    // by default...
    currentAutoAdSelection = TURN_OFF_IMAGE;
    currentAdSelection = currentUserAdSelection;                  

    // if auto Ad feature is on and auto Ad EMD parm is on...
    autoAdState = fnFlashGetAutoAdType();
    if(IsFeatureEnabled(&fEnabled, AUTO_AD_SUPPORT, &ucStatus, &ucIndex) && fEnabled && autoAdState > 0)
    {
        switch (autoAdState)
        {
           // Auto Ad Based on Data Capture Rule Information
           // If Auto Ad is Enabled, see if one is required for this class
           // Since FPHX uses 3rd Gen Dcap, it will never come to this place.
           case AUTO_AD_VIA_DCAP:
           case FULL_AUTO_AD_VIA_DCAP:
               break;

           // Auto Ad Based on Rating PCL Information
           case AUTO_AD_VIA_RATES:
           case FULL_AUTO_AD_VIA_RATES:
                retval = fnGetAutoAdIDViaRateInfo( &usAutoAdID );
                break;

           // Auto Ad not supported
           default:
           case NO_AUTO_AD:
           break;
        }
    
        if (retval == BOB_OK)
        {   
            // if an auto Ad is defined for the current rating configuration
            // use it and clear the user's selection.  We clear the user selection
            // because when the rating configuration changes to one that does not
            // dictate an auto Ad we want the user to have to explicitly
            // start with no Ad and have to select an Ad again. 
            if (usAutoAdID > 0)
            {
                currentAutoAdSelection = usAutoAdID;
                currentAdSelection = currentAutoAdSelection;
                currentUserAdSelection = TURN_OFF_IMAGE;
            }
            // usAutoInscID == 0 when no auto Ad is specified
            else if(0 == usAutoAdID)
            {
                // if auto Ad feature is on and full auto Ad EMD parm is on...
                // when DCM doesn't have an auto Ad for a selected product, print with no Ad 
                if( (FULL_AUTO_AD_VIA_DCAP == autoAdState) || 
                    (FULL_AUTO_AD_VIA_RATES == autoAdState) )
                {      
                    currentAutoAdSelection = TURN_OFF_IMAGE;
                    currentUserAdSelection = TURN_OFF_IMAGE;
                    currentAdSelection = currentUserAdSelection;
                }
            }
        }
        else
        {
            //required Ad not found or DCAP rules indicate an invalid Ad required for this class
            fnSetBobInternalErr(retval);
            retval = BOB_SICK;
        }
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnGetAutoInscrIDViaRateInfo
   AUTHOR       : Tom Dometios
   DESCRIPTION  : Get the auto-inscripition name from the indicia string 
                : returned from the data capture engine for the currently selected 
                : rate class, then look up its graphic ID 
   PARAMETERS   : Ptr to the graphic ID
   RETURN       : passback the the graphic ID of the inscription in usGraphicID 
                : BOB_OK if auto inscription found or none is specified or
                : BOB_AUTO_INSCRIPTION_NOT_FOUND if rules indicate a graphic is
                :     required but the graphic could not be found
                : BOB_AUTO_INSCRIPTION_OUT_OF_RANGE if rules indicate a graphic
                :     name that is too long
   NOTES        :
   
 HISTORY: 
  2010.08.03 Clarisa Bellamy - An empty string indicates no auto inscription, same 
                            as "NO_INSCRIPTION".  Use macro for string index. 
                            Update comments.  
******************************************************************************/
char fnGetAutoInscrIDViaRateInfo(ushort *ptrGraphicID)
{
//    unichar InscName[MAX_INSCR_NAME+1] = {0x00, 0x00};
//    uchar ucLen = 0;
    char retVal = BOB_OK;
//    const struct ind_output *pDCInfo;
//    SINT16 sRet;
//    const unichar NoInscValue[ 15 ] = { 'N','O','_','I','N','S','C','R','I','P','T','I','O','N','\0' };

    // Default to no auto inscription.
    *ptrGraphicID = 0;  

#if 0 //TODO Rate info
//TODO RAM remove rates    sRet = fnRateGetIndiciaInfo( &pDCInfo );
    
    // If there is an Auto Inscription, it's uniname will be in the 3rd Indicia_HRT entry.

    if( pDCInfo && sRet ==  RSSTS_NO_ERROR )
    {
        if( pDCInfo->NumIndiciaStrings > DCI_HRT_AUTO_INSC_IDX )
        {
            // If we have an Auto Inscription string, check if it is an empty string or not.
            ucLen = (UINT8)unistrlen( pDCInfo->Indicia_HRT[DCI_HRT_AUTO_INSC_IDX].String );

            if( (ucLen > 0) && (ucLen <= MAX_INSCR_NAME) )
            {
                // Copy the unistring name from the DCAP/rates data to our local buffer.
                (void)unistrcpy( InscName, pDCInfo->Indicia_HRT[DCI_HRT_AUTO_INSC_IDX].String );
            
                // If the unistring is not empty, check if it is "NO_INSCRIPTION"
                if( !unistrcmp( NoInscValue, InscName ) )
                {
                    retVal = BOB_OK;
                }
                else
                {
                    // Find inscription with matching uniname.
                    *ptrGraphicID = fnGrfxGetGraphicIdByTypeAndName( IMG_ID_INSCRIPTION, InscName );

                    // If inscription can't be found, return error
                    if( *ptrGraphicID == 0 )
                    {
                        retVal = BOB_AUTO_INSCRIPTION_NOT_FOUND;
                    }
                }
            }
            else if( ucLen == 0 )
            {
                // In 3rd gen, an empty string indicates No Inscription.
                retVal = BOB_OK;
            }
            else
            {
                // If specified name is too long, return error
                retVal = BOB_AUTO_INSCRIPTION_OUT_OF_RANGE;
            }
        }
    }
#endif //TODO Rate info
    return(retVal); 
}

/******************************************************************************
   FUNCTION NAME: fnSetIGInscr
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Tell the image generator to set the current inscription 
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnSetIGInscr(ushort ignore)
{
    char    retval = BOB_OK;
    uchar   *pGraphicDataAfterSig = NULL_PTR;
    uchar   *pGraphicComponent = NULL_PTR;
    ushort  usIgInscId = SET_NO_INSCR;      // the Inscription Generated ID
    ulong   ulIGretval;
    BOOL    fSetInsc = FALSE;

    if(currentInscrSelection == TURN_OFF_IMAGE)
    {
        usIgInscId = SET_NO_INSCR;
        fSetInsc = TRUE;
    }
    // check if VAS service selection diasables inscription region
    else if(fnCheckIfVASDisablesRegion(IMG_ID_INSCRIPTION))
    {
        usIgInscId = SET_NO_INSCR;
        currentInscrSelection = TURN_OFF_IMAGE;
        fSetInsc = TRUE;
    }
    else
    {
        pGraphicComponent = fnGrfxGetGrfkPtrByGenID( IMG_ID_INSCRIPTION, currentInscrSelection, GRFX_ANY_MATCH );
        if(   (pGraphicComponent != NULL_PTR) 
            && fnFlashGetGraphicMemberAddr(pGraphicComponent, GRAPHIC_DATA_AREA_AFTER_SIG ,(void**)&pGraphicDataAfterSig) )
        {
            usIgInscId = currentInscrSelection; 
            fSetInsc = TRUE;
        }
        else
        {
            fnSetBobInternalErr(BOB_INSCR_ACCESS_ERR);
            retval = BOB_SICK;
        }
    }

    if(fSetInsc)
    {
        ulIGretval = IG_SetInscr( (UINT8)usIgInscId, pGraphicComponent, pGraphicDataAfterSig );
        if(ulIGretval !=  IG_NO_ERROR)
        {
            fnSetIGBCErr(ulIGretval);
            retval = BOB_SICK;
        }
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnGetAutoInscriptionID
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Get the auto-inscripition name from DCAP for the
                : currently selected rate class, then look up its graphic ID 
   PARAMETERS   : None
   RETURN       : passback the the graphic ID of the inscription in usGraphicID 
                : BOB_OK if auto inscription found or none is specified or
                : BOB_AUTO_INSCRIPTION_NOT_FOUND if rules indicate a graphic is
                :     required but the graphic could not be found
                : BOB_AUTO_INSCRIPTION_OUT_OF_RANGE if rules indicate a graphic
                :     name that is too long
   NOTES        : Might have to get it from the rates PCL structure instead of 
                : DCAP in the future
******************************************************************************/
/*  not needed for G9
char fnGetAutoInscriptionID(ushort *ptrGraphicID)
{
    unichar InscName[MAX_INSCR_NAME+1] = {0x00, 0x00};
    uchar ucLen = 0;
    char *ptrInscName = NULL_PTR;
    char retVal = BOB_OK;
    char NoInscName[] = "NO_INSCRIPTION";

    *ptrGraphicID = 0;  //default

    // This is the DCAP inteface...
    // If an auto inscription is required for this class, a name will appear in
    // the second product information string for the current rate selection
//TODO JAH Remove dcapi.c    ptrInscName = fnDCAPGetProductDescriptionLine(1);

    if( ptrInscName != NULL_PTR )
    {
        ucLen = (unsigned char)strlen(ptrInscName);
        
        if( (ucLen > 0) && (ucLen <= MAX_INSCR_NAME) )
        {
            //(void)unistrcpyFromCharStr( InscName, ptrInscName );
            // if the inscription name isn't "NO_INSCRIPTION",
            // try to find the graphic that goes with the inscription name.
            if (strcmp(ptrInscName, NoInscName) != 0)
            {
                // the name in the graphic component is a Unicode string, change
                // the auto-inscription name from a char string to a Unicode string.
            (void)unistrcpyFromCharStr( InscName, ptrInscName );

                *ptrGraphicID = fnGrfxGetGraphicIdByTypeAndName(IMG_ID_INSCRIPTION, InscName);

                // if the graphic ID can't be found based on this name, return error
                if (*ptrGraphicID == 0)
                    retVal = BOB_AUTO_INSCRIPTION_NOT_FOUND;
            }
            // else, the return value is BOB_OK and the ID is zero
        }
        else
        {
            // if specified name is out of range, return error
            retVal = BOB_AUTO_INSCRIPTION_OUT_OF_RANGE;
        }
    }

    // else, the return value is BOB_OK and the ID is zero


    return(retVal); 
}
*/


/******************************************************************************
   FUNCTION NAME: fnSetIGInscrNone
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Tell the image generator to set no inscription 
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnSetIGInscrNone(ushort ignore)
{
    char retval = BOB_OK;
    uchar   *pGraphicDataAfterSig = NULL_PTR;
    uchar   *pGraphicComponent = NULL_PTR;
    ulong ulIGretval;

    ulIGretval = IG_SetInscr(SET_NO_INSCR, pGraphicComponent, pGraphicDataAfterSig);
    if(ulIGretval !=  IG_NO_ERROR)
    {
        fnSetIGBCErr(ulIGretval);
        retval = BOB_SICK;
    }   

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnDetermineInscrSelection
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Figure out which inscription selection is in use, the user's
                : selection or the auto selection.
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnDetermineInscrSelection(ushort ignore)
{
    char    retval = BOB_OK;
    BOOL    fEnabled = FALSE;
    uchar   ucStatus, ucIndex;
    char    autoInscState;          // auto inscription requirement per pcn parameter
    ushort  usAutoInscID = 0;       // inscription feature code

    // by default...
    currentAutoInscrSelection = TURN_OFF_IMAGE;
    currentInscrSelection = currentUserInscrSelection;                  

    // if autoinscription feature is on and auto inscription EMD parm is on...
    autoInscState = fnFlashGetAutoInscrType();
    if(IsFeatureEnabled(&fEnabled, AUTO_AD_INSCR_SUPPORT, &ucStatus, &ucIndex) && fEnabled && autoInscState > 0)
    {
        switch (autoInscState)
        {
/*  not needed for G9
           // Auto Inscription Based on Data Capture Rule Information
           // If Auto Inscription is Enabled, see if one is required for this class
           case AUTO_INSCRIPTION_VIA_DCAP:
           case FULL_AUTO_INSCRIPTION_VIA_DCAP:
               retval = fnGetAutoInscriptionID(&usAutoInscID);
               break;
*/

           // Auto Inscription Based on Rating PCL Information
           case AUTO_INSCRIPTION_VIA_RATES:
           case FULL_AUTO_INSCRIPTION_VIA_RATES:
                retval = fnGetAutoInscrIDViaRateInfo( &usAutoInscID );
                break;

           // Auto Inscription not supported
           default:
           case NO_AUTO_INSCRIPTION:
               break;
        }
    
        if (retval == BOB_OK)
        {   
            // if an autoinscription is defined for the current rating configuration
            // use it and clear the user's selection.  We clear the user selection
            // because when the rating configuration changes to one that does not
            // dictate an autoinscription we want the user to have to explicitly
            // start with no inscription and have to select an inscription again. 
            if (usAutoInscID > 0)
            {
                currentAutoInscrSelection = usAutoInscID;
                currentInscrSelection = currentAutoInscrSelection;
                currentUserInscrSelection = TURN_OFF_IMAGE;
            }
            // usAutoInscID == 0 when no auto inscription is specified
            else if(0 == usAutoInscID)
            {
                // if autoinscription feature is on and full auto inscription EMD parm is on...
                // when DCM doesn't have an auto inscription for a selected product, print with no inscription 
                if( (FULL_AUTO_INSCRIPTION_VIA_DCAP == autoInscState) || 
                    (FULL_AUTO_INSCRIPTION_VIA_RATES == autoInscState) )
                {      
                    currentAutoInscrSelection = TURN_OFF_IMAGE;
                    currentUserInscrSelection = TURN_OFF_IMAGE;
                    currentInscrSelection = currentUserInscrSelection;
                }
            }
        }
        else
        {
            //required inscription not found or DCAP rules indicate an invalid inscription required for this class
            fnSetBobInternalErr(retval);
            retval = BOB_SICK;
        }
    }

    return(retval);
}


/******************************************************************************
   FUNCTION NAME: fnLoadPermit
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Tell the IG to load the permit and the static vars
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnLoadPermit(ushort ignore)
{
    char retval = BOB_OK;
    ulong   ulIGretval;
    
    retval = fnLoadStaticVars(FALSE);

    if(retval == BOB_OK)
    {
        ulIGretval = IG_LoadPermit((uchar**)&rCMReply.pImage, &rCMReply.wImageLength, &rCMReply.wMinPrintLength,
                    &rStaticIGBlob.rIGBlob, (uchar) rStaticIGBlob.rIGBlobIndex.usRegGrpIndex & 0x0FF);
        
        if(ulIGretval == IG_TOO_MANY_COLUMNS)
        {
            ucStaticImageStatus = BOB_STATIC_IMAGE_TOO_MANY_COLUMNS;
            retval = BOB_SKIP_ALL;
        }
        else if(ulIGretval != IG_NO_ERROR)
        {
            fnSetIGBCErr(ulIGretval);
            retval = BOB_SICK;
        }
        else if(!fnCheckIGImageParams())
        {
            fnSetBobInternalErr(BOB_PERMIT_NOT_FOUND);
            retval = BOB_SICK;          
        }
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnSetIGPermitType
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Set the Permit type 
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : CompType is IMG_ID_PERMIT1_PE, IMG_ID_PERMIT2_PE, IMG_ID_PERMIT3_PE
                : for Permits currentPermitSelection contains generated graphic id.
                :
******************************************************************************/
char fnSetIGPermitType(ushort ignore)
{
    UINT8   retval = BOB_OK;
    UINT8   *pGraphicDataAfterSig = NULL_PTR;
    UINT8   *pGraphicComponent = NULL_PTR;
    UINT8   ucCompType = (UINT8)currentPermitTypeSelection;
    UINT32  ulIGretval;

    pGraphicComponent = fnGrfxGetGrfkPtrByGenID( ucCompType, currentPermitSelection, GRFX_ANY_MATCH );
    if( pGraphicComponent != NULL_PTR )
    {
        (void)fnFlashGetGraphicMemberAddr( pGraphicComponent, GRAPHIC_DATA_AREA_AFTER_SIG, (void**)&pGraphicDataAfterSig );
    }
    
    if( (pGraphicComponent == NULL_PTR) || (pGraphicDataAfterSig == NULL_PTR) )
    {
        fnSetBobInternalErr(BOB_PERMIT_NOT_FOUND);
        retval = BOB_SICK;
    }
    
    if(retval == BOB_OK)
    {
        ulIGretval = IG_SetPermit(ucCompType, pGraphicComponent, pGraphicDataAfterSig);
        
        if(ulIGretval != IG_NO_ERROR)
        {
            fnSetIGBCErr(ulIGretval);
            retval = BOB_SICK;
        }
        else if(!SetPermitIndex(currentPermitName)) // set permit batch counter index by name
        {
            DeleteMissingPermits();
            if(!SetPermitIndex(currentPermitName))
            {
                fnSetBobInternalErr(BOB_PERMIT_BATCH_COUNT_SET_ERROR);
                retval = BOB_SICK;
            }
        }
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnSetCurrentPermitName
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Set the current Permit name when the current permit ID is set
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : Called from SCRIPT_SET_CURRENT_PERMIT_SELECTION, which is a
                  followup script, called when there is an fnWriteData( SET_CURRENT_PERMIT_SELECTION )
                  This only really works if the PermitType is set correctly first, which is why the 
                  wrapper function fnAISetActive() should always be used to set the permit selection. -cwb
******************************************************************************/
char fnSetCurrentPermitName(ushort ignore)
{
    UINT8 *pUnikodeName = NULL_PTR;

    if(currentPermitSelection == TURN_OFF_IMAGE)
    {
        (void)unistrset( currentPermitName, 0 );
    }
    else
    {
        pUnikodeName = fnGrfxGetUniNameByGenID( (UINT8)currentPermitTypeSelection, 
                                                currentPermitSelection, GRFX_ANY_MATCH );

        if( pUnikodeName != NULL_PTR )
        {
            memcpy(currentPermitName, pUnikodeName, sizeof(currentPermitName));
        }

        currentPermitName[MAX_PERMIT_UNICHARS] = 0;  //make sure its null terminated
    }
    return(BOB_OK);
}

/******************************************************************************
   FUNCTION NAME: fnSetCurrentPermitSelection
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Set the current Permit ID when the current permit name is set
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnSetCurrentPermitSelection(ushort ignore)
{
    UINT16  uwPermitId = 0;
    UINT8   ubPermitType;

    uwPermitId = fnGrfxGetPermitIdAndTypeByName( &ubPermitType, currentPermitName );

    if( uwPermitId != 0 )
    {
        currentPermitSelection = uwPermitId;
        currentPermitTypeSelection = ubPermitType;
    }
    else
    {
        currentPermitSelection = TURN_OFF_IMAGE;
        currentPermitTypeSelection = TURN_OFF_IMAGE;
    }

    return(BOB_OK);
}

/******************************************************************************
   FUNCTION NAME: fnLoadDateTime
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Tell the IG to load the date time stamp
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnLoadDateTime(ushort ignore)
{
    char retval = BOB_OK;
    ulong   ulIGretval;
    
    retval = fnLoadStaticVars(FALSE);

    if(retval == BOB_OK)
    {
        ulIGretval = IG_LoadDateTime((uchar**)&rCMReply.pImage, &rCMReply.wImageLength, &rCMReply.wMinPrintLength,
                                    &rStaticIGBlob.rIGBlob, (uchar) rStaticIGBlob.rIGBlobIndex.usRegGrpIndex & 0x0FF);

        if(ulIGretval == IG_TOO_MANY_COLUMNS)
        {
            ucStaticImageStatus = BOB_STATIC_IMAGE_TOO_MANY_COLUMNS;
            retval = BOB_SKIP_ALL;
        }
        if(ulIGretval != IG_NO_ERROR)
        {
            fnSetIGBCErr(ulIGretval);
            retval = BOB_SICK;
        }
        else if(!fnCheckIGImageParams())
        {
            fnSetBobInternalErr(BOB_DATETIME_NOT_FOUND);
            retval = BOB_SICK;          
        }

    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnCheckIGImageParams
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Check that the image pointer is not null, that the column widths
                : are not zero and the column width is at least as wide as the 
                : minimum column width.
   PARAMETERS   : None
   RETURN       : TRUE if everything is OK, FALSE otherwise
   NOTES        : none
******************************************************************************/
BOOL fnCheckIGImageParams(void)
{
    char retval = TRUE;

    // the print buffer pointer and the column width were filled in by IG_LoadIndicia or IG_LoadReport, etc
    // check for obvious errors
    if(   (rCMReply.pImage == NULL_PTR) 
       || (rCMReply.wImageLength == 0) 
       || (rCMReply.wMinPrintLength == 0) 
       || (rCMReply.wImageLength < rCMReply.wMinPrintLength)
      )
    {
        retval = FALSE;
    }

    return(retval);
}


/******************************************************************************
   FUNCTION NAME: fnGenStaticImageReqScriptSelect
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Select an image gen script to run based on the print mode
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
UINT16 fnGenStaticImageReqScriptSelect( UINT16 ignore )
{
    UINT16 retval = DUMMY_SCRIPT;

    ucStaticImageStatus = BOB_STATIC_IMAGE_OK;

    switch(bUICPrintMode)
    {
        case BOB_DEBIT_FUNDS:
            lowWarningFlag = LOW_WARNING_ENABLED;
            retval = SCRIPT_STATIC_INDICIA; 
            break;
        case BOB_TEST_PRINT_MODE:
            retval = SCRIPT_LOAD_TEST_PRINT;
            break;
        case BOB_REPORT_MODE:
            retval = SCRIPT_LOAD_REPORT;
            break;
        case BOB_AD_ONLY_MODE:
            // since the Ad + Text Mode prints are done w/ the indicia region map,
            // turn off all the possible indicia graphics
            currentIndiToPrint = TURN_OFF_IMAGE;
            currentPreBarSelection = TURN_OFF_IMAGE;
            currentLagSelection = TURN_OFF_IMAGE;
            retval = SCRIPT_STATIC_AD_INSC;
            break;
        case BOB_PERMIT_MODE:
            retval = SCRIPT_STATIC_PERMIT;
            break;
        case BOB_DATE_STAMP_MODE:
            retval = SCRIPT_STATIC_DATE_STAMP;
            break;
        case BOB_DATE_STAMP_ONLY_MODE:
            retval = SCRIPT_STATIC_DATE_STAMP_ONLY;
            break;
        case BOB_SEAL_MODE:
        case BOB_IDLE_PRINT_MODE:
        default:
            retval = DUMMY_SCRIPT; 
            break;
    } 

    currentIndiToPrintReadOnly = currentIndiToPrint;
    return(retval);
}

/******************************************************************************
 FUNCTION NAME:       fnSetLiteIndiciaAndBC
 DESCRIPTION:
        Determine if we should print a normal or Lite or Extra Lite Indicia
         based on the user-selection. 
 ARGUMENTS:
    None
 RETURNS:
    bob's disposition
 NOTES: 
  1. This is called AFTER  fnSetNormalOrLowIndicia has already set the value of
     currentIndiToPrint to either IMG_ID_LOW_INDICIA or IMG_ID_NORMAL_INDICIA.
     If it is normal, check to see if user has selected IBI-Lite or IBI-ExtraLite.
 HISTORY:
  2009.09.12    Clarisa Bellamy  
******************************************************************************/
char fnSetLiteIndiciaAndBC( ushort ignore )
{
    UINT8   fIBILiteOn = FALSE;

    if( currentIndiToPrint == IMG_ID_NORMAL_INDICIA )
    {
        switch( currentIndiSelection )
        {
            case LITE_INDICIA_SETTING:
            {
                // Make sure that we are still IBILite capable.
                if( fnIsIBILiteCapable() == TRUE )
                {
                    currentIndiToPrint = IMG_ID_LITE_INDICIA_IMAGE;
                    fIBILiteOn = TRUE;
                }
                else 
                {
                    currentIndiSelection = NORMAL_INDICIA_SETTING;
                }
                break;
            }

            case EXTRA_LITE_INDICIA_SETTING:
            {
                // Make sure that we are still IBIExtraLite capable.
                if( fnIsIBIExtraLiteCapable() == TRUE )
                {
                    currentIndiToPrint = IMG_ID_EXTRA_LITE_INDICIA_IMAGE;
                    fIBILiteOn = TRUE;
                }
                else 
                {
                    currentIndiSelection = NORMAL_INDICIA_SETTING;
                }
                break;
            }

            case NORMAL_INDICIA_SETTING:
            default:    // No other user-selections supported at this time.
                currentIndiSelection = NORMAL_INDICIA_SETTING;
                break;
        }
    } // End of if( currentIndiToPrint == IMG_ID_NORMAL_INDICIA )
    // else (currentIndiToPrint is low value) do nothing...
    // {  Leave currentIndiToPrint as LowValue, and leave selection as is. 
    //    If low-value condition goes away (postage increases) the user 
    //      selection should take affect.
    // }

    // If IBILite or ExtrLite is the active print, change the barcode selection 
    //  and the shadow register debit type.   
    if( fIBILiteOn == TRUE )
    {
        fIPSD_IbiLiteOn = TRUE;
        currentBarCodeSelection = IMG_ID_REDUCED_BC;
        fnCMOSSetShadowRecordIndiciumType( IPSD_IND_TY_IBILITE );
    }
    else
    {
        fIPSD_IbiLiteOn = FALSE;
        currentBarCodeSelection = IMG_ID_BAR_GRAPHIC;
        fnCMOSSetShadowRecordIndiciumType( bIPSD_indiciaType );
    }
  
    return( BOB_OK );
}



/******************************************************************************
 FUNCTION NAME:  fnSetSpecialIndicia

 DESCRIPTION:    Determine if we should print special indicia

 ARGUMENTS:      None

 RETURNS:        None
******************************************************************************/
static void fnSetSpecialIndicia(void)
{
    UINT16  usCountry = fnGetDefaultRatingCountryCode();

    if (currentIndiToPrint == IMG_ID_NORMAL_INDICIA)
    {
        switch (usCountry)
        {
            case CANADA:
                if (getIndiciaType() == MACH_CTRL_INDICIATYPES_RETURN_POSTAGE_PREPAID)
                {
                    currentIndiToPrint = IMG_ID_CANADA_PREPAID_INDICIA;
                }
                break;

            default:
                break;
        }
    }

}



/******************************************************************************
   FUNCTION NAME: fnSetNormalOrLowIndicia
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Determine if we should print a normal or Low value Indicia
                : based on the current postage setting.
   PARAMETERS   : None
   RETURN       : bob's disposition
 NOTES        : 
 1. Use bobsPostageValue, because this is called before ulDebitedPostageValue is 
    set.
    HISTORY:
  2008.10.08 Clarisa Bellamy - Fix bug: When changing currentIndiSelection to 
     currentIndiToPrint for IBI-Lite, missed one.
  2008.09.16 Clarisa Bellamy - Add a call to fnSetLiteIndiciaAndBC() to 
     support IBI-Lite.  Merged from Janus.
  2008.08.05 Clarisa Bellamy - Modify fnSetNormalOrLowIndicia so that it can 
     call a new subroutine to check if the low-value indicia is supported.  
     After the first call to that routine, it will save the result, so 
     subsequent calls will go faster.  (Merged from 1.07 to fix fraca 147093.
******************************************************************************/
char fnSetNormalOrLowIndicia(ushort ignore)
{
    uchar semolians[SPARK_MONEY_SIZE]; 
    uchar *pLowValueIndiciaThreshold;
    double dblSemolians;

    if( fnFlashIsLowValueIndiciaSupported() == TRUE )
    {
        pLowValueIndiciaThreshold = fnFlashGetMoneyParm( MP_LOW_VALUE_INDICIA_THRESHOLD );

        memset( semolians, 0, sizeof(semolians) );
        memcpy( &semolians[1], bobsPostageValue, sizeof(bobsPostageValue) );
        dblSemolians = fnBinFive2Double( semolians );

        if(   (dblSemolians < fnBinFive2Double(pLowValueIndiciaThreshold)) 
           || (   (dblSemolians == 0.0) 
               && fnFlashGetByteParm(BP_USE_LOW_VALUE_INDICIA_FOR_0) ) ) 
        {
            currentIndiToPrint = IMG_ID_LOW_INDICIA;
        }
        else
        {
            currentIndiToPrint = IMG_ID_NORMAL_INDICIA;
        }
    }
    else
    {
        currentIndiToPrint = IMG_ID_NORMAL_INDICIA;
    }
    
    // Determines if we should print a normal, lite, or extralite indicia, 
    //  based on the user selection.
    (void)fnSetLiteIndiciaAndBC( ignore );

//  Check for other special indicia
    fnSetSpecialIndicia();
    
    // Set the variable that can be read by outside tasks:
    currentIndiToPrintReadOnly = currentIndiToPrint;
    
    return(BOB_OK);
}

/******************************************************************************
   FUNCTION NAME: fnSetLeadingImage
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : If we are using leading images, figure out which one to use.
                : Depends on EMD parameters, PCL info, and the phase of the moon.
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : fnSetNormalOrLowIndicia must be called first to predetermine
                : low vs normal value selection
******************************************************************************/
char fnSetLeadingImage(ushort ignore)
{
//    const struct ind_output *pDCInfo;
//    ulong ulIGretval1 = IG_NO_ERROR;
//	unsigned char ucCountry; // = fnGetSupportedCountryID();


    if (fnFlashGetByteParm(BP_LEADING_BAR_REQUIRED))
    {
/*
        if (ucCountry == RATE_COUNTRY_ID_GERMANY)
		{
			// This is what we do for Germany
            if( currentIndiToPrint == IMG_ID_LOW_INDICIA )
            {
                currentPreBarSelection = IMG_ID_LOWVAL_STD_LEADING_IMAGE;

				// the following is from when we thought we would have to have different
				// leading graphic images based on domestic vs. international because
				// the strings would be in different fonts depending on the mail type.
				//
                //if(pcl_output->pPCLInfo->RecordType == PCL_OUT_REC_TYPE_DOM_VAS_OVRFLW)
                //  currentPreBarSelection = IMG_ID_LOWVAL_DOM_LEADING_IMAGE;
                //else if(pcl_output->pPCLInfo->RecordType == PCL_OUT_REC_TYPE_INTL_VAS_OVRFLW)
                //  currentPreBarSelection = IMG_ID_LOWVAL_INTL_LEADING_IMAGE;
            }
            else
            {
                currentPreBarSelection = IMG_ID_NORMVAL_STD_LEADING_IMAGE;

				// the following is from when we thought we would have to have different
				// leading graphic images based on domestic vs. international because
				// the strings would be in different fonts depending on the mail type.
				//
                //if(pcl_output->pPCLInfo->RecordType == PCL_OUT_REC_TYPE_DOM_VAS_OVRFLW)
                //  currentPreBarSelection = IMG_ID_NORMVAL_DOM_LEADING_IMAGE;
                //else if(pcl_output->pPCLInfo->RecordType == PCL_OUT_REC_TYPE_INTL_VAS_OVRFLW)
                //  currentPreBarSelection = IMG_ID_NORMVAL_INTL_LEADING_IMAGE;
            }
		}
        else
        {
            // Get the Indicia info from the Data Capture Engine
	        if (fnRateGetIndiciaInfo( &pDCInfo ) == RSSTS_NO_ERROR)
	        {
		        if (ucCountry == RATE_COUNTRY_ID_UK)
				{
					switch (pDCInfo->IndiciaRecordType)
					{
						case PCL_OUT_REC_TYPE_UK_DOMESTIC:
						case PCL_OUT_REC_TYPE_UK_RETURN_MAIL:
						case PCL_OUT_REC_TYPE_UK_BUS_MAIL_1ST:
						case PCL_OUT_REC_TYPE_UK_BUS_MAIL_2ND:
		                	currentPreBarSelection = IMG_ID_UK_DOM_LEADING_IMAGE;
							break;

						case PCL_OUT_REC_TYPE_UK_INTERNATIONAL:
		                	currentPreBarSelection = IMG_ID_UK_INTL_LEADING_IMAGE;
							break;

						default:
					        currentPreBarSelection = IMG_BAR_LEADING_IMAGE;
							break;
		            }
	            }
				else
				{
					switch (pDCInfo->IndiciaRecordType)
					{
						case PCL_OUT_REC_TYPE_INTL_VAS:
		                	currentPreBarSelection = IMG_ID_INTERNAT_LEAD;
							break;

						case PCL_OUT_REC_TYPE_DOM_VAS:
						default:
		                	currentPreBarSelection = IMG_BAR_LEADING_IMAGE;
							break;
		            }
				}
			}
			else
			{
				 currentPreBarSelection = IMG_BAR_LEADING_IMAGE;
			}
        }
*/
    	currentPreBarSelection = IMG_BAR_LEADING_IMAGE;
        currentIndiToPrint = TURN_OFF_IMAGE;  // if leading BC images are used, no indicia is used
        currentIndiToPrintReadOnly = currentPreBarSelection;
    }
    else
    {
        currentPreBarSelection = IMG_BAR_LEADING_IMAGE;
    }

    return(BOB_OK);
}

//**************************************************************************
//  FUNCTION NAME:  
//       fnSetLaggingImage
//  DESCRIPTION:
//       If we are using lagging images, figure out which one to use.
//       Depends on EMD parameters, PCL info, and the height of the tide.
//  INPUTS:
//       None
//  RETURNS:
//       bob's disposition
//  NOTES:          
//       None
//  MODIFICATION HISTORY:
//  09-09-09    D. Kohl           Turn off image if product without
//                                lagging image for Holland
//  09-08-08    D. Kohl           Merge code from Janus for NetSet2
//  07-14-06    Dicky Sun         Check pcl_output pointer before using.
//  07-10-06    Dicky Sun         Update for supporting new return type of 
//                                fnRateGetProdConfigListInfo.
//              Craig DeFilippo   Initial version
//**************************************************************************
char fnSetLaggingImage(ushort ignore)
{
    char retval = BOB_OK;    
//	unsigned char ucCountry; // = fnGetSupportedCountryID();
    
#if defined (K700) || defined (G900)
    //const DCAI_IND_OUTPUT *pIndOutput;
//    const struct ind_output *pDCInfo;
//    ulong ulIGretval1 = IG_NO_ERROR;
#endif


	switch(fnFlashGetByteParm(BP_LAGGING_BAR_REQUIRED))
	{
    	case LAG_ALWAYS_REQUIRED:
	        currentLagSelection = IMG_BAR_LAGGING_IMAGE;
			break;

    	case LAG_DEPENDS_ON_VAS:
	        // set default value to no image
	        currentLagSelection = TURN_OFF_IMAGE; 
			
	        if( fnFlashGetByteParm(BP_ENBL_INT_DOM_SELECT) )
	        {
	            // Get the Indicia info from the Data Capture Engine
//TODO RAM remove rates	            (void)fnRateGetIndiciaInfo( &pDCInfo );

#if 0 //TODO RAM remove rates
	            if(pDCInfo->IndiciaRecordType == PCL_OUT_REC_TYPE_DOM_VAS)
	            {
	                currentLagSelection = IMG_ID_DOM_LAGGING_IMAGE;
	            }
	            else if(pDCInfo->IndiciaRecordType == PCL_OUT_REC_TYPE_INTL_VAS)
	            {
	                currentLagSelection = IMG_ID_INTL_LAGGING_IMAGE;
	            }
#endif //TODO RAM remove rates
	        }
	        else
	        {
// The following should be the same for all countries that use leading/lagging indicia
//	            if( ( ucCountry == RATE_COUNTRY_ID_HOLLAND  ) || 
//	                ( ucCountry == RATE_COUNTRY_ID_AUSTRIA  ) ||
//	                ( ucCountry == RATE_COUNTRY_ID_PORTUGAL ) ||
//	                ( ucCountry == RATE_COUNTRY_ID_BRAZIL ) )
//	            {
	                // Get the Indicia info from the Data Capture Engine
#if 0 //TODO RAM remove rates
			        if (fnRateGetIndiciaInfo( &pDCInfo ) == RSSTS_NO_ERROR)
			        {
		                if( pDCInfo->IndiciaRecordType == PCL_OUT_REC_TYPE_DOM_VAS )
		                {
		                    currentLagSelection = IMG_ID_DOM_LAGGING_IMAGE;

/* The following should be covered by the BP_VAS_DISABLES_AD parameter
		                    // if text entry is in the current print map, remove it
		                    if( fnCheckForComponentInPrintMap(IMG_ID_NORMAL_INDICIA, IMG_ID_TEXT_ENTRY) ) 
		                    {
		                        currentTextEntrySelection = TURN_OFF_IMAGE;
		                        ulIGretval1 = IG_DuckTextEntry( TRUE );
		                    }
						
		                    if( ulIGretval1 !=  IG_NO_ERROR )
		                    {
		                         fnSetIGBCErr( ulIGretval1 );
		                         retval = BOB_SICK;
		                    }
*/
		                }
//		            } // End if fnRateCheckCountryId
				}
#endif //TODO RAM remove rates
	        } // End BP_ENBL_INT_DOM_SELECT == 0
			break;

		default:
		    currentLagSelection = TURN_OFF_IMAGE;
			break;
	}

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnSetTownCircle
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Determine if we should print a town circle
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : This function not required since implementation of Bob's
                ; print maps. Town circle selection occurs in fnLoadMailSpecs
******************************************************************************/
char fnSetTownCircle(ushort ignore)
{
/*    // check if town circle is required, 
    // igonore town strip(2), Canada apparently has one
    if(fnFlashGetByteParm(BP_TC_TYPE) == TC_TYPE_CIRCLE)
        currentTCSelection = IMG_ID_TOWN_CIRCLE;
    else
        currentTCSelection = TURN_OFF_IMAGE;

*/
    return(BOB_OK);
}

/******************************************************************************
   FUNCTION NAME: fnIndiciaQuackers
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Check Indicia mode quack bits and set IG image duck options
                : accordingly.
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : 

1. Use bobsPostageValue because this is called before ulDebitedPostageValue is set.

   MODIFICATION HISTORY:
   07/15/2009  Bonnie Xu    Add the case for AUSTRIA
   08/27/2008  Joe Qu   Merged changes from K700 for Netherlands NetSet2,
                        add option to duck Visual Service Indicator
   08/14/2003  Craig DeFilippo  Initial version
******************************************************************************/
char fnIndiciaQuackers(ushort ignore)
{
    ulong ulIGretval1 = IG_NO_ERROR;
    ulong ulIGretval2 = IG_NO_ERROR;
    ulong ulIGretval3 = IG_NO_ERROR;
    ulong ulIGretval4 = IG_NO_ERROR;
    ulong ulIGretval5 = IG_NO_ERROR;
    ulong ulIGretval6 = IG_NO_ERROR;
//    ulong ulZero = 0;
    uchar ucDateDuckAllowed, ucDateDuckState = NO_DUCKING;
    uchar ucTownCircleUsed, ucTownDuckAllowed, ucTownDuckState = NO_DUCKING;
//    uchar ucEntgeltAvailable, ucEntgeltDuckState = TRUE;
    uchar ucPINAvailable, ucPINDuckState = FALSE;
    uchar ucMiscDuckAllowed, ucMiscDuckState = TRUE;
    uchar ucTextEntryDuckState = FALSE;
    char  retval = BOB_OK;
    BOOL  fReloadIndicia = FALSE;
	
#if defined (K700) || defined (G900)
    // for K700 3rd Gen Dcap
    // Get the Indicia info from the Data Capture Engine
//    const struct ind_output *pDCInfo;
//    const struct ts_output  *pTrackInfo=NULL;
//    SINT16 sRet;
#else
    char    bPCLRecordType = 0;
#endif


    ucDateDuckAllowed = fnFlashGetByteParm(BP_DATE_DUCKING_ALLOWED);
    ucTownCircleUsed = fnFlashGetByteParm(BP_TC_TYPE);
    ucTownDuckAllowed = fnFlashGetByteParm(BP_TC_DUCKING_ALLOWED);
//    ucEntgeltAvailable = fnFlashGetByteParm(BP_ENTGELT_AVAIL);
    ucPINAvailable = fnFlashGetByteParm(BP_PIN_ENABLE);
    ucMiscDuckAllowed = fnFlashGetByteParm(BP_MISC_DUCKING_ALLOWED);

    switch(ucDateDuckAllowed)
    {
        case ANY_DUCKING:
            if(usIndiciaPrintFlags & BOB_DUCK_DAY)
                ucDateDuckState = VLT_DUCKED_OUT_DAY;
            if(usIndiciaPrintFlags & BOB_DUCK_ENTIRE)
                ucDateDuckState = VLT_DUCKED_OUT_DATE;
            break;

        case ONLY_ALL_DUCKING:
            if(usIndiciaPrintFlags & BOB_DUCK_ENTIRE)
                ucDateDuckState = VLT_DUCKED_OUT_DATE;
            break;

        case ONLY_DAY_DUCKING:
            if(usIndiciaPrintFlags & BOB_DUCK_DAY)
                ucDateDuckState = VLT_DUCKED_OUT_DAY;
            break;

        case ONLY_DUCK_FROM_RATING:
            // check for fee based date ducking here
            // e.g. if in swiss predate mode, then duck the whole date
            (void)fnCheckIndiciaQuackers( 0 );
            if( usIndiciaPrintFlags & BOB_DUCK_ENTIRE )
                ucDateDuckState = VLT_DUCKED_OUT_DATE;
            else
                ucDateDuckState = NO_DUCKING;
            break;

        case NO_DUCKING:
        default:
            break;
    }

    ulIGretval1 = IG_DuckDate(ucDateDuckState);

    // if a town circle is used
    if(ucTownCircleUsed != TC_TYPE_NONE )
    {
        switch(ucTownDuckAllowed)
        {
            case TC_SEPARATE_DUCK:
                if(usIndiciaPrintFlags & BOB_DUCK_TC)
                {
                    ucTownDuckState = VLT_DUCKED_OUT_CIRCLE;
                }
                break;

            case TC_DUCK_W_DATE:
                if(ucDateDuckState == VLT_DUCKED_OUT_DATE)
                {
                    ucTownDuckState = VLT_DUCKED_OUT_CIRCLE;
                }
                break;

            case TC_NO_DUCK:
            default:
                break;
        }
        fReloadIndicia = TRUE;
        ulIGretval2 = IG_DuckTC(ucTownDuckState);
    }

    // if entgelt is available
/*
    if(ucEntgeltAvailable)
    {
        if(usIndiciaPrintFlags & BOB_PRINT_ENTGELT)
            ucEntgeltDuckState = FALSE;
        
        fReloadIndicia = TRUE;
        ulIGretval3 = IG_DuckEB(ucEntgeltDuckState);
    }
*/

    // if PIN is available
    if(ucPINAvailable)
    {
        if(usIndiciaPrintFlags & BOB_DUCK_PIN)
            ucPINDuckState = TRUE;
    
        ulIGretval4 = IG_DuckPIN(ucPINDuckState);
    }

    // if text entry is in the current print map
    if( fnCheckForComponentInPrintMap(IMG_ID_NORMAL_INDICIA, IMG_ID_TEXT_ENTRY) ) 
    {
        // check if VAS service selection diasables text message region
        if(fnCheckIfVASDisablesRegion(IMG_ID_TEXT_ENTRY))
        {
            currentTextEntrySelection = TURN_OFF_IMAGE;
        }

        //if(usIndiciaPrintFlags & BOB_DUCK_TEXT_MSG)
        if(currentTextEntrySelection == TURN_OFF_IMAGE)
        {
            ucTextEntryDuckState = TRUE;
        }

        fReloadIndicia = TRUE;
        ulIGretval5 = IG_DuckTextEntry(ucTextEntryDuckState);       
    }

    // check Miscellaneous Graphic Ducking
    if( ucMiscDuckAllowed != MG_DUCK_NEVER )
    {
        switch( ucMiscDuckAllowed )
        {
            case MG_DUCK_PCL_IND_STR2:
                 break;

            case MG_DUCK_DCM_IND_STR1:
                // Austria G961
/*
               if (fnRateCheckCountryId(RATE_COUNTRY_ID_AUSTRIA))
               {    
                  // if there is lagging image, remove the misc graphic.
                  if (currentLagSelection == IMG_ID_DOM_LAGGING_IMAGE)                    
                  {
                    ucMiscDuckState = TRUE;
                  } 
                  else 
                  {   
                    // Get the tracking info from the Data Capture Engine
                    sRet = fnRateGetTSInfo ( &pTrackInfo );
                    if( (sRet == RSSTS_NO_ERROR) 
                         && ((unistrlen(pTrackInfo -> TrackServ_HRT[0].String) != 0) 
                          || (unistrlen(pTrackInfo -> TrackServ_HRT[1].String) != 0)  
                          || (unistrlen(pTrackInfo -> TrackServ_HRT[2].String) != 0) 
                          || (unistrlen(pTrackInfo -> TrackServ_HRT[3].String) != 0)  
                          || (unistrlen(pTrackInfo -> TrackServ_HRT[4].String) != 0)))
                    {
                       ucMiscDuckState = FALSE;
                    }
                  }
                }
                else 
                {
                // NetSet2 VSI
                // for K700 & G900 3rd Gen Dcap
                // Get the Indicia info from the Data Capture Engine
                sRet = fnRateGetIndiciaInfo(&pDCInfo);
                if(   (sRet == RSSTS_NO_ERROR) 
                   && (unistrlen(pDCInfo->Indicia_HRT[1].String) != 0) )
                {
                     ucMiscDuckState = FALSE;
                }

                }
*/                break;

            default:
                break;
        }
        fReloadIndicia = TRUE;
        ulIGretval6 = IG_DuckMiscGraphic(ucMiscDuckState);
    }
    
    if(ulIGretval1 !=  IG_NO_ERROR)
    {
        fnSetIGBCErr(ulIGretval1);
        retval = BOB_SICK;
    }
    else if(ulIGretval2 !=  IG_NO_ERROR)
    {
        fnSetIGBCErr(ulIGretval2);
        retval = BOB_SICK;
    }
    else if(ulIGretval3 !=  IG_NO_ERROR)
    {
        fnSetIGBCErr(ulIGretval3);
        retval = BOB_SICK;
    }
    else if(ulIGretval4 !=  IG_NO_ERROR)
    {
        fnSetIGBCErr(ulIGretval4);
        retval = BOB_SICK;
    }
    else if(ulIGretval5 !=  IG_NO_ERROR)
    {
        fnSetIGBCErr(ulIGretval5);
        retval = BOB_SICK;
    }
    else if( ulIGretval6 !=  IG_NO_ERROR )
    {
        fnSetIGBCErr( ulIGretval6 );
        retval = BOB_SICK;
    }

    // we have already loaded the indicia, if we want to duck an image
    //  we have to reload the indicia
    if ( (retval == BOB_OK) && fReloadIndicia && (fIndiciaTypeSet == TRUE) ) 
    {
        retval = fnLoadIndicia(0);
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnPermitQuackers
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Check Permit Mode quack bits and set IG image duck options
                : accordingly.
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnPermitQuackers(ushort ignore)
{
    ulong ulIGretval1 = IG_NO_ERROR, ulIGretval2 = IG_NO_ERROR;
    ulong ulIGretval3 = IG_NO_ERROR, ulIGretval4 = IG_NO_ERROR;
    uchar ucDateDuckAllowed, ucDateDuckState = NO_DUCKING;
    uchar ucTownCircleUsed, ucTownDuckAllowed, ucTownDuckState = NO_DUCKING;
    uchar ucBatchDuckAllowed, ucBatchDuckState = FALSE;
    uchar ucTextEntryDuckState = FALSE;
    char  retval = BOB_OK;
    BOOL  fReloadPermit = FALSE;


    ucDateDuckAllowed = fnFlashGetByteParm(BP_PERMIT_DATE_DUCKING_ALLOWED);
    ucTownCircleUsed = fnFlashGetByteParm(BP_PERMIT_TC_TYPE);
    ucTownDuckAllowed = fnFlashGetByteParm(BP_PERMIT_TC_DUCKING_ALLOWED);
    ucBatchDuckAllowed = fnFlashGetByteParm(BP_PERMIT_BC_DUCKING_ALLOWED);

    switch(ucDateDuckAllowed)
    {
        case ANY_DUCKING:
            if(usPermitPrintFlags & BOB_DUCK_DAY)
                ucDateDuckState = VLT_DUCKED_OUT_DAY;
            if(usPermitPrintFlags & BOB_DUCK_ENTIRE)
                ucDateDuckState = VLT_DUCKED_OUT_DATE;
            break;

        case ONLY_ALL_DUCKING:
            if(usPermitPrintFlags & BOB_DUCK_ENTIRE)
                ucDateDuckState = VLT_DUCKED_OUT_DATE;
            break;

        case ONLY_DAY_DUCKING:
            if(usPermitPrintFlags & BOB_DUCK_DAY)
                ucDateDuckState = VLT_DUCKED_OUT_DAY;
            break;

        case NO_DUCKING:
        default:
            break;
    }

    ulIGretval1 = IG_DuckDate(ucDateDuckState);

    // if a town circle is used
    if(ucTownCircleUsed != TC_TYPE_NONE )
    {
        switch(ucTownDuckAllowed)
        {
            case TC_SEPARATE_DUCK:
                if(usPermitPrintFlags & BOB_DUCK_TC)
                {
                    ucTownDuckState = VLT_DUCKED_OUT_CIRCLE;
                }
                break;

            case TC_DUCK_W_DATE:
                if(ucDateDuckState == VLT_DUCKED_OUT_DATE)
                {
                    ucTownDuckState = VLT_DUCKED_OUT_CIRCLE;
                }
                break;

            case TC_NO_DUCK:
            default:
                break;
        }
        fReloadPermit = TRUE;
        ulIGretval2 = IG_DuckTC(ucTownDuckState);
    }

    // if Permit batch count ducking is allowed
    if(ucBatchDuckAllowed)
    {
        if(usPermitPrintFlags & BOB_DUCK_BATCH)
        {
            ucBatchDuckState = TRUE;
            // batch count will be in a misc graphic comp
        }
        fReloadPermit = TRUE;
        ulIGretval3 = IG_DuckBatchCount(ucBatchDuckState);
    }


    // if text entry is in the current print map
    if( fnCheckForComponentInPrintMap(IMG_ID_PERMIT1_PE, IMG_ID_TEXT_ENTRY) ) 
    {
        //if(usIndiciaPrintFlags & BOB_DUCK_TEXT_MSG)
        if(currentTextEntrySelection == TURN_OFF_IMAGE)
        {
            ucTextEntryDuckState = TRUE;
        }

        fReloadPermit = TRUE;
        ulIGretval4 = IG_DuckTextEntry(ucTextEntryDuckState);       
    }

    if(ulIGretval1 !=  IG_NO_ERROR)
    {
        fnSetIGBCErr(ulIGretval1);
        retval = BOB_SICK;
    }
    else if(ulIGretval2 !=  IG_NO_ERROR)
    {
        fnSetIGBCErr(ulIGretval2);
        retval = BOB_SICK;
    }
    else if(ulIGretval3 !=  IG_NO_ERROR)
    {
        fnSetIGBCErr(ulIGretval3);
        retval = BOB_SICK;
    }
    else if(ulIGretval4 !=  IG_NO_ERROR)
    {
        fnSetIGBCErr(ulIGretval4);
        retval = BOB_SICK;
    }

    // we have already loaded the perit, if we want to duck an image
    //  we have to reload the permit
    if( (retval == BOB_OK) &&  fReloadPermit )      
    {
        retval = fnLoadPermit(0);
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnDateTimeQuackers
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Check DateTime Mode quack bits and set IG image duck options
                : accordingly.
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnDateTimeQuackers(ushort ignore)
{
    ulong ulIGretval1 = IG_NO_ERROR, ulIGretval2 = IG_NO_ERROR;
    uchar ucTimeDuckAllowed, ucTimeDuckState = FALSE;
    char  retval = BOB_OK;
    uchar ucTextEntryDuckState = FALSE;
    BOOL  fReloadDateTime = FALSE;

    ucTimeDuckAllowed = fnFlashGetByteParm(BP_DATETIME_TIMEDUCK_ALLOWED);

    if(ucTimeDuckAllowed)
    {
        if(usDateTimePrintFlags & BOB_DUCK_TIME)
            ucTimeDuckState = TRUE;
        
        ulIGretval1 = IG_DuckTime(ucTimeDuckState);
    }

    // if text entry is in the current print map
    if( fnCheckForComponentInPrintMap(IMG_ID_DATE_TIME, IMG_ID_TEXT_ENTRY) ) 
    {
        // and text entry is not allowed
		// or none is selected
        if ((bUICPrintMode == BOB_DATE_STAMP_ONLY_MODE) ||
            (currentTextEntrySelection == TURN_OFF_IMAGE))
        {
            ucTextEntryDuckState = TRUE;
        }

        fReloadDateTime = TRUE;
        ulIGretval2 = IG_DuckTextEntry(ucTextEntryDuckState);       
    }

    if(ulIGretval1 !=  IG_NO_ERROR)
    {
        fnSetIGBCErr(ulIGretval1);
        retval = BOB_SICK;
    }
    else if(ulIGretval2 !=  IG_NO_ERROR)
    {
        fnSetIGBCErr(ulIGretval2);
        retval = BOB_SICK;
    }

    // we have already loaded the DateTime Stamp, if we want to duck an image
    //  we have to reload the DateTime Stamp
    if( (retval == BOB_OK) &&  fReloadDateTime )        
    {
        retval = fnLoadDateTime(0);
    }

    return(retval);
}


/******************************************************************************
   FUNCTION NAME: fnCheckIndiciaQuackers
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Check if any BOB managed implicit ducking should be set.
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES    : none
******************************************************************************/
char fnCheckIndiciaQuackers(ushort ignore)
{
//    ulong ulZero = 0;
//    const struct ind_output *pDCInfo;
//    SINT16 sRet;
	
	
    // only change the print flags for auto ducking based on rating, i.e as for P740
    if(fnFlashGetByteParm(BP_DATE_DUCKING_ALLOWED) == ONLY_DUCK_FROM_RATING)
    {
//TODO RAM remove rates        sRet = fnRateGetIndiciaInfo(&pDCInfo);
#if 0 //TODO RAM remove rates
        if(    (sRet == RSSTS_NO_ERROR) 
           &&  (   (pDCInfo->IndiciaRecordType == PCL_OUT_REC_TYPE_PREDATE) 
                || (pDCInfo->IndiciaRecordType == PCL_OUT_REC_TYPE_MPE_PREDATE) )
           &&  (memcmp(&bobsPostageValue, &ulZero, sizeof(bobsPostageValue)) != 0) )
        {
            usIndiciaPrintFlags |= BOB_DUCK_ENTIRE;
        }
#endif //TODO RAM remove rates
/*        else if ((sRet == RSSTS_NO_ERROR) && (pDCInfo->IndiciaRecordType == PCL_OUT_REC_TYPE_UK_RETURN_MAIL) &&
				 (fnGetSupportedCountryID() == RATE_COUNTRY_ID_UK))
        {
            usIndiciaPrintFlags |= BOB_DUCK_ENTIRE;
        }*/
#if 0 //TODO RAM remove rates
        else
        {
            usIndiciaPrintFlags &= ~BOB_DUCK_ENTIRE;
        }
#endif //TODO RAM remove rates
    }
	
    return (0); //TODO FIXME was return(BOB_OK);

}
/******************************************************************************
   FUNCTION NAME: fnClearCMReplyStatus
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : format the status bytes that go back to the CM and OI
   PARAMETERS   : None
   RETURN       : None
   NOTES        : None
******************************************************************************/
char fnClearCMReplyStatus(ushort ignore)
{
    memset(&rCMReply, 0, sizeof(rCMReply));
    return (BOB_OK);
}

//****************************************************************************
// FUNCTION NAME:   fnBobShadowRefill
//   DESCRIPTION:   Log the PostageValue Download response message to the CMOS
//                  shadow.
//
//        AUTHOR:   Craig Defilippo
//
//     ARGUMENTS:   Std PostMsg
//        RETURN:   standard prefunction
//
//         NOTES:   None 
//-----------------------------------------------------------------------------
char fnBobShadowRefill(ushort ignore)
{
    char    retval = BOB_OK;
    ulong   ulPieceCount, ulZeroPieceCount;

    memcpy(&ulPieceCount, pIPSD_pieceCount, sizeof(ulPieceCount));
    memcpy(&ulZeroPieceCount, pIPSD_ZeroPostagePieceCount, sizeof(ulZeroPieceCount));
    if(!fnCMOSAddNewExtendedShadowRefill (localXferCrtlStructPtr->bytesInSource, ulPieceCount, ulZeroPieceCount,
                                                (uchar *)(&m5IPSD_ascReg), (uchar *)(&mStd_DescendingReg),
                                                (uchar *) localXferCrtlStructPtr->ptrToDataToSendToDevice,
                                                SHADOW_TRANS_REFILL))
    {
        // Log the Error but don't terminate the script
//      fnSetBobInternalErr(BOB_SHADOW_REFILL_ERROR);
//      retval = BOB_ALLOW;

        fnLogError(ERR_TYPE_NORMAL, ERROR_CLASS_JBOB_TASK, BOB_SHADOW_REFILL_ERROR);
    }

    return( retval );
}

//****************************************************************************
// FUNCTION NAME:   fnBobShadowRefund
//   DESCRIPTION:   Log the PostageValue Refund response message to the CMOS
//                  shadow.
//
//        AUTHOR:   Craig Defilippo
//
//     ARGUMENTS:   Std PostMsg
//        RETURN:   standard prefunction
//
//         NOTES:   The refund should be logged right after the transaction with
//                  the ibutton and only if the refund was sucessful. 
//-----------------------------------------------------------------------------
char fnBobShadowRefund( ushort ignore )
{
    char    retval = BOB_OK;
    UINT32  ulPieceCount, ulZeroPieceCount;
    UINT8   state;
    BOOL    fStat;
    
    state = pIPSD_state[3];
    
    if(   (state == eIPSDSTATE_WITHDRAWN) 
       || (state == eIPSDSTATE_GERM_WITHDRAWN)  )
    {
        (void)memcpy( &ulPieceCount, pIPSD_pieceCount, sizeof(ulPieceCount) );
        (void)memcpy( &ulZeroPieceCount, pIPSD_ZeroPostagePieceCount, sizeof(ulZeroPieceCount) );
        fStat = fnCMOSAddNewExtendedShadowRefill( localXferCrtlStructPtr->bytesInSource, 
                                                  ulPieceCount, 
                                                  ulZeroPieceCount,
                                                  (uchar *)(&m5IPSD_ascReg), 
                                                  (uchar *)(&mStd_DescendingReg),
                                                  (uchar *)(localXferCrtlStructPtr->ptrToDataToSendToDevice),
                                                  SHADOW_TRANS_REFUND );
        if( !fStat )
        {
            // Log the Error but don't terminate the script
            //  fnSetBobInternalErr(BOB_SHADOW_REFILL_ERROR);
            //  retval = BOB_ALLOW;

            fnLogError( ERR_TYPE_NORMAL, ERROR_CLASS_JBOB_TASK, BOB_SHADOW_REFILL_ERROR );
        }
    }
    return( retval );
}

//****************************************************************************
// FUNCTION NAME:   fnBobShadowDebit
//   DESCRIPTION:   Log the Debit certificate message to the CMOS
//                  shadow.
//
//        AUTHOR:   Craig Defilippo
//
//     ARGUMENTS:   Std PostMsg
//        RETURN:   standard postfunction
//
//         NOTES:   
//  For Flex Debit:
//      The Shadow Debit Data will contain the Flex Debit input (including any 
//      padding) followed by the flex debit output (all 40 bytes of the glob 
//      variable, even if we only use a few of those bytes for the barcode and 
//      indicia.)  
//      If this combination is longer than 255 bytes, then the function that 
//      saves the data will truncate it, so we don't have to check the length 
//      here, unless we want some other functionality.

// 
// Modifications:
//  2008.04.09 Clarisa Bellamy - Added flex debit support, per Sandra's changes 
//      to Janus.  
//-----------------------------------------------------------------------------
char fnBobShadowDebit(ushort ignore, BOOL msgStat)
{
    char    retval = msgStat;
    UINT32  ulPieceCount;
    UINT32  ulZeroPieceCount;
    BOOL    fStat;

    if( (retval == BOB_OK) && fBobDebitOK)
    {
        // Now save the debit data in the shadow debit log.
        (void)memcpy( &ulPieceCount, pIPSD_pieceCount, sizeof(ulPieceCount) );
        (void)memcpy( &ulZeroPieceCount, pIPSD_ZeroPostagePieceCount, sizeof(ulZeroPieceCount) );

        // Send the proper data depending on whether we're doing a Flex Debit or not
        if( bIPSD_indiciaType != IPSD_IND_TY_FLEX_DEBIT )
        {
            fStat = fnCMOSAddNewExtendedShadowDebit( wIPSD_barcodeDataGlobSize, ulPieceCount, ulZeroPieceCount,
                                                      (uchar *)(&m5IPSD_ascReg), (uchar *)(&mStd_DescendingReg),
                                                      pIPSD_barcodeData.pGlob, SHADOW_TRANS_DEBIT );
        }
        else
        {
            unsigned char   ucTempData[512];
            // Sends the flex debit input, followed by the flex debit output.  For most countries, the 
            //  barcode data is a subset of this information.
            (void)memcpy( ucTempData, pIPSD_flexDebitDataGlob, wIPSD_flexDebitDataSize );
            (void)memcpy( &ucTempData[wIPSD_flexDebitDataSize], pIPSD_flexDebitOutput.pGlob, IPSD_MAXGLOBSZ_FLEXDEBITOUTPUT );
            fStat = fnCMOSAddNewExtendedShadowDebit( wIPSD_flexDebitDataSize+IPSD_MAXGLOBSZ_FLEXDEBITOUTPUT,
                                                      ulPieceCount, ulZeroPieceCount,
                                                      (uchar *)(&m5IPSD_ascReg), (uchar *)(&mStd_DescendingReg),
                                                      ucTempData, SHADOW_TRANS_DEBIT );
        }

        if(fStat == false)
        {
            // log the error but don't stop the print
    //      fnSetBobInternalErr(BOB_SHADOW_DEBIT_ERROR);
    //      retval = BOB_ALLOW;
            fnLogError(ERR_TYPE_NORMAL, ERROR_CLASS_JBOB_TASK, BOB_SHADOW_DEBIT_ERROR);
        }       
    }

    return( retval );
}

//****************************************************************************
// FUNCTION NAME:   fnIndiciaDebitPowerFailLog
//   DESCRIPTION:   Log the Debit data for the power fail report
//
//        AUTHOR:   Craig Defilippo
//
//     ARGUMENTS:   Std PostMsg
//        RETURN:   standard postfunction
//
//         NOTES:   Currently only for PCN's that use the French indicia algorithm
//                  Can be enhanced to support other algorithms.
// HISTORY:
//  2011.07.20  Clarisa Bellamy - Change to copy the debited postage value instead of bobs
//                          postage value (which may have been changed after the predebit.)
//                          If we loose power after debiting, we want to know how much
//                          the debit was for, not how much the next piece was going to be.                         
//                          This is called AFTER ulDebitPostageValue is set.
//  2008.09.16  Clarisa Bellamy  To remove lint error, change the size of the copy into
//                               zFrAuthCode, so it doesn't overwrite the array.  Then
//                               add copy into zFrVerCode, just so it still does what 
//                               it did before.  
//-----------------------------------------------------------------------------
void fnIndiciaDebitPowerFailLog(void)
{
    PF_DEBIT_LOG_REC    *tPtr;              // pointer to the CMOS print fail record

    if( (bIPSD_indiciaType == IPSD_IND_TY_FRANCE) && fnFlashGetByteParm(BP_DCAP_INDICIA_PWR_FAIL_REPORT) )
    {
        tPtr = (PF_DEBIT_LOG_REC *) (&CMOSpfDebitLogRec.pfCMOSDebitRecord[0]);

        memcpy( (char *)&tPtr->zIndiciaSerialNumber[0], (char *)&pIPSD_barcodeData.pFrance.bPSDSN[0],       sizeof(pIPSD_barcodeData.pFrance.bPSDSN)        );
        memcpy( (char *)&tPtr->iPostageValue[0],        (char *)&ulDebitedPostageValue,                     sizeof(ulDebitedPostageValue)                        );
        memcpy( (char *)&tPtr->zRegateCode[0],          (char *)&pIPSD_barcodeData.pFrance.pZipCode[0],     sizeof(pIPSD_barcodeData.pFrance.pZipCode)      );
        memcpy( (char *)&tPtr->zPieceCount[0],          (char *)&pIPSD_barcodeData.pFrance.pPieceCount[0],  sizeof(pIPSD_barcodeData.pFrance.pPieceCount)   );
        memcpy( (char *)&tPtr->zItemCategory[0],        (char *)&pIPSD_barcodeData.pFrance.pRateCategory[0],sizeof(pIPSD_barcodeData.pFrance.pRateCategory) );
        memcpy( (char *)&tPtr->zFrAuthCode[0],          (char *)&pIPSD_barcodeData.pFrance.pMAC[0],         sizeof(tPtr->zFrAuthCode)          );
        memcpy( (char *)&tPtr->zFrVerCode[0],           (char *)&pIPSD_barcodeData.pFrance.pMAC[ sizeof(tPtr->zFrAuthCode) ],
                                                                                                            sizeof(pIPSD_barcodeData.pFrance.pMAC) - sizeof(tPtr->zFrAuthCode)  );
        memcpy( (char *)&tPtr->zIndiciaDate[0],         (char *)&pIPSD_barcodeData.pFrance.pMailingDate[0], sizeof(pIPSD_barcodeData.pFrance.pMailingDate)  );
    }

    fnSetMailProgressState( PF_DEBIT_COMPLETE );
}

//****************************************************************************
// FUNCTION NAME:   fnAndYouShallBeCalled
//   DESCRIPTION:   Set the UIC SN, PCN and SMR
//
//        AUTHOR:   Craig Defilippo
//
//     ARGUMENTS:   Std PostMsg
//        RETURN:   standard postfunction
//
//         NOTES:   None 
//-----------------------------------------------------------------------------
char fnAndYouShallBeCalled(ushort ignore, BOOL msgStat)
{
    char    retval = msgStat;

    if(retval == BOB_OK)
    {
        // i button containers are smaller
        memcpy(CMOSSignature.bUicSN, pIPSD_PBISerialNum, sizeof(pIPSD_PBISerialNum));
        CMOSSignature.bUicSN[sizeof(pIPSD_PBISerialNum)] = 0;
        memcpy(CMOSSignature.bUicPcn, pIPSD_PCN, sizeof(pIPSD_PCN));
        CMOSSignature.bUicPcn[sizeof(pIPSD_PCN)] = 0;
        memcpy(CMOSSignature.bUicSMR, pIPSD_manufSMR, sizeof(pIPSD_manufSMR));
        CMOSSignature.bUicSMR[sizeof(pIPSD_manufSMR)] = 0;
    }
    return( retval );
}

//****************************************************************************
// FUNCTION NAME:   fnSavePSDStatus
//   DESCRIPTION:   Save the current 4 byte PSD status
//
//        AUTHOR:   Craig Defilippo
//
//     ARGUMENTS:   Std PostMsg
//        RETURN:   standard prefunction
//
//         NOTES:   None 
//-----------------------------------------------------------------------------
char fnSavePSDStatus(ushort ignore)
{
    memcpy(pIPSD_savedStatus, pIPSD_4ByteCmdStatusResp, sizeof(pIPSD_savedStatus)); 
    return( BOB_OK );
}


// IPSD MFG support stuff

/******************************************************************************
   FUNCTION NAME: fnPostMfgGetPsdChallengeAndStat
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : build a compound reponse consisting of:
                : 4 bytes of command status + 8 bytes of nonce
                : as the result of fnValidData(BOBID_MFG_GET_PSD_CHALLENGE_AND_STAT)
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnPostMfgGetPsdChallengeAndStat(ushort ignore, BOOL msgStat)
{
    char    retval;

    retval = msgStat;
    memset(pIPSD_ChallengeAndStat, 0, sizeof(pIPSD_ChallengeAndStat));

    if (retval == BOB_OK)
    {
        memcpy(pIPSD_ChallengeAndStat+2, pIPSD_cmdStatusResp, sizeof(pIPSD_cmdStatusResp)); 
        memcpy(pIPSD_ChallengeAndStat+4, pIPSD_nonce, sizeof(pIPSD_nonce)); 
    }
    else
    {
        memcpy(pIPSD_ChallengeAndStat+2, pIPSD_cmdStatusResp, sizeof(pIPSD_cmdStatusResp)); 
        memset(pIPSD_ChallengeAndStat+4, 0, sizeof(pIPSD_nonce)); 
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnPostGetStateCheckMfg
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : After a get state query check if we are in a manufacturing mode
                : 
                : If so, we want to skip the rest of the current script.
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnPostGetStateCheckMfg(ushort ignore, BOOL msgStat)
{
    char    retval;

    retval = msgStat;
    if ( (retval == BOB_OK) &&
         ( (pIPSD_state[3] < eIPSDSTATE_KEYSGENERATED)       ||
           (pIPSD_state[3] == eIPSDSTATE_PROVIDERKEYLOADED)  ||
           (pIPSD_state[3] == eIPSDSTATE_GERM_INITIALIZED)   ||
           (pIPSD_state[3] == eIPSDSTATE_GERM_KEYSGENERATED) )  ) 
    {
        retval = BOB_SKIP_ALL;
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnBuildPsdXportMsgStatus
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Build a 4 byte version of the IPSD status.  Upper two bytes
                : are null.
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : Seems like there must be a better way but I can't think of it.
******************************************************************************/
char fnBuildPsdXportMsgStatus(ushort ignore)
{

    memset(&pIPSD_4ByteCmdStatusResp,0,sizeof(pIPSD_4ByteCmdStatusResp));
    memcpy(&pIPSD_4ByteCmdStatusResp[2],&pIPSD_cmdStatusResp, sizeof(pIPSD_cmdStatusResp));

    return(BOB_OK);
}

// special rates testing requirement stuff

/******************************************************************************
   FUNCTION NAME: fnPickAZipcode
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : point origin zip code pointer to IPSD origin zip code or
                : fake origin zipcode
   PARAMETERS   : None
   RETURN       : bob's disposition
   NOTES        : none
******************************************************************************/
char fnPickAZipcode(ushort ignore)
{
    if(strlen((char*)pIPSD_FakeOriginZipcode) > 0)
        memcpy(pIPSD_OriginZipCode, pIPSD_FakeOriginZipcode, sizeof(pIPSD_FakeOriginZipcode));
    else
        memcpy(pIPSD_OriginZipCode, pIPSD_zipCode, sizeof(pIPSD_zipCode));

    return(BOB_OK);
}

/******************************************************************************
   FUNCTION NAME: fnModifyBarcodeDataGlob
 DESCRIPTION  : 
      Pad or append data to the debit glob generated by psd debit.
      Always passes back the proper barcode glob pointer and always returns
       the proper barcode glob length.
 PARAMETERS : 
    Pass back pointer to barcode data array
    Packed Byte Parameter ID of the appropriate Create Barcode Record.  Typically 
     IPB_BUILD_BC_DEF_REC, but it could be a different one.
 RETURN :     
    Length of barcode data array
 NOTES        : 
    Yikes!
 HISTORY:
  2010.07.23 Clarisa Bellamy - Clean up and fix bug, where if length of actual data 
                        was zero, the field was treated as a variable-length field.
 2010.04.12  Jingwei,Li  Fix bug where a variable barcode record length is set to 0, that
                        caused the actual barcode data can not be copied to the barcode data glob.
 2008.10.08 Clarisa Bellamy - Fixed initialization of usSize. 
 2008.08.13 Clarisa Bellamy - Fix bug where a Right-justified field, with less data 
                        than the field length indiciated by the field record, will 
                        cause the Modified Barcode Length to be wrong (short).
                        Solution: 
                          1) Use the field size, instead of the copy size, to 
                             adjust the total size.
                          2) Make sure the local copy of the field length is set to 
                             the copy size for variable-length fields, before adding 
                             it to the total size.
                        The rest of the changes are white-space, comments, and type renames.
  2008.06.12 Clarisa Bellamy - Fix it so that it always returns the correct size 
                        of the barcode data glob (if there is no create barcode 
                        definition) or of the modified barcode data glob (if 
                        there IS a create barcode definition.)
   AUTHOR       : Craig DeFilippo
******************************************************************************/
ushort fnModifyBarcodeDataGlob( uchar **pBarcodeGlobAddr, ushort usBarCodeRecID )
{
    IPSD_BC_DEF_REC rFieldRec;  // Copy of a single record from the ID'd Definition Record.
    UINT16  usNumFields;        // Number of fields in the ID'd definition record.
    UINT16  i;                  // Index into Definition Record of field that is currently in rFieldRec.
    UINT16  j;                  // byte-index used when doing a justified copy from temp glob to Modified Barcode glob.
    UINT16  usTempLen;          // Length of data in TempGlob
    UINT16  usCopySize = 0;     // Size of data to copy from temp glob to Mod barcode glob..
    UINT16  usRetLen = 0;       // Return value!
                                // Size of Modified Barcode Glob (or Barcode glob, if no modification.)
    UINT8   *pGlob;             // Travelling ptr to data Destination, in modified barcode data glob.
    UINT8   *pTempGlob;         // Ptr to temp glob.
    IPSD_DEF_REC rFieldRec1;    // Copy of 1 field record from IPB_GET_INDICIA_DEF_REC, that has Field ID = INDICIA_DEF_REC_PAD_82_ID.

    // If we don't modify it, point to the unmodified barcode, and return the 
    // length of data received.
    *pBarcodeGlobAddr = pIPSD_barcodeData.pGlob;    
    usRetLen = wIPSD_barcodeDataGlobSize;   
     
    usNumFields = fnFlashGetIBPNumFields( usBarCodeRecID );
	
    // Clear out the Modified glob, and size to start with.
    pGlob = pIPSD_ModifiedBarcodeData.pGlob;
    memset( pGlob, 0, sizeof(pIPSD_ModifiedBarcodeData.pGlob) );
    wIPSD_ModifiedBarcodeDataGlobSize = 0;
	
    // Set ptr to location for temp storage of one field
    pTempGlob = pIPSD_TempModifiedBarcodeData.pGlob;

    // If the number of fields in this record is Zero,
    //  no barcode modification is neccesary
    if( usNumFields )
    {
        // clear the temp glob which will capture one variable each iteration
        memset( pTempGlob, 0, sizeof(pIPSD_TempModifiedBarcodeData.pGlob) );

        // scan through the IPB_BUILD_BC_DEF_REC and fetch each variable
        for( i = 0; i < usNumFields; i++ )
        {
            if( fnFlashGetIBPBcDefRecVarOrdered( &rFieldRec, usBarCodeRecID, i ) )
            {
                // Default copy size to field length.
                usCopySize = rFieldRec.usFieldLen;

                // Get the data into the temp glob, and get the size.
                usTempLen = fnCallReportTableFunction( rFieldRec.usFieldFn, 
                                                       (void*)pTempGlob, 
                                                       rFieldRec.usFieldID );

                // Adjust the copy length and set background as necessary.
                if( rFieldRec.usFieldLen == IBP_BC_DEF_REC_LEN_VAR )
                {
                    // Variable length fields are always the actual data size.
                    // Set the copy size and ...
                    usCopySize = usTempLen;
					
                    // ...our local copy of the field size to the actual data size.
                    rFieldRec.usFieldLen = usTempLen;
                }
                else // Fixed-length field...
                {
                    // If less data than destination size, adjust copy size 
                    //  to the amount of data and fill in the background.
                    if( usTempLen < rFieldRec.usFieldLen )
                    {
                        usCopySize = usTempLen;
						
                        // Set entire field to pad character.
                        memset( pGlob, rFieldRec.ucFieldPad, rFieldRec.usFieldLen );
                    }
                }
                
                // If there is data to copy, copy the data from temp glob to 
                //  modified barcode glob.
                if( usTempLen )
                {
                    // Right Justified copy...
                    // WARNING!!! Do NOT try Right justification w/ variable length.
                    if(   (rFieldRec.ucFieldJust == IBP_DCR_FLD_JUST_RIGHT)
                       && (rFieldRec.usFieldLen != usTempLen) )
                    {
                        // Copy data, from right-side of temp to right-side of dest field in glob.
                        for( j = 0; j < usCopySize; j++ )
                        {
                            *(pGlob + ((rFieldRec.usFieldLen -1) -j)) = *(pTempGlob + ((usTempLen -1) -j));
                        }
                    }
                    // Copy left justified or variable length. (No center justification supported) 
                    //  Justification is moot for variable-length fields, as the actual length
                    //  and field length are the same, but they should be copied as left-jutified.
                    else
                    {
                        // Copy the temp data to the glob, left-justiifed.
                        memcpy( pGlob, pTempGlob, usCopySize );
                    }
                }
                // Now, rFieldRec.usFieldLen should contain the actual length of
                //  the data that was added to the glob, including padding.
                //  Update the pointer to the next available position in the glob, 
                //  and the total glob size.
                pGlob += rFieldRec.usFieldLen;
                wIPSD_ModifiedBarcodeDataGlobSize += rFieldRec.usFieldLen;
            } // End of If( successfully got a copy of the field record )
        } // End of loop (for each field record in the Definition Record.)

        // All fields have been processed. Set the address and total length.
        *pBarcodeGlobAddr = pIPSD_ModifiedBarcodeData.pGlob;
        usRetLen = wIPSD_ModifiedBarcodeDataGlobSize;
    }
    // The code below must be removed if the Candian EMD is changed to accomodate the new general mechanism above

    // if the definition record includes the parameter for an 82 byte Zero pad then do it
    // e.g. required for Canadian Indicia
    else if(fnFlashGetIBPRecVar(&rFieldRec1, IPB_GET_INDICIA_DEF_REC, INDICIA_DEF_REC_PAD_82_ID))
    {
        pGlob = pIPSD_barcodeData.pGlob;
        pGlob += rFieldRec1.usFieldOff;

        // pad
        memset( pGlob, 0, 82 );

        // adjust recorded size of glob
        // add in the pad and subtract the 4 byte creation time,
        // which is not encoded in the 2D barcode
        wIPSD_barcodeDataGlobSize += (82 - 4);
        usRetLen = wIPSD_barcodeDataGlobSize;        
    }

    return( usRetLen );
}

//******************************************************************************
// FUNCTION NAME:           fnSwitchPreDebitScript
// DESCRIPTION  : choose a predebit script, precreate or precompute_r
// PARAMETERS   : Default subscript to run.
// RETURN       : Script ID of actual subscript to run.
// NOTES: 
//  1. Soze you wanna do a debit...I tink we could help youz
//  2. For IPSD versions 3.x and 5.x:       for all others:      
//       Indicia Type 5 = USPS ECDSA            Indicia Type 5 =  BELGIUM
//       Indicia Type 6 = Canada ECDSA          Indicia Type 6 =  NETHERLANDS
//    For this reason, we have to avoid the switch on the indicia type if we
//      are running with a 3.x or 5.x ISPD.  Luckily, the ECDSA types both use
//      the Standard Precreate message.
//HISTORY:
// 2009.06.10  Clarisa Bellamy - Merge changes from Janus for canadian types.
// 2008.03.03  Clarisa Bellamy - Merge changes from Janus 15.04.02 for flex
//                    debit.  Orbit supports the Spain/Netherlands option, 
//                    but we don't need it yet.
// 2008.06.12  Clarisa Bellamy - This is a redirect function, so it should
//                    return UINT16 (a script ID.)
// 2008.08.26  Joe Qu  - Uncomment cases for IPSD_IND_TY_NETHERLANDS and 
//                     IPSD_IND_TY_SPAIN_GENERIC for NetSet2 Netherlands
// AUTHOR       : Craig DeFilippo
//******************************************************************************/
UINT16 fnSwitchPreDebitScript( UINT16 msgID )
{
    UINT16  retval = msgID;  // Default to do the script passed from Orbit record

    if(   (bIPSD_indiciaType == IPSD_IND_TY_CANADA_DSA) 
       && (strcmp( pIPSD_FirmwareVerSubStr, "1.50" ) == 0) )
    {
        retval = SCRIPT_IPSD_PRECOMPUTE_R;
    }
    else
    {
        // Don't send a second precreate message, if the last message
        //  sent to the IPSD was a precreate message.
        if( fnGetIpsdPreCreateState() == TRUE )
            retval = DUMMY_SCRIPT;
        else 
        {
            // Before we switch on the indicia type, check for IPSD version 3.x or 5.x.   
            //  See note 2 above. 
            if( fIPSD_FWVerModsIndiciaType == FALSE )
            {
            switch(bIPSD_indiciaType)
            {
               case IPSD_IND_TY_GERMANY:
                  retval = SCRIPT_IPSD_PRECRE_INDI_GERM;
                  break;

               case IPSD_IND_TY_NETHERLANDS:
               case IPSD_IND_TY_SPAIN_GENERIC:
                  retval = SCRIPT_IPSD_PRECRE_INDI_WITH_MSG_TYPE;
                  break;

               case IPSD_IND_TY_FLEX_DEBIT:
                  retval = SCRIPT_IPSD_PRECRE_INDI_FLEX_DEBIT;
                  break;

               default: //SCRIPT_IPSD_PRECRE_INDI_STD;
                  break;
            } 
            } // else 3.x or 5.x, just used passed in script ID.
        }
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnSwitchDebitScript
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : choose a debit script, 
             : commitTransaction - by default
             : createIndicium - only for Canada DSA 1.5 ibutton 
             : commitFlexDebit - for flex debits
   PARAMETERS   : Default subscript to run.
   RETURN       : Script ID of actual subscript to run.
   NOTES        : Passed in script is SCRIPT_IPSD_COMMIT_TRANSACTION
******************************************************************************/
UINT16 fnSwitchDebitScript( UINT16 msgID )
{
    UINT16  retval = msgID;  // Default to do the script passed from Orbit record

    switch(bIPSD_indiciaType)
    {
        case IPSD_IND_TY_FLEX_DEBIT:
            retval = SCRIPT_IPSD_COMMIT_FLEX_DEBIT;
            break;

        case IPSD_IND_TY_CANADA_DSA:
            if( !strcmp( pIPSD_FirmwareVerSubStr, "1.50" ) )
            {
                retval = SCRIPT_IPSD_CREATEINDICIA;
            }
            break;

        default:
            // do the script passed from Orbit record
            break;
    }
    return(retval);
}

// Miscelleneous
/******************************************************************************
   FUNCTION NAME: fnFormatMailCreationDate
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Convert two byte binary representation of indicia creation date
                : from the psd to ascii format required to print on indicia
   PARAMETERS   : creation date, pass back ascii date, pass back DATETIME
   RETURN       : None
   NOTES        : It's a Canada thang

    First 7 bits comprise year, e.g. 099 = 1999, 100 = 2000, 127 = 2027,
                                     000 = 2028, 050 = 2078, 098 = 2126
    Last 9 bits comprise day of year, e.g. 0 = Jan 1, 1 = Jan 2, 31 = Feb 1,
                                         364 (non-leap yr) or 365 (leap yr) = Dec 31
    Format pAsciiDate as "MMDD" string for output, "0228" = Feb 28
******************************************************************************/
void fnFormatMailCreationDate(const uchar* pBinDate, char* pAsciiDate, DATETIME* pDateTime)
{
    ushort ucYear;
    ushort wDays;
    uchar aDate[2];


    (void)memset(pDateTime, 0, sizeof(DATETIME));
    pDateTime->bDayOfWeek = 2;  // I met her on a monday and my heart stood still
    (void)memcpy(aDate, pBinDate, 2);

    ucYear = (aDate[0] >> 1);
    if (ucYear < 99)
        ucYear += 2028;
    else
        ucYear += 1900; 

    pDateTime->bCentury = (unsigned char)(ucYear / 100);
    pDateTime->bYear = ucYear % 100;

    wDays = ((aDate[0] & 0x01) * 256) + aDate[1] + 1;

    fnDayOfYearToDayMonth(wDays, pDateTime);

    (void)sprintf(pAsciiDate, "%02d%02d", pDateTime->bMonth, pDateTime->bDay);
}

/******************************************************************************
   FUNCTION NAME: fnFormatNetSet2ProdDate
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Convert NetSet2 ascii indica date format to formatted date
             : and to formal date time structure
   PARAMETERS   : production date, pass back ascii date, pass back DATETIME
   RETURN       : None
   NOTES         : Its a NetSet2 thang
                 : the format of production date in the debit certificate is YYMMDD
******************************************************************************/
void fnFormatNetSet2ProdDate( const UINT8* pInDate, char* pOutDate, DATETIME* pDateTime )
{
    char   pTemp[4];

    memset( pDateTime, 0, sizeof(DATETIME) );
    pDateTime->bDayOfWeek = 2;  // I met her on a monday and my heart stood still
    pDateTime->bCentury = 20;

    memset( pTemp, 0, sizeof(pTemp) );
    memcpy( pTemp, pInDate, 2 );
    pDateTime->bYear = (UINT8)atoi( pTemp );

    memcpy( pTemp, pInDate+2, 2 );
    pDateTime->bMonth = (UINT8)atoi( pTemp );

    memcpy(pTemp, pInDate+4, 2);
    pDateTime->bDay = (UINT8)atoi( pTemp );

    (void)fnFormatIndiciaDate( (void *) pOutDate, pDateTime );
}

/******************************************************************************
   FUNCTION NAME: fnFormatSecurityCode
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Convert the 30 MSbytes of input byte array to 6 ascii characters
                : Interpret the byte array as 5 6-bit strings.  Add the decimal 
                : value of each 6-bit number to 50 to determine the ascii value
                : of each byte of output. Use the substitution table
                : below on the output values to eliminate visual misinterpretation.
   PARAMETERS   : None
   RETURN       : None
   NOTES        : its a Canada thang
******************************************************************************/
const uchar aSecurityCodeSub[][2] = {
{58, 114},
{59, 115},
{79, 116},
{91, 117},
{96, 118},
{105, 119},
{106, 120},
{108, 121},
{111, 122},
{0, 0},     //end table 
};

void fnFormatSecurityCode(const uchar* pSecCode, char* pAsciiCode) 
{
    uchar   pTemp[6],i,j;
    ulong   ulSecCode = 0;
    
    // on a little endian machine this is a problem...
    // memcpy((uchar*) &ulSecCode, pSecCode, sizeof(ulong));
    // but this works either way...
    for(i=0, j=3 ;i<4;i++, j--)
    {       
        ulSecCode += ( pSecCode[i] * ( 1<<(j*8) ) );
    }

    pTemp[0] = (uchar) ((ulSecCode & 0xFC000000) >> 26);
    pTemp[1] = (uchar) ((ulSecCode & 0x03F00000) >> 20);
    pTemp[2] = (uchar) ((ulSecCode & 0xF00FC000) >> 14);
    pTemp[3] = (uchar) ((ulSecCode & 0xF0003F00) >> 8);
    pTemp[4] = (uchar) ((ulSecCode & 0xF00000FC) >> 2); 
    pTemp[5] = 0; //null terminate

    for(i=0; i<5; i++)
    {
        pTemp[i] += 50;
        // scan for possible substitution
        j = 0;
        while(aSecurityCodeSub[j][0] > 0)
        {
            if(aSecurityCodeSub[j][0] == pTemp[i])
            {
                pTemp[i] = aSecurityCodeSub[j][1];
                break;
            }
            j++;                
        }       
    }
    strcpy(pAsciiCode, (char*) pTemp);
}

/******************************************************************************
   FUNCTION NAME: fnGetLowWarningStatus
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Utility to retreive the value of Bob's low warning status 
   PARAMETERS   : None
   RETURN       : low warning status 
   NOTES        : none
******************************************************************************/
uchar fnGetLowWarningStatus(void)
{
    return(lowWarningFlag);
}

/******************************************************************************
   FUNCTION NAME: fnSetLowWarningStatus
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Utility to set the value of Bob's low warning status 
   PARAMETERS   : None
   RETURN       : none
   NOTES        : state should be LOW_WARNING_ENABLED or LOW_WARNING_DISABLED
******************************************************************************/
void fnSetLowWarningStatus(uchar ucState)
{
    lowWarningFlag = ucState;
}

//****************************************************************************
// FUNCTION NAME:   fnIsGerman
//   DESCRIPTION:   If the flash is configured for a german build,
//                  returns TRUE.
//
//        AUTHOR:   blu
//
//     ARGUMENTS:   none
//        RETURN:   BOB_OK:     Script handler will process the script command
//                  BOB_SKIP:   Script handler will move on to the next script line
//
//         NOTES:   
//-----------------------------------------------------------------------------
char fnIsGerman(ushort msgID)
{
    char retval;

    retval = BOB_SKIP;
    if (fnFrancanSense()) retval = BOB_OK;
    return(retval);
}
//****************************************************************************
// FUNCTION NAME:   fnIsNotGerman
//   DESCRIPTION:   If the flash is NOT configured for a german build,
//                  informs the script handler to skip the current script instruction.
//
//        AUTHOR:   blu
//
//     ARGUMENTS:   none
//        RETURN:   BOB_OK:     Script handler will process the script command
//                  BOB_SKIP:   Script handler will move on to the next script line
//
//         NOTES:   
//-----------------------------------------------------------------------------
char fnIsNotGerman(ushort msgID)
{
    BOOL    retval;

    retval = BOB_OK;
    if (fnFrancanSense()) retval = BOB_SKIP;
    return(retval);
}

//****************************************************************************
// FUNCTION NAME:       fnSwitchPVDScript
// DESCRIPTION:     
//      Redirect script that choose to run the normal or German version of the
//      PVD response script.                 
// ARGUMENTS:           
//      msgID = Default subscript to run.  
// RETURN:
//      Script Id of subscript to run.   
// NOTES:   
//-----------------------------------------------------------------------------
UINT16 fnSwitchPVDScript( UINT16 msgID )
{
    UINT16    retval;
    
    retval = SCRIPT_NORMAL_PVD_RSP_RECORD;
    if( fnFrancanSense() )
    {
        retval = SCRIPT_GERMAN_PVD_RSP_RECORD;
    }
    return( retval );
}



/****************************************************************************
   FUNCTION NAME: fnXferPsdNonce2XferContStruct
   AUTHOR       : blu, merged from Mega
   DESCRIPTION  : Copies the iButton nonce and command status to a transfer
                : control structure passed from the pbptask.
   PARAMETERS   : Standard post message function arguments.
   RETURN       : The passed message status is echoed back.
   NOTES        : Related to EDC operations with the data center
******************************************************************************/
char fnXferPsdNonce2XferContStruct( ushort ignore, BOOL msgStat ) 
{
    struct  TransferControlStructure *myXferStruc;
    UINT8    *dest;

    if( msgStat == BOB_OK ) 
    {
        myXferStruc = (struct TransferControlStructure *)(bobsCommand.IntertaskUnion.PointerData.pData);
        dest = myXferStruc->ptrToDeviceReplyData[0];
        memcpy( dest, pIPSD_nonce, sizeof(pIPSD_nonce) );
        myXferStruc->bytesWrittenToDest[0] = sizeof(pIPSD_nonce);
        memcpy( myXferStruc->deviceReplyStatus, pIPSD_cmdStatusResp, sizeof(pIPSD_cmdStatusResp) );
    }
    return(msgStat);
}
/****************************************************************************
   FUNCTION NAME: fnChooseEdcRecord
   AUTHOR       : blu, merged from Mega
   DESCRIPTION  : Evalutes an EDC record received from the data center and determines
                : which subscript to run in response to the content of the record.
   PARAMETERS   : Default subscript to run.
   RETURN       : Returns the ID of the subscript to use for the EDC operation
   NOTES        : Related to EDC operations with the data center
******************************************************************************/
UINT16 fnChooseEdcRecord( UINT16 msgID ) 
{
    uchar   ucRecType;
    UINT16  retval = msgID;     // Default is passed in, dummy_script, do nothing.

    (void)fnLoadXfrCtlPtr( msgID );
    // Clear the flag for the next call.
    bGotXfrStructPtr = FALSE;

    localXferCrtlStructPtr->ptrToDataToSendToDevice += 4;   // skip over 4 bytes of msg header

    ucRecType = *localXferCrtlStructPtr->ptrToDataToSendToDevice;

    switch( ucRecType ) 
    {
        case eIPSDMT_ENABLE_PSD_DATA:
            retval = PSD_EDC_ENABLE_RECORD;
            break;   
        case eIPSDMT_DISABLE_PSD_DATA:
            retval = PSD_EDC_DISABLE_RECORD;
            break;   
        default:
            retval = BAD_SCRIPT;    // This halts the script!
            break;  
    }

    return( retval );
}

/******************************************************************************
   FUNCTION NAME: fnSwitchAuditScript
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : choose a create device audit script, audit with or without
                : the piece count.
   PARAMETERS   : Default subscript to run.
   RETURN       : Script ID of actual subscript to run.
   NOTES        : none
******************************************************************************/
UINT16 fnSwitchAuditScript( UINT16 msgID )
{
    UINT16  retval = msgID; // by default do the script passed from Orbit record

    if( fnFlashGetIBByteParm(IBP_PC_IN_AUDIT) > 0 )
    {
        retval = SCRIPT_CREATE_DEV_AUDIT_W_PC;
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnBobEarlyReply
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : In special cases, we want to reply to the Intertask message, 
                 and THEN finish the script.
   PARAMETERS   :   msgID, ushort (Device MsgID, OR Subscript ID)
                    msgStat, BOOL - BOB_OK if the Device Reply was fine
                                  or BOB_SICK.
   RETURN       : bob's disposition, same as msgStat.
   NOTES        : The bobReply() function sets a flag so it won't reply a second 
                  time when the script ends. 
******************************************************************************/
char fnBobEarlyReply( ushort msgID, BOOL msgStat )
{
    // Reply to the calling routine, THEN continue the script (if BOB_OK)
    bobReply( bobsCommand.bSource, bobsCommand.bMsgId, msgStat );
    return( msgStat );
}

/******************************************************************************
   FUNCTION NAME: fnDuckSwissProductText
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Figure out if should duck the Swiss Indicia Product Text.
                : We should duck it if we are in predate mode
                :  OR we are in MPE mode
                :  OR the postage is zero AND BP_ZERO_PRINT_INDICIA = 1
   PARAMETERS   : None
   RETURN       : True if it should be ducked, False otherwise
   NOTES    : none
******************************************************************************/
BOOL fnDuckSwissProductText(void)
{
    BOOL fRetVal = FALSE;
    ulong   ulCurrentPostageVal = 0;

    memcpy(&ulCurrentPostageVal, bobsPostageValue, sizeof(ulong));

    if( (fnFlashGetByteParm(BP_ZERO_PRINT_INDICIA) == ZERO_INDICIA_FEATURE_SWISS &&
         ulCurrentPostageVal == 0) )
    {
        fRetVal = TRUE;
    }
    return(fRetVal);
}


/******************************************************************************
   FUNCTION NAME: fnSwitchGetAllRegsScript
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Checks the rev of the i-button and if it is 9.0 or greater,
                  then it redirects to SCRIPT_IPSD_GET_REGS_ALL.
   PARAMETERS   : Default subscript to run.
   RETURN       : The actual subscript to run.
   NOTES        : 
      If ibutton < 9.0: The script passed in should be:
         if getting piece count:  SCRIPT_IPSD_GET_PSD_PARAMS
         if getting all reg info: SCRIPT_IPSD_GET_ALL_REGS_PRE9

    This is the same as Janus's function fnSwitchPieceCountScript().
******************************************************************************/
BOOL X_NEW_PIECE_COUNT_METHOD = TRUE;   // This means do it the short way if we have a 9.0

UINT16 fnSwitchGetAllRegsScript( UINT16 msgID )
{
    UINT16 retval = msgID; // by default do the script passed from Orbit record

    if( X_NEW_PIECE_COUNT_METHOD == TRUE )
    {
        if( lIPSD_FirmwareVerNum >= 0x00090000 )
        {
            retval = SCRIPT_IPSD_GET_REGS_ALL;
        }
    }

    return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnAddMultiByteBuffers
   AUTHOR       : Sandra Peterson
   DESCRIPTION  : Add two multi-byte buffers together and put the result in a
                  given destination buffer.

   PARAMETERS   : pucNum1 = pointer to the buffer w/ the first number to add
                  ucNum1Bytes = number of bytes in the first number buffer
                  pucNum2 = pointer to the buffer w/ the second number to add
                  ucNum2Bytes = number of bytes in the second number buffer
                  pucDest = pointer to the buffer that is to hold the sum
                  ucDestBytes = number of bytes in the sum buffer

   RETURN       : TRUE if the addition was done, FALSE otherwise

   NOTES        : It is up to the calling routine to make sure the destination
                  buffer is properly initialized.
                  ucDestBytes should be >= ucNum1Bytes and >= ucNum2Bytes.
                  ucNum1Bytes, ucNum2Bytes & ucDestBytes must be non-zero.
******************************************************************************/
BOOL fnAddMultiByteBuffers(const unsigned char *pucNum1, const unsigned char ucNum1Bytes,
                            const unsigned char *pucNum2, const unsigned char ucNum2Bytes,
                            unsigned char *pucDest, const unsigned char ucDestBytes)
{
    unsigned short usSum;    
    const unsigned char *pucLeftOver;
    unsigned char ucInx1, ucInx2, ucInx3, ucCarry = 0;
    BOOL    retValue = FALSE;



    if (pucNum1 && pucNum2 && pucDest && ucNum1Bytes && ucNum2Bytes && ucDestBytes)
    {
        // all the pointers are non-null and all the bytes counts are non-zero.
        // add the two numbers together until you run out of bytes for at least one of them
        for (ucInx1 = ucNum1Bytes, ucInx2 = ucNum2Bytes, ucInx3 = ucDestBytes;
                ucInx1 && ucInx2 && ucInx3; )
        {
            usSum = pucNum1[--ucInx1] + pucNum2[--ucInx2] + ucCarry;
            pucDest[--ucInx3] = usSum & 0xFF;

            if (usSum > 0xFF)
                ucCarry = 1;
            else
                ucCarry = 0;
        }

        // if there are still bytes in the sum buffer and still bytes in one of the input
        // buffers, continue w/ the addition in order to get all the bytes in the sum buffer.
        if (ucInx3 && (ucInx1 || ucInx2))
        {
            // determine which buffer still has bytes and set up the pointer & byte
            // count appropriately.
            if (ucInx1)
            {
                pucLeftOver = pucNum1;
            }
            else
            {
                ucInx1 = ucInx2;
                pucLeftOver = pucNum2;
            }

            // while still bytes to process, add in any carry.
            while (ucInx1 && ucInx3)
            {
                usSum = pucLeftOver[--ucInx1] + ucCarry;
                pucDest[--ucInx3] = usSum & 0xFF;

                if (usSum > 0xFF)
                    ucCarry = 1;
                else
                    ucCarry = 0;
            }
        }

        retValue = TRUE;
    }

    return (retValue);
}


/******************************************************************************
   FUNCTION NAME: spMultiply
   AUTHOR       : Sandra Peterson
   DESCRIPTION  : Multiply two unsigned longs together and put the result in a
                  given destination buffer.

                    Ref: Arbitrary Precision Computation
                    http://numbers.computation.free.fr/Constants/constants.html

                         high    p1                p0     low
                        +--------+--------+--------+--------+
                        |      x1*y1      |      x0*y0      |
                        +--------+--------+--------+--------+
                               +-+--------+--------+
                               |1| (x0*y1 + x1*y1) |
                               +-+--------+--------+
                                ^carry from adding (x0*y1+x1*y1) together
                                        +-+
                                        |1|< carry from adding LOHALF t
                                        +-+  to high half of p0

   PARAMETERS   : 
                  p = pointer an array of 2 unsigned longs
                  x = multiplicand
                  y = multiplier

   RETURN       : Nothing
   NOTES        : 
******************************************************************************/
#define BITS_PER_DIGIT 32
#define BITS_PER_HALF_DIGIT (BITS_PER_DIGIT / 2)

#define MAX_HALF_DIGIT 0xFFFF

#define LOHALF(x) ((unsigned long)((x) & MAX_HALF_DIGIT))
#define HIHALF(x) ((unsigned long)((x) >> BITS_PER_HALF_DIGIT & MAX_HALF_DIGIT))
#define TOHIGH(x) ((unsigned long)((x) << BITS_PER_HALF_DIGIT))

void spMultiply(unsigned long *p, unsigned long x, unsigned long y)
{    
    unsigned long ulLowWord1, ulLowWord2, ulHighWord1, ulHighWord2;
    unsigned long t, u, ulCarry;


    /*    Split each x,y into two halves
        x = x0 + B*x1
        y = y0 + B*y1
        where B = 2^16, half the digit size
        Product is
        xy = x0y0 + B(x0y1 + x1y0) + B^2(x1y1)
    */

    ulLowWord1 = LOHALF(x);
    ulHighWord1 = HIHALF(x);
    ulLowWord2 = LOHALF(y);
    ulHighWord2 = HIHALF(y);

    /* Calc low part - no carry */
    p[0] = ulLowWord1 * ulLowWord2;

    /* Calc middle part */
    t = ulLowWord1 * ulHighWord2;
    u = ulHighWord1 * ulLowWord2;
    t += u;
    if (t < u)
        ulCarry = 1;
    else
        ulCarry = 0;

    /*    This carry will go to high half of p[1]
        + high half of t into low half of p[1] */
    ulCarry = TOHIGH(ulCarry) + HIHALF(t);

    /* Add low half of t to high half of p[0] */
    t = TOHIGH(t);
    p[0] += t;
    if (p[0] < t)
        ulCarry++;

    p[1] = ulHighWord1 * ulHighWord2;
    p[1] += ulCarry;

    return;
}


/******************************************************************************
   FUNCTION NAME: fnMultiplyMultiByteBuffers
   AUTHOR       : Sandra Peterson
   DESCRIPTION  : Multiply two multi-byte buffers together and put the result in a
                  given destination buffer.  The data from the input buffers are
                  moved to local buffer unions TempBuf1 & TempBuf2.  The data from the
                  resulting product is moved from local buffer union TempProduct to the
                  given destination buffer.

    Computes product ulProduct = ulTemp1 * ulTemp2
    where ulTemp1, ulTemp2 are unsigned long arrays of 5 entries
    and ulProduct is a unsigned long array of 10 entries

    Ref: Knuth Vol 2 Ch 4.3.1 p 268 Algorithm M.

   PARAMETERS   : pucNum1 = pointer to the buffer w/ the multiplier
                  ucNum1Bytes = number of bytes in the multiplier
                  pucNum2 = pointer to the buffer w/ the multiplicand
                  ucNum2Bytes = number of bytes in the multiplicand
                  pucDest = pointer to the buffer that is to hold the product
                  ucDestBytes = number of bytes in the product buffer

   RETURN       : TRUE if the multiplication was done, FALSE otherwise

   NOTES        : ucNum1Bytes, ucNum2Bytes & ucDestBytes must be < 21.
******************************************************************************/
BOOL fnMultiplyMultiByteBuffers(const unsigned char *pucNum1, const unsigned char ucNum1Bytes,
                            const unsigned char *pucNum2, const unsigned char ucNum2Bytes,
                            unsigned char *pucDest, const unsigned char ucDestBytes)
{
    union
    {
        unsigned long ulTemp1[5];
        unsigned char ucTemp1[20];
    } TempBuf1;

    union
    {
        unsigned long ulTemp2[5];
        unsigned char ucTemp2[20];
    } TempBuf2;

    union
    {
        unsigned long ulProduct[10];
        unsigned char ucProduct[40];
    } TempProduct;

    unsigned long ulTemp3[2], ulCarry;
    unsigned char ucInx1, ucInx2, ucInx3;
    BOOL    retValue = FALSE;



    if (pucNum1 && pucNum2 && pucDest && ucNum1Bytes && ucNum2Bytes && ucDestBytes &&
        (ucNum1Bytes < 21) && (ucNum2Bytes < 21) && (ucDestBytes < 21)  &&
        (ucNum1Bytes <= ucDestBytes) && (ucNum2Bytes <= ucDestBytes))
    {
        // all the pointers are non-null and all the bytes counts are non-zero.
        // multiply the two numbers together.

        ucInx3 = (ucDestBytes + 3) / 4;

        /* Step M1. Initialise */
        (void)memset(TempBuf1.ucTemp1, 0, 20);
        (void)memcpy(&TempBuf1.ucTemp1[20-ucNum1Bytes], pucNum1, ucNum1Bytes);


        (void)memset(TempBuf2.ucTemp2, 0, 20);
        (void)memcpy(&TempBuf2.ucTemp2[20-ucNum2Bytes], pucNum2, ucNum2Bytes);

        (void)memset(TempProduct.ucProduct, 0, 40);

        for (ucInx1 = 5; ucInx1 > 5-ucInx3; ucInx1--)
        {
            /* Step M2. Zero multiplier? */
            if (TempBuf1.ulTemp1[ucInx1 - 1] == 0)
            {
                TempProduct.ulProduct[ucInx1 - 1] = 0;
            }
            else
            {
                /* Step M3. Initialise ucInx2 */
                ulCarry = 0;
                for (ucInx2 = 5; ucInx2 > 5-ucInx3; ucInx2--)
                {
                    /* Step M4. Multiply and add */
                    /* ulTemp3 = ulTemp2_ucInx2 * ulTemp1_ucInx1 + TempProduct.ulProduct_(ucInx1+ucInx2) + ulCarry */
                    spMultiply(ulTemp3, TempBuf2.ulTemp2[ucInx2 - 1], TempBuf1.ulTemp1[ucInx1 - 1]);

                    ulTemp3[0] += ulCarry;
                    if (ulTemp3[0] < ulCarry)
                        ulTemp3[1]++;

                    ulTemp3[0] += TempProduct.ulProduct[ucInx1 + ucInx2 - 1];
                    if (ulTemp3[0] < TempProduct.ulProduct[ucInx1 + ucInx2 - 1])
                        ulTemp3[1]++;

                    TempProduct.ulProduct[ucInx1 + ucInx2 - 1] = ulTemp3[0];
                    ulCarry = ulTemp3[1];
                }    /* Step M5. Loop on ucInx2 */

                /* set ulProduct_(ucInx1+ucInx3) = ulCarry */
                TempProduct.ulProduct[ucInx1 - 1] = ulCarry;
            }
        }    /* Step M6. Loop on ucInx1 */

        (void)memcpy(pucDest, &TempProduct.ucProduct[40-ucDestBytes], ucDestBytes);
        retValue = TRUE;
    }

    return (retValue);
}


/******************************************************************************
   FUNCTION NAME: fnGetSwedenPackedData
   AUTHOR       : Sandra Peterson
   DESCRIPTION  : Generates the packed data for the Sweden 2D barcode.

   PARAMETERS   : pucDest = pointer to the 19-byte buffer that is to hold the data.
                  fBeforeDebit = TRUE if this function is being called before
                  the debit is done (e.g., when generating the data for the
                  Flex Debit). FALSE if this function is being called after
                  the debit is done (e.g., when generating the modified data
                  for the 2D barcode.)

   RETURN       : TRUE if the data could be obtained, FALSE otherwise
   NOTES        :
******************************************************************************/
#define MULTIPLY_1_BILLION    1000000000
#define MULTIPLY_10_MILLION    10000000
#define POST_CODE_LEN        5
#define    STOPDEL_CODE_LEN    7
#define    POSTSTOPDEL_CODE_LEN    (POST_CODE_LEN+STOPDEL_CODE_LEN)


BOOL fnGetSwedenPackedData(const unsigned char *pucDest, const BOOL fBeforeDebit)
{
    DATETIME        sDateTime;
    double            dFunds;
    char            *pcString;
    unsigned long    ulTempLong;
    unsigned short    usTempShort;
    unsigned char     ucPackedData[19];
    unsigned char    ucTemp5Char[5];
    unsigned char     ucMultiply1Trillion[5] = {0xE8, 0xD4, 0xA5, 0x10, 0x00};
    unsigned char    ucTempChar;
    unsigned char    ucNumDecimals = fnFlashGetIBByteParm(IBP_DECIMAL_PLACES);
    char            cPostStopDelivery[POSTSTOPDEL_CODE_LEN+1]= {0};
    BOOL    retValue = FALSE, status;


    if (pucDest)
    {
        (void)memset(ucPackedData, 0, 19);

        // copy the binary equivalent of the 6 LSBs of the indicia serial number into the packed data
        ucTempChar = (unsigned char)strlen((char *)pIPSD_indiciaSN);
        if (ucTempChar > 6)
            ucTempChar -= 6;
        else
            ucTempChar = 0;

        ulTempLong = (unsigned long)atol((char *)&pIPSD_indiciaSN[ucTempChar]);
        (void)memcpy(&ucPackedData[15], (unsigned char *)&ulTempLong, 4);

        // multiply by 1,000,000,000,000 to make room for the postcode/stoppoint/deliverypoint data
        status = fnMultiplyMultiByteBuffers(ucMultiply1Trillion, 5, ucPackedData, 19, ucPackedData, 19);

        // if OK, add in the postcode/stoppoint/deliverypoint data
        if (status == TRUE)
        {
            // need to convert to a 5-byte binary number
            dFunds = 0;
            (void)memcpy( cPostStopDelivery, &pIPSD_zipCode[0], POST_CODE_LEN);
            (void)memcpy( &cPostStopDelivery[POST_CODE_LEN], &pIPSD_zipCode[6], STOPDEL_CODE_LEN);
            pcString = cPostStopDelivery;
            for (ucTempChar = 0; ucTempChar < POSTSTOPDEL_CODE_LEN; ucTempChar++)
            {
                dFunds *= 10;
                dFunds += pcString[ucTempChar] - '0';
            }

            fnDouble2BinFive(ucTemp5Char, dFunds, TRUE);
            status = fnAddMultiByteBuffers (ucTemp5Char, 5, ucPackedData, 19, ucPackedData, 19);
        }

        // if OK, multiply by 3660 to make room for mailing date
        if (status == TRUE)
        {
            usTempShort = 3660;
            status = fnMultiplyMultiByteBuffers((unsigned char *)&usTempShort, 2, ucPackedData, 19, ucPackedData, 19);
        }

        // if OK, add in mailing date
        if (status == TRUE)
        {
            // need to convert
            if (GetPrintedDate(&sDateTime, GREGORIAN) == SUCCESSFUL)
            {
                usTempShort = fnDayOfYear(&sDateTime) - 1;
                usTempShort = (unsigned short)((usTempShort * 10) + (sDateTime.bYear % 10));
            }

            else
                usTempShort = 0;

            status = fnAddMultiByteBuffers ((unsigned char *)&usTempShort, 2, ucPackedData, 19, ucPackedData, 19);
        }

        // if OK, multiply by 1,000,000,000 to make room for AR
        if (status == TRUE)
        {
            ulTempLong = MULTIPLY_1_BILLION;
            status = fnMultiplyMultiByteBuffers((unsigned char *)&ulTempLong, 4, ucPackedData, 19, ucPackedData, 19);
        }

        // if OK, add in AR
        if (status == TRUE)
        {
            (void)memcpy(ucTemp5Char, m5IPSD_ascReg, sizeof(ucTemp5Char));

            if (fBeforeDebit == TRUE)
            {
                // add in the postage for the piece about to be done
                status = fnAddMultiByteBuffers (bobsPostageValue, 4, ucTemp5Char, 5, ucTemp5Char, 5);
            }

            if (status == TRUE)
            {
                dFunds = fnBinFive2Double(ucTemp5Char);

                if (ucNumDecimals == 3)
                {
                    dFunds /= 10;                // change from 3 dec places to 2 dec places
                }

                // make sure the AR is no more than 9 digits
                while (dFunds > MULTIPLY_1_BILLION)
                    dFunds -= MULTIPLY_1_BILLION;

                fnDouble2BinFive(ucTemp5Char, dFunds, TRUE);
                status = fnAddMultiByteBuffers (&ucTemp5Char[1], 4, ucPackedData, 19, ucPackedData, 19);
            }
        }

        // if OK, multiply by 10,000,000 to make room for piece count
        if (status == TRUE)
        {
            ulTempLong = MULTIPLY_10_MILLION;
            status = fnMultiplyMultiByteBuffers((unsigned char *)&ulTempLong, 4, ucPackedData, 19, ucPackedData, 19);
        }

        // if OK, add in piece count
        if (status == TRUE)
        {
            (void)memcpy(&ulTempLong, pIPSD_pieceCount, sizeof(ulTempLong));

            if (fBeforeDebit == TRUE)
            {
                // add in the piece about to be done
                ulTempLong++;
            }

            // make sure the actual piece count value isn't too big
            ulTempLong %= MULTIPLY_10_MILLION;
            status = fnAddMultiByteBuffers ((unsigned char *)&ulTempLong, 4, ucPackedData, 19, ucPackedData, 19);
        }

        // if OK, multiply by 512 to make room for postal service product code
        if (status == TRUE)
        {
            usTempShort = 512;
            status = fnMultiplyMultiByteBuffers((unsigned char *)&usTempShort, 2, ucPackedData, 19, ucPackedData, 19);
        }

        // if OK, add in postal service product code
        if (status == TRUE)
        {
            usTempShort = 0;        // clear just in case we can't get the data

            status = fnAddMultiByteBuffers ((unsigned char *)&usTempShort, 2, ucPackedData, 19, ucPackedData, 19);
        }

        // if OK, multiply by 2 to make room for currency type
        if (status == TRUE)
        {
            ucTempChar = 2;
            status = fnMultiplyMultiByteBuffers(&ucTempChar, 1, ucPackedData, 19, ucPackedData, 19);
        }

        // if OK, add in currency type
        if (status == TRUE)
        {
            status = fnAddMultiByteBuffers (&cmosEuroConversion.ucCurrencyID, 1, ucPackedData, 19, ucPackedData, 19);
        }

        // if OK, multiply by 100,000 to make room for postage value
        if (status == TRUE)
        {
            ulTempLong = 100000;
            status = fnMultiplyMultiByteBuffers((unsigned char *)&ulTempLong, 4, ucPackedData, 19, ucPackedData, 19);
        }

        // if OK, add in postage value
        if (status == TRUE)
        {
            (void)memcpy(&ulTempLong, bobsPostageValue, sizeof(ulTempLong));

            if (ucNumDecimals == 3)
            {
                ulTempLong /= 10;                // change from 3 dec places to 2 dec places
            }

            status = fnAddMultiByteBuffers ((unsigned char *)&ulTempLong, 4, ucPackedData, 19, ucPackedData, 19);
        }

        // if OK, copy packed data to destination buffer
        if (status == TRUE)
        {
            (void)memcpy((unsigned char *)pucDest, ucPackedData, 19);
            retValue = TRUE;
        }
    }

    return (retValue);
}

//****************************************************************************
// FUNCTION NAME:   fnBobCheckGBL_CmdInCurrProgMode
// DESCRIPTION:   
//    Check if the current Boot Loader command is allowed in the current ProgMode
// ARGUMENTS:   
//      ubCmd - Boot Loader command.
//      ubProgMode - Current IPSD ProgMode.
// RETURN:   
//      Bobs Error Code
//
// NOTES:
// MODS:
//  2009.03.19 Clarisa Bellamy - Initial revision, added for Gemin Boot Loader 
//                      support.   
//-----------------------------------------------------------------------------
UINT8 fnBobCheckGBL_CmdInCurrProgMode( UINT8 ubCmd, UINT8 ubProgMode )
{
    UINT8   fRetVal = JMEGABOB_STATE_OK;    //OK

    switch( ubProgMode )
    {
        case IPSD_MODE_GEMINI_UNKNOWN:  // Unkown mode, try boot loader messages.
        case IPSD_MODE_BOOT_LOADER:     
            // Allow all except these two.  We should go to application mode
            //  and test it first.
            if(   (ubCmd == IBTN_BLCMD_DISABLE_BOOT_LOADER)
               || (ubCmd == IBTN_BLCMD_SET_SECURITY_LOCK) )
            {
                fRetVal = BOB_IPSD_BL_TOO_RISKY;
            }
            break;

        // In the App mode, but haven't passed the self tests yet.
        case IPSD_MODE_UNTESTEDAPP:
            // These are allowed by Gemini, but not by us.     
            if(   (ubCmd == IBTN_BLCMD_DISABLE_BOOT_LOADER)
               || (ubCmd == IBTN_BLCMD_SET_SECURITY_LOCK) )
            {
                // Won't allow lock out of any kind until the self tests pass.
                fRetVal = BOB_IPSD_BL_TOO_RISKY;
            }
            else 
            {
                // Only these two are allowed here.
                if(   (ubCmd != IBTN_BLCMD_READ_MICRO_STATUS)
                   && (ubCmd != IBTN_BLCMD_EXIT_APP_MODE_OR_FACTORY) )
                {
                    // If it's not one of the good ones, return bad mode for cmd
                    fRetVal = BOB_BL_CMD_2_GEMINI_IN_APP;
                }
            }
            break;

        // In the App mode, and passed selftests
        case IPSD_MODE_APPLICATION:     
            // Allow only these 4.
            if(   (ubCmd != IBTN_BLCMD_READ_MICRO_STATUS)
               && (ubCmd != IBTN_BLCMD_SET_SECURITY_LOCK)
               && (ubCmd != IBTN_BLCMD_DISABLE_BOOT_LOADER)
               && (ubCmd != IBTN_BLCMD_EXIT_APP_MODE_OR_FACTORY) )
            {
                // If it's not one of the allowed ones, bad mode for cmd.
                fRetVal = BOB_BL_CMD_2_GEMINI_IN_APP;
            }
            break;

        case IPSD_MODE_SECURE_APP:      
            // From here, we can only do this one.
            if( ubCmd != IBTN_BLCMD_READ_MICRO_STATUS )
            {
                // If not an allowed message, bad mode for cmd.
                fRetVal = BOB_BL_CMD_2_GEMINI_IN_APP;
            }
            break;


        // In these modes, NO boot loader messages are supported.
        case IPSD_MODE_ASTEROID:
        case IPSD_MODE_UNKNOWN:     // Don't know the family code, treat it like an Asteroid.
            {
                // Asteroids don't respond to BL messages.
                fRetVal = BOB_IPSD_BL_2_ASTEROID;
            }
            break;
             
        case IPSD_MODE_NONE:        // Cannot talk to IPSD.
        default:
                fRetVal = BOB_IPSD_BL_EDM_ERR;
                break;

    }
    return( fRetVal );
}


// ***************************************************************************
// FUNCTION NAME:        fnPreGBLPassthroughFromEDM
// PURPOSE:          
//   Premessage function, run before we send a passthrough message from the 
//   EDM to the Gemini Boot Loader.
//   Checks the progMode and returns BOB_SICK if trying to send a Boot Loader 
//    command to an ASTEROID i-button or to one that we cannot talk to.
//    Returns BOB_SICK if sending a Boot Loader command to a Gemini that is
//    not in Boot Loader Mode, unless it is one of the commands that can be
//    handled by the app modes.
// INPUTS:           
//       msgID of message to send.
// OUTPUT:
//       BOB_OK
// MODS:
// 2009.03.11 Clarisa Bellamy - New function                  
//-----------------------------------------------------------------------------
char fnPreGBLPassthroughFromEDM( ushort bMsgID )
{   
    char    retVal = BOB_OK;
    UINT8   ubCmd;
    UINT16  ubLen;
    UINT8   ubBobErrorCode;

    // Set Bob's local Xfr ptr to point to the EDM structure by grabbing the 
    //  pointer that the EDM task loaded with fnWriteData.
    localXferCrtlStructPtr = edmXferStructPtr;
    ubCmd = localXferCrtlStructPtr->ptrToDataToSendToDevice[0];
    ubLen = localXferCrtlStructPtr->bytesInSource;
    // Record the message command and length:
    sprintf( pBobsLogScratchPad, 
             "GBL msg from EDM: cmd= %x, len= %d", ubCmd, ubLen  );
    fnDumpStringToSystemLog( pBobsLogScratchPad );
    ubBobErrorCode = fnBobCheckGBL_CmdInCurrProgMode( ubCmd, bIPSD_ProgMode );
    if( ubBobErrorCode )
    {
        fnSetBobInternalErr( ubBobErrorCode );
        fnLogSystemError( ERROR_CLASS_JBOB_TASK, BOB_IPSD_BL_2_ASTEROID, ubCmd, bIPSD_ProgMode, 0 );
        retVal = BOB_SICK;

        sprintf( pBobsLogScratchPad, 
                "Error returned to EDM %x%x", (UINT)ERROR_CLASS_JBOB_TASK, (UINT)megabobStat  );
        fnDumpStringToSystemLog( pBobsLogScratchPad );
        if( megabobStat[0] || megabobStat[1] )
        {
            // If we are posting an error, make sure that the caller gets it.
            if(   (localXferCrtlStructPtr->deviceReplyStatus[0] == 0)
               && (localXferCrtlStructPtr->deviceReplyStatus[1] == 0)
               && (localXferCrtlStructPtr->deviceReplyStatus[2] == 0)
               && (localXferCrtlStructPtr->deviceReplyStatus[3] == 0) )
            {
                // If the i-button returned an error it will be in ->deviceReplyStatus[3]
                //  because it is only 1-byte long.  So post a JBOB error in the first two bytes.
                localXferCrtlStructPtr->deviceReplyStatus[0] = ERROR_CLASS_JBOB_TASK;
                localXferCrtlStructPtr->deviceReplyStatus[1] = megabobStat[0] | megabobStat[1];
            }
        }
    }

    return( retVal );
}

// ***************************************************************************
// FUNCTION NAME:        fnPostGBLPassthroughFromEDM
// PURPOSE:          
//   Postmessage function, run after we send a passthrough message from the 
//   EDM to the Gemini Boot Loader.  
//  1. Puts a message in the System Log with the reply code.
//  2. If there was an error returned from the Gemini Boot Loader, but the send 
//      and reply worked correctly, return BOB_OK instead of BOB_SICK, and let 
//      the EDM deal with the error code.
//  3. If there was a BOBtask error, put it into the deviceReplyStatus of the xfr
//      struct so that the EDM task can include it in the Extended Parameter 
//      Acknowledgement message sent back to PCT.
//
// INPUTS:           
//   msgID and Status of pre-function and message sent.
// OUTPUT:
// NOTES:
// MODS:
// 2009.03.11 Clarisa Bellamy - New function 
// 2009.06.15 Clarisa Bellamy - Remove code that creates and sends the RunSelfTests
//                  message after the EnterAppMode is sent, because now we have a 
//                  way for the EDM to send it.                 
//-----------------------------------------------------------------------------
char fnPostGBLPassthroughFromEDM(  ushort msgID, BOOL msgStat )
{
    UINT8   ubCmd;
    UINT8   ubCode; 

    ubCmd = localXferCrtlStructPtr->ptrToCallersDestBuffer[0];
    ubCode = localXferCrtlStructPtr->ptrToCallersDestBuffer[1];

    // Record the message reply code:
    sprintf( pBobsLogScratchPad, 
             "Gemini BL msg reply to EDM: cmd= %02X, code= %02X", ubCmd, ubCode ); 
    fnDumpStringToSystemLog( pBobsLogScratchPad );

    // If we had a problem...
    if( msgStat != BOB_OK )
    {
        // If the i-button returned an error, it will be in ->deviceReplyStatus[3]
        //  because it is only 1-byte long.  
        if(   (localXferCrtlStructPtr->deviceReplyStatus[0] == 0)
           && (localXferCrtlStructPtr->deviceReplyStatus[1] == 0)
           && (localXferCrtlStructPtr->deviceReplyStatus[2] == 0)
           && (localXferCrtlStructPtr->deviceReplyStatus[3] != 0) )
        {
            // If we have an error from the Gemini, then we want to return BOB_OK
            //  to the EDM, that way, it will repsond with the MFG_BOOT_LOADER_RSP
            //  and the status code from the I-button.
            // BOB_EXIT_SUBSCRIPT will set lower_stat to BOB_OK so that the 
            //  calling EDM function will return the data in deviceReplyStatus.
            // And also, we don't have to check the MicroStatus, because whatever the
            //  message was, it failed.
            msgStat = BOB_EXIT_SUBSCRIPT;    
        }
        else
        {
            // If we are posting a JBOB error, make sure that the caller gets it in the
            //  deviceReplyStatus of the xfer control structue.
            if( megabobStat[0] || megabobStat[1] )
            {
                localXferCrtlStructPtr->deviceReplyStatus[2] = ERROR_CLASS_JBOB_TASK;
                localXferCrtlStructPtr->deviceReplyStatus[3] = megabobStat[0] | megabobStat[1];
                // This causes the EDM reply to be Extended Parameter Ack with
                //  the error code, instead of MFG_BOOT_LOADER_RSP.
                msgStat = BOB_SICK;
            }
        }
    }
    else // No error!
    {
        // Check if the message may have changed the Gemini ProgMode.  
        switch( ubCmd )
        {
            case IBTN_BLCMD_ENTER_APPLICATION_MODE:
            {
                bIPSD_ProgMode = IPSD_MODE_UNTESTEDAPP;
                fGetMicroStatusAgain = TRUE;
                break;
            }

            // These messages my change the Gemini mode, so set the flag that 
            //  will cause this script to resend the GetMicroStatus msg and 
            //  update ProgMode.
            case IBTN_BLCMD_EXIT_APP_MODE_OR_FACTORY:
            case IBTN_BLCMD_SET_SECURITY_LOCK:
            case IBTN_BLCMD_DISABLE_BOOT_LOADER:
                fGetMicroStatusAgain = TRUE;
                break;

            default:
                break;
        
        }

    }
    return( BOB_OK );    
}


// ***************************************************************************
// FUNCTION NAME:        fnIfGetMicroStatusAgain
// PURPOSE:          
//   Premessage function, called in script SCRIPT_GBL_PASSTHROUGH_EDM, after
//  the message has been sent and responded to, to see if we need to check 
//  the Micro Status (Boot loader or Application mode.)   It checks the flag
//  fGetMicroStatusAgain 
// 
// INPUTS:           
//       msgID of message to send.
// OUTPUT:
//       If the flag is set, return BOB_OK, which will to send the GetMicroStatus
//          message and update the ProgMode. 
//       else skip the GetMicroStatus message.
// MODS:
// 2009.03.23 Clarisa Bellamy - New function                  
//-----------------------------------------------------------------------------
char fnIfGetMicroStatusAgain( ushort bMsgID )
{
    if( fGetMicroStatusAgain == TRUE )
    {
        fGetMicroStatusAgain = FALSE;
        return( BOB_OK );
    }
    else 
    {
        return( BOB_SKIP );   
    }
}


// ***************************************************************************
// FUNCTION NAME:        fnPreRunSelfTests
// PURPOSE:          
//   Premessage function, called before we send the RunSelfTests application
//   message to the IPSD.
// INPUTS:           
//       msgID of message to send.
// OUTPUT:
//       BOB_OK
// MODS:
// 2009.03.23 Clarisa Bellamy - New function                  
//-----------------------------------------------------------------------------
char fnPreRunSelfTests( ushort bMsgID )
{
    sprintf( pBobsLogScratchPad, "fnPreRunSelfTests: ProgMode= %d", bIPSD_ProgMode  );
    fnDumpStringToSystemLog( pBobsLogScratchPad );
	fBobUberDebugDevMessage = TRUE;
  
    // Default the Command Status Response
    pIPSD_4ByteCmdStatusResp[ 0 ] = 0x00;
    pIPSD_4ByteCmdStatusResp[ 1 ] = 0x00;
    pIPSD_4ByteCmdStatusResp[ 2 ] = 0x00;
    pIPSD_4ByteCmdStatusResp[ 3 ] = 0x00;

	pIPSD_cmdStatusResp[0] = pIPSD_cmdStatusResp[1] = 0;
	megabobStat[0] = megabobStat[1] = 0;

    // We can only run the self tests, if the APP is running.
    // Don't bother if in secure App mode, since we can't do anything about it.
    if(   (bIPSD_ProgMode == IPSD_MODE_UNTESTEDAPP) 
       || (bIPSD_ProgMode == IPSD_MODE_APPLICATION) )
    {
        return( BOB_OK );
    }
    // If the state is 1 or greater, then we will fake the selftests.
    //  Otherwise indicate that it didn't run.
    //  If we are in the process of initializing, then the data will be overwritten
    //  soon enough.
    // Default the Command Status Response
    pIPSD_4ByteCmdStatusResp[ 0 ] = 0x90;
    pIPSD_4ByteCmdStatusResp[ 1 ] = 0x01;
    pIPSD_4ByteCmdStatusResp[ 2 ] = 0x00;
    pIPSD_4ByteCmdStatusResp[ 3 ] = bIPSD_ProgMode;
    return( BOB_SKIP );   
}

// ***************************************************************************
// FUNCTION NAME:        fnPostRunSelfTests
// PURPOSE:          
//   Postmessage function, called after we send the RunSelfTests application
//    message to the IPSD.
//   If the self tests are successful, then change the ProgMode from UNTESTEDAPP 
//    to APPLICATION, and exit the subscript to continue the calling script as normal.
//   If there was an error, the error should be posted, but we want the remainder
//    of the script to run.  Which has the following results:
//    1. If this is called from the SCRIPT_CHECK_GEMINI, then the rest of the script
//      will send messages to the Boot Loader to try to exit the application mode.  
//    2. If it is called from the SCRIPT_EDM_IPSD_RUN_SELF_TESTS script, then there
//      isn't anymore to the script, and fnValidData( ) returns BOB_SICK.
//
// INPUTS:           
//   msgID and Status of pre-function and message sent.
// OUTPUT:
//   BOB_EXIT_SUBSCRIPT - If the SelfTests were successful.
//   BOB_ALLOW_BUT_POST - If there was an error in the selftests, then post the 
//                        error, but allow the subscript to continue, because it
//                        should exit the application mode so that the I-button
//                        can be reprogrammed.
//  
// NOTES:
// MODS:
// 2009.03.11 Clarisa Bellamy - New function                  
// 2009.06.15 Clarisa Bellamy - Modified after exception-handling testing, when 
//                           the Gemini initialization was separated out into 
//                           its own subscript.                  
//-----------------------------------------------------------------------------
char fnPostRunSelfTests( ushort msgID, BOOL msgStat )
{
    // This is for debugging return status. 
    sprintf( pBobsLogScratchPad, "fnPostRunSelfTests: msgStat= %d   MegaBobStat= %02X%02X, CmdStat= %02X%02X", 
    		 msgStat, megabobStat[0], megabobStat[1], pIPSD_cmdStatusResp[0], pIPSD_cmdStatusResp[1]  );
    fnDumpStringToSystemLog( pBobsLogScratchPad );

	// Make sure this is turned off, or the syslog will have LOTS of extra stuff 
	// in it that is not usually helpful.
	fBobUberDebugDevMessage = FALSE;

    if( msgStat == BOB_OK )
    {
        // If the selftests passed, then the App is now Tested, so change the 
        //  ProgMode.  
        if( bIPSD_ProgMode == IPSD_MODE_UNTESTEDAPP ) 
        {
            bIPSD_ProgMode = IPSD_MODE_APPLICATION;
        }
        // If this is the SCRIPT_CHECK_GEMINI, we exit to continue the normal
        //  SCRIPT_INIT_IPSD.
        msgStat = BOB_EXIT_SUBSCRIPT;
    }
    else // The IPSD must not have passed the tests
    {   
		// Leave the mode as is.  If this script is called from the Gemini
		//  init function, later scripts will try to reset the I-button if 
		//  appropriate. 

        // Copy any bobError to the first 2 bytes of the 4Byte status.
		// 0000 is no error. 
        memcpy( pIPSD_4ByteCmdStatusResp, megabobStat, sizeof( megabobStat ) );
		// Copy any error returned from the PSD into the last 2 bytes of 
		//  the 4-byte status.
		memcpy( &pIPSD_4ByteCmdStatusResp[2], pIPSD_cmdStatusResp, 2 );

        if( (pIPSD_cmdStatusResp[0] | pIPSD_cmdStatusResp[1]) != 0 )
        {
            // Something was returned from the ibutton. 
            msgStat = BOB_ALLOW_BUT_POST;
        }
    }
    return( msgStat );
}

// ***************************************************************************
// FUNCTION NAME:        fnPreExitIpsdApp
// PURPOSE:          
//   Premessage function, called when talking to a Gemini IPSD, after the 
//      RunSelfTests command has been run.  
//   The ProgMode tells us what we should do now.  
//   If the SelfTests succeeded, the ProgMode was changed to APPMODE.  
//   If it failed, then we are still in the Untested App Mode.
//   So....
//   If the Progmode is Untested App, then we need to reset the I-button comm
//    task (and the 2480?), then call the subscript to exit the app mode, then 
//    get the microstatus again.  Ideally, the unit should be repowered at this 
//    point.  Since all of this should be happening during the PCT process, 
//    that is up to the PCT routine.
//   If the Progmode is anything else, then we don't want to do any of that stuff.  
//   Instead, we want to exit this subscript, and continue on with the IPSD 
//   initialization script.
// 
// INPUTS:           
//       msgID - Subscript ID = IPSDBL_EXIT_APPMODE.
// OUTPUT:
//       BOB_OK
// MODS:
// 2009.03.31 Clarisa Bellamy - New function                  
//-----------------------------------------------------------------------------
char fnPreExitIpsdApp( ushort bMsgID )
{
    struct  msgInfo     messyInfo;
    ulong               appliedTimeout;
    BOOL                fEventity;                  // status received when checking events                     
    ulong               lwEventsReceived;           // copy of event group after waiting for event              
    char                msgStatus = BOB_SKIP;
    ushort          internalError = JMEGABOB_STATE_OK;

    // If the ProgMode is anything other than Untested App, we need to exit 
    //  this subscript, and continue on with the SCRIPT_INIT_IPSD.
    if( bIPSD_ProgMode != IPSD_MODE_UNTESTEDAPP )
    {
        return( BOB_EXIT_SUBSCRIPT );
    }
    
    // We didn't pass the selftests!!!  So indicate that status in the ProgMode.
    bIPSD_ProgMode = IPSD_MODE_GEMINI_EXITING;

    // This does what the fnDeviceMessage() would normally do, except that there 
    //  is not really a message to send.  The IT MessaageID instructs the ibutton 
    //  comm task to go back to the Idle state, and reset the baud rate to the
    //  "standard" one-wire speed, so that it will be ready for the INIT command.
    // There is no reply for this message.  The ibnt comm task sets either the 
    //  ok or error event when it has finished.

    // Reset the Tx and Rx buffers.
    memset( juniorBuildMsg, 0xFF, BUILD_BUF_SIZ );
    memset( juniorReplyMsg, 0xFF, REPLY_BUF_SIZ );

    // Partially fill the structure that will be used to direct building of the
    //  message.  
    messyInfo.msgID = bMsgID;                                               
    messyInfo.xmitBuf = &juniorBuildMsg[0];                                 
    messyInfo.recBuf = &juniorReplyMsg[0];
    messyInfo.xferStruc = &transferStruct;

    // build the message and examine the results
        messyInfo.timeout =     2000;                       // Set it to 2 seconds.
        messyInfo.device  =     IPSD;                       // 
        messyInfo.flags[0] =    RPLY_IPSD;
        messyInfo.flags[1] =    0;
        messyInfo.flags[2] =    0;
        messyInfo.flags[3] =    0;
        messyInfo.statusPlace = bobaVarAddresses[ BOBID_IPSD_CMDSTATUSRESP ].addr; 
        messyInfo.xferStruc->bMsgLen = 0;
        messyInfo.xferStruc->retries = 0;
        messyInfo.xferStruc->msg_number = messyInfo.msgID;      // fill its transfer structure with the 
        messyInfo.xferStruc->device_type = IPSD;                // message ID, mux setting, output      
        messyInfo.xferStruc->pMessage = messyInfo.xmitBuf;      // message pointer, length of message,  
        messyInfo.xferStruc->pDataRecv[0] = messyInfo.recBuf;   // address for the reply, message case, 
        messyInfo.xferStruc->bMsgCase = 0;                      // expected reply... also clear the     
        messyInfo.xferStruc->expectedReply = 8;                 // the error flag and reply length      
        messyInfo.xferStruc->errorFlags = 0;                    // that the driver will fill...         
        messyInfo.xferStruc->replyLen[0] = 0;                   // also specify whether a get response  
        messyInfo.xferStruc->replyLen[1] = 0;                   // also specify whether a get response  
        messyInfo.xferStruc->replyLen[2] = 0;                   // also specify whether a get response  
        messyInfo.xferStruc->replyCnt = 1;
        messyInfo.xferStruc->replyNum = 0;
        messyInfo.xferStruc->eventID = BOB_DRIVERS_EVENTS;  // NOTE!!!! these three lines must be changed to variables 
        messyInfo.xferStruc->errMask = T0_ERROR;
        messyInfo.xferStruc->okMask = T0_SUCCESS;
        messyInfo.xferStruc->getResponse = 0;               // not used in comet 

    appliedTimeout = messyInfo.timeout;                             


    // all drivers handshake to bob through an event register
    (void)OSSetEvents( BOB_DRIVERS_EVENTS, WAITING_FOR_T0, OS_AND );          

    // Send message to IPSD driver to reset the ibutton comm task.
    (void)OSSendIntertask( CIBUTTON, SCM, RESET_IBUTTON_COMMAND, GLOBAL_PTR_DATA, 
                           &transferStruct, sizeof(struct bobDriverIface) );
                            
    // Reset message has been sent to I-button driver.  Suspend until the 
    //  ibutton comm task has finished.
    fEventity = OSReceiveEvents( BOB_DRIVERS_EVENTS, T0_SUCCESS | T0_ERROR, 
                                 appliedTimeout, OS_OR, &lwEventsReceived );
    // See if we were successful...
    if( fEventity == SUCCESSFUL ) 
    {   
        if( lwEventsReceived & T0_SUCCESS ) 
        {   
            // When the handshake arrives, try to initialize again.
            msgStatus = fnIPSDSetupPowerup( bMsgID );
        }
        else if( lwEventsReceived & T0_ERROR )
        {
            // timeout, ibutton is incommunicado
            internalError = BOB_IPSD_BL_COM_TIMEOUT_FAILURE;
            ucErrorFlags = transferStruct.errorFlags;
            memset( pIPSD_state, 0, sizeof(pIPSD_state) );
            pIPSD_state[3] = eIPSDSTATE_INCOMMUNICADO;
            msgStatus = BOB_SKIP_ALL;
        }
        else 
        {
            internalError = BOB_IPSD_COM_FAILURE;   
            ucErrorFlags = transferStruct.errorFlags;
            memset(pIPSD_state, 0, sizeof(pIPSD_state));
            pIPSD_state[3] = eIPSDSTATE_INCOMMUNICADO;
            msgStatus = BOB_SICK;
        }
    }
    else
    {
        if( fEventity == OS_NO_EVENT_MATCH )
            internalError = BOB_IPSD_COM_OS_NO_EVENT_MATCH;
        else if( fEventity == OS_TIMEOUT )
            internalError = BOB_IPSD_COM_OS_TIMEOUT;
        else
            internalError = BOB_IPSD_HANDSHAKE_FAILURE;
        msgStatus = BOB_SKIP_ALL;
    }

    if( msgStatus == BOB_SICK )
    {
        if( internalError ) 
            fnSetBobInternalErr( internalError );       
    }


    // Override whatever we found, because we are trying to exit application mode.
    bIPSD_ProgMode = IPSD_MODE_GEMINI_EXITING;
    return( msgStatus );
}

// ***************************************************************************
// FUNCTION NAME:        fnPostExitIbtnApp
// PURPOSE:          
//      Postmessage function, called after restarting the I-button interface.
//      This calls fnIPSDStorePowerup, to save the I-button Manufacturer Serial
//       Number, and sets the mode so that any subsequent functions know that 
//       this is not this is an attempt to exit the Application mode because 
//       it's not working. 
// INPUTS:           
//   msgID and Status of pre-function and message sent.
// OUTPUT:
// NOTES:
// MODS:
// 2009.03.31 Clarisa Bellamy - New function                  
//-----------------------------------------------------------------------------
char fnPostExitIbtnApp(  ushort msgID, BOOL msgStat )
{
    // If we get here, we are trying to reset the IPSD and 2480 after an unsuccessful 
    //  selftest.  The next line in the script should be a second try at GetMicroStatus.
    msgStat = fnIPSDStorePowerup( msgID, msgStat );
    bIPSD_ProgMode = IPSD_MODE_GEMINI_EXITING;
    return( msgStat );
}



// ***************************************************************************
// FUNCTION NAME:        fnAlwaysOK
// PURPOSE:          
//   Postmessage function, that always returns BOB_OK, ignoring any error from
//   the sent message.  We need this because we want to send a message to the 
//   GEMINI, and ignore the results, before we send the RunSelfTest message.  
//   There is a bug in the current GEMINI code that if you send EnterApp, then 
//   reboot, then send RunSelfTests as the first message, it locks up the IPSD.
// INPUTS:           
//      msgID and Status of pre-function and message sent.
// OUTPUT:
//      BOB_OK  
// MODS:
// 2009.03.04 Clarisa Bellamy - New function                  
//-----------------------------------------------------------------------------
char fnAlwaysOK( ushort msgID, BOOL msgStat )
{   
    // We don't really care whether the message was successful, we just 
    //  need it as a filler.  So ignore the response, and continue with 
    //  the script processing.
    return( BOB_ALLOW );    
}


// ***************************************************************************
// FUNCTION NAME:        fnPostCheckGemini
// PURPOSE:          
//   Postmessage function, run after we run the script to get the device status. 
// INPUTS:           
//   msgID and Status of pre-function and message sent.
// OUTPUT:
//       BOB_OK or BOB_SKIP_ALL
// MODS:
// 2009.03.04 Clarisa Bellamy - New function                  
//-----------------------------------------------------------------------------
char fnPostCheckGemini( ushort msgID, BOOL msgStat )
{   
    switch( bIPSD_ProgMode )
    {
        case IPSD_MODE_ASTEROID:      
        case IPSD_MODE_APPLICATION:   
        case IPSD_MODE_SECURE_APP:
            msgStat = BOB_OK;
            break;
            
        case IPSD_MODE_NONE:          
        case IPSD_MODE_BOOT_LOADER:   
        case IPSD_MODE_UNTESTEDAPP:   
        case IPSD_MODE_GEMINI_UNKNOWN:
        case IPSD_MODE_GEMINI_EXITING:
        default:
            msgStat = BOB_SKIP_ALL;
            break;
    }
    return( msgStat );    
}


const uchar GetIPSDLogsIndicia[] = { 0x80, 0x19, 0x00, 0x01, 0x00 };
const uchar GetIPSDLogsRefill[]  = { 0x80, 0x19, 0x00, 0x02, 0x00 };
const uchar GetIPSDLogsAudit[]   = { 0x80, 0x19, 0x00, 0x03, 0x00 };

BOOL fnAccessIButton( ushort bMsgID )
{
    struct  msgInfo     messyInfo;
    ulong               appliedTimeout;
    BOOL                fEventity;                  // status received when checking events                     
    ulong               lwEventsReceived;           // copy of event group after waiting for event              
    char                msgStatus = BOB_OK;
    ushort				internalError = JMEGABOB_STATE_OK;
	void *              pDest = NULL;
	ulong 				size = 0;


    // Reset the Tx and Rx buffers.
    memset( juniorBuildMsg, 0xFF, BUILD_BUF_SIZ );
    memset( juniorReplyMsg, 0xFF, REPLY_BUF_SIZ );

	if(bMsgID == I_BUTTON_INDICIA_LOGS)
	{
		size = sizeof(GetIPSDLogsIndicia);
		memcpy(juniorBuildMsg, GetIPSDLogsIndicia, size);
		pDest = bobaVarAddresses[ BOBID_IPSD_INDCREATELOG ].addr;
	}
	else if (bMsgID == I_BUTTON_REFILL_LOGS)
	{
		size = sizeof(GetIPSDLogsRefill);
		memcpy(juniorBuildMsg, GetIPSDLogsRefill, size);
		pDest = bobaVarAddresses[ BOBID_IPSD_REFILLLOG ].addr;
	}
	else if (bMsgID == I_BUTTON_AUDIT_LOGS)
	{
		size = sizeof(GetIPSDLogsAudit);
		memcpy(juniorBuildMsg, GetIPSDLogsAudit, size);
		pDest = bobaVarAddresses[ BOBID_IPSD_AUDITLOG ].addr;
	}
	else
		msgStatus = BOB_SICK;  //TODO JAH fix me and give me a real meaning
 
	// Partially fill the structure that will be used to direct building of the
    //  message.  
    messyInfo.msgID = TRANSMIT_COMMAND;
    messyInfo.xmitBuf = &juniorBuildMsg[0];                                 
    messyInfo.recBuf = &juniorReplyMsg[0];
    messyInfo.xferStruc = &transferStruct;

    // build the message and examine the results
    messyInfo.timeout =     2000;                       // Set it to 2 seconds.
    messyInfo.device  =     IPSD;                       // 
    messyInfo.flags[0] =    RPLY_IPSD;
    messyInfo.flags[1] =    0;
    messyInfo.flags[2] =    0;
    messyInfo.flags[3] =    0;
    messyInfo.statusPlace = bobaVarAddresses[ BOBID_IPSD_CMDSTATUSRESP ].addr; 
    messyInfo.xferStruc->bMsgLen = size;
    messyInfo.xferStruc->retries = 0;
    messyInfo.xferStruc->msg_number = messyInfo.msgID;      // fill its transfer structure with the 
    messyInfo.xferStruc->device_type = IPSD;                // message ID, mux setting, output      
    messyInfo.xferStruc->pMessage = messyInfo.xmitBuf;      // message pointer, length of message,  
    messyInfo.xferStruc->pDataRecv[0] = messyInfo.recBuf;   // address for the reply, message case, 
    messyInfo.xferStruc->bMsgCase = 0;                      // expected reply... also clear the     
    messyInfo.xferStruc->expectedReply = 8;                 // the error flag and reply length      
    messyInfo.xferStruc->errorFlags = 0;                    // that the driver will fill...         
    messyInfo.xferStruc->replyLen[0] = 0;                   // also specify whether a get response  
    messyInfo.xferStruc->replyLen[1] = 0;                   // also specify whether a get response  
    messyInfo.xferStruc->replyLen[2] = 0;                   // also specify whether a get response  
    messyInfo.xferStruc->replyCnt = 1;
    messyInfo.xferStruc->replyNum = 0;
    messyInfo.xferStruc->eventID = BOB_DRIVERS_EVENTS;  // NOTE!!!! these three lines must be changed to variables 
    messyInfo.xferStruc->errMask = T0_ERROR;
    messyInfo.xferStruc->okMask = T0_SUCCESS;
    messyInfo.xferStruc->getResponse = 0;               // not used in comet 

    appliedTimeout = messyInfo.timeout;                             


    // all drivers handshake to bob through an event register
    (void)OSSetEvents( BOB_DRIVERS_EVENTS, WAITING_FOR_T0, OS_AND );          

    // Send message to IPSD driver.
    (void)OSSendIntertask( CIBUTTON, SCM, TRANSMIT_COMMAND, GLOBAL_PTR_DATA,
                           &transferStruct, sizeof(struct bobDriverIface) );
                            
    // Message has been sent to I-button driver.  Suspend until the 
    //  ibutton comm task has finished.
    fEventity = OSReceiveEvents( BOB_DRIVERS_EVENTS, T0_SUCCESS | T0_ERROR, 
                                 appliedTimeout, OS_OR, &lwEventsReceived );
    // See if we were successful...
    if( fEventity == SUCCESSFUL ) 
    {   
        if( lwEventsReceived & T0_SUCCESS ) 
        {   
			if((messyInfo.statusPlace[0] == 0x90) && 
			   (messyInfo.statusPlace[1] == 0x00) )
			{
				memcpy(pDest, messyInfo.xferStruc->pDataRecv[0], messyInfo.xferStruc->replyLen[0]-2);
			}
			else
			{
				//iButton message failure
				EndianAwareCopy(&internalError, messyInfo.statusPlace, sizeof(internalError));
				ucErrorFlags = transferStruct.errorFlags;
				msgStatus = BOB_SICK;
			}
        }
        else if( lwEventsReceived & T0_ERROR )
        {
            // timeout, ibutton is incommunicado
            internalError = BOB_IPSD_BL_COM_TIMEOUT_FAILURE;
            ucErrorFlags = transferStruct.errorFlags;
            msgStatus = BOB_SICK;
        }
        else 
        {
            internalError = BOB_IPSD_COM_FAILURE;   
            ucErrorFlags = transferStruct.errorFlags;
            msgStatus = BOB_SICK;
        }
    }
    else
    {
        if( fEventity == OS_NO_EVENT_MATCH )
            internalError = BOB_IPSD_COM_OS_NO_EVENT_MATCH;
        else if( fEventity == OS_TIMEOUT )
            internalError = BOB_IPSD_COM_OS_TIMEOUT;
        else
            internalError = BOB_IPSD_HANDSHAKE_FAILURE;
        msgStatus = BOB_SICK;
    }

    if( msgStatus == BOB_SICK )
    {
		sprintf( pBobsLogScratchPad, "BOB_SICK in fnAccessIButton, internal errror = %d.", internalError );
		fnDumpStringToSystemLog( pBobsLogScratchPad );
        
		if( internalError ) 
            fnSetBobInternalErr( internalError );       
    }

    return( msgStatus );
}

/*lint -restore */
