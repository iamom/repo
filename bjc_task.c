/*
----------------------------------------------------------------------------
NB700 program source file
Copyright (c) 2002-2003 by CanonAPTEX Inc.
----------------------------------------------------------------------------
Filename : ENG_TASK.c
Purpose  : Engine Control
----------------------------------------------------------------------------
02/07/24 : newly created by k.Ishikawa
10
*/
/*****************************************************************Preprocessor*/
#include <pbos.h>			// PBI file
#include <ossetup.h>	// PBI file
#include <cmpm.h>		// PBI file

/*********************************************************Prototype and Extern*/
void		BJControlTask(ulong argc, void *argv);

extern BOOL	Run(INTERTASK *Msg);
extern BOOL	Print(INTERTASK *Msg);
extern BOOL	PerformPrinterMaintenance(INTERTASK *Msg);
extern BOOL	InitializePrintManager(INTERTASK *Msg);
extern BOOL	PerformPrintManagerDiagnostic(INTERTASK *Msg);
extern BOOL	PerformReplacement(INTERTASK *Msg);
extern BOOL	ClearPrinterError(INTERTASK *Msg);
extern BOOL	PrinterShutdown(INTERTASK *Msg);
extern BOOL	SetMode(INTERTASK *Msg);
extern BOOL	Eject(INTERTASK *Msg);
extern BOOL	PmReset(INTERTASK *Msg);
extern BOOL	RequestPrinterSensorData(INTERTASK *Msg);
extern BOOL	GetPrintHeadData(INTERTASK *Msg);
extern BOOL	GetInkTankData(INTERTASK *Msg);
extern BOOL	GetPrintManagerSwVersion(INTERTASK *Msg);
extern BOOL	GetPmNvmData(INTERTASK *Msg);
extern BOOL	PmNvmUpdate(INTERTASK *Msg);
extern BOOL	PmReplacementComplete(INTERTASK *Msg);

extern BOOL	PmMaintenanceCheck(INTERTASK *Msg);
extern BOOL	PmStopRun(INTERTASK *Msg);
extern BOOL	PmRunTape(INTERTASK *Msg);
extern BOOL	PmStopTapeRun(INTERTASK *Msg);

extern BOOL	UnsolicitedPrinterStatus(INTERTASK *Msg);
extern BOOL	AutoStartRequest(INTERTASK *Msg);

extern void	InterTaskMessageError(uchar command,INTERTASK *Msg,uchar number);

#ifndef FPGA_WATCHDOG_ENABLE
extern void HALDisableFPGAWatchdog(void);
#endif


/*****************************************************************************
 IN	
	void				:	
 OUT	
	void				:	
*****************************************************************************/
void
BJControlTask(ulong argc, void *argv){

/*	int         status; */	/*warning�Ή�*/
	INTERTASK	Msg;

	/* Access argc and argv just to avoid compilation warnings.  */
/*	status =  (int) argc + (int) argv;*/	/*warning�Ή�*/


	while(1){

			while(OSReceiveIntertask(BJCTRL,&Msg,OS_SUSPEND) != SUCCESSFUL){};
			if(Msg.bSource != CM){
				InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_SRC);
			}else if(Msg.bDest != BJCTRL){
				InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_DEST);
			}else{
				if(Msg.bMsgId ==  CP_INIT_PM){
					if(Msg.bMsgType == GLOBAL_PTR_DATA){
						InitializePrintManager(&Msg);
#ifndef FPGA_WATCHDOG_ENABLE
						HALDisableFPGAWatchdog();
#endif
						break;
					}else{
						InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
					}
				}else if(Msg.bMsgId ==  CP_GET_PM_VER){
					if(Msg.bMsgType == NO_DATA){
						GetPrintManagerSwVersion(&Msg);
					}else{
						InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
					}
				}else{
					InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_ID);
				}
			}
	}

	while(1){
			while(OSReceiveIntertask(BJCTRL,&Msg,OS_SUSPEND) != SUCCESSFUL){};

			if((Msg.bSource != CM)&&(Msg.bSource != BJSTATUS)){
				InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_SRC);
			}else if(Msg.bDest != BJCTRL){
				if(Msg.bSource == CM){	InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_DEST);	}
			}else if(Msg.bSource == CM){

				switch(Msg.bMsgId){
				/* Exe */
					case CP_RUN:
						if(Msg.bMsgType != BYTE_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
#ifdef NO_PRINTER //TODO JAH Remove after we have printers
                        OSWakeAfter(1200);
#endif //TODO JAH Remove after we have printers

						Run(&Msg);
						//Important - if FPGA_WATCHDOG_ENABLE is defined, then WATCHDOG_TIMER_ENABLE must be defined also;
						// i.e. - if FPGA watchdog is not disabled, then CPU watchdog must be enabled
#ifndef FPGA_WATCHDOG_ENABLE
						HALDisableFPGAWatchdog();
#endif

//TODO remove temp code to cause FPGA watchdog timer timeout
#if 0
						int old_level;
						int i;
						extern void  HALDelayTimerTicks (UINT32 no_of_ticks);

						old_level = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS); // disables FIQ and IRQ for ARM
						for (i = 0; i< 100; i++) //delay 100 ms
							HALDelayTimerTicks(25000);
						NU_Local_Control_Interrupts(old_level); // restore ints but too late for watchdog
#endif
						break;
					case CP_PRINT:
						if(Msg.bMsgType != GLOBAL_PTR_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
#ifdef NO_PRINTER //TODO JAH Remove after we have printers
                        OSWakeAfter(175);
                        OSSendIntertask (CM, BJSTATUS, PC_PRINT_READY, NO_DATA, 0, 0);
                        OSWakeAfter(556);
#endif //TODO JAH Remove after we have printers

						Print(&Msg);
						break;
					case CP_PERFORM_PRT_MTNC:
						if(Msg.bMsgType != BYTE_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						PerformPrinterMaintenance(&Msg);
						break;
					case CP_SET_MODE:
						if(Msg.bMsgType != GLOBAL_PTR_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						SetMode(&Msg);
						break;
					case CP_INIT_PM:
						if(Msg.bMsgType != GLOBAL_PTR_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						InitializePrintManager(&Msg);
#ifndef FPGA_WATCHDOG_ENABLE
						HALDisableFPGAWatchdog();
#endif
						break;
					case CP_PERFORM_PM_DIAG:
						if(Msg.bMsgType != BYTE_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						PerformPrintManagerDiagnostic(&Msg);
						break;
					case CP_PERFORM_RPL:
						if(Msg.bMsgType != NO_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						PerformReplacement(&Msg);
						break;
					case CP_CLR_PRT_ERR:
						if(Msg.bMsgType != LONG_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						ClearPrinterError(&Msg);
						break;
					case CP_PRT_SHUTDOWN:
						if(Msg.bMsgType != BYTE_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						PrinterShutdown(&Msg);
						break;
					case CP_EJECT:
						if(Msg.bMsgType != NO_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						Eject(&Msg);
						break;
					case CP_RESET:
						if(Msg.bMsgType != NO_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						PmReset(&Msg);
						break;
					case CP_PM_NVM_UPDATE:
						if(Msg.bMsgType != GLOBAL_PTR_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						PmNvmUpdate(&Msg);
						break;
					case CP_RPL_COMPLETE:
						if(Msg.bMsgType != NO_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						PmReplacementComplete(&Msg);
						break;

					case CP_MAINTENANCE_CHECK:
						if(Msg.bMsgType != NO_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						PmMaintenanceCheck(&Msg);
						break;
					case CP_STOP_RUN:
						if(Msg.bMsgType != BYTE_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						PmStopRun(&Msg);
						break;
					case CP_RUN_TAPE:
						if(Msg.bMsgType != BYTE_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						PmRunTape(&Msg);
						break;
					case CP_STOP_TAPE_RUN:
						if(Msg.bMsgType != NO_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						PmStopTapeRun(&Msg);
						break;

				/* Data Request*/
					case CP_REQ_SENSOR_DATA:
						if(Msg.bMsgType != GLOBAL_PTR_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						RequestPrinterSensorData(&Msg);
						break;
					case CP_GET_PRTHEAD_DATA:
						if(Msg.bMsgType != GLOBAL_PTR_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						GetPrintHeadData(&Msg);
						break;
					case CP_GET_INKTANK_DATA:
						if(Msg.bMsgType != GLOBAL_PTR_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						GetInkTankData(&Msg);
						break;
					case CP_GET_PM_VER:
						if(Msg.bMsgType != NO_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						GetPrintManagerSwVersion(&Msg);
						break;
					case CP_GET_PM_NVM_DATA:
						if(Msg.bMsgType != GLOBAL_PTR_DATA){
							InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_TYPE);
							break;
						}
						GetPmNvmData(&Msg);
						break;
					default:
						InterTaskMessageError(PC_PRT_STATUS,&Msg,E_MSG_ID);
						break;
				}
			}else{ 		//(Msg.bSource == BJSTATUS){
				switch(Msg.bMsgId){
				/* Exe */
					case PC_PRT_STATUS:
						UnsolicitedPrinterStatus(&Msg);
						break;
					case PC_AUTOSTART:
						AutoStartRequest(&Msg);
						break;
					default:
						break;
				}
			}
	}
}
