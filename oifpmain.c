/************************************************************************
  PROJECT:        Future Phoenix
  COPYRIGHT:      2005, Pitney Bowes, Inc.
  AUTHOR:         
  MODULE NAME:    OifpMain.c       

  DESCRIPTION:    This file contains all types of screen interface functions 
                  (hard key, event, pre, post, field init and field refresh)

//-----------------------------------------------------------------------------
 MODIFICATION HISTORY:
 
 2018.02.08 CWB - CSD-Sr
 - Added checks for the new Disabling Condition: DCOND_ACCESSING_PM_NVM.  
	
 01-May-15 sa002pe on FPHX 02.12 shelton branch
	For Fraca 229158, altered the change for Fraca 222452 so it won't affect Brazil,
	or any other countries that use the HRT strings for KIP mode.
	
	Changed the missed midnight processing to display an error if the processing was
	missed, but only if the processing was missed because the "last" date is BEFORE "today".

 11-Mar-15 sa002pe on FPHX 02.12 int branch
	Making changes for the real implementation of Brazil tracking services.
	Changed fnIndiciaReadyConditionsCheck & fnIndiciaReadyConditionsCheck2.

 21-Jan-15 Jane Li on FPHX02.12 cienet branch
    Modified fnIndiciaReadyConditionsCheck2() to close the batch when KIP value is changed.

 22-Jul-14 Renhao  on FPHX 02.12 cienet branch   
    Modified function fnIndiciaReadyConditionsCheck() to put up the "Rates Expired" 
    screen when ANY of the rates/DCM files has expired.
 	
 09-May-14 sa002pe on FPHX 02.12 shelton branch
 	Moved Renhao's change to fnIndiciaReadyConditionsCheck to replace what was
	already there.

 05-May-14  Renhao     on FPHX 02.12 cienet branch
    Modified function fnIndiciaReadyConditionsCheck2() to fix fraca 225188,if all Rates or all 
    Dcap Modules are expired, "Rates have expired" screen  will prompt.
 	
 02-May-14 sa002pe on FPHX 02.12 shelton branch
	Undid Wang Biao's change for fraca 225110 because it wasn't what I asked CieNET to
	do for the fraca.
	Removed some code that has been commented out for a long time.

 29-Apr-14  Wang Biao  on FPHX 02.12 cienet branch
    Moved the codes about checking rates expiration from the function fnIndiciaReadyConditionsCheck() 
    to the function fnIndiciaReadyConditionsCheck2() for fraca 225110.
	
 18-Apr-14 sa002pe on FPHX 02.12 int branch
 	In order to fix Fraca 224948, changed Wang Biao's fix for Fraca 222452 to
	call fnRateUnSelectClass BEFORE calling fnRateSetNonRateMode. Doing the calls
	in the opposite order is what caused Fraca 224948.
 
 03-Apr-14 Renhao on FPHX 02.12 cienet branch
    Modified function fnIndiciaReadyConditionsCheck()to add "New Rates Effective" 
    and "Rates Expire" check for G922 Rates Expiration  new feature.
 
 18-Mar-14 Wang Biao on FPHX 02.12 cienet branch
    Modified function fnIndiciaReadyConditionsCheck() to fix fraca 222452.
	
 20-Mar-14 sa002pe on FPHX 02.12 shelton branch
 	Related to Janus Fraca 224510:
	1. Fixed fnfInViewDisableError so it will display the error.
	2. Changed ucAbacusAcntErrorCode to ucCMOSAbacusAcntErrorCode so it can be retained
		thru power cycles in case some other error comes up first and the other error
		requires a power cycle.

 11-Mar-14 sa002pe on FPHX 02.12 shelton branch
 	Related to Janus Fraca 224510:
	1. Added ucAbacusAcntErrorCode 
	2. Changed fnIndiciaReadyConditionsCheck to display the AbacusAccountingDisabled screen
		if fCMOSAbacusAcntProblem is set to TRUE.
	3. Added fnOISetAccountingProblem, fnfInViewDisableError & fnhkClearAbacusProblem.

 11-Feb-14 Wang Biao on FPHX 02.12 cienet branch
    Modified function fnfFencingLimitLowValue() and fnfFencingLimitHighValue() to fix fraca 224400.
    
 24-Feb-13 sa002pe on FPHx 02.10 shelton branch
 	Now that the screen changes for the Balance Inquiry reports have been done, filled in
	the latest menu/string options in the header area for some of the functions.

 22-Feb-13 sa002pe on FPHx 02.10 shelton branch
 	Merging in changes from the 01.10 branch for Brazil that didn't get merged back to the main branch
----
	  2009.05.27 Clarisa Bellamy - FPHX 1.10 - Modified fnpIndiciaReadyCheck by adding a call to 
	                                fnOiSetBalanceInquiry() to clear the Balance Inquiry flag. 
	                                This is just a catchall, in case some condition may have
	                                by-passed the other places where it is cleared.  It should
	                                only be set while we are engaged in a Balance Inquiry call.
	  10/29/2008  Raymond Shen      Modified fnHomeScreenMenuInit() and added fnCheckServicesHome()
	                                to support options "Services" and "Batch".
	  10/24/2008  Raymond Shen      Added code to support Brazil KIP Indicia HRT selection and Batch Clear from Home screen.
----

  2012.10.15 Clarisa Bellamy    fphx 02.10
  - Fix fraca 215576 - If an update tag is received when not an a screen that 
    can handle it, handle it the next time one of those screens is reached.
    - Modified fnpIndiciaError and fnIndiciaReadyConditionsCheck2 to check for 
      the update-from-host pending condition before going to the MMIndiciaErrorAbacus
      or the MMIndiciaReadyAbacus screens (respectively.)  If the update is pending,
      go to the AbacusHostUpdateProgress screen instead.

 2012.09.26  Bob Li
 - Modified function fnIndiciaReadyConditionsCheck() to check the print mode 
    if feature enabled for fixing fraca 218434.

 2012.09.24 Clarisa Bellamy     fphx 02.10 
  Fix fraca 139154, except it is unknown at this time if Inview requires the 
  reporting of the TextAdDateTimeStamp mode pieces. So the code is written so
  that 2 variables, one for abacus, and one for hosted abacus, control this.
  - Modify fnOptionalPrintModeConditionsCheck2 to use the function that checks 
    these variables to determine if abacus account selection is required when 
    in TextAdDateTimeStamp mode.
    
 2012.09.24  John Gao
 - Added fWrongUsbPlatform.
 - Changed fnIndiciaReadyConditionsCheck2
   to check fWrongUsbPlatform and put up the error screen when necessary.
   
 2012.08.01  Clarisa Bellamy
 Modifications to Jupeng's changes:
 - Moved the event flag from the CM_EVENT_GROUP to the PLAT_EVENT_GROUP 
   (which already exists) and change the flag name to PLAT_OK_TO_SEND_UPDATE.
 - Added syslog entries for clearing/setting and checking the event flag.
 
 07/12/2012  Liu Jupeng        
 - Modified the pre-function fnpIndiciaReadyCheck() for fixing the issue that 
   the meter prints the empty evelope.

 2011.07.18  Clarisa Bellamy FPHX 02.07
	- Fix fraca 206084, Manual Transactions are missing 
      Carrier/Class info, by fixing the class-selection 
      flag logic:
       - In fnIndiciaReadyConditionsCheck, use the 
         accessor function to find out if we are 
         selecting a class for an abacus manual 
         transaction, because the flag is now private.  
       - This function should not clear any MX flags.
  08/11/2010 Clarisa Bellamy  Fraca 192992, (with changes to oiclock.h, oiclock.c, clock.c 
    - Modified fnpIndiciaReadyCheck to NOT put up the 
      reboot screen if the missed midnight was detected.
      It will still record the error in the exception.log file
      and in the CMOSErrorLog, but only once a day, and only
      for 5 days in a row.  If it missed midnight, it
      checks to see if the clock was set past midnight recently,
      and if so records that date/time, then clears that flag.
  03/29/2010  Raymond Shen      Modified fnpIndiciaReadyCheck() to always enable the local midnight event check without caring
                                whether DCAP is enabled.
  03/25/2010  Raymond Shen      Modified fnpIndiciaReadyCheck() to check DCAP midnight event only when DCAP is enabled.
  02/25/2010  Raymond Shen      Basing on Linda's code, modified fnpIndiciaReadyCheck() to force a system reboot if it detects any midnight event is missed.
  01/11/2010  Raymond Shen      Modified fnAbacusHomeScreenMenuInit() and fnAbacusPrintingScreenMenuInit() to add the �EKP/Task Number, 
                                Piece ID, Reply Cd� soft keys to Abacus home screens.
  11/24/2009  Raymond Shen      Modified fnpPostIndiciaReady() for GMSE00171558 (G900 - Loss of Postage reported 
                                by customer after No Paper message and error 2482).
  09/21/2009  Bonnie Xu         Modified functions fnIndiciaReadyConditionsCheck(), fnIndiciaReadyConditionsCheck2()
                                to support Portugal.
  09/22/2009  Raymond Shen      Modified fnHomeScreenMenuInit() and fnAbacusHomeScreenMenuInit() to fix the inscription display problem invoked
                                by adding "Barcode" option in the Home screen. 
  09/21/2009  Raymond Shen      Modified fnIndiciaReadyConditionsCheck2() to fix the problem that it can't navigate to
                                the Mode Ajout Ready screen when Abacus Accounting is actived.
  08/26/2009  Jingwei,Li       Modified fnHomeScreenMenuInit() and Added fnIsCanadaPackageServie() for CPS.
  08/26/2009  Raymond Shen      Modified fnHomeScreenMenuInit() and fnAbacusHomeScreenMenuInit() to fix the inscription softkey display problem
                                invoked by previous merge.
  07/15/2009  Bonnie Xu         Modified functions fnIndiciaReadyConditionsCheck(), fnIndiciaReadyConditionsCheck2()
                                to support Austria 
  07/29/2009  Jingwei,Li       Modified fnUpdateDisablingWarningConditions() for Canada Package service.
  07/20/2009  Raymond Shen     Modified fnUpdateDisablingWarningConditions(), fnSealOnlyUpdateConditions(), and fnTransToErrorScreens() 
                               to let "Cancel" key in "Tape Jammed" screen navigate to Home screen with disabling condition "Clear Print transport".
  07/10/2009  Raymond Shen     Modified fngHomeWarningDisplayed() and fngHomeErrorDisplayed() to always show the bottom line on Home screen for DM475C.
  07/08/2009  Raymond Shen     Modified fnTransToErrorScreens(), fnResetAllWarningCheckFlags(), fnIndiciaReadyConditionsCheck2(), fnPostCheckCMConditions()
                               and added fnSetPaperErrorShownStatus(), fnGetPaperErrorShownStatus() to let "CANCEL" key 
                               on "Mail Jammed" screen navigate to Home screen with disabling condition.
  06/24/2009  Raymond Shen     Removed the check of error condition "Paper in Feeder transport". 
  06/23/2009  Jingwei,Li       Modified fnIndiciaReadyConditionsCheck2(),fnUpdateDisablingWarningConditions() and
                               fnUpdateErrorStatus() for "Paper in Feeder transport" error.
  06/16/2009  Jingwei,Li       Modified fnevUpdateWarningMessages() to show business mgr offline warning.
  05/21/2009  Jingwei,Li       Modified fnpIndiciaReadyCheck() to start query sensor timer.
  05/20/2009  Jingwei,Li       Modified fnpIndiciaAbacusCheck() to avoid avoid 2003 error.
  05/18/2009  Raymond Shen     Modified fnIndiciaReadyConditionsCheck2(), fnUpdateErrorStatus(), fnUpdateDisablingWarningConditions() for wow.
  05/05/2009   Jingwei,Li       Modified function fnIndiciaReadyConditionsCheck() for revert wow.
  04/01/2009  Jingwei,Li       Modified function fnfHomeErrorStrings() and fnfHomeErrorCounter() for no error case.
  03/10/2009  Jingwei,Li       Modified function fnfHomeWarningStrings() and fnfHomeWarningCounter() for no warning case.
  12/11/2008  Jingwei,Li       Modified function fnIndiciaReadyConditionsCheck() to support WOW.
  10/23/2008  Bob Li            Modified functions to support GTS for Brazil.
  11/21/2008  Joey Cui          Modified fnHomeScreenMenuInit(), fnPermitModeMenuInit() and
                                fnAbacusHomeScreenMenuInit() to fix fraca 152664 for French.
  09/24/2008  Bob Li            Modified function fnfMeterStamp to undo the fake codes to get the real selected indicia.
  09/24/2008  Joey Cui          Modified fnIndiciaReadyConditionsCheck to check Active Operator Number before goto
                                Operator Login screen.
  09/12/2008  P.Vrillaud        Modified fnIndiciaReadyConditionsCheck to fix fraca148331: Add disabling condition
  09/12/2008  Oscar Wang        Modified fnIndiciaReadyConditionsCheck to fix fraca148331: at home screen, clear class 
                                when invalid weight happens
  08/13/2008  Joey Cui          Merged stuffs from Janus to support PC proxy feature.
  08/13/2008  Ivan Le Goff      Added condition to Mike's change on 01/08/2008: if we are not in KIP_CLASS_NOT_ALLOWED 
                                KIP is not a non rate mode and going to non rate mode makes us lose information
  08/05/2008  Bob Li            Modified some functions for supporting IBI-Lite feature.
  08/01/2008  Raymond Shen      Modified fnpDataCenterOptionsMenu() and added fnIsDataCenterUploadEnabled() 
                                for Budget accounting upload.
  07/18/2008  Raymond Shen      Added ucOITBatteryStatus, fnCheckBatteryState(), fnpClearBatteryWarnings(), 
                                fnOITSetBatteryStatus() and modified fnUpdateWarningStatus(), fnCheckPSDState(),
                                fnIndiciaReadyConditionsCheck(), fnSealOnlyConditionsCheck(), 
                                fnOptionalPrintModeConditionsCheck(), fnPermitModeConditionsCheck()
                                for PSD/CMOS battery life checking.
  05/26/2008  Joey Cui          Modified fnIndiciaReadyConditionsCheck() to check whether archive file is present
  03/10/2008  Oscar Wang        Updated for End of Life warning/error display.
  01/08/2008  Michael Lepore    Fraca 135466. Added call to fnRateSetNonRateMode() in fnIndiciaReadyConditionsCheck().
                                This ensures the the Indicia strings are uptodate. Clear Key will clear out rating info. 
  12/04/2007  Raymond Shen      Modified fnAbacusHomeScreenMenuInit() to fix the refresh problem of
                                displaying the selected Abacus full name on home screen.
  12/03/2007  Oscar Wang        Renamed ESERVICE_UPLOAD_RECORD_PROMPT to ESERVICE_HOME_SCRN_UPLOAD_PROMPT. 
 11/29/2007  Andy Mo           Modified fnIndiciaReadyConditionsCheck() to fix fraca133739&133684
  11/28/2007  Joey Cui          Modified fnIndiciaReadyConditionsCheck() to move ahead the process to check Operator
                                to fix FRACA GMSE00133812
  11/20/2007  Oscar Wang        Remove the call fnSetGTSPieceProcessed() from fnClearDisablingWarningConditions()
  29 Nov 2007 Martin O'Brien    fnNoLongerInZeroPrint(void): added the
                                'static' storage class specifier to both
                                declaration and definition.
                                fnpDataCenterOptionsMenu(): Added comment
                                regarding the compiler's warning 'Illegal
                                pointer assignment'
                                fnIndiciaReadyConditionsCheck(): in two blocks,
                                removed the lone semicolons to make them truly empty
  12 Nov 2007 Martin O'Brien    New function fnNoLongerInZeroPrint() added, a
                                and a call to it made in fnIndiciaReadyConditionsCheck(). 
  11/07/2007  Adam Liu          Added reseting animator counter and GTS handling 
                                to fnpIndiciaPrintingAbacus() and fnpIndiciaTapePrintingAbacus().
   1 Nov 2007 Martin O'Brien    Modified fnIndiciaReadyConditionsCheck() to support Zero Postage
  10/24/2007  Oscar Wang        Modified function fnIndiciaReadyConditionsCheck() to fix FRACA GMSE00131603:
                                Replace fnhkModeSetDefault() with fnRatingSetDefault().
  10/19/2007  Oscar Wang        Modified function fnIndiciaReadyConditionsCheck() to fix FRACA GMSE00131327: 
                                Upload should prompt prior to preset loading.
  10/19/2007  Andy Mo           Modified fnIndiciaReadyConditionsCheck() to fix FRACA 131517
  10/18/07    P.VRILLAUD        FPHX 1.09 (Swiss adaptations) merge from K7 (fnIndiciaReadyConditionsCheck())
  09/28/2007  Andy Mo           Modified fnIndiciaReadyConditionsCheck to fix GTS issues
  09/28/2007  Raymond Shen      Modified fnIndiciaReadyConditionsCheck() to:
                                    1. Confined My EKP initializing code to German to avoid causing 
                                       US DUNS number problem.
                                    2. Change calling of fnRateCheckZWZPManWgt() to fnRateCheckZWManWgt().
  09/28/2007  Joey Cui          Modified fnpDataCenterOptionsMenu() by adding support for Firewall settings.
  09/28/2007  Adam Liu          Merged fnevRemoteRefillDone() from mega
  09/28/2007  Vincent Yi        In fnClearDisablingWarningConditions(), added code to 
                                clear GTS flag before entering Home screen.
  09/27/2007  Andy Mo           Modified fnpDataCenterOptionsMenu() by adding support for connection mode.
  09/26/2007  Adam Liu          Modified fnHomeScreenMenuInit(), fnAbacusHomeScreenMenuInit()
                                and fnAbacusPrintingScreenMenuInit() to adjust the ZZC dest and fee menu item in the menu
  09/25/2007  Vincent Yi        Commented out case ACT_NO_ACCOUNT_SELECTED in fnUpdateErrorStatus()
                                to not display "Select Account" at footer area, it have
                                displayed next to "Account:" if necessary.
  09/24/2007  Raymond Shen      Modified fnIndiciaReadyConditionsCheck() to confine the disabling condition
                                of "Postage Value Too Low" for MWE 0 wt to German system.
  09/03/2007  Oscar Wang        Modified fnIndiciaReadyConditionsCheck.
                                When power up, don't care weight unstable case since the weight is often not 
                                stable when loading GTS normal preset.In this case, we don't want to display 
                                "Place item on scale"
  08/24/2007  Joey Cui          Modified fnClearDisablingWarningConditions to fix fraca 128084
  08/23/2007  Oscar Wang        Modified fnIndiciaReadyConditionsCheck to fix FRACA GMSE00127908, don't update 
                                DCAP need class disable condition for 0 manual weight case.
  08/23/2007  Raymond Shen      Modified fnIndiciaReadyConditionsCheck() to fix fraca 127849.
  08/23/2007  Vincent Yi        Modified fnevMailJobCompleted, 
                                fnHandleDiffWtAfterMailPC,
                                fnevMailPieceCompleted,  
                                fnDWPostMailPiecePrinted for Updated for GTS + diff. wt
  08/22/2007  Oscar Wang        Modified fnIndiciaReadyConditionsCheck() to check GTS integrity.
  08/20/2007  Raymond Shen      Modified fnIndiciaReadyConditionsCheck() to fix fraca 125251 and 125252:
                                For Canada meter it should update DCap service code while switching between
                                zero and none-zero postage value in KIP mode.
  08/17/2007  Joey Cui          Modified fnpPrePrintingInstructions to fix fraca GMSE00127680
                                Modified fnCheckReplyCodeHome to block reply code menu for other country than German
  08/16/2007  Oscar Wang        Modified functions fnIndiciaReadyConditionsCheck and fnResetAllWarningCheckFlags
                                to check unmanifested record.
  08/15/2007  Oscar Wang        Modified mail/tape printing pre-function and mail job complete 
                                event function to handle error for Germany GTS Non-Stop mode.
  08/10/2007  Raymond Shen      Added code in fnIndiciaReadyConditionsCheck() to 
                                initialize My EKP in case that it is skipped during OOB.
  08/09/2007  Oscar Wang    Update for VAS disable check, check 0 manual weight error.
  08/09/2007 Raymond Shen     Modified fnIndiciaReadyConditionsCheck() to check German Piece ID Duck status.
    08/08/2007  Vincent Yi  Clean up GTS error & warning checking   
  08/07/2007  Joey Cui      Added logic to handle Manifest limit reached error and flags 
  08/06/2007  Oscar Wang    Modified function fnIndiciaReadyConditionsCheck2() for ENTER DUNS 
                            number prompt screen.
  08/06/2007  Joey Cui      Modified the reply code checking condition
  08/03/2007  Joey Cui     Added fnResetInspectionCheckFlags,fnTrackingLogConditionsCheck,
                                 fnBarcodeConditionsCheck 
                           Modified fnResetAllWarningCheckFlags, fnIndiciaReadyConditionsCheck,
                                    fnHomeScreenMenuInit, fnUpdateWarningStatus
                           to check reply code/barcode/record warning & error conditions
  02 Aug 2007 Raymond Shen  Modified fnHomeScreenMenuInit() to add EKP/Task/PieceID/ReplyCode
  01 Jul 2007  Andy Mo      Modified fnIndiciaReadyConditionsCheck to check the duck condiction
                            of Piece ID
  26 Jul 2007  P.Vrillaud     Stop ZeroWeight timer in fnpPostIndiciaReady()
  23 Jul 2007 Simon Fox    Modified checks for unstable scale and added specific checks for
                           scale timeouts.  Unstable checks now work correctly in all printing
                           modes.
  23 Jul 2007  I. Le Goff  In fnIndiciaReadyConditionsCheck(), set up weight 
                           disabling condition when the scale is not stable
  20 Jul 2007  Simon Fox   Previously when the weight changed while on the
                           weigh/rate menu with change/retain class set to
                           'change', the flag would be set to force a re-rate
                           which would cause the user to be redirected to the
                           carrier/class selection screen.  However, if the
                           user then cancelled out of that screen they would
                           end up back at the main screen with the new rate but
                           the old rateable value.  This has been fixed by
                           setting the fMayNeedRerate flag to true before
                           redirecting to the carrier/class screen when
                           change/retain class set to 'change'.
  07/12/2007   I. Le Goff  In fnIndiciaReadyConditionsCheck(), if we have to print a print 
                           fail report and if we were in differential mode, we need to go
                           to the MM_DIFF_REMOVE_ONEPIECE screen after the print failure report
                           has been printed: to do so, set fDiffWeighPieceNeeded to TRUE
  07/05/2007   I. Le Goff  Add fnIsIncidentReportNeeded() and use it.
    07/05/2007  Oscar Wang      Modified fnIndiciaReadyConditionsCheck() to fix FRACA GMSE00123827.
                                If debit is done but GTS printing is stopped by mail jam or such error, 
                                we'll continue the normal GTS workflow. 
    07/04/2007  Raymond Shen    Modified fnhkBufBlankOrExit(), fnhkExitCanadaPrintingMode() to fix a Data correction problem.
    07/03/2007  Adam Liu        Modified fnhkTogglePrintDateTime to ommit the date ducking EMD switch for AdDatetime mode
    06/29/2007  Raymond Shen    Modified fnOptionalPrintModeConditionsCheck2(), fnhkBufBlankOrExit(), 
                                and fnhkExitCanadaPrintingMode() to fix fraca 122018.
    06/28/2007    Joey Cui        Add function : fnpPostDataCenterOptionsMenu
    06/22/2007    Joey Cui        Added functions 
                                  fnpPermitModePrinting(), 
                                  fnpPostPermitModePrinting(),
                                  Modify functions
                                  fnUpdateDisablingWarningConditions, fnUpdateCMWarnings 
                                  to fix fraca GMSE00114431
    06/22/2007    Raymond Shen    Modified fnIndiciaReadyConditionsCheck() to fix fraca 121661.
  06/21/2007  Vincent Yi      Added functions 
                              fnDWPostMailPiecePrinted(), 
                              fnClearFlagDWPiecePrintedForDM400C(),
                              fnIsDWPiecePrintedForDM400C()
  06/21/2007   Oscar Wang  Modified function fnevMailJobCompleted() and fnevMailPieceCompleted()
                           to fix FRACA GMSE00121606. Added flag fDWPiecePrintedForDM400C to 
                           see if mail piece is really printed for DM400.
  06/19/2007   D. Kohl     FRACA 122728: 
                           Add define WARNING_STR_END_OF_LIFE_WARNING  and use it 
                           in fnUpdateWarningStatus()
                           Add define ERROR_STR_PIECE_COUNT_EOL and use it in
                           fnUpdateErrorStatus()
                           In fnIndiciaReadyConditionsCheck(),fnIndiciaReadyConditionsCheck2(),
                           fnOptionalPrintModeConditionsCheck2(),fnPermitModeConditionsCheck2(),
                           fnUpdateDisablingWarningConditions(), manage the piece count end
                           of life error 
  06/14/2007   I. Le Goff  In fnUpdateErrorStatus() added "Enter Batch" for mode ajout.
                           Added ERROR_STR_ENTER_AJOUT_BATCH.
  06/07/2007   Oscar Wang  Modified function fnIndiciaReadyConditionsCheck().
                           For EService Express mode, keep rating info when going to home screen.
  06/04/2007   Oscar Wang  Update function fnIndiciaReadyConditionsCheck2() to 
                           fix FRACA GMSE00121355. Go to disable condition if 
                           DUNS number is not entered.
  06/04/2007   Dan Zhang   Added fnpPermitError to fix fraca 114603.
  05/30/2007   I. Le Goff  In fnIndiciaReadyConditionsCheck(), do not reset the ajout piece count here
                           in case of a meter move outside the region. It already has been reset right
                           after the withdrawal.
    05/31/2007  Joey Cui        Modify fnUpdateCMWarnings, move clearing code just before re-checking 
    05/31/2007  Raymond Shen    Modified fnIndiciaReadyConditionsCheck() to fix fraca 116039:
                                check whether it need to restore status of previous print mode.
  05/28/2007   I. Le Goff  In fnevMailJobCompleted(), exit mode ajout only if we have printed all tge
                           mail pieces
    05/25/2007      Joey Cui    Add menu item for Data Center Options screen
    05/18/2007  Adam Liu        Modified fnOptionalPrintModeConditionsCheck2 and
                                fncOptionalPrintModeCheck to fix TextAdDateTime menu issue.

    05/18/2007  Andy Mo         Modified fnOptionalPrintModeConditionsCheck2() by adding error screens SCREEN_DATE_CORRECTION_ERROR 
                                and SCREEN_MANIFEST_ERROR for Date Correction and Manifest modes.
    05/18/2007  Joey Cui        Modify fnPermitModeConditionsCheck2 to check error for feeder_cover_open
    05/16/2007  Joey Cui        Modify fnUpdateWarningMessages to keep warning messages scroll
                                even when running a mail or a tape
fnevMailJobCompleted
    15 May 2007 Bill Herring    fnevMailJobCompleted() now checks and exits ajout mode.
    05/15/2007  Vincent Yi      Added handling on DCOND_AGD_SETUP_FAILED
    05/10/2007  Oscar Wang      Added fnUpdateWarningMessages() in function fnpIndiciaAbacusCheck()
    05 May 2007 Bill Herring    Removed unnecessary ZWZP code from fnevMailJobCompleted().
    05/10/2007  Raymond Shen    Added declaration of WARNING_STR_TRANS_LOG_NEAR_FULL, 
                                WARNING_STR_ABACUS_HOST_DISCONNECTED and modified 
                                fnIndiciaReadyConditionCheck(),fnUpdateWarningStatus()
                                to show abacus account related warning in main screen.
    04/26/2007  Andy Mo         1.Added fnfDiplayPrintingModeText() for MMPostageCorrectionPrintingComplete
                                2.Modified in fnIndiciaReadyConditionsCheck()by adding cases for 
                                Date Correction and Manifest Modes.
    04/25/2007  Andy Mo         1.Modified in fnIndiciaReadyConditionsCheck() by replaceing
                                unused lwPostageValue with ulPostageValue .
                                2.Added fnfInitPostageCorrectionAmount() to initialize the field
                                to display the postage correction amount.
    04/23/2007  Adam Liu        Lint Code to correct some potential variable 
                                initialization issues
    04/20/2007  Andy Mo         Remove unused fnpPreEnterPostageCorrection()
    04/19/2007  Andy Mo         Added below funcitons to support Postage Correction:
                                fnfPostageorDateCorrection()
                                fnpPreEnterPostageCorrection()
                                fnfFencingLimitHighValue()
                                fnfFencingLimitLowValue()
                                fnhkExitInstructions()
                                fnfDisplayInstructionLine1-4()
                                fnInstructionLines()
                                fnhkExitCanadaPrintingMode()
    04/17/2007  Joey Cui        uncomment the code to check for the errors when power up
                                Add function fnOICheck4AbacusErrors
    
*
* 17-Apr-07 sa002pe on FPHX 01.04 shelton branch
*   Changed fnIndiciaReadyConditionsCheck to call fnSetAlterNativeMode. Also to
*   add cases for PMODE_DATE_CORRECTION, PMODE_POSTAGE_CORRECTION & PMODE_MANIFEST,
*   but the load of the actual screen names are commented out until the screens
*   are created.
*
    04/25/2007  I. Le Goff    Replace uiAjoutPieceCount with uiCMOSAjoutPieceCount
    04/13/2007  Oscar Wang      Update function fnIndiciaReadyConditionsCheck2 
                                for DUNS number check.
    04/11/2007  Joey Cui        Add the code check for MXaction setting condition
    04/10/2007  Derek DeGennaro Add another check for PSOC disabling conditions.      
    04/10/2007  Raymond Shen    Added code to check operator
    04/10/2007  I. Le Goff     In fnIndiciaReadyConditionsCheck, remove temporary
                               code for powerfail test
    03/30/2007  I. Le Goff     Set back to static the following functions
                               fnUpdateCMWarnings()
                               fnUpdateDisablingWarningConditions()
                               fnCheckInspection()
                               fnPostCheckInspection()
                               fnCheckPSDState()
                               fnCheckMissingGraphics()
                               fnPostCheckCMConditions()
                               Make the following functions public for
                                                           mode ajout:
                               fnIndiciaReadyConditionsCheck()
                               fnIndiciaReadyConditionsCheck2()
                               Move mode ajout redirection to fnpIndiciaReadyCheck
    04/03/2007  Derek DeGennaro Added PSOC Please Wait Screen.
    03/29/2007  Oscar Wang     Changes for Rates Change Prompt Feature.
                               Modified function fnIndiciaReadyConditionsCheck.
    29 Mar 2007 Bill Herring    Modified fnIndiciaReadyConditionsCheck() to activate
                                power interrupt report for and Meter Move detection
                                for France.
    03/27/2007  Adam Liu       Added Abacus related error message string in fnUpdateErrorStatus() 
    03/22/2007   Adam Liu      Added functions for Abacus Home screens
    03/05/2007  I. Le Goff     In fnevMailJobCompleted(), fnIndiciaReadyConditionsCheck
                               add modification for mode ajout
                               Make the following functions public for
                                                           mode ajout:
                                                           fnUpdateDisablingWarningConditions()
                                                           fnUpdateWarningMessages()
                                                           fnCheckMissingGraphics()
                                                           fnCheckInspection()
                                                           fnUpdateErrorMessages()
                                                           fnUpdateCMWarnings()
                                                           fnPostCheckCMConditions()
                                                           fnPostCheckInspection()
                                                           fnRequestCMStatus()
    02/12/2006  Kan Jiang      Add the calling of fnGetPresetNextScr in function 
                               fnIndiciaReadyConditionsCheck for Fraca 114529 
    02/09/2007  Kan Jiang      Modified function fnIndiciaReadyConditionsCheck for fixing 
                               Fraca GMSE00114529
    02/07/2007  Vincent Yi     Added functions fnpPostReportsPrint(), modified fncReportsPrintCheck()
    01/19/2007  Oscar Wang     1. Added functions fnhkMfgToService and 
                               fnhkServiceToMfg for manufacturing screen.
                               2. Added unsent EService record in Seal Only/
                               Permit/Time-Text-AD/Diff. Weighing mode to fix
                               FRACA GMSE00113167.
    01/18/2007  Kan Jiang      Modified function fnIndiciaReadyConditionsCheck2() for fixing Fraca 113169
    01/11/2007  Oscar Wang     Fix FRACA GMSE00112739: check Meter lock 
                               prior to entering "Remove mail on scale".
                               Modified fnpDiffWeighPieceCheck().
    01/08/2007  Vincent Yi     Modified functions fnevMailJobCompleted() 
    01/08/2007  Kan Jiang      Modified function fnReportsPrintConditionsCheck() to 
                               fix Fraca 112344.
    12/15/2006  Raymond Shen   Implemented ZWZP feature: 
                               Modified fnevMailJobCompleted(), fnIndiciaReadyConditionsCheck(),
                               fnIndiciaReadyConditionsCheck2(), fnUpdateErrorStatus().
    12/15/2006  Adam Liu       Modified fnUpdateErrorStatus() by adding error
                               ERROR_STR_INFRA_UPDATE_REQUIRED.
                               Add "Update Required" check in prefunctions of 
                               all printing modes.
    12/15/2006  Oscar Wang     Added "Need Class" check on main screen.
    12/14/2006  Adam Liu       Add DCAP support on main screens.
                               New functions:
                               fnpDataCenterOptionsMenu()
                               fnevDCAPDoFlashWrite()
                               Modified functions:
                               fnIndiciaReadyConditionsCheck()
                               fnIndiciaReadyConditionsCheck2()
                               fnUpdateWarningStatus()
                               fnUpdateErrorStatus()
                               
    12/12/2006  Oscar Wang     Modified function fnIndiciaReadyConditionsCheck2
                               and fnIndiciaReadyConditionsCheck to support KIP
                               class and KIP password.
    12/13/2006  Vincent Yi      Modified function fnevMailJobCompleted() for Midnight
                                crossing event
    12/07/2006  Kan Jiang      Modified the following functions for Auto Daylight Saving:
                               fnIndiciaReadyConditionsCheck2()
                               fnOptionalPrintModeConditionsCheck2()
                               fnPermitModeConditionsCheck2()
    12/06/2006  Vincent Yi      Added function fnpReportPHTapePrinting()
    11/27/2006  Kan Jiang      Fix FRACA GMSE00110726 by commented the check of
                               OUT_OF_SERVICE error in the following functions:
                               fnTestPatternConditionCheck2()
                               fnSealOnlyConditionsCheck2()
                               fnReportsPrintConditionsCheck2().
    11/17/2006  Adam Liu       Fix fraca GMSE00106326 by changing the 
                               "printing..." animator to use screen ticks.
                               Add variable ucAnimatorCounter and related functions.
                               Modified fnfIndiciaPrintingStatus() and 
                               fnevUpdateWarningMessages(), fnpIndiciaPrinting().
    11/15/2006  Oscar Wang     Fix FRACA GMSE00102800 by checking high value
                               warning prior to other disable conditions.
                               modified function fnIndiciaReadyConditionsCheck2.
    11/15/2006  Vincent Yi      Fixed fraca 106327 106330,
                                Added mode check before transition to High 
                                Postage Warning screen, only for KIP mode.
    09/11/2006  Adam Liu        Modified fnPermitModeMenuInit() and 
                                fnhkExitPermitMode() to restore and reset PermitScreenPageNo.
    09/06/2006  Vincent Yi      Removed midnight crossover event check in 
                                fnIndiciaReadyConditiosCheck()
    09/08/2006  Oscar Wang      Modified fnIndiciaReadyConditionsCheck2 to add
                                new rate effective condition check.
    09/06/2006  Vincent Yi      Added midnight crossover event check in 
                                fnIndiciaReadyConditiosCheck()
    09/05/2006  Dicky Sun       Update function fnIndiciaReadyConditionsCheck
                                for GTS deleting manifest record. 
    09/05/2006  Kan Jiang       Modified function fnGetInspectionDays() to fix fraca 103435.

    09/04/2006  Raymond Shen    Added scale location code condition check in function 
                                fnIndiciaReadyConditionsCheck2() to fix fraca 103460.

    08/28/2006  Vincent Yi      Added code to fnTransToErrorScreens to transition 
                                to Tape error screen when paper jam error occurs 
                                during printing tape
    08/16/2006  Vincent Yi      Updated fnTransToErrorScreens for more error types
    08/15/2006  Vincent Yi      Added function fnTransToErrorScreens
    08/10/2006  Dicky Sun       Update the following functions for Clear key
                                and Home key in GTS screens:
                                      fnIndiciaReadyConditionsCheck
                                      fnUpdateDisablingWarningConditions 
    07/27/2006  Raymond Shen    Fix fraca GMSE00100293: added the logic of changing
                                "duck date-time" setting in function fnhkTogglePrintDateTime.
    07/27/2006  Oscar Wang      Modified function fnIndiciaReadyConditionsCheck
                                for re-rate when going back to home screen.
    07/26/2006  Derek DeGennaro Change to Differential Weighing Mode for DM400C.
                                Changed fnevMailPieceCompleted so that on a DM400C 
                                the OI doesn't transition from the printing screen 
                                until after the CM/SYS tells the OIT that the entire mail 
                                run is complete.  Without this change the OIT transitions 
                                to the next DWM prompt even while the transport is still running.
    07/25/2006  Dicky Sun       Update function fnpIndiciaTransient for GTS PH
                                receipt printing.
    07/20/2006  Vincent Yi      Added code for checking "Meter Not Authorized"
                                disabling condition and displayed at footer area
    07/18/2006  Clarisa Bellamy Merge changes from cl501be branch:
    07/17/2006  Clarisa Bellamy branch cl501be
                                - In fnhkTogglePrintDateTime(), add call to 
                                fnSYSAllowRepeatOIEvent so that bobtask will 
                                get the Gen Static Image request, even though 
                                we never left the print ready state. This allows 
                                the first print run after toggling the date/time
                                stamp on/or off, to print correctly.
    07/18/2006   Dicky Sun      Update function fnIndiciaReadyConditionsCheck2
                                for GTS adding record mode.
    07/18/2006   Oscar Wang     Added function fnevMailPieceCompleted().
    
    07/12/2006   Kan Jiang      Added the code to check the Auto Date Advance message 
                                in the pre funciton of main screen.
    07/10/2006  Raymond Shen    1. Added error status updating function for the
                                routine that error occurs in fncIndiciaReadyCheck()
                                2. Remove call of fnUpdateErrorMessages() in 
                                fnpIndiciaError().
    07/08/2006  Dicky Sun       1.Add ERROR_ZIP_BARCODE_REQUIRE define for GTS
                                2.Updated the following functions for GTS:
                                    fnIndiciaReadyConditionsCheck2()
                                    fnUpdateDisablingWarningConditions()
                                    fnUpdateErrorStatus()
    07/04/2006  Oscar Wang      Updated function fnpDiffWeighPieceCheck()
    06/30/2006  Raymond Shen    Modified fncIndiciaReadyCheck() and added
                                fnpIndiciaError() to fix fraca 99703
    06/22/2006  Raymond Shen    Modified function fnpIndiciaReadyCheck() and 
                                fnIndiciaReadyConditionsCheck() for OOB.
    05/31/2006  Steve Terebesi  Merged from FPHX_01.02_C1_Shelton_ST001TE
    05/31/2006  Steve Terebesi  Merged from FPHX_01.02_c1_shelton
    05/24/2006  Steve Terebesi  Added include file for new disabled status bits
                                and new function calls
    05/11/2006  Steve Terebesi  Updated to call fnGetSysMode instead of OI mode
    12/27/2005  Vincent Yi      Initial version

//-----------------------------------------------------------------------------
    OLD PVCS REVISION HISTORY
 * \main\jnus11.00_i1_shelton\7 cx17598 19 oct 2005
 *
 *  Check the RR Sync status whatever current print mode is if a pending flag is set.
 *
*************************************************************************/


/**********************************************************************
        Include Header Files
**********************************************************************/
//#include "abacuscommtask.h"
//#include "AbacusGDM.h"
//#include "abacusopermanager.h"
//#include "AbacusTypes.h"
//#include "abacusXBAPI.h"
//#include "AbacusXCM.h"

#include "aiservic.h"
#include "bob.h"
#include "bobutils.h"
#include "clock.h"
#include "cmos.h"
#include "cwrapper.h"
#include "datdict.h"
//#include "deptaccount.h"
#include "fwrapper.h"
#include "ipsd.h"
#include "oi_intellilink.h"
//#include "oiabacus2.h"          // fnGetAbacusMXSelectClass()
//#include "OiAbacus5.h"
#include "oiclock.h"
#include "oicmenucontrol.h"
#include "oicommon.h"
//#include "oideptaccount.h"
#include "oierrhnd.h"
#include "oifields.h"
//#include "oifpimage.h"
#include "oifpinfra.h"
#include "oifpmain.h"
#include "oifpprint.h"
//#include "oioob.h"
#include "oipostag.h"
//#include "oipreset.h"
#include "oipwrup.h"
//#include "oirefill.h"
//#include "oigts.h"
//#include "oireport.h"
//#include "oirateservice.h"
//#include "oirateutils.h"
#include "oiscreen.h"
#include "oit.h"
#include "oitcm.h"
#include "nucleus.h"
#include "permits.h"
#include "pbplatform.h"
//#include "ratesvcdcai.h"
//#include "ratesvcpublic.h"
//#include "ratetask.h"
#include "sys.h"
#include "sysdata.h"
#include "sysdatadefines.h"

#include "cmpublic.h"
#include "oientry.h"
#include "oifields.h"
#include "ajout.h"//for ajout api

//#include "oiweight.h"
//#include "oifpmenu.h"
#include "features.h"
#include "exception.h"
#include "errcode.h"
//#include "AbacusXCM.h"
#include "flashutil.h"
#include "cJSON.h"
#include "wsox_tx_queue.h"
#include "wsox_services.h"
#include "syspriv.h"
#include "api_status.h"
#include "utils.h"



#define THISFILE "oifpmain.c"

/**********************************************************************
        Local Function Prototypes
**********************************************************************/
static SINT8 fncIndiciaReadyCheck( void         (** pContinuationFcn)(),  
                                   UINT32       *   pMsgsAwaited,
                                   T_SCREEN_ID  *   pNextScr );


static SINT8 fncSealOnlyCheck( void         (** pContinuationFcn)(), 
                               UINT32       *   pMsgsAwaited,
                               T_SCREEN_ID  *   pNextScr );
static T_SCREEN_ID fnSealOnlyConditionsCheck(void);
static T_SCREEN_ID fnSealOnlyConditionsCheck2(void);

static SINT8 fncOptionalPrintModeCheck ( void       (** pContinuationFcn)(), 
                                         UINT32         *   pMsgsAwaited,
                                         T_SCREEN_ID    *   pNextScr );
static T_SCREEN_ID fnOptionalPrintModeConditionsCheck(void);
static T_SCREEN_ID fnOptionalPrintModeConditionsCheck2(void);

static T_SCREEN_ID fnPermitModeConditionsCheck(void);
static T_SCREEN_ID fnPermitModeConditionsCheck2(void);
static SINT8 fncPermitModeCheck ( void       (** pContinuationFcn)(), 
                                         UINT32         *   pMsgsAwaited,
                                         T_SCREEN_ID    *   pNextScr );

static SINT8 fncReportsPrintCheck ( void       (** pContinuationFcn)(), 
                                    UINT32         *   pMsgsAwaited,
                                    T_SCREEN_ID    *   pNextScr );
static T_SCREEN_ID fnReportsPrintConditionsCheck(void);
static T_SCREEN_ID fnReportsPrintConditionsCheck2(void);

static void fnHomeScreenMenuInit (BOOL fUpdatePageLEDs);
static void fnOptionalPrintModeMenuInit (BOOL fUpdatePageLEDs);
static void fnPermitModeMenuInit (BOOL fUpdatePageLEDs);
static void fnAbacusHomeScreenMenuInit (BOOL fUpdatePageLEDs);
static void fnAbacusPrintingScreenMenuInit (BOOL fUpdatePageLEDs);

BOOL fnIsAdAllowedInPermitMode(void);
BOOL fnIsInscAllowedInPermitMode(void);
BOOL fnIsTxtMsgAllowedInPermitMode(void);
BOOL fnCanPermitCountOnOff(void);
BOOL fnCanPermitTownCircleOnOff(void);
BOOL fnCanPermitDateOnOff(void);

BOOL fnCheckBatchAvailable (void);
BOOL fnCheckAbacusAvailable (void);

static void fnGetNextDisplayedMessage (BOOL   * pStatusTable,
                                       UINT16   usTableSize,
                                       BOOL     fInit,
                                       UINT16 * pDisplayedIndex,
                                       UINT16 * pDisplayedSeqNo);

static void fnUpdateCMWarnings(void);
 void fnUpdateDisablingWarningConditions (void);
static void fnSealOnlyUpdateConditions (void);

static BOOL fnOITCheckPSDMarriedStatus(void);

T_SCREEN_ID fnCheckInspection (void);
T_SCREEN_ID fnPostCheckInspection (void);
static T_SCREEN_ID fnPostCheckEndOfLife (void);

T_SCREEN_ID fnCheckPSDState (void);

static T_SCREEN_ID fnCheckMissingGraphics (void);

static T_SCREEN_ID fnPostCheckCMConditions (void);

static T_SCREEN_ID fnCheckPSOCDisablingConditions (void);

static BOOL fnCheckIsPrintDateTimeOn (void);
static void fnUpdateOptionalPrintMode (void);

static void fnUpdateWarningStatus(void);
static void fnUpdateErrorStatus(void);
static UINT8 fnIsIncidentReportNeeded( void );
static BOOL fnCheckPieceIDHome(void);
static BOOL fnCheckReplyCodeHome(void);
static BOOL fnNoLongerInZeroPrint(void);
static BOOL fnCheckBatchHome(void);
static BOOL fnCheckServicesHome(void);

static T_SCREEN_ID fnCheckBatteryState(void);

static BOOL fnIsDataCenterUploadEnabled(void);

static BOOL fnCheckBatchHome(void);
static BOOL fnCheckServicesHome(void);
static BOOL fnIsCanadaPackageServie(void);

#define CRLF "\r\n"

/**********************************************************************
        Local Defines, Typedefs, and Variables
**********************************************************************/
#define SIZE_MSG_COUNTER                3

#define MAX_WARNING_NUM                 32
#define MAX_ERROR_NUM                   64

// Warning string list in pre-defined sequence
// !!!This sequence is same as the one in the warning message field, both of
//    them should match the sequence defined in UI spec 
#define WARNING_STR_INSPECTION_DUE          0
#define WARNING_STR_LOW_FUNDS               1
#define WARNING_STR_LOW_INK                 2
#define WARNING_STR_INK_TANK_EXPIRING       3
#define WARNING_STR_WASTE_TANK_NEAR_FULL    4
#define WARNING_STR_PSD_BATTERY_LOW         5
#define WARNING_STR_DCAP_UPLOAD_DUE         6
//#define WARNING_STR_      7   //Reserved for Low Credit
#define WARNING_STR_TRANS_LOG_NEAR_FULL             8
#define WARNING_STR_ABACUS_HOST_DISCONNECTED        9
#define WARNING_STR_END_OF_LIFE_WARNING     10
#define WARNING_STR_BARCODE_LOW             11
#define WARNING_STR_TRACKING_LOG_NEAR_FULL  12

// Error string list in pre-defined sequence
// !!!This sequence is same as the one in the error message field, both of
//    them should match the sequence defined in UI spec 
#define ERROR_STR_PAPER_IN_TRANSPORT        0
#define ERROR_STR_NO_INK_TANK               1
#define ERROR_STR_NO_PRINTHEAD              2
#define ERROR_STR_INSUFFICIENT_FUNDS        3
#define ERROR_STR_FEEDER_COVER_OPEN         4
#define ERROR_STR_INK_OUT                   5
#define ERROR_STR_INK_TANK_ERROR            6
#define ERROR_STR_PRINTHEAD_ERROR           7
#define ERROR_STR_WASTE_TANK_FULL           8
#define ERROR_STR_OUT_OF_SERVICE            9
#define ERROR_STR_INSPECTION_REQUIRED       10
#define ERROR_STR_PRINTER_ERROR             11
#define ERROR_STR_TANK_LID_OPEN             12
#define ERROR_STR_JAM_LEVER_OPEN            13
#define ERROR_ZIP_BARCODE_REQUIRE           14
#define ERROR_STR_AUTHR_REQUIRED            15
#define ERROR_STR_MISSING_GRAPHIC           16
#define ERROR_STR_DCAP_UPLOAD_REQUIRED      17
#define ERROR_STR_INFRA_UPDATE_REQUIRED     18
#define ERROR_STR_ZWZP_PLACE_ITEM           19
#define ERROR_STR_ZWZP_ENTER_WEIGHT         20

#define ERROR_STR_SELECT_ACCOUNT            21
#define ERROR_STR_ACCT_PERIOD_ENDED         22
#define ERROR_STR_ACCT_PERIOD_INVALID       23
#define ERROR_STR_TRANSACTION_LOG_FULL      24
#define ERROR_STR_ENTER_SELECT_JOB_ID       25
#define ERROR_STR_ENTER_SELECT_JOB_ID_1     26
#define ERROR_STR_SELECT_JOB_ID_1           27
#define ERROR_STR_ENTER_SELECT_JOB_ID_2     28
#define ERROR_STR_AGD_SETUP_FAILED          29
#define ERROR_STR_ENTER_DUNS_NUMBER         30
#define ERROR_STR_ENTER_AJOUT_BATCH         31
#define ERROR_STR_PIECE_COUNT_EOL           32
#define ERROR_STR_ZERO_MANUAL_WEIGHT        33
#define ERROR_STR_PAPER_IN_WOW_TRANSPORT    34

#define         MAX_ANIMATOR_COUNT   4    //max value for ucAnimatorCounter

// Only record missed midnight error for a this many days in a row.  
//  Don't record again until it doesn't happen (clears the flag) or 
//  it happens 255 days in a row.
#define DAYS_MISSED_MIDNIGHT_RECORDED   5


static UINT16   usDisplayedWarningIndex;// The index of current displayed 
                                        // warning in the FieldText list
static UINT16   usTotalWarnings;        // The total number of occurred warnings
static UINT16   usDisplayedWarningSeqNo;// The sequence number of current displayed
                                        // warning in all occurred warnings

static UINT16   usDisplayedErrorIndex;  // The index of current displayed error 
                                        // in the FieldText list
static UINT16   usTotalErrors;          // The total number of occurred errors
static UINT16   usDisplayedErrorSeqNo;  // The sequence number of current displayed
                                        // error in all occurred errors

static BOOL     pWarningsStatus[MAX_WARNING_NUM];   
static BOOL     pErrorsStatus[MAX_ERROR_NUM];

static UINT8    ucMsgDisplayDivisor;    // Used for message display delay
static UINT8    ucAnimatorCounter = 0;    // Used for display animate char

static unsigned short * pwTempFeeName = NULL;
static unsigned char     bFeeNameSpaceAllocated = false;

static BOOL     fInRunningScreen = FALSE;// Flag to check whether in running/
                                         // printing screens or not

 BOOL     fNoInkTank = FALSE;
 BOOL     fNoPrintHead = FALSE;
 BOOL     fSkipPaperInTranport = FALSE;
 
static BOOL     fPSDBatteryLowShown         = FALSE;
static BOOL     fAuthrRequiredShown         = FALSE;
static BOOL     fInspectionWarningShown     = FALSE;
static BOOL     fInspectionRequiredShown    = FALSE;
static BOOL     fInkOutShown                = FALSE;
static BOOL     fInkTankErrShown            = FALSE;
static BOOL     fNoInkTankShown             = FALSE;
static BOOL     fPrintHeadErrShown          = FALSE;
static BOOL     fNoPrintHeadShown           = FALSE;
static BOOL     fPrinterFaultShown          = FALSE;
static BOOL     fWasteTankFullShown         = FALSE;
static BOOL     fWasteTankNearFullShown     = FALSE;
static BOOL     fInkTankExpiringShown       = FALSE;
static BOOL     fGTSManifestPromptShown     = FALSE;
// For Power up case, we don't check this unstable case since the weight is often not 
// stable when loading GTS normal preset.In this case, we don't want to display "Place
// item on scale"
static BOOL     fCheckUnStableWeigh         = FALSE;
       BOOL     fDCAPUploadReqShown         = FALSE;
       BOOL     fDCAPUploadDueShown         = FALSE;
       BOOL     fLowBarcodeWarning          = FALSE;
       BOOL     fLowRecordSpaceWarning      = FALSE;
static BOOL     fEOFErrShown                = FALSE;
static BOOL     fEOFWarnShown               = FALSE;
static BOOL     fPaperErrorShown = FALSE;
       BOOL     fWrongUsbPlatform = FALSE;
       BOOL     fUploadInProgress			= FALSE;
       BOOL     fNoTabletConnection         = TRUE;
       BOOL     fTrmFatalError         		= FALSE;
	   BOOL     bMidnightProcessingStart    = FALSE;
       BOOL     fFileSystemCorruption  		= FALSE;
static UINT8    pMissingGraphicList[MISSING_GRAPHIC_LIST_SZ];

static BOOL     fPrintDateTimeOn = TRUE;

// Used to check if German Piece ID is Ducked by EService routine.
static BOOL     fPieceIDIsDuckedByEService = FALSE;

// Used to store PSD/CMOS battery status
static UINT8   ucOITBatteryStatus = BOTH_BATTERIES_OK_STATUS;

// Used to check if host status changed
static UINT8   ucHostStatus = 0;

// Only record a missed midnight event once a day.
static DATETIME sLastMissedMidnightErrRecordedDate;

// Only record it for a x days out of every 255, if it happens every time.
static UINT8 bMissedMidnightErrRecordedCount;

BOOL localMidReBootRequired = FALSE;
BOOL dcapMidReBootRequired = FALSE;

/**********************************************************************
        Global Functions Prototypes
**********************************************************************/
extern BOOL fnOITIsMeterMoveOutOfRegion(void);//For meter move region check

//TODO JAH Remove dcapi.c extern error_t fnDCAPTimerExpired(void);
extern BOOL fnArePrinterFilesPresent(void);
extern BOOL fnUSUPIsUsbFilePresent( void );

extern unsigned char fnCheckPostCorrectionVal(double dblAmtEntered);
//TODO JAH remove dcperiod.c extern DATETIME fnDCAP_PM_NormaliseDate(DATETIME *pDateTime);
extern BOOL fnGetSysTaskPwrupSeqCompleteFlag( void );
extern unsigned long fnInfraGetTimeSyncStatus( void );

/**********************************************************************
        Global Variables
**********************************************************************/

//Add a new flag to indicate that the fees detail screen comes from the review selection screen.
BOOL fCanadaPackServFlag = FALSE;

BOOL            fIsFromManufacturing = FALSE;

unsigned long lwPsdState     = 0xFFFFFFFF;

//unsigned char bPreviousNonSealMode = PMODE_SEAL_ONLY;
BOOL fForceReRate = FALSE;
BOOL fNeedStartMailRun = FALSE;
DC_UPLOAD_STATUS_T  DCAPUploadStatus;
unsigned short  usDcapRRStatus = 0;
BOOL        fRRStatusPending = FALSE;

BOOL fSetScaleLocCodePending = FALSE;

ulong       lwAppActualCheckSum = 0xFFFFFFFF; // Actual Application CheckSum

/* declarations */
//unsigned long lwDisablingConditions = 0xFFFFFFFF;
//unsigned long lwDisablingConditions2 = 0xFFFFFFFF;
//unsigned long lwWarningConditions = 0xFFFFFFFF;


//extern LAST_KNOWN_RATING_CONFIG sLastKnownRatingConfig;

/* externs */
extern UINT8    pOldManualWeight [LENWEIGHT];

extern unsigned char bEnterFromScreen;
extern BOOL fMeterLocked;
extern BOOL fInkLowWarned;
extern BOOL fHighPostagePending;
extern BOOL fLowFundsWarningPending;
extern BOOL fLowFundsWarningDisplayed;
extern BOOL fLowPieceCountWarningPending;
extern BOOL fLowPieceCountWarningDisplayed;

//extern BOOL fInOOBMode;
extern BOOL fRecheckPrinterStatus;
extern BOOL                 fDiffWeighPieceNeeded;  /* used to trigger the 'remove one mailpiece' prompt */
extern BOOL bFromRemoveItem;
extern BOOL            fCMOSPresetRatesUpdated;
extern BOOL               fCMOSDSTAutoUpdated;
//extern unsigned char bLastPaperStatus;

/* kip carrier */
extern BOOL             fKIPLastScreenClass;
extern BOOL             fKIPCarrierChanged;

extern char             marriedVaultNumber[16];

extern unsigned short   usDcapWriteStatus;
extern unsigned short   usDcapActionStatus;

// the last platform status and weight that we have processed with */
extern unsigned char    pLastBCDWeight[LENWEIGHT]; /* clear to zero */
extern unsigned char    bLastPlatformStatus;
extern BOOL fMayNeedRerate;

// the meter moved status
extern BOOL fOITMeterMoved;
extern BOOL fEServiceRecordUploaded;

extern BOOL     oiAbacusMXClassSet;

extern DATETIME CMOSLastLocalMidnightDT;
extern DATETIME CMOSLastDCAPPeriodCheckDT;

extern BOOL fCMOSAbacusAcntProblem;
extern unsigned char ucCMOSAbacusAcntErrorCode;

//extern AXB_RATING_INFO      sAbPieceData;

// G922 UK auto advance date new feature.

extern BOOL CMOSRatesExpired;
extern BOOL bRateExpHasBeenPrompted;
extern unsigned char bNumPreviouslyActiveRates;
extern UINT8 bNumOfActiveRates;
extern UINT8 bNumOfActiveDcaps;


extern BOOL bGoToReady;
extern BOOL NoEMDPresent;
extern BOOL NoGARFilePresent;

extern BOOL     fTrmFatalError;
/**********************************************************************
        Public Functions
**********************************************o************************/
/* Utility functions */


/* *************************************************************************
// FUNCTION NAME: 
//      fnTransToErrorScreens
//
// DESCRIPTION: 
//      Transition to corresponding error screen according to input error type 
//      and code.
//
// INPUTS:  
//      ulErrType -     Error Type 
//      ulErr     -     Error code
//
// RETURNS:
//      Error screen id
//
// NOTES:
//
// MODIFICATION HISTORY:
//  07/20/2009  Raymond Shen    Add code to let "Cancel" key in Paper/Tape error screen really
//                            work: go to Home screen with corresponding disabling condition.
//  08/28/2006  Vincent Yi  Added code to transition to Tape error screen when
//                          paper jam error occurs during printing tape
//  08/15/2006  Vincent Yi  Initial version
//      
// *************************************************************************/
T_SCREEN_ID fnTransToErrorScreens (UINT32 ulErrType, UINT32 ulErr)
{
    T_SCREEN_ID     usNext = 0;
    UINT8           ucLastPrintEvent = fnGetResumingPrintEvent();
    
    switch (ulErrType)
    {
    case ERTYPE_WARNING:
        switch (ulErr)
        {
        case WRN_WTANK_FULL:
            fWasteTankFullShown = TRUE;
            usNext = GetScreen (SCREEN_WASTETANK_FULL);
            break;
        default:
            break;
        }
        break;

    case ERTYPE_PAPER_ERR:
        switch (ulErr)
        {
        case ERR_JAM_LEVER_OPEN:
            usNext = GetScreen (SCREEN_JAM_LEVEL_OPEN);
            break;
        case ERR_JAM:
            // During printing tape, this paper error will lead to tape error
            // screen
            if ((ucLastPrintEvent == OIT_PRINT_TAPE)        ||
                (ucLastPrintEvent == OIT_PRINT_TAPE_PERMIT) || 
                (ucLastPrintEvent == OIT_PRINT_TAPE_TEST)   || 
                (ucLastPrintEvent == OIT_PRINT_RPT_TO_TAPE))
            {
                if(fPaperErrorShown == FALSE)
                {
                    fPaperErrorShown = TRUE;
                usNext = GetScreen (SCREEN_TAPE_ERRORS);
            }
            else
            {
                    usNext = GetScreen (SCREEN_MM_INDICIA_READY);
                }
            }
            else
            {
                if(fPaperErrorShown == FALSE)
                {
                    fPaperErrorShown = TRUE;
                    usNext = GetScreen (SCREEN_PAPER_ERROR);
                }
                else
                {
                    usNext = GetScreen (SCREEN_MM_INDICIA_READY);
                }
            }
            break;
        default:
            if(fPaperErrorShown == FALSE)
            {
                fPaperErrorShown = TRUE;
                usNext = GetScreen (SCREEN_PAPER_ERROR);
            }
            else
            {
                usNext = GetScreen (SCREEN_MM_INDICIA_READY);
            }
            break;
        }
        break;

    case ERTYPE_PHEAD_ERR:
        switch (ulErr)
        {
        case ERR_NO_PHEAD:
            fNoPrintHeadShown = TRUE;
            usNext = GetScreen (SCREEN_NO_PRINTHEAD);
            break;
        default:
            fPrintHeadErrShown = TRUE;
            usNext = GetScreen (SCREEN_PRINTHEAD_ERROR);
            break;
        }
        break;

    case ERTYPE_INK_OR_TANK_ERR:
        switch (ulErr)
        {
        case ERR_INK_OUT:
            fInkOutShown = TRUE;
            usNext = GetScreen (SCREEN_INK_OUT);
            break;
        case ERR_NO_INKTANK:
            fNoInkTankShown = TRUE;
            usNext = GetScreen (SCREEN_NO_INKTANK);
            break;
/*      case WRN_INKTANK_MRTR_TIMEOUT:
        case WRN_INKTANK_INST_TIMEOUT:
            fInkTankExpiringShown = TRUE;
            usNext = GetScreen(SCREEN_INKTANK_EXPIRING);
            break;          
*/
        default:
            fInkTankErrShown = TRUE;
            usNext = GetScreen (SCREEN_INKTANK_ERROR);
            break;
        }
        break;

    case ERTYPE_FATAL_ERR:
        fPrinterFaultShown = TRUE;
        usNext = GetScreen (SCREEN_PRINTER_FAULT);
        break;

    case ERTYPE_TAPE_ERR:
        if(fPaperErrorShown == FALSE)
        {
            fPaperErrorShown = TRUE;
        usNext = GetScreen (SCREEN_TAPE_ERRORS);
        }
        else
        {
            usNext = GetScreen (SCREEN_MM_INDICIA_READY);
        }
        break;

    case ERTYPE_FEEDER_ERR:
        break;

    default:
        break;
    }

    return usNext;      
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnResetAllWarningCheckFlags
//
// DESCRIPTION: 
//      Utility function to reset all flags for checking warning conditions
//      in pre-function of Home screen.
//
// INPUTS:
//      None.
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  03/10/2008  Oscar Wang      Clear EOL flags.
//  08/16/2007  Oscar Wang      Clear flag fGTSManifestPromptShown.
//  8/01/2007   Joey Cui        Add fOITInfraLogRecordsFullReqShown and 
//                              fOITInfraNOTrackingReqShown  
//  2/21/2006   Vincent Yi      Initial version
//
// *************************************************************************/
void fnResetAllWarningCheckFlags (void)
{
    fLowFundsWarningDisplayed = FALSE; // reset low funds warning displayed flag
    fLowPieceCountWarningDisplayed = FALSE; // reset low piece count warning displayed flag

    fPSDBatteryLowShown         = FALSE;
    fInspectionWarningShown     = FALSE; // reset the inspection warning displayed flag
    fInspectionRequiredShown    = FALSE;
    fInkOutShown                = FALSE;
    fInkTankErrShown            = FALSE;
    fNoInkTankShown             = FALSE;
    fPrintHeadErrShown          = FALSE;
    fNoPrintHeadShown           = FALSE;
    fPrinterFaultShown          = FALSE;
    fWasteTankFullShown         = FALSE;
    fWasteTankNearFullShown     = FALSE;
    fInkTankExpiringShown       = FALSE;

    fDCAPUploadDueShown = FALSE; // reset the inspection warning displayed flag

    // reset the warning flags for GTS warnings.
    fLowRecordSpaceWarning = FALSE;
    fLowBarcodeWarning = FALSE;
    
    fGTSManifestPromptShown = FALSE;

    fEOFErrShown = FALSE;
    fEOFWarnShown =  FALSE;
    fPaperErrorShown = FALSE;

    fUploadInProgress =  FALSE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnResetInspectionCheckFlags
//
// DESCRIPTION: 
//      Utility function to reset inspection flags for checking warning conditions
//      in pre-function of Home screen.
//
// INPUTS:
//      None.
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/31/2006      Joey Cui    Initial Version
//
// *************************************************************************/
void fnResetInspectionCheckFlags (void)
{
    fInspectionWarningShown     = FALSE; // reset the inspection warning displayed flag
    fInspectionRequiredShown    = FALSE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnResetReplacementConditions
//
// DESCRIPTION: 
//      Utility function to reset all flags of error/warnings related to 
//      replacement after replacement finished.
//
// INPUTS:
//      None.
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/15/2006   Vincent Yi      Initial version
//
// *************************************************************************/
void fnResetReplacementConditions(void)
{
    fInkOutShown        = FALSE;
    fInkTankErrShown    = FALSE;
    fNoInkTankShown     = FALSE;
    fPrintHeadErrShown  = FALSE;
    fNoPrintHeadShown   = FALSE;
    fInkTankExpiringShown = FALSE;

//      fInkLowWarned = FALSE;
//      fInkTankTimeoutWarned = FALSE;
//      fWTankNearFullWarned = FALSE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnOITInRunningScreen
//
// DESCRIPTION: 
//      Utility function to set a flag to TRUE when in running/printing screens,
//      this flag will be checked when display warning messages int footer area 
//      in any running/printing screens, because in running screen, the warning 
//      conditions should be update.
//
// INPUTS:
//      TRUE or FALSE
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  2/02/2006   Vincent Yi      Initial version
//
// *************************************************************************/
void fnOITInRunningScreen (BOOL fRunning)
{
    fInRunningScreen = fRunning;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnTestPatternConditionCheck2
//
// DESCRIPTION: 
//      Utility function to check warning & disabling conditions after 
//      requesting CM status when test parttern ready
//
// INPUTS:
//      None
//
// RETURNS:
//      Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/15/2006  Adam Liu        Update for "Update Required" check
//  11/27/2006  Kan Jiang       Commented the check of DCOND_OUT_OF_SERVICE error
//                              to fix Fraca110726
//  07/20/2006  Vincent Yi      Added checking disabling conditions "Meter Not
//                              Authorized" and "Out of Service"
//  1/09/2006   Vincent Yi      Code cleanup and added test pattern error screen
//              Henry Liu       Initial version
//
// *************************************************************************/
// VBL_TBD_FP
T_SCREEN_ID fnTestPatternConditionCheck2()
{
    T_SCREEN_ID usNextScreen = 0;

    fnUpdateDisablingWarningConditions();
    if ((usNextScreen = fnPostCheckCMConditions()) != 0)
    {
        return usNextScreen;
    }
    if ((usNextScreen = fnCheckPSOCDisablingConditions()) != 0)
    {
        return usNextScreen;
    }


    // Errors to be displayed in Home screen should be checked here
    if ((fnIsDisabledStatusSet(DCOND_INK_OUT) == TRUE)            ||    
        (fnIsDisabledStatusSet(DCOND_ACCESSING_PM_NVM) == TRUE)   ||    
        (fnIsDisabledStatusSet(DCOND_INKTANK_ERROR) == TRUE)      ||    
        (fnIsDisabledStatusSet(DCOND_WASTE_TANK_FULL) == TRUE)    ||    
        (fnIsDisabledStatusSet(DCOND_PRINTHEAD_ERROR) == TRUE)    ||    
        (fnIsDisabledStatusSet(DCOND_PAPER_IN_TRANSPORT) == TRUE) ||        
        (fnIsDisabledStatusSet(DCOND_TRANSPORT_OPEN) == TRUE)     ||
        (fnIsDisabledStatusSet(DCOND_PRINTER_FAULT) == TRUE)      ||
        //(fnIsDisabledStatusSet(DCOND_OUT_OF_SERVICE) == TRUE)   ||
        (fnIsDisabledStatusSet(DCOND_AUTHORIZATION_REQ) == TRUE)  ||
        (fnIsDisabledStatusSet(DCOND2_FEEDER_COVER_OPEN) == TRUE) ||
        (fnIsDisabledStatusSet(DCOND2_INFRA_UPDATE_REQUIRED) == TRUE))
    {
        usNextScreen = GetScreen (SCREEN_TEST_PATTERN_ERROR);
    }

    return (usNextScreen);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnTestPatternConditionCheck2
//
// DESCRIPTION: 
//      Utility function to update disabling or warning messages in Test
//      Pattern prefunction
//
// INPUTS:
//      None
//
// RETURNS:
//      Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  1/09/2006   Vincent Yi      Initial version
//
// *************************************************************************/
// VBL_TBD_FP
void fnTestPatternUpdateMessages(T_SCREEN_ID * pNextScr)
{
    if (*pNextScr == 0)
    {
        // Check all warning conditions for displaying the warning messages and
        // counter at the first time
        fnUpdateWarningMessages(TRUE);
    }
    else if (*pNextScr == GetScreen (SCREEN_TEST_PATTERN_ERROR))
    {   
        // Check all error conditions for displaying the error messages and
        // counter at the first time
        fnUpdateErrorMessages(TRUE);
    }
}

BOOL  fnFrankTaskEKPEnable( void )
{
    return ( fnFrancanSense() ) ;
    //return ( TRUE ) ;
}

/* *************************************************************************
// FUNCTION NAME: fnInspectionStatus
// DESCRIPTION: Public function that returns the inspection status of the
//              system date (plus offsets) and the printed date.  The status
//              of the system date will be INSPECTION_REQUIRED if the date
//              is equal to or later than the next inspection date in the
//              vault.  INSPECTION_DUE is returned when the date is earlier
//              than the next inspection date, but within BP_INSPEC_WARNING_VALUE (from flash)
//              days of it.  INSPECTION_OK is returned otherwise.
//              For the printed date, INSPECTION_REQUIRED is returned if the
//              printed date (with date advance) is greater than or equal to 
//              the next inspectino date plus the number of grace period days.
//              INSPECTION_DUE does not apply to the printed date.
//              
//              Note that both inspection statuses are returned in one call
//              for optimization purposes.  This keeps us from having to
//              fetch inspection data from the vault for both system date
//              and printed date checks.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pSysStatus- Return location for the system date inspection status.  Either
//                      INSPECTION_OK, INSPECTION_DUE or INSPECTION_REQUIRED is
//                      returned here.
//          pPrintedStatus- Return location for the printed date inspection status.
//                      INSPECTION_OK or INSPECTION_REQUIRED is returned.
//
// *************************************************************************/
void fnInspectionStatus (unsigned char *pSysStatus, unsigned char *pPrintedStatus)
{
    unsigned char   bInspecEnable;
    unsigned char   pNextInspecDate[4];
    DATETIME        rOurDate;
    DATETIME        rNextInspecDate;
    long            lDateDiff;
    unsigned char   bWarningDays;
    unsigned long   i;
    unsigned char   bDebitGrace = 0;

    /* initialize return values to OK */
    *pSysStatus = INSPECTION_OK;
    *pPrintedStatus = INSPECTION_OK;

    if (fnValidData(BOBAMAT0, VLT_SECDATA_INSP_LOCK_ENABLED, (void *) &bInspecEnable) != BOB_OK)
    {
        fnProcessSCMError();
        goto xit;
    }
    
    /* if inspections are not enabled, we are ok- don't bother checking anything else */
    if (bInspecEnable == 0)
        goto xit;

    /* get the next inspection date from the vault */
    if (fnValidData(BOBAMAT0, VLT_CVA_NEXT_INSPECTION_DATE, (void *) pNextInspecDate) != BOB_OK)
    {
        fnProcessSCMError();
        goto xit;
    }
	
    rNextInspecDate.bHour = 0;
    rNextInspecDate.bMinutes = 0;
    rNextInspecDate.bSeconds = 0;
    rNextInspecDate.bCentury = pNextInspecDate[0];
    rNextInspecDate.bYear = pNextInspecDate[1];
    rNextInspecDate.bMonth = pNextInspecDate[2];
    rNextInspecDate.bDay = pNextInspecDate[3];
    rNextInspecDate.bDayOfWeek = 1; /* we don't care about day of week for this calculation */

    /* ---check the inspection status of the SYSTEM DATE--- */

    /* get the system date and zero out the time */
    GetSysDateTime(&rOurDate, ADDOFFSETS, GREGORIAN);
    rOurDate.bHour = 0;
    rOurDate.bMinutes = 0;
    rOurDate.bSeconds = 0;

    /* calculate difference between our date and the date the next inspection is required */
    CalcDateTimeDifference(&rNextInspecDate, &rOurDate, &lDateDiff);

    /* if the difference in date is zero or greater, it means that the current date
        is equal to or later than the day that the next inspection is required. */
    if (lDateDiff >= 0)
    {
        *pSysStatus = INSPECTION_REQUIRED;
    }
    else
    {
        /* if inspection is not required, check if we are within the warning period.  lDateDiff
            is a negative number indicating the number of seconds that the current date lags
            the date the next inspection is required.  the inspection warning is adjusted from
            days to seconds and compared to see if the warning is due */
        bWarningDays = fnFlashGetByteParm(BP_INSPEC_WARNING_VALUE);
        if ( -lDateDiff <= (60 * 60 * 24) * bWarningDays)
        {
            *pSysStatus = INSPECTION_DUE;
        }
    }


    /* ---check the inspection status of the PRINTED DATE--- */

    /* we need to fetch the debit grace period to check the printed inspection date */
    if (fnValidData(BOBAMAT0, VLT_PURSE_DEBIT_GRACE, (void *) &bDebitGrace) != BOB_OK)
    {
        fnProcessSCMError();
        goto xit;
    }

    /* the printed date can be bDebitGrace period days past the next inspection date, so
        bump up the next inspection date by that many days so we can use the same comparison */
    for (i = 0; i < bDebitGrace; i++)
        AdjustDayByOne(&rNextInspecDate, TRUE);
    
    /* get the printed date and zero out the time */
    GetPrintedDate(&rOurDate, GREGORIAN);
    rOurDate.bHour = 0;
    rOurDate.bMinutes = 0;
    rOurDate.bSeconds = 0;


    CalcDateTimeDifference(&rNextInspecDate, &rOurDate, &lDateDiff);

    /* if the difference in date is zero or greater, it means that the printed date
        is equal to or later than the day that the next inspection is required. */
    if (lDateDiff >= 0)
    {
        *pPrintedStatus = INSPECTION_REQUIRED;
    }

    /* there is no need to check for an inspection warning with the printed date */

xit:
    return;

}




/* *************************************************************************
// FUNCTION NAME: 
//      fnGetInspectionDays
//
// DESCRIPTION: 
//      Utility function which is used to get the amount of days we can advance
//          before Inspection Required
//
// INPUTS:
//      None
//
// RETURNS:
//      the amount of days to Inspection Required
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//    09/05/2006    Kan Jiang    Modified to fix Fraca 103435
//    04/18/2006    Kan Jiang    Intial version.
// *************************************************************************/
SINT32  fnGetInspectionDays (void)
{
    unsigned char   bInspecEnable;
    unsigned char   pNextInspecDate[4];
    DATETIME        rOurDate;
    DATETIME        rNextInspecDate;
    long            lDateDiff;
    unsigned char   bWarningDays;
    unsigned long   i;
    unsigned char   bDebitGrace = 0;

    SINT32 lTmpInspectionDays = 0xff;  // set it as a big value.;

    if (fnValidData(BOBAMAT0, VLT_SECDATA_INSP_LOCK_ENABLED, (void *) &bInspecEnable) != BOB_OK)
    {
        fnProcessSCMError();
        goto xit;
    }
    
    /* if inspections are not enabled, we are ok- don't bother checking anything else */
    if (bInspecEnable == 0)
    {
        goto xit;
    }

    /* get the next inspection date from the vault */
    if (fnValidData(BOBAMAT0, VLT_CVA_NEXT_INSPECTION_DATE, (void *) pNextInspecDate) != BOB_OK)
    {
        fnProcessSCMError();
        goto xit;
    }
    rNextInspecDate.bHour = 0;
    rNextInspecDate.bMinutes = 0;
    rNextInspecDate.bSeconds = 0;
    rNextInspecDate.bCentury = pNextInspecDate[0];
    rNextInspecDate.bYear = pNextInspecDate[1];
    rNextInspecDate.bMonth = pNextInspecDate[2];
    rNextInspecDate.bDay = pNextInspecDate[3];
    rNextInspecDate.bDayOfWeek = 1; /* we don't care about day of week for this calculation */

    /* ---check the inspection status of the PRINTED DATE--- */

    /* we need to fetch the debit grace period to check the printed inspection date */
    if (fnValidData(BOBAMAT0, VLT_PURSE_DEBIT_GRACE, (void *) &bDebitGrace) != BOB_OK)
    {
        fnProcessSCMError();
        goto xit;
    }

    /* the printed date can be bDebitGrace period days past the next inspection date, so
        bump up the next inspection date by that many days so we can use the same comparison */
    for (i = 0; i < bDebitGrace; i++)
        AdjustDayByOne(&rNextInspecDate, TRUE);
    
    /* get the printed date and zero out the time */
    GetSystemDate(&rOurDate, GREGORIAN);
    rOurDate.bHour = 0;
    rOurDate.bMinutes = 0;
    rOurDate.bSeconds = 0;


    CalcDateTimeDifference(&rNextInspecDate, &rOurDate, &lDateDiff);

    /* if the difference in date is zero or greater, it means that the printed date
        is equal to or later than the day that the next inspection is required. */
    lTmpInspectionDays = -lDateDiff/(3600*24) -1;


    /* there is no need to check for an inspection warning with the printed date */
xit:
    return lTmpInspectionDays;

}


/* *************************************************************************
// FUNCTION NAME: 
//      fnClearDisablingWarningConditions
//
// DESCRIPTION: 
//      Utility function to clear various disabling and warning conditions.
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  11/20/2007  Oscar Wang      Move fnClearDisablingWarningConditions() to 
//                              fnCheckEServiceDebitStatus()
//  09/28/2007  Vincent Yi      Added code to clear GTS flag before entering
//                              Home screen.
//  08/24/2007  Joey Cui        Added code to reset printer maint entry ID
//  05/23/2006  Steve Terebesi  Added status bit accessor function calls
//  12/06/2005  Vincent Yi      Code cleanup, rename it from 
//                              fnClearDisablingWarningCondition to 
//                              fnClearDisablingWarningConditions
//              Henry Liu       Initial version
//
// *************************************************************************/
void fnClearDisablingWarningConditions(void)
{
//  Clear all Disabling conditions
    fnClearAllDisabledStatus();

//  Clear all Warning conditions
    fnClearAllWarningStatus();

//    lwDisablingConditions = 0;
//    lwDisablingConditions2 = 0;
//    lwWarningConditions = 0;

    fNoInkTank = FALSE;
    fNoPrintHead = FALSE;

    ucMsgDisplayDivisor = 0;

//    fnResetPrinterMaintEntryID();
}

///* *************************************************************************
//// FUNCTION NAME: 
////      fnOITGetDisablingConditions
////
//// DESCRIPTION: 
////      Utility function to get the value of lwDisablingConditions
////
//// INPUTS:
////      TRUE or FALSE
////
//// RETURNS:
////      None.
////
//// WARNINGS/NOTES:
////
//// MODIFICATION HISTORY:
////  02/23/2006   Vincent Yi      Initial version
////
//// *************************************************************************/
//UINT32 fnOITGetDisablingConditions(void)
//{
//  return lwDisablingConditions;
//}

/* *************************************************************************
// FUNCTION NAME: 
//      fnOITGetDisablingConditions2
//
// DESCRIPTION: 
//      Utility function to get the value of lwDisablingConditions2
//
// INPUTS:
//      TRUE or FALSE
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/31/2006   Vincent Yi      Initial version
//
// *************************************************************************/
UINT32 fnOITGetDisablingConditions2(void)
{
//  return lwDisablingConditions2;
    return 0;
}


/* *************************************************************************
// FUNCTION NAME: fnIsIPSDEnabled
// DESCRIPTION: Utility function, return the enable/disable state of the iButton
//      TRUE -iButton Enabled state
//          FALSE - iButton Disabled state
// AUTHOR: David
//
// *************************************************************************/
// VBL_TBD_FP
BOOL fnIsIPSDEnabled ( void )
{
    uchar ipsdStat ;
    BOOL retVal = FALSE;

    if ( fnValidData( BOBAMAT0, BOBID_IPSD_ENBLDSBLSTAT, (void *)(&ipsdStat) ) != BOB_OK)
    {
        fnProcessSCMError();
        goto xit;
    }

    if ( ipsdStat > 0 )
        retVal = TRUE ;
xit:
    return ( retVal ) ;
    

}

/* *************************************************************************
// FUNCTION NAME: fnCheckPSDPresent
//
// DESCRIPTION: Utility function to set disable status if PSD is present
//
// INPUTS: None
//
// RETURNS: None
//
// *************************************************************************/
void fnCheckPSDPresent ( void )
{
    if (fnGetPsdState(&lwPsdState) == FALSE)
    {
       if(lwPsdState == eIPSDSTATE_INCOMMUNICADO)
       {
           fnPostDisabledStatus(DCOND_NO_PSD);
       }
   }
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetMissingGraphicList
//
// DESCRIPTION:                
//      Utility function that get the pointer of pMissingGraphicList.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of pMissingGraphicList
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//  04/24/2006  Vincent Yi      Initial version
//      
// *************************************************************************/
UINT8 * fnGetMissingGraphicList (void)                  
{
      return pMissingGraphicList;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnInkTankErrShown, fnInkOutErrorShown, fnNoInkTankErrorShown,
//      fnNoPrintHeadErrorShown, fnPrinterFaultShown, fnPrintHeadErrShown
//
// DESCRIPTION:                
//      Utility function that set err-shown flag.
//
// INPUTS:      
//      fShown  - TRUE or FALSE
//
// RETURNS:
//      None.
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//  05/17/2006  Vincent Yi      Initial version
//      
// *************************************************************************/
void fnInkTankErrShown (BOOL fShown)
{
    fInkTankErrShown = fShown;
}

void fnInkOutErrorShown (BOOL fShown)
{
    fInkOutShown = fShown;
}

void fnNoInkTankErrorShown (BOOL fShown)
{
    fNoInkTankShown = fShown;
}

void fnNoPrintHeadErrorShown (BOOL fShown)
{
    fNoPrintHeadShown = fShown;
}

void fnPrinterFaultShown (BOOL fShown)
{
    fPrinterFaultShown = fShown;
}

void fnPrintHeadErrShown (BOOL fShown)
{
    fPrintHeadErrShown = fShown;
}

void fnWasteTankFullShown (BOOL fShown)
{
    fWasteTankFullShown = fShown;
}

void fnWasteTankNearFullShown (BOOL fShown)
{
    fWasteTankNearFullShown = fShown;
}

void fnInkTankExpiringShown (BOOL fShown)
{
    fInkTankExpiringShown = fShown;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnDWPostMailPiecePrinted
//
// DESCRIPTION: 
//      Handling after mail piece printed in diff weight mode.
//
// INPUTS:  
//      None
//
// OUTPUTS:
//      None
//
// RETURNs: 
//      None
//
// MODIFICATION HISTORY:
//  06/21/2007  Vincent Yi  Initial version
//
// *************************************************************************/
void fnDWPostMailPiecePrinted (void)
{
    // now we need the piece again.
    fDiffWeighPieceNeeded = TRUE;
    
/*
    if (fnIsClassSelected() == true)
    {
        UINT8   ucEvent;
        
        fnGetScreenEventById(fnGetCurrentScreenID(), &ucEvent);
        if (ucEvent == OIT_PRINTING)
        {   // only send message for envelope printing
            //Send message to CM when printing finish.
            OSSendIntertask(CM, OIT, OC_RATE_NOT_READY, NO_DATA, NULL, 0);
        }
    }
*/
}


/* *************************************************************************
// FUNCTION NAME: fnOICheck4AbacusErrors
//
// DESCRIPTION: Check the status of Abacus power up flags and show any
//              relevent error screens.
//
// INPUTS:  None
//
// OUTPUTS:
//
// RETURNs: 
//
// MODIFICATION HISTORY:
//  04/17/2007  Joey Cui         Adapted for FP 
//              Craig DeFilippo  Initial version
// *************************************************************************/
T_SCREEN_ID fnOICheck4AbacusErrors(void)
{   
    T_SCREEN_ID     usNext = 0;
    UINT16          usDetailStatus;
    UINT16          usRetVal;
//    AGD_TEMP_INFO*  ptrAGDPowUpStat;

//    usRetVal = fnAGDGetAbacusStatus(&usDetailStatus);

/*
    if(usRetVal == ABACUS_IN_USE)
    {
        ptrAGDPowUpStat = (AGD_TEMP_INFO * )fnAGDGetAbacusPowerUpStatus();  //lint  Warning 605:

        if((ptrAGDPowUpStat->fExtMemNotConnected == TRUE) ||
           ((usDetailStatus & ABACUS_USB_CONNECTION_STATUS_VAL) == 0) )
        {
            usNext = GetScreen(ABACUS_ERR_EXT_MEM_NOT_RESPOND);
        }
 by Joey Cui, ABACUS_ERR_ACCOUNTING_DISCREPANCY2 is not needed any more
        else if(ptrAGDPowUpStat->fExtMemActParamIncompatible == TRUE)
        {
            usNext = GetScreen(ABACUS_ERR_ACCOUNTING_DISCREPANCY2);
        }

        else if(ptrAGDPowUpStat->fFundsDiscrepancyFound == TRUE)
        {
            if(fnGetAccountingType() == (UINT8) ACT_SYS_TYPE_ABACUS)
            {
                usNext = GetScreen(ABACUS_ERR_ACCOUNTING_DISCREPANCY3);
            }
        }
 by Joey Cui, ABACUS_ERR_ACCOUNTING_DISCREPANCY is not needed any more
        else if(ptrAGDPowUpStat->fDiffMemDevConnected == TRUE)
        {
            usNext = GetScreen(ABACUS_ERR_ACCOUNTING_DISCREPANCY);
        }

    }

*/
    return(usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnOITSetBatteryStatus
//
// DESCRIPTION: 
//      Utility function which is used to store the PSD/CMOS battery status
//      into OIT variable ucBatteryStatus.
//
// INPUTS:
//      the value to store in ucOITBatteryStatus
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/18/2008    Raymond Shen    Intial version
//
// *************************************************************************/
void  fnOITSetBatteryStatus(UINT8 ucSatus)
{
    ucOITBatteryStatus = ucSatus;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnSetPaperErrorShownStatus
//
// DESCRIPTION:                
//      Utility function that set "paper error shown" flag.
//
// INPUTS:      
//      fShown  - TRUE or FALSE
//
// RETURNS:
//      None.
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//  07/06/2009  Raymond Shen      Initial version
//      
// *************************************************************************/
void fnSetPaperErrorShownStatus(BOOL fShown)
{
    fPaperErrorShown = fShown;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetPaperErrorShownStatus
//
// DESCRIPTION:                
//      Utility function that get "paper error shown" flag.
//
// INPUTS:      
//      None
//
// RETURNS:
//      Bool.
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//  07/06/2009  Raymond Shen      Initial version
//      
// *************************************************************************/
BOOL  fnGetPaperErrorShownStatus(void)
{
    return fPaperErrorShown;
}

/* Pre/post functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fnpIndiciaReadyCheck
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Home Ready screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr, depends on the conditions checking
//      If no any exception, 
//              *pContinuationFcn = fncIndiciaReadyCheck
//              *pMsgsAwaited = 0 | CM_STATUS_AWAITED_MASK
//      Else, no change on the variables above.
//
// RETURNS:
//      PREPOST_COMPLETE, if screen transition to other screens due to some 
//                          reasons after checking
//      PREPOST_WAITING, if no any exception
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/12/2012  Liu Jupeng     Added code to fix the issue that the meter prints 
//                              the empty envelope.
//  08/11/2010 Clarisa Bellamy Modify missed midnight checking. No longer go to reboot screen.
//                             So only record error once a day, then only 5 days in a row.  If
//                             a recent time adjustment by the user crossed midnight, then 
//                             record that too.
//  03/29/2010  Raymond Shen    Always enable the local midnight event check without caring
//                              whether DCAP is enabled.
//  03/25/2010  Raymond Shen    Check the DCAP midnight event only when DCAP is enabled.
//  02/25/2010  Raymond Shen    Basing on Linda's code, added code to force a system reboot
//                              if midnight event is missed. 
//  03/30/2007  I. Le Goff      Move mode ajout redirection here
//  08/22/2006  Vincent Yi      Added code to clean last print-resume event before 
//                              entering Home screen
//  05/19/2006  James Wang      To request control manager status with a new BR function.
//  11/08/2005  Vincent Yi      Initial version 
//
// *************************************************************************/
SINT8 fnpIndiciaReadyCheck( void        (** pContinuationFcn)(), 
                            UINT32      *   pMsgsAwaited,
                            T_SCREEN_ID *   pNextScr )
{
    DATETIME lastLocalMidnightDT;
    DATETIME lastDCAPPeriodCheckDT;
    DATETIME TodayDT;
    DATETIME sDriftPastMidnightDT;
	long lDateDiff;
    char scratchPad[256];
    SINT8 ucRetval = PREPOST_COMPLETE;


    if(bGoToReady == FALSE)
    {

    	*pNextScr = GetScreen (SCREEN_MM_INDICIA_ERROR);
    	ucRetval = PREPOST_COMPLETE;

    }
    else
    {
    // In case we were doing a balance inquiry and a timeout or sleep occurred.
    fnOiSetBalanceInquiry( FALSE );

    /************************************************************************************************** 
     1. The PLAT_OK_TO_SEND_UPDATE event flag is cleared when the CM receives the 
        PC_AUTOSTART message, to keep the Platform from sending an update message, 
        which might cause a screen update, which could interfere with the debiting 
        process. 
     2. Normally the flag is set by the CM task when CS_S3_COVERED, to indicate 
        that it is now safe for the platform to send update msgs without 
        interfering with the debiting process.
     3. However, when the printing process fails, the CS_S3_COVERED message might 
        not be sent.  So, we set the flag here to allow the PLAT task to resume
        if it was waiting for this flag.  We know it is safe now, because we 
        have already started a screen change process (we are in a screen prefunction.)
    ****************************************************************************************************/ 
    putString("Set PLAT_OK_TO_SEND_UPDATE flag", __LINE__);
    OSSetEvents( PLAT_EVENT_GROUP, (unsigned long)PLAT_OK_TO_SEND_UPDATE, OS_OR );
//TODO - AB - Restore the code below once local midnight processing is properly handled
    ///////////////// This is stuff to detect a missed midnight problem.
    // Get the date of the last midnight processing, and current date, set both times to 0.
    //TODO JAH remove dcperiod.c lastLocalMidnightDT = fnDCAP_PM_NormaliseDate( &CMOSLastLocalMidnightDT );
    (void)GetSysDateTime( &TodayDT, ADDOFFSETS, GREGORIAN );
    //TODO JAH remove dcperiod.c TodayDT = fnDCAP_PM_NormaliseDate( &TodayDT );
#if 0
    // Check if we missed midnight processing....add to exception log in FFS if we did.
	(void)CalcDateTimeDifference(&lastLocalMidnightDT, &TodayDT, &lDateDiff);
	if (lDateDiff > 0)
    {
		// last midnight wasn't today
       	localMidReBootRequired = TRUE;
    }
	else
	{
        localMidReBootRequired = FALSE;
	}
#endif
#if 0 //TODO JAH remove dcap 
	if ( fnDCAPIsDataCaptureActive() == TRUE )
    {
        // Check if we missed DCAP midnight processing (End of period etc.)
        // ... .add to exceptions.log in FFS if we did.
        //TODO JAH remove dcperiod.c lastDCAPPeriodCheckDT = fnDCAP_PM_NormaliseDate(&CMOSLastDCAPPeriodCheckDT);
        
		(void)CalcDateTimeDifference(&lastDCAPPeriodCheckDT, &TodayDT, &lDateDiff);
		if (lDateDiff > 0)
        {
            // last DCAP period check wasn't today
            dcapMidReBootRequired = TRUE;
        }
		else
		{
        	dcapMidReBootRequired = FALSE;
		}
    }
#endif //TODO JAH remove dcap 

    // Log Errors in exception.log file in FFS.
    if( fnGetDriftPastMidnightFlag() == TRUE )
    {
        // If we set the drift past midnight, record that too, then clear the flag.
        fnGetDriftPastMidnightDT( &sDriftPastMidnightDT );
					
        // Log that we have an error after adjusting the drift past midnight.
        sprintf( scratchPad, "Error After Adjusting Drift Past Midnight: YR: %d, MN: %d, DY: %d, %d:%d:%d", 
                             sDriftPastMidnightDT.bYear, sDriftPastMidnightDT.bMonth, sDriftPastMidnightDT.bDay,
                             sDriftPastMidnightDT.bHour, sDriftPastMidnightDT.bMinutes, sDriftPastMidnightDT.bSeconds );
        logException(scratchPad);
					
        // Don't log this possible cause again, unless it happens again.
        fnClearDriftPastMidnightFlag();
    }

    if( (localMidReBootRequired == TRUE) || (dcapMidReBootRequired == TRUE) )
    {
/*
        if( memcmp( &TodayDT, &sLastMissedMidnightErrRecordedDate, sizeof( DATETIME ) ) == 0 )
        {
            // We've already recorded this error, just clear it.
            localMidReBootRequired = FALSE;
            dcapMidReBootRequired = FALSE;
        }
        else
*/
        {
            // This is the first time today we detected this error. Record it. 
            memcpy( &sLastMissedMidnightErrRecordedDate, &TodayDT, sizeof( DATETIME ) );
			
            // After recording it a few days in a row, don't record it.
            if( bMissedMidnightErrRecordedCount < DAYS_MISSED_MIDNIGHT_RECORDED )
            {
                if( localMidReBootRequired )
                {
                    // dates are not the same - must have missed local midnight
                    sprintf(scratchPad, "Local Midnight Missed: last occurred YR: %d, MN: %d, DY: %d  %d:%d:%d", 
                        CMOSLastLocalMidnightDT.bYear, CMOSLastLocalMidnightDT.bMonth, CMOSLastLocalMidnightDT.bDay,
                        CMOSLastLocalMidnightDT.bHour, CMOSLastLocalMidnightDT.bMinutes, CMOSLastLocalMidnightDT.bSeconds);
                    logException(scratchPad);
                }
				
                if( dcapMidReBootRequired )
                {
                    // dates are not the same - must have missed DCAP midnight / period check
                    sprintf(scratchPad, "DCAP Period Chk Missed: last occurred YR: %d, MN: %d, DY: %d  %d:%d:%d", 
                        CMOSLastDCAPPeriodCheckDT.bYear, CMOSLastDCAPPeriodCheckDT.bMonth, CMOSLastDCAPPeriodCheckDT.bDay,
                        CMOSLastDCAPPeriodCheckDT.bHour, CMOSLastDCAPPeriodCheckDT.bMinutes, CMOSLastDCAPPeriodCheckDT.bSeconds);
                    logException(scratchPad);
                }

                // If we missed either midnight stuff, log an error.
                if ( (localMidReBootRequired == TRUE) && (dcapMidReBootRequired == FALSE) )
                {
                    /* log the error */
                    fnReportMeterError(ERROR_CLASS_OIT, ECOIT_LOCAL_MIDNIGHT_MISSED); 
                }

                if ( (localMidReBootRequired == FALSE) && (dcapMidReBootRequired == TRUE) )
                {
                    /* log the error */
                    fnReportMeterError(ERROR_CLASS_OIT, ECOIT_DCAP_MIDNIGHT_MISSED); 
                }

                if ( (localMidReBootRequired == TRUE) && (dcapMidReBootRequired == TRUE) )
                {
                    /* log the error */
                    fnReportMeterError(ERROR_CLASS_OIT, ECOIT_LOCAL_AND_DCAP_MIDNIGHT_MISSED); 
                }
            }
        }
		
        // Clear the flags so we don't put up the reboot screen. (below)
        // At this time, we don't want to display the reboot screen, because  
        //  we've logged the error, and we don't want to freak out the user
        //  for what might not be a problem.
//        localMidReBootRequired = FALSE;
//        dcapMidReBootRequired = FALSE;
        bMissedMidnightErrRecordedCount++;
    }
    else 
    {
        bMissedMidnightErrRecordedCount = 0;   
    }
    ///////////////// End of stuff to detect a missed midnight problem.

    //Query sensor timer will always run after profile is downloaded, but in serive mode
    // the query sensor timer will be stopped, so at this point, make sure the 
    // query sensor timer will be start and can get the sensor status.
//TODO: FIXME Renable Start of query sensor timer when feeder task is working    fnStartQuerySensorTimer();

/*
    if ( (localMidReBootRequired == TRUE) || (dcapMidReBootRequired == TRUE) )
    {
        // Force to reboot if any midnight is missed.
        *pNextScr = GetScreen(SCREEN_SYSTEM_REBOOT_NEEDED);
    }


    // Check if we are recovering from Mode ajout power fail
    else
*/

    if(fnIsIncidentReportNeeded() == FALSE)
    {
        // Go back to ajout screens
//      *pNextScr = fnManageModeAjoutRedirection();
    }
	else
    {
        //empty the stack for saving the screen IDs after error processing
        fnErrClearReturnScreens();
		  
        // Clean last print-resume event
        fnSetResumingPrintFlow(0xFF);
      
        *pNextScr = 0;
        fnClearDisablingWarningConditions();
		  
        // Initializing the error related parameters
        usDisplayedErrorIndex = MAX_ERROR_NUM-1;
        usTotalErrors = 0;
		  
        // Initializing the warning related parameters
        usDisplayedWarningIndex = MAX_WARNING_NUM-1;
        usTotalWarnings = 0;
      
        // Clear main menu check event so CM won't initiate debiting to avoid 2003 error
        OSSetEvents(OIT_EVENT_GROUP, ~(UINT32) MAIN_MENU_CHK_END_EVT, OS_AND);
    
        // Warning or disabling conditions checking
        *pNextScr = fnIndiciaReadyConditionsCheck();
        
        if (*pNextScr == 0)
        {   // No screen transition

            /* request control manager status */
            *pContinuationFcn = (void (*)()) fncIndiciaReadyCheck;
            *pMsgsAwaited = 0 | CM_STATUS_AWAITED_MASK;
            fnRequestCMStatus();


            /* clear this flag after we send the status request message */
            fRecheckPrinterStatus = FALSE;

            /* waiting for cm status response. */
            ucRetval = PREPOST_WAITING;

            *pNextScr = fnIndiciaReadyConditionsCheck2();

            if (*pNextScr == 0)
            {
                // Prepare the menu table
                fnHomeScreenMenuInit(TRUE);
                // Check all warning conditions for displaying the warning messages and
                // counter at the first time
                fnUpdateWarningMessages(TRUE);
                /* Send idle message if PC host is connected */
        //Ram commented not to send PC unsolicited message
        //        (void)fnPCSendUnsolicitedStatus(fnGetCurrentScreenID());
            }
            else if (*pNextScr == GetScreen (SCREEN_MM_INDICIA_ERROR))
            {
                // Check all error conditions for displaying the error messages and
                // counter at the first time
                fnUpdateErrorMessages(TRUE);
            }


            ucRetval = PREPOST_COMPLETE;

        }

        // Set event here too in case it jumped to here to change screens
        OSSetEvents(OIT_EVENT_GROUP, (unsigned long) MAIN_MENU_CHK_END_EVT, OS_OR);
    }
    }

    return (ucRetval);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostIndiciaReady
//
// DESCRIPTION: 
//      Post function that is called when leaving the Home Ready screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  10/24/2009  Raymond Shen    To fix GMSE00171558 (G900 - Loss of Postage reported 
//                              by customer after No Paper message and error 2482),
//                              just stop ZWZP timer and clear ZWZP timer flag
//                              without sending out the message "OIT_RATE_ZWZP_EXPIRED".
//  26 Jul 2007  P.Vrillaud     Stop ZeroWeight timer 
//  11/10/2005  Vincent Yi      Initial version 
//
// *************************************************************************/
SINT8 fnpPostIndiciaReady (void         (** pContinuationFcn)(), 
                           UINT32       *   pMsgsAwaited)
{
    SET_MENU_LED_OFF();

    // make sure timer doesn't restart
//    (void) OSStopTimer( (unsigned char) RATE_ZERO_WEIGHT_TIMER );

    // reset the timer running flag
//    fnSetZWZPTimerFlag(FALSE);

    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME:           fnpIndiciaError
// DESCRIPTION: 
//      Pre function that is called prior to going to the Home Ready with Error
//      screen.
//
// INPUTS:      Standard screen pre-function inputs.
// OUTPUTS:     None
// RETURNS:     Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  2012.10.15 Clarisa Bellamy - Before going to the MMIndiciaErrorAbacus screen, 
//                      check to see if there is a update-from-host pending condition,
//                      and if so, go to the AbacusHostUpdateProgress screen instead. 
//  03/22/2007  Adam Liu                Update from Abacus Error check.
//  07/10/2006  Raymond Shen        Remove call of fnUpdateErrorMessages().
//  06/30/2006  Raymond Shen        Initial version 
//
// *************************************************************************/
SINT8 fnpIndiciaError( void        (** pContinuationFcn)(), 
                       UINT32      *   pMsgsAwaited,
                       T_SCREEN_ID *   pNextScr )
{
    UINT16  usDetailAbacusStatus;
    SINT16  sAbacusStatus;
    T_SCREEN_ID usNextScreen = 0;
    UINT8   ucMode;
    UINT8   ucSubMode;
    UINT8   ucPrintMode = fnOITGetPrintMode();

    
//    sAbacusStatus = fnAGDGetAbacusStatus(&usDetailAbacusStatus);

/*
    if ( (sAbacusStatus == ABACUS_IN_USE) || 
        (fnGetAccountingType() == (UINT8) ACT_SYS_TYPE_ACCUTRAC ||
         fnGetAccountingType() == (UINT8) ACT_SYS_TYPE_METERNET ))
    {
        // If a hosted abacus update is pending, perform it now.
        if( IsHostUpdateRequestPending() > 0 )
        {
            // The prefunction for this screen sends the message to the abacusHostTask
            //  to actually perform the updates (to send Upd???Req to host)
            // When it is done, it will return to the main screen, and go through all the
            //  ready checking again.
            *pNextScr = GetScreen( SCREEN_HOSTED_ABACUS_UPLOAD_PROGRESS ); 
        }
        else
        {
            *pNextScr = GetScreen( SCREEN_MM_INDICIA_ERROR_ABACUS );
        }
        return( (BOOL)PREPOST_COMPLETE );
    }
*/
    
    if(NoEMDPresent == TRUE)
    {
    	SendBaseEventToTablet(BASE_EVENT_EMD_NOT_LOADED, NULL);
    }

    if(NoGARFilePresent == TRUE)
    {
    	SendBaseEventToTablet(BASE_EVENT_GAR_NOT_LOADED, NULL);
    }

//    fnGetPsdState(&lwPsdState);
//    if ( lwPsdState == (UINT32)eIPSDSTATE_INCOMMUNICADO ){
//    	SendBaseEventToTablet(BASE_EVENT_NO_PSD, NULL);
//    }

    //Using ucWhyDoingOOB to check OOB Testprint printed or not
    if(fnCMOSSetupGetCMOSDiagnostics()->ucWhyDoingOOB == TRUE)
    {

		usNextScreen = fnCheckPSDState();

		usNextScreen = fnCheckInspection();
		usNextScreen = fnPostCheckInspection();
    }
    // Prepare the menu table
    fnHomeScreenMenuInit(TRUE);

    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPermitError
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Permit Ready with Error
//      screen.
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  06/01/2007  Dan Zhang       Initial version 
//
// *************************************************************************/
SINT8 fnpPermitError( void        (** pContinuationFcn)(), 
                            UINT32      *   pMsgsAwaited,
                            T_SCREEN_ID *   pNextScr )
{
    
    fnUpdateLEDPageIndicator();

    return (BOOL)PREPOST_COMPLETE;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostIndiciaError
//
// DESCRIPTION: 
//      Post function that is called when leaving the Home Ready with Error
//      screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  11/15/2005  Vincent Yi      Initial version 
//
// *************************************************************************/
SINT8 fnpPostIndiciaError (void     (** pContinuationFcn)(), 
                           UINT32      *   pMsgsAwaited)
{
    SET_MENU_LED_OFF();

    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpMMFeesDetails
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Fees Details screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  09/26/2007 Adam Liu    Add the soft key for "Barcode" view
//  03/10/2006 Sunny Liao    Modify this function
//  1/05/2006   Vincent Yi      Initial version 
//
// *************************************************************************/
SINT8 fnpMMFeesDetails( void        (** pContinuationFcn)(), 
                        UINT32      *   pMsgsAwaited,
                        T_SCREEN_ID *   pNextScr )
{
    SINT32   lSelectedFeeIndex = 0;
    SINT32   lSlotIndex = 0;    
    SINT32   lCurrentFeeValue;
    SINT32   lFeeCharge = 0;
    SINT32   lFeeValue = 0;

    UINT16   *pusCurrentFeeName = NULL_PTR;
    UINT16   usTempFeeSymbol[ LENNAME ] = {0,0};
    UINT16   usNewLastIndex;

    UINT8    ucSelectedFees = 0;    
    UINT8    ucFeeChargeDecPos = 0;
    UINT8    ucFeeValueDecPos = 0;
    
   
    S_MENU_TABLE     *  pMenuTable = fnGetSMenuTable();
    S_RATEMENU_TABLE *  pRateMenuTable = fnGetSRateMenuTable();

      //get the selected fees num
    (void)fnRateGetNumSelectedFees(&ucSelectedFees);
      
    if( !bFeeNameSpaceAllocated &&  !bFeeNameSpaceAllocated)
    {
    fnClearSMenuTable();
    fnClearSRateMenuTable();
    }

         
    // Loop through the selected fee list first.
    if ( ucSelectedFees > 0 )
    {
        // allocate memory for FEE names. the memory would be freed in the post screen function of this screen.
        if( (OSGetMemory( (void**) &pwTempFeeName, ucSelectedFees * LENNAME * sizeof(UINT16)) ) == (BOOL)SUCCESSFUL )
        {
            // init the memory to 0
            memset(pwTempFeeName, 0, ucSelectedFees * LENNAME * sizeof(UINT16));

            bFeeNameSpaceAllocated = true;

            for (lSelectedFeeIndex = 0; lSelectedFeeIndex < ucSelectedFees; lSelectedFeeIndex++, lSlotIndex++ )
            {
                // Allocate slots for selected fees
                pusCurrentFeeName =  pwTempFeeName + LENNAME * lSelectedFeeIndex; //  get the address of the fee name
                // Get the fee name.
                (void)fnRateGetAnIdxSelFeeName((UINT8)lSelectedFeeIndex+1,
                                                pusCurrentFeeName,
                                                usTempFeeSymbol);
                        
                pMenuTable->pSlot[lSlotIndex].bKeyAssignment = lSlotIndex + 1;
                pMenuTable->pSlot[lSlotIndex].wDisplayNumber = 0;
                pMenuTable->pSlot[lSlotIndex].bLabelString = 0;
                pMenuTable->pSlot[lSlotIndex].fActive = FALSE;   //display only

                PBUnicodeToUnicode(pusCurrentFeeName);
                
                pMenuTable->pSlot[lSlotIndex].pUnicode = pusCurrentFeeName;

                //get the fee charge
                (void)fnRateGetAnIdxSelFeeCharge((UINT8)lSelectedFeeIndex+1, &lFeeCharge);
                pRateMenuTable->pSlot[lSlotIndex].lwSFCharge = lFeeCharge;
            
                //get the fee charge decimal position
                (void)fnRateGetAnIdxSelFeeChargeDecPos((UINT8)lSelectedFeeIndex+1, &ucFeeChargeDecPos);
                pRateMenuTable->pSlot[lSlotIndex].bSFDecimalPos = ucFeeChargeDecPos;
                
                //get the fee value
                (void)fnRateGetAnIdxSelFeeValue((UINT8)lSelectedFeeIndex+1, &lFeeValue);                
                lCurrentFeeValue = lFeeValue;
                // if this fee has value, allocate a new slot and display value there.
                if ( lCurrentFeeValue > 0) 
                {
                       // allocate a new menu slot
                    lSlotIndex++; 
                    pMenuTable->pSlot[lSlotIndex].bKeyAssignment = lSlotIndex + 1;
                    pMenuTable->pSlot[lSlotIndex].wDisplayNumber = 0;
                    pMenuTable->pSlot[lSlotIndex].bLabelString = 1;  
                    pMenuTable->pSlot[lSlotIndex].fActive = FALSE;   //display only
                
                    // here we reuse the pSRateMenuTable display function
                    pRateMenuTable->pSlot[lSlotIndex].lwSFCharge = lCurrentFeeValue;
                    //get the fee value decimal position
                    (void)fnRateGetAnIdxSelFeeValueDecPos((UINT8)lSelectedFeeIndex+1, &ucFeeValueDecPos);
                    pRateMenuTable->pSlot[lSlotIndex].bSFDecimalPos = ucFeeValueDecPos;
                 }
            }//end for (lSelectedFeeIndex...    

       }//end if( (OSGetMemory(...
    }
   

    pMenuTable->bEntriesPerPage = 4;
    // Set the total number of entries which will be use for displaying 
    // scroll bar
    fnSetMenuEntriesTotalNumber (lSlotIndex);

    fnUpdateLEDPageIndicator();
    *pNextScr = 0;
    
    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostMMFeesDetails
//
// DESCRIPTION: 
//      Post function that is called when leaving the Fees Details screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  03/10/2006  Sunny Liao      Modified for Future Phoenix
//  1/05/2006   Vincent Yi      Initial version 
//
// *************************************************************************/
SINT8 fnpPostMMFeesDetails (void        (** pContinuationFcn)(), 
                           UINT32       *   pMsgsAwaited)
{
    SET_MENU_LED_OFF();

    if ( pwTempFeeName != NULL )
    {
        OSReleaseMemory( pwTempFeeName);
        pwTempFeeName = NULL;
        bFeeNameSpaceAllocated  = false;
    }

    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpIndiciaPrinting
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Indicia Printing 
//      screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0, 
//
// RETURNS:
//      always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/15/2007  Oscar Wang      Get barcode/tracking log number here for Germany
//                              Non-stop mode since during printing, there is no
//                              enough time to get these value from archive file.
//  11/17/2005  Adam Liu        Add reset animator counter 
//  11/14/2005  Vincent Yi      Initial version 
//
// *************************************************************************/
SINT8 fnpIndiciaPrinting( void      (**     pContinuationFcn)(), 
                          UINT32        *   pMsgsAwaited,
                          T_SCREEN_ID   *   pNextScr )
{
    *pNextScr = 0;

    fnResetAnimatorCounter();
    
    // Prepare the menu table
    fnHomeScreenMenuInit(FALSE);
	
    // Check all warning conditions for displaying the warning messages and
    // counter at the first time
//  fnUpdateWarningMessages (TRUE);
    fnOITInRunningScreen (TRUE);
    fnSetMultiPagePrintingStopped (FALSE);

    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostIndiciaPrinting
//
// DESCRIPTION: 
//      Post function that is called when leaving Inidicia Printing screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  2/02/2006   Vincent Yi      Initial version 
//
// *************************************************************************/
SINT8 fnpPostIndiciaPrinting (void      (** pContinuationFcn)(), 
                              UINT32        *   pMsgsAwaited)
{
    fnOITInRunningScreen (FALSE);

    return (PREPOST_COMPLETE);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnpResetPrintingAnimator
//
// DESCRIPTION: 
//      Pre function that reset the animator for printing/running screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0, 
//
// RETURNS:
//      always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  11/17/2006  Adam Liu      Initial version 
//
// *************************************************************************/
SINT8 fnpResetPrintingAnimator( void      (**     pContinuationFcn)(), 
                                UINT32        *   pMsgsAwaited,
                                T_SCREEN_ID   *   pNextScr )
{
    *pNextScr = 0;

    fnResetAnimatorCounter();

    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpIndiciaTransient
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Indicia Transient 
//      screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0, 
//
// RETURNS:
//      always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/25/2006  Dicky Sun       Update for GTS PH receipt.
//  11/14/2005  Vincent Yi      Initial version 
//
// *************************************************************************/
SINT8 fnpIndiciaTransient( void         (**     pContinuationFcn)(), 
                          UINT32        *   pMsgsAwaited,
                          T_SCREEN_ID   *   pNextScr )
{
    *pNextScr = 0;

//    if(fnGetCurrentReportType() == DELCON_RPT)
//    {
//       fnSetFinishGTSReceiptFlag(TRUE);
//    }

    // Prepare the menu table
    fnHomeScreenMenuInit(FALSE);

    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpIndiciaTapePrinting
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Tape Printing 
//      screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0, 
//
// RETURNS:
//      always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/15/2007  Oscar Wang      Get barcode/tracking log number here for Germany
//                              Non-stop mode since during printing, there is no
//                              enough time to get these value from archive file.
//  11/30/2005  Vincent Yi      Initial version 
//
// *************************************************************************/
SINT8 fnpIndiciaTapePrinting( void      (**     pContinuationFcn)(), 
                              UINT32        *   pMsgsAwaited,
                              T_SCREEN_ID   *   pNextScr )
{
    *pNextScr = 0;

    // Prepare the menu table
    fnHomeScreenMenuInit(FALSE);

    fnOITInRunningScreen (TRUE);

    fnTapePrintingInit();

    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostIndiciaTapePrinting
//
// DESCRIPTION: 
//      Post function that is called when leaving Tape Printing screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  2/02/2006   Vincent Yi      Initial version 
//
// *************************************************************************/
SINT8 fnpPostIndiciaTapePrinting (void      (** pContinuationFcn)(), 
                                  UINT32        *   pMsgsAwaited)
{
    fnOITInRunningScreen (FALSE);

    return (PREPOST_COMPLETE);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnpSealOnlyCheck
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the the Seal Only Ready
//      screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr, depends on the conditions checking
//      If no any exception, 
//              *pContinuationFcn = fncSealOnlyCheck
//              *pMsgsAwaited = 0 | CM_STATUS_AWAITED_MASK
//      Else, no change on the variables above.
//
// RETURNS:
//      PREPOST_COMPLETE, if screen transition to other screens due to some 
//                          reasons after checking
//      PREPOST_WAITING, if no any exception
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/06/2005  Vincent Yi      Initial version 
//  05/19/2006  James Wang      To request control manager status with a new BR function.
//
// *************************************************************************/
SINT8 fnpSealOnlyCheck( void        (** pContinuationFcn)(), 
                        UINT32      *   pMsgsAwaited,
                        T_SCREEN_ID *   pNextScr )
{
    char chRetval = PREPOST_COMPLETE;

//  unsigned char   bSysInspecStatus;
//  unsigned char   bPrintedInspecStatus;

    //empty the stack for saving the screen IDs after error processing
    fnErrClearReturnScreens();

    // Initializing
    *pNextScr = 0;
    fnClearDisablingWarningConditions();

    // Warning or disabling conditions checking
    *pNextScr = fnSealOnlyConditionsCheck();
    if (*pNextScr == 0)
    {   // No screen transition

        /* request control manager status */
        *pContinuationFcn = (void (*)()) fncSealOnlyCheck;
        *pMsgsAwaited = 0 | CM_STATUS_AWAITED_MASK;
        fnRequestCMStatus();

        /* clear this flag after we send the status request message */
        fRecheckPrinterStatus = FALSE;

        /* waiting for cm status response. */
        chRetval = PREPOST_WAITING;
    }

    return (chRetval);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpOptionalPrintModeCheck
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Ad Only / Date-Time Only
//      screens.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr, depends on the conditions checking
//      If no any exception, 
//              *pContinuationFcn = fncOptionalPrintModeCheck
//              *pMsgsAwaited = 0 | CM_STATUS_AWAITED_MASK
//      Else, no change on the variables above.
//
// RETURNS:
//      PREPOST_COMPLETE, if screen transition to other screens due to some 
//                          reasons after checking
//      PREPOST_WAITING, if no any exception
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/23/2005  Adam Liu        Initial version 
//  05/19/2006  James Wang      To request control manager status with a new BR function.
//
// *************************************************************************/
SINT8 fnpOptionalPrintModeCheck( void       (** pContinuationFcn)(), 
                                 UINT32      *  pMsgsAwaited,
                                 T_SCREEN_ID *  pNextScr )
{
    SINT8 ucRetval = PREPOST_COMPLETE;

    //empty the stack for saving the screen IDs after error processing
    fnErrClearReturnScreens();

    *pNextScr = 0;
    fnClearDisablingWarningConditions();

    // Clear main menu check event so CM won't initiate debiting to avoid 2003 error
    OSSetEvents(OIT_EVENT_GROUP, ~(unsigned long) MAIN_MENU_CHK_END_EVT, OS_AND);

    // Warning or disabling conditions checking
    *pNextScr = fnOptionalPrintModeConditionsCheck();
    if (*pNextScr == 0)
    {   // No screen transition

        /* request control manager status */
        *pContinuationFcn = (void (*)()) fncOptionalPrintModeCheck;
        *pMsgsAwaited = 0 | CM_STATUS_AWAITED_MASK;
        fnRequestCMStatus();

        /* clear this flag after we send the status request message */
        fRecheckPrinterStatus = FALSE;

        /* waiting for cm status response. */
        ucRetval = PREPOST_WAITING;
    }

    // Set event here too in case it jumped to here to change screens
    OSSetEvents(OIT_EVENT_GROUP, (unsigned long) MAIN_MENU_CHK_END_EVT, OS_OR);

    return (ucRetval);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpOptionalPrintModeTapePrinting
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Tape Printing 
//      screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0, 
//
// RETURNS:
//      always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  2/15/2006   Adam Liu      Initial version 
//
// *************************************************************************/
SINT8 fnpOptionalPrintModeTapePrinting( void      (**     pContinuationFcn)(), 
                                        UINT32        *   pMsgsAwaited,
                                        T_SCREEN_ID   *   pNextScr )
{
    *pNextScr = 0;

    fnOITInRunningScreen (TRUE);

    fnTapePrintingInit();

    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpOptionalPrintModeError
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the optional print 
//      Error or disabled screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0, 
//
// RETURNS:
//      always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  06/08/2006   Adam Liu      Initial version 
//
// *************************************************************************/
SINT8 fnpOptionalPrintModeError( void      (**     pContinuationFcn)(), 
                                        UINT32        *   pMsgsAwaited,
                                        T_SCREEN_ID   *   pNextScr )
{
    *pNextScr = 0;

    // Prepare the menu table
    fnOptionalPrintModeMenuInit(TRUE);
    // switch to correct mode based on text, ad, text selections
    fnUpdateOptionalPrintMode(); 


    return (BOOL)PREPOST_COMPLETE;
}
/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostOptionalPrintModeTapePrinting
//
// DESCRIPTION: 
//      Post function that is called when leaving Tape Printing screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  2/15/2006   Adam Liu      Initial version 
//
// *************************************************************************/
SINT8 fnpPostOptionalPrintModeTapePrinting (void      (** pContinuationFcn)(), 
                                            UINT32        *   pMsgsAwaited)
{
    fnOITInRunningScreen (FALSE);

    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPermitModeCheck
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Permit Ready
//      screens.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr, depends on the conditions checking
//      If no any exception, 
//              *pContinuationFcn = fncPermitModeCheck
//              *pMsgsAwaited = 0 | CM_STATUS_AWAITED_MASK
//      Else, no change on the variables above.
//
// RETURNS:
//      PREPOST_COMPLETE, if screen transition to other screens due to some 
//                          reasons after checking
//      PREPOST_WAITING, if no any exception
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  03/13/2005  Adam Liu        Initial version 
//
// *************************************************************************/
SINT8 fnpPermitModeCheck( void       (** pContinuationFcn)(), 
                                 UINT32      *  pMsgsAwaited,
                                 T_SCREEN_ID *  pNextScr )
{
    SINT8 ucRetval = PREPOST_COMPLETE;

    //empty the stack for saving the screen IDs after error processing
    fnErrClearReturnScreens();
    
    *pNextScr = 0;
    fnClearDisablingWarningConditions();

    // Clear main menu check event so CM won't initiate debiting to avoid 2003 error
    OSSetEvents(OIT_EVENT_GROUP, ~(unsigned long) MAIN_MENU_CHK_END_EVT, OS_AND);

    // Warning or disabling conditions checking, and setting lwWarningConditions
    // lwDisablingConditions, lwDisablingConditions2
    *pNextScr = fnPermitModeConditionsCheck();
    if (*pNextScr == 0)
    {   // No screen transition

        /* request control manager status */
        *pContinuationFcn = (void (*)()) fncPermitModeCheck;
        *pMsgsAwaited = 0 | CM_STATUS_AWAITED_MASK;
        fnRequestCMStatus();

        /* clear this flag after we send the status request message */
        fRecheckPrinterStatus = FALSE;

        /* waiting for cm status response. */
        ucRetval = PREPOST_WAITING;
    }

    // Set event here too in case it jumped to here to change screens
    OSSetEvents(OIT_EVENT_GROUP, (unsigned long) MAIN_MENU_CHK_END_EVT, OS_OR);

    return (ucRetval);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostPermitModeCheck
//
// DESCRIPTION: 
//      Post function that is called when leaving the Permit Ready screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  03/13/2006  Adam Liu     Initial version 
//
// *************************************************************************/
SINT8 fnpPostPermitModeCheck (void         (** pContinuationFcn)(), 
                           UINT32       *   pMsgsAwaited)
{
    SET_MENU_LED_OFF();

    return (BOOL)PREPOST_COMPLETE;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnpPermitModeTapePrinting
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Tape Printing 
//      screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0, 
//
// RETURNS:
//      always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  04/13/2006  Adam Liu      Initial version 
//
// *************************************************************************/
SINT8 fnpPermitModeTapePrinting(  void      (**     pContinuationFcn)(), 
                                  UINT32        *   pMsgsAwaited,
                                  T_SCREEN_ID   *   pNextScr )
{
    *pNextScr = 0;

    // Prepare the menu table
    fnPermitModeMenuInit(FALSE);

    fnOITInRunningScreen (TRUE);

    fnTapePrintingInit();

    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostPermitModeTapePrinting
//
// DESCRIPTION: 
//      Post function that is called when leaving Tape Printing screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  04/13/2006  Adam Liu      Initial version 
//
// *************************************************************************/
SINT8 fnpPostPermitModeTapePrinting (void      (** pContinuationFcn)(), 
                                     UINT32        *   pMsgsAwaited)
{
    fnOITInRunningScreen (FALSE);

    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpReportsPrintCheck
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Report Print
//      ready screens.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr, depends on the conditions checking
//      If no any exception, 
//              *pContinuationFcn = fncReportsPrintCheck
//              *pMsgsAwaited = 0 | CM_STATUS_AWAITED_MASK
//      Else, no change on the variables above.
//
// RETURNS:
//      PREPOST_COMPLETE, if screen transition to other screens due to some 
//                          reasons after checking
//      PREPOST_WAITING, if no any exception
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  03/08/2006  Vincent Yi      Initial version
//  05/19/2006  James Wang      To request control manager status with a new BR function.
//
// *************************************************************************/
SINT8 fnpReportsPrintCheck( void        (** pContinuationFcn)(), 
                            UINT32      *   pMsgsAwaited,
                            T_SCREEN_ID *   pNextScr )
{
    SINT8 ucRetval = PREPOST_COMPLETE;

    //empty the stack for saving the screen IDs after error processing
    fnErrClearReturnScreens();

    *pNextScr = 0;
    fnClearDisablingWarningConditions();
    // Initializing the error related parameters
    usDisplayedErrorIndex = MAX_ERROR_NUM-1;
    usTotalErrors = 0;
    // Initializing the warning related parameters
    usDisplayedWarningIndex = MAX_WARNING_NUM-1;
    usTotalWarnings = 0;

    // Since CM is still waiting for the MAIN_MENU_CHK_END_EVT event,
    // this is just defensive code as no debit for report
    OSSetEvents(OIT_EVENT_GROUP, ~(UINT32) MAIN_MENU_CHK_END_EVT, OS_AND);

    // Warning or disabling conditions checking
    *pNextScr = fnReportsPrintConditionsCheck();
    if (*pNextScr == 0)
    {   // No screen transition
        
        // Clear main menu check event so CM can run mail piece
        OSSetEvents(OIT_EVENT_GROUP, (unsigned long) MAIN_MENU_CHK_END_EVT, OS_OR);

        /* request control manager status */
        *pContinuationFcn = (void (*)()) fncReportsPrintCheck;
        *pMsgsAwaited = 0 | CM_STATUS_AWAITED_MASK;
        fnRequestCMStatus();

        /* clear this flag after we send the status request message */
        fRecheckPrinterStatus = FALSE;

        /* waiting for cm status response. */
        ucRetval = PREPOST_WAITING;
    }
    else
    {
        // When online print, before entering any error screen, disconnect
        // from att
//      if (fnIsOnlineReportPrinting() == TRUE)
//      {
//          fnpInfraGeneralErr (pContinuationFcn, pMsgsAwaited, pNextScr);
//      }
    }

    // Set event here too in case it jumped to here to change screens
    OSSetEvents(OIT_EVENT_GROUP, (unsigned long) MAIN_MENU_CHK_END_EVT, OS_OR);

    return (ucRetval);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostReportsPrint
//
// DESCRIPTION: 
//      Post function that is called when leaving the Reports Ready/Error screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/07/2007  Vincent Yi     Initial version 
//
// *************************************************************************/
SINT8 fnpPostReportsPrint (void         (** pContinuationFcn)(), 
                           UINT32       *   pMsgsAwaited)
{
    SINT8   cRetval = PREPOST_COMPLETE;
    if (fnIsOnlineReportPrinting() == TRUE)
    {   // Stop the timer when online print receipt
        cRetval = fnpPostInfraStopTimeoutClock (pContinuationFcn, pMsgsAwaited);
    }

    return cRetval;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnpReportPHTapePrinting
//
// DESCRIPTION: 
//      Pre function that is called prior to going to Report Tape Printing 
//      screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0, 
//
// RETURNS:
//      always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/06/2006  Vincent Yi      Initial version 
//
// *************************************************************************/
SINT8 fnpReportPHTapePrinting( void      (**     pContinuationFcn)(), 
                               UINT32        *   pMsgsAwaited,
                               T_SCREEN_ID   *   pNextScr )
{
    *pNextScr = 0;

    fnOITInRunningScreen (TRUE);

    fnTapePrintingInit();

    return (BOOL)PREPOST_COMPLETE;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnpIndiciaAbacusCheck
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Abacus Ready 
//      screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0, 
//
// RETURNS:
//      always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/20/2009  Jingwei,Li    Set MAIN_MENU_CHK_END_EVT to avoid 2003 error.
//  05/10/2007  Oscar Wang    Update warning message here.
//  02/06/2007  Adam Liu        Initial version 
//
// *************************************************************************/
SINT8 fnpIndiciaAbacusCheck( void      (**     pContinuationFcn)(), 
                          UINT32        *   pMsgsAwaited,
                          T_SCREEN_ID   *   pNextScr )
{
    *pNextScr = 0;
    
    // Clear main menu check event so CM won't initiate debiting to avoid 2003 error
    OSSetEvents(OIT_EVENT_GROUP, ~(UINT32) MAIN_MENU_CHK_END_EVT, OS_AND);
    
    // Prepare the menu table
    fnAbacusHomeScreenMenuInit(TRUE);
    // Check all warning conditions for displaying the warning messages and
    // counter at the first time
    fnUpdateWarningMessages(TRUE);
    
    // Set event here too in case it jumped to here to change screens
    OSSetEvents(OIT_EVENT_GROUP, (unsigned long) MAIN_MENU_CHK_END_EVT, OS_OR);

    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpIndiciaErrorAbacus
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Abacus Error 
//      screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0, 
//
// RETURNS:
//      always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/26/2007  Adam Liu        Initial version 
//
// *************************************************************************/
SINT8 fnpIndiciaErrorAbacus( void      (**     pContinuationFcn)(), 
                          UINT32        *   pMsgsAwaited,
                          T_SCREEN_ID   *   pNextScr )
{
    *pNextScr = 0;
    
    // Prepare the menu table
    fnAbacusHomeScreenMenuInit(TRUE);

    return (BOOL)PREPOST_COMPLETE;
}    


/* *************************************************************************
// FUNCTION NAME: 
//      fnpIndiciaPrintingAbacus
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Abacus Printing 
//      screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0, 
//
// RETURNS:
//      always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  11/07/2007  Adam Liu        Add reset animator counter and GTS handling 
//                              by refering to fnpIndiciaPrinting().
//  02/26/2007  Adam Liu        Initial version 
//
// *************************************************************************/
SINT8 fnpIndiciaPrintingAbacus( void      (**     pContinuationFcn)(), 
                          UINT32        *   pMsgsAwaited,
                          T_SCREEN_ID   *   pNextScr )
{
    *pNextScr = 0;

    fnResetAnimatorCounter();
    
    // Prepare the menu table
    fnAbacusPrintingScreenMenuInit(FALSE);
    
    fnOITInRunningScreen (TRUE);
    fnSetMultiPagePrintingStopped (FALSE);

    return (BOOL)PREPOST_COMPLETE;
}    

/* *************************************************************************
// FUNCTION NAME: 
//      fnpIndiciaTapePrintingAbacus
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the Abacus Tape Printing 
//      screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0, 
//
// RETURNS:
//      always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  11/07/2007  Adam Liu        Add GTS handling by refering to fnpIndiciaTapePrinting().
//  11/06/2007  Vincent Yi      Added tape printing init functions
//  02/26/2007  Adam Liu        Initial version 
//
// *************************************************************************/
SINT8 fnpIndiciaTapePrintingAbacus( void      (**     pContinuationFcn)(), 
                          UINT32        *   pMsgsAwaited,
                          T_SCREEN_ID   *   pNextScr )
{
    *pNextScr = 0;
    
    // Prepare the menu table
    fnAbacusPrintingScreenMenuInit(FALSE);

    fnOITInRunningScreen (TRUE);

    fnTapePrintingInit();

    return (BOOL)PREPOST_COMPLETE;
}    

/* *************************************************************************
// FUNCTION NAME: 
//      fnpDiffWeighPieceCheck
//
// DESCRIPTION: 
//      Pre function that is called prior to going to the differential
//      weighing "remove one piece" screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr, depends on the conditions checking
//
// RETURNS:
//      PREPOST_COMPLETE 
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/03/2007  Oscar Wang      Renamed ESERVICE_UPLOAD_RECORD_PROMPT to 
//                              ESERVICE_HOME_SCRN_UPLOAD_PROMPT. 
//  01/19/2007  Oscar Wang      Fix FRACA GMSE00113167: check unsent EService
//                              record first.
//  01/11/2007  Oscar Wang      Fix FRACA GMSE00112739: check Meter lock 
//                              prior to entering "Remove mail on scale"
//              Tim Zhang       Initial version
//
// *************************************************************************/
SINT8 fnpDiffWeighPieceCheck( void        (** pContinuationFcn)(), 
                            UINT32      *   pMsgsAwaited,
                            T_SCREEN_ID *   pNextScr )
{
    SINT8   ucRetval = PREPOST_COMPLETE;
    UINT8   pBCDWeight[LENWEIGHT];
    UINT8   bPlatformStatus;


    *pNextScr = 0;

    // PSD battery low warning
    /*if (fPSDBatteryLowPending == TRUE)
    {
        *pNextScr = GetScreen(PSD_BATTERY_LOW);
        goto xit;
    }*/

    /* meter locked screen should come up firstly if meter lock is enabled */
    if (fMeterLocked == TRUE) 
    {
        *pNextScr = GetScreen(SCREEN_METER_LOCKED);
        fDiffWeighPieceNeeded = TRUE;
        goto xit;
    }
    
    /* Check if any Confirmation Service Records are pending */
    if ( fnGetRecordsPendingFlag() )   
    {
        *pNextScr = GetScreen(ESERVICE_HOME_SCRN_UPLOAD_PROMPT);
        goto xit;
    }

    // check account status
/*
    if (fnGetAccountDisabledStatus() != ACT_SUCCESS)
    {
        // *pNextScr = GetScreen( DIFF_NEEDS_ACCT);
        goto xit;
    }
*/
  
    //  bDiffWeigh1stClassSelection = false;
    
xit:    

    return (ucRetval);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpDataCenterOptionsMenu
//
// DESCRIPTION: 
//      Pre function that sets up the menu items for Data Center Options screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
//      
// MODIFICATION HISTORY:
//  07/24/2008  Raymond Shen    Modified for "Data upload" for Budget data upload.
//  13 Dec 2006 Adam Liu        Initial version 
//  29 Nov 2007 Martin O'Brien  Added comment regarding the compiler's warning
// *************************************************************************/
SINT8 fnpDataCenterOptionsMenu( void        (** pContinuationFcn)(), 
                      UINT32        *   pMsgsAwaited,
                      T_SCREEN_ID   *   pNextScr )
{
    UINT32      ulSlotIndex = 0;
    UINT32      ulNextSlotIndex;

	// can't be made "const" because the value of the "Active Flag" needs to be changed.
    static S_MENU_SETUP_ENTRY pDCOptionsMenuInitTable[] = 
    {
        // force menu table reset before defining new slots
        {   CLEAR_MENU_TABLE,   (UINT8) 0,  (UINT8) 0,  FALSE,  NULL_PTR },

        // Display number           Key           Label        Active   Conditional 
        // offset,                  assignment,   string id,   flag,    function
                                    
        /* Check for updates */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 1,    (UINT8) 1,   TRUE,    NULL_PTR },
        /* Data upload */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 2,    (UINT8) 2,   TRUE,    fnIsDataCenterUploadEnabled },
        /* Phone Settings */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 3,   TRUE,    NULL_PTR },
        /* Distributor settings */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 4,    (UINT8) 4,   TRUE,    NULL_PTR },
        /* Network settings */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 5,    (UINT8) 5,   TRUE,    NULL_PTR },
        /* Firewall settings */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 6,    (UINT8) 6,   TRUE,    NULL_PTR},
        /* Connection Mode:Auto*/
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 7,    (UINT8) 7,   TRUE,    fnConnectionModeAuto},
        /* Connection Mode:Phone*/
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 7,    (UINT8) 8,   TRUE,    fnConnectionModePhone},
        {   END_MENU_TABLE_MARKER,  (UINT8) 0,    (UINT8) 0,   FALSE,   NULL_PTR }  
    } ;

    // invoke generic slot initialization function with this menu initializer table
    ulNextSlotIndex = fnSetupMenuSlots( pDCOptionsMenuInitTable, ulSlotIndex, 
                                        (UINT8) 4 ) ;
    
    // Set the total number of entries which will be use for displaying 
    // scroll bar
    fnSetMenuEntriesTotalNumber ((UINT16) (ulNextSlotIndex - ulSlotIndex));

    // Update LED indicator
    fnUpdateLEDPageIndicator();
    *pNextScr = 0;      /* leave next screen unchanged */

    return (PREPOST_COMPLETE);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostDataCenterOptionsMenu
//
// DESCRIPTION: 
//      Post function that is called when leaving DataCenterOption screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  6/28/2007   Joey Cui      Initial version 
//
// *************************************************************************/
SINT8 fnpPostDataCenterOptionsMenu (void      (** pContinuationFcn)(), 
                                    UINT32        *   pMsgsAwaited)
{
    SET_MENU_LED_OFF();

    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrePostageCorrectionPrinting
//
// DESCRIPTION: 
//      Pre function that reset the animator for printing/running screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0, 
//
// RETURNS:
//      always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  11/17/2006  Adam Liu      Initial version 
//
// *************************************************************************/
SINT8 fnpPrePostageCorrectionPrinting(  void      (**     pContinuationFcn)(), 
                                        UINT32        *   pMsgsAwaited,
                                        T_SCREEN_ID   *   pNextScr )
{
    *pNextScr = 0;

    fnResetAnimatorCounter();
    fnOITInRunningScreen (TRUE);

    return (BOOL)PREPOST_COMPLETE;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostPostageCorrectionPrinting
//
// DESCRIPTION: 
//      Post function that is called when leaving Inidicia Printing screen
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  2/02/2006   Vincent Yi      Initial version 
//
// *************************************************************************/
SINT8 fnpPostPostageCorrectionPrinting (void      (** pContinuationFcn)(), 
                                        UINT32        *   pMsgsAwaited)
{
    fnOITInRunningScreen (FALSE);

    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrePrintingInstructions
//
// DESCRIPTION: 
//      Pre function that sets up the items of printing instructions. 
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
/       
// MODIFICATION HISTORY:
//  08/17/2007  Joey Cui    Modify total line to 8 to fix 127680
//  04/16/2007  Andy Mo     Initial version 
// *************************************************************************/
SINT8 fnpPrePrintingInstructions( void      (** pContinuationFcn)(), 
                           UINT32       *   pMsgsAwaited,
                           T_SCREEN_ID  *   pNextScr )
{
    #define TOTAL_INSTRUCTION_LINES 8      //the number of  instructions lines
    UINT16      usSlotIndex = 0;
    UINT32      ulNextSlotIndex;

    /*
	// can't be made "const" because the value of the "Active Flag" needs to be changed.
    static S_MENU_SETUP_ENTRY   pPrintingInstructionsTable[] = 
    {
        // force menu table reset before defining new slots
        {   CLEAR_MENU_TABLE,   (UINT8) 0,  (UINT8) 0,  FALSE,  NULL_PTR },

        // Display number           Key           Label        Active   Conditional 
        // offset,                  assignment,   string id,   flag,    function
                                    
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 1,    (UINT8) 1,   OIC_SLOT_DISPLAY_ONLY,    NULL_PTR },
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 2,    (UINT8) 2,   OIC_SLOT_DISPLAY_ONLY,    NULL_PTR },
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 3,   OIC_SLOT_DISPLAY_ONLY,    NULL_PTR },
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 4,    (UINT8) 4,   OIC_SLOT_DISPLAY_ONLY,    NULL_PTR },
        {   END_MENU_TABLE_MARKER,  (UINT8) 0,    (UINT8) 0,   FALSE,   NULL_PTR }  
    } ;  */

//    CLEAR_SMENU_TABLE(pSMenuTable);
//    pSMenuTable.bEntriesPerPage = 4;
    for(usSlotIndex=0;usSlotIndex<TOTAL_INSTRUCTION_LINES;usSlotIndex++)
    {
//        pSMenuTable.pSlot[usSlotIndex].bKeyAssignment = usSlotIndex+1;
    }
    // Set the total number of entries which will be use for displaying 
    // scroll bar
    fnSetMenuEntriesTotalNumber (TOTAL_INSTRUCTION_LINES);

    // Update LED indicator
    fnUpdateLEDPageIndicator();
    *pNextScr = 0;      /* leave next screen unchanged */

    #undef TOTAL_INSTRUCTION_LINES
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostPrintingInstrucition
//
// DESCRIPTION: 
//      Post function that turns off time menu led indicator. 
//
// INPUTS:
//      None
//
// OUTPUTS:
//      Standard post function outputs.
//
// RETURNS:
//      PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 04/24/2007 Andy Mo           Initial version
//
// *************************************************************************/
SINT8 fnpPostPrintingInstrucition( void (** pContinuationFcn)(), 
                                  UINT32 *pMsgsAwaited )
{
    SET_MENU_LED_OFF();

    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPreClearBatteryWarnings
//
// DESCRIPTION: 
//      Pre function that clear the battery warning pending flag. 
//
// INPUTS:
//      None
//
// OUTPUTS:
//      Standard pre function outputs.
//
// RETURNS:
//      PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/18/2008  Raymond Shen    Initial version
//
// *************************************************************************/
SINT8 fnpClearBatteryWarnings( void         (** pContinuationFcn)(), 
                           UINT32       *   pMsgsAwaited,
                           T_SCREEN_ID  *   pNextScr )
{
    // clear the pending flag after the warning is shown
    ucOITBatteryStatus = BOTH_BATTERIES_OK_STATUS;

    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}

/* Field functions */


/* *************************************************************************
// FUNCTION NAME: 
//      fnfHomeWarningStrings
//
// DESCRIPTION: 
//      Output field function for cycling displaying warning strings.
//
// PRE-CONDITIONS:
//      usDisplayedWarningIndex and usTotalWarnings been updated.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//      Menu possibilities:
//      1. Inspection Due
//      2. Low Funds
//      3. Low Ink
//      4. Ink Tank Expiring
//      5. Waste Tank Near Full
//      6. PSD Battery Low
//
// MODIFICATION HISTORY:
//  03/10/2009  Jingwei,Li      Don't display background color when no warning.
//  11/11/2005  Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnfHomeWarningStrings (UINT16      *pFieldDest, 
                            UINT8       bLen, 
                            UINT8       bFieldCtrl,
                            UINT8       bNumTableItems, 
                            UINT8       *pTableTextIDs,
                            UINT8       *pAttributes)
{
    SET_SPACE_UNICODE(pFieldDest, bLen);

    if (usTotalWarnings > 0)
    {
        if (usDisplayedWarningIndex < bNumTableItems)
        {
            fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                pTableTextIDs, usDisplayedWarningIndex);
        }
    }
    else
    {
        //If no warning, don't show anything.
        *pFieldDest = NULL_VAL;
    }
        
    return (BOOL)SUCCESSFUL;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfHomeWarningCounter
//
// DESCRIPTION: 
//      Output field function for displaying warning message counter, if only 
//      one message or no message in queue, no display
//
// PRE-CONDITIONS:
//      usDisplayedWarningSeqNo and usTotalWarnings have been updated
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  04/14/2009  Jingwei,Li      Modified for one error case.
//  03/10/2009  Jingwei,Li      Don't display background color when no warning.
//  11/11/2005  Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnfHomeWarningCounter (UINT16      *pFieldDest, 
                            UINT8       bLen, 
                            UINT8       bFieldCtrl,
                            UINT8       bNumTableItems, 
                            UINT8       *pTableTextIDs,
                            UINT8       *pAttributes)
{
    char   pcTempString[SIZE_MSG_COUNTER+1] = {0};
    UINT8   bStrLen;

    SET_SPACE_UNICODE(pFieldDest, bLen);

    if (usTotalWarnings > 1)
    {
        if (usTotalWarnings < 10)
        {
            bStrLen = sprintf (pcTempString, "%d/%d ", usDisplayedWarningSeqNo, 
                                                usTotalWarnings );
            /* if field is right-aligned, pad left of string with spaces */
            if (bFieldCtrl & ALIGN_MASK)
            {
                fnAsciiPadSpace(pcTempString, bStrLen, bLen);
            }

            fnAscii2Unicode(pcTempString, pFieldDest, bLen);
            pFieldDest[bLen] = (UNICHAR) NULL;
        }
        else
        {   // if total number bigger than 9, show "*/*"
            fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                pTableTextIDs, 0);
        }
    }   
    else if(usTotalWarnings == 0)
    {
        *pFieldDest = NULL_VAL;
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfHomeErrorStrings
//
// DESCRIPTION: 
//      Output field function for cycling displaying error strings.
//
// PRE-CONDITIONS:
//      usDisplayedErrorIndex and usTotalErrors have been updated
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//      Menu possibilities:
//       1. Paper in Transport
//       2. No Ink Tank
//       3. No Print Head
//       4. Insufficient Funds
//       5. Feeder Cover Open
//       6. Out of Ink
//       7. Ink Tank Fault
//       8. Print Head Fault
//       9. Waste Tank Full
//      10. Out of Service
//      11. Inspection Required
//      12. Printer Fault
//      13. Tank Lid Open
//      14. Jam Lever Open
//
// MODIFICATION HISTORY:
//  04/01/2009  Jingwei,Li      Don't display background color when no error.
//  11/11/2005  Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnfHomeErrorStrings (UINT16    *pFieldDest, 
                          UINT8     bLen, 
                          UINT8     bFieldCtrl,
                          UINT8     bNumTableItems, 
                          UINT8     *pTableTextIDs,
                          UINT8     *pAttributes)
{
    SET_SPACE_UNICODE(pFieldDest, bLen);

    if (usTotalErrors > 0)
    {
        if (usDisplayedErrorIndex < bNumTableItems)
        {
            fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                pTableTextIDs, usDisplayedErrorIndex);
        }
    }
    else
    {
        fnOITSetToNormalVideo(pAttributes);
        *pFieldDest = NULL_VAL;
    }
        
    return (BOOL)SUCCESSFUL;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfHomeErrorCounter
//
// DESCRIPTION: 
//      Output field function for displaying error message counter, if only 
//      one message or no message in queue, no display
//
// PRE-CONDITIONS:
//      usDisplayedErrorSeqNo and usTotalErrors have been updated
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  04/01/2009  Jingwei,Li      Don't display background color when no error.
//  11/11/2005  Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnfHomeErrorCounter (UINT16    *pFieldDest, 
                          UINT8     bLen, 
                          UINT8     bFieldCtrl,
                          UINT8     bNumTableItems, 
                          UINT8     *pTableTextIDs,
                          UINT8     *pAttributes)
{
    char   pcTempString[SIZE_MSG_COUNTER+1] = {0};
    UINT8   bStrLen;

    SET_SPACE_UNICODE(pFieldDest, bLen);

    if (usTotalErrors > 1)
    {
        if (usTotalErrors < 10)
        {
            bStrLen = sprintf (pcTempString, "%d/%d ", usDisplayedErrorSeqNo, 
                                                usTotalErrors);
            /* if field is right-aligned, pad left of string with spaces */
            if (bFieldCtrl & ALIGN_MASK)
            {
                fnAsciiPadSpace(pcTempString, bStrLen, bLen);
            }

            fnAscii2Unicode(pcTempString, pFieldDest, bLen);
            pFieldDest[bLen] = (UNICHAR) NULL;
        }
        else
        {   // if total number bigger than 9, show "*/*"
            fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                pTableTextIDs, 0);
        }
    }   
    else if (usTotalErrors == 0)
    {
        fnOITSetToNormalVideo(pAttributes);
        *pFieldDest = NULL_VAL;
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfPrintDateTimeOnOff
//
// DESCRIPTION: 
//      Output field function to display print DateTime on/off status
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always successful
//
// TEXT TABLE USAGE:  
//      1st string- Print
//      2nd string- Do Not Print
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Adam Liu           Initial version
//
// *************************************************************************/
BOOL fnfPrintDateTimeOnOff  ( UINT16 *pFieldDest, 
                              UINT8   bLen, 
                              UINT8   bFieldCtrl, 
                              UINT8   bNumTableItems, 
                              UINT8  *pTableTextIDs, 
                              UINT8  *pAttributes )
{   
    UINT8  bString = 0;

    SET_SPACE_UNICODE(pFieldDest, bLen);  

    if (fnCheckIsPrintDateTimeOn() == TRUE)
    {      
        bString = 0 ;  // Print
    }
    else
    {      
        bString = 1 ;  // Do Not Print
    }
    fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,bString);        
    
    return ((BOOL) SUCCESSFUL);     
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfShowFooterMsgInAdDateTimeDisabled
//
// DESCRIPTION: 
//      Output field function to display Footer Msg in AdDateTimeDisabled
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always successful
//
// TEXT TABLE USAGE:  
//      1st string- Footer Msg
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Adam Liu           Initial version
//
// *************************************************************************/
BOOL fnfShowFooterMsgInAdDateTimeDisabled   ( UINT16 *pFieldDest, 
                                              UINT8   bLen, 
                                              UINT8   bFieldCtrl, 
                                              UINT8   bNumTableItems, 
                                              UINT8  *pTableTextIDs, 
                                              UINT8  *pAttributes )
{   
    UINT8  bString = 0;

    SET_SPACE_UNICODE(pFieldDest, bLen);  

    fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,bString);        
    
    return ((BOOL) SUCCESSFUL);     
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfShowTextAdDateTimeCaption
//
// DESCRIPTION: 
//      Output field function to display Optional print mode caption
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always successful
//
// TEXT TABLE USAGE:  
//      1st string- Text-Ad-Date-Time
//      2nd string- Ad-Date-Time
//      3rd string- Text-Date-Time
//      4th string- Date-Time
//      5th string- Text-Ad
//      6th string- Ad
//      7th string- Text
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Adam Liu           Initial version
//
// *************************************************************************/
BOOL fnfShowTextAdDateTimeCaption(UINT16 *pFieldDest, 
                                  UINT8   bLen, 
                                  UINT8   bFieldCtrl, 
                                  UINT8   bNumTableItems, 
                                  UINT8  *pTableTextIDs, 
                                  UINT8  *pAttributes )
{   
    UINT8  bString = 0;

    SET_SPACE_UNICODE(pFieldDest, bLen);  

/*    if (fnCheckIsPrintDateTimeOn() == TRUE)
    {     
        if (fnCheckIsAdSelected() == TRUE)
        {
            if (fnCheckIsTextSelected() == TRUE)
            {
                bString = 0 ;  // Text-Ad-Date-Time
            }
            else
            {
                bString = 1 ;  // Ad-Date-Time
            }
        }
        else
        {
            if (fnCheckIsTextSelected() == TRUE)
            {
                bString = 2 ;  // Text-Date-Time
            }
            else
            {
                bString = 3 ;  // Date-Time
            }
        }
    }
    else
    {      
        if (fnCheckIsAdSelected() == TRUE)
        {
            if (fnCheckIsTextSelected() == TRUE)
            {
                bString = 4 ;  // Text-Ad
            }
            else
            {
                bString = 5 ;  // Ad
            }
        }
        else
        {
            if (fnCheckIsTextSelected() == TRUE)
            {
                bString = 6 ;  // Text
            }
        }
    }*/
    fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,bString);        
    
    return ((BOOL) SUCCESSFUL);     
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfPSDState
//
// DESCRIPTION: 
//      Output field function for the psd state on the manufacturing screen.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always successful
//
// TEXT TABLE USAGE:  
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Victor Li           Initial version
//
// *************************************************************************/
BOOL fnfPSDState   ( UINT16 *pFieldDest, 
                                              UINT8   bLen, 
                                              UINT8   bFieldCtrl, 
                                              UINT8   bNumTableItems, 
                                              UINT8  *pTableTextIDs, 
                                              UINT8  *pAttributes )
{
    UINT32 lwPsdState;
    char pTmpBuf[6];
    UINT8 bMyLen;

    fnGetPsdState(&lwPsdState);
    bMyLen = (UINT16)sprintf(pTmpBuf,"%04X", lwPsdState);

    SET_SPACE_UNICODE(pFieldDest, bLen);

    if (bMyLen < bLen)
    {
        fnAscii2Unicode(pTmpBuf, pFieldDest, bLen);
    }
    
    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: fnfAbacusSurchargeValue
//
// DESCRIPTION: 
//      Field function that displays the abacus surcharge value along
//      with any necessary labels
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//
// WARNINGS/NOTES:
//
// TEXT TABLE USAGE:    
//      1st string: displayed if no surcharge selected (but abacus acct'ng enabled)
//      2nd string: displayed if more than one surcharge type active
//      3rd string: appended to per piece surcharge if that's the only type active
//      4th string: appended to per transaction surcharge if that's the only type active
//
// MODIFICATION HISTORY:
//      Joe Mozdzer  Initial version
// *************************************************************************/
BOOL fnfAbacusSurchargeValue (UINT16 *pFieldDest, 
                              UINT8 bLen, 
                              UINT8 bFieldCtrl, 
                              UINT8 bNumTableItems, 
                              UINT8 *pTableTextIDs,
                              UINT8  *pAttributes )
{
/*
    UINT8   bWhichString = 0xFF;
    AGD_SETUP_DATA  stAbacusSetup;
    SINT16          sAbacusStatus;
    UINT16  usTextID;
    BATCH_SURCHARGE stCurrentSurcharge;
    BATCH_SURCHARGE  *pBatchCharge;

    sAbacusStatus = fnAGDGetGlobalSetupData(&stAbacusSetup);
    
    if (sAbacusStatus == ABACUS_IN_USE)
    {
        pBatchCharge = (BATCH_SURCHARGE * )fnAXC_GetBatchSurcharges();  //lint Warning 605:

        if(pBatchCharge->ucBatchSurchargeMethod != SURCH_NONE)
        {
            stCurrentSurcharge.dblSurchargePieceVal = pBatchCharge->dblSurchargePieceVal;
            stCurrentSurcharge.dblSurchargeXactionVal = pBatchCharge->dblSurchargeXactionVal;
            stCurrentSurcharge.sSurchargePercentageVal = pBatchCharge->sSurchargePercentageVal;
            if(pBatchCharge->dblSurchargePieceVal == 0 && 
               pBatchCharge->dblSurchargeXactionVal == 0 &&
               pBatchCharge->sSurchargePercentageVal == 0)
            {
                stCurrentSurcharge.ucBatchSurchargeMethod = SURCH_NONE;
            }
            else
            {
                stCurrentSurcharge.ucBatchSurchargeMethod = pBatchCharge->ucBatchSurchargeMethod;
            }
        }
        
        else if (stAbacusSetup.fGlobalSurchargeEnabled == TRUE)
        {
            stCurrentSurcharge.ucBatchSurchargeMethod = stAbacusSetup.ucGlobalSurchargeMethod;
            stCurrentSurcharge.dblSurchargePieceVal = stAbacusSetup.dblSurchargePieceVal;
            stCurrentSurcharge.dblSurchargeXactionVal = stAbacusSetup.dblSurchargeXactionVal;
            stCurrentSurcharge.sSurchargePercentageVal = stAbacusSetup.sSurchargePercentageVal;
        }

        else
        {
            stCurrentSurcharge.ucBatchSurchargeMethod = SURCH_NONE;
        }
        
         display the current surcharge value
        {
            UINT8   bMinDisplayedIntegers;
            UINT8   bMinDisplayedDecimals;
            UINT8   bValLen;
            double  dblMoneyValue;

//          bMinDisplayedIntegers = fnFlashGetByteParm(BP_MIN_DISPLAYED_INTEGERS);
//          bMinDisplayedDecimals = fnFlashGetByteParm(BP_MIN_DISPLAYED_DECIMALS);
            switch (stCurrentSurcharge.ucBatchSurchargeMethod)
            {
                case SURCH_PERPIECE:
                    dblMoneyValue = fnDoubleAdjustedMoney2Double(stCurrentSurcharge.dblSurchargePieceVal );
//                  bValLen = fnMoney2Unicode(pFieldDest, dblMoneyValue, TRUE, bMinDisplayedIntegers, bMinDisplayedDecimals);
//                  bValLen = fnUnicodeAddMoneySign(pFieldDest, bValLen);
                    bValLen = fnOIFormatMoneyForDisplay( pFieldDest, dblMoneyValue, FALSE ) ;
                     add a space, then append the "per piece" label
                    pFieldDest[bValLen++] = UNICODE_SPACE;
                    EndianAwareCopy(&usTextID, pTableTextIDs + (2 * sizeof(UINT16)), sizeof(UINT16));
                    bValLen += fnCopyTableText(pFieldDest + bValLen, usTextID, bLen - bValLen);
                    break;
                case SURCH_PERXACTION:
                    dblMoneyValue = fnDoubleAdjustedMoney2Double(stCurrentSurcharge.dblSurchargeXactionVal);
//                  bValLen = fnMoney2Unicode(pFieldDest, dblMoneyValue, TRUE, bMinDisplayedIntegers, bMinDisplayedDecimals);
//                  bValLen = fnUnicodeAddMoneySign(pFieldDest, bValLen);
                    bValLen = fnOIFormatMoneyForDisplay( pFieldDest, dblMoneyValue, FALSE ) ;
                     add a space, then append the "per transaction" label
                    pFieldDest[bValLen++] = UNICODE_SPACE;
                    EndianAwareCopy(&usTextID, pTableTextIDs + (3 * sizeof(UINT16)), sizeof(UINT16));
                    bValLen += fnCopyTableText(pFieldDest + bValLen, usTextID, bLen - bValLen);
                    break;
                case SURCH_PERCENT:
                    bValLen = fnSignBin2Unicode(pFieldDest, stCurrentSurcharge.sSurchargePercentageVal);
                     add a percent symbol, a space, then append the "per transaction" label
                    pFieldDest[bValLen++] = 0x0025;
                    pFieldDest[bValLen++] = UNICODE_SPACE;
                    EndianAwareCopy(&usTextID, pTableTextIDs + (3 * sizeof(UINT16)), sizeof(UINT16));
                    bValLen += fnCopyTableText(pFieldDest + bValLen, usTextID, bLen - bValLen);
                    break;

                case SURCH_NONE:
                    if (bNumTableItems != 0)
                    {
                        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                           pTableTextIDs, 0);
                    }
                    break;
                default:     must be multiple surcharge types
                    if (bNumTableItems >= 2)
                    {
                        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                           pTableTextIDs, 1);
                    }
                    break;
            }
        }
    }
*/

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: fnfAccountingSurchargeValue
//
// DESCRIPTION: 
//      Field function that selects the correct field function to
//      populate the surcharge value field on the main screen.  The
//      same screen is shared for abacus and external accounting 
//      (accutrac, meternet) packages, so this function makes the
//      distinction between the them.
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//
// WARNINGS/NOTES:
//
// TEXT TABLE USAGE:    
//      1st string: displayed if no surcharge selected (but abacus acct'ng enabled)
//      2nd string: displayed if more than one surcharge type active
//      3rd string: appended to per piece surcharge if that's the only type active
//      4th string: appended to per transaction surcharge if that's the only type active
//
// MODIFICATION HISTORY:
//      Joe Mozdzer  Initial version
// *************************************************************************/
BOOL fnfAccountingSurchargeValue(UINT16 *pFieldDest, 
                                                 UINT8 bLen, 
                                                 UINT8 bFieldCtrl, 
                                 UINT8 bNumTableItems, 
                                 UINT8 *pTableTextIDs,
                                 UINT8  *pAttributes )
{
    BOOL fRetval = FALSE;

/*
    if ((fnGetAccountingType() == ACT_SYS_TYPE_ACCUTRAC) || 
        (fnGetAccountingType() == ACT_SYS_TYPE_METERNET))
    {
        //ADAMTODO: uncomment the following if accutrac is available
        //fRetval = fnfAccutracMMSurchargeAmount(pFieldDest, bLen, bFieldCtrl, 
        //                                       bNumTableItems, pTableTextIDs,pAttributes);
    }
    else
    {
         must be abacus accounting
        fRetval = fnfAbacusSurchargeValue(pFieldDest, bLen, bFieldCtrl, 
                                          bNumTableItems, pTableTextIDs,pAttributes);
    }
*/
    return (fRetval);
}

/* *************************************************************************
// FUNCTION NAME: fnfAccountingJobIDMain
//
// DESCRIPTION: 
//      Field function that selects the correct field function to
//      populate the job ID field on the main screen.  The
//      same screen is shared for abacus and external accounting 
//      (accutrac, meternet) packages, so this function makes the
//      distinction between the them.
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//
// WARNINGS/NOTES:
//
// TEXT TABLE USAGE:    
//      1st string: displayed if no job id used
//
// MODIFICATION HISTORY:
//      02/26/2007  Adam Liu Adapted from Mega1600
//      Joe Mozdzer  Initial version
// *************************************************************************/
BOOL fnfAccountingJobIDMain(UINT16 *pFieldDest, 
                                        UINT8 bLen, 
                                        UINT8 bFieldCtrl, 
                            UINT8 bNumTableItems, 
                            UINT8 *pTableTextIDs,
                            UINT8  *pAttributes )
{
    BOOL fRetval = FALSE;

/*
    if ((fnGetAccountingType() == ACT_SYS_TYPE_ACCUTRAC) || 
        (fnGetAccountingType() == ACT_SYS_TYPE_METERNET))
    {
        //ADAMTODO: uncomment the following if accutrac is available
        //fRetval = fnfAccutracJobID(pFieldDest, bLen, bFieldCtrl, 
        //                           bNumTableItems, pTableTextIDs, pAttributes);
    }
    else
    {
         must be abacus accounting
//        fRetval = fnfAbacusJobIDMain(pFieldDest, bLen, bFieldCtrl,
//                                     bNumTableItems, pTableTextIDs, pAttributes);
    }
*/
    return (fRetval);
}


/* *************************************************************************
// FUNCTION NAME: fnfDiplayPrintingModeText
//
// DESCRIPTION:
//      Output field function that different Canada printing mode text according
//      to current mode.
//
// RETURNS:
//      always returns successful
//
// WARNINGS/NOTES:  
//        
// MODIFICATION HISTORY:
//     04/23/2007 Andy Mo   Initial Version
// *************************************************************************/
BOOL fnfDiplayPrintingModeText(UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes)
{
    UINT8  ucPrintMode;
        
    SET_SPACE_UNICODE(pFieldDest, bLen);
    ucPrintMode = fnOITGetPrintMode(); 
    if(ucPrintMode == PMODE_POSTAGE_CORRECTION)
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,0);        
    else if(ucPrintMode == PMODE_DATE_CORRECTION)
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,1);        
    else if(ucPrintMode == PMODE_MANIFEST)
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,2);        

    return(SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: fnInstructionLines
//
// DESCRIPTION:
//      uitility function that displays instuctions.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      always returns successful
//      usIndex: softkey index
//
// WARNINGS/NOTES:  
//
//
// MODIFICATION HISTORY:
//  04/17/2007      Andy Mo     Initial version
// *************************************************************************/
 static BOOL fnInstructionLines(UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems,
                                UINT8  *pTableTextIDs, 
                                UINT16  usIndex)
{
    UINT16  usSlotIndex;

    SET_SPACE_UNICODE(pFieldDest, bLen);
    
//    usSlotIndex = pSMenuTable.usFirstDisplayedSlotIndex + usIndex;
    if ((usSlotIndex < MAX_SMENU_SLOTS) && (bNumTableItems != 0)&&(usSlotIndex < bNumTableItems))
    {
            fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, 
                                bNumTableItems, pTableTextIDs, usSlotIndex);
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: fnfDisplayInstructionLine1-4
//
// DESCRIPTION:
//      Output field function that displays the instructions lines.
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      always returns successful
//
// WARNINGS/NOTES:  
//      Display instructions lines in the table according to the line number
//      passed in this functions.    
//
// MODIFICATION HISTORY:
//     04/17/2007 Andy Mo   Initial Version
// *************************************************************************/
BOOL fnfDisplayInstructionLine1(UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes)
{
    return  fnInstructionLines( pFieldDest, bLen, bFieldCtrl,
                                bNumTableItems, pTableTextIDs, 0);
}

BOOL fnfDisplayInstructionLine2(UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes)
{
    return  fnInstructionLines( pFieldDest, bLen, bFieldCtrl,
                                bNumTableItems, pTableTextIDs, 1);
}


BOOL fnfDisplayInstructionLine3(UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes)
{
    return  fnInstructionLines( pFieldDest, bLen, bFieldCtrl,
                                bNumTableItems, pTableTextIDs, 2);
}


BOOL fnfDisplayInstructionLine4(UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes)
{
    return  fnInstructionLines( pFieldDest, bLen, bFieldCtrl,
                                bNumTableItems, pTableTextIDs, 3);
}



/* *************************************************************************
// FUNCTION NAME: fnfMeterStamp
//
// DESCRIPTION:
//      Output field function that displays the current selected indicia type
//      Standard field function inputs.
//
// RETURNS:
//      always returns successful
//
// WARNINGS/NOTES:  
//
// MODIFICATION HISTORY:
//     09/24/2008   Bob Li      Undo the fake codes to get the real selected indicia.
//     08/05/2008   Bob Li      Initial Version
// *************************************************************************/
BOOL fnfMeterStamp( UINT16 *pFieldDest, 
                    UINT8   bLen, 
                    UINT8   bFieldCtrl,
                    UINT8   bNumTableItems, 
                    UINT8  *pTableTextIDs, 
                    UINT8  *pAttributes)
{
    UINT8 ucIdx = 0;

    SET_SPACE_UNICODE(pFieldDest, bLen);

    ucIdx = fnGetCurrentIndiSelection();  // 3 text strings for the indicia type
    fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                        pTableTextIDs, ucIdx);

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: fnfFencingLimitLowValue
//
// DESCRIPTION:
//      Output field function that displays the low value of fencing limit
//      Standard field function inputs.
//
// RETURNS:
//      always returns successful
//
// WARNINGS/NOTES:  
//      Display instructions lines in the table according to the line number
//      passed in this functions.    
//
// MODIFICATION HISTORY:
//     02/11/2014 Wang Biao Modified code to fix fraca 224400.
//     04/17/2007 Andy Mo   Initial Version
// *************************************************************************/
BOOL fnfFencingLimitLowValue(UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes)
{
    double  dblLowFencingLimit;
    UINT8   bMinDisplayedIntegers,bMinDisplayedDecimals;
    UINT8   bPVLen;

    SET_SPACE_UNICODE(pFieldDest, bLen);
    if(fnFlashGetByteParm( BP_ADD_POSTAGE_MODE )== ADD_POSTAGE_ALLOWED_FENCINGLIMIT)
    {   
/*        if((fnRateRMInitialized() == true)&&(fnRateGetFencingLimitStatus() == SUCCESS))
        {
            dblLowFencingLimit = fnRateGetLowFencingLimit();

            //dblLowFencingLimit = fnRateGetLowFencingLimit();
             convert it to Unicode
            bMinDisplayedIntegers = fnGetMinDisplayedIntegers();
            bMinDisplayedDecimals = fnGetMinDisplayedDecimals();

            bPVLen = fnMoney2Unicode(pFieldDest, dblLowFencingLimit, TRUE, 
                                    bMinDisplayedIntegers, bMinDisplayedDecimals);
            bPVLen = fnUnicodeAddMoneySign(pFieldDest, bPVLen);
             if field is right-aligned, pad left of string with spaces
            if (bFieldCtrl & ALIGN_MASK)
            {
                fnUnicodePadSpace(pFieldDest, bPVLen, bLen);
            }

             make sure the field is NULL terminated.  if the translation happened
                to exceed the width of this field, this will truncate the unicode 
                string that is sitting there
            pFieldDest[bLen] = (UINT16) NULL;

        }*/
    }

    return(SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: fnfFencingLimitHighValue
//
// DESCRIPTION:
//      Output field function that displays the high value of fencing limit
//      Standard field function inputs.
//
// RETURNS:
//      always returns successful
//
// WARNINGS/NOTES:  
//      Display instructions lines in the table according to the line number
//      passed in this functions.    
//
// MODIFICATION HISTORY:
//     02/11/2014 Wang Biao Modified code to fix fraca 224400.
//     04/17/2007 Andy Mo   Initial Version
// *************************************************************************/
BOOL fnfFencingLimitHighValue(UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes)
{
    double  dblHighFencingLimit;
    UINT8   bMinDisplayedIntegers,bMinDisplayedDecimals;
    UINT8   bPVLen;
    
    SET_SPACE_UNICODE(pFieldDest, bLen);
    if(fnFlashGetByteParm( BP_ADD_POSTAGE_MODE )== ADD_POSTAGE_ALLOWED_FENCINGLIMIT)
    {   
/*        if((fnRateRMInitialized() == true)&&(fnRateGetFencingLimitStatus() == SUCCESS))
        {
            dblHighFencingLimit = fnRateGetHighFencingLimit();

             convert it to Unicode
            bMinDisplayedIntegers = fnGetMinDisplayedIntegers();
            bMinDisplayedDecimals = fnGetMinDisplayedDecimals();

            bPVLen = fnMoney2Unicode(pFieldDest, dblHighFencingLimit, TRUE, 
                                    bMinDisplayedIntegers, bMinDisplayedDecimals);
            bPVLen = fnUnicodeAddMoneySign(pFieldDest, bPVLen);
             if field is right-aligned, pad left of string with spaces
            if (bFieldCtrl & ALIGN_MASK)
            {
                fnUnicodePadSpace(pFieldDest, bPVLen, bLen);
            }

             make sure the field is NULL terminated.  if the translation happened
                to exceed the width of this field, this will truncate the unicode 
                string that is sitting there
            pFieldDest[bLen] = (UINT16) NULL;

        }*/
    }
    return(SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: fnfInitPostageCorrectionAmount
//
// DESCRIPTION:
//      Output field function that is called to initalize the input field and 
//      displays the correction amount in last correction printing. 
//
// RETURNS:
//      always returns successful
//
// WARNINGS/NOTES:  
//      Display the amount used in last printing    
//
// MODIFICATION HISTORY:
//     04/17/2007 Andy Mo   Initial Version
// *************************************************************************/
BOOL fnfInitPostageCorrectionAmount(UINT16 *pFieldDest, 
                                    UINT8   bLen, 
                                    UINT8   bFieldCtrl,
                                    UINT8   bNumTableItems, 
                                    UINT8  *pTableTextIDs, 
                                    UINT8  *pAttributes)
{
    UINT16  pBuffer[MAX_FIELD_LEN+1];
    UINT8   ucIndex=0;
    UINT32 ulPostageValue;
    double dblPostage;
    UINT8       bMinDisplayedIntegers;
    UINT8       bMinDisplayedDecimals;
    UINT8       bPVLen;

    
    (void) fnfStdEntryInit(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, pAttributes); 
    // come from MMPostageCorrectionReady screen.
    if(fnGetPreviousScreenID() == GetScreen(SCREEN_POSTAGE_CORRECTION_READY))
    {
        if(fnGetDebitValue(&ulPostageValue) != TRUE)
            return(SUCCESSFUL);   
        /* convert it to Unicode */
        bMinDisplayedIntegers = fnGetMinDisplayedIntegers();
        bMinDisplayedDecimals = fnGetMinDisplayedDecimals(); 
        bPVLen = fnMoney2Unicode(pBuffer, (double)(ulPostageValue), TRUE, 
                                bMinDisplayedIntegers, bMinDisplayedDecimals);

         while (ucIndex < bPVLen)
         {
              fnBufferUnicode(pBuffer[ucIndex++]);
         }
         return(fnfStdEntryRefresh(pFieldDest, bLen, bFieldCtrl, 
                                    bNumTableItems, pTableTextIDs, pAttributes));
    }

    return(SUCCESSFUL); 
}


/* Hard key functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkExitSealOnlyMode
//
// DESCRIPTION: 
//      hard key function that set the seal only mode status to false.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      Next screen 1, Home Ready screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Victor Li       Initial version
//  12/6/2005   Vincent Yi      Code cleanup
//
// *************************************************************************/
T_SCREEN_ID fnhkExitSealOnlyMode(UINT8          bKeyCode, 
                                 UINT8          bNumNext, 
                                 T_SCREEN_ID *  pNextScreens)
{
    T_SCREEN_ID usNext = 0;

    /* Set the print mode to a default mode, which is either key in 
        postage mode or a weighing mode. */
    (void)fnModeSetDefault();

    if (bNumNext > 0)
    {
        usNext = pNextScreens[0];
    }

    return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnhkExitPermitMode
//
// DESCRIPTION: 
//      hard key function that exit permit mode.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      Next screen 1, Home Ready screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  09/11/2005  Adam Liu       Reset PermitScreenPage no on exit permit mode.
//  05/16/2005  Adam Liu       Initial version
// *************************************************************************/
T_SCREEN_ID fnhkExitPermitMode ( UINT8          bKeyCode, 
                                 UINT8          bNumNext, 
                                 T_SCREEN_ID *  pNextScreens)
{
    T_SCREEN_ID usNext = 0;

    /* turn off permit when exiting permit mode */
    // FP will restore the last permit selection - Adam
    //(void)fnAISetActive( PERMIT_COMP_TYPE , 0); 

    /* Set the print mode to a default mode, which is either key in 
        postage mode or a weighing mode. */
    (void)fnModeSetDefault();

    fnResetPermitScreenPageNo();
    
    if (bNumNext > 0)
    {
        usNext = pNextScreens[0];
    }

    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkClearPermitBatch
//
// DESCRIPTION: 
//      hard key function that clears the batch count of currently selected permit.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      Permit main ready screen if clearing successful.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/16/2005  Adam Liu       Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkClearPermitBatch( UINT8          bKeyCode, 
                                 UINT8          bNumNext, 
                                 T_SCREEN_ID *  pNextScreens)
{
    T_SCREEN_ID usNext = 0;

    // NULL permit name means currently selected permit
    if (ClearPermitBatch(NULL) == TRUE) 
    {
        usNext = pNextScreens[0];
    }

    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkTogglePrintDateTime
//
// DESCRIPTION: 
//      Hard key function that toggle the datetime 
//      between PRINT and DO NOT PRINT.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY:
//  07/13/2007  Adam Liu       Remove the date ducking special process in Ad-Datetime mode
//  07/03/2007  Adam Liu       Ommited the date ducking EMD switch for AdDatetime mode
//  2006.07.27 Raymond Shen - Add the logic of changing "duck date-time" setting.
//  2006.07.18 Clarisa Bellamy - Add call to fnSYSAllowRepeatOIEvent so that 
//              the normal Enter Print Ready process will complete.  This will
//              allow the new image to be downloaded and the first print run, 
//              after the change, to print correctly.
//              Adam Liu       Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkTogglePrintDateTime (UINT8          bKeyCode, 
                                     UINT8          bNumNext, 
                                     T_SCREEN_ID *  pNextScreens)
{
    T_SCREEN_ID usNext = 0;
    UINT16   usTempBobWord = 0;

    //toggle datetime flag
    fnSetPrintDateTimeFlag (!fnCheckIsPrintDateTimeOn());

    if (bNumNext > 0)
    {
        usNext = pNextScreens[0];
    }
    
    // This allows the Enter Print Ready event to be sent to SYS, even if it was the last 
    //  event sent to sys from OI, so that the new static image will be loaded by the 
    //  "screen change" even when we don't ever leave the print ready state.
    // The current screen must be in the next screen list for this to work, by the way.
    fnSYSAllowRepeatOIEvent( OIT_ENTER_PRINT_RDY );

    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: fnhkRebootUIC
// DESCRIPTION: Hard key function that reboots the UIC.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:      Standard hard key function inputs, format 1
// NEXT SCREEN:  Does not matter since we are rebooting
// *************************************************************************/
unsigned short fnhkRebootUIC(unsigned char bKeyCode, unsigned short wNextScr1, unsigned short wNextScr2)
{
    RebootSystem();
    return (0);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnhkMfgToService
//
// DESCRIPTION: 
//      Hard key function that can change screen from Manufacturing screen to
//      service screen.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY:
//      01/19/2007    Oscar Wang   Initial version
// *************************************************************************/
T_SCREEN_ID fnhkMfgToService (UINT8          bKeyCode, 
                                     UINT8          bNumNext, 
                                     T_SCREEN_ID *  pNextScreens)
{
    T_SCREEN_ID usNext = pNextScreens[0];

//    fnProcessEventMfgToService();

    fIsFromManufacturing = TRUE;

    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkServiceToMfg
//
// DESCRIPTION: 
//      Hard key function that can change screen from service screen to
//      Manufacturing screen.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY:
//      01/19/2007    Oscar Wang   Initial version
// *************************************************************************/
T_SCREEN_ID fnhkServiceToMfg (UINT8          bKeyCode, 
                                     UINT8          bNumNext, 
                                     T_SCREEN_ID *  pNextScreens)
{
    T_SCREEN_ID usNext = pNextScreens[0];

//    fnProcessEventServiceToMfg();
    fIsFromManufacturing = FALSE;
    
    return (usNext);
}


/* *************************************************************************
// FUNCTION NAME: fnhkAbacusEndBatch
//
// DESCRIPTION: 
//      Hard key function that ends an abacus batch in progress, assuming
//      there is one in progress.
//
// INPUTS:  
//      Standard hard key function inputs, format 1
//
// RETURNS:
//
// WARNINGS/NOTES:
//
// NEXT SCREEN: 
//      Goes to next screen 1 if we ended the batch in progress, otherwise
//      stays on the same screen
//
// MODIFICATION HISTORY:
//      Joe Mozdzer  Initial version
// *************************************************************************/
UINT16 fnhkAbacusEndBatch(UINT8 bKeyCode, 
                          UINT16 wNextScr1, 
                          UINT16 wNextScr2)
{
    SINT16  sAbacusStatus;
    UINT16  usDetailAbacusStatus;
    UINT16  usNext = 0;

    sAbacusStatus = fnAGDGetAbacusStatus(&usDetailAbacusStatus);
/*
    if (sAbacusStatus == ABACUS_IN_USE)
    {
        if (fnAXB_AreThereOpenTransactions() == TRUE)
        {
            (void)fnAXC_JournalXactions();   //lint
        }
        usNext = wNextScr1;
    }
*/
	
    return (usNext);
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnhkBufBlankOrChangeScr
//
// DESCRIPTION:
//         Hard key function that does one of the following:
//         1) If the current entry buffer is not blank (all spaces),
//            the entry buffer is cleared and we stay on the same screen.
//         2) If the current entry buffer is blank, we
//            exit current mode and go to HOME page
//
// INPUTS:
//         Standard hard key function inputs (format 2).
//
// RETURNS:
//         Next screen: If entry buffer is clear, this function transitions to usNextScr1.
//                      If entry buffer is not clear, we stay on the same screen.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//  07/04/2007  Raymond Shen    Added print mode check into the condition check.
//  06/29/2007  Raymond Shen    Modified it to fix fraca 122018
// 04/18/2007  Andy Mo  Initial version 
// *************************************************************************/
T_SCREEN_ID fnhkBufBlankOrExit( UINT8       bKeyCode, 
                                UINT8          bNumNext, 
                                T_SCREEN_ID *  pNextScreens)
{
    T_SCREEN_ID  usNext = 0; 

    if (fnIsBufferBlank() == FALSE)
    {
        fnClearEntryBuffer();
    }
/*
    else if((fnGetAccountingType() == ACT_SYS_TYPE_INTERNAL_ON)
            && ((fnOITGetPrintMode() == PMODE_POSTAGE_CORRECTION)
                ||(fnOITGetPrintMode() == PMODE_MANIFEST)))
    {
        usNext = fnhkSelectAcct(bKeyCode, bNumNext-1, &pNextScreens[1]);
    }
*/
    else
    {
        usNext = fnhkExitCanadaPrintingMode(bKeyCode, 1, &pNextScreens[0]);
    }

    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkExitInstructions
//
// DESCRIPTION: 
//      hard key function that exits instuctions.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      Next screen 1, Home Ready screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  09/23/2009       Rocky Dai   Modified to fix GMSE00170556
//  04/17/2007       Andy Mo     Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkExitInstructions(   UINT8          bKeyCode, 
                                    UINT8          bNumNext, 
                                    T_SCREEN_ID *  pNextScreens)
{
    T_SCREEN_ID usNext = pNextScreens[0];

    // Commented out the following lines because we want respond from 'Back' key 
    // wherever we are (ie. for both page 1 and 2) --Rocky
    /*
    UINT16  usTotalItems;
    //Check if it is the last line of the instuctions
    usTotalItems = fnGetMenuEntriesTotalNumber();
    if(pSMenuTable.usFirstDisplayedSlotIndex+pSMenuTable.bEntriesPerPage == usTotalItems)   // the last page
        usNext = pNextScreens[0];
    */
          
    return(usNext); 
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkExitCanadaPrintingMode
//
// DESCRIPTION: 
//      hard key function that set the datetime only mode status to false.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      Next screen 1, Home Ready screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/04/2007  Raymond Shen    Counted on Date correction. 
//  06/29/2007  Raymond Shen    Restricted the current mode to postage correction and manifest
//                              to fox fraca 122018.
//  12/23/2005  Adam Liu        Code cleanup
//              Victor Li       Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkExitCanadaPrintingMode( UINT8          bKeyCode, 
                                        UINT8          bNumNext, 
                                        T_SCREEN_ID *  pNextScreens)
{
    T_SCREEN_ID usNext = 0;

    if(( fnOITGetPrintMode() == PMODE_POSTAGE_CORRECTION)
        ||(fnOITGetPrintMode() == PMODE_MANIFEST)
        ||(fnOITGetPrintMode() == PMODE_DATE_CORRECTION))
    {
        /* Set the print mode to a default mode, which is either key in 
            postage mode or a weighing mode. */
        (void)fnModeSetDefault();
    }

    if (bNumNext > 0)
    {
        usNext = pNextScreens[0];
    }

    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: fnhkGotoMeterStamp
// DESCRIPTION: 
//
// AUTHOR: Tom Zhao
//
// INPUTS:  Standard hard key function inputs (format 2).
// NEXT SCREEN:  Mainscreen.
// *************************************************************************/
T_SCREEN_ID fnhkGotoMeterStamp( UINT8         bKeyCode, 
                                UINT8         bNumNext, 
                                T_SCREEN_ID * pNextScreens)
{
    UINT16  usNext = 0;

    if( fnIsIBILiteIndiciaSupport() == TRUE )
    {
        usNext = pNextScreens[0];
    }

    return usNext;
}

/* Event functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fnevUpdateWarningMessages
//
// DESCRIPTION: 
//      Event function to update the warning messages, and cycling display 
//      warning strings.
//
// INPUTS:
//      Standard event function inputs. (format 2)
//
// OUTPUTS:
//      usTotalWarnings         - the total number of occurred warnings
//      usDisplayedWarningIndex - the index of current displayed warning in 
//                                  the FieldText list
//      usDisplayedWarningSeqNo - the sequence number of current displayed 
//                                  warning in all occurred warnings
//
// RETURNS:
//      No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  06/16/2009  Jingwei,Li      If host connect status changed, update warning string.
//  04/03/2007  Derek DeGennaro Branch to PSOC Please Wait if the PSOC is
//                              busy doing something.
//  11/17/2006  Adam Liu       Fix fraca GMSE00106326 by changing the 
                               "printing..." animator to use screen ticks.
//  11/11/2005  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevUpdateWarningMessages (UINT8            bNumNext, 
                                       T_SCREEN_ID  *   pNextScreens, 
                                       INTERTASK    *   pIntertask)
{
    T_SCREEN_ID usNextScreen = 0;
    UINT8 bHostStatus = 0;

/*
    if( fnGetAccountingType() == (UINT8) ACT_SYS_TYPE_HOSTED_ABACUS)
    {

        if(ucHostStatus !=IsHostConnected())
        {
            usNextScreen = pNextScreens[0];
            ucHostStatus = IsHostConnected();
        }

  
    }
*/
    if ((fnIsDisabledStatusSet(DCOND_PSOC_PROGRAMMING_IN_PROG) || fnIsDisabledStatusSet(DCOND_PSOC_RESEED_IN_PROG)) 
        && fInRunningScreen == FALSE)
    {
        // The branching to the please wait is done in this 
        // function to catch instances where the disabling condition was posted after
        // going to the print ready screen - thereby bypassing
        // the disabling condition check on the main screen pre-function.
        // The PSOC is doing something, the system isn't running,
        // so branch to the PSOC Please Wait screen. 
        usNextScreen = GetScreen(SCREEN_PSOC_PLEASE_WAIT);
    }
    else if (ucMsgDisplayDivisor == 1)
    {
        ucMsgDisplayDivisor = 0;
        if (fInRunningScreen == TRUE)
        {
            fnUpdateCMWarnings();
            fnUpdateWarningMessages (TRUE);
        }
        else
        {
            fnUpdateWarningMessages (FALSE);
        }
    }
    else
    {
        ucMsgDisplayDivisor++;
    }

    //update "printing..."
    fnUpdateAnimatorCounter();
    
    return usNextScreen;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnevUpdateErrorMessages
//
// DESCRIPTION: 
//      Event function to update the error messages, and cycling display 
//      error strings.
//
// INPUTS:
//      Standard event function inputs. (format 2)
//
// OUTPUTS:
//      usTotalErrors           - the total number of occurred errors
//      usDisplayedErrorIndex   - the index of current displayed error in 
//                                  the FieldText list
//      usDisplayedErrorSeqNo   - the sequence number of current displayed 
//                                  error in all occurred errors
//
// RETURNS:
//      No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  11/11/2005  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevUpdateErrorMessages (UINT8          bNumNext, 
                                     T_SCREEN_ID  * pNextScreens, 
                                     INTERTASK    * pIntertask)
{
    T_SCREEN_ID usNextScreen = 0;

    if (ucMsgDisplayDivisor == 1)
    {
        ucMsgDisplayDivisor = 0;
        fnUpdateErrorMessages (FALSE);
    }
    else
    {
        ucMsgDisplayDivisor++;
    }
        
    return usNextScreen;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnevWaitPSOCDisablingConditionsClear
//
// DESCRIPTION: 
//      Event function that waits for the PSOC to clear its disabling conditions.
//
// INPUTS:
//      Standard event function inputs. (format 2)
//
// OUTPUTS:
//      
// RETURNS:
//      Next Screen 1 (index 0)
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  04/03/2007 Derek DeGennaro Initial Version
//
// *************************************************************************/
T_SCREEN_ID fnevWaitPSOCDisablingConditionsClear (UINT8            bNumNext, 
                                       T_SCREEN_ID  *   pNextScreens, 
                                       INTERTASK    *   pIntertask)
{
    T_SCREEN_ID usNextScreen = 0;

    while (fnIsDisabledStatusSet(DCOND_PSOC_PROGRAMMING_IN_PROG) || fnIsDisabledStatusSet(DCOND_PSOC_RESEED_IN_PROG))
    {
        // wait for the PSOC Task to clear the disabling condition
        (void)OSWakeAfter(1000);
    }

    if (bNumNext)
        usNextScreen = pNextScreens[0];

    return usNextScreen;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnevDCAPDoFlashWrite 
//
// DESCRIPTION: 
//      Event function that backup DataCapture information
//
// INPUTS:
//      Standard event function inputs. (format 1)
//
// RETURNS:
//      No screen change/Home screen.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/14/2006  Adam Liu                            Code Cleanup
//              Derek Degennaro & Maya Mansjur      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnevDCAPDoFlashWrite    (T_SCREEN_ID    wNextScr1, 
                                     T_SCREEN_ID    wNextScr2, 
                                     INTERTASK    * pIntertask)
{
//TODO JAH Remove dcapi.c     usDcapWriteStatus &= ~DCAP_FLASH_WRITE_PENDING; /* Vinny was getting tired of writing :-) */
//TODO JAH Remove dcapi.c     fnDCAPDoFlashWrite();
    
    return(wNextScr1);
}

/* *************************************************************************
// FUNCTION NAME: fnevRemoteRefillDone
//
// DESCRIPTION: 
//      Event function that checks If the system is remote controlled and determine
//      which screen  to jump back.
//
// INPUTS:  
//      Standard event function inputs (format 2).
//
// RETURNS:
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      Yingbo Zhang  Initial version
// *************************************************************************/
UINT16 fnevRemoteRefillDone(UINT8 bNumNext, 
                                   UINT16 *pNextScreens, 
                                   INTERTASK *pIntertask)
{
    UINT16 usNext;

    usNext = pNextScreens[0];   //MMIndiciaReady    
        
    return usNext;  
}

/* Variable graphic functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fngHomeWarningDisplayed
//
// DESCRIPTION: 
//      Variable graphic function that displays the bottom separator line 
//      according to whether warnings occur or not.
//
// PRE-CONDITIONS:
//      The total number of the warnings has been set into usTotalWarnings.
//
// INPUTS:
//      Standard graphic function inputs.
//
// OUTPUTS:
//      pSymID points to the buffer where stores the displayed Unicode graphics.
//
// RETURNS:
//      always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/09/2009  Raymond Shen    Always show the bottom line on Home screen for DM475C.
//  11/14/2005  Vincent Yi      Initial Version
//
// *************************************************************************/
BOOL fngHomeWarningDisplayed (VARIABLE_GRAPHIC  *pVarGraphic, 
                              UINT16            *pSymID, 
                              UINT8             bNumSymListItems,                       
                              UINT8             *pSymList,
                              UINT8             *pAttributes)
{
    pVarGraphic->fUsed = FALSE;
    *pSymID = (UINT16) 0x0000;

    if ( usTotalWarnings > 0 )
    {   // Display the bottom separator line
    	EndianAwareCopy((UINT8 *)pSymID, pSymList, sizeof(UINT16));
    }
    
    return (BOOL)SUCCESSFUL;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fngHomeErrorDisplayed
//
// DESCRIPTION: 
//      Variable graphic function that displays the bottom separator line 
//      according to whether errors occur or not.
//
// PRE-CONDITIONS:
//      The total number of the errors has been set into usTotalWarnings.
//
// INPUTS:
//      Standard graphic function inputs.
//
// OUTPUTS:
//      pSymID points to the buffer where stores the displayed Unicode graphics.
//
// RETURNS:
//      always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/10/2009  Raymond Shen    Always show the bottom line on Home screen for DM475C.
//  11/14/2005  Vincent Yi      Initial Version
//
// *************************************************************************/
BOOL fngHomeErrorDisplayed (VARIABLE_GRAPHIC    *pVarGraphic, 
                            UINT16              *pSymID, 
                            UINT8               bNumSymListItems,                       
                            UINT8               *pSymList,
                            UINT8               *pAttributes)
{
    pVarGraphic->fUsed = FALSE;
    *pSymID = (UINT16) 0x0000;

    if ( usTotalErrors > 0  )
    {   // Display the bottom separator line
    	EndianAwareCopy((UINT8 *)pSymID, pSymList, sizeof(UINT16));
    }
    
    return (BOOL)SUCCESSFUL;
}



/**********************************************************************
        Private Functions
**********************************************************************/

/* *************************************************************************
// FUNCTION NAME: 
//      fncIndiciaReadyCheck
//
// DESCRIPTION: 
//      Continuation function that continues the Indicia ready checking after
//      requesting CM status.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr, depends on the conditions checking
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/13/2008  Joey Cui        Send idle message if PC is connected
//  07/10/2006  Raymond Shen    Added error status updating function for the
//                              routine that error occurs.
//  06/30/2006  Raymond Shen    Remove the codes that do menu init for screen
//                              MMIndiciaError(move to fnpIndiciaError())
//  11/08/2005  Vincent Yi      Initial version 
//
// *************************************************************************/
static SINT8 fncIndiciaReadyCheck( void         (** pContinuationFcn)(), 
                                   UINT32       *   pMsgsAwaited,
                                   T_SCREEN_ID  *   pNextScr )
{
    SINT8 ucRetval = PREPOST_COMPLETE;

    *pNextScr = 0;

    *pNextScr = fnIndiciaReadyConditionsCheck2();

    if (*pNextScr == 0)
    {
        // Prepare the menu table
        fnHomeScreenMenuInit(TRUE);
        // Check all warning conditions for displaying the warning messages and
        // counter at the first time
        fnUpdateWarningMessages(TRUE);
        /* Send idle message if PC host is connected */
//Ram commented not to send PC unsolicited message
//        (void)fnPCSendUnsolicitedStatus(fnGetCurrentScreenID());
    }
    else if (*pNextScr == GetScreen (SCREEN_MM_INDICIA_ERROR))
    {   
        // Check all error conditions for displaying the error messages and
        // counter at the first time
        fnUpdateErrorMessages(TRUE);
    }

    return ucRetval;        
}

/**************************************************************************
// FUNCTION NAME: 
//      fnNoLongerInZeroPrint
//
// DESCRIPTION: 
//      Check we're no longer in Zero Print mode.
//
// INPUTS:
//      void
//
// OUTPUTS:
//      None
//
// RETURNS:
//      1 if we're no longer in Zero Print mode; otherwise 0
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12 Nov 2007   Martin O'Brien   Function created
//  29 Nov 2007   Martin O'Brien   added the 'static' storage class specifier
// *************************************************************************/
static BOOL fnNoLongerInZeroPrint(void)
{
    UINT8 manual_weight[LENWEIGHT];
    UINT8 pZeroWeight[LENWEIGHT] = {0};
    int print_mode = fnOITGetPrintMode();

    fnGetManualWeight( manual_weight, FALSE );
    return (    fnCheckForZeroPrint()
         && print_mode != PMODE_MANUAL
//         && fnGetZeroPostage()
         && ! (   print_mode == PMODE_MANUAL_WGT_ENTRY
               && memcmp(manual_weight, pZeroWeight, ARRAY_LENGTH(pZeroWeight)) == 0
              )
        );
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIndiciaReadyConditionsCheck
//
// DESCRIPTION: 
//      Private function to check warning & disabling conditions in 
//      fnpIndiciaReadyCheck()
//
// INPUTS:
//      None
//
// RETURNS:
//      Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY: 
// 22-Jul-14   Renhao           Modified code to put up the "Rates Expired" screen when ANY of 
//                              the rates/DCM files has expired.
//  02-May-14  S. Peterson		Undid Wang Biao's change for fraca 225110 because it
//                              wasn't what I asked CieNET to do for the fraca.
// 29-Apr-2014  Wang Biao       Remove the codes about checking rates expirition to the function fnIndiciaReadyConditionsCheck2()
//                              to fix fraca 225110.
// 03-Apr-2014  Renhao          Update for "New Rates Effective" and "Rates Expire" check.
// 18-Mar-2014  Wang Biao       Modified to fix fraca 222452.
// 22-Feb-2013	S. Peterson		Removed check for NEW_PERIOD_REQUIRED, not needed because it can be
//                              covered by UPLOAD_REQUIRED.
//  09/26/2012  Bob Li          Modified this function to check the print mode if feature enabled for fixing fraca 218434.
//  09/21/2009  Bonnie Xu       Add the case for PORTUGAL
//  07/15/2009  Bonnie Xu       Add the case for AUSTRIA
//  2009.06.29 Clarisa Bellamy - Added check for the DCAPUploadStatus set to NEW_PERIOD_REQUIRED.  If 
//                              it is, then set the disabling condition bit: DCOND_DCAP_MANUAL_UPLOAD_REQ.
//                              This supports Brazil, which requires that we block the meter until the  
//                              new period endlist is downloaded via a direct Balance Inquiry. 
//  05/05/2008  Jingwei,Li      Modified for revert to WOW case.
//  12/01/2008  Jingwei,Li        Added WOW/W1P modes support.
//  09/24/2008  Joey Cui        Added check for Active Operator Number before go to Operator login screen.
//  09/12/2008  P.Vrillaud      Add disabling condition (DCOND_NEED_CLASS) in case of INV_WEIGHT.
//  09/12/2008  Oscar Wang      Fix fraca148331: clear class if INV_WEIGHT occurred but not recovered yet.
//  07/18/2008  Raymond Shen    Added code to check PSD/CMOS battery life.
//  05/26/2008  Joey Cui        Added code to post errer message when there is not archive file found
//  29 Nov 2007 Martin O'Brien  Removed the lone semicolons in two blocks to make them truly empty
//  11/28/2007  Joey Cui        Move Operator Login checking process to the beginning 
//                              Fix FRACA GMSE00133812
//   1 Nov 2007 Martin O'Brien  Function now disables Zero Print when the franking is over and 
//                              Zero Print is no longer a disabling condition
//  10/24/2007  Oscar Wang      Replace fnhkModeSetDefault() with fnRatingSetDefault().
//  10/19/2007  Oscar Wang      Fix FRACA GMSE00131327: Upload should prompt 
//                              prior to preset loading.
//  10/18/07    P.VRILLAUD      FPHX 1.09 (Swiss adaptations) merge from K7
//  09/28/2007  Raymond Shen    1. Confined My EKP initializing code to German to 
//                              avoid causing US DUNS number problem.
//                              2. Change calling of fnRateCheckZWZPManWgt() to fnRateCheckZWManWgt().
//  09/24/2007  Raymond Shen    Confine the disabling condition of "Postage Value Too Low" 
//                              for MWE 0 wt to German system.
//  09/03/2007  Oscar Wang      When power up, don't care weight unstable case since the weight is often not 
//                              stable when loading GTS normal preset.In this case, we don't want to display 
//                              "Place item on scale"
//  08/23/2007  Oscar Wang      Fix FRACA GMSE00127908, don't update DCAP need class disable condition 
//                              for 0 manual weight case.
//  08/23/2007  Raymond Shen    Fixed fraca 127849: if "Select Class" error message is shown, 
//                              don't show "Postage Value Too Low".
//  08/22/2007  Oscar Wang      Added GTS integrity check. 
//  08/20/2007  Raymond Shen    Fixed fraca 125251 and 125252: For Canada meter it should update
//                              DCap service code while switching between zero and none-zero postage
//                              value in KIP mode.
//  08/16/2007  Oscar Wang      Check unmanifested record.
//  08/10/2007  Raymond Shen    Added code to initialize My EKP in case that it is
//                              skipped during OOB.
//  08/09/2007  Oscar Wang      Updated for VAS disable check.
//                              Added 0 manual weight error display
//  08/09/2007 Raymond Shen     Added code to check German Piece ID Duck status.
//  08/06/2007  Joey Cui        Modified the reply code checking condition
//  07/31/2007  Joey Cui        Add Recode & Barcode warning/disabled condition check
                                Add Reply Code check
//  23 Jul 2007 Simon Fox       Modified checks for unstable scale and added specific checks for
//                              scale timeouts.  Unstable checks now work correctly in all printing
//                              modes.
//  07/23/2007  I. Le Goff      Set up weight disabling condition when the scale is not
//                              stable
//  07/05/2007  Oscar Wang      If debit is done but GTS printing is stopped by mail jam or such error, 
//                              we'll continue the normal GTS workflow. Fix FRACA GMSE00123827.
//  06/19/2007  D. Kohl         Re-activate management of piece count end of life
//  06/07/2007  Oscar Wang      For EService Express mode, keep rating info when going to home screen.
//  05/31/2007  Raymond Shen    Add code to fix fraca 116039:check whether it need to restore
//                              status of previous print mode.
//  05/10/2007  Raymond Shen    Modified it to show abacus account related warning in main screen.
//  04/11/2007  Joey Cui        Add the code check for MXaction setting condition
//  04/10/2007  Raymond Shen    Added code to check operator
//  04/10/2007  I. Le Goff      Remove temporary code to test powerfail
//  03/12/2007  I. Le Goff      Move redirection to mode ajout to fnpIndiciaReadyCheck
//  03/29/2007  Oscar Wang      Added rate change check.
//  03/12/2007  I. Le Goff      Add redirection to mode ajout if necessary
//  02/12/2006  Kan Jiang       Add the calling of function fnGetPresetNextScr
//  02/09/2006  Kan Jiang       Modified for Fraca GMSE00114529
//  12/15/2006  Oscar Wang      Update for "Need class" check.
//  12/14/2006  Adam Liu        Update for adding DCAP check
//  12/12/2006  Oscar Wang      Support KIP carrier.
//  09/05/2006  Dicky Sun       Update for auto-deleting GTS manifest record.
//  08/10/2006  Dicky Sun       Update for Clear key and Home key in GTS 
//                              screens.
//  07/31/2006  Oscar Wang      Modified for re-rate. 
//  05/19/2006  James Wang      To get debit value and descending register 
//                              value from bob with some new BR functions.
//  11/09/2005  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnIndiciaReadyConditionsCheck(void)
{
    #define COMING_FROM_LOGFULL 2
    T_SCREEN_ID usNextScreen = 0;

    UINT32      ulPostageValue;
    UINT8       bBobErrorClass;
    UINT8       bBobErrorCode;
    UINT8       pDescRegValue[SPARK_MONEY_SIZE];
    UINT8       bPrintMode;
    UINT8       uiEolStat;

    DC_UPLOAD_STATUS_T  DCAPUploadStatus;
    UINT8  usDCAPSasFlag=0;
    UINT32 lwPostageValue = 0;
    UINT8    bKIPCarType = fnFlashGetByteParm(BP_KEY_IN_POSTAGE_CLASS);  // get KIP Carrier type

    UINT16  usLogRecords=0;
    T_SCREEN_ID usTempScr;
    UINT16  wCountryCode = fnGetDefaultRatingCountryCode();
    BOOL    fCurrentModeDisable = FALSE;
    BOOL            fEnabled = FALSE;
    unsigned char   bFeatureState;
    unsigned char   bFeatureIndex;


    // The first uncompleted OOB step's first screen should come up if is in OOB mode.
/*
    if (fnIsInOOBMode() == TRUE) 
    {
        usNextScreen = fnOOBHandleUnexpectedQuit();
        return usNextScreen;
    }
*/

    // meter locked screen should come up firstly if meter lock is enabled
//    if (fMeterLocked == TRUE)
//    {
//        usNextScreen = GetScreen(SCREEN_METER_LOCKED);
//        return usNextScreen;
//    }


	if (fCMOSAbacusAcntProblem == TRUE)
	{
        usNextScreen = GetScreen(SCREEN_ABACUS_ACNT_DISABLED);
        return usNextScreen;
	}
	
   /** added by Kan to check if we need to change next 
    screen based on the result of setting normal preset **/
/*
   if( fnGetRestorePresetFlag() == TRUE )
   {
       usNextScreen = fnGetPresetNextScr();
       fnClearRestorePresetFlag();

       if (usNextScreen != 0) 
       {
         return usNextScreen;
       }
   }
*/

    /** added by Kan for confirm Auto Date Advance message **/
    if (fnGetAutoDateAdvConfirmFlag() == TRUE )
    {
        usNextScreen= GetScreen(AUTO_DATEADVANCE_CONFIRM);
        return usNextScreen;
    }

    // clear all rating info if we are not in key in postage mode but a MPE fee is selected.
    if (fnFlashGetByteParm(BP_KEY_IN_POSTAGE_MODE) == KIP_FROM_FEE_LIST_ONLY)
    {
        if (fnOITGetPrintMode() != PMODE_MANUAL)
        {
/*
            if (fnCheckSwissMPERatingStatus() == TRUE)
            {
                fnOIClearRatingInfo(); // clear postage and all rating info
            }
*/
        }
    }

    /* power failure report */
    if ( fnFlashGetByteParm(BP_DCAP_INDICIA_PWR_FAIL_REPORT) )
    {
        if (fnIsIncidentReportNeeded() == TRUE) 
        {
            fnPostDisabledStatus(DCOND2_RPT_PWR_FAILURE);
            
            /* Prepare the report and go */
//            fnReportPowerInterruption( SCREEN_MM_INDICIA_READY );
            usNextScreen = GetScreen(SCREEN_REPORT_READY);

            // If we were in differential mode, we need to go to the 
            // MM_DIFF_REMOVE_ONEPIECE screen after the print failure report
            // has been printed: to do so, set fDiffWeighPieceNeeded to TRUE
            if(fnOITGetPrintMode() == PMODE_DIFFERENTIAL_WEIGH)
            {
              // now we need the piece again.
              fDiffWeighPieceNeeded = TRUE;
            }

            goto xit;
        }
    }

    /* meter has been moved */
    if (fOITMeterMoved == TRUE)
    {
        // We clear the flag fOITMeterMoved in the post function of this screen.
        usNextScreen = GetScreen(SCREEN_METER_MOVED);
        goto xit;
    }

    // check for insufficient funds
    if (fnGetDebitValue(&ulPostageValue) != TRUE || fnBobGetDescendingReg(pDescRegValue) != TRUE)
    {
        goto xit;
    }

    if ((double) (ulPostageValue) > fnBinFive2Double(pDescRegValue))
    {
        fnPostDisabledStatus(DCOND_INSUFFICIENT_FUNDS);
        fnDumpStringToSystemLog( "InsuffFunds set flag in fnIndiciaReadyConditionsCheck" );
        fnBobErrorInfo(&bBobErrorClass, &bBobErrorCode);
    }

    if ((double)(fnCMOSSetupGetCMOSSetupParams()->lwLowFundsWarning) >= fnBinFive2Double(pDescRegValue))
    {
        fLowFundsWarningPending = TRUE;
        fnDumpStringToSystemLog( "LowFundsWarn set flag in fnIndiciaReadyConditionsCheck" );
    }

 //     We have to use BOB public function, we can't use BOB interal function fnCheckPCEndOfLife(0) here 
//      avoid set bobErrFlag.  - David
        uiEolStat= fnCheckPieceCountEndOfLife();
        if ( PSD_EOL_LOCKOUT == uiEolStat)
        {
          fnPostDisabledStatus(DCOND2_PIECE_COUNT_EOL);
        }
        else if ( PSD_EOL_WARNING == uiEolStat)
        {
          fLowPieceCountWarningPending = TRUE;
        }

    // After all BOB related operation, set the event flag so CM can debit.
    OSSetEvents(OIT_EVENT_GROUP, (unsigned long) MAIN_MENU_CHK_END_EVT, OS_OR);

    // Restore to previous print mode if the flow of rating the mail on scale invoked by key operation 
    // "Weigh/Rate-> Rate the Mail on Scale" doesn't complete.
/*
    if( fnIsNeedRestoreFromPlatmodeFlag() == TRUE )
    {
        UINT8   bPreMode;
 //       bPreMode = fnGetPreviousModeBeforeScale();
        fnOITSetPrintMode(bPreMode);
        
        if(bPreMode == PMODE_MANUAL_WGT_ENTRY 
            || bPreMode == PMODE_AJOUT_MANUAL_WGT_ENTRY)
        {
//            fnStoreManualWeight(pOldManualWeight);
        }
        
        fnSetNeedRestoreFromPlatmodeFlag( FALSE );
    }
*/

    /* the mode check should after the PSD check especially for the BOB_STATIC_IMAGE_TOO_MANY_COLUMNS error. */       
    /* We are going to different ready to print screen for different print mode, especially the differential
     * weighing mode, therefore the continuation function of the main menu check might not be involved, so
     * most of above vital errors need to get the screen id and go to the screen immediately. -Victor 24/01/05 */
    bPrintMode = fnOITGetPrintMode();                   // Get printmode

    fnSetAlterNativeMode(bPrintMode);
    switch (bPrintMode)
    {
        case PMODE_PERMIT:
            (void)IsFeatureEnabled(&fEnabled, PERMIT_MODE_SUPPORT, &bFeatureState, &bFeatureIndex);	
            if( fEnabled == TRUE )
            {
            usNextScreen = GetScreen(SCREEN_PERMIT_READY);
            goto xit;
            }
            else
                fCurrentModeDisable = TRUE; // means the current OI print mode is feature disabled.
            break;

        // goto seal only screen if we are in seal only mode. 
        case PMODE_SEAL_ONLY:
            usNextScreen = GetScreen(SCREEN_SEAL_ONLY_READY);
            goto xit;

        // goto ad only screen if we are in Ad only mode 
        case PMODE_AD_ONLY:
//            usNextScreen = GetScreen(SCREEN_AD_ONLY_READY);
//            goto xit;

        case PMODE_TIME_STAMP:
//            usNextScreen = GetScreen(SCREEN_DATE_TIME_ONLY_READY);
//            goto xit;

        case PMODE_AD_TIME_STAMP:
//            if( fnCheckTextAdDateTimePrintingMode() )
//            {
//            usNextScreen = GetScreen(SCREEN_AD_DATE_TIME_ONLY_READY);
//            goto xit;
//            }
//            else
//                fCurrentModeDisable = TRUE; // means the current OI print mode is feature disabled.
            break;

 //Added below to support Canada printing mode
        case PMODE_DATE_CORRECTION:
//            fnClearPostage();       //postage should be zero in date correction mode
//            fnRateSetNonRateMode();
            usNextScreen = GetScreen(SCREEN_DATE_CORRECTION_READY);
            goto xit;

        case PMODE_POSTAGE_CORRECTION:   
            if(fnFlashGetByteParm(BP_ADD_POSTAGE_MODE ) == ADD_POSTAGE_ALLOWED_FENCINGLIMIT)          
            {
                if((fnCheckPostCorrectionVal((UINT32)ulPostageValue) == PosInValid)||((UINT32)ulPostageValue == 0))
                {   
                    usNextScreen = GetScreen(SCREEN_ENTER_POSTAGE_CORRECTION);
                }
                else
                {
//                    fnRateSetNonRateMode();
                    usNextScreen = GetScreen(SCREEN_POSTAGE_CORRECTION_READY);
                }
            }
            goto xit;
            
        case PMODE_MANIFEST:
            if(fnCheckManifestAmount((UINT32)ulPostageValue)==TRUE)
            {
//                fnRateSetNonRateMode();
                usNextScreen = GetScreen(SCREEN_MANIFEST_READY);
            }
            else
            {         
                usNextScreen = GetScreen(SCREEN_MANIFEST_ENTER_AMOUNT);
            }
            goto xit;

     case PMODE_DIFFERENTIAL_WEIGH:
            (void)IsFeatureEnabled(&fEnabled, DIFF_WEIGHING_SUPPORT, &bFeatureState, &bFeatureIndex);
            if( fEnabled == TRUE )
            {
		        if ( fDiffWeighPieceNeeded ) 
		        {
		            // if it is diff weighing mode and piece weight needed, jump to the diff
		            //  weighing screens.
/*		            if (fnCheckPlatformDiffWeight() == FALSE)
		            {
		                usNextScreen = GetScreen( MM_DIFF_REMOVE_ONEPIECE );
		            }
		            else
		            {
		                usNextScreen = GetScreen( MM_DIFF_WEIGHT_ZERO );
		            }*/
		            fDiffWeighPieceNeeded = FALSE;

		            goto xit;
		        }
            }
            else
                fCurrentModeDisable = TRUE;  // means the current OI print mode is feature disabled.
        break;

        default: // lint requirement
            if( bPrintMode == PMODE_MANUAL_WGT_ENTRY )
            {
                (void)IsFeatureEnabled(&fEnabled, MANUAL_WEIGHT_ENTRY_SUPPORT, &bFeatureState, &bFeatureIndex);
                if( fEnabled == FALSE )
                    fCurrentModeDisable = TRUE;  // means the current OI print mode is feature disabled.
            }
            break;
    }   // switch ()

    if( fCurrentModeDisable == TRUE)
    {
        (void)fnModeSetDefault();
        if( bPrintMode== fnOITGetPrintMode()) // means we are in a dead loop, faul to change default mode
        {
            fnReportMeterError(ERROR_CLASS_OIT, ECOIT_CANNOT_SET_DEFAULT_MODE);
            goto xit;
        }
		
        //  Force to re-enter the main menu again to go through the pre function
        usNextScreen = GetScreen(SCREEN_MM_INDICIA_READY);
        goto xit;
    }
    
	if(bPrintMode != PMODE_AD_ONLY  ){
		// Check PSD
		if((usNextScreen = fnCheckPSDState()) != 0)
		{
			return usNextScreen;
		}

		// Check PSD/UIC battery state.
		if((usNextScreen = fnCheckBatteryState()) != 0)
		{
			return usNextScreen;
		}

		if((usNextScreen = fnCheckInspection()) != 0)
		{
			return usNextScreen;
		}
	}
    /* check for missing graphics */
    if((usNextScreen = fnCheckMissingGraphics()) != 0)
    {
        return usNextScreen;
    }

//**********************************************************************************
//  Check Accounting 
//**********************************************************************************

    
//AdamTODO: porting mega code, need to uncomment after merged with operator
    if( (usNextScreen = fnOICheck4AbacusErrors()) != 0 )
    {
        return usNextScreen;
    }


/*
    if (fnGetAccountDisabledStatus() != ACT_SUCCESS)
    {
        fnPostDisabledStatus(DCOND_ACCOUNT_PROBLEM);
    }
*/

/*    if ( fnGetAccountingType() == (UINT8) ACT_SYS_TYPE_ABACUS ||
          fnGetAccountingType() == (UINT8) ACT_SYS_TYPE_HOSTED_ABACUS)
    {
        (void)fnAXC_CheckMidnightAlarm( NULL_PTR );

        if(fnAAXMDoesXLogFullWarningExceeded() == TRUE)
        {
            fnPostWarningStatus(WCOND_TRANS_LOG_NEAR_FULL);
        }
                
        if( fnGetAccountingType() == (UINT8) ACT_SYS_TYPE_HOSTED_ABACUS)
        {
            if(IsHostConnected() != 0)
            {
                fnClearWarningStatus( WCOND_ABACUS_HOST_DISCONNECTED );
            }
            else
            {
                fnPostWarningStatus( WCOND_ABACUS_HOST_DISCONNECTED );
            }       
        }

 M_Abacus_Raymond: Uncomment it after scanner feature is ready.
        if (fAbacusJobListFull == TRUE)
        {
                lwWarningConditions |= WCOND_ABACUS_JOB_LIST_FULL;
        }

    }*/


    /* check the finialized franking transaction status */
/*Vincent
    if (fnFlashGetByteParm(BP_PBP_PERFORM_FINALIZED_FRANKING)
            && fnPBPWasFFRecXferInterrupted())
    {
        lwDisablingConditions2 |= DCOND2_FFXFER_INTERRUPTED;
    }
*/

        // Reminder to scrap the label if neccessary.
/*
        if(fnRateCheckCountryId(CANADA) == TRUE && fnGetLabelPrintedFlag() ==TRUE )
        {
            usNextScreen = GetScreen(ESERVICE_SCRAP_LABEL_REMINDER);
            goto xit;
        }
*/

                                     // Reset CheckAutoDelete Request 
    // else - We need to Figure out what we want to do here with an iError!!


//**********************************************************************************************************
//* Evaluate current selection for trackable concerns end */

//****************************************************************************************************************
//  Check for KIP Carrier mode
//****************************************************************************************************************
//-------------------------- BP_KEY_IN_POSTAGE_MODE 0x05 ----------------------------
// #define KIP_NOT_ALLOWED              0   //   Key in Postage not allowed 
// #define KIP_ALLOWED                  1   //   Key in Postage allowed no restrictions
// #define KIP_ALLOWED_PASSWORD     2   //   Key in Postage allowed password required
// #define KIP_ALLOWED_VALIDATION       3   //   Key in Postage allowed postage amt requires validation
//
//------------------------- BP_KEY_IN_POSTAGE_CLASS 0x14 ----------------------------
// #define KIP_CLASS_NOT_ALLOWED        0  //   Key in Postage Class not allowed 
// #define KIP_CLASS_REQUIRED           1  //   Key in Postage Class Required
// #define KIP_CLASS_REQ_ENTRY_LIMITED  2  //   KIP Class Required no entry from enter amount screen                                    
// 
//****************************************************************************************************************

    bPrintMode = fnOITGetPrintMode();                  // Get printmode

/*    if (bKIPCarType != KIP_CLASS_NOT_ALLOWED)               // KIP Carrier ?
    {
        fKIPLastScreenClass = FALSE;                        // Make sure flag is always clear 
        // **************************************************************************************************
        //  Carrier just changed
        // **************************************************************************************************
        if (fnKIPRateCarrierChanged() == TRUE)                      // Rating changed ?
        {
            fKIPCarrierChanged = FALSE;                 // Clear changed flag

            if (fnRateIsCurrentCarrierBranded()== RSSTS_CARRIER_KIP_CARRIER)             // Carrier KIP Carrier ?
            {
                if (bPrintMode != PMODE_MANUAL)             // Pmode = KIP mode ?
                {
                    //fnSaveNonKIPMode();
                    fnOITSetPrintMode(PMODE_MANUAL);
                }
                if (bKIPCarType == KIP_CLASS_REQ_ENTRY_LIMITED)     // Check EMD Param 
                {
                    fKIPLastScreenClass = TRUE;         // 
                    usNextScreen = GetScreen(SCREEN_ENTER_POSTAGE);     // goto KIPEnterAmount
                    goto xit;
                }
                else
                {
                    // FIXME: maybe need to reset the postage to zero for kip class requried condition.
                }
            }
            else                                // Carrier not KIP Carrier
            {
                if (bPrintMode == PMODE_MANUAL)             // Pmode = KIP mode ?
                {
                    //fnRestoreNonKIPMode();            // Exit Kip Mode, Restore Pmode.
                                        // goto to attach scale or manual weight entry mode

                    if(fnCheckSwissMPERatingStatus() == FALSE)
                    {
                    if ( fnPlatIsAttached() )
                    {
                        fnOITSetPrintMode( PMODE_PLATFORM );
                    }
                    else
                    {
                        fnOITSetPrintMode( PMODE_MANUAL_WGT_ENTRY );
                    }
                }
            }
        }
        }
        //**************************************************************************************************
        //  Carrier did not change
        //**************************************************************************************************
#if 0
        else     // Rating not changed ?
        {
            if (fnRateIsCurrentCarrierBranded()==TRUE)          // Carrier KIP Carrier ?
            {
                if (bCometPrintMode != PMODE_MANUAL)            // Carrier = KIP, Pmode =/= Kip Mode
// XXX need to change here
//                  fnRestoreNonKIPRating();            // Exit Kip Mode, Restore Rating.
                    fnRateClearAllRatingInfo();         // Exit Kip mode clear all rating 

            }
            else    // Carrier not KIP Carrier
            {
                if (bCometPrintMode == PMODE_MANUAL)            // Carrier =/= KIP, Pmode = KIP mode
                {
                    //fnRestoreNonKIPMode();            // Exit Kip Mode, Restore Pmode.

                                            // goto to attach scale or manual weight entry mode
// XXX need to change here
//                  fnSaveNonKIPRating();               // save non KIP Print mode.
// set carrier KIPcarrier.
                }
            }
        }    // end Rating not changed ?
#endif
    }*/

    if ( fnKipPasswordRequired() )
    {
        fnPostDisabledStatus(DCOND2_KIP_PWD_REQUIRED);
    }

     /* we don't want a disabling condition if SWiss-style Zero Print is valid */
//    if ( fnCheckForZeroPrint() && fnGetZeroPostage() )
//     {
//        /* if guard is true, next guard will be skipped */
//     }
//    // check for class selection, if we are in weighing mode (platform mode, manual weight entry mode or diff wgt. mode)
//    // or key in postage mode but a class is requried for the key in postage, set the disabling condition.
//    else if (fnInWeighingMode()
//          || ((bPrintMode == PMODE_MANUAL)
//          && (fnFlashGetByteParm(BP_KEY_IN_POSTAGE_CLASS)
//              != KIP_CLASS_NOT_ALLOWED)))
//    {
//       if (fnRateIsClassSelected() != (SINT16) RSSTS_CLASS_SELECTED)
//       {
//           fnPostDisabledStatus(DCOND_NEED_CLASS);
//       }
//    }

    //check Reply code 
/*
    if (fnIsReplyMailInUse((SINT8*) NULL) == TRUE)
    {

        if ((fnReplyPostcodeValid()!= SUCCESSFUL) || (fnGetReplyCodePurpose() == REPLY_CODE_PURPOSE_PRESET))
        {
            usNextScreen = GetScreen(RATE_REPLY_ZIP_ENTRY_SCN) ;
            goto xit;
        }

    }
*/
/*    else
    {
        fnClearCurrentReplyPostCode();       
    } */

//****************************************************************************************************************
//* if we are in a platform or manual weight entry mode, check that the current weight is 
//      valid for the rating settings. */
//****************************************************************************************************************
    if ((fForceReRate)
            && (bPrintMode == PMODE_PLATFORM))
    {
        fForceReRate = false;
        if (fnCMOSGetNewPieceACClassOption() == CLEAR_RATING_DATA &&
                usNextScreen == GetScreen(RATE_SELECT_CARRIER))
        {
            /* We're forcing the user to the rate selection screen because a
               re-rate is required and rating data will not be retained.  We've
               cleared the fForceReRate flag so the user isn't continuously
               prompted for a rate but we need to ensure that we update the
               postage value if the user cancels out of the rate selection
               screen.  Setting fMayNeedReRate to TRUE achieves that. */
//            fMayNeedRerate = TRUE;
        }
        //goto xit ;
    }
//**********************************************************************************
//  Check WOW & W1st Piece
//********************************************************************************** 
    if ((fForceReRate)
                && ((bPrintMode == PMODE_WOW)  || (bPrintMode == PMODE_WEIGH_1ST)))
    {
        SINT8   chRateStatus;

//        chRateStatus = fnRateWOWWeight(&usNextScreen);
        fForceReRate = FALSE;
/*
        if(chRateStatus != (UINT8) RSTATUS_OK)
        {
//            fMayNeedRerate = TRUE;
        }
        else if(fNeedStartMailRun == TRUE)
        {
            usNextScreen = fnStartToPrintMail (0, 0, 0, TRUE, FALSE);
            fNeedStartMailRun = FALSE;
        }
*/
        goto xit;    
    }
                
    // Check ZWZP
/*
    if (fnRateCheckZWZPDisablingCondition() == TRUE)
    {
        // zero the postage
        fnZeroPostageForZeroWeight();
    
        fnPostDisabledStatus(DCOND_WEIGHT_PROBLEM); // weight error code WEIGHT_PROB_ZERO
    }
    // Check for manual weight + 0 weight
    else if(fnRateCheckZWManWgt() == TRUE)
    {
        // if "Select Class" is shown, don't show "Postage Value Too Low"
        if(fnIsDisabledStatusSet(DCOND_NEED_CLASS) == TRUE)
        {
            ;
        }
        else
        {
            fnPostDisabledStatus(DCOND_ZERO_MANUAL_WEIGHT); // weight error code WEIGHT_PROB_ZERO
        }
    }
*/

    if (bPrintMode == PMODE_PLATFORM && fnIsAutoScaleModeEnabled() == TRUE )
    {
        // For Power up case, don't check this unstable case since the weight is often not 
        // stable when loading GTS normal preset.In this case, we don't want to display "Place
        // item on scale"
        if(fCheckUnStableWeigh == FALSE)
        {
            fCheckUnStableWeigh = TRUE;
        }
        else
        {
            /* The scale is unstable or has a PLAT_MOTION_TIMEOUT status.  The
               latter can occur if you download a lot of graphics from DLA.  While
               installing the graphics the meter will not respond to the scale and
               it will time out.  It is important that we disable the meter in that
               case.  Putting an item on the scale (or removing one) will put the
               scale back to normal. */
            fnPostDisabledStatus(DCOND_WEIGHT_PROBLEM); // weight error code WEIGHT_PROB_ZERO
        }
    }
    else if (bPrintMode == PMODE_DIFFERENTIAL_WEIGH)
    {
        UINT8 pBCDWeight[LENWEIGHT];
        UINT8 bPlatformStatus;
//        fnGetPlatformWeight(pBCDWeight, &bPlatformStatus, TRUE);
    
        if (bPlatformStatus == PLAT_MOTION_TIMEOUT)
            /* See comment above.  In differential weighing mode all hell
               breaks loose if the scale times out.  Redirect to put all mail
               on scale screen */
            usNextScreen = GetScreen(MM_DIFF_WEIGHT_ZERO);
    }
    else if  (bPrintMode == PMODE_MANUAL && bKIPCarType == KIP_CLASS_NOT_ALLOWED)
    {
		// reset the rating system status to initial state (for Fraca 222452)
		// but only if you aren't using HRT strings for KIP (for Brazil Fraca 229158).
/*
		if (fnFlashGetKIPModeControlVal() != KIP_ALLOWED_HRT_STRINGS)
	        (void)fnRateUnSelectClass();
*/
		
		// go fetch indicia info stuff again. Clear Key always clears this, so must be retrieved. Fix fraca 135466 wherein 2D
        // barcode had bad rate category because indicia strings were cleared.
	    // added condition bKIPCarType == KIP_CLASS_NOT_ALLOWED because if the class is required, KIP is not a non rate mode
	    // and going to non rate mode makes us lose information (Added by Ivan on 08/13/2008)
//        (void)fnRateSetNonRateMode();
    }

   // Fix for fraca148331
   // Check if class is selected
/*
   if (fnRateIsClassSelected() == (SINT16) RSSTS_CLASS_SELECTED)
   {
      // make sure INV_WEIGHT occurred
      if(fnGetRMInvalidWeightStatus() == TRUE)
      {
          // clear the class to avoid weight/class discrepancy          
          fnRateUnSelectClass();          
          fnPostDisabledStatus(DCOND_NEED_CLASS);
          // clear the flag
          fnSetRMInvalidWeightStatus(FALSE);
      }
   }
*/
    
xit: 
    return usNextScreen;            
    #undef COMING_FROM_LOGFULL
}

/* *************************************************************************
// FUNCTION NAME:               fnIndiciaReadyConditionsCheck2
// DESCRIPTION: 
//      Private function to check warning & disabling conditions after 
//      requesting CM status 
//
// INPUTS:      None
// RETURNS:     Next screen
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  21-Jan-15  Jane Li          Before going to Abacus ready screen, close the batch 
//                              when KIP value is changed 
//  02-May-14  S. Peterson		Undid Wang Biao's change for fraca 225110 because it
//                              wasn't what I asked CieNET to do for the fraca.
//  2014.04.29 Wang Biao        Updated codes to fix fraca 225110 to display the screen 
//                              'Data upload required' before 'Rates have expired'.  
//  2012.10.15 Clarisa Bellamy - Before going to the MMIndiciaReadyAbacus screen, 
//                      check to see if there is a update-from-host pending condition,
//                      and if so, go to the AbacusHostUpdateProgress screen instead. 
//  09/21/2009  Bonnie Xu       Add the case for PORTUGAL
//  09/21/2009  Raymond Shen    Fixed the problem that it can't navigate to the 
//                              Mode Ajout Ready screen when Abacus Accounting is actived.
//  07/15/2009  Bonnie Xu       Add the case for AUSTRIA
//  06/23/2009  Jingwei,Li      Added "Paper in Feeder transport" case.
//  03/10/2007  Oscar Wang      Added End of Life condition check.
//  12/03/2007  Oscar Wang      Renamed ESERVICE_UPLOAD_RECORD_PROMPT to 
//                              ESERVICE_HOME_SCRN_UPLOAD_PROMPT. 
//  08/09/2007  Oscar Wang      Update for 0 manual weight check.
//  08/06/2007  Oscar Wang      Modified for ENTER DUNS number prompt screen.
//  06/19/2007  D. Kohl         Add treatment for piece count end of life
//                              error
//  06/04/2007  Oscar Wang      Fix FRACA GMSE00121355. Go to disable 
//                              condition if DUNS number is not entered.
//  04/13/2007  Oscar Wang      Update for DUNS number check
//  03/22/2007  Adam Liu        Update for Abacus home check
//  12/15/2006  Adam Liu        Update for "Update Required" check
//  12/15/2006  Oscar Wang      Update for "Need class" check.
//  12/14/2006  Adam Liu        Update for adding DCAP check
//  12/12/2006  Oscar Wang      Check if KIP password is required.
//  11/15/2006  Oscar Wang      Fix FRACA GMSE00102800 by checking high value
//                              warning prior to other disable conditions.
//  09/08/2006  Oscar Wang      Added new rate effective condition check.
//  09/04/2006  Raymond Shen    Added scale location code condition check
//                              to fix fraca 103460.
//  07/20/2006  Vincent Yi      Added checking disabling condition "Meter Not
//                              Authorized"
//  07/18/2006  Dicky Sun       Update for GTS adding record workflow
//  07/07/2006  Dicky Sun       Add GTS support
//  11/09/2005  Vincent Yi      Initial version
//
// *************************************************************************/
// VBL_TBD_FP
T_SCREEN_ID fnIndiciaReadyConditionsCheck2( void )
{
    T_SCREEN_ID usNextScreen = 0;
    long DcStatus = 0;      //Initialize the value.
    UINT16  usDetailAbacusStatus;
    SINT16  sAbacusStatus;
    UINT8   bTmpVal;
    UINT16  wCountryCode = fnGetDefaultRatingCountryCode();
    UINT32      ulPostageValue;


    if((usNextScreen = fnPostCheckInspection()) != 0)
    {
        return usNextScreen;
    }

    if((usNextScreen = fnPostCheckEndOfLife()) != 0)
    {
        return usNextScreen;
    }


    fnUpdateDisablingWarningConditions();

    if ((usNextScreen = fnPostCheckCMConditions()) != 0)
    {
        return usNextScreen;
    }
    if ((usNextScreen = fnCheckPSOCDisablingConditions()) != 0)
    {
        return usNextScreen;
    }

    /* the priority of the various disabling and other conditions is established by the order of
        the processing below */

    /* if necessary, show the screen that tells the user the time has been updated for
        daylight savings time (either ending or beginning)   DST Change  */

    if (fCMOSDSTAutoUpdated)
    {
        usNextScreen = GetScreen(DST_AUTO_CHANGE_OCCURRED_SCRN);        
        return usNextScreen;
    }

    /* set the scale location code if the code is pending */
    if (fSetScaleLocCodePending == TRUE)
    {
        usNextScreen = GetScreen(SCREEN_SET_SCALE_CODE);
        return usNextScreen;
    }

    /* The DCAPI may need to write to flash. */
    //if (lwDisablingConditions & DCOND_FLASH_WRITE_REQ)
    if (fnIsDisabledStatusSet(DCOND_FLASH_WRITE_REQ) == TRUE) 
    {
        usNextScreen = GetScreen(SCREEN_DCAP_BACKUP);
        return usNextScreen;
    }
    
    /* Data capture upload is required or finialized franking transaction was interrupted. */
    //if ((lwDisablingConditions & (DCOND_DCAP_AUTO_UPLOAD_REQ | DCOND_DCAP_MANUAL_UPLOAD_REQ))
    //        || (lwDisablingConditions2 & DCOND2_FFXFER_INTERRUPTED))
    if (((fnIsDisabledStatusSet(DCOND_DCAP_AUTO_UPLOAD_REQ) == TRUE) || 
 //       (fnIsDisabledStatusSet(DCOND_DCAP_MANUAL_UPLOAD_REQ) == TRUE) ||
        (fnIsDisabledStatusSet(DCOND2_FFXFER_INTERRUPTED) == TRUE)) 
        && (fDCAPUploadReqShown == FALSE))
    {
        usNextScreen = GetScreen(SCREEN_DCAP_UPLOAD_REQ);
        fDCAPUploadReqShown = TRUE;
        return usNextScreen;
    }

    /* Duns not needed - so make the state idle */
    fnSetAutoDunsState( AUTO_DUNS_IDLE );
    
    /* A data capture upload is due, if the warning hasn't been shown, show it now */
    //if ((lwWarningConditions & DCOND_DCAP_UPLOAD_DUE)
    //        && (fDCAPUploadDueShown == FALSE))
    if ((fnIsWarningStatusSet(WCOND_DCAP_UPLOAD_DUE) == TRUE) 
        && (fDCAPUploadDueShown == FALSE))     
    {
        usNextScreen = GetScreen(SCREEN_DCAP_UPLOAD_DUE);
        fDCAPUploadDueShown = TRUE;
        return usNextScreen;
    }

    // if high value postage needs to be confirmed, go to the confirmation screen
    if (fHighPostagePending == TRUE)
    {
        if (fnOITGetPrintMode() == PMODE_MANUAL)
        {   // The High postage warning only prompt when in KIP mode
            usNextScreen = GetScreen(SCREEN_HIGHPOSTAGE_EXCEEDED);
            return usNextScreen;
        }
    }

    // Errors to be displayed in Home screen should be checked here
    if ((fnIsDisabledStatusSet(DCOND_INSUFFICIENT_FUNDS) == TRUE)  ||    
        (fnIsDisabledStatusSet(DCOND_INK_OUT) == TRUE)             ||    
        (fnIsDisabledStatusSet(DCOND_ACCESSING_PM_NVM) == TRUE)    ||    
        (fnIsDisabledStatusSet(DCOND_INKTANK_ERROR) == TRUE)       ||    
        (fnIsDisabledStatusSet(DCOND_WASTE_TANK_FULL) == TRUE)     ||    
        (fnIsDisabledStatusSet(DCOND_PRINTHEAD_ERROR) == TRUE)     ||    
        (fnIsDisabledStatusSet(DCOND_PAPER_IN_TRANSPORT) == TRUE)  ||        
        (fnIsDisabledStatusSet(DCOND_TRANSPORT_OPEN) == TRUE)      ||
//RAM added
        (fnIsDisabledStatusSet(DCOND_INSPECTION_REQUIRED) == TRUE) ||
        (fnIsDisabledStatusSet(DCOND_DCAP_AUTO_UPLOAD_REQ) == TRUE) ||
//RAM added
//        (fnIsDisabledStatusSet(DCOND_DCAP_MANUAL_UPLOAD_REQ) == TRUE) ||
        (fnIsDisabledStatusSet(DCOND_OUT_OF_SERVICE) == TRUE)      ||
        (fnIsDisabledStatusSet(DCOND_AUTHORIZATION_REQ) == TRUE)   ||
        (fnIsDisabledStatusSet(DCOND_PRINTER_FAULT) == TRUE)       ||
        (fnIsDisabledStatusSet(DCOND_ZIP_BARCODE_REQUIRED) == TRUE) || //Dicky adds here on 6/28/2006 for GTS
        (fnIsDisabledStatusSet(DCOND2_FEEDER_COVER_OPEN) == TRUE)   ||
        (fnIsDisabledStatusSet(DCOND_AGD_SETUP_FAILED) == TRUE)     ||
//RAM added
//        (fnIsDisabledStatusSet(DCOND2_INFRA_UPDATE_REQUIRED) == TRUE) ||
        (fnIsDisabledStatusSet(DCOND2_PIECE_COUNT_EOL) == TRUE) ||
        (fnIsDisabledStatusSet(DCOND_ENTER_DUNS_NUMBER) == TRUE) ||
        (fnIsDisabledStatusSet(DCOND_ZERO_MANUAL_WEIGHT) == TRUE) ||
        (fnIsDisabledStatusSet(DCOND_PAPER_IN_WOW_TRANSPORT) == TRUE) ||
        (fnIsDisabledStatusSet(DCOND_PAPER_ERROR) == TRUE))
    {
        usNextScreen = GetScreen (SCREEN_MM_INDICIA_ERROR);
        return usNextScreen;
    }
     
    if ((fnIsDisabledStatusSet(DCOND_TC_REQUIRED) == TRUE)  || 
        (fnIsDisabledStatusSet(DCOND_AD_REQUIRED) == TRUE)  || 
        (fnIsDisabledStatusSet(DCOND_RPT_REQUIRED) == TRUE)  || 
        (fnIsDisabledStatusSet(DCOND_GRAPHIC_REQUIRED) == TRUE))
    {
        if (fnGetDisplayMissingGraphicError())
        {
            usNextScreen = GetScreen(SCREEN_MISSING_GRAPHIC);
            return usNextScreen;
        }
        else
        {
            usNextScreen = GetScreen (SCREEN_MM_INDICIA_ERROR);
            return usNextScreen;
        }
    }

    /* errors that can be cleared by changing a setting */
    if (fnIsDisabledStatusSet(DCOND2_KIP_PWD_REQUIRED) == TRUE)
    {
        usNextScreen = GetScreen(KIP_CHECK_PASSWORD);
        return usNextScreen;
    }

    // check if an invalid USB platform is attached.
    if (fWrongUsbPlatform == TRUE)
    {
        fWrongUsbPlatform = FALSE;
        usNextScreen = GetScreen(SCREEN_WRONG_USB_PLATFORM);
        return usNextScreen;
    }

    if (fnIsDisabledStatusSet(DCOND_ACCOUNT_PROBLEM) == TRUE)
    {
        // Needs account
        usNextScreen = GetScreen (SCREEN_MM_INDICIA_ERROR);
        return usNextScreen;
    }  

    /* a class is required */
    // Adam merged this part from K700 for 3rd DCAP
    //if (lwDisablingConditions & DCOND_DCAP_NEED_CLASS)
    if (fnIsDisabledStatusSet(DCOND_DCAP_NEED_CLASS) == TRUE)         
    {
//        fnRateGetBucketStatus(&DcStatus);
        if(DcStatus !=0)
        {
            
            usNextScreen = GetScreen(RATE_ERR_DATA_CAPTURE_ENGINE);
        }
        else
        {
            //goto home error screen "need class" status 
            usNextScreen = GetScreen(SCREEN_MM_INDICIA_ERROR);  
        }
        return usNextScreen;
    }

    //Check if class is needed.
    if (fnIsDisabledStatusSet(DCOND_NEED_CLASS) == TRUE)         
    {
        //goto home error screen "need class" status 
        usNextScreen = GetScreen(SCREEN_MM_INDICIA_ERROR);  
        return usNextScreen;
    }

    /* zero weight zero postage disalbing condition */
    if(fnIsDisabledStatusSet(DCOND_WEIGHT_PROBLEM) == TRUE) // WEIGHT_PROB_ZERO
    {
        usNextScreen = GetScreen(SCREEN_MM_INDICIA_ERROR);
        return usNextScreen;
    }

    /* warnings */
//    sAbacusStatus = fnAGDGetAbacusStatus(&usDetailAbacusStatus);

    /* go to the tape resume version of the main screen if there are tapes pending.  this
        supercedes any warnings which are shown on the printing screen anyway. */

    //AdamTODO: uncomment this after determining how to handle tape resume
    /*
    if (fnOITGetTapesPending() > 1)
    {
        if ((sAbacusStatus == ABACUS_IN_USE) || 
            (fnGetAccountingType() == (UINT8) ACT_SYS_TYPE_ACCUTRAC ||
             fnGetAccountingType() == (UINT8) ACT_SYS_TYPE_METERNET ))
        {
            usNextScreen = GetScreen(MM_INDICIA_TAPE_RESUME_RDY_AB);
        }
        else
        {
            usNextScreen = GetScreen(MM_INDICIA_TAPE_RESUME_RDY);
        }
        return usNextScreen;
    }
    */
	

    return (usNextScreen);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fncSealOnlyCheck
//
// DESCRIPTION: 
//      Continuation function that continues the Seal Only ready checking after
//      requesting CM status.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr, depends on the conditions checking
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/06/2005  Vincent Yi      Initial version 
//
// *************************************************************************/
static SINT8 fncSealOnlyCheck( void         (** pContinuationFcn)(), 
                               UINT32       *   pMsgsAwaited,
                               T_SCREEN_ID  *   pNextScr )
{
    SINT8 ucRetval = PREPOST_COMPLETE;

    *pNextScr = 0;

    *pNextScr = fnSealOnlyConditionsCheck2();

    if (*pNextScr == 0)
    {
        // Check all warning conditions for displaying the warning messages and
        // counter at the first time
        fnUpdateWarningMessages(TRUE);
    }
    else if (*pNextScr == GetScreen (SCREEN_SEAL_ONLY_ERROR))
    {   
        // Check all error conditions for displaying the error messages and
        // counter at the first time
        fnUpdateErrorMessages(TRUE);
    }

    SET_MENU_LED_OFF();
    return ucRetval;        
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnSealOnlyConditionsCheck
//
// DESCRIPTION: 
//      Private function to check warning & disabling conditions in 
//      fnpSealOnlyCheck()
//
// INPUTS:
//      None
//
// RETURNS:
//      Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/18/2008  Raymond Shen    Added code to check PSD/CMOS battery life.
//  12/03/2007  Oscar Wang      Renamed ESERVICE_UPLOAD_RECORD_PROMPT to 
//                              ESERVICE_HOME_SCRN_UPLOAD_PROMPT.
//  01/19/2007  Oscar Wang      Fix FRACA GMSE00113167: check unsent EService
//                              record first.
//  12/06/2005  Vincent Yi      Initial version
//
// *************************************************************************/
// VBL_TBD_FP
static T_SCREEN_ID fnSealOnlyConditionsCheck(void)
{
    T_SCREEN_ID usNextScreen = 0;
    
    // Check PSD
    if((usNextScreen = fnCheckPSDState()) != 0)
    {
        return usNextScreen;
    }

    // Check PSD/UIC battery state.
    if((usNextScreen = fnCheckBatteryState()) != 0)
    {
        return usNextScreen;
    }

    /* Check if any Confirmation Service Records are pending */
    if ( fnGetRecordsPendingFlag() )   
    {
        usNextScreen = GetScreen(ESERVICE_HOME_SCRN_UPLOAD_PROMPT);
        return usNextScreen;
    }

    return usNextScreen;            
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnSealOnlyConditionsCheck2
//
// DESCRIPTION: 
//      Private function to check warning & disabling conditions after 
//      requesting CM status when seal only ready
//
// INPUTS:
//      None
//
// RETURNS:
//      Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/15/2006  Adam Liu        Update for "Update Required" check
//  11/27/2006  Kan Jiang       Commented the check of DCOND_OUT_OF_SERVICE error
//                              to fix Fraca110726
//  07/20/2006  Vincent Yi      Added checking disabling conditions "Meter Not
//                              Authorized" and "Out of Service"
//  11/09/2005  Vincent Yi      Initial version
//
// *************************************************************************/
static T_SCREEN_ID fnSealOnlyConditionsCheck2(void)
{
    T_SCREEN_ID usNextScreen = 0;

    fnSealOnlyUpdateConditions();

    if ((fnIsDisabledStatusSet(DCOND_PRINTER_FAULT) == TRUE) &&
        (fPrinterFaultShown == FALSE))
    {   // fatal error, display the error and hang everything.
        fPrinterFaultShown = TRUE;
        return (usNextScreen = GetScreen (SCREEN_PRINTER_FAULT));
    }
    if (fnIsDisabledStatusSet(DCOND_COVER_OPEN) == TRUE)
    {   // top cover open, warning, can be cleared.
        return (usNextScreen = GetScreen (SCREEN_TOP_COVER_OPEN));
    }
    if (fnIsDisabledStatusSet(DCOND_PAPER_ERROR) == TRUE)
    {   // paper error, can be cleared.
        return (usNextScreen = GetScreen (SCREEN_PAPER_ERROR));
    }
    if (fnIsDisabledStatusSet(DCOND2_FEEDER_FAULT) == TRUE)
    {
        return (usNextScreen = GetScreen (SCREEN_FEEDER_FAULT));
    }

    // Errors to be displayed in Home screen should be checked here
    if ((fnIsDisabledStatusSet(DCOND_TRANSPORT_OPEN) == TRUE)     ||
        (fnIsDisabledStatusSet(DCOND_PAPER_IN_TRANSPORT) == TRUE) ||
        (fnIsDisabledStatusSet(DCOND_PRINTER_FAULT) == TRUE)      ||
        //(fnIsDisabledStatusSet(DCOND_OUT_OF_SERVICE) == TRUE)     ||
        (fnIsDisabledStatusSet(DCOND_AUTHORIZATION_REQ) == TRUE)  ||
        (fnIsDisabledStatusSet(DCOND2_FEEDER_COVER_OPEN) == TRUE) ||
        (fnIsDisabledStatusSet(DCOND2_INFRA_UPDATE_REQUIRED) == TRUE))
    {
        usNextScreen = GetScreen (SCREEN_SEAL_ONLY_ERROR);
        return (usNextScreen);
    }
     
    return (usNextScreen);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fncOptionalPrintModeCheck
//
// DESCRIPTION: 
//      Continuation function that continues the Ad Only / Date-Time Only 
//      checking after requesting CM status.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr, depends on the conditions checking
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/18/2007  Adam Liu        Added PMODE_AD_ONLY and PMODE_TIME_STAMP along 
//                              with PMODE_AD_TIME_STAMP to support TextAdDateTime 
//  04/19/2007  Andy Mo         Added cases for postage correction,date correction and manifest
//  12/23/2005  Adam Liu        Initial version 
//
// *************************************************************************/
static SINT8 fncOptionalPrintModeCheck ( void       (** pContinuationFcn)(), 
                                         UINT32         *   pMsgsAwaited,
                                         T_SCREEN_ID    *   pNextScr )
{
    SINT8 ucRetval = PREPOST_COMPLETE;
    UINT8 ucPrintMode;


    *pNextScr = 0;

    *pNextScr = fnOptionalPrintModeConditionsCheck2();
    ucPrintMode = fnOITGetPrintMode();

    switch(ucPrintMode)
    {
    case PMODE_AD_ONLY:
    case PMODE_TIME_STAMP:
    case PMODE_AD_TIME_STAMP:   
        if (*pNextScr == 0)
        {
            // Prepare the menu table
            fnOptionalPrintModeMenuInit(TRUE);
            // switch to correct mode based on text, ad, text selections
            fnUpdateOptionalPrintMode();
            // Check all warning conditions for displaying the warning messages and
            // counter at the first time
            fnUpdateWarningMessages(TRUE);
        }
        else if (*pNextScr == GetScreen (SCREEN_AD_DATE_TIME_ONLY_ERROR))
        {   
            // Prepare the menu table
            fnOptionalPrintModeMenuInit(TRUE);
            // switch to correct mode based on text, ad, text selections
            fnUpdateOptionalPrintMode();
            // Check all error conditions for displaying the error messages and
            // counter at the first time
            fnUpdateErrorMessages(TRUE);
        }
        break;
    case PMODE_POSTAGE_CORRECTION:
        if(*pNextScr == 0) //No error occurs
        {   
            // Check all warning conditions for displaying the warning messages and
            // counter at the first time
            fnUpdateWarningMessages(TRUE);
        }
        else if(*pNextScr == GetScreen (SCREEN_POSTAGE_CORRECTION_ERROR))
        {
            // Check all error conditions for displaying the error messages and
            // counter at the first time
            fnUpdateErrorMessages(TRUE);
        }
        break;
    case PMODE_DATE_CORRECTION:
        if(*pNextScr == 0) //No error occurs
        {   
            // Check all warning conditions for displaying the warning messages and
            // counter at the first time
            fnUpdateWarningMessages(TRUE);
        }
        else if(*pNextScr == GetScreen (SCREEN_DATE_CORRECTION_ERROR))
        {
            // Check all error conditions for displaying the error messages and
            // counter at the first time
            fnUpdateErrorMessages(TRUE);
        }

        break;
    case PMODE_MANIFEST:
        if(*pNextScr == 0) //No error occurs
        {   
            // Check all warning conditions for displaying the warning messages and
            // counter at the first time
            fnUpdateWarningMessages(TRUE);
        }
        else if(*pNextScr == GetScreen (SCREEN_MANIFEST_ERROR))
        {
            // Check all error conditions for displaying the error messages and
            // counter at the first time
            fnUpdateErrorMessages(TRUE);
        }

        break;
    default:
        break;
   }
    return ucRetval;        
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnOptionalPrintModeConditionsCheck
//
// DESCRIPTION: 
//      Private function to check warning & disabling conditions in 
//      fnpOptionalPrintModeCheck()
//
// INPUTS:
//      None
//
// RETURNS:
//      Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/18/2008  Raymond Shen    Added code to check PSD/CMOS battery life.
//  12/03/2007  Oscar Wang      Renamed ESERVICE_UPLOAD_RECORD_PROMPT to 
//                              ESERVICE_HOME_SCRN_UPLOAD_PROMPT.
//  01/19/2007  Oscar Wang      Fix FRACA GMSE00113167: check unsent EService
//                              record first.
//  07/20/2006  Vicnent Yi      Removed the code for checking account problem
//  12/29/2005  Adam Liu        Initial version
//
// *************************************************************************/
// VBL_TBD_FP
static T_SCREEN_ID fnOptionalPrintModeConditionsCheck(void)
{
    T_SCREEN_ID usNextScreen = 0;

    UINT32      ulPostageValue;
    UINT8       bBobErrorClass;
    UINT8       bBobErrorCode;
    UINT8       pDescRegValue[SPARK_MONEY_SIZE];
    UINT8       bPrintMode;

    // Check PSD
    if((usNextScreen = fnCheckPSDState()) != 0)
    {
        return usNextScreen;
    }

    // Check PSD/UIC battery state.
    if((usNextScreen = fnCheckBatteryState()) != 0)
    {
        return usNextScreen;
    }

/* by Adam, uncomment this when this feature is supported
    if ( fnFlashGetByteParm(BP_DCAP_INDICIA_PWR_FAIL_REPORT) )
    {
            if ( fnIsIncidentReportNeeded() == TRUE )
        {
            lwDisablingConditions2 |= DCOND2_RPT_PWR_FAILURE;
            *pNextScr = GetScreen(METER_FAILURE_REPORT_NOTICE);
            goto xit;
        }
    }
*/

//  Temporarily comment out, may be useful for certain printing modes,
//    - Vincent
//  if((usNextScreen = fnCheckInspection()) != 0)
//  {
//      return usNextScreen;
//  }

    /* check for missing graphics */
    if((usNextScreen = fnCheckMissingGraphics()) != 0)
    {
        return usNextScreen;
    }

    /* Check if any Confirmation Service Records are pending */
    if ( fnGetRecordsPendingFlag() )   
    {
        usNextScreen = GetScreen(ESERVICE_HOME_SCRN_UPLOAD_PROMPT);
        return usNextScreen;
    }

xit:
    return (usNextScreen);
}

/* *************************************************************************
// FUNCTION NAME:           fnOptionalPrintModeConditionsCheck2
//
// DESCRIPTION: 
//      Private function to check warning & disabling conditions after 
//      requesting CM status 
//
// INPUTS:      None
// RETURNS:     Next screen
//
// WARNINGS/NOTES:
// 1. Currently, the Inview spec indicates that accounting information IS required
//    for TextAdDateTimeStamp mode.  However, that requirement may change.  So,   
//    there is a new function that will check the "current" requirements against  
//    the current accounting type to determine whether to check for a any account 
//    problems when in TextAdDateTimeStamp mode.
//
// MODIFICATION HISTORY:
//  2012.09.14 Clarisa Bellamy - Fraca fix 139154 - Except that the fraca is  
//                      not quite right.  See Note 1 above.  Now, use function 
//                      fnIsAbActReqForTxtAdDateTime, to determine if an 
//                      account selection is required in TextAdDateTimeStamp 
//                      modes. 
//  06/29/2007  Raymond Shen    Modified it to fix fraca 122018
//  06/19/2007  D. Kohl         Add treatment for piece count end of life
//                              error
//  05/18/2007  Adam Liu        Added PMODE_AD_ONLY and PMODE_TIME_STAMP along 
//                              with PMODE_AD_TIME_STAMP to support TextAdDateTime 
//  12/15/2006  Adam Liu        Update for "Update Required" check
//  07/20/2006  Vincent Yi      Added checking disabling condition "Meter Not
//                              Authorized"
//  03/24/2006  Adam Liu        Update the error detection logic to conform 
//                              with the home screen  
//  12/29/2005  Adam Liu        Initial version
// *************************************************************************/
// VBL_TBD_FP
static T_SCREEN_ID fnOptionalPrintModeConditionsCheck2(void)
{
    T_SCREEN_ID usNextScreen = 0;
    UINT16      wImageID;
    UINT8       ucPrintMode;
    UINT8       ubAcctType;

    fnUpdateDisablingWarningConditions();
    ucPrintMode = fnOITGetPrintMode();

    // Update other disabling/warning conditions that are not included in 
    // fnUpdateDisablingWarningCondition() if needed.
    switch( ucPrintMode )
    {
        case PMODE_POSTAGE_CORRECTION:
        case PMODE_MANIFEST:
/*
            if (fnGetAccountDisabledStatus() != ACT_SUCCESS)
            {
                fnPostDisabledStatus(DCOND_ACCOUNT_PROBLEM);
            }
*/
            break;

        case PMODE_TIME_STAMP:
        case PMODE_AD_TIME_STAMP:
        case PMODE_AD_ONLY:
            // If Acct Type is abacus, check if account selection is required 
            // for TextAdDateTimeStamp mode.  If so, check accounting status.
            if( fnIsAbActReqForTxtAdDateTime() == TRUE ) 
            {
/*
                if( fnAGDCheckDisablingConditions() != ACT_SUCCESS )
                {
                    fnPostDisabledStatus(DCOND_ACCOUNT_PROBLEM);
                }
*/
            }
            break;

        default:
            break;
    }

    if ((usNextScreen = fnPostCheckCMConditions()) != 0)
    {
        return usNextScreen;
    }
    if ((usNextScreen = fnCheckPSOCDisablingConditions()) != 0)
    {
        return usNextScreen;
    }

    /* the priority of the various disabling and other conditions is established by the order of
        the processing below */

    /* if necessary, show the screen that tells the user the time has been updated for
        daylight savings time (either ending or beginning) */
    if (fCMOSDSTAutoUpdated)
    {
        usNextScreen = GetScreen(DST_AUTO_CHANGE_OCCURRED_SCRN);       
        return usNextScreen;
    }
    /* set the scale location code if the code is pending */
/* Adam
    if (fSetScaleLocCodePending == TRUE)
    {
        *pNextScr = GetScreen(SCREEN_SET_SCALE_CODE);
        goto xit;
    }
    // The DCAPI may need to write to flash.
    if (lwDisablingConditions & DCOND_FLASH_WRITE_REQ)
    {
        *pNextScr = GetScreen(SCREEN_DCAP_BACKUP);
        goto xit;
    }
    // Check if any Confirmation Service Records are pending
    if ( fnGetRecordsPendingFlag() )   
    {
        *pNextScr = GetScreen(ESERVICE_HOME_SCRN_UPLOAD_PROMPT);
        goto xit;
    }
*/

//  Temporarily comment out, may be useful for certain printing modes,
//    - Vincent
//  if((usNextScreen = fnPostCheckInspection()) != 0)
//  {
//      return usNextScreen;
//  }

    if ((fnIsDisabledStatusSet(DCOND_TC_REQUIRED) == TRUE)      || 
        (fnIsDisabledStatusSet(DCOND_AD_REQUIRED) == TRUE)      || 
        (fnIsDisabledStatusSet(DCOND_RPT_REQUIRED) == TRUE)     || 
        (fnIsDisabledStatusSet(DCOND_GRAPHIC_REQUIRED) == TRUE))
    {
        if (fnGetDisplayMissingGraphicError())
        {
            usNextScreen = GetScreen(SCREEN_MISSING_GRAPHIC);
            return usNextScreen;
        }
        else
        {
            usNextScreen = GetScreen (SCREEN_MM_INDICIA_ERROR);
            return usNextScreen;
        }
    }

    // Errors to be displayed in Home screen should be checked here
    if ((fnIsDisabledStatusSet(DCOND_INSUFFICIENT_FUNDS) == TRUE) ||    
        (fnIsDisabledStatusSet(DCOND_INK_OUT) == TRUE)            ||    
        (fnIsDisabledStatusSet(DCOND_ACCESSING_PM_NVM) == TRUE)   ||    
        (fnIsDisabledStatusSet(DCOND_INKTANK_ERROR) == TRUE)      ||    
        (fnIsDisabledStatusSet(DCOND_WASTE_TANK_FULL) == TRUE)    ||    
        (fnIsDisabledStatusSet(DCOND_PRINTHEAD_ERROR) == TRUE)    ||    
        (fnIsDisabledStatusSet(DCOND_PAPER_IN_TRANSPORT) == TRUE) ||        
        (fnIsDisabledStatusSet(DCOND_TRANSPORT_OPEN) == TRUE)     ||
        (fnIsDisabledStatusSet(DCOND_OUT_OF_SERVICE)    == TRUE)  ||
        (fnIsDisabledStatusSet(DCOND_AUTHORIZATION_REQ) == TRUE)  ||
        (fnIsDisabledStatusSet(DCOND_PRINTER_FAULT) == TRUE)      ||
        (fnIsDisabledStatusSet(DCOND_ACCOUNT_PROBLEM) == TRUE)   ||
        (fnIsDisabledStatusSet(DCOND2_FEEDER_COVER_OPEN) == TRUE) ||
        (fnIsDisabledStatusSet(DCOND2_PIECE_COUNT_EOL) == TRUE)   ||
        (fnIsDisabledStatusSet(DCOND2_INFRA_UPDATE_REQUIRED) == TRUE))
    {
        ucPrintMode = fnOITGetPrintMode();
        switch(ucPrintMode)
        {
        case PMODE_AD_ONLY:
        case PMODE_TIME_STAMP:      
        case PMODE_AD_TIME_STAMP:
            usNextScreen = GetScreen(SCREEN_AD_DATE_TIME_ONLY_ERROR);
            break;
        case PMODE_POSTAGE_CORRECTION:
            usNextScreen = GetScreen(SCREEN_POSTAGE_CORRECTION_ERROR);
            break;
        case PMODE_DATE_CORRECTION:
            usNextScreen = GetScreen(SCREEN_DATE_CORRECTION_ERROR); 
            break;
        case PMODE_MANIFEST:
            usNextScreen = GetScreen(SCREEN_MANIFEST_ERROR); 
            break;
        default:
            break;
        }
        goto xit;
    }

    /* check for conditions for disabled screen */
    switch (fnOITGetPrintMode())
    {
        case PMODE_AD_ONLY:
        case PMODE_AD_TIME_STAMP:
        case PMODE_TIME_STAMP:
            if ((fnCheckIsPrintDateTimeOn() == FALSE)&&
                (fnCheckIsAdSelected() == FALSE) && 
                (fnCheckIsTextSelected() == FALSE))
            {
                usNextScreen = GetScreen(SCREEN_AD_DATE_TIME_ONLY_DISABLED);
                goto xit;                    
            }
            break;

        default:
            break;
    }

xit:
    return (usNextScreen);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnOptionalPrintModeMenuInit
//
// DESCRIPTION: 
//      Private function to prepare Optional Print Mode screen menu.
//
// INPUTS:
//      None
//
// WARNINGS/NOTES:
//      Menu possibilities:
//      1. Date-Time:  Print/Do Not Print
//      2. Ad
//      3. Text
//
// MODIFICATION HISTORY:
//  06/05/2006  Adam Liu      Initial version
//
// *************************************************************************/
static void fnOptionalPrintModeMenuInit (BOOL fUpdatePageLEDs)
{
    #define DATETIME_INDEX_IN_OPTIONAL_PRINT_MENU_TABLE   1     //the index of datetime in the menu table array below 
    #define AD_INDEX_IN_OPTIONAL_PRINT_MENU_TABLE   2     //the index of ad in the menu table array below 
    #define TEXT_INDEX_IN_OPTIONAL_PRINT_MENU_TABLE 3     //the index of text in the menu table array below 

    UINT32      ulSlotIndex = 0;
    UINT32      ulNextSlotIndex;

	// can't be made "const" because the value of the "Active Flag" needs to be changed.
    static S_MENU_SETUP_ENTRY   pOptionalPrintModeMenuInitTable[] = 
    {
        // force menu table reset before defining new slots
        {   CLEAR_MENU_TABLE,   (UINT8) 0,  (UINT8) 0,  OIC_SLOT_NOT_ACTIVE,  NULL_PTR },

        // Display number           Key           Label        Active   Conditional 
        // offset,                  assignment,   string id,   flag,    function
                                    
        /* Date-Time */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 1,    (UINT8) 1,   OIC_SLOT_ACTIVE,    NULL_PTR  },
        /* AD */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 2,    (UINT8) 2,   OIC_SLOT_ACTIVE,    NULL_PTR },
        /* TXT Msg */    
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 3,   OIC_SLOT_ACTIVE,    NULL_PTR },   

        {   END_MENU_TABLE_MARKER,  (UINT8) 0,    (UINT8) 0,   OIC_SLOT_NOT_ACTIVE,   NULL_PTR }  
    } ;
    
    //if Date-Time is enabled, show active menu slot, otherwise display the item only
    if (fnCheckDateTimeOnlyPrintingMode() == TRUE)   
    {
        pOptionalPrintModeMenuInitTable[DATETIME_INDEX_IN_OPTIONAL_PRINT_MENU_TABLE].bSlotActivity = OIC_SLOT_ACTIVE;
    }
    else
    {
        pOptionalPrintModeMenuInitTable[DATETIME_INDEX_IN_OPTIONAL_PRINT_MENU_TABLE].bSlotActivity = OIC_SLOT_DISPLAY_ONLY;
    }
    
    //if Ad is enabled, show active menu slot, otherwise display the item only
    if (fnGetMaxADSloganNum() > 0)   
    {
        pOptionalPrintModeMenuInitTable[AD_INDEX_IN_OPTIONAL_PRINT_MENU_TABLE].bSlotActivity = OIC_SLOT_ACTIVE;
    }
    else
    {
        pOptionalPrintModeMenuInitTable[AD_INDEX_IN_OPTIONAL_PRINT_MENU_TABLE].bSlotActivity = OIC_SLOT_DISPLAY_ONLY;
    }

    //if Text is enabled, show active menu slot, otherwise display the item only
    if (fnIsTextMessageSupported() == TRUE)   
    {
        pOptionalPrintModeMenuInitTable[TEXT_INDEX_IN_OPTIONAL_PRINT_MENU_TABLE].bSlotActivity = OIC_SLOT_ACTIVE;
    }
    else
    {
        pOptionalPrintModeMenuInitTable[TEXT_INDEX_IN_OPTIONAL_PRINT_MENU_TABLE].bSlotActivity = OIC_SLOT_DISPLAY_ONLY;
    }    
    
    // invoke generic slot initialization function with this menu initializer table
    ulNextSlotIndex = fnSetupMenuSlots( pOptionalPrintModeMenuInitTable, ulSlotIndex, 
                                        (UINT8) 3 ) ;
    
    // Set the total number of entries which will be use for displaying 
    // scroll bar
    fnSetMenuEntriesTotalNumber (ulNextSlotIndex - ulSlotIndex);

    #undef DATETIME_INDEX_IN_OPTIONAL_PRINT_MENU_TABLE 
    #undef AD_INDEX_IN_OPTIONAL_PRINT_MENU_TABLE      
    #undef TEXT_INDEX_IN_OPTIONAL_PRINT_MENU_TABLE     

}

/* *************************************************************************
// FUNCTION NAME: 
//      fncReportsPrintCheck
//
// DESCRIPTION: 
//      Continuation function that continues the Reports print checking after 
//      requesting CM status.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr, depends on the conditions checking
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/07/2006  Vincent Yi      Added code to start timer when online printing receipt
//  03/08/2006  Vincent Yi      Initial version
//
// *************************************************************************/
static SINT8 fncReportsPrintCheck( void         (** pContinuationFcn)(), 
                                   UINT32       *   pMsgsAwaited,
                                   T_SCREEN_ID  *   pNextScr )
{
    SINT8 ucRetval = PREPOST_COMPLETE;

    *pNextScr = 0;

    if (fnIsOnlineReportPrinting() == TRUE) 
    {   // Start timer when online print receipt
        fnInfraSetScreenTimeoutInterval (INFRA_OI_TIMEOUT_INTERVAL_REFUND_COMPLETE);
        fnpInfraStartTimeoutClock (pContinuationFcn, pMsgsAwaited, pNextScr);       
    }

    *pNextScr = fnReportsPrintConditionsCheck2();

    if (*pNextScr == 0)
    {
        // Check all warning conditions for displaying the warning messages and
        // counter at the first time
        fnUpdateWarningMessages(TRUE);
//      if (fnIsOnlineReportPrinting() == TRUE)
//      {   // For online printing report, to ReportOnlinePrintReady screen
//          pNextScr = GetScreen (SCREEN_REPORTS_ONLINE_PRINT);
//      }
    }
    else 
    {
        // When online print, before entering any error screen, disconnect
        // from att
//      if (fnIsOnlineReportPrinting() == TRUE)
//      {
//          fnpInfraGeneralErr (pContinuationFcn, pMsgsAwaited, pNextScr);
//      }

        if (*pNextScr == GetScreen (SCREEN_REPORT_PH_PRINT_ERROR))
        {   
            // Check all error conditions for displaying the error messages and
            // counter at the first time
            fnUpdateErrorMessages(TRUE);
        }
    }

    return ucRetval;        
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnReportsPrintConditionsCheck
//
// DESCRIPTION: 
//      Private function to check warning & disabling conditions in 
//      fnpReportsPrintCheck()
//
// INPUTS:
//      None
//
// RETURNS:
//      Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  01/08/2007  Kan Jiang      Modified for Fraca 112344
//  12/06/2005  Vincent Yi      Initial version
// *************************************************************************/
// VBL_TBD_FP
static T_SCREEN_ID fnReportsPrintConditionsCheck(void)
{
    T_SCREEN_ID usNextScreen = 0;

/*  Kan commented for fixing Fraca 112344
    // Check PSD
    if((usNextScreen = fnCheckPSDState()) != 0)
    {
        return usNextScreen;
    }
*/
    // check for missing graphics
    if((usNextScreen = fnCheckMissingGraphics()) != 0)
    {
        return usNextScreen;
    }

    return usNextScreen;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnReportsPrintConditionsCheck2
//
// DESCRIPTION: 
//      Private function to check warning & disabling conditions after 
//      requesting CM status when report print ready
//
// INPUTS:
//      None
//
// RETURNS:
//      Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/15/2006  Adam Liu        Update for "Update Required" check
//  11/27/2006  Kan Jiang       Commented the check of DCOND_OUT_OF_SERVICE error
//                              to fix Fraca110726
//  07/20/2006  Vincent Yi      Added checking disabling conditions "Meter Not
//                              Authorized" and "Out of Service"
//  03/08/2006  Vincent Yi      Initial version
//
// *************************************************************************/
static T_SCREEN_ID fnReportsPrintConditionsCheck2(void)
{
    T_SCREEN_ID usNextScreen = 0;

    fnUpdateDisablingWarningConditions();
    if ((usNextScreen = fnPostCheckCMConditions()) != 0)
    {
        return usNextScreen;
    }
    if ((usNextScreen = fnCheckPSOCDisablingConditions()) != 0)
    {
        return usNextScreen;
    }

    if ((fnIsDisabledStatusSet(DCOND_TC_REQUIRED) == TRUE)       || 
        (fnIsDisabledStatusSet(DCOND_AD_REQUIRED) == TRUE)       || 
        (fnIsDisabledStatusSet(DCOND_RPT_REQUIRED) == TRUE)      || 
        (fnIsDisabledStatusSet(DCOND_GRAPHIC_REQUIRED) == TRUE))
    {
        if (fnGetDisplayMissingGraphicError())
        {
            usNextScreen = GetScreen(SCREEN_MISSING_GRAPHIC);
            return usNextScreen;
        }
        else
        {
            usNextScreen = GetScreen (SCREEN_MM_INDICIA_ERROR);
            return usNextScreen;
        }
    }

    // Errors to be displayed in Home screen should be checked here
    if ((fnIsDisabledStatusSet(DCOND_INK_OUT) == TRUE)            ||    
        (fnIsDisabledStatusSet(DCOND_ACCESSING_PM_NVM) == TRUE)   ||    
        (fnIsDisabledStatusSet(DCOND_INKTANK_ERROR) == TRUE)      ||    
        (fnIsDisabledStatusSet(DCOND_WASTE_TANK_FULL) == TRUE)    ||    
        (fnIsDisabledStatusSet(DCOND_PRINTHEAD_ERROR) == TRUE)    ||    
        (fnIsDisabledStatusSet(DCOND_PAPER_IN_TRANSPORT) == TRUE) ||        
        (fnIsDisabledStatusSet(DCOND_TRANSPORT_OPEN) == TRUE)     ||
        (fnIsDisabledStatusSet(DCOND_PRINTER_FAULT) == TRUE)      ||
        //(fnIsDisabledStatusSet(DCOND_OUT_OF_SERVICE) == TRUE)   ||
        (fnIsDisabledStatusSet(DCOND_AUTHORIZATION_REQ) == TRUE)  ||
        (fnIsDisabledStatusSet(DCOND2_FEEDER_COVER_OPEN) == TRUE) ||
        (fnIsDisabledStatusSet(DCOND2_INFRA_UPDATE_REQUIRED) == TRUE))
    {
        usNextScreen = GetScreen (SCREEN_REPORT_PH_PRINT_ERROR);
        return usNextScreen;
    }

    return (usNextScreen);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnHomeScreenMenuInit
//
// DESCRIPTION: 
//      Private function to prepare Home screen menu.
//
// INPUTS:
//      None
//
// WARNINGS/NOTES:
//      Menu possibilities:
//      1. Account
//      2. Class
//      3. Ad
//      4. Inscription
//      5. Text Message
//    6~9. <Dest>   -   ZIP, ZONE, ZIP/ZONE, Country
//     10. Fees
//     11. EKP
//     12. Task Number
//     13. German Piece Count
//     14. Reply Code
//     15. Indicia Type
//     16. Services
//     17. Batch
//     18. Barcode
//
// MODIFICATION HISTORY:
//  09/22/2009  Raymond Shen    Polish the code for full auto inscription support to
//                              to correct the problem invoked by adding "Barcode" option.
//  08/26/2009  Jingwei,Li      Add option "Barcode" for CPS.
//  11/21/2008  Joey Cui        Added full auto inscription support for French
//  10/29/2008  Raymond Shen    Add options "Services" and "Batch".
//  08/05/2008  Bob Li          Added item to display indicia type
//  09/26/2007  Adam Liu        Adjust the ZZC dest and fee menu item in the menu
//  08/01/2007  Joey Cui        Add condition check for reply code
//  11/09/2005  Vincent Yi      Initial version
//
// *************************************************************************/
static void fnHomeScreenMenuInit (BOOL fUpdatePageLEDs)
{
    UINT32      ulSlotIndex = 0;
    UINT32      ulNextSlotIndex;
    UINT32      ulTotalNumber;
    UINT8       autoInscState, ucStatus, ucIndex;
    BOOL        fEnabled = FALSE;
    UINT32      ulInscrSlotIndex = 1;
    UINT32      ulReplyCdSlotIndex = 1;

#define INSCR_LABEL_STRING_ID     4         //The label string id of "Inscrip.:"
#define REPLYCD_LABEL_STRING_ID     14  //The label string id of "Reply Cd:"

    
	// can't be made "const" because the value of the "Active Flag" needs to be changed.
    static S_MENU_SETUP_ENTRY   pHomeMenuInitTable[] = 
    {
        // force menu table reset before defining new slots
        {   CLEAR_MENU_TABLE,   (UINT8) 0,  (UINT8) 0,  FALSE,  NULL_PTR },

        // Display number           Key           Label        Active   Conditional 
        // offset,                  assignment,   string id,   flag,    function
                                    
        /* Account */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 1,    (UINT8) 1,   TRUE,    fnCheckAccountEnabled },
        /* Class */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 2,    (UINT8) 2,   TRUE,    fnCheckRatingEnabled },
        /* ZIP */        
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 6,   TRUE,    fnCheckZipHome },
        /* Zone */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 7,   TRUE,    fnCheckZoneHome },
        /* ZIP/Zone */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 8,   TRUE,    fnCheckZipZoneHome },
        /* Country */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 9,   TRUE,    fnCheckCountryHome },
        /* Barcode */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 15,    (UINT8) 18,  TRUE,    fnIsCanadaPackageServie },
        /* Services */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 13,    (UINT8) 16,  TRUE,    fnCheckServicesHome },
        /* Fees */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 4,    (UINT8) 10,  TRUE,    fnCheckFeesHome },
        /* AD */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 5,    (UINT8) 3,   TRUE,    fnCheckAdHome },
        /* INS */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 6,    (UINT8) INSCR_LABEL_STRING_ID,   TRUE,    fnCheckInscHome },
        /* INDICIA */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 7,    (UINT8) 15,   TRUE,    fnIsIBILiteIndiciaSupport },
        /* TXT Msg */    
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 8,    (UINT8) 5,   TRUE,    fnCheckTextMessageHome },
        /* EKP */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 9,    (UINT8) 11,  TRUE,    fnFrankTaskEKPEnable },
        /* Task Number, key assignment 10 is the zero key on the keypad,
			but key assignment 9 actually does the changing of the task number */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 10,    (UINT8) 12,  OIC_SLOT_DISPLAY_ONLY,    fnFrankTaskEKPEnable },
        /* Piece ID */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 11,    (UINT8) 13,  TRUE,   fnFrankTaskEKPEnable },
        /* Reply Cd */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 12,    (UINT8) REPLYCD_LABEL_STRING_ID,  TRUE,    fnCheckReplyCodeHome },
        /* Batch */
       // {   USE_DEFAULT_ASSIGNMENT, (UINT8) 14,    (UINT8) 17,  TRUE,    fnCheckBatchHome },

        {   END_MENU_TABLE_MARKER,  (UINT8) 0,    (UINT8) 0,   FALSE,   NULL_PTR }  
    } ;

    
    // Modify the attribute of option "Reply Cd:"
    while(pHomeMenuInitTable[ulReplyCdSlotIndex].wDisplayNumber != END_MENU_TABLE_MARKER)
    {
        if(pHomeMenuInitTable[ulReplyCdSlotIndex].bLabelStringId == REPLYCD_LABEL_STRING_ID)
        {
/*
    if (fnIsReplyMailInUse((SINT8*) NULL) != TRUE)
    {   
                pHomeMenuInitTable[ulReplyCdSlotIndex].bSlotActivity = OIC_SLOT_DISPLAY_ONLY;
    }
    else
    {
                pHomeMenuInitTable[ulReplyCdSlotIndex].bSlotActivity = TRUE; // must do, static variable
    }
*/
            break;
        }
        ulReplyCdSlotIndex ++;
    }

    // Modify the attribute of option "Inscrip:"
    while(pHomeMenuInitTable[ulInscrSlotIndex].wDisplayNumber != END_MENU_TABLE_MARKER)
    {
        if(pHomeMenuInitTable[ulInscrSlotIndex].bLabelStringId == INSCR_LABEL_STRING_ID)
        {
            autoInscState = fnFlashGetAutoInscrType();
            if(IsFeatureEnabled(&fEnabled, AUTO_AD_INSCR_SUPPORT, &ucStatus, &ucIndex) && fEnabled &&
                ((FULL_AUTO_INSCRIPTION_VIA_DCAP == autoInscState) || (FULL_AUTO_INSCRIPTION_VIA_RATES == autoInscState)) )
            {
                  pHomeMenuInitTable[ulInscrSlotIndex].bSlotActivity = OIC_SLOT_DISPLAY_ONLY;
            }
            else
            {
                  pHomeMenuInitTable[ulInscrSlotIndex].bSlotActivity = OIC_SLOT_ACTIVE;
            }
            break;
        }
        ulInscrSlotIndex ++;
    }

#undef INSCR_LABEL_STRING_ID            //The label string id of "Inscrip.:"
#undef REPLYCD_LABEL_STRING_ID       //The label string id of "Reply Cd:"

    // invoke generic slot initialization function with this menu initializer table
    ulNextSlotIndex = fnSetupMenuSlots( pHomeMenuInitTable, ulSlotIndex, 
                                        (UINT8) 4 ) ;

    ulTotalNumber = ulNextSlotIndex - ulSlotIndex;

    // Set the total number of entries which will be use for displaying 
    // scroll bar
    fnSetMenuEntriesTotalNumber (ulTotalNumber);

    if(fnCheckBatchHome() == TRUE)
    {
        S_MENU_SLOT stMenuSlot;

        // Initialize the slot to be inserted into the Home menu.
        memset(&stMenuSlot, 0, sizeof(S_MENU_SLOT));

        if(ulTotalNumber < 3)
        {
            stMenuSlot.fActive = OIC_SLOT_RESERVE;

            for(; ulTotalNumber < 3; ulTotalNumber ++)
            {
                (void) fnInsertMenuSlot(&stMenuSlot, (UINT16)ulTotalNumber, TRUE, FALSE);
            }

            stMenuSlot.bKeyAssignment = 14;
            stMenuSlot.wDisplayNumber = 4;
            stMenuSlot.bLabelString = 17;   // Batch
            stMenuSlot.fActive = OIC_SLOT_ACTIVE;
            (void) fnInsertMenuSlot(&stMenuSlot, (UINT16)ulTotalNumber, TRUE, FALSE);
            ulTotalNumber ++;
        }
        else
        {
            stMenuSlot.bKeyAssignment = 14;
            stMenuSlot.wDisplayNumber = 4;
            stMenuSlot.bLabelString = 17;   // Batch
            stMenuSlot.fActive = OIC_SLOT_ACTIVE;
            (void) fnInsertMenuSlot(&stMenuSlot, (UINT16)3, TRUE, FALSE);
            ulTotalNumber ++;
        }
    }

    // Update LED indicator
    if (fUpdatePageLEDs == TRUE)
    {
        fnUpdateLEDPageIndicator();
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnAbacusHomeScreenMenuInit
//
// DESCRIPTION: 
//      Private function to prepare Abacus Home screen menu.
//
// INPUTS:
//      None
//
// WARNINGS/NOTES:
//      Menu possibilities:
//      1. Account
//      2. Class
//      3. Ad
//      4. End batch
//      5. Inscription
//      6. Text Message
//    7~10. <Dest>   -   ZIP, ZONE, ZIP/ZONE, Country
//     11. Fees
//     12. Job ID
//     13. Surchrg
//     14. Batch
//     15. Batch Count
//
// MODIFICATION HISTORY:
//  01/11/2009  Raymond Shen    Added the �EKP/Task Number, Piece ID, Reply Cd� soft keys 
//                              to Abacus home screens.
//  09/22/2009  Raymond Shen    Polish the code for full auto inscription support.
//  11/21/2008  Joey Cui      Added full auto inscription support for French
//  08/05/2008  Bob Li          Added item to display indicia type.
//  12/04/2007  Raymond Shen    Add code to fix the refresh problem of displaying the
//                              selected Abacus full name on home screen.
//  09/26/2007  Adam Liu      Adjust the ZZC dest and fee menu item in the menu,
//                            also make sure "End batch" is on the soft4 slot, if any. 
//  02/07/2007  Adam Liu      Initial version
//
// *************************************************************************/
static void fnAbacusHomeScreenMenuInit (BOOL fUpdatePageLEDs)
{
    UINT32      ulSlotIndex = 0;
    UINT32      ulNextSlotIndex;
    UINT8       autoInscState, ucStatus, ucIndex;
    BOOL        fEnabled = FALSE;
    SINT16      sAbacusStatus;
//    ACCOUNT_INFO    pAbTmpActInfo;
    UNICHAR     *pTempAcctName = fnOiAbGetTempActName();
    UINT32      ulInscrSlotIndexInAbacusHome = 1;
    UINT32      ulInscrSlotIndexInAbacusHomeWithoutDest = 1;
    UINT32      ulInscrSlotIndexInAbacusHomeWithoutDestFee = 1;
    UINT32      ulReplyCdSlotIndexInAbacusHome = 1;
    UINT32      ulReplyCdotIndexInAbacusHomeWithoutDest = 1;
    UINT32      ulReplyCdotIndexInAbacusHomeWithoutDestFee = 1;

#define INSCR_LABEL_STRING_ID     5         //The label string id of "Inscrip.:"
#define REPLYCD_LABEL_STRING_ID     20  //The label string id of "Reply Cd:"

    //These following three menu tables are to make sure "End Batch" is on the
    //soft4 slot on the first page menu in every condition. - Adam
    
    //with ZZC dest and selected fee, or ZZC dest only
	// can't be made "const" because the value of the "Active Flag" needs to be changed.
    static S_MENU_SETUP_ENTRY   pAbacusHomeMenuInitTable[] = 
    {
        // force menu table reset before defining new slots
        {   CLEAR_MENU_TABLE,   (UINT8) 0,  (UINT8) 0,  FALSE,  NULL_PTR },

        // Display number           Key           Label        Active   Conditional 
        // offset,                  assignment,   string id,   flag,    function
                                    
        /* Account */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 1,    (UINT8) 1,   OIC_SLOT_ACTIVE,    fnCheckAccountEnabled },
        /* Class */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 2,    (UINT8) 2,   OIC_SLOT_ACTIVE,    fnCheckRatingEnabled },
        /* ZIP */        
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 7,   OIC_SLOT_ACTIVE,    fnCheckZipHome },
        /* Zone */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 8,   OIC_SLOT_ACTIVE,    fnCheckZoneHome },
        /* ZIP/Zone */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 9,   OIC_SLOT_ACTIVE,    fnCheckZipZoneHome },
        /* Country */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 10,  OIC_SLOT_ACTIVE,    fnCheckCountryHome },
        /* End Batch */        
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 4,    (UINT8) 4,   OIC_SLOT_ACTIVE,    fnCheckBatchAvailable },        
        /* Fees */
///        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 5,    (UINT8) 11,  OIC_SLOT_ACTIVE,    fnCheckFeesHome },
        /* AD */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 6,    (UINT8) 3,   OIC_SLOT_ACTIVE,    fnCheckAdHome },
        /* INS */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 7,    (UINT8) INSCR_LABEL_STRING_ID,   OIC_SLOT_ACTIVE,    fnCheckInscHome },
        /* INDICIA */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 8,    (UINT8) 16,   OIC_SLOT_ACTIVE,    fnIsIBILiteIndiciaSupport },
        /* TXT Msg */    
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 9,    (UINT8) 6,   OIC_SLOT_ACTIVE,    fnCheckTextMessageHome },
        /* Job ID */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 10,    (UINT8) 12,  OIC_SLOT_ACTIVE,    fnCheckAbacusAvailable },        
        /* Surchrg */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 11,    (UINT8) 13,  OIC_SLOT_ACTIVE,    fnCheckAbacusAvailable },
        /* Batch */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 12,    (UINT8) 14,  OIC_SLOT_DISPLAY_ONLY,    NULL_PTR }, 
        /* Batch Count */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 13,    (UINT8) 15,  OIC_SLOT_DISPLAY_ONLY,    NULL_PTR }, 
        /* EKP/Task */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 14,    (UINT8) 17,  TRUE,    fnFrankTaskEKPEnable },
        /* Number */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 15,    (UINT8) 18,  OIC_SLOT_DISPLAY_ONLY,    fnFrankTaskEKPEnable },
        /* Piece ID */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 16,    (UINT8) 19,  TRUE,   fnFrankTaskEKPEnable },
        /* Reply Cd */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 17,    (UINT8) REPLYCD_LABEL_STRING_ID,  TRUE,    fnCheckReplyCodeHome },
        
        {   END_MENU_TABLE_MARKER,  (UINT8) 0,    (UINT8) 0,   FALSE,   NULL_PTR }  
    } ;

    //with selected fee only, no ZZC dest
	// can't be made "const" because the value of the "Active Flag" needs to be changed.
    static S_MENU_SETUP_ENTRY   pAbacusHomeMenuWithoutDestInitTable[] = 
    {
        // force menu table reset before defining new slots
        {   CLEAR_MENU_TABLE,   (UINT8) 0,  (UINT8) 0,  FALSE,  NULL_PTR },

        // Display number           Key           Label        Active   Conditional 
        // offset,                  assignment,   string id,   flag,    function
                                    
        /* Account */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 1,    (UINT8) 1,   OIC_SLOT_ACTIVE,    fnCheckAccountEnabled },
        /* Class */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 2,    (UINT8) 2,   OIC_SLOT_ACTIVE,    fnCheckRatingEnabled },
        /* Fees */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 5,    (UINT8) 11,  OIC_SLOT_ACTIVE,    fnCheckFeesHome },
        /* End Batch */        
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 4,    (UINT8) 4,   OIC_SLOT_ACTIVE,    fnCheckBatchAvailable },        
        /* AD */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 6,    (UINT8) 3,   OIC_SLOT_ACTIVE,    fnCheckAdHome },
        /* INS */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 7,    (UINT8) INSCR_LABEL_STRING_ID,   OIC_SLOT_ACTIVE,    fnCheckInscHome },
        /* INDICIA */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 8,    (UINT8) 16,   OIC_SLOT_ACTIVE,    fnIsIBILiteIndiciaSupport },
        /* TXT Msg */    
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 9,    (UINT8) 6,   OIC_SLOT_ACTIVE,    fnCheckTextMessageHome },
        /* Job ID */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 10,    (UINT8) 12,  OIC_SLOT_ACTIVE,    fnCheckAbacusAvailable },        
        /* Surchrg */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 11,    (UINT8) 13,  OIC_SLOT_ACTIVE,    fnCheckAbacusAvailable },
        /* Batch */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 12,    (UINT8) 14,  OIC_SLOT_DISPLAY_ONLY,    NULL_PTR }, 
        /* Batch Count */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 13,    (UINT8) 15,  OIC_SLOT_DISPLAY_ONLY,    NULL_PTR }, 
        /* EKP/Task */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 14,    (UINT8) 17,  TRUE,    fnFrankTaskEKPEnable },
        /* Number */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 15,    (UINT8) 18,  OIC_SLOT_DISPLAY_ONLY,    fnFrankTaskEKPEnable },
        /* Piece ID */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 16,    (UINT8) 19,  TRUE,   fnFrankTaskEKPEnable },
        /* Reply Cd */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 17,    (UINT8) REPLYCD_LABEL_STRING_ID,  TRUE,    fnCheckReplyCodeHome },
        
        {   END_MENU_TABLE_MARKER,  (UINT8) 0,    (UINT8) 0,   FALSE,   NULL_PTR }  
    } ;
    
    //neither ZZC dest nor fee
	// can't be made "const" because the value of the "Active Flag" needs to be changed.
    static S_MENU_SETUP_ENTRY   pAbacusHomeMenuWithoutDestFeeInitTable[] = 
    {
        // force menu table reset before defining new slots
        {   CLEAR_MENU_TABLE,   (UINT8) 0,  (UINT8) 0,  FALSE,  NULL_PTR },

        // Display number           Key           Label        Active   Conditional 
        // offset,                  assignment,   string id,   flag,    function
                                    
        /* Account */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 1,    (UINT8) 1,   OIC_SLOT_ACTIVE,    fnCheckAccountEnabled },
        /* Class */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 2,    (UINT8) 2,   OIC_SLOT_ACTIVE,    fnCheckRatingEnabled },
        /* AD */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 6,    (UINT8) 3,   OIC_SLOT_ACTIVE,    fnCheckAdHome },
        /* End Batch */        
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 4,    (UINT8) 4,   OIC_SLOT_ACTIVE,    fnCheckBatchAvailable },        
        /* INS */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 7,    (UINT8) INSCR_LABEL_STRING_ID,   OIC_SLOT_ACTIVE,    fnCheckInscHome },
        /* INDICIA */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 8,    (UINT8) 16,   OIC_SLOT_ACTIVE,    fnIsIBILiteIndiciaSupport },
        /* TXT Msg */    
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 9,    (UINT8) 6,   OIC_SLOT_ACTIVE,    fnCheckTextMessageHome },
        /* Job ID */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 10,    (UINT8) 12,  OIC_SLOT_ACTIVE,    fnCheckAbacusAvailable },        
        /* Surchrg */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 11,    (UINT8) 13,  OIC_SLOT_ACTIVE,    fnCheckAbacusAvailable },
        /* Batch */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 12,    (UINT8) 14,  OIC_SLOT_DISPLAY_ONLY,    NULL_PTR }, 
        /* Batch Count */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 13,    (UINT8) 15,  OIC_SLOT_DISPLAY_ONLY,    NULL_PTR },         
        /* EKP/Task */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 14,    (UINT8) 17,  TRUE,    fnFrankTaskEKPEnable },
        /* Number */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 15,    (UINT8) 18,  OIC_SLOT_DISPLAY_ONLY,    fnFrankTaskEKPEnable },
        /* Piece ID */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 16,    (UINT8) 19,  TRUE,   fnFrankTaskEKPEnable },
        /* Reply Cd */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 17,    (UINT8) REPLYCD_LABEL_STRING_ID,  TRUE,    fnCheckReplyCodeHome },
        
        {   END_MENU_TABLE_MARKER,  (UINT8) 0,    (UINT8) 0,   FALSE,   NULL_PTR }  
    } ;

    // Modify the attribute of option "Reply Cd:"
    while(pAbacusHomeMenuInitTable[ulReplyCdSlotIndexInAbacusHome].wDisplayNumber != END_MENU_TABLE_MARKER)
    {
        if(pAbacusHomeMenuInitTable[ulReplyCdSlotIndexInAbacusHome].bLabelStringId == REPLYCD_LABEL_STRING_ID)
        {
            break;
        }
        ulReplyCdSlotIndexInAbacusHome ++;
    }

    while(pAbacusHomeMenuWithoutDestInitTable[ulReplyCdotIndexInAbacusHomeWithoutDest].wDisplayNumber != END_MENU_TABLE_MARKER)
    {
        if(pAbacusHomeMenuWithoutDestInitTable[ulReplyCdotIndexInAbacusHomeWithoutDest].bLabelStringId == REPLYCD_LABEL_STRING_ID)
        {
            break;
        }
        ulReplyCdotIndexInAbacusHomeWithoutDest ++;
    }
    
    while(pAbacusHomeMenuWithoutDestFeeInitTable[ulReplyCdotIndexInAbacusHomeWithoutDestFee].wDisplayNumber != END_MENU_TABLE_MARKER)
    {
        if(pAbacusHomeMenuWithoutDestFeeInitTable[ulReplyCdotIndexInAbacusHomeWithoutDestFee].bLabelStringId == REPLYCD_LABEL_STRING_ID)
        {
            break;
        }
        ulReplyCdotIndexInAbacusHomeWithoutDestFee ++;
    }

    if (fnIsReplyMailInUse((SINT8*) NULL) != TRUE)
    {   
        pAbacusHomeMenuInitTable[ulReplyCdSlotIndexInAbacusHome].bSlotActivity = OIC_SLOT_DISPLAY_ONLY;
        pAbacusHomeMenuWithoutDestInitTable[ulReplyCdotIndexInAbacusHomeWithoutDest].bSlotActivity = OIC_SLOT_DISPLAY_ONLY;
        pAbacusHomeMenuWithoutDestFeeInitTable[ulReplyCdotIndexInAbacusHomeWithoutDestFee].bSlotActivity = OIC_SLOT_DISPLAY_ONLY;
    }
    else
    {
        // must do, static variable
        pAbacusHomeMenuInitTable[ulReplyCdSlotIndexInAbacusHome].bSlotActivity = OIC_SLOT_ACTIVE;
        pAbacusHomeMenuWithoutDestInitTable[ulReplyCdotIndexInAbacusHomeWithoutDest].bSlotActivity = OIC_SLOT_ACTIVE;
        pAbacusHomeMenuWithoutDestFeeInitTable[ulReplyCdotIndexInAbacusHomeWithoutDestFee].bSlotActivity = OIC_SLOT_ACTIVE;
    }


    // Modify the attribute of option "Inscrip:"
    while(pAbacusHomeMenuInitTable[ulInscrSlotIndexInAbacusHome].wDisplayNumber != END_MENU_TABLE_MARKER)
    {
        if(pAbacusHomeMenuInitTable[ulInscrSlotIndexInAbacusHome].bLabelStringId == INSCR_LABEL_STRING_ID)
        {
            break;
        }
        ulInscrSlotIndexInAbacusHome ++;
    }

    while(pAbacusHomeMenuWithoutDestInitTable[ulInscrSlotIndexInAbacusHomeWithoutDest].wDisplayNumber != END_MENU_TABLE_MARKER)
    {
        if(pAbacusHomeMenuWithoutDestInitTable[ulInscrSlotIndexInAbacusHomeWithoutDest].bLabelStringId == INSCR_LABEL_STRING_ID)
        {
            break;
        }
        ulInscrSlotIndexInAbacusHomeWithoutDest ++;
    }
    
    while(pAbacusHomeMenuWithoutDestFeeInitTable[ulInscrSlotIndexInAbacusHomeWithoutDestFee].wDisplayNumber != END_MENU_TABLE_MARKER)
    {
        if(pAbacusHomeMenuWithoutDestFeeInitTable[ulInscrSlotIndexInAbacusHomeWithoutDestFee].bLabelStringId == INSCR_LABEL_STRING_ID)
        {
            break;
        }
        ulInscrSlotIndexInAbacusHomeWithoutDestFee ++;
    }
#undef INSCR_LABEL_STRING_ID            //The label string id of "Inscrip.:"
#undef REPLYCD_LABEL_STRING_ID       //The label string id of "Reply Cd:"

    autoInscState = fnFlashGetAutoInscrType();
    if(IsFeatureEnabled(&fEnabled, AUTO_AD_INSCR_SUPPORT, &ucStatus, &ucIndex) && fEnabled &&
        ((FULL_AUTO_INSCRIPTION_VIA_DCAP == autoInscState) || (FULL_AUTO_INSCRIPTION_VIA_RATES == autoInscState)) )
    {
          pAbacusHomeMenuInitTable[ulInscrSlotIndexInAbacusHome].bSlotActivity = OIC_SLOT_DISPLAY_ONLY;
          pAbacusHomeMenuWithoutDestInitTable[ulInscrSlotIndexInAbacusHomeWithoutDest].bSlotActivity = OIC_SLOT_DISPLAY_ONLY;
          pAbacusHomeMenuWithoutDestFeeInitTable[ulInscrSlotIndexInAbacusHomeWithoutDestFee].bSlotActivity = OIC_SLOT_DISPLAY_ONLY;
    }
    else
    {
          pAbacusHomeMenuInitTable[ulInscrSlotIndexInAbacusHome].bSlotActivity = OIC_SLOT_ACTIVE;
          pAbacusHomeMenuWithoutDestInitTable[ulInscrSlotIndexInAbacusHomeWithoutDest].bSlotActivity = OIC_SLOT_ACTIVE;
          pAbacusHomeMenuWithoutDestFeeInitTable[ulInscrSlotIndexInAbacusHomeWithoutDestFee].bSlotActivity = OIC_SLOT_ACTIVE;
    }

    if ((fnCheckZipHome() == TRUE) || (fnCheckZoneHome() == TRUE)
        || (fnCheckZipZoneHome() == TRUE) || (fnCheckCountryHome() == TRUE))
    {   
        // ZZC dest is available
        ulNextSlotIndex = fnSetupMenuSlots( pAbacusHomeMenuInitTable, ulSlotIndex, 
                                            (UINT8) 4 ) ;
    }
    else
    {
        if (fnCheckFeesHome() == TRUE)
        {
            // Fee is available but no ZZC dest
            ulNextSlotIndex = fnSetupMenuSlots( pAbacusHomeMenuWithoutDestInitTable, ulSlotIndex, 
                                                (UINT8) 4 ) ;
        }
        else
        {
            // neither Fee nor ZZC dest
            ulNextSlotIndex = fnSetupMenuSlots( pAbacusHomeMenuWithoutDestFeeInitTable, ulSlotIndex, 
                                                (UINT8) 4 ) ;
        }
    }

    // Set the total number of entries which will be use for displaying 
    // scroll bar
    fnSetMenuEntriesTotalNumber (ulNextSlotIndex - ulSlotIndex);

    // Update LED indicator
    if (fUpdatePageLEDs == TRUE)
    {
        fnUpdateLEDPageIndicator();
    }

    // Get Abacus full display name and store it in a buffer, which will be used by
    // corresponding acct name field function, to avoid the field refresh problem
    // caused by long time of getting Abacus full name.
/*
    sAbacusStatus = fnAAMGetCurrentAcctInfo(&pAbTmpActInfo);
    if(sAbacusStatus == ABACUS_SUCCESS)
    {
        (void)fnAbGetAcctFullDisplayNameByID(pAbTmpActInfo.usAccountID, pTempAcctName);

        // Try again if get name process failed for some reason.
        if(*pTempAcctName == 0)
        {
            OSWakeAfter(100);
            (void)fnAbGetAcctFullDisplayNameByID(pAbTmpActInfo.usAccountID, pTempAcctName);
        }
    }
    else
    {
        *pTempAcctName = 0;
    }
*/
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnAbacusPrintingScreenMenuInit
//
// DESCRIPTION: 
//      Private function to prepare Abacus printing screen menu.
//
// INPUTS:
//      None
//
// WARNINGS/NOTES:
//      Menu possibilities:
//      1. Account
//      2. Class
//      3. Ad
//      4. Inscription
//      5. Text Message
//    6~9. <Dest>   -   ZIP, ZONE, ZIP/ZONE, Country
//     10. Fees
//
// MODIFICATION HISTORY:
//  01/11/2009  Raymond Shen    Added the �EKP/Task Number, Piece ID, Reply Cd� soft keys 
//                              to Abacus Home printing screens.
//  08/05/2008  Bob Li        Added item to display indicia type.
//  09/26/2007  Adam Liu      Adjust the ZZC dest and fee menu item in the menu
//  03/08/2007  Adam Liu      Initial version
//
// *************************************************************************/
static void fnAbacusPrintingScreenMenuInit (BOOL fUpdatePageLEDs)
{
    UINT32      ulSlotIndex = 0;
    UINT32      ulNextSlotIndex;

	// can't be made "const" because the value of the "Active Flag" needs to be changed.
    static S_MENU_SETUP_ENTRY   pHomeMenuInitTable[] = 
    {
        // force menu table reset before defining new slots
        {   CLEAR_MENU_TABLE,   (UINT8) 0,  (UINT8) 0,  FALSE,  NULL_PTR },

        // Display number           Key           Label        Active   Conditional 
        // offset,                  assignment,   string id,   flag,    function
                                    
        /* Account */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 1,    (UINT8) 1,   TRUE,    fnCheckAccountEnabled },
        /* Class */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 2,    (UINT8) 2,   TRUE,    fnCheckRatingEnabled },
        /* ZIP */        
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 6,   TRUE,    fnCheckZipHome },
        /* Zone */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 7,   TRUE,    fnCheckZoneHome },
        /* ZIP/Zone */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 8,   TRUE,    fnCheckZipZoneHome },
        /* Country */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 9,   TRUE,    fnCheckCountryHome },
        /* Fees */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 4,    (UINT8) 10,  TRUE,    fnCheckFeesHome },
        /* AD */
  //      {   USE_DEFAULT_ASSIGNMENT, (UINT8) 5,    (UINT8) 3,   TRUE,    fnCheckAdHome },
        /* INS */
  //      {   USE_DEFAULT_ASSIGNMENT, (UINT8) 6,    (UINT8) 4,   TRUE,    fnCheckInscHome },
        /* INDICIA */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 7,    (UINT8) 11,   TRUE,    fnIsIBILiteIndiciaSupport },
        /* TXT Msg */    
  //      {   USE_DEFAULT_ASSIGNMENT, (UINT8) 8,    (UINT8) 5,   TRUE,    fnCheckTextMessageHome },
        /* EKP/Task */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 9,    (UINT8) 12,  TRUE,    fnFrankTaskEKPEnable },
        /* Number */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 10,    (UINT8) 13,  OIC_SLOT_DISPLAY_ONLY,    fnFrankTaskEKPEnable },
        /* Piece ID */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 11,    (UINT8) 14,  TRUE,   fnFrankTaskEKPEnable },
        /* Reply Cd */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 12,    (UINT8) 15,  TRUE,    fnCheckReplyCodeHome },
        
        {   END_MENU_TABLE_MARKER,  (UINT8) 0,    (UINT8) 0,   FALSE,   NULL_PTR }  
    } ;

    // invoke generic slot initialization function with this menu initializer table
    // 
    ulNextSlotIndex = fnSetupMenuSlots( pHomeMenuInitTable, ulSlotIndex, 
                                        (UINT8) 2 ) ;
    
    // Set the total number of entries which will be use for displaying 
    // scroll bar
    fnSetMenuEntriesTotalNumber (ulNextSlotIndex - ulSlotIndex);

    
}

      
/* *************************************************************************
// FUNCTION NAME: 
//      fnGetNextDisplayedMessage
//
// DESCRIPTION: 
//      Scan the input status table, to get the index of next displayed string,
//      and its sequence number in all TRUE elements.
//
// PRE-CONDITIONS:
//      Assumed that there is at least one TRUE elements in the array, this 
//      should be checked before calling this function
//
// INPUTS:
//      pStatusTable    -   point ot a status table
//      usTableSize     -   size of the table
//      fInit           -   flag for checking whether this is the first time 
//                          to update.
//      pDisplayedIndex -   point to the index of current displayed string
//      pDisplayedSeqNo -   point to the sequence number
//
// OUTPUTS:
//      pDisplayedIndex -   point to the index of next displayed string
//      pDisplayedSeqNo -   point ot the sequence number of next displayed 
//                          string in all TRUE elements 
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  11/11/2005  Vincent Yi      Initial version
//
// *************************************************************************/
static void fnGetNextDisplayedMessage (BOOL   * pStatusTable,
                                       UINT16   usTableSize,
                                       BOOL     fInit,
                                       UINT16 * pDisplayedIndex,
                                       UINT16 * pDisplayedSeqNo)
{
    UINT16  usIndex;
    UINT16  usSeqNo;
    UINT16  usFirstTRUEIndex = 0;
    UINT16  usCurDisplayedIndex;
    BOOL    fGotNextIndex = FALSE;

    if (fInit == TRUE)
    {
        usCurDisplayedIndex = 0;
        usSeqNo = 1;
    }
    else
    {
        usCurDisplayedIndex = *pDisplayedIndex;
        usSeqNo  = *pDisplayedSeqNo;
    }
    
    for (usIndex = usCurDisplayedIndex; usIndex < usTableSize; usIndex++)
    {
        if (pStatusTable[usIndex] == TRUE)
        {
            if (fInit == TRUE)
            {   // For the update first time,  see the first TRUE element
                // as the next to display, and set sequence number to 1.
                fGotNextIndex = TRUE;
                *pDisplayedIndex = usIndex;
                *pDisplayedSeqNo = 1;
                break;  
            }
            if (usIndex == usCurDisplayedIndex)
            {   // skip current index
                continue;   
            }
            // The next TRUE element should be the one we need.
            fGotNextIndex = TRUE;
            *pDisplayedIndex = usIndex;
            *pDisplayedSeqNo = usSeqNo+1;
            break;  
        }
    }   // for (...)
        
    if (fGotNextIndex == FALSE)
    {   // if we didn't catch the next index during scanning above, the 
        // first TRUE element should be the one we need (looping case)
        for (usIndex = 0; usIndex<usCurDisplayedIndex; usIndex++)
        {
            if (pStatusTable[usIndex] == TRUE)
            {
                fGotNextIndex = TRUE;
                *pDisplayedIndex = usIndex;
                *pDisplayedSeqNo = 1;
                break;
            }
        }
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnUpdateDisablingWarningConditions
//
// DESCRIPTION: 
//      Private function to update the global disabling conditions and warning 
//      conditions value. 
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      This function not for seal-only mode
//
// MODIFICATION HISTORY:
//  07/20/2009  Raymond Shen    Let CM Paper error and Tape error share the same disabling condition 
//                              "DCOND_PAPER_ERROR" which says "Clear Printer transport".
//  07/17/2009  Jingwei,Li      Turn DCOND_ZIP_BARCODE_REQUIRED on for Package service.
//  06/23/2009  Jingwei,Li      Added "Paper in Feeder transport" case.
//  06/22/2007  Joey Cui        Block Low-Fund warning in some Print Mode
//  06/19/2007  D. Kohl         Add treatment for piece count end of life
//                              warning
//  08/10/2006  Dicky Sun       Update for Clear key and Home key in
//                              GTS screen.
//  07/07/2006  Dicky Sun       Update Disable checking for GTS
//  05/15/2006  Vincent Yi      Initial version
//
// *************************************************************************/
void fnUpdateDisablingWarningConditions (void)
{
    UINT8   ucMode;
    UINT8   ucSubMode;
    UINT8   ucPrintMode = fnOITGetPrintMode();
    ulong 	ulPsdState;
    UINT8       uiEolStat;
    unsigned long	lwTimeSyncStatus = 0;
    //    fnGetOITMode (&ucMode, &ucSubMode);
    fnGetSysMode(&ucMode, &ucSubMode);

    // Disabling conditions
    if (fnIsCmFatalError() == TRUE)
    {   // fatal error
        fnPostDisabledStatus(DCOND_PRINTER_FAULT);
    }
    if (fnIsCmTopCoverOpen() == TRUE)
    {   // top cover open
        fnPostDisabledStatus(DCOND_COVER_OPEN);
    }
    if (fnIsCmInkTankLidOpen() == TRUE)
    {   // ink tank lid open
        fnPostDisabledStatus(DCOND_INKTANK_LID_OPEN);
    }
    if ((fnIsCmPaperError() == TRUE) || (fnIsCmTapeError() == TRUE) )
    {   // paper error
        fnPostDisabledStatus(DCOND_PAPER_ERROR);
    }
    if (fnIsCmWasteTankFull() == TRUE)
    {   // waste tank full
        fnPostDisabledStatus(DCOND_WASTE_TANK_FULL);
    }
    if (fnIsCmPrintheadError() == TRUE)
    {   // no printhead, printhead id/checksum/sensor range/heater error
        fnPostDisabledStatus(DCOND_PRINTHEAD_ERROR);
        fNoPrintHead = fnIsCmNoPrinthead();
    }
    if (fnIsCmInkTankError() == TRUE)
    {   // no ink tank, ink tank id/checksum error
        fnPostDisabledStatus(DCOND_INKTANK_ERROR);
        fNoInkTank = fnIsCmNoInkTank();
    }
    if (fnIsCmInkOut())
    {   // ink out
        fnPostDisabledStatus(DCOND_INK_OUT);
    }
    if (fnIsCmJamLeverOpen() == TRUE)
    {   // jam lever open
        fnPostDisabledStatus(DCOND_TRANSPORT_OPEN);
    }
    if (fnIsCmPaperInTransport() == TRUE)
    {   // S1 is blocked
      UINT8 sMode;
      UINT8 sSubmode;

      fnGetSysMode(&sMode,&sSubmode);

      // In order to make tablet not display "Clear Paper Jam" message when a mailpiece is inserted at the same  
      // time as the transport running times out, ignore "paper in transport" error until maintenance is finished
      if(sMode == SYS_RUNNING && fnIsCmS2Blocked() == FALSE && fnIsCmS3Blocked() == FALSE)
      {
          fSkipPaperInTranport = TRUE;
      }

      if (fSkipPaperInTranport == FALSE)
      {       
          fnPostDisabledStatus(DCOND_PAPER_IN_TRANSPORT);
      }
    }
    if ((fnIsCmFeederError() == TRUE)   &&
        (fnOITGetBaseType() == OIT_BASE_400C))
    {   // Feeder fault
        fnPostDisabledStatus(DCOND2_FEEDER_FAULT);
    }
    if ((fnIsCmFeederCoverOpen() == TRUE)   &&
        (fnOITGetBaseType() == OIT_BASE_400C))
    {   // Feeder cover open
        fnPostDisabledStatus(DCOND2_FEEDER_COVER_OPEN);
    }
    if((fnIsWOWSensorBlocked() == TRUE)
            && ((ucPrintMode == PMODE_WOW) || (ucPrintMode == PMODE_WEIGH_1ST)))
    {// Paper in WOW transport
        fnPostDisabledStatus(DCOND_PAPER_IN_WOW_TRANSPORT);
    }


    if( fDCAPUploadReqShown == TRUE)
	{// Upload Required
		fnPostDisabledStatus(DCOND_DCAP_AUTO_UPLOAD_REQ);
	}

    if(lwPsdState == (UINT32)eIPSDSTATE_INCOMMUNICADO)
    {//No PSD
        fnPostDisabledStatus(DCOND_NO_PSD);
    }

    //ucWhyDoingOOB indicates OOB Testprint is printed or not
    if(fnCMOSSetupGetCMOSDiagnostics()->ucWhyDoingOOB == TRUE)
    {

/*
    	if(ucPrintMode == PMODE_MANUAL ||
    	        ucPrintMode == PMODE_TIME_STAMP ||
    	        ucPrintMode == PMODE_POSTAGE_CORRECTION  ||
    	        ucPrintMode == PMODE_DATE_CORRECTION ||
    	        ucPrintMode == PMODE_MANUAL_WGT_ENTRY)
*/
    	{

    		if(lwPsdState != (UINT32)eIPSDSTATE_INCOMMUNICADO)
			{
    			if(fInspectionRequiredShown == TRUE)
				{// Inspection Required
					fnPostDisabledStatus(DCOND_INSPECTION_REQUIRED);
				}
			}

			if ((lwPsdState < eIPSDSTATE_FULLPOSTAL) ||
					(lwPsdState == eIPSDSTATE_GERM_RSAKEYLOADED) )
			{
				fnPostDisabledStatus(DCOND_AUTHORIZATION_REQ);
			}

			if ((lwPsdState == eIPSDSTATE_WITHDRAWN) ||
							(lwPsdState == eIPSDSTATE_GERM_WITHDRAWN) )
			{
				fnPostDisabledStatus(DCOND_OUT_OF_SERVICE);
			}

			//Time difference check
			lwTimeSyncStatus = fnInfraGetTimeSyncStatus();
			if ( lwTimeSyncStatus & TIME_SYNC_MAX_DRIFT_ERR )
			{
				fnPostDisabledStatus(DCOND2_TIME_SYNC);
			}

			if( lwTimeSyncStatus & TIME_SYNC_NEGATIVE_DRIFT_LOCKOUT )
			{
				fnPostWarningStatus(WCOND_TIME_SYNC_NEGATIVE);
			}
    	}

    }
/*
    if ( fnIsIPSDEnabled() == FALSE )
    {
            fnPostDisabledStatus(DCOND2_PSD_DISABLED);
    }

    if ( fnOITCheckPSDMarriedStatus() == FALSE )
    {
        fnPostDisabledStatus(DCOND2_PSD_MISMATCH);
    }
*/

    if (NoEMDPresent == TRUE){
    	fnPostDisabledStatus(DCOND_NO_EMD);
    }

    if (NoGARFilePresent == TRUE){
    	fnPostDisabledStatus(DCOND_NO_GAR);
    }

    // Warning conditions
    if (fnIsCmInkLow() == TRUE)
    {   // ink low
        fnPostWarningStatus(WCOND_LOW_INK);
    }
    if (fnIsCmInkTankExpiring() == TRUE)
    {   // ink tank timeout
        fnPostWarningStatus(WCOND_INKTANK_EXPIRING);
    }
    if (fnIsCmWasteTankNearFull() == TRUE)
    {   // waste tank near full
        fnPostWarningStatus(WCOND_WASTETANK_NEAR_FULL);
    }

    if(lwPsdState != (UINT32)eIPSDSTATE_INCOMMUNICADO)
    {
		if (fLowFundsWarningPending == TRUE)
		{   // low funds warning
			if (! (ucPrintMode == PMODE_AD_ONLY         ||
				   ucPrintMode == PMODE_TIME_STAMP      ||
				   ucPrintMode == PMODE_AD_TIME_STAMP   ||
				   ucPrintMode == PMODE_PERMIT          ||
				   ucSubMode == OIT_REPORT              ||
				   ucSubMode == OIT_TEST_PATTERN))
			{
				fnPostWarningStatus(WCOND_LOW_FUNDS);
		        fnDumpStringToSystemLog( "LowFundsWarn post cond in fnUpdateDisablingWarningConditions" );
			}
		}
    }
    if (fLowPieceCountWarningPending == TRUE)
    {   // warning to indicate that the PSD is reaching its maximum number
        // of items printed
        fnPostWarningStatus(WCOND_END_OF_LIFE_WARNING);
    }

    if (fInspectionWarningShown == TRUE)
    {   // warning to indicate that the PSD is Inspection Due
        fnPostWarningStatus(WCOND_INSPECTION_DUE);
    }

    if (fDCAPUploadDueShown == TRUE)
    {   // warning to indicate that the PSD is Inspection Due
    	fnPostWarningStatus(WCOND_DCAP_UPLOAD_DUE);
    }

    if (fUploadInProgress == TRUE)
	{   // Disabling to indicate upload in progress
    	fnPostDisabledStatus(DCOND_UPLOAD_IN_PROGRESS);
	}

    if (fNoTabletConnection == TRUE)
    {
        fnPostDisabledStatus(DCOND_NO_TABLET_CONNECTION);
    }

    if(fTrmFatalError == TRUE)
	{
		fnPostDisabledStatus(DCOND_TRM_FATAL_ERROR);
	}

	if(bMidnightProcessingStart == TRUE)
	{
		fnPostDisabledStatus(DCOND_MIDNIGHT_OCCUR);
	}

    if(fFileSystemCorruption == TRUE)
    {
        fnPostDisabledStatus(DCOND_FILE_SYS_CORRUPTION);
    }
	
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnSealOnlyUpdateConditions
//
// DESCRIPTION: 
//      Private function to update the global disabling conditions after 
//      requesting CM status when seal only ready. 
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      This function not for seal-only mode
//
// MODIFICATION HISTORY:
//  07/20/2009  Raymond Shen    Let CM Paper error and Tape error share the same disabling condition 
//                              "DCOND_PAPER_ERROR" which says "Clear Printer transport".
//  05/15/2006  Vincent Yi      Initial version
//
// *************************************************************************/
static void fnSealOnlyUpdateConditions (void)
{
    if (fnIsCmFatalError() == TRUE) 
    {   // fatal error
        fnPostDisabledStatus(DCOND_PRINTER_FAULT);
    }
    if (fnIsCmTopCoverOpen() == TRUE) 
    {   // top cover open
        fnPostDisabledStatus(DCOND_COVER_OPEN);
    }
    if ( (fnIsCmPaperError() == TRUE) || (fnIsCmTapeError() == TRUE) )
    {   // paper error
        fnPostDisabledStatus(DCOND_PAPER_ERROR);
    }
    if (fnIsCmJamLeverOpen() == TRUE) 
    {   // jam lever open
        fnPostDisabledStatus(DCOND_TRANSPORT_OPEN);
    }
    if (fnIsCmPaperInTransport() == TRUE) 
    {   // S1 is blocked
        fnPostDisabledStatus(DCOND_PAPER_IN_TRANSPORT);
    }
    if ((fnIsCmFeederError() == TRUE)   &&
        (fnOITGetBaseType() == OIT_BASE_400C))
    {
        fnPostDisabledStatus(DCOND2_FEEDER_FAULT);
    }
    if ((fnIsCmFeederCoverOpen() == TRUE)   &&
        (fnOITGetBaseType() == OIT_BASE_400C))
    {
        fnPostDisabledStatus(DCOND2_FEEDER_COVER_OPEN);
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnUpdateWarningStatus
//
// DESCRIPTION: 
//      Private function to update the warning status array value and 
//      the total warnings number.
//
// INPUTS:
//      None
//
// OUTPUTS:
//      None
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/01/2007  Joey Cui        Update for adding barcode & record warning
//  06/15/2007  D. Kohl         Update for adding end_of_life warning
//  05/10/2007  Raymond Shen    Update for adding TransLog and BusinessMgr check.
//  12/14/2006  Adam Liu        Update for adding DCAP check
//  05/19/2006  James Wang      Update wanring status with a new BR function.
//  11/14/2005  Vincent Yi      Initial version
//
// *************************************************************************/
static void fnUpdateWarningStatus(void)
{
    // Initialize the array pWarningsStatus
    memset (pWarningsStatus, 0, sizeof(pWarningsStatus));
    usTotalWarnings = 0;
    
    // Check all warning conditions, if occur, set the element which has 
    // the same index in pWarningsStatus as the field index to TRUE

    if (fnIsWarningStatusSet(WCOND_INSPECTION_DUE) == TRUE)
    {
        pWarningsStatus[WARNING_STR_INSPECTION_DUE] = TRUE;
        usTotalWarnings++;
    }

    if (fnIsWarningStatusSet(WCOND_LOW_FUNDS) == TRUE)
    {
        pWarningsStatus[WARNING_STR_LOW_FUNDS] = TRUE;
        usTotalWarnings++;  
    }

    if (fnIsWarningStatusSet(WCOND_LOW_INK ) == TRUE)
    {
        pWarningsStatus[WARNING_STR_LOW_INK] = TRUE;
        usTotalWarnings++;
    }

    if (fnIsWarningStatusSet(WCOND_INKTANK_EXPIRING) == TRUE)
    {
        pWarningsStatus[WARNING_STR_INK_TANK_EXPIRING] = TRUE;
        usTotalWarnings++;
    }

    if (fnIsWarningStatusSet(WCOND_WASTETANK_NEAR_FULL) == TRUE)
    {
        pWarningsStatus[WARNING_STR_WASTE_TANK_NEAR_FULL] = TRUE;
        usTotalWarnings++;
    }

    if (fnIsWarningStatusSet(WCOND_LOW_BATTERY_PSD) == TRUE)
    {
        pWarningsStatus[WARNING_STR_PSD_BATTERY_LOW] = TRUE;
        usTotalWarnings++;
    }

    if (fnIsWarningStatusSet(WCOND_DCAP_UPLOAD_DUE) == TRUE)
    {
        pWarningsStatus[WARNING_STR_DCAP_UPLOAD_DUE] = TRUE;
        usTotalWarnings++;
    }

    if (fnIsWarningStatusSet(WCOND_TRANS_LOG_NEAR_FULL) == TRUE)
    {
        pWarningsStatus[WARNING_STR_TRANS_LOG_NEAR_FULL] = TRUE;
        usTotalWarnings++;
    }

    if (fnIsWarningStatusSet(WCOND_ABACUS_HOST_DISCONNECTED) == TRUE)
    {
        pWarningsStatus[WARNING_STR_ABACUS_HOST_DISCONNECTED] = TRUE;
        usTotalWarnings++;
    }

    if (fnIsWarningStatusSet(WCOND_END_OF_LIFE_WARNING) == TRUE)
    {
        pWarningsStatus[WARNING_STR_END_OF_LIFE_WARNING] = TRUE;
        usTotalWarnings++;
    }

    if (fnIsWarningStatusSet(WCOND_REMAINED_BARCODE_LOW) == TRUE)
    {
        pWarningsStatus[WARNING_STR_BARCODE_LOW] = TRUE;
        usTotalWarnings++;
    }

    if (fnIsWarningStatusSet(WCOND_TRACKING_LOG_NEAR_FULL) == TRUE)
    {
        pWarningsStatus[WARNING_STR_TRACKING_LOG_NEAR_FULL] = TRUE;
        usTotalWarnings++;
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnUpdateWarningMessages
//
// DESCRIPTION: 
//      Private function to update the warning messages, and cycling display 
//      warning strings.
//
// INPUTS:
//      fInit   -   flag for checking whether this is the first time to update.
//
// OUTPUTS:
//      usTotalWarnings         - the total number of occurred warnings
//      usDisplayedWarningIndex - the index of current displayed warning in 
//                                  the FieldText list
//      usDisplayedWarningSeqNo - the sequence number of current displayed 
//                                  warning in all occurred warnings
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/16/2007  Joey Cui        Keep warning messages scroll in running screen
//  05/19/2006  James Wang      Update wanring status with a new BR function.
//  11/14/2005  Vincent Yi      Initial version
//
// *************************************************************************/
void fnUpdateWarningMessages (BOOL fInit)
{
    if (fInit == TRUE)
    {
        fnUpdateWarningStatus();

        if (usTotalWarnings > 0)
        {
            if (fInRunningScreen == TRUE)
            {
                fnGetNextDisplayedMessage (pWarningsStatus, 
                                           MAX_WARNING_NUM,
                                           FALSE,
                                           &usDisplayedWarningIndex, 
                                           &usDisplayedWarningSeqNo);
            }
            else
            {
                fnGetNextDisplayedMessage (pWarningsStatus, 
                                           MAX_WARNING_NUM,
                                           fInit,
                                           &usDisplayedWarningIndex, 
                                           &usDisplayedWarningSeqNo);
            }
        }
    } // if (fInit == TRUE)
    else
    {
        if (usTotalWarnings > 1)
        {
            fnGetNextDisplayedMessage (pWarningsStatus, 
                                       MAX_WARNING_NUM,
                                       fInit,
                                       &usDisplayedWarningIndex, 
                                       &usDisplayedWarningSeqNo);
        }
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnUpdateErrorStatus
//
// DESCRIPTION: 
//      Private function to update the error status array value and the 
//      total errors number.
//
// INPUTS:
//      None
//
// OUTPUTS:
//      None
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  06/23/2009  Jingwei,Li      Added "Paper in Feeder transport" case.
//  09/25/2007  Vincent Yi      Commented out case ACT_NO_ACCOUNT_SELECTED to not
//                              display "Select Account" at footer area, it have
//                              displayed next to "Account:" if necessary.
//  08/09/2007  Oscar Wang      Added "Postage Value Too Low" error message.
//  06/19/2007  D. Kohl         Add treatment for piece count end of life
//                              error
//  06/14/2007  I. Le Goff      Added "Enter Batch" error message.
//  06/04/2007  Oscar Wang      Added "Enter DUNS Number" error message.
//  03/27/2007  Adam Liu        Added Abacus related error message string
//  12/15/2006  Adam Liu        Added ERROR_STR_INFRA_UPDATE_REQUIRED
//  12/14/2006  Adam Liu        Update for adding DCAP check
//  07/20/2006  Vincent Yi      Update error status for Meter Not Authorized
//  07/07/2006  Dicky Sun       Update Error status for GTS
//  05/19/2006  James Wang      Update error status with a new BR function.
//  11/14/2005  Vincent Yi      Initial version
//
// *************************************************************************/
static void fnUpdateErrorStatus(void)
{
    UINT8 ucPrintMode;

    // Initialize the array pErrorsStatus
    memset (pErrorsStatus, 0, sizeof(pErrorsStatus));
    usTotalErrors = 0;
    
    // Check all error conditions, if occur, set the element which has the 
    // same index in pErrorsStatus as the field index to TRUE

    if (fnIsDisabledStatusSet(DCOND_PAPER_IN_TRANSPORT) == TRUE)
    {   
        pErrorsStatus[ERROR_STR_PAPER_IN_TRANSPORT] = TRUE;
        usTotalErrors++;    
    }
    if (fnIsDisabledStatusSet(DCOND_INKTANK_ERROR) == TRUE)
    {
        if (fNoInkTank == TRUE)
        {
            pErrorsStatus[ERROR_STR_NO_INK_TANK] = TRUE;
        }
        else
        {
            pErrorsStatus[ERROR_STR_INK_TANK_ERROR] = TRUE;
        }
        usTotalErrors++;    
    }
    if (fnIsDisabledStatusSet(DCOND_PRINTHEAD_ERROR) == TRUE)
    {
        if (fNoPrintHead == TRUE)
        {
            pErrorsStatus[ERROR_STR_NO_PRINTHEAD] = TRUE;
        }
        else
        {   // Other print head errors  ...
            pErrorsStatus[ERROR_STR_PRINTHEAD_ERROR] = TRUE;
        }
        usTotalErrors++;    
    }
    if (fnIsDisabledStatusSet(DCOND_INSUFFICIENT_FUNDS) == TRUE)
    {
        pErrorsStatus[ERROR_STR_INSUFFICIENT_FUNDS] = TRUE;
        usTotalErrors++;    
    }
    if (fnIsDisabledStatusSet(DCOND2_FEEDER_COVER_OPEN) == TRUE)
    {   
        pErrorsStatus[ERROR_STR_FEEDER_COVER_OPEN] = TRUE;
        usTotalErrors++;    
    }
    if (fnIsDisabledStatusSet(DCOND_INK_OUT) == TRUE)
    {
        pErrorsStatus[ERROR_STR_INK_OUT] = TRUE;
        usTotalErrors++;    
    }
    if (fnIsDisabledStatusSet(DCOND_WASTE_TANK_FULL) == TRUE)
    {
        pErrorsStatus[ERROR_STR_WASTE_TANK_FULL] = TRUE;
        usTotalErrors++;    
    }
    if (fnIsDisabledStatusSet(DCOND_OUT_OF_SERVICE) == TRUE)
    {   
        pErrorsStatus[ERROR_STR_OUT_OF_SERVICE] = TRUE;
        usTotalErrors++;    
    }
    if (fnIsDisabledStatusSet(DCOND_INSPECTION_REQUIRED) == TRUE)
    {   
        pErrorsStatus[ERROR_STR_INSPECTION_REQUIRED] = TRUE;
        usTotalErrors++;    
    }
    if (fnIsDisabledStatusSet(DCOND_PRINTER_FAULT) == TRUE)
    {
        pErrorsStatus[ERROR_STR_PRINTER_ERROR] = TRUE;
        usTotalErrors++;    
    }
    if (fnIsDisabledStatusSet(DCOND_INKTANK_LID_OPEN) == TRUE)
    {
        pErrorsStatus[ERROR_STR_TANK_LID_OPEN] = TRUE;
        usTotalErrors++;    
    }
    if (fnIsDisabledStatusSet(DCOND_TRANSPORT_OPEN) == TRUE)
    {
        pErrorsStatus[ERROR_STR_JAM_LEVER_OPEN] = TRUE;
        usTotalErrors++;    
    }

    if(fnIsDisabledStatusSet(DCOND_ZIP_BARCODE_REQUIRED) == TRUE)
    {
        pErrorsStatus[ERROR_ZIP_BARCODE_REQUIRE] = TRUE;
        usTotalErrors++;    
    }
    if (fnIsDisabledStatusSet(DCOND_AUTHORIZATION_REQ) == TRUE)
    {
        pErrorsStatus[ERROR_STR_AUTHR_REQUIRED] = TRUE;
        usTotalErrors++;    
    }
    if ((fnIsDisabledStatusSet(DCOND_TC_REQUIRED) == TRUE)  || 
        (fnIsDisabledStatusSet(DCOND_AD_REQUIRED) == TRUE)  || 
        (fnIsDisabledStatusSet(DCOND_RPT_REQUIRED) == TRUE)  || 
        (fnIsDisabledStatusSet(DCOND_GRAPHIC_REQUIRED) == TRUE))
    {
        pErrorsStatus[ERROR_STR_MISSING_GRAPHIC] = TRUE;
        usTotalErrors++; 
    }
    
    if ((fnIsDisabledStatusSet(DCOND_DCAP_MANUAL_UPLOAD_REQ) == TRUE) ||
       (fnIsDisabledStatusSet(DCOND_DCAP_AUTO_UPLOAD_REQ) == TRUE)) 
    {
        pErrorsStatus[ERROR_STR_DCAP_UPLOAD_REQUIRED] = TRUE;
        usTotalErrors++;    
    }    
    
    if (fnIsDisabledStatusSet(DCOND2_INFRA_UPDATE_REQUIRED) == TRUE)
    {
        pErrorsStatus[ERROR_STR_INFRA_UPDATE_REQUIRED] = TRUE;
        usTotalErrors++;    
    }    

    if (fnIsDisabledStatusSet(DCOND_WEIGHT_PROBLEM) == TRUE)
    {
        ucPrintMode = fnOITGetPrintMode();
        if (ucPrintMode == PMODE_PLATFORM || ucPrintMode == PMODE_AJOUT_PLATFORM)
        {
            // ZWZP in platform mode
            pErrorsStatus[ERROR_STR_ZWZP_PLACE_ITEM] = TRUE;
        }
        else //if (fnOITGetPrintMode() == PMODE_MANUAL_WGT_ENTRY)
        {
            // ZWZP in manual weight entry mode
            pErrorsStatus[ERROR_STR_ZWZP_ENTER_WEIGHT] = TRUE;
        }
        usTotalErrors++;    
    }

    if (fnIsDisabledStatusSet(DCOND_ACCOUNT_PROBLEM) == TRUE)
    {
/*
        switch (fnGetAccountDisabledStatus())
        {

            case ACT_PERIOD_EXPIRED:
                pErrorsStatus[ERROR_STR_ACCT_PERIOD_ENDED] = TRUE;
                usTotalErrors++; 
                break;

            case ACT_PERIOD_INVALID:
                pErrorsStatus[ERROR_STR_ACCT_PERIOD_INVALID] = TRUE;
                usTotalErrors++; 
                break;

           case ACT_NO_ACCOUNT_SELECTED:
                ucPrintMode = fnOITGetPrintMode();
                switch( ucPrintMode )
                {
                    case PMODE_TIME_STAMP:
                    case PMODE_AD_TIME_STAMP:
                    case PMODE_AD_ONLY:
                pErrorsStatus[ERROR_STR_SELECT_ACCOUNT] = TRUE;
                usTotalErrors++; 
                break;

                    default:
                        // Other modes display it on the "Account: " line, 
                        //  instead of the error line.
                        break;
                }
                break;


            case ACT_ABACUS_XACTION_LOG_FULL:
                pErrorsStatus[ERROR_STR_TRANSACTION_LOG_FULL] = TRUE;
                usTotalErrors++; 
                break;

            

            case ACT_ABACUS_JOBID_1_REQUIRED:
                if(fnGetAccountingType() == (UINT8) ACT_SYS_TYPE_HOSTED_ABACUS)
                {
                    pErrorsStatus[ERROR_STR_SELECT_JOB_ID_1] = TRUE; 
                }
                else if (pReadOnlyAGDGlobal->rPCN.ucMaxUserFields > 1)
                {
                    pErrorsStatus[ERROR_STR_ENTER_SELECT_JOB_ID_1] = TRUE;
                }
                else
                {
                    pErrorsStatus[ERROR_STR_ENTER_SELECT_JOB_ID] = TRUE;
                }
                usTotalErrors++;
                break;



            case ACT_ABACUS_JOBID_2_REQUIRED:
                pErrorsStatus[ERROR_STR_ENTER_SELECT_JOB_ID_2] = TRUE;
                usTotalErrors++; 
                break;



            default:        // lint 11/06/03
                break;
            
        }
*/
    }

    
    if (fnIsDisabledStatusSet(DCOND_AGD_SETUP_FAILED) == TRUE)
    {
        pErrorsStatus[ERROR_STR_AGD_SETUP_FAILED] = TRUE;
        usTotalErrors++;    
    } 

    if (fnIsDisabledStatusSet(DCOND_ENTER_DUNS_NUMBER) == TRUE)
    {
        pErrorsStatus[ERROR_STR_ENTER_DUNS_NUMBER] = TRUE;
        usTotalErrors++;    
    } 
    

    if (fnIsDisabledStatusSet(DCOND2_PIECE_COUNT_EOL) == TRUE)
    {
        pErrorsStatus[ERROR_STR_PIECE_COUNT_EOL] = TRUE;
        usTotalErrors++;    
    }

    if (fnIsDisabledStatusSet(DCOND_ZERO_MANUAL_WEIGHT) == TRUE)
    {
        pErrorsStatus[ERROR_STR_ZERO_MANUAL_WEIGHT] = TRUE;
        usTotalErrors++;    
    }    

    if (fnIsDisabledStatusSet(DCOND_PAPER_IN_WOW_TRANSPORT) == TRUE)
    {
        pErrorsStatus[ERROR_STR_PAPER_IN_WOW_TRANSPORT] = TRUE;
        usTotalErrors++;    
    }  
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnUpdateErrorMessages
//
// DESCRIPTION: 
//      Event function to update the error messages, and cycling display 
//      error strings.
//
// INPUTS:
//      fInit   -   flag for checking whether this is the first time to update.
//
// OUTPUTS:
//      usTotalErrors           - the total number of occurred errors
//      usDisplayedErrorIndex   - the index of current displayed error in 
//                                  the FieldText list
//      usDisplayedErrorSeqNo   - the sequence number of current displayed 
//                                  error in all occurred errors
//
// RETURNS:
//      No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/19/2006  James Wang      Update error status with a new BR function.
//  11/14/2005  Vincent Yi      Initial version
//
// *************************************************************************/
void fnUpdateErrorMessages(BOOL fInit) 
{
    if (fInit == TRUE)
    {
        fnUpdateErrorStatus();

        if (usTotalErrors > 0)
        {
            fnGetNextDisplayedMessage (pErrorsStatus, 
                                       MAX_ERROR_NUM,
                                       fInit,
                                       &usDisplayedErrorIndex, 
                                       &usDisplayedErrorSeqNo);
        }
    } // if (fInit == TRUE)
    else
    {
        if (usTotalErrors > 1)
        {
            fnGetNextDisplayedMessage (pErrorsStatus, 
                                       MAX_ERROR_NUM,
                                       fInit,
                                       &usDisplayedErrorIndex, 
                                       &usDisplayedErrorSeqNo);
        }
    }
}

/* *************************************************************************
// FUNCTION NAME: fnOITCheckPSDMarriedStatus
// DESCRIPTION: Check to see if the current PSD marrying to current meter is legal or not
//
// AUTHOR: Yingbo Zhang
//
// INPUTS:  None
// OUTPUTS: TRUE: Yes , it's legal, including the condition that we don't need check this for non-French meters
//                 FALSE: illegal
//
// MODIFICATION HISTORY:
//              Yingbo Zhang    Initial version.
//  05/19/2006  James Wang      To get UIC/PSD marriage state and get the 
//                              vault serial number with some new BR functions.
// *************************************************************************/
static BOOL fnOITCheckPSDMarriedStatus(void)
{   
    #define PSD_MAX_SERIAL_NO_SZ 16

    BOOL retval = TRUE;
    char pSN[PSD_MAX_SERIAL_NO_SZ+1];
    
    /* this UIC/PSD marriage check is only for specified country's meters, e.g. French meter */
    uchar WhetherOrNotToMarry = fnGetUicPsdMarriedState();

    if(!fnSYSIsPSDDetected())
    {
        retval = TRUE;
        goto xit;
    }
        
    if(!WhetherOrNotToMarry)
    {
        retval = TRUE;
        goto xit;
    }

    else/* check it now */
    {
        if(fnGetVaultSerialNum(pSN) != TRUE)
        {
            goto xit;
        }

        if(strlen(marriedVaultNumber)) /*It's not NULL*/
        {
            if( strcmp( (char*)&marriedVaultNumber[0], (char*)pSN ) != 0)
            {
                retval = FALSE;
                goto xit;
            }

        }
    }

xit:
    return((BOOL)(retval));

}

/* *************************************************************************
// FUNCTION NAME: 
//      fncPermitModeCheck
//
// DESCRIPTION: 
//      Continuation function that continues the Permit 
//      checking after requesting CM status.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr, depends on the conditions checking
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  03/13/2006  Adam Liu        Initial version 
//
// *************************************************************************/
static SINT8 fncPermitModeCheck ( void       (** pContinuationFcn)(), 
                                         UINT32         *   pMsgsAwaited,
                                         T_SCREEN_ID    *   pNextScr )
{
    SINT8 ucRetval = PREPOST_COMPLETE;

    *pNextScr = 0;

    *pNextScr = fnPermitModeConditionsCheck2();

    if (*pNextScr == 0)
    {
        // Prepare the menu table
        fnPermitModeMenuInit(TRUE);
        // Check all warning conditions for displaying the warning messages and
        // counter at the first time
        fnUpdateWarningMessages(TRUE);
    }

    else if (*pNextScr == GetScreen (SCREEN_PERMIT_ERROR))
    {   
        // Prepare the menu table
        fnPermitModeMenuInit(TRUE);
        // Check all error conditions for displaying the error messages and
        // counter at the first time
        fnUpdateErrorMessages(TRUE);
    }

    return ucRetval;        
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnPermitModeConditionsCheck
//
// DESCRIPTION: 
//      Private function to check warning & disabling conditions in 
//      fnpPermitModeCheck()
//
// INPUTS:
//      None
//
// RETURNS:
//      Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/18/2008  Raymond Shen    Added code to check PSD/CMOS battery life.
//  12/03/2007  Oscar Wang      Renamed ESERVICE_UPLOAD_RECORD_PROMPT to 
//                              ESERVICE_HOME_SCRN_UPLOAD_PROMPT.
//  01/19/2007  Oscar Wang      Fix FRACA GMSE00113167: check unsent EService
//                              record first.
//  07/20/2006  Vincent Yi      Added account problem checking
//  03/13/2006  Adam Liu        Initial version
//
// *************************************************************************/
// VBL_TBD_FP
static T_SCREEN_ID fnPermitModeConditionsCheck(void)
{
    T_SCREEN_ID usNextScreen = 0;

    UINT32      ulPostageValue;
    UINT8       bBobErrorClass;
    UINT8       bBobErrorCode;
    UINT8       pDescRegValue[SPARK_MONEY_SIZE];
    UINT8       bPrintMode;


    if((usNextScreen = fnCheckInspection()) != 0)
    {
        return usNextScreen;
    }

    /* check for missing graphics */
    if((usNextScreen = fnCheckMissingGraphics()) != 0)
    {
        return usNextScreen;
    }

    /* check for postage settings with funds and PSD stuff */
    /* check that there are sufficient funds to cover the currently set postage value */
    if((usNextScreen = fnCheckPSDState()) != 0)
    {
        return usNextScreen;
    }

    // Check PSD/UIC battery state.
    if((usNextScreen = fnCheckBatteryState()) != 0)
    {
        return usNextScreen;
    }

    // Check accounting stuff
/*
    if (fnGetAccountDisabledStatus() != ACT_SUCCESS)
    {
        fnPostDisabledStatus(DCOND_ACCOUNT_PROBLEM);
    }
*/

    /* Check if any Confirmation Service Records are pending */
    if ( fnGetRecordsPendingFlag() )   
    {
        usNextScreen = GetScreen(ESERVICE_HOME_SCRN_UPLOAD_PROMPT);
        return usNextScreen;
    }

xit:
    return (usNextScreen);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnPermitModeConditionsCheck2
//
// DESCRIPTION: 
//      Private function to check warning & disabling conditions after 
//      requesting CM status 
//
// INPUTS:
//      None
//
// RETURNS:
//      Next screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  06/19/2007  D. Kohl         Add treatment for piece count end of life
//                              error
//  05/18/2007  Joey Cui        Add error checking for feeder_cover_open
//  12/15/2006  Adam Liu        Update for "Update Required" check
//  03/13/2006  Adam Liu        Initial version
//
// *************************************************************************/
// VBL_TBD_FP
static T_SCREEN_ID fnPermitModeConditionsCheck2(void)
{
    T_SCREEN_ID usNextScreen = 0;
    UINT16      wImageID;

    fnUpdateDisablingWarningConditions();
    if ((usNextScreen = fnPostCheckCMConditions()) != 0)
    {
        return usNextScreen;
    }
    if ((usNextScreen = fnCheckPSOCDisablingConditions()) != 0)
    {
        return usNextScreen;
    }

    /* the priority of the various disabling and other conditions is established by the order of
        the processing below */

    /* if necessary, show the screen that tells the user the time has been updated for
        daylight savings time (either ending or beginning) */
    if (fCMOSDSTAutoUpdated)
    {
        usNextScreen = GetScreen(DST_AUTO_CHANGE_OCCURRED_SCRN);       
       return usNextScreen;
    }

    /* set the scale location code if the code is pending */
/* Adam
    if (fSetScaleLocCodePending == TRUE)
    {
        *pNextScr = GetScreen(SCREEN_SET_SCALE_CODE);
        goto xit;
    }

    // The DCAPI may need to write to flash.
    if (lwDisablingConditions & DCOND_FLASH_WRITE_REQ)
    {
        *pNextScr = GetScreen(SCREEN_DCAP_BACKUP);
        goto xit;
    }

    // Check if any Confirmation Service Records are pending
    if ( fnGetRecordsPendingFlag() )   
    {
        *pNextScr = GetScreen(ESERVICE_HOME_SCRN_UPLOAD_PROMPT);
        goto xit;
    }
*/

    if((usNextScreen = fnPostCheckInspection()) != 0)
    {
        return usNextScreen;
    }

    if((usNextScreen = fnPostCheckEndOfLife()) != 0)
    {
        return usNextScreen;
    }

    // Errors to be displayed in Home screen should be checked here
    if ((fnIsDisabledStatusSet(DCOND_INSUFFICIENT_FUNDS) == TRUE)  ||
        (fnIsDisabledStatusSet(DCOND_INK_OUT) == TRUE)             ||
        (fnIsDisabledStatusSet(DCOND_ACCESSING_PM_NVM) == TRUE)    ||    
        (fnIsDisabledStatusSet(DCOND_INKTANK_ERROR) == TRUE)       ||
        (fnIsDisabledStatusSet(DCOND_INKTANK_LID_OPEN) == TRUE)    ||
        (fnIsDisabledStatusSet(DCOND_WASTE_TANK_FULL) == TRUE)     ||
        (fnIsDisabledStatusSet(DCOND_PRINTHEAD_ERROR) == TRUE)     ||
        (fnIsDisabledStatusSet(DCOND_PRINTER_FAULT) == TRUE)       ||
        (fnIsDisabledStatusSet(DCOND_PAPER_ERROR) == TRUE)         ||
        (fnIsDisabledStatusSet(DCOND_PAPER_IN_TRANSPORT) == TRUE)  ||
        (fnIsDisabledStatusSet(DCOND_TRANSPORT_OPEN) == TRUE)      ||
        (fnIsDisabledStatusSet(DCOND_INSPECTION_REQUIRED) == TRUE) ||        
        (fnIsDisabledStatusSet(DCOND_AUTHORIZATION_REQ) == TRUE)   ||
        (fnIsDisabledStatusSet(DCOND_OUT_OF_SERVICE) == TRUE)      ||
        (fnIsDisabledStatusSet(DCOND2_FEEDER_COVER_OPEN) == TRUE)  ||
        (fnIsDisabledStatusSet(DCOND2_PIECE_COUNT_EOL) == TRUE)    ||
        (fnIsDisabledStatusSet(DCOND2_INFRA_UPDATE_REQUIRED) == TRUE))
    {
  
        if (fnOITGetPrintMode() == PMODE_PERMIT)
        {
             usNextScreen = GetScreen(SCREEN_PERMIT_ERROR);
        }        
        goto xit;
    }

    /* check for permit selection */
    switch (fnOITGetPrintMode())
    {
        case PMODE_PERMIT:
            // a Permit must be selected before we go to ready to print it.
            if (fnAIGetActive(PERMIT_COMP_TYPE, &wImageID) != SUCCESSFUL)
            {
                fnProcessSCMError();
                goto xit;
            }

            if (wImageID == 0)
            {
                usNextScreen = GetScreen(SCREEN_PERMIT_ERROR);
                goto xit;
            }
            break;
            
        default:
            break;
    }
    
    /* errors that can be cleared by changing a setting */
    if (fnIsDisabledStatusSet(DCOND_ACCOUNT_PROBLEM) == TRUE)
    {

        if (fnOITGetPrintMode() == PMODE_PERMIT)
        {
            usNextScreen = GetScreen(SCREEN_PERMIT_ERROR);
        }
        else
        {   // Needs account
            usNextScreen = GetScreen (SCREEN_MM_INDICIA_ERROR);
        }
        return usNextScreen;
    }  

    /* warnings */

xit:
    return (usNextScreen);

}

/* *************************************************************************
// FUNCTION NAME: 
//      fnPermitModeMenuInit
//
// DESCRIPTION: 
//      Private function to prepare Permit Mode screen menu.
//
// INPUTS:
//      None
//
// WARNINGS/NOTES:
//      Menu possibilities:
//      1. Account
//      2. Permit
//      3. Ad
//      4. Inscription
//      5. Text Message
//      6. Allow print Count
//      7. Allow print Date
//      8. Allow print Town Circle
//      9. Zero Permit Count
//     10. View Permit Report
//
// MODIFICATION HISTORY:
//  11/21/2008  Joey Cui      Added full auto inscription support for French.
//  03/13/2006  Adam Liu      Initial version
//  09/11/2006  Adam Liu      Restore saved PermitScreenPage No.
// *************************************************************************/
static void fnPermitModeMenuInit (BOOL fUpdatePageLEDs)
{
    UINT32      ulSlotIndex = 0;
    UINT32      ulNextSlotIndex;
    UINT8       autoInscState, ucStatus, ucIndex;
    BOOL        fEnabled = FALSE;

	// can't be made "const" because the value of the "Active Flag" needs to be changed.
    static S_MENU_SETUP_ENTRY   pPermitModeMenuInitTable[] = 
    {
        // force menu table reset before defining new slots
        {   CLEAR_MENU_TABLE,   (UINT8) 0,  (UINT8) 0,  FALSE,  NULL_PTR },

        // Display number           Key           Label        Active   Conditional 
        // offset,                  assignment,   string id,   flag,    function
                                    
        /* Account */
//        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 1,    (UINT8) 1,   TRUE,    fnCheckAccountEnabled },
        /* Permit */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 2,    (UINT8) 2,   TRUE,    NULL_PTR },
        /* AD */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 3,   TRUE,    fnIsAdAllowedInPermitMode },
        /* INS */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 4,    (UINT8) 4,   TRUE,    fnIsInscAllowedInPermitMode },
        /* TXT Msg */    
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 5,    (UINT8) 5,   TRUE,    fnIsTxtMsgAllowedInPermitMode },   
        /* Allow print Count */        
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 6,    (UINT8) 6,   TRUE,    fnCanPermitCountOnOff },     
        /* Allow print Date */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 7,    (UINT8) 7,   TRUE,    fnCanPermitDateOnOff },  
        /* Allow print Town Circle */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 8,    (UINT8) 8,   TRUE,    fnCanPermitTownCircleOnOff },
        /* Zero Permit Count */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 9,    (UINT8) 9,   TRUE,    NULL_PTR },
        /* View Permit Report */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 10,    (UINT8) 10,  TRUE,    NULL_PTR },

        {   END_MENU_TABLE_MARKER,  (UINT8) 0,    (UINT8) 0,   FALSE,   NULL_PTR }  
    } ;

    autoInscState = fnFlashGetAutoInscrType();
    if(IsFeatureEnabled(&fEnabled, AUTO_AD_INSCR_SUPPORT, &ucStatus, &ucIndex) && fEnabled &&
        ((FULL_AUTO_INSCRIPTION_VIA_DCAP == autoInscState) || (FULL_AUTO_INSCRIPTION_VIA_RATES == autoInscState)) )
    {
          pPermitModeMenuInitTable[4].bSlotActivity = OIC_SLOT_DISPLAY_ONLY;
    }
    else
    {
          pPermitModeMenuInitTable[4].bSlotActivity = OIC_SLOT_ACTIVE;
    }

    // invoke generic slot initialization function with this menu initializer table
    ulNextSlotIndex = fnSetupMenuSlots( pPermitModeMenuInitTable, ulSlotIndex, 
                                        (UINT8) 3 ) ;
    
    // Set the total number of entries which will be use for displaying 
    // scroll bar
    fnSetMenuEntriesTotalNumber (ulNextSlotIndex - ulSlotIndex);

    fnRestorePermitScreenPageNo();
    
    // Update LED indicator
    if (fUpdatePageLEDs == TRUE)
    {
        fnUpdateLEDPageIndicator();
    }
}

/* *************************************************************************
 FUNCTION NAME: fnIsAdAllowedInPermitMode
 AUTHOR: David Huang
 DESCRIPTION: Utility function used to check if AD is allowed in permit mode 

  PARAMETERS   : None, to meet the type defined in pre function table
  RETURN       : TRUE if AD allowed
  NOTES     : None
***************************************************************************/

BOOL fnIsAdAllowedInPermitMode( void )
{
    BOOL fRetVal = FALSE;

    if ( fnFlashGetByteParm(BP_MAX_AD_SLOGAN) > 0
        && fnCheckIfUserSelectGraphicAllowedInMode(BOB_PERMIT_MODE, IMG_ID_AD_SLOGAN) == TRUE )
    {
        fRetVal = TRUE;
    }
    return ( fRetVal );
}

/* *************************************************************************
 FUNCTION NAME: fnIsInscAllowedInPermitMode
 AUTHOR: David Huang
 DESCRIPTION: Utility function used to check if Inscription is allowed in permit mode 

  PARAMETERS   : None, to meet the type defined in pre function table
  RETURN       : TRUE if Inscription allowed
  NOTES     : None
***************************************************************************/

BOOL fnIsInscAllowedInPermitMode( void )
{
    BOOL fRetVal = FALSE;

    if ( fnFlashGetByteParm(BP_INSC_SUPPORTED) != 0
        && fnCheckIfUserSelectGraphicAllowedInMode(BOB_PERMIT_MODE, IMG_ID_INSCRIPTION) == TRUE )
    {
        fRetVal = TRUE;
    }
    return ( fRetVal );
}

/* *************************************************************************
 FUNCTION NAME: fnIsTxtMsgAllowedInPermitMode
 AUTHOR: David Huang
 DESCRIPTION: Utility function used to check if Text msg is allowed in permit mode 

  PARAMETERS   : None, to meet the type defined in pre function table
  RETURN       : TRUE if text msg allowed
  NOTES     : None
***************************************************************************/

BOOL fnIsTxtMsgAllowedInPermitMode( void )
{
    BOOL fRetVal = FALSE;

    if ( fnIsTextMessageSupported()
        && fnCheckIfUserSelectGraphicAllowedInMode(BOB_PERMIT_MODE, IMG_ID_TEXT_ENTRY) == TRUE )
    {
        fRetVal = TRUE;
    }
    return ( fRetVal );
}


/* *************************************************************************
 FUNCTION NAME: fnCanPermitCountOnOff
 AUTHOR: David Huang
 DESCRIPTION: Utility function used to check if user can toggle Permit Count

  PARAMETERS   : None, to meet the type defined in pre function table
  RETURN       : TRUE if user can toggle permit count on or off
  NOTES     : None
***************************************************************************/

BOOL fnCanPermitCountOnOff( void )
{
    BOOL fRetVal = FALSE;

    /* Permit Count  On/Off */
    if ( fnFlashGetByteParm(BP_PERMIT_BC_DUCKING_ALLOWED) )
    {
        fRetVal = TRUE;
    }
    return ( fRetVal );
}

/* *************************************************************************
 FUNCTION NAME: fnCanPermitTownCircleOnOff
 AUTHOR: David Huang
 DESCRIPTION: Utility function used to check if user can toggle Town Circle in Permit mode

  PARAMETERS   : None, to meet the type defined in pre function table
  RETURN       : TRUE if user can toggle town circle on or off
  NOTES     : None
***************************************************************************/

BOOL fnCanPermitTownCircleOnOff( void )
{
    BOOL fRetVal = FALSE;

    /* Town Circle On/Off */
    if ( fnFlashGetByteParm(BP_PERMIT_TC_DUCKING_ALLOWED) )
    {
        fRetVal = TRUE;
    }
    return ( fRetVal );
}


/* *************************************************************************
 FUNCTION NAME: fnCanPermitDateOnOff
 AUTHOR: David Huang
 DESCRIPTION: Utility function used to check if user can toggle Date in permit mode

  PARAMETERS   : None, to meet the type defined in pre function table
  RETURN       : TRUE if user can toggle date on or off
  NOTES     : None
***************************************************************************/

BOOL fnCanPermitDateOnOff( void )
{
    BOOL fRetVal = FALSE;

    /* Date On/Off */
    if ( fnFlashGetByteParm(BP_PERMIT_DATE_DUCKING_ALLOWED) )
    {
        fRetVal = TRUE;
    }
    return ( fRetVal );
}

/* *************************************************************************
// FUNCTION NAME: fnIsAdDateTimeMode
//
// DESCRIPTION: 
//      check if the given mode is text-ad-datetime modes
// INPUTS:
//      None.
//
// RETURNS:
//      TRUE or FALSE
//
// WARNINGS/NOTES:
//          
// MODIFICATION HISTORY:
//      07/02/2007    Adam Liu   Initial version
// *************************************************************************/
BOOL fnIsAdDateTimeMode(UINT8   ucPrintMode)
{
    return ((ucPrintMode == PMODE_AD_ONLY)||
           (ucPrintMode == PMODE_AD_TIME_STAMP)||
           (ucPrintMode == PMODE_TIME_STAMP));
}

/* *************************************************************************
// FUNCTION NAME: fnCheckBatchAvailable
//
// DESCRIPTION: 
//      TRUE if there are batch available.
//
// INPUTS:  
//      None.
//
// RETURNS:
//      TRUE if there are batch available.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/26/2007   Adam Liu      Initial version, Adapted from Mega1600
//
// *************************************************************************/
BOOL fnCheckBatchAvailable (void)
{
    BOOL fRetVal = FALSE;

        //Adam: we dont support these two account types so far
/*    if ((fnGetAccountingType() == ACT_SYS_TYPE_ACCUTRAC) || 
        (fnGetAccountingType() == ACT_SYS_TYPE_METERNET))
    {
        if (fnEDMEasIsBatchOpen() == TRUE)
        {
          fRetVal = TRUE;
        }
    }
    else
    {
*/
        /* must be abacus accounting */
        if (fnAXB_AreThereOpenTransactions() == TRUE)
        {
            fRetVal = TRUE;
        }
//    }
    
    return ( fRetVal );
}

/* *************************************************************************
// FUNCTION NAME: fnCheckAbacusAvailable
//
// DESCRIPTION: 
//      TRUE if Abacus is available.
//
// INPUTS:  
//      None.
//
// RETURNS:
//      TRUE if Abacus is available.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/26/2007   Adam Liu      Initial version, Adapted from Mega1600
//
// *************************************************************************/
BOOL fnCheckAbacusAvailable (void)
{
    BOOL fRetVal = FALSE;
    UINT16  usDetailAbacusStatus;
    SINT16  sAbacusStatus;
    
    sAbacusStatus = fnAGDGetAbacusStatus(&usDetailAbacusStatus);

/*
    if ((sAbacusStatus == ABACUS_IN_USE) || 
        (fnGetAccountingType() == (UINT8) ACT_SYS_TYPE_ACCUTRAC ||
         fnGetAccountingType() == (UINT8) ACT_SYS_TYPE_METERNET ))
    {
        fRetVal = TRUE;
    }
*/
    
    return ( fRetVal );
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnUpdateCMWarnings
//
// DESCRIPTION: 
//      Private function to update the CM status during running/printing.
//      Only the warning conditions which not triggers new warning screen
//      are checked here.
//
// INPUTS:
//      None
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  03/10/2008  Oscar Wang      Added code for End of Life.
//  06/22/2007  Joey Cui        Block Low-Fund warning in Permit Mode
//  05/31/2007  Joey Cui        Move clearing code just before re-checking 
//  2/02/2006   Vincent Yi      Initial version
//
// *************************************************************************/
// VBL_TBD_FP
static void fnUpdateCMWarnings(void)
{
    UINT8   ucMode;
    UINT8   ucSubMode;
    UINT8   ucPrintMode = fnOITGetPrintMode();

    fnGetSysMode(&ucMode, &ucSubMode);

    // First clean the old status of warnings
    if (ucSubMode != OIT_SEAL_ONLY)
    {
        fnClearWarningStatus(WCOND_LOW_INK);
        fnClearWarningStatus(WCOND_INKTANK_EXPIRING);
        fnClearWarningStatus(WCOND_WASTETANK_NEAR_FULL);
        fnClearWarningStatus(WCOND_END_OF_LIFE_WARNING);
        
        if (fnIsCmInkLow() == TRUE) 
        {   // ink low, can be cleared, warning once.
            fnPostWarningStatus(WCOND_LOW_INK);
        }
        if (fnIsCmInkTankExpiring() == TRUE) 
        {   // ink tank timeout, can be cleared, warning once.
            fnPostWarningStatus(WCOND_INKTANK_EXPIRING);
        }
        if (fnIsCmWasteTankNearFull() == TRUE) 
        {   // waste tank near full, can be cleared, warning once.
            fnPostWarningStatus(WCOND_WASTETANK_NEAR_FULL);
        }

        if (! (ucPrintMode == PMODE_AD_ONLY         ||
               ucPrintMode == PMODE_TIME_STAMP      ||
               ucPrintMode == PMODE_AD_TIME_STAMP   ||
               ucPrintMode == PMODE_PERMIT          ||
               ucSubMode == OIT_REPORT              ||
               ucSubMode == OIT_TEST_PATTERN))
        {
    fnClearWarningStatus(WCOND_LOW_FUNDS);
    fnDumpStringToSystemLog( "LowFundsWarn clear cond in fnUpdateCMWarnings" );

            if (fLowFundsWarningPending == TRUE)
            {   // low funds warning
                fnPostWarningStatus(WCOND_LOW_FUNDS);
		        fnDumpStringToSystemLog( "LowFundsWarn post cond in fnUpdateCMWarnings" );
            }
        }

        if (fLowPieceCountWarningPending == TRUE)
        {   // warning to indicate that the PSD is reaching its maximum number
            // of items printed
            fnPostWarningStatus(WCOND_END_OF_LIFE_WARNING);
        }        
    }   // if (ucSubMode != OIT_SEAL_ONLY)
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckInspection
//
// DESCRIPTION: 
//      Do inspection checking before enter print mode ready screen
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      DCOND_INSPECTION_REQUIRED will be set in lwDisablingConditions if
//          inspection required.
//      WCOND_INSPECTION_DUE will be set in lwWarningConditions if 
//          inspection due.
//
// RETURNS:
//      No screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  1/31/2006   Vincent Yi      Initial version
//
// *************************************************************************/
// VBL_TBD_FP
T_SCREEN_ID fnCheckInspection (void)
{
    T_SCREEN_ID usNext = 0;
    UINT8       bSysInspecStatus;
    UINT8       bPrintedInspecStatus;

    fnInspectionStatus(&bSysInspecStatus, &bPrintedInspecStatus);
    // check if inspection is required, only checking once after power up
    if ((bSysInspecStatus == INSPECTION_REQUIRED) ||
        (bPrintedInspecStatus == INSPECTION_REQUIRED))
    {
        fnPostDisabledStatus(DCOND_INSPECTION_REQUIRED);
    }
    // check for inspection warning, , only checking once after power up 
    if (bSysInspecStatus == INSPECTION_DUE)
    {
        fnPostWarningStatus(WCOND_INSPECTION_DUE);
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnPostCheckInspection
//
// DESCRIPTION: 
//      After checking inspection conditions, transition to proper screens.
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      the screen ID transition to after checking
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  1/31/2006   Vincent Yi      Initial version
//
// *************************************************************************/
// VBL_TBD_FP
T_SCREEN_ID fnPostCheckInspection (void)
{
    T_SCREEN_ID usNext = 0;

    /* inspection request */
    if ((fnIsDisabledStatusSet(DCOND_INSPECTION_REQUIRED) == TRUE)  &&
        (fInspectionRequiredShown == FALSE))
    {
//        usNext = GetScreen(SCREEN_INSPECTION_REQ);
        fInspectionRequiredShown = TRUE;
		SendBaseEventToTablet(BASE_EVENT_INSPECTION_REQUIRED, NULL);

		return usNext;
    }

    /* the inspection due */
    if ((fnIsWarningStatusSet(WCOND_INSPECTION_DUE) == TRUE) && 
        (fInspectionWarningShown == FALSE))
    {
//        usNext = GetScreen(SCREEN_INSPECTION_WARNING);
        fInspectionWarningShown = TRUE;
		SendBaseEventToTablet(BASE_EVENT_INSPECTION_DUE, NULL);
        return usNext;
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnPostCheckEndOfLife
//
// DESCRIPTION: 
//      After checking EOL conditions, transition to proper screens.
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      the screen ID transition to after checking
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  03/10/2008   Oscar Wang      Initial version
//
// *************************************************************************/
static T_SCREEN_ID fnPostCheckEndOfLife (void)
{
    T_SCREEN_ID usNext = 0;

    /* EOL error */
    if ((fnIsDisabledStatusSet(DCOND2_PIECE_COUNT_EOL) == TRUE)  &&
        (fEOFErrShown == FALSE))
    {
        usNext = GetScreen(SCREEN_PIECE_COUNT_EOL);
        fEOFErrShown = TRUE;
        return usNext;
    }

    /* EOL warning */
    if ((fnIsWarningStatusSet(WCOND_END_OF_LIFE_WARNING) == TRUE) && 
        (fEOFWarnShown == FALSE))
    {
        usNext = GetScreen(SCREEN_PIECE_COUNT_EOL_WARN);
        fEOFWarnShown = TRUE;
        return usNext;
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckPSDState
//
// DESCRIPTION: 
//      Do PSD state checking before enter print mode ready screen 
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      DCOND_OUT_OF_SERVICE will be set in lwDisablingConditions if meter is 
//      out of service.
//      DCOND_PSD_MOOD_PROBLEM will be set in lwDisablingConditions.
//
// RETURNS:
//      the screen ID transition to after checking
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/18/2008  Raymond Shen    Remove checking of PSD battery status which is done
//                              in function fnCheckBatteryState now.
//  07/20/2006  Vincent Yi      Added code for "Meter Not Authorized" error to
//                              show this error screen once
//  02/14/2006  Vincent Yi      Initial version
//
// *************************************************************************/
// VBL_TBD_FP
T_SCREEN_ID fnCheckPSDState (void)
{
    T_SCREEN_ID     usNext = 0;
//    UINT32          ulPsdState = 0;

    if ( fnIsIPSDEnabled() == FALSE )
    {
        fnPostDisabledStatus(DCOND2_PSD_DISABLED);
        SendBaseEventToTablet(BASE_EVENT_PSD_DISABLED, NULL);
//        return (usNext = GetScreen(SCREEN_PSD_DISABLED_STATE));
    }
    if ( fnOITCheckPSDMarriedStatus() == FALSE )
    {
        fnPostDisabledStatus(DCOND2_PSD_MISMATCH);
        SendBaseEventToTablet(BASE_EVENT_PSD_MISMATCH, NULL);
//        return (usNext = GetScreen(SCREEN_PSD_MISMATCH));
    }

    if (fnGetPsdState(&lwPsdState) == FALSE)
    {
       if(lwPsdState == eIPSDSTATE_INCOMMUNICADO){
    	   fnPostDisabledStatus(DCOND_NO_PSD);
           SendBaseEventToTablet(BASE_EVENT_NO_PSD, NULL);
       }
       if ((lwPsdState < eIPSDSTATE_FULLPOSTAL) ||
              (lwPsdState == eIPSDSTATE_GERM_RSAKEYLOADED) )
       {   // PSD manufactured but not authorized
           fnPostDisabledStatus(DCOND_AUTHORIZATION_REQ);
           SendBaseEventToTablet(BASE_EVENT_PSD_AUTH_REQ, NULL);
       }

       if ((lwPsdState == eIPSDSTATE_WITHDRAWN) ||
    		   (lwPsdState == eIPSDSTATE_GERM_WITHDRAWN) )
       {
    	   fnPostDisabledStatus(DCOND_OUT_OF_SERVICE);
           SendBaseEventToTablet(BASE_EVENT_PSD_OUT_OF_SERVICE, NULL);
       }

       fnPostDisabledStatus(DCOND_PSD_MOOD_PROBLEM);

   }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckFileSysCorruption
//
// DESCRIPTION: 
//      Do File System Corruption checking before enter print mode ready screen 
//      On file I/O calls that return NUF_INTERNAL, e.g. flush, close, delete, the base 
//      should generate a System Error. 
//
// INPUTS:
//      errcode: .
//
// OUTPUTS:
//      DCOND_FILE_SYS_CORRUPTION will be set in PDisabled if on file I/O calls that  
//      return NUF_INTERNAL.
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//
// *************************************************************************/
void fnCheckFileSysCorruption(STATUS errcode)
{
   //if status is NUF_INTERNAL, generate system error
   if (fnFileSysCorruption(errcode))
   {
       fFileSystemCorruption = TRUE;       
       fnLogError( ERR_TYPE_FATAL, ERROR_CLASS_SYS, ECSYS_FILE_SYS_CORRUPTION );

       fnPostDisabledStatus(DCOND_FILE_SYS_CORRUPTION);
       SendBaseEventToTablet(BASE_EVENT_FILE_SYS_CORRUPTION, NULL);

       //Removed the line below once Tablet check the disabled condition DCOND_FILE_SYS_CORRUPTION
       unsigned char   pMsgData[2];
       
       pMsgData[0] = OIT_LEFT_PRINT_RDY;
       pMsgData[1] = 0;
       (void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);
   }
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckMissingGraphics
//
// DESCRIPTION: 
//      Check miss graphics before enter print mode ready screen
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      No screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  04/21/2006  Vincent Yi      Initial version
//
// *************************************************************************/
// VBL_TBD_FP
static T_SCREEN_ID fnCheckMissingGraphics (void)
{
    T_SCREEN_ID usNext = 0;

    if(fnAnyGraphicsMissing(pMissingGraphicList))   
    {
        if(pMissingGraphicList[0] == IMG_ID_TOWN_CIRCLE)
        {
            fnPostDisabledStatus(DCOND_TC_REQUIRED);
        }
        else if(pMissingGraphicList[1] > 0)
        {
            fnPostDisabledStatus(DCOND_AD_REQUIRED);
        }
        else if(pMissingGraphicList[2] > 0)
        {
            fnPostDisabledStatus(DCOND_RPT_REQUIRED);
        }
        else
        {
            fnPostDisabledStatus(DCOND_GRAPHIC_REQUIRED);
        }
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnPostCheckCMConditions
//
// DESCRIPTION: 
//      After checking CM conditions, transition to proper screens.
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      the screen ID transition to after checking
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/17/2006  Vincent Yi      Initial version
//
// *************************************************************************/
static T_SCREEN_ID fnPostCheckCMConditions (void)
{
    T_SCREEN_ID usNext = 0;

    // Disabling conditions

    if ((fnIsDisabledStatusSet(DCOND_PRINTER_FAULT) == TRUE) &&
        (fPrinterFaultShown == FALSE))
    {   // fatal error, display the error and hang everything.
        fPrinterFaultShown = TRUE;
        return (usNext = GetScreen (SCREEN_PRINTER_FAULT));
    }
    if (fnIsDisabledStatusSet(DCOND_COVER_OPEN) == TRUE)
    {   // top cover open, warning, can be cleared.
        return (usNext = GetScreen (SCREEN_TOP_COVER_OPEN));
    }
    if (fnIsDisabledStatusSet(DCOND_INKTANK_LID_OPEN) == TRUE)
    {   // ink tank lid open, prompt to close
        return (usNext = GetScreen (SCREEN_INKTANK_LID_OPEN));
    }

    // Do not show Paper Error screen if it is just shown.
    if ((fnIsDisabledStatusSet(DCOND_PAPER_ERROR) == TRUE) &&
            (fPaperErrorShown == FALSE) )
    {   // paper error, can be cleared.
    	fPaperErrorShown = TRUE;
        return (usNext = GetScreen (SCREEN_PAPER_ERROR));
    }
    if (fnIsDisabledStatusSet(DCOND2_FEEDER_FAULT) == TRUE)
    {
        return (usNext = GetScreen (SCREEN_FEEDER_FAULT));
    }
    if ((fnIsDisabledStatusSet(DCOND_INK_OUT) == TRUE)  &&
        (fInkOutShown == FALSE))
    {
        fInkOutShown = TRUE;
        return (usNext = GetScreen (SCREEN_INK_OUT));
    }
    if (fnIsDisabledStatusSet(DCOND_INKTANK_ERROR) == TRUE)
    {
        if (fNoInkTank == TRUE)
        {
            if (fNoInkTankShown == FALSE)
            {
                fNoInkTankShown = TRUE;
                return (usNext = GetScreen (SCREEN_NO_INKTANK));
            }
        }
        else
        {   
            if (fInkTankErrShown == FALSE)
            {
                fInkTankErrShown = TRUE;
                return (usNext = GetScreen (SCREEN_INKTANK_ERROR));
            }
        }
    }
    if (fnIsDisabledStatusSet(DCOND_PRINTHEAD_ERROR) == TRUE)
    {
        if (fNoPrintHead == TRUE)
        {
            if (fNoPrintHeadShown == FALSE)
            {
                fNoPrintHeadShown = TRUE;
                return (usNext = GetScreen (SCREEN_NO_PRINTHEAD));
            }
        }
        else
        {   
            if (fPrintHeadErrShown == FALSE)
            {
                fPrintHeadErrShown = TRUE;
                return (usNext = GetScreen (SCREEN_PRINTHEAD_ERROR));
            }
        }
    }
    if ((fnIsDisabledStatusSet(DCOND_WASTE_TANK_FULL) == TRUE) &&
        (fWasteTankFullShown == FALSE))
    {   // waste tank full
        fWasteTankFullShown = TRUE;
        return (usNext = GetScreen (SCREEN_WASTETANK_FULL));
    }

    // Warnings conditions

    if ((fnIsWarningStatusSet(WCOND_WASTETANK_NEAR_FULL) == TRUE) &&
        (fWasteTankNearFullShown == FALSE))
    {   // waste tank near full
        fWasteTankNearFullShown = TRUE;
        return (usNext = GetScreen (SCREEN_WASTETANK_NEAR_FULL));
    }
    if ((fnIsWarningStatusSet(WCOND_INKTANK_EXPIRING) == TRUE) &&
        (fInkTankExpiringShown == FALSE))
    {   // ink tank expiring
        fInkTankExpiringShown = TRUE;
        return (usNext = GetScreen(SCREEN_INKTANK_EXPIRING));
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckPSOCDisablingConditions
//
// DESCRIPTION: 
//      Check for PSOC disabling conditions, and branch to the
//      "Please Wait" screen if any disabling conditions exist.
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      the screen ID transition to after checking
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  4/10/2007  Derek DeGennnaro      Initial version
//
// *************************************************************************/
static T_SCREEN_ID fnCheckPSOCDisablingConditions (void)
{
    T_SCREEN_ID usNextScreen = 0;
    if (fnIsDisabledStatusSet(DCOND_PSOC_PROGRAMMING_IN_PROG) || fnIsDisabledStatusSet(DCOND_PSOC_RESEED_IN_PROG)) 
    {
        usNextScreen = GetScreen(SCREEN_PSOC_PLEASE_WAIT);
    }
    return usNextScreen;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckIsPrintDateTimeOn
//
// DESCRIPTION: 
//      Check date time flag in optional print mode
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      No screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      Adam Liu        Initial version
//
// *************************************************************************/
static BOOL fnCheckIsPrintDateTimeOn (void)
{
    return fPrintDateTimeOn;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnSetPrintDateTimeFlag
//
// DESCRIPTION: 
//      Set date time flag in optional print mode
//
// INPUTS:
//      fFlag - flag of print datetime.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      No screen change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      Adam Liu        Initial version
//
// *************************************************************************/
BOOL fnSetPrintDateTimeFlag (BOOL fFlag)
{
    return(fPrintDateTimeOn = fFlag);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnUpdateOptionalPrintMode
//
// DESCRIPTION: 
//      Private function to switch to correct mode based on 
//      selection of Text, Ad, Datetime.
//
// INPUTS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      Adam Liu      Initial version
//
// *************************************************************************/
static void fnUpdateOptionalPrintMode (void)
{
    UINT8 bNewPrintMode = PMODE_AD_TIME_STAMP; // Ad-DateTime default

 
    if (fnCheckIsPrintDateTimeOn() == TRUE)
    {
            if ((fnCheckIsAdSelected() == FALSE) && 
                (fnCheckIsTextSelected() == FALSE))
            {
                bNewPrintMode = PMODE_TIME_STAMP; // DateTime only
            }
    }
    else
    {
        bNewPrintMode = PMODE_AD_ONLY; // Ad only
    }
    
    fnOITSetPrintMode(bNewPrintMode);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnRequestCMStatus
//
// DESCRIPTION: 
//      Function that Send Intertask to request control manager status and set
//      message timeout.
//
// INPUTS:
//      None
//
// OUTPUTS:
//      None
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//  05/19/2006  James Wang      Initial version 
//
// *************************************************************************/
void fnRequestCMStatus(void)
{
    OSSendIntertask(CM, OIT, OC_GET_STATUS_REQ, NO_DATA, NULL, 0);
    fnSetMessageTimeout(CM, CO_GET_STATUS_RSP, STANDARD_INTERTASK_TIMEOUT);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnResetAnimatorCounter
//
// DESCRIPTION: 
//      Function that reset the animator counter.
//
// INPUTS:
//      None
//
// OUTPUTS:
//      None
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//  11/17/2006  Adam Liu      Initial version 
//
// *************************************************************************/
void fnResetAnimatorCounter( void )
{
    ucAnimatorCounter = 0;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetAnimatorCounter
//
// DESCRIPTION: 
//      Function that get the animator counter.
//
// INPUTS:
//      None
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Animator count
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//  11/17/2006  Adam Liu      Initial version 
//
// *************************************************************************/
UINT8 fnGetAnimatorCounter( void )
{
    return ucAnimatorCounter;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnUpdateAnimatorCounter
//
// DESCRIPTION: 
//      Function that update the animator counter.
//
// INPUTS:
//      None
//
// OUTPUTS:
//      None
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//  11/17/2006  Adam Liu      Initial version 
//
// *************************************************************************/
void fnUpdateAnimatorCounter( void )
{
    ucAnimatorCounter ++;
    if (ucAnimatorCounter >= MAX_ANIMATOR_COUNT)
    {
        ucAnimatorCounter = 0;
    }
}


/* *************************************************************************
// FUNCTION NAME: fnIsIncidentReportNeeded
//
// DESCRIPTION:
//      uitility function that Check the mail state progress to find
//      out if the incident report is needed
//
// INPUTS:
//      None.
//
// RETURNS:
//      TRUE if report is needed
//      FALSE otherwise
//
// WARNINGS/NOTES:  
//
//
// MODIFICATION HISTORY:
//  07/02/2007    I. Le Goff   Initial version
// *************************************************************************/
static UINT8 fnIsIncidentReportNeeded( void )
{
    switch(fnGetMailProgressState())
    {
    case PF_DEBIT_COMPLETE:
    case PF_TRANSPORT_START:
    case PF_PRINT_IN_PROGRESS:
        return( TRUE );
    default:
        return( TRUE );
    }   
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckPieceIDHome
//
// DESCRIPTION: 
//      Check whether Piece ID need to be shown on home screen.
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      TRUE/FALSE
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY:
//  07/23/2007      Raymond Shen    Initial version
//
// *************************************************************************/
static BOOL fnCheckPieceIDHome(void)
{
    //return ( fnFrancanSense() );
    return (TRUE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckReplyCodeHome
//
// DESCRIPTION: 
//      Check whether Reply code number need to be shown on home screen.
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      TRUE/FALSE
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY:
//  07/23/2007      Raymond Shen    Initial version
//
// *************************************************************************/
static BOOL fnCheckReplyCodeHome(void)
{
    return (fnFrancanSense());
    //return (TRUE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckBatteryState
//
// DESCRIPTION: 
//      Do PSD/UIC battery state checking before enter print mode ready screen 
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      the screen ID transition to after checking
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/17/2008  Raymond Shen        Initial version
//
// *************************************************************************/
static T_SCREEN_ID fnCheckBatteryState(void)
{
    T_SCREEN_ID     usNext = 0;

    switch(ucOITBatteryStatus)
    {
    case BOTH_BATTERIES_OK_STATUS:
        break;
        
    case PSD_BATTERY_LOW_STATUS:
    case CMOS_BATTERY_LOW_STATUS:
    case BOTH_BATTERIES_LOW_STATUS:
        // go to WrnBatteryLow screen
    	fnPostWarningStatus(WCOND_LOW_BATTERY_PSD);
    	SendBaseEventToTablet(BASE_EVENT_LOW_PSD_BATTERY, NULL);
//    	usNext = GetScreen(SCREEN_BATTERY_LOW_WARNING);
        break;
        
    case PSD_BATTERY_EOL_STATUS:
    case CMOS_BATTERY_EOL_STATUS:
    case BOTH_BATTERIES_EOL_STATUS:
    	SendBaseEventToTablet(BASE_EVENT_EOL_PSD_BATTERY, NULL);
        // go to WrnBatteryEOL screen
//        usNext = GetScreen(SCREEN_BATTERY_EOL_WARNING);
        break;

    default:
        break;
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsDataCenterUploadEnabled
//
// DESCRIPTION: 
//      Check whether Upload Data need to be shown on home screen.
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      TRUE/FALSE
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY:
//  07/24/2008      Raymond Shen    Initial version
//
// *************************************************************************/
static BOOL fnIsDataCenterUploadEnabled(void)
{
    BOOL    fRet = FALSE;

/*
    if((fnDCAPIsDataCaptureActive() == TRUE)
            || (IsAbAccountingUploadEnabled() == TRUE))
    {
        fRet = TRUE;
    }
*/
    
    return (fRet);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckServicesHome
//
// DESCRIPTION: 
//      Check whether "Services" option need to be shown on home screen.
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      TRUE/FALSE
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY:
//  10/29/2008      Raymond Shen    Initial version
//
// *************************************************************************/
static BOOL fnCheckServicesHome(void)
{
    BOOL    fStatus = false;

/*
    if((fnRateIsKIPHrtRequired() == TRUE) 
            && (fnCheckRatingEnabled() == TRUE))
    {
        fStatus = TRUE;
    }
*/

    return fStatus;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckBatchHome
//
// DESCRIPTION: 
//      Check whether Batch information need to be shown on home screen.
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      TRUE/FALSE
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY:
//  10/10/2008      Raymond Shen    Initial version
//
// *************************************************************************/
static BOOL fnCheckBatchHome(void)
{
    BOOL    fStatus = false;

    if(fnFlashGetByteParm(BP_BATCH_CLEARABLE) == BATCH_CLEARABLE_NORMAL_AND_HOME_ONLY_ACCOUNT_SELECTED)
    {
        // Show "Batch" at home screen only if a standard account is selected.
/*
        if((fnCheckAccountFeatureEnabled() == TRUE) && 
        (fnGetAccountingType() == ACT_SYS_TYPE_INTERNAL_ON) &&
        (fnGetActiveAccount() != 0))
        {
            fStatus = TRUE;
        }
*/
    }
    else if(fnFlashGetByteParm(BP_BATCH_CLEARABLE) == BATCH_CLEARABLE_NORMAL_AND_HOME)
    {
        fStatus = TRUE;
    }

    return fStatus;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCandaPackageServie
//
// DESCRIPTION: 
//      Check whether "Barcode" option need to be shown on tape printing screen.
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      TRUE/FALSE
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY:
//  08/19/2009      Jingwei,Li    Initial version
//
// *************************************************************************/
static BOOL fnIsCanadaPackageServie(void)
{
    BOOL    fStatus = FALSE;
    return fStatus;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnOISetAccountingProblem
//
// DESCRIPTION: 
//      Set up to show that BudMgr/InView accounting was disabled during 
//      power up due to a problem.
//
// INPUTS:
//      Error code for the problem.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY:
//  10-Mar-14      Sandra Peterson    Initial version
//
// *************************************************************************/
void fnOISetAccountingProblem(unsigned char ucError)
{
	if (fnGetSysTaskPwrupSeqCompleteFlag() == TRUE)
	{
		fCMOSAbacusAcntProblem = TRUE;
		ucCMOSAbacusAcntErrorCode = ucError;
	}
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfInViewDisableError
//
// DESCRIPTION: 
//      Set up to show that BudMgr/InView accounting was disabled due to a problem.
//
// INPUTS:
//      Error code for the problem.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY:
//  10-Mar-14      Sandra Peterson    Initial version
//
// *************************************************************************/
BOOL fnfInViewDisableError (UINT16 *pFieldDest, UINT8 bLen, UINT8 bFieldCtrl,
							UINT8 bNumTableItems, UINT8 *pTableTextIDs, UINT8 *pAttributes)
{
    char pTmpBuf[6];
    UINT8 bMyLen;


	// clear the field
    SET_SPACE_UNICODE(pFieldDest, bLen);

	// load the error code
	bMyLen = sprintf(pTmpBuf, "%02X%02X", ERROR_CLASS_ABACUS, ucCMOSAbacusAcntErrorCode);
    if (bMyLen <= bLen)
    {
        fnAscii2Unicode(pTmpBuf, pFieldDest, bLen);
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: fnhkClearAbacusProblem
//
// DESCRIPTION: 
//      Hard key function that clears the flag indicating that there was
//      a problem w/ the set up of BudMgr/InView accounting.
//
// INPUTS:  
//      Standard hard key function inputs, format 1
//
// RETURNS:
//
// WARNINGS/NOTES:
//
// NEXT SCREEN: 
//      Always go to next screen 1
//
// MODIFICATION HISTORY:
//      11 Mar 14   Sandra Peterson  Initial version
// *************************************************************************/
UINT16 fnhkClearAbacusProblem(UINT8 bKeyCode, UINT16 wNextScr1, UINT16 wNextScr2)
{
	fCMOSAbacusAcntProblem = FALSE;
	return (wNextScr1);
}


