/******************************************************************************
	PROJECT:    	Mega
    COMPANY:        Pitney Bowes
    AUTHOR :        Craig DeFilippo
	MODULE NAME:    $Workfile:   uniString.c  $
    REVISION:       $Revision:   1.11  $
       
    DESCRIPTION:    Partial Unicode String Library

    Subset of standard 'C' string library functions for use with 16 bit
	unicode character strings. Plus some additional utilities. 
	
-------------------------------------------------------------------------------

	unistrchr() locates the first occurence of a unicode char in the
    source string, the terminating null unicode char is considerated a part
    of the string, returning a pointer to the first occurence of the unicode
    character located within the source string, a null pointer is returned if
    the source string doesn't contain the unicode character.

	unistrcat() concatenates (appends) a copy of the source string to the
    end of the destination string, returning the destination string.
    Strings are unichar strings.

    unistrcpy() copies the source string to the spot pointed to be
    the destination string, returning the destination string.
    Strings are unichar strings.

	unistrncpy() copies the initial usCount characters of src to dst.If usCount
	is less than or equal to the length of src, a null character is
	not appended automatically to the copied string. If count is
	greater than the length of strSource, the destination string
	is padded with null characters up to length count.

	unistrcmp() compares two unichar strings and returns an integer
    to indicate whether the first is less than the second, the two are
    equal, or whether the first is greater than the second.
	
	unistricmp() compare unichar strings, ignore case	

	unistrlen() unistrlen returns the length of a null-terminated unichar
	string, not including the null unichar itself.

	unistrsqz() Copies the unichar string src into the spot specified by dest,
	if it does not fit some characters in the middle will be replaced by '..'.

	unistrcpyAlign() Copies the unichar string src into the spot specified by dest,
	assumes enough room. Spaces are used to pad the alignment.

	unistrpartcmp() compares two unichar strings and returns an integer
	to indicate whether the first is less than the second, the two are
	equal, or whether the first is greater than the second. The
	comparison is only carried out up to length len.

	unistripartcmp() performs a case-insensitive unichar string comparision.  The
	comparison is only carried out up to length len. 

	uniisinteger() tests each unichar in the passed string to make sure it
   	comprises a decimal integer number.

	unistrset() Sets all the characters of unistr to 'unich' up to the first NULL
	character found.				
   
	unistrnset() Sets, at most, the first usCount characters of unistr to unich
	If ucCount is greater than the length of unistr, the length of unistr is
	used instead of usCount.				

	unistrcpyFromCharStr() Copies a char string from src to the unicode string dst,
	assumes enough room.  

 ----------------------------------------------------------------------------
               Copyright (c) 2001 Pitney Bowes Inc.
                    35 Waterview Drive
                 Shelton, Connecticut  06484
 ----------------------------------------------------------------------------

        REVISION HISTORY:
        $Log:   H:/group/SPARK/ARCHIVES/UIC/uniString.c_v  $
* 
*    Rev 1.11   11 Feb 2004 11:39:28   CR002DE
* 1. fix unistrcpyFromCharStr.  For character values
* 128 and higher, the msb of the unichar was being 
* set to FF, due to sign extension.  
* 
*    Rev 1.10   Nov 17 2003 09:56:28   MA507HA
* Warning correction
* 
*    Rev 1.9   29 Aug 2003 16:41:58   JI002HU
* Fixed a problem in fnUnistringAlign(), make manual align to work.
* 
*    Rev 1.8   Apr 25 2003 10:14:20   cristeb
* Allow normal and templated 'all-space' strings (used to clear input entry field on screen)
* to pass through without adjustment; let input field display strings go through normal left/right
* alignment logic.
* 
*    Rev 1.7   03 Apr 2003 15:52:28   CX08854
* Fixed fields with no refresh functions
* 
*    Rev 1.6   03 Apr 2003 10:30:38   CX08854
* Alignment Routeen now treats all input fields as Manual Alignment
* 
*    Rev 1.5   26 Mar 2003 11:59:22   CX08854
* change required to support different alignment codes
* 
*    Rev 1.4   Mar 20 2003 11:18:00   cristeb
* OI Cleanup/Code Reduction: Branch version tested and moved to tips.
* 
*    Rev 1.3.1.0   19 Mar 2003 11:22:44   CX08854
* Added automatic alignment of field functions
* NOTE: Removes white space prior to alignment
* 
* 
*    Rev 1.3   Sep 11 2002 18:48:42   LIV
* Add unistrchr().
* 
*    Rev 1.2   08 Jul 2002 11:29:00   defilcj
* added unistrncpy(), unistrset(), unistrnset(),
* and unistrcpyFromCharStr()


*
*    Rev 1.1   09 Apr 2002 11:05:24   defilcj
* 1. cleanup
* 
*    Rev 1.0   14 Dec 2001 17:12:04   defilcj
* initial Department Accounting Integration
 
******************************************************************************/
#include "unistring.h"
#include "oitpriv.h"


/******************************************************************************
   FUNCTION NAME: unistrcat
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Concatenates src onto the end of dest.  Assumes enough space
				: in dest.
   PARAMETERS   : dst - unichar string to which "src" is to be appended
				: src - unichar string to be appended to the end of "dst"
   RETURN       : The address of "dst"
   NOTES		: None
******************************************************************************/
unichar* unistrcat(unichar* dst, const unichar* src)
{
        unichar* cp = dst;

        while(*cp)
			cp++;					/* find end of dst */

        while(*cp++ = *src++) ;     /* Copy src to end of dst */

        return(dst);                /* return dst */
}


/******************************************************************************
   FUNCTION NAME: unistrcpy
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Copies the unichar string src into the spot specified by
				: dest, assumes enough room.
   PARAMETERS   : dst - unichar string over which "src" is to be copied
				: src - unichar string to be copied over "dst"
   RETURN       : The address of "dst"
   NOTES		: None
******************************************************************************/

unichar* unistrcpy(unichar* dst, const unichar* src)
{
        unichar* cp = dst;

        while(*cp++ = *src++);               /* Copy src over dst */

        return(dst);
}

/******************************************************************************
   FUNCTION NAME: unistrcmp
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : unistrcmp compares two unichar strings and returns an integer
				: to indicate whether the first is less than the second, the two are
				: equal, or whether the first is greater than the second.
   PARAMETERS   : src - string for left-hand side of comparison
				: dst - string for right-hand side of comparison
   RETURN       : returns -1 if src <  dst
				: returns  0 if src == dst
				: returns +1 if src >  dst
   NOTES		: None
******************************************************************************/
short unistrcmp(const unichar* src, const unichar* dst)
{
        short ret = 0 ;

        while( ! (ret = (short)(*src - *dst)) && *dst)
                ++src, ++dst;

        if ( ret < 0 )
                ret = -1 ;
        else if ( ret > 0 )
                ret = 1 ;

        return( ret );
}

/******************************************************************************
   FUNCTION NAME: unistricmp
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : performs a case-insensitive unichar string comparision.
   PARAMETERS   : src - string for left-hand side of comparison
				: dst - string for right-hand side of comparison
   RETURN       : returns -1 if src <  dst
				: returns  0 if src == dst
				: returns +1 if src >  dst
   NOTES		: None
******************************************************************************/
short unistricmp(const unichar* src, const unichar* dst)
{
        unichar f,l;

        do  {
            f = ((*src <= (unichar)'Z') && (*src >= (unichar)'A'))
                ? *src + (unichar)'a' - (unichar)'A'
                : *src;
            l = ((*dst <= (unichar)'Z') && (*dst >= (unichar)'A'))
                ? *dst + (unichar)'a' - (unichar)'A'
                : *dst;
            dst++;
            src++;
        } while ( (f) && (f == l) );

        return (short)(f - l);
}

/******************************************************************************
   FUNCTION NAME: unistrlen
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Finds the length in unichar's of the given string, not including
				: the final null unichar.
   PARAMETERS   : unistr - string whose length is to be computed
   RETURN       : length of the string "unistr", exclusive of the final null unichar
   NOTES		: None
******************************************************************************/
unsigned short unistrlen(const unichar* unistr)
{
        const unichar* eos = unistr;

        while( *eos++ ) ;

        return( (unsigned short)(eos - unistr - 1) );
}


/******************************************************************************
   FUNCTION NAME: unistrsqz
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Copies the unichar string src into the spot specified by
				: dest, if it does not fit some characters in the middle 
				: will be replaced by the character defined by UNICHR_SQZER
   PARAMETERS   : pointer to destination container,
   				: pointer to null terminated unicode string,
   				: length of dest container, not including space for NULL
				:
   RETURN       : dst 
   NOTES		: If compression is not necessary, a simple strcpy is performed
******************************************************************************/
unichar* unistrsqz(unichar* dst, const unichar* src, unsigned short dstlen)
{
	unsigned short i=0,j,srclen;

	srclen = unistrlen(src);

	if (dstlen < 1)
		*dst = 0;
	else if (srclen <= dstlen)
		unistrcpy(dst, src);
	else
	{
		//copy first half of src to dst
		j = (dstlen)>>1;		
		while(i<j)
		{
			dst[i] = src[i];
			i++;
		}
		
		//append stuffing character
		dst[i++] = UNICHR_SQZER;
		
		//copy src to dst, from right to left
		for(j=dstlen-1; j>=i; j--)
		{
			dst[j] = src[--srclen];
		}
		
		//poke in null terminator
		dst[dstlen] = 0;
	}
	return(dst);
}

/******************************************************************************
   FUNCTION NAME: unistrcpyAlign
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Copies the unichar string src into the spot specified by
				: dest, assumes enough room. Spaces are used to pad the alignment.
   PARAMETERS   : pointer to null terminated unicode string,
   				: pointer to destination container,
				: length of dest container, not including space for NULL
   RETURN       : The address of "dst"
   NOTES		: align can be:
				: UNISTR_LEFT_ALIGN,
				: UNISTR_RIGHT_ALIGN,
				: UNISTR_CENTER_ALIGN
******************************************************************************/
unichar* unistrcpyAlign(unichar* dst, const unichar* src, unsigned short dstlen, unsigned char align)
{
	short pad, i;

	pad = dstlen - unistrlen(src);

	switch (align & UNISTR_ALIGN_MASK)
	{
		case UNISTR_CENTER_ALIGN:
			pad >>=1;
			i = 0;
			while(pad-- > 0) dst[i++] = UNICHR_SPACE;
			dst[i] = 0;
			unistrcat((dst+i),src);
			
			pad = dstlen - unistrlen(dst);
			i = dstlen - pad;
			while(pad-- > 0) dst[i++] = UNICHR_SPACE;
			break;
		case UNISTR_LEFT_ALIGN:
			unistrcpy(dst,src);
			i = dstlen - pad;
			while(pad-- > 0) dst[i++] = UNICHR_SPACE;
			break;
		case UNISTR_RIGHT_ALIGN:
			i = 0;
			while(pad-- > 0) dst[i++] = UNICHR_SPACE;
			dst[i] = 0;
			unistrcat((dst+i),src);
			break;
	}
	
	//poke in null terminator
	dst[dstlen] = 0;

    return(dst);
}
/******************************************************************************
   FUNCTION NAME: unistrspartcmp
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : unistrpartcmp compares two unichar strings and returns an integer
				: to indicate whether the first is less than the second, the two are
				: equal, or whether the first is greater than the second. The
				: comparison is only carried out up to length len. 
   PARAMETERS   : src - string for left-hand side of comparison
				: dst - string for right-hand side of comparison
				: len - max number of unichar's to compare
   RETURN       : returns -1 if src <  dst
				: returns  0 if src == dst
				: returns +1 if src >  dst
   NOTES		: None
******************************************************************************/
short unistrpartcmp(const unichar* src, const unichar* dst, unsigned short len)
{
        short ret = 0 ;
		unsigned short i = 0;

		//len = len * sizeof(unichar);
        while( ! (ret = (short)(*src - *dst)) && *dst)
		{
			++src;
			++dst;
			if (++i >= len)
				break;
		}
        if ( ret < 0 )
                ret = -1 ;
        else if ( ret > 0 )
                ret = 1 ;

        return( ret );
}
/******************************************************************************
   FUNCTION NAME: unistripartcmp
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : performs a case-insensitive unichar string comparision.  The
				: comparison is only carried out up to length len. 
   PARAMETERS   : src - string for left-hand side of comparison
				: dst - string for right-hand side of comparison
				: len - max number of unichar's to compare
   RETURN       : returns -1 if src <  dst
				: returns  0 if src == dst
				: returns +1 if src >  dst
   NOTES		: None
******************************************************************************/
short unistripartcmp(const unichar* src, const unichar* dst, unsigned short len)
{
        unichar f,l, i= 0;

        do  {
            f = ((*src <= (unichar)'Z') && (*src >= (unichar)'A'))
                ? *src + (unichar)'a' - (unichar)'A'
                : *src;
            l = ((*dst <= (unichar)'Z') && (*dst >= (unichar)'A'))
                ? *dst + (unichar)'a' - (unichar)'A'
                : *dst;
            dst++;
            src++;
			if (++i >= len)
				break;
			
        } while ( (f) && (f == l) );

        return (short)(f - l);
}

/******************************************************************************
   FUNCTION NAME: uniisinteger
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : tests each unichar in the passed string to make sure it
   				: comprises a decimal integer number
   PARAMETERS   : src - string
   RETURN       : returns true if all characters are decimal digits
				: returns false otherwise
   NOTES		: None
******************************************************************************/
BOOL uniisinteger(const unichar* src)
{
	BOOL retVal = false;

	while(*src)
	{			
		if ((*src >= UNICODE_ZERO) && (*src <= UNICODE_NINE))
		{
			src++;
			retVal = true;
		}
		else
		{
			retVal = false;
			break;
		}
	}

	return(retVal);
}

/******************************************************************************
   FUNCTION NAME: unistrset
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Sets all the characters of unistr to 'unich' up to the first NULL
				: character found.				
   PARAMETERS   : unistr - unistring to be set
				: unich - unichar to set				
   RETURN       : The address of "unistr"
   NOTES		: None
******************************************************************************/
unichar* unistrset(unichar* unistr, unichar unich)
{
    unichar* unistart = unistr;
    
	while(*unistr)
		*unistr++ = unich;

    return(unistart);
}

/******************************************************************************
   FUNCTION NAME: unistrnset
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Sets, at most, the first usCount characters of unistr to unich
				: If usCount is greater than the length of unistr, the length of
				: unistr is used instead of usCount.				
   PARAMETERS   : unistr - unistring to be set
				: unich - unichar to set
				: ucCount - number of unichar's to be set
   RETURN       : The address of "unistr"
   NOTES		: None
******************************************************************************/
unichar* unistrnset(unichar* unistr, unichar unich, unsigned short usCount)
{
    unichar* unistart = unistr;
    
	while(*unistr && usCount--)
		*unistr++ = unich;

    return(unistart);
}

/******************************************************************************
   FUNCTION NAME: unistrcpyFromCharStr
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Copies a char string from src to the unicode string dst,
				: assumes enough room.  
   PARAMETERS   : dst - unichar string over which "src" is to be copied
				: src - char string to be copied into "dst"
   RETURN       : The address of "dst"
   NOTES		: None
******************************************************************************/
unichar* unistrcpyFromCharStr(unichar* dst, const char* src)
{
        unichar* cp = dst;

        while(*cp++ = 0x00FF & *src++);  /* will pad msb of *cp with 0 */

        return(dst);
}

/******************************************************************************
   FUNCTION NAME: unistrncpy
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Copies the initial usCount characters of src to dst.If usCount
				: is less than or equal to the length of src, a null character is
				: not appended automatically to the copied string. If count is
				: greater than the length of strSource, the destination string
				: is padded with null characters up to length count.

   PARAMETERS   : dst - unichar string over which "src" is to be copied
				: src - unichar string to be copied over "dst"
				: usCount - the max number of characters to copy.
   RETURN       : The address of "dst"
   NOTES		: None
******************************************************************************/

unichar* unistrncpy(unichar* dst, const unichar* src, unsigned short usCount)
{
        unichar* cp = dst;

        while(usCount && (*cp++ = *src++))
			usCount--;							/* Copy src over dst */

		if(usCount)
		{										/* pad with nulls */
			while(--usCount)
				*cp++ = 0;
		}

        return(dst);
}

/******************************************************************************
   FUNCTION NAME: unistrchr
   AUTHOR       : Victor Li
   DESCRIPTION  : Locates the first occurence of a unicode char in the
    				: source string, the terminating null unicode char 
				: is considerated a part of the string.

   PARAMETERS   : src - unichar string to be searched
				: unich - unicode char to search for in "src"
   RETURN       : A pointer to the first occurence of the unicode character located 
   				: within the source string, a null pointer is returned 
				: if the source string doesn't contain the unicode character.
   NOTES	: None
******************************************************************************/

unichar* unistrchr(const unichar* src, const unichar unich)
{
	unichar *cp = (unichar *)src;
	while (*cp != 0 && *cp != unich)
		cp ++;

	if (*cp == unich)
		return cp;
	else
		return 0;
}

/******************************************************************************
   FUNCTION NAME: fnUniStringAlign
   AUTHOR       : Greg Boria - Adapted from unistrcpyAlign 
   DESCRIPTION  : Performs alignment on a string passed in.  Removes leading
				  and trailing blanks prior to alignment.

   PARAMETERS   : pointer to a unicode string,
   				: final length once padded and justified (not including NULL)
				: Type of alignment to apply
   RETURN       : none
   NOTES :
   1.	"align" can be UNISTR_LEFT_ALIGN, UNISTR_RIGHT_ALIGN, UNISTR_CENTER_ALIGN
		or UNISTR_MANUAL_ALIGN.
   2.	This procedure starts by Left Justifying the string unless the field is
		designated as manual alignment, OR the string is all spaces used to blank an
		input field on screen refresh, OR the field uses a template in which case,
		the field function has probably already taken care of alignment.
		Left-justification on remaining cases is done to insure that if a string is
		aligned in the screen tool database with spaces and centering is selected,
		it really will end up centered.
   3.	Input fields with initial string values are aligned normallay, except when
		string contains ALL spaces which is needed to clear the field (no adjustment
		required as string is probably maxed-out with spaces).

******************************************************************************/
void fnUniStringAlign(unichar string[], unsigned short stringlen, unsigned char align)
{
	short move_size = 0;
	short from      = 0;
	short to        = 0;
	short size      = 0;

	// no adjustment for manual aligned fields
	// do not try to adjust fields with templates

	if( !(align & JUMP_MASK) && ((align & UNISTR_ALIGN_MASK) != UNISTR_MANUAL_ALIGN ))
	{
		/**** ALIGN STRING ONLY IF NOT NULL ****/
		if (string[0] != 0)
		{
			/**** GUARANTEE NULL TERMINATOR *****/
			string[stringlen] = 0;

			/**** START BY LEFT JUSTIFYING ****/
			/* identify first non-space character, if any */
			while ((from < stringlen) && (string[from] == UNICHR_SPACE)) {
				from++;
			}

			/**** FILTER OUT STRINGS CONTAINING ONLY SPACE CHARS ****/
			/* these strings needed to 'clear' the input entry field */
			if( from < stringlen ) {
				while (from < stringlen) {
					string[to++] = string[from++];
				}
				string[to] = 0;

				/***** FIND END OF NON-WHITE SPACE *****/
				to = 0;
				while ((to < stringlen) && (string[to] != 0)) {
					if (string[to] != UNICHR_SPACE)
						size = to + 1;
					to++;
				}
			}
		
			/***** Determine how much to move the string *****/
			switch (align & UNISTR_ALIGN_MASK)	{
				case UNISTR_CENTER_ALIGN:
					move_size = (stringlen - size)/2;
				break;
				case UNISTR_RIGHT_ALIGN:
					move_size = stringlen - size;
				break;
				case UNISTR_LEFT_ALIGN:
				break;
			}

			/**** THE STRING IS LEFT JUSTIFIED WITH NO LEADING BLANKS *****/
			to   = stringlen - 1;
			from = size - 1;
		
			/**** PERFORM ACTUAL ALIGNMENT HERE ****/
			/* centering a string = 'pad right' + 'pad left' */
			while (to > (from + move_size)) {             /*** PAD RIGHT   ****/
				string[to--] = UNICHR_SPACE;
			}
			if (move_size > 0) {
				while (from >= 0) {                       /*** MOVE STRING ***/
					string[to--] = string[from--];
				}
				while (to >= 0) {                         /*** PAD LEFT    ***/
					string[to--] = UNICHR_SPACE;
				}
			}
		
			/**** GUARANTEE NULL TERMINATOR *****/
			string[stringlen] = 0;
		}
		 /*end null string check*/
	}
	 /*end template/manual alignment check*/
}
 /*end fnUniStringAlign*/
