/*************************************************************************
*   PROJECT:        Horizon CSD2-3
*	MODULE NAME:	p_filePrivKeyPwd.c
*
*	DESCRIPTION:	This file contains the obfuscated file private key password.
*
* ----------------------------------------------------------------------
*               Copyright (c) 2017 Pitney Bowes Inc.
*                    37 Executive drive
*                   Danbury, Connecticut  06810
* ----------------------------------------------------------------------
*
*************************************************************************/

#include "p_filePrivKeyPwd.h"
