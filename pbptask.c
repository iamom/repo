/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    tsks.c

   DESCRIPTION:    This file contains the Postage By Phone
                   Services Task.  This task interfaces with
                   Data Center and other servers to provide
                   Postage By Phone services such as refills and refunds.

 ----------------------------------------------------------------------
                   Copyright (c) 2016 Pitney Bowes Inc.
                   37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
* ----------------------------------------------------------------------
* CHANGE HISTORY:
*
* 2018.08.07 CWB - Fixed Endianess when copying PSDStatus from XferStruct in 
*				fnPBPSHandleWaitPSDKeyMsg() and in fnPBPSHandleWaitPSDRecord().
*
*************************************************************************/

// TODO: remove these for normal builds, they are defined in the .sub file.  I want them for Lint though.
//#define USESSLLIB 1
//#define PBPS_DEBUG 1
/*lint -e641 */ /* converting enum to int warning */
/*lint -e730 */ /* boolean argment to function warning */
/*lint -e46  */ /* field type should be int warning */
/*lint -e746 */ /* call to function 'XXX' not made in the presence of a prototype warning */
/*lint -e525 */ /* Negative indentation from line 4734 warning */
/*lint -e539  */ /*  Did not expect positive indentation from line warning */
/*lint -save -e537 -e64 -e7*/ /* removes include file warning crap */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "sys/stat.h"
#include "custdat.h"
#include "posix.h"

#include "networking/externs.h"
#include "ssl.h"
#include "pbos.h"
#include "sys.h"
#include "datdict.h"
#include "bobutils.h"
#include "bob.h"
#include "features.h"
#include "oit.h"
#include "pbptask.h"
#include "xmlparse.h"
#include "xmlform.h"
#include "dcaputil.h"
#include "dcapi.h"
#include "bwrapper.h"
#include "oidongle.h"
#include "urlencode.h"
#include "errcode.h"
#include "clock.h"
#include "fnames.h"
//#include "abupload.h"
//#include "pc_daemon.h"
#include "networkmonitor.h"
#include "urlencode.h"
#include "intellilink.h"
#include "ipsd.h"
#include "novadriver.h"
#include "utils.h"
#include "flashutil.h"
#include "api_funds.h"
#include "target.h"       // fnNetSyslog definitions and macros
//#include "deptaccount.h"
//#include "abacusgdm.h"
//#include "oioob.h"
#include "trmstrg.h"
#include "trm.h"
#include "sysdata.h"
#include "sysdatadefines.h"
#include "api_temporary.h"
#include "api_status.h"

#ifdef JANUS
	#include "dummy.h"
#else
	#include "oicinfra.h"
#endif

#if !defined(G900)
	#include "oijinfra.h"
#endif

#if defined(K700) || defined(G900)
//	#include "RateService.h"        // definition of RS_FALSE
//	#include "oirateservice.h"
#else
	#include "ratetask.h"
#endif

#define THISFILE "pbptask.c"

extern BOOL     fUploadInProgress;
int GUIDStatus = PBPS_OK;
/*lint -restore */

/* Reader's Digest Description of the task ************************
The PBP task is responsible for interfacing with the Postage By
Phone Servers to provide Postabe By Phone Services such as
Refills, Refunds, Key Management, Delcon, Data Capture, and
other Special Services.

The task is basically a state machine.  Entering a state causes
some action to be performed.  So, to send a message to the data
center you would enter the Send Request state.

The task uses a Data ID to specifically determine what needs to
be done.  For instance there is a data ID for the sign on message,
audit request message, and refill request message.  The data id
is important because it determines what intertask messages get 
sent, what the next state is, what the next data id is,
what xml message is sent to the data center, and what xml message
should be received from the data center.

Error handling is based on returning an error code from the 
functions in the task.  The error codes are integrated with
the XML error codes (see xmltypes.h and pbptask.h).  When an
error is detected in a function it returns an error code.  
This error code propogates to the error handling function.
Appropiate action is taken based on the error code.

The task tries to be as flexible as possible.  It should be
fairly easy to add new states, new data id's, and new error
codes.  Table entries define most of the behavior of the
task.

XML parsing/formatting is done using the DXML parser and the
XML formatter.  The task sends its messages to the data center
using HTTP 1.1.  It is important that the data center also
adheres to the 1.1 version of HTTP because the task utilizes
several features of 1.1 to reduce buffer sizes, and work with 
the formatter/parser.  The new features that the task uses
are Persistent connections and chunked messages.  It is almost
impossible to use the current xml formatter without the ability
to chunk messages sent to the data center.  The task includes
an HTTP parser that could be easily generalized for use elsewhere.
******************************************************************/

#ifdef PBPS_DEBUG
/*Debuggin Stuff ********************************/
#define PBPS_MAX_DEBUG 128
unsigned char pPBPSDebugBuff[PBPS_MAX_DEBUG]="";
unsigned char *pPBPSDebugCurr = pPBPSDebugBuff;
int iPBPSDebugTemp;
#define XTSKMSG 0x11
#define PBPS_DEBUG_MSG(type,length,value)\
{\
    *pPBPSDebugCurr = type;\
    if (++pPBPSDebugCurr == (pPBPSDebugBuff + PBPS_MAX_DEBUG))\
        pPBPSDebugCurr = pPBPSDebugBuff;\
    for (iPBPSDebugTemp = 0;iPBPSDebugTemp < length; iPBPSDebugTemp++)\
    {\
        *pPBPSDebugCurr = *((unsigned char *)value + iPBPSDebugTemp);\
        if (++pPBPSDebugCurr == (pPBPSDebugBuff + PBPS_MAX_DEBUG))\
            pPBPSDebugCurr = pPBPSDebugBuff;\
    }\
}\
/*End Debuggin Stuff ****************************/
#else
#define PBPS_DEBUG_MSG(type,length,value)
#endif


//-----------------------------------------------

void dbgMsg(char *, int, int);

ulong lwWaitTimeToSync; // Used in Janus for sync timer callback

extern unsigned short   usDcapWriteStatus;
extern BOOL   fLowFundsWarningPending;
extern UINT16           usOITCurrentDisplayService; // Added for Data Upload functionality for Web Vis.

/*
 * Data Capture external variables
 */
extern BOOL bDCAP_DnlStarted;  // TDT
extern unsigned short m_wDCAPDnlCharDataBuffLen; // TDT
extern unsigned short m_wDCAPDnlBktNameBuffLen; // TDT
extern unsigned short m_wDCAPDnlBktNumBuffLen; // TDT
extern unsigned char m_bDCAPDnlBktNameBuff[m_wMaxDcpDnlCharDataLen + 1]; // TDT
extern unsigned char m_bDCAPDnlBktNumBuff[m_wMaxDcpDnlCharDataLen + 1]; // TDT
extern enum DCAP_DCPBKTNUM_CONTEXT m_DCAPCurrBktNumContext; // TDT
extern enum DCAP_WBNNUM_CONTEXT m_DCAPCurrWbnNumContext; // TDT
extern unsigned short usDcapActionStatus;
extern  pfp_t   cmosDcapPfp;
extern EuroConversion cmosEuroConversion;
extern unsigned char        ucOIErrorClass;
extern unsigned char        ucOIErrorCode;
extern ulong			  CMOSTrmUploadState;

/* private dcap functions */
extern char tmpBuf[];

extern int iOITDistrLoopCount;
extern BOOL fOITConfSrvUploadDone;
extern EXP_MTR_CMOS_DATA exp_mtr_cmos;
extern CMOSDownloadCtrlBuf  CmosDownloadCtrlBuf;

extern UINT32 fnGetNetLoggingState(void);
extern BOOL remoteLoggingIsOn(void);
extern STATUS remoteLog(char *, char *);
extern ProxyProtocolSettings *fnGetProxyProtocolSettings(char *pDestinationProtocol);

void *fnGetDCAPBobData (unsigned long,  unsigned long, unsigned char, unsigned short *);

extern NU_MEMORY_POOL    *pSystemMemoryCached;
extern BOOL DISTTASK_Waiting;


/**********************************
Definitions of PBPS task states
**********************************/
enum {
PBPS_IDLE,			//0
PBPS_RCVRSP,		//1
PBPS_SENDREQ,		//2
PBPS_RCVREQ,		//3
PBPS_SENDRSP,		//4
PBPS_WAITPSD,		//5
PBPS_NUM_STATES}; /* PBPS_NUM_STATES MUST BE Last in this list */


/**********************************
    Forward Declaration State
    Handler functions
**********************************/
static int fnPBPSHandleIdle(void);
static int fnPBPSHandleRcvRsp(void);
static int fnPBPSHandleSendReq(void);
static int fnPBPSHandleRcvReq(void);
static int fnPBPSHandleSendRsp(void);
static int fnPBPSHandleWaitPSD(BOOL *pWaitForIntertask);

/**********************************
    The PBPS task state
**********************************/
static unsigned char bPBPSState = PBPS_IDLE;
static unsigned char bPBPSPreviousState = PBPS_IDLE;

/**********************************
    Forward Declaration of
    Change State Routine
**********************************/
/* Transition directly to the new state/data id */
static int fnPBPSGotoState(unsigned char bNewState, unsigned char bDataID);
/* Transition to new state/data id using tables */
static int fnPBPSGotoNextState(unsigned char bCurrID);

/**********************************
    Forward Declaration of
    Data Initialization Routine
**********************************/
static void fnPBPSInitTaskData(void);
static int fnPBPSInitTaskDataEachServiceRequest(void);

/**********************************
    Forward Declarations of
    Message Handler Functions
**********************************/
static int fnPBPSPerformService(INTERTASK *pMsg);
static int fnPBPSHandlePSDMsg(INTERTASK *pMsg);
static int fnPBPSPerformRemoteRefill(INTERTASK *pMsg);

/**********************************
    Forward Declarations of 
    Send/Receive functions
**********************************/
static int fnPBPSReceiveMsg(void);
static int fnPBPSSendMsg(unsigned char);

/**********************************
    Socket Open flag and
    Socket id
**********************************/
static SOCKET_DESCRIPTOR pbpSocket;

static int fnPBPSInitializeSocket(SOCKET_DESCRIPTOR *pSocket);
static int fnPBPSGetServerAddress(SOCKET_DESCRIPTOR *pSocket, sServiceURL *pService);
static int fnPBPSCreateSocket(SOCKET_DESCRIPTOR *pSocket);
static int fnPBPSConnectSocket(SOCKET_DESCRIPTOR *pSocket);
static void fnPBPSDestroySocket(SOCKET_DESCRIPTOR *pSocket);
static short fnPBPSSendHttpChunk(SOCKET_DESCRIPTOR *pSocket, char *pChunk, unsigned long ulSize);
//static int fnPBPSSocketSend(SOCKET_DESCRIPTOR *pSocket, char *pBuff, int iLength);
//static int fnPBPSSocketReceive(SOCKET_DESCRIPTOR *pSocket, char *pBuff, int iLength);
static int fnPBPSCreateSSLConnection(SOCKET_DESCRIPTOR *pSocket);
static void fnPBPSDestroySSLConnection(SOCKET_DESCRIPTOR *pSocket);
static int fnPBPSGetCurrentCertificateListVersion(void);

typedef int (*PBPS_SSL_LIO_FCN)(unsigned int, void *, int, long);

#define PBPS_NEW_CERT_LIST_MAX_LEN 10
static char pPBPSNewCertListBuff[PBPS_NEW_CERT_LIST_MAX_LEN+1];
static unsigned long iPBPSNewCertListBuffLen = 0;
static int iPBPSNewCertListVer = 0;
static BOOL fPBPSLogVerifyCertError = TRUE;

#define PBPS_CERT_LIST_FILE_ROOT_LEN 8
#define PBPS_MAX_CERT_TEMP_FILENAME_SIZE 32
static char PBPS_CERTLIST_FILE_NAME[16] = "XXXXCERT.BIN";
static char PBPS_CERTLIST_FILE_EXT[5] = ".BIN";


static unsigned char *pPBPSCertBase64Buff = NULL;
static unsigned long iPBPSCertBase64BuffLen = 0;

#define PBPS_MAX_TRUSTED_HOSTS 128
#define PBPS_TRUSTED_HOST_LENGTH 64

static char pPBPSTrustedHostList[PBPS_MAX_TRUSTED_HOSTS][PBPS_TRUSTED_HOST_LENGTH];
static unsigned long lwPBPSTrustedHostListEntries = 0;

static char pbpLog[SSL_LOG_SIZE + PBPS_XML_SEND_BUFF_LEN];

/**********************************
    PBPS Data ID
**********************************/
static unsigned char bPBPSDataID, bPBPSPreviousID;

/**********************************
    Task that last sent a message
**********************************/
static unsigned char bPBPSRequestingTaskID;

/**********************************
    Parsing Data
**********************************/
// The buffer used to store incoming xml data
static unsigned char pParserBuff[PBPS_XML_RECV_BUFF_LEN * 2];

// Used to signal the message that has been received
static unsigned char bMessageTag = 0;

DXMLParser dxmlReceive;
DXMLParser dxmlCertificate;    
DXMLParser dxmlLoadCert;

/**********************************
    Parsing Functions
**********************************/
short fnPBPS_ParserStart();
short fnPBPS_ParserEnd();
static short fnPBPS_StartElement(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_EndElement(XmlChar *pName);
static short fnPBPS_ElementChars(XmlChar *pName, XmlChar *pChars, unsigned long iLen);

/**********************************
    Formatting Data
**********************************/
// Buffer used to store XML to be sent to Data Center
static unsigned char pFormatterBuff[PBPS_XML_SEND_BUFF_LEN + 5];
static unsigned long ulFormatterBuffLen = 0;

/**********************************
    Formatter Functions
**********************************/
static short fnPBPS_Formatter_Start(void);
static short fnPBPS_Formatter_Char(XmlChar x);
static short fnPBPS_Formatter_End(BOOL fValid);

/**********************************
    Forward declarations for
    scripts
**********************************/
static char pPBPTemp[PBP_TEMP_BUFF_LEN];
static unsigned long lwPBPTempBuffLen = 0;
static const char pPBPSBaseSignOnAttr[] = "module=\"Base\"";

extern void putString(char *, int);
extern void *fnPBPS_GetData (unsigned long ulDataID,
                    unsigned long ulDataItemNo,
                    unsigned char bFormatType, 
                    unsigned short *bsize);
extern unsigned long fnPBPS_SendBasePcnInSignOn(unsigned long lCounter);
extern unsigned long fnPBPS_CheckPSDStatusOk(unsigned long lCounter);
extern unsigned long fnPBPS_SendPSDStatusInLSKRsp(unsigned long lCounter);
extern unsigned long fnPBPS_SendRecordInLSKRsp(unsigned long lCounter);
extern unsigned long fnPBPS_SendRootCertLstVerInSignOn(unsigned long lCounter);
extern unsigned long fnSendEffectiveDate(unsigned long lCounter);

extern unsigned long fnPBPS_CheckIfGermanyFFRecRequiredInRefill(unsigned long lCounter);
extern unsigned long fnPBPS_CheckIfGermanyFFRecRequiredInRefund(unsigned long lCounter);
extern unsigned long fnPBPS_CheckIfGermanyFFRecRequiredInAudit(unsigned long lCounter);
extern XMLFormatScript DCAPUplScript2,DCAPUplScript;
extern unsigned long fnPBPSSendIButtonPieceCountInAudit(unsigned long lReserved);
extern XMLFormatScript POPUploadScript;

unsigned long fnPBPS_SendCommType( unsigned long lCounter );
unsigned long fnPBPS_SendUkEibEnabled( unsigned long lCounter );

/**********************************
    The scripts for requests
**********************************/
/*lint -save -e605 -e785 */ 
/* The above line suppresses the following warnings:
"Warning 605: Increase in pointer capability (initialization)" for scripts and
"Info 785: Too few initializers for aggregate" for scripts */

/* Very Important - Update entry count here when script entries are added or removed */
XMLFORM_SIMPLE_SCRIPT( SignOnScript, SignOnReq, 18, pSignOnEntry)
XMLFORM_SIMPLE_SCRIPT( AuditScript, AuditReq, 5, pAuditEntry )
XMLFORM_SIMPLE_SCRIPT( RefillScript, RefillReq, 4, pRefillEntry )
XMLFORM_SIMPLE_SCRIPT( RefundScript, RefundReq, 4, pRefundEntry )
XMLFORM_SIMPLE_SCRIPT( PollIndScript, PollInd, 1, pPollIndEntry )
XMLFORM_SIMPLE_SCRIPT( SignOffScript, SignOffInd, 2, pSignOffEntry )

/**********************************
    The scripts for responses
**********************************/
XMLFORM_SIMPLE_SCRIPT( AuthScript, AuthorizationCfm, 3, pAuthEntry )
XMLFORM_SIMPLE_SCRIPT( NonceScript, EDCNonceRsp, 2, pNonceEntry )
XMLFORM_SIMPLE_SCRIPT( EDCScript, EDCRsp, 2, pEDCEntry )
XMLFORM_SIMPLE_SCRIPT( ConfigScript, PostalConfigCfm, 2, pConfigEntry ) 
XMLFORM_SIMPLE_SCRIPT( RekeyScript, RekeyCfm, 3, pRekeyEntry )
XMLFORM_SIMPLE_SCRIPT( SKRScript, SKeyRecCfm, 2, pSKREntry )
/* sign off rsp ??? */
XMLFORM_SIMPLE_SCRIPT( FileRspScript, FileXferRsp, 2, pFileRspEntry )   
XMLFORM_SIMPLE_SCRIPT( GenRspScript, GenRes, 2, pGenRspEntry )  
XMLFORM_SIMPLE_SCRIPT( GenKeyExchKeyRspScript, GenKeyExcKeyRsp, 3, pGenKeyExchKeyEntry )
XMLFORM_SIMPLE_SCRIPT( LoadSecretKeyRspScript, LoadEncryptedKeyRsp, 3, pLoadSecretKeyEntry )

/**********************************
    Common Scripts
**********************************/
XMLFORM_SIMPLE_SCRIPT(NonceRecScript,PsdEdcNonc,1,pNonceRecEntry)
XMLFORM_SIMPLE_SCRIPT(RekeyRecScript,PskRecKeyReg,1,pRekeyRecEntry)
XMLFORM_SIMPLE_SCRIPT(AuditRecScript,IbiRecAud,1,pAuditRecEntry)
XMLFORM_SIMPLE_SCRIPT(RefillRecScript,PsdRecPvdReq,1,pRefillRecEntry)
XMLFORM_SIMPLE_SCRIPT(RefundRecScript,PsdRecPvr,1,pRefundRecEntry)
XMLFORM_SIMPLE_SCRIPT(FinalizedFrankingRecScript,PsdFFRec,1,pFinalizedFrankingRecEntry)
XMLFORM_SIMPLE_SCRIPT(AuthRecScript,PsdRecAuthRsp,1,pAuthRecEntry)


/**********************************
    Common Script entries - Very Important - Update entry count above when script entries are added or removed

**********************************/
_XMLFORM_START_SCRIPT_ENTRIES( pSignOnEntry )
    _XMLFORM_SIMPLE_TAG( CusPbpAcc, CUSTOMER_ACCT_SIGNON, fnPBPS_GetData)
    _XMLFORM_SIMPLE_TAG( MtrIdtSer, PBP_SERIAL_NUMBER, fnPBPS_GetData)
    _XMLFORM_SIMPLE_TAG( MtrIdtTyp, DEVICE_TYPE_ID, fnPBPS_GetData)
    _XMLFORM_SIMPLE_TAG( MtrIdtVer, VERSION_NUMBER, fnPBPS_GetData)
    _XMLFORM_SIMPLE_TAG( SesIdCur, SIGNON_SESSION_ID, fnPBPS_GetData)
    _XMLFORM_SIMPLE_TAG( MtrDatTim, METER_DATE_TIME_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_TAG( MtrParDpl, METER_DECIMAL_PLACES_ID, fnPBPS_GetData )
	_XMLFORM_SIMPLE_TAG( LocDateTime, METER_DATE_TIME_ID, fnPBPS_GetData )
	_XMLFORM_SIMPLE_TAG( GUID, DCAP_GUID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_OPT_TAG( MsgTmo, NULL, 0, fnPBPS_GetData )
    _XMLFORM_SIMPLE_OPT_TAG( MsgRoutReq, NULL, 0, fnPBPS_GetData )
    _XMLFORM_SIMPLE_OPT_TAG( MtrStaEur, NULL, 0, fnPBPS_GetData )
    _XMLFORM_SIMPLE_OPT_TAG( MtrIdtCcd, NULL, 0, fnPBPS_GetData )
    _XMLFORM_SIMPLE_OPT_TAG( RootCertLstVer, fnPBPS_SendRootCertLstVerInSignOn, CERTIFICATE_LIST_VERSION, fnPBPS_GetData )
    _XMLFORM_SIMPLE_OPT_TAG_ATTR( PrdIdtPcn, fnPBPS_SendBasePcnInSignOn, GET_BASE_PCN, fnPBPS_GetData, GET_BASE_PCN_ATTR, fnPBPS_GetData)
    _XMLFORM_SIMPLE_OPT_TAG( RteDtaEffDat, fnSendEffectiveDate, GET_EFFECTIVE_DATE, fnPBPS_GetData )
	_XMLFORM_SIMPLE_OPT_TAG( CommType, fnPBPS_SendCommType, GET_COMM_TYPE, fnPBPS_GetData )
	_XMLFORM_SIMPLE_OPT_TAG( UKEIBEnabled, fnPBPS_SendUkEibEnabled, GET_UK_EIB_ENABLED_FLAG, fnPBPS_GetData )

	//_XMLFORM_SIMPLE_OPT_TAG( PrdIdtPcn, 1, PROD_IDT_PCN, fnPBPS_GetData )
    
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pPollIndEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pGenRspEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_TAG( GenSta, GENRSP_STATUS_INFO, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pFileRspEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_TAG( CmdStaCmp, FILE_XFER_STATUS, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pSignOffEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_TAG( GenSta, SIGNOFF_STATUS_INFO, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pAuditEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_SUBSCRIPT( AuditRecScript )
    _XMLFORM_SIMPLE_TAG( CusPbpAcc, CUSTOMER_ACCT, fnPBPS_GetData )
    _XMLFORM_SIMPLE_OPT_TAG( PsdPieceCnt, fnPBPSSendIButtonPieceCountInAudit, PBP_PSD_PIECE_COUNT, fnPBPS_GetData )
    _XMLFORM_SIMPLE_OPT_SUBSCRIPT( fnPBPS_CheckIfGermanyFFRecRequiredInAudit, FinalizedFrankingRecScript )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pAuditRecEntry )
    _XMLFORM_SIMPLE_TAG( Base64, AUDIT_REQUEST_RECORD, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pAuthEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_OPT_SUBSCRIPT( fnPBPS_CheckPSDStatusOk, AuthRecScript ) 
    _XMLFORM_SIMPLE_TAG( PsdSta, PSD_STATUS_INFO, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pAuthRecEntry )
    _XMLFORM_SIMPLE_TAG( Base64, AUTH_RESPONSE, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pNonceEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_SUBSCRIPT( NonceRecScript )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pNonceRecEntry )
    _XMLFORM_SIMPLE_TAG( Base64, NONCE_RESPONSE, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pEDCEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_TAG( PsdSta, PSD_STATUS_INFO, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pConfigEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_TAG( PsdSta, PSD_STATUS_INFO, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pRefillEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_SUBSCRIPT( RefillRecScript )
    _XMLFORM_SIMPLE_OPT_SUBSCRIPT( fnPBPS_CheckIfGermanyFFRecRequiredInRefill, FinalizedFrankingRecScript )
    _XMLFORM_SIMPLE_TAG( CusPbpAcc, CUSTOMER_ACCT, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pRefillRecEntry )
    _XMLFORM_SIMPLE_TAG( Base64, REFILL_REQUEST_RECORD, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pRefundEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_SUBSCRIPT( RefundRecScript )
    _XMLFORM_SIMPLE_OPT_SUBSCRIPT( fnPBPS_CheckIfGermanyFFRecRequiredInRefund, FinalizedFrankingRecScript )
    _XMLFORM_SIMPLE_TAG( CusPbpAcc, CUSTOMER_ACCT, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pRefundRecEntry )
    _XMLFORM_SIMPLE_TAG( Base64, REFUND_REQUEST_RECORD, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pFinalizedFrankingRecEntry )
    _XMLFORM_SIMPLE_TAG( Base64, FINALIZED_FRANKING_RECORD, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pRekeyEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData ) 
    _XMLFORM_SIMPLE_OPT_SUBSCRIPT( fnPBPS_CheckPSDStatusOk, RekeyRecScript )
    _XMLFORM_SIMPLE_TAG( PsdSta, PSD_STATUS_INFO, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pRekeyRecEntry )
    _XMLFORM_SIMPLE_TAG( Base64, REKEY_RESPONSE_RECORD, fnPBPS_GetData)
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pSKREntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_TAG( PsdSta, PSD_STATUS_INFO, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

XMLFORM_SIMPLE_SCRIPT( GenKeyExchKeyRecScript, PsdKeyExcRspRec, 1, pGenKeyExchKeyRecEntry )

_XMLFORM_START_SCRIPT_ENTRIES( pGenKeyExchKeyEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_OPT_SUBSCRIPT( fnPBPS_CheckPSDStatusOk, GenKeyExchKeyRecScript )
    _XMLFORM_SIMPLE_TAG( PsdSta, PSD_STATUS_INFO, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pGenKeyExchKeyRecEntry )
    _XMLFORM_SIMPLE_TAG( Base64, KEY_EXCH_KEY_RSP_RECORD, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

XMLFORM_SIMPLE_SCRIPT( LoadSecretKeyRecScript, PsdEncrKeyRspRec, 1, pLoadSecretKeyRecEntry )

_XMLFORM_START_SCRIPT_ENTRIES( pLoadSecretKeyEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_OPT_SUBSCRIPT( fnPBPS_SendRecordInLSKRsp, LoadSecretKeyRecScript )
    _XMLFORM_SIMPLE_OPT_TAG( PsdSta, fnPBPS_SendPSDStatusInLSKRsp, PSD_STATUS_INFO, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pLoadSecretKeyRecEntry )
    _XMLFORM_SIMPLE_TAG( Base64, LOAD_SECRET_KEY_RSP_RECORD, fnPBPS_GetData )
_XMLFORM_END_SCRIPT_ENTRIES()


/****************************************************
 *
 ****************************************************/
/**********************************
    Transaction Record Scripts
**********************************/




/**********************************
    Flag that says we should 
    abort current connection.
**********************************/
static BOOL fPBPSAbortConnection = FALSE;

/**********************************
    Flag that says there is
    a transaction in progress.
***********************************/
#define PBPS_NO_TRANSACTION 0
#define PBPS_AUDIT_TRANSACTION 1
#define PBPS_REFILL_TRANSACTION 2
#define PBPS_REFUND_TRANSACTION 3
static BOOL fPBPSTransaction = PBPS_NO_TRANSACTION;

/**********************************
    Private function that
    determines if it is safe
    to abort yet.
**********************************/
static BOOL fnPBPSSafeToAbort(void);

/***********************************
    Private function to actually
    abort the connection.
***********************************/
static void fnPBPSAbortActiveConnection(void);

/***********************************
    Private Status Variable
***********************************/
static unsigned long lwPBPSStatus = 0;

/***********************************
    Private Status Variable Values
***********************************/
#define PBPS_PERFORM_REFILL                 0x00000001
#define PBPS_PERFORM_REFUND                 0x00000002
#define PBPS_PERFORM_POST_OP_AUDIT          0x00000004
#define PBPS_PERFORM_DCAP_UPLOAD            0x00000008
#define PBPS_PERFORM_CONFSRVC_UPLOAD        0x00000010
#define PBPS_PERFORM_POST_MOVE_AUDIT        0x00000020
#define PBPS_PERFORM_GERMAN_ZERO_REFILL     0x00000040
#define PBPS_PERFORM_ZERO_REFILL_AUDIT      0x00000080
#define PBPS_PERFORM_POP_UPLOAD             0x00000100

#define PBPS_USER_REQ_ACCTBAL           0x00010000
#define PBPS_USER_REQ_REFILL            0x00020000
#define PBPS_USER_REQ_REFUND            0x00040000
#define PBPS_USER_REQ_DCAP_UPLOAD       0x00080000
#define PBPS_USER_REQ_CONFSRVC_UPLOAD   0x00100000
#define PBPS_USER_REQ_METER_MOVE        0x00200000
#define PBPS_DC_SENT_POLL               0x00400000

#define PBPS_GERMAN_ZERO_REFILL_IN_PROGRESS 0x00800000  // DSD 06-17-2005 - FF Rec Fix
// DSD 06-17-2005 - FF Rec Fix
static BOOL fnPBPSIsFFRecXferInProgress(void);
static void fnPBPSSetFFRecXferGuard(void);
static void fnPBPSClearFFRecXferGuard(void);

/* Parses a service request and schecdules whatever is required. */
static int fnPBPSParseServiceRequest(sServiceURL *pService);

/* Called to determine the status of the data center's response. */
static int fnPBPSInvestigateDcRsp(unsigned char bRequestedID,unsigned char bID, int iCode,int iMsgRcvStatus);
/* Called after a successfull message has been received */
static int fnPBPSCommitXmlData(unsigned char bDataID);

/* Determine what service to do now. */
static int fnPBPSIsAuditRequired(unsigned char *pNewState, unsigned char *pNewID);
static int fnPBPSGetNextService(unsigned char *pNewState, unsigned char *pNewID);

/*lint -save -e605 removes Warning 605: Increase in pointer capability (initialization) for table entries */
// !!!!!!! The order of the entries in the following table must match the order of the entries in
//			the data IDs enum in pbptask.h
static const sDataIDEntry TheBigTable[PBPS_NUM_DATAID] = {
/*                                Type      New State       New Data ID             get next state/ID                                   handler         */
/*                                                                                                                                                      */
/* PBPS_NODATAID,           */{ { PNONE,    0,              0,                      NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCSIGNON_RSP,       */{ { PFUNCT,   0,   			0,						fnPBPSIsAuditRequired	},  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCAUDIT_RSP,        */{ { PCONST,   PBPS_WAITPSD,   PBPS_PSDAUDIT_RSP,      NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
///* PBPS_DC_INIT_REFILL,     */
///* PBPS_DC_INIT_REFILL_RSP, */
/* PBPS_DCREFILL_RSP,       */{ { PCONST,   PBPS_WAITPSD,   PBPS_PSDREFILL_RSP,     NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCREFUND_RSP,       */{ { PCONST,   PBPS_WAITPSD,   PBPS_PSDREFUND_RSP,     NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCGENERAL_RSP,      */{ { PCONST,   PBPS_SENDREQ,   PBPS_SENDSIGNOFF_REQ,   NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCSIGNOFF_RSP,      */{ { PCONST,   PBPS_IDLE,      PBPS_NODATAID,          NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCFILEXFER_RSP,     */{ { PFUNCT,   0,              0,                      fnPBPSGetNextService    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCSIGNON_REQ,       */{ { PCONST,   PBPS_RCVRSP,    PBPS_DCSIGNON_RSP,      NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&SignOnScript    },
/* PBPS_DCAUDIT_REQ,        */{ { PCONST,   PBPS_RCVRSP,    PBPS_DCAUDIT_RSP,       NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&AuditScript     },
/* PBPS_DCREFILL_REQ,       */{ { PCONST,   PBPS_RCVRSP,    PBPS_DCREFILL_RSP,      NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&RefillScript    },
/* PBPS_DCREFUND_REQ,       */{ { PCONST,   PBPS_RCVRSP,    PBPS_DCREFUND_RSP,      NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&RefundScript    },
/* PBPS_DCSEND_POLL,        */{ { PCONST,   PBPS_RCVREQ,    PBPS_NODATAID,          NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&PollIndScript   },
/* PBPS_SENDSIGNOFF_REQ,    */{ { PCONST,   PBPS_RCVRSP,    PBPS_DCSIGNOFF_RSP,     NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&SignOffScript   },
/* PBPS_DCAP_UPLOAD,        */{ { PCONST,   PBPS_RCVRSP,    PBPS_DCFILEXFER_RSP,    NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&DCAPUplScript	},
/* PBPS_DCAUTH_REQ,         */{ { PCONST,   PBPS_WAITPSD,   PBPS_PSDAUTH_REQ,       NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCNONCE_REQ,        */{ { PCONST,   PBPS_WAITPSD,   PBPS_PSDNONCE_REQ,      NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCEDC_REQ,          */{ { PCONST,   PBPS_WAITPSD,   PBPS_PSDEDC_REQ,        NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCCONFIG_REQ,       */{ { PCONST,   PBPS_WAITPSD,   PBPS_PSDCONFIG_REQ,     NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCREKEY_REQ,        */{ { PCONST,   PBPS_WAITPSD,   PBPS_PSDREKEY_REQ,      NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCSKR_REQ,          */{ { PCONST,   PBPS_WAITPSD,   PBPS_PSDSKR_REQ,        NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCRCV_POLL,         */{ { PFUNCT,   0,              0,                      fnPBPSGetNextService    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCSIGNOFF_REQ,      */{ { PCONST,   PBPS_SENDRSP,   PBPS_SENDSIGNOFF_RSP,   NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCAP_RULES,         */{ { PCONST,   PBPS_SENDRSP,   PBPS_SENDDCFILEXFER_RSP,NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_CERTLIST,           */{ { PCONST,   PBPS_SENDRSP,   PBPS_SENDDCFILEXFER_RSP,NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DCAUTH_RSP,         */{ { PCONST,   PBPS_RCVREQ,    PBPS_NODATAID,          NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&AuthScript      },
/* PBPS_DCNONCE_RSP,        */{ { PCONST,   PBPS_RCVREQ,    PBPS_NODATAID,          NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&NonceScript     },
/* PBPS_DCEDC_RSP,          */{ { PCONST,   PBPS_RCVREQ,    PBPS_NODATAID,          NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&EDCScript       },
/* PBPS_DCCONFIG_RSP,       */{ { PCONST,   PBPS_RCVREQ,    PBPS_NODATAID,          NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&ConfigScript    },
/* PBPS_DCREKEY_RSP,        */{ { PCONST,   PBPS_RCVREQ,    PBPS_NODATAID,          NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&RekeyScript     },
/* PBPS_DCSKR_RSP,          */{ { PCONST,   PBPS_RCVREQ,    PBPS_NODATAID,          NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&SKRScript       },
/* PBPS_SENDSIGNOFF_RSP,    */{ { PCONST,   PBPS_IDLE,      PBPS_NODATAID,          NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)NULL             },
/* PBPS_SENDDCFILEXFER_RSP, */{ { PCONST,   PBPS_RCVREQ,    PBPS_NODATAID,          NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&FileRspScript   },
/* PBPS_SENDGEN_RSP         */{ { PCONST,   PBPS_RCVREQ,    PBPS_NODATAID,          NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&GenRspScript    },
/* PBPS_PSDAUDIT_REQ,       */{ { PCONST,   PBPS_SENDREQ,   PBPS_DCAUDIT_REQ,       NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_PSDAUDIT_RSP,       */{ { PFUNCT,   0,              0,                      fnPBPSGetNextService    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_PSDAUTH_REQ,        */{ { PCONST,   PBPS_SENDRSP,   PBPS_DCAUTH_RSP,        NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_PSDNONCE_REQ,       */{ { PCONST,   PBPS_SENDRSP,   PBPS_DCNONCE_RSP,       NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_PSDEDC_REQ,         */{ { PCONST,   PBPS_SENDRSP,   PBPS_DCEDC_RSP,         NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_PSDCONFIG_REQ,      */{ { PCONST,   PBPS_SENDRSP,   PBPS_DCCONFIG_RSP,      NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_PSDREKEY_REQ,       */{ { PCONST,   PBPS_SENDRSP,   PBPS_DCREKEY_RSP,       NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_PSDSKR_REQ,         */{ { PCONST,   PBPS_SENDRSP,   PBPS_DCSKR_RSP,         NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_PSDREFILL_REQ,      */{ { PCONST,   PBPS_SENDREQ,   PBPS_DCREFILL_REQ,      NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
///* PBPS_PSDREFILL_RSP,    */{ { PFUNCT,   0,              0,                      fnPBPSGetNextService    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_PSDREFILL_RSP,      */{ { PCONST,   PBPS_WAITPSD,   PBPS_PSDAUDIT_REQ,      NULL    				},  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_PSDREFUND_REQ,      */{ { PCONST,   PBPS_SENDREQ,   PBPS_DCREFUND_REQ,      NULL                    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_PSDREFUND_RSP,      */{ { PFUNCT,   0,              0,                      fnPBPSGetNextService    },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_PSD_GEN_KEY_EXCH_KEY    */{ { PCONST,   PBPS_SENDRSP,   PBPS_DC_GEN_KEY_EXCH_KEY_RSP,   NULL        },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_PSD_LOAD_SECRET_KEY     */{ { PCONST,   PBPS_SENDRSP,   PBPS_DC_LOAD_SECRET_KEY_RSP,    NULL        },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DC_GEN_KEY_EXCH_KEY     */{ { PCONST,   PBPS_WAITPSD,   PBPS_PSD_GEN_KEY_EXCH_KEY,      NULL        },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DC_GEN_KEY_EXCH_KEY_RSP */{ { PCONST,   PBPS_RCVREQ,    PBPS_NODATAID,                  NULL        },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&GenKeyExchKeyRspScript  },
/* PBPS_DC_LOAD_SECRET_KEY      */{ { PCONST,   PBPS_WAITPSD,   PBPS_PSD_LOAD_SECRET_KEY,       NULL        },  PBPS_NO_HANDLER,                    (void*)NULL             },
/* PBPS_DC_LOAD_SECRET_KEY_RSP  */{ { PCONST,   PBPS_RCVREQ,    PBPS_NODATAID,                  NULL        },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&LoadSecretKeyRspScript  },
#ifndef JANUS
/* PBPS_POP_UPLOAD,             */{ { PCONST,   PBPS_RCVRSP,    PBPS_DCFILEXFER_RSP,   NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)&POPUploadScript },
#else
/* PBPS_POP_UPLOAD,             */{ { PCONST,   PBPS_RCVRSP,    PBPS_DCFILEXFER_RSP,   NULL                    },  PBPS_FORMATTER_SEND_HANDLER,        (void*)NULL          },  // not ported yet for Janus
#endif
};
/*lint -restore */

/**********************************
    Janus Integration Support
**********************************/
static uchar fnGetPsdTypeIndex(void);
static ulong fnNormalizePsdStatus(ulong ulPsdStatus);

/**********************************
    PSD Data
**********************************/
// The buffer used to send/recieve data between the PBP task and BOB
static unsigned char pPSDCommonBuff[COMMON_BUFF_LEN];
static unsigned char pTmpPSDCommonBuff[COMMON_BUFF_LEN];
// Buffer(s) to hold temporary records
static unsigned long lwPSDFFRecBuffLen = 0;
static unsigned char pPSDFFRecBuff[COMMON_BUFF_LEN];

/* inits some structures */
static void fnPBPS_InitParserData(void);

/* parser handler functions */
static short fnPBPS_SesIdCur_ETag(XmlChar *pName);
static short fnPBPS_CmdStaCmp_ETag(XmlChar *pName);
static short fnPBPS_Base64_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_Base64_Chars(XmlChar *pName, XmlChar *pChars, unsigned long iLen);
static short fnPBPS_Base64_ETag(XmlChar *pName);
static short fnPBPS_CusBalDeb_ETag(XmlChar *pName);
static short fnPBPS_CusBalCre_ETag(XmlChar *pName);
static short fnPBPS_SesIdNew_ETag(XmlChar *pName);
static short fnPBPS_PbpActNum_ETag(XmlChar *pName);
static short fnPBPS_RisuPvdAmt_ETag(XmlChar *pName);
static short fnPBPS_CodOrgPst_ETag(XmlChar *pName);
static short fnPBPS_PrtIndNum_ETag(XmlChar *pName);
static short fnPBPS_IndLangCod_ETag(XmlChar *pName);
static short fnPBPS_DcnStaCmp_ETag(XmlChar *pName);
static short fnPBPS_DcnInfTim_ETag(XmlChar *pName);
static short fnPBPS_CusRecRef_ETag(XmlChar *pName);
static short fnPBPS_TowNamOne_Etag(XmlChar *pName);
static short fnPBPS_TowNamTwo_Etag(XmlChar *pName);
static short fnPBPS_TowNamThree_Etag(XmlChar *pName);
static short fnPBPS_TowNamFour_Etag(XmlChar *pName);
static short fnPBPS_GUID_ETag(XmlChar *pName);
static short fnPBPS_CurrencyID_ETag(XmlChar *pName);


/* Trusted Root Certificate handlers - SSL */
static short fnPBPS_RootCertListVer_STag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_RootCertListVer_Chars(XmlChar *pName, XmlChar *pChars, unsigned long iLen);
static short fnPBPS_RootCertListVer_Etag(XmlChar *pName);

/* special tags */
static short fnPBPS_SignOnRsp_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_SignOffInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_SignOffRsp_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_PollInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_GenRsp_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_DcpDnl_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_FileXferRsp_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_AuditRsp_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_AuthorizationInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_EDCInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_EDCNonceReq_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_PostalConfigInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_RefillRsp_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_RefundRsp_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_RekeyInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_SKeyRecInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_GenKeyExcKeyInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_LoadEncryptedKeyInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_RootCertList_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_PsdRecPvdErr_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);

/* generic functions (for buffer management) */
static short fnPBPS_Generic_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr);
static short fnPBPS_Generic_Chars(XmlChar *pName, XmlChar *pChars, unsigned long iLen);


/* parser element lookup table. */
const ElementLookupEntry PBPS_XmlLookupTbl[] = 
{
/******************************
    data capture elements
*******************************/
// TDT - copied this table from dcaputil.c
{(XmlChar *)"DcpDnl", { fnPBPS_DcpDnl_Stag, NULL, fnDCAPHandleDcpDnlETag }}, // TDT

{(XmlChar *)"DcpParPcnTyp", {NULL,NULL,NULL}},
{(XmlChar *)"DcpParPcnNam", {NULL,NULL,NULL}},
{(XmlChar *)"DcpParPcnVal", {NULL,NULL,NULL}},
//{(XmlChar *)"DcpUplPerMgt", { NULL, NULL, NULL }}, // TDT
{(XmlChar *)"DcpUplPerMgt", { NULL, NULL, fnDCAPHandleDcpUplPerMgtETag }}, // TDT

{(XmlChar *)"DcpGmtSynDat", { NULL, NULL, NULL }}, // TDT
{(XmlChar *)"DcpGmtSynTim", { NULL, NULL, NULL }}, // TDT
{(XmlChar *)"DcpCreLim", { NULL, NULL, NULL }}, // TDT
{(XmlChar *)"DcpNxtUpl", { NULL, NULL, NULL }}, // TDT
//{(XmlChar *)"DcpNxtUplDat", { NULL, NULL, NULL }}, // TDT
{(XmlChar *)"DcpNxtUplDat", { NULL, fnDCAPHandleNxtUplDatChars, NULL }}, // TDT
//{(XmlChar *)"DcpNxtUplTim", { NULL, NULL, NULL }}, // TDT
{(XmlChar *)"DcpNxtUplTim", { NULL, fnDCAPHandleNxtUplTimChars, NULL }}, // TDT
//{(XmlChar *)"DcpPerEnd", { NULL, NULL, NULL }}, // TDT
{(XmlChar *)"DcpPerEnd", { fnDCAPHandleDcpPerEndSTag, NULL, fnDCAPHandleDcpPerEndETag }}, // TDT

//{(XmlChar *)"DcpGraDay", { NULL, NULL, NULL }}, // TDT
//{(XmlChar *)"DcpPerEndDat", { NULL,NULL,NULL }}, // TDT
//{(XmlChar *)"DcpPerEndTim", { NULL,NULL,NULL }}, // TDT
{(XmlChar *)"DcpGraDay", { fnDCAPHandleDcpGraDaySTag, fnDCAPHandleDcpGraDayChars, fnDCAPHandleDcpGraDayETag }}, // TDT
{(XmlChar *)"DcpPerEndDat", { NULL,fnDCAPHandleDcpPerEndDatChars,NULL }}, // TDT
{(XmlChar *)"DcpPerEndTim", { NULL,fnDCAPHandleDcpPerEndTimChars,NULL }}, // TDT

{(XmlChar *)"DcpMdtMapPcn",{NULL,NULL,NULL}},
{(XmlChar *)"DcpMdtMapSid",{NULL,NULL,NULL}},
{(XmlChar *)"DcpMdtMapEnt",{NULL,NULL,NULL}},
 
/*********************************
    general response elements
**********************************/
/* GenSta has status of general response */
{(XmlChar *)"GenSta", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_CmdStaCmp_ETag}},

/*********************************
    Sign On Response elements
**********************************/
/* SesIdNew - the new session id */
{(XmlChar *)"SesIdNew", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_SesIdNew_ETag}},
/* DcnInfTim - data center time */
{(XmlChar *)"DcnInfTim", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_DcnInfTim_ETag}},
/* MsgTmo - message time out (data center ) */
{(XmlChar *)"MsgTmo", {NULL,NULL,NULL}},
/* HstFeaTyp - host feature type */
{(XmlChar *)"HstFeaTyp", {NULL,NULL,NULL}},
/* CusPbpAcc - customer PBP account number */
{(XmlChar *)"CusPbpAcc", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_PbpActNum_ETag}},

/********************************
    Audit Response Elements
*********************************/
/* IbiRecAudRsp - the Audit Response Record */
{(XmlChar *)"IbiRecAudRsp", {NULL,NULL,NULL}},
/* RisuPvdAmt - do a refill for this much (reissue) */
{(XmlChar *)"RisuPvdAmt", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_RisuPvdAmt_ETag}},
{(XmlChar *)"CurrId", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_CurrencyID_ETag}},


/*********************************
    Authorization Request Elements
**********************************/
/* PsdRecKey - a Comet PSD Key record */
{(XmlChar *)"PsdRecKey", {NULL,NULL,NULL}},

/*********************************
    EDC Request Elements
**********************************/
/* DcnRecEdcPub - the edc record */
{(XmlChar *)"DcnRecEdcPub", {NULL,NULL,NULL}},

/*********************************
    Postal Config Request
**********************************/
/* IbiRecCfgPst - Postal Config record */
{(XmlChar *)"IbiRecCfgPst", {NULL,NULL,NULL}},
/* CodOrgPst - Origin Postal Code */
{(XmlChar *)"CodOrgPst", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_CodOrgPst_ETag}},
{(XmlChar *)"PrtIndNum", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_PrtIndNum_ETag}},
{(XmlChar *)"IndLangCod", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_IndLangCod_ETag}},
{(XmlChar *)"CusRecRef", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_CusRecRef_ETag}},
//Merged from janus for Slovakia Town Name    
{(XmlChar *)"TowNamOne",{fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_TowNamOne_Etag}},
{(XmlChar *)"TowNamTwo",{fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_TowNamTwo_Etag}},
{(XmlChar *)"TowNamThree",{fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_TowNamThree_Etag}},
{(XmlChar *)"TowNamFour",{fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_TowNamFour_Etag}},
{(XmlChar *)"GUID",{fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_GUID_ETag}},
/*********************************
    Refill Response Elements
**********************************/
/* PsdRecPvd - good refill record */
{(XmlChar *)"PsdRecPvd", {NULL,NULL,NULL}},
/* PsdRecPvdErr - bad refill record */
{(XmlChar *)"PsdRecPvdErr", {fnPBPS_PsdRecPvdErr_Stag,NULL,NULL}},

/*********************************
    Refund Response Elements
**********************************/
/* DcnStaCmp - command completion status ??? */
{(XmlChar *)"DcnStaCmp", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_DcnStaCmp_ETag}},
/* PsdRecPvrAuth - refund record */
{(XmlChar *)"PsdRecPvrAuth", {NULL,NULL,NULL}},

/*********************************
    Rekey Request Elements
**********************************/
/* PsdRecParDsa - rekey record */
{(XmlChar *)"PsdRecParDsa", {NULL,NULL,NULL}},

/*********************************
    Load SKR Request Elements
**********************************/
/* PsdRecKeySgn - the SKR */
{(XmlChar *)"PsdRecKeySgn", {NULL,NULL,NULL}},

/*********************************
    SSL Elements
**********************************/
{(XmlChar *)"RootCertListVer",{fnPBPS_RootCertListVer_STag,fnPBPS_RootCertListVer_Chars,fnPBPS_RootCertListVer_Etag}},

/*********************************
    Shared Elements
**********************************/
/* SesIdCur - the session id */
{(XmlChar *)"SesIdCur", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_SesIdCur_ETag}},
/* CusBalDeb - customre Debit Balance (AcctBalanceRsp,AuditRsp) */
{(XmlChar *)"CusBalDeb", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_CusBalDeb_ETag}},
/* CusBalCre - customer Credit Balance (AcctBalanceRsp,AuditRsp) */
{(XmlChar *)"CusBalCre", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_CusBalCre_ETag}},
/* CmdStaCmp - command completion status (SignOnRsp, FileXferRsp) */
{(XmlChar *)"CmdStaCmp", {fnPBPS_Generic_Stag,fnPBPS_Generic_Chars,fnPBPS_CmdStaCmp_ETag}},
/* Base64 - contians binary data encoded in base 64. */
{(XmlChar *)"Base64", {fnPBPS_Base64_Stag,fnPBPS_Base64_Chars,fnPBPS_Base64_ETag}},

/*********************************
    Special start tags
**********************************/
{(XmlChar *)"SignOnRsp", {fnPBPS_SignOnRsp_Stag,NULL,NULL}},
{(XmlChar *)"SignOffInd", {fnPBPS_SignOffInd_Stag,NULL,NULL}},
{(XmlChar *)"SignOffRsp", {fnPBPS_SignOffRsp_Stag,NULL,NULL}},
{(XmlChar *)"PollInd", {fnPBPS_PollInd_Stag,NULL,NULL}},
{(XmlChar *)"GenRsp", {fnPBPS_GenRsp_Stag,NULL,NULL}},
{(XmlChar *)"FileXferInd", {NULL,NULL,NULL}},
{(XmlChar *)"FileXferRsp", {fnPBPS_FileXferRsp_Stag,NULL,NULL}},
{(XmlChar *)"AuditRsp", {fnPBPS_AuditRsp_Stag,NULL,NULL}},
{(XmlChar *)"AuthorizationInd", {fnPBPS_AuthorizationInd_Stag,NULL,NULL}},
{(XmlChar *)"EDCInd", {fnPBPS_EDCInd_Stag,NULL,NULL}},
{(XmlChar *)"EDCNonceReq", {fnPBPS_EDCNonceReq_Stag,NULL,NULL}},
{(XmlChar *)"PostalConfigInd", {fnPBPS_PostalConfigInd_Stag,NULL,NULL}},
{(XmlChar *)"RefillRsp", {fnPBPS_RefillRsp_Stag,NULL,NULL}},
{(XmlChar *)"RefundRsp", {fnPBPS_RefundRsp_Stag,NULL,NULL}},
{(XmlChar *)"RekeyInd", {fnPBPS_RekeyInd_Stag,NULL,NULL}},
{(XmlChar *)"SKeyRecInd", {fnPBPS_SKeyRecInd_Stag,NULL,NULL}},
{(XmlChar *)"GenKeyExcKeyInd", {fnPBPS_GenKeyExcKeyInd_Stag,NULL,NULL}},
{(XmlChar *)"LoadEncryptedKeyInd", {fnPBPS_LoadEncryptedKeyInd_Stag,NULL,NULL}},
{(XmlChar *)"RootCertList",{fnPBPS_RootCertList_Stag,NULL,NULL}}
};

/*********************************
    Globally Available variable
    containing the number of
    entries in the PBP Task's
    parser lookup table.
**********************************/
const short sPBPNumXmlParserTblEntries = ARRAY_LENGTH(PBPS_XmlLookupTbl);

#define COMET_METER_DEVICE 20
#define COMET_WITH_DATA_CAPTURE 21

#define I_BUTTON_METER_DEVICE 24
#define I_BUTTON_WITH_DATA_CAPTURE 25

#ifdef HORIZON_DCAP
#define I_BUTTON_METER_DEVICE_ACCPT_ACCT        29
#define I_BUTTON_WITH_DATA_CAPTURE_ACCPT_ACCT   29
#else
#define I_BUTTON_METER_DEVICE_ACCPT_ACCT        26
#define I_BUTTON_WITH_DATA_CAPTURE_ACCPT_ACCT   27
#endif

#define PBP_MAX_SESSIONID_LEN 50
// per session id
static char cCurrSessionID[PBP_MAX_SESSIONID_LEN];
// per message id
static char cSessionID[PBP_MAX_SESSIONID_LEN];
// command completion status
static int iCmdStaCmp;
// balances
static BOOL fDebitBal,fCreditBal;
static double dblDebitBal,dblCreditBal;
// Audit Rsp;
static sAuditRsp AuditRec;
// Load Postal Config
extern char CMOSPrintedIndiciaNum[PRINTED_INDICIA_NUM_SZ];
extern char CMOSIndiciaLanguageCode[INDICIA_LANGUAGE_CODE_SZ];
extern char CMOSCustomerID_RRN[CUSTOMERID_RRN_SZ];

/**********************************
    HTTP output stuff
**********************************/
/* used to store header for
all http messages sent to server */
static char cHttpHeaderBuff[512];

/**********************************
    HTTP parsing stuff
**********************************/
/* The size of buffer for an HTTP header line. */
#define HTTP_HEADER_LINE_LEN 256
/* The buffer for an HTTP header line. */
static char cHttpLineBuff[HTTP_HEADER_LINE_LEN + 1];

/* The definition of the states of the HTTP parser. */
enum {
HEADER_S1, /* we are processing a Http header line. */
HEADER_S2, /* we have found a CR after the line */
HEADER_S3, /* we have found a CR LF after the line */
HEADER_S4, /* we have found a CR LF CR after the line */
CHUNKLEN_S1, /* we have found the CR LF CR LF (a blank line).  If chunked we are finding the chunk length */
CHUNKLEN_S2, /* we have found a CR after the chunk length */
CHUNKLEN_S3, /* we are processing the chunk extension after the chunk length */
CHUNK_S1, /* we are processing the a chunk */
CHUNK_S2, /* we have found the CR that signals the end of a chunk */
BODY_S1, /* we are processing a non-chunked body */
HTTP_DONE, /* there is no more body (chunked or non-chunked) */
NUM_HTTP_STATES /* MUST BE LAST!!! this is a place holder for # of states */
};

/* Flag that says the Http body is chunked. */
static BOOL fHttpChunked = FALSE;
/* Flag that says the Http body is not chunked and it's length if found in ulHttpBodyLen. */
static BOOL fHttpContentLen = FALSE;
/* ulHttpBodyLen - size of body or chunk.
ulHttpBodyLeft - the number bytes still in the body or chunk. */
static unsigned long ulHttpBodyLen = 0, ulHttpBodyLeft = 0;
/* Current line of HTTP message */
static unsigned long ulHttpCurrLine = 0;
/* Does HTTP response contain Connection: close header ? */
static BOOL fHttpCloseConnection = FALSE;
static BOOL fXMLParserError = FALSE;
/* Response used status code 100 */
static BOOL fHttpContinueRspFound = FALSE;
/* variable to hold the status code in
response. */
static int iHttpStatusCode = 0;
/* is the status code 4xx or 5xx */
static BOOL fHttpBadStatus = FALSE;
/* is the char just passed to parser in
the body of the message */
static BOOL fHttpIsBodyChar = FALSE;
/* the state of the http parser */
static unsigned char bHttpState;
/* function that initializes all
the variables used by the http 
parser */
static void fnPBPS_InitHttpParserData(void);
/* send a char in the message to
the http parser */
static int fnPBPS_SendHttpParserChar(char x);

/************************************
    Error handling.
************************************/
/* The task error handling functions */
static void fnPBPS_ErrSignOffClose(int, unsigned char , unsigned char , unsigned char , unsigned char );
static void fnPBPS_ErrGotoIdle(int, unsigned char , unsigned char , unsigned char , unsigned char );
static void fnPBPS_ErrEndDialog(int, unsigned char , unsigned char , unsigned char , unsigned char );
static void fnPBPS_ErrContinueRsp(int, unsigned char , unsigned char , unsigned char , unsigned char );
static void fnPBPS_ErrContinueSingleRsp(int iErr, unsigned char bDataID, unsigned char bReserved2, unsigned char bReserved3, unsigned char bReserved4);

/**********************************************************
1. Socket Errors (timeout,can't send/recieve) - can't 
continue communication, just close connection and
return  to Idle state.

2. XML Errors
2a. Formatter errors while sending a message - Close the
connection and return to idle state.
2b. Parse errors while receiving a message
2b1 If in control of dialog - Send Sign Off message and
close the connection.
2b2 If not in control of dialog - Send GenRsp message to
Data Center and wait for Sign Off message from the Data
Center.

3. PSD Errors
3a. If there is an error handling mechanism for dialog -
Do what it says.
3b. If there is no error handling mechanism
3b1. If in control of dialog - Send Sign Off message and
close the connection.
3b2. If not in control of dialog - Send GenRsp message to
Data Center and Wait for a Sign Off message.

4. Procedure Errors
4a. If in control of dialog - Send Sign Off message and
close the connection.
4b. If not in control of dialog - Send GenRsp message to
Data Center and Wait for a Sign Off message.
**********************************************************/
static const sPBPS_ErrEntry sPBPSErrTable[] = {
{fnPBPS_ErrEndDialog,0,0,0,0},      /* PBPS_PARSE_ERR                        */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_XMLDATAID_ERR,                   */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_BAD_SERVICEID,                   */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_UNABLE_CONNECT,                  */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_PSDAUDITREQ_ERR,                 */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_PSDAUDITRSP_ERR,                 */
{fnPBPS_ErrContinueSingleRsp,PBPS_DCAUTH_RSP,0,0,0},                /* PBPS_PSDAUTHRSP_ERR,                  */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_PSDNONCE_ERR,                    */
{fnPBPS_ErrContinueSingleRsp,PBPS_DCEDC_RSP,0,0,0},                 /* PBPS_PSDEDC_ERR,                      */
{fnPBPS_ErrContinueSingleRsp,PBPS_DCCONFIG_RSP,0,0,0},              /* PBPS_PSDCONFIG_ERR,                   */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_PSDREFILLREC_ERR,                */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_PSDREFILLRSP_ERR,                */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_PSDREFUNDREC_ERR,                */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_PSDREFUNDRSP_ERR,                */
{fnPBPS_ErrContinueSingleRsp,PBPS_DCREKEY_RSP,0,0,0},               /* PBPS_PSDREKEYRSP_ERR,                 */
{fnPBPS_ErrContinueRsp,PBPS_DCSKR_RSP,PBPS_RCVREQ,PBPS_NODATAID,0}, /* PBPS_PSDSKRRSP_ERR,                   */
{fnPBPS_ErrEndDialog,0,0,0,0},      /* PBPS_PROCEDURE_ERR,                   */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_UNKNOWNURL_ERR,                  */
{fnPBPS_ErrEndDialog,0,0,0,0},      /* PBPS_XMLFORMAT_ERR,                   */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_SOCKETSEND_ERR,                  */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_SOCKETRECV_ERR,                  */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_SOCKETTIMEOUT_ERR,               */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DCGENRSP_ERR,                    */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_DC_BADRESPONSE_ERR,              */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DCSIGNON_ERR,                    */
{fnPBPS_ErrEndDialog,0,0,0,0},      /* PBPS_DCSESSIONID_ERR,                 */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DCAUDITREC_ERR,                  */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DCREFILL_ERR,                    */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DCREFUND_ERR,                    */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DCAUTH_ERR,                      */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DCEDC_ERR,                       */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DCCONFIG_ERR,                    */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DCREKEY_ERR,                     */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DCSKR_ERR,                       */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_HTTP_PARSE_ERR,                  */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_HTTP_UNEXPECTED_CLOSE           */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_HTTP_CLOSE_EXPECTED             */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_PVD_ERROR_RECORD             */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_UNKNOW_RSP_STATUS */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_NO_ACCOUNT                   */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_NO_METER                     */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_NO_LINK                      */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_COMM_TIMEOUT                 */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_CASHBOX_CONN_ERR             */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_ICEBOX_CONN_ERR              */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_SERVER_EXCEPTION             */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_PUBLIC_KEY_MISSING           */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_SERVER_PARSE_ERR             */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_MSG_OUTOFCONTEXT             */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_MSG_MISSINGPARAM             */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_MSG_PARAMFORMAT_ERR          */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_MSG_PARAMDATA_ERR            */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_SVR_RESEND_FILE              */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_SVR_INCOMPLETE_FILE          */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_SVR_BAD_FILE_TYPE            */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_SVR_WHAT_FILE_TYPE           */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_SVR_BAD_DELCON_ZIP           */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_SVR_DUP_DELCON_REC           */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_SERVICE_SCHEDULER_ERR           */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_INSUFFICIENT_FUNDS_IN_ACCT      */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_PSD_GEN_KEY_EXCH_KEY_ERR        */
{fnPBPS_ErrContinueSingleRsp,PBPS_DC_LOAD_SECRET_KEY_RSP,0,0,0},    /* PBPS_PSD_LOAD_SECRET_KEY_ERR         */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_GEN_KEY_EXCH_KEY_ERR         */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_DC_LOAD_SECRET_KEY_ERR          */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_METER_REFUND_NOT_ALLOWED        */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_XML_GENERAL_ERR                 */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_CERT_LIST_ERROR                 */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_CERT_LIST_BASE64_ERROR          */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_CERT_LIST_FILE_OPEN_ERROR       */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_CERT_LIST_FILE_WRITE_ERROR      */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_CERT_LIST_FILE_READ_ERROR       */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_CERT_LIST_FILE_SEEK_ERROR       */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_CERT_LIST_FILE_VERIFY_ERROR     */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_SSL_OUT_OF_MEMORY               */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_SSL_VERIFY_CERT_ERROR           */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_INVALID_SSL_EMD_PARAM           */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_CONNECT_UNTRUSTED_HOST_ERR      */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_DC_SEND_PBP_ACCT_NUM_ERR        */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_PSD_FFREC_ERR                   */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_PSD_SET_REFILL_AMT_ERR          */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_PROCESS_XML_TEMPLATE            */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_PSD_REFILL_NOT_ALLOWED          */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_USING_BACKUP_URLS               */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_DNS_CANT_RESOLVE_DISTRIBUTOR    */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_DISTRIBUTOR_CONNECTION_FAILED   */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_DC_GUID_MISMATCH                */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_DC_NO_GUID                      */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_CRASH                           */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_GENERAL_FAILURE,				*/
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_SSL_CONTEXT_CREATE_ERROR,		*/
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_HOST_LOOKUP_ERROR,     		    */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_CERT_LOAD_ERROR,       		    */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_TLS_CLIENT_METHOD_ERROR,        */
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_TLS_USE_SNI_ERROR,       		*/
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_NO_ALLOWED_CIPHERS,       		*/
{fnPBPS_ErrGotoIdle,0,0,0,0},       /* PBPS_SET_CIPHERS_ERROR,       		*/
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_CERT_LIST_DOWNLOAD_VERSION_OLD, */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_SIG_VERIFY_UNAVAILABLE,		    */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_CERT_LIST_FILE_STAT_ERROR,		*/
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_CERT_LIST_FILE_CLOSE_ERROR,	    */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_CERT_LIST_FILE_RENAME_ERROR,	*/
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_CERT_LIST_FILE_UNAVAILABLE,	    */
{fnPBPS_ErrSignOffClose,0,0,0,0},   /* PBPS_CERT_LIST_FILE_NOT_SIGNED, 	    */
};

static unsigned long lwErrorStatus = PBPS_OK;
static unsigned long ulPSDLastStatus = 0;
static unsigned long ulDCStatus = 0;
static BOOL fDCPvdErrorRecord = FALSE;
static unsigned short wPSDLastMode = 0;
static BOOL fPsdPhcModeOk = FALSE;
static unsigned char bPBPSRefillType = PSD_REFILL_TYPE_DEFAULT;
static BOOL fMeterMoveDone = FALSE;
static BOOL fPvdDone = FALSE;
static BOOL fPvrDone = FALSE;
static BOOL fAuditDone = FALSE;
static BOOL fDcapUploadDone = FALSE;
static BOOL fDelcUploadDone = FALSE;
static BOOL fSendGermanFFRecInAudit = FALSE;
static BOOL fDoingFinalizeFranking = FALSE;

extern BOOL fnAllowedPsdPhcMoods(ushort opCode);
extern BOOL fnVerifyFileSignature( char *fileName, long *dataOffset, long *dataSize );
extern void dottedQuadToArray(unsigned char *array, char *dotttedQuad);
 extern error_t fnDCAPTimerExpired(void);
extern void registerUrl(CRACKED_URL *originalUrl);
extern unsigned char fnGetSupportedCountryID(void);


#define C_BLK_IBI_PVD_ERROR_RECORD 0x1002001A

//-----------------------------------------------------------------------------
// The function fnGetPsdTypeIndex() converts each of the many PSD types into an 
//  index used to access the tables below.  
//
//  NUM_PSD_INDICES is the number of entries each of the following tables must support.
//
//  NOTE: Each new PSD type does NOT require a new table entry, as  long as the table
//        entries for an existing PSD type will suffice for the new PSD type.
//
//  If NUM_PSD_TYPES is changed, not only do the tables below have to be updated, the
//  switch statement in fnGetPsdTypeIndex() must also be updated.
//
#define NUM_PSD_INDICES   2

// Define psd specific status codes for a few conditions:.......... myko/low-cost/cygnus, asteroid/gemini
const UINT32 PSD_STATUS_OK_TBL[ NUM_PSD_INDICES ] =                         { 0x00000000,0x00009000,  }; 
const UINT32 PSD_STATUS_ERR_DO_REISSUE_TBL[ NUM_PSD_INDICES ] =             { 0x0C00802B,0x0000691A,  }; 
const UINT32 PSD_FUNDSMGR_INSUFFICIENT_FUNDS_TBL[ NUM_PSD_INDICES ] =       { 0x0B008028,0x0000691C,  }; 
const UINT32 PSD_FUNDSMGR_PSD_NO_WITHDRAW_TBL[ NUM_PSD_INDICES ] =          { 0x0B008015,0x0000691C,  };
const UINT8  PSD_REFILL_TYPE_DEFAULT_TBL[ NUM_PSD_INDICES ] =               {     0,         1,       };
const UINT8  PSD_REFILL_TYPE_FRENCH_CREDIT_LIMIT_TBL[ NUM_PSD_INDICES ] =   {     3,         3,       };

// Likewise define the psd device type for sign on identification:...
const unsigned char PSD_SIGN_ON_DEVICE_TYPE_ID[ NUM_PSD_INDICES ][ 4 ] = 
{   // meterid,             meterid_with_datcap,        meterid_accpt_acct,                 meterid_with_datcap_accpt_acct]
// myko/low-cost/cygnus:
    {COMET_METER_DEVICE,    COMET_WITH_DATA_CAPTURE,    COMET_METER_DEVICE,                 COMET_WITH_DATA_CAPTURE},
// asteroid/gemini:
    {I_BUTTON_METER_DEVICE, I_BUTTON_WITH_DATA_CAPTURE, I_BUTTON_METER_DEVICE_ACCPT_ACCT, I_BUTTON_WITH_DATA_CAPTURE_ACCPT_ACCT}, 
};
                                                                        
/******************************************************************************
   FUNCTION NAME: fnGetPsdTypeIndex
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Convert PSD type to index for these tables:
                : PSD_STATUS_OK_TBL
                : PSD_STATUS_ERR_DO_REISSUE_TBL
                : PSD_FUNDSMGR_INSUFFICIENT_FUNDS_TBL
                : PSD_FUNDSMGR_PSD_NO_WITHDRAW_TBL
                : PSD_REFILL_TYPE_DEFAULT_TBL
                : PSD_REFILL_TYPE_FRENCH_CREDIT_LIMIT_TBL
                : PSD_SIGN_ON_DEVICE_TYPE_ID

   PARAMETERS   : None
   RETURN       : index value
   NOTES        : none
******************************************************************************/
uchar fnGetPsdTypeIndex(void)
{
    uchar ucIdx;

    switch (fnGetCurrentPsdType())
    {
        case PSDT_IBUTTON:
        case PSDT_GEMINI:
            ucIdx = 1;
            break;

        default:
            ucIdx = 0;
            break;
    }

    return(ucIdx);  
}

/******************************************************************************
   FUNCTION NAME: fnNormalizePsdStatus
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : If psd is not a myko AND psd status is sucessful, then normalize the
                : status code to myko success (i.e. 0), otherwise leave the native 
                : psd status code as is.
   PARAMETERS   : None
   RETURN       : index value
   NOTES        : Yea I know...but this is what the DC wants.
******************************************************************************/
ulong fnNormalizePsdStatus(ulong ulPsdStatus)
{
    ulong ulRetPsdStatus;
    UINT8   ubPsdType;

    ubPsdType = fnGetCurrentPsdType();
    
    if(   ((ubPsdType & PSDT_MYKO_TYPE) == 0) 
       && (ulPsdStatus == PSD_STATUS_OK_TBL[ fnGetPsdTypeIndex() ]) )
    {
        ulRetPsdStatus = PSD_STATUS_OK_TBL[0];
    }
    else
    {
        ulRetPsdStatus = ulPsdStatus;
    }

    return(ulRetPsdStatus);
}

/******************************************************************************
   FUNCTION NAME: fnGetNumXtraPsdStatBytes
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Determine the number of PSD status bytes prepended to certain
                : psd responses, depending on the vault type. 
   PARAMETERS   : None
   RETURN       : index value
   NOTES        : none
******************************************************************************/
static uchar fnGetNumXtraPsdStatBytes(void)
{
    uchar ucIdx;

    switch( fnGetCurrentPsdType() )
    {
        case PSDT_IBUTTON:
        case PSDT_GEMINI: 
            ucIdx = 0;
            break;

        default:
            ucIdx = 4;
            break;
    }

    return(ucIdx); 
}

/**************************************
    Some PSD stuff
**************************************/
/* structure passed to bob to get/send psd records */
static struct TransferControlStructure psdTransferControl;
extern struct TransferControlStructure *edmXferStructPtr;
extern struct TransferControlStructure *localXferCrtlStructPtr;
struct TransferControlStructure tmpPsdTransferControl;

/* Map from current data id to psd error. */
static const int sPSDMsgInfoTbl[PBPS_NUM_PSD_DATAID] = 
{
    PBPS_PSDAUDITREQ_ERR,
    PBPS_PSDAUDITRSP_ERR,
    PBPS_PSDAUTHRSP_ERR,
    PBPS_PSDNONCE_ERR,
    PBPS_PSDEDC_ERR,
    PBPS_PSDCONFIG_ERR,
    PBPS_PSDREKEYRSP_ERR,
    PBPS_PSDSKRRSP_ERR,
    PBPS_PSDREFILLREC_ERR,
    PBPS_PSDREFILLRSP_ERR,
    PBPS_PSDREFUNDREC_ERR,
    PBPS_PSDREFUNDRSP_ERR,
    PBPS_PSD_GEN_KEY_EXCH_KEY_ERR,
    PBPS_PSD_LOAD_SECRET_KEY_ERR
};

/* PSD Handlers */
static int fnPBPSHandleWaitPSDSimpleInfraReq(unsigned char bMsg);
static int fnPBPSHandleWaitPSDSimpleUicReq(unsigned char bMsg);
static int fnPBPSHandleWaitPSDComplexInfraReq(unsigned char bMsg, BOOL fEDMCompatable);
static int fnPBPSHandleWaitPSDKeyMsg(unsigned short sMsgID);
static int fnPBPSHandleWaitPSDNonce(unsigned char bMsg);
static int fnPBPSHandleWaitPSDRecord(unsigned short wRecordId, int iErrCode);

/* Some file system stuff. */
#define MAINFFS_REG_PATH_SZ 9

/**********************************************************/
#ifdef PBPTASK_INTEGRATION

int fnPBPIntegrationTest(unsigned char bType)
{
        
    return PBPS_OK;
}
#endif

void dbgStateName(unsigned char bPBPSState)
{
	switch (bPBPSState)
	{
	case PBPS_IDLE:
		dbgMsg("  *** [PBPS_IDLE] ***", bPBPSState, __LINE__);
		break;
	case PBPS_RCVRSP:
		dbgMsg("  *** [PBPS_RCVRSP] ***", bPBPSState, __LINE__);
		break;
	case PBPS_SENDREQ:
		dbgMsg("  *** [PBPS_SENDREQ] ***", bPBPSState, __LINE__);
		break;
	case PBPS_RCVREQ:
		dbgMsg("  *** [PBPS_RCVREQ] ***", bPBPSState, __LINE__);
		break;
	case PBPS_SENDRSP:
		dbgMsg("  *** [PBPS_SENDRSP] ***", bPBPSState, __LINE__);
		break;
	case PBPS_WAITPSD:
		dbgMsg("  *** [PBPS_WAITPSD] ***", bPBPSState, __LINE__);
		break;
	case PBPS_NUM_STATES:
		dbgMsg("  *** [PBPS_NUM_STATES] ***", bPBPSState, __LINE__);
		break;
	default:
		dbgMsg("  *** [PBPS_UNKNOWN] ***", bPBPSState, __LINE__);
	}
}

/**********************************************************
Function: fnPBPServicesTask
Author: Derek DeGennaro
Description: 
The entry point in the task.  This function waits for
intertask messages and acts on them based on tables.
Errors propogate back to this function and this function
calls error handling routines.

Parameters:

Return Value:

**********************************************************/
void fnPBPServicesTask (unsigned long argc, void *argv)
{
    INTERTASK msg;
    int iStatus = PBPS_OK, iIndex;
    BOOL fWaitForPsdMessage;
    char fReceivedIntertask;

    fnSysLogTaskStart( "PBPServicesTask", PBPTASK );  
#ifdef PBPTASK_INTEGRATION
    fnPBPIntegrationTest(0);
#endif

    /* init task variables */
    fnPBPSInitTaskData();
    if (DISTTASK_Waiting)
    {
    	dbgTrace(0, "-->-->--> PBPServicesTask Calling DISTTASK To Wake Up! <--<--<--");
        (void)OSSendIntertask( bPBPSRequestingTaskID, PBPTASK, OIT_SERVICE_COMPLETE, LONG_DATA, PBPS_CRASH, sizeof(UINT32) );
    }

    fnSysLogTaskLoop( "PBPServicesTask", PBPTASK );  
    while (1) /*lint !e716 */
    {
        if (iStatus == PBPS_OK && fPBPSAbortConnection == TRUE && fnPBPSSafeToAbort() == TRUE)
        {
            fnPBPSAbortActiveConnection();
            fPBPSAbortConnection = FALSE;
        }
        else 
        {
            dbgMsg("PbpE#", iStatus, __LINE__);
        }

        dbgStateName(bPBPSState);
        switch (bPBPSState)
        {
        case PBPS_IDLE:
            if (bPBPSPreviousState != PBPS_IDLE)
                iStatus = fnPBPSHandleIdle();

            if (iStatus == PBPS_OK)
            {
                fReceivedIntertask = OSReceiveIntertask (PBPTASK, &msg, OS_SUSPEND);
                if (fReceivedIntertask == SUCCESSFUL)
                {
                    /* Record the task that sent the message. */
                    bPBPSRequestingTaskID = msg.bSource;
                    
                    /* Do processing based on the message. */
                    if (msg.bMsgId == PBP_SERVICE_URL && msg.bMsgType == GLOBAL_PTR_DATA && msg.bSource == DISTTASK)
                        iStatus = fnPBPSPerformService(&msg);
                    else if (msg.bMsgId == REMOTE_REFILL_REQUEST && msg.bMsgType == GLOBAL_PTR_DATA)
                        iStatus = fnPBPSPerformRemoteRefill(&msg);                  
                    else
                        iStatus = PBPS_PROCEDURE_ERR;
                }
                else
                    iStatus = PBPS_PROCEDURE_ERR;

                dbgMsg("PBPS_IDLE PbpE#", iStatus, __LINE__);

                /* This task shouldn't get PART_PTR_DATA, but if it does release it. */
                if (fReceivedIntertask == SUCCESSFUL && msg.bMsgType == PART_PTR_DATA)
                    (void)OSReleaseMemory(msg.IntertaskUnion.PointerData.pData);
            }
            else
            {
                dbgMsg("PBPS_IDLE PbpE#", iStatus, __LINE__);
            }
            break;

        case PBPS_RCVRSP:
            iStatus = fnPBPSHandleRcvRsp();
            dbgMsg("PBPS_RCVRSP PbpE#", iStatus, __LINE__);
            break;

        case PBPS_SENDREQ:
            iStatus = fnPBPSHandleSendReq();
            dbgMsg("PBPS_SENDREQ PbpE#", iStatus, __LINE__);
            break;

        case PBPS_RCVREQ:
            iStatus = fnPBPSHandleRcvReq();
            dbgMsg("PBPS_RCVREQ PbpE#", iStatus, __LINE__);
            break;

        case PBPS_SENDRSP:
            iStatus = fnPBPSHandleSendRsp();
            dbgMsg("PBPS_SENDRSP PbpE#", iStatus, __LINE__);
            break;

        case PBPS_WAITPSD:
            iStatus = fnPBPSHandleWaitPSD(&fWaitForPsdMessage);
            dbgMsg("PBPS_WAITPSD PbpE#", iStatus, __LINE__);
            if (fWaitForPsdMessage)
            {
                fReceivedIntertask = OSReceiveIntertask (PBPTASK, &msg, OS_SUSPEND);
                if (fReceivedIntertask == SUCCESSFUL && msg.bSource == SCM && msg.bMsgType == BYTE_DATA)
                {
                    iStatus = fnPBPSHandlePSDMsg(&msg);
                    dbgMsg("PBPS_WAITPSD PbpE#", iStatus, __LINE__);
                }
                else
                    iStatus = PBPS_PROCEDURE_ERR;

                /* This task shouldn't get PART_PTR_DATA, but if it does release it. */
                if (fReceivedIntertask == SUCCESSFUL && msg.bMsgType == PART_PTR_DATA)
                    (void)OSReleaseMemory(msg.IntertaskUnion.PointerData.pData);
            }
            break;

        default:
            iStatus = PBPS_PROCEDURE_ERR;
            break;
        };

        if (iStatus != PBPS_OK)
        {
            /* An error has occurred */
            dbgMsg("fnPBPServicesTask PbpE#", iStatus, __LINE__);

            /* Store the error */
            lwErrorStatus = (unsigned long)iStatus;

			/* Log the offending error and KILL the PBPTASK and Restart it*/
			fnPBPSLogError(iStatus);

            /* if there is a error routine specified
            then call it, else just return to idle. */
            iIndex = iStatus - XML_USER_ERROR_BASE;
            if (iIndex >= 0 && iIndex < NELEMENTS(sPBPSErrTable) && sPBPSErrTable[iIndex].fcnHandle != NULL)
            {
            	dbgTrace(0, "-->-->--> fnPBPServicesTask sPBPSErrTable PbpE#:%d, %d, %d, %d, %d <--<--<--",
            			iStatus, sPBPSErrTable[iIndex].b1, sPBPSErrTable[iIndex].b2, sPBPSErrTable[iIndex].b3, sPBPSErrTable[iIndex].b4);
                sPBPSErrTable[iIndex].fcnHandle(iStatus,
                                                sPBPSErrTable[iIndex].b1,
                                                sPBPSErrTable[iIndex].b2,
                                                sPBPSErrTable[iIndex].b3,
                                                sPBPSErrTable[iIndex].b4);
            }
            else
            {
            	dbgTrace(0, "-->-->--> fnPBPServicesTask sPBPSErrTable Unknown PbpE#:%d <--<--<--", iStatus);
                (void)fnPBPSGotoState(PBPS_IDLE,PBPS_NODATAID);
            }

            /* Set iStatus to PBPS_OK to prevent infinite loop */
            iStatus = PBPS_OK;
            /* Clear Abort connection flag when an error occurs. */
            fPBPSAbortConnection = FALSE;
        }
    } //while
}

/**********************************************************
Function: fnPBPSInitTaskData
Author: Derek DeGennaro
Description:
Initialized task variables.

Parameters:

Return Value:

**********************************************************/
static void fnPBPSInitTaskData(void)
{
    extern BOOL psdClockFlag;   
    
    bPBPSState = PBPS_IDLE;
    bPBPSDataID = PBPS_NODATAID;
    bPBPSPreviousID = PBPS_NODATAID;
    bPBPSPreviousState = PBPS_IDLE;
    lwErrorStatus = PBPS_OK;
    *cCurrSessionID = 0;
    *cSessionID = 0;
    dblDebitBal = 0.0;
    dblCreditBal = 0.0;
    lwPBPSStatus = 0;
    ulFormatterBuffLen = 0;
    fDCPvdErrorRecord = FALSE;
    fPBPSAbortConnection = FALSE;
    fPBPSTransaction = PBPS_NO_TRANSACTION;
    fMeterMoveDone = FALSE;
    fPvdDone = FALSE;
    fPvrDone = FALSE;
    fDcapUploadDone = FALSE;
    fDelcUploadDone = FALSE;
    fAuditDone = FALSE;
    fSendGermanFFRecInAudit = FALSE;
    fDoingFinalizeFranking = FALSE;

    // Clear the Data Center status in the Bob Task
    ulDCStatus = 0;
    if( fnWriteData(BOBAMAT0, BOBS_LAST_DCSTATUS, &ulDCStatus) != BOB_OK )  
    {
        fnProcessSCMError();
    } 

    bPBPSRefillType = PSD_REFILL_TYPE_DEFAULT_TBL[fnGetPsdTypeIndex()];
    (void)fnPBPSInitializeSocket(&pbpSocket);

    iPBPSNewCertListVer = 0;
    pPBPSCertBase64Buff = NULL;
    iPBPSCertBase64BuffLen = 0;
    fPBPSLogVerifyCertError = TRUE;
    
    /* Initialize the certificate list file name.  The
    file name depends on the PCN of the UIC.  The
    file name is defined with the first 4 characters allocated
    for the PCN. */
    memcpy((void*)PBPS_CERTLIST_FILE_NAME,(void*)CMOSSignature.bUicPcn,4);
    
    memset((void*)pPBPSTrustedHostList,0,sizeof(pPBPSTrustedHostList));
    lwPBPSTrustedHostListEntries = 0;
    /* 
    Wait for system RTC to be set.  The RTC is set by the
    bob task after reading the time from the PSD. 
    */
    while (!psdClockFlag)
        OSWakeAfter(100);   /* TODO: figure out a better way to do this */

    /* 
    Wait for file system, nuc net, and ssl lib to initialize. 
    */
}

/**********************************************************
Function: fnPBPSInitTaskDataEachServiceRequest
Author: Derek DeGennaro
Description:
Initialized task variables.  This function is called each
time the pbp task is told to perform a service.  It is
called before actually performing the service.

Parameters:

Return Value:

**********************************************************/
static int fnPBPSInitTaskDataEachServiceRequest(void)
{
    *cCurrSessionID = 0;
    lwErrorStatus = PBPS_OK;
    dblDebitBal = 0.0;
    dblCreditBal = 0.0;
    lwPBPSStatus = 0;
    AuditRec.fReissue = FALSE;
    AuditRec.dblReissueAmt = 0.0;
    ulPSDLastStatus = 0;
    fDCPvdErrorRecord = FALSE;
    fPBPSTransaction = PBPS_NO_TRANSACTION;
    fMeterMoveDone = FALSE;
    fPvdDone = FALSE;
    fPvrDone = FALSE;
    fDcapUploadDone = FALSE;
    fDelcUploadDone = FALSE;
    fAuditDone = FALSE;
    fSendGermanFFRecInAudit = FALSE;
    fDoingFinalizeFranking = FALSE;

    // clear the Data Center status in the Bob Task
    ulDCStatus = 0;
    if( fnWriteData(BOBAMAT0, BOBS_LAST_DCSTATUS, &ulDCStatus) != BOB_OK )  
    {
        fnProcessSCMError();
    }


    if (fnValidData(BOBAMAT0,PSD_REFILL_TYPE,&bPBPSRefillType) != BOB_OK)
        bPBPSRefillType = PSD_REFILL_TYPE_DEFAULT_TBL[fnGetPsdTypeIndex()]; /* error getting the type - set to default */

    (void)fnPBPSInitializeSocket(&pbpSocket);

    iPBPSNewCertListVer = 0;
    pPBPSCertBase64Buff = NULL;
    iPBPSCertBase64BuffLen = 0;
    fPBPSLogVerifyCertError = TRUE;
    
    memset((void*)pPBPSTrustedHostList,0,sizeof(pPBPSTrustedHostList));
    lwPBPSTrustedHostListEntries = 0;

    return PBPS_OK;
}
static sServiceURL pbpRemoteRefillService;

static int fnPBPSPerformRemoteRefill(INTERTASK *pMsg)
{
    int iStatus = PBPS_OK;
    unsigned long lwRemoteRefillAmt = 0;
    REMOTE_REFILL_INFO *remoteRefillInfo = NULL;
    unsigned char bMoneyDecimals;
    char cProgMsg[MAX_INTERTASK_BYTES]="";
    unsigned long *pLongData;
    char *pCode = "C1";
    int correctedDecimalPlaces = 1;

    /* Check Inputs */
    if (pMsg == NULL || pMsg->bMsgType != GLOBAL_PTR_DATA || pMsg->IntertaskUnion.PointerData.pData == NULL)
        return PBPS_PROCEDURE_ERR;
    
/*     if(pMsg == NULL)                                */
/*         return PBPS_PROCEDURE_ERR;                  */
/*     if(pMsg->bMsgType != GLOBAL_PTR_DATA)           */
/*         return PBPS_PROCEDURE_ERR;                  */
/*  if(pMsg->IntertaskUnion.PointerData.pData == NULL) */
/*         return PBPS_PROCEDURE_ERR;                  */
    
    /* Reset task data. */
    (void)fnPBPSInitTaskDataEachServiceRequest();

    /* Re-init the socket descriptor */
    (void)fnPBPSInitializeSocket(&pbpSocket);
    
    /* Init the service struct */
    remoteRefillInfo = (REMOTE_REFILL_INFO *)pMsg->IntertaskUnion.PointerData.pData;
    (void)memset((void*)&pbpRemoteRefillService,0,sizeof(pbpRemoteRefillService));
    (void)strcpy((char*)pbpRemoteRefillService.pServiceID,(const char*)"Refill");
    (void)strcpy((char*)pbpRemoteRefillService.pServiceURL,(const char*)remoteRefillInfo->serverUrl);   
    
    /* Try to set the refill amount */
    if (fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *) &bMoneyDecimals) != BOB_OK)
        return PBPS_PSD_SET_REFILL_AMT_ERR;
    lwRemoteRefillAmt = (unsigned long)atoi((const char*)remoteRefillInfo->amount);
    
    // adjust the decimal places
    while(bMoneyDecimals--)
        correctedDecimalPlaces *= 10;

    lwRemoteRefillAmt *= (unsigned long)correctedDecimalPlaces;    
    
    if (fnWriteData((unsigned short)BOBAMAT0, (unsigned short)VAULT_REFILL_AMT, (void *)(&lwRemoteRefillAmt)) != BOB_OK)
        return PBPS_PSD_SET_REFILL_AMT_ERR;
    
    /* Determine what is being requested. */
    iStatus = fnPBPSParseServiceRequest(&pbpRemoteRefillService);

    if (iStatus == PBPS_OK)
    {
        /* Determine the IP address of the Postage By Phone Server. */
        iStatus = fnPBPSGetServerAddress(&pbpSocket,&pbpRemoteRefillService);
    
        if (iStatus == PBPS_OK)
        {
            /* Create the socket connection to the Postage By Phone Server. */
            iStatus = fnPBPSCreateSocket(&pbpSocket);

            if (iStatus == PBPS_OK)
            {
                /* Connect to the Postage By Phone server. */
                iStatus = fnPBPSConnectSocket(&pbpSocket);

                if (iStatus == PBPS_OK)
                {               
                    /* The connection has been made now sign on. */
                    iStatus = fnPBPSGotoState(PBPS_SENDREQ,PBPS_DCSIGNON_REQ);
                }
            }
        }   
    }

    if (iStatus != PBPS_OK)
    {
        /* 
        There was an error in the service request or while
        connecting to the Postage By Phone server.  Abort 
        the service request, inform the distributor task,
        and log the error. Also release any resources 
        obtained during the connection attempt.
        */
        fnPBPSDestroySocket(&pbpSocket);    

        pLongData = (unsigned long*)cProgMsg;
        *pLongData = (unsigned long)iStatus;
        (void)OSSendIntertask( pMsg->bSource, PBPTASK, OIT_SERVICE_COMPLETE, LONG_DATA, cProgMsg, sizeof(unsigned long) );

    }
    
    return iStatus;
}

/**********************************************************
Function: fnPBPSPerformService
Author: Derek DeGennaro
Description:
Function that handles the message from the distributor
task that says to perform a service.  This function
is responsible for making the socket connection with the
server.

Parameters:

Return Value:

**********************************************************/
static int fnPBPSPerformService(INTERTASK *pMsg)
{
    int iStatus = PBPS_OK;  
    char cProgMsg[MAX_INTERTASK_BYTES]="";
    unsigned long *pLongData;
    sServiceURL *pService;
    char *pCode = "C1";

    /* Reset task data. */
    (void)fnPBPSInitTaskDataEachServiceRequest();

    /* Re-init the socket descriptor */
    (void)fnPBPSInitializeSocket(&pbpSocket);
    
    /* Determine what is being requested. */
    pService = (sServiceURL *)pMsg->IntertaskUnion.PointerData.pData;
    iStatus = fnPBPSParseServiceRequest(pService);

    if (iStatus == PBPS_OK)
    {
        /* Determine the IP address of the Postage By Phone Server. */
        iStatus = fnPBPSGetServerAddress(&pbpSocket,pService);
    
        if (iStatus == PBPS_OK)
        {
            /* Create the socket connection to the Postage By Phone Server. */
            iStatus = fnPBPSCreateSocket(&pbpSocket);

            if (iStatus == PBPS_OK)
            {
                /* Connect to the Postage By Phone server. */
                iStatus = fnPBPSConnectSocket(&pbpSocket);

                if (iStatus == PBPS_OK)
                {               
                    /* The connection has been made now sign on. */
                    iStatus = fnPBPSGotoState(PBPS_SENDREQ,PBPS_DCSIGNON_REQ);
                }
            }
        }   
    }

    if (iStatus != PBPS_OK)
    {
        /* 
        There was an error in the service request or while
        connecting to the Postage By Phone server.  Abort 
        the service request, inform the distributor task,
        and log the error. Also release any resources 
        obtained during the connection attempt.
        */
        fnPBPSDestroySocket(&pbpSocket);    

        pLongData = (unsigned long*)cProgMsg;
        *pLongData = (unsigned long)iStatus;
        (void)OSSendIntertask( pMsg->bSource, PBPTASK, OIT_SERVICE_COMPLETE, LONG_DATA, cProgMsg, sizeof(unsigned long) );

        //send message to tablet with error status
        //TODO - see if general message can be sent
        if (strcmp(pService->pServiceID,"AcctBal") == 0)
        {
    	   //send_AccountBalanceRsp(API_STATUS_DATA_CENTER_COMM_FAIL, 0.0, 0.0);
    	   iStatus = setDnldException(API_STATUS_DATA_CENTER_COMM_FAIL, iStatus);
        }


    }

    return iStatus;
}

/**********************************************************
Function: fnPBPSHandleRcvRsp
Author: Derek DeGennaro
Description:
This function is called when the task enters the
receive response state.

Parameters:

Return Value:

**********************************************************/
static int fnPBPSHandleRcvRsp(void)
{
    int iStatus = PBPS_OK;
    
    /* Get the response from the data center. */
    iStatus = fnPBPSReceiveMsg();

    if (iStatus != PBPS_OK)
    {
        //send message to tablet with error status
        //TODO - see if general message can be sent
        if ((bPBPSDataID == PBPS_DCSIGNON_RSP) || (bPBPSDataID == PBPS_DCAUDIT_RSP))
        {
    	   //send_AccountBalanceRsp(API_STATUS_DATA_CENTER_COMM_FAIL, 0.0, 0.0);
    	   iStatus = setDnldException(API_STATUS_DATA_CENTER_COMM_FAIL, iStatus);
        }

        //incase of failure clear the progress flag
		if(fUploadInProgress == TRUE)
		{
			trmUploadFailed(iStatus);
		}
    }


    /* Examine the response. */
    iStatus = fnPBPSInvestigateDcRsp(bPBPSDataID,bMessageTag,iCmdStaCmp,iStatus);

    if (iStatus == PBPS_OK || (iStatus == PBPS_DC_GUID_MISMATCH) || (iStatus == PBPS_DC_NO_GUID))/* no error parsing or receiving */
    {
    	GUIDStatus = iStatus;
        /* commit any data */
        iStatus = fnPBPSCommitXmlData(bPBPSDataID);

        /* The next state and ID are based on the current data id */
        if (iStatus == PBPS_OK || (iStatus == PBPS_DC_GUID_MISMATCH) || (iStatus == PBPS_DC_NO_GUID))
            iStatus = fnPBPSGotoNextState(bPBPSDataID);     
    }
    
    return iStatus;
}

/**********************************************************
Function: fnPBPSHandleSendReq
Author: Derek DeGennaro
Description:
Function called when the state enters the Send Request
state.

Parameters:

Return Value:

**********************************************************/
static int fnPBPSHandleSendReq(void)
{
    int iStatus = PBPS_OK;
    /* we want to send a request to the DC */

    /* 
    DSD 06-17-2005 - FF Rec Fix
    If we are about to send a finalized franking record
    then set a flag to guard the transaction against
    power failure or network connection errors. 
    */
    if (fnPBPSIsFFRecXferInProgress())
        fnPBPSSetFFRecXferGuard();  

    iStatus = fnPBPSSendMsg(bPBPSDataID);

    if (iStatus == PBPS_OK)     /* No errors sending request */
    {
        iStatus = fnPBPSGotoNextState(bPBPSDataID);
    }
        
    return iStatus;
}
/**********************************************************
Function: fnPBPSHandleRcvReq
Author: Derek DeGennaro
Description:
Function that is called when the task enters the 
receive request state.

Parameters:

Return Value:

**********************************************************/
static int fnPBPSHandleRcvReq(void)
{
    int iStatus = PBPS_OK;
    /* wait for dc to send us a request */

    /*
    parse the XML.  the parser handlers
    must determine what type of request this is.
    the data id of the response to send
    back is based on what type of request
    this is.  we also may have to wait for
    the psd to process the request.
    */
    iStatus = fnPBPSReceiveMsg();

    dbgMsg("fnPBPSHandleRcvReq PbpE#", iStatus, __LINE__);

//#if 0 //TODO JAH remove dcap
    if (fnDCAPIsDataCaptureActive() == TRUE)
    {
        /* Tell DCAPI that we are done downloading the rules. */
        if( bDCAP_DnlStarted ) // TDT - 
        {
            //TODO JAH Remove dcapi.c
            bDCAP_DnlStarted = FALSE;
        }
    }
//#endif //TODO JAH remove dcap

    if (iStatus == PBPS_OK)/* no error with parse or receiving */
    {            
        /* make sure the DC sent us a message we understand */
        if (bMessageTag == PBPS_NODATAID)
            iStatus =  PBPS_DC_BADRESPONSE_ERR;
        else if (bMessageTag == PBPS_DCGENERAL_RSP)
            iStatus =  PBPS_DCGENRSP_ERR;
        
        if (iStatus == PBPS_OK)/* the contents of the message were ok */
        {
            bPBPSDataID = bMessageTag;

            /* commit any data */
            iStatus = fnPBPSCommitXmlData(bPBPSDataID);
            
            if (iStatus == PBPS_OK)
                iStatus = fnPBPSGotoNextState(bPBPSDataID);
        }
    }    

    return iStatus;
}
/**********************************************************
Function: fnPBPSHandleSendRsp
Author: Derek DeGennaro
Description:
Function that is called when the task enters the 
send response state.

Parameters:

Return Value:

**********************************************************/
static int fnPBPSHandleSendRsp(void)
{
    int iStatus = PBPS_OK;

    /* send a response to the data center */
    iStatus = fnPBPSSendMsg(bPBPSDataID);

    if (iStatus == PBPS_OK)     /* There were no error sending the response */
    {
        iStatus = fnPBPSGotoNextState(bPBPSDataID);
    }

    return iStatus;
}

/**********************************************************
Function: fnPBPSHandleWaitPSD
Author: Derek DeGennaro
Description:
Function that is called when the task enters the wait psd
state.

Parameters:

Return Value:

**********************************************************/
static int fnPBPSHandleWaitPSD(BOOL *pWaitForIntertask)
{
    int iStatus = PBPS_OK;
    //unsigned char bEMDGermanyZeroRefill = FALSE, 
    unsigned char bEMDGermanyFinalizedFranking = FALSE;
    unsigned char bBobWriteStatus=BOB_OK;
    unsigned char bEncrKeyType; 


    /* make sure the data ID is valid */
    if (bPBPSDataID < PBPS_FIRST_PSD_DATAID || bPBPSDataID > PBPS_LAST_PSD_DATAID || pWaitForIntertask == NULL)
        return PBPS_PROCEDURE_ERR;

    /* by default the response is sent in an
    intertask message. exceptions are handled
    in separately. */
    *pWaitForIntertask = TRUE;
    
    /* set all known Bob Xfer Control Structures since they
    have been getting out of sync lately */
    localXferCrtlStructPtr = &psdTransferControl;
    edmXferStructPtr = &psdTransferControl;
    
    /* Set up the intertask message to Bob. */
    switch (bPBPSDataID) 
    {
    case PBPS_PSDAUDIT_REQ:
#ifdef JANUS
        /*
        I-button based UIC's perform the FF After the Refund and send it with the Audit.
        Check to see if this is an I-button based UIC and if the German FF functionality
        is enabled in the EMD.
        */
        bEMDGermanyFinalizedFranking = fnFlashGetByteParm(BP_PBP_PERFORM_FINALIZED_FRANKING);       
        if(   bEMDGermanyFinalizedFranking 
           && (fnGetCurrentPsdType() & PSDT_ALL_IBUTTON) )
        {
            unsigned long ulPsdState = 0;
            BOOL fPsdStateRead;             
            if ( (fAuditDone == FALSE) || (fSendGermanFFRecInAudit == TRUE) )
            {
                /* To guard against a power-failure situation
                the FF is done during the first Audit when the PSD is in the
                withdrawn state. */ 
                fPsdStateRead = fnGetPsdState(&ulPsdState);             
                if (ulPsdState == eIPSDSTATE_GERM_WITHDRAWN)
                {           
                    // Use the Generic PBPTask-fnWriteData interface.
                    // This interface utilizes the Xfer Ctrl structure.
                    lwPSDFFRecBuffLen = 0;
                    (void)memset((void*)pPSDFFRecBuff,0,sizeof(pPSDFFRecBuff));
                    iStatus = fnPBPSHandleWaitPSDRecord(PSD_PBP_FINAL_FRANK_REC,PBPS_PSD_FFREC_ERR);            
                    if (iStatus != PBPS_OK)
                        return iStatus;
                        
                    // Save the record because the Xfer Ctrl structure
                    // and buffers now need to be used for the refill
                    // request record.
                    lwPSDFFRecBuffLen = psdTransferControl.bytesWrittenToDest[0];
                    (void)memcpy((void*)pPSDFFRecBuff,pPSDCommonBuff,lwPSDFFRecBuffLen);
                    (void)memset((void*)pPSDCommonBuff,0,sizeof(pPSDCommonBuff));
                    
                    // Re-set the Bob task transfer control structures just in
                    // case they were accidentally changed by the Bob task
                    // during the finalized franking processing.
                    localXferCrtlStructPtr = &psdTransferControl;
                    edmXferStructPtr = &psdTransferControl;
                    
                    /*
                    This is the first Audit being done and the 
                    PSD is in the withdrawn state so set the flag so that 
                    the Finalized Franking Record is generated with the next
                    Audit record and sent in the next Audit Request message. 
                    */
                    fSendGermanFFRecInAudit = TRUE;
                }
            }
        }
#endif
        iStatus = fnPBPSHandleWaitPSDSimpleUicReq(GET_AUDIT_RECORD);        
        if (fPBPSTransaction == PBPS_NO_TRANSACTION)    
            fPBPSTransaction = PBPS_AUDIT_TRANSACTION;
        return iStatus;
    case PBPS_PSDAUDIT_RSP:
        iStatus = fnPBPSHandleWaitPSDSimpleInfraReq(AUDIT_RSP_RECORD);
        return iStatus;
    case PBPS_PSDREFILL_REQ:
        iStatus = fnPBPSHandleWaitPSDSimpleUicReq(GET_PVD_RECORD);      
        fPBPSTransaction = PBPS_REFILL_TRANSACTION;
        return iStatus;
    case PBPS_PSDREFILL_RSP:
        iStatus = fnPBPSHandleWaitPSDSimpleInfraReq(PVD_RSP_RECORD);
        return iStatus;     
    case PBPS_PSDREFUND_REQ:
        /* 
        Check to see if it is necessary to send a German
        Finalized Franking record along with the Refund (PVR)
        Request.        
        */
        if( !(fnGetCurrentPsdType() & PSDT_ALL_IBUTTON) )
        {
            /*
            Currently the myko PSD based UIC's perform the FF before generating the Refund Request
            and send the FF with the Refund Request message.
            */
            bEMDGermanyFinalizedFranking = fnFlashGetByteParm(BP_PBP_PERFORM_FINALIZED_FRANKING);
            if (bEMDGermanyFinalizedFranking)
            {
                // Use the Generic PBPTask-fnWriteData interface.
                // This interface utilizes the Xfer Ctrl structure.
                lwPSDFFRecBuffLen = 0;
                (void)memset((void*)pPSDFFRecBuff,0,sizeof(pPSDFFRecBuff));
                iStatus = fnPBPSHandleWaitPSDRecord(PSD_PBP_FINAL_FRANK_REC,PBPS_PSD_FFREC_ERR);            
                if (iStatus != PBPS_OK)
                    return iStatus;
                    
                // Save the record because the Xfer Ctrl structure
                // and buffers now need to be used for the refill
                // request record.
                lwPSDFFRecBuffLen = psdTransferControl.bytesWrittenToDest[0];
                (void)memcpy((void*)pPSDFFRecBuff,pPSDCommonBuff,lwPSDFFRecBuffLen);
                (void)memset((void*)pPSDCommonBuff,0,sizeof(pPSDCommonBuff));
                
                // Re-set the Bob task transfer control structures just in
                // case they were accidentally changed by the Bob task
                // during the finalized franking processing.
                localXferCrtlStructPtr = &psdTransferControl;
                edmXferStructPtr = &psdTransferControl;
            }
        }
        else
        {
            /*
            I-button based UIC's perform the FF After the Refund and send it with the Audit.
            So set the flag so that the Finalized Franking Record is generated with the next
            Audit record and sent in the next Audit Request message. 
            */
            fSendGermanFFRecInAudit = TRUE;         
        }
        
        iStatus = fnPBPSHandleWaitPSDSimpleUicReq(GET_REFUND_RECORD);
        fPBPSTransaction = PBPS_REFUND_TRANSACTION;
        return iStatus;
    case PBPS_PSDREFUND_RSP:
        iStatus = fnPBPSHandleWaitPSDSimpleInfraReq(REFUND_RESPONSE_RECORD);
        return iStatus;
    case PBPS_PSDAUTH_REQ:
        iStatus = fnPBPSHandleWaitPSDComplexInfraReq(AUTH_REQ_RECORD,FALSE);
        return iStatus;     
    case PBPS_PSDCONFIG_REQ:
        iStatus = fnPBPSHandleWaitPSDSimpleInfraReq(POSTAL_CONFIG_RECORD);
        return iStatus;
    case PBPS_PSDREKEY_REQ:
        iStatus = fnPBPSHandleWaitPSDComplexInfraReq(PSD_KEY_RECORD,TRUE);
        return iStatus;
    case PBPS_PSDNONCE_REQ:
        iStatus = fnPBPSHandleWaitPSDNonce(PSD_EDC_NONCE);
        return iStatus;
    case PBPS_PSDEDC_REQ:
        if( fnGetCurrentPsdType() & PSDT_ALL_IBUTTON )
            iStatus = fnPBPSHandleWaitPSDSimpleInfraReq(PSD_EDC_RECORD);
        else
            iStatus = fnPBPSHandleWaitPSDComplexInfraReq(PSD_EDC_RECORD,TRUE);
        return iStatus;             
#ifndef JANUS               
    case PBPS_PSD_GEN_KEY_EXCH_KEY:
        *pWaitForIntertask = FALSE;
        return fnPBPSHandleWaitPSDKeyMsg(GET_GPKXKEY_REQ_LAST_PACKET);
#endif
    case PBPS_PSD_LOAD_SECRET_KEY:
        *pWaitForIntertask = FALSE;

        if( fnGetCurrentPsdType() & PSDT_ALL_IBUTTON )
        {

            // if this is an iButton it will need the key type to be copied from FLASH to bob
            bEncrKeyType = fnFlashGetIBByteParm(IBP_PBP_ENKEY_TYPE);
            dbgMsg("KeyType", bEncrKeyType, __LINE__);
            bBobWriteStatus = fnWriteData( BOBAMAT0,       // Operation
                                           BOBID_SET_IPSD_LD_ENCR_KEY_TYPE, // ID
                               (void *)&bEncrKeyType );    // Source
        }
        return fnPBPSHandleWaitPSDKeyMsg(PSD_LD_ENCR_KEY_REQ);
    default:
        /* Unknown PSD request */
        return PBPS_PROCEDURE_ERR;
    }
}

/**********************************************************
Function: fnPBPSHandleIdle
Author: Derek DeGennaro
Description:
Function that is called when the state enters the 
idle state.

Parameters:

Return Value:

**********************************************************/
static int fnPBPSHandleIdle(void)
{
    char cProgMsg[MAX_INTERTASK_BYTES];
    unsigned long *pLongData;
    if (bPBPSPreviousState != PBPS_IDLE)
    {
        /* we are now done */
        
        /* release any resources */
        fnPBPSDestroySocket(&pbpSocket);        
    }

    /* send notifications to OIT and DISTR task */
    pLongData = (unsigned long*)cProgMsg;
    *pLongData = lwErrorStatus;
	dbgTrace(0, "-->-->--> fnPBPSHandleIdle lwErrorStatus:%d DISTTASK Wake Up! <--<--<--", lwErrorStatus);

    (void)OSSendIntertask( bPBPSRequestingTaskID, PBPTASK, OIT_SERVICE_COMPLETE, LONG_DATA, cProgMsg, sizeof(lwErrorStatus) );

    /* clear some status variables */
    lwErrorStatus = PBPS_OK;
    fPBPSAbortConnection = FALSE;
    fPBPSTransaction = PBPS_NO_TRANSACTION;

    return PBPS_OK;
}

/**********************************************************
Function: fnPBPSHandleWaitPSDSimpleInfraReq
Author: Derek DeGennaro
Description:

This function is called when the Infrastructure sends a
Signed Data Record (SDR) to the PSD and expects the
command status to be returned.

Parameters:
bMsg - The message ID for the record/message/request being
sent to the PSD.

Return Value:
Standard PBPTASK error codes.

**********************************************************/
static int fnPBPSHandleWaitPSDSimpleInfraReq(unsigned char bMsg)
{
    int iStatus = PBPS_OK;

    /* set up the xfer struct */
    psdTransferControl.ptrToDataToSendToDevice = pPSDCommonBuff;
    psdTransferControl.ptrToCallersDestBuffer = NULL;
    psdTransferControl.ptrToDeviceReplyData[0] = NULL;
    /* psdTransferControl.bytesInSource is set when we b64-decode */
    memset(psdTransferControl.deviceReplyStatus,0,4);
    psdTransferControl.bytesWrittenToDest[0] = 0;   

    /* 
    Now send the intertask message to Bob.
    The task then waits for the response in the
    task's main loop.
    */
    (void)OSSendIntertask(SCM,PBPTASK,bMsg,GLOBAL_PTR_DATA,(void*)&psdTransferControl,sizeof(psdTransferControl));

    return iStatus;
}

/**********************************************************
Function: fnPBPSHandleWaitPSDSimpleUicReq
Author: Derek DeGennaro
Description:

This function is called in order to generate a Signed Data 
Record (SDR) which is sent to the Infrastructure. 

Parameters:
bMsg - The message ID for the record/message/request being
sent to the PSD.

Return Value:
Standard PBPTASK error codes.

**********************************************************/
static int fnPBPSHandleWaitPSDSimpleUicReq(unsigned char bMsg)
{
    int iStatus = PBPS_OK;

    /* set up the xfer struct */
    psdTransferControl.ptrToDataToSendToDevice = NULL;
    psdTransferControl.ptrToCallersDestBuffer = pPSDCommonBuff;
    psdTransferControl.ptrToDeviceReplyData[0] = NULL;
    psdTransferControl.bytesInSource = 0;
    memset(psdTransferControl.deviceReplyStatus,0,4);
    psdTransferControl.bytesWrittenToDest[0] = 0;

    /* 
    Now send the intertask message to Bob.
    The task then waits for the response in the
    task's main loop.
    */
    (void)OSSendIntertask(SCM,PBPTASK,bMsg,GLOBAL_PTR_DATA,(void*)&psdTransferControl,sizeof(psdTransferControl));

    return iStatus;
}

/**********************************************************
Function: fnPBPSHandleWaitPSDComplexInfraReq
Author: Derek DeGennaro
Description:

This function is called when the Infrastructure sends the
PSD a Signed Data Record (SDR) and expects the PSD to send it
a SDR in response.  This function can optionally handle 
EDM style messages.

Parameters:
bMsg - The message ID for the record/message/request being
sent to the PSD.

Return Value:
Standard PBPTASK error codes.

**********************************************************/
static int fnPBPSHandleWaitPSDComplexInfraReq(unsigned char bMsg, BOOL fEDMCompatable)
{
    int iStatus = PBPS_OK;

    /* set up the xfer struct */
    psdTransferControl.ptrToDataToSendToDevice = pPSDCommonBuff;
    psdTransferControl.ptrToCallersDestBuffer = pPSDCommonBuff;
    psdTransferControl.ptrToDeviceReplyData[0] = NULL;
    /* psdTransferControl.bytesInSource is set when we b64-decode */
    memset(psdTransferControl.deviceReplyStatus,0,4);
    psdTransferControl.bytesWrittenToDest[0] = 0;

    if (fEDMCompatable)
    {
        // Data Center Prepends 4 bytes of status info for myko, so skip over it
        if( fnGetCurrentPsdType() & PSDT_ALL_IBUTTON )
        {
            psdTransferControl.ptrToDataToSendToDevice = pPSDCommonBuff + 4;
        }
        else
        {
            /* For compatibility with EDM stuff, set xfer structure. */
            edmXferStructPtr = &psdTransferControl;
            /* For compatibility with EDM, set packet and size info. */
            pPSDCommonBuff[0] = pPSDCommonBuff[1] = 1;  /* packet 1 of 1 */
            pPSDCommonBuff[2] = (psdTransferControl.bytesInSource & 0xFF00) >> 8;   /* size byte 1 */
            pPSDCommonBuff[3] = psdTransferControl.bytesInSource & 0x00FF;          /* size byte 2 */
            /* For compatibility with EDM, adjust size to include packet & size info */
            psdTransferControl.bytesInSource += 4;
        }
    }

    /* 
    Now send the intertask message to Bob.
    The task then waits for the response in the
    task's main loop.
    */
    (void)OSSendIntertask(SCM,PBPTASK,bMsg,GLOBAL_PTR_DATA,(void*)&psdTransferControl,sizeof(psdTransferControl));

    return iStatus;
}

/**********************************************************
Function: fnPBPSHandleWaitPSDKeyMsg
Author: Derek DeGennaro
Description:

This function handles the special key related messages of
the PSD.  For now, this function only deals with the 
new ECDSA key related messages.

Parameters:
bMsg - The message ID for the record/message/request being
sent to the PSD.

Return Value:
Standard PBPTASK error codes.

**********************************************************/
static int fnPBPSHandleWaitPSDKeyMsg(unsigned short sMsgID)
{
    int iStatus = PBPS_OK;
    unsigned char bIndex, bBobWriteStatus=BOB_OK;

    /* This message is compatable with the EDM implementation */
    edmXferStructPtr = &psdTransferControl;

    /* initialize the pbp side of the xfer structure */
    psdTransferControl.ptrToDataToSendToDevice = pPSDCommonBuff;
    psdTransferControl.ptrToCallersDestBuffer = pPSDCommonBuff;
    psdTransferControl.ptrToDeviceReplyData[0] = NULL;      
    memset(psdTransferControl.deviceReplyStatus,0,4);
    psdTransferControl.bytesWrittenToDest[0] = 0;
    /* psdTransferControl.bytesInSource is set when we b64-decode */

    // Data Center Prepends 4 bytes of status info for myko, so skip over it
    if( fnGetCurrentPsdType() & PSDT_ALL_IBUTTON )
    {
        psdTransferControl.ptrToDataToSendToDevice = pPSDCommonBuff + 4;
    }
    else
    {
        /* set the packet and size info */
        pPSDCommonBuff[0] = pPSDCommonBuff[1] = 1;  /* packet 1 of 1 */
        pPSDCommonBuff[2] = (psdTransferControl.bytesInSource & 0xFF00) >> 8;   /* size byte 1 */
        pPSDCommonBuff[3] = psdTransferControl.bytesInSource & 0x00FF;          /* size byte 2 */

        /* add 4 bytes for the packet and size info header we just added */
        psdTransferControl.bytesInSource += 4;  
    }

    /* call the bob function to do the work */  
    bBobWriteStatus = fnWriteData(  (unsigned short)BOBAMAT0,       // Operation
                                    (unsigned short)sMsgID,         // ID
                                    (void *)&psdTransferControl );  // Source  (Mod by ejg)
    
    /* save the PSD status regardless of success/failure */
    EndianAwareCopy((void*)&ulPSDLastStatus,(void*)&psdTransferControl.deviceReplyStatus[0],sizeof(ulPSDLastStatus));    
	//dbgTrace( DBG_LVL_INFO,  "PBP_TASK: ulPSDLastStatus=%08X", ulPSDLastStatus );
    ulPSDLastStatus = fnNormalizePsdStatus(ulPSDLastStatus);
	//dbgTrace( DBG_LVL_INFO,  "PBP_TASK: Normalized ulPSDLastStatus=%08X", ulPSDLastStatus );
    
    if (bBobWriteStatus != BOB_OK || ulPSDLastStatus != PSD_STATUS_OK)
    {       
        /* there was an error. */
        bIndex = bPBPSDataID - PBPS_FIRST_PSD_DATAID;
        
        /* 
        if we have a valid dataid and error return
        the error in the talbe.  otherwise return
        the procedure error. 
        */
        if (bIndex >= PBPS_NUM_PSD_DATAID)
        {
            iStatus = PBPS_PROCEDURE_ERR;
        }
        else
        {
            iStatus = (int)sPSDMsgInfoTbl[bIndex];
        }

        dbgMsg("PbpE#", iStatus, __LINE__);
    }
    else
    {
        /* 
        No errors, we are fine.
        */
        iStatus = fnPBPSGotoNextState(bPBPSDataID);
    }
    
    /* clear all known Bob Xfer Control Structures since they
    have been getting out of sync lately */
    localXferCrtlStructPtr = NULL;
    edmXferStructPtr = NULL;

    return iStatus;
}

/**********************************************************
Function: fnPBPSHandleWaitPSDNonce
Author: Derek DeGennaro
Description:

This function is called by the UIC when it wants the PSD
to generate a Nonce.  The Nonce is used in subsequent
communications to the data center to rekey and 
enable/cancel/disable.

Parameters:
bMsg - The message ID for the record/message/request being
sent to the PSD.

Return Value:
Standard PBPTASK error codes.

**********************************************************/
static int fnPBPSHandleWaitPSDNonce(unsigned char bMsg)
{
    int iStatus = PBPS_OK;

    /* set up the xfer struct */
    psdTransferControl.ptrToDataToSendToDevice = NULL;
    psdTransferControl.ptrToCallersDestBuffer = NULL;
    psdTransferControl.ptrToDeviceReplyData[0] = pPSDCommonBuff;
    psdTransferControl.bytesInSource = 0;
    memset(psdTransferControl.deviceReplyStatus,0,4);
    psdTransferControl.bytesWrittenToDest[0] = 0;               

    /* 
    Now send the intertask message to Bob.
    The task then waits for the response in the
    task's main loop.
    */
    (void)OSSendIntertask(SCM,PBPTASK,bMsg,GLOBAL_PTR_DATA,(void*)&psdTransferControl,sizeof(psdTransferControl));

    return iStatus;
}

static int fnPBPSHandleWaitPSDRecord(unsigned short wRecordId, int iErrCode)
{
    int iStatus = PBPS_OK;
    BOOL bBobWriteStatus = BOB_SICK;
    
    /* set up the xfer struct */
    psdTransferControl.ptrToDataToSendToDevice = NULL;
    psdTransferControl.ptrToCallersDestBuffer = pPSDCommonBuff;
    psdTransferControl.ptrToDeviceReplyData[0] = NULL;
    psdTransferControl.bytesInSource = 0;
    memset(psdTransferControl.deviceReplyStatus,0,4);
    psdTransferControl.bytesWrittenToDest[0] = 0;
    
    /* call the bob function to do the work */  
    bBobWriteStatus = fnWriteData(  (unsigned short)BOBAMAT0,       // Operation
                                    (unsigned short)wRecordId,      // ID
                                    (void *)&psdTransferControl );  // Source  (Mod by ejg)
                                    
    /* save the PSD status regardless of success/failure */
    EndianAwareCopy((void*)&ulPSDLastStatus,(void*)&psdTransferControl.deviceReplyStatus[0],sizeof(ulPSDLastStatus));    
    ulPSDLastStatus = fnNormalizePsdStatus(ulPSDLastStatus);
    
    if (bBobWriteStatus != BOB_OK || ulPSDLastStatus != PSD_STATUS_OK)
    {
        /* there was an error. */
        iStatus = iErrCode;
    }   
    
    return iStatus; 
}

/**********************************************************
Function: fnPBPSHandlePSDMsg
Author: Derek DeGennaro
Description:

Parameters:

Return Value:

Modification History:
 07-06-10   Jingwei,Li Don't turn reissue flag off to handle psd aduit rsp message,
                       move it to fnPBPSGetNextService() when reissue refill service started.
 12-12-05   Bob Li  Modified this function,
            replacing fnRMAIInitRatePlus() with fnRateInitRates().
**********************************************************/
static int fnPBPSHandlePSDMsg(INTERTASK *pMsg)
{
    char* pCode = "D1";     //Code for a Refill
    char* pCode9 = "09";    //Code for a meter move
	
    int iStatus = PBPS_OK ;
    unsigned long ulLocalDCStatus = 0;
    unsigned long ulPSDErrorStatus = PSD_STATUS_OK;
    unsigned short wTmpPsdMood = 0;
    unsigned char bIndex;
    unsigned char pDiscardData[4];
    unsigned char bEMDGermanyFinalizedFranking = FALSE;
    unsigned char bClass, bCode;
	char pLogBuf[50];
    BOOL fIsPsdPhcModeOk = FALSE;


    /* clear all known Bob Xfer Control Structures since they
    have been getting out of sync lately */
    localXferCrtlStructPtr = NULL;
    edmXferStructPtr = NULL;
    
    /* The PSD status is good until we find a reason it isn't. */
    ulPSDLastStatus = 0;

    /* a psd message must come from Bob */
    if (pMsg->bSource != SCM)
        return PBPS_PROCEDURE_ERR;
    
    /* index into table */
    bIndex = bPBPSDataID - PBPS_FIRST_PSD_DATAID;

    /* make sure the index and data id are valid */
    if (bIndex >= PBPS_NUM_PSD_DATAID)
        return PBPS_PROCEDURE_ERR;
    
    /* check the message status and psd status */
    EndianAwareCopy((void*)&ulPSDErrorStatus,(void*)&psdTransferControl.deviceReplyStatus[0],sizeof(ulPSDErrorStatus));
    ulPSDErrorStatus = fnNormalizePsdStatus(ulPSDErrorStatus);

    /* 
    DSD 06-17-2005 - FF Rec Fix
    Clear the flag that guards the Finalized Franking Record 
    Transaction against power failure and network connection 
    errors. 
    */
    if (bPBPSDataID == PBPS_PSDREFILL_RSP && fnPBPSIsFFRecXferInProgress())
        fnPBPSClearFFRecXferGuard();

    /* as a general rule, if the PSD is happy Bob returns BOB_OK and the status is zero - PSD_STATUS_OK */
    if (pMsg->bMsgType != BYTE_DATA || pMsg->IntertaskUnion.bByteData[0] != BOB_OK || ulPSDErrorStatus != PSD_STATUS_OK)
    {
        /* log the fact that an error occurred */
        fnLogSystemError(ERROR_CLASS_PBP_PSD,
                        ((ulPSDErrorStatus & 0xFF000000)>>24),
                        ((ulPSDErrorStatus & 0x00FF0000)>>16),
                        ((ulPSDErrorStatus & 0x0000FF00)>>8),
                        (ulPSDErrorStatus & 0x000000FF));
                        
        /* now go through some exceptions to general rule */
        // Only ignore unsuccessfull audits if we are doing a re-issue
        if (bPBPSDataID == PBPS_PSDAUDIT_RSP && ulPSDErrorStatus == PSD_STATUS_ERR_DO_REISSUE_TBL[fnGetPsdTypeIndex()] && AuditRec.fReissue == TRUE)
        {
            // The data center has issued a reissue, and the psd is complaining.  It will do that
            //  and it's okay.  Clear the Bob error flag, clear the reissue flag, and ignore the 
            //  error this time.
            fnBobErrorInfo(&bClass, &bCode);
            //2010.7.6 we will turn it off when reissue refill service started.
            // now turn it off in function fnPBPSGetNextService().
            /* Turn reissue off so we don't continue doing this */
           // AuditRec.fReissue = FALSE;

        }
        else if (bPBPSDataID == PBPS_PSDREFILL_RSP && fDCPvdErrorRecord == TRUE)
        {
            /* store the psd command status for safe keeping */
            ulPSDLastStatus = ulPSDErrorStatus;
            
            if( fnGetCurrentPsdType() & PSDT_ALL_IBUTTON )
            {
                /* The data center has returned a pvd error record.  We check the 
                psd status to see if there was a data center error */
                if (ulPSDLastStatus == PSD_FUNDSMGR_INSUFFICIENT_FUNDS_TBL[fnGetPsdTypeIndex()])
                {
                    /* For an Ibutton, the PSD status is a general error, so we check BOB's last DC status */
                    if(fnValidData(BOBAMAT0, BOBS_LAST_DCSTATUS, (void *) &ulLocalDCStatus) == BOB_OK)
                    {
                        if( ulLocalDCStatus == PBP_DC_STAT_INSUFFUCIENT_FUNDS )
                            return PBPS_INSUFFICIENT_FUNDS_IN_ACCT; /* account balance too low */
                    }
                }

                /* any unknown problems follow here */
                return PBPS_DC_PVD_ERROR_RECORD;        /* unknown problem */
            }
            else
            {
                /* The data center has returned a pvd error record.  We check the 
                PSD command status to see if this was caused by the account balance 
                being too low. */
                if (ulPSDLastStatus == PSD_FUNDSMGR_INSUFFICIENT_FUNDS_TBL[fnGetPsdTypeIndex()]) 
                {
                    return PBPS_INSUFFICIENT_FUNDS_IN_ACCT; /* account balance too low */
                }
                else
                    return PBPS_DC_PVD_ERROR_RECORD;        /* unknown problem */
            }
        }
        else if (bPBPSDataID == PBPS_PSDREFUND_RSP && ulPSDErrorStatus == PSD_FUNDSMGR_PSD_NO_WITHDRAW_TBL[fnGetPsdTypeIndex()])
        {
            dbgMsg("PBPS_METER_REFUND_NOT_ALLOWED", PBPS_METER_REFUND_NOT_ALLOWED, __LINE__);
            return PBPS_METER_REFUND_NOT_ALLOWED;   /* the meter isn't allowed to do the refund */
        }
        else
        {
            ulPSDLastStatus = ulPSDErrorStatus;

	   		(void)sprintf(pLogBuf, "ulPSDLastStatus =0x%lx, #%u,pbptask", ulPSDLastStatus, __LINE__);
	   		fnDumpStringToSystemLog(pLogBuf);

            return ((int)sPSDMsgInfoTbl[bIndex]);
        }
    }

    /* At this point either there is no error, or there is an error that
    is being ignored in order to send an error message back to the 
    data center.  The following switch statement checks some special cases
    for different types of PSD messages.  If a message is not present in the
    cases of the switch statement then there is no error flagged and the
    communication with the data center continues. */
    switch (bPBPSDataID)
	{
    case PBPS_PSDAUTH_REQ:
        /*
        This code gets called when the Data Center successfully 
        Authorizes a meter (errors are handled elsewhere).      
        */
        
        break;
        
    case PBPS_PSDEDC_REQ:
#ifndef JANUS
        if (fnValidData(BOBAMAT0, INSIDE_THE_PSD, (void *) &wTmpPsdMood) == BOB_OK && 
            (wTmpPsdMood == PSD_DISABLED_FROM_DATA_CENTER_CHALLENGE_PROV_STATE || wTmpPsdMood == PSD_DISABLED_FROM_DATA_CENTER_STATE))
        {
            /*
            This code gets called when the Data Center disables
            a meter.            
            */
            
        }
        break;
#endif
    case PBPS_PSDAUDIT_REQ:
        /* The result should be in pPSDCommonBuffer.  It's length should be
        contained in the transfer structure. */
        if (psdTransferControl.bytesWrittenToDest[0] > COMMON_BUFF_LEN)
        {
            iStatus = ((int)sPSDMsgInfoTbl[bIndex]);
            dbgMsg("bIndex", bIndex, __LINE__);
        }
        break;

    case PBPS_PSDAUDIT_RSP:
        fAuditDone = TRUE;
        // A successfull audit rsp clears all transactions 
        fPBPSTransaction = PBPS_NO_TRANSACTION;
        if(fPvrDone)
        {
        	fPvrDone = FALSE;
        	CMOSSetupParams.lwPBPAcctNumber = 0;
            dbgTrace(0, "-->-->--> fnPBPSHandlePSDMsg:PBPS_PSDAUDIT_RSP : CMOSSetupParams.lwPBPAcctNumber:%d  <--<--<--", CMOSSetupParams.lwPBPAcctNumber);
        }

        send_AccountBalanceRsp(API_STATUS_SUCCESS, dblDebitBal, dblCreditBal);
        break;

    case PBPS_PSDREFILL_REQ:
        /* The result should be in pPSDCommonBuffer.  It's length should be
        contained in the transfer structure. */
        
        /* Check to see if it is necessary to send a German
        Finalized Franking record along with the Refill (PVD)
        Request.        
        */
        bEMDGermanyFinalizedFranking = fnFlashGetByteParm(BP_PBP_PERFORM_FINALIZED_FRANKING);

        if (bEMDGermanyFinalizedFranking)
        {
            // Use the Generic PBPTask-fnWriteData interface.
            // This interface utilizes the Xfer Ctrl structure.
            lwPSDFFRecBuffLen = 0;
            (void)memcpy(&tmpPsdTransferControl, &psdTransferControl, sizeof(psdTransferControl));   //save pvd req data before getting ff record
            (void)memcpy(pTmpPSDCommonBuff, pPSDCommonBuff, sizeof(pPSDCommonBuff)); 

            (void)memset((void*)pPSDFFRecBuff,0,sizeof(pPSDFFRecBuff));
            iStatus = fnPBPSHandleWaitPSDRecord(PSD_PBP_FINAL_FRANK_REC,PBPS_PSD_FFREC_ERR);            
            if (iStatus != PBPS_OK)
                return iStatus;

            // Indicate doing Finalize Franking zero refill
            fDoingFinalizeFranking = TRUE;
                
            // Save the record because the Xfer Ctrl structure
            // and buffers now need to be used for the refill
            // request record.
            lwPSDFFRecBuffLen = psdTransferControl.bytesWrittenToDest[0];
            (void)memcpy((void*)pPSDFFRecBuff,pPSDCommonBuff,lwPSDFFRecBuffLen);
            
            (void)memcpy(&psdTransferControl,&tmpPsdTransferControl, sizeof(psdTransferControl));      //restore saved pvd req data
            (void)memcpy(pPSDCommonBuff, pTmpPSDCommonBuff, sizeof(pPSDCommonBuff)); 
            
            // Re-set the Bob task transfer control structures just in
            // case they were accidentally changed by the Bob task
            // during the finalized franking processing.
            localXferCrtlStructPtr = &psdTransferControl;
            edmXferStructPtr = &psdTransferControl;
        }       

        if (psdTransferControl.bytesWrittenToDest[0] > COMMON_BUFF_LEN)
        {
            dbgMsg("bIndex", bIndex,  __LINE__);
            iStatus = ((int)sPSDMsgInfoTbl[bIndex]);
        }
        break;
    case PBPS_PSDREFILL_RSP:        
    /* reset the low funds warning flags */
    fLowFundsWarningPending = FALSE;
        
        /* If we are not in OOB, then set the refill receipt
        pending flage.  We don't set it in OOB because
        OOB wants to handle refill receipts in a different
        way. */
        if (CMOSDiagnostics.fOOBComplete == TRUE)
        {
            /*
            Only set the refill receipt pending if the user actually
            requests a refill.  A refill may be done automatically
            in some instances (French Credit Limit Reset during 
            DCAP upload).
            */
            if ((lwPBPSStatus & PBPS_USER_REQ_REFILL) &&
                (bPBPSRefillType != PSD_REFILL_TYPE_FRENCH_CREDIT_LIMIT_TBL[fnGetPsdTypeIndex()]))
                CMOSDiagnostics.fReceiptPending = TRUE;

         }
        // if this isn't the refill response from doing the Finalize Franking zero refill for Germany,
        // indicate that the refill was done.
        // else, clear the flag.
        if (fDoingFinalizeFranking == FALSE)
            fPvdDone = TRUE;
        else
            fDoingFinalizeFranking = FALSE;

        send_RefillRsp();
        break;

    case PBPS_PSDREFUND_REQ:
        /* The result should be in pPSDCommonBuffer.  It's length should be
        contained in the transfer structure. */
        if (psdTransferControl.bytesWrittenToDest[0] > COMMON_BUFF_LEN)
        {
            dbgMsg("bIndex", bIndex,  __LINE__);
            iStatus = ((int)sPSDMsgInfoTbl[bIndex]);
        }
        break;
    case PBPS_PSDREFUND_RSP:
        /* 
        This code is run when the UIC has withdrawn the 
        funds from its meter. A meter is also withdrawn
        as part of a meter move outside teh current region.     
        */
        
        if ((lwPBPSStatus & PBPS_USER_REQ_METER_MOVE) &&
            (fnOITIsMeterMoveOutOfRegion() == TRUE))
        {
            fMeterMoveDone = TRUE;
            
            // (Meter Move Outside Region)
        }
        fPvrDone = TRUE;
        dbgTrace(0, "-->-->--> fnPBPSHandlePSDMsg:PBPS_PSDREFUND_RSP  fPvrDone = %d<--<--<--", fPvrDone);
        
        break;
    case PBPS_PSDCONFIG_REQ:
        /*
        This code is run when the Data Center loads a new postal code
        (zip code) into the meter.  This occurs during a Meter Move
        within the current region.
        */
        if ((lwPBPSStatus & PBPS_USER_REQ_METER_MOVE) &&
            (fnOITIsMeterMoveOutOfRegion() == FALSE))
        {
            fMeterMoveDone = TRUE;
            
            // (Meter Move Inside Region)
        }
        
        break;
    default:
        /*
        This type of PSD message does not require any special checks 
        or processing so continue communicating with the data center.
        */
        iStatus = PBPS_OK;
        break;
    };
    
    if (iStatus == PBPS_OK)     
        iStatus = fnPBPSGotoNextState(bPBPSDataID); /* everthing is ok */

    return iStatus;
}

/**********************************************************
Function: fnPBPSGotoState
Author: Derek DeGennaro
Description:
This function transitions to a new state/data id directly.

Parameters:

Return Value:

**********************************************************/
static int fnPBPSGotoState(unsigned char bNewState, unsigned char bDataID)
{
    int iStatus = PBPS_OK;

    /* record old info */
    bPBPSPreviousState = bPBPSState;
    bPBPSPreviousID = bPBPSDataID;
    
    /* set the new data id */
    bPBPSDataID = bDataID;

    /* set new state */
    bPBPSState = bNewState;
    
	dbgTrace(0, "-->-->--> fnPBPSGotoState PrevState:%d PrevID:%d NewState:%d NewID:%d <--<--<--", bPBPSPreviousState, bPBPSPreviousID, bPBPSState, bPBPSDataID);
    return iStatus;
}
/**********************************************************
Function: fnPBPSGotoNextState
Author: Derek DeGennaro
Description:
This function transitions to a new state/data id
based on the id passed into the functions and
the information in the task tables.

Parameters:

Return Value:

**********************************************************/
static int fnPBPSGotoNextState(unsigned char bCurrID)
{
    int iStatus = PBPS_OK;
    unsigned char bNewState = 0, bNewID = 0;

    if (TheBigTable[bCurrID].sNextInfo.bType == PNONE)
        iStatus = PBPS_PROCEDURE_ERR;
    else if (TheBigTable[bCurrID].sNextInfo.bType == PCONST)
    {
        bNewState = TheBigTable[bCurrID].sNextInfo.bState;
        bNewID = TheBigTable[bCurrID].sNextInfo.bID;
    }
    else if (TheBigTable[bCurrID].sNextInfo.bType == PFUNCT && TheBigTable[bCurrID].sNextInfo.fcnGetNext != NULL)
    {
        iStatus = TheBigTable[bCurrID].sNextInfo.fcnGetNext(&bNewState,&bNewID);
//        if (iStatus == PBPS_OK)
//        {
//            bNewState = bNewState;
//            bNewID = bNewID;
//        }
    }
    else
        iStatus = PBPS_PROCEDURE_ERR;

if (iStatus == PBPS_OK)
        iStatus = fnPBPSGotoState(bNewState,bNewID);

    return iStatus;
}

/**********************************************************
Function: fnPBPSAbortConnection
Author: Derek DeGennaro
Description:
This public function is called by another task (OIT) in
order to tell the PBP task that it should abort it's 
connection to the data center.  Internally, it just sets
the abort flag.

Parameters:

Return Value:

**********************************************************/
void fnPBPSAbortConnection(void)
{
    fPBPSAbortConnection = TRUE;
}

/**********************************************************
Function: fnPBPSSafeToAbort
Author: Derek DeGennaro
Description:
This private function is used by the PBP task to determine
if it is in a state that allows it to abort the connection.
The pbp task cannot abort in the following conditions:
1. It is waiting for a request from the data center.
2. It is sending a response to a request to the data center.
3. It is waiting for a response from the data center.
4. It is in the middle of a PSD transaction (audit,refill,refund).
5. A data capture upload needs to be done.

The pbp task can abort in the following conditions:
1. There are no PSD transactions in progress AND
2. There will not be any data capture uploads AND
3. The pbp task is about to send a request to the data center OR
        The pbp task is about to start an audit.

Parameters:

Return Value:
TRUE if it is ok to abort the connection,
FALSE otherwise.

**********************************************************/
static BOOL fnPBPSSafeToAbort(void)
{
    return FALSE;
}

/**********************************************************
Function: fnPBPSAbortActiveConnection
Author: Derek DeGennaro
Description:
This is the function that actually aborts the connection 
with the data center.  It sends the SignOffReq message, 
gets the response from the data center, and then goes
to the idle state.

Parameters:

Return Value:

**********************************************************/
static void fnPBPSAbortActiveConnection(void)
{
    /* At this point we have control
    of the dialog and have nothing
    that must be completed first. So
    send the sign off request to data
    center and wait for the response. */
    bPBPSState = PBPS_SENDREQ;
    bPBPSDataID = PBPS_SENDSIGNOFF_REQ;
    (void)fnPBPSSendMsg(PBPS_SENDSIGNOFF_REQ);
    (void)fnPBPSReceiveMsg(); /* get response */
    (void)fnPBPSGotoState(PBPS_IDLE,PBPS_NODATAID);
}

/**********************************************************
Function: fnPBPSGetPSDStatus
Author: Derek DeGennaro
Description:
Returns the status of the last PSD message that the 
pbp task sent to the PSD.

**********************************************************/
unsigned long fnPBPSGetPSDStatus(void)
{
    return ulPSDLastStatus;
}

/**********************************************************
Function: fnPBPWasMeterMoved
Author: Derek DeGennaro
Description:
Public function that is called to determine if a meter move
was done on the last connection to the Data Center.  Note
that this is not saved in NVM so it only applies to the
last connection since the unit was powered up.

**********************************************************/
BOOL fnPBPWasMeterMoved(void)
{
    return fMeterMoveDone;
}

/**********************************************************
Function: fnPBPWasRefillDone
Author: Derek DeGennaro
Description:
Public function that is called to determine if a refill
was done on the last connection to the Data Center.  Note
that this is not saved in NVM so it only applies to the
last connection since the unit was powered up.

**********************************************************/
BOOL fnPBPWasRefillDone(void)
{
    BOOL fRetval = FALSE;
    
    if ((fPvdDone == TRUE) &&
        (lwPBPSStatus & PBPS_USER_REQ_REFILL) &&
        (bPBPSRefillType != PSD_REFILL_TYPE_FRENCH_CREDIT_LIMIT_TBL[fnGetPsdTypeIndex()]))
    {
        fRetval = TRUE;
    }       
    
    return fRetval;
}
BOOL fnPBPWasRefundDone(void)
{
    BOOL fRetval = FALSE;
    
    if (fPvrDone == TRUE && (lwPBPSStatus & PBPS_USER_REQ_REFUND))
        fRetval = TRUE;
    
    return fRetval;
}

BOOL fnPBPWasDcapUploadDone(void)
{
    return fDcapUploadDone;
}

BOOL fnPBPWasAcctBalanceDone(void)
{
    BOOL fRetval = FALSE;

    if ((fAuditDone == TRUE) && (lwPBPSStatus & PBPS_USER_REQ_ACCTBAL))
        fRetval = TRUE;

    return fRetval;
}

char *fnPBPGetCurrentSessionID(void)
{
    return cCurrSessionID;
}

/**********************************************************
Function: fnPBPWasFFRecXferInterrupted
Author: Derek DeGennaro
Description:
This is a PUBLIC function that the OIT/SYS or any other
Task can call to determine if a Finalized Franking Transaction
was interrupted.  If the transaction was interrupted the
PBP server should be contacted again to re-do the transaction.
If the transaction is not re-done and postage is dispensed 
then the information sent to the German Post may not match
the usage recorded by the PSD.

// DSD 06-17-2005 - FF Rec Fix
**********************************************************/
BOOL fnPBPWasFFRecXferInterrupted(void)
{
    extern BOOL fCMOSGuardedFFRecXferInProgress;
    unsigned char bEMDGermanyZeroRefill = fnFlashGetByteParm(BP_PBP_GERMAN_ZERO_REFILL_REQ);
    if (bEMDGermanyZeroRefill)
        return fCMOSGuardedFFRecXferInProgress;
    else
        return FALSE;
}
/**********************************************************
Function: fnPBPSIsFFRecXferInProgress
Author: Derek DeGennaro
Description:
This function is a PRIVATE function that determines if 
the PBP Task is in the process of performing a 
German Finalized Franking Transaction.

// DSD 06-17-2005 - FF Rec Fix
**********************************************************/
static BOOL fnPBPSIsFFRecXferInProgress(void)
{
    extern BOOL fCMOSGuardedFFRecXferInProgress;
    unsigned char bEMDGermanyZeroRefill = fnFlashGetByteParm(BP_PBP_GERMAN_ZERO_REFILL_REQ);
    if (bEMDGermanyZeroRefill &&
        (lwPBPSStatus & PBPS_GERMAN_ZERO_REFILL_IN_PROGRESS) == PBPS_GERMAN_ZERO_REFILL_IN_PROGRESS)
        return TRUE;
    else
        return FALSE;
}
/**********************************************************
Function: fnPBPSSetFFRecXferGuard
Author: Derek DeGennaro
Description:
This is a PRIVATE function that sets the flag that 
guards the German Finalized Franking Transaction 
against power failure and network connection errors.

// DSD 06-17-2005 - FF Rec Fix
**********************************************************/
static void fnPBPSSetFFRecXferGuard(void)
{
    extern BOOL fCMOSGuardedFFRecXferInProgress;
    unsigned char bEMDGermanyZeroRefill = fnFlashGetByteParm(BP_PBP_GERMAN_ZERO_REFILL_REQ);
    if (bEMDGermanyZeroRefill)
        fCMOSGuardedFFRecXferInProgress = TRUE;
}
/**********************************************************
Function: fnPBPSClearFFRecXferGuard
Author: Derek DeGennaro
Description:
This is a PRIVATE function that clears the flag that 
guards the German Finalized Franking Transaction 
against power failure and network connection errors.

// DSD 06-17-2005 - FF Rec Fix
**********************************************************/
static void fnPBPSClearFFRecXferGuard(void)
{
    extern BOOL fCMOSGuardedFFRecXferInProgress;
    unsigned char bEMDGermanyZeroRefill = fnFlashGetByteParm(BP_PBP_GERMAN_ZERO_REFILL_REQ);
    if (bEMDGermanyZeroRefill)
        fCMOSGuardedFFRecXferInProgress = FALSE;
}



/**********************************************************
Function: fnPBPSParseServiceRequest
Author: Derek DeGennaro
Description:
Parses a service request, sets the requested service
parameter, and schedules whatever needs to be done.

Parameters:
pService - structure that contains the URL and the service
type ID of the requested service.

Return Value:
PBPS_OK - no errors
PBPS_PROCEDURE_ERR - Invalid parameters.
PBPS_BAD_SERVICEID - Invalid service type ID being requested.
PBPS_SERVICE_SCHEDULER_ERR - Cannot schedule the service.

**********************************************************/
static int fnPBPSParseServiceRequest(sServiceURL *pService)
{
    int iStatus = PBPS_OK;  
    char *pName = NULL;
    double dblDefaultRefillAmt = 0.0;
    unsigned char bRefillCtrl = PBP_REFILL_CTRL_USER_SELECTED;
    unsigned char bEMDGermanyZeroRefill = FALSE;
    unsigned char pDefaultRefillAmtBuff[4];
    MONEY pDefaultRefillAmt[SPARK_MONEY_SIZE];
    BOOL fDCAPActive = FALSE, fDCAPDataAvailable = FALSE;
    BOOL popEnabled     = FALSE;
    BOOL popAvailable   = FALSE;
    //BOOL popDaysElapsed = FALSE;

    /* check the status of data capture */  
    ///?TODO
//    fDCAPActive = fnDCAPIsDataCaptureActive();
//	fDCAPDataAvailable = fnDCAPIsThereDataForUpload();
    
	/*  Check to see if POP is enabled */
    if ((popEnabled = fnPOPISEnabled()) == TRUE)
    {
		/*  Check to see if POP data is available for upload */
        popAvailable = fnPOPIsDataAvailableForUpload();
    }
    
    if (pService != NULL)
    {
        pName = pService->pServiceID;
        dbgTrace(0, "-->-->--> fnPBPSParseServiceRequest pServiceID:%s <--<--<--", pName);
        if (strcmp(pName,"Refill") == 0)
        {           
            lwPBPSStatus |= (PBPS_PERFORM_REFILL | PBPS_USER_REQ_REFILL);
            iStatus = PBPS_OK;          
        }
        else if (strcmp(pName,"Refund") == 0)
        {
            lwPBPSStatus |= (PBPS_PERFORM_REFUND | PBPS_USER_REQ_REFUND);
            if (fDCAPActive == TRUE)
                lwPBPSStatus |=  PBPS_PERFORM_DCAP_UPLOAD;
            iStatus = PBPS_OK;          
        }   
        else if (strcmp(pName,"AcctBal") == 0)
        {       
            lwPBPSStatus |= PBPS_USER_REQ_ACCTBAL;
            iStatus = PBPS_OK;
        }
        else if (strcmp(pName,"DcpUpld") == 0)  
        {       
            lwPBPSStatus |= (PBPS_PERFORM_DCAP_UPLOAD | PBPS_USER_REQ_DCAP_UPLOAD);
            iStatus = PBPS_OK;
        }
        else if (strcmp(pName,"Move") == 0)
        {
            lwPBPSStatus |= PBPS_USER_REQ_METER_MOVE;           
            if (fnOITIsMeterMoveOutOfRegion() == TRUE)
                lwPBPSStatus |= PBPS_PERFORM_REFUND;    
            if (fDCAPActive == TRUE)
                lwPBPSStatus |=  PBPS_PERFORM_DCAP_UPLOAD;
            iStatus = PBPS_OK;
        }
        else
        {   
            /* Unknown service type. */         
            iStatus = PBPS_BAD_SERVICEID;
        }
        
        /* Check to see if data capture needs to be done. */
        if (fDCAPActive == TRUE && fDCAPDataAvailable == TRUE)
            lwPBPSStatus |= PBPS_PERFORM_DCAP_UPLOAD;
            
        /* Check to see if a refill (Postage Value Download - PVD)
        needs to be done on each connection to the data center.  
        This is done in some countries in order to reset the credit
        limit. */
        bRefillCtrl = fnFlashGetByteParm(BP_PBP_REFILL_CONTROL);
        if (iStatus == PBPS_OK && 
            iOITDistrLoopCount == 0 &&
            bRefillCtrl == PBP_REFILL_CTRL_REFILL_ALWAYS &&
            bPBPSRefillType == PSD_REFILL_TYPE_FRENCH_CREDIT_LIMIT_TBL[fnGetPsdTypeIndex()] &&
            (lwPBPSStatus & PBPS_PERFORM_REFUND) == 0)
        {
            /* Schedule a refill if one isn't scheduled already. */
            if ((lwPBPSStatus & PBPS_PERFORM_REFILL) == 0)
            {
                /* A refill wasn't scheduled so schedule the refill now. */
                lwPBPSStatus |= PBPS_PERFORM_REFILL;                
            }           

            /* A refill is scheduled.  At this point the refill
            amount is reset to the factory default value. */
            (void)memcpy((void*)pDefaultRefillAmt,(void*)fnFlashGetMoneyParm(MP_FACTORY_REFILL_AMT),SPARK_MONEY_SIZE);
            dblDefaultRefillAmt = fnBinFive2Double((uchar*)pDefaultRefillAmt);
            fnDouble2BinFour(pDefaultRefillAmtBuff,dblDefaultRefillAmt);
            if (fnWriteData((unsigned short)BOBAMAT0, (unsigned short)VAULT_REFILL_AMT, (void *)(&pDefaultRefillAmtBuff[0])) != BOB_OK)
                iStatus = PBPS_PSD_SET_REFILL_AMT_ERR;  /* There was an error setting the refill amount. */
        }

        
        /* An ibutton may be caught in the withdraw pending state and can not perform a refill request.  Therefore,
           If the PSD is an Ibutton and the current service request is not a refund then don't do the zero refill
        */      
        if(   (strcmp(pName,"Refund") != 0) 
           || !(fnGetCurrentPsdType() & PSDT_ALL_IBUTTON) )
            bEMDGermanyZeroRefill = fnFlashGetByteParm(BP_PBP_GERMAN_ZERO_REFILL_REQ);
        
        /* Check to see if we need to do a Germany Zero Refill
        in order to refresh mSecret.
        */
        if (bEMDGermanyZeroRefill)
        {
            /* In Germany we will just do a zero refill on each connection. */
            lwPBPSStatus |= PBPS_PERFORM_GERMAN_ZERO_REFILL;
        }

        /* Check to see if POP data needs to be uploaded. */
        if ((popEnabled == TRUE) && (popAvailable == TRUE))
        {
            lwPBPSStatus |= PBPS_PERFORM_POP_UPLOAD;
        }
    }
    else
    {
        /* Invalid parameters. */
        iStatus = PBPS_PROCEDURE_ERR;
    }

    return iStatus;
}

/**********************************************************
Function: fnPBPSCanPSDBeRefilled
Author: Derek DeGennaro
Description:
Determines if the PSD can be refilled.  This function utilizes
"Table 1 Command by State" in VA97011 to determine if the PSD
is in a state that will allow the "Create Postage Value Download
Request" command.  The values of the PSD internal state values
come from VA97008.

**********************************************************/
static BOOL fnPBPSCanPSDBeRefilled(void)
{
    BOOL fRefillOk = TRUE;
    uchar ipsdStat;

#ifndef JANUS
    unsigned short wPSDState = 0;
    if (fnValidData(BOBAMAT0, INSIDE_THE_PSD, (void *) &wPSDState) == BOB_OK)
    {
        if (wPSDState == PSD_SERIALNUM_LOCKOUT_STATE || wPSDState == PSD_SERIALNUM_LOCKOUT_AWAIT_PVD_STATE ||
            wPSDState == PSD_INSPECTION_LOCKOUT_STATE || wPSDState == PSD_INSPECTION_LOCKOUT_AWAIT_PVD_STATE)
            fRefillOk = TRUE;   /* Refill is only allowed in the above states. */
        else
            fRefillOk = FALSE;
    }
#else
    ulong ulPsdState = 0; 
    fnGetPsdState(&ulPsdState);
    {
        if( (ulPsdState == (UINT32)eIPSDSTATE_LOGGEDIN) || (ulPsdState == (UINT32)eIPSDSTATE_GERM_LOGGEDIN) )
            fRefillOk = TRUE;
        else
            fRefillOk = FALSE;
    }   
    if (fRefillOk)
    {
        if ( fnValidData( BOBAMAT0, BOBID_IPSD_ENBLDSBLSTAT, (void *)(&ipsdStat) ) != BOB_OK)
        {
            fnProcessSCMError();
            fRefillOk = FALSE;
        }
        else
        {
            if (!ipsdStat) fRefillOk = FALSE;
        }
    }
#endif
            
    return fRefillOk;
}

/**********************************************************
Function: fnPBPSGetNextService
Author: Derek DeGennaro
Description:
This function looks at the first service to perform is.
The function sets its out parameters to the state/data id
needed to perform this service.
If there is no more services to perform the function would
return the appropriate sign off state/data id.

This function defines the order of messages sent to the 
Data Center (excluding the SignOn, SignOff, and the 
first Audit that follows the SignOn).

After the SignOn the UIC initiates an Audit with the
Data Center.  After the first Audit the function
fnPBPSGetNextService is called to determine the next
message that is sent to the Data Center.  If a Data Capture
Upload needs to be sent it is done at this point.  If the
upload occurrs then fnPBPSGetNextService is called after
the upload to again determine what needs to occur.  So, 
after the SignOn, Audit, and optional Data Capture Upload
fnPBPSGetNextService checks to see if a Confirmation Services
Upload is needed.  If it is then this is the next message
sent to the Data Center.  After the SignOn, Audit, optional
DCAP upload, and optional Confirmation Services the UIC calls
fnPBPSGetNextService again.  Now, a Poll Indicator message
is sent to the Data Center.  Until the Data Center sends a
Poll Indicator message back to the UIC, the Data Center is
in control of the session and the UIC simply responds to
their messages.  After the Data Center sends the Poll 
Indicator to the UIC the fnPBPSGetNextService function is
called to determine the next message to send to the 
Data Center.  The messages that can be sent are based on
the selection of the user.  The user could have chosen
to do a refill, account balance, or refund (withdrawal).
Since all the account balance info is wrapped in the Audit
message no new messages are sent if the user chose to 
do an account balance.  If a refill was chosen then the
refill message is sent to the Data Center.  After the
refill is complete an additional Audit must be performed.
So after the refill fnPBPSGetNextService is called again
and it will figure out that an audit is needed.  If the
user chose to do a refund then the refund message is sent
after the UIC got the Poll Indicator.  As with the refill
an Audit is required after the refund is completed.  So
after a refund fnPBPSGetNextService is called and it
determines that an Audit is needed.  After the optional
refill/refund and Audit combo fnPBPSGetNextService is
called again and it determines that it is time to end
the session and sends the data center the SignOff message.

Parameters:

Return Value:

MODIFICATION HISTORY:
  09/29/2012  Bob Li        Updated the function to clear the display service value for 
                                displaying the correct service status. Implement Enh Req 213340.
  06/29/2010  Jingwei,Li    Add code to support reissue case.
**********************************************************/

static int fnPBPSIsAuditRequired(unsigned char *pNewState, unsigned char *pNewID)
{
    int iStatus = PBPS_OK;
    unsigned char bGUIDEnabled = 0;
    
    bGUIDEnabled = fnFlashGetByteParm(BP_GUID_ENABLED);

    if((GUIDStatus == PBPS_OK ) && (iOITDistrLoopCount == 0) && (lwPBPSStatus & PBPS_USER_REQ_DCAP_UPLOAD) && (bGUIDEnabled == TRUE))
	{
    	//jump past the audit
    	dbgTrace(0, "-->-->--> fnPBPSIsAuditRequired[1] in lwPBPSStatus:0x%08x, GUIDStatus:%d <--<--<--", lwPBPSStatus, GUIDStatus);
    	//use the Big Table to decide the next state and id
       	//and set the global state and id values
    	fnPBPSGotoNextState(PBPS_PSDAUDIT_RSP);
	}
    else
    {
    	dbgTrace(0, "-->-->--> fnPBPSIsAuditRequired[0] in lwPBPSStatus:0x%08x, GUIDStatus:%d <--<--<--", lwPBPSStatus, GUIDStatus);
    	//set the global state and id values
    	fnPBPSGotoState(PBPS_WAITPSD, PBPS_PSDAUDIT_REQ);
    }

    //use the global vars to set the return var values
	*pNewID = bPBPSDataID;
    *pNewState = bPBPSState;

	//Return to main PBP loop to execute next state
	return iStatus;
}

static int fnPBPSGetNextService(unsigned char *pNewState, unsigned char *pNewID)
{
    int iStatus = PBPS_OK;
    const unsigned long lwZeroRefillAmt = 0;
    unsigned long lwUserRefillAmt = 0;
    double dblUserRefillAmt = 0.0;
    BOOL fRefillOk = TRUE;

    unsigned char pMsgData[8];
    short *pInt16;

    //firstly clear the current display service
    usOITCurrentDisplayService = 0;

	dbgTrace(0, "-->-->--> fnPBPSGetNextService in lwPBPSStatus:0x%08x, GUIDStatus:%d <--<--<--", lwPBPSStatus, GUIDStatus);

    /*
    First, check to see if we need to do a dcap upload.
    We do an upload if dcap is on AND we have rulse AND we this is the first time we
    have connected during this section AND we haven't done an upload this session AND
    there is data OR we are doing a refund.
    */
    if ((iOITDistrLoopCount == 0) && (lwPBPSStatus & PBPS_PERFORM_DCAP_UPLOAD))
    {
        *pNewState = PBPS_SENDREQ;
        *pNewID = PBPS_DCAP_UPLOAD;
        lwPBPSStatus &= ~PBPS_PERFORM_DCAP_UPLOAD;
//        UploadUpdateOI(DCAP_UPLOAD_SERVICE, OIT_INFRA_UPLOAD_START, 0, 0);
    }
    else if ((lwPBPSStatus & (PBPS_USER_REQ_METER_MOVE | PBPS_PERFORM_REFUND)) ==
                (PBPS_USER_REQ_METER_MOVE | PBPS_PERFORM_REFUND))
    {
        *pNewState = PBPS_WAITPSD;
        *pNewID = PBPS_PSDREFUND_REQ;

        lwPBPSStatus &= ~PBPS_PERFORM_REFUND;
        lwPBPSStatus |= PBPS_PERFORM_POST_MOVE_AUDIT;
    }
    else if (lwPBPSStatus & PBPS_PERFORM_POST_MOVE_AUDIT)
    {
        *pNewState = PBPS_WAITPSD;
        *pNewID = PBPS_PSDAUDIT_REQ;

        lwPBPSStatus &= ~PBPS_PERFORM_POST_MOVE_AUDIT;
    }
    else if ((lwPBPSStatus & PBPS_DC_SENT_POLL) == 0)
    {
        /* The data center hasn't been given control yet, send poll. */
        *pNewState = PBPS_SENDREQ;
        *pNewID = PBPS_DCSEND_POLL;
    }   
    else if (lwPBPSStatus & PBPS_PERFORM_GERMAN_ZERO_REFILL)
    {
        /* A refill is only allowed under certain
        circumstances. At this point a refill
        was scheduled to occur.  But due to
        the state of the PSD it may not be possible
        to refill.  The state of the PSD can 
        be affected by operations that occur before
        we get around to doing the refill. */
        fRefillOk = fnPBPSCanPSDBeRefilled();
        if (fRefillOk == TRUE)
        {
            /* set the refill amount to zero */
            if (fnWriteData((unsigned short)BOBAMAT0, (unsigned short)VAULT_REFILL_AMT, (void *)(&lwZeroRefillAmt)) != BOB_OK)
                iStatus = PBPS_PSD_SET_REFILL_AMT_ERR;
            
            if (iStatus == PBPS_OK)
            {
                /* The Refill can proceed. */
                *pNewState = PBPS_WAITPSD;
                *pNewID = PBPS_PSDREFILL_REQ;

                // DSD 06-17-2005 - FF Rec Fix
                lwPBPSStatus |= PBPS_GERMAN_ZERO_REFILL_IN_PROGRESS;
            }
            else
            {
                /* There error setting the refill amount - sign off */
                *pNewState = PBPS_SENDREQ;
                *pNewID = PBPS_SENDSIGNOFF_REQ;
            }
        }
        else
        {
            /* The Refill is not allowed. Since
            Refills are done at the end of the session
            with the data center, it is safe to just
            end the session with the data center. */
            *pNewState = PBPS_SENDREQ;
            *pNewID = PBPS_SENDSIGNOFF_REQ;

            /* set the error of psd refill not allowed */
            iStatus = PBPS_PSD_REFILL_NOT_ALLOWED;
        }
        
        lwPBPSStatus &= ~PBPS_PERFORM_GERMAN_ZERO_REFILL;
        lwPBPSStatus |= PBPS_PERFORM_ZERO_REFILL_AUDIT;
    }
    else if (lwPBPSStatus & PBPS_PERFORM_ZERO_REFILL_AUDIT)
    {
        *pNewState = PBPS_WAITPSD;
        *pNewID = PBPS_PSDAUDIT_REQ;
        
        if ((lwPBPSStatus & (PBPS_PERFORM_REFILL | PBPS_USER_REQ_REFILL)) ==
                (PBPS_PERFORM_REFILL | PBPS_USER_REQ_REFILL))
        {
            /* set the refill amount back to the user specified
            value */
            dblUserRefillAmt = fnBinFive2Double(CMOSMoneyParams.RefillAmt);
            fnDouble2BinFour((unsigned char*)&lwUserRefillAmt,dblUserRefillAmt);
            if (fnWriteData((unsigned short)BOBAMAT0, (unsigned short)VAULT_REFILL_AMT, (void *)(&lwUserRefillAmt)) != BOB_OK)
                iStatus = PBPS_PSD_SET_REFILL_AMT_ERR;
        }
        
        if (iStatus == PBPS_OK)
            lwPBPSStatus &= ~PBPS_PERFORM_ZERO_REFILL_AUDIT;

        // DSD 06-17-2005 - FF Rec Fix
        lwPBPSStatus &= ~PBPS_GERMAN_ZERO_REFILL_IN_PROGRESS;
    }
    else if (lwPBPSStatus & PBPS_PERFORM_REFILL)
    {
        /* A refill is only allowed under certain
        circumstances. At this point a refill
        was scheduled to occur.  But due to
        the state of the PSD it may not be possible
        to refill.  The state of the PSD can 
        be affected by operations that occur before
        we get around to doing the refill. */
        fRefillOk = fnPBPSCanPSDBeRefilled();
        if (fRefillOk == TRUE)
        {
            /* The Refill can proceed. */
            *pNewState = PBPS_WAITPSD;
            *pNewID = PBPS_PSDREFILL_REQ;
            if(AuditRec.fReissue == TRUE)
            {
                 //tell OIT task that refill serice start
                 memset(pMsgData, 0, sizeof(pMsgData));
                 pInt16 = (short *)&pMsgData[0];
                 *pInt16++ = REFILL_SERVICE;
                 *pInt16++ = OIT_INFRA_REFILL_START;
                 OSSendIntertask(OIT, DISTTASK, OIT_INFRA_REFILL_SERV_STATUS, BYTE_DATA, pMsgData, sizeof(pMsgData));
             }
        }
        else
        {
            /* The Refill is not allowed. Since
            Refills are done at the end of the session
            with the data center, it is safe to just
            end the session with the data center. */
            *pNewState = PBPS_SENDREQ;
            *pNewID = PBPS_SENDSIGNOFF_REQ; 

            /* set the error of psd refill not allowed */
            iStatus = PBPS_PSD_REFILL_NOT_ALLOWED;
        }
        lwPBPSStatus &= ~PBPS_PERFORM_REFILL;
       if(AuditRec.fReissue == TRUE)
       {
          AuditRec.fReissue = FALSE;
       }
       else
       {
          //if refill is by reissue, don't need a post audit.
          //lwPBPSStatus |= PBPS_PERFORM_POST_OP_AUDIT;
       }
    }
    else if (lwPBPSStatus & PBPS_PERFORM_REFUND)
    {
        *pNewState = PBPS_WAITPSD;
        *pNewID = PBPS_PSDREFUND_REQ;

        lwPBPSStatus &= ~PBPS_PERFORM_REFUND;
        lwPBPSStatus |= PBPS_PERFORM_POST_OP_AUDIT;
    }
    else if (lwPBPSStatus & PBPS_PERFORM_POST_OP_AUDIT)
    {
        *pNewState = PBPS_WAITPSD;
        *pNewID = PBPS_PSDAUDIT_REQ;

        lwPBPSStatus &= ~PBPS_PERFORM_POST_OP_AUDIT;
    }
    else if (lwPBPSStatus & PBPS_PERFORM_POP_UPLOAD)
    {
        *pNewState = PBPS_SENDREQ;
        *pNewID    = PBPS_POP_UPLOAD;

/*      Initialize the POP upload */
        fnPOPInitUpload();
        
/*      Clear Perform POP Upload status bit */
        lwPBPSStatus &= ~PBPS_PERFORM_POP_UPLOAD;
    }
    else
    {
        *pNewState = PBPS_SENDREQ;
        *pNewID = PBPS_SENDSIGNOFF_REQ; 
    }

	dbgTrace(0, "-->-->--> fnPBPSGetNextService out lwPBPSStatus:0x%08x <--<--<--", lwPBPSStatus);
	dbgTrace(0, "-->-->--> fnPBPSGetNextService NewState:%d NewID:%d iStatus:%d <--<--<--", *pNewState, *pNewID, iStatus);
    return iStatus;
}

/**********************************************************
Function: fnPBPSInvestigateDcRspCode
Author: Derek DeGennaro
Description:
This function examines the return code from the data
center for the various responses.  It returns an appropriate
error code.  The error handler determines if the pbp task
can recover.  The error codes (given in the iCode parameter) 
are listed in MM97003.

Parameters:

Return Value:

**********************************************************/
static int fnPBPSInvestigateDcRsp(unsigned char bRequestedID,unsigned char bID, int iCode,int iMsgRcvStatus)
{
    int iStatus = PBPS_OK;

    /* If the message was transmitted ok, check to see that
    we have a known message. */
    if (iMsgRcvStatus == PBPS_OK)
    {
        if (bID == PBPS_DCGENERAL_RSP)
            iStatus = PBPS_DCGENRSP_ERR;
        else if (bRequestedID != bID || bID == PBPS_NODATAID)
            iStatus =  PBPS_DC_BADRESPONSE_ERR;
    }
    else
    {
        iStatus = iMsgRcvStatus;

    }

    /* If there is any cleanup required after getting receiving
    a message, do it now.*/
	if (bPBPSPreviousID == PBPS_DCAP_UPLOAD)
    {
        if (iStatus == PBPS_OK && iCode == DC_RESPONSE_NO_ERR)
        {
			//Set the upload state
			CMOSTrmUploadState = TRM_STATE_UPLOAD_CONFIRMED;
			CMOS2TrmDcapFileToBeUploaded = FALSE;
			trmClearDcapFileName();
        	(void) trmProcessTRUploadConfirmation();
        }

     }

    /* Now, if we are cleaned up and nothing is wrong do some
    pbp task specific error handling. */
    if (iStatus == PBPS_OK)
    {
        switch (bID) {
        case PBPS_DCSIGNON_RSP:
            {               
                switch (iCode) {
                case DC_RESPONSE_NO_ERR: iStatus = PBPS_OK; break;
                case DC_NO_ACCOUNT: iStatus = PBPS_DC_NO_ACCOUNT; break;
                case DC_NO_METER: iStatus = PBPS_DC_NO_METER; break;
                case DC_NO_LINK: iStatus = PBPS_DC_NO_LINK; break;

                case DC_GUID_MISMATCH: iStatus = PBPS_DC_GUID_MISMATCH;
                	break;
                case DC_NO_GUID: iStatus = PBPS_DC_NO_GUID;
                	break;
                default: iStatus = PBPS_DC_UNKNOW_RSP_STATUS;
                };
            }
            break;
        case PBPS_DCGENERAL_RSP:
            {
                switch (iCode) {
                case DC_COMM_TIMEOUT: iStatus = PBPS_DC_COMM_TIMEOUT; break;
                case DC_CASHBOX_CONN_ERR: iStatus = PBPS_DC_CASHBOX_CONN_ERR; break;
                case DC_ICEBOX_CONN_ERR: iStatus = PBPS_DC_ICEBOX_CONN_ERR; break;
                case DC_SERVER_EXCEPTION: iStatus = PBPS_DC_SERVER_EXCEPTION; break;
                case DC_PUBLIC_KEY_MISSING: iStatus = PBPS_DC_PUBLIC_KEY_MISSING; break;
                case DC_SERVER_PARSE_ERR: iStatus = PBPS_DC_SERVER_PARSE_ERR; break;
                case DC_MSG_OUTOFCONTEXT: iStatus = PBPS_DC_MSG_OUTOFCONTEXT; break;
                case DC_MSG_MISSINGPARAM: iStatus = PBPS_DC_MSG_MISSINGPARAM; break;
                case DC_MSG_PARAMFORMAT_ERR: iStatus = PBPS_DC_MSG_PARAMFORMAT_ERR; break;
                case DC_MSG_PARAMDATA_ERR: iStatus = PBPS_DC_MSG_PARAMDATA_ERR; break;
                default: iStatus = PBPS_DCGENRSP_ERR;
                };
            }
            break;
        case PBPS_DCFILEXFER_RSP:
            {
                switch (iCode) {
                case DC_RESPONSE_NO_ERR: iStatus = PBPS_OK; break;
                case DC_SERVER_PARSE_ERR: iStatus = PBPS_DC_SERVER_PARSE_ERR; break;
                case DC_MSG_OUTOFCONTEXT: iStatus = PBPS_DC_MSG_OUTOFCONTEXT; break;
                case DC_MSG_MISSINGPARAM: iStatus = PBPS_DC_MSG_MISSINGPARAM; break;
                case DC_MSG_PARAMFORMAT_ERR: iStatus = PBPS_DC_MSG_PARAMFORMAT_ERR; break;
                case DC_MSG_PARAMDATA_ERR: iStatus = PBPS_DC_MSG_PARAMDATA_ERR; break;
                case DC_SVR_RESEND_FILE: iStatus = PBPS_DC_SVR_RESEND_FILE; break;
                case DC_SVR_INCOMPLETE_FILE: iStatus = PBPS_DC_SVR_INCOMPLETE_FILE; break;
                case DC_SVR_WHAT_FILE_TYPE: iStatus = PBPS_DC_SVR_WHAT_FILE_TYPE; break;
                case DC_SVR_BAD_FILE_TYPE: iStatus = PBPS_DC_SVR_BAD_FILE_TYPE; break;
                case DC_SVR_BAD_DELCON_ZIP: iStatus = PBPS_DC_SVR_BAD_DELCON_ZIP; break;
                case DC_SVR_DUP_DELCON_REC: iStatus = PBPS_DC_SVR_DUP_DELCON_REC; break;
                default: iStatus = PBPS_DC_UNKNOW_RSP_STATUS;
                };
            }
            break;
        default:
            iStatus = PBPS_OK;
            break;
        };
    }

    return iStatus;
}

/**********************************************************
Function: fnPBPSCommitXmlData
Author: Derek DeGennaro
Description:
Function that is called after a successfull message has
been received from the data center.  It's purpose is to
store data that was received in XML form into it's final
form.
idle state.

Parameters:

Return Value:

**********************************************************/
static int fnPBPSCommitXmlData(unsigned char bDataID)
{
    unsigned long   lwFourBytes;
    double dblCMOSDebit=0.0, dblCMOSCredit=0.0;
    unsigned char cRefillBuffer[4];
    int iStatus = PBPS_OK;

    switch (bDataID) {
    case PBPS_DCAUDIT_RSP:
    case PBPS_DCREFILL_RSP:
        if (fDebitBal)
        {
            dblCMOSDebit = fnfGetCMOSDoubleParam(CMOS_DB_PBP_DEBIT_BAL);
            (void)fnfSetCMOSDoubleParam(CMOS_DB_PBP_DEBIT_BAL, dblDebitBal);
            (void)fnfSetCMOSDoubleParam(CMOS_DB_PBP_PREV_DEBIT_BAL, dblCMOSDebit);              
        }
        if (fCreditBal)
        {           
            dblCMOSCredit = fnfGetCMOSDoubleParam(CMOS_DB_PBP_CREDIT_BAL);
            (void)fnfSetCMOSDoubleParam(CMOS_DB_PBP_CREDIT_BAL, dblCreditBal);
            (void)fnfSetCMOSDoubleParam(CMOS_DB_PBP_PREV_CREDIT_BAL, dblCMOSCredit);
        }

        if (bDataID == PBPS_DCAUDIT_RSP && AuditRec.fReissue)
        {
            /* Give the new refill amount to bob. */
            //fnDouble2BinFour(cRefillBuffer,AuditRec.dblReissueAmt);
        	{
				lwFourBytes = (unsigned long)AuditRec.dblReissueAmt;
				//flip the Endian
				lwFourBytes = EndianSwap32(lwFourBytes);
				(void)memcpy(cRefillBuffer, &lwFourBytes, sizeof(unsigned long));
        	}
            fnDouble2BinFive(CMOSMoneyParams.RefillAmt,AuditRec.dblReissueAmt, FALSE);

            if (fnWriteData(BOBAMAT0, VAULT_REFILL_AMT, (void *)(&cRefillBuffer[0])) == BOB_OK)
            {
                /* Check to see if a refill is already scheduled. */
                if ((lwPBPSStatus & PBPS_PERFORM_REFILL) == 0)
                {
                    /* Schedule the refill. */
                    lwPBPSStatus |= PBPS_PERFORM_REFILL;
                	dbgTrace(0, "-->-->--> fnPBPSCommitXmlData AuditRec.fReissue %d <--<--<--", AuditRec.fReissue);
                    lwPBPSStatus |= PBPS_PERFORM_POST_OP_AUDIT;
                }

                /* Turn reissue off so we don't continue doing this */
                // Don't turn off! Need to preserve this flag until the PSD 
                //  responds, then it will be cleared, in fnPBPSHandlePSDMsg.
                //AuditRec.fReissue = FALSE;
            }
            else
            {
                /* There was an error setting the refill amount. */
                iStatus = PBPS_PSD_SET_REFILL_AMT_ERR;
            }
        }

        break;
    default:
        iStatus = PBPS_OK;
        break;
    };

    return iStatus;
}

/************************************
    Routine to get the address 
    of the data center.
************************************/

/**********************************************************
Function: fnPBPSGetServerAddress
Author: Derek DeGennaro
Description:
This function takes a service request and determines the
necessary information regarding the server that will
perform the service.  This information includes the
fully decoded URL, the IP address, and the port number. 

Parameters:
pSocket - The socket descriptor that will be used
to connect to the data center.  This function fills
in the descriptor with the parsed url information
(protocol,host,port,path), the requested url,
and the resolved ip address of the data center
server.

Return Value:
PBPS_OK - no errors
PBPS_PROCEDURE_ERR - invalid data passed to it
PBPS_UNKNOWNURL_ERR - Falied DNS lookup of the host name
(no IP address available).

**********************************************************/
static int fnPBPSGetServerAddress(SOCKET_DESCRIPTOR *pSocket, sServiceURL *pService)
{
    NU_HOSTENT hostEnt;
    char *pHostAddrList=NULL;
    STATUS      nu_err;
    unsigned char bHostIpAddr[4];
    char pLogBuf[200];
    BOOL bUsingProxy = FALSE ;


    if (pService != NULL && pSocket != NULL)
    {
        /* clear out the host entry struct and address */
        memset((void*)&hostEnt,0,sizeof(hostEnt));
        hostEnt.h_addr_list = &pHostAddrList;
        memset((void*)&pSocket->Address,0,sizeof(pSocket->Address));        

		strcpy(pSocket->serverURL, pService->pServiceURL);
		
   		(void)sprintf(pLogBuf, "URL = %s", pService->pServiceURL);
   		fnDumpStringToSystemLog(pLogBuf);

        /* url / ip is given in pMsg */
        if (crackUrl(&pSocket->urlInfo, pService->pServiceURL) == NU_SUCCESS)
        {

            /* is the host specified as a name or ip address? */
            if (isIpAddress(pSocket->urlInfo.host) == FALSE)
            {
                /* get IP address using DNS lookup */
            	//<<<<<<<<<<<<<<<<<<<<-- Proxy addition -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            	// if proxy is in use we cannot do a dns lookup and its result would be useless anyway since we need the proxy address
//            	if(globalProxyConfig.fEnabled && globalProxyConfig.Https.pAddress != NULL)
               	if(getProxyUseState(pSocket->urlInfo.host) == PROXY_URL_INCLUDED)
            	{
            		// let's copy the proxy address into the required ip for now
            		dottedQuadToArray(pSocket->Address.id.is_ip_addrs, globalProxyConfig.Https.pAddress) ;

            		// force use of port 80 if we are in proxymode, this seems to be a requirement of CCProxy and does not seem to matter for others...
            		pSocket->Address.port = globalProxyConfig.Https.uwPort ; // 80;
//            		pSocket->fUseProxy;
            		bUsingProxy = TRUE ;
            	}
            	else
				//<<<<<<<<<<<<<<<<<<<-- End of proxy addition -- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            	{
					nu_err = NU_Get_Host_By_Name(pSocket->urlInfo.host, &hostEnt);
					if (nu_err == NU_SUCCESS)
					{
						pSocket->Address.id.is_ip_addrs[0] = hostEnt.h_addr[0];
						pSocket->Address.id.is_ip_addrs[1] = hostEnt.h_addr[1];
						pSocket->Address.id.is_ip_addrs[2] = hostEnt.h_addr[2];
						pSocket->Address.id.is_ip_addrs[3] = hostEnt.h_addr[3];
					}
					else
					{
						(void)sprintf(pLogBuf, "Couldn't resolve URL, NU err = %d", nu_err);
						fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
						return PBPS_UNKNOWNURL_ERR;
					}
            	}
            }
            else
            {
                /* IP address is present in the url */
                dottedQuadToArray((unsigned char *)bHostIpAddr, pSocket->urlInfo.host);

                //hostEnt.h_addr = (char*)bHostIpAddr;
                pHostAddrList = (char *)bHostIpAddr;
                pSocket->Address.id.is_ip_addrs[0] = hostEnt.h_addr[0];
                pSocket->Address.id.is_ip_addrs[1] = hostEnt.h_addr[1];
                pSocket->Address.id.is_ip_addrs[2] = hostEnt.h_addr[2];
                pSocket->Address.id.is_ip_addrs[3] = hostEnt.h_addr[3];
            }
        }
        else
        {
            (void)sprintf(pLogBuf, "Couldn't crack the URL");
            fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
			
	   		(void)sprintf(pLogBuf, "Could not resolve Tier3 URL = %s", pService->pServiceURL);
	        logException(pLogBuf);
            return PBPS_UNKNOWNURL_ERR;
        }
        
        sprintf( pLogBuf, "PBP Server IP is %d.%d.%d.%d", 
                 (unsigned char)pSocket->Address.id.is_ip_addrs[0],
                 (unsigned char)pSocket->Address.id.is_ip_addrs[1],
                 (unsigned char)pSocket->Address.id.is_ip_addrs[2],
                 (unsigned char)pSocket->Address.id.is_ip_addrs[3] );
				 
        fnDumpStringToSystemLog( pLogBuf ); 

        /* set some common socket stuff */  
        pSocket->Address.family = NU_FAMILY_IP;
        pSocket->Address.name = "PBPSocket";
        
#ifdef ALLOW_NON_SSL
        // set the necessary protocol and port information
		// the use of SSL is controlled by the distributor.		
        if (strlen(pSocket->urlInfo.protocol) > 0)
        {
            (void)sprintf(pLogBuf, "Original URL Protocol = %s", pSocket->urlInfo.protocol);
            fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
			
	        /* The distributor specified the protocol.  */
            if (strncmp(pSocket->urlInfo.protocol,"https",5) == 0)
            {
                pSocket->fUseSSL = TRUE;
            }
            else if (strncmp(pSocket->urlInfo.protocol,"http",4) == 0)
            {
                pSocket->fUseSSL = FALSE;
            }
            else
            {
                /* The distributor specified an unknown protocol.
                   Set the protocol to the default HTTP - don't use SSL. */
	            (void)strcpy(pSocket->urlInfo.protocol,"http://");
	            pSocket->fUseSSL = FALSE;
            }
        }
		else
		{
	        (void)sprintf(pLogBuf, "Original URL Protocol not specified");
	        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));

	        /* The distributor did not specify the protocol.
				By default SSL is not used. */
	        (void)strcpy(pSocket->urlInfo.protocol,"http://");
	        pSocket->fUseSSL = FALSE;
		}
#else
        pSocket->fUseSSL = TRUE;
        (void)strcpy(pSocket->urlInfo.protocol,"https://");
#endif

        if (strlen(pSocket->urlInfo.port) > 0)
        {
   			(void)sprintf(pLogBuf, "Original URL Port = %s", pSocket->urlInfo.port);
   			fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));

	        /* The distributor specified the port.  */
	        pSocket->Address.port = (unsigned short)atoi(pSocket->urlInfo.port);
        }
		else
		{
	        (void)sprintf(pLogBuf, "Original URL Port not specified");
	        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));

	        /* The distributor did not specified the port.
	        	The protocol determines the port. */
	        if (pSocket->fUseSSL == TRUE)
	        {
	            pSocket->Address.port = bUsingProxy ? globalProxyConfig.Https.uwPort : 443;
	        }
	        else
	        {
	            pSocket->Address.port = bUsingProxy ? globalProxyConfig.Http.uwPort : 80;
	        }
		}

        if (pSocket->fUseSSL == TRUE)
        {
 			(void)sprintf(pLogBuf, "Using SSL, Protocol = %s", pSocket->urlInfo.protocol);
   			fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
 			(void)sprintf(pLogBuf, "Using SSL, Port = %u", pSocket->Address.port);
   			fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
        }
        else
        {
 			(void)sprintf(pLogBuf, "Not using SSL, Protocol = %s", pSocket->urlInfo.protocol);
   			fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
 			(void)sprintf(pLogBuf, "Not using SSL, Port = %u", pSocket->Address.port);
   			fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
        }

		if(fnDoWeNeedToUseProxy(pSocket->urlInfo.protocol, TIER3_CONNECTION))	
		{
			pSocket->fUseProxy = 1;
            switch (fnSetupProxySocket(&pSocket->Address, pSocket->urlInfo.protocol))
            {
                case PROXY_SOCKET_ERR_CRACK_URL:
                case PROXY_SOCKET_ERR_RESOLVE_HOST:
                    return PBPS_UNKNOWNURL_ERR;

				default:
					break;
            }
		}
        
        return PBPS_OK;
    }
    else
    {
        /* Invalid Parameters. */
        (void)sprintf(pLogBuf, "fnPBPSGetServerAddress got NULL ptrs");
        fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
        return PBPS_PROCEDURE_ERR;
    }
}

/************************************
    Routines to get messages from the
    Data Center.
************************************/

/**********************************************************
Function: fnPBPSReceiveMsg
Author: Derek DeGennaro
Description:
Function that receives the Http body of the 
request/response from the server. 

Parameters:

Return Value:

**********************************************************/
static int fnPBPSReceiveMsg(void)
{
    unsigned char PBS_Handler = PBPS_PARSER_HANDLER;   // Assume a normal parser message process
    int iCount = 0, iParserError = PBPS_OK, iResult = PBPS_OK, iEndResult = PBPS_OK;
    int iSocketStatus = PBPS_OK;
    BOOL fParseDone = FALSE;
    char *pSend; 
    //char *pResumeStream=NULL;
    DXMLParser *TmpltDXML = NULL;
    uchar timeWaiting = 0;

    /* initialize task XML variables */
    fnPBPS_InitParserData();
    
    /* initialize task Http variables and Http Parser */
    fnPBPS_InitHttpParserData();

    /* initialize the XML Parser */
    iResult = (int)XMLParserInit(&dxmlReceive,   /* parser struct */
        (short)(ARRAY_LENGTH(PBPS_XmlLookupTbl)),/* # entries */
        (ElementLookupEntry*)PBPS_XmlLookupTbl, /* entries */
        fnPBPS_ParserStart,                     /* parser start fcn */
        fnPBPS_ParserEnd,                       /* parser end fcn */
        fnPBPS_StartElement,                    /* default parser start element fcn */
        fnPBPS_EndElement,                      /* default parser end element fcn */
        fnPBPS_ElementChars,                    /* default parser char data fcn */
        0);

    dbgMsg("fnPBPSReceiveMsg PbpE#", iResult, __LINE__);
    if (iResult != XML_STATUS_OK)
        return PBPS_PARSE_ERR;                  /* internal XML parser error */

    while (!fParseDone)
    {
        /* Try to read some data from the socket */
        iCount = fnPBPSSocketReceive(&pbpSocket,(char*)pParserBuff,PBPS_XML_RECV_BUFF_LEN);
        
        if (strstr((char *)pParserBuff, "100 Continue"))
        {
            continue;
        }
        
        if (iCount > 0)
        {
            /* there were some bytes, send them to parser */
            pSend = (char*)pParserBuff;

            send_PbpIO(pSend,Tier3Rx,'<','>');

            while (pSend < ((char*)pParserBuff + iCount) && !fParseDone)
            {
                iResult = fnPBPS_SendHttpParserChar(*pSend);
                /* if there was a serious http syntax error
                or the end of the http was received succesfully
                then we are done.
                else if the http parser is done and the char
                is part of the http parser body send it to
                xml parser.*/

                dbgMsg("fnPBPSReceiveMsg PbpE#", iResult, __LINE__);
                if (fHttpIsBodyChar && !fXMLParserError && iResult == PBPS_OK)
                {
                    switch(PBS_Handler)
                    {
                    case PBPS_PARSER_HANDLER:               // Process message using the PBPS_XmlLookupTbl[]
                                                            // Buffer data for future reference
                        iResult = (int)XMLSendParserChar(&dxmlReceive,(XmlChar)*pSend);
                        dbgMsg("fnPBPSReceiveMsg PbpE#", iResult, __LINE__);
                        
                        if (iResult != XML_STATUS_OK)
                        {                                   // Did Parser detect an anomoly?
                            fXMLParserError = TRUE;     // Indicate some other Parser issue
                            iParserError = iResult;
                        }                       
                        break;

                    default:
                        iResult = PBPS_PROCEDURE_ERR;
                        dbgMsg("fnPBPSReceiveMsg PbpE#", iResult, __LINE__);
                    }
                }
                
                if (bHttpState == HTTP_DONE)
                {
                    fParseDone = TRUE;                          
                }
                
                pSend++;
            }
        }
        else if (iCount == 0)
        {
            /* suspend to allow nucleus net task
            receive data */
            OSWakeAfter(PBPS_SUSPEND_LEN);

            if (++timeWaiting == PBPS_NUM_SUSPEND)
            {
                fParseDone = TRUE;
                iSocketStatus = PBPS_SOCKETTIMEOUT_ERR;
            }
        }
        else
        {
            /* socket error */
            fParseDone = TRUE;
            iSocketStatus = PBPS_SOCKETRECV_ERR;
        }
    }
    
    if (TmpltDXML != NULL)
    {
        fnSystemLogEntry(SYSLOG_TEXT, "PBP Task failed to free parser memory", 27);
        free(TmpltDXML);
    }
    
    /* tell the parser it is done */
    iEndResult = XMLParserEndParse(&dxmlReceive,!fXMLParserError);
        
    if (iEndResult != XML_STATUS_OK)
    {
        iParserError = iEndResult;      
        fXMLParserError = TRUE;
        dbgMsg("fnPBPSReceiveMsg PbpE#", iEndResult,__LINE__);
    }
    
    if (iSocketStatus != PBPS_OK)
    {
        /* the connection was not ok */
        return iSocketStatus;
    }

    /* check to see if the Http Server has sent
    an unexpected Connection close token */
    if (fHttpCloseConnection == TRUE && bPBPSDataID != PBPS_DCSIGNOFF_RSP && bPBPSDataID != PBPS_DCGENERAL_RSP)
        return PBPS_HTTP_UNEXPECTED_CLOSE;
        
    /* check to see if we should have received
    a Connection close token and didn't */
    if (fHttpCloseConnection != TRUE && (bPBPSDataID == PBPS_DCSIGNOFF_RSP || bPBPSDataID == PBPS_DCGENERAL_RSP))
        return PBPS_HTTP_CLOSE_EXPECTED;
        
    /* we are parsing a full body here */
    if (fHttpBadStatus || fHttpContinueRspFound)
        return PBPS_HTTP_PARSE_ERR;

    /* check for parser errors */
    if (fXMLParserError)
        return iParserError;
    
    /* check for other errors */
    if (iResult != PBPS_OK)
        return iResult;

    return PBPS_OK;
}

/************************************
    Routines to send messages to
    Data Center.
************************************/

void fnPBPsetCmdStaCmplt(const int iCmdStatus)
{
    iCmdStaCmp = iCmdStatus;
}

/**********************************************************
Function: fnPBPSSendMsg
Author: Derek DeGennaro
Description:
Function that sends an Http message to the server.

Parameters:

Return Value:

**********************************************************/
static int fnPBPSSendMsg(unsigned char bDataID)
{
    XMLFormatScript *pScript = NULL;

    int iResult;
    int iCount = 0;
    BOOL fParseDone = FALSE;
    int iSocketStatus = PBPS_OK;
    char *pSend;
    uchar timeWaiting = 0;
	char proxyAuth[350]={0};    
	int totalCount =0;

    /* make sure data id is ok */
    if (bDataID >= PBPS_NUM_DATAID ||
        TheBigTable[bDataID].bHandlerType == PBPS_NO_HANDLER ||
        TheBigTable[bDataID].handlerInfo == NULL)
        return PBPS_PROCEDURE_ERR;

    pScript = (XMLFormatScript*)TheBigTable[bDataID].handlerInfo;
	if (pScript->pTag != NULL)
		send_PbpIO((char*)pScript->pTag, Tier3Tx,'\0','\0');

    /* Send the Http Header */
    *pFormatterBuff = 0;
    ulFormatterBuffLen = 0;
	if ((pbpSocket.fUseProxy == TRUE) && (pbpSocket.fUseSSL == FALSE))
	{
		(void)fnGetProxyAuthStringForHeader(pbpSocket.serverURL, proxyAuth);
    	totalCount = sprintf(cHttpHeaderBuff,"POST %s HTTP/1.1\r\nHost: %s\r\n", pbpSocket.serverURL, pbpSocket.urlInfo.host);
	    totalCount += sprintf(cHttpHeaderBuff + totalCount, "%s", proxyAuth);  //Rest of header
	}
	else
	{
    	sprintf(cHttpHeaderBuff,"POST %s HTTP/1.1\r\nHost: %s\r\n",pbpSocket.urlInfo.path,pbpSocket.urlInfo.host);
	}

    strcat(cHttpHeaderBuff,"Transfer-Encoding: chunked\r\nContent-Type: text/xml\r\nExpect: 100-continue\r\n");
    if (bPBPSState == PBPS_SENDREQ && bDataID == PBPS_SENDSIGNOFF_REQ)
        strcat(cHttpHeaderBuff,"Connection: close\r\n");
		
    strcat(cHttpHeaderBuff,"\r\n");
    (void)fnPBPSSocketSend(&pbpSocket,cHttpHeaderBuff,(int)strlen(cHttpHeaderBuff));

    /* Now the server should send an HTTP/1.1 100 Continue response */
    fnPBPS_InitHttpParserData();
    while (!fParseDone)
    {
        iCount = fnPBPSSocketReceive(&pbpSocket,(char*)pParserBuff,PBPS_XML_RECV_BUFF_LEN);
        if (iCount > 0)
        {
            /* there were some bytes, send them to parser */
            pSend = (char*)pParserBuff;
            while (pSend < ((char*)pParserBuff + iCount) && !fParseDone)
            {
                iResult = fnPBPS_SendHttpParserChar(*pSend);
                if (bHttpState == HTTP_DONE)
                    fParseDone = TRUE;
                pSend++;
            }
        }
        else if (iCount == 0)
        {
            /* no bytes at this time */
            OSWakeAfter(PBPS_SUSPEND_LEN);

            if (++timeWaiting == PBPS_NUM_SUSPEND)
            {
                fParseDone = TRUE;
                iSocketStatus = PBPS_SOCKETTIMEOUT_ERR;
            }
        }
        else
        {
            /* socket error */
            fParseDone = TRUE;
            iSocketStatus = PBPS_SOCKETRECV_ERR;
        }
    }

    /* check socket */
    if (iSocketStatus != PBPS_OK)
    {
        /* the connection was not ok */
        return iSocketStatus;
    }

    /* check Http */
    if (iHttpStatusCode != 100 || fHttpCloseConnection || fHttpBadStatus)
        return PBPS_HTTP_PARSE_ERR;

    switch(TheBigTable[bDataID].bHandlerType)
    {
    case PBPS_FORMATTER_SEND_HANDLER:
        /* choose script based on data id */
        pScript = (XMLFormatScript*)TheBigTable[bDataID].handlerInfo;

        iResult = (int)XMLFormat(pScript,
            fnPBPS_Formatter_Start,
            fnPBPS_Formatter_End,
            fnPBPS_Formatter_Char,
            0);
        break;

    default:
        iResult = PBPS_PROCEDURE_ERR;
    }
	
    if (iResult != XML_STATUS_OK)
    {
        /* there was an error, iResult stores a
        custom error code. */
        return iResult;
    }

    return PBPS_OK;
}

/**********************************************************
Function: fnPBPSSendHttpChunk
Author: Derek DeGennaro
Description:
This function takes part of an HTTP Body and generates a
"chunk" that is sent to the Data Center.  A chunk is a string
of bytes that consists of an ascii hex number which contains
the number of bytes in the chunk, a CRLF, a series of
bytes (the number of which was in the previous line), followed
by a CRLF.  A special zero-length "empty" chunk signals the
end of the HTTP body. 
Parameters:
pSocket - Socket descriptor used to send the data
pChunk - The buffer containing part of the HTTP body.
ulSize - The number of bytes in pChunk.
Return Value:
XML_STATUS_OK if successfull, otherwise the number of 
bytes sent to the Data Center.
**********************************************************/
static short fnPBPSSendHttpChunk(SOCKET_DESCRIPTOR *pSocket, char *pChunk, unsigned long ulSize)
{
    int iLength,iSent;
    char cLength[10];
    
    if (pSocket != NULL && ulSize > 0 && pChunk != NULL)
    {
        /* output the size */
        iLength = sprintf(cLength,"%lX\r\n",ulSize);
        iSent = fnPBPSSocketSend(pSocket,cLength,iLength);
        if (iSent != iLength)
            return PBPS_SOCKETSEND_ERR;

        /* append a CRLF for HTTP purposes */
        *((XmlChar*)pChunk + ulSize++) = (XmlChar)'\r';
        *((XmlChar*)pChunk + ulSize++) = (XmlChar)'\n';
        *((XmlChar*)pChunk + ulSize) = (XmlChar)NULL;

        /* output the chunk */
        iSent = fnPBPSSocketSend(pSocket,pChunk,(int)ulSize);
        if (iSent != (int)ulSize)
            return PBPS_SOCKETSEND_ERR;
        else
            return XML_STATUS_OK;
    }
    else
    {
        return PBPS_SOCKETSEND_ERR;
    }
}

/************************************
    Socket Wrapper Functions
************************************/

/**********************************************************
Function: fnPBPSInitializeSocket
Author: Derek DeGennaro
Description:
This function initializes the a PBP socket descriptor.
Parameters:
pSocket - Socket descriptor used to communicate with the
data center.
Return Value:
XML_STATUS_OK if successfull, otherwise one of the 
error codes defined in pbptask.h.
**********************************************************/ 
static int fnPBPSInitializeSocket(SOCKET_DESCRIPTOR *pSocket)
{
    if (pSocket == NULL)
        return PBPS_PROCEDURE_ERR;
		
    pSocket->iSocketID = -1;
    memset((void*)&pSocket->urlInfo,0,sizeof(pSocket->urlInfo));
    memset((void*)&pSocket->Address,0,sizeof(pSocket->Address));
    
    pSocket->sslDesc=NULL;
    pSocket->sslConnection=NULL;
    pSocket->sslWriteBIO=NULL;
    pSocket->sslReadBIO=NULL;
    pSocket->sslConnectStatus=0;
    memset((void*)pSocket->bCertificateBuff,0,sizeof(pSocket->bCertificateBuff));

    return PBPS_OK;
}

/**********************************************************
Function: fnPBPSCreateSocket
Author: Derek DeGennaro
Description:
This function creates a handle to a socket descriptor by
calling the network library socket API.

Parameters:
pSocket - Socket descriptor used to communicate with the
data center
Return Value:
NU_SUCCESS if successfull, otherwise an error.
**********************************************************/
static int fnPBPSCreateSocket(SOCKET_DESCRIPTOR *pSocket)
{   
    int iReturnValue;

    if (pSocket == NULL)
        return PBPS_PROCEDURE_ERR;

    /* create socket handle using NU_Socket */
    pSocket->iSocketID = NU_Socket(NU_FAMILY_IP,NU_TYPE_STREAM,NU_NONE);
    if (pSocket->iSocketID >= 0)
    {
        CHAR strTmp[50];
        snprintf(strTmp, 50, "fnPBPSCreateSocket using socket %d", pSocket->iSocketID);
        NLOG_Error_Log(strTmp, NERR_INFORMATIONAL,__FILE__, __LINE__);

        iReturnValue = PBPS_OK;
    }
    else
    {
        iReturnValue = PBPS_UNABLE_CONNECT;
        dbgMsg("PbpE#", iReturnValue, __LINE__);
    }

    return iReturnValue;    
}
/**********************************************************
Function: fnPBPSDestroySocket
Author: Derek DeGennaro
Description:
This function is called to release any socket resources
after the UIC has disconnected from the data center.
If the UIC is using HTTP over SSL then this function
releases the SSL resources as well.
Parameters:
pSocket - The socket descriptor that needs to be freed.
Return Value:
**********************************************************/
static void fnPBPSDestroySocket(SOCKET_DESCRIPTOR *pSocket)
{
    if (pSocket == NULL)
        return;

    fnPBPSDestroySSLConnection(pSocket);        

    if (pSocket->iSocketID >= 0)
    {
        /* A socket was created. Release it. */
        (void)NU_Close_Socket(pSocket->iSocketID); 
        pSocket->iSocketID = -1;
    }   
}


/**********************************************************
Function: fnPBPSConnectSocket
Author: Derek DeGennaro
Description:
This function makes a connection to a Data Center server.
If HTTP over SSL is used then the function makes the
SSL connection.
Parameters:
pSocket - Socket descriptor for the socket.  The descriptor
contains parsed url info (protocol,host,port,and path), 
the Network Library socket descriptor handle, and the 
ip address/port of the Network Library socket.  If using
SSL the descriptor also includes all the necessary socket
information.
Return Value:
PBPS_OK if successfull, an error otherwise.
**********************************************************/
static int fnPBPSConnectSocket(SOCKET_DESCRIPTOR *pSocket)
{
    STATUS iSocketStatus = NU_SUCCESS;


    if (pSocket != NULL)
    {
        /* connect the socket to the server using NU_Connect */

        //<<<<<<<<<<<<<<<<<<<<<<<-- Proxy Addition -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    	// RD CCProxy seems to insist on port 80 apache does not care so for now force use of port 80 if we are in proxy mode
//        ProxyProtocolSettings *pProxy = fnGetProxyProtocolSettings(pSocket->serverURL);
//        if((pProxy != NULL) && (strlen(pProxy->pAddress) > 0))

//        if(getProxyUseState(pSocket->serverURL) == PROXY_URL_INCLUDED)
//        {
//        	pSocket->Address.port = 80; //  force use of port 80 if we are using a proxy.
//        }
//        else
//        	pSocket->Address.port = getProxyUseState(pSocket->serverURL) == PROXY_URL_INCLUDED ? 80 : pSocket->Address.port;

        iSocketStatus = NU_Connect(pSocket->iSocketID,&pSocket->Address,0);
        // If this is a proxy connection the address will already have been changed but we need to send the connect request to the proxy
        // this function checks to see if proxy is enabled.
        if(iSocketStatus >= 0)
        	iSocketStatus =  sendProxyConnectMessage(pSocket->iSocketID, pSocket->urlInfo.host, 443 );
        //<<<<<<<<<<<<<<<<<<<<<<<-- End of proxy addition -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        if (iSocketStatus >= 0)
        {
            /* set the socket to block. */
            if (NU_Fcntl(pSocket->iSocketID, NU_SETFLAG, NU_BLOCK) == NU_SUCCESS)
            {               
                sprintf( pbpLog, "PBP: Socket Connection: Block for Socket %d", pSocket->iSocketID  );
                if (pSocket->fUseSSL == FALSE)
                {
                    /* The socket has been connected and the UIC is using normal HTTP. */
                    strcat( pbpLog, " http."  );
                    fnDumpStringToSystemLog( pbpLog );
                    return PBPS_OK;
                }
                else
                {
                    /* The socket has been connected and the UIC is using HTTP over SSL (HTTPS). */
                    strcat( pbpLog, " https."  );
                    fnDumpStringToSystemLog( pbpLog );
                    return fnPBPSCreateSSLConnection(pSocket);
                }                                   
            }
            else
            {   
                dbgMsg("PbpE#", PBPS_UNABLE_CONNECT,__LINE__);
                return PBPS_UNABLE_CONNECT;
            }
        }
        else
        {
            dbgMsg("PbpE#", PBPS_UNABLE_CONNECT,__LINE__);
            return PBPS_UNABLE_CONNECT;
        }
    }
    else
    {
        return PBPS_PROCEDURE_ERR;
    }
}

/**********************************************************
Description:
This function takes a buffer and puts in it a generated file name that is based on some type.

Parameters:
bFileNameBuff - buffer for the name of the file
type - type of name desired

Return PBPS_OK if successful, otherwise one of the error codes defined in pbptask.h.
**********************************************************/
static BOOL fnPBPSGetCertificateListFileName(char *bFileNameBuff, size_t filebuflen, CERT_LIST_NAME_TYPE type)
{
    BOOL iStatus = FALSE;
	DATETIME dateTimeStamp;

    if ((bFileNameBuff == NULL) || (filebuflen < PBPS_MAX_CERT_TEMP_FILENAME_SIZE))
    {
    	return iStatus;
    }

    /* clear buffer */
    (void)memset((void*)bFileNameBuff,0,filebuflen);

    // first part always the same
    (void)strncpy(bFileNameBuff, PBPS_CERTLIST_FILE_NAME, PBPS_CERT_LIST_FILE_ROOT_LEN);

    switch (type)
    {
    	case CLNT_CURRENT:
            iStatus = TRUE;
            break;
    	case CLNT_PREDECESSOR:
    		strcat(bFileNameBuff, "_PREDECESSOR");
            iStatus = TRUE;
            break;
    	case CLNT_CANDIDATE:
    		strcat(bFileNameBuff, "_CANDIDATE");
            iStatus = TRUE;
            break;
    	case CLNT_DOWNLOADED:
        	GetSysDateTime(&dateTimeStamp, ADDOFFSETS, GREGORIAN);
        	snprintf((bFileNameBuff + PBPS_CERT_LIST_FILE_ROOT_LEN),
        			(filebuflen - PBPS_CERT_LIST_FILE_ROOT_LEN),
    				"_%02d%02d%02d%02d%02d%02d%02d",
        			dateTimeStamp.bCentury, dateTimeStamp.bYear, dateTimeStamp.bMonth, dateTimeStamp.bDay,
    				dateTimeStamp.bHour, dateTimeStamp.bMinutes, dateTimeStamp.bSeconds);
            iStatus = TRUE;
            break;
    	default:
    		break;
    }

    // last part always the same
	strcat(bFileNameBuff, PBPS_CERTLIST_FILE_EXT);

    return iStatus;
}

/**********************************************************
Description:
This function replaces the current certificate list file with a new one.

* Steps:
 * acquire semaphore for current file
 * delete any predecessor file,
 * rename current file to predecessor,
 * rename candidate file to current file, if this fails, rename predecessor back to original
 * release semaphore for current file

Return Value:
PBPS_OK if successful, otherwise one of the error codes defined in pbptask.h.
**********************************************************/
static int fnPBPSInstallCertificateFile(char *candFileName)
{
    int iStatus = PBPS_OK;
	struct stat fileInfo;
	int fileStatus;
    char predFileNameBuff[PBPS_MAX_CERT_TEMP_FILENAME_SIZE];
    char curFileNameBuff[PBPS_MAX_CERT_TEMP_FILENAME_SIZE];
	char  semstat;
    char aErr[80] = {0};

	// get name of predecessor file
	(void) fnPBPSGetCertificateListFileName(predFileNameBuff, sizeof(predFileNameBuff), CLNT_PREDECESSOR);

	// get name of current file
	(void) fnPBPSGetCertificateListFileName(curFileNameBuff, sizeof(curFileNameBuff), CLNT_CURRENT);

    // Get semaphore for accessing certificate list file
	semstat = OSAcquireSemaphore (CERT_FILE_SEM_ID, 1000);
	if (semstat != SUCCESSFUL)
	{// cannot acquire semaphore even after waiting 1000 ms - something is wrong internally
		dbgTrace(DBG_LVL_ERROR,"Error: Failed to acquire certificate file semaphore: %d\n", semstat);
    	return PBPS_CERT_LIST_FILE_UNAVAILABLE;
	}

	// delete old predecessor file if it exists because it will be replaced
	fileStatus = stat( predFileNameBuff, &fileInfo );
    if (fileStatus == POSIX_SUCCESS)
    {
        fileStatus = unlink(predFileNameBuff);
        OSWakeAfter(0); // relinquish control to OS so it can properly update file system
    }
    fnCheckFileSysCorruption(fileStatus);

    // rename current file
    fileStatus = rename(curFileNameBuff, predFileNameBuff);
    if (fileStatus == POSIX_ERROR)
    {
    	(void) OSReleaseSemaphore(CERT_FILE_SEM_ID);
        fnCheckFileSysCorruption(fileStatus);
        strerror_r(errno, aErr, sizeof(aErr)-1);
        dbgTrace(DBG_LVL_ERROR,"Error: Failed to rename current cert list file %s: %s\n", curFileNameBuff, aErr);
        return PBPS_CERT_LIST_FILE_RENAME_ERROR;
    }

    // rename candidate file
    fileStatus = rename(candFileName, curFileNameBuff);
    if (fileStatus == POSIX_ERROR)
    {// try to rename predecessor back to current, leave candidate as is
    	(void) OSReleaseSemaphore(CERT_FILE_SEM_ID);
        fnCheckFileSysCorruption(fileStatus);
        strerror_r(errno, aErr, sizeof(aErr)-1);
        dbgTrace(DBG_LVL_ERROR,"Error: Failed to rename candidate cert list file %s: %s\n", candFileName, aErr);
        fnCheckFileSysCorruption( rename(predFileNameBuff, curFileNameBuff) );
        return PBPS_CERT_LIST_FILE_RENAME_ERROR;
    }

    // all went well, so release semaphore
	(void) OSReleaseSemaphore(CERT_FILE_SEM_ID);
    dbgTrace(DBG_LVL_INFO, "Downloaded root cert list candidate file installed");


    return iStatus;

}

/**********************************************************
Description:
   Get Current cert list version from static SSL Cert Descriptor
 * This should only be called when an SSL session exists */
static int fnPBPSGetCurrentCertificateListVersion(void)
{
	if (pbpSocket.sslDesc == NULL)
	{// default
		return 0;
	}
	else
	{// get from static socket descriptor
		return 	pbpSocket.sslDesc->certListDesc.iCertListVer;
	}
}

/**********************************************************
Description:
This function takes the certificates and associated data from the input file and
creates a new root cert list file that replaces the current one.  It does not copy
the signature so signature is only verified once on download and not every time
certificates are loaded into SSL session.

* Steps:
 * read xml from downloaded file
 * delete any pre-existing candidate file
 * create candidate file,
 * populate candidate file with xml data
 * call function to replace current file with candidate

Return Value:
PBPS_OK if successful, otherwise one of the error codes defined in pbptask.h.
**********************************************************/
static int fnPBPSActivateCertificates(char *bFileName, long sigOffset)
{
    FILE *pCertFileHandle = NULL;
	struct stat fileInfo;
	int fileStatus;
    long lwDataSize = 0;
    size_t readlen, datalen = 0;
    unsigned char *pBuffer;
	BOOL osstat;
    char candFileNameBuff[PBPS_MAX_CERT_TEMP_FILENAME_SIZE];
    char *strres;

    fileStatus = stat( bFileName, &fileInfo );
    if (fileStatus == POSIX_ERROR)
    {
        fnCheckFileSysCorruption(fileStatus);
        return PBPS_CERT_LIST_FILE_STAT_ERROR;
    }
	lwDataSize = fileInfo.st_size;

	if (sigOffset >= lwDataSize)
	{// either size or offset is not right
        return PBPS_CERT_LIST_FILE_STAT_ERROR;
	}

    pCertFileHandle = fopen(bFileName,"rb");
    if (pCertFileHandle == NULL)
    {
        fnCheckFileSysCorruption(POSIX_ERROR);
        /* Couldn't open the file. */
    	return PBPS_CERT_LIST_FILE_OPEN_ERROR;
    }

    fileStatus = fseek(pCertFileHandle, sigOffset, SEEK_SET);
    if (fileStatus == POSIX_ERROR)
    {
        fnCheckFileSysCorruption(fileStatus);
        fnCheckFileSysCorruption(fclose(pCertFileHandle));
        return PBPS_CERT_LIST_FILE_SEEK_ERROR;
    }

    // allocate memory for file
    datalen = lwDataSize - sigOffset;
    osstat = OSGetMemory ((void **) &pBuffer, datalen );
    if (osstat == FAILURE)
    {
        fnCheckFileSysCorruption(fclose(pCertFileHandle));
        return PBPS_SSL_OUT_OF_MEMORY;
    }
	memset((void*)pBuffer, 0, datalen);

	// read char data from file
	readlen = fread((void*)pBuffer, 1, datalen, pCertFileHandle);
	if (readlen < datalen)
    {
	    (void) OSReleaseMemory(pBuffer);
        fnCheckFileSysCorruption(POSIX_ERROR);   //check errno for fread
        fnCheckFileSysCorruption(fclose(pCertFileHandle));
		return PBPS_CERT_LIST_FILE_READ_ERROR;
    }

	// close input file
	fnCheckFileSysCorruption(fclose(pCertFileHandle));

	// Validate data
	// Does it have allowed ciphers?
	strres = strstr((const char *)pBuffer, "AllowedCipher");
	if (strres == NULL)
	{//no ciphers
	    (void) OSReleaseMemory(pBuffer);
		return PBPS_NO_ALLOWED_CIPHERS;
	}
	// get name of candidate file
	(void) fnPBPSGetCertificateListFileName(candFileNameBuff, sizeof(candFileNameBuff), CLNT_CANDIDATE);
    fileStatus = stat( candFileNameBuff, &fileInfo );
    if (fileStatus == POSIX_SUCCESS)
    {// file exists; it should not, so delete it and continue
		fnCheckFileSysCorruption( unlink(candFileNameBuff) );
        OSWakeAfter(0); // relinquish control to OS so it can properly update file system
    }
    else 
        fnCheckFileSysCorruption(fileStatus);

    pCertFileHandle = fopen(candFileNameBuff,"wb");
    if (pCertFileHandle == NULL)
    {
        /* Couldn't open the file. */
        fnCheckFileSysCorruption(POSIX_ERROR);
	    (void) OSReleaseMemory(pBuffer);
    	return PBPS_CERT_LIST_FILE_OPEN_ERROR;
    }

    fileStatus = fseek(pCertFileHandle, 0, SEEK_SET);
    if (fileStatus == POSIX_ERROR)
    {
        fnCheckFileSysCorruption(POSIX_ERROR);
	    (void) OSReleaseMemory(pBuffer);
        fnCheckFileSysCorruption(fclose(pCertFileHandle));
        return PBPS_CERT_LIST_FILE_SEEK_ERROR;
    }

    if (fwrite((void*)pBuffer, 1, datalen, pCertFileHandle) != datalen)
    {
        fnCheckFileSysCorruption(POSIX_ERROR);
	    (void) OSReleaseMemory(pBuffer);
        fnCheckFileSysCorruption(fclose(pCertFileHandle));
        return PBPS_CERT_LIST_FILE_WRITE_ERROR;
    }

    (void) OSReleaseMemory(pBuffer);
    fileStatus = fclose(pCertFileHandle);
    if (fileStatus == POSIX_ERROR)
    {
        fnCheckFileSysCorruption(fileStatus);
        return PBPS_CERT_LIST_FILE_CLOSE_ERROR;
    }

//TODO - Determine if downloaded file should be kept; this will depend on whether signatures are desired and
    //whether it is the only file with the signature;  maybe cleanup of old files can be done on startup
    dbgTrace(DBG_LVL_INFO, "Downloaded root cert list placed in candidate file %s ", candFileNameBuff);
    return fnPBPSInstallCertificateFile(candFileNameBuff);

}

/**********************************************************
Function: fnPBPSSaveCertificatesToFile
Author: Derek DeGennaro
Description:
This function takes a buffer and saves its data into a temporary
root certificate list file.  This function verifies the
signature of the file.  If the file is corrupted then it
is deleted.
Parameters:
bFileNameBuff - buffer for the name of the file
iNewVersion - version of new list
pBuffer - Buffer containing the data.
Return Value:
PBPS_OK if successfull, otherwise one of the 
error codes defined in pbptask.h.
**********************************************************/ 
static int fnPBPSSaveCertificatesToFile(char *bFileNameBuff, size_t filebuflen, int iNewVersion, long *psigOffset, unsigned char *pBuffer, unsigned long lwBufferLength)
{
    int iStatus = PBPS_OK, iCurrentCertVersion = 0;
	struct stat fileInfo;
	int fileStatus;
    FILE *pCertFileHandle = NULL;
    VERIFY_FILESIG_STATUS  verifyStat;

    if (bFileNameBuff != NULL && pBuffer != NULL)
    {
        /* set up file name. */
    	(void) fnPBPSGetCertificateListFileName(bFileNameBuff, filebuflen, CLNT_DOWNLOADED);
        
        /* get the certificate list version from the SSL session currently in use. */
        iCurrentCertVersion = fnPBPSGetCurrentCertificateListVersion();

        /* only save the certificates if there is data
        	and the version is new */
        if (lwBufferLength > 0 &&
            iNewVersion > iCurrentCertVersion)
        {                   
            fileStatus = stat( bFileNameBuff, &fileInfo );
            if (fileStatus == POSIX_ERROR)
            {// assume file does not exist, which is expected
                fnCheckFileSysCorruption(fileStatus);
            	errno = ENOERR; //clear errno
                pCertFileHandle = fopen(bFileNameBuff,"wb");
            }

            if (pCertFileHandle != NULL)
            {
                if (fseek(pCertFileHandle,0,SEEK_SET) == 0)
                {
                    if (fwrite((void*)pBuffer, 1, lwBufferLength, pCertFileHandle) == lwBufferLength)
                        iStatus = PBPS_OK;  /* The data was written. */
                    else
                        iStatus = PBPS_CERT_LIST_FILE_WRITE_ERROR;  /* Couldn't write the data to the file. */
                }
                else
                {
                    iStatus = PBPS_CERT_LIST_FILE_SEEK_ERROR;   /* Couldn't go to the beginning of the file. */
                }

                if (iStatus!= PBPS_OK)
                    fnCheckFileSysCorruption(POSIX_ERROR);

                fnCheckFileSysCorruption(fclose(pCertFileHandle));
                dbgTrace(DBG_LVL_INFO, "Downloaded root cert list to file %s ", bFileNameBuff);
                OSWakeAfter(0); // relinquish control to OS so it can properly update file system
            }
            else
            {
                fnCheckFileSysCorruption(POSIX_ERROR);
                /* Either file exists or could not create file */
                iStatus = PBPS_CERT_LIST_FILE_OPEN_ERROR;
            }       
            if (iStatus == PBPS_OK)
            {
            	*psigOffset = 0;
                if(fnCheckVaultAllowsVerifyHashSignature() == FALSE)
                {
                    /* Couldn't verify the file. */
                    iStatus = PBPS_SIG_VERIFY_UNAVAILABLE;
					fnCheckFileSysCorruption(unlink(bFileNameBuff));
                }
                else
				{
//TODO - If signing is desired, then also need to confirm whether signature verification should be done every time file is read
                	verifyStat = fnVerifyFileSignature(bFileNameBuff, psigOffset, NULL);
                	if (verifyStat == VFS_SUCCESS)
	                {
	                    dbgTrace(DBG_LVL_INFO, "Downloaded root cert list signature verified");
	                }
                	else if (verifyStat == VFS_FILE_NOT_SIGNED)
                	{
//TODO - Determine whether signing is desired or not
//If signing is desired, then replace logging with commented error handling below

                		// Signature not required so continue if not present
	                    dbgTrace(DBG_LVL_INFO, "Downloaded root cert list not signed, continuing");
//    					iStatus = PBPS_CERT_LIST_FILE_NOT_SIGNED;  // use only if signature required
//    					(void) unlink(bFileNameBuff); // use only if signature required
                	}
                	else
                	{
    					// Failed verification - delete the file
    					iStatus = PBPS_CERT_LIST_FILE_VERIFY_ERROR;
    					fnCheckFileSysCorruption(unlink(bFileNameBuff));
                	}
				}
            }

        }
        else
        {
            iStatus = PBPS_CERT_LIST_DOWNLOAD_VERSION_OLD;
        }
    }
    else
    {
        /* Invalid parameters. */
        iStatus = PBPS_PROCEDURE_ERR;
    }
    
    return iStatus;
}

/**********************************************************
Function: fnPBPSCreateSSLConnection
Author: Derek DeGennaro
Description:
This function takes a socket descriptor and uses it to
create an SSL connection to the data center.  If this
function succeeds the UIC is securely connected to the
data center and can begin to exchange secure messages
using HTTP over SSL.
Parameters:
pSocket - Descriptor of the socket used to communicate
with the data center.
Return Value:
PBPS_OK if successfull, PBPS_UNABLE_CONNECT if unsuccessfull.
**********************************************************/
static int fnPBPSCreateSSLConnection(SOCKET_DESCRIPTOR *pSocket)
{
    int iStatus = PBPS_UNABLE_CONNECT;

    fnDumpStringToSystemLog( "fnPBPSCreateSSLConnection" );
    
    if (pSocket->iSocketID >= 0)
    {
        /* The socket has been connected and the UIC is using HTTPS - SSL. Set up the SSL connection. */
		// set up the SSL Context
    	pSocket->sslDesc = fnCreateSSLContext(&(pSocket->urlInfo));
		if (pSocket->sslDesc != NULL)
		{
			if (fnSSLConnect(pSocket))
			{
				iStatus = PBPS_OK;
			}
        }
    }
    else
    {
        iStatus = PBPS_PROCEDURE_ERR;
        dbgMsg("PbpE#", iStatus, __LINE__);
    }
    
    return iStatus;
}


/**********************************************************
Function: fnPBPSDestroySSLConnection
Author: Derek DeGennaro
Description:
This function releases any SSL resources after the UIC 
disconnects from the data center.
Parameters:
pSocket - The descriptor of the socket that was used to
communicate with the data center.
Return Value: 
**********************************************************/
static void fnPBPSDestroySSLConnection(SOCKET_DESCRIPTOR *pSocket)
{   
    if (pSocket == NULL)
        return; 
        
    fnDumpStringToSystemLog( "fnPBPSDestroySSLConnection" );
    
    if (pSocket->sslConnection != NULL)
    {
        // SSL_free will free the connection, lio's,  and the context.
        SSL_free(pSocket->sslConnection);
    }
    else
    {
        // Free the lio's.
        if (pSocket->sslWriteBIO != NULL)
            OPENSSL_free(pSocket->sslWriteBIO);
                
        if (pSocket->sslReadBIO != NULL)
            OPENSSL_free(pSocket->sslReadBIO);
    }
    
    pSocket->sslConnection = NULL;
    pSocket->sslWriteBIO = NULL;
    pSocket->sslReadBIO = NULL;
    pSocket->sslConnectStatus = 0;

	if (pSocket->sslDesc != NULL)
	{
		(void) fnDestroySSLContext(pSocket->sslDesc);
	}

}

/**********************************************************
Function: fnPBPSSocketSend
Author: Derek DeGennaro
Description:
This function sends a series of bytes to the Data Center.
This function is a wrapper for the underlying network layer.
If the UIC is using normall HTTP this function calls the
network library socket API to send the data.  If the 
UIC is using HTTP over SSL this function calls the SSL
API to send the data.

Parameters:
pSocket - The descriptor of the Socket being used to communicate
with the Data Center.
pBuff - The buffer containig the data to be sent to the 
data center.
iLength - The number of bytes in pBuff.
Return Value:
The number of bytes sent.  If there is an error then this
will not equal iLength.
**********************************************************/
//TODO - determine if static or not
int fnPBPSSocketSend(SOCKET_DESCRIPTOR *pSocket, char *pBuff, int iLength)
{
    int iNucStatus, iSent = 0, iLen;

    if (pSocket == NULL || pBuff == NULL || iLength == 0)
	{
	    fnDumpStringToSystemLog( "PbP: Bad Socket Send Call" );
        return 0;
	}

    while (iSent < iLength)
    {
        if (pSocket->fUseSSL == FALSE)
            iNucStatus = NU_Send(pSocket->iSocketID,pBuff + iSent,(unsigned short)(iLength - iSent),0);
        else
            iNucStatus = SSL_write(pSocket->sslConnection, (char*)(pBuff + iSent), iLength - iSent);

        if (iNucStatus > 0)
        {
            (void)memset(pbpLog, 0, sizeof(pbpLog));
            strncpy(pbpLog, (char*)(pBuff+iSent), iNucStatus );
    		dbgTrace(DBG_LVL_INFO,"PBP SEND: Total: %d Count: %d DATA: %s", iLength, iNucStatus, pbpLog);
        }
        else
        {
    		dbgTrace(DBG_LVL_INFO,"PBP SEND: Total: %d Error = %d", iLength, iNucStatus);
        }

        if (iNucStatus > 0)
            iSent += iNucStatus;
        else
            return iSent;
    }

    return iSent;
}

/**********************************************************
Function: fnPBPSSocketReceive
Author: Derek DeGennaro
Description:
This function gets a series of bytes from the Data Center.
This function is a wrapper for the underlying network layer.
If the UIC is using normall HTTP this function calls the
network library socket API to send the data.  If the 
UIC is using HTTP over SSL this function calls the SSL
API to send the data.
Parameters:
pSocket - The descriptor of the Socket being used to communicate
with the Data Center.
pBuff - The buffer to hold the incoming data
iLength - The size (in bytes) of pBuff.
Return Value:
The number of bytes received (zero or greater is successfull
less than zero is an error).
**********************************************************/
//TODO - determine if static or not
int fnPBPSSocketReceive(SOCKET_DESCRIPTOR *pSocket, char *pBuff, int iLength)
{
    int iCount, iLen;   

    if (pSocket == NULL || pBuff == NULL || iLength == 0)
	{
	    fnDumpStringToSystemLog( "PbP: Bad Socket Receive Call" );
        return 0;
	}

	// Prepare to receive
    (void)memset( pBuff, 0, (unsigned int)iLength );
        
    if (pSocket->fUseSSL == FALSE)
        iCount = NU_Recv(pSocket->iSocketID, pBuff, (unsigned short)iLength,0);
    else
    	iCount = fnSSLRead(pSocket, pBuff, iLength);

    if (iCount > 0)
    {
		dbgTrace(DBG_LVL_INFO,"PBP GOT: Count: %d DATA: %s", iCount, pBuff);
    }
    else if (iCount == 0)
    {
		dbgTrace(DBG_LVL_INFO,"PBP GOT: no data - wait");
    }
    else
    {
		dbgTrace(DBG_LVL_INFO,"PBP GOT: Recv Error = %d", iCount);
    }

    return iCount;
}

/************************************
    XML parser and formatter 
    interface functions.
************************************/

/**********************************************************
Function: fnPBPS_ParserStart
Author: Derek DeGennaro
Description:
This function is called by the XML parser before it starts
parsing an XML message sent by the data center.  It 
initializes data used in the parsing routines.

Parameters:

Return Value:
XML_STATUS_OK if successfull, otherwise an error

**********************************************************/
short fnPBPS_ParserStart()
{
    bMessageTag = 0;
    if (bPBPSState == PBPS_RCVREQ)
    {
        bPBPSDataID = PBPS_NODATAID;
    }

    #if 0 //TODO JAH Remove dcapi.c
/*
 * Init some data capture stuff - TDT
 */
    m_wDCAPDnlCharDataBuffLen = 0;
    m_wDCAPDnlBktNameBuffLen = 0;
    m_wDCAPDnlBktNumBuffLen = 0;
    *m_bDCAPDnlBktNameBuff = 0;
    *m_bDCAPDnlBktNumBuff = 0;
    m_DCAPCurrBktNumContext = NODCPBKTNUM_CONTEXT;
    m_DCAPCurrWbnNumContext = NO_WBNNUM_CONTEXT;
#endif //TODO JAH Remove dcapi.c
    pPBPSCertBase64Buff = NULL;
    iPBPSCertBase64BuffLen = 0; 

    return XML_STATUS_OK;
}
/**********************************************************
Function: fnPBPS_ParserEnd
Author: Derek DeGennaro
Description:
This function is called by the XML parser after it has
finished parsing an XML message sent by the data center.

Parameters:

Return Value:
XML_STATUS_OK if successfull, otherise an error.
**********************************************************/
short fnPBPS_ParserEnd()
{
    /* Release the buffer */
    if (pPBPSCertBase64Buff != NULL)
    {
        free(pPBPSCertBase64Buff);
        pPBPSCertBase64Buff = NULL;
    }
    iPBPSCertBase64BuffLen = 0;

    return XML_STATUS_OK;
}
/**********************************************************
Function: fnPBPS_StartElement
Author: Derek DeGennaro
Description:
This function is called by the XML parser when it finds a start
tag of an element that is not in its lookup table in an XML message 
from the data center.  (This is the "default" start element
callback function for the parser.)

Parameters:
pName - The name of the element.
pAttrList - Array of attribute structures.
iNumAttr - The number of attributes in pAttrList.
Return Value:
XML_STATUS_OK if successfull, otherise an error.
**********************************************************/
static short fnPBPS_StartElement(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    return XML_STATUS_OK;
}
/**********************************************************
Function: fnPBPS_EndElement
Author: Derek DeGennaro
Description:
This function is called by the XML parser when it finds
the end tag of an element that is not in its lookup table 
in an XML message from the data center.  
(This is the "default" end element callback function for 
the parser.)
Parameters:
pName - The name of the element.
Return Value:
 XML_STATUS_OK if successfull, otherise an error.
**********************************************************/
static short fnPBPS_EndElement(XmlChar *pName)
{
    return XML_STATUS_OK;
}
/**********************************************************
Function: fnPBPS_ElementChars
Author: Derek DeGennaro
Description:
This function is called by the XML parser when it finds
character data inside of an element that is not in its lookup table 
in an XML message from the data center.  
(This is the "default" element character data callback function for 
the parser.) 
Parameters:
pName - The name of the element.
pChars - An array of character data.
iLen - The number of characters in pChars.
Return Value:
XML_STATUS_OK if successfull, otherise an error.
**********************************************************/
static short fnPBPS_ElementChars(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    return XML_STATUS_OK;
}
                  
/**********************************************************
Function: fnPBPS_Formatter_Start
Author: Derek DeGennaro
Description:
This function is called by the XML Formatter before it
starts generating an XML message that is sent to the
data center.  It initializes data that is used by
the Formatter functions.

Parameters:

Return Value:
XML_STATUS_OK if successfull, otherise an error.
**********************************************************/
static short fnPBPS_Formatter_Start(void)
{
    return XML_STATUS_OK;
}
/**********************************************************
Function: fnPBPS_Formatter_Char
Author: Derek DeGennaro
Description:
This function is called by the XML Formatter each time it
generates an character that is part of an XML message that
is sent to the Data Center.  It inserts the character in
an output buffer.  If the buffer is full the buffer is
wrapped up in an HTTP chunk and sent to the Data Center.

Parameters:
x - The next character in the XML message being sent to the 
data center.
Return Value:
XML_STATUS_OK if successfull, otherwise an error.
**********************************************************/
static short fnPBPS_Formatter_Char(XmlChar x)
{
    short sResult = XML_STATUS_OK;

    /* if we haved filled the chunk buffer,
    send the chunk. */
    if (ulFormatterBuffLen == PBPS_XML_SEND_BUFF_LEN)
    {
        sResult = fnPBPSSendHttpChunk(&pbpSocket,(char*)pFormatterBuff,ulFormatterBuffLen);
        /* reset the buffer */
        *pFormatterBuff = 0;
        ulFormatterBuffLen = 0;
    }

    /* add the character to the end of the buffer */
    *((XmlChar*)pFormatterBuff + ulFormatterBuffLen++) = x;
    /* null terminate the buffer */
    *((XmlChar*)pFormatterBuff + ulFormatterBuffLen) = (XmlChar)NULL;

    return sResult;
}
/**********************************************************
Function: fnPBPS_Formatter_End
Author: Derek DeGennaro
Description:
This function is called by the XML Formatter after it is done
generating an XML message that is sent to the data center.
The Formatter may be done due to the successfull generation
of the message or an error during the generation.  This function
sends any remaining data in an HTTP chunk then sends the 
"empty" chunk to the data center to signal that there is no
more data to send.

Parameters:
fValid - Status of the message generation.  True if the XML
message was generated successfully, False otherwise.
Return Value:
XML_STATUS_OK if successfull, otherwise an error.
**********************************************************/
static short fnPBPS_Formatter_End(BOOL fValid)
{
    short sResult = XML_STATUS_OK;
    int iLastChunkLength;
    char cLastChunk[40];

    sprintf(cLastChunk,"0\r\n\r\n");

    if (fValid)
    {
        /* there are no formatter errors, output the last chunk */

        // until we figure out why the confirmation services upload last chunk has a
        // reported length that's 1 too big, if the current length -1 is pointing to a
        // nul-terminator, decrement the length to allow the proxy stuff to work.
        // (confirmation services gives a length that points to the byte AFTER
        //  the nul-terminator.)
        if (!pFormatterBuff[ulFormatterBuffLen-1])
            ulFormatterBuffLen--;

        /* add the "No Wei" padding */
        *((XmlChar*)pFormatterBuff + ulFormatterBuffLen++) = (XmlChar)' ';
        /* null terminate the buffer */
        *((XmlChar*)pFormatterBuff + ulFormatterBuffLen) = (XmlChar)NULL;

        /* send the chunk */
        sResult = fnPBPSSendHttpChunk(&pbpSocket,(char*)pFormatterBuff,ulFormatterBuffLen);
        
        /* if ok, send the last chunk which
        only has a size of zero */
        if (sResult == XML_STATUS_OK)
        {
            iLastChunkLength = (int)strlen(cLastChunk);
            if (fnPBPSSocketSend(&pbpSocket,cLastChunk,iLastChunkLength) != iLastChunkLength)           
                sResult = PBPS_SOCKETSEND_ERR;
        }
        
        /* set flags for uploads done */
        if (bPBPSDataID == PBPS_DCAP_UPLOAD)
        {
            fDcapUploadDone = TRUE;
//            UploadUpdateOI(DCAP_UPLOAD_SERVICE, OIT_INFRA_UPLOAD_COMPLETE, 0, 0);
        }

    }
    else
        sResult = PBPS_XMLFORMAT_ERR;

    return sResult;
}

/*********************************
    Error Handling
*********************************/

/**********************************************************
Function: fnPBPSLogError
Author: Derek DeGennaro
Description:
This function records any errors that are reported to it.
Parameters:
iErrorCode - The error that is being reported. Most errors
are defined in pbptask.h, xmltypes.h, and MM97023
Return Value:

**********************************************************/
void fnPBPSLogError(int iErrorCode)
{
    if (iErrorCode >= XML_USER_ERROR_BASE && iErrorCode < PBPS_LASTERROR)
    {
        /* Normal PBP task level errors. */
        fnLogError(RM_SIMPLE_CLEARING,ERROR_CLASS_PBP,(unsigned char)(iErrorCode - XML_USER_ERROR_BASE));
        fnLogSystemError(ERROR_CLASS_PBP,(unsigned char)(iErrorCode - XML_USER_ERROR_BASE),0,0,0);      

        // set the Display error bytes so PCMC will know what the problem is.       
        ucOIErrorClass = ERROR_CLASS_PBP;
        ucOIErrorCode = (unsigned char)(iErrorCode - XML_USER_ERROR_BASE);
    }
    else if (iErrorCode > 0 && iErrorCode < XML_USER_ERROR_BASE)
    {
        /* An XML Parser or Formatter error. */
        fnLogError(RM_SIMPLE_CLEARING,ERROR_CLASS_PBP,(unsigned char)(PBPS_XML_GENERAL_ERR - XML_USER_ERROR_BASE));
        fnLogSystemError(ERROR_CLASS_PBP,
                        (unsigned char)(PBPS_XML_GENERAL_ERR - XML_USER_ERROR_BASE),
                        (unsigned char)((iErrorCode & 0x0000FF00) >> 8),
                        (unsigned char)(iErrorCode & 0x000000FF),
                        0);             

        // set the Display error bytes so PCMC will know what the problem is.       
        ucOIErrorClass = ERROR_CLASS_PBP;
        ucOIErrorCode = (unsigned char)(PBPS_XML_GENERAL_ERR - XML_USER_ERROR_BASE);
    }
}

/**********************************************************
Function:
Author:
Description:

Parameters:

Return Value:

**********************************************************/
void fnPBPS_ErrContinueRsp(int iErr, unsigned char bDataID, unsigned char bNewState, unsigned char bNewDataID, unsigned char bReserved)
{
    /* Change state to Send Response. */
    bPBPSState = PBPS_SENDRSP;
    /* Call the send message function. */
    (void)fnPBPSSendMsg(bDataID);
    /* Reset the error status since we are gonna continue. */
    lwErrorStatus = PBPS_OK;
    /* Continue like nothing happened. */
    (void)fnPBPSGotoState(bNewState,bNewDataID);
}
static void fnPBPS_ErrContinueSingleRsp(int iErr, unsigned char bDataID, unsigned char bReserved2, unsigned char bReserved3, unsigned char bReserved4)
{
    /* Change state to Send Response. */
    bPBPSState = PBPS_SENDRSP;
    /* Call the send message function. */
    (void)fnPBPSSendMsg(bDataID);
    /* End Session, goto idle state. */
    (void)fnPBPSGotoState(PBPS_IDLE,PBPS_NODATAID);
}

/**********************************************************
Function:
Author:
Description:

Parameters:

Return Value:

**********************************************************/
static void fnPBPS_ErrEndDialog(int iErr, unsigned char bReserved1, unsigned char bReserved2, unsigned char bReserved3, unsigned char bReserved4)
{
    bPBPSState = PBPS_SENDREQ;
    (void)fnPBPSSendMsg(PBPS_SENDSIGNOFF_REQ);
    //iReturnValue = fnPBPSGotoState(PBPS_RCVRSP,PBPS_DCSIGNOFF_RSP);
    (void)fnPBPSReceiveMsg(); /* get response */
    (void)fnPBPSGotoState(PBPS_IDLE,PBPS_NODATAID);
}
/**********************************************************
Function:
Author:
Description:

Parameters:

Return Value:

**********************************************************/
static void fnPBPS_ErrGotoIdle(int iErr, unsigned char bReserved1, unsigned char bReserved2, unsigned char bReserved3, unsigned char bReserved4)
{
    // just go to idle
    (void)fnPBPSGotoState(PBPS_IDLE,PBPS_NODATAID);
}
/**********************************************************
Function:
Author:
Description:

Parameters:

Return Value:

**********************************************************/
static void fnPBPS_ErrSignOffClose(int iErr, unsigned char bReserved1, unsigned char bReserved2, unsigned char bReserved3, unsigned char bReserved4)
{
    // Send SignOff Request and close connection
    if (pbpSocket.iSocketID >= 0)
    {
        bPBPSState = PBPS_SENDREQ;
        (void)fnPBPSSendMsg(PBPS_SENDSIGNOFF_REQ);
        //iReturnValue = fnPBPSGotoState(PBPS_RCVRSP,PBPS_DCSIGNOFF_RSP);
        (void)fnPBPSReceiveMsg(); // get the response
    }
    (void)fnPBPSGotoState(PBPS_IDLE,PBPS_NODATAID);
}

/*******************************
    Http parser
*******************************/
/* Ascii Carriage Return and Line feed characters */
#define CR 0x0D
#define LF 0x0A

/**********************************************************
Function:
Author:
Description:

Parameters:

Return Value:

**********************************************************/
static void fnPBPS_InitHttpParserData(void)
{
    fHttpChunked = FALSE;
    fHttpContentLen = FALSE;
    ulHttpBodyLen = 0;
    ulHttpBodyLeft = 0;
    *cHttpLineBuff = 0;
    ulHttpCurrLine = 1;
    fHttpCloseConnection = FALSE; /* default for HTTP 1.1 */
    bHttpState = HEADER_S1;
    fHttpContinueRspFound = FALSE;
    iHttpStatusCode = 0;
    fHttpBadStatus = FALSE;
    fHttpIsBodyChar = FALSE;
    ulFormatterBuffLen = 0;
}
/**********************************************************
Function:
Author:
Description:

Parameters:

Return Value:

**********************************************************/
static int fnPBPS_SendHttpParserChar(char x)
{
    int iStatus = PBPS_OK, iLength;
    char cTemp[2] = "", *pTemp, *pToken, cLowerCase;

    *cTemp = x;
    *(cTemp + 1) = 0;
    fHttpIsBodyChar = FALSE;

    iLength = (int)strlen(cHttpLineBuff);
    if (iLength == HTTP_HEADER_LINE_LEN)
        /* The line has grown too big for the buffer or there should be no more HTTP */
        return PBPS_HTTP_PARSE_ERR;

    switch (bHttpState)
	{
    case HEADER_S1: /* we are accumulating chars for the header line */
        strcat(cHttpLineBuff,cTemp);
        if (x == CR)
            bHttpState = HEADER_S2; /* found CR look for LF */
        break;
		
    case HEADER_S2: /* we have found the end of line and r looking for a LF */
        strcat(cHttpLineBuff,cTemp);
        if (x == LF)
        {
            /* Examine the Status Line */
            if (ulHttpCurrLine == 1)
            {
                /* make sure it is HTTP/1.1 */
                if (strncmp(cHttpLineBuff,"HTTP/1.1",8) != 0)
                    return PBPS_HTTP_PARSE_ERR; /* status line MUST have this in it */
					
                /* Check the status */
                pToken = cHttpLineBuff + 8;
                iHttpStatusCode = atoi(pToken);
                if (iHttpStatusCode == 100)
                    /* We have a continue status.  In this case, 
                    we finish reading this response, continue 
                    sending the body, and then wait for the either
                    another continue response or the final response. */
                    fHttpContinueRspFound = TRUE;
                else if (iHttpStatusCode >= 200 && iHttpStatusCode < 300)
                    /* We have the final response and it is good */
                    fHttpBadStatus = FALSE;
                else
                    /* We have the final response and it is bad */
                    fHttpBadStatus = TRUE;
            }
			
            /* this begins some pbp specific stuff */
            /* the LF has been found for the header line, now process it */
            pToken = strchr(cHttpLineBuff,':');
            if (pToken)
            {
                /* change all Http headers to uppercase */
                pTemp = cHttpLineBuff;
                while (pTemp < pToken)
                {
                    cLowerCase = *pTemp;
                    *pTemp = (char)toupper((int)cLowerCase);
                    pTemp++;
                }
            }

            if (strncmp(cHttpLineBuff,"CONTENT-LENGTH:",15) == 0)
            {
                /* The body is non-chunked.  The entire body-length is specified in this line. */
                fHttpContentLen = TRUE;
                pTemp = cHttpLineBuff + 15;
                ulHttpBodyLen = (unsigned long)atoi(pTemp);
                ulHttpBodyLeft = ulHttpBodyLen;
            }
			
            if (strncmp(cHttpLineBuff,"TRANSFER-ENCODING:",18) == 0)
            {
                /* The body is chunked. Each chunk is preceded by the length of the chunk.
                The last chunk is preceded by a zero for its length. */
                if (findString("chunked", cHttpLineBuff))
                    fHttpChunked = TRUE;
            }
			
            if (strncmp(cHttpLineBuff,"CONNECTION:",11) == 0)
            {
                /* for HTTP 1.1, connections are persistent by
                default.  Thus, the only time the connection 
                is closed by the sever is when the client sends
                a request with a "Connection: close" header. */
                if (findString("close", cHttpLineBuff) ||
                    findString("Close", cHttpLineBuff))
                {
                    fHttpCloseConnection = TRUE;
                }
            }

            if (strncmp(cHttpLineBuff,"TRAILER:", 8) == 0)
            {
                /* signal the trailer header is found */
            }

            *cHttpLineBuff = 0;
            bHttpState = HEADER_S3;
            ulHttpCurrLine += 1;
        }
        else
            /* this is an error cause the LF should follow the CR */
            iStatus = PBPS_HTTP_PARSE_ERR;
        break;
		
    case HEADER_S3: /* we have found a CR LF now decide what to do */
        strcat(cHttpLineBuff,cTemp);
        if (x == CR)
            bHttpState = HEADER_S4; /* this is a blank line, LF should be next */
        else
            bHttpState = HEADER_S1; /* this starts another header line */
        break;
		
    case HEADER_S4: /* we havd found a CR LF CR and are looking for a LF */
        strcat(cHttpLineBuff,cTemp);
        if (x == LF)
        {
            /* we have found the CR LF CR LF and next comes the (optional) body */
            if (fHttpContentLen)
            {
                /* The Content-Length header gave us the length of the body */
                bHttpState = BODY_S1;
            }
            else if (fHttpChunked)
            {
                /* The body is chunked, now we haved to parse the length of the chunk. */
                ulHttpBodyLen = 0;
                *cHttpLineBuff = 0;
                bHttpState = CHUNKLEN_S1;
            }
            else if (ulHttpBodyLen == 0)
                /* there is no body, we are done */
                bHttpState = HTTP_DONE;
            ulHttpCurrLine += 1;
        }
        else
            /* this is an error cause the LF should come after the CR */
            iStatus = PBPS_HTTP_PARSE_ERR;
        break;
		
    case CHUNKLEN_S1:
        if (x == CR)
            /* the end of the line is found, now we look for a LF */
            bHttpState = CHUNKLEN_S2;
        else if (isxdigit(x))
        {
            /* append the hex digit */
            strcat(cHttpLineBuff,cTemp);
            bHttpState = CHUNKLEN_S1;
        }
        else if (x == ';')
            /* there is a chunk extension */
            bHttpState = CHUNKLEN_S3; 
        else
            /* this is an error cause the chunk 
            length should only include hex digits 
            and an optional extension */
            iStatus = PBPS_HTTP_PARSE_ERR;
        break;
		
    case CHUNKLEN_S2:
        if (x == LF)
        {
            ulHttpBodyLen = 0;
            /* convert line to number and set ulHttpBodyLen */
            for (pTemp = cHttpLineBuff; *pTemp != 0 && !isxdigit(*pTemp); pTemp++) ;
            /* pTemp points to first hex digit or null */
            sscanf(pTemp,"%lx",&ulHttpBodyLen);
            if (ulHttpBodyLen == 0)
                /* the zero length chunk is the last chunk */
                bHttpState = HTTP_DONE;
            else
            {
                /* go get the rest of the bytes */
                ulHttpBodyLeft = ulHttpBodyLen;
                bHttpState = CHUNK_S1;
            }
        }
        else
            /* this is an error cause the LF should follow the CR */
            iStatus = PBPS_HTTP_PARSE_ERR;
        break;
		
    case CHUNKLEN_S3: /* we are processing the chunk extension */
        if (x == CR)
            /* the end of the line has been found, look for LF */
            bHttpState = CHUNKLEN_S2;
        else
            /* this is part of the chunk extension, keep looking for CR */
            bHttpState = CHUNKLEN_S3;
        break;
		
    case CHUNK_S1: /* we are processing the chunk itself */
        if (ulHttpBodyLeft == 0)
        {
            /* there are no more chars in chunk.
            a CR MUST be next */
            if (x == CR)
                // we want go get the next chunk len
                bHttpState = CHUNK_S2;
            else
                /* This is an error cause the chunk length 
                said there are no more characters. */
                iStatus = PBPS_HTTP_PARSE_ERR;
        }
        else
        {
            // send char to xml parser
            ulHttpBodyLeft -= 1;
            fHttpIsBodyChar = TRUE;
        }
        break;
		
    case CHUNK_S2: /* we have found a CR after the last char in chunk, LF should be next */
        if (x == LF)
        {
            /* start gathering hex digits for chunk length */
            *cHttpLineBuff = 0;
            bHttpState = CHUNKLEN_S1;
        }
        else
            /* this is an error cause the LF should follow the CR */
            iStatus = PBPS_HTTP_PARSE_ERR;
        break;
		
    case BODY_S1: /* we are processing the body of the message */
        if (ulHttpBodyLeft > 0)
        {
            fHttpIsBodyChar = TRUE;
            if (--ulHttpBodyLeft == 0)
                bHttpState = HTTP_DONE;
        }
        else
        {
            fHttpIsBodyChar = FALSE;
            bHttpState = HTTP_DONE;
        }
        break;
		
    case HTTP_DONE:
        /* we ignore any chars found after the body */
        /* maybe we should put in something to accept
        trailers to make this truly compliant */
        iStatus = PBPS_OK;
        break;
		
    default:
        iStatus = PBPS_HTTP_PARSE_ERR;
        break;
    }

    return iStatus;
}

/**********************************
    Count functions for the
    formatter scripts.
***********************************/
unsigned long fnPBPS_SendBasePcnInSignOn(unsigned long lCounter)
{
	if (fnDCAPIsDataCaptureActive() == TRUE)
    return 1;
    else
        return 0;
}

unsigned long fnPBPS_SendRootCertLstVerInSignOn(unsigned long lCounter)
{
    // TODO: figure out what data capture has to do with this
    return 1;
}

unsigned long fnSendEffectiveDate(unsigned long lCounter)
{
    
/*
    if (fnRateGet_RR_SynchType())
    {
        return(1);
    }
    else
    {
*/
        return(0);
//    }
}

unsigned long fnPBPS_CheckPSDStatusOk(unsigned long lCounter)
{
    if (ulPSDLastStatus == 0)
        return 1;
    else
        return 0;
}

unsigned long fnPBPS_SendRecordInLSKRsp(unsigned long lCounter)
{
    /* Send the response record if it is present.  A record
    is present if the PSD response contained a success status (zero)
    AND the response contained more than the 4-byte status.
    */
    if (ulPSDLastStatus == 0 && psdTransferControl.bytesWrittenToDest[0] > 4)
        return 1;
    else
        return 0;
}

unsigned long fnPBPS_SendPSDStatusInLSKRsp(unsigned long lCounter)
{
    /* Send the status if there was an error, or the response
    record could not be generated. */
    if (ulPSDLastStatus != 0 || psdTransferControl.bytesWrittenToDest[0] <= 4)
        return 1;
    else
        return 0;
}

unsigned long fnPBPSSendIButtonPieceCountInAudit(unsigned long lReserved)
{
    if( fnGetCurrentPsdType() & PSDT_ALL_IBUTTON )
        return 1;
    else    
        return 0;
    
}

unsigned long fnPBPS_CheckIfGermanyFFRecRequiredInRefill(unsigned long lCounter)
{
    unsigned char bFFReq = 0;
    
    bFFReq = fnFlashGetByteParm(BP_PBP_PERFORM_FINALIZED_FRANKING);
    
    if (bFFReq)
        return 1;
    else
        return 0;   
}

unsigned long fnPBPS_CheckIfGermanyFFRecRequiredInRefund(unsigned long lCounter)
{
    unsigned char bFFReq = 0;
    
    bFFReq = fnFlashGetByteParm(BP_PBP_PERFORM_FINALIZED_FRANKING);
    
    if( bFFReq &&  !(fnGetCurrentPsdType() & PSDT_ALL_IBUTTON) )
        return 1;
    else
        return 0;   
}

unsigned long fnPBPS_CheckIfGermanyFFRecRequiredInAudit(unsigned long lCounter)
{
    if (fSendGermanFFRecInAudit)
        return 1;
    else
        return 0;
}

unsigned long fnPBPS_SendCommType( unsigned long lCounter )
{
	return 1;
}

unsigned long fnPBPS_SendUkEibEnabled( unsigned long lCounter )
{
     
	if (fnFlashGetIBByteParm(IBP_COUNTRY_ID) == ISO_UK)
		return(1);
	else

		return(0);
}


/**********************************
    The "Get Data" function for
    the PBP task formatter.
**********************************/
extern char *gp_tr_xml;

extern int32_t g_xml_len;
void *fnPBPS_GetData (unsigned long ulDataID,
                    unsigned long ulDataItemNo,
                    unsigned char bFormatType, 
                    unsigned short *bsize)
{
    unsigned char euroConverted,euroCapable,euroStatus,bMoneyDecimals=0;
    unsigned long sResultLen;
    unsigned long ulTempStatus;
    unsigned char bPsdSwVersion[4];
    unsigned char bPCNLength = 0;
    DATETIME currDateTime;
    uchar ucDeviceType;
    unsigned long ulPsdPieceCount = 0;


    *bsize = 0;
//	pPBPTemp[0] = 0;	// can't do this here because this function is sometimes first
                        // called to get the data (which is put in pPBPTemp), then called
						// to get the attribute string (which is in a different buffer).
						// the second call kills the data in pPBPTemp.
    switch (ulDataID)
	{
    case CUSTOMER_ACCT_SIGNON:
        memset(pPBPTemp,0,sizeof(CMOSSetupParams.lwPBPAcctNumber)*2 + 1);

        if(fnFlashGetByteParm(BP_FORCE_PBPACCT_NUMBER_DOWNLOAD)) {
            *bsize = (unsigned short)sprintf(pPBPTemp,"00000000");
        } else {
            fnPackedBcdToAsciiBcd((char*)pPBPTemp,(const unsigned char *)&CMOSSetupParams.lwPBPAcctNumber,sizeof(CMOSSetupParams.lwPBPAcctNumber));     
            *bsize = (unsigned short)strlen(pPBPTemp);
        }

        return pPBPTemp;

    case CUSTOMER_ACCT:
        memset(pPBPTemp,0,sizeof(CMOSSetupParams.lwPBPAcctNumber)*2 + 1);
        fnPackedBcdToAsciiBcd((char*)pPBPTemp,(const unsigned char *)&CMOSSetupParams.lwPBPAcctNumber,sizeof(CMOSSetupParams.lwPBPAcctNumber));     

        *bsize = (unsigned short)strlen(pPBPTemp);
        return pPBPTemp;
		
    case PBP_SERIAL_NUMBER:     
        /* call bob to get the meter serial number */   
        memset((void*)pPBPTemp,0,sizeof(pPBPTemp));
        if (fnValidData(BOBAMAT0, VLT_INDI_FILE_PBP_SERIAL, (void *) pPBPTemp) != BOB_OK)
            *bsize = (unsigned short)sprintf(pPBPTemp," ");
        else
            *bsize = (unsigned short)strlen(pPBPTemp);
			
        return pPBPTemp;
		
    case DEVICE_TYPE_ID:
        if (fnDCAPIsDataCaptureActive() == TRUE)
        {
        if(fnFlashGetByteParm(BP_ACCEPT_PBPACCT_NUMBER_DOWNLOAD))
            ucDeviceType = trmGetDcapDeviceID();//PSD_SIGN_ON_DEVICE_TYPE_ID[fnGetPsdTypeIndex()][3];
        else
            ucDeviceType = PSD_SIGN_ON_DEVICE_TYPE_ID[fnGetPsdTypeIndex()][1];
        }
        else
        {
            if(fnFlashGetByteParm(BP_ACCEPT_PBPACCT_NUMBER_DOWNLOAD))
                ucDeviceType = PSD_SIGN_ON_DEVICE_TYPE_ID[fnGetPsdTypeIndex()][2];              
            else
                ucDeviceType = PSD_SIGN_ON_DEVICE_TYPE_ID[fnGetPsdTypeIndex()][0];
        }
		
        *bsize = sprintf(pPBPTemp,"%d",ucDeviceType);
        return pPBPTemp;
		
    case VERSION_NUMBER:
        memset((void*)pPBPTemp,0,sizeof(pPBPTemp));
        if (fnValidData(BOBAMAT0, GET_VLT_SW_VERSION, (void *) &bPsdSwVersion[0] )== BOB_OK) 
        {
            memcpy(&ulTempStatus,&bPsdSwVersion[0],sizeof(ulTempStatus));
            *bsize = (unsigned short)sprintf(pPBPTemp,"%lX",ulTempStatus);
        }
        else
            *bsize = (unsigned short)sprintf(pPBPTemp," ");
			
        return pPBPTemp;
		
    case EUROCURRENCY_STATUS:
        if (( IsEuroConversionDone (&euroConverted)) && (IsEuroCapable (&euroCapable)))
        {
             if (euroCapable)
             {
                 if (euroConverted)
                     euroStatus = CONVERTED_EURO_METER;
                 else
                     euroStatus = CONVERTIBLE_METER;
             }
             else
                 euroStatus = NO_EURO_CONVERSION_SUPPORTED;
         }
         else
             euroStatus = NO_EURO_CONVERSION_SUPPORTED;
			 
         *bsize = (unsigned short)sprintf(pPBPTemp,"%d",euroStatus);
         return pPBPTemp;   
		 
    case SIGNON_SESSION_ID:
        *bsize = (unsigned short)sprintf(pPBPTemp,"0"); // initially zero, unless resigning on
        return pPBPTemp;
		
    case CURRENT_SESSION_ID:
        *bsize = (unsigned short)strlen(cCurrSessionID);
        return cCurrSessionID;
		
    case PSD_STATUS_INFO:
        memcpy(&ulTempStatus,&psdTransferControl.deviceReplyStatus[0],sizeof(ulTempStatus));
        ulTempStatus = EndianSwap32(ulTempStatus);
        ulTempStatus = fnNormalizePsdStatus(ulTempStatus);
        *bsize = (unsigned short)sprintf(pPBPTemp,"%lu",ulTempStatus);
        return pPBPTemp;
		
    case GENRSP_STATUS_INFO:
    case SIGNOFF_STATUS_INFO:
        *bsize = (unsigned short)sprintf(pPBPTemp, "%lu", lwErrorStatus);
        return pPBPTemp;
		
    case AUTH_RESPONSE:
        if (B64Encode((unsigned char*)psdTransferControl.ptrToCallersDestBuffer,(unsigned long)psdTransferControl.bytesWrittenToDest[0] - fnGetNumXtraPsdStatBytes(),pPBPTemp,PBP_TEMP_BUFF_LEN,&sResultLen,TRUE) == CONVERSION_OK)
            *bsize = (unsigned short)sResultLen;
        else
            *bsize = (unsigned short)sprintf(pPBPTemp," ");
			
        return pPBPTemp;
		
    case REKEY_RESPONSE_RECORD:
        /*
        For compatibility with EDM stuff, ptrToDeviceReplyData[0] points to the
        reply from the PSD.  However, this reply includes a 4-byte status.  After
        the 4 byte status is the actual record.  Also, bytesWrittenToDest[0] is the
        size of the response INCLUDING the 4-byte status.  So the actual length
        of the record is bytesWrittenToDest[0] - 4. 
        */ 
        if( fnGetCurrentPsdType() & PSDT_ALL_IBUTTON )
        {
            psdTransferControl.ptrToDeviceReplyData[0] = psdTransferControl.ptrToCallersDestBuffer;
        }
		
        if (B64Encode((unsigned char*)psdTransferControl.ptrToDeviceReplyData[0] + fnGetNumXtraPsdStatBytes(),(unsigned long)psdTransferControl.bytesWrittenToDest[0] - fnGetNumXtraPsdStatBytes(),pPBPTemp,PBP_TEMP_BUFF_LEN,&sResultLen,TRUE) == CONVERSION_OK)
            *bsize = (unsigned short)sResultLen;
        else
            *bsize = (unsigned short)sprintf(pPBPTemp," ");
			
        return pPBPTemp;
		
    case AUDIT_REQUEST_RECORD: /* the binary data is in pPSDCommonBuff, length is ulPSDBuffLength */        
        if (B64Encode((unsigned char*)psdTransferControl.ptrToCallersDestBuffer,(unsigned long)psdTransferControl.bytesWrittenToDest[0] - fnGetNumXtraPsdStatBytes(),pPBPTemp,PBP_TEMP_BUFF_LEN,&sResultLen,TRUE) == CONVERSION_OK)
            *bsize = (unsigned short)sResultLen;
        else
            *bsize = (unsigned short)sprintf(pPBPTemp," ");
			
        return pPBPTemp;
		
    case REFILL_REQUEST_RECORD: /* the binary data is in pPSDCommonBuff, length is ulPSDBuffLength */
        if (B64Encode((unsigned char*)psdTransferControl.ptrToCallersDestBuffer,(unsigned long)psdTransferControl.bytesWrittenToDest[0] - fnGetNumXtraPsdStatBytes(),pPBPTemp,PBP_TEMP_BUFF_LEN,&sResultLen,TRUE) == CONVERSION_OK)
            *bsize = (unsigned short)sResultLen;
        else
            *bsize = (unsigned short)sprintf(pPBPTemp," ");
			
        return pPBPTemp;
		
    case REFUND_REQUEST_RECORD: /* the binary data is in pPSDCommonBuff, length is ulPSDBuffLength */
        if (B64Encode((unsigned char*)psdTransferControl.ptrToCallersDestBuffer,(unsigned long)psdTransferControl.bytesWrittenToDest[0] - fnGetNumXtraPsdStatBytes(),pPBPTemp,PBP_TEMP_BUFF_LEN,&sResultLen,TRUE) == CONVERSION_OK)
            *bsize = (unsigned short)sResultLen;
        else
            *bsize = (unsigned short)sprintf(pPBPTemp," ");
			
        return pPBPTemp;
		
    case NONCE_RESPONSE: /* the binary data is in pPSDCommonBuff, length is ulPSDBuffLength */      
        /* for EDM compatability, the first 4 bytes of pPSDCommonBuff are the status.
        the next 8 are the NONCE. */
        if (B64Encode((unsigned char*)psdTransferControl.ptrToDeviceReplyData[0] + fnGetNumXtraPsdStatBytes(),(unsigned long)psdTransferControl.bytesWrittenToDest[0] - fnGetNumXtraPsdStatBytes(),pPBPTemp,PBP_TEMP_BUFF_LEN,&sResultLen,TRUE) == CONVERSION_OK)
            *bsize = (unsigned short)sResultLen;
        else
            *bsize = (unsigned short)sprintf(pPBPTemp," ");
			
        return pPBPTemp;
		
    case KEY_EXCH_KEY_RSP_RECORD:
        /* for EDM compatability, the first 4 bytes of pPSDCommonBuff are the status. */        
        if (B64Encode((unsigned char*)psdTransferControl.ptrToDeviceReplyData[0] + fnGetNumXtraPsdStatBytes(),(unsigned long)psdTransferControl.bytesWrittenToDest[0] - fnGetNumXtraPsdStatBytes(),pPBPTemp,PBP_TEMP_BUFF_LEN,&sResultLen,TRUE) == CONVERSION_OK)
            *bsize = (unsigned short)sResultLen;
        else
            *bsize = (unsigned short)sprintf(pPBPTemp," ");
			
        return pPBPTemp;
		
    case LOAD_SECRET_KEY_RSP_RECORD:
        if( fnGetCurrentPsdType() & PSDT_ALL_IBUTTON )
        {
            psdTransferControl.ptrToDeviceReplyData[0] = psdTransferControl.ptrToCallersDestBuffer;
        }
		
        /* for EDM compatability, the first 4 bytes of pPSDCommonBuff are the status. */        
        if (B64Encode((unsigned char*)psdTransferControl.ptrToDeviceReplyData[0] + fnGetNumXtraPsdStatBytes(),(unsigned long)psdTransferControl.bytesWrittenToDest[0] - fnGetNumXtraPsdStatBytes(),pPBPTemp,PBP_TEMP_BUFF_LEN,&sResultLen,TRUE) == CONVERSION_OK)
            *bsize = (unsigned short)sResultLen;
        else
            *bsize = (unsigned short)sprintf(pPBPTemp," ");
			
        return pPBPTemp;
		
    case FINALIZED_FRANKING_RECORD:
        if (B64Encode(pPSDFFRecBuff,lwPSDFFRecBuffLen,pPBPTemp,PBP_TEMP_BUFF_LEN,&sResultLen,TRUE) == CONVERSION_OK)
            *bsize = (unsigned short)sResultLen;
        else
            *bsize = (unsigned short)sprintf(pPBPTemp," ");
			
        return pPBPTemp;
		
    case FILE_XFER_STATUS:
        *bsize = (unsigned short)sprintf(pPBPTemp,"0");
        return pPBPTemp;
        
    case GET_BASE_PCN:

            bPCNLength = pfnBaseGetPCN((char*)pPBPTemp);
            if(bPCNLength)
            {
				pPBPTemp[bPCNLength] = 0;
				*bsize = (unsigned short)strlen(pPBPTemp);
				return pPBPTemp;
			}
			else
			{   //No Rules Base PCN Capability anymore
				// The rules come from the rates set
				*bsize = (unsigned short)sprintf(pPBPTemp," ");
				return pPBPTemp;
			}
		
    case GET_BASE_PCN_ATTR:     
        *bsize = (unsigned short)strlen(pPBPSBaseSignOnAttr);
        return ((void*)pPBPSBaseSignOnAttr);
		
    case CERTIFICATE_LIST_VERSION:
        *bsize = sprintf(pPBPTemp,"%d", fnPBPSGetCurrentCertificateListVersion());
        return pPBPTemp;

    case METER_DATE_TIME_ID:
        /* Get the Meter (PSD) GMT time. */
        if (GetSysDateTime(&currDateTime, ADDOFFSETS, GREGORIAN) == TRUE)
        {
            *bsize = sprintf(pPBPTemp,"%02d%02d%02d%02d%02d%02d%02d",
                        currDateTime.bCentury, currDateTime.bYear, currDateTime.bMonth, currDateTime.bDay,
                        currDateTime.bHour, currDateTime.bMinutes, currDateTime.bSeconds); 
        }
        else
        {
            *bsize = sprintf(pPBPTemp," ");
        }
		
        return pPBPTemp;
		
    case METER_DECIMAL_PLACES_ID:
        if (fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *) &bMoneyDecimals) == BOB_OK)
            *bsize = sprintf(pPBPTemp,"%d",(int)bMoneyDecimals);
        else
            *bsize = sprintf(pPBPTemp," ");
			
        return pPBPTemp;
		
    case PBP_PSD_PIECE_COUNT:       
        if (fnValidData(BOBAMAT0, GET_VLT_PIECE_COUNTER, (void *) &ulPsdPieceCount) == BOB_OK) 
            *bsize = sprintf(pPBPTemp,"%lu",ulPsdPieceCount);
        else
		
            *bsize = sprintf(pPBPTemp," ");
        return pPBPTemp;
                
    case GET_COMM_TYPE:
		pPBPTemp[0] = 0;	// so strcat can be used by the subroutine.
        fnXMLAppendConnectivityInfo(pPBPTemp, FALSE);
        *bsize = (unsigned short)strlen(pPBPTemp);
        return pPBPTemp;

    case GET_UK_EIB_ENABLED_FLAG:
		// to match Connect+, leave the size as zero and return a null string.
		pPBPTemp[0] = 0;
        return pPBPTemp;

    case DCAP_GUID:
    	pPBPTemp[0] = 0;	// so strcat can be used by the subroutine.
    	trmGetDcapGUID(pPBPTemp, bsize);
		*bsize = (unsigned short)strlen(pPBPTemp);
    	return pPBPTemp;
    default:
        *bsize = (unsigned short)sprintf(pPBPTemp," ");
        return pPBPTemp;
    }
}

/************************************
    Money conversion Routines
************************************/
static short fnPBPS_XmlMoney2Double(XmlChar *pSource, short iLen, double *pDest, BOOL fAdjustDecimals)
{
    double dblTemp;
    unsigned short pUni[20], usInx;
    unsigned short usDecimalSeparator = fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR);
    unsigned char bMoneyDecimals;

    /* get the number of decimals */
    if (fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *) &bMoneyDecimals) != BOB_OK)
    {
        fnProcessSCMError();
        return PBPS_PARSE_ERR;
    }

    /* make sure there is room */
    if (iLen >= 20)
        return PBPS_PARSE_ERR;

    /* convert the ascii to unicode */
    fnAscii2Unicode( (char *)pSource,pUni,(unsigned short)iLen);
    pUni[iLen] = 0;

    // fnUnicode2Money is expecting the decimal separator in the value to be the same 
    // as the WP_DISP_DECIMAL_POINT_CHAR parameter, so if the parameter isn't a period,
    // we need to replace the decimal separator, if any, in the value with the 
    // parameter character.
    if (usDecimalSeparator != '.')
    {
        for (usInx = 0; usInx < (unsigned short)iLen; usInx++)
        {
            if (pUni[usInx] == '.')
            {
                pUni[usInx] = usDecimalSeparator;
                break;
            }
        }
    }

    /* convert unicode to double */
    dblTemp = fnUnicode2Money(pUni);
    
    if (fAdjustDecimals)
    {
        /* adjust for decimal places */
        while (bMoneyDecimals--)
            dblTemp /= 10.0;
    }

    *pDest = dblTemp;

    return PBPS_OK;
}

/************************************
    Parsing Routines and stuff
************************************/

static void fnPBPS_InitParserData(void)
{
    fXMLParserError = FALSE;

    *cSessionID = 0;

    iCmdStaCmp = -1;

    fCreditBal = fDebitBal = FALSE;
    
    psdTransferControl.bytesInSource = 0;

    fDebitBal = fCreditBal = FALSE;
    dblDebitBal = dblCreditBal = 0.0;
}

static short fnPBPS_CmdStaCmp_ETag(XmlChar *pName)
{
    pPBPTemp[lwPBPTempBuffLen] = 0;
    iCmdStaCmp = atoi((char*)pPBPTemp);
    return XML_STATUS_OK;
}

static short fnPBPS_SesIdCur_ETag(XmlChar *pName)
{
    pPBPTemp[lwPBPTempBuffLen] = 0;
    memset((void*)cSessionID,0,sizeof(cSessionID));
    strncpy(cSessionID,(const char*)pPBPTemp,PBP_MAX_SESSIONID_LEN-1);
    return XML_STATUS_OK;
}
static short fnPBPS_Base64_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    short iStatus = PBPS_OK;    

    if (bMessageTag == PBPS_CERTLIST)
    {
        /* The PBP task is parsing the Certificate list and
        doesn't need to do anything at this point. */
        iStatus = PBPS_OK;
    }
    else
        iStatus = fnPBPS_Generic_Stag(pName,pAttrList,iNumAttr);
    return iStatus;
}
static short fnPBPS_Base64_Chars(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    short iStatus = PBPS_OK;
    unsigned long iTmpBase64Len = 0, sResultLen = 0;
    XmlChar *pRealBase64 = pChars;
    char bFileNameBuff[PBPS_MAX_CERT_TEMP_FILENAME_SIZE];
    long sigOffset;

    
    if (bMessageTag == PBPS_CERTLIST && iPBPSNewCertListVer > 0)
    {       
        if (pChars != NULL && iLen > 0)
        {
            /* Remove leading whitespace. */
            while (iLen > 0 && *pRealBase64 != '\0' && isspace((int)(*pRealBase64)))
            {
                pRealBase64++;
                iLen--;
            }       
            if (pRealBase64 != NULL && *pRealBase64 != '\0')
            {           
                /* Figure out how big the binary data will be. */
                iTmpBase64Len = B64_UCHAR_CHARS(iLen);
                /* Allocate memory to hold the certificate list. */         
                pPBPSCertBase64Buff = (unsigned char*)malloc(iTmpBase64Len);
                if (pPBPSCertBase64Buff != NULL)
                {
                    iPBPSCertBase64BuffLen = iTmpBase64Len;                 
                    /* The pPBPTemp buffer contains the Base-64
                    encoded contents of the Trusted Root Certificate
                    List.  Decode the buffer and save its contents to
                    a file. */
                    if (B64Decode((char*)pRealBase64,iLen,(unsigned char*)pPBPSCertBase64Buff,iPBPSCertBase64BuffLen,&sResultLen) == CONVERSION_OK)
                    {
                    	iStatus = fnPBPSSaveCertificatesToFile(bFileNameBuff, sizeof(bFileNameBuff), iPBPSNewCertListVer, &sigOffset, (unsigned char*)pPBPSCertBase64Buff, sResultLen);  /*lint !e571 */
						
//TODO - restore generation of a proper error for this condition once Tier3 fixes problem with downloading old file
                        if (iStatus == PBPS_CERT_LIST_DOWNLOAD_VERSION_OLD)
                        {
            	            dbgTrace(DBG_LVL_INFO, "Ignoring command from Tier3 to download root cert list that is not a higher version than current");
            	            fnCheckFileSysCorruption(unlink(bFileNameBuff)); // delete file
            	            iStatus = PBPS_OK;
                        }
                    	else if (iStatus == PBPS_OK)
                        {// transfer downloaded certs to cert list file used by SSL interface layer
                        	iStatus = fnPBPSActivateCertificates(bFileNameBuff, sigOffset);
                        }
                        else
                        {
                            dbgTrace(DBG_LVL_ERROR, "Downloaded root cert list file save failure %s: %d ", bFileNameBuff, iStatus);
                        }
                    }
                    else
                    {
                        /* Error - Invalid Base64 */
                        iStatus = PBPS_CERT_LIST_BASE64_ERROR;
                    }
                }
                else
                {
                    /* Error - Out of memory. */
                    // TODO: log the error and continue
                    iStatus = PBPS_SSL_OUT_OF_MEMORY;
                }
            }
        }
        else
        {
            /* Error - Invalid Base64 */
            iStatus = PBPS_CERT_LIST_BASE64_ERROR;
        }
        
        /* Release the buffer. */
        if (pPBPSCertBase64Buff != NULL)
        {
            free(pPBPSCertBase64Buff);
            pPBPSCertBase64Buff = NULL; 
        }
        iPBPSCertBase64BuffLen = 0;
    }
    else
        iStatus = fnPBPS_Generic_Chars(pName,pChars,iLen);
    return iStatus;
}


static short fnPBPS_Base64_ETag(XmlChar *pName)
{
	unsigned long sResultLen = 0;
	unsigned long sMaxBufferLen = 0;
    unsigned long ulPSDHeader = 0;
    unsigned char *pB64Dest = NULL;

    if (bMessageTag == PBPS_CERTLIST)
    {
        /* The PBP task is parsing the Certificate list and
        doesn't need to do anything at this point. */
        return XML_STATUS_OK;
    }
    else
    {
        /* NULL terminate the temp buffer */
        pPBPTemp[lwPBPTempBuffLen] = 0;

        /* determine the length and offset at which we will decode
        the record. */
        if (bMessageTag == PBPS_DCREKEY_REQ || bMessageTag == PBPS_DCEDC_REQ ||
            bMessageTag == PBPS_DC_GEN_KEY_EXCH_KEY || bMessageTag == PBPS_DC_LOAD_SECRET_KEY )
        {
            /* For compatibility with the EDM stuff, the buffer we
            send to the PSD must contain the packet and size info as
            well as the record itself. */
            pB64Dest = (unsigned char*)pPSDCommonBuff + 4;
            sMaxBufferLen = COMMON_BUFF_LEN - 4;
        }
        else
        {
            /* The record we pass to the psd has no packet or size info in it. */
            pB64Dest = pPSDCommonBuff;
            sMaxBufferLen = COMMON_BUFF_LEN;
        }

        /* Decode the record. */
        if (B64Decode((char*)pPBPTemp,lwPBPTempBuffLen,pB64Dest,sMaxBufferLen,&sResultLen) != CONVERSION_OK)
            return PBPS_PARSE_ERR;

        /* make sure that a record has been sent. */
        if (sResultLen < 4)
        {
            /* we didn't even get the header */
            switch (bMessageTag)
			{
            case PBPS_DCAUDIT_RSP: return PBPS_DCAUDITREC_ERR;          
            case PBPS_DCREFILL_RSP: return PBPS_DCREFILL_ERR;
            case PBPS_DCREFUND_RSP: return PBPS_DCREFUND_ERR;
            case PBPS_DCAUTH_REQ: return PBPS_DCAUTH_ERR;
            case PBPS_DCCONFIG_REQ: return PBPS_DCCONFIG_ERR;
            case PBPS_DCEDC_REQ: return PBPS_DCEDC_ERR;
            case PBPS_DCREKEY_REQ: return PBPS_DCREKEY_ERR;     
            default:
                /* this is bad because we are receiving a 
                record, but we don't know what kind. */
                return PBPS_PROCEDURE_ERR;
            }
        }   
        
        /* store the header of this record */
        memcpy(&ulPSDHeader,(void*)&pPSDCommonBuff[0],sizeof(ulPSDHeader));

        /* check for the PVD error record */
        if (bMessageTag == PBPS_DCREFILL_RSP)
        {
            /* this record is the response to a refill
            request and there is at least enough room 
            for the 4 byte header, check to see if it 
            is the PVD error record. this record requires
            special handling. */        
            if (ulPSDHeader == C_BLK_IBI_PVD_ERROR_RECORD)
            {
                /* the data center has returned a PVD_ERROR_RECORD.
                this record must be sent to the psd.  so we
                set a flag and pretend that the record is ok.  after
                the record is sent to the psd, we look at the flag 
                and then return the pbp task error. */
                fDCPvdErrorRecord = TRUE;           
            }
        }
        
        /* the base-64 is ok, send it to the PSD */
        psdTransferControl.bytesInSource = (ushort)sResultLen;
        return XML_STATUS_OK;   
    }
}
static short fnPBPS_RootCertListVer_STag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    short iStatus = PBPS_OK;

    /* reset some variables. */
    iPBPSNewCertListBuffLen = 0;
    (void)memset((void*)pPBPSNewCertListBuff,0,sizeof(pPBPSNewCertListBuff));
    iPBPSNewCertListVer = 0;

    return iStatus;
}
static short fnPBPS_RootCertListVer_Chars(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    short iStatus = PBPS_OK;
    
    /* the new certificate list version is being parsed.
    only parse this once per connection. */
    if (bMessageTag == PBPS_CERTLIST && iPBPSNewCertListVer == 0)
    {
        if ((iPBPSNewCertListBuffLen + iLen) < PBPS_NEW_CERT_LIST_MAX_LEN)
        {
            (void)memcpy((void*)&pPBPSNewCertListBuff[iPBPSNewCertListBuffLen],pChars,iLen);
            iPBPSNewCertListBuffLen += iLen;            
        }           
    }   
    return iStatus;
}
static short fnPBPS_RootCertListVer_Etag(XmlChar *pName)
{
    if (bMessageTag == PBPS_CERTLIST && iPBPSNewCertListVer == 0 && iPBPSNewCertListBuffLen > 0)
    {
        pPBPSNewCertListBuff[iPBPSNewCertListBuffLen] = 0;
        iPBPSNewCertListVer = atoi((const char*)pPBPSNewCertListBuff);
    }
    return PBPS_OK;
}
static short fnPBPS_CusBalDeb_ETag(XmlChar *pName)
{
    short iStatus = 0;

    pPBPTemp[lwPBPTempBuffLen] = 0;

    iStatus = fnPBPS_XmlMoney2Double((XmlChar*)pPBPTemp,(short)lwPBPTempBuffLen,&dblDebitBal,FALSE);

    if (iStatus == PBPS_OK)
        fDebitBal = TRUE;

        if (*pPBPTemp == '-')   // debit balance can be negative in Switzerland if "emergency credit" is issued
            dblDebitBal *= -1;

    return iStatus;     
}
static short fnPBPS_CusBalCre_ETag(XmlChar *pName)
{
    short iStatus = 0;

    pPBPTemp[lwPBPTempBuffLen] = 0;

    iStatus = fnPBPS_XmlMoney2Double((XmlChar*)pPBPTemp,(short)lwPBPTempBuffLen,&dblCreditBal,FALSE);

    if (iStatus == PBPS_OK)
        fCreditBal = TRUE;
    return iStatus;
}
// Sign On Rsp
static short fnPBPS_SesIdNew_ETag(XmlChar *pName)
{
    pPBPTemp[lwPBPTempBuffLen] = 0;
    memset((void*)cSessionID,0,sizeof(cSessionID));
    memset((void*)cCurrSessionID,0,sizeof(cCurrSessionID));

    strncpy(cCurrSessionID,(const char*)pPBPTemp,PBP_MAX_SESSIONID_LEN-1);
    strncpy(cSessionID,(const char*)pPBPTemp,PBP_MAX_SESSIONID_LEN-1);
    
    return XML_STATUS_OK;
}
static short fnPBPS_PbpActNum_ETag(XmlChar *pName)
{
    long lPbpActNum = 0;
    ushort usPbpActNumLen = 0;

    if(fnFlashGetByteParm(BP_ACCEPT_PBPACCT_NUMBER_DOWNLOAD))
    {
        pPBPTemp[lwPBPTempBuffLen] = 0;
    	usPbpActNumLen = fnAsciiBcdToPackedBcd((uchar*) &lPbpActNum, sizeof(lPbpActNum), (char*) pPBPTemp, TRUE);
        
        if(usPbpActNumLen == sizeof(lPbpActNum))
            CMOSSetupParams.lwPBPAcctNumber = lPbpActNum;
        else
        {
            dbgMsg("PBPS_DC_SEND_PBP_ACCT_NUM_ERR", PBPS_DC_SEND_PBP_ACCT_NUM_ERR, __LINE__);
            fnLogError(ERR_TYPE_NORMAL, ERROR_CLASS_PBP,(unsigned char)(PBPS_DC_SEND_PBP_ACCT_NUM_ERR - XML_USER_ERROR_BASE));    //log error in CMOS

            // set the Display error bytes so PCMC will know what the problem is.       
            ucOIErrorClass = ERROR_CLASS_PBP;
            ucOIErrorCode = (unsigned char)(PBPS_DC_SEND_PBP_ACCT_NUM_ERR - XML_USER_ERROR_BASE);
        }
    }

    return XML_STATUS_OK; 
}

#define GMT_TIME_SYNC_UPDATE_IN_PROGRESS            0x80000000

extern unsigned long ulCMOSTimeSyncStatus;
extern DATETIME dtCMOSTimeSyncLockoutEnd;

static BOOL fnPBPSParseXmlDateTime(XmlChar *pXmlDateTime, DATETIME *pDateTime)
{
    char *pIndex = NULL;
    BOOL fRetval = FALSE;
    char TempString[3];
    
    // YYYYMMDDHHSS
    if (pXmlDateTime != NULL && pDateTime != NULL)
    {
        pIndex = (char*)pXmlDateTime;
        TempString[2] = '\0';
        if (strlen(pIndex) == 14)
        {
            memcpy( TempString, pIndex, 2 );
            pIndex += 2;    
            pDateTime->bCentury = (unsigned char)atoi( (char*)TempString );
            memcpy( TempString, pIndex,  2 );
            pIndex += 2;
            pDateTime->bYear = (unsigned char)atoi( (char*)TempString );
            memcpy( TempString, pIndex, 2 );
            pIndex += 2;
            pDateTime->bMonth = (unsigned char)atoi( (char*)TempString );
            memcpy( TempString, pIndex, 2 );
            pIndex += 2;
            pDateTime->bDay = (unsigned char)atoi( (char*)TempString );
            memcpy( TempString, pIndex, 2 );
            pIndex += 2;
            pDateTime->bHour = (unsigned char)atoi( (char*)TempString );
            memcpy( TempString, pIndex, 2 );
            pIndex += 2;
            pDateTime->bMinutes = (unsigned char)atoi( (char*)TempString );
            memcpy( TempString, pIndex, 2 );
            pIndex += 2;
            pDateTime->bSeconds = (unsigned char)atoi( (char*)TempString );
            pDateTime->bDayOfWeek = 1;
            
            fRetval = fnValidDate(pDateTime);
        }
    }
    
    return fRetval;
}

static short fnPBPS_DcnInfTim_ETag(XmlChar *pName)
{
    //EVENT_SCHEDULER_INFO *pNewEvent = NULL; 
    long lDateTimeDiff = 0;
    unsigned long lwTodSecs = 0;    /* seconds passed starting at the beginning of the day */
    uchar ucDummyData = 0;
    BOOL activateTimer = FALSE;
    //BOOL RtcTimeSyncOk = FALSE;
    DATETIME currDateTime, currDateTimeWithOffsets, dcDateTime; 
    BOOL fOkToAdjustClock = FALSE, fSetNegativeDriftDisablingCond = FALSE, fForwardDayBoundaryCrossed = FALSE;
    unsigned char bTimeSyncEnabledInUic = TRUE, bTimeSyncEnabledInEmd = fnFlashGetByteParm(BP_PBP_TIME_SYNC_ENABLE);


    if (ulCMOSTimeSyncStatus & TIME_SYNC_DISABLED_BY_UIC)
        bTimeSyncEnabledInUic = FALSE;  
    
    if (bTimeSyncEnabledInUic == TRUE && bTimeSyncEnabledInEmd == TRUE)
    {
        /* Set Power Failure Detection flag and clear any disabling conditions. */
        ulCMOSTimeSyncStatus = GMT_TIME_SYNC_UPDATE_IN_PROGRESS;
        
        /* Is the PSD present? No point in trying to sync a clock that isn't present. */
        if (fnSYSIsPSDDetected() == TRUE)
        {
            /* Parse the Data Center GMT time. */
            pPBPTemp[lwPBPTempBuffLen] = 0;
            if (fnPBPSParseXmlDateTime((XmlChar*)pPBPTemp,&dcDateTime) == TRUE)
            {
                /* Get the Meter GMT time and determine the difference
                between the Meter and Data Center GMT times. */

                #ifdef JANUS
                // Resync local clock before calculating the time difference
                // so GMT correction does not include other drifts caused by out-of-sync
                // local time
                if(fnValidData(BOBAMAT0, BOBID_IPSD_INIT_CLOCK, &ucDummyData) != BOB_OK)
                {
                    fnProcessSCMError();
                }
                else
                #endif
                if (GetSysDateTime(&currDateTime, FALSE, TRUE) == TRUE &&                               
                    GetSysDateTime(&currDateTimeWithOffsets, TRUE, TRUE) == TRUE && 
                    CalcDateTimeDifference(&dcDateTime,&currDateTime,&lDateTimeDiff) == TRUE &&
                    lDateTimeDiff != 0)
                {
                    #ifdef JANUS
                    sprintf(tmpBuf,"DC GMT Time %02d/%02d/%02d %02d:%02d:%02d", 
                        dcDateTime.bMonth, dcDateTime.bDay, dcDateTime.bYear,
                        dcDateTime.bHour,dcDateTime.bMinutes, dcDateTime.bSeconds);
                    putString(tmpBuf, __LINE__);

                    sprintf(tmpBuf,"Local GMT Time %02d/%02d/%02d %02d:%02d:%02d", 
                        currDateTime.bMonth, currDateTime.bDay, currDateTime.bYear,
                        currDateTime.bHour,currDateTime.bMinutes, currDateTime.bSeconds);
                    putString(tmpBuf, __LINE__);

                    sprintf(tmpBuf,"Time diff %ld", lDateTimeDiff);
                    putString(tmpBuf, __LINE__);
                    #endif

                    /* is the adjustment too large? */
                    if (abs(lDateTimeDiff) < 7200)  /* 2 * 3600 seconds per hour = 2 hours */
                    {           
                        /* Is the Data Center Time in the past? */
                        if (lDateTimeDiff > 0)
                        {
                            /* Moving the clock backwards can NOT result in
                            the date moving across a date boundary. */
                            lwTodSecs = fnTodToSecs(&currDateTimeWithOffsets); /* secs since day started */
                            if (lwTodSecs < (unsigned long)lDateTimeDiff)
                            {                           
                                /* the adjustment would result in the Date being crossed. */                    
                                fOkToAdjustClock = FALSE;
                            }
                            else
                            {
                                /* The backwards adjustment does NOT cross
                                a date boundary. */
                                fOkToAdjustClock = TRUE;
                                
                                /* The Meter must be disabled for a period equal to
                                the difference between the Meter Date/Time and the
                                Data Center Date/Time. Store the disabling period
                                and disable the meter. */                   
                                fSetNegativeDriftDisablingCond = TRUE;

                                lwWaitTimeToSync = lDateTimeDiff;
                                activateTimer = (lDateTimeDiff >= 240) ? TRUE : FALSE;
                            }
                        }
                        else
                        {
                            /* Adjusting the drift to move the clock forwards
                            is always allowed. */
                            fOkToAdjustClock = TRUE;
                            
                            /* Determine if the forwards drift correction results
                            in a day boundary being crossed. */
                            lwTodSecs = fnTodToSecs(&currDateTimeWithOffsets); /* secs since day started */                                         
                            if ((lwTodSecs + (unsigned long)abs(lDateTimeDiff)) > 86400) /* 86400 seconds in a day */
                            {
                                /* The forwards adjustment causes the date to change. */
                                fForwardDayBoundaryCrossed = TRUE;
                            }
                        }       
                        
                        /* diff is in seconds, convert it to minutes required in meter (psd) */
                        lDateTimeDiff /= 60;
						
                        /* change direction of drift to correct it */
                        lDateTimeDiff *= -1;

                        /* if it is ok to do so, try to set the meter (psd) drift */
                        if (fOkToAdjustClock == TRUE)
                        {
                            BOOL fTimeset = TRUE;
#ifndef JANUS
                            if(fnWriteData(BOBAMAT0, WRITE_PSD_DRIFT, (void *) &lDateTimeDiff) == BOB_OK)
                            {
                                /* the meter (psd) accepted the drift change, save it on the UIC side */
                                (void)SetRTClockDrift(lDateTimeDiff);
                            }
                            else
                                fTimeset = FALSE;
#else
                            if(fnUserAdjustRTC(&dcDateTime, ADJUST_GMT) != IN_RANGE)
                                fTimeset = FALSE;
#endif
                            if(fTimeset)
                            {
                                /* now that the meter clock has been set set the disabling condition */
                                if (fSetNegativeDriftDisablingCond == TRUE)
                                {
                                    dtCMOSTimeSyncLockoutEnd = currDateTimeWithOffsets;
                                    ulCMOSTimeSyncStatus |= TIME_SYNC_NEGATIVE_DRIFT_LOCKOUT;

                                    #ifdef JANUS // Janus uses timer call back rather than polling in OI
                                    if (activateTimer)
                                    {
                                        OSStopTimer(TIME_SYNC_WAIT_TIMER);
										
                                        // Add 2 more seconds to avoid the case when lDateTimeDiff == 0
                                        OSChangeTimerDuration(TIME_SYNC_WAIT_TIMER, (lwWaitTimeToSync + 2) * SEC);
                                        OSStartTimer(TIME_SYNC_WAIT_TIMER);
                                    }
                                    #endif
                                }
                                                            
                            }
                        }
                        #ifdef JANUS
                        else// fOkToAdjustClock == FALSE
						{   // update the alarm timers since time has been resynced
							// Don't need to do this if fOkToAdjustClock == TRUE
							// as fnUserAdjustRTC() will do it
							fnResyncTimers();
                        }
                        #endif
                    }
                    else
                    {
                        /* There is an error.  The Meter Clock and Data Center Date/Time
                        are 2 or more hours out of sync. Disable the meter. */                  
                        ulCMOSTimeSyncStatus |= TIME_SYNC_MAX_DRIFT_ERR;            
                    }
                }   
            }
        }
        
        /* Clear power-failure flag. */
        ulCMOSTimeSyncStatus &= ~GMT_TIME_SYNC_UPDATE_IN_PROGRESS;
    }

    return XML_STATUS_OK;
}

unsigned long fnInfraGetTimeSyncStatus(void)
{
    unsigned long ulTimeSyncStatus = 0;
    long lDateTimeDiff = 0;
    DATETIME currDateTime;
    unsigned char bTimeSyncEnabledInUic = TRUE, bTimeSyncEnabledInEmd = fnFlashGetByteParm(BP_PBP_TIME_SYNC_ENABLE);
    
    if (ulCMOSTimeSyncStatus & TIME_SYNC_DISABLED_BY_UIC)
        bTimeSyncEnabledInUic = FALSE;
    
    if (bTimeSyncEnabledInUic == TRUE && bTimeSyncEnabledInEmd == TRUE)
    {
        if (ulCMOSTimeSyncStatus & GMT_TIME_SYNC_UPDATE_IN_PROGRESS)
        {
            /* power was interrupted while synchronizing the clock */
            ulTimeSyncStatus |= TIME_SYNC_CONTACT_PB;
        }
        else
        {   
            /* power was not interrupted while synchronizing the clock. */
            if (ulCMOSTimeSyncStatus  & TIME_SYNC_NEGATIVE_DRIFT_LOCKOUT)
            {
                /* the clock was adjusted backwards. check to see if the lockout period has ended. */
                if (fnSYSIsPSDDetected() == TRUE && 
                    GetSysDateTime(&currDateTime, TRUE, TRUE) == TRUE && 
                    CalcDateTimeDifference(&dtCMOSTimeSyncLockoutEnd,&currDateTime,&lDateTimeDiff) == TRUE &&
                    (lDateTimeDiff >= 0))
                {
                    /* the lockout period has ended. */
                    ulCMOSTimeSyncStatus &= ~TIME_SYNC_NEGATIVE_DRIFT_LOCKOUT;             
                    sprintf(tmpBuf,"Clear Time sync lockout. diff=%ld", lDateTimeDiff);
                    putString(tmpBuf, __LINE__);
                }
                else
                {           
                    /* the lockout period has not yet ended. */
                    ulTimeSyncStatus |= TIME_SYNC_NEGATIVE_DRIFT_LOCKOUT;

                    #ifdef JANUS // Janus uses timer call back rather than polling in OI
                    if(lDateTimeDiff != 0)
                    {
                        lwWaitTimeToSync = abs(lDateTimeDiff);
                        OSStopTimer(TIME_SYNC_WAIT_TIMER); 
                        // Add 2 more seconds to avoid the case when lDateTimeDiff == 0
                        OSChangeTimerDuration(TIME_SYNC_WAIT_TIMER, (lwWaitTimeToSync + 2) * SEC);
                        OSStartTimer(TIME_SYNC_WAIT_TIMER);

                        sprintf(tmpBuf,"CMOS Sync End Time %02d/%02d/%02d %02d:%02d:%02d", 
                            dtCMOSTimeSyncLockoutEnd.bMonth, dtCMOSTimeSyncLockoutEnd.bDay, dtCMOSTimeSyncLockoutEnd.bYear,
                            dtCMOSTimeSyncLockoutEnd.bHour,dtCMOSTimeSyncLockoutEnd.bMinutes, dtCMOSTimeSyncLockoutEnd.bSeconds);
                        putString(tmpBuf, __LINE__);

                        (void)GetSysDateTime(&currDateTime, TRUE, TRUE);
                        sprintf(tmpBuf,"Local GMT Time %02d/%02d/%02d %02d:%02d:%02d", 
                            currDateTime.bMonth, currDateTime.bDay, currDateTime.bYear,
                            currDateTime.bHour,currDateTime.bMinutes, currDateTime.bSeconds);
                        putString(tmpBuf, __LINE__);

                        sprintf(tmpBuf,"Time diff %ld", lDateTimeDiff);
                        putString(tmpBuf, __LINE__);
                    }
                    #endif
                }
            }
                
            /* check to see if the meter has drifted past the max allowed value. */
            if (ulCMOSTimeSyncStatus & TIME_SYNC_MAX_DRIFT_ERR)
                ulTimeSyncStatus |=  TIME_SYNC_MAX_DRIFT_ERR;
        }
    }

    return ulTimeSyncStatus;
}

#ifdef JANUS
void fnInfraTimeSyncLockoutEndCallback(ulong inputVar)
{
    // ulCMOSTimeSyncStatus &= ~TIME_SYNC_NEGATIVE_DRIFT_LOCKOUT;
    OSStopTimer(TIME_SYNC_WAIT_TIMER);
    OSSendIntertask(OIT, DISTTASK, CHECK_TIME_SYNC, NO_DATA, NULL, 0);
    lwWaitTimeToSync = 0;
}

#else
void fnInfraTimeSyncLockoutEndCallback(EVENT_SCHEDULER_INFO *eventInfoPtr)
{
    ulCMOSTimeSyncStatus &= ~TIME_SYNC_NEGATIVE_DRIFT_LOCKOUT;
}
#endif

// Audit Response
static short fnPBPS_RisuPvdAmt_ETag(XmlChar *pName)
{   
    double dblTemp;
    short iStatus = PBPS_OK;
    
    pPBPTemp[lwPBPTempBuffLen] = 0;

    /* Get the reissue amount. */
    iStatus = fnPBPS_XmlMoney2Double((XmlChar*)pPBPTemp,(short)lwPBPTempBuffLen,&dblTemp,FALSE);
    
    /* Make sure there is a reissue amount. */
    if (iStatus == PBPS_OK && dblTemp != 0.0)
    {       
        /* Record the fact that we are doing a reissue. */
        AuditRec.fReissue = TRUE;

        /* Record the reissue amount. */
        AuditRec.dblReissueAmt = dblTemp;
    }

    return iStatus;
}

// Initialize the cmosEuroConversion with the CurrencyID sent from Meter Services
static short fnPBPS_CurrencyID_ETag(XmlChar *pName)
{
    pPBPTemp[lwPBPTempBuffLen] = 0;
    cmosEuroConversion.ucCurrencyID = (unsigned char)atoi((char*)pPBPTemp);
    return XML_STATUS_OK;
}

// Postal config Request
static short fnPBPS_CodOrgPst_ETag(XmlChar *pName)
{
    pPBPTemp[lwPBPTempBuffLen] = 0;

    return XML_STATUS_OK;
}
static short fnPBPS_PrtIndNum_ETag(XmlChar *pName)
{
    pPBPTemp[lwPBPTempBuffLen] = 0;
    memset((void*)&CMOSPrintedIndiciaNum[0],0,sizeof(CMOSPrintedIndiciaNum));

    if (bMessageTag == PBPS_DCCONFIG_REQ && lwPBPTempBuffLen > 0)
    {
        strncpy(&CMOSPrintedIndiciaNum[0],(char*)pPBPTemp,PRINTED_INDICIA_NUM_SZ-1);
    }
    return XML_STATUS_OK;
}
static short fnPBPS_IndLangCod_ETag(XmlChar *pName)
{
    pPBPTemp[lwPBPTempBuffLen] = 0;
    memset((void*)&CMOSIndiciaLanguageCode[0],0,sizeof(CMOSIndiciaLanguageCode));

    if (bMessageTag == PBPS_DCCONFIG_REQ && lwPBPTempBuffLen > 0)
    {
        strncpy(&CMOSIndiciaLanguageCode[0],(char*)pPBPTemp,INDICIA_LANGUAGE_CODE_SZ-1);
    }
    return XML_STATUS_OK;
}
static short fnPBPS_CusRecRef_ETag(XmlChar *pName)
{
    pPBPTemp[lwPBPTempBuffLen] = 0;
    memset((void*)&CMOSCustomerID_RRN[0],0,sizeof(CMOSCustomerID_RRN));

    if (bMessageTag == PBPS_DCCONFIG_REQ && lwPBPTempBuffLen > 0)
    {
        strncpy(&CMOSCustomerID_RRN[0],(char*)pPBPTemp,CUSTOMERID_RRN_SZ-1);
    }
    return XML_STATUS_OK;
}

/**************************************************************************
// FUNCTION NAME:
//      fnPBPS_TowNamOne_Etag
// DESCRIPTION:
//      Handler function for Town Name one's end element.
// INPUTS:
//      pName
// OUTPUTS:
// RETURNS:
//      XML_STATUS_OK/XML_HANDLER_GENERAL_ERR
// REVISION:
//      Feb 17 2010   Deborah Kohl  Remove #if defined around 
//                                  CmosDownloadCtrlBuf.tcNameDownloaded
//      Feb 18 2009     Rocky Dai   With the first IF statement, lwPBPTempBuffLen
//                                  should be compared with (PBP_TEMP_BUFF_LEN - 1),
//                                  which is merged from Express Meter.
//      Feb 16 2009     Rocky Dai   Merged from Express Meter.
// *************************************************************************/
static short fnPBPS_TowNamOne_Etag(XmlChar *pName)
{
    if(lwPBPTempBuffLen > (PBP_TEMP_BUFF_LEN - 1))
    {
      return XML_HANDLER_GENERAL_ERR;
    }
    pPBPTemp[lwPBPTempBuffLen] = 0;
/*
    if (UTF8ToUnicode(exp_mtr_cmos.town_name_1, EXP_MTR_TOWN_NAME_1_SIZE_BUF , (unsigned char *)pPBPTemp, lwPBPTempBuffLen) == 0)
    {
      return XML_HANDLER_GENERAL_ERR;
    }
*/
    CmosDownloadCtrlBuf.tcNameDownloaded = TRUE;
    return PBPS_OK;
}

/**************************************************************************
// FUNCTION NAME:
//      fnPBPS_TowNamTwo_Etag
// DESCRIPTION:
//      Handler function for Town Name two's end element.
// INPUTS:
//      pName
// OUTPUTS:
// RETURNS:
//      XML_STATUS_OK/XML_HANDLER_GENERAL_ERR
// REVISION:
//      Feb 17 2010   Deborah Kohl  Remove #if defined around 
//                                  CmosDownloadCtrlBuf.tcNameDownloaded
//      Feb 18 2009     Rocky Dai   With the first IF statement, lwPBPTempBuffLen
//                                  should be compared with (PBP_TEMP_BUFF_LEN - 1),
//                                  which is merged from Express Meter.
//      Feb 16 2009     Rocky Dai   Merged from Express Meter.
// *************************************************************************/
static short fnPBPS_TowNamTwo_Etag(XmlChar *pName)
{
    if(lwPBPTempBuffLen > (PBP_TEMP_BUFF_LEN - 1))
    {
        return XML_HANDLER_GENERAL_ERR;
    }
    pPBPTemp[lwPBPTempBuffLen] = 0;
/*
    if (UTF8ToUnicode(exp_mtr_cmos.town_name_2, EXP_MTR_TOWN_NAME_2_SIZE_BUF , (unsigned char *)pPBPTemp, lwPBPTempBuffLen) == 0)
    {
      return XML_HANDLER_GENERAL_ERR;
    }
*/
    CmosDownloadCtrlBuf.tcNameDownloaded = TRUE;
    return PBPS_OK;
}

static short fnPBPS_TowNamThree_Etag(XmlChar *pName)
{
    if(lwPBPTempBuffLen > (PBP_TEMP_BUFF_LEN - 1))
    {
        return XML_HANDLER_GENERAL_ERR;
    }
    pPBPTemp[lwPBPTempBuffLen] = 0;
/*
    if (UTF8ToUnicode(exp_mtr_cmos.town_name_2, EXP_MTR_TOWN_NAME_2_SIZE_BUF , (unsigned char *)pPBPTemp, lwPBPTempBuffLen) == 0)
    {
      return XML_HANDLER_GENERAL_ERR;
    }
*/
    CmosDownloadCtrlBuf.tcNameDownloaded = TRUE;
    return PBPS_OK;
}

static short fnPBPS_TowNamFour_Etag(XmlChar *pName)
{
    if(lwPBPTempBuffLen > (PBP_TEMP_BUFF_LEN - 1))
    {
        return XML_HANDLER_GENERAL_ERR;
    }
    pPBPTemp[lwPBPTempBuffLen] = 0;
/*
    if (UTF8ToUnicode(exp_mtr_cmos.town_name_2, EXP_MTR_TOWN_NAME_2_SIZE_BUF , (unsigned char *)pPBPTemp, lwPBPTempBuffLen) == 0)
    {
      return XML_HANDLER_GENERAL_ERR;
    }
*/
    CmosDownloadCtrlBuf.tcNameDownloaded = TRUE;
    return PBPS_OK;
}

static short fnPBPS_GUID_ETag(XmlChar *pName)
{
    pPBPTemp[lwPBPTempBuffLen] = 0;
    memset((void*)&CMOSDcapGuid[0],0,sizeof(CMOSDcapGuid));

    dbgTrace(DBG_LVL_INFO, "fnPBPS_GUID_ETag: %32s ", pPBPTemp);

    if (bMessageTag == PBPS_DCCONFIG_REQ && lwPBPTempBuffLen > 0)
    {
        strncpy((char*)&CMOSDcapGuid[0], (char*)pPBPTemp, GUID_SIZE);
        dbgTrace(DBG_LVL_INFO, "fnPBPS_GUID_ETag: %32s ", CMOSDcapGuid);
    }
    return PBPS_OK;
}



// Refund Response
static short fnPBPS_DcnStaCmp_ETag(XmlChar *pName)
{
    pPBPTemp[lwPBPTempBuffLen] = 0;
    return XML_STATUS_OK;
}

/*********************************
    Special start tags
**********************************/
static short fnPBPS_SignOnRsp_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCSIGNON_RSP;
    return PBPS_OK;
}
static short fnPBPS_SignOffInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCSIGNOFF_REQ;
    return PBPS_OK;
}
static short fnPBPS_SignOffRsp_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCSIGNOFF_RSP;
    return PBPS_OK;
}
static short fnPBPS_PollInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    lwPBPSStatus |= PBPS_DC_SENT_POLL;

    bMessageTag = PBPS_DCRCV_POLL;
    return PBPS_OK;
}
static short fnPBPS_GenRsp_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCGENERAL_RSP;
    return PBPS_OK;
}
static short fnPBPS_DcpDnl_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCAP_RULES;  
    //TODO JAH Remove dcaputilcomet.c return fnDCAPHandleDcpDnlSTag(pName,pAttrList,iNumAttr);
	return(0); //TODO JAH Remove dcaputilcomet.c 
}
short fnDCAPHandleDcpDnlETag( XmlChar *pName ) // TDT
{
   return XML_STATUS_OK;
}

static short fnPBPS_FileXferRsp_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCFILEXFER_RSP;
    return PBPS_OK;
}
static short fnPBPS_RootCertList_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_CERTLIST;    
    return PBPS_OK;
}
static short fnPBPS_AuditRsp_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCAUDIT_RSP;
    return PBPS_OK;
}
static short fnPBPS_AuthorizationInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCAUTH_REQ;
    return PBPS_OK;
}
static short fnPBPS_EDCInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCEDC_REQ;
    return PBPS_OK;
}
static short fnPBPS_EDCNonceReq_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCNONCE_REQ;
    return PBPS_OK;
}
static short fnPBPS_PostalConfigInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCCONFIG_REQ;
    return PBPS_OK;
}
static short fnPBPS_RefillRsp_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCREFILL_RSP;
    return PBPS_OK;
}
static short fnPBPS_RefundRsp_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCREFUND_RSP;
    return PBPS_OK;
}
static short fnPBPS_RekeyInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCREKEY_REQ;
    return PBPS_OK;
}
static short fnPBPS_SKeyRecInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DCSKR_REQ;
    return PBPS_OK;
}
static short fnPBPS_GenKeyExcKeyInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DC_GEN_KEY_EXCH_KEY;
    return PBPS_OK;
}
static short fnPBPS_LoadEncryptedKeyInd_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    bMessageTag = PBPS_DC_LOAD_SECRET_KEY;
    return PBPS_OK;
}

/* generic buffer management handlers */
static short fnPBPS_Generic_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    lwPBPTempBuffLen = 0;
    return PBPS_OK;
}
static short fnPBPS_Generic_Chars(XmlChar *pName, XmlChar *pChars, unsigned long iLen)
{
    if ((lwPBPTempBuffLen + (iLen*sizeof(XmlChar))) < (PBP_TEMP_BUFF_LEN - 1))
    {
        (void)memcpy( pPBPTemp + lwPBPTempBuffLen, 
                      (void*)pChars, 
                      iLen * sizeof(XmlChar) );
        lwPBPTempBuffLen += (SINT16)(iLen * (SINT16)sizeof(XmlChar) );
        return PBPS_OK;
    }
    else
        return XML_HANDLER_GENERAL_ERR;
}


/* We are detecting the PVD error here only for iButton. Don't want to break MYKO.
 May be possible to do the same thing for MYKO. But not sure.*/
static short fnPBPS_PsdRecPvdErr_Stag(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr)
{
    if( fnGetCurrentPsdType() & PSDT_ALL_IBUTTON )
    {
        fDCPvdErrorRecord = TRUE;
    }
    return PBPS_OK;
}
/* End */


void dbgMsg(char *msg, int status, int lineNo)
{
#ifdef JANUS
     if(status)
     {
        (void)sprintf( tmpBuf, "%s %d", msg, status );
        putString(tmpBuf, lineNo);
     }
#endif
}// dbgMsg()


void TestDCAPXMLFormat(void)
{
	int status;
    XMLFormatScript *pScript = NULL;
    int dataID = PBPS_DCAP_UPLOAD;

	pScript = (XMLFormatScript*)TheBigTable[dataID].handlerInfo;

	status = (int)XMLFormat(pScript,
	            fnPBPS_Formatter_Start,
	            fnPBPS_Formatter_End,
	            fnPBPS_Formatter_Char,
	            0);


}

