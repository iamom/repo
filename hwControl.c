//=====================================================================
//
//	FileName:	hwControl.c
//
//	Description: Hardware functions that used to be in hw7720.c
//		
//
//=====================================================================
#include "pbos.h"
#include "commontypes.h"
#include "hal.h"

extern void Shutdown_USB(void);

//TODO - Determine which functions are still needed and move those to HAL, then delete this file

//=====================================================================
//
//	Function Name:	fnTurnOffHwInterrupts
//
//	Description: Turn off any hardware interrupts
//		that may be enabled
//
//	Parameters: NONE
//		
//		
//		
//	Returns: NONE
//		
//
//	Preconditions:
//					
//
//=====================================================================
fnTurnOffHwInterrupts()
{
#if 0
  // Disable the priority encoders
  //Interrupt priority registers 
  IPRA  = 0x0000;
  IPRB  = 0x0000;
  IPRC  = 0x0000;
  IPRD  = 0x0000;
  IPRE  = 0x0000;
  IPRF  = 0x0000;
  IPRG  = 0x0000;
  IPRH  = 0x0000;
  IPRI  = 0x0000;
  IPRJ  = 0x0000;
  // Turn of interrupts from the USB Host controller
  // Also disables USB descriptor list processing
  USBHC = 0;
  //Turn off all sources of USBH interrupts
  USBHID = USBH_MIE |  USBH_OC | USBH_RHSC | USBH_FNO  | USBH_UE   | USBH_RD   | USBH_SF   | USBH_WDH  | USBH_SO;
  //Disable UART interrupts
  SCFER1 = 0;
  SCFER0 = 0;
  //Timers
  TMU_TCR0 = 0;
  TMU_TCR1 = 0;
  TMU_TCR2 = 0;
  //Compare Match Timer  
  CMCSR = 0;
  //IIC Interface
  ICIER = 0;
#endif
  HALTurnOffHwInterrupts();
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================
void RebootSystem()
{
//TODO - Do we want to log this event first?
	// DEBUG - Store syslog info
//	storeSyslogToFile(POWERDOWN_SYSLOG_DIR, "RebootSystem_Syslog.bin");



//	  fnTurnOffHwInterrupts();
//	  HALRebootSystem();
	  on_RebootReq(NULL, NULL);
}


//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================
char * ReturnLowVersion(void)
{
  return(HALGetLowVersion());
}

//=====================================================================
//
//	Function Name:	
//
//	Description:
//		
//
//	Parameters:
//		
//		
//		
//	Returns:
//		
//
//	Preconditions:
//					
//
//=====================================================================
char *SPReturnProtVersion(void)
{
  return(HALGetProtVersion());
}

//=====================================================================
//
//	Function Name:	fnGetHardwareVersion
//
//	Description: Get the major and minor versions of the hardware.
//      The first version of hardware for FPHX - initially designed to 
//      release the DM300c/400c systems had five minor versions/hardware 
//      main board respins: DE1, DE2, MT, MT2, MP.
//      The second version of hardware for FPHX - initially designed to 
//      release the DM475c system had four minor versions/hardware 
//      main board respins: DC, DE, MT, MP.
//      The main difference between the 1st and 2nd major versions:
//          addition of integrated LAN
//          addition of color display
//          addition of WOW/W1P/SBR functionality
//
//	Parameters: pointer to container to store major version (or a NULL ptr
//                  to not retrieve the major version)
//              pointer to container to store minor version(or a NULL ptr
//                  to not retrieve the minor version)
//		
//		
//	Returns: NONE
//		
//
//	Preconditions:
//					
//
//=====================================================================
void fnGetHardwareVersion(UINT8 *pMajorHwVer, UINT8 *pMinorHwVer)
{   
#if 0
    // DM300c/400c "old" hw only uses 3 least significant bits
    // DM475c "new" hw only uses 5 least significant bits

    // read the 3 least significant bits of the DIP switch register in the ASIC
    // 010 is an illegal combination for "old" hardware or "Major version 1" hardware
    if ( (0x07 & (UINT8) (*FP_HW_VERSION_ADDR)) == 0x02 )
    {
        if (pMajorHwVer != NULL_PTR)
            *pMajorHwVer = FP_MAIN_BD_HW_VERSION2;
        if (pMinorHwVer != NULL_PTR)
            *pMinorHwVer = 0x1F & (UINT8)(*FP_HW_VERSION_ADDR);    // only interested in 5 least significant bits!
    }
    else
    {
        if (pMajorHwVer != NULL_PTR)
            *pMajorHwVer = FP_MAIN_BD_HW_VERSION1;
        if (pMinorHwVer != NULL_PTR)
            *pMinorHwVer = 0x07 & (UINT8)(*FP_HW_VERSION_ADDR);    // only interested in 3 least significant bits!
    }
#endif
    if (pMajorHwVer != NULL_PTR)
    {
    	*pMajorHwVer = HALGetMajorHardwareVersion();
    }
    if (pMinorHwVer != NULL_PTR)
    {
    	*pMinorHwVer = HALGetMinorHardwareVersion();
    }
}

/* **********************************************************************
// FUNCTION NAME: fnToggleLED0
// PURPOSE: Toggles LED0 on the main board
//
// AUTHOR: George Monroe
//
// INPUTS: NONE
// RETURNS: NONE
// **********************************************************************/
void  fnToggleLED0()
{
	HALToggleFPGALED(0);
}

/* **********************************************************************
// FUNCTION NAME: fnToggleLED1
// PURPOSE: Toggles LED1 on the main board
//
// AUTHOR: George Monroe
//
// INPUTS: NONE
// RETURNS: NONE
// **********************************************************************/
void  fnToggleLED1()
{
	HALToggleFPGALED(1);
}

#ifdef REVECTOR

const unsigned long lRFDOffset __attribute__ ((section ("fpappflashheader"))) = 0;
const unsigned long lRFDSize __attribute__ ((section ("fpappflashheader"))) = 0;
const unsigned long lRFDChecksum __attribute__ ((section ("fpappflashheader"))) = 0;

#endif
