/************************************************************************
*   PROJECT:        Horizon CSD
*   COMPANY:        Pitney Bowes
*   MODULE NAME:    tskutil.c
*
*   DESCRIPTION:    Utility functions to support the interface between
                  the operating system and the higher level application.
*
* ----------------------------------------------------------------------
*               Copyright (c) 2016 Pitney Bowes Inc.
*                        37 Executive Dr
*                        Danbury, CT 06810
 30-Nov-15 sa002pe on FPHX 02.12 shelton branch
 	Merged in changes from the 02.13 branch:
----*
	 20-Nov-15 sa002pe on FPHX 02.13 shelton branch
		Changed fnLogIntertaskMsg, fnSystemLogEntry, fnIbuttonLogEntry & fnSystemLogEntryLong
		to also make sure lwIntertaskMsgLogIndex isn't set to overwrite entries in the header area.
----*
* ----------------------------------------------------------------------
*   MODIFICATION HISTORY:
*
*************************************************************************/

#include <stdlib.h>
#include <stdio.h>
//#include <stdarg.h>
#include <string.h>
#include "posix.h"
#include "services/stdiodflt.h"
#include "networking/nu_networking.h"

#include "pcdisk.h"
#include "bobutils.h"   // MEM_ACCESS, PERFORM_SCRIPT
#include "global.h"
#include "hal.h"
#include "mmcintf.h"
#include "nucleus.h"
#include "error_management.h"
#include "oit.h"        // OIT_SCREEN_TICK_OCCURRED
#include "ossetup.h"
#include "pb232.h"
#include "pbos.h"
#include "partition_memory.h"
#include "thread_control.h"
#include "utils.h"      // fnDisplayErrorDirect()
#include "lcd.h"
#include "sig.h"
#include "debugTask.h"
#include "flashutil.h"
#include "cometphc.h"
#include "counters/fram_log.h"
#include "trmdefs.h"

/* ******************************************************************** */
/* EXTERNAL DECLARATIONS                                                */
/* ******************************************************************** */
extern TASK_DEFINITION Tasks[];
extern unsigned char numberOfTasks;                   
extern QUEUE_DEFINITION Queues[];
extern SEMAPHORE_DEFINITION Semaphores[];
extern EVENT_GROUP_DEFINITION EventGroups[];
extern TIMER_DEFINITION Timers[];

extern NU_MEMORY_POOL    *pOSALMemPoolCB;
//extern unsigned long RTClockCount;
extern CMOS_Signature CMOSSignature;
extern BOOL stdio_enabled;
extern NLOG_ENTRY NLOG_Entry_List[NLOG_MAX_ENTRIES];

#define DBGTRACE_BUF_SIZE  		3500
typedef struct _structBuffArray
		{
			char Buff[DBGTRACE_BUF_SIZE+3];
		} StructBuffArray, *PStructBuffArray ;

//--------------------------------------------------
// From custdat.c:
//  Stuff for CMOSMiniSysLog:
extern UINT16   wCMOSMiniSyslogFlags;
extern UINT16   wCMOSMiniSyslogIndexGC;  // GreyCode Index to Next entry
extern INTERTASK_LOG_ENTRY pCMOSMiniSysLog[ CMOS_MINISYSLOG_SIZE ];   // INTERTASK_LOG_SIZE

//--------------------------------------------------
static   char dbgBuf[SYSLOG_MAX_DATA];
/* flight recorder stuff */
//#define CMOS_SYSLOG

#define MAX_UIC_HISR        15
#define MAX_HISR_NAME       11
static unsigned char    bMaxHisrAvailable = 0;

typedef struct
{
    unsigned long   lStackSize;
    unsigned long   *plStartingStackAddr;
    OS_HISR         *pHisrControlBlock;
    char            HisrName[MAX_HISR_NAME+1];
} HISR_DEBUG_DATA;

static HISR_DEBUG_DATA  HisrDataList[MAX_UIC_HISR];

//---------------------------------------------------------------------------
// Size of syslog header:    
// These entries, at the beginning, will only be written to once each power 
// cycle.   Set to a default value.  However it will be set to a more 
// meaningful value by the call to fnSystemLogResetHeader near the end of
// fnpwSystemInitialize. 
#define INITIAL_SYSLOG_HEADER_SIZE	14
static UINT32 ulIntertaskMsgHdrEnd = INITIAL_SYSLOG_HEADER_SIZE;

//----------------------------------------------------------------------------
// This structure is used in the table to filter out certain less-helpfull 
//  messages from the CMOSMiniSysLog.  

typedef struct 
{
    UINT8   bType;     // Type of Log Entry (intertask msg, event, etc
    UINT8   bSourceID;
    UINT8   bDestID;
    UINT8   bMsgID; 
}T_SYSLOG_FILTER;



#ifdef K700
#define INTERTASK_LOG_HEADER_SIZE   3
#else
#define INTERTASK_LOG_HEADER_SIZE   ulIntertaskMsgHdrEnd
#endif

INTERTASK_LOG_ENTRY pIntertaskMsgLog[INTERTASK_LOG_SIZE] __attribute__ ((section ("syslog")));



// Start at the beginning of the system log, so the header info gets 
//  written there on powerup.
unsigned long   ulHeaderIndex = 4;
unsigned long   lwIntertaskMsgLogIndex = INITIAL_SYSLOG_HEADER_SIZE;
unsigned char   pIntertaskLogMask[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};  
static BOOL     fMsgLoggingEnabled = TRUE;
static BOOL     fEventLoggingEnabled = TRUE;
static BOOL     fAddingSyslogEntry = FALSE;
static unsigned long    ulMissedSysLogs = 0;


static void fnLogIntertaskMsg(INTERTASK rIntertask, unsigned char bCall);

int currentLogLvl = DBG_LVL_INFO;

//--------------------------------------------------
// RAM Stuff for CMOSMiniSysLog:

UINT16      wMiniSyslogSeqIndex = 0;
BOOL        fMiniSyslogOff = FALSE;
//UINT8       bEarlyMiniSyslogIndex = 0;
//INTERTASK_LOG_ENTRY *fnEarlyMiniSyslogList[10];

// This is used to set the initial line to 0x11s on a power up.
static INTERTASK_LOG_ENTRY CMOSSyslogStarter;


//----------------------------------------------------------------------------
//   Global Constants:
//----------------------------------------------------------------------------
// For use as the CMOS index for the CMOSsyslog:
// This allows the index to never change by more than 1 bit at a time.

const UINT8 GreyCodeTable[ 256 ] = 
{
  0x00, 0x01, 0x03, 0x02, 0x06, 0x04, 0x05, 0x07, 
  0x0F, 0x0E, 0x0C, 0x0D, 0x09, 0x0B, 0x0A, 0x08,
  0x18, 0x1A, 0x1B, 0x19, 0x1D, 0x1C, 0x1E, 0x1F, 
  0x17, 0x15, 0x14, 0x16, 0x12, 0x13, 0x11, 0x10,
  0x30, 0x31, 0x33, 0x32, 0x36, 0x34, 0x35, 0x37, 
  0x3F, 0x3E, 0x3C, 0x3D, 0x39, 0x3B, 0x3A, 0x38,
  0x28, 0x2A, 0x2B, 0x29, 0x2D, 0x2C, 0x2E, 0x2F, 
  0x27, 0x25, 0x24, 0x26, 0x22, 0x23, 0x21, 0x20,
  0x60, 0x61, 0x63, 0x62, 0x66, 0x64, 0x65, 0x67, 
  0x6F, 0x6E, 0x6C, 0x6D, 0x69, 0x6B, 0x6A, 0x68,
  0x48, 0x4A, 0x4B, 0x49, 0x4D, 0x4C, 0x4E, 0x4F, 
  0x47, 0x45, 0x44, 0x46, 0x42, 0x43, 0x41, 0x40,
  0x50, 0x51, 0x53, 0x52, 0x56, 0x54, 0x55, 0x57, 
  0x5F, 0x5E, 0x5C, 0x5D, 0x59, 0x5B, 0x5A, 0x58,
  0x78, 0x7A, 0x7B, 0x79, 0x7D, 0x7C, 0x7E, 0x7F, 
  0x77, 0x75, 0x74, 0x76, 0x72, 0x73, 0x71, 0x70,
  0xF0, 0xF1, 0xF3, 0xF2, 0xF6, 0xF4, 0xF5, 0xF7, 
  0xFF, 0xFE, 0xFC, 0xFD, 0xF9, 0xFB, 0xFA, 0xF8,
  0xE8, 0xEA, 0xEB, 0xE9, 0xED, 0xEC, 0xEE, 0xEF, 
  0xE7, 0xE5, 0xE4, 0xE6, 0xE2, 0xE3, 0xE1, 0xE0,
  0xC0, 0xC1, 0xC3, 0xC2, 0xC6, 0xC4, 0xC5, 0xC7, 
  0xCF, 0xCE, 0xCC, 0xCD, 0xC9, 0xCB, 0xCA, 0xC8,
  0xD8, 0xDA, 0xDB, 0xD9, 0xDD, 0xDC, 0xDE, 0xDF, 
  0xD7, 0xD5, 0xD4, 0xD6, 0xD2, 0xD3, 0xD1, 0xD0,
  0x90, 0x91, 0x93, 0x92, 0x96, 0x94, 0x95, 0x97, 
  0x9F, 0x9E, 0x9C, 0x9D, 0x99, 0x9B, 0x9A, 0x98,
  0xB8, 0xBA, 0xBB, 0xB9, 0xBD, 0xBC, 0xBE, 0xBF, 
  0xB7, 0xB5, 0xB4, 0xB6, 0xB2, 0xB3, 0xB1, 0xB0,
  0xA0, 0xA1, 0xA3, 0xA2, 0xA6, 0xA4, 0xA5, 0xA7, 
  0xAF, 0xAE, 0xAC, 0xAD, 0xA9, 0xAB, 0xAA, 0xA8,
  0x88, 0x8A, 0x8B, 0x89, 0x8D, 0x8C, 0x8E, 0x8F, 
  0x87, 0x85, 0x84, 0x86, 0x82, 0x83, 0x81, 0x80,
};

#define MATCH_ANY   0xFF

//----------------------------------------------------------------------------
// This table is used to filter out certain less-helpfull messages from the 
//  CMOSMiniSysLog.  

// For now, filter out all the screen tick messages and the update display 
//  string messages.

const T_SYSLOG_FILTER pMiniSyslogFilterTbl[ ]=
{// bType,          bSourceID, bDestID,     bMsgID
//  { SYSLOG_ITM,     OIT,        OIT,        OIT_SCREEN_TICK_OCCURRED },
  { SYSLOG_ITM,     OIT,        LCDTASK,    LCD_DISPLAY_STRINGS },
};

//----------------------------------------------------------------------------


/* ***************************************************************************
// FUNCTION NAME:   fnConvertFromGreyCode
// PURPOSE: 
//      Converts a greycode index into the equivalent sequential index.      
// INPUTS:      16-bit greycode 
// OUTPUTS:     16-bit sequential index
// NOTES: 
//  1. Uses the Greycode table in this file.
//  2. Used for sensitive indices that are stored in CMOS, because a power
//     loss during a write to CMOS is less likely to destroy data if only
//     one bit is being changed.
// HISTORY: 
// AUTHOR:  Clarisa Bellamy 2012.11.05
// *************************************************************************/
UINT16 fnConvertFromGreyCode( UINT16 uwGreyIndex )
{
    UINT16  uwSeqIndex;
    UINT8   ubHiByte;
    UINT8   ubLoByte;
    int     iLoop1;
    int     iLoop2;

    ubHiByte = uwGreyIndex / 0x100;
    ubLoByte = uwGreyIndex % 0x100;
    for( iLoop1 = 0; iLoop1 < 256; iLoop1++ )
    {
        if( ubHiByte == GreyCodeTable[ iLoop1 ] )
            break;
    }

    if( iLoop1 % 2 )
    {
        // If the high byte is odd, then start from the back of the table
        for( iLoop2 = 0; iLoop2 < 256; iLoop2++ )
        {
            if( ubLoByte == GreyCodeTable[ 255 - iLoop2 ] )
                break;
        }
    }
    else
    {
        // If the high byte is even, then start from the start of the table
        for( iLoop2 = 0; iLoop2 < 256; iLoop2++ )
        {
            if( ubLoByte == GreyCodeTable[ iLoop2 ] )
                break;
        }
    }
	
    uwSeqIndex = (iLoop1 * 0x100) + iLoop2;
    return( uwSeqIndex );
}

/* ***************************************************************************
// FUNCTION NAME:   fnConvertToGreyCode
// PURPOSE: 
//      Converts a greycode index into the equivalent sequential index.      
// INPUTS:      16-bit greycode 
// OUTPUTS:     16-bit sequential index
// NOTES: 
//  1. Uses the Greycode table in this file.
//  2. Used for sensitive indices that are stored in CMOS, because a power
//     loss during a write to CMOS is less likely to destroy data if only
//     one bit is being changed.
// HISTORY: 
// AUTHOR:  Clarisa Bellamy 2012.11.05
// *************************************************************************/
UINT16 fnConvertToGreyCode( UINT16 uwSeqIndex )
{
    UINT16  uwGreyIndex;
    UINT16  uwHiByte;
    UINT16  uwLoByte;

    uwHiByte = uwSeqIndex / 0x100;
    uwLoByte = uwSeqIndex % 0x100;
    if( uwHiByte % 2 )
    {
        // For odd high bytes, low-byte starts at the end of the table:
        uwLoByte = GreyCodeTable[ 255 - uwLoByte ];
    }
    else 
    {
        // For odd high bytes, low-byte starts at the start of the table:
        uwLoByte = GreyCodeTable[ uwLoByte ];
    }

    uwHiByte = GreyCodeTable[ uwHiByte ];
    uwGreyIndex = (uwHiByte * 0x100) + uwLoByte;
    return( uwGreyIndex );
}

/* **********************************************************************
// FUNCTION NAME:       OSClearSysLog
// PURPOSE: 
//      Clears the System Log      
//
// INPUTS:  NONE
// OUTPUTS: NONE
// NOTES: 
//  1. The CMOSMiniSysLog is only initialized if the hardware is 475.
//  2. If the INIT flag is clear, init the whole log to 0xFFs, the indices
//     to 0, and then set the INIT flag.
//  3. If the INIT flag is set, convert the CMOS GreyCode index to the RAM 
//     sequential index, and add a line of 0x11 to denote a new powerup.
//
// HISTORY: 
//  2012.11.01 Clarisa Bellamy - Add initialization of the CMOSMiniSysLog 
//                  variables if the hardware is 475. 
// AUTHOR: George Monroe
// *************************************************************************/
void OSClearSysLog( void )
{
    UINT8   ubMajorHwVer;


	// Init the header entries to empty entries
	(void)memset(pIntertaskMsgLog, 0x00, (sizeof(INTERTASK_LOG_ENTRY) * ulIntertaskMsgHdrEnd));
	
	// put in the divider between the header entries and the rest of the log entries in the last header entry
    pIntertaskMsgLog[ulIntertaskMsgHdrEnd - 1].bType = SYSLOG_TEXT;
	pIntertaskMsgLog[ulIntertaskMsgHdrEnd - 1].lwTimeStamp = EndianSwap32(getMsClock());
    (void)memcpy(pIntertaskMsgLog[ulIntertaskMsgHdrEnd - 1].pUnionData, "-----------------------------------", 35);

	// fill in the rest of the entries w/ 0xFF
    (void)memset(&pIntertaskMsgLog[ulIntertaskMsgHdrEnd], 0xFF, (sizeof(INTERTASK_LOG_ENTRY) * (INTERTASK_LOG_SIZE - ulIntertaskMsgHdrEnd)));
	
	// set up the index for the first entry for the rest of the log.
	lwIntertaskMsgLogIndex = ulIntertaskMsgHdrEnd;

}


/* **********************************************************************
// FUNCTION NAME: OSSendIntertask
// PURPOSE:     Supports intertask messaging among UIC tasks
// AUTHOR: Wes Kirschner, Joe Mozdzer, Stolen from Documatch
//
// INPUTS:  bDestID - Destination task ID
//          bSourceID - Source task ID
//          bMsgID - Message identifier
//          bMessageType - Format of the message data union
//          pMsgBuf - Pointer to message data
//          sSize - Size of the message data
//          
// *************************************************************************/
BOOL  OSSendIntertask (unsigned char bDestID, unsigned char bSourceID, unsigned char bMsgID, 
                        unsigned char bMessageType, void *pMsgBuf, unsigned long lwSize)
{
   INTERTASK rMessage ;
   int     status;


   rMessage.bSource = bSourceID ;
   rMessage.bDest = bDestID;
   rMessage.bMsgId = bMsgID ;
   rMessage.bMsgType = bMessageType;
   
   if ((bMessageType == PART_PTR_DATA) || (bMessageType == GLOBAL_PTR_DATA))
   {   
      rMessage.IntertaskUnion.PointerData.pData = pMsgBuf ;
      rMessage.IntertaskUnion.PointerData.lwLength = lwSize ;
   }
   /* COMM CHANGE */
   else
   {
    if (bMessageType == COMM_DATA || ( bMsgID == COMM_REVECTOR_DEVICE && bMessageType != PART_PTR_DATA))
    {
       if (lwSize < MAX_INTERTASK_BYTES)
       {
          rMessage.IntertaskUnion.CommData.bLength = (unsigned char) lwSize;
          memcpy (rMessage.IntertaskUnion.CommData.bByteData, pMsgBuf, lwSize) ;
       }
       else
       {
          rMessage.bMsgType = PART_PTR_DATA ;
          if (OSGetMemory ((void **) &rMessage.IntertaskUnion.PointerData.pData, lwSize) == FAILURE)
          {
        	    return ((BOOL) FAILURE) ;
          }
          memcpy (rMessage.IntertaskUnion.PointerData.pData, pMsgBuf, lwSize) ;
          rMessage.IntertaskUnion.PointerData.lwLength = lwSize ;
       }
     }   
     else
     {
       if (bMessageType == COMM_RECEIVE_DATA)
       {
          rMessage.IntertaskUnion.PointerData.pData = pMsgBuf ;
          rMessage.IntertaskUnion.PointerData.lwLength = lwSize ;
       }   
       else
       {
          if( lwSize <= MAX_INTERTASK_BYTES )
          {
            memcpy (rMessage.IntertaskUnion.bByteData, pMsgBuf, lwSize) ;
          }  
          else
          {
            memcpy (rMessage.IntertaskUnion.bByteData, pMsgBuf, MAX_INTERTASK_BYTES) ;
          }
        }
      }
    }
   // Added for later resend if communication error occurs H.Sun
   // memcpy(cmStatus.tPmMsg, rMessage, sizeof(INTERTASK));

   fnLogIntertaskMsg(rMessage, INTERTASK_NORMAL_CALL);
   status = NU_Send_To_Queue (Queues[bDestID].pQueue, (unsigned long *) &rMessage, INTERTASK_MSG_SIZE, NU_NO_SUSPEND);
   if ( status == NU_SUCCESS)
   {
      return ((BOOL) SUCCESSFUL) ;
   }
   else
   {
      sprintf( dbgBuf, "OSSendIntertask Error %d", status );
      fnSystemLogEntry(SYSLOG_TEXT, dbgBuf, strlen(dbgBuf));
      sprintf( dbgBuf, "  Src=%d,Dest=%d,MsgID=%d", (int)bSourceID, (int)bDestID, (int)bMsgID );
      fnDumpStringToSystemLog( dbgBuf );
      return ((BOOL) FAILURE) ;
   }
}

/* **********************************************************************
// FUNCTION NAME: OSReceiveIntertask
// PURPOSE: Supports receipt of an intertask message
// AUTHOR: Joe Mozdzer, Wes Kirschner, port from Documatch
//
// INPUTS:  bQueueID - ID of queue to receive the message from
//          prMsg - pointer to intertask structure in which to store message
//          lwSuspend - suspension option:
//                      OS_SUSPEND - suspend calling task until a message
//                                      becomes available.
//                      OS_NO_SUSPEND - do not suspend if a message is not
//                                      available
//                      any other value
//                       from 1 to 0xFFFFFFFE : number of milliseconds to
//                                              stay suspended while waiting
//                                              for a message.  subject to
//                                              granularity of the system
//                                              timer tick.
//
// RETURNS: SUCCESSFUL if message received successfully
//          OS_MESSAGE_NOT_AVAILABLE if no message available and OS_NO_SUSPEND
//              was used as the suspension option
//          OS_TIMEOUT if message was still not available after suspending
//              for the specified number of milliseconds (not possible if
//              lwSuspend is set to OS_SUSPEND or OS_NO_SUSPEND)
//          FAILURE otherwise               
// ************************************************************************ */
char OSReceiveIntertask (unsigned char bQueueID, INTERTASK *prMsg, unsigned long lwSuspend)
{
    STATUS status;
    unsigned long lwSizeReceived;
    unsigned long lwSuspensionTicks;


    if (lwSuspend == OS_SUSPEND)
        lwSuspensionTicks = NU_SUSPEND;
    else if (lwSuspend == OS_NO_SUSPEND)
        lwSuspensionTicks = NU_NO_SUSPEND;
    else
        lwSuspensionTicks = lwSuspend / MILLISECONDS_PER_TICK;

    status = NU_Receive_From_Queue (Queues[bQueueID].pQueue, (void *) prMsg,
                INTERTASK_MSG_SIZE, &lwSizeReceived, lwSuspensionTicks);

    if ((status == NU_SUCCESS) && (lwSizeReceived == INTERTASK_MSG_SIZE))
        return ((char) SUCCESSFUL);
    else if ((status == NU_QUEUE_EMPTY) && (lwSuspend == OS_NO_SUSPEND))
        return ((char) OS_MESSAGE_NOT_AVAILABLE);
    else if (status == NU_TIMEOUT)
        return ((char) OS_TIMEOUT);
    else
        return ((char) FAILURE); 
}


/* **********************************************************************
// FUNCTION NAME: OSWakeAfter
// PURPOSE:  Temporarily suspend calling task for the specified time
//           interval.
//           
// AUTHOR: Joe Mozdzer
// 
// INPUTS:  ulMilliseconds - Number of milliseconds to temporarily 
//          suspend.  Note that the level of granularity is governed
//          by the system timer tick setup.  Any deviation from the 
//          granularity will result in rounding to the next lowest
//          multiple of the timer tick interval. 
//          Example:  OSWakeAfter(549) will result in 540ms suspension 
//          if the timer tick interval is 10ms.
// *********************************************************************** */
void OSWakeAfter (unsigned long lwMilliseconds)
{
    if (lwMilliseconds/MILLISECONDS_PER_TICK)
        NU_Sleep ((UNSIGNED) lwMilliseconds/MILLISECONDS_PER_TICK);
    else
        /* If the number of milliseconds specified ends up translating to
            zero timer ticks, call the relinquish function instead of the
            sleep function to reduce overhead.  Any other task of equal
            priority will get a chance to run before the calling task gets
            control again. */
        NU_Relinquish();

}


/* **********************************************************************
// FUNCTION NAME: OSSuspendTask
// PURPOSE: Suspends the selected task
// AUTHOR: Wes Kirschner, Joe Mozdzer, port from Documatch
//
// INPUTS:  bTaskID - ID of task to be suspended
// ********************************************************************** */
BOOL OSSuspendTask (unsigned char bTaskID)
{          
    STATUS status;

    status = NU_Suspend_Task( Tasks[bTaskID].pControlBlock );

    if (status != NU_SUCCESS)
        return ((BOOL) FAILURE);
    else
        return ((BOOL) SUCCESSFUL);
}

/* **********************************************************************
// FUNCTION NAME: OSResumeTask
// PURPOSE: Resumes the selected task
// AUTHOR: Wes Kirschner, Joe Mozdzer, port from Documatch
//
// INPUTS:  bTaskID - ID of task to be resumed
// *********************************************************************** */
BOOL OSResumeTask (unsigned char bTaskID)
{
    STATUS status;

    status = NU_Resume_Task( Tasks[bTaskID].pControlBlock );
    
    if (status != NU_SUCCESS)
        return ((BOOL) FAILURE);
    else
        return ((BOOL) SUCCESSFUL);
}

/* ************************************************************************
// MODULE     : OSGetCurrentTask
// AUTHOR     : Joe Mozdzer, Barbara Poscich
// DESCRIPTION: Returns the task ID of the currently executing task thread.        
// INPUTS     : None
// RETURNS    : The ID of the task that is currently running.  If a task is
//              not the active thread, OS_NO_TASK is returned.
// ***********************************************************************/
unsigned char OSGetCurrentTask(void)
{
    short           i;                                   
    NU_TASK         *pTask;


    /*Get Current Task Pointer */
    pTask=NU_Current_Task_Pointer(); 

    /* if Nucleus NULL returned, a task is not the active thread */
    if (pTask == NU_NULL)
        return (OS_NO_TASK);
    else
    {
        /*Compare the pControlBlock of Tasks with the given task ptr */
        for (i=0;i<numberOfTasks;i++)
        {    
            /* Found a match */
            if(Tasks[i].pControlBlock == pTask)
                return ((unsigned char) i);
        }
    }

    /*If we got here, the current task pointer wasn't found in our table of tasks.  This
        is a strange situation, so show an error message and freeze. */
//  fnDisplayErrorDirect("No match with task ptr was found in tasks array",0,0xFF);
//freeze_here:
//   goto freeze_here;      
                                                          
    return(OS_UNKNOWN_TASK);
}

/* ************************************************************************
// MODULE     :             OSGetCurrentHisr
// DESCRIPTION: 
//      Returns the Index into HisrDataList of the currently executing HISR.
// INPUTS     : 
//      None
// RETURNS    : 
//      UINT8 - Index into HisrDataList of currently executing HISR.
//              If no HISR currently executing, returns OS_NO_HISR (0xFF).
//              If HISR executing, but it is not in the list, return 
//              OS_UNKNOWN_HISR( 0xFE ).
// NOTES:
// HISTORY:
//  2009.10.22  Clarisa Bellamy - Initial revision.
// ***********************************************************************/
UINT8 OSGetCurrentHisr( void )
{
    short           i;                                   
    OS_HISR         *pHisr;


    /*Get Current Hisr Pointer */
    pHisr = (OS_HISR *)NU_Current_HISR_Pointer(); 

    /* if Nucleus NULL returned, a task is not the active thread */
    if( pHisr == NU_NULL )
        return( OS_NO_HISR );
    else
    {
        /*Compare the pControlBlock of Tasks with the given task ptr */
        for( i = 0; i < bMaxHisrAvailable; i++ )
        {    
            /* Found a match */
            if( HisrDataList[i].pHisrControlBlock == pHisr )
                return( (UINT8)i );
        }
    }

    // If we got here, the current HISR pointer wasn't found in our table of HISRs.  This
    //  is probably a problem, and an entry should be made in the system log, but we 
    //  didn't do that for the Task version of this function, so we won't here either.
    return( OS_UNKNOWN_HISR );
}

// ************************************************************************
// MODULE     :             OSGetCurrentHisrName
// DESCRIPTION: 
//      Returns pointer to the name in the local Hisr table of the currently 
//      executing HISR.
// INPUTS     : 
//      None
// RETURNS    : 
//      * char - Index into HisrDataList of currently executing HISR.
//              If no HISR currently executing, or current HISR is not in table
//               returns NULL_PTR.
// NOTES:
// HISTORY:
//  2009.10.22  Clarisa Bellamy - Initial revision.
// ----------------------------------------------------------------------------
char *OSGetCurrentHisrName( void )
{
    UINT8   pHisrID;
    char   *pHisrName = NULL_PTR;

    pHisrID = OSGetCurrentHisr();
    if( pHisrID < bMaxHisrAvailable )
    {
        pHisrName = HisrDataList[pHisrID].HisrName;
    }
    
    return( pHisrName );
}


/* **********************************************************************
// FUNCTION NAME: OSSetEvents
// PURPOSE: Modifes the contents of an event group
// AUTHOR: Joe Mozdzer
//
// INPUTS:  bEventID - ID of event group to modify
//          lwEvents - Event flag values to use
//          sOperation - logical operation to perform on the event group:
//                      OS_AND - Logically AND's the event group with the
//                                  value of lwEvents
//                      OS_OR - Logically OR's the event group with the 
//                              value of lwEvents
//
// ************************************************************************ */
BOOL OSSetEvents (unsigned char bEventId, unsigned long lwEvents, short sOperation)
{
    STATUS status;

    if (fEventLoggingEnabled)   
    {
        fnSystemLogEntryLong( SYSLOG_EVENT, bEventId, 1, 0, lwEvents, (UINT32)sOperation,
                              0, (UINT32)OSGetCurrentTask(), 0, 0, 0, 0, 0, 0);
    }

    status = NU_Set_Events(EventGroups[bEventId].pEventGroup, lwEvents, 
                            sOperation ? (OPTION) NU_OR : (OPTION) NU_AND);
    if(status == NU_SUCCESS)
    {
        return ((BOOL) SUCCESSFUL) ;
    }
    else
    {
        sprintf(dbgBuf, "OSSetEvents Error %d   ", status);
        fnSystemLogEntry(SYSLOG_TEXT, dbgBuf, 30);
        return ((BOOL) FAILURE) ;
    }
}

/* **********************************************************************
// FUNCTION NAME: OSSetIrqVector
// PURPOSE: Modifes the contents of an event group
// AUTHOR: George Monroe
// BACKSEAT CODER Wes K
// INPUTS:  Vector Number - Interrupt Vector Number
//      handler - address of function that will handle this vector
//
// ************************************************************************ */

BOOL OSSetIrqVector ( unsigned long vectorNum , void (*handler)(int) )
{
  void (*old_handler)(int);

    if (NU_Register_LISR( vectorNum , handler , &old_handler ) == NU_SUCCESS)
      return ((BOOL) SUCCESSFUL) ;
    else
      return ((BOOL) FAILURE) ;
}

/* **********************************************************************
// FUNCTION NAME: OSReceiveEvents
// PURPOSE: Checks/retrieves the contents of an event group.  Allows for suspension
//          of the calling task until a target set of events are satisfied.
// AUTHOR: Joe Mozdzer
//
// INPUTS:  bEventID - ID of event group
//          lwRequestedEvents - The requested event flags.  A set bit indicates
//                              the corresponding event flag is requested.
//          lwSuspend - suspension option:
//                      OS_SUSPEND - suspend calling task until the requested
//                                      event combination is available
//                      OS_NO_SUSPEND - do not suspend if not available
//
//                      any other value
//                       from 1 to 0xFFFFFFFE : number of milliseconds to
//                                              stay suspended while waiting
//                                              for the event combination.  
//                                              subject to the granularity 
//                                              of the system timer tick.
//
//
//          sOperation - OS_AND - all requested event flags are required to
//                                  satisfy the request
//                       OS_OR - one or more of the requested event flags are
//                               sufficient to satisfy the request
//          sOperation - OS_AND_CONSUME - all requested event flags are required to
//                                  satisfy the request and the event flags are cleared
//                       OS_OR_CONSUME - one or more of the requested event flags are
//                               sufficient to satisfy the request and the event flags are cleared
//
//          pReceivedEvents- the actual contents of the event group are 
//                              returned at this address.  note that this
//                              is VALID ONLY WHEN SUCCESSFUL is returned.
//
// RETURNS: SUCCESSFUL if event flags match those requested in accordance
//              with sOperation
//          OS_NO_EVENT_MATCH if the event flags present in the event group
//              do not match those requested and OS_NO_SUSPEND was used as
//              the suspension option
//          OS_TIMEOUT if requested event flag combination was still not available
//              after suspending for the specified number of milliseconds (not 
//              possible if lwSuspend is set to either OS_SUSPEND or OS_NO_SUSPEND)
//          FAILURE otherwise               
// ************************************************************************ */
char OSReceiveEvents (unsigned char bEventID, unsigned long lwRequestedEvents,
                        unsigned long lwSuspend, short sOperation, unsigned long *pReceivedEvents)
{
    STATUS  status;
    unsigned long lwSuspensionTicks;
    OPTION  Operation;

    if (lwSuspend == OS_SUSPEND)
        lwSuspensionTicks = NU_SUSPEND;
    else if (lwSuspend == OS_NO_SUSPEND)
        lwSuspensionTicks = NU_NO_SUSPEND;
    else
        lwSuspensionTicks = lwSuspend / MILLISECONDS_PER_TICK;
    
    /* Default to NU_AND if not specified*/
    switch (sOperation) {
        case OS_OR          : Operation = (OPTION) NU_OR; break;
        case OS_AND_CONSUME : Operation = (OPTION) NU_AND_CONSUME; break;
        case OS_OR_CONSUME  : Operation = (OPTION) NU_OR_CONSUME; break;
        case OS_AND         : 
        default             : Operation = (OPTION) NU_AND; break;
    }
	
    status = NU_Retrieve_Events(EventGroups[bEventID].pEventGroup, lwRequestedEvents, 
                                Operation,  pReceivedEvents, lwSuspensionTicks);
    if (status == NU_SUCCESS)
    {
        if (fEventLoggingEnabled)   
        {
            fnSystemLogEntryLong(SYSLOG_EVENT, bEventID, 2, 0, lwRequestedEvents, (ulong)sOperation,
                                 *pReceivedEvents, (UINT32)OSGetCurrentTask(), 0, 0, 0, 0, 0, 0);
        }
        return ((char) SUCCESSFUL);
    }
    else
    {
        if ((status == NU_NOT_PRESENT) && (lwSuspend == OS_NO_SUSPEND))
            return ((char) OS_NO_EVENT_MATCH);
        else if (status == NU_TIMEOUT)
            return ((char) OS_TIMEOUT);
        else
        {
            sprintf(dbgBuf, "OSReceiveEvents Error %d   ", status);
            fnSystemLogEntry(SYSLOG_TEXT, dbgBuf, 30);
            return ((char) FAILURE);
        }
    }
}


/* **********************************************************************
// FUNCTION NAME: OSStartTimer
// PURPOSE: Starts a pre-created timer
// AUTHOR: Joe Mozdzer
//
// INPUTS:  bTimerID - ID of timer to start
// ************************************************************************ */
BOOL OSStartTimer (unsigned char bTimerID)
{
#ifdef LOG_OS_TIMER_START_STOP
    char pBuff[ 75 ];
    sprintf( pBuff, "TIMER: Start [%3d]%s", bTimerID, timerIDToNameString(bTimerID));
    fnDumpStringToSystemLog( pBuff );
#endif

    if (NU_Control_Timer(Timers[bTimerID].pTimer, NU_ENABLE_TIMER) == NU_SUCCESS)
        return ((BOOL) SUCCESSFUL);
    else
        return ((BOOL) FAILURE);
}

/* **********************************************************************
// FUNCTION NAME: OSStopTimer
// PURPOSE: Stops a pre-created timer.  If the timer is restarted later, it
//          will run for the full timer period.  For example, say we have
//          a 3 second timer:
//          1)  Timer is started w/ OSStartTimer
//          2)  2 seconds elapse.
//          3)  Timer is stopped w/ OSStopTimer
//          4)  Timer is re-started w/ OSStartTimer
//          It will take 3 seconds for the timer to expire, not 1.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  bTimerID - ID of timer to stop
// ************************************************************************ */
BOOL OSStopTimer (unsigned char bTimerID)
{
#ifdef LOG_OS_TIMER_START_STOP
    char pBuff[ 75 ];
    sprintf( pBuff, "TIMER: Stop [%3d]%s", bTimerID, timerIDToNameString(bTimerID));
    fnDumpStringToSystemLog( pBuff );
#endif

    if (NU_Control_Timer(Timers[bTimerID].pTimer, NU_DISABLE_TIMER) == NU_SUCCESS)
        return ((BOOL) SUCCESSFUL);
    else
        return ((BOOL) FAILURE);
}

/* **********************************************************************
// FUNCTION NAME: OSChangeTimerDuration
// PURPOSE: Changes the expiration time of a timer.  If the timer is running
//          when this function is called, it will be stopped.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  bTimerID - ID of timer to stop
// ************************************************************************ */
BOOL OSChangeTimerDuration (unsigned char bTimerID, unsigned long lwNewMilliseconds)
{
    unsigned long   lwNewTicks;
    STATUS          errCode;
    char            errBuf[60];

    lwNewTicks = lwNewMilliseconds / MILLISECONDS_PER_TICK;

    errCode = NU_Control_Timer(Timers[bTimerID].pTimer, NU_DISABLE_TIMER);
    if(errCode != NU_SUCCESS) 
    {
        sprintf(errBuf, "NU_Control_Timer 0x%02X %ldmS Err 0x%X", bTimerID, lwNewMilliseconds, errCode);
        fnDisplayErrorDirect(errBuf, (long)bTimerID, OSGetCurrentTask());
    }

    errCode = NU_Reset_Timer(Timers[bTimerID].pTimer, Timers[bTimerID].pExpirationRoutine,
            lwNewTicks, lwNewTicks, NU_DISABLE_TIMER);
    if(errCode != NU_SUCCESS) 
    {
        sprintf(errBuf, "NU_Reset_Timer Err 0x%02X %ldmS Err 0x%X", bTimerID, lwNewMilliseconds, errCode);
        fnDisplayErrorDirect(errBuf, (long)bTimerID, OSGetCurrentTask());
    }
	
    return ((BOOL) SUCCESSFUL);
}


/* **********************************************************************
// FUNCTION NAME: OSChangeTimerDurationByTicks
// PURPOSE: Changes the expiration time of a timer by ticks.  If the timer is running
//          when this function is called, it will be stopped.
//
// AUTHOR: Raymond Shen
//
// INPUTS:  bTimerID - ID of timer to stop
// ************************************************************************ */
BOOL OSChangeTimerDurationByTicks (unsigned char bTimerID, unsigned long lwNewTicks)
{
    STATUS          errCode;
    char            errBuf[60];

    errCode = NU_Control_Timer(Timers[bTimerID].pTimer, NU_DISABLE_TIMER);
    if(errCode != NU_SUCCESS) 
    {
        sprintf(errBuf, "NU_Control_Timer 0x%02X %ldmS Err 0x%X", bTimerID, lwNewTicks, errCode);
        fnDisplayErrorDirect(errBuf, (long)bTimerID, OSGetCurrentTask());
    }

    errCode = NU_Reset_Timer(Timers[bTimerID].pTimer, Timers[bTimerID].pExpirationRoutine,
            lwNewTicks, lwNewTicks, NU_DISABLE_TIMER);
    if(errCode != NU_SUCCESS) 
    {
        sprintf(errBuf, "NU_Reset_Timer Err 0x%02X %ldmS Err 0x%X", bTimerID, lwNewTicks, errCode);
        fnDisplayErrorDirect(errBuf, (long)bTimerID, OSGetCurrentTask());
    }
	
    return ((BOOL) SUCCESSFUL);
}

/* **********************************************************************
// FUNCTION NAME: OSAcquireSemaphore
// PURPOSE: Function to aquire a Nucleus semaphore.
// AUTHOR:  Joe Mozdzer
// INPUTS:  bSemaphoreID - ID of semaphore to acquire
//          lwSuspend - suspension option:
//                      OS_SUSPEND - suspend calling task until the requested
//                                      semaphore is available
//                      OS_NO_SUSPEND - do not suspend if not available
//
//                      any other value
//                       from 1 to 0xFFFFFFFE : number of milliseconds to
//                                              stay suspended while waiting
//                                              for the semaphore. 
//                                              subject to the granularity 
//                                              of the system timer tick.
//
// RETURNS: SUCCESSFUL if semaphore is acquired
//          OS_SEMAPHORE_NOT_AVAILABLE if the semaphore is not available
//              and the suspension option was OS_NO_SUSPEND
//          OS_TIMEOUT if the semaphore did not become available even after
//              waiting for the specified number of milliseconds (this is
//              only possible if lwSuspend is not set to OS_SUSPEND or 
//              OS_NO_SUSPEND)
//          FAILURE otherwise
// ************************************************************************ */
char OSAcquireSemaphore (unsigned char bSemaphoreID, unsigned long lwSuspend)
{
    STATUS  status;
    unsigned long lwSuspensionTicks;


    if (lwSuspend == OS_SUSPEND)
        lwSuspensionTicks = NU_SUSPEND;
    else if (lwSuspend == OS_NO_SUSPEND)
        lwSuspensionTicks = NU_NO_SUSPEND;
    else
        lwSuspensionTicks = lwSuspend / MILLISECONDS_PER_TICK;

    status = NU_Obtain_Semaphore (Semaphores[bSemaphoreID].pSemaphore, 
                                    lwSuspensionTicks);
    
    if (status == NU_SUCCESS)
        return ((BOOL) SUCCESSFUL);
    else if ((status == NU_UNAVAILABLE) && (lwSuspend == OS_NO_SUSPEND))
        return ((char) OS_SEMAPHORE_NOT_AVAILABLE);
    else if (status == NU_TIMEOUT)
        return ((char) OS_TIMEOUT);
    else
        return ((char) FAILURE);
}


/* **********************************************************************
// FUNCTION NAME: OSReleaseSemaphore
// PURPOSE: Function to release a Nucleus semaphore.
// AUTHOR:  W. Kirschner
// INPUTS:  bSemaphoreID - ID of semaphore to release
// ************************************************************************ */
char OSReleaseSemaphore (unsigned char bSemaphoreID)
{
	STATUS semstat;
	UNSIGNED count;

	// Assume semaphore is created if ID is valid
	if (bSemaphoreID >= MAX_UIC_SEMAPHORES)
		return ((char) OS_INVALID_SEMAPHORE);

	semstat = NU_Semaphore_Information(Semaphores[bSemaphoreID].pSemaphore, NULL, &count, NULL, NULL, NULL);
   if ((semstat == NU_SUCCESS) && (count == Semaphores[bSemaphoreID].lwInitialCount))
	   {// Trying to release semaphore we don't have
			return ((char) OS_DONT_OWN_SEMAPHORE);
	   }

   if (NU_Release_Semaphore (Semaphores[bSemaphoreID].pSemaphore) == NU_SUCCESS)
      return ((char) SUCCESSFUL) ;
   else
      return ((char) FAILURE) ;
}

/* **********************************************************************
// FUNCTION NAME: OSGetSemaphoreState
// PURPOSE: Function to determine if a Nucleus semaphore is acquired
// INPUTS:  bSemaphoreID - ID of semaphore
// ************************************************************************ */
char OSGetSemaphoreState (unsigned char bSemaphoreID)
{
	STATUS semstat;
	char semname[8];
	UNSIGNED count;
	OPTION suspendType;
	UNSIGNED tasks_waiting;
	NU_TASK *first_task;

	// Assume semaphore is created if ID is valid
	if (bSemaphoreID >= MAX_UIC_SEMAPHORES)
		return ((char) OS_INVALID_SEMAPHORE);

	semstat = NU_Semaphore_Information(Semaphores[bSemaphoreID].pSemaphore, &semname[0], &count, &suspendType, &tasks_waiting, &first_task);
   if (semstat == NU_SUCCESS)
   {
	   if (count == 0)
		   return ((char) OS_SEMAPHORE_NOT_AVAILABLE) ;
	   else if (count > Semaphores[bSemaphoreID].lwInitialCount)
			return ((char) OS_SEMAPHORE_ABOVE_MAX);
	   else
		   return ((char) OS_SEMAPHORE_AVAILABLE) ;
   }
   else
   {
      return ((char) FAILURE) ;
   }
}

/* ************************************************************************
// MODULE     : OSGetMemory
// AUTHOR     : Joe Mozdzer
// DESCRIPTION: Requests dynamic allocation of a memory buffer 
// INPUTS     : pAddr - address of void pointer which will point to the buffer
//              lwSize - size of the buffer being requested
//
// OUTPUTS    : *pAddr - will point to a buffer of lwSize bytes if the
//                       function returns SUCCESSFUL
//SE********************************************************************** */
//TODO - There used to be G900 specific memory address min and max validation in here.
// The results of the validation were ignored so this code was removed but if Nucleus
// starts returning bad memory to callers, then this checking (CSD specific) may be reinstated.
//
//NOTE - In case of memory fragmentation, we can go back to using a couple of NU_MEMORY_PARTITIONS
//       of different sized (G900 used to have 128 and 1024 sized partition and allocate from
//       the apropriate one. We do need to use a trick in the OSReleaseMemory and use the start
//       and end address of each memory partition to figure out which one to call to do the 
//       release.
BOOL OSGetMemory (void **pAddr, unsigned long lwSize)
{
  BOOL retStatus = FAILURE;
  
   *pAddr = NULL;
   if (NU_Allocate_Memory(pOSALMemPoolCB, pAddr, (unsigned) lwSize, NU_NO_SUSPEND) == NU_SUCCESS)
   {
	   retStatus = SUCCESSFUL;
   }
	 
   return (retStatus) ;
}

/* ************************************************************************
// MODULE     : OSReleaseMemory
// AUTHOR     : Wes Kirschner, Joe Mozdzer
// DESCRIPTION: Releases a memory buffer
// INPUTS     : pAddr - pointer to block being released
********************************************************************** */
BOOL OSReleaseMemory (void *pAddr)
{
	unsigned long lwAddr = ( unsigned long ) pAddr;
	BOOL fStatus = FAILURE;

	if (lwAddr && ((lwAddr & 0x03 ) == 0 ))
    {
		if (NU_Deallocate_Memory (pAddr) == NU_SUCCESS)
		{
			fStatus = SUCCESSFUL;
		}
    }
	
    return (fStatus) ;
}

/* ************************************************************************
// MODULE     : OSCreateHISR
// AUTHOR     : George Monroe
// DESCRIPTION: Create a High Level Interrupt Service Routine
// INPUTS     : hisr - pointer to NU_Hisr Structure
//                      handler - address of Hisr Handler
//                      stacksize - size of Hisr Handler Stack
//                      name   - pointer to character string for naming this Hisr
//SE********************************************************************** */
int OSCreateHISR( OS_HISR *hisr , void (*handler)(void), unsigned long stacksize , char *name )
{
  int iStatus;
  unsigned long *Stack;


  Stack = (unsigned long *)malloc(stacksize);
  if(Stack)
    {
        memset((unsigned char *)Stack, 0xAA, stacksize);
        iStatus = NU_Create_HISR( (NU_HISR *) (hisr) , name , handler , 0 , Stack , stacksize );
        if(iStatus == NU_SUCCESS )
        {
            iStatus = SUCCESSFUL;
            if (bMaxHisrAvailable < MAX_UIC_HISR)
            {
                if (bMaxHisrAvailable == 0)
                {
                    memset((unsigned char *)HisrDataList, 0, sizeof(HisrDataList));
                }

                HisrDataList[bMaxHisrAvailable].lStackSize = stacksize;
                HisrDataList[bMaxHisrAvailable].plStartingStackAddr = Stack;
                HisrDataList[bMaxHisrAvailable].pHisrControlBlock = hisr;
                if (strlen(name) > MAX_HISR_NAME)
                    strncpy(HisrDataList[bMaxHisrAvailable].HisrName, name, MAX_HISR_NAME);
                else
                    strcpy(HisrDataList[bMaxHisrAvailable].HisrName, name);

                HisrDataList[bMaxHisrAvailable].HisrName[MAX_HISR_NAME] = 0;
                bMaxHisrAvailable++;
            }
        }
        else
            iStatus = FAILURE;
    }
    else
    {
        iStatus = FAILURE;
    }
	
  return(iStatus);
}

/* ************************************************************************
// MODULE     : OSActivateHISR
// AUTHOR     : George Monroe
// DESCRIPTION: Activate a High Level Interrupt Service Routine
// INPUTS     : hisr - pointer to OS_Hisr Structure
//SE********************************************************************** */
unsigned long OSActivateHISR(OS_HISR *hisr)
{
  STATUS iStatus;

  iStatus = NU_Activate_HISR((NU_HISR *)hisr); 
  if(iStatus == NU_SUCCESS )
     iStatus = SUCCESSFUL;
  else
     iStatus = FAILURE;
	 
  return(iStatus);
}

/* ************************************************************************
// MODULE     : OSDeleteHISR
// AUTHOR     : Bob Li
// DESCRIPTION: Delete a High Level Interrupt Service Routine; Nucleus frees the stack
// INPUTS     : hisr - pointer to OS_Hisr Structure
// ********************************************************************** */
void OSDeleteHISR(OS_HISR * hisr)
{
    STATUS iStatus;
	
    iStatus = NU_Delete_HISR((NU_HISR *)hisr); 
    if(iStatus == NU_SUCCESS)
    {
        unsigned char bHisrIndex = 0;
        
        //clear the HISR data from the HisrDataList.
        for(bHisrIndex = 0; bHisrIndex < bMaxHisrAvailable; bHisrIndex++)
        {
            if(HisrDataList[bHisrIndex].pHisrControlBlock == hisr)
                break;            
        }

        if(bHisrIndex < bMaxHisrAvailable)
        {
            //clear the HISR data from the HisrDataList since the HISR is deleted.
            memset(&HisrDataList[bHisrIndex], 0, sizeof(HISR_DEBUG_DATA));
			
            //move the HISRs after the deleted one forward one position.
            memmove(&HisrDataList[bHisrIndex], &HisrDataList[bHisrIndex + 1], sizeof(HISR_DEBUG_DATA)*(bMaxHisrAvailable - bHisrIndex - 1));

            //clear the last element since the data is obsolete.
            memset(&HisrDataList[bMaxHisrAvailable - 1], 0, sizeof(HISR_DEBUG_DATA));

            bMaxHisrAvailable--;
        }
        
    }
}

/* ************************************************************************
// MODULE     : OSRunningInTask
// DESCRIPTION: Indicates whether caller is running in a task
// ********************************************************************** */
BOOL OSRunningInTask(void)
{
	NU_TASK *pCurThr		= NU_Current_Task_Pointer();

	// Non null pointer means task; Null means HISR (typical) or no context at all
	return (pCurThr != NU_NULL);
}

/* ************************************************************************
// MODULE     : FATAL_EXCEP
// AUTHOR     : George Monroe
// DESCRIPTION: Vectored to when there is an unhandled exception
// INPUTS     : 
//SE********************************************************************** */
void FATAL_EXCEP()
{
//TODO - invoke our own error handler here that does more than this which should effectively halt the system
	NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS); // disables FIQ and IRQ for ARM
	ERC_System_Error(NU_FATAL_ERROR);
}

/* **********************************************************************
// FUNCTION NAME: TskSendMessage
// PURPOSE:  Issue an intertask message with a BLOCKING WAIT on the queue
//             if it is full.  This will allow the 'consumer task' an
//             opportunity to process some of the queue contents.  This is
//             a blocking version of 'OSSendIntertask()'.
//           
// AUTHOR: Joseph Tokarski / Gary Jacobson
// 
// INPUTS:  bDestID - Destination task ID
//          bSourceID - Source task ID
//          bMsgID - Message identifier
//          bMessageType - Format of the message data union
//          pMsgBuf - Pointer to message data
//          sSize - Size of the message data
// *********************************************************************** */
BOOL TskSendMessage(unsigned char bDestID,
                    unsigned char bSourceID,
                    unsigned char bMsgID, 
                    unsigned char bMessageType, 
                    void *pMsgBuf, 
                    unsigned long lwSize)
{
   INTERTASK rMessage ;


   rMessage.bSource = bSourceID ;
   rMessage.bDest = bDestID;
   rMessage.bMsgId = bMsgID ;
   rMessage.bMsgType = bMessageType;
   
   if ((bMessageType == PART_PTR_DATA) || (bMessageType == GLOBAL_PTR_DATA))
   {   
      rMessage.IntertaskUnion.PointerData.pData = pMsgBuf ;
      rMessage.IntertaskUnion.PointerData.lwLength = lwSize ;
   }
   /* COMM CHANGE */
   else if (bMessageType == COMM_DATA)
   {
       if (lwSize < MAX_INTERTASK_BYTES)
       {
          rMessage.IntertaskUnion.CommData.bLength = (unsigned char) lwSize;
          memcpy (rMessage.IntertaskUnion.CommData.bByteData, pMsgBuf, lwSize) ;
       }
       else
       {
          rMessage.bMsgType = PART_PTR_DATA ;
          if (OSGetMemory ((void **) &rMessage.IntertaskUnion.PointerData.pData, lwSize) == FAILURE)
          {
        	    return ((BOOL) FAILURE) ;
          }
          memcpy (rMessage.IntertaskUnion.PointerData.pData, pMsgBuf, lwSize) ;
          rMessage.IntertaskUnion.PointerData.lwLength = lwSize ;
       }
   }   
   /* COMM CHANGE */
   else if (bMessageType == COMM_RECEIVE_DATA)
   {
      rMessage.IntertaskUnion.PointerData.pData = pMsgBuf ;
      rMessage.IntertaskUnion.PointerData.lwLength = lwSize ;
   }   
   else if( lwSize <= MAX_INTERTASK_BYTES )
   {
      memcpy (rMessage.IntertaskUnion.bByteData, pMsgBuf, lwSize) ;
   }  
   else
   {
      memcpy (rMessage.IntertaskUnion.bByteData, pMsgBuf, MAX_INTERTASK_BYTES) ;
   }
   
   if (NU_Send_To_Queue (Queues[bDestID].pQueue, (unsigned long *) &rMessage, 
        INTERTASK_MSG_SIZE, NU_SUSPEND) == NU_SUCCESS)
   {
      fnLogIntertaskMsg(rMessage, INTERTASK_BLOCKING_CALL);
      return ((BOOL) SUCCESSFUL) ;
   }
   else
	   return ((BOOL) FAILURE) ;

    // we should never get here, but to satisfy lint, return a failure
    return ((BOOL) FAILURE) ;
}

/* **********************************************************************
// FUNCTION NAME: isUIConBase
// PURPOSE:  Determine if the UIC is on base or not.
//           
// AUTHOR: Brian Hannigan
// 
// INPUTS:  None.
//
// RETURNS: TRUE  - if the UIC is on the base
//          FALSE - if the UIC is NOT on the base
//
// CAVEATS: DM150 is ALWAYS on base.
//
// REVISION HISTORY:
// *********************************************************************** */
BOOL isUIConBase(void)
{
    return(TRUE);
}

/* **********************************************************************
// FUNCTION NAME: fnTurnOffMiniSysLog
// PURPOSE:  Copy a syslog entry to the CMOSSyslog
// NOTES: 
// 1. The CMOSSyslog is only accessible on a DM475.  On the 300 and 400, this
//    function just returns.
// 2. This may be called before the CMOS preservation has been run.  In that
//    case, save a ptr to the entry to copy, and the next time this is called
//    after the preservation has been run, copy those entries first. 
//
// History:
// AUTHOR: Clarisa Bellamy
// -------------------------------------------------------------------------*/
void fnTurnOffMiniSysLog( void )
{
    INTERTASK_LOG_ENTRY *pDest;

    fnDumpStringToSystemLog( "********** TurnOffMiniSysLog *********" );
    fMiniSyslogOff = TRUE;  
}

/* **********************************************************************
// FUNCTION NAME: fnLogIntertaskMsg
// PURPOSE:  Logs an entry in the intertask message flight recorder.
// 
// INPUTS:  rIntertask- intertask message being logged
//          bCall- byte identifying the wrapper function used to send the message
// HISTORY:
// 2012.09.14 Clarisa Bellamy - Modifiy to not allow an entry to be put in the 
//                      syslog if currently in the middle of putting an entry
//                      in the syslog. Increment ulMissedSysLogs whenever this 
//                      happens.  
//           
// AUTHOR: Joe Mozdzer
// *********************************************************************** */
static void fnLogIntertaskMsg(INTERTASK rIntertask, unsigned char bCall)
{
    void *pData;
    unsigned long lwLen;    
    unsigned long filter=FALSE;

    
    if (fAddingSyslogEntry == TRUE)
    {
        if (ulMissedSysLogs != 0xFFFFFFFF)
            ulMissedSysLogs++;
        return;
    }
    else
    {
        fAddingSyslogEntry = TRUE;
    }

	// Check the filters before logging this message
    if ((pIntertaskLogMask[rIntertask.bSource] & SRC_MASK) == SRC_MASK)
        filter = TRUE;

    if ((pIntertaskLogMask[rIntertask.bDest] & DEST_MASK) == DEST_MASK)
        filter = TRUE;

    // DSD 3/24/2006 - temp change to remove feeder sensor poll msg logging
    if (rIntertask.bSource == FDRMCPTASK && 
        rIntertask.bDest == FDRMCPTASK &&
        rIntertask.bMsgId == 3)//FDR_POLL_SENSOR_TIMEOUT
	{
        filter = TRUE;
	}

    if (rIntertask.bSource == MCP_CLASS_DRVR &&
        rIntertask.bDest == FDRMCPTASK &&
        rIntertask.bMsgId == COMM_RECEIVE_MESSAGE)
    {
        PB232BUFFER *pPB232Ptr = rIntertask.IntertaskUnion.PointerData.pData;       
        if (pPB232Ptr->bData[MMC_MSG_CLASS_OFFSET] == MOTION_CLASS_CODE &&
            (pPB232Ptr->bData[MMC_MSG_COMMAND_OFFSET] == MMC_SENSOR_STATE_RSP ||
             pPB232Ptr->bData[MMC_MSG_COMMAND_OFFSET] == MMC_SENSOR_LOG_DATA_RSP ||
             pPB232Ptr->bData[MMC_MSG_COMMAND_OFFSET] == MMC_PROFILE_HIST_DATA_RSP))
            filter = TRUE;
    }

    // Filter Update display strings out of log.
    if (rIntertask.bSource == OIT && 
        rIntertask.bDest == LCDTASK &&
        rIntertask.bMsgId == LCD_DISPLAY_STRINGS)
        filter = TRUE;

    if ((fMsgLoggingEnabled) && (filter == FALSE))
    {
        if (lwIntertaskMsgLogIndex < INTERTASK_LOG_HEADER_SIZE)
            lwIntertaskMsgLogIndex = INTERTASK_LOG_HEADER_SIZE;

        pIntertaskMsgLog[lwIntertaskMsgLogIndex].lwTimeStamp = EndianSwap32(getMsClock());
        pIntertaskMsgLog[lwIntertaskMsgLogIndex].bType = SYSLOG_ITM;
        pIntertaskMsgLog[lwIntertaskMsgLogIndex].bSourceID = rIntertask.bSource;
        pIntertaskMsgLog[lwIntertaskMsgLogIndex].bDestID = rIntertask.bDest;
        pIntertaskMsgLog[lwIntertaskMsgLogIndex].bMsgID = rIntertask.bMsgId;
        memset( pIntertaskMsgLog[lwIntertaskMsgLogIndex].pUnionData, 0x00, 
                sizeof(pIntertaskMsgLog[lwIntertaskMsgLogIndex].pUnionData) );
        
        switch(rIntertask.bMsgType)
        {
            case NO_DATA:
                break;

            case PART_PTR_DATA:         // Msg contains ptr to data
            case COMM_RECEIVE_DATA:
            case GLOBAL_PTR_DATA:
                if (rIntertask.bMsgType == COMM_RECEIVE_DATA &&
                    rIntertask.bDest == FDRMCPTASK)
                {
                    pData = ((PB232BUFFER *)rIntertask.IntertaskUnion.PointerData.pData)->bData;
                    lwLen = SYSLOG_MAX_DATA;                    
                    memcpy (pIntertaskMsgLog[lwIntertaskMsgLogIndex].pUnionData, pData, lwLen);

                }
                else
                {
	                pData = rIntertask.IntertaskUnion.PointerData.pData;
	                lwLen = rIntertask.IntertaskUnion.PointerData.lwLength;
	                if (lwLen > SYSLOG_MAX_DATA)        // Limit to size of log entry
	                    lwLen = SYSLOG_MAX_DATA;
					
	                memcpy (pIntertaskMsgLog[lwIntertaskMsgLogIndex].pUnionData, pData, lwLen);
                }
                break;

            case MEM_ACCESS:
            case PERFORM_SCRIPT:
                // Copy the bWhy value (routine that called scheduleBob to send this message.)
                pIntertaskMsgLog[lwIntertaskMsgLogIndex].bMsgID = rIntertask.IntertaskUnion.bByteData[2];
                // Fall through...
                // no break

            case COMM_DATA:             // Msg contains data
            case BYTE_DATA:
            case SHORT_DATA:
            case LONG_DATA:
            case BOOL_DATA:
            default:
                memcpy (pIntertaskMsgLog[lwIntertaskMsgLogIndex].pUnionData, &rIntertask.IntertaskUnion.bByteData[0], MAX_INTERTASK_BYTES);
                break;
        }

//      pIntertaskMsgLog[lwIntertaskMsgLogIndex].bCall = bCall;
        lwIntertaskMsgLogIndex++;
        if (lwIntertaskMsgLogIndex >= INTERTASK_LOG_SIZE)
            lwIntertaskMsgLogIndex = INTERTASK_LOG_HEADER_SIZE;
			
        /* set the next item to all 1's so it is easy to find in a mem dump window */
        memset(&pIntertaskMsgLog[lwIntertaskMsgLogIndex], 0x11, sizeof(pIntertaskMsgLog[lwIntertaskMsgLogIndex]));
    }

    fAddingSyslogEntry = FALSE;
}

/* **********************************************************************
// FUNCTION NAME: fnIntertaskMsgLogEnable
// PURPOSE:  Enables logging of intertask messages in flight recorder.
//           
// AUTHOR: Joe Mozdzer
// 
// *********************************************************************** */
void fnIntertaskMsgLogEnable(void)
{
    fMsgLoggingEnabled = TRUE;
}

/* **********************************************************************
// FUNCTION NAME: fnIntertaskMsgLogDisable
// PURPOSE:  Disables logging of intertask messages in flight recorder.
//           
// AUTHOR: Joe Mozdzer
// 
// *********************************************************************** */
void fnIntertaskMsgLogDisable(void)
{
    fMsgLoggingEnabled = FALSE;
}


/* **********************************************************************
// FUNCTION NAME:   OSDirectSendIntertask
// PURPOSE:         Supports an urgent intertask message among UIC tasks
// AUTHOR:          H. Sun
//
// INPUTS:  rMessage - Message to be resent
//        
// ************************************************************************ */
BOOL  OSResendIntertask (INTERTASK  *rMessage)
{
   if (NU_Send_To_Queue (Queues[rMessage->bDest].pQueue, (unsigned long *) &rMessage, 
        INTERTASK_MSG_SIZE, NU_NO_SUSPEND) == NU_SUCCESS)
   {
      fnLogIntertaskMsg(*rMessage, INTERTASK_NORMAL_CALL);
      return ((BOOL) SUCCESSFUL) ;
   }
   else
      return ((BOOL) FAILURE) ;
} // end of OSSendIntertask()



/* **********************************************************************
// FUNCTION NAME:           fnSystemLogResetHeader
// PURPOSE:  
//      Resets the length of the SysLogHeader to the current size of the 
//      syslog.  To prevent abuse, this only works once after a reset.
//      To give a visual indication that the header size was changed, a
//      "solid" line is added to the syslog first.  
//
// INPUTS:  None
// HISTORY:
//  2012.09.14 - Clarisa Bellamy - fixed function header.          
// AUTHOR: Clarsia Bellamy
// *********************************************************************** */
/* not using this any more
static BOOL fHeaderSet = FALSE;
void fnSystemLogResetHeader( void )
{
    // This should only be called once.
    if( fHeaderSet == FALSE )
    {
        fnDumpStringToSystemLog( "------------------------------");    
        ulIntertaskMsgHdrEnd = lwIntertaskMsgLogIndex;
        fHeaderSet = TRUE;
    }
}
*/
/* **********************************************************************
// FUNCTION NAME:               fnSystemLogEntry
// PURPOSE:  Logs a text entry in the system log
// 
// INPUTS:  bType - log entry type (defined as SYSLOG_xxx in OS.H)
//          pData - pointer to string data
//          lwLen - length of string data
// HISTORY:
// 2012.09.14 Clarisa Bellamy - Modifiy to not allow an entry to be put in the 
//                      syslog if currently in the middle of putting an entry
//                      in the syslog. Increment ulMissedSysLogs whenever this 
//                      happens.  
//           
// AUTHOR: Brian Hannigan
// *********************************************************************** */
//TODO - determine if static or not
void fnSystemLogEntry(unsigned char bType, char *pData, UINT32 lwLen )
{
    if( fAddingSyslogEntry == TRUE )
    {
        if( ulMissedSysLogs != 0xFFFFFFFF )
            ulMissedSysLogs++;
        return;
    }
    else
    {
        fAddingSyslogEntry = TRUE;
    }

    if (fMsgLoggingEnabled)
    {
        if (lwIntertaskMsgLogIndex < INTERTASK_LOG_HEADER_SIZE)
            lwIntertaskMsgLogIndex = INTERTASK_LOG_HEADER_SIZE;

        memset( &pIntertaskMsgLog[lwIntertaskMsgLogIndex], 0x00, sizeof(INTERTASK_LOG_ENTRY) );
        pIntertaskMsgLog[lwIntertaskMsgLogIndex].bType = bType;
        pIntertaskMsgLog[lwIntertaskMsgLogIndex].lwTimeStamp = EndianSwap32(getMsClock());
        if (lwLen > SYSLOG_MAX_DATA)
            lwLen = SYSLOG_MAX_DATA;
			
        memcpy (pIntertaskMsgLog[lwIntertaskMsgLogIndex].pUnionData, pData, lwLen);
        lwIntertaskMsgLogIndex++;
        if (lwIntertaskMsgLogIndex >= INTERTASK_LOG_SIZE)
            lwIntertaskMsgLogIndex = INTERTASK_LOG_HEADER_SIZE;
			
        /* set the next item to all 1's so it is easy to find in a mem dump window */
        memset(&pIntertaskMsgLog[lwIntertaskMsgLogIndex], 0x11, sizeof(pIntertaskMsgLog[lwIntertaskMsgLogIndex]));
    }

    fAddingSyslogEntry = FALSE;
}

/* **********************************************************************
// FUNCTION NAME: fnIbuttonLogEntry
// PURPOSE:  Logs an Ibutton message entry in the system log
// 
// INPUTS:  bType - log entry type (defined as SYSLOG_xxx in OS.H)
//          bSource - 
//          bDest -
//          bMsgID -
//          pData - pointer to string data
//          lwLen - length of string data (max 40 bytes)
// HISTORY:
// 2012.09.14 Clarisa Bellamy - Modifiy to not allow an entry to be put in the 
//                      syslog if currently in the middle of putting an entry
//                      in the syslog. Increment ulMissedSysLogs whenever this 
//                      happens.  
//           
// AUTHOR: Brian Hannigan
// *********************************************************************** */
void fnIbuttonLogEntry(uchar bType, uchar bSource, uchar bDest, uchar bMsgId, char *pData, UINT32 lwLen )
{
    if (fAddingSyslogEntry == TRUE)
    {
        if (ulMissedSysLogs != 0xFFFFFFFF)
            ulMissedSysLogs++;
        return;
    }
    else
    {
        fAddingSyslogEntry = TRUE;
    }

    if (fMsgLoggingEnabled)
    {
        if (lwIntertaskMsgLogIndex < INTERTASK_LOG_HEADER_SIZE)
            lwIntertaskMsgLogIndex = INTERTASK_LOG_HEADER_SIZE;

        pIntertaskMsgLog[lwIntertaskMsgLogIndex].lwTimeStamp = EndianSwap32(getMsClock());
        pIntertaskMsgLog[lwIntertaskMsgLogIndex].bType = bType;
        pIntertaskMsgLog[lwIntertaskMsgLogIndex].bSourceID = bSource;
        pIntertaskMsgLog[lwIntertaskMsgLogIndex].bDestID = bDest;
        pIntertaskMsgLog[lwIntertaskMsgLogIndex].bMsgID = bMsgId;
        memset( pIntertaskMsgLog[lwIntertaskMsgLogIndex].pUnionData, 0x00, SYSLOG_PAYLOAD_BYTES );
        
        if (lwLen > SYSLOG_PAYLOAD_BYTES)
            lwLen = SYSLOG_PAYLOAD_BYTES;
			
        memcpy (pIntertaskMsgLog[lwIntertaskMsgLogIndex].pUnionData, pData, lwLen);
        lwIntertaskMsgLogIndex++;
        if (lwIntertaskMsgLogIndex >= INTERTASK_LOG_SIZE)
            lwIntertaskMsgLogIndex = INTERTASK_LOG_HEADER_SIZE;
			
        /* set the next item to all 1's so it is easy to find in a mem dump window */
        memset(&pIntertaskMsgLog[lwIntertaskMsgLogIndex], 0x11, sizeof(pIntertaskMsgLog[lwIntertaskMsgLogIndex]));
    }

    fAddingSyslogEntry = FALSE;
}


/* **********************************************************************
// FUNCTION NAME: fnSystemLogEntryLong
// PURPOSE:  Logs a variable number of long words in the system log
// 
// INPUTS:  bType - log entry type (defined as SYSLOG_xxx in OS.H)
//          bHeader1
//          bHeader2
//          bHeader3
//          lwData1
//          ..
//          lwData10
// HISTORY:
// 2012.09.14 Clarisa Bellamy - Modifiy to not allow an entry to be put in the 
//                      syslog if currently in the middle of putting an entry
//                      in the syslog. Increment ulMissedSysLogs whenever this 
//                      happens.  
//           
// AUTHOR: Brian Hannigan
// *********************************************************************** */
void fnSystemLogEntryLong( UINT8 bType, UINT8 bHeader1, UINT8 bHeader2, UINT8 bHeader3,
                           UINT32 lwData1, UINT32 lwData2, UINT32 lwData3, UINT32 lwData4, UINT32 lwData5,
                           UINT32 lwData6, UINT32 lwData7, UINT32 lwData8, UINT32 lwData9, UINT32 lwData10 )
{
    unsigned long *pLogData;
    if (fAddingSyslogEntry == TRUE)
    {
        if (ulMissedSysLogs != 0xFFFFFFFF)
            ulMissedSysLogs++;
        return;
    }
    else
    {
        fAddingSyslogEntry = TRUE;
    }

    if (fMsgLoggingEnabled)
    {
        if (lwIntertaskMsgLogIndex < INTERTASK_LOG_HEADER_SIZE)
            lwIntertaskMsgLogIndex = INTERTASK_LOG_HEADER_SIZE;

        pIntertaskMsgLog[lwIntertaskMsgLogIndex].lwTimeStamp = EndianSwap32(getMsClock());
        pIntertaskMsgLog[lwIntertaskMsgLogIndex].bType = bType;
        pIntertaskMsgLog[lwIntertaskMsgLogIndex].bSourceID = bHeader1;
        pIntertaskMsgLog[lwIntertaskMsgLogIndex].bDestID = bHeader2;
        pIntertaskMsgLog[lwIntertaskMsgLogIndex].bMsgID = bHeader3;
        
        pLogData = (unsigned long *)&pIntertaskMsgLog[lwIntertaskMsgLogIndex].pUnionData[0];  // start of payload
        *pLogData++ = EndianAwareGet32(&lwData1);
        *pLogData++ = EndianAwareGet32(&lwData2);
        *pLogData++ = EndianAwareGet32(&lwData3);
        *pLogData++ = EndianAwareGet32(&lwData4);
        *pLogData++ = EndianAwareGet32(&lwData5);
        *pLogData++ = EndianAwareGet32(&lwData6);
        *pLogData++ = EndianAwareGet32(&lwData7);
        *pLogData++ = EndianAwareGet32(&lwData8);
        *pLogData++ = EndianAwareGet32(&lwData9);
        *pLogData   = EndianAwareGet32(&lwData10);
        lwIntertaskMsgLogIndex++;
        if (lwIntertaskMsgLogIndex >= INTERTASK_LOG_SIZE)
            lwIntertaskMsgLogIndex = INTERTASK_LOG_HEADER_SIZE;
			
        /* set the next item to all 1's so it is easy to find in a mem dump window */
        memset(&pIntertaskMsgLog[lwIntertaskMsgLogIndex], 0x11, sizeof(pIntertaskMsgLog[lwIntertaskMsgLogIndex]));
    }

    fAddingSyslogEntry = FALSE;
}

/* **********************************************************************
// FUNCTION NAME: fnDumpToSystemLog
// PURPOSE: Helper function to add strings or buffers to the system log
//          using multiple system log entries if necessary. Any entries
//          apart from the first one will have the SYSLOG_ENTRY_CONTINE_FLAG
//          set in the entry type field.
//          
//           
// AUTHOR:  Vinny Rozendaal
// 
// INPUTS:  buf        - pointer to buffer/string to be logged.
//          buf_len    - # of bytes in buf to add to the system log
//          entry_type - The system log entry type to use
//
// *********************************************************************** */
static void fnDumpToSystemLog(void *buf, unsigned long buf_len, unsigned char entry_type)
{

    if (fAddingSyslogEntry == TRUE)
    {
        if (ulMissedSysLogs != 0xFFFFFFFF)
            ulMissedSysLogs++;
        return;
    }
    // else have to wait for the single entry function to clear the flag

    if (buf)  // Make sure it is not a NULL pointer
    {

        while (buf_len > 0)
        {
            fnSystemLogEntry(entry_type, buf, buf_len);

            if (buf_len > SYSLOG_PAYLOAD_BYTES)
            {    
                buf_len    -= SYSLOG_PAYLOAD_BYTES;
                buf        += SYSLOG_PAYLOAD_BYTES;
                entry_type |= SYSLOG_ENTRY_CONTINE_FLAG;
            }
            else
            {
                buf_len = 0;
            }
        }
    }
}

/* **********************************************************************
// FUNCTION NAME: fnDumpStringToSystemLog
// PURPOSE: Breaks up a long string into multiple system log entries
//           
// AUTHOR: Brian Hannigan
// 
// INPUTS:  inStr - pointer to string to be logged.
//
// *********************************************************************** */
void fnDumpStringToSystemLog(char *inStr)
{
    if (inStr)   // Make sure it is not a NULL pointer
    {
        fnDumpToSystemLog(inStr, strlen(inStr), SYSLOG_TEXT);
    }
}


void fnDumpRawToSystemLog(char *inStr)
{
    if (inStr)   // Make sure it is not a NULL pointer
    {
        fnDumpToSystemLog(inStr, strlen(inStr), SYSLOG_EDMMSG);
    }
}

void fnDumpRawLenToSystemLog(char *inStr, int Len)
{
    if (inStr)   // Make sure it is not a NULL pointer
    {
        fnDumpToSystemLog(inStr, Len, SYSLOG_EDMMSG);
    }
}


/* **********************************************************************
// FUNCTION NAME: fnDumpStringToSystemLogWithNum
// PURPOSE:     
//      Breaks up a long string into multiple system log entries, and adds a
//      '#' an a number at the end of the string.  The caller can use it to 
//      add the line number to a message that is more than 40 chars long. 
// INPUTS:  
//      inStr - pointer to string to be logged.
//      iNumber - integer, number to print at the end of the string, usually 
//                      the caller's line number.
// HISTORY:
//  2010.07.16 Clarisa Bellamy - initial
// *********************************************************************** */
void fnDumpStringToSystemLogWithNum( char *inStr, int iNumber )
{
    char    tempStr[SYSLOG_PAYLOAD_BYTES+1];
    char    *pos=inStr;
    int     len;


    if (fAddingSyslogEntry == TRUE)
    {
        if (ulMissedSysLogs != 0xFFFFFFFF)
            ulMissedSysLogs++;
        return;
    }
    // else have to wait for the single entry function to clear the flag

    if( pos )  // Make sure it is not a NULL pointer
    {
        while( strlen(pos) > SYSLOG_PAYLOAD_BYTES )      // Check for multiple lines
        {
            strncpy( tempStr, pos, SYSLOG_PAYLOAD_BYTES );
            fnSystemLogEntry( SYSLOG_TEXT, tempStr, strlen(tempStr) );
            pos += SYSLOG_PAYLOAD_BYTES;
        }

        len = sprintf( tempStr, " #%d", iNumber );
        if( (strlen(pos) + len ) <= SYSLOG_PAYLOAD_BYTES) 
        {
            // If we have enough bytes for one more line, but the line is not full, AND 
            // if we have enough room for a line number, add the line number at the end.
            sprintf( tempStr, "%s #%d", pos, iNumber );
            fnSystemLogEntry( SYSLOG_TEXT, tempStr, strlen(tempStr) );
        }
        else
        {
            // it won't fit on one line with the line number data, so make it two lines.
            if( strlen( pos ) )
                fnSystemLogEntry( SYSLOG_TEXT, pos, strlen(pos) );
				
            fnSystemLogEntry( SYSLOG_TEXT, tempStr, strlen(tempStr) );
        }
    }
}


/* **************************************************************************
// FUNCTION NAME:   fnDumpStrWithPrefixToSyslog
// PURPOSE:     
//      Prepends the pHdr string tot he pText string, and puts the result 
//      into the system log. 
// INPUTS:  
//      pText - pointer to string to be logged.
//      pHdr - pointer to string to prefix to pText, before it is added to 
//              the syslog. 
// NOTES:
//  1. The header is limited to 20 bytes. 
//  2. 
// HISTORY:
//  2010.08.24 Clarisa Bellamy - initial
//--------------------------------------------------------------------------*/
void fnDumpStrWithPrefixToSyslog( char *pText, char *pHdr )
{
    INT     iTextStartLen = 0;
    INT     iHdrLen = 0;
    INT     iTextLen;
    char    tempStr[ SYSLOG_PAYLOAD_BYTES +1 ];


    if (fAddingSyslogEntry == TRUE)
    {
        if (ulMissedSysLogs != 0xFFFFFFFF)
            ulMissedSysLogs++;
        return;
    }
    // else have to wait for the single entry function to clear the flag

    // Verify we have a good string:
    if( pText == NULL_PTR )
    {
        pText = pHdr;
        pHdr = NULL_PTR;
    }

    iTextLen = strlen( pText );
	
    // If there is a header, prepend it
    if( pHdr != NULL_PTR )
    {
        // Limit the length of the header to half of a line:
        iHdrLen = strlen( pHdr );
        if( iHdrLen > SYSLOG_PAYLOAD_BYTES / 2 )
            iHdrLen = SYSLOG_PAYLOAD_BYTES / 2; 
        
        iTextStartLen = ((SYSLOG_PAYLOAD_BYTES - 1) - iHdrLen);
        sprintf( tempStr, "%.*s %.*s", iHdrLen, pHdr, iTextStartLen, pText );
        fnDumpStringToSystemLog( tempStr );
    }
	
    // If there is more than 1 line to print, or there was no header,
    //  dump the rest of the string to syslog.
    if( iTextStartLen < iTextLen )
    {
        fnDumpStringToSystemLog( &pText[ iTextStartLen ] );
    }
}

/* ****************************************************************************
// FUNCTION NAME: fnSysLogTaskStart
// PURPOSE:     
//      Add message to syslog indicating that a task has started. 
// INPUTS:  
//      pTaskName - pointer to Task Name string, doesn't need to be official.
//      pTaskID - integer, Task ID.   Use 0xFF is not defined.
// HISTORY:
//  2012.11.05 Clarisa Bellamy - initial
// ------------------------------------------------------------------------- */
void fnSysLogTaskStart( char *pTaskName, int pTaskID )
{
    char pBuff[ 75 ];


    sprintf( pBuff, "TASK: %s Started, ID:%02X", pTaskName, pTaskID );  
    fnDumpStringToSystemLog( pBuff );
}

/* ****************************************************************************
// FUNCTION NAME: fnSysLogTaskLoop
// PURPOSE:     
//      Add message to syslog indicating that a task is about to enter its 
//      forever loop. 
// INPUTS:  
//      pTaskName - pointer to Task Name string, doesn't need to be official.
//      pTaskID - integer, Task ID.
// HISTORY:
//  2012.11.05 Clarisa Bellamy - initial
// ------------------------------------------------------------------------- */
void fnSysLogTaskLoop( char *pTaskName, int pTaskID )
{
    char pBuff[ 75 ];

    sprintf( pBuff, "TASK: %s Loop, ID:%02X", pTaskName, pTaskID );
    fnDumpStringToSystemLog( pBuff );  
}


/* **********************************************************************
// FUNCTION NAME: fnSystemLogHeader
// PURPOSE:  Adds a heade entry to the system log. The header is the  
//           first INTERTASK_LOG_HEADER_SIZE entries. They are not 
//           overwritten by normal log entries, so they are always visible
//
//          Entry 0: UIC SW Version
//          Entry 1: Time of last sync with iButton RTC (when RTClockCount is set to 0)
//          Entry 2: Powerup Time (GMT)
//           
// AUTHOR: Brian Hannigan
// 
// INPUTS:  pData - pointer to string data
//          bIndex - log entry to use.  Must be less than INTERTASK_LOG_HEADER_SIZE
// NOTE: 
//  12-Nov-12 S. Peterson  Changed to ignore bIndex. Instead we'll have a separate
//                         index for the header data so the size of the header can grow.
// *********************************************************************** */
void fnSystemLogHeader(char *pData, uchar bIndex )
{
    ulong   lwLen;


	if (fMsgLoggingEnabled)
	{
// If standard I/O is set for a functioning UART, then send strings there also.
        if (stdio_enabled)
        	printf("%s\r\n" ,pData);
		
		
	    if ( bIndex != 0 )
	    {   
	        (void)memset( &pIntertaskMsgLog[bIndex-1], 0x00, sizeof(INTERTASK_LOG_ENTRY) );
	        pIntertaskMsgLog[bIndex-1].bType = SYSLOG_TEXT;
	        pIntertaskMsgLog[bIndex-1].lwTimeStamp = EndianSwap32(getMsClock());
        
	        lwLen = strlen(pData);
	        if (lwLen > SYSLOG_MAX_DATA)
	            lwLen = SYSLOG_MAX_DATA;
			
	        (void)memcpy (pIntertaskMsgLog[bIndex-1].pUnionData, pData, lwLen);
	    }
		else
		{
	        (void)memset( &pIntertaskMsgLog[ulHeaderIndex], 0x00, sizeof(INTERTASK_LOG_ENTRY) );
	        pIntertaskMsgLog[ulHeaderIndex].bType = SYSLOG_TEXT;
	        pIntertaskMsgLog[ulHeaderIndex].lwTimeStamp = EndianSwap32(getMsClock());
        
	        lwLen = strlen(pData);
	        if (lwLen > SYSLOG_MAX_DATA)
	            lwLen = SYSLOG_MAX_DATA;
			
	        (void)memcpy(pIntertaskMsgLog[ulHeaderIndex].pUnionData, pData, lwLen);
		
			ulHeaderIndex++;
			if (ulHeaderIndex == ulIntertaskMsgHdrEnd)
			{
				ulIntertaskMsgHdrEnd = ulHeaderIndex + 1;
		        (void)memset( &pIntertaskMsgLog[ulHeaderIndex], 0x00, sizeof(INTERTASK_LOG_ENTRY) );
		        pIntertaskMsgLog[ulHeaderIndex].bType = SYSLOG_TEXT;
		        pIntertaskMsgLog[ulHeaderIndex].lwTimeStamp = EndianSwap32(getMsClock());
	        	(void)memcpy(pIntertaskMsgLog[ulHeaderIndex].pUnionData, "-----------------------------------", 35);
			}
		}

	}
}

void * fnSysLogGetPointerToData(void)
{
    return pIntertaskMsgLog;
}
unsigned long fnSysLogGetDataLength(void)
{
    return sizeof(pIntertaskMsgLog);
}


/*
 * Name
 *   fnGetHisrRemainingStackSize
 *
 * Description
 *   This function checks how much space hasn't been used on an HISR's stack.
 *
 * Inputs
 *   bHisrID         - HISR ID number
 *   plStackSize     - pointer to where to load the size of the stack
 *   plRemainingSize - pointer to where to load the size of the unused space on the stack
 *   pbHisrName      - pointer to where to load the name of the HISR
 *
 * Returns
 *   TRUE if task ID is OK. FALSE otherwise.
 *
 * Caveats
 *   None
 *
 * Revision History
 *   12 Jan 05 Sandra Peterson - Initial Revision.
 */

BOOL fnGetHisrRemainingStackSize(unsigned char bHisrID,
                            unsigned long *plStackSize,
                            unsigned long *plRemainingSize,
                            char *pbHisrName)
{
    unsigned long   *plStartAddr;
    unsigned long   lInx, lEnd;
    BOOL    bRetVal = TRUE;


    if ((bMaxHisrAvailable == 0) || (bHisrID >= bMaxHisrAvailable) ||
        ((bHisrID < bMaxHisrAvailable) && (HisrDataList[bHisrID].plStartingStackAddr == NULL_PTR)))
    {
        bRetVal = FALSE;
    }
    else
    {
        *plStackSize = HisrDataList[bHisrID].lStackSize;
        plStartAddr = (unsigned long *)HisrDataList[bHisrID].plStartingStackAddr;
        strcpy(pbHisrName, HisrDataList[bHisrID].HisrName);

        lInx = 0;
        lEnd = *plStackSize - 4;    // because the first 4 bytes on the stack aren't used
        while ( (lInx < lEnd) && (*plStartAddr == 0xAAAAAAAA) && (plStartAddr != NULL_PTR) )
        {
            lInx += 4;
            plStartAddr++;
        }

        *plRemainingSize = lInx;
    }

    return (bRetVal);
}


/* **********************************************************************
// FUNCTION NAME: fnGetMissedSyslogEntries
// PURPOSE:  Get the number of missed syslog entries due to trying to 
//           write to the syslog while writing to the syslog. 
//           
// AUTHOR: Sandra Peterson
// 
// INPUTS:  plMissedLogs - where to store the value
//
// *********************************************************************** */
void fnGetMissedSyslogEntries( UINT32 *plMissedLogs)
{
    *plMissedLogs = ulMissedSysLogs;
}


/* **********************************************************************
// FUNCTION NAME: fnSetMissedSyslogEntries
// PURPOSE:  Reset the number of missed syslog entries to the given value  
//           
// AUTHOR: Sandra Peterson
// 
// INPUTS:  ulMissedLogs - new value
//
// *********************************************************************** */
void fnSetMissedSyslogEntries( UINT32 ulMissedLogs)
{
    ulMissedSysLogs = ulMissedLogs;
}


#define DBGTRACE_BUF_COUNT_MAX	8

static UINT	errBuffArrayMaxCount 	= DBGTRACE_BUF_COUNT_MAX;
static UINT errBuffIndex 			= 0 ;

static PStructBuffArray pBuffArray	= NULL ;

/* **********************************************************************
// FUNCTION NAME: dbgTrace
// PURPOSE:  Writes a formatted string to the syslog and optionally 
//			 outputs it on stdio.  
// *********************************************************************** */
void dbgTrace(const int lvl, const char *fmt, ...)
{
	BOOL inHISR;
	char *errStartBuf;

#ifdef NDEBUG
	return;
#endif
	// Some issues here, the buffer used by this process used to be on hte stack however it needs to be large so ideally it should be on the stack.
	// If on the stack then it is common for all callers and this routine can be called by many threads very rapidly causing buffer corruption.
	// synchronizing the process was not viable since some callers are HISRs and synchronization does not work for them.
	// a rather makeshift solution to all this is to create an array of large buffers on the heap and run them in a circular loop. In testing the largest
	// number of simultaneous calls I found was 6 at startup so we will try 8 buffers. Now each caller should get a clean buffer to use however if there happens
	// to be over 8 concurrent calls then the worst case is a corrupted buffer (usually shows up as a duplicated line on the debug terminal output.
	// A little overkill maybe but this implementation will try for 8 buffers but if the memory is unavailable it will try 7 then 6 etc until eventually
	// 0. There is a check for a valid buffer value prior to main routine entry and if none is available the routine is exited.
	while((pBuffArray == 0) && (errBuffArrayMaxCount > 0))
	{
		pBuffArray = (PStructBuffArray)malloc(errBuffArrayMaxCount * sizeof(StructBuffArray)) ;
		if(pBuffArray == NULL)
			errBuffArrayMaxCount--;
		else
			memset(pBuffArray,  0, errBuffArrayMaxCount * sizeof(StructBuffArray));
	}

	if(pBuffArray == NULL) return ;

	// grab a buffer for ourselves and move the index onto the next one
	char *errBuf = &(pBuffArray[errBuffIndex++].Buff[0]);
	errBuffIndex %= errBuffArrayMaxCount;

	errStartBuf = errBuf; // This includes tag and is what is sent out
	inHISR = !OSRunningInTask(); // assume in HISR for logging purposes
	if (inHISR)
	{
		strcpy(errBuf, HISR_ID_TAG);
		errBuf += strlen(HISR_ID_TAG);
	}

	if (lvl <= currentLogLvl)
	{
		va_list argp;
		va_start(argp, fmt);

		if(vsnprintf(errBuf, DBGTRACE_BUF_SIZE ,fmt, argp) == PF_BUF_OVERFLOW)
		{
			errStartBuf[DBGTRACE_BUF_SIZE-1] = 0 ;
		}
		va_end(argp);

#ifdef DBG_TRACE_TO_SYSLOG
		fnDumpStringToSystemLog(errStartBuf);
#endif

// If standard I/O is set for a functioning UART, then send it there.
		if (stdio_enabled && !inHISR)
		{// buffer is 3 bytes bigger than DBGTRACE_BUF_SIZE
			errStartBuf[DBGTRACE_BUF_SIZE]	= 0x0D;
			errStartBuf[DBGTRACE_BUF_SIZE+1] = 0x0A;
			errStartBuf[DBGTRACE_BUF_SIZE+2] = 0x00;
			printf("%s",errStartBuf);
		}
		else
		{
			// if the buffer is not large enough for the complete dump
			// we need to ensure it is null terminated
			errStartBuf[DBGTRACE_BUF_SIZE] = 0 ;
		}

#if DBG_TRACE_TO_TCPIP == 1
			if(!inHISR)
			{
				debugSendString(errStartBuf) ;
				debugSendString("\r\n") ;
			}
			else
			{
				debugQueueString(errStartBuf) ;
				debugQueueString("\r\n") ;
			}
#endif
	}

}

// DESCRIPTION: Starterware compatibility - set interrupt priority
//
// INPUTS:
// RETURNS:
// **********************************************************************/
void IntPrioritySet(unsigned int intrNum, unsigned int priority,
                    unsigned int hostIntRoute)
{
	INT                     vector_id;
	ESAL_GE_INT_TRIG_TYPE   trigger_type = ESAL_TRIG_NOT_SUPPORTED; //don't care value
	INT                     nuc_priority;

	vector_id = (INT) intrNum + 2; // add 2 because Nucleus inserts two ints of its own into vector table
	nuc_priority = (INT) (priority | (hostIntRoute ? ESAL_AR_INT_FIQ_ROUTED: ESAL_AR_INT_IRQ_ROUTED));

	(void) ESAL_GE_INT_Enable(vector_id, trigger_type, nuc_priority);
}


/* **********************************************************************
// DESCRIPTION: Starterware compatibility - enable IRQ interrupts
//
// INPUTS:
// RETURNS:
// **********************************************************************/
void IntMasterIRQEnable(void)
{
	INT                     vector_id = ESAL_AR_IRQ_INT_VECTOR_ID;
	ESAL_GE_INT_TRIG_TYPE   trigger_type = ESAL_TRIG_NOT_SUPPORTED; //don't care value
	INT                     priority = 0; //don't care value

   (void) ESAL_AR_INT_Enable(vector_id, trigger_type, priority);
}

/* **********************************************************************
// DESCRIPTION: storeSyslogToFile
//
// INPUTS:	file path to use for the directory (NULL if not required) "/<path>"
//		    filename for the resultant filename of the target
// RETURNS:
// **********************************************************************/
#define	MAX_FILENAME_LENGTH		100
VOID storeSyslogToFile(char *filePath, char *fileName)
{
	dbgTrace(DBG_LVL_INFO, "Running storeSyslogToFile(%s, %s)\r\n", filePath, fileName);
	STATUS status = NU_RETURN_ERROR;
	char	strFullName[MAX_FILENAME_LENGTH];
	if(filePath)
	{
		snprintf(strFullName, MAX_FILENAME_LENGTH, "%s/%s", filePath, fileName);
	}
	else
	{
		strncpy(strFullName, fileName, MAX_FILENAME_LENGTH);
	}
	// try an write something to our file system....
	if((filePath != NULL) && ((status = NU_Make_Dir(filePath)) != NU_SUCCESS))
	{
		if(status == NUF_EXIST)
			debugDisplayOnly("Directory %s already exists\r\n", filePath);
		else
			debugDisplayOnly("Unable to create the required subdirectory %s [%d]\r\n", filePath, status);
	}

	if((status == NU_SUCCESS) || (status == NUF_EXIST))
		status = NU_Open(strFullName,(PO_TEXT|PO_CREAT|PO_TRUNC|PO_WRONLY), PS_IWRITE);

	if(status >= 0)
	{
		// write the file in one gulp...
		INT iSz = NU_Write(status, (char *)&(pIntertaskMsgLog[0]), sizeof(INTERTASK_LOG_ENTRY) * INTERTASK_LOG_SIZE);
		if((iSz > 0) && (iSz != ( sizeof(INTERTASK_LOG_ENTRY) * INTERTASK_LOG_SIZE)))
		{
			debugDisplayOnly("ERROR writing syslog file, tried to write %u bytes, only wrote %u\r\n", ( sizeof(INTERTASK_LOG_ENTRY) * INTERTASK_LOG_SIZE), iSz);
		}
		else
		{
			if(iSz > 0)
				debugDisplayOnly("%u bytes of syslog successfully written to file %s", iSz, strFullName);
			else
			{
				debugDisplayOnly("ERROR %d returned during syslog write operation\r\n",iSz);
				fnCheckFileSysCorruption(iSz);
			}
		}
		STATUS closeStatus = NU_Close(status);
		if(closeStatus != NU_SUCCESS)
			fnCheckFileSysCorruption(closeStatus);
	}
	else
	{
		fnCheckFileSysCorruption(status);
	}
}

/* **********************************************************************
// DESCRIPTION: storeMemoryToFile
//
// INPUTS:	file path to use for the directory (NULL if not required) "/<path>"
//		    filename for the resultant filename of the target
//			start address of memory to write (low)
//			end address of memory to write   (high)
// RETURNS:
// **********************************************************************/
#define	MAX_FILENAME_LENGTH		100
VOID storeMemoryToFile(char *filePath, char *fileName, void *pStart, void *pEnd)
{
	dbgTrace(DBG_LVL_INFO, "Running storeMemoryToFile(%s, %s)\r\n", filePath, fileName);
	STATUS status = NU_RETURN_ERROR;
	char	strFullName[MAX_FILENAME_LENGTH];
	if(filePath)
	{
		snprintf(strFullName, MAX_FILENAME_LENGTH, "%s/%s", filePath, fileName);
	}
	else
	{
		strncpy(strFullName, fileName, MAX_FILENAME_LENGTH);
	}
	// try an write something to our file system....
	if((filePath != NULL) && ((status = NU_Make_Dir(filePath)) != NU_SUCCESS))
	{
		if(status == NUF_EXIST)
			debugDisplayOnly("Directory %s already exists\r\n", filePath);
		else
			debugDisplayOnly("Unable to create the required subdirectory %s [%d]\r\n", filePath, status);
	}

	if((status == NU_SUCCESS) || (status == NUF_EXIST))
		status = NU_Open(strFullName,(PO_TEXT|PO_CREAT|PO_TRUNC|PO_WRONLY), PS_IWRITE);

	if(status >= 0)
	{

		// write the file in one gulp...
		INT iSz = NU_Write(status, (char *)pStart, (UINT)(pEnd - pStart));
		if((iSz > 0) && (iSz != (UINT)(pEnd - pStart)))
		{
			debugDisplayOnly("ERROR writing memory file, tried to write %u bytes, only wrote %u\r\n", ( sizeof(INTERTASK_LOG_ENTRY) * INTERTASK_LOG_SIZE), iSz);
		}
		else
		{
			if(iSz > 0)
				debugDisplayOnly("%u bytes of memory successfully written to file %s", iSz, strFullName);
			else
			{
				debugDisplayOnly("ERROR %d returned during memory write operation\r\n",iSz);
				fnCheckFileSysCorruption(iSz);
			}
		}
		STATUS closeStatus = NU_Close(status);
		if(closeStatus != NU_SUCCESS)
			fnCheckFileSysCorruption(closeStatus);
	}
	else
	{
		fnCheckFileSysCorruption(status);
	}
}

/* **********************************************************************
// DESCRIPTION: storeNetlogToFile
//
// INPUTS:	file path to use for the directory (NULL if not required) "/<path>"
//		    filename for the resultant filename of the target
// RETURNS:
// **********************************************************************/
#define	MAX_FILENAME_LENGTH		100
VOID storeNetlogToFile(char *filePath, char *fileName)
{
	dbgTrace(DBG_LVL_INFO, "Running storeNetlogToFile(%s, %s)\r\n", filePath, fileName);

	STATUS status = NU_RETURN_ERROR;

	char	strFullName[MAX_FILENAME_LENGTH];
	if(filePath)
	{
		snprintf(strFullName, MAX_FILENAME_LENGTH, "%s/%s", filePath, fileName);
	}
	else
	{
		strncpy(strFullName, fileName, MAX_FILENAME_LENGTH);
	}
	// try an write something to our file system....
	if((filePath != NULL) && ((status = NU_Make_Dir(filePath)) != NU_SUCCESS))
	{
		if(status == NUF_EXIST)
			debugDisplayOnly("Directory %s already exists\r\n", filePath);
		else
			debugDisplayOnly("Unable to create the required subdirectory %s [%d]\r\n", filePath, status);
	}

	if((status == NU_SUCCESS) || (status == NUF_EXIST))
		status = NU_Open(strFullName,(PO_TEXT|PO_CREAT|PO_TRUNC|PO_WRONLY), PS_IWRITE);

	if(status >= 0)
	{
		size_t  szCount = 0;
		INT iPass;
		char	strLine[NLOG_MAX_BUFFER_SIZE+3];
	    int iTimeLen = snprintf(strLine,NLOG_MAX_BUFFER_SIZE, "Start Time  %02d/%02d/%02d  %02d:%02d:%02d\r\n",
	    		globalNetlogStartTime.bMonth, globalNetlogStartTime.bDay, globalNetlogStartTime.bYear,
				globalNetlogStartTime.bHour, globalNetlogStartTime.bMinutes, globalNetlogStartTime.bSeconds);
	    if(iTimeLen > 0)
	    {
	    	STATUS writeStatus = NU_Write(status, strLine, strlen(strLine));
	    	fnCheckFileSysCorruption(writeStatus);
	    }
		for(iPass=0 ; (iPass < NLOG_MAX_ENTRIES) && (strlen(NLOG_Entry_List[iPass].log_msg) > 0) ; iPass++)
		{
			char *ptr;
			int iPass1 ;
			strcpy(strLine, NLOG_Entry_List[iPass].log_msg);
			strLine[NLOG_MAX_BUFFER_SIZE] = 0 ;
			for(iPass1 = 0, ptr = strLine; (iPass1 < NLOG_MAX_BUFFER_SIZE) && *ptr ; iPass1++, ptr++)
			{
				if(*ptr == DEBUG_CR) *ptr = '\t' ;
				if(*ptr == DEBUG_LF) *ptr = ' ' ;
			}
			*ptr++ = '\r';
			*ptr++ = '\n';
			*ptr++ = '\0';
			INT iLen = strlen(strLine) ;
			INT iSz = 0;

			iSz = NU_Write(status, strLine, iLen);

			if(iSz == iLen)
				szCount += iSz;
			else
				while(iSz < iLen)
				{

					if(iSz < 0)
					{
						debugDisplayOnly("Error writing file [%d]\r\n", iSz) ;
						fnCheckFileSysCorruption(iSz);
						break ;
					}
					else if(iSz < iLen)
					{
						debugDisplayOnly("NetLog Incorrect amount of data written, %d available only %d written\r\n",
								iLen, iLen - iSz) ;
						INT iBytes = NU_Write(status, strLine+iSz, iLen-iSz);
						fnCheckFileSysCorruption(iBytes);
						szCount += iBytes ;  //??1. miss the 1st iSz  2. suppose  iBytes > 0
						iSz += iBytes;
					}
					else
					{
						szCount += iSz;
					}
				}
		}

		if(status >= 0)
		{
			dbgTrace(DBG_LVL_INFO, "%u bytes of netlog successfully written to file %s\r\n", szCount, strFullName);
			STATUS closeStatus = NU_Close(status);
			if(closeStatus != NU_SUCCESS)
				fnCheckFileSysCorruption(closeStatus);
		}
	}
	else
	{
		fnCheckFileSysCorruption(status);
	}
}

VOID storeErrorLogToFile(char *filePath, char *fileName, char *bufferPtr, int bufferSize)
{
    char strHeader[NLOG_MAX_BUFFER_SIZE+2];
	char strLine[NLOG_MAX_BUFFER_SIZE+3];
    int length, bytes;
    STATUS status = NU_SUCCESS;
    bool truncate;

	dbgTrace(DBG_LVL_INFO, "errorlog saving to file");

	snprintf(strLine,NLOG_MAX_BUFFER_SIZE, "Start Time  %02d/%02d/%02d  %02d:%02d:%02d\r\n",
			globalNetlogStartTime.bMonth, globalNetlogStartTime.bDay, globalNetlogStartTime.bYear,
			globalNetlogStartTime.bHour, globalNetlogStartTime.bMinutes, globalNetlogStartTime.bSeconds);

	bytes = 0;
	*bufferPtr = 0;
	(void)strcpy(strHeader, "<CSDErrorLog>");
	(void)strcat(strHeader, "<DateTime>");
	(void)strcat(strHeader, strLine);
	(void)strcat(strHeader, "</DateTime>");

	(void)strcat(strHeader, "<Pcn>");
	(void)strcat(strHeader, CMOSSignature.bUicPcn);
	(void)strcat(strHeader, "</Pcn><PSDSerial>");
	(void)strcat(strHeader, CMOSSignature.bUicSN);
	(void)strcat(strHeader, "</PSDSerial>");
	(void)strcpy(bufferPtr, strHeader);

	addSystemErrorLog(bufferPtr + strlen(bufferPtr), bufferSize - strlen(bufferPtr));

	(void)strcat(bufferPtr, "</CSDErrorLog>");

	length = strlen(bufferPtr);
	if (length > 0 && length < bufferSize)
		status = fnFileAppend(filePath, fileName, bufferPtr, length, truncate);

	bytes += length;
	dbgTrace(DBG_LVL_INFO, "errorlog %d bytes saved", bytes);

}

VOID storeFramLogToFile(char *filePath, char *fileName, char *bufferPtr, int bufferSize)
{
	const int HEADER_ENTRY_SIZE = 50;
    char strHeader[NLOG_MAX_BUFFER_SIZE+2];
	char strLine[NLOG_MAX_BUFFER_SIZE+3];
    char entry_hdr_str[HEADER_ENTRY_SIZE];
    int length, bytes;
    char *p_msg;
    framlog_get_context_t ctx;
    fram_log_entry_hdr_t hdr;
    STATUS status = NU_SUCCESS;
    bool truncate;

	dbgTrace(DBG_LVL_INFO, "framlog saving to file");

	snprintf(strLine,NLOG_MAX_BUFFER_SIZE, "Start Time  %02d/%02d/%02d  %02d:%02d:%02d\r\n",
			globalNetlogStartTime.bMonth, globalNetlogStartTime.bDay, globalNetlogStartTime.bYear,
			globalNetlogStartTime.bHour, globalNetlogStartTime.bMinutes, globalNetlogStartTime.bSeconds);

	truncate =  true;
	bytes = 0;
	(void)strcpy(strHeader, "CSD Fram Log\r\n");
	(void)strcat(strHeader, "DateTime ");
	(void)strcat(strHeader, strLine);
	(void)strcat(strHeader, "\r\n");
	(void)strcat(strHeader, "Pcn ");
	(void)strcat(strHeader, CMOSSignature.bUicPcn);
	(void)strcat(strHeader, "\r\nPSD Serial ");
	(void)strcat(strHeader, CMOSSignature.bUicSN);
	(void)strcat(strHeader, "\r\n\r\n");
	(void)strcpy(bufferPtr, strHeader);

	(void)strcat(bufferPtr,"\r\nFram log\r\n");
	(void)strcat(bufferPtr,"ctx    seq #  len    repeat  timestamp  msg\r\n");
	(void)strcat(bufferPtr,"-----  -----  -----  ------  ---------  ------------------------------------------------------------------------------------------");

	if (Framlog_GetFirstEntry(&ctx, &hdr, &p_msg))
	{
		do
		{
			length = strlen(bufferPtr);
			sprintf(entry_hdr_str,"\r\n%5X  %5d  %5d  %6u  %9u  ", ctx.idx, hdr.seq_nr, hdr.len, hdr.repeat_count, hdr.timestamp * 10);
			if (length + strlen(entry_hdr_str) + strlen(p_msg) > bufferSize)
			{
	            dbgTrace(DBG_LVL_INFO, "framlog append %d bytes", length);
				status = fnFileAppend(filePath, fileName, bufferPtr, length, truncate);
				truncate = false;
				bytes += length;
				*bufferPtr = 0;
			}
			(void)strcat(bufferPtr, entry_hdr_str);
			(void)strcat(bufferPtr, p_msg);
		} while(Framlog_GetNextEntry(&ctx, &hdr, &p_msg));

		(void)strcat(bufferPtr, "\r\n\r\nFram log end\r\n");

	}

	length = strlen(bufferPtr);
	if (length > 0 && length < bufferSize)
		status = fnFileAppend(filePath, fileName, bufferPtr, length, truncate);

	bytes += length;
    dbgTrace(DBG_LVL_INFO, "framlog %d bytes saved", bytes);

}

VOID storeTrnFramLogToFile(char *filePath, char *fileName, char *bufferPtr, int bufferSize)
{
    char strHeader[NLOG_MAX_BUFFER_SIZE+2];
	char strLine[NLOG_MAX_BUFFER_SIZE+3];
    int length, count, bytes;
    int32_t out_length;
    STATUS status = NU_SUCCESS;
    bool truncate;

	dbgTrace(DBG_LVL_INFO, "trm-framlog saving to file");

	snprintf(strLine,NLOG_MAX_BUFFER_SIZE, "Start Time  %02d/%02d/%02d  %02d:%02d:%02d\r\n",
			globalNetlogStartTime.bMonth, globalNetlogStartTime.bDay, globalNetlogStartTime.bYear,
			globalNetlogStartTime.bHour, globalNetlogStartTime.bMinutes, globalNetlogStartTime.bSeconds);

    truncate =  true;
	bytes = 0;
    (void)strcpy(strHeader, "<CSDTrmLog>");
    (void)strcat(strHeader, "<DateTime>");
    (void)strcat(strHeader, strLine);
    (void)strcat(strHeader, "</DateTime>");
    (void)strcat(strHeader, "<Pcn>");
    (void)strcat(strHeader, CMOSSignature.bUicPcn);
    (void)strcat(strHeader, "</Pcn><PSDSerial>");
    (void)strcat(strHeader, CMOSSignature.bUicSN);
    (void)strcat(strHeader, "</PSDSerial>");
	(void)strcpy(bufferPtr, strHeader);

	length = strlen(bufferPtr);
    status = generate_fram_log_xml(bufferPtr + length, bufferSize - length, &out_length);

	length = strlen(bufferPtr);
	if (length > 0 && length < bufferSize)
	{
        dbgTrace(DBG_LVL_INFO, "trm-framlog append %d bytes", length);
		status = fnFileAppend(filePath, fileName, bufferPtr, length, truncate);
		truncate = false;
		bytes += length;
		*bufferPtr = 0;
	}

	 if(generate_fram_bcuket_cnt(0))
	 {
		 //bucket 1
		 (void)strcpy(bufferPtr, "<TrmArea1>");
		 for( count = 0 ; count < CFG_NUM_RECORDS; count++)
		 {
			 length = strlen(bufferPtr);
			 generate_fram_log_bcuket_xml(bufferPtr + length, bufferSize - length, &out_length, 0, count);
		 }
		 (void)strcat(bufferPtr, "</TrmArea1>");

		  	length = strlen(bufferPtr);
			if (length > 0 && length < bufferSize)
			{
				dbgTrace(DBG_LVL_INFO, "trm-framlog append %d bytes", length);
				status = fnFileAppend(filePath, fileName, bufferPtr, length, truncate);
				truncate = false;
				bytes += length;
				*bufferPtr = 0;
			}
	 }

	 if(generate_fram_bcuket_cnt(1))
	 {
		 //Bucket2
		 (void)strcpy(bufferPtr, "<TrmArea2>");
		 for( count = 0 ; count < CFG_NUM_RECORDS; count++)
		 {
			 length = strlen(bufferPtr);
			 generate_fram_log_bcuket_xml(bufferPtr + length, bufferSize - length, &out_length, 1, count);
		 }
		 (void)strcat(bufferPtr, "</TrmArea2>");
	 }

	 (void)strcat(bufferPtr, "</CSDTrmLog>");

  	 length = strlen(bufferPtr);
	 if (length > 0 && length < bufferSize)
			status = fnFileAppend(filePath, fileName, bufferPtr, length, truncate);

	 bytes += length;
     dbgTrace(DBG_LVL_INFO, "trm-framlog %d bytes saved", bytes);

}

VOID storeDiagnosticsLogToFile(char *filePath, char *fileName, char *bufferPtr, int bufferSize)
{
    char strHeader[NLOG_MAX_BUFFER_SIZE+2];
	char strLine[NLOG_MAX_BUFFER_SIZE+3];
    int length, bytes;
    STATUS status = NU_SUCCESS;
    bool truncate;

	dbgTrace(DBG_LVL_INFO, "diagnosticslog saving to file");

	snprintf(strLine,NLOG_MAX_BUFFER_SIZE, "Start Time  %02d/%02d/%02d  %02d:%02d:%02d\r\n",
			globalNetlogStartTime.bMonth, globalNetlogStartTime.bDay, globalNetlogStartTime.bYear,
			globalNetlogStartTime.bHour, globalNetlogStartTime.bMinutes, globalNetlogStartTime.bSeconds);

    truncate =  true;
	*bufferPtr = 0;
	(void)strcpy(strHeader, "CSDDiagnosticsLog\r\n");
	(void)strcat(strHeader, "DateTime ");
	(void)strcat(strHeader, strLine);
	(void)strcat(strHeader, "\r\n");
	(void)strcat(strHeader, "Pcn ");
	(void)strcat(strHeader, CMOSSignature.bUicPcn);
	(void)strcat(strHeader, "\r\nPSD Serial ");
	(void)strcat(strHeader, CMOSSignature.bUicSN);
	(void)strcat(strHeader, "\r\n");
	(void)strcpy(bufferPtr, strHeader);

	//this call will save the file to the Logs folder
	addDiagnosticsLog(bufferPtr + strlen(bufferPtr), bufferSize - strlen(bufferPtr));

	length = strlen(bufferPtr);
	 if (length > 0 && length < bufferSize)
			status = fnFileAppend(filePath, fileName, bufferPtr, length, truncate);

	bytes += length;
    dbgTrace(DBG_LVL_INFO, "diagnosticslog %d bytes saved", bytes);
}


