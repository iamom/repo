/************************************************************************
*	PROJECT:		Janus
*	MODULE NAME:	$Workfile:   PUBUTILS.C  $
*	REVISION:		$Revision:   1.1  $
*	
*	DESCRIPTION:	General purpose utility functions for public use.
*
* ----------------------------------------------------------------------
*               Copyright (c) 2003 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*
*	REVISION HISTORY:
* 	$Log:   H:/group/SPARK/ARCHIVES/DM150/uic_app/PUBUTILS.C_v  $
 * 
 *    Rev 1.1   19 Mar 2003 10:13:42   defilcj
 * changes for OS_06 build
*     
*************************************************************************/

#include <string.h>	
#include <stdio.h>	
#include <stdlib.h>
#include <math.h>
#include "global.h"                                   
#include "pbos.h"
#include "clock.h"
#include "custdat.h"


/* externs */
extern unsigned char CAI_CMOS[];


/********************************************************************************/
/* TITLE: GetCAICMOSByte                                                        */
/* AUTHOR: Brian Hannigan                                                       */
/* INPUT:  offset = location in CAI CMOS area to read.  Range = 0 to 1023       */
/* DESCRIPTION: Returns contents of CAI CMOS area at specified offset           */
/* 				Returns zero if offset is out of range                          */
/********************************************************************************/
unsigned char GetCAICMOSByte(unsigned short offset)
{
	if ((offset >= 0) & (offset <=1023))
	{
		return (CAI_CMOS[offset]);
	}
	else
		return(0);
}

/********************************************************************************/
/* TITLE: SetCAICMOSByte                                                        */
/* AUTHOR: Brian Hannigan                                                       */
/* INPUT:  offset = location in CAI CMOS area to write. Range = 0 to 1023       */
/*			bValue is the value to write                 						*/
/* DESCRIPTION: Sets one byte in CAI CMOS 					      			    */
/* OUTPUT: Returns successful if offset is in range.                            */
/*		   Returns failure if offset out of range.    							*/
/********************************************************************************/
BOOL SetCAICMOSByte(unsigned short offset, unsigned char bValue)
{
	if ((offset >= 0) & (offset <=1023))
	{
		CAI_CMOS[offset] = bValue;
		return (SUCCESSFUL);
	}
	else
		return(FAILURE);
}
