/*************************************************************************
*   PROJECT:        Horizon CSD2-3
*	MODULE NAME:	afsapi.c
*
*	DESCRIPTION:	Implementation file for Archive File System 
*					Application Programming Interfacing (AFSApi)
*
* ----------------------------------------------------------------------
*               Copyright (c) 2017 Pitney Bowes Inc.
*                    37 Executive drive
*                   Danbury, Connecticut  06810
* ----------------------------------------------------------------------
*
*************************************************************************/
#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include "posix.h"
#include "AfsApi.h"
#include "AfsFile.h"
#include "CRC16.h"

extern unsigned long crc32(unsigned long, unsigned char *, unsigned int);
extern unsigned short crc16c(unsigned short, unsigned char *, unsigned int);
extern void fnSwapBytesForLongs( unsigned char *bBytesToSwap );
extern unsigned long UnZipBlock(unsigned char * pZippedData, 
				  	  	  unsigned long ZipDataLen, 
						  unsigned char * pUnzipBuffer, 
						  unsigned long UnzipDataLen, int *);

//
// Data Types
//
#define	AFS_ARCHIVE_SIG		0xAFCBAFCB					//Archive File Control Block Signature
//
enum AFS_Mode { AFS_MODE_NATIVE = 0, AFS_MODE_GENERAL };
//
// AFS File Control Block
//
typedef struct
{
	unsigned long ulAFSID;
	unsigned char ucMode;
	FILE *pFileHandle;
	unsigned long ulDirStartPosition;
	AFS_HEADER sAFSHdr;
	char	*pPrefix;
	unsigned char *pBuffer;
	unsigned long ulAllocSize;
	unsigned long ulBlockCount;

}AFILE;
//
// Private Prototypes - Data
//
static AFILE *AFSOpen ( char *pArchiveName);
static AFILE *isAFSValidHandle( AFH Handle);
static int AFSParseFileName (char *pFullName, char *pPrefix, char *pArchive);
static int AFSRead(AFILE *pAFS, int iStartOffset, unsigned char *pBuffer, unsigned int iLength);
static int AFSGetDirectoryEntryByIndex( AFILE *pAFS, unsigned int iIndex, AFS_DIR *pAFSDirContainer);
static int AFSGetDirectoryEntryByName( AFILE *pAFS, const char *pFileName, AFS_DIR *pAFSDirContainer);
static int AFSArchiveCheck( AFILE *pAFS, void *pCheckData );
static int AFSCheckFile( AFILE *pArchive, const AFS_DIR *pAfsDir);
static int AFSExtractBlock(AFILE *pAFS, unsigned long ulAttrib, unsigned long ulOffset, int *piReadLen);   // The Uncompressed Length
//
// Public Interface Functions
//
/***************************************************************************************
*    Function: OpenArchive()
*
* Description: Opens an archive file - checks that it is a valid archive; 
*              pFilename may contain a full path name including filesystem prefix.
*              
*   Arguments: pFileName - filename of Archive.
*
*      Returns: -1 - File Not Found; 
*				>0 - File Handle
*
*      Author: Mark Harris
*
*****************************************************************************************/
AFH	OpenArchive(char *pArchivename )
{
	char  cPrefix[MAX_ARCH_FILENAME];
	char  cArchive[MAX_ARCH_FILENAME];
	AFILE *pArchive;
	int iRead;
	AFH AFHHandle = (AFH)AFS_ARCHIVE_NOT_FOUND;
	
	(void)AFSParseFileName( pArchivename, cPrefix, cArchive);
	pArchive = AFSOpen(cArchive);
	if(pArchive)
	{
		// Successfully Opened - Check signature and Load Header
		iRead = AFSRead(pArchive, (int)0, (unsigned char *)&pArchive->sAFSHdr, sizeof(AFS_HEADER));
		if(iRead == (int)sizeof(AFS_HEADER))
		{
			if(memcmp((void *)&pArchive->sAFSHdr.AfsSig, (void *)AFS_FILE_SIG, sizeof(AFS_FILE_SIG)-1) == 0)
			{
				if( pArchive->sAFSHdr.AfsVer <= AFS_ARCHIVE_VERSION )
				{
					if(strlen(cPrefix) > 0)
					{
						pArchive->pPrefix = (char *)calloc(MAX_ARCH_FILENAME, 1);
						if(pArchive->pPrefix)
							strcpy(pArchive->pPrefix, cPrefix);
					}
					pArchive->ulDirStartPosition = sizeof(AFS_HEADER) + (pArchive->sAFSHdr.ulSecuritySize * sizeof(long));
					AFHHandle = (AFH)pArchive;
				}
				else
				{
					AFHHandle = (AFH)AFS_ARCHIVE_VERSION_CHECK;
				}
			}
			else
			{
				AFHHandle = (AFH)AFS_NOT_VALID_ARCHIVE;
			}
		}
		else
		{
			AFHHandle = (AFH)AFS_NOT_VALID_ARCHIVE;
		}
	}
	if( AFHHandle == (AFH)AFS_NOT_VALID_ARCHIVE ||
		AFHHandle == (AFH)AFS_ARCHIVE_VERSION_CHECK)
	{
		(void)CloseArchive((AFH)pArchive);
	}
	
	return AFHHandle;
}
/***************************************************************************************
*    Function: HowManyFilesAreInArchive()
*
* Description: Returns the number of files in an archive. 
*              
*   Arguments: AFH is the handle returned from an OpenArchive()
*
*      Return: -253 - Directory Error
*			   -246 - Invalid Archive Handle
*
*      Author: Mark Harris
*
*****************************************************************************************/
int HowManyFilesAreInArchive( AFH FileHandle )
{
	AFILE	*pArchive;
	int iResult = AFS_OK;

	pArchive = isAFSValidHandle(FileHandle);
	if(pArchive)
	{
		iResult = (int)pArchive->sAFSHdr.ulFileCount;
	}
	else
	{
		iResult = (int)AFS_INVALID_HANDLE;
	}
	return iResult;
}
/***************************************************************************************
*    Function: GetListOfFileInArchive()
*
* Description: Returns a list of information about the files in the archive. 
*              
*   Arguments: AFH is the handle returned from an OpenArchive()
*              AFP *pFileInfo - Container of large enough to hold File Information for
*                               each file.
*
*      Return: 0 - 0xFFFFFF03 - Directory Error
*			   -246  - Invalid Archive Handle
*
*      Author: Mark Harris
*
*****************************************************************************************/
int GetListOfFilesInArchive( AFH ArchiveHandle, AFP *pFileInfo )
{
	AFILE	*pArchive;
	int iResult = AFS_OK;
	AFS_DIR AfsDir;
	unsigned int i;

	pArchive = isAFSValidHandle( ArchiveHandle );
	if(pArchive != NULL)
	{
		for(i = 0; i < pArchive->sAFSHdr.ulFileCount; i++)
		{
			if(AFSGetDirectoryEntryByIndex(pArchive, i, &AfsDir) == 0)
				break;
			strcpy((pFileInfo + i)->cFileName, AfsDir.cFilename);
			(pFileInfo + i)->ulFileSize = AfsDir.ulFileSize;
			(pFileInfo + i)->sFDTStamp =  AfsDir.dtFileDateTime;
		}
		if( i == pArchive->sAFSHdr.ulFileCount)
			iResult = i;
		else
			iResult = (int)AFS_DIRECTORY_ERROR;
	}
	else
	{
		iResult = (int)AFS_INVALID_HANDLE;
	}
	return iResult;
}
/***************************************************************************************
*    Function: ExtractFileFromArchive()
*
* Description: Copies a the file pFileName from archive represented by FileHandle, 
*              to the destination
*
*              
*   Arguments: AFH FileHandle is the handle returned from an OpenArchive()
*              unsigned char *pFileName - Name of file to extract from archive.
*                            *pDestFile - optional (NULL Allowed) - Destination file.
*
*      Return:    0 - Extract OK
*			   -252 - File Not In Archive Error
*			   -251 - File extraction failed
*			   -250 - File fail to uncompress failed
*			   -249 - Create Destination File failed
*			   -246 - Invalid Archive Handle
*			   -242 - Error Writing Destination file.
*			   -241 - Invalid Destination File Spec
*
*      Author: Mark Harris
*
*****************************************************************************************/
int ExtractFileFromArchive( AFH ArchiveHandle, char *pFileName, char *pDestFile )
{
	AFILE	*pArchive;
	AFS_DIR AfsDir;
	char  cPrefix[MAX_ARCH_FILENAME];
	char  cFileName[MAX_ARCH_FILENAME];
	char  cFullFileName[MAX_ARCH_FILENAME * 2];
	char  *pOutFile;
	unsigned long ulOffset;
	unsigned long ulFileSize;
	int  iWriteLen;
	int  iReadLen;
	size_t  iWritten;
	FILE *pFile;
	int iResult = AFS_OK;

	pArchive = isAFSValidHandle( ArchiveHandle );
	if(pArchive != NULL)
	{
		pArchive->ulBlockCount = 0;
		// Prepare Source File
		if(AFSGetDirectoryEntryByName(pArchive, pFileName, &AfsDir) != AFS_OK)
			iResult = (int)AFS_FILE_NOT_IN_ARCHIVE;
		else
		{
			//
			// Prepare Destination File Name
			//
			memset(cFullFileName, 0, (MAX_ARCH_FILENAME * 2));
			if(pDestFile)
			{
				(void)AFSParseFileName( pDestFile, cPrefix, cFileName);
				pOutFile = pDestFile;
			}
			else
			{
				pOutFile = pFileName;
			}
			//
			// Prepare Destination File
			//
			if(strlen(pOutFile) < DIRNAMESIZE )
			{
				if(pArchive->pPrefix && (pDestFile == NULL))
				{
					strcpy(cFullFileName, pArchive->pPrefix );
					strcat(cFullFileName, pOutFile);
					pOutFile = cFullFileName;
				}

				pFile = fopen(pOutFile, "rwb");
				if( pFile == NULL)
					iResult = (int)AFS_FAIL_FILE_CREATE;
			}
			else
			{
				iResult = AFS_INVALID_FILE_SPEC;
			}
		}
		if( iResult == AFS_OK)
		{
			// Start Copying file ....
			ulOffset = AfsDir.ilFileOffset;
			ulFileSize = AfsDir.ulFileSize;
			while(ulFileSize)
			{
				iWriteLen = AFSExtractBlock(pArchive, AfsDir.ulAttrib, ulOffset, &iReadLen);
				//
				// Write Extracted date to new file 
				//
				if(iWriteLen)
				{
					iWritten = fwrite((const void *)pArchive->pBuffer, 1, iWriteLen, pFile);
					if(iWriteLen != iWritten)
					{
						iResult = (int)AFS_FILE_WRITE_ERROR;
						break;
					}
				}
				else
				{
					break;
				}
				ulFileSize -= (unsigned long)iReadLen;
				ulOffset = ~0;
			}
			(void)fclose(pFile);
			if(pArchive->pBuffer)
			{
				free(pArchive->pBuffer);
				pArchive->pBuffer = 0;
				pArchive->ulAllocSize = 0;
			}
			if(ulFileSize && iResult == AFS_OK)
				iResult = (int)AFS_FILE_EXTRACT_FAILED;
		}
	}
	else
	{
		iResult = (int)AFS_INVALID_HANDLE;
	}
	return iResult;
}
/***************************************************************************************
*    Function: ValidateArchive()
*
* Description: Runs the specified security check for the archive file.
*
*              
*   Arguments: AFH FileHandle is the handle returned from an OpenArchive()
*              unsigned char *pFileName - Name of file to extract from archive.
*                            *pDestFile - optional (NULL Allowed) - Destination file.
*
*      Return: 0xFFFFFF08 - Validate Archive failed
*			   -246  - Invalid Archive Handle
*
*      Author: Mark Harris
*
*****************************************************************************************/
int ValidateArchive( AFH ArchiveHandle )
{
	AFILE	*pArchive;
	unsigned char ucCheck[2];
	void *pCheckData = NULL;
	void *pFileCheckData = NULL;
	unsigned int uiCheckDataSize = 0;
	int iResult = AFS_OK;
	unsigned int sRead = 0;

	pArchive = isAFSValidHandle( ArchiveHandle );
	if(pArchive != NULL)
	{
		// Check that file size matches Header's File size.
		sRead = (unsigned int)AFSRead(pArchive, (int)pArchive->sAFSHdr.ulArchiveSize - 1, ucCheck, 2);
		if(sRead != 1 )
			iResult = AFS_INVALID_ARCHIVE_SIZE;

		if(pArchive->sAFSHdr.ulSecurityCheckType != (unsigned long)SEC_NONE && iResult == AFS_OK)
		{
			uiCheckDataSize = (unsigned int)(pArchive->sAFSHdr.ulSecuritySize * 4);
			pFileCheckData = calloc(1, uiCheckDataSize);
			if(pFileCheckData)
				sRead = (unsigned int)AFSRead(pArchive, (int)sizeof(AFS_HEADER), (unsigned char *)pFileCheckData, uiCheckDataSize);
			if(sRead == uiCheckDataSize)
			{
				pCheckData = calloc(1, (unsigned int)uiCheckDataSize);
				if(pCheckData)
					iResult = AFSArchiveCheck(pArchive, pCheckData);
				else
					iResult = (int)AFS_ARCHIVE_VALIDATE_FAIL;
				if(iResult == AFS_OK)
				{
					iResult = memcmp(pFileCheckData, pCheckData, uiCheckDataSize); 
					if(iResult)
						iResult = (int)AFS_ARCHIVE_VALIDATE_FAIL;
					else
						iResult = (int)AFS_OK;
				}
				free(pFileCheckData);
				free(pCheckData);
			}
			else
			{
				if(pFileCheckData)
					free( pFileCheckData);
				iResult = (int)AFS_ARCHIVE_VALIDATE_FAIL;
			}
		}
	}
	else
	{
		iResult = (int)AFS_INVALID_HANDLE;
	}
	return iResult;
}
/***************************************************************************************
*    Function: ValidateFileInArchive()
*
* Description: Runs the specified security check for the file pFilename in an archive.
*
*              
*   Arguments: AFH FileHandle is the handle returned from an OpenArchive()
*			   *pFilename - Filename to check.
*
*      Return:    0  - OK
*              -247  - Validate File Failed
*			   -246  - Invalid Archive Handle
*
*      Author: Mark Harris
*
*****************************************************************************************/
int ValidateFileInArchive( AFH ArchiveHandle, const char *pFileName )
{
	AFILE	*pArchive;
	AFS_DIR AfsDir;
	int iResult = AFS_OK;

	pArchive = isAFSValidHandle( ArchiveHandle );
	if(pArchive != NULL)
	{
		if(AFSGetDirectoryEntryByName(pArchive, pFileName, &AfsDir) != AFS_OK)
				iResult = (int)AFS_FILE_NOT_IN_ARCHIVE;
		else
		{
			iResult = AFSCheckFile(pArchive, &AfsDir);
		}
	}
	else
	{
		iResult = (int)AFS_INVALID_HANDLE;
	}
	return iResult;
}
/***************************************************************************************
*    Function: CloseArchive()
*
* Description: Close Archive file - freeing handle etc.
*              
*   Arguments: AFH FileHandle is the handle returned from an OpenArchive()
*
*      Return:   0 - OK
*			  -246 - Invalid Archive Handle
*
*      Author: Mark Harris
*
*****************************************************************************************/
int CloseArchive( AFH ArchiveHandle )
{
	AFILE	*pArchive;
	int iResult = AFS_OK;

	pArchive = isAFSValidHandle( ArchiveHandle );
	if(pArchive != NULL)
	{
		fclose(pArchive->pFileHandle);
		if(pArchive->pPrefix)
			free(pArchive->pPrefix);
		free(pArchive);
	}
	else
	{
		iResult = (int)AFS_INVALID_HANDLE;
	}
	return iResult;
}
/*****************************************************************************************
* Private Function Implementation
******************************************************************************************/
/***************************************************************************************
*    Function: AFSOpen()
*
* Description: Opens an archive. Allocates a AFILE control structure
*              
*   Arguments: pucFilenanme - File to open
*			   
*      Return: AFILE *ptr; NULL if not valid Handle
*
*      Author: Mark Harris
*
*****************************************************************************************/
static AFILE *AFSOpen(char *pcFilename)
{

	FILE *pFile;
	AFILE *pHandle = NULL;

	pHandle = (AFILE *)calloc(1, sizeof(AFILE));
	if( pHandle )
	{
		pFile = fopen(pcFilename,"rb");
		if(pFile == NULL)
		{
			free(pHandle);
			pHandle = NULL;
		}
		if(pHandle)
		{
			pHandle->ulAFSID = AFS_ARCHIVE_SIG;
		}
	}
	return pHandle;
}
/***************************************************************************************
*    Function: isAFSValidHandle()
*
*****************************************************************************************/
static AFILE *isAFSValidHandle(AFH Handle)
{
	AFILE *pFile = (AFILE *)Handle;
	if((Handle == AFS_ARCHIVE_NOT_FOUND) ||
			(pFile && (pFile->ulAFSID != AFS_ARCHIVE_SIG)))
		pFile = NULL;
	return pFile;
}

/***************************************************************************************
*    Function: AFSParseFileName()
*
* Description: Scans a full path name and returns the prefix, archive, and filenames.
*              
*   Arguments: pFullName - File to open
*			   
*      Return: AFILE *ptr; NULL if not valid Handle
*
*      Author: Mark Harris
*
*****************************************************************************************/
static int AFSParseFileName (char *pFullName, char *pPrefix, char *pArchive)
{
	char *pCharPtr;
	int ucSep;
	int iResult = 0;

	
	*pPrefix = 0;
	*pArchive = 0;

	ucSep = '/';

	if(pFullName)
	{
		pCharPtr = strchr(pFullName, ucSep);
		if(pCharPtr == NULL)
		{
			ucSep = '\\';
			pCharPtr = strchr(pFullName, ucSep);
		}
		if(pCharPtr != NULL)
		{
			*pPrefix++ = *pCharPtr++;
			while(pCharPtr && *pCharPtr)
			{
				*pPrefix++ = *pCharPtr;
				if(*pCharPtr == ucSep)
					break;
				pCharPtr++;
			}
			*pPrefix = 0;
			while(pCharPtr && *pCharPtr)
			{
				++pCharPtr;
				*pArchive++ = *pCharPtr;
				if(*pCharPtr == ucSep)
					break;
			}
			*pArchive = 0;
		}
		else
		{
			pCharPtr = pFullName;
			while(*pCharPtr)
				*pArchive++ = *pCharPtr++;
			*pArchive = 0;
		}
	}
	else
		iResult = -1;

	return iResult;
		
}
/***************************************************************************************
*    Function: AFSRead()
*
* Description: Reads data from archive file. .
*              
*   Arguments: pAFS - Archive File Control Block
*              iStartOffset - Move pointer to this location before read.
*                             -1 - Read from current position.
*              pBuffer - Buffer to load read data
*			   iLength - Amount to read
*			   
*      Return: 0 - of not read, # bytes read.
*
*      Author: Mark Harris
*
*****************************************************************************************/
static int AFSRead(AFILE *pAFS, int iStartOffset, unsigned char *pBuffer, unsigned int iLength)
{
	size_t iRead = 0;
	char *pTemp = (char *)pBuffer;

	if(iStartOffset != -1)
	{
		(void)fseek(pAFS->pFileHandle, (unsigned long)iStartOffset, SEEK_SET );
	}
	if(pBuffer && iLength)
		iRead = fread(pTemp, 1, iLength, pAFS->pFileHandle);

	if(pBuffer && iLength)
	{
		if(iRead < 0)
			iRead = 0;
	}
	return iRead;
}
/***************************************************************************************
*    Function: AFSGetDirectoryEntryByIndex()
*
* Description: Reads data from archive file. .
*              
*   Arguments: pAFS - Archive File Control Block
*              iIndex - The Directory Entry - 0 - based
*              pAFSDirContainer - Buffer to load directory data
*			   
*      Return: 0 - of not read, # bytes read - sizeof(AFS_DIR)
*
*      Author: Mark Harris
*
*****************************************************************************************/
static int AFSGetDirectoryEntryByIndex( AFILE *pAFS, unsigned int uiIndex, AFS_DIR *pAFSDirContainer)
{
	int iRead;


	if( uiIndex < pAFS->sAFSHdr.ulFileCount)
		iRead = AFSRead(pAFS, (int)(pAFS->ulDirStartPosition + (uiIndex * sizeof(AFS_DIR))),
		                (unsigned char *)pAFSDirContainer,(int)sizeof(AFS_DIR));
	else
		iRead = 0;

	return iRead;
}
/***************************************************************************************
*    Function: AFSGetDirectorEntryByName()
*
* Description: Reads data from archive file. .
*              
*   Arguments: pAFS - Archive File Control Block
*              pFileName - The Directory Entry for this file name.
*              pAFSDirContainer - Buffer to load directory data
*			   
*      Return: 0 - Successful, AFS_FILE_NOT_IN_ARCHIVE;
*
*      Author: Mark Harris
*
*****************************************************************************************/
static int AFSGetDirectoryEntryByName( AFILE *pAFS, const char *pFileName, AFS_DIR *pAFSDirContainer)
{
	unsigned int i;
	int iResult = AFS_OK;

	for(i = 0; i < pAFS->sAFSHdr.ulFileCount; i++)
	{
		(void)AFSGetDirectoryEntryByIndex(pAFS, i, pAFSDirContainer);
		if(strcmp(pFileName, pAFSDirContainer->cFilename) == 0)
			break;
	}
	if( i == pAFS->sAFSHdr.ulFileCount)
		iResult = (int)AFS_FILE_NOT_IN_ARCHIVE;

	return iResult;
}
/***************************************************************************************
*    Function: AFSArchiveCheck()
*
* Description: Creates the check data for the Archive
*              
*   Arguments: pArchive - Pointer to Archive Control Block;
*              pCheckData - void pointer to container the size of the check data
*
*      Return: 0 - Successful; <>0 if unable to crate check data
*
*      Author: Mark Harris
*
*****************************************************************************************/
static int AFSArchiveCheck( AFILE *pArchive, void *pCheckData )
{
	unsigned long ulSecurityType;
	int sRead;
	unsigned char ucTemp;
	int bEof = 0;
	unsigned long ulCRCxxCheck = 0;
	int iResult = (AFH)AFS_OK;

	ulSecurityType = pArchive->sAFSHdr.ulSecurityCheckType;

	switch( ulSecurityType )
	{
		case SEC_CRC16:
				ulCRCxxCheck = _crc16((unsigned short)ulCRCxxCheck, (char *) &pArchive->sAFSHdr, sizeof(AFS_HEADER));
				break;
		case SEC_CRC16C:
				ulCRCxxCheck = crc16c((unsigned short)ulCRCxxCheck, (unsigned char *)&pArchive->sAFSHdr, sizeof(AFS_HEADER));
				break;
		case SEC_CRC32:
				ulCRCxxCheck = crc32(ulCRCxxCheck, (unsigned char *)&pArchive->sAFSHdr, sizeof(AFS_HEADER));
				break;
		default:
			iResult = (int)AFS_UNKNOWN_ARCHIVE_CHECK_METHOD;
			break;
	}
	if(iResult == AFS_OK)
	{
		// Seek Past Header and Check Data
		sRead = AFSRead(pArchive, (int)(sizeof(AFS_HEADER) + (pArchive->sAFSHdr.ulSecuritySize * 4)), NULL, 0);
		// Read Rest of file
		while(!bEof )
		{
			sRead = AFSRead(pArchive, (int)-1, &ucTemp, 1);
			if(sRead)
			{
				switch( ulSecurityType )
				{
					case SEC_CRC16:
							ulCRCxxCheck = _crc16(ulCRCxxCheck, (char *) &ucTemp, sRead);
							break;
					case SEC_CRC16C:
							ulCRCxxCheck = crc16c((unsigned short)ulCRCxxCheck, &ucTemp, sRead);
							break;
					case SEC_CRC32:
							ulCRCxxCheck = crc32(ulCRCxxCheck, &ucTemp, sRead);
							break;
					default:
						bEof = 1;
						iResult = (int)AFS_UNKNOWN_ARCHIVE_CHECK_METHOD;
						break;
				}
			}
			else
				bEof = 1;
		}
	}
	switch( ulSecurityType )
	{
		case SEC_CRC16:
		case SEC_CRC16C:
		case SEC_CRC32:
				memcpy(pCheckData, (void *)&ulCRCxxCheck, sizeof(unsigned long));
				break;
		default:
			iResult = (int)AFS_UNKNOWN_ARCHIVE_CHECK_METHOD;
			break;
	}
	return iResult;
}
/***************************************************************************************
*    Function: AFSCheckFile()
*
* Description: Validates a file in an Archive
*              If file is compressed check that the compression is a valid format.
*			   If file has a checksum - will verify the checksum.
*              
*   Arguments: pArchive - a pointer to the archive control block;
*              pAfsDir  - Directory Entry for File
*
*      Return: 0 - if File is Validates
*				< 0 for Errors
*
*      Author: Mark Harris
*
*****************************************************************************************/
static int AFSCheckFile( AFILE *pArchive, const AFS_DIR *pAfsDir)
{
	unsigned char Temp;
	unsigned long ulMethod;
	int sRead;
	unsigned long ulBlockSize;
	unsigned long ulBlockCount = 0;
	unsigned long ulFileSize = 0;
	unsigned long ulFileChksum = 0;
	int iResult = AFS_OK;

	if(pAfsDir->ulAttrib & ATR_MASK_COMPRS)
	{
		// Walk the block structure and check against File size.
		ulFileSize = AFSRead(pArchive, (int)pAfsDir->ilFileOffset, (unsigned char*)&ulBlockCount, sizeof(unsigned long));
		fnSwapBytesForLongs((unsigned char *)&ulBlockCount);
		while(ulBlockCount)
		{
			ulFileSize += (unsigned long)AFSRead(pArchive, (int)(pAfsDir->ilFileOffset + ulFileSize), (unsigned char*)&ulBlockSize, sizeof(unsigned long));
			fnSwapBytesForLongs((unsigned char *)&ulBlockSize);
			ulFileSize += ulBlockSize;
			ulBlockCount--;
			if(ulFileSize > pAfsDir->ulFileSize)
				break;
		}
		if(ulFileSize != pAfsDir->ulFileSize)
			iResult = (int)AFS_INVALID_FILE_COMPRESSION;
	}
	if(iResult == AFS_OK)
	{
		ulMethod = (pAfsDir->ulAttrib & ATR_MASK_CKSUM);
		if((pAfsDir->ulAttrib & ATR_MASK_CKSUM) != ATR_CHKSUM_NONE)
		{
			ulFileSize = pAfsDir->ulFileSize;
			sRead = AFSRead(pArchive, (int)pAfsDir->ilFileOffset, &Temp, 1);
			while(sRead && ulFileSize)
			{
				switch(ulMethod)
				{
					case ATR_CHKSUM_CRC16:
						ulFileChksum = _crc16((unsigned short)ulFileChksum, (char *) &Temp, sRead);
						break;
					case ATR_CHKSUM_CRC16C:
						ulFileChksum = crc16c((unsigned short)ulFileChksum, &Temp, sRead);
						break;
					case ATR_CHKSUM_CRC32:
						ulFileChksum = crc32(ulFileChksum, &Temp, sRead);
						break;
					default:
						ulFileSize = 0;
						iResult = (int)AFS_UNKNOWN_FILE_CHECK_METHOD;
						break;

				}
				ulFileSize--;
				sRead = AFSRead(pArchive, (int)-1, &Temp, 1);

			}
			if( ulFileChksum != pAfsDir->ilFileCheckSum)
				iResult = (int)AFS_FILE_VALIDATE_FAIL;
		}
	}
	return iResult;
}
/***************************************************************************************
*    Function: AFSExtractBlock()
*
* Description: Extracts a block of data from a file in an Archive.
*              
*   Arguments: pAFS - a pointer to the archive control block;
*              ulAttrb - File that is being extracted attributes.
*			   pBuffer - Pointer that will hold the address of the expanded
*                        data buffer.
			   piRead - holds the amount of data read from the Archive.
*
*      Return: Returns the number of bytes in the the expanded buffer to be written
*			   0 - if Error
*
*      Author: Mark Harris
*
*****************************************************************************************/
static int AFSExtractBlock(AFILE *pAFS, unsigned long ulAttrib, unsigned long ulOffset, 
						   int *piReadLen)
{
	unsigned char *pTempBuffer;
	int iUZResult;
	unsigned long ulBlockSize = 0;
	int iReadLen = 0;
	int iWriteLen = 0;
	unsigned long ulCBS = 0;

	if(pAFS->pBuffer == NULL && pAFS->ulAllocSize == 0)
	{
		if(ulAttrib & ATR_MASK_COMPRS)
		{
			switch(ulAttrib & ATR_MASK_CBS)
			{
				case ATR_CBS_4K:
					ulCBS = CBS_4K;
					break;
				case ATR_CBS_8K:
					ulCBS = CBS_8K;
					break;
				case ATR_CBS_16K:
					ulCBS = CBS_16K;
					break;
				case ATR_CBS_32K:
					ulCBS = CBS_32K;
					break;
				case ATR_CBS_64K:
					ulCBS = CBS_64K;
					break;
				case ATR_CBS_128K:
					ulCBS = CBS_128K;
					break;
				case ATR_CBS_256K:
					ulCBS = CBS_256K;
					break;
				default:
					ulCBS = 0;
					break;
			}
			if(ulCBS)
				pAFS->ulAllocSize = ulCBS;
			else
				pAFS->ulAllocSize = CBS_16K;
		}
		else
		{
			pAFS->ulAllocSize = CBS_16K;
		}
		pAFS->pBuffer = (unsigned char *)calloc(1,  pAFS->ulAllocSize );
		if(!pAFS->pBuffer)
			pAFS->ulAllocSize = 0;
	}
	if( pAFS->pBuffer )
	{
		if(ulAttrib & ATR_MASK_COMPRS)
		{
			if(pAFS->ulBlockCount == 0)
			{
				// Read Max Compressed blocks
				iReadLen = AFSRead(pAFS, (int)ulOffset, (unsigned char *)&pAFS->ulBlockCount, sizeof(long));
				fnSwapBytesForLongs((unsigned char *)&pAFS->ulBlockCount);
				ulOffset = ~0;
			}
			iReadLen += AFSRead(pAFS, (int)ulOffset, (unsigned char *)&ulBlockSize, sizeof(long));
			fnSwapBytesForLongs((unsigned char *)&ulBlockSize);
			pTempBuffer = (unsigned char *)calloc(1, ulBlockSize);
			if(pTempBuffer)
			{
				iReadLen += AFSRead(pAFS, (int)ulOffset, pTempBuffer, ulBlockSize);
				iWriteLen = (int)UnZipBlock(pTempBuffer, ulBlockSize, pAFS->pBuffer, pAFS->ulAllocSize, &iUZResult);
				free(pTempBuffer);
			}
		}
		else
		{
			 iReadLen  = AFSRead(pAFS, (int)ulOffset, pAFS->pBuffer, 1);
			 iWriteLen = iReadLen;
		};
	}
	*piReadLen = iReadLen;
	return ( iWriteLen );
}
