/* TAB SETTING = 4 (e.g. 5, 9, 13, 17...)   */

/*********************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    cjunior.c
   
   DESCRIPTION:    Data file for megabob
                   

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
* ----------------------------------------------------------------------
* HISTORY: 
*				  
* 2018.08.22 CWB - 1H10 - Jira 8243 - Service Code in Barcode is 0.
* - Changed bobsBinaryServiceCode[] from a 2 character array to a 4 character 
*	 array, because it NOW stores the 4-byte value to be copied to the 
*    Predebit command data. For Canada, this is the 2-byte, BE, ServiceCodeNumeric
*    followed by 2 zero bytes.  
*

 23-Apr-18 sa002pe on FPHX 02.12 shelton branch
   For Janus Fraca 236437: Changed the default power up setting for currentIndiSelection to NORMAL_INDICIA_SETTING.
* 
******************************************************************************/


#include "ossetup.h"
#include "pbos.h"
#include "global.h"
#include "bob.h"
#include "clock.h"
#include "junior.h"
#include "bobutils.h"
#include "fwrapper.h"
#include "tealeavs.h"
#include "datdict.h"
//#include "delconf.h"
#include "sig.h"
#include "ipsd.h"
#include "cm.h"
#include "cjunior.h"
#include "custdat.h"

//---------------------------------------------------------------------
//          Externs
//extern  ErrorLog    CMOSErrorLog;
//extern ShadowDebitLog             CMOSShadowDebitLog;
//extern ShadowRefillLog            CMOSShadowRefillLog;
//extern  struct      iHEAD_DESMAC jdataDESMAC;
//extern  uchar       phStatus[2];                                /* status of last print head message            */
extern  CMOS_Signature      CMOSSignature;                      /* for Cherry parms */
extern ushort  usIndiciaPrintFlags;
extern ushort  usPermitPrintFlags;
extern ushort  usDateTimePrintFlags;

//---------------------------------------------------------------------
//   Global variable definitions, for private bob module data.

// This buffer is used to assemble messages for the system log.
//  It can be shared by all of the bob module (in theory) because it's data is 
//  very temporary.
char        pBobsLogScratchPad[ 150 ];

BOOL        dirtyDebit = FALSE;
BOOL        psdClockFlag= FALSE;

uchar       dummyVar;

// Used to indicate which PSD type we have successfully found/talked to.
char    bPsdType = PSDT_NA;

/* ================
    FOR COMET
*/

BOOL        repeatCommandFlag;
BOOL        alreadyRepeated;

const char  numberOfTestPrints = TEST_PRINT_MAX_ID;

//uchar psdCertificateKey[CERTIFICATE_SIZE];

double  germanDisCountPiece = 0;
char    germanPCstate       = DISCOUNT_PC_NO_PRINT;
char    germanPieceMask[MAX_GPC_MASK];

uchar rcString[HR_RCSTRING_SZ] = {0,0,0,0};

struct sPHCprintParms phcprintparms1;       // data for the load print parameters 1 message         

uchar   STAT_EdmCommand;                            // Global for comet psd key exchange key process


// Comet PHC Reply Data Structures
//uchar phcReplyHead[8];            // reply header         
uchar msgPacketInfo[4];         // reply header 

uchar phcAllStats[12];          // phc device status fields in reply messages   


INTERTASK   bobsCommand;                            // container for incoming intertask message 
char        megabobStat[2] = { 0, 0 };

ushort      IGBCState;                              // image generator - barcode generator error class
ushort      IGBCStatus;                             // image generator - barcode generator error code

BOOL    phInPrintMode =         FALSE;
BOOL    phInReportMode =        FALSE;
BOOL    bobsBeenInitialized =   FALSE;              // set TRUE after initialization
BOOL    phcRunTimePowerup =     FALSE;

uchar   bobsProductLine0[HR_PRODLINE_SZ0];
uchar   bobsProductLine1[HR_PRODLINE_SZ1];
uchar   bobsTotalProductLine[HR_PRODLINE_TOT_SZ];
uchar   bobsProductCode[BOBS_PROD_CODE_SZ];
uchar   bobsBCProductCode[BOB_BC_PROD_CODE_SZ];
uchar   bobsBinaryServiceCode[4];
uchar   bobsCebitField[CEBIT_PROD_LINE0 + CEBIT_PROD_LINE1];
uchar   bobsRateClass[BOBS_RATE_CLASS_SZ];
uchar   bobsItemCategory[BOBS_ITEM_CATEGORY_SZ];


uchar   readableServType[HR_SERVICETYPE_SZ];
uchar   bcServicetype[BC_SERVICETYPE_SZ];
uchar   bobsHRTerminal[PRINTED_INDICIA_NUM_SZ];
uchar   bobsBCTerminal[PRINTED_INDICIA_NUM_SZ];
uchar   laPosteLogo = 'A';
uchar   diePostLogo = 'B';

ushort  currentTestPrintSelection = TURN_OFF_IMAGE;
ushort  currentBarCodeSelection =   TURN_OFF_IMAGE;
ushort  currentPreBarSelection =    TURN_OFF_IMAGE;
ushort  currentIndiSelection =      NORMAL_INDICIA_SETTING;
ushort  currentEBSelection =        TURN_OFF_IMAGE;
ushort  currentTCSelection =        TURN_OFF_IMAGE;
ushort  currentInscrSelection =     TURN_OFF_IMAGE;
ushort  currentAdSelection =        TURN_OFF_IMAGE;
ushort  currentLagSelection =       TURN_OFF_IMAGE;
ushort  currentStarSelection =      TURN_OFF_IMAGE;
ushort  currentGDPCSelection =      TURN_OFF_IMAGE;
uchar   currentRptSelection =       TURN_OFF_IMAGE_BYTE;
ushort  currentPermitSelection =    TURN_OFF_IMAGE;
ushort  currentTextEntrySelection = TURN_OFF_IMAGE;
ushort  currentDateTimeSelection =  TURN_OFF_IMAGE;
ushort  currentPermitTypeSelection = TURN_OFF_IMAGE;

unichar currentPermitName[MAX_PERMIT_UNICHARS+1] = {0,0,};

ushort  currentAutoInscrSelection =     TURN_OFF_IMAGE;
ushort  currentUserInscrSelection =     TURN_OFF_IMAGE;

ushort  currentAutoAdSelection =     TURN_OFF_IMAGE;
ushort  currentUserAdSelection =     TURN_OFF_IMAGE;

// This is a copy of currentIndiToPrint, just to make sure that no task other
//  than bob ever writes to currentIndiToPrint.  This variable can be read 
//  using fnValidData.  
ushort  currentIndiToPrintReadOnly = TURN_OFF_IMAGE;


uchar   rateDescriptionForPSD[MAX_RATE_DESCRIPT_LEN];
uchar   terminalIDForPSD[TERMINALIDLEN];
uchar   laDiePostCharForPSD;


uchar   lastImageID =           NOT_AN_IMAGE_ID;
uchar   lastImageType =         NOT_AN_IMAGE_SELECTION;

ushort  currentPrintWidth =     0;                  // width of total current DOF print image    
struct bobDriverIface transferStruct;               // passes information from bob to 7816 driver


uchar   independImgSelect;


BOOL    trustedKeySupplied = FALSE;

// Data Sent to ASIC Download Task  
ushort  asicDLsize;
uchar   *asicDLaddress;                             // address sent to ASIC Download Driver         
uchar   dLoadStatRepeat;                            // number of times we have retried to get download status

// Buffers for Building Messages    
uchar   juniorBuildMsg[BUILD_BUF_SIZ];              // buffer for building messages for devices 
uchar   juniorReplyMsg[REPLY_BUF_SIZ];              // buffer for received device messages  

uchar   kickTheBucket[2];                           // current item code for data capture


// Message Statuses 
uchar   vaultStatus[2];                             // status of last vault message     
uchar   extCardStatus[2];                           // status of last external card message
uchar   asicStatus[2];                              // status of last asic message      

uchar   vltLockStatus[2];                           // vault status returned a mfg card lock attempt

uchar   ErrClassFrombob;
uchar   ErrCodeFrombob;
BOOL    bobErrFlag = FALSE;
uchar   bobState;

uchar   bobSessionState;
char    sessionStopFlag;

uchar   ucStaticImageStatus;                        // static image generation status

/* Device States    */
uchar   vaultState;                                 // state of vault                   
uchar   megabobState;                               // internal state of megabob        
uchar   extCardState;                               // state of external card           
uchar   phState;                                    // state of print head              
uchar   asicState;                                  // state of communication with ASIC 
uchar   psdState;
uchar   bPrintReplyState;
uchar   vaultPowerupResults;                        // used during power up to flag card mfg state
uchar   headPowerupResults;                         // used during power up to flag head mfg state
uchar   xCardEvaluation;


// Date/Time Variables:
//  These variables are extremely temporary.  They are used strictly for getting the
//   current systime and printtime, and converting it to different formats.  All the
//   formats are populated when any message needs the current system time or the 
//   print time (current time +/- data advance days or backdate days.)
//  These should not be accessed outside of megabob.c, even through fnValidData.
//   They are in the Variable Address table, so that the table that indicates which
//   time to use in which message (whenAmI), can do so by ID.  
//
DATETIME    msgPrintTime;
DATETIME    msgSysTime;
// BCD version of stuff
struct current_time             jdataCurrentTime;       
struct current_date             jdataCurrentDate;
struct current_date             jdataPrintedDate;
struct current_month            jdataCurrentMonth;
struct current_month            jdataPrintedMonth;
struct current_date             jdataSessionDate;
struct current_date             updateTest;
// IPSD version of maildate.
ulong       ulJdataMailDate;

/* UIC stuff that bobutils needs */
unsigned char bUICPrintMode;
UNSIGNED butilsEvents, butilSuspend, *ptrButilsEvents;
//OPTION operation;
char butilsEventName[8];


BOOL    fVltDataInitialized = FALSE;    // set to one when vault power up sequence is completed 
BOOL    fphDataInitialized = FALSE;     // set to one when print head power up sequence is completed
BOOL    fExtDataInitialized = FALSE;    // set to one when external card power up sequence is completed 
BOOL    errorAlreadyHandled;


/*
    Running Status of bob's Device Messages

*/
ushort  lastVaultMessage =  DEVICE_MSG_CNT;     // last successful vault message    
ushort  lastphMessage =     DEVICE_MSG_CNT;     // last successful print head message
ushort  lastExtCardMessage= DEVICE_MSG_CNT;     // last successful external card message
ushort  lastAsicMessage =   DEVICE_MSG_CNT;     // last successful ASIC message     
ushort  lastScriptPerformed = INVALID_BOB_MSG;  // last successful script performed by bob
ushort  lastIpsdMessage =   DEVICE_MSG_CNT;     // last successful ipsd message     
ushort  lastIpsdBlMessage = DEVICE_MSG_CNT;     // last successful ipsd Boot Loader message     



// Variables For Print Parameters Message 
// NOTE: initial values are temporary... perhaps these variables should be retrieved from FLASH 

char    maxSessionSiz[2] =          { 0, 50 };                  /* Initial Values are Temporary: maximum of 50 trips per session*/

uchar   bobsFontCnt;                    // number of fonts stored in FLASH
uchar   debitRecordNum;                 // number of retrieved debit records


uchar   fileRoster[ 3 ];                // ZZZ file info table stuff

char    vvVerTokensReply[5];            // intertask messages       

/* Data Used for Total Log Retrieval */
uchar   bobsRefillLogCounter;
uchar   bobsFatalLogCounter;

//BOOL  newTCloaded = FALSE;

/* initialized FOR ALPHA Release    */
short   wBobsMeterStatus = 0x0000;                  // bob's portion of meter status returned to mfg

JPRINT_IMAGE rCMReply;
BOOL fBobDebitOK;

//  PSD Appended Data And Reply Data Variables 

ushort  FatErrLogReks;

uchar   bobsPostageValue[4];

struct  TransferControlStructure    *edmXferStructPtr;
struct  TransferControlStructure    localXferCrtlStruct;
struct  TransferControlStructure    *localXferCrtlStructPtr = &localXferCrtlStruct;

uchar   ipsdFlightRecord[PSD_FLIGHT_REC_SIZ];

uchar   adPSDnonSecData[36];                        // nonce, PHC ID, and Base PCN sent to the PSD when getting the printer init record 
uchar   psdInitRecordForPHC[200];                   // printer init record sent from PSD to PHC... NOTE: size may vary

uchar   rpPSDTeststat[4];
char    *floatingReplyPtr;

//  COMPILED VARIABLE CROSS-REFERENCES

// !!!cb this is just a place to point to until we can get the real data created.
uchar   DummyData[ 8 ];

// This is a place to point to for "Bad Data".  0 index indicates Bad Data.
uchar   bCjuniorBadData[ 8 ];

const struct varXref bobaVarAddresses[] = {
//  LOCATION                             SIZ
{ &bCjuniorBadData,                   sizeof( bCjuniorBadData ),                        },  // 0x0  0     // BAD_VARIABLE_NAME 

// IPSD variables:
//  LOCATION                            SIZE                                                              // ID label
{ &pIPSD_cmdStatusResp,                 sizeof( pIPSD_cmdStatusResp ),                  },  // 0x1  1     // BOBID_IPSD_CMDSTATUSRESP
{ &pIPSD_state,                         sizeof( pIPSD_state ),                          },  // 0x2  2     // BOBID_IPSD_STATE
{ &pIPSD_auditStatus,                   IPSD_SZ_AUDIT_STATUS,                           },  // 0x3  3     // BOBID_IPSD_AUDITSTATUS
{ &pIPSD_DCstatus,                      IPSD_SZ_PB_DATACENTER_STATUS,                   },  // 0x4  4     // BOBID_IPSD_DCSTATUS
{ &rIPSD_packetInfo.rHdr.bPacketCode,   sizeof( rIPSD_packetInfo.rHdr.bPacketCode ),    },  // 0x5  5     // BOBID_IPSD_PACKETCODE
{ &rIPSD_packetInfo.rHdr.bP2,           sizeof( rIPSD_packetInfo.rHdr.bP2 ),            },  // 0x6  6     // BOBID_IPSD_P2
{ &rIPSD_packetInfo.rHdr.bPacketSize,   sizeof( rIPSD_packetInfo.rHdr.bPacketSize ),    },  // 0x7  7     // BOBID_IPSD_PACKETSIZE
{ &pIPSD_packet_data,                   MAX_IPSD_PCKT_SZ,                               },  // 0x8  8     // BOBID_IPSD_PACKETDATA
{ &bIPSD_msgType,                       sizeof( bIPSD_msgType ),                        },  // 0x9  9     // BOBID_IPSD_MSGTYPE
{ &pIPSD_nonce,                         IPSD_SZ_NONCE,                                  },  // 0xA  10    // BOBID_IPSD_NONCE
{ &pIPSD_encryptedNonce,                IPSD_SZ_NONCE,                                  },  // 0xB  11    // BOBID_IPSD_ENCRYPTEDNONCE
{ &pIPSD_userPassword,                  IPSD_SZ_USER_PWD,                               },  // 0xC  12    // BOBID_IPSD_USERPASSWORD
{ &pIPSD_loginData,                     IPSD_SZ_LOGIN_DATA,                             },  // 0xD  13    // BOBID_IPSD_LOGINDATA
{ &pIPSD_originCountry,                 sizeof( pIPSD_originCountry ),                  },  // 0xE  14    // BOBID_IPSD_ORIGINCOUNTRY
{ &bIPSD_currencyCode,                  sizeof( bIPSD_currencyCode ),                   },  // 0xF  15    // BOBID_IPSD_CURRENCYCODE
{ &pIPSD_zipCode,                       IPSD_SZ_ZIPCODE,                                },  // 0x10  16   // BOBID_IPSD_ZIPCODE
{ &pIPSD_PCN,                           IPSD_SZ_PSDPCN,                                 },  // 0x11  17   // BOBID_IPSD_PCN
{ &pIPSD_certificatSN,                  IPSD_SZ_SN_CERTIFICATE,                         },  // 0x12  18   // BOBID_IPSD_CERTIFICATSN
{ &pIPSD_PBISerialNum,                  IPSD_SZ_SN_PBI,                                 },  // 0x13  19   // BOBID_IPSD_PBISERIALNUM
{ &pIPSD_indiciaSN,                     IPSD_SZ_SN_INDICIA,                             },  // 0x14  20   // BOBID_IPSD_INDICIASN
{ &pIPSD_psdSerialNum,                  sizeof( IPSD_SZ_SN_IND_BIN ),                   },  // 0x15  21   // BOBID_IPSD_PSDSERIALNUM
{ &pIPSD_FirmwareVer,                   IPSD_MAXSZ_FWVER_LEN,                           },  // 0x16  22   // BOBID_IPSD_FIRMWAREVERSTR
{ &pIPSD_hostSW_ID,                     IPSD_SZ_HOST_SW_ID,                             },  // 0x17  23   // BOBID_IPSD_HOSTSW_ID
{ &pIPSD_manufSMR,                      IPSD_SZ_MANUF_SMR,                              },  // 0x18  24   // BOBID_IPSD_MANUFSMR
{ &pIPSD_manufacturerID,                IPSD_SZ_MANUF_ID,                               },  // 0x19  25   // BOBID_IPSD_MANUFACTURERID
{ &pIPSD_modelNumber,                   IPSD_SZ_MODEL_NUM,                              },  // 0x1A  26   // BOBID_IPSD_MODELNUMBER 
{ &bIPSD_indiciaType,                   sizeof( bIPSD_indiciaType ),                    },  // 0x1B  27   // BOBID_IPSD_INDICIATYPE
{ &bIPSD_indiciaVerNum,                 sizeof( bIPSD_indiciaVerNum ),                  },  // 0x1C  28   // BOBID_IPSD_INDICIAVERNUM
{ &bIPSD_algorithID,                    sizeof( bIPSD_algorithID ),                     },  // 0x1D  29   // BOBID_IPSD_ALGORITHID
{ &rIPSD_indiciumCreate,                sizeof( rIPSD_indiciumCreate ),                 },  // 0x1E  30   // BOBID_IPSD_INDICIUMCREATE
{ &pIPSD_barcodeData.pGlob,            IPSD_MAXGLOBSZ_INDDATA,                          },  // 0x1F  31   // BOBID_IPSD_INDICIADATAGLOB
{ &m5IPSD_ascReg,                       sizeof( m5IPSD ),                               },  // 0x20  32   // BOBID_IPSD_ASCREG
{ &m4IPSD_descReg,                      sizeof( m4IPSD ),                               },  // 0x21  33   // BOBID_IPSD_DESCREG
{ &pIPSD_pieceCount,                    sizeof( pIPSD_pieceCount ),                     },  // 0x22  34   // BOBID_IPSD_PIECECOUNT
{ &bIPSD_refillType,                    sizeof( bIPSD_refillType ),                     },  // 0x23  35   // BOBID_IPSD_REFILLTYPE
{ &fIPSD_keypadRefillInvokesInspect,    sizeof( fIPSD_keypadRefillInvokesInspect ),     },  // 0x24  36   // BOBID_IPSD_KEYPADREFILLINVOKESINSPECT
{ &pIPSD_keypadRefillComboData,         sizeof( pIPSD_keypadRefillComboData ),          },  // 0x25  37   // BOBID_IPSD_KEYPADREFILLCOMBODATA
{ &pIPSD_keypadWithdrawalComboData,     sizeof( pIPSD_keypadWithdrawalComboData ),      },  // 0x26  38   // BOBID_IPSD_KEYPADWITHDRAWALCOMBODATA
{ &m4IPSD_postageRequested,             sizeof( m4IPSD_postageRequested ),              },  // 0x27  39   // BOBID_IPSD_POSTAGEREQUESTED
{ &rIPSD_PVDMessage,                    sizeof( rIPSD_PVDMessage ),                     },  // 0x28  40   // BOBID_IPSD_PVDMESSAGE
{ &rIPSD_PVRMessage,                    sizeof( rIPSD_PVRMessage ),                     },  // 0x29  41   // BOBID_IPSD_PVRMESSAGE
{ &rIPSD_devAuditResponse,              sizeof( rIPSD_devAuditResponse ),               },  // 0x2A  42   // BOBID_IPSD_RDEVAUDITRESP
{ &fIPSD_clearARwithPVR,                sizeof( fIPSD_clearARwithPVR ),                 },  // 0x2B  43   // BOBID_IPSD_CLEARARWITHPVR
{ &pIPSD_refillCount,                   IPSD_SZ_REFILL_CNT,                             },  // 0x2C  44   // BOBID_IPSD_REFILLCOUNT
{ &pIPSD_GMTOffset,                     IPSD_SZ_TIME,                                   },  // 0x2D  45   // BOBID_IPSD_GMTOFFSET
{ &pIPSD_clockOffset,                   IPSD_SZ_TIME,                                   },  // 0x2E  46   // BOBID_IPSD_CLOCKOFFSET
{ &pIPSD_RealTimeClock,                 IPSD_SZ_TIME,                                   },  // 0x2F  47   // BOBID_IPSD_RTC
{ &pIPSD_dateAdvanceLimit,              IPSD_SZ_TIME,                                   },  // 0x30  48   // BOBID_IPSD_DATEADVANCELIMIT
{ &pIPSD_backdateLimit,                 IPSD_SZ_TIME,                                   },  // 0x31  49   // BOBID_IPSD_BACKDATELIMIT
{ &m5IPSD_ascRegPreset,                 sizeof( m5IPSD ),                               },  // 0x32  50   // BOBID_IPSD_ASCREGPRESET
{ &m5IPSD_maxAscReg,                    sizeof( m5IPSD ),                               },  // 0x33  51   // BOBID_IPSD_MAXASCREG
{ &m4IPSD_maxDescReg,                   sizeof( m4IPSD ),                               },  // 0x34  52   // BOBID_IPSD_MAXDESCREG
{ &bIPSD_frenchMAC_Sz,                  sizeof( bIPSD_frenchMAC_Sz ),                   },  // 0x35  53   // BOBID_IPSD_FRENCHMAC_SZ
{ &bIPSD_frenchOCR_Sz,                  sizeof( bIPSD_frenchOCR_Sz ),                   },  // 0x36  54   // BOBID_IPSD_FRENCHOCR_SZ
{ &pIPSD_IndCreateLog,                  IPSD_LOGSZ_INDCREATE,                           },  // 0x37  55   // BOBID_IPSD_INDCREATELOG
{ &pIPSD_RefillLog,                     IPSD_LOGSZ_REFILL,                              },  // 0x38  56   // BOBID_IPSD_REFILLLOG
{ &pIPSD_AuditLog,                      IPSD_LOGSZ_AUDIT,                               },  // 0x39  57   // BOBID_IPSD_AUDITLOG
{ &pIPSD_Speed,                         IPSD_SZ_SPEED,                                  },  // 0x3A  58   // BOBID_IPSD_SPEED
{ &pIPSD_FreeRam,                       IPSD_SZ_FREERAM,                                },  // 0x3B  59   // BOBID_IPSD_FREERAM
{ &pIPSD_PORCount,                      IPSD_SZ_POR_CNT,                                },  // 0x3C  60   // BOBID_IPSD_PORCOUNT
{ &pIPSD_Random,                        IPSD_MAXSZ_RANDOM,                              },  // 0x3D  61   // BOBID_IPSD_RANDOM
{ &pIPSD_RandomSz,                      sizeof( pIPSD_RandomSz ),                       },  // 0x3E  62   // BOBID_IPSD_RANDOMSZ
{ &pIPSD_watchDogTmr,                   IPSD_SZ_TIME,                                   },  // 0x3F  63   // BOBID_IPSD_WATCHDOGTMR
{ &pIPSD_watchDogTmrReset,              IPSD_SZ_TIME,                                   },  // 0x40  64   // BOBID_IPSD_WATCHDOGTMRRESET
{ &pIPSD_watchDogTmrExpire,             IPSD_SZ_TIME,                                   },  // 0x41  65   // BOBID_IPSD_WATCHDOGTMREXPIRE
{ &m4IPSD_postageMax,                   sizeof( m4IPSD ),                               },  // 0x42  66   // BOBID_IPSD_POSTAGEMAX
{ &m4IPSD_postageMin,                   sizeof( m4IPSD ),                               },  // 0x43  67   // BOBID_IPSD_POSTAGEMIN
{ &bIPSD_familyCode,                    sizeof( bIPSD_familyCode ),                     },  // 0x44  68   // BOBID_IPSD_FAMILYCODE
{ &pIPSD_MfgSNlsbf,                     sizeof( pIPSD_MfgSNlsbf ),                      },  // 0x45  69   // BOBID_IPSD_MFGSNLSBF
{ &puIPSD_MfgSNAscii,                   sizeof( puIPSD_MfgSNAscii ),                    },  // 0x46  70   // BOBID_IPSD_MFGSNASCII
{ &pIPSD_paramListGlob,                 sizeof( pIPSD_paramListGlob ),                  },  // 0x47  71   // BOBID_IPSD_PARAMLISTGLOB
{ &rIPSD_indiciumCreate.pMailingDate,   sizeof( rIPSD_indiciumCreate.pMailingDate ),    },  // 0x48  72   // BOBID_IPSD_MAILINGDATE
{ &bobsPostageValue,                    sizeof( bobsPostageValue ),                     },  // 0x49  73   // VAULT_DEBIT_VALUE
                                                                                            
// FLASH 'variables' that are used to be in Myko PSD.                                       
// Flash ID                             size of Data                                        
{ (void *)IBP_SETTABLE_DIGITS,          sizeof( uchar ),                                },  // 0x4A  74   // VLT_PURSE_SETTABLE_DIGITS
// End of IPSD grouped data.                                                                
                                                                                            
// Stuff that used to be in Myko:                                                           
// Address of data                      size of data                                        
{ &ulDerivedPieceCount,                 sizeof(ulDerivedPieceCount),                    },  // 0x4B  75   // GET_VLT_PIECE_COUNTER, (usually the same as bobID_IPSD_pieceCount, sometimes the non zero piece count)
{ &m4IPSD_postageRequested,             sizeof( m4IPSD_postageRequested ),              },  // 0x4C  76   // VAULT_REFILL_AMT,  (bobID_IPSD_postageRequested)
{ &mStd_ControlSum,                     SPARK_MONEY_SIZE,                               },  // 0x4D  77   // GET_VLT_CONTROL_SUM 
{ &mStd_MaxDescReg,                     sizeof( mStd_MaxDescReg ),                      },  // 0x4E  78   // VLT_PURSE_MAX_DESC_REG
{ (void*) IBP_DECIMAL_PLACES,           sizeof( uchar ),                                },  // 0x4F  79   // VLT_PURSE_DECIMALS
{ &mStd_DescendingReg,                  SPARK_MONEY_SIZE,                               },  // 0x50  80   // GET_VLT_DESCENDING_REG (converted version of m4IPSD_descReg)
{ &m5IPSD_ascReg,                       sizeof( m5IPSD ),                               },  // 0x51  81   // GET_VLT_ASCENDING_REG (bobID_IPSD_ascReg)
                                                                                            
//  Bob internal stuff...                                                                   
{ &megabobStat,                         sizeof( megabobStat ),                          },  // 0x52  82   // MEGABOBSTAT                
{ &megabobState,                        sizeof( megabobState ),                         },  // 0x53  83   // MEGABOBSTATE               
{ &vaultState,                          sizeof( vaultState ),                           },  // 0x54  84   // VAULTSTATE,
{ &floatingReplyPtr,                    sizeof( floatingReplyPtr ),                     },  // 0x55  85   // FLOATINGREPLYPTR
{ &bUICPrintMode,                       sizeof( bUICPrintMode ),                        },  // 0x56  86   // UIC_PRINT_MODE
{ &currentIndiSelection,                sizeof( currentIndiSelection ),                 },  // 0x57  87   // READ_CURRENT_INDI_SELECTION
{ &currentIndiSelection,                sizeof( currentIndiSelection ),                 },  // 0x58  88   // SET_CURRENT_INDI_SELECTION 
{ &updateTest,                          sizeof(updateTest),                             },  // 0x59  89   // UPDATETEST,
{ &msgSysTime.bCentury,                 sizeof(msgSysTime.bCentury),                    },  // 0x5A  90   // MSGSYSTIME_BCENTURY,
{ &msgSysTime.bYear,                    sizeof(msgSysTime.bYear),                       },  // 0x5B  91   // MSGSYSTIME_BYEAR,
{ &msgSysTime.bMonth,                   sizeof(msgSysTime.bMonth),                      },  // 0x5C  92   // MSGSYSTIME_BMONTH,
{ &msgSysTime.bDay,                     sizeof(msgSysTime.bDay),                        },  // 0x5D  93   // MSGSYSTIME_BDAY,
{ &msgSysTime.bHour,                    sizeof(msgSysTime.bHour),                       },  // 0x5E  94   // MSGSYSTIME_BHOUR,
{ &msgSysTime.bMinutes,                 sizeof(msgSysTime.bMinutes),                    },  // 0x5F  95   // MSGSYSTIME_BMINUTES,
{ &jdataCurrentTime,                    sizeof(jdataCurrentTime),                       },  // 0x60  96   // JDATACURRENTTIME,
{ &jdataCurrentDate,                    sizeof(jdataCurrentDate),                       },  // 0x61  97   // JDATACURRENTDATE,
{ &jdataCurrentMonth,                   sizeof(jdataCurrentMonth),                      },  // 0x62  98   // JDATACURRENTMONTH,
{ &jdataCurrentTime.centuryYY,          sizeof(jdataCurrentTime.centuryYY),             },  // 0x63  99   // JDATACURRENTTIME_CENTURYYY,
{ &jdataCurrentTime.yearYY,             sizeof(jdataCurrentTime.yearYY),                },  // 0x64  100  // JDATACURRENTTIME_YEARYY,
{ &jdataCurrentTime.monthMM,            sizeof(jdataCurrentTime.monthMM),               },  // 0x65  101  // JDATACURRENTTIME_MONTHMM,
{ &jdataCurrentTime.dayDD,              sizeof(jdataCurrentTime.dayDD),                 },  // 0x66  102  // JDATACURRENTTIME_DAYDD,
{ &jdataCurrentTime.hourHH,             sizeof(jdataCurrentTime.hourHH),                },  // 0x67  103  // JDATACURRENTTIME_HOURHH,
{ &jdataCurrentTime.minuteMM,           sizeof(jdataCurrentTime.minuteMM),              },  // 0x68  104  // JDATACURRENTTIME_MINUTEMM,
{ &jdataCurrentDate.centuryYY,          sizeof(jdataCurrentDate.centuryYY),             },  // 0x69  105  // JDATACURRENTDATE_CENTURYYY,
{ &jdataCurrentDate.yearYY,             sizeof(jdataCurrentDate.yearYY),                },  // 0x6A  106  // JDATACURRENTDATE_YEARYY,
{ &jdataCurrentDate.monthMM,            sizeof(jdataCurrentDate.monthMM),               },  // 0x6B  107  // JDATACURRENTDATE_MONTHMM,
{ &jdataCurrentDate.dayDD,              sizeof(jdataCurrentDate.dayDD),                 },  // 0x6C  108  // JDATACURRENTDATE_DAYDD,
{ &jdataCurrentMonth.yearYY,            sizeof(jdataCurrentMonth.yearYY),               },  // 0x6D  109  // JDATACURRENTMONTH_YEARYY,
{ &jdataCurrentMonth.monthMM,           sizeof(jdataCurrentMonth.monthMM),              },  // 0x6E  110  // JDATACURRENTMONTH_MONTHMM,
{ &msgPrintTime.bCentury,               sizeof(msgPrintTime.bCentury),                  },  // 0x6F  111  // MSGPRINTTIME_BCENTURY,
{ &msgPrintTime.bYear,                  sizeof(msgPrintTime.bYear),                     },  // 0x70  112  // MSGPRINTTIME_BYEAR,
{ &msgPrintTime.bMonth,                 sizeof(msgPrintTime.bMonth),                    },  // 0x71  113  // MSGPRINTTIME_BMONTH,
{ &msgPrintTime.bDay,                   sizeof(msgPrintTime.bDay),                      },  // 0x72  114  // MSGPRINTTIME_BDAY,
{ &jdataPrintedMonth,                   sizeof(jdataPrintedMonth),                      },  // 0x73  115  // JDATAPRINTEDMONTH,
{ &jdataPrintedDate,                    sizeof(jdataPrintedDate),                       },  // 0x74  116  // JDATAPRINTEDDATE,
{ &jdataPrintedDate.centuryYY,          sizeof(jdataPrintedDate.centuryYY),             },  // 0x75  117  // JDATAPRINTEDDATE_CENTURYYY,
{ &jdataPrintedDate.yearYY,             sizeof(jdataPrintedDate.yearYY),                },  // 0x76  118  // JDATAPRINTEDDATE_YEARYY,
{ &jdataPrintedDate.monthMM,            sizeof(jdataPrintedDate.monthMM),               },  // 0x77  119  // JDATAPRINTEDDATE_MONTHMM,
{ &jdataPrintedDate.dayDD,              sizeof(jdataPrintedDate.dayDD),                 },  // 0x78  120  // JDATAPRINTEDDATE_DAYDD,
{ &jdataPrintedMonth.yearYY,            sizeof(jdataPrintedMonth.yearYY),               },  // 0x79  121  // JDATAPRINTEDMONTH_YEARYY,
{ &jdataPrintedMonth.monthMM,           sizeof(jdataPrintedMonth.monthMM),              },  // 0x7A  122  // JDATAPRINTEDMONTH_MONTHMM,
{ &ulJdataMailDate,                     IPSD_SZ_TIME,                                   },  // 0x7B  123  // JDATAMAILDATE
{ &pIPSD_FirmwareVerSubStr,             IPSD_MAXSZ_FWVER_SUB_LEN,                       },  // 0x7C  124  // bobID_IPSD_FirmwareVerSubStr
{ &currentAdSelection,                  sizeof(currentAdSelection),                     },  // 0x7D  125  // READ_CURRENT_AD_SELECTION
{ &currentUserAdSelection,                  sizeof(currentUserAdSelection),                     },  // 0x7E  126  // SET_CURRENT_AD_SELECTION
{ &currentInscrSelection,               sizeof(currentInscrSelection),                  },  // 0x7F  127  // READ_CURRENT_INSCR_SELECT
{ &currentUserInscrSelection,           sizeof(currentUserInscrSelection),              },  // 0x80  128  // SET_CURRENT_INSCR_SELECT
{ &wBobsMeterStatus,                    sizeof(wBobsMeterStatus),                       },  // 0x81  129  // MFG_METER_STATUS
{ &localXferCrtlStructPtr,              sizeof(localXferCrtlStructPtr),                 },  // 0x82  130  // RPLOCALXFERCTRLSTRUCT
{ &lIPSD_FirmwareVerNum,                sizeof( lIPSD_FirmwareVerNum ),                 },  // 0x83  131  // LIPSD_FIRMWAREVERNUM
{ &pIPSD_nonce,                         IPSD_SZ_NONCE,                                  },  // 0x84  132  // GET_PSD_CHALLENGE
{ &pIPSD_encryptedNonce,                IPSD_SZ_NONCE,                                  },  // 0x85  133  // SET_PSD_XPRT_AUTH
{ &pIPSD_state,                         sizeof( pIPSD_state ),                          },  // 0x86  134  // GET_PSD_STATUS
{ &edmXferStructPtr,                    sizeof(edmXferStructPtr),                       },  // 0x87  135  // MFG_SCRAP_PSD_INIT_3
{ &IPSDTimerecord,                      sizeof(IPSDTimerecord),                         },  // 0x88  136  // GET_PSD_REAL_TIME_CLOCK
{ &IPSDSetimeclock,                     sizeof(IPSDSetimeclock),                        },  // 0x89  137  // SET_IPSD_REAL_TIME_CLOCK
{ &pEdmIPSDClockOffset,                 sizeof(pEdmIPSDClockOffset),                    },  // 0x8A  138  // GET_IPSD_CLOCK_OFFSET
{ &bPsdType,                            sizeof(bPsdType),                               },  // 0x8B  139  // PSD_TYPE
{ &pIPSD_providerPublicKeyData,         sizeof(pIPSD_providerPublicKeyData),            },  // 0x8C  140  // BOBID_IPSD_PROVIDERKEYDATA
{ &pIPSD_InitDataGlob,                  sizeof(pIPSD_InitDataGlob),                     },  // 0x8D  141  // BOBID_IPSD_INITDATAGLOB
{ &wIPSD_providerPublicKeyDataSize,     sizeof(wIPSD_providerPublicKeyDataSize),        },  // 0x8E  142  // BOBID_IPSD_PROVIDERKEYSIZE
{ &wIPSD_InitDataSize,                  sizeof(wIPSD_InitDataSize),                     },  // 0x8F  143  // BOBID_IPSD_INITDATASIZE
{ &pIPSD_4ByteCmdStatusResp,            sizeof(pIPSD_4ByteCmdStatusResp),               },  // 0x90  144  // PSD_XPORT_MSG_STATUS
{ &pIPSD_4ByteCmdStatusResp,            sizeof(pIPSD_4ByteCmdStatusResp),               },  // 0x91  145  // BOBID_MFG_IPSD_COMMIT_PARAMETER
{ &edmXferStructPtr,                    sizeof(edmXferStructPtr),                       },  // 0x92  146  // GET_GEN_PSD_KEY_REQ
{ &bIPSDEncrKeyType,                    sizeof(bIPSDEncrKeyType),                       },  // 0x93  147  // BOBID_SET_IPSD_LD_ENCR_KEY_TYPE
{ &edmXferStructPtr,                    sizeof(edmXferStructPtr),                       },  // 0x94  148  // BOBID_MFG_IPSD_LD_ENCR_KEY
{ &rcString,                            sizeof(rcString),                               },  // 0x95  149  // RATE_STRING                
{ &pIPSD_createIndDataGlob,             IPSD_MAXGLOBSZ_CREATEIND,                       },  // 0x96  150  // BOBID_IPSD_CREATEINDDATAGLOB
{ &IGBCState,                           sizeof(IGBCState),                              },  // 0x97  151  // IGBC_STATE
{ &IGBCStatus,                          sizeof(IGBCStatus),                             },  // 0x98  152  // IGBC_STATUS
{ (void*) IBP_FIXED_ZEROS,              sizeof( uchar ),                                },  // 0x99  153  // VLT_PURSE_FIXED_ZEROS
{ (void*) IBP_REFILL_INCREMENT,         sizeof( uchar ),                                },  // 0x9A  154  // VLT_PURSE_REFILL_INCREMNT
{ (void*) IBP_FILE_DATEDUCK_FLAG,       sizeof( uchar ),                                },  // 0x9B  155  // VLT_DEBOPTS_FILE_DATEDUCK_FLAG
{ (void*) IBP_FILE_TOWNDUCK_FLAG,       sizeof( uchar ),                                },  // 0x9C  156  // VLT_DEBOPTS_FILE_TOWNDUCK_FLAG
{ (void*) IBP_SECDATA_INSP_LOCK_ENABLED,sizeof( uchar ),                                },  // 0x9D  157  // VLT_SECDATA_INSP_LOCK_ENABLED
{ (void*) ILP_MAX_REFILL_VAL,           sizeof( ulong ),                                },  // 0x9E  158  // VLT_PURSE_MAX_REFILL_VAL
{ (void*) ILP_MIN_REFILL_VAL,           sizeof( ulong ),                                },  // 0x9F  159  // VLT_PURSE_MIN_REFILL_VAL
{ &m4IPSD_postageMax,                   sizeof( m4IPSD ),                               },  // 0xA0  160  // VLT_DEBOPTS_FILE_MAX_DEB_VAL
{ &m4IPSD_postageMin,                   sizeof( m4IPSD ),                               },  // 0xA1  161  // VLT_DEBOPTS_FILE_MIN_DEB_VAL
{ (void*) ILP_REFILL_FAIL_LIMIT,        sizeof( ulong ),                                },  // 0xA2  162  // VLT_PURSE_REFILL_FAIL_LIMIT
{ (void*) ILP_USERSET_LOW_POST_WARN,    sizeof( ulong ),                                },  // 0xA3  163  // VLT_USERSET_LOW_POST_WARN
{ (void*) IWP_SECDATA_INSPECT_WARN_PER, sizeof( ushort ),                               },  // 0xA4  164  // VLT_SECDATA_INSPECT_WARN_PER
{ &pIPSD_DeviceID,                      sizeof(pIPSD_DeviceID),                         },  // 0xA5  165  // BOBID_MFG_GET_IPSD_DEVICE_ID
{ &pIPSD_ChallengeAndStat,              sizeof(pIPSD_ChallengeAndStat),                 },  // 0xA6  166  // BOBID_MFG_GET_PSD_CHALLENGE_AND_STAT
{ &usIndiciaPrintFlags,                 sizeof(usIndiciaPrintFlags),                    },  // 0xA7  167  // GET_INDICIA_PRINT_FLAGS 
{ &usIndiciaPrintFlags,                 sizeof(usIndiciaPrintFlags),                    },  // 0xA8  168  // SET_INDICIA_PRINT_FLAGS 
{ &bIPSD_MfgSNcrc,                      sizeof(bIPSD_MfgSNcrc),                         },  // 0xA9  169  // BOBID_IPSD_MFGSNCRC 
{ &pIPSD_InspectionDueDate,             sizeof(pIPSD_InspectionDueDate),                },  // 0xAA  170  // VLT_CVA_NEXT_INSPECTION_DATE
{ &bIPSD_DebitGracePeriod,              sizeof(bIPSD_DebitGracePeriod),                 },  // 0xAB  171  // VLT_PURSE_DEBIT_GRACE
{ &DummyData,                           sizeof(uchar),                                  },  // 0xAC  172  // BOBID_IPSD_INIT_CLOCK
{ &pIPSD_FakeOriginZipcode,             sizeof(pIPSD_FakeOriginZipcode),                },  // 0xAD  173  // BOBID_IPSD_FAKE_ORIGIN_ZIPCODE
{ &pIPSD_OriginZipCode,                 sizeof(pIPSD_OriginZipCode),                    },  // 0xAE  174  // VLT_IDENT_FILE_ZIPCODE
// OPC_MAX is the size of the ratetask zipcode container, it should be no bigger than either pIPSD_FakeOriginZipcode or pIPSD_zipCode
{ &bIPSD_dateAdvanceLimitDaze,          sizeof(bIPSD_dateAdvanceLimitDaze),             },  // 0xAF  175  // VLT_INDI_DATEADV_LIMIT
{ &bIPSD_backdateLimitDaze,             sizeof(bIPSD_backdateLimitDaze),                },  // 0xB0  176  // VLT_INDI_BACKDATE_LIMIT
{ &bIPSD_ItalyCurrencyCode,             sizeof(bIPSD_ItalyCurrencyCode),                },  // 0xB1  177  // BOBID_IPSD_ITALYCURCODE 
{ &bIPSD_IndiciumCode,                  sizeof(bIPSD_IndiciumCode),                     },  // 0xB2  178  // BOBID_IPSD_INDICIUMCODE 
{ &bIPSD_AlgorithmObjectID,             sizeof(bIPSD_AlgorithmObjectID),                },  // 0xB3  179  // BOBID_IPSD_ALGRTHMOBJID 
{ &pIPSD_PieceCountAtRefill,            sizeof(pIPSD_PieceCountAtRefill),               },  // 0xB4  180  // BOBID_IPSD_PCCNTATREFILL
{ &pIPSD_MeterKeyExpiryDate,            sizeof(pIPSD_MeterKeyExpiryDate),               },  // 0xB5  181  // BOBID_IPSD_MTRKEYEXPDATE
{ &fIPSD_EnableDisableStatus,           sizeof(fIPSD_EnableDisableStatus),              },  // 0xB6  182  // BOBID_IPSD_ENBLDSBLSTAT 
{ &ulVerifyHashSignatureDataPtr,        sizeof(ulVerifyHashSignatureDataPtr),           },  // 0xB7  183  // BOBID_IPSD_VERIFY_HASH_SIGNATURE_DATA_PTR
{ &edmXferStructPtr,                    sizeof(edmXferStructPtr),                       },  // 0xB8  184  // IPSD_LD_PROV_KEY
{ &m5IPSD_ascReg,                       sizeof( m5IPSD ),                               },  // 0xB9  185  // GET_LOCAL_AR
{ &ulDerivedPieceCount,                 sizeof(ulDerivedPieceCount),                    },  // 0xBA  186  // GET_LOCAL_PIECE_COUNT
{ &mStd_DescendingReg,                  sizeof( mStd_DescendingReg ),                   },  // 0xBB  187  // GET_LOCAL_DR
{ &CMOSSignature.bUicPcn[0],            SIZEOFUICPCN,                                   },  // 0xBC  188  // MFG_UIC_PCN            
{ &CMOSSignature.bUicSN[0],             SIZEOFUICSN,                                    },  // 0xBD  189  // MFG_UIC_SERIAL_NUM 
{ &CMOSSignature.bUicSMR[0],            SIZEOFSMR,                                      },  // 0xBE  190  // MFG_UIC_SMR        
{ &CMOSSignature.bUicHWRev[0],          SIZEOFUICHWREV,                                 },  // 0xBF  191  // MFG_UIC_HW_REV     
{ &currentPermitSelection,              sizeof(currentPermitSelection),                 },  // 0xC0  192  // READ_CURRENT_PERMIT_SELECTION
{ &currentPermitSelection,              sizeof(currentPermitSelection),                 },  // 0xC1  193  // SET_CURRENT_PERMIT_SELECTION
{ &currentPermitName,                   sizeof(currentPermitName),                      },  // 0xC2  194  // READ_CURRENT_PERMIT_NAME
{ &currentPermitName,                   sizeof(currentPermitName),                      },  // 0xC3  195  // SET_CURRENT_PERMIT_NAME
{ &currentTextEntrySelection,           sizeof(currentTextEntrySelection),              },  // 0xC4  196  // READ_CURRENT_TEXTENTRY_SELECTION
{ &currentTextEntrySelection,           sizeof(currentTextEntrySelection),              },  // 0xC5  197  // SET_CURRENT_TEXTENTRY_SELECTION
{ &usPermitPrintFlags,                  sizeof(usPermitPrintFlags),                     },  // 0xC6  198  // GET_PERMIT_PRINT_FLAGS 
{ &usPermitPrintFlags,                  sizeof(usPermitPrintFlags),                     },  // 0xC7  199  // SET_PERMIT_PRINT_FLAGS 
{ &usDateTimePrintFlags,                sizeof(usDateTimePrintFlags),                   },  // 0xC8  200  // GET_DATETIME_PRINT_FLAGS 
{ &usDateTimePrintFlags,                sizeof(usDateTimePrintFlags),                   },  // 0xC9  201  // SET_DATETIME_PRINT_FLAGS 
{ &ucStaticImageStatus,                 sizeof(ucStaticImageStatus),                    },  // 0xCA  202  // GET_STATIC_IMAGE_STATUS
{ &bIPSD_ProdPriceVersion,              sizeof(bIPSD_ProdPriceVersion),                 },  // 0xCB  203  // BOBID_IPSD_PRODPRICE_VERSION
{ &localXferCrtlStructPtr,              sizeof(localXferCrtlStructPtr),                 },  // 0xCC  204  // PSD_PBP_FINAL_FRANK_REC
{ &pIPSD_GermanServiceID,               sizeof(pIPSD_GermanServiceID),                  },  // 0xCD  205  // BOBID_IPSD_GERMAN_SERVICEID
{ &pIPSD_GermanProductKey,              sizeof(pIPSD_GermanProductKey),                 },  // 0xCE  206  // BOBID_IPSD_GERMAN_PRODUCTKEY
{ &pIPSD_GermanSecondDate,              sizeof(pIPSD_GermanSecondDate),                 },  // 0xCF  207  // BOBID_IPSD_GERMAN_SECONDDATE
{ &pIPSD_GermanPieceCount,              sizeof(pIPSD_GermanPieceCount),                 },  // 0xD0  208  // BOBID_IPSD_GERMAN_PIECECOUNT
{ &pIPSD_GermanVariablePostalData,      sizeof(pIPSD_GermanVariablePostalData),         },  // 0xD1  209  // BOBID_IPSD_GERMAN_VARPOSTALDATA
{ &pIPSD_GermanARatLastRefill,          sizeof(pIPSD_GermanARatLastRefill),             },  // 0xD2  210  // BOBID_IPSD_GERMAN_ARATLASTREF
{ &pIPSD_GermanCryptoString,            sizeof(pIPSD_GermanCryptoString),               },  // 0xD3  211  // BOBID_IPSD_GERMAN_CRYPTOSTR
{ &pIPSD_GermanPostalIDdata,            sizeof(pIPSD_GermanPostalIDdata),               },  // 0xD4  212  // BOBID_IPSD_GERMAN_POSTIDDATA
{ &pIPSD_GermanAuthKeyRev,              sizeof(pIPSD_GermanAuthKeyRev),                 },  // 0xD5  213  // BOBID_IPSD_GERMAN_AUTHKEYREV
{ &bIPSD_GermanAddDRtoARonPVRFlag,      sizeof(bIPSD_GermanAddDRtoARonPVRFlag),         },  // 0xD6  214  // BOBID_IPSD_ADDDRTOARONPVR
{ &usIPSD_GermanSuppleLine1,            sizeof(usIPSD_GermanSuppleLine1),               },  // 0xD7  215  // BOBID_IPSD_GERMAN_SUPPLE_LINE1
{ &usIPSD_GermanSuppleLine2,            sizeof(usIPSD_GermanSuppleLine2),               },  // 0xD8  216  // BOBID_IPSD_GERMAN_SUPPLE_LINE2
{ &localXferCrtlStructPtr,              sizeof(localXferCrtlStructPtr),                 },  // 0xD9  217  // PSD_LD_ENCR_KEY_REQ
{ &bIPSD_refillType,                    sizeof( bIPSD_refillType ),                     },  // 0xDA  218  // PSD_REFILL_TYPE
{ &ulIPSD_ProviderKeyRev,               IPSD_SZ_KEY_REV,                                },  // 0xDB  219  // BOBID_IPSD_CANADA_PROVKEYREV
{ &pIPSD_DCstatus,                      IPSD_SZ_PB_DATACENTER_STATUS,                   },  // 0xDC  220  // BOBS_LAST_DCSTATUS
{ &currentTestPrintSelection,           sizeof(short),                                  },  // 0xDD  221  // SET_CURRENT_TEST_PRINT
{ &bIPSD_FrenchAlphaNumPieceCount,      sizeof(bIPSD_FrenchAlphaNumPieceCount),         },  // 0xDE  222  // BOBID_IPSD_FRENCH_ALPHANUMPIECECOUNT
{ &bIPSD_ResetPcOnPvr,                  sizeof(bIPSD_ResetPcOnPvr),                     },  // 0xDF  223  // BOBID_IPSD_RESETPCONPVR
{ &bIPSD_AllowZeroPostage,              sizeof(bIPSD_AllowZeroPostage),                 },  // 0xE0  224  // BOBID_IPSD_ALLOWZEROPPOSTAGE
{ &pIPSD_ZeroPostagePieceCount,         sizeof(pIPSD_ZeroPostagePieceCount),            },  // 0xE1  225  // BOBID_IPSD_ZEROPOSTAGEPIECECOUNT

{ &currentPermitTypeSelection,          sizeof(currentPermitTypeSelection),             },  // 0xE2  226  // READ_CURRENT_PERMIT_TYPE_SELECTION
{ &currentPermitTypeSelection,          sizeof(currentPermitTypeSelection),             },  // 0xE3  227  // SET_CURRENT_PERMIT_TYPE_SELECTION
{ &bIPSD_BelgianApplicationValue,       sizeof(bIPSD_BelgianApplicationValue),          },  // 0xE4  228  // BOBID_IPSD_BELGIANAPPVALUE
{ &pIPSD_pieceCount,                    sizeof(pIPSD_pieceCount),                        },  // 0xE5  229  // GET_LOCAL_TOTAL_PIECE_COUNT
{ &pIPSD_ZeroPostagePieceCount,         sizeof(pIPSD_ZeroPostagePieceCount),            },  // 0xE6  230  // GET_LOCAL_ZERO_PIECE_COUNT
{ &bIPSD_IbiLiteIndiciaVersion,         sizeof(bIPSD_IbiLiteIndiciaVersion),            },  // 0xE7  231  // BOBID_IPSD_IBILITEINDICIAVERSION
{ &bIPSD_IbiLiteVendorModel,            sizeof(bIPSD_IbiLiteVendorModel),               },  // 0xE8  232  // BOBID_IPSD_IBILITEVENDORMODEL
{ &bIPSD_CreateIndiciaMessageType,      sizeof(bIPSD_CreateIndiciaMessageType),         },  // 0xE9  233  // BOBID_IPSD_CREATEINDICIA_MSGTYPE
{ &pIPSD_NetSetCLR,                     sizeof(pIPSD_NetSetCLR),                        },  // 0xEA  234  // BOBID_IPSD_NETSET_CLR
{ &pIPSD_NetSetIssuerCode,              sizeof(pIPSD_NetSetIssuerCode),                 },  // 0xEB  235  // BOBID_IPSD_NETSET_ISSUERCODE
{ &pIPSD_AdvancedDateOffset,            sizeof(pIPSD_AdvancedDateOffset),               },  // 0xEC  236  // BOBID_IPSD_ADVANCEDDATEOFFSETASCII
{ &pIPSD_NetSetProductCode,             sizeof(pIPSD_NetSetProductCode),                },  // 0xED  237  // BOBID_IPSD_NETSET_PRODUCTCODE
{ &pIPSD_NetSetSecurityCodeVer,         sizeof(pIPSD_NetSetSecurityCodeVer),            },  // 0xEE  238  // BOBID_IPSD_NETSET_SECURITYCODEVER
{ &pIPSD_flexDebitOutput.pGlob,         sizeof(pIPSD_flexDebitOutput.pGlob),            },  // 0xEF  239  // BOBID_IPSD_FLEXDEBIT_OUTPUT_GLOB
{ &pIPSD_flexDebitOutput.pDSASIG,       sizeof(pIPSD_flexDebitOutput.pDSASIG),          },  // 0xF0  240  // BOBID_IPSD_FLEXDEBIT_DSASIG
{ &pIPSD_flexDebitOutput.pHMAC,         sizeof(pIPSD_flexDebitOutput.pHMAC),            },  // 0xF1  241  // BOBID_IPSD_FLEXDEBIT_HMAC
{ &pIPSD_flexDebitOutput.pDESMAC[6],    2,                                              },  // 0xF2  242  // BOBID_IPSD_FLEXDEBIT_2LSB_3DESMAC
{ &pIPSD_flexDebitOutput.pDESMAC,       sizeof(pIPSD_flexDebitOutput.pDESMAC),          },  // 0xF3  243  // BOBID_IPSD_FLEXDEBIT_DESMAC
{ &pIPSD_flexDebitDataGlob,             sizeof(pIPSD_flexDebitDataGlob),                },  // 0xF4  244  // BOBID_IPSD_FLEXDEBIT_DATAGLOB
{ &wIPSD_flexDebitDataSize,             sizeof(wIPSD_flexDebitDataSize),                },  // 0xF5  245  // BOBID_IPSD_FLEXDEBIT_DATA_SZ
{ &pIPSD_HmacDesMacKeyID,               sizeof(pIPSD_HmacDesMacKeyID),                  },  // 0xF6  246  // BOBID_IPSD_HMACDESMAC_KEYID
{ &pIPSD_PsdAuthorizedKeyRev,           sizeof(pIPSD_PsdAuthorizedKeyRev),              },  // 0xF7  247  // BOBID_IPSD_PSDAUTH_KEYREV
{ &pIPSD_FlexDebitValueKeyData,         sizeof(pIPSD_FlexDebitValueKeyData),            },  // 0xF8  248  // BOBID_IPSD_FLEXDEBIT_VALKEY_DATA
{ &pIPSD_RTCLocalDate,                  sizeof(pIPSD_RTCLocalDate),                     },  // 0xF9  249  // BOBID_IPSD_RTC_LOCAL_DATE
{ &pIPSD_flexDebitOutput.pDESMAC[6],    2,                                              },  // 0xFA  250  // GET_SOM_MAC
{ &wIPSD_paramListGlobSize,             sizeof( wIPSD_paramListGlobSize ),              },  // 0xFB  251  // BOBID_IPSD_PARAMLISTGLOB_SIZE
{ &wIPSD_flexDebitOutputGlobSize,       sizeof( wIPSD_flexDebitOutputGlobSize ),        },  // 0xFC  252  // BOBID_IPSD_FLEXDEBIT_OUTPUT_GLOB_SIZE
{ &wIPSD_barcodeDataGlobSize,           sizeof( wIPSD_barcodeDataGlobSize ),            },  // 0xFD  253  // BOBID_IPSD_DEBIT_OUTPUT_GLOB_SIZE
{ &pIPSD_Bl_ReplyRaw,                   sizeof(pIPSD_Bl_ReplyRaw),                      },  // 0xFE  254  // BOBID_IPSD_BL_REPLYRAW
{ &wIPSD_Bl_ReplySize,                  sizeof(wIPSD_Bl_ReplySize),                     },  // 0xFF  255  // BOBID_IPSD_BL_REPLYSIZE
{ &pIPSD_Bl_ReplyHeader,                sizeof(pIPSD_Bl_ReplyHeader),                   },  // 0x100 256  // BOBID_IPSD_BL_RESPHDR
{ &bIPSD_Bl_ReplyStatus,                sizeof(bIPSD_Bl_ReplyStatus),                   },  // 0x101 257  // BOBID_IPSD_BL_RESPSTATUS

{ &pIPSD_Bl_MicroStatusString,          sizeof(pIPSD_Bl_MicroStatusString),             },  // 0x102 258  // BOBID_IPSD_BL_GET_MICRO_STATUS   
{ &bIPSD_Bl_HdrRemainder,               sizeof(bIPSD_Bl_HdrRemainder),                  },  // 0x103 259  // BOBID_IPSD_BL_HDR_REMAINDER    
{ &edmXferStructPtr,                    sizeof(edmXferStructPtr),                       },  // 0x104 260  // BOBID_IPSD_BL_PASSTHROUGH_MSG
{ &pIPSD_BL_HdrAdr,                     sizeof(pIPSD_BL_HdrAdr),                        },  // 0x105 261  // BOBID_IPSD_BL_ADR            
{ (void*)pIPSD_BL_HdrFill,              sizeof(pIPSD_BL_HdrFill),                       },  // 0x106 262  // BOBID_IPSD_BL_HDRFILL        
{ &bIPSD_Bl_ReplyState,                 sizeof(bIPSD_Bl_ReplyState),                    },  // 0x107 263  // BOBID_IPSD_BL_RESPSTATE
{ &bIPSD_ProgMode,                      sizeof(bIPSD_ProgMode),                         },  // 0x108 264  // BOBID_IPSD_GET_PROG_MODE
{ &edmXferStructPtr,                    sizeof(edmXferStructPtr),                       },  // 0x109 265  // BOBID_IPSD_TRANSPORT_LOCK_KEY
{ &pIPSD_4ByteCmdStatusResp,            sizeof(pIPSD_4ByteCmdStatusResp),               },  // 0x10A 266  // BOBID_RUNSELFTESTS_GETSTATUS
{ &pIPSD_FirmwareVerDateStr[0],         sizeof(pIPSD_FirmwareVerDateStr),               },  // 0x10B 267  // BOBID_IPSD_FIRMWAREVER_DATE_STR
{ &currentInscrSelection,               sizeof(currentInscrSelection),                  },  // 0x10C 268  // BOBID_UPDATE_AND_READ_INSCR_SELECT
{ &currentAdSelection,                  sizeof(currentAdSelection),                     },  // 0x10D 269  // BOBID_UPDATE_AND_READ_AD_SELECT
{ &currentIndiToPrintReadOnly,          sizeof(currentIndiToPrintReadOnly),             },  // 0x10E 270  // BOBID_UPDATE_AND_READ_INDICIA_TO_PRINT
};                                                                                                                      
                                                                                      
//  COMPILED FUNCTION CROSS-REFERENCE

const void *funcCrossRef[] = {
&fnStdPreMsgTask,                   // 0x0   0   //  fnSTDPREMSGTASK             preMessage
&fnStdPostMsgTask,                  // 0x1   1   //  fnSTDPOSTMSGTASK            postMessage
&fnIPSDSetupPowerup,                // 0x2   2   //  fnIPSDSETUPPOWERUP          preMessage
&fnIPSDStorePowerup,                // 0x3   3   //  fnIPSDSTOREPOWERUP          postMessage
&fnTestIPSDPreTask,                 // 0x4   4   //  fnTESTIPSDPRETASK           preMessage
&fnTestIPSDPostTask,                // 0x5   5   //  fnTESTIPSDPOSTTASK          postMessage
&fnIPSDcheckLoginState,             // 0x6   6   //  fnIPSDCHECKLOGINSTATE       preMessage
&fnIPSDloginPre,                    // 0x7   7   //  fnIPSDLOGINPRE              preMessage
&fnIPSDPostClockInit,               // 0x8   8   //  fnIPSDPOSTCLOCKINIT         postMessage
&fnIPSDcalcGMToffset,               // 0x9   9   //  fnIPSDCALCGMTOFFSET         preMessage
&fnIPSDLoadCreateIndiciaData,       // 0xA   10  //  fnIPSDLOADCREATEINDICIADATA preMessage
&fnPostIPSDGetParamList,            // 0xB   11  //  fnPOSTIPSDGETPARAMLIST      postMessage
&fnIsDebitMode,                     // 0xC   12  //  fnISDEBITMODE               preMessage
&fnGetPrintPrep,                    // 0xD   13  //  fnGETPRINTPREP              preMessage
&fnComputeControlSum,               // 0xE   14  //  fnCOMPUTECONTROLSUM         postMessage
&fnInitImagGen,                     // 0xF   15  //  fnINITIMAGGEN               preMessage
&fnLoadDynamicVars,                 // 0x10  16  //  fnLOADDYNAMICVARS           preMessage
&fnConvertFirmwareVersion,          // 0x11  17  //  fnCONVERTFIRMWAREVERSION    postMessage
&fnWhileMoreIPSDPackets,            // 0x12  18  //  fnWHILEMOREIPSDPACKETS      postMessage 
&fnReplyIPSDredirect,               // 0x13  19  //  fnREPLYIPSDREDIRECT         postMessage 
&fnSendIPSDPacket,                  // 0x14  20  //  fnSENDIPSDPACKET            preMessage
&fnIPSDGetReplyXferCtrlStructPtr,   // 0x15  21  //  fnIPSDGETREPLYXFERCTRLSTRUCTPTR    preMessage
&fnIPSDSetDateTime,                 // 0x16  22  // fnIPSDSETDATETIME           postMessage
&fnIPSDGetDateTime,                 // 0x17  23  // fnIPSDGETDATETIME           postMessage
&fnIPSDGetClockOffset,              // 0x18  24  // fnIPSDGETCLOCKOFFSET        postMessage
&fnLoadEdmXfrCtrlPtr,               // 0x19  25  // fnLOADEDMXFRCTRLPTR         preMessage
&fnIPSDGatherInit,                  // 0x1A  26  // fnIPSDGATHERINIT            preMessage
&fnIPSDLoadEncrKey,                 // 0x1B  27  // fnIPSDLOADENCRKEY           preMessage  
&fnLoadTestPrint,                   // 0x1C  28  // fnLOADTESTPRINT             preMessage
&fnLoadReport,                      // 0x1D  29  // fnLOADREPORT                preMessage
&fnUpdateTheBatch,                  // 0x1E  30  // fnUPDATETHEBATCH            postMessage
&fnGenStaticImageReqScriptSelect,   // 0x1F  31  // fnGENSTATICIMAGEREQSCRIPTSELECT     preMessage
&fnPostMfgGetPsdChallengeAndStat,   // 0x20  32  // fnPOSTMFGGETPSDCHALLENGEANDSTAT     postMessage
&fnPostGetStateCheckMfg,            // 0x21  33  // fnPOSTGETSTATECHECKMFG      postMessage
&fnLoadMailSpecs,                   // 0x22  34  // fnLOADMAILSPECS             preMessage
&fnSetNormalOrLowIndicia,           // 0x23  35  // fnSETNORMALORLOWINDICIA     preMessage
&fnSetIGIndiciaType,                // 0x24  36  // fnSETIGINDICIATYPE          preMessage
&fnSetIGAd,                         // 0x25  37  // fnSETIGAD                   preMessage
&fnSetIGInscr,                      // 0x26  38  // fnSETIGINSC                 preMessage
&fnLoadIndicia,                     // 0x27  39  // fnLOADINDICIA               preMessage
&fnIndiciaQuackers,                 // 0x28  40  // fnINDICIAQUACKERS           preMessage
&fnTestIPSDPreTask,                 // 0x29  41  // AVAILABLE FOR FUTURE USE    preMessage
&fnWhileMoreIPSDPacketsAllowLPE,    // 0x2A  42  // fnWHILEMOREIPSDPACKETSALLOWLPE postMessage
&fnAllowError,                      // 0x2B  43  // fnALLOWERROR                preMessage
&fnSwitchReportScript,              // 0x2C  44  // fnSWITCHREPORTSCRIPT        preMessage  
&fnClearCMReplyStatus,              // 0x2D  45  // fnCLEARCMREPLYSTATUS        preMessage
&fnInitReportPageInfo,              // 0x2E  46  // fnINITREPORTPAGEINFO        preMessage
&fnPostConvertWatchdogTimer,        // 0x2F  47  // fnPOSTCONVERTWATCHDOGTIMER  postMessage
&fnIPSDPacketize2IButtonP20,        // 0x30  48  // fnIPSDPACKETIZE2IBUTTONP20  preMessage
&fnPickAZipcode,                    // 0x31  49  // fnPICKAZIPCODE              preMessage
&fnPreIPSDVerifyHashSignature,      // 0x32  50  // fnPREIPSDVERIFYHASHSIGNATURE preMessage
&fnBobShadowRefill,                 // 0x33  51  // fnBOBSHADOWREFILL           preMessage
&fnBobShadowDebit,                  // 0x34  52  // fnBOBSHADOWDEBIT            postMessage
&fnCheckAccountFull,                // 0x35  53  // fnCHECKACCOUNTFULL          preMessage
&fnAndYouShallBeCalled,             // 0x36  54  // fnANDYOUSHALLBECALLED       postMessage
&fnSavePSDStatus,                   // 0x37  55  // fnSAVEPSDSTATUS             preMessage
&fnLoadAdInscOnly,                  // 0x38  56  // fnLOADADINSCONLY            preMessage
&fnSwitchPreDebitScript,            // 0x39  57  // fnSWITCHPREDEBITSCRIPT      preMessage
&fnSwitchDebitScript,               // 0x3A  58  // fnSWITCHDEBITSCRIPT         preMessage
&fnSetTownCircle,                   // 0x3B  59  // fnSETTOWNCIRCLE             preMessage
&fnCheckIndiciaCurrency,            // 0x3C  60  // fnCHECKINDICIACURRENCY      preMessage
&fnLoadPermit,                      // 0x3D  61  // fnLOADPERMIT                preMessage
&fnSetIGPermitType,                 // 0x3E  62  // fnSETIGPERMITTYPE           preMessage
&fnSetCurrentPermitName,            // 0x3F  63  // fnSETCURRENTPERMITNAME      preMessage
&fnSetCurrentPermitSelection,       // 0x40  64  // fnSETCURRENTPERMITSELECTION preMessage
&fnLoadDateTime,                    // 0x41  65  // fnLOADDATETIME              preMessage
&fnPermitQuackers,                  // 0x42  66  // fnPERMITQUACKERS            preMessage
&fnDateTimeQuackers,                // 0x43  67  // fnDATETIMEQUACKERS          preMessage
&fnIsGerman,                        // 0x44  68  // fnISGERMAN                  preMessage
&fnIsNotGerman,                     // 0x45  69  // fnISNOTGERMAN               preMessage
&fnSwitchPVDScript,                 // 0x46  70  // fnSWITCHPVDSCRIPT           preMessage
&fnXferPsdNonce2XferContStruct,     // 0x47  71  // fnXFERPSDNONCE2XFERCONTSTRUCT, postMessage
&fnChooseEdcRecord,                 // 0x48  72  // fnCHOOSEEDCRECORD,          preMessage
&fnSetIGInscrNone,                  // 0x49  73  // fnSETIGINSCRNONE            preMessage
&fnSetIGAdNone,                     // 0x4A  74  // fnSETIGADNONE               preNessage
&fnSetLeadingImage,                 // 0x4B  75  // fnSETLEADINGIMAGE           preMessage
&fnSetLaggingImage,                 // 0x4C  76  // fnSETLAGGINGIMAGE           preMessage
&fnValidateShadowLogs,              // 0x4D  77  // fnVALIDATESHADOWLOGS        preMessage
&fnCheckPCEndOfLife,                // 0x4E  78  // fnCHECKPCENDOFLIFE          preMessage
&fnBobShadowRefund,                 // 0x4F  79  // fnBOBSHADOWREFUND           preMessage
&fnDetermineInscrSelection,         // 0x50  80  // fnDETERMINEINSCRSELECTION   preMessage
&fnSwitchAuditScript,               // 0x51  81  // fnSWITCHAUDITSCRIPT         preMessage
&fnBobEarlyReply,                   // 0x52  82  // fnBobEarlyReply             postMessage
&fnLoadKindofDynamicVars,           // 0x53  83  // fnLoadKindofDynamicVars     preMessage
&fnBobClearBC,                      // 0x54  84  // fnBobClearBC                preMessage
&fnUpdateTheBatchAndRegs,           // 0x55  85  // fnUpdateTheBatchAndRegs     postMessage
&fnBobPrintComplete,                // 0x56  86  // fnBobPrintComplete          preMessage
&fnSwitchGetAllRegsScript,          // 0x57  87  // fnSwitchGetAllRegsScript    preMessage
&fnComputeDerivedPieceCount,        // 0x58  88  // fnComputeDerivedPieceCount  postMessage
&fnInitShadowLogs,                  // 0x59  89  // fnInitShadowLogs            preMessage
&fnCheckIndiciaQuackers,            // 0x5A  90  // fnCheckIndiciaQuackers      preMessage
&fnSetIGText,                       // 0x5B  91  // fnSetIGText                 preMessage
&fnGetVasBarcodes,                  // 0x5C  92  // fnGetVasBarcodes            preMessage
&fnPreCheckIPSDType,                // 0x5D  93  // fnPreCheckIPSDType          preMessage
&fnPostCheckGeminiMode,             // 0x5E  94  // fnPostCheckGeminiMode       postMessage
&fnPreGBLPassthroughFromEDM,        // 0x5F  95  // fnPreGBLPassthroughFromEDM  preMessage
&fnPostGBLPassthroughFromEDM,       // 0x60  96  // fnPostGBLPassthroughFromEDM  postMessage
&fnPreRunSelfTests,                 // 0x61  97  // fnPreRunSelfTests           preMessage
&fnPostRunSelfTests,                // 0x62  98  // fnPostRunSelfTests          postMessage
&fnPreExitIpsdApp,                  // 0x63  99  // fnPreExitIpsdApp            preMessage
&fnPostExitIbtnApp,                 // 0x64 100  // fnPostExitIbtnApp           postMessage
&fnIfGetMicroStatusAgain,           // 0x66 101  // fnIfGetMicroStatusAgain     preMessage
&fnAlwaysOK,                        // 0x66 102  // fnAlwaysOK                  postMessage
&fnPostCheckGemini,                 // 0x67 103  // fnPostCheckGemini           postMessage
&fnDetermineAdSelection,        // 0x68 104  // fnDetermineAdSelection           postMessage
};
