/************************************************************************
 PROJECT:        Future Phoenix
 COPYRIGHT:      2005, Pitney Bowes, Inc.
 AUTHOR:         
 MODULE NAME:    OiScrnfp.c  

 DESCRIPTION:    This file contains the screen table required to
                 map the screen identification constants in oiscrnP.h
                 to the actual screen IDs for Phoenix.

----------------------------------------------------------------------
 MODIFICATION HISTORY:

  15-May-14 Rong Junyu on FPHX 02.12 cienet branch
    Added  entry for screen MMIndiciaPrinting.

  03-Apr-14 Renhao on FPHX 02.12 cienet branch
	 Added  entry for screen  ErrAdvanceManual,ErrRatesExpire.

 10-Mar-14 sa002pe on FPHX 02.12 shelton branch
 	Related to Janus Fraca 224510:
	1. Added an entry for mAbacusAccountingDisabled.

 08-Mar-13 sa002pe on FPHX 02.10 shelton branch
 	For Janus Fraca 219851: Added screen ID for AbacusMXEnterMetricWeight.

 22-Feb-13 sa002pe on FPHx 02.10 shelton branch
 	Added placeholders for the screen positions in the list are still unused.
	
 	Merging in changes from the 01.10 branch for Brazil that didn't get merged back to the main branch
----
	  2009.05.27    Clarisa Bellamy  Added screen ID for mLockEnterPasswordForReport, not at the end.
	                                  Requires a change to oiscrnfp.h.
----

  2012.10.15 Clarisa Bellamy    fphx 02.10
  - Fix fraca 215576 - If an update tag is received when not an a screen that 
    can handle it, handle it the next time one of those screens is reached.
    - Added mHostedAbacusUploadProgress to the table. 

  11/23/2012      John Gao        Added mAbacusAccountLimitError to fix fraca GMSE00131524
  09/24/2012      John Gao        Added mErrWrongUsbPlatform for the unsupported USB platform check.
  08/14/2012      Bob Li          For support of Intelligent Ink Management, 
                                  added screen IDs for ServModeNonPBApprovedManufacturer 
                                  and ServModePBApprovedManufacturer.
  06/30/2011      Joe Zhang   Added a new screen ID for InfraCallErrGeneralRefillSuccess  
  06/29/2010      Jingwei,LI      Added screen ID for mRefillReissueMeter.
  02/25/2010      Raymond Shen    Added screen ID for mSystemRebootNeeded.
  07/29/2009      Jingwei,Li      Added screen ID for mEServiceOutOfLabel and mEServiceOutOfLabel.
  06/17/2009      Raymond Shen    Added screen ID for mAbacusDataUploadAlert, mAccountTypeIsOff, 
                                  mAbacusTransactionClearQueryCon, and mAbacusAccountResetAllQuery.
  09/08/2008      Joe Qu          Replace place holder screen IDs in pScreenTable[] with their corresponding
                                  screen IDs now that refill screens have been merged from fphx01.11 to fphx01.13, 
                                  they are mInfraCallErrPbPAmtExceedDCMax, mInfraCallErrPbPAmtBelowDCMin
                                  and mInfraCallErrPbPAmtExceedDCAuth.
  08/29/2008      Joe Qu          Added these screen IDs for mInfraCallErrPbPAmtBelowDCMin, mInfraCallErrPbPAmtExceedDCAuth
                                  and mInfraCallErrPbPAmtExceedDCMax in pScreenTable[]
  08/27/2008      Oscar Wang      Added screen ID for mEServiceRecordManifesting
  08/13/2008      Joey Cui        Added screen ID for mRefillSuccessful
  08/01/2008      Raymond Shen    Added screen ID for mInfraProcessDataUploadErr and mInfraProcessDataUploadComplete.
  07/18/2008      Raymond Shen    Added screen ID for mWrnBatteryLow and mWrnBatteryEOL.
  06/05/2008      Bob Li          Added screen ID for mPresetConfirmCurrentSetting.
  03/10/2008      Oscar Wang      Added screen ID for mErrPieceCountEOL and mErrPieceCountEOLWarning.
  01/24/2007      Adam Liu        Added screen ID for mAbacusAccountPeriodMenu2
  11/28/2007      Joey Cui        Added screen ID for mLockEnterPassword
  11/09/2007      Andy Mo         Added screen ID for mEServiceAddModeEnterDUNSNum
  10/08/2007      Raymond Shen    Removed screen ID for mEServiceLogNearFull.
  09/07/2007      Oscar Wang      Added screen ID for mEServiceNoEnoughBarcodeForTape.
  08/28/2007      Raymond Shen    Added screen ID for mRefillEnterAmount2, mPBPBalancesRefillEnterAmt2.
  08/14/2007      Oscar Wang  Added screen ID for mEServiceCMOSLogFull.
  08/10/2007      Oscar Wang  Added screen ID for mEServiceSelectOrEnterConsignee
  08/10/2007      Andy Mo     Added screen ID for reportmenu
  08/09/2007      Oscar Wang  Added screen ID for mEServiceConsigneeDescriptionEntry
  08/07/2007      Joey Cui    Added screen ID for mEServiceManifestLimitReached
  08/03/2007      Joey Cui    Added screen ID for 
                              mRateEnterReplyMail
                              mEServiceLogNearFull
                              mEServiceLowBarcodeWarning
                              mEServiceNoBarcodeAvailable
  05/31/2007      Dan Zhang   Added screen ID for mFundsOptions.
  05/29/2007      Raymond Shen    Added screen ID for mAccountSwitchoverWarning
  04/19/2007    I. Le Goff  Added screen id for mRateEnterWeightLBofAVOIRDUPOIS, mRateEnterWeightMetric, 
                            mRateErrRMNotInitialized
    04/26/2007      Andy Mo     Added screen IDs for
                                MMDateCorrectionReady
                                MMDateCorrectionError 
                                MMManifestReady
                                MMManifestError
                                MMManifestEnterAmount

    04/20/2007      Andy Mo     Deleted screen IDs for 
                                mErrInsufficentFunds
                                mRateErrManualPostageFile

    04/19/2007      Andy Mo     Added screen IDs for
                                mMMPostageCorrectionReady
                                mMMPostageCorrectionError
                                mMMPostageCorrectionEnterAmount 
                                mRateErrRMManualPostageFile
                                mErrInsufficientFunds           
    04/18/2007      Raymond Shen    Added screen id for mAbacusAccountCannotDelete.
    04/17/2007      Joey Cui    Added screen id for mAbacusErrAccountingDiscrepancy3
                                and mAbacusErrExtMemNotRespond.
    04/13/2007      Oscar Wang  Added screen id for mSetupEnterDUNSNum
                                and mEServicePrintManifestPrompt.
    04/12/2007      Adam Liu    Added screen id for mAbacusDateRangeEnter.  
    04/12/2007      Dan Zhang   Add screen for abacus summary, budget preformance
                                and transaction log report.

    04/11/2007      Raymond Shen    Added screen id for mAbacusAuthorizationRequired.
    04/10/2007      Raymond Shen    Added screen id for mAbacusOperatorLogonIDEnter.
    03/30/2007      Joey Cui        Add screen ids for manual transaction screen
    29 Mar 2007 Bill Herring    Added Meter Move complete/fail screens and credit
                                limit refill screen
    03/29/2007      Oscar Wang      Added screen ids for rate change screens.
    03/29/2007      Vincent Yi      Added screen ids for Abacus screens
  03/27/2007    I. Le Goff  Rename mMMAjoutNextEnvelope into mMMAjoutNextPiece
    03/27/2007      Raymond Shen   Removed screen id for mAbacusAccountConfirmDelete 
                                   and added screen if for mAbacusAccountNameEnter.
  03/26/2007    I. Le Goff  Added screen id for mMMAjoutNextEnvelope, mAjoutErrNegativeValue,
                            mAjoutErrNoScale
    03/22/2007      Raymond Shen   Added screen ids for mHostedAbacusAccountingSetupMenu
                                                    mAcctNeedCreateOne
                                                    mAbacusAccountConfirmDelete 
    03/22/2007      Adam Liu        Added screen ids for Abacus home screens    
  03/12/2007    I. Le Goff  Added screen id for mMMAjoutReady, mMMAjoutError
    02/09/2007      Kan Jiang       Added screen id for mPresetInvalidSettings
    02/06/2007      Vincent Yi      Added screen id for InfraCallErrGeneral
    01/18/2007      Kan Jiang       Added screen id for mEServiceUploadRecordPrompt.
    01/05/2006      Oscar Wang      Added screen id for mWeightRateOptionMain.
    12/14/2006      Adam Liu        Added screen id mPBPDcapUploadDue,
                                                    mPBPDcapUploadReq,   
                                                    mPBPDcapBackup,
                                                    mRateErrDataCaptureEngine
    12/12/2006      Oscar Wang      Added screen id for KIPEnterAmount and 
                                    KIPCheckPassword. 
    11/15/2006      Vincent Yi      Adjusted the location of screen id SCREEN_HIGHPOSTAGE_EXCEEDED
    09/13/2006      Vincent Yi      Added screen id for ErrFeederError
    09/08/2006      Vincent Yi      Removed screen id for ErrMidnightAdvDateConfirm
    09/08/2006      Oscar Wang      Added screen id for mRateNewEffective.
    09/06/2006      Vincent Yi      Added screen id for ErrMidnightAdvDateConfirm
    09/05/2006      Dicky Sun       For GTS deleting manifest record, add the
                                    following screen id:
                                       mEServiceRecordDeleteProgress,
                                       mEServiceMaxNumberOfRecords
    09/04/2006      Raymond Shen    Added mScaleSetLocationCode for entering location code
                                    before navigating to main screen on condition that no location
                                    code has been set.
    09/04/2006      Vincent Yi      Added screen id for ErrNetworkPowerOff
    08/28/2006      Oscar Wang      Added screenID for RateDiffLastItem.
    08/17/2006      Oscar Wang      Added screenID for RateErrMaxPostageExceeded
    08/16/2006      Vincent Yi      Added screen ids for ErrTapeErrors, ErrJamLeverOpen
    08/10/2006      Oscar Wang      Added raGMSE00102589ting error screen 
                                    temporarily for testing.
    08/02/2006      Dicky Sun       Add mMMIndiciaTransient for GTS custom
                                    settings.
    07/28/2006      Dicky Sun       Add the following screen ids for uploading
                                    record workflow:
                                       mEServiceInvalidZIPWarning     
                                       mEServicePromptReceipt         
                                       mEServiceCheckPendingRecord
    07/18/2006      Vincent Yi      Clear up screen IDs
    07/18/2006      Dicky Sun       Add mEServiceRateAgainPrompt screen id.
    07/14/2006      Oscar Wang      Added mEServiceNotActive screen.
    07/12/2006      Kan Jiang       Add AutoDate Adv screen: AUTO_DATEADVANCE_CONFIRM
    07/07/2006      Dicky Sun       Add GTS related screens.
    22/06/2006      Raymond Shen    Added OOB related screens.
    12/27/2005      Vincent Yi      Initial version

    OLD PVCS REVISION HISTORY
 * \main\jnus11.00_i1_shelton\2 cx17598 12 oct 2005
 *
 *  Added screen RateActivatedByDateAdv3 which is for waning the rate rule needs
 *  to be updated to the screen table.
 *
*************************************************************************/


/**********************************************************************
        Include Header Files
**********************************************************************/
#include "oifunctfp.h"
#include "oiscrnfp.h"
#include "oit.h"                                                                


/**********************************************************************
        Local Function Prototypes
**********************************************************************/

/**********************************************************************
        Local Defines, Typedefs, and Variables
**********************************************************************/

/**********************************************************************
        Global Functions Prototypes
**********************************************************************/

/**********************************************************************
        Global Variables
**********************************************************************/
/* The oiscrnfp.h file defines the indices into this table, so the order 
   of the entries in this table must match those indices.  Otherwise, 
   the order is not relevent. */

const unsigned short pScreenTable[] =       
{
    mPowerOn,                           /* 0 */
    mMMIndiciaReady,                    /* 1 */
    mMMIndiciaError,                    /* 2 */
    0x0000,                   /* 3 */
    0x0000,                   /* 4 */
    0x0000,                     /* 5 */
    0x0000,                     /* 6 */
    0x0000,                   /* 7 */
    0x0000,              /* 8 */
    0x0000,               /* 9 */
	
    0x0000,               /* 10 */
    0x0000,             /* 11 SCREEN_AD_DATE_TIME_ONLY_READY*/
    0x0000,             /* 12 SCREEN_AD_DATE_TIME_ONLY_ERROR*/
    mMMIndiciaReadyTransient,           /* 13 */
    0x0000,            /* 14 */
    0x0000,         /* 15 */
    0x0000,      /* 16 */
    0x0000,          /* 17 */
    0x0000,          /* 18 */
    0x0000,                /* 19 */
	
    0x0000,             /* 20 */
    0x0000,          /* 21 SCREEN_AD_DATE_TIME_ONLY_DISABLED*/
    mMMIndiciaTransient,                /* 22 */
    0x0000,                /* 23 */
    0x0000,          /* 24 */
    0x0000,                 /* 25 METER_LOCKED */
    mWrnBatteryLow,                     /* 26 */
    mManufacturingPSDDisabled,          /* 27 */
    mManufacturingPSDMisMatch,          /* 28 */
    0x0000,      /* 29 */
	
    mErrMeterError,                     /* 30 */
    mInspectionRequired,                /* 31 */
    mInspectionWarning,                 /* 32 */
    mErrMeterUnauthorized,              /* 33 */
    mErrInkTankLidToOpenCover,          /* 34 */
    mErrTopCoverOpen,                   /* 35 */
    mErrPaperErrors,                    /* 36 */
    mErrInkTankLidOpen,                 /* 37 */
    mErrPrinterFault,                   /* 38 */
    mErrMissingGraphic,                 /* 39 */
	
    mErrMissingImage,               /* 40 */
    mErrMeterErrorPowerOff,         /* 41 */
    mErrFeederFatalFault,               /* 42 */ //mErrFeederError
    mErrInkOut,                         /* 43 */
    mErrInkTank,                        /* 44 */
    mErrNoInkTank,                      /* 45 */
    mErrPrinthead,                      /* 46 */
    mErrNoPrinthead,                    /* 47 */
    mErrWasteTankFull,                  /* 48 */
    mErrWasteTankNearFull,              /* 49 */
	
    mErrInkTankExpiring,                /* 50 */
    mErrJamLeverOpen,                   /* 51 */
    mErrTapeErrors,                     /* 52 */
    mErrNetworkPowerOff,                /* 53 */
    mErrFeederError,                    /* 54 */
    mErrHighPostageExceeded,            /* 55 */
    0x0000,     /* 56 */
    0x0000,          /* 57 */
    0x0000,                    /* 58 */
    0x0000,               /* 59 */

    0x0000,                 /* 60 */
    0x0000,                   /* 61 */
    0x0000,                      /* 62 */
    0x0000,                   /* 63 */
    0x0000,                 /* 64 */
    0x0000,              /* 65 */
    0x0000,         /* 66 */
    0x0000,                /* 67 */
    0x0000,                    /* 68 */
    0x0000,         /* 69 */
	
    0x0000,    /* 70 */
    0x0000,             /* 71 */
    0x0000,           /* 72 */
    /* Temporarily place here, To be replaced by mRateErrInvalidWeightForClass */ 
    0x0000,           /* 73 */
	/**************************************************************************/
    0x0000,                  /* 74 */
    0x0000,              /* 75 */
    0x0000,                  /* 76 */
    mKIPEnterAmount,                    /* 77 */
    mKIPCheckPassword,                  /* 78 */
    0x0000,          /* 79 */

    0x0000,                 /* 80 */
    0x0000,                      /* 81 */
    0x0000,                /* 82 */
    0x0000,             /* 83 */
    0x0000,           /* 84 */
    0x0000,                   /* 85 */
    0x0000,      /* 86 */
    0x0000,   /* 87 */
    0x0000,      /* 88 */
    0x0000,/* 89 */
	
    0x0000,      /* 90 */
    0x0000,               /* 91 */
    0x0000,        /* 92 */
    0x0000,             /* 93 */
    0x0000,               /* 94 */
    0x0000,            /* 95 */
    0x0000,           /* 96 */
    0x0000,                  /* 97 */
    0x0000,       /* 98 */
    0x0000,     /* 99 */
	
    0x0000,       /* 100 */
    0x0000,                  /* 101 */
    0x0000,                 /* 102 */
    0x0000,           /* 103 */
    0x0000,         /* 104 */
    0x0000,             /* 105 */
    0x0000,        /* 106 */
    0x0000,      /* 107 */
    0x0000,        /* 108 */
    0x0000,        /* 109 */

    0x0000,                  /* 110 */
    0x0000,                  /* 111 */
    0x0000,                     /* 112 */
    0x0000,              /* 113 */
    0x0000,                      /* 114 */
    0x0000,                      /* 115 */
    0x0000,                  /* 116 */
    0x0000,             /* 117 */
    0x0000,                   /* 118 */
    0x0000,         /* 119 */
    
    0x0000,    /* 120 */
    0x0000,               /* 121 */
    0x0000,          /* 122 */
    0x0000,         /* 123 */
    0x0000,                   /* 124 */
    0x0000,   /* 125 */
    0x0000,                 /* 126 */
    0x0000,            /* 127 */
    0x0000,        /* 128 */
    0x0000,          /* 129 */
	
    0x0000,        /* 130 */
    0x0000,           /* 131 */
    0x0000,        /* 132 */
    0x0000,       /* 133 */
    0x0000,   /* 134 */
    0x0000,         /* 135 */
    0x0000,    /* 136 */
    0x0000,                /* 137 */
    0x0000,              /* 138 */
    0x0000,         /* 139 */
	
    0x0000,         /* 140 */
    0x0000,          /* 141 */
    0x0000,          /* 142 */
    0x0000,                /* 143 */
    0x0000,                 /* 144 */
    0x0000,              /* 145 */
    0x0000,                     /* 146 */
    0x0000,                  /* 147 */
    0x0000,                    /* 148 */
    0x0000,          /* 149 */

    mPSOCPleaseWait,                    /* 150 */
    0x0000,                 /* 151 */
    0x0000,       /* 152 */
    0x0000,          /* 153 */
    0x0000,          /* 154 */
    0x0000,    /* 155 */
    0x0000,             /* 156 */
    0x0000,             /* 157 */
    0x0000,                   /* 158 */
    0x0000,                   /* 159 */
	
    0x0000,             /* 160 */
    0x0000,    /* 161 */
    0x0000,             /* 162 */
    0x0000,           /* 163 */
    mFundsOptions,                      /* 164 */
    0x0000,                /* 165 */
    0x0000,                   /* 166 */
    0x0000,         /* 167 */
    0x0000,        /* 168 */
    0x0000,      /* 169 */

    0x0000,  /* 170 */
    0x0000,                       /* 171 */
    0x0000,             /* 172 */
    0x0000,    /* 173 */
    0x0000,               /* 174 */
    0x0000,                /* 175 */
    0x0000,        /* 176 */
    0x0000,    /* 177 */
    0x0000,       /* 178 */
    0x0000,                 /* 179 */
	
    mErrPieceCountEOL,                  /* 180 */
    mErrPieceCountEOLWarning,           /* 181 */
    0x0000,       /* 182 */
    mWrnBatteryEOL,                     /* 183 */
    0x0000,         /* 184 */
    0x0000,    /* 185 */
    0x0000,         /* 186 */
    0x0000,     /* 187 */
    0x0000,      /* 188 */
    0x0000,    /* 189 */

    0x0000,                  /* 190 */
    0x0000,             /* 191 */
    0x0000,                  /* 192 */
    0x0000,    /* 193 */
    0x0000,        /* 194 */
    0x0000,                /* 195 */
    0x0000,       /* 196 */
    mSystemRebootNeeded,                /* 197 */
    0x0000,                /* 198 */
    0x0000,  /* 199 */
	
    0x0000, /* 200 */
    0x0000,    /* 201 */
    mErrWrongUsbPlatform,               /* 202 */
    0x0000,           /* 203 */
    0x0000,        /* 204 */
    0x0000,         /* 205 */
    mErrManualAdvanceDate,                   /* 206 */
    mErrRatesExpire,                   /* 207 */
    mMMIndiciaPrinting, 					/*208*/
    0x0000,                   /* 209 */
	
    0x0000,                   /* 210 */
    0x0000,                   /* 211 */
    0x0000,                   /* 212 */
    0x0000,                   /* 213 */
    0x0000,                   /* 214 */
    0x0000,                   /* 215 */
    0x0000,                   /* 216 */
    0x0000,                   /* 217 */
    0x0000,                   /* 218 */
    0x0000,                   /* 219 */
	
    0x0000,                   /* 220 */
    0x0000,                   /* 221 */
    0x0000,                   /* 222 */
    0x0000,                   /* 223 */
    0x0000,                   /* 224 */
    0x0000,                   /* 225 */
    0x0000,                   /* 226 */
    0x0000,                   /* 227 */
    0x0000,                   /* 228 */
    0x0000,                   /* 229 */
	
    0x0000,                   /* 230 */
    0x0000,                   /* 231 */
    0x0000,                   /* 232 */
    0x0000,                   /* 233 */
    0x0000,                   /* 234 */
    0x0000,                   /* 235 */
    0x0000,                   /* 236 */
    0x0000,                   /* 237 */
    0x0000,                   /* 238 */
    0x0000,                   /* 239 */
	
    0x0000,                   /* 240 */
    0x0000,                   /* 241 */
    0x0000,                   /* 242 */
    0x0000,                   /* 243 */
    0x0000,                   /* 244 */
    0x0000,                   /* 245 */
    0x0000,                   /* 246 */
    0x0000,                   /* 247 */
    0x0000,                   /* 248 */
    0x0000,                   /* 249 */

    0x0000,                   /* 250 */
    0x0000,                   /* 251 */
    0x0000,                   /* 252 */
    0x0000,                   /* 253 */
    0x0000,                   /* 254 */
    0xFFFF                    /* 255 */  /* end of table marker */

/* IMPORTANT: Actually the function that gets the screen ID based on the #defines
	in oiscrnfp.h only accepts a byte, so 0xFF is used as the offset to the end of
	table marker, so don't load a screen name for 255 (e.g., 0xFF)! */

};


/**********************************************************************
        Public Functions
**********************************************************************/
/* Utility functions */

/* Pre/post functions */

/* Field functions */

/* Hard key functions */

/* Event functions */

/* Variable graphic functions */

/**********************************************************************
        Private Functions
**********************************************************************/
