/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    api_utils.c

   DESCRIPTION:    API utility functions

 ----------------------------------------------------------------------
               Copyright (c) 2017 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------

*************************************************************************/
#include "commontypes.h"
#include "cJSON.h"
#include "ctype.h"
#include "wsox_tx_queue.h"
#include "wsox_services.h"
#include "api_utils.h"

#define  CRLF   "\r\n"


/* utility function */
#define RESPONSE_BUF_SIZE 256


void generateResponse(WSOX_Queue_Entry *pEntry, cJSON *rspMsg, const char * funcStr, const char * statStr, cJSON *reqMsg)
{
	char tempBuf[RESPONSE_BUF_SIZE];

	strncpy(tempBuf, funcStr, RESPONSE_BUF_SIZE/4);
	strcat(tempBuf, "Rsp");
	cJSON_AddStringToObject(rspMsg, "MsgID", tempBuf);
	cJSON_AddStringToObject(rspMsg, "Status", statStr);

	strcpy(tempBuf, "  ");
	strncat(tempBuf, funcStr, RESPONSE_BUF_SIZE/4);
	strcat(tempBuf, "Req: Added ");
	strncat(tempBuf, funcStr, RESPONSE_BUF_SIZE/4);
	strcat(tempBuf, "Rsp to tx queue. status=%d");
	strcat(tempBuf, CRLF);

    addEntryToTxQueue(pEntry, reqMsg, tempBuf);
}

cJSON *CreateStringItem(const char *pField, char* pString)
{
    cJSON *item = cJSON_CreateObject();
    cJSON_AddStringToObject(item, pField, pString);
    return item;
}


cJSON *CreateNumberItem(const char *pField, int number)
{
    cJSON *item = cJSON_CreateObject();
    cJSON_AddNumberToObject(item, pField, number);
    return item;
}

void clearJSONArray(cJSON *jsonArray)
{
	int arrIndex, arrSize;

	if (jsonArray == NULL)
		return;

	arrSize = cJSON_GetArraySize(jsonArray);
	if (arrSize == 0)
		return;

	for (arrIndex = 0; arrIndex < arrSize; arrIndex++)
		{
			cJSON_DeleteItemFromArray(jsonArray, 0); //always delete the first one
		}
}

/* -------------------------------------------------------------------------------------------------------[stricmp]-- */
int stricmp(const char *s1, const char *s2)
{
    char c1, c2;
    int  diff;

    do
    {
        c1 = *s1++;
        c2 = *s2++;
        diff = toupper(c1) - toupper(c2);
    }while( (diff==0) && (c1 != 0) && (c2 != 0));

    return diff;
}

/* -------------------------------------------------------------------------------------------------------[strapp]-- */
// safestrcat - safe string append
//  safe version of an append utility since we all know that the numeric value passed to  strncat simply ensures no more
//  than that number of bytes will be copied to the destination. This is designed to prevent destination buffer overrun.
//
//  s1  = null terminated destination string
//  s2  = null terminated string to append
//  end = address of the location after the last valid location in the destination string.
//
//  returns TRUE if append is within bounds else FALSE
//
BOOL safestrcat(char *s1, const char *s2, char *end)
{
    while(*s1++);                           /* Find end */

    for(--s1;  (s1 < end) && (*s1++ = *s2++) != 0;);       /* Append s2 and check we have not overrun supplied destination*/

    if(s1 >= end) end[-1] = 0; // force termination of dest string in case of attempted overrun.

    return (s1 < end);
}



