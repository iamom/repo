/**********************************************************************
 PROJECT    :   Horizon CSD
 MODULE     :   wdttask.c

 DESCRIPTION:
    Watchdog Timer Task function and support functions.

* ----------------------------------------------------------------------
*               Copyright (c) 2016 Pitney Bowes Inc.
*                        37 Executive Dr
*                      Danbury, CT 06810
* ----------------------------------------------------------------------
*************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "posix.h"

#include "pbos.h"
#include "hal.h"
#include "watchdog.h"

#define THISFILE "wdttask.c"

static BOOL runBefore = FALSE;

//TODO - Determine if we want to log just prior to a system reset caused by watchdog.
// This would require generating an interrupt from watchdog timeout and having it initiate a log prior to system reset.

/*------------------------*/
/*   Task entry point &   */
/*   main control loop    */
/*------------------------*/
void WatchdogTimerTask (unsigned long argc, void *argv)
{
    while ( 1 )
    {
    	// simple policy for now; in the future can base the reset on messages received from tasks
#ifdef WATCHDOG_TIMER_ENABLE
    	HALResetWDT();
    	if (runBefore == FALSE)
    	{// initial timeout value is long to allow for initialization to finish; now set shorter standard value
    		runBefore = TRUE;
    	    HALStartWDT(WATCHDOG_STANDARD_TIMEOUT_VALUE);
    	}
#endif
    	OSWakeAfter(WATCHDOG_TASK_KICK_VALUE);
    }
}

