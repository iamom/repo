/************************************************************************
  PROJECT:        Horizon CSD
   MODULE NAME:    wsox_services.c

   DESCRIPTION:    Service implementations for web socket server

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------

*************************************************************************/

#include "nucleus.h"
#include "networking/nu_networking.h"
#include "commontypes.h"
#include "varassert.h"
#include "cJSON.h"
#include "am335x_pbdefs.h"
#include "am335x_timer.h"
#include "wsox_tx_queue.h"
#include "wsox_services.h"
#include "lists.h"
#include "hal.h"
#include "clock.h"

#ifdef MEMORYTEST
#include "events.h"
#endif /* MEMORYTEST */

#include "platform.h"
#include "pbos.h"
#include "clock.h"
#include "ossetup.h"
#include "pb232.h"
#include "wsox_EMDHandlers.h"
#include "oit.h"
#include "oifpinfra.h"
#include "oifpprint.h"
#include "oitcm.h"
#include "cm.h"
#include "cmpm.h"
#include "bobutils.h"
#include "oifpmain.h"
#include "memory_stats.h"
#include "networkmonitor.h"

#include "api_diags.h"
#include "api_funds.h"
#include "api_mach_ctrl.h"
#include "api_maint.h"
#include "api_mfg.h"
#include "api_settings.h"
#include "api_status.h"
#include "api_temporary.h"
#include "api_weight.h"
#include "ccdapi.h"
#include "settingsmgr.h"
#include "sysdatadefines.h"
#include "version.h"
#include "debugTask.h"
#include "wsox_server.h"
#include "utils.h"

#define  CRLF   "\r\n"

int                    verbosity=3;

const char *VersionStr =  "csd_sr " CSD_VERSION_STRING;

NU_WSOX_CONTEXT_STRUCT wsox_lstnr;
extern NU_MEMORY_POOL  cJSON_mem_pool;
extern void fnSetThroughput(PRINT_THRUPUT thruput);
extern BOOL trace_wsox_ping_pong_enabled;
extern HBLogStructure globalHBLogStructure;
cJSON *CreateStringItem(const char *pField, char* pString);


#define HBREMAINING_MAX_COUNT		18
UINT32 	globalHBRemaining[HBREMAINING_MAX_COUNT] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
UINT32	globalTotalHB = 0;

static char    DbgBuf[DBG_BUF_LEN]; //For use with WSOX Master task callback logging (no concurrent usage)

NU_PIPE wsox_tablet_usb_devices_rsp_pipe;
NU_PIPE wsox_tablet_usb_write_test_rsp_pipe;
NU_PIPE wsox_tablet_versions_rsp_pipe;
NU_PIPE wsox_tablet_status_rsp_pipe;

char last_hb_rsp_seqnum_attempted_to_send[35];
char last_hb_rsp_seqnum_sent_success[35];
char last_hb_rsp_seqnum_sent_failed[35];

/* ----------------------------------------------------------------------------------------------------[prototypes]-- */

char *wsox_get_next_seq_num_str(uint32_t handle, char *str);
bool is_dbgTrace_allowed_for(cJSON *root);

/* ----------------------------------------------------------------------------------------[Websocket handles list]-- */
#define CFG_WSOX_HANDLE_SIZE   2*1024L

list_t wsox_handles_list;

uint32_t wsox_handles_list_mem[CFG_WSOX_HANDLE_SIZE / sizeof(uint32_t)];

typedef struct
{
    uint32_t handle;
    uint32_t group;
    uint32_t base_seq_num;
    uint32_t heartbeat_interval;               /* expected time (in mSec) between HeartbeatReq messages   */
                                               /* if set to 0 --> no heartbeat checking                   */

    uint32_t heartbeat_timeout;                /* if no heartbeatReq received for this many mSecs --> set */
                                               /* DCOND_NO_TABLET_CONNECTION disabling condition          */

    NU_TIMER *heartbeat_tmr;                    /* nucleus timer control block                             */
    struct addr_struct faddr;                  /* foreign address */
    struct addr_struct laddr;                  /* local address   */
    uint32_t tablet_connection:1, pct_connection:1, csdt_connection:1, dlas_connection:1;  /* type of client connected    */
} wsox_handle_entry_t; /* locate context */


typedef struct
{
    uint32_t handle;

} wsox_loc_handle_context_t; /* locate context */


/* -----------------------------------------------------------------------------------------[fn_locate_wsox_handle]-- */
static list_locate_result_t fn_locate_wsox_handle(void *p_context, void *p_data)

{
    if (p_context && p_data)
    {
        return ((wsox_loc_handle_context_t *) p_context)->handle == 
               ((      wsox_handle_entry_t *) p_data   )->handle ? LOCATE_MATCH : LOCATE_NO_MATCH;
    }

    return LOCATE_ERROR;   /* Error occured */
}

/* --------------------------------------------------------------------------------------------[wsox_remove_handle]-- */
void wsox_remove_handle(uint32_t handle)
{
    list_entry_t *p_entry = NULL;
    wsox_loc_handle_context_t handle_locate_context;

    handle_locate_context.handle = handle;

    p_entry = list_get_with_criteria(&wsox_handles_list, fn_locate_wsox_handle, &handle_locate_context);

    //varassert(p_entry != NULL, "wsox_remove_handle: we should have found handle %d" CRLF, handle);

    if (p_entry == NULL)
    {
    	fnDumpStringToSystemLogWithNum("Error: no handle to remove: ", (int)handle);
    }
    else
    {
        list_free_entry(&wsox_handles_list, p_entry);
    }
}


/* -----------------------------------------------------------------------------------------------[wsox_add_handle]-- */
void wsox_add_handle(uint32_t handle, struct addr_struct *faddr, struct addr_struct *laddr)
{
    list_entry_t *p_entry;
    wsox_handle_entry_t *p_handle;

    p_entry = list_get_new_entry(&wsox_handles_list);

    if (p_entry)
    {
        p_handle = (/*ev_t*/ void *) p_entry->data;
        p_handle->handle = handle;
        p_handle->faddr = *faddr;
        p_handle->laddr = *laddr;
        p_handle->base_seq_num = 0;
        p_handle->tablet_connection = 0;
        p_handle->group = 0;
        p_handle->heartbeat_interval = 0;
        p_handle->heartbeat_timeout  = 0;
        p_handle->heartbeat_tmr = NU_NULL;

        list_append_entry(&wsox_handles_list, p_entry);
    }
    else
    {
       varassert(p_entry != NULL, " wsox_add_handle: couldn't get an entry from the free list" CRLF);
    }
}

/* -----------------------------------------------------------------------------------------[wsox_get_handle_entry]-- */
wsox_handle_entry_t *wsox_get_handle_entry_by_handle(uint32_t handle)
{
    wsox_handle_entry_t *p_handle;
    list_entry_t *p_entry = list_peek_first(&wsox_handles_list);

    while (p_entry)
    {
        p_handle = (/*ev_t*/ void *) p_entry->data;
        if (p_handle && p_handle->handle == handle)
            return p_handle;
        else
            p_entry = p_entry->next;
    }

    return NULL;
}

/* -----------------------------------------------------------------------------------------[wsox_get_handle_entry]-- */
wsox_handle_entry_t *wsox_get_heartBeatTimer_handle()
{
    wsox_handle_entry_t *p_handle;
    list_entry_t *p_entry = list_peek_first(&wsox_handles_list);

    while (p_entry)
    {
        p_handle = (/*ev_t*/ void *) p_entry->data;
        if (p_handle && p_handle->heartbeat_tmr)
            return p_handle;
        else
            p_entry = p_entry->next;
    }

    return NULL;
}

/* -----------------------------------------------------------------------------------------[wsox_get_tablet_handle]-- */
uint32_t wsox_get_tablet_handle()
{
	uint32_t tablet_handle = 0;
	wsox_handle_entry_t *p_handle;
	list_entry_t *p_entry = list_peek_first(&wsox_handles_list);

    while (p_entry)
    {

        p_handle = (/*ev_t*/ void *) p_entry->data;
        if (p_handle->tablet_connection){
        	tablet_handle = p_handle->handle;
        	break;
        }
        else
           p_entry = p_entry->next;
    }

    return tablet_handle;
}

/* -----------------------------------------------------------------------------------------[wsox_get_base_seq_num]-- */
uint32_t wsox_get_base_seq_num(uint32_t handle)
{
    wsox_handle_entry_t *p_handle = wsox_get_handle_entry_by_handle(handle);
    uint32_t base_seq_num = ~0;

    if (p_handle)
    {
        base_seq_num = ++(p_handle->base_seq_num);
    }

    return base_seq_num;
}

/* ----------------------------------------------------------------------------[fn_locate_and_copy_entry_by_handle]-- */
/*
 * this is a function that is passed as a function pointer to the list_process_active() function to
 * find a given handle in the wsox_handles_list. An instance of copy_wsox_handle_list_entry_context_t
 * is passed around as context and is used specify which handle to find and when found, the 
 * list entry is copied to the context so that caller can acces the local and foreign ip address
 * associated with the given wsox handle
 */
typedef struct 
{
    uint32_t handle_to_find;
    wsox_handle_entry_t entry;  /* copy of the entry if found */
} copy_wsox_handle_list_entry_context_t;

static int fn_locate_and_copy_entry_by_handle(copy_wsox_handle_list_entry_context_t *p_context, wsox_handle_entry_t *p_entry)
{
    if (p_context && p_entry)
    {
        if (p_context->handle_to_find == p_entry->handle)
        {
            memcpy(&p_context->entry, p_entry, sizeof(wsox_handle_entry_t));
            return 0;   /* this will stop list_process_active from going down the list any further */
        }
         
    }
    return 1;  /* lets try next list entry */
}

/* -----------------------------------------------------------------------------[wsox_get_local_ip_addr_for_handle]-- */

bool wsox_get_local_ip_addr_for_handle(uint32_t handle, struct addr_struct *laddr)
{
    bool found;

    copy_wsox_handle_list_entry_context_t context;

    context.handle_to_find = handle; 

    list_process_active(&wsox_handles_list, (fn_process_t) fn_locate_and_copy_entry_by_handle, &context);

    /* check if we found it */
    found = (context.entry.handle == handle);
    if (found)
    {
        memcpy(laddr, &context.entry.laddr, sizeof(struct addr_struct));
    }

    return found;
}


/* -----------------------------------------------------------------------------------[tx_data_to_handle_list_item]-- */

typedef struct handle_list_s
{
    struct handle_list_s *next;
    uint32_t       handle;  // websocket connection handle
} handle_list_t;

typedef struct
{
    //char   *data;
    UINT32  mask;
    //UINT64  data_len;
    UINT32  tx_count;
    cJSON  *msg;
    handle_list_t delete_list_head;
    bool    base_seq_num_added;
    bool    dbgTrace_allowed;
} wsox_tx_data_to_list_context_t; /* locate context */

int tx_data_to_handle_list_item(void *p_context, void *p_entry_data)
{  
    wsox_tx_data_to_list_context_t *p_cntx;
    wsox_handle_entry_t            *p_handle;
    UINT64                          tx_len;
    STATUS                          status;
    char                            base_seq_num[35]; // Max: "C123456789-123456789-123456.123"
    char                           *data;
    DATETIME      					rTime;

	
    if (p_context && p_entry_data)
    {
        p_cntx   = (void *)p_context;
        p_handle = (void *)p_entry_data;

        if((p_cntx->mask == 0) || (p_cntx->mask & p_handle->group) != 0)
        {
            cJSON *base_seq_num_obj = cJSON_CreateString(wsox_get_next_seq_num_str(p_handle->handle, base_seq_num));
            if (p_cntx->base_seq_num_added)
            {
                cJSON_ReplaceItemInObject(p_cntx->msg, "BaseSeqNum", base_seq_num_obj);
            }
            else
            {
                cJSON_AddItemToObject(p_cntx->msg, "BaseSeqNum", base_seq_num_obj);
                p_cntx->base_seq_num_added = TRUE;
            }

            p_cntx->tx_count++;
            data = cJSON_PrintUnformatted(p_cntx->msg);
            tx_len = strlen(data);
			status = NU_WSOX_Send(p_handle->handle, data, &tx_len, WSOX_TEXT_FRAME, 0);
			if (status != NU_SUCCESS)
			{
				dbgTrace(DBG_LVL_ERROR, "WTXB(%d): Error sending message: %d" CRLF, (int)p_handle->handle, status);
			    if (status == WSOX_INVALID_HANDLE)  /* -12060 */
                {
                    handle_list_t *p_entry = memory_allocate(sizeof(handle_list_t));
                    if (p_entry)
                    {
                        p_entry->handle = p_handle->handle;
                        p_entry->next = p_cntx->delete_list_head.next;
                        p_cntx->delete_list_head.next = p_entry;
                        dbgTrace(DBG_LVL_ERROR, "WTXB(%d): Added handle %u to handle delete list" CRLF, (int)p_handle->handle, p_entry->handle);
                    }
                    else
                    {
                        dbgTrace(DBG_LVL_ERROR, "WTXB(%d): Failed to add handle %u to handle delete list" CRLF, (int)p_handle->handle, p_handle->handle);
                    }
                }
			}
			else if (verbosity > 1 && p_cntx->dbgTrace_allowed)
			{
				(void)GetSysDateTime( &rTime, ADDOFFSETS, GREGORIAN );
				dbgTrace(DBG_LVL_INFO, "WTXB(%d)[%02d:%02d:%02d]: %s" CRLF,
						(int)p_handle->handle, rTime.bHour, rTime.bMinutes, rTime.bSeconds, data);
			}
			memory_free(data);
        }
    }

    return 1;   /* to indicate we can go to next item in the list */
}

extern BOOL fNoTabletConnection;
/* -----------------------------------------------------------------------------------------[on_ConnectionStartReq]-- */
void on_ConnectionStartReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg,  "MsgID", "ConnectionStartRsp"   );
    cJSON_AddStringToObject(rspMsg, "Server", VersionStr);

    if (cJSON_HasObjectItem(root, "Client"))
    {
        //WSOX_Queue_Entry schedule_ping_entry  =  { WSOX_QET_SCHEDULE_PING, handle};
        cJSON            *param;
        char             *client;
        char             sz_line[50];

        wsox_handle_entry_t *wsox_info = wsox_get_handle_entry_by_handle(handle);

        client = cJSON_GetObjectItem(root, "Client")->valuestring;
        if (strstr(client, "csd_tablet"))
        {
            /* we have a tablet connection --> remove the disabling condition */
            fNoTabletConnection = FALSE;
            fnClearDisabledStatus(DCOND_NO_TABLET_CONNECTION);
            if (wsox_info)
                wsox_info->tablet_connection = 1;

            sprintf(sz_line, "Tab Conn Start hndl=%d skt=%d", handle, socketIDFromWSOXHandle(handle));
            fnSystemLogHeader(sz_line, 4);     // put in log header so it is not overwritten

            // Heartbeat data is for tablet connection only
            param = cJSON_GetObjectItem(root, "HeartbeatInterval");
            if (param)
            {
                if (param->type == cJSON_Number)
                {
                    if (wsox_info)
                        wsox_info->heartbeat_interval = param->valueint;
                }
                else
                    dbgTrace(DBG_LVL_INFO, "  ConnectionStartReq(%d): HeartbeatInterval isn't a number" CRLF, (int)handle);
            }

            param = cJSON_GetObjectItem(root, "HeartbeatTimeout");
            if (param)
            {
                if (param->type == cJSON_Number)
                {
                     if (wsox_info)
                        wsox_info->heartbeat_timeout = param->valueint;
                }
                else
                    dbgTrace(DBG_LVL_INFO, "  ConnectionStartReq(%d): HeartbeatTimeout isn't a number" CRLF, (int)handle);
            }

            if ((wsox_info != NULL) && (wsox_info->heartbeat_interval != 0 || wsox_info->heartbeat_timeout != 0))
            {
                cJSON_AddNumberToObject(rspMsg, "HeartbeatInterval",         (wsox_info ? wsox_info->heartbeat_interval : 0));
                cJSON_AddNumberToObject(rspMsg, "HeartbeatTimeout",          (wsox_info ? wsox_info->heartbeat_timeout  : 0));
            }
        }
        else if (strstr(client, "pct"))
        {
            if (wsox_info)
                wsox_info->pct_connection = 1;

            sprintf(sz_line, "PCT Conn Start hndl=%d skt=%d", handle, socketIDFromWSOXHandle(handle));
            fnDumpStringToSystemLog(sz_line);
        }
        else if (strstr(client, "csd_tester"))
        {
            if (wsox_info)
                wsox_info->csdt_connection = 1;

            sprintf(sz_line, "CDT Conn Start hndl=%d skt=%d", handle, socketIDFromWSOXHandle(handle));
            fnDumpStringToSystemLog(sz_line);
        }
        else if (strstr(client, "CsdSeniorDla"))
        {
            if (wsox_info)
                wsox_info->dlas_connection = 1;

            sprintf(sz_line, "DLS Conn Start hndl=%d skt=%d", handle, socketIDFromWSOXHandle(handle));
            fnDumpStringToSystemLog(sz_line);
        }
        else
        {// Other type of client
            fnDumpStringToSystemLog("Error: Unknown client connected");
        }

        // For all callers provide API version
        cJSON_AddNumberToObject(rspMsg, "APIVersion", BASE_API_VERSION);


    }

    addEntryToTxQueue(&entry, root, "  ConnectionStartReq: Added ConnectionStartRsp to tx queue. status=%d"  CRLF);
}

/* -------------------------------------------------------------------------------------------------[getHBStoreRawIndex]-- */
// this returns the raw index, that is effectively a call count for the store, the user
// needs to mod this against the HB_STORE_MAX_COUNT value to ensure it remains within the array bounds
UINT getHBStoreRawIndex()
{
	return globalHBLogStructure.index;
}

/* -------------------------------------------------------------------------------------------------[clearHBStore]-- */
void clearHBStore()
{
	memset(&globalHBLogStructure, 0, sizeof(HBLogStructure));
	strncpy(globalHBLogStructure.magicInit, HB_MAGICINIT, 8);
}

/* -------------------------------------------------------------------------------------------------[getHBStoreRecord]-- */
HBRecord *getHBStoreRecord(UINT uIndex, char *strOut,  UINT szStrSize)
{
	HBRecord *hbr = &globalHBLogStructure.HBStore[uIndex %= HB_STORE_MAX_COUNT];
	if(szStrSize > 0)
	{
		snprintf(strOut, szStrSize, "%5d  (%2d-%03d.%03d.%03d.%03d)  %02d/%02d/%02d %02d:%02d:%02d tablet=%s  remaining=%5dmS  TranID=%5d  SeqNum=%5d  timed out at %02d/%02d/%02d %02d:%02d:%02d" ,
				uIndex,
				hbr->handle, hbr->remote.id.is_ip_addrs[0], hbr->remote.id.is_ip_addrs[1], hbr->remote.id.is_ip_addrs[2], hbr->remote.id.is_ip_addrs[3],
				(INT)hbr->wallTime.bMonth,(INT)hbr->wallTime.bDay,(INT)hbr->wallTime.bYear,
				(INT)hbr->wallTime.bHour,(INT)hbr->wallTime.bMinutes,(INT)hbr->wallTime.bSeconds,
				hbr->tablet == 1 ? "  CONNECTED " : "DISCONNECTED",
				hbr->timeLeft,
				hbr->tranID,
				hbr->seqNum,
				(INT)hbr->timeoutTime.bMonth,(INT)hbr->timeoutTime.bDay,(INT)hbr->timeoutTime.bYear,
				(INT)hbr->timeoutTime.bHour,(INT)hbr->timeoutTime.bMinutes,(INT)hbr->timeoutTime.bSeconds
				);
	}

	return hbr;
}

/* -------------------------------------------------------------------------------------------------[addToHBStore]-- */
extern HBLogStructure	globalHBLogStructure;

void addToHBStore(cJSON *root, UINT32 utRemain, wsox_handle_entry_t *wsox_info)
{
	if(!root || !wsox_info) return;
	HBRecord *hbr = &globalHBLogStructure.HBStore[globalHBLogStructure.index % HB_STORE_MAX_COUNT];
	memset(hbr, 0, sizeof(HBRecord));
// unable to use this, it is added by queue handler...
//	strncpy(hbr->baseSeqNum, safeCJSONGetString(root, "BaseSeqNum"),HB_STORE_BASESEQNUMSIZE);
	hbr->seqNum = (UINT32)atoi(safeCJSONGetString(root, "SeqNum"));
	hbr->tranID = (UINT32)atoi(safeCJSONGetString(root, "TranID"));
	hbr->timeLeft = utRemain;
	GetSysDateTime(&(hbr->wallTime), ADDOFFSETS, GREGORIAN);
	hbr->tablet = wsox_info->tablet_connection;
	hbr->handle = wsox_info->handle;
	hbr->remote = wsox_info->faddr;
	// note - do not increment the index, this is done when there is a timeout so we only record timeouts.
}


/* --------------------------------------------------------------------------------------------[heartbeat_timedout]-- */
// This is called from a HISR, so there should be no dbgTrace calls here (use syslog writes only)
void heartbeat_timedout(UNSIGNED handle)
{
    STATUS	status;
    // Keep error strings short in this function!
	char    sDbgBuf[SHORT_DBG_BUF_LEN];

	snprintf(sDbgBuf, SHORT_DBG_BUF_LEN, "Error! HB TO(%d). Net Buffers %d of %d", (int)handle, MEM_Buffers_Used, MAX_BUFFERS);
    fnDumpStringToSystemLog(sDbgBuf);

    wsox_handle_entry_t *wsox_info = wsox_get_handle_entry_by_handle((UINT32)handle);

    // RD HB Stats, bucket 0 is for timeout/disconnect
    globalHBRemaining[0]++;

    if (wsox_info)
    {
        if (wsox_info->heartbeat_tmr)
        {
        	status = NU_Delete_Timer(wsox_info->heartbeat_tmr);
            if (status != NU_SUCCESS)
            {
            	snprintf(sDbgBuf, SHORT_DBG_BUF_LEN, "Error! HB timer delete fail (%d), stat=%d", (int)handle, status);
                fnDumpStringToSystemLog(sDbgBuf);
            }
            free(wsox_info->heartbeat_tmr);
            wsox_info->heartbeat_tmr = NU_NULL;
        }

        if (wsox_info->tablet_connection && (fNoTabletConnection == FALSE) )
        {
            uint8_t pMsgData[2];

            snprintf(sDbgBuf, SHORT_DBG_BUF_LEN, "HB TO(%d): set tablet conn disabling cond", (int)handle);
            fnDumpStringToSystemLog(sDbgBuf);
            fNoTabletConnection = TRUE;
            fnPostDisabledStatus(DCOND_NO_TABLET_CONNECTION);

            //Send mode change msg to OIT task to make sure we leave print ready
            pMsgData[0] = OIT_LEFT_PRINT_RDY;
            pMsgData[1] = 0;
            (void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);

            // and also add a message to the wsox_tx queue to drop the connection that timed-out (
            WSOX_Queue_Entry entry  =  { WSOX_QET_CLOSE_HANDLE_HEARTBEAT_TIMEDOUT, (UINT32)handle};

            STATUS status = NU_Send_To_Queue(&Queue_wsox_tx, &entry, sizeof(entry)/sizeof(UNSIGNED), NU_NO_SUSPEND);
            if (status != NU_SUCCESS)
            {
            	snprintf(sDbgBuf, SHORT_DBG_BUF_LEN, "HB TO(%d): Error dropping TO conn: %d", (int)handle, status);
                fnDumpStringToSystemLog(sDbgBuf);
            }
            
            // ClearSettings();   not clearing settings on JH advice
        }
    }

    // increment our HB log index - do no cap it we do then when using the value this way we get the full count of
    // the number of times the process was called. Log the time of the timeout trip.
	GetSysDateTime(&(globalHBLogStructure.HBStore[globalHBLogStructure.index%HB_STORE_MAX_COUNT].timeoutTime), ADDOFFSETS, GREGORIAN);
    globalHBLogStructure.index++;

}

/* -----------------------------------------------------------------------------------------------[on_GetHeartbeatLogReq]-- */
void on_GetHeartbeatLogReq(UINT32 handle, cJSON *root)
{

    cJSON *rspMsg = cJSON_CreateObject();

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg,  "MsgID", "GetHeartbeatLogRsp"   );

	UINT uIndex = getHBStoreRawIndex();
	char strOut[150];
	if(uIndex > 0)
	{
	    cJSON *arrayMsg = cJSON_CreateArray();
		snprintf(strOut, 150, "%d total heartbeat timeouts, %d displayed", uIndex, HB_STORE_MAX_COUNT);
	    cJSON_AddStringToObject(rspMsg,  "Status", strOut);
		if(uIndex < HB_STORE_MAX_COUNT)
		{
			for(--uIndex; uIndex != (UINT)-1 ; uIndex--)
			{
				getHBStoreRecord(uIndex, strOut,  150);
			    cJSON_AddItemToArray(arrayMsg, cJSON_CreateString(strOut));
			}
		}
		else
		{
			// if index is larger than the array size then we have wrapped around so we need to print the entire array
			// backwards...hmmmm
			UINT uIdx = (uIndex % HB_STORE_MAX_COUNT) + HB_STORE_MAX_COUNT;
			for(--uIdx ; uIdx >= (uIndex % HB_STORE_MAX_COUNT) ; uIdx--)
			{
				// no worries the get function applies a modulo....
				getHBStoreRecord(uIdx, strOut,  150);
			    cJSON_AddItemToArray(arrayMsg, cJSON_CreateString(strOut));
			}
		}
		// add array to response
		cJSON_AddItemToObject(rspMsg, "Timeouts", arrayMsg);

	}
	else
	{
	    cJSON_AddStringToObject(rspMsg,  "Status", "No entries in heartbeat timeout log"   );
	}

    cJSON_AddStringToObject(rspMsg, "LastHBSeqnumAttempted",   last_hb_rsp_seqnum_attempted_to_send);
    cJSON_AddStringToObject(rspMsg, "LastHBSeqnumSentSuccess", last_hb_rsp_seqnum_sent_success);
    cJSON_AddStringToObject(rspMsg, "LastHBSeqnumSentFailed",  last_hb_rsp_seqnum_sent_failed);

    addEntryToTxQueue(&entry, root, " Heartbeat"  CRLF);
}

/* -----------------------------------------------------------------------------------------------[on_ClearHeartbeatLogReq]-- */
void on_ClearHeartbeatLogReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    clearHBStore();

    cJSON_AddStringToObject(rspMsg,  "MsgID", "ClearHeartbeatLogRsp"   );
    addEntryToTxQueue(&entry, root, " Heartbeat"  CRLF);
}

/* -----------------------------------------------------------------------------------------------[on_HeartbeatReq]-- */
// This is called from WSOX Master Task callback, so there should be no dbgTrace calls here (use syslog writes only)
void on_HeartbeatReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();
    UINT msRemaining = 0;
    STATUS status;
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    wsox_handle_entry_t *wsox_info = wsox_get_handle_entry_by_handle(handle);

    // RD HB Stats total counter
    globalTotalHB++;

    cJSON_AddStringToObject(rspMsg,  "MsgID", "HeartbeatRsp"   );

    if (wsox_info && wsox_info->heartbeat_timeout != 0)
    {
    	// timeout from this point on is the timeout period plus the heartbeat interval (when tablet will send next HB)
        uint32_t timeout_tps = (wsox_info->heartbeat_timeout * NU_TICKS_PER_SECOND) / 1000
        		+ (wsox_info->heartbeat_interval * NU_TICKS_PER_SECOND) / 1000;   /* mSec converted to ticks per second */

        if (wsox_info->tablet_connection && fNoTabletConnection)
        {
            fNoTabletConnection = FALSE;
            fnClearDisabledStatus(DCOND_NO_TABLET_CONNECTION);
            snprintf(DbgBuf, DBG_BUF_LEN, "  on_HeartbeatReq(%d): received heartbeat from tablet --> clearing disabling condition", (int)handle);
            fnDumpStringToSystemLog(DbgBuf);
        }
        if (wsox_info->heartbeat_tmr == NU_NULL)
        {
        	/* get the last handle entry with active heartbeat timer if existing , and then disconnect the channel */
			wsox_handle_entry_t *wsox_info_temp = wsox_get_heartBeatTimer_handle();
			if(wsox_info_temp && wsox_info->tablet_connection && wsox_info_temp->handle != handle)
			{
				wsox_info_temp->tablet_connection = 0;
			}
			
			/* must be first time --> create timer */
            wsox_info->heartbeat_tmr = malloc(sizeof(NU_TIMER));
            if (wsox_info->heartbeat_tmr)
            {
                char tmr_name[8];
                sprintf(tmr_name, "HBT%4.4x", handle);

                status = NU_Create_Timer(wsox_info->heartbeat_tmr, tmr_name, heartbeat_timedout, handle, timeout_tps, 0, NU_ENABLE_TIMER);
                if (status != NU_SUCCESS)
                {
                	snprintf(DbgBuf, DBG_BUF_LEN, "Error! failed to create heartbeat timeout timer for handle %d. status=%d", (int)handle, status);
                    fnDumpStringToSystemLog(DbgBuf);
                    free(wsox_info->heartbeat_tmr);
                    wsox_info->heartbeat_tmr = NU_NULL;
                }
                else if (trace_wsox_ping_pong_enabled)
                {
                	snprintf(DbgBuf, DBG_BUF_LEN, "on_HeartbeatReq(%d): Created timer!", (int)handle);
                    fnDumpStringToSystemLog(DbgBuf);
                }
            }
            else
            {
                if (trace_wsox_ping_pong_enabled)
                {
                	snprintf(DbgBuf, DBG_BUF_LEN, "on_HeartbeatReq(%d): Failed to malloc memory for timer!", (int)handle);
                    fnDumpStringToSystemLog(DbgBuf);
                }
            }
        }
        else
        {
        	UNSIGNED time_left = 0;
            status = NU_Get_Remaining_Time(wsox_info->heartbeat_tmr, &time_left);
            if (status != NU_SUCCESS)
            {
            	snprintf(DbgBuf, DBG_BUF_LEN, "Error! failed to get remaining time on heartbeat timeout timer for handle %d. status=%d", (int)handle, status);
                fnDumpStringToSystemLog(DbgBuf);
            }
            /* stop existing timer */
            status = NU_Control_Timer(wsox_info->heartbeat_tmr, NU_DISABLE_TIMER);
            if (status != NU_SUCCESS)
            {
            	snprintf(DbgBuf, DBG_BUF_LEN, "Error! failed to stop heartbeat timeout timer for handle %d. status=%d", (int)handle, status);
                fnDumpStringToSystemLog(DbgBuf);
            }

            msRemaining = (time_left * 1000)/NU_TICKS_PER_SECOND;
            if (verbosity > 3)
            	fnDumpStringToSystemLogWithNum("Got HBReq: Stop Tmr ms left:", msRemaining);

            // HB Stats - count the number of times a time remaining bucket is hit in half second resolution.
            // note that count 0 is a disconnect count special case so resolutions start offset 1
            UINT halfSecRemaining = msRemaining/500;
            globalHBRemaining[MIN(HBREMAINING_MAX_COUNT, (halfSecRemaining+1))]++;  // reserve position 0 for disconnects....

            status = NU_Reset_Timer(wsox_info->heartbeat_tmr, heartbeat_timedout, timeout_tps, 0, NU_ENABLE_TIMER);

            if (status != NU_SUCCESS)
            {
            	snprintf(DbgBuf, DBG_BUF_LEN, "Error! failed to reset heartbeat timeout timer for handle %d. status=%d", (int)handle, status);
                fnDumpStringToSystemLog(DbgBuf);
                status = NU_Delete_Timer(wsox_info->heartbeat_tmr);
                if (status != NU_SUCCESS)
                {
                	snprintf(DbgBuf, DBG_BUF_LEN, "Error! failed to delete heartbeat timeout timer for handle %d. status=%d", (int)handle, status);
                    fnDumpStringToSystemLog(DbgBuf);
                }
                free(wsox_info->heartbeat_tmr);
                wsox_info->heartbeat_tmr = NU_NULL;
            }
            else if (trace_wsox_ping_pong_enabled)
            {
            	snprintf(DbgBuf, DBG_BUF_LEN, "on_HeartbeatReq(%d): restarted timer! time left was %d mSec", (int)handle, msRemaining);
                fnDumpStringToSystemLog(DbgBuf);
            }
            if (verbosity > 3)
            	fnDumpStringToSystemLogWithNum("HBReq; Reset Tmr, TOut ms: ", wsox_info->heartbeat_timeout);
        }
    }

    addToHBStore(root, msRemaining, wsox_info);
    entry.hbMsg = TRUE;
    addEntryToTxQueueFast(&entry, root);
    if (verbosity > 3)
    	fnDumpStringToSystemLog("HBTrace HeartbeatReq sent");

}


/* -------------------------------------------------------------------------------------------------[opcode_to_str]-- */
char *opcode_to_str(int opcode)
{
    switch(opcode)
    {
    case WSOX_CONT_FRAME:   return "CONT_FRAME";
    case WSOX_TEXT_FRAME:   return "TEXT_FRAME";
    case WSOX_BINARY_FRAME: return "BINARY_FRAME";
    case WSOX_CLOSE_FRAME:  return "CLOSE_FRAME";
    case WSOX_PING_FRAME:   return "PING_FRAME";
    case WSOX_PONG_FRAME:   return "PONG_FRAME";
    default:                return "UNKNOWN_FRAME";
    }
}

/* --------------------------------------------------------------------------------------------------[sendErrorRsp]-- */
void sendErrorRsp(UINT32 handle, cJSON *reqMsg, char *errorMsg)
{
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "ErrorRsp");
    cJSON_AddStringToObject(rspMsg, "ErrorMsg", errorMsg);
    cJSON_AddItemToObject(rspMsg, "ReqMsg", cJSON_Duplicate(reqMsg, 1 /* recurse==yes */));

    addEntryToTxQueueFast(&entry, reqMsg);
    if (verbosity > 3)
    	fnDumpStringToSystemLog("sendErrorRsp: Added ErrorRsp to tx queue");
}

/* -------------------------------------------------------------------------------------------[sendGenericErrorRsp]-- */
void sendGenericErrorRsp(UINT32 handle, char *reqMsg, char *errorMsg)
{
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "ErrorRsp");
    cJSON_AddStringToObject(rspMsg, "ErrorMsg", errorMsg);
    cJSON_AddStringToObject(rspMsg, "ReqMsgStr", reqMsg);

    // reqMsg is not JSON so send null object
    addEntryToTxQueueFast(&entry, NULL);
    if (verbosity > 3)
    	fnDumpStringToSystemLog("sendGenericErrorRsp: Added ErrorRsp to tx queue");
}

/* ------------------------------------------------------------------------------------------------------[AddError]-- */
void AddError(cJSON *rspMsg, const char *reqMsgName, const char *errStr)
{
    cJSON_AddStringToObject(rspMsg, "ErrorStr", errStr);
    if(verbosity > 0)
        dbgTrace(DBG_LVL_INFO, "  %s: Error=%s", reqMsgName, errStr);
}

/* --------------------------------------------------------------------------------------[RequiredFieldsArePresent]-- */
bool RequiredFieldsArePresent(cJSON *obj, cJSON *rspMsg, required_fields_tbl_t *field_tbl)
{
    int i = 0;
    const char *fieldname;
    char  errStr[100];

    while( (fieldname = field_tbl->fieldnames[i]) != NULL)
    {
        if (cJSON_HasObjectItem(obj, fieldname))
            i++;
        else
        {
            sprintf(errStr, "Missing %s in %s", fieldname, field_tbl->reqMsgName);
            if (rspMsg != NULL)
                AddError(rspMsg, field_tbl->reqMsgName, errStr);
            return false;
        }
    }
    return true;
}

/* -----------------------------------------------------------------------------[get_local_addr_given_foreign_addr]-- */
static bool get_local_addr_given_foreign_addr(struct addr_struct *foreign_addr, struct addr_struct *local_addr)
{
	bool                retval = false;
    int iPass;
    STATUS                      status;

    for(iPass = 0 ; (iPass < NSOCKETS) ; iPass++)
    {
    	//TODO - With new Nucleus release, confirm access of internal structures is still valid and check whether it can be replaced with API call
        status = SCK_Protect_Socket_Block(iPass);
        if (status != NU_SUCCESS)
            continue; // go on to next socket

        if(   SCK_Sockets[iPass] != NULL 
           && SCK_Sockets[iPass]->s_state != 0 
           && SCK_Sockets[iPass]->s_family == foreign_addr->family )
        {
#if (INCLUDE_IPV6 == 1)
            if (SCK_Sockets[iPass]->s_family == NU_FAMILY_IP6)
            {
                if (memcmp(&foreign_addr->id, &SCK_Sockets[iPass]->s_foreign_addr.ip_num, IP6_ADDR_LEN) == 0)
                {
                    /* found matching foreign address --> copy local address */
                    local_addr->family = SCK_Sockets[iPass]->s_local_addr.family;
                    local_addr->port   = SCK_Sockets[iPass]->s_local_addr.port_num;
                    local_addr->id     = SCK_Sockets[iPass]->s_local_addr.ip_num;

                    retval = true;
                    break;
                }
            }
            else /* ipv4 */
#endif /* INCLUDE_IPV6 */
            {
                if (memcmp(&foreign_addr->id, &SCK_Sockets[iPass]->s_foreign_addr.ip_num, sizeof(UINT32)) == 0)
                {
                    /* found matching foreign address --> copy local address */
                    local_addr->family = SCK_Sockets[iPass]->s_local_addr.family;
                    local_addr->port   = SCK_Sockets[iPass]->s_local_addr.port_num;
                    local_addr->id     = SCK_Sockets[iPass]->s_local_addr.ip_num;

                    SCK_Release_Socket();
                    retval = true;
                    break;
                }
            }
        }

        SCK_Release_Socket();
    }

    return retval;
}

/* -------------------------------------------------------------------------------------[is_net0_or_net1_id_struct]-- */
bool is_net0_or_net1_id_struct(struct id_struct *id)
{
    return id != NULL
        && id->is_ip_addrs[0] == 192
        && id->is_ip_addrs[1] == 168
        && id->is_ip_addrs[2] ==  10
        && (id->is_ip_addrs[3] == 246 || id->is_ip_addrs[3] == 110 );
}

/* --------------------------------------------------------------------------------------[is_net0_or_net1_sockaddr]-- */
bool is_net0_or_net1_sockaddr(struct sockaddr_struct *addr)
{
    return addr != NULL && is_net0_or_net1_id_struct(&addr->ip_num);
}

/* ------------------------------------------------------------------------------------------[is_net0_or_net1_addr]-- */
bool is_net0_or_net1_addr(struct addr_struct *addr)
{
    return addr != NULL && is_net0_or_net1_id_struct(&addr->id);
}

/* ---------------------------------------------------------------------------------------------------[wsox_onopen]-- */
// This is called from WSOX Master Task callback, so there should be no dbgTrace calls here (use syslog writes only)
VOID wsox_onopen (UINT32 handle, struct addr_struct *foreign_addr)
{
    STATUS           status;
    struct addr_struct local_addr;

    /* we need to find the local_addr. We need that to generate a url
     * for the GetSystemLogRsp      */
    if( false == get_local_addr_given_foreign_addr(foreign_addr, &local_addr))
    {
        /* fill in the default usb-C address */
        memset(&local_addr, 0, sizeof(local_addr));
        local_addr.family = NU_FAMILY_IP;
        local_addr.id.is_ip_addrs[0] = 192;
        local_addr.id.is_ip_addrs[1] = 168;
        local_addr.id.is_ip_addrs[2] = 10;
        local_addr.id.is_ip_addrs[3] = 246;
    }
    else
    {
        if( (CMOSglobalNetworkInformationStructure.eth0WsoxFlag == 0) && !is_net0_or_net1_addr(&local_addr))
        {
            /* must be eth0 which isn't allowed! */
            WSOX_Queue_Entry entry  =  { WSOX_QET_CLOSE_HANDLE_ETH0_NOT_ALLOWED, handle};

            status = NU_Send_To_Queue(&Queue_wsox_tx, &entry, sizeof(entry)/sizeof(UNSIGNED), NU_NO_SUSPEND);
            if (status != NU_SUCCESS)
            {
            	snprintf(DbgBuf, DBG_BUF_LEN, "wsox_onopen(%d): Error adding WSOX_QET_CLOSE_HANDLE_ETH0_NOT_ALLOWED: %d", (int)handle, status);
                fnDumpStringToSystemLog(DbgBuf);
            }
            
            if (verbosity > 0)
            	{
            	snprintf(DbgBuf, DBG_BUF_LEN, "wsox_onopen() handle=%d socket=%d, foreign_addr=%d.%d.%d.%d:%d  not allowed",
                        (int)handle,
    					socketIDFromWSOXHandle(handle),
                        foreign_addr->id.is_ip_addrs[0],
                        foreign_addr->id.is_ip_addrs[1],
                        foreign_addr->id.is_ip_addrs[2],
                        foreign_addr->id.is_ip_addrs[3],
                        foreign_addr->port);
                fnDumpStringToSystemLog(DbgBuf);
            	}
            return;
        }
    }

    wsox_add_handle(handle, foreign_addr, &local_addr);

    if (verbosity > 0)
    	{
    	snprintf(DbgBuf, DBG_BUF_LEN, "wsox_onopen() handle=%d socket=%d, foreign_addr=%d.%d.%d.%d:%d",
                (int)handle,
				socketIDFromWSOXHandle(handle),
                foreign_addr->id.is_ip_addrs[0],
                foreign_addr->id.is_ip_addrs[1],
                foreign_addr->id.is_ip_addrs[2],
                foreign_addr->id.is_ip_addrs[3],
                foreign_addr->port);
        fnDumpStringToSystemLog(DbgBuf);
    	}
}


typedef struct { const char *MsgId;   void (*event_fn)(UINT32 handle, cJSON *root);} item_t;

// NOTE: This list should match the "Horizon Tablet-Base API" Spec.  
//       If you add or modify a message, update the spec.

item_t msg_table[] =
    {
		//Funds
        {               "GetFundsReq",   on_GetFundsReq              }
       ,{     	  	  "GetPSDInfoReq", 	 on_GetPSDInfoReq    	 	 }
       ,{     	 "GetCustomerInfoReq", 	 on_GetCustomerInfoReq	 	 }
	   ,{            "GetPSDStateReq",   on_GetPSDStateReq           }
	   ,{              "GetPSDLogReq",   on_GetPSDLogReq             }

        //Status
       ,{          "GetBaseStatusReq",   on_GetBaseStatusReq         }
       ,{        "GetBaseVersionsReq",   on_GetBaseVersionsReq       }
       ,{            "GetDateTimeReq",   on_GetDateTimeReq           }
       ,{        "GetGraphicsListReq",   on_GetGraphicsListReq       }
       ,{             "GetEMDDataReq",   on_GetEMDDataReq            }
       ,{            "GetEMDGroupReq",   on_GetEMDGroupReq           }
       ,{     	  "GetPrinterInfoReq", 	 on_GetPrinterInfoReq    	 }
       ,{     	   "DeleteGraphicReq", 	 on_DeleteGraphicReq    	 }


		//Weight
       ,{          "TabToWPlatMsgReq",   on_SetTabToWPlatMsg      	 }

		//Settings
       ,{         "GetJobSettingsReq",   on_GetJobSettingsReq      	 }
       ,{       "ClearJobSettingsReq",   on_ClearJobSettingsReq      }
	   ,{       "SetBaseParameterReq",   on_SetBaseParameterReq      }
       ,{       "GetBaseParameterReq",   on_GetBaseParameterReq    	 }
       ,{       "SetProxyReq",   		 on_SetProxyReq 		     }
  	   ,{       "GetProxyReq",   		 on_GetProxyReq 		     }
#ifdef TEST_SETTINGS_MGR
       ,{        "TestSettingsMgrReq",   on_TestSettingsMgrReq    	 }
#endif /* TEST_SETTINGS_MGR */

        //Machine control API
       ,{              "GoToReadyReq",   on_GoToReadyReq	         }
       ,{           "GoToNotReadyReq",   on_GoToNotReadyReq	         }
       ,{                  "StartReq",   on_StartReq	             }
       ,{                   "StopReq",   on_StopReq	             	 }
       ,{          "SetThroughputReq",   on_SetThroughputReq	     }
       ,{          "GetThroughputReq",   on_GetThroughputReq	     }
       ,{              "GoToMFGReq",   	 on_GoToMFGReq	         	 }


		//Maintenance
       ,{       "StartReplacementReq",   on_StartReplacementReq      }
       ,{    "CompleteReplacementReq",   on_CompleteReplacementReq   }
       ,{     "PrinterMaintenanceReq",   on_PrinterMaintenanceReq    }
    ,{     "GetOutOfBoxPrintStateReq",   on_GetOutOfBoxPrintStateReq         }
    ,{"CompleteOutOfBoxPrintSetupReq",   on_CompleteOutOfBoxPrintSetupReq    }

       ,{  "SetNetworkInformationReq",   on_SetNetworkInformationReq }


		//Diagnostic
       ,{           "SaveBaseLogsReq",   on_SaveBaseLogsReq          }
       ,{         "GetBaseLogURLsReq",   on_GetBaseLogURLsReq        }
       ,{         "GetErrorLogUrlReq",   on_GetErrorLogUrlReq        }
       ,{           "GetNetLogUrlReq",   on_GetNetLogUrlReq          }
       ,{           "GetSystemLogReq",   on_GetSystemLogReq          }
       ,{   "GetSocketInformationReq",   on_GetSocketInformationReq  }
       ,{        "PrintDiagnosticReq",   on_PrintDiagnosticReq	     }
       ,{           "GetTaskStatsReq",   on_GetTaskStatsReq	     	 }
       ,{           "GetHISRStatsReq",   on_GetHISRStatsReq	     	 }
       ,{     "GetMemoryPoolStatsReq",   on_GetMemoryPoolStatsReq    }
       ,{  "GetNetworkInformationReq",   on_GetNetworkInformationReq }
       ,{         "MailSimulationReq",   on_MailSimulationReq	     }
       ,{          "DiagGetMemoryReq",   on_DiagGetMemoryReq         }
       ,{          "DiagSetMemoryReq",   on_DiagSetMemoryReq         }
       ,{        "DiagSetTestDataReq",   on_DiagSetTestDataReq       }
       ,{        "DiagGetTestDataReq",   on_DiagGetTestDataReq       }
       ,{          "GetSensorInfoReq",   on_GetSensorInfoReq         }
       ,{            "SetBootModeReq",   on_SetBootModeReq           }
       ,{            "GetBootModeReq",   on_GetBootModeReq           }
       ,{                 "RebootReq",   on_RebootReq                }
       ,{         "GetStorageInfoReq",   on_GetStorageInfoReq        }
       ,{            "StorageDiagReq",   on_StorageDiagReq           }
       ,{           "WipeCMOSDiagReq",   on_DiagWipeCMOSReq          } //TODO to remove
       ,{           "DiagWipeCMOSReq",   on_DiagWipeCMOSReq          }
       ,{  "DiagGetShadowDebitLogReq",   on_DiagGetShadowDebitLogReq }
       ,{ "DiagGetShadowRefillLogReq",   on_DiagGetShadowRefillLogReq}
       ,{           "ConnectivityReq",   on_ConnectivityReq          }
       ,{     "DiagClearWasteTankReq",   on_DiagClearWasteTankReq 	 }
       ,{       "DiagGetWasteTankReq",   on_DiagGetWasteTankReq 	 }
       ,{      "DebugTerminalDumpReq",   on_DebugTerminalDumpReq     }
       ,{     "DiagInitFileSystemReq",   on_DiagInitFileSystemReq    }
       ,{   "RespawnDebugTerminalReq",   on_RespawnDebugTerminalReq  }
        ,{       "GetDHCPActivityReq",   on_GetDHCPActivityReq  }

		// Intfc Lib Functions
       ,{    "GetTabletUSBDevicesRsp",   on_GetTabletUSBDevicesRsp   }
       ,{  "GetTabletUsbWriteTestRsp",   on_GetTabletUsbWriteTestRsp }
       ,{      "GetTabletVersionsRsp",   on_GetTabletVersionsRsp     }
       ,{        "GetTabletStatusRsp",   on_GetTabletStatusRsp       }

        // Power down/sleep handling
       ,{              "SleepModeReq",   on_SleepModeReq	     	 }
       ,{                   "WakeReq",   on_WakeReq	     	         }
       ,{               "PowerOffReq",   on_PowerOffReq	     	         }

		// Connection management
       ,{        "ConnectionStartReq",   on_ConnectionStartReq       }
       ,{           "SetVerbosityReq",   on_SetVerbosityReq          }
       ,{              "HeartbeatReq",   on_HeartbeatReq             }
       ,{        "GetHeartbeatLogReq",   on_GetHeartbeatLogReq       }
       ,{      "ClearHeartbeatLogReq",   on_ClearHeartbeatLogReq     }

		//Temp (Not in API Spec)  
		//TODO - remove temporary API functions
       ,{          		  	 "TRMReq",   on_TRMReq         		 	 }
       ,{      "SetDcapParametersReq",   on_SetDcapParametersReq	 }
       ,{  "SetDcapRequiredFieldsReq",   on_SetDcapRequiredFieldsReq }

#ifdef MEMORYTEST
       ,{             "MemoryTestReq",   on_RunMemoryTestReq	     }
#endif /* MEMORYTEST */
       ,{           "SetBaseStateReq",   on_SetBaseStateReq 	     }
       ,{              "TestPrintReq",   on_TestPrintReq	         }
       ,{          "SetRatingInfoReq",   on_SetRatingInfoReq         }
       ,{                  "PrintReq",   on_PrintReq	             } //TODO JAH Remove me when real debit command is available
       ,{          "GetNetworkDLAReq",   on_GetNetworkDLAReq         }
       ,{           "PerformAuditReq",   on_PerformAuditReq          }
       ,{          "PerformRefillReq",   on_PerformRefillReq         }
       ,{   "GetNetworkInterfacesReq",   on_GetNetworkInterfacesReq  }
       ,{       "SetNetworkConfigReq",   on_SetNetworkConfigReq      }
       ,{        "SetDefaultRouteReq", 	 on_SetDefaultRouteReq       }
       ,{      "PerformWithdrawalReq",   on_PerformWithdrawalReq     }
       ,{              "ShowPbpIoReq",   on_ShowPbpIoReq             }
       ,{                 "GetCCDReq",   on_GetCCDReq           	 }
       ,{         "InstallUpdatesReq",   on_InstallUpdatesReq      	 }

       ,{           "UploadTrxPkgReq",   on_UploadTrxPkgReq      	 }

        // PSD Messages
       ,{        "PSDBLGetMicroStatus", on_PSDBLGetMicroInfo         }
       ,{ "PSDBLExitAppAndMasterErase", on_PSDBLExitAppAndMasterErase}
       ,{     "PSDBLDisableBootLoader", on_PSDBLDisableBootLoader    }
       ,{      "PSDBLCalcCRCProgSpace", on_PSDBLCalcCRCProgSpace     }
       ,{         "PSDBLLoadAndVerify", on_PSDBLLoadAndVerify        }
       ,{          "PSDBLEnterAppMode", on_PSDBLEnterAppMode         }
       ,{       "PSDRunGeminiSelfTest", on_PSDRunGeminiSelfTest      }
       ,{        "PSDLoadTransportKey", on_PSDLoadTransportKey       }
       ,{       "PSDGetChallengeNonce", on_PSDGetChallengeNonce      }
       ,{         "PSDTransportUnlock", on_PSDTransportUnlock        }
       ,{         "PSDLoadProviderKey", on_PSDLoadProviderKey        }
       ,{         "PSDCalcClockOffset", on_PSDCalcClockOffset        }
       ,{    "PSDInitializeParameters", on_PSDInitializeParameters   }
       ,{            "PSDGenerateKeys", on_PSDGenerateKeys           }
       ,{             "PSDMasterErase", on_PSDMasterErase            }
       ,{        "PSDGetParameterList", on_PSDGetParameterList       }
       ,{        "PSDLoadEncryptedKey", on_PSDLoadEncryptedKeys      }

       ,{   		"GetTabletInfoReq", on_GetTabletInfoReq 		 }
       ,{     "GetFullTabletStatusRsp", on_GetFullTabletStatusRsp    }

       ,{         "InstallGraphicReq",   on_InstallGraphicReq      	 }  //TODO: JAH Remove me

   };

const int num_msg_table_entries = sizeof(msg_table) / sizeof(msg_table[0]);

/* ------------------------------------------------------------------------------------------------[wsox_onmessage]-- */
// This is called from WSOX Master Task callback, so there should be no dbgTrace calls here (use syslog writes only)
VOID wsox_onmessage (UINT32 handle, WSOX_MSG_INFO *msg_info)
{
    CHAR    *buffer = NULL;
    STATUS   status;

/*
    if (verbosity > 1)
        "wsox_onmessage() handle=%d data_len=%lu opcode=%d(%s) flags=0x%x" CRLF,
                (int)handle,
                (unsigned long)msg_info->data_len,
                msg_info->opcode,
                opcode_to_str(msg_info->opcode),
                msg_info->flags);
*/

    /* Allocate a buffer for the data. */
    status = NU_Allocate_Memory(&cJSON_mem_pool, (VOID**)&buffer, msg_info->data_len+1, NU_NO_SUSPEND);
    if (status == NU_SUCCESS)
    {
        status = NU_WSOX_Recv(handle, buffer, &msg_info->data_len);
        if (status == NU_SUCCESS)
        {
            buffer[msg_info->data_len] = 0;
            if (msg_info->opcode == WSOX_TEXT_FRAME)
            {
                 if (verbosity > 3)
                     show_cJSON_mempool_details("wsox_rx_beg");

                 cJSON *root = cJSON_Parse(buffer);
                 if (!root)
                 {
    				 CHAR *errorMsg = "Message isn't a valid JSON object";
                     if (verbosity > 2 )
                     {
                    	 snprintf(DbgBuf, DBG_BUF_LEN, "WRX(%d): %s", (int)handle, buffer);
                        fnDumpStringToSystemLog(DbgBuf);
                     }

                     snprintf(DbgBuf, DBG_BUF_LEN, "Error on handle %d: %s", (int)handle, errorMsg);
    	             fnDumpStringToSystemLog(DbgBuf);
    				 sendGenericErrorRsp(handle, buffer, errorMsg);
                 }
                 else
                 {
    				 cJSON *MsgId = cJSON_GetObjectItem(root, "MsgID");
    				 if (!MsgId)
    				 {
    					 CHAR *errorMsg = "Message doesn't contain a MsgID field";
    	                 if (verbosity > 2 )
    	                 {
    	                	 snprintf(DbgBuf, DBG_BUF_LEN, "WRX(%d): %s", (int)handle, buffer);
        	                 fnDumpStringToSystemLog(DbgBuf);
    	                 }

    	                 snprintf(DbgBuf, DBG_BUF_LEN, "Error on handle %d: %s", (int)handle, errorMsg);
        	             fnDumpStringToSystemLog(DbgBuf);
    					 sendErrorRsp(handle, root, errorMsg);
    				 }
    				 else
    				 {
    					 BOOLEAN found = NU_FALSE;
    					 INT     i;
    					 char* ptrMsgId = fnTrim(MsgId->valuestring);
						 DATETIME      rTime;

    	                 if (verbosity > 2 && is_dbgTrace_allowed_for(root))
						 {
							(void)GetSysDateTime( &rTime, ADDOFFSETS, GREGORIAN );
							snprintf(DbgBuf, DBG_BUF_LEN, "WRX(%d)[%02d:%02d:%02d]: %s", (int)handle, rTime.bHour, rTime.bMinutes, rTime.bSeconds, buffer);
    	                    fnDumpStringToSystemLog(DbgBuf);
						 }

    					 for(i=0; i<num_msg_table_entries && !found; i++ )
    					 {
    						 if (strcmp(msg_table[i].MsgId, ptrMsgId) == 0)
    						 {
    						     INTERTASK itm;
    						     BOOL msgstat;

    							 found = NU_TRUE;

    							 if (msg_table[i].event_fn == on_HeartbeatReq)
                                 {
                                     /* we can't let heartbeat message be waiting in the queue */
                                     on_HeartbeatReq(handle, root);
                                 }
                                 else
                                 {
    								 itm.IntertaskUnion.wsox_req_msg.handle   = handle;
    								 itm.IntertaskUnion.wsox_req_msg.root     = root;
    								 itm.IntertaskUnion.wsox_req_msg.event_fn = msg_table[i].event_fn;
    								 /* source id is -1 since we are called form the Nucleus websocket server task */
    								 msgstat = OSSendIntertask(WSRXSRVTASK, -1 /*srcid*/, 0 /*msgid*/, WSOX_REQ_MSG_DATA, &itm.IntertaskUnion.wsox_req_msg, sizeof(itm.IntertaskUnion.wsox_req_msg));
    								 if (msgstat == FAILURE)
    								 {
    									 snprintf(DbgBuf, DBG_BUF_LEN, "WRX(%d): Error to forward web socket message", (int)handle);
    						             fnDumpStringToSystemLog(DbgBuf);
    								 }
    								 else
    								 {
        								 root = NULL;  /* because WSRXSRVTASK is responsible for destroying the cJSON object */
    								 }
                                 }
    						 }
    					 }
    					 if (!found && verbosity > 0)
    					 {
    						 char errorMsg[60];

    						 if (strlen(MsgId->valuestring) < 40)
    							 sprintf(errorMsg, "Unknown MsgID=[%s]", MsgId->valuestring);
    						 else
    							 sprintf(errorMsg, "Unknown MsgID in ReqMsg");

    						 if (verbosity > 0)
    						 {
    							 snprintf(DbgBuf, DBG_BUF_LEN, "  %s", errorMsg);
     			                fnDumpStringToSystemLog(DbgBuf);
    						 }

    						 sendErrorRsp(handle, root, errorMsg);
    					 }
    				 }
                 }
                 cJSON_Delete(root);
                 if (verbosity > 3)
                     show_cJSON_mempool_details("wsox_rx_end");
            }
            else if (msg_info->opcode == WSOX_PING_FRAME)
            {
                /* send pong frame */
                UINT32 data_len = msg_info->data_len + 1;
                WSOX_Queue_Entry entry  =  { WSOX_QET_PONG, handle, {.pong_info = {data_len, buffer}}};

                status = NU_Send_To_Queue(&Queue_wsox_tx, &entry, sizeof(entry)/sizeof(UNSIGNED), NU_NO_SUSPEND);
            	if (status != NU_SUCCESS)
            	{
            		snprintf(DbgBuf, DBG_BUF_LEN, "WRXB(%d): Error sending pong: %d", (int)handle, status);
                    fnDumpStringToSystemLog(DbgBuf);
            	}
                buffer = NULL; /* it will be deallocated after pulled from the queue */
            }
            else if (msg_info->opcode == WSOX_PONG_FRAME)
            {
                /* check to see if this pong was from the tablet */
                wsox_handle_entry_t *wsox_info = wsox_get_handle_entry_by_handle(handle);

                if (wsox_info)
                {
                    if (wsox_info->tablet_connection && fNoTabletConnection)
                    {
                        fNoTabletConnection = FALSE;
                        fnClearDisabledStatus(DCOND_NO_TABLET_CONNECTION);
                        snprintf(DbgBuf, DBG_BUF_LEN, "  wsox_onmessage(%d): tablet started answering pings --> clearing disabling condition", (int)handle);
                        fnDumpStringToSystemLog(DbgBuf);
                    }
                }
            }
            else if (verbosity > 3)
            {
            	snprintf(DbgBuf, DBG_BUF_LEN, "  received %lu bytes", (unsigned long)msg_info->data_len);
                fnDumpStringToSystemLog(DbgBuf);
            }

            if (buffer) NU_Deallocate_Memory(buffer);
        }
        else
        {
        	snprintf(DbgBuf, DBG_BUF_LEN, "WRX(%d): Error to get web socket message: %d", (int)handle, status);
            fnDumpStringToSystemLog(DbgBuf);
        }
    }
    else
    {
    	snprintf(DbgBuf, DBG_BUF_LEN, "WRX(%d): Error to allocate memory for message", (int)handle);
        fnDumpStringToSystemLog(DbgBuf);
    }
}

/* --------------------------------------------------------------------------------------------------[wsox_onerror]-- */
// This is called from WSOX Master Task callback, so there should be no dbgTrace calls here (use syslog writes only)
VOID wsox_onerror (UINT32 handle, STATUS error, CHAR *reason)
{

	if (error == WSOX_PING_TIMED_OUT)
    {
        wsox_handle_entry_t *wsox_info = wsox_get_handle_entry_by_handle(handle);

        /* if this was the tablet connection --> set disabling condition */
        if (wsox_info)
        {
            WSOX_Queue_Entry schedule_ping_entry  =  { WSOX_QET_SCHEDULE_PING, handle};

            //until we can debug why we are getting these errors even after receiving pong frames
            //if (wsox_info->tablet_connection && (fNoTabletConnection == FALSE) )
            //{
            //    fNoTabletConnection = TRUE;
            //    fnPostDisabledStatus(DCOND_NO_TABLET_CONNECTION);
            //    "  wsox_onerror(%d): tablet didn't answer pings --> setting disabling condition" CRLF, (int)handle);
            //}

            /* and schedule the ping again, since this error canceled it */
            STATUS status = NU_Send_To_Queue(&Queue_wsox_tx, &schedule_ping_entry, sizeof(schedule_ping_entry)/sizeof(UNSIGNED), NU_NO_SUSPEND);
            if (status != NU_SUCCESS)
            {
            	snprintf(DbgBuf, DBG_BUF_LEN, "  wsox_onerror(%d): Error queuing ping schedule: %d", (int)handle, status);
                fnDumpStringToSystemLog(DbgBuf);
            }
            else if (verbosity > 0)
            {
            	snprintf(DbgBuf, DBG_BUF_LEN, "  wsox_onerror(%d): rescheduled ping", (int)handle);
                fnDumpStringToSystemLog(DbgBuf);
            }

        }
    }
    if (verbosity > 0)
    {
    	snprintf(DbgBuf, DBG_BUF_LEN, "wsox_onerror() handle=%d error=%d reason=%s",
                (int)handle,
                error,
                reason);
        fnDumpStringToSystemLog(DbgBuf);
    }
}

/* --------------------------------------------------------------------------------------------------[wsox_onclose]-- */
// This is called from WSOX Master Task callback, so there should be no dbgTrace calls here (use syslog writes only)
VOID wsox_onclose (UINT32 handle, STATUS error)
{
    STATUS   			status;
    wsox_handle_entry_t *wsox_info = wsox_get_handle_entry_by_handle(handle);

    // RD HB Stats increment the disconnect counter
    // remove for now - this should be incremented in the timeout handler
    //globalHBRemaining[0]++;

    /* if this was the tablet connection --> set disabling condition */
    if (wsox_info)
    {
        if (wsox_info->tablet_connection && (fNoTabletConnection == FALSE) )
        {
            uint8_t pMsgData[2];

            fNoTabletConnection = TRUE;
            fnPostDisabledStatus(DCOND_NO_TABLET_CONNECTION);
            snprintf(DbgBuf, DBG_BUF_LEN, "  wsox_onclose(%d) socket=%d: tablet connection closed --> setting NO_TABLET_CONNECTION disabling condition", (int)handle, socketIDFromWSOXHandle(handle));
            fnDumpStringToSystemLog(DbgBuf);


            //Send mode change msg to OIT task to make sure we leave print ready
            pMsgData[0] = OIT_LEFT_PRINT_RDY; 
            pMsgData[1] = 0;
            (void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);

            // ClearSettings();   not clearing settings on JH advice
        }

        if (wsox_info->heartbeat_tmr)
        {
        	status = NU_Control_Timer(wsox_info->heartbeat_tmr, NU_DISABLE_TIMER);
            if (status != NU_SUCCESS)
            {
            	snprintf(DbgBuf, DBG_BUF_LEN, "Error! failed to stop heartbeat timeout timer for handle %d. status=%d", (int)handle, status);
                fnDumpStringToSystemLog(DbgBuf);
            }
        	status = NU_Delete_Timer(wsox_info->heartbeat_tmr);
            if (status != NU_SUCCESS)
            {
            	snprintf(DbgBuf, DBG_BUF_LEN, "Error! failed to delete heartbeat timeout timer for handle %d. status=%d", (int)handle, status);
                fnDumpStringToSystemLog(DbgBuf);
            }

            free(wsox_info->heartbeat_tmr);
            wsox_info->heartbeat_tmr = NU_NULL;
        }
    }


    wsox_remove_handle(handle);
    if (verbosity > 1)
    {
    	snprintf(DbgBuf, DBG_BUF_LEN, "wsox_onclose() handle=%d socket=%d error=%d",
                (int)handle,
    			socketIDFromWSOXHandle(handle),
                error);
        fnDumpStringToSystemLog(DbgBuf);
    }
}

/* -----------------------------------------------------------------------------------------------[wsox_initialize]-- */
VOID wsox_initialize(NU_MEMORY_POOL *mem_pool)
{
    STATUS                 status;
    UINT32                 lstnr_handle;
    VOID                  *data_pipe;

    list_create(&wsox_handles_list, sizeof(wsox_handle_entry_t),    wsox_handles_list_mem, sizeof(wsox_handles_list_mem), NULL);

    /* Clear the NU_WSOX_CLIENT_STRUCT structure. */
    memset(&wsox_lstnr, 0, sizeof(NU_WSOX_CONTEXT_STRUCT));
    /* Set up a valid listener structure. */
    wsox_lstnr.resource = "ws_csd";
    wsox_lstnr.protocols = "chat, chatty";
    wsox_lstnr.flag = (NU_WSOX_LISTENER | NU_WSOX_SECURE);
    wsox_lstnr.onclose = wsox_onclose;
    wsox_lstnr.onerror = wsox_onerror;
    wsox_lstnr.onmessage = wsox_onmessage;
    wsox_lstnr.onopen = wsox_onopen;
    wsox_lstnr.max_connections = 10;
    wsox_lstnr.origins = "http://sample.com"; /* because if left as NULL, the open request will trigger an error by dereferencing it! */

    /* Create the listener structure. */
    dbgTrace(DBG_LVL_INFO, "Creating WSOX Context...........");
    status = NU_WSOX_Create_Context(&wsox_lstnr, &lstnr_handle);
    if (status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_ERROR, "WSOX Server Failed to Initialize: Error = %d (0x%X)" CRLF, status, status);
    }
    else
    {
        dbgTrace(DBG_LVL_INFO, "OK" CRLF);
    }

    // create response pipes
    status = NU_Allocate_Memory(mem_pool, &data_pipe, 1024, NU_NO_SUSPEND);
    if (status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_ERROR, "WSOX Error: Failed to allocate memory for response pipe wti_dev: Error: %d" CRLF, status);
    }
    else
    {
    	status = NU_Create_Pipe(&wsox_tablet_usb_devices_rsp_pipe,    "wti_dev", data_pipe, 1024, NU_VARIABLE_SIZE, 250 /*max variable msg size */, NU_FIFO);
        if (status != NU_SUCCESS)
        {
            dbgTrace(DBG_LVL_ERROR, "WSOX Error: Failed to create pipe for wti_dev: Error: %d" CRLF, status);
        }
    }
    
    status = NU_Allocate_Memory(mem_pool, &data_pipe, 1024, NU_NO_SUSPEND);
    if (status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_ERROR, "WSOX Error: Failed to allocate memory for response pipe wti_wtr: Error: %d" CRLF, status);
    }
    else
    {
    	status = NU_Create_Pipe(&wsox_tablet_usb_write_test_rsp_pipe, "wti_wtr", data_pipe, 1024, NU_FIXED_SIZE, sizeof(tablet_usb_write_test_rsp_msg_t), NU_FIFO);
        if (status != NU_SUCCESS)
        {
            dbgTrace(DBG_LVL_ERROR, "WSOX Error: Failed to create pipe for wti_wtr: Error: %d" CRLF, status);
        }
    }

    status = NU_Allocate_Memory(mem_pool, &data_pipe, 1024, NU_NO_SUSPEND);
    if (status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_ERROR, "WSOX Error: Failed to allocate memory for response pipe wti_vr: Error: %d" CRLF, status);
    }
    else
    {
    	status = NU_Create_Pipe(&wsox_tablet_versions_rsp_pipe,       "wti_vr",  data_pipe, 1024, NU_VARIABLE_SIZE, 250 /*max variable msg size */, NU_FIFO);
        if (status != NU_SUCCESS)
        {
            dbgTrace(DBG_LVL_ERROR, "WSOX Error: Failed to create pipe for wti_vr: Error: %d" CRLF, status);
        }
    }
    
    status = NU_Allocate_Memory(mem_pool, &data_pipe, 1024, NU_NO_SUSPEND);
    if (status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_ERROR, "WSOX Error: Failed to allocate memory for response pipe wti_sr: Error: %d" CRLF, status);
    }
    else
    {
    	status = NU_Create_Pipe(&wsox_tablet_status_rsp_pipe,         "wti_sr",  data_pipe, 1024, NU_VARIABLE_SIZE, 250 /*max variable msg size */, NU_FIFO);
        if (status != NU_SUCCESS)
        {
            dbgTrace(DBG_LVL_ERROR, "WSOX Error: Failed to create pipe for wti_sr: Error: %d" CRLF, status);
        }
    }
    
}

/* ---------------------------------------------------------------------------------------[is_dbgTrace_allowed_for]-- */

bool heartbeat_trace_inhibit_override = false;

typedef struct
{
    const char *msgid;
    bool *override;
} inhibit_trace_tbl_entry_t;

inhibit_trace_tbl_entry_t inhibited_dbgTrace_msgids[] = 
{
    {   "TabToWPlatMsgReq",  NULL },
    {   "WPlatToTabMsgRsp",  NULL },
    {   "HeartbeatReq",     &heartbeat_trace_inhibit_override },
    {   "HeartbeatRsp",     &heartbeat_trace_inhibit_override },
};

bool is_dbgTrace_allowed_for(cJSON *root)
{
    int i;
    bool found = NU_FALSE;
    cJSON *MsgId = cJSON_GetObjectItem(root, "MsgID");

    if (MsgId != NULL)
    {
        for(i=0; i<ARRAY_LENGTH(inhibited_dbgTrace_msgids) && !found; i++ )
        {
            if (strcmp(inhibited_dbgTrace_msgids[i].msgid, MsgId->valuestring) == 0)
            {
                if ( inhibited_dbgTrace_msgids[i].override == NULL ||
                    *inhibited_dbgTrace_msgids[i].override == false   )
                {
                    found = NU_TRUE;
                }
            }
        }
    }

    return !found;  /* if entry not found in inhibit table --> dbgTrace allowed */
}

wsox_handle_entry_t* wsox_get_handle_entry(WSOX_Queue_Entry *entry)
{
    list_t 							*p_list = &wsox_handles_list;
    wsox_handle_entry_t             *p_handle = NULL;
    UINT32                          found = 0;

	//get the handle for this entry
    if (p_list)
    {
        int go_on = TRUE;
        list_entry_t *p_entry = list_peek_first(p_list);
        while (p_entry && go_on)
        {
        	p_handle = (void *)p_entry->data;
        	if(entry->handle == p_handle->handle)
        	{
        		found = 1;
            	break;
        	}
            p_entry = p_entry->next;
        }
    }

    if(!found)
    	p_handle = NULL;

    return p_handle;
}

UINT32 wsox_get_handle_group(WSOX_Queue_Entry *entry)
{
    UINT32              group = 0;
    wsox_handle_entry_t *p_handle = wsox_get_handle_entry(entry);

    if (p_handle != NULL)
    {
		group = p_handle->group;
    }

    return group;
}

VOID wsox_set_handle_group(WSOX_Queue_Entry *entry)
{
    wsox_handle_entry_t *p_handle = wsox_get_handle_entry(entry);

    if (p_handle != NULL)
    {
    	p_handle->group = entry->groupMask;
    }
}

// Get client type for given handle
WSOX_CLIENT wsox_get_client_type(UINT32 handle)
{
	WSOX_CLIENT client = WSOX_CLIENT_NONE;

    wsox_handle_entry_t *wsox_info = wsox_get_handle_entry_by_handle(handle);
    if (wsox_info)
    {
    	if (wsox_info->tablet_connection)
    		client = WSOX_CLIENT_TABLET;
    	else if (wsox_info->pct_connection)
    		client = WSOX_CLIENT_PCT;
    	else if (wsox_info->csdt_connection)
    		client = WSOX_CLIENT_CSDTESTER;
    	else if (wsox_info->dlas_connection)
    		client = WSOX_CLIENT_DLASVC;
    	else
    		client = WSOX_CLIENT_OTHER;
    }
	return client;
}

/* -------------------------------------------------------------------------------------[wsox_get_next_seq_num_str]-- */
char *wsox_get_next_seq_num_str(uint32_t handle, char *str)
{
    uint32_t ts = GetSystemTime();
    sprintf(str, "C%u-%u-%06u.%03u", handle, wsox_get_base_seq_num(handle), ts / 1000, ts % 1000);

    return str;
}

/* -------------------------------------------------------------------------------------[wsox_tx_msg_queue_handler]-- */
VOID wsox_tx_msg_queue_handler(WSOX_Queue_Entry *entry)
{
    CHAR  *data;
    UINT64 data_len, tx_len;
    STATUS status;
    char base_seq_num[35];
	DATETIME      rTime;

    if (entry == NULL) return;

    switch(entry->entry_type)
    {
        case WSOX_QET_CJSON :
            {
                //dbgTrace(DBG_LVL_INFO, "WSOX_QET_CJSON Starting");
                struct cjson_info *pi = (VOID *)&entry->i;

                if (verbosity > 3)
                    show_cJSON_mempool_details("wsox_tx_beg");

                cJSON_AddStringToObject(pi->msg, "BaseSeqNum", wsox_get_next_seq_num_str(entry->handle, base_seq_num));

                data = cJSON_PrintUnformatted(pi->msg);
                if(data != NULL)
                {
					data_len = tx_len = strlen(data);

                    if (entry->hbMsg)
                    {
                        memcpy(last_hb_rsp_seqnum_attempted_to_send, base_seq_num, sizeof(last_hb_rsp_seqnum_attempted_to_send));
                    }
					status = NU_WSOX_Send(entry->handle, data, &data_len, WSOX_TEXT_FRAME, entry->hbMsg ? NU_WSOX_HBTTL : 0);
					if (status != NU_SUCCESS)
					{
						dbgTrace(DBG_LVL_ERROR, "WTXB(%d): Error sending message: %d" CRLF, (int)entry->handle, status);
                        if (entry->hbMsg)
                        {
                            memcpy(last_hb_rsp_seqnum_sent_failed, base_seq_num, sizeof(last_hb_rsp_seqnum_sent_failed));
                            dbgTrace(DBG_LVL_ERROR, "WTXB(%d): hb rsp - last attempted=[%s] last success=[%s] last failed=[%s]" CRLF, (int)entry->handle,
                                                last_hb_rsp_seqnum_attempted_to_send,
                                                last_hb_rsp_seqnum_sent_success,
                                                last_hb_rsp_seqnum_sent_failed   );
                            
                        }
					}
					else
                    {
                        if (verbosity > 1 && is_dbgTrace_allowed_for(pi->msg))
                        {
                            (void)GetSysDateTime( &rTime, ADDOFFSETS, GREGORIAN );
                            dbgTrace(DBG_LVL_INFO, "WTX(%d)[%02d:%02d:%02d]: %s" CRLF,
                                    (int)entry->handle, rTime.bHour, rTime.bMinutes, rTime.bSeconds, data);
                        }
                        if (entry->hbMsg)
                        {
                            memcpy(last_hb_rsp_seqnum_sent_success, base_seq_num, sizeof(last_hb_rsp_seqnum_sent_success));
                        }
                    }

					memory_free(data);
					cJSON_Delete(pi->msg);
					if (verbosity > 3)
						show_cJSON_mempool_details("wsox_tx_end");
                }
                else
                {
                	dbgTrace(DBG_LVL_INFO, "wsox_services[wsox_tx_msg_queue_handler] Out of memory error");
                }
                //dbgTrace(DBG_LVL_INFO, "WSOX_QET_CJSON Complete");
           }
            break;

        case WSOX_QET_CJSON_BROADCAST :
            {
 //               dbgTrace(DBG_LVL_INFO, "WSOX_QET_CJSON_BROADCAST Starting");
                wsox_tx_data_to_list_context_t cntx;
                struct cjson_info *pi = (VOID *)&entry->i;

                if (verbosity > 3)
                    show_cJSON_mempool_details("wsox_txb_beg");

                //cntx.data = cJSON_PrintUnformatted(pi->msg);
                cntx.msg = pi->msg;
                cntx.mask = entry->groupMask;
                cntx.tx_count = 0;
                cntx.delete_list_head.next = NULL;

                if(cntx.msg != NULL)
                {
					//cntx.data_len = strlen(cntx.data);

					cntx.dbgTrace_allowed = is_dbgTrace_allowed_for(pi->msg);
					cntx.base_seq_num_added = FALSE;

					list_process_active(&wsox_handles_list, tx_data_to_handle_list_item, &cntx);

                    /* see if any handles need to be removed */
                    while(cntx.delete_list_head.next != NULL)
                    { 
                        handle_list_t *entry = cntx.delete_list_head.next;

                        dbgTrace(DBG_LVL_ERROR, "WTXB: removing handle %u" CRLF, entry->handle);

                        cntx.delete_list_head.next = entry->next;
                        wsox_remove_handle(entry->handle);
                        memory_free(entry);
                    }

					//memory_free(cntx.data);
					cJSON_Delete(pi->msg);
					if (verbosity > 3)
						show_cJSON_mempool_details("wsox_txb_end");
                }
                else
                {
                	dbgTrace(DBG_LVL_INFO, "wsox_services[wsox_tx_msg_queue_handler] Out of memory error");
                }
 //               dbgTrace(DBG_LVL_INFO, "WSOX_QET_CJSON_BROADCAST Complete tx_count=%u", cntx.tx_count);
            }
            break;


        case WSOX_QET_PONG :
            {
                struct pong_info *pi = (VOID *)&entry->i;
                data_len = tx_len = pi->datalen;
                status = NU_WSOX_Send(entry->handle, pi->data, &tx_len, WSOX_PONG_FRAME, 0);
				if (status != NU_SUCCESS)
				{
					dbgTrace(DBG_LVL_ERROR, "WTXB(%d): Error sending pong: %d" CRLF, (int)entry->handle, status);
				}
                // RD 27July2016 - not sure why but the following trace file caused errors in the xprintf level, the argument iterator seemed to be offset causing a memory
                // violation. By changing the printf formatting from %llu to %u and casting the data seems to stop this so maybe the %llu formatting was not handled
                // correctly.
				else if (verbosity > 1)
				{
                    dbgTrace(DBG_LVL_INFO, "wsox_tx pong: handle=%d status=%d data_len=%u, tx_len=%u data=[%s]\r\n",
                            (int)entry->handle, status, (UINT)data_len, (UINT)tx_len, pi->data);
				}
                NU_Deallocate_Memory(pi->data);
            }
            break;

        case WSOX_QET_SCHEDULE_PING :
            {
                //wsox_handle_entry_t *wsox_info = wsox_get_handle_entry_by_handle(entry->handle);

                //if (wsox_info)
                //{
                //    status = NU_WSOX_Schedule_Ping(entry->handle, wsox_info->ping_interval, wsox_info->ping_delay, wsox_info->ping_retransmit_before_error);
                //    if (status == WSOX_DUP_REQUEST)
                //    {
                //        // schedule with interval == 0 will cancel an existing ping schedule */
                //        status = NU_WSOX_Schedule_Ping(entry->handle, 0 /* interval */, wsox_info->ping_delay, wsox_info->ping_retransmit_before_error);
                //        status = NU_WSOX_Schedule_Ping(entry->handle, wsox_info->ping_interval, wsox_info->ping_delay, wsox_info->ping_retransmit_before_error);
                //    }

                //    if (status != NU_SUCCESS)
                //    {
                //        dbgTrace(DBG_LVL_ERROR, "wsox_tx schedule ping: handle=%d  Error scheduling ping: %d" CRLF, (int)entry->handle, status);
                //    }
                //    else if (verbosity > 0)
                //    {
                //        dbgTrace(DBG_LVL_INFO, "wsox_tx schedule ping: handle=%d status=%d" CRLF, (int)entry->handle, status);
                //    }
                //}
                //else
                {
                    dbgTrace(DBG_LVL_ERROR, "wsox_tx schedule ping: couldn't find wsox_info for handle %d" CRLF, (int)entry->handle);
                }
            }
            break;

        case WSOX_QET_CLOSE_HANDLE_HEARTBEAT_TIMEDOUT:
            {
                //char hdr_line[50];
                //char sz_log_line[128]; 
                //DATETIME rDateTimeStamp;

                status = NU_WSOX_Close(entry->handle, WSOX_POLICY_VIOLATION, "Heartbeat timedout" );

                dbgTrace(DBG_LVL_ERROR, "WCH(%d): Closed websocket because Heartbeat Timedout: result=%d" CRLF, (int)entry->handle, status);

                //GetSysDateTime(&rDateTimeStamp, ADDOFFSETS, GREGORIAN);

                //sprintf(hdr_line, "Tasklog created on %02d%02d%02d%02d-%02d%02d%02d\r\n",
                //            rDateTimeStamp.bCentury,
                //            rDateTimeStamp.bYear,
                //            rDateTimeStamp.bMonth,
                //            rDateTimeStamp.bDay,
                //            rDateTimeStamp.bHour,
                //            rDateTimeStamp.bMinutes,
                //            rDateTimeStamp.bSeconds  );

                // dumping the tasklog to file is commented out since we have seen 3 occurences where
                // multiple consecutive heart beat timeouts caused a file system failure.
                //tasklog_dump_to_file(hdr_line, sz_log_line);
                //dbgTrace(DBG_LVL_ERROR, "WCH(%d): %s" CRLF, (int)entry->handle, sz_log_line);
            }
            break;

        case WSOX_QET_CLOSE_HANDLE_ETH0_NOT_ALLOWED:
            {
                status = NU_WSOX_Close(entry->handle, WSOX_POLICY_VIOLATION, "eth0 not allowed" );

                dbgTrace(DBG_LVL_ERROR, "WCH(%d): Closed websocket because eth0 not allowed: result=%d" CRLF, (int)entry->handle, status);

            }
            break;
    }
}


unsigned mem_alloc_success = 0;
unsigned mem_alloc_fail    = 0;
unsigned mem_free_success  = 0;
unsigned mem_free_fail     = 0;

/* -----------------------------------------------------------------------------------------------[memory_allocate]-- */
VOID *memory_allocate(size_t sz)
{
    VOID *temp_ptr;

    if (NU_Allocate_Memory(&cJSON_mem_pool, &temp_ptr, (UNSIGNED)sz, NU_NO_SUSPEND) == NU_SUCCESS)
    {
        mem_alloc_success++;
        return temp_ptr;
    }
    else
    {
        mem_alloc_fail++;
        return NU_NULL;
    }
}

/* ---------------------------------------------------------------------------------------------------[memory_free]-- */
VOID memory_free(VOID *memory_ptr)
{
    if (NU_Deallocate_Memory(memory_ptr) == NU_SUCCESS)
    {
        mem_free_success++;
        /* Memory successfully deallocated. */
    }
    else
    {
        mem_free_fail++;
        /* An error occurred deallocating memory. */
    }
}

/* ------------------------------------------------------------------------------------[show_cJSON_mempool_details]-- */
// This is called from WSOX Master Task callback, so there should be no dbgTrace calls here (use syslog writes only)
VOID show_cJSON_mempool_details(CHAR *prompt)
{
    STATUS status;
    // Keep error strings short in this function!
	char    sDbgBuf[SHORT_DBG_BUF_LEN];
    CHAR name[8];
    VOID *start_address;
    UNSIGNED pool_size;
    UNSIGNED min_allocation;
    UNSIGNED available;
    OPTION suspend_type;
    UNSIGNED task_waiting;
    NU_TASK *first_task;

    status = NU_Memory_Pool_Information(&cJSON_mem_pool, name, &start_address, &pool_size, &min_allocation, &available, &suspend_type, &task_waiting, &first_task);
    if (status == NU_SUCCESS)
    {
    	snprintf(sDbgBuf, SHORT_DBG_BUF_LEN, "%s mem avail=%u alloc(s/f)=%u/%u free(s/f)=%u/%u",
                prompt, (unsigned)available,
                mem_alloc_success, mem_alloc_fail,
                mem_free_success, mem_free_fail);
    }
    else
    {
    	snprintf(sDbgBuf, SHORT_DBG_BUF_LEN, "%s Mem stat err %d alloc(s/f)=%u/%u free(s/f)=%u/%u",
                prompt, status,
                mem_alloc_success, mem_alloc_fail,
                mem_free_success, mem_free_fail);
    }
    fnDumpStringToSystemLog(sDbgBuf);
}

/* -----------------------------------------------------------------------------------------------------[Send_JSON]-- */
void Send_JSON(cJSON *root, UINT32 groupMask)
{
    char msg[100];
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON_BROADCAST, 0, {.cjson_info = {root}}, groupMask};

    sprintf(msg, "  Added %s to tx queue for broadcast. \r\n", cJSON_GetObjectItem(root, "MsgID")->valuestring);
    addEntryToTxQueue(&entry, NULL, msg);
}







