/*SB
   ************************************************************************
     PROJECT:	Apollo Motion Controller
     AUTHOR:    Gary S. Jacobson                                           
     $Workfile:   comm.c  $
     $Revision:   1.1  $
     MODULE NAME:  MCP_COMM.C                                              
     DESCRIPTION: Application Layer Communication Routines
  
     DATE:        24-Jan-94                                         
   ----------------------------------------------------------------------
                                                                        
                 Copyright (c) 1993 Pitney Bowes Inc.                   
                      35 WaterView Drive                                
                     Shelton, Connecticut  06484                        
                                                                        
   ----------------------------------------------------------------------
  	PUBLIC FUNCTIONS:
		SendProfileStatus	
  		DoCommunications
		HandleCommMessages
		InitComm 
	PRIVATE FUNCTIONS:
  		RegisterProfile
		RegisterAccelTable
   		RemoveRegisteredProfiles
		SanityCheckProfile
		DownLoadProfile
		DownLoadAccelTable
		DownLoadSCB
		DownLoadMCB
		DownLoadProcessorCfg
		ExamineMemory
		ModifyMemory
		StartProfile
		ProfileCancel
		SendLogData
		CancelAllProfiles
		LoopBack
		StatusReply
		SensorReply
		SendDeviceId
		MessageHandler

   Revision History Notes:

   $Log:   H:/group/SPARK/ARCHIVES/SebringBase/H8s_2357/comm.c_v  $
 SE
   ************************************************************************/

/*

	comm.c

	Applications Layer Communications Interface Code

	This module performs all application layer messaging.  All messages
	from the 'host' computer are dequeued and evaulated here.


*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <ctype.h>
#include "mcp_data.h"
//#include "timeoday.h"
//#include "mcp.h"
//#include "nvm.h"

//unsigned char bMcpMonitorMode = MONITOR_LOG_DATA;

//static unsigned char bComBuffer[MAX_DATA_MSG_LENGTH];	/* buffer for incomming message */
//static unsigned char bRespBuffer[MAX_DATA_MSG_LENGTH];  /* buffer for outgoing message */
//static unsigned char bLastDest;		//lint !e551
//static unsigned char bLastSrc;
unsigned char bSourceAddress=0;
unsigned char bSystemControlAddress = 0;
//static unsigned int uSegsDownloaded = 0;
//static unsigned int uAccelBytesDownloaded = 0;
//static unsigned char fbUploadingLog = 0;
//static unsigned char fbUploadingSensorLog = 0;
//static unsigned char fbUploadingTraceLog = 0;
//static unsigned char fbClearingProfiles = 0;
//static unsigned char fbChangingBaudRate = 0;

//static void MessageHandler(void);

//extern MCP_PROFLAGS profileFlags;

//TODO - enable commented out code but use new HAL
#if 0

/*SB
   ************************************************************************
   FUNCTION	: SendProfileStatus
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: This function creates and queues a Profile Status message to the comm network layer.
   LIMITATIONS	: This function may be called at interrupt level and uses a local buffer to create the message.
		  There is no error checking done on any of the function arguments.

   INPUTS     	: 
		bProfileNumber:	Profile which the status message is about.
		bDest:		Address to send the status message to.
		bStatus:	Status token to send.
		bContext:	Context information to send (depends on bStatus)

   OUTPUTS    	:  
			TRUE if message queued

   FUNCTIONS CALLED:
  			QueueMessage
   PDL:
		Create a local buffer on the stack.
		Set the message source to bSourceAddress (which is us).
		Set the message dest. to bDest.
		Set the message command to PROFILE_STATUS
		Set the message status code to bStatus
		Set the message profile number to bProfileNumber
		Set the message context info to bContext
		Queue the message to the link layer transmitter

   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release
     1.2	GSJ		14-Feb-94	Fix bug in message src/dest (it was reversed)

  SE**********************************************************************/

void SendProfileStatus(unsigned char bProfileNumber, unsigned char bDest, unsigned char bStatus, unsigned char bContext)
{
	unsigned char bBuff[16];

	*bBuff 	   = bDest;
	*(bBuff+1) = bSourceAddress;
	*(bBuff+2) = PROFILE_STATUS;
	*(bBuff+3) = bStatus;
	*(bBuff+4) = bProfileNumber;
	*(bBuff+5) = bContext;
	(void)QueueMessage(bBuff, 6, APOLLO_MCP);

}

/*

	ErrorClass:
			0x01 - Fatal
			0x02 - Service 
			0x03 - Operator
			0x04 - Informational
*/
void LogError(unsigned char bErrorClass, unsigned char bError, unsigned char context1, unsigned char context2)
{
	unsigned char bBuff[16];
	extern unsigned char bMicroId;
	
	if (bErrorClass == FATAL_ERROR) {
		*bBuff      = bSystemControlAddress;
		*(bBuff+1) = (unsigned char) (bMicroId + MCP_TASK_ID_OFFSET);
		*(bBuff+2) = INITIALIZE_LINK;
		(void)QueueMessage(bBuff, 3, DIAGNOSTIC);
	}
	
	*bBuff 	   = bSystemControlAddress;
	*(bBuff+1) = (unsigned char) (bMicroId + MCP_TASK_ID_OFFSET);
	*(bBuff+2) = LOG_ERROR;
	*(bBuff+3) = bError;
	*(bBuff+4) = 0x07;
	*(bBuff+5) = bErrorClass;
	*(bBuff+6) = 0x00;
	*(bBuff+7) = context1;
	*(bBuff+8) = context2;
	(void)QueueMessage(bBuff, 9, APOLLO_MCP);
}

void SendMailpieceData(MAIL_PIECE_RECORD * pData)
{
	unsigned char bBuff[sizeof(MAIL_PIECE_RECORD)+8];
	extern unsigned char bMicroId;
	
	*bBuff 	   = bSystemControlAddress;
	*(bBuff+1) = (unsigned char) (bMicroId + MCP_TASK_ID_OFFSET);
	*(bBuff+2) = MAILPIECE_DATA;
	*(bBuff+3) = 4;            					//   Mailpiece type 4 - DM400c Feeder Gap Data
	
	memcpy((bBuff+4), pData, sizeof(MAIL_PIECE_RECORD));	
	(void)QueueMessage(bBuff, 4 + sizeof(MAIL_PIECE_RECORD), APOLLO_MCP);
}

/*SB
   ************************************************************************
   FUNCTION	: RegisterProfile
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: 
		  This function enters the profile into the installed profile
		table and initializes the table entry.  If the profile is found
		in the installed profile table, it is deallocated and replaced 
		with the new entry.

   LIMITATIONS	:
		  The profile number must be less than MAX_PROFILES and the profile
		must not be running.  This function can not be called at interrupt
		level and MUST be called from background level.

   INPUTS	: 
		pSeg		: Pointer to first segment of the profile
		uProfileNumber	: Profile Number to install
		uNumberofSegments: Number of segments in the profile
		uLastProfile	: Last Profile Number Used

   OUTPUTS	:  
		This function returns a TRUE (non-zero) value if no errors were detected.
		This function also updates the InstalledProfile[] Table.
		This function may also free up heap space if the existing profile was replaced.

   FUNCTIONS CALLED:
  			free()
			QueueMessage
   PDL:
		IF the profile number is invalid THEN
		   send a PROFILE_STORED message with the status of ILLEGAL_PROFILE_NUMBER.
		   return(ERROR)
		IF the profile is currently active THEN
		   send a ERROR_STATUS message with the status of ERR_PROFILES_ACTIVE.
		   return(ERROR)
		IF the profile entry in the installed profile table is installed THEN
		   Deallocate the memory for the old profile
		Update the installed profile table
		  Save the address of the new profile.
		  Set the profile state to idle;
		  Clear the cancel flag .
		  Save the number of segments.
		return(OK)


   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release
     1.3	GSJ		24-Feb-94	Swap LastDest & LastSource
  SE**********************************************************************/

unsigned char * RegisterProfile(unsigned int uBytesInProfile, unsigned uProfileNumber, unsigned uNumberOfSegments)
{
	extern INSTALLED_PROFILE InstalledProfile[];
	extern unsigned int uLastProfile;
	unsigned char * pSeg; 
	register INSTALLED_PROFILE * p;
    unsigned char bRespBuffer[10];

	if (uProfileNumber < MAX_PROFILES) 
	{
		if (uProfileNumber > uLastProfile)
			uLastProfile = uProfileNumber;	/* Save Highest Profile Number */

		p = InstalledProfile+uProfileNumber;	/* Calculate the table entry pointer */


		if (p->fbActive) {
			p->fbActive = 0;
			/* send out error message */
			*bRespBuffer 	 = bLastSrc;
			*(bRespBuffer+1) = bSourceAddress;
			*(bRespBuffer+2) = ERROR_STATUS;
			*(bRespBuffer+3) = ERR_PROFILES_ACTIVE;
			*(bRespBuffer+4) = (unsigned char)uProfileNumber;
			*(bRespBuffer+5) = 0;
			(void)QueueMessage(bRespBuffer, 6, APOLLO_MCP);
//			return(NULL);
		}  

//		set_imask_ccr(1);
		if (p->pProfile != NULL) {		/* Don't allow duplicate profile numbers */
			free(p->pProfile);		/* dealocate the profile currently assigned to this profile # */
		}
		pSeg = (unsigned char *) malloc((size_t)uBytesInProfile);	/* Allocate Memory for profile */
		//		set_imask_ccr(0);

		if (pSeg == NULL) {
			/* send out of memory message */
			*bRespBuffer 	 = bLastSrc;
			*(bRespBuffer+1) = bSourceAddress;
			*(bRespBuffer+2) = PROFILE_STORED;
			*(bRespBuffer+3) = OUT_OF_MEMORY;
			*(bRespBuffer+4) = (unsigned char)uProfileNumber;
			*(bRespBuffer+5) = 0;
			(void)QueueMessage(bRespBuffer, 6, APOLLO_MCP);
			return(NULL);
		} 
		p->pProfile = (SEGMENT *) pSeg;		/* Save the new profile address */
		p->uNumberOfSegments = uNumberOfSegments;
		p->fbActive = 0;
		p->fbCancel = 0;
		return(pSeg);
	}

	*bRespBuffer 	 = bLastSrc;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = PROFILE_STORED;
	*(bRespBuffer+3) = ILLEGAL_PROFILE_NUM;
	*(bRespBuffer+4) = (unsigned char) uProfileNumber;
	*(bRespBuffer+5) = 0;
	(void)QueueMessage(bRespBuffer, 6, APOLLO_MCP);

	return(NULL);
}

/*SB
   ************************************************************************
   FUNCTION	: RemoveRegisteredProfiles
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: 
		  This function will remove all installed profiles and deallocate
		the profiles in the table.

   LIMITATIONS	:
		  The caller should not have any active profiles when this function is 
		called.  This function does not cancel any profiles! 

   INPUTS	: 	none
   OUTPUTS	:  	none
   FUNCTIONS CALLED:	none
  			
   PDL:
		LOOP for all of the installed profiles
		  IF profile is installed THEN
			disable tasking
			deallocate the profile
			reset the number of segments to 0
			enable tasking
		ENDLOOP

   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release

  SE**********************************************************************/
static void RemoveRegisteredProfiles(void)
{
	register INSTALLED_PROFILE * p;
	extern INSTALLED_PROFILE InstalledProfile[];
	
	for (p=InstalledProfile; p < InstalledProfile+MAX_PROFILES; p++) 	/* For all table entries ... */
		if (p->pProfile != NULL) 					/* If the profile is installed, */
		{
			set_imask_ccr(1);
			free(p->pProfile);					/* dealocate the memory */
			p->uNumberOfSegments = 0;				/* and reset the segment count */
			set_imask_ccr(0);
		}
}

/*SB
   ************************************************************************
   FUNCTION	: SanityCheckProfile
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: 
		  This function will verify the correctness of the profile in
		the context of the existing system.

   LIMITATIONS	:
		  This function may be called in background mode ONLY.
   INPUTS	: 
			uProfileNumber:	Profile number to check
   OUTPUTS	:  
		  This function returns TRUE (non-zero) if no errors are found.

   FUNCTIONS CALLED:
  			QueueMessage
   PDL:
		Send a PROFILE_STORED message with a the error status 

   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release
     1.2	GSJ		07-Feb-94	Don't send status back if there are no errors
     1.3	GSJ		24-Feb-94	Swap LastDest & LastSource

  SE**********************************************************************/
static int SanityCheckProfile(unsigned int uProfileNumber)
{
/*
	Send Error Status ONLY
	*bRespBuffer 	 = bLastSrc;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = PROFILE_STORED;
	*(bRespBuffer+3) = NO_ERROR;
	*(bRespBuffer+4) = uProfileNumber;
	*(bRespBuffer+5) = 0;
	QueueMessage(bRespBuffer, 6, APOLLO_MCP);
*/
	return(1);
}     


/*SB
   ************************************************************************
   FUNCTION	: DoCommunications
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: 
		  This function is invoked by the event handler to do a periodic 
		call to the linklayer transmitter routines.  This routine will force the link
		timers to be aged for each communications tick.  Recursion into
		the link layer is NOT ALLOWED!

   LIMITATIONS	:
		  This function should ONLY be called by the event handler.

   INPUTS	: none
   OUTPUTS	: none
 
   FUNCTIONS CALLED:	
		DecrementTimers
		LinkLayerTransmit

   PDL:
		Call Decrement Link Timers
		IF Link layer is active THEN
		   return
		ELSE
		   Set link layer active flag
		   Call the link layer
		   Clear the link layer active flag

   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release

  SE**********************************************************************/
void DoCommunications(void)
{
	static unsigned char fbActive=0;

	if (fbActive++ == 0) {	/* Don't allow recursion into com layer */
		Transmit();
		MessageHandler();
	}
	fbActive--;	/* Clear the Comm Active flag */
}

/*SB
   ************************************************************************
   FUNCTION	: DownLoadMaintTable
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: This function will take the Maint. Tables and save them in 
   				  the flash save area
   				  
*/
static void DownLoadMaintTable(void)
{                                                         
	extern MAINT_TABLE MaintTable[];
	unsigned char bTableID; 
	unsigned char bNumberOfEntries;
	unsigned int * pMaint;
	unsigned int uTableSize;
	
	bTableID = *(bComBuffer + DL_MAINTANCE_TABLE_ID);
	bNumberOfEntries = *(bComBuffer + DL_NUM_MAINT_ENTRIES); 
	uTableSize = bNumberOfEntries * 2;  // each entry is 2 bytes
	
	MaintTable[bTableID].bNumberOfEntries = bNumberOfEntries;
	     
	if (MaintTable[bTableID].pTable != NULL){
		//		set_imask_ccr(1);
		free((void *) (MaintTable[bTableID].pTable));
		//		set_imask_ccr(0);
	}
	//	set_imask_ccr(1);
	pMaint = (unsigned int *) malloc((size_t)uTableSize);	
	//	set_imask_ccr(0);
	memcpy(pMaint, bComBuffer + DL_MAINT_DATA, (size_t)uTableSize);

	MaintTable[bTableID].pTable = pMaint; 
}

static void DownLoadProfileCfg(void)
{
	extern unsigned char * pProfileCfg;
	unsigned char * pCfg;
	
	pCfg = bComBuffer+3;
	
	if (pProfileCfg != NULL){
		//		set_imask_ccr(1);
		free(pProfileCfg);   
		//		set_imask_ccr(0);
	}
	//	set_imask_ccr(1);
	pProfileCfg = malloc(strlen((char *)pCfg)+1);
	//	set_imask_ccr(0);
	memcpy(pProfileCfg, pCfg, strlen((char *)pCfg)+1); 
	pProfileCfg[8] = 0;
}	

static void Profile_Cfg_Reply(void)
{
	extern unsigned char * pProfileCfg;
    unsigned char bRespBuffer[80];
  
	*bRespBuffer 	 = bLastSrc;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = PROFILE_CFG_RESPONSE; 
    *(bRespBuffer+3) = 0;		// Default to null string

	strcpy((char *) bRespBuffer + 3, (char *)pProfileCfg);

	(void) QueueMessage(bRespBuffer,  (unsigned int) strlen((char *)pProfileCfg) + 3, APOLLO_MCP);
}

/*SB
   ************************************************************************
   FUNCTION	: DownLoadProfile
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: 
		  This function will take the raw download profile message
		buffers and de-packetizes and store the profile data on the 
		heap.

   LIMITATIONS	:
		  This function should be called from the backgound ONLY.
		The integrity of the profile is subject to the proper ordering
		of the raw profile messages.  Once a new download starts, it must
		be completed in the proper sequence before the next profile download 
		is started. If an error was generated on the first download packet 
		then the function will not accept any further packets for the profile
		that caused the error. (The packet sequence number will be non-zero)
 
   INPUTS	: 
			uLen		: Length of message buffer
   GLOBALS	:
			bComBuffer	: Incomming message buffer
		  	bRespBuffer	: Outgoing message buffer
			uSegsDownloaded : Number of segments loaded for current profile


   OUTPUTS	:  	none

   FUNCTIONS CALLED:
  			malloc
			QueueMessage
			RegisterProfile
			memcpy
			SanityCheckProfile

   PDL:
	IF this is the first packet for this download THEN
	   IF profile packet sequence number is not 0 THEN
	      ignore the packet and return
	   Allocate memory in heap for profile
	   IF allocation fails THEN
	      Send PROFILE_STORED message with a status of OUT_OF_MEMORY
	      return
	   Call RegisterProfile
	   IF error THEN return
	
	Copy the profile data into the heap profile storage area
	IF we are finshed with the download THEN
	   Setup for the first time download
	   Call the SanityCheckProfile	
	ELSE
	   setup for next packet

   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release
     1.1        GSJ		25-Jan-94	Add sequence number check if first packet is expected	
     1.2	GSJ		07-Feb-94	Make the uSegsDownloaded global for this module  
     1.3	GSJ		24-Feb-94	Swap LastDest & LastSource
		GSJ		11-Jul-94	Add Error messages for sequence numb

  SE**********************************************************************/
static void DownLoadProfile(int uLen)
{
	static unsigned char * pSeg = NULL;
	static unsigned int uCurrentProfile;

	register unsigned char * pStart;
	unsigned int uProfileNumber;
	unsigned int uNumberOfSegments;
	unsigned int uSegmentsInPacket;
	unsigned int uBytesInProfile;
	unsigned int uBytesInPacket;
	unsigned char bSeqNum;
    unsigned char bRespBuffer[10];

	pStart = bComBuffer;
	uProfileNumber 	  = *(pStart+DL_PROFILE_NUMBER);
	uNumberOfSegments = *(pStart+DL_NUMBER_OF_SEGMENTS);
	uSegmentsInPacket = *(pStart+DL_NUM_SEG_IN_PKT);
	bSeqNum = *(pStart+DL_SEQ_NUM);
	pStart += DL_PROFILE;					/* Point to start of profile segment data */

	uBytesInProfile = (unsigned int) (uNumberOfSegments * sizeof(SEGMENT));	/* Calculate the number of bytes in the current profile */

	if (uSegsDownloaded == 0) {				/* If this is the first packet for this profile */
		if (bSeqNum != 0) {				/* If the first packet was rejected then ignore the rest */
			*bRespBuffer 	 = bLastSrc;
			*(bRespBuffer+1) = bSourceAddress;
			*(bRespBuffer+2) = PROFILE_STORED;
			*(bRespBuffer+3) = ERR_PROFILE_SEQ;
			*(bRespBuffer+4) = (unsigned char)uProfileNumber;
			*(bRespBuffer+5) = 0;
			(void)QueueMessage(bRespBuffer, 6, APOLLO_MCP);
			return;
		}

		uCurrentProfile = uProfileNumber;
	
		pSeg = RegisterProfile(uBytesInProfile, uProfileNumber, uNumberOfSegments);
		if (pSeg == NULL)
				return;
						
	}

	if (uCurrentProfile == uProfileNumber) {
		uBytesInPacket = (unsigned int)(uSegmentsInPacket * sizeof(SEGMENT));
	
		memcpy(pSeg, pStart, (size_t)uBytesInPacket);
		pSeg += uBytesInPacket;
		uSegsDownloaded += uSegmentsInPacket;
	
		if (uSegsDownloaded == uNumberOfSegments) {		/* If we are finished with this profile download then ... */
//			SanityCheckProfile(uProfileNumber);		/* check the profile for errors */
			uSegsDownloaded = 0;
			pSeg = NULL;
		} 
	} else {
		*bRespBuffer 	 = bLastSrc;
		*(bRespBuffer+1) = bSourceAddress;
		*(bRespBuffer+2) = PROFILE_STORED;
		*(bRespBuffer+3) = ERR_PROFILE_NUMB_ORDER;
		*(bRespBuffer+4) = (unsigned char)uProfileNumber;
		*(bRespBuffer+5) = 0;
		(void)QueueMessage(bRespBuffer, 6, APOLLO_MCP);
	}
}


/*SB
   ************************************************************************
   FUNCTION	: DownLoadAccelTable
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: 
		  This function takes the raw download accel table packets and
		de-packetizes and stores the acceleration table data on the heap.

   LIMITATIONS	:
		  This function should be called from the backgound ONLY.
		The integrity of the acceleration table is subject to the proper ordering
		of the raw accel table messages.  Once a new download starts, it must
		be completed before the next accel table download is started. If an error
		was generated on the first download packet then the function will not
		expect any more packets for the acceleration table with the error in it.
 

   INPUTS	: 
			uLen	: Length of the packet

   GLOBALS	:
			bComBuffer	: Incomming message buffer
		  	bRespBuffer	: Outgoing message buffer

   OUTPUTS	:  	none

   FUNCTIONS CALLED:
  			malloc
			QueueMessage
			RegisterAccelTable
 
   PDL:
		IF this is the first packet for this acceleration table THEN
		   IF the accel table packet sequence number is not 0 THEN
		      ignore the packet and return
		   Allocate heap memory for table
		   IF Allocation fails THEN
		      Send ERROR_STATUS message with status of OUT_OF_MEMORY
		      return
		   IF Invalid acceleration table number THEN
		      Send ERROR_STATUS message with status of INVALID_ACCEL_TABLE
		      return
		   Call RegisterAccelTable	
		Copy acceleration table data into heap table storage
		IF we are finshed with the download THEN
	   	   Setup for the first time download
		ELSE
	   	   Setup for next packet

		
   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release
     1.1    GSJ		25-Jan-94	Add sequence number check if first packet is expected	
     1.2	GSJ		07-Feb-94	Make uAccelBytesDownloaded global for this module   
     1.3	GSJ		24-Feb-94	Swap LastDest & LastSource
			GSJ		11-Jul-94	Add error messages for out of sequence & out of order packets

  SE**********************************************************************/
static void DownLoadAccelTable(int uLen)
{
	extern ACCEL_TABLE AccelTable[];
	static unsigned char * pAccelTable = NULL;
	static unsigned char bCurrentTable;

	register unsigned char * pStart;

	unsigned int uAccelTableNumber;
	unsigned int uBytesInTable;
	unsigned int uBytesInPacket;
	unsigned char bSeqNum;
    unsigned char bRespBuffer[10];

	pStart = bComBuffer;
	uAccelTableNumber = *(pStart+DL_ATBL_NUM);
	uBytesInTable = *(unsigned int *)(pStart+DL_ATBL_LEN);
	uBytesInPacket = uLen-DL_ATBL_DATA;
	bSeqNum = *(pStart+DL_ATBL_SEQ_NUM);

	pStart += DL_ATBL_DATA;					/* Point to start of acceleration table data */

	if (uAccelBytesDownloaded == 0) {			/* If this is the first packet for this table */
		if (bSeqNum != 0) {				/* If the first packet was rejected then ignore the rest */
			/* send error message */
			*bRespBuffer 	 = bLastSrc;
			*(bRespBuffer+1) = bSourceAddress;
			*(bRespBuffer+2) = ERROR_STATUS;
			*(bRespBuffer+3) = ERR_ACCEL_SEQ;
			*(bRespBuffer+4) = (unsigned char)uAccelTableNumber;
			*(bRespBuffer+5) = 0;
			(void)QueueMessage(bRespBuffer, 6, APOLLO_MCP);
			return;		
		}


		if (uAccelTableNumber < MAX_ACCEL_TBL) {
			//			set_imask_ccr(1);
			if (AccelTable[uAccelTableNumber].pTable != NULL) { 	/* If accel table is already used */
				free(AccelTable[uAccelTableNumber].pTable);	/* deallocate the memory */
			}	
			pAccelTable = (unsigned char *) malloc((size_t)uBytesInTable);	/* Allocate Memory for table */
			//			set_imask_ccr(0);
		} else
			pAccelTable = NULL;


		bCurrentTable = (unsigned char)uAccelTableNumber;
		if ((pAccelTable == NULL) || (uAccelTableNumber >= MAX_ACCEL_TBL)) {
			/* send out of memory message */
			*bRespBuffer 	 = bLastSrc;
			*(bRespBuffer+1) = bSourceAddress;
			*(bRespBuffer+2) = ERROR_STATUS;

			if (uAccelTableNumber < MAX_ACCEL_TBL)
				*(bRespBuffer+3) = OUT_OF_MEMORY;
			else
				*(bRespBuffer+3) = INVALID_ACCEL_TABLE;

			*(bRespBuffer+4) = (unsigned char)uAccelTableNumber;
			*(bRespBuffer+5) = 0;
			(void)QueueMessage(bRespBuffer, 6, APOLLO_MCP);
			return;
		} else 
			(void)RegisterAccelTable(pAccelTable, uAccelTableNumber, uBytesInTable);
	}
	if (uAccelTableNumber == bCurrentTable) {
		memcpy(pAccelTable, pStart, (size_t)uBytesInPacket);
		pAccelTable += uBytesInPacket;
		uAccelBytesDownloaded += uBytesInPacket;
	
		if (uAccelBytesDownloaded == uBytesInTable) {		/* If we are finished with this profile download then ... */
			uAccelBytesDownloaded = 0;			/* reset the pointers for the next accel table */
			pAccelTable = NULL;	
		} 
	} else {
			/* send error message */
			*bRespBuffer 	 = bLastSrc;
			*(bRespBuffer+1) = bSourceAddress;
			*(bRespBuffer+2) = ERROR_STATUS;
			*(bRespBuffer+3) = ERR_ACCEL_NUMB_ORDER;
			*(bRespBuffer+4) = (unsigned char)uAccelTableNumber;
			*(bRespBuffer+5) = 0;
			(void)QueueMessage(bRespBuffer, 6, APOLLO_MCP);
	}
}

/*SB
   ************************************************************************
   FUNCTION	: DownLoadSCB
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: 
		  This function takes the raw download SCB packet and configures
		each sensor in the SCB download list.

   LIMITATIONS	:
		  This function should be called from the backgound ONLY.
		The Sensor Configuration data is assumed to be correct, no
		error checking is done to avoid duplications.  The 'last' dupiclate
		will overwrite `prior` sensors.


   INPUTS	: 
			none

   GLOBALS	:
			bComBuffer	: Incomming message buffer
			bSensorOffsets	: Sensor Offset A/D Value array
			bSensorThreshold: Sensor Threshold A/D Value array
			bDebounceTicks	: Sensor Debounce ticks array
			fbBlockedState	: Sensor Blocked State flag value array

   OUTPUTS	:  	none

   FUNCTIONS CALLED:	none
 
   PDL:
			LOOP for each device
				Copy data into Blocked State array 
				Convert Device ID into array index
				IF ANALOG sensor
					COPY offsets into offset array
					COPY Thresholds into Thresholds array
				ELSE
					COPY data into DEBOUNCE array
				Advance Pointer to next device
			END
		
   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.4	GSJ		24-Feb-94	Initial Release


  SE**********************************************************************/
static void DownLoadSCB(void)
{
	unsigned char bNumberOfDevices;
	register unsigned char * pCfg;
	register unsigned char bSensorIndex;

	extern unsigned char bSensorOffsets[];	 
	extern unsigned char bSensorThreshold[];
	extern unsigned char bDebounceTicks[];
	extern unsigned char fbBlockedState[];
#ifdef MIDJET
	extern NVM_REC Nvm;
#endif

	bNumberOfDevices = *(bComBuffer + DL_SCB_NUM_SENSORS);
	pCfg = (bComBuffer + DL_SCB_DEVID);

	while (bNumberOfDevices--) 
	{
		bSensorIndex = pCfg[DL_SCB_SENSOR_ID];
		bSensorIndex &= 0x0F;				/* Mask off device type */
		if (pCfg[DL_SCB_SENSOR_TYPE] == 0) 		/* 0 == ANALOG SENSOR */
		{
			if (bSensorIndex < N_SENSORS) {
				fbBlockedState[bSensorIndex+N_DINPUTS] = pCfg[DL_SCB_BLOCKED_STATE];
				bSensorOffsets[bSensorIndex] = pCfg[DL_SCB_OFFSET]; 
				bSensorThreshold[bSensorIndex] = pCfg[DL_SCB_THRESHOLD];
				if (255 - bSensorOffsets[bSensorIndex] <= bSensorThreshold[bSensorIndex]) 
				{
				/* Send a Service Error Message - We should not continue because sensor will never transition ! */
					LogError(SVC_ERROR, ERR_SENSOR_THRESHOLD, bSensorIndex, bSensorOffsets[bSensorIndex]);
					SetBlinkCode(BLINK_SENSOR_ERROR,1);
				}
			}
		} else if (bSensorIndex < N_DINPUTS) {
			fbBlockedState[bSensorIndex] = pCfg[DL_SCB_BLOCKED_STATE];
			bDebounceTicks[bSensorIndex] = pCfg[DL_SCB_DEBOUNCE_TICKS];
		}
		pCfg += DL_SCB_BLKSIZE;
	}
	RefreshSensorFlags();
}

/*SB
   ************************************************************************
   FUNCTION	: DownLoadMCB
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: 
		  This function takes the raw download MCB packet and configures
		each motor in the MCB download list.

   LIMITATIONS	:
		  This function should be called from the backgound ONLY.
		The Motor Configuration data is assumed to be correct, no
		error checking is done to avoid duplications or overlap of
		internal resources with respect to each motor.		

   INPUTS	: 
			none

   GLOBALS	:
			bComBuffer	: Incomming message buffer
			wLastMotor	: Last Motor Used

   OUTPUTS	:  	none

   FUNCTIONS CALLED:
			ConfigureMotor
 
   PDL:
			LOOP for each device
				CALL ConfigureMotor
				Advance Pointer to next device
			END
		
   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.3	GSJ		16-Feb-94	Initial Release


  SE**********************************************************************/
static void DownLoadMCB(void)
{
	unsigned char bNumberOfDevices;
	register MOTOR_CFG * pMtrCfg;
	extern unsigned int uLastMotor;
	unsigned char bRespBuffer[10]; 

	bNumberOfDevices = *(bComBuffer + DL_MCB_NUMDEV);
	pMtrCfg = (MOTOR_CFG *) (bComBuffer + DL_MCB_DEVID);
	uLastMotor = 0;
	

	if (bNumberOfDevices > MAX_MOTORS)
	{
		*bRespBuffer 	 = bLastSrc;
		*(bRespBuffer+1) = bSourceAddress;
		*(bRespBuffer+2) = ERROR_STATUS;
		*(bRespBuffer+3) = ERR_TOO_MANY_MCBS;
		*(bRespBuffer+4) = bNumberOfDevices;
		*(bRespBuffer+5) = 0;
		(void)QueueMessage(bRespBuffer, 6, APOLLO_MCP);		
	}

	//	set_imask_ccr(1);
	while (bNumberOfDevices--) 
	{
		if (bNumberOfDevices < MAX_MOTORS)
			ConfigureMotor(pMtrCfg++);
	}
	//	set_imask_ccr(0);
}

/*SB
   ************************************************************************
   FUNCTION	: DownLoadProcessorCfg
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: 
		  This function takes the raw processor config download packet and 
		reconfigures the event handler & event task queue.

   LIMITATIONS	:
		  This function should be called from the backgound ONLY.
		The event interrupts are suspended while this function modifies
		the system tick timer.

   INPUTS	: 
			none

   GLOBALS	:
			bComBuffer	: Incomming message buffer

   OUTPUTS	:  	none

   FUNCTIONS CALLED:
			disable
			enable
			memcpy
			InitSystemTimer
			CalibrateTimers
 
   PDL:
			Get Timer Quantum Information
			STOP the event handler interrupts
			LOOP for all TASK ID's 
				IF TASK ID is legal THEN
					Update the event queue
			END 
			Restart the system tick timer with the new values
			Recalibrate the link timers
		
   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.3	GSJ		16-Feb-94	Initial Release
     1.5	GSJ		01-Mar-94	Add Link Timers to download info
     1.6	GSJ		06-May-94	Don't allow task id's > MAX_TASK_ID
  SE**********************************************************************/

static void DownLoadProcessorCfg(void)
{
	extern PROC_CFG ProcessorConfig;
	register unsigned char * pTaskInfo;
			
	StopWatchdogTimer();
    StopSystemTimer();
	ProcessorConfig.bDivisor   = bComBuffer[DL_PCFG_DIVISOR];
	ProcessorConfig.bPrescaler = (unsigned char) (bComBuffer[DL_PCFG_PRESCALE] & 0x0F);
	ProcessorConfig.bServoPrescaler = (unsigned char)((bComBuffer[DL_PCFG_PRESCALE] & 0xF0) >> 4);
	ProcessorConfig.bServoDivisor   = bComBuffer[DL_SERVO_DIVISOR];
	ProcessorConfig.wLinkPacketTimeout = bComBuffer[DL_LINK_TIMEOUT];
	memcpy(&ProcessorConfig.wLinkDeadmanTimeout, (bComBuffer + DL_LINK_DEADMAN),2);

	//	set_imask_ccr(1);

	for (pTaskInfo  = bComBuffer + DL_PCFG_TASKINFO; *(pTaskInfo) != 0xFF; pTaskInfo += DL_PCFG_TASKBLKSIZE)
		if (pTaskInfo[DL_PCFG_TASKID] < MAX_EVENTS)
			(void)UpdateEvent((unsigned)pTaskInfo[DL_PCFG_TASKID],(unsigned)pTaskInfo[DL_PCFG_START_TICK],(unsigned)pTaskInfo[DL_PCFG_RESCHED]);

//	InitSystemTimer(ProcessorConfig.bDivisor, ProcessorConfig.bPrescaler);

#ifdef MIDJET
	InitServoTimer(ProcessorConfig.bServoDivisor, ProcessorConfig.bServoPrescaler);
#endif

	StartSystemTimer();
	//	set_imask_ccr(0);
	TurnOffRedLed(0,0);			/* Clear Error Indicator if any ... */
			
	StartWatchdogTimer(WATCH_DOG_TIMEOUT);

}

/*SB
   ************************************************************************
   FUNCTION	: ExamineMemory
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: 
		  This function will copy memory from the microprocessors
		address space and send the copy to the requestor.

   LIMITATIONS	:
		  The bytecount to transfer must be less than or equal to 
		300 bytes in length.  It is the callers responsibility to
		pick a valid memory address and length.  No runtime error
		checking is done.  This function must be called at background
		level and may NOT be called from interrupt level.

   INPUTS	: none
   GLOBALS	:
			bComBuffer	: Incomming message buffer
		  	bRespBuffer	: Outgoing message buffer

   OUTPUTS	: none
 
   FUNCTIONS CALLED:
  			memcpy
			QueueMessage

   PDL:
		Create pointer to target memory from examine command buffer
		Extract bytecount from examine command buffer
		Copy requested bytes into outgoing message buffer
		Send MEMORU_IMAGE message to requestor.

   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release
     1.3	GSJ		24-Feb-94	Swap LastDest & LastSource

  SE**********************************************************************/
static void ExamineMemory(void)
{
	unsigned char * pMemory;
	unsigned char bCount;
	unsigned char bRespBuffer[MAX_DATA_MSG_LENGTH]; 

	memcpy(&pMemory, (bComBuffer+START_ADDRESS), sizeof(long));	/* Must use byte moves to get the target memory address */
									/* because the source might be on an odd address boundary */
	bCount  = *(bComBuffer+MEM_BYTE_COUNT);				/* Copy the number of bytes requested */

	*bRespBuffer 	 = bLastSrc;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = MEMORY_IMAGE;
	*(bRespBuffer+3) = bCount;
	memcpy(bRespBuffer+4, pMemory, (size_t)bCount);

	(void)QueueMessage(bRespBuffer, (unsigned) (bCount+4), APOLLO_MCP);
}


/*SB
   ************************************************************************
   FUNCTION	: ModifyMemory
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: Copy bytes from Modify Memory message into target address.

   LIMITATIONS	: It is the callers responsibility to pick a valid memory
		 address and length.  No runtime error checking is done.
		 This function must be called at background level and may
		 NOT be called from interrupt level.

   INPUTS	: 
   GLOBALS	:
			bComBuffer	: Incomming message buffer
		  	bRespBuffer	: Outgoing message buffer

   OUTPUTS	:  	Target memory address is modified
   FUNCTIONS CALLED:
  			memcpy 
   PDL:
		Create pointer into targer memory from command buffer address
		Extract bytecount from command buffer
		Copy memory from command buffer data into target address

   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release

  SE**********************************************************************/
static void ModifyMemory(void)
{
	unsigned char *pMemory;
	unsigned char bCount;
	unsigned char bRespBuffer[MAX_DATA_MSG_LENGTH]; 

	memcpy(&pMemory, (bComBuffer+START_ADDRESS), sizeof(long));	/* Must use byte moves to get the target memory address
									 * because the source might be on an odd address boundary */
	bCount = *(bComBuffer+MEM_BYTE_COUNT);				/* Get the number of bytes to modify */
	memcpy(pMemory, bComBuffer+MEM_WDATA, (size_t) bCount);			/* Copy the memory from the frame buffer to the dest. addr */
}


/*SB
   ************************************************************************
   FUNCTION	: StartProfile
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: This function marks the profile(s) as active and
		enables it to be executed stating from the first segment.

   LIMITATIONS	: This function should be called from background level and
		NOT at interrupt level.

   INPUTS	: 
   GLOBALS	:
			bComBuffer	: Incomming message buffer
		  	bRespBuffer	: Outgoing message buffer

   OUTPUTS	:  
   FUNCTIONS CALLED:
  			QueueMessage
   PDL:
		LOOP through all profiles in the start profile message
		   IF profile is active THEN
		      Send ERROR_STATUS message with ERR_PROFILES_ACTIVE status
		   ELSE IF profile is not installed THEN
		      Send ERROR_STATUS message with ERR_PROFILE_NOT_INSTALLED status
		   ELSE
		      Set first time flag
		      Set segment start to 0
		      Save id of requestor
		      Clear motor mask
		      Clear Cancle flag
		      Mark profile as active
		END LOOP

   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release
     1.3	GSJ		24-Feb-94	Swap LastDest & LastSource

  SE**********************************************************************/
static void StartProfile(void)
{
	extern INSTALLED_PROFILE InstalledProfile[];
	register INSTALLED_PROFILE * pInstProf;
	register unsigned char *pStartProfile;
	unsigned char bRespBuffer[10]; 

	/*	Loop through all profiles in packet and setup the installed profile data structure */
	for (pStartProfile = bComBuffer+SP_PROFILE_NUMBER; *pStartProfile != 0xFF; pStartProfile += 2) {
		pInstProf = &InstalledProfile[*pStartProfile];
		if (pInstProf->fbActive || pInstProf->pProfile == NULL) {
			/* If we are already active or not installed then fire off an error message */
			*bRespBuffer 	 = bLastSrc;
			*(bRespBuffer+1) = bSourceAddress;
			*(bRespBuffer+2) = ERROR_STATUS;

			if (pInstProf->fbActive)
				*(bRespBuffer+3) = ERR_PROFILES_ACTIVE;
			else
				*(bRespBuffer+3) = ERR_PROFILE_NOT_INSTALLED;

			*(bRespBuffer+4) = *pStartProfile;
			*(bRespBuffer+5) = 0;
			(void)QueueMessage(bRespBuffer, 6, APOLLO_MCP);	
		} else {
			pInstProf->fbFirstTime = TRUE;
			pInstProf->bSegment = 0;
			pInstProf->bMotorMask = 0;
			pInstProf->bSolenoidMask = 0;
			pInstProf->bServoMask = 0;
			pInstProf->fbCancel  = 0;
			pInstProf->bSessionId = bLastSrc;
//			pInstProf->bIterations = *(pStartProfile+1); 
		/* use  pInstProf->wIterations = *((unsigned int *) (pStartProfile+1))  when spec is chagned */
			pInstProf->fbActive = TRUE;
		}
	}
}

/*SB
   ************************************************************************
   FUNCTION	: ProfileCancel
   AUTHOR	: Gary S. Jacobson

   DESCRIPTION	: This function marks profile(s) for cancelation in the installed profile list.

   LIMITATIONS	: This function should be called from background level, NOT interrupt level.  No
		error check is done for non-installed profiles.

   INPUTS	: 
   GLOBALS	:
			bComBuffer	: Incomming message buffer
		  	bRespBuffer	: Outgoing message buffer

   OUTPUTS	:  
   FUNCTIONS CALLED:
  			QueueMessage
   PDL:
		LOOP though all profiles to cancel in cancel profile message
		  IF profile is not active THEN
		     Send PROFILE_STATUS message with status of PS_CAN_NOT_FOUND
		  ELSE
		     Mark the profile for cancelation in installed profile list
		END LOOP

   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release
     1.3	GSJ		24-Feb-94	Swap Src& Dest
  SE**********************************************************************/
static void ProfileCancel(void)
{
	extern INSTALLED_PROFILE InstalledProfile[];
	register INSTALLED_PROFILE * pInstProf;
	register unsigned char *pCancelProfile;
	unsigned char bRespBuffer[10]; 

	/*	Loop through all profiles in packet and setup the installed profile data structure */
	for (pCancelProfile = bComBuffer+CP_PROFILE_NUMBER; *pCancelProfile != 0xFF; pCancelProfile++) {
		pInstProf = &InstalledProfile[*pCancelProfile];
		if (pInstProf->pProfile) {
			pInstProf->fbActive = 1;
			pInstProf->fbCancel = 1;
		} else {
			*bRespBuffer 	 = bLastSrc;
			*(bRespBuffer+1) = bSourceAddress;
			*(bRespBuffer+2) = PROFILE_STATUS;
			*(bRespBuffer+3) = PS_CAN_NOT_FOUND;
			*(bRespBuffer+4) = *pCancelProfile;
			*(bRespBuffer+5) = 0;
			(void)QueueMessage(bRespBuffer, 6, APOLLO_MCP);

		}
	}

}

#define MAX_PACKET_LENGTH 150

/*SB
   ************************************************************************
   FUNCTION	: SendLogData
   AUTHOR	: Gary S. Jacobson

   DESCRIPTION	: This function sends the current log data buffer as a
		series of log data packets to the requestor.

   LIMITATIONS	:  This function should be called at background level only.
		This function will disable logging while the transfer is in
		progress and will re-arm the trigger when finished.

   INPUTS	: 
   GLOBALS	:
			bComBuffer	: Incomming message buffer
		  	bRespBuffer	: Outgoing message buffer

   OUTPUTS	:  
   FUNCTIONS CALLED:
			disable
			enable
  			QueueMessage
			memcpy

   PDL:
		Stop tasking
		Disable the log data trigger
		Resume tasking
		Calculate the number of records per packet
		Setup message buffer static header section
		WHILE more records to send
		   copy records into message buffer
		   Send LOG_DATA mesage
		END WHILE
		Enable the log data trigger

			
   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release
     1.3	GSJ		24-Feb-94	Swap LastDest & LastSource
     1.6	GSJ		09-Mar-94	Don't allow recursion into this message! 
  SE**********************************************************************/
static void SendLogData(void)
{
	extern unsigned int wNumLogItems;
	extern unsigned int wNumLogEntries;
	extern int iLogTrigger;
	extern unsigned int * pLogData;
	unsigned char bRespBuffer[MAX_PACKET_LENGTH+LD_DATA+1]; 
	
	register unsigned int i;
	static unsigned int wBytesToSend;
	static unsigned int wRecordSize;
	static unsigned int wRecordsInPacket;
	static unsigned int wNextLogEntry;
    static unsigned char bRequestor;

	if (!fbUploadingLog) {
		//		set_imask_ccr(1);
		iLogTrigger = -1;	/* disable data logging */
		//		set_imask_ccr(0);

		fbUploadingLog = 1;	/* disable recursion into this message handler*/
		wNextLogEntry = 0;
		wRecordSize = wNumLogItems*2;
		wRecordsInPacket = (MAX_PACKET_LENGTH - LD_DATA) / wRecordSize;
		
		bRequestor = bLastSrc;
		
		*bRespBuffer 	 = bRequestor;
		*(bRespBuffer+1) = bSourceAddress;
		*(bRespBuffer+2) = LOG_DATA;
		*(bRespBuffer+LD_REC_SIZE) = (unsigned char)wRecordSize;
		*(unsigned int *)(bRespBuffer+LD_NUM_RECS) = wNumLogEntries;

		if (wNumLogEntries == 0) {
			(void)QueueMessage(bRespBuffer, 6, APOLLO_MCP);
			iLogTrigger = 0;	/* Re-arm log trigger */
			return;
		}
	} else {
		*bRespBuffer 	 = bRequestor;
		*(bRespBuffer+1) = bSourceAddress;
		*(bRespBuffer+2) = LOG_DATA;
		*(bRespBuffer+LD_REC_SIZE) = (unsigned char)wRecordSize;
		*(unsigned int *)(bRespBuffer+LD_NUM_RECS) = wNumLogEntries;
	}

	if (wNextLogEntry < wNumLogEntries) {
		wBytesToSend = wNumLogEntries-wNextLogEntry;		/* Calcualate the number of records in this packet */
		if (wBytesToSend > wRecordsInPacket)			
			wBytesToSend = wRecordsInPacket * wRecordSize;	/* Calculate the number of bytes for FULL packet */
		else
			wBytesToSend *=	wRecordSize;			/* Calculate the number of bytes for PARTIAL packet */ 

		i = wNextLogEntry * wNumLogItems;
		memcpy(bRespBuffer+LD_DATA, pLogData+i, (size_t)wBytesToSend);
		(void)QueueMessage(bRespBuffer, LD_DATA + wBytesToSend, APOLLO_MCP);
		wNextLogEntry += wRecordsInPacket;
	} else {
		iLogTrigger = 0;	/* Re-arm log trigger */
		fbUploadingLog = 0;	/* and enable this message handler */
	}
}

static void SetLogParameters(void)
{
	extern unsigned int fwAnalogSensorBits;
	extern unsigned int fwDigitalSensorBits;
	extern unsigned int wAmps;
	extern unsigned int wNumLogItems;
	extern unsigned int wNumLogEntries;
	extern int iLogTrigger;
	extern volatile unsigned int * pLogItems[];
	extern LOG_PARAMS Log;
	register unsigned int i;
	unsigned char bLogType;
	
	
	//	set_imask_ccr(1);
	iLogTrigger = -1;	/* disable data logging */
	//	set_imask_ccr(0);
	wNumLogEntries = 0;

	bLogType = bComBuffer[SL_LOGTYPE];
	if ( bLogType < MAX_MOTORS) {
		SetMotorLog((unsigned)bLogType);
		iLogTrigger = -1;	/* disable data logging because SetMotorLog enables  logginh */
	}
	
	if (bComBuffer[SL_MODE])
		Log.chLoggingMode  = bComBuffer[SL_MODE];	/* Set the Logging Mode	*/
		
	if (bComBuffer[SL_TRIGGER]) {
		memcpy((void *)&Log.pTriggerAddress, (void *)(bComBuffer+SL_TRIG_ADDR), 4);
		Log.bTriggerCompare = bComBuffer[SL_TRIGGER];
		Log.bTriggerValue   = bComBuffer[SL_TRIG_VALUE];
	}
 	
	
	if (bLogType == USERDEF_LOG) {
		
		Log.bLogType = USERDEF_LOG;

		wNumLogItems = bComBuffer[SL_LOGNUMB];
		
		if (wNumLogItems) 
		{
			/* User Defined Logging */
			if (wNumLogItems > MAX_LOG_ITEMS)
				wNumLogItems = MAX_LOG_ITEMS;
	
			for (i=0; i< wNumLogItems; i++) {
				memcpy((void *)&pLogItems[i], (void *)(bComBuffer+SL_LOG_ADDR+(i*4)),4);
			}
		}
		else
		{
			/* Default Data Log */
			Log.bLogType = DEFAULT_LOG;
			wNumLogItems = 8;
			pLogItems[0] = (volatile unsigned int *)(0xffffffd8); /*GRA0 */
			pLogItems[1] = (volatile unsigned int *)(0xffffffe8); /*GRA1 */
			pLogItems[2] = (volatile unsigned int *)(0xfffffff8); /*GRA2 */
			pLogItems[3] = (volatile unsigned int *)(0xfffffe86); /*GRA3 */
			pLogItems[4] = (volatile unsigned int *)(0xfffffe98); /*GRB4 */
			pLogItems[5] = (volatile unsigned int *)(0xfffffea8); /*GRB5 */
			pLogItems[6] = (unsigned int *) &fwAnalogSensorBits;
			pLogItems[7] = (unsigned int *) &fwDigitalSensorBits;
		}
		pLogItems[wNumLogItems] = NULL;
	}
	
	iLogTrigger = 0;

}

static void GetLogParameters(void)
{
	extern unsigned int fwAnalogSensorBits;
	extern unsigned int fwDigitalSensorBits;
	extern unsigned int wAmps;
	extern unsigned int wNumLogItems;
	extern unsigned int wNumLogEntries;
	extern int iLogTrigger;
	extern volatile unsigned int * pLogItems[];
	extern LOG_PARAMS Log;
	register unsigned int i;
	unsigned char bRespBuffer[MAX_DATA_MSG_LENGTH]; 

	*bRespBuffer 	 = bLastSrc;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = LOG_PARAM_INFO;
	*(bRespBuffer+GL_LOGTYPE) = Log.bLogType;  	/* Set the Logging Type	*/
	if (Log.bLogType != USERDEF_LOG) {
		(void)QueueMessage(bRespBuffer, 4, APOLLO_MCP);
	} else {
		*(bRespBuffer+GL_LOGNUMB) = (unsigned char)wNumLogItems;
		if (wNumLogItems) 
		{
			/* User Defined Logging */
			if (wNumLogItems > MAX_LOG_ITEMS)
				wNumLogItems = MAX_LOG_ITEMS;
	
			for (i=0; i< wNumLogItems; i++) {
				memcpy((void *)(bRespBuffer+GL_LOG_ADDR+(i*4)),(void *)&pLogItems[i],4);
			}
		}

		(void)QueueMessage(bRespBuffer, wNumLogItems*4 + GL_LOG_ADDR, APOLLO_MCP);
	}
}

static void SendSensorLogData(unsigned char bContinuousLogging)
{
	extern unsigned int wHistIndx;
	extern SENSOR_HISTORY *fpSensorHistory;
	extern unsigned int wSensorLogIndx;

#ifdef CONTINUOUS_LOGGING_ENABLED
	extern unsigned int wNumSensorLogEntries;
	extern SENSOR_HISTORY SensorHistoryUploadBuff[];
	extern void SendSensorLogDataPacket(SENSOR_HISTORY *pSensorHist, unsigned int wNumEntries);
#endif // CONTINUOUS_LOGGING_ENABLED

	static unsigned int wBytesToSend;
	static unsigned int wRecordsInPacket;
	static unsigned int wNextLogEntry;
    static unsigned char bRequestor;
	unsigned char bRespBuffer[MAX_PACKET_LENGTH+LD_DATA+1]; 

#ifdef CONTINUOUS_LOGGING_ENABLED
	if (bContinuousLogging)	// check to see if the log is being uploaded 
	{
		SendSensorLogDataPacket(SensorHistoryUploadBuff,wNumSensorLogEntries);
		wNumSensorLogEntries = 0;
		wHistIndx = 0;
		return;
	}
#endif
   
	if (!fbUploadingSensorLog) {
		fbUploadingSensorLog = 1;	/* disable recursion into this message handler*/
		wNextLogEntry = 0;
		wRecordsInPacket = (MAX_PACKET_LENGTH - LD_DATA) / sizeof(SENSOR_HISTORY);
		bRequestor = bLastSrc; 
	}

	*bRespBuffer 	 = bRequestor;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = SENSOR_LOG_DATA;
	*(bRespBuffer+LD_REC_SIZE) = sizeof(SENSOR_HISTORY);
	*(unsigned int *)(bRespBuffer+LD_NUM_RECS) = MAX_HISTORY;

	if (wNextLogEntry < MAX_HISTORY) {
		wBytesToSend = MAX_HISTORY-wNextLogEntry;		/* Calcualate the number of records in this packet */
		if (wBytesToSend > wRecordsInPacket)			
			wBytesToSend = (unsigned) (wRecordsInPacket * sizeof(SENSOR_HISTORY));	/* Calculate the number of bytes for FULL packet */
		else
			wBytesToSend *=	sizeof(SENSOR_HISTORY);			/* Calculate the number of bytes for PARTIAL packet */ 

		memcpy(bRespBuffer+LD_DATA, fpSensorHistory+wNextLogEntry, (size_t)wBytesToSend); 
				
		(void)QueueMessage(bRespBuffer, LD_DATA + wBytesToSend, APOLLO_MCP);
		wNextLogEntry += wRecordsInPacket;
		
	} else {
			/* Reset Event Log */
		wHistIndx = 0;//MAX_HISTORY-1;
		memset(fpSensorHistory, 0, MAX_HISTORY * sizeof(SENSOR_HISTORY));
		fbUploadingSensorLog = 0;	/* and enable this message handler */
	}
}

static void SendTraceLogData(unsigned char bContinuousLogging)
{
	extern unsigned int wTraceHistIndx;
	extern TRACE_HISTORY *fpTraceHistory;
	extern void SendTraceLogDataPacket(TRACE_HISTORY *pTraceHistory, unsigned int wNumEntries);
	extern unsigned int wNumTraceLogEntries;
	extern unsigned int wTraceLogIndx;

#ifdef CONTINUOUS_LOGGING_ENABLED
	extern TRACE_HISTORY TempTraceBuffer[];
	extern unsigned int wTempTraceBuffCount;
#endif // CONTINUOUS_LOGGING_ENABLED

	static unsigned int wBytesToSend;
	static unsigned int wRecordsInPacket;
	static unsigned int wNextLogEntry;
    static unsigned char bRequestor;
	unsigned char bRespBuffer[MAX_PACKET_LENGTH + LD_DATA + 1]; 

#ifdef CONTINUOUS_LOGGING_ENABLED
	if (bContinuousLogging)	// check to see if the log is being uploaded 
	{
		SendTraceLogDataPacket(TempTraceBuffer, wTempTraceBuffCount);
		wTempTraceBuffCount = 0;
		wNumTraceLogEntries = 0;
		return;
	}
#endif // CONTINUOUS_LOGGING_ENABLED

	if (!fbUploadingTraceLog) {
		fbUploadingTraceLog = 1;	/* disable recursion into this message handler*/
		wNextLogEntry = 0;
		wRecordsInPacket = (MAX_PACKET_LENGTH - LD_DATA) / sizeof(SENSOR_HISTORY);
		bRequestor = bLastSrc;
	}

	*bRespBuffer 	 = bRequestor;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = TRACE_HIST_DATA;
	*(bRespBuffer+LD_REC_SIZE) = sizeof(TRACE_HISTORY);
	*(unsigned int *)(bRespBuffer+LD_NUM_RECS) = MAX_TRACE_HISTORY;

	if (wNextLogEntry < MAX_TRACE_HISTORY) {
		wBytesToSend = MAX_TRACE_HISTORY-wNextLogEntry;		/* Calcualate the number of records in this packet */
		if (wBytesToSend > wRecordsInPacket)			
			wBytesToSend = (unsigned)(wRecordsInPacket * sizeof(TRACE_HISTORY));	/* Calculate the number of bytes for FULL packet */
		else
			wBytesToSend *=	sizeof(TRACE_HISTORY);			/* Calculate the number of bytes for PARTIAL packet */ 

		memcpy(bRespBuffer+LD_DATA, fpTraceHistory+wNextLogEntry, (size_t)wBytesToSend);
		(void)QueueMessage(bRespBuffer, LD_DATA + wBytesToSend, APOLLO_MCP);
		wNextLogEntry += wRecordsInPacket;
	} else {
			/* Reset Trace Event Log */
		wTraceHistIndx = 0;
		memset(fpTraceHistory, 0, MAX_TRACE_HISTORY * sizeof(TRACE_HISTORY));
		fbUploadingTraceLog = 0;	/* and enable this message handler */
	}
}



/*SB
   ************************************************************************
   FUNCTION	: CancelAllProfiles
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	: This function will mark all active profiles for cancelation
   LIMITATIONS	: This function should be called at background level only.
   INPUTS	: none
   OUTPUTS	: none 
   FUNCTIONS CALLED:
  			none

   PDL:
		LOOP for all installed profiles
		   IF profile is active THEN
		      Mark profile for cancelation
		END LOOP

   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release

  SE**********************************************************************/
static void CancelAllProfiles(void)
{
	register INSTALLED_PROFILE * ActProf;
	extern INSTALLED_PROFILE InstalledProfile[];

	for (ActProf = InstalledProfile; ActProf < MAX_PROFILES+InstalledProfile;ActProf++) 		
	/* For all Active Profiles */
		if (ActProf->pProfile) {
			ActProf->fbActive = 1;			
			ActProf->fbCancel = 1;		/* if Profile exists and is not marked for cancelation */
	}
}

/*SB
   ************************************************************************
   FUNCTION	: LoopBack
   AUTHOR	: Gary S. Jacobson

   DESCRIPTION	: This function will mirror the incomming message back to the
		sender for diagnoistic purposes.

   LIMITATIONS	: This function should be called at background level only.

   INPUTS	: 	uClass		: received message class
			uLen		: received message length
   GLOBALS	:
			bComBuffer	: Incomming message buffer
   OUTPUTS	:  
   FUNCTIONS CALLED:
  			QueueMessage

   PDL:
		Set the destination to the source id
		Set the source to the destination id
		Send the message that we just received

   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release

  SE**********************************************************************/
static void LoopBack(int uClass, int uLen) 
{
	bComBuffer[0] = bLastSrc;
	bComBuffer[1] = bSourceAddress;
	(void)QueueMessage(bComBuffer, (unsigned)uLen, (unsigned char) uClass);	
}

static void SendSetMonitorModeAck(unsigned char bNewMcpMonitorMode) 
{	
	unsigned char bBuff[3];
	if (bNewMcpMonitorMode != RETRIEVE_MONITOR_MODE)
		bMcpMonitorMode = bNewMcpMonitorMode;
	bBuff[0] = 0x01;
	bBuff[1] = SET_MONITOR_MODE_ACK;
	bBuff[2] = bMcpMonitorMode;
	(void)QueueMessage(bBuff, (unsigned)sizeof(bBuff), (unsigned char) DIAGNOSTIC);	
}
/*SB
   ************************************************************************
   FUNCTION	: StatusReply
   AUTHOR	: Gary S. Jacobson

   DESCRIPTION	: This function will send a status reply for the status request message
   LIMITATIONS	: This function should be called at background level only.

   INPUTS	: none

   GLOBALS	:
			bRespBuffer		: Outgoing message buffer
			uAccelBytesDownloaded	: Accel Table Download in progress if != 0
			uSegsDownloaded		: Profile Download in progress if != 0
   OUTPUTS	:  
   FUNCTIONS CALLED:
  			QueueMessage

   PDL:
		Set the destination to the source id
		Set the source to the destination id
		IF download profile is active
			Send Download Profile Active Status
		ELSE IF download acceleration table is active
			Send Download Accel Table Active Status
		ELSE
			Send Idle Status Message

   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		07-Feb-94	Initial Release
	 1.2	GSJ		10-Mar-99	Fix wrong errormsg return for profile not finished
  SE**********************************************************************/
static void StatusReply(void) 
{
	unsigned char bRespBuffer[10]; 
	bRespBuffer[0] = bLastSrc;
	bRespBuffer[1] = bSourceAddress;
	bRespBuffer[2] = STATUS_REPLY;

	if (uSegsDownloaded)
		bRespBuffer[3] = STATUS_WAIT_FOR_PROFILE;
	else if (uAccelBytesDownloaded)
		bRespBuffer[3] = STATUS_WAIT_FOR_ACCTBL;
	else
		bRespBuffer[3] = STATUS_IDLE;

	(void)QueueMessage(bRespBuffer, 4, APOLLO_MCP);
}

static void ConfigReply(void) 
{
	unsigned char bRespBuffer[80]; 
	extern char Version[];

	bRespBuffer[0] = bLastSrc;
	bRespBuffer[1] = bSourceAddress;
	bRespBuffer[2] = CONFIGURATION;


	strncpy((char *)(bRespBuffer+3), Version, sizeof(bRespBuffer)-4); 

	(void)QueueMessage(bRespBuffer, (unsigned) (4 + strlen(Version)), APOLLO_MCP);
}


static void SendFlagData(void)
{
//	extern unsigned char fbProfileFlag[];

	unsigned char bRespBuffer[MAX_DATA_MSG_LENGTH]; 

	*bRespBuffer 	 = bLastSrc;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = FLAG_STATE_DATA;

	memcpy(bRespBuffer+3, profileFlags.fbProfileFlag, 256);

	(void)QueueMessage(bRespBuffer, 259, APOLLO_MCP);
}
static void SendRawSensors(void)
{
	register int i;
	register unsigned char * pBuff;
	unsigned char bRespBuffer[80]; 

	extern unsigned char bSensorOffsets[];
	extern unsigned char bSensorThreshold[];
	extern unsigned char bAveragedValues[];	


	*bRespBuffer 	 = bLastSrc;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = RAW_SENSOR_DATA;

	for (pBuff = bRespBuffer+3, i=0; i<N_SENSORS; i++)
	{
		*(pBuff++) = bAveragedValues[i];
		*(pBuff++) = bSensorThreshold[i];
		*(pBuff++) = bSensorOffsets[i];
	}

	(void)QueueMessage(bRespBuffer, 51, APOLLO_MCP);
}

/*SB
   ************************************************************************
   FUNCTION	: SensorReply
   AUTHOR	: Gary S. Jacobson

   DESCRIPTION	: This function will send the current state of the sensors as 
		  the reply for the Query Sensors Command
   LIMITATIONS	: This function should be called at background level only.

   INPUTS	: none

   GLOBALS	:
			fbBlockedState		: Sensor Blocked State Flags
			fbSensorDflag		: Digital Sensor Flags [0..7]
			fbSensorFlag		: Analog Sensor Flags [0..15]
			bRespBuffer		: Outgoing message buffer

   OUTPUTS	:  	none
   FUNCTIONS CALLED:
  			QueueMessage

   PDL:
		Set the destination to the source id
		Set the source to the destination id
		
		LOOP through all Digital Sensor Flags
		     IF SensorFlag is equal to Blocked Flag state THEN
			Set sensor bit;
		     ELSE
			Clear sensor bit;
		END LOOP

		LOOP through all Analog Sensor Flags
		     IF SensorFlag is equal to Blocked Flag state THEN
			Set sensor bit;
		     ELSE
			Clear sensor bit;
		END LOOP

		Set sensor bits in outgoing message
		Send Sensor State Reply

	
   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.4	GSJ		24-Feb-94	Initial Release
		GSJ		10-Oct-94	Clear out bBits before use
		GSJ		28-Mar-97	Add 2nd digital sensor byte  
		GSJ		05-Jul-01	Use flag state - flags are 1 for blocked
  SE**********************************************************************/
static void SensorReply(void) 
{
//	extern unsigned char fbSensorDflag[];
//	extern unsigned char fbSensorFlag[];
	unsigned char bRespBuffer[10]; 

	unsigned char bMask;
	register unsigned int	i;
	register unsigned char * bBits;

	bRespBuffer[0] = bLastSrc;
	bRespBuffer[1] = bSourceAddress;
	bRespBuffer[2] = SENSOR_STATES;

	bBits = bRespBuffer+SENSOR_DATA_OFFSET;
	*bBits = 0;
	for (i = 0, bMask = 1; i<8; i++, (bMask <<= 1))
		if (profileFlags.flags.fbSensorDflag[i])
			*bBits |= bMask;
	*(++bBits) = 0;			 
	for (i = 8, bMask = 1; i<16; i++, (bMask <<= 1))
		if (profileFlags.flags.fbSensorDflag[i])
			*bBits |= bMask;
	*(++bBits) = 0;
	for (i = 0, bMask = 1; i<8; i++, (bMask <<= 1))
		if (profileFlags.flags.fbSensorFlag[i])
			*bBits |= bMask;
	*(++bBits) = 0;

	for (i = 8, bMask = 1; i<16; i++, (bMask <<= 1))
		if (profileFlags.flags.fbSensorFlag[i])
			*bBits |= bMask;
	
	
	(void)QueueMessage(bRespBuffer, 7, APOLLO_MCP);
}

/*SB
   ************************************************************************
   FUNCTION	: SendDeviceId
   AUTHOR	: Gary S. Jacobson

   DESCRIPTION	: This function will send the reply to the Device Query Id message.
   LIMITATIONS	: This function should be called at background level only.

   INPUTS	: none

   GLOBALS	:
			bComBuffer		: Incomming message buffer
			bRespBuffer		: Outgoing message buffer
			bMicroId		: MCP Board ID nubmer
			bSourceAddress		: Our Session layer address

   OUTPUTS	:  
   FUNCTIONS CALLED:
  			QueueMessage

   PDL:
		Set the destination to the source id
		Set the source to the MicroId
		Send Device Id Reply


   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.2	GSJ		14-Feb-94	Initial Release
     1.6	GSJ		07-Mar-94	Save source address
 SE**********************************************************************/
static void SendDeviceId(unsigned mode) 
{
	extern unsigned char bMicroId;
	unsigned char bRespBuffer[10]; 
    
	if (mode == APOLLO_MODE) {
		bSourceAddress = (unsigned char) (bMicroId + MCP_TASK_ID_OFFSET);
		bRespBuffer[0] = bComBuffer[1];
		bRespBuffer[1] = (unsigned char) (bMicroId + MCP_TASK_ID_OFFSET);
		bRespBuffer[2] = QUERY_ID_REPLY;
	
		(void)QueueMessage(bRespBuffer, 3, DEVICE_QUERY);
	} else {
		bRespBuffer[0] = QUERY_ID_REPLY;
		(void)QueueMessage(bRespBuffer, 1, DEVICE_QUERY); 
	}
}


static void SetCancelMsgFlag(void)
{
	extern unsigned char fbSendProfileCancelMsg;

	fbSendProfileCancelMsg = *(bComBuffer+CM_SEND_MSG);
}

static void SetRemoteFlag(void)
{
//	extern unsigned char fbProfileFlag[];
	register unsigned char * pFlagInfo;
	
	for (pFlagInfo = bComBuffer+SF_FLAG_DATA; *pFlagInfo != 0xFF; pFlagInfo += SF_FLAG_INFO_SIZE)
		profileFlags.fbProfileFlag[pFlagInfo[SF_FLAG_NUMBER]] = pFlagInfo[SF_FLAG_STATE];
}

void ResetFlags(void)
{
	extern unsigned char fbUserFlags[];

	memset(profileFlags.flags.fbUserFlags, 0, 196);	/* Don't touch motor & sensor flags */

}
static void CancelGroup(void)
{
	extern unsigned char fbSendProfileCancelMsg;
	extern INSTALLED_PROFILE InstalledProfile[];
	register INSTALLED_PROFILE * pInstProf;
	register unsigned char *pCancelProfile;
	unsigned char bRespBuffer[10]; 

	/*	Loop through all profiles in packet and setup the installed profile data structure */
	for (pCancelProfile = bComBuffer+CG_PROFILE; *pCancelProfile != 0xFF; pCancelProfile++) {
		pInstProf = &InstalledProfile[*pCancelProfile];
		if (pInstProf->pProfile) {	
			pInstProf->fbActive = 1;	// Activate Profile
			pInstProf->fbCancel = 1;	// so we can cancel it
		} else if (fbSendProfileCancelMsg & SEND_CANCEL_PROFILE_MSG){
			*bRespBuffer 	 = bLastSrc;
			*(bRespBuffer+1) = bSourceAddress;
			*(bRespBuffer+2) = PROFILE_STATUS;
			*(bRespBuffer+3) = PS_CAN_NOT_FOUND;
			*(bRespBuffer+4) = *pCancelProfile;
			*(bRespBuffer+5) = 0;
			(void)QueueMessage(bRespBuffer, 6, APOLLO_MCP);

		}
	}
	
	if (fbSendProfileCancelMsg & SEND_CANCEL_GROUP_MSG) {	
		*bRespBuffer 	 = bLastSrc;
		*(bRespBuffer+1) = bSourceAddress;
		*(bRespBuffer+2) = GROUP_CANCEL_REPLY;
		*(bRespBuffer+3) = *(bComBuffer+CG_GROUP);
		(void)QueueMessage(bRespBuffer, 4, APOLLO_MCP);
	}
}

/*SB
   ************************************************************************
   FUNCTION	: DoFlashDownload
   AUTHOR	: Gary S. Jacobson

   DESCRIPTION	: This function will download the flash image into ram and 
		  reprogram the flash memory.	
   LIMITATIONS	: This function will never exit !
   INPUTS	: 	uPort		: Serial port to use
   GLOBALS	:
   OUTPUTS	:  
   FUNCTIONS CALLED:
		StopWatchdogTimer
		SwitchStack
   PDL:		
		Stop the Watchdog
		Switch to internal ram stack
		Save Flash Image Checksum
		Clear out the ram heap & stack to use for the flash image
		Set the flash image to 0xFF's
		LOOP
			Get a S-record buffer
			Verify S-record checksum
			Store data in Flash Image Ram
		WHILE not at end S-record
		Calculate Flash Image Checksum
		if Checksum is correct
			Erase Flash Memory
			Program Flash Memory
			Reset System
		else
			Reset System

		
   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		7-Aug-97	Initial Release

  SE**********************************************************************/
#define FLASH_IMAGE_START (unsigned char *)(0x0260000)
#define FLASH_IMAGE_LENGTH 0x20000L
#define MAX_FLASH	   0x20000L
#define NAK    0x15                /* Negative acknowledgement control char.*/
#define ACK    0x06                /* Acknowledgement control character.    */

unsigned int wFlashImageChecksum;
static FILE * fin, * fout;
static unsigned int LoadFlashImage(void);
static void SetFlashImage(unsigned char fill, unsigned long length);
static unsigned Cvt_Buff_To_Binary(unsigned char * in, unsigned char * out);
extern void ProgramFlash(void * Start, unsigned long length);
extern jmp_buf _init_env;
 
#define auxin       (&_iob[3])          /* standard error output file */
#define auxout      (&_iob[4])          /* standard output file */
#define aux2in      (&_iob[5])          /* standard error output file */
#define aux2out     (&_iob[6])          /* standard output file */

void DoFlashDownload(int port)
{
#ifdef FLASH_PROGRAM_SERIAL
	StopWatchdogTimer();
	if (port) {
		fin = stdin;
		fout = stdout;
	} else {
		fin = auxin;
		fout = auxout;
		memcpy(&wFlashImageChecksum,(bComBuffer+FD_CHECKSUM),2); 
	}

	set_imask_ccr(1);						/* Disable all interrupts */

	SetFlashImage(0xFF, FLASH_IMAGE_LENGTH);
	if (wFlashImageChecksum == LoadFlashImage()) {
		ProgramFlash(FLASH_IMAGE_START,  FLASH_IMAGE_LENGTH);
		/* we never get here..... */
		return;
	}
	*POWER_FAIL_TEST = BAD_FLASH_CKSUM;
	longjmp(_init_env,1);

/*	StartWatchdogTimer(WATCH_DOG_TIMEOUT);
	while(1); 	 Wait for reset 
*/
#endif
}

static void SetFlashImage(unsigned char fill, unsigned long length)
{
	register unsigned long i;
	register unsigned char * FlashRamImage = FLASH_IMAGE_START;

	for (i=0; i<length; i++)
		FlashRamImage[i]=0xFF;
}

static unsigned Cvt_Buff_To_Binary(unsigned char * in, unsigned char * out)
{
	register unsigned char tmp;
	register unsigned cksum = 1;

	while (*in) {
		tmp = (unsigned char) (*in++ - (unsigned)'0');
		if (tmp>9) tmp -= 7;
		*out = (unsigned char)(tmp << 4);
		tmp = (unsigned char) (*in++ - (unsigned)'0');
		if (tmp>9) tmp -= 7;
		*out += tmp;
		cksum += *out++;
	}
	return(cksum&0x00ff);
}

static unsigned int LoadFlashImage(void)
{
	register long i;
	register unsigned int chksum;
	unsigned char rec[80];
	unsigned char hex[40];

	unsigned char * pRam = FLASH_IMAGE_START;

	int done=0;
	unsigned bytecnt;
	unsigned long address;

	while (!done) {
	  (void)fgets((char *)rec,80,fin);
	  if (GREEN_LED_PORT & GREEN_LED)
		GREEN_LED_PORT &=~GREEN_LED;
	  else
		GREEN_LED_PORT |=GREEN_LED;
	  if (*rec)
		return 0;
/*	  rec[strlen(rec)-1] = 0;  Null terminate eol char */
	  if (toupper(rec[0]) != 'S') {
		(void)putc(NAK, fout);
		continue;
	  }
	  if (Cvt_Buff_To_Binary(rec+2, hex)) {
		(void)putc(NAK, fout);
		continue;
	  }
	  bytecnt = hex[0];
	  switch (rec[1]) {
		case '1' : /* 16 bit address & data record */
			address = (((unsigned long) hex[1]) << 8) + hex[2];
			bytecnt = hex[0] - 3;
			bcopy((char *)FLASH_IMAGE_START + address, (char *)hex + 3, bytecnt);
			break;
		case '2' : /* 24 bit address & data record */
			address = (((unsigned long)hex[1]) << 16) + (((unsigned long)hex[2]) << 8) + hex[3];
			bytecnt = hex[0] - 4;
			bcopy((char *)FLASH_IMAGE_START + address, (char *)hex + 4, bytecnt);
			break;
		case '3' : /* 32 bit address & data record */
			address = (((unsigned long)hex[1]) << 24) + (((unsigned long)hex[2]) << 16) + 
				  (((unsigned long)hex[3]) << 8) + hex[4];
			bytecnt = hex[0] - 5;
			bcopy((char *)FLASH_IMAGE_START + address, (char *)hex + 5, bytecnt);
			break;
		case '8' : /* End of file record */
		case '9' : /* End of file record */
			done = 1;
			break;
		case '0' : /* Sign on record  */
		case '7' : /* Start Address Field */
			break;
		default :
			(void) putc(NAK, fout);
			continue;
	   }
	   (void) putc(ACK, fout);
	}
	for (i=0, chksum = 0; i < FLASH_IMAGE_LENGTH; i++)
		chksum += pRam[i];
	for (i= FLASH_IMAGE_LENGTH; i< MAX_FLASH; i++)
		chksum += 0xFF;
	GREEN_LED_PORT &= ~GREEN_LED;
	return chksum; 
}	


void EraseFlashProfiles(void)
{
	int ok;
#ifndef EMULATOR
#ifdef FLASH_PROFILES_USED
	set_imask_ccr(1);
	StopWatchdogTimer();
	ok = EraseFlashSector(FLASH_SAVE_AREA); /* && EraseFlashSector((unsigned char *)0x1C000); */
	set_imask_ccr(0);
	StartWatchdogTimer(WATCH_DOG_TIMEOUT);
	if (!ok)     
#endif
		LogError(INFO_ERROR, ERR_FLASH_PROF_ERASE, 0, 0);
#endif
}

	
void SaveFlashProfiles(void)
{
	int ok;
	unsigned char bRespBuffer[10]; 
#ifndef EMULATOR
#ifdef FLASH_PROFILES_USED
	set_imask_ccr(1);
	StopWatchdogTimer();
	ok = EraseFlashSector(FLASH_SAVE_AREA); /* && EraseFlashSector((unsigned char *)0x1C000);*/
	if (!ok) {
		set_imask_ccr(0);
		StartWatchdogTimer(WATCH_DOG_TIMEOUT);
		LogError(INFO_ERROR, ERR_FLASH_PROF_ERASE, 0, 0);
		return;
	}
	WriteFlashByte(FLASH_SAVE_AREA,0x55);
	SetFlashPointer(FLASH_SAVE_AREA+2);
	if (!SaveAccelTables()) {
		set_imask_ccr(0);
		StartWatchdogTimer(WATCH_DOG_TIMEOUT);
		LogError(INFO_ERROR, ERR_FLASH_PROF_SAVE, 1, 0);
		return;
	}
	if (!SaveMaintTables()) {
		set_imask_ccr(0);
		StartWatchdogTimer(WATCH_DOG_TIMEOUT);
		LogError(INFO_ERROR, ERR_FLASH_PROF_SAVE, 1, 0);
		return;
	}
	if (!SaveProfiles()) {
		set_imask_ccr(0);
		StartWatchdogTimer(WATCH_DOG_TIMEOUT);
		LogError(INFO_ERROR, ERR_FLASH_PROF_SAVE, 2, 0);
		return;
	}
	if (!SaveSensors()) {
		set_imask_ccr(0);
		StartWatchdogTimer(WATCH_DOG_TIMEOUT);
		LogError(INFO_ERROR, ERR_FLASH_PROF_SAVE, 3, 0);
		return;
	}
	if (!SaveMotors()) {
		set_imask_ccr(0);
		StartWatchdogTimer(WATCH_DOG_TIMEOUT);
		LogError(INFO_ERROR, ERR_FLASH_PROF_SAVE, 4, 0);
		return;
	}
	if (!SaveProcessorConfig()) {
		set_imask_ccr(0);
		StartWatchdogTimer(WATCH_DOG_TIMEOUT);
		LogError(INFO_ERROR, ERR_FLASH_PROF_SAVE, 5, 0);
		return;
	}
	if (!SaveProfileStart(bComBuffer+START_ADDRESS)) {
		set_imask_ccr(0);
		StartWatchdogTimer(WATCH_DOG_TIMEOUT);
		LogError(INFO_ERROR, ERR_FLASH_PROF_SAVE, 6, 0);
		return;
	}
	if (!SaveProfileConfig()) {
		set_imask_ccr(0);
		StartWatchdogTimer(WATCH_DOG_TIMEOUT);
		LogError(INFO_ERROR, ERR_FLASH_PROF_SAVE, 7, 0);
		return;
	}
	FlushWriteBuff();
	set_imask_ccr(0);
	
	*bRespBuffer 	 = bLastSrc;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = FLASH_PROFILES_SAVED;

	(void)QueueMessage(bRespBuffer, 3, APOLLO_MCP);

	StartWatchdogTimer(WATCH_DOG_TIMEOUT);
#else
		LogError(INFO_ERROR, ERR_FLASH_PROF_ERASE, 0, 0);
#endif
#endif
}

void ResetCounters(void)
{
	extern unsigned wCounters[];  /* Counter array for profile executor */
	
	memset(wCounters, 0, MAX_COUNTERS * 2);
}

void SetCounter(void)
{
	extern unsigned wCounters[];  /* Counter array for profile executor */
	unsigned char bCounterIndex;
	
	
	
	bCounterIndex = *(bComBuffer+CT_COUNTER);
/*
		Use byte moves because int may be at odd address.
*/
	if (bCounterIndex < MAX_COUNTERS)
		memcpy(&wCounters[bCounterIndex], (bComBuffer+CT_VALUE), sizeof(int));	

}

void RequestCounter(void)
{
	extern unsigned wCounters[];  /* Counter array for profile executor */
	unsigned char bCounterIndex;
	unsigned char bRespBuffer[10]; 
	
		
	bCounterIndex = *(bComBuffer+CT_COUNTER);
	
	*bRespBuffer 	 = bLastSrc;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = COUNTER_DATA;
	*(bRespBuffer+3) = bCounterIndex;
/*
		Use byte moves because int may be at odd address.
*/
	memcpy((bRespBuffer+4), &wCounters[bCounterIndex], sizeof(int));	
	
	(void)QueueMessage(bRespBuffer, 6, APOLLO_MCP);
}

void RequestMultiCounter(void)
{
	extern unsigned wCounters[];  /* Counter array for profile executor */
	unsigned char bCounterIndex;
	unsigned char bNumberOfBytes; 
	unsigned char bRespBuffer[MAX_DATA_MSG_LENGTH]; 
		
	bCounterIndex  = *(bComBuffer+CT_COUNTER);
	bNumberOfBytes = (unsigned char) (*(bComBuffer+CT_NUMB) * sizeof(int));
	
	*bRespBuffer 	 = bLastSrc;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = MULTIPLE_COUNTER_DATA;
	*(bRespBuffer+3) = bCounterIndex;
/*
		Use byte moves because int may be at odd address.
*/
	memcpy((bRespBuffer+4), &wCounters[bCounterIndex], (size_t)bNumberOfBytes);	
	
	(void)QueueMessage(bRespBuffer, (unsigned) (4 + bNumberOfBytes), APOLLO_MCP);
}
void SetMultiCounter(void)
{
	extern unsigned wCounters[];  /* Counter array for profile executor */
	unsigned char bCounterIndex;
	unsigned char bNumberOfBytes; 
	
	
	
	bCounterIndex = *(bComBuffer+CT_COUNTER);
	bNumberOfBytes = (unsigned char) (*(bComBuffer+CT_NUMB) * sizeof(int));
	
/*
		Use byte moves because int may be at odd address.
*/
	if ((bCounterIndex + bComBuffer[CT_NUMB]) < MAX_COUNTERS)
		memcpy(&wCounters[bCounterIndex], (bComBuffer+CT_VALUES), (size_t)bNumberOfBytes);	

}
static unsigned bDiagDest;
static unsigned bDiag;

void StartDiagnostics(void)
{

	unsigned char bQual;
	
	bDiag =  *(bComBuffer + DIAG_CMD);
	bQual =  *(bComBuffer + DIAG_QUAL);
	
	bDiagDest = bLastSrc; /* Save source of request */
	
	switch (bDiag) {
		case SERVO_STEP_RESPONSE : 	SetupServoStepDiag(bQual, bComBuffer + DIAG_DATA); break;
		case SERVO_SET_FILTER :		SetServoFilter(bQual, bComBuffer + DIAG_DATA); break;
		case SERVO_GET_FILTER :		GetServoFilter(bQual); break;
		case SERVO_SET		  :		SetServo(bQual,bComBuffer + DIAG_DATA); break;
		case SERVO_MOTOR_KT_TEST :	MotorKtTest(bQual,bComBuffer + DIAG_DATA); break;			
		case SERVO_GET_ENCODER :	GetEncoderCounts(bQual); break;
		case SENSOR_CALIBRATION :	AutoCalibrateSensorGain(bQual, TRUE); break;
		case SENSOR_BURNTHRU :		AutoBurnThruSensorGain(bQual, TRUE); break;
		case SENSOR_BIN :			AutoBinDD_Sensor(TRUE); break;
		default :
			SendDiagResponse(DIAG_ERROR, DIAG_ERROR_UKNOWN_DIAG, 0, NULL);
	}
}

void PollDiagnositcs(void)
{
	bDiagDest = bLastSrc; /* Save source of request */
	SendPolledDiagStatus();
}

void SendDiagResponse(unsigned char bStatus, unsigned char bError, unsigned char bCount, unsigned char * pData)
{
	unsigned char bDiagResp[MAX_DATA_MSG_LENGTH];

	*bDiagResp     = (unsigned char) bDiagDest;
	*(bDiagResp+1) = bSourceAddress;
	*(bDiagResp+2) = DIAG_STATUS;
	*(bDiagResp+3) = bStatus;
	*(bDiagResp+4) = bError;
	*(bDiagResp+5) = (unsigned char) bDiag;
	*(bDiagResp+6) = bCount;
	if (bCount)
		memcpy((bDiagResp+7), pData, (size_t)bCount);	
	(void)QueueMessage(bDiagResp, (unsigned) (7+bCount), APOLLO_MCP);
}


static void PCN_Reply(void) 
{
	extern unsigned char bMicroId;
	unsigned char bRespBuffer[80]={0}; 
                    
	bRespBuffer[0] = PCN_REPLY;
	strcpy((char *)&bRespBuffer[1], (char *)"DM400C");	
	(void)QueueMessage(bRespBuffer, (unsigned) (2 + strlen((char *)"DM400C")), DEVICE_QUERY);
}

static void ModifyAckTimeout(void)
{
	int time;
	extern PROC_CFG ProcessorConfig;
	unsigned char bRespBuffer[10]; 
	
	time =  bComBuffer[1] + (bComBuffer[2] << 8);		// Convert from little endian to BIG ENDIAN
	if (time) {
		ProcessorConfig.wLinkPacketTimeout = time >> 1;		// Convert from ms to ticks (assume 2 ms/tick)
		bRespBuffer[1] = bComBuffer[1];
		bRespBuffer[2] = bComBuffer[2];
	} else {
		time = ProcessorConfig.wLinkPacketTimeout << 1;  
		bRespBuffer[1] = (unsigned char)(time & 0xFF);
		bRespBuffer[2] = (unsigned char) (time >> 8);
	}
	bRespBuffer[0] = ACK_TIMEOUT_REPLY;
	(void)QueueMessage(bRespBuffer, 3, DEVICE_QUERY);
}

static void SendClassReply(void)
{       
	unsigned char bRespBuffer[10]; 
	bRespBuffer[0] = CLASS_REPLY; 
	bRespBuffer[1] = bComBuffer[1] == APOLLO_MCP ? 'Y' : 'N';
	(void)QueueMessage(bRespBuffer, 2, DEVICE_QUERY);
}

static void SendGeneralClassReply(void)
{
	unsigned char bRespBuffer[10]; 

	bRespBuffer[0] = GENERAL_CLASS_REPLY; 
	bRespBuffer[1] = APOLLO_MCP;
	(void)QueueMessage(bRespBuffer, 2, DEVICE_QUERY);
}

static void SetBaudRate(void)
{ 
	unsigned char bRespBuffer[10]; 
	int baudrate[] = {9600,19200,38400}; 
	static int baud;	

	if (!fbChangingBaudRate) {  
		baud = bRespBuffer[1];
		if (baud > 2)
			baud = 1;
	    fbChangingBaudRate = TRUE;
		bRespBuffer[0] = BAUDRATE_REPLY; 
		bRespBuffer[1] = (unsigned char)baud;
		(void)QueueMessage(bRespBuffer, 2, DEVICE_QUERY); 
	} else {
		//initialize_sio (0, baudrate[baud], 0, 1);	
		fbChangingBaudRate = FALSE;
	}
}

void InitializeNVM(void)
{
#ifndef DM400C_FDR
	 (void) InitNVM();
	 (void) InitPcnNVM();
	 (void) InitNVM_CycleCounters();
	 (void) InitNVM_JamCounters();
	 (void) InitNVM_ErrorLog();
#endif
}

static void SendNvmCycleCounts(void)
{
	unsigned char bRespBuffer[10]={0}; 
	//extern NVM_CYCLE_CNTR_REC NvmCntr;
    //extern NVM_REC Nvm; 

	*bRespBuffer 	 = bLastSrc;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = NVM_CYCLE_CNTR_DATA;
/*
		Use byte moves because longs may be at odd address.
*/
	//memcpy((bRespBuffer+3), &NvmCntr, sizeof(NVM_CYCLE_CNTR_REC)-2);	// 6 longs = 24 bytes
	(void)QueueMessage(bRespBuffer, 31, APOLLO_MCP);
}

static void SendNvmData(void)
{
	unsigned char bRespBuffer[sizeof(NVM_REC)+6]={0}; 
    //extern NVM_REC Nvm; 

	*bRespBuffer 	 = bLastSrc;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = NVM_REC_DATA;
/*
		Use byte moves because longs may be at odd address.
*/
	//memcpy((bRespBuffer+3), &Nvm, sizeof(NVM_REC)-2);	
	(void)QueueMessage(bRespBuffer, sizeof(NVM_REC)-2+3, APOLLO_MCP);

}
	
static void SaveCycleCountData(void)
{
#ifndef DM400C_FDR
	//extern NVM_CYCLE_CNTR_REC NvmCntr;
    //extern NVM_REC Nvm; 
	//extern unsigned char NvmCntrSaveRequired;
	//extern unsigned char NvmSaveRequired;
	//unsigned char bRespBuffer[ sizeof(NVM_CYCLE_CNTR_REC)+6]={0}; 

/*
		Use byte moves because longs may be at odd address.
*/
	//memcpy(&NvmCntr, (bComBuffer+3), sizeof(NVM_CYCLE_CNTR_REC)-2);	// 6 longs = 24 bytes
//	memcpy(&Nvm.WasteTankVolume, (bComBuffer+27), 4);
	//NvmCntrSaveRequired=TRUE;
	//NvmSaveRequired=TRUE;
#endif
}	

static void SaveNvmData(void)
{
#ifndef DM400C_FDR
    extern NVM_REC Nvm; 
	extern unsigned char NvmSaveRequired;
/*
		Use byte moves because longs may be at odd address.
*/
	memcpy(&Nvm, (bComBuffer+3), sizeof(NVM_REC)-2);	// no sig and cksum
    NvmSaveRequired = TRUE;
#endif
}

static void SaveGapParameters(void)
{
#ifndef DM400C_FDR
//	extern GAP_PARAM_REC GapCalc;
/*
		Use byte moves because longs may be at odd address.
*/
//	memcpy(&GapCalc, (bComBuffer+3), sizeof(GAP_PARAM_REC));
#endif
}

void SendPowerDownRequest(void)
{
	extern unsigned char bMicroId;  
	unsigned char bRespBuffer[10]; 
	
	*bRespBuffer 	   = bSystemControlAddress;
	*(bRespBuffer+1) = (unsigned char) (bMicroId + MCP_TASK_ID_OFFSET);
 	*(bRespBuffer+2) = POWER_DOWN_REQUEST;
	(void)QueueMessage(bRespBuffer, 3, APOLLO_MCP);
}

static void DoPowerDown(void)
{
	extern unsigned char fbResumePowerDown;
	
	fbResumePowerDown = TRUE;
}


static void ProcessSleepModeCmd(void)
{    
	extern unsigned char fbSleepModeActive;
	unsigned char ModeRequest;
	
	ModeRequest = bComBuffer[3];
	
	if (ModeRequest) { 		// Request to enter sleep mode
		if ((MTR_EBL & MTR_EBL_BITS_MASK) != MOTORS_OFF) {
			LogError(INFO_ERROR, ERR_SLEEP_MODE_NOT_IDLE, PE.DR.BYTE,0);
			return;
		}
		
		if (ModeRequest & ~(SLEEP_PMC|SLEEP_SENSORS)) {
			LogError(INFO_ERROR, ERR_SLEEP_MODE_ERROR, ModeRequest,0);
			return;
		}					

	    fbSleepModeActive = ModeRequest; 

	    if (ModeRequest & SLEEP_PMC)
		    PowerDownPMC();   

		if (ModeRequest & SLEEP_SENSORS)
			PowerDownSensors();
		
	} else {
	    if (fbSleepModeActive & SLEEP_PMC)
		    PowerUpPMC();           
		    
		if (fbSleepModeActive & SLEEP_SENSORS)
			PowerUpSensors();
			
		fbSleepModeActive = SLEEP_WAKEUP;
	}
}                  


static void SendJamCounterData(void)
{
	//extern NVM_JAM_COUNTERS_REC NvmJams; 
	unsigned char bRespBuffer[sizeof(NVM_JAM_COUNTERS_REC)+6]={0}; 
	
	*bRespBuffer 	 = bLastSrc;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = JAM_COUNTER_DATA;
/*
		Use byte moves because longs may be at odd address.
*/
	//memcpy((bRespBuffer+3), &NvmJams, sizeof(NVM_JAM_COUNTERS_REC)-2);	// don't copy sig and cksum
	(void)QueueMessage(bRespBuffer,  sizeof(NVM_JAM_COUNTERS_REC)-2+3, APOLLO_MCP);
}


static void SaveJamCounterData(void)
{
#ifndef DM400C_FDR
    extern unsigned char NvmJamSaveRequired;
/*
		Use byte moves because longs may be at odd address.
*/
	memcpy(&NvmJams, (bComBuffer+3), sizeof(NVM_JAM_COUNTERS_REC)-2);	// no sig and cksum
    NvmJamSaveRequired = TRUE;
#endif
}
 

static void RequestNvmPcnInfo(void)
{  
	unsigned char bRespBuffer[sizeof(NVM_PCN_REC)+4]; 
	*bRespBuffer 	 = bLastSrc;
	*(bRespBuffer+1) = bSourceAddress;
	*(bRespBuffer+2) = NVM_PCN_INFO_DATA; 
	*(bRespBuffer+3) = 0; 
	
/*
		Use byte moves because longs may be at odd address.
*/ 

	memcpy((bRespBuffer+4), &PcnInfo, sizeof(NVM_PCN_REC)-2);	// don't copy crc
			
	(void) QueueMessage(bRespBuffer,  sizeof(NVM_PCN_REC)-2 + 4, APOLLO_MCP);

}
static void SaveNvmPcnInfo(void)
{
#ifndef DM400C_FDR
	extern unsigned char NvmPcnInfoSaveRequired;	

	memcpy(&PcnInfo, (bComBuffer+4), sizeof(NVM_PCN_REC)-2);	// don't copy crc
	NvmPcnInfoSaveRequired = TRUE;
#endif
}


void HandleCommMessages(void)
{   
	while (( fbUploadingLog || fbUploadingSensorLog || fbUploadingTraceLog) && (TransmitQueueLength() < TRANSMIT_QUEUE_SIZE/2))
	{
		if (fbUploadingLog)
			SendLogData();
		else if(fbUploadingSensorLog)
			SendSensorLogData(0);
		else if(fbUploadingTraceLog)
			SendTraceLogData(0);
	}

	
}
		
/*SB
   ************************************************************************
   FUNCTION	: MessageHandler
   AUTHOR	: Gary S. Jacobson

   DESCRIPTION	: This function is the application layer message dispatacher.
		This function is called in the main() loop at background level.

   LIMITATIONS	:  This function must be called frequently enough to de-queue
		the link layer messages.  This function should be called at
		background level only!  This function calls other functions that
		MUST be called at background level.  This function MUST NOT BE
		CALLED AT INTERRUPT LEVEL!
   
   INPUTS	: none
   GLOBALS	:
			bComBuffer	: Incomming message buffer
			bLastDest	: Destination ID
			bLastSrc	: Source ID

   OUTPUTS	: none
   FUNCTIONS CALLED:
			GetReceiveFrame
  			DownLoadProfile
			DownLoadAccelTable
			StartProfile
			ProfileCancel
			CancelAllProfiles
			ExamineMemory
			SendLogData
			ModifyMemory
			LoopBack
			SensorReply
			StatusReply
			SendDeviceId
			DownLoadSCB
			DownLoadMCB
			DownLoadProcessorCfg
			SetCancelMsgFlag

   PDL:
		Call GetReceiveFrame
		IF there is a message THEN
		   IF message class is APOLLO_MCP THEN
		      Save the Destination ID
		      Save the Source ID
		      Dispatch to Message Handler for each apollo mcp message type
		   IF message class is DIAGNOSTIC THEN
		      Save the Destination ID
		      Save the Source ID
		      Dispatch to Message Handler for each diagnostic message type	
		ELSE IF we are uploading the log data AND there is room in the xmit queue THEN
		   Send the next log data buffer

   REVISION HISTORY:                                      
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ---------------------------- 
     1.0	GSJ		24-Jan-94	Initial Release
     1.2	GSJ		14-Feb-94	Add Query ID message
     1.3	GSJ		16-Feb-94	Add Download MCB message & Download Processor Cfg
     				24-Feb-94	Swap LastDest & LastSource
     1.4	GSJ		24-Feb-94	Add Sensor Download message
     1.5	GSJ		01-Mar-94	Add Initialize Link message
     1.6	GSJ		11-Mar-94	Don't let the upload of log data use all of the xmit buffers
  SE**********************************************************************/
static void MessageHandler(void)
{
	unsigned int uLen;
	unsigned char uClass;
	register unsigned char * pBuff = bComBuffer;
	extern PROC_CFG ProcessorConfig;
	extern SENSOR_HISTORY *fpSensorHistory;
	
	if (GetMessage(pBuff, &uLen, &uClass)) 	/* If there is a message to process then...*/
	{
		if (uClass == APOLLO_MCP) {
			bLastDest = *pBuff++;
			bLastSrc  = *pBuff++;
			switch (*(pBuff)) {
				case SET_REMOTE_FLAG	:
							SetRemoteFlag();
							break;
				case CANCEL_MESSAGE_CMD :
							SetCancelMsgFlag();
							break;
				case CANCEL_GROUP	:
							CancelGroup();
							break;

				case EMERGENCY_STOP :
							CancelAllProfiles();
							break;
				case RESET_ALL_FLAGS	:
							KickWatchdog();
							ResetFlags();
							break;
				case START_PROFILE : 
							StartProfile();
							break;
				case CANCEL_PROFILE :
							ProfileCancel();
							break;
				case REQUEST_STATUS :
							StatusReply();
							break;
				case REQUEST_RAW_SENSORS :
							SendRawSensors();
							break;
				case QUERY_SENSORS :
							SensorReply();
							break;
				case PROFILE_DOWNLOAD :
							StopWatchdogTimer();
							DownLoadProfile((int)uLen);
							break;
				case ACCEL_TABLE_DNLD :
							StopWatchdogTimer();
							DownLoadAccelTable((int)uLen);
							break;
				case MCB_DOWNLOAD :
							StopWatchdogTimer();
							DownLoadMCB();
							StartWatchdogTimer(WATCH_DOG_TIMEOUT);
							break;
				case SCB_DOWNLOAD :
							StopWatchdogTimer();
							DownLoadSCB();
							StartWatchdogTimer(WATCH_DOG_TIMEOUT);
							break;
				case SET_LOG_PARAM :
							if (!fbUploadingLog)
								SetLogParameters();
							break;

				case EXAMINE_MEMORY :	
							ExamineMemory();
							break;
				case REQUEST_LOG_DATA :
							if (!fbUploadingLog)
								SendLogData();
							break;

				case REQUEST_SENSOR_LOG_DATA :							
							if (!fbUploadingSensorLog)
								SendSensorLogData(*(pBuff + 1));
							break;

				case REQUEST_PROF_HIST_TRACE :
							if (!fbUploadingTraceLog)
								SendTraceLogData(*(pBuff + 1));
							break;
				case MODIFY_MEMORY :
							ModifyMemory();
							break;
				case LOOPBACK_COMMAND :
							LoopBack((int)uClass, (int)uLen);
							break;

				case PROCESSOR_CFG_DOWNLOAD :
							DownLoadProcessorCfg();
							break;

				case REQUEST_CONFIG :	
							ConfigReply();
							break;

				case REQUEST_FLAG_STATES :
							SendFlagData();
							break;

				case GET_LOG_PARAM	:
							if (!fbUploadingLog)
								GetLogParameters();
							break;
#ifdef FLASH_PROFILES_USED
				case FLASH_DOWNLOAD_CMD :
								DoFlashDownload(0);
								break;
								
				case FLASH_SAVE_PROFILES :
							SaveFlashProfiles();
							break;
							
				case ERASE_SAVED_PROFILES :
							EraseFlashProfiles();
							break;
#else
				case FLASH_DOWNLOAD_CMD :
				case FLASH_SAVE_PROFILES :
				case ERASE_SAVED_PROFILES :
							break;							
#endif
				case RESET_COUNTERS :
							ResetCounters(); 
							break;
				case SET_COUNTER:
							SetCounter();
							break;
				case REQUEST_COUNTER:
							RequestCounter();
							break;
				case REQUEST_MULT_COUNTER :	
							RequestMultiCounter();
							break;  
				case SET_MULT_COUNTER:
							SetMultiCounter();
							break;
				case START_DIAGNOSTICS :
							StartDiagnostics();
							break;
				case REQUEST_DIAG_STATUS :
							PollDiagnositcs();
							break;
				case ABORT_DIAGNOSTICS :
							AbortDiagnostics();
							break;
				case RESET_MCP :
							StopWatchdogTimer();
							HostReset();  /* Bring down the house! */
							break;
				case NVM_CYCLE_CNTR_REQ :
							SendNvmCycleCounts();
							break;   
				case NVM_CYCLE_CNTR_DATA :
							SaveCycleCountData();
							break;
				case POWER_DOWN_CMD : 
							DoPowerDown();
							break;	
				case JAM_COUNTER_REQUEST :
							SendJamCounterData(); 
							break;
				case JAM_COUNTER_DATA :
							SaveJamCounterData();					
							break; 
				case NVM_PCN_INFO_REQUEST :
							RequestNvmPcnInfo();
							break;
				case NVM_PCN_INFO_DATA :
							SaveNvmPcnInfo();
							break;   
				case NVM_REC_REQUEST :
							SendNvmData();
							break;
				case NVM_REC_DATA :
							SaveNvmData();
							break;
				case NVM_INITIALIZATION :
							StopWatchdogTimer();
							InitializeNVM();
							StartWatchdogTimer(WATCH_DOG_TIMEOUT);
							break;							
				case DYNAMIC_GAP_PARAM :
							SaveGapParameters();
							break;
				case SLEEP_MODE_CMD :
							ProcessSleepModeCmd();
							break;
				case SET_TIME_CMD :  
							if (SetCurrentTime( (DATETIME *)(pBuff+1) ))
								LogError(INFO_ERROR, ERR_INVALID_TIME, 0, 0);
							break; 
                case PROFILE_CONFIG_REQUEST :
                			Profile_Cfg_Reply();
                			break;
                case MAINT_TABLE_DNLD : 
							DownLoadMaintTable();
							break;				  							
				case PROFILE_CFG_DNLD :   
							DownLoadProfileCfg();
							break;
			default :
						LogError(INFO_ERROR, ERR_UNSUPPORTED_MSG, uClass,*pBuff);
						break;
			
			}
		} else if (uClass == DIAGNOSTIC) {
				bLastDest  = *pBuff++;
				bLastSrc   = *pBuff++;
				if (*pBuff == LOOPBACK_COMMAND)
					LoopBack((int)uClass, (int)uLen);
				else if (*pBuff == INITIALIZE_LINK) {
					//					set_imask_ccr(1);
//					StartLinkLayer();
//					CalibrateTimers((int)ProcessorConfig.wLinkPacketTimeout,(int)ProcessorConfig.wLinkDeadmanTimeout);
					//					set_imask_ccr(0);
				} 
				else if (bLastDest == 0x01 && bLastSrc == SET_MONITOR_MODE) {
					SendSetMonitorModeAck(*pBuff);
				} else
					LogError(INFO_ERROR, ERR_UNSUPPORTED_MSG, uClass,*pBuff);
		} else if (uClass == DEVICE_QUERY) {
			switch (*(pBuff)) {
			case PCN_REQUEST :
					PCN_Reply(); 
					break;
#ifdef MMC_DEVICE_QUERY_CLASS
			case QUERY_ID :			
					SendDeviceId(FOX_MODE); 
					break;
			case CLASS_SUPPORT :   	
					SendClassReply();
					break;
			case GENERAL_CLASS_SUPPORT : 
					SendGeneralClassReply();
					break; 
			case CHANGE_BAUDRATE :
					if (!fbChangingBaudRate)
						SetBaudRate();
					break;
			case ACK_TIMEOUT_REQ :  
					ModifyAckTimeout(); 
					break;
#endif
			default :
				if (*(pBuff+2) == QUERY_ID)
					SendDeviceId(APOLLO_MODE);
				else
					LogError(INFO_ERROR, ERR_UNSUPPORTED_MSG, uClass,*pBuff);
				break;
			} 
		} else   // unsupported class
			LogError(INFO_ERROR, ERR_UNSUPPORTED_MSG, uClass,*pBuff);
	} 
	
}
#endif

/*SB
   ************************************************************************
   FUNCTION	: RegisterAccelTable
   AUTHOR	: Gary S. Jacobson
   DESCRIPTION	:
		   This function updates the Acceleration table list with the
		address of the new acceleration table.  If the acceleration table
		is already in the table it is dealocated.

   LIMITATIONS	:
		   It is the callers responsibility to make shure that the system
		is idle and the acceleration table (if installed) is not in use.
		This function MUST be called at background level and may NOT be called
		at interrupt level.

   INPUTS	:
			pTbl	: Pointer to the acceleration table
			uTblNum	: Acceleration table number

   OUTPUTS	:
			This function returns TRUE (non-zero) if there are no errors.

   FUNCTIONS CALLED:
  			free()

   PDL:
		IF table number is ilegal THEN
		   return(ERROR)
		IF table number is already in acceleration table list THEN
		   dealocate the old acceleration table
		Set the acceleration table entry to the new acceleration table
		return (OK)

   REVISION HISTORY:
   Rev #	Name		Date			Reason
   -----    ----------------- -------------- ----------------------------
     1.0	GSJ		24-Jan-94	Initial Release

  SE**********************************************************************/
static int RegisterAccelTable(void * pTbl, unsigned uTblNum, unsigned uLength)
{
	extern ACCEL_TABLE AccelTable[];

	if (uTblNum < MAX_ACCEL_TBL)
	{
		AccelTable[uTblNum].pTable = pTbl;		/* Save pointer to new acceleration table */
		AccelTable[uTblNum].uLength = uLength;  /* and the length in bytes of the table */
		return(1);
	}
	return(0);
}

void fnProcessAccelTable(unsigned char * pAccelTable, unsigned char tableNum, unsigned short tableSize)
{
	extern ACCEL_TABLE AccelTable[];


		if (tableNum < MAX_ACCEL_TBL) {
			//TODO disable interrupts - set_imask_ccr(1);
			if (AccelTable[tableNum].pTable != NULL) { 	/* If accel table is already used */
				free(AccelTable[tableNum].pTable);	/* deallocate the memory */
			}
			(void)RegisterAccelTable(pAccelTable, tableNum, tableSize);
			//TODO enable interrupts - set_imask_ccr(0);
		}
}

void fnProcessProfile(unsigned char * pProfile, unsigned char profileNum, unsigned char segmentCnt)
{
	extern INSTALLED_PROFILE InstalledProfile[];
	extern unsigned int uLastProfile;
	register INSTALLED_PROFILE * p;

	if (profileNum < MAX_PROFILES)
	{
		if (profileNum > uLastProfile)
			uLastProfile = profileNum;	/* Save Highest Profile Number */

		p = InstalledProfile + profileNum;	/* Calculate the table entry pointer */

		//TODO disable interrupts - set_imask_ccr(1);
		if (p->pProfile != NULL) {		/* Don't allow duplicate profile numbers */
			free(p->pProfile);		/* dealocate the profile currently assigned to this profile # */
		}

		p->pProfile = (SEGMENT *) pProfile;		/* Save the new profile address */
		p->uNumberOfSegments = segmentCnt;
		p->fbActive = 0;
		p->fbCancel = 0;
		//TODO enable interrupts - set_imask_ccr(0);
	}

}

void fnProcessProcessorCfg(unsigned char * pConfig)
{
	extern PROC_CFG ProcessorConfig;
	register unsigned char * pTaskInfo;

//	StopWatchdogTimer();
//    StopSystemTimer();
	ProcessorConfig.bDivisor   = pConfig[PCFG_DIVISOR];
	ProcessorConfig.bPrescaler = (unsigned char) (pConfig[PCFG_PRESCALE] & 0x0F);
	ProcessorConfig.bServoPrescaler = (unsigned char)((pConfig[PCFG_PRESCALE] & 0xF0) >> 4);
	ProcessorConfig.bServoDivisor   = pConfig[SERVO_DIVISOR];
	ProcessorConfig.wLinkPacketTimeout = pConfig[LINK_TIMEOUT];
	memcpy(&ProcessorConfig.wLinkDeadmanTimeout, (pConfig + LINK_DEADMAN),2);

//	set_imask_ccr(1);
#if 0
	//TODO - is task info still necessary?
	for (pTaskInfo  = pConfig + DL_PCFG_TASKINFO; *(pTaskInfo) != 0xFF; pTaskInfo += DL_PCFG_TASKBLKSIZE)
		if (pTaskInfo[DL_PCFG_TASKID] < MAX_EVENTS)
			(void)UpdateEvent((unsigned)pTaskInfo[DL_PCFG_TASKID],(unsigned)pTaskInfo[DL_PCFG_START_TICK],(unsigned)pTaskInfo[DL_PCFG_RESCHED]);
#endif
	free(pConfig);		/* dealocate config memory after copying data # */
//	InitSystemTimer(ProcessorConfig.bDivisor, ProcessorConfig.bPrescaler);

//	StartSystemTimer();
//	set_imask_ccr(0);
//	TurnOffRedLed(0,0);			/* Clear Error Indicator if any ... */

//	StartWatchdogTimer(WATCH_DOG_TIMEOUT);

}

void fnProcessMotorCfg(unsigned char * pConfig)
{
	unsigned char bNumberOfDevices;
	register MOTOR_CFG * pMtrCfg;
	extern unsigned int uLastMotor;

	bNumberOfDevices = *(pConfig + MCB_NUMDEV);
	pMtrCfg = (MOTOR_CFG *) (pConfig + MCB_DEVID);
	uLastMotor = 0;


	if (bNumberOfDevices > MAX_MOTORS)
	{
		return;
	}

	//	set_imask_ccr(1);
	while (bNumberOfDevices--)
	{
		if (bNumberOfDevices < MAX_MOTORS)
			ConfigureMotor(pMtrCfg++);
	}
	//	set_imask_ccr(0);
	free(pConfig);		/* dealocate config memory after copying data # */
}


void fnProcessSensorCfg(unsigned char * pConfig)
{
	unsigned char bNumberOfDevices;
	register unsigned char * pCfg;
	register unsigned char bSensorIndex;

	extern unsigned char bSensorOffsets[];
	extern unsigned char bSensorThreshold[];
	extern unsigned char bDebounceTicks[];
	extern unsigned char fbBlockedState[];

	bNumberOfDevices = *(pConfig + SCB_NUM_SENSORS);
	pCfg = (pConfig + SCB_DEVID);

	while (bNumberOfDevices--)
	{
		bSensorIndex = pCfg[SCB_SENSOR_ID];
		bSensorIndex &= 0x0F;				/* Mask off device type */
		if (pCfg[SCB_SENSOR_TYPE] == 0) 		/* 0 == ANALOG SENSOR */
		{
			if (bSensorIndex < N_SENSORS) {
				fbBlockedState[bSensorIndex+N_DINPUTS] = pCfg[SCB_BLOCKED_STATE];
				bSensorOffsets[bSensorIndex] = pCfg[SCB_OFFSET];
				bSensorThreshold[bSensorIndex] = pCfg[SCB_THRESHOLD];
				if (255 - bSensorOffsets[bSensorIndex] <= bSensorThreshold[bSensorIndex])
				{
				/* Send a Service Error Message - We should not continue because sensor will never transition ! */
					//LogError(SVC_ERROR, ERR_SENSOR_THRESHOLD, bSensorIndex, bSensorOffsets[bSensorIndex]);
					//SetBlinkCode(BLINK_SENSOR_ERROR,1);
				}
			}
		} else if (bSensorIndex < N_DINPUTS) {
			fbBlockedState[bSensorIndex] = pCfg[SCB_BLOCKED_STATE];
			bDebounceTicks[bSensorIndex] = pCfg[SCB_DEBOUNCE_TICKS];
		}
		pCfg += SCB_BLKSIZE;
	}
	//RefreshSensorFlags();
	free(pConfig);		/* dealocate config memory after copying data # */
}


void fnProcessMaintTableCfg(unsigned char * pTable, unsigned char tableNum, unsigned char numParams)
{
	extern MAINT_TABLE MaintTable[];
	unsigned char bTableID;
	unsigned char bNumberOfEntries;

	bTableID = *(pTable + MAINTANCE_TABLE_ID);
	bNumberOfEntries = *(pTable + NUM_MAINT_ENTRIES);

	MaintTable[bTableID].bNumberOfEntries = bNumberOfEntries;

	if (MaintTable[bTableID].pTable != NULL){
		//		set_imask_ccr(1);
		free((void *) (MaintTable[bTableID].pTable));
		//		set_imask_ccr(0);
	}

	MaintTable[bTableID].pTable = (unsigned int *)pTable;
}

