/*************************************************************************
*	PROJECT:		Spark
*	AUTHOR:			Derek DeGennaro
*	MODULE NAME:	$Workfile:   dxml.c  $
*	REVISION:		$Revision:   1.28  $
*	
*	DESCRIPTION:	The DXML parser in all its glory.
*					
*
* ----------------------------------------------------------------------
*               Copyright (c) 1998 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*
*	REVISION HISTORY:
*	$Log:   H:/group/SPARK/ARCHIVES/UIC/dxml.c_v  $
* 
*    Rev 1.28   Feb 19 2004 11:52:36   DE009DE
* fixed lint's wacky warnings :)
* 
*    Rev 1.27   Feb 05 2004 17:59:24   degends
* As a temporary step to fix the stack overwrite issue
* the XML parser is malloc'd in XMLParse and
* ParseXMLTemplate.  The real solution will be
* addressed later since we are being rushed to release
* version 10 of the UIC.
* 
*    Rev 1.26   09 Jan 2004 08:42:58   TX08853
* Modified the ParseXMLTemplate() to monitor
* for a NULL character or when iParseResult 
* != XML_STATUS_OK to terminate processing
* an XML Template.
* 
* 
*    Rev 1.25   Jan 07 2004 11:39:02   vi200ro
* - Added ParseXMLTemplate
* - changed "poshorts" to "points" :-)
* 
*    Rev 1.24   Mar 03 2003 16:41:14   MA507HA
* Added stdlib.h to have prototypes for
* malloc() and free()
* 
*    Rev 1.23   Feb 28 2003 15:04:30   degends
* Lint Patrol
* 
*    Rev 1.22   Nov 21 2002 11:30:30   mernandez
* Changes made with Derek...to fix a problem introduced when the xml 
* lookup tables were made const. Removed the table sort and changed 
* the binary search to a linear search.
* 
*    Rev 1.21   Feb 15 2002 13:35:22   degends
* Fixed a bug in the parser that was found by Rob.
* The lexical analyzer was generating a token error
* if a '>' character was found in character data.
* Example: <test>abc>def</test>
* This is legal XML, but the parser would give
* a token error.  The lexical anaylzer state table
* was adjusted so this is allowed.  I also put in
* a test case in Unit Test 1 to make sure this is
* ok.
* 
*    Rev 1.20   Nov 14 2001 18:10:56   degends
* Lint Patrol: removed some unused include files.
* 
*    Rev 1.19   Jul 05 2001 12:40:04   degends
* Fixed a problem in the parser.  When you had a start tag, followed
* by whitespace, followed by CharData the parser would send incorrect
* data to the handlers.
* 
*    Rev 1.18   May 30 2001 13:55:48   degends
* Fixed function XMLParse.  There may have been a case
* where the function did not release all memory when a null
* pointer was given for the begin or end pointer.  Also, I don't
* know how it compiled but there was a local variable of type 
* DXMLParser named DXMLParser.
* 
*    Rev 1.17   Feb 02 2001 16:01:36   degends
* My bad, I added some checking to make sure the
* parser does not sort a lookup table that is
* invalid because of zero elements or a NULL pointer.
* 
*    Rev 1.16   Feb 02 2001 15:48:30   degends
* Fixed a bug where the document handlers were not
* being called.  The cause was that when I made the changes
* to clean up the interfaces I forgot to sort the lookup
* table in XMLParserInit
* 
*    Rev 1.15   Jan 11 2001 15:12:36   degends
* Cleaned up interfaces as much as I could.  Fixed a bug in the parser where
* memory might not be released properly.
* 
*    Rev 1.14   Nov 29 2000 13:33:02   degends
* Modified the transitions entries for the lexical
* analyzer so they don't have redundant current
* state information.
* 
* 
*    Rev 1.13   Nov 28 2000 11:09:10   degends
* This is the "Mother of All Check-in's", aka
* the "Big Bang" check-in.  There have been
* so many changes I can't remember them all,
* but here are the highlights:
* 
* 1.  The formatter now has Blinding built in.
* 2.  There were several modifications resulting
* from Wes's informal code review.  The highlight
* being a parsing optimization with the lexical 
* analyzer.
* 3.  The memory allocation in the stack and
* XMLBufferMgs has been reworked so it does
* not use realloc.  Instead it uses malloc in a
* somewhat inefficient manner.
* 
* 4.  I have put my 2 cents into how errors should
* be handled in relation to the parser and formatter.
* I have included this in the comment section
* of xmltypes.h.
* 
* 5. Made an effort to minimize the size of the
* DXML structure.  This included replacing some
* flags with bit fields.
* 
* 6.  In general, more comments to clarify some
* things.
* 
* 7.  Fixed some minor bugs.
* 
*    Rev 1.12   Oct 02 2000 10:56:30   degends
* Corrected little bug introduced by adding the null-terminator to the
* ends of the char data and whitespace.  This was done using the 
* buffer-manager, and the count was incremented.  This count is not
* the actual number of chars passed to the doc-handler so I subtract
* 1 from the number and pass this value to the doc-handler.
* 
*    Rev 1.11   Sep 29 2000 16:35:14   degends
* Fixed a bug in the fnFoundSTag function that prevented the attributes
* from getting sent to the doc-handler correctly.
* 
*    Rev 1.10   Sep 29 2000 14:59:10   degends
* I made sure all "FoundXYZ" functions insert a NULL terminator
* for any calls made to the doc-handler interface.  This makes sure
* any mis-behaving doc-handlers don't blow up.  This is not ideal however
* since a 0-value char may naturally occur in the stream --> doc-handler
* code should really use the character counts provided in the interface.
* 
*    Rev 1.9   Sep 21 2000 16:58:10   degends
* Major work on the formatter.  Fixed a bug int he 
* B64 decoding algorithm.  Added more comments.
* Fixed some bits and pieces.
* 
*    Rev 1.8   Sep 13 2000 17:32:44   degends
* This revision has the usual PVCS header at the top of the files.
* Also, DXML now has Blinding built in.
*
*************************************************************************/


#include <stdio.h>
#include <string.h>
#include "stdlib.h"
#include "xmltypes.h"
#include "xmlparse.h"

//lint -e641 -e785 -e746
/* #define XML_DEBUG_PRINTPARSE */

/* Buffer Manager Stuff ***********************************/
/* return values for the XMLBuffMgr routines */
#define XMLBUFFMGR_OK 0
#define XMLBUFFMGR_ERROR 1
/* (forward definitions for private functions) */
/* initialize mgr (allocates memory) */
static short XMLBuffMgrInit(XMLBuffMgr *pMgr);
/* releases all memory for the mgr */
static short XMLBuffMgrReleaseMem(XMLBuffMgr *pMgr);
/* throws out all chars in buffer (does not deallocate mem) */
static short XMLBuffMgrReset(XMLBuffMgr *pMgr);
/* add another char to buffer (allocates mem if necessary) */
static short XMLBuffMgrAddChar(XMLBuffMgr *pMgr, XmlChar x);
/**********************************************************/

/**********************************************************/
/* functions called when a token is found */
static short fnFoundSTag(void *pParser);
static short fnFoundETag(void *pParser);
static short fnFoundCharData(void *pParser);
static short fnFoundEmptyElementTag(void *pParser);
static short fnFoundReference(void *pParser);
static short fnFoundWS(void *pParser);

/* debug functions */
#ifdef XML_DEBUG_PRINTPARSE
static void PrshortProduction(struct Production p);
static void PrshortSymbolStack(Stack *pStack, short iTop);
#endif

/* Private Send Char function for the parser */
static short DXMLSendCharUnFiltered(DXMLParser *p, XmlChar x);

/* Private Send EOF function for the parser */
static short DXMLSendEOFUnFiltered(DXMLParser *p);
static short XMLSendParserEOF(DXMLParser *p);
/**********************************************************/

/* Forward Declarations of Action & Compare Functions****************/
static short fnDXMLActStartSTag(DXMLParser *p, XmlChar x);
static short fnDXMLActNextSTagChar(DXMLParser *p, XmlChar x);

static short fnDXMLActStartNextAttrVal(DXMLParser *p, XmlChar x);
static short fnDXMLActNextAttrValChar(DXMLParser *p, XmlChar x);

static short fnDXMLActStartAttrName(DXMLParser *p, XmlChar x);
static short fnDXMLActAttrNameChar(DXMLParser *p, XmlChar x);

static short fnDXMLActEndAttrValChar(DXMLParser *p, XmlChar x);

static short fnDXMLActStartNameRef(DXMLParser *p, XmlChar x);
static short fnDXMLActNextNameRefChar(DXMLParser *p, XmlChar x);
static short fnDXMLActEndNameRef(DXMLParser *p, XmlChar x);

static short fnDXMLActStartCharRef(DXMLParser *p, XmlChar x);
static short fnDXMLActNextCharRefChar(DXMLParser *p, XmlChar x);
static short fnDXMLActEndCharRef(DXMLParser *p, XmlChar x);

static short fnDXMLActStartHexCharRef(DXMLParser *p, XmlChar x);
static short fnDXMLActNextHexRefChar(DXMLParser *p, XmlChar x);
static short fnDXMLActEndHexCharRef(DXMLParser *p, XmlChar x);

static short fnDXMLActStartWS(DXMLParser *p, XmlChar x);
static short fnDXMLActWSChar(DXMLParser *p, XmlChar x);

static short fnDXMLActStartCharData(DXMLParser *p, XmlChar x);
static short fnDXMLNextChardataChar(DXMLParser *p, XmlChar x);

static short fnDXMLEndStartChars(DXMLParser *p, XmlChar x);

static short fnDXMLActEndAttrHexRef(DXMLParser *p, XmlChar x);
static short fnDXMLActEndAttrDigitRef(DXMLParser *p, XmlChar x);
static short fnDXMLActEndAttrNameRef(DXMLParser *p, XmlChar x);

static short fnDXMLActStartCDSect(DXMLParser *p, XmlChar x);

static short fnDXMLActEndCDSect(DXMLParser *p, XmlChar x);

static short fnDXMLActStartHexRefAttr(DXMLParser *p, XmlChar x);

static short fnDXMLActReturnTknErr(DXMLParser *p, XmlChar x)
{
	p->m_wErrorStatus |= DXML_TOKEN_ERR;
	return XML_TOKEN_ERR;
}

static short fnDefaultCompareFcn(XmlChar x);

/* handles document */
static short DXMLHandleStartElement(DXMLParser *p, XmlChar *pName, XMLAttribute *ppAttrList, short iNumAttr);
static short DXMLHandleCharacters(DXMLParser *p, XmlChar *pChars, unsigned long iLen);
static short DXMLHandleEndElement(DXMLParser *p, XmlChar *pName);

/* lookup table stuff */
static short SearchLookupTable(ElementLookup lookup, XmlChar *pKey);

/* turn Blinding on */
static short XMLParserSetBlindingActive(DXMLParser *p);
/**********************************************************/

/* Private definitions ***************************/
#define NON_TERM_OFFSET 13;
#define NUM_NONTERM 11
#define NUM_TOK 12
#define IS_NON_TERM(x) (x >= 13 && x <= 24)
#define MAX_RIGHT_SIDE 3
#define NUM_XML_PRODUCTIONS 25

/* Definition of Tokens & Non-Terminals***********/
/* Tokens
	Below are some samples of the tokens
	specified in the XMLTokens list. Next
	to each token is a brief description.

  XMLDecl - <?xml version="1.0"> encoding="utf-8"?>
  DocTypeDecl - <!DOCTYPE foo SYSTEM ... >
  Comment - <!-- this is a comment -->
  PI - <?my_instruction foo bar ?>
  S - whitespace
  EmptyElemTag - <foo a1="def"/>
  STag - <foo a1="xyz" b='hello'>
  ETag - </foo>
  Reference - &#x12;  &#18;  &gt;
  CDSect - <![CDATA[ this is not markup <11> ]]>
  CharData - this is text
*/
enum XMLTokens {
	XMLDecl,			/* specifies version, encoding, & standalone */
	DocTypeDecl,		/* specifies an external or internal DTD */
	Comment,			/* comments in XML */
	PI,					/* Processing Instruction (not used much in XML) */
	S,					/* whitespace -> tabs, CR, LF, SP */
	EmptyElemTag,		/* empty element tag, same as STag followed by ETag */
	STag,				/* start tag, signals the start of an element */
	ETag,				/* end tag, signals the end of an element */
	Reference,			/* numeric reference, escape char, or replacement entity */
	CDSect,				/* way to include markup chars without escaping */
	CharData,			/* character data - i.e. text */
	EOS,				/* signals the end of stream used internal to the parser */
	NoToken				/* used internally by the parser, not an actual XML token */
};

/* Non-Terminals
The list below describe the components of
the grammar.  next to each is a brief 
description.
*/
enum XMLNonTerminals {
	document = 13,		/* the start symbol of the grammar.  this is the big kahuna */
	prolog,				/* a prolog has stuff like DTD's, xml declarations, pi's, comments, & ws */
	opt_xmldecl,		/* optional XMLDecl */
	misc_lst,			/* optional series of PI's, comments, or whitespace */
	misc_lst2,			/* needed for grammar's sake */
	opt_docmis,			/* optional DocTypeDecl */
	misc,				/* a PI, comment, or whitespace */
	element,			/* an XML element */
	content_lst,		/* optional series of XML content */
	content_lst2,		/* needed for grammar's sake */
	content,			/* an element, chardata, pi, ws, ref, or cdsect */
	eos					/* end of stream used internally to parser */
};
/*************************************************/

/* Definition of Production *******************************
A production is represened like this:
X -> ABC
where X is the non-terminal being
described, and ABC are the symbols
that make up X.  In the struct below
the left member refers to X, num is
the number of symbols on the right
of the arrow, and the array right
are the symbols on the right.  There
should always be at least one symbol
on the right side of the production.
**********************************************************/
struct Production {
	unsigned char left;		/* non-term on left of arrow */
	unsigned char num;		/* n # of symbols on right of arrow */
	unsigned char right[MAX_RIGHT_SIDE];	/* the symbols on right of arrow */
};

/********************************************************************************/
/* Grammar Definitions, Parse Table, Productions, Parsing Method, 
Tokenizer Function */
/********************************************************************************/
/* m_XMLProductions *********************
This is an array of productions that define the DXML grammar.  The
entries in the parse table (m_bParseTable) refer to these productions.
***************************************/
static const struct Production m_XMLProductions[NUM_XML_PRODUCTIONS] =
{{document, 3, {prolog, element, misc_lst}},
{prolog, 3, {opt_xmldecl, misc_lst, opt_docmis}},
{opt_xmldecl, 1, {XMLDecl}},
{opt_xmldecl, 1, {NoToken}},
{misc_lst, 1, {misc_lst2}},
{misc_lst2, 2, {misc, misc_lst2}},
{misc_lst2, 1, {NoToken}},
{opt_docmis, 2, {DocTypeDecl, misc_lst}},
{opt_docmis, 1, {NoToken}},
{misc, 1, {Comment}},
{misc, 1, {PI}},
{element, 1, {EmptyElemTag}},
{element, 3, {STag, content_lst, ETag}},
{content_lst, 2, {content, content_lst2}},
{content_lst, 1, {NoToken}},
{content_lst2, 2, {content, content_lst2}},
{content_lst2, 1, {NoToken}},
{content, 1, {element}},
{content, 1, {CharData}},
{content, 1, {Reference}},
{content, 1, {CDSect}},
{content, 1, {PI}},
{content, 1, {Comment}},
{misc, 1, {S}},
{content, 1, {S}}};

/***********************************************************
m_bParseTable
This is the parse table for the XML language in DXML.
The entry m_bParseTable[Y][X] refers to the action for
M[Y,X].  The values are the indexes of the productions
above in m_XMLProductions.  A value of 0xFF indicates an
error.  A table of error strings could be added.
***********************************************************/
static const unsigned char m_bParseTable[NUM_NONTERM][NUM_TOK] =
{
	{0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0x00,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF},
	{0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0x01,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF},
	{0x02,	0x03,	0x03,	0x03,	0x03,	0x03,	0x03,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF},
	{0xFF,	0x04,	0x04,	0x04,	0x04,	0x04,	0x04,	0xFF,	0xFF,	0xFF,	0xFF,	0x04},
	{0xFF,	0x06,	0x05,	0x05,	0x05,	0x06,	0x06,	0xFF,	0xFF,	0xFF,	0xFF,	0x06},
	{0xFF,	0x07,	0xFF,	0xFF,	0xFF,	0x08,	0x08,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF},
	{0xFF,	0xFF,	0x09,	0x0A,	0x17,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF},
	{0xFF,	0xFF,	0xFF,	0xFF,	0xFF,	0x0B,	0x0C,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF},
	{0xFF,	0xFF,	0x0D,	0x0D,	0x0D,	0x0D,	0x0D,	0x0E,	0x0D,	0x0D,	0x0D,	0xFD},
	{0xFF,	0xFF,	0x0F,	0x0F,	0x0F,	0x0F,	0x0F,	0x10,	0x0F,	0x0F,	0x0F,	0xFF},
	{0xFF,	0xFF,	0x16,	0x15,	0x18,	0x11,	0x11,	0xFF,	0x13,	0x14,	0x12,	0xFF}
};

/*********************************************************/
/* Definition of the Entries for the Token State Table.
This structure defines the transitions between states 
in the lexical analyzer.  The states are defined in
the XmlTokenState enumeration in the dxml.h file.  There
are 2 ways to transition from a given state.  First is
the compare function is not NULL, we call this.  If
True is returned then we can transition  The second way
occurs if the compare function is NULL.  Then the char
being sent is compared with the c member of the entry.
Both of these results can be negated if the fNegate
flag is true.  This final result determines if a
transition can occur.  If a transition can occur, then
the action function is called and the new state is set to
the value of the newstate member.  The result field indicates
that a valid token has been found.
*/
typedef struct tagXmlTokenStateEntry {
	XmlChar			c;
	BOOL			fNegate;
	short (*fcnCompare)(XmlChar x);
	short (*fcnAction)(DXMLParser *p, XmlChar x);
	XmlTokenState	newstate;
	unsigned char	result;
} XmlTokenStateEntry;

typedef struct tagXmlTokenStateEntryList {
	short iNumEntries;
	const XmlTokenStateEntry *pEntries;
} XmlTokenStateEntryList;
/**********************************************************/

/* Parser Token State Table *******************************************************************
Below are the arrays that describe the transitions
from one state to another.  Each array of XmlTokenStateEntry
structures represents the possible transitions 
from a certain state.  Although redundant, the state
is given explicitly as the first member of the 
XmlTokenStateEntry's in the arrays.  The states
referred to in this design are indexes into another
table XmlTokenStateLookup.  XmlTokenStateLookup
is an array where each entry has a pointer to the
correct array of transitions.  for example,
XmlTokenStateLookup[TKN_RDY].pEntries ==> XmlTknStateTblRdy
***********************************************************************************************/
static const XmlTokenStateEntry XmlTknStateTblRdy[] = 
/* val		NOT		cmp fcn						action fcn					new state		result*/
{{'<',	FALSE,	NULL,						NULL,						TKN_LT,			NoToken},
{0,		FALSE,	fnIsUCWs,					fnDXMLActStartWS,			TKN_WS,			NoToken},
{'&',	FALSE,	NULL,						NULL,						TKN_AMP,		NoToken},
{0,		FALSE,	fnDefaultCompareFcn,		fnDXMLActStartCharData,		TKN_CHARDATA,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblAmp[] = 
{{'#',	FALSE,	NULL,						NULL,						TKN_AMPPND,		NoToken},
{0,		FALSE,	fnIsUCStartNameChar,		fnDXMLActStartNameRef,		TKN_REFERENCE1,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblAmpPnd[] = 
{{'x',	FALSE,	NULL,						fnDXMLActStartHexCharRef,	TKN_HEXCHARREF1,NoToken},
{0,		FALSE,	fnIsUCDigit,				fnDXMLActStartCharRef,		TKN_CHARREF1,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblLt[] = 
{{'!',	FALSE,	NULL,						NULL,						TKN_LTEX,		NoToken},
{'?',	FALSE,	NULL,						NULL,						TKN_LTQ,		NoToken},
{'/',	FALSE,	NULL,						NULL,						TKN_ETAG1,		NoToken},
{0,		FALSE,	fnIsUCLetter,				fnDXMLActStartSTag,			TKN_STAG1,		NoToken},
{'_',	FALSE,	NULL,						fnDXMLActStartSTag,			TKN_STAG1,		NoToken},
{':',	FALSE,	NULL,						fnDXMLActStartSTag,			TKN_STAG1,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblLtEx[] = 
{{'-',	FALSE,	NULL,						NULL,						TKN_LTEXDSH,	NoToken},
{'D',	FALSE,	NULL,						NULL,						TKN_DOCTYPE1,	NoToken},
{'[',	FALSE,	NULL,						fnDXMLActStartCDSect,		TKN_CDSECT1,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblLtExDsh[] = 
{{'-',	FALSE,	NULL,						NULL,						TKN_COMMENT1,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblComm1[] = 
{{'-',	FALSE,	NULL,						NULL,						TKN_COMMENT2,	NoToken},
{'>',	FALSE,	NULL,						fnDXMLActReturnTknErr,		TKN_COMMENT1,	NoToken},
{'-',	TRUE,	NULL,						NULL,						TKN_COMMENT1,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblComm2[] = 
{{'-',	FALSE,	NULL,						NULL,						TKN_COMMENT3,	NoToken},
{'>',	FALSE,	NULL,						fnDXMLActReturnTknErr,		TKN_COMMENT2,	NoToken},
{'-',	TRUE,	NULL,						NULL,						TKN_COMMENT1,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblComm3[] = 
{{'>',	FALSE,	NULL,						NULL,						TKN_RDY,		Comment},
{'-',	FALSE,	NULL,						NULL,						TKN_COMMENT3,	NoToken},
{'-',	TRUE,	NULL,						NULL,						TKN_COMMENT1,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblDocT1[] = 
{{'O',	FALSE,	NULL,						NULL,						TKN_DOCTYPE2,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblDocT2[] = 
{{'C',	FALSE,	NULL,						NULL,						TKN_DOCTYPE3,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblDocT3[] = 
{{'T',	FALSE,	NULL,						NULL,						TKN_DOCTYPE4,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblDocT4[] = 
{{'Y',	FALSE,	NULL,						NULL,						TKN_DOCTYPE5,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblDocT5[] = 
{{'P',	FALSE,	NULL,						NULL,						TKN_DOCTYPE6,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblDocT6[] = 
{{'E',	FALSE,	NULL,						NULL,						TKN_DOCTYPE7,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblDocT7[] = 
{{'[',	FALSE,	NULL,						NULL,						TKN_DOCTYPE9,	NoToken},
{'>',	FALSE,	NULL,						NULL,						TKN_RDY,		DocTypeDecl},
{0,		FALSE,	fnIsUCWs,					NULL,						TKN_DOCTYPE7,	NoToken},
{0,		TRUE,	fnIsUCWs,					NULL,						TKN_DOCTYPE8,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblDocT8[] = 
{{'[',	FALSE,	NULL,						NULL,						TKN_DOCTYPE9,	NoToken},
{'>',	FALSE,	NULL,						NULL,						TKN_RDY,		DocTypeDecl},
{'>',	TRUE,	NULL,						NULL,						TKN_DOCTYPE8,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblDocT9[] = 
{{']',	FALSE,	NULL,						NULL,						TKN_DOCTYPE10,	NoToken},
{']',	TRUE,	NULL,						NULL,						TKN_DOCTYPE9,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblDocT10[] = 
{{0,	FALSE,	fnIsUCWs,					NULL,						TKN_DOCTYPE10,	NoToken},
{'>',	FALSE,	NULL,						NULL,						TKN_RDY,		DocTypeDecl}};

static const XmlTokenStateEntry XmlTknStateTblLtQ[] = 
{{'X',	FALSE,	NULL,						NULL,						TKN_LTQX,		NoToken},
{'x',	FALSE,	NULL,						NULL,						TKN_LTQX,		NoToken},
{0,		FALSE,	fnIsUCLetter,				NULL,						TKN_PI1,		NoToken},
{'_',	FALSE,	NULL,						NULL,						TKN_PI1,		NoToken},
{':',	FALSE,	NULL,						NULL,						TKN_PI1,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblLtQX[] = 
{{'M',	FALSE,	NULL,						NULL,						TKN_LTQXM,		NoToken},
{'m',	FALSE,	NULL,						NULL,						TKN_LTQXM,		NoToken},
{0,		FALSE,	fnIsUCNameChar,				NULL,						TKN_PI1,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblLtQM[] = 
{{'L',	FALSE,	NULL,						NULL,						TKN_XMLDECL1,	NoToken},
{'l',	FALSE,	NULL,						NULL,						TKN_XMLDECL1,	NoToken},
{0,		FALSE,	fnIsUCNameChar,				NULL,						TKN_PI1,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblXDec1[] = 
{{'?',	FALSE,	NULL,						NULL,						TKN_XMLDECL2,	NoToken},
{'>',	FALSE,	NULL,						fnDXMLActReturnTknErr,		TKN_XMLDECL1,	NoToken},
{'?',	TRUE,	NULL,						NULL,						TKN_XMLDECL1,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblXDecl2[] = 
{{'>',	FALSE,	NULL,						NULL,						TKN_RDY,		XMLDecl}};

static const XmlTokenStateEntry XmlTknStateTblPi1[] = 
{{'?',	FALSE,	NULL,						NULL,						TKN_PI2,		NoToken},
{0,		FALSE,	fnIsUCWs,					NULL,						TKN_PI3,		NoToken},
{0,		FALSE,	fnIsUCNameChar,				NULL,						TKN_PI1,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblPi2[] = 
{{'>',	FALSE,	NULL,						NULL,						TKN_RDY,		PI}};

static const XmlTokenStateEntry XmlTknStateTblPi3[] = 
{{'?',	FALSE,	NULL,						NULL,						TKN_PI2,		NoToken},
{0,		FALSE,	fnIsUCWs,					NULL,						TKN_PI3,		NoToken},
{'>',	TRUE,	NULL,						NULL,						TKN_PI4,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblPi4[] = 
{{'?',	FALSE,	NULL,						NULL,						TKN_PI2,		NoToken},
{'>',	TRUE,	NULL,						NULL,						TKN_PI4,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblETag1[] = 
{{0, 	FALSE,	fnIsUCLetter,				fnDXMLActStartSTag,			TKN_ETAG2,		NoToken},
{'_',	FALSE,	NULL,						fnDXMLActStartSTag,			TKN_ETAG2,		NoToken},
{':',	FALSE,	NULL,						fnDXMLActStartSTag,			TKN_ETAG2,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblETag2[] = 
{{0,	FALSE,	fnIsUCNameChar,				fnDXMLActNextSTagChar,		TKN_ETAG2,		NoToken},
{0,		FALSE,	fnIsUCWs,					NULL,						TKN_ETAG3,		NoToken},
{'>',	FALSE,	NULL,						NULL,						TKN_RDY,		ETag}};

static const XmlTokenStateEntry XmlTknStateTblETag3[] = 
{{0,	FALSE,	fnIsUCWs,					NULL,						TKN_ETAG3,		NoToken},
{'>',	FALSE,	NULL,						NULL,						TKN_RDY,		ETag}};

static const XmlTokenStateEntry XmlTknStateTblSTag1[] = 
{{0,	FALSE,	fnIsUCNameChar,				fnDXMLActNextSTagChar,		TKN_STAG1,		NoToken},
{'>',	FALSE,	NULL,						NULL,						TKN_RDY,		STag},
{'/',	FALSE,	NULL,						NULL,						TKN_STAG2,		NoToken},
{0,		FALSE,	fnIsUCWs,					NULL,						TKN_STAG3,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblSTag2[] = 
{{'>',	FALSE,	NULL,						NULL,						TKN_RDY,		EmptyElemTag}};

static const XmlTokenStateEntry XmlTknStateTblSTag3[] = 
{{'>',	FALSE,	NULL,						NULL,						TKN_RDY,		STag},
{'/',	FALSE,	NULL,						NULL,						TKN_STAG2,		NoToken},
{0,		FALSE,	fnIsUCWs,					NULL,						TKN_STAG3,		NoToken},
{0,		FALSE,	fnIsUCLetter,				fnDXMLActStartAttrName,		TKN_STAG4,		NoToken},
{'_',	FALSE,	NULL,						fnDXMLActStartAttrName,		TKN_STAG4,		NoToken},
{':',	FALSE,	NULL,						fnDXMLActStartAttrName,		TKN_STAG4,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblSTag4[] = 
{{0,	FALSE,	fnIsUCNameChar,				fnDXMLActAttrNameChar,		TKN_STAG4,		NoToken},
{0,		FALSE,	fnIsUCWs,					NULL,						TKN_STAG6,		NoToken},
{'=',	FALSE,	NULL,						NULL,						TKN_STAG5,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblSTag5[] = 
{{0, 	FALSE,	fnIsUCWs,					NULL,						TKN_STAG5,		NoToken},
{0,		FALSE,	fnIsUCQuote,				fnDXMLActStartNextAttrVal,	TKN_STAG7,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblSTag6[] = 
{{0,	FALSE,	fnIsUCWs,					NULL,						TKN_STAG6,		NoToken},
{'=',	FALSE,	NULL,						NULL,						TKN_STAG5,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblSTag7[] = 
{/*{TKN_STAG7,			0x27,	0,	NULL,						fnDXMLActNextAttrValChar,	TKN_STAG7,		NoToken},*/
{0x27,	FALSE,	NULL,						fnDXMLActEndAttrValChar,	TKN_STAG3,		NoToken},
/*{TKN_STAG7,			'"',	0,	NULL,						fnDXMLActNextAttrValChar,	TKN_STAG7,		NoToken},*/
{'"',	FALSE,	NULL,						fnDXMLActEndAttrValChar,	TKN_STAG3,		NoToken},
{'&',	FALSE,	NULL,						NULL,						TKN_STAG8,		NoToken},
{0,		TRUE,	fnIsUCLtOrAmpOrGt,			fnDXMLActNextAttrValChar,	TKN_STAG7,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblSTag8[] = 
{{'#',	FALSE,	NULL,						NULL,						TKN_STAG9,		NoToken},
{0,		FALSE,	fnIsUCStartNameChar,		fnDXMLActStartNameRef,		TKN_STAG13,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblSTag9[] = 
{{'x',	FALSE,	NULL,						NULL,						TKN_STAG10,		NoToken},
{0,		FALSE,	fnIsUCDigit,				fnDXMLActStartCharRef,		TKN_STAG12,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblSTag10[] = 
{{0,	FALSE,	fnIsUCHexChar,				fnDXMLActStartHexRefAttr,	TKN_STAG11,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblSTag11[] = 
{{0,	FALSE,	fnIsUCHexChar,				fnDXMLActNextHexRefChar,	TKN_STAG11,		NoToken},
{';',	FALSE,	NULL,						fnDXMLActEndAttrHexRef,		TKN_STAG7,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblSTag12[] = 
{{0,	FALSE,	fnIsUCDigit,				fnDXMLActNextCharRefChar,	TKN_STAG12,		NoToken},
{';',	FALSE,	NULL,						fnDXMLActEndAttrDigitRef,	TKN_STAG7,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblSTag13[] = 
{{0,	FALSE,	fnIsUCNameChar,				fnDXMLActNextNameRefChar,	TKN_STAG13,		NoToken},
{';',	FALSE,	NULL,						fnDXMLActEndAttrNameRef,	TKN_STAG7,		NoToken}};

static const XmlTokenStateEntry XmlTknStateTblHRef1[] = 
{{0,	FALSE,	fnIsUCHexChar,				fnDXMLActNextHexRefChar,	TKN_HEXCHARREF1,NoToken},
{';',	FALSE,	NULL,						fnDXMLActEndHexCharRef,		TKN_RDY,		Reference}};

static const XmlTokenStateEntry XmlTknStateTblCRef1[] = 
{{0,	FALSE,	fnIsUCDigit,				fnDXMLActNextCharRefChar,	TKN_CHARREF1,	NoToken},
{';',	FALSE,	NULL,						fnDXMLActEndCharRef,		TKN_RDY,		Reference}};

static const XmlTokenStateEntry XmlTknStateTblRef1[] = 
{{0,	FALSE,	fnIsUCNameChar,				fnDXMLActNextNameRefChar,	TKN_REFERENCE1,	NoToken},
{';',	FALSE,	NULL,						fnDXMLActEndNameRef,		TKN_RDY,		Reference}};

static const XmlTokenStateEntry XmlTknStateTblWs[] = 
{{0,	FALSE,	fnIsUCWs,					fnDXMLActWSChar,			TKN_WS,			NoToken},
{'<',	FALSE,	NULL,						NULL,						TKN_LT,			S},
{'&',	FALSE,	NULL,						NULL,						TKN_AMP,		S},
{0,		FALSE,	fnDefaultCompareFcn,		fnDXMLEndStartChars,		TKN_CHARDATA,	S}};

static const XmlTokenStateEntry XmlTknStateTblCharData[] = 
{{0,	TRUE,	fnIsUCLtOrAmpOrGt,			fnDXMLNextChardataChar,		TKN_CHARDATA,	NoToken},
{'>',	FALSE,	NULL,						fnDXMLNextChardataChar,		TKN_CHARDATA,	NoToken},
{'<',	FALSE,	NULL,						NULL,						TKN_LT,			CharData},
{'&',	FALSE,	NULL,						NULL,						TKN_AMP,		CharData},
{0,		FALSE,	fnIsUCWs,					fnDXMLEndStartChars,		TKN_WS,			CharData}};

static const XmlTokenStateEntry XmlTknStateTblCDSect1[] = 
{{'C',	FALSE,	NULL,						NULL,						TKN_CDSECT2,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblCDSect2[] = 
{{'D',	FALSE,	NULL,						NULL,						TKN_CDSECT3,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblCDSect3[] = 
{{'A',	FALSE,	NULL,						NULL,						TKN_CDSECT4,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblCDSect4[] = 
{{'T',	FALSE,	NULL,						NULL,						TKN_CDSECT5,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblCDSect5[] = 
{{'A',	FALSE,	NULL,						NULL,						TKN_CDSECT6,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblCDSect6[] = 
{{'[',	FALSE,	NULL,						NULL,						TKN_CDSECT7,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblCDSect7[] = 
{{']',	FALSE,	NULL,						fnDXMLNextChardataChar,		TKN_CDSECT8,	NoToken},
{'>',	FALSE,	NULL,						fnDXMLActReturnTknErr,		TKN_CDSECT7,	NoToken},
{0,		FALSE,	fnDefaultCompareFcn,		fnDXMLNextChardataChar,		TKN_CDSECT7,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblCDSect8[] = 
{{']',	FALSE,	NULL,						fnDXMLNextChardataChar,		TKN_CDSECT9,	NoToken},
{'>',	FALSE,	NULL,						fnDXMLActReturnTknErr,		TKN_CDSECT8,	NoToken},
{0,		FALSE,	fnDefaultCompareFcn,		fnDXMLNextChardataChar,		TKN_CDSECT7,	NoToken}};

static const XmlTokenStateEntry XmlTknStateTblCDSect9[] = 
{{']',	FALSE,	NULL,						fnDXMLNextChardataChar,		TKN_CDSECT9,	NoToken},
{'>',	FALSE,	NULL,						fnDXMLActEndCDSect,			TKN_RDY,		CDSect},
{0,		FALSE,	fnDefaultCompareFcn,		fnDXMLNextChardataChar,		TKN_CDSECT7,	NoToken}};

/* State Transition Lookup Table ************************************
Below is a list of XmlTokenStateEntryList's that point to transitions
for a given state.  The nth entry in this list contains a pointer to
the allowable transitions from state n.
*********************************************************************/
/* #, ptr */
static const XmlTokenStateEntryList XmlTokenStateLookup[TKN_LAST_DXML_STATE] =
{{4,XmlTknStateTblRdy},			/* 0 */
{6,XmlTknStateTblLt},			/* 1 */
{3,XmlTknStateTblLtEx},			/* 3 */
{3,XmlTknStateTblETag1},		/* 4 */
{4,XmlTknStateTblSTag1},		/* 5 */
{2,XmlTknStateTblAmp},			/* 6 */
{2,XmlTknStateTblAmpPnd},		/* 7 */
{2,XmlTknStateTblHRef1},		/* 8 */
{2,XmlTknStateTblCRef1},		/* 9 */
{2,XmlTknStateTblRef1},			/* 10 */
{1,XmlTknStateTblLtExDsh},		/* 11 */
{3,XmlTknStateTblComm1},		/* 12 */
{3,XmlTknStateTblComm2},		/* 13 */
{3,XmlTknStateTblComm3},		/* 14 */
{1,XmlTknStateTblDocT1},		/* 15 */
{1,XmlTknStateTblDocT2},		/* 16 */
{1,XmlTknStateTblDocT3},		/* 17 */
{1,XmlTknStateTblDocT4},		/* 18 */
{1,XmlTknStateTblDocT5},		/* 19 */
{1,XmlTknStateTblDocT6},		/* 20 */
{4,XmlTknStateTblDocT7},		/* 21 */
{3,XmlTknStateTblDocT8},		/* 22 */
{2,XmlTknStateTblDocT9},		/* 23 */
{2,XmlTknStateTblDocT10},		/* 24 */
{5,XmlTknStateTblLtQ},			/* 25 */
{3,XmlTknStateTblLtQX},		/* 26 */
{3,XmlTknStateTblLtQM},		/* 27 */
{3,XmlTknStateTblXDec1},		/* 28 */
{3,XmlTknStateTblPi1},		/* 29 */
{1,XmlTknStateTblPi2},		/* 30 */
{3,XmlTknStateTblPi3},		/* 31 */
{2,XmlTknStateTblPi4},		/* 32 */
{3,XmlTknStateTblETag2},		/* 33 */
{2,XmlTknStateTblETag3},		/* 34 */
{1,XmlTknStateTblSTag2},		/* 35 */
{6,XmlTknStateTblSTag3},		/* 36 */
{3,XmlTknStateTblSTag4},		/* 37 */
{2,XmlTknStateTblSTag5},		/* 38 */
{2,XmlTknStateTblSTag6},		/* 39 */
{4,XmlTknStateTblSTag7},		/* 40 */
{2,XmlTknStateTblSTag8},		/* 41 */
{2,XmlTknStateTblSTag9},		/* 42 */
{1,XmlTknStateTblSTag10},		/* 43 */
{2,XmlTknStateTblSTag11},		/* 44 */
{2,XmlTknStateTblSTag12},		/* 45 */
{2,XmlTknStateTblSTag13},		/* 46 */
{4,XmlTknStateTblWs},		/* 47 */
{5,XmlTknStateTblCharData},		/* 48 */
{1,XmlTknStateTblCDSect1},		/* 50 */
{1,XmlTknStateTblCDSect2},		/* 51 */
{1,XmlTknStateTblCDSect3},		/* 52 */
{1,XmlTknStateTblCDSect4},		/* 53 */
{1,XmlTknStateTblCDSect5},		/* 54 */
{1,XmlTknStateTblCDSect6},		/* 55 */
{3,XmlTknStateTblCDSect7},		/* 56 */
{3,XmlTknStateTblCDSect8},		/* 57 */
{3,XmlTknStateTblCDSect9},		/* 58 */
{1,XmlTknStateTblXDecl2},		/* 59 */
};
/***********************************************************/

/* the list of supported name references (predefined escape sequences) */
static const struct {
	XmlChar *pName;
	XmlChar xChar;
} EscapeSeq[] = {
	{"amp", 0x26},
	{"lt", 0x3C}, 
	{"gt", 0x3E}, 
	{"apos", 0x27}, 
	{"quot", 0x22}
};

/**********************************************************
Function: XMLParserInit
Author: Derek DeGennaro
Description:
This function simply initializes a DXMLParser structure.
This function MUST be used to create a DXML parser
before parsing in streaming mode.  Since DXMLParser structures
are large, pointers are used in the DXML routines.  
This makes the routines more efficient 
(no passing huge structures around on the stack).

Inputs:
pDXMLParser - A pointer to a DXMLParser structure.  This 
structure is what get initialized.  

iEntries - Number of entries in the pEntries table.

pElements - Pointer to a table that contains element names
and the functions to call when we see start tags, characters,
and end tags.  When an element or character data is found
this table is searched.  If an element is found then
the appropriate function is called.  If an element is not
found or pElements if null then the default function pfn***
is called.

pfnStartDocument - Function that is called before parsing.
If this is null then no function is called.

pfnEndDocument - Function that is called after parsing.
If this is null then no function is called.

pfnCharacters - Function that gets called if chardata is
found and there is no lookup table or no match is found
in the data.  This may be null.

pfnStartElement - Function that gets called if start tag is
found and there is no lookup table or no match is found
in the data.  This may be null.

pfnEndElement - Function that gets called if end tag is
found and there is no lookup table or no match is found
in the data.  This may be null.

ulFlags - This value contains bit flags which turn on various
featurs of the parser.  For instance, Turning on the
XMLPARSER_BLINDING_ON flag turns Blinding on.

Outputs:
Return Value:
XML status code
************************************************************/
static short XMLParserInitInternal(DXMLParser *pDXMLParser,
						  short iEntries,
						  ElementLookupEntry *pElements,
						  short (*pfnStartDocument)(),
						  short (*pfnEndDocument)(),
						  short (*pfnStartElement)(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr),
						  short (*pfnEndElement)(XmlChar *pName),
						  short (*pfnElementCharacters)(XmlChar *pName, XmlChar *pChars, unsigned long iLen),
						  unsigned long ulFlags,
						  void * context,
						  short (*pfnStartDocumentCtx)(void * context),
						  short (*pfnEndDocumentCtx)(void * context),
						  short (*pfnStartElementCtx)(void * context, XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr),
						  short (*pfnEndElementCtx)(void * context, XmlChar *pName),
						  short (*pfnElementCharactersCtx)(void * context, XmlChar *pName, XmlChar *pChars, unsigned long iLen)
						  )
{
	StackElement stack_element;
	short iResult = XML_STATUS_OK;

	/* first, begin by initializing all normal variables */
	pDXMLParser->m_iXMLAttrListLen = 0;
	pDXMLParser->m_uCharRef = 0;
	pDXMLParser->m_ReferenceType = XML_REF_NONE;
	pDXMLParser->m_wErrorStatus = DXML_NO_ERRORS;
	pDXMLParser->m_wStatus = DXML_INITIAL_STATUS;
	pDXMLParser->m_pFilter = NULL;
	pDXMLParser->m_fnFilterSendChar = NULL;
	pDXMLParser->m_fnFilterSendEOF = NULL;
	pDXMLParser->m_fnFilterReleaseMem = NULL;
	
	if (pElements != NULL && iEntries > 0)
	{
		pDXMLParser->m_xmlLookup.iNumElements = iEntries;
		pDXMLParser->m_xmlLookup.pEntry = pElements;
	}
	else
	{
		pDXMLParser->m_xmlLookup.iNumElements = 0;
		pDXMLParser->m_xmlLookup.pEntry = NULL;
	}
	if (context == NULL)
	{// use legacy callbacks
		pDXMLParser->fnEndDocument = pfnEndDocument;
		pDXMLParser->fnStartDocument = pfnStartDocument;
		pDXMLParser->fnCharacters = pfnElementCharacters;
		pDXMLParser->fnEndElement = pfnEndElement;
		pDXMLParser->fnStartElement = pfnStartElement;
	}
	else
	{// use callbacks with context
		pDXMLParser->fnEndDocumentCtx = pfnEndDocumentCtx;
		pDXMLParser->fnStartDocumentCtx = pfnStartDocumentCtx;
		pDXMLParser->fnCharactersCtx = pfnElementCharactersCtx;
		pDXMLParser->fnEndElementCtx = pfnEndElementCtx;
		pDXMLParser->fnStartElementCtx = pfnStartElementCtx;
	}
	pDXMLParser->context = context;

	pDXMLParser->m_State = TKN_RDY;

	pDXMLParser->m_ulCharDataCount = 0;
	pDXMLParser->m_ulMarkupCharCount = 0;
	pDXMLParser->m_ulTotalCharCount = 0;
	pDXMLParser->m_ulLineNo = 1;

	pDXMLParser->m_bCharsOnHold = 0;

	/* second, set bounds & stuff on dynamic structures.  this way
	the generic release memory functions will work on them. */
	pDXMLParser->m_xmlElemStack.iLimit = pDXMLParser->m_xmlParseStack.iLimit = 0;
	pDXMLParser->m_xmlElemStack.iTop = pDXMLParser->m_xmlParseStack.iTop = 0;
	pDXMLParser->m_xmlElemStack.pItems = pDXMLParser->m_xmlParseStack.pItems = NULL;
	pDXMLParser->m_xmlElemStack.opResult = pDXMLParser->m_xmlParseStack.opResult = STACK_NO_ERR;

	pDXMLParser->m_xmlCharDataMgr.iLimit = pDXMLParser->m_xmlCharDataMgr.iLength = 0;
	pDXMLParser->m_xmlCharDataMgr.pBuff = NULL;

	pDXMLParser->m_xmlTagNameBuff.iLimit = pDXMLParser->m_xmlTagNameBuff.iLength = 0;
	pDXMLParser->m_xmlTagNameBuff.pBuff = NULL;

	if(ulFlags & XMLPARSER_BLINDING_ON)
	{
		iResult = XMLParserSetBlindingActive(pDXMLParser);
		if (iResult != XML_STATUS_OK)
			return iResult;
	}

	/* third, initialize all the dynamic structure using the 
	appropriate init function */
	if (!fnInitStack(&pDXMLParser->m_xmlElemStack))
	{
		(void)XMLParserReleaseMem(pDXMLParser);
		return XML_PARSER_INIT_ERR;
	}
	
	if (!fnInitStack(&pDXMLParser->m_xmlParseStack))
	{
		(void)XMLParserReleaseMem(pDXMLParser);
		return XML_PARSER_INIT_ERR;
	}

	if (XMLBuffMgrInit(&pDXMLParser->m_xmlCharDataMgr) != XMLBUFFMGR_OK)
	{
		(void)XMLParserReleaseMem(pDXMLParser);
		return XML_PARSER_INIT_ERR;
	}

	if (XMLBuffMgrInit(&pDXMLParser->m_xmlTagNameBuff) != XMLBUFFMGR_OK)
	{
		(void)XMLParserReleaseMem(pDXMLParser);
		return XML_PARSER_INIT_ERR;
	}

	/* lastly, we need to set up the grammar and element stack */
	/* we always use a byte stack */
	stack_element.bType = STACK_BYTE;
	/* put EOS symbol on stack */
	stack_element.data.b = eos;
	if (!fnPushStack(&pDXMLParser->m_xmlParseStack,stack_element)) 
		return XML_PARSER_INIT_ERR;
	/* put start symbol on stack */
	stack_element.data.b = document;
	if (!fnPushStack(&pDXMLParser->m_xmlParseStack,stack_element))
		return XML_PARSER_INIT_ERR;

	/* if we have blinding on, do it */
	if (ulFlags & XMLPARSER_BLINDING_ON)
		iResult = XMLParserSetBlindingActive(pDXMLParser);

	return iResult;
}

/* Wrapper for XMLParserInitInternal that provides original external interface */
short XMLParserInit(DXMLParser *pDXMLParser,
					  short iEntries,
					  ElementLookupEntry *pElements,
					  short (*pfnStartDocument)(),
					  short (*pfnEndDocument)(),
					  short (*pfnStartElement)(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr),
					  short (*pfnEndElement)(XmlChar *pName),
					  short (*pfnElementCharacters)(XmlChar *pName, XmlChar *pChars, unsigned long iLen),
					  unsigned long ulFlags)
{
	return XMLParserInitInternal(pDXMLParser,
			iEntries,
			pElements,
			pfnStartDocument,
			pfnEndDocument,
			pfnStartElement,
			pfnEndElement,
			pfnElementCharacters,
			ulFlags,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL
			);
}

/* Wrapper for XMLParserInitInternal that provides external interface with context for callbacks */
short XMLParserWithContextInit(DXMLParser *pDXMLParser,
					  short iEntries,
					  ElementLookupEntry *pElements,
					  short (*pfnStartDocumentCtx)(void * context),
					  short (*pfnEndDocumentCtx)(void * context),
					  short (*pfnStartElementCtx)(void * context, XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr),
					  short (*pfnEndElementCtx)(void * context, XmlChar *pName),
					  short (*pfnElementCharactersCtx)(void * context, XmlChar *pName, XmlChar *pChars, unsigned long iLen),
					  unsigned long ulFlags,
					  void * context)
{
	return XMLParserInitInternal(pDXMLParser,
			iEntries,
			pElements,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			ulFlags,
			context,
			pfnStartDocumentCtx,
			pfnEndDocumentCtx,
			pfnStartElementCtx,
			pfnEndElementCtx,
			pfnElementCharactersCtx
			);
}


/*************************************************************
Function: XMLParserGetChatacterStats
Author: Derek DeGennaro
Description:
During the parsing, the parser records the total number of
chars, the number of markup chars, and the number of actual
text chars.  This function can only be called after the
parser is initialized, otherwise the data returned is
garbage.

Inputs:
p - This is a pointer to the parser.
pulTotal - Address where total # chars is written.
pulMarkup - Address where # markup chars is written.
pulCharData - Address where # of Character Data chars
is written.

Outputs:
Return Value:
XML_STATUS_OK if everything went OK
XML_INVALID_PARSER_ERR if an invalid parser was given
***************************************************************/
short XMLParserGetChatacterStats(DXMLParser *p, unsigned long *pulTotal, unsigned long *pulMarkup, unsigned long *pulCharData)
{
	if (p == NULL)
		return XML_INVALID_PARSER_ERR;

	*pulTotal = p->m_ulTotalCharCount;
	*pulMarkup = p->m_ulMarkupCharCount;
	*pulCharData = p->m_ulCharDataCount;

	return XML_STATUS_OK;
}
/*************************************************************
Function: XMLParserReleaseMem
Author: Derek DeGennaro
Description:
Releases memory allocated by the parser.  This function MUST
be called after the parser has been used and before another
call to XMLParserInit.

After the init function has been called and before or after
any parsing (and ONLY then) it is safe to call 
this function.

Inputs:
pDXMLParser - This is a pointer to the parser whose memory is
going to be released.

Outputs:
Return Value:
XML_STATUS_OK if everything went OK
XML_INVALID_PARSER_ERR if an invalid parser was given
***************************************************************/
short XMLParserReleaseMem(DXMLParser *p)
{
	if (p == NULL)
		return XML_INVALID_PARSER_ERR;
	/* signal we have found EOF so this pointer can't be used again */
	p->m_wStatus |= DXML_EOF_FOUND;
	/* call release mem functions for each of the structures */
	(void)fnReleaseStack(&p->m_xmlElemStack);
	(void)fnReleaseStack(&p->m_xmlParseStack);
	(void)XMLBuffMgrReleaseMem(&p->m_xmlCharDataMgr);
	(void)XMLBuffMgrReleaseMem(&p->m_xmlTagNameBuff);
	
	/* we release the filter memory */
	if (p->m_pFilter != NULL && p->m_fnFilterReleaseMem != NULL)
		p->m_fnFilterReleaseMem(p->m_pFilter);

	return XML_STATUS_OK;
}
/*************************************************************
Function: XMLParserEndParse
Author: Derek DeGennaro
Description:
This function tells the parser that no more data is coming
and if this is the result of an error or because the end of
the document was reached.
Inputs:
p - pointer to DXMLParser structure that holds the state
of the parser.
fValid - if true then the document was ok, else there was
a problem and we need to abort.
Return Value:
XML_STATUS_OK - if the caller is aborting or the document was ok
others - the document was not ok and the caller was not aborting.
**************************************************************/
short XMLParserEndParse(DXMLParser *p, BOOL fValid)
{
	short iResult = XML_STATUS_OK;

	/* check to see that we have a valid parser */
	if (p == NULL)
		return XML_INVALID_PARSER_ERR;

	/* if we have already found the end of the
	document we cannot use this function again
	(the parser init function needs to be called
	again). */
	if (p->m_wStatus & DXML_EOF_FOUND)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_INVALID_PARSER_ERR;
	}

	if (fValid == TRUE)
	{
		/* if the doc is valid so far we
		want to continue by sending it
		the EOF token. */
		iResult = XMLSendParserEOF(p);
	}
	if (!(p->m_wStatus & DXML_END_CALLED))
	{
		if (p->context == NULL)
		{
			if (p->fnEndDocument != NULL)
				/* if we haven't signalled the
				end of doc to the handler do it now */
				p->fnEndDocument();
		}
		else
		{
			if (p->fnEndDocumentCtx != NULL)
				/* if we haven't signalled the
				end of doc to the handler do it now */
				p->fnEndDocumentCtx(p->context);
		}
	}

	
	/* make sure the parser knows that it has found the
	end of the doc */
	p->m_wStatus |= DXML_END_CALLED;
	p->m_wStatus |= DXML_EOF_FOUND;
	
	(void)XMLParserReleaseMem(p);

	return iResult;
}


Template_t gTemplate;

/*************************************************************
Function: ParseXMLTemplate
Author:   Derek DeGennaro / Vinny Rozendaal
Description:

   This function is a modification of the XMLParse function 
   below. It allows the current parser position in the memory
   buffer to be modified. It is the modifiers responsibility
   to make sure that the parser sees balanced XML i.e. modify 
   the parse position in such a way that the parse allways sees 
   well formed XML.

   the gTemplate global variable contains the following members:-

      XmlChar *pCurrentPos        - the next character to be parsed
                                    update this modify the parser position
      XmlChar *pStart;            - Start of the XML buffer
      XmlChar *pEnd;              - End of the XML buffer
      XmlChar *pStartTagPosition; - The position of the "<" of the
                                    last start tag encountered
                                    by the parser.
                                    
   See the XMLParse function header for additional information

**********************************************************/
short ParseXMLTemplate( XmlChar *pBuffBegin,
                        XmlChar *pBuffEnd,
                        short iEntries,
                        ElementLookupEntry *pElements,
                        short (*pfnStartDocument)(),
                        short (*pfnEndDocument)(),
                        short (*pfnStartElement)(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr),
                        short (*pfnEndElement)(XmlChar *pName),
                        short (*pfnElementCharacters)(XmlChar *pName, XmlChar *pChars, unsigned long iLen),
                        unsigned long ulFlags)
{
   short iParseResult = XML_STATUS_OK, iEndResult = XML_STATUS_OK;
   DXMLParser *dxmlPtr = NULL;
   BOOL fValidDocument = FALSE;

   gTemplate.pStart = gTemplate.pCurrentPos = pBuffBegin;
   gTemplate.pEnd = pBuffEnd;
   gTemplate.pStartTagPosition = NULL;
   
   /* malloc the parser structure since it has gotten 
	too large for the stack. */
	dxmlPtr = (DXMLParser*)malloc(sizeof(DXMLParser));
	if (dxmlPtr == NULL)
		return XML_PARSER_INIT_ERR;
	(void)memset((void*)dxmlPtr,0,sizeof(DXMLParser));

   /* we ensure that XMLParserInit has been called !!! */
   iParseResult = XMLParserInit(dxmlPtr,
                                 iEntries,
                                 pElements,
                                 pfnStartDocument,
                                 pfnEndDocument,
                                 pfnStartElement,
                                 pfnEndElement,
                                 pfnElementCharacters,
                                 ulFlags);

   /* if the parser initialized ok */
   if (iParseResult == XML_STATUS_OK)
   {
      /* make sure the there is a document to parse */
      if (pBuffBegin == NULL || pBuffEnd == NULL)
         /* a begin or end pointer is bad */
         iParseResult = XML_PARSE_NULL_PTR_ERR;
      else
      {
         XmlChar       ParseChar;
         XmlTokenState prevState;

         /* Send each of the characters to the parser.  */
         while( *(gTemplate.pCurrentPos) && (iParseResult==XML_STATUS_OK) )
         {
            ParseChar = *(gTemplate.pCurrentPos++);
            prevState = dxmlPtr->m_State;

            #ifdef XML_DEBUG_PRINTPARSE
            dbgTrace(DBG_LVL_INFO, "XMLParse: sending char %c\n", ParseChar);
            #endif

            iParseResult = XMLSendParserChar(dxmlPtr, ParseChar);

            if (prevState != TKN_LT && dxmlPtr->m_State == TKN_LT)
            {
               gTemplate.pStartTagPosition = gTemplate.pCurrentPos-1;
            }

         }
      }
   }

   /* see if the document is valid so far */
   if (iParseResult == XML_STATUS_OK)
      fValidDocument = TRUE;

   /* tell the parser nothing else is coming.  this
   function releases all memory, calls the handler's
   end doc function, and takes care of filtering. */
   iEndResult = XMLParserEndParse(dxmlPtr, fValidDocument);
   
   /* release malloc'd parser */
	free(dxmlPtr);

   if (iParseResult == XML_STATUS_OK)
      return iEndResult;
   else
      return iParseResult;
}




/*************************************************************
Function: XMLParse
Author: Derek DeGennaro
Description:
This function is the parsing function for DXML.  This function
can only be used when the document resides completely in memory.
Buffers must be set up correctly.  Failure to set the pointers pBuffBegin
and pBuffEnd will lead to incorrect results.  pBuffBegin MUST be
LESS THAN OR EQUAL to pBuffEnd.  pBuffBegin points the first
byte, pBuffEnd points to the last byte.  If pBuffEnd points to
the NULL terminator for the buffer, this function will not work.
If NULL termination is required it should be setup like below.
(If no NULL termination, then ignore the NULL in the diagram)

  X X X X X X X X X X X X NULL
  ^                     ^
  |                     |
  pBuffBegin            pBuffEnd

Inputs:
pBuffBegin - This points to the beginning of the XML document.

pBuffEnd - This points to the last valid byte of the XML document.

iEntries - Number of entries in the pEntries table.

pElements - Pointer to a table that contains element names
and the functions to call when we see start tags, characters,
and end tags.  When an element or character data is found
this table is searched.  If an element is found then
the appropriate function is called.  If an element is not
found or pElements if null then the default function pfn***
is called.

pfnStartDocument - Function that is called before parsing.
If this is null then no function is called.

pfnEndDocument - Function that is called after parsing.
If this is null then no function is called.

pfnCharacters - Function that gets called if chardata is
found and there is no lookup table or no match is found
in the data.  This may be null.

pfnStartElement - Function that gets called if start tag is
found and there is no lookup table or no match is found
in the data.  This may be null.

pfnEndElement - Function that gets called if end tag is
found and there is no lookup table or no match is found
in the data.  This may be null.

ulFlags - This value contains bit flags which turn on various
featurs of the parser.  For instance, Turning on the
XMLPARSER_BLINDING_ON flag turns Blinding on.

Outputs:
Return Value:
A DXML error code (see xmltypes.h)
If the function does not return a successfull code (XML_STATUS_OK)
then the parser pointed to by pDXMLParser should not be used
again until it has been reinitialized.  If this structure is
used again it will give an invalid parser error.
**********************************************************/
short XMLParse(XmlChar *pBuffBegin, 
				 XmlChar *pBuffEnd, 
				 short iEntries, 
				 ElementLookupEntry *pElements,
				 short (*pfnStartDocument)(),
				 short (*pfnEndDocument)(),
				 short (*pfnStartElement)(XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr),
				 short (*pfnEndElement)(XmlChar *pName),
				 short (*pfnElementCharacters)(XmlChar *pName, XmlChar *pChars, unsigned long iLen),
				 unsigned long ulFlags)
{
	short iParseResult = XML_STATUS_OK, iEndResult = XML_STATUS_OK;
	DXMLParser *dxmlPtr = NULL;
	XmlChar *pBegin = pBuffBegin;
	BOOL fValidDocument = FALSE;
	
	/* malloc the parser structure since it has gotten 
	too large for the stack. */
	dxmlPtr = (DXMLParser*)malloc(sizeof(DXMLParser));
	if (dxmlPtr == NULL)
		return XML_PARSER_INIT_ERR;
	(void)memset((void*)dxmlPtr,0,sizeof(DXMLParser));	

	/* we ensure that XMLParserInit has been called !!! */
	iParseResult = XMLParserInit(dxmlPtr,
							iEntries,
							pElements,
							pfnStartDocument,
							pfnEndDocument,
							pfnStartElement,
							pfnEndElement,
							pfnElementCharacters,
							ulFlags);

	/* if the parser initialized ok */
	if (iParseResult == XML_STATUS_OK)
	{
		/* make sure the there is a document to parse */
		if (pBuffBegin == NULL || pBuffEnd == NULL)
			/* a begin or end pointer is bad */
			iParseResult = XML_PARSE_NULL_PTR_ERR;
		else
		{
			/* Send each of the characters to the parser.  */
			for (pBegin = pBuffBegin; iParseResult == XML_STATUS_OK && pBegin <= pBuffEnd; pBegin++)
			{
#ifdef XML_DEBUG_PRINTPARSE
				dbgTrace(DBG_LVL_INFO, "XMLParse: sending char %c\n", *pBegin);
#endif
				iParseResult = XMLSendParserChar(dxmlPtr, *pBegin);
			}
		}
	}

	/* see if the document is valid so far */
	if (iParseResult == XML_STATUS_OK)
		fValidDocument = TRUE;
	
	/* tell the parser nothing else is coming.  this
	function releases all memory, calls the handler's
	end doc function, and takes care of filtering. */
	iEndResult = XMLParserEndParse(dxmlPtr, fValidDocument);
	
	/* release malloc'd parser */
	free(dxmlPtr);

	if (iParseResult == XML_STATUS_OK)
		return iEndResult;
	else
		return iParseResult;
}
/**********************************************************
Funciton: XMLSendParserPacket
Author: Derek DeGennaro
Description:
This function is used to send a packet of bytes to the parser.
The function is flexible because it doesn't require the
packets to be any specific size.  The function does require
that the number of valid XmlChar's in the packet is given.
It is left up to the caller of the function to determine this.
So for the packet below

  -----------------------------------------
  |<|a|>|h|e|l|l|o|<|/|a|>| | | | | | | | |
  -----------------------------------------
                         ^
						 |(End of data)
the packet size is 20 but the caller of XMLSendParserPacket
must specify 12 for the value of ulChars.  Also, the 
parser MUST be initialized using XMLParserInit before 
this function is called.

Inputs:
p - pointer to a parser structure. this holds the state of
the parser.
pPacket - a pointer to the beginning of the packet's 
characters.
ulChars - the number of valid characters in the packet, NOT
the number of bytes in the packet.

Return Value:
A DXML error code (see xmltypes.h)
If the function does not return a successfull code (XML_STATUS_OK)
then the parser pointed to by pDXMLParser should not be used
again until it has been reinitialized.  If this structure is
used again it will give an invalid parser error.
**********************************************************/
short XMLSendParserPacket(DXMLParser *p, XmlChar *pPacket, unsigned long ulChars)
{
	short iResult = 0;
	unsigned long i;
	if (p == NULL || pPacket == NULL)
		return XML_PARSE_NULL_PTR_ERR;

	/* we assume that XMLParserInit has been called !!! */

	/* send each of the chars to the parser */
	iResult = XML_STATUS_OK;
	for (i = 0; i < ulChars && iResult == XML_STATUS_OK; i++)
	{
		iResult = XMLSendParserChar(p,pPacket[i]);
	}
	
	return iResult;
}
/**********************************************************
Function: XMLSendParserChar
Author: Derek DeGennaro
Description:
Use this function to give the parser the next character
in the document.  This function may result in a call
to one of the functions in the lookup table, or the
default functions.

Return Value:
A DXML error code (see xmltypes.h)
If the function does not return a successfull code (XML_STATUS_OK)
then the parser pointed to by pDXMLParser should not be used
again until it has been reinitialized.  If this structure is
used again it will give an invalid parser error.
**********************************************************/
/*lint -save -e429 */
short XMLSendParserChar(DXMLParser *p, XmlChar x)
{
	short iResult = XML_STATUS_OK;
	
	if (p == NULL)
		return XML_INVALID_PARSER_ERR;

	/*
	If this parser has found an error it cannot parse
	any more.
	*/
	if (p->m_wErrorStatus != DXML_NO_ERRORS)
		return XML_INVALID_PARSER_ERR;

	/*
	If we have already found EOF the parser needs to be
	reinitialized in order to be used again.
	*/
	if (p->m_wStatus & DXML_EOF_FOUND)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_INVALID_PARSER_ERR;
	}
	
	if (!(p->m_wStatus & DXML_START_CALLED))
	{
		if (p->context == NULL)
		{
			if (p->fnStartDocument != NULL)
				p->fnStartDocument();
		}
		else
		{
			if (p->fnStartDocumentCtx != NULL)
				p->fnStartDocumentCtx(p->context);
		}
		p->m_wStatus |= DXML_START_CALLED;
	}

	/* check to see if we are filtering */
	if (p->m_wStatus & DXML_FILTER_ON)
	{
		/* the filter flag is set, check to see if everything is ok */
		if (p->m_pFilter != NULL && p->m_fnFilterSendChar != NULL)
		{
			/* filter appears to be setup ok */
			iResult = p->m_fnFilterSendChar(p->m_pFilter, x);
			/* check return value */
			if (iResult != XML_STATUS_OK)
			{
				/* the filter has returned an error code */
				p->m_wErrorStatus |= DXML_FILTER_ERR;
				(void)XMLParserReleaseMem(p);
				return XML_PARSER_FILTER_DATA_ERR;
			}
			return iResult;
		}
		else
		{
			/* return error - the filter is not setup
			correctly */
			p->m_wErrorStatus |= DXML_FILTER_ERR;
			
			/* end parser will call the release mem
			functions for the parser and filter */
			(void)XMLParserReleaseMem(p);
			return XML_PARSER_FILTER_ERROR;
		}
			
	}
	else
		return DXMLSendCharUnFiltered(p, x);
}
/*lint -restore */
/*********************************************************
Function: XMLParserSetBlindingActive
Author: Derek DeGennaro
Description:
This function sets the filter for the parser to be a
blinding filter.  This function MUST becalled AFTER
XMLParserInit and BEFORE XMLSendParserChar, XMLSendParserEOF,
XMLSendParserPacket, or XMLParse.  By default, blinding
is inactive (i.e. calling XMLParserInit turns blinding off).
This function cannot be used after a call to XMLSendParserEOF
or XMLParserEndParse.
*********************************************************/
static short XMLParserSetBlindingActive(DXMLParser *p)
{
	/********************************
	Blinding is not supported yet.
	When the blinding mechanism is 
	worked out do stuff here.
	********************************/
	return XML_PARSER_INIT_ERR;
}
/****************************************************************************
m_pTokenFoundFcnTbl
This table contains a list of function pointers.
These functions are called from XMLSendParserToken
when a token has been found.  The token is the
index into this table.  A NULL entry in the
table means that no function exists and
we can safely not call any function.  The
purpose of this table its use are to actually
make the calls to a doc-handler if it exists.
For instance, the fnFoundSTag function would
call the XMLDocHandlerStartElement function
if a doc-handler is registered with the 
parse.
****************************************************************************/
typedef short (*fcnDXMLFoundToken)(void *);
static fcnDXMLFoundToken m_pTokenFoundFcnTbl[NUM_TOK] = {
	NULL,				/* xmldecl */
	NULL,				/* doctypedecl */
	NULL,				/* comment */
	NULL,				/* pi */
	fnFoundWS,			/* S */
	fnFoundEmptyElementTag, /* emptyelemtag */
	fnFoundSTag,		/* stag */
	fnFoundETag,		/* etag */
	fnFoundReference,	/* ref */
	fnFoundCharData,	/* cdsect */
	fnFoundCharData,	/* chardata */
	NULL,				/* EOS */
};
/*****************************************************************************
Function: XMLSendParserToken
Author: Derek DeGennaro
Description:
*****************************************************************************/
static short XMLSendParserToken(DXMLParser *pParser, unsigned char Token)
{
	unsigned char X, reduction, Xindex;
	short i, iHandlerResult = XML_STATUS_OK;
	StackElement stack_element;
#ifdef XML_DEBUG_PRINTPARSE
	int iStackTop = 0;
#endif

	/* a parser is required */
	if (pParser == NULL)
		return XML_INVALID_PARSER_ERR;
	
#ifdef XML_DEBUG_PRINTPARSE
	iStackTop = pParser->m_xmlParseStack.iTop;
	dbgTrace(DBG_LVL_INFO, "Stack: a = %s, state = %d\n", pTokenStr[Token], pParser->m_State);
	PrshortSymbolStack(&pParser->m_xmlParseStack,iStackTop);
#endif

	/* get the top of the stack */
	if (!fnPeekStack(&pParser->m_xmlParseStack,&stack_element)) 
	{ 
		pParser->m_wErrorStatus |= DXML_STACK_ERR;
		return XML_PARSE_STACK_ERR;
	}
	if (stack_element.bType != STACK_BYTE) 
	{
		pParser->m_wErrorStatus |= DXML_STACK_DATA_ERR;
		return XML_BAD_STACK_DATA;
	}
	X = stack_element.data.b;
	
	while (IS_NON_TERM(X) && X != eos)
	{
#ifdef XML_DEBUG_PRINTPARSE
		iStackTop = pParser->m_xmlParseStack.iTop;
		PrshortSymbolStack(&pParser->m_xmlParseStack,iStackTop);
#endif
		Xindex = X - NON_TERM_OFFSET;
		reduction = m_bParseTable[Xindex][Token];
		if (reduction != 0xFF && m_XMLProductions[reduction].left == X)
		{
			if (!fnPopStack(&pParser->m_xmlParseStack,&stack_element)) 
			{
				pParser->m_wErrorStatus |= DXML_STACK_ERR;
				return XML_PARSE_STACK_ERR;
			}
			if (stack_element.bType != STACK_BYTE)
			{
				pParser->m_wErrorStatus |= DXML_STACK_DATA_ERR;
				return XML_BAD_STACK_DATA;
			}
			for (i = m_XMLProductions[reduction].num - 1; i >= 0; i--)
			{
				if (IS_NON_TERM(m_XMLProductions[reduction].right[i]) || m_XMLProductions[reduction].right[i] != NoToken)
				{
					/* Push grammar symbol i onto stack */
					stack_element.data.b = m_XMLProductions[reduction].right[i];
					if (!fnPushStack(&pParser->m_xmlParseStack,stack_element)) 
					{
						pParser->m_wErrorStatus |= DXML_STACK_ERR;
						return XML_PARSE_STACK_ERR;
					}
				}
			}
			if (!fnPeekStack(&pParser->m_xmlParseStack,&stack_element)) 
			{
				pParser->m_wErrorStatus |= DXML_STACK_ERR;
				return XML_PARSE_STACK_ERR;
			}
			if (stack_element.bType != STACK_BYTE)
			{
				pParser->m_wErrorStatus |= DXML_STACK_DATA_ERR;
				return XML_BAD_STACK_DATA;
			}
			X = stack_element.data.b;

		}
		else
		{
			pParser->m_wErrorStatus |= DXML_SYNTAX_ERR;
			return XML_SYNTAX_ERR;
		}

	}

	/* now, the top of the stack should be a token */
	if (Token != X && (!(Token == EOS && X == eos)))
	{
		pParser->m_wErrorStatus |= DXML_SYNTAX_ERR;
		return XML_SYNTAX_ERR;
	}
	else
	{
		if (!fnPopStack(&pParser->m_xmlParseStack, &stack_element))
		{
			pParser->m_wErrorStatus |= DXML_STACK_ERR;
			return XML_PARSE_STACK_ERR;
		}
		if (stack_element.bType != STACK_BYTE)
		{
			pParser->m_wErrorStatus |= DXML_STACK_DATA_ERR;
			return XML_BAD_STACK_DATA;
		}
		
		if (Token < NUM_TOK && m_pTokenFoundFcnTbl[Token] != NULL)
			iHandlerResult = m_pTokenFoundFcnTbl[Token](pParser);
		if (iHandlerResult != XML_STATUS_OK)
		{
			pParser->m_wErrorStatus |= DXML_GENERAL_ERR;
			return iHandlerResult;
		}

	}

	return XML_STATUS_OK;
}
/**********************************************************
Function: XMLSendParserEOF
Author: Derek DeGennaro
Description:
**********************************************************/
static short XMLSendParserEOF(DXMLParser *p)
{
	short iResult = XML_STATUS_OK;

	/* make sure we have a valid parser */
	if (p == NULL)
		return XML_INVALID_PARSER_ERR;

	/*
	If this parser has found an error it cannot parse
	any more.
	*/
	if (p->m_wErrorStatus != DXML_NO_ERRORS || p->m_wStatus & DXML_EOF_FOUND)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_INVALID_PARSER_ERR;
	}
	
	/* set the EOF found flag */
	p->m_wStatus |= DXML_EOF_FOUND;

	/* check to see if we are filtering */
	if (p->m_wStatus & DXML_FILTER_ON)
	{
		/* we are supposedly filtering */
		if (p->m_pFilter != NULL && p->m_fnFilterSendEOF != NULL)
		{
			/* filter appears to be setup ok */
			iResult = p->m_fnFilterSendEOF(p->m_pFilter);
			/* check the result */
			if (iResult != XML_STATUS_OK)
			{
				/* something went wrong */
				p->m_wErrorStatus |= DXML_FILTER_ERR;
				return XML_PARSE_UNEXPECTED_EOF;
			}
			return iResult;
		}
		else
		{
			/* the filter does not appear to be setup ok */
			p->m_wErrorStatus |= DXML_FILTER_ERR;
			return XML_PARSER_FILTER_ERROR;
		}
	}
	else
		return DXMLSendEOFUnFiltered(p);
}


/********************************************************************************/
/* Encoding & Conversion Routines */
/********************************************************************************/
/* 
The utf8-unicode conversion code was taken from Joe's book, the Unicode Standard
version 1.0 Appendix A.
*/
#ifdef DXML_UNICODE
static const unsigned char bytesFromUTF8[256] = {
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,	1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
	2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,	3,3,3,3,3,3,3,3,4,4,4,4,5,5,5,5};

static const unsigned long offsetsFromUTF8[6] = 
{ 0x0UL, 0x3080UL, 0x0E2080UL, 0x3C82080UL, 0xFA082080UL, 0x82082080UL };

static const unsigned long kReplacementCharacter = 0x0000FFFDUL;
static const unsigned long kMaximumSimpleUniChar = 0x0000FFFFUL;
static const unsigned long kMaximumUniChar = 0x0010FFFFUL;

static const short halfShift = 10;
static const unsigned long halfBase = 0x00100000UL;
static const unsigned long halfMask = 0x3FFUL;
static const unsigned long kSurrogateHighStart = 0xD800UL;
static const unsigned long kSurrogateLowStart = 0xDC00UL;

/*************************************************************
Function: ConvertUTF8toUniChar
Author: Derek DeGennaro & Joe's Unicode Book
Description:
This function converts a string of UTF8 bytes shorto simple 
16-bit Unicode values.  The function will continue consuming
bytes until it has filled the given number of Unicode characters
or an error occurs.  An error occurrs when either there are
not enough source bytes, or there are not enough Unicode 
characters.

Inputs:
sourceStart - The beginning of the utf8 data.
sourceEnd - The end of the utf8 data.
targetStart - The beginning of the Unicode data.
targetEnd - The end of the Unicode data.

Return Value:
ok - success
sourceExhausted - There were not enough utf8 bytes to
fill the number of Unicode Bytes.
targetExhausted - There were too many utf8 bytes.
*/
ConversionResult ConvertUTF8toUniChar(UTF8 **sourceStart,
									  UTF8 *sourceEnd,
									  XmlChar** targetStart,
									  const XmlChar *targetEnd)
{
	ConversionResult result = CONVERSION_OK;
	UTF8 *source = *sourceStart;
	XmlChar *target = *targetStart;
	while (source <= sourceEnd)
	{
		unsigned long ch = 0;
		unsigned short extraBytesToWrite = bytesFromUTF8[*source];
		if (source + extraBytesToWrite - 1 > sourceEnd)
		{
			result = CONVERSION_SOURCE_ERR;
		break;
		}
		switch (extraBytesToWrite) {
		case 5: ch+= *source++;	ch <<= 6;
		case 4: ch+= *source++; ch <<= 6;
		case 3: ch+= *source++; ch <<= 6;
		case 2: ch+= *source++; ch <<= 6;
		case 1: ch+= *source++; ch <<= 6;
		case 0:	ch+= *source++;
		};
		ch -= offsetsFromUTF8[extraBytesToWrite];

		if (target > targetEnd)
		{
			result = CONVERSION_TARGET_ERR;
			break;
		}
		if (ch <= kMaximumSimpleUniChar)
			*target++ = (XmlChar)ch;
		else if (ch > kMaximumUniChar)
			*target++ = (XmlChar)kReplacementCharacter;
		else
		{
			if (target + 1 > targetEnd)
			{
				result = CONVERSION_TARGET_ERR;
				break;
			}
			ch -= halfBase;
			*target++ = (XmlChar)((ch >> halfShift) + kSurrogateHighStart);
			*target++ = (XmlChar)((ch & halfMask) + kSurrogateLowStart);
		}
	}
	*sourceStart = source;
	*targetStart = target;
	return result;
}

/*************************************************************
Function: ConvertLatin1toUniChar
Author: Derek DeGennaro & expat
Description:
The latin1-unicode conversion was taken from the xmltok
module of expat.  This function simply converts the bytes of
Latin1 to 16-bit simple Unicode.
*/
ConversionResult ConvertLatin1toUniChar(U_CHAR **sourceStart,
										const U_CHAR *sourceEnd,
										XmlChar **targetStart,
										const XmlChar *targetEnd)
{
	ConversionResult result = CONVERSION_OK;
	U_CHAR *source = *sourceStart;
	XmlChar *target = *targetStart;

	while (source <= sourceEnd && target <= targetEnd)
	{
		*target++ = (XmlChar)*source++;
	}

	*sourceStart = source;
	*targetStart = target;

	return result;
}
#endif
/********************************************************************************/

/**********************************************************
pNonTermStr, pTokenStr
tables that contain the ascii strings for the names of
tokens and non-terminals in the DXML grammar.
**********************************************************/
#ifdef XML_DEBUG_PRINTPARSE
static const char *pNonTermStr[] = {"doc", "prolog","optdecl","mlst","mlst2","optdmis","misc", "element", "content_lst", "content_lst2","content", "eos"};
static const char *pTokenStr[] = { "XMLDec","DocType","Comment","PI", "S", "EmptyElemTag", "STag", "ETag", "Reference","CDSect","CharData","EOS","NoToken"};
#endif


#ifdef XML_DEBUG_PRINTPARSE
static void PrshortProduction(struct Production p)
{
	short i, index;
	index = p.left - NON_TERM_OFFSET;
	dbgTrace(DBG_LVL_INFO, "%s -> ", pNonTermStr[index]);
	for (i = 0; i < p.num; i++)
	{
		if (IS_NON_TERM(p.right[i]))
		{
			index = p.right[i] - NON_TERM_OFFSET;
			dbgTrace(DBG_LVL_INFO, "%s ", pNonTermStr[index]);
		}
		else
		{
			dbgTrace(DBG_LVL_INFO, "%s ", pTokenStr[p.right[i]]);
		}
	}
	dbgTrace(DBG_LVL_INFO, "\n");

}
static void PrshortSymbolStack(Stack *pStack, short iTop)
{
	short i, index;
	unsigned char bValue;
	dbgTrace(DBG_LVL_INFO, "Stack: ");
	for (i = 0; i < pStack->iTop; i++)
	{
		bValue = pStack->pItems[i].data.b;
		if (IS_NON_TERM(bValue))
		{
			index = bValue - NON_TERM_OFFSET;
			dbgTrace(DBG_LVL_INFO, "%s ", pNonTermStr[index]);
		}
		else
		{
			index = bValue;
			dbgTrace(DBG_LVL_INFO, "%s ", pTokenStr[index]);
		}
	}
	dbgTrace(DBG_LVL_INFO, "\n");
}
#endif
/*********************************************************************************/


/* Token Found Functions*******************************************************/
static short fnFoundSTag(void *pParser)
{
	DXMLParser *p = (DXMLParser *)pParser;
	short i, k;
	XMLAttribute pAttr[MAX_XML_TAG_NUM_ATTR/2];
	StackElement stack_element;
	XmlChar *pTemp;

	/* Step 1. Null terminate the name in the buffer */
	if (XMLBuffMgrAddChar(&p->m_xmlTagNameBuff, (XmlChar)0) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_TAG_BUFF_ERR;
	}

	/* Step 2. Put the name of the element on the stack */
	stack_element.bType = STACK_DYN_PTR;
	
	stack_element.data.p = (void*)malloc(sizeof(XmlChar) * p->m_xmlTagNameBuff.iLength);
	if (stack_element.data.p == NULL)
	{
		p->m_wErrorStatus |= DXML_STACK_ERR;
		return XML_PARSE_STACK_ERR;
	}
	pTemp = (XmlChar*)stack_element.data.p;

	memcpy(pTemp, p->m_xmlTagNameBuff.pBuff, p->m_xmlTagNameBuff.iLength); /* includes the NULL */

	if (!fnPushStack(&p->m_xmlElemStack, stack_element))
	{
		p->m_wErrorStatus |= DXML_STACK_ERR;
		return XML_PARSE_STACK_ERR;
	}
	
	/* Step 3. Set the attributes */
	for (i = 0; i < p->m_iXMLAttrListLen; i++)
	{
		k = p->m_pXMLAttrValLen[i];
		p->m_pXMLAttrList[i][k] = 0;
	}
		
	for (k = 0,i = 0; i < p->m_iXMLAttrListLen; i += 2, k += 1)
	{

		pAttr[k].iNameLength = p->m_pXMLAttrValLen[i];
		pAttr[k].pName = p->m_pXMLAttrList[i];
		if ((i + 1) < p->m_iXMLAttrListLen)
		{
			pAttr[k].iValueLength = p->m_pXMLAttrValLen[i+1];
			pAttr[k].pValue = p->m_pXMLAttrList[i+1];
		}
	}
	/* Step 4. call the doc-handler if there is one */
	return DXMLHandleStartElement(p,p->m_xmlTagNameBuff.pBuff, pAttr, (short)(p->m_iXMLAttrListLen/2));
}
static short fnFoundETag(void *pParser)
{
	DXMLParser *p = (DXMLParser *)pParser;
	StackElement stack_element;
	XmlChar *pName;
	short iLength = 0;

	/* Step 0. Terminate the name in the buffer */
	if (XMLBuffMgrAddChar(&p->m_xmlTagNameBuff, (XmlChar)0) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_TAG_BUFF_ERR;
	}
	
	/* Step 1. Pop the element name from the stack */
	if (!fnPopStack(&p->m_xmlElemStack, &stack_element))
	{
		/*
		The stack will be empty if there is no
		matching start tag for this end tag.
		This is a violation of the well-formed
		constraint and is returned as a semantic
		error.
		*/
		p->m_wErrorStatus |= DXML_STACK_ERR;
		return XML_SEMANTIC_ERR;
	}
	if (stack_element.bType != STACK_DYN_PTR)
	{
		p->m_wErrorStatus |= DXML_STACK_DATA_ERR;
		return XML_BAD_STACK_DATA;
	}

	/* Step 2. Determine length of name */
	pName = (XmlChar *)stack_element.data.p;
	while (pName[iLength] != 0)
		iLength += 1;

	/* Step 3. Compare the name from the stack with the current one */
	if (fnCompareXMLStr(pName,p->m_xmlTagNameBuff.pBuff) != 0)
	{
		free(stack_element.data.p);
		p->m_wErrorStatus |= DXML_SEMANTIC_ERR;
		return XML_SEMANTIC_ERR;
	}

	/* Step 4. Release memory for the name */
	free(stack_element.data.p);

	/* Step 5. Call any doc-handler stuff */
	return DXMLHandleEndElement(p,p->m_xmlTagNameBuff.pBuff);
}
static short fnFoundCharData(void *pParser)
{
	short sResult;
	int i;
	unsigned char bCharsOnHold;

	DXMLParser *p = (DXMLParser *)pParser;

	/* null terminate the character buffer */
	if (XMLBuffMgrAddChar(&p->m_xmlCharDataMgr, (XmlChar)0) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_DATA_BUFF_ERR;
	}

	/* length of buffer includes null char, so we subtract it in the call */
	sResult = DXMLHandleCharacters(p,p->m_xmlCharDataMgr.pBuff, (p->m_xmlCharDataMgr.iLength - 1));

	if (sResult == XML_STATUS_OK)
	{
		/* Due to the state transitions, a character(s)
		may need to be added to the char data buffer.
		First record the number of chars, then reset
		the number of chars. */
		bCharsOnHold = p->m_bCharsOnHold;
		p->m_bCharsOnHold = 0;

		/* remove all chars from chardata buffer */
		if (XMLBuffMgrReset(&p->m_xmlCharDataMgr) != XMLBUFFMGR_OK)
		{
			p->m_wErrorStatus |= DXML_GENERAL_ERR;
			return XML_DATA_BUFF_ERR;
		}

		/* add all the chars that were put on hold */
		for (i = 0; i < bCharsOnHold; i++)
		{
			if (XMLBuffMgrAddChar(&p->m_xmlCharDataMgr, p->m_xmlCharsOnHold[i]) != XMLBUFFMGR_OK)
			{
				p->m_wErrorStatus |= DXML_GENERAL_ERR;
				return XML_DATA_BUFF_ERR;
			}
		}
	}

	return sResult;
}
static short fnFoundReference(void *pParser)
{
	DXMLParser *p = (DXMLParser *)pParser;
	XmlChar pTempReference[2];

	pTempReference[0] = p->m_uCharRef;
	pTempReference[1] = 0;

	if (p->m_ReferenceType == XML_REF_CHAR)
	{
		return DXMLHandleCharacters(p,pTempReference,1);
	}
	return XML_STATUS_OK;
}
static short fnFoundEmptyElementTag(void *pParser)
{
	short iResult;
	iResult = fnFoundSTag(pParser);
	if (iResult == XML_STATUS_OK)
		return fnFoundETag(pParser);
	else
		return iResult;
}
static short fnFoundWS(void *pParser)
{
	short sResult;
	int i;
	unsigned char bCharsOnHold;

	DXMLParser *p = (DXMLParser *)pParser;

	/* null terminate the chardata buffer */
	if (XMLBuffMgrAddChar(&p->m_xmlCharDataMgr, (XmlChar)0) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_DATA_BUFF_ERR;
	}

	/* length of buffer includes null char, so we subtract it in the call */
	sResult = DXMLHandleCharacters(p,p->m_xmlCharDataMgr.pBuff, (p->m_xmlCharDataMgr.iLength - 1));

	if (sResult == XML_STATUS_OK)
	{
		/* Due to the state transitions, a character(s)
		may need to be added to the char data buffer.
		First record the number of chars, then reset
		the number of chars. */
		bCharsOnHold = p->m_bCharsOnHold;
		p->m_bCharsOnHold = 0;

		/* remove all chars from chardata buffer */
		if (XMLBuffMgrReset(&p->m_xmlCharDataMgr) != XMLBUFFMGR_OK)
		{
			p->m_wErrorStatus |= DXML_GENERAL_ERR;
			return XML_DATA_BUFF_ERR;
		}

		/* add all the chars that were put on hold */
		for (i = 0; i < bCharsOnHold; i++)
		{
			if (XMLBuffMgrAddChar(&p->m_xmlCharDataMgr, p->m_xmlCharsOnHold[i]) != XMLBUFFMGR_OK)
			{
				p->m_wErrorStatus |= DXML_GENERAL_ERR;
				return XML_DATA_BUFF_ERR;
			}
		}
	}

	return sResult;
}

/*********************************************************************************/
/* Character Matching Routines */
/*********************************************************************************/
short fnIsUCBaseChar(XmlChar x)
{
#ifdef DXML_UNICODE
	if (g_UnicodeCharTable[x] & UNI_BASECHAR_MASK)
		return 1;
	else
		return 0;
#else
	return 0;
#endif
}
short fnIsUCIdeographic(XmlChar x)
{
#ifdef DXML_UNICODE
	if (g_UnicodeCharTable[x] & UNI_IDEOGRAPHIC_MASK)
		return 1;
	else
		return 0;
#else
	return 0;
#endif
}
short fnIsUCXMLChar(XmlChar x)
{
#ifdef DXML_UNICODE
	if (g_UnicodeCharTable[x] & UNI_XMLCHAR_MASK)
		return 1;
	else
		return 0;
#else
	return 1;
#endif
}
short fnIsUCAscii(XmlChar x)
{
#ifdef DXML_UNICODE
	if (g_UnicodeCharTable[x] & UNI_ASCII_MASK)
		return 1;
	else
		return 0;
#else
	if (/* x >= 0 && ---VRO: allways true */ x <= 0x7F)
		return 1;
	else
		return 0;
#endif
}
short fnIsUCLetter(XmlChar x)
{
#ifdef DXML_UNICODE
	if (fnIsUCBaseChar(x) || fnIsUCIdeographic(x))
		return 1;
	else
		return 0;
#else
	if ((x >= 0x41 && x <= 0x5A) || (x >=0x61 && x <= 0x7A))
		return 1;
	else
		return 0;
#endif
	
}
short fnIsUCCapitalLetter(XmlChar x)
{
	if (x >= 0x41 && x <= 0x5A)
		return 1;
	else return 0;
}
short fnIsUCDigit(XmlChar x)
{
#ifdef DXML_UNICODE
	if (g_UnicodeCharTable[x] & UNI_DIGIT_MASK)
		return 1;
	else
		return 0;
#else
	if (x >= 0x30 && x <= 0x39)
		return 1;
	else
		return 0;
#endif
}
short fnIsUCCombiningChar(XmlChar x)
{
#ifdef DXML_UNICODE
	if (g_UnicodeCharTable[x] & UNI_COMBININGCHAR_MASK)
		return 1;
	else
		return 0;
#else
	return 0;
#endif
}
short fnIsUCExtenderChar(XmlChar x)
{
#ifdef DXML_UNICODE
	if (g_UnicodeCharTable[x] & UNI_EXTENDER_MASK)
		return 1;
	else
		return 0;
#else
	return 0;
#endif
}

short fnIsUCNameChar(XmlChar x)
{
	if (fnIsUCLetter(x) || 
		fnIsUCDigit(x) || 
		x == '.' || 
		x == '-' || 
		x == '_' || 
		x == ':' ||
		fnIsUCCombiningChar(x) ||
		fnIsUCExtenderChar(x))
		return 1;
	else
		return 0;
}
short fnIsUCWs(XmlChar x)
{
	if (x == 0x20 || x == 0x9 || x == 0xD || x == 0xA)
		return 1;
	else
		return 0;
}
short fnIsUCQuote(XmlChar x)
{
	if (x == 0x22 || x == 0x27)
		return 1;
	else
		return 0;
}
short fnIsUCHexChar(XmlChar x)
{
	if (fnIsUCDigit(x) || (x >= 0x41 && x <= 0x46) || (x >= 0x61 && x <= 0x66))
		return 1;
	else
		return 0;
}
short fnIsUCVersionNumChar(XmlChar uNext)
{
	if ((fnIsUCLetter(uNext)) ||
		(fnIsUCDigit(uNext)) ||
		(uNext == 0x5F) ||
		(uNext == 0x2E) ||
		(uNext == 0x3A) ||
		(uNext == 0x2D))
		return 1;
	else
		return 0;
}
short fnIsUCPubidChar(XmlChar x)
{
	if ((x == 0x20) ||
		(x == 0x0D) ||
		(x == 0x0A) ||
		(fnIsUCHexChar(x)) ||
		(x == 0x21) ||
		(x >= 0x23 && x <= 0x25) ||
		(x == 0x27) ||
		(x >= 0x28 && x <= 0x2F) ||
		(x >= 0x3A && x <= 0x40) ||
		(x == 0x5F))
		return 1;
	else
		return 0;
}
short fnIsUCLtOrAmpOrGt(XmlChar x)
{
	if (x == 0x3C || x == 0x3E || x == 0x26)
		return 1;
	else
		return 0;
}
short fnIsUCStartNameChar(XmlChar x)
{
	if (fnIsUCLetter(x) || x == 0x5F || x == 0x3A)
		return 1;
	else
		return 0;
}
static short fnDefaultCompareFcn(XmlChar x)
{
	return 1;
}
/****************************************************************
Function: DXMLSendCharUnFiltered
Author: Derek DeGennaro
Description:
This function is the low-level lexical anaylzer for the DXML parser.
This function is a trusted function by the DXML module and
should not be called outside the module.  All error
checking needs to be done outside of this function.  This stuff
used to be included in the main SendChar function but the main
SendChar function now deals with any filters that may be used.
The parser sets up the filter to call this function when it has
recieved a char.
****************************************************************/
/*lint -save -e429 */
static short DXMLSendCharUnFiltered(DXMLParser *p, XmlChar x)
{
	short iEntry = 0, iNumEntries, i, iResult, iActionResult;
	XmlTokenStateEntry entry;
	const XmlTokenStateEntry *pEntries = NULL;
	
	if (p == NULL)
		return XML_INVALID_PARSER_ERR;

	/*
	If this parser has found an error it cannot parse
	any more.
	*/
	if (p->m_wErrorStatus != DXML_NO_ERRORS)
		return XML_INVALID_PARSER_ERR;

	/*
	If we have already found EOF the parser needs to be
	reinitialized in order to be used again.
	*/
	if (p->m_wStatus & DXML_EOF_FOUND)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_INVALID_PARSER_ERR;
	}

	if (p->m_State >= 0 && p->m_State < TKN_LAST_DXML_STATE) /*lint !e568 */
	{
		iNumEntries = XmlTokenStateLookup[p->m_State].iNumEntries;
		if (XmlTokenStateLookup[p->m_State].pEntries != NULL)
		{
			pEntries = XmlTokenStateLookup[p->m_State].pEntries;
			for (i = 0; i < iNumEntries; i++)
			{
				entry = pEntries[i];

				if (entry.fcnCompare != NULL)
					iResult = entry.fcnCompare(x);
				else
					iResult = (x == entry.c);
				if (entry.fNegate)
					iResult = !iResult;
				if (iResult)
				{
					if (entry.fcnAction != NULL)
					{
						iActionResult = entry.fcnAction(p,x);
						if (iActionResult != XML_STATUS_OK)
							return iActionResult;
					}
					if (entry.result != NoToken)
					{
						iResult = XMLSendParserToken(p,entry.result);
						if (iResult != XML_STATUS_OK)
							return iResult;
					}
					p->m_ulTotalCharCount += 1;
					if (entry.newstate == TKN_WS || entry.newstate == TKN_CHARDATA)
						p->m_ulCharDataCount += 1;
						
					else
						p->m_ulMarkupCharCount += 1;
					if (x == (XmlChar)'\n')
						p->m_ulLineNo += 1;
					p->m_State = entry.newstate;
					return XML_STATUS_OK;
				}
			}
		}
	}
	p->m_wErrorStatus |= DXML_TOKEN_ERR;
	return XML_TOKEN_ERR;
}
/*lint -restore */
/**********************************************************
Function: DXMLSendEOFUnFiltered
Author: Derek DeGennaro
Description:
This is the low-level function that tells the DXML parser
that there are no more characters coming.  This stuff
used to be part of the public sendEOF function but
was moved so filters could be used.  This is a trusted
function and should only be used by the DXML module.
**********************************************************/
static short DXMLSendEOFUnFiltered(DXMLParser *p)
{
	if (p == NULL)
		return XML_INVALID_PARSER_ERR;

	/* 
	If we hit EOF, only WS is acceptable.  We will
	ignore WS.  Char data is not acceptable.
	*/
	if (p->context == NULL)
	{
		if (p->fnEndDocument != NULL && !(p->m_wStatus & DXML_END_CALLED))
			p->fnEndDocument();
	}
	else
	{
		if (p->fnEndDocumentCtx != NULL && !(p->m_wStatus & DXML_END_CALLED))
			p->fnEndDocumentCtx(p->context);
	}

	if (p->m_State == TKN_RDY || p->m_State == TKN_WS)
	{
		p->m_wStatus |= DXML_END_CALLED;

		/* send eof token */
		return XMLSendParserToken(p, EOS);
	}
	else
	{
		p->m_wStatus |= DXML_END_CALLED;
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_PARSE_UNEXPECTED_EOF;
	}
}

/* Action Functions for DXML tokens ***********************************************************/
static short fnDXMLActEndCDSect(DXMLParser *p, XmlChar x)
{
	/*
	The last 2 chars in the buffer are the first 2 chars of "]]>"
	and do not count for the char data.
	*/
	p->m_xmlCharDataMgr.iLength -= 2;
	return XML_STATUS_OK;
}
static short fnDXMLActStartSTag(DXMLParser *p, XmlChar x)
{
	short i;
	if (XMLBuffMgrReset(&p->m_xmlTagNameBuff) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_TAG_BUFF_ERR;
	}
	if (XMLBuffMgrAddChar(&p->m_xmlTagNameBuff, x) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_TAG_BUFF_ERR;
	}
	p->m_iXMLAttrListLen = 0;
	for (i = 0; i < MAX_XML_TAG_NUM_ATTR; i++)
		p->m_pXMLAttrValLen[i] = 0;

	return XML_STATUS_OK;
}
static short fnDXMLActNextSTagChar(DXMLParser *p, XmlChar x)
{
	if (XMLBuffMgrAddChar(&p->m_xmlTagNameBuff, x) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_TAG_BUFF_ERR;
	}

	return XML_STATUS_OK;
}
static short fnDXMLActStartAttrName(DXMLParser *p, XmlChar x)
{
	short index = p->m_iXMLAttrListLen;
	if (index < MAX_XML_TAG_NUM_ATTR)
	{
		p->m_pXMLAttrValLen[index] = 1;
		p->m_pXMLAttrList[index][0] = x;
		
	}
	return XML_STATUS_OK;
}
static short fnDXMLActAttrNameChar(DXMLParser *p, XmlChar x)
{
	short iLen, index = p->m_iXMLAttrListLen;
	if (index < MAX_XML_TAG_NUM_ATTR)
	{
		iLen = p->m_pXMLAttrValLen[p->m_iXMLAttrListLen];
		if (iLen < MAX_XML_ATTR_LENGTH)
		{
			p->m_pXMLAttrList[index][iLen] = x;
			p->m_pXMLAttrValLen[p->m_iXMLAttrListLen] += 1;
		}
		else
		{
			p->m_wErrorStatus |= DXML_GENERAL_ERR;
			return XML_ATTR_NAME_BUFF_ERR;
		}
	}
	else
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_ATTR_BUF_ERR;
	}
	return XML_STATUS_OK;
}
static short fnDXMLActStartNextAttrVal(DXMLParser *p, XmlChar x)
{
	p->m_uQuoteValue = x;
	if (p->m_iXMLAttrListLen < MAX_XML_TAG_NUM_ATTR)
		p->m_iXMLAttrListLen += 1;
	else
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_ATTR_BUF_ERR;
	}
	return XML_STATUS_OK;
}
static short fnDXMLActNextAttrValChar(DXMLParser *p, XmlChar x)
{
	short iLen, index = p->m_iXMLAttrListLen;
	if (index < MAX_XML_TAG_NUM_ATTR)
	{
		iLen = p->m_pXMLAttrValLen[p->m_iXMLAttrListLen];
		if (iLen < MAX_XML_ATTR_LENGTH)
		{
			p->m_pXMLAttrList[index][iLen] = x;
			p->m_pXMLAttrValLen[p->m_iXMLAttrListLen] += 1;
		}
		else
		{
			p->m_wErrorStatus |= DXML_GENERAL_ERR;
			return XML_ATTR_VAL_BUFF_ERR;
		}
	}
	else
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_ATTR_BUF_ERR;
	}
	return XML_STATUS_OK;
}
static short fnDXMLActEndAttrValChar(DXMLParser *p, XmlChar x)
{
	if (p->m_iXMLAttrListLen < MAX_XML_TAG_NUM_ATTR)
		p->m_iXMLAttrListLen += 1;
	return XML_STATUS_OK;
}

static short fnDXMLActEndAttrNameRef(DXMLParser *p, XmlChar x)
{
	short iResult, index, iLen;
	iResult = fnDXMLActEndNameRef(p, x);
	if (iResult != XML_STATUS_OK)
		return iResult;
	index = p->m_iXMLAttrListLen;
	if (index < MAX_XML_TAG_NUM_ATTR)
	{
		iLen = p->m_pXMLAttrValLen[index];
		if (iLen < MAX_XML_ATTR_LENGTH)
		{
			p->m_pXMLAttrList[index][iLen] = p->m_uCharRef;
			p->m_pXMLAttrValLen[index] += 1;
		}
		else
		{
			p->m_wErrorStatus |= DXML_GENERAL_ERR;
			return XML_ATTR_VAL_BUFF_ERR;
		}
	}
	else
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_ATTR_BUF_ERR;
	}

	return XML_STATUS_OK;
}

static short fnDXMLActEndAttrHexRef(DXMLParser *p, XmlChar x)
{
	short iResult, index, iLen;
	iResult = fnDXMLActEndHexCharRef(p, x);
	if (iResult != XML_STATUS_OK)
		return iResult;
	index = p->m_iXMLAttrListLen;
	if (index < MAX_XML_TAG_NUM_ATTR)
	{
		iLen = p->m_pXMLAttrValLen[index];
		if (iLen < MAX_XML_ATTR_LENGTH)
		{
			p->m_pXMLAttrList[index][iLen] = p->m_uCharRef;
			p->m_pXMLAttrValLen[index] += 1;
		}
		else
		{
			p->m_wErrorStatus |= DXML_GENERAL_ERR;
			return XML_ATTR_VAL_BUFF_ERR;
		}
	}
	else
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_ATTR_BUF_ERR;
	}

	return XML_STATUS_OK;
}
static short fnDXMLActEndAttrDigitRef(DXMLParser *p, XmlChar x)
{
	short iResult, index, iLen;
	iResult = fnDXMLActEndCharRef(p, x);
	if (iResult != XML_STATUS_OK)
		return iResult;
	index = p->m_iXMLAttrListLen;
	if (index < MAX_XML_TAG_NUM_ATTR)
	{
		iLen = p->m_pXMLAttrValLen[index];
		if (iLen < MAX_XML_ATTR_LENGTH)
		{
			p->m_pXMLAttrList[index][iLen] = p->m_uCharRef;
			p->m_pXMLAttrValLen[index] += 1;
		}
		else
		{
			p->m_wErrorStatus |= DXML_GENERAL_ERR;
			return XML_ATTR_VAL_BUFF_ERR;
		}
	}
	else
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_ATTR_BUF_ERR;
	}

	return XML_STATUS_OK;
}
static short fnDXMLActStartNameRef(DXMLParser *p, XmlChar x)
{
	p->m_ReferenceType = XML_REF_ENTITY;
	if (XMLBuffMgrReset(&p->m_xmlCharDataMgr) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_REF_BUFF_ERR;
	}
	if (XMLBuffMgrAddChar(&p->m_xmlCharDataMgr, x) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_REF_BUFF_ERR;
	}

	return XML_STATUS_OK;
}
static short fnDXMLActNextNameRefChar(DXMLParser *p, XmlChar x)
{
	if (XMLBuffMgrAddChar(&p->m_xmlCharDataMgr, x) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_REF_BUFF_ERR;
	}
	return XML_STATUS_OK;
}
static short fnDXMLActEndNameRef(DXMLParser *p, XmlChar x)
{
	unsigned long i, iLen;
	for (i = 0; i < 5; i++)
	{
		iLen = strlen((char*)EscapeSeq[i].pName);
		if (p->m_xmlCharDataMgr.iLength == iLen && (memcmp(p->m_xmlCharDataMgr.pBuff,EscapeSeq[i].pName, iLen) == 0))
		{
			p->m_ReferenceType = XML_REF_CHAR;
			p->m_uCharRef = EscapeSeq[i].xChar;
			return XML_STATUS_OK;
		}
	}
	/* no dtd defined */
	p->m_wErrorStatus |= DXML_SEMANTIC_ERR;
	return XML_SEMANTIC_ERR;
}
static short fnDXMLActStartCharRef(DXMLParser *p, XmlChar x)
{
	p->m_ReferenceType = XML_REF_CHAR;
	if (XMLBuffMgrReset(&p->m_xmlCharDataMgr) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_REF_BUFF_ERR;
	}
	if (XMLBuffMgrAddChar(&p->m_xmlCharDataMgr, x) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_REF_BUFF_ERR;
	}
	return XML_STATUS_OK;
}
static short fnDXMLActNextCharRefChar(DXMLParser *p, XmlChar x)
{
	if (XMLBuffMgrAddChar(&p->m_xmlCharDataMgr, x) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_REF_BUFF_ERR;
	}
	return XML_STATUS_OK;
}
static short fnDXMLActEndCharRef(DXMLParser *p, XmlChar x)
{
	unsigned long ulTemp = 0, ulPower = 1;
	unsigned long i;
	for (i = p->m_xmlCharDataMgr.iLength - 1; i >= 0; i--, ulPower *= 10)
	{
		ulTemp += ((p->m_xmlCharDataMgr.pBuff[i] - '0') * ulPower);
#ifdef DXML_UNICODE
		if (ulTemp > 65535)
		{
			p->m_fSematicError = TRUE;
			return XML_SEMANTIC_ERR;
		}
#else
		if (ulTemp > 255)
		{
			p->m_wErrorStatus |= DXML_SEMANTIC_ERR;
			return XML_SEMANTIC_ERR;
		}
#endif
	}
	p->m_uCharRef = (XmlChar)ulTemp;
	return XML_STATUS_OK;
}
static short fnDXMLActStartHexRefAttr(DXMLParser *p, XmlChar x)
{
	p->m_ReferenceType = XML_REF_CHAR;
	if (XMLBuffMgrReset(&p->m_xmlCharDataMgr) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_REF_BUFF_ERR;
	}
	if (XMLBuffMgrAddChar(&p->m_xmlCharDataMgr, x) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_REF_BUFF_ERR;
	}
	return XML_STATUS_OK;
}
static short fnDXMLActStartHexCharRef(DXMLParser *p, XmlChar x)
{
	p->m_ReferenceType = XML_REF_CHAR;
	if (XMLBuffMgrReset(&p->m_xmlCharDataMgr) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_REF_BUFF_ERR;
	}
	return XML_STATUS_OK;
}
static short fnDXMLActNextHexRefChar(DXMLParser *p, XmlChar x)
{
	if (XMLBuffMgrAddChar(&p->m_xmlCharDataMgr, x) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_REF_BUFF_ERR;
	}
	return XML_STATUS_OK;
}
static short fnDXMLActEndHexCharRef(DXMLParser *p, XmlChar x)
{
	unsigned long ulTemp = 0, ulPower = 1;
	unsigned long i;
	for (i = p->m_xmlCharDataMgr.iLength - 1; i >= 0; i--, ulPower *= 16)
	{
		if (fnIsUCDigit(p->m_xmlCharDataMgr.pBuff[i]))
			ulTemp += ((p->m_xmlCharDataMgr.pBuff[i] - '0') * ulPower);
		else if (p->m_xmlCharDataMgr.pBuff[i] >= 'a' && p->m_xmlCharDataMgr.pBuff[i] <= 'f')
			ulTemp += ((p->m_xmlCharDataMgr.pBuff[i] - 'a' + 10) * ulPower);
		else if (p->m_xmlCharDataMgr.pBuff[i] >= 'A' && p->m_xmlCharDataMgr.pBuff[i] <= 'F')
			ulTemp += ((p->m_xmlCharDataMgr.pBuff[i] - 'A' + 10) * ulPower);
#ifdef DXML_UNICODE
		if (ulTemp > 65535)
		{
			p->m_fSematicError = TRUE;
			return XML_SEMANTIC_ERR;
		}
#else
		if (ulTemp > 255)
		{
			p->m_wErrorStatus |= DXML_SEMANTIC_ERR;
			return XML_SEMANTIC_ERR;
		}
#endif
	}
	p->m_uCharRef = (XmlChar)ulTemp;
	return XML_STATUS_OK;
}
static short fnDXMLActStartWS(DXMLParser *p, XmlChar x)
{
	if (XMLBuffMgrReset(&p->m_xmlCharDataMgr) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_WS_BUFF_ERR;
	}
	if (XMLBuffMgrAddChar(&p->m_xmlCharDataMgr, x) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_WS_BUFF_ERR;
	}
	return XML_STATUS_OK;
}
static short fnDXMLActWSChar(DXMLParser *p, XmlChar x)
{
	if (XMLBuffMgrAddChar(&p->m_xmlCharDataMgr, x) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_WS_BUFF_ERR;
	}
	return XML_STATUS_OK;
}
static short fnDXMLActStartCDSect(DXMLParser *p, XmlChar x)
{
	if (XMLBuffMgrReset(&p->m_xmlCharDataMgr) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_DATA_BUFF_ERR;
	}
	return XML_STATUS_OK;
}
static short fnDXMLActStartCharData(DXMLParser *p, XmlChar x)
{
	if (XMLBuffMgrReset(&p->m_xmlCharDataMgr) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_DATA_BUFF_ERR;
	}
	if (XMLBuffMgrAddChar(&p->m_xmlCharDataMgr, x) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_DATA_BUFF_ERR;
	}
	return XML_STATUS_OK;
}
static short fnDXMLNextChardataChar(DXMLParser *p, XmlChar x)
{
	if (XMLBuffMgrAddChar(&p->m_xmlCharDataMgr, x) != XMLBUFFMGR_OK)
	{
		p->m_wErrorStatus |= DXML_GENERAL_ERR;
		return XML_DATA_BUFF_ERR;
	}
	return XML_STATUS_OK;
}
static short fnDXMLEndStartChars(DXMLParser *p, XmlChar x)
{
	p->m_bCharsOnHold = 1;
	p->m_xmlCharsOnHold[0] = x;

	return XML_STATUS_OK;
}

/* Buffer Manager Stuff ***********************************/
/* default sizes of blocks */
#define XMLBUFFMGR_DEF_SIZE 100	/* arbitrarily chosen, MUST be greater than or equal to 1 */
/* initialize mgr (allocates memory) */
static short XMLBuffMgrInit(XMLBuffMgr *pMgr)
{
	if (pMgr == NULL)
		return XMLBUFFMGR_ERROR;
	pMgr->iLength = 0;
	pMgr->iLimit = 0;
	pMgr->pBuff = (XmlChar*)malloc(sizeof(XmlChar)*XMLBUFFMGR_DEF_SIZE);
	if (pMgr->pBuff == NULL)
		return XMLBUFFMGR_ERROR;
	pMgr->iLimit = XMLBUFFMGR_DEF_SIZE - 1;
	return XMLBUFFMGR_OK;
}
/* releases all memory for the mgr */
static short XMLBuffMgrReleaseMem(XMLBuffMgr *pMgr)
{
	if (pMgr == NULL)
		return XMLBUFFMGR_ERROR;
	if (pMgr->pBuff != NULL)
		free(pMgr->pBuff);
	pMgr->iLength = pMgr->iLimit = 0;
	pMgr->pBuff = NULL;
	return XMLBUFFMGR_OK;
}
/* throws out all chars in buffer (does not deallocate mem) */
static short XMLBuffMgrReset(XMLBuffMgr *pMgr)
{
	if (pMgr == NULL)
		return XMLBUFFMGR_ERROR;
	pMgr->iLength = 0;
	return XMLBUFFMGR_OK;
}
/* add another char to buffer (allocates mem if necessary) */
static short XMLBuffMgrAddChar(XMLBuffMgr *pMgr, XmlChar x)
{
	unsigned long iNewLimit;
	XmlChar *pTemp = NULL;
	if (pMgr == NULL)
		return XMLBUFFMGR_ERROR;
	if (pMgr->iLength == pMgr->iLimit)
	{
		/* we need more memory */
		iNewLimit = pMgr->iLimit + XMLBUFFMGR_DEF_SIZE;
		pTemp = (XmlChar*)malloc(sizeof(XmlChar)*iNewLimit);
		if (pTemp == NULL)
		{
			return XMLBUFFMGR_ERROR;
		}
		else
		{
			memcpy(pTemp, pMgr->pBuff, pMgr->iLength * sizeof(XmlChar));
			free(pMgr->pBuff);
			pMgr->pBuff = pTemp;
			pMgr->iLimit = iNewLimit;
		}
	}
	pMgr->pBuff[pMgr->iLength++] = x;
	return XMLBUFFMGR_OK;
}

/************************************************************/
/* Built in doc-handler */
/************************************************************/
static short DXMLHandleStartElement(DXMLParser *p, XmlChar *pName, XMLAttribute *ppAttrList, short iNumAttr)
{
	short iIndex,iResult = XML_STATUS_OK;

	if (p->m_xmlLookup.iNumElements > 0 && p->m_xmlLookup.pEntry != NULL)
	{
		iIndex = SearchLookupTable(p->m_xmlLookup,pName);
		if (iIndex >= 0 && iIndex <= p->m_xmlLookup.iNumElements)
		{
			/* an entry was found, if there is a startelem fcn, call it */
			if (p->m_xmlLookup.pEntry[iIndex].handler.startElement != NULL)
			{
				return p->m_xmlLookup.pEntry[iIndex].handler.startElement(pName, ppAttrList,iNumAttr);
			}
		}
	}
	
	/* no entry, if there is a default fcn call it */
	if (p->context == NULL)
	{
		if (p->fnStartElement != NULL)
			iResult = p->fnStartElement(pName, ppAttrList,iNumAttr);
	}
	else
	{
		if (p->fnStartElementCtx != NULL)
			iResult = p->fnStartElementCtx(p->context, pName, ppAttrList,iNumAttr);
	}
	
	return iResult;
}
static short DXMLHandleCharacters(DXMLParser *p, XmlChar *pChars, unsigned long iLen)
{
	XmlChar *pName=NULL;
	StackElement stack_element;
	short iIndex, iResult = XML_STATUS_OK;
	if (fnPeekStack(&p->m_xmlElemStack,&stack_element))
	{
		pName = (XmlChar*)stack_element.data.p;
		if (p->m_xmlLookup.iNumElements > 0 && p->m_xmlLookup.pEntry != NULL)
		{
			iIndex = SearchLookupTable(p->m_xmlLookup,pName);
			if (iIndex >= 0 && iIndex <= p->m_xmlLookup.iNumElements)
			{
				/* an entry was found, if there is a startelem fcn, call it */
				if (p->m_xmlLookup.pEntry[iIndex].handler.characters != NULL)
				{
					return p->m_xmlLookup.pEntry[iIndex].handler.characters(pName, pChars, iLen);
				}
			}
		}
	}
	
	/* no entry, if there is a default fcn call it */
	if (p->context == NULL)
	{
		if (p->fnCharacters != NULL)
			iResult = p->fnCharacters(pName, pChars,iLen);
	}
	else
	{
		if (p->fnCharactersCtx != NULL)
			iResult = p->fnCharactersCtx(p->context, pName, pChars,iLen);
	}
	
	return iResult;
}
static short DXMLHandleEndElement(DXMLParser *p, XmlChar *pName)
{
	short iIndex, iResult = XML_STATUS_OK;

	if (p->m_xmlLookup.iNumElements > 0 && p->m_xmlLookup.pEntry != NULL)
	{
		iIndex = SearchLookupTable(p->m_xmlLookup,pName);
		if (iIndex >= 0 && iIndex <= p->m_xmlLookup.iNumElements)
		{
			/* an entry was found, if there is a startelem fcn, call it */
			if (p->m_xmlLookup.pEntry[iIndex].handler.endElement != NULL)
			{
				return p->m_xmlLookup.pEntry[iIndex].handler.endElement(pName);
			}
		}
	}

	/* no entry, if there is a default fcn call it */
	if (p->context == NULL)
	{
		if (p->fnEndElement != NULL)
			iResult = p->fnEndElement(pName);
	}
	else
	{
		if (p->fnEndElementCtx != NULL)
			iResult = p->fnEndElementCtx(p->context, pName);
	}

	return iResult;
}
/************************************************************/
short SearchLookupTable(ElementLookup lookup, XmlChar *pKey)
{
	/* Linear search of the lookup table for the element pKey. */
	short low = 0, hi = lookup.iNumElements - 1, iResult;
	if (pKey == NULL) return -1;
	while (low <= hi)
	{
		iResult = fnCompareXMLStr(pKey,lookup.pEntry[low].pName);
		if (iResult == 0)
			return low;
		low++;
	}
	return -1;
}

/************************************************************/
#ifdef DXML_TEST_HARNESS
typedef struct tagDXMLUnitTestScript {
	char *pName;
	const XmlChar *pTest;
	short result;
} DXMLUnitTestScript;
static const DXMLUnitTestScript DXMLUnitTestScript1[] = 
{{"105","<abcd z='&gt;'>hi &#x32;&#10;&gt; <![CDATA[sowefnew]]]><def/><ghi></ghi></abcd>", XML_STATUS_OK},
{"0", "<tag a='&gt;&#48;&#x31;'/>", XML_STATUS_OK},
{"1", "<tag>abc</tag>", XML_STATUS_OK},
{"2", "<tag></tag>", XML_STATUS_OK},
{"3", "</tag>  </tag>", XML_SYNTAX_ERR},
{"4", "<", XML_PARSE_UNEXPECTED_EOF},
{"5", "<a",XML_PARSE_UNEXPECTED_EOF},
{"6", "<_", XML_PARSE_UNEXPECTED_EOF},
{"7", "<:", XML_PARSE_UNEXPECTED_EOF},
{"8", "<ab", XML_PARSE_UNEXPECTED_EOF},
{"9", "<ab></ab>", XML_STATUS_OK},
{"10", "<ab/", XML_PARSE_UNEXPECTED_EOF},
{"11", "<ab/>", XML_STATUS_OK},
{"12", "<ab ", XML_PARSE_UNEXPECTED_EOF},
{"13", "<ab  ", XML_PARSE_UNEXPECTED_EOF},
{"14", "<ab ></ab>", XML_STATUS_OK},
{"15", "<ab /", XML_PARSE_UNEXPECTED_EOF},
{"16", "<ab />", XML_STATUS_OK},
{"17", "<ab c", XML_PARSE_UNEXPECTED_EOF},
{"18", "<ab _", XML_PARSE_UNEXPECTED_EOF},
{"19", "<ab :", XML_PARSE_UNEXPECTED_EOF},
{"20", "<ab cd", XML_PARSE_UNEXPECTED_EOF},
{"21",  "<ab cd=", XML_PARSE_UNEXPECTED_EOF},
{"22","<ab cd =", XML_PARSE_UNEXPECTED_EOF},
{"23", "<ab cd = ", XML_PARSE_UNEXPECTED_EOF},
{"24",  "<ab cd='", XML_PARSE_UNEXPECTED_EOF},
{"25", "<ab cd='<", XML_TOKEN_ERR},
{"26",  "<ab cd='e", XML_PARSE_UNEXPECTED_EOF},
{"27",  "<ab cd='e'", XML_PARSE_UNEXPECTED_EOF},
{"28", "<ab cd='e'></ab>", XML_STATUS_OK},
{"29", "<ab cd='e'/>", XML_STATUS_OK},
{"30", "<ab cd='&'>", XML_TOKEN_ERR},
{"31",  "<ab cd='&~'", XML_TOKEN_ERR},
{"32",  "<ab cd='&e>", XML_TOKEN_ERR},
{"33","<ab cd='_'></ab>", XML_STATUS_OK},
{"34", "<ab cd=':'></ab>", XML_STATUS_OK},
{"35",  "<ab cd='ef'></ab>", XML_STATUS_OK},
{"36",  "<ab cd='&_;'></ab>", XML_SEMANTIC_ERR},
{"37", "<ab cd='&a;'></ab>", XML_SEMANTIC_ERR},
{"38", "<ab cd='&:;'></ab>", XML_SEMANTIC_ERR},
{"39", "<ab cd='&ab;'></ab>",XML_SEMANTIC_ERR},
{"40",  "<ab cd='de&ab;'></ab>", XML_SEMANTIC_ERR},
{"41",  "<ab cd= 'de&ab;'></ab>",XML_SEMANTIC_ERR},
{"42",  "<ab cd = 'de&ab;'></ab>", XML_SEMANTIC_ERR},
{"42a", "<ab cd='&gt;'></ab>", XML_STATUS_OK},
{"43",  "<ab cd='&#'>", XML_TOKEN_ERR},
{"44",  "<ab cd='&#x'>", XML_TOKEN_ERR},
{"45", "<ab cd='&#xm'>", XML_TOKEN_ERR},
{"46",  "<ab cd='&#xm;'>", XML_TOKEN_ERR},
{"47", "<ab cd='&#x1'>", XML_TOKEN_ERR},
{"48", "<ab cd='&#x1';>", XML_TOKEN_ERR},
{"49",  "<ab cd='&#x1;'></ab>", XML_STATUS_OK},
{"50", "<ab cd='&#xabb;'></ab>", XML_SEMANTIC_ERR},
{"50a", "<ab cd='&#xab;'></ab>", XML_STATUS_OK},
{"51",  "<ab cd='&#1a;'>", XML_TOKEN_ERR},
{"52",  "<ab cd='&#1';>", XML_TOKEN_ERR},
{"53", "<ab cd='&#1;'></ab>", XML_STATUS_OK},
{"54", "<ab cd='&#1;lmno'></ab>", XML_STATUS_OK},
{"55",  "<", XML_PARSE_UNEXPECTED_EOF},
{"56", "<2", XML_TOKEN_ERR},
{"57",  "</~>", XML_TOKEN_ERR},
{"58","</ab", XML_PARSE_UNEXPECTED_EOF},
{"59", "<ab></ab>", XML_STATUS_OK},
{"60","</ab l>", XML_TOKEN_ERR},
{"61", "<ab></ab  >", XML_STATUS_OK},
{"62", "<ab></ab >", XML_STATUS_OK},
{"63", 	"<!>", XML_TOKEN_ERR},
{"64", 	"< !---->", XML_TOKEN_ERR},
{"65",	"<! --->", XML_TOKEN_ERR},
{"66", 	"<![]>", XML_TOKEN_ERR},
{"67", 	"<![C]>", XML_TOKEN_ERR},
{"68", 	"<![CD]>", XML_TOKEN_ERR},
{"69", 	"<![CDA]>", XML_TOKEN_ERR},
{"70", 	"<![CDAT]>", XML_TOKEN_ERR},
{"71", 	"<![CDATA]>", XML_TOKEN_ERR},
{"72", 	"<![CDATA[]>", XML_TOKEN_ERR},
{"73", 	"<![CDATA[]]2>", XML_TOKEN_ERR},
{"74", 	"<ab><![CDATA[]]></ab>", XML_STATUS_OK},
{"75", 	"<ab><![CDATA[]1]]2]]]3]]]></ab>", XML_STATUS_OK},
{"76", 	"<!-a>", XML_TOKEN_ERR},
{"77",	"<!-->", XML_TOKEN_ERR},
{"78", 	"<!--->", XML_TOKEN_ERR},
{"79",	"<!---a>", XML_TOKEN_ERR},
{"80", 	"<!---a->", XML_TOKEN_ERR},
{"81", 	"<!---a--a>", XML_TOKEN_ERR},
{"82", 	"<ab><!---a--></ab>", XML_STATUS_OK},
{"83",	"<!D>", XML_TOKEN_ERR},
{"84", 	"<!DO>", XML_TOKEN_ERR},
{"85", 	"<!DOC>", XML_TOKEN_ERR},
{"86",	"<!DOCT>", XML_TOKEN_ERR},
{"87",	"<!DOCTY>", XML_TOKEN_ERR},
{"88", 	"<!DOCTYP>", XML_TOKEN_ERR},
{"89",	"<!DOCTYPE><ab/>", XML_STATUS_OK},
{"90", 	"<!DOCTYPE ><ab/>", XML_STATUS_OK},
{"91", 	"<!DOCTYPE [>", XML_PARSE_UNEXPECTED_EOF},
{"92",	"<!DOCTYPE [a>", XML_PARSE_UNEXPECTED_EOF},
{"93", 	"<!DOCTYPE [a]b>", XML_TOKEN_ERR},
{"94", 	"<!DOCTYPE [a]><ab/>", XML_STATUS_OK},
{"95", 	"<?X>", XML_TOKEN_ERR},
{"96", 	"<?XM>", XML_TOKEN_ERR},
{"97", 	"<?XML>", XML_TOKEN_ERR},
{"98", 	"<?xMl?a>", XML_TOKEN_ERR},
{"99", 	"<?XmL?><ab/>", XML_STATUS_OK},
{"100", 	"<?xll?><ab/>", XML_STATUS_OK},
{"101",		"<?XMm?><ab/>", XML_STATUS_OK},
{"102", 	"<?XMz?><ab/>", XML_STATUS_OK},
{"103", 	"<?abc ?><ab/>", XML_STATUS_OK},
{"104", 	"<?abc lmnop?><ab/>", XML_STATUS_OK},
{"106", "<3", XML_TOKEN_ERR},
{"107", "<a<",XML_TOKEN_ERR},
{"108", "<_<", XML_TOKEN_ERR},
{"109", "<:&", XML_TOKEN_ERR},
{"110", "<ab&", XML_TOKEN_ERR},
{"111", "<ab/?", XML_TOKEN_ERR},
{"112", "<ab ?", XML_TOKEN_ERR},
{"113", "<ab  \\", XML_TOKEN_ERR},
{"114", "<ab /\\", XML_TOKEN_ERR},
{"115", "<ab c\\", XML_TOKEN_ERR},
{"116", "<ab _\\", XML_TOKEN_ERR},
{"117", "<ab :+", XML_TOKEN_ERR},
{"118", "<ab cd\\", XML_TOKEN_ERR},
{"119",  "<ab cd=\\", XML_TOKEN_ERR},
{"120","<ab cd =\\", XML_TOKEN_ERR},
{"121", "<ab cd = \\", XML_TOKEN_ERR},
{"122",  "<ab cd='<", XML_TOKEN_ERR},
{"123",  "<ab cd='e<", XML_TOKEN_ERR},
{"124",  "<ab cd='e'?", XML_TOKEN_ERR},
{"125","<a>abc></a>",XML_STATUS_OK},
};
/*DXMLParser g_dxmlUnitTest;*/

static short fnDXMLUnitTest1()
{
	short iEntries, i, iResult, iStrLength, j;
	DXMLUnitTestScript entry;
	DXMLParser g_dxmlUnitTest;
	iEntries = sizeof (DXMLUnitTestScript1) / sizeof (DXMLUnitTestScript);
	for (i = 0; i < iEntries; i++)
	{
		iResult = XML_STATUS_OK;
		entry = DXMLUnitTestScript1[i];
		
		iStrLength = 0;
		while (entry.pTest[iStrLength] != 0)
			iStrLength += 1;
		
		iResult = XMLParse(entry.pTest,
							entry.pTest + strlen((char*)entry.pTest) - 1,
							0,		// # handler entries
							NULL,	// handler entries
							NULL,	// start fcn
							NULL,	// end fcn
							NULL,	// elem start fcn
							NULL,	// elem end fcn 
							NULL,	// char fcn
							0);

		if (iResult != entry.result)
			return 0;
	}
	return 1;

}

/* tests blinding */
/* XML... <BlindMsg>***b64***</BlindMsg> ... XML... */
static const char g_pBlindXML1[] = {
	0x3C, 0x42, 0x6C, 0x69, 0x3E, 0x0D, 0x0A, 0x3C, 0x68, 0x65, 0x6C, 0x6C, 0x6F, 0x3E, 0x0D, 0x0A, 
	0x3C, 0x42, 0x6C, 0x69, 0x6E, 0x64, 0x4D, 0x73, 0x67, 0x3E, 0x50, 0x45, 0x4A, 0x76, 0x62, 0x32, 
	0x74, 0x7A, 0x50, 0x67, 0x30, 0x4B, 0x49, 0x43, 0x41, 0x67, 0x50, 0x45, 0x4A, 0x76, 0x62, 0x32, 
	0x73, 0x78, 0x50, 0x67, 0x30, 0x4B, 0x49, 0x43, 0x41, 0x67, 0x49, 0x43, 0x41, 0x67, 0x56, 0x47, 
	0x68, 0x70, 0x63, 0x79, 0x42, 0x70, 0x63, 0x79, 0x42, 0x69, 0x62, 0x32, 0x39, 0x72, 0x4D, 0x51, 
	0x30, 0x4B, 0x49, 0x43, 0x41, 0x67, 0x49, 0x43, 0x41, 0x67, 0x50, 0x46, 0x52, 0x70, 0x64, 0x47, 
	0x78, 0x6C, 0x50, 0x6B, 0x64, 0x79, 0x59, 0x58, 0x42, 0x6C, 0x63, 0x79, 0x42, 0x76, 0x5A, 0x69, 
	0x42, 0x58, 0x63, 0x6D, 0x46, 0x30, 0x61, 0x44, 0x77, 0x76, 0x56, 0x47, 0x6C, 0x30, 0x62, 0x47, 
	0x55, 0x2B, 0x44, 0x51, 0x6F, 0x67, 0x49, 0x43, 0x41, 0x67, 0x49, 0x43, 0x41, 0x38, 0x55, 0x48, 
	0x4A, 0x70, 0x59, 0x32, 0x55, 0x2B, 0x4A, 0x44, 0x4D, 0x31, 0x4C, 0x6A, 0x41, 0x77, 0x50, 0x43, 
	0x39, 0x51, 0x63, 0x6D, 0x6C, 0x6A, 0x5A, 0x54, 0x34, 0x4E, 0x43, 0x69, 0x41, 0x67, 0x49, 0x44, 
	0x77, 0x76, 0x51, 0x6D, 0x39, 0x76, 0x61, 0x7A, 0x45, 0x2B, 0x44, 0x51, 0x6F, 0x38, 0x4C, 0x30, 
	0x4A, 0x76, 0x62, 0x32, 0x74, 0x7A, 0x50, 0x67, 0x30, 0x4B, 0x3C, 0x2F, 0x42, 0x6C, 0x69, 0x6E, 
	0x64, 0x4D, 0x73, 0x67, 0x3E, 0x0D, 0x0A, 0x3C, 0x2F, 0x68, 0x65, 0x6C, 0x6C, 0x6F, 0x3E, 0x0D, 
	0x0A, 0x3C, 0x2F, 0x42, 0x6C, 0x69, 0x3E, 0x00 };

static const DXMLUnitTestScript DXMLUnitTestScript2[] = 
{{"1", "<def><efg a1='&gt;&#49;&#x34;efg'>some chardata<![CDATA[hello]]>&lt;&#53;&#x35;<abc/></efg></def>", 1},
{"2", "<B><B/><Bl></Bl><Bl/><Bli></Bli><Bli/><Blin></Blin><Blin/><Blind></Blind><Blind/></B>", 1},
{"3", "<BlindM><BlindM/><BlindMs></BlindMs><BlindMs/></BlindM>", 1},
{"4", "<BlindMsg1><BlindMsg1/></BlindMsg1>", 1},
{"5", "<BlindMsg>", 0},
{"6", "<BlindMsg/>", 0},
{"6a", "<abc><BlindMsg/></abc>", 1},
{"7", "<BlindMsg>A</BlindMsg>", 0},
{"7a", "<abc><BlindMsg>A</BlindMsg></abc>", 1},
{"8", "<BlindMsg>AB</BlindMsg>", 0},
{"8a", "<abc><BlindMsg>AB</BlindMsg></abc>", 1},
{"9", "<BlindMsg>ABC</BlindMsg>", 0},
{"9a", "<abc><BlindMsg>ABC</BlindMsg></abc>", 1},
{"10", "<BlindMsg>A=</BlindMsg>", 0},
{"10a", "<abc><BlindMsg>A=</BlindMsg></abc>", 1},
{"11", "<BlindMsg>AB=</BlindMsg>", 0},
{"11a", "<abc><BlindMsg>AB=</BlindMsg></abc>", 1},
{"12", "<BlindMsg>ABC=</BlindMsg>", 0},
{"12a", "<abc><BlindMsg>ABC=</BlindMsg></abc>", 1},
{"9b", "<BlindMsg>ABCD</BlindMsg>", 0},
{"9c", "<abc><BlindMsg>ABCD</BlindMsg></abc>", 1},
{"13", "<BlindMsg>A==</BlindMsg>", 0},
{"14", "<a><BlindMsg>A==</BlindMsg></a>", 1},
{"15", "<BlindMsg>AB==</BlindMsg>", 0},
{"16", "<a><BlindMsg>AB==</BlindMsg></a>", 1},
{"17", "<BlindMsg>A===</BlindMsg>", 0},
{"18", "<a><BlindMsg>A===</BlindMsg></a>", 1},
{"19", "<BlindMsg>A=B=</BlindMsg>", 0},
{"20", "<a><BlindMsg>A=B=</BlindMsg></a>", 1},
{"21", "<a><BlindMsg>YWJj<xyz>", 0},
{"22", "<a><BlindMsg>YWJj</BlindMsg>", 0},
{"23", "<a><BlindMsg>YWJj<BlindMsg></BlindMsg></a>", 0},
{"24", "<a><BlindMsg>YWJj<BlindMsg></BlindMsg></BlindMsg></a>", 0},
{"25", "<a><BlindMsg>YWJj", 0},
{"26", "<a><BlindMsg>YWJj<%/BlindMsg></a>", 0},
{"27", "<a><BlindMsg>YWJj<a></BlindMsg></a></a>", 0},
{"28", "<a><BlindMsg>YWJj</BlindMsg></a>", 1},
{"29", (const XmlChar*)g_pBlindXML1, 1}
};

static short fnDXMLUnitTest2()
{
	short iResult, i, iEntries;
	
	XmlChar *pBegin, *pEnd; 
	DXMLParser dxml;
	DXMLUnitTestScript entry;

	iEntries = sizeof(DXMLUnitTestScript2)/sizeof(DXMLUnitTestScript);
	for (i = 0; i < iEntries; i++)
	{
		entry = DXMLUnitTestScript2[i];
		pBegin = (XmlChar*)entry.pTest;
		pEnd = pBegin + strlen((char*)pBegin) - 1;

		iResult = XMLParse(pBegin,
							pEnd,
							0,		// # handler entries
							NULL,	// handler entries
							NULL,	// start fcn
							NULL,	// end fcn
							NULL,	// elem start fcn
							NULL,	// elem end fcn
							NULL,	// char fcn
							XMLPARSER_BLINDING_ON);
		
		if ((iResult == XML_STATUS_OK && entry.result == 0) ||
			(iResult != XML_STATUS_OK && entry.result == 1))
			return 0;
	}
	return 1;
}
short fnDXMLTestHarness()
{
	return (fnDXMLUnitTest1() && fnDXMLUnitTest2());
}
#endif
