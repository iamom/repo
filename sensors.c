/*SB
   ************************************************************************
     PROJECT:	Apollo Motion Controller
     AUTHOR:    Gary S. Jacobson                                           
     $Workfile:   sensors.c  $
     $Revision:   1.0  $
     MODULE NAME:  Sensors                                               
     DESCRIPTION:  Low Level Sensor I/O
  
     DATE:                                                 
   ----------------------------------------------------------------------
                                                                        
                 Copyright (c) 1995 Pitney Bowes Inc.                   
                      35 WaterView Drive                                
                     Shelton, Connecticut  06484                        
                                                                        
   ----------------------------------------------------------------------
  		PUBLIC FUNCTIONS:	
  		PRIVATE FUNCTIONS:
  
   
   Revision History Notes:
   Rev #		Name				Date				Reason
   -----    ----------------- -------------- ---------------------------- 
   $Log:   H:/group/SPARK/ARCHIVES/SebringBase/H8s_2357/sensors.c_v  $

 SE
   ************************************************************************/


#include <stdlib.h> 
#include "bitdef.h"
#include "mcp_data.h"
//#include "appl_msg.h"

extern MCP_PROFLAGS profileFlags;
//extern unsigned char fbSensorFlag[];	/* Sensor Flags, 0->OFF, 1->ON */
//extern unsigned char fbSensorDflag[];
extern unsigned char StepperEnables;
extern unsigned char ServoEnables;

SENSOR_HISTORY *fpSensorHistory;

unsigned int wHistIndx;
static unsigned char fbSensorsPowered;
static unsigned char bSensorPoll;
static unsigned char fbA2D_Sampling;
static unsigned char bSensorWaitCycles;

unsigned char fbBlockedState[N_DINPUTS+N_SENSORS];	/* Sensor Flag Desired States for Blocked sensor */
unsigned short wA2D_Data[N_SENSORS];
//unsigned char bSensorValues[N_SENSORS];		/* Raw A/D values for sensors */
unsigned char bAveragedValues[N_SENSORS];	/* Averaged Sensor Readings */
unsigned char bSensorOffsets[N_SENSORS];	/* Background offset readings */
unsigned char bSensorThreshold[N_SENSORS];
unsigned int fwAnalogSensorBits;		/* Sensor bit flags */
unsigned int fwDigitalSensorBits;		/* Sensor bit flags */
unsigned int fwPrevDigitalSensorBits = 0;
unsigned int fwPrevActuatorBits = 0;
// DSD 12/20/2005 - Initialized Actuator Bits to zero
unsigned int fwActuatorBits = 0;			/* Motor and Solenoid bits */
unsigned char bSensorGains[8];

unsigned char bDebounceTicks[N_DINPUTS];
unsigned char bDebounceTimer[N_DINPUTS];

unsigned int wVolts;				/* Raw A/D Volts */
unsigned int wAmps;					/* Raw A/D Amps */
unsigned int wSnsrVolts;			/* Raw A/D Sensor Voltage */

static unsigned short wLastData;	/* Last Input Data Bits */

//TODO - uncomment below code and make it work.
#if 0
void PowerUpSensors(void)
{
	SENSOR_CTRL_PORT |= SENSOR_EBL_BIT;	/* Turn on the source for the IR emiters */
    fbSensorsPowered = 1;
}

void PowerDownSensors(void)
{
	SENSOR_CTRL_PORT &= ~SENSOR_EBL_BIT;	/* Turn off the source for the IR emiters */
    fbSensorsPowered = 0;
}

void RefreshSensorFlags(void)
{
	register long i;
	unsigned int wInputData;		/* Current latched input bits */
	unsigned int wBitMask;			/* mask to isolate bits */
	register unsigned char * pBlockedFlag = fbBlockedState;   /* point to first digital flag state */

// DSD 12/20/2005 - Adjusted the retrieval of the Digital Sensor values
#ifdef DM400C_FDR
	//SENSOR_CTRL_PORT |= SENSOR_EBL_BIT;	// the sensor enables are on the same port as
											// the motor enables and the motion control 
											// code disables them.
	wInputData = (DIGITAL_SENSORS & DIGITAL_SENSORS_MASK);
#else	
	wInputData = (DIGITAL_SENSORS >> 4);
#endif
	for (i=0, wBitMask = 0x01; i < N_DINPUTS; i++, (wBitMask<<=1), pBlockedFlag++) {	/* Loop through all bits in the input data */
		if (wBitMask & wInputData) {		/* yes - set the flag to it's logical state 1 == blocked*/
			if (*pBlockedFlag) {
				profileFlags.flags.fbSensorDflag[i] = 1;
				fwDigitalSensorBits |= wBitMask;
			} else {
				profileFlags.flags.fbSensorDflag[i] = 0;
				fwDigitalSensorBits &= ~wBitMask;
			}
		} else {
			if (*pBlockedFlag) {
				profileFlags.flags.fbSensorDflag[i] = 0;
				fwDigitalSensorBits &= ~wBitMask;
			} else {
				profileFlags.flags.fbSensorDflag[i] = 1;
				fwDigitalSensorBits |= wBitMask;
			}
		}
	}
}

void InitDtoA(void)
{
// DSD 12/20/2005 - Don't use D/A unit - H8S/2212 doesn't have this function
#ifndef DM400C_FDR
	MSTPCRC &= ~D2A;			/* Enable D/A unit */
	DACR |= DAOE1;			/* Enable D/A conversions on Bit 7 only*/
#endif
}

void InitSensors(void)
{
	register unsigned int i=0;
	
    PowerDownSensors();
	MSTP.CRA.BIT._AD = 0;	/* Enable A/D unit */
	AD.ADCSR.BYTE 		= 0;	// Stop the A/D converter 
	AD.ADCR.BIT.TRGS	= 0;	// Software start
	AD.ADCR.BIT.CKS  	= 2;	// 134t state / conversion
	
	
	for (i=0; i<N_SENSORS; i++) {
		bSensorOffsets[i] = 0;
		bSensorThreshold[i] = 0x80; 
		wA2D_Data[i] = 0;
	}
	
		
	for (i=0; i < N_DINPUTS+N_SENSORS; i++)
		if (i < N_DINPUTS)
			fbBlockedState[i] = 1;	// Digital Default
		else
			fbBlockedState[i] = 0;	// Analog Default

	for (i=0; i < N_DINPUTS; i++)
	{
		bDebounceTicks[i] = 1;
		bDebounceTimer[i] = 1;
	}
	/*
		Default values for motor temp stuff
	*/
	bSensorThreshold[0] = 187;
	bSensorThreshold[1] = 41;
	bSensorThreshold[2] = 32;
	bSensorThreshold[3] = 18;
	fbBlockedState[N_DINPUTS] = 1;
	
	wLastData = wVolts = wAmps = wHistIndx = 0;
	fbA2D_Sampling = 0;
	bSensorPoll = 0;

	/* Enable the source drivers */
	PowerUpSensors();

	RefreshSensorFlags();
	
	AD.ADCSR.BYTE = 0x0E;	// Read AN14 only in non-scan mode
	AD.ADCSR.BIT.ADST = 1;				// Start the A/D converter
}

#ifdef AVERAGE_SENSORS 
void ProcessAnalogSensors(void)
{
	register long i;
	register unsigned char * avg = bAveragedValues;

	for (i=0; i<N_SENSORS; i++,avg++) {
		if (bSensorValues[i] > bSensorOffsets[i])
			*avg = ((bSensorValues[i] - bSensorOffsets[i]) >> 1)  + (*avg  >> 1);
		else
			*avg = (*avg>>1);

		fwAnalogSensorBits >>= 1;
		if (*avg > bSensorThreshold[i]) {
			profileFlags.flags.fbSensorFlag[i] = 1;		/* set the flag */
			fwAnalogSensorBits |= 0x8000;	/* set the bitflag */
		} else {
			profileFlags.flags.fbSensorFlag[i] = 0;
		}
	}

}
#else
void ProcessAnalogSensors(void)
{
	register int i; 
	register unsigned char * avg = bAveragedValues;
	register unsigned char * pBlockedFlag = fbBlockedState + N_DINPUTS; /* point to first analog device flag state */
	
	for (i=0; i<N_SENSORS; i++,avg++,pBlockedFlag++) {
			*avg = wA2D_Data[i] >> 8;
		if (*avg > bSensorOffsets[i])
			*avg -= bSensorOffsets[i];
		else
			*avg = 0;

		fwAnalogSensorBits >>= 1;
				
		if (*avg > bSensorThreshold[i]) {
			if (*pBlockedFlag) {
				profileFlags.flags.fbSensorFlag[i] = 1;		/* set the flag */
				fwAnalogSensorBits |= 0x8000;	/* set the bitflag */
			} else {
				profileFlags.flags.fbSensorFlag[i] = 0;
			}
		} else {
			if (*pBlockedFlag) {
				profileFlags.flags.fbSensorFlag[i] = 0;
			} else {
				profileFlags.flags.fbSensorFlag[i] = 1;		/* set the flag */
				fwAnalogSensorBits |= 0x8000;	/* set the bitflag */
			}				
		}
	}

}
#endif


void ProcessDigitalSensors(void)
{
	unsigned short wInputData;		/* Current latched input bits */
	unsigned short wBitMask;			/* mask to isolate bits */
	register long i;
	unsigned short wChangeMask;		/* Result mask for stable sensors, 1=valid */	
	register unsigned char * timer;		/* Pointer to Debounce timer element */
	register unsigned char * pBlockedFlag = fbBlockedState;   /* point to first digital flag state */

	wBitMask = 0x01;			/* First bit position */
	wInputData = (DIGITAL_SENSORS & DIGITAL_SENSORS_MASK);
	wChangeMask = wInputData^wLastData;	/* Find bits that have changed from last input */

	for (i=0, timer = bDebounceTimer; i < N_DINPUTS; i++, (wBitMask<<=1), timer++, pBlockedFlag++)	/* Loop through all bits in the input data */	
	{ 
		if (wBitMask&wChangeMask)			/* If bit has changed from last input */
			*timer = bDebounceTicks[i];		/* Reset the debounce timer */
		else if (*timer && !(--(*timer))) 		/* See if timer expired */
			if (wBitMask & wLastData) {		/* yes - set the flag to it's logical state 1 == blocked*/
				if (*pBlockedFlag) {
					profileFlags.flags.fbSensorDflag[i] = 1;
					fwDigitalSensorBits |= wBitMask;
				} else {
					profileFlags.flags.fbSensorDflag[i] = 0;
					fwDigitalSensorBits &= ~wBitMask;
				}
			} else {
				if (*pBlockedFlag) {
					profileFlags.flags.fbSensorDflag[i] = 0;
					fwDigitalSensorBits &= ~wBitMask;
				} else {
					profileFlags.flags.fbSensorDflag[i] = 1;
					fwDigitalSensorBits |= wBitMask;
				}
			}
	}
	wLastData = wInputData;
	fwActuatorBits = (MTR_EBL & 0x80) >> 7;	/* Save the Motor & Solenoid Enable States */
}

#ifdef CONTINUOUS_LOGGING_ENABLED
unsigned int wNumSensorLogEntries = 0;
SENSOR_HISTORY SensorHistoryUploadBuff[MAX_SENSOR_LOG_ENTRIES_IN_BUFF];

void SendSensorLogDataPacket(SENSOR_HISTORY *pSensorHist, unsigned int wNumEntries)
{
	extern SENSOR_HISTORY *fpSensorHistory;
	extern unsigned char bSourceAddress;
	extern unsigned char bSystemControlAddress;
	extern unsigned char bMcpMonitorMode;

	unsigned char bRespBuffer[EP2_PACKET_SIZE-1]; 

	if (bMcpMonitorMode & MONITOR_LOG_DATA)   
	{
		*bRespBuffer 	 = bSystemControlAddress;
		*(bRespBuffer+1) = bSourceAddress;
		*(bRespBuffer+2) = SENSOR_LOG_DATA;
		*(bRespBuffer+LD_REC_SIZE) = sizeof(SENSOR_HISTORY);
		*(unsigned int *)(bRespBuffer+LD_NUM_RECS) = wNumEntries;

		memcpy(bRespBuffer+LD_DATA, pSensorHist, (size_t)(wNumEntries*sizeof(SENSOR_HISTORY))); 
				
		(void)QueueMessage(bRespBuffer, LD_DATA + (wNumEntries*sizeof(SENSOR_HISTORY)), APOLLO_MCP);
	}
}
#endif	// CONTINUOUS_LOGGING_ENABLED

void DoSensorFlags(void)
{
	register SENSOR_HISTORY * hist = wHistIndx + fpSensorHistory;
	extern volatile unsigned long lwSystemClockTicks;

	ProcessAnalogSensors();

	if ((fwPrevDigitalSensorBits != fwDigitalSensorBits) ||	    
	    (fwPrevActuatorBits !=  fwActuatorBits) ) {	/* If there has been a change */
		hist->lwClockTick = lwSystemClockTicks;
		hist->fwDigitalSensors = fwDigitalSensorBits;
		hist->fwActuators	= fwActuatorBits;
		fwPrevDigitalSensorBits = fwDigitalSensorBits;
		fwPrevActuatorBits = fwActuatorBits;		
		wHistIndx++;

#ifdef CONTINUOUS_LOGGING_ENABLED
		if (wNumSensorLogEntries >= MAX_SENSOR_LOG_ENTRIES_IN_BUFF)
		{
			SendSensorLogDataPacket(SensorHistoryUploadBuff,wNumSensorLogEntries);
			wNumSensorLogEntries = 0;
		}
		SensorHistoryUploadBuff[wNumSensorLogEntries++] = *hist;		
#endif // CONTINUOUS_LOGGING_ENABLED

		if (wHistIndx >= MAX_HISTORY)
			wHistIndx = 0;
	}
}


void AcquireAnalogSensors(void)
{
	register unsigned i;
	
	while(!AD.ADCSR.BIT.ADF);		/* Wait for the current scan to complete */
	AD.ADCSR.BIT.ADF = 0;			// Clear done bit

	for (i=0; i < N_SENSORS; i++)	/* For all chan's  */
		wA2D_Data[i] = AD.ADDRC;		/* Save the data into the result array */


	AD.ADCSR.BIT.ADST = 1;				// Start the A/D converter
}

/*
	void DoSensorAcq ( void )

	Copy the most significant 8 bits of A/D Result Register data into
	the SensorValues storage array for one entire sweep of the A/D converter.
	Program the A/D and start the next sweep.

	While waiting for next sweep, process the digital sensors.

	Copy the most significant 8 bits of A/D Result Register data into
	the SensorValues storage array for one entire sweep of the A/D converter.
	Program the A/D and start the next sweep.

	
*/
void DoSensorAcq(void)
{
	extern unsigned char fbSleepModeActive;

	if (fbSensorsPowered)
	{    		
		//SENSOR_CTRL_PORT |= SENSOR_EBL_BIT;	// the sensor enables are on the same port as
											// the motor enables and the motion control 
											// code disables them.
		AcquireAnalogSensors();
		ProcessDigitalSensors();	/* Acquire & process digital stuff */
		if (fbSleepModeActive & SLEEP_SENSORS)			
			PowerDownSensors();                                 
	} 
	else
	{
		//ProcessDigitalSensors();	/* Acquire & process digital stuff */
		if (bSensorPoll++ == 0) 
			PowerUpSensors();			
	}
}
#endif

