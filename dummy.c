/************************************************************************
*	PROJECT:		Janus
*	MODULE NAME:	$Workfile:   DUMMY.C  $
*	REVISION:		$Revision:   1.70  $
*	
*   DESCRIPTION:    Dummy functions - These will be removed as the real
*					functions are created.
*
*   MODIFICATION HISTORY:
*   06/19/2006  Kan Jiang   Remove some dummy functions that already defined in USB report related files.
*                                       And add a dummy function fnBasePcnLogGetCount().
* ----------------------------------------------------------------------
*               Copyright (c) 2003 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
 	MODIFICATION HISTORY:
	09/05/2006   Dicky Sun   Remove lwPercItemsDone.
*
*	REVISION HISTORY:
* 	$Log:   H:/group/SPARK/ARCHIVES/DM150/uic_app/DUMMY.C_v  $
 * 
 *    Rev 1.70   24 Aug 2005 09:22:12   tx08853
 * Moved fnGetTemplatePrefix() to ExtReports.c
 * 
 *    Rev 1.69   08 Aug 2005 17:28:34   sa002pe
 * Minor modification to the previous change so the function
 * returns something.
 * 
 *    Rev 1.68   08 Aug 2005 16:19:28   sa002pe
 * Added EDMSendRawCommMessage.
 * 
 *    Rev 1.67   26 Apr 2005 18:04:50   sa002pe
 * Moved some local variables to near the top of the file so
 * all the local variables are in the same place.
 * Changed fnUsingPCLInfo to call the new function in
 * ratepublic.c.
 * 
 *    Rev 1.66   Apr 19 2005 06:30:54   NEX3RPR
 * removed function fnMaxRecordCheck(void),which is in oigts.c. -David
 * 
 *    Rev 1.65   18 Apr 2005 16:58:46   CR002DE
 * 1. use new EMD parameter in fnUsingPCLInfo
 * 
 *    Rev 1.64   13 Apr 2005 15:50:48   HO501SU
 * Modified protype of function crc16() so
 * it can be called repeatedly.
 * 
 *    Rev 1.63   Apr 11 2005 23:06:30   CX17598
 * Added dummy lwPercItemsDone & InitPrinterControlFiles for solving compilation problem during integrating
 * VAS from Mega 13.06. -Victor
 * 
 *    Rev 1.62   08 Apr 2005 14:23:28   CR002DE
 * 1. added some stubs for new version 11 pbptask.c
 * 
 *    Rev 1.61   07 Apr 2005 16:18:38   sa002pe
 * Added fnUsingPCLInfo.
 * Cleaned up some stuff.
 * 
 *    Rev 1.60   Mar 01 2005 17:38:08   cx17598
 * Deleted the function fnOITIsMeterMoveOutOfRegion,
 * it is simplemented in oijinfra.c now.
 * -Victor
 * 
 *    Rev 1.59   Aug 25 2004 15:46:40   cx17598
 * The file "delcon.c" has been removed from the project.
 * Added a bunch of stub definition for the delivery confirmation.
 * -Victor
 * 
 *    Rev 1.58   Aug 25 2004 14:26:02   cx17598
 * Moved some oi functions to oigts.c and deteted some unused functions.
 * -Victor
 * 
 *    Rev 1.57   24 Aug 2004 14:40:26   cx08856
 * removed a few things that Victor found a home for
 * 
 *    Rev 1.56   23 Aug 2004 09:04:20   cx08856
 *  
 * 
 *    Rev 1.55   19 Aug 2004 14:16:10   cx08856
 * updates for germany
 * 
 *    Rev 1.54   24 Mar 2004 17:54:54   SA002PE
 * Added ConvertUnitoAscii & fnPadString.
 * 
 *    Rev 1.53   11 Dec 2003 11:10:24   cx08856
 * added stuff required by new download task
 * JANUS specific
 * 
 *    Rev 1.52   08 Dec 2003 16:54:42   SA002PE
 * Moved fnIsEServiceSupported, fnEnableAllUSPSServices,
 * & fnDisabledAllUSPSServices to oidelcon.c now that I know
 * what is supposed to be in these functions and because that's
 * where they are for Mega & Phoenix.
 * 
 *    Rev 1.51   03 Dec 2003 14:00:18   defilcj
 * 1. add fnOITIsMeterMoveOutOfRegion and pfnBaseGetPCN
 * 
 *    Rev 1.50   14 Nov 2003 14:29:32   defilcj
 * 1. change fnGetPsdType to fnGetCurrentPsdType to match common Mega/Phoenix
 * function names.
 * 
 *    Rev 1.49   11 Nov 2003 16:20:44   defilcj
 * 1. implemented fnFlashGetGCNamesCnt in new fservice.c.
 * 
 *    Rev 1.48   07 Nov 2003 14:56:54   BR507HA
 *  
 * 
 *    Rev 1.47   05 Nov 2003 10:59:28   SA002PE
 * Added fnDisableAllUSPSServices.
 * 
 *    Rev 1.46   Nov 05 2003 10:28:56   CX17598
 * Added some empty functions which were used in oijpreset.c for preset
 * account. -David
 * 
 *    Rev 1.45   28 Oct 2003 12:39:34   SA002PE
 * Removed the shadow register log functions now that
 * chasing_shadows.c exists.
 * 
 *    Rev 1.44   21 Oct 2003 14:36:00   SA002PE
 * Added shadow log functions.
 * Removed functions that were already commented out.
 * Removed some of the feature functions now that we're
 * using cfeature.c.
 * Added fnFlashGetGCnamesCnt.
 * 
 *    Rev 1.43   21 Oct 2003 12:04:04   CX17598
 * Added some dummy functions that is needed by feature enabling.
 * -Victor
 * 
 *    Rev 1.42   Oct 17 2003 09:39:20   CX17598
 * Commented out three account functions. Added some functions for account build. -David
 * 
 *    Rev 1.41   Oct 14 2003 14:13:04   CX17598
 * Added a dumy alarm clock init interrupt function
 * "fnInitAlarmInterrupt". -Victor
 * 
 *    Rev 1.40   Oct 13 2003 16:23:52   CX17598
 * Added some dummy clock functions. -Victor
 * 
 *    Rev 1.39   Oct 08 2003 16:32:24   mozdzjm
 * Removed temp declaration of PreserveConvertWarnings
 * 
 *    Rev 1.38   Sep 19 2003 17:26:26   CX17598
 * Removed fnCheckKeyOrEventParameter function, make a new one to oiparm.c.
 * -David
 * 
 *    Rev 1.37   Sep 16 2003 16:48:32   mozdzjm
 * Removed flash flush stubs.
 * 
 *    Rev 1.36   16 Sep 2003 16:12:08   SA002PE
 * Put back in the changes from 1.34 that were killed in 1.35.
 * Cleaned up the includes, externs and variable definitions to
 * removed the stuff that is no longer needed.
 * 
 *    Rev 1.35   Sep 11 2003 16:20:26   CX17598
 * Removed fnevSetNormalPreset(). -Tim
 * 
 *    Rev 1.34   11 Sep 2003 15:53:16   SA002PE
 * Added fnGetCurrentPsdType & fnGetPsdTypes until
 * the Bob stuff can be updated.
 * 
 *    Rev 1.33   Aug 15 2003 17:46:04   CX17598
 * Moved the functions fnfAdSelected and fnfInscSelected to 
 * oiimagej.c/h.
 * -Victor, David check in.
 * 
 *    Rev 1.32   07 Aug 2003 09:01:42   BR507HA
 * Moved RebootSystem() to hw7622.c
 * 
 *    Rev 1.31   01 Aug 2003 15:33:38   SA002PE
 * Removed fnPHCResetMultiPacketVariables.
 * It's now in edmtask.c
 * 
 *    Rev 1.30   24 Jul 2003 14:20:56   defilcj
 * 1. implement batch kicking and clearing
 * 
 *    Rev 1.29   Jun 30 2003 15:08:40   CX17598
 * Added dummy functions for main screens.
 * 
 *    Rev 1.28   23 Jun 2003 10:03:06   CX08856
 * removed fnReadPSOCVersion again.
 * 
 *    Rev 1.27   Jun 18 2003 13:27:32   CX17598
 * Removed dummy function fnfClassSelectedComp(). It is
 * implemented in oirating.c. -Tim
 * 
 *    Rev 1.26   17 Jun 2003 17:11:04   BR507HA
 * Moved fnReadASICVersion to janusuart.c
 * 
 *    Rev 1.25   Jun 17 2003 16:46:36   CX17598
 * Restore the stub function read psoc version. -Victor
 * 
 *    Rev 1.24   17 Jun 2003 12:36:28   SA002PE
 * Changed RebootSystem to send a reply.
 * Added a bunch of routines used by mfg, diag, or scale msgs.
 * Took out ProcessMfgMessage.
 * 
 *    Rev 1.23   12 Jun 2003 10:36:42   CX08856
 * removed fnReadPSOCVersion( ) stub
 * 
 *    Rev 1.22   05 Jun 2003 13:01:08   BR507HA
 * Added stub for ProcessMfgMessage
 * 
 *    Rev 1.21   Jun 05 2003 09:32:22   mozdzjm
 * Removed temporary distributor task defintion.
 * 
 *    Rev 1.20   Jun 02 2003 11:38:08   CX17598
 * Remove dummy fnProcessSCMError().
 * -Victor
 * 
 *    Rev 1.19   28 May 2003 13:23:02   BR507HA
 * Remove PMC Comm Task
 * 
 *    Rev 1.18   May 21 2003 15:30:02   CX17595
 * Removed fnReadPSDVersion().
 * 
 *    Rev 1.17   May 21 2003 12:03:18   bellamyc
 * Change fnReadPSDVer to fill a string buffer instead of 
 * returning a number.
 * 
 *    Rev 1.16   May 21 2003 11:58:22   bellamyc
 * Modified fnReadPSDVersion to just call fnValidData to read a new variable that is populated 
 * on startup by a conversion routine.
 * This function should eventually be replaced by one that reads the version substring and returns
 * a string instead of a word, since the Version is alphanumeric.
 * Commented out last version for now.
 * 
 *    Rev 1.15   May 16 2003 14:15:02   bellamyc
 * _Now includes ipsd.h so that I could ...
 * _Mod the fnReadPSDVersion function to read the version string into a local buffer,
 *   and convert it to a ushort.  The value returned is also stored in a new global 
 *   for debugging purposes only: TestPSDVer.
 * _When we figure out what this function should REALLY do, then it will be removed 
 *   from this file and so will the include of ipsd.h.
 * 
 *    Rev 1.14   14 May 2003 14:22:04   HO501SU
 * Added version information to dummy.c
 * 
 *    Rev 1.13   May 14 2003 12:29:06   CX17595
 * Remove the bob function stubs, change to the real ones. - Henry
 * 
 *    Rev 1.12   12 May 2003 17:23:00   defilcj
 * 1. by popular request and against my better
 * judgement, an intermediate check in for the 
 * debit build.
 * 
 *    Rev 1.11   05 May 2003 14:54:50   cx17594
 * Add fnBeepSpeaker. - Henry
 * 
 *    Rev 1.10   Apr 30 2003 13:44:06   mozdzjm
 * Temporarily added fake distributor task function until files are added containing the real thing.
 * 
 *    Rev 1.9   25 Apr 2003 17:30:56   defilcj
 * added stub for fnShouldDeleteGraphicID
 * 
 *    Rev 1.8   23 Apr 2003 12:08:14   BR507HA
 * Move ExternalDeviceTask to EDMTASK.C and move 
 * ReadRTClock to JANUSCLOCK.C
 * 
 *    Rev 1.7   Apr 13 2003 14:01:16   bellamyc
 * The dummy function for fnValidData() now returns a failed condition, BOB_SICK, so that 
 * it is more obvious when adding code that has these function calls in it, that this is 
 * not supported yet.
 * 
 *    Rev 1.6   04 Apr 2003 19:23:44   defilcj
 * 1. comment out topoofflash def, now found in tof.c
 * 
 *    Rev 1.5   Apr 04 2003 16:24:38   bellamyc
 * Commented out stub for fnBobInterfaseTask() since
 * this is now provided by megabob.c file.
 * 
 *    Rev 1.4   Mar 27 2003 17:38:20   CX17595
 * Added call fnUnicodeProcessImpliedDecimal() in fnhkValidatePostageValueF2().
 * 
 *    Rev 1.3   Mar 24 2003 13:22:52   BR507HA
 * Change default date to release date.  Add functions to read versions.
 * 
 *    Rev 1.2   19 Mar 2003 10:13:40   defilcj
 * changes for OS_06 build
*     
*************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include "commontypes.h"

#include "custdat.h"
#include "datdict.h"
//#include "deptaccount.h"
#include "dummy.h"
#include "oifpprint.h"
#include "bwrapper.h"
#include "mmcimjet.h"
#include "oit.h"  	  //rcd-gts
#include "oiscrnfp.h"  //rcd-gts
//#include "oireport.h" //rcd-gts
#include "features.h" //rcd-gts
#include "oicmenucontrol.h" //rcd-gts
#include "xmlform.h" //vli-gts
#include "bobutils.h"		// must come before bob.h
#include "bob.h"

extern ulong	*pmVerForScreen;
extern char bPsdType;
extern CMOS_Signature CMOSSignature;
extern unsigned char   led_image;

/* dummy declarations */

const unsigned short wRightMarginPos[NUM_MARGIN_POSITIONS] = {30, 49, 68, 87, 105}; // for oiprint.c

const char Delivery_Confirm[]	= "14";	/* Service Code for Delivery Confirmation */
const char Signature_Confirm[]	= "34";	/* Service Code for Signature Confirmation */
const char Certified_Mail[]		= "71";	/* Service Code for Certified Mail */

const char Priority_Mail[]			= "PM";	/* Class Code for Priority Mail */
const char First_Class[]			= "FC";	/* Class Code for First Class */
const char Pkg_Services_Bound[]		= "BB";	/* Class Code for Package Services (Bound Printed Matter) */
const char Pkg_Services_Library[]	= "BL";	/* Class Code for Package Services (Library Mail) */
const char Pkg_Services_Parcel[]	= "BP";	/* Class Code for Standard Mail (B) Parcel Post */
const char Pkg_Services_Media[]		= "BS";	/* Class Code for Package Services (Media Mail) */
const char Pkg_Services_Std[]		= "SA";	/* Class Code for Package Services (Standard Parcels) */

const unsigned int crcTable[ 256 ] = {                                 
   0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,   
   0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,   
   0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,   
   0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,   
   0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,   
   0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,   
   0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,   
   0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,   
   0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,   
   0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,   
   0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,   
   0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,   
   0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,   
   0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,   
   0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,   
   0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,   
   0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,   
   0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,   
   0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,   
   0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,   
   0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,   
   0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,   
   0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,   
   0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,   
   0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,   
   0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,   
   0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,   
   0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,   
   0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,   
   0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,   
   0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,   
   0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040 }; 


/*********************** DUMMY FUNCTIONS ****************************** */
/*********************** DUMMY FUNCTIONS ****************************** */
// void __SaveState()
// {	Halt_System(); }
unsigned long fnReadPMVersion()
{
	if(pmVerForScreen == NULL)
		return 0;
	else
		return(*pmVerForScreen);
}

void fnBeepSpeaker(void) {}

void fnMtsProductionTests(unsigned char *pbData, unsigned short pbDataLen) {}

void fnEDMLockMeter(unsigned char * pbData, unsigned short pbDataLen ) {}

void fnEDMWakeUpMeter(unsigned char * pbData, unsigned short pbDataLen ) {}

BOOL fnMailMachineInManufTestMode()
{	return(FALSE); }

void fnOIEasInitAccountingSystem( unsigned char AcctType ) {}

void fnXBInitialization(void) {}

void fnEDMEasInitHandler(void) {}

void fnEDMEasSessionInit(void) {}

void fnEDMEasShutdown(void) {}

void fnOIAdsGetAccountIdComponent( unichar *pDest, unsigned char ucAcctLevel,
									unichar *pAcctId ) {}

double fnEDMEasFetchPresetSurchargeAmount( )
{	return 0; }

unsigned char fnEDMEasFetchNbrAcctLevels( )
{	return 0; }

unsigned char fnEDMEasFetchPresetSurchargeType( )
{	return 0; }

unsigned short fnEDMEasFetchPresetAcctIdToken( unsigned char ucPresetIdx )
{	return 0; }

unsigned short fnEDMEasFetchPresetJobIdToken( unsigned char ucPresetIdx )
{	return 0; }  /*end fnEDMEasFetchPresetJobIdToken*/

unsigned char pfnBaseGetPCN(char* dest)
{
	strcpy(dest, CMOSSignature.bUicPcn);
	return(strlen(CMOSSignature.bUicPcn));
}

BOOL fnSYSIsBaseAttached( void )
{	return (TRUE); }

uchar fnBaseWhichBaseAPI( void )
{	return (MIDJET_BASE_API); }

void GetMidjetBaseVersionInfo(MMC_VERSION_INFO *pMMCIVersionInfo)
{	memset((void *)pMMCIVersionInfo, 0x20, sizeof(MMC_VERSION_INFO)); }
                                                                      

uint crc16(char *char_p, unsigned long memLength, BOOL resetFlg)
{
	ulong	i;
	static  uint	crc = 0; 

	if(resetFlg) crc = 0;

	for (i = 0; i < memLength; i++, char_p++)
		crc = crcTable[(crc ^ *char_p) & 0xff] ^ (crc >> 8); 

	return (crc);
}

/************************************************************************
DESCRIPTION :
	 The ConvertUnitoAscii function takes an Unicode string and converts
	 it to ASCII.

INPUT PARAMS : 
	 pSource             - Pointer to unicode string
	 pDest               - Pointer to ASCII string (for output)
	 wLen                - Number of characters in unicode string
	 
RETURN VALUE :
	 None
************************************************************************/
void ConvertUnitoAscii(unsigned short* pSource, char* pDest, unsigned short wLen)
{
	unsigned long	i;

//  Convert each character in unicode string
	for (i = 0; i < wLen; i++)
		*(pDest + i) = (char) *(pSource + i);

//  Null terminate ASCII string
	pDest[wLen] = 0 ;
}



//rcd-gts

void fnRptSetCurrentScr(ushort ScrID) { }

BOOL fnIsEServiceSupported(const unsigned char ucFeatureID)
{	return (FALSE); }

void fnEnableAllUSPSServices(FeatureTbl *pFeatureList) { }

void fnDisableAllUSPSServices(FeatureTbl *pFeatureList) { }

BOOL fnIsSelectedEServiceEnabled(unsigned char bEServiceID)
{	return (FALSE); }

//BOOL fnMaxRecordCheck()
//{	return (TRUE); }

unsigned char fnGetNumOfUSPSServicesEnabled()
{ 	return (0);	}


void fnDelConClearLogs( void ) { }

XMLScriptEntry pServUplEntry[] = { 0 };

unsigned char fnDelConRecordsUploaded(void)
{	return (0);	}

void fnDelConBeginUpload(void) { }

void fnDelConUpdateRecordXmitStatus( unsigned long ulResponseRecordId, 	// record ID
										char *pPIC,						// PIC
										char *pZipCode,					// Zip Code
										unsigned char TransmitStatus )	// Xmit Status recceived from Data center
{ }

unsigned char fnDelConRecordsPending( void )
{	return (0);	}

void fnDelConEndUpload(void) { }

BOOL fnPOPISEnabled()
{	return (FALSE);	}

BOOL fnPOPIsDataAvailableForUpload(void)
{    return (FALSE);	}

void fnPOPInitUpload(void) { }


void  UartTxExpire( unsigned long wArg )
{
}

void TLS_Unused_Parameter(){ return; }


/*** added by kan for usb report ***/
extern SysBasePCNLog			   CMOSSysBasePCNLog;

/* **********************************************************************
// FUNCTION NAME: fnBasePcnLogGetCount
// DESCRIPTION:  
//
// INPUTS:	none
//					
// AUTHOR: Derek DeGennaro
//
// RETURNS: The number of pcn log entries.
//
// **********************************************************************/
unsigned char fnBasePcnLogGetCount(void)
{
	return CMOSSysBasePCNLog.bCount;
}
