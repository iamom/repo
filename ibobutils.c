/* TAB SETTING = 5 (e.g. 5, 9, 13, 17...)	*/

/************************************************************************
*	PROJECT:		PHOENIX & JANUS
*	MODULE NAME:	$Workfile:   ibobutils.c  $
*	REVISION:		$Revision:   1.8  $
*	
*	DESCRIPTION:	This is a sub set of comet bobutils.c file which includes
*					iButton PSD Manufacturing parameter tables.
*					
*
* ----------------------------------------------------------------------
*               Copyright (c) 2003 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*
*	REVISION HISTORY:
*	$Log:   H:/group/SPARK/ARCHIVES/UIC/ibobutils.c_v  $
* 
* 19-Oct-07 cr002de on shelton jnus_tips
*	1. Add new 8.0 ibutton IBI Lite Indicia Version and IBI Lite Vendor Model variables
*  	MFG_IBILITE_INDICIA_VERSION and MFG_IBILITE_VENDOR_MODEL
*
*	12-Jul-07 cr002de on shelton jnus_tips
*		1. Correct table entry for MFG_IPSDZEROPOSTAGEPC.
*		2. Add support for MFG_IPSDBELGIAN_APPL_VALUE. 
*
*    Rev 1.8   06 Jul 2005 16:13:24   cr002de
* 1. support for new 4.0 ibutton parameters
* 
*    Rev 1.7   Nov 28 2004 20:46:14   arsenbg
* the following changes were made in this file and other files in support of Canada ECDSA PCT operations:
* ibobutil.h	added new long parameter id: MFG_IPSDPROVIDERKEY
* 
* ibobutils.c	added new record in mfgVaultIPSDLongRef for MFG_IPSDPROVIDERKEY
* cjunior 		added reference for BOBID_IPSD_CANADA_PROVKEYREV (ulIPSD_ProviderKeyRev)
* glob2bob.h	unmasked GLOBID_CANADAPROVKEYREV
* glob2bob.c	unmasked GLOBID_CANADAPROVKEYREV
* ipsd.h		added reference for ulIPSD_ProverKeyRev
* ipsd.c		added creation of ulIPSD_ProverKeyRev storage
* 
*    Rev 1.6   Aug 10 2004 14:10:14   arsenbg
* add support for German specific flag: add DR to AR on PVR
* 
*    Rev 1.5   03 Mar 2004 11:14:34   CR002DE
* 1. fix mfgVaultIPSDByteRef table, was missing
* an entry for MFG_IPSDMSGTYPE.
* 
*    Rev 1.4   27 Jan 2004 10:34:30   defilcj
* 1. added id's and table entries to retreive 3 new
* psd parameters for the 1.5  ibutton:
* MFG_IPSDITALYCURRENCYCODE
* MFG_IPSDINDICIUMCODE
* MFG_IPSDALGORITHMOBJECTID
* 
*    Rev 1.3   Dec 02 2003 12:02:42   thillas
* Fixed fraca 9048 and included more 
* string variable into the table.
* 
*    Rev 1.2   18 Aug 2003 14:25:10   defilcj
* 1. move all edmmfg command table entries
* to new table MFGPARMAMAT0.
* 
*    Rev 1.1   05 Aug 2003 13:13:14   defilcj
* 1. fix compile and link problems
* 
*    Rev 1.0   Aug 05 2003 11:01:02   thillas
* *** Initial Revision ***
* This is a sub set of comet bobutils.h file which includes
* iButton PSD Manufacturing parameter tables.
* 
* Used by Phoenix and Janus for iButton
* Mfg parameter messages.
* 
*
*************************************************************************/

#include "ibobutils.h"
#include "bob.h"

/* CROSS-REFERENCES BETWEEN MANUFACTURING PARAMETER ID'S AND BOB MESSAGE ID'S
   --------------------------------------------------------------------------

	Manufacturing Number which is an index	bobutilities ID For Access				bob Message ID For Writing
*/
/* New bobamat IDs that must be implemented
   Temporarily defined until they are implemented
   Note that the xx is just for ease in finding them in the tables below
*/

const struct mfgXref mfgVaultIPSDByteRef[] = {

/* don MFG_IPSDREFILLTYPE,		   	*/	{ BOBID_IPSD_REFILLTYPE,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDCLEARARWITHPVR,	   	*/	{ BOBID_IPSD_CLEARARWITHPVR,	   		INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDINDICIAVERSNUMBER,	*/	{ BOBID_IPSD_INDICIAVERNUM,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDCURRENCYCODE,		*/	{ BOBID_IPSD_CURRENCYCODE,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDALGORITHM_ID,		*/	{ BOBID_IPSD_ALGORITHID,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDFRENCHMACSIZE,		*/	{ BOBID_IPSD_FRENCHMAC_SZ,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDFRENCHOCRSIZE,		*/	{ BOBID_IPSD_FRENCHOCR_SZ,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDINDICIATYPE,			*/	{ BOBID_IPSD_INDICIATYPE,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDKEYPADREFILLINVINS,	*/	{ BOBID_IPSD_KEYPADREFILLINVOKESINSPECT,INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDMSGTYPE,				*/	{ BAD_VARIABLE_NAME, 					INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDITALYCURRENCYCODE,	*/	{ BOBID_IPSD_ITALYCURCODE,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDINDICIUMCODE,		*/	{ BOBID_IPSD_INDICIUMCODE,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDALGORITHMOBJECTID, 	*/	{ BOBID_IPSD_ALGRTHMOBJID,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDADDDRTOARONPVR,		*/	{ BOBID_IPSD_ADDDRTOARONPVR,			INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDFRENCHALPHANUMPC,	*/	{ BOBID_IPSD_FRENCH_ALPHANUMPIECECOUNT,	INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDRESETPCONPVR,		*/	{ BOBID_IPSD_RESETPCONPVR,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDALLOWZEROPPOSTAGE, 	*/	{ BOBID_IPSD_ALLOWZEROPPOSTAGE,			INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDBELGIAN_APPL_VALUE,	*/	{ BOBID_IPSD_BELGIANAPPVALUE,			INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IBILITE_INDICIA_VERSION */  { BOBID_IPSD_IBILITEINDICIAVERSION,		INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IBILITE_VENDOR_MODEL	*/  { BOBID_IPSD_IBILITEVENDORMODEL,		INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
};																								   	
																								   	
const struct mfgXref mfgVaultIPSDWordRef[] = {	  													   	
/* don MFG_IPSDORIGINCOUNTRY,		*/	{ BOBID_IPSD_ORIGINCOUNTRY,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
};																										

const struct mfgXref mfgVaultIPSDLongRef[] = {
/* don MFG_IPSDMAXDESCENDINGREG,	*/	{ BOBID_IPSD_MAXDESCREG,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDBACKDATELIMIT,		*/	{ BOBID_IPSD_BACKDATELIMIT,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDDATEADVANCEDLIMIT,	*/	{ BOBID_IPSD_DATEADVANCELIMIT,			INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDCLOCKOFFSET,			*/	{ BOBID_IPSD_CLOCKOFFSET,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFD_IPSDPROVIDERKEY,			*/	{ BOBID_IPSD_CANADA_PROVKEYREV,			INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDZEROPOSTAGEPC,		*/	{ BOBID_IPSD_ZEROPOSTAGEPIECECOUNT,		INVALID_VARIABLE_NAME,			BOBAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
};																									

																								
const struct mfgXref mfgVaultIPSDMonyRef[] = {															
/* don MFG_IPSDARPRESET,			*/	{ BOBID_IPSD_ASCREGPRESET,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDMAXAR,				*/	{ BOBID_IPSD_MAXASCREG,		 			INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
}; 

const struct mfgXref mfgVaultIPSDStringRef[] = {
/* don MFG_IPSDINDICIASERIALNUMBER,	*/	{ BOBID_IPSD_INDICIASN,					INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDMANUFACTURESMR,		*/	{ BOBID_IPSD_MANUFSMR,					INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDMETERPCN,			*/	{ BOBID_IPSD_PCN,						INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDPBPSERIALNUM,		*/	{ BOBID_IPSD_PBISERIALNUM,				INVALID_VARIABLE_NAME,			MFGPARMAMAT0,	PERFORM_SCRIPT,	MEM_ACCESS		},
/* don MFG_IPSDORIGINPOSTCODE,		*/	{ BAD_VARIABLE_NAME,					INVALID_VARIABLE_NAME,			0,				MEM_ACCESS,		MEM_ACCESS		},
/* don MFG_IPSDLICENSINGZIP,		*/	{ BAD_VARIABLE_NAME,					INVALID_VARIABLE_NAME,			0,				MEM_ACCESS,		MEM_ACCESS		},
/* don MFG_IPSDMAILMACHINEBASEPCN,	*/	{ BAD_VARIABLE_NAME,					INVALID_VARIABLE_NAME,			0,				MEM_ACCESS,		MEM_ACCESS		},
/* don MFG_IPSDUIC_CMOS_PCN			*/	{ MFG_UIC_PCN,							NOT_A_MESSAGE,					0,				MEM_ACCESS,		MEM_ACCESS		},
/* don MFG_IPSDUIC_CMOS_SERIAL_NUM,	*/	{ MFG_UIC_SERIAL_NUM,					NOT_A_MESSAGE,					0,				MEM_ACCESS,		MEM_ACCESS		},
/* don MFG_IPSDUIC_CMOS_SMR			*/	{ MFG_UIC_SMR,							NOT_A_MESSAGE,					0,				MEM_ACCESS,		MEM_ACCESS		},
/* don MFG_IPSDUIC_CMOS_HW_REV		*/	{ MFG_UIC_HW_REV,						NOT_A_MESSAGE,					0,				MEM_ACCESS,		MEM_ACCESS		},
};

const struct mfgXref mfgVaultIPSDPackedRef[] = {
/* don MFG_IPSDUSERPASSWORD,   		*/	{ BAD_VARIABLE_NAME/*BOBID_IPSD_USERPASSWORD*/,				INVALID_VARIABLE_NAME,			0,	MEM_ACCESS,	MEM_ACCESS		},
};

const struct mfgXref mfgVaultIPSDUnicodRef[1];

const struct mfgXref mfgVaultIPSDMultiPackedRef[] = {
/* 	MFG_IPSDPROVIDERPUBLICKEY 		*/	 {BAD_VARIABLE_NAME/*BOBID_IPSD_PROVIDERKEYDATA*/,			INVALID_VARIABLE_NAME,			0,	MEM_ACCESS,	MEM_ACCESS		},			
};


/* LOCATIONS OF TABLES THAT CROSS-REFERENCE MANUFACTURING PARAMETER ID's
   ---------------------------------------------------------------------
   Note: this list must be in the same sequence as the enumerated list called bobsParmOps
*/


extern const struct mfgXref mfgPHCByteRef[];		
extern const struct mfgXref mfgPHCWordRef[];		
extern const struct mfgXref mfgPHCLongRef[];		
extern const struct mfgXref mfgPHCStringRef[]; 	
extern const struct mfgXref mfgPHCPackedRef[]; 	
extern const struct mfgXref mfgPHCMultiPackedRef[];


const struct mfgXXref IPSDmfgTables[] = {

/*	Table					Max Parameter ID					Minimum Parameter ID	Instruction Sent To fnParameterJanitor	*/
/*	-----------				--------------------				--------------------	--------------------------------------	*/
{	mfgVaultIPSDByteRef,	BAD_MFG_VAULT_IPSD_BYTE_ID - 1,		START_OF_MFG_VLT_IDS},	/*	MFG_BYTE_READ							*/
{	mfgVaultIPSDByteRef,	BAD_MFG_VAULT_IPSD_BYTE_ID - 1,		START_OF_MFG_VLT_IDS},	/*	MFG_BYTE_WRITE							*/
{	mfgVaultIPSDWordRef, 	BAD_MFG_VAULT_IPSD_WORD_ID - 1,		START_OF_MFG_VLT_IDS},	/*	MFG_WORD_READ							*/
{	mfgVaultIPSDWordRef, 	BAD_MFG_VAULT_IPSD_WORD_ID - 1,		START_OF_MFG_VLT_IDS},	/*	MFG_WORD_WRITE							*/
{	mfgVaultIPSDLongRef,	BAD_MFG_VAULT_IPSD_LONG_ID - 1,		START_OF_MFG_VLT_IDS},	/*	MFG_LONG_READ							*/
{	mfgVaultIPSDLongRef,	BAD_MFG_VAULT_IPSD_LONG_ID - 1,		START_OF_MFG_VLT_IDS},	/*	MFG_LONG_WRITE							*/
{	mfgVaultIPSDMonyRef,	BAD_MFG_VAULT_IPSD_MONY_ID - 1,		START_OF_MFG_VLT_IDS},	/*	MFG_MONETARY_READ						*/
{	mfgVaultIPSDMonyRef,	BAD_MFG_VAULT_IPSD_MONY_ID - 1,		START_OF_MFG_VLT_IDS},	/*	MFG_MONETARY_WRITE						*/
{	mfgVaultIPSDStringRef,	BAD_MFG_VAULT_IPSD_STRING_ID - 1,	START_OF_MFG_VLT_IDS},	/*	MFG_STRING_READ							*/
{	mfgVaultIPSDStringRef,	BAD_MFG_VAULT_IPSD_STRING_ID - 1,	START_OF_MFG_VLT_IDS},	/*	MFG_STRING_WRITE						*/
{	mfgVaultIPSDUnicodRef,	BAD_MFG_VAULT_IPSD_UNICOD_ID - 1,	START_OF_MFG_VLT_IDS},	/*	MFG_UNICODE_READ						*/
{	mfgVaultIPSDUnicodRef, 	BAD_MFG_VAULT_IPSD_UNICOD_ID - 1,	START_OF_MFG_VLT_IDS},	/*	MFG_UNICODE_WRITE						*/
{	mfgVaultIPSDPackedRef, 	BAD_MFG_VAULT_IPSD_PACKED_ID - 1,	START_OF_MFG_VLT_IDS},	/*	MFG_PACKED_BYTE_READ					*/
{	mfgVaultIPSDPackedRef, 	BAD_MFG_VAULT_IPSD_PACKED_ID - 1,	START_OF_MFG_VLT_IDS},	/*	MFG_PACKED_BYTE_WRITE					*/
{	mfgVaultIPSDStringRef, 	BAD_MFG_VAULT_IPSD_STRING_ID - 1,	START_OF_MFG_VLT_IDS},	/*	MFG_STRING_LEN							*/
{	mfgVaultIPSDPackedRef, 	BAD_MFG_VAULT_IPSD_PACKED_ID - 1,	START_OF_MFG_VLT_IDS},	/*	MFG_PACKED_LEN							*/

{	mfgPHCByteRef,			BAD_MFG_PHC_BYTE_ID -1,				START_OF_MFG_PHC_IDS},	//  MFG_PHC_BYTE_READ						*/
{	mfgPHCWordRef,			BAD_MFG_PHC_WORD_ID -1,				START_OF_MFG_PHC_IDS},	//  MFG_PHC_WORD_READ						*/
{	mfgPHCLongRef,			BAD_MFG_PHC_LONG_ID -1,				START_OF_MFG_PHC_IDS},	//  MFG_PHC_LONG_READ						*/
{	mfgPHCStringRef, 		BAD_MFG_PHC_STRING_ID -1,			START_OF_MFG_PHC_IDS},	//  MFG_PHC_STRING_READ						*/
{	mfgPHCPackedRef, 		BAD_MFG_PHC_PACKED_ID -1,			START_OF_MFG_PHC_IDS},	//  MFG_PHC_PACKED_READ						*/

{	mfgPHCByteRef,			BAD_MFG_PHC_BYTE_ID -1,				START_OF_MFG_PHC_IDS},	//  MFG_PHC_BYTE_WRITE						*/
{	mfgPHCWordRef,			BAD_MFG_PHC_WORD_ID -1,				START_OF_MFG_PHC_IDS},	//  MFG_PHC_WORD_WRITE						*/
{	mfgPHCLongRef,			BAD_MFG_PHC_LONG_ID -1,				START_OF_MFG_PHC_IDS},	//  MFG_PHC_LONG_WRITE						*/
{	mfgPHCStringRef, 		BAD_MFG_PHC_STRING_ID -1,			START_OF_MFG_PHC_IDS},	//  MFG_PHC_STRING_WRITE					*/
{	mfgPHCPackedRef, 		BAD_MFG_PHC_PACKED_ID -1,			START_OF_MFG_PHC_IDS},	//  MFG_PHC_PACKED_WRITE					*/

{	mfgVaultIPSDMonyRef,	BAD_MFG_VAULT_IPSD_MONY_ID - 1,		START_OF_MFG_VLT_IDS},	/*	MFG_MONETARY_LEN						*/
{	mfgPHCStringRef,		BAD_MFG_PHC_STRING_ID - 1,			START_OF_MFG_PHC_IDS},	/*	MFG_PHC_STRING_LEN						*/
{	mfgPHCPackedRef, 		BAD_MFG_PHC_PACKED_ID -1,			START_OF_MFG_PHC_IDS},	//  MFG_PHC_PACKEDBYTE_LEN		 			*/

{	mfgPHCMultiPackedRef, 	BAD_MFG_PHC_MULTI_PACKED_ID -1,		START_OF_MFG_PHC_IDS},	//  MFG_PHC_MULTI_PACKED_READ	 		*/
{	mfgPHCMultiPackedRef,  	BAD_MFG_PHC_MULTI_PACKED_ID -1,		START_OF_MFG_PHC_IDS},	//  MFG_PHC_MULTI_PACKED_WRITE	 		*/
{	mfgPHCMultiPackedRef,  	BAD_MFG_PHC_MULTI_PACKED_ID -1,		START_OF_MFG_PHC_IDS},	//  MFG_PHC_MULTI_PACKED_LEN	 		*/

{	mfgVaultIPSDMultiPackedRef, BAD_MFG_VAULT_IPSD_MULTI_PACKED_ID -1,	START_OF_MFG_VLT_IDS},	//  MFG_PSD_MULTI_PACKED_READ		*/
{	mfgVaultIPSDMultiPackedRef, BAD_MFG_VAULT_IPSD_MULTI_PACKED_ID -1,	START_OF_MFG_VLT_IDS},	//  MFG_PSD_MULTI_PACKED_WRITE		*/
{	mfgVaultIPSDMultiPackedRef, BAD_MFG_VAULT_IPSD_MULTI_PACKED_ID -1,	START_OF_MFG_VLT_IDS},	//  MFG_PSD_MULTI_PACKED_LEN		*/
};
