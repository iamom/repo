#include <string.h> /* for memcpy */
#include <assert.h>
#include "nucleus.h"
#include "kernel/nu_kernel.h"
#include "connectivity/ctsystem.h"
#include "commontypes.h"
#include "circ_buf.h"
#include "varassert.h"

#include "events.h"

#define NO_DBG_FROM_THIS_FILE       /* must be before including logging.h */
#include "logging.h"


//#define DBG_CIRC_BUF

#ifdef DBG_CIRC_BUF
   #include "events.h"
#define DUMP_CB(prompt, cb) dump_circ_buf(prompt, cb)

void dump_circ_buf(const char *prompt, cb_t *cb);

static ev_data_t ev_cb_log_data = { EVDT_CIRC_BUF_LOG, /* EV_INFO_NORMAL,*/ {{0, 0, 0, 0, 0}} };

void dump_circ_buf(const char *prompt, cb_t *cb)
{
   ev_cb_log_data.d.circ_buf_log.cb = *cb;
   strncpy(ev_cb_log_data.d.circ_buf_log.str, prompt, sizeof(ev_cb_log_data.d.circ_buf_log.str));
   ev_create_log_event(EV_LOG_CIRC_BUF, &ev_cb_log_data);
},
#else /* !DBG_CIRC_BUF */
   #define DUMP_CB(prompt, cb) 
#endif /* DBG_CIRC_BUF */



cb_state_t init_cb(cb_t *cb, uint8_t *buf_start, uint16_t buf_len)
{
   cb_state_t result = CB_ERROR;

   DBGINFO("init_cb�cb=%p, buf_start=%p, buf_len=%u", cb, buf_start, buf_len);
   assert(cb != NULL);
   if (cb)
   {
      assert(buf_start != NULL);
      if (buf_start && buf_len > 0)
      {
         cb->cb_start = buf_start;
         cb->cb_end = cb->cb_start + buf_len;
         cb->cb_size = buf_len;
         cb->cb_put = cb->cb_start;
         cb->cb_get = cb->cb_start;
         cb->cb_free_bytes = buf_len;
         result = CB_SUCCESS;
         DUMP_CB("init_cb", cb);
      }
   }
   return result;
}

cb_state_t add_ch_to_cb(cb_t *cb, uint8_t ch)
{
   cb_state_t result = CB_SUCCESS;

   assert(cb != NULL);
   if (cb->cb_free_bytes)
   {
      cb->cb_free_bytes--;
      *cb->cb_put++ = ch;
      if (cb->cb_put >= cb->cb_end)
      {
         cb->cb_put = cb->cb_start;  /* wrap around */
         result = CB_WRAPPED;
      }
   }
   else
   {
      result = CB_FULL;
   }
   return result;
}

cb_state_t add_buf_to_cb(cb_t *cb, const uint8_t *p_buf, uint16_t buf_len, uint8_t const ** pos_in_cb)
{
   cb_state_t result = CB_SUCCESS;
   uint16_t   bytes_till_boundry;
   uint16_t   bytes_to_copy;

   assert(cb != NULL);
   if (cb->cb_free_bytes >= buf_len)
   {

      result = CB_SUCCESS;
      cb->cb_free_bytes -= buf_len;
      if (pos_in_cb != NULL)
      {
         *pos_in_cb = cb->cb_put;
      }

      while (buf_len)
      {
         /* make sure we don't copy past the boundry */
         bytes_till_boundry = cb->cb_end - cb->cb_put;
         bytes_to_copy = min(bytes_till_boundry, buf_len);

         assert(bytes_till_boundry != 0); /* otherwise we are in an endless loop */

         if (bytes_to_copy > 0)
         {
            memcpy(cb->cb_put, p_buf, bytes_to_copy);

            cb->cb_put += bytes_to_copy;
            if (cb->cb_put >= cb->cb_end)
            {
               cb->cb_put = cb->cb_start;
               //result = CB_WRAPPED; /* to make it consistent with add_ch_to_cb */
            }
            buf_len -= bytes_to_copy;
            p_buf += bytes_to_copy;
         }
      }
   }
   else
   {
      result = CB_NOT_ENOUGH_SPACE;
   }
   return result;
}   

cb_state_t remove_ch_from_cb(cb_t *cb, uint8_t *p_ch)
{
   cb_state_t result = CB_EMPTY;

   if (cb->cb_free_bytes < cb->cb_size)
   {
      *p_ch = *cb->cb_get++;
      if (cb->cb_get >= cb->cb_end)
      {
         cb->cb_get = cb->cb_start;  /* wrap around */
      }
      cb->cb_free_bytes++;
      result = CB_SUCCESS;
   }
   return result;
}

cb_state_t clear_cb(cb_t *cb)
{
   cb->cb_free_bytes = cb->cb_size;
   cb->cb_get = cb->cb_put;
   return CB_SUCCESS;
}

cb_state_t move_cb_block_over_boundry(cb_t *cb, uint8_t *p_block, uint8_t **pp_new_block, uint16_t len)
{
   cb_state_t result = CB_ERROR;
   uint16_t bytes_to_end_of_cb;

   DBGINFO("move_cb_block_over_boundry�cb=%p, p_block=%p, len=%u", cb, p_block, len);
   assert(cb != NULL && p_block != NULL && pp_new_block != NULL && len != 0);
   DUMP_CB("mobstrt", cb);
   if (cb != NULL && p_block != NULL && pp_new_block != NULL)
   {
      bytes_to_end_of_cb = cb->cb_end - p_block;

      DBGINFO("move_cb_block_over_boundry�bytes_to_end=%u, req len=%u available=%u", bytes_to_end_of_cb, len, cb->cb_free_bytes);
      assert(len < cb->cb_free_bytes);
      if (len < cb->cb_free_bytes)
      {
         cb->cb_free_bytes -= len;
         memcpy(cb->cb_start, p_block, len);
         cb->cb_put = cb->cb_start + len;
         *pp_new_block = cb->cb_start;
         result = CB_SUCCESS;
      }
      else
      {
         result = CB_NOT_ENOUGH_SPACE;
      }
   }
   DUMP_CB("mobend ", cb);

   return result;
}

cb_state_t free_cb_block(cb_t *cb, const uint8_t *p_block, uint16_t len)
{
   INT old_intr_level;
   cb_state_t result = CB_ERROR;

   DBGINFO("free_cb_block�cb=%p, p_block=%p, len=%u", cb, p_block, len);
   assert(cb != NULL);
   assert(p_block != NULL);
   varassert(p_block == cb->cb_get, "p_block=%p cb_get=%p cb_put=%p len=%d", p_block, cb->cb_get, cb->cb_put, len);
   DUMP_CB("freesta", cb);

   // asm("orc #7, EXR"); /* block all interrupts -- allowed because this is a monitor function */
   old_intr_level = NU_Control_Interrupts(NU_DISABLE_INTERRUPTS);
   if (p_block != cb->cb_get)
   {
      ev_data_t ev_data;

      ev_data.type = EVDT_FAULT;
      ev_data.d.fault_info.error_id = E_CB_BUFFER_ERROR;
      ev_raise_event(EV_FAULT_RAISE, &ev_data);

      /* see if we can salvage the free to reduce follow on errors */
      /* We do this by prepending the block between the cb_get and the block to be freed */
      /* so that free blocks following have a chance to be freed again. */
      if (p_block > cb->cb_get)
      {
         if (p_block < cb->cb_end)
         {
            len += p_block - cb->cb_get;
            p_block = cb->cb_get;
         }
      }
      else
      {
         if (p_block >= cb->cb_start && p_block < cb->cb_put)
         {
            len += (cb->cb_end - cb->cb_get) + (p_block - cb->cb_start);
            p_block = cb->cb_get;
         }
      }
   }

   if (p_block == cb->cb_get && len + cb->cb_free_bytes <= cb->cb_size)
   {
      cb->cb_get += len;
      if (cb->cb_get >= cb->cb_end)
      {
         cb->cb_get = cb->cb_start + (cb->cb_get - cb->cb_end);
      }
      cb->cb_free_bytes += len;

      assert(cb->cb_free_bytes <= cb->cb_size);
      result = CB_SUCCESS;
   }
   DUMP_CB("freeend", cb);
   NU_Control_Interrupts(old_intr_level);
   return result;
}   

#ifdef UNIT_TEST

#include "unit_test.h"

#define UT_CB_BUF_SIZE 200
uint8_t test_cb_mem[UT_CB_BUF_SIZE];
cb_t ut_cb /* __attribute__ section(".near"))) */;

void test_circ_buf(void)
{
   uint16_t i;
   uint8_t  ch;
   uint16_t test_num = 1;
   ut_res_t res;

   ut_register_test_grp("Circ Buf");
   
   ut_assert_expected = TRUE;
   ut_num_asserts_raised = 0;
   res = init_cb(NULL, test_cb_mem, sizeof(test_cb_mem));
   ut_result(test_num++, CB_SUCCESS == res && ut_num_asserts_raised == 0 ? UT_PASS : UT_FAIL, UT_EXP_FAIL, "init_cb parameter 1 check");

   ut_num_asserts_raised = 0;
   res = init_cb(&ut_cb, NULL, UT_CB_BUF_SIZE);
   ut_result(test_num++, CB_SUCCESS == res && ut_num_asserts_raised == 0 ? UT_PASS : UT_FAIL, UT_EXP_FAIL, "init_cb parameter 2 check");

   ut_assert_expected = FALSE;
   ut_num_asserts_raised = 0;
   res = init_cb(&ut_cb, test_cb_mem, 0);
   ut_result(test_num++, CB_SUCCESS == res && ut_num_asserts_raised == 0? UT_PASS : UT_FAIL, UT_EXP_FAIL, "init_cb parameter 3 check");

   res = init_cb(&ut_cb, test_cb_mem, sizeof(test_cb_mem));
   ut_result(test_num++, CB_SUCCESS == res ? UT_PASS : UT_FAIL, UT_EXP_PASS, "init_cb");

   ut_result(test_num++, ut_cb.cb_start == ut_cb.cb_put ? UT_PASS : UT_FAIL, UT_EXP_PASS, "init_cb start == put");

   res = CB_SUCCESS;
   for (i=0, ch=0; i<UT_CB_BUF_SIZE-1 && res == CB_SUCCESS; i++, ch++)
   {
      res = add_ch_to_cb(&ut_cb, ch);
      ut_result(test_num++, res == CB_SUCCESS ? UT_PASS : UT_FAIL, UT_EXP_PASS, "add_ch_to_cb filling up cb");
   }

   ut_result(test_num++, ut_cb.cb_size - ut_cb.cb_free_bytes == UT_CB_BUF_SIZE-1 ? UT_PASS : UT_FAIL, UT_EXP_PASS, "add_ch_to_cb [size-1]");

   ut_result(test_num++, ut_cb.cb_free_bytes == 1 ? UT_PASS : UT_FAIL, UT_EXP_PASS, "free_bytes == 1");

   res = add_ch_to_cb(&ut_cb, ch);
   ut_result(test_num++, CB_WRAPPED == res ? UT_PASS : UT_FAIL, UT_EXP_PASS, "add_ch_to_cb returns CB_WRAPPED on wrapping around");

   /* now checkout free_cb_block */
   ut_assert_expected = TRUE;
   ut_num_asserts_raised = 0;
   res = free_cb_block(&ut_cb, &test_cb_mem[1], sizeof(test_cb_mem));
   ut_result(test_num++, CB_SUCCESS == res && ut_num_asserts_raised == 1 ? UT_PASS : UT_FAIL, UT_EXP_FAIL, "free_cb_block wrong block pointer");
   ut_assert_expected = FALSE;

   res = free_cb_block(&ut_cb, &test_cb_mem[0], sizeof(test_cb_mem));
   ut_result(test_num++, CB_SUCCESS == res ? UT_PASS : UT_FAIL, UT_EXP_PASS, "free_cb_block in one go");
   ut_result(test_num++, ut_cb.cb_free_bytes == ut_cb.cb_size ? UT_PASS : UT_FAIL, UT_EXP_PASS, "free_cb_block free_bytes==size");

   res = free_cb_block(&ut_cb, &test_cb_mem[0], sizeof(test_cb_mem));
   ut_result(test_num++, CB_SUCCESS == res ? UT_PASS : UT_FAIL, UT_EXP_FAIL, "free_cb_block same block twice");
   ut_result(test_num++, ut_cb.cb_free_bytes == ut_cb.cb_size ? UT_PASS : UT_FAIL, UT_EXP_PASS, "free_cb_block same block twice free_bytes==size");

   /* setup correct cb for test scenario */
   res = init_cb(&ut_cb, test_cb_mem, sizeof(test_cb_mem));

   /* todo: add test scenario(s) for move_cb_block_over_boundry */
   /* todo: add test scenario(s) for remove_ch_from_cb */

   ut_test_grp_finished();
}
#endif /* UNIT_TEST */

/******************************************************************************
* The information in this section will be part of PVCS tracking information    
*                                                       
* $Workfile:   circ_buf.c  $                                         
* $Revision:   1.10  $ 
* $Log:   W:/Pvcs-Arc/Nexus/archives/wave2/Common/src/circ_buf.c-arc  $                                        
 * 
 *    Rev 1.10   Aug 30 2006 01:26:00   vi200ro
 * - support for pwm logs
 * 
 *    Rev 1.9   Jan 06 2006 14:42:16   vi200ro
 * - fixed compiler error
 * 
 *    Rev 1.8   Dec 22 2005 14:14:14   vi200ro
 * - now raising E209 if trying to free incorrect block.
 * - now also trying to honour the free if at all possible to preventa waterfall of errors
 * 
 *    Rev 1.7   Nov 14 2005 11:56:00   vi200ro
 * - Added clear_cb() function
 * 
 *    Rev 1.6   Oct 26 2005 16:38:42   vi200ro
 * - now using varassert
 * 
 *    Rev 1.5   Sep 22 2005 18:12:24   vi200ro
 * - now using eventlog to log circular buffer free-ing
 * - fixed problem with free-ing a block crossing the buffer end boundry
 * 
 *    Rev 1.4   Jul 07 2005 19:47:26   vi200ro
 * - reduced logging
 * 
 *    Rev 1.3   Jun 23 2005 16:09:42   vi200ro
 * - made add_buf_to_cb() and free_cb_block() const correct
 * 
 *    Rev 1.2   Jun 17 2005 15:30:16   vi200ro
 * - added add_buf_to_cb
 * 
 *    Rev 1.1   Jun 16 2005 09:52:00   vi200ro
 * - Added remove_ch_from_cb
 * - Added more debugging info
 * 
 *    Rev 1.0   Jun 14 2005 15:07:22   vi200ro
 * Initial revision.
 * 
 * 
*******************************************************************************/
    
