/* TAB SETTING = 4 (e.g. 5, 9, 13, 17...)	*/
/************************************************************************
*	PROJECT:		Spark
*	MODULE NAME:	$Workfile:   fservice.c  $
*	REVISION:		$Revision:   1.8  $
*	
*	DESCRIPTION:	Routines for retrieving information from FLASH and from
*					FLASH components.
*
* ----------------------------------------------------------------------
*               Copyright (c) 1998 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*
*	REVISION HISTORY:
*	$Log:   H:/group/SPARK/ARCHIVES/DM150/uic_app/fservice.c_v  $
 * 
 *    Rev 1.8   21 Dec 2004 12:59:42   sa002pe
 * Changed the compType local variable from a short to a char
 * because the component type is only one byte.
 * Also fixed a minor lint error concerning the included files.
 * 
 *    Rev 1.7   03 Mar 2004 14:20:46   HO501SU
 * Deleted #include "pmcitask.h" if not using it.
 * 
 * Left cedmmfg.c unchanged since it's shared
 * with Mega. PMCITASK.H can be deleted from
 * Janus project if cedmmfg.c is corrected for 
 * Janus. The #defines in PMCITASK.H are moved 
 * to sys.h and oijprint.h.
 *  
 * This related files are:
 * CMOS.C, creport.c, FService.c, megabob.c,
 * OICMENUCONTROL.C, OICOMMON.C,
 * oijprint.h, OIPWRUP.C, PMCITASK.H
 * 
 *    Rev 1.6   11 Nov 2003 16:25:52   defilcj
 * 1. implement fnFlashGetGCNamesCnt
 * 
 *    Rev 1.5   24 Oct 2003 13:57:38   defilcj
 * 1. cleanup, remove #if 0 around fnFlashGetGraphicFeatureCode
 * 
 *    Rev 1.4   25 Sep 2003 11:32:12   defilcj
 * 1. EMD split
 * 2. trim the fat
 * 
 *    Rev 1.3   22 Aug 2003 17:02:36   defilcj
 * 1. fix fnFlashGetGCunikodes
 * 
 *    Rev 1.2   06 Aug 2003 16:32:24   defilcj
 * 1. some fixes for janus ad and insc support
 * 
 *    Rev 1.1   29 Apr 2003 16:55:08   defilcj
 * 1. add abitlity to fnFlashGetGraphicMemberAddr
 * to get the address of the currency type which is the
 * start of the data area the IG is interested in.
 * 
 *    Rev 1.0   25 Apr 2003 17:34:22   defilcj
 * initial janus rev, for debit build
* 
*	4 janus 
*	
*    Rev 1.32   Apr 08 2002 19:21:16   bailebf
* Changes to support GPS serial connection and
* art card requires GPS 5.0.28
* 
*    Rev 1.31   Mar 19 2002 17:16:42   bailebf
* Fixed return value on fnIsThisArtInstalled
* 
*    Rev 1.30   Mar 15 2002 15:35:14   bailebf
* Change over from barcode type in visiblilty to
* using component type in header to decide 
* what header to use in extracting data
* 
*    Rev 1.29   Mar 14 2002 18:07:26   arsenbg
* removed a coupla more HEIGHT conditionals that i had forgotten about
* 
*    Rev 1.28   Mar 14 2002 17:34:58   arsenbg
* enabled retrieval of height from graphic component headers
* 
*    Rev 1.27   Mar 14 2002 00:05:20   arsenbg
* encased the graphic height thingy in conditionals where necessary
* 
*    Rev 1.26   13 Mar 2002 15:46:04   bailebf
* Added support for height in graphic header
* Requires GCS/FSTUB with height or you will
* crash.
* Not tested because FSTUB not available as of EOD
* 
*    Rev 1.25   28 Feb 2002 16:03:10   bailebf
* Updated fnIsThisArtInstalled to check data name
* and rev.  May still need to be refined for TC
* 
*    Rev 1.24   Jan 15 2002 14:08:54   bailebf
* Fixed typo/cut&paste error
* 
*    Rev 1.23   Jan 15 2002 13:26:20   bailebf
* Changes to fnIsThisArtInstalled, fnGetHashSDRfieldPtr,
* fnIfNotRequestedXLink, for artcard download.
* 
*    Rev 1.22   Dec 03 2001 14:52:54   thillas
* fnReportRegisters function changed to accomodate
* two bytes for justification
* 
*    Rev 1.21   Nov 29 2001 17:40:14   thillas
* added more compatability and more fields 
* for comet graphic headers in gcextractor
* 
*    Rev 1.20   Nov 09 2001 10:49:30   monrogt
* Changed where system include files
* were included
* 
* 
*    Rev 1.19   Oct 25 2001 15:51:40   arsenbg
* no changes
* 
*    Rev 1.18   27 Aug 2001 20:51:52   arsenbg
* added more compatability and more fields for comet graphic headers in gcextractor.
* 
*    Rev 1.17   Mar 07 2001 10:46:02   bailebf
* More changes to support Public key 
* components and extended flash directory
* 
*    Rev 1.16   Feb 23 2001 09:43:06   bailebf
* remove references to gcHeader structure where
* it was not needed to make it easier to support
* new comet/mega graphic components
* 
*    Rev 1.15   Nov 09 2000 06:59:32   bailebf
* Added code to support multiple language PH
* report graphic selection based on screen language
* in use.  Fixed code to support byte addressed
* access versus word bounded access.
* 
*    Rev 1.14   01 May 2000 19:30:38   arsenbg
* removed several more unused functions
* 
*    Rev 1.13   01 May 2000 16:45:56   arsenbg
* removed fnFlashGetLinkedGraphicStruct() and fnFlashBuildGraphicStruct()
* 
*    Rev 1.12   Dec 21 1999 18:06:16   kirscwa
* Corrected fnIsArtInstalled so that it correctly
* loops through all of the graphics.
* 
*    Rev 1.11   15 Dec 1999 11:35:12   arsenbg
* added function fnIsThisArtInstalled
* 
*    Rev 1.10   12 Dec 1999 20:41:44   arsenbg
* town circle stuff, art stuff
* 
*    Rev 1.9   27 Oct 1999 14:23:18   arsenbg
* SUPPORT FOR NEW FLASH ARCHITECTURE
* 
*    Rev 1.8   09 Jul 1999 15:22:38   arsenbg
* changed reference from GCSflash to TOP_OF_FLASH
* 
*    Rev 1.7   01 Jun 1999 10:51:44   arsenbg
* compatability with PHC version 1.8 (low postage indicia, blanking, currency in image, more)
* 
* 
*    Rev 1.6   09 May 1999 17:55:24   arsenbg
* changed anything having to do with the graphics headers and currency type, for compliance with the new GCSflash
* 
*    Rev 1.5   23 Apr 1999 10:38:46   arsenbg
* added references to pDirectory, lwCurrentContext, and GCSflash
* added new function, fnReportRegisters
* made some changes to  fnFlashGetReportIndexRecord and fnFlashReportMember
* 
* 
* 
*    Rev 1.4   25 Mar 1999 14:49:10   arsenbg
* support for new flash containing a function ID in the graphic register descriptors
* 
*    Rev 1.3   22 Mar 1999 14:43:52   arsenbg
* converted anything that had to do with the original FLASH MO FIASCO to compatability with the new FLASH LOGICAL NUMBER FIASCO
* 
*    Rev 1.2   21 Feb 1999 15:15:38   arsenbg
* a large amount of editing of graphic component header services
* 
* 
*************************************************************************/

/*lint -e7*/
#include <string.h>
/*lint +e7*/

#include "global.h"
#include "ossetup.h"
#include "pbos.h"
#include "bob.h"
#include "junior.h"
#include "bobutils.h"
#include "clock.h"
#include "fwrapper.h"
#include "fstub.h"
#include "aiservic.h"
#include "flashutil.h"

extern	DIRECTORYCOMP	pDirectory;		/* derived FLASH directory, in which the offsets	*/
extern	ulong	lwCurrentContext;
extern unsigned long Current_Top_Of_Flash;

BOOL fnFlashGetGCvisibility		(void *componentAddr, void *destination)	{ return(fnFlashGetGraphicMember(componentAddr, VISIBILITY, destination) );	}
BOOL fnFlashGetGCcomponentType	(void *componentAddr, void *destination) 	{ return(fnFlashGetGraphicMember(componentAddr, COMPONENTTYPE, destination)); }
BOOL fnFlashGetGCnamesCnt		(void *componentAddr, void *destination) 	{ return(fnFlashGetGraphicMember(componentAddr, NAMESCNT, destination)); }
BOOL fnFlashGetGCunikodes		(void *componentAddr, void *destination) 	{ return(fnFlashGetGraphicMemberAddr(componentAddr, UNIKODES, destination)); }
	

/* **********************************************************************

 FUNCTION NAME: 	void *fnFlashGetNextAnyLink(void *currentLink)

 PURPOSE: 			Returns the address of the next link in a linked graphics
					component list.  Passed void pointer (currentLink) is normally the
					address of the current point in the linked list.  
					
					If the passed pointer is NULL, this function assumes that it is to
					start at the beginning of the linked list.

					A NULL return indicates that the passed link is at the end
					of the linked list.
										
 AUTHOR: 			R. Arsenault

 INPUTS:			
 *************************************************************************/
void *fnFlashGetNextAnyLink(void *currentLink) {
	char  *linkaddr;															/* local copy to store the link info structure	*/
	ulong  nextLink;															/*  pointer to next link						*/
			
	linkaddr = NULL;											 
	if (currentLink == NULL) {
		linkaddr = (char *)fnFlashGetAddress(LINKED_IMAGES_DC);
	}
	else {
		if (currentLink != NULL) {													
			EndianAwareCopy((char *)(&nextLink), (char *)currentLink + COMPONENT_OFFSET, sizeof(long));
			
			if (nextLink != 0 && nextLink != -1) 
				linkaddr = (char *)(nextLink + Current_Top_Of_Flash);
		}
	}
	return((void *)linkaddr);
}


/******************************************************************************
   FUNCTION NAME: fnFlashGetGCHeaderAddr
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  :
   				: 
   PARAMETERS   : None
   RETURN       : None
   NOTES		: None
******************************************************************************/
void* fnFlashGetGCHeaderAddr(void* pLinkHeader)
{
	return( (uchar*)pLinkHeader+STARTOFMEMBER_INDEX);
}


/* **********************************************************************

 FUNCTION NAME: 	ushort fnFlashGetGraphicFeatureCode(void *linkPtr);

 PURPOSE: 			Returns the feature enable code for the graphic component
					referenced by the passed link pointer.
										
 AUTHOR: 			R. Arsenault

 INPUTS:			
 *************************************************************************/
ushort fnFlashGetGraphicFeatureCode(void *linkPtr) {
	struct	LinkHeader linky;														/* structure for reading the link info			*/

	memcpy((char *)(&linky), (char *)linkPtr, sizeof(struct LinkHeader));			/* copy the link info							*/
	return(linky.FeatureCode);														/* return the feature code						*/
}


/******************************************************************************
   FUNCTION NAME: fnFlashGetGraphicMember
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Get a copy of a graphic header or data component
   				: 
   PARAMETERS   : pointer a graphic component header,
   				: Component member ID (see enum of gcMemberIDs in fwrqpper.h),
   				: destination pointer where requested data will be copied  
   RETURN       : FALSE if memberID is invalid, True otherwise
   NOTES		: Some components must be accessed via a pointer with 
				: fnFlashGetGraphicMemberAddr
******************************************************************************/

BOOL fnFlashGetGraphicMember(void *gcHeader, short memberID, void *dest)
{
	BOOL	retval = FALSE;
	uchar	*source, compType;
	ushort	copysiz, numitems;
	ushort  offset1;

	source = (uchar *)gcHeader;
	memcpy(&compType, (uchar*)(source+COMPONENTTYPE_OFF), SZ_COMPONENTTYPE);

	if(memberID <= UNIKODES)
	{
		retval = TRUE;

		//most of the header stuff is here
		switch(memberID)
		{
			case COMPONENTSUM: 
				source += COMPONENTSUM_OFF;
				copysiz = SZ_COMPONENTSUM;
				break;
			case COMPONENTSIZE:
				source += COMPONENTSIZE_OFF;
				copysiz = SZ_COMPONENTSIZE;
				break;
			case VISIBILITY:
				source += VISIBILITY_OFF;
				copysiz = SZ_VISIBILITY;
				break;
			case SCHEMA_VERSION:
				source += SCHEMA_VERSION_OFF;
				copysiz = SZ_SCHEMA_VERSION;
				break;
			case GRAPHIC_DATA_AREA_SIZE:
				source += GRAPHIC_DATA_AREA_SIZE_OFF;
				copysiz = SZ_GRAPHIC_DATA_AREA_SIZE;
				break;
			case GRAPHIC_DATA_AREA_ADDR:
				source += GRAPHIC_DATA_AREA_ADDR_OFF;
				copysiz = SZ_GRAPHIC_DATA_AREA_ADDR;
				break;	
			case COMPONENTTYPE:
				source += COMPONENTTYPE_OFF;
				copysiz = SZ_COMPONENTTYPE;
				break;
			case NAMESCNT:
				source += NAMESCNT_OFF;
				copysiz = SZ_NAMESCNT;
				break;
//			case UNIKODES:
//				source += UNIKODES_OFF;
//				copysiz = SZ_UNIKODES;
//				break;			 	
			default:
				retval = FALSE;
				break;
		}
	}				
	else if((memberID > UNIKODES) && (memberID < CURRENCYTYPE) && (compType != IMG_ID_TEXT_AD))
	{
		retval = TRUE;

		//index source to point to data area
		EndianAwareCopy((uchar*)&offset1, (uchar*)(source+GRAPHIC_DATA_AREA_ADDR_OFF), SZ_GRAPHIC_DATA_AREA_ADDR);
		source += offset1;

		switch(memberID)
		{
			case SZOF_IBUTTON_HASHANDSIG:
				copysiz = SZ_SZOF_IBUTTON_HASHANDSIG;
				break;
			case IBUTTON_HASHANDSIG:
				source += SZ_SZOF_IBUTTON_HASHANDSIG;
				EndianAwareCopy((uchar*)&copysiz,(uchar*)source,SZ_SZOF_IBUTTON_HASHANDSIG);
				break;
			default:
				retval = FALSE;
				break;
		}
	}
	else if( (memberID <= HEIGHT) && (compType != IMG_ID_TEXT_AD) )
	{
		retval = TRUE;

		//index source to point to data area
		EndianAwareCopy((uchar*)&offset1, (uchar*)(source+GRAPHIC_DATA_AREA_ADDR_OFF), SZ_GRAPHIC_DATA_AREA_ADDR);
		source += offset1;
		//index source to point to currency type
		EndianAwareCopy((uchar*)&offset1,(uchar*)source,SZ_SZOF_IBUTTON_HASHANDSIG);
		source += SZ_SZOF_IBUTTON_HASHANDSIG + offset1;

		switch(memberID)
		{
			case CURRENCYTYPE:
				copysiz = SZ_CURRENCYTYPE;
				break;
			case RESERVED:
				source += RESERVED_OFF;
				copysiz = SZ_RESERVED;
				break;
			case GRAPHIC_DATA_NAME_REV:
				source += GRAPHIC_DATA_NAME_REV_OFF;
				copysiz = SZ_GRAPHIC_DATA_NAME_REV;
				break;
			case WIDTH:					
				source += WIDTH_OFF;
				copysiz = SZ_WIDTH;
				break;
			case HEIGHT:					
				source += HEIGHT_OFF;
				copysiz = SZ_HEIGHT;
				break;
			default:
				retval = FALSE;
				break;
		}
	}
	else if( (compType != IMG_ID_TEXT_AD) && (compType != IMG_ID_BAR_GRAPHIC) )
	{
		retval = TRUE;

		//index source to point to data area
		EndianAwareCopy((uchar*)&offset1, (uchar*)(source+GRAPHIC_DATA_AREA_ADDR_OFF), SZ_GRAPHIC_DATA_AREA_ADDR);
		source += offset1;
		//index source to point to currency type
		EndianAwareCopy((uchar*)&offset1,(uchar*)source,SZ_SZOF_IBUTTON_HASHANDSIG);
		source += SZ_SZOF_IBUTTON_HASHANDSIG + offset1;

		switch(memberID)
		{
			case NUM_OF_REG_GROUPS:		
				source += NUM_OF_REG_GROUPS_OFF;
				copysiz = SZ_NUM_OF_REG_GROUPS;
				break;
			case PAD:					
				source += PAD_OFF;
				copysiz = SZ_PAD;
				break;
			case REGFIELDCNT:			
				source += REGFIELDCNT_OFF;
				copysiz = SZ_REGFIELDCNT;
				break;
			case BITMAPSIZE:				
				source += BITMAPSIZE_OFF;
				copysiz = SZ_BITMAPSIZE;
				break;
			case OFFSET_TO_REGFDS:			
				source += OFFSET_TO_REGFDS_OFF;
				copysiz = SZ_OFFSET_TO_REGFDS;
				break;
			case OFFSET_TO_BITMAP:
				source += OFFSET_TO_BITMAP_OFF;				
				copysiz = SZ_OFFSET_TO_BITMAP;				
				break;						
//			case REGISTERS:
//				memcpy((uchar*)&ucNumitems,(uchar*)source+NUM_OF_REG_GROUPS_OFF, SZ_NUM_OF_REG_GROUPS);
//				source += REGISTERS_OFF;
//				copysiz = ucNumitems * sizeof();				
//				break;														
//			case REG_FIELD_DESC_RECS:
//				memcpy((uchar*)&ucNumitems,(uchar*)source+REGFIELDCNT_OFF, SZ_REGFIELDCNT);
//				memcpy((uchar*)&offset1,(uchar*)(source + OFFSET_TO_REGFDS), SZ_OFFSET_TO_REGFDS);
//				source += offset1;
//				copysiz = ucNumitems * sizeof();				
//				break;
//			case BITMAP:
//				memcpy((uchar*)&offset1,(uchar*)(source + OFFSET_TO_BITMAP_OFF), SZ_OFFSET_TO_BITMAP);
//				source += offset1;
//				copysiz = SZ_REG_FIELD_DESC_RECS;
//				break;
			default:
				retval = FALSE;
				break;
		}
	}

	if(retval)
	{
		EndianAwareCopy((char *)dest, source, copysiz);
	}
		
	return(retval);
}

/******************************************************************************
   FUNCTION NAME: fnFlashGetGraphicMemberAddr
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Get a pointer to a graphic header or data component
   				: 
   PARAMETERS   : pointer a graphic component header,
   				: Component member ID (see enum of gcMemberIDs in fwrqpper.h),
   				: pointer to pointer where requested data can be read
   RETURN       : FALSE if memberID is invalid, True otherwise
   NOTES		: None
******************************************************************************/

BOOL fnFlashGetGraphicMemberAddr(void *gcHeader, short memberID, void** dest)
{
	BOOL	retval = FALSE;
	uchar	*source,compType;
	ushort	copySiz;
	ushort  offset1;

	source = (uchar *)gcHeader;
	memcpy(&compType, (uchar*)(source+COMPONENTTYPE_OFF), SZ_COMPONENTTYPE);

	if((memberID <= UNIKODES) && (compType != IMG_ID_BAR_GRAPHIC))
	{
		retval = TRUE;

		//most of the header stuff is here
		switch(memberID)
		{
			case UNIKODES:
				source += UNIKODES_OFF;
				break;			 	
			default:
				retval = FALSE;
				break;
		}
	}				
	else if((memberID > UNIKODES) && (memberID <= CURRENCYTYPE) && (compType != IMG_ID_TEXT_AD))
	{
		retval = TRUE;

		//index source to point to data area
		EndianAwareCopy((uchar*)&offset1, (uchar*)(source+GRAPHIC_DATA_AREA_ADDR_OFF), SZ_GRAPHIC_DATA_AREA_ADDR);
		source += offset1;

		switch(memberID)
		{
			case IBUTTON_HASHANDSIG:
				source += SZ_SZOF_IBUTTON_HASHANDSIG;
				break;
			case GRAPHIC_DATA_AREA_AFTER_SIG:
				//index source to point to currency type
				EndianAwareCopy((uchar*)&offset1,(uchar*)source,SZ_SZOF_IBUTTON_HASHANDSIG);
				source += SZ_SZOF_IBUTTON_HASHANDSIG + offset1;
				break;
			default:
				retval = FALSE;
				break;
		}
	}
	else if( (memberID <= HEIGHT) && (compType != IMG_ID_TEXT_AD) ) 
	{
		retval = TRUE;

		//index source to point to data area
		EndianAwareCopy((uchar*)&offset1, (uchar*)(source+GRAPHIC_DATA_AREA_ADDR_OFF), SZ_GRAPHIC_DATA_AREA_ADDR);
		source += offset1;
		//index source to point to currency type
		EndianAwareCopy((uchar*)&offset1,(uchar*)source,SZ_SZOF_IBUTTON_HASHANDSIG);
		source += SZ_SZOF_IBUTTON_HASHANDSIG + offset1;

		switch(memberID)
		{
			case GRAPHIC_DATA_NAME_REV:
				source += GRAPHIC_DATA_NAME_REV_OFF;
				break;
			default:
				retval = FALSE;
				break;
		}
	}
	else if((compType != IMG_ID_TEXT_AD) && (compType != IMG_ID_BAR_GRAPHIC)) 
	{
		retval = TRUE;

		//index source to point to data area
		EndianAwareCopy((uchar*)&offset1, (uchar*)(source+GRAPHIC_DATA_AREA_ADDR_OFF), SZ_GRAPHIC_DATA_AREA_ADDR);
		source += offset1;
		//index source to point to currency type
		EndianAwareCopy((uchar*)&offset1,(uchar*)source,SZ_SZOF_IBUTTON_HASHANDSIG);
		source += SZ_SZOF_IBUTTON_HASHANDSIG + offset1;

		switch(memberID)
		{
			case REGISTERS:
				source += REGISTERS_OFF;				
				break;														
			case REG_FIELD_DESC_RECS:
				EndianAwareCopy((uchar*)&offset1,(uchar*)(source + OFFSET_TO_REGFDS_OFF), SZ_OFFSET_TO_REGFDS);
				source += offset1;
				break;
			case BITMAP:
				EndianAwareCopy((uchar*)&offset1,(uchar*)(source + OFFSET_TO_BITMAP_OFF), SZ_OFFSET_TO_BITMAP);
				source += offset1;
				break;
			default:
				retval = FALSE;
				break;
		}
	}

	if(retval)
	{
		*dest = (const void*)source;
	}

	return(retval);
}

/* *********************************************************************************************

 FUNCTION NAME: 	BOOL fnThisArtInstalled(uchar compTyp, void pGCHeader) 

 PURPOSE: 			Compares the data name and data rev in the signed data record (sdr) of the
 					incoming graphic to the data name and data rev in the sdr of all the graphics
 					stored in the flash.
 					
 					Returns TRUE if	an exact match is found and false otherwise.

					INPUTS
					----------------------
					compType - the component type that is being compared.
					sdr - a ptr to the sdr of the graphic that is being used as a reference
										
 AUTHOR: 			R. Arsenault, W. Kirschner / Craig DeFilippo

 ************************************************************************************************/
//TODO - not called - confirm and remove
BOOL fnIsThisArtInstalled(uchar compTyp, void *pGCHeader) 
{
	char	*LinkPtr;				// Pointer to current graphic 			
	uchar	testComp;				// Component type of current graphic 	
	uchar 	*refDataNameRev; 		// data name and rev from refSdr
	uchar 	*thisDataNameRev; 		// data name and rev from refSdr
	BOOL	retVal = FALSE; 

	//Get reference data name and rev pointers
	if(fnFlashGetGraphicMemberAddr(pGCHeader, GRAPHIC_DATA_NAME_REV, (void**) &refDataNameRev))
	{
		// Get the first link in the list in flash
		LinkPtr = (char *)(fnFlashGetNextAnyLink((void *)(NULL)));

		// While there is another graphic 
		while(LinkPtr != NULL && retVal == FALSE)
		{

			// Get the component type of the graphic 
			fnFlashGetGCcomponentType(LinkPtr + COMPONENTGRAPHIC_OFFSET , &testComp);

			// If the component type equals the one we are looking for 
			if (testComp == compTyp) 
			{
				// Get the data name field pointer
				if(fnFlashGetGraphicMemberAddr((void *)(LinkPtr + FLASHLINKHEADERSIZE), GRAPHIC_DATA_NAME_REV, (void**) &thisDataNameRev))
				{
					// Compare Data name to reference and return true if they are equal 
					if (memcmp(thisDataNameRev, refDataNameRev, SZ_GRAPHIC_DATA_NAME_REV) == 0)
						retVal = TRUE;
				}
			}
			// Get the next pointer 
			LinkPtr = fnFlashGetNextAnyLink((void *)(LinkPtr));
		}
	}

	// No matches 
	return(retVal);
}
