/************************************************************************
*   PROJECT:        Mega, Phoenix, Janus, NextGem
*   MODULE NAME:    Chasing_Shadows.c
*   Author:         Sunny Liao & Sandra Peterson
*   Date:           23th Oct.
*   DESCRIPTION:    Some Functions which are used in Chasing Shadows feature.
*       
* ----------------------------------------------------------------------
*               Copyright (c) 2003-2005 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*   REVISION HISTORY:
*
* 2009.10.08    Clarisa Bellamy     FPHX 1.11
*  - Added two new functions: fnCMOSShadowUpgradeDebit and fnCMOSShadowUpgradeRefill.
*    These will handle the initialization of the new log and entry members, for the 
*    debit and refill logs, including any adjustment to the size member of each entry.
*    These are called from fnInitializeCMOS() after a CMOS Preservation.  
*    This initialization must use the Layout Version instead of the CMOS version, 
*    because of the overlapping CMOS versions used for old and new layout versions
*    on different branches.
*
* 2009.09.11    Clarisa Bellamy     FPHX 1.11
*   Merged from Janus Tips:
    * 09-Jul-08 sa002pe on Janus tips shelton branch
    *   Added some comments to the header for fnCMOSSetShadowRecordTypes.
    *   Added fnCMOSSetShadowRecordIndiciumType.
    *
    * 14-Jan-08 sa002pe on Janus tips shelton branch
    *   Fixed a bug in fnCMOSValidateAgainstShadowLogs where it was doing the post-adjustment
    *   validation against the debit log instead of the refill log even though the adjustment
    *   is now stored in the refill log.
    *
    * 10-Oct-07 sa002pe on Janus tips shelton branch
    *   Changed fnCMOSAddNewExtendedShadowRefill to have a case for loading the record type
    *   for adjustment records.
    *   Changed fnCMOSValidateAgainstShadowLogs to put the adjustment record in the refill log.
    *
    * 29-Nov-06 sa002pe on mega tips shelton branch
    *   Changed fnClearShadowLogs to fix the century & year portion of sStartDateTime to be 2000, not 1006.
    *
    * 28-Nov-06 wi001ba on mega tips shelton branch
    *   Changed fnCMOSAddNewExtendedShadowDebit to load the indicia record layout type as
    *   the record type for adjustment transactions for Mega.
    *
    * 16-Nov-06 sa002pe on mega tips shelton branch
    *   Merging in latest changes to shadow stuff from janus tips:
    ----*
        * 16-Nov-06 sa002pe on janus tips shelton branch
        *   Removed most of the old stuff now that Mega is upgrading to the latest generation
        *   of the shadow stuff and everyone else will have to make a conscious decision to
        *   use this version of this file.
        *   Changed fnClearShadowLogs & fnCMOSAddNewExtendedShadowDebit per what Mega will be doing.
        *   #ifdef'ed out fnCMOSValidateAgainstShadowLogs for Mega because it won't work based on
        *   what they have in their shadow debit log.
        *
        * 27-Oct-06 sa002pe on Janus tips shelton branch
        *   Changed fnClearShadowLogs to load an actual date into the date/time area
        *   of each entry in both logs.
        *   Added new stuff to fnCMOSAddNewExtendedShadowDebit & fnCMOSAddNewExtendedShadowRefill
        *   to support the 2nd extension of the refill log & the 3rd extension of the debit log.
        *   Fixed boo-boo's in fnGetShadowDebitRecord & fnGetShadowRefillRecord. They were
        *   validating the stored entry size against the size of the entire log instead of
        *   the size of the log entry structure.
        *   Changed fnCMOSValidateAgainstShadowLogs to load the PBI serial number as the
        *   record data when doing an adjustment.
        *   Added fnCMOSSetShadowRefillRecordType,
        *   fnCMOSSetShadowRefundRecordType & fnCMOSSetShadowIndiciumRecordType.
        *
        * 20-Oct-06 sa002pe on Janus tips shelton branch
        *   Changed fnClearShadowLogs to init the new stuff in the logs.
        *   Fixed some lint stuff.
    ----*
----*
*
* 12-Oct-05 sa002pe on Janus 11.00 shelton branch
*   Changed to truncate the passed in data size to the max size when necessary and to load a 
*   different record status of the data was truncated.
*
* ----------------------------------------------------------------------
* PRE-CLEARCASE HISTORY:
*       $Log:   H:/group/SPARK/ARCHIVES/UIC/Chasing_Shadows.c_v  $
* 
*    Rev 1.5   18 Jul 2005 13:27:40   sa002pe
* Removed the check in the validation concerning the PC or the zero
* PC going backwards.  In some cases it is OK for this to happen.
* 
*    Rev 1.4   11 Jul 2005 11:30:58   sa002pe
* Added new stuff to support extended refill log & the 2nd
* extension of the debit log, all so the register validation
* can be done.
* 
*    Rev 1.3   05 Nov 2004 09:29:58   sa002pe
* Changed to have the new function only if the
* EXTENDED_DEBIT_LOG constant is defined.
* 
*    Rev 1.2   04 Nov 2004 15:04:40   sa002pe
* Added fnCMOSAddNewExtendedShadowDebit.
* Added conditionally compiled test code to
* fnCMOSAddNewShadowDebit.
* 
*    Rev 1.1   30 Oct 2003 20:55:14   SA002PE
* Fixed the check of the id of the requested record in
* Get Shadow Debit Record & Get Shadow Refill Record.
* Added some comments.
* 
*    Rev 1.0   28 Oct 2003 12:05:52   SA002PE
* Shadow register log functions
* 
***************************************************************************/

#include <stdio.h>
#include <string.h>

#include "chasing_shadows.h"
#include "clock.h"
#include "datdict.h"
#include "utils.h"

// ---------------------------------------------------------------------------
//      Local Variables:
// ---------------------------------------------------------------------------

// Local scratch pad for system log entries: 
static char pLogScratchPad[ 100 ];


// **********************************************
// Function Name:   fnClearShadowLogs
//
// Operation:   Clears both shadow logs.
//              For both CMOSShadowRefillLog and CMOSShadowDebitLog the
//              steps are the same:
//Author:   Sunny Liao          
//Date:     23th Oct.
//
// Inputs:      None. 
// Outputs:     None.
// Returns:     None.
// **********************************************

void fnClearShadowLogs(void) 
{   
    DATETIME    sStartDateTime = {20, 00, 01, 01, 00, 00, 00, 00};
    unsigned char i;


    //Clear Shadow Debit Log
    (void)memset(&CMOSShadowDebitLog,  0x30 , sizeof(ShadowDebitLog));
    CMOSShadowDebitLog.LogIndex = 0;
    CMOSShadowDebitLog.bRecordVersion = 0;
    CMOSShadowDebitLog.bIndicumRecordType = UNKNOWN_SHADOW_RECORD_TYPE;

    for (i = 0; i < NUMBER_OF_DEBIT_RECORDS; i++)
    {
        CMOSShadowDebitLog.ShadowDebitLogTable[ i ].bDataLayoutVersion = DEBIT_LOG_LAYOUT_VERSION;
        CMOSShadowDebitLog.ShadowDebitLogTable[ i ].bRecordType = UNKNOWN_SHADOW_RECORD_TYPE;
        CMOSShadowDebitLog.ShadowDebitLogTable[ i ].wDebitDataSize = 0;
        CMOSShadowDebitLog.ShadowDebitLogTable[ i ].bRecordStatus = INVALID_SHADOW;

#ifdef PHOENIX      // everybody except Mega
        CMOSShadowDebitLog.ShadowDebitLogTable[ i ].sDebitDateTime = sStartDateTime;
#endif
    }
    
    //Clear Shadow Refill Log
    (void)memset(&CMOSShadowRefillLog, 0x30, sizeof(ShadowRefillLog));
    CMOSShadowRefillLog.LogIndex = 0;
    CMOSShadowRefillLog.bRecordVersion = 0;
    CMOSShadowRefillLog.bRefillRecordType = UNKNOWN_SHADOW_RECORD_TYPE;
    CMOSShadowRefillLog.bRefundRecordType = UNKNOWN_SHADOW_RECORD_TYPE;

    for (i = 0; i < NUMBER_OF_REFILL_RECORDS; i++)
    {
        CMOSShadowRefillLog.ShadowRefillLogTable[ i ].bDataLayoutVersion = REFILL_LOG_LAYOUT_VERSION;
        CMOSShadowRefillLog.ShadowRefillLogTable[ i ].bRecordType = UNKNOWN_SHADOW_RECORD_TYPE;
        CMOSShadowRefillLog.ShadowRefillLogTable[ i ].wRefillDataSize = 0;
        CMOSShadowRefillLog.ShadowRefillLogTable[ i ].bRecordStatus = INVALID_SHADOW;
        CMOSShadowRefillLog.ShadowRefillLogTable[ i ].sRefillDateTime = sStartDateTime;
    }
}


// **********************************************
// Function Name:   fnCmosClearShadowRefillLogIndex
// Operation:   Sets CMOSShadowRefillLog.LogIndex to 0
// Author:  Sunny Liao
// Inputs:      none
// Outputs:     none
// **********************************************

void fnCmosClearShadowRefillLogIndex(void)
{
    CMOSShadowRefillLog.LogIndex = 0;
}


// **********************************************
// Function Name:   fnCmosClearShadowDebitLogIndex
// Operation:   Sets CMOSShadowDebitLog.LogIndex to 0
// Author:  Sunny Liao
// Inputs:      none
// Outputs:     none
// **********************************************
void fnCmosClearShadowDebitLogIndex(void)
{
    CMOSShadowDebitLog.LogIndex = 0;
}


// **********************************************
// Function Name:   fnCMOSAddNewExtendedShadowDebit
//
// Operation:   Adds a new entry record to the debit shadow log.
//              The index of where in the log the record is to be
//              copied is specified by CMOSShadowDebitLog.LogIndex
//
//Inputs:       dataSiz:
//              Number of bytes to be copied to entry member
//              bDebitLogData.
//
//              pieceCount:
//              value to be copied to entry member lDebitPieceCount
//
//              zeroPieceCount:
//              value to be copied to entry member lZeroPieceCount
//
//              pARptr:
//              pointer to the 5 bytes to be copied into entry member
//              bAR.
//
//              pDRptr:
//              pointer to the 5 bytes to be copied into entry member
//              bDR.
//
//              transType:
//              value to be copied to entry member lTransType
//
//              dataptr:
//              pointer to bytes to be copied into entry member
//              bDebitLogData.
//
// Outputs:     BOOL return value:
//                  FALSE = unable to enter data
//                  TRUE = entry has been successfully completed.
// **********************************************

BOOL fnCMOSAddNewExtendedShadowDebit(unsigned short dataSiz, unsigned long pieceCount,
                                        unsigned long zeroPieceCount, const unsigned char *pARptr,
                                        const unsigned char *pDRptr, const unsigned char *dataptr,
                                        unsigned char transType)
{
    BOOL bStatus = FALSE;
    BOOL bTruncated = FALSE;
    unsigned char bLogIndex;
    ShadowDebitLogEntry *pLogEntry;


    bLogIndex = CMOSShadowDebitLog.LogIndex;
    pLogEntry = &CMOSShadowDebitLog.ShadowDebitLogTable[bLogIndex];

    // If the data size is too big, truncate to the max
    if (dataSiz > MAX_SHADOW_DEBIT_ENTRY_SZ)
    {
        dataSiz = MAX_SHADOW_DEBIT_ENTRY_SZ;
        bTruncated = TRUE;
    }

    //If the index is within bounds, set value to a new record
    if (bLogIndex < NUMBER_OF_DEBIT_RECORDS)
    {
        bStatus = TRUE;

        //Clear the record, and set up some preliminary values
        (void)memset(pLogEntry, 0x30, sizeof(ShadowDebitLogEntry));
        pLogEntry->bRecordStatus = INVALID_SHADOW;
        pLogEntry->wDebitDataSize = 1;              // just to get the status to begin with

        //Add a new entry record to debit shadow log.
        pLogEntry->bDataLayoutVersion = DEBIT_LOG_LAYOUT_VERSION;

#ifdef PHOENIX      // everybody except Mega
        pLogEntry->lDebitPieceCount = pieceCount;
        (void)memcpy(pLogEntry->bAR, pARptr, DEBIT_LOG_AR_SIZE);
        (void)memcpy(pLogEntry->bDR, pDRptr, DEBIT_LOG_DR_SIZE);
        pLogEntry->lZeroPieceCount = zeroPieceCount;
#endif

        pLogEntry->bTransType = transType;

        switch(transType)
        {
        case SHADOW_TRANS_DEBIT:
            pLogEntry->bRecordType = CMOSShadowDebitLog.bIndicumRecordType;
            break;

        case SHADOW_TRANS_ADJUSTMENT:
#ifdef PHOENIX      // everybody except Mega
            pLogEntry->bRecordType = ADJUSTMENT_SHADOW_RECORD_TYPE;
#else
            pLogEntry->bRecordType = CMOSShadowDebitLog.bIndicumRecordType;
#endif
            break;

        default:
            pLogEntry->bRecordType = UNKNOWN_SHADOW_RECORD_TYPE;
            break;
        }

        //size = data size + everything from bRecordStatus up to,
        // but not including, bDebitLogData
        pLogEntry ->wDebitDataSize = dataSiz 
                                   + (   sizeof(ShadowDebitLogEntry) 
                                       - (  MAX_SHADOW_DEBIT_ENTRY_SZ 
                                          + sizeof(pLogEntry ->wDebitDataSize) ) );

        (void)memcpy(pLogEntry->bDebitLogData, dataptr,dataSiz);

#ifdef PHOENIX      // everybody except Mega
        if (GetSysDateTime(&pLogEntry->sDebitDateTime, ADDOFFSETS, GREGORIAN) == TRUE)
#endif
        {
            // now that the records been loaded, increment the index
            CMOSShadowDebitLog.LogIndex++;
            if (CMOSShadowDebitLog.LogIndex > NUMBER_OF_DEBIT_RECORDS - 1)
            {
                CMOSShadowDebitLog.LogIndex = 0;
            }

            // indicate that the record is good
            if (bTruncated == TRUE)
                pLogEntry->bRecordStatus = GOOD_TRUNCATED_SHADOW;
            else
                pLogEntry->bRecordStatus = GOOD_SHADOW;

            // indicate that the register validation can be done
            CMOSShadowDebitLog.bRecordVersion = SHADOW_VALIDATION_VERSION;
        }
    }

    return (bStatus);
}


// **********************************************
// Function Name:   fnCMOSAddNewExtendedShadowRefill
//
// Operation:   Adds a new entry record to the refill shadow log.
//              The index of where in the log the record is to be 
//              copied is specified by CMOSShadowDebitLog.LogIndex
//
//Inputs:       dataSiz:
//              Number of bytes to be copied to entry member 
//              bRefillLogData.
//
//              pieceCount:
//              value to be copied to entry member lPieceCount
//
//              zeroPieceCount:
//              value to be copied to entry member lZeroPieceCount
//
//              pARptr:
//              pointer to the 5 bytes to be copied into entry member 
//              bAR.
//
//              pDRptr:
//              pointer to the 5 bytes to be copied into entry member 
//              bDR.
//
//              transType:
//              value to be copied to entry member lTransType
//
//              dataptr:
//              pointer to bytes to be copied into entry member 
//              bRefillLogData.
//
// Outputs:     BOOL return value:
//                  FALSE = unable to enter data
//                  TRUE = entry has been successfully completed.
// **********************************************

BOOL fnCMOSAddNewExtendedShadowRefill (unsigned short dataSiz, unsigned long pieceCount, unsigned long zeroPieceCount,
                                      const unsigned char *pARptr, const unsigned char *pDRptr, const unsigned char *dataptr,
                                      unsigned char transType)
{
    BOOL bStatus = FALSE;
    BOOL bTruncated = FALSE;
    unsigned char bLogIndex;
    ShadowRefillLogEntry *pLogEntry;


    bLogIndex = CMOSShadowRefillLog.LogIndex;
    pLogEntry= &CMOSShadowRefillLog.ShadowRefillLogTable[ bLogIndex ];

    // If the data size is too big, truncate to the max
    if (dataSiz > MAX_SHADOW_REFILL_ENTRY_SZ)
    {
        dataSiz = MAX_SHADOW_REFILL_ENTRY_SZ;
        bTruncated = TRUE;
    }

    //If the index is within bounds, set value to a new record
    if (bLogIndex < NUMBER_OF_REFILL_RECORDS)
    {
        bStatus = TRUE;

        //Clear the record, and set up some preliminary values
        (void)memset(pLogEntry, 0x30, sizeof(ShadowRefillLogEntry));
        pLogEntry->bRecordStatus = INVALID_SHADOW;
        pLogEntry->wRefillDataSize = 1;             // just to get the status to begin with

        //Add a new entry record to refill shadow log.
        pLogEntry->bDataLayoutVersion = REFILL_LOG_LAYOUT_VERSION;
        pLogEntry->lPieceCount = pieceCount;
        pLogEntry->lZeroPieceCount = zeroPieceCount;
        (void)memcpy(pLogEntry->bAR, pARptr, SHADOW_LOG_AR_SIZE);
        (void)memcpy(pLogEntry->bDR, pDRptr, SHADOW_LOG_DR_SIZE);
        pLogEntry->bTransType = transType;

        switch(transType)
        {
        case SHADOW_TRANS_REFILL:
        case SHADOW_TRANS_KEYPAD_REFILL:
            pLogEntry->bRecordType = CMOSShadowRefillLog.bRefillRecordType;
            break;

        case SHADOW_TRANS_REFUND:
        case SHADOW_TRANS_KEYPAD_WITHDRAWAL:
            pLogEntry->bRecordType = CMOSShadowRefillLog.bRefundRecordType;
            break;

        case SHADOW_TRANS_ADJUSTMENT:
#ifdef PHOENIX      // everybody except Mega
            pLogEntry->bRecordType = ADJUSTMENT_SHADOW_RECORD_TYPE;
#else
            pLogEntry->bRecordType = CMOSShadowDebitLog.bIndicumRecordType;
#endif
            break;

        default:
            pLogEntry->bRecordType = UNKNOWN_SHADOW_RECORD_TYPE;
            break;
        }

        //size = data size + everything from bRecordStatus up to,
        // but not including, bRefillLogData
        pLogEntry ->wRefillDataSize = dataSiz 
                                    + (   sizeof(ShadowRefillLogEntry) 
                                        - (  MAX_SHADOW_REFILL_ENTRY_SZ 
                                           + sizeof(pLogEntry ->wRefillDataSize) ) );

        (void)memcpy(pLogEntry->bRefillLogData, dataptr, dataSiz);

        if (GetSysDateTime(&pLogEntry->sRefillDateTime, ADDOFFSETS, GREGORIAN) == TRUE)
        {
            // now that the records been loaded, increment the index
            CMOSShadowRefillLog.LogIndex++;
            if (CMOSShadowRefillLog.LogIndex > NUMBER_OF_REFILL_RECORDS - 1)
            {
                CMOSShadowRefillLog.LogIndex = 0;
            }

            // indicate that the record is good
            if (bTruncated == TRUE)
                pLogEntry->bRecordStatus = GOOD_TRUNCATED_SHADOW;
            else
                pLogEntry->bRecordStatus = GOOD_SHADOW;

            // indicate that the register validation can be done
            CMOSShadowRefillLog.bRecordVersion = SHADOW_VALIDATION_VERSION;
        }
    }

    return (bStatus);
}


// **********************************************
// Function Name:   fnGetShadowDebitRecord
//
// Operation:   Copies wDebitDataSize bytes of data starting at
//              CMOSShadowDebitLog.ShadowDebitLogTable[bID-1].bRecordStatus
//              to pbDest.
//
// Inputs:      bID is a binary number, from 1 to 10, that identifies 
//              the record that is to be retrieved. 
//
// Outputs:     *pwLen is set to the value in
//              CMOSShadowDebitLog.ShadowDebitLogTable[bID-1].wDebitDataSize.
//
// Returns:     True if the record can be retrieved. FALSE otherwise.
// **********************************************

BOOL fnGetShadowDebitRecord(unsigned char bID, char *pbDest, unsigned short *pwLen)
{   
    BOOL fbReturnStatus = FALSE;


    // make sure the requested record id is in the 0 -> max-1 range
    if (bID-1 < NUMBER_OF_DEBIT_RECORDS)
    {   
        fbReturnStatus = TRUE;
        *pwLen = CMOSShadowDebitLog.ShadowDebitLogTable[ bID - 1 ].wDebitDataSize;

        // if the size is bad, set the returned size to 1 so only
        // the record status will be returned.
        if (*pwLen > (sizeof(ShadowDebitLogEntry) - 2))
            *pwLen = 1;

        (void)memcpy(pbDest, (char *)&CMOSShadowDebitLog.ShadowDebitLogTable[ bID - 1 ].bRecordStatus, *pwLen);
    }

    return (fbReturnStatus);
}


// **********************************************
// Function Name:   fnGetShadowRefillRecord
//
// Operation:   Copies wRefillDataSize bytes of data starting at
//              CMOSShadowRefillLog.ShadowRefillLogTable[bID-1].bRecordStatus
//              to pbDest.
//
// Inputs:      bID is a binary number, from 1 to 10, that identifies 
//              the record that is to be retrieved. 
//
// Outputs:     *pwLen is set to the value in
//              CMOSShadowRefillLog.ShadowRefillLogTable[bID-1].wRefillDataSize.
//
// Returns:     True if the record can be retrieved. FALSE otherwise.
// **********************************************

BOOL fnGetShadowRefillRecord(unsigned char bID, char *pbDest, unsigned short *pwLen)
{   
    BOOL fbReturnStatus = FALSE;


    // make sure the requested record id is in the 0 -> max-1 range
    if (bID-1 < NUMBER_OF_REFILL_RECORDS)
    {   
        fbReturnStatus = TRUE;
        *pwLen = CMOSShadowRefillLog.ShadowRefillLogTable[ bID - 1 ].wRefillDataSize;

        // if the size is bad, set the returned size to 1 so only
        // the record status will be returned.
        if (*pwLen > (sizeof(ShadowRefillLogEntry) - 2))
            *pwLen = 1;

        (void)memcpy(pbDest, (char *)&CMOSShadowRefillLog.ShadowRefillLogTable[ bID - 1 ].bRecordStatus, *pwLen);
    }

    return (fbReturnStatus);
}


// **********************************************
// Function Name:   fnCMOSValidateAgainstShadowLogs
//
// Operation:   Validates the current readings against the readings in the
//              latest shadow debit log or shadow refill log, depenind on
//              which is newer
//
// Inputs:      pieceCount is the current piece count value 
//              zeroPieceCount is the current zero piece count value 
//              pARptr is a pointer to a buffer with the current AR value 
//              pDRptr is a pointer to a buffer with the current DR value 
//              pPbiSN is a pointer to a buffer with the PBI Serial Number
//
// Outputs:     None.
//
// Returns:     True if the validation succeeds. FALSE otherwise.
// **********************************************

#ifdef PHOENIX      // everybody except Mega
BOOL fnCMOSValidateAgainstShadowLogs(unsigned long pieceCount, unsigned long zeroPieceCount,
            unsigned char *pARptr, unsigned char *pDRptr, const char *pPbiSN)
{
    ShadowRefillLogEntry    *pRefillLogEntry;
    ShadowDebitLogEntry *pDebitLogEntry;
    DATETIME            sDebitDateTime, sRefillDateTime;
    double              dLogAR = 0, dLogDR = 0;
    double              dGivenAR, dGivenDR;
    unsigned long       lLogPC = 0, lLogZeroPC = 0;
    long                lPCDiff, lZeroPCDiff, lDateDiff;
    unsigned char       bFakeRecord[SHADOW_SERIAL_SIZE+1] = {0};
    unsigned char       bLen = 1;
    unsigned char       bLogIndex, bValidationVersion;
    BOOL                bStatus;


    // Convert the given values to doubles for later comparison
    dGivenAR = fnBinFive2Double(pARptr);
    dGivenDR = fnBinFive2Double(pDRptr);

    // Get the date-time stamp from the latest debit log
    bStatus = fnCMOSGetShadowDebitLogIndex(&bLogIndex);
    if (bStatus == TRUE)
    {
        pDebitLogEntry = &CMOSShadowDebitLog.ShadowDebitLogTable[bLogIndex];
        sDebitDateTime = pDebitLogEntry->sDebitDateTime;
    }

    // Get the date-time stamp from the latest refill log
    if (bStatus == TRUE)
    {
        bStatus = fnCMOSGetShadowRefillLogIndex(&bLogIndex);
        if (bStatus == TRUE)
        {
            pRefillLogEntry = &CMOSShadowRefillLog.ShadowRefillLogTable[bLogIndex];
            sRefillDateTime = pRefillLogEntry->sRefillDateTime;
        }
    }

    if (bStatus == TRUE)
    {
        // If refill log date-time stamp > debit log date-time stamp
        //   load bValidVersion, lLogPC, lLogZeroPC, dLogAR & dLogDR from the latest refill log.
        // Else,
        //   load bValidVersion, lLogPC, lLogZeroPC, dLogAR & dLogDR from the latest debit log.

        // The following call will do the following: lDateDiff = sRefillDateTime - sDebitDateTime
        (void)CalcDateTimeDifference(&sDebitDateTime, &sRefillDateTime, &lDateDiff );

        // Ignore lint warnings about pRefillLogEntry & pDebitLogEntry not being initialized
        if (lDateDiff > 0)
        {
            bValidationVersion = CMOSShadowRefillLog.bRecordVersion;
            lLogPC = pRefillLogEntry->lPieceCount;              //lint !e644
            lLogZeroPC = pRefillLogEntry->lZeroPieceCount;
            dLogAR = fnBinFive2Double(pRefillLogEntry->bAR);
            dLogDR = fnBinFive2Double(pRefillLogEntry->bDR);
        }
        else
        {
            bValidationVersion = CMOSShadowDebitLog.bRecordVersion;
            lLogPC = pDebitLogEntry->lDebitPieceCount;              //lint !e644
            lLogZeroPC = pDebitLogEntry->lZeroPieceCount;
            dLogAR = fnBinFive2Double(pDebitLogEntry->bAR);
            dLogDR = fnBinFive2Double(pDebitLogEntry->bDR);
        }

        // If bValidVersion doesn't = SHADOW_VALIDATION_VERSION,
        //   The validation can't be done because we haven't yet stored a shadow record with
        //   all the necessary data, so just return TRUE.
        if (bValidationVersion != SHADOW_VALIDATION_VERSION)
            return(TRUE);

        // Compare the log values with the given values.
        // If they're the same, do nothing.
        // Else,
        //    Try to put the adjustment in the refill log.
        lPCDiff = (long)(pieceCount - lLogPC);
        lZeroPCDiff = (long)(zeroPieceCount - lLogZeroPC);

        // ignore the lint warning about testing floats for equality
        if ((lPCDiff != 0) || (lZeroPCDiff != 0) || (dGivenAR != dLogAR) || (dGivenDR != dLogDR)) //lint !e777
        {
            bLen = (unsigned char)strlen(pPbiSN);
            if (bLen > SHADOW_SERIAL_SIZE)
                bLen = SHADOW_SERIAL_SIZE;

            (void)memcpy(bFakeRecord, pPbiSN, bLen);
            bLen++;                     // add in the nul-terminator

            // Store the latest register values in the refill log
            bStatus = fnCMOSAddNewExtendedShadowRefill(bLen, pieceCount, zeroPieceCount, pARptr,
                                                        pDRptr, bFakeRecord, SHADOW_TRANS_ADJUSTMENT);

            // If the storage worked, compare the newly written log values with the given values.
            // If any are different, return an error.
            if (bStatus == TRUE)
            {
                bStatus = fnCMOSGetShadowRefillLogIndex(&bLogIndex);
                if (bStatus == TRUE)
                {
                    pRefillLogEntry = &CMOSShadowRefillLog.ShadowRefillLogTable[bLogIndex];
                    lPCDiff = (long)(pieceCount - pRefillLogEntry->lPieceCount);
                    lZeroPCDiff = (long)(zeroPieceCount - pRefillLogEntry->lZeroPieceCount);
                    dLogAR = fnBinFive2Double(pRefillLogEntry->bAR);
                    dLogDR = fnBinFive2Double(pRefillLogEntry->bDR);

                    // ignore the lint warning about testing floats for equality
                    if ((lPCDiff != 0) || (lZeroPCDiff != 0) || (dGivenAR != dLogAR) || (dGivenDR != dLogDR)) //lint !e777
                        bStatus = FALSE;
                }
            }
        }
    }

    return (bStatus);
}
#endif


// **********************************************
// Function Name:   fnCMOSGetShadowDebitLogIndex
//
// Operation:   Gets the index of the latest shadow debit log
//
// Inputs:      None 
//
// Outputs:     *pInx is set to the proper index
//
// Returns:     True if the index can be retrieved. FALSE otherwise.
// **********************************************

BOOL fnCMOSGetShadowDebitLogIndex(unsigned char *pInx)
{
    BOOL bStatus = FALSE;
    unsigned char bLogIndex;


    bLogIndex = CMOSShadowDebitLog.LogIndex;
    if (bLogIndex < NUMBER_OF_DEBIT_RECORDS)
    {
        bStatus = TRUE;
        // if the current log index is zero, set the temp log index to the index for the
        // last record. Since we start numbering at zero, the last record is at max-1.
        // else, just decrement the temp log index.
        if (!bLogIndex)
            bLogIndex = NUMBER_OF_DEBIT_RECORDS - 1;
        else
            bLogIndex--;

        *pInx = bLogIndex;
    }

    return (bStatus);
}


// **********************************************
// Function Name:   fnCMOSGetShadowRefillLogIndex
//
// Operation:   Gets the index of the latest shadow refill log
//
// Inputs:      None 
//
// Outputs:     *pInx is set to the proper index
//
// Returns:     True if the index can be retrieved. FALSE otherwise.
// **********************************************

BOOL fnCMOSGetShadowRefillLogIndex(unsigned char *pInx)
{
    BOOL bStatus = FALSE;
    unsigned char bLogIndex;


    bLogIndex = CMOSShadowRefillLog.LogIndex;
    if (bLogIndex < NUMBER_OF_REFILL_RECORDS)
    {
        bStatus = TRUE;
        // if the current log index is zero, set the temp log index to the index for the
        // last record. Since we start numbering at zero, the last record is at max-1.
        // else, just decrement the temp log index.
        if (!bLogIndex)
            bLogIndex = NUMBER_OF_REFILL_RECORDS - 1;
        else
            bLogIndex--;

        *pInx = bLogIndex;
    }

    return (bStatus);
}


// **********************************************
// Function Name:   fnCMOSSetShadowValidationByte
//
// Operation:   Sets the validation byte in one or more shadow logs
//              to a given value
//
// Inputs:      bWhichToDo = CLEAR_DEBIT_VALIDATION, or CLEAR_REFILL_VALIDATION, or
//                          CLEAR_BOTH_VALIDATION
//              bValue = 0 or SHADOW_VALIDATION_VERSION
//
// Outputs:     None
//
// Returns:     None
// **********************************************

void fnCMOSClearShadowValidationByte(unsigned char bWhichToDo, unsigned char bValue)
{
    if (bWhichToDo & CLEAR_DEBIT_VALIDATION)
    {
        CMOSShadowDebitLog.bRecordVersion = bValue;
    }

    if (bWhichToDo & CLEAR_REFILL_VALIDATION)
    {
        CMOSShadowRefillLog.bRecordVersion = bValue;
    }
}


// **********************************************
// Function Name:   fnCMOSSetShadowRecordTypes
//
// Operation:   Sets the record types in the header of the debit log
//              & the refill log to the given values
//              If this is the first time the record types are being
//              set, the record type value in the individual records
//              will be updated to the appropriate record type.
//
// Inputs:      bIndiciumType = indicium record type value
//              bRefillType = refill record type value
//              bRefundType = refund record type value
//
// Outputs:     None
//
// Returns:     None
// **********************************************

void fnCMOSSetShadowRecordTypes(unsigned char bIndiciumType, unsigned char bRefillType,
                                unsigned char bRefundType)
{
    unsigned char   bInx;


    // if we haven't set the indicium record type before (usually due to a CMOS upgrade),
    // load the type for all the debit records that exist.
    if (CMOSShadowDebitLog.bIndicumRecordType == UNKNOWN_SHADOW_RECORD_TYPE)
    {
        for (bInx = 0; bInx < NUMBER_OF_DEBIT_RECORDS; bInx++)
        {
            // if the log entry is in use AND it's record type is unknown AND it's a debit entry,
            // set the record type for the entry
            if ((CMOSShadowDebitLog.ShadowDebitLogTable[bInx].wDebitDataSize) &&
                (CMOSShadowDebitLog.ShadowDebitLogTable[bInx].bRecordType == UNKNOWN_SHADOW_RECORD_TYPE) &&
                (CMOSShadowDebitLog.ShadowDebitLogTable[bInx].bTransType == SHADOW_TRANS_DEBIT))
            {
                CMOSShadowDebitLog.ShadowDebitLogTable[bInx].bRecordType = bIndiciumType;
            }
        }
    }

    // set the record type to use for all future entries
    CMOSShadowDebitLog.bIndicumRecordType = bIndiciumType;

    // if we haven't set the refill record type before (usually due to a CMOS upgrade),
    // load the type for all the refill records that exist.
    if (CMOSShadowRefillLog.bRefillRecordType == UNKNOWN_SHADOW_RECORD_TYPE)
    {
        for (bInx = 0; bInx < NUMBER_OF_REFILL_RECORDS; bInx++)
        {
            // if the log entry is in use AND it's record type is unknown AND it's a refill entry,
            // set the record type for the entry
            if ((CMOSShadowRefillLog.ShadowRefillLogTable[bInx].wRefillDataSize) &&
                (CMOSShadowRefillLog.ShadowRefillLogTable[bInx].bRecordType == UNKNOWN_SHADOW_RECORD_TYPE) &&
                (CMOSShadowRefillLog.ShadowRefillLogTable[bInx].bTransType == SHADOW_TRANS_REFILL))
            {
                CMOSShadowRefillLog.ShadowRefillLogTable[bInx].bRecordType = bRefillType;
            }
        }
    }

    // set the record type to use for all future entries
    CMOSShadowRefillLog.bRefillRecordType = bRefillType;

    // if we haven't set the refund record type before (usually due to a CMOS upgrade),
    // load the type for all the refund records that exist.
    if (CMOSShadowRefillLog.bRefundRecordType == UNKNOWN_SHADOW_RECORD_TYPE)
    {
        for (bInx = 0; bInx < NUMBER_OF_REFILL_RECORDS; bInx++)
        {
            // if the log entry is in use AND it's record type is unknown AND it's a refund entry,
            // set the record type for the entry
            if ((CMOSShadowRefillLog.ShadowRefillLogTable[bInx].wRefillDataSize) &&
                (CMOSShadowRefillLog.ShadowRefillLogTable[bInx].bRecordType == UNKNOWN_SHADOW_RECORD_TYPE) &&
                (CMOSShadowRefillLog.ShadowRefillLogTable[bInx].bTransType == SHADOW_TRANS_REFUND))
            {
                CMOSShadowRefillLog.ShadowRefillLogTable[bInx].bRecordType = bRefundType;
            }
        }
    }

    // set the record type to use for all future entries
    CMOSShadowRefillLog.bRefundRecordType = bRefundType;
}


// **********************************************
// Function Name:   fnCMOSSetShadowRecordIndiciumType
//
// Operation:   Sets the indicium record type in the header
//              of the debit log. This is needed initially for
//              the U.S. so the debit records can be marked as
//              being for normal debits or IBI-Lite debits.
//
// Inputs:      bIndiciumType = indicium record type value
//
// Outputs:     None
//
// Returns:     None
// **********************************************

void fnCMOSSetShadowRecordIndiciumType(unsigned char bIndiciumType)
{
    // set the record type to use for all future entries
    CMOSShadowDebitLog.bIndicumRecordType = bIndiciumType;
}

//* **********************************************************************
//
// FUNCTION NAME:   fnCMOSShadowUpgradeDebit
//
// PURPOSE:     
//      When upgrading from a version of software that has D1 Shadow debit
//      registers to D2, run this function to initialize the new members.
// INPUTS:      
//      None
// OUTPUT: 
//      None.
// NOTES:
//  1. This is called AFTER the CMOS Preservation, so existing members should contain
//      the same data.
//
//  Layout details:
//
//  D2 differs from previous layouts:
//    1. Two new ENTRY members: 
//      bRecordType - Indicates if this debit is a normal debit, an IBI-Lite 
//                    debit, a Flex-debit, etc. and should be initialized to
//                    UNKNOWN if the record is full and the Transaction type
//                    is a debit, else to initialize to EMPTY. 
//      bPad - Keeps the total size of each entry a multiple of 4 and should be 
//             initialized to 0x30 ('0').
//    2. Two new LOG members:
//      bIndicumRecordType - The current Indicium Record Type.
//      bLayoutVersionUpdated - Latest Layout Version that all the entries have
//                      been updated to.  If this routine is up to date, then this
//                      value should equal DEBIT_LOG_LAYOUT_VERSION when this
//                      routine is exited.
// HISTORY:
//  2008.10.02  Clarisa Bellamy - Initial
//------------------------------------------------------------------------
void fnCMOSShadowUpgradeDebit( void )
{
#if (DEBIT_LOG_LAYOUT_VERSION  >= 0xD2)

    ShadowDebitLogEntry *pLogEntry;
    BOOL        fOldEntryFound = FALSE;
    UINT8       bInx;

    if( CMOSShadowDebitLog.bLayoutVersionUpdated < 0xD2 )
    {
        for( bInx = 0; bInx < NUMBER_OF_DEBIT_RECORDS; bInx++ )
        {
            pLogEntry = &CMOSShadowDebitLog.ShadowDebitLogTable[ bInx ];
            if( pLogEntry->bDataLayoutVersion < 0xD2 )
            {
                pLogEntry->bPad = 0x30;
                // Check if this log entry has been loaded with data ...
                if( pLogEntry->wDebitDataSize == 0)
                {
                    // This log entry hasn't been used yet, so default to unknown record type
                    pLogEntry->bRecordType = UNKNOWN_SHADOW_RECORD_TYPE;
                }
                else
                {
                    // Load the appropriate record_type...
                    if( pLogEntry->bTransType == SHADOW_TRANS_DEBIT )
                    {
                        // If this is UNKNOWN, and the bIndicumRecordType is also
                        //  unknown when fnInitShadowLogs() runs, then this will 
                        //  get initialized to the bIPSD_indiciaType.
                        pLogEntry->bRecordType = UNKNOWN_SHADOW_RECORD_TYPE;
                    }
                    else
                    {
                        // ...must be an adjustment record
                        pLogEntry->bRecordType = EMPTY_SHADOW_RECORD_TYPE;
                    }
                    // Adjust for the new bytes (pad and record_type.)
                    // To reduce errors due to power loss at this stage, the size adjustment 
                    //  should be immediately followed by the Version update.
                    pLogEntry->wDebitDataSize += 2;
                } // End: this entry IS used.
                pLogEntry->bDataLayoutVersion = 0xD2;
                fOldEntryFound = TRUE;
            } // End: this entry has older layout version.
        } // End loop through all entries in log.
        if( fOldEntryFound == TRUE )
        {
            // This allows all the entries with UNKNOWN record types to be 
            //  set by fnInitShadowLogs().
            CMOSShadowDebitLog.bIndicumRecordType = UNKNOWN_SHADOW_RECORD_TYPE;
        }
        // Now that all entries have been updated, mark this so we don't recheck
        //  them everytime we upgrade to a new version of CMOS.
        CMOSShadowDebitLog.bLayoutVersionUpdated = 0xD2;
    } // End- bLayoutVersionUpdated < D2.

    // Put a little check in to make sure this function is up to date.
    if( CMOSShadowDebitLog.bLayoutVersionUpdated != DEBIT_LOG_LAYOUT_VERSION )
    {
        // Indicate in the system log that this function needs to be upgraded.
        sprintf( pLogScratchPad, "fnCMOSShadowUpgradeDebit: version mismatch: %x, %x",
                                 CMOSShadowDebitLog.bLayoutVersionUpdated,
                                 DEBIT_LOG_LAYOUT_VERSION );
        fnDumpStringToSystemLog( pLogScratchPad );
    }
#endif
    return;
}

//* **********************************************************************
//
// FUNCTION NAME:   fnCMOSShadowUpgradeRefill
//
// PURPOSE:     
//      When upgrading from a version of software that has F0 Shadow Refill
//      registers to F1, run this function to initialize the new members.
// INPUTS:      
//      None
// OUTPUT: 
//      None.
// NOTES:
//  1. This is called AFTER the CMOS Preservation, so existing members should contain
//      the same data.
//
//  Layout details:
//
//  F1 differs from previous layouts: 
//    1. Two new ENTRY members: 
//      bRecordType - Should be initialized to UNKNOWN.  Will be initialized later 
//              depending upon the transaction type and the values of bRefillRecordType,
//              and bRefundRecordType.
//      bPad - Keeps the total size of each entry a multiple of 4 and should be 
//             initialized to 0x30 ('0').
//    2. Four new LOG members:
//      bRefillRecordType - Should be initialized to UNKNOWN here.  It will be 
//                      re-initialized properly later.
//      bRefillRecordType - Should be initialized to UNKNOWN here.  It will be 
//                      re-initialized properly later.
//      bLayoutVersionUpdated - Latest Layout Version that all the entries have
//                      been updated to.  If this routine is up to date, then this
//                      value should equal REFILL_LOG_LAYOUT_VERSION when this
//                      routine is exited.
//      bPad[ 3 ] - To keep the ENTRY table on an even boundary.  Can be left as 0.
//  
// HISTORY:
//  2008.10.02  Clarisa Bellamy - Initial
//------------------------------------------------------------------------
void fnCMOSShadowUpgradeRefill( void )
{
#if (REFILL_LOG_LAYOUT_VERSION  >= 0xF1)

    ShadowRefillLogEntry *pLogEntry;
    UINT8       bInx;

    // Layout F1 differs from previous layouts because 2 new 
    if( CMOSShadowRefillLog.bLayoutVersionUpdated < 0xF1 )
    {
        // Note: the correct record types will be loaded after the PSD is powered up.
        CMOSShadowRefillLog.bRefillRecordType = UNKNOWN_SHADOW_RECORD_TYPE;
        CMOSShadowRefillLog.bRefundRecordType = UNKNOWN_SHADOW_RECORD_TYPE;
        
        for( bInx = 0; bInx < NUMBER_OF_REFILL_RECORDS; bInx++ )
        {
            pLogEntry = &CMOSShadowRefillLog.ShadowRefillLogTable[ bInx ];
            if( pLogEntry->bDataLayoutVersion < 0xF1 )
            {
                pLogEntry->bPad = 0x30;
                // For the refill log, whether the entry has been used or not, default to unknown record type
                pLogEntry->bRecordType = UNKNOWN_SHADOW_RECORD_TYPE;
                // Check if this log entry has been loaded with data ...
                if( pLogEntry->wRefillDataSize > 0)
                {
                    // Adjust for the new bytes: Pad, and record_type.
                    // To reduce errors due to power loss at this stage, the size adjustment 
                    //  should be immediately followed by the Version update.
                    pLogEntry->wRefillDataSize += 2;
                }
                pLogEntry->bDataLayoutVersion = 0xF1;
            } // End: this entry has older layout version.
        } // End loop through all entries in log.

        // Now that all entries have been updated, mark this so we don't recheck
        //  them everytime we upgrade to a new version of CMOS.
        CMOSShadowRefillLog.bLayoutVersionUpdated = 0xF1;
    } // End- bLayoutVersionUpdated < F1.

    // Put a little check in to make sure this function is up to date.
    if( CMOSShadowRefillLog.bLayoutVersionUpdated != REFILL_LOG_LAYOUT_VERSION )
    {
        // Indicate in the system log that this function needs to be upgraded.
        sprintf( pLogScratchPad, "fnCMOSShadowUpgradeRefill: version mismatch: %x, %x",
                                 CMOSShadowRefillLog.bLayoutVersionUpdated,
                                 REFILL_LOG_LAYOUT_VERSION );
        fnDumpStringToSystemLog( pLogScratchPad );
    }

#endif
    return;
}


bool fnGetLastLogEntry( ShadowDebitLogEntry *sDebitLogEntry)
{
	BOOL retStatus = SUCCESS;
	char bLogIndex;
	unsigned short wLen;

	retStatus = fnCMOSGetShadowDebitLogIndex(&bLogIndex);
	if (retStatus == TRUE)
	{
		retStatus = fnGetShadowDebitRecord(bLogIndex, sDebitLogEntry, &wLen);
	}

	return retStatus;
}


bool fnGetLogEntry(int *idx, ShadowDebitLogEntry *sDebitLogEntry)
{
	BOOL retStatus = SUCCESS;
	char bLogIndex = idx;
	unsigned short wLen;

	retStatus = fnGetShadowDebitRecord(bLogIndex, sDebitLogEntry, &wLen);

	return retStatus;
}

// End of Chasing_shadows.c
