/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    HAL_Power.c

   DESCRIPTION:    Hardware abstraction layer implementation for Power control interface.
                   Power controls via FPGA are in HAL_FPGA.c.

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------

*************************************************************************/
#include "commontypes.h"
#include "stdio.h"
#include "stddef.h"
#include "cJSON.h"
#include "hal.h"
#include "pbos.h"
#include "HAL_AM335x_GPIO.h"
#include "HAL_Power.h"
#include "sysdatadefines.h"
#include "blinker.h"
#include "debugTask.h"
#include "cJSON.h"
#include "am335x_pbdefs.h"
#include "nucleus_gen_cfg.h"
#include "kernel/nu_kernel.h"
#include "api_funds.h"

extern void HALInitGPIOPins(P_GPIO_INFO pGPIOTable, unsigned numPinsUsed);
extern void setGPIOPinHigh(GPIO_INFO pinInfo);
extern unsigned char readGPIOPin(GPIO_INFO pinInfo);
extern BOOL fnGetSysTaskPwrupSeqCompleteFlag( void );
extern void FATAL_EXCEP();
extern void Send_JSON(cJSON *pMsg, UINT32);
extern const char *log_names[];

// Global variables

// After a reset, a pin configured for output will output a 0.

// pins should match schematic
static GPIO_INFO CSD_GPIO_Table[] = {
		{AM335X_CTRL_PADCONF_MII1_RX_CLK, AM335X_GPIO_BIT_10, GPIO_BANK_3, DIR_OUTPUT, GPIO_PULL_OFF}, // OFF_CTRL
		{AM335X_CTRL_PADCONF_UART0_CTSN, AM335X_GPIO_BIT_8, GPIO_BANK_1, DIR_INPUT, GPIO_PULL_OFF} // MODEL_SEL
};
static const unsigned numPinsUsed = sizeof(CSD_GPIO_Table) / sizeof(GPIO_INFO);
static P_GPIO_INFO pGPIOTable = &CSD_GPIO_Table[0];




/* **********************************************************************
// DESCRIPTION: Init Power Interface.
//
// INPUTS:
// RETURNS:
// **********************************************************************/
void HALInitPowerInterface(void)
{
	//Init GPIO pins used to interface with Panel
	HALInitGPIOPins(pGPIOTable, numPinsUsed);

}


// *****************************************************************************************
// HALIsFeederPresent
//
// Detects if CSD2 (FALSE) or CSD3 (TRUE)
// *****************************************************************************************
BOOL HALIsFeederPresent(void)
{
	unsigned char readVal;

	readVal = readGPIOPin(*(pGPIOTable + POWER_IDX_MODEL_SEL));
	// CSD2 = 0; CSD3 = 1;

	return ((readVal == 0) ? FALSE : TRUE);
}

// *****************************************************************************************
//	storeJAMLeverState
//
// store the current state of the jam lever, this is used in the power down operation to
// decide if the syslog and netlog files should be written to nv storage or not.
// If the lever is in the closed state initially and changes to the open state during the
// 5 seconds the button is depressed then the files will be written.
// *****************************************************************************************
static UINT8 localShutDownJAMLeverState = FALSE;
VOID storeJAMLeverState()
{
	localShutDownJAMLeverState = fnIsDisabledStatusSet(DCOND_TRANSPORT_OPEN);
}

STATUS save_logs_to_file()
{
	const int MAX_BUFFER_SIZE = 0x17FFF;
	char *bufferPtr;
    char *path = "Logs";
    STATUS status = NU_SUCCESS;

	storeSyslogToFile(path, log_names[LOG_SYSTEM]);
	storeNetlogToFile(path, log_names[LOG_NETWORK]);

    NU_Allocate_Memory(MEM_Cached, (void **)&bufferPtr, MAX_BUFFER_SIZE, NU_NO_SUSPEND);
    if(!bufferPtr)
    {
    	status = NU_NO_MEMORY;
    }
    else
    {
		storeErrorLogToFile(path, log_names[LOG_ERROR], bufferPtr, MAX_BUFFER_SIZE);
		storeTrnFramLogToFile(path, log_names[LOG_TRM_FRAM], bufferPtr, MAX_BUFFER_SIZE);
		//storeDiagnosticsLogToFile(path, log_names[LOG_DIAGNOSTICS], bufferPtr, MAX_BUFFER_SIZE);
		//storeFramLogToFile(path, log_names[LOG_FRAM], bufferPtr, MAX_BUFFER_SIZE);
   }

	//release memory
    status = NU_Deallocate_Memory(bufferPtr);

	return status;
}

// ******************************************************************************************
// preShutdownHook
//
// store to file any data arrays we are interested in keeping. Check the initial state of the
// jam lever and the current state of the jam lever to decide if we should store the log files.
// ******************************************************************************************
VOID preShutdownHook()
{
	storeSyslogToFile(POWERDOWN_SYSLOG_DIR, log_names[LOG_SYSTEM_RESET]);

	if(!localShutDownJAMLeverState && fnIsDisabledStatusSet(DCOND_TRANSPORT_OPEN))
	{
		set_blink_sleep_save();
		//storeSyslogToFile(POWERDOWN_SYSLOG_DIR, POWERDOWN_SYSLOG_FILENAME);
		//storeNetlogToFile(POWERDOWN_SYSLOG_DIR, POWERDOWN_NETLOG_FILENAME);
		save_logs_to_file();
	}
	else
	{
		set_blink_sleep();
	}
}

