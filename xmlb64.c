/*************************************************************************
*	PROJECT:		Spark
*	AUTHOR:			Derek DeGennaro
*	MODULE NAME:	$Workfile:   xmlB64.c  $
*	REVISION:		$Revision:   1.9  $
*	
*	DESCRIPTION:	Base-64 stuff.  Incudes conversion routines and 
*					decoder/encoder filters for parser and formatter.
*					
*
* ----------------------------------------------------------------------
*               Copyright (c) 1998 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*
*	REVISION HISTORY:
*	$Log:   H:/group/SPARK/ARCHIVES/UIC/xmlB64.c_v  $
* 
*    Rev 1.9   Nov 17 2003 09:58:24   MA507HA
* Added includes for prototype checking
* 
*    Rev 1.8   Apr 21 2003 12:48:32   degends
* Updated B64Decode routine to accept whitespace
* as input.  Actually, the whitespace is ignored as far
* as the output is concerned.  Really this just makes
* big Base-64 chunks of text more readable by allowing
* them to be on different lines and be indented.
* 
*    Rev 1.7   Feb 28 2003 14:37:28   degends
* Lint patrol
* 
*    Rev 1.6   Nov 14 2001 18:13:58   degends
* Lint Patrol: Cleaned up some lint stuff.  Also took
* out all the base-64 stuff that was related to the proposed
* spark blinding method for data capture.
* 
*    Rev 1.5   May 15 2001 17:18:04   degends
* Fixed a bug in the decoding algorithm.  The '/' was
* not being recognized correctly.
* 
*    Rev 1.4   Feb 28 2001 15:52:48   degends
* Fixed the Base 64 Decode function once and for all.
* I used basically the same algorithm as the Apache
* Server so it should be ok.
* 
*    Rev 1.3   Jan 11 2001 15:01:12   degends
* Last check in of XML base-64 stuff.  The base-64
* encoding/decoding stuff will go into a utils file.  These
* files are kept here for posterity's sake.
* 
*    Rev 1.2   Nov 28 2000 11:09:12   degends
*  
* 
*    Rev 1.1   Sep 21 2000 16:58:16   degends
* Major work on the formatter.  Fixed a bug int he 
* B64 decoding algorithm.  Added more comments.
* Fixed some bits and pieces.
* 
*    Rev 1.0   Sep 13 2000 17:39:40   degends
* Initial check-in of Base-64 module.
*
*************************************************************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "xmltypes.h"

/*****************************************
	General Base-64 Stuff
******************************************/

/******************************************************
Function: UCHAR_B64_CHARS
Author: Derek DeGennaro
Description:
This function returns the number of bytes needed to 
encode into base-64 a stream of x bytes.
*******************************************************/
unsigned long UCHAR_B64_CHARS(unsigned long x)
{
	unsigned long whole = x / 3, result;
	result = whole * 4;
	if (x % 3) result += 4;
	return result;
}
/******************************************************
Function: B64_UCHAR_CHARS
Author: Derek DeGennaro
Description:
This function returns the number of bytes needed to store
the result of decoding a stream of x base-64 chars.
*******************************************************/
unsigned long B64_UCHAR_CHARS(unsigned long x)
{
	unsigned long whole = x / 4, fract = x % 4, result;
	result = whole * 3;
	result += fract;
	return result;
}

/******************************************************
m_bBin_2_B64Table
This is the encoding table for the base-64 encoding
routines.  Entry X (0 <= X <= 64) holds the value of
the base-64 char for the 6-bit value X.
*******************************************************/
static const char m_bBin_2_B64Table[64] = 
{ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
  'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
  'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 
  'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/' };

/******************************************************
m_bB64_2_BinTable
This is the decoding table for the base-64 functions.
*******************************************************/
static const unsigned char m_bB64_2_BinTable[256] =
{
	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,
	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,
	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	62,		255,	255,	255,	63,
	52,		53,		54,		55,		56,		57,		58,		59,		60,		61,		255,	255,	255,	255,	255,	255,
	255,	0,		1,		2,		3,		4,		5,		6,		7,		8,		9,		10,		11,		12,		13,		14,		
	15,		16,		17,		18,		19,		20,		21,		22,		23,		24,		25,		255,	255,	255,	255,	255,
	255,	26,		27,		28,		29,		30,		31,		32,		33,		34,		35,		36,		37,		38,		39,		40,
	41,		42,		43,		44,		45,		46,		47,		48,		49,		50,		51,		255,	255,	255,	255,	255,
	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,
	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,
	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,
	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,
	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,
	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,
	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,
	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255,	255
};
/*****************************************************
public Base-64 char matching function 
*****************************************************/
short fnIsUCB64Char(XmlChar x)
{
	if (m_bB64_2_BinTable[x] != 255 || x == '=')
		return 1;
	else
		return 0;
}

/******************************************************
Function: B64Encode
Author: Derek DeGennaro
Description:
This function encodes a stream of bytes into a stream
of base-64 chars.
*******************************************************/
ConversionResult B64Encode(unsigned char *pSource, unsigned long iSourceLen, char *pTarget, unsigned long iMaxTargetLen, unsigned long *pTargetLen, BOOL fUsePadding)
{
	unsigned long iTempLen, i, iTemp, j;
	*pTargetLen = 0;
	iTempLen = (iSourceLen / 3) * 4;
	if (iSourceLen % 3) iTempLen += 4;
	if (iTempLen > iMaxTargetLen)
		return CONVERSION_TARGET_ERR;
	*pTargetLen = iTempLen;
	i = j = 0;
	while (i < iSourceLen && j < iMaxTargetLen)
	{
		pTarget[j] = (pSource[i] >> 2) & 0x3F;
		pTarget[++j] = (pSource[i++] & 0x03) << 4;
		if (i < iSourceLen)
		{
			pTarget[j] += (pSource[i] >> 4) & 0x0F;
			pTarget[++j] = (pSource[i++] & 0x0F) << 2;
			if (i < iSourceLen)
			{
				pTarget[j++] += (pSource[i] >> 6) & 0x3;
				pTarget[j++] = (pSource[i++] & 0x3F);
			}
		}		
	}
	for (i = 0; i < iTempLen; i++)
	{
		iTemp = pTarget[i];
		pTarget[i] = m_bBin_2_B64Table[iTemp];
	}

	if (fUsePadding == TRUE)
	{
		while (++j < iTempLen)
			pTarget[j] = '=';
	}

	return CONVERSION_OK;
}

/******************************************************
Function: B64DecodeSequence
Author: Derek DeGennaro
Description:
This function decodes a stream that contains from 
2 to 4 base-64 chars into a stream of bytes.  
Only base-64 characters are allowed.
A single output byte requires 2 Base-64 characters
to define it fully.  Two output bytes require
3 base-64 characters to define them fully.
Three output bytes require 4 base-64 bytes to
define them fully.  Thus the 2-4 byte input length
restriction.
*******************************************************/
static BOOL B64DecodeSequence(U_CHAR *pSequence, unsigned long iSequenceLen, U_CHAR *pDest, unsigned long *pDestLen)
{	
	/* less than 2 input Base64 characters -> error */
	/* 2 input Base64 characters -> 2 output Bytes */
	/* 3 input Base64 characters -> 3 output Bytes */
	/* 4 input Base64 characters -> 3 output Bytes */
	/* more than 4 input Base64 characters -> error */
	
	if (pDestLen != NULL)
		*pDestLen = 0;															/* output count = 0 */
	
	if (iSequenceLen < 2) {
		return FALSE;
	}
		
	*pDest = (unsigned char)(m_bB64_2_BinTable[*pSequence] << 2);				/* output byte 1 (hi nibble) */
	*pDest++ |= (unsigned char)(m_bB64_2_BinTable[*(pSequence + 1)] >> 4);		/* output byte 1 (lo nibble) */	
	if (pDestLen != NULL)
		*pDestLen += 1;															/* output count = 1 */
		
	if (iSequenceLen > 2) {		
		*pDest = (unsigned char)(m_bB64_2_BinTable[*(pSequence + 1)] << 4);		/* output byte 2 (hi nibble) */
		*pDest++ |= (unsigned char)(m_bB64_2_BinTable[*(pSequence + 2)] >> 2);	/* output byte 2 (lo nibble) */
		if (pDestLen != NULL)
			*pDestLen += 1;														/* output count = 2 */		
	}
	if (iSequenceLen > 3) {
		*pDest = (unsigned char)(m_bB64_2_BinTable[*(pSequence + 2)] << 6);		/* output byte 3 (hi nibble) */		
		*pDest++ |= (unsigned char)(m_bB64_2_BinTable[*(pSequence + 3)]);		/* output byte 3 (lo nibble) */
		if (pDestLen != NULL)
			*pDestLen += 1;														/* output count = 3 */
	}	
	if (iSequenceLen > 4)
		return FALSE;
	else
		return TRUE;		
}

/******************************************************
Function: B64Decode
Author: Derek DeGennaro
Description:
This function decodes a stream of base-64 chars into
a stream of bytes.  Whitespace is filtered out of
the input stream (this is usefull for formatting
of base-64 in text enviroments).
*******************************************************/
ConversionResult B64Decode(char *pSource, unsigned long iSourceLen, U_CHAR *pTarget, unsigned long iMaxTargetLen, unsigned long *pResultLen)
{
	U_CHAR bBase64Buff[3], b64Char;
	unsigned long iBase64BuffLen = 0, iBase64DecodeLen = 0;
	char *pB64Src = pSource;
	unsigned long iB64SrcLen = iSourceLen;
	U_CHAR *pBinDest = pTarget;
	unsigned long iBinDestLen = iMaxTargetLen, iOutputLen = 0;
	BOOL fKeepGoing = TRUE;
	
	memset((void*)bBase64Buff,0,sizeof(bBase64Buff));
	
	while (iB64SrcLen > 0 && fKeepGoing == TRUE)
	{
		b64Char = *pB64Src;
		if (b64Char == '=')
		{
			/* The end of the input has been reached. */
			fKeepGoing = FALSE;
		}
		else if (fnIsUCB64Char((XmlChar)b64Char))
		{
			/* Add the next Base64 character to the buffer. */
			bBase64Buff[iBase64BuffLen++] = b64Char;		
			
			if (iBase64BuffLen == 4)
			{
				/* A complete sequence of 4 Base64 characters has
				been buffered. */				
				if (iBinDestLen >= 3)
				{
					/* Output a sequence of 3 bytes. */
					if (B64DecodeSequence(bBase64Buff,iBase64BuffLen,pBinDest,&iBase64DecodeLen) == TRUE)
					{											
						pBinDest += iBase64DecodeLen;
						iBinDestLen -= iBase64DecodeLen;
						iOutputLen += iBase64DecodeLen;
					}					
					else
					{
						/* There was an error. */
						return CONVERSION_SOURCE_ERR;
					}
				}
				else
				{
					/* There is no more room in the output buffer. */
					return CONVERSION_TARGET_ERR;
				}				
				
				/* Reset the Base64 buffer. */
				iBase64BuffLen = 0;					
				memset((void*)bBase64Buff,0,sizeof(bBase64Buff));
			}			
		}
		else if (!isspace(*pB64Src))
		{
			/* Only Base64 characters or whitespace are allowed.
			Everything else is an error. */
			return CONVERSION_SOURCE_ERR;
		}
		
		if (fKeepGoing == TRUE)
		{
			/* Get the next character. */
			iB64SrcLen--;
			pB64Src++;		
		}
	}
	
	/* At this point there are no more input characters.
	Output anything remaining in the Base64 buffer. */
	if (iBase64BuffLen > 0)
	{
		if (B64_UCHAR_CHARS(iBase64BuffLen) <= iBinDestLen)
		{
			if (B64DecodeSequence(bBase64Buff,iBase64BuffLen,pBinDest,&iBase64DecodeLen) == TRUE)
			{											
				pBinDest += iBase64DecodeLen;
				iBinDestLen -= iBase64DecodeLen;
				iOutputLen += iBase64DecodeLen;
			}					
			else
			{
				/* There was an error. */
				return CONVERSION_SOURCE_ERR;
			}
		}
		else
		{
			/* There is not enough room in the output buffer. */
			return CONVERSION_TARGET_ERR;
		}
	}	
	
	if (pResultLen != NULL)
		*pResultLen = iOutputLen;
	
	return CONVERSION_OK;
}

int fnCompareXMLStr(const XmlChar *x1, const XmlChar *x2)
{
	int ret = 0 ;

	ret = *x1 - *x2;
    while( ret == 0 && *x2 != 0)
	{
		++x1; ++x2;
		ret = *x1 - *x2;
	}

    if ( ret < 0 )
		ret = -1 ;
    else if ( ret > 0 )
		ret = 1 ;

    return( ret );
}
