/************************************************************************
*   PROJECT:        Horizon CSD 2-3
*   MODULE NAME:    version.c
*
*   DESCRIPTION:    Version number functions
*
*************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "version.h"
#include "apphdr.h"

/* UIC software version number */

//const unsigned short  wUICSoftwareVersion = UIC_SOFTWARE_VERSION;     /* e.g. 0x0251 would be version 2.51 */
const char              UicVersionString[APPVERSIONLEN] __attribute__ ((section ("version"))) = UIC_VERSION_STRING;
const char              ProtVersionString[APPVERSIONLEN] __attribute__ ((section ("version"))) = UIC_VERSION_STRING;


/* *************************************************************************
// FUNCTION NAME:       fnGetPrettyUicVersionString
// DESCRIPTION: 
//      Prints the UicVersionString into the caller's buffer..
// INPUTS:
//      dest -Ptr to buffer that must be at least 12 bytes big, to hold the NULL_TERMINATOR.
// OUTPUTS: 
//       Returns the length in bytes of the string (not including the NULL_TERMINATOR.)
// NOTES: 
//  1. UicVersionString is set to UIC_VERSION_STRING, which is defined in version.h
//  2. The destination is not checked for length so the caller MUST have enough room
//  3. The input strings are not checked for length, so the programmer must not make 
//     the strings any longer.
// HISTORY:
//  2011.05.17 Clarisa Bellamy - Added function header.  Appends test letter 
//                      to string.
// --------------------------------------------------------------------------*/
uchar fnGetPrettyUicVersionString(char* dest)
{
    sprintf(dest, "%s", UicVersionString );

    return( (uchar)strlen(dest) );
}

UINT8 fnGetLimitedUicVersionString( char* pDest, UINT8 ubMaxLen )
{
    UINT8  ubStrLen;

    memset( pDest, 0, ubMaxLen +1 );
    
    if( ubMaxLen > strlen( UicVersionString ) )
    {
        ubStrLen = fnGetPrettyUicVersionString( pDest );
    }
    else 
    {
        memcpy( pDest, UicVersionString, ubMaxLen );    
        ubStrLen = ubMaxLen;
    }
    
    return( ubStrLen );
}


