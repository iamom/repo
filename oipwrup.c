/************************************************************************
    PROJECT:        Future Phoenix
    COPYRIGHT:      2005, Pitney Bowes, Inc.
    AUTHOR:         
    MODULE NMAE:    OiPwrup.c

    DESCRIPTION:    This file contains all types of screen interface functions 
                    (hard key, event, pre, post, field init and field refresh)
                    and global variables pertaining to power up.

 ----------------------------------------------------------------------
 MODIFICATION HISTORY:  
 
 22-May-17 Jennifer Hao on FPHX 02.12 cienet branch
      Commented out function fnGetPendingMainScr because it isn't used by G9.	

   	
 04-May-14 sa002pe on FPHX 02.12 shelton branch
 	Changed fnDoStuffsAfterOOBSetup to split the syslog message for the PCMC/LAN
	wait into 2 messages like Janus now has so we can see how long it takes
	to detect the connection and how long it takes for the setup.
  
 02-Mar-13 sa002pe on FPHX 02.10 shelton branch
 	For Fraca 220572: Changed fnDoStuffsAfterOOBSetup to not do the delays for PCMC,
	the LAN and the platform if coming from OOB because the start of OOB already did a delay.

 20-Feb-13 sa002pe on FPHX 02.10 integration branch
 	Changed fnDoStuffsAfterOOBSetup to fix the syslog message for the PCMC/LAN set up time.

 18-Feb-13 sa002pe on FPHX 02.10 integration branch
 	For Fraca 219325: Changed fnDoStuffsAfterOOBSetup to give PCMC & the LAN time to get set up.

    2012.09.25      Bob Li          Updated fnDoStuffsAfterOOBSetup() to make sure the normal preset with platform
									could be reloaded successfully. 
    2009.10.28		Michael Lepore	In fnevPowerUpComplete() removed fnIsNewPlatConnected(). Platform self enables. 
    2009.08.20      Deborah Kohl    Merge fix for fraca GMSE00152669, event 51 issue
                                    from french version
                                    Modified function fnevPowerUpComplete()
    2008.09.30      Clarisa Bellamy Merge change from Janus to initialize new CMOS 
                                    variable, CMOSLastPrintedMailDate, to the current 
                                    date, time = midnight, if it is currently zeros.
    07/18/2008      Raymond Shen    Modified fnevPowerUpComplete() and removed fPSDBatteryLowPending,
                                    fnSetPSDBatteryLowPending(), fnIsPSDBatteryLowPending() for
                                    battery life checking. 
    02/15/2008      Michael Lepore  Moved Checksum checks from systask.c. If systask reported metererror then we never 
                                    get powerupcomplete! Opps. Effects Accounting and who know's what else!
    10/18/2007      Oscar Wang      Modified function fnevPowerUpComplete() to fix FRACA GMSE00131327: 
                                    Upload should prompt prior to preset loading.
    08/28/2007      Andy Mo         Undo the changes made on 08/22/2007
    08/22/2007      Andy Mo         Added call to fnSetPieceIDduckFlag() in fnevPowerUpComplete to duck piece ID.
                                    Fixed Fraca127832
    07/19/2007      Vincent Yi      Fixed lint errors
    05/03/2007      Michael Lepore  Put checks around InitializeCFFS(). Only continue if returns TRUE
    04/17/2007      Joey Cui        Modify fnevPowerUpComplete, no need to enter screen  
                                    AbacusErrAcountingDiscrepancy2 
    04/02/2007      Oscar Wang      Set a flag for reloading normal preset if it is 
                                    changed by user during power up or wake up.
    03/28/2007      Adam Liu        Added Abacus PWF checking in fnevPowerUpComplete()
    02/12/2007      Kan Jiang       Modify fnDoStuffsAfterOOBSetup for Fraca 114529
    02/09/2007      Kan Jiang       Modify fnDoStuffsAfterOOBSetup for Fraca 114529
    07/18/2006      Dicky Sun       Modify fnevPowerUpComplete for GTS pending
                                    record check.
    06/22/2006      Raymond Shen    Modefied OOB related code and added
                                    function fnDoStuffsAfterOOBSetup().
    12/27/2005      Vincent Yi      Initial version

    OLD PVCS REVISION HISTORY
 * \main\jnus11.00_i1_cienet\2  ming.huang  10/17/2005 4:38PM
 * Clean up fnevPowerUpComplete. Added event function fnevIsServiceMode for 
 * ErrMissingImage and ErrMeterError screens. If user press '.' and '0' during
* power up and get system fault, it will go to service mode password , too.

*************************************************************************/


/**********************************************************************
        Include Header Files
**********************************************************************/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "aiservic.h"
#include "bob.h"
#include "bobutils.h"
#include "bwrapper.h"
//#include "cmos.h"
#include "clock.h"
#include "cwrapper.h"
#include "datdict.h"
//#include "deptaccount.h"    // fnPwrupInitAccounting()
#include "errcode.h"
#include "fwrapper.h"
#include "keypad.h"
#include "mmcitask.h"
#include "oiclock.h"
#include "oidongle.h"
#include "oifields.h"
//#include "oigts.h"
//#include "oioob.h"
#include "oipostag.h"
#include "oipwrup.h"
//#include "oipreset.h"
#include "oiscreen.h"
#include "oiscrnfp.h"
#include "oit.h"
//#include "oiweight.h"
#include "nucleus.h"
#include "pbos.h"
#include "platform.h"
#include "sys.h"
#include "XBData.h"     // Accounting Transaction Builder defines -ebc
#include "intellilink.h"
//#include "abacusFileUpdate.h"
//#include "abacusGDM.h"
//#include "abacustypes.h"
//#include "ratesvcpublic.h"


// ----------------------------------------------------------------------
// Protypes for External functions that are not yet in an include file:
// ----------------------------------------------------------------------

// From fpffscrdio.c:
extern char InitializeCFFS( void );


// ----------------------------------------------------------------------
//              Declaration of External variables:
// ----------------------------------------------------------------------

// From custdat.c:
extern DATETIME CMOSLastPrintedMailDate;
// From dcaputilcomet.c
extern unsigned short  usDcapWriteStatus;  



// ----------------------------------------------------------------------
//              Declaration of Global variables:
// ----------------------------------------------------------------------

// ----------------------------------------------------------------------
//             Local definitions:
// ----------------------------------------------------------------------

// ----------------------------------------------------------------------
//              Declaration of Local variables:
// ----------------------------------------------------------------------


static BOOL     fKey0Down = FALSE;
static BOOL     fKeyDotDown = FALSE;
static UINT16   usPendingMainScr = 0;



// ----------------------------------------------------------------------
//              Protypes for Local functions :
// ----------------------------------------------------------------------





/**********************************************************************
        Public Functions
**********************************************************************/
/* Utility functions */

BOOL fnIsServiceMode()
{
    return ( fKey0Down && fKeyDotDown );
}

void fnResetServiceMode()
{
    fKey0Down = FALSE;
    fKeyDotDown = FALSE;
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnDoStuffsAfterOOBSetup
//
// DESCRIPTION:
//         Utility function that do stuffs that must be done after OOB setup.
//
// INPUTS:
//         Standard hard key function inputs (format 2)
//         + pNextScr(pointer of return screen ID),
//         except bKeyCode = 0 if from powerup, 0xFF if from OOB.
//
// OUTPUTS:
//         None 
//
// RETURNS:
//         None
//
// WARNINGS/NOTES: 
//
// MODIFICATION HISTORY:
//  19-Dec-12   S. Peterson     Make sure PCMC & the LAN have enough time to establish a connection.
//  09/25/2012  Bob Li          Make sure the normal preset with platform can be reloaded successfully.
//  04/02/2007  Oscar Wang      Set a flag for reloading normal preset if it is 
//                              changed by user during power up or wake up.
//  02/12/2007  Kan Jiang          Add the invoking of fnPowerUpRestoreNormalPreset
//  02/09/2006  Kan Jiang          Modified for Fraca GMSE00114529
//  06/22/2006  Raymond Shen        Initial version
// *************************************************************************/
void fnDoStuffsAfterOOBSetup(UINT8          bKeyCode, 
                                UINT8           bNumNext, 
                                T_SCREEN_ID     *   pNextScreens,
                                T_SCREEN_ID     *   pNextScr)
{
    unsigned long	ulTime = 0, ulTime2 = 0;
    UINT8        bWhichLAN;
	char		pDiagLogBuf[45];
	
	
    // If the last printed mail date hasn't been loaded, load the system 
    //  date as a starting point.
    if (CMOSLastPrintedMailDate.bCentury == 0)  
    {
        (void)GetSysDateTime(&CMOSLastPrintedMailDate, ADDOFFSETS, GREGORIAN);
        CMOSLastPrintedMailDate.bHour = 0;
        CMOSLastPrintedMailDate.bMinutes = 0;
        CMOSLastPrintedMailDate.bSeconds = 0;
    }

    // Initialize Normal preset if required, Note: rate manager must 
    // be initialized first

//RAM added
//    // Initialize the Normal preset if it's not initialized.
//    if( fnCheckNormalPresetInitialized() == FALSE )
//    {
//        fnInitializeNormalPreset();
//    }
//
//    fnUpdateNewRatesNormalPresetFlag();
//END
    ulTime = 0;

//RAM added
    bKeyCode = 0xFF;
    ulTime = 60;
//END
	// if not coming from OOB, then do the waits for PCMC, the LAN and the platform
	if (bKeyCode != 0xFF)
	{
		// wait before starting to give PCMC time to establish a connection w/ the meter, or
		// time for the LAN to enumerate & get an IP address.
		// no more pc_daemon
#if 0
	    while(( FALSE == isProxyReachable() ) && ( connectedToInternetViaLan(&bWhichLAN) == FALSE ) && (ulTime < 30))
	    {
	        OSWakeAfter(1000);
	        ulTime++;
	    }

	    if(ulTime < 30)
	    {
			//if PCMC, we need wait for PC_DEAMON task to be ready
			//if PC_DEAMON is not ready, it will cause Sign on failure and display DE00 error.
			if(isProxyReachable() == TRUE)
			{
				OSWakeAfter(10000);
	            ulTime2 = 100;
			}
			else //it must be LAN connection
			{
	            ulTime2 = 0;
			    if ( LAN_ADAPTER_USB == bWhichLAN )
			    {
	                // wait for up to 45 seconds for getting the IP for usb LAN adapter.
/*
			        while ((GetInitializedEtherCount() == 0) && (ulTime2 < 450))
			        {
			            OSWakeAfter(100);
			            ulTime2 ++;
			        }
*/
			    }
			    else
			    {
				    BOOL fLANConfigComplete = FALSE;

/*
	                // wait for up to 45 seconds for getting the IP for integrated LAN.
			        while (((fnLANGetIntegratedLANLinkStatus(&fLANConfigComplete) == FALSE) || 
			                (FALSE == fLANConfigComplete)) &&
						   (ulTime2 < 450))
			        {
			            OSWakeAfter(100);
			            ulTime2 ++;
			        }
*/
			    }          
			}
		}


		(void)sprintf(pDiagLogBuf, "PCMC/LAN Detect Time = %lu ms", (ulTime * 1000));
		fnSystemLogEntry(SYSLOG_TEXT, pDiagLogBuf, strlen(pDiagLogBuf));

		(void)sprintf(pDiagLogBuf, "PCMC/LAN Setup Time = %lu ms", (ulTime2 * 100));
		fnSystemLogEntry(SYSLOG_TEXT, pDiagLogBuf, strlen(pDiagLogBuf));
#endif

//RAM added
//	    ulTime = 0;
		bNumNext = 0;

//END
	    //wait up to 5 additional seconds for the platform to connect
	    while (ulTime < 50)
	    {
	        ulTime++;
	        OSWakeAfter(100);
	        if (fnPlatIsAttached() == TRUE)
	        {
	            break;
	        }
	    }
	
		ulTime *= 100;
		(void)sprintf(pDiagLogBuf, "Platform Time = %lu ms", ulTime);
		fnSystemLogEntry(SYSLOG_TEXT, pDiagLogBuf, strlen(pDiagLogBuf));
	}
	
    // set system to the normal preset  
    if (bNumNext > 0)
    {
        *pNextScr = GetScreen(SCREEN_MM_INDICIA_ERROR); //pNextScreens[0];
//RAM added
//        fnPowerUpRestoreNormalPreset();
//        fnSetRestorePresetFlag();
//END
    }
    else
    {
        *pNextScr = GetScreen(SCREEN_MM_INDICIA_ERROR);
    }
}

/* Pre/post functions */



/* Field functions */



/* Hard key functions */



/* Event functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fnevCometPowerUpComplete
//
// DESCRIPTION: 
//      Handles housekeeping necessary after powerup for Comet.
//
// INPUTS:
//      Standard event function inputs (format 2).
//
// RETURNS:
//      Next screen: if initialization failed, go to next screen 2
//                   else,  go to next screen 1
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/18/2008  Joe Qu          Modified function fnevPowerUpComplete() to fix fraca 
//                              GMSE00152669, event 51 issue.
//  07/18/2008  Raymond Shen    Changed checking of PSD battery low to checking
//                              of PSD/CMOS battery status.
//  10/18/2007  Oscar Wang      Fix FRACA GMSE00131327: Upload should prompt 
//                              prior to preset loading.
//  03/28/2007  Adam Liu        Added Abacus PWF checking
//  08/04/2006  Vincent Yi      Added new platform checking before activate it
//  10/28/2005  Vincent Yi      Code cleanup
//              Joe Mozdzer     Initial version
//
// *************************************************************************/
// VBL_TBD_FP
T_SCREEN_ID fnevPowerUpComplete(UINT8           bNumNext, 
                                T_SCREEN_ID *   pNextScreens, 
                                INTERTASK   *   pIntertask)
{
    T_SCREEN_ID     usNext = 0;
    UINT32          ulEvents;
    UINT32  lwMinSettablePostage = 0;
//    SINT32  lwAbacusFileStatus = ABFU_OK;
    BOOL fAbFileUpdateReq = FALSE;
//    AGD_TEMP_INFO *pAbacusPwrUpInfo = NULL_PTR;

//    UINT16 sAbacusPwrUpErr = ABACUS_SUCCESS;

    BOOL    fCheckSumTest;
    ulong   lwRmActualCheckSum;
    ulong   lwDCEActualCheckSum;

    extern ulong lwAppActualCheckSum;
    
    SetupParams *   pCMOSSetupParams = fnCMOSSetupGetCMOSSetupParams();
    Diagnostics *   pCMOSDiagnostics = fnCMOSSetupGetCMOSDiagnostics();

    usNext = pNextScreens[0];   // default to Home Ready screen

    // Moved from Systas.c - fnpwFinishPowerup()
    // verify application checksum. The reason to use OIT error class is that the same
    // check will be done more frequently in OI to display checksums in system information
    // screen
    fCheckSumTest = fnFlashVerifyIndividualCheckSum(CHECKSUM_APP, &lwAppActualCheckSum);
    if (!fCheckSumTest)
        fnReportMeterError(ERROR_CLASS_OIT, ECOIT_APP_CHECKSUM);

//    if (!fCheckSumTest)
//        fnReportMeterError(ERROR_CLASS_OIT, ECOIT_DCE_CHECKSUM);
//Fix me RAM
//    fCheckSumTest = fnFlashVerifySumsIndexCheckSums();
//    if (!fCheckSumTest)
//        fnReportMeterError(ERROR_CLASS_SYS, ECSYS_FLASH_CS_BAD);
//END
    // (ML) - Fix Normal Preset with Plat Mode
// (ML) 10/28/09 - not really needed
//    if (fnIsNewPlatConnected() == FALSE)
//    {
//        fnActivePlatform();
//    }
//    else
//    {
//        fnSetNewPlatConnected (FALSE);
//   }

    OSWakeAfter(500); // Give the Plat a chance to respond

    /* if for some reason the sleep timeout gets set to zero (should not happen 
        unless there is a flash parameter or cmos update issue), set it to the 
        proper factory default */
    if (pCMOSSetupParams->wSleepTimeout == 0)
    {
        pCMOSSetupParams->wSleepTimeout = 
                                fnGetFactorySleepTimeout();
    }

    /* enable sleep if there is a non-zero number in the sleep timer.  the only 
        way there would not be at this point is if the flash factory value is 
        zero.  note that we must be attached to the base to activate sleep mode 
        (this prevents us from having to work around the systask messaging to 
        the pmci and is not a requirement).  */
    fnEnableSleepTimeout(); 
    
    /* enable Normal Preset timeout if there is a non-zero number in the normal
        preset timer.  the only way there would not be at this point is if the 
        flash factory value is zero.  note that we must be attached to the base 
        to activate normal preset mode.  */
    fnEnableNormalPresetTimeout();
    
    // schedule the daylight savings time auto-change event if applicable JMMDST
    //fnScheduleDSTAutoChangeEvent(); -- Ram removed not to check DST Auto Daylight savings.

/*Vincent
    // Initialize the Rate Manager and set the proper weight unit to the base
    //  if WOW is installed.
    fnRMAIInitRatePlus();
*/
       // fnRMAIInitRate(NULL);
//       (void)fnRateInitRates();
    // initialize the appropriate accounting system
    // Note: rate manager must be initialized first
//    fnInitDeptAccountAndSupervisorPointers();
    
    //AdamTODO: -Porting the abacus power recovery code from mega1600
    //Uncomment this part after Abacus PWF is ready, 
    // Comment now for Abacus testing 
//#ifdef 0
    /*      
    Determine if the abacus files need to be updated.
    If they do, then branch off to the update screen.
    The stuff below the brance will be continued in the
    fnevAbacusCompletePowerup function.
    */
//    if (InitializeCFFS())
//    {
/*
        lwAbacusFileStatus = CFFSPowerFailRecovery( pCFFS );
        if(lwAbacusFileStatus == CFFSPWRRECOVERY_WRONGDEV)
        {
            sAbacusPwrUpErr = (ERROR_CLASS_ABACUS<<8) + ABACUS_POWER_RECOVERY_REQ_MATE_NOT_ATTACHED;
        }
        else if(lwAbacusFileStatus != CFFSOK)
        {
            sAbacusPwrUpErr = (ERROR_CLASS_ABACUS<<8) + ABACUS_DEVICE_ERROR;
        }
        else
        {
            lwAbacusFileStatus = fnABFU_IsFileUpdateRequired(&fAbFileUpdateReq);
            sAbacusPwrUpErr = fnABFU_ConvertErrorToPowerUpStatus(lwAbacusFileStatus);
        }

        if (lwAbacusFileStatus == ABFU_OK)
        {
             The file update status was successfully
            retrieved.
            if (fAbFileUpdateReq == TRUE)
            {
                if (bNumNext > 3)
                {
                     An update needs to occur, goto the udpate screen.
                    usNext = pNextScreens[3];
                    goto xit;
                }
                else
                {
                     A screen is missing.
                    fnReportMeterError(ERROR_CLASS_ABACUS,(unsigned char)sAbacusPwrUpErr);
                    goto xit;
                }
            }
        }
        else
        {
            if (lwAbacusFileStatus != ABFU_NO_CFFS)
            {
                 There was an error checking the abacus
                media for the file update required
                condition.
                fnAGDSetAbacusPowerUpStatusCode(sAbacusPwrUpErr);
                 by Joey Cui,   AbacusErrAcountingDiscrepancy2 screen is not needed any more
                if (bNumNext > 4)
                {
                usNext = pNextScreens[4];           
                goto xit;
                }
                else
                {

                 A screen is missing.
                fnReportMeterError(ERROR_CLASS_ABACUS,(unsigned char)sAbacusPwrUpErr);
                goto xit;
                //          }
            }
        }
        //#endif
*/
//    }

    // Platform may self-activated
//  (ML) moved to top of this fct..fix Normal preset issue.
//  if (fnIsNewPlatConnected() == FALSE)
//  {
//      fnActivePlatform();
//  }
//  else
//  {
//      fnSetNewPlatConnected (FALSE);
//  }

    // Call main Accounting Initialization routine
//Fix me RAM - Enable when Account is available
//    fnPwrupInitAccounting();
//END
    fnInitKIPPasswordPointer();

    // check PSD/CMOS battery status and and store it in an OIT variable during power up.
    fnOITSetBatteryStatus(fnBobCheckBatteryStatus());

    if (fnSYSIsPSDDetected() == TRUE)
    {

//RAM added TODO need to remove when CMOS file system is complete
    	pCMOSDiagnostics->fOOBComplete = TRUE;
//End
        if (pCMOSDiagnostics->fOOBComplete == FALSE)
        {
            // As OOB setup has not completed, do OOB.
//            usNext = fnStartOOB();
        }
        else 
        {
            // Do stuffs that must be done after OOB setup.
            fnDoStuffsAfterOOBSetup(0, bNumNext, pNextScreens, &usNext);
        }
    }


/*Vincent
    if ( fnIsServiceMode() )
    {
        usPendingMainScr = usNext; // keep the screen pending
        usNext = pNextScreens[2]; // Service Mode Password screen
    }
*/

xit:
    return (usNext);
}


/* Variable graphic functions */



/**********************************************************************
        Private Functions
**********************************************************************/


