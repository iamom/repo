/*************************************************************************
*	PROJECT:		Spark
*	AUTHOR:			Derek DeGennaro
*	MODULE NAME:	$Workfile:   xmlform.c  $
*	REVISION:		$Revision:   1.15  $
*	
*	DESCRIPTION:	Implementation of the XML Formatter.
*					
*
* ----------------------------------------------------------------------
*               Copyright (c) 1998 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*
*	REVISION HISTORY:
*	$Log:   H:/group/SPARK/ARCHIVES/UIC/XMLFORM.C_V  $
* 
*    Rev 1.15   Nov 30 2004 16:13:46   CH510RE
* Changed all formatter unsigned char ucCounter 
* references unsigned long ulCounter. Counter 
* was rolling over on uploads greater than 255
* buckets... 
* 
*    Rev 1.14   02 Sep 2004 14:38:06   HO501SU
* Corrected compiling warning.
* 
*    Rev 1.13   Nov 17 2003 09:53:30   MA507HA
* Warning Clean Up
* 
*    Rev 1.12   Feb 28 2003 14:45:24   degends
* Lint patrol
* 
*    Rev 1.11   Feb 27 2003 13:19:22   degends
* Removed some unused parameters to the XMLFormat function
* 
*    Rev 1.10   Feb 27 2003 10:03:14   degends
* Modified the XML Formatter module implementation
* to support relocating the scripts exclusively in FLASH.
* Currently, they are located both in FLASH and RAM since
* the counter is stored in the scripts and is modified during
* formatting.  To fix this the counter is now passed around
* on the stack within the formatter module.  From now on
* all scripts should be declared as "const" so that they are
* located entirely in FLASH.
* 
*    Rev 1.9   Nov 15 2002 15:28:02   degends
* Fixed a bug in the fnFormatterSendAttributes
* function that would return an error when a valid
* attribute was specified whose name was more 
* than one char.  The state machine for this function
* needed to be modified.
* 
*    Rev 1.8   Nov 05 2002 13:37:04   degends
* Added functionality to formatter so that markup characters
* are replaced with their escape sequences in the output stream.
* 
*    Rev 1.7   Sep 30 2002 13:15:32   degends
* Added new functionality for Abacus.
* The formatter can now output variable length lists.
* Also, the formatter can be set to not output empty 
* elements or elements that have no data.
* 
*    Rev 1.6   Nov 14 2001 18:14:30   degends
* Lint Patrol: Cleaned up the includes, unused variables, and 
* other stuff.
* 
*    Rev 1.5   Feb 01 2001 15:50:02   degends
* Fixed fnHandleXMLScript so that an error code
* returned by the handler is returned to the user.
* 
*    Rev 1.4   Jan 11 2001 15:11:26   degends
* Cleaned up interface to formatter as much as I could.
* 
*    Rev 1.3   Nov 28 2000 11:09:34   degends
*  
* 
*    Rev 1.2   Oct 06 2000 17:48:08   monrogt
* DCAP integration things
* 
*    Rev 1.1   Sep 21 2000 16:58:18   degends
* Major work on the formatter.  Fixed a bug int he 
* B64 decoding algorithm.  Added more comments.
* Fixed some bits and pieces.
* 
*    Rev 1.0   Sep 15 2000 17:45:22   degends
* Initial check-in for the XML Formatter.
***************************************************************************/

#include <stdio.h>
#include "xmlform.h"

/*******************************
	definition of formatter
*******************************/
typedef struct tagXMLFormatter {
	short (*fnSendChar)(XmlChar);	/* called when there is a char ready */
	short (*fnStartFormatter)();	/* called before any chars are sent */
	short (*fnEndFormatter)(BOOL);	/* called after last char is sent */
	BOOL fBlinding;						/* is blinding Active */
} XMLFormatter;

/******************************
	Forward declarations
	of private functions.
******************************/
static short fnHandleXMLScript(XMLFormatter *p, XMLFormatScript *pScript, unsigned long ulCounter);
static short fnHandleXMLScriptEntry(XMLFormatter *p, XMLScriptEntry entry, unsigned long ulCounter);
static short fnHandleCharDataCmd(XMLFormatter *p, const XMLScriptEntry entry, unsigned long ulCounter);
static short fnHandleSubscriptEntry(XMLFormatter *p, const XMLScriptEntry entry, unsigned long ulCounter);
static short fnHandleRetrieveAndTag(XMLFormatter *p, const XMLScriptEntry entry, unsigned long ulCounter);
static short fnHandleCommentCmd(XMLFormatter *p, const XMLScriptEntry entry, unsigned long ulCounter);
static short fnHandleCDSectCmd(XMLFormatter *p, const XMLScriptEntry entry, unsigned long ulCounter);
static short fnFormatterSendString(XMLFormatter *p, XmlChar *pString);
static short fnFormatterSendSTag(XMLFormatter *p, XmlChar *pTag, XmlChar *pAttr, short wAttrLen);
static short fnFormatterSendETag(XMLFormatter *p, XmlChar *pTag);
static short fnFormatterSendEmptyElemTag(XMLFormatter *p, XmlChar *pTag, XmlChar *pAttr, unsigned short wAttrLen);
static short fnFormatterSendParsedCharData(XMLFormatter *p, XmlChar *pData, unsigned short wDataLen);
static short fnFormatterSendAttributes(XMLFormatter *p, XmlChar *pData, unsigned short wDataLen);
static short XMLFormatterInit(XMLFormatter *p, short (*fnChar)(XmlChar), short (*fnStart)(), short (*fnEOD)(BOOL fValid), unsigned long ulFlags);

/******************************
	Private Tables
******************************/

/*************************************************************
m_pHandleScriptFunctionTbl 
This table contains a list of low-level functions to call to
handle a given command in a script entry.  The command is an
index into this table so that the command N results in a
call to the Nth function in this list.  This function will 
handle the entry for the script.  Note: not all commands
are supported yet.  A NULL value in the table indicates that
the command is not supported.
*************************************************************/
typedef short (*fcnHandleXMLScriptEntry)(XMLFormatter *, const XMLScriptEntry, unsigned long ulCounter);

static const fcnHandleXMLScriptEntry m_pHandleScriptFunctionTbl[XMLFORMATTER_NUM_CMDS] =
{fnHandleCharDataCmd,
NULL,
NULL,
NULL,
fnHandleCommentCmd,
NULL,
fnHandleRetrieveAndTag,
NULL,
fnHandleCDSectCmd,
fnHandleSubscriptEntry};

/*************************************************************
Table defines escape sequences for markup characters.
*************************************************************/
#define XML_FORM_NUM_ESCAPE_CHARS 5

static const struct {
	XmlChar *pName;
	XmlChar xChar;
} EscapeSeq[] = {
	{(XmlChar *)"&amp;", 0x26},
	{(XmlChar *)"&lt;", 0x3C},
	{(XmlChar *)"&gt;", 0x3E},
	{(XmlChar *)"&apos;", 0x27},
	{(XmlChar *)"&quot;", 0x22}
};

#define FORM_ATTR_INIT_STATE 0
#define FORM_ATTR_NAME_STATE 1
#define FORM_ATTR_VALUE_QUOTE_STATE 2
#define FORM_ATTR_VALUE_STATE 3

/**********************************
	Public formatter functions
**********************************/

/*********************************************************************
Function: XMLFormat
Author: Derek DeGennaro
Description:
This function formats an XML document using a format script.  The
format script basically describes the structure of the document.
The XMLFormatter structure describes how the document is output.
This function MUST be called AFTER XMLFormatterInit.  This function
is a very simple function that basically calls the start fcn, calls
a lower-level (recursive) function to handle the script, and then
calls the end fcn with a status value.  The return value from
this function is XML_STATUS_OK if everything went ok, and something
else otherwise.  Once this function has been called, the only way
to stop the formatter is for its start fcn, end fcn, or send fcn to
return a value other than XML_STATUS_OK.

Inputs:
pScript - Pointer to a script that will be executed to output the
XML document.

pfnStart - Pointer to a function that will be called before the
formatting begins.

pfnEnd - Pointer to a function that will be called after the 
formatting is finished and no more characters will be output.

pfnChar - Pointer to a function that gets called when the
formatter outputs a character.  This gets called once for
every character in the output document.

ulFlags - A variable that contains bit flags which turn on various
features of the formatter.

ppTbl - A table containing element names whose contents must
be "Blinded".  Blinding is done by Base-64 Encoding and the
formatter's built in Base-64 Encoder.

iTblLen - The number of tags in ppTbl

Outputs:
Return Value:
An XML Status return value (see xmltypes.h)
*********************************************************************/
short XMLFormat(XMLFormatScript *pScript,
				  short (*pfnStart)(),
				  short (*pfnEnd)(BOOL),
				  short (*pfnChar)(XmlChar),
				  unsigned long ulFlags)
{
	short iResult = XML_STATUS_OK, iEndResult = XML_STATUS_OK;
	XMLFormatter xmlFormatter, *p = &xmlFormatter;

	/* initialize the structure */
	iResult = XMLFormatterInit(p,pfnChar,pfnStart,pfnEnd,ulFlags);
	if (iResult != XML_STATUS_OK)
		return iResult;

	/* we now call the start fcn.  This tells the
	user of the formatter that we are about to
	format. */
	if (p->fnStartFormatter != NULL)
		iResult = p->fnStartFormatter();
	if (iResult != XML_STATUS_OK)
		return iResult;

	/* We now call a lower-level function to
	actually do the formatting. */
	iResult = fnHandleXMLScript(p, pScript, pScript->ulCounter);
	

	/* We call the end fcn. This tells the user
	that we are done and indicates whether there
	was an error. */
	if (p->fnEndFormatter != NULL)
	{
		/* call the b64 encoder end function if
		we are blinding, else call the user
		end function directly. */
		if (p->fBlinding == TRUE)
		{
			/*********************************
			When Blinding mechanism has been
			finalized, put the appropriate
			stuff here.
			*********************************/
			iEndResult = XML_FORMATTER_BAD_OPTION;
		}
		else
			iEndResult = p->fnEndFormatter((unsigned char)(iResult==XML_STATUS_OK));
	}

	if (iResult == XML_STATUS_OK)
	{
		/* There were no errors during the formatting */
		if (iEndResult == XML_STATUS_OK)
			/* There were no errors during or after the formatting */
			return XML_STATUS_OK;
		else
			/* There was an error after the formatting. */
			return iEndResult;
	}
	else
		/* There was an error during formatting. */
		return iResult;
}

/********************************************
	Implementation of private
	utility functions.
********************************************/

/*********************************************************************
Function: XMLFormatterInit
Author: Derek DeGennaro
Description:
This function initializes an XMLFormatter structure. This function
MUST be called before the XMLFormat function.  A formatter is simply
an interface that contains pointers to functions that get called 
when the formatter starts, ends, and outputs a character.  This function
simply initializes the structure with the values passed as paramters.
Each of the function pointers is required.
Inputs:
pScript - Pointer to a script that will be executed to output the
XML document.

pfnStart - Pointer to a function that will be called before the
formatting begins.

pfnEnd - Pointer to a function that will be called after the 
formatting is finished and no more characters will be output.

pfnChar - Pointer to a function that gets called when the
formatter outputs a character.  This gets called once for
every character in the output document.

ulFlags - A variable that contains bit flags which turn on various
features of the formatter.

ppTbl - A table containing element names whose contents must
be "Blinded".  Blinding is done by Base-64 Encoding and the
formatter's built in Base-64 Encoder.

iTblLen - The number of tags in ppTbl

Outputs:
Return Value:
An XML Status return value (see xmltypes.h)
*********************************************************************/
static short XMLFormatterInit(XMLFormatter *p, 
					   short (*fnChar)(XmlChar),
					   short (*fnStart)(),
					   short (*fnEOD)(BOOL),
					   unsigned long ulFlags)
{
	short iResult = XML_STATUS_OK;

	if (p == NULL || fnChar == NULL )
	{
		/* error, the formatter is null or there is a null
		function pointer. */
		return XML_FORMATTER_INVALID;
	}
	p->fnStartFormatter = fnStart;
	p->fnEndFormatter = fnEOD;
	p->fnSendChar = fnChar;
	p->fBlinding = FALSE;

	return iResult;
}

/*********************************************************************
Function: fnXMLOutputXmlChar
Author: Derek DeGennaro
Description:
This function is responsible for actually sending the generated
characters.  The characters may go directly to the user if blinding
is disabled, or they may go to the base-64 encoder if blinding is
activated.  This adds a layer of indirection in the formatter and
affects performance since every character generated now has to go
through this new layer.  However, given the architecture of the
formatter and encoder, it cannot be done any other way.  The
function prototypes for the input to the base-64 encoder and
the user functions are differenct so we cannot use a function
pointer.  The performance hit is also pretty small so i think
we can live with it.
*********************************************************************/
static short fnXMLOutputXmlChar(XMLFormatter *p, XmlChar x)
{
	if (p->fBlinding == TRUE)
	{
		/* we are blinding, send the encoder the char */
		return XML_FORMATTER_BAD_OPTION;
	}
	else
	{
		/* we are not blinding, send the char to the user */
		return p->fnSendChar(x);
	}
}
/*********************************************************************
Function: fnHandleXMLScript
Author: Derek DeGennaro
Description:
This is the lower-level (recursive) function that formats an XML format
script.  Each script has a list of script entries.  For each of these
entries this function calls an even lower-level function to handle
the entry.  If the entry requires the processing of another script
then this function will eventually be called recursively.  A script
always has a tag associated with it.  This was done as a compromise
between ease of use and ensuring well-formed documents.
*********************************************************************/
static short fnHandleXMLScript(XMLFormatter *p, XMLFormatScript *pScript, unsigned long ulCounter)
{
	short i, iResult = XML_STATUS_OK;
	unsigned short wAttrLen = 0;
	XmlChar *pAttr = NULL;

	if (pScript->iEntries > 0 && pScript->fShowEmptyElemTag == TRUE)
	{
		/* error, want empty tag with actual entries */
		return XML_FORMATTER_BAD_DATA;
	}
	if (pScript->pTag == NULL)
	{
		/* error, a script must have a tag */
		return XML_FORMATTER_BAD_DATA;
	}

	if (pScript->fResetCntr)
	{
		/* We initialize the script counter based on
		the fStartWithZero flag. */
		ulCounter = 1;
		if (pScript->fStartWithZero == TRUE) 
			ulCounter = 0;
		
	}
	else if (pScript->fUseParentCtr == TRUE)
	{
		/* Initialize the counter based on the 
		script's parent counter value. */
		
	}
	else
	{
		/* Initialize the counter based on the
		default value specified in the script. */
		ulCounter = pScript->ulCounter;
	}

	/* check to see whether the script is empty */
	if (pScript->iEntries > 0)
	{
		/* this is not an empty script */

		/* get the attribute string if necessary */
		if (pScript->fcnGetAttrString != NULL)
			pAttr = pScript->fcnGetAttrString(pScript->ulDataID, ulCounter, XMLFORMATTER_STRING_TYPE, &wAttrLen);

		/* a script always has a tag, output the STag. */
		iResult = fnFormatterSendSTag(p, pScript->pTag, pAttr, wAttrLen);

		/* for each of the entries, take appropriate action. */
		for (i = 0; i < pScript->iEntries && iResult == XML_STATUS_OK; i++)
		{
			/* call the lower level function to handle the entry */
			iResult = fnHandleXMLScriptEntry(p, pScript->pEntry[i], ulCounter);
			
			/* check to see if the next function is needed */
			
			/* update the counter if necessary */
			if (pScript->fIncCntr == TRUE)
				ulCounter++;
		}

		/* output ETag */
		if (iResult == XML_STATUS_OK)
			iResult = fnFormatterSendETag(p, pScript->pTag);
	}
	else
	{
		/* nothing in script (this is an empty
		script).  output STAG,ETAG or
		EmptyElemTag depending on flag */

		/* get the attribute string if necessary */
			if (pScript->fcnGetAttrString != NULL)
				pAttr = pScript->fcnGetAttrString(pScript->ulDataID, ulCounter, XMLFORMATTER_STRING_TYPE, &wAttrLen);

		if (pScript->fShowEmptyElemTag == TRUE)
		{
			/* now out put an EmptyElemTag */
			iResult = fnFormatterSendEmptyElemTag(p, pScript->pTag, pAttr, wAttrLen);
		}
		else
		{
			/* output a STag followed by an ETag */
			iResult = fnFormatterSendSTag(p, pScript->pTag, pAttr, wAttrLen);
			if (iResult == XML_STATUS_OK)
				iResult = fnFormatterSendETag(p, pScript->pTag);
		}

	}

	return iResult;
}
/*********************************************************************
Function: fnHandleXMLScriptEntry
Author: Derek DeGennaro
Description:
This function is the low level function that handles each of the 
entries in a script.  The job of this function is to determine how
may times to execute this script entry and to call the pre,post,
and next functions that will update the counter.
The entries have a command that tells this
function what kind of action needs to be preformed.  The table
m_pHandleScriptFunctionTbl to determine which function will handle
the entry.
*********************************************************************/
static short fnHandleXMLScriptEntry(XMLFormatter *p, XMLScriptEntry entry, unsigned long ulCounter)
{

	unsigned long ulIndex = 1, ulLimit = entry.iDefaultCount;
	short iResult = XML_STATUS_OK;
	BOOL fContinue = TRUE;

	/* A variable length entry must at least have a 
	start entry call back. */
	if (entry.fVariableLength == TRUE && entry.fcnStartEntry == NULL)
		return XML_FORMATTER_BAD_CMD;

	if (entry.fResetCntr == TRUE)
	{
		/* initialize the counter based on the fStartWithZero flag */
		ulCounter = 1;
		if (entry.fStartWithZero == TRUE)
			ulCounter = 0;
	}
	else if (entry.fInheritCntr == TRUE)
	{
		/* Initialize the counter based on the 
		current counter value. */		
	}
	else
	{
		/* Initialize the counter based on the 
		entry's default value. */
		ulCounter = entry.ulCounter;
	}

	/* check to see if we need to call a get count
	function instead of using the default count */
	if (entry.fcnGetCount != NULL)
	{
		ulLimit = entry.fcnGetCount(ulCounter);
	}

	/* Finally, check to see if the value returned by
	a get count fcn should be interpreted as a boolean
	value.  i.e. if the get count returns a value > 0
	then we should execute the entry once, else we
	should not execute the entry. This trickery allows
	us to use a normal get count fcn to return a bool
	value and not the number of times to execute an
	entry. */
	if (entry.fcnGetCount != NULL && entry.fUseCountAsBool == TRUE)
	{
		if (ulLimit > 0)
			ulLimit = 1;
		else
			ulLimit = 0;
	}
	
	/* execute the script for X number of times */
	while (iResult == XML_STATUS_OK && fContinue == TRUE)
	{		
		if (entry.fVariableLength)
		{
			/* 
			This entry is flagged as variable length,
			we always call the start entry function
			before processing this entry. 
			*/
			if(entry.fcnStartEntry() == FALSE)			
				fContinue = FALSE;
		}
		else 
		{
			/*
			The entry is not flagged as variable length.
			First, determine if we have are done with this
			entry (the index has exceeded the limit). If
			not, continue.
			Second, if there is a start entry call back function
			then call it and stop/continue as necessary.			
			*/			
			if (ulIndex > ulLimit || iResult != XML_STATUS_OK)
				fContinue = FALSE;
			if (iResult == XML_STATUS_OK && fContinue == TRUE && entry.fcnStartEntry != NULL)
			{
				if(entry.fcnStartEntry() == FALSE)			
					fContinue = FALSE;
			}
		}

		if (iResult == XML_STATUS_OK && fContinue == TRUE)
		{
			/* look up and call the function to handle the command */
			iResult = m_pHandleScriptFunctionTbl[entry.iCommand](p, entry, ulCounter);					


			/* increment index */
			ulIndex += 1;
			/* check to see if we call a next fcn */

			/* upate the counter if necessary */
			if (entry.fIncCntr == TRUE)
				ulCounter++;
			
			if (entry.fcnEndEntry != NULL && entry.fcnEndEntry() == FALSE)
				fContinue = FALSE;
		}
	}

	return iResult;
}
/*********************************************************************
Function: fnHandleCharDataCmd
Author: Derek DeGennaro
Description:
This function basically gets some char data and simply outputs it.
*********************************************************************/
static short fnHandleCharDataCmd(XMLFormatter *p, const XMLScriptEntry entry, unsigned long ulCounter)
{
	char *pData = NULL;
	unsigned short wResultLen, i;
	short iResult = XML_STATUS_OK;

	/* make sure the get data fcn is ok */
	if (entry.fcnGetData != NULL)
	{	
		pData = (char*)entry.fcnGetData(entry.ulDataID,ulCounter,XMLFORMATTER_STRING_TYPE,&wResultLen);
		if (pData != NULL)
		{
			/* if we have some data */
			for (i = 0; i < wResultLen && iResult == XML_STATUS_OK; i++)
			{
				/* send each of the chars to the user */
				/* iResult = p->fnSendChar(pData[i]); */
				iResult = fnXMLOutputXmlChar(p,pData[i]);
			}
		}
	}

	return iResult;
}
/*********************************************************************
Function: fnHandleSubscriptEntry
Author: Derek DeGennaro
Description:
This function handles the recursive nature of XML scripts.  Since
elements can have child elements, the scripts need a way to execute
another script from within a script.
*********************************************************************/
static short fnHandleSubscriptEntry(XMLFormatter *p, const XMLScriptEntry entry, unsigned long ulCounter)
{
	XMLFormatScript *pScript = NULL;
	if (entry.pSubscript == NULL)
	{
		/* error, the subscript is invalid */
		return XML_FORMATTER_BAD_SCRIPT;
	}

	pScript = (XMLFormatScript*)entry.pSubscript;
	
	/* recursively handle the new script */
	return fnHandleXMLScript(p, pScript, ulCounter);

}
/*********************************************************************
Function: fnHandleRetrieveAndTag
Author: Derek DeGennaro
Description:
This function is used to stick some data between a start and end tag.
This function makes it a little easier to output elements which
only have some char data in them (as opposed to recursively executing
another script).
*********************************************************************/
static short fnHandleRetrieveAndTag(XMLFormatter *p, const XMLScriptEntry entry, unsigned long ulCounter)
{
	char *pData = NULL;
	unsigned short wResultLen = 0, wAttrLen = 0;
	short iResult = XML_STATUS_OK;
	XmlChar *pAttr = NULL;

	if (entry.pDataTag == NULL)
	{
		/* error, the tag is invalid */
		return XML_FORMATTER_BAD_TAG;
	}
	
	/* get the data to begin */
	/* make sure the get data fcn is ok */
	if (entry.fcnGetData != NULL)
	{		
		/* get the data */
		pData = (char*)entry.fcnGetData(entry.ulDataID,ulCounter,XMLFORMATTER_STRING_TYPE,&wResultLen);
	}

	/* if there is an attribute required */
	if (entry.fcnGetAttrString != NULL)
	{
		if (entry.fUseDiffAttrID == FALSE)
			pAttr = (XmlChar*)entry.fcnGetAttrString(entry.ulDataID,ulCounter,XMLFORMATTER_STRING_TYPE,&wAttrLen);
		else
			pAttr = (XmlChar*)entry.fcnGetAttrString(entry.ulAttrID,ulCounter,XMLFORMATTER_STRING_TYPE,&wAttrLen);
	}	

	/* try to output the STag */
	if (entry.fRemoveEmptyElements == FALSE || 
		pAttr != NULL ||
		(pData != NULL && wResultLen > 0) )
	{
		/*
		If the RemoveEmptyElements flag is clear OR
			there is data OR 
			there is an attribute
			Then we must send out the start tag				
		*/
		iResult = fnFormatterSendSTag(p, entry.pDataTag, pAttr, wAttrLen);
	}

	/* try to output the data */
	if (iResult == XML_STATUS_OK)
	{		
		if (pData != NULL && wResultLen > 0)
		{
			/* if we have some data */
			iResult = fnFormatterSendParsedCharData(p,(XmlChar*)pData,wResultLen);			
		}
	}

	/* output the ETag */
	if (iResult == XML_STATUS_OK)
	{
		/*
		If the RemoveEmptyElements flag is clear OR
			there is data OR 
			there is an attribute
			Then we must send out the end tag				
		*/
		if (entry.fRemoveEmptyElements == FALSE || 
			pAttr != NULL ||
			(pData != NULL && wResultLen > 0) )
			iResult = fnFormatterSendETag(p, entry.pDataTag);
	}

	return iResult;
}
/*********************************************************************
Function: fnHandleCommentCmd
Author: Derek DeGennaro
Description:
This function is similar to the Retrieve and tag function, it sticks
some stuff between some other stuff.
*********************************************************************/
static short fnHandleCommentCmd(XMLFormatter *p, const XMLScriptEntry entry, unsigned long ulCounter)
{
	char *pData = NULL;
	unsigned short wResultLen, i;
	short iResult = XML_STATUS_OK;

	if (entry.fcnGetData != NULL)
	{	
		iResult = fnFormatterSendString(p, (XmlChar*)"<!--");
		pData = (char*)entry.fcnGetData(entry.ulDataID,ulCounter,XMLFORMATTER_STRING_TYPE,&wResultLen);
		if (pData != NULL && iResult == XML_STATUS_OK)
		{
			for (i = 0; i < wResultLen && iResult == XML_STATUS_OK; i++)
			{
				/* iResult = p->fnSendChar(pData[i]); */
				iResult = fnXMLOutputXmlChar(p,pData[i]);
			}
		}
		if (iResult == XML_STATUS_OK)
			iResult = fnFormatterSendString(p, (XmlChar*)"--!>");
	}

	return iResult;
}
/*********************************************************************
Function: fnHandleCDSectCmd
Author: Derek DeGennaro
Description:
This function is similar to the Retrieve and tag function, it sticks
some stuff between some other stuff.
*********************************************************************/
static short fnHandleCDSectCmd(XMLFormatter *p, const XMLScriptEntry entry , unsigned long ulCounter)
{
	char *pData = NULL;
	unsigned short wResultLen, i;
	short iResult = XML_STATUS_OK;

	if (entry.fcnGetData != NULL)
	{	
		iResult = fnFormatterSendString(p, (XmlChar*)"<![CDATA[");
		pData = (char*)entry.fcnGetData(entry.ulDataID,ulCounter,XMLFORMATTER_STRING_TYPE,&wResultLen);
		if (pData != NULL && iResult == XML_STATUS_OK)
		{
			for (i = 0; i < wResultLen && iResult == XML_STATUS_OK; i++)
			{
				/* iResult = p->fnSendChar(pData[i]); */
				iResult = fnXMLOutputXmlChar(p,pData[i]);
			}
		}
		if (iResult == XML_STATUS_OK)
			iResult = fnFormatterSendString(p, (XmlChar*)"]>");
	}

	return iResult;
}
/*********************************************************************
Function: fnFormatterSendString
Author: Derek DeGennaro
Description:
This function is used to send strings to the user.  Only NULL-terminated
strings can be used for this function.  This is used primarily to
output STag's,ETag's, and EmptyElemTag's.  Other strings are sent
based on their length.
*********************************************************************/
/* NULL-TERMINATED STRINGS ONLY */
static short fnFormatterSendString(XMLFormatter *p, XmlChar *pString)
{
	short iResult = XML_STATUS_OK;
	XmlChar *pTemp = pString;
	if (pString != NULL)
	{
		while (iResult == XML_STATUS_OK && pTemp != NULL && *pTemp != (XmlChar)0)
		{
			/* iResult = p->fnSendChar(*pTemp++); */
			iResult = fnXMLOutputXmlChar(p, *pTemp++);
		}
	}
	return iResult;
}
/*********************************************************************
Function: fnFormatterSendSTag
Author: Derek DeGennaro
Description:
Sends a STag to the user.
*********************************************************************/
static short fnFormatterSendSTag(XMLFormatter *p, XmlChar *pTag, XmlChar *pAttr, short wAttrLen)
{
	short j, iResult = XML_STATUS_OK;

	/* output STag */
	j = 0;
	/* iResult = p->fnSendChar((XmlChar)'<'); */
	iResult = fnXMLOutputXmlChar(p,(XmlChar)'<');
	while (iResult == XML_STATUS_OK && pTag[j] != (XmlChar)0)
	{
		/* iResult = p->fnSendChar(pTag[j++]); */
		iResult = fnXMLOutputXmlChar(p,pTag[j++]);
	}
	if (iResult == XML_STATUS_OK && pAttr != NULL && wAttrLen > 0)
		iResult = fnXMLOutputXmlChar(p,0x20); /* iResult = p->fnSendChar(0x20); */

	if (iResult == XML_STATUS_OK && pAttr != NULL && wAttrLen > 0)
	{
		iResult = fnFormatterSendAttributes(p,pAttr,wAttrLen);		
	}
	if (iResult == XML_STATUS_OK)
		iResult = fnXMLOutputXmlChar(p,(XmlChar)'>'); /* iResult = p->fnSendChar((XmlChar)'>'); */
	return iResult;
}
/*********************************************************************
Function: fnFormatterSendETag
Author: Derek DeGennaro
Description:
Sends an ETag to the user.
*********************************************************************/
static short fnFormatterSendETag(XMLFormatter *p, XmlChar *pTag)
{
	short j, iResult = XML_STATUS_OK;
	/* output STag */
	j = 0;
	/* iResult = p->fnSendChar((XmlChar)'<'); */
	iResult = fnXMLOutputXmlChar(p,(XmlChar)'<');
	if (iResult == XML_STATUS_OK)
		iResult = fnXMLOutputXmlChar(p,(XmlChar)'/'); /* iResult = p->fnSendChar((XmlChar)'/'); */
	while (iResult == XML_STATUS_OK && pTag[j] != (XmlChar)0)
	{
		/* iResult = p->fnSendChar(pTag[j++]); */
		iResult = fnXMLOutputXmlChar(p,pTag[j++]);
	}
	if (iResult == XML_STATUS_OK)
		iResult = fnXMLOutputXmlChar(p,(XmlChar)'>'); /* iResult = p->fnSendChar((XmlChar)'>'); */
	return iResult;
}
/*********************************************************************
Function: fnFormatterSendEmptyElemTag
Author: Derek DeGennaro
Description:
Sends an EmptyElemTag to the user.
*********************************************************************/
static short fnFormatterSendEmptyElemTag(XMLFormatter *p, XmlChar *pTag, XmlChar *pAttr, unsigned short wAttrLen)
{
	short j, iResult = XML_STATUS_OK;
	unsigned short k;
	/* output STag */
	j = 0;
	/* iResult = p->fnSendChar((XmlChar)'<'); */
	iResult = fnXMLOutputXmlChar(p,(XmlChar)'<');
	while (iResult == XML_STATUS_OK && pTag[j] != (XmlChar)0)
	{
		/* iResult = p->fnSendChar(pTag[j++]); */
		iResult = fnXMLOutputXmlChar(p,pTag[j++]);
	}
	if (iResult == XML_STATUS_OK && pAttr != NULL && wAttrLen > 0)
		iResult = fnXMLOutputXmlChar(p,0x20); /* iResult = p->fnSendChar(0x20); */

	if (iResult == XML_STATUS_OK && pAttr != NULL && wAttrLen > 0)
	{
		for (k = 0; k < wAttrLen && iResult == XML_STATUS_OK; k++)
		{
			/* iResult = p->fnSendChar((XmlChar)pAttr[k]); */
			iResult = fnXMLOutputXmlChar(p,(XmlChar)pAttr[k]);
		}
	}
	if (iResult == XML_STATUS_OK)
		iResult = fnXMLOutputXmlChar(p,(XmlChar)'/'); /* iResult = p->fnSendChar((XmlChar)'/'); */
	if (iResult == XML_STATUS_OK)
		iResult = fnXMLOutputXmlChar(p,(XmlChar)'>'); /* iResult = p->fnSendChar((XmlChar)'>'); */
	return iResult;
}
/*********************************************************************
Function: fnFormatterSendParsedCharData
Author: Derek DeGennaro
Description:
Outputs Parsed Character Data. If an markup character is found its
escape sequence is output.
*********************************************************************/
static short fnFormatterSendParsedCharData(XMLFormatter *p, XmlChar *pData, unsigned short wDataLen)
{
	short iResult = XML_STATUS_OK;
	unsigned short		i,j,k;
	int iEscape = 0;
	
	/* for each character */
	for (i=0; i < wDataLen && iResult == XML_STATUS_OK; i++)
	{
		/* check to see if it is markup */
		iEscape = 0;
		j = 0;
		while (j < XML_FORM_NUM_ESCAPE_CHARS && iEscape == 0)
		{
			if (pData[i] == EscapeSeq[j].xChar)
				iEscape = 1;
			else
				j++;
		}
		if (iEscape == 1 && j < XML_FORM_NUM_ESCAPE_CHARS)
		{
			/* the character is markup, sends its
			escape sequence. */
			k = 0;
			while (EscapeSeq[j].pName[k] != 0 && iResult == XML_STATUS_OK)
			{
				iResult = fnXMLOutputXmlChar(p,EscapeSeq[j].pName[k]);
				k++;
			}
		}
		else
		{
			/* the character is NOT markup, just output it */
			iResult = fnXMLOutputXmlChar(p,pData[i]);
		}
	}

	return iResult;
}
/*********************************************************************
Function: fnFormatterSendAttributes
Author: Derek DeGennaro
Description:
Outputs an attribute string returned from the "Get Data" style functions. 
If an markup character is found its escape sequence is output.
*********************************************************************/
static short fnFormatterSendAttributes(XMLFormatter *p, XmlChar *pData, unsigned short wDataLen)
{
	short iResult = XML_STATUS_OK, i,j,k;
	int iEscape = 0, iSkipEscape = 0, iState = FORM_ATTR_INIT_STATE, iLookForQuote = 0;
	XmlChar xQuoteChar = 0;
	
	/* for each character */
	for (i=0; i < wDataLen && iResult == XML_STATUS_OK; i++)
	{
		switch (iState) {
		case FORM_ATTR_INIT_STATE:
			if (fnIsUCWs(pData[i]))
			{
				iState = FORM_ATTR_INIT_STATE;	/* skip whitespace */
			}
			else if (fnIsUCStartNameChar(pData[i]))	
			{
				iLookForQuote = 0;
				iState = FORM_ATTR_NAME_STATE;	/* the name has started */
			}
			else if (!fnIsUCWs(pData[i]))
			{
				return XML_FORMATTER_BAD_DATA;	/* invalid char */			
			}
			break;
		case FORM_ATTR_NAME_STATE:
			if (pData[i] == (XmlChar)'=')
			{
				if (iLookForQuote == 0)
					iState = FORM_ATTR_VALUE_QUOTE_STATE; /* look for quote */
				else
					return XML_FORMATTER_BAD_DATA;		/* invalid char */
			}
			else if (fnIsUCNameChar(pData[i]))
			{
				if (iLookForQuote == 0)
					iState = FORM_ATTR_NAME_STATE;		/* this is ok, output it */
				else
					return XML_FORMATTER_BAD_DATA;		/* a char between the ws and quote is bad */
			}
			else if (fnIsUCWs(pData[i]))
			{
				iLookForQuote = 1;						/* set flag and output the char */
				iState = FORM_ATTR_NAME_STATE;			/* this is ok, output it */
			}
			else
			{
				return XML_FORMATTER_BAD_DATA;			/* invalid char */
			}
			break;
		case FORM_ATTR_VALUE_QUOTE_STATE:
			if (pData[i] == (XmlChar)0x22 || pData[i] == (XmlChar)0x27)
			{
				xQuoteChar = pData[i];
				iSkipEscape = 1;
				iState = FORM_ATTR_VALUE_STATE;
			}
			else if (!fnIsUCWs(pData[i]))		/* skip whitespace */
				return XML_FORMATTER_BAD_DATA;	/* invalid char */
			break;
		case FORM_ATTR_VALUE_STATE:
			if (pData[i] == xQuoteChar)			/* we are done */
			{
				iSkipEscape = 1;
				iState = FORM_ATTR_INIT_STATE;
			}
			break;	
		default:
			return XML_FORMATTER_BAD_DATA;
		};

		j = 0;
		if (iSkipEscape == 0)
		{
			/* check to see if this char needs to be escaped */			
			while (j < XML_FORM_NUM_ESCAPE_CHARS && iEscape == 0)
			{
				if (pData[i] == EscapeSeq[j].xChar)
					iEscape = 1;
				else
					j++;
			}
		}
		
		/* if we are here then output the char */
		if (iEscape == 0 || iSkipEscape == 1)
		{		
			iResult = fnXMLOutputXmlChar(p,pData[i]);
		}
		else
		{			
			/* the character is markup, sends its
			escape sequence. */
			k = 0;
			while (EscapeSeq[j].pName[k] != 0 && iResult == XML_STATUS_OK)
			{
				iResult = fnXMLOutputXmlChar(p,EscapeSeq[j].pName[k]);
				k++;
			}
		}
		iEscape = 0;
		iSkipEscape = 0;
	}

	return iResult;
}

/***********************
	Test Stuff
***********************/

/**************************************************************/

//#define XMLFORMATTER_TEST_HARNESS

#ifdef XMLFORMATTER_TEST_HARNESS

#define FORMATTER_DEBUG_BUFF_LEN 100000
static XmlChar g_pFormatterDebugBuffer[FORMATTER_DEBUG_BUFF_LEN] = "";
static short g_wFormatterDebugLen = 0;

static short fnTestFormStartDoc()
{
#ifdef FOMATTER_DEBUG_PRINTF
	dbgTrace(DBG_LVL_INFO, "Starting to format...\n");
#endif
	g_wFormatterDebugLen = 0;
	return XML_STATUS_OK;
}
static short fnTestFormEndDoc(BOOL fValid)
{
#ifdef FOMATTER_DEBUG_PRINTF
	dbgTrace(DBG_LVL_INFO, "Ending formatting (Status=%d).\n", fValid);
#endif
	return XML_STATUS_OK;
}
static short fnTestFormSendChar(XmlChar c)
{
#ifdef FOMATTER_DEBUG_PRINTF
	dbgTrace(DBG_LVL_INFO, "%c", c);
#endif
	if (g_wFormatterDebugLen < FORMATTER_DEBUG_BUFF_LEN)
		g_pFormatterDebugBuffer[g_wFormatterDebugLen++] = c;
	return XML_STATUS_OK;
}

static short fnTestSendCharFailure(XmlChar c)
{
	return XML_FORMATTER_INVALID;
}
/*cmd,			def count,	zero?,	fncount,	tag?,		dataID,			fngetdata,		fngetattr,		subscript ptr */
short fnFormatterUnitTest1()
{
	/* tests send stag,etag, & emptyelemtag plus send string */
	short iResult;
	XMLFormatter xmlFormatter;
	XmlChar pABC[] = "abc";

	/* testing send string */
	xmlFormatter.fnSendChar = fnTestFormSendChar;
	xmlFormatter.fnEndFormatter = fnTestFormEndDoc;
	xmlFormatter.fnStartFormatter = fnTestFormStartDoc;

	/* try NULL string, should return ok */
	xmlFormatter.fnStartFormatter();
	iResult = fnFormatterSendString(&xmlFormatter, NULL);
	if (iResult != XML_STATUS_OK) return 0;

	/* try a bad sendfcn call */
	xmlFormatter.fnStartFormatter();
	xmlFormatter.fnSendChar = fnTestSendCharFailure;
	iResult = fnFormatterSendString(&xmlFormatter, (XmlChar*)"abc");
	if (iResult != XML_FORMATTER_INVALID) return 0;

	/* try successfull call */
	xmlFormatter.fnStartFormatter();
	xmlFormatter.fnSendChar = fnTestFormSendChar;
	iResult = fnFormatterSendString(&xmlFormatter, (XmlChar*)"abc");
	if (iResult != XML_STATUS_OK || memcmp(pABC, g_pFormatterDebugBuffer, 3) != 0) return 0;

	/* try send stag */

	/* try bad sendfcn call */
	xmlFormatter.fnSendChar = fnTestSendCharFailure;
	xmlFormatter.fnStartFormatter();
	iResult = fnFormatterSendSTag(&xmlFormatter,(XmlChar*)"abc",NULL,0);
	if (iResult != XML_FORMATTER_INVALID) return 0;

	/* make sure the thing outputs an ok tag*/
	xmlFormatter.fnSendChar = fnTestFormSendChar;
	xmlFormatter.fnStartFormatter();
	iResult = fnFormatterSendSTag(&xmlFormatter,(XmlChar*)"abc",NULL,0);
	if (iResult != XML_STATUS_OK || memcmp("<abc>",g_pFormatterDebugBuffer,5) != 0) return 0;
	xmlFormatter.fnSendChar = fnTestFormSendChar;
	xmlFormatter.fnStartFormatter();
	iResult = fnFormatterSendSTag(&xmlFormatter,(XmlChar*)"abc",(XmlChar*)"a='b'",5);
	if (iResult != XML_STATUS_OK || memcmp("<abc a='b'>",g_pFormatterDebugBuffer,10) != 0) return 0;

	/* try etag */

	/* try bad sendfcn call */
	xmlFormatter.fnSendChar = fnTestSendCharFailure;
	xmlFormatter.fnStartFormatter();
	iResult = fnFormatterSendETag(&xmlFormatter,(XmlChar*)"abc");
	if (iResult != XML_FORMATTER_INVALID) return 0;

	/* make sure the thing outputs an ok tag*/
	xmlFormatter.fnSendChar = fnTestFormSendChar;
	xmlFormatter.fnStartFormatter();
	iResult = fnFormatterSendETag(&xmlFormatter,(XmlChar*)"abc");
	if (iResult != XML_STATUS_OK || memcmp("</abc>",g_pFormatterDebugBuffer,6) != 0) return 0;

	return 1;
}
enum {	FTDATA_1,FTDATA_2 };

static XmlChar m_xTestBuffer1[100];

static void *fnGetFormatterData (unsigned long ulDataID,
					unsigned long ulDataItemNo,
					unsigned char bFormatType, 
					unsigned short *bsize)
{
	*bsize = 0;
	switch (ulDataID) {
	case FTDATA_1:
		strcpy((char*)m_xTestBuffer1,"abc");
		*bsize = strlen((char*)m_xTestBuffer1);
		break;
	case FTDATA_2:
	default:
		return NULL;
	};
	return m_xTestBuffer1;
}
/*cmd,			def count,	zero?,	fncount,	tag?,		dataID,			fngetdata,		fngetattr,		subscript ptr */
static XMLScriptEntry m_pTestScript1a[] = 
{{MK_RETRIEVE_N_TAG,1,		FALSE,	NULL,		"Blind",	FTDATA_1,		fnGetFormatterData,NULL,		NULL}};

static XMLFormatScript m_TestScript1 = { 1, "Test",  DEFAULT_XML_FORM_ID, NULL, FALSE, m_pTestScript1a};

static XmlChar *m_pBlindTagList[] = {"ZABC","zabc","Cabc","Blind","Dabc","Aabc"};
static const short m_iBlindTagListLen = 6;

static XmlChar m_pBlindTestBuff1[100] = "";
static short m_iBlindTestBuff1Len = 0;

static short fnTestBlindSendChar(XmlChar x)
{
	m_pBlindTestBuff1[m_iBlindTestBuff1Len] = x;
	m_iBlindTestBuff1Len = (++m_iBlindTestBuff1Len)%100;

	return XML_STATUS_OK;
}
static short fnTestBlindSendEOF(BOOL fValid)
{
	return XML_STATUS_OK;
}
static short fnTestBlindSendStart()
{
	return XML_STATUS_OK;
}
static short fnTestFormatterBlind1()
{
	XMLFormatter xmlFormatter;
	short iResult;

	if (XMLFormatterInit(&xmlFormatter,fnTestBlindSendChar,fnTestBlindSendStart,fnTestBlindSendEOF, 0) != XML_STATUS_OK)
		return 0;
	
	iResult = XMLFormat(&xmlFormatter,&m_TestScript1, 0);
	return (iResult == XML_STATUS_OK);
}
short TestFormatter()
{	
	return fnTestFormatterBlind1();
}
#endif
/* END TEST STUFF ****************************************/
