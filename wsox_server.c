
#include "nucleus.h"
#include "networking/nu_networking.h"
#include "wolfssl/wolfcrypt/logging.h"
#include "commontypes.h"
#include "wsox_server.h"
#include "cJSON.h"
#include "wsox_tx_queue.h"
#include "wsox_services.h"
#include "sys_log_demo.h"
#include "am335x_timer.h"
#include "blinker.h"
#include "csd_sim.h"
#include "events.h"
#include "pbos.h"
#include "version.h"
#include "UTILS.h"
#include "sslinterface.h"
#include "networking/websocket/wsox_int.h"
#include "sys.h"

#define  CRLF   "\r\n"

extern NU_SEMAPHORE            WSOX_Resource;


#ifdef MEMORYTEST
static  NU_TASK EventHandler_CB;
static VOID EventHandler_Task(UNSIGNED argc, VOID *argv);
#endif /* MEMORYTEST */

NU_MEMORY_POOL cJSON_mem_pool;
#define WSOX_MEMPOOL_SIZE			1024 * 100			// was 24576
static NU_HTTP_SSL_STRUCT  http_ssl;


/* -------------------------------------------------------------------------------------------------[wolfDebugCb]-- */
/*
 * Callback for the WOLFSSL debug tracing commands, we tack in the level and print the supplied string.
 * we allow the global level filter to select filter level to include. It seems that level 0 is important
 * and level 3 is less so.
 */
WOLFSSL_DEBUG_LEVELS	globalWolfDebugCbLevelFilter = WOLFDEBUG_ALL;
void wolfDebugCb(const int logLevel, const char *const logMessage)
{
	if(logLevel <= globalWolfDebugCbLevelFilter)
       dbgTrace(DBG_LVL_INFO, "%s[%3d] - %s", "WOLFSSL", logLevel, logMessage) ;
}


/* -------------------------------------------------------------------------------------------------[wsox_svr_init]-- */
STATUS wsox_svr_init(NU_MEMORY_POOL* mem_pool)
{
    VOID   *wsox_tx_q_start_addr;
    VOID   *cJSON_mem_pool_start_addr;
#ifdef MEMORYTEST
    VOID   *ev_handler_task_stack_start_addr;
#endif /* MEMORYTEST */
    STATUS  status, internal_sts=NU_SUCCESS;
    UINT8   rollback=0;

    wolfSSL_SetLoggingCb(wolfDebugCb);
    wolfSSL_Debugging_OFF();


    /* Allocate memory for communication queue */
    status = NU_Allocate_Memory(mem_pool, &wsox_tx_q_start_addr,
                                WSOX_TX_QUEUE_ENTRIES * sizeof(WSOX_Queue_Entry),
                                NU_NO_SUSPEND);

    /* Check to see if previous operation successful */
    if (status != NU_SUCCESS) rollback = 1;

    if (!rollback)
    {
        /* Create communication queue with a message size of 1.  */
        status = NU_Create_Queue(&Queue_wsox_tx, "WS TX Q", wsox_tx_q_start_addr,
                                 WSOX_TX_QUEUE_ENTRIES * sizeof(WSOX_Queue_Entry)/sizeof(UNSIGNED),
                                 NU_FIXED_SIZE, sizeof(WSOX_Queue_Entry)/sizeof(UNSIGNED), NU_FIFO);

        if (status != NU_SUCCESS) rollback = 2;
    }


    //if (start_network_and_http_lite_server())
    if (!rollback)
    {
        /* allocate memory for cJSON library */
        status = NU_Allocate_Memory(mem_pool, &cJSON_mem_pool_start_addr, WSOX_MEMPOOL_SIZE, NU_NO_SUSPEND);
        if (status != NU_SUCCESS) rollback = 3;
    }
    if (!rollback)
    {
        status = NU_Create_Memory_Pool(&cJSON_mem_pool, "cJSONmp", cJSON_mem_pool_start_addr, WSOX_MEMPOOL_SIZE, 8, NU_PRIORITY);
        if (status != NU_SUCCESS) rollback = 4;
    }
    if (!rollback)
    {
        cJSON_Hooks hooks;
        hooks.malloc_fn = memory_allocate;
        hooks.free_fn   = memory_free;

        cJSON_InitHooks(&hooks);

#ifdef MEMORYTEST
        /* Allocate memory for the EventHandler_Task. */
        status = NU_Allocate_Memory(mem_pool, &ev_handler_task_stack_start_addr, 16 * 1024 /* STACK */, NU_NO_SUSPEND);
        if (status != NU_SUCCESS) rollback = 5;
#endif /* MEMORYTEST */

    }

#if MEMORYTEST
    if (!rollback)
    {
        /* Create the EventHandler_Task.  should be the lowest priority task (i.e. highest priority number). */
        status = NU_Create_Task(&EventHandler_CB, "EVHNDLR", EventHandler_Task,
                                0, NU_NULL, ev_handler_task_stack_start_addr,
                                16 * 1024 /* STACK */, 255 /* EV_TASK_PRIORITY */, 0 /* TASK_TIMESLICE */,
                                NU_PREEMPT, NU_START);
        if (status != NU_SUCCESS) rollback = 6;
    }
#endif /* MEMORYTEST */

    /* On error, deallocate memory. */
    switch(rollback)
    {
#ifdef MEMORYTEST
        case 6:
        	internal_sts |= NU_Deallocate_Memory(ev_handler_task_stack_start_addr);
        	//no break
#endif /* MEMORYTEST */

        case 5:
        	internal_sts |= NU_Delete_Memory_Pool(&cJSON_mem_pool);
        	//no break
        case 4:
        	internal_sts |= NU_Deallocate_Memory(cJSON_mem_pool_start_addr);
        	//no break
        case 3:
        	internal_sts |= NU_Delete_Queue(&Queue_wsox_tx);
        	//no break
        case 2:
        	internal_sts |= NU_Deallocate_Memory(wsox_tx_q_start_addr);
        	//no break
        case 1:
        	//no break
        case 0:
        	break;
    }
    /* internal_sts is not used after this, so remove the unused warning */
    NU_UNUSED_PARAM(internal_sts);

    return status;
}

/* ----------------------------------------------------------------------------------------[start_http_lite_server]-- */
static BOOLEAN start_http_lite_server()
{
    STATUS              status;
    struct addr_struct  servaddr;            /* Server address structure */
    SSL_DESCRIPTOR      *pSSLDesc;

    dbgTrace(DBG_LVL_INFO, "Initializing HTTP_Lite_Server...........");
    status = HTTP_Lite_Server_Init();
    if (status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_INFO, "  Error = %d (0x%X)" CRLF, status, status);
        return NU_FALSE;
    }
    else
    {
        dbgTrace(DBG_LVL_INFO, "OK" CRLF);
    }

    //dbgTrace(DBG_LVL_INFO, "HTTP_Lite_Bind..........................");
    memset(&servaddr, 0, sizeof(servaddr));
    //servaddr.family = NU_FAMILY_IP;
    //servaddr.port   = 80;

    //status = HTTP_Lite_Bind(&servaddr, NULL);
    //if (status != NU_SUCCESS)
    //{
    //    dbgTrace(DBG_LVL_ERROR, "HTTP_Lite_Bind Port = %d, Error = %d (0x%X)" CRLF, servaddr.port, status, status);
    //    return NU_FALSE;
    //}
    //else
    //{
    //    dbgTrace(DBG_LVL_INFO, "HTTP_Lite_Bind Port = %d OK" CRLF, servaddr.port);
    //}
    // Bind with SSL
    pSSLDesc = fnCreateSSLServerContext();
    if (pSSLDesc == NULL)
    {
        dbgTrace(DBG_LVL_ERROR, "Create SSL Server Context Error");
        return NU_FALSE;
    }
    memset(&http_ssl, 0, sizeof(http_ssl));
    http_ssl.ssl_context = (void *) pSSLDesc->sslContext;
    http_ssl.ssl_flags = NU_HTTP_USER_CTX;
    servaddr.family = NU_FAMILY_IP;
    servaddr.port   = 443;

    status = HTTP_Lite_Bind(&servaddr, &http_ssl);
    if (status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_ERROR, "HTTP_Lite_Bind Port = %d, Error = %d (0x%X)" CRLF, servaddr.port, status, status);
        return NU_FALSE;
    }
    else
    {
        dbgTrace(DBG_LVL_INFO, "HTTP_Lite_Bind Port = %d OK" CRLF, servaddr.port);
    }

    return NU_TRUE;
}


/* ----------------------------------------------------------------------------------------[socketIDfromWSOXHandle]-- */
INT socketIDFromWSOXHandle(UINT32 handle)
{
    WSOX_CONTEXT_STRUCT *wsox_ptr = NULL;
    INT                 socketd = -1;
    STATUS              status;

    // Note: WSOX_Find_Context_By_Handle is an internal Nucleus function so requires using the same internal
    // semaphore used by Nucleus.

    /* Get the WebSocket semaphore. */
    status = NU_Obtain_Semaphore(&WSOX_Resource, NU_SUSPEND);

    if (status == NU_SUCCESS)
    {
		/* Find a matching handle. */
		wsox_ptr = WSOX_Find_Context_By_Handle(handle);
        NU_Release_Semaphore(&WSOX_Resource);
    }
    else if (status == NU_SEMAPHORE_ALREADY_OWNED)
    {// This may be called from the WSOX Master Task which already has this semaphore
		wsox_ptr = WSOX_Find_Context_By_Handle(handle);
    }

	/* Check that the connection can send data, and that this is
	 * not a listener handle.  Only non-listener sockets will be flagged
	 * WSOX_TX, so there is no need to check that this is not a
	 * listener.
	 */
	if (wsox_ptr)
	{
		/* Get the socket associated with this handle. */
		socketd = wsox_ptr->socketd;
	}

	return socketd;
}


/* --------------------------------------------------------------------------------------[WebSocket_Server_tx_Task]-- */
VOID WebSocket_Server_tx_Task(UNSIGNED argc, VOID *argv)
{
    BOOLEAN started = false;
    STATUS  status;
    UNSIGNED received_size;
    WSOX_Queue_Entry entry;
    NU_MEMORY_POOL* mem_pool;
    unsigned long   lwCurrentEvents;
    char osstat;
    /* Reference unused parameters to avoid toolset warnings. */
    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);

    //ShowBootTracing();
    //ShowMACs();

    status = NU_System_Memory_Get(&mem_pool, NU_NULL);
    if (status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_INFO, "WsoxTxSrv: Can't retrieve system memory pool");
        return;
    }
    //prepTimerForUpCounting(TIMER_TO_TEST); -- Now done in Application_Initialize()
    sys_log_init();
    set_blink_error(0);

    // wait for SCM Init to be done which sets system clock; this is needed for SSL usage by the HTTP Server; 2 minute timeout
    osstat = OSReceiveEvents(SYS_PWRUP_EVENT_GROUP, (EV_SCM_INIT_COMPLETE | EV_SYS_INIT_HUNG), 120000, OS_OR, &lwCurrentEvents);
    if (osstat != (char) SUCCESSFUL)
    {
        dbgTrace(DBG_LVL_ERROR, "WsoxTxSrv Error %d, Event %X: Failed waiting for system clock to be set. Continuing anyway.", osstat, lwCurrentEvents);
    }

    if (wsox_svr_init(mem_pool) == NU_SUCCESS)
        started = start_http_lite_server();

    if (started)
    {
        log_handler_initialize();
        wsox_initialize(mem_pool);
        dbgTrace(DBG_LVL_INFO, "WsTxox server running...\n");

        fnSysLogTaskStart("WebSocketTxServerTask", WSTXSRVTASK);

        while(1)
        {
            status =  NU_Receive_From_Queue(&Queue_wsox_tx, &entry,
                                            sizeof(WSOX_Queue_Entry)/sizeof(UNSIGNED),
                                            &received_size, NU_SUSPEND);

            if (status == NU_SUCCESS)
            {
                wsox_tx_msg_queue_handler(&entry);

            }

        }

    }
}

/* --------------------------------------------------------------------------------------[WebSocket_Server_rx_Task]-- */
VOID WebSocket_Server_rx_Task(UNSIGNED argc, VOID *argv)
{
    INTERTASK	Msg;

    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);

    dbgTrace(DBG_LVL_INFO, "Wsox RX server running...\n");

    fnSysLogTaskStart("WebSocketRxServerTask", WSRXSRVTASK);

    while(1)
    {
        while(OSReceiveIntertask(WSRXSRVTASK, &Msg, OS_SUSPEND) != SUCCESSFUL){};

        if(Msg.bMsgType == WSOX_REQ_MSG_DATA)
        {
            cJSON *MsgId = cJSON_GetObjectItem(Msg.IntertaskUnion.wsox_req_msg.root, "MsgID");
            if (MsgId)
            {
                //dbgTrace(DBG_LVL_INFO, "WebSocket_Server_rx_Task: handle=%u, MsgID=%s\n",
                //        Msg.IntertaskUnion.wsox_req_msg.handle,
                //        MsgId->valuestring);

                Msg.IntertaskUnion.wsox_req_msg.event_fn(Msg.IntertaskUnion.wsox_req_msg.handle, Msg.IntertaskUnion.wsox_req_msg.root);

                cJSON_Delete(Msg.IntertaskUnion.wsox_req_msg.root); 
            }
            else
            {
                dbgTrace(DBG_LVL_ERROR, "WebSocket_Server_rx_Task: MsgID missing from cJSON object\n");
            }

        }
        else
        {
            dbgTrace(DBG_LVL_ERROR, "WebSocket_Server_rx_Task: Unexpected bMsgType (%d i.s.o. %d)\n", Msg.bMsgType, WSOX_REQ_MSG_DATA);
        }
    }

}

/* ---------------------------------------------------------------------------------------------[EventHandler_Task]-- */
#if MEMORYTEST
static VOID EventHandler_Task(UNSIGNED argc, VOID *argv)
{
    csd_sim_init();

    while(1)
    {
        ev_dispatch_one_event();
    }

}
#endif


/* -----------------------------------------------------------------------------------------------------------[EOF]-- */

