/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    api_weight.c

   DESCRIPTION:    API implementations for weight messages

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------

*************************************************************************/
#include "commontypes.h"
#include "cJSON.h"
#include "wsox_tx_queue.h"
#include "wsox_services.h"
#include "pbos.h"
#include "osmsg.h"

#define  CRLF   "\r\n"

extern BOOL fNoTabletConnection;

// Platform task requirements...
required_fields_tbl_t required_fields_tbl_SetTabToWPlatMsg =
    {
    "TabToWPlatMsgReq", { 		"Msg"
                           , NULL }
    };


void on_SetTabToWPlatMsg(UINT32 handle, cJSON *root)
{
    char rspStr[10];
    size_t rspLen = 0;
    char *ptr;

    memset(rspStr, 0x0, sizeof(rspStr));
    cJSON           *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_SetTabToWPlatMsg))
    {
    	char *HexMsg = cJSON_GetObjectItem(root, "Msg")->valuestring;
       	rspLen = strlen(HexMsg);
        if (OSGetMemory((void **) &ptr, 10) != SUCCESSFUL)
        {
                   //allocate partition memory
                   ;//todo:error handling
        }
        else
        {
        	memcpy(ptr, HexMsg, rspLen +1);
           	/* send intertask message to platform task */
        	(void)OSSendIntertask(PLAT, WSRXSRVTASK, 0, PART_PTR_DATA, ptr, rspLen);
        }

        // clean up allocated memory....
        cJSON_Delete(rspMsg) ;

    }
    else
    {
        cJSON_AddStringToObject(rspMsg, "MsgID", "WPlatToTabMsgRsp");
        /* an error has been added to rspMsg by RequiredFieldsArePresent() */
        addEntryToTxQueue(&entry, root, "  on_SetTabToWPlatMsg: Added error response to tx queue." CRLF);
    }
}

void Send_WPlatToTabMsgRsp(char *rspMsg)
{
	// Allow platform messages only if tablet is connected
	if (fNoTabletConnection  == FALSE)
	{
	    cJSON *rsp = cJSON_CreateObject();
	    cJSON_AddStringToObject(rsp, "MsgID", "WPlatToTabMsgRsp");
	    cJSON_AddStringToObject(rsp, "RspMsg", rspMsg);
	    Send_JSON(rsp, BROADCAST_MASK);
	}
}


