/* this file is used temporarily to get past a bug in bootloader 1.8
 * which prevents writing the file signature if the file footer doesn't
 * fall within the same 512 byte flash page as the end of the payload hash
 *
    hdr content type: APPIMGv2
    name in hdr     : App-local
    payload len     : 1608501 (0x00188B35)
    load address    : 0x80000000
    exec address    : 0x8000006C
    payload type    : 3 (compressed & signed)
    sha256          : FF5DA22BC3162E65D5FEB4B10BA91D00197BF01124CA61C56A82CDD4287BD07C
    image len       : 5073184 (0x004D6920)
    image sha256    : CA9DDD95A6C91E61BEECB4DDB8DC042B58A6BAC8613E85A3D1002416A7BF5281
    Created         : Horizon-CSD-Sr.bin.csd-app-img.qa_signed

 */
const char image_len_adjust[] = 
//   "0123";
 "cIMR8xsNn6yt7U89WFAWLX6K4awpmHBCbOjIYwgR3l9zrjxG02ZXrq95TO1IL9u002AehLlolQbDCkvv7FPfrsx0wbzi";
// "5etqcGmopMFR082mhpVt5MgeCsbIEdNiTPc5l5ZbSnJd56odnhTQkPt47TAAqFxwzAvF2eqjleRN7SpCYpD6p8NskjCgbTgP78cALMjj9weqpC30l5tAF76bNPI94t6e3nb5gv0DGFhOiEUJR6x11BVq0T2R4VTV1wOX9bZvmf6fcG6deqEuuwvKNckSZ0xM0tFr1WBefoOUvB7hvXd3Z5qexgShK1yfDiSgHSoBIO3j7L41buO8atSdv5ow8zgV3";


