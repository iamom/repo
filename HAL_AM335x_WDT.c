/************************************************************************
*   PROJECT:        Horizon CSD
*   COMPANY:        Pitney Bowes
*
//	FileName:	HAL_AM335x_WDT.c
//
//	Description: Hardware abstraction layer implementation for AM335x Watchdog timer initialization & control
*				Watchdog is used for reset only.
* ----------------------------------------------------------------------
*               Copyright (c) 2016 Pitney Bowes Inc.
*                        37 Executive Dr
*                        Danbury, CT 06810
* ----------------------------------------------------------------------
*
*       REVISION HISTORY:
*
*
*************************************************************************/
#include "bsp/csd_2_3_defs.h"
#include "nucleus.h"
// include types.h for the NU_Sleep prototype.
#include "types.h"


// Disable WDT
void HALDisableWDT(void)
{
	// Disable timer
    ESAL_GE_MEM_WRITE32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WSPR_OFFSET, AM335X_WATCHDOG_WSPR_DISABLE_1);

    // Wait for write to finish.
    while((ESAL_GE_MEM_READ32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WWPS_OFFSET) & AM335X_WATCHDOG_WWPS_WSPR_WRITE_PENDING) !=
    																					AM335X_WATCHDOG_WWPS_NO_WRITE_PENDING);

    ESAL_GE_MEM_WRITE32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WSPR_OFFSET, AM335X_WATCHDOG_WSPR_DISABLE_2);

    // Wait for write to finish.
    while((ESAL_GE_MEM_READ32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WWPS_OFFSET) & AM335X_WATCHDOG_WWPS_WSPR_WRITE_PENDING) !=
    																					AM335X_WATCHDOG_WWPS_NO_WRITE_PENDING);
}

// Enable WDT
void HALEnableWDT(void)
{
	// Enable timer
    ESAL_GE_MEM_WRITE32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WSPR_OFFSET, AM335X_WATCHDOG_WSPR_ENABLE_1);

    // Wait for write to finish.
    while((ESAL_GE_MEM_READ32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WWPS_OFFSET) & AM335X_WATCHDOG_WWPS_WSPR_WRITE_PENDING) !=
    																					AM335X_WATCHDOG_WWPS_NO_WRITE_PENDING);

    ESAL_GE_MEM_WRITE32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WSPR_OFFSET, AM335X_WATCHDOG_WSPR_ENABLE_2);

    // Wait for write to finish.
    while((ESAL_GE_MEM_READ32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WWPS_OFFSET) & AM335X_WATCHDOG_WWPS_WSPR_WRITE_PENDING) !=
    																					AM335X_WATCHDOG_WWPS_NO_WRITE_PENDING);
}

// Reset WDT
void HALResetWDT(void)
{
	unsigned readVal;
	// Increment the value in the trigger register
    readVal = ESAL_GE_MEM_READ32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WTGR_OFFSET);
    ESAL_GE_MEM_WRITE32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WTGR_OFFSET, ++readVal);
    // Wait for write to finish.
    while((ESAL_GE_MEM_READ32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WWPS_OFFSET) & AM335X_WATCHDOG_WWPS_WTGR_WRITE_PENDING) != AM335X_WATCHDOG_WWPS_NO_WRITE_PENDING)
    {
    	NU_Sleep(1);
    }

}

// Set up WDT
void HALInitWDT(void)
{
	unsigned readVal;

	//Set up clock, use default source of 32 kHz
    ESAL_GE_MEM_WRITE32(AM335X_CM_WKUP_BASE + AM335X_CM_WKUP_WDT1_CLKCTRL, AM335X_CM_CLKCTRL_MODULEMODEEN);

    // Wait for CM domain to settle.
    while((ESAL_GE_MEM_READ32(AM335X_CM_WKUP_BASE + AM335X_CM_WKUP_WDT1_CLKCTRL) & AM335X_CM_CLKCTRL_IDLEST) !=
                             AM335X_CM_CLKCTRL_IDLEST_FUNC);

    // Reset & configure to freeze during emulator suspension
    readVal = ESAL_GE_MEM_READ32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WDSC_OFFSET);
    ESAL_GE_MEM_WRITE32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WDSC_OFFSET, readVal | AM335X_WATCHDOG_WDSC_SOFTRESET | AM335X_WATCHDOG_WDSC_EMUFROZEN);

    ESAL_GE_MEM_WRITE32(AM335X_DEBUGSS_DRM_BASE + AM335X_DEBUGSS_DRM_WDT_OFFSET, AM335X_DEBUGSS_DRM_SUSPEND_DEF_OVERRIDE);

    // Wait for WDT to come out of reset
    while((ESAL_GE_MEM_READ32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WDSC_OFFSET) & AM335X_WATCHDOG_WDSC_SOFTRESET) !=
    		AM335X_WATCHDOG_WDSC_RESETDONE);

}


// Specify timeout value in milliseconds and start WDT
void HALStartWDT(unsigned ms_timeout)
{
	if (ms_timeout == 0) return; //invalid timeout, do nothing

    HALDisableWDT();

    // Load the timer
    // 1 ms = 32 ticks, so set prescalar to 32 to get counter to increment every ms ( 2^5 = 32 )
    ESAL_GE_MEM_WRITE32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WCLR_OFFSET, AM335X_WATCHDOG_WCLR_PRESCALAR_ENABLE | AM335X_WATCHDOG_WCLR_PTV(5) );
    // Wait for write to finish.
    while((ESAL_GE_MEM_READ32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WWPS_OFFSET) & AM335X_WATCHDOG_WWPS_WCLR_WRITE_PENDING) !=
    																					AM335X_WATCHDOG_WWPS_NO_WRITE_PENDING);

    // write to load register
    ESAL_GE_MEM_WRITE32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WLDR_OFFSET, (AM335X_WATCHDOG_WLDR_MAX_VALUE - ms_timeout) );
    // Wait for write to finish.
    while((ESAL_GE_MEM_READ32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WWPS_OFFSET) & AM335X_WATCHDOG_WWPS_WLDR_WRITE_PENDING) !=
    																					AM335X_WATCHDOG_WWPS_NO_WRITE_PENDING);

    // write to counter register just in case
    ESAL_GE_MEM_WRITE32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WCRR_OFFSET, (AM335X_WATCHDOG_WLDR_MAX_VALUE - ms_timeout) );
    // Wait for write to finish.
    while((ESAL_GE_MEM_READ32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WWPS_OFFSET) & AM335X_WATCHDOG_WWPS_WCRR_WRITE_PENDING) !=
    																					AM335X_WATCHDOG_WWPS_NO_WRITE_PENDING);

    // trigger new values taking effect
    HALResetWDT();

    HALEnableWDT();
}

void HALRebootSystem(void)
{
    // write reset value to load register
    ESAL_GE_MEM_WRITE32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WLDR_OFFSET, AM335X_WATCHDOG_WLDR_FORCE_RESET );
    // Wait for write to finish.
    while((ESAL_GE_MEM_READ32(AM335X_WATCHDOG_BASE + AM335X_WATCHDOG_WDT_WWPS_OFFSET) & AM335X_WATCHDOG_WWPS_WLDR_WRITE_PENDING) !=
    																					AM335X_WATCHDOG_WWPS_NO_WRITE_PENDING);
    // trigger new value taking effect
    HALResetWDT();
    while (1) ; // reset should be in one functional clock: ~ 30 us

}

