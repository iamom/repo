//=====================================================================
//
//	FileName:	HAL_PSOC.c
//
//	Description: Hardware abstraction layer implementation for PSOC interface
//               that is not board specific.
//=====================================================================
#include "HAL_PSOC.h"
#include "HAL_FPGA.h"
#include "am335x_pbdefs.h"
#include "nucleus_gen_cfg.h"
#include "bsp/csd_2_3_defs.h"
#include "nucleus.h"
#include "commontypes.h"
#include "hal.h"

extern void HALInitGPIOPins(P_GPIO_INFO pGPIOTable, unsigned numPinsUsed);
extern void setGPIOPin(GPIO_INFO pinInfo, unsigned char value);
extern void setDirGPIOPin(GPIO_INFO pinInfo, unsigned char direction);
extern unsigned char readGPIOPin(GPIO_INFO pinInfo);
extern void  HALDelayTimerTicks (UINT32 no_of_ticks);

// The order of the entries in the following table must match the order of the entries in
// the GPIO_INFO enum in HAL_PSOC.h
// After a reset, a pin configured for output will output a 0.

#ifdef CFG_NU_BSP_BEAGLEBONE_BLACK_ENABLE
// *** indicates different pin from CSD
static GPIO_INFO BBB_GPIO_Table[] = {
							{AM335X_CTRL_PADCONF_GPMC_AD9, AM335X_GPIO_BIT_23, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //***PGM_DATA_DIR
							{AM335X_CTRL_PADCONF_GPMC_AD10, AM335X_GPIO_BIT_26, GPIO_BANK_0, DIR_INPUT, GPIO_PULL_DOWN}, //***PGM_DATA_PH - in/out
							{AM335X_CTRL_PADCONF_GPMC_AD11, AM335X_GPIO_BIT_27, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //***PGM_CLK_PH
							{AM335X_CTRL_PADCONF_LCD_DATA13, AM335X_GPIO_BIT_9, GPIO_BANK_0, DIR_INPUT, GPIO_PULL_DOWN}, //***PGM_DATA_MCB - in/out
							{AM335X_CTRL_PADCONF_GPMC_AD8, AM335X_GPIO_BIT_22, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //***PGM_CLK_MCB
							{AM335X_CTRL_PADCONF_SPI0_SCLK, AM335X_GPIO_BIT_2, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //SYNC_CLK
							{AM335X_CTRL_PADCONF_UART1_TXD, AM335X_GPIO_BIT_15, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //SYNC_TX_SEL
							{AM335X_CTRL_PADCONF_SPI0_D0, AM335X_GPIO_BIT_3, GPIO_BANK_0, DIR_INPUT, GPIO_PULL_UP}, //SYNC_TX
							{AM335X_CTRL_PADCONF_SPI0_D1, AM335X_GPIO_BIT_4, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //SYNC_RX
							{AM335X_CTRL_PADCONF_LCD_DATA14, AM335X_GPIO_BIT_10, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //***RESET_MCB_LFSR
							{AM335X_CTRL_PADCONF_UART1_RTSN, AM335X_GPIO_BIT_13, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //SYNC_CS_MCB
							{AM335X_CTRL_PADCONF_ECAP0_IN_PWM0_OUT, AM335X_GPIO_BIT_7, GPIO_BANK_0, DIR_INPUT, GPIO_PULL_UP}, //STATUS_MCB
							{AM335X_CTRL_PADCONF_LCD_DATA15, AM335X_GPIO_BIT_11, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //***RESET_PH_LFSR
							{AM335X_CTRL_PADCONF_UART1_RXD, AM335X_GPIO_BIT_14, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //SYNC_CS_PH
							{AM335X_CTRL_PADCONF_UART1_CTSN, AM335X_GPIO_BIT_12, GPIO_BANK_0, DIR_INPUT, GPIO_PULL_UP} //STATUS_PH
};
static const unsigned numPinsUsed = sizeof(BBB_GPIO_Table) / sizeof(GPIO_INFO);
static P_GPIO_INFO pGPIOTable = &BBB_GPIO_Table[0];

#else
// CSD - pins should match schematic
static GPIO_INFO CSD_GPIO_Table[] = {
		{AM335X_CTRL_PADCONF_GPMC_A11, AM335X_GPIO_BIT_27, GPIO_BANK_1, DIR_OUTPUT, GPIO_PULL_OFF}, //PGM_DATA_DIR
		{AM335X_CTRL_PADCONF_GPMC_A3, AM335X_GPIO_BIT_19, GPIO_BANK_1, DIR_INPUT, GPIO_PULL_DOWN}, //PGM_DATA_PH - in/out
		{AM335X_CTRL_PADCONF_GPMC_A1, AM335X_GPIO_BIT_17, GPIO_BANK_1, DIR_OUTPUT, GPIO_PULL_OFF}, //PGM_CLK_PH
		{AM335X_CTRL_PADCONF_GPMC_A2, AM335X_GPIO_BIT_18, GPIO_BANK_1, DIR_INPUT, GPIO_PULL_DOWN}, //PGM_DATA_MCB - in/out
		{AM335X_CTRL_PADCONF_GPMC_A0, AM335X_GPIO_BIT_16, GPIO_BANK_1, DIR_OUTPUT, GPIO_PULL_OFF}, //PGM_CLK_MCB
		{AM335X_CTRL_PADCONF_SPI0_SCLK, AM335X_GPIO_BIT_2, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //SYNC_CLK
		{AM335X_CTRL_PADCONF_UART1_TXD, AM335X_GPIO_BIT_15, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //SYNC_TX_SEL
		{AM335X_CTRL_PADCONF_SPI0_D0, AM335X_GPIO_BIT_3, GPIO_BANK_0, DIR_INPUT, GPIO_PULL_UP}, //SYNC_TX
		{AM335X_CTRL_PADCONF_SPI0_D1, AM335X_GPIO_BIT_4, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //SYNC_RX
		{AM335X_CTRL_PADCONF_MII1_TXD3, AM335X_GPIO_BIT_16, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //RESET_MCB_LFSR
		{AM335X_CTRL_PADCONF_UART1_RTSN, AM335X_GPIO_BIT_13, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //SYNC_CS_MCB
		{AM335X_CTRL_PADCONF_SPI0_CS0, AM335X_GPIO_BIT_5, GPIO_BANK_0, DIR_INPUT, GPIO_PULL_UP}, //STATUS_MCB
		{AM335X_CTRL_PADCONF_MII1_TXD2, AM335X_GPIO_BIT_17, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //RESET_PH_LFSR
		{AM335X_CTRL_PADCONF_UART1_RXD, AM335X_GPIO_BIT_14, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_OFF}, //SYNC_CS_PH
		{AM335X_CTRL_PADCONF_SPI0_CS1, AM335X_GPIO_BIT_6, GPIO_BANK_0, DIR_INPUT, GPIO_PULL_UP} //STATUS_PH
};
static const unsigned numPinsUsed = sizeof(CSD_GPIO_Table) / sizeof(GPIO_INFO);
static P_GPIO_INFO pGPIOTable = &CSD_GPIO_Table[0];

#endif

const PSOC_INT perPSOCInterface[] = {
		// order must match that of PH_PSOC and MCB_PSOC in hal.h
		{PSOC_IDX_PGM_DATA_PH, PSOC_IDX_PGM_CLK_PH, PSOC_IDX_RESET_PH_LFSR, PSOC_IDX_SYNC_CS_PH, PSOC_IDX_STATUS_PH},
		{PSOC_IDX_PGM_DATA_MCB, PSOC_IDX_PGM_CLK_MCB, PSOC_IDX_RESET_MCB_LFSR, PSOC_IDX_SYNC_CS_MCB, PSOC_IDX_STATUS_MCB}
};

// Output pins are set to output
// Input pins are set to input
// In/out pins are set to input (default state)
void HALInitPSOCInterface()
{

	HALInitGPIOPins(pGPIOTable, numPinsUsed);

}

void HALResetLFSR(unsigned char psocId)
{
	unsigned int psocIndex = perPSOCInterface[psocId].reset;

	setGPIOPin(*(pGPIOTable + psocIndex), 1);
	ESAL_PR_Delay_USec( PSOC_XRES_CLK_DELAY );
	setGPIOPin(*(pGPIOTable + psocIndex), 0);
	ESAL_PR_Delay_USec( PSOC_POST_XRES_DELAY ); // need small delay after reset
}

void HALResetMCBAndPHLFSR()
{

	setGPIOPin(*(pGPIOTable + PSOC_IDX_RESET_PH_LFSR), 1);
	setGPIOPin(*(pGPIOTable + PSOC_IDX_RESET_MCB_LFSR), 1);
	ESAL_PR_Delay_USec( PSOC_XRES_CLK_DELAY );
	setGPIOPin(*(pGPIOTable + PSOC_IDX_RESET_MCB_LFSR), 0);
	setGPIOPin(*(pGPIOTable + PSOC_IDX_RESET_PH_LFSR), 0);
	ESAL_PR_Delay_USec( PSOC_POST_XRES_DELAY ); // need small delay after reset

}

// Set PGM_DATA for output -
// a) set transceiver chip direction (0 = output)
// b) set GPIO direction
// Note order matters
void HALSetPDATAOutput(unsigned char psocId)
{
	unsigned int psocIndex = perPSOCInterface[psocId].pgm_data;

	setGPIOPin(*(pGPIOTable + PSOC_IDX_PGM_DATA_DIR), TRANSCEIVER_DIR_OUTPUT);
	setDirGPIOPin(*(pGPIOTable + psocIndex), DIR_OUTPUT);

}

// Set PGM_DATA for input -
// a) set GPIO direction
// b) set transceiver chip direction (0 = output)
// Note order matters
void HALSetPDATAInput(unsigned char psocId)
{
	unsigned int psocIndex = perPSOCInterface[psocId].pgm_data;

	setDirGPIOPin(*(pGPIOTable + psocIndex), DIR_INPUT);
	setGPIOPin(*(pGPIOTable + PSOC_IDX_PGM_DATA_DIR), TRANSCEIVER_DIR_INPUT);

}

void HALSetPDATA(unsigned char psocId, unsigned char value)
{
	unsigned int psocIndex = perPSOCInterface[psocId].pgm_data;

	setGPIOPin(*(pGPIOTable + psocIndex), value);
}

unsigned char HALReadPDATA(unsigned char psocId)
{
	unsigned int psocIndex = perPSOCInterface[psocId].pgm_data;

	return readGPIOPin(*(pGPIOTable + psocIndex));
}

void HALSetPCLK(unsigned char psocId, unsigned char value)
{
	unsigned int psocIndex = perPSOCInterface[psocId].pgm_clock;

	setGPIOPin(*(pGPIOTable + psocIndex), value);
}

// Includes delay after value is set to enforce max 8 MHz clock
void HALSetPCLKwDelay(unsigned char psocId, unsigned char value)
{
	HALSetPCLK(psocId, value);
//TODO - In debugger this is still slow - confirm release code outside debugger is fast
// Max 8 MHz clock = 63 ns delay per level
//	ESAL_PR_Delay_USec( PSOC_500KHZ_CLK_DELAY ); //slow clock
	HALDelayTimerTicks(PSOC_6MHZ_CLK_DELAY); // 83 ns delay
}

// Includes delay after value is set to enforce max 8 MHz clock
void HALRunPCLK(unsigned char psocId, unsigned char direction, unsigned numCycles)
{
    int curCycle;

    for(curCycle=0; curCycle < numCycles; curCycle++)
    {
    	if (direction == PSOC_CLK_LOW_TO_HIGH)
    	{
        	HALSetPCLKwDelay(psocId, 0);
        	HALSetPCLKwDelay(psocId, 1);
    	}
    	else
    	{
        	HALSetPCLKwDelay(psocId, 1);
        	HALSetPCLKwDelay(psocId, 0);
    	}
    }

}

#if 0
// G900 code for comparison
//  if(powerOn == TRUE)
//    PVDR |= 0x1;
//  else
//    PVDR &= ~0x1;
#endif
void HALSetPHPowerState(unsigned char powerOn)
{
	// CSD FPGA does not support read of this signal, so this stores the state of the signal
	if (powerOn)
	{
	    ESAL_GE_MEM_WRITE16(CSD_FPGA_BASE + CARRIAGE_5V_EN_REG, 1);
	}
	else
	{
	    ESAL_GE_MEM_WRITE16(CSD_FPGA_BASE + CARRIAGE_5V_EN_REG, 0);
	}
}

#if 0
// G900 code for comparison
//  if(PVDR & 0x1)
#endif
unsigned char HALGetPHPowerState(void)
{
	unsigned short readVal;

    readVal = ESAL_GE_MEM_READ16(CSD_FPGA_BASE + CARRIAGE_5V_EN_REG);
	return ((readVal & POWER_ON) == POWER_ON);
}

//TODO - Add error checking
unsigned char HALReadPBit(unsigned char psocId)
{
	HALRunPCLK(psocId, PSOC_CLK_LOW_TO_HIGH, 1);

    if (HALReadPDATA(psocId)) {
        return(1);
    }
    else {
        return(0);
    }
}

unsigned char HALReadPByte(unsigned char psocId)
{
    unsigned char b;
    unsigned char bCurrByte = 0x00;

    for (b=0; b<8; b++) {
        bCurrByte = (bCurrByte<<1) + HALReadPBit(psocId);
    }
    return(bCurrByte);
}



#if 0
// G900 code for comparison
  nv = *phClkDataPort;
  nv &= ~phPortClkBit;
  *phClkDataPort = nv;
  *phClkDataPort & ~( phPortDataBit );
#endif
void HALSetFinalPSOCState(void)
{
  // Above G900 code sets CLK and DATA pins low for PH only, not MCB;
  // For CSD, when not in use, DATA pin is an input and CLK pin is low by virtue of
  // last access is always a write and a write always ends with a low CLK.
  // So no need to do anything for CLK and DATA pins of either PSOC
}

void HALSendWord(unsigned char psocId, unsigned long currentWord, unsigned long numBits)
{
    unsigned long curBit = 0;

    for(curBit=0; curBit < numBits; curBit++) {

		if (currentWord & 0x80000000)
            // Send a '1'
			HALSetPDATA(psocId, 1);
        else
            // Send a '0'
        	HALSetPDATA(psocId, 0);

		HALRunPCLK(psocId, PSOC_CLK_HIGH_TO_LOW, 1);

        currentWord = currentWord << 1;
    }

}

#if 0
// G900 code for comparison purposes

// Port bit definitions
//This configuration has MCB_PGM_DATA as an output
#define PMCR_V                         0x4C55
//This configuration has MCB_PGM_DATA as an input
#define PMCR2_V                        0x4C75

//PORT M bit definitions
#define MCB_RESET_LFSR                 0x01
#define MCB_SYNC_CS                    0x02
#define MCB_PGM_DATA                   0x04
#define MCB_PGM_CLK                    0x08
#define MCB_STATUS                     0x20
#define SYNC_TX_SEL                    0x80

//Port k Definitions
// Port bit defintions
//This configuration has PH_PGM_DATA as an output
#define PKCR_V                         0x0097
//This configuration has PH_PGM_DATA as an input
#define PKCR2_V                        0x009f

//Port S Definitions
// Port bit defintions
//This configuration has PH_PGM_DATA as an output
#define PSCR_V                         0x015D
//This configuration has PH_PGM_DATA as an input
#define PSCR2_V                        0x01DD

#define PSDRPORT ((volatile unsigned char *)0xA405015C)
#define PKDRPORT ((volatile unsigned char *)0xA4050152)

#define PH_STATUS                      0x01
#define PH_PGM_DATA                    0x02
#define PH_PGM_CLK                     0x04
#define MT2_PH_PGM_CLK                 0x10
#define MT2_PH_PGM_DATA                0x08
//Port U Definitions

#define PH_RESET_LFSR                  0x01
#define PH_SYNC_CS                     0x02
#define PGM_DATA_DIR                   0x04

//Port S Definitions
#define PSOC_SYNC_CLK                  0x01
#define PSOC_SYNC_TX                   0x02 //This is data transmitted by the PSOC so this is an Input
#define PSOC_SYNC_RX                   0x04


void HALWritePSOCVector(unsigned char psocId, unsigned long *vecInfo_p, unsigned vecSize, unsigned long vecMask, unsigned char numBits, unsigned char resetChip)
{

  unsigned int	j, vecData;
  unsigned char	i, x, psocData, nv, k,dummyCh;
  volatile unsigned char *pDummy = ( volatile unsigned char * ) SYS_CMOS_BASE;

  // ********************************* CODE ***********************************

  if(psocId == MCB_PSOC)
    {
      if (resetChip)
	{
	  PMDR |= MCB_RESET_LFSR;
	  OSWakeAfter( 20 );		// LEAVE PIN HIGH FOR 20 MILLISECONDS
	  PMDR &= ~MCB_RESET_LFSR;
	  for( k = 0 ; k < READSAFTERRESET ; k++ )
	    dummyCh = *pDummy;
	}
      nv = PMDR & ~( MCB_PGM_DATA );
      for (i = 0; i < vecSize; i++, vecInfo_p++)
	{
	  vecData = *vecInfo_p;
	  for (x = 0; x < numBits; x++)
	    {
	      if( i == ( vecSize - 1 ) )
		{
		  if(vecMask == 0 )
		    break;
		}
	      if (vecData & 0x80000000)
		nv |= MCB_PGM_DATA;
	      else
		nv &= ~MCB_PGM_DATA;
	      nv |= MCB_PGM_CLK;
	      PMDR = nv;
	      vecData <<= 1;
	      if( i == ( vecSize - 1 ))
		vecMask <<= 1;
	      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
		dummyCh = *pDummy;
	      nv &= ~MCB_PGM_CLK;
	      PMDR  = nv;
	      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
		dummyCh = *pDummy;
	    }
	}
      nv = PMDR;
      nv &= ~MCB_PGM_DATA;
      nv  &= ~MCB_PGM_CLK;
      PMDR  = nv;
    }
  else
    {
      if (resetChip)
	{
	  PUDR |= PH_RESET_LFSR;
	  OSWakeAfter( 20 );		// LEAVE PIN HIGH FOR 20 MILLISECONDS
	  PUDR &= ~PH_RESET_LFSR;
	  for( k = 0 ; k < READSAFTERRESET ; k++ )
	    dummyCh = *pDummy;
	}
      nv = *phClkDataPort & ~( phPortDataBit );
      for (i = 0; i < vecSize; i++, vecInfo_p++)
	{
	  vecData = *vecInfo_p;
	  for (x = 0; x < numBits; x++)
	    {
	      if( i == ( vecSize - 1 ) )
		{
		  if(vecMask == 0 )
		    break;
		}
	      if (vecData & 0x80000000)
		nv |= phPortDataBit;
	      else
		nv &= ~phPortDataBit;
	      nv |= phPortClkBit;
	      *phClkDataPort = nv;
	      vecData <<= 1;
	      if( i == ( vecSize - 1 ))
		vecMask <<= 1;
	      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
		dummyCh = *pDummy;
	      nv &= ~phPortClkBit;
	      *phClkDataPort = nv;
	      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
		dummyCh = *pDummy;
	    }
	}
      nv = *phClkDataPort;
      nv &= ~phPortDataBit;
      nv &= ~phPortClkBit;
      *phClkDataPort = nv;
    }

}
#endif

void HALSendVector(unsigned char psocId, unsigned long *vecInfo_p, unsigned long numBits)
{
    while(numBits >= 32) {
    	HALSendWord(psocId, *(vecInfo_p), 32);
        numBits -= 32;
        vecInfo_p++;
    }

	if(numBits > 0)
		HALSendWord(psocId, *(vecInfo_p), numBits);

}

void HALWritePSOCVector(unsigned char psocId, unsigned long *vecInfo_p, unsigned long numBits, BOOL resetMode)
{
	int old_level;

	HALSetPDATAOutput(psocId);

	if (resetMode){
		//disable interrupts in this block so that first 9 bits are sent within 125 us of reset
	    old_level = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
		HALResetLFSR(psocId);
		HALSendVector(psocId, vecInfo_p, numBits);
	    NU_Local_Control_Interrupts(old_level);
	}
	else
	{
		HALSendVector(psocId, vecInfo_p, numBits);
	}

	// TODO - Set PDATA to write low before turning to input so that next time it turns
	// to output it writes low and prevent pulse if the last bit was high here?
	// HALSetPDATA(psocId, 0);
	HALSetPDATAInput(psocId);

}


#if 0
// G900 code for comparison purposes
int WaitForHighToLow( unsigned char psocId )
{
  unsigned char		sdataMask,giveUp,dummyCh;
  unsigned short;
  int	i, x , k;
  unsigned char portDataI;
  unsigned char portDataO;
  unsigned char nv;
  int err = -1;
  volatile unsigned char *pDummy = ( volatile unsigned char * ) SYS_CMOS_BASE;

  /********************************* CODE ***********************************/
  FireUpPsocTimer();

  giveUp = 0;
  if(psocId == MCB_PSOC)
    {
      PUDR |= PGM_DATA_DIR;
      for( x = 0 ; x < SERIALDUMMYREADS ; x++ )
	dummyCh = *pDummy;
      //Reconfigure port
      PMCR = PMCR2_V;
      PMDR &= ~MCB_PGM_CLK ;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      PMDR |= MCB_PGM_CLK;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      PMDR &= ~MCB_PGM_CLK;
      while(1)
	{
	  //First wait for a low to high transition
	  portDataI = PMDR;
	  if((portDataI & MCB_PGM_DATA) != MCB_PGM_DATA)
	    {
	      if (giveUpWaiting)
		{
		  giveUp++;
		  break;
		}
	    }
	  else
	    {
	      for ( x = 1 ; x < 5 ; x++)
		;
	      break;
	    }
	}
      ResetPsocTimer();
      while (1)
	{
	  portDataO = PMDR;
	  if( (portDataO & MCB_PGM_DATA) == MCB_PGM_DATA)
	    {
	      if(giveUpWaiting)
		{
		  giveUp++;
		  PUDR &= ~PGM_DATA_DIR;
		  PMCR = PMCR_V;
		  PMDR &= ~MCB_PGM_CLK;
		  break;
		}
	    }
	  else
	    {
	      for( x = 1 ; x < 5 ; x++)
		;
	      PMCR = PMCR_V;
	      PUDR &= ~PGM_DATA_DIR;
	      nv = PMDR;
	      nv &= ~MCB_PGM_DATA;
	      for (x = 0; x < IDLEBITSAFTERHIGHTTOLOW; x++)
		{
		  nv |= MCB_PGM_CLK;
		  PMDR |= nv;
		  for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
		    dummyCh = *pDummy;
		  nv &= ~MCB_PGM_CLK;
		  PMDR  = nv;
		  for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
		    dummyCh = *pDummy;
		}
	      err = 0;
	      break;
	    }
	}
      pLog[ pLogIndex ].dev = psocId;
      pLog[ pLogIndex ].portI = portDataI & MCB_PGM_DATA;
      pLog[ pLogIndex ].portO = portDataO & MCB_PGM_DATA;
      pLog[ pLogIndex ].giveUp = giveUp;
      pLogIndex++;
      if(pLogIndex >= MAXPLOG )
	pLogIndex = 0;
    }
  else
    {
      PUDR |= PGM_DATA_DIR;
      for( x = 0 ; x < SERIALDUMMYREADS ; x++ )
	dummyCh = *pDummy;
      //Reconfigure port
      *phClkDataCtrl = phDataInputValue;
      *phClkDataPort &= ~phPortClkBit;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      *phClkDataPort |= phPortClkBit;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      *phClkDataPort &= ~phPortClkBit;
      while(1)
	{
	  //First wait for a low to high transition
	  portDataI = *phClkDataPort;
	  if((portDataI & phPortDataBit) != phPortDataBit)
	    {
	      if (giveUpWaiting)
		{
		  giveUp++;
		  break;
		}
	    }
	  else
	    {
	      for ( x = 1 ; x < 5 ; x++)
		;
	      break;
	    }
	}
      ResetPsocTimer();
      while(1)
	{
	  portDataO = *phClkDataPort;
	  if((portDataO & phPortDataBit) == phPortDataBit)
	    {
	      if (giveUpWaiting)
		{
		  giveUp++;
		  PUDR &= ~PGM_DATA_DIR;
		  *phClkDataCtrl = phDataOutputValue;
		  *phClkDataPort &= ~phPortClkBit;
		  break;
		}
	    }
	  else
	    {
	      for( x = 1 ; x < 5 ; x++)
		;
	      *phClkDataCtrl = phDataOutputValue;
	      PUDR &= ~PGM_DATA_DIR;
	      nv = *phClkDataPort;
	      nv &= ~phPortDataBit;
	      for (x = 0; x < IDLEBITSAFTERHIGHTTOLOW; x++)
		{
		  nv |= phPortClkBit;
		  *phClkDataPort |= nv;
		  for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
		    dummyCh = *pDummy;
		  nv &= ~phPortClkBit;
		  *phClkDataPort  = nv;
		  for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
		    dummyCh = *pDummy;
		}
	      err = 0;
	      break;
	    }
	}
      pLog[ pLogIndex ].dev = psocId;
      pLog[ pLogIndex ].portI = portDataI & phPortDataBit;
      pLog[ pLogIndex ].portO = portDataO & phPortDataBit;
      pLog[ pLogIndex ].giveUp = giveUp;
      pLogIndex++;
      if(pLogIndex >= MAXPLOG )
	pLogIndex = 0;
    }
  ShutOffPsocTimer();
  return(err);
}
#endif

int HALPSOCWaitForHighToLow(unsigned char psocId)
{
	int retVal = -1; //Success = 0; Failure = -1
    unsigned int timeoutPolls;

//default configuration is input - 	HALSetPDATAInput(MCB_PSOC);
    // Assume direction was changed from output back to input prior to this call, so wait a while
	ESAL_PR_Delay_USec( PSOC_POST_DIR_CHG_DELAY );
	//Confirm can read OK
	if (HALReadPDATA(psocId) == GPIO_ERROR)
    {//cleanup and exit
		//default configuration is input - 		HALSetPDATAOutput(MCB_PSOC);
		return(retVal);
    }

	//Send one clock
	HALRunPCLK(psocId, PSOC_CLK_HIGH_TO_LOW, 1);

	// wait for PDATA to go high
    timeoutPolls = PSOC_PDATA_TIMEOUT_POLLS;
	while (1)
	{
		if (HALReadPDATA(psocId))
			break;
		ESAL_PR_Delay_USec( PSOC_PDATA_POLL_DELAY );

		// If the wait is too long then timeout
        if (timeoutPolls-- == 0)
        {// Cypress sample says to return an error here but G900 code just continues
         // to next wait, assuming pulse occurred but was missed
        	break;
        }
	}

	// wait for PDATA to go low
    timeoutPolls = PSOC_PDATA_TIMEOUT_POLLS;
	while (1)
	{
		if (!HALReadPDATA(psocId))
			break;
		ESAL_PR_Delay_USec( PSOC_PDATA_POLL_DELAY );

		// If the wait is too long then timeout
        if (timeoutPolls-- == 0)
        {// cleanup  and exit - G900 code also sets PGM_CLK low, but it is already low here
        	//default configuration is input -     		HALSetPDATAOutput(MCB_PSOC);
			return(retVal);
        }
	}

	// clock in the 40 zeros
	HALWritePSOCVector(psocId, WaitNPollEndVec, numBitsWaitNPollEndVec, FALSE);
	return(0);
}

#if 0
// G900 code for comparison purposes
unsigned char ReadPsocByte( unsigned char psocId, unsigned long vec ,  unsigned char nZeroBits)
{
	int		i;
  unsigned char	b=0,msk, sdata, k,dummyCh,x,nv;
  unsigned short	j;
  unsigned long vecData;
  volatile unsigned char *pDummy = ( volatile unsigned char * ) SYS_CMOS_BASE;


  msk = 0x80;
  if(psocId == MCB_PSOC)
    {
      //First we clock in the 11 bits
      vecData = vec;
      nv = PMDR;
      for (x = 0; x < 11 ; x++)
	{
	  if (vecData & 0x80000000)
	    nv |= MCB_PGM_DATA;
	  else
	    nv &= ~MCB_PGM_DATA;
	  nv |= MCB_PGM_CLK;
	  PMDR = nv;
	  vecData <<= 1;
	  for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	    dummyCh = *pDummy;
	  nv &= ~MCB_PGM_CLK;
	  PMDR  = nv;
	  for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	    dummyCh = *pDummy;
	}
      //Reconfigure port
      //Clock in a single Z
      PMCR = PMCR2_V;
      PUDR |= PGM_DATA_DIR;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      PMDR |= MCB_PGM_CLK;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      PMDR &= ~MCB_PGM_CLK;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      //Clock reading the data on the rising edge
      // READ THE VALUE
      for (i = 0; i < 8 ; i++)
	{
	  PMDR |= MCB_PGM_CLK;
	  for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	    dummyCh = *pDummy;
	  PMDR &= ~MCB_PGM_CLK;
	  for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	    dummyCh = *pDummy;
	  if (PMDR & MCB_PGM_DATA)
	    b = b |= msk;
	  msk >>= 1;
	}
      PUDR &= ~PGM_DATA_DIR;
      PMCR = PMCR_V;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      PMDR |= MCB_PGM_CLK;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      PMDR &= ~MCB_PGM_CLK;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      nv |= MCB_PGM_DATA;
      nv |= MCB_PGM_CLK;
      PMDR = nv;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      nv &= ~MCB_PGM_CLK;
      PMDR  = nv;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      nv &= ~MCB_PGM_DATA;
      PMDR  = nv;
    }
  else
    {
      //First we clock in the 11 bits
      vecData = vec;
      nv = *phClkDataPort;
      for (x = 0; x < 11 ; x++)
	{
	  if (vecData & 0x80000000)
	    nv |= phPortDataBit;
	  else
	    nv &= ~phPortDataBit;
	  nv |= phPortClkBit;
	  *phClkDataPort = nv;
	  vecData <<= 1;
	  for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	    dummyCh = *pDummy;
	  nv &= ~phPortClkBit;
	  *phClkDataPort  = nv;
	  for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	    dummyCh = *pDummy;
	}
      //Reconfigure port
      //Clock in a single Z
      *phClkDataCtrl = phDataInputValue;
      PUDR |= PGM_DATA_DIR;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      *phClkDataPort |= phPortClkBit;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      *phClkDataPort &= ~phPortClkBit;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      //Clock reading the data on the rising edge
      // READ THE VALUE
      for (i = 0 ; i < 8 ; i++)
	{
	  *phClkDataPort |= phPortClkBit;
	  for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	    dummyCh = *pDummy;
	  *phClkDataPort &= ~phPortClkBit;
	  for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	    dummyCh = *pDummy;
	  if (*phClkDataPort & phPortDataBit)
	    b = b |= msk;
	  msk >>= 1;
	}
      PUDR &= ~PGM_DATA_DIR;
      *phClkDataCtrl = phDataOutputValue;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      *phClkDataPort |= phPortClkBit;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      *phClkDataPort &= ~phPortClkBit;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      nv |= phPortDataBit;
      nv |= phPortClkBit;
      *phClkDataPort = nv;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      nv &= ~phPortClkBit;
      *phClkDataPort  = nv;
      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
	dummyCh = *pDummy;
      nv &= ~phPortDataBit;
      *phClkDataPort  = nv;
    }
#endif
unsigned char HALReadPSOCData(unsigned char psocId, unsigned long vecInfo, unsigned long  numBits)
{//Send Read ID vector and get Target ID
	unsigned char retVal;

	if (numBits > 32)
	{
		return GPIO_ERROR;
	}
	HALWritePSOCVector(psocId, &vecInfo, numBits, FALSE);
	HALRunPCLK(psocId, PSOC_CLK_LOW_TO_HIGH, 2);                    // Two SCLK cycles between write & read
    retVal = HALReadPByte(psocId);
	HALRunPCLK(psocId, PSOC_CLK_LOW_TO_HIGH, 1);
    return retVal;
}

// PSOC Communication Interface


void HALSetSyncTXSel(unsigned char value)
{
	setGPIOPin(*(pGPIOTable + PSOC_IDX_SYNC_TX_SEL), value);
}

unsigned char HALReadSyncTX(void)
{
	return readGPIOPin(*(pGPIOTable + PSOC_IDX_SYNC_TX));
}

void HALSetSyncCS(unsigned char psocId, unsigned char value)
{
	unsigned int psocIndex = perPSOCInterface[psocId].sync_cs;

	setGPIOPin(*(pGPIOTable + psocIndex), value);
}

void HALSetSyncRX(unsigned char value)
{
	setGPIOPin(*(pGPIOTable + PSOC_IDX_SYNC_RX), value);
}

void HALSetSyncCLK(unsigned char value)
{
	setGPIOPin(*(pGPIOTable + PSOC_IDX_SYNC_CLK), value);
}


void HALSetSyncCLKwDelay(unsigned char value)
{
	HALSetSyncCLK(value);
//TODO - Speed up all clocks in here - up to 8 MHz?
	ESAL_PR_Delay_USec( PSOC_500KHZ_CLK_DELAY ); //slow clock

}

void HALRunSyncCLK(unsigned char direction, unsigned numCycles)
{
	unsigned curCycle;

    for(curCycle=0; curCycle < numCycles; curCycle++)
    {
    	if (direction == PSOC_CLK_LOW_TO_HIGH)
    	{
    		HALSetSyncCLKwDelay(0);
    		HALSetSyncCLKwDelay(1);
    	}
    	else
    	{
    		HALSetSyncCLKwDelay(1);
    		HALSetSyncCLKwDelay(0);
    	}
    }
}


#if 0
void WriteSerialPort( unsigned char psocId , unsigned char ch)
{
  unsigned char i,j,k,dummyCh;
  unsigned char bitOut,msk;
  unsigned short byte;

  volatile unsigned char *pDummy = ( volatile unsigned char * ) SYS_CMOS_BASE;

  if(psocId == MCB_PSOC)
    PMDR &= ~MCB_SYNC_CS;
  else
    PUDR &= ~PH_SYNC_CS;
  byte = 0;
  msk = 0x1; // we transmit LSB first
  for( i = 0 ; i < SERIALBITSPERBYTE ; i++ )
    {
      if( i == 0 )
	{
	  bitOut = 0;
	}
      else
	{
	  if(i == (SERIALBITSPERBYTE - 1))
	    bitOut = PSOC_SYNC_RX;
	  else
	    {
	      if( ch & msk )
		bitOut = PSOC_SYNC_RX;
	      else
		bitOut = 0;
	      msk <<= 1;
	    }
	}
	  if(bitOut)
	    PSDR |= PSOC_SYNC_RX;
	  else
	    PSDR &= ~PSOC_SYNC_RX;
	  for( j = 0 ; j < SERIALCLKSPERBIT ; j++ )
	    {
	      PSDR |= PSOC_SYNC_CLK;
	      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
		dummyCh = *pDummy;
	      PSDR &= ~PSOC_SYNC_CLK;
	      for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
		dummyCh = *pDummy;
	    }
    }
  PMDR |= MCB_SYNC_CS;
  PUDR |= PH_SYNC_CS;
  PSDR |= PSOC_SYNC_RX;
#endif

void HALWriteSyncRXBit(unsigned char value)
{
	HALSetSyncRX(value);
	HALRunSyncCLK(PSOC_CLK_HIGH_TO_LOW, PSOC_SYNC_CLKS_PER_BIT);
}

void HALWriteSyncRXByte(unsigned char ch)
{
  	unsigned long curBit = 0;

  	for(curBit=0; curBit < 8; curBit++)
  	{// transmit LSB first
  		if (ch & 0x01)
  			// Send a '1'
  			HALWriteSyncRXBit(1);
  		else
  			// Send a '0'
  			HALWriteSyncRXBit(0);

  		ch = ch >> 1;
  	}
}

void HALWritePSOCSerialData(unsigned char psocId , unsigned char ch)
{
	HALSetSyncCS(psocId, 0);
	HALWriteSyncRXBit(0); //start bit
	HALWriteSyncRXByte(ch);
	HALWriteSyncRXBit(1); //stop bit
	HALSetSyncCS(psocId, 1);
	HALSetSyncRX(1);

}

#if 0
unsigned short ReadSerialPort( unsigned char psocId )
{
  unsigned char i,j,k,dummyCh,edgeDetected,startDetected;
  unsigned char seen1,bitRead,msk = 1;
  unsigned short byte = 0;
  volatile unsigned char *pDummy =  ( volatile unsigned char * ) SYS_CMOS_BASE;

      if(psocId == MCB_PSOC)
	PMDR |= SYNC_TX_SEL;
      else
	PMDR &= ~SYNC_TX_SEL;
      if(psocId == MCB_PSOC)
	PMDR &= ~MCB_SYNC_CS;
      else
	PUDR &= ~PH_SYNC_CS;
      byte = 0;
      i = 0 ;
      startDetected = 0;
      edgeDetected = 0;
      while( i < SERIALBITSPERBYTE && giveUpWaiting == 0 )
	{ //while i < SERIALBITSPERBYTE
	  seen1 = 0;
	  j = 0;
	  while( j < SERIALCLKSPERBIT )
	    { //while( j < SERIALCLKSPERBIT )
		  PSDR |= PSOC_SYNC_CLK;
		  for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
		    dummyCh = *pDummy;
		  if(edgeDetected == 0 )
		    {
		      if( ( PSDR & PSOC_SYNC_TX ) == 0 )
				{
				  edgeDetected = 1;
				  j = 1;
				}
		      else
				{
				  if (giveUpWaiting)
					{
					  //No Start Bit
					  byte |= 0xfe00;
					  break;
					}
				}
		    }
		  else
		    {
		      if( j == 2 )
				{
				  if(PSDR & PSOC_SYNC_TX)
					seen1++;
				}
		      else
				{
				  if( j == 4 )
					{
					  if(PSDR & PSOC_SYNC_TX)
					seen1++;
					}
				  else
					{
					  if( j == 6 )
						{
						  if(PSDR & PSOC_SYNC_TX)
							seen1++;
						}
					}
				}
		      j++;
		    }
		  PSDR &= ~PSOC_SYNC_CLK;
		  for( k = 0 ; k < SERIALDUMMYREADS ; k++ )
		    dummyCh = *pDummy;
	    } //while j
	  if(giveUpWaiting)
	    break;
	  if(seen1 >= 2 )
	    bitRead = 1;
	  else
	    bitRead = 0;

	  pT = &PSOCRecv[ PSOCRecvCnt ];
	  pT->startDetected = startDetected;
	  pT->bitRead = bitRead;
	  pT->i = i;
	  pT->j = j;
	  pT->edgeDetected = edgeDetected;
	  pT->msk = msk;
	  pT->byte = byte;
	  PSOCRecvCnt++;
	  if(PSOCRecvCnt == PSOCMAXRECVBUFFERSIZE)
	    PSOCRecvCnt = 0;
	  //Make sure we see a start bit
	  if( startDetected == 0 )
	    {
	      if(bitRead == 0)
			  {
				  startDetected = 1;
			  }
	      else
			{
			  if (giveUpWaiting)
				{
				  //No Start Bit
				  byte |= 0xfe00;
				  break;
				}
			  else
				{
				  edgeDetected = 0;
				  i = 0;
				  continue;
				}
			}
	    }
	  if(startDetected)
	    {
	      //Make sure we see a stop bit
	      if( i == (SERIALBITSPERBYTE - 1) )
			{
			  if( bitRead == 0 )
				{
				  //No Stop Bit
				  byte |= 0xef00;
				  break;
				}
			}
	      else
			{
			  if( i != 0 && i != (SERIALBITSPERBYTE - 1) )
				{
				  if(bitRead)
						byte |= msk;
				  msk <<= 1;
				}
			}
	      i++;

	    }
	  PMDR |= MCB_SYNC_CS; // why disable CS right after first bit?
	  PUDR |= PH_SYNC_CS;
	}//while i
}
#endif

// Read signal multiple times and use some algorithm to determine whether it is 1 or 0
unsigned char HALMultiReadSyncTX(unsigned numCycles)
{
	unsigned curCycle;
	unsigned count = 0;

    for(curCycle=0; curCycle < numCycles; curCycle++)
    {
    		HALSetSyncCLKwDelay(1);
    		count += HALReadSyncTX();
    		HALSetSyncCLKwDelay(0);
    }
    //simple algo
	return (count >= 2) ? 1 : 0;
}

// Read SYNC_TX value only if activity on line; otherwise timeout
unsigned short HALReadSBit(void)
{
	unsigned short retValue = 0;
    unsigned int timeoutPolls;

	// wait for SyncTX to go low
    timeoutPolls = PSOC_SYNCTX_TIMEOUT_POLLS;
	while (1)
	{
		HALSetSyncCLKwDelay(1);
		if (!HALReadSyncTX())
		{//activity detected so read several times to confirm value
			retValue = HALMultiReadSyncTX(PSOC_SYNC_CLKS_PER_BIT);
			break;
		}

		// If the wait is too long then timeout
        if (timeoutPolls-- == 0)
        {// cleanup  and exit
        	  retValue = PSOC_STATUS_NO_START_BIT;
			  break;
        }
		HALSetSyncCLKwDelay(0);
	}
	return retValue;
}

unsigned short HALReadSByte(void)
{
	unsigned short retValue = 0;
	unsigned short tempValue = 0;
    unsigned char b;
    unsigned short currByte = 0;
    unsigned short ssbit;

    // Check for start bit
    ssbit = HALReadSBit();
    if ((ssbit == 1) || (ssbit == PSOC_STATUS_NO_START_BIT))
    {// error - start bit should be 0 or timeout
    	return PSOC_STATUS_NO_START_BIT;
    }

    // Get byte - receive LSB first
    for (b=0; b<8; b++)
    {
    	tempValue =  HALMultiReadSyncTX(PSOC_SYNC_CLKS_PER_BIT);
        currByte += (tempValue << b);
    }
    retValue = currByte;

    // Check for stop bit
    ssbit = HALMultiReadSyncTX(PSOC_SYNC_CLKS_PER_BIT);
    if (ssbit == 0)
    {// error - stop bit should be 1
    	return PSOC_STATUS_NO_STOP_BIT;
    }
    return retValue;
}

unsigned short HALReadPSOCSerialData( unsigned char psocId)
{
	unsigned short retValue;
// For BBB, SyncTXSel does not work (no mux chip) so SyncTX has to be manually switched between PH and MCB
    if(psocId == MCB_PSOC)
    {
    	HALSetSyncTXSel(1);
    }
    else
    {
    	HALSetSyncTXSel(0);
    }
	HALSetSyncCS(psocId, 0);
	retValue = HALReadSByte();
	HALSetSyncCS(psocId, 1);
	return retValue;
}

#if 0
// G900 code for comparison purposes
void fnInitPSOCState( ){
unsigned char nv;

nv = PMDR;
nv |= MCB_SYNC_CS;
nv &= ~MCB_PGM_CLK;
nv &= ~(MCB_PGM_DATA);
//  nv &= ~(MCB_PGM_DATA | MCB_PGM_CLK);
PMDR = nv;
nv = *phClkDataPort;
nv &= ~phPortClkBit;
nv &= ~(phPortDataBit );
*phClkDataPort = nv;
nv = PUDR;
nv |= PH_SYNC_CS;
nv &= ~PGM_DATA_DIR;
PUDR = nv;
PMDR |= MCB_RESET_LFSR;
OSWakeAfter( 100 );
PMDR &= ~MCB_RESET_LFSR;
if(PSOCInvertSerialData)
PSDR &= ~PSOC_SYNC_RX;
else
PSDR |= PSOC_SYNC_RX;
if(PSOCInvertSerialClk)
PSDR |= PSOC_SYNC_CLK;
else
PSDR &= ~PSOC_SYNC_CLK;
}
#endif

//default configuration for all in/out pins is input
void HALInitPSOCState()
{
// MCB Init
	HALSetSyncCS(MCB_PSOC, 1);
	HALSetPCLK(MCB_PSOC, 0);
	HALSetPDATAInput(MCB_PSOC);

// PH Init
	HALSetSyncCS(PH_PSOC, 1);
	HALSetPCLK(PH_PSOC, 0);
	HALSetPDATAInput(PH_PSOC);

// Init comm interface
    HALSetSyncRX(1);
    HALSetSyncCLK(0);

}
