/**********************************************************************
 PROJECT    :   Future Phoenix
 COPYRIGHT  :   2005, Pitney Bowes, Inc.  
 AUTHOR     :   
 MODULE     :   OiMsgfp.c

 DESCRIPTION:
    This file contains OI message handling tables and functions specific to FP.
    Originally adapted from Mega file oimsgc.c

 	MODIFICATION HISTORY:
 	
 22-May-17 Jennifer Hao on FPHX 02.12 cienet branch
    Commented out functions  fnmDISTModemActivated,fnmDISTModemDisconnected,  fnIsNewPlatConnected,
    fnSetNewPlatConnected,fnIsCmFS2Blocked,  because they aren't used by G9.

 15-Sep-14 Renhao on FPHX 02.12 cienet branch
    Modified function fnForwardBOBError() to fix fraca 227107.

 18-Mar-13 sa002pe on FPHX 02.10 shelton branch
	For Fraca 219088:
	1. Added entries in the StOITMsgTable table for OIT_ARCHIVE_NEEDS_UNPACKING & OIT_UNPACKING_ARCHIVE_COMPLETE.
	2. At the same time re-arranged the entries in StOITMsgTable so all the messages from a particular task
		are grouped together.

 	11/01/2012  John Gao        Fixed fraca GMSE00162040, Modified fnmEDMUSBDisconnected() to process event EVENT_USB_DISCONNECTED when usb cable disconnected
 	
 	10/29/2012  Bob Li          Modified the fnmPLATConnected() to fix fraca 218810.
 	
 	09/11/2012  John Gao & Bob Li 
 	                         -  Modified fnmPLATConnected(), never show the Scale Location Code screen if a W&M platform 
 	                            is attached (Enh Req 167225, part 2).
 	                         -  Updated the function fnmPLATDisconnected() to make sure the postage can be reset to 0 and
 	                            mode is switched to non-platform mode if no scale is attached.
 	                            
    08/30/2012  John Gao        Merge code from Janus for Enh Req 166545 and 166346. 
								If the weight on the W&M platform is non-zero, 
								the meter won�t time out to the Normal Preset (Enh Req 166545) and won�t go to sleep (Enh Req 166346).
 	06/29/2010  Jingwei,Li      Added fnmDISTRefillServiceStatus() to hanlde reissue case.
 	07/08/2009  Raymond Shen    Modified fnmCMClrErrRsp() to clear CM's paper error status when CM tell OIT the error has been cleared.
 	06/24/2009  Raymond Shen    Modified fnIsWOWMailJammed() and fnForwardFeederWOWMailJamStatus()to remove check of sensor FS2.
    	2009.06.15  Clarisa Bellamy Added entry in table for OIT_UPDATE_DISPLAY message from SYS.
 	06/23/2009  Jingwei,Li      Added fnIsCmFS2Blocked() and modified fnForwardFeederWOWMailJamStatus() for wow error handling.
 	05/20/2009  Raymond Shen    Added fnForwardFeederWOWSBRError(), fnForwardFeederWOWDimRatingErr() and modified fnmCMProgress()
 	                            for wow error handling.
 	05/18/2009  Raymond Shen    Added fnIsWOWMailJammed(), fnForwardFeederWOWMailJamStatus(), and modified fnmCMProgress for WOW.
 	04/15/2009  Raymond Shen    Modified code for the new format of sensor status message between OI, CM, and FDR.
 	02/06/2009  Bob Li          Merged codes from Mega for fixing fraca 156221.
 	01/22/2009  Raymond Shen    Added some code for WOW feature.
 	08/07/2008  Joey Cui        Merged stuffs from Janus to support PC proxy feature.
 	08/01/2008  Raymond Shen    Added fnmDISTDataUploadStatus() and add it to StOITMsgTable[] for Budget 
 	                            account upload.
 	09/28/2007  Adam Liu        Added slots of REMOTE_REFILL_REQ and REMOTE_REFILL_DONE in StOITMsgTable
 	08/24/2007  Andy Mo         Added OIT_GTS_RECOVERY in table StOITMsgTable
    08/23/2007  Raymond Shen    In fnmPLATDisconnected(), added code to clear postage and
                                 rate info before switching print mode to fix fraca 127849.
    08/08/2007  Adam Liu        Modified function fnmPLATWeightUpdate() to 
                                Only omit the event EVENT_PLATFORM_UPDATE_RCVD 
                                on select carrier and select class screen 
                                if in diff weight mode.Fix GMSE00125139, GMSE00125140.
    07/19/2007  Vincent Yi      Fixed lint errors
 	06/26/2007  Oscar Wang      Add Differential Weigh specific code in fnmPLATWeightUpdate() to fix fraca 119437.
 	05/31/2007  Joey Cui        Remove the code to clear LowFunds Warning in fnForwardMailPieceProcessedMsg
	05/24/2007  Oscar Wang      Added OIT_SCANNER_NEWDATA in StOITMsgTable[] for barcode scanner.
	05/15/2007	Vincent Yi		Renamed OIT_ABACUS_TRANSACTION_FAILED to OIT_ABACUS_DATA_SETUP_FAILED
    04/24/2007  Raymond Shen    Added OIT_ABACUS_TRANSACTION_FAILED in StOITMsgTable[] for
                                Abacus transaction.
	04/12/2007	Vincent Yi		Added three SYS messages for Abacus	into StOITMsgTable[]
 	19 Mar 2007 Bill Herring    Activated EVENT_RATE_ZWZP in event table.
  03/05/2007  I. Le Goff      In fnmPLATDisconnected() add modifications for mode ajout
 	01/22/2007  Adam Liu        Added new event EVENT_DCAP_DO_AUTO_UPLOAD to event table.
	01/08/2007	Vincent Yi		Added flag fPieceCompleted
 	12/21/2006  Raymond Shen    Remove function fnmOITPrintModeChange().
	12/19/2006	Vincent Yi		Added fnClearCMWasteTankError()
 	12/15/2006  Raymond Shen    Implemented ZWZP feature: added fnmOITPrintModeChange() and
 	                            added OIT_PRINT_MODE_CHANGE to message table.
	11/15/2006	Vincent Yi		Fixed fraca 97362 106923, changed function fnmCMStatusRsp() on
								handling the sensor status in CM response.
 	09/12/2006  Raymond Shen    For platform calibration feature, added PLAT_CALIB_REF_PROMPT 
 	                            to message table, and added fnmPLATCalibRefPrompt(), 
 	                            fnGetPlatCalibWeightStyle(), fnGetPlatCalibWeight().
	09/08/2006	Vincent Yi		Added message OIT_MIDNIGHT_COMING into StOITMsgTable[]
	09/05/2006  Dicky Sun      For GTS deleting manifest record, add the following
	                           events:
	                                  EVENT_ARCHIVE_DELETE_RECORDS 
	                                  EVENT_ARCHIVE_UPDATE_COMPETE
	08/31/2006  Oscar Wang      Modified function fnmPLATWeightUpdate for 
	                            re-rate
	08/15/2006	Vincent Yi		Fixed fraca 99437, added function fnmCMMaintenanceRsp,
								Modified functions fnmCMRplStartRsp, fnmCMRplCmpltRsp
								to handle the response with error
	08/04/2006	Vincent Yi		Added functions fnSetNewPlatConnected(), 
								fnIsNewPlatConnected()
								Modified fnmPLATConnected(), fnmPLATDisconnected()
								to fit new USB platform
	07/21/2006  Dicky Sun      Update fnForwardMailPieceProcessedMsg for GTS 
	                           single piece mode when we have a warning.
    07/19/2006  Dicky Sun      Add EVENT_SCREEN_TIMER_EXPIRED to message table.
	07/05/2006	Vincent Yi	   	Added functions fnIsCmS1Blocked(), fnIsCmS2Blocked(), 
								fnIsCmS3Blocked()
							   	Added CM->OIT messages CO_DIAG_ACTN_RSP, CO_GET_DATA_RSP 
							   	and their event handling functions
	07/04/2006  Oscar Wang      Added event handle function in  
								fnmPLATDisconnected
	07/03/2006	Raymond Shen	Added OIT_METER_ENABLE_REQ and OIT_SET_POSTAGE_REQ
								to message table;
								Added function fnmEDMSetPostage().
 	05/15/2006	Raymond Shen	Added event handle function in fnmPLATConnected().
    05/10/2006  Steve Terebesi  Merged from rearchitecture branch
    03/21/2006  Steve Terebesi  Added OIT_REC_EVENT and OIT_ACK to message table
    03/17/2006  Steve Terebesi  Added messages to StOITMsgTable

-----------------------------------------------------------------------
	OLD PVCS REVISION HISTORY
 *    Rev 1.86   Jul 26 2005 04:03:50   cx17598
 * Rollback to previous previsos version, we do not need a new timer to handle alpha character input.
 * -Victor
*************************************************************************/


/**********************************************************************
		Include Header Files
**********************************************************************/
#include <stdio.h>
#include <string.h>
#include "nucleus.h"
#include "oit.h"
#include "oitpriv.h"
#include "oierrhnd.h"
#include "oifields.h"
#include "oifpinfra.h"
#include "oifpprint.h"
#include "oifunctfp.h"
#include "oiscreen.h"
#include "oiparm.h"
//#include "oirefill.h"
#include "global.h"
#include "keypad.h"
#include "lcd.h"
#include "bob.h"
#include "sys.h"		/* this is temp only.  there are pmci message definitions in here that must be moved to pmcitask.h */
#include "errcode.h"
#include "printer.h"
#include "datdict.h"
#include "pbplatform.h"
#include "oipostag.h"
#include "cmerr.h"
#include "clock.h"
#include "syspriv.h"
//#include "ratetask.h"																		
#include "mmcimjet.h"

//#include "pc_daemon.h"
#include "oifpmain.h"
#include "oitcm.h"
//#include "oidiagnostic.h"
//#include "oiweight.h"
#include "trm.h"

#include "networking/externs.h"
//TODO - cleanly remove PPP dependent code
//#include "nu_ppp.h"
//#include "ppp\inc\pppurt.h"
//#include "ppp\inc\mdm_defs.h"
//#include "ppp\inc\lcp_defs.h"
//#include "ppp\inc\chp_defs.h"
//#include "ppp\inc\pap_defs.h"
//#include "ppp\inc\ncp_defs.h"
//#include "ppp\inc\ppp_defs.h"
#include "intellilink.h"
#include "download.h"
#include "sys.h"
#include "ajout.h"
//#include "OiScanner.h"
//#include "oirateservice.h"

//#include "..\include\n_daemon.h"
#include "oifpmain.h"
#include "api_status.h"
#include "api_funds.h"
#include "bobutils.h"
#include "sysdatadefines.h"

#include "cjunior.h" //TODO: JAH temporary measure to get debitted postage value until transaction manager is implemented
#include "cm.h"
#include "cmpublic.h"

//Temporarily copy this defines from n_daemon.h to pass compile
// should remove this after merged n_daemon.h  - Adam
typedef enum tagN_DaemonMsg{
    REMOTE_REFILL_REQ = 0x00,
    REMOTE_REFILL_OK,    
    REMOTE_REFILL_BUSY,
    REMOTE_REFILL_DONE
    }N_DAEMON_MSG;

/**********************************************************************
		Local Function Prototypes
**********************************************************************/

/* message handling functions */
static BOOL fnmSYSInitialize(INTERTASK *pIntertask);
static BOOL fnmSYSChangeMode(INTERTASK *pIntertask);
//static BOOL fnmSYSDisabledProfiles(INTERTASK *pIntertask);

static BOOL fnmCMStatusRsp(INTERTASK *pIntertask);
static BOOL fnmCMClrErrRsp(INTERTASK *pIntertask);
static BOOL fnmCMProgress(INTERTASK *pIntertask);
static BOOL	fnmCMMaintenanceRsp(INTERTASK *pIntertask);
static BOOL fnmCMRplStartRsp(INTERTASK *pIntertask);
static BOOL fnmCMRplCmpltRsp(INTERTASK *pIntertask);
static BOOL fnmCMSetMarginRsp(INTERTASK *pIntertask);
static BOOL fnmCMDiagActnRsp(INTERTASK *pIntertask);
static BOOL fnmCMGetDataRsp(INTERTASK *pIntertask);
static BOOL fnmCMFeederDiagRsp(INTERTASK *pIntertask);


static BOOL fnmKEYReceiveKeyData(INTERTASK *pIntertask);

static BOOL fnmOITScreenTick(INTERTASK *pIntertask);

static BOOL fnmOITMidnightComing(INTERTASK *pIntertask);

static BOOL fnmABAConnectionResult(INTERTASK *pIntertask);

static BOOL fnForwardPMFatalError(INTERTASK *pIntertask, 
								  OIT_CM_STATUS * pCurCMStatus);
static BOOL fnForwardPMPrintheadError(INTERTASK *pIntertask, 
									  OIT_CM_STATUS * pCurCMStatus);
static BOOL fnForwardPMInkTankError(INTERTASK *pIntertask, 
									OIT_CM_STATUS * pCurCMStatus);
static BOOL fnForwardPMWasteTankError(INTERTASK *pIntertask, 
									  OIT_CM_STATUS * pCurCMStatus);
static BOOL fnForwardPMInkError(INTERTASK *pIntertask, 
								OIT_CM_STATUS * pCurCMStatus);
static BOOL fnForwardPMPaperError(INTERTASK *pIntertask, 
								  OIT_CM_STATUS * pCurCMStatus);
static BOOL fnForwardMailPieceProcessedMsg(INTERTASK *pIntertask, 
										   OIT_CM_STATUS * pCurCMStatus);
static BOOL fnForwardTopCoverStatus(INTERTASK *pIntertask, 
									OIT_CM_STATUS * pCurCMStatus);
static BOOL fnForwardJamLeverStatus(INTERTASK *pIntertask, 
									OIT_CM_STATUS * pCurCMStatus);	
static BOOL fnForwardSensorsStatus(INTERTASK *pIntertask, 
								   OIT_CM_STATUS * pCurCMStatus);	
static BOOL fnForwardFeederWOWMailJamStatus(INTERTASK 	 *	pIntertask, 
								   OIT_CM_STATUS * 	pCurCMStatus);
static BOOL fnForwardFeederWOWSBRError(INTERTASK 	  *	pIntertask, 
									OIT_CM_STATUS * pCurCMStatus);
static BOOL fnForwardFeederWOWDimRatingErr(INTERTASK 	  *	pIntertask, 
									OIT_CM_STATUS * pCurCMStatus);
static BOOL fnForwardBOBError(INTERTASK *pIntertask, 
							  OIT_CM_STATUS * pCurCMStatus);	
static BOOL fnForwardPSOCError(INTERTASK *pIntertask, 
							   OIT_CM_STATUS * pCurCMStatus);	
static BOOL fnForwardFeederCoverStatus(INTERTASK 	  *	pIntertask, 
									   OIT_CM_STATUS * pCurCMStatus);	
static BOOL fnForwardTapeError(INTERTASK 	 *	pIntertask, 
 							   OIT_CM_STATUS * 	pCurCMStatus);
static BOOL fnForwardFeederAddedMailMsg(INTERTASK 	 *	pIntertask, 
								   		OIT_CM_STATUS * 	pCurCMStatus);	
static BOOL fnForwardFeederNoMailError(INTERTASK 	 *	pIntertask, 
								   		 OIT_CM_STATUS * 	pCurCMStatus);
static BOOL fnForwardFeederError(INTERTASK 	 *	pIntertask, 
					   		     OIT_CM_STATUS * 	pCurCMStatus);
static BOOL fnForwardMailRunStatusUpdateMsg(INTERTASK 	  *	pIntertask, 
									OIT_CM_STATUS * pCurCMStatus);
//static BOOL fnmDISTModemActivated(INTERTASK *pIntertask);
//static BOOL fnmDISTModemDisconnected(INTERTASK *pIntertask);

static BOOL fnmDISTConnectionResult(INTERTASK *pIntertask);
static BOOL fnmDISTDnsLookupError(INTERTASK *pIntertask);
static BOOL fnmDISTStartService(INTERTASK *pIntertask);
static BOOL fnmDISTEndService(INTERTASK *pIntertask);
static BOOL fnmDISTAllServicesComplete(INTERTASK *pIntertask);
static BOOL fnmDISTDataUploadStatus(INTERTASK *pIntertask);
static BOOL fnmDISTRefillServiceStatus(INTERTASK *pIntertask);
//static BOOL fnmDISTCheckTimeSyncStatus(INTERTASK *pIntertask);
//static BOOL fnmRemoteRefillRequest(INTERTASK *pIntertask) ;

static BOOL fnmPLATConnected(INTERTASK *pIntertask);
static BOOL fnmPLATDisconnected(INTERTASK *pIntertask);
static BOOL fnmPLATWeightUpdate(INTERTASK *pIntertask);
static BOOL fnmPLATCalibRefPrompt(INTERTASK *pIntertask);
static BOOL fnmMeterControlPC(INTERTASK *pIntertask);
static BOOL fnmOITEnterSleep(INTERTASK *pIntertask);
static BOOL fnmOITSetToNormalPreset(INTERTASK *pIntertask);
void fnSendStopMailRequest (void);
void fnSetMeterCapture(BOOL	 );
BOOL fnPCSendUnsolicitedStatus(unsigned short);
void fnGetSysMode(UINT8* pMode,UINT8* pSubmode);
void fnClearDateCorrection();
/**********************************************************************
		Local Defines, Typedefs, and Variables
**********************************************************************/
#define KBD_ENTER_KEY	0x54
#define KBD_STOP_KEY	0x44

#define THISFILE "oimsgfp.c"

// Store the CM respond status after OI request
static OIT_CM_STATUS	stRespondCMStatus;	
// Store the lastest CM status reported by CM unsolicitedly
static OIT_CM_STATUS	stLatestCMStatus;	

static BOOL				fPieceCompleted = FALSE;

static BOOL				fNewPlatConnected = FALSE;

static UINT8    bPlatCalibWeightStyle = 0;
static UINT8   pPlatCalibWeight[LENWEIGHT] = {0,0,4,0};

// Store the previous CM status reported by CM unsolicitedly
static OIT_CM_STATUS	stPreviousCMStatus;	

/**********************************************************************
		Global Functions Prototypes
**********************************************************************/
extern void RebootSystem(void);

extern INTERTASK *fnGetlastIntertaskMsg(void);

extern void fnChangeScreen(unsigned short wScreenNumber, BOOL fCallPreFn, BOOL fCallPostFn);

extern Diagnostics  *fnCMOSSetupGetCMOSDiagnostics(void);
/**********************************************************************
		Global Variables
**********************************************************************/

//extern PREPOS_WAIT_CTRL_BLOCK	rWaitCtrl;
//extern BOOL			fOITDistrConnectStatus;
//extern BOOL			fOITAbortModemConnection;


//extern OIPMReplacement		oiPmReplacement; // oi variable for print head or ink tank replacement.

//extern OI_PLATFORM_CTRL		rPlatformCtrl;

extern unsigned long   lwTimes; // counter used by the PM
unsigned long	*lwCountPtr = &lwTimes;

extern BOOL	fLowFundsWarningPending;
//extern BOOL	fLowFundsWarningDisplayed;
extern BOOL	fLowPieceCountWarningPending;
//extern BOOL	fLowPieceCountWarningDisplayed;

extern unsigned char 		fCMOSScaleLocationCodeInited;
extern BOOL	fSetScaleLocCodePending;
extern KIPPWInfo*    pKIPPasswordInfo;

extern BOOL	fScreenTickEventQueued;

extern BOOL	fForceReRate;

extern BOOL bIsCorrecteDate;

extern BOOL bGoToReady;

extern tCmStatus               cmStatus;
//extern unsigned char	bDialingServiceComplete ;

unsigned char				bOITCurrentDistrMsgID;
unsigned char				bOITCurrentService;
unsigned char				bOITCurrentServicePriority;
UINT16   usOITCurrentDisplayService = 0; // Added for Data Upload functionality for Web Vis.

//BOOL					fOITCurrentServiceTimeSet;
//BOOL					fOITScheduledUpdateOnline;
BOOL					fForceScaleLocReset = FALSE;
BOOL					fOITDistrDnsLookupErr = FALSE;

BOOL					fRecheckPrinterStatus = FALSE;

/* OIT message handling table- can automatically generate an event or call a handler function */
//const OIT_MSG_TABLE_ENTRY pOITMsgTable[] = 
static const OIT_MSG_TABLE_ENTRY StOITMsgTable[] = 
{
/* source 	message ID       		byte data [0]    byte data [1]  event (FFFF means call fn)  fn pointer		      	 	 */
/* task																		   instead						   				 */
{ SYS,		OIT_POWER_UP_COMPLETE,		OIT_DC,	OIT_DC,				EVENT_POWER_UP_COMPLETED,	NULL			},
{ SYS,		OIT_INITIALIZE_REQUEST,		OIT_DC,	OIT_DC,				0xFFFF,						fnmSYSInitialize},
{ SYS,		OIT_CHANGE_MODE,			OIT_DC,	OIT_DC,				0xFFFF,						fnmSYSChangeMode},
{ SYS,		OIT_MAIL_JOB_COMPLETE,		OIT_DC,	OIT_DC,				EVENT_MAIL_JOB_COMPLETED,	NULL			},
{ SYS,      OIT_ACK,                    OIT_DC, OIT_DC,             0xFFFF,                     fnmProcessSYSAck},
{ SYS,      OIT_REC_EVENT,              OIT_DC, OIT_DC,             0xFFFF,                     fnmProcessEvent },
{ SYS,		OIT_METER_ENABLE_REQ,		OIT_DC,	OIT_DC,	 			EVENT_EDM_METER_ENABLE,		NULL						},
//{ SYS,      OIT_GTS_RECOVERY,       OIT_DC, OIT_DC,                 EVENT_OIT_GTS_RECOVERY,     NULL    },

//{ SYS,      OIT_STATUS_ACK,             OIT_DC, OIT_DC,             0xFFFF,                     fnmDoNothing    },
//{ SYS,      OIT_DONOTHING,              OIT_DC, OIT_DC,             0xFFFF,                     fnmDoNothing    },

// Currently, following two messages are useless for OI 
//{ SYS,	OIT_START_MAIL_RSP,			OIT_DC,	OIT_DC,				0xFFFF,						NULL},
//{ SYS,	OIT_STOP_MAIL_RSP,			OIT_DC,	OIT_DC,				0xFFFF,						NULL},

// merged from Mega codes for PC connection
{ SYS,      OIT_PC_CONNECTION_RESULT,   OIT_DC, OIT_DC,             0xFFFF,                     fnmABAConnectionResult      },

//{ SYS,      OIT_ABACUS_FILE_UPDATE_PROGRESS,OIT_DC,OIT_DC,          EVENT_AB_FILE_UPDATE_MSG,   NULL      		},
//{ SYS,      OIT_ABACUS_DISCONNECT_USB,      OIT_DC,OIT_DC,          EVENT_ABACUS_DISCONNECT_USB,NULL            },
//{ SYS,      OIT_ABACUS_HOST_ATTENTION,      OIT_DC,OIT_DC,          EVENT_ABACUS_HOST_ATTENTION,NULL            },
//{ SYS,      OIT_ABACUS_DATA_SETUP_FAILED,   OIT_DC,OIT_DC,      	EVENT_ABACUS_DATA_SETUP_FAILED,NULL    },
{ SYS,      OIT_UPDATE_DISPLAY,             OIT_DC, OIT_DC,         EVENT_UPDATE_DISPLAY,       NULL            },
//{ SYS,		OIT_UPDATE_DCAP_STATUS,		OIT_DC,	OIT_DC,				EVENT_DCAP_DO_AUTO_UPLOAD,	NULL	},
//{ SYS,      OIT_UNPACKING_ARCHIVE_COMPLETE, OIT_DC, OIT_DC,         EVENT_UNPACKING_ARCHIVE_COMPLETE,     NULL  },
//{ SYS,      OIT_ARCHIVE_NEEDS_UNPACKING,    OIT_DC, OIT_DC,         EVENT_ARCHIVE_NEEDS_UNPACKING,           NULL},

{ CM,		CO_GET_STATUS_RSP,			OIT_DC,	OIT_DC,				0xFFFF,						fnmCMStatusRsp		},
{ CM,		CO_MAIL_PROGRESS,			OIT_DC,	OIT_DC,	 			0xFFFF,						fnmCMProgress	 	},
{ CM,		CO_MTNC_RSP,				OIT_DC,	OIT_DC,				0xFFFF,						fnmCMMaintenanceRsp	},

{ CM,		CO_CLR_ERR_RSP,				OIT_DC,	OIT_DC,				0xFFFF,						fnmCMClrErrRsp		},
{ CM,		CO_RPL_START_RSP,			OIT_DC,	OIT_DC,				0xFFFF,						fnmCMRplStartRsp	},
{ CM,		CO_RPL_CMPLT_RSP,			OIT_DC,	OIT_DC,				0xFFFF,						fnmCMRplCmpltRsp	},
{ CM,		CO_SET_MARGIN_RSP,			OIT_DC,	OIT_DC,				0xFFFF,						fnmCMSetMarginRsp},
{ CM,		CO_DIAG_ACTN_RSP,			OIT_DC,	OIT_DC,				0xFFFF,						fnmCMDiagActnRsp},
{ CM,		CO_GET_DATA_RSP,			OIT_DC,	OIT_DC,				0xFFFF,						fnmCMGetDataRsp},
{ CM,		CO_FDR_DIAG_RSP,			OIT_DC,	OIT_DC,				0xFFFF,						fnmCMFeederDiagRsp},

{ KEYTASK,	KEY_RX_KEYDATA,				OIT_DC, OIT_DC,	 			0xFFFF,						fnmKEYReceiveKeyData},
//{ KEYTASK,	OIT_SCANNER_NEWDATA,		    OIT_DC,OIT_DC,			0xFFFF,                     fnmScannerDataHandler   	},

{ OIT,		OIT_SCREEN_CHG_TMR_EXPIRED,	OIT_DC,	OIT_DC,				EVENT_SCREEN_TIMER_EXPIRED,	NULL	},
//{ OIT,		OIT_SCREEN_TICK_OCCURRED,	OIT_DC,	OIT_DC,				0xFFFF,						fnmOITScreenTick	},
{ OIT,		OIT_SLEEP_TIMER_EXPIRED,	OIT_DC,	OIT_DC,	 			0xFFFF,				        fnmOITEnterSleep	},
{ OIT,		OIT_SHORT_TICK_OCCURRED,	OIT_DC,	OIT_DC,	 			EVENT_SHORT_TICK_OCCURRED,	NULL	},
{ OIT,		OIT_WAKE_UP,				OIT_DC,	OIT_DC,				EVENT_WAKE_UP,				NULL		},
{ OIT,		OIT_MIDNIGHT_COMING,		OIT_DC,	OIT_DC,				0xFFFF,						fnmOITMidnightComing},
{ OIT,		OIT_PRINT_MODE_CHANGE,		OIT_DC,	OIT_DC,			    EVENT_PRINT_MODE_CHANGE,	NULL},
{ OIT,		OIT_NORMAL_PRESET_TIMER_EXPIRED,	OIT_DC,	OIT_DC,		0xFFFF, fnmOITSetToNormalPreset 	},
{ OIT,		OIT_RATE_ZWZP_EXPIRED,		OIT_DC,	OIT_DC,	 			EVENT_RATE_ZWZP,			NULL	},
//{ OIT,		OIT_PROCESS_ARCHIVE_UPDATE,	OIT_DC,	OIT_DC,				EVENT_ARCHIVE_DELETE_RECORDS,	NULL	 	},
//{ OIT,		OIT_ARCHIVE_UPDATE_COMPLETE,	OIT_DC,	OIT_DC,			EVENT_ARCHIVE_UPDATE_COMPETE,	NULL		},

//{ DISTTASK,	MDM_CONNECTED,				OIT_DC,	OIT_DC,		0xFFFF,						fnmDISTModemActivated},
//{ DISTTASK,	MDM_DISCONNECT,				OIT_DC,	OIT_DC,		0xFFFF,						fnmDISTModemDisconnected},
{ DISTTASK,	OIT_ATT_CONNECTION_RESULT,	OIT_DC,	OIT_DC,		0xFFFF,						fnmDISTConnectionResult	},
//{ DISTTASK,	OIT_CONNECTION_START,		OIT_DC, OIT_DC,		EVENT_DC_DIALING_DONE,		NULL			},
{ DISTTASK,	OIT_DNS_TIMEOUT,			OIT_DC,	OIT_DC,		0xFFFF,						fnmDISTDnsLookupError},
{ DISTTASK,	OIT_START_SERVICE_REQ,		OIT_DC,	OIT_DC,		0xFFFF,						fnmDISTStartService	},
{ DISTTASK,	OIT_SERVICE_COMPLETE,		OIT_DC,	OIT_DC,		0xFFFF,						fnmDISTEndService	},
{ DISTTASK,	OIT_ALL_SERVICES_COMPLETE,	OIT_DC,	OIT_DC,		0xFFFF,						fnmDISTAllServicesComplete},
//{ DISTTASK,	OIT_DISTRIBUTOR_CONTACTED,	OIT_DC,	OIT_DC,		EVENT_DC_PROCESSING,		NULL			},
{ DISTTASK,	DLA_SAYS_NO_UPDATES_AVAILABLE,	OIT_DC,	OIT_DC,			EVENT_NO_UPDATES_AVAILABLE,	NULL },
{ DISTTASK, OIT_INFRA_UPLOAD_XFER_STATUS,   OIT_DC, OIT_DC,         0xFFFF,                     fnmDISTDataUploadStatus         },
{ DISTTASK, OIT_INFRA_REFILL_SERV_STATUS,   OIT_DC, OIT_DC,         0xFFFF,                     fnmDISTRefillServiceStatus         },

{ PLAT,		PLAT_CONNECT,			OIT_DC, OIT_DC,			0xFFFF,				fnmPLATConnected },
{ PLAT,		PLAT_DISCONNECT,		OIT_DC, OIT_DC,			0xFFFF,				fnmPLATDisconnected },
{ PLAT,		PLAT_UPDATE,			OIT_DC, OIT_DC,			0xFFFF,				fnmPLATWeightUpdate },
{ PLAT,		PLAT_CALIB_ZERO_PROMPT,		OIT_DC, OIT_DC,			EVENT_PLAT_ZERO_PROMPT,		NULL },
{ PLAT,		PLAT_CALIB_REF_PROMPT,		OIT_DC, OIT_DC,			0xFFFF,		fnmPLATCalibRefPrompt },

{ OIT_MSG_END_OF_TABLE, 0, 0, 0, 0, 0 }
 
};


/**********************************************************************
		Public Functions
**********************************************************************/
/* Utility functions */

/* *************************************************************************
// FUNCTION NAME: 
//		fnIsMailPieceCompleted
//
// DESCRIPTION: 
//		None.
//
// INPUTS:
//		None.
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		value of fPieceCompleted
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	01/08/2006	Vincent Yi		Initial version
// 
// *************************************************************************/
BOOL fnIsMailPieceCompleted (void)
{
	return fPieceCompleted;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnSetMailPieceCompleted
//
// DESCRIPTION: 
//      None.
//
// INPUTS:
//      fCompleted - 
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	01/08/2006	Vincent Yi		Initial version
// 
// *************************************************************************/
void fnSetMailPieceCompleted (BOOL fCompleted)
{
	fPieceCompleted = fCompleted;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetOITMessageTable
//
// DESCRIPTION: 
//      Accessor function to return StOITMsgTable const pointer
//
// INPUTS:
//		None.
//
// RETURNS:
//		const pointer of StOITMsgTable.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	1/04/2006	Vincent Yi		Initial version 
//
// *************************************************************************/
const OIT_MSG_TABLE_ENTRY * fnGetOITMessageTable (void)
{
    return (StOITMsgTable);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnGetLatestCMStatus
//
// DESCRIPTION: 
//      Accessor function to return the pointer of stLatestCMStatus
//
// INPUTS:
//		None.
//
// RETURNS:
//		pointer of stLatestCMStatus.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	1/06/2006	Vincent Yi		Initial version 
//
// *************************************************************************/
const OIT_CM_STATUS * fnGetLatestCMStatus (void)
{
    return (&stLatestCMStatus);	
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmFatalError
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmFatalError(void)
{
	BOOL fRet = FALSE;

	if (stLatestCMStatus.ucGenStatus & FATAL_ERR)
	{
		fRet = TRUE;
	}
	else
	{
		fRet = fnIsCmPSOCError();
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmPSOCError
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmPSOCError(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.ucGenStatus & PSOC_ERR) && 
		(stLatestCMStatus.ucPsocStatus == ERR_PSOC))
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmPrintheadError
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmPrintheadError(void)
{
	BOOL fRet = FALSE;

	if (stLatestCMStatus.ucGenStatus & PHEAD_ERR) 
	{	// no printhead, printhead id/checksum/sensor range/heater error
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmInkTankError
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmInkTankError(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.ucGenStatus & INKTANK_ERR) && (
		(stLatestCMStatus.ucInkTankStatus == ERR_NO_INKTANK) 	|| 
		(stLatestCMStatus.ucInkTankStatus == ERR_INKTANK_ID) || 
		(stLatestCMStatus.ucInkTankStatus == ERR_INKTANK_CHECKSUM)) )
	{	// no ink tank, ink tank id/checksum error 
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmNoPrinthead
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmNoPrinthead(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.ucGenStatus & PHEAD_ERR)	&& 
		(stLatestCMStatus.ucPheadStatus == ERR_NO_PHEAD))
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmNoInkTank
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmNoInkTank(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.ucGenStatus & INKTANK_ERR)	&& 
		(stLatestCMStatus.ucInkTankStatus == ERR_NO_INKTANK))
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmInkTankLidOpen
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmInkTankLidOpen(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.ucGenStatus & INKTANK_ERR)	&& 
		(stLatestCMStatus.ucInkTankStatus == ERR_INKTANK_LID_OPEN))
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmInkTankExpiring
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmInkTankExpiring(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.ucGenStatus & INKTANK_ERR)	&& 
		(stLatestCMStatus.ucInkTankStatus == WRN_INKTANK_MRTR_TIMEOUT	||
		 stLatestCMStatus.ucInkTankStatus == WRN_INKTANK_INST_TIMEOUT))
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmInkOut
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmInkOut(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.ucGenStatus & INK_ERR)	&& 
		(stLatestCMStatus.ucInkTankStatus == ERR_INK_OUT))
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmInkLow
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmInkLow(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.ucGenStatus & INK_ERR)	&& 
		(stLatestCMStatus.ucInkTankStatus == WRN_INK_LOW))
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmWasteTankFull
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmWasteTankFull(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.ucGenStatus & WASTETANK_ERR)	&& 
		(stLatestCMStatus.ucWasteTankStatus == WRN_WTANK_FULL))
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmWasteTankNearFull
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmWasteTankNearFull(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.ucGenStatus & WASTETANK_ERR)	&& 
		(stLatestCMStatus.ucWasteTankStatus == WRN_WTANK_NEAR_FULL))
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmPaperError
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmPaperError(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.ucGenStatus & PAPER_ERR)	&& 
		(stLatestCMStatus.ucPaperStatus != ERR_NO_PAPER))
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmJamLeverOpen
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmJamLeverOpen(void)
{
	BOOL fRet = FALSE;

	if (stLatestCMStatus.usSensorStatus & JAM_LEVER_SENSOR_MASK)
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmPaperInTransport
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmPaperInTransport(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.usSensorStatus & S1_TRANS_SENSOR_MASK)	||
		(stLatestCMStatus.usSensorStatus & S2_TRANS_SENSOR_MASK)	||
		(stLatestCMStatus.usSensorStatus & S3_TRANS_SENSOR_MASK))
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsPaperInWOWTransport
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	05/20/2009	Raymond Shen		Initial version
//
// *************************************************************************/
BOOL fnIsPaperInWOWTransport(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.usSensorStatus & W2_SENSOR_MASK)	||
		(stLatestCMStatus.usSensorStatus & W3_SENSOR_MASK))
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsPaperInFeederTransport
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	05/20/2009	Raymond Shen		Initial version
//
// *************************************************************************/
BOOL fnIsPaperInFeederTransport(void)
{
	BOOL fRet = FALSE;

	if (stLatestCMStatus.usSensorStatus & FS2_SENSOR_MASK)
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmTopCoverOpen
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
BOOL fnIsCmTopCoverOpen(void)
{
	BOOL fRet = FALSE;

	if (stLatestCMStatus.usSensorStatus & TOP_COVER_SENSOR_MASK)
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmTapeError
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	05/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnIsCmTapeError(void)
{
	BOOL fRet = FALSE;

	if (stLatestCMStatus.ucGenStatus & TAPE_ERR)
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmTapeOut
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	1/17/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnIsCmTapeOut(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.ucGenStatus & TAPE_ERR)	&&
		(stLatestCMStatus.ucPaperStatus == ERR_NO_TAPE))
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmFeederFatalFault
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	05/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnIsCmFeederFatalFault(void)
{
	BOOL fRet = FALSE;

	if ((stLatestCMStatus.ucGenStatus & FDR_ERR) &&
		!(stLatestCMStatus.ucPaperStatus == ERR_FDR_NO_MAIL ||
		  stLatestCMStatus.ucPaperStatus == ERR_FDR_JAM))
	{	
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmFeederError
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	05/12/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnIsCmFeederError(void)
{
	BOOL fRet = FALSE;

	if (stLatestCMStatus.ucGenStatus & FDR_ERR)
	{	
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmFeederCoverOpen
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	05/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnIsCmFeederCoverOpen(void)
{
	BOOL fRet = FALSE;

	if (stLatestCMStatus.usSensorStatus & FDR_COVER_SENSOR_MASK)
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmS1Blocked
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	07/05/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnIsCmS1Blocked(void)
{
	BOOL fRet = FALSE;

	if (stLatestCMStatus.usSensorStatus & S1_TRANS_SENSOR_MASK)
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmS2Blocked
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	07/05/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnIsCmS2Blocked(void)
{
	BOOL fRet = FALSE;

	if (stLatestCMStatus.usSensorStatus & S2_TRANS_SENSOR_MASK)
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsCmS3Blocked
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	07/05/2006	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnIsCmS3Blocked(void)
{
	BOOL fRet = FALSE;

	if (stLatestCMStatus.usSensorStatus & S3_TRANS_SENSOR_MASK)
	{
		fRet = TRUE;
	}

	return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsWOWMailJammed
//
// DESCRIPTION: 
//      Function to check whether there is mail jammed on WOW transport.
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:     
//	06/24/2009  Raymond Shen    Removed check of sensor FS2
//	05/18/2009	Raymond Shen		Initial version     
//
// *************************************************************************/
BOOL fnIsWOWMailJammed(void)
{
    BOOL fRet = FALSE;
    UINT8   ucMode = fnOITGetPrintMode();

    if( (ucMode == PMODE_WOW) || (ucMode == PMODE_WEIGH_1ST) )
    {
        if( (stLatestCMStatus.usSensorStatus & W2_SENSOR_MASK)
                ||(stLatestCMStatus.usSensorStatus & W3_SENSOR_MASK) )
        {
            fRet = TRUE;
        }
    }
    
    return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnIsWOWSensorBlocked
//
// DESCRIPTION: 
//      Function to check whether there is mail jammed on WOW transport.
//
// INPUTS:
//		None.
//
// RETURNS:
//		TRUE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:     
//  06/23/2009  Jingwei,Li          Initial version     
//
// *************************************************************************/
BOOL fnIsWOWSensorBlocked(void)
{
    BOOL fRet = FALSE;
    UINT8   ucMode = fnOITGetPrintMode();

    if( (ucMode == PMODE_WOW) || (ucMode == PMODE_WEIGH_1ST) )
    {
        if(  (stLatestCMStatus.usSensorStatus & W2_SENSOR_MASK)
           ||(stLatestCMStatus.usSensorStatus & W3_SENSOR_MASK) )
        {
            fRet = TRUE;
        }
    }
    
    return fRet;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnClearCMSensorStatus
//
// DESCRIPTION: 
//      Clear latest Sensor status.
//
// INPUTS:
//		None.
//
// RETURNS:
//		None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	05/15/2018	Jane Li	Initial version
//
// *************************************************************************/
void fnClearCMSensorStatus (void)
{
    stLatestCMStatus.usSensorStatus &= ~(S1_MASK | S2_MASK | S3_MASK);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetCmFatalErrorCode
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		The fatal error code
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
UINT8 fnGetCmFatalErrorCode(void)
{
	if (stLatestCMStatus.ucGenStatus & FATAL_ERR)
	{
		return stLatestCMStatus.ucFatalErrorStatus;
	}
	else if (stLatestCMStatus.ucGenStatus & PSOC_ERR)
	{
		return stLatestCMStatus.ucPsocStatus;
	}
	else
	{
		return FTL_OK; // not cm fatal error
	}
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetCmPheadErrorCode
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		The fatal error code
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
UINT8 fnGetCmPheadErrorCode(void)
{
	if (stLatestCMStatus.ucGenStatus & PHEAD_ERR)
	{
		return stLatestCMStatus.ucPheadStatus;
	}
	else
	{
		return ERR_PHEAD_OK;
	}
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetCmInktankErrorCode
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		The fatal error code
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
UINT8 fnGetCmInktankErrorCode(void)
{
	if (stLatestCMStatus.ucGenStatus & INKTANK_ERR)
	{
		return stLatestCMStatus.ucInkTankStatus;
	}
	else
	{
		return ERR_INKTANK_OK;
	}
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetCmPaperErrorCode
//
// DESCRIPTION: 
//      
//
// INPUTS:
//		None.
//
// RETURNS:
//		The fatal error code
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/06/2006	Vincent Yi		Code cleanup 
//
// *************************************************************************/
UINT8 fnGetCmPaperErrorCode(void)
{
	if (stLatestCMStatus.ucPaperStatus != ERR_PAPER_OK)
	{
		return stLatestCMStatus.ucPaperStatus;
	}
	else if (stLatestCMStatus.usSensorStatus & JAM_LEVER_SENSOR_MASK)
	{
		return ERR_JAM_LEVER_OPEN;
	}
	else
	{
		return ERR_PAPER_OK;
	}
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnClearCMPaperErrorStatus
//
// DESCRIPTION: 
//      Clear latest Paper error status.
//
// INPUTS:
//		None.
//
// RETURNS:
//		None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	03/10/2006	Vincent Yi		Initial version
//
// *************************************************************************/
void fnClearCMPaperErrorStatus(void)
{
	stLatestCMStatus.ucGenStatus &= ~(PAPER_ERR);
	stLatestCMStatus.ucGenStatus &= ~(TAPE_ERR);
	stLatestCMStatus.ucGenStatus &= ~(FDR_ERR);
	stLatestCMStatus.ucPaperStatus = ERR_PAPER_OK;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnClearCMWasteTankError
//
// DESCRIPTION: 
//      Clear latest Waste Tank error.
//
// INPUTS:
//		None.
//
// RETURNS:
//		None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	12/19/2006	Vincent Yi		Initial version
//
// *************************************************************************/
void fnClearCMWasteTankError(void)
{
	stLatestCMStatus.ucGenStatus &= ~(WASTETANK_ERR);
	stLatestCMStatus.ucWasteTankStatus = 0;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnGetPlatCalibWeightStyle
//
// DESCRIPTION: 
//      Get platform calibration weight style.
//
// INPUTS:
//		None.
//
// RETURNS:
//		platform calibration weight style.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	09/12/2006	Raymond Shen		Initial version
//
// *************************************************************************/
UINT8 fnGetPlatCalibWeightStyle(void)
{
    return bPlatCalibWeightStyle;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetPlatCalibWeight
//
// DESCRIPTION: 
//      Get platform calibration weight value.
//
// INPUTS:
//		None.
//
// RETURNS:
//		calibration weight pointer.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//	09/12/2006	Raymond Shen		Initial version
//
// *************************************************************************/
UINT8* fnGetPlatCalibWeight()
{
    return (pPlatCalibWeight);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnClearMainMenuCheckEvent
//
// DESCRIPTION: 
//      If the system submode is indicia printing or permit printing and the system
//		  is stopped, then clear the Main Menu Check event.  The Main Menu Check event
//		  is set when checking the indicia conditions prior to printing.  Do not clear
//		  this event if printing reports or test prints.  
//
// INPUTS:
//		None
//
// RETURNS:
//		None.
//
// *************************************************************************/
void fnClearMainMenuCheckEvent(void)
{
	UINT8 ucMode, ucSubMode;
			 
	fnGetSysMode(&ucMode, &ucSubMode);

	// check system submode - if indicia or permit printing & system stopped
	if (((ucSubMode == SYS_INDICIA) || (ucSubMode == SYS_PERMIT)) && 
			(stLatestCMStatus.ucMailRunStatus == IS_STOPPED))
	{
		// clear main menu check event after a stop run during indicia printing
		// prevent mailpiece from starting before flag is set (ready to print)
		OSSetEvents(OIT_EVENT_GROUP, ~(UINT32) MAIN_MENU_CHK_END_EVT, OS_AND);	
	}	
	// else do not clear event - event does not need to be cleared for test prints
	// and reports
}

/* Pre/post functions */

/* Field functions */

/* Hard key functions */

/* Event functions */

/* Variable graphic functions */



/**********************************************************************
		Private Functions
**********************************************************************/

/* *************************************************************************
// FUNCTION NAME: 
//		fnmSYSInitialize
//
// DESCRIPTION: 
// 		OIT message handling function that processes Initialize	command from 
//		the SYS task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//		move logic from oitmsg.c
//
// MODIFICATION HISTORY:
// 	12/05/2005	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnmSYSInitialize(INTERTASK *pIntertask)
{
	/* this used to trigger displaying the first screen, but that was moved
		to an earlier in the function/ */
	fnDisplayFirstScreen();
	OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_OIT_INIT_COMPLETE, OS_OR);
	OSSendIntertask(SYS, OIT, OIT_INITIALIZE_RESPONSE, NO_DATA, NULL, 0);

	return SUCCESSFUL ;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmSYSChangeMode
//
// DESCRIPTION: 
// 		OIT message handling function that processes a mode change request 
//		from the SYS task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//		move logic from oitmsg.c
//
// MODIFICATION HISTORY:
// 	12/05/2005	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnmSYSChangeMode(INTERTASK *pIntertask)
{
	fnProcessModeChange(pIntertask->IntertaskUnion.bByteData[0], 
					    pIntertask->IntertaskUnion.bByteData[1]);

	return SUCCESSFUL ;
}

#if 0
/* *************************************************************************
// FUNCTION NAME: 
//		fnmSYSDisabledProfiles
//
// DESCRIPTION: 
// 		OIT message handling function that processes a profiles-disabled 
//		indication from the SYS task when UIC is changing from one print
//		job to another.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//		move logic from oitmsg.c
//
// MODIFICATION HISTORY:
// 	12/05/2005	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnmSYSDisabledProfiles(INTERTASK *pIntertask)
{
	/* this message is used to solve a race condition problem that
		occurs when	transitioning from a screen that is ready to
		print one thing	(such as a report) to a screen that will
	 	print another.  Simply receiving this message indicates that
		the profiles are disabled, so a prefunction can wait on
		the message to accomplish its needs.  This can easily be
		expanded or changed to an event if the need arises. */
	rWaitCtrl.lwMsgsReceived |= PROFILE_DISABLE_AWAITED_MASK;

	return SUCCESSFUL ;
}
#endif

/* *************************************************************************
// FUNCTION NAME: 
//		fnmCMStatusRsp
//
// DESCRIPTION: 
// 		OIT message handling function that processes status message
//		from the CM task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
// 	1/06/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
static BOOL fnmCMStatusRsp(INTERTASK *pIntertask)
{
	// Initialize respond status
	memset (&stRespondCMStatus, 0, sizeof(OIT_CM_STATUS));
	// Store the respond status
	stRespondCMStatus.ucMotionStatus 	= pIntertask->IntertaskUnion.bByteData[0];
	stRespondCMStatus.ucGenStatus 		= pIntertask->IntertaskUnion.bByteData[OIT_PROG_STATUS];
	stRespondCMStatus.ucFatalErrorStatus= pIntertask->IntertaskUnion.bByteData[OIT_PROG_FATAL_ERROR_STATUS];
	stRespondCMStatus.ucPaperStatus 	= pIntertask->IntertaskUnion.bByteData[OIT_PROG_PAPER_STATUS];
	stRespondCMStatus.ucPheadStatus 	= pIntertask->IntertaskUnion.bByteData[OIT_PROG_PHEAD_STATUS];
//	ucReserved5		
	stRespondCMStatus.ucInkTankStatus 	= pIntertask->IntertaskUnion.bByteData[OIT_PROG_INK_TANK_STATUS];
	stRespondCMStatus.ucWasteTankStatus = pIntertask->IntertaskUnion.bByteData[OIT_PROG_WASTE_TANK_STATUS];
	stRespondCMStatus.ucMailRunStatus 	= pIntertask->IntertaskUnion.bByteData[OIT_PROG_MAILRUN_STATUS];
	stRespondCMStatus.usSensorStatus 	= ((UINT16)(pIntertask->IntertaskUnion.bByteData[OIT_PROG_SENSOR_GROUP1_STATUS]) |
	                                                                            (UINT16)(pIntertask->IntertaskUnion.bByteData[OIT_PROG_SENSOR_GROUP2_STATUS] << 8));
//	ucBobStatus
	stRespondCMStatus.ucPsocStatus 		= pIntertask->IntertaskUnion.bByteData[OIT_PROG_PSOC_STATUS];	

	// Refresh the latest CM status for those bits with 1, that is occured error or warning
	stLatestCMStatus.ucGenStatus 		|= stRespondCMStatus.ucGenStatus;

	if (stRespondCMStatus.ucFatalErrorStatus != 0)
	{
		stLatestCMStatus.ucFatalErrorStatus = stRespondCMStatus.ucFatalErrorStatus;
	}
	if (stRespondCMStatus.ucPaperStatus != 0)
	{
		stLatestCMStatus.ucPaperStatus = stRespondCMStatus.ucPaperStatus;
	}

	if (stRespondCMStatus.ucPheadStatus != 0)
	{
		stLatestCMStatus.ucPheadStatus = stRespondCMStatus.ucPheadStatus;
	}

//	ucReserved5		
	
	if (stRespondCMStatus.ucInkTankStatus != 0)
	{
		stLatestCMStatus.ucInkTankStatus = stRespondCMStatus.ucInkTankStatus;
	}

	if (stRespondCMStatus.ucWasteTankStatus != 0)
	{
		stLatestCMStatus.ucWasteTankStatus = stRespondCMStatus.ucWasteTankStatus;
	}

	stLatestCMStatus.ucMailRunStatus = stRespondCMStatus.ucMailRunStatus;
		
	stLatestCMStatus.usSensorStatus	= stRespondCMStatus.usSensorStatus;

//	ucBobStatus

	stLatestCMStatus.ucPsocStatus |= stRespondCMStatus.ucPsocStatus;

	stLatestCMStatus.ucFeederStatus		= pIntertask->IntertaskUnion.bByteData[OIT_PROG_SENSOR_GROUP1_STATUS];

	fnGetPrePostWaitCtrlBlock()->lwMsgsReceived |= CM_STATUS_AWAITED_MASK;
	// Indicate sensor status is now known
    (void) OSSetEvents(OIT_EVENT_GROUP, SENSOR_STATUS_KNOWN_EVT, OS_OR);
	(void)fnProcessEvent(EVENT_GOT_CM_STATUS, pIntertask);
	
	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmCMProgress
//
// DESCRIPTION: 
// 		OIT message handling function that processes progress updates
//		from the CM task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
//				Henry Liu		Initial version
// 	1/06/2006	Vincent Yi		Code cleanup
//
// *************************************************************************/
static BOOL fnmCMProgress(INTERTASK *pIntertask)
{
	UINT8		ucMailRunEvent;
	BOOL		fEventProcessed = TRUE;

	memcpy(&stPreviousCMStatus, &stLatestCMStatus, sizeof(stLatestCMStatus));
    
	ucMailRunEvent = pIntertask->IntertaskUnion.bByteData[OIT_PROG_MAILRUN_EVENT];

	stLatestCMStatus.ucGenStatus 		= pIntertask->IntertaskUnion.bByteData[OIT_PROG_STATUS];
	stLatestCMStatus.ucFatalErrorStatus = pIntertask->IntertaskUnion.bByteData[OIT_PROG_FATAL_ERROR_STATUS];
	stLatestCMStatus.ucPaperStatus 		= pIntertask->IntertaskUnion.bByteData[OIT_PROG_PAPER_STATUS];
	stLatestCMStatus.ucPheadStatus 		= pIntertask->IntertaskUnion.bByteData[OIT_PROG_PHEAD_STATUS];
//	ucReserved5
	stLatestCMStatus.ucInkTankStatus 	= pIntertask->IntertaskUnion.bByteData[OIT_PROG_INK_TANK_STATUS];
	stLatestCMStatus.ucWasteTankStatus 	= pIntertask->IntertaskUnion.bByteData[OIT_PROG_WASTE_TANK_STATUS];
	stLatestCMStatus.ucMailRunStatus 	= pIntertask->IntertaskUnion.bByteData[OIT_PROG_MAILRUN_STATUS];
	stLatestCMStatus.usSensorStatus 	= ((UINT16)(pIntertask->IntertaskUnion.bByteData[OIT_PROG_SENSOR_GROUP1_STATUS]) |
                                                                                    (UINT16)(pIntertask->IntertaskUnion.bByteData[OIT_PROG_SENSOR_GROUP2_STATUS] << 8));
#ifdef NO_PRINTER
	stLatestCMStatus.usSensorStatus = 0;
#endif
	stLatestCMStatus.ucBobStatus 		= pIntertask->IntertaskUnion.bByteData[OIT_PROG_BOB_STATUS];
	stLatestCMStatus.ucPsocStatus 		= pIntertask->IntertaskUnion.bByteData[OIT_PROG_PSOC_STATUS];	

	switch (ucMailRunEvent)
	{
	case EVT_MAILRUN_STATUS_UPDATE:
		fEventProcessed = fnForwardMailRunStatusUpdateMsg(pIntertask, &stLatestCMStatus);
		break;

	case EVT_MAIL_PIECE_PROCESSED:
		fEventProcessed = fnForwardMailPieceProcessedMsg(pIntertask, &stLatestCMStatus);
		break;
	
	case EVT_BOB_ERR:
		fEventProcessed = fnForwardBOBError(pIntertask, &stLatestCMStatus);
		break;
	
	case EVT_PM_FATAL_ERR:
		fEventProcessed = fnForwardPMFatalError(pIntertask, &stLatestCMStatus);
		break;

	case EVT_PM_PAPER_ERR:
		fEventProcessed = fnForwardPMPaperError(pIntertask, &stLatestCMStatus);
		break;

	case EVT_PM_PHEAD_ERR:
		fEventProcessed = fnForwardPMPrintheadError(pIntertask, &stLatestCMStatus);
		break;

	case EVT_PM_INK_ERR:
		fEventProcessed = fnForwardPMInkError(pIntertask, &stLatestCMStatus);
		break;

	case EVT_PM_INKTANK_ERR:
		fEventProcessed = fnForwardPMInkTankError(pIntertask, &stLatestCMStatus);
		break;

	case EVT_PM_WASTETANK_ERR:
		fEventProcessed = fnForwardPMWasteTankError(pIntertask, &stLatestCMStatus);
		break;

		/* the cover open status will be hold if the mail piece is processing 
			and will be sent after the mail pieces processed. */
	case EVT_TOP_COVER_STATUS_UPDATE:
		fEventProcessed = fnForwardTopCoverStatus(pIntertask, &stLatestCMStatus);
		break;

	case EVT_JAM_LEVER_STATUS_UPDATE:
		fEventProcessed = fnForwardJamLeverStatus(pIntertask, &stLatestCMStatus);
		break;

	case EVT_SENSOR_STATUS_UPDATE:
		fEventProcessed = fnForwardSensorsStatus(pIntertask, &stLatestCMStatus);
		fnClearMainMenuCheckEvent();
		break;

	case EVT_PSOC_ERR:
		fEventProcessed = fnForwardPSOCError(pIntertask, &stLatestCMStatus);
		break;
	
	case EVT_FDR_COVER_STATUS_UPDATE:
		fEventProcessed = fnForwardFeederCoverStatus(pIntertask, &stLatestCMStatus);
		break;
		
	case EVT_TAPE_ERR:
		fEventProcessed = fnForwardTapeError(pIntertask, &stLatestCMStatus);
		break;

	case EVT_FDR_MAIL_ADDED:
		fEventProcessed = fnForwardFeederAddedMailMsg(pIntertask, &stLatestCMStatus);
		break;

	case EVT_FDR_NO_MAIL:
		fEventProcessed = fnForwardFeederNoMailError(pIntertask, &stLatestCMStatus);
		break;

	case EVT_FDR_ERR:
		fEventProcessed = fnForwardFeederError(pIntertask, &stLatestCMStatus);
		break;

	case EVT_WOW_MAIL_JAM_STATUS_UPDATE:
		fEventProcessed = fnForwardFeederWOWMailJamStatus(pIntertask, &stLatestCMStatus);
		break;

	case EVT_WOW_SBR_ERR:
		fEventProcessed = fnForwardFeederWOWSBRError(pIntertask, &stLatestCMStatus);
		break;

	case EVT_WOW_DIM_RATING_ERR:
		fEventProcessed = fnForwardFeederWOWDimRatingErr(pIntertask, &stLatestCMStatus);
		break;

	default:
	break;
	} // switch (...)

	//check if there is any pending writes
	if(trmGetState() == TRM_STATE_PENDING_TX_FILE_SAVE)
	{
		dbgTrace(DBG_LVL_INFO, "TRM-fnmCMProgress performing Pending Trx Write ");
		trmProcessTxPendingWrite();
	}

//	if ( (ucMailRunEvent != EVT_MAILRUN_STATUS_UPDATE) &&
//			(ucMailRunEvent != EVT_MAIL_PIECE_PROCESSED) )
//		trm_recover_mp();

//xit:
	/* sometime CM cannot obtain the exact the printer status in the 
		CO_GET_STATUS_RSP message and sends OI the progress message in the 
		middle of OI request the mode change with SYS instead, in this case 
		the progress message handling is ignored, so we add a flag here and 
		force to check the printer status again. */
	if (!fEventProcessed)
	{
		fRecheckPrinterStatus = TRUE;
	}

	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmCMMaintenanceRsp
//
// DESCRIPTION: 
// 		OIT message handling function that processes maintenance response message
//		from the CM task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	08/15/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL	fnmCMMaintenanceRsp(INTERTASK *pIntertask)
{
	UINT32 ulStatus = (UINT32)pIntertask->IntertaskUnion.lwLongData[0];

	if (ulStatus == 0)
	{	// OK
		(void)fnProcessEvent(EVENT_CM_MTNC_OK, pIntertask);
	}
	else
	{	
		(void)fnProcessEvent(EVENT_CM_MTNC_FAILED, pIntertask);
	}

	return (SUCCESSFUL);
}

/* **********************************************************************
// FUNCTION NAME: fnmCMClrErrRsp
// DESCRIPTION: OIT message handling function that processes clearing error
//				message responding from the CM task.
//
// AUTHOR: Victor Li
//
// INPUTS:	pIntertask- intertask message that is being processed
//
// OUTPUTS:	Always returns SUCCESSFUL 
// *************************************************************************/
static BOOL fnmCMClrErrRsp(INTERTASK *pIntertask)
{
	UINT32 ulStatus = (unsigned long)pIntertask->IntertaskUnion.lwLongData[0];

	if (ulStatus == 0)
	{	// OK
		fnSetPaperErrorShownStatus(FALSE);

        if (fnIsCmPaperInTransport())
        {
            //Clear latest sensor status when paper error is cleared.
            //After the Paper Cleared event, the status will be updated again due
            //to the calling of fnRequestCMStatus in fnForwardJamLeverStatus.
            fnDumpStringToSystemLog("fnmCMClrErrRsp:clear latest sensor status");
            fnClearCMSensorStatus();
        }
		
		if (fnIsCmPaperError() == TRUE 	||
			fnIsCmTapeError() == TRUE	||
			fnIsCmFeederError() == TRUE)
		{

//			(void)fnProcessEvent(EVENT_ERROR_CLEARED, pIntertask);

			fnClearCMPaperErrorStatus();
			SendBaseEventToTablet(BASE_EVENT_PAPER_CLEARED, NULL);
		}

		if(fnIsCmWasteTankFull() == TRUE){
			fnClearCMWasteTankError();
		}

	}
	else
	{	
		(void)fnProcessEvent(EVENT_ERROR_NOT_CLEARED, pIntertask);
	}



	return (SUCCESSFUL);
}

/* **********************************************************************
// FUNCTION NAME: fnmCMRplStartRsp
// DESCRIPTION: OIT message handling function that processes replacement
// 				start response message from the CM task.
//
// AUTHOR: Victor Li
//
// INPUTS:	pIntertask- intertask message that is being processed
//
// OUTPUTS:	Always returns SUCCESSFUL 
// *************************************************************************/
static BOOL fnmCMRplStartRsp(INTERTASK *pIntertask)
{
	UINT32 ulStatus = (UINT32)pIntertask->IntertaskUnion.lwLongData[0];

	if (ulStatus == 0)
	{	// OK
//		fnDiagSetPMReplacementReady();
		// trigger an evernt here
		(void)fnProcessEvent(EVENT_PM_REPLACEMENT_READY, pIntertask);
	}
	else
	{	
		(void)fnProcessEvent(EVENT_PM_REPLACEMENT_NOT_READY, pIntertask);
	}
	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmCMRplCmpltRsp
//			
// DESCRIPTION: 
// 		OIT message handling function that processes replacement complete 
//		response message from the CM task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//		move logic from oitmsg.c
//
// MODIFICATION HISTORY:
//	04/25/2005	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnmCMRplCmpltRsp(INTERTASK *pIntertask)
{
	UINT32 ulStatus = (UINT32)pIntertask->IntertaskUnion.lwLongData[0];

	if (ulStatus == 0)
	{	// OK
		(void)fnProcessEvent(EVENT_REPLACE_COMPLETE, pIntertask);
	}
	else
	{	
		(void)fnProcessEvent(EVENT_REPLACE_FAILED, pIntertask);
	}

	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmCMSetMarginRsp
//			
// DESCRIPTION: 
// 		OIT message handling function that processes CO_SET_MARGIN_RSP from 
//		the CM task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//		move logic from oitmsg.c
//
// MODIFICATION HISTORY:
//	2/07/2005	Vincent Yi		Modified to create OI events for CO_SET_MARGIN_RSP
// 				Henry Liu		Initial version
//
// *************************************************************************/
static BOOL fnmCMSetMarginRsp(INTERTASK *pIntertask)
{
	UINT8	ucCMRspStatus = pIntertask->IntertaskUnion.bByteData[0];
	//UINT8	ucCMRspErrCode = pIntertask->IntertaskUnion.bByteData[1];

	if (ucCMRspStatus == 0)
	{	// OK
		(void)fnProcessEvent(EVENT_SET_MARGIN_OK, pIntertask);
	}
	else
	{	// Failed
		(void)fnProcessEvent(EVENT_SET_MARGIN_FAILED, pIntertask);
	}

	return (SUCCESSFUL);
}
static BOOL fnmCMFeederDiagRsp(INTERTASK *pIntertask)
{
//    fnProcessFeederDiagRsp(pIntertask);

    fnGetPrePostWaitCtrlBlock()->lwMsgsReceived |= CM_DIAG_AWAITED_MASK;

//	(void)fnProcessEvent(EVENT_FEEDER_DIAG_STATUS,pIntertask);
	return (SUCCESSFUL);
}
/* *************************************************************************
// FUNCTION NAME: 
//		fnmCMDiagActnRsp
//			
// DESCRIPTION: 
// 		OIT message handling function that processes CO_DIAG_ACTN_RSP from 
//		the CM task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//		None
//
// MODIFICATION HISTORY:
//	06/30/2005	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnmCMDiagActnRsp(INTERTASK *pIntertask)
{
//	fnDiagActionResponse (pIntertask);
	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmCMGetDataRsp
//			
// DESCRIPTION: 
// 		OIT message handling function that processes CO_GET_DATA_RSP from 
//		the CM task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//		None
//
// MODIFICATION HISTORY:
//	06/30/2005	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnmCMGetDataRsp(INTERTASK *pIntertask)
{
//	fnDiagGetDataResponse (pIntertask);
	fnGetPrePostWaitCtrlBlock()->lwMsgsReceived |= CM_DATA_AWAITED_MASK;
	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmKEYReceiveKeyData
//
// DESCRIPTION: 
// 		OIT message handling function that handles a key code from the UI meter 
//		keyboard.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//		move logic from oitmsg.c
//
// MODIFICATION HISTORY:
// 	12/05/2005	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnmKEYReceiveKeyData(INTERTASK *pIntertask)
{
	// if we get the key sequence 'SHIFT-SYMBOL-STOP', reboot...
	//
	// This has been implemented at the request of Human Factors.
	//
	if (pIntertask->IntertaskUnion.bByteData[0] == KBD_STOP_KEY &&
		pIntertask->IntertaskUnion.bByteData[1] == KEYDOWN &&
		pIntertask->IntertaskUnion.bByteData[2] == SHIFT_KEYCODE_3)
	{
		RebootSystem();
	}

	fnProcessKey(pIntertask->IntertaskUnion.bByteData[0], 
				 pIntertask->IntertaskUnion.bByteData[1],
	  			 pIntertask->IntertaskUnion.bByteData[2]);

	return SUCCESSFUL ;
}

/* **********************************************************************
// FUNCTION NAME: fnmOITScreenTick
// DESCRIPTION: OIT message handling function that handles a queued
//				screen tick event.  There is a flag that prevents the timer
//				expiration routine from queueing more than one event.  When
//				we handle the event, we clear the flag in this function.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:	pIntertask- intertask message that is being processed
//
// OUTPUTS:	Always returns SUCCESSFUL 
// *************************************************************************/
static BOOL fnmOITScreenTick(INTERTASK *pIntertask)
{
	// if the event is processed, clear the flag
	if ( fnProcessEvent(EVENT_SCREEN_TICK_OCCURRED, pIntertask) == TRUE)
		fScreenTickEventQueued = FALSE;

	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmOITMidnightComing
//
// DESCRIPTION: 
// 		OIT message handling function that processes midnight coming message
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	09/13/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnmOITMidnightComing (INTERTASK *pIntertask)
{
	SINT16	sAdvDays = GetPrintedDateAdvanceDays();

	if (sAdvDays != 1)
	{
		fnProcessEvent(EVENT_MIDNIGHT_COMING, pIntertask);
	}

	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//        fnmABAConnectionResult
//
// DESCRIPTION:
//        OIT message handling function that handles the results of the modem/PPP connection.
//
// INPUTS:
//        pIntertask- pointer to the intertask message that is currently
//                    being processed.
//
// RETURNS:
//        Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION HISTORY:
//        02/06/2009    Bob Li  Merged from Mega codes and update it. 
//        04/03/2001  mozdzjm   Initial Version
// *************************************************************************/
static BOOL fnmABAConnectionResult(INTERTASK *pIntertask)
{
//TODO - cleanly remove PPP dependent code
#if 0  //obsolete code - needs nu_ppp.h to compile, which is problematic
    SINT32 lStatus = NU_SUCCESS;

    if (pIntertask->bMsgType == LONG_DATA)
    {
        lStatus = (SINT32)pIntertask->IntertaskUnion.lwLongData[0];
        switch ( lStatus )
        {
            case NU_SUCCESS:
                //if (fOITAbortModemConnection == FALSE)
                {   // No abort operation, send service request to dist task
                    (void)fnProcessEvent((UINT8)EVENT_DC_DIALING_DONE, pIntertask);
                }
                /* Bob Li
                else                
                {
                    (void)fnProcessEvent((UINT8)EVENT_DC_DIALING_DONE, pIntertask);
                }*/
                break;
            case NU_BUSY:
                (void)fnProcessEvent((UINT8)EVENT_DC_BUSY, pIntertask);
                break;
            case NU_INVALID_LINK:
            case NU_NO_CARRIER:
            case NU_NO_MODEM:
            case NU_NO_CONNECT:
            case NU_NCP_FAILED:
            case NU_LCP_FAILED:
            case NU_LOGIN_FAILED:
            case NU_NETWORK_BUSY:
            default:
                (void)fnProcessEvent((UINT8)EVENT_DC_DIALING_DONE, pIntertask);
        }
    }
    else
    {
        (void)fnProcessEvent((UINT8)EVENT_DC_DIALING_DONE, pIntertask);
    }
#endif
    return (SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardPMFatalError
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardPMFatalError(INTERTASK 	*	pIntertask, 
								  OIT_CM_STATUS *	pCurCMStatus)
{
	BOOL retVal = TRUE;
	retVal = fnProcessEvent(EVENT_PRINTER_FAULT, pIntertask);
	SendBaseEventToTablet(BASE_EVENT_PRINTER_ERROR, NULL);
	return retVal;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardPMPrintheadError
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardPMPrintheadError(INTERTASK 	*	pIntertask, 
									  OIT_CM_STATUS * 	pCurCMStatus)
{
	BOOL fProcessed = FALSE;

	if (pCurCMStatus->ucPheadStatus == ERR_NO_PHEAD)
	{
		fProcessed = fnProcessEvent(EVENT_NO_PRINTHEAD, pIntertask); 
		SendBaseEventToTablet(BASE_EVENT_NO_PRINT_HEAD, NULL);
	}
	else
	{
		fProcessed = fnProcessEvent(EVENT_PRINTHEAD_ERROR, pIntertask); 
		SendBaseEventToTablet(BASE_EVENT_PRINT_HEAD_ERROR, NULL);
	}

	return fProcessed;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardPMInkTankError
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardPMInkTankError(INTERTASK 	  *	pIntertask, 
									OIT_CM_STATUS * pCurCMStatus)
{
	BOOL fProcessed = FALSE;

	switch (pCurCMStatus->ucInkTankStatus)
	{
	case ERR_NO_INKTANK:
		fProcessed = fnProcessEvent(EVENT_NO_INKTANK, pIntertask); 
		SendBaseEventToTablet(BASE_EVENT_NO_INK_TANK, NULL);
		break;
	case ERR_INKTANK_LID_OPEN: 
		fProcessed = fnProcessEvent(EVENT_INKTANK_LID_OPEN, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_INK_TANK_LID_OPEN, NULL);
		break;
	case WRN_INKTANK_MRTR_TIMEOUT: 
	case WRN_INKTANK_INST_TIMEOUT:
		fProcessed = fnProcessEvent(EVENT_INKTANK_EXPIRING, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_INK_EXPIRING_SOON, NULL);
		break;
	default:
		fProcessed = fnProcessEvent(EVENT_INKTANK_ERROR, pIntertask); 
		SendBaseEventToTablet(BASE_EVENT_INK_TANK_ERROR, NULL);
		break;
	}

	return fProcessed;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardPMWasteTankError
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardPMWasteTankError(INTERTASK 	*	pIntertask, 
									  OIT_CM_STATUS * 	pCurCMStatus)
{
	BOOL fProcessed = FALSE;

	switch (pCurCMStatus->ucWasteTankStatus)
	{
	case WRN_WTANK_NEAR_FULL:
		fProcessed = fnProcessEvent(EVENT_WASTETANK_NEAR_FULL, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_WASTE_TANK_NEAR_FULL, NULL);
		break;
	case WRN_WTANK_FULL:
		fProcessed = fnProcessEvent(EVENT_WASTETANK_FULL, pIntertask); 
		SendBaseEventToTablet(BASE_EVENT_WASTE_TANK_FULL, NULL);
		break;
	default:
		break;
	}

	return fProcessed;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardPMInkError
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardPMInkError(INTERTASK 	  *	pIntertask, 
								OIT_CM_STATUS * pCurCMStatus)	
{
	BOOL fProcessed = FALSE;

	switch (pCurCMStatus->ucInkTankStatus)
	{
	case WRN_INK_LOW:
		fProcessed = fnProcessEvent(EVENT_INK_LOW, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_LOW_INK, NULL);
		break;
	case ERR_INK_OUT:
		fProcessed = fnProcessEvent(EVENT_INK_OUT, pIntertask); 
		SendBaseEventToTablet(BASE_EVENT_INK_OUT, NULL);
		break;
	default:
		break;
	}

	return fProcessed;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardPMPaperError
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardPMPaperError(INTERTASK 	*	pIntertask, 
								  OIT_CM_STATUS * 	pCurCMStatus)	
{
	BOOL fProcessed = FALSE;

    if (pCurCMStatus->ucPaperStatus == ERR_JAM_LEVER_OPEN)
    {
        fProcessed = fnProcessEvent(EVENT_JAM_LEVER_OPEN, pIntertask);
        SendBaseEventToTablet(BASE_EVENT_JAM_LEVER_OPEN, NULL);
    }
    else
    {
		char        	pLogBuf[80];
	    sprintf( pLogBuf, "Paper Error Type : %x", pCurCMStatus->ucPaperStatus );
	    fnDumpStringToSystemLog( pLogBuf );

        //If "No Paper" error occurs, don't send base event "Paper Error" to tablet in order to not
        // display any error on the tablet. As the error has been cleared in fillPmFdrErrState(), 
        // don't report paper error event that change the screen from printing to ErrPaperError.  
        if(pCurCMStatus->ucPaperStatus == ERR_NO_PAPER)
        {        
            return TRUE;
        }

	    fProcessed = fnProcessEvent(EVENT_PAPER_ERROR, pIntertask); 

	    cJSON *parameters = cJSON_CreateObject();

	    if(pCurCMStatus->ucPaperStatus == ERR_PAPER_SKEW)
	    {
	    	cJSON_AddStringToObject(parameters, "Type", "Mail Skewed");
 	    }
	    else if(pCurCMStatus->ucPaperStatus == ERR_PIECE_TOO_SHORT)
	    {
	       	cJSON_AddStringToObject(parameters, "Type", "Mail Too Short");
	    }
	    else if(pCurCMStatus->ucPaperStatus == ERR_JAM)
	    {
	      	cJSON_AddStringToObject(parameters, "Type", "Mail Jammed");
	    }
	    else if(pCurCMStatus->ucPaperStatus == ERR_PIECE_TOO_FAST)
	    {
	      	cJSON_AddStringToObject(parameters, "Type", "Piece Too Fast");
	    }
	    else if(pCurCMStatus->ucPaperStatus == ERR_NO_PRINT)
	    {
	      	cJSON_AddStringToObject(parameters, "Type", "No Print");
	    }
		else
		{
	      	cJSON_AddStringToObject(parameters, "Type", "Unknown");
		}

	    SendBaseEventToTablet(BASE_EVENT_PAPER_ERROR, parameters);

    }

/*Vincent
	else if (fnIsInOOBMode() == TRUE )
	{
		fEventProcessed = fnProcessEvent(EVENT_PAPER_ERROR, pIntertask); 
	}
	else if (fnGetCurrentReportType() == REFILL_RECEIPT_RPT) 
	{
		fEventProcessed = fnProcessEvent(EVENT_PAPER_ERROR, pIntertask);
	} 		
*/

	return fProcessed;
}		

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardMailPieceProcessedMsg
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
//  07/21/2006  Dicky Sun       Delete checking ucGenStatus for GTS single
//                              piece mode when we have a warning.
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardMailPieceProcessedMsg(INTERTASK 	 *	pIntertask, 
										   OIT_CM_STATUS * 	pCurCMStatus)	
{
	BOOL fProcessed = FALSE;

/*Vincent
	if( wMailSimulation.ReadyToSimu && *lwCountPtr == 0 )
	{
			wMailSimulation.TimeInterval = 0;
			wMailSimulation.NumTrips =0;
			wMailSimulation.ReadyToSimu = FALSE;
			wMailSimulation.fInvalidValueTime = FALSE;
			wMailSimulation.fInvalidValueMail = FALSE;
	}
*/
	if (//pCurCMStatus->ucGenStatus == OK 	&& 
		pCurCMStatus->ucMailRunStatus == WAITING_FOR_MAIL )
	{
		if ( fnKipPasswdCountReached() == TRUE)
		{
			pKIPPasswordInfo->ucPasswordSelectPurpose = KIPW_BATCH_LIMIT_REACHED ;
			pKIPPasswordInfo->ucPasswordStatus = KIP_PW_BATCH_LIMIT ;
			fProcessed = fnProcessEvent(EVENT_KIP_COUNT_REACHED, pIntertask);
			SendBaseEventToTablet(BASE_EVENT_KIP_COUNT_REACHED, NULL);
		}
		else
		{
            UINT32      lwPostageVal;
            UINT8 		ucPrintMode = fnOITGetPrintMode();
			fPieceCompleted = TRUE;
			fProcessed = fnProcessEvent(EVENT_MAIL_PIECE_COMPLETED, pIntertask);

			if(ucPrintMode == PMODE_AD_ONLY || ucPrintMode == PMODE_TIME_STAMP)
			{
					SendBaseEventToTablet(BASE_EVENT_MAILPIECE_COMPLETE, NULL);
			}
			else
			{

				if(fnSYSWhichSubMode() == OIT_REPORT || fnSYSWhichSubMode() == OIT_SEAL_ONLY
						|| fnSYSWhichSubMode() == OIT_TEST_PATTERN )
				{
					if(bIsCorrecteDate == TRUE)
					{
						bIsCorrecteDate = FALSE;
						fnClearDateCorrection();
					}
					SendBaseEventToTablet(BASE_EVENT_MAILPIECE_COMPLETE, NULL);
				}
				else
				{
					cJSON *parameters = cJSON_CreateObject();

					//fnGetDebitValue(&lwPostageVal);
					EndianAwareCopy(&lwPostageVal, bobsPostageValue, sizeof(lwPostageVal));

					cJSON_AddNumberToObject(parameters, "Postage", lwPostageVal);
					get_psd_regs_cJSON(parameters);
					SendBaseEventToTablet(BASE_EVENT_MAILPIECE_COMPLETE, parameters);
				}
			}

		}
	}

	if (pCurCMStatus->ucBobStatus == SCM_LOW_FUNDS_WARNING)
	{
        fnDumpStringToSystemLog( "LowFundsWarn SCM flag read in fnForwardMailPieceProcessedMsg" );
		fLowFundsWarningPending = TRUE;
		fProcessed = fnProcessEvent(EVENT_LOW_FUNDS, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_LOW_FUNDS, NULL);
	}
/* by Joey Cui, Funds will not increase during mail running
	else
	{
		fLowFundsWarningPending = FALSE;
	}
*/
	
	if (pCurCMStatus->ucBobStatus == SCM_PC_END_OF_LIFE_WARNING)
	{
		fLowPieceCountWarningPending = TRUE;
		fProcessed = fnProcessEvent(EVENT_PIECE_COUNT_LOW, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_LOW_PIECE_COUNT, NULL);
	}
	else
	{
		fLowPieceCountWarningPending = FALSE;
	}

	return fProcessed;
}		

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardTopCoverStatus
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardTopCoverStatus(INTERTASK 	  *	pIntertask, 
									OIT_CM_STATUS * pCurCMStatus)	
{
	BOOL fProcessed = FALSE;

	if (pCurCMStatus->usSensorStatus & TOP_COVER_SENSOR_MASK)
	{
		fProcessed = fnProcessEvent(EVENT_COVER_OPEN, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_TOP_COVER_OPEN, NULL);
	}
	else
	{
		fProcessed = fnProcessEvent(EVENT_COVER_CLOSED, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_TOP_COVER_CLOSED, NULL);
	}

	return fProcessed;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardJamLeverStatus
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardJamLeverStatus(INTERTASK 	  *	pIntertask, 
									OIT_CM_STATUS * pCurCMStatus)	
{
	BOOL fProcessed = FALSE;

	if (pCurCMStatus->usSensorStatus & JAM_LEVER_SENSOR_MASK)
	{
		UINT32 pMsgData[2];
    	pMsgData[0] = OIT_LEFT_PRINT_RDY; //OIT_LEFT_PRINT_RDY;
    	pMsgData[1] = 0;
    	(void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);

		fProcessed = fnProcessEvent(EVENT_JAM_LEVER_OPEN, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_TRANSPORT_OPEN, NULL);
	}
	else
	{

		UINT32 pMsgData[2];
	    unsigned char  ucOIErrorCode = fnGetCmPaperErrorCode();

	    if(ucOIErrorCode != ERR_PAPER_OK){

//Ram added to clear paper error after closing Transport.
	        // error type, not equal to the last status
	        pMsgData[0] = (UINT32) ERTYPE_PAPER_ERR;
	        // sometime sensor status update will cause jam lever open, not only
	        // paper error does, so we use ucOIErrorCode that is prepared in the
	        // pre function of this screen instead of bLastPaperStatus;
	        pMsgData[1] = (UINT32) ucOIErrorCode;

	        (void)OSSendIntertask(BJCTRL, CM, CP_CLR_PRT_ERR, LONG_DATA, pMsgData, sizeof(pMsgData));

	        (void)OSSendIntertask(CM, OIT, OC_CLR_ERR_REQ, LONG_DATA, pMsgData,
	                                sizeof(pMsgData));

	    }

    	//Send mode change msg to OIT task
/*
    	pMsgData[0] = OIT_LEFT_PRINT_RDY; //OIT_LEFT_PRINT_RDY;
    	pMsgData[1] = 0;
    	(void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);
*/
		bGoToReady = FALSE;
	    fProcessed = fnProcessEvent(EVENT_JAM_LEVER_CLOSED, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_TRANSPORT_CLOSED, NULL);
		fnRequestCMStatus();
	}


	return fProcessed;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardSensorsStatus
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardSensorsStatus(INTERTASK 	 *	pIntertask, 
								   OIT_CM_STATUS * 	pCurCMStatus)	
{
	BOOL fProcessed = FALSE;

	if (!(pCurCMStatus->usSensorStatus & S1_TRANS_SENSOR_MASK)		&& 
		!(pCurCMStatus->usSensorStatus & S2_TRANS_SENSOR_MASK)		&& 
		!(pCurCMStatus->usSensorStatus & S3_TRANS_SENSOR_MASK))
	{
//		fProcessed = fnProcessEvent(EVENT_PAPER_CLEARED, pIntertask);
//		SendBaseEventToTablet(BASE_EVENT_PAPER_CLEARED, NULL); //not to send Paper cleared event to Tablet,
																//Any queries in the transport when paper is removed.
		fnSetPaperErrorShownStatus(FALSE);
	}
	else
	{
		if (pCurCMStatus->usSensorStatus & S1_TRANS_SENSOR_MASK)
		{
			//Not to send Paper inserted when it is not ready
		    UINT8   ucMode;
		    UINT8   ucSubMode;
			fnGetSysMode(&ucMode, &ucSubMode);
			if(ucMode !=  SYS_PRINT_NOT_READY && pCurCMStatus->ucPaperStatus != ERR_NO_PAPER){
				fProcessed = fnProcessEvent(EVENT_PAPER_INSERTED, pIntertask);
				SendBaseEventToTablet(BASE_EVENT_PAPER_INSERTED, NULL);
			}
		}
	}
	fProcessed = fnProcessEvent (EVENT_CM_SENSOR_UPDATE, pIntertask);
//	SendBaseEventToTablet("sensor updated", NULL);
	return fProcessed;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardFeederWOWMailJamStatus
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
//  06/24/2009  Raymond Shen    Removed check of sensor FS2
//  06/22/2009  Jingwei,Li      Updated sensor mask.
//  05/18/2009  Raymond Shen    Initial version
//
// *************************************************************************/
static BOOL fnForwardFeederWOWMailJamStatus(INTERTASK 	 *	pIntertask, 
								   OIT_CM_STATUS * 	pCurCMStatus)	
{
	BOOL fProcessed = FALSE;

	if((pCurCMStatus->usSensorStatus & W2_SENSOR_MASK)		||
		(pCurCMStatus->usSensorStatus & W3_SENSOR_MASK))
	{
		fProcessed = fnProcessEvent(EVENT_PAPER_ON_WOW_TRANSPORT, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_PAPER_IN_WOW_TRANSPORT, NULL);

	}
	else if(!(pCurCMStatus->usSensorStatus & W2_SENSOR_MASK)		&& 
		!(pCurCMStatus->usSensorStatus & W3_SENSOR_MASK))
	{
		fProcessed = fnProcessEvent(EVENT_PAPER_CLEARED, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_PAPER_CLEARED, NULL);

	}

	return fProcessed;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardBOBError
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		 Initial version
//  09/15/2014  Renhao           Modified code to fix fraca 227107.
// *************************************************************************/
static BOOL fnForwardBOBError(INTERTASK 	*	pIntertask, 
							  OIT_CM_STATUS * 	pCurCMStatus)	
{
	BOOL 	fProcessed = FALSE;
	UINT8	bBaseErrorClass;
	UINT8	bBaseErrorCode;

	switch (pCurCMStatus->ucBobStatus)
	{
	case SCM_INSUFFICIENT_FUNDS:
	    fnSendStopMailRequest();
		fProcessed = fnProcessEvent(EVENT_INSUFFICIENT_FUNDS, pIntertask);
		(void)fnBobErrorInfo(&bBaseErrorClass, &bBaseErrorCode);
		fnPostDisabledStatus(DCOND_INSUFFICIENT_FUNDS);
		SendBaseEventToTablet(BASE_EVENT_INSUFFICIENT_FUNDS, NULL);
		break;

	case SCM_PC_END_OF_LIFE_ERR:
		fProcessed = fnProcessEvent(EVENT_PIECE_COUNT_EOL, pIntertask);
		(void)fnBobErrorInfo(&bBaseErrorClass, &bBaseErrorCode);
		SendBaseEventToTablet(BASE_EVENT_END_OF_LIFE, NULL);
		break;
		
	case SCM_POSTAGE_OUT_OF_RANGE:
		fProcessed = fnProcessEvent(EVENT_POSTAGE_OUT_OF_RANGE,pIntertask);
		(void)fnBobErrorInfo(&bBaseErrorClass, &bBaseErrorCode);
		SendBaseEventToTablet(BASE_EVENT_POSTAGE_OUT_OF_RANGE, NULL);
		break;

	case SCM_LOW_FUNDS_WARNING: // ingore it
        fnDumpStringToSystemLog( "LowFundsWarn SCM flag ignored in fnForwardBOBError" );
		break;

	case ACCT_ACCOUNT_OUTTA_FUNDS:
	case ACCT_ACCOUNT_OUTTA_PEACE:
	case ACCT_UNKNOWN_ACCOUNT_ERR:
// FIXME: uncomment the following 3 lines when EVENT_ACCT_FULL is implemented, defilcj
//					fEventProcessed = fnProcessEvent(EVENT_ACCT_FULL, pIntertask);
//					if (fEventProcessed)
//						fnBobErrorInfo(&bBaseErrorClass, &bBaseErrorCode);
//					break;

	default:
		fnProcessSCMError();
		break;
	}

	return fProcessed;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardPSOCError
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardPSOCError(INTERTASK 	 *	pIntertask, 
							   OIT_CM_STATUS * 	pCurCMStatus)	
{
	BOOL fProcessed = FALSE;

	switch (pCurCMStatus->ucPsocStatus)
	{
	case ERR_PSOC:
		fProcessed = fnProcessEvent(EVENT_PRINTER_FAULT, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_PSOC_ERROR, NULL);
		break;
	default:
		break;
	}

	return fProcessed;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardFeederCoverStatus
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardFeederCoverStatus(INTERTASK 	  *	pIntertask, 
									   OIT_CM_STATUS * pCurCMStatus)	
{
	BOOL fProcessed = FALSE;

	if (pCurCMStatus->usSensorStatus & FDR_COVER_SENSOR_MASK)
	{
//		fProcessed = fnProcessEvent(EVENT_FEEDER_COVER_OPEN, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_FEEDER_COVER_OPEN, NULL);
	}
	else
	{
//		fProcessed = fnProcessEvent(EVENT_FEEDER_COVER_CLOSED, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_FEEDER_COVER_CLOSED, NULL);
	}

	return fProcessed;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardTapeError
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardTapeError(INTERTASK 	 *	pIntertask, 
							   OIT_CM_STATUS * 	pCurCMStatus)	
{
	return (fnProcessEvent(EVENT_TAPE_ERROR, pIntertask)); 
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardFeederAddedMailMsg
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardFeederAddedMailMsg(INTERTASK 	 *	pIntertask, 
								   		OIT_CM_STATUS * 	pCurCMStatus)	
{
	fnFeederPaperAdded(TRUE);

	return (TRUE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardFeederNoMailError
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardFeederNoMailError(INTERTASK 	 *	pIntertask, 
								   		 OIT_CM_STATUS * 	pCurCMStatus)	
{
	BOOL fProcessed = FALSE;

	if (pCurCMStatus->ucPaperStatus == ERR_FDR_NO_MAIL) 
	{
//		 fProcessed = fnProcessEvent(EVENT_FEEDER_NO_MAIL, pIntertask);
		 SendBaseEventToTablet(BASE_EVENT_FEEDER_NO_MAIL, NULL);
	}
	return fProcessed;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardFeederError
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	1/06/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnForwardFeederError(INTERTASK 	 	*	pIntertask, 
								 OIT_CM_STATUS 	* 	pCurCMStatus)	
{
	BOOL fProcessed = FALSE;

	if (pCurCMStatus->ucPaperStatus == ERR_FDR_JAM) 
	{	// For feeder errors which can be recovered
//		fProcessed = fnProcessEvent(EVENT_FEEDER_ERROR, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_FEEDER_ERROR, NULL);
	}
	else if (pCurCMStatus->ucPaperStatus != ERR_FDR_NO_MAIL)
	{	// For fatal feeder faults
//		fProcessed = fnProcessEvent(EVENT_FEEDER_FAULT, pIntertask);
		SendBaseEventToTablet(BASE_EVENT_FEEDER_FAULT, NULL);
	}

	return fProcessed;
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardMailRunStatusUpdateMsg
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	11/28/2008	Raymond Shen		Initial version
//
// *************************************************************************/
static BOOL fnForwardMailRunStatusUpdateMsg(INTERTASK 	  *	pIntertask, 
									OIT_CM_STATUS * pCurCMStatus)	
{
    BOOL fProcessed = FALSE;
    UINT8   bPaperHandlingStatus;
    fPieceCompleted = FALSE;

    if (stLatestCMStatus.ucMailRunStatus != stPreviousCMStatus.ucMailRunStatus)
    {
        fProcessed = fnProcessEvent((UINT8)EVENT_PRINTER_STATE_UPDATE, pIntertask);

        //bPaperHandlingStatus = pIntertask->IntertaskUnion.bByteData[OIT_PROG_MAILRUN_STATUS];
        //if(PERFORM_MAINTENANCE !=bPaperHandlingStatus)
        if(cmStatus.mtncType == WIPE_PURGE)//TODO Need better way of skipping Base event during maintenance
        {
        	dbgTrace(DBG_LVL_INFO, "fnForwardMailRunStatusUpdateMsg :Maintenance[%d] - Skipping Printer Status Updated Event ", cmStatus.mtncType);
        }
        else
        	SendBaseEventToTablet(BASE_EVENT_PRINTER_STATUS_UPDATED, NULL);
    }

    return (fProcessed);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardFeederWOWSBRError
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	05/20/2009	Raymond Shen		Initial version
//
// *************************************************************************/
static BOOL fnForwardFeederWOWSBRError(INTERTASK 	  *	pIntertask, 
									OIT_CM_STATUS * pCurCMStatus)	
{
    BOOL fProcessed = FALSE;

//    fProcessed = fnProcessEvent((UINT8)EVENT_WOW_SBR_ERROR, pIntertask);
    SendBaseEventToTablet(BASE_EVENT_WOW_SBR_ERROR, NULL);

    return (fProcessed);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnForwardFeederWOWDimRatingErr
//
// DESCRIPTION: 
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		pCurCMStatus- current CM status
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
// 	05/20/2009	Raymond Shen		Initial version
//
// *************************************************************************/
static BOOL fnForwardFeederWOWDimRatingErr(INTERTASK 	  *	pIntertask, 
									OIT_CM_STATUS * pCurCMStatus)	
{
    BOOL fProcessed = FALSE;

//    fProcessed = fnProcessEvent((UINT8)EVENT_WOW_DIM_RATING_ERROR, pIntertask);

    SendBaseEventToTablet(BASE_EVENT_WOW_RATING_ERROR, NULL);
    return (fProcessed);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmDISTModemActivated
//
// DESCRIPTION: 
// 		OIT message handling function for message MDM_CONNECTED from DIST
//		task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 				Victor Li		Initial version
//	12/28/2005	Vincent Yi		Move to OifpInfra
//
// *************************************************************************/
//static BOOL fnmDISTModemActivated(INTERTASK *pIntertask)
//{
//	fnInfraModemActivated (pIntertask);
//	return (SUCCESSFUL);
//}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmDISTModemDisconnected
//
// DESCRIPTION: 
// 		OIT message handling function for message MDM_DISCONNECT from DIST
//		task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 				Victor Li		Initial version
//	12/28/2005	Vincent Yi		Move to OifpInfra
//
// *************************************************************************/
//static BOOL fnmDISTModemDisconnected(INTERTASK *pIntertask)
//{
//	fnInfraModemDisconnected (pIntertask);
//	return (SUCCESSFUL);
//}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmDISTConnectionResult
//
// DESCRIPTION: 
// 		OIT message handling function for message OIT_ATT_CONNECTION_RESULT 
//		from DIST task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 				Derek DeGennaro	Initial version
//				Victor Li		Ported for Janus
//	12/29/2005	Vincent Yi		Move to OifpInfra
//
// *************************************************************************/
static BOOL fnmDISTConnectionResult(INTERTASK *pIntertask)
{
	fnInfraConnectionResult (pIntertask);
	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmDISTDnsLookupError
//
// DESCRIPTION: 
// 		OIT message handling function for message OIT_DNS_TIMEOUT from 
//		DIST task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/3/2006	Vincent Yi		Move to OifpInfra
//
// *************************************************************************/
static BOOL fnmDISTDnsLookupError(INTERTASK *pIntertask)
{
	fnInfraDNSLookupError (pIntertask);
	return (TRUE);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmDISTStartService
//
// DESCRIPTION: 
// 		OIT message handling function for message OIT_START_SERVICE_REQ from 
//		DIST task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/3/2006	Vincent Yi		Move to OifpInfra
//
// *************************************************************************/
static BOOL fnmDISTStartService(INTERTASK *pIntertask)
{
	fnInfraStartService (pIntertask);
	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmDISTEndService
//
// DESCRIPTION: 
// 		OIT message handling function for message OIT_SERVICE_COMPLETE from 
//		DIST task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/3/2006	Vincent Yi		Move to OifpInfra
//
// *************************************************************************/
static BOOL fnmDISTEndService(INTERTASK *pIntertask)
{
	fnInfraEndService (pIntertask);
	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmDISTAllServicesComplete
//
// DESCRIPTION: 
// 		OIT message handling function for message OIT_ALL_SERVICES_COMPLETE from 
//		DIST task.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	1/3/2006	Vincent Yi		Move to OifpInfra
//
// *************************************************************************/
static BOOL fnmDISTAllServicesComplete(INTERTASK *pIntertask)
{
	fnInfraAllServicesComplete (pIntertask);
	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//        fnmDISTDataUploadStatus
//
// DESCRIPTION:
//       OIT message handling function that processes the Data Upload
//       service message from the distributor task.
//
// INPUTS:
//       pIntertask- pointer to the intertask message that is currently
//                   being processed.
//
// RETURNS:
//       Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//       None
//
// MODIFICATION HISTORY:
//       07/22/2001 Zhang Yingbo   Initial Version
// *************************************************************************/
static BOOL fnmDISTDataUploadStatus(INTERTASK *pIntertask)
{
    INTERTASK *pLastMsg;
    short *pMsgData16;

    pMsgData16 = (short *)&(pIntertask->IntertaskUnion.bByteData[0]);

    //bOITCurrentDistrMsgID = pIntertask->bMsgId;//OIT_INFRA_UPLOAD_XFER_STATUS
    usOITCurrentDisplayService = *pMsgData16;//DATA_UPLOAD_SERVICE
    //bOITCurrentServicePriority = pIntertask->IntertaskUnion.bByteData[1];

    //~~ RF
    pLastMsg = fnGetlastIntertaskMsg();
    memcpy(pLastMsg, pIntertask, sizeof(INTERTASK));
    //~~ end RF

    (void)fnProcessEvent((UINT8)EVENT_DISTR_TASK_MSG, pIntertask);

    return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//        fnmRemoteRefillRequest
//
// DESCRIPTION:
//        Handler for remote refill request
//
// INPUTS:
//        pIntertask- pointer to the intertask message that is currently
//                    being processed.
//
// RETURNS:
//        Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION HISTORY:
//        04/03/2001    Mozdzjm    Initial Version
// *************************************************************************/
#if 0
// n_daemon task removed
static BOOL fnmRemoteRefillRequest(INTERTASK *pIntertask)
{
    if(fnProcessEvent((UINT8)EVENT_REMOTE_REFILL_REQ, pIntertask) == TRUE)
    {
          if(OSSendIntertask(N_DAEMON,OIT,(UINT8)REMOTE_REFILL_OK,NO_DATA,NULL,0)==FAILURE)
          {
                throwException("OSSendIntertask() fail");
          }
    }
    else
    {
          if( OSSendIntertask(N_DAEMON,OIT,(UINT8)REMOTE_REFILL_BUSY,NO_DATA,NULL,0)==FAILURE)
          {
                throwException("OSSendIntertask() fail");
          }
    }

    return (SUCCESSFUL);
}
#endif

/* *************************************************************************
// FUNCTION NAME: 
//		fnmDISTCheckTimeSyncStatus
//
// DESCRIPTION: 
// 		OIT message handling function that checks the time synchornization
// 		status again. Trigger the event EVENT_TIME_SYNC_COMPLETED if the 
//		status is OK.
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//
// RETURNS:
//		Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//				Victor Li		Initial version
//	03/28/2006	Vincent Yi		Move to OifpInfra
//
// *************************************************************************/
/*Vincent
static BOOL fnmDISTCheckTimeSyncStatus(INTERTASK *pIntertask)
{
	fnInfraCheckTimeSyncStatus (pIntertask);
	return (SUCCESSFUL);
}
*/

/* *************************************************************************
// FUNCTION NAME: 
//		fnmPLATConnected
//
// DESCRIPTION: OIT message handling function that processes the platform connect
//				message from the weighing task.
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
//  10/28/2012      Bob Li          Modified the function to fix fraca 218810.
//	09/11/2012		John Gao		Modified the function to set pending flag if non-W&M scale connected.
//	05/15/2006		Raymond Shen	Added event process code.
// 					Victor Li		Initial version
//
// *************************************************************************/
static BOOL fnmPLATConnected(INTERTASK *pIntertask)
{

	UINT8 ucPrintMode;
	/* if the scale location code is not set, AND it isn't a Weights & Measures platform,
	trigger an event and set a pending flag so that the main menu check can detecte it. */
	if ((fCMOSScaleLocationCodeInited == FALSE)&& (fnIsWMPlatform() == FALSE))
	{
		fSetScaleLocCodePending = TRUE;
	}
	else if (fnIsWMPlatform() == TRUE)
	{
	    ucPrintMode = fnOITGetPrintMode();
		if (ucPrintMode == PMODE_DIFFERENTIAL_WEIGH)
        {
//        	fnClearPostageAndAllRateInfo();
        	fnOITSetPrintMode(PMODE_MANUAL); // set the print mode to KIP
		}
	}
        
	fnProcessEvent(EVENT_PLATFORM_INSTALL, pIntertask);
	fNewPlatConnected = TRUE;

	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmPLATDisconnected
//
// DESCRIPTION: OIT message handling function that processes the platform disconnect
//				message from the weighing task.
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
//  09/11/2012  Bob Li          Updated the codes to support Diff weight mode detecting.
//  08/23/2007  Raymond Shen    Clear postage and rate info before switching
//                              print mode to fix fraca 127849.
//  03/05/2007    I. Le Goff  Add modifications for mode ajout
//	07/04/2006		Oscar Wang 	Added event process code.
// 	                Victor Li   Initial version
//
// *************************************************************************/
static BOOL fnmPLATDisconnected(INTERTASK *pIntertask)
{
  UINT8 ucPrintMode;

	/* clear the location code pending flag */
	fSetScaleLocCodePending = FALSE;
	if (fnPlatIsAttached() == FALSE)
	{
        ucPrintMode = fnOITGetPrintMode();
        if (ucPrintMode == PMODE_PLATFORM || ucPrintMode == PMODE_DIFFERENTIAL_WEIGH) // Platform mode or Diff weight mode
        {
//            fnClearPostageAndAllRateInfo();
            fnOITSetPrintMode(PMODE_MANUAL); // set the print mode to KIP
        }


		fnProcessEvent(EVENT_PLATFORM_DISCONNECTED, pIntertask);
	}

	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmPLATWeightUpdate
//
// DESCRIPTION: OIT message handling function that processes the weight update
//				message from the weighing task.
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
//      08/08/2007  Adam Liu        Only omit the event EVENT_PLATFORM_UPDATE_RCVD 
//                                  on select carrier and select class screen 
//                                  if in diff weight mode.Fix GMSE00125139, GMSE00125140.
//      06/26/2007  Oscar Wang      Add Differential Weigh specific code to fix 119437.
//                  Victor Li		Initial version
//
// *************************************************************************/
static BOOL fnmPLATWeightUpdate(INTERTASK *pIntertask)
{	
	//UINT8		              pWeight[LENWEIGHT];
	//UINT8		              bStatus;
	//unsigned char			bPlatID;

	//OI_PLATFORM_DATA		rPlatformData;
    UINT16                    usScrnID = fnGetCurrentScreenID();    
	//UINT8		              pLastPieceWeight[LENWEIGHT];
	
	/* Byte 0 - Status
	 * Byte 1 - Units
	 * Byte 2 - Weighting Mode
	 * Byte 3 - Reserved
	 * Bytes 4-7 - Piece Weight
	 * Bytes 8-11 - Platform Weight
	 */

	//bPlatID = 0;

    // Only omit the event EVENT_PLATFORM_UPDATE_RCVD on select carrier and
    // select class screen if in diff weight mode.
    // Fix fraca  GMSE00125139, GMSE00125140. - Adam
	if((fnOITGetPrintMode() == PMODE_DIFFERENTIAL_WEIGH)
	    && ((usScrnID == GetScreen(RATE_SELECT_CARRIER))
	        || (usScrnID == GetScreen(RATE_SELECTCLASS_SUBCLASS))))
	{
	    //save current piece weight since we don't want to update it.
//	    memcpy(pLastPieceWeight, rPlatformCtrl.pPlatformData[0].pPieceWeight, sizeof(pLastPieceWeight));
//        memcpy ( & rPlatformCtrl.pPlatformData[0], &pIntertask->IntertaskUnion.PointerData, sizeof( rPlatformCtrl.pPlatformData[0]));
//        rPlatformCtrl.bActivePlatformID = 0;
        //restore last piece weight since piece weight shouldn't be changed during DWM.
//	    memcpy(rPlatformCtrl.pPlatformData[0].pPieceWeight, pLastPieceWeight, sizeof(pLastPieceWeight));
        return (SUCCESSFUL);
	}
	
	//memcpy(&rPlatformData, &pIntertask->IntertaskUnion.PointerData, sizeof(rPlatformData));
//	memcpy ( & rPlatformCtrl.pPlatformData[0], &pIntertask->IntertaskUnion.PointerData, sizeof( rPlatformCtrl.pPlatformData[0]));
//	rPlatformCtrl.bActivePlatformID = 0;
	
	//if (rPlatformCtrl.pPlatformData[bPlatID].bConfig == Mega_Disp)
	/*{
		pWeight[0] = rPlatformData.pPieceWeight[0];
		pWeight[1] = rPlatformData.pPieceWeight[1];
		pWeight[2] = rPlatformData.pPieceWeight[2];
		pWeight[3] = rPlatformData.pPieceWeight[3];
		bStatus = rPlatformData.bCurrentStatus;
	}*/
	//else
	/* must be differential weighing mode.  that has a different format for weight reports. */
	/*{
		pWeight[0] = uDataIn.pDiffWeightUpdateRcvd->WGT_data[0];
		pWeight[1] = uDataIn.pDiffWeightUpdateRcvd->WGT_data[1];
		pWeight[2] = uDataIn.pDiffWeightUpdateRcvd->WGT_data[2];
		pWeight[3] = uDataIn.pDiffWeightUpdateRcvd->WGT_data[3];
		bStatus = uDataIn.pDiffWeightUpdateRcvd->Status;
	}*/

	/* temp to patch formatting problem with metric weights received from scale */
	/* - not needed on newer platforms - */
/*	if (fnRateGetWeightUnit() != AVOIRDUPOIS)
	{
		unsigned char	pTempWeight[4];

		pTempWeight[0] = pWeight[0] << 4;
		pTempWeight[1] = (pWeight[1] << 4) | (pWeight[0] >> 4);
		pTempWeight[2] = (pWeight[2] << 4) | (pWeight[1] >> 4);
		pTempWeight[3] = (pWeight[3] << 4) | (pWeight[2] >> 4);
		memcpy(pWeight, pTempWeight, 4);
	}
*/

	//fnStorePlatformData(bStatus, pWeight);

	if (!fnProcessEvent(EVENT_PLATFORM_UPDATE_RCVD, pIntertask))
	{
		// When the event is ignored to be processed, we set a flag
		// so we can rerate with the new weight somewhere later.
//		fnGetPlatformWeight(pWeight, &bStatus, TRUE);
//		fForceReRate = fnRerateNeeded(pWeight, bStatus);
	}
	else
	{
		fForceReRate = FALSE; // clear the flag if the event is not igonred
	}
   	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnmPLATCalibRefPrompt
//
// DESCRIPTION: OIT message handling function that processes the platform calibration 
//				reference weight prompt message from the platform task.
// 		
//
// INPUTS:
//		pIntertask- intertask message that is being processed
//		
//
// OUTPUTS:
//		None.
//
// RETURNS:
//		Event processed or not
//
// WARNINGS/NOTES:
//		
//
// MODIFICATION HISTORY:
//	09/11/2006		Raymond Shen        Initial version
//
// *************************************************************************/
static BOOL fnmPLATCalibRefPrompt(INTERTASK *pIntertask)
{	

    bPlatCalibWeightStyle = pIntertask->IntertaskUnion.bByteData[0];
    pPlatCalibWeight[0] = pIntertask->IntertaskUnion.bByteData[4];
    pPlatCalibWeight[1] = pIntertask->IntertaskUnion.bByteData[3];
    pPlatCalibWeight[2] = pIntertask->IntertaskUnion.bByteData[2];
    pPlatCalibWeight[3] = pIntertask->IntertaskUnion.bByteData[1];
    //memcpy(&lwPlatCalibWeight, &(pIntertask->IntertaskUnion.bByteData[1]), sizeof(lwPlatCalibWeight));

	fnProcessEvent(EVENT_PLAT_REF_PROMPT, pIntertask);

   	return (SUCCESSFUL);
}

/* **********************************************************************
// FUNCTION NAME: fnmMeterControlPC
// DESCRIPTION: OIT message handling function that processes the EDM
//              Meter Control PC message.
//
// AUTHOR:  Joey Cui
//
// INPUTS:  pIntertask- intertask message that is being processed
//
// OUTPUTS: Always returns SUCCESSFUL 
// *************************************************************************/
static BOOL fnmMeterControlPC(INTERTASK *pIntertask)
{   
    fnChangeScreen(GetScreen(SCREEN_MM_INDICIA_READY),TRUE,TRUE);
	return (SUCCESSFUL);
}
/* *************************************************************************
// FUNCTION NAME:
//        fnmDISTRefillServiceStatus
//
// DESCRIPTION:
//       OIT message handling function that processes the refill service
//       message for reissue from the distributor task.
//
// INPUTS:
//       pIntertask- pointer to the intertask message that is currently
//                   being processed.
//
// RETURNS:
//       Always returns SUCCESSFUL
//
// WARNINGS/NOTES:
//       None
//
// MODIFICATION HISTORY:
//       06/25/2010 Jingwei,Li  Initial Version
// *************************************************************************/
static BOOL fnmDISTRefillServiceStatus(INTERTASK *pIntertask)
{
    //INTERTASK *pLastMsg;
    //short *pMsgData16;

    //pMsgData16 = (short *)&(pIntertask->IntertaskUnion.bByteData[0]);

    //usOITCurrentDisplayService = *pMsgData16;//REFILL_SERVICE

    //pLastMsg = fnGetlastIntertaskMsg();
    //memcpy(pLastMsg, pIntertask, sizeof(INTERTASK));
    //fnOITSetRequestedService(REFILL_SERVICE);
    (void)fnProcessEvent((UINT8)EVENT_DISTR_TASK_MSG, pIntertask);

    return (SUCCESSFUL);
}

/* **********************************************************************
// FUNCTION NAME: fnmOITEnterSleep
// DESCRIPTION: OIT message handling function that handles a sleep
//				event.
//
// AUTHOR: Sandra Peterson
//
// INPUTS:	pIntertask- intertask message that is being processed
//
// OUTPUTS:	Always returns SUCCESSFUL 
// *************************************************************************/
static BOOL fnmOITEnterSleep(INTERTASK *pIntertask)
{
	//unsigned long	ulWeight = 0;
	//unsigned char	bPlatformStatus;


//	fnGetPlatformWeight((unsigned char *)&ulWeight, &bPlatformStatus, FALSE);

	// if a Weights&Measures platform is attached and there is some
	// weight on it, don't go to sleep
	// so, if there is no platform attached or there isn't a W&M platform attached
	// or there is no weight on the platform, go to sleep
//Ram added to disable sleep
//	if ((fnPlatIsAttached() == FALSE) || (fnIsWMPlatform() == FALSE) || !ulWeight)
//		(void)fnProcessEvent(EVENT_SLEEP, pIntertask);
//	bGoToReady = FALSE;

	return (SUCCESSFUL);
}

/* **********************************************************************
// FUNCTION NAME: fnmOITSetToNormalPreset
// DESCRIPTION: OIT message handling function that handles a timeout to
//				normal preset event.
//
// AUTHOR: Sandra Peterson
//
// INPUTS:	pIntertask- intertask message that is being processed
//
// OUTPUTS:	Always returns SUCCESSFUL 
// *************************************************************************/
static BOOL fnmOITSetToNormalPreset(INTERTASK *pIntertask)
{
	//unsigned long	ulWeight = 0;
	//unsigned char	bPlatformStatus;


//	fnGetPlatformWeight((unsigned char *)&ulWeight, &bPlatformStatus, FALSE);

	// if a Weights&Measures platform is attached and there is some
	// weight on it, don't set the system to the Normal Preset
	// so, if there is no platform attached or there isn't a W&M platform attached
	// or there is no weight on the platform, set the system to the Normal Preset
//	if ((fnPlatIsAttached() == FALSE) || (fnIsWMPlatform() == FALSE) || !ulWeight)
//		(void)fnProcessEvent(EVENT_NORMAL_PRESET_TIMEOUT, pIntertask);
//	bGoToReady = FALSE;
	return (SUCCESSFUL);
}


/*end oimsgc.c*/

