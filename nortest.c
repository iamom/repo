//#include <string.h> /* For memcpy */
#include <stdio.h>

#ifdef MEMORYTEST

#include "nor.h"
#include "events.h"

bool validate_test_data(memory_test_data_t *memory_test)
{
	if( (memory_test->memory_test_req.address  < 0 ) ||
		((memory_test->memory_test_req.address  + memory_test->memory_test_req.size) >= ( NOR_FLASH_SIZE )))
	{
			memory_test->result =  E_CB_OUT_OF_BOUNDS;
			return 1;
	}

	return 0;
}

void flash_erase_test(memory_test_data_t *memory_test)
{
	if( (memory_test->memory_test_req.address  < 0 ) ||
		((memory_test->memory_test_req.address  + memory_test->memory_test_req.size) >= ( NOR_FLASH_SIZE )))
			memory_test->result =  E_CB_OUT_OF_BOUNDS;
	else
	{
		memory_test->result = EraseFlash((void*)NOR_ADDR_TO_SECTOR_START(NOR_BASE_ADDR+memory_test->memory_test_req.address), TRUE);
	}

}
void flash_erase_chip_test(memory_test_data_t *memory_test)
{
	if( (memory_test->memory_test_req.address  < 0 ) ||
			((memory_test->memory_test_req.address  + memory_test->memory_test_req.size) >= ( NOR_FLASH_SIZE )))
				memory_test->result =  E_CB_OUT_OF_BOUNDS;
	else
	{

		memory_test->result = NorChipEraseOp((UINT8*)NOR_BASE_ADDR);
	}

}

void flash_read_test(memory_test_data_t *memory_test)
{

	if( (memory_test->memory_test_req.address  < 0 ) ||
		((memory_test->memory_test_req.address  + memory_test->memory_test_req.size) >= (NOR_FLASH_SIZE )))
					memory_test->result =  E_CB_OUT_OF_BOUNDS;
	else
	{
		memory_test->result = ReadFlash((void*)(NOR_BASE_ADDR + memory_test->memory_test_req.address), (void*) &memory_test->info, 4);

		if(memory_test->memory_test_req.size > 4)
		{
			if(memory_test->memory_test_req.size > 512)
				memory_test->result = ReadFlash((void*)(NOR_BASE_ADDR + memory_test->memory_test_req.address), (void*)memory_test->buffer, 512);
			else
				memory_test->result = ReadFlash((void*)(NOR_BASE_ADDR + memory_test->memory_test_req.address), (void*)memory_test->buffer, memory_test->memory_test_req.size);
		}
	}

}

void flash_write_buffer_test(memory_test_data_t *memory_test)
{

	if( (memory_test->memory_test_req.address  < 0 ) ||
		((memory_test->memory_test_req.address  + memory_test->memory_test_req.size) >= (NOR_FLASH_SIZE )))
	{
					memory_test->result =  E_CB_OUT_OF_BOUNDS;
	}
	else
	{
		int i;
		UINT16* pTemp16 = (UINT16*)(NOR_BASE_ADDR + memory_test->memory_test_req.address);
		UINT8 buffer[32];
		UINT8 buffer2[32];

		for(i = 0; i < sizeof(buffer); i++)
			buffer[i] = i;

		//memset(&temp16, memory_test->memory_test_req.value8, sizeof(temp16));
		//ERR_CODE WriteBufferToFlash(UINT16* pDst, UINT16* data_buffer, UINT16 word_count)
		memory_test->result = WriteBufferToFlash(pTemp16, (UINT16*)buffer, sizeof(buffer)/2);
		if(memory_test->result == E_CB_NO_ERROR)
		{
			memory_test->result = ReadFlash(pTemp16, (UINT16*)buffer2, sizeof(buffer2));
			if(memory_test->result  == E_CB_NO_ERROR)
			{
				if(memcmp(buffer, buffer2, sizeof(buffer)) != 0)
					memory_test->result = E_CB_VERIFICATION_FAILURE;
			}
		}

	}
}


void flash_write_test(memory_test_data_t *memory_test)
{

	if( (memory_test->memory_test_req.address  < 0 ) ||
		((memory_test->memory_test_req.address  + memory_test->memory_test_req.size) >= (NOR_FLASH_SIZE )))
	{
					memory_test->result =  E_CB_OUT_OF_BOUNDS;
	}
	else
	{
		int i;
		UINT16* pTemp16 = (UINT16*)(NOR_BASE_ADDR + memory_test->memory_test_req.address);
		UINT16 temp16;

		memset(&temp16, memory_test->memory_test_req.value8, sizeof(temp16));

		for(i = 0; i <memory_test->memory_test_req.size/2; i++)
		{
			memory_test->result = WriteFlash(pTemp16++, temp16);
			if(memory_test->result != 0)
				break;
		}


	}
}


void flash_write_16_test(memory_test_data_t *memory_test)
{
	if( (memory_test->memory_test_req.address  < 0 ) ||
		((memory_test->memory_test_req.address  + memory_test->memory_test_req.size) >= (NOR_FLASH_SIZE )))
		{
			memory_test->result =  E_CB_OUT_OF_BOUNDS;
		}
		else
		{
			int i;
			UINT16* pTemp16 = (UINT16*)(NOR_BASE_ADDR + memory_test->memory_test_req.address);
			UINT16 temp16 = memory_test->memory_test_req.value16;

			for(i = 0; i <memory_test->memory_test_req.size/2; i++)
			{
				memory_test->result = WriteFlash(pTemp16++, temp16);
				if(memory_test->result != 0)
					break;
			}
		}

}

void flash_write_32_test(memory_test_data_t *memory_test)
{
	if( (memory_test->memory_test_req.address  < 0 ) ||
		((memory_test->memory_test_req.address  + memory_test->memory_test_req.size) >= (NOR_FLASH_SIZE )))
		{
			memory_test->result =  E_CB_OUT_OF_BOUNDS;
		}
		else
		{
			int i;
			UINT16* pTemp16 = (UINT16*)(NOR_BASE_ADDR + memory_test->memory_test_req.address);
			UINT16 temp16 = memory_test->memory_test_req.value32 >>16;

			for(i = 0; i <memory_test->memory_test_req.size/2; i++)
			{
				memory_test->result = WriteFlash(pTemp16++, temp16);
				if(memory_test->result != 0)
					break;
			}
		}

}


//uint32_t nor_init(void) {return 0;}
//uint32_t nor_getCFIParams(params){ return 0;}


//void log_test(uint32_t result, void* buf, uint16_t size)
//{
//	sys_log_add_sprintf(LOG_SRC_MEMORY_TEST, "Flash test result=%u", result);
//	sys_log_add(LOG_SRC_MEMORY_TEST, lT_MEMORY_TEST, buf, size);
//}
void flash_crc_test(memory_test_data_t *memory_test)
{
	//uint32_t result;

	int cnt;
	int sect;

	//uint8_t readarr[256];
	//uint8_t sec_buffer[NOR_FLASH_SECTOR_SIZE];
	uint16_t *addr;
	unsigned int value;
	uint16_t crc;

	uint8_t barr[16];
	volatile uint16_t *wp = (uint16_t *)(NOR_BASE_ADDR);


	/* Test writeflash() routine */
	printf("Test writeflash() bounds check\n");
	/* Setup data for write */
	barr[0] = 0x55;
	barr[1] = 0xAA;

	for(sect = 0; sect < NOR_FLASH_NUM_OF_SECTORS; sect++)
	{
		//Read Sector nor_readflash
		addr = (uint16_t*)NOR_SECTOR_NUM_TO_ADDR(sect);
		for(cnt = 0 ; cnt < (NOR_FLASH_SECTOR_SIZE/2); cnt++)
		{
			memory_test->result = ReadFlash((void*)(addr + cnt), (UINT16*)barr, sizeof(barr));
			if (E_CB_NO_ERROR != memory_test->result)
			{
				printf("FAIL: Failed to read sector.\n");
				memory_test->info = (uint32_t)addr;
				return;
			}
		}


		//Erase sector
		/* Erase the sector we will be writing. */
		memory_test->result  = EraseFlash(addr, TRUE);
		if (E_CB_NO_ERROR != memory_test->result )
		{
			printf("FAIL: Failed to erase sector.\n");
			memory_test->info = (uint32_t)addr;
			return;
		}
		else
		{
			printf("Successful erase sector.\n");
		}

		//Verify Erase
		if(IsNorFlashSectorErased(addr) != TRUE)
		{
			printf("FAIL: Erase failed, read: 0x%04X, sector: %0X, offset %04x, expected: 0xFFFF.\n", wp[0], sect, cnt);
			memory_test->result = E_CB_ERASE_FAILURE;
			memory_test->info = (uint32_t)addr;
			return;
		}

		//Write CRC of address

		for(cnt = 0 ; cnt < (NOR_FLASH_SECTOR_SIZE/2); cnt++)
		{
			value =  (uint32_t)(addr + cnt);
			crc = _crc16(0, (char *)&value, sizeof(uint32_t));
			memory_test->result = WriteFlash(addr + cnt, crc);
			if(E_CB_NO_ERROR != memory_test->result)
			{
				printf("FAIL: Failed to write %0X, sector %0X.\n", value, sect);
				memory_test->info = (uint32_t)addr + cnt;
				return;
			}

			memory_test->result = ReadFlash(addr + cnt, (void*)&value, sizeof(value));
			if(E_CB_NO_ERROR != memory_test->result)
			{
				printf("FAIL: Failed to Read %0X, sector %0X.\n", value, sect);
				memory_test->info = (uint32_t)addr + cnt;
				return;
			}
			else
			{
				if ( value != crc)
				{
					printf("FAIL: Verify of write, read: 0x%04X, sector: %d, offset %d, expected: 0xAAAA.\n", wp[0], sect, cnt);
					memory_test->result = E_CB_VERIFICATION_FAILURE;
					memory_test->info = (uint32_t)addr + cnt;
					return;
				}
			}
		}

	}

    memory_test->result =  E_CB_NO_ERROR;
}

#define FLASH_WRITE_OFFSET			(TEST_PATTERN_OFFSET + 2)
#define FLASH_TEST_PATTERN			"FLASHTest"

void flash_data_retention(memory_test_data_t *memory_test)
{
	uint32_t	addr = NOR_BASE_ADDR + memory_test->memory_test_req.address;
	uint16_t	crc;
	uint16_t	crc2;
	uint8_t 	barr[16];
	uint32_t	value;
	uint32_t	sect, cnt, sectorsToTest;
	UINT32		null_value = 0xFFFFFFFF;

	if( (memory_test->memory_test_req.size >= NOR_FLASH_SECTOR_SIZE) &&
		(memory_test->memory_test_req.size < NOR_FLASH_SIZE) )
	{
		sectorsToTest = memory_test->memory_test_req.size/NOR_FLASH_SECTOR_SIZE;
	}
	else
		sectorsToTest = 1;

	/** If written display */
	/*Check first and second sectors to check for erased*/
//	if ( (IsNorFlashSectorErased((void*)NOR_BASE_ADDR) != TRUE) &&
//		 (IsNorFlashSectorErased((void*)(NOR_BASE_ADDR + NOR_FLASH_SECTOR_SIZE)) != TRUE))
	memory_test->result = ReadFlash((void*)(addr), (void*)&barr, sizeof(barr));
	if(E_CB_NO_ERROR != memory_test->result)
	{
		printf("FAIL: Failed to Read sector 0.\n");
		memory_test->info = (uint32_t)addr;
		return;
	}

	if(memcmp(barr, &null_value, sizeof(null_value)) != 0)
	{
		//if flash is not erased then try to verify
		addr = NOR_ADDR_TO_SECTOR_START(addr);
		for(sect = 0; sect < sectorsToTest; sect++)
		{
			//Read Sector nor_readflash
			addr += (sect*NOR_FLASH_SECTOR_SIZE);
			for(cnt = 0 ; cnt < (NOR_FLASH_SECTOR_SIZE/2); cnt++)
			{
				value =  (uint32_t)((uint16_t*)addr + cnt);
				crc = _crc16(0, (char *)&value, sizeof(uint32_t));

				memory_test->result = ReadFlash(((UINT16*)addr + cnt), (UINT16*)barr, sizeof(barr));
				if (E_CB_NO_ERROR != memory_test->result)
				{
					printf("FAIL: Failed to read 0x%lx.\n", (uint32_t)((UINT16*)addr + cnt));
					memory_test->info = (uint32_t)((UINT16*)addr + cnt);
					return;
				}

				if ( *(UINT16*)barr != crc)
				{
					printf("FAIL: Data retention test failed. Address: 0x%08lx, sector: 0x%08lx, crc:0x%4x, expected:0x%08x.\n", value, sect, crc, *(UINT16*)barr);
					memory_test->result = E_CB_VERIFICATION_FAILURE;
					memory_test->info = (uint32_t)((UINT16*)addr + cnt);
					return;
				}
			}
		}

		printf("FLASH Data retention Successful.\n");

	}
	else
	{
//		printf("***************************\n");
//		printf("Pattern NOT found. Setting.\n");
//		printf("***************************\n");
		//Erase chip
		//memory_test->result = NorChipEraseOp((UINT8*)NOR_BASE_ADDR);
		if(memory_test->result == 0)
		{
			//Write CRC of address
			addr = NOR_ADDR_TO_SECTOR_START(addr);
			for(sect = 0 ; sect < sectorsToTest; sect++)
			{
				addr += (sect*NOR_FLASH_SECTOR_SIZE);

				memory_test->result = EraseFlash((void*)addr, TRUE);
				if(memory_test->result == E_CB_NO_ERROR)
				{
					for(cnt = 0 ; cnt < (NOR_FLASH_SECTOR_SIZE/2); cnt++)
					{
						value =  (uint32_t)((UINT16*)addr + cnt);
						crc = _crc16(0, (char *)&value, sizeof(uint32_t));
						memory_test->result = WriteFlash(((UINT16*)addr + cnt), crc);
						if(E_CB_NO_ERROR != memory_test->result)
						{
							printf("FAIL: Failed to write 0x%0lx, sector 0x%0lx.\n", value, sect);
							memory_test->info = (uint32_t)((UINT16*)addr + cnt);
							return;
						}

						//verify that write is good
						memory_test->result = ReadFlash(((UINT16*)addr + cnt), (void*)&crc2, sizeof(crc2));
						if(E_CB_NO_ERROR != memory_test->result)
						{
							printf("FAIL: Failed to Read 0x%0us sector 0x%0lu.\n", crc2, sect);
							memory_test->info = (uint32_t)((UINT16*)addr + cnt);
							return;
						}
						else
						{
							if ( crc2 != crc)
							{
								printf("FAIL: flash_data_retention-Verify  write, read: 0x%04lx, sector: 0x%0lx, offset 0x%0lx,\n", (uint32_t)(addr + cnt), sect, cnt );
								printf("Expected: 0x%4ux, Read:0x%4ux.\n", crc, crc2);
								memory_test->result = E_CB_VERIFICATION_FAILURE;
								memory_test->info = (uint32_t)((UINT16*)addr + cnt);
								return;
							}
						}
					}//end for(cnt...
				}//end if(status..
			}//end for(sect...
		}
	}

	//printf("+++++++++++++++++++++++\n");
	//printf("FLASH Test Passed.\n");
	//printf("+++++++++++++++++++++++\n");

	/** We're done */
	return ;
}

void flash_chip_erase_test(memory_test_data_t *memory_test)
{
	/** Return value */
	UINT32	addr, sect;

	/** If written display */
	/*Check first and second sectors to check for erased*/
	//Erase chip
	memory_test->result = NorChipEraseOp((UINT8*)NOR_BASE_ADDR);
	if(memory_test->result == 0)
	{
		//check all sectors
		for(sect = 0; sect < NOR_FLASH_NUM_OF_SECTORS; sect++)
		{
			addr = (UINT32)NOR_SECTOR_NUM_TO_ADDR(sect);
			if ( IsNorFlashSectorErased((void*)addr) != TRUE)
			{
				memory_test->result = DEV_ERASE_ERROR;
				memory_test->info = sect;
				break;
			}
		}
	}
	//printf("+++++++++++++++++++++++\n");
	//printf("FLASH Test Passed.\n");
	//printf("+++++++++++++++++++++++\n");

	/** We're done */
	return;
}

//void flash_write_buffer_test(memory_test_data_t *memory_test)
//{
//	/** Return value */
//	uint8_t		flash_patern_string[10];
//	uint32_t	addr;
//	uint32_t		sect;
//
//	for(sect = 4; sect < NOR_FLASH_NUM_OF_SECTORS; sect++)
//	{
//		addr = (UINT32)NOR_SECTOR_NUM_TO_ADDR(sect);
////		//check for erased sector if not erase
////		if(IsNorFlashSectorErased((void*)addr)!= TRUE)
////		{
////			memory_test->result = EraseFlash((void*)addr, TRUE);
////			if(memory_test->result != 0)
////			{
////				memory_test->info = addr;
////				return;
////			}
////		}
//
//		memcpy(flash_patern_string, FLASH_TEST_PATTERN, sizeof(flash_patern_string));
//
//		memory_test->result = WriteBufferToFlash((void*)addr, sizeof(flash_patern_string)/2, (void*)flash_patern_string);
//		if(E_CB_NO_ERROR != memory_test->result)
//		{
//			printf("FAIL: flash_write_test:WriteBufferToFlash Failed to write sector 0x%0lx.\n", sect);
//			memory_test->info = (uint32_t)addr;
//			return;
//		}
//
//		memory_test->result = ReadFlash((void*)addr, (void*)flash_patern_string, sizeof(flash_patern_string));
//		if(E_CB_NO_ERROR != memory_test->result)
//		{
//			printf("FAIL: flash_write_test:ReadFlash sector 0x%0lx.\n", sect);
//			memory_test->info = (uint32_t)addr;
//			return;
//		}
//	}
//
//
//	//printf("+++++++++++++++++++++++\n");
//	//printf("FLASH Test Passed.\n");
//	//printf("+++++++++++++++++++++++\n");
//
//	/** We're done */
//	return ;
//}


void flash_error_checks(memory_test_data_t *memory_test)
{
	uint16_t	value16 = 0;
	uint8_t barr[4];


	/* NOTE: The direct call to nor_getCFIParams() is not required for normal operation. */
	/* I am using it to get the NOR parameters to print to the screen. */
//	//TODO
//	if (E_CB_NO_ERROR != nor_getCFIParams(params))
//	{
//		printf("Could not retrieve NOR parameters\n");
//		memory_test->result = E_CB_COMMON_ERR_UNKNOWN;
//	}


	//nor_printParams(params);

	/* Test writeflash() routine */
	printf("Test writeflash() bounds check\n");
	/* Setup data for write */
	barr[0] = 0x55;
	barr[1] = 0xAA;
	/* Erase the sector we will be writing. */
	if (E_CB_NO_ERROR != EraseFlash((uint16_t *)(NOR_BASE_ADDR), TRUE))
	{
		printf("FAIL: Failed to erase sector.\n");
		memory_test->result = E_CB_COMMON_ERR_UNKNOWN;
	}

	while (NorStatusGet((void *)(NOR_BASE_ADDR)) != DEV_NOT_BUSY );

	if (E_CB_NO_ERROR != WriteFlash((void*)(NOR_BASE_ADDR), *(UINT16*)barr))
    {
    	printf("FAIL: Failed to write.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }
    if (E_CB_NO_ERROR != ReadFlash((void *)(NOR_BASE_ADDR), (void*)&value16, sizeof(value16)))
    {
    	printf("FAIL: ReadFlash failed: 0x%08X.\n", NOR_BASE_ADDR);
    }
    else if(value16 != 0xaa55)
    {
        printf("FAIL: Verify of write, read: 0x%04X, expected: 0xAA55.\n", value16);
    }

	if (DEV_INVALID_ADDRESS != WriteFlash((void *)(NOR_BASE_ADDR-1), *(UINT16*)barr))
    {
    	printf("FAIL: Bounds check low.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }
	if (DEV_INVALID_ADDRESS != WriteFlash((void*)(NOR_BASE_ADDR-2), *(UINT16*)barr))
    {
    	printf("FAIL: Bounds check low.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }
	if (DEV_INVALID_WORD_BOUNDARY != WriteFlash((void *)(NOR_BASE_ADDR+1), *(UINT16*)barr))
    {
    	printf("FAIL: Alignment test.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }
	if (DEV_INVALID_ADDRESS != WriteFlash((void *)(NOR_BASE_ADDR+NOR_FLASH_SIZE), *(UINT16*)barr))
    {
    	printf("FAIL: Bounds check high.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }
	if (E_CB_NO_ERROR != EraseFlash((uint16_t *)(NOR_BASE_ADDR), TRUE))
	{
    	printf("FAIL: Failed to erase sector.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }

	while (NorStatusGet((uint16_t *)(NOR_BASE_ADDR)) != DEV_NOT_BUSY );

	/* Test programword() */
    printf("Testing programword() routine.\n");
    if (DEV_INVALID_ADDRESS != WriteFlash((uint16_t *)(NOR_BASE_ADDR+NOR_FLASH_SIZE),0x55AA))
    {
        printf("FAIL: Bounds check high.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }
    if (DEV_INVALID_ADDRESS != WriteFlash((uint16_t *)(NOR_BASE_ADDR-1),0x55AA))
    {
        printf("FAIL: Bounds check low.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }
    if (DEV_INVALID_ADDRESS != WriteFlash((uint16_t *)(NOR_BASE_ADDR-2),0x55AA))
    {
        printf("FAIL: Bounds check low.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }

    /* Test handling of NOR flash lockup when same word programmed twice. */
    printf("Testing handling of flash lockup detection in isflashready().\n");
    if (E_CB_NO_ERROR != WriteFlash((uint16_t *)(NOR_BASE_ADDR),0x55AA))
    {
        printf("FAIL: Simple program word.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }

    while (NorStatusGet((uint16_t *)(NOR_BASE_ADDR)) != DEV_NOT_BUSY );
    /* Try to make flash lockup, attempt to write 0s to 1s. */
    if (E_CB_NO_ERROR != WriteFlash((uint16_t *)(NOR_BASE_ADDR),0xF0F0))
    {
        printf("FAIL: Simple program word.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }
    /* If flash locks up, isflashready will loop forever. */

    while (NorStatusGet((uint16_t *)(NOR_BASE_ADDR)) != DEV_NOT_BUSY );

	if (E_CB_NO_ERROR != EraseFlash((uint16_t *)(NOR_BASE_ADDR), TRUE))
	{
    	printf("FAIL: Failed to erase sector.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }

	while (NorStatusGet((uint16_t *)(NOR_BASE_ADDR)) != DEV_NOT_BUSY );

    /* Test erasesector() */
	printf("Testing erasesector()\n");
	if (DEV_INVALID_ADDRESS != EraseFlash((uint16_t *)(NOR_BASE_ADDR-2), TRUE))
	{
    	printf("FAIL: Bounds check low.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }
	if (DEV_INVALID_ADDRESS != EraseFlash((uint16_t *)(NOR_BASE_ADDR+NOR_FLASH_SIZE), TRUE))
	{
    	printf("FAIL: Bounds check high.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }
	if (E_CB_NO_ERROR != EraseFlash((uint16_t *)(NOR_BASE_ADDR), TRUE))
	{
    	printf("FAIL: Failed to erase sector.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }

	while (NorStatusGet((uint16_t *)(NOR_BASE_ADDR)) != DEV_NOT_BUSY );

	if (E_CB_NO_ERROR != WriteFlash((void *)(NOR_BASE_ADDR), *(UINT16*)barr))
    {
    	printf("FAIL: Failed to write.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }

	if (E_CB_NO_ERROR != ReadFlash((void *)(NOR_BASE_ADDR), (void*)&value16, sizeof(value16)))
	{
		printf("FAIL: ReadFlash failed: 0x%08X.\n", NOR_BASE_ADDR);
	}

    if (value16 != 0xAA55)
    {
        printf("FAIL: Verify of write.\n");
    }
	if (E_CB_NO_ERROR != EraseFlash((uint16_t *)(NOR_BASE_ADDR), TRUE))
	{
    	printf("FAIL: Failed to erase sector.\n");
        memory_test->result =  E_CB_COMMON_ERR_UNKNOWN;
    }

	while (NorStatusGet((uint16_t *)(NOR_BASE_ADDR)) != DEV_NOT_BUSY );

	if (E_CB_NO_ERROR != ReadFlash((void *)(NOR_BASE_ADDR), (void*) &value16, sizeof(value16)))
	{
		printf("FAIL: ReadFlash failed: 0x%08X.\n", NOR_BASE_ADDR);
	}

    if (value16 != 0xFFFF)
    {
        printf("FAIL: Verify of erase.\n");
    }

	printf("Begin read while erasing test: Erase Bank A while continuously reading Bank B.\n");

    memory_test->result =  E_CB_NO_ERROR;
}


bool PerformMemoryTests(memory_test_data_t *memory_test);

void nor_flash_tests(void)
{
	memory_test_data_t stest;

	stest.memory_test_req.address = 0x20000;
	stest.memory_test_req.count = 1;
	stest.memory_test_req.memory_type = FLASH;
	stest.memory_test_req.test_type = DATA_RETENTION; //read
	stest.memory_test_req.value8 = 0xAA;
	stest.memory_test_req.value16 = 0xBBBB;
	stest.memory_test_req.value32 = 0xCCCCCCCC;
	stest.memory_test_req.size = NOR_FLASH_SECTOR_SIZE;
	stest.info = 0;
	stest.result = 0;


	////////////////////////////////////////////////////
	stest.memory_test_req.test_type = WRITE; //
	if(stest.memory_test_req.test_type == WRITE)
	{
		flash_write_buffer_test(&stest);
	}


	stest.memory_test_req.test_type = DATA_RETENTION; //read
	if(stest.memory_test_req.test_type == DATA_RETENTION)
	{

		//Data retention test
		 //flash_data_retention(&stest);
		PerformMemoryTests(&stest);
		 if(stest.result)
			printf("flash_data_retention failed \r\n");


	}

	flash_error_checks(&stest);

		//////////////////////////////////////////////////
	stest.memory_test_req.test_type = READ;
	if(stest.memory_test_req.test_type == READ)
	{
		stest.memory_test_req.test_type = READ; //read
		//flash_read_test(&stest);
		PerformMemoryTests(&stest);
		if(stest.result)
			printf("flash_read failed \r\n");
		else
			printf("flash_read %8lx\r\n", stest.info);
	}


	//////////////////////////////////////////////////
	stest.memory_test_req.test_type = READ;
	if(stest.memory_test_req.test_type == READ)
	{
		//flash_erase_test(&stest);
		PerformMemoryTests(&stest);
		if(stest.result)
			printf("flash_erase failed \r\n");
	}



	//////////////////////////////////////////////////
	stest.memory_test_req.test_type = ERASE_SECTOR;
	if(stest.memory_test_req.test_type == ERASE_SECTOR)
	{
		//flash_chip_erase_test(&stest);
		PerformMemoryTests(&stest);
		if(stest.result)
			printf("SECTOR ERASE failed Sect: 0x%0lx\r\n", stest.info);
		else
			printf("SECTOR ERASE Success \r\n");
	}

	////////////////////////////////////////////////////
	stest.memory_test_req.test_type = WRITE;
	if(stest.memory_test_req.test_type == WRITE)
	{
		//flash_write_buffer_test(&stest);
		PerformMemoryTests(&stest);
		if(stest.result)
			printf("flash_write_buffer_test failed Sect: 0x%0lx\r\n", stest.info);
		else
			printf("flash_write_buffer_test Success \r\n");
	}

}



#endif //MEMORYTEST
