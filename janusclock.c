/************************************************************************
  PROJECT:        Janus
  MODULE NAME:    JanusClock.c
 
  DESCRIPTION:    
    Janus Real Time clock shadow routines.  The iButton real time clock
    is accessed on powerup and at midnight to obtain a calibration for 
    the real time shadow, which is based on the Nucleus 10ms time tick.
 ----------------------------------------------------------------------
               Copyright (c) 2003 Pitney Bowes Inc.
                    35 Waterview Drive
                   Shelton, Connecticut  06484
 ----------------------------------------------------------------------

   REVISION HISTORY:

 01-May-15 sa002pe on FPHX 02.12 shelton branch
	In order to make sure all the timers get set up properly when the clock is resynced:
	1. Changed fnStartClockAlarmJanus to not resync the clock because it will
		throw off the timing for all the other timers.

 18-Feb-13 sa002pe on FPHX 02.10 shelton branch
 	Changed SetSystemClock to:
	1. Put a message in the syslog to indicate that we're resyncing the
		software clock w/ the PSD clock.
	2. Use the local date in the "Set Time" message instead of the GMT date
		because the GMT date could be different.
	3. Put the "Set Time" message in the syslog.

 2010.07.21 Clarisa Bellamy     FPHX - 2.07
  Merged fix from Janus.  Running out of stack caused problems on Janus that 
  took a long time to trace, so we will try to prevent them by being proactive
  and merging the fix.
  * Changed fnInitAlarmInterrupt to change the requested stack size from *2 to *10 to fix
  * Fraca 188194 and to hopefully avoid problems in the future.

 2010.07.16 Raymond Shen    FPHX - 2.07
   For fixing the problem that alarm event may miss on two conditions:
   1. when the alarm period is 1 year.
   2. when the local system time is faster than the PSD time. 
   - Modified fnStartClockAlarmJanus() to extend the max supported alarm period and
     to resync the system time with PSD when the system time is faster than PSD time.
 
 2009.06.22 Clarisa Bellamy  FPHX - 2.00   Merge from 1.15Dev
  2009.03.13 Clarisa Bellamy  FPHX - 1.15
   For support of Gemini:
   - Modify function fnCheckGMTOffset, to check for ANY type of I-button,
     because now, PSDT_IBUTTON is only the ASTEROID type.
   LINT:
   - Cast functions as pointing to void if return value is ignored.
   - In fnStartClockAlarmJanus, in if() statement, replace & with &&.
   - Remove stdlib.h, add string.h and bobutils.h
   - Add a cast and some function prototypes.

 05/31/2007 Dan Zhang  Added getMsClock() function.

 11-Jan-06 sa002pe on Janus 11.00 shelton branch
   Changed fnInitAlarmInterrupt to double the requested stack size.

* ----------------------------------------------------------------------
* Pre-clearcase history:
*       $Log:   H:/group/SPARK/ARCHIVES/DM150/uic_app/JanusClock.c_v  $
 * 
 *    Rev 1.6   09 May 2005 13:58:02   BR507HA
 * Modified log entry in SetSystemClock() to show local and GMT time.
 * For K700 only:  Added log header entry when clock is set.
 * 
 *    Rev 1.5   14 Nov 2003 14:29:32   defilcj
 * 1. change fnGetPsdType to fnGetCurrentPsdType to match common Mega/Phoenix
 * function names.
 * 
 *    Rev 1.4   07 Nov 2003 14:56:54   BR507HA
 *  
 *    Rev 1.3   19 Sep 2003 16:21:22   defilcj
 * 1. add support for user clock drift setting via set GMTOffset.
 * 
 *    Rev 1.2   28 May 2003 13:19:02   BR507HA
 * Added log entry in SetSystemClock
 * 
 *    Rev 1.1   09 May 2003 17:40:48   BR507HA
 * Bumped default date to 5/1/03
 * 
 *    Rev 1.0   23 Apr 2003 12:10:42   BR507HA
 * Initial Version - created from phoenix cometclock.c
*     
*************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "nucleus.h"
#include "posix.h"
#include "bob.h"
#include "bobutils.h"   // fnGetCurrentPsdType()
#include "clock.h"
#include "datdict.h"
#include "ipsd.h"
#include "pbos.h"
#include "sac.h"
#include "commontypes.h"
#include "Ossetup.h"
#include "hal.h"
#include "utils.h"

//----------------------------------------------------------------------------
//  Prototypes for functions that are not yet prototyped in an include file: 


//*************************** File Scope Variables **************************
unsigned long RTClockCount;     // 10 millisecond counts since latest iButton sync
DATETIME RTClockSync ;          // Time of latest iButton sync;
DATETIME AlarmExpireTime;       // Current Alarm setting
static OS_HISR ClockDevHisr;            /* clock Hisr control block */

BOOL TimeSetByPSD = FALSE;


//**************************  Function Prototypes ***************************
// These are prototyped in cometclock.h.  Not sure why that cannot be included here,
//  but I think it causes a problem.

BOOL SetSystemClock(DATETIME *);
BOOL ReadRTClock(DATETIME *);
BOOL setOSClock(DATETIME *);

//----------------------------------------------------------------------------
/*
 * The following structure is used as a reference for calculating the day of the week when
 * setting the clock.  January 1, 2001 was chosen as a reference.  
 */
const DATETIME RefClockSettings = { 20, // century
                                    01, // year
                                    01, // month
                                    01, // day
                                    01, // day of week : 1=Mon, 2=Tues,...7=Sun
                                    00, // hour
                                    00, // minutes
                                    00  // seconds
}; 

void SysClockSetByPSD(void)
{
	TimeSetByPSD = TRUE;
}

//****************************************************************************
// FUNCTION NAME: SetSystemClock
// DESCRIPTION: This function sets the real time clock shadow to the current   
//  time.  This function must be called on powerup to initialize the Rt 
//  clock shadow and periodically to re-sync
//
// AUTHOR: Brian Hannigan
//
// INPUTS:  DATETIME *pBinaryClockSettings - pointer to new settings
//
// OUTPUTS: BOOL - TRUE is returned if date/time is valid.
//***************************************************************************
BOOL SetSystemClock(DATETIME *pBinaryClockSettings) 
{
  unsigned char ucRefDayOfWeek;
  long lStartJulianDays;
  long lEndJulianDays;
  long lDaysInBetween;
  BOOL bRetval = true;
  char  sbuf[50];
  DATETIME      rTime;


    sprintf(sbuf,"SetSystemClock function");
    fnSystemLogEntry(SYSLOG_TEXT, sbuf, strlen(sbuf));
	
  /*
   * Depending on who is calling this function, the day of week is not always passed in
   * as a valid parameter.  In order to avoid problems, we will calculate the day of week,
   * using 1/1/2001 as a reference date defined in the RefClockSettings structure.  Then,
   * the reference date and the date being set will be converted to Julian days and the
   * referenece date will be subtracted from the date being set.  If a negative value results,
   * then the date being set is older than the reference date which is not valid so report an error.
   * Else, apply a modulus of 7 to the difference but before doing so, add an offset to the difference 
   * that is equal to the reference date day of the week.  In this case 1/1/2001 was a Monday, which is
   * a day of week = 1. By adding this offset, (ucRefDayOfWeek) the result of the modulus operation will 
   * yield the correct day of week.
   */ 
  ucRefDayOfWeek = RefClockSettings.bDayOfWeek;
  lStartJulianDays = MdyToJulian( &RefClockSettings );
  lEndJulianDays = MdyToJulian( pBinaryClockSettings );
  lDaysInBetween = lEndJulianDays - lStartJulianDays;

  if( lDaysInBetween < 0 )
    bRetval = false;
  else
    {
      /*
       * The modulus operation in the following line will yield a result of 0-6.  Note that 1 is subtracted
       * from ucRefDayOfWeek because by convention, the day of week goes from 1-7, e.g., in the 
       * RefClockSettings structure, it is set to 1 indicating  Monday.  This convention is used elsewhere
       * in the Mega code thus consistency is important.  However, for example, if lDaysInBetween is 0 and
       * ucRefDayOfWeek is 1, we want the result of the modulus to be 0, that's why 1 is subtracted from
       * ucRefDayOfWeek. 
       * Also, we are going to  add 1 to the result because the subsequent call to "fnValidDate" expects a 
       * valid day of the week of 1-7.  Since fnValidDate is common to Spark also, the correction is made here 
       * instead of changing fnValidDate. 
       */ 
      pBinaryClockSettings->bDayOfWeek = ( ( lDaysInBetween + ( ucRefDayOfWeek - 1 ) ) % 7 ) + 1; 
    }

  if( bRetval == true && fnValidDate( pBinaryClockSettings ) ) 
  {
	   bRetval = HALSetRTC(pBinaryClockSettings); // set RTC
	   if (bRetval == true)
	   {// set OS RTC
		   bRetval = setOSClock(pBinaryClockSettings);
	   }
    
// Create log entry with local and GMT time when system clock is set
       (void)GetSysDateTime( &rTime, ADDOFFSETS, GREGORIAN );
       sprintf(sbuf,"Set Time %02d/%02d/%02d %02d:%02d:%02d (%02d:%02d:%02dGMT)",
            rTime.bMonth, rTime.bDay, rTime.bYear, 
            rTime.bHour, rTime.bMinutes, rTime.bSeconds,
            pBinaryClockSettings->bHour, pBinaryClockSettings->bMinutes, pBinaryClockSettings->bSeconds);
       fnSystemLogEntry(SYSLOG_TEXT, sbuf, strlen(sbuf));
       fnSystemLogHeader(sbuf, 2);     // Also put in log header so it is not overwritten
    
       memcpy(&CMOSPreserve.lwClockWasSet, "CLOK", 4); // update CMOS with "clock set" signature

  }
  else
  {
       bRetval = false;
  }

  //Need to report error if clock is no good
  if (bRetval == false)
  {
      fnLogError(ERR_TYPE_NORMAL, ERROR_CLASS_SYS, ECSYS_CLOCK_NOT_OK);
  }

  return( bRetval );
}



/****************************************************************************
** FUNCTION NAME: ReadRTClock
** DESCRIPTION:  Returns the value of the real time clock
**             
** AUTHOR: Brian Hannigan
**             
** INPUTS:  DATETIME *pDateTimePtr - pointer to a DATETIME structure
**  
** OUTPUTS: BOOL - TRUE is returned if date is valid.
**  
****************************************************************************/
BOOL ReadRTClock( DATETIME *pDateTimePtr ) 
{
	HALGetRTC(pDateTimePtr);

  if( MdyValid( pDateTimePtr ) )    // check for valid date  
    return (true);
  else
    return (false);
    
}

/****************************************************************************\
** FUNCTION NAME: fnStartClockAlarmJanus
** DESCRIPTION: Starts the OS timer ALARM_TIMER to expire at the specified
**              time.   
**
**              NOTE: This function is unique for Janus because the SH7622 does 
**              not have a hardware alarm clock.  
**                                    
** AUTHOR: Brian Hannigan
**
** INPUTS:  DATETIME alarmtime = the alarm expiration time
**
** OUTPUTS: BOOL - SUCCESS/FAIL
\****************************************************************************/
BOOL fnStartClockAlarmJanus( DATETIME *alarmtime)
{
  DATETIME currentTime;
  BOOL bRangeCheckFailed = false;
  uchar ucRetcode = ALARM_SET_SUCCESSFUL;
  long  timeDiffInSecs;
  unsigned long timeDiffInTicks;
  uchar           ucDummyData = 0;


  if( !DTRangeCheck( alarmtime->bDay, BCDtoBinary( 0x00 ), BCDtoBinary( 0x31 ) ) )
    bRangeCheckFailed = true;

  else if( !DTRangeCheck( alarmtime->bHour, BCDtoBinary( 0x00 ), BCDtoBinary( 0x23 ) ) )
    bRangeCheckFailed = true;

  else if( !DTRangeCheck( alarmtime->bMinutes, BCDtoBinary( 0x00 ), BCDtoBinary( 0x59 ) ) )
    bRangeCheckFailed = true;

  else if( !DTRangeCheck( alarmtime->bSeconds, BCDtoBinary( 0x00 ), BCDtoBinary( 0x59 ) ) )
    bRangeCheckFailed = true;
  
  if (( !bRangeCheckFailed ) && (GetSysDateTime(&currentTime, NOOFFSETS, GREGORIAN)))
  {
    AlarmExpireTime = *alarmtime;   // If the alarm time is valid, store it in the current alarm setting.

    (void)CalcDateTimeDifference( &currentTime, &AlarmExpireTime, &timeDiffInSecs );  

    timeDiffInTicks = timeDiffInSecs * (1000 / MILLISECONDS_PER_TICK);

    if(   (timeDiffInSecs > 0)
       && (timeDiffInTicks < (0xFFFFFFFF)) )  // make sure time delta is within our range
    {
        (void)OSChangeTimerDurationByTicks( ALARM_TIMER, timeDiffInTicks );  // set new period
        (void)OSStartTimer( ALARM_TIMER );  // and start the timer
    }
    else if(timeDiffInSecs <= 0)
    {
/*
        // It may be caused by the case that the local time is faster than PSD time, 
        // so try to resync the system clock with PSD
        if(fnValidData(BOBAMAT0, BOBID_IPSD_INIT_CLOCK, &ucDummyData) != BOB_OK)
        {
            fnProcessSCMError();
        }
        else if(GetSysDateTime(&currentTime, NOOFFSETS, GREGORIAN) == TRUE)
        {
            (void)CalcDateTimeDifference( &currentTime, &AlarmExpireTime, &timeDiffInSecs );

            // Change the time diff to 1 if it is still less than 0, 
            // so that the alarm will happen one second later than current time.
            if(timeDiffInSecs <= 0)
*/
            {
                timeDiffInSecs = 1;
            }

            timeDiffInTicks = timeDiffInSecs * (1000 / MILLISECONDS_PER_TICK);

            (void)OSChangeTimerDurationByTicks( ALARM_TIMER, timeDiffInTicks );  // set new period
            (void)OSStartTimer( ALARM_TIMER );  // and start the timer
/*
        }
        else
        {
            ucRetcode = ALARM_SET_ERROR;
        }
*/
    }
    else
    {
        ucRetcode = ALARM_SET_ERROR;
    }
  }
  else
  {
    ucRetcode = ALARM_SET_ERROR;
  }

  return( ucRetcode );
}


/****************************************************************************\
** FUNCTION NAME: fnStopClockAlarm  
** DESCRIPTION:  Disable the OS Timer used for alarms
** AUTHOR: Brian Hannigan
\****************************************************************************/
void fnStopClockAlarm( void )
{
    (void)OSStopTimer( ALARM_TIMER );
}

/****************************************************************************\
** FUNCTION NAME: fnInitAlarmInterrupt  
** DESCRIPTION: Create HISR for alarm timer
** AUTHOR: Brian Hannigan
\****************************************************************************/
void fnInitAlarmInterrupt( void ) 
{
  (void) OSCreateHISR( &ClockDevHisr, ClockHisrHandler, HISRSTACKSIZE*10, "AlarmHisr" );
}

/******************************************************************************
   FUNCTION NAME: fnCheckGMTOffset
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Check if passed GMT offset value is within range for our PSD
   PARAMETERS   : None
   RETURN       : OUT_OF_RANGE 0 or IN_RANGE 1
   NOTES        : 
******************************************************************************/
char fnCheckGMTOffset(long gmtOffset)
{
    char retval = OUT_OF_RANGE;

    if(  fnGetCurrentPsdType() & PSDT_ALL_IBUTTON )
    {
        if( (gmtOffset > IPSD_MIN_GMT_OFFSET) && (gmtOffset < IPSD_MAX_GMT_OFFSET) )
        {
            retval = IN_RANGE;
        }
    }

    return(retval);
}
       
/****************************************************************************\
** FUNCTION NAME: fnClockTimerExpire
** DESCRIPTION:  Timer expiration function - just activates clock HISR
** AUTHOR: Brian Hannigan
\****************************************************************************/
void fnClockTimerExpire(unsigned long l)
{
    (void)OSActivateHISR( &ClockDevHisr );
}

/****************************************************************************\
** FUNCTION NAME: getMsClock
** DESCRIPTION: Returns 1 millisecond count value from reset.
\****************************************************************************/
#define             NU_HW_Ticks_Per_MilliSecond                  (NU_HW_Ticks_Per_Second / 1000)

unsigned long getMsClock(void)
{
    return GetSystemTime();
}

/****************************************************************************\
** FUNCTION NAME: setOSClock
** DESCRIPTION: Sets OS Clock to be a RTC value
\****************************************************************************/

BOOL setOSClock( DATETIME *pDateTimePtr )
{
	clockid_t clock_id;
	struct timespec tp;
	struct tm time_struct;
	time_t conv_time;
	int ret;

	memset(&time_struct, 0, sizeof(time_struct));
	time_struct.tm_sec = pDateTimePtr->bSeconds;
	time_struct.tm_min = pDateTimePtr->bMinutes;
	time_struct.tm_hour = pDateTimePtr->bHour;
	time_struct.tm_mday = pDateTimePtr->bDay;
	time_struct.tm_mon = pDateTimePtr->bMonth - 1; //Posix mktime month is 0 based so convert from 1 based input
	time_struct.tm_year = ((pDateTimePtr->bCentury * 100) + pDateTimePtr->bYear) - 1900;

	conv_time = mktime(&time_struct);
	if (conv_time == -1)
	{
		return false;
	}

	tp.tv_nsec = 0;
	tp.tv_sec = conv_time;

	/* Get the time for the system clock */
	clock_id = CLOCK_REALTIME;
	ret = clock_settime(clock_id, &tp);

	if (ret == POSIX_SUCCESS)
	{
		return true;
	}
	else
	{
		return false;
	}

}

/****************************************************************************\
** DESCRIPTION: Determines if OS Clock is valid (namely if date is valid and is in 2017 or later)
\****************************************************************************/
BOOL validSysClock(void)
{
	BOOL retval;
    DATETIME    currentTime;

    // Get the current time (GMT)
    retval = GetSysDateTime( &currentTime, NOOFFSETS, GREGORIAN );

    if (retval == FALSE)
    	return FALSE;

	if(TimeSetByPSD == FALSE)
    	return FALSE;

    if (currentTime.bYear < 17)
    	return FALSE;

    return TRUE;
}
