
#include "commontypes.h"
#include "blinker.h"
#include "hal.h"

#define N_ELEMENTS(x) (sizeof(x)/sizeof((x)[0]))

typedef struct
{
    uint32_t on_length  : 7;    //  on length in 10mSecs steps
    uint32_t off_length : 7;    // off length in 10mSec steps
    uint32_t num_blinks : 4;    // #times to blink  
    uint32_t pat_delay  : 12;   // delay to next pattern line in 10mSec steps 0.00 - 40.95 Seconds 
    uint32_t color      : 2;    // 1=white, 2=red 3=blue
} pattern_t;

typedef enum
{
    LED_NONE = 0,
    LED_WHITE,
    LED_RED,
    LED_BLUE
} panel_led_color_t;

//
// Blinker patterns as per 1R00036 - Cygnus X3 - Sigma ASIC Error Blinker Codes Rev C
//
//
//  No error pattern :-
//
//      50mS        50mS                                    50mS        50mS         
//      +---+       +---+                                   +---+       +---+        
//      |   | 100mS |   |            2.0 Sec                |   | 100mS |   |        
//  ----+   +-------+   +-----------------------------------+   +-------+   +--------
//
//
#ifdef BLINK_LOUD_HEARTBEAT
pattern_t blink_ok[] =  {
                            { 5, 10, 1,   0, LED_WHITE}
                           ,{ 5, 10, 1,   0, LED_RED}
                           ,{ 5, 10, 1,  55, LED_BLUE}
                        };
#else
//                  {50mS ON, 100mS OFF} 2 times, then delay 1900mSec
//pattern_t blink_ok[] = {{ 5, 10, 2, 190, LED_WHITE}};		// Heartbeat flash
//
pattern_t blink_ok[] = {{ 127, 0, 1, 0, LED_WHITE}};		// constant on white LED
#endif


//
// Error counting code 2 - 3
//
//  50mS    50mS          100mS         100mS                 100mS     100mS     100mS             50mS    50mS 
//  +-+     +-+           +---+         +---+                 +---+     +---+     +---+             +-+     +-+  
//  | |100mS| |   300mS   |   |  300mS  |   |     1.0 Sec     |   |300mS|   |300mS|   |    1.0 Sec  | |100mS| |  
// -+ +-----+ +-----------+   +---------+   +-----------------+   +-----+   +-----+   +-------------+ +-----+ +--
//    beacon                                                                                          beacon     
//
//
pattern_t blink_err[] = {//  on, off, #, pauze
                            { 5,  15, 2,    20, LED_WHITE }
                           ,{10,  30, 2,    70, LED_RED   }  // the 2 is just a sample count
                           ,{10,  30, 3,    70, LED_BLUE  }  // the 3 is just a sample count.
                        };

pattern_t blink_sleep[] = {//  on, off, #, pauze
                            {25,  25, 2,    70, LED_WHITE }
                        };

pattern_t blink_sleep_save[] = {//  on, off, #, pauze
                            {15,  5, 1,    10, LED_WHITE }
                            ,{15, 5, 1,    10, LED_RED }
                            ,{15, 5, 1,    10, LED_WHITE }
                            ,{15, 5, 1,    10, LED_RED }
                        };

pattern_t blink_crash[] = {//  on, off, #, pauze
                            {15,  5, 1,    10, LED_WHITE }
                            ,{5,  5, 1,    10, LED_WHITE }
                            ,{15, 5, 1,    10, LED_RED }
                            ,{5, 5, 1,    10, LED_RED }
                            ,{15, 5, 1,    10, LED_BLUE }
                            ,{5, 5, 1,    10, LED_BLUE }
                            ,{15, 5, 1,    10, LED_RED }
                            ,{5, 5, 1,    10, LED_RED }
                        };

typedef enum
{
    PAT_WAIT_FOR_TICK = 0,
    PAT_LED_ON,             // waiting for the end of the ON state
    PAT_LED_OFF,            // waiting for the end of the OFF state
    PAT_DELAY               // waiting to go to next pattern
} pattern_state_t;

#define MAX_PATTERNS 10

typedef struct
{
    pattern_state_t state;
    uint8_t         num_of_patterns;
    uint8_t         pat_idx;    // current pattern
    pattern_t       pattern[MAX_PATTERNS];
    uint8_t         ticks_until_next_step;
    uint8_t         blinks_done;

} blinker_t;

bool blinker_enabled = false;

blinker_t blinker;

/* -----------------------------------------------------------------------------------------------[set_blink_error]-- */
void set_blink_error(uint8_t code)
{
    blinker_enabled = false;
    if (code == 0)
    {
        memcpy(blinker.pattern, blink_ok, sizeof(blink_ok));
        blinker.num_of_patterns = N_ELEMENTS(blink_ok);
        blinker.state = PAT_LED_ON;
    }
    else
    {
        memcpy(blinker.pattern, blink_err, sizeof(blink_err));
        blinker.num_of_patterns = N_ELEMENTS(blink_err);
        blinker.pattern[1].num_blinks = (code >> 4);
        blinker.pattern[2].num_blinks = (code & 0x0F);
        blinker.state = PAT_WAIT_FOR_TICK;
    }
    blinker_enabled = true;

}

/* -----------------------------------------------------------------------------------------------[set_blink_sleep]-- */
void set_blink_crash()
{
    blinker_enabled = false;
	memcpy(blinker.pattern, blink_crash, sizeof(blink_crash));
	blinker.num_of_patterns = N_ELEMENTS(blink_crash);
    blinker.state = PAT_WAIT_FOR_TICK;
    blinker_enabled = true;
}

/* -----------------------------------------------------------------------------------------------[set_blink_sleep]-- */
void set_blink_sleep()
{
    blinker_enabled = false;
	memcpy(blinker.pattern, blink_sleep, sizeof(blink_sleep));
	blinker.num_of_patterns = N_ELEMENTS(blink_sleep);
    blinker.state = PAT_WAIT_FOR_TICK;
    blinker_enabled = true;
}

/* -----------------------------------------------------------------------------------------------[set_blink_sleep]-- */
void set_blink_sleep_save()
{
    blinker_enabled = false;
	memcpy(blinker.pattern, blink_sleep_save, sizeof(blink_sleep_save));
	blinker.num_of_patterns = N_ELEMENTS(blink_sleep_save);
    blinker.state = PAT_WAIT_FOR_TICK;
    blinker_enabled = true;
}

/* ------------------------------------------------------------------------------------------------[SelectPanelLed]-- */
void SelectPanelLed(panel_led_color_t color)
{
    switch (color)
    {
        case LED_BLUE : HALSetBlueLED(true);   break;
        case LED_RED  : HALSetRedLED(true);    break;
        case LED_WHITE: HALSetWhiteLED(true);  break;
        case LED_NONE : /* fall through */
        default:        HALSetWhiteLED(false); break;
    }
}

/* -----------------------------------------------------------------------------------------[led_blinker_10ms_tick]-- */
void led_blinker_10ms_tick(void)
{
    // to be called every 10mSecs
    if (blinker.ticks_until_next_step > 1)
    {
        blinker.ticks_until_next_step--;
        return;
    }

    pattern_state_t next_state = blinker.state;

    switch(blinker.state)
    {
        case PAT_WAIT_FOR_TICK:
            blinker.blinks_done = 0;
            blinker.pat_idx = 0;
            blinker.ticks_until_next_step = blinker.pattern[blinker.pat_idx].on_length;
            next_state = PAT_LED_ON;
            break;

        case PAT_LED_ON:
            blinker.ticks_until_next_step = blinker.pattern[blinker.pat_idx].off_length;
            if(blinker.ticks_until_next_step > 0)
            	next_state = PAT_LED_OFF;
            break;

        case PAT_LED_OFF:
            blinker.blinks_done++;
            if (blinker.blinks_done < blinker.pattern[blinker.pat_idx].num_blinks)
            {
                blinker.ticks_until_next_step = blinker.pattern[blinker.pat_idx].on_length;
                next_state = PAT_LED_ON;
            }
            else
            {
                blinker.ticks_until_next_step = blinker.pattern[blinker.pat_idx].pat_delay;
                next_state = PAT_DELAY;
            }
            break;

        case PAT_DELAY:
            blinker.pat_idx = (blinker.pat_idx+1) % blinker.num_of_patterns;
            blinker.ticks_until_next_step = blinker.pattern[blinker.pat_idx].on_length;
            blinker.blinks_done = 0;
            next_state = PAT_LED_ON;
            break;

        default:
            blinker_enabled = false;
            break;

    }

    blinker.state = next_state;
    if (next_state==PAT_LED_ON)
    {
        SelectPanelLed(blinker.pattern[blinker.pat_idx].color);
        if (blinker.pattern[blinker.pat_idx].num_blinks == 0)
        {
            // if we need to represent 0 blinks, switch off immediately
            SelectPanelLed(LED_NONE);
        }
    }
    else
        SelectPanelLed(LED_NONE);

}

/* -------------------------------------------------------------------------------------------[set_blinker_enabled]-- */
bool set_blinker_enabled(bool new_state)
{
    bool old_State = blinker_enabled;
    blinker_enabled = new_state;
    return old_State;
}

