#include                <stdio.h>
#include                "nucleus.h"

// these includes are needed for the TCC_INTERRUPTS_DISABLE call
#include        "kernel/nu_kernel.h"
#include        "os/kernel/plus/common/error_management.h"
#include        "os/kernel/plus/common/thread_control.h"
#include        "os/kernel/plus/common/cbm.h"
#include        "services/nu_trace_os_mark.h"
//
#include		"commontypes.h"
#include        "am335x_timer.h"
#include        "blinker.h"

#define NUCLEUS_VECTOR_OFFSET			2
#define BANK(x)							(x/32)
#define BITPOS(x)						{x % 32)

// Set up timer addresses so that user can simply specify the timer required by number....
const UINT32	AM335TimerBase[] = { 	TIMER0_BASE,
										TIMER1_BASE,
										TIMER2_BASE,
										TIMER3_BASE,
										TIMER4_BASE,
										TIMER5_BASE,
										TIMER6_BASE,
										TIMER7_BASE} ;


const UINT32		CMPerTimerClkCtrl[] = { CM_PER_NULL_CLKCTRL,
										CM_PER_NULL_CLKCTRL,
										CM_PER_TIMER2_CLKCTRL,
										CM_PER_TIMER3_CLKCTRL,
										CM_PER_TIMER4_CLKCTRL,
										CM_PER_TIMER5_CLKCTRL,
										CM_PER_TIMER6_CLKCTRL,
										CM_PER_TIMER7_CLKCTRL};

const UINT32		CMDPLLTimerClk[] = { CM_DPLL_TIMER0_CLK,
										CM_DPLL_TIMER1_CLK,
										CM_DPLL_TIMER2_CLK,
										CM_DPLL_TIMER3_CLK,
										CM_DPLL_TIMER4_CLK,
										CM_DPLL_TIMER5_CLK,
										CM_DPLL_TIMER6_CLK,
										CM_DPLL_TIMER7_CLK};

const UINT		TimerIRQVectors[] = {	ESAL_PR_INT_IRQ66_TINT0     ,
										ESAL_PR_INT_IRQ67_TINT1_1MS ,
										ESAL_PR_INT_IRQ68_TINT2     ,
										ESAL_PR_INT_IRQ69_TINT3     ,
										ESAL_PR_INT_IRQ92_TINT4     ,
										ESAL_PR_INT_IRQ93_TINT5     ,
										ESAL_PR_INT_IRQ94_TINT6     ,
										ESAL_PR_INT_IRQ95_TINT7      };

const UINT32      DRMSuspendCtrl[] = {  DRM_SUSP_CTRL_TIMER0,
                                        DRM_SUSP_CTRL_TIMER1,
                                        DRM_SUSP_CTRL_TIMER2,
                                        DRM_SUSP_CTRL_TIMER3,
                                        DRM_SUSP_CTRL_TIMER4,
                                        DRM_SUSP_CTRL_TIMER5,
                                        DRM_SUSP_CTRL_TIMER6,
                                        DRM_SUSP_CTRL_TIMER7 };

// ISR call counter - debug
UINT32 globalISRScratchPad = 0;

/* ----------------------------------------------------------------------------------------[prepTimerForUpCounting]-- */
// simply set up the required timer for up count overflow work then preload it with a start value
// that will allow it to overflow in a short time
// for now assume TIMER4 is in use
void prepTimerForUpCounting(int iTimerIdx)
{
	UINT32 *ptrCMDPLL 	= (UINT32 *)CMDPLLTimerClk[iTimerIdx];
	UINT32 *ptrCMPer 	= (UINT32 *)CMPerTimerClkCtrl[iTimerIdx];
	UINT32 *ptrTimer	= (UINT32 *)AM335TimerBase[iTimerIdx] ;
	UINT32 *ptrDRM	    = (UINT32 *)DRMSuspendCtrl[iTimerIdx] ;
	UINT	vector		= TimerIRQVectors[iTimerIdx] ;
    VOID                (*old_lisr)(INT);


    // enable clock and peripheral modules...
	*ptrCMDPLL	= 0x01;
	*ptrCMPer	= 0x0002;
	// wait for timer module to become functional...
	while((*ptrCMPer & 0xFFFF0000) != 0)
		NU_Sleep(1);

    /* Register the interrupt service routine for this device. */
    NU_Register_LISR(vector, timerIRQHandler,&old_lisr);

    /* Enabling the interrupt for timer 4, with no trigger time, and priority 5 */
    ESAL_GE_INT_Enable(vector,0,0x5);

    //todo: select which clock to use. We now default to CLK_M_OSC
    
	*ptrDRM = 9; /* suspend clock during debug halt */

	// Start timer, auto reload, (PTV)0 prescale, prescale enable  --> CLK_M_OSC/2 == 24/2 == 12MHz 
	*(ptrTimer  + TIMER_TCLR/4)  = 0x0023;

    // timer rate = (FFFF_FFFF - TLDR + 1) * (1/12MHz) * 2(PTV+1)
    //            = (FFFF_FFFF - FFFE_7960) * (1 / 12 Mhz) * 2 = 16.666 mSec
    //
    // TLDR = 
	// start value...
	*(ptrTimer + TIMER_TCRR/4) = 0xFFFE7960; // = -100,000

	// reload value
	*(ptrTimer + TIMER_TLDR/4) = 0xFFFFFFFF-11999;  // 12000 ticks --> 1mSec

	// Enable IRQ for overflow
	*(ptrTimer + TIMER_IRQENABLE_SET/4) = 2 ;
}

/* ----------------------------------------------------------------------------------[prepTimer_0_AsFreerunningTmr]-- */
void prepTimer_0_AsFreerunningTmr()
{
    /* timer 0 is already started by the rom bootloader 
     * we need to make it autoreload on overflow. The clock
     * for timer 0 is always 32768 Hz!
     */

    UINT32 *ptrTimer    = (UINT32 *)AM335TimerBase[0];

    *(ptrTimer + TIMER_TLDR/4) = 0; /* reload 0x00000000 on overflow */
    *(ptrTimer + TIMER_TCLR/4) = 0x0003;    /* start timer, auto reload prescale=0 --> 32768Hz */
}

#ifdef MEMORYTEST
void ev_timer_tick_changed(uint32_t time);
#endif

volatile uint32_t tick_count_1ms = 0;

/* -------------------------------------------------------------------------------------------------[GetSystemTime]-- */
uint32_t GetSystemTime()
{
    INT old_intr_level = NU_Control_Interrupts(NU_DISABLE_INTERRUPTS);
    uint32_t current_tick = tick_count_1ms;
    NU_Control_Interrupts(old_intr_level); 

    return current_tick;

    //uint64_t hw_tick = NU_Get_Time_Stamp();  /* There are NU_HW_Ticks_Per_Second */
    ///* convert to #mSec ticks */
    //return (hw_tick /100) * NU_HW_Ticks_Per_Second / 10000;  
}

/* ------------------------------------------------------------------------------------------[get_32kHz_tick_count]-- */
uint32_t get_32kHz_tick_count()
{
    return *(volatile uint32_t *)(TIMER0_BASE + TIMER_TCRR);
}

/* -----------------------------------------------------------------------------------------------[timerIRQHandler]-- */
// simple ISR handler
VOID    timerIRQHandler(INT vector)
{
    static uint32_t last_10mSec_tick = 0;
           uint32_t reg;

    /* Read the interrupt status register */
    reg = ESAL_GE_MEM_READ32(AM335TimerBase[TIMER_TO_TEST] + TIMER_IRQSTATUS);

    /* Clear the set interrupts */
    ESAL_GE_MEM_WRITE32(AM335TimerBase[TIMER_TO_TEST] + TIMER_IRQSTATUS, reg);

    tick_count_1ms++;

	if (blinker_enabled && tick_count_1ms - last_10mSec_tick >= 10)
    {
        last_10mSec_tick = tick_count_1ms;
        led_blinker_10ms_tick();
    }

#ifdef MEMORYTEST
    ev_timer_tick_changed(tick_count_1ms);
#endif
}

/* ------------------------------------------------------------------------------------------------[readTimerValue]-- */
UINT32 readTimerValue(int iTimerIdx)
{
	return (UINT32) *(((UINT32 *)AM335TimerBase[iTimerIdx]) + TIMER_TCRR/4) ;
}

/* -------------------------------------------------------------------------------------------------[readIRQStatus]-- */
UINT32 readIRQStatus(int iTimerIdx)
{
	return (UINT32) *(((UINT32 *)AM335TimerBase[iTimerIdx]) + TIMER_IRQSTATUS/4) ;
}

/* -----------------------------------------------------------------------------------------------------------[eof]-- */

