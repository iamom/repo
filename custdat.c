/*****************************************************************************
  PROJECT:        Mega
  COMPANY:        Pitney Bowes
  AUTHOR :        
  MODULE NAME:    CUSTDAT.C
      
  DESCRIPTION:    Non volatile Customer Data

----------------------------------------------------------------------
              Copyright (c) 2001 Pitney Bowes Inc.
                   35 Waterview Drive
                Shelton, Connecticut  06484
----------------------------------------------------------------------
 MODIFICATION HISTORY: 

 21-Mar-14 sa002pe on FPHX 02.12 shelton branch
 	For CMOS version 0x292:
	1. Merged in the following changes for U.K. EIB from Janus:
		a. Increased the size of the feature table from 64 to 65 for U.K. EIB development.
		b. Added CMOSRatesExpired.
		c. Added nextBetweenPeriodEndDate & fbUseNextEndDate to the DCB_CURRENT_PERIOD_t_EXP structure.
		d. Changed the number of entries in the system base log from 10 to 1 since the base can't be changed for G9.
		e. Added ExpirationDate to the sCMOSRateComponents structure.
	2. Removed PrnSettingsTable from PrintParams because it isn't used by G9.

 20-Mar-14 sa002pe on FPHX 02.12 shelton branch
 	For CMOS version 0x291:
	1. Related to Janus Fraca 224510: Added ucCMOSAbacusAcntErrorCode. 

 11-Mar-14 sa002pe on FPHX 02.12 shelton branch
	More for CMOS version 0x290:
	1. Related to Janus Fraca 224510: Added fCMOSAbacusAcntProblem. 
	2. Added usCMOSMaxSmallParitions & usCMOSMaxLargeParitions.
	
 26-Jun-13 Kevin Brown on FPHX 02.10 shelton branch
    Added ProxyConfig CMOSProxyConfig for the proxy implementation.
 
 19-Feb-13 sa002pe on FPHx 02.10 shelton branch
 	Merging in changes from the 01.10 branch for Brazil that didn't get merged back to the main branch
----
	  2009.05.08  Clarisa Bellamy   1.10  
	  - Added Balance Inquiry Log for Brazil.
----

  2012.11.08 Clarisa Bellamy  FPHX 02.10
  - Put the CMOSMiniSysLog into its own (new) section, so that it can live at
    a fixed address.  This makes it easier to retrieve.

  2012.11.06 Clarisa Bellamy  FPHX 02.10
  - Add a MiniSysLog in the CMOS2 section (only on 475), and data to support it.

  2012.09.21    John  Gao FPHX 2.10
  Added new vairable CMOSPlatformFilteringOn for scale message filtering.

  2010.07.12    Raymond  Shen FPHX 2.07
  Added new vairable CMOSRefillCode[] for Refill code feature.

  2009.02.16     Rocky Dai  FPHX01.14
   Added new variable: exp_mtr_cmos. it's needed by Slovakia Town Name.

  2009.02.24    Clarisa Bellamy FPHX 2.00
   Added stuff for support of extended CMOS on DM475.
   - Added a variable to mark identify the second CMOS section, CMOS2_Mark.  
     Even though the preserved section starts at the same address as the 
     Hardware second cmos section (unlike the first preserved section,) this 
     just makes a bunch of stuff easier and more consistent.  
   - Added some code that is just for testing new cmos preservation stuff on a 
     a DM300/400 by faking a second section of cmos.
     The key, TESTING_FOR_2_SECTIONS, is turned off, so it should not affect anyone. 
     It is delimited by comments: !!!!!!
  
  2009.02.18     Jingwei,Li 
  Add new variables :bCMOSWeightLogHead,bCMOSWeightLogNumEntries,
                     wCMOSWeightLog,bCMOSPortraitMode,CMOSWOWCalibrationLog
                     for WOW feature.
  2008.12.19     Yan, Jiang        FPHX02.00
   Add new array for the full version of Weight Break Table into the 
   additional area of CMOS based on hardware check.

 2008.12.11      Jingwei.Li             FPHX02.00
  Added new variables: pCMOSWOWWeightLimit,fCMOSHybridModeRevertToWOW and 
  sbrErrorScreen for WOW feature.

  2008.11.04     Clarisa Bellamy    FPHX01.11
   - Moved fCMOSInfraUpdateReq here from CMOS.c, because it should be 
     preserved by the CMOS preservation software.

  2008.09.12     Clarisa Bellamy    FPHX01.11
   Added new variable: CMOSLastPrintedMailDate.   This will be needed when we
   go to countries that require a foreced upload on date change.

  2008.09.08     Joe Qu    FPHX01.13
    Add usCMOSGTSArchiveMaxSize for NetSet2

  2008.07.29     Joe Qu    FPHX01.12
    Merged varible cmosEuroConversion from jnus15.00_c1_cienet for Sweden
    to support the Dual Currency feature
    
  2008.07.28    Raymond Shen    FPHX01.11
    Added CMOSLastBatteryDisplay and CMOSPsdBatteryTestDate for PSD/CMOS Battery life.

  2008.07.24     Bob Li   FPHX01.11
    Added a varible to indicate if ignore the reverse rating override parameter.
    
  2008.05.22     Joey Cui   FPHX01.11
    Added Country Specific archive file related definitions
  
  2008.05.19    Clarisa Bellamy     FPHX 1.11
   Part of updating to latest graphics modules:
   - Add new CMOS variable, uwCMOSGFNeedsCmpxn.  This keeps track of how often
     something has been "deleted" but not removed from the graphics file. 
     It is not used yet, but it is written to, and will eventually be used.
  
  2008.02.29     Joey CUi   FPHX01.10
    Added CMOSLongDHOnTime
 2007.10.16 Patrice VRILLAUD - FPHX 1.09 (Swiss adaptations)
    Add CMOSCustomerID_RRN[]
  2007.09.28     Joey Cui    FPHX01.08
    Added bCMOSLANFirewallPingEnabled for Firewall Ping setting
  2007.09.27     Andy Mo    FPHX01.08
    Added CMOS variable bCMOSConnectionMode 
  2007.08.24     Andy Mo     FPHX01.07
    Added ulCMOSActualManifestedRecords and CMOSLastManifestedRecordID
    to support manifest power recovery functionality
    
  2007.08.07     Raymond Shen     FPHX01.07
    Added CMOSPieceID for German piece ID.
 
   2007.04.25     Ivan Le Goff     FPHX 01.05
    Replace uiAjoutPieceCount with uiCMOSAjoutPieceCount
    
  2007.03.29     Oscar Wang       FPHX 01.04
    Added CmosDownloadCtrlBuf, fCMOSShowRateActive, fCMOSShowRateEffective, fCMOSUserDenyRateActivePrompt,
    fCMOSUserDenyRateEffectivePrompt, fCMOSDateAdvUseNewRates for Rates Enhancements.
  
  2007.03.05     Ivan Le Goff     FPHX 01.05
    Add variables for mode ajout

  2006.08.03     Clarisa Bellamy     FPHX 01.02
    Eliminating delcon code, variables, etc that is obsolete since FPHX 
    supports GTS now.
    - Remove the CMOSDeliveryConfirmationLog
    - Remove MasterUploadRecordId, used for delcon.

  2006.07.31     Clarisa Bellamy     FPHX 01.02
    Removed CMOSAdSloganTable, CMOSInscrTable, CMOSOthGraphics, which are
    all obsolete since the invention of the .GAR file.
*
* 23-Sep-05 sa002pe on Janus 11.00 shelton branch
*   Added wBuildTestMode for debugging & testing purposes.
*   Changed lDcapTimestampFudge from unsigned to signed.
*
* 14-Sep-05 sa002pe on Janus 11.00 shelton branch
*   Added lDcapTimestampFudge & wMinPrintMargin for debugging & testing purposes.
*
* ----------------------------------------------------------------------
* PRE-CLEARCASE REVISION HISTORY:
*
*       $Log:   H:/group/SPARK/ARCHIVES/DM150/uic_app/CUSTDAT.C_v  $
 * 
 *    Rev 1.25   24 Jun 2005 12:24:22   HO501SU
 * Added a CMOS variable fCMOGuardedFFRecXferInProgress to work 
 * with Derek's fix for Final Frankit Record.
 * 
 *    Rev 1.24   11 May 2005 11:28:52   CR002DE
 * 1. Added a variable definition for FFS power failure. - Victor
 * 
 *    Rev 1.23   21 Apr 2005 17:58:56   CR002DE
 * 1. General CMOS optimization by eliminating unreferenced variables.
 * 2. Also marked other variables in custdat.c as obsolete.  They have refernces in 
 * the code but no real purpose.  They can be eliminated in the future.
 * 3. Optimized DCAP.  Aliased cmosDcapCurrPeriod to cmosDcapCurrPeriodEXP
 * so only one of them is defined in the build.  This means MAX_BUCKETS_EXP
 * always defines the number of buckets.
 * 
 *    Rev 1.22   10 Dec 2004 10:44:30   HO501SU
 * Added CMOSpfDebitLogRec.
 * 
 *    Rev 1.21   11 Nov 2004 09:44:54   NEX8SGV
 * Added new variable definition  marriedVaultNumber[16] . -David
 * 
 *    Rev 1.20   04 Nov 2004 15:44:20   sa002pe
 * Renamed cmosThirtyOneDayLog to cmosAdjustableLog
 * to match the changes in the common DC files.
 * 
 *    Rev 1.19   26 Oct 2004 15:36:30   cx08856
 * fixed a typo
 * 
 *    Rev 1.18   26 Oct 2004 11:47:50   cx08856
 * added cmosDcapEkpTable definition.
 * 
 *    Rev 1.17   18 Oct 2004 15:59:54   HO501SU
 * Added a CMOS ZWZP variable for OI.
 * 
 *    Rev 1.16   18 Oct 2004 10:04:32   NEX8SGV
 * Added new variable and definition  CMOSKIPPaswordInfo to CMOS. -David
 * 
 *    Rev 1.15   30 Aug 2004 16:10:50   SA002PE
 * CMOS version 31.
 * Added dummy variables for the EKP & Task ID
 * structures so CMOS preservation code will know
 * to set up things in the FFS.
 * Also added CMOS_DpServiceId for Germany.
 * 
 *    Rev 1.14   25 Aug 2004 16:19:38   SA002PE
 * CMOS version 30.
 * Commented out the definition of CMOSTaskNumberArray &
 * CMOSEkpNumberArray.  They're no longer needed now
 * that we're using the same stuff as Mega for GTS.
 * 
 *    Rev 1.13   19 Aug 2004 14:13:48   cx08856
 * CMOS version 29
 * updates for germany
 * Added cmosThirtyOneDayLog, cmosDcapCurrPeriodEXP, picklist_file_headers,
 * gts_archive_file_header, CMOSGenTrackingSvcs, CMOSEKPTaskNumberData.
 * 
 *    Rev 1.12   02 Aug 2004 16:28:00   SA002PE
 * CMOS version 28.
 * Added declaration of the task number & EKP number arrays.
 * 
 *    Rev 1.11   08 Jul 2004 13:09:18   SA002PE
 * Changes for CMOS v26.
 * Removed declaration of CMOSMemKeys. Now in RAM & declared
 * in oijpreset.c.
 * Commented out declaration of CMOSEasState because it isn't
 * being used.
 * Added declaration of CMOSFileUpdateStateInfo.
 * Added DUMMYCmos section for declaration of things that
 * are being moved to FFS so the datamaps for the new sw
 * will be different from the existing datamaps.
 * 
 *    Rev 1.10   30 Jun 2004 13:28:58   SA002PE
 * For CMOS version 25.
 * Added fCMOSDSTAutoUpdated.
 * 
 *    Rev 1.9   20 Apr 2004 11:49:30   SA002PE
 * Changes for CMOS version 22.
 * Added CMOS allocation of the permit array.
 * 
 *    Rev 1.8   31 Mar 2004 17:40:00   SA002PE
 * Added declaration of the text ad & text entry arrays.
 * 
 *    Rev 1.7   03 Dec 2003 13:53:12   defilcj
 * 1. updated all data capture stuff
 * 
 *    Rev 1.6   29 Oct 2003 16:58:18   SA002PE
 * Removed the declarations for the audit, inspection, &
 * UIC maintenance logs.
 * 
 *    Rev 1.5   21 Oct 2003 14:30:54   SA002PE
 * Replaced CMOS debit log w CMOS shadow logs.
 * 
 *    Rev 1.4   Oct 13 2003 11:09:06   mozdzjm
 * Added CMOSUpdatedToVersion variable.
 * Removed declaration of print manager cmos area from this file (moved to cmos.c)
 * because we don't want the preserve stuff to apply to that area..
 * 
 *    Rev 1.3   Aug 20 2003 14:18:42   mozdzjm
 * Changed bCMOSScaleVibrationSetting to fCMOSScaleLocallyCalibrated.  The former is not needed for Janus.
 * 
 *    Rev 1.2   19 Mar 2003 10:13:40   defilcj
 * changes for OS_06 build
* 
******************************************************************************/

#include "bob.h"
#include "custdat.h"
#include "trmdefs.h"
#include "networking/nu_net.h"
#include "networkmonitor.h"
/*--------------------------*/
/*     Global Variables     */
/*--------------------------*/



/**************************************************************************************************/
/* Note Do not move CMOSPreserve or place anything before it. It's starting address
  is used to determine the start of of the applications cmos area
*/
CMOSPRESERVE               CMOSPreserve __attribute__ ((section ("cmos")));
Diagnostics                CMOSDiagnostics __attribute__ ((section ("cmos")));
BatchData                  CMOSBatchData1 __attribute__ ((section ("cmos")));
ccStatus                   CMOSStatus1 __attribute__ ((section ("cmos")));
SetupParams                CMOSSetupParams __attribute__ ((section ("cmos")));
PCNParams                  CMOSPCNParams __attribute__ ((section ("cmos")));
PrintParams                CMOSPrintParams __attribute__ ((section ("cmos")));
FeatureTbl                 CMOSFeatureTable __attribute__ ((section ("cmos")));
InspectParams              CMOSInspectParams __attribute__ ((section ("cmos")));
ErrorLog                   CMOSErrorLog __attribute__ ((section ("cmos")));
ServiceModeLog             CMOSServiceLog __attribute__ ((section ("cmos")));              //obsolete
ShadowDebitLog             CMOSShadowDebitLog __attribute__ ((section ("cmos")));
ShadowRefillLog            CMOSShadowRefillLog __attribute__ ((section ("cmos")));
PostageChangeLog           CMOSPostageChangeLog __attribute__ ((section ("cmos")));
MoneyParams                CMOSMoneyParams __attribute__ ((section ("cmos")));
C6029Params                CMOSC6029Params __attribute__ ((section ("cmos")));
SystemErrorLog             CMOSSystemErrorLog __attribute__ ((section ("cmos")));
DeptAccountListManager     CMOSAccountLM __attribute__ ((section ("cmos")));
DeptAccountHeader          CMOSAccountHeader __attribute__ ((section ("cmos")));
SupervisorInfo             CMOSSupervisorInfo __attribute__ ((section ("cmos")));
NETCONFIG                  CMOSNetworkConfig __attribute__ ((section ("cmos")));
ProxyConfig                unusedCMOSProxyConfig __attribute__ ((section ("cmos")));
SysBasePCNLog              CMOSSysBasePCNLog __attribute__ ((section ("cmos")));
TEXT_AD_ARRAY              CMOSTextAdArray __attribute__ ((section ("cmos")));
TEXT_ENTRY_ARRAY           CMOSTextEntryArray __attribute__ ((section ("cmos")));
PERMIT_ARRAY               CMOSPermitArray __attribute__ ((section ("cmos")));
KIPPWInfo                  CMOSKIPPaswordInfo __attribute__ ((section ("cmos")));
ZWZP_DATA                  CMOSZwzpParams __attribute__ ((section ("cmos")));
PF_RECOVERY_LOG            CMOSpfDebitLogRec __attribute__ ((section ("cmos")));
T_CMOSBalanceInquiryLog    CMOSBalanceInquiryLog __attribute__ ((section ("cmos")));

BOOL                       fCMOSGuardedFFRecXferInProgress __attribute__ ((section ("cmos")));
UINT32                     CMOSPieceID __attribute__ ((section ("cmos")));
BOOL                       bCMOSConnectionMode __attribute__ ((section ("cmos")));

//  Used to be Number of unmanifested records in the manifest report
char reserved013[44]  __attribute__ ((section ("cmos")));

//TODO - pragmas below are instructions for datamap.exe to create special map; replace with something else
// look for the union label
//#pragma datamap datamapcommon SelectAccountingDatamap
//#pragma datamap datamapselect 20
ACCOUNTING_UNION           CMOSAccountingUnion __attribute__ ((section ("cmos")));
//#pragma datamapend

unsigned char   fCMOSLockCodeEnable __attribute__ ((section ("cmos")));
unsigned char   fCMOSAutoScaleEnable __attribute__ ((section ("cmos")));
unsigned char   fCMOSScaleLocationCodeInited __attribute__ ((section ("cmos")));
unsigned char   bCMOSScaleLocationCode __attribute__ ((section ("cmos")));
unsigned char   fCMOSScaleLocallyCalibrated __attribute__ ((section ("cmos")));
unsigned char   fCMOSDiffWAutoTapeEnable __attribute__ ((section ("cmos")));         //obsolete
//unsigned char bCMOSDiffWThreshold;
//unsigned char fCMOSDiffWThresholdInited;
unsigned char   pCMOSDiffTripWeight[LENWEIGHT] __attribute__ ((section ("cmos")));   //obsolete
unsigned char   pCMOSWOWWeightLimit[LENWEIGHT] __attribute__ ((section ("cmos")));
BOOL            fCMOSHybridModeRevertToWOW __attribute__ ((section ("cmos")));
unsigned char bCMOSWeightLogHead __attribute__ ((section ("cmos")));
unsigned char bCMOSWeightLogNumEntries __attribute__ ((section ("cmos")));
unsigned short wCMOSWeightLog[MAIL_PIECE_WEIGHT_LOG_NUM_ENTRIES] __attribute__ ((section ("cmos")));
unsigned char bCMOSPortraitMode __attribute__ ((section ("cmos")));
BOOL            fCMOSPresetRatesUpdated __attribute__ ((section ("cmos")));
BOOL            fCMOSDSTAutoUpdated __attribute__ ((section ("cmos")));
WOW_CALIBRATION_LOG        CMOSWOWCalibrationLog __attribute__ ((section ("cmos")));

char   CMOSLocalPhoneNumber[MAX_PHONE_LEN + 1] __attribute__ ((section ("cmos")));

BOOL fCMOSMultiTapes __attribute__ ((section ("cmos")));   // CMOS variable to store the Tape Print configuration
                        // TRUE, multi tapes print; FALSE, one tape print
BOOL fCMOSIgnoreReverseRateParam __attribute__ ((section ("cmos")));

// 04/24/2002 Joseph P. Tokarski - Added structure containing files to be
//                                   downloaded through the infrastructure
//                                   that are required for normal operation.
//
//                                 The structure replaces the variables
//                                   'CMOSMcpDownloadFilename' and
//                                   'CMOSSnipDownloadFilename'
//
FILES_TO_DOWNLOAD CMOSFilesToDownload __attribute__ ((section ("cmos")));

char remoteLoggingUrl[128] __attribute__ ((section ("cmos")));
unsigned char remoteLoggingEnabled __attribute__ ((section ("cmos")));

double dblCMOSPbpDebitBal __attribute__ ((section ("cmos")));
double dblCMOSPbpPrevDebitBal __attribute__ ((section ("cmos")));
double dblCMOSPbpCreditBal __attribute__ ((section ("cmos")));
double dblCMOSPbpPrevCreditBal __attribute__ ((section ("cmos")));
double dblCMOSPbpRefillAmt __attribute__ ((section ("cmos")));                 //probably obsolete
char reserved010[32]  __attribute__ ((section ("cmos")));

CMOSRATECOMPSMAP CMOSRateCompsMap __attribute__ ((section ("cmos")));

char CMOSPrintedIndiciaNum[PRINTED_INDICIA_NUM_SZ] __attribute__ ((section ("cmos")));
char CMOSIndiciaLanguageCode[INDICIA_LANGUAGE_CODE_SZ] __attribute__ ((section ("cmos")));
char CMOSCustomerID_RRN[CUSTOMERID_RRN_SZ] __attribute__ ((section ("cmos")));

unsigned long infrastructureNVM[2048] __attribute__ ((section ("cmos")));
unsigned long nvmHeap[3124] __attribute__ ((section ("cmos")));

//unsigned long MasterUploadRecordId; // Added for Special Service (Delcon) Record Unique ID
unsigned long CMOSExtUpdatedToVersion __attribute__ ((section ("cmos")));

// 06/06/2002 Joseph P. Tokarski - Added structures for the Base Revision data and
//                                    'Active Base Firmware' data for the Infrastructure.
//
CMOSACTIVEVERSIONDATA       CMOSActiveVersionData __attribute__ ((section ("cmos")));
CMOSBASEREVISIONDATA        CMOSBaseRevisionData __attribute__ ((section ("cmos")));           //obsolete


// External Accounting State
//S_EAS_STATE                   CMOSEasState ;      // persistent state info for external accounting
//ACCOUNTING_CONFIGURATION  sAccountingConfig;  // Accounting configuration data
//STATIC_XACTION_DATA           sXActionStaticData; // static Xaction data
//DYNAMIC_XACTION_DATA      sXActionDynamicData;// dynamic Xaction data 
REGISTER_VALUES               sStartRegData __attribute__ ((section ("cmos")));      // registers data before start a job      //JY, get it back.
REGISTER_VALUES               sEndRegData __attribute__ ((section ("cmos")));        // registers data before start a job      //JY, get it back.
//unsigned char             ucXStatus;          // XAction processing status (STATUS_NONE, STATUS_INPROCESS, STATUS_SAVED)


DCB_DIRECTORY_t cmosDcapDirectory __attribute__ ((section ("cmos")));
//DCB_CURRENT_PERIOD_t cmosDcapCurrPeriod;
DCB_PARAMETERS_t cmosDcapParameters __attribute__ ((section ("cmos")));
DCB_RULES_TABLE_t cmosDcapRuleSet[DC_MAX_RULES_TABLES] __attribute__ ((section ("cmos")));
DCB_BUCKET_NAME_LIST_t cmosDcapBucketNameList[DC_MAX_BUCKET_NAME_LISTS] __attribute__ ((section ("cmos")));
DCB_CLOSED_PERIOD_t cmosDcapClosedPeriod[DC_MAX_CLOSED_PERIODS] __attribute__ ((section ("cmos")));
pfp_t cmosDcapPfp __attribute__ ((section ("cmos")));
Daily_Register_Logs cmosDailyLogs __attribute__ ((section ("cmos")));
Tech_Supervision_Data cmosTechData __attribute__ ((section ("cmos")));         // Technical Supervisor Data Log
PostdatingLog       cmosPostdatingLog __attribute__ ((section ("cmos")));
Adjustable_Log  cmosAdjustableLog __attribute__ ((section ("cmos")));  // 31 Day circular buffer France
DCAP_UIC_TO_BASE_LOCKING_TABLE_t cmosDcapUicBaseLockingTbl __attribute__ ((section ("cmos")));
DCAP_EKP_TABLE_t    cmosDcapEkpTable __attribute__ ((section ("cmos")));
DCB_CURRENT_PERIOD_t_EXP cmosDcapCurrPeriodEXP __attribute__ ((section ("cmos")));
SBR_Control  sbrErrorScreen __attribute__ ((section ("cmos")));                // control the Shape Based Rating oversize error screen


DCAP_PF_SEQ DcapState __attribute__ ((section ("cmos")));
/* Dummy Variables used for conversion */
token_info_t DCAPTOKENVARNAME_t __attribute__ ((section ("cmos")));
rule_map_t DCAPRULEMAPSVARNAME_t __attribute__ ((section ("cmos")));
virtual_map_t DCAPVIRTMAPVARNAME_t __attribute__ ((section ("cmos")));
HBucket DCAPPDATAVARNAME_t __attribute__ ((section ("cmos")));
unsigned long DCAPPSTRVARNAME_t __attribute__ ((section ("cmos")));
unsigned short DCAPVIRTTOKENVARNAME_t __attribute__ ((section ("cmos")));

//unsigned char     ucAccType;          // present or last used accounting type 
//unsigned short        usXActionIDCounter; // XAction ID counter
unsigned long     ulMaxBuffSize __attribute__ ((section ("cmos")));      // Max buffer saved xaction for current accounting system     //JY, get it back.
unsigned long     ulHeadBuffPoint __attribute__ ((section ("cmos")));    // Pointer to first saved xaction in buffer   //JY, get it back.
unsigned long     ulTailBuffPoint __attribute__ ((section ("cmos")));    // Pointer to first spare field in buffer     //JY, get it back.
BOOL              bXBufferFull __attribute__ ((section ("cmos")));       // indicator of full buffer   //JY, get it back.
unsigned short        usCurrBuffPoint __attribute__ ((section ("cmos")));    // index of current xaction in buffer     //JY, get it back.
unsigned short        usFirstOpenPoint __attribute__ ((section ("cmos")));   // index of first open xaction in buffer  //JY, get it back.
unsigned short        usFirstFreeWBPoint __attribute__ ((section ("cmos"))); // index of first free space in WB buffer //JY, get it back.
unsigned short        usWBCMOSBuffSize __attribute__ ((section ("cmos")));   // size of WB buffer      //JY, get it back.
unsigned short        usWBBuffPoint __attribute__ ((section ("cmos")));      // WB record for current xaction      //JY, get it back.

unsigned char     eJournallingFlag __attribute__ ((section ("cmos")));   //            //JY, get it back.
unsigned char     eTransferFlag __attribute__ ((section ("cmos")));      //            //JY, get it back.
//unsigned short        shTransferAccIDFlag;//
//unsigned short        shTransferXIDFlag;  //

XFER_RECOVERY       sXferRecovery __attribute__ ((section ("cmos")));      //JY added, copied from COMT.
MIDNIGHT_CLOSE      sMidnightAlarm __attribute__ ((section ("cmos")));     //JY added, copied from COMT.
//Abacus Global Data Manager
//AGD_PCN_DATA          CMOS_AGDPcnData __attribute__ ((section ("cmos")));  //JY, get it back.
//AGD_INFO              CMOS_AGDInfo __attribute__ ((section ("cmos")));     //JY, get it back.

PWR_FAIL_REG_STORE      CMOSPwrFailRegStore __attribute__ ((section ("cmos")));

// Reserved - was used by power fail structure used by the CFFS file system
//PFAIL CFFSpf __attribute__ ((section ("cmos")));
char reserved000[536] __attribute__ ((section ("cmos")));

// Reserved - was used by power fail structure used by the internal flash file system
//INTPFAIL FFSpf __attribute__ ((section ("cmos")));
char reserved001[48] __attribute__ ((section ("cmos")));

//Data element table
DATA_ELEMENTS_FOR_UPLOAD_TO_DLS dataElementsForUploadToDls __attribute__ ((section ("cmos")));
unsigned char CMOSUpdatedToVersion __attribute__ ((section ("cmos")));

// GMT Time Sync NVM storage
unsigned long ulCMOSTimeSyncStatus __attribute__ ((section ("cmos")));
DATETIME dtCMOSTimeSyncLockoutEnd __attribute__ ((section ("cmos")));
char     marriedVaultNumber[16] __attribute__ ((section ("cmos")));

// File Update State Info
FILE_UPDATE_STATE CMOSFileUpdateStateInfo __attribute__ ((section ("cmos")));

/* Picklist file headers */
picklist_file_header picklist_file_headers[NUM_OF_CMOS_PICKLIST_HEADERS] __attribute__ ((section ("cmos"))); /* File Headers */

/* Used to be GTS Archive File Headers */
char reserved011[9684]  __attribute__ ((section ("cmos")));

/* Used to be special services */
char reserved012[40]  __attribute__ ((section ("cmos")));

//Germany IBI parameters
unsigned char CMOS_DpServiceId[8] __attribute__ ((section ("cmos")));    //Service ID delivered from DP by PbP through Derek.  Used in FF generation

// test parameters
long    lDcapTimestampFudge __attribute__ ((section ("cmos")));
unsigned short  wMinPrintMargin __attribute__ ((section ("cmos")));
unsigned short  wBuildTestMode __attribute__ ((section ("cmos")));
unsigned long USBPCLogIndex __attribute__ ((section ("cmos")));
USBPCLOGGING USBPCLog[MAX_USB_PC_LOGGING_ENTRIES] __attribute__ ((section ("cmos")));
unsigned long USBRegistrationIndex __attribute__ ((section ("cmos")));
unsigned long USBBootCycle __attribute__ ((section ("cmos")));
USB_ENUM_REGISTRATION_LIST USBEnumRegistrations[ MAX_TRACKED_USB_REGISTRATIONS ] __attribute__ ((section ("cmos")));


// Mode ajout
AJOUT_NV  CMOSAjout __attribute__ ((section ("cmos")));
UINT32  uiCMOSAjoutPieceCount __attribute__ ((section ("cmos")));

ASRLog CMOSASRLog __attribute__ ((section ("cmos")));

/* Country specific archive file registry (for request and download of
 * archive files */
CTY_ARCHIVE_PROPS PctyArchiveRegistry[TOTAL_CTY_ARCHIVES] __attribute__ ((section ("cmos")));

/* Control bits and saved graphics Gid for new sw/rates/graphics download */
CMOSDownloadCtrlBuf CmosDownloadCtrlBuf __attribute__ ((section ("cmos")));
unsigned char fCMOSShowRateActive __attribute__ ((section ("cmos")));
unsigned char fCMOSShowRateEffective __attribute__ ((section ("cmos")));

BOOL fCMOSUserDenyRateActivePrompt __attribute__ ((section ("cmos")));
BOOL fCMOSUserDenyRateEffectivePrompt __attribute__ ((section ("cmos")));

BOOL fCMOSDateAdvUseNewRates __attribute__ ((section ("cmos")));

// Used for the LAN Firewall Settings
BOOL bCMOSLANFirewallPingEnabled __attribute__ ((section ("cmos")));

// Used for the DH On time
UINT32 CMOSLongDHOnTime __attribute__ ((section ("cmos")));

// WebVis log:
tBMWVDataUploadLog CMOSWebVisDataUploadLog[BMWV_MAX_DATA_UPLOAD_LOG] __attribute__ ((section ("cmos")));

// Used for Graphics cleanup.  Not customer settable.
unsigned short  uwCMOSGFNeedsCmpxn __attribute__ ((section ("cmos")));

// Used in countries that require a forced upload on date change.
DATETIME CMOSLastPrintedMailDate __attribute__ ((section ("cmos")));

// Used for Battery life
DATETIME    CMOSLastBatteryDisplay __attribute__ ((section ("cmos")));
DATETIME    CMOSPsdBatteryTestDate __attribute__ ((section ("cmos")));

// contains information needed when a country's currency changes to the Euro
EuroConversion cmosEuroConversion __attribute__ ((section ("cmos")));

BOOL fCMOSInfraUpdateReq __attribute__ ((section ("cmos")));

//Merged from janus for Slovakia Town Name
EXP_MTR_CMOS_DATA exp_mtr_cmos __attribute__ ((section ("cmos")));

// Variables to track if we missed midnight processing (either local midnight or DCAP midnight)
// Added to track if we are missing midnight processing based on the DTC submission issue - 
// meter did not lock out after grace days expired. Even though we could not reproduce the 
// issue seen at DTC - added this code b/c we assumed we missed a midnight processing.
DATETIME CMOSLastLocalMidnightDT __attribute__ ((section ("cmos")));       // contains either the last local midnight DT or
                                        // the DT of the last powerup - whichever happened last
DATETIME CMOSLastDCAPPeriodCheckDT __attribute__ ((section ("cmos")));     // contains the last DCAP period check DT
                                        // may be midnight (or a few seconds after) OR 
                                        // some other time other than midnight if the period check 
                                        // was last run during powerup

UNICHAR CMOSRefillCode[STD_PWD_LEN + 1] __attribute__ ((section ("cmos")));

// Indicates if platform message filtering is turned on
BOOL CMOSPlatformFilteringOn __attribute__ ((section ("cmos")));

BOOL fCMOSAbacusAcntProblem __attribute__ ((section ("cmos")));

//TODO - remove these unused two data items
unsigned short usCMOSMaxSmallParitions __attribute__ ((section ("cmos")));
unsigned short usCMOSMaxLargeParitions __attribute__ ((section ("cmos")));

unsigned char	ucCMOSAbacusAcntErrorCode __attribute__ ((section ("cmos")));

// Add the flag for expired rates.
BOOL CMOSRatesExpired __attribute__ ((section ("cmos")));


NetworkInformationStruct	CMOSglobalNetworkInformationStructure  __attribute__ ((section ("cmos")));

ulong			  CMOSTrmInitialzed1								__attribute__ ((section ("cmos")));
ulong			  CMOSTrmMailPieceID								__attribute__ ((section ("cmos")));
trm_common_t   	  CMOSTrmCommon										__attribute__ ((section ("cmos")));
trm_mem_t         CMOSTrmArea 										__attribute__ ((section ("cmos")));
trm_mem_t         CMOSTrmArea2 										__attribute__ ((section ("cmos")));
ulong			  CMOSTrmActiveBucket								__attribute__ ((section ("cmos")));

ulong			  CMOSTrmFailedWrites									__attribute__ ((section ("cmos")));
ulong			  CMOSTrmFailedReads									__attribute__ ((section ("cmos")));
ulong			  CMOSTrmFailedDeletes									__attribute__ ((section ("cmos")));
ulong			  CMOSTrmZeroLengthFiles								__attribute__ ((section ("cmos")));
ulong			  CMOSTrmZeroLenDeletFails								__attribute__ ((section ("cmos")));
char			  Reserved10[1776]									__attribute__ ((section ("cmos")));
pkg_info_t   	  CMOSTrmPkgInfoPrev								__attribute__ ((section ("cmos")));
pkg_info_t   	  CMOSTrmPkgInfoCur									__attribute__ ((section ("cmos")));
trm_psd_info	  CMOSTrmPSDInfo									__attribute__ ((section ("cmos")));
ushort			  CMOSTrmDcapDeviceID								__attribute__ ((section ("cmos")));
ulong			  CMOSTrmFlashFileSeqCount							__attribute__ ((section ("cmos")));
ulong			  CMOSTrmVersion									__attribute__ ((section ("cmos")));
ulong			  CMOSTRMState										__attribute__ ((section ("cmos")));
ulong			  CMOSTrmUploadState								__attribute__ ((section ("cmos")));
ulong			  CMOSTrmDcapFileSeqCount							__attribute__ ((section ("cmos")));
uchar 			  CMOSDcapGuid[GUID_SIZE]  							__attribute__ ((section ("cmos")));
DATETIME		  CMOSTrmPrevUploadDate								__attribute__ ((section ("cmos")));
ulong 			  CMOSTrmUploadDueReqd								__attribute__ ((section ("cmos")));
ulong			  CMOSTrmMidnightFlag								__attribute__ ((section ("cmos")));
ulong			  CMOSTrmMaxFilesToUpload							__attribute__ ((section ("cmos")));
DATETIME		  CMOSTrmTimeStampOfOldestMP						__attribute__ ((section ("cmos")));
DATETIME		  CMOSTrmTimeStampOfMPIDReset						__attribute__ ((section ("cmos")));
ulong			  CMOSTrmMailPiecesToUpload							__attribute__ ((section ("cmos")));
ulong			  CMOSTrmPendingBucketToUpload						__attribute__ ((section ("cmos")));
ulong 			  CMOSTrmFatalError			   						__attribute__ ((section ("cmos")));
ulong 			  CMOSTrmTRCFilesToBeRemoved	   					__attribute__ ((section ("cmos")));
ulong 			  CMOSTrmMailPiecesPackaged	 						__attribute__ ((section ("cmos")));
ulong 			  Reserved2[31]				 						__attribute__ ((section ("cmos")));
ulong			  CMOSTrmInitialzed2								__attribute__ ((section ("cmos")));
// IMPORTANT NOTE - Do not append anything to this section!  Either replace a reserved field keeping size the same or add to the cmos2 section below!


// IMPORTANT NOTE - Add new "cmos" fields to this section like comment below
//ulong 			  MyNewField				   						__attribute__ ((section ("cmos2")));
ulong 			CMOS2TrmInitialzed1								__attribute__ ((section ("cmos2")));
char 			CMOS2TrmDcapFileName[64]						__attribute__ ((section ("cmos2")));
char 			CMOS2TrmDcapFileToBeUploaded					__attribute__ ((section ("cmos2")));
trm_file_list_t_ex	CMOSTrmFileList								__attribute__ ((section ("cmos2")));
ulong 			CMOS2TrmInitialzed2								__attribute__ ((section ("cmos2")));
ulong 			CMOS2TaskListLogFileIdx							__attribute__ ((section ("cmos2")));
tRefillData     CMOS2LastRefillData                             __attribute__ ((section ("cmos2")));
tRefundData     CMOS2RefundData                                 __attribute__ ((section ("cmos2")));
trm_ext_deb_cer_t CMOSExtTrmDebitCerts                          __attribute__ ((section ("cmos2")));
