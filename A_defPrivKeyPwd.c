/*************************************************************************
*   PROJECT:        Horizon CSD2-3
*	MODULE NAME:	A_defPrivKeyPwd.c
*
*	DESCRIPTION:	This file contains the obfuscated default private key password.
*
* ----------------------------------------------------------------------
*               Copyright (c) 2017 Pitney Bowes Inc.
*                    37 Executive drive
*                   Danbury, Connecticut  06810
* ----------------------------------------------------------------------
*
*************************************************************************/

const unsigned char defPrivKeyRand4[] = {0xDC, 0x01, 0xAB, 0xCB, 0x9C, 0x8A, 0x92, 0x2E};
const unsigned char defPrivKeyPwd2[] = {0x1C, 0xD9, 0xB7, 0xAB, 0x7E, 0x59, 0x84, 0x0D};
