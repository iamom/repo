/************************************************************************
    PROJECT:        Future Phoenix
    COPYRIGHT:      2005, Pitney Bowes, Inc.
    AUTHOR:         
    MODULE NAME:    OiPostage.c

    DESCRIPTION:    This file contains all types of screen interface functions 
                    (hard key, event, pre, post, field init and field refresh)
                    and global variables pertaining to postage entry and
                    validation, including high value & low value warnings.
//-----------------------------------------------------------------------------
 MODIFICATION HISTORY:
 
 22-May-17 Jennifer Hao on FPHX 02.12 cienet branch
      Commented out functions  fnfMinManifestPostage,fnhkPrintPostageCorrection, fnhkCheckManifestAmount,  fnfConfirmManifestPostageValue because they aren't used by G9.

 01-Oct-13 sa002pe on FPHx 02.10 shelton branch
	Merging in changes from Janus:
----*
	* 29-Sep-14 sa002pe on Janus tips shelton branch
	*	For Fraca 227335: Changed fnhkValidateHighValueWarnAmt to not allow the High Postage Warning Value to be set to zero.
----*

 21-May-13 Kevin Brown on FPHx 02.10 shelton branch
 	Removed the function prototype fnKeypadWaitKey().

 22-Feb-13 sa002pe on FPHx 02.10 shelton branch
 	Merging in changes from the 01.10 branch for Brazil that didn't get merged back to the main branch
----
	  2009.13.08  Clarisa Bellamy   Lint it. 
	                                Change default value in function fnpEnterAmountTapePre.
	  2009.05.08  Clarisa Bellamy   1. Add new screen entry function, fnpEnterAmountTapePre,
	                                called upon entry to the MMEnterPostage screen.  This
	                                function checks the current postage to see if tape 
	                                entry should be allowed at this time.
	                                2. Modify the functions fnfPostageTapeAmtPrompt so
	                                that the prompt to enter Tape Qty does not appear
	                                when the current postage value is invalid. 
	                                3. Modify the function fnfSetPostagePrompt so that 
	                                the prompt to save the postage value appears only 
	                                when entering a postage value is allowed and the 
	                                value entered meets some very basic criteria.
	                                4. Modify the function fnfSetTapesPrompt so that 
	                                the prompt to hit the TAPE button appears only
	                                when the current postage value is valid, and the 
	                                value entered is a valid tape quantity.
----

  2012.10.03 Clarisa Bellamy  FPHX 02.10 
  - Added call to fnClearEntryBuffer to fnhkCheckEngrServiceModeCodes function, 
    after the service mode password is verified.  Some later Service screens use 
    the function fnhkBufBlankOrChangeScr when the clear key is pressed, even 
    though they do not have an input field.  This meant that the clear key had
    to be pressed twice to exit the screen, for no apparent reason.  Really, 
    those screens should be corrected to use a different function (perhaps
    fnhkGoPreviousScreen?), but this is faster. 
     
  2011.08.09 Clarisa Bellamy    Modified fnhkValidatePostageCorrectionAmount to fix fraca 207174.
                                Modified SetUpKIPPiece() just for cleanup.
  2011.05.17      Joe Zhang   fix fraca GMSE00180584: per Clarisa, do not apply fencing limits to High-value warning setting.
  2011.04.28      Joe Zhang   Modify fnhkValidateHighValueWarnAmt to turn the buffer contents into a double. 
                            This fixed fraca GMSE00180585.
  05/14/2010    Jingwei,LI      Modified function fnfPostageValue() to trim the redundant sopaces when
                                get table text for RTL.
  05/10/2010    Deborah Kohl    Modified all functions testing (bFieldCtrl & ALIGN_MASK) 
                                to add padding characters either when right aligned and
                                in LTR mode or left aligned and in RTL mode
  12/11/2009    Raymond Shen    Modified fnhkBufferKeySmartPostage() to do not accept decimal point for France KIP mode, thus fixed
                                GMSE00175646(G970/DM475 - The decimal key should be inactive in KIP or Enter Tape Qty screen
                                per the "UISPEC_Rel 8.x.pdf".).
    05/07/2009  Jiang, Yan      Initial the array for getting the string in function fnfPostageValue(). 
    10/24/2008  Raymond Shen    Added code to support Brazil KIP Indicia HRT selection.
  2008.09.05  Joe Qu            Modified function fnhkUseHighValuePostage() and SetUpKIPPiece() to add call to 
                                fnRateRefreshIndiciaInfo() to refresh Indicia info after postage is written 
                                into PSD in KIP mode.
  2008.03.24  Clarisa Bellamy   Fix problem where you can't enter more than 3 digits on the Enter Postage 
                                screen for Brazil.  Check the min displayed decimals, because that is what
                                is used to "automatically" put the decimal point in upon the ENTER key.
                                Make sure user can enter enough digits, without decimal point, to enter 
                                the service/engineering mode passwords.
    29 Nov 2007 Martin O'Brien  Included oiweight.h for prototype of fnIsZeroPrintByKIPwPasswordSupported()
                                fnhkSetZeroPostageValue(): Changed it recognises French-style KIP as well as Swiss-style
    24 Oct 2007 P.Vrillaud      Modified SetUpKIPPiece(), remove printmode test (for Switzerland)
    17 Oct 2007 Deborah Kohl    Modified SetUpKIPPiece() to prevent KIP selection outside
                                fee slection (for Switzerland)
    08/21/2007  Adam Liu        Modified fnhkPCNCheckForEnterAmount() to allow zero postage KIP mode on 
                                Canadian large meter, fix fraca GMSE00127939.
    08/20/2007  Oscar Wang      Fixed FRACA GMSE00127740, remove "High value warning" from Setup menu.
    07/19/2007  Vincent Yi      Fixed lint errors
    06/27/2007  Raymond Shen    Modified fnhkPCNCheckForEnterAmount() to forbid KIP mode on Canadian large meter.
    21 Jun 2007 Simon Fox       Modified fnhkSetZeroPostageValue so it doesn't
                                rely on entry-buffer functions (the screen may
                                not have an entry buffer).
    06/19/2007  Bill Herring    New function fnhkSetZeroPostageValue() to support
                                zero print menu option.
    06/13/2007  Raymond Shen    Modified fnhkValidateHighValueWarnAmt() to check fencing limit.
    05/18/2007  Adam Liu        Modified fnhkBufferKeySmartPostage() to fix fraca GMSE00116405.     
    04/28/2007  Andy Mo         Modified fnhkPCNCheckForEnterAmount(),fnfSetPostagePrompt() amd
                                fnfPostageTapeAmtPrompt() and fnfSetTapesPrompt() to support Canada
                                KIP mode.
    04/27/2007  Andy Mo         Fixed the issue that input buffer containing invalid postage amount  couldn't be cleared correctly.
    04/26/2007  Andy Mo         Added fnhkManifestAmount() for MMManifestEnterAmount
    04/20/2007  Andy Mo         Modified fnhkValidatePostageCorrectionAmount().
    04/19/2007  Andy Mo         Added fnhkValidatePostageCorrectionAmount() for MMPostageCorrectionEnterAmount
    04/18/2007  Vincent Yi      Modified fnhkPCNCheckForEnterAmount() to handle
                                KIP_NOT_ALLOWED case
*
* 17-Apr-07 sa002pe on FPHX 01.04 shelton branch
*   Added dbTempManifestPostage.
*   Added fnCheckInsufficientFundsPostage, fnfMinManifestPostage,
*   fnhkPrintPostageCorrection, fnhkCheckManifestAmount & fnfConfirmManifestPostageValue.
*
    29 Mar 2007 Bill Herring    Modified fnfDisplayHighPostage(),
                                fnhkSetupHighPostage(), and fnfHighValueWarningAmt()
                                for optional HV in (France)
    26 Mar 2007 I. Le Goff      Modify fnfPostageValue() for mode ajout: display a zero postage value
                                if not all input have been entered.
    23 Mar 2007 Bill Herring    Modified functions fnfSetPostagePrompt(),
                                fnfSetTapesPrompt(), fnhkPCNCheckForEnterAmount(),
                                fnfPostageTapeAmtPrompt() to support the MMEnterPostage
                                screen for kip mode = KIP_ZERO_ONLY_W_PASSWORD.
                                Modified kip batch count functions to accept either
                                KIP_ALLOWED_PASSWORD or KIP_ZERO_ONLY_W_PASSWORD.
    03/16/2007  Vincent Yi      Added two functions fnfSetPostagePrompt()
                                fnfSetTapesPrompt()
    02/08/2007  Kan Jiang       Modified function fnhkBufferKeySmartPostage() for 
                                fixing fraca 114092.
    12/15/2006  Oscar Wang      Modified function fnhkPCNCheckForEnterAmount().
    12/12/2006  Oscar Wang      Support KIP class and KIP password.
                                Added functions fnKipPasswordRequired(), 
                                fnfKIPEnteredPostageAmt(), 
                                fnhkRejectHighValuePostage().
                                Modified functions fnhkPCNCheckForEnterAmount(),
                                fnhkUseHighValuePostage(). 
    11/15/2006  Vincent Yi      Fixed fraca 106327 106330,
                                Removed BR functions which destroy the correct logic
                                when setting High postage warning value and Low funds
                                warning value.
                                Added print mode check in fnhkValidateHighValueWarnAmt()
    08/17/2006  Vincent Yi      Fixed bugs in fnhkValidateHighValueWarnAmt and
                                fnhkValidateLowFundsWarnAmt
    07/03/2006  Raymond Shen    Added function fnevValidateEdmPostage().
    05/11/2006  Steve Terebesi  Updated to call fnGetSysMode instead of OI mode

*   03/21/2006  Raymond Shen
*   Added fnhkCheckEngrServiceModeCodes() to enter the Service Mode. 
*

    OLD PVCS REVISION HISTORY
 * \main\jnus11.00_i1_shelton\2 cx17598 18 oct 2005
 *
 *  Replaced the call fnhkModeSetManual using fnModeSetDefault.
 *
 * 08-Sep-05 sa002pe
 * Merging in changes from 10.12 build for Italy:

*************************************************************************/


/**********************************************************************
        Include Header Files
**********************************************************************/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "bob.h"
#include "bobutils.h"
#include "cwrapper.h"
#include "datdict.h"
#include "errcode.h"
#include "fwrapper.h"
#include "global.h"
#include "keypad.h"
#include "oicommon.h"
#include "oientry.h"
#include "oipostag.h"
#include "oit.h"
#include "oitpriv.h"
#include "nucleus.h"
#include "tealeavs.h"
#include "ajout.h"
//#include "oiRateService.h"
#include "oiscrnfp.h"
#include "oifpprint.h"
#include "unistring.h"
#include "oierrhnd.h"
#include "oiscreen.h"
#include "sys.h"
//#include "oiweight.h"
#include "flashutil.h"
#include "settingsmgr.h"
#include "utils.h"


#define COLOR_LCD_BACKDOOR_ENABLED  1   // this allows color changes using special codes on the enter postage screen


/**********************************************************************
        Local Function Prototypes
**********************************************************************/

static UINT8 fnUnicodeAddPostageSymbol(UNICHAR *    pUnicode, 
                                       UINT8        bInputLen, 
                                       UNICHAR      usSymbol);
static T_SCREEN_ID SetUpKIPPiece(double dblAmtEntered, BOOL *bClearBuffer,
                                 UINT8  bNumNext, 
                                 T_SCREEN_ID *pNextScreens);

/**********************************************************************
        Local Defines, Typedefs, and Variables
**********************************************************************/

#define MAX_DESC_REG_VAL    4294967295.0


double          dblHighPostageProposal;
//static double           dblLowFundsPostageProposal;
static unsigned long lwKipBatchCount = 0;  
double dbTempManifestPostage = 0;

#define USTR_LEN        10 //temp unistring length

/**********************************************************************
        Global Functions Prototypes
**********************************************************************/
//extern BOOL fnIsZeroPrintByKIPSupported(void);
extern void fnSetAlterNativeMode(unsigned char  bPrintMode);
extern unsigned char fnCheckPostCorrectionVal(double dblAmtEntered);

/**********************************************************************
        Global Variables
**********************************************************************/

extern KIPPWInfo            CMOSKIPPaswordInfo;
extern BOOL fKIPLastScreenClass;
extern ENTRY_BUFFER     rGenPurposeEbuf;

/* static variables */

/* declarations */
unsigned long           lwOldBobPostage = 0;
BOOL                fHighPostagePending = FALSE;

BOOL                fLowFundsWarningPending = FALSE;
BOOL                fLowFundsWarningDisplayed = FALSE;
BOOL                fLowPieceCountWarningPending = FALSE;
BOOL                fLowPieceCountWarningDisplayed = FALSE;

BOOL                fKIPModePending = FALSE;


KIPPWInfo*                  pKIPPasswordInfo;

// This is used to save the status of the "current postage value" when we enter
//  the MMEnterPostage screen.  If the current postage value is not OK, do NOT 
//  prompt to entry Tape Quantity on that screen. 
static UINT8    bEnterAmt_CurrPVIsLegal = POSTAGE_OK;

/**********************************************************************
        Public Functions
**********************************************************************/
/* Utility functions */
/* *************************************************************************
// FUNCTION NAME: fnCheckInsufficientFundsPostage
// DESCRIPTION: Utility function (public within OI) that checks if a postage
//              value in insufficient,
//
// AUTHOR: Tom Zhao
//
// INPUTS:   dblPostage - the postage value being checked
// RETURNS: TRUE if the postage is subject to the high value warning
//          FALSE if the postage is not subject to the high value warning
// *************************************************************************/
BOOL fnCheckInsufficientFundsPostage(double dblPostage)
{
    BOOL    fRetval = FALSE;
    unsigned char   pDR[SPARK_MONEY_SIZE];

    if (fnValidData(BOBAMAT0, GET_VLT_DESCENDING_REG, (void *) pDR) != BOB_OK)
    {
        fnProcessSCMError();
        goto xit;
    }       

    if (dblPostage > fnBinFive2Double(pDR))
    {
        fRetval = TRUE;
    }
xit:
    return (fRetval);
}

/* *************************************************************************
// FUNCTION NAME: fnCheckLegalPostage
// DESCRIPTION: Utility function that checks if a postage value complies with
//              PCN & vault configuration.  DOES NOT check for insufficient funds.
//
// AUTHOR: Joe Mozdzer//
// INPUTS:  dblPostage - postage value being tested
//          pResult - pointer to location where the result of
//                      the check is returned
//
// RETURNS: SUCCESSFUL if the function was able to check the postage.  In
//          this case, *pResult will contain one of the following:
//              POSTAGE_OK
//              FIXED_ZERO_VIOLATION
//              BANK_RESTR_VIOLATION
//              MAX_SETTABLE_VIOLATION
//
//          FAILURE is returned if one of the PCN or vault parameters needed
//          to perform the check were invalid.  A meter error is automatically
//          reported.  In this case, the contents of *pResult should not
//          be considered valid.
//              
// *************************************************************************/
BOOL fnCheckLegalPostage(double dblPostage, unsigned char *pResult)
{
    BOOL            fRetval = FAILURE;
    
    unsigned long   i;
    unsigned long   lwMaxSettable;
    unsigned long   lwMinSettable;
    unsigned char   bFixedZeroes = 0;
    unsigned char   bNumSettable;
    unsigned long   lwDivisor;


    /* get the number of fixed zeroes and settable digits */
    if ( (fnValidData(BOBAMAT0, VLT_PURSE_FIXED_ZEROS, (void *) &bFixedZeroes) != BOB_OK) ||
         (fnValidData(BOBAMAT0, VLT_PURSE_SETTABLE_DIGITS, (void *) &bNumSettable) != BOB_OK) )
    {
        fnProcessSCMError();
        goto xit;
    }

    /* the number of fixed zeroes can't be more than 3 and the number
        of settable digits can't be more than 7 */
    if ((bFixedZeroes > 3) || (bNumSettable > 7))
    {
        fnReportMeterError(ERROR_CLASS_OIT, ECOIT_ILLEGAL_VAULT_PARM_VALUE);
        goto xit;
    }


    /* calculate 10 to the *pFixedZeroes power */
    lwDivisor = 1;
    for (i = 0; i < bFixedZeroes; i++)
        lwDivisor = lwDivisor * 10;
    
    /* check if the amount entered violates the number of required fixed zeroes */
    if (fmod(dblPostage, (double) lwDivisor) != 0.0)
    {
        *pResult = FIXED_ZERO_VIOLATION;
        fRetval = SUCCESSFUL;
        goto xit;
    }

    /* check for compliance with bank restrictions */   
    switch (fnFlashGetByteParm(BP_BANK_RESTRICTIONS))
    {
        case BANK_RESTR_NONE:
            break;
        case BANK_RESTR_1_ZERO:
            if (fmod(dblPostage, (double) lwDivisor * 10) != 0.0)
            {
                *pResult = BANK_RESTR_VIOLATION;
                fRetval = SUCCESSFUL;
                goto xit;
            }
            break;
        case BANK_RESTR_1_HALF:
        {
            double      dblLastDigit;

            dblLastDigit = fmod(dblPostage, (double) lwDivisor * 10);
            if ((dblLastDigit != 0.0) && (dblLastDigit != (5.0 * lwDivisor)))
            {
                *pResult = BANK_RESTR_VIOLATION;
                fRetval = SUCCESSFUL;
                goto xit;
            }
            break;
        }
        case BANK_RESTR_2_HALF:
        {
            double      dblLastDigits;

            dblLastDigits = fmod(dblPostage, (double) lwDivisor * 100);
            if ((dblLastDigits != 0.0) && (dblLastDigits != (50.0 * lwDivisor)))
            {
                *pResult = BANK_RESTR_VIOLATION;
                fRetval = SUCCESSFUL;
                goto xit;
            }
            break;
        }
        case BANK_RESTR_2_QUARTER:
        {
            double      dblLastDigits;

            dblLastDigits = fmod(dblPostage, (double) lwDivisor * 100);
            if ((dblLastDigits != 0.0) && (dblLastDigits != (25.0 * lwDivisor)) &&
                (dblLastDigits != (50.0 * lwDivisor)) && (dblLastDigits != (75.0 * lwDivisor)))
            {
                *pResult = BANK_RESTR_VIOLATION;
                fRetval = SUCCESSFUL;
                goto xit;
            }
            break;
        }
        default:   /* the bank restriction parameter must be illegal */
            fnReportMeterError(ERROR_CLASS_OIT, ECOIT_ILLEGAL_PCN_PARM_VALUE);
            goto xit;
    }

    /* check if value exceeds max settable postage */
    if ( (fnValidData(BOBAMAT0, VLT_DEBOPTS_FILE_MAX_DEB_VAL, (void *) &lwMaxSettable) == BOB_OK) &&
         (fnValidData(BOBAMAT0, VLT_DEBOPTS_FILE_MIN_DEB_VAL, (void *) &lwMinSettable) == BOB_OK) )
    {
        double  dblBankMax;

        /* the maximum settable postage is the lesser of 1) the maximum debit value stored
            in the vault and 2) the maximum value that can fit in the number of postage banks
            that the meter is configured for. */

        /* first calculate the max that can fit in the number of banks we have.  this is 
            equal to 10 to the (number of settable banks + number of fixed zeroes) power
            - 1.  e.g. 3 settable + 1 fixed gives 4 banks with max = 9999 (or 10000 - 1).
            (the actual max would be 9990 but the fixed zeroes are enforced above so
            we don't need to make that adjustment here) */
        dblBankMax = 1.0;
        for (i = 0; i < (bNumSettable + bFixedZeroes); i++)
            dblBankMax = dblBankMax * 10.0;
        dblBankMax -= 1.0;

        /* do the max settable comparison */
        if ((dblPostage > (double) lwMaxSettable) || (dblPostage > dblBankMax))
        {
            *pResult = MAX_SETTABLE_VIOLATION;
            fRetval = SUCCESSFUL;
            goto xit;
        }
        
        /* do the min settable comparison */
        if (dblPostage < (double) lwMinSettable) 
        {
            *pResult = MIN_SETTABLE_VIOLATION;
            fRetval = SUCCESSFUL;
            goto xit;   
        }
    }
    else
    {
        fnProcessSCMError();
        goto xit;
    }

    *pResult = POSTAGE_OK;
    fRetval = SUCCESSFUL;

xit:
    return (fRetval);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckHighPostage
//
// DESCRIPTION: 
//      Utility function (public within OI) that checks if a postage value needs 
//      to be subjected to the high value warning.  If it does, the pending 
//      value is stored in a global container and a flag is set indicating that 
//      confirmation is required.
//
// INPUTS:
//      dblPostage - the postage value being checked
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      TRUE if the postage is subject to the high value warning
//      FALSE if the postage is not subject to the high value warning
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Joe Mozdzer     Initial version
//  11/16/2005  Vincent Yi      Code cleanup
//
// *************************************************************************/
BOOL fnCheckHighPostage(double dblPostage)
{
    BOOL    fRetval;

    // set the global postage amount
    dblHighPostageProposal = dblPostage;

    if (dblPostage >= (double) (fnCMOSSetupGetCMOSSetupParams()
                                            ->lwHighValueWarning))
    {
        fHighPostagePending = TRUE;
        fRetval = TRUE;
    }
    else
    {
        fHighPostagePending = FALSE;
        fRetval = FALSE;
    }

    return (fRetval);
}

/******************************************************************************
   FUNCTION NAME: fnCheckManifestAmount
   DESCRIPTION  : utility function that is called to check if the postage is valid
   PARAMETERS   : None
   RETURN       : TRUE - If the amount is valid
                  FALSE - IF the amount is invalid
   NOTES        :     
   04/23/2007   Andy Mo     Initial version
******************************************************************************/
BOOL fnCheckManifestAmount(UINT32 dblAmount)
{
    BOOL bRev=TRUE;
    UINT32  lwMaxSettable,lwMinSettable;    
    //get max and min settable value first
    if ( !(fnValidData(BOBAMAT0, VLT_DEBOPTS_FILE_MAX_DEB_VAL, (void *) &lwMaxSettable) == BOB_OK)  )
    {
        fnProcessSCMError();
        bRev = FALSE;
        return(bRev);
    }
/*
    if (fnFlashGetByteParm(BP_MAILING_MANIFEST_MODE) == MANIFEST_ALLOWED_MIN_KIP_LIMIT)
        lwMinSettable = (UINT32)fnDoubleAdjustedMoney2Double( fnRateGetMinKIPAmtValue() );
    else
        lwMinSettable = (UINT32)(fnBinFive2Double(fnFlashGetMoneyParm(MP_MANIFEST_MIN_SETTABLE_POSTAGE )));
*/
    if( (dblAmount > lwMaxSettable) || (dblAmount <lwMinSettable))//Amount is valid
        bRev = FALSE;
    return(bRev);
}
/******************************************************************************
   FUNCTION NAME: fnInitKIPPasswordPointer
   AUTHOR       : Yingbo Zhang
   DESCRIPTION  : Initialize the Key In postage password structures after power up
   PARAMETERS   : None
   RETURN       : None
   NOTES        : Yingbo-ff2
******************************************************************************/
void fnInitKIPPasswordPointer(void)
{
    //Password info initialization
    pKIPPasswordInfo= &CMOSKIPPaswordInfo;

}

/* *************************************************************************
// FUNCTION NAME: 
//      fnKipPasswordRequired
//
// DESCRIPTION: 
//      Utility function that checks if KIP password is required
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      TRUE if KIP password is required.
//      FALSE if KIP password is not required.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/06/2006  Oscar Wang      Code cleanup
//                              Initial version
//
// *************************************************************************/
BOOL  fnKipPasswordRequired( void )
{
    BOOL bReval = FALSE ;
    UINT8 bKipMode = fnGetKIPMode();
    
/*
    if( ((bKipMode == KIP_ALLOWED_PASSWORD) || (bKipMode == KIP_ZERO_ONLY_W_PASSWORD)) 
        &&(fnRateIsCurrentCarrierBranded() == RSSTS_CARRIER_KIP_CARRIER) )
    {
        if ( PMODE_MANUAL == fnOITGetPrintMode() )
        {
            switch ( pKIPPasswordInfo->ucPasswordStatus )
            {

                case KIP_PW_FROM_PRESET :
                case KIP_PW_BATCH_LIMIT :
                case KIP_PW_RESET :
                    bReval = TRUE ;
                    break;
                        
                case KIP_PW_COMPLETE :
                case KIP_PW_FORCE_TO_EXIT :
                    bReval = FALSE ;
                    break;
                default:
                    bReval = FALSE ;
                    break;
                       
            }
        }
    }              
*/
    return ( bReval ) ;
}

/* *************************************************************************
// FUNCTION NAME: fnGetMeterRefillType
// DESCRIPTION: Utility function, return the meter refill type, 
//     1 = Postage Value Download type refill ?funds added to Descending Register via signed message 
//     2 = Keypad type refill ?funds added to Descending Register via keyed-in combination 
//     3 = Credit Limit ?credit limit set by setting the Descending Register to value in signed message 
//     0  =  Bob Error
// AUTHOR: David
//
// *************************************************************************/
                                     
unsigned char fnGetMeterRefillType ( void )
{
    unsigned char bMeterRefillType = 0 ;

    /* get the variable value from the Bob task */
    if ( fnValidData(BOBAMAT0, BOBID_IPSD_REFILLTYPE, (void * ) &bMeterRefillType) != BOB_OK)
    {
        fnProcessSCMError();
        goto xit;
    }

xit:
    return ( bMeterRefillType ) ;
}

/* *************************************************************************
// FUNCTION NAME: fnResetKipCount
// DESCRIPTION: Reset the KIP count, called after entering KIP password
//
// AUTHOR: Howell Sun
//
// INPUTS: None
// 
// RETURNS:  KIP Postage count
//
// *************************************************************************/
void fnResetKipCount(void)
{
    UINT8 bKipMode = fnGetKIPMode();
    
    if( ((bKipMode == KIP_ALLOWED_PASSWORD) || (bKipMode == KIP_ZERO_ONLY_W_PASSWORD))/*&&(fnOITGetCometPrintMode() == PMODE_MANUAL)*/)
        lwKipBatchCount = 0;
} // fnResetKipCount


/* *************************************************************************
// FUNCTION NAME: fnKipPasswdCountReached
// DESCRIPTION: Check the KIP count to see if it reaches KIP password count
//
// AUTHOR: Howell Sun
//
// INPUTS: None
// 
//         Use globe variable usKIPBatchCnt, a EMD Parameter.
//
// RETURNS:  TRUE       KIP password count (EMD parameter) reached
//           FALSE      KIP password count not reached
//
// *************************************************************************/
BOOL fnKipPasswdCountReached(void)
{
//  unsigned long   lwPostageVal;
    unsigned short usKIPBatchCnt;
    BOOL retVal = FALSE;
    
    usKIPBatchCnt = fnFlashGetWordParm(WP_PASSWORD_ENTRY_INTERVAL);
    
//  if (fnValidData(BOBAMAT0, VAULT_DEBIT_VALUE, (void *) &lwPostageVal) != BOB_OK)
//  {
//      fnProcessSCMError();
//      goto xit;
//  }   

    if( lwKipBatchCount >= usKIPBatchCnt /*&& lwPostageVal>0*/)
    {
        UINT8 bKipMode = fnGetKIPMode();

        // Check these conditions only when KIP count is above limit
              
        if ( ((bKipMode == KIP_ALLOWED_PASSWORD) || (bKipMode == KIP_ZERO_ONLY_W_PASSWORD))
             && (fnOITGetCometPrintMode() == PMODE_MANUAL) )
             retVal = TRUE;
    }
xit:
    return retVal;

} // fnKipPasswdCountReached()


/* *************************************************************************
// FUNCTION NAME: fnIncrementKipCount
// DESCRIPTION: Increment KIP count, called by Bob
//
// AUTHOR: Howell Sun
//
// INPUTS: None
// 
// RETURNS:  None
//
// *************************************************************************/
void fnIncrementKipCount(void)
{
    UINT8 bKipMode = fnGetKIPMode();

    if ( ((bKipMode == KIP_ALLOWED_PASSWORD) || (bKipMode == KIP_ZERO_ONLY_W_PASSWORD))
         && (fnOITGetCometPrintMode() == PMODE_MANUAL) )
        {
            lwKipBatchCount++;
        }
} // fnIncrementKipCount()


/* Pre/post functions */


// ***************************************************************************
// FUNCTION NAME:   fnpEnterAmountTapePre
// PURPOSE: 
//     Called on entry to the MMEnterPostage screen.  The screen is entered 
//      from the main screen when the user presses a digit key.  It can be
//      used to enter postage, or to entry a tape quantity.
//     If the postage that is current when the screen is first entered is 
//      not legal, then we don't want to prompt to enter a tape quantity.
//     Here, we check the current postage to see if it is legal, and set a
//      file-static variable, so that the output field functions can 
//      determine what to display.   
//               
// INPUTS:      
//      Standard Screen Entry function inputs.
// RETURNS:     
//      PREPOST_COMPLETE - always.
// OUTPUTS:     
//      Sets the bEnterAmt_CurrPVIsLegal to: 
//                              0xFF    if the this function problems reading parameters from BOB (or vault)
//                      POSTAGE_OK(0)   if the current postage is a valid
//              other non-zero value    if the postage is invalid. 
//
// NOTES:
//   1. Non-zero values are defined in oipostage.h and here are some examples:
//              FIXED_ZERO_VIOLATION    
//              BANK_RESTR_VIOLATION    
//            MAX_SETTABLE_VIOLATION    
//            MIN_SETTABLE_VIOLATION    
//              POSTAGE_OUT_OF_RANGE    
//
// HISTORY:
//  2009.05.07 Created by Clarisa Bellamy
//---------------------------------------------------------------------------
SINT8 fnpEnterAmountTapePre( void (** pContinuationFcn)(), 
                             UINT32      *pMsgsAwaited,
                             T_SCREEN_ID *pNextScr )
{
    double  dblCurrPostage = 0;
    UINT32  ulCurrentPostageValue = 0;


    // Default to bad postage, in case fnCheckLegalPostage fails.
    bEnterAmt_CurrPVIsLegal = 0xFF;  
	
    // Get the postage on entry to the screen.
    if( fnValidData(BOBAMAT0, VAULT_DEBIT_VALUE, &ulCurrentPostageValue) != BOB_OK )
    {
        dblCurrPostage = 0;
        ulCurrentPostageValue = 0;
    }
    else
    {   
        dblCurrPostage = (double)ulCurrentPostageValue;
    }
    
    // Check if this postage is legal to print.  This looks at a bunch of 
    //  different EMD values, and puts the status in location at arg2.
    (void)fnCheckLegalPostage( dblCurrPostage, &bEnterAmt_CurrPVIsLegal );

    return( PREPOST_COMPLETE );
}


/* *************************************************************************
// FUNCTION NAME: fnpHighPostageWarning
//
// DESCRIPTION: 
//      Pre function that sets the high postage warning pending flag to false.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//      
// MODIFICATION HISTORY:
//      Victor Li     Initial version 
// *************************************************************************/
SINT8 fnpHighPostageWarning( void (** pContinuationFcn)(), 
                            UINT32      *pMsgsAwaited,
                            T_SCREEN_ID *pNextScr )
{
    SET_MENU_LED_OFF();

    fHighPostagePending = FALSE;

    return (PREPOST_COMPLETE);
}


/* Field functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fnfPostageValue
//
// DESCRIPTION: 
//      Output field function for the postage value with menoy sign
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//      Currently, the case of negative value isn't considered in the function, 
//      the minus sign may need to be added.
//
// MODIFICATION HISTORY:
//              Joe Mozdzer     Initial version
//  11/15/2005  Vincent Yi      Code cleanup and added money sign 
//  05/19/2006  James Wang      To get the value of some parameters from BOB 
//                              and flash with some new BR functions, and replace
//                              hard-coded value with "#define" statement.
//  03/26/2006  I. Le Goff      In ajout mode, display zero postage Value if not
//                              all inputs have been entered
//  05/10/2010  Deborah Kohl    Add padding characters either when 
//                              right aligned andin LTR mode or 
//                              left aligned and in RTL mode
//  05/14/2010  Jingwei,Li      Trim the redundant space when get table text for RTL.
// *************************************************************************/
BOOL fnfPostageValue (UINT16    *pFieldDest, 
                      UINT8     bLen, 
                      UINT8     bFieldCtrl,
                      UINT8     bNumTableItems, 
                      UINT8     *pTableTextIDs,
                      UINT8     *pAttributes)
{
    UINT8       bPVLen;
    UINT8       bMinDisplayedIntegers;
    UINT8       bMinDisplayedDecimals;
    UINT8       bWritingDir;
    UINT32      lwPostageVal;
    UINT16      wTextID;
    UNICHAR     pUStr[USTR_LEN] = {0};
    UINT8       bDisplay = TRUE;


    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

  switch( fnOITGetPrintMode() )
  {
      case PMODE_AJOUT_MANUAL_WGT_ENTRY:
      case PMODE_AJOUT_PLATFORM:
        // If we are in mode ajout check if all the values have been entered by the user.
        // If not, the postage value displayed will be zero
//        bDisplay = fnAreAllValuesEntered();
        break;

      default:
        break;
  }

    /* get the register value from the Bob task */
    if(fnGetDebitValue(&lwPostageVal) == TRUE)
    {
    // Force to display a zero postage value
    if(bDisplay == FALSE)
    {
        lwPostageVal = 0;
    }

        /* convert it to Unicode */
        bMinDisplayedIntegers = fnGetMinDisplayedIntegers();
        bMinDisplayedDecimals = fnGetMinDisplayedDecimals();

        bPVLen = fnMoney2Unicode(pFieldDest, (double)(lwPostageVal), TRUE, 
                                bMinDisplayedIntegers, bMinDisplayedDecimals);
        bPVLen = fnUnicodeAddMoneySign(pFieldDest, bPVLen);

        /* copy the appropriate name string */
        bWritingDir = fnGetWritingDir();
        if (bNumTableItems > 0)
        {
        	EndianAwareCopy(&wTextID, pTableTextIDs + (0 * sizeof(UINT16)), sizeof(UINT16));
            if(bWritingDir == RIGHT_TO_LEFT)
            {
                 fnCopyTableTextForRTL(pUStr, wTextID, bLen);
            }
            else
            {
                fnCopyTableText(pUStr, wTextID, bLen);
            }
            if(pUStr[0] == 0) 
            {
                pUStr[0] = ' ';
            }

            bPVLen = fnUnicodeAddPostageSymbol(pFieldDest, bPVLen, pUStr[0]);
        }

        /* if field is right-aligned, pad left of string with spaces */
        if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
            ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
        {
            // when field right-aligned, we need to pad only if we are not in RTL mode, 
            // because right aligned in LTR mode gives left aligned in RTL mode
            // when field left-aligned, we need to pad only if we are in RTL mode, 
            // because left aligned in LTR mode gives right aligned in RTL mode
            fnUnicodePadSpace(pFieldDest, bPVLen, bLen);
        }

        /* make sure the field is NULL terminated.  if the translation happened 
            to exceed the width of this field, this will truncate the unicode 
            string that is sitting there */
        pFieldDest[bLen] = 0;
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfLowFundsWarningAmt
//
// DESCRIPTION: 
//      Output field function for the low funds warning amount.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Joe Mozdzer     Initial version
//  11/25/2005  Vincent Yi      Code cleanup
//
// *************************************************************************/
BOOL fnfLowFundsWarningAmt (UINT16      *pFieldDest, 
                            UINT8       bLen, 
                            UINT8       bFieldCtrl,
                            UINT8       bNumTableItems, 
                            UINT8       *pTableTextIDs,
                            UINT8       *pAttributes)
{
    // Clear out field 
    SET_SPACE_UNICODE(pFieldDest, bLen);

    (void) fnOIFormatMoneyForDisplay( pFieldDest, 
                    (double) fnCMOSSetupGetCMOSSetupParams()->lwLowFundsWarning,
                    FALSE ) ;

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfHighValueWarningAmt
//
// DESCRIPTION: 
//      Output field function for the high value warning amount.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/20/2007  Oscar Wang      Value text is blank if KIP is not allowed
//  29 Mar 2007 Bill Herring    Value text is blank if KIP zero postage only mode
//  11/25/2005  Vincent Yi      Code cleanup
//              Joe Mozdzer     Initial version
//
// *************************************************************************/
BOOL fnfHighValueWarningAmt (UINT16     *pFieldDest, 
                             UINT8      bLen, 
                             UINT8      bFieldCtrl,
                             UINT8      bNumTableItems, 
                             UINT8      *pTableTextIDs,
                             UINT8      *pAttributes)
{
    UINT8   ucKIPMode = fnGetKIPMode();
    
    // Clear out field 
    SET_SPACE_UNICODE(pFieldDest, bLen);

    if ( (ucKIPMode != KIP_ZERO_ONLY_W_PASSWORD )
            && (ucKIPMode != KIP_NOT_ALLOWED))
    {
        (void) fnOIFormatMoneyForDisplay( pFieldDest, 
                        (double) fnCMOSSetupGetCMOSSetupParams()->lwHighValueWarning,
                        FALSE ) ;
    }
    else
    {
        //Leave blank if KIP zero postage only
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfPostageTapeAmtPrompt
//
// DESCRIPTION: 
//      Output field function that selects a string based on the whether the 
//      current entry buffer contains a valid postage and or a valid tape count.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// FIELD TEXT TABLE values:  (English)
//      0   Enter Amount
//      1   Enter Amount/Tape Qty
//      2   Enter Tape Qty
//
// WARNINGS/NOTES:
//  1. In the case where tapes are not allowed (current postage is illegal) AND
//      KIP is not allowed (from this screen), then displays nothing.  This
//      screen is still needed to enter Service mode.
//     In the future, if HF decides on a good idea for a prompt in this case, 
//      it can be added to the screens.
//
// MODIFICATION HISTORY:
//  05/30/2006  Vincent Yi      Initial version
//  16 Mar 2007 Bill Herring    Support new KIP zero postage only mode
//  2009.05.07  Clarisa Bellamy Modified so Tape Qty is not displayed when the 
//                      current postage value is illegal.
// *************************************************************************/
BOOL fnfPostageTapeAmtPrompt (UINT16     *pFieldDest, 
                              UINT8         bLen, 
                              UINT8         bFieldCtrl,
                              UINT8         bNumTableItems, 
                              UINT8      *pTableTextIDs,
                              UINT8      *pAttributes)
{
	pPostFn           pContinuationFunction;
	unsigned long lwMsgsAwaited;
	UINT16 *pEntryBuf;
    UINT8   ucFieldIndex = 1;  // Default to show both.
    UINT8   ubNumDigitsEntered = 0;
    T_SCREEN_ID NextScrID = 0;          // Not used, but required for call to fnpEnterAmountPre 
    UINT8   ubKipMode = fnGetKIPMode();
    BOOL    fTapesAllowed = TRUE;
	BOOL	fValidTapeBuf = FALSE;


    SET_SPACE_UNICODE( pFieldDest, bLen );
	pEntryBuf = fnGetGenPurposeEbuf()->pBuf;
    ubNumDigitsEntered = fnCharsBuffered();
	fValidTapeBuf = fnTapeBufferValid();

// ---------------
//  Until we get an entry function, check the current postage value.
    (void)fnpEnterAmountTapePre( (void (**) ()) &pContinuationFunction, &lwMsgsAwaited, &NextScrID );
	
/* ----------- Remove the code above, when the entry function fnpEnterAmountTapePre is 
                added to the screen database.  
*/
    // First check if tape entry prompt is allowed... if not, change the selection.
    if( bEnterAmt_CurrPVIsLegal != POSTAGE_OK )
    {
        // Only display Postage entry prompt.
        ucFieldIndex = 0;
        fTapesAllowed = FALSE;    
    }

    // Next check if Postage entry prompt is allowed... if not, change the selection.
    switch( ubKipMode )
    {
        // If KIP not allowed here...
        case KIP_NOT_ALLOWED:
        case KIP_FROM_FEE_LIST_ONLY:
            // ... and tape entry is allowed...
            if( fTapesAllowed == TRUE )
            {
                // ... only allow to enter # of tapes.
                ucFieldIndex = 2;
            }
            else
            {
                // ... if tapes not allowed, and KIP not allowed, show nothing.
                ucFieldIndex = bNumTableItems;  
            }
            break;
        
		case KIP_ZERO_ONLY_W_PASSWORD:
	        if (ubNumDigitsEntered <= 0)
	        {
	            //Nothing entered
	            ucFieldIndex = 1;    
	        }
	        else if (fValidTapeBuf == TRUE)
	        {
	            //Valid tape quantity
	            ucFieldIndex = 2;    
	        }
	        else
	        {
	            //Invalid tape quantity or zero postage
	            if (fnUnicode2Money( pEntryBuf ) == (double)0)
	            {
	                //Zero postage
	                ucFieldIndex = 0;
	            }
	            else
	            {
	                //Invalid for postage or tapes . . .
	                //Display no text (blank) - don't use one of the tabled texts
	                ucFieldIndex = bNumTableItems;
	            }
	        }
			break;
			
        default:
	        // if the buffer contains a valid tape count or the buffer is empty,
	        // show both indicator message. fix fraca GMSE00100316. - Adam
	        if ((fValidTapeBuf == TRUE) || (ubNumDigitsEntered <= 0))
	        {
	            ucFieldIndex = 1;    
	        }
	        else 
	        {
	            ucFieldIndex = 0;
	        }
            break;    
    }

    if( ucFieldIndex < bNumTableItems )
    {
        fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                            pTableTextIDs, ucFieldIndex );
    }

    return( (BOOL)SUCCESSFUL );
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfSetPostagePrompt
//
// DESCRIPTION: 
//      Output field function that selects text if the user needs to be prompted
//      set the postage to the value entered.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// FIELD TEXT TABLE values:  (English)
//      0   Set Postage [Enter]
//      1   - Blank -
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  16 Mar 2007 Bill Herring    Initial version
//  2009.05.07  Clarisa Bellamy Modified to make simpler and comment.
//                  If KIP not allowed from this screen (KIP_FROM_FEE_LIST_ONLY)
//                  don't prompt to save postage.  
// *************************************************************************/
BOOL fnfSetPostagePrompt (UINT16     *pFieldDest, 
                          UINT8         bLen, 
                          UINT8         bFieldCtrl,
                          UINT8         bNumTableItems, 
                          UINT8      *pTableTextIDs,
                          UINT8      *pAttributes)
{
    UINT16 *pEntryBuf   = NULL_PTR;
    UINT8   ucFieldIndex = 0;       // Default to display the prompt.
    UINT8   ubNumDigitsEntered = 0;
    UINT8   ubKipMode = fnGetKIPMode();


    SET_SPACE_UNICODE( pFieldDest, bLen );
    pEntryBuf = fnGetGenPurposeEbuf()->pBuf;
    ubNumDigitsEntered = fnCharsBuffered();

	switch (ubKipMode)
	{
        // If KIP not allowed at all, or not allowed from this screen...
        case KIP_NOT_ALLOWED:
        case KIP_FROM_FEE_LIST_ONLY:
	        ucFieldIndex = 1;  //does not display the text   
			break;
			
		case KIP_ZERO_ONLY_W_PASSWORD:
	        //KIP with zero postage only . . .
	        if ( (fnCharsBuffered() <= 0) || (fnUnicode2Money( pEntryBuf ) == (double)0) )
	        {
	            //Nothing or zero value entered
	            ucFieldIndex = 0;
	        }
	        else
	        {
	            //Non zero value - don't show text.
	            ucFieldIndex = 1;
	        }
			break;
			
		default:
            // KIP is allowed.  
            //  If the buffer is clear, don't display the save prompt.
            if( ubNumDigitsEntered == 0 )
            {
                // For now, display it, because the display will change to 
                //  invalid entry if they hit ENTER.
                // ucFieldIndex = 1;     
            }
			break;
    }
    
    if (ucFieldIndex < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                            pTableTextIDs, ucFieldIndex);
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfSetTapesPrompt
//
// DESCRIPTION: 
//      Output field function that selects text if the user needs to be prompted
//      to set the tape function to produce the quantity entered.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// FIELD TEXT TABLE values:  (English)
//      0    - Blank -
//      1   Enter Tapes Qty [ Tape ]
//  
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  16 Mar 2007 Bill Herring    Initial version
//  16 Mar 2007 Bill Herring    Support new KIP zero postage only mode
//  2009.05.04  Clarisa Bellamy If the current postage is not in a valid range,
//                  do not prompt the user to enter #of tapes.  Whether KIP is 
//                  supported or not is irrelevant to whether the user can 
//                  print tapes.
//
// *************************************************************************/
BOOL fnfSetTapesPrompt (UINT16     *pFieldDest, 
                        UINT8       bLen, 
                        UINT8       bFieldCtrl,
                        UINT8       bNumTableItems, 
                        UINT8      *pTableTextIDs,
                        UINT8      *pAttributes)
{
	pPostFn           pContinuationFunction;
	unsigned long lwMsgsAwaited;
    T_SCREEN_ID NextScrID = 0;          // Not used, but required for call to fnpEnterAmountPre 
    UINT8   ucFieldIndex = 0;
    BOOL    fTapeNumValid = FALSE;  // Default to invalid value.


    SET_SPACE_UNICODE(pFieldDest, bLen);

// ---------------
//  Until we get an entry function, check the current postage value.
    (void)fnpEnterAmountTapePre( (void (**) ()) &pContinuationFunction, &lwMsgsAwaited, &NextScrID );
	
/* ----------- Remove the code above, when the entry function fnpEnterAmountTapePre is 
                added to the screen database.  
*/
    // First check if tape entry prompt is allowed... if not, change the selection.
    if( bEnterAmt_CurrPVIsLegal == POSTAGE_OK )
    {
        // If we don't have anything to save, don't prompt to save.
        if( fnCharsBuffered() > 0 )
        {
            // If the current postage is good, and we have an entered value, 
            //  check if the entered value is valid for # of tapes...
            fTapeNumValid = fnTapeBufferValid(); 
        }
    }

    if (fTapeNumValid == TRUE)
    {
        //There is a valid tape quantity
        ucFieldIndex = 1;    
    }
    else 
    {
        //No entry or Invalid tape quantity value
        ucFieldIndex = 0;
    }

    if (ucFieldIndex < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                            pTableTextIDs, ucFieldIndex);
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfKIPEnteredPostageAmt
//
// DESCRIPTION: 
//      Output field function for the KIP value  amount.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/07/2006  Oscar                          Adapted for Future Phoenix      
//              Joe Mozdzer/Mike Richardson    Initial version
//
// *************************************************************************/
BOOL fnfKIPEnteredPostageAmt (UINT16     *pFieldDest, 
                              UINT8         bLen, 
                              UINT8         bFieldCtrl,
                              UINT8         bNumTableItems, 
                              UINT8      *pTableTextIDs,
                              UINT8      *pAttributes)
{
    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

#ifdef NEED_OLD_STUFF
    //Please use new stuff now. -Victor Aug 31 '04
    bMinDisplayedIntegers = fnFlashGetByteParm(BP_MIN_DISPLAYED_INTEGERS);
    bMinDisplayedDecimals = fnFlashGetByteParm(BP_MIN_DISPLAYED_DECIMALS);
    bARLen = fnMoney2Unicode(sFunds, pTAccount->dFunds, true, bMinDisplayedIntegers, bMinDisplayedDecimals);
    bARLen = fnUnicodeAddMoneySign(sFunds, bARLen);
    sFunds[bARLen] = 0;
#else /*NEW_STUFF*/
    // format value for standard output field display
    (void) fnOIFormatMoneyForDisplay( pFieldDest, 
                                        dblHighPostageProposal, 
                                        FALSE ) ;
#endif /*OLD_STUFF*/

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfDisplayHighPostage
//
// DESCRIPTION: 
//      Output field function that displays the high value setting prompt if
//      it is appropriate
//      set the postage to the value entered.
//      
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFieldDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//  Menu text NOT shown if KIP is zero postage only (France)
//  Menu text NOT shown if KIP is not allowed (Germany)
//  Menu text shown otherwise
//
// MODIFICATION HISTORY:
//  20 Aug 2007 Oscar Wang      Update for KIP Not Allowed mode.
//  29 Mar 2007 Bill Herring    Initial
//  
//
// *************************************************************************/
BOOL fnfDisplayHighPostage (UINT16     *pFieldDest, 
                            UINT8       bLen, 
                            UINT8       bFieldCtrl,
                            UINT8       bNumTableItems, 
                            UINT8      *pTableTextIDs,
                            UINT8      *pAttributes)
{
    UINT8   ucFieldIndex = 0;
    UINT8   ucKIPMode = fnGetKIPMode();
    
    SET_SPACE_UNICODE(pFieldDest, bLen);

    if (( ucKIPMode != KIP_ZERO_ONLY_W_PASSWORD )
        && (ucKIPMode != KIP_NOT_ALLOWED))
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                            pTableTextIDs, ucFieldIndex);
    }
    else
    {
        //Field remains blank
    }

    return ((BOOL) SUCCESSFUL);
}

/* Hard key functions */

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkBufferKeySmartPostage
//
// DESCRIPTION: 
//      Hard key function that buffers the keypress in the current entry buffer
//      if the digit entered would not cause the buffered postage to violate 
//      PCN and vault parameters pertaining to postage configuration. 
//      Currently we only check that the number of digits past the decimal point
//      does not exceed the number of decimal places in the meter less the number
//      of fixed zeroes (FIXED ZERO CHECK IS TEMPORARILY REMOVED!).
//      There is no dynamic check of bank restrictions.
//
// INPUTS:
//      Standard hard key function inputs (format 1).
//
// OUTPUTS:
//      None
//
// RETURNS:
//      This hard key function always stays on the existing screen.
//
// MODIFICATION HISTORY:
//              Joe Mozdzer     Initial version
//  11/15/2005  Vincent Yi      Code cleanup
//  04/24/2006  James Wang      Extract business rules from this function.
//  02/08/2007  Kan Jiang       Modified to use the length of Max Settable Value 
//                              as the max input number instead the hard code 5 
//                              for fixing Fraca 114092
//  05/18/2007  Adam Liu        Use (ucLength - bMoneyDecimals) to replace hardcoded
//                              3 to fix fraca GMSE00116405.
//  2008.03.24  Clarisa Bellamy  The minimum dispalyed decimals is used to convert 
//                               an entry without a decimal point to an entry with
//                               a decimal point. This function will no longer assume
//                              that the minumum displayed decimals is 1 less than the
//                              decimals implied in the vault.  With the introduction
//                              of flex debiting, the implied decimals can change.
//  12/11/2009  Raymond Shen    Doesn't accept decimal point for France.
// *************************************************************************/
T_SCREEN_ID fnhkBufferKeySmartPostage (UINT8        bKeyCode, 
                                       T_SCREEN_ID  wNextScr1,
                                       T_SCREEN_ID  wNextScr2)
{
    BOOL            fCursorDisable = FALSE;
    BOOL            fDecimalEntered = FALSE;// TRUE if a decimal has already been entered.
    UINT16          *pEntry;                
    UINT16          usUnicodeDecimalChar;   // The character used for decimal point in this country.
    UINT32          ulNumDecimals = 0;      // Number of digits right of the entered decimal point.
    UINT8           bMoneyDecimals;         // Implied number of decimal places in the vault.
    UINT16          usNextChar;             // Character the current key maps to.
    SINT32          lDecimalsAccepted;      // Number of decimals ALLOWED to the right of a decimal place.
    T_SCREEN_ID     usNext = 0;
    UINT32          ulMaxSettable = 0;      // Maximum settable postage, sent down in postal config.
    UINT8           ucLength = 0;           // Number of total digits in max settable postage
    UINT8           bMinDisplayedDecimals;  // Number of decimals implied in the entry if user does not enter a decimal point.
    UINT8           bMaxWholeDigits;        // Max number of digits allowed before the decimal point.
    UINT8           bMaxDigitsImpliedDec;   // Max number of digits that can be entered if the decimal is not entered.

    /* get the unicode decimal point character for this pcn */
    usUnicodeDecimalChar = fnGetDecimalChar();

    /* check if the decimal point character is already in the entry buffer */
    pEntry = fnGetCurrentEntryBuffer()->pBuf;
    while ((*pEntry != 0) && 
           ((*pEntry) != usUnicodeDecimalChar))
    {
        pEntry++;
    }

    if (*pEntry == usUnicodeDecimalChar)
    {
        /* if it is there, determine the number of digits currently to the
            right of the decimal point */
        fDecimalEntered = TRUE;
        while (fnIsUnicodeDigit(*(++pEntry)) == TRUE)
        {
            ulNumDecimals++;
        }
    }

   /** Get the Max Settable value **/
    if( fnValidData(BOBAMAT0, VLT_DEBOPTS_FILE_MAX_DEB_VAL, (void *)&ulMaxSettable)!=BOB_OK )
    {
           fnProcessSCMError();
           ulMaxSettable = 0;
    }

    // Calculates the number of digits in the maximum settable postage.
    while(ulMaxSettable > 0)
    {
         ulMaxSettable = ulMaxSettable/10;
         ucLength++;
    }
    
    // Need to get some information about decimals.  To interpret the max settable value, we need 
    //  to know the number of implied decimals in the vault.
    if( (fnValidData( BOBAMAT0, VLT_PURSE_DECIMALS, &bMoneyDecimals ) != (BOOL)BOB_OK) )
    {
        fnProcessSCMError();
        return usNext;
    }
    // The minimum displayed decimals is used to imply the decimal point if the user doesn't enter one.
    bMinDisplayedDecimals = fnFlashGetByteParm( BP_MIN_DISPLAYED_DECIMALS );
    bMaxWholeDigits = ucLength - bMoneyDecimals;

    // If they have NOT entered a decimal, we imply it based on min displayed decimals, and
    //  do not allow more digits than needed to create the max settable postage.
    // Except we must allow enough digits to enter the service or Engineering mode passwords.
    if( !fDecimalEntered )
    {
        bMaxDigitsImpliedDec = (ucLength - bMoneyDecimals) + bMinDisplayedDecimals;
        if(   (fnCharsBuffered() >= bMaxDigitsImpliedDec)
           && (fnCharsBuffered() >= STD_PWD_LEN) )      // allow enough digits to enter the service/Engr mode passwords.
        {
            return usNext;
        }
    }

    /* find out what the entered character maps to */
    if (fnSearchKeyMap(bKeyCode, &usNextChar) == FALSE)
    {
        /* the character is automatically not buffered if it doesn't map to 
            anything */
        return usNext;
    }

    // get the number of characters that may be typed after
    // the decimal point
    if(fnGetDecimalsAccepted(&lDecimalsAccepted) == FALSE)
    {
        return usNext;
    }

    
    /* if characters have been entered... */
    if( fnCharsBuffered() > 0 )
    {
        // Check for reasons we can't accept a decimal point for this character.
        if( usNextChar == usUnicodeDecimalChar )
        { 
            if(   (fDecimalEntered == TRUE)                 // If decimal already entered, 
               || (lDecimalsAccepted <= 0)                  // or if no decimal digits allowed for this PCN,
               || (fnCharsBuffered() > bMaxWholeDigits)    // or already entered more than max whole digits allowed (decimal is implied already.),
               || (fnFlashGetByteParm( BP_DEFAULT_RATING_COUNTRY_CODE ) == FRANCE) )  // or the country is France
            {
                return( usNext );
            }
        }
        
        /* we cannot accept a character if we have typed the maximum number of 
            decimals that this PCN supports */
        if ((fDecimalEntered == TRUE) && (ulNumDecimals >= lDecimalsAccepted))
        {
            if (fnIsKIPAllowed() == TRUE)
            {
//t             usNext = wNextScr2; // invalid postage amount
            }
            return usNext;
        }
        else
        {
            /* if this is the last character we will accept, then turn off the
                cursor after we buffer it */
            if ((fDecimalEntered == TRUE) && ((ulNumDecimals + 1) 
                                                    >= lDecimalsAccepted))
            {
                fCursorDisable = TRUE;
            }
        }
    }
    else
    {
        /* if this is the first character typed, it will always be accepted unless
            it is a decimal point and the current configuration does not have
            any decimal places */
        if ((usNextChar == usUnicodeDecimalChar) && (lDecimalsAccepted <= 0))
        {
            return usNext;
        }
    }

    /* buffer the key if it passed the tests above */
    fnBufferKeyClear1st(bKeyCode);

    /* disable the buffer is we have entered the last valid character */
    if (fCursorDisable == TRUE)
    {
        fnBufferDisable();
    }

    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      SetUpKIPPiece
//
// DESCRIPTION: 
//      Validates amount entered, class selected etc against business rules for
//      KIP and then sets up the meter for the KIP print if successful.
//
// INPUTS:
//      dblAmtEntered - The value of the KIP print to perform
//      bClearBuffer  - Set or clear this value to determine whether to clear
//                      the entry buffer on the KIP screen.  The caller should
//                      only act on this if this function returns 0 (stay on
//                      same screen).
//      bNumNext, pNextScreens - As per standard HK functions (format 2)
//
// OUTPUTS:
//      bClearBuffer  - This function will set or clear this value to determine
//                      whether the caller should clear the entry buffer on the
//                      KIP screen.  The caller should only act on this if this
//                      function returns 0 (stay on same screen).
//
// RETURNS:
//      NEXT SCREEN:  The next screen returned is one of the following:
//
//      Note: The prefunction sequence of the main screen will handle the warnings 
//              listed below. (MMIndiciaReady)  
//
//      if the postage value is valid
//      if the value entered exceeds the max settable postage value
//      if the amount entered does not comply with bank restriction requirements
//      if the amount entered does not comply with fixed zero requirements
//      if the value is subject to the high value warning.
//
//      next screen 0 -  MMIndiciaReady     (USA)           KIP_ALLOWED         1
//      next screen 1 -  ErrHighPostageExceeded *high postage warning
//      next screen 2 -  ErrInvalidPostage  *invalid postage
//      next screen 3 -  ErrKIPNotAllowed   (Australia)     KIP_NOT_ALLOWED         0   
//      next screen 4 -  KIPClassRequired   (Holland)   
//      next screen 5 -  KIPConfirmAmount   (Holland)       KIP_ALLOWED_VALIDATION      3
//      next screen 5 -  KIPConfirmAmount   (Italy)         KIP_ALLOWED_USE_MDS_TOKENS  6
//      next screen 6 -  KIPEnterPassword   (France)        KIP_ALLOWED_PASSWORD        2
//
// NOTES:
//  1. For G9 Canada, where KIP mode is KIP_ALLOWED_FENCINGLIMIT, KIP is   
//      actually only allowed if the meter is configured as a "small capacity" 
//      meter.  If it is configured as a large capacity meter (which most are), 
//      then all postage entries are invalid.  (No explanation on the screen.
//  2. If the postage is invalid becase it is within the fencing limits, then 
//      their is no explanation given to the user.
//
// MODIFICATION HISTORY:
//  2011.08.14  Clarisa Bellamy - Just cleaned up a few things.
//  15 JUL 2009 Jingwei,Li       Call fnOITSetPrintMode to disable hybrid mode for KIP.
//  01 Dec 2008 Jingwei,Li       Call fnOITSetPrintModeHybrid to change mode.
//  05 Sep 2008 Joe Qu          Added call to fnRateRefreshIndiciaInfo() to refresh 
//                              Indicia info after postage is written into PSD in KIP mode
//  24 Oct 2007 P.Vrillaud      For Swiss, remove fnOITGetPrintMode() test
//  17 Oct 2007 Deborah Kohl    For Swiss, KIP is allowed only from fee selection
//                              so display the message for redirection to
//                              class selection (KIPClassRequired)
//  21 Jun 2007 Simon Fox       Created this function by pulling out the
//                              business logic from fnhkPCNCheckForEnterAmount
//                              for re-use by the French zero-print short-cut.
// *************************************************************************/
static T_SCREEN_ID SetUpKIPPiece(double dblAmtEntered, BOOL *bClearBuffer,
                                 UINT8  bNumNext, 
                                 T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;
    UINT8 bKIPMode = fnGetKIPMode();
    UINT32 ulPV;
//    double dbMinKipAmt;
    double dbLowFlimit,dbHighFlimit;
    UINT8 bKIPCarType = 0;
    UINT8 bIsLegal;

    *bClearBuffer = FALSE;

    if ( (bKIPMode == KIP_ZERO_ONLY_W_PASSWORD) && (dblAmtEntered != (double)0) )
    {
        //If the KIP mode only allows zero postage but postage is > 0.
        //Ignore keypress - exit (to same screen)
        return usNext;
    }

    if (bKIPMode == KIP_FROM_FEE_LIST_ONLY)
    {
        usNext = pNextScreens[4]; // Goto class required screen            
        return usNext;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////
// If Holland must select class Right before postage
///////////////////////////////////////////////////////////////////////////////////////////////////////
    bKIPCarType = fnFlashGetByteParm(BP_KEY_IN_POSTAGE_CLASS);  // 2 - Holland
    if ( (bKIPCarType == KIP_CLASS_REQ_ENTRY_LIMITED)   // 2 - Holland
            || ( (bKIPCarType == KIP_CLASS_REQUIRED)
                && !fnRateIsCurrentCarrierBranded() ) ) // have to select a class before entering the postage
                                // if current class is not a branded class, too.
    {
        if (fnKIPWasLastScreenClass()== FALSE)
        {
            usNext = pNextScreens[4];               // Goto class required screen

            if (fnOITGetPrintMode() != PMODE_MANUAL)                        // KIP mode ?
            {
                //fnOITSetPrintModeHybrid(PMODE_MANUAL);
                  fnOITSetPrintMode(PMODE_MANUAL);
                //clear postage, to avoid the case: by pressing clear
                // key, home screen display KIP print mode and last
                // postage maybe rated by manual/platform weight.
                fnZeroPostage();
                          
            }   

            return usNext;
        }
/*
        else
        {
            fKIPLastScreenClass = FALSE;
        }
*/
    }

    //Canada validation
    if(   (dblAmtEntered !=0)
       && (bKIPMode == KIP_ALLOWED_FENCINGLIMIT) )
    {   
        // If rates manager is initialized, and fencing limit is not trashed, get fencing limit
        if(   (fnRateRMInitialized() == TRUE) 
           && (fnRateGetFencingLimitStatus() == SUCCESS) )
        {
            // Get fencing limits.
            dbLowFlimit = fnDoubleAdjustedMoney2Double(fnRateGetLowFencingLimit());
            dbHighFlimit = fnDoubleAdjustedMoney2Double(fnRateGetHighFencingLimit());

            // So far, only "Small Meter" configurations support KIP when 
            //  KIP_ALLOWED_FENCINGLIMIT.  If we ever need to support the 
            //  KIP_ALLOWED_FENCINGLIMIT configuration for a "large meter" 
            //  config, then we will need to change code and maybe add a new 
            //  EMD parameter.
            if( fnIsSupportSmallMeter() == TRUE )
            {
                if( fnIsSmallMeter() == TRUE )
                {
                    // small meter - Postages within fencing limits are not allowed.
                    if( (dblAmtEntered >= dbLowFlimit) && (dblAmtEntered <= dbHighFlimit) )
                    {
                        // The error text is undefined.
                        // Set the entry message to invalid entry, no screen change
                        *bClearBuffer = TRUE;
                        fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
                        return usNext;              // Error Condition
                    }
                    // else, check value further below.
                }
                else
                {
                    // large meter - KIP not allowed at all
                    // The error text is undefined.
                    // Set the entry message to invalid entry, no screen change
                    *bClearBuffer = TRUE;
                    fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
                    return usNext;              // Error Condition
                }
            }
        }
        else
        {
            // Rating not intitialized OR could not get fencing limits.
            usNext = pNextScreens[8];    //KIP file error
            return usNext;               
        }
        
    }
  //Andy comment below for the feature is not required so far.
  /*  else
        if ((dblAmtEntered != 0) && (bKIPMode == KIP_ALLOWED_MIN_KIP_LIMIT))
        {   
            //get fencing limit if rates manager is initialized
            if (fnRateRMInitialized() && fnRateGetMinKIPAmtStatus() == SUCCESS)
            {
                dbMinKipAmt = fnDoubleAdjustedMoney2Double( fnRateGetMinKIPAmtValue() );
                if (dblAmtEntered < dbMinKipAmt)
                {
                    // The error text is undefined.
                    // Clear the entry buffer
                    fnEntryBufferSnapshotRestore();
                    fnClearEntryBuffer();
                    // Set the entry message to invalid entry, no screen change
                    fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
                    return usNext;              // Error Condition
                }

            }
            else
            {
                usNext = pNextScreens[8];  //KIP file error    
                return usNext;               
            }
        }*/
///////////////////////////////////////////////////////////////////////////////
//   Validate Postage value 
///////////////////////////////////////////////////////////////////////////////
    /* check for fixed zero, bank restrictions, or max settable postage 
        violations check if value subject to the high value warning */
    if (fnCheckLegalPostage(dblAmtEntered, &bIsLegal) != SUCCESSFUL)
    {
        // The error text is undefined.
        *bClearBuffer = TRUE;
        // Set the entry message to invalid entry, no screen change
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
        return usNext;              // Error Condition
    }

    if (bIsLegal != POSTAGE_OK)
    {
        // FIXED_ZERO_VIOLATION:
        // BANK_RESTR_VIOLATION:
        // MIN_SETTABLE_VIOLATION:
        // MAX_SETTABLE_VIOLATION:
//      usNext = pNextScreens[2]; // invalid postage screen
        *bClearBuffer = TRUE;
        // Set the entry message to invalid entry, no screen change
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE);
        return usNext;
    }

    if (fnCheckHighPostage(dblAmtEntered) == TRUE)
    {
        // Set a pending flag so the high value postage warning
        // screen knows the print mode needs to be changed.     
        fKIPModePending = TRUE;     
        
        if (fnGetKIPMode() == KIP_ALLOWED_PASSWORD)
        {
            pKIPPasswordInfo->lwPostageValue = dblAmtEntered;
            pKIPPasswordInfo->ucPasswordSelectPurpose = KIPW_HIGH_VALUE_WARN;
        }           
        usNext = pNextScreens[1]; // high postage warning screen
        return usNext;
    }

    ulPV = (UINT32) dblAmtEntered;


///////////////////////////////////////////////////////////////////////////////
//  Postage Value OK. Set next screen
///////////////////////////////////////////////////////////////////////////////

    switch (bKIPMode)
    {
        case KIP_ALLOWED:                       // 1 - USA
        case KIP_ALLOWED_FENCINGLIMIT:
        case KIP_ALLOWED_HRT_STRINGS:   // 9 - Brazil
                usNext = pNextScreens[0];           // MMIndiciaReady
            break;                                  // Normal processing.

        case KIP_ALLOWED_PASSWORD:              // 2 - France
        case KIP_ZERO_ONLY_W_PASSWORD:          // 8 - France (zero only allowed)
//          if  (ulPV != 0)                     // if postage = 0 -> MMIndiciaReady
            {
                pKIPPasswordInfo->lwPostageValue = ulPV;
                pKIPPasswordInfo->ucPasswordSelectPurpose = KIPW_POSTAGE_ENTER_REQUIRED;
                usNext = pNextScreens[7];       // KIPEnterPassword

                return usNext;                      // Processing to continue next screen
            }
//          else
//          {
//              pKIPPasswordInfo->lwPostageValue = 0;
//              usNext = pNextScreens[0];       // MMIndiciaReady
//          }                                   // Normal Processing
//          break;

        case KIP_ALLOWED_VALIDATION:            // 3 - Holland
//      case KIP_ALLOWED_USE_MDS_TOKENS:        // 6 - Italy
            fKIPModePending = TRUE;         // Set a pending flag so the postage confirm screen
                                // knows the print mode needs to be changed.
            usNext = pNextScreens[5];           // KIPConfirmAmount
            return usNext;                      // Processing to continue next screen

        default:            // lint requirement
            break;                              // Normal Processing    
    }   // switch (...)


///////////////////////////////////////////////////////////////////////////////
//   Postage value OK, Write to Indicia
///////////////////////////////////////////////////////////////////////////////
    if (smgr_set_setting_data(SETTING_ITEM_POSTAGE, &ulPV, sizeof(UINT32)) != NU_SUCCESS)
    {
        return usNext;
    }
    else
    {
        // Indicate who just told Bob to set the debit value
        //TODO JAH Remove dcrules.c fnDCAP_SetWhoCalled(OIJ_NORMAL_POSTAGE_CALLEDME);
    }

    (void)fnRateRefreshIndiciaInfo();
    
    if (fnOITGetPrintMode() != PMODE_MANUAL)          // KIP mode ?
    {
      //  fnOITSetPrintModeHybrid(PMODE_MANUAL); // set the print mode to KIP
        fnOITSetPrintMode(PMODE_MANUAL);
    }

/*Vincent
///////////////////////////////////////////////////////////////////////////////
//  Set Hybrid mode, if conditions allow.
///////////////////////////////////////////////////////////////////////////////
    if (fnOITGetPrintMode() != PMODE_MANUAL)            // KIP mode ?
    {
              fnOITSetPrintMode(PMODE_MANUAL);
        if ( (bKIPCarType == KIP_CLASS_NOT_ALLOWED)
                || !fnRateIsCurrentCarrierBranded() )
        {
            // clear class selection
            fnSetRatingStatus( RATE_INITIAL_STATUS );
            fnUnSelectClass();
        }

    }
*/
    return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnhkPCNCheckForEnterAmount
//
// DESCRIPTION: 
//      Hard key function that Checks the PCN parameters for the Enter key,
//      Then when approiate validates and stores the postage value, sets the 
//      hybrid mode, selects the appropriate screens.
//      This function is written in format 2 for hard key functions, 
//      which allows a variable number of next screen choices.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None
//
// RETURNS:
//      NEXT SCREEN:  The next screen returned is one of the following:
//
//      Note: The prefunction sequence of the main screen will handle the warnings 
//              listed below. (MMIndiciaReady)  
//
//      if the postage value is valid
//      if the value entered exceeds the max settable postage value
//      if the amount entered does not comply with bank restriction requirements
//      if the amount entered does not comply with fixed zero requirements
//      if the value is subject to the high value warning.
//
//      next screen 0 -  MMIndiciaReady     (USA)           KIP_ALLOWED         1
//      next screen 1 -  ErrHighPostageExceeded *high postage warning
//      next screen 2 -  ErrInvalidPostage  *invalid postage
//      next screen 3 -  ErrKIPNotAllowed   (Australia)     KIP_NOT_ALLOWED         0   
//      next screen 4 -  KIPClassRequired   (Holland)   
//      next screen 5 -  KIPConfirmAmount   (Holland)       KIP_ALLOWED_VALIDATION      3
//      next screen 5 -  KIPConfirmAmount   (Italy)         KIP_ALLOWED_USE_MDS_TOKENS  6
//      next screen 6 -  KIPEnterPassword   (France)        KIP_ALLOWED_PASSWORD        2
//
// MODIFICATION HISTORY:
//  08/21/2007  Adam Liu        Allow zero postage KIP mode on Canadian large meter, fix fraca GMSE00127939.
//  06/27/2007  Raymond Shen    Add code to forbid KIP on Canadian large meter.
//  21 Jun 2007 Simon Fox       Split into two functions.  Separated screen
//                              logic (in this function) from business logic
//                              which is now in SetUpKIPPiece which is called
//                              by this function.  Allows re-use of business
//                              logic for the French zero-print short-cut.
//  04/18/2007  Vincent Yi      Added code to handle KIP_NOT_ALLOWED case
//  12/15/2006  Oscar Wang      Clear the postage if print mode is set to KIP.
//  12/12/2006  Oscar Wang      Check if nothing or only period is inputted.
//  05/19/2006  James Wang      To get KIP mode and set register debit value 
//                              with some new BR functions.
//  11/15/2005  Vincent Yi      Code cleanup
//              MRichardson     Initial version
// *************************************************************************/
T_SCREEN_ID fnhkPCNCheckForEnterAmount(UINT8            bKeyCode, 
                                       UINT8            bNumNext, 
                                       T_SCREEN_ID *    pNextScreens)
{
    BOOL bClearBuffer;
    double          dblAmtEntered;
    T_SCREEN_ID     usNext = 0;
    UINT8           bKIPMode = fnGetKIPMode();
    UINT16      *   pEntryBuf = fnGetGenPurposeEbuf()->pBuf;
    const UINT16         pPeriodOnly[] = { //"."
    0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x002E, 0x0000
        };

    if (bKIPMode == KIP_NOT_ALLOWED)    // 0 - Australia
    {
        usNext = pNextScreens[3];
        return usNext;   // KIPNotAllowed,stay current screen

    }

    // if nothing in buffer, or only period in buffer
    if( (fnIsBufferBlank() == TRUE)
        ||( unistrcmp(pEntryBuf, pPeriodOnly) == 0))
    {
        // Clear the entry buffer
        fnClearEntryBuffer();
        // Set the entry message to invalid entry, no screen change
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
        return usNext;
    }
    
    // Capture all entry buffer data so it can be later restore
    fnEntryBufferSnapshotCapture();
    /* turn the buffer contents into a double */
    fnUnicodeProcessImpliedDecimal(pEntryBuf, fnBufferLen());   
    dblAmtEntered = fnUnicode2Money(pEntryBuf);

    usNext = SetUpKIPPiece(dblAmtEntered, &bClearBuffer, bNumNext, pNextScreens);
    if (usNext == 0)
    {
        fnEntryBufferSnapshotRestore();
        if (bClearBuffer)
            fnClearEntryBuffer();
    }
    return usNext;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnhkUseHighValuePostage
//
// DESCRIPTION: 
//      Hard key function that sets the current postage value to the high 
//      value postage amount that is being stored in a temporary global 
//      container.  This function is called when the user confirms that they 
//      want to accept the high value postage amount that they just entered.
//
// INPUTS:
//      Standard hard key function inputs (format 1).
//
// OUTPUTS:
//      None
//
// RETURNS:
//      wNextScr1
//      wNextScr1, when KIP password is needed
//
// MODIFICATION HISTORY:
//              Joe Mozdzer     Initial version
//  11/16/2005  Vincent Yi      Code cleanup
//  05/19/2006  James Wang      To get KIP mode and set register debit value 
//                              with some new BR functions.
//  09/05/2008  Joe Qu          Added call to fnRateRefreshIndiciaInfo() to refresh 
//                              Indicia info after postage is written into PSD in KIP mode
//  12/01/2008 Jingwei,Li      Call fnOITSetPrintModeHybrid to set mode.
//  07/15/2009 Jingwei,Li       Call fnOITSetPrintMode to disable hybrid mode for KIP. 
// *************************************************************************/
T_SCREEN_ID fnhkUseHighValuePostage (UINT8          bKeyCode, 
                                     T_SCREEN_ID    wNextScr1,
                                     T_SCREEN_ID    wNextScr2)
{
    T_SCREEN_ID     usNext = wNextScr1; // ready screen
    UINT32          ulPV;

    ulPV = (UINT32) dblHighPostageProposal;

    if ((fnGetKIPMode() == KIP_ALLOWED_PASSWORD) && 
        (pKIPPasswordInfo->ucPasswordSelectPurpose == KIPW_HIGH_VALUE_WARN))
    {
        pKIPPasswordInfo->lwPostageValue = ulPV;
        pKIPPasswordInfo->ucPasswordSelectPurpose = KIPW_HIGH_VALUE_WARN;
        usNext = wNextScr2; // Go to KIP check password screen.
    }
    else
    {
        if (smgr_set_setting_data(SETTING_ITEM_POSTAGE, &ulPV, sizeof(UINT32)) != NU_SUCCESS)
        {
            return usNext;
        }
        else
        {
            // Indicate who just told Bob to set the debit value
            //TODO JAH Remove dcrules.c fnDCAP_SetWhoCalled(OIJ_NORMAL_POSTAGE_CALLEDME);
        }

        if (fKIPModePending == TRUE)
        {
            fKIPModePending = FALSE; // always clear the flag after it is used.
            if (fnOITGetPrintMode() != PMODE_MANUAL)            // KIP mode ?
            {
                //fnOITSetPrintModeHybrid(PMODE_MANUAL); // set the print mode to KIP
                // 2009.7.13 don't allow to set hybrid mode from KIP
                fnOITSetPrintMode(PMODE_MANUAL);
            }

            if (fnOITGetPrintMode() != PMODE_MANUAL)    // KIP mode ?
            {
                //fnOITSetPrintModeHybrid(PMODE_MANUAL);
                // 2009.7.13 don't allow to set hybrid mode from KIP
                fnOITSetPrintMode(PMODE_MANUAL);
                if (fnFlashGetByteParm(BP_KEY_IN_POSTAGE_CLASS) 
                                    == KIP_CLASS_NOT_ALLOWED)
                {
                    // clear class selection
//                  fnSetRatingStatus( RATE_INITIAL_STATUS );
                    fnRateUnSelectClass();
                }

            } // end of PMODE_MANUAL

        } // end of fKIPModePending

        (void)fnRateRefreshIndiciaInfo();
   }

    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkRejectHighValuePostage
//
// DESCRIPTION: 
//                        Hard key function that rejects the high postage amount that
//              is being stored in a temporary global container.  The container
//              is cleared as well as the high postage pending flag.//
// INPUTS:
//      Standard hard key function inputs (format 1).
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always goes to wNextScr1
//
// MODIFICATION HISTORY:
//  12/07/2006  Oscar Wang      Adapted for Future Phoenix
//              Joe Mozdzer     Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkRejectHighValuePostage (UINT8       bKeyCode, 
                                          T_SCREEN_ID   wNextScr1,
                                          T_SCREEN_ID   wNextScr2)
{
    dblHighPostageProposal = 0.0;
    fHighPostagePending = FALSE;                                                            

    if (fnFlashGetByteParm(BP_KEY_IN_POSTAGE_CLASS) == KIP_CLASS_REQ_ENTRY_LIMITED) // 2 - Holland
    {
        fKIPLastScreenClass = TRUE;
    }
    
    // get the KIP mode without the Dcap bit
    if (((fnFlashGetByteParm(BP_KEY_IN_POSTAGE_MODE) & ~KIP_USE_MDS_TOKENS_MASK) == KIP_ALLOWED_PASSWORD)
        && pKIPPasswordInfo->ucPasswordSelectPurpose == KIPW_HIGH_VALUE_WARN )
    {
        pKIPPasswordInfo->lwPostageValue = 0;
        pKIPPasswordInfo->ucPasswordSelectPurpose = 0;
    }
    return (wNextScr1); // KIP enter postage screen
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnhkValidateHighValueWarnAmt
//
// DESCRIPTION: 
//      Hard key function that validates and stores a new high value warning 
//      amount.
//
// INPUTS:
//      Standard hard key function inputs (format 1).
//
// OUTPUTS:
//      None
//
// RETURNS:
//      if amount ok, go to wNextScr1, otherwise to wNextScr2
//
// MODIFICATION HISTORY:
//  05/17/2011  Joe Zhang     Get rid of checking fencing limit to fix fraca GMSE00180584
//  04/28/2011  Joe Zhang      Add a temporary buffer to fix fraca GMSE00180585 
//  06/12/2007  Raymond Shen    Add code to check fencing limit
//  08/18/2006  Vincent Yi      Moved the code of setting input postage into CMOS
//                              to this function after all postage validation checking
//  04/24/2006  James Wang      Extract business rules from this function.
//  11/25/2005  Vincent Yi      Code cleanup, added condition checking when
//                              the entry buffer is blank
//              Joe Mozdzer     Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkValidateHighValueWarnAmt (UINT8         bKeyCode, 
                                          T_SCREEN_ID   wNextScr1,
                                          T_SCREEN_ID   wNextScr2)
{
    double          dblAmtEntered;
    UINT32          ulPostageVal;
    SINT8       *   pCode = HIGHPOSTAGE_OP_CODE;    // Setting the High postage amount op Code 
    T_SCREEN_ID     usNext = 0;
    UINT16          usTempBuffer[MAX_FIELD_LEN+1]={0};
    UINT8           bIsLegal;
    
    if (fnIsBufferBlank() == TRUE)
    {
        // If entry buffer is blank, no screen change
        // warning message may need to display at the footer area
        return usNext;
    }

    /* if characters were typed, we need to convert the entry to our storage 
        format. if no characters were typed, we don't need to look at the entry 
        buffer at all since the value won't be changed.  (note that in that 
        instance the entry buffer will not contain anything anyway.  even though 
        a default amount may appear on the screen, it is not necessarily placed 
        in the buffer.) */
    if (fnCharsBuffered() > 0)
    {
        // Manipulate a temp buffer, not the input/display buffer, or it will mess up the screen.
        // refer to fnhkValidatePostageCorrectionAmount, turn the buffer contents into a double 
        memcpy( usTempBuffer, fnGetGenPurposeEbuf()->pBuf, (UINT16)fnBufferLen()*sizeof(UINT16) );
        fnUnicodeProcessImpliedDecimal(usTempBuffer, (UINT16)fnBufferLen());        
        dblAmtEntered = fnUnicode2Money(usTempBuffer);

        // Fix fraca 180584: per Clarisa, fencing limits do not apply when setting the 
        //  high-value warning threshold.  When a postage is entered, it is checked 
        //  against EITHER the fencing limits OR the high-value warning threshold.
        // if(fnFlashGetByteParm(BP_ADD_POSTAGE_MODE ) == ADD_POSTAGE_ALLOWED_FENCINGLIMIT)
        // {
        //    if( fnCheckPostCorrectionVal( dblAmtEntered) == PosInValid )
        //    {
        //        fnClearEntryBuffer();
        //        fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
        //        return usNext;
        //    }
        //}
        // 
    
		if ((dblAmtEntered == 0) ||
		    (fnCheckLegalPostage(dblAmtEntered, &bIsLegal) != SUCCESSFUL))
        {
            return usNext;
        }

        /* make sure the threshold is a valid postage value */
        if (bIsLegal != POSTAGE_OK)
        {
            if( bIsLegal == MAX_SETTABLE_VIOLATION)
            {
                usNext = wNextScr2;     // ErrMaxPostageExceeded screen
            }
            return usNext;
        }

        /* update the stored warning value in CMOS */
        CMOSSetupParams.lwHighValueWarning = (UINT32)dblAmtEntered;

        /* check if the current postage setting is now subject to the
            warning.  */
        if (fnValidData(BOBAMAT0, VAULT_DEBIT_VALUE, (void *) &ulPostageVal) != BOB_OK)
        {
            fnProcessSCMError();
            return usNext;
        }
        /* if it is higher, that function sets a flag which later causes the warning
            to be displayed.  We must also set the postage to 0 so there is
            a fallback value in case the user does not decide to reconfirm. */
        if (fnCheckHighPostage((double) ulPostageVal) == TRUE &&
            fnOITGetPrintMode() == PMODE_MANUAL)
        {
            UINT32  lwMinSettablePostage = 0;

            if (fnValidData(BOBAMAT0, VLT_DEBOPTS_FILE_MIN_DEB_VAL, 
                            (void *) &lwMinSettablePostage) != BOB_OK)
            {
                fnProcessSCMError();
                return usNext;
            }

            /* set baseline postage to whatever is configured as the minimum settable postage.
                eventually this will be replaced with the normal preset postage amount.  */
            if (fnWriteData(BOBAMAT0, VAULT_DEBIT_VALUE, (void *) &lwMinSettablePostage) != BOB_OK)
            {
                fnProcessSCMError();
                return usNext;
            }
            else
            {
                // Indicate who just told Bob to set the debit value
                //TODO JAH Remove dcrules.c fnDCAP_SetWhoCalled(OIJ_NORMAL_POSTAGE_CALLEDME);
            }
        }       
    } // if (fnCharsBuffered() > 0)

    // If enabled update the Technical Supervision Log
    //TODO JAH Remove dcrules.c if( fnDCAPMaintainTechData() == TRUE )
    //TODO JAH Remove dcrules.c {
    //TODO JAH Remove dcrules.c     (void)fnDCAP_PM_UpdateTechSuperLog( DCAP_UPOTHER_OP, pCode);
    //TODO JAH Remove dcrules.c }

    usNext = wNextScr1;

    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkValidateLowFundsWarnAmt
//
// DESCRIPTION: 
//      Hard key function that validates and stores a new low funds warning 
//      amount.
//
// INPUTS:
//      Standard hard key function inputs (format 1).
//
// OUTPUTS:
//      None
//
// RETURNS:
//      always wNextScr1
//
// MODIFICATION HISTORY:
//              Joe Mozdzer     Initial version
//  11/25/2005  Vincent Yi      Code cleanup, added condition checking when
//                              the entry buffer is blank
//  05/19/2006  James Wang      To validate low postage value warning amount 
//                              with a new BR function, and replace hard-coded
//                              value with "#define" statement.
// *************************************************************************/
T_SCREEN_ID fnhkValidateLowFundsWarnAmt (UINT8      bKeyCode, 
                                          T_SCREEN_ID   wNextScr1,
                                          T_SCREEN_ID   wNextScr2)
{
    UINT8           pMoneyPtr[SPARK_MONEY_SIZE];
    UINT8           bIsLegal;
    double          dblAmtEntered;
    double          dblMaxDR;
    T_SCREEN_ID     usNext = 0;
    SINT8        *  pCode = LOWPOSTAGE_OP_CODE; //Low funds setting op Code

    if (fnIsBufferBlank() == TRUE)
    {
        // If entry buffer is blank, no screen change
        // warning message may need to display at the footer area
        return usNext;
    }

    /* if characters were typed, we need to convert the entry to our storage 
        format. if no characters were typed, we don't need to look at the 
        entry buffer at all since the value won't be changed.  (note that in 
        that instance the entry buffer will not contain anything anyway.  even 
        though a default amount may appear on the screen, it is not necessarily 
        placed in the buffer.) */
    if (fnCharsBuffered() > 0)
    {
        fnEntryBufferSnapshotCapture();
        //fnUnicodeProcessImpliedDecimal(rGenPurposeEbuf.pBuf, fnBufferLen());  
        dblAmtEntered = fnUnicode2Money(fnGetGenPurposeEbuf()->pBuf);
        fnEntryBufferSnapshotRestore();

        /* get the maximum decending register value from vault */
        if (fnValidData(BOBAMAT0, VLT_PURSE_MAX_DESC_REG, (void *) pMoneyPtr) == BOB_OK)
        {
            dblMaxDR = fnBinFive2Double(pMoneyPtr);
        }
        else
        {
            fnProcessSCMError();
            return usNext;
        }

        /* check if value typed exceeds max descending register value or the max that we
            can store in a long */
        if ((dblAmtEntered > MAX_DESC_REG_VAL) || (dblAmtEntered > dblMaxDR))
        {
            return usNext;
        }

        CMOSSetupParams.lwLowFundsWarning = (UINT32)dblAmtEntered;
    }

    // If enabled update the Technical Supervision Log
    //TODO JAH Remove dcrules.c if( fnDCAPMaintainTechData() == TRUE )
    //TODO JAH Remove dcrules.c {
    //TODO JAH Remove dcrules.c     (void)fnDCAP_PM_UpdateTechSuperLog( DCAP_UPOTHER_OP, pCode);
    //TODO JAH Remove dcrules.c }

    usNext = wNextScr1;

    /* reset the low funds warning flags */
    fLowFundsWarningPending = FALSE;
    fLowFundsWarningDisplayed = FALSE;

    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkValidateLowFundsWarnAmt
//
// DESCRIPTION: 
//      Hard key function that checks the access codes to service and engineering mode. 
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None
//
// NEXT SCREEN:
//      Service options screen or not change screen
//
// MODIFICATION HISTORY:
//              Victor Li       Initial version
//  03/21/2005  Raymond Shen    Code cleanup
//
// *************************************************************************/
#ifdef COLOR_LCD_BACKDOOR_ENABLED
extern unsigned long lwDefaultFG;   // temp to see different color combinations
extern unsigned long lwDefaultBG;
extern unsigned long lwDefaultFooterFG;
extern unsigned long lwDefaultFooterBG;
extern unsigned long lwScreenBG;
void fnLCDShowPaletteEpson480x272( void );
#endif
T_SCREEN_ID fnhkCheckEngrServiceModeCodes( UINT8        bKeyCode,
                                            UINT8       bNumNext,
                                            T_SCREEN_ID     *pNextScreens)
{
    T_SCREEN_ID     usNext = 0;

    UINT16          *pUnicodeServPassword;
    UINT32          i;
    UINT32          j;
    BOOL            bServiceCodeFound     = FALSE;
    BOOL            bEngrServiceCodeFound = FALSE;
    ENTRY_BUFFER    *pGenPurposeEbuf = fnGetGenPurposeEbuf();
    
    static UINT8  cEngrServiceCodeSecretPW[ LOCK_CODE_LEN + 1] = {"3869"};

    if (fnCharsBuffered() == SERVICE_MODE_PASSWORD_LEN)
    {
        // get the service mode entry password from flash
        pUnicodeServPassword = fnFlashGetUnicodeStringParm(USP_SERVICE_MODE_ENTRY_PASSWORD);
        if (pUnicodeServPassword && (unistrlen(pUnicodeServPassword) == SERVICE_MODE_PASSWORD_LEN))
        {

			// walk through the input buffer and check if the password is in there somewhere.  we can't just assume
			//  it will be at the start of the entry buffer because we don't know the alignment.
			i = 0;
			bServiceCodeFound = FALSE;
			while (i <= fnBufferLen() - SERVICE_MODE_PASSWORD_LEN)
			{
				j = 0;
				while ((pGenPurposeEbuf->pBuf[i + j] == pUnicodeServPassword[j]) && (j < SERVICE_MODE_PASSWORD_LEN))
				{
					j++;
				}

				if (j == SERVICE_MODE_PASSWORD_LEN)
				{
					bServiceCodeFound     = TRUE;
					bEngrServiceCodeFound = FALSE;
					break;
				}
				i++;
			}
        }

        // If Service Code not found, then check for Engr. Service Code.
        if ( bServiceCodeFound  ==  FALSE )
        {
            i = 0;
            bServiceCodeFound = FALSE;
            while (i <= fnBufferLen() - LOCK_CODE_LEN)
            {
                j = 0;
                while ((pGenPurposeEbuf->pBuf[i + j] == cEngrServiceCodeSecretPW[j]) && (j < LOCK_CODE_LEN))
                {
                    j++;
                }

                if (j == LOCK_CODE_LEN)
                {
                    bEngrServiceCodeFound = TRUE;
                    bServiceCodeFound     = FALSE;
                    break;
                }
                i++;
            }
        }

        // if either Service Code found, then go to next screen 1, else not change screen
        if ( ( bServiceCodeFound == TRUE )  ||  ( bEngrServiceCodeFound == TRUE ) )
        {
            fnClearEntryBuffer();
            // If Engr Service Code found, then set the Special Engr. Mode Flag!
            if ( bEngrServiceCodeFound == TRUE )
            {
                //bEngrMode = TRUE;
            }
            // Otherwise, we are in the simple boreing Service Mode!
            else
            {
                //bEngrMode = FALSE;
            }

            usNext = pNextScreens[0];
        }
        else
        {
#ifdef COLOR_LCD_BACKDOOR_ENABLED
            (void) fnUnicode2Bin(pGenPurposeEbuf->pBuf, &j, fnBufferLen());
            if ((j / 1000) == 7)
            {
                if (j == 7777)
                {
                    fnLCDShowPaletteEpson480x272( );
                    fnKeypadWaitKey();
                }
                else
                {
                    // flip current foreground and background colors if value starts with 7
                    i = lwDefaultFG;
                    lwDefaultFG = lwDefaultBG;
                    lwDefaultBG = i;
                    lwScreenBG = lwDefaultBG;
                }
            }
            if ((j / 1000) == 8)
            {
                // set foreground if value starts with 8
                i = (unsigned char) (j % 1000);
                lwDefaultFG = i;
            }
            if ((j / 1000) == 9)
            {
                // set background if value starts with 9
                i = (unsigned char) (j % 1000);
                lwDefaultBG = i;
                lwScreenBG = i;
            }
            if ((j / 1000) == 4)
            {
                // set footer area foreground if value starts with 4
                i = (unsigned char) (j % 1000);
                lwDefaultFooterFG = i;
            }
            if ((j / 1000) == 5)
            {
                // set footer area background if value starts with 5
                i = (unsigned char) (j % 1000);
                lwDefaultFooterBG = i;
            }

#endif
        }
    }

    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkSetupHighPostage
//
// DESCRIPTION: 
//      Hard key function that checks if high value setup is supported.
//      
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None
//
// NEXT SCREEN:
//      High value setting screen (pNextScreen[0]) if high value supported.
//      Same screen otherwise (KIP mode is zero postage only).
//
// MODIFICATION HISTORY:
//  08/20/2007  Oscar Wang      Update for KIP Not Allowed case.
//  29 Mar 2007 Bill Herring    Initial
//
// *************************************************************************/
T_SCREEN_ID fnhkSetupHighPostage( UINT8             bKeyCode,
                                  UINT8             bNumNext,
                                  T_SCREEN_ID       *pNextScreens)
{
    T_SCREEN_ID usNext = 0;
    UINT8   ucKIPMode = fnGetKIPMode();

    if ( (ucKIPMode != KIP_ZERO_ONLY_W_PASSWORD )
            && (ucKIPMode != KIP_NOT_ALLOWED))
    {
        usNext = pNextScreens[0];
    }

    return usNext;
}


/* *************************************************************************
// FUNCTION NAME:       fnhkValidatePostageCorrectionAmount
// DESCRIPTION: 
//      Hard key function that validates the postaqge correction amount.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
// NEXT SCREENS:
//      [0] = MMPostageCorrectionReady
//      [1] = ErrorInsufficentFunds
// RETURNS:
//      Next Screen:
//      - If the postage is invalid, stay on current screen and put up Invalid 
//      message.
//      - If the postage is fine, but there is not enough money in the meter, 
//      for this postage value, go to the ErrorInsufficentFunds screen (NextScreen[1])
//      - Otherwise, if the postage is fine, go to the MMPostageCorrectionReady 
//      screen (NextScreen[0])
//
// WARNINGS/NOTES:
// 1. If the Postage cannot be validated due to problems with PSD data, called 
//    function MAY divert to error screen.  Otherwise, stay on this screen. It's
//    not a great solution, but that is what is done in other screens.  
//
// MODIFICATION HISTORY:
// 2011.08.09 Clarisa Bellamy - Fix fraca 207174: after calling function to
//                          check if the postage was legal, this function only 
//                          checked whether or not the tests could be performed
//                          not the actual results of the tests. 
//  03/16/2007           Andy Mo        Initial version
// *************************************************************************/
T_SCREEN_ID fnhkValidatePostageCorrectionAmount( UINT8           bKeyCode, 
                                                 UINT8           bNumNext, 
                                                 T_SCREEN_ID *   pNextScreens)
{
    double          dblAmtEntered;
    UINT32          ulPV;
    UINT16      *   pEntryBuf = fnGetGenPurposeEbuf()->pBuf;  
    T_SCREEN_ID     usNext = 0;
    UINT8           bIsLegal;
    const UINT16         pPeriodOnly[] = { //"."
    0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x002E, 0x0000
        };
    UINT16          usTempBuffer[MAX_FIELD_LEN+1]={0};

    // if nothing in buffer, or only period in buffer
    if( (fnIsBufferBlank() == TRUE)
        ||( unistrcmp(pEntryBuf, pPeriodOnly) == 0))
    {
        // Clear the entry buffer
        fnClearEntryBuffer();
        // Set the entry message to invalid entry, no screen change
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
        return usNext;
    }


    /* turn the buffer contents into a double */
    memcpy(usTempBuffer,pEntryBuf,(UINT16)fnBufferLen()*sizeof(UINT16));
    fnUnicodeProcessImpliedDecimal(usTempBuffer, (UINT16)fnBufferLen());    
    dblAmtEntered = fnUnicode2Money(usTempBuffer);

    /* Check if the amount is valid*/

    if(dblAmtEntered == 0)   //zero postage is disallowed 
    {
        // Clear the entry buffer
        fnClearEntryBuffer();   
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
        return usNext;
    }
        
    
    if(fnFlashGetByteParm(BP_ADD_POSTAGE_MODE ) == ADD_POSTAGE_ALLOWED_FENCINGLIMIT)
    {
        if( fnCheckPostCorrectionVal( dblAmtEntered) == PosInValid )
        {
            fnClearEntryBuffer();   
            fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
            return usNext;
        }
    }

    if( fnCheckLegalPostage(dblAmtEntered, &bIsLegal) != SUCCESSFUL )
    {
        // There was an error in the PSD data used to check the postage against.
        return( usNext );
    }
    if( bIsLegal != POSTAGE_OK )
    {
        // If the postage is not a legal postage for this meter, stay on this 
        //  screen and put up INVALID ENTRY message.
        fnClearEntryBuffer();   
        fnSetEntryErrorCode( ENTRY_INVALID_VALUE ); 
        return( usNext );
    }

    if(fnCheckInsufficientFundsPostage(dblAmtEntered))
    {
        usNext= pNextScreens[1];      //insufficient screen.
        fnSetErrReturnScreen(GetScreen(SCREEN_MM_INDICIA_READY));
        return usNext;
    }


        
/* The amount is valid,save the amount and  goes to Postage correction ready screen*/
    ulPV = (UINT32)dblAmtEntered;
    if (smgr_set_setting_data(SETTING_ITEM_POSTAGE, &ulPV, sizeof(UINT32)) != NU_SUCCESS)
    {
        return usNext;
    }   
    // go fetch indicia info stuff again
    (void)fnRateSetNonRateMode();

    usNext = pNextScreens[0];
    
    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkValidateManifestAmount
//
// DESCRIPTION: 
//      Hard key function that validate the manifest amount.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      goes Next screen 1 - manifest ready screen
//      stay current screen - amount is invalid
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  04/23/2007           Andy Mo        Initial version
// *************************************************************************/
T_SCREEN_ID fnhkValidateManifestAmount   (  UINT8           bKeyCode, 
                                            UINT8           bNumNext, 
                                            T_SCREEN_ID *   pNextScreens)
{
    double dblAmtEntered=0;
    UINT32 lwPV;
    T_SCREEN_ID  wNext=0;
    UINT16      *   pEntryBuf = fnGetGenPurposeEbuf()->pBuf;  
    UINT16  usTempBuffer[MAX_FIELD_LEN+1]={0};

    if(fnCharsBuffered()==0)
    {
        fnClearEntryBuffer();
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE);
        return wNext; 
        
    }
    /* turn the buffer contents into a double */
    memcpy(usTempBuffer,pEntryBuf,(UINT16)fnBufferLen()*sizeof(UINT16));
    fnUnicodeProcessImpliedDecimal(usTempBuffer, fnBufferLen());    
    dblAmtEntered = fnUnicode2Money(usTempBuffer);

    if(dblAmtEntered == 0)      //0 postage is disallowed
    {
        fnClearEntryBuffer();
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
        return wNext;
    }
    lwPV = (UINT32) dblAmtEntered;
    if(fnCheckManifestAmount(lwPV)==TRUE)
    {
        if (smgr_set_setting_data(SETTING_ITEM_POSTAGE, &lwPV, sizeof(UINT32)) == NU_SUCCESS)
            wNext = pNextScreens[0];        //Home screen        
    } 
    else         //Invalid amount
    {
        fnClearEntryBuffer();
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
        return wNext;
    }

    // go fetch indicia info stuff again
    (void)fnRateSetNonRateMode();

    return wNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkSetZeroPostageValue
//
// DESCRIPTION: 
//      Hard key function that simulates entry of zero postage.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// OUTPUTS:
//      None
//
// RETURNS:
//      NEXT SCREEN:  The next screen returned is one of the following:
//
//      next screen 0 -  MMIndiciaReady     (USA)           KIP_ALLOWED         1
//      next screen 1 -  ErrHighPostageExceeded *high postage warning
//      next screen 2 -  ErrInvalidPostage  *invalid postage
//      next screen 3 -  ErrKIPNotAllowed   (Australia)     KIP_NOT_ALLOWED         0   
//      next screen 4 -  KIPClassRequired   (Holland)   
//      next screen 5 -  KIPConfirmAmount   (Holland)       KIP_ALLOWED_VALIDATION      3
//      next screen 5 -  KIPConfirmAmount   (Italy)         KIP_ALLOWED_USE_MDS_TOKENS  6
//      next screen 6 -  KIPEnterPassword   (France)        KIP_ALLOWED_PASSWORD        2
//
// MODIFICATION HISTORY:
//  19 Jun 2007 Bill Herring    wInitial
//  22 Oct 2007 Martin O'Brien  Modified to include support Swiss style Zero Print
//  29 Nov 2007 Martin O'Brien  Now recognises French-style KIP as well as Swiss-style
// *************************************************************************/
T_SCREEN_ID fnhkSetZeroPostageValue( UINT8          bKeyCode, 
                                     UINT8          bNumNext, 
                                     T_SCREEN_ID   *pNextScreens)
{

    unsigned long pv = 0;    /* postage value */
    T_SCREEN_ID   next = 0;  /* next screen, 0 is current, altered by folowing code */


    /* check for French-style Zero Postage first */
    if ( fnIsZeroPrintByKIPwPasswordSupported() )
    {
        BOOL bClearBuf;
        next = SetUpKIPPiece(0.0, &bClearBuf, bNumNext, pNextScreens);
    }
    else if ( fnCheckForZeroPrint() )
    {   /* check if Swiss-style Zero Postage is available */

        /* indicate zero postage is allowed */
        fnSetZeroPostage();

        /* set print mode to Key-in Postage */
        fnOITSetPrintMode(PMODE_MANUAL);

        /* set the indicium to zero value postage */
        if ( fnWriteData(BOBAMAT0, VAULT_DEBIT_VALUE, (void *) &pv) != BOB_OK )
        {
            fnProcessSCMError();
            next = 0;          /* make it explicit rather than rely on init. value */
        }
        else
            next = pNextScreens[0];  /* MMIndiciaReady */
    }

    return next;
}



/* Event functions */
/* *************************************************************************
// FUNCTION NAME: 
//      fnevValidateEdmPostage
//
// DESCRIPTION: 
//      Event function that validates and stores a postage value
//              received from an external pb232 device.  A response message
//              is sent to the EDM task, since it is assumed that only a
//              set postage request message can trigger this function.  Note
//              that the indicia selection may be affected by the new value.//
// INPUTS:
//      Standard event function inputs.
//
// OUTPUTS:
//      None
//
// NEXT SCREEN:
//      Always goes to wNextScr1 (unless there is a system level error)
//
// MODIFICATION HISTORY:
//              Joe Mozdzer     Initial version
//  07/03/2006  Raymond Shen    Code cleanup
//
// *************************************************************************/
T_SCREEN_ID fnevValidateEdmPostage(T_SCREEN_ID wNextScr1,
                                    T_SCREEN_ID wNextScr2,
                                    INTERTASK *pIntertask)
{
    UINT32  lwPV;       /* postage value in long format for bob */
    UINT8   pMsgBuf[2]; /* data returned in the intertask response to bob */
    UINT8   pDR[SPARK_MONEY_SIZE];
    UINT8   *pLowValueIndiciaThreshold;
    double          dblRemotePostageProposal;
    UINT8   *pMaxSettable;
    UINT8   bIsLegal;

    pMsgBuf[0] = (UINT8) SUCCESSFUL;    /* the first data byte is the success/fail status
                                                    of the set postage operation- init to success */
    pMsgBuf[1] = 0;                             /* the second byte is the reason for failure, which
                                                    is not valid unless the first byte is FAILURE,
                                                    but initialize it anyway to be safe */

    /* extract the proposed postage value from the message */
    dblRemotePostageProposal = fnBinFive2Double(&(pIntertask->IntertaskUnion.bByteData[0]));


    /* check if it is a legal postage value */
    if (fnCheckLegalPostage(dblRemotePostageProposal, &bIsLegal) != SUCCESSFUL)
        return (0);

    switch (bIsLegal)
    {
        case FIXED_ZERO_VIOLATION:
            pMsgBuf[0] = (UINT8) FAILURE;
            pMsgBuf[1] = (UINT8) OIT_SPR_FIXED_ZERO_VIOLATION;
            goto send_rsp_and_xit;
        case BANK_RESTR_VIOLATION:
            pMsgBuf[0] = (UINT8) FAILURE;
            pMsgBuf[1] = (UINT8) OIT_SPR_BANK_RESTR_VIOLATION;
            goto send_rsp_and_xit;
        case MAX_SETTABLE_VIOLATION:
            pMsgBuf[0] = (UINT8) FAILURE;
            pMsgBuf[1] = (UINT8) OIT_SPR_EXCEEDED_MAX_ALLOWABLE;
            goto send_rsp_and_xit;
        default:
            break;
    }


    /* check that there are sufficient funds in the descending register */
    if (fnValidData(BOBAMAT0, GET_VLT_DESCENDING_REG, (void *) pDR) != BOB_OK)
    {
        fnProcessSCMError();
        return (0);     /* stay on this screen if we have a meter error to avoid 
                            executing screen change/post function code that could cause
                            further problems */
    }
    if (dblRemotePostageProposal > fnBinFive2Double(pDR))
    {
        /* if there are insufficient funds, we still set the postage value even
            though there is an error.  this is to accomodate the scale protocol. */
        pMsgBuf[0] = (UINT8) FAILURE;
        pMsgBuf[1] = (UINT8) OIT_SPR_INSUFFICIENT_FUNDS;
    }
    else
    {
        /* check if value subject to the high value warning.  we still set the postage
            value, as with the insufficient funds.  */
        if( dblRemotePostageProposal >= (double) CMOSSetupParams.lwHighValueWarning )
        {
            pMsgBuf[0] = (UINT8) FAILURE;
            pMsgBuf[1] = (UINT8) OIT_SPR_HIGH_VALUE;
        }
    }
    
    /* if we get here, the value must be ok, so store the value */
    lwPV = (UINT32) dblRemotePostageProposal;
    if( fnWriteData(BOBAMAT0, VAULT_DEBIT_VALUE, (void *) &lwPV) != BOB_OK )
    {
        fnProcessSCMError();
        return (0);
    }

    /* now select the appropriate indicia.  we will take care of informing the bob task of the
        change later during the post function. */
/* G900_TEMP Raymond Shen
    pLowValueIndiciaThreshold = fnFlashGetMoneyParm(MP_LOW_VALUE_INDICIA_THRESHOLD);
    if (dblRemotePostageProposal < fnBinFive2Double(pLowValueIndiciaThreshold))
        rNewImageSelections.bIndicia = S_LOW_VALUE_INDICIA;
    else
        rNewImageSelections.bIndicia = S_NORMAL_INDICIA;
*/

send_rsp_and_xit:
    return( wNextScr1 );
}


/* Variable graphic functions */



/**********************************************************************
        Private Functions
**********************************************************************/

/* *************************************************************************
// FUNCTION NAME: 
//      fnUnicodeAddPostageSymbol
//
// DESCRIPTION: 
//      Adds the postage symbol to the beginning and end of a Unicode string.
//
// INPUTS:
//      pUnicode  - pointer to a Unicode string.  it must be pre-malloced
//                      to allow room for the minus sign to be added.
//      bInputLen - Initial length of the Unicode string.
//      usSymbol  - The symbol to be added
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      The length after adding symbols
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Joe Mozdzer     Initial version
//  2/23/2006   Vincent Yi      Code cleanup
//
// *************************************************************************/
static UINT8 fnUnicodeAddPostageSymbol(UNICHAR *    pUnicode, 
                                       UINT8        bInputLen, 
                                       UNICHAR      usSymbol)
{
    UINT8       ucMode;
    UINT8       ucSubMode;

    fnGetSysMode(&ucMode, &ucSubMode);
    
    if (ucMode == OIT_PRINT_READY)
    {
        memmove(pUnicode + 1, pUnicode, bInputLen * sizeof(UNICHAR));
        pUnicode[0] = usSymbol;
        pUnicode[bInputLen + 1] = usSymbol;
        pUnicode[bInputLen + 2] = 0;

        return (bInputLen + 2);
    }

    return (bInputLen);
}
