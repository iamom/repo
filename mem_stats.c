#if 0
#define __mem_stats_c__

#include <stdio.h>
#include "commontypes.h"
#include "mem_stats.h"

#define GET_CURRENT_32K_TICK (*(uint32_t *)0x4804003C)

#define INSTRUMENTED_MEMSET

#ifdef INSTRUMENTED_MEMSET
extern void *memset_asm(void *s,int c, size_t n);
#endif

#define INSTRUMENTED_MEMCPY

#ifdef INSTRUMENTED_MEMCPY
extern void *memcpy_asm(void *s,int c, size_t n);
#endif

void init_stats(mem_stats_t *stats)
{
    int x = sizeof(mem_stats_t);
    uint8_t *ptr = (uint8_t *)stats;

    while(x-->0)
        *ptr++ = 0;
    
    stats->initialized = true;
}

void update_memcpy_stats(uint32_t dst, uint32_t src, uint32_t size, uint32_t ticks)
{
    int size_idx = size > 0 ? __builtin_clzl(size) : 32;
    int src_idx;
    int dst_idx;

    memcpy_stats.size_bucket_count[size_idx]++;
    memcpy_stats.size_bucket_totbytes[size_idx] += size;
    memcpy_stats.size_bucket_duration[size_idx] += ticks;

    src = src & 0x07F;
    src_idx = (src > 0 ? __builtin_ctzl(src) : 6);
    dst = dst & 0x7F;
    dst_idx = (dst > 0 ? __builtin_ctzl(dst) : 6);
    memcpy_stats.alignment_bucket_count[src_idx][dst_idx]++;
    memcpy_stats.alignment_bucket_totbytes[src_idx][dst_idx]+= size;
    memcpy_stats.alignment_bucket_duration[src_idx][dst_idx]+= ticks;
}

void update_memset_stats(uint32_t dst, uint32_t size, uint32_t ticks)
{
    int size_idx = size > 0 ? __builtin_clzl(size) : 32;
    int src_idx;
    int dst_idx;
    
    memset_stats.size_bucket_count[size_idx]++;
    memset_stats.size_bucket_totbytes[size_idx] += size;
    memset_stats.size_bucket_duration[size_idx] += ticks;

    src_idx = 6;
    dst_idx = (dst > 0 ? __builtin_ctzl(dst & 0x7F) : 6);
    memset_stats.alignment_bucket_count[src_idx][dst_idx]++;
    memset_stats.alignment_bucket_totbytes[src_idx][dst_idx]+= size;
    memset_stats.alignment_bucket_duration[src_idx][dst_idx]+= ticks;
}


void *memset(void *s,int c, size_t n)
{
    uint32_t start = GET_CURRENT_32K_TICK;
#ifdef INSTRUMENTED_MEMSET
    memset_asm(s, c, n);
#else
    unsigned char  val = (unsigned char)  c;
    unsigned char *ptr = (unsigned char*) s;

    unsigned char *end = ptr + n;

    while(ptr < end)
        *ptr++ = val;
#endif

    update_memset_stats((uint32_t) s, n, GET_CURRENT_32K_TICK - start);
    return(s);
}


void *memcpy(void *s1, const void *s2, size_t n)
{
    uint32_t start = GET_CURRENT_32K_TICK;
#ifdef INSTRUMENTED_MEMCPY
    memcpy_asm(s1, s2, n);
#else
    unsigned char *dst = (unsigned char*) s1;
    unsigned char *src = (unsigned char*) s2;

    unsigned char *end = src + n;

    while(src < end)
        *dst++ = *src++;
#endif
    update_memcpy_stats((uint32_t) s1, (uint32_t) s2, n, GET_CURRENT_32K_TICK - start);
    return(s1);
}

static void dump_stats(mem_stats_t *stats, const char *prompt)
{
    int i,j;
    char alignment_stats[30];

    printf(prompt);

    printf("size            count    tot bytes  total duration      throughput\n");
    printf("-------------   -------  ---------  ------------------  ----------\n");
    for(i=0; i< NELEMENTS(stats->size_bucket_count); i++)
    {
        printf("%s  %7lu   %7lu  %7lu\n", size_bucket_description[i], 
                                 stats->size_bucket_count[i], 
                                 stats->size_bucket_totbytes[i], 
                                 stats->size_bucket_duration[i]  );
    }
    printf("\n");

    printf("Alignment  destination\n");
    printf("source     ");   for(i=0; i<7; i++) printf("     %s        ", alignment_bucket_description[i]);

    printf("\n---------  "); for(i=0; i<7; i++) printf("--------------------  ");
    printf("\n");
    for(i=0; i<7; i++)
    {
        printf("%s  ", alignment_bucket_description[i]);
        for(j=0; j<7; j++)
        {
            sprintf(alignment_stats, "%5lu/%8lu/%5lu", 
                                       stats->alignment_bucket_count[i][j], 
                                       stats->alignment_bucket_totbytes[i][j],
                                       stats->alignment_bucket_duration[i][j]);
            printf("%-20s  ", alignment_stats);
        }
        printf("\n");
    }

    printf("\n");
}

void dump_memfunc_stats()
{
    dump_stats(&memset_stats, "memset stats:\n");
    dump_stats(&memcpy_stats, "memcpy stats:\n");
}

#endif
