/************************************************************************
*   PROJECT:        DM300c/400c
*   MODULE NAME:    keypad.c 
*   
*   DESCRIPTION:    Task entry point & other functions to support the 
*                   keypad driver task.
*
* ----------------------------------------------------------------------
*               Copyright (c) 2005 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
* REVISION HISTORY:
*
*  2009.07.13 Clarisa.bellamy - FPHX 2.0  - 
*  - LINT: Replace empty argument lists with void argument lists.
*
* Nov 14 2007 - Oscar Wang
* Support more special characters.
*
* May 24 2007 - Oscar Wang
* Support Mega-like barcode scanner.
* 
* Mar 09 2007 - Simon Fox
* Added fnDecSepScanCode.
* 
* Nov 14 2005 li003do
* Initial revision.
* 
*************************************************************************/


#ifndef NONUC
#include "pbos.h"
#include "global.h"
#include "pbioext.h"
#else
#include "commontypes.h"
#endif
#include "keypad.h"
#include "cmpublic.h"
#include "oit.h"
//#include "Oiscanner.h"
#include "hal.h"

typedef unsigned char SENSETYPE;
typedef unsigned char SCANTYPE;

extern unsigned char contFlag;

//TODO - this whole file should be removed once OIT, which has dependencies on functions in here, is removed/refactored
//Replaced by HAL
//static unsigned char    *pKeypadScan = (unsigned char *) &PCDR; /* Pointer to memory mapped output port for keypad scan */
//static unsigned char *pKeypadSense = (unsigned char *) &PDDR;   /* Pointer to memory mapped input port for keypad sense */
// unsigned char    *pCmdPort = (unsigned char *) &PFDR; /* Input port F has the non-matrix keys */

static unsigned char    bDelayCount = 8;    /* controls the delay time required between asserting a key scan line and       reading a sense line.  */


/* task global declarations */
static unsigned char    aCurrentKeyMatrix[9];   /* current state of each key */
static unsigned char    aLastKeyMatrix[9];      /* previous state of each key */
static unsigned char    bCurrentNonMatrixKeys = 0xFF;   /* current state of the non-matrix keys */
static unsigned char    bLastNonMatrixKeys = 0xFF;      /* previous state of the non-matrix keys */
static BOOL             fKeysEnabled = FALSE;
static char             dbgBuf[SYSLOG_MAX_DATA];

/* 255 means unused */
//#ifdef PHOENIX
// const unsigned char  pCometShiftKeyCodes[] = { 0x72, 0x80, 255, 255 };       
//#else
// const unsigned char  pCometShiftKeyCodes[] = { 0x80, 0x81, 255, 255 };  
//#endif
//                                          symbol, shift
const unsigned char pCometShiftKeyCodes[] = { 0x86, 0x85, 255, 255 };

const unsigned char     pCometNumKeyCodes[] = { 0x72, 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x70 }; /* starts w/ 0 */
const unsigned char     pCometDigitGroup[]  = { 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x70, 0x72, END_KEY_GROUP }; /* starts w/ 1 */
const unsigned char     pCometQwertyGroup[] = { 0x21, 0x37, 0x31, 0x27, 0x25, 0x32, 0x35, 0x40, 0x44, 0x43,
                            0x46, 0x50, 0x45, 0x42, 0x47, 0x51, 0x20, 0x30, 0x24, 0x33,
                            0x41, 0x34, 0x22, 0x26, 0x36, 0x23, END_KEY_GROUP }; /* should start w/ A if using
                                                        S menu library functions */
const unsigned char     KeypadDot           = 0x73; // Scan code for the decimal seperator (sometimes a dot, sometimes a comma) on the numeric keypad


unsigned char   *pNumKeyCodes = 0x0000;
unsigned char   *pShiftKeyCodes = 0x0000;
unsigned char   *aDigitGroup = 0x0000;
unsigned char   *aQwertyGroup = 0x0000;

#define MAXTESTSCAN 64

unsigned short testScan1[] = { 0x021e,
                   0x021f,
                   0x0220,
                   0x0221,
                   0x0222,
                   0x0223,
                   0x0224,
                   0x0236,
                   0x0226,
                   0x0227,
                   0x002c,
                   0x022e,
                   0x022f,
                   0x0230,
                   0x0231,
                   0x0233,
                   0x0234,
                   0x0238,
                               0x0000 };
unsigned long pKeyDownTraceBuffer[256];
unsigned char bKeyTraceBufferLoc;
unsigned short testScanBuffer[ MAXTESTSCAN ];
unsigned short *initTestScan = ( unsigned short *) NULL;
unsigned short *curTestScan = ( unsigned short *) NULL;

//unsigned char scanKeycodeBuff[SCANBUFSIZE];
//unsigned char scanKeyModifierBuff[SCANBUFSIZE];
//unsigned char scanAsciiBuff[SCANBUFSIZE];
unsigned int scanIndex=0;

#define ALPHA_UNICODE_TABLE_LENGTH 16
typedef unsigned char alternate_keys[ALPHA_UNICODE_TABLE_LENGTH];
const unsigned char pDummySpecialUnicode[] = { 0x00, '1', 0x00, ',', 0x00, '*', 0x00, '.', 0x00, ':', 0x00, '$',
                           0x00, '-', 0x00, '?', 0x00, '/', 0x00, '\\', 0x00, '(', 0x00, ')',
                           0x00, '!', 0x00, '@', 0x00, '+', 0x00, '#', 0x00, '^', 0x00, '=',
                           0x00, '&', 0x00, '%', 0x00, '_', 0x00, '"', 0x00, 0x00 };
const alternate_keys pDummyUnicodeTable[] = {
  /* upper case */
  { 0x00, '2', 0x00, 'A', 0x00, 'B', 0x00, 'C', 0x00, 0x00 },
  { 0x00, '3', 0x00, 'D', 0x00, 'E', 0x00, 'F', 0x00, 0x00 },
  { 0x00, '4', 0x00, 'G', 0x00, 'H', 0x00, 'I', 0x00, 0x00 },
  { 0x00, '5', 0x00, 'J', 0x00, 'K', 0x00, 'L', 0x00, 0x00 },
  { 0x00, '6', 0x00, 'M', 0x00, 'N', 0x00, 'O', 0x00, 0x00 },
  { 0x00, '7', 0x00, 'P', 0x00, 'Q', 0x00, 'R', 0x00, 'S', 0x00, 0x00 },
  { 0x00, '8', 0x00, 'T', 0x00, 'U', 0x00, 'V', 0x00, 0x00 },
  { 0x00, '9', 0x00, 'W', 0x00, 'X', 0x00, 'Y', 0x00, 'Z', 0x00, 0x00 },
  /* lower case */
  { 0x00, '2', 0x00, 'a', 0x00, 'b', 0x00, 'c', 0x00, 0x00 },
  { 0x00, '3', 0x00, 'd', 0x00, 'e', 0x00, 'f', 0x00, 0x00 },
  { 0x00, '4', 0x00, 'g', 0x00, 'h', 0x00, 'i', 0x00, 0x00 },
  { 0x00, '5', 0x00, 'j', 0x00, 'k', 0x00, 'l', 0x00, 0x00 },
  { 0x00, '6', 0x00, 'm', 0x00, 'n', 0x00, 'o', 0x00, 0x00 },
  { 0x00, '7', 0x00, 'p', 0x00, 'q', 0x00, 'r', 0x00, 's', 0x00, 0x00 },
  { 0x00, '8', 0x00, 't', 0x00, 'u', 0x00, 'v', 0x00, 0x00 },
  { 0x00, '9', 0x00, 'w', 0x00, 'x', 0x00, 'y', 0x00, 'z', 0x00, 0x00 }
};
unsigned char *pAlphaSpecialUnicode = 0x0000;
alternate_keys *pAlphaUnicodeTable = 0x0000;

/* private function prototypes */
static void fnDoKeyScan( void );
static void fnInitializeKeyInfo( void );
static unsigned char fnGetShiftStatus( void );

unsigned long ledPat[] = {
  1,
  2,
  4,
  8,
  0x10,
  0x20,
  0x40,
  0x80,
  0x40,
  0x20,
  0x10,
  0x8,
  0x4,
  0x2,
  0x1,
  2,
  4,
  8,
  0x10,
  0x20,
  0x40,
  0x80,
  0xc0,
  0xe0,
  0xf0,
  0xf8,
  0xfc,
  0xfe,
  0xff,
  0,
  0x81,
  0x42,
  0x24,
  0x18,
  0x24,
  0x42,
  0x81,
  0,
  0x80,
  0x40,
  0x20,
  0x10,
  0x8,
  0x4,
  0x2,
  0x1,
  0x2,
  0x4,
  0x8,
  0x10,
  0x20,
  0x40,
  0x80,
  0xc3,
  0x3c,
  0xc3,
  0x3c,
  0xc3,
  0x3c,
  0xc3,
  0x3c,
  0xc3,
  0x3c,
  0x55,
  0xaa,
  0x55,
  0xaa,
  0x55,
  0xaa,
  0
};

const unsigned char ScannerReMap[] = {
  /*              0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F */
  /* 00 */       00,  00,  00,  00,  33,  55,  49,  39,  37,  50,  53,  64,  68,  67,  70,  80,
  /* 10 */       69,  66,  71,  81,  32,  48,  36,  51,  65,  52,  34,  38,  54,  35,  96,  97,
  /* 20 */       98,  99, 100, 101, 102, 103, 112,0x72, 116,  23, 113,  00,0x52,0x33,  0x21,0x41,
  /* 30 */     0x72,0x60,0x61,0x52,0x43,0x52,0x40,0x73,0x41,0x70,   4,   3,   2,0x21,   7,  16,
  /* 40 */       17,  18,  19,  20,  21,  22,  00,  00,  00,  00, 118,  00,  00,  00,  00,  00,
  /* 50 */       00,   6,   5,  00,  00,   0,   0,  00,   0,   0,   0,   0,   0,   0,   0,   0,
  /* 60 */        0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  /* 70 */        0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  /* 80 */        0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  /* 90 */        0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  /* A0 */        0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  /* B0 */        0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  /* C0 */        0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  /* D0 */        0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  /* E0 */        0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  /* F0 */        0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0 };

//Generic one: for barcode scanner key convert
const unsigned char ScannerAutoSymbolReMap[] = {
  /*              0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F */
  /* 00 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 10 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  0x44,0x47,
  /* 20 */       0x22,0x25,0x30,0x52,0x52,0x24,0x27,0x32,0x24,0x27,0x24,0x35,0x44,0x36,0x35,0x46,
  /* 30 */     0x32,0x44,0x47,0x51,0x52,0x52,0x00,0x73,0x46,0x00,0x51,0x51,0x00,0x35,0x00,0x46,
  /* 40 */       0x47,00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 50 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  0x36,
  /* 60 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 70 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 80 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 90 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* A0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* B0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* C0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* D0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* E0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* F0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00 };
const unsigned char ScannerUnShiftSymbol[] = {
  /*              0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F */
  /* 00 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 10 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 20 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  01,  01,  01,
  /* 30 */       00,  00,  00,  00,  01,  00,  01,  00,  01,  00,  00,  00,  00,  01,  00,  00,
  /* 40 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 50 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 60 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 70 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 80 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 90 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* A0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* B0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* C0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* D0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* E0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* F0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00 };
const unsigned char ScannerAsciiReMap[] = {
  /*              0    1    2    3    4    5    6    7    8    9    A    B    C  D    E    F */
/* use upper case character as Mega */
  /* 00 */     0x00,0x00,0x00,0x00,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,
  /* 10 */     0x4d,0x4e,0x4f,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,0x31,0x32,
  /* 20 */     0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x30,0x0d,0x27,0x2A,0x09,0x20,0x2D,0x3d,0x2F,
  /* 30 */     0x20,0x35,0x37,0x3b,0x27,0x60,0x2c,0x2e,0x2f,0x39,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F,
  /* 40 */     0xA5,0x36,0x38,0x30,0x44,0x45,0x48,0x42,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,
  /* 50 */     0xA2,0x33,0x39,0x2E,0x0D,0x52,0x4A,0x4E,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,
  /* 60 */     0x31,0xA5,0xA7,0xA0,0x50,0x54,0x4B,0x4D,0x68,0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,
  /* 70 */     0x32,0xA8,0xAB,0x73,0x55,0x59,0x4C,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0x7F,
  /* 80 */     0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,
  /* 90 */     0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,
  /* A0 */     0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,
  /* B0 */     0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,
  /* C0 */     0xC0,0xC1,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,0xC8,0xC9,0xCA,0xCB,0xCC,0xCD,0xCE,0xCF,
  /* D0 */     0xD0,0xD1,0xD2,0xD3,0xD4,0xD5,0xD6,0xD7,0xD8,0xD9,0xDA,0xDB,0xDC,0xDD,0xDE,0xDF,
  /* E0 */     0xE0,0xE1,0xE2,0xE3,0xE4,0xE5,0xE6,0xE7,0xE8,0xE9,0xEA,0xEB,0xEC,0xED,0xEE,0xEF,
  /* F0 */     0xF0,0xF1,0xF2,0xF3,0xF4,0xF5,0xF6,0xF7,0xF8,0xF9,0xFA,0xFB,0xFC,0xFD,0xFE,0xFF };

//for barcode scanner string convert
const unsigned char ScannerAutoSymbol2ASCIIReMap[] = {
  /*              0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F */
  /* 00 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 10 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  0x21,0x40,
  /* 20 */     0x23,0x24,0x25,0x5e,0x26,0x2a,0x28,0x29,0x24,0x27,0x24,0x35,0x21,0x5f,0x2b,0x46,
  /* 30 */     0x32,0x44,0x47,0x3a,0x22,0x7e,0x00,0x00,0x3f,0x00,0x51,0x51,0x00,0x35,0x00,0x46,
  /* 40 */       0x47,00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 50 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  0x36,
  /* 60 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 70 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 80 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* 90 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* A0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* B0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* C0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* D0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* E0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,
  /* F0 */       00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00,  00 };

extern BOOL fnNeedBarcodeScannerString(void);

/* **********************************************************************
// FUNCTION NAME: fnInitializeKeyInfo
// DESCRIPTION:  Initializes global pointers to some scan code arrays needed by
//                  the keypad driver and user interface.  The arrays can
//                  vary by product and need to be conditionally initialized.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none used
// **********************************************************************/
static void fnInitializeKeyInfo( void )
{
  if ((pNumKeyCodes == 0x0000) || (pShiftKeyCodes == 0x0000) ||
      (aDigitGroup == 0x0000) || (aQwertyGroup == 0x0000))
    {
      /* COMET with official COMET screens */
      pNumKeyCodes = pCometNumKeyCodes;
      pShiftKeyCodes = pCometShiftKeyCodes;
      aDigitGroup = pCometDigitGroup;
      aQwertyGroup = pCometQwertyGroup;

    }

  // TEMP until EMD ready???
  pAlphaSpecialUnicode = pDummySpecialUnicode;
  pAlphaUnicodeTable = pDummyUnicodeTable;
    
  return;
}

/* **********************************************************************
// FUNCTION NAME: fnKeyExpire
// DESCRIPTION:  Timer expiration routine that invokes a keypad scan cycle.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none used
// **********************************************************************/
void fnKeyExpire(unsigned long wArg)
{
    
  fnDoKeyScan();
  return;
}


/* **********************************************************************
// FUNCTION NAME: fnEnableKeys
// DESCRIPTION:  Enables keypad scanning.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none
// **********************************************************************/
void fnEnableKeys( void )
{

//  register SCANTYPE     *pKeypadScanR = pKeypadScan;
//  register SENSETYPE    *pKeypadSenseR = pKeypadSense;
  unsigned char     bScan;
  unsigned char     i;
  UINT8             bHardwareMajorVersion;
  UINT8             bHardwareMinorVersion;


  fnInitializeKeyInfo();
  /* INITIALIZE THE CURRENT KEY STATE GRID.  Since we are doing this here, this effectively
     means that we will only detect changes to the state of the keypad after this function
     is called.  If a key was pressed while the key scan timer was disabled, and then held 
     down until this function was called to re-enable, a keypress will not be reported.  */

  /* Get the board revision and save it  */
    fnGetHardwareVersion(&bHardwareMajorVersion, &bHardwareMinorVersion);

//TODO - move into HAL and delete here
#if 0
  /* walk a zero through each scan line and read in the key sense lines, saving in the current
     state matrix */
      for (bScan = 0x80, i = 7; bScan > 0; bScan = bScan >> 1, i--)
        {     
          *pKeypadScanR = ~bScan;
        if (bHardwareMajorVersion < FP_MAIN_BD_HW_VERSION2)
            aCurrentKeyMatrix[i] = *pKeypadSenseR;
        else
            aCurrentKeyMatrix[i] = (PDDR & 0x3F) | ((PEDR & 0x0C) << 4);
    }
    *pKeypadScanR = 0xFF;

    /* the shift keys go in byte 8 of the matrix, 3 except for version 2 hardware rev DC only */
    if ((bHardwareMajorVersion != FP_MAIN_BD_HW_VERSION2) || (bHardwareMinorVersion != FP_HW_VERSION2_DC_VAL))
      aCurrentKeyMatrix[8] = 0x9F | *pCmdPort;
    else
      aCurrentKeyMatrix[8] = 0x9F | ((PEDR & 0x03) << 5);
#endif
    HALInitKeyMatrix(aCurrentKeyMatrix);

#ifndef NONUC
  OSStartTimer(KEY_SCAN_TIMER);
#endif
  /* update the global status flag */
  fKeysEnabled = TRUE;

  return;
}


/* **********************************************************************
// FUNCTION NAME: fnDisableKeys
// DESCRIPTION:  Disables keypad scanning.  All keypresses are ignored.
//                  There is no buffering of keypresses while in this state.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none
// **********************************************************************/
void fnDisableKeys( void )
{
#ifndef NONUC
  OSStopTimer(KEY_SCAN_TIMER);
#endif
  /* update the global status flag */
  fKeysEnabled = FALSE;

  return;
}

/* **********************************************************************
// FUNCTION NAME: fnAreKeysEnabled
// DESCRIPTION:  Checks if the keypad is currently enabled.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none
// RETURNS:  TRUE if keys are enabled; FALSE if they are not
// **********************************************************************/
BOOL fnAreKeysEnabled( void )
{
  return(fKeysEnabled);
}


/* **********************************************************************
// FUNCTION NAME: fnKeypadDriverTask
// DESCRIPTION: The OS entry point for the keypad driver task.  This task no  
//              longer does anything because the key scan is driven directly
//              from the timer, which makes the keypad much more responsive.
//              The task was left in in case we need to go back to the 
//              original task-based implementation due to timing issues.
//              Once we verify that the debit cycle is working properly at
//              the correct throughput, it should be ok to remove the task.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none used
// *************************************************************************/
void fnKeypadDriverTask (unsigned long argc, void *argv) 
{
    OSSuspendTask(KEYTASK);
}


/* **********************************************************************
// FUNCTION NAME: fnDoKeyScan
// DESCRIPTION: Scans the keypad, detects any key state changes from the last   
//              scan, and reports the changes with message(s) to the OIT task.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none
// *************************************************************************/
static void fnDoKeyScan( void ) 
{
//  register SCANTYPE *pKeypadScanR = pKeypadScan;
//  register SENSETYPE    *pKeypadSenseR = pKeypadSense;
  unsigned char     bScan, i, j, bLast;
  unsigned char     bChange, bWhichBit;
  KEYMSG rKeydataMsg;
  UINT8             bHardwareMajorVersion;
  UINT8             bHardwareMinorVersion;

  static BOOL keyBufInit = FALSE;
  MonitorPowerButtonForExtendedOffPress();
  if (keyBufInit == FALSE)
    {
      keyBufInit = TRUE;

      memset(pKeyDownTraceBuffer, 0, 256);
      bKeyTraceBufferLoc = 0;

      /* initialize last key matrix to none pressed */
      for (i = 0; i < 9; i++)
    {
      aLastKeyMatrix[i] = 0xFF;
    }
    }

  /* Get the board revision and save it  */
  fnGetHardwareVersion(&bHardwareMajorVersion, &bHardwareMinorVersion);
//TODO - move into HAL and delete here
#if 0
    /* walk a zero through each scan line and read in the key sense lines, saving in the current
    state matrix */
        for (bScan = 0x80, i = 7; bScan > 0; bScan = bScan >> 1, i--)
        {
            *pKeypadScanR = ~bScan;
            for (j = 0; j < bDelayCount; j++)
            {
                bLast += (bScan >> j) + 1;  /* this loop increases the scan time and
                                               should fix crosstalk problems per electronics group */
            }
        if (bHardwareMajorVersion < FP_MAIN_BD_HW_VERSION2)
            aCurrentKeyMatrix[i] = *pKeypadSenseR;
        else
            aCurrentKeyMatrix[i] = (PDDR & 0x3F) | ((PEDR & 0x0C) << 4);

    }
    *pKeypadScanR = 0xFF;

    /* the shift keys go in byte 8 of the matrix, 3 except for version 2 hardware rev DC only */
    if ((bHardwareMajorVersion != FP_MAIN_BD_HW_VERSION2) || (bHardwareMinorVersion != FP_HW_VERSION2_DC_VAL))
      aCurrentKeyMatrix[8] = 0x9F | *pCmdPort;
    else
      aCurrentKeyMatrix[8] = 0x9F | ((PEDR & 0x03) << 5);
#endif
    HALGetKeyMatrix(aCurrentKeyMatrix);

  /* key state change detection/response loop */
  for (i = 0; i < 9; i++)
    {
      /* get the current key data and previous key data for a bank of 8 keys */
      bScan = aCurrentKeyMatrix[i];
      bLast = aLastKeyMatrix[i];

      /* update the previous key data while the info is handy */
      aLastKeyMatrix[i] = bScan;

      /* check if a change is detected in any of the keys */
      if (bScan != bLast)
    {
      /* determine which bits are different */
      bChange = bScan ^ bLast;  

      /* this is the current bit we are checking for a change; initialize to bit 0 */
      bWhichBit = 0;

      /* process all of the changed bits.  once we process a bit it gets shifted out
         of the byte so we can end the loop when there's no more 1's in bChange.
         this is faster than always repeating 8 times. */
      while (bChange)
        {
          /* if the current rightmost bit is one that has changed, send
         the appropriate intertask message */
          if (bChange & 0x01)
        {
          /* first calculate the key code */
          rKeydataMsg.bKeyCode = (i << 4) + bWhichBit;


          /* determine if the changed key has been pressed or released */
          if (bScan & 0x01)
            rKeydataMsg.bDirection = KEYUP;
          else
            {
              rKeydataMsg.bDirection = KEYDOWN;

              // log the key to the trace buffer
              pKeyDownTraceBuffer[bKeyTraceBufferLoc++] = rKeydataMsg.bKeyCode;
            }

          /* get the shift key status for the control byte */

          rKeydataMsg.fControl = fnGetShiftStatus();

          /* convert comet keycode to spark keycode */

#ifndef NONUC
          OSSendIntertask(OIT, KEYTASK, KEY_RX_KEYDATA, BYTE_DATA,
                  &rKeydataMsg, sizeof(rKeydataMsg));
#endif
        }

          bScan = bScan >> 1;
          bChange = bChange >> 1;
          bWhichBit++;
        }

    }

    }

  return;
    
}

/* **********************************************************************
// FUNCTION NAME: fnGetShiftStatus
// DESCRIPTION: Provides a snapshot of the current state of all keys defined
//              as shift keys.  (Shift keys are defined by pShiftKeyCodes).
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none 
// RETURNS: byte containing the status of up to 4 shift keys:
//              b0: state of shift key 0 (1 = pressed, 0 = not pressed)
//              b1: state of shift key 1
//              b2: state of shift key 2
//              b3: state of shift key 3
//              b4-7: unused (0 is returned in these bits)
//              
// *************************************************************************/
static unsigned char fnGetShiftStatus( void ) 
{
  unsigned char bControlByte = 0;
  unsigned char bShiftKeycode;
  unsigned char bShiftCheckMask;
  unsigned long j;


  /* check if the bit in aCurrentKeyMatrix is low for the
     key designated as shift 0.  set the bit for shift 0 in
     the control byte if so. */
  bShiftKeycode = pShiftKeyCodes[SHIFT_KEYCODE_0];
  if (bShiftKeycode != 255)
    {
      j = bShiftKeycode >> 4;
      bShiftCheckMask = bShiftKeycode & 0x0F;
      bShiftCheckMask = 0x01 << bShiftCheckMask;
      if (!(aCurrentKeyMatrix[j] & bShiftCheckMask))
    bControlByte |= 0x01;
    }

  /* repeat the check for shift key 1 */
  bShiftKeycode = pShiftKeyCodes[SHIFT_KEYCODE_1];
  if (bShiftKeycode != 255)
    {
      j = bShiftKeycode >> 4;
      bShiftCheckMask = bShiftKeycode & 0x0F;
      bShiftCheckMask = 0x01 << bShiftCheckMask;
      if (!(aCurrentKeyMatrix[j] & bShiftCheckMask))
    bControlByte |= 0x02;
    }

  /* repeat the check for shift key 2 */
  bShiftKeycode = pShiftKeyCodes[SHIFT_KEYCODE_2];
  if (bShiftKeycode != 255)
    {
      j = bShiftKeycode >> 4;
      bShiftCheckMask = bShiftKeycode & 0x0F;
      bShiftCheckMask = 0x01 << bShiftCheckMask;
      if (!(aCurrentKeyMatrix[j] & bShiftCheckMask))
    bControlByte |= 0x04;
    }

  /* repeat the check for shift key 3 */
  bShiftKeycode = pShiftKeyCodes[SHIFT_KEYCODE_3];
  if (bShiftKeycode != 255)
    {
      j = bShiftKeycode >> 4;
      bShiftCheckMask = bShiftKeycode & 0x0F;
      bShiftCheckMask = 0x01 << bShiftCheckMask;
      if (!(aCurrentKeyMatrix[j] & bShiftCheckMask))
    bControlByte |= 0x08;
    }

  return (bControlByte);
}

/* **********************************************************************
// FUNCTION NAME: fnKeypadWaitKey
// DESCRIPTION: Wait for a Key to be Pressed
//
// AUTHOR: George Monroe/Joe Mozdzer
//
// INPUTS:  none used
// *************************************************************************/
unsigned short fnKeypadWaitKey( void )
{
//    register SCANTYPE   *pKeypadScanR = pKeypadScan;
//    register SENSETYPE  *pKeypadSenseR = pKeypadSense;
    unsigned char   bScan, i, bLast;
    unsigned char   bChange, bWhichBit;
    unsigned char   j, bShiftCheckMask;
    unsigned short code;
    struct {
        unsigned char   bKeyCode;
        unsigned char   bDirection;
        unsigned char   fControl;
    } rKeydataMsg;      /* struct for building the RX_KEYDATA message */
    UINT8               bHardwareMajorVersion;
    UINT8               bHardwareMinorVersion;


    fnInitializeKeyInfo();

    /* initialize the key scan lines */
    //TODO - move into HAL and delete here
//    *pKeypadScanR = 0xFF;
    HALInitKeyScanLines();

    /* initialize last key matrix to none pressed */
    for (i = 0; i < 9; i++)
    {
        aLastKeyMatrix[i] = 0xFF;
    }

    while (1)
    {

        /* Get the board revision and save it  */
        fnGetHardwareVersion(&bHardwareMajorVersion, &bHardwareMinorVersion);
//TODO - move into HAL and delete here
#if 0
        /* walk a zero through each scan line and read in the key sense lines, saving in the current
        state matrix */
            for (bScan = 0x80, i = 7; bScan > 0; bScan = bScan >> 1, i--)
            {
                *pKeypadScanR = ~bScan;
                for (j = 0; j < bDelayCount; j++)
                {
                    bLast += (bScan >> j) + 1;  /* this loop increases the scan time and
                                                   should fix crosstalk problems per electronics group */
                }
            if (bHardwareMajorVersion < FP_MAIN_BD_HW_VERSION2)
                aCurrentKeyMatrix[i] = *pKeypadSenseR;
            else
                aCurrentKeyMatrix[i] = (PDDR & 0x3F) | ((PEDR & 0x0C) << 4);

        }
        *pKeypadScanR = 0xFF;

        /* the shift keys go in byte 8 of the matrix, 3 except for version 2 hardware rev DC only */
        if ((bHardwareMajorVersion != FP_MAIN_BD_HW_VERSION2) || (bHardwareMinorVersion != FP_HW_VERSION2_DC_VAL))
          aCurrentKeyMatrix[8] = 0x9F | *pCmdPort;
        else
          aCurrentKeyMatrix[8] = 0x9F | ((PEDR & 0x03) << 5);
#endif
    HALGetKeyMatrix(aCurrentKeyMatrix);


        /* key state change detection/response loop */
        for (i = 0; i < 9; i++)
        {
            /* get the current key data and previous key data for a bank of 8 keys */
            bScan = aCurrentKeyMatrix[i];
            bLast = aLastKeyMatrix[i];

            /* update the previous key data while the info is handy */
            aLastKeyMatrix[i] = bScan;

            /* check if a change is detected in any of the keys */
            if (bScan != bLast)
            {
                /* determine which bits are different */
                bChange = bScan ^ bLast;

                /* this is the current bit we are checking for a change; initialize to bit 0 */
                bWhichBit = 0;

                /* process all of the changed bits.  once we process a bit it gets shifted out
                 of the byte so we can end the loop when there's no more 1's in bChange.
                 this is faster than always repeating 8 times. */
                while (bChange)
                {
                    /* if the current rightmost bit is one that has changed, send
                         the appropriate intertask message */
                    if (bChange & 0x01)
                    {
                        /* first calculate the key code */
                        rKeydataMsg.bKeyCode = (i << 4) + bWhichBit;


                        /* determine if the changed key has been pressed or released */
                        if (bScan & 0x01)
                            rKeydataMsg.bDirection = KEYUP;
                        else
                            rKeydataMsg.bDirection = KEYDOWN;


                        /* determine if the changed key is shifted.  start by
                         initializing the control byte to 0 (all unshifted) */
                        rKeydataMsg.fControl = 0;

                        /* check if the bit in aCurrentKeyMatrix is low for the
                         key designated as shift 0.  set the bit for shift 0 in
                         the control byte if so. */
                        if (pShiftKeyCodes[SHIFT_KEYCODE_0] != 255)
                        {
                            j = ((unsigned char) pShiftKeyCodes[SHIFT_KEYCODE_0]) >> 4;
                            bShiftCheckMask = ((unsigned char) pShiftKeyCodes[SHIFT_KEYCODE_0]) & 0x0F;
                            bShiftCheckMask = 0x01 << bShiftCheckMask;
                            if (!(aCurrentKeyMatrix[j] & bShiftCheckMask))
                                rKeydataMsg.fControl |= 0x01;
                        }

                        /* repeat for shift 1.  better to waste a little code
                         space instead of setting up a smaller but slower loop. */
                        if (pShiftKeyCodes[SHIFT_KEYCODE_1] != 255)
                        {
                            j = ((unsigned char) pShiftKeyCodes[SHIFT_KEYCODE_1]) >> 4;
                            bShiftCheckMask = ((unsigned char) pShiftKeyCodes[SHIFT_KEYCODE_1]) & 0x0F;
                            bShiftCheckMask = 0x01 << bShiftCheckMask;
                            if (!(aCurrentKeyMatrix[j] & bShiftCheckMask))
                                rKeydataMsg.fControl |= 0x02;
                        }

                        /* repeat for shift 2.  better to waste a little code
                         space instead of setting up a smaller but slower loop. */
                        if (pShiftKeyCodes[SHIFT_KEYCODE_2] != 255)
                        {
                            j = ((unsigned char) pShiftKeyCodes[SHIFT_KEYCODE_2]) >> 4;
                            bShiftCheckMask = ((unsigned char) pShiftKeyCodes[SHIFT_KEYCODE_2]) & 0x0F;
                            bShiftCheckMask = 0x01 << bShiftCheckMask;
                            if (!(aCurrentKeyMatrix[j] & bShiftCheckMask))
                                rKeydataMsg.fControl |= 0x04;
                        }

                        /* repeat for shift 3.  better to waste a little code
                         space instead of setting up a smaller but slower loop. */
                        if (pShiftKeyCodes[SHIFT_KEYCODE_3] != 255)
                        {
                            j = ((unsigned char) pShiftKeyCodes[SHIFT_KEYCODE_3]) >> 4;
                            bShiftCheckMask = ((unsigned char) pShiftKeyCodes[SHIFT_KEYCODE_3]) & 0x0F;
                            bShiftCheckMask = 0x01 << bShiftCheckMask;
                            if (!(aCurrentKeyMatrix[j] & bShiftCheckMask))
                                rKeydataMsg.fControl |= 0x08;
                        }

                        code = rKeydataMsg.fControl << 8 | rKeydataMsg.bKeyCode;
                        if(rKeydataMsg.bDirection == KEYUP )
                            return(code);
                    }
                    bScan = bScan >> 1;
                    bChange = bChange >> 1;
                    bWhichBit++;
                }
            }
        }
    }
}


/******************************************************************************
   MODULE       : fnGetShiftKeys
   AUTHOR       : Wes Kirschner
   DESCRIPTION  : This function returns the number of shift keys and shift keys in
                  a buffer that is passed in.  The buffer must be large enough to handle
                  the maximum number of shift keys.
   PARAMETERS   : *pBuf - Buffer for maximum number of shift keys
   RETURNS      : Actual Number of shift keys
******************************************************************************/

unsigned char fnGetShiftKeys (unsigned char *pBuf)
{
  unsigned char i = 0;      /* Loop Control Variable */

  /* If shift keys are initialized....*/
  if (pShiftKeyCodes != NULL)
    {
      /* While not at max num of shift keys and shift key is not unused */
      while ((i <= SHIFT_KEYCODE_3) && (*(pShiftKeyCodes + i) != 255))
    {
      /* Copy in the key */
      *(pBuf + i) = *(pShiftKeyCodes + i) ;

      /* Go to the next entry */
      i++;
    }
    }
  /* Return number of shift keys */
  return (i);
}


/******************************************************************************
   MODULE       : fnDecSepScanCode
   AUTHOR       : Simon Fox
   DESCRIPTION  : Returns the scan code of the decimal seperator key on the
                  numeric keypad
   PARAMETERS   : None
   RETURNS      : Scan code
******************************************************************************/
unsigned char fnDecSepScanCode(void)
{
    return KeypadDot;
}
