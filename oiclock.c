/************************************************************************
  PROJECT:        Future Phoenix
  COPYRIGHT:      2005, Pitney Bowes, Inc.
  AUTHOR:         
  MODULE NMAE:    OiClock.c

  DESCRIPTION:    This file contains all types of screen interface functions 
                  (hard key, event, pre, post, field init and field refresh)
                  and global variables pertaining to clock and date related
                  screens and fields.

 ----------------------------------------------------------------------
  MODIFICATION HISTORY:
  
 18-Sep-14 Renhao  on FPHX 02.12 cienet branch 
    Modified function fnhkDateAdvanceOneDay() to fix fraca 227272.
 
 18-Sep-14 Renhao  on FPHX 02.12 cienet branch 
    Modified function fnhkDateAdvValidEntered() to fix fraca 227231.
    
 18-Sep-14 Wang Biao on FPHX 02.12 cienet branch   
    Modified function fnhkDateAdvanceOneDay()& fnhkDateAdvanceEnteredDays() to
    fix fraca 227259.
    
 22-Jul-14 Renhao  on FPHX 02.12 cienet branch   
    Modified function fnhkDateAdvanceOneDay()& fnhkDateAdvanceEnteredDays() to put up 
    the "Date advance not allowed" screen when ANY of the rates/DCM files will expire.

 05-May-14 Renhao on FPHX 02.12 cienet branch 
    Modified function fnhkDateAdvanceOneDay()& fnhkDateAdvanceEnteredDays() for fraca
    225188 to prompt "Rate will expire" screen if all Rates will expireor all Dcap Modules 
    will expire.
  
 15-Apr-14 Wang Biao on FPHX 02.12 cienet branch
    Modified function fnhkAutoDateAdvUseDate() for the issue about wrong date 
    displayed in the main screen when press soft key to accept the auto advance date 
    changed after midnight.
    
 08-Apr-14 sa002pe on FPHX 02.12 shelton branch
	Changes for U.K. EIB:
	1. Changed fnfDataCaptureDate to not load the time if the specific time doesn't apply to
		when the next upload will be due/required.
 
   2014.04.03 Renhao           Added function fnfShowChangeDateMenu(),modified function
                               fnfViewPrintedDate(),fnpOptionAutoDateAdv(),fnhkDateAdvClearToZero()
                               fnhkValidateAutoDateAdvTime(),fnhkDateAdvanceEnteredDays(),
                               fnhkDateAdvanceOneDay() for G922 Rates Expiration  new feature.
   
   2012.09.18 Bob Li           Added fnChangeScreenTimerDuration(),fnRestoreScreenTimerDuration()to 
                               change and restore the screen timer duration.
   2012.09.05 Bob Li           Modified fnhkValidateSleepTimeout() & fnhkValidateNormalPresetTimer()
                               fix fraca GMSE00169484.
   2010.08.11 Clarisa Bellamy  Fraca 192992 (plus changes to oifpmain.c, clock.h, oiclock.h)
                               Added code to detect and record the date/time whenever a
                                user crosses midnight while adjusting the time (drift.)
                                Added new private variables and accessor funcitons, and 
                                modified fnhkValidateDriftTime and fnhkValidateClockDriftAndSaving.
   2010.05.10  Deborah Kohl     Modified all functions testing (bFieldCtrl & ALIGN_MASK) 
                                to add padding characters either when right aligned and
                                in LTR mode or left aligned and in RTL mode 
   2010.04.27  Jingwei,Li       Modified fnfUserCorrectDriftTime() to use utility function
                                fnCopyTableTextForRTL() to get table text for RTL.
   2010.04.21  Deborah Kohl     In functions fnBuildTime(),fnfUserCorrectDriftTime(),fnfRTCTime(), 
                                use fnCopyTableTextForRTL()to get the text without the extra padding 
                                characters introduced by screen generator when mirroring screens for
                                Right to Left writing
   2010.04.21  Jingwei,Li       Modified fnfUserTime() to use utility function fnCopyTableTextForRTL()
                                to get table text for RTL.
   2010.04.16  Jingwei,Li       Modified fnfUserTime() to trim the redundant spaces that are padded 
                                during screen mirror for RTL
   2009.07.23  Jingwei,Li       Modified fnhkValidateNormalPresetTimer(),fnhkValidateNormalPresetTimer()
                                fnhkValidateWaitForEnvTimeout() and fnhkValidateZWZPTimeout() to fix
                                fraca GMSE00165988.
   2009.02.19 Clarisa Bellamy   Removed prototype for fnCMOSRateCallback. It is in cmos.h now.
    07/25/2008  Raymond Shen    Merged function fnBuildTime() and fnShowAmPm() from Mega.
    02/29/2008  Joey Cui        Added fnpPrepServMainTimeFunctions(), fnfDHOnTimeInit(),
                                fnfDHOnTimeAmPm(), fnhkValidateDHOnTime() 
                                to implement DH On time setup feature 
    01/10/2008    Oscar Wang    Modified fnhkDateAdvUseNewRates() to fix FRACA GMSE00135544: 
                                since FP doesn't prefer to do rating by default class, we will 
                                clear current class/postage info when using new rating
    08/06/2007  Adam Liu        Add utility function fnIsDCAPDateAvailable() to fix GMSE00124601. 
    07/19/2007  Vincent Yi      Fixed lint errors
    18 Jul 2007 Simon Fox       Modified fnfDataCaptureDate to show date and time.
    05/09/2007  Raymond Shen    Modified fnhkDateAdvAddOneDay(), fnhkDateAdvValidEntered() and added
                                fnhkDateAdvanceOneDay(), fnhkDateAdvanceEnteredDays(), 
                                fnhkManualDateAdvUseDate(), fnfPreViewPrintedDate() to add 
                                manual date advance confirmation for French requirement.
   04/26/2007  Andy Mo         Added fnhkValidateDateCorrect(),fnhkDateCorrectionAddOneDay()
                               fnhkDateCorrectionResetToToday() to support Canada Date Correction mode.    
 17-Apr-07 sa002pe on FPHX 01.04 shelton branch
   Changed fnhkDateAdvAddOneDay & fnhkDateAdvValidEntered to use new EMD
   parameter BP_DATE_ADVANCE_TYPE.

 17-Apr-07 sa002pe on FPHX 01.04 shelton branch
   Added bIsCorrecteDate.
   There are several places in the K700 version of oiclock.c where this value is
   used, but I couldn't determine where to make the changes for FPHX.

    26 Mar 2007 Bill Herring    New fnhkValidateZWZPTimeout() to input ZWZP timeout.
                                New fnfZWZPTimeoutInit() to output ZWZP timeout.
                                Completed fnfDataCaptureDate() for next upload date.
    02/16/2007  Vincent Yi      Fixed fraca 114588, modified function fnhkDateAdvAddOneDay()
    02/13/2007  Oscar Wang      Modified function fnhkDateAdvValidEntered. Added
                                new rate effective date error screen.
    12/05/2006  Kan Jiang       Add function fnhkClearDSTUpdateFlag() for new feature
                                Automatic Daylight Saving.
    11/15/2006  Vincent Yi      Fixed fraca 106228, in functions fnfSystemDate,
                                fnfViewPrintedDate, fnfMidnightPrintedDate,
                                changed the input value of fCheckDuck to FALSE
                                when invoking function fnBuildDateFmt()
    09/06/2006  Vincent Yi      Removed function fnhkMidnightDateAdvUseDate()
    09/08/2006  Oscar Wang      Added function fnhkDateAdvUseNewRates for rate 
                                effective.
    09/06/2006  Vincent Yi      Added function fnhkMidnightDateAdvUseDate() for
                                midnight crossover screen
    09/05/2006   Kan Jiang      Modify following functions to fix Fraca 103435
                                fnpPrepSetPrintDate
                                fnfSystemDate 

    09/04/2006   Kan Jiang   Modify following functions to fix Fraca 103437
                             fnhkValidateDriftTime
                             fnCheckValidDriftTime 
    09/01/2006   Dicky Sun   Fix fraca GMSE00103434. We should refresh
                             all the fields in a screen even if it is
                             in hidden status. Modify the following functions:
                                fnfShow12HourClockText
                                fnfAutoDateAdvAmPm
                                fnfAmPmToggleFieldRefresh
    08/29/2006  Vincent Yi      Added functions fnfWaitForEnvTimeoutInit, 
                                fnhkValidateWaitForEnvTimeout for OptionTimeoutWaitForEnvelope
                                screen
    07/14/2006   Kan Jiang   Modify the problem found by Santity Check.
    07/12/2006   Kan Jiang   Modify function fnhkValidateAutoDateAdvTime(), 
                             add new funcitons fnhkAutoDateAdvUseDate(), 
                             fnhkAutoDateAdvDoNotUseDate(), fnfSystemDate() 
                             and fnfViewPrintedDate() for the new work flow 
                             of Auto Date Advance.
    06/28/2006  Raymond Shen    Added function fnfRTCTime, fnfRTCTimeAmPm.

 ----------------------------------------------------------------------
    OLD PVCS REVISION HISTORY
 * \main\jnus11.00_i1_shelton\2 cx17598 18 oct 2005
 *
 *  Fixed a minor problem.
 *
*************************************************************************/


/**********************************************************************
        Include Header Files
**********************************************************************/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "oiclock.h"
#include "oicommon.h"
#include "oit.h"
#include "clock.h"
#include "cometclock.h"
#include "cwrapper.h"
#include "fwrapper.h"
#include "bob.h"
#include "bobutils.h"
#include "oifields.h"
#include "Datdict.h"
#include "global.h"
#include "oientry.h"
#include "oicmenucontrol.h"
#include "features.h"
#include "oitpriv.h"
#include "extreports.h"
//#include "oipreset.h"
//#include "oirateservice.h"
#include "dcapi.h"
#include "flashutil.h"
#include "utils.h"

/**********************************************************************
        Local Function Prototypes
**********************************************************************/



/**********************************************************************
        Local Defines, Typedefs, and Variables
**********************************************************************/
//static    BOOL    fNormalPresetEnable = FALSE;
static short    sDateAdvDaysPendingRates;   /* temp container for number of days to date advance if user 
                                                        decides they want to update rates (applicable when date advance
                                                        amount crosses a rate effectivity date) */

static short    wTmpDateAdvanceAmount = 0;      /* the temp amount of days advanced for edit. note that 
                                                         this number can be positive or negative. */
static SINT8    bDateAdvanceLimit = 0;  // max settable date advance
static short    wOldDateAdvanceAmount = 0;     /* the variant storing the old advanced days for recovery aftertime */

static BOOL bAutoDateAdvDisabling = TRUE;

// count down timer display
static unsigned long lwTimeSyncCountDown = 0;

static DATETIME    stDTNewTime;

static    BOOL   fAutoDateAdvConfirmFlag = FALSE;

// -------------------
// This flag is set, and the time recorded, when the user adjusts the drift 
//  time so that the "current time" crosses midnight.  That may cause a "missed 
//  midnight" detection the next time the fnpIndiciaReadyCheck() function runs.  
//  This flag is cleared when the midnight processing is actually done, or when 
//  a missed midnight incident is reported.
// These are SUPER private:
static  BOOL    fDriftPastMidnightTemp = FALSE;  
static DATETIME stDTDriftPastMidnightTemp;
// The variables above are set when the time is entered by the user. 
// The variables below are set when they confirm the time change.  
// These are egular private, the can be accessed via accessor functions:
static  BOOL    fDriftPastMidnight = FALSE;  
static DATETIME stDTDriftPastMidnight;
// -------------------

BOOL bIsCorrecteDate = FALSE;

#define DATE_ADVANCE_CLEAR              0
#define DATE_ADVANCE_ADD_ONE_DAY        1
#define DATE_ADVANCE_ADD_DAYS           2
static UINT8 bManualDateAdvanceType = DATE_ADVANCE_CLEAR;

/**********************************************************************
        Global Functions Prototypes
**********************************************************************/
extern BOOL SetPrintedDateCorrectionDays( short NewDays );
extern UINT32 fnGetCMOSLongDHOnTime(void);
extern void fnSetCMOSLongDHOnTime(UINT32 ulValue);



/**********************************************************************
        Global Variables
**********************************************************************/
unsigned char   bDateAdvancePrompt = 0 ;
BOOL            bRateExpHasBeenPrompted = FALSE;
BOOL            fAllOldRatesExpNewRatesEff = FALSE;



extern CHOICE_FIELD     rGenPurposeBinChoiceField;
extern ENTRY_BUFFER     rGenPurposeEbuf; //VBL-TBD-FP
extern BOOL     fValidTimeOutValue;
extern BOOL AutoDateAdvanceActive;
extern BOOL     fCMOSDSTAutoUpdated;
extern DATETIME stMidnightTime;

extern BOOL CMOSRatesExpired;
extern BOOL bIsExistAnyActiveRates;
extern BOOL bIfExistAnyRates;
extern BOOL bIfExistAnyDcaps;
extern UINT8 bNumOfActiveRates;
extern BOOL bIfExpireAnyRates;
/* declarations */



/**********************************************************************
        Public Functions
**********************************************************************/
/* Utility functions */



/* *************************************************************************
// FUNCTION NAME:       fnGetDriftPastMidnightDT
// DESCRIPTION:
//         Sets the stDTBackPastMidnightDate to the current date, normalized. 
// INPUTS:
//         None.
// RETURNS:
//         None
// WARNINGS/NOTES: 
//
// MODIFICATION HISTORY:               
//  1020.08.11 Clarisa Bellamy - Initial version
// *************************************************************************/
void fnGetDriftPastMidnightDT( DATETIME *stDTDest ) 
{
    memcpy( stDTDest, &stDTDriftPastMidnight, sizeof( DATETIME ) );
    return;
}

/* *************************************************************************
// FUNCTION NAME:       fnClearDriftPastMidnightFlag
// DESCRIPTION:
//         Clears the fDriftPastMidnight flag. 
// INPUTS:
//         None - external functions can only clear this.
// RETURNS:
//         None
// WARNINGS/NOTES: 
//
// MODIFICATION HISTORY:               
//  1020.08.11 Clarisa Bellamy - Initial version
// *************************************************************************/
void fnClearDriftPastMidnightFlag( void ) 
{
    fDriftPastMidnightTemp = FALSE;
    fDriftPastMidnight = FALSE;
    return;
}

/* *************************************************************************
// FUNCTION NAME:   fnGetDriftPastMidnightFlag
// DESCRIPTION:
//         Return the value of the flag used to indicate that the drift value
//          was set past midnight.
// INPUTS:
//         None
// RETURNS:
//         fDriftPastMidnight
//
// WARNINGS/NOTES: 
//
// MODIFICATION HISTORY:               
//  1020.08.11 Clarisa Bellamy - Initial version
// *************************************************************************/
BOOL fnGetDriftPastMidnightFlag( void ) 
{
    return ( fDriftPastMidnight );
}

/* *************************************************************************
// FUNCTION NAME:  fnSetAutoDateAdvConfirmFlag
//
// DESCRIPTION:
//         This routine allows the confirmation of the auto date advance to be set. 
//
// INPUTS:
//         the new value of fAutoDateAdvConfirmFlag
//
// RETURNS:
//         None
//
// WARNINGS/NOTES: 
//
// MODIFICATION HISTORY:               
//     06/29/2006   Kan Jiang      Initial version
// *************************************************************************/
void fnSetAutoDateAdvConfirmFlag(BOOL  fFlag) 
{

    fAutoDateAdvConfirmFlag = fFlag;
    return;
}



/* *************************************************************************
// FUNCTION NAME:  fnGetAutoDateAdvConfirmFlag
//
// DESCRIPTION:
//         Return the value of the flag used to allow confirmation to be done later.
//
// INPUTS:
//         None
//
// RETURNS:
//         fAutoDateAdvConfirmFlag
//
// WARNINGS/NOTES: 
//
// MODIFICATION HISTORY:               
//     06/29/2006   Kan Jiang      Initial version
// *************************************************************************/
BOOL fnGetAutoDateAdvConfirmFlag(void) 
{
    return ( fAutoDateAdvConfirmFlag );
}



/* *************************************************************************
// FUNCTION NAME: fnDateDuckOff
// DESCRIPTION: Utility function that turns date ducking off so that the  
//              entire date gets printed.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  None
// *************************************************************************/
unsigned char fnDateDuckOff()
{
    unsigned short  wDDS = 0;

    if (fnValidData(BOBAMAT0, GET_INDICIA_PRINT_FLAGS, &wDDS) != SUCCESSFUL)
    {
        fnProcessSCMError();
        return (FAILURE);
    }

    wDDS = wDDS & ~(BOB_DUCK_ENTIRE | BOB_DUCK_DAY);
    if (fnWriteData(BOBAMAT0, SET_INDICIA_PRINT_FLAGS, &wDDS) != SUCCESSFUL)
    {
        fnProcessSCMError();
        return (FAILURE);
    }
    return (SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: fnDateDuckEntire
// DESCRIPTION: Utility function that ducks the entire date.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none
// *************************************************************************/
unsigned char fnDateDuckEntire()
{
    unsigned short  wDDS = 0;

    if (fnValidData(BOBAMAT0, GET_INDICIA_PRINT_FLAGS, &wDDS) != SUCCESSFUL)
    {
        fnProcessSCMError();
        return (FAILURE);
    }
    wDDS = wDDS & ~(BOB_DUCK_DAY);
    wDDS = wDDS | BOB_DUCK_ENTIRE;
    if (fnWriteData(BOBAMAT0, SET_INDICIA_PRINT_FLAGS, &wDDS) != SUCCESSFUL)
    {
        fnProcessSCMError();
        return (FAILURE);
    }

    return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: fnDateDuckDayOnly
// DESCRIPTION: Utility function that ducks only the day part of the date.
//              The month and year will still be printed.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none
// *************************************************************************/
unsigned char fnDateDuckDayOnly()
{
    unsigned short  wDDS = 0;

    if (fnValidData(BOBAMAT0, GET_INDICIA_PRINT_FLAGS, &wDDS) != SUCCESSFUL)
    {
        fnProcessSCMError();
        return (FAILURE);
    }
    wDDS = wDDS & ~(BOB_DUCK_ENTIRE);
    wDDS = wDDS | BOB_DUCK_DAY;
    if (fnWriteData(BOBAMAT0, SET_INDICIA_PRINT_FLAGS, &wDDS) != SUCCESSFUL)
    {
        fnProcessSCMError();
        return (FAILURE);
    }
    return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: fnMeterSetTownCircleDucking
// DESCRIPTION: Utility function that ducks the Town Circle Printing
//
// AUTHOR: Paul Hembrook
//
// INPUTS:  none
// *************************************************************************/
unsigned char fnMeterSetTownCircleDucking()
{
    unsigned short  wDuckBits = 0;

    if (fnValidData(BOBAMAT0, GET_INDICIA_PRINT_FLAGS, &wDuckBits) != SUCCESSFUL)
    {
        fnProcessSCMError();
        return (FAILURE);
    }

    // Set bit to 1 indicating Ducking on so Don't Print
    wDuckBits = wDuckBits | (BOB_DUCK_TC);  

    //inform Bob about the state change
    if (fnWriteData(BOBAMAT0, SET_INDICIA_PRINT_FLAGS, &wDuckBits) != SUCCESSFUL)
    {
        fnProcessSCMError();
        return (FAILURE);
    }

    return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: fnMeterSetTownCirclePrint
// DESCRIPTION: Utility function that allows the Town Circle Printing
//
// AUTHOR: Paul Hembrook
//
// INPUTS:  none
// *************************************************************************/
unsigned char fnMeterSetTownCirclePrint()
{
    unsigned short  wDuckBits = 0;

    if (fnValidData(BOBAMAT0, GET_INDICIA_PRINT_FLAGS, &wDuckBits) != SUCCESSFUL)
    {
        fnProcessSCMError();
        return (FAILURE);
    }

    // Set bit to 0 indicating Ducking off so Print
    wDuckBits = wDuckBits & ~(BOB_DUCK_TC);

    //inform Bob about the state change
    if (fnWriteData(BOBAMAT0, SET_INDICIA_PRINT_FLAGS, &wDuckBits) != SUCCESSFUL)
    {
        fnProcessSCMError();
        return (FAILURE);
    }

    return (SUCCESSFUL);
}
/***************************************************************************
// FUNCTION NAME: fnSetOldDateAdvanceAmount
// DESCRIPTION: Utility function to save old Date Advance Amount in case for restored.
//
// AUTHOR: David
// INPUTS:  None
// *************************************************************************/
void fnSaveOldDateAdvanceAmount()
{
    wOldDateAdvanceAmount = GetPrintedDateAdvanceDays();
}

/***************************************************************************
// FUNCTION NAME: fnGetOldDateAdvanceAmount
// DESCRIPTION: Utility function to return the old Date Advance Amount for restored.
//
// AUTHOR: David
// INPUTS:  None
// *************************************************************************/
short fnGetOldDateAdvanceAmount()
{
    return ( wOldDateAdvanceAmount );
}

//Reuse from JA1108.
BOOL fnIsDateAdvanceChanged ( void )
{

    BOOL retcode = false;
    DATETIME SysDateTimeBuff , PrintDateTimeBuff;
    unsigned long JulianSysDays, JulianPrintDays;

    if( GetSysDateTime(&SysDateTimeBuff, ADDOFFSETS, GREGORIAN)
        && GetPrintedDate(&PrintDateTimeBuff, GREGORIAN) ) 
    {
        JulianSysDays = MdyToJulian( &SysDateTimeBuff);
        JulianPrintDays = MdyToJulian( &PrintDateTimeBuff);

        if ( JulianSysDays != JulianPrintDays )
        {
           retcode = true;
        }
    }
    
    return(retcode);
}

/*******************************************************************/
/* FUNCTION NAME        : fnBuildTime()                            */
/* AUTHOR               : Mark Harris                              */
/* FUNCTION DESCRIPTION : Genralized function to builds a          */
/*                      : displayable time field; Defaults         */
/*                      : to display HH:MM, options to display     */
/*                      : HH:MM:SS and AM/PM                       */
/* RETURNS              : TRUE               */
/*******************************************************************/
BOOL fnBuildTime( DATETIME pTime, BOOL fShowSecs, BOOL fInsertTimeSep, unsigned short *pFieldDest, unsigned char bLen,
                  unsigned char bNumTableItems, unsigned char *pTableTextIDs)
{

    unsigned short  wTextID;
    BOOL            fDateStatus;
    unsigned long   i;
    char            pStr[9];
    int             iStrLen;
    UINT8     ubWritingDir;

    ubWritingDir = fnGetWritingDir();

    /* adjust for a 12 hour clock format, if applicable */
    if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
    {
        if (pTime.bHour >= 13)
            pTime.bHour -= 12;
        else if (pTime.bHour == 0)
                pTime.bHour = 12;
    }

    /* if it will fit in the field, add the hour */
    i = 0;

    if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
        iStrLen = sprintf(pStr, "%2d", pTime.bHour);
    else
        iStrLen = sprintf(pStr, "%02d", pTime.bHour);

    if ((i + iStrLen) <= bLen)
    {
        fnAscii2Unicode(pStr, pFieldDest + i, iStrLen);
        i += iStrLen;
    }

    /* if there is one, add the separation string that goes between the hours and minutes */
    if( fInsertTimeSep)
    {
        if (bNumTableItems > 0)
        {
        	EndianAwareCopy(&wTextID, pTableTextIDs, 2);
            if(ubWritingDir == RIGHT_TO_LEFT)
            {
               //trim the redundant spaces that are padded during screen mirror for RTL.
               i += fnCopyTableTextForRTL(pFieldDest + i, wTextID, bLen - i);  
            }
            else
            {
                i += fnCopyTableText(pFieldDest + i, wTextID, bLen - i);  
            }
        }
        else
        {
            *(pFieldDest + i) = fnFlashGetWordParm(WP_DISP_TIME_SEP_CHAR);
            i++;
        }
    }
    /* if it will fit in the field, add the minutes */
    iStrLen = sprintf(pStr, "%02d", pTime.bMinutes);
    if ((i + iStrLen) <= bLen)
    {
        fnAscii2Unicode(pStr, pFieldDest + i, iStrLen);
        i += iStrLen;
    }

    /* if there is one, add the string that goes after the minutes */
    if (bNumTableItems > 1 || fShowSecs )
    {
        if( fInsertTimeSep)
        {
            if( bNumTableItems > 1)
            {
            	EndianAwareCopy(&wTextID, pTableTextIDs + (1 * sizeof(unsigned short)), 2);
                i += fnCopyTableText(pFieldDest + i, wTextID, bLen - i);
            }
            else
            {
                *(pFieldDest + i) = fnFlashGetWordParm(WP_DISP_TIME_SEP_CHAR);
                i++;
            }
        }
        /* if it will fit in the field, add the seconds */
        iStrLen = sprintf(pStr, "%02d", pTime.bSeconds);
        if ((i + iStrLen) <= bLen)
        {
            fnAscii2Unicode(pStr, pFieldDest + i, iStrLen);
            i += iStrLen;
        }
    }
    return TRUE;

}

/*******************************************************************/
/* FUNCTION NAME        : fnShowAmPm()                         */
/* AUTHOR               : Mark Harris                              */
/* FUNCTION DESCRIPTION : Genralized function to display AM/PM or  */
/*                      : nothing for specific 24 Hour             */
/* TEXT TABLE USAGE:    : 1st string is the "AM" string,            *
/*                      : 2nd is the "PM" string
/* RETURNS              : TRUE                                     */
/*******************************************************************/
BOOL fnShowAmPm( unsigned char bHour,
                 unsigned short *pFieldDest, unsigned char bLen, unsigned char bFieldCtrl,
                 unsigned char bNumTableItems, unsigned char *pTableTextIDs)

{
    unsigned short  wTextID;


    if ( fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
    {
        /* check to make sure there are at least two table items, which are required for this field */
            if (bNumTableItems >= 2)
            {
                if (bHour <= 11)
                    /* "am" is the first string */
                	EndianAwareCopy(&wTextID, pTableTextIDs, 2);
                else
                    /* "pm" is the second one */
                	EndianAwareCopy(&wTextID, pTableTextIDs + sizeof(unsigned short), 2);

                fnCopyTableText(pFieldDest, wTextID, bLen);
            }
    }
    return TRUE;
}

//******************************************************************************
// Function Name: fnChangeScreenTimerDuration                                     
// Description:  This function changes the screen timer duration. 
//                                                                             
// AUTHOR: Tim Zhang		                                           
//                                                                             
// INPUTS:	lwMilliseconds - the new timer duration value.
// MODIFICATION HISTORY:
//  09/18/2012  Bob Li    Initial Build                                                                    
//******************************************************************************
void fnChangeScreenTimerDuration(unsigned long lwMilliseconds)
{

	OSStopTimer( SCREEN_CHANGE_TIMER );
	OSChangeTimerDuration(SCREEN_CHANGE_TIMER, lwMilliseconds);
	OSStartTimer( SCREEN_CHANGE_TIMER );
}

//******************************************************************************
// Function Name: fnRestoreScreenTimerDuration                                     
// Description:  This function changes the screen timer duration to default value which is 2 seconds. 
//                                                                             
// AUTHOR: Tim Zhang		                                           
//                                                                             
// INPUTS:	None.
// MODIFICATION HISTORY:
//  09/18/2012  Bob Li    Initial Build
//******************************************************************************
void fnRestoreScreenTimerDuration()
{

	OSStopTimer( SCREEN_CHANGE_TIMER );
	OSChangeTimerDuration(SCREEN_CHANGE_TIMER, 2 *SEC);
	OSStartTimer( SCREEN_CHANGE_TIMER );
}


/* Pre/post functions */
/* *************************************************************************
// FUNCTION NAME: 
//      fnpOptionAutoDateAdv
//
// DESCRIPTION: 
//      Pre function that sets the auto date advance disabling to TRUE.
//
// INPUTS:
//      Standard pre function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//                Henry Liu    Initial Build
//    02/13/2006  Kan Jiang    Code cleanup
//    04/03/2014  Renhao       Modified code for G922 Rates Expiration 
//                             new feature.
// *************************************************************************/
SINT8  fnpOptionAutoDateAdv( void        (** pContinuationFcn)(), 
                             UINT32        * pMsgsAwaited,
                             T_SCREEN_ID   * pNextScr )
{
	UINT16	wAutoAdvEMDParam = fnFlashGetWordParm(WP_AUTO_DATE_ADVANCE_OPTIONS);


	SET_MENU_LED_OFF();
    
    if (GetPrintedDateAdvanceDays() < 0)
    {
        SetPrintedDateAdvanceDays(0);
    }
	
	// If auto Advance is mandatory, shouldn't display "Disable" item.
    if(wAutoAdvEMDParam & AUTO_ADV_DATE_IS_MANDATORY)
    {
	    bAutoDateAdvDisabling = FALSE;
    }
	else
	{
	    bAutoDateAdvDisabling = TRUE; 
	}

    return (PREPOST_COMPLETE);
}



/* *************************************************************************
// FUNCTION NAME: fnpPrepSetPrintDate
//
// DESCRIPTION: 
//      Pre function that sets up the menu items for Set print date.
//
// INPUTS:
//      Standard pre function inputs.
//
// OUTPUTS:
//      Set next screen unchanged.
//
// RETURNS:
//      PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  09/05/2006  Kan Jiang   Modified for fixing Fraca 103435
//  04/17/2006  Kan Jiang   Initial version
// *************************************************************************/
SINT8 fnpPrepSetPrintDate( void (** pContinuationFcn)(),
                           UINT32 *pMsgsAwaited,
                           UINT16 *pNextScr )
{
    UINT32   ulSlotIndex = 0;
    SINT32  lInspectionDays = fnGetInspectionDays();
    
    static S_MENU_SETUP_ENTRY   pSetupMenuInitTable[] = 
    {
        // force menu table reset before defining new slots
        {   CLEAR_MENU_TABLE,       (UINT8) 0,    (UINT8) 0,   FALSE,   NULL_PTR },                                 
        /* Set to today's date*/
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 1,    (UINT8) 1,   TRUE,    NULL_PTR },
        /* Advance one day */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 2,    (UINT8) 2,   TRUE,    NULL_PTR},
        /* Specify days to advance */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 3,   TRUE,    NULL_PTR },
    
        {   END_MENU_TABLE_MARKER,  (UINT8) 0,    (UINT8) 0,   FALSE,   NULL_PTR }  
    } ;

 
    /* if the system date already reach the Inspection Day, then disable 
      "Advance one day" and "Specify days to advance" */    
    if ( lInspectionDays <= 0 ) 
    {
         pSetupMenuInitTable[2].bSlotActivity = OIC_SLOT_DISPLAY_ONLY;
         pSetupMenuInitTable[3].bSlotActivity = OIC_SLOT_DISPLAY_ONLY;
    }
    else
    {
         pSetupMenuInitTable[2].bSlotActivity = OIC_SLOT_ACTIVE;
         pSetupMenuInitTable[3].bSlotActivity = OIC_SLOT_ACTIVE;
    }
    
    // invoke generic slot initialization function with this menu initializer table
    ulSlotIndex = fnSetupMenuSlots( pSetupMenuInitTable, ulSlotIndex, (UINT8) 4 ) ;
    
    // Set the total number of entries which will be use for displaying scroll bar
    fnSetMenuEntriesTotalNumber (ulSlotIndex);
      
    // Update LED indicator
    fnUpdateLEDPageIndicator();
    *pNextScr = 0;  /* leave next screen unchanged */

    return ( PREPOST_COMPLETE );
}




/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrintOptionAdvDate
//
// DESCRIPTION: 
//      Pre function that is called when going to SetPrintDateAddDays screen
//
// INPUTS:
//      Standard pre function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//    07/14/2006  Kan Jiang    Modify the problem found by Santity Check.
//    06/29/2006  Kan Jiang    Modify according to new Nav Spec 
//    02/14/2006  Kan Jiang    Code cleanup
//                Henry Liu    Initial Build
// *************************************************************************/
SINT8  fnpPrintOptionAdvDate( void       (** pContinuationFcn)(), 
                             UINT32        * pMsgsAwaited,
                             T_SCREEN_ID   * pNextScr )
{
    wTmpDateAdvanceAmount = GetPrintedDateAdvanceDays();

     if ( wTmpDateAdvanceAmount > 0 ) 
    {
          wTmpDateAdvanceAmount = 0;
    }

    if( fnValidData( BOBAMAT0, VLT_INDI_DATEADV_LIMIT, &bDateAdvanceLimit ) != BOB_OK)
    {
        fnProcessSCMError();
    }

    
    return (PREPOST_COMPLETE);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnpPreDateCorrectionAddDays
//
// DESCRIPTION: 
//      Pre function that is called when going to MMDateCorrectionAddDays screen
//
// INPUTS:
//      Standard pre function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//04/25/2007    Andy Mo     Initial version
// *************************************************************************/
SINT8  fnpPreDateCorrectionAddDays( void         (** pContinuationFcn)(), 
                                    UINT32     * pMsgsAwaited,
                                    T_SCREEN_ID   * pNextScr )
{
    if( fnValidData( BOBAMAT0, VLT_INDI_DATEADV_LIMIT, &bDateAdvanceLimit ) != BOB_OK)
    {
        fnProcessSCMError();
    }

    
    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPrepServMainTimeFunctions
//
// DESCRIPTION: 
//      Pre function that sets up the menu items for Serv Mode Time Functions screen.
//
// INPUTS:
//      Standard screen pre function inputs.
//
// OUTPUTS:
//      *pNextScr = 0
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//      
// MODIFICATION HISTORY:
//  02/28/2008  Joey Cui        Initial version 
//
// *************************************************************************/
SINT8 fnpPrepServMainTimeFunctions( void        (** pContinuationFcn)(), 
                                    UINT32      *   pMsgsAwaited,
                                    T_SCREEN_ID     *   pNextScr )
{
    UINT32      ulSlotIndex = 0;
    UINT32      ulNextSlotIndex;

    static S_MENU_SETUP_ENTRY   pTimeFunctionsMenuInitTable[] = 
    {
        // force menu table reset before defining new slots
        {   CLEAR_MENU_TABLE,   (UINT8) 0,  (UINT8) 0,  OIC_SLOT_NOT_ACTIVE,  NULL_PTR },

        // Display number           Key           Label        Active   Conditional 
        // offset,                  assignment,   string id,   flag,    function
                                    
        /* View Current Settings */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 1,    (UINT8) 1,   OIC_SLOT_NOT_ACTIVE,    NULL_PTR},
        /* Change Time Zone Offset */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 2,    (UINT8) 2,   OIC_SLOT_NOT_ACTIVE,    NULL_PTR },
        /* Change DST Offset */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 3,    (UINT8) 3,   OIC_SLOT_NOT_ACTIVE,    NULL_PTR },
        /* Set up DH Time */
        {   USE_DEFAULT_ASSIGNMENT, (UINT8) 4,    (UINT8) 4,   OIC_SLOT_ACTIVE,    NULL_PTR },

        {   END_MENU_TABLE_MARKER,  (UINT8) 0,    (UINT8) 0,   OIC_SLOT_NOT_ACTIVE,   NULL_PTR }  
    } ;

    
    // invoke generic slot initialization function with this menu initializer table
    ulNextSlotIndex = fnSetupMenuSlots( pTimeFunctionsMenuInitTable, ulSlotIndex, 
                                        (UINT8) 4 ) ;
    
    // Set the total number of entries which will be use for displaying 
    // scroll bar
    fnSetMenuEntriesTotalNumber (ulNextSlotIndex - ulSlotIndex);

    // Update LED indicator
    fnUpdateLEDPageIndicator();
    *pNextScr = 0;      /* leave next screen unchanged */

    return (PREPOST_COMPLETE);
}

/* Field functions */
/* *************************************************************************
// FUNCTION NAME: 
//      fnfAutoDateAdvInit
//
// DESCRIPTION: 
//      Initializes the entry field where the current time of auto date 
//          advance setting.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//    06/29/2006  Kan Jiang    Modify according to new Nav Spec that default value is 12:00AM
//    02/13/2006  Kan Jiang    Code cleanup
//       Victor Li      Initial Build
// *************************************************************************/
BOOL fnfAutoDateAdvInit (UINT16    *pFieldDest, 
                         UINT8      bLen,   
                         UINT8      bFieldCtrl, 
                         UINT8      bNumTableItems, 
                         UINT8     *pTableTextIDs,
                         UINT8     *pAttributes)
{
    DATETIME  stDateTime;
    UINT32    ulClockval;
    UINT32    i;
    char     pStr[8];
    UNICHAR   pUniStr[8];
    SINT32    lStrLen;

    /* initialize the buffer to spaces */
    SET_SPACE_UNICODE(rGenPurposeEbuf.pBuf, bLen);
                    
    (void) fnfStdEntryInit(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, pAttributes);

    ulClockval = fnCMOSSetupGetCMOSSetupParams()->AutoAdvTime;

     if (ulClockval != 0)
     {
           fnTodSecsToDatetime(ulClockval, &stDateTime, FALSE);
     }
    else
    {
          if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
          {
              stDateTime.bHour = 12;
           stDateTime.bMinutes = 0;
          }
          else
      {
              stDateTime.bHour = 0;
           stDateTime.bMinutes = 0;
      }
    }

        /* adjust for a 12 hour clock format, if applicable */
        if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
        {
            if (stDateTime.bHour >= 13)
            {
                rGenPurposeBinChoiceField.bCurrentChoice = 1; // PM
                stDateTime.bHour -= 12;
            }
            else    
            {
                rGenPurposeBinChoiceField.bCurrentChoice = 0; // AM
                if (stDateTime.bHour == 0)
                    stDateTime.bHour = 12;
            }
        }
        else
        {
            rGenPurposeBinChoiceField.bCurrentChoice = 0;
        }

        /* if it will fit in the field, add the hour */
        lStrLen = sprintf(pStr, "%d", stDateTime.bHour);
        fnAscii2Unicode(pStr, pUniStr, lStrLen);
        for(i = 0; i < lStrLen; i++)
        {
            fnBufferUnicode(pUniStr[i]);
        }

    
        /* if it will fit in the field, add the minutes */
        lStrLen = sprintf(pStr, "%02d", stDateTime.bMinutes);
        fnAscii2Unicode(pStr, pUniStr, lStrLen);
        for(i = 0; i < lStrLen; i++)
        {
            fnBufferUnicode(pUniStr[i]);
        }
//  }

    return (fnfStdEntryRefresh(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, pAttributes));
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnfAutoDateAdvStringSelect
//
// DESCRIPTION: 
//      Output field function for disabling or continuing to set the
//      auto date advance time.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Victor Li       Initial Build
// *************************************************************************/
BOOL fnfAutoDateAdvStringSelect (UINT16    *pFieldDest, 
                                 UINT8      bLen,   
                                 UINT8      bFieldCtrl, 
                                 UINT8      bNumTableItems, 
                                 UINT8     *pTableTextIDs,
                                 UINT8     *pAttributes)
{
    UINT8   bWhichString = 0xFF;


    /* initialize the buffer to spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    if ( bAutoDateAdvDisabling && fnCharsBuffered() > 0 &&
     fnCMOSSetupGetCMOSSetupParams()->AutoAdvTime != 0 ) 
    {
        bWhichString = 1;
    }
    else if (fnCharsBuffered() > 2)
    {
        bWhichString = 0;
    }

    if (bWhichString < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, bWhichString);
    }

    return ((BOOL) SUCCESSFUL); 
}



/* *************************************************************************
// FUNCTION NAME: fnfDisplayMaxAdvDays
//
// DESCRIPTION: 
//      Displays the alllowalbe max advanced days. 
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//   05/10/2010   Deborah Kohl  Add padding characters either when right aligned 
//                              and in LTR mode or left aligned and in RTL mode
//   07/14/2006   Kan Jiang     Modify the problem found by Santity Check.
//   02/17/2006   Kan Jiang       Code cleanup
//                Kemble Wang     Initial Build
// *************************************************************************/
// VBL_TBD_FP
BOOL fnfDisplayMaxAdvDays (UINT16    *pFieldDest, 
                           UINT8      bLen,     
                           UINT8      bFieldCtrl, 
                           UINT8      bNumTableItems, 
                           UINT8     *pTableTextIDs,
                           UINT8     *pAttributes)
{
    UINT8 bStrLen;
    UINT32 ulAdvanceDate = bDateAdvanceLimit;
    SINT32 ulInspectionDays =  fnGetInspectionDays();
    UINT8   bWritingDir;           

    if ( ulInspectionDays > 0 ) 
    {
          if ( ulInspectionDays  < ulAdvanceDate )
          {
               ulAdvanceDate = ulInspectionDays;
          }
    }
    
    SET_SPACE_UNICODE(pFieldDest, bLen);

    bStrLen = fnBin2Unicode(pFieldDest, ulAdvanceDate);
    
    bWritingDir = fnGetWritingDir();

    /* if field is right-aligned, pad left of string with spaces */
    if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
        fnUnicodePadSpace(pFieldDest, bStrLen, bLen);
    }
    pFieldDest[bLen] = (UINT16) NULL;

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfPrintedDate
//
// DESCRIPTION: 
//      Output field function for the printed date.
//
// INPUTS:
//      Standard field function inputs.
//
// TEXT TABLE USAGE:  
//      1st string- date duck replacement character
//      2nd - 13th string: ordered names or abbreviations of months
//                                      (e.g. "JAN", "FEB", etc.)
//
// RETURNS:
//      TURE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Joe Mozdzer     Initial Build
// 11/07/2005   Vincent Yi      Code cleanup
// 02/28/2006   John Gao        Business Rules Extraction
// *************************************************************************/
BOOL fnfPrintedDate(UINT16      *pFieldDest, 
                    UINT8       bLen,   
                    UINT8       bFieldCtrl, 
                    UINT8       bNumTableItems, 
                    UINT8       *pTableTextIDs,
                    UINT8       *pAttributes)
{
    BOOL            fRetval = FAILURE;
    DATETIME        stDateTime;
    UINT8           bDateFormat;
    UINT16          usDDS;
    UINT16          usDateSepChar;

    //Get the date parameters and format the date
    if (fnGetDateParms(&stDateTime, &bDateFormat, &usDDS, &usDateSepChar) == SUCCESSFUL)
    {
           fRetval = fnBuildDateFmt(&stDateTime, bDateFormat, usDDS, usDateSepChar, TRUE, 
                                 TRUE, TRUE, TRUE, pFieldDest, bLen, bNumTableItems, 
                                 pTableTextIDs, bFieldCtrl);
    }

    if (fRetval == FAILURE)
    {
        /* make the field blank if we have failed */
        SET_SPACE_UNICODE(pFieldDest, bLen);
    }
        
    return ((BOOL) SUCCESSFUL); 
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfPrintedDateWithoutDuck
//
// DESCRIPTION: 
//      Output field function for the printed date without ducking.
//
// INPUTS:
//      Standard field function inputs.
//
// TEXT TABLE USAGE:  
//      1st string- date duck replacement character
//      2nd - 13th string: ordered names or abbreviations of months
//                                      (e.g. "JAN", "FEB", etc.)
//
// RETURNS:
//      TURE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 04/23/2007   Andy Mo     Inital version
// *************************************************************************/
BOOL fnfPrintedDateWithoutDuck( UINT16      *pFieldDest, 
                                UINT8       bLen,   
                                UINT8       bFieldCtrl, 
                                UINT8       bNumTableItems, 
                                UINT8       *pTableTextIDs,
                                UINT8       *pAttributes)
{
    BOOL            fRetval = FAILURE;
    DATETIME        stDateTime;
    UINT8           bDateFormat;
    UINT16          usDDS;
    UINT16          usDateSepChar;

    //Get the date parameters and format the date
    if (fnGetDateParms(&stDateTime, &bDateFormat, &usDDS, &usDateSepChar) == SUCCESSFUL)
    {
           fRetval = fnBuildDateFmt(&stDateTime, bDateFormat, usDDS, usDateSepChar, TRUE, 
                                 TRUE, FALSE, TRUE, pFieldDest, bLen, bNumTableItems, 
                                 pTableTextIDs, bFieldCtrl);
    }

    if (fRetval == FAILURE)
    {
        /* make the field blank if we have failed */
        SET_SPACE_UNICODE(pFieldDest, bLen);
    }
        
    return ((BOOL) SUCCESSFUL); 
}




/* *************************************************************************
// FUNCTION NAME: fnfViewPrintedDate
//
// DESCRIPTION: 
//      Output field function for printed date that advanced by Auto date advance.
//
// INPUTS:
//      Standard field function inputs.
//
// TEXT TABLE USAGE:  
//      1st string- date duck replacement character
//      2nd - 13th string: ordered names or abbreviations of months
//                         (e.g. "JAN", "FEB", etc.)
//
// RETURNS:
//      TURE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//       07/11/2006    Kan Jiang    Initial version.
//       04/03/2014    Renhao       Modified code for G922 Rates Expiration  
//                                  new feature.
// *************************************************************************/
BOOL fnfViewPrintedDate(UINT16  *pFieldDest, 
                        UINT8    bLen,  
                        UINT8    bFieldCtrl, 
                        UINT8    bNumTableItems, 
                        UINT8   *pTableTextIDs,
                        UINT8   *pAttributes)
{
    BOOL            fRetval = FAILURE;
    DATETIME        stDateTime;
    UINT8           bDateFormat;
    UINT16          usDDS;
    UINT16          usDateSepChar;

    //Get the date parameters and format the date
    if (fnGetDateParms(&stDateTime, &bDateFormat, &usDDS, &usDateSepChar) == SUCCESSFUL)
    {
         //  Print date already advanced, so there's no need to advance it again.
         
           fRetval = fnBuildDateFmt(&stDateTime, bDateFormat, usDDS, usDateSepChar, 
                                    TRUE, TRUE, FALSE, TRUE, pFieldDest, bLen, 
                                    bNumTableItems, pTableTextIDs, bFieldCtrl);
    }

    if (fRetval == FAILURE)
    {
        /* make the field blank if we have failed */
        SET_SPACE_UNICODE(pFieldDest, bLen);
    }
        
    return ((BOOL) SUCCESSFUL); 
}

/* *************************************************************************
// FUNCTION NAME: fnfPreViewPrintedDate
//
// DESCRIPTION: 
//      Output field function that previews the printed date which advanced by manual date advance.
//
// INPUTS:
//      Standard field function inputs.
//
// TEXT TABLE USAGE:  
//      1st string- date duck replacement character
//      2nd - 13th string: ordered names or abbreviations of months
//                         (e.g. "JAN", "FEB", etc.)
//
// RETURNS:
//      TURE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//       05/09/2007    Raymond Shen    Initial version.
//
// *************************************************************************/
BOOL fnfPreViewPrintedDate(UINT16   *pFieldDest, 
                        UINT8    bLen,  
                        UINT8    bFieldCtrl, 
                        UINT8    bNumTableItems, 
                        UINT8   *pTableTextIDs,
                        UINT8   *pAttributes)
{
    BOOL            fRetval = FAILURE;
    DATETIME        stDateTime;
    UINT8           bDateFormat;
    UINT16          usDDS;
    UINT16          usDateSepChar;
    SINT16          i;

    //Get the date parameters and format the date
    if (fnGetSysDateParms(&stDateTime, &bDateFormat, &usDDS, &usDateSepChar) == SUCCESSFUL)
    {
        switch(bManualDateAdvanceType)
        {
            case DATE_ADVANCE_ADD_ONE_DAY:
                AdjustDayByOne( &stDateTime,  TRUE); // advance one day for user to see.
                break;
            case DATE_ADVANCE_ADD_DAYS:
                CalcPrintedDate( &stDateTime, TRUE, (UINT16)wTmpDateAdvanceAmount );
                break;
            default:
                break;
        }

        fRetval = fnBuildDateFmt(&stDateTime, bDateFormat, usDDS, usDateSepChar, 
                                    TRUE, TRUE, FALSE, TRUE, pFieldDest, bLen, 
                                    bNumTableItems, pTableTextIDs, bFieldCtrl);
    }

    if (fRetval == FAILURE)
    {
        /* make the field blank if we have failed */
        SET_SPACE_UNICODE(pFieldDest, bLen);
    }
        
    return ((BOOL) SUCCESSFUL); 
}

/* *************************************************************************
// FUNCTION NAME:  fnfSystemDate
//
// DESCRIPTION: 
//      Output field function for the system date.  The system date 
//      does not reflect any adjustments made to the system time/date 
//      for time zone, daylight savings time, or clock drift correction.
//
// INPUTS:
//      Standard field function inputs.
//
// TEXT TABLE USAGE:  
//      1st string- date duck replacement character
//      2nd - 13th string: ordered names or abbreviations of months
//                         (e.g. "JAN", "FEB", etc.)
//
// RETURNS:
//      TURE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//       09/05/2006  Kan Jiang  Modify for fixing Fraca 103435.
//       07/05/2006  Kan Jiang  Modify for FP project.
//       Joe Mozdzer       Initial version
// *************************************************************************/
BOOL fnfSystemDate (UINT16     *pFieldDest, 
                    UINT8       bLen,   
                    UINT8       bFieldCtrl, 
                    UINT8       bNumTableItems, 
                    UINT8      *pTableTextIDs,
                    UINT8      *pAttributes)
{
    BOOL            fRetval = FAILURE;
    DATETIME        stDateTime;
    UINT8           bDateFormat;
    UINT16          usDDS;
    UINT16          usDateSepChar;

    //Get the date parameters and format the date
    if (fnGetSysDateParms(&stDateTime, &bDateFormat, &usDDS, &usDateSepChar) == SUCCESSFUL)
    {
           fRetval = fnBuildDateFmt(&stDateTime, bDateFormat, usDDS, usDateSepChar, TRUE, 
                                 TRUE, FALSE, TRUE, pFieldDest, bLen, bNumTableItems, 
                                 pTableTextIDs, bFieldCtrl);
    }

    if (fRetval == FAILURE)
    {
        /* make the field blank if we have failed */
        SET_SPACE_UNICODE(pFieldDest, bLen);
    }
        
    return ((BOOL) SUCCESSFUL); 
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfMidnightPrintedDate
//
// DESCRIPTION: 
//      Output field function to display the printed date after midnight
//
// INPUTS:
//      Standard field function inputs.
//
// TEXT TABLE USAGE:  
//      1st string- date duck replacement character
//      2nd - 13th string: ordered names or abbreviations of months
//                         (e.g. "JAN", "FEB", etc.)
//
// RETURNS:
//      TURE or FALSE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  09/12/2006  Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnfMidnightPrintedDate(UINT16  *pFieldDest, 
                            UINT8    bLen,  
                            UINT8    bFieldCtrl, 
                            UINT8    bNumTableItems, 
                            UINT8   *pTableTextIDs,
                            UINT8   *pAttributes)
{
    BOOL            fRetval = FAILURE;
    DATETIME        stDateTime;
    UINT8           bDateFormat;
    UINT16          usDDS;
    UINT16          usDateSepChar;

    //Get the date parameters and format the date
    if (fnGetMidnightDateParms(&stDateTime, &bDateFormat, &usDDS, &usDateSepChar) == SUCCESSFUL)
    {
           fRetval = fnBuildDateFmt(&stDateTime, bDateFormat, usDDS, usDateSepChar, 
                                    TRUE, TRUE, FALSE, TRUE, pFieldDest, bLen, 
                                    bNumTableItems, pTableTextIDs, bFieldCtrl);
    }

    if (fRetval == FAILURE)
    {
        /* make the field blank if we have failed */
        SET_SPACE_UNICODE(pFieldDest, bLen);
    }
        
    return ((BOOL) SUCCESSFUL); 
}






/* *************************************************************************
// FUNCTION NAME: 
//      fnfUserTime
//
// DESCRIPTION: 
//      Output field function for the current time of day.  This is
//      the system time, plus any offset adjustments for time zone,
//      daylight savings time, and clock drift. 
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//      1st string is used to separate the hours value from 
//      the minutes value, e.g. ":" in "12:30".  If there
//      is no string, there is no separation between hours
//      and minutes.
//      2nd string is appended after the minutes value.  There
//      does not have to be a 2nd string.  Note that it is
//      not possible to have a minutes trailer if there is
//      no hours/minutes separator.
// MODIFICATION HISTORY:
//              Joe Mozdzer     Initial Build
// 11/07/2005   Vincent Yi      Code cleanup
// 02/28/2006   John Gao        Code cleanup and Business Rules Extraction
// 04/16/2010   Jingwei,Li      Trim the redundant spaces that are padded 
//                              during screen mirror for RTL
// 04/21/2010   Jingwei,Li      Use fnCopyTableTextForRTL() to get table text for RTL.
// 05/10/2010   Deborah Kohl    Add padding characters either when right aligned 
//                              and in LTR mode or left aligned and in RTL mode
// *************************************************************************/
BOOL fnfUserTime (  UINT16 *pFieldDest, 
                    UINT8  bLen, 
                    UINT8  bFieldCtrl,
                    UINT8  bNumTableItems, 
                    UINT8  *pTableTextIDs,
                    UINT8  *pAttributes)
{
    UINT16    usTextID;
    DATETIME  stDateTime;
    BOOL      fDateStatus;
    UINT32    i;
    char     pStr[8];
    SINT32    lStrLen;
    UINT8     ubWritingDir;

    /* initialize the buffer to spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    ubWritingDir = fnGetWritingDir();

    fDateStatus = GetSysDateTime(&stDateTime, ADDOFFSETS, GREGORIAN);

    /* adjust for a 12 hour clock format, if applicable */
    if(fnIsTimeFormat12() == TRUE)
    {
        if (stDateTime.bHour >= 13)
        {
            stDateTime.bHour -= 12;
        }
        else if (stDateTime.bHour == 0)
        {
            stDateTime.bHour = 12;
        }
        lStrLen = sprintf(pStr, "%d", stDateTime.bHour);
    }
    else
    {
        lStrLen = sprintf(pStr, "%02d", stDateTime.bHour);
    }


    /* if it will fit in the field, add the hour */
    i = 0;
    

    if ((i + lStrLen) <= bLen)
    {
        fnAscii2Unicode(pStr, pFieldDest + i, lStrLen);
        i += lStrLen;
    }

    /* if there is one, add the separation string that goes between the hours and minutes */
    if (bNumTableItems > 0)
    {
    	EndianAwareCopy(&usTextID, pTableTextIDs, 2);
        if(ubWritingDir == RIGHT_TO_LEFT)
        {
           //trim the redundant spaces that are padded during screen mirror for RTL.
           i += fnCopyTableTextForRTL(pFieldDest + i, usTextID, bLen - i);  
        }
        else
        {
            i += fnCopyTableText(pFieldDest + i, usTextID, bLen - i);  
        }
    }
    
    /* if it will fit in the field, add the minutes */
    lStrLen = sprintf(pStr, "%02d", stDateTime.bMinutes);
    if ((i + lStrLen) <= bLen)
    {
        fnAscii2Unicode(pStr, pFieldDest + i, lStrLen);
        i += lStrLen;
    }

    /* if there is one, add the string that goes after the minutes */
    if (bNumTableItems > 1)
    {
    	EndianAwareCopy(&usTextID, pTableTextIDs + (1 * sizeof(unsigned short)), 2);
        i += fnCopyTableText(pFieldDest + i, usTextID, bLen - i);
    }

    if (  (   (bFieldCtrl & ALIGN_MASK)  && (ubWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (ubWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
        pFieldDest[i] = (unsigned short) NULL;
        fnUnicodePadSpace(pFieldDest, i, bLen);
    }

    return ((BOOL) SUCCESSFUL); 
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnfUserCorrectDriftTime
//
// DESCRIPTION: 
//      Output field function for the time with corrected drift.  This is
//      the system time, plus any offset adjustments for time zone,
//      daylight savings time, and clock drift. 
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      05/10/2010   Deborah Kohl      Add padding characters either when 
//                                     right aligned  and in LTR mode or left 
//                                     aligned and in RTL mode
//      04/21/2010   Deborah Kohl      Use fnCopyTableTextForRTL() to get
//                                     the text without the extra padding
//                                     characters introduced by screen generator
//                                     when mirroring screens
//      04/20/2006     Kan Jiang       Initial Build
// *************************************************************************/
// VBL_TBD_FP
BOOL fnfUserCorrectDriftTime (  UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs,     
                                UINT8  *pAttributes)
{
    UINT16    usTextID;
    UINT32    i;
    char     pStr[8];
    SINT32    lStrLen;
    DATETIME  stDTTmpTime;
    UINT8     ubWritingDir;

    ubWritingDir = fnGetWritingDir();   

    /* initialize the buffer to spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    memcpy(&stDTTmpTime,  &stDTNewTime,  sizeof(DATETIME)); 
    /* adjust for a 12 hour clock format, if applicable */
    if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
    {
        if (stDTTmpTime.bHour >= 13)
        {
            stDTTmpTime.bHour -= 12;
        }
        else if (stDTTmpTime.bHour == 0)
        {
            stDTTmpTime.bHour = 12;
        }
    }

    /* if it will fit in the field, add the hour */
    i = 0;
    
    if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
        lStrLen = sprintf(pStr, "%d", stDTTmpTime.bHour);
    else
        lStrLen = sprintf(pStr, "%02d", stDTTmpTime.bHour);

    if ((i + lStrLen) <= bLen)
    {
        fnAscii2Unicode(pStr, pFieldDest + i, lStrLen);
        i += lStrLen;
    }

    /* if there is one, add the separation string that goes between the hours and minutes */
    if (bNumTableItems > 0)
    {
    	EndianAwareCopy(&usTextID, pTableTextIDs, 2);
        if(ubWritingDir == RIGHT_TO_LEFT)
        {
           //trim the redundant spaces that are padded during screen mirror for RTL.
           i += fnCopyTableTextForRTL(pFieldDest + i, usTextID, bLen - i);  
        }
        else
        {
            i += fnCopyTableText(pFieldDest + i, usTextID, bLen - i);  
        }
    }
    
    /* if it will fit in the field, add the minutes */
    lStrLen = sprintf(pStr, "%02d", stDTNewTime.bMinutes);
    if ((i + lStrLen) <= bLen)
    {
        fnAscii2Unicode(pStr, pFieldDest + i, lStrLen);
        i += lStrLen;
    }

    /* if there is one, add the string that goes after the minutes */
    if (bNumTableItems > 1)
    {
    	EndianAwareCopy(&usTextID, pTableTextIDs + (1 * sizeof(unsigned short)), 2);
        if(ubWritingDir == RIGHT_TO_LEFT)
        {
           //trim the redundant spaces that are padded during screen mirror for RTL.
           i += fnCopyTableTextForRTL(pFieldDest + i, usTextID, bLen - i);  
        }
        else
        {
            i += fnCopyTableText(pFieldDest + i, usTextID, bLen - i);  
        }
    }

    /* if field is right-aligned, pad left of string with spaces */
    if (  (   (bFieldCtrl & ALIGN_MASK)  && (ubWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (ubWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
        pFieldDest[i] = (unsigned short) NULL;
        fnUnicodePadSpace(pFieldDest, i, bLen);
    }

    return ((BOOL) SUCCESSFUL); 
}



/* *************************************************************************
// FUNCTION NAME:  fnfAddDriftUserAmPm
//
// DESCRIPTION: 
//      Output field function that displays "AM" or "PM" 
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//      1st string is the "AM" string, 2nd is the "PM" string
//
// MODIFICATION HISTORY:
//    04/17/2006    Kan Jiang        Initial version
// *************************************************************************/
// VBL_TBD_FP
BOOL fnfAddDriftUserAmPm ( UINT16 *pFieldDest, 
                           UINT8   bLen,    
                           UINT8   bFieldCtrl, 
                           UINT8   bNumTableItems, 
                           UINT8  *pTableTextIDs,
                           UINT8  *pAttributes)
{
    DATETIME stDateTime;
    BOOL     fDateStatus;
    UINT16   usTextID;
    SINT32   lTmpDrift;
    
    lTmpDrift = fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift ;
      
    SET_SPACE_UNICODE(pFieldDest, bLen);

    if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
    {
        fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift = MAX_DRIFT_VAL ;
        fDateStatus = GetSysDateTime(&stDateTime, ADDOFFSETS, GREGORIAN);
        fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift = lTmpDrift;
        
        /* check to make sure there are at least two table items, 
           which are required for this field */
        if ((bNumTableItems >= 2) && (fDateStatus == true))
        {
            if (stDateTime.bHour <= 11)
            {
                /* "am" is the first string */
            	EndianAwareCopy(&usTextID, pTableTextIDs, 2);
            }
            else
            {
                /* "pm" is the second one */
            	EndianAwareCopy(&usTextID, pTableTextIDs + sizeof(UINT16), 2);
            }

            fnCopyTableText(pFieldDest, usTextID, bLen);
        }
    }

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME:  fnfSubDriftUserAmPm
//
// DESCRIPTION: 
//      Output field function that displays "AM" or "PM" 
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//      1st string is the "AM" string, 2nd is the "PM" string
//
// MODIFICATION HISTORY:
//    04/17/2006    Kan Jiang        Initial version
// *************************************************************************/
// VBL_TBD_FP
BOOL fnfSubDriftUserAmPm ( UINT16 *pFieldDest, 
                           UINT8   bLen,    
                           UINT8   bFieldCtrl, 
                           UINT8   bNumTableItems, 
                           UINT8  *pTableTextIDs,
                           UINT8  *pAttributes)
{
    DATETIME stDateTime;
    BOOL     fDateStatus;
    UINT16   usTextID;
    SINT32   lTmpDrift;
    
    lTmpDrift = fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift ;
      
    SET_SPACE_UNICODE(pFieldDest, bLen);

    if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
    {
        fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift = -MAX_DRIFT_VAL ;
        fDateStatus = GetSysDateTime(&stDateTime, ADDOFFSETS, GREGORIAN);
        fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift = lTmpDrift;
        
        /* check to make sure there are at least two table items, 
           which are required for this field */
        if ((bNumTableItems >= 2) && (fDateStatus == true))
        {
            if (stDateTime.bHour <= 11)
            {
                /* "am" is the first string */
            	EndianAwareCopy(&usTextID, pTableTextIDs, 2);
            }
            else
            {
                /* "pm" is the second one */
            	EndianAwareCopy(&usTextID, pTableTextIDs + sizeof(UINT16), 2);
            }

            fnCopyTableText(pFieldDest, usTextID, bLen);
        }
    }

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnfDriftAddTimeRange
//
// DESCRIPTION: 
//      Displays the amount of drift time that can be added
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      Always true
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//     04/18/2006   Kan jiang     Initial version
// *************************************************************************/
BOOL fnfDriftAddTimeRange( UINT16 *pFieldDest, 
                           UINT8   bLen,    
                           UINT8   bFieldCtrl, 
                           UINT8   bNumTableItems, 
                           UINT8  *pTableTextIDs,
                           UINT8  *pAttributes)
{
      SINT32 lTmpDrift;

      lTmpDrift = fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift ;
      fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift = MAX_DRIFT_VAL ;
      
      fnfUserTime(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, pAttributes);

      fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift = lTmpDrift;

      return ((BOOL) SUCCESSFUL); 
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnfDriftSubTimeRange
//
// DESCRIPTION: 
//      Displays the amount of drift time that can be added
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      Always true
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//     04/18/2006   Kan jiang     Initial version
// *************************************************************************/
BOOL fnfDriftSubTimeRange(  UINT16      *pFieldDest, 
                    UINT8       bLen,   
                    UINT8       bFieldCtrl, 
                    UINT8       bNumTableItems, 
                    UINT8       *pTableTextIDs,
                    UINT8       *pAttributes)
{
      SINT32 lTmpDrift;

      lTmpDrift = fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift ;
      fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift = -MAX_DRIFT_VAL ;
      
      fnfUserTime(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, pAttributes);

      fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift = lTmpDrift;

      return ((BOOL) SUCCESSFUL); 
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfShow12HourClockText
//
// DESCRIPTION: 
//      Output field function that displays field text the should 
//      only be displayed if 12 hour clock is in effect.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//      1st string is shown if 12 hour clock is in use.
//
// MODIFICATION HISTORY:
// 09/01/2006   Dicky Sun        Fix fraca GMSE00103434. We should refresh
//                               all the fields in a screen even if it is
//                               in hidden status.
// 02/28/2006   John Gao         Code cleanup and Business Rules Extraction
// 02/13/2006   Kan Jiang        Code cleanup
//              Criag DeFilippo  Initial Build
// *************************************************************************/
BOOL fnfShow12HourClockText (UINT16    *pFieldDest, 
                             UINT8      bLen,   
                             UINT8      bFieldCtrl, 
                             UINT8      bNumTableItems, 
                             UINT8     *pTableTextIDs,
                             UINT8     *pAttributes)
{

    SET_SPACE_UNICODE(pFieldDest, bLen);

    if(fnIsTimeFormat12() == TRUE)    
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, 0);
    }
    else
    {
        fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, 
                            0xff );
    }

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnfUserAmPm
//
// DESCRIPTION: 
//      Output field function that displays "AM" or "PM" (or language
//      equivalent) depending on the current time of day, taking
//      into account adjustments for time zone, DST, and clock drift.  
//      This function displays a blank field if the flash is not configured 
//      for 12 hour time format.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//      1st string is the "AM" string, 2nd is the "PM" string
//
// MODIFICATION HISTORY:
//                Joe Mozdzer       Initial Build
//    02/13/2006  Kan Jiang         Code cleanup
//    02/28/2006  John Gao          Code cleanup and Business Rules Extraction
// *************************************************************************/
BOOL fnfUserAmPm (  UINT16      *pFieldDest, 
                    UINT8       bLen,   
                    UINT8       bFieldCtrl, 
                    UINT8       bNumTableItems, 
                    UINT8       *pTableTextIDs,
                    UINT8       *pAttributes)
{
    UINT8    bCheckTimeAmPm;
    UINT16   usTextID;

    SET_SPACE_UNICODE(pFieldDest, bLen);

    if(fnIsTimeFormat12() == TRUE)
    {
        /* check to make sure there are at least two table items, 
           which are required for this field */
        if ((bNumTableItems >= 2) && (fnCheckTimeAMPM (&bCheckTimeAmPm) == TRUE))
        {
            if( bCheckTimeAmPm == TIME_AM)
            {
                /* "am" is the first string */
            	EndianAwareCopy(&usTextID, pTableTextIDs, 2);
            }
            else
            {
                /* "pm" is the second one */
            	EndianAwareCopy(&usTextID, pTableTextIDs + sizeof(UINT16), 2);
            }
            (void)fnCopyTableText(pFieldDest, usTextID, bLen);
        }
    }

    return ((BOOL) SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfUserCorrectDriftAmPm
//
// DESCRIPTION: 
//      Output field function that displays "AM" or "PM" of the time with 
//      corrected drift
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//      1st string is the "AM" string, 2nd is the "PM" string
//
// MODIFICATION HISTORY:
//    04/20/2006  Kan Jiang    Initial version
// *************************************************************************/
// VBL_TBD_FP
BOOL fnfUserCorrectDriftAmPm ( UINT16 *pFieldDest, 
                               UINT8   bLen,    
                               UINT8   bFieldCtrl, 
                               UINT8   bNumTableItems, 
                               UINT8  *pTableTextIDs,
                               UINT8  *pAttributes)
{
    UINT16   usTextID;

    SET_SPACE_UNICODE(pFieldDest, bLen);

    if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
    {
        /* check to make sure there are at least two table items, 
           which are required for this field */
        if (bNumTableItems >= 2) 
        {
            if (stDTNewTime.bHour <= 11)
            {
               /* "am" is the first string */
               usTextID = 0 ;
            }
            else
            {
               /* "pm" is the second one */
                usTextID = 1;
            }

            //fnCopyTableText(pFieldDest, usTextID, bLen);
            fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                               pTableTextIDs, usTextID);
        }
    }

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnfDSTActive
//
// DESCRIPTION: 
//      Output field function that displays a text table string
//      based on if daylight savings time is currently activated.
//
// INPUTS:
//      Standard field function inputs.
//
// TEXT TABLE USAGE:  
//      1st string displayed if DST is Enable
//      2nd string displayed if DST is Disable
//
// RETURNS:
//      Always true
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Joe Mozdzer   Initial Build
// 11/22/2005   Kan jiang     Reuse from JA1108 and cleanup
// *************************************************************************/
BOOL fnfDSTActive(  UINT16      *pFieldDest, 
                    UINT8       bLen,   
                    UINT8       bFieldCtrl, 
                    UINT8       bNumTableItems, 
                    UINT8       *pTableTextIDs,
                    UINT8       *pAttributes)
{
    UINT8   bItemID;

    /* check to make sure there are at least two table items, 
       which are required for this field */
    if (bNumTableItems >= 2)
    {
        if (GetRTClockDayLightSavEnable() == true)
        {
            /* use the first string if DST is turned on */
            bItemID = 0;
        }
        else
        {
            /* use the second string if DST is turned off */
            bItemID = 1;
        }
        
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems,
                           pTableTextIDs, bItemID);

    }

    return ((BOOL) SUCCESSFUL);
}


/* *************************************************************************
// Reuse from JA1108.
//
// FUNCTION NAME:
//        fnfAmPmToggleFieldInit
//
// DESCRIPTION:
//        Output field function that initializes the selection in the
//        general purpose binary choice field to match the state of
//        the am/pm setting of the user time.  (0 for am, 1 for pm)
//        for locked).  It then uses this number to select the field
//        output string. 
//
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:  
//        1st string for not in out of service lock
//        2nd string if in out of service lock
//
// MODIFICATION HISTORY:
//              Mozdzer     Initial version  
// 02/28/2006   John Gao    Business Rules Extraction 
// *************************************************************************/
BOOL fnfAmPmToggleFieldInit ( UINT16    *pFieldDest, 
                              UINT8      bLen, 
                              UINT8      bFieldCtrl, 
                              UINT8      bNumTableItems, 
                              UINT8     *pTableTextIDs, 
                              UINT8     *pAttributes )
{
    UINT8     bCheckTimeAmPm;

    SET_SPACE_UNICODE(pFieldDest, bLen);
    /* check if using 12 hour time format */
    if(fnIsTimeFormat12() == TRUE)
    {
        //No error to get the sys date time
        if(fnCheckTimeAMPM (&bCheckTimeAmPm) == TRUE)
        {
            if( bCheckTimeAmPm == TIME_AM)
            {
                //AM
                rGenPurposeBinChoiceField.bCurrentChoice = 0;
            }
            else
            {
                //PM
                rGenPurposeBinChoiceField.bCurrentChoice = 1;
            }
            fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,
                                rGenPurposeBinChoiceField.bCurrentChoice );
        }
    }
    else
    {
        /* if using 24 hr format,set the choice field to 0.it will not be used. */
        rGenPurposeBinChoiceField.bCurrentChoice = 0;
    }

    return ( (BOOL)SUCCESSFUL );
}


/* *************************************************************************
// FUNCTION NAME: fnfAutoDateAdvAmPm
//
// DESCRIPTION:
//        Output field function that displays "AM" or "PM" (or language
//        equivalent) that goes with the auto date advance time.
//        This function displays a blank field if the flash is not configured 
//        for 12 hour time format.
//
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:  
//
// MODIFICATION HISTORY:
// 09/01/2006   Dicky Sun        Fix fraca GMSE00103434. We should refresh
//                               all the fields in a screen even if it is
//                               in hidden status.
// 07/14/2006   Kan Jiang        Initial version
// *************************************************************************/
BOOL fnfAutoDateAdvAmPm ( UINT16    *pFieldDest, 
                          UINT8      bLen, 
                          UINT8      bFieldCtrl, 
                          UINT8      bNumTableItems, 
                          UINT8     *pTableTextIDs, 
                          UINT8     *pAttributes )
{
    DATETIME  stDateTime;

    SET_SPACE_UNICODE(pFieldDest, bLen);
    
    /* check if using 12 hour time format */
    if(fnIsTimeFormat12() == TRUE)
    {      
        fnTodSecsToDatetime(CMOSSetupParams.AutoAdvTime, &stDateTime, FALSE);
     
        if ( stDateTime.bHour <= 11 )
        {
            //AM
            rGenPurposeBinChoiceField.bCurrentChoice = 0;
        }
        else
        {
            //PM
            rGenPurposeBinChoiceField.bCurrentChoice = 1;
        }
            
        fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,
                            rGenPurposeBinChoiceField.bCurrentChoice );
    }
    else
    {
        /* if using 24 hr format,set the choice field to 0.it will not be used. */
        rGenPurposeBinChoiceField.bCurrentChoice = 0;

        fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,
                            0xff );
    }

    return ( (BOOL)SUCCESSFUL );
 }




/* *************************************************************************
// FUNCTION NAME: fnfShowChangeDateMenu 
//
// DESCRIPTION:
//
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:  
// MODIFY HISTORY:
// 2014-04-01 Wang Biao  Merged codes to FP project.
// 2013-11-13 Bob.li Modified this function for PR20 UK auto advance with
//                    mandatory condition.
// *************************************************************************/
BOOL fnfShowChangeDateMenu ( UINT16    *pFieldDest, 
                          UINT8      bLen, 
                          UINT8      bFieldCtrl, 
                          UINT8      bNumTableItems, 
                          UINT8     *pTableTextIDs, 
                          UINT8     *pAttributes )
{
    UINT8    bWhichString;	
    UINT16   wAutoAdvEMDParam = fnFlashGetWordParm(WP_AUTO_DATE_ADVANCE_OPTIONS);

	if (((DATE_ADVANCE_TIME_OF_DAY == bDateAdvancePrompt) 
	            && ((CMOSSetupParams.AutoAdvTime == 0 ) 
	                || (CMOSSetupParams.AutoAdvTime != 0 && (wAutoAdvEMDParam & AUTO_ADV_DATE_IS_MANDATORY))))  // if auto advance time is midnight or not midnight with mandatory condition
			|| DATE_ADVANCE_FROM_MIDNIGHT == bDateAdvancePrompt
			|| DATE_ADVANCE_FORCE_TO_TODAY == bDateAdvancePrompt
			|| DATE_ADVANCE_CONFIRMED == bDateAdvancePrompt 
			|| ((wAutoAdvEMDParam & AUTO_ADV_DATE_IS_MANDATORY) && (DATE_ADVANCE_FROM_PRESET == bDateAdvancePrompt)))
		bWhichString = 1 ; // show nothing, because have to advance the date
	else	
		bWhichString = 0 ;   //  allow do not change date
		
	fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, bWhichString);

	return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// Reuse from JA1108.
//
// FUNCTION NAME: 
//         fnfAmPmToggleFieldRefresh
//
// DESCRIPTION:
//         Output field function that displays the table text string
//         that matches the current selection in the general purpose
//         binary choice field.  Same as the generic toggle field refresh
//         function except that the field is displayed as all blanks if
//         the time format is not 12 hour.
//
// INPUTS:
//         Standard field function inputs.
//
// RETURNS:
//         always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
// 09/01/2006   Dicky Sun     Fix fraca GMSE00103434. We should refresh
//                            all the fields in a screen even if it is
//                            in hidden status.
// 02/28/2006   John Gao      Business Rules Extraction
//              Mozdzer       Initial version  
// *************************************************************************/
BOOL fnfAmPmToggleFieldRefresh ( UINT16 *pFieldDest, 
                                 UINT8   bLen, 
                                 UINT8   bFieldCtrl, 
                                 UINT8   bNumTableItems, 
                                 UINT8  *pTableTextIDs, 
                                 UINT8  *pAttributes )
{
    /* check if using 12 hour time format */
    if(fnIsTimeFormat12() == TRUE)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,
                           rGenPurposeBinChoiceField.bCurrentChoice);
    }
    else
    {
        fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,
                            0xff );
    }

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnfNormalPresetTimerInit
//
// DESCRIPTION:
//          Entry field function to initialize the entry field where the 
//          normal preset timer is set in minute(s). Maximum time allowed is 
//          10 minutes (sleep mode ECR)     
//
// INPUTS:
//          Standard field function inputs.
//
// RETURNS:
//          always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//          None.
//
// MODIFICATION HISTORY:
// 
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                   Victor Li  Initial version  
//
// *************************************************************************/
BOOL fnfNormalPresetTimerInit (   UINT16 *pFieldDest, 
                                  UINT8   bLen, 
                                  UINT8   bFieldCtrl, 
                                  UINT8   bNumTableItems, 
                                  UINT8  *pTableTextIDs, 
                                  UINT8  *pAttributes )
{       
    BOOL    fRetval = FAILURE;

    /* use the general timeout field init function */
    fRetval = fnTimeoutInit(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,
                fnCMOSSetupGetCMOSSetupParams()->wpUserSetPresetClearTimeout);

    return (fRetval);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfRTCTime
//
// DESCRIPTION: 
//      Output field function for the RTC time of day.  This is
//      the RTC time, plus any offset adjustments for time zone,
//      daylight savings time, and clock drift. 
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//      1st string is used to separate the hours value from 
//      the minutes value, e.g. ":" in "12:30".  If there
//      is no string, there is no separation between hours
//      and minutes.
//      2nd string is appended after the minutes value.  There
//      does not have to be a 2nd string.  Note that it is
//      not possible to have a minutes trailer if there is
//      no hours/minutes separator.
// MODIFICATION HISTORY:
// 05/10/2010   Deborah Kohl    Add padding characters either when 
//                              right aligned  and in LTR mode or left 
//                              aligned and in RTL mode
// 04/21/2010   Deborah Kohl    Use fnCopyTableTextForRTL() to get
//                              the text without the extra padding
//                              characters introduced by screen generator
//                              when mirroring screens
// 06/28/2006   Raymond Shen    Initial Build
//
// *************************************************************************/
BOOL fnfRTCTime (  UINT16 *pFieldDest, 
                    UINT8  bLen, 
                    UINT8  bFieldCtrl,
                    UINT8  bNumTableItems, 
                    UINT8  *pTableTextIDs,
                    UINT8  *pAttributes)
{
    UINT16    usTextID;
    DATETIME  stDateTime;
    BOOL      fDateStatus;
    UINT32    i;
    char     pStr[8];
    SINT32    lStrLen;
    UINT8     ubWritingDir;

    ubWritingDir = fnGetWritingDir();       

    /* initialize the buffer to spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    fDateStatus = ReadRTClock(&stDateTime);

    /* adjust for a 12 hour clock format, if applicable */
    if(fnIsTimeFormat12() == TRUE)
    {
        if (stDateTime.bHour >= 13)
        {
            stDateTime.bHour -= 12;
        }
        else if (stDateTime.bHour == 0)
        {
            stDateTime.bHour = 12;
        }
        lStrLen = sprintf(pStr, "%d", stDateTime.bHour);
    }
    else
    {
        lStrLen = sprintf(pStr, "%02d", stDateTime.bHour);
    }


    /* if it will fit in the field, add the hour */
    i = 0;
    

    if ((i + lStrLen) <= bLen)
    {
        fnAscii2Unicode(pStr, pFieldDest + i, lStrLen);
        i += lStrLen;
    }

    /* if there is one, add the separation string that goes between the hours and minutes */
    if (bNumTableItems > 0)
    {
    	EndianAwareCopy(&usTextID, pTableTextIDs, 2);
        if(ubWritingDir == RIGHT_TO_LEFT)
        {
           //trim the redundant spaces that are padded during screen mirror for RTL.
           i += fnCopyTableTextForRTL(pFieldDest + i, usTextID, bLen - i);  
        }
        else
        {
            i += fnCopyTableText(pFieldDest + i, usTextID, bLen - i);  
        }
    }
    
    /* if it will fit in the field, add the minutes */
    lStrLen = sprintf(pStr, "%02d", stDateTime.bMinutes);
    if ((i + lStrLen) <= bLen)
    {
        fnAscii2Unicode(pStr, pFieldDest + i, lStrLen);
        i += lStrLen;
    }

    /* if there is one, add the string that goes after the minutes */
    if (bNumTableItems > 1)
    {
    	EndianAwareCopy(&usTextID, pTableTextIDs + (1 * sizeof(unsigned short)), 2);
        i += fnCopyTableText(pFieldDest + i, usTextID, bLen - i);
    }

    /* if field is right-aligned, pad left of string with spaces */
    if (  (   (bFieldCtrl & ALIGN_MASK)  && (ubWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (ubWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
        pFieldDest[i] = (unsigned short) NULL;
        fnUnicodePadSpace(pFieldDest, i, bLen);
    }

    return ((BOOL) SUCCESSFUL); 
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfRTCTimeAmPm
//
// DESCRIPTION: 
//      Output field function that displays "AM" or "PM" (or language
//      equivalent) depending on the RTC time, taking into account 
//      adjustments for time zone, DST, and clock drift.  
//      This function displays a blank field if the flash is not configured 
//      for 12 hour time format.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// WARNINGS/NOTES:
//      1st string is the "AM" string, 2nd is the "PM" string
//
// MODIFICATION HISTORY:
//    06/28/2006  Raymond Shen        Initial Build
// *************************************************************************/
BOOL fnfRTCTimeAmPm (  UINT16       *pFieldDest, 
                    UINT8       bLen,   
                    UINT8       bFieldCtrl, 
                    UINT8       bNumTableItems, 
                    UINT8       *pTableTextIDs,
                    UINT8       *pAttributes)
{
    UINT8    bCheckTimeAmPm;
    UINT16   usTextID;

    SET_SPACE_UNICODE(pFieldDest, bLen);

    if(fnIsTimeFormat12() == TRUE)
    {
        /* check to make sure there are at least two table items, 
           which are required for this field */
        if ((bNumTableItems >= 2) && (fnCheckRTCTimeAMPM (&bCheckTimeAmPm) == TRUE))
        {
            if( bCheckTimeAmPm == TIME_AM)
            {
                /* "am" is the first string */
            	EndianAwareCopy(&usTextID, pTableTextIDs, 2);
            }
            else
            {
                /* "pm" is the second one */
            	EndianAwareCopy(&usTextID, pTableTextIDs + sizeof(UINT16), 2);
            }
            (void)fnCopyTableText(pFieldDest, usTextID, bLen);
        }
    }

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfWaitForEnvTimeoutInit
//
// DESCRIPTION:
//      Entry field function to initialize the entry field where the 
//      timeout for wait-for-envelope is set in second(s). 
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//      None.
//
// MODIFICATION HISTORY:
// 
//  08/29/2006  Vincent Yi      Initial version  
//
// *************************************************************************/
BOOL fnfWaitForEnvTimeoutInit (  UINT16 *pFieldDest, 
                                 UINT8   bLen, 
                                 UINT8   bFieldCtrl, 
                                 UINT8   bNumTableItems, 
                                 UINT8  *pTableTextIDs, 
                                 UINT8  *pAttributes )
{
    BOOL    fRetval = FAILURE;

    /* use the general timeout field init function */
    fRetval = fnTimeoutInit(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,
                fnCMOSSetupGetCMOSSetupParams()->ulMailInactivityTimeout);

    return (fRetval);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfZWZPTimeoutInit
//
// DESCRIPTION:
//      Entry field function to initialize the entry field where the 
//      timeout for ZWZP is set in second(s). 
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//      None.
//
// MODIFICATION HISTORY:
//  26 Mar 2007 Bill Herring    Initial
// 
//
// *************************************************************************/
BOOL fnfZWZPTimeoutInit (  UINT16 *pFieldDest, 
                           UINT8   bLen, 
                           UINT8   bFieldCtrl, 
                           UINT8   bNumTableItems, 
                           UINT8  *pTableTextIDs, 
                           UINT8  *pAttributes )
{
    BOOL    fRetval = FAILURE;

    /* use the general timeout field init function */
    fRetval = fnTimeoutInit(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,
                fnCMOSGetZwzpParams()->timeout);

    return (fRetval);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfDataCaptureDate
//
// DESCRIPTION:
//       
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//      None.
//
// MODIFICATION HISTORY:
//  18 Jul 2007 Simon Fox    Modified to display date & time.
//  29 Mar 2007 Bill Herring Initial
//
// *************************************************************************/
BOOL fnfDataCaptureDate (  UINT16 *pFieldDest, 
                           UINT8   bLen, 
                           UINT8   bFieldCtrl, 
                           UINT8   bNumTableItems, 
                           UINT8  *pTableTextIDs, 
                           UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, bLen);

    return ((BOOL)SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfDHOnTimeInit
//
// DESCRIPTION:
//      Entry field function to initialize the entry field for DH On time. 
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//      None.
//
// MODIFICATION HISTORY:
//      02/28/2008  Joey Cui    Initial Version
// 
//
// *************************************************************************/
BOOL fnfDHOnTimeInit (  UINT16 *pFieldDest, 
                        UINT8   bLen, 
                        UINT8   bFieldCtrl, 
                        UINT8   bNumTableItems, 
                        UINT8  *pTableTextIDs, 
                        UINT8  *pAttributes )
{
    DATETIME  stDateTime;
    UINT32    ulClockval;
    UINT32    i;
    char     pStr[8];
    UNICHAR   pUniStr[8];
    SINT32    lStrLen;

    /* initialize the buffer to spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);
                    
    (void) fnfStdEntryInit(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, pAttributes);

    ulClockval = fnGetCMOSLongDHOnTime();

    if (ulClockval != 0)
    {
        fnTodSecsToDatetime(ulClockval, &stDateTime, FALSE);
    }
    else
    {
        if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
        {
            stDateTime.bHour = 12;
            stDateTime.bMinutes = 0;
        }
        else
        {
            stDateTime.bHour = 0;
            stDateTime.bMinutes = 0;
        }
    }

    /* adjust for a 12 hour clock format, if applicable */
    if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
    {
        if (stDateTime.bHour >= 13)
        {
            rGenPurposeBinChoiceField.bCurrentChoice = 1; // PM
            stDateTime.bHour -= 12;
        }
        else    
        {
            rGenPurposeBinChoiceField.bCurrentChoice = 0; // AM
            if (stDateTime.bHour == 0)
                stDateTime.bHour = 12;
        }
    }
    else
    {
        rGenPurposeBinChoiceField.bCurrentChoice = 0;
    }

    /* if it will fit in the field, add the hour */
    lStrLen = sprintf(pStr, "%d", stDateTime.bHour);
    fnAscii2Unicode(pStr, pUniStr, lStrLen);
    for(i = 0; i < lStrLen; i++)
    {
        fnBufferUnicode(pUniStr[i]);
    }

    
    /* if it will fit in the field, add the minutes */
    lStrLen = sprintf(pStr, "%02d", stDateTime.bMinutes);
    fnAscii2Unicode(pStr, pUniStr, lStrLen);
    for(i = 0; i < lStrLen; i++)
    {
        fnBufferUnicode(pUniStr[i]);
    }

    return (fnfStdEntryRefresh(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, pAttributes));
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfDHOnTimeAmPm
//
// DESCRIPTION:
//      Entry field function to initialize the AM/PM entry field.  
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//      None.
//
// MODIFICATION HISTORY:
//      02/28/2008  Joey Cui    Initial Version
// 
//
// *************************************************************************/
BOOL fnfDHOnTimeAmPm (  UINT16 *pFieldDest, 
                        UINT8   bLen, 
                        UINT8   bFieldCtrl, 
                        UINT8   bNumTableItems, 
                        UINT8  *pTableTextIDs, 
                        UINT8  *pAttributes )
{
    DATETIME  stDateTime;

    SET_SPACE_UNICODE(pFieldDest, bLen);
    
    /* check if using 12 hour time format */
    if(fnIsTimeFormat12() == TRUE)
    {      
        fnTodSecsToDatetime(fnGetCMOSLongDHOnTime(), &stDateTime, FALSE);
     
        if ( stDateTime.bHour <= 11 )
        {
            //AM
            rGenPurposeBinChoiceField.bCurrentChoice = 0;
        }
        else
        {
            //PM
            rGenPurposeBinChoiceField.bCurrentChoice = 1;
        }
            
        fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,
                            rGenPurposeBinChoiceField.bCurrentChoice );
    }
    else
    {
        /* if using 24 hr format,set the choice field to 0.it will not be used. */
        rGenPurposeBinChoiceField.bCurrentChoice = 0;

        fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,
                            0xff );
    }

    return ( (BOOL)SUCCESSFUL );
}


/* Hard key functions */
/* *************************************************************************
// FUNCTION NAME: 
//          fnhkBufferKeySmartTime
//
// DESCRIPTION:
//         Hard key function that buffers the keypress in the current
//              entry buffer if the digit entered will or potentially will 
//              result in a valid time string being buffered.
//
// INPUTS:
//          Standard hard key function inputs (format 1).
//
// RETURNS:
//
// WARNINGS/NOTES: 
//          This hard key function always stays on the existing screen.
//
// MODIFICATION HISTORY:               
//          Mozdzer       Initial version 
//          02/13/2006   Kan Jiang    Code cleanup
//          02/28/2006  John Gao      Business Rules Extraction
// *************************************************************************/
T_SCREEN_ID fnhkBufferKeySmartTime( UINT8       bKeyCode, 
                                     T_SCREEN_ID wNextScr1, 
                                     T_SCREEN_ID wNextScr2 )
{
    BOOL     fAccept = TRUE;
    BOOL     fCursorDisable = FALSE;
    UINT16  *pEntry = NULL;        /* pointer to the first character entered */
    UINT8    bNumBuffered;
    UINT16   usNextChar;

    bNumBuffered = fnCharsBuffered();
    
    /* find the first number entered in the entry buffer */
    if (bNumBuffered > 0)
    {
        pEntry = rGenPurposeEbuf.pBuf;
        while ((*pEntry != (unsigned short) NULL) && (fnIsUnicodeDigit(*pEntry) == FALSE))
        {
            pEntry++;
        }
    }
    else
    {
        fnClearEntryBuffer(); //clear buffer 1st
    }


    /* find out what the entered character maps to */
    if (fnSearchKeyMap(bKeyCode, &usNextChar) == FALSE)
    {
        fAccept = FALSE;
        return (0);
    }

    if(fnIsTimeFormat12() == TRUE)
    {
        /* if time format = 12 hour... */
        switch (bNumBuffered)
        {
            case 0:
                /* if this is the first character typed, any digit except 0 is ok */
                if (usNextChar == UNICODE_ZERO)
                {
                    fAccept = FALSE;
                }
                break;

            case 1:
                /* if this is the 2nd character typed, it must be between 0 and 5 */
                if ((usNextChar < UNICODE_ZERO) || (usNextChar > UNICODE_FIVE))
                {
                    fAccept = FALSE;
                }
                break;

            case 2:
                /* the third character typed can be any digit */
                break;
                    
            case 3:
                if (pEntry != NULL)
                {
                    /* the fourth character is trickier.  if the first character is not a
                        "1", it is disallowed because there is no valid 4 digit time that
                        starts with anything else */
                    if (*pEntry != UNICODE_ONE)
                    {
                        fAccept = FALSE;
                        break;
                    }

                    /* if first character is a 1, the second must be a 0, 1 or 2 in order
                        to have a valid 4-digit time */
                    if ((*(pEntry+1) < UNICODE_ZERO) || (*(pEntry+1) > UNICODE_TWO))
                    {
                        fAccept = FALSE;
                        break;
                    }

                    /* if we start with 10, 11, or 12, the third character entered must
                        be between 0 and 5 */
                    if ((*(pEntry+2) < UNICODE_ZERO) || (*(pEntry+2) > UNICODE_FIVE))
                    {
                        fAccept = FALSE;
                        break;
                    }
                }
                break;

            case 4:
                fAccept = FALSE;
                break;

            default:
                break;

        }
        /* regardless of which position is being checked, nothing besides digits are ever allowed */
        if (fnIsUnicodeDigit(usNextChar) == FALSE)
        {
            fAccept = FALSE;
        }
    }
    else
    {
        /* if time format = 24 hour... */
        switch (bNumBuffered)
        {
            case 0:
                /* if this is the first character typed, any digit is ok */
                break;

            case 1:
                if (pEntry != NULL)
                {
                    /* if this is the 2nd character typed, then if the first character is a
                        0 or 1, any char is ok; if the first char is > 1 only 0 thru 5 are ok. */
                    if ((*pEntry == UNICODE_ZERO) || (*pEntry == UNICODE_ONE))
                        break;
                    else
                        if (usNextChar > UNICODE_FIVE)
                            fAccept = FALSE;
                }
                break;

            case 2:
                if (pEntry != NULL)
                {
                    /* if the 2nd char is between 0 and 5, the 3rd can be any digit;
                        otherwise, it must be between 0 and 5 */
                    if (*(pEntry+1) <= UNICODE_FIVE)
                        break;
                    else
                        if (usNextChar > UNICODE_FIVE)
                            fAccept = FALSE;
                }
                break;
                
            case 3:
                if (pEntry != NULL)
                {
                    /* a fourth character is allowed only if the third is between 0 and 5... */
                    if (*(pEntry+2) > UNICODE_FIVE)
                    {
                        fAccept = FALSE;
                        break;
                    }

                    /* ...the first character is not > 2... */
                    if (*pEntry > UNICODE_TWO)
                    {
                        fAccept = FALSE;
                        break;
                    }

                    /* ... and if the first character is a 2, the 2nd is between 0 and 3 */
                    if (*pEntry == UNICODE_TWO)
                        if (*(pEntry+1) > UNICODE_THREE)
                            fAccept = FALSE;
                }
                break;
                    
            case 4:
                fAccept = FALSE;
                break;

            default:
                break;
        }
        /* regardless of which position is being checked, only digits are ever allowed */
        if (fnIsUnicodeDigit(usNextChar) == FALSE)
        {
            fAccept = FALSE;
        }

    }

    /* buffer the key if it passed the tests above */
    if (fAccept == TRUE)
    {
        fnBufferKey(bKeyCode);
    }

    /* now check if the buffer should be disabled (i.e. cursor turned off) */
    if (fnCharsBuffered() == 4)
    {
        fCursorDisable = TRUE;  /* after 4 characters we are always done */
    }

    /* after 3 characters we have to check what has been enetered so far in order
        to make this determination */
    if (fnCharsBuffered() == 3)
    {
        /* refresh the pointer to the first character, since it may have moved after 
            we buffered the newest one */
        pEntry = rGenPurposeEbuf.pBuf;
        while ((*pEntry != (unsigned short) NULL) 
               && (fnIsUnicodeDigit(*pEntry) == FALSE))
        {
            pEntry++;
        }

        if(fnIsTimeFormat12() == TRUE)
        {
            /* for 12 hour time format we cannot accept a fourth character if... */

            /* ... the first char is not a 1, */
            if (*pEntry != UNICODE_ONE)
                fCursorDisable = TRUE;

            /* the second is not a 0, 1 or 2, */
            if ((*(pEntry+1) < UNICODE_ZERO) || (*(pEntry+1) > UNICODE_TWO))
                fCursorDisable = TRUE;

            /* or the third is not between 0 and 5 */
            if ((*(pEntry+2) < UNICODE_ZERO) || (*(pEntry+2) > UNICODE_FIVE))
                fCursorDisable = TRUE;

        }
        else
        {
            /* for 24 hour time format we cannot accept a fourth character if... */

            /* ... third is not between 0 and 5, */
            if ((*(pEntry+2) < UNICODE_ZERO) || (*(pEntry+2) > UNICODE_FIVE))
                fCursorDisable = TRUE;

            /* the first character is not a 0, 1, or 2, */
            if ((*pEntry < UNICODE_ZERO) || (*pEntry > UNICODE_TWO))
                fCursorDisable = TRUE;

            /* or if the first character is a 2, the 2nd is not between 0 and 3 */
            if (*pEntry == UNICODE_TWO)
                if ((*(pEntry+1) < UNICODE_ZERO) || (*(pEntry+1) > UNICODE_THREE))
                    fCursorDisable = TRUE;
        }
    }

    if (fCursorDisable == TRUE)
    {
        fnBufferDisable();
    }

     return (0);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkDateAdvAddOneDay
//
// DESCRIPTION: 
//      Hard key function that advances one day.
//
// INPUTS:
//      Standard hard key function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Go to Main screen if success.
//      Go to OptionAdvanceDateTooHigh if the advance date is out of range.
//      Go to RateActiveByDateAdv screen if advance date cross rate effective
//         date and Data Capture disabled
//      Go to ErrAdavanceDate screen if advance date cross rate effective
//         date and Data Capture enabled
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/09/2007  Raymond Shen    Add confirmation logic for French feature
//  02/16/2007  Vincent Yi      Added new err screen
//  09/07/2006  Kan Jiang       Modified for fixing Fraca 103415
//  06/29/2006  Kan Jiang       Modified according to new Nav Spec 
//  11/18/2005  Dicky Sun       Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkDateAdvAddOneDay(UINT8 bKeyCode,
                                 UINT8 bNumNext,
                                 T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;

    bDateAdvancePrompt = DATE_ADVANCE_SET_DAYS ;

    if ( fnFlashGetByteParm( BP_MAIL_DATE_CHANGE_CONFIRMATION_REQUIRED ) == TRUE )
    {
        bManualDateAdvanceType = DATE_ADVANCE_ADD_ONE_DAY;
        usNext = pNextScreens[0];
    }
    else
    {
        usNext = fnhkDateAdvanceOneDay(bKeyCode, bNumNext-1, &pNextScreens[1]);
    }


    return ( usNext );
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkDateCorrectionResetToToday
//
// DESCRIPTION: 
//      Clears the number of date correction days to zero 
//
// INPUTS:
//      Standard hard key function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      0.Stay at current screen.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//04/23/2007   Andy Mo  Initial version
// *************************************************************************/
T_SCREEN_ID fnhkDateCorrectionResetToToday( UINT8        bKeyCode,
                                            UINT8        bNumNext,
                                            T_SCREEN_ID *pNextScreens)
{
    SetPrintedDateCorrectionDays(0);
    return ((bNumNext > 0)? pNextScreens[0] : 0);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkDateCorrectionAddOneDay
//
// DESCRIPTION: 
//      Hard key function that advances one day.
//
// INPUTS:
//      Standard hard key function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Go to Main screen
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 04/23/2007   Andy Mo     Initial version
// *************************************************************************/
T_SCREEN_ID fnhkDateCorrectionAddOneDay(  UINT8 bKeyCode,
                                        UINT8 bNumNext,
                                        T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = pNextScreens[0];

    /* attempt to set the printed date advance days to this value.  if this
        fails it must be because the number is out of range, so go to wNextScr2.
        if successful, continue processing. */
    SetPrintedDateCorrectionDays(1);
    return usNext;
}
    
/* *************************************************************************
// FUNCTION NAME: 
//      fnhkValidateDateCorrect
//
// DESCRIPTION: 
//      Hard key function that is called to validate the date correction amount.
//
// INPUTS:
//      Standard hard key function inputs.
//
// OUTPUTS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
/   04/23/2007  Andy Mo     Initial version
// *************************************************************************/
T_SCREEN_ID fnhkValidateDateCorrect(UINT8        bKeyCode,
                                    UINT8        bNumNext,
                                    T_SCREEN_ID *pNextScreens)

{
    T_SCREEN_ID usNext = 0;      
    UINT32   ulDays;

    //If no char entered, we should return.
    if(fnCharsBuffered() == 0)
    {
        fnClearEntryBuffer();
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
        return usNext;
    }

    /* read the value in the entry buffer, translating to binary */
    fnUnicode2Bin(rGenPurposeEbuf.pBuf, &ulDays, fnBufferLen());
       
    if ((ulDays <= bDateAdvanceLimit)&& (ulDays > 0) )
    {
        SetPrintedDateCorrectionDays(ulDays);
        usNext= pNextScreens[0];  //MMIndicaReady

    }
    else
    {
        fnClearEntryBuffer();
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
        return ( usNext );
    }
    
    return usNext;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnhkDateAdvClearToZero
//
// DESCRIPTION: 
//      Clears the number of days the date is advanced to 0.
//
// INPUTS:
//      Standard hard key function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      0.Stay at current screen.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//              Henry Liu   Initial version
//  02/13/2006  Kan Jiang   Code Cleanup
//  02/29/2006  John Gao    Business Rules Extraction
//  04/03/2014  Renhao      Modified code for G922 Rates Expiration  new feature.
// *************************************************************************/
T_SCREEN_ID fnhkDateAdvClearToZero(UINT8        bKeyCode,
                                   UINT8        bNumNext,
                                   T_SCREEN_ID *pNextScreens)
{  
    DATETIME    rPrintedDate;
    char       pPrintedDateAscii[9];
    
    if ( fnClearDateAdvToZero() == TRUE )
    {
        bDateAdvancePrompt = DATE_ADVANCE_RESET_TO_TODAY ;
    }
  
    (void)GetPrintedDate(&rPrintedDate, TRUE);
    (void)sprintf(pPrintedDateAscii, "%02d%02d", rPrintedDate.bCentury, rPrintedDate.bYear);
    (void)sprintf(pPrintedDateAscii+4, "%02d", rPrintedDate.bMonth);
    (void)sprintf(pPrintedDateAscii+6, "%02d", rPrintedDate.bDay);
    pPrintedDateAscii[8] = 0;

    (void) fnCMOSRateCallback(TRUE, pPrintedDateAscii, TRUE, FALSE);
    
    return ((bNumNext > 0)? pNextScreens[0] : 0);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkSetDSTState
//
// DESCRIPTION: 
//      Hard key function that Enable/Disable day lignt saving status.
//
// INPUTS:
//      Standard hard key function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      0.Stay at current screen.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/12/2005  Dicky Sun    Initial version
//  12/22/2005  Kan Jiang    Use GetRTClockDayLightSavEnable() to get current DST state
// *************************************************************************/
T_SCREEN_ID fnhkSetDSTState(UINT8        bKeyCode,
                            UINT8        bNumNext,
                            T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;

    //If day light saving is enabled, disable it. Or else, enable it.
    if(GetRTClockDayLightSavEnable()  == TRUE)
    {
        SetRTClockDayLightSavEnable(FALSE);
    }
    else
    {
        SetRTClockDayLightSavEnable(TRUE);
    }

    return (usNext);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnhkDateAdvValidEntered
//
// DESCRIPTION: 
//      Hard key function that works to set the number of date advance days.
//
// INPUTS:
//      Standard hard key function inputs.
//
// OUTPUTS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  09/16/2014  Renhao        Modified code to fix fraca 227231.
//  05/09/2007  Raymond Shen    Add confirmation logic for French feature
//  02/13/2007  Oscar Wang   Add new rate err screen.
//  12/22/2005  Kan Jiang    modified and code Cleanup.
//              Henry Liu    Initial version
// *************************************************************************/
T_SCREEN_ID fnhkDateAdvValidEntered(UINT8        bKeyCode,
                                    UINT8        bNumNext,
                                    T_SCREEN_ID *pNextScreens)

{
    T_SCREEN_ID usNext = 0;
    UINT32  ulDays;

    //If no char entered, we should return.
    if(fnCharsBuffered() == 0)
    {
         return 0;
    }

    /* read the value in the entry buffer, translating to binary */
    fnUnicode2Bin(rGenPurposeEbuf.pBuf, &ulDays, fnBufferLen());
       
    if ( (wTmpDateAdvanceAmount + ulDays) <= bDateAdvanceLimit 
        && (ulDays > 0) )
    {
        wTmpDateAdvanceAmount += ulDays;
    }
    else
    {
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
        return ( pNextScreens[1] );
    }
    
    bDateAdvancePrompt = DATE_ADVANCE_SET_DAYS ;

    if ( fnFlashGetByteParm( BP_MAIL_DATE_CHANGE_CONFIRMATION_REQUIRED ) == TRUE )
    {
        bManualDateAdvanceType = DATE_ADVANCE_ADD_DAYS;
        usNext = pNextScreens[0];
    }
    else
    {
        usNext = fnhkDateAdvanceEnteredDays(bKeyCode, bNumNext - 1, &pNextScreens[1]);
    }
    
    //Added to fix fraca 227231.
    fnSetAutoDateAdvConfirmFlag(FALSE);
    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkDateAdvanceOneDay
//
// DESCRIPTION: 
//      Hard key function that works to advance printed day by one.
//
// INPUTS:
//      Standard hard key function inputs.
//
// OUTPUTS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//
//      18/09/2014  Renhao          Modified code to fix fraca 227272.
//      18/09/2014  Wang Biao       Modified code to fix fraca 227259.
//      22/07/2014  Renhao          Modified code to put up the "Date advance 
//                                  not allowed" screen when ANY of the rates/DCM 
//                                  files will expire.
//      05/05/2014  Renhao          Modified code to fix fraca 225188.
//      05/09/2007  Raymond Shen    Initial version.
//      03/04/2014  Renhao          Modified code for G922 Rates 
//                                  Expiration  new feature.
// *************************************************************************/
T_SCREEN_ID fnhkDateAdvanceOneDay(UINT8 bKeyCode,
                                 UINT8 bNumNext,
                                 T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = pNextScreens[0];
    SINT32  sCurrentDateAdvance;
    SINT32  sTmpDateAdvanceAmount;
    UINT8     bAllowManualDate;


    sCurrentDateAdvance = GetPrintedDateAdvanceDays();

    if ( sCurrentDateAdvance > 0 )
    {
          sCurrentDateAdvance = 0;
    }

    /* save the current date advance setting in case we need to revert later */ 
    sTmpDateAdvanceAmount = sCurrentDateAdvance + 1;
    bAllowManualDate = fnFlashGetByteParm(BP_DATE_ADVANCE_TYPE);

    /* attempt to set the printed date advance days to this value.  if this
        fails it must be because the number is out of range, so go to wNextScr2.
        if successful, continue processing. */
    if( SetPrintedDateAdvanceDays(sTmpDateAdvanceAmount) == SUCCESSFUL )
    {
        DATETIME    rPrintedDate;
        char       pPrintedDateAscii[9];
        /* determine if advancing the date the proposed number of days will cross a 
            rate effectivity boundary */
        (void)GetPrintedDate(&rPrintedDate, TRUE);
        (void)sprintf(pPrintedDateAscii, "%02d%02d", rPrintedDate.bCentury, 
                      rPrintedDate.bYear);
        (void)sprintf(pPrintedDateAscii+4, "%02d", rPrintedDate.bMonth);
        (void)sprintf(pPrintedDateAscii+6, "%02d", rPrintedDate.bDay);
        pPrintedDateAscii[8] = 0;

         //Indicate that there's no Rates or Dcaps Expired
	     CMOSRatesExpired = FALSE;
       
        if( fnCMOSRateCallback(TRUE, pPrintedDateAscii, FALSE, FALSE) )
        {           
            //if bAllowManualDate's B0 bit is 0, the code will run the original logic.
		    if( 0 == (bAllowManualDate & ALLOW_DATE_ADV_BF_RATE_EFF) )
		    {
		        /* if we are crossing a rate effectivity boundary, revert to the previous 
    		      date advance setting and choose next screen 3 */
    			SetPrintedDateAdvanceDays(sCurrentDateAdvance);
    			sDateAdvDaysPendingRates = sTmpDateAdvanceAmount;		/* save desired date advance amount in case user decides
    														to invoke the new rates */
    			 usNext = pNextScreens[1];
		    }
            else
		    {
                //user can NOT advance date.
                SetPrintedDateAdvanceDays(sCurrentDateAdvance);
                //show out of limit. so, the system can NOT be advanced date.
                usNext = pNextScreens[2];
			}
			//for fixing fraca 227259
			//when jump to screen 'RateActivatedByDateAdv'or 'ErrAdvanceDate',
			//it indicates the 'advance date' operation hasn't finished, and 
			// flags followed should be reset.
			CMOSRatesExpired = FALSE;
            bIfExpireAnyRates = FALSE; 
        }
        else
		{	
            if(0 == (bAllowManualDate & ALLOW_DATE_ADV_BF_ALL_RATES_EXP) || 
              ((ALLOW_DATE_ADV_BF_ALL_RATES_EXP == (bAllowManualDate & ALLOW_DATE_ADV_BF_ALL_RATES_EXP)) 
                   && TRUE == bIsExistAnyActiveRates ))
			  //still there are active rating files.
            {
             //For fraca 225188, if there's no active Rates or Dcap Modules
                if( bIfExpireAnyRates == TRUE )
                {
                       usNext = pNextScreens[3];// Go to sreecn error for rates will expire.
                       //user can NOT advance date.
                       SetPrintedDateAdvanceDays(sCurrentDateAdvance);
                       CMOSRatesExpired = FALSE;
                       bIfExpireAnyRates = FALSE;
                }
                else
                {
	                    usNext = pNextScreens[0];
	                    //some countries have no date advanced comfirm screen 
	                    //so we reset the flag here, see fraca17332
	                    if(fnFlashGetByteParm( BP_MAIL_DATE_CHANGE_CONFIRMATION_REQUIRED ) != TRUE)
	                    {
		                      AutoDateAdvanceActive = FALSE;
	                    }

	                    //After advancing some day, the system needs to update the current rates status
                        //using the current printing date
                        fnCMOSRateCallback(TRUE, pPrintedDateAscii, TRUE, FALSE);
                }

            }
            else  // it must be bNumPreviouslyActiveRates is zero.
            {               
				//there are rate files but all active files have been expired  
				if(TRUE == CMOSRatesExpired)
				{
				    usNext = pNextScreens[3];// Go to sreecn error for rates will expire.
                    //user can NOT advance date.
                    SetPrintedDateAdvanceDays(sCurrentDateAdvance);

                    bIsExistAnyActiveRates = TRUE;
                    CMOSRatesExpired = FALSE;

                }
                else
                {
                    //no any rates file at all, or all rates are still pending
                    usNext = pNextScreens[0]; // go to home screen.
                }
            }
		} 
    }

    if ( fnIsDateAdvanceChanged( ) == FALSE )
    {
        bDateAdvancePrompt = DATE_ADVANCE_RESET ;
    }

    return ( usNext );
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkDateAdvanceEnteredDays
//
// DESCRIPTION: 
//      Hard key function that works to set the number of date advance days.
//
// INPUTS:
//      Standard hard key function inputs.
//
// OUTPUTS:
//      None
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//      18/09/2014  Wang Biao       Modified code to fix fraca 227259.
//      22/07/2014  Renhao          Modified code to put up the "Date advance 
//                                  not allowed" screen when ANY of the rates/DCM 
//                                  files will expire.
//      05/05/2014  Renhao          Modified code to fix fraca 225188.
//      05/09/2007  Raymond Shen    Initial version.
//      03/04/2014  Renhao          Modified code for G922 Rates 
//                                  Expiration  new feature.
// *************************************************************************/
T_SCREEN_ID fnhkDateAdvanceEnteredDays(UINT8        bKeyCode,
                                    UINT8        bNumNext,
                                    T_SCREEN_ID *pNextScreens)

{
    T_SCREEN_ID usNext = pNextScreens[1];
    SINT16  sCurrentDateAdvance;
    UINT8     bAllowManualDate;
    

    /* save the current date advance setting in case we need to revert later */
    sCurrentDateAdvance = GetPrintedDateAdvanceDays();

    bAllowManualDate = fnFlashGetByteParm(BP_DATE_ADVANCE_TYPE);

    /* attempt to set the printed date advance days to this value.  if this
        fails it must be because the number is out of range, so go to wNextScr2.
        if successful, continue processing. */
    if (SetPrintedDateAdvanceDays(wTmpDateAdvanceAmount) == SUCCESSFUL)
    {
        DATETIME  rPrintedDate;
        char     pPrintedDateAscii[9];
        
        /* determine if advancing the date the proposed number of days will cross a 
            rate effectivity boundary */
        GetPrintedDate(&rPrintedDate, TRUE);
        sprintf(pPrintedDateAscii, "%02d%02d", rPrintedDate.bCentury, rPrintedDate.bYear);
        sprintf(pPrintedDateAscii+4, "%02d", rPrintedDate.bMonth);
        sprintf(pPrintedDateAscii+6, "%02d", rPrintedDate.bDay);
        pPrintedDateAscii[8] = 0;
        
        //Indicate that there's no Rates or Dcaps Expired
        CMOSRatesExpired = FALSE;

        if (fnCMOSRateCallback(TRUE, pPrintedDateAscii, FALSE, FALSE))
        {
            //if bAllowManualDate's B0 bit is 0, the code will run the original logic.
		    if( 0 == (bAllowManualDate & ALLOW_DATE_ADV_BF_RATE_EFF) )
		    {
		        /* if we are crossing a rate effectivity boundary, revert to the previous 
    		      date advance setting and choose next screen 3 */
    			fnClearEntryBuffer();
    			SetPrintedDateAdvanceDays(sCurrentDateAdvance);
    			sDateAdvDaysPendingRates = wTmpDateAdvanceAmount;		/* save desired date advance amount in case user decides
    														to invoke the new rates */
    			 usNext = pNextScreens[2];
		    }
            else
		    {
                //user can NOT advance date.
                SetPrintedDateAdvanceDays(sCurrentDateAdvance);
                //show out of limit. so, the system can NOT be advanced date.
                usNext = pNextScreens[3];
			}
			//for fixing fraca 227259
			//when jump to screen 'RateActivatedByDateAdv'or 'ErrAdvanceDate',
			//it indicates the 'advance date' operation hasn't finished, and 
			// flags followed should be reset.
			CMOSRatesExpired = FALSE;
            bIfExpireAnyRates = FALSE; 
        }
        else
		{	
            if(0 == (bAllowManualDate & ALLOW_DATE_ADV_BF_ALL_RATES_EXP) || 
              ((ALLOW_DATE_ADV_BF_ALL_RATES_EXP == (bAllowManualDate & ALLOW_DATE_ADV_BF_ALL_RATES_EXP)) 
                   && TRUE == bIsExistAnyActiveRates ))
			  //still there are active rating files.
            {
                //For fraca 225188, if there's no active Rates or Dcap Modules.
                if( bIfExpireAnyRates == TRUE )
                {
                    usNext = pNextScreens[4];// Go to sreecn error for rates will expire.
                    //user can NOT advance date.
                    SetPrintedDateAdvanceDays(sCurrentDateAdvance);	
                    CMOSRatesExpired = FALSE;
                    bIfExpireAnyRates = FALSE;                   
                }
                else
                {
                	usNext = pNextScreens[1];
                	//some countries have no date advanced comfirm screen 
                	//so we reset the flag here, see fraca17332
                	if(fnFlashGetByteParm( BP_MAIL_DATE_CHANGE_CONFIRMATION_REQUIRED ) != TRUE)
                	{
                		AutoDateAdvanceActive = FALSE;
                	}
                
                	//After advancing some day, the system needs to update the current rates status
                    //using the current printing date
                    fnCMOSRateCallback(TRUE, pPrintedDateAscii, TRUE, FALSE);
                }
            }
            else  // it must be bNumPreviouslyActiveRates is zero.
            {               
				//there are rate files but all active files have been expired  
				if(TRUE == CMOSRatesExpired)
				{
				    usNext = pNextScreens[4];// Go to sreecn error for rates will expire.
                    //user can NOT advance date.
                    SetPrintedDateAdvanceDays(sCurrentDateAdvance);

                    bIsExistAnyActiveRates = TRUE;
                    CMOSRatesExpired = FALSE;

                }
                else
                {
                    //no any rates file at all, or all rates are still pending
                    usNext = pNextScreens[1]; // go to home screen.
                }
            }
		}
    }
    else
    {
//        wTmpDateAdvanceAmount -= ulDays;
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
        return ( pNextScreens[0] );
    }

    if ( fnIsDateAdvanceChanged ( ) == FALSE)
    {
        bDateAdvancePrompt = DATE_ADVANCE_RESET ;
    }

    return ( usNext );
}


/* *************************************************************************
// FUNCTION NAME: fnhkManualDateAdvUseDate
//
// DESCRIPTION:
//         Accepts newest mannual set print date. 
//
// INPUTS:
//         Standard hard key function inputs (format 2).
//
// RETURNS:
//         Next screen
// WARNINGS/NOTES: 
//
// MODIFICATION HISTORY:               
//         05/09/2007    Raymond Shen    Initial version
//
// *************************************************************************/
T_SCREEN_ID  fnhkManualDateAdvUseDate(UINT8        bKeyCode,
                                    UINT8        bNumNext,
                                    T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = pNextScreens[1];

    switch(bManualDateAdvanceType)
    {
        case DATE_ADVANCE_ADD_ONE_DAY:
            usNext = fnhkDateAdvanceOneDay(bKeyCode, bNumNext-1, &pNextScreens[1]);
            break;
        case DATE_ADVANCE_ADD_DAYS:
            usNext = fnhkDateAdvanceEnteredDays(bKeyCode, bNumNext , pNextScreens);
            break;
        default:
            break;
    }

    return (usNext );
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnhkValidateNormalPresetTimer
//
// DESCRIPTION:
//         Hard key function that validates and stores the user input of 
//         normal preset allowable into CMOS
//
// INPUTS:
//         Standard hard key function inputs (format 1).
//
// RETURNS:
//
// WARNINGS/NOTES: 
//         if offset is within valid range, go to wNextScr1;
//         otherwise, go to wNextScr2
//
// MODIFICATION HISTORY:               
//                Victor Li     Initial version 
//    09/05/2012  Bob Li        fix fraca GMSE00169484.
//    02/13/2006  Kan Jiang     code cleanup
//    02/29/2006  John Gao      Business Rules Extraction
//    07/23/2009  Jingwei,Li    Fix fraca GMSE00165988.
// *************************************************************************/
T_SCREEN_ID fnhkValidateNormalPresetTimer(UINT8        bKeyCode, 
                                          UINT8        bNumNext, 
                                          T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID  usNext = 0;
    UINT32       ulNormalPresetTimer;
    BOOL         fStatus;
    if(fnIsBufferBlank() == FALSE)
    {
        fStatus = fnUnicode2Bin(rGenPurposeEbuf.pBuf, &ulNormalPresetTimer, fnBufferLen());
        if (fStatus == SUCCESSFUL)
        {
            if(fnValidateNormalPresetTimer(ulNormalPresetTimer) == TRUE)
            {
                if (bNumNext > 0)
                {
                    usNext = pNextScreens[0]; // Time & Timeouts screen
                }
            }

            else
            {
                fnClearEntryBuffer();
                fnSetEntryErrorCode(ENTRY_INVALID_VALUE);         
            }

        }
    }
    return (usNext); 
}




/* *************************************************************************
// FUNCTION NAME: 
//         fnhkValidateUserTimeOption
//
// DESCRIPTION:
//         Hard key function that validates and stores the clock timezone
//         adjustment.
//
// INPUTS:
//         Standard hard key function inputs (format 1).
//
// RETURNS:
//         Next screen
// WARNINGS/NOTES: 
//         if the offset derived from the time entered is within legal range, 
//            go to wNextScr1
//         if the drift is out of range, go to wNextScr2
//         if the value entered is not a valid time, stay on the same screen
//
// MODIFICATION HISTORY:               
//              Joe Mozdzer    Initial version 
//  02/13/2006  Kan Jiang      Code cleanup
// *************************************************************************/
T_SCREEN_ID fnhkValidateUserTimeOption( UINT8       bKeyCode, 
                                        T_SCREEN_ID wNextScr1, 
                                        T_SCREEN_ID wNextScr2 )
{
    T_SCREEN_ID  usNext = 0;  /*initialize to no screen change, which is what we do for illegal entry*/
    BOOL         fStatus;
    DATETIME     stDTTimeZone;
    UINT8        bCharsBuffered;
    UINT16      *pEntry;
    UINT32       ulHour;
    UINT32       ulMinutes;


    /* STEP 1: convert the time entered into binary hours and minutes */

    /* a valid time requires that 3 or 4 characters were entered */
    bCharsBuffered = fnCharsBuffered();
    if(bCharsBuffered == 0)
    {
//      wNext = fnhkNextScrOrNextOOBScr( bKeyCode, 0, &wNextScr1);
        return (usNext);
    }
    else if ((bCharsBuffered > 4) || (bCharsBuffered < 3))
    {
        return (usNext);
    }
    
    /* find the first number entered in the entry buffer */
    pEntry = rGenPurposeEbuf.pBuf;
    while ((*pEntry != (unsigned short) NULL) 
           && (fnIsUnicodeDigit(*pEntry) == FALSE))
    {
        pEntry++;
    }

    /* if we didn't find a number, ignore the hard key */
    if (*pEntry == (unsigned short) NULL)
    {
        return (usNext);
    }

    /* if it is a 2 digit hour, make sure the 2nd character is also a digit */
    if (bCharsBuffered == 4)
        if (fnIsUnicodeDigit(*(pEntry+1)) == FALSE)
        {
            return (usNext);
        }

    /* convert the first 1 or 2 digits (depending on if it was a 1 or 2 digit hour) to binary */    
    fStatus = fnUnicode2Bin(pEntry, &ulHour, (bCharsBuffered == 3) ? 1 : 2);

    /* advance the pointer to the where the minutes should be */
    if (bCharsBuffered == 3)
    {
        pEntry++;
    }
    else
    {
        pEntry += 2;
    }

    /* make sure there are 2 digits for the minutes */
    if((fnIsUnicodeDigit(*pEntry) == FALSE)||(fnIsUnicodeDigit(*(pEntry+1)) == FALSE))
    {
        usNext = 0;
        return (usNext);
    }

    /* convert minutes to binary */
    fStatus = fnUnicode2Bin(pEntry, &ulMinutes, 2);

    /* adjust the hour if this meter is configured to use 12 hour time w/ am & pm.  
        if applicable, the am/pm selection is stored in the binary choice field. */
    if(fnIsTimeFormat12() == TRUE)
    {
        if ((rGenPurposeBinChoiceField.bCurrentChoice == 1) && (ulHour != 12))
        {
            /* add 12 hours if PM... but not if it is 12 PM */
            ulHour += 12;
        }

        if ((rGenPurposeBinChoiceField.bCurrentChoice == 1) && (ulHour == 12))
        {
            /* subtract 12 hours if 12PM <- H. Liu*/  
            ulHour = 0;
        }
    }

    if(fnSetUserTime(ulHour, ulMinutes) == FALSE)
    {
        fnClearEntryBuffer();
        usNext = 0;  //  Need to define a new screen.
    }
    else
    {
        /* if we get to here, the drift value must be in range.  so set it and go to
        the wNextScr1 */
        usNext = wNextScr1;
    }


    return (usNext);    
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnhkEnableAutoDateAdv
//
// DESCRIPTION:
//         Hard key function that sets the auto date advance disabling
//         flag to FALSE;
//
// INPUTS:
//         Standard hard key function inputs (format 1).
//
// RETURNS:
//
// WARNINGS/NOTES: 
//          no screen change, and continue to search next hard key function.
//
// MODIFICATION HISTORY:               
//              Victor Li      Initial version 
//  02/13/2006  Kan Jiang      Code cleanup
// *************************************************************************/
T_SCREEN_ID  fnhkEnableAutoDateAdv( UINT8       bKeyCode, 
                                    T_SCREEN_ID wNextScr1, 
                                    T_SCREEN_ID wNextScr2 )
{
    if (fnFlashGetByteParm(BP_TIME_FORMAT) == TIME_FORMAT_12)
    {
        bAutoDateAdvDisabling = FALSE;
    }

    return (0);
}



/* *************************************************************************
// FUNCTION NAME: 
//         fnhkEnableAutoDateAdv2
//
// DESCRIPTION:
//         Hard key function that sets the auto date advance disabling
//         flag to FALSE;
//
// INPUTS:
//         Standard hard key function inputs (format 1).
//
// RETURNS:
//
// WARNINGS/NOTES: 
//         no screen change, and continue to search next hard key function.
//
// MODIFICATION HISTORY:               
//               Victor Li     Initial version
//   02/13/2006  Kan Jiang     Code cleanup
// *************************************************************************/
T_SCREEN_ID  fnhkEnableAutoDateAdv2( UINT8       bKeyCode, 
                                     T_SCREEN_ID wNextScr1, 
                                     T_SCREEN_ID wNextScr2 )
{
    // no more chars will be buffered if the buffer is full
    if (fnCharsBuffered() < fnBufferLen())
    {
        bAutoDateAdvDisabling = FALSE;
    }

    return (0);
}




/* *************************************************************************
// FUNCTION NAME: 
//          fnhkValidateAutoDateAdvTime
//
// DESCRIPTION:
//          Hard key function that validates and stores the auto date advance time.
//
// INPUTS:
//          Standard hard key function inputs (format 1).
//
// RETURNS:
//          Next screen
//
// WARNINGS/NOTES: 
//          if a valid time was entered, go to wNextScr1, 
//          otherwise, stay on the same screen
//
// MODIFICATION HISTORY: 
//  04/03/2014   Renhao            Updated for G922 Rates Expiration  new feature.
//  06/21/2006   Kan Jiang         Modify for fixing FRACA 99252 and 97613
//  05/18/2006  James Wang      To judge time format and disable auto date 
//                              advance with some new BR functions.  
//              Mozdzer         Initial version
// *************************************************************************/
T_SCREEN_ID  fnhkValidateAutoDateAdvTime( UINT8       bKeyCode, 
                                          T_SCREEN_ID wNextScr1, 
                                          T_SCREEN_ID wNextScr2 )
{
    T_SCREEN_ID usNext = 0; /* initialize to no screen change, which is what we do for illegal entry */
    BOOL        fStatus;
    DATETIME    rNewADA;
    UINT16     *pEntry;
    UINT32      ulHour;
    UINT32      ulMinutes;
    UINT8       bCharsBuffered;
    UINT32  ulOldCMOSAutoTime = fnCMOSSetupGetCMOSSetupParams()->AutoAdvTime;
	UINT16  wAutoAdvEMDParam = fnFlashGetWordParm(WP_AUTO_DATE_ADVANCE_OPTIONS);

    
    /* STEP 1: convert the time entered into binary hours and minutes */

    /* a valid time requires that 3 or 4 characters were entered */
    bCharsBuffered = fnCharsBuffered();

    // disable the auto date advance
    if (bAutoDateAdvDisabling && bCharsBuffered > 0)
    {
         if(fnDisableDateAdv() == TRUE)
             bDateAdvancePrompt = DATE_ADVANCE_FROM_MIDNIGHT;

         (void)fnSetAutoAdvanceTimeEvent();
         usNext = wNextScr1;

         return (usNext);
    }

    if ((bCharsBuffered > 4) || (bCharsBuffered < 3))
    {
        return (usNext);
    }
    
    /* find the first number entered in the entry buffer */
    pEntry = rGenPurposeEbuf.pBuf;
    while ((*pEntry != (UINT16)NULL) && (fnIsUnicodeDigit(*pEntry) == FALSE))
    {
        pEntry++;
    }

    /* if we didn't find a number, ignore the hard key */
    if (*pEntry == (UINT16) NULL)
    {
        return (usNext);
    }

    /* if it is a 2 digit hour, make sure the 2nd character is also a digit */
    if (bCharsBuffered == 4)
    {
        if (fnIsUnicodeDigit(*(pEntry+1)) == FALSE)
        {
            return (usNext);
        }
    }

    /* convert the first 1 or 2 digits (depending on if it was a 1 or 2 digit hour) to binary */    
    fStatus = fnUnicode2Bin(pEntry, &ulHour, (bCharsBuffered == 3) ? 1 : 2);

    /* advance the pointer to the where the minutes should be */
    if (bCharsBuffered == 3)
    {
        pEntry++;
    }
    else
    {
        pEntry += 2;
    }

    /* make sure there are 2 digits for the minutes */
    if ((fnIsUnicodeDigit(*pEntry) == FALSE)||(fnIsUnicodeDigit(*(pEntry+1)) == FALSE))
    {
        usNext = 0;
        return (usNext);
    }

    /* convert minutes to binary */
    fStatus = fnUnicode2Bin(pEntry, &ulMinutes, 2);

    /* adjust the hour if this meter is configured to use 12 hour time w/ am & pm.  
        if applicable, the am/pm selection is stored in the binary choice field. */
    if (fnIsTimeFormat12() == TRUE)
    {
        if ((rGenPurposeBinChoiceField.bCurrentChoice == 1) && (ulHour != 12))
        {
            /* add 12 hours if PM... but not if it is 12 PM */
            ulHour += 12;
        }
        if ((rGenPurposeBinChoiceField.bCurrentChoice == 0) && (ulHour == 12))
        {
            /* subtract 12 hours if 12AM */
            ulHour = 0;
        }
    }

    /* validate the hours and minutes */
    if ((ulHour > 23) || (ulMinutes > 59))
    {
        return (usNext);
    }
	
	/* if auto advance date is mandatory, we should not allow midnight 00:00 input */
	if(wAutoAdvEMDParam & AUTO_ADV_DATE_IS_MANDATORY)
	{
         if ((ulHour ==0) && (ulMinutes == 0))
        {
            usNext = 0;
    	    fnClearEntryBuffer();
            return (usNext);
        }
	}

    rNewADA.bHour    = (UINT8) ulHour;
    rNewADA.bMinutes = (UINT8) ulMinutes;
    rNewADA.bSeconds = 0;

    fnCMOSSetupGetCMOSSetupParams()->AutoAdvTime = fnTodToSecs(&rNewADA);

    if( fnIsDateAdvanceChanged( ) == TRUE )
    {
        long     currTimeInSecs;
        DATETIME theCurrDateTime;

        (void) GetSysDateTime (&theCurrDateTime, ADDOFFSETS, GREGORIAN);
        currTimeInSecs = fnTodToSecs ( &theCurrDateTime );

        if( GetPrintedDateAdvanceDays() == 1 
            && bDateAdvancePrompt == DATE_ADVANCE_TIME_OF_DAY
            && currTimeInSecs >= ulOldCMOSAutoTime )
        {
            bDateAdvancePrompt = DATE_ADVANCE_FROM_MIDNIGHT ;

            /* no need to check return val since this can't fail */
            (void) SetPrintedDateAdvanceDays(0); 
        }
    }
       
    (void)fnSetAutoAdvanceTimeEvent();
    usNext = wNextScr1;

    return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: 
//         fnhkValidateSleepTimeout
//
// DESCRIPTION:
//         Hard key function that validates and stores the sleep timeout
//         setting (in minutes).
//
// INPUTS:
//         Standard hard key function inputs (format 2).
//
// RETURNS:
//         Next screen
//
// WARNINGS/NOTES: 
//         If a valid value entered, go to next screen 1
//         If an illegal value entered, go to next screen 2
//
// MODIFICATION HISTORY:               
//              Mozdzer      Initial version 
// 02/13/2006  Kan Jiang     Code Cleanup
// 02/29/2006   John Gao     Business Rules Extraction
// 07/23/2009  Jingwei,Li    Fix fraca GMSE00165988.
// 09/05/2012  Bob Li        fix fraca GMSE00169484.
// *************************************************************************/
T_SCREEN_ID fnhkValidateSleepTimeout(UINT8 bKeyCode,
                                     UINT8 bNumNext,
                                     T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;
    UINT32      ulSleepTimeout;
    BOOL        fStatus = 0;
    if(fnIsBufferBlank() == FALSE)
    {
        fStatus = fnUnicode2Bin(rGenPurposeEbuf.pBuf, &ulSleepTimeout, fnBufferLen());

        if ((fStatus == TRUE) && (fnValidateSleepTimer(ulSleepTimeout) == TRUE))
        {
            fValidTimeOutValue = TRUE;
            if (bNumNext != 0)
            {
                usNext = pNextScreens[0];
            }
        }
        else
        {
            fValidTimeOutValue = FALSE;
            fnClearEntryBuffer();
            fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
            
            if (bNumNext > 1)
            {
                usNext = pNextScreens[1];
            }
            else
            {
                usNext = 0;
            }
        }
    }
    return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: 
//         fnCheckValidDriftTime
//
// DESCRIPTION:
//         Check whether the drift time is in range or not.
//
// INPUTS:
//         ulHours, ulMinutes: hour and minute that input by user.
//         stDTSysTime: system time without drift time.
//
// RETURNS:
//         Next screen
//
// WARNINGS/NOTES: 
//
// MODIFICATION HISTORY: 
//     09/04/2006   Kan Jiang    Modified for fixing Fraca 103437              
//     04/17/2006   Kan Jiang    Initial version
// *************************************************************************/
static T_SCREEN_ID fnCheckValidDriftTime(UINT32    ulHours,  
                                         UINT32    ulMinutes, 
                                         DATETIME  stDTSysTime,
                                         T_SCREEN_ID wNextScr1, 
                                         T_SCREEN_ID wNextScr2 )
{
     SINT32       lTmpAdjustTime = 0;
     SINT32       lDateDiff;
     T_SCREEN_ID  usNext = 0;  /* initialize to no screen change */

     memcpy(&stDTNewTime, &stDTSysTime, sizeof(DATETIME));
    
    /* copy the time entered into a structure with today's date */
    stDTNewTime.bHour    = (UINT8) ulHours;
    stDTNewTime.bMinutes = (UINT8) ulMinutes;
    
    CalcDateTimeDifference(&stDTSysTime, &stDTNewTime, &lDateDiff);
    
    lTmpAdjustTime = lDateDiff/60 ;

    if ( (lTmpAdjustTime >= -MAX_DRIFT_VAL) && (lTmpAdjustTime <= MAX_DRIFT_VAL) )
    {    
          usNext = wNextScr1;
          return usNext;
    }
    else if (fnIsTimeFormat12() == TRUE)
    {
          stDTNewTime.bHour    = (UINT8) ulHours +12;
          stDTNewTime.bMinutes = (UINT8) ulMinutes;
          if( stDTNewTime.bHour == 24 )
          {
                stDTNewTime.bHour = 0;
          }

          CalcDateTimeDifference(&stDTSysTime, &stDTNewTime, &lDateDiff);
    
          lTmpAdjustTime = lDateDiff/60 ;
          if( (lTmpAdjustTime >= -MAX_DRIFT_VAL) && (lTmpAdjustTime <= MAX_DRIFT_VAL) )
          {
                usNext = wNextScr1;
          }
          else
          {
                fnClearEntryBuffer();
                fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
                usNext = 0; 
          }
    }
    else
    {
          fnClearEntryBuffer();
          fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
          usNext = 0; 
    }

    return usNext;
    
}



/* *************************************************************************
// FUNCTION NAME: 
//         fnhkValidateDriftTime
//
// DESCRIPTION:
//         Hard key function that validates the clock drift adjustment.
//
// INPUTS:
//         Standard hard key function inputs (format 1).
//
// RETURNS:
//         Next screen
// WARNINGS/NOTES: 
//
// MODIFICATION HISTORY:
//  2010.08.11 Clarisa Bellamy  Added detection of time change that crosses 
//                      midnight either way, and save the dateTime when it
//                      happens.  Updated comments. 
//     09/04/2006   Kan Jiang    Modified for fixing Fraca 103437               
//     04/17/2006   Kan Jiang      Initial version
// *************************************************************************/
T_SCREEN_ID fnhkValidateDriftTime( UINT8       bKeyCode, 
                                   T_SCREEN_ID wNextScr1, 
                                   T_SCREEN_ID wNextScr2 )
{
    T_SCREEN_ID  usNext = 0;  /* initialize to no screen change */
    BOOL         fStatus;
    DATETIME     stDTSysTime;
    DATETIME     stDTAddTime;
    DATETIME     stDTSubTime;
    UINT8        bToday;
    UINT8        bCharsBuffered;
    UINT16      *pEntry;
    UINT32       lwHour;
    UINT32       lwMinutes;
    SINT32       lTmpDrift;


    memset(&stDTNewTime, 0, sizeof(DATETIME));
    /* a valid time requires that 3 or 4 characters were entered */
    bCharsBuffered = fnCharsBuffered();
    if(bCharsBuffered == 0)
    {
        return (usNext);
    }
    else if ((bCharsBuffered > 4) || (bCharsBuffered < 3))
    {
        return (usNext);
    }
    
    /* find the first number entered in the entry buffer */
    pEntry = rGenPurposeEbuf.pBuf;
    while((*pEntry != (unsigned short) NULL) 
          && (fnIsUnicodeDigit(*pEntry) == FALSE))
    {
        pEntry++;
    }

    /* if we didn't find a number, ignore the hard key */
    if (*pEntry == (unsigned short) NULL)
    {
        return (usNext);
    }

    /* if it is a 2 digit hour, make sure the 2nd character is also a digit */
    if (bCharsBuffered == 4)
        if (fnIsUnicodeDigit(*(pEntry+1)) == FALSE)
        {
            return (usNext);
        }

    /* convert the first 1 or 2 digits (depending on if it was a 1 or 2 digit hour) to binary */    
    fStatus = fnUnicode2Bin(pEntry, &lwHour, (bCharsBuffered == 3) ? 1 : 2);

    /* advance the pointer to the where the minutes should be */
    if (bCharsBuffered == 3)
    {
        pEntry++;
    }
    else
    {
        pEntry += 2;
    }

    /* make sure there are 2 digits for the minutes */
    if((fnIsUnicodeDigit(*pEntry) == FALSE)||(fnIsUnicodeDigit(*(pEntry+1)) == FALSE))
    {
        usNext = 0;
        return (usNext);
    }

    /* convert minutes to binary */
    fStatus = fnUnicode2Bin(pEntry, &lwMinutes, 2);

    /* validate the hours and minutes */
    if(fnIsTimeFormat12() == TRUE)
    {
        if ((lwHour > 12) || (lwMinutes > 59))
    {
           fnClearEntryBuffer();
           fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
           usNext = 0; 
           return (usNext);
       }
    }
    else if ((lwHour > 23) || (lwMinutes > 59))
    {
         fnClearEntryBuffer();
         fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
         usNext = 0; 
         return (usNext);
    }

    // Get the current day, so we can determine if we set the time on the other side
    //  of midnight.
    GetSysDateTime( &stDTSysTime, ADDOFFSETS, GREGORIAN );
    bToday = stDTSysTime.bDay;
    
    // Now get the current dateTime with NO drift.
    lTmpDrift = fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift ;
    
    fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift = 0;
    GetSysDateTime(&stDTSysTime, ADDOFFSETS, GREGORIAN);
    
    fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift = MAX_DRIFT_VAL;
    GetSysDateTime(&stDTAddTime, ADDOFFSETS, GREGORIAN);
    
    fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift = -MAX_DRIFT_VAL;
    GetSysDateTime(&stDTSubTime, ADDOFFSETS, GREGORIAN);
    
    fnCMOSSetupGetCMOSSetupParams()->ClockOffset.Drift = lTmpDrift;
    
    if(fnIsTimeFormat12() == TRUE)
    {
        // 12-hour clock...
        if (stDTAddTime.bDay == stDTSubTime.bDay) 
        {
            // Min time and max time are on the same day, so verification is simple.
             usNext = fnCheckValidDriftTime(lwHour, lwMinutes, stDTSysTime, wNextScr1, wNextScr1);
        }
        // Min & max time straddle midnight... 
        else if( ((lwHour+12)*60+lwMinutes) >= (stDTSubTime.bHour*60 + stDTSubTime.bMinutes) )
        {
            // New time is before midnight...
            memcpy(&stDTNewTime, &stDTSubTime, sizeof(DATETIME));

            /* copy the time entered into a structure with today's date */
            stDTNewTime.bHour = (UINT8) lwHour + 12;
            stDTNewTime.bMinutes = (UINT8) lwMinutes; 
            usNext = wNextScr1;
        }
        else if ( (lwHour*60+lwMinutes) <= (stDTAddTime.bHour*60 + stDTAddTime.bMinutes) )
        {
            // New time is after midnight...
            memcpy(&stDTNewTime, &stDTAddTime, sizeof(DATETIME));

            /* copy the time entered into a structure with today's date */
            stDTNewTime.bHour = (UINT8) lwHour;
            stDTNewTime.bMinutes = (UINT8) lwMinutes; 
            usNext = wNextScr1;     
        }
        else
        {
            // New time is out of range.
            fnClearEntryBuffer();
            fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
            usNext = 0;         
        }
    }
    else
    {
        // 24-hour clock...
        if (stDTAddTime.bDay == stDTSubTime.bDay) 
        {
            // Min time and max time are on the same day, so verification is simple.
             usNext = fnCheckValidDriftTime(lwHour, lwMinutes, stDTSysTime, wNextScr1, wNextScr1);
        }
        // Min time and max time stradle midnight...
        else if (((lwHour*60+lwMinutes) <= ((stDTAddTime.bHour+24)*60 + stDTAddTime.bMinutes) ) &&
            ( (lwHour*60+lwMinutes) >= ((stDTSubTime.bHour)*60 + stDTSubTime.bMinutes) ) )
        {
            // New time is before midnight...
            memcpy(&stDTNewTime, &stDTSubTime, sizeof(DATETIME));

            /* copy the time entered into a structure with today's date */
            stDTNewTime.bHour = (UINT8) lwHour;
            stDTNewTime.bMinutes = (UINT8) lwMinutes; 
            usNext = wNextScr1;     
        }
        else if(((lwHour*60+lwMinutes) <= (stDTAddTime.bHour*60 + stDTAddTime.bMinutes) ) &&
                (((lwHour+24)*60+lwMinutes) >= ((stDTSubTime.bHour)*60 + stDTSubTime.bMinutes) ) )
        {
            // New time is after midnight...
            memcpy(&stDTNewTime, &stDTAddTime, sizeof(DATETIME));

            /* copy the time entered into a structure with today's date */
            stDTNewTime.bHour = (UINT8) lwHour;
            stDTNewTime.bMinutes = (UINT8) lwMinutes; 
            usNext = wNextScr1;     
        }
        else
        {
            // New time is out of range.
            fnClearEntryBuffer();
            fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
            usNext = 0;         
        }
    }
    
    // If the date will change while adjusting the drift, it could give a false
    //  missed midnight error when we next go to the main screen. Set the flag
    //  to indicate this.
    if( (usNext != 0) && (stDTNewTime.bDay != bToday) )
    {
        // Set the temp flag, and set the date that we set the flag.
        fDriftPastMidnightTemp = TRUE;
        (void)GetSysDateTime( &stDTDriftPastMidnightTemp, ADDOFFSETS, GREGORIAN );
    }

    return (usNext);    
}



/* *************************************************************************
// FUNCTION NAME: 
//         fnhkValidateClockDriftAndSaving
//
// DESCRIPTION:
//         Hard key function that validates and stores a PSD Clock Drift Offset.
//       Drift adjustment is bounded to +/- 120 minutes
//
// INPUTS:
//         Standard hard key function inputs (format 2).
//
// RETURNS:
//         Next screen
// WARNINGS/NOTES: 
//
// MODIFICATION HISTORY:               
//  2010.08.11 Clarisa Bellamy - Save the temp crossed-midnight data, now that 
//                          the user has confirmed the change.
//     04/17/2006   Kan Jiang      Initial version
// *************************************************************************/
T_SCREEN_ID fnhkValidateClockDriftAndSaving(UINT8        bKeyCode,
                                            UINT8        bNumNext,
                                            T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;
    DATETIME dtNow;
        
    GetSysDateTime(&dtNow, ADDOFFSETS, GREGORIAN);
    stDTNewTime.bSeconds = dtNow.bSeconds;
      
    memcpy( &stDTDriftPastMidnight, &stDTDriftPastMidnightTemp, sizeof( DATETIME ) );
    fDriftPastMidnight = fDriftPastMidnightTemp;
    if (fnUserAdjustRTC(&stDTNewTime, ADJUST_DRIFT) == IN_RANGE) 
    {
        //return to option date time screen
        usNext = pNextScreens[0];
    }
    else
    {
        usNext = pNextScreens[1];
    }

    return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: fnhkAutoDateAdvUseDate
//
// DESCRIPTION:
//         Accepts newest set print date from user. 
//
// INPUTS:
//         Standard hard key function inputs (format 2).
//
// RETURNS:
//         Next screen
// WARNINGS/NOTES: 
//
// MODIFICATION HISTORY:
//         04/15/2014    Wang Biao    Modified code for issue about wrong date 
//                                    displayed in the main screen when press 
//                                    soft key to accept the auto advance date 
//                                    changed after midnight.
//         07/07/2006    Kan Jiang    Initial version
// *************************************************************************/
T_SCREEN_ID  fnhkAutoDateAdvUseDate(UINT8        bKeyCode,
                                    UINT8        bNumNext,
                                    T_SCREEN_ID *pNextScreens)
{
     T_SCREEN_ID usNext = pNextScreens[0];
     DATETIME   rPrintedDateTime;
     char      pPrintedDateAscii[9];
     DATETIME   newEvent;
     BOOL       UpdateMode=TRUE;
	 SINT32      currTimeInSecs;
     DATETIME    theCurrDateTime;

     bDateAdvancePrompt = DATE_ADVANCE_TIME_OF_DAY ;
     fnSaveOldDateAdvanceAmount();

	 (void)GetSysDateTime (&theCurrDateTime, ADDOFFSETS, GREGORIAN);
	 
      currTimeInSecs = fnTodToSecs ( &theCurrDateTime );
    
	if (currTimeInSecs < CMOSSetupParams.AutoAdvTime) 
	{					
		(void)SetPrintedDateAdvanceDays(0);
	}
	else
	{
		(void)SetPrintedDateAdvanceDays(1);
	}

     AutoDateAdvanceActive = TRUE;  
       
     if (GetPrintedDate(&rPrintedDateTime, GREGORIAN) != SUCCESSFUL)
     {
         return usNext;
     }
       
     sprintf(pPrintedDateAscii, "%02d%02d%02d%02d", rPrintedDateTime.bCentury, 
             rPrintedDateTime.bYear, rPrintedDateTime.bMonth, 
             rPrintedDateTime.bDay);

     pPrintedDateAscii[8] = 0;
    
     //Check for and install any pending rate updates
     (void) fnCMOSRateCallback(TRUE, pPrintedDateAscii, TRUE, FALSE);
     OSSendIntertask (OIT, SYS, OIT_UPDATE_DISPLAY, BYTE_DATA, &UpdateMode, 1);
       
     fnSetAutoDateAdvConfirmFlag(FALSE);

     return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: fnhkAutoDateAdvDoNotUseDate
//
// DESCRIPTION:
//         User selects not to use new date.
//
// INPUTS:
//         Standard hard key function inputs (format 2).
//
// RETURNS:
//         Next screen
// WARNINGS/NOTES: 
//
// MODIFICATION HISTORY:
//     04/03/2014   Renhao      Updated code for G922 Rates Expiration  
//                              new feature.
//     07/07/2006   Kan Jiang   Initial version
// *************************************************************************/
T_SCREEN_ID fnhkAutoDateAdvDoNotUseDate(UINT8        bKeyCode,
                                        UINT8        bNumNext,
                                        T_SCREEN_ID *pNextScreens)
{
    UINT32	        lwDays;
	BOOL		    fStatus;
	UINT16	        wWhichNext = 0xFF;
	short			sCurrentDateAdvance;
	DATETIME       rPrintedDate;
	char	        pPrintedDateAscii[9];
    UINT16         wAutoAdvEMDParam = fnFlashGetWordParm(WP_AUTO_DATE_ADVANCE_OPTIONS);


	if (((DATE_ADVANCE_TIME_OF_DAY == bDateAdvancePrompt) 
	            && ( (CMOSSetupParams.AutoAdvTime == 0 )
	                || ( (CMOSSetupParams.AutoAdvTime != 0 ) && (wAutoAdvEMDParam & AUTO_ADV_DATE_IS_MANDATORY))))  // if auto advance time is midnight or not midnight with mandatory condition
			|| DATE_ADVANCE_FROM_MIDNIGHT == bDateAdvancePrompt
			|| DATE_ADVANCE_FORCE_TO_TODAY == bDateAdvancePrompt
			|| DATE_ADVANCE_CONFIRMED == bDateAdvancePrompt 
			|| ((wAutoAdvEMDParam & AUTO_ADV_DATE_IS_MANDATORY) && (DATE_ADVANCE_FROM_PRESET == bDateAdvancePrompt)))
	{
		return ( 0 ) ;
	}
    else 
    { 
        /* revert the date advance */ 
		(void)SetPrintedDateAdvanceDays(0); 
		
        bDateAdvancePrompt = DATE_ADVANCE_RESET ; 
		
		fnSetAutoDateAdvConfirmFlag(FALSE);
		
		(void)GetPrintedDate(&rPrintedDate, TRUE);
		(void)sprintf(pPrintedDateAscii, "%02d%02d", rPrintedDate.bCentury, rPrintedDate.bYear);
		(void)sprintf(pPrintedDateAscii+4, "%02d", rPrintedDate.bMonth);
		(void)sprintf(pPrintedDateAscii+6, "%02d", rPrintedDate.bDay);
		pPrintedDateAscii[8] = 0;
		
		(void) fnCMOSRateCallback(TRUE, pPrintedDateAscii, TRUE, FALSE);

        return ( pNextScreens[0] );
	}
}

/* *************************************************************************
// FUNCTION NAME: 
//         fnhkValidateWaitForEnvTimeout
//
// DESCRIPTION:
//      Hard key function that validates and stores the wait-for-envelope timeout
//      setting (in seconds).
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      Next screen
//      If a valid value entered, go to next screen 1
//      Else, no screen change
//
// WARNINGS/NOTES: 
//      None.
//
// MODIFICATION HISTORY:               
//  07/23/2009  Jingwei,Li    Fix fraca GMSE00165988.
//  08/29/2006  Vincent Yi      Initial version 
//
// *************************************************************************/
T_SCREEN_ID fnhkValidateWaitForEnvTimeout(UINT8 bKeyCode,
                                          UINT8 bNumNext,
                                          T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID     usNext = 0;
    UINT32          ulInputTimeout;
    BOOL            fStatus = 0;
    if(fnIsBufferBlank() == FALSE)
    {
    fStatus = fnUnicode2Bin(rGenPurposeEbuf.pBuf, &ulInputTimeout, fnBufferLen());

    if ((fStatus == TRUE) && 
        (fnValidateMailInactivityTimer(ulInputTimeout) == TRUE))
    {
        if (bNumNext != 0)
        {
            usNext = pNextScreens[0];
        }
    }
    else
    {
        // Clear the entry buffer
        fnClearEntryBuffer();
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
    }
    }
    return (usNext);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnhkDateAdvUseNewRates
//
// DESCRIPTION:
//      
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//
// WARNINGS/NOTES: 
//      None.
//
// MODIFICATION HISTORY:    
//  01/10/2008    Oscar Wang        Fix FRACA GMSE00135544: since FP doesn't 
//                                  prefer to do rating by default class, we 
//                                  will clear current class/postage info when 
//                                  using new rating.
//  09/07/2006    Oscar Wang        Adapted for Future Phoenix
//                Joe Mozdzer       Initial version 
// *************************************************************************/
T_SCREEN_ID fnhkDateAdvUseNewRates(UINT8            bKeyCode,
                                       UINT8            bNumNext,
                                       T_SCREEN_ID *    pNextScreens)
{
    UINT16  usWhichNext = 0;
    
    if (SetPrintedDateAdvanceDays(sDateAdvDaysPendingRates) == SUCCESSFUL)
    {
        DATETIME    rPrintedDate;
        char       pPrintedDateAscii[9];

  //            DataSetNextScr = enmDateset;
        /* make the new rates effectivity */
        GetPrintedDate(&rPrintedDate, TRUE);
        sprintf(pPrintedDateAscii, "%02d%02d%02d%02d", rPrintedDate.bCentury, rPrintedDate.bYear,
                 rPrintedDate.bMonth, rPrintedDate.bDay);
        pPrintedDateAscii[8] = 0;

        (void) fnCMOSRateCallback(TRUE, pPrintedDateAscii, TRUE, FALSE);

        //if ( fnRMInitialized() != FALSE)
        if (fnRateRMInitialized()==true)
        {        
            fnRatingSetDefault();
        }
        
        //go to normal preset warning screen
        if(fnNormalPresetIsKIPMode() == TRUE)
        {
            usWhichNext = pNextScreens[1];
        }
        //go to home screen.
        else
        {
            usWhichNext = pNextScreens[0];
        }
    }

    return usWhichNext;
}



/* *************************************************************************
// FUNCTION NAME: fnhkClearDSTUpdateFlag
//
// DESCRIPTION: Hard key function that clears the flag indicating we need
//              to show the daylight savings time updated screen.
//      
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      Always goes to next screen 1
//
// WARNINGS/NOTES: 
//      None.
//
// MODIFICATION HISTORY: 
//      12/05/2006   Kan Jiang   Code cleanup              
//                  Joe Mozdzer  Initial version 
// *************************************************************************/
T_SCREEN_ID fnhkClearDSTUpdateFlag(UINT8        bKeyCode,
                                   UINT8        bNumNext,
                                   T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID  usNext = 0;

    fCMOSDSTAutoUpdated = FALSE;    

    if (bNumNext > 0)
    {
        usNext = pNextScreens[0];
    }
    
    return (usNext);
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnhkValidateZWZPTimeout
//
// DESCRIPTION:
//      Hard key function that validates and stores the ZWZP timeout
//      setting (in seconds).
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      Next screen
//      If a valid value entered, go to next screen 1
//      Else, no screen change
//      
//
// WARNINGS/NOTES: 
//      None.
//
// MODIFICATION HISTORY:               
//    07/23/2009  Jingwei,Li    Fix fraca GMSE00165988.
//      26 Mar 2007 Bill Herring    Initial
//
//
// *************************************************************************/
T_SCREEN_ID fnhkValidateZWZPTimeout(UINT8 bKeyCode,
                                    UINT8 bNumNext,
                                    T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID     usNext = 0;
    UINT32          ulInputTimeout;
    BOOL            fStatus = 0;
    if(fnIsBufferBlank() == FALSE)
    {
    fStatus = fnUnicode2Bin(rGenPurposeEbuf.pBuf, &ulInputTimeout, fnBufferLen());

    if ((fStatus == TRUE) && 
        (fnValidateZWZPTimer(ulInputTimeout) == TRUE))
    {
        if (bNumNext != 0)
        {
            usNext = pNextScreens[0];
        }
    }
    else
    {
        // Clear the entry buffer
        fnClearEntryBuffer();
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE); 
    }
    }
    return (usNext);
}


/* *************************************************************************
// FUNCTION NAME: 
//          fnhkValidateDHOnTime
//
// DESCRIPTION:
//          Hard key function that validates and stores the DH On time.
//
// INPUTS:
//          Standard hard key function inputs (format 1).
//
// RETURNS:
//          Next screen
//
// WARNINGS/NOTES: 
//          if a valid time was entered, go to wNextScr1, 
//          otherwise, stay on the same screen
//
// MODIFICATION HISTORY:               
//  02/28/2008  Joey Cui        Initial version
// *************************************************************************/
T_SCREEN_ID  fnhkValidateDHOnTime(  UINT8       bKeyCode, 
                                    T_SCREEN_ID wNextScr1, 
                                    T_SCREEN_ID wNextScr2 )
{
    T_SCREEN_ID usNext = 0; /* initialize to no screen change, which is what we do for illegal entry */
    BOOL        fStatus;
    DATETIME    rNewDateTime;
    UINT16     *pEntry;
    UINT32      ulHour;
    UINT32      ulMinutes;
    UINT8       bCharsBuffered;
    
    /* STEP 1: convert the time entered into binary hours and minutes */

    /* a valid time requires that 3 or 4 characters were entered */
    bCharsBuffered = fnCharsBuffered();

    if ((bCharsBuffered > 4) || (bCharsBuffered < 3))
    {
        return (usNext);
    }
    
    /* find the first number entered in the entry buffer */
    pEntry = rGenPurposeEbuf.pBuf;
    while ((*pEntry != (UINT16)NULL) && (fnIsUnicodeDigit(*pEntry) == FALSE))
    {
        pEntry++;
    }

    /* if we didn't find a number, ignore the hard key */
    if (*pEntry == (UINT16) NULL)
    {
        return (usNext);
    }

    /* if it is a 2 digit hour, make sure the 2nd character is also a digit */
    if (bCharsBuffered == 4)
    {
        if (fnIsUnicodeDigit(*(pEntry+1)) == FALSE)
        {
            return (usNext);
        }
    }

    /* convert the first 1 or 2 digits (depending on if it was a 1 or 2 digit hour) to binary */    
    fStatus = fnUnicode2Bin(pEntry, &ulHour, (bCharsBuffered == 3) ? 1 : 2);

    /* advance the pointer to the where the minutes should be */
    if (bCharsBuffered == 3)
    {
        pEntry++;
    }
    else
    {
        pEntry += 2;
    }

    /* make sure there are 2 digits for the minutes */
    if ((fnIsUnicodeDigit(*pEntry) == FALSE)||(fnIsUnicodeDigit(*(pEntry+1)) == FALSE))
    {
        usNext = 0;
        return (usNext);
    }

    /* convert minutes to binary */
    fStatus = fnUnicode2Bin(pEntry, &ulMinutes, 2);

    /* adjust the hour if this meter is configured to use 12 hour time w/ am & pm.  
        if applicable, the am/pm selection is stored in the binary choice field. */
    if (fnIsTimeFormat12() == TRUE)
    {
        if ((rGenPurposeBinChoiceField.bCurrentChoice == 1) && (ulHour != 12))
        {
            /* add 12 hours if PM... but not if it is 12 PM */
            ulHour += 12;
        }
        if ((rGenPurposeBinChoiceField.bCurrentChoice == 0) && (ulHour == 12))
        {
            /* subtract 12 hours if 12AM */
            ulHour = 0;
        }
    }

    /* validate the hours and minutes */
    if ((ulHour > 23) || (ulMinutes > 59))
    {
        return (usNext);
    }

    rNewDateTime.bHour    = (UINT8) ulHour;
    rNewDateTime.bMinutes = (UINT8) ulMinutes;
    rNewDateTime.bSeconds = 0;

    fnSetCMOSLongDHOnTime(fnTodToSecs(&rNewDateTime));

    usNext = wNextScr1;

    return (usNext);
}



/* Event functions */

/* Variable graphic functions */



/**********************************************************************
        Private Functions
**********************************************************************/
