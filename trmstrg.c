/************************************************************************
*        Horizon CSD
*   MODULE NAME:    trmstrg.c
*
*   DESCRIPTION:    Functions and data for Transaction Record Manager
*   					Storage. 
* ----------------------------------------------------------------------
*               Copyright (c) 2017 Pitney Bowes Inc.
*                    37 Executive Drive
*                   Danbury, Connecticut  06810
* ----------------------------------------------------------------------
* HISTORY: 
*				  
* 2018.08.29 CWB - Per Sathish.  Jira 8602
* - Modified trmUpdateTranRecWithSettingMgr() so that an invalid record will
*   contain <Mode>InvalidPrintMode</Mode> instead of <Mode>ManualWeightEntry</Mode>.
*   This will help Accounting server filter out these bad records. 
*
******************************************************************************/

#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <posix.h>
#include <sys/mman.h>

#include "trmstrg.h"
#include "trm.h"
#include "trmdefs.h"
#include "Utils.h"
#include "settingsmgr.h"
#include "bkgndmgr.h"
#include <string.h>
#include "ctype.h"
#include "pbos.h"
#include "datdict.h"
#include "version.h"
#include "clock.h"
#include "custdat.h"
#include "bobutils.h"
#include "xmltypes.h"
#include "pcdisk.h"
#include "cwrapper.h"
#include "zlib.h"
#include "api_status.h"
#include "flashutil.h"
#include "cm.h"
#include "fdrcm.h"
#include "errcode.h"
#include "sysdatadefines.h"
#include "fwrapper.h"  //fnFlashGetByteParm
#include "oifpmain.h"  //fnCheckFileSysCorruption

//Globals
DCB_PARAMETERS_t *pParameters;
extern DCB_PARAMETERS_t cmosDcapParameters;
char bIgnoreSemaphore = TRUE;

extern uchar bVltDecimalPlaces;
extern BOOL trm_log_level;

extern char CMOSCustomerID_RRN[CUSTOMERID_RRN_SZ];
extern ulong 			  trmMAXMailPiecesToUpload;
//extern uchar   pIPSD_zipCode[ IPSD_SZ_ZIPCODE ];
extern NU_MEMORY_POOL    	*pSystemMemoryCached;
extern TXREC_FILE_INFO 		txFileInfo;

extern BOOL     		fDCAPUploadReqShown;
extern BOOL				fDCAPUploadDueShown;
extern BOOL bGoToReady;

extern trm_ext_deb_cer_t CMOSExtTrmDebitCerts;
extern CM_SYSTEM_STATUS_t sSystemStatus;

INT fsAppend(char * filename, CHAR* filedata, int32_t fileLength);
void GetPSDInfo(PSD_INFO *pPsdInfo);
trm_fee_list_t* GetFeeList(void);

//char trmFileName[TRM_MAX_FILE_NAME_SIZE];
extern DATETIME trmTimeStampOfLatestRec;

//static int trmFileCount;
char *trmFileList[TRM_MAX_NUMBER_OF_FILES];

trm_semaphore_t trm_semaphore[2];

uint32_t gTrmFatalError;
//trm_mem_t *p_trm_mem;
//trm_mem_t *p_trm_mem2;

required_fields_t trm_required_fields;

//trm_mem_t *get_trm_mem(void)
//{
//	return &CMOSTrmArea;
//}

trm_mem_t *get_trm_bucket(int bucket)
{
	if(bucket == 0)
		return &CMOSTrmArea;
	else
		return &CMOSTrmArea2;

}

uint8_t get_mp_idx(mp_id_t mp_id)
{
     return (mp_id & IDX_MASK);
}

ulong get_cur_mp_id()
{
	return CMOSTrmMailPieceID;
}

ulong reset_cur_mp_id()
{
	dbgTrace(DBG_LVL_INFO, "reset_cur_mp_id: Current %d Setting to %d", CMOSTrmMailPieceID, 0);
	return CMOSTrmMailPieceID = 0;
}

void StopMailRunAndSendBaseEvent(base_event_t baseEvent)
{
	unsigned char   pMsgData[2];
	pMsgData[0] = OIT_PRINT_NOT_READY; //OIT_LEFT_PRINT_RDY;
	pMsgData[1] = 0;
	(void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);

	//Send base event
	SendBaseEventToTablet(baseEvent, NULL);

}

ulong get_new_mp_id()
{
	uint32_t preBucket = CMOSTrmActiveBucket;

	//Check mail piece id and it is close to fill the bucket and still there is pending write
	//stop mail run.
	if( (CMOSTrmPendingBucketToUpload != -1) &&
		(CMOSTrmMailPieceID % CFG_NUM_RECORDS) > (CFG_NUM_RECORDS-2) )
	{
		StopMailRunAndSendBaseEvent(BASE_EVENT_PENDING_FLASH_WRITE);
		dbgTrace(DBG_LVL_INFO, "TRM_STRG - get_new_mp_id-Pending Write Needs to Stop Mail Run: Pending :%d, active:%d, mpID:%d ", CMOSTrmPendingBucketToUpload, CMOSTrmActiveBucket, CMOSTrmMailPieceID);
		return 0;
	}

	++CMOSTrmMailPieceID;
	++CMOSTrmMailPiecesToUpload;

	if( (CMOSTrmMailPieceID % (CFG_NUM_RECORDS * 2) ) < CFG_NUM_RECORDS)
		CMOSTrmActiveBucket = CFG_BUCKET0;
	else
		CMOSTrmActiveBucket = CFG_BUCKET1;

	if(preBucket != CMOSTrmActiveBucket)
	{
		dbgTrace(DBG_LVL_INFO, "TRM_STRG - get_new_mp_id-bucket: Pend:%d, prev:%d, active:%d ", CMOSTrmPendingBucketToUpload, preBucket, CMOSTrmActiveBucket);
		CMOSTrmPendingBucketToUpload = preBucket;
		trmSetState(TRM_STATE_PENDING_TX_FILE_SAVE);

		//Set rate ver of new bucket
		trm_mem_t * p_trm_mem = get_trm_bucket(CMOSTrmActiveBucket);
		strncpy( p_trm_mem->header.rate_ver, (char*)CMOSTrmCommon.rate_ver, sizeof(CMOSTrmCommon.rate_ver));
	}

	return CMOSTrmMailPieceID;
}

ulong adjust_mp_id_to_prev()
{
	return --CMOSTrmMailPieceID;
}

ulong trmGetActiveBucket(void)
{
	return CMOSTrmActiveBucket;
}


ulong trm_recover_mp(void)
{
	trm_mem_t *p_trm = get_trm_mem();
	uint8_t idx = get_mp_idx(get_cur_mp_id());

	if(p_trm->trans_rec[idx].debit_cert_size || p_trm->trans_rec[idx].print_finished)
	{
		//Can not be rolled back
		dbgTrace(DBG_LVL_INFO,
						"TRM_STRG - trm_recover_mp: mail piece cannot be rolled back %u", CMOSTrmMailPieceID);
	}
	else
	{
		CMOSTrmMailPieceID--;

	}
	return CMOSTrmMailPieceID;

}


char *rec_state_to_str(rec_state_t rec_state); /* Forward declaration */

#define SEGMENT 16



void hex_dump(uint8_t *data, size_t data_len)
{
    char buf[SEGMENT+1];
    int c,i,m = 0;
    char        	pLogBuf[80];
    while(data_len-- > 0)
    {
        c = *data++;
        if(!(m % SEGMENT))
        {
            memset(&buf,0,SEGMENT+1);
            sprintf( pLogBuf, "    [0x%04X] | ", m);
            fnDumpStringToSystemLog( pLogBuf );
        }

        sprintf( pLogBuf, "%02X ",c);
        fnDumpStringToSystemLog( pLogBuf );

        if(!isprint(c))
            buf[m % SEGMENT] = '.';
        else
            buf[m % SEGMENT] = c;
        m++;
        if(!(m % SEGMENT))
            sprintf( pLogBuf, "| %s\n",buf);
    }

    if(m % SEGMENT != 0)
    {
        for(i = 0; i < (SEGMENT - m % SEGMENT); i++)
        {
            //sprintf( pLogBuf, " ");
            fnDumpStringToSystemLog( " " );
        }
        sprintf(pLogBuf, "| %s\n",buf);
        fnDumpStringToSystemLog( pLogBuf );
    }
}


void dump_addresses(void)
{
    #ifdef DUMP_ADDR
    cout << "shmem @" << p_trm_mem << "records @ " << &p_trm_mem->trans_rec[0] << "\n";

    cout << "Offsets : trans_rec=" << offsetof(trm_mem_t, trans_rec)
         << "rec_mutex="      << offsetof(trans_rec_t, rec_mutex)
         << "rec_state="      << offsetof(trans_rec_t, rec_state)
         << "flags="          << offsetof(trans_rec_t, flags)
         << "mp_id="          << offsetof(trans_rec_t, mp_id)
         << "ar="     << offsetof(trans_rec_t, ar)
         << "state flags="         << offsetof(trans_rec_t, debit_requested)
         << "\n";
    #endif
}

static 	char smName[10] = "TRM_HDR";

void trmCreateSemaphore(void)
{
    int i;

    memset(&trm_semaphore, 0, sizeof(trm_semaphore));

    trm_semaphore[0].p_hdr_rwlock = NULL;
    trm_semaphore[1].p_hdr_rwlock = NULL;
	for ( i = 0; i < CFG_NUM_RECORDS; ++i)
	{
		trm_semaphore[0].p_rec_semaphore[i] = NULL;
	}
	for ( i = 0; i < CFG_NUM_RECORDS; ++i)
	{
		trm_semaphore[1].p_rec_semaphore[i] = NULL;
	}
}


static STATUS trmObtainHeaderAccess(int bucket)
{
	STATUS status = SUCCESS;

	if(trm_semaphore[bucket].p_hdr_rwlock == NULL)
	{
		// RD - note name allowance is only 7 characters followed by a NULL
		sprintf(smName, "TH_DR%02d", bucket);
		status = NU_Create_Semaphore(&trm_semaphore[bucket].hdr_rwlock, smName, 1, NU_PRIORITY_INHERIT);
		if(status != NU_SUCCESS)
			return status;

		trm_semaphore[bucket].p_hdr_rwlock = &trm_semaphore[bucket].hdr_rwlock;
	}

	// RD implement a timeout on the acquisition, register the error if it occurs but move on.
	status = NU_Obtain_Semaphore(trm_semaphore[bucket].p_hdr_rwlock, TRM_SEMAPHORE_TIMEOUT);

	if(status == NU_SUCCESS)
	{
		trm_semaphore[bucket].hdr_lock_count++;
	}
	else
	{
		// semaphore not locked for some reason...
		dbgTrace(DBG_LVL_INFO, "Error trmObtainHeaderAccess unable to acquire semaphore for bucket %d [%d]", bucket, status);
	}

	return status;
}


static STATUS trmReleaseHeaderAccess(int bucket)
{
	STATUS status = SUCCESS;
	status  = NU_Release_Semaphore(trm_semaphore[bucket].p_hdr_rwlock);
	if(status == NU_SUCCESS)
	{
		trm_semaphore[bucket].hdr_lock_count--;
	}
	else
	{
		// RD - log any failure to release the semaphore
		dbgTrace(DBG_LVL_INFO, "Error trmReleaseHeaderAccess unable to release semaphore [%d]", status);
	}
	return status;
}


static STATUS trmObtainRecordAccess(int bucket, int idx)
{
	STATUS status = SUCCESS;
	unsigned long lwSuspensionTicks;

	if(trm_semaphore[bucket].p_rec_semaphore[idx] == NULL)
	{
		sprintf(smName, "TR_RC%02d_%02d", bucket, idx);
		status = NU_Create_Semaphore(&trm_semaphore[bucket].rec_semaphore[idx], smName, 1, NU_PRIORITY_INHERIT);
		if(status != NU_SUCCESS)
		{
			dbgTrace(DBG_LVL_ERROR, "trmObtainRecordAccess Failed to create semaphore Status: %d", status);
			return status;
		}
		trm_semaphore[bucket].p_rec_semaphore[idx] = &trm_semaphore[bucket].rec_semaphore[idx];

	}

	lwSuspensionTicks = 100 / MILLISECONDS_PER_TICK;
	status = NU_Obtain_Semaphore(trm_semaphore[bucket].p_rec_semaphore[idx], lwSuspensionTicks);
	if(status == NU_SUCCESS)
	{
		trm_semaphore[bucket].rec_lock_count[idx]++;
		//dbgTrace(DBG_LVL_ERROR, "trmObtainRecordAccess: rec_lock_count:%d, bucket:%d, idx:%d ", trm_semaphore[bucket].rec_lock_count[idx], bucket, idx);
	}
	else
	{
		dbgTrace(DBG_LVL_ERROR, "trmObtainRecordAccess: Failed to obtain semaphore Status: %d, rec_lock_count:%d, bucket:%d, idx:%d ",
										status, trm_semaphore[bucket].rec_lock_count[idx], bucket, idx);
	}

	return status;
}


static STATUS trmReleaseRecordAccess(int bucket, int idx)
{
	STATUS status = SUCCESS;
	status = NU_Release_Semaphore(trm_semaphore[bucket].p_rec_semaphore[idx]);
	if(status == NU_SUCCESS)
	{
		trm_semaphore[bucket].rec_lock_count[idx]--;
		//dbgTrace(DBG_LVL_ERROR, "trmReleaseRecordAccess: rec_lock_count:%d, bucket:%d, idx:%d ", trm_semaphore[bucket].rec_lock_count[idx], bucket, idx);
	}
	else
	{
		dbgTrace(DBG_LVL_ERROR, "trmReleaseRecordAccess: NU_Release_Semaphore failed Status: %d, rec_lock_count:%d, bucket:%d, idx:%d ",
								status, trm_semaphore[bucket].rec_lock_count[idx], bucket, idx);
	}


	return status;
}


trm_mem_t * InitTrmBucket(int bucket)
{
	//TODO: Remove hardcoded rate_ver once tablet is populating
    STATUS status = SUCCESS;
	int i;
	uint8 ucWeightUnits = fnFlashGetByteParm(BP_WEIGHING_UNITS);

	trm_mem_t *p_trm_mem =  get_trm_bucket(bucket);

	if (NU_SUCCESS == trmObtainHeaderAccess(bucket) )
	{
		/* and now create */
		p_trm_mem->header.b_print_required = true;
		p_trm_mem->header.b_debit_required = true;

		p_trm_mem->header.num_records_in_use = 0;
		p_trm_mem->header.tr_version = TR_VERSION;
		//p_trm_mem->header.job_id[0] = '\0';
		p_trm_mem->header.next_transaction_id = 0;
		p_trm_mem->header.verbosity_lvl = 0;
		//p_trm_mem->header.pbi_serial_number[0] = '\0';
		//p_trm_mem->header.wgt_source_idx = 0; \\TODO Platform;
		p_trm_mem->header.num_dec_places_in_PSD = bVltDecimalPlaces;
		//p_trm_mem->header.date_of_submission[0] = '\0';
		p_trm_mem->header.flags.all_fields = 0;
		p_trm_mem->header.b_recovering_to_tape = false;
		p_trm_mem->header.b_AutoTapeRecovery = false;



	//		if(p_trm_mem->header.decimalPosition== 0)
	//			p_trm_mem->header.decimalPosition = 2;

	    p_trm_mem->header.weight_unit = ucWeightUnits;
		/* now initialise all TR records */


		for ( i = 0; i < CFG_NUM_RECORDS; ++i)
		{
			status = trmObtainRecordAccess(bucket, i);
			if(status == SUCCESS)
			{
				p_trm_mem->trans_rec[i].flags.all_fields = 0;
				p_trm_mem->trans_rec[i].rec_state = TRM_UNUSED;
			}
			trmReleaseRecordAccess(bucket, i);
		}

		trmReleaseHeaderAccess(bucket);
	}

	return p_trm_mem;
}

//static pthread_mutexattr_t rwlock_attr;
//static pthread_mutexattr_t mutex_attr;
trm_mem_t *InitTrmMem(void)
{
    trm_mem_t * p_trm_mem= NULL;
    dbgTrace(DBG_LVL_INFO, "TRM - InitTrmMem: Initiliazing TRM memory ");
    memset(&CMOSTrmInitialzed1, 0, ((void*)&CMOSTrmInitialzed2 - (void*)&CMOSTrmInitialzed1) + sizeof(CMOSTrmInitialzed2));

    CMOSTrmInitialzed1 = TRM_INIT_PATTERN;
    CMOSTrmInitialzed2 = TRM_INIT_PATTERN;
    CMOSTrmVersion = TR_VERSION;
    CMOSTrmFlashFileSeqCount = 0;
    CMOSTrmDcapFileSeqCount = 1;
    CMOSTrmActiveBucket = 0;
    CMOSTrmMaxFilesToUpload = MAX_FILES_TO_UPLOAD_EX;
    CMOSTrmPendingBucketToUpload = -1;
    CMOSTrmFatalError = 0;

    CMOSTrmMailPiecesToUpload = 0;
    memset(&CMOSTrmTimeStampOfOldestMP, 0, sizeof(CMOSTrmTimeStampOfOldestMP));
    memset(&CMOSTrmTimeStampOfMPIDReset, 0, sizeof(CMOSTrmTimeStampOfMPIDReset));

    sprintf((char*)CMOSDcapGuid, "{00000000-0000-0000-0000-000000000000}");

    trmInitPSDInfo();

    //Set default device id as 29.
    //TODO: this can be changed to 26 on need.
    trmSetDcapDeviceID(29);

    clear_active_mp_log(-1);

    p_trm_mem = InitTrmBucket(0);
    p_trm_mem = InitTrmBucket(1);

    return p_trm_mem;
}


void trmClearSemaphore(void)
{
	int i;
	for ( i = 0; i < CFG_NUM_RECORDS; ++i)
	{
		trmReleaseRecordAccess(0, i);
	}
	trmReleaseHeaderAccess(0);

	for ( i = 0; i < CFG_NUM_RECORDS; ++i)
	{
		trmReleaseRecordAccess(1, i);
	}

	trmReleaseHeaderAccess(1);
}


//void clr_wgt_source(void)
//{
//    if (NU_SUCCESS == trmObtainHeaderAccess())
//    {
//        p_trm_mem->header.flags.is_populated.wgt_src = false;
//
//        dbgTrace(DBG_LVL_INFO,
//                "TRM_STRG - clr_wgt_source.");
//
//        trmReleaseHeaderAccess(bucket);   /* TODO: check result */
//    }
//    else
//    {
//        dbgTrace(DBG_LVL_INFO,
//                "TRM_STRG - clr_wgt_source can not apply lock" );
//    }
//}


#define TR_REC_LOCK_START                                                       \
    uint8_t idx;                                                                \
    error_t result = SUCCESS;                                                   \
    trm_mem_t *p_trm_mem =  get_trm_bucket(trmGetActiveBucket());					\
    idx = get_mp_idx(mp_id);                                                    \
    if (NU_SUCCESS == trmObtainRecordAccess(trmGetActiveBucket(), idx)){


#define TR_REC_LOCK_END                                                         \
		result = trmReleaseRecordAccess(trmGetActiveBucket(), idx);       		\
    }else{ result = E_MUTEX_LOCK_FAILED;                                         \
    	dbgTrace(DBG_LVL_INFO, "TRM_STRG - could not apply lock in %s line %d", __FUNCTION__ , __LINE__ );} \
    return result;


#define TR_VERIFY_POPULATING_AND_MPID_START                                     \
        if ( p_trm_mem->trans_rec[idx].rec_state == TRM_POPULATING         		\
          && p_trm_mem->trans_rec[idx].mp_id == mp_id){

#define TR_VERIFY_POPULATING_OR_PENDING_UPLOAD_AND_MPID_START                   \
        if ((  p_trm_mem->trans_rec[idx].rec_state == TRM_POPULATING       		\
            || p_trm_mem->trans_rec[idx].rec_state == TRM_PENDING_UPLOAD ) 		\
          && p_trm_mem->trans_rec[idx].mp_id == mp_id){							\

#define TR_VERIFY_POPULATING_AND_MPID_END                                       \
        }else{result = E_MP_ID_NOT_FOUND;}


#define TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START \
    TR_REC_LOCK_START                                \
    TR_VERIFY_POPULATING_AND_MPID_START

#define TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END   \
    TR_VERIFY_POPULATING_AND_MPID_END                \
    TR_REC_LOCK_END


#define TR_REC_LOCK_VERIFY_POPULATING_OR_PENDING_UPLOAD_AND_MPID_START \
    TR_REC_LOCK_START                                \
    TR_VERIFY_POPULATING_OR_PENDING_UPLOAD_AND_MPID_START



error_t trmCreateTR(const mp_id_t mp_id, bool bWaitForUpload)
{
    uint8_t idx;
    error_t result = SUCCESS;
    trm_fee_list_t* pFeeList;
    long curBucket = trmGetActiveBucket();
    trm_mem_t *p_trm_mem =  get_trm_bucket(curBucket);
    DATETIME dummy;

    idx = get_mp_idx(mp_id);

    memset(&dummy, 0 , sizeof(dummy));

    if(NU_SUCCESS == trmObtainHeaderAccess(curBucket) )
    {
        trans_rec_t *p_tr = &p_trm_mem->trans_rec[idx];

        dbgTrace(DBG_LVL_INFO, "TRM_STRG - trmCreateTR(%u) Locking bucket: %d, record: %d ", mp_id, curBucket, idx );
        if (( result == SUCCESS)  && (NU_SUCCESS == trmObtainRecordAccess(curBucket, idx)))
        {
        	if (p_tr->rec_state == TRM_UNUSED)
            {
                memset(&p_tr->mode, 0, sizeof(trans_rec_t) -  offsetof(trans_rec_t, mode));
            	memset(&p_tr->flags.is_populated, 0, sizeof(p_tr->flags.is_populated));

            	p_tr->mp_id     = mp_id;
            	p_tr->flags.is_populated.mp_id = true;

            	p_tr->rec_state = TRM_POPULATING;

                result = trmUpdateTranRecWithSettingMgr(mp_id);
                if(result != SUCCESS)
                {
                	dbgTrace(DBG_LVL_INFO, "TRM_STRG - trmCreateTR(%u) failed to set data from settings manager\n", mp_id );
                }

                CMOSTrmUploadDueReqd |= TRM_UPLOAD_AVAILABLE;

                //
                if(memcmp(&CMOSTrmTimeStampOfOldestMP, &dummy, sizeof(dummy)) == 0)
                	GetSysDateTime(&CMOSTrmTimeStampOfOldestMP, ADDOFFSETS, GREGORIAN);

            	//Set date time
				GetSysDateTime(&p_tr->start_time, ADDOFFSETS, GREGORIAN);
				p_tr->flags.is_populated.start_time = true;


				//HORCSD-4225-Need <DebTime in NonDebit modes
				memcpy(&p_tr->debitTime, &p_tr->start_time, sizeof(p_tr->debitTime));
				p_tr->flags.is_populated.debitTime = true;

				if(p_trm_mem->trans_rec[idx].debit_required == true)
	            {


					//Set Fee
					pFeeList = GetFeeList();
					if(pFeeList->cnt > 0)
					{
						memcpy(&p_tr->fee_list, pFeeList, sizeof(trm_fee_list_t));
						p_tr->flags.is_populated.fee_list = true;
					}

	            }

                p_tr->transaction_id   = p_trm_mem->header.next_transaction_id++;
                p_tr->ph_maintenance_done = p_trm_mem->header.b_PH_maintenance_done;


                p_tr->tape_used = p_trm_mem->header.b_media;  /* TODO: reflect what job media type is */
                p_tr->recovered = false;


                p_trm_mem->header.num_records_in_use++;
                p_trm_mem->header.b_PH_maintenance_done = false;

                if(trm_log_level)
                	dbgTrace(DBG_LVL_INFO,
                        "TRM_STRG - CreateTR(%u) bucket:%d, TransActionID=%u%s"
                        , mp_id, curBucket
                        , p_tr->transaction_id
                        , p_tr->ph_maintenance_done ? " PH-maintenance-done" : "" );

                //pause_if_required();
            }
            else
            {
            	if (p_tr->mp_id == mp_id)
                {
                    result = E_MP_ID_ALREADY_EXISTS;
                    dbgTrace(DBG_LVL_INFO,
                            "TRM_STRG - CreateTR(%u) mp_id already existed. Going to return E_MP_ID_ALREADY_EXISTS!"
                            , mp_id );
                    fnLogError( ERR_TYPE_FATAL, ERROR_CLASS_TRM, E_MP_ID_ALREADY_EXISTS );
                }
                else
                {
                	result = E_DATA_UPLOAD_REQUIRED;

                	dbgTrace(DBG_LVL_INFO,
                            "TRM_STRG - CreateTR(%u) record idx %d in unexpected %s state rec[%d].mp_id=%d. Going to return E_TRANS_REC_STILL_IN_USE!"
                            , mp_id
                            , idx
                            , rec_state_to_str(p_tr->rec_state)
                            , idx
                            , p_tr->mp_id );


                  	//SendBaseEventToTablet(BASE_EVENT_TX_UPLOAD_DUE, NULL);
                	fDCAPUploadReqShown = TRUE;
                	SendBaseEventToTablet(BASE_EVENT_TX_UPLOAD_REQUIRED, NULL);
                  	fnLogError( ERR_TYPE_FATAL, ERROR_CLASS_TRM, E_DATA_UPLOAD_REQUIRED );
                }
            }

            trmReleaseRecordAccess(curBucket, idx);
        }
        else
        {
        	fnPostDisabledStatus(DCOND_TRM_FATAL_ERROR);
        	SendBaseEventToTablet(BASE_EVENT_TRM_FATAL_ERROR, NULL);

            result = E_MUTEX_LOCK_FAILED;
            dbgTrace(DBG_LVL_INFO, "TRM_STRG - could not apply record lock in %s line %d (rv=%d)", __PRETTY_FUNCTION__ , __LINE__ , result);
            fnLogError( ERR_TYPE_FATAL, ERROR_CLASS_TRM, E_MUTEX_LOCK_FAILED );
        }

        trmReleaseHeaderAccess(curBucket);
    }
    else
    {
    	fnPostDisabledStatus(DCOND_TRM_FATAL_ERROR);
    	SendBaseEventToTablet(BASE_EVENT_TRM_FATAL_ERROR, NULL);

        result = E_MUTEX_LOCK_FAILED;
        dbgTrace(DBG_LVL_INFO, "TRM_STRG - could not apply lock in %s line %d (rv=%d)", __PRETTY_FUNCTION__ , __LINE__ , result);
        fnLogError( ERR_TYPE_FATAL, ERROR_CLASS_TRM, E_MUTEX_LOCK_FAILED );
    }
    return result;
}

error_t CreateXR(void)
{
	error_t result = SUCCESS;
	//int idx;

	//trm_mem_t *p_tr = get_trm_bucket(trmGetActiveBucket());

    active_mp_log_t *p_active_mp = get_active_mp();

   	clear_active_mp_log(0);

   	p_active_mp->mp_id = get_new_mp_id();

   	if(p_active_mp->mp_id != 0)
   	{
		result =  trmCreateTR(p_active_mp->mp_id, FALSE);
		dbgTrace(DBG_LVL_INFO, "CreateXR - CMOSTrmMailPiecesToUpload:%d/%d ",  CMOSTrmMailPiecesToUpload, MAX_MP_TO_UPLOAD);

		if(trmMAXMailPiecesToUpload > MAX_MP_TO_UPLOAD)
			trmMAXMailPiecesToUpload = MAX_MP_TO_UPLOAD;

		if( CMOSTrmMailPiecesToUpload  >= trmMAXMailPiecesToUpload)
		{
			fDCAPUploadReqShown = TRUE;
			CMOSTrmUploadDueReqd |= TRM_UPLOAD_REQUIRED;
		}
   	}
   	else
   	{
   		//somehow cannot create new mail piece
   		//due to pending writes etc
   		result = -1;
   	}

   	return result;
}

error_t trm_set_mode(const mp_id_t mp_id, char *mode)
{
	TR_REC_LOCK_VERIFY_POPULATING_OR_PENDING_UPLOAD_AND_MPID_START

	strncpy(p_trm_mem->trans_rec[idx].mode,  mode, sizeof(p_trm_mem->trans_rec[idx].mode));
    dbgTrace(DBG_LVL_INFO, "TRM_STRG - trm_set_mode to [%d]", mode);

    p_trm_mem->trans_rec[idx].flags.is_populated.mode = true;

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}

error_t trmGetTransactionID(const mp_id_t mp_id, uint32_t *transaction_id)
{
    TR_REC_LOCK_VERIFY_POPULATING_OR_PENDING_UPLOAD_AND_MPID_START

    *transaction_id = p_trm_mem->trans_rec[idx].transaction_id;
    if (p_trm_mem->header.verbosity_lvl > 0)
        dbgTrace(DBG_LVL_INFO,
            "TRM_STRG - GetTransactionID: mp_id=%d transaction_id=%u)"
            , mp_id, transaction_id);

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}


error_t trm_set_start_time(const mp_id_t mp_id, DATETIME *dateTime)
{
	TR_REC_LOCK_VERIFY_POPULATING_OR_PENDING_UPLOAD_AND_MPID_START

	memcpy(&p_trm_mem->trans_rec[idx].start_time, dateTime, sizeof(DATETIME));
//    dbgTrace(DBG_LVL_INFO, "TRM_STRG - trm_set_start_time to: %2d%02d-%02d-%02dT%02d:%02d:%02d",
//    										dateTime->bCentury,
//    										dateTime->bYear,
//    										dateTime->bMonth,
//    										dateTime->bDay,
//    										dateTime->bHour,
//    										dateTime->bMinutes,
//    										dateTime->bSeconds);

    p_trm_mem->trans_rec[idx].flags.is_populated.start_time = true;

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}

error_t trm_set_finish_time(const mp_id_t mp_id, DATETIME *dateTime)
{
	TR_REC_LOCK_VERIFY_POPULATING_OR_PENDING_UPLOAD_AND_MPID_START

	memcpy(&p_trm_mem->trans_rec[idx].finish_time, dateTime, sizeof(DATETIME));
//    dbgTrace(DBG_LVL_INFO, "TRM_STRG - trm_set_finish_time to : %2d%02d-%02d-%02dT%02d:%02d:%02d",
//    										dateTime->bCentury,
//    										dateTime->bYear,
//    										dateTime->bMonth,
//    										dateTime->bDay,
//    										dateTime->bHour,
//    										dateTime->bMinutes,
//    										dateTime->bSeconds);

    p_trm_mem->trans_rec[idx].flags.is_populated.finish_time = true;

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}



error_t trm_set_upload_required(const mp_id_t mp_id)
{
	int bucket = trmGetActiveBucket();
	trm_mem_t *p_trm_mem = get_trm_bucket(trmGetActiveBucket());

	if(NU_SUCCESS == trmObtainHeaderAccess(bucket))
	{
		p_trm_mem->header.b_upload_required = true;

		dbgTrace(DBG_LVL_INFO,
					"TRM_STRG - set_upload_required.");

		trmReleaseHeaderAccess(bucket);
	}
	else
	{
		dbgTrace(DBG_LVL_INFO,
				"TRM_STRG - set_upload_required can not apply hdr lock" );
	}

    return SUCCESS;
}



//error_t trmMPExit(const mp_id_t mp_id, bool tape_exit)
//{
//    bool b_inhibit_upload = false;
//
//    TR_REC_LOCK_START
//    TR_VERIFY_POPULATING_AND_MPID_START
//
//	//clock_gettime( CLOCK_REALTIME, &p_trm_mem->trans_rec[idx].finish_time);
//    //p_trm_mem->trans_rec[idx].flags.is_populated.finish_time = true;
//    p_trm_mem->trans_rec[idx].tape_used = tape_exit;
//
///*
//    if (p_trm_mem->header.b_print_required
//     && p_trm_mem->trans_rec[idx].flags.is_populated.debit_cert
//     && p_trm_mem->trans_rec[idx].flags.is_populated.image_done)
//    {
//        b_inhibit_upload = !(    p_trm_mem->trans_rec[idx].flags.is_populated.print_finished_ph1
//                              || p_trm_mem->trans_rec[idx].flags.is_populated.print_finished_ph2 );
//    }
//*/
//    if (p_trm_mem->header.b_debit_required && !p_trm_mem->trans_rec[idx].flags.is_populated.debit_cert)
//    {
//    	b_inhibit_upload=true;
//    }
//    if (p_trm_mem->header.b_print_required)
//    {
//    	if(!p_trm_mem->trans_rec[idx].flags.is_populated.image_done)
//    	{
//    		b_inhibit_upload=true;
//    	}
//
//        if (p_trm_mem->header.b_debit_required && !p_trm_mem->trans_rec[idx].print_finished)
//        {
//            /* since a debit is required, we should have had a finished postal (ph1) print */
//            b_inhibit_upload=true;
//        }
//
//    	if(!(p_trm_mem->trans_rec[idx].print_finished))
//    	{
//    		b_inhibit_upload=true;
//    	}
//    }
//
//    if (!b_inhibit_upload)
//    {
//    	p_trm_mem->trans_rec[idx].rec_state = TRM_PENDING_UPLOAD;
//    }
//    dbgTrace(DBG_LVL_INFO,
//       "TRM_STRG - %s mp_id=%d %s"
//       , tape_exit? "TapeExit" : "PieceExit", mp_id, b_inhibit_upload ? "TRM upload inhibited" : "");
//
//    TR_VERIFY_POPULATING_AND_MPID_END
//
//    if (result == SUCCESS && !b_inhibit_upload)
//    {
//        result = trm_set_upload_required(mp_id);
//    }
//
//    TR_REC_LOCK_END
//}
//
//error_t trmPieceExit(const mp_id_t mp_id)
//{
//    return trmMPExit(mp_id, false);
//}
//
//error_t trmTapeExit(const mp_id_t mp_id)
//{
//    return trmMPExit(mp_id, true);
//}


error_t trmInitiateUpload(const mp_id_t mp_id)
{
    TR_REC_LOCK_START
    TR_VERIFY_POPULATING_AND_MPID_START

    p_trm_mem->trans_rec[idx].rec_state = TRM_PENDING_UPLOAD;

    TR_VERIFY_POPULATING_AND_MPID_END

    if (result == SUCCESS)
    {
        //result = pulse_server_to_upload_record(mp_id);
    	result = trm_set_upload_required(mp_id);
    }

    TR_REC_LOCK_END
}

bool trmNeedToUploadRecords(void)
{
    uint8_t idx;
    int bucket = trmGetActiveBucket();
    trm_mem_t *p_trm_mem = get_trm_bucket(bucket);

    /* first lets find all records that are in the populating state */
    for (idx=0; idx < CFG_NUM_RECORDS; idx++)
    {
        //if (POSIX_SUCCESS == pthread_mutex_lock( &p_trm_mem->trans_rec[idx].rec_mutex ))
    	if (NU_SUCCESS == trmObtainRecordAccess(bucket, idx))
        {
            if ((p_trm_mem->trans_rec[idx].rec_state == TRM_POPULATING)
            		|| (p_trm_mem->trans_rec[idx].rec_state == TRM_PENDING_UPLOAD)
            		|| (p_trm_mem->trans_rec[idx].rec_state == TRM_PENDING_UPLOAD_CONFIRMATION))
            {
                return true;
            }
            //pthread_mutex_unlock( &p_trm_mem->trans_rec[idx].rec_mutex);
            trmReleaseRecordAccess(bucket, idx);
        }
    }

    return false;
}
//
//error_t trmInitiateUploadForAllRecordsInProgress(void)
//{
//    uint8_t idx;
//    uint8_t rec_count = 0;
//    uint32_t 	mp_id_list[CFG_NUM_RECORDS];
//    uint32_t	it;
//    error_t result = SUCCESS;
//
//    //mp_id_t	cur_mp_id = get_cur_mp_id();
//
//    /* first lets find all records that are in the populating state */
//    for (idx=get_cur_mp_id(); idx < CFG_NUM_RECORDS; idx--)
//    {
//        if (NU_SUCCESS == trmObtainRecordAccess(idx))
//        {
//            if (p_trm_mem->trans_rec[get_mp_idx(idx)].rec_state == TRM_POPULATING)
//            {
//               // mp_id_list.push_back(p_trm_mem->trans_rec[idx].mp_id);
//            	mp_id_list[rec_count] = p_trm_mem->trans_rec[get_mp_idx(idx)].mp_id;
//                rec_count++;
//            }
//            trmReleaseRecordAccess(idx);
//        }
//    }
//
//    if (rec_count > 0)
//    {
//        /* now sort them so that we get the lowest (oldest) mp_id first */
//        //std::sort(mp_id_list.begin(), mp_id_list.end());
//
//        /* and now initiate the upload on all of them */
//        for (it = 0; it < rec_count; it++)
//        {
//            dbgTrace(DBG_LVL_INFO, "TRM_STRG - Initiate upload for rec in progress with mp id %d", mp_id_list[it]);
//
//            trmInitiateUpload(mp_id_list[it]);
//        }
//
//        dbgTrace(DBG_LVL_INFO,
//           "TRM_STRG - trmInitiateUploadForAllRecordsInProgress() %u record%s needed uploading", rec_count, rec_count == 1 ? "" : "s");
//    }
//    else
//    {
//        dbgTrace(DBG_LVL_INFO, "TRM_STRG - trmInitiateUploadForAllRecordsInProgress() No records needed uploading");
//    }
//
//    return result;
//}

error_t trmPrintHeadMaintenancePerformed(void)
{
    error_t result = SUCCESS;
    int bucket = trmGetActiveBucket();
    trm_mem_t *p_trm_mem = get_trm_bucket(bucket);

    if (NU_SUCCESS ==trmObtainHeaderAccess(bucket))
    {
        p_trm_mem->header.b_PH_maintenance_done = true;
        dbgTrace(DBG_LVL_INFO,
                "TRM_STRG - PrintHeadMaintenancePerformed ");

        trmReleaseHeaderAccess(bucket);   /* TODO: check result */
    }
    else
    {
        result = E_MUTEX_LOCK_FAILED;
        dbgTrace(DBG_LVL_INFO,
                "TRM_STRG - PrintHeadMaintenancePerformed can not apply lock" );
    }

    return result;
}

//TODO
//error_t trm_set_recovery_to_tape_in_progress(void)
//{
//    error_t result = SUCCESS;
//
//    if (POSIX_SUCCESS == pthread_mutex_lock(&trm_semaphore.hdr_rwlock))
//    {
//        p_trm_mem->header.b_recovering_to_tape = true;
//        dbgTrace(DBG_LVL_INFO,
//                "TRM_STRG - set_recovery_to_tape_in_progress()  ");
//
//        pthread_mutex_unlock(&trm_semaphore.hdr_rwlock);   /* TODO: check result */
//    }
//    else
//    {
//        result = E_MUTEX_LOCK_FAILED;
//        dbgTrace(DBG_LVL_INFO,
//                "TRM_STRG - set_recovery_to_tape_in_progress() can not apply lock" );
//    }
//
//    return result;
//}

//error_t trm_clr_recovery_to_tape_in_progress(void)
//{
//    error_t result = SUCCESS;
//
//    if (POSIX_SUCCESS == pthread_mutex_lock(&trm_semaphore.hdr_rwlock))
//    {
//        p_trm_mem->header.b_recovering_to_tape = false;
//        dbgTrace(DBG_LVL_INFO,
//                "TRM_STRG - clr_recovery_to_tape_in_progress()  ");
//
//        pthread_mutex_unlock(&trm_semaphore.hdr_rwlock);   /* TODO: check result */
//    }
//    else
//    {
//        result = E_MUTEX_LOCK_FAILED;
//        dbgTrace(DBG_LVL_INFO,
//                "TRM_STRG - clr_recovery_to_tape_in_progress() can not apply lock" );
//    }
//
//    return result;
//}

bool    is_recovery_to_tape_in_progress(void)
{
    trm_mem_t *p_trm_mem = get_trm_bucket(trmGetActiveBucket());

    return p_trm_mem->header.b_recovering_to_tape;
}

bool get_next_recoverable_mp_id(mp_id_t *mp_id)
{
    //uint8_t idx;
    uint8_t rec_count = 0;
//    vector<mp_id_t> mp_id_list;
//    vector<mp_id_t>::iterator it;
//
////    error_t result = SUCCESS;
//
//    if(p_trm_mem->header.b_AutoTapeRecovery == false)
//    {
//        dbgTrace(DBG_LVL_INFO,
//                               "TRM_STRG - get_next_recoverable_mp_id(): JobSettings says no AutoRecovery to tape");
//        return false;
//    }
//
//    dbgTrace(DBG_LVL_INFO,
//                           "TRM_STRG - get_next_recoverable_mp_id(): start");
//
//    /* first lets find all records that are in the populating state, have debited but not yet started printing */
//    for (idx=0; idx < CFG_NUM_RECORDS; idx++)
//    {
//        if (POSIX_SUCCESS == pthread_mutex_lock( &p_trm_mem->trans_rec[idx].rec_mutex ))
//        {
//            if (   p_trm_mem->trans_rec[idx].rec_state == TRM_POPULATING
//                && p_trm_mem->trans_rec[idx].flags.is_populated.debit_cert)
//            {
//                bool ph1_started =    p_trm_mem->trans_rec[idx].flags.is_populated.print_started_ph1
//                                   && p_trm_mem->trans_rec[idx].print_started_ph1;
//                bool ph2_started =    p_trm_mem->trans_rec[idx].flags.is_populated.print_started_ph2
//                                   && p_trm_mem->trans_rec[idx].print_started_ph2;
//                bool ph1_finished=    p_trm_mem->trans_rec[idx].flags.is_populated.print_finished_ph1
//                                   && p_trm_mem->trans_rec[idx].print_finished_ph1;
//                bool ph2_finished=    p_trm_mem->trans_rec[idx].flags.is_populated.print_finished_ph2
//                                   && p_trm_mem->trans_rec[idx].print_finished_ph2;
//
//                bool page_complete= ((p_trm_mem->trans_rec[idx].page_complete_result == 0) &&
//                					p_trm_mem->trans_rec[idx].flags.is_populated.page_complete_result);
//
//                //If debited, check if there is MAC info that needs to be added to this record
//            	if (!p_trm_mem->trans_rec[idx].flags.is_populated.MACData_posted) //TODO: and if the print start but not completed
//                {
//            		mac_data_t mac_data;
//            		int result = GetMACInfoFromDS(p_trm_mem->trans_rec[idx].transaction_id, mac_data);
//					if(result == SUCCESS)
//					{
//						if(strlen(mac_data.macdata) !=0 )
//						{
//						    memcpy(p_trm_mem->trans_rec[idx].MACData,mac_data.macdata, CFG_MAX_MAC_LEN);
//						    p_trm_mem->trans_rec[idx].flags.is_populated.MACData_posted = true;
//
//						    dbgTrace(DBG_LVL_INFO,
//						       "TRM_STRG - MACData mp_id=%d. MACData=%s"
//						       , p_trm_mem->trans_rec[idx].mp_id, p_trm_mem->trans_rec[idx].MACData );
//						}
//					}
//                }
//            	if (!p_trm_mem->trans_rec[idx].flags.is_populated.image_done )
//                {
//                    dbgTrace(DBG_LVL_INFO,
//                       "TRM_STRG - get_next_recoverable_mp_id(): mp_id %d can't be recovered debit_cert=true image_done=false"
//                       , p_trm_mem->trans_rec[idx].mp_id);
//                }
//                else
//                {
//                    // todo: check which printhead we need to verify for recovery
//                    if ((!page_complete ) && (!ph1_started)) //(   !ph1_started && !ph2_started)
//                    {
//                        mp_id_list.push_back(p_trm_mem->trans_rec[idx].mp_id);
//                        rec_count++;
//                    }
//                    else
//                    {
//                        if (ph1_started && (!ph1_finished || !page_complete))
//                        {
//                            dbgTrace(DBG_LVL_INFO,
//                               "TRM_STRG - get_next_recoverable_mp_id(): mp_id %d needs to be aborted", p_trm_mem->trans_rec[idx].mp_id);
//
//                            //Do this only if it hasn't been populated already
//                            if(p_trm_mem->trans_rec[idx].flags.is_populated.column_printed_posted == false)
//                            {
//								//Get Power Failure Info from MCP services
//								sPowerFailureInfo pfInfo;
//
//								pfInfo.transactionId = 0;
//								pfInfo.printLengthColumns = 0;
//								int result = GetPFInfoFromMCP(pfInfo);
//								if(result == SUCCESS)
//								{
//									dbgTrace(DBG_LVL_INFO,
//									   "TRM_STRG - PFTransID =%d,  TRMTransID=%d"
//									   ,pfInfo.transactionId, p_trm_mem->trans_rec[idx].transaction_id );
//									if(pfInfo.transactionId == p_trm_mem->trans_rec[idx].transaction_id)
//									{
//										if(pfInfo.printLengthColumns != 0)
//										{
//											p_trm_mem->trans_rec[idx].column_printed = pfInfo.printLengthColumns;
//											p_trm_mem->trans_rec[idx].flags.is_populated.column_printed_posted = true;
//
//											dbgTrace(DBG_LVL_INFO,
//											   "TRM_STRG - SetColumnPrinted mp_id=%d. column_printed=%d"
//											   ,p_trm_mem->trans_rec[idx].mp_id, p_trm_mem->trans_rec[idx].column_printed );
//										}
//									}
//								}
//                            }
//                        }
//
//						if (ph2_started && !ph2_finished)
//                        {
//                            dbgTrace(DBG_LVL_INFO,
//                               "TRM_STRG - get_next_recoverable_mp_id(): mp_id %d needs to be aborted", p_trm_mem->trans_rec[idx].mp_id);
//                        }
//                    }
//                }
//            }
//            pthread_mutex_unlock( &p_trm_mem->trans_rec[idx].rec_mutex);
//        }
//    }
//
//    if (rec_count > 0)
//    {
//        /* now sort them so that we get the lowest (oldest) mp_id first */
//        std::sort(mp_id_list.begin(), mp_id_list.end());
//
//        /* and now report which mp_id's can be recovered to tape */
//        it = mp_id_list.begin();
//        mp_id = *it;
//        for (;it != mp_id_list.end(); it++)
//        {
//            dbgTrace(DBG_LVL_INFO,
//            "TRM_STRG - mp_id %d needs to be recovered to tape", *it);
//
//            /* todo: mark record up for recovery to tape */
//        }
//    }
//    dbgTrace(DBG_LVL_INFO,
//       "TRM_STRG - get_next_recoverable_mp_id(): end next_mp_id=%u (%u record%s need recovery to tape)", mp_id, rec_count, rec_count == 1 ? "" : "s");

    return rec_count > 0;
}

error_t trmIdentifyRecoverableToTapeRecords(void)
{
    mp_id_t mp_id;
    get_next_recoverable_mp_id(&mp_id);
    return SUCCESS;
}

//error_t trm_record_upload_complete(mp_id_t mp_id)
//{
//    uint8_t idx;
//    error_t result = SUCCESS;
//    idx = get_mp_idx(mp_id);
//    int mutex_result;
//
//    if (POSIX_SUCCESS == (mutex_result = pthread_mutex_lock( &trm_semaphore.hdr_rwlock )))
//    {
//        if (POSIX_SUCCESS == (mutex_result = pthread_mutex_lock( &p_trm_mem->trans_rec[idx].rec_mutex )))
//        {
//            p_trm_mem->trans_rec[idx].rec_state = TRM_UNUSED;
//            pthread_mutex_unlock( &p_trm_mem->trans_rec[idx].rec_mutex);
//        if (p_trm_mem->header.num_records_in_use > 0)
//        {
//            p_trm_mem->header.num_records_in_use--;
//        }
//        resume_if_required();
//        }
//        else
//        {
//            result = E_MUTEX_LOCK_FAILED;
//            dbgTrace(DBG_LVL_INFO, "TRM_STRG - could not apply record lock in %s line %d (rv=%d)", __PRETTY_FUNCTION__ , __LINE__ , mutex_result );
//        }
//        pthread_mutex_unlock(&trm_semaphore.hdr_rwlock);
//    }
//    else
//    {
//        result = E_MUTEX_LOCK_FAILED;
//        dbgTrace(DBG_LVL_INFO, "TRM_STRG - could not apply lock in %s line %d (rv=%d)", __PRETTY_FUNCTION__ , __LINE__ , mutex_result);
//    }
//    return result;
//}

error_t trmGetBackgroundImageList(const mp_id_t mp_id, char *p_image_list, size_t max_image_list_size)
{
    return SUCCESS;
}

//error_t trmTR_Rated(const mp_id_t mp_id)
//{
//    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START
//
//    p_trm_mem->trans_rec[idx].rated = true;
//    p_trm_mem->trans_rec[idx].flags.is_populated.rated = true;
//
//    dbgTrace(DBG_LVL_INFO,
//        "TRM_STRG - TR_Rated mp_id=%d"
//        , mp_id);
//
//    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
//}



//error_t trmPutDebitRequestMsg(const mp_id_t mp_id, char const * const PSD_request, size_t PSD_request_size)
//{
//    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START
//
//    if (PSD_request_size > sizeof(p_trm_mem->trans_rec[idx].debit_request))
//    {
//        result = E_TR_RECORD_FULL;
//        dbgTrace(DBG_LVL_INFO,
//            "TRM_STRG - PutDebitRequestMsg Error mp_id=%d PSD_request_size > CFG_MAX_PSD_DEBIT_REQ_SIZE   (%d>%d)"
//            , mp_id, PSD_request_size, sizeof(p_trm_mem->trans_rec[idx].debit_request));
//    }
//    else
//    {
//        memcpy(p_trm_mem->trans_rec[idx].debit_request, PSD_request, PSD_request_size);
//        p_trm_mem->trans_rec[idx].debit_request_size = PSD_request_size;
//        p_trm_mem->trans_rec[idx].flags.is_populated.debit_request = true;
//        dbgTrace(DBG_LVL_INFO,
//            "TRM_STRG - PutDebitRequestMsg mp_id=%u size=%u"
//            , mp_id, PSD_request_size);
//    }
//
//    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
//
//	return 0;
//}

//error_t trmGetDebitRequestMsg(const mp_id_t mp_id, char  * const debit_request_msg, size_t *debit_request_msg_size)
//{
//    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START
//
//    if (p_trm_mem->trans_rec[idx].flags.is_populated.debit_request)
//    {
//        if (*debit_request_msg_size < p_trm_mem->trans_rec[idx].debit_request_size)
//        {
//            dbgTrace(DBG_LVL_INFO,
//                "TRM_STRG - GetDebitRequestMsg Error mp_id=%d buf size < debit req size (%d<%d)"
//                , mp_id, debit_request_msg_size, p_trm_mem->trans_rec[idx].debit_request_size);
//
//            result = E_BUFFER_SIZE_INSUFFICIENT;
//
//        }
//        else
//        {
//            *debit_request_msg_size = p_trm_mem->trans_rec[idx].debit_request_size;
//            memcpy(debit_request_msg, p_trm_mem->trans_rec[idx].debit_request, p_trm_mem->trans_rec[idx].debit_request_size);
//            dbgTrace(DBG_LVL_INFO,
//                "TRM_STRG - GetDebitRequestMsg mp_id=%u size=%u"
//                , mp_id, *debit_request_msg_size);
//        }
//    }
//    else
//    {
//        result = E_DATA_NOT_AVAILABLE;
//        dbgTrace(DBG_LVL_INFO,
//            "TRM_STRG - GetDebitRequestMsg Error mp_id=%d not populated yet"
//            , mp_id);
//    }
//    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
//	return 0;
//}

#ifndef CASSERT
#define CASSERT( exp, name ) typedef int dummy##name [ (exp ) ? 1 : -1 ];
#endif

enum recovery_data_type_e { RDT_DEBIT };

struct recovery_data_s
{
    uint8_t recovery_data_type;
    uint8_t version;

    uint32_t mp_id;
    uint32_t trans_id;
    uint16_t job_id_LSW;
    uint16_t weight;
    uint16_t width;
    uint16_t length;
    uint8_t  thickness;
    uint8_t  padding[1];
}__attribute__((__packed__));

typedef struct recovery_data_s recovery_data_t;

error_t trm_build_recovery_data(uint8_t idx, uint8_t RecoveryData[20])
{
////    CASSERT(sizeof(recovery_data_t) == 20, recovery_data_is_20_bytes);
//    recovery_data_t *p_recovery_data;
//    char sz_hexline[20/4*9+1] = "00010203-04050607-08090A0B-0C0D0E0F-10111213:";
//    char sz_ascline[20/4*5+1] = ".... .... .... .... ....";
//    int  grp_offset=0;
//    char sz_byte[3];
//
//    p_recovery_data = (recovery_data_t *)RecoveryData;
//    memset(p_recovery_data, 0, sizeof(recovery_data_t));
//
//    p_recovery_data->recovery_data_type = RDT_DEBIT;
//    p_recovery_data->version            = 1;
//    p_recovery_data->mp_id              = p_trm_mem->trans_rec[idx].mp_id;
//    p_recovery_data->trans_id           = p_trm_mem->trans_rec[idx].transaction_id;
//    p_recovery_data->job_id_LSW         = p_trm_mem->header.job_id[strlen(p_trm_mem->header.job_id)-1]; /* TODO: pick last 2 characters of job_id */
//    if (p_trm_mem->trans_rec[idx].flags.is_populated.dimensions)
//    {
//        p_recovery_data->weight             = p_trm_mem->trans_rec[idx].mp_dimensions.wowWeight;   /* TODO: what about units (0.01 Oz or 0.1g) */
//        p_recovery_data->width              = p_trm_mem->trans_rec[idx].mp_dimensions.width;
//        p_recovery_data->length             = p_trm_mem->trans_rec[idx].mp_dimensions.length;
//        p_recovery_data->thickness          = p_trm_mem->trans_rec[idx].mp_dimensions.thickness;
//    }
//
//    for (size_t i=0; i<sizeof(recovery_data_t) ; i++)
//    {
//        int c = RecoveryData[i];
//        sprintf(sz_byte, "%02X", c);
//
//        sz_hexline[i*2+grp_offset+0] = sz_byte[0];
//        sz_hexline[i*2+grp_offset+1] = sz_byte[1];
//
//        sz_ascline[i+grp_offset] = (isprint(c)? c : '.');
//
//        if ((i & 0x3) == 0x03)
//            grp_offset++;
//    }
//
//    dbgTrace(DBG_LVL_INFO,
//        "TRM_STRG - recovery data mp_id=%u %s%s", p_recovery_data->mp_id, sz_hexline, sz_ascline);

    return SUCCESS;
}

//float ConvertLongWeightToFloat(long value, uint8_t decimalPos, uint8_t unitsWeight)
//{
//	uint8_t i, j=0;
//	float weight = value;
//	// For metric the implied scaling is tenths of a gram and imperial it's thousands of an oz.
//	if(unitsWeight == WT_UNIT_OZ)
//		j = 3;
//	else
//		j = 1;
//
//	//if JobSetting digit number and default digit number are not match, adjust for precision.
//	for(i=0; i<j-decimalPos; i++)
//	{
//		value /= 10;
//	}
//	for(i=0; i<decimalPos-j; i++)
//	{
//		value *=10;
//	}
//
//	for(i=0; i<decimalPos; i++)
//	{
//		weight /= 10;
//	}
//
//	return weight;
//}

float ConvertFloatWeightPrecision(float value, uint8_t decimalPos)
{
	uint16_t	i;

	for(i=0; i<decimalPos; i++)
	{
		value *= 10;
	}

	int temp = value;
	//lose precision
	value = temp;
	for(i=0; i<decimalPos; i++)
	{
		value /= 10;
	}

	return value;
}


//error_t trmDebitRequested(const mp_id_t mp_id)
//{
//    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START
//
//    p_trm_mem->trans_rec[idx].debit_requested = true;
//    //p_trm_mem->trans_rec[idx].flags.is_populated.debit_requested = true;
//
//    dbgTrace(DBG_LVL_INFO,
//        "TRM_STRG - DebitRequested mp_id=%d"
//        , mp_id);
//
//    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
//}


error_t trmDebitCompleted(const mp_id_t mp_id, unsigned char *p_debit_cert, size_t cert_size)
{
    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START

	// put debit cert into either default buffer or larger external buffer

    if (cert_size <= CFG_MAX_DEBIT_CERT_SIZE)
    {
        memcpy(p_trm_mem->trans_rec[idx].debit_cert, p_debit_cert, cert_size);
        p_trm_mem->trans_rec[idx].debit_cert_size = cert_size;
        p_trm_mem->trans_rec[idx].flags.is_populated.debit_cert = true;
        p_trm_mem->trans_rec[idx].external_debit_cert = false;

        GetSysDateTime(&p_trm_mem->trans_rec[idx].debitTime, ADDOFFSETS, GREGORIAN);
        if(trm_log_level)
        	dbgTrace(DBG_LVL_INFO, "TRM_STRG - DebitCompleted mp_id=%d debit_cert_size=%u", mp_id, cert_size);
    }
    else if (cert_size <= CFG_MAX_INTL_DEBIT_CERT_SIZE)
    { // store debit cert in larger external storage
    	void * dc_ptr = &(CMOSExtTrmDebitCerts.ext_debit_cert[trmGetActiveBucket()][idx][0]);

        memcpy(dc_ptr, p_debit_cert, cert_size);
        // store pointer to data in default buffer & mark tr as having external debit cert
        memcpy(p_trm_mem->trans_rec[idx].debit_cert, &dc_ptr, 4);
        p_trm_mem->trans_rec[idx].external_debit_cert = true;

        p_trm_mem->trans_rec[idx].debit_cert_size = cert_size;
        p_trm_mem->trans_rec[idx].flags.is_populated.debit_cert = true;

        GetSysDateTime(&p_trm_mem->trans_rec[idx].debitTime, ADDOFFSETS, GREGORIAN);
        if(trm_log_level)
        	dbgTrace(DBG_LVL_INFO, "TRM_STRG - DebitCompleted mp_id=%d external debit_cert_size=%u", mp_id, cert_size);
    }
    else
    {
        result = E_TR_RECORD_FULL;
        dbgTrace(DBG_LVL_ERROR,
            "Error: TRM_STRG - DebitCompleted mp_id=%d: Not enough storage for debit cert %u", mp_id, cert_size);

    }

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}

error_t trmSetPSDRegisters(const mp_id_t mp_id
                                ,uint32_t  pieceCount
                                ,uint32_t  numOfZeroDebits
                                ,uint8_t  *p_ascendingReg
                                ,uint8_t  *p_descendingReg
                                ,uint32_t  decimalPosition)
{
    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START

    p_trm_mem->trans_rec[idx].pc = pieceCount;
    p_trm_mem->trans_rec[idx].numOfZeroDebits = numOfZeroDebits;
    memcpy(p_trm_mem->trans_rec[idx].ar, p_ascendingReg, sizeof(p_trm_mem->trans_rec[idx].ar));
    memcpy(p_trm_mem->trans_rec[idx].dr, p_descendingReg, sizeof(p_trm_mem->trans_rec[idx].dr));

//    if(decimalPosition != 0)
//    	p_trm_mem->header.decimalPosition = decimalPosition;

    p_trm_mem->trans_rec[idx].psd_registers = true;

    //if(trm_log_level)
    dbgTrace(DBG_LVL_INFO, "TRM_STRG - PutPSDRegisters mp_id=%u pieceCount=%u", mp_id, pieceCount);

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END

}


error_t trmGetDebitCertificate(const mp_id_t mp_id, unsigned char const **pp_debit_cert, size_t *cert_size)
{
    TR_REC_LOCK_VERIFY_POPULATING_OR_PENDING_UPLOAD_AND_MPID_START

    if (p_trm_mem->trans_rec[idx].flags.is_populated.debit_cert)
    {
        *cert_size = p_trm_mem->trans_rec[idx].debit_cert_size;
        *pp_debit_cert = (unsigned char const *)&p_trm_mem->trans_rec[idx].debit_cert[0];
        if (p_trm_mem->header.verbosity_lvl > 0)
            dbgTrace(DBG_LVL_INFO,
                "TRM_STRG - GetDebitCertificate: mp_id=%d debit_cert_size=%u)"
                , mp_id, cert_size);
    }
    else
    {
        result = E_DATA_NOT_AVAILABLE;
        dbgTrace(DBG_LVL_INFO,
            "TRM_STRG - GetDebitCertificate Error: Not populsted for mp_id=%d", mp_id);
    }
    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}

error_t trmImageDone(const mp_id_t mp_id)
{
    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START

    p_trm_mem->trans_rec[idx].image_done = true;
    p_trm_mem->trans_rec[idx].flags.is_populated.image_done = true;

    dbgTrace(DBG_LVL_INFO,
        "TRM_STRG - ImageDone mp_id=%d"
        , mp_id);

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}

error_t trmSetPageReadyResult(mp_id_t mp_id, uint16_t page_ready_result)
{
    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START

    p_trm_mem->trans_rec[idx].page_ready_result = page_ready_result;
    p_trm_mem->trans_rec[idx].flags.is_populated.page_ready_result = true;

    dbgTrace(DBG_LVL_INFO,
        "TRM_STRG - SetPageReadyResult mp_id=%d result=%d", mp_id, page_ready_result);

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}

//error_t trmSetPageCompleteResult(mp_id_t mp_id, uint16_t page_complete_result)
//{
//    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START
//
//    p_trm_mem->trans_rec[idx].page_complete_result = page_complete_result;
//    p_trm_mem->trans_rec[idx].flags.is_populated.page_complete_result = true;
//
//    dbgTrace(DBG_LVL_INFO,
//        "TRM_STRG - SetPageCompleteResult mp_id=%d result=%d", mp_id, page_complete_result);
//
//    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
//}

error_t trmStartPrint(const mp_id_t mp_id)
{
    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START

	p_trm_mem->trans_rec[idx].print_started = true;

//	if (p_trm_mem->header.b_recovering_to_tape)
//	{
//		p_trm_mem->trans_rec[idx].tape_used = true;
//	}
    if(trm_log_level)
    	dbgTrace(DBG_LVL_INFO, "TRM_STRG - StartPrint mp_id=%d " , mp_id);

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}

error_t trmEndPrint(const mp_id_t mp_id)
{
    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START

    GetSysDateTime(&p_trm_mem->trans_rec[idx].finish_time, ADDOFFSETS, GREGORIAN);
    p_trm_mem->trans_rec[idx].flags.is_populated.finish_time = true;

    p_trm_mem->trans_rec[idx].page_complete_result = 0;
    p_trm_mem->trans_rec[idx].flags.is_populated.page_complete_result = true;

	p_trm_mem->trans_rec[idx].print_finished = true;

//	if (p_trm_mem->header.b_recovering_to_tape)
//	{
//		p_trm_mem->trans_rec[idx].recovered = true;
//	}

	//trmSetUploadState(TRM_STATE_UPLOAD_REQUIRED);


	//check if there is any pending writes
	if(trmGetState() == TRM_STATE_PENDING_TX_FILE_SAVE)
		trmProcessTxPendingWrite();

	if(trm_log_level)
		dbgTrace(DBG_LVL_INFO, "TRM_STRG - EndPrint mp_id=%d ", mp_id);

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}

error_t trmSetPrintLength(const mp_id_t mp_id, int print_len)
{
    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START

    p_trm_mem->trans_rec[idx].print_length = print_len;
    p_trm_mem->trans_rec[idx].flags.is_populated.print_length = true;


    dbgTrace(DBG_LVL_INFO,
        "TRM_STRG - SetPrintLength mp_id=%d  len=%d"
        , mp_id, print_len);

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}


char *str_n_cat(char *p_s1_end, const char *p_s2, size_t num)
{
    char c;

    if (p_s1_end && p_s2)
    {
        while ( (c = *p_s2++) != 0)
        {
            if (num > 1)
            {
                *p_s1_end++ = c;
            }
            else
            {
                break;
            }
            num--;
        }
        *p_s1_end = '\0';
    }
    return p_s1_end;
}

//static char *time_to_str(timespec *time, char *dest)
//{
//	if (time != NULL && dest != NULL)
//    {
//	    struct tm timeinfo;
//	    char millDest[12];
//	    double millsec=0.0;
//
//	    memset(millDest, 0x0, sizeof(millDest));
//
//	    localtime_r(&time->tv_sec, &timeinfo);
//	    strftime(dest,64, "%Y-%m-%dT%T", &timeinfo);
//
//	    millsec = (double)time->tv_nsec/(double)BILLION;
//	    sprintf(millDest, "%.3f", millsec);
//	    strcat(dest, millDest+1);
//    }
//    return dest;
//}



char *mp_mode_to_str(mp_mode_t mode)
{
    switch (mode)
    {
    	case MODE_KIP 					: return "KeyInPostage";  break;
        case MODE_SEAL_ONLY				: return "SealOnly";  break;
        case MODE_PERMIT				: return "Permit";  break;
        case MODE_DATE_TIME_STAMP		: return "DateTimeStamp";  break;
        case MODE_MANUAL_WEIGHT_ENTRY	: return "ManualWeightEntry";  break;
        case MODE_EXTERNAL_SCALE		: return "ExternalScale";  break;
        case MODE_POSTAGE_CORRECTION	: return "PostageCorrection";  break;
        case MODE_DATE_CORRECTION		: return "DateCorrection";  break;
        case MODE_DIFF_WEIGH			: return "DiffWeightMode";  break;
        case MODE_MANUAL				: return "Manual";  break;
        case MODE_PRINT_HEAD_REPORT		: return "PrintheadReport";  break;
        case MODE_AJOUT					: return "ModeAjout";  break;
        case MODE_SHIPPING				: return "Shipping";  break;
        case MODE_AD_ONLY				: return "AdOnly";  break;
        case MODE_SOM					: return "StatementOfMailing";  break;
        default							: return "Unknown";
    }
}

//char *mp_status_to_str(e_mp_status_t mp_status)
//{
//    switch (mp_status)
//    {
////    	case MP_STATUS_DEBIT_STARTED 	: return "DebitStarted";  break;
////        case MP_STATUS_DEBIT_COMPLETED	: return "DebitCompleted";  break;
////        case MP_STATUS_PRINT_STARTED	: return "PrintStarted";  break;
////        case MP_STATUS_EXITED			: return "Exited";  break;
//		case TRM_POPULATING 			: return "DebitStarted";  break;
//		case MP_STATUS_DEBIT_COMPLETED	: return "DebitCompleted";  break;
//		case TRM_PENDING_UPLOAD			: return "PrintStarted";  break;
//		case MP_STATUS_EXITED			: return "Exited";  break;
//        default							: return "Unknown";
//    }
//}

char *mp_status_to_str(e_mp_status_t mp_status)
{
    switch (mp_status)
    {
//    	case MP_STATUS_DEBIT_STARTED 	: return "DebitStarted";  break;
//        case MP_STATUS_DEBIT_COMPLETED	: return "DebitCompleted";  break;
//        case MP_STATUS_PRINT_STARTED	: return "PrintStarted";  break;
//        case MP_STATUS_EXITED			: return "Exited";  break;
		case TRM_UNUSED 						: return "Unused";  break;
		case TRM_POPULATING						: return "DebitStarted";  break;
		case TRM_PENDING_UPLOAD					: return "Exited";  break;
		case TRM_PENDING_UPLOAD_CONFIRMATION	: return "Exited";  break;
        default									: return "Unknown";
    }
}

char *mp_wt_unit_to_str(e_wt_unit_t wt_unit)
{
    switch (wt_unit)
    {
    	case WT_UNIT_GRAM 	: return "g";  break;
        case WT_UNIT_OZ		: return "oz";  break;
        default				: return "Unknown";
    }
}

const char * lookup_mp_status_str(trans_rec_t *p_trans_rec, char* mp_status)
{

    bool        b_debit_failure;
    bool        b_print_failure;
    //int bucket = trmGetActiveBucket();
    //trm_mem_t *p_trm_mem = get_trm_bucket(bucket);

	b_debit_failure = (p_trans_rec->debit_required
					   && !p_trans_rec->flags.is_populated.debit_cert);

	b_print_failure = (p_trans_rec->print_required &&
					   !(   p_trans_rec->flags.is_populated.page_complete_result
						 && p_trans_rec->page_complete_result == 0));

	if (!b_debit_failure && !b_print_failure)
	{
		if(p_trans_rec->recovered)
			strcpy(mp_status, "DebitCompleted");
		else
			strcpy(mp_status, "Exited");
	}
	else if (!b_debit_failure)
	{
		if (p_trans_rec->print_started)
			strcpy(mp_status, "PrintStarted");
		else
			strcpy(mp_status, "DebitCompleted");

	}
	else
		strcpy(mp_status, "DebitStarted");

    return mp_status;
}

uint64_t LE_byte_array_to_uint64_t(uint8_t *reg, size_t num_bytes)
{
    uint64_t reg_val;
    uint64_t tmp;
    int8_t i;

    reg_val =0;
	for ( i=0; i<num_bytes; i++)
	{
		tmp = 0;
		tmp = reg[i];
		reg_val |= ( tmp << (((num_bytes - 1 - i)&7) << 3) );
	}


    return reg_val;
}

char * const get_postage_value_as_str(uint32_t total_postage_value, uint8_t  decimalPosition, char * const sz_postage_val, size_t max_len)
{
    uint32_t factor = 1;
    uint8_t i;

    memset(sz_postage_val, 0, max_len);

    for ( i=decimalPosition; i>0; i--)
    {
        factor *= 10;
    }

    if(decimalPosition == 0)
    	sprintf(sz_postage_val, "%u.%00u" , total_postage_value / factor, total_postage_value % factor);
    if(decimalPosition == 1)
    	sprintf(sz_postage_val, "%u.%01u" , total_postage_value / factor, total_postage_value % factor);
    if(decimalPosition == 2)
    	sprintf(sz_postage_val, "%u.%02u" , total_postage_value / factor, total_postage_value % factor);
    else if(decimalPosition == 3)
    	sprintf(sz_postage_val, "%u.%03u" , total_postage_value / factor, total_postage_value % factor);

    return sz_postage_val;
}

char * const get_register_value_as_str(uint64_t reg_value, uint8_t  decimalPosition, char * const sz_reg_val, size_t max_len)
{
    uint32_t factor = 1;
    uint8_t i;
    for ( i=decimalPosition; i > 0; i--)
    {
        factor *= 10;
    }

    if(decimalPosition == 2)
    	sprintf(sz_reg_val, "%llu.%02llu" , reg_value / factor, reg_value % factor);
    else if(decimalPosition == 3)
    	sprintf(sz_reg_val, "%llu.%03llu" , reg_value / factor, reg_value % factor);

    return sz_reg_val;
}


char * const get_SSamount_value_as_str(int32_t total_postage_value, uint8_t  decimalPosition, char * const sz_postage_val, size_t max_len)
{
 	const char *sign = "";
 	char i;
	if(total_postage_value < 0)
	{
		sign = "-";
		total_postage_value *= -1;
	}

    unsigned long factor = 1;
	//Note to check if Decimal position  = 0 is valid if so is Decimal supressed.
	for ( i=decimalPosition; i>0; i--)
    {
        factor *= 10;
    }


    sprintf(sz_postage_val, "%s%lu.%0*ld"
            ,sign
			,total_postage_value / factor
            , decimalPosition
            , total_postage_value % factor);

    return sz_postage_val;
}

char const * const weigh_precision_mode_to_string(uint8_t weigh_precision_mode)
{
    switch (weigh_precision_mode)
    {
        case 0: return "Normal";    break;
        case 1: return "Precision"; break;
        default:return "BadMode";   break;
    }

    return NULL;
}

//TODO: following services needs to be replaced
char* get_rate_version(void)
{
	return CMOSTrmCommon.rate_ver;
}

STATUS generate_hdr_xml(char *p_dest, int32_t max_xml_length, int32_t *p_xml_len)
{
	STATUS status = SUCCESS;
    void *temp_buf = NULL;
	char *p_wr = p_dest;
	char *p_limit = p_dest + max_xml_length;
    PSD_INFO PsdInfo;
    DATETIME        rDateTimeStamp;
    CMOS_Signature *sig;
    int bucket = trmGetActiveBucket();
    trm_mem_t *p_trm_mem = get_trm_bucket(bucket);

    #define add_to_xml(str) p_wr = str_n_cat(p_wr, (str), p_limit - p_wr)

    status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &temp_buf, CFG_MAX_DAY_ENTRY_SIZE, NU_NO_SUSPEND);
    if(temp_buf != NULL)
	{
    	//p_trm = get_trm_mem();
    	dbgTrace(DBG_LVL_INFO, "In generate_hdr_xml:");
		sig = fnCMOSSetupGetCMOSSignature();

		GetPSDInfo(&PsdInfo);

		sprintf(temp_buf,
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<Pkg xsi:noNamespaceSchemaLocation=\"schema/DMRefresh/FlatTransactionRecordFormat/Pkg_002.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" );
		add_to_xml(temp_buf);

		memset(temp_buf, 0, 50);
		sprintf(temp_buf, "<PkgVer>2</PkgVer>");
		add_to_xml(temp_buf);

		memset(temp_buf, 0, 50);
		snprintf(temp_buf, 48, "<BasePCN>%s</BasePCN>", sig->bUicPcn);
		add_to_xml(temp_buf);

		memset(temp_buf, 0, 50);
		snprintf(temp_buf, 48, "<BaseSN>%s</BaseSN>", sig->bUicSN);
		add_to_xml(temp_buf);

		memset(temp_buf, 0, 50);
		dbgTrace(DBG_LVL_INFO, "pIPSD_PCN:%s ", PsdInfo.PCN);
		//snprintf(temp_buf, 48, "<PSDPCN>%s</PSDPCN>", CMOSTrmPSDInfo.psdPCN);
		snprintf(temp_buf, 48, "<PSDPCN>%s</PSDPCN>", PsdInfo.PCN);
		add_to_xml(temp_buf);

		memset(temp_buf, 0, 50);
		dbgTrace(DBG_LVL_INFO, "pIPSD_PBISerialNum:%s", PsdInfo.PBISerialNum);
		//snprintf(temp_buf, 48, "<PBISN>%s</PBISN>", CMOSTrmPSDInfo.pbi_serial_number);
		snprintf(temp_buf, 48, "<PBISN>%s</PBISN>", PsdInfo.PBISerialNum);
		add_to_xml(temp_buf);

		memset(temp_buf, 0, 50);
		snprintf(temp_buf, 48, "<NumDig>%d</NumDig>", bVltDecimalPlaces);
		add_to_xml(temp_buf);

		memset(temp_buf, 0, 50);
		snprintf(temp_buf, 32, "<WgtUnit>%s</WgtUnit>", mp_wt_unit_to_str(p_trm_mem->header.weight_unit));
		add_to_xml(temp_buf);

		memset(temp_buf, 0, 50);
		GetSysDateTime(&rDateTimeStamp, ADDOFFSETS, GREGORIAN);
		snprintf(temp_buf, 48, "<UpDate>%2d%02d-%02d-%02dT%02d:%02d:%02d</UpDate>",
							rDateTimeStamp.bCentury,
							rDateTimeStamp.bYear,
							rDateTimeStamp.bMonth,
							rDateTimeStamp.bDay,
							rDateTimeStamp.bHour,
							rDateTimeStamp.bMinutes,
							rDateTimeStamp.bSeconds);
		add_to_xml(temp_buf);

		//TODO
		//Appver
//		memset(temp_buf, 0, 50);
//		snprintf(temp_buf, 48, "<AppVer>%s.%s.%s.%04d</AppVer>", CSD_MAJOR_VER, CSD_MINOR_VER, BUILD_NUM, 0);
//		add_to_xml(temp_buf);

		//Base ver
		memset(temp_buf, 0, 50);
		snprintf(temp_buf, 48, "<BaseVer>%s.%s.%s.%04d</BaseVer>", CSD_MAJOR_VER, CSD_MINOR_VER, BUILD_NUM, 0);
		add_to_xml(temp_buf);

//			if(CMOSSetupParams.lwBPN == 0)
//				snprintf(temp_buf, 50, "<CustId>%010lu</CustId>", CMOSSetupParams.lwPBPAcctNumber);
//			else
		memset(temp_buf, 0, 50);
		snprintf(temp_buf, 50, "<CustId>%s</CustId>", CMOSSetupParams.BPN);
		add_to_xml(temp_buf);

		//TODO: Better of handling missing origin code
		memset(temp_buf, 0, 50);
		dbgTrace(DBG_LVL_INFO, "pIPSD_originCountry:%s ", pIPSD_originCountry);
		//snprintf(temp_buf, 32, "<OrgCntry>%s</OrgCntry>", CMOSTrmPSDInfo.orgCountry);
		//snprintf(temp_buf, 24, "<OrgCntry>%s</OrgCntry>", pIPSD_originCountry);
		if(strcmp((char*)pIPSD_originCountry, "") == 0)
			snprintf(temp_buf, 32, "<OrgCntry>US</OrgCntry>");
		else
			snprintf(temp_buf, 32, "<OrgCntry>%s</OrgCntry>", pIPSD_originCountry);
		add_to_xml(temp_buf);

		//TODO
		memset(temp_buf, 0, 50);
		dbgTrace(DBG_LVL_INFO, "pIPSD_zipCode:%s ", pIPSD_zipCode);
		//snprintf(temp_buf, 48, "<SrcZip>%s</SrcZip>",CMOSTrmPSDInfo.sourceZipCode);
		snprintf(temp_buf, 48, "<SrcZip>%s</SrcZip>", pIPSD_zipCode);
		add_to_xml(temp_buf);

		//TODO
		memset(temp_buf, 0, 50);
		dbgTrace(DBG_LVL_INFO, "pIPSD_indiciaSN:%s ", PsdInfo.indiciaSN);
		//snprintf(temp_buf, 48, "<IndSN>%s</IndSN>",  (char*)CMOSTrmPSDInfo.indiciaSN);
		snprintf(temp_buf, 48, "<IndSN>%s</IndSN>",  PsdInfo.indiciaSN);
		add_to_xml(temp_buf);

		//Mailer ID
		memset(temp_buf, 0, 50);

		if( CMOSTrmCommon.flags.is_populated.mailer_id == true)
		{
			snprintf(temp_buf, 48, "<MailID>%s</MailID>", CMOSTrmCommon.mailer_id);
			add_to_xml(temp_buf);
		}

		dbgTrace(DBG_LVL_INFO, "generate_hdr_xml -mailer_id:%s, set:%d ",
				CMOSTrmCommon.mailer_id,
				CMOSTrmCommon.flags.is_populated.mailer_id );
		*p_xml_len = p_wr - p_dest;

		dbgTrace(DBG_LVL_INFO, "generate_hdr_xml: leaving");

		NU_Deallocate_Memory(temp_buf);
	}

    return status;

}


uint8_t trmGetWeightUnit(void)
{
	trm_mem_t *p_trm_mem = get_trm_mem();
	return p_trm_mem->header.weight_unit;

}

STATUS generate_tr_xml(const trans_rec_t *p_trans_rec, char *p_dest, int32_t max_xml_length, int32_t *p_xml_len, required_fields_t *prqd)
{
	STATUS status;
   	char *temp_buf = NULL;
    char *p_wr = p_dest;
    char *p_limit = p_dest + max_xml_length;

    //char asctimebuff[64];   //need this for thread safety
    status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &temp_buf, CFG_MAX_PSD_DEBIT_REQ_SIZE, NU_NO_SUSPEND);
    if(temp_buf != NULL)
    {
		if ( p_trans_rec->rec_state != TRM_UNUSED) // &&  p_trans_rec->print_finished) //p_trans_rec->print_started)
		{

			sprintf(temp_buf, "<MP>");
			add_to_xml(temp_buf);

			if(p_trans_rec->flags.is_populated.mp_id)
			{
				sprintf(temp_buf, "<MPNum>%u</MPNum>", p_trans_rec->mp_id);
				add_to_xml(temp_buf);
			}

			//Mode
			if(p_trans_rec->flags.is_populated.mode)
			{
				sprintf(temp_buf, "<Mode>%s</Mode>", p_trans_rec->mode);
				add_to_xml(temp_buf);
			}
			else
			{
				//Set ManualWeightEntry as default mode
				sprintf(temp_buf, "<Mode>ManualWeightEntry</Mode>");
				add_to_xml(temp_buf);
			}

			//DebitControl
			if(p_trans_rec->debit_required)
			{
				sprintf(temp_buf, "<DebitControl>Debit</DebitControl>");
				add_to_xml(temp_buf);
			}
			else
			{
				sprintf(temp_buf, "<DebitControl>NoDebit</DebitControl>");
				add_to_xml(temp_buf);
			}

			//			//Status
			if(p_trans_rec->rec_state)
			{
				//sprintf(temp_buf, "<status>%s</status>", mp_status_to_str(p_trans_rec->rec_state));
				snprintf(temp_buf, 50, "<status>%s</status>", lookup_mp_status_str(p_trans_rec, temp_buf+ 50));
				add_to_xml(temp_buf);
			}

			//Debit time
			if(p_trans_rec->flags.is_populated.debitTime)
			{
				sprintf(temp_buf, "<DebTime>%2d%02d-%02d-%02dT%02d:%02d:%02d</DebTime>",
									p_trans_rec->debitTime.bCentury,
									p_trans_rec->debitTime.bYear,
									p_trans_rec->debitTime.bMonth,
									p_trans_rec->debitTime.bDay,
									p_trans_rec->debitTime.bHour,
									p_trans_rec->debitTime.bMinutes,
									p_trans_rec->debitTime.bSeconds);
				add_to_xml(temp_buf);

			}

			//Carrier
			if(p_trans_rec->flags.is_populated.carrier)
			{
				snprintf(temp_buf, 32+sizeof(p_trans_rec->carrier), "<Carrier>%d</Carrier>", p_trans_rec->carrier);
				add_to_xml(temp_buf);
			}

			//Cat
			if(p_trans_rec->flags.is_populated.cat)
			{
				sprintf(temp_buf, "<Cat>%d</Cat>", p_trans_rec->cat);
				add_to_xml(temp_buf);

			}

			//Class
			if(p_trans_rec->flags.is_populated.rate_class)
			{
				snprintf(temp_buf,  32 + sizeof(p_trans_rec->cat), "<Class>%u</Class>", p_trans_rec->rate_class);
				add_to_xml(temp_buf);
			}

			//ClMail
			if(p_trans_rec->flags.is_populated.mail_class)
			{
				snprintf(temp_buf, 32 + sizeof(p_trans_rec->mail_class), "<ClMail>%s</ClMail>", p_trans_rec->mail_class);
				add_to_xml(temp_buf);
			}

			//ShrtClass
			if(p_trans_rec->flags.is_populated.short_class)
			{
				snprintf(temp_buf, 32 + sizeof(p_trans_rec->short_class), "<ShrtClass>%s</ShrtClass>", p_trans_rec->short_class);
				add_to_xml(temp_buf);
			}

			//Fee List
			if(p_trans_rec->flags.is_populated.fee_list && (p_trans_rec->fee_list.cnt > 0) && (p_trans_rec->fee_list.cnt <= 5))
			{
				int count;

				for(count = 0; count < p_trans_rec->fee_list.cnt ; count++)
				{
					sprintf(temp_buf, "<Fee>");
					add_to_xml(temp_buf);
					//FeeTkn
					if(p_trans_rec->fee_list.list[count].token >= 0)
					{
						snprintf(temp_buf, 32+sizeof(p_trans_rec->fee_list.list[count].token), "<FeeTkn>%d</FeeTkn>", p_trans_rec->fee_list.list[count].token);
						add_to_xml(temp_buf);
					}

					//SrvCod
					if(p_trans_rec->fee_list.list[count].servicecode >= 0 )
					{
						snprintf(temp_buf, 32+sizeof(p_trans_rec->fee_list.list[count].servicecode), "<SrvCod>%02d</SrvCod>",  p_trans_rec->fee_list.list[count].servicecode);
						add_to_xml(temp_buf);
					}

					//ShrtFee
					if(p_trans_rec->fee_list.list[count].feesymbol!= 0)
					{
						strncpy(temp_buf + 100, p_trans_rec->fee_list.list[count].feesymbol, sizeof(p_trans_rec->fee_list.list[count].feesymbol));

						snprintf(temp_buf, 32+sizeof(p_trans_rec->fee_list.list[count].feesymbol), "<ShrtFee>%s</ShrtFee>", temp_buf + 100);
						add_to_xml(temp_buf);
					}

					//FeeAmt
					if(p_trans_rec->fee_list.list[count].amount >= 0)
					{
						memset(temp_buf, 0, 50);
						get_postage_value_as_str(p_trans_rec->fee_list.list[count].amount, p_trans_rec->decimalPosition, temp_buf+50, sizeof(temp_buf) - 50);
						snprintf(temp_buf, 32+sizeof(p_trans_rec->fee_list.list[count].amount), "<FeeAmt>%s</FeeAmt>",  temp_buf+50);
						add_to_xml(temp_buf);
					}

					//FeeVal
					if(p_trans_rec->fee_list.list[count].value >= 0)
					{
						memset(temp_buf, 0, 50);
						get_postage_value_as_str(p_trans_rec->fee_list.list[count].value, p_trans_rec->decimalPosition, temp_buf+50, sizeof(temp_buf) - 50);
						snprintf(temp_buf, 32+sizeof(p_trans_rec->fee_list.list[count].value), "<FeeVal>%s</FeeVal>",  temp_buf+50 );
						add_to_xml(temp_buf);
					}

					sprintf(temp_buf, "</Fee>");
					add_to_xml(temp_buf);
				}


			}

			//ProdID
			if(p_trans_rec->flags.is_populated.prod_id)
			{
				snprintf(temp_buf, 32 + sizeof(p_trans_rec->prod_id), "<ProdID>%s</ProdID>", p_trans_rec->prod_id);
				add_to_xml(temp_buf);
			}

			//Weight
			if(p_trans_rec->flags.is_populated.weight)
			{
				///uint16_t rates_decPos = 3;//trmGetDecimalPosition();
				//float fWeight = ConvertLongWeightToFloat(p_trans_rec->weight, rates_decPos, trmGetWeightUnit());
				snprintf(temp_buf, 32 + sizeof(p_trans_rec->weight), "<Weight>%s</Weight>", p_trans_rec->weight);
				add_to_xml(temp_buf);
			}

			if(p_trans_rec->debit_required == true)
			{
				//Postage
				memset(temp_buf, 0, 50);
				get_postage_value_as_str(p_trans_rec->postage, bVltDecimalPlaces, temp_buf+50, sizeof(temp_buf) - 50);
				sprintf(temp_buf, "<Postage>%s</Postage>", temp_buf+50);
				add_to_xml(temp_buf);

				//BaseRate
				if(p_trans_rec->flags.is_populated.base_rate)
				{
					memset(temp_buf, 0, 100);
					get_postage_value_as_str(p_trans_rec->base_rate, p_trans_rec->decimalPosition, temp_buf+50, sizeof(temp_buf) - 50);

					sprintf(temp_buf, "<BaseRate>%s</BaseRate>", temp_buf+50);
					add_to_xml(temp_buf);
				}
				else
				{
					//TODO: in case of missing base rate Use postage
					sprintf(temp_buf, "<BaseRate>%s</BaseRate>", temp_buf+50);
					add_to_xml(temp_buf);
				}
			}

			//DestZip
			if(p_trans_rec->flags.is_populated.dest_zip)
			{
				snprintf(temp_buf, 32 + sizeof(p_trans_rec->dest_zip), "<DestZip>%s</DestZip>", p_trans_rec->dest_zip);
				add_to_xml(temp_buf);
			}

			//DestZone
			if(p_trans_rec->flags.is_populated.dest_zone)
			{
				snprintf(temp_buf, 32+sizeof(p_trans_rec->dest_zone), "<DestZone>%s</DestZone>", p_trans_rec->dest_zone);
				add_to_xml(temp_buf);
			}

			//CC
			if(p_trans_rec->flags.is_populated.country_code)
			{
				snprintf(temp_buf, 20+sizeof(p_trans_rec->country_code), "<CC>%s</CC>", p_trans_rec->country_code);
				add_to_xml(temp_buf);
			}

			//PBCC
			if(p_trans_rec->flags.is_populated.pb_cc)
			{
				snprintf(temp_buf, 32+sizeof(p_trans_rec->pb_cc), "<PBCC>%d</PBCC>", p_trans_rec->pb_cc);
				add_to_xml(temp_buf);
			}

			//TrackID
			if(p_trans_rec->flags.is_populated.track_id)
			{
				snprintf(temp_buf, 32+sizeof(p_trans_rec->track_id), "<TrackID>%s</TrackID>", p_trans_rec->track_id);
				add_to_xml(temp_buf);
			}

			//RefNum
			if(p_trans_rec->flags.is_populated.ref_num)
			{
				snprintf(temp_buf, 32+sizeof(p_trans_rec->ref_num), "<RefNum>%s</RefNum>", p_trans_rec->ref_num);
				add_to_xml(temp_buf);
			}

			//PrintedDate
			if(p_trans_rec->flags.is_populated.printed_date)
			{
				//HORCSD-3939 Transaction Record - Need to add default time to <PrintedDate>
				snprintf(temp_buf, 32+sizeof(p_trans_rec->printed_date), "<PrintedDate>%sT23:59:59</PrintedDate>", p_trans_rec->printed_date);
				add_to_xml(temp_buf);
			}

			//AR
			if(p_trans_rec->psd_registers)
			{
				uint64_t dRgValue = LE_byte_array_to_uint64_t(p_trans_rec->ar, sizeof(p_trans_rec->ar));
				//get_register_value_as_str(dRgValue, bVltDecimalPlaces, temp_buf+50, sizeof(temp_buf) - 50);
				sprintf(temp_buf, "<AR>%lu</AR>",  dRgValue);
				add_to_xml(temp_buf);


				dRgValue = LE_byte_array_to_uint64_t(p_trans_rec->dr, sizeof(p_trans_rec->dr));
				//get_register_value_as_str(dRgValue, bVltDecimalPlaces, temp_buf+50, sizeof(temp_buf) - 50);
				sprintf(temp_buf, "<DR>%lu</DR>", dRgValue);

				add_to_xml(temp_buf);

				sprintf(temp_buf, "<PC>%u</PC>", p_trans_rec->pc);

				add_to_xml(temp_buf);

			}

			if(p_trans_rec->debit_required == true)
			{
				//Cert
				if(p_trans_rec->debit_cert_size)
				{ // get debit cert either from default buffer or larger external buffer
					unsigned long result_len = 0;
			    	char * dc_ptr ;

			    	if (p_trans_rec->external_debit_cert == true)
			    	{ // use external debit cert buffer - default buffer contains pointer to it
			            memcpy(&dc_ptr, p_trans_rec->debit_cert, 4);
			    	}
			    	else
			    	{ // use default debit cert buffer
				        dc_ptr = (char *) p_trans_rec->debit_cert;
			    	}

					if (B64Encode((unsigned char*) dc_ptr, p_trans_rec->debit_cert_size,
									temp_buf, CFG_MAX_PSD_DEBIT_REQ_SIZE, &result_len, TRUE) == CONVERSION_OK)
					{
						temp_buf[result_len] = '\0';

						add_to_xml("<Cert>");

						add_to_xml( temp_buf);

						add_to_xml( "</Cert>");
					}
					else
					{
						sprintf(temp_buf, "<Cert>Invalid Debit Cert</Cert>");
						add_to_xml(temp_buf);
					}
				}
			}

			//EKP
			if(p_trans_rec->flags.is_populated.ekp)
			{
				snprintf(temp_buf, 20+sizeof(p_trans_rec->ekp),  "<EKP>%s</EKP>", p_trans_rec->ekp);
				add_to_xml(temp_buf);
			}

			//EKPTyp
			if(p_trans_rec->flags.is_populated.ekp_type)
			{
				snprintf(temp_buf, 32+sizeof(p_trans_rec->ekp_type), "<EKPTyp>%u</EKPTyp>", p_trans_rec->ekp_type);
				add_to_xml(temp_buf);
			}

			//Consignor
			if(p_trans_rec->flags.is_populated.consignor)
			{
				snprintf(temp_buf, 32+sizeof(p_trans_rec->consignor), "<Consignor>%s</Consignor>", p_trans_rec->consignor);
				add_to_xml(temp_buf);
			}

			//Consignee
			if(p_trans_rec->flags.is_populated.consignee)
			{
				snprintf(temp_buf, 32+sizeof(p_trans_rec->consignee), "<Consignee>%s</Consignee>", p_trans_rec->consignee);
				add_to_xml(temp_buf);
			}

			//TaskNum
			if(p_trans_rec->flags.is_populated.task_num)
			{
				snprintf(temp_buf, 32+sizeof(p_trans_rec->task_num), "<TaskNum>%d</TaskNum>", p_trans_rec->task_num);
				add_to_xml(temp_buf);
			}

			//ActName
			if(p_trans_rec->flags.is_populated.act_name)
			{
				snprintf(temp_buf, 32+sizeof(p_trans_rec->act_name), "<ActName>%s</ActName>", p_trans_rec->act_name);
				add_to_xml(temp_buf);
			}

			//ActCode
			if(p_trans_rec->flags.is_populated.actCode)
			{
				snprintf(temp_buf, 32+sizeof(p_trans_rec->actCode), "<ActCode>%s</ActCode>", p_trans_rec->actCode);
				add_to_xml(temp_buf);
			}

			//BatchID
			if(p_trans_rec->flags.is_populated.batch_id)
			{
				sprintf(temp_buf, "<BatchID>%s</BatchID>", p_trans_rec->batch_id);
				add_to_xml(temp_buf);
			}

			//AcctBatchEnd
			if(p_trans_rec->acct_batch_end)
			{
				sprintf(temp_buf, "<AcctBatchEnd>true</AcctBatchEnd>");
				add_to_xml(temp_buf);
			}


			//SurTyp
			sprintf(temp_buf, "<SurTyp>0</SurTyp>");
			add_to_xml(temp_buf);

			//Tag1
			if(p_trans_rec->flags.is_populated.tag1)
			{
				sprintf(temp_buf, "<Tag1>%s</Tag1>", p_trans_rec->tag1);
				add_to_xml(temp_buf);
			}

			//Tag2
			if(p_trans_rec->flags.is_populated.tag2)
			{
				sprintf(temp_buf, "<Tag2>%s</Tag2>", p_trans_rec->tag2);
				add_to_xml(temp_buf);
			}

			//Oper
			if(p_trans_rec->flags.is_populated.oper)
			{
				snprintf(temp_buf, 32+sizeof(p_trans_rec->oper), "<Oper>%s</Oper>", p_trans_rec->oper);
				add_to_xml(temp_buf);
			}
			else
			{
				snprintf(temp_buf, 32+sizeof(p_trans_rec->oper), "<Oper>supersuper</Oper>");
				add_to_xml(temp_buf);
			}

			//Tape
			if(p_trans_rec->flags.is_populated.tape)
			{
				//Add tape tag when it is true
				if(p_trans_rec->tape)
				{
					sprintf(temp_buf, "<Tape>true</Tape>");
					add_to_xml(temp_buf);
				}
			}

			//Graph
			if(p_trans_rec->flags.is_populated.graph)
			{
				unsigned short len;
				unsigned short slen ;
				unsigned short graphUnicode[128]={0};
				char graphName[128];
				short result;

				memset(graphName, 0, sizeof(graphName));
				slen = strlen(p_trans_rec->graph);
				if(slen > sizeof(p_trans_rec->graph))
					slen = sizeof(p_trans_rec->graph);

				len = sizeof(graphName);
				result = fnXmlConvertEscapeCharInString(graphName, &len, (char*)p_trans_rec->graph, slen);
				if(result == SUCCESS)
				{
					if(len >= sizeof(graphName))
						graphName[sizeof(graphName) -1] = 0;

					//convert graphic name in case some 16 bit unicode, such as Latin Capital 
					//letter E with acute can't be displayed in xml file
					fnAscii2Unicode(graphName, graphUnicode, strlen(graphName));                                                     
					(void)UnicodeToUTF8(graphName, sizeof(graphName), graphUnicode, fnUnicodeLen((unsigned char*)graphUnicode));

					snprintf(temp_buf,  CFG_MAX_DAY_ENTRY_SIZE, "<Graph>%s</Graph>", graphName);
					add_to_xml(temp_buf);
				}
			}

			sprintf(temp_buf, "</MP>");
			add_to_xml(temp_buf);

		}
		#if 0
		else
		{
			sprintf(temp_buf,"<gen_xml_error>can not find mp_id %u</gen_xml_error>", mp_id);
			add_to_xml(temp_buf);
		}
		#endif

		NU_Deallocate_Memory(temp_buf);
    }
    else
    {
    	status = E_CB_BUFFER_ERROR;
    }

	*p_xml_len = p_wr - p_dest;

    return status;
}



char *rec_state_to_str(rec_state_t rec_state)
{
    switch (rec_state)
    {
        case TRM_UNUSED                         : return "UNUSED";     break;
        case TRM_POPULATING                     : return "POPULATING"; break;
        case TRM_PENDING_UPLOAD                 : return "PENDING UPLOAD"; break;
        case TRM_PENDING_UPLOAD_CONFIRMATION    : return "PENDING UPLOAD CONFIRMATION"; break;
        default:
            return "unknown";
    }
}

void dump_TR(uint16_t idx)
{
    char asctimebuff[64];   //need this for thread safety
	char buffer[80];

    int bucket = trmGetActiveBucket();
    trm_mem_t *p_trm_mem = get_trm_bucket(bucket);

    trans_rec_t *p_trans_rec = &p_trm_mem->trans_rec[idx];

    sprintf(buffer, "dump_TR rec [ %d ] state [ %s ]", idx, rec_state_to_str(p_trans_rec->rec_state) );
    fnDumpStringToSystemLog( buffer );

    sprintf(buffer,  "populated fields=[ " );
    fnDumpStringToSystemLog( buffer );

    if (p_trans_rec->flags.is_populated.mp_id              ) fnDumpStringToSystemLog("mid "); //"MP_ID ";
//    if (p_trans_rec->flags.is_populated.dimensions         ) fnDumpStringToSystemLog("dim "); //"dimensions ";
//    if (p_trans_rec->flags.is_populated.length             ) fnDumpStringToSystemLog("len "); //"length ";
//    if (p_trans_rec->flags.is_populated.rated              ) fnDumpStringToSystemLog("rtd "); //"rated ";
//    if (p_trans_rec->flags.is_populated.rate_info          ) fnDumpStringToSystemLog("rif "); //"rate_info ";
//    if (p_trans_rec->debit_requested    					 ) 	fnDumpStringToSystemLog("drq "); //"debit_requested ";
    if (p_trans_rec->flags.is_populated.image_done         ) fnDumpStringToSystemLog("imd "); //"image_done ";
//    if (p_trans_rec->flags.is_populated.print_started_ph1  ) fnDumpStringToSystemLog("ps1 "); //"print_started_ph1 ";
//    if (p_trans_rec->flags.is_populated.print_started_ph2  ) fnDumpStringToSystemLog("ps2 "); //"print_started_ph2 ";
//    if (p_trans_rec->flags.is_populated.print_finished_ph1 ) fnDumpStringToSystemLog("pf1 "); //"print_finished_ph1 ";
//    if (p_trans_rec->flags.is_populated.print_finished_ph2 ) fnDumpStringToSystemLog("pf2 "); //"print_finished_ph2 ";
//    if (p_trans_rec->flags.is_populated.print_length_ph1   ) fnDumpStringToSystemLog("pl1 "); //"print_length_ph1 ";
//    if (p_trans_rec->flags.is_populated.print_length_ph2   ) fnDumpStringToSystemLog("pl2 "); //"print_length_ph2 ";
    if (p_trans_rec->flags.is_populated.debit_cert         ) fnDumpStringToSystemLog("dce "); //"debit_cert ";
    if (p_trans_rec->psd_registers      					) fnDumpStringToSystemLog("reg "); //"psd registers ";
    if (p_trans_rec->flags.is_populated.finish_time        ) fnDumpStringToSystemLog("fit "); //"finish_time ";
    if (p_trans_rec->flags.is_populated.page_ready_result  ) fnDumpStringToSystemLog("prr "); //"page_ready_result ";
    if (p_trans_rec->flags.is_populated.page_complete_result)fnDumpStringToSystemLog("pcr "); //"page_complete_result ";
//    if (p_trans_rec->flags.is_populated.retained_cnt_posted) fnDumpStringToSystemLog("rbc "); //"retained_cnt_posted ";
    if (p_trans_rec->flags.is_populated.graph) fnDumpStringToSystemLog("grf");	//"graphic ";
//    if (p_trans_rec->flags.is_populated.printer_time_margin) fnDumpStringToSystemLog("ptm" ); //"printer_time_margin ";
//    if (p_trans_rec->flags.is_populated.tar1_length) fnDumpStringToSystemLog("t1l"); //"tar1_length ";
//    if (p_trans_rec->flags.is_populated.batch_cnt_posted) fnDumpStringToSystemLog("btc"); //"batch_cnt_posted ";
    fnDumpStringToSystemLog("]\n");

    if (p_trans_rec->recovered)
    {
        fnDumpStringToSystemLog("****Recovered=true**** \n");
    }

    if (p_trans_rec->flags.is_populated.mp_id)
    {
    	sprintf(buffer, "    mp_id= %u  TransactionID= %u \n", p_trans_rec->mp_id, p_trans_rec->transaction_id );
        fnDumpStringToSystemLog(buffer) ;
        if (p_trans_rec->ph_maintenance_done) fnDumpStringToSystemLog("     Print Maintenance Done Before \n");
        //time_to_str(&p_trans_rec->start_time, asctimebuff);
        sprintf(buffer, "    start_time= %s \n", asctimebuff );
        fnDumpStringToSystemLog(buffer);
        sprintf(buffer, "         media= %s \n",  (p_trans_rec->tape_used ? "Tape" : "Env"));
        fnDumpStringToSystemLog(buffer);
    }


//
//    if (p_trans_rec->flags.is_populated.length)
//        fnDumpStringToSystemLog("    length=" << p_trans_rec->length << "\n";
//
//    if (p_trans_rec->flags.is_populated.print_length_ph1   ) fnDumpStringToSystemLog("    print_length_ph1=" << p_trans_rec->print_length_ph1 << "\n";
//    if (p_trans_rec->flags.is_populated.print_length_ph2   ) fnDumpStringToSystemLog("    print_length_ph2=" << p_trans_rec->print_length_ph2 << "\n";
//
//    if (p_trans_rec->flags.is_populated.rate_info && p_trans_rec->rate_info_size != 0)
//    {
//        fnDumpStringToSystemLog("    rateinfo=" << endl;
//        hex_dump((uint8_t *)p_trans_rec->rate_info, p_trans_rec->rate_info_size);
//    }
//
//    if (p_trans_rec->flags.is_populated.debit_request && p_trans_rec->debit_request_size != 0)
//    {
//        fnDumpStringToSystemLog("    deb_req_msg= \n");
//        hex_dump((uint8_t *)p_trans_rec->debit_request, p_trans_rec->debit_request_size);
//    }
//
//    if (p_trans_rec->flags.is_populated.debit_cert         )
//    {
//        fnDumpStringToSystemLog("     debit_cert=");
//        hex_dump((uint8_t *)p_trans_rec->debit_cert, p_trans_rec->debit_cert_size);
//    }
//
//    if (p_trans_rec->psd_registers)
//    {
//    	sprintf(buffer, "    PC= %d \n", (size_t)p_trans_rec->pc );
//        fnDumpStringToSystemLog(buffer);
//        fnDumpStringToSystemLog("    AR=" );
//        hex_dump((uint8_t *)p_trans_rec->ar, sizeof(p_trans_rec->ar));
//        fnDumpStringToSystemLog("    DR=");
//        hex_dump((uint8_t *)p_trans_rec->dr, sizeof(p_trans_rec->dr));
//        sprintf(buffer, "    DP= %u) \n", (size_t)trmGetDecimalPosition());
//        fnDumpStringToSystemLog(buffer);
//        sprintf(buffer, "    #0= %d \n", (size_t)p_trans_rec->numOfZeroDebits);
//        fnDumpStringToSystemLog(buffer);
//    }
//
//    if (p_trans_rec->flags.is_populated.page_ready_result  )
//    {
//        fnDumpStringToSystemLog("page_ready_result=" << p_trans_rec->page_ready_result << "\n";
//    }
//
//    if (p_trans_rec->flags.is_populated.page_complete_result  )
//    {
//        fnDumpStringToSystemLog("page_complete_result=" << p_trans_rec->page_complete_result << "\n";
//    }
//
    if (p_trans_rec->flags.is_populated.finish_time        )
    {
        //time_to_str(&p_trans_rec->finish_time, asctimebuff);
        sprintf(buffer, "    finish_time= %s \n", asctimebuff );
        fnDumpStringToSystemLog(buffer);
    }


    if (p_trans_rec->flags.is_populated.graph)
    {
    	sprintf(buffer, "	graphic[ %s ] \n	", p_trans_rec->graph);
        fnDumpStringToSystemLog(buffer);
    }

}

void dump_mp(mp_id_t mp_id)
{
	char buf[80];
    int bucket = trmGetActiveBucket();
    trm_mem_t *p_trm_mem = get_trm_bucket(bucket);

    if (p_trm_mem->trans_rec[get_mp_idx(mp_id)].mp_id == mp_id)
    {
        dump_TR(get_mp_idx(mp_id));
    }
    else
    {
        sprintf(buf,  "dump_mp: mp id %u not found", mp_id);
        fnDumpStringToSystemLog( buf );
    }
}


error_t trm_recovery_data(uint8_t idx, uint8_t RecoveryData[20])
{
//    CASSERT(sizeof(recovery_data_str_t) == 20, recovery_data_is_20_bytes);
//    recovery_data_str_t *p_recovery_data;
//    char sz_hexline[20/4*9+1] = "00010203-04050607-08090A0B-0C0D0E0F-10111213:";
//    char sz_ascline[20/4*5+1] = ".... .... .... .... ....";
//    int  grp_offset=0;
//    char sz_byte[3];
//
//    p_recovery_data = (recovery_data_str_t *)RecoveryData;
//    memset(p_recovery_data, 0, sizeof(recovery_data_str_t));
//
//
//    p_recovery_data->jobid 		= atoi(p_trm_mem->header.job_id); /* TODO: pick last character of job_id */
//    p_recovery_data->transaction_id	= p_trm_mem->trans_rec[idx].transaction_id;
//
//    dbgTrace(DBG_LVL_INFO,
//            "TRM_STRG - p_trm_mem->trans_rec[idx].transaction_id=%d  "
//            "p_recovery_data->transaction_id=%d  "
//            "p_trm_mem->header.job_id[strlen(p_trm_mem->header.job_id)]=%d",
//            p_trm_mem->trans_rec[idx].transaction_id,
//            p_recovery_data->transaction_id,
//            p_trm_mem->header.job_id[strlen(p_trm_mem->header.job_id)-1]);
//
//    if (p_trm_mem->trans_rec[idx].flags.is_populated.rate_info)
//    {
//		CRatingServicesInterface rating;
//		uint32_t DimensionBreakIndex;
//		eRateDataType type;
//        uint32_t  dataSize = 0;
//
//        rating.GetRateData( eDimensionBreakIndex,
//							0,
//							p_trm_mem->trans_rec[idx].rate_info,
//							&DimensionBreakIndex,
//							sizeof(DimensionBreakIndex),
//							type,
//							dataSize);
//        // TODO: check this value
//        Transaction trackingTransaction;
//        rating.getTrackingTransaction((void*)p_trm_mem->trans_rec[idx].rate_info,
//        		p_trm_mem->trans_rec[idx].rate_info_size,
//        		trackingTransaction);
//    	p_recovery_data->dimension_index = DimensionBreakIndex; //0;
//    	p_recovery_data->track_Id_table_Index = trackingTransaction.getTrackingIdIndex();
//    }
//
//    if (p_trm_mem->trans_rec[idx].flags.is_populated.dimensions)
//    {
//    	p_recovery_data->raw_weight = p_trm_mem->trans_rec[idx].mp_dimensions.wowWeight;
//    }
//
//    if(p_trm_mem->header.retained_cnt.bActive == true)
//    {
//    	p_recovery_data->backup_counter = p_trm_mem->header.retained_cnt.value; //p_trm_mem->trans_rec[idx].retained_cnt;
//    }
//
//
//    for (size_t i=0; i<sizeof(recovery_data_t) ; i++)
//    {
//        int c = RecoveryData[i];
//        sprintf(sz_byte, "%02X", c);
//
//        sz_hexline[i*2+grp_offset+0] = sz_byte[0];
//        sz_hexline[i*2+grp_offset+1] = sz_byte[1];
//
//        sz_ascline[i+grp_offset] = (isprint(c)? c : '.');
//
//        if ((i & 0x3) == 0x03)
//            grp_offset++;
//    }
//
//    dbgTrace(DBG_LVL_INFO,
//        "TRM_STRG - recovery data mp_id=%u %s%s", idx, sz_hexline, sz_ascline);
//
    return SUCCESS;
}



//error_t trmGetNoDebitRequestData(const mp_id_t mp_id, uint8_t RecoveryData[20])
//{
//    uint8_t idx;
//    error_t result = SUCCESS;
//    idx = get_mp_idx(mp_id);
//
//    if (POSIX_SUCCESS == pthread_mutex_lock( &trm_semaphore.hdr_rwlock ))
//    {
//    	if(p_trm_mem->header.retained_cnt.bActive == true)
//    	{
//	        if (POSIX_SUCCESS == pthread_mutex_lock( &p_trm_mem->trans_rec[idx].rec_mutex ))
//	        {
//	        	//Build Recovery Data
//	        	recovery_data(idx, RecoveryData);
//
//	            dbgTrace(DBG_LVL_INFO,
//	                "TRM_STRG - GetNoDebitRequestData mp_id=%d  RecoveryData=%s"
//	                ,mp_id
//	                ,RecoveryData);
//
//	            pthread_mutex_unlock( &p_trm_mem->trans_rec[idx].rec_mutex);
//	        }
//    	}
//        pthread_mutex_unlock(&trm_semaphore.hdr_rwlock);
//    }
//    else
//    {
//        result = E_MUTEX_LOCK_FAILED;
//        dbgTrace(DBG_LVL_INFO, "TRM_STRG - could not apply lock in"); // %s line %d", __PRETTY_FUNCTION__ , __LINE__ );
//    }
//    return result;
//}


//error_t trmGetLastTranIDFromLog(const int psdID, uint16_t *lastTrecID)
//{
//	error_t result = SUCCESS;
//
//	iov_t iovs[3];
//	iov_t iovr[2];
//
//	REG_LOG_MSG_TYPE regLog;
//	psd_reg_log_str_t psdRegLog;
//	int status = SUCCESS;
//
//	// Now package the msg ID with message args.
//	msg_buf_t header;
//	header.msgID = mReadPSDRegisterLog;
//	regLog.logType = 1;		// REGISTER_LOG
//	regLog.logDirection = 0; //newest log entry
//	regLog.logEntryToStart = 0; //start of the log entry to retrieve
//	regLog.numOfLogToRead = 1; //number of log entry to read
//
//	SETIOV(&iovs[0], &header, sizeof(header));
//	SETIOV(&iovs[1], &psdID, sizeof(psdID));
//	SETIOV(&iovs[2], &regLog, sizeof(regLog));
//
//
//	SETIOV(&iovr[0], &status, sizeof(status));
//	SETIOV(&iovr[1], &psdRegLog, sizeof(psdRegLog));
//
//    if (m_trm_srv_fd == -1)
//    {
//        m_trm_srv_fd = name_open(TRM_SERVER_NAME, 0);
//    }
//
//	int msgstat = MsgSendv(m_trm_srv_fd,iovs,SIZEOF(iovs),iovr,SIZEOF(iovr));
//
//	dbgTrace(DBG_LVL_INFO,
//					"Read PSD Register Logs Message received status %d", msgstat);
//
//    if(msgstat == -1)
//	{
//		char mess[100];
//		snprintf(mess,sizeof(mess),"ReadPSDRegisterLog::MsgSendv err %d (%s)",errno, strerror(errno));
//	    dbgTrace(DBG_LVL_INFO,
//				mess);
//	}
//    else
//    {
//    	if(status == SUCCESS)
//    	{
//    		lastTrecID = psdRegLog.sRecoveryData.transaction_id;
//    	}
//    	else
//    	{
//    		lastTrecID = 0;
//    		result = (error_t)status;
//    	}
//
//    	dbgTrace(DBG_LVL_INFO,
//    			"TRM_STRG - GetLastTranIDFromLog :%d", lastTrecID);
//    }
//	return result;
//
//}

//error_t trmPFValidateParameters(int *status)
//{
//	iov_t iovs;
//	// Now package the msg ID with message args.
//
//	msg_buf_t msgID;
//	msgID.msgID = mPFValidateParameters;
//	SETIOV(&iovs, &msgID, sizeof(msgID));
//
//    if (m_trm_srv_fd == -1)
//    {
//        m_trm_srv_fd = name_open(TRM_SERVER_NAME, 0);
//    }
//
//	int msgstat = MsgSendvs(m_trm_srv_fd,&iovs,1,&status,sizeof(status));
//
//
//	dbgTrace(DBG_LVL_INFO,
//					"PFValidateParameters Message received status %d", msgstat);
//
//    if(msgstat == -1)
//	{
//		char mess[100];
//		snprintf(mess,sizeof(mess),"PFValidateParameters::MsgSendv err %d (%s)",errno, strerror(errno));
//	    dbgTrace(DBG_LVL_INFO,
//				mess);
//	}
//
//    return SUCCESS;
//
//}

//error_t trmReCreateTRecForPwrFail(const uint32_t start_transID, uint32_t *failed_transID, int *status)
//{
//	error_t result = SUCCESS;
//
//	iov_t iovs[2];
//	iov_t iovr[2];
//
//	// Now package the msg ID with message args.
//	msg_buf_t header;
//	header.msgID = mCreateTRecForPwrFail;
//
//	SETIOV(&iovs[0], &header, sizeof(header));
//	SETIOV(&iovs[1], &start_transID, sizeof(start_transID));
//
//	SETIOV(&iovr[0], &status, sizeof(status));
//	SETIOV(&iovr[1], &failed_transID, sizeof(failed_transID));
//
//    if (m_trm_srv_fd == -1)
//    {
//        m_trm_srv_fd = name_open(TRM_SERVER_NAME, 0);
//    }
//
//	int msgstat = MsgSendv(m_trm_srv_fd,iovs,SIZEOF(iovs),iovr,SIZEOF(iovr));
//
//	dbgTrace(DBG_LVL_INFO,
//					"ReCreateTRecForPwrFail Message received status %d", msgstat);
//
//    if(msgstat == -1)
//	{
//		char mess[100];
//		snprintf(mess,sizeof(mess),"ReCreateTRecForPwrFail::MsgSendv err %d (%s)",errno, strerror(errno));
//	    dbgTrace(DBG_LVL_INFO,
//				mess);
//	}
//
//	dbgTrace(DBG_LVL_INFO,
//					"ReCreateTRecForPwrFail status = %d and failed trans ID=%d", status, failed_transID);
//
//
//    return result;
//}

error_t trmSetTRecRecreated(const mp_id_t mp_id)
{
    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START

    p_trm_mem->trans_rec[idx].PFTRecReCreated = true;

    dbgTrace(DBG_LVL_INFO,
        "SetTRecRecreated to true");

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}

error_t trmSetColumnPrinted(const mp_id_t mp_id, const uint16_t column_printed)
{
    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START

    p_trm_mem->trans_rec[idx].column_printed = column_printed;
    p_trm_mem->trans_rec[idx].PF_piece_status_printed = true;
    p_trm_mem->trans_rec[idx].flags.is_populated.column_printed = true;

    dbgTrace(DBG_LVL_INFO,
       "TRM_STRG - SetColumnPrinted mp_id=%d. column_printed=%d"
       , mp_id, p_trm_mem->trans_rec[idx].column_printed );

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}

error_t trmSetMACData(const mp_id_t mp_id, char* MacData)
{
    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START

//    memcpy(p_trm_mem->trans_rec[idx].MACData,MacData, CFG_MAX_MAC_LEN);
//    p_trm_mem->trans_rec[idx].flags.is_populated.MACData_posted = true;
//
//    dbgTrace(DBG_LVL_INFO,
//       "TRM_STRG - MACData mp_id=%d. MACData=%s"
//       , mp_id, p_trm_mem->trans_rec[idx].MACData );

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}

error_t trmSetPrintedGraphics(const mp_id_t mp_id, char* graph, uint16_t size)
{
    // do nothing for empty vector
    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START
    if(size == 0)
    	return SUCCESS;

    //support max 4
    if(size > CFG_MAX_GRAPHIC_FILE_NAME)
    	size = CFG_MAX_GRAPHIC_FILE_NAME;

    memcpy(&p_trm_mem->trans_rec[idx].graph[0], graph, size);

    dbgTrace(DBG_LVL_INFO,
		   "TRM_STRG - SetPrintedGraphics mp_id=%d. print file name of graphic = %s"
		   , mp_id, p_trm_mem->trans_rec[idx].graph);

    p_trm_mem->trans_rec[idx].flags.is_populated.graph = true;

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}

bool generate_tracking_service_xml(trans_rec_t *trans_rec, char *xml, int xmlSize)
{
//	Transaction ts;
	bool retVal = false;
//
//	memset(xml, 0x0, xmlSize);
//
//	/*
//	dbgTrace(DBG_LVL_INFO,
//	       "TRM_STRG - generate_tracking_service_xml >" );
//
//	dbgTrace(DBG_LVL_INFO,
//		       "TRM_STRG - rating.getTrackingTransaction(%08X, %08X)",
//		       trans_rec->rate_info, trans_rec->rate_info_size );
//	 */
//
//	CRatingServicesInterface rating;
//	int trackingStatus =
//		rating.getTrackingTransaction(trans_rec->rate_info,
//										trans_rec->rate_info_size,
//										ts);
//	if (trackingStatus == RS_SUCCESS)
//	{
//		/*
//		dbgTrace(DBG_LVL_INFO,
//			       "TRM_STRG - got tracking transaction" );
//		*/
//
//		if(ts.isTrackingServiceSelected() == true)
//		{
//			/*
//			dbgTrace(DBG_LVL_INFO,
//						       "TRM_STRG - tracking service selected" );
//			*/
//
//		    char temp_buf[128];
//
//	        snprintf(xml, xmlSize, "<Tracking><TrackingId>%s</TrackingId>", ts.getTrackingId());
//
//	        snprintf(temp_buf, sizeof(temp_buf), "<ClassOfMail>%s</ClassOfMail>", ts.getClassOfMail());
//	        strcat(xml, temp_buf);
//
//			unsigned short bLen = 0;
//			bLen = strlen(ts.getClassShortName());
//
//			char getShortName[bLen + 1];
//			unsigned short bDest[bLen + 1];
//			char shortNameBuff[256];
//
//			memset(bDest, 0x0, sizeof(bDest));
//			memset(getShortName, 0x0, sizeof(getShortName));
//			(void)memset((void*)shortNameBuff,0x0,sizeof(shortNameBuff));
//
//			memcpy(getShortName, ts.getClassShortName(), sizeof(getShortName));
//			fnTRMAscii2Unicode(getShortName, bDest, bLen);
//
//			uint16_t uUTF8Len= 0;
//			int cStringLen = sizeof(shortNameBuff);
//			uUTF8Len = UnicodeToUTF8(shortNameBuff, cStringLen, bDest, (int)bLen);
//
//			char temp_shortname[256];
//			uint32_t outLen = 0;
//			memset(temp_shortname, 0x0, sizeof(temp_shortname));
//			encode_xml_entity_chars (temp_shortname, 256, outLen, shortNameBuff, uUTF8Len);
//
//			dbgTrace(DBG_LVL_INFO,
//										"TRM_STRG - Tracking Short Class Name UTF8Len=%d and outlen=%d", uUTF8Len, outLen);
//
//
//	        //optional item
//	        // TODO: display empty tag or display nothing if no short class name?
//	        snprintf(temp_buf, sizeof(temp_buf), "<ShortClassName>%s</ShortClassName>", temp_shortname );
//	        strcat(xml, temp_buf);
//
//	        //optional item
//	        // TODO: display empty tag or display nothing if no ref number?
//	        char temp_cust_ref[128];
//	        outLen = 0;
//	        uint32_t inputLen = strlen(p_trm_mem->header.RefNumValue);
//	        if(inputLen > 0)
//			{
//	        	encode_xml_entity_chars (temp_cust_ref, 128, outLen, p_trm_mem->header.RefNumValue, inputLen);
//			    snprintf(temp_buf, sizeof(temp_buf), "<RefNum>%s</RefNum>", temp_cust_ref);
//			    strcat(xml, temp_buf);
//			}
//	        // HRTIndicaStrings
//			uint32_t HRTStrNum = 0;
//			HRTStrNum = ts.getNumberHumanReadableTextStrings();
//			char hrtbuffer[TS_STRING_LENGHT + 1];
//			char temp_hrtbuffer[4*TS_STRING_LENGHT];
//
//			for(uint32_t i=0; i < HRTStrNum; i++)
//			{
//				const uint16_t *pHRTString = ts.getHumanReadableTextString(i);
//				if (pHRTString)
//				{
//
//					(void)memset((void*)hrtbuffer,0x0,sizeof(hrtbuffer));
//					uint16_t uUTF8Len= 0;
//					int cStringLen = sizeof(hrtbuffer);
//					uUTF8Len = UnicodeToUTF8(hrtbuffer, cStringLen, pHRTString, TS_STRING_LENGHT);
//
//					dbgTrace(DBG_LVL_INFO,
//							"TRM_STRG - Tracking Indicia String Unicode Length uUTF8Len=%d", uUTF8Len);
//
//					memset(temp_hrtbuffer, 0x0, sizeof(temp_hrtbuffer));
//					uint32_t outLen = 0;
//			        encode_xml_entity_chars (temp_hrtbuffer, 4*TS_STRING_LENGHT, outLen, hrtbuffer, uUTF8Len);
//					sprintf(temp_buf, "<IndiciaStr>%s</IndiciaStr>", temp_hrtbuffer );
//					strcat(xml, temp_buf);
//				}
//			}
//
//			char numSpecialService = ts.getNumberSpecialServices();
//			if(numSpecialService > 0)
//				strcat(xml, "<SpecialSvcs>");
//
//	        for(char i=0; i<numSpecialService; i++)
//	        {
//	        	SpecialService	ss;
//	        	ss = ts.getSpecialService(i);
//
//				snprintf(temp_buf, sizeof(temp_buf), "<SpecSvc><Code>%s</Code>", ss.getCode());
//				strcat(xml, temp_buf);
//
//				snprintf(temp_buf, sizeof(temp_buf), "<Token>%u</Token>", ss.getToken());
//				strcat(xml, temp_buf);
//
//				unsigned short bLen = 0;
//				bLen = strlen(ss.getShortName());
//
//				char getShortName[bLen + 1];
//				unsigned short bDest[bLen + 1];
//				char shortNameBuff[256];
//
//				memset(bDest, 0x0, sizeof(bDest));
//				memset(getShortName, 0x0, sizeof(getShortName));
//				(void)memset((void*)shortNameBuff,0x0,sizeof(shortNameBuff));
//
//				memcpy(getShortName, ss.getShortName(), sizeof(getShortName));
//				fnTRMAscii2Unicode(getShortName, bDest, bLen);
//
//				uint16_t uUTF8Len= 0;
//				int cStringLen = sizeof(shortNameBuff);
//				uUTF8Len = UnicodeToUTF8(shortNameBuff, cStringLen, bDest, (int)bLen);
//
//				char temp_shortname[256];
//				uint32_t outLen = 0;
//				memset(temp_shortname, 0x0, sizeof(temp_shortname));
//				encode_xml_entity_chars (temp_shortname, 256, outLen, shortNameBuff, uUTF8Len);
//
//				dbgTrace(DBG_LVL_INFO,
//											"TRM_STRG - Tracking Special services Short Fee Name UTF8Len=%d and outlen=%d", uUTF8Len, outLen);
//
//
//
//				snprintf(temp_buf, sizeof(temp_buf), "<ShortFeeName>%s</ShortFeeName></SpecSvc>", temp_shortname);
//				strcat(xml, temp_buf);
//	        }
//			if(numSpecialService > 0)
//				strcat(xml, "</SpecialSvcs>");
//
//	        strcat(xml, "</Tracking>");
//	        retVal = true;
//		}
//		else
//		{
//			/*
//			dbgTrace(DBG_LVL_INFO,
//						       "TRM_STRG - tracking service not selected." );
//			*/
//		}
//	}
//	else
//	{
//		dbgTrace(DBG_LVL_INFO,
//				"TRM_STRG - Failure to get Tracking Transaction Record.");
//	}
//
//	/*
//	dbgTrace(DBG_LVL_INFO,
//		       "TRM_STRG - generate_tracking_service_xml <" );
//	*/

	return retVal;
}

//error_t trmSetPrinterTimeMargin(const mp_id_t mp_id, int printer_id, uint16_t	margin)
//{
//	// TODO: printer id is not used for now. Once TR schema is updated to report
//	// time margin along with the printer ID, we need update here.
//
//    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START
//
//	p_trm_mem->trans_rec[idx].printer_time_margin = margin;
//    p_trm_mem->trans_rec[idx].flags.is_populated.printer_time_margin = true;
//
//	dbgTrace(DBG_LVL_INFO,
//	   "TRM_STRG - SetPrinterTimeMargin mp_id=%d. Printer time margin =%d."
//	   , mp_id, p_trm_mem->trans_rec[idx].printer_time_margin);
//
//    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
//}

//error_t trmSetTar1Length(const mp_id_t mp_id, const uint16_t tar1Length)
//{
//    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START
//
//    p_trm_mem->trans_rec[idx].tar1Length = tar1Length;
//    p_trm_mem->trans_rec[idx].flags.is_populated.tar1_length = true;
//
//    dbgTrace(DBG_LVL_INFO,
//       "TRM_STRG - SetTar1Length mp_id=%d. tar1 length=%d"
//       , mp_id, tar1Length);
//
//    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
//}

error_t trmSetEngineerData(const mp_id_t mp_id, char *engIDString, uint16_t engData)
{
//    if(p_trm_mem->trans_rec[get_mp_idx(mp_id)].engData.num>MAX_ENG_DATA_NUM-1)
//    {
//        dbgTrace(DBG_LVL_INFO,
//           "TRM_STRG - SetEngineerData reached max engineer data number");
//        return E_BUFFER_SIZE_INSUFFICIENT;
//    }

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_START

//    engineer_item *engItem = &p_trm_mem->trans_rec[idx].engData.engItem[p_trm_mem->trans_rec[idx].engData.num];
//    memset(engItem, 0x0, sizeof(engineer_item));
//    strncpy(engItem->engID, engIDString, sizeof(engItem->engID));
//    engItem->engData = engData;
//
//    p_trm_mem->trans_rec[idx].engData.num++;
    dbgTrace(DBG_LVL_INFO,
       "TRM_STRG - SetEngineerData mp_id=%d. engineer ID string=%s, engineer data=%u"
       , mp_id, engIDString, engData);

    TR_REC_LOCK_VERIFY_POPULATING_AND_MPID_END
}

void encode_xml_entity_chars (char * out_buff, uint32_t buff_size, uint32_t *out_len,
											const char * in_str, uint32_t in_len)
{
//	if (HasXmlSpecialChar((XML_Char *)in_str, in_len) == true)
//	{
//		memset (out_buff, 0, buff_size);
//		EncodeXmlEntityReference((XML_Char *)out_buff, out_len, buff_size,
//												(XML_Char *)in_str, in_len);
//	}
//	else
//	{
//		memset (out_buff, 0x0, buff_size);
//		memcpy(out_buff, in_str, in_len);
//	}

}





//
//STATUS PrepareTRFileName(char *fileName, int cnt, BOOL compressed)
//{
//    DATETIME        rDateTimeStamp;
//
//    GetSysDateTime(&rDateTimeStamp, ADDOFFSETS, GREGORIAN);
//
//    	sprintf(fileName, "TRC-%08x-%02d%02d%02d%02d-%02d%02d%02d.zip",
//    						cnt,
//                            rDateTimeStamp.bCentury,
//                            rDateTimeStamp.bYear,
//                            rDateTimeStamp.bMonth,
//                            rDateTimeStamp.bDay,
//							rDateTimeStamp.bHour,
//							rDateTimeStamp.bMinutes,
//							rDateTimeStamp.bSeconds
//                           );
//
//
//    return SUCCESS;
//}

//int trmGetTRFileName(char *fileName, BOOL compressed)
//{
//	if(PrepareTRFileName(fileName, CMOSTrmFlashFileSeqCount, compressed) != SUCCESS)
//		return -1;
//
//	return 0;
//}


int trmGetTRFileName(char *fileName, BOOL compressed)
{
	DATETIME        rDateTimeStamp;

	GetSysDateTime(&rDateTimeStamp, ADDOFFSETS, GREGORIAN);

	sprintf(fileName, "TRD-%08x-%02d%02d%02d%02d-%02d%02d%02d",
							CMOSTrmFlashFileSeqCount,
							rDateTimeStamp.bCentury,
							rDateTimeStamp.bYear,
							rDateTimeStamp.bMonth,
							rDateTimeStamp.bDay,
							rDateTimeStamp.bHour,
							rDateTimeStamp.bMinutes,
							rDateTimeStamp.bSeconds
						   );

	return 0;
}

int trmGetTRFileNameEx(char *fileName)
{
    DATETIME        rDateTimeStamp;

    GetSysDateTime(&rDateTimeStamp, ADDOFFSETS, GREGORIAN);


    sprintf(fileName, "TZ-%08x-%02d%02d%02d%02d-%02d%02d%02d",
    						CMOSTrmFlashFileSeqCount,
                            rDateTimeStamp.bCentury,
                            rDateTimeStamp.bYear,
                            rDateTimeStamp.bMonth,
                            rDateTimeStamp.bDay,
							rDateTimeStamp.bHour,
							rDateTimeStamp.bMinutes,
							rDateTimeStamp.bSeconds
                           );


	return 0;
}

//Update the state of Tx file after creating, preparing for uplaod , delete etc.
STATUS trmUpdateTRFileState(int fileID, uint8_t state)
{
	if(fileID > MAX_FILE_LIST_EX)
		return 1;

	CMOSTrmFileList.trm_file_list[fileID].file_state = state;

	return 0;
}


void fnTRMAscii2Unicode(char *pSource, unsigned short *pDest, unsigned short bLen)
{
	unsigned long	i;

	for (i = 0; i < bLen; i++)
		*(pDest + i) = (unsigned short) *((unsigned char *) pSource + i);

	return;
}



//TRM Flash Files
VOID fnFilePath(CHAR* outPath, CHAR* path, CHAR* fileName);
BOOL fnFolderExists(CHAR *folderName);


//static char trfilename[32];
//static char path[64];


//error_t trmReadTxFileInfo(char *path, char * trxFileName, TXREC_FILE_INFO *pTxFileInfo)
//{
//	STATUS status = SUCCESS;
//	//uint32_t sourceLen = 0;
//    DATETIME SysDateTimePtr;
//    uint32_t crc = 0;
//
//	do
//	{
//	    GetSysDateTime(&SysDateTimePtr, ADDOFFSETS, GREGORIAN);
//		dbgTrace(DBG_LVL_INFO, "trmReadTxFileInfo - Before read: %02d%02d%02d%02d-%02d%02d%02d \n",
//						SysDateTimePtr.bCentury,
//						SysDateTimePtr.bYear,
//						SysDateTimePtr.bMonth,
//						SysDateTimePtr.bDay,
//						SysDateTimePtr.bHour,
//						SysDateTimePtr.bMinutes,
//						SysDateTimePtr.bSeconds);
//
//
//		status  = fnFileRead(path, trxFileName, (CHAR*)pTxFileInfo, 0, sizeof(TXREC_FILE_INFO));
//		if(status == NU_SUCCESS)
//		{
//			//validate CRC is on Info struct only
//			crc = crc32(0, (Byte*)pTxFileInfo, offsetof(TXREC_FILE_INFO, crc32));
//			if(crc != pTxFileInfo->crc32)
//			{
//				dbgTrace(DBG_LVL_ERROR, "trmReadTxFileInfo - CRCError: Expected: %08u: calculated %08u \n", pTxFileInfo->crc32, crc);
//				return 1;
//			}
//
//		}
//
//		GetSysDateTime(&SysDateTimePtr, ADDOFFSETS, GREGORIAN);
//		dbgTrace(DBG_LVL_INFO, "trmReadTxFileInfo - After read: %02d%02d%02d%02d-%02d%02d%02d \n",
//						SysDateTimePtr.bCentury,
//						SysDateTimePtr.bYear,
//						SysDateTimePtr.bMonth,
//						SysDateTimePtr.bDay,
//						SysDateTimePtr.bHour,
//						SysDateTimePtr.bMinutes,
//						SysDateTimePtr.bSeconds);
//
//	}while(0);
//
//
//	return status;
//}




error_t trmWriteTrmBucket(int bucket)
{
	STATUS status = SUCCESS;
	STATUS status2 = SUCCESS;
	STATUS nuStatus = NU_SUCCESS;
	static BOOL bInUse = false;
	char path[64];
	trm_mem_t *p_trm_mem = get_trm_bucket(bucket);
	int32_t sourceLen = 0;
	int start_idx, end_idx;
    uint32_t start_pc, end_pc;
    DATETIME SysDateTimePtr;
    TXREC_FILE_INFO_EX txFileInfo;
	uint32_t bufLen = 0;
	uint8_t * buffer = NULL;
	uint8_t *pCompData;
	uint32_t compDataLen = 0;
	int cnt;
	int fd = 0;
	int readConfirmationCount = 0;

	char trfilename[32];
	int bytes_wr = 0;

	if(bInUse == false)
	{
		bInUse = true;
		start_pc = 0;
		end_pc = 0;
		start_idx = 0;
		end_idx = 0;

		do
		{
			dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucket - bucket:%d ", bucket);

			fnFilePath(path, "trs", NULL);

			if(!fnFolderExists(path))
			{
				status = NU_Make_Dir(path);
				if(status != 0)
				{
					fnCheckFileSysCorruption(status);
					break;
				}
			}

			//Check for No empty
			//Clean FRAM buckeet and release all locks
			if(NU_SUCCESS == trmObtainHeaderAccess(bucket) )
			{
				//Prepare  file header with start and end pc
				for(cnt = 0; cnt < CFG_NUM_RECORDS; cnt++)
				{
					if((p_trm_mem->trans_rec[cnt].mp_id != 0) && (start_idx == 0) )
					{
						start_idx = p_trm_mem->trans_rec[cnt].mp_id;
						end_idx = p_trm_mem->trans_rec[cnt].mp_id;
					}

					if(p_trm_mem->trans_rec[cnt].mp_id > end_idx)
						end_idx = p_trm_mem->trans_rec[cnt].mp_id;


					if((p_trm_mem->trans_rec[cnt].pc != 0) && (start_pc == 0) )
					{
						start_pc = p_trm_mem->trans_rec[cnt].pc;
						end_pc = p_trm_mem->trans_rec[cnt].pc;
					}

					if(p_trm_mem->trans_rec[cnt].pc > end_pc)
						end_pc = p_trm_mem->trans_rec[cnt].pc;


				}

				//if start and end pc are zero there is nothing to write.
				if(p_trm_mem->header.num_records_in_use == 0)
				{
					//Check for pending write flag, clear if necessary
					if(trmGetState() == TRM_STATE_PENDING_TX_FILE_SAVE)
						trmSetState(TRM_NONE);

					// Ensure semaphore is release in this code path...
					if((status = trmReleaseHeaderAccess(bucket)) != NU_SUCCESS)
					{
						dbgTrace(DBG_LVL_INFO, "ERROR trmWriteTrmBucket unable to release semaphore for bucket %d [%d]", bucket, status);
					}

					dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucket- Nothing to write: bucket:%d, records in use [%d]", bucket, p_trm_mem->header.num_records_in_use);
					status = SUCCESS;
					break;
				}


				sourceLen = sizeof(trm_mem_t);

				if(sourceLen > 0)
				{
					//sourceLen = ((uint8_t*)&p_trm_mem->trans_rec[CFG_NUM_RECORDS-1] - (uint8_t*)p_trm_mem) + sizeof(trans_rec_t);
					bufLen = sourceLen + sizeof(txFileInfo) + 500;
					status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &buffer, bufLen, NU_NO_SUSPEND);
					if(buffer != NULL)
					{
						GetSysDateTime(&SysDateTimePtr, ADDOFFSETS, GREGORIAN);

						pCompData = buffer + sizeof(txFileInfo);
						compDataLen = sourceLen + 500;
						status = compress (pCompData, &compDataLen, (CHAR*)p_trm_mem, sourceLen);
						if(status == SUCCESS)
						{
							do{
								status = SUCCESS;
								trmGetTRFileName(trfilename, true);
								fnFilePath(path, "trs", trfilename);
								dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucket FileName:%s ", trfilename);
								GetSysDateTime(&SysDateTimePtr, ADDOFFSETS, GREGORIAN);

		//						dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucket - BeforeSave : %02d%02d%02d%02d-%02d%02d%02d \n",
		//										SysDateTimePtr.bCentury,
		//										SysDateTimePtr.bYear,
		//										SysDateTimePtr.bMonth,
		//										SysDateTimePtr.bDay,
		//										SysDateTimePtr.bHour,
		//										SysDateTimePtr.bMinutes,
		//										SysDateTimePtr.bSeconds);
								txFileInfo.signature = 0x31435254; //0x54524331
								txFileInfo.start_id = start_idx;
								txFileInfo.end_id = end_idx;
								txFileInfo.start_pc = start_pc;
								txFileInfo.end_pc = end_pc;
								txFileInfo.uncompress_len = sourceLen;
								txFileInfo.compress_len = compDataLen;
								memcpy(&txFileInfo.dateTime, &SysDateTimePtr, sizeof(SysDateTimePtr));
								txFileInfo.crc32 = crc32(0, (Byte*)&txFileInfo, offsetof(TXREC_FILE_INFO_EX, crc32));
								txFileInfo.crc32_uncompressed_data = crc32(0, (Byte*)p_trm_mem, sourceLen);

								memcpy(buffer, &txFileInfo, sizeof(txFileInfo));
								dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucket - length %d ", sizeof(txFileInfo) + compDataLen);
								//status = fsAppend(path, (CHAR*)buffer, sizeof(txFileInfo) + compDataLen);

								fd = NU_Open(path,(PO_TEXT|PO_RDWR|PO_CREAT|PO_TRUNC), PS_IWRITE);
								if (fd < 0)
								{
									dbgTrace(DBG_LVL_ERROR, "Error: trmWriteTrmBucket - Failed to open file info: %s, status:%d \n", path, fd);
									status = fd;
									fnCheckFileSysCorruption(status);
								}
								else
								{
									//Read file info block
									bytes_wr = NU_Write(fd, (CHAR*)buffer, sizeof(txFileInfo) + compDataLen);
									if(bytes_wr != (sizeof(txFileInfo) + compDataLen) )
									{
										CMOSTrmFailedWrites++;
										dbgTrace(DBG_LVL_ERROR, "Error: trmWriteTrmBucket - Failed to write file -written:%d, expected:%d \n", bytes_wr, (sizeof(txFileInfo) + compDataLen));
										status = bytes_wr;
										fnCheckFileSysCorruption(status);
									}

									status2 = NU_Close(fd);
									if(status2 != SUCCESS)
									{
										fnCheckFileSysCorruption(status2);
										dbgTrace(DBG_LVL_ERROR, "Error: trmWriteTrmBucket - Close failed %d\n", status2);
									}

									if(status == SUCCESS) status = status2;

									////////////////
									//Read and confirm that write is successful
									if(status == SUCCESS)
									{
										fd = NU_Open(path,(PO_TEXT|PO_RDONLY), PS_IREAD);
										if (fd < 0)
										{
											dbgTrace(DBG_LVL_ERROR, "Error: trmWriteTrmBucket - Failed to open file info: %s, status:%d \n", path, fd);
											status = fd;
											fnCheckFileSysCorruption(status);
										}
										else
										{
											UINT8* bufferRead = NULL;
											INT32 bytes_rd = 0;
											status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &bufferRead, sizeof(txFileInfo) + compDataLen, NU_NO_SUSPEND);
											if(status == SUCCESS)
											{

												bytes_rd = NU_Read(fd, (CHAR*)bufferRead, sizeof(txFileInfo) + compDataLen);
												if(bytes_rd != (sizeof(txFileInfo) + compDataLen) )
												{
													CMOSTrmFailedReads++;
													dbgTrace(DBG_LVL_ERROR, "Error: trmWriteTrmBucket - Failed to read file -read:%d, expected:%d \n", bytes_rd, (sizeof(txFileInfo) + compDataLen));
													status = bytes_wr;
													fnCheckFileSysCorruption(status);
												}

												status2 = NU_Close(fd);
												if(status2 != SUCCESS)
												{
													fnCheckFileSysCorruption(status2);
													dbgTrace(DBG_LVL_ERROR, "Error: trmWriteTrmBucket - Close failed %d\n", status2);
												}

												if(status == SUCCESS) status = status2;

												if((status = memcmp(bufferRead, buffer, sizeof(txFileInfo) + compDataLen)) != 0)
												{
													CMOSTrmFailedReads++; //TBD: we neede another counter for this
													dbgTrace(DBG_LVL_ERROR, "Error: trmWriteTrmBucket - Write Read confirmation failed status:%d\n", status);

													//delete the file
													if((nuStatus = NU_Delete(path)) == NU_SUCCESS)
													{
														dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucket - Write,Read,Delete confirmation successful \n");
													}
													else
													{
														fnCheckFileSysCorruption(nuStatus);
														CMOSTrmFailedDeletes++;
														dbgTrace(DBG_LVL_ERROR, "Error: trmWriteTrmBucket - Delete failed File:%s\n", path );
													}
												}
												else
												{
													dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucket - Write Read confirmation successful \n");
												}

												if(status == SUCCESS) status = status2;
											}

											if(bufferRead != NULL)
												NU_Deallocate_Memory(bufferRead);
										}

									}

								}
								readConfirmationCount++;

							}while((status != SUCCESS) && (readConfirmationCount <= 2));

							if(status == SUCCESS)
							{
								CMOSTrmFlashFileSeqCount++;

	//							GetSysDateTime(&SysDateTimePtr, ADDOFFSETS, GREGORIAN);
	//							dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucket - AfterSave: %02d%02d%02d%02d-%02d%02d%02d \n",
	//											SysDateTimePtr.bCentury,
	//											SysDateTimePtr.bYear,
	//											SysDateTimePtr.bMonth,
	//											SysDateTimePtr.bDay,
	//											SysDateTimePtr.bHour,
	//											SysDateTimePtr.bMinutes,
	//											SysDateTimePtr.bSeconds);

								//Clear header
								p_trm_mem->header.num_records_in_use = 0;

								dbgTrace(DBG_LVL_INFO, "Releasing Records : ");
								for(cnt = 0; cnt < CFG_NUM_RECORDS; cnt++)
								{
									if (NU_SUCCESS == trmObtainRecordAccess(bucket, cnt))
									{
										memset(&p_trm_mem->trans_rec[cnt], 0, sizeof(trans_rec_t ));
										memset(&p_trm_mem->trans_rec[cnt].flags.is_populated, 0, sizeof(p_trm_mem->trans_rec[cnt].flags.is_populated));
										if(NU_SUCCESS != trmReleaseRecordAccess(bucket, cnt))
										{
											status = cnt+1;
											dbgTrace(DBG_LVL_ERROR, "Error: Releasing Records failed- bucket :%d, record:%d ", bucket, cnt);
										}

									}
									else
									{
										status = 101;
										dbgTrace(DBG_LVL_ERROR, "Error: Obtaining Records failed- bucket :%d, record:%d ", bucket, cnt);
									}
								}

								//Clear pending file write
								if(status == SUCCESS)
								{
									if(trmGetState() == TRM_STATE_PENDING_TX_FILE_SAVE)
										trmSetState(TRM_NONE);
								}

							}
							else
							{
								//TODO: How do we handle this? attempt another write then fail...if it fails multiple times then disable the printer
								if(sSystemStatus.fRunning == TRUE)
									StopMailRunAndSendBaseEvent(BASE_EVENT_PENDING_FLASH_WRITE);

								status = 102;
								dbgTrace(DBG_LVL_ERROR, "Error: trmWriteTrmBucket - Failed to write File\n");
							}
						}
						else
						{
							status = 103;
							dbgTrace(DBG_LVL_ERROR, "Error: trmWriteTrmBucket - Failed to compress Buffer %d \n", sourceLen);
						}
					}
					else
					{
						status = 104;
						dbgTrace(DBG_LVL_ERROR, "Error: trmWriteTrmBucket - Failed to Allocate Buffer %d \n", bufLen);
					}
				}

				//release header
				if(trmReleaseHeaderAccess(bucket) == SUCCESS)
					dbgTrace(DBG_LVL_ERROR, "Releasing header-bucket:%d ", bucket);
				else
				{
					status = 105;
					dbgTrace(DBG_LVL_ERROR, "Error: Releasing header failed -bucket:%d \n", bucket);
				}
			}
			else
			{
				status = 106;
				dbgTrace(DBG_LVL_ERROR, "Error: trmObtainHeaderAccess failed -bucket:%d \n", bucket);
			}
		}while(0);

		if(status == SUCCESS)
		{
			memset(&p_trm_mem->trans_rec[0], 0, (sizeof(trans_rec_t )*CFG_NUM_RECORDS ));
			memset(&p_trm_mem->header.flags.is_populated, 0, sizeof(p_trm_mem->header.flags.is_populated));
		}
		else
		{
			dbgTrace(DBG_LVL_ERROR, "Error: trmWriteTrmBucket failed -bucket:%d , status%d \n, %s", bucket, status, trfilename);
		}

		dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucket: ");
		CMOSTrmPendingBucketToUpload = -1;

		bInUse = false;
	}
	else
	{
		status = 110;
		dbgTrace(DBG_LVL_ERROR, "Error: trmWriteTrmBucket: In Use:%d", bucket);

	}

	if(buffer != NULL)
		NU_Deallocate_Memory(buffer);

	return status;
}

//int compress_overhead(void);
//
//
//error_t trmWriteTrmBucketWithHeader(int bucket)
//{
//	STATUS status = SUCCESS;
//	static BOOL bInUse = false;
//	char path[64];
//	trm_mem_t *p_trm_mem = get_trm_bucket(bucket);
//	int32_t sourceLen = 0;
//	int start_idx, end_idx;
//    uint32_t start_pc, end_pc;
//    DATETIME SysDateTimePtr;
//    TXREC_FILE_INFO txFileInfo;
//	uint32_t bufLen = 0;
//	uint8_t * buffer = NULL;
//	uint8_t *pCompData;
//	uint32_t compDataLen = 0;
//	int cnt;
//	int fileCount = 0;
//    char trfilename[64];
//    char cmpfilename[64];
//    int writelength = 0;
//    INT fd;
//
//	if(bInUse == false)
//	{
//		bInUse = true;
//		start_pc = 0;
//		end_pc = 0;
//		start_idx = 0;
//		end_idx = 0;
//
//		do
//		{
//			dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucketWithHeader - bucket:%d ", bucket);
//
//			fnFilePath(path, "trs", NULL);
//
//			if(!fnFolderExists(path))
//			{
//				status = NU_Make_Dir(path);
//				if(status != 0)
//					break;
//			}
//
//			//start_idx = 0;
//			//end_idx =  p_trm_mem->header.num_records_in_use - 1;
//			//end_idx =  get_mp_idx(get_cur_mp_id());
//
//			//Check for No empty
//			//Clean FRAM buckeet and release all locks
//			if(NU_SUCCESS == trmObtainHeaderAccess(bucket) )
//			{
////				//Prepare  file header with start and end pc
////				for(cnt = 0; cnt < CFG_NUM_RECORDS; cnt++)
////				{
////					if((p_trm_mem->trans_rec[cnt].mp_id != 0) && (start_idx == 0) )
////					{
////						start_idx = p_trm_mem->trans_rec[cnt].mp_id;
////						end_idx = p_trm_mem->trans_rec[cnt].mp_id;
////					}
////
////					if(p_trm_mem->trans_rec[cnt].mp_id > end_idx)
////						end_idx = p_trm_mem->trans_rec[cnt].mp_id;
////
////
////					if((p_trm_mem->trans_rec[cnt].pc != 0) && (start_pc == 0) )
////					{
////						start_pc = p_trm_mem->trans_rec[cnt].pc;
////						end_pc = p_trm_mem->trans_rec[cnt].pc;
////					}
////
////					if(p_trm_mem->trans_rec[cnt].pc > end_pc)
////						end_pc = p_trm_mem->trans_rec[cnt].pc;
////
////
////				}
////				start_pc = p_trm_mem->trans_rec[start_idx].pc;
////				end_pc = p_trm_mem->trans_rec[end_idx].pc;
//
//				//if start and end pc are zero there is nothing to write.
//				if(p_trm_mem->header.num_records_in_use == 0)
//				{
//					//Check for pending write flag, clear if necessary
//					if(trmGetState() == TRM_STATE_PENDING_TX_FILE_SAVE)
//						trmSetState(TRM_NONE);
//
//					// Ensure semaphore is release in this code path...
//					if((status = trmReleaseHeaderAccess(bucket)) != NU_SUCCESS)
//					{
//						dbgTrace(DBG_LVL_INFO, "ERROR trmWriteTrmBucketWithHeader unable to release semaphore for bucket %d [%d]", bucket, status);
//					}
//
//					dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucketWithHeader- Nothing to write: bucket:%d, records in use [%d]", bucket, p_trm_mem->header.num_records_in_use);
//					status = SUCCESS;
//					break;
//				}
//
//				//Get filename with extension .zip
//				memset(trfilename, 0, sizeof(trfilename));
//				memset(cmpfilename, 0, sizeof(cmpfilename));
//				trmGetTRFileNameEx(trfilename);
//
//				strcpy(cmpfilename, trfilename);
//				strcat(cmpfilename, ".zip");
//				strcat(trfilename, ".dat");
//
//				fnFilePath(path, "trs", cmpfilename);
//				dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucketWithHeader FileName:%s ", cmpfilename);
//				sourceLen = sizeof(trm_mem_t);
//
//				if(sourceLen > 0)
//				{
//					//sourceLen = ((uint8_t*)&p_trm_mem->trans_rec[CFG_NUM_RECORDS-1] - (uint8_t*)p_trm_mem) + sizeof(trans_rec_t);
//					bufLen = sourceLen + sizeof(txFileInfo) + compress_overhead();
//					status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &buffer, bufLen, NU_NO_SUSPEND);
//					if(buffer != NULL)
//					{
//						GetSysDateTime(&SysDateTimePtr, ADDOFFSETS, GREGORIAN);
//
//						pCompData = buffer;
//						compDataLen = bufLen;
//						//status = compress (pCompData, &compDataLen, (CHAR*)p_trm_mem, sourceLen);
//						status = compressWithHeaderEx (pCompData, &compDataLen, (CHAR*)p_trm_mem, sourceLen, trfilename, strlen(trfilename), (char*)cmpfilename );
//						if(status == SUCCESS)
//						{
//
//	//						GetSysDateTime(&SysDateTimePtr, ADDOFFSETS, GREGORIAN);
//	//						dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucketWithHeader - BeforeSave : %02d%02d%02d%02d-%02d%02d%02d \n",
//	//										SysDateTimePtr.bCentury,
//	//										SysDateTimePtr.bYear,
//	//										SysDateTimePtr.bMonth,
//	//										SysDateTimePtr.bDay,
//	//										SysDateTimePtr.bHour,
//	//										SysDateTimePtr.bMinutes,
//	//										SysDateTimePtr.bSeconds);
//
////							txFileInfo.start_id = start_idx;
////							txFileInfo.end_id = end_idx;
////							txFileInfo.start_pc = start_pc;
////							txFileInfo.end_pc = end_pc;
////							txFileInfo.uncompress_len = sourceLen;
////							txFileInfo.compress_len = compDataLen;
////							txFileInfo.crc32 = crc32(0, (Byte*)&txFileInfo, offsetof(TXREC_FILE_INFO, crc32));
////							txFileInfo.crc32_uncompressed_data = crc32(0, (Byte*)p_trm_mem, sourceLen);
//
//							//memcpy(buffer, &txFileInfo, sizeof(txFileInfo));
//							//dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucketWithHeader - fsAppend Append Length %d ", sizeof(txFileInfo) + compDataLen);
//							//status = fsAppend(path, (CHAR*)buffer, sizeof(txFileInfo) + compDataLen);
//
//							if((pCompData != NULL) && (compDataLen != 0) )
//							{
//
//								fd = NU_Open(path, (PO_TEXT|PO_RDWR|PO_CREAT|PO_TRUNC), (PS_IWRITE | PS_IREAD));
//								if(fd < 0)
//								{
//									dbgTrace(DBG_LVL_ERROR, "trmWriteTrmBucketWithHeader: Failed to open xml file %s \r\n", path);
//									status = fd;
//								}
//								else
//								{
//									writelength = NU_Write(fd, (void*)pCompData, compDataLen);
//									if ((writelength < 0) || (writelength != compDataLen))
//									{
//										dbgTrace(DBG_LVL_ERROR, "trmWriteTrmBucketWithHeader: Failed to write XML file: %s, write length: %0x / %0x \r\n", path, writelength, compDataLen);
//										status = writelength;
//									}
//									else
//										dbgTrace(DBG_LVL_ERROR, "trmWriteTrmBucketWithHeader: Write Success: %s, write length: %0x / %0x \r\n", path, writelength, compDataLen);
//								}
//
//								NU_Flush(fd);
//								NU_Close(fd);
//							}
//							else
//							{
//								dbgTrace(DBG_LVL_ERROR, "trmWriteTrmBucketWithHeader: Invalid comp data length: %s, writelength: %0x \r\n", path, compDataLen);
//								status = -100;
//							}
//
//							if(status == SUCCESS)
//							{
//								CMOSTrmFlashFileSeqCount++;
//
//	//							GetSysDateTime(&SysDateTimePtr, ADDOFFSETS, GREGORIAN);
//	//							dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucketWithHeader - AfterSave: %02d%02d%02d%02d-%02d%02d%02d \n",
//	//											SysDateTimePtr.bCentury,
//	//											SysDateTimePtr.bYear,
//	//											SysDateTimePtr.bMonth,
//	//											SysDateTimePtr.bDay,
//	//											SysDateTimePtr.bHour,
//	//											SysDateTimePtr.bMinutes,
//	//											SysDateTimePtr.bSeconds);
//
//								//Clear header
//								p_trm_mem->header.num_records_in_use = 0;
//
//								dbgTrace(DBG_LVL_INFO, "Releasing Records : ");
//								for(cnt = 0; cnt < CFG_NUM_RECORDS; cnt++)
//								{
//									if (NU_SUCCESS == trmObtainRecordAccess(bucket, cnt))
//									{
//										memset(&p_trm_mem->trans_rec[cnt], 0, sizeof(trans_rec_t ));
//										memset(&p_trm_mem->trans_rec[cnt].flags.is_populated, 0, sizeof(p_trm_mem->trans_rec[cnt].flags.is_populated));
//										if(NU_SUCCESS != trmReleaseRecordAccess(bucket, cnt))
//										{
//											status = cnt+1;
//											dbgTrace(DBG_LVL_INFO, "Releasing Records failed- bucket :%d, record:%d ", bucket, cnt);
//										}
//
//									}
//									else
//									{
//										status = 101;
//										dbgTrace(DBG_LVL_INFO, "Obtaining Records failed- bucket :%d, record:%d ", bucket, cnt);
//									}
//								}
//
//								//memset(&p_trm_mem->trans_rec[0], 0, (sizeof(trans_rec_t )*CFG_NUM_RECORDS ));
//								//memset(&p_trm_mem->header.flags.is_populated, 0, sizeof(p_trm_mem->header.flags.is_populated));
//
//								//Clear pending file write
//								if(status == SUCCESS)
//								{
//									if(trmGetState() == TRM_STATE_PENDING_TX_FILE_SAVE)
//										trmSetState(TRM_NONE);
//								}
//
//							}
//							else
//							{
//								//TODO: How do we handle this? attempt another write then fail...if it fails multiple times then disable the printer
//								if(sSystemStatus.fRunning == TRUE)
//									StopMailRunAndSendBaseEvent(BASE_EVENT_PENDING_FLASH_WRITE);
//
//								status = 102;
//								dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucketWithHeader - Failed to write File Information \n");
//							}
//						}
//						else
//						{
//							status = 103;
//							dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucketWithHeader - Failed to compress Buffer %d \n", sourceLen);
//						}
//					}
//					else
//					{
//						status = 104;
//						dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucketWithHeader - Failed to Allocate Buffer %d \n", bufLen);
//					}
//				}
//
//				//release header
//				if(trmReleaseHeaderAccess(bucket) == SUCCESS)
//					dbgTrace(DBG_LVL_INFO, "Releasing header-bucket:%d ", bucket);
//				else
//				{
//					status = 105;
//					dbgTrace(DBG_LVL_INFO, "Releasing header failed -bucket:%d \n", bucket);
//				}
//			}
//			else
//			{
//				status = 106;
//				dbgTrace(DBG_LVL_INFO, "trmObtainHeaderAccess failed -bucket:%d \n", bucket);
//			}
//		}while(0);
//
//		if(status == SUCCESS)
//		{
//			memset(&p_trm_mem->trans_rec[0], 0, (sizeof(trans_rec_t )*CFG_NUM_RECORDS ));
//			memset(&p_trm_mem->header.flags.is_populated, 0, sizeof(p_trm_mem->header.flags.is_populated));
//		}
//		else
//		{
//			dbgTrace(DBG_LVL_INFO, "trmObtainHeaderAccess failed -bucket:%d , status%d \n, %s", bucket, status, trfilename);
//
//			//Remove file if there is an issue in creating/incomplete
//			fnFileDelete("trs", trfilename);
//		}
//
//		dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucketWithHeader: ");
//		CMOSTrmPendingBucketToUpload = -1;
//
//		bInUse = false;
//	}
//	else
//	{
//		status = 110;
//		dbgTrace(DBG_LVL_INFO, "trmWriteTrmBucketWithHeader: In Use:%d", bucket);
//
//	}
//
//	if(buffer != NULL)
//		NU_Deallocate_Memory(buffer);
//
//	return status;
//}


error_t trmReadTxFile(char* path, char * trxFileName, uint8* buffer, int *pLength, TXREC_FILE_INFO *pTxFileInfo)
{
	STATUS status = SUCCESS;
	STATUS nuStatus = NU_SUCCESS;
    DATETIME SysDateTimePtr;
    uint32_t crc = 0;
	uint8_t * compBuffer = NULL;
	CHAR 	fullpath[128] = {0};
	int 	fd = 0;
	int bytes_read = 0;


	do
	{
		//memset(&txFileInfo, 0, sizeof(txFileInfo));

	    GetSysDateTime(&SysDateTimePtr, ADDOFFSETS, GREGORIAN);
		dbgTrace(DBG_LVL_INFO, "trmReadTxFile - BeforeRead: %02d%02d%02d%02d-%02d%02d%02d ",
						SysDateTimePtr.bCentury,
						SysDateTimePtr.bYear,
						SysDateTimePtr.bMonth,
						SysDateTimePtr.bDay,
						SysDateTimePtr.bHour,
						SysDateTimePtr.bMinutes,
						SysDateTimePtr.bSeconds);


		//status = trmReadTxFileInfo(path, trxFileName, pTxFileInfo);
		fnFilePath(fullpath, path, trxFileName);
		fd = NU_Open(fullpath,(PO_TEXT|PO_RDONLY), PS_IREAD);
		if (fd < 0)
		{
			fnCheckFileSysCorruption(fd);
			dbgTrace(DBG_LVL_ERROR, "trmReadTxFile - Failed to open file info: %s, status:%d \n", fullpath, fd);
			status = fd;
			break;
		}
		else
		{
			//Read file info block
			bytes_read = NU_Read(fd, pTxFileInfo, sizeof(TXREC_FILE_INFO));
			if(bytes_read != sizeof(TXREC_FILE_INFO))
			{
				fnCheckFileSysCorruption(bytes_read);
				dbgTrace(DBG_LVL_ERROR, "trmReadTxFile - Failed to read file info read:%d, expected:%d \n", bytes_read, sizeof(TXREC_FILE_INFO));
				status = -200;
				break;
			}
			else
			{
				//Check CRC
				//validate CRC is on Info struct only
				crc = crc32(0, (Byte*)pTxFileInfo, offsetof(TXREC_FILE_INFO, crc32));
				if(crc != pTxFileInfo->crc32)
				{
					dbgTrace(DBG_LVL_ERROR, "trmReadTxFile - CRCError: Expected: %08u: calculated %08u \n", pTxFileInfo->crc32, crc);
					status = -201;
					break;
				}

				if(*pLength < pTxFileInfo->uncompress_len)
				{
					dbgTrace(DBG_LVL_ERROR, "trmReadTxFile - insufficient out buffer : Expected: %08u: available %08u ", pTxFileInfo->uncompress_len, *pLength);
					status = -202;
					break;
				}

				if(pTxFileInfo->compress_len != 0 && pTxFileInfo->compress_len < 65535)
				{
					status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &compBuffer, pTxFileInfo->compress_len + 64, NU_NO_SUSPEND);
					if(compBuffer != NULL)
					{
						//status  = fnFileRead(path,  trxFileName, (CHAR*)compBuffer, sizeof(txFileInfo), pTxFileInfo->compress_len );
						bytes_read = NU_Read(fd, compBuffer, pTxFileInfo->compress_len);
						if(bytes_read == pTxFileInfo->compress_len)
						{
							status = uncompress (buffer, pLength, compBuffer, pTxFileInfo->compress_len);
							if(status != SUCCESS)
							{
								dbgTrace(DBG_LVL_INFO, "trmReadTxFile - Failed to uncompress file %s ", trxFileName);
								status = -203;
								break;
							}
						}
						else
						{
							fnCheckFileSysCorruption(bytes_read);
							dbgTrace(DBG_LVL_INFO, "trmReadTxFile - Failed to read compressed file %s read:%d, expected:%d", trxFileName, bytes_read, pTxFileInfo->compress_len);
							status = -204;
							break;
						}

					}
					else
					{
						dbgTrace(DBG_LVL_INFO, "trmReadTxFile - Failed to allocate Buffer %d ", pTxFileInfo->compress_len);
						status = -205;
						break;
					}



				}
				else
				{
					//Read TRM area
					//status  = fnFileRead(path,  trxFileName, (CHAR*)buffer, sizeof(txFileInfo), pTxFileInfo->uncompress_len );
					dbgTrace(DBG_LVL_ERROR, "trmReadTxFile - Invalid Compressed length: compLength: %d: Max %d ", pTxFileInfo->compress_len, 65535);
					status = -210;
					break;
				}

				if(status == NU_SUCCESS)
				{

					//validate CRC is on Info struct only
					crc = crc32(0, (Byte*)buffer, pTxFileInfo->uncompress_len);
					if(crc != pTxFileInfo->crc32_uncompressed_data)
					{
						dbgTrace(DBG_LVL_ERROR, "trmReadTxFile - CRC uncompressed data Error: Expected: %08u: calculated %08u ",
								pTxFileInfo->crc32_uncompressed_data, crc);
						status = -220;
						break;
					}
					*pLength = pTxFileInfo->uncompress_len;

				}
				else
				{
					dbgTrace(DBG_LVL_INFO, "trmReadTxFile - Failed to read file %s, status: %d \n", trxFileName, status);
					status = -220;
					break;
				}
			}//read file info
		}

		GetSysDateTime(&SysDateTimePtr, ADDOFFSETS, GREGORIAN);
		dbgTrace(DBG_LVL_INFO, "trmReadTxFile - AfterRead: %02d%02d%02d%02d-%02d%02d%02d ",
						SysDateTimePtr.bCentury,
						SysDateTimePtr.bYear,
						SysDateTimePtr.bMonth,
						SysDateTimePtr.bDay,
						SysDateTimePtr.bHour,
						SysDateTimePtr.bMinutes,
						SysDateTimePtr.bSeconds);

	}while(0);

	if(fd >= 0)
	{
		if((nuStatus = NU_Close(fd)) != SUCCESS)
		{
			fnCheckFileSysCorruption(nuStatus);
			dbgTrace(DBG_LVL_INFO, "trmReadTxFile - Failed to close file:%s, status:%d \n", trxFileName, status);
		}
	}

	if(compBuffer != NULL)
		NU_Deallocate_Memory(compBuffer);

	return status;
}

error_t trmReadTxFileEx(char* path, char * trxFileName, uint8* buffer, int *pLength, TXREC_FILE_INFO_EX *pTxFileInfo)
{
	STATUS status = SUCCESS;
	STATUS nuStatus;
    DATETIME SysDateTimePtr;
    uint32_t crc = 0;
	uint8_t * compBuffer = NULL;
	CHAR 	fullpath[128] = {0};
	int 	fd = 0;
	int bytes_read = 0;


	do
	{
		//memset(&txFileInfo, 0, sizeof(txFileInfo));

	    GetSysDateTime(&SysDateTimePtr, ADDOFFSETS, GREGORIAN);
		dbgTrace(DBG_LVL_INFO, "trmReadTxFileEx - BeforeRead: %02d%02d%02d%02d-%02d%02d%02d ",
						SysDateTimePtr.bCentury,
						SysDateTimePtr.bYear,
						SysDateTimePtr.bMonth,
						SysDateTimePtr.bDay,
						SysDateTimePtr.bHour,
						SysDateTimePtr.bMinutes,
						SysDateTimePtr.bSeconds);


		//status = trmReadTxFileInfo(path, trxFileName, pTxFileInfo);
		fnFilePath(fullpath, path, trxFileName);
		fd = NU_Open(fullpath,(PO_TEXT|PO_RDONLY), PS_IREAD);
		if (fd < 0)
		{
			fnCheckFileSysCorruption(fd);
			dbgTrace(DBG_LVL_ERROR, "trmReadTxFileEx - Failed to open file info: %s, status:%d \n", fullpath, fd);
			status = fd;
			break;
		}
		else
		{
			//Read file info block
			bytes_read = NU_Read(fd, pTxFileInfo, sizeof(TXREC_FILE_INFO_EX));
			if(bytes_read != sizeof(TXREC_FILE_INFO_EX))
			{
				fnCheckFileSysCorruption(bytes_read);
				dbgTrace(DBG_LVL_ERROR, "trmReadTxFileEx - Failed to read file info read:%d, expected:%d \n", bytes_read, sizeof(TXREC_FILE_INFO_EX));
				status = -200;
				break;
			}
			else
			{
				//Check CRC
				//validate CRC is on Info struct only
				crc = crc32(0, (Byte*)pTxFileInfo, offsetof(TXREC_FILE_INFO_EX, crc32));
				if(crc != pTxFileInfo->crc32)
				{
					dbgTrace(DBG_LVL_ERROR, "trmReadTxFileEx - CRCError: Expected: %08u: calculated %08u \n", pTxFileInfo->crc32, crc);
					status = -201;
					break;
				}

				if(*pLength < pTxFileInfo->uncompress_len)
				{
					dbgTrace(DBG_LVL_ERROR, "trmReadTxFileEx - insufficient out buffer : Expected: %08u: available %08u ", pTxFileInfo->uncompress_len, *pLength);
					status = -202;
					break;
				}

				if(pTxFileInfo->compress_len != 0 && pTxFileInfo->compress_len < 65535)
				{
					status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &compBuffer, pTxFileInfo->compress_len + 64, NU_NO_SUSPEND);
					if(compBuffer != NULL)
					{
						//status  = fnFileRead(path,  trxFileName, (CHAR*)compBuffer, sizeof(txFileInfo), pTxFileInfo->compress_len );
						bytes_read = NU_Read(fd, compBuffer, pTxFileInfo->compress_len);
						if(bytes_read == pTxFileInfo->compress_len)
						{
							status = uncompress (buffer, pLength, compBuffer, pTxFileInfo->compress_len);
							if(status != SUCCESS)
							{
								dbgTrace(DBG_LVL_INFO, "trmReadTxFileEx - Failed to uncompress file %s ", trxFileName);
								status = -203;
								break;
							}
						}
						else
						{
							fnCheckFileSysCorruption(bytes_read);
							dbgTrace(DBG_LVL_INFO, "trmReadTxFileEx - Failed to read compressed file %s read:%d, expected:%d", trxFileName, bytes_read, pTxFileInfo->compress_len);
							status = -204;
							break;
						}

					}
					else
					{
						dbgTrace(DBG_LVL_INFO, "trmReadTxFileEx - Failed to allocate Buffer %d ", pTxFileInfo->compress_len);
						status = -205;
						break;
					}



				}
				else
				{
					//Read TRM area
					//status  = fnFileRead(path,  trxFileName, (CHAR*)buffer, sizeof(txFileInfo), pTxFileInfo->uncompress_len );
					dbgTrace(DBG_LVL_ERROR, "trmReadTxFileEx - Invalid Compressed length: compLength: %d: Max %d ", pTxFileInfo->compress_len, 65535);
					status = -210;
					break;
				}

				if(status == NU_SUCCESS)
				{

					//validate CRC is on Info struct only
					crc = crc32(0, (Byte*)buffer, pTxFileInfo->uncompress_len);
					if(crc != pTxFileInfo->crc32_uncompressed_data)
					{
						dbgTrace(DBG_LVL_ERROR, "trmReadTxFileEx - CRC uncompressed data Error: Expected: %08u: calculated %08u ",
								pTxFileInfo->crc32_uncompressed_data, crc);
						status = -220;
						break;
					}
					*pLength = pTxFileInfo->uncompress_len;

				}
				else
				{
					dbgTrace(DBG_LVL_INFO, "trmReadTxFileEx - Failed to read file %s, status: %d \n", trxFileName, status);
					status = -220;
					break;
				}
			}//read file info
		}

		GetSysDateTime(&SysDateTimePtr, ADDOFFSETS, GREGORIAN);
		dbgTrace(DBG_LVL_INFO, "trmReadTxFileEx - AfterRead: %02d%02d%02d%02d-%02d%02d%02d ",
						SysDateTimePtr.bCentury,
						SysDateTimePtr.bYear,
						SysDateTimePtr.bMonth,
						SysDateTimePtr.bDay,
						SysDateTimePtr.bHour,
						SysDateTimePtr.bMinutes,
						SysDateTimePtr.bSeconds);

	}while(0);

	if(fd >= 0)
	{
		if((nuStatus = NU_Close(fd)) != SUCCESS)
		{
			fnCheckFileSysCorruption(nuStatus);
			dbgTrace(DBG_LVL_INFO, "trmReadTxFileEx - Failed to close file:%s, status:%d \n", trxFileName, status);
		}
	}

	if(compBuffer != NULL)
		NU_Deallocate_Memory(compBuffer);

	return status;
}


error_t trmReadTxFileWithHeader(char* path, char * trxFileName, uint8* buffer, int *pLength)
{
	STATUS status = SUCCESS;
    DATETIME SysDateTimePtr;
//	uint8_t * compBuffer = NULL;
//	CHAR 	fullpath[128] = {0};
//	int 	fd = 0;



	do
	{

	    GetSysDateTime(&SysDateTimePtr, ADDOFFSETS, GREGORIAN);
		dbgTrace(DBG_LVL_INFO, "trmReadTxFileEx - BeforeRead: %02d%02d%02d%02d-%02d%02d%02d ",
						SysDateTimePtr.bCentury,
						SysDateTimePtr.bYear,
						SysDateTimePtr.bMonth,
						SysDateTimePtr.bDay,
						SysDateTimePtr.bHour,
						SysDateTimePtr.bMinutes,
						SysDateTimePtr.bSeconds);


		//status = trmReadTxFileExInfo(path, trxFileName, pTxFileInfo);
//		fnFilePath(fullpath, path, trxFileName);
//		fd = NU_Open(fullpath,(PO_TEXT|PO_RDONLY), PS_IREAD);
//		if (fd < 0)
//		{
//			dbgTrace(DBG_LVL_ERROR, "trmReadTxFileEx - Failed to open file info: %s, status:%d \n", fullpath, fd);
//			status = fd;
//			break;
//		}
//		else
//		{

			status = uncompressWithHeaderEx (buffer, pLength, "trs", trxFileName);
			if(status != SUCCESS)
			{
				dbgTrace(DBG_LVL_INFO, "trmReadTxFileEx - Failed to uncompress status %d ",status);
				status = -205;
				break;
			}
//		}

		GetSysDateTime(&SysDateTimePtr, ADDOFFSETS, GREGORIAN);
		dbgTrace(DBG_LVL_INFO, "trmReadTxFileEx - AfterRead: %02d%02d%02d%02d-%02d%02d%02d ",
						SysDateTimePtr.bCentury,
						SysDateTimePtr.bYear,
						SysDateTimePtr.bMonth,
						SysDateTimePtr.bDay,
						SysDateTimePtr.bHour,
						SysDateTimePtr.bMinutes,
						SysDateTimePtr.bSeconds);

	}while(0);

//	if(fd >= 0)
//	{
//		if( NU_Close(fd) != SUCCESS)
//		{
//			dbgTrace(DBG_LVL_INFO, "trmReadTxFileEx - Failed to close file:%s, status:%d \n", trxFileName, status);
//		}
//	}
//
//	if(compBuffer != NULL)
//		NU_Deallocate_Memory(compBuffer);

	return status;
}


uint8_t active_idx = 0;
active_mp_log_t active_mp_log[MAX_ACTIVE_MP];

void clear_active_mp_log(int index)
{
	if (index == -1)
	{
		int i;
		for (i=0; i<MAX_ACTIVE_MP; i++)
		{
			active_mp_log[i].mp_id = -1;
			active_mp_log[i].state = 0;
			active_mp_log[i].exited = 0;
			active_mp_log[i].recover = 0;
		}
		active_idx = 0;
	}
	else
	{
		active_mp_log[index].mp_id = -1;
		active_mp_log[index].state = 0;
		active_mp_log[index].exited = 0;
		active_mp_log[index].recover = 0;
	}
}

active_mp_log_t *get_active_mp(void)
{
	return &active_mp_log[active_idx];
}

void finish_mp(int index)
{
	active_mp_log[index].exited = 1;
}


STATUS InitTrm(void)
{
	STATUS status = NU_SUCCESS;
	int i;

    pParameters = &cmosDcapParameters;

	trmCreateSemaphore();

	trmMAXMailPiecesToUpload = MAX_MP_TO_UPLOAD;
	gTrmFatalError = 0;

	if(CMOSTrmMaxFilesToUpload < MAX_FILES_TO_UPLOAD)
		CMOSTrmMaxFilesToUpload = MAX_FILES_TO_UPLOAD_EX;

	for(i = 0; i < TRM_MAX_NUMBER_OF_FILES; i++)
	{
		trmFileList[i] = NULL;
	}

	//Initialize TRM in following cases
	if( ((CMOSTrmInitialzed1 != TRM_INIT_PATTERN ) && (CMOSTrmInitialzed1 != TRM_INIT_PATTERN2 ))||
		((CMOSTrmInitialzed2 != TRM_INIT_PATTERN ) && (CMOSTrmInitialzed2 != TRM_INIT_PATTERN2 ))
		|| 		(CMOSTrmVersion != TR_VERSION) )
	{
		InitTrmMem();

	}

	if( (CMOS2TrmInitialzed1 != TRM_INIT_PATTERN2 ) || (CMOS2TrmInitialzed2 != TRM_INIT_PATTERN2 ) )
	{
		//Initialize CMOS2
		memset(&CMOS2TrmInitialzed1, 0, ((void*)&CMOS2TrmInitialzed2 - (void*)&CMOS2TrmInitialzed1) + sizeof(CMOS2TrmInitialzed2));
		CMOS2TrmInitialzed1 = TRM_INIT_PATTERN2;
		CMOS2TrmInitialzed2 = TRM_INIT_PATTERN2;

	}

	active_mp_log[active_idx].mp_id = get_cur_mp_id();

    dbgTrace(DBG_LVL_INFO, "TRM - Transaction Record Manager initialized");

	//Check for pending upload confirmation.
	//This is to complete any pending cleanup after powerup
	if( CMOSTrmUploadState >= TRM_STATE_UPLOAD_CONFIRMED)
		(void) trmProcessTRUploadConfirmation();

    //TODO: Revisit this with powerfail ecovery
    //Complete any pending tx file writes if necessary may be due to power fail
    trmProcessTxPendingWrite();

    memset(&trmTimeStampOfLatestRec, 0, sizeof(trmTimeStampOfLatestRec));

    //Check any pending upload confirmation activity
//    status = trmProcessTRUploadConfirmation();
//    if(status != SUCCESS)
//    	dbgTrace(DBG_LVL_INFO, "InitTrm: Failed to complete cleanup \n");

    //Check for upload due or required
    trmSendMsgToBackground (CHECK_FOR_UPLOAD_DUE_REQUIRED, CMOSTrmActiveBucket, 0, 0);
    return status;
}


//TODO move these items to TRM
int GetDcapFileSeqCount(void)
{
	return CMOSTrmDcapFileSeqCount;
}

//void trmIncrementSeqCount(void)
void trmIncrementDcapFileSeqCount(void)
{
	CMOSTrmDcapFileSeqCount++;
}

//TODO: Some way to set these values externally
int GetScheme(void)
{
	return 2;
}

STATUS trmGeneratetDcapFileName(DATETIME *pEndDateTime, BOOL bNMR)
{
	//trmGenerateFileName();
	STATUS status = SUCCESS;
	CMOS_Signature *sig;
	PSD_INFO psdInfo;
	char trmFileName[TRM_MAX_FILE_NAME_SIZE];

	sig = fnCMOSSetupGetCMOSSignature();
	GetPSDInfo(&psdInfo);

	//BASE PCN(4) - BASE SN(7) - PSD PCN(4) - PSD SN(7) - DATE (8) - SEQNUM(3)- SCHEME(2) - NMR(OPTIONAL)
	snprintf(trmFileName, TRM_MAX_FILE_NAME_SIZE, "%04s-%07s-%04s-%07s-%02d%02d%02d%02d-%03d-%02d",
			sig->bUicPcn,
			sig->bUicSN,
			psdInfo.PCN,
			psdInfo.PBISerialNum,
			pEndDateTime->bCentury, pEndDateTime->bYear, pEndDateTime->bMonth, pEndDateTime->bDay,
			GetDcapFileSeqCount(),
			GetScheme());

	if(bNMR)
		strncat(trmFileName, "-NMR", sizeof(trmFileName));

	strncpy(CMOS2TrmDcapFileName, trmFileName, TRM_MAX_FILE_NAME_SIZE);

	return status;
}

STATUS trmGetDcapFileName(char* fileName, int *pSize, bool bZipExt)
{
	strcpy(fileName, CMOS2TrmDcapFileName);
	if(bZipExt)
		strcat(fileName, ".zip");
	else
		strcat(fileName, ".xml");

	*pSize = strlen(fileName);

	return SUCCESS;
}

void trmClearDcapFileName(void)
{
	memset(CMOS2TrmDcapFileName, 0 , TRM_MAX_FILE_NAME_SIZE);
}


static PRINT_MODE_MAPPING_TBL prntModeMappingTbl[] = {
	//xml					print mode	rate_mode				wt_mode			report_name
	{"KeyInPostage", 		"Indicia", 	"KeyInPostage",  		"", 			""   				},
	{"SealOnly", 			"SealOnly", "",  					"", 			""   				},
	{"Permit", 				"Permit", 	"",  					"", 			""   				},
	{"DateTimeStamp", 		"DateTime", "",  					"", 			""   				},
	{"ManualWeightEntry", 	"Indicia", "Weight",  				"Manual", 		""       			},
	{"ExternalScale", 		"Indicia", "Weight",  				"Scale", 		""       			},
	{"PostageCorrection", 	"Indicia", "PostageCorrection",  	"", 			""       			},
	{"PostageCorrection", 	"Indicia", "PostageCorrection",  	"Manual", 		""       			},
	{"PostageCorrection", 	"Indicia", "PostageCorrection",  	"Scale", 		""       			},
	{"DateCorrection", 		"Report",  "",  					"", 			"Date Correction"   },
	{"DiffWeightMode", 		"Indicia", "Weight",  				"Differential", ""       			},
	{"PrintheadReport" , 	"Report", 	"",  					"", 			"Funds Report"      },
	{"PrintheadReport" , 	"Report", 	"",  					"", 			"Refill Receipt"    },
	{"PrintheadReport" , 	"Report", 	"",  					"", 			"Refund Report"     },
	{"ModeAjout" , 			"ModeAjout","",  					"", 			""      			},
	{"AdOnly" , 			"AdOnly", 	"",  					"", 			""     				},
	//TODO: Confirm Test Print
	{"PrintheadReport" , 	"TestPrint","",  					"", 			""     				},
//	{"StatementOfMailing", 	"SOM", 		"",  					"", 			""       			},
//	{"Manual" , 			"Manual", 	"",  					"", 			""      			},
//	{"Shipping", 			"Shipping", "",  					"", 			""      			}
};


char* GetPrintMode(void)
{
	int count;
	char printMode[32] = "";
	char rateMode[32] = "";
	char wtMode[32] = "";
	char rprtName[32] = "";
	char *pXmlPrintMode = NULL;
	uint16_t length;
	uint32_t type;
	bool is_set;

	smgr_get_settings_info( SETTING_ITEM_PRINT_MODE, &length, &type, &is_set);
	if(is_set)
    	smgr_get_setting_data(SETTING_ITEM_PRINT_MODE, printMode, length);

	smgr_get_settings_info( SETTING_ITEM_RATING_MODE, &length, &type, &is_set);
	if(is_set)
    	smgr_get_setting_data(SETTING_ITEM_RATING_MODE, rateMode, length);

	smgr_get_settings_info( SETTING_ITEM_WEIGHT_METHOD, &length, &type, &is_set);
	if(is_set)
		smgr_get_setting_data(SETTING_ITEM_WEIGHT_METHOD, wtMode, length);

	smgr_get_settings_info( SETTING_ITEM_REPORT_NAME, &length, &type, &is_set);
	if(is_set)
		smgr_get_setting_data(SETTING_ITEM_REPORT_NAME, rprtName, length);

	for (count = 0; count < sizeof(prntModeMappingTbl)/sizeof(PRINT_MODE_MAPPING_TBL); count++)
	{
		if( (strcmp(printMode, prntModeMappingTbl[count].print_mode) == 0 ) &&
				(strcmp(rateMode, prntModeMappingTbl[count].rate_mode) == 0 ) &&
				(strcmp(wtMode, prntModeMappingTbl[count].wt_mode) == 0 ) &&
				(strcmp(rprtName, prntModeMappingTbl[count].report_name) == 0 ) )
		{
			pXmlPrintMode = prntModeMappingTbl[count].xml_print_mode;
		}
	}

	return pXmlPrintMode;
}

error_t trmUpdateTranRecWithSettingMgr(const mp_id_t mp_id)
{
	error_t status;
	int settings_idx;
	uint8_t buffer[sizeof(string_256)+4]; //last byte for null terminator
	uint16_t length;
	uint32_t type;
	bool is_set;

    int bucket = trmGetActiveBucket();
    trm_mem_t *p_trm_mem = get_trm_bucket(bucket);

	int idx = get_mp_idx(mp_id);

	for (settings_idx=0; settings_idx < NUM_OF_SETTING_ITEMS ; settings_idx++)
	{
		smgr_get_settings_info( settings_idx, &length, &type, &is_set);
		if (is_set)
		{
			memset(buffer, 0x0, sizeof(buffer)); // clear buffer before every read
			status = smgr_get_setting_data(settings_idx, buffer, length);
			if (status == NU_SUCCESS )
			{
				switch(settings_idx)
				{
				case SETTING_ITEM_PRINT_MODE:
				{
					char *pMode = GetPrintMode();
					if(pMode != NULL)
					{
						strncpy( p_trm_mem->trans_rec[idx].mode, (char*)pMode, sizeof(p_trm_mem->trans_rec[idx].mode));
					}
					else
					{
						strncpy( p_trm_mem->trans_rec[idx].mode, (char*)"InvalidPrintMode", sizeof(p_trm_mem->trans_rec[idx].mode));
					}
					dbgTrace(DBG_LVL_INFO, "trmUpdateTranRecWithSettingMgr -printmode:%s, count:%d ", p_trm_mem->trans_rec[idx].mode, idx);
					if(strncmp((char*)buffer,  "Indicia", 7 ) == 0)
						p_trm_mem->trans_rec[idx].debit_required = true;
					else
						p_trm_mem->trans_rec[idx].debit_required = false;

					if(strncmp((char*)buffer,  "SealOnly", 8 ) == 0)
						p_trm_mem->trans_rec[idx].print_required = false;
					else
						p_trm_mem->trans_rec[idx].print_required = true;

					p_trm_mem->trans_rec[idx].flags.is_populated.mode = true;
					break;
				}
				case SETTING_ITEM_POSTAGE:
					memcpy( &p_trm_mem->trans_rec[idx].postage, buffer, sizeof(p_trm_mem->trans_rec[idx].postage));
					p_trm_mem->trans_rec[idx].flags.is_populated.postage = true;
					break;
				case SETTING_ITEM_AD_GRAPHIC:
					//strncpy( p_trm_mem->trans_rec[idx].graph, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].graph));
					if((length / 2) < sizeof(p_trm_mem->trans_rec[idx].graph))
					{
						//if(fnUnicodeStr2AsciiStr((unsigned short *)buffer, p_trm_mem->trans_rec[idx].graph, length))
						UINT8   bcdGraphName[ sizeof(p_trm_mem->trans_rec[idx].graph) ];
						UINT8   bcdGraphName2[sizeof(p_trm_mem->trans_rec[idx].graph) ];
						short len, i;
						memset(bcdGraphName, 0x0, sizeof(bcdGraphName));
						memset(bcdGraphName2, 0x0, sizeof(bcdGraphName2));
						len = fnAsciiBcdToPackedBcd(bcdGraphName, sizeof(bcdGraphName), (const char *) buffer, TRUE);
						if ((len > 0) && (len <= 64))
						{
							EndianAwareArray16Copy(bcdGraphName2, bcdGraphName, len);

							if(len <= length)
							{
								for( i = 0; i < len/2; i++ )
								{
									p_trm_mem->trans_rec[idx].graph[i] = bcdGraphName2[i*2];
								}
								p_trm_mem->trans_rec[idx].flags.is_populated.graph = true;
							}
							else
							{
								//Could be invalid graphic
								p_trm_mem->trans_rec[idx].flags.is_populated.graph = false;
							}
						}
						else
						{
							dbgTrace(DBG_LVL_ERROR, "trmUpdateTranRecWithSettingMgr - Error: failed to add Ad Graphic %s, len = %d ", buffer, len);
						}
					}
					else
						p_trm_mem->trans_rec[idx].flags.is_populated.graph = false;
					break;
//				case SETTING_ITEM_INSC_GRAPHIC:
//					strncpy( p_trm_mem->trans_rec[idx].inscGraphic, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].inscGraphic));
//					p_trm_mem->trans_rec[idx].flags.is_populated.inscGraphic = true;
//					break;
				case SETTING_ITEM_CARRIER:
					memcpy( &p_trm_mem->trans_rec[idx].carrier, buffer, sizeof(p_trm_mem->trans_rec[idx].carrier));
					p_trm_mem->trans_rec[idx].flags.is_populated.carrier = true;
					break;
				case SETTING_ITEM_CATEGORY:
					memcpy(&p_trm_mem->trans_rec[idx].cat, buffer, sizeof(p_trm_mem->trans_rec[idx].cat));
					p_trm_mem->trans_rec[idx].flags.is_populated.cat = true;
					break;
				case SETTING_ITEM_CLASS:
					memcpy( &p_trm_mem->trans_rec[idx].rate_class, buffer, sizeof(p_trm_mem->trans_rec[idx].rate_class));
					p_trm_mem->trans_rec[idx].flags.is_populated.rate_class = true;
					break;
//				case SETTING_ITEM_CLASSTOKEN:
//					memcpy( &p_trm_mem->trans_rec[idx].rate_class, buffer, length);
//					p_trm_mem->trans_rec[idx].flags.is_populated.rate_class = true;
//					break;
				case SETTING_ITEM_CLASS_NAME:
				case SETTING_ITEM_CLASS_CODE:
					strncpy( p_trm_mem->trans_rec[idx].mail_class, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].mail_class));
					p_trm_mem->trans_rec[idx].flags.is_populated.mail_class = true;
					break;
				case SETTING_ITEM_SHORT_CLASS_NAME:
					strncpy( p_trm_mem->trans_rec[idx].short_class, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].short_class));
					p_trm_mem->trans_rec[idx].flags.is_populated.short_class = true;
					break;
				case SETTING_ITEM_WEIGHT:
					strncpy( p_trm_mem->trans_rec[idx].weight, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].weight));
					p_trm_mem->trans_rec[idx].flags.is_populated.weight = true;
					break;
				case SETTING_ITEM_WEIGHT_UNIT:
					if(memcmp( buffer, "g", 1) == 0)
						p_trm_mem->header.weight_unit = WT_UNIT_GRAM;
					else
						p_trm_mem->header.weight_unit = WT_UNIT_OZ;
					break;
				case SETTING_ITEM_PRODID:
					strncpy( p_trm_mem->trans_rec[idx].prod_id, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].prod_id));
					p_trm_mem->trans_rec[idx].flags.is_populated.prod_id = true;
					break;
				case SETTING_ITEM_BASE_RATE:
					memcpy( &p_trm_mem->trans_rec[idx].base_rate, buffer, sizeof(p_trm_mem->trans_rec[idx].base_rate));
					p_trm_mem->trans_rec[idx].flags.is_populated.base_rate = true;
					break;
				case SETTING_ITEM_DEST_ZIP:
					strncpy( p_trm_mem->trans_rec[idx].dest_zip, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].dest_zip));
					p_trm_mem->trans_rec[idx].flags.is_populated.dest_zip = true;
					break;
				case SETTING_ITEM_DEST_ZONE:
					strncpy( p_trm_mem->trans_rec[idx].dest_zone, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].dest_zone));
					p_trm_mem->trans_rec[idx].flags.is_populated.dest_zone = true;
					break;
				case SETTING_ITEM_PBCC:
					strncpy( &p_trm_mem->trans_rec[idx].pb_cc, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].pb_cc));
					p_trm_mem->trans_rec[idx].flags.is_populated.pb_cc = true;
					break;
				case SETTING_ITEM_DEST_CC:
				case SETTING_ITEM_DEST_CC_ISO:
					strncpy( p_trm_mem->trans_rec[idx].country_code, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].country_code));
					p_trm_mem->trans_rec[idx].flags.is_populated.country_code = true;
					break;
				case SETTING_ITEM_TRACK_ID:
					strncpy( p_trm_mem->trans_rec[idx].track_id, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].track_id));
					p_trm_mem->trans_rec[idx].flags.is_populated.track_id = true;
					break;
				case SETTING_ITEM_REF_NUM:
					strncpy( p_trm_mem->trans_rec[idx].ref_num, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].ref_num));
					p_trm_mem->trans_rec[idx].flags.is_populated.ref_num = true;
					break;
				case SETTING_ITEM_MAILER_ID:
					dbgTrace(DBG_LVL_INFO, "trmUpdateTranRecWithSettingMgr -MailerID:%s ", buffer);
					if(strncmp( CMOSTrmCommon.mailer_id, (char*)buffer, sizeof(CMOSTrmCommon.mailer_id)) != 0)
					{
						strncpy( CMOSTrmCommon.mailer_id, (char*)buffer, sizeof(CMOSTrmCommon.mailer_id));
						CMOSTrmCommon.flags.is_populated.mailer_id = true;
					}
					break;
//				case SETTING_ITEM_RATE_EFF_DATE:
//					break;
				case SETTING_ITEM_NUM_DEC:
					memcpy( &p_trm_mem->trans_rec[idx].decimalPosition, buffer, sizeof(p_trm_mem->trans_rec[idx].decimalPosition));
					break;
				case SETTING_ITEM_ACCT_NAME:
					if(length < sizeof(p_trm_mem->trans_rec[idx].act_name))
					{
						strncpy( p_trm_mem->trans_rec[idx].act_name, (char*)buffer, length);
						p_trm_mem->trans_rec[idx].flags.is_populated.act_name = true;
					}
					else
					{
						strncpy( p_trm_mem->trans_rec[idx].act_name, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].act_name));
						p_trm_mem->trans_rec[idx].flags.is_populated.act_name = true;
					}

					break;
				case SETTING_ITEM_ACCT_CODE:
					if(length < sizeof(p_trm_mem->trans_rec[idx].actCode))
					{
						strncpy( p_trm_mem->trans_rec[idx].actCode, (char*)buffer, length);
						p_trm_mem->trans_rec[idx].flags.is_populated.actCode = true;
					}
					else
					{
						strncpy( p_trm_mem->trans_rec[idx].actCode, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].actCode));
						p_trm_mem->trans_rec[idx].flags.is_populated.actCode = true;
					}
					break;
				case SETTING_ITEM_BATCH_ID:
					strncpy( p_trm_mem->trans_rec[idx].batch_id, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].batch_id));
					p_trm_mem->trans_rec[idx].flags.is_populated.batch_id = true;
					break;

				case SETTING_ITEM_ACCT_BATCH_END:
					memcpy( &p_trm_mem->trans_rec[idx].acct_batch_end, buffer, sizeof(p_trm_mem->trans_rec[idx].acct_batch_end));
					p_trm_mem->trans_rec[idx].flags.is_populated.acct_batch_end = true;
					break;
				case SETTING_ITEM_TAG_1:
					strncpy( p_trm_mem->trans_rec[idx].tag1, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].tag1));
					p_trm_mem->trans_rec[idx].flags.is_populated.tag1 = true;
					break;
				case SETTING_ITEM_TAG_2:
					strncpy( p_trm_mem->trans_rec[idx].tag2, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].tag2));
					p_trm_mem->trans_rec[idx].flags.is_populated.tag2 = true;
					break;
				case SETTING_ITEM_OPERATOR:
					strncpy( p_trm_mem->trans_rec[idx].oper, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].oper));
					p_trm_mem->trans_rec[idx].flags.is_populated.oper = true;
					break;
				case SETTING_ITEM_EKP:
					strncpy( p_trm_mem->trans_rec[idx].ekp, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].ekp));
					p_trm_mem->trans_rec[idx].flags.is_populated.ekp = true;
					break;
				case SETTING_ITEM_EKP_TYPE:
					memcpy( &p_trm_mem->trans_rec[idx].ekp_type, buffer, sizeof(p_trm_mem->trans_rec[idx].ekp_type));
					p_trm_mem->trans_rec[idx].flags.is_populated.ekp_type = true;
					break;
				case SETTING_ITEM_CONSIGNOR:
					strncpy( p_trm_mem->trans_rec[idx].consignor, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].consignor));
					p_trm_mem->trans_rec[idx].flags.is_populated.consignor = true;
					break;
				case SETTING_ITEM_CONSIGNEE:
					strncpy( p_trm_mem->trans_rec[idx].consignee, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].consignee));
					p_trm_mem->trans_rec[idx].flags.is_populated.consignee = true;
					break;
				case SETTING_ITEM_TASK_NUM:
					memcpy( &p_trm_mem->trans_rec[idx].task_num, buffer, sizeof(p_trm_mem->trans_rec[idx].task_num));
					p_trm_mem->trans_rec[idx].flags.is_populated.task_num = true;
					break;
				case SETTING_ITEM_TAPE:
					memcpy( &p_trm_mem->trans_rec[idx].tape, buffer, sizeof(p_trm_mem->trans_rec[idx].tape));
					p_trm_mem->trans_rec[idx].flags.is_populated.tape = true;
					break;
				case SETTING_ITEM_SUBMISSION_DATE:
					strncpy( p_trm_mem->trans_rec[idx].printed_date, (char*)buffer, sizeof(p_trm_mem->trans_rec[idx].printed_date));
					p_trm_mem->trans_rec[idx].flags.is_populated.printed_date = true;
					break;
//				case SETTING_ITEM_SUR_TYPE:
//					memcpy( &p_trm_mem->trans_rec[idx].sur_type, buffer, length);
//					p_trm_mem->trans_rec[idx].flags.is_populated.sur_type = true;
//					break;
//				case SETTING_ITEM_PER_PIECE:
//					memcpy( p_trm_mem->trans_rec[idx].per_peice, buffer, length);
//					p_trm_mem->trans_rec[idx].flags.is_populated.per_peice = true;
//					break;
//				case SETTING_ITEM_PER_JOB:
//					memcpy( p_trm_mem->trans_rec[idx].per_job, buffer, length);
//					p_trm_mem->trans_rec[idx].flags.is_populated.per_job = true;
//					break;
//				case SETTING_ITEM_PERCENT_OF_JOB:
//					memcpy( p_trm_mem->trans_rec[idx].percent_job, buffer, length);
//					p_trm_mem->trans_rec[idx].flags.is_populated.percent_job = true;
//					break;
//				case SETTING_ITEM_PKG_TYP:
//					memcpy( p_trm_mem->trans_rec[idx].pkg_type, buffer, length);
//					p_trm_mem->trans_rec[idx].flags.is_populated.pkg_type = true;
//					break;
//				case SETTING_ITEM_REASON_CODE:
//					memcpy( p_trm_mem->trans_rec[idx].reason_code, buffer, length);
//					p_trm_mem->trans_rec[idx].flags.is_populated.reason_code = true;
//					break;
//				case SETTING_ITEM_EXT_PKG_ID:
//					memcpy( p_trm_mem->trans_rec[idx].ext_pkg_id, buffer, length);
//					p_trm_mem->trans_rec[idx].flags.is_populated.ext_pkg_id = true;
//					break;
//				case SETTING_ITEM_WARN_VAL:
//					memcpy( p_trm_mem->trans_rec[idx].warn_val, buffer, length);
//					p_trm_mem->trans_rec[idx].flags.is_populated.warn_val = true;
//					break;
//				case SETTING_ITEM_FEE_LIST:
//					memcpy( p_trm_mem->trans_rec[idx].fee_list, buffer, sizeof(p_trm_mem->trans_rec[idx].fee_list));
//					p_trm_mem->trans_rec[idx].flags.is_populated.fee_list = true;
//					break;
				default:
					break;
				}


			}
		}

	}

	return SUCCESS;
}

uint32_t trmMoveToValidIdx(uint32_t idx)
{
    trm_mem_t *p_trm_mem = get_trm_mem();

    while((idx < CFG_NUM_RECORDS) && (p_trm_mem->trans_rec[idx].debit_cert_size == 0))
    {
    	idx++;
    }

    if(idx >= CFG_NUM_RECORDS)
    	idx = 0;

    return idx;
}


//Call this after package is uploaded
STATUS trmInitializeDcapPackageInfo(void)
{

	STATUS status = SUCCESS;
	MONEY dummy;
	memset(&dummy, 0, sizeof(MONEY));

	//Set initialize Package info once in life after Audit.
	if( (CMOSTrmPkgInfoCur.start_pc == 0 ) &&
		(memcmp(&CMOSTrmPkgInfoCur.start_dr, &dummy, sizeof(MONEY)) == 0) )
	{
		memcpy(&CMOSTrmPkgInfoCur.start_pc, pIPSD_pieceCount, sizeof(CMOSTrmPkgInfoCur.start_pc));
		memcpy(&CMOSTrmPkgInfoCur.start_ar, &m5IPSD_ascReg, sizeof(MONEY));
		memcpy(&CMOSTrmPkgInfoCur.start_dr, &mStd_DescendingReg, sizeof(MONEY));
		CMOSTrmPkgInfoCur.start_mp_id = get_cur_mp_id();

		memcpy(&CMOSTrmPkgInfoCur.end_pc, pIPSD_pieceCount, sizeof(CMOSTrmPkgInfoCur.end_pc));
		memcpy(&CMOSTrmPkgInfoCur.end_ar, &m5IPSD_ascReg, sizeof(MONEY));
		memcpy(&CMOSTrmPkgInfoCur.end_dr,&mStd_DescendingReg, sizeof(MONEY));
		CMOSTrmPkgInfoCur.end_mp_id = get_cur_mp_id();



		memcpy(&CMOSTrmPkgInfoPrev.start_pc, pIPSD_pieceCount, sizeof(CMOSTrmPkgInfoPrev.start_pc));
		memcpy(&CMOSTrmPkgInfoPrev.start_ar, &m5IPSD_ascReg, sizeof(MONEY));
		memcpy(&CMOSTrmPkgInfoPrev.start_dr, &mStd_DescendingReg, sizeof(MONEY));
		CMOSTrmPkgInfoPrev.start_mp_id = get_cur_mp_id();

		memcpy(&CMOSTrmPkgInfoPrev.end_pc, pIPSD_pieceCount, sizeof(CMOSTrmPkgInfoPrev.end_pc));
		memcpy(&CMOSTrmPkgInfoPrev.end_ar, &m5IPSD_ascReg, sizeof(MONEY));
		memcpy(&CMOSTrmPkgInfoPrev.end_dr, &mStd_DescendingReg, sizeof(MONEY));
		CMOSTrmPkgInfoPrev.end_mp_id = get_cur_mp_id();
	}

	return status;
}



STATUS trmUpdateDcapCurPackageInfo(BOOL bStart, mp_id_t mp_id, uint32_t pc, MONEY *pAR, MONEY *pDR)
{
	if(bStart)
	{
		CMOSTrmPkgInfoCur.start_pc = pc;
		memcpy(&CMOSTrmPkgInfoCur.start_ar, pAR, sizeof(MONEY));
		memcpy(&CMOSTrmPkgInfoCur.start_dr, pDR, sizeof(MONEY));
		CMOSTrmPkgInfoCur.start_mp_id = mp_id;

		CMOSTrmPkgInfoCur.end_pc = pc;
		memcpy(&CMOSTrmPkgInfoCur.end_ar, pAR, sizeof(MONEY));
		memcpy(&CMOSTrmPkgInfoCur.end_dr, pDR, sizeof(MONEY));
		CMOSTrmPkgInfoCur.end_mp_id = mp_id;
	}
	else
	{
		CMOSTrmPkgInfoCur.end_pc = pc;
		memcpy(&CMOSTrmPkgInfoCur.end_ar, pAR, sizeof(MONEY));
		memcpy(&CMOSTrmPkgInfoCur.end_dr, pDR, sizeof(MONEY));
		CMOSTrmPkgInfoCur.end_mp_id = mp_id;
	}
	return SUCCESS;
}

void MoveCurPkgInfoToPrevPkgInfo(void)
{
	memcpy(&CMOSTrmPkgInfoPrev, &CMOSTrmPkgInfoCur, sizeof(pkg_info_t));
}

STATUS trmDeleteAllTxFiles(void)
{
	DSTAT statobj;
	char pattern[32];
	char path[64];

	fnFilePath(path, "trs", "");

	//Remove Transaction record files
	sprintf(pattern, "%s\\TR-*", path);
	if (NU_Get_First(&statobj, pattern) == NU_SUCCESS)
	{
		while(1)
		{
			fnFilePath(path, "trs", statobj.lfname);
			if(NU_Delete(path) == NU_SUCCESS)
				dbgTrace(DBG_LVL_INFO, "TRM - trmDeleteAllTxFiles Deleted file %s", path);
			else
			{
				CMOSTrmFailedDeletes++;
				dbgTrace(DBG_LVL_INFO, "TRM - trmDeleteAllTxFiles Failed to Delete file %s, failedDeletes:%d", path, CMOSTrmFailedDeletes);
			}


			if (NU_Get_Next(&statobj) != NU_SUCCESS)
			{
				NU_Done(&statobj);
				break;
			}
		}
	}

	fnFilePath(path, "trs", "");
	//Remove Compressed Transaction record files
	sprintf(pattern, "%s\\TRC-*", path);
	if (NU_Get_First(&statobj, pattern) == NU_SUCCESS)
	{
		while(1)
		{
			fnFilePath(path, "trs", statobj.lfname);
			if(NU_Delete(path) == NU_SUCCESS)
				dbgTrace(DBG_LVL_INFO, "trmDeleteAllTxFiles Deleted file %s", path);
			else
			{
				CMOSTrmFailedDeletes++;
				dbgTrace(DBG_LVL_INFO, "trmDeleteAllTxFiles Failed to Delete file %s, failedWrites:%d", path, CMOSTrmFailedDeletes);
			}

			if (NU_Get_Next(&statobj) != NU_SUCCESS)
			{
				NU_Done(&statobj);
				break;
			}
		}
	}

	//Remove Compressed Transaction record files
	sprintf(pattern, "%s\\TRD-*", path);
	if (NU_Get_First(&statobj, pattern) == NU_SUCCESS)
	{
		while(1)
		{
			fnFilePath(path, "trs", statobj.lfname);
			if(NU_Delete(path) == NU_SUCCESS)
				dbgTrace(DBG_LVL_INFO, "trmDeleteAllTxFiles Deleted file %s", path);
			else
			{
				CMOSTrmFailedDeletes++;
				dbgTrace(DBG_LVL_INFO, "trmDeleteAllTxFiles Failed to Delete file %s, failed writes:%d", path, CMOSTrmFailedDeletes);
			}

			if (NU_Get_Next(&statobj) != NU_SUCCESS)
			{
				NU_Done(&statobj);
				break;
			}
		}
	}

	return SUCCESS;
}


STATUS trmDeleteUploadedTxFiles(void)
{
	STATUS status = SUCCESS;
	int cnt;
	char path[64];

	dbgTrace(DBG_LVL_INFO, "trmDeleteUploadedTxFiles Files to be deleted:%d ", CMOSTrmFileList.fileCount);
	for( cnt = 0; (cnt < CMOSTrmFileList.fileCount) && (cnt < MAX_FILE_LIST_EX); cnt++)
	{
		dbgTrace(DBG_LVL_INFO, "trmDeleteUploadedTxFiles Files-%d: %s, state: %d`",
									cnt, CMOSTrmFileList.trm_file_list[cnt].trmFileName, CMOSTrmFileList.trm_file_list[cnt].file_state);
	}

	for( cnt = 0; (cnt < CMOSTrmFileList.fileCount) && (cnt < MAX_FILE_LIST_EX) && (status == SUCCESS) ; cnt++)
	{
		if( (strcmp(CMOSTrmFileList.trm_file_list[cnt].trmFileName, "") != 0) &&
			(CMOSTrmFileList.trm_file_list[cnt].file_state != TRM_FILE_DELETE) )
		{
			fnFilePath(path, "trs", CMOSTrmFileList.trm_file_list[cnt].trmFileName);
			dbgTrace(DBG_LVL_INFO, "TRM - trmDeleteUploadedTxFiles File Name: %s", path);
			if(CMOSTrmFileList.trm_file_list[cnt].file_state != TRM_FILE_DELETE)
			{
				status = NU_Delete(path);
				if((status == NU_SUCCESS) || (status == NUF_NOFILE))
				{
					CMOSTrmFileList.trm_file_list[cnt].file_state = TRM_FILE_DELETE;
					dbgTrace(DBG_LVL_INFO, "TRM - trmDeleteUploadedTxFiles Deleted file %s", path);
					//strcpy(CMOSTrmFileList.trm_file_list[cnt].trmFileName, "");
					status = SUCCESS;
				}
				else
				{
					dbgTrace(DBG_LVL_INFO, "TRM - trmDeleteUploadedTxFiles Failed to Delete file:%s, status %d", path, status);
					fnCheckFileSysCorruption(status);
					break;
				}
			}
//			else
//			{
//				dbgTrace(DBG_LVL_INFO, "TRM - trmDeleteUploadedTxFiles Not marked for delete file:%s, status %d", path, status);
//				status = 1;
//			}
		}
//		else
//		{
//			dbgTrace(DBG_LVL_INFO, "TRM - trmDeleteUploadedTxFiles Cannot Delete empty file %s", path);
//
//		}

	}

	if( status == SUCCESS)
	{
		memset(&CMOSTrmFileList, 0, sizeof(CMOSTrmFileList));

	}

	return status;

}

STATUS trmCheckForUploadCleanup(void)
{
	STATUS status = SUCCESS;

	if(CMOSTrmTRCFilesToBeRemoved == TRUE)
	{
		status = trmDeleteUploadedTxFiles();
		if(status == SUCCESS)
		{
			dbgTrace(DBG_LVL_INFO, "trmCheckForUploadCleanup - Deleted uploaded trx files ");
			CMOSTrmTRCFilesToBeRemoved = FALSE;

		}
		else
		{
			dbgTrace(DBG_LVL_INFO, "trmCheckForUploadCleanup - Deleting uploaded trx files failed:%d ", status);
		}
	}

	return status;
}

void trmReleaseFileList(void)
{
	int i;
	for( i = 0; i < TRM_MAX_NUMBER_OF_FILES; i++)
	{
		if(trmFileList[i] != NULL)
		{
			NU_Deallocate_Memory(trmFileList[i]);
			trmFileList[i] = NULL;
		}
	}

}

STATUS trmDeleteUploadedDcapFiles(void)
{
	STATUS status = SUCCESS;
	STATUS nuStatus;
	DSTAT statobj;
	char pattern[32];
	char path[64];
	int count = 0;
	int cntFileList = 0;
	int i;


	memset(&path[0], 0, sizeof(path));
	fnFilePath(path, "upld", NULL);

	//Remove XML
	sprintf(pattern, "%s\\*.xml", path);
	if ((nuStatus = NU_Get_First(&statobj, pattern)) == NU_SUCCESS)
	{
		while(1)
		{
			count++;

			if ((nuStatus = NU_Get_Next(&statobj)) != NU_SUCCESS)
			{
				NU_Done(&statobj);
				break;
			}
		}
	}

	if (nuStatus != NU_SUCCESS) 
		fnCheckFileSysCorruption(nuStatus);

	//Prepare xml list and sort
	cntFileList = count;
	status = trmPrepareFileNamesList(trmFileList, &cntFileList, pattern);
	if(status == SUCCESS)
	{
		if(cntFileList != count)
		{
			dbgTrace(DBG_LVL_INFO, "trmDeleteUploadedDcapFiles Invalid xml files noticed count:%d, validFiles:%d ", count, cntFileList);
			count = cntFileList;
		}

		for(i = 0; i < count - TRM_MAX_DECAP_FILES_TO_KEEP; i++)
		{
			memset(&path[0], 0, sizeof(path));
			fnFilePath(path, "upld", trmFileList[i]);
			if((nuStatus = NU_Delete(path)) == NU_SUCCESS)
				dbgTrace(DBG_LVL_INFO, "trmDeleteUploadedDcapFiles Deleted file %s", path);
			else
			{
				fnCheckFileSysCorruption(nuStatus);
				dbgTrace(DBG_LVL_INFO, "trmDeleteUploadedDcapFiles Failed to Delete file %s", path);
			}
		}
	}
	else
	{
		dbgTrace(DBG_LVL_INFO, "trmDeleteUploadedDcapFiles Failed prepare list count:%d, cntFileList:%d", count, cntFileList);
	}

	trmReleaseFileList();

//	if(count > MAX_DECAP_FILES_TO_KEEP)
//	{
//		if (NU_Get_First(&statobj, pattern) == NU_SUCCESS)
//		{
//			dbgTrace(DBG_LVL_INFO, "trmDeleteAllTxFiles Deleting *.xml file(s)");
//			while(1)
//			{
//				if(( count - MAX_DECAP_FILES_TO_KEEP) > 0)
//				{
//					fnFilePath(path, NULL, statobj.lfname);
//					if(NU_Delete(path) == NU_SUCCESS)
//						dbgTrace(DBG_LVL_INFO, "trmDeleteAllTxFiles Deleted file %s", path);
//					else
//						dbgTrace(DBG_LVL_INFO, "trmDeleteAllTxFiles Failed to Delete file %s", path);
//
//					if (NU_Get_Next(&statobj) != NU_SUCCESS)
//					{
//						break;
//					}
//
//					count--;
//				}
//				else
//					break;
//
//			}
//			NU_Done(&statobj);
//		}
//	}

	//Remove Compressed .zip

	memset(&path[0], 0, sizeof(path));
	fnFilePath(path, "upld", NULL);
	sprintf(pattern, "%s\\*.zip", path);
	count = 0;
	if ((nuStatus = NU_Get_First(&statobj, pattern)) == NU_SUCCESS)
	{
		while(1)
		{
			count++;

			if ((nuStatus = NU_Get_Next(&statobj)) != NU_SUCCESS)
			{
				NU_Done(&statobj);
				break;
			}
		}
	}
	fnCheckFileSysCorruption(nuStatus);

	//Prepare list and sort
	cntFileList = count;
	status = trmPrepareFileNamesList(trmFileList, &cntFileList, pattern);
	if(status == SUCCESS)
	{
		if(cntFileList != count)
		{
			dbgTrace(DBG_LVL_INFO, "trmDeleteUploadedDcapFiles Invalid .zip files noticed count:%d, validFiles:%d ", count, cntFileList);
			count = cntFileList;
		}

		for(i = 0; i < count - TRM_MAX_DECAP_FILES_TO_KEEP; i++)
		{
			memset(&path[0], 0 , sizeof(path));
			fnFilePath(path, "upld", trmFileList[i]);
			if((nuStatus = NU_Delete(path)) == NU_SUCCESS)
				dbgTrace(DBG_LVL_INFO, "trmDeleteUploadedDcapFiles Deleted file %s", path);
			else
			{
				fnCheckFileSysCorruption(nuStatus);
				dbgTrace(DBG_LVL_INFO, "trmDeleteUploadedDcapFiles Failed to Delete file %s", path);
			}
		}
	}
	else
	{
		dbgTrace(DBG_LVL_INFO, "trmDeleteUploadedDcapFiles Failed prepare zip file list count:%d, cntFileList:%d", count, cntFileList);
	}

	trmReleaseFileList();

	return status;
}



void trmSetDcapDeviceID(ushort devID)
{
	CMOSTrmDcapDeviceID = devID;
}

ushort trmGetDcapDeviceID(void)
{
	return CMOSTrmDcapDeviceID;
}

void trmSetDcapGUID(char * pGuid, uint16_t length)
{
	strncpy((char*)CMOSDcapGuid, pGuid, sizeof(CMOSDcapGuid));
}

void trmGetDcapGUID(char * pGuid, uint16_t *length)
{
	strncpy(pGuid, (char*)CMOSDcapGuid, sizeof(CMOSDcapGuid));
	*length = strlen(pGuid);
}

static int cmpstringp(const void *p1, const void *p2)
{
    /* The actual arguments to this function are "pointers to
       pointers to char", but strcmp(3) arguments are "pointers
       to char", hence the following cast plus dereference */

   return strcmp(* (char * const *) p1, * (char * const *) p2);
}

STATUS trmPrepareFileNamesList(char *fileNameList[], int *listSize, char * pattern)
{
	STATUS status = SUCCESS;
	STATUS nuStatus;
	DSTAT statobj;
	int i;
	int count = *listSize;
	int cntValid = 0;

	dbgTrace(DBG_LVL_INFO, "trmPrepareListofFileNames Count: %d \n", count);
	if(count < TRM_MAX_NUMBER_OF_FILES)
	{

		//Prepare the list of file names
		if ((nuStatus = NU_Get_First(&statobj, pattern)) == NU_SUCCESS)
		{
			for(i = 0; i < count; i++)
			{
				//if(trm_log_level)
					dbgTrace(DBG_LVL_INFO, "trmPrepareFileNamesList fileName=%s, file size=%d ", statobj.lfname, statobj.fsize);

				//if file sise zero move them to trs2
				if(statobj.lfname == NULL)
				{
					dbgTrace(DBG_LVL_INFO, "trmPrepareFileNamesList invalid file name fileName=%s, file size=%d  Ignore ", statobj.lfname, statobj.fsize);
				}
				else if(statobj.fsize == 0)
				{
					char path[80];
					CMOSTrmZeroLengthFiles++;
					dbgTrace(DBG_LVL_INFO, "trmPrepareFileNamesList Delete fileName=%s, file size=%d to trs2", statobj.lfname, statobj.fsize);

					fnFilePath(path, "trs", statobj.lfname);
					status = NU_Delete(path);
					if(status != SUCCESS)
					{
						fnCheckFileSysCorruption(status);
						CMOSTrmZeroLenDeletFails++;
						dbgTrace(DBG_LVL_INFO, "Delete failed %s, status:%d, zeroLength file deletes:%d", path, status, CMOSTrmZeroLenDeletFails );
						status = SUCCESS;
					}
					else
					{
						dbgTrace(DBG_LVL_INFO, "Delete Success %s, status:%d, zeroLength file deletes:%d", path, status, CMOSTrmZeroLenDeletFails );
					}

				}
				else
				{
					NU_Allocate_Memory(pSystemMemoryCached, ( void *) &fileNameList[i], (TRM_MAX_FILE_NAME_SIZE), NU_NO_SUSPEND);
					if(fileNameList[i] == NULL)
					{
						dbgTrace(DBG_LVL_INFO, "trmPrepareListofFileNames  failed: buffer length:%d, count:%d \n", TRM_MAX_FILE_NAME_SIZE, i);
						status = -100;
						break;
					}
					else
					{
						memset(fileNameList[i], 0, TRM_MAX_FILE_NAME_SIZE);
					}


					strcpy(fileNameList[cntValid], statobj.lfname);
					cntValid++;
				}


				if ((nuStatus = NU_Get_Next(&statobj)) != NU_SUCCESS)
				{
					fnCheckFileSysCorruption(nuStatus);
					break;
				}
			}

			NU_Done(&statobj);
		}
		else
		{
			fnCheckFileSysCorruption(nuStatus);
			dbgTrace(DBG_LVL_INFO, "trmPrepareListofFileNames  failed get first object:\n");
			//status = -200;
		}

		//Sort file names
		qsort(fileNameList, cntValid, sizeof(char *), cmpstringp);

		dbgTrace(DBG_LVL_INFO, "qsort:  After \n");
		for(i = 0; i < cntValid; i++)
			dbgTrace(DBG_LVL_INFO, "qsort:  %s ",fileNameList[i]);
	}
	else
	{
		dbgTrace(DBG_LVL_INFO, "trmPrepareListofFileNames  count:%d exceeds the limit %d  \n", count, TRM_MAX_NUMBER_OF_FILES);
		status = -300;
	}

	*listSize = cntValid;

	return status;
}



//Used tp prepare TRC and TRD file list
STATUS trmPrepareFileNamesListEx(char *fileNameList[], int *listSize)
{
	STATUS status = SUCCESS;
	STATUS nuStatus;
	DSTAT statobj;
	int i;
	int count = 0;
	int countNew = 0;
	int cntValid = 0;
	char path[64];
	char pattern[32];

	memset(&path[0], 0, sizeof(path));
	fnFilePath(path, "trs", NULL);

	//Check for old pattern
	sprintf(pattern, "%s\\TRC-*", path);
	if ((nuStatus = NU_Get_First(&statobj, pattern)) == NU_SUCCESS)
	{
		while(1)
		{
			count++;

			if ((nuStatus = NU_Get_Next(&statobj)) != NU_SUCCESS)
			{
				NU_Done(&statobj);
				break;
			}
		}
	}
	fnCheckFileSysCorruption(nuStatus);


	if(count > 0)
	{
		//Prepare the list of file names
		if ((nuStatus = NU_Get_First(&statobj, pattern)) == NU_SUCCESS)
		{
			for(i = 0; i < count; i++)
			{
				//if(trm_log_level)
					dbgTrace(DBG_LVL_INFO, "trmPrepareFileNamesList fileName=%s, file size=%d ", statobj.lfname, statobj.fsize);

				//if file sise zero move them to trs2
				if(statobj.lfname == NULL)
				{
					dbgTrace(DBG_LVL_INFO, "trmPrepareFileNamesList invalid file name fileName=%s, file size=%d  Ignore ", statobj.lfname, statobj.fsize);
				}
				else if(statobj.fsize == 0)
				{
					char path[80];
					CMOSTrmZeroLengthFiles++;
					dbgTrace(DBG_LVL_INFO, "trmPrepareFileNamesList delete fileName=%s, file size=%d to trs2", statobj.lfname, statobj.fsize);
					fnFilePath(path, "trs", statobj.lfname);

					status = NU_Delete(path);
					if(status != SUCCESS)
					{
						fnCheckFileSysCorruption(status);
						CMOSTrmZeroLenDeletFails++;
						dbgTrace(DBG_LVL_INFO, "Delete failed %s, status:%d, CMOSTrmZeroLenDeletFails:%d", path, status, CMOSTrmZeroLenDeletFails );
						status = SUCCESS;
					}
					else
					{
						dbgTrace(DBG_LVL_INFO, "Delete Success %s, status:%d, CMOSTrmZeroLenDeletFails:%d", path, status, CMOSTrmZeroLenDeletFails );
					}

				}
				else
				{
					NU_Allocate_Memory(pSystemMemoryCached, ( void *) &fileNameList[cntValid], (TRM_MAX_FILE_NAME_SIZE), NU_NO_SUSPEND);
					if(fileNameList[cntValid] == NULL)
					{
						dbgTrace(DBG_LVL_INFO, "trmPrepareFileNamesListEx  failed: buffer length:%d, count:%d \n", TRM_MAX_FILE_NAME_SIZE, i);
						status = -100;
						break;
					}
					else
					{
						memset(fileNameList[cntValid], 0, TRM_MAX_FILE_NAME_SIZE);
					}

					strcpy(fileNameList[cntValid], statobj.lfname);
					cntValid++;
				}


				if ((nuStatus = NU_Get_Next(&statobj)) != NU_SUCCESS)
				{
					fnCheckFileSysCorruption(nuStatus);
					break;
				}
			}

			NU_Done(&statobj);
		}
		else
		{
			//not finding files is not an error
			fnCheckFileSysCorruption(nuStatus);
			dbgTrace(DBG_LVL_INFO, "trmPrepareFileNamesListEx  failed get first object:\n");
		}

		//Sort file names
		qsort(fileNameList, cntValid, sizeof(char *), cmpstringp);

		dbgTrace(DBG_LVL_INFO, "qsort:  After File# %d \n", cntValid);
		for(i = 0; i < cntValid; i++)
			dbgTrace(DBG_LVL_INFO, "qsort:  %s ",fileNameList[i]);
	}

	count = cntValid;


	///////////////////////////////////////////////////////////
	//Check for new pattern
	sprintf(pattern, "%s\\TRD-*", path);
	if ((nuStatus = NU_Get_First(&statobj, pattern)) == NU_SUCCESS)
	{
		while(1)
		{
			countNew++;

			if ((nuStatus = NU_Get_Next(&statobj)) != NU_SUCCESS)
			{
				NU_Done(&statobj);
				break;
			}
		}
	}
	fnCheckFileSysCorruption(nuStatus);

	if(countNew > 0)
	{
		//Prepare the list of file names
		if ((nuStatus = NU_Get_First(&statobj, pattern)) == NU_SUCCESS)
		{
			for(i = count; i < (count+ countNew); i++)
			{
				//if(trm_log_level)
					dbgTrace(DBG_LVL_INFO, "trmPrepareFileNamesList fileName=%s, file size=%d ", statobj.lfname, statobj.fsize);

				//if file sise zero move them to trs2
				if(statobj.lfname == NULL)
				{
					dbgTrace(DBG_LVL_INFO, "trmPrepareFileNamesList invalid file name fileName=%s, file size=%d  Ignore ", statobj.lfname, statobj.fsize);
				}
				else if(statobj.fsize == 0)
				{
					char path[80];
					CMOSTrmZeroLengthFiles++;
					dbgTrace(DBG_LVL_INFO, "trmPrepareFileNamesList delete fileName=%s, file size=%d to trs2", statobj.lfname, statobj.fsize);
					fnFilePath(path, "trs", NULL);

					fnFilePath(path, "trs", statobj.lfname);

					status = NU_Delete(path);
					if(status != SUCCESS)
					{
						fnCheckFileSysCorruption(status);
						CMOSTrmZeroLenDeletFails++;
						dbgTrace(DBG_LVL_INFO, "Delete failed %s, status:%d, CMOSTrmZeroLenDeletFails:%d", path, status, CMOSTrmZeroLenDeletFails );
						status = SUCCESS;
					}
					else
						dbgTrace(DBG_LVL_INFO, "Delete Success %s, status:%d, CMOSTrmZeroLenDeletFails:%d", path, status, CMOSTrmZeroLenDeletFails );

				}
				else
				{
					NU_Allocate_Memory(pSystemMemoryCached, ( void *) &fileNameList[cntValid], (TRM_MAX_FILE_NAME_SIZE), NU_NO_SUSPEND);
					if(fileNameList[cntValid] == NULL)
					{
						dbgTrace(DBG_LVL_INFO, "trmPrepareFileNamesListEx  failed to allocate: buffer length:%d, count:%d \n", TRM_MAX_FILE_NAME_SIZE, i);
						status = -100;
						break;
					}
					else
					{
						memset(fileNameList[cntValid], 0, TRM_MAX_FILE_NAME_SIZE);
					}

					strcpy(fileNameList[cntValid], statobj.lfname);
					cntValid++;
				}


				if ((nuStatus = NU_Get_Next(&statobj)) != NU_SUCCESS)
				{
					fnCheckFileSysCorruption(nuStatus);
					break;
				}
			}

			NU_Done(&statobj);
		}
		else
		{
			fnCheckFileSysCorruption(nuStatus);
			dbgTrace(DBG_LVL_INFO, "trmPrepareFileNamesListEx  failed get first object:\n");
			status = -200;
		}

		//Sort file names
		qsort(fileNameList, cntValid, sizeof(char *), cmpstringp);

		dbgTrace(DBG_LVL_INFO, "qsort:  After File# %d \n", cntValid);
		for(i = 0; i < cntValid; i++)
			dbgTrace(DBG_LVL_INFO, "qsort:  %s ",fileNameList[i]);
	}
	else
	{
		dbgTrace(DBG_LVL_INFO, "trmPrepareFileNamesListEx  count:%d No new transaction files %d  \n", count, TRM_MAX_NUMBER_OF_FILES);
	}

	*listSize = cntValid;

	return status;
}


STATUS trmGetTrxFileCount(int *fileCount)
{
	STATUS status = SUCCESS;
	STATUS nuStatus;
	DSTAT statobj;
	char pattern[32];
	char path[64];

	int trxFileCount = 0;
	int trxNewFileCount = 0;


	fnFilePath(path, "trs", "");

	//Prepare Mail piece buffer
	//Get count of TRC files
	trxFileCount = 0;
	sprintf(pattern, "%s\\TRC-*", path);
	if ((nuStatus = NU_Get_First(&statobj, pattern)) == NU_SUCCESS)
	{
		while(1)
		{
			trxFileCount++;

			if ((nuStatus = NU_Get_Next(&statobj)) != NU_SUCCESS)
			{
				NU_Done(&statobj);
				break;
			}
		}
	}
	fnCheckFileSysCorruption(nuStatus);

	sprintf(pattern, "%s\\TRD-*", path);
	if ((nuStatus = NU_Get_First(&statobj, pattern)) == NU_SUCCESS)
	{
		while(1)
		{
			trxNewFileCount++;

			if ((nuStatus = NU_Get_Next(&statobj)) != NU_SUCCESS)
			{
				NU_Done(&statobj);
				break;
			}
		}
	}
	fnCheckFileSysCorruption(nuStatus);

	*fileCount = trxFileCount + trxNewFileCount;

	return status;
}



STATUS trmCompletePendingWrites(void)
{
	STATUS status = SUCCESS;
	int i;

    if(CMOSTrmPendingBucketToUpload != -1)
    {
    	i = 0;
    	do{
    		if(i++ > 2)
				break;

    		status = trmWriteTrmBucket(CMOSTrmPendingBucketToUpload);
    		if(status != SUCCESS)
    			dbgTrace(DBG_LVL_INFO, "trmCompletePendingWrites-Write Pending Bucket failed: CMOSTrmPendingBucketToUpload:%d, Attempt %d", CMOSTrmPendingBucketToUpload, i);

    	}while(status != SUCCESS);
    }
    else
    {
		dbgTrace(DBG_LVL_INFO, "trmCompletePendingWrites-No pending writes CMOSTrmPendingBucketToUpload:%d ", CMOSTrmPendingBucketToUpload);
    }

    return status;
}


STATUS generate_fram_log_xml(char *p_dest, int32_t max_xml_length, int32_t *p_xml_len)
{
	STATUS status = SUCCESS;

	void *temp_buf = NULL;
	char *p_wr = p_dest;
	char *p_limit = p_dest + max_xml_length;


	status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &temp_buf, CFG_MAX_DAY_ENTRY_SIZE, NU_NO_SUSPEND);
	if(temp_buf != NULL)
	{

		 //Append
		snprintf(temp_buf, 64, "<MailPieceID>%ld</MailPieceID>",CMOSTrmMailPieceID);
		add_to_xml( temp_buf);

		if(CMOSTrmCommon.flags.is_populated.rate_ver == true)
		{
			snprintf(temp_buf, 64, "<RateVer>%s</RateVer>",CMOSTrmCommon.rate_ver);
			add_to_xml( temp_buf);
		}

		if(CMOSTrmCommon.flags.is_populated.mailer_id == true)
		{
			snprintf(temp_buf, 64, "<MailID>%s</MailID>",CMOSTrmCommon.mailer_id);
			add_to_xml( temp_buf);
		}


		snprintf(temp_buf, 64, "<ActiveBucket>%ld</ActiveBucket>",CMOSTrmActiveBucket);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<FailedWrites>%ld</FailedWrites>",CMOSTrmFailedWrites);
		add_to_xml( temp_buf);
		snprintf(temp_buf, 64, "<FailedReads>%ld</FailedReads>",CMOSTrmFailedReads);
		add_to_xml( temp_buf);
		snprintf(temp_buf, 64, "<FailedDeletes>%ld</FailedDeletes>",CMOSTrmFailedDeletes);
		add_to_xml( temp_buf);
		snprintf(temp_buf, 64, "<ZeroLengthFiles>%ld</ZeroLengthFiles>",CMOSTrmZeroLengthFiles);
		add_to_xml( temp_buf);
		snprintf(temp_buf, 64, "<ZeroLenDeletFails>%ld</ZeroLenDeletFails>",CMOSTrmZeroLenDeletFails);
		add_to_xml( temp_buf);


		snprintf(temp_buf, 64, "<FlashFileSeqCount>%ld</FlashFileSeqCount>",CMOSTrmFlashFileSeqCount);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<TrmVersion>%ld</TrmVersion>",CMOSTrmVersion);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<TrmState>%ld</TrmState>",CMOSTRMState);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<UploadState>%ld</UploadState>",CMOSTrmUploadState);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<UploadPkgSeqCount>%ld</UploadPkgSeqCount>",CMOSTrmDcapFileSeqCount);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<GUID>%s</GUID>",CMOSDcapGuid);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 80, "<PrevUploadDate>%2d%02d-%02d-%02dT%02d:%02d:%02d</PrevUploadDate>",
									CMOSTrmPrevUploadDate.bCentury,
									CMOSTrmPrevUploadDate.bYear,
									CMOSTrmPrevUploadDate.bMonth,
									CMOSTrmPrevUploadDate.bDay,
									CMOSTrmPrevUploadDate.bHour,
									CMOSTrmPrevUploadDate.bMinutes,
									CMOSTrmPrevUploadDate.bSeconds);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<UploadDueRequired>%ld</UploadDueRequired>",CMOSTrmUploadDueReqd);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<Midnight>%ld</Midnight>",CMOSTrmMidnightFlag);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<MaxFilesToUpload>%ld</MaxFilesToUpload>",CMOSTrmMaxFilesToUpload);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 80, "<TimeStampOfOldestMP>%2d%02d-%02d-%02dT%02d:%02d:%02d</TimeStampOfOldestMP>",
									CMOSTrmTimeStampOfOldestMP.bCentury,
									CMOSTrmTimeStampOfOldestMP.bYear,
									CMOSTrmTimeStampOfOldestMP.bMonth,
									CMOSTrmTimeStampOfOldestMP.bDay,
									CMOSTrmTimeStampOfOldestMP.bHour,
									CMOSTrmTimeStampOfOldestMP.bMinutes,
									CMOSTrmTimeStampOfOldestMP.bSeconds);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 80, "<TimeStampOfMPIDReset>%2d%02d-%02d-%02dT%02d:%02d:%02d</TimeStampOfMPIDReset>",
									CMOSTrmTimeStampOfMPIDReset.bCentury,
									CMOSTrmTimeStampOfMPIDReset.bYear,
									CMOSTrmTimeStampOfMPIDReset.bMonth,
									CMOSTrmTimeStampOfMPIDReset.bDay,
									CMOSTrmTimeStampOfMPIDReset.bHour,
									CMOSTrmTimeStampOfMPIDReset.bMinutes,
									CMOSTrmTimeStampOfMPIDReset.bSeconds);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<MailPiecesToUpload>%ld</MailPiecesToUpload>",CMOSTrmMailPiecesToUpload);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<MailPiecesPackaged>%ld</MailPiecesPackaged>",CMOSTrmMailPiecesPackaged);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<PendingBucketToUpload>%ld</PendingBucketToUpload>",CMOSTrmPendingBucketToUpload);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<FatalError>%ld</FatalError>",CMOSTrmFatalError);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<TRCFilesToBeRemoved>%ld</TRCFilesToBeRemoved>",CMOSTrmTRCFilesToBeRemoved);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<DcapFileName>%s</DcapFileName>", CMOS2TrmDcapFileName);
		add_to_xml( temp_buf);

		snprintf(temp_buf, 64, "<DcapFileToBeUploaded>%ld</DcapFileToBeUploaded>", CMOS2TrmDcapFileToBeUploaded);
		add_to_xml( temp_buf);

	}
	else
	{
		dbgTrace(DBG_LVL_INFO, "generate_fram_log_xml-failure to allocate:%d ", status);
	}

	if(temp_buf != NULL)
		NU_Deallocate_Memory(temp_buf);
}

int32_t generate_fram_bcuket_cnt(int32_t bucket)
{
	if((bucket == 0 || bucket == 1 ) )
	{
		trm_mem_t *p_trm_mem = get_trm_bucket(bucket);

		return p_trm_mem->header.num_records_in_use;
	}
	else
		return 0;
}

STATUS generate_fram_log_bcuket_xml(char *p_dest, int32_t max_xml_length, int32_t *p_xml_len, int32_t bucket, int32_t record)
{
	STATUS status = SUCCESS;

	trm_mem_t *p_trm_mem = get_trm_bucket(bucket);

	if( (bucket == 0 || bucket == 1 ) &&
		(record >= 0 && record < 16) &&
		(p_trm_mem->header.num_records_in_use != 0) &&
		(p_trm_mem->trans_rec[record].rec_state != TRM_UNUSED) )
	{
		status = generate_tr_xml(&p_trm_mem->trans_rec[record],
							p_dest,
							max_xml_length,
							p_xml_len,
							NULL);
		if(status != SUCCESS)
		{
			dbgTrace(DBG_LVL_INFO, "generate_fram_log_bcuket_xml-Failed processing transaction record: %d, status:%d", record, status);
			*p_xml_len = 0;
		}
	}
	else
	{
		*p_xml_len = 0;
	}

	return status;
}

//Checks rate version change from previous rate version.
BOOL trmRateVerChange(char* ratever)
{
	return (strncmp(CMOSTrmCommon.rate_ver, ratever, sizeof(CMOSTrmCommon.rate_ver)) != 0);
}

//Checks if rate version valid
BOOL trmRateVerValid()
{
	return (CMOSTrmCommon.flags.is_populated.rate_ver == true);
}

//Check if TR records in FRAM or files; Dont care if packaged files have not been uploaded yet
BOOL trmTRUploadPending(void)
{
	BOOL retval = FALSE;
	int fileCount;

    (void) trmGetTrxFileCount(&fileCount);
    if( ((CMOSTrmUploadDueReqd & TRM_UPLOAD_AVAILABLE) == TRM_UPLOAD_AVAILABLE) || (fileCount != 0))
    {
    	retval = TRUE;
    }

	return retval;
}

//Set rate version to FRAM including both bucket headrers
void trmApplyRateVer(char* ratever)
{
	trm_mem_t *p_trm_mem;

	dbgTrace(DBG_LVL_INFO, "New rate version detected: Old: %s, New: %s", CMOSTrmCommon.rate_ver, ratever);

	//Set rate version in common area
	strncpy( CMOSTrmCommon.rate_ver, ratever, sizeof(CMOSTrmCommon.rate_ver));
	CMOSTrmCommon.flags.is_populated.rate_ver = true;

	//Set rate version in bucket headers
	p_trm_mem = get_trm_bucket(CFG_BUCKET0);
	strncpy( p_trm_mem->header.rate_ver, ratever, sizeof(p_trm_mem->header.rate_ver));

	p_trm_mem = get_trm_bucket(CFG_BUCKET1);
	strncpy( p_trm_mem->header.rate_ver, ratever, sizeof(p_trm_mem->header.rate_ver));
}

