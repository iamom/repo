/*******************************************************************************
   PROJECT    :   Future Phoenix
   COPYRIGHT  :   2005, Pitney Bowes, Inc.  
   AUTHOR     :   mozdzjm
   MODULE     :   OIFIELDS.C  
    
   DESCRIPTION:
        Field processing engine and related utility functions 
        for the operator interface.

   MODIFICATION HISTORY:
   
 22-May-17 Jennifer Hao on FPHX 02.12 cienet branch
      Commented out functions fnCreateMWEEntryBuffer,  fnBufferKeyOnce, fnSuppressZeroKey, 
      fnBufferUnicodeScrollLTR,fnBufferUnicodeScrollRTL, fnSetCharsBuffered,  fnSetEntryBufFullFlag because they aren't used by G9.

 24-Mar-14 Wang Biao on FPHX 02.12 cienet barnch
    Changed static variable TmpCombiningEntryBuf to be global for fraca 224776.
    
 20-Mar-14 Wang Biao on FPHX 02.12 cienet branch
    Modifeid function fnMoveCursorRight() to fix fraca 224734.
   
 08-Jan-14 Wang Biao on FPHX 02.10 cienet branch   
    Added function fnIsBufferFull() for Network Proxy Settings.
    
 07-Jun-13 sa002pe on FPHX 02.10 shelton branch
	Changed CombiningCharTable to add the n + tilda characters, which are used in Spanish, and
	possibly Portuguese.
	
 04-Jun-13 sa002pe on FPHX 02.10 shelton branch
 	Changed CombiningCharTable so the letter are in alpha order & to add some comments
	to the comments that have '?' so we know what the accent mark is.
 
 17-Oct-12 sa002pe on FPHX 02.10 shelton branch
	Changed a couple of calls to fnDisplayErrorDirect so they pass the proper task ID.
	
      24 Jun 2010   Rocky Dai     Modified the following functions to fix Fraca for Israel that users can not sort and 
                                  search items using the input field: fnUpdateBufferFullStatus, fnCreateEntryBuffer, 
                                  fnDeleteCharacter, fnBufferKey and fnClearEntryBuffer.
      09 Jun 2010   Jingwei,Li    Modified fnUpdateBufferFullStatus(),fnCreateEntryBuffer() and fnClearEntryBuffer() to
                                  make IP address entry in LTR mode regardless of current language writting mode.
                                  Modified fnBufferKeyScrollRTL() and fnDistrParamBufferKeyScrollRTL() to remove
                                  restriction of insert mode.
      02 Jun 2010   Jingwei,Li    Modified fnBufferKeyScrollRTL() and added fnDistrParamBufferKeyScrollRTL() for RTL.
      29 Apr 2010   Deborah Kohl  Modified fnDeleteCharacterScrollRTL() to remove restriction of insert mode
                                  otherwise, distributors settings can't be modified by deleting characters
      29 Apr 2010   Jingwei,Li    Modified fnMoveCursorRight(),fnMoveCursorLeft() to fix cursor move issue for RTL.
                                  Modified fnBufferKeyScrollRTL() to fix scroll input issue for RTL.
      27 Apr 2010   Deborah Kohl  Modified fnBufferUnicodeRTL() to correctly set the cursor in dialing prefix
                                  screen from OOB for example
      27 Apr 2010   Jingwei,Li    Modified function fnBufferKeyRTL() to handle input field with template character for RTL.
      24 Apr 2010   Jingwei,Li    Added functions fnBufferKeyScrollLTR(), fnBufferKeyScrollRTL()
                                  fnDeleteCharacterScrollLTR(),fnDeleteCharacterScrollRTL(),
                                  fnBufferUnicodeScrollLTR() and fnBufferUnicodeScrollRTL() to support 
                                  scroll input field in RTL writting.
      22 Apr 2010   Deborah Kohl  Modified fnBufferKeyRTL to insert alphanumeric char
                                  on the left and not on the right of hebrew char 
      21 Apr 2010   Jingwei,LI    Update to support RTL language writting direction.
      14 Apr 2010   Jingwei,Li    Modified fnDeleteCharacterRTL() to fix delete issue.
      13 Apr 2010   Jingwei,Li    Update to support RTL language writting direction.
      09 Mar 2010   Deborah Kohl  Added E with caron above in CombiningCharTable array.
      05 Mar 2010   Deborah Kohl  Added U with ring above in CombiningCharTable array.
      10 Feb 2010   Jingwei,Li    Updated the unicode value of Ring Above u.
      09 Feb 2010   Jingwei,Li    Added some specific Czech characters in 
                                  CombiningCharTable array.
      23 Feb 2009   Deborah Kohl  Added lower case specific Slovakian characters in 
                                  CombiningCharTable array.  
      16 Feb 2009   Rocky Dai     Added support for the Special characters of Slovakian.
      03/04/2008    Oscar Wang    Added support for the Tilde in CombiningCharTable array.
      07/23/2007    Vincent Yi    Fixed lint errors
      12 Jun 2007 Simon Fox     Now when combinations are switched off only
      accents that are allowed to be uncombined may be entered.
      12 Jun 2007 Simon Fox     Added functions to enable and disable character
      combinations so that the modem entry screen can have the ^ character
      displayed on its own.
      06/04/2007  Raymond Shen  Added function fnGetEntryBufCharIndex().
      9 Mar 2007  Simon Fox     
      fnSearchKeymap now only does decimal separator substitution for the keypad not the
      full-stop on the keyboard.

      09/15/2006  Raymond Shen  Modified fnDeleteCharacterScroll(), fnMoveCursorLeft(), 
                                fnMoveCursorRight(), fnBufferKeyScroll(), and 
                                fnBufferUnicodeScroll() to deal with cursor's insert mode
                                displayproblems.
      09/11/2006  Kan Jiang  Modified function fnMoveCursorRight to fix cursor display 
                             problem in overwite-type field. 
      07/13/2006  Dicky Sun  Merge the following functions from Mega:
									fnSetCharsBuffered
									fnSetEntryBufFullFlag
									fnSetEntryBufCharIndex
      12/05/2005  Adam Liu   Merged with Mega Rearch code

   OLD PVCS REVISION HISTORY
 *    Rev 1.8   Dec 08 2004 01:47:48   CX17598
 * Changed function fnSearchKeyMap() to map '.' ( scan code remain to 0x002e)
 *  to PCN specific decimal point. -David
* 
*************************************************************************/

/**********************************************************************
        Include Header Files
**********************************************************************/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "datdict.h" 
#include "fwrapper.h"
#include "keypad.h"
#include "oifields.h"
#include "oitpriv.h" 
#include "oicommon.h" 
//#include "ratesvcpublic.h"
#include "unistring.h"
#include "utils.h"



/**********************************************************************
        Local Function Prototypes
**********************************************************************/
/* private prototypes for the entry field engine */
static void fnUpdateBufferFullStatus(void);
static void fnUpdateDisplayBuf(UINT16 *pDispBuf, 
                               UINT16 *pSource,
                               UINT8  bNumTemplateChars, 
                               UINT8  *pTemplateCharLocs);
static UINT8 fnSetEntryBufferJumpTemplate(UINT16 *pTemplate);
static UINT8 fnCalculateCursorPosition(UINT8 bCharIndex);


UINT16 TmpCombiningEntryBuf = 0xFFFF; /* buffer to store the combining character */
static UINT8  bShiftState = 0;

/* there is only one global entry buffer control structure for now.  if the 
   need arises to manage multiple buffers at once with a changing focus, this 
   could be accomodated by using a dynamic list of control structures.*/
ENTRY_BUFFER_CTRL  rEntryBuf;

/* snapshots allow you to buffer a character using library routines */
static ENTRY_BUFFER_CTRL rEntryBufCtrlSnapshot; 

/* evaluate the outcome, and then restore the pre-buffered state if
   he end result is not valid */
static ENTRY_BUFFER   rEntryBufSnapshot;     

/* Allows us to determine whether to allow combining chars for the current
   screen.  The default is yes (TRUE).  For some screens (such as the modem
   init string screen it needs to be FALSE.  Use the API functions in this
   file to turn it on or off */
static BOOL bCombiningCharsEnabled = TRUE;

/* Accented key table */
static COMBINING_CHAR_MAPPING CombiningCharTable[] = {
    {0x0300, 0x0041, 0x00C0, 0 }, // '`', 'A', '�' 
    {0x0300, 0x0061, 0x00E0, 0 }, // '`', 'a', '�'
    {0x0300, 0x0045, 0x00C8, 0 }, // '`', 'E', '�'
    {0x0300, 0x0065, 0x00E8, 0 }, // '`', 'e', '�'
    {0x0300, 0x0049, 0x00CC, 0 }, // '`', 'I', '�'
    {0x0300, 0x0069, 0x00EC, 0 }, // '`', 'i', '�'
    {0x0300, 0x004F, 0x00D2, 0 }, // '`', 'O', '�'
    {0x0300, 0x006F, 0x00F2, 0 }, // '`', 'o', '�'
    {0x0300, 0x0055, 0x00D9, 0 }, // '`', 'U', '�'
    {0x0300, 0x0075, 0x00F9, 0 }, // '`', 'u', '�'
    
    {0x0301, 0x0041, 0x00C1, 0 }, // '�', 'A', '�'
    {0x0301, 0x0061, 0x00E1, 0 }, // '�', 'a', '�'
    {0x0301, 0x0045, 0x00C9, 0 }, // '�', 'E', '�'
    {0x0301, 0x0065, 0x00E9, 0 }, // '�', 'e', '�'
    {0x0301, 0x0049, 0x00CD, 0 }, // '�', 'I', '�'
    {0x0301, 0x0069, 0x00ED, 0 }, // '�', 'i', '�'
    {0x0301, 0x004C, 0x0139, 0 }, // '�', 'L', '?'
    {0x0301, 0x006C, 0x013A, 0 }, // '�', 'l', '?'
    {0x0301, 0x004F, 0x00D3, 0 }, // '�', 'O', '�'
    {0x0301, 0x006F, 0x00F3, 0 }, // '�', 'o', '�'
    {0x0301, 0x0052, 0x0154, 0 }, // '�', 'R', '?'
    {0x0301, 0x0072, 0x0155, 0 }, // '�', 'r', '?'
    {0x0301, 0x0055, 0x00DA, 0 }, // '�', 'U', '�'
    {0x0301, 0x0075, 0x00FA, 0 }, // '�', 'u', '�'
    {0x0301, 0x0059, 0x00DD, 0 }, // '�', 'Y', '�'
    {0x0301, 0x0079, 0x00FD, 0 }, // '�', 'y', '�'
        
    {0x005E, 0x0041, 0x00C2, 1 }, // '^', 'A', '�'
    {0x005E, 0x0061, 0x00E2, 1 }, // '^', 'a', '�'
    {0x005E, 0x0045, 0x00CA, 1 }, // '^', 'E', '�'
    {0x005E, 0x0065, 0x00EA, 1 }, // '^', 'e', '�'
    {0x005E, 0x0049, 0x00CE, 1 }, // '^', 'I', '�'
    {0x005E, 0x0069, 0x00EE, 1 }, // '^', 'i', '�'
    {0x005E, 0x004F, 0x00D4, 1 }, // '^', 'O', '�'
    {0x005E, 0x006F, 0x00F4, 1 }, // '^', 'o', '�'
    {0x005E, 0x0055, 0x00DB, 1 }, // '^', 'U', '�'
    {0x005E, 0x0075, 0x00FB, 1 }, // '^', 'u', '�'
    
    {0x0308, 0x0041, 0x00C4, 0 }, // '�', 'A', '�'
    {0x0308, 0x0061, 0x00E4, 0 }, // '�', 'a', '�'
    {0x0308, 0x0045, 0x00CB, 0 }, // '�', 'E', '�'
    {0x0308, 0x0065, 0x00EB, 0 }, // '�', 'e', '�'
    {0x0308, 0x0049, 0x00CF, 0 }, // '�', 'I', '�'
    {0x0308, 0x0069, 0x00EF, 0 }, // '�', 'i', '�'
    {0x0308, 0x004F, 0x00D6, 0 }, // '�', 'O', '�'
    {0x0308, 0x006F, 0x00F6, 0 }, // '�', 'o', '�'
    {0x0308, 0x0055, 0x00DC, 0 }, // '�', 'U', '�'
    {0x0308, 0x0075, 0x00FC, 0 }, // '�', 'u', '�'
    {0x0308, 0x0079, 0x00FF, 0 }, // '�', 'y', '�'
	
    {0x0327, 0x0043, 0x00C7, 0 }, // '�', 'C', '�'
    {0x0327, 0x0063, 0x00E7, 0 }, // '�', 'c', '�'

    {0x007E, 0x0041, 0x00C3, 0 }, // '~', 'A', '�'
    {0x007E, 0x0061, 0x00E3, 0 }, // '~', 'a', '�'
    {0x007E, 0x004E, 0x00D1, 0 }, // '~', 'N', '�'
    {0x007E, 0x006E, 0x00F1, 0 }, // '~', 'n', '�'
    {0x007E, 0x004F, 0x00D5, 0 }, // '~', 'O', '�'
    {0x007E, 0x006F, 0x00F5, 0 }, // '~', 'o', '�'

    {0x02DA, 0x0055, 0x016E, 0 }, // '?', 'U', '?' - tiny circle above the character
    {0x02DA, 0x0075, 0x016F, 0 }, // '?', 'u', '?'

    {0x030C, 0x0043, 0x010C, 0 }, // '?', 'C', '?' - inverted caret above the character
    {0x030C, 0x0063, 0x010D, 0 }, // '?', 'c', '?'
    {0x030C, 0x0044, 0x010E, 0 }, // '?', 'D', '?'
    {0x030C, 0x0045, 0x011A, 0 }, // '?', 'E', '?'   
    {0x030C, 0x0065, 0x011B, 0 }, // '?', 'e', '?'
    {0x030C, 0x004E, 0x0147, 0 }, // '?', 'N', '?'
    {0x030C, 0x006E, 0x0148, 0 }, // '?', 'n', '?'
    {0x030C, 0x0052, 0x0158, 0 }, // '?', 'R', '?'
    {0x030C, 0x0072, 0x0159, 0 }, // '?', 'r', '?'
    {0x030C, 0x0053, 0x0160, 0 }, // '?', 'S', '?'
    {0x030C, 0x0073, 0x0161, 0 }, // '?', 's', '?'
    {0x030C, 0x0054, 0x0164, 0 }, // '?', 'T', '?'
    {0x030C, 0x005A, 0x017D, 0 }, // '?', 'Z', '?'
    {0x030C, 0x007A, 0x017E, 0 }, // '?', 'z', '?'

    {0x031B, 0x0064, 0x010F, 0 }, // '�', 'd', '?' - apostrophe on the right side of the character
    {0x031B, 0x004C, 0x013D, 0 }, // '�', 'L', '?' - rounded inverted caret above the character
    {0x031B, 0x006C, 0x013E, 0 }, // '�', 'l', '?' - rounded inverted caret above the character
    {0x031B, 0x0074, 0x0165, 0 }, // '�', 't', '?' - apostrophe on the right side of the character

    {END_OF_COMBINING_CHAR_MAP, 0x0000 , 0x0000} };


/*----------------------------------------------------------------*/
/*------    private functions for the entry field engine    ------*/
/*----------------------------------------------------------------*/

/* *************************************************************************
// FUNCTION NAME: 
//          fnUpdateBufferFullStatus
//
// DESCRIPTION:
//          Updates the buffer full flag in the global entry buffer.
//
// INPUTS:
//          None
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//  06/24/2010  Rocky Dai       Set the input field to be left-to-right mode on specific screens for Israel.
//  06/09/2010  Jingwei,Li      Set IP address entry in left to right mode.
//  04/22/2010  Deborah Kohl    For RTL change limit value for loop from 
//                              (rEntryBuf.bLenNonTempl - 1) to rEntryBuf.bLenNonTempl
//  04/13/2010  Jingwei,LI      Modified to support RTL language writting direction.
//          Mozdzer    Initial version 
// *************************************************************************/
static void fnUpdateBufferFullStatus(void)
{
    BOOL  fIsFull = FALSE;
	UINT8       ubWritingDir;
	UINT8 i;

    ubWritingDir = fnGetWritingDir();
    // IP address entry is always in left to right mode.

    switch(ubWritingDir)
    {
        case LEFT_TO_RIGHT:
        /* if overtype mode, buffer is full if the number
           of characters entered equals the buffer size */
        if ( rEntryBuf.fInsert == FALSE)
        {
            if (rEntryBuf.bCharsEntered >= rEntryBuf.bLenNonTempl)
            {
                fIsFull = TRUE;
            }
        }
        else
        {
            /* if insert mode and aligned left, buffer is considered full if
               adding another character will result in a non-space character
               being scrolled off the right edge */
            if (rEntryBuf.fAlignL == TRUE)
            {
                if ( (rEntryBuf.pEbuf->pBuf[rEntryBuf.bLenNonTempl - 1] != UNICODE_SPACE)
                      ||(rEntryBuf.bCharsEntered >= rEntryBuf.bLenNonTempl) )
                {
                    fIsFull = TRUE;
                }
            }
            else        
            {
                  /* if insert mode and aligned right, buffer is full if a non-space
                    character will be scrolled off the left edge */
                if (rEntryBuf.pEbuf->pBuf[0] != UNICODE_SPACE)
                {
                    fIsFull = TRUE;
                }
            }
        }
        rEntryBuf.fFull = fIsFull;
        
    	break;
        case RIGHT_TO_LEFT:
			// if buffer empty, fill with spaces
			if (rEntryBuf.bCharsEntered == 0)
			{
				for(i = 0; i < rEntryBuf.bLenNonTempl; i++)
				{
					rEntryBuf.pEbuf->pBuf[i] = UNICODE_SPACE;
				}

				rEntryBuf.pEbuf->pBuf[rEntryBuf.bLenNonTempl] = 0;
				rEntryBuf.bNextCharIndex = rEntryBuf.bLenNonTempl - 1;
			}

			if (rEntryBuf.pEbuf->pBuf[0] != UNICODE_SPACE)
			{
				fIsFull = TRUE;
			}

			rEntryBuf.fFull = fIsFull;
			break;

		default:
		break;

	}

    return;
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnUpdateDisplayBuf
//
// DESCRIPTION:
//          Takes the current entry buffer and formats it into a display 
//          buffer that might require a template.  For example, if the
//          current entry buffer contains " 130" and the display buffer
//          template is " : ", this function would build the string " 1:30"
//
// INPUTS:
//          pDispBuf - pointer to the destination Unicode buffer.  this buffer
//                     should contain the template characters (if there are
//                     any) in the positions marked off with pTemplateCharLocs.
//          pSource - pointer to the source Unicode buffer that contains the
//                    characters entered by the user.  must be null-terminated.
//          bNumTemplateChars - the number of characters within pDispBuf that
//                              are considered template characters. if it is 0, 
//                              pDispBuf will be identical to pSource after
//                              the function runs
//          pTemplateCharLocs - pointer to an array of bytes that specify the
//                              locations of the template characters
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          Mozdzer  Initial version 
// *************************************************************************/
static void fnUpdateDisplayBuf(UINT16 *pDispBuf, 
                               UINT16 *pSource,
                               UINT8   bNumTemplateChars, 
                               UINT8  *pTemplateCharLocs)
{
    UINT32  i = 0;      /* the source index */
    UINT32  j = 0;      /* the destination index */
    UINT32  k;          /* loop variable for looking in the pTemplateCharLocs array */
    UINT16  usUnicode;
    BOOL    fCollision;

    while ((usUnicode = pSource[i]) != (UINT16) 0)
    {
        do
        {
            /* is the destination index on a template character? */
            for (k = 0, fCollision = FALSE; k < bNumTemplateChars; k++)
                if (j == pTemplateCharLocs[k])
                {
                    /* yes... increment the destination index and try again */
                    fCollision = TRUE;
                    j++;
                    break;
                }
        } while (fCollision == TRUE);

        /* when we've found a non-template character location in the destination buffer,
          copy the source character to that location and then repeat for the next character */
        pDispBuf[j++] = usUnicode;
        i++;
    }

    return;
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnSetEntryBufferJumpTemplate
//
// DESCRIPTION:
//          Private function used to set up an entry buffer jump template.
//          An entry buffer jump template is used to format
//          the displayable version of the buffer string.  For
//          example, if the user is entering a time, we might want the
//          buffer to have the template "  :  " so that the hours and
//          minutes are always separated by a colon while typing.
//
// INPUTS:
//          pTemplate - pointer to null terminated template string.  any
//                      non space characters are considered part of the 
//                      template.  note that the maximum number of typable
//                      characters is reduced by the number of characters
//                      in the template.
//
// RETURNS:
//          The current shift state
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          Mozdzer  Initial version
// *************************************************************************/
static UINT8 fnSetEntryBufferJumpTemplate(UINT16 *pTemplate)
{
    UINT16  usTemplUnicode;
    UINT32  i = 0;
    UINT8   ucNumTemplChars = 0;

    /* go through every character of the template string, initializing
       the displayable buffer and list of template character locations
       as we go.  stop when we hit a null in the template string or we
       have processed as many characters as the full length of the buffer */
    while (((usTemplUnicode = pTemplate[i]) != (UINT16) 0) && (i < rEntryBuf.bLen))
    {
        /* move a character from the template string to the displayable 
           buffer to intialize it */
        rEntryBuf.pEbuf->pDispBuf[i] = usTemplUnicode;

        /* if the character from the template string is not a space, it
           must be a template character, so save its position in the
           array in the entry buffer control structure */
        if (usTemplUnicode != UNICODE_SPACE)
        {
            rEntryBuf.pTemplateCharLoc[ucNumTemplChars] = i;
            ucNumTemplChars++;
        }
        
        i++;
    }

    /* the template string may have been shorter than the displayable
       buffer needs to be, so pad it with spaces before slapping on
       the null terminator.  */
    while (i < rEntryBuf.bLen)
    {
        rEntryBuf.pEbuf->pDispBuf[i] = UNICODE_SPACE;
        i++;
    }
    
    rEntryBuf.pEbuf->pDispBuf[i] = (UINT16) 0;

    return (ucNumTemplChars);   
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnCalculateCursorPosition
//
// DESCRIPTION:
//          Private function that calculates the cursor position in the  
//          displayable buffer based on the next character index within
//          the character entry buffer.  Since the character entry buffer
//          (which actually stores the characters entered) is always  
//          kept in a packed format, and the displayable buffer may need
//          to be formatted with template characters, this is more
//          complicated than it may appear to be.
//
// INPUTS:
//          bCharIndex- the index of the next character insertion point 
//                      within the character entry buffer (rEntryBuf.pEbuf->pBuf).
//
// RETURNS:
//          the cursor position within the displayble buffer(rEntryBuf.pEbuf->pDispBuf)
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          Mozdzer    Initial version 
// *************************************************************************/
static UINT8 fnCalculateCursorPosition(UINT8 bCharIndex)
{
    UINT32  i;
    UINT32  j;
    BOOL    fCollision;

    /* i marks the buffer position, which will eventually be returned */
    for (i = 0; i < rEntryBuf.bLen; i++)
    {
        /* check if position in the buffer we are processing (i) is occupied 
           by a template character */
        for (j = 0, fCollision = FALSE; j < rEntryBuf.bNumTemplateChars; j++)
        {
            if (rEntryBuf.pTemplateCharLoc[j] == i)
            {
                fCollision = TRUE;
                break;
            }
        }
        /* if it is not occupied, check if bCharIndex is 0.  the value of bCharIndex is equivalent
           to saying "find the (bCharIndex+1)'th non-template location in the displayable buffer".  if
           we have no more spaces to find, (bCharIndex = 0), we are done.  otherwise, decrement
           bCharIndex because we now have one fewer locations to find.*/
        if (fCollision == FALSE)
        {
            if (bCharIndex == 0)
            {
                break;
            }
            else
            {
                bCharIndex--;
            }
        }
    }

    return ((UINT8) i);
}


/*----------------------------------------------------------------*/
/*------              ENTRY FIELD ENGINE                    ------*/
/*----------------------------------------------------------------*/


// *************************************************************************
// FUNCTION NAME: 
//      fnSwitchOffCharacterCombinations
//
// DESCRIPTION:
//      Switches off character combinations for those accent characters which
//      are valid on their own in certain circumstances.  Use this in the pre-
//      function for any screen containing an entry field which should not
//      allow these characters to be combined.  PLEASE remember to switch it
//      back on again in the post function of the screen because this is
//      exceptional behaviour - the default is on.
//
// INPUTS:
//      None.
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      PLEASE remember to switch it back on
//      again in the post function of the screen because this is exceptional
//      behaviour - the default is on.
//
// MODIFICATION  HISTORY:
//      12 Jun 2006 Simon Fox   Initial version
// *************************************************************************
void fnSwitchOffCharacterCombinations(void)
{
	bCombiningCharsEnabled = FALSE;
}


// *************************************************************************
// FUNCTION NAME: 
//      fnSwitchOnCharacterCombinations
//
// DESCRIPTION:
//      Switches all character combinations back on if they have previously been
//      switched off.  Use this in the post-function of any screen which calls
//      fnSwitchOffCharacterCombinations in the pre-function.
//
// INPUTS:
//      None.
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION  HISTORY:
//      12 Jun 2006 Simon Fox   Initial version
// *************************************************************************
void fnSwitchOnCharacterCombinations(void)
{
	bCombiningCharsEnabled = TRUE;
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnCreateEntryBuffer
//
// DESCRIPTION:
//          Sets up an entry buffer with the given attributes.  The entry
//      field engine is responsible for interpreting keycodes and
//      inserting them as Unicode characters into a specified
//      memory area.  The location of the blinking cursor within
//      the entry buffer is also managed by the engine.
//
// INPUTS:
//      pEbuf - pointer to entry buffer structure in which to store
//                  the Unicode characters and the cursor information.  
//                  the unicode buffer portion of the structure should
//                  be pre-allocated and initialized to either a 
//                  context-specific initial value or spaces.  a null
//                  terminator must be present at the end of the
//                  buffer.
//          bLen - length of the buffer, NOT including the null terminator
//      fAlignL - true if the buffer should be aligned left, false if
//              aligned right
//      fInsert - true if buffer uses insertion mode (characters are
//              pushed along rather than overwritten), false for
//              overtype mode
//      fCursorOn - true if blinking cursor should be used to denote
//                  location of next character entered
//          pTemplate - Input Template as provided by caller
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//         06/24/2010 Rocky Dai  Added an exception when setting bNextCharIndex for Israel.
//         06/09/2010 Jingwei,Li Set IP address entry buffer to LTR mode.
//         04/21/2010 Jingwei,Li Updated for RTL.
//         12/05/2005 Adam Liu  Adapted for FuturePhoenix
//                           Mozdzer    Initial version 
// *************************************************************************/
void fnCreateEntryBuffer( ENTRY_BUFFER *pEbuf, 
                          UINT8         bLen, 
                          BOOL          fAlignL, 
                          BOOL          fInsert,
                          BOOL          fCursorOn,
                          UINT16       *pTemplate )
{

    UINT8 ubWritingDir;
    ubWritingDir = fnGetWritingDir();

    /* make sure this we have valid pointer and length, since an error could be disastrous */
    if ((pEbuf == NULL) || (bLen == 0))
    {
        fnDisplayErrorDirect("Attempt to create invalid entry field", 0, OIT);
    }

    /* setup our global entry buffer structure */
    rEntryBuf.pEbuf = pEbuf;
    rEntryBuf.bLen = bLen;
    rEntryBuf.fAlignL = fAlignL;
    rEntryBuf.fInsert = fInsert;
    rEntryBuf.fCursorOn = fCursorOn;
    rEntryBuf.bCharsEntered = 0;
    rEntryBuf.fFull = false;
    rEntryBuf.fEnabled = true;

    rEntryBuf.bNumTemplateChars = fnSetEntryBufferJumpTemplate(pTemplate);

    rEntryBuf.bLenNonTempl = bLen - rEntryBuf.bNumTemplateChars;

    /* initialize the index where the next character will go.  this depends on 
        the alignment of the field */
    if (fAlignL == true)
        rEntryBuf.bNextCharIndex = 0;
    else
        rEntryBuf.bNextCharIndex = rEntryBuf.bLenNonTempl - 1;

    if(( ubWritingDir == RIGHT_TO_LEFT ))
    {
        rEntryBuf.bNextCharIndex = rEntryBuf.bLenNonTempl - 1;
    } 

    /* ensure there is a NULL at the correct location in the entry buffer.  this must
        be done after checking the template because we don't know how many characters
        of the buffer will be available for typing otherwise */
    rEntryBuf.pEbuf->pBuf[rEntryBuf.bLenNonTempl] = (unsigned short) NULL;


    /* initialize the cursor position */
    if (fCursorOn == true)
    {
        rEntryBuf.pEbuf->bCursorLoc = fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);
        if (rEntryBuf.fInsert == true)
            rEntryBuf.pEbuf->bCursorStyle = LINE_CURSOR;
        else
            rEntryBuf.pEbuf->bCursorStyle = BLOCK_CURSOR;
    }
    else
        rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;

    /* reset the shift state to "unshifted".  (prevents shift state from carrying over from a previous buffer) */
    bShiftState = 0;

    fnUpdateDisplayBuf(rEntryBuf.pEbuf->pDispBuf, rEntryBuf.pEbuf->pBuf, 
                        rEntryBuf.bNumTemplateChars, rEntryBuf.pTemplateCharLoc);

    return; 
}



/* *************************************************************************
// FUNCTION NAME: 
//         fnEntryBufferSnapshotCapture
//
// DESCRIPTION:
//         Captures all entry buffer data so it can be later restored
//         to the exact state using fnEntryBufferSnapshotRestore.
//
// INPUTS:
//         None
//
// RETURNS:
//         None
//
// WARNINGS/NOTES:
//         None
//
// MODIFICATION  HISTORY:
//         Mozdzer      Initial version
// *************************************************************************/
void fnEntryBufferSnapshotCapture(void)
{
    memcpy(&rEntryBufCtrlSnapshot, &rEntryBuf, sizeof(rEntryBuf));
    memcpy(&rEntryBufSnapshot, rEntryBuf.pEbuf, sizeof(ENTRY_BUFFER));
        
    return;
}



/* *************************************************************************
// FUNCTION NAME: 
//        fnEntryBufferSnapshotRestore
//
// DESCRIPTION:
//        Restores the current entry buffer to its exact state when
//        fnEntryBufferSnapshotCapture was last called.  Caller
//        must be careful to make sure the capture function was
//        in fact called before using this function.
//
// INPUTS:
//        None
//
// RETURNS:
//        None
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//        Mozdzer   Initial version  
// *************************************************************************/
void fnEntryBufferSnapshotRestore(void)
{
    memcpy(&rEntryBuf, &rEntryBufCtrlSnapshot, sizeof(rEntryBuf));
    memcpy(rEntryBuf.pEbuf, &rEntryBufSnapshot, sizeof(ENTRY_BUFFER));

    return;
}



/* *************************************************************************
// FUNCTION NAME: 
//       fnDeleteCharacter
//
// DESCRIPTION:
//       Deletes the last character from the currently established
//       entry buffer.  If no characters were entered, this function
//       has no effect.
//
// INPUTS:
//       None
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       This function works only for entry buffers that
//       are in insert mode.  If that is not the buffer
//       configuration, this function has no effect.
//
// MODIFICATION  HISTORY:
//      06/24/2010  Rocky Dai       Consider it to be LTR mode when we're on screens that require
//                                  sorting and searching items. (to fix Fraca for Israel)
//      04/13/2010  Jingwei,LI      Modified to support RTL language writting direction.
//       Mozdzer   Initial version   
// *************************************************************************/
void fnDeleteCharacter(void)
{
    UINT8 ubWritingDir;
    ubWritingDir = fnGetWritingDir();


    switch(ubWritingDir)
    {
        case LEFT_TO_RIGHT:
            fnDeleteCharacterLTR();
            break;
        case RIGHT_TO_LEFT:
            fnDeleteCharacterRTL();
            break;
        default:
            break;
    }


    return;
}


/* *************************************************************************
// FUNCTION NAME:
//       fnDeleteCharacterLTR
//
// DESCRIPTION:
//       In LTR language writting direction,deletes the last character from 
//       the currently established  entry buffer.  If no characters were 
//       entered, this function has no effect. 
//
// INPUTS:
//       None
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       This function works only for entry buffers that
//       are in insert mode.  If that is not the buffer
//       configuration, this function has no effect.
//
// MODIFICATION  HISTORY:
//      04/13/2010  Jingwei,LI   Initial version(based on fnDeleteCharacter)
// *************************************************************************/
void fnDeleteCharacterLTR(void)
{

    if((rEntryBuf.fEnabled) == FALSE)
    {
        rEntryBuf.fEnabled = TRUE;
    }
    
    if( (rEntryBuf.bCharsEntered > 0) && (rEntryBuf.fInsert == TRUE) 
        && (rEntryBuf.fEnabled == TRUE) )
    {
        if (rEntryBuf.fAlignL == TRUE)
        {
            rEntryBuf.bCharsEntered--;
            rEntryBuf.bNextCharIndex--;
            rEntryBuf.pEbuf->pBuf[rEntryBuf.bNextCharIndex] = UNICODE_SPACE;
            rEntryBuf.fFull = FALSE;
        }
        else
        {
            /* if aligned right shift the whole buffer 1 space to the right */
            memmove( rEntryBuf.pEbuf->pBuf+1, rEntryBuf.pEbuf->pBuf,
                    (rEntryBuf.bLenNonTempl-1) * sizeof(UINT16));
            rEntryBuf.pEbuf->pBuf[0] = UNICODE_SPACE;   /* put a space in the vacant spot */
            rEntryBuf.bCharsEntered--;
            rEntryBuf.fFull = FALSE;
        } 

        /* update the cursor position/status */

        if (rEntryBuf.fCursorOn != TRUE)
        {
            /* report that the cursor is inactive if it is not used at all for this entry buffer */
            rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;
        }
        else
        {
            /* if the buffer is disabled or full */
            if (rEntryBuf.fFull == TRUE)
            {
                rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
            }
            else
            {
                rEntryBuf.pEbuf->bCursorLoc = fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);
            }
        }

        /* Clear the combining char buf */
        TmpCombiningEntryBuf = 0xFFFF; 

        fnUpdateDisplayBuf( rEntryBuf.pEbuf->pDispBuf, rEntryBuf.pEbuf->pBuf, 
                            rEntryBuf.bNumTemplateChars, rEntryBuf.pTemplateCharLoc);

        fnChangeShiftState(0);  /* after removing the character, reset shift state to 0 */

    }

    return;
}


/* *************************************************************************
// FUNCTION NAME:
//       fnDeleteCharacterRTL
//
// DESCRIPTION:
//       In RTL language writting direction,deletes the last character from 
//       the currently established  entry buffer.  If no characters were 
//       entered, this function has no effect. 
//
// INPUTS:
//       None
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       This function works only for entry buffers that
//       are in insert mode.  If that is not the buffer
//       configuration, this function has no effect.
//
// MODIFICATION  HISTORY:
//      04/14/2010  Jingwei,Li   Update to fix fraca.
//      04/13/2010  Jingwei,LI   Initial version(based on fnDeleteCharacter)
// *************************************************************************/
void fnDeleteCharacterRTL(void)
{
    INT16 i;

    if((rEntryBuf.fEnabled) == FALSE)
    {
        rEntryBuf.fEnabled = TRUE;
    }


    if( (rEntryBuf.bCharsEntered > 0) && (rEntryBuf.fInsert == TRUE)
        && (rEntryBuf.fEnabled == TRUE) )
    {
    /*
        rEntryBuf.bNextCharIndex = rEntryBuf.bLenNonTempl - rEntryBuf.bCharsEntered;
        rEntryBuf.bCharsEntered--;
        rEntryBuf.pEbuf->pBuf[rEntryBuf.bNextCharIndex] = UNICODE_SPACE;
        rEntryBuf.fFull = FALSE;*/
        
        rEntryBuf.pEbuf->pBuf[rEntryBuf.bNextCharIndex] = UNICODE_SPACE;
        for(i = rEntryBuf.bNextCharIndex;i > 0;i--)
        {
            rEntryBuf.pEbuf->pBuf[i] = rEntryBuf.pEbuf->pBuf[i - 1];
        }
        
        if(rEntryBuf.bNextCharIndex <= rEntryBuf.bLenNonTempl -rEntryBuf.bCharsEntered )
        {
           //delete from the left-most side
           rEntryBuf.bNextCharIndex++;
        }
        else
        { 
            rEntryBuf.pEbuf->pBuf[rEntryBuf.bLenNonTempl -rEntryBuf.bCharsEntered] = UNICODE_SPACE;
        }

        rEntryBuf.bCharsEntered--;
        rEntryBuf.fFull = FALSE;

        /* update the cursor position/status */

        if (rEntryBuf.fCursorOn != TRUE)
        {
            /* report that the cursor is inactive if it is not used at all for this entry buffer */
            rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;
        }
        else
        {
            /* if the buffer is disabled or full */
            if (rEntryBuf.fFull == TRUE)
            {
                rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
            }
            else 
            {
                rEntryBuf.pEbuf->bCursorLoc = fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);
                if(rEntryBuf.bNextCharIndex > rEntryBuf.bLenNonTempl -1)
                {
                    //delete the last character, don't need move cursor.
                    rEntryBuf.pEbuf->bCursorLoc --;
                }
            }
        }

        /* Clear the combining char buf */
        TmpCombiningEntryBuf = 0xFFFF;

        fnUpdateDisplayBuf( rEntryBuf.pEbuf->pDispBuf, rEntryBuf.pEbuf->pBuf,
                            rEntryBuf.bNumTemplateChars, rEntryBuf.pTemplateCharLoc);

        fnChangeShiftState(0);  /* after removing the character, reset shift state to 0 */

    }

    return;
}


/* *************************************************************************
// FUNCTION NAME: 
//       fnDeleteCharacterScroll
//
// DESCRIPTION:
//       Deletes the last character from the currently established
//       entry buffer.  If no characters were entered, this function
//       has no effect.
//
// INPUTS:
//       None
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       This function works only for entry buffers that
//       are in OverWrite mode.  If that is not the buffer
//       configuration, this function has no effect.
//
// MODIFICATION  HISTORY:
//      04/23/2010  Jingwei,LI      Modified to support RTL language writting direction.
//       09/15/2006    Raymond Shen  Modified it to deal with cursor's insert mode.
//       04/10/2006    Kan / Oscar   Initial version   
// *************************************************************************/
void fnDeleteCharacterScroll()
{
	 UINT8 ubWritingDir;
	ubWritingDir = fnGetWritingDir();

	switch(ubWritingDir)
	{
		case LEFT_TO_RIGHT:
			fnDeleteCharacterScrollLTR();
			break;
		case RIGHT_TO_LEFT:
			fnDeleteCharacterScrollRTL();
			break;
		default:
		break;
	}


    return;
	return;
}
/* *************************************************************************
// FUNCTION NAME:
//       fnDeleteCharacterScrollLTR
//
// DESCRIPTION:
//       Deletes the last character from the currently established
//       entry buffer.  If no characters were entered, this function
//       has no effect.
//
// INPUTS:
//       None
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       This function works only for entry buffers that
//       are in OverWrite mode.  If that is not the buffer
//       configuration, this function has no effect.
//
// MODIFICATION  HISTORY:
//      04/23/2010  Jingwei,LI   Initial version(based on fnDeleteCharacterScroll)
// *************************************************************************/
void fnDeleteCharacterScrollLTR()
{
	unsigned char i;
	
	if (!(rEntryBuf.fEnabled))
		rEntryBuf.fEnabled = TRUE;
	
	if (( rEntryBuf.bNextCharIndex <= rEntryBuf.bCharsEntered ) 
		&& (rEntryBuf.bCharsEntered > 0) &&  rEntryBuf.fEnabled)
	{
		if (rEntryBuf.fAlignL)
		{
		    if( rEntryBuf.fInsert == 0)
            {      
                if(  (rEntryBuf.bNextCharIndex == rEntryBuf.bCharsEntered) 
	 	      	  &&  (rEntryBuf.pEbuf->bCursorLoc == rEntryBuf.bNextCharIndex) ) 
                {
				    fnMoveCursorLeft();
	 	      	}
			    else
			    {
				    rEntryBuf.bCharsEntered--;
				    for( i=rEntryBuf.bNextCharIndex; i<rEntryBuf.bCharsEntered;i++)
                    {            
					    rEntryBuf.pEbuf->pBuf[i] = rEntryBuf.pEbuf->pBuf[i+1];
                    }

                    if(rEntryBuf.bNextCharIndex > 0)
                    {
                        rEntryBuf.bNextCharIndex--;   
				    }
                 }
            }
            else if( rEntryBuf.fInsert == 1 )
            {
                
                if( rEntryBuf.bNextCharIndex == rEntryBuf.bCharsEntered )
                {
                    if(rEntryBuf.bNextCharIndex > 0)
                    {
                        rEntryBuf.pEbuf->pBuf[rEntryBuf.bNextCharIndex] = 0;
                        rEntryBuf.bNextCharIndex--;
                        rEntryBuf.bCharsEntered--;
                    }
                }
            }

				rEntryBuf.pEbuf->pBuf[rEntryBuf.bCharsEntered] = UNICODE_SPACE;
				rEntryBuf.fFull = false;
		}

    	/* update the cursor position/status */

    	if (rEntryBuf.fCursorOn != true)
    		/* report that the cursor is inactive if it is not used at all for this entry buffer */
    		rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;
    	else
    	{
    		/* if the buffer is disabled or full */
    		if (rEntryBuf.fFull == true)
    			rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
    		else
    			rEntryBuf.pEbuf->bCursorLoc = fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);

    	}

    	/* Clear the combining char buf */
    	TmpCombiningEntryBuf = 0xFFFF; 

    	fnUpdateDisplayBuf(rEntryBuf.pEbuf->pDispBuf, rEntryBuf.pEbuf->pBuf, 
    						rEntryBuf.bNumTemplateChars, rEntryBuf.pTemplateCharLoc);

    	fnChangeShiftState(0);	/* after removing the character, reset shift state to 0 */

    }
	return;
}
/* *************************************************************************
// FUNCTION NAME:
//       fnDeleteCharacterScrollRTL
//
// DESCRIPTION:
//       Deletes the last character from the currently established
//       entry buffer.  If no characters were entered, this function
//       has no effect.
//
// INPUTS:
//       None
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       This function works only for entry buffers that
//       are in insert mode.  If that is not the buffer
//       configuration, this function has no effect.
//
// MODIFICATION  HISTORY:
//      04/29/2010  Deborah Kohl Remove restriction for insert mode only
//                  otherwise we can't change the distributor settings when
//                  in RTL mode
//      04/23/2010  Jingwei,LI   Initial version(based on fnDeleteCharacterScroll)
// *************************************************************************/
void fnDeleteCharacterScrollRTL(void)
{
    INT16 i;

    if((rEntryBuf.fEnabled) == FALSE)
    {
        rEntryBuf.fEnabled = TRUE;
    }

    if( (rEntryBuf.bCharsEntered > 0) 
        && (rEntryBuf.fEnabled == TRUE) )
    {
        rEntryBuf.pEbuf->pBuf[rEntryBuf.bNextCharIndex] = UNICODE_SPACE;
        for(i = rEntryBuf.bNextCharIndex;i > 0;i--)
        {
            rEntryBuf.pEbuf->pBuf[i] = rEntryBuf.pEbuf->pBuf[i - 1];
        }
        
        if(rEntryBuf.bNextCharIndex <= rEntryBuf.bLenNonTempl -rEntryBuf.bCharsEntered )
        {
           //delete from the left-most side
           rEntryBuf.bNextCharIndex++;
        }
        else
        { 
            rEntryBuf.pEbuf->pBuf[rEntryBuf.bLenNonTempl -rEntryBuf.bCharsEntered] = UNICODE_SPACE;
        }

        rEntryBuf.bCharsEntered--;
        rEntryBuf.fFull = FALSE;
        /* update the cursor position/status */

        if (rEntryBuf.fCursorOn != TRUE)
        {
            /* report that the cursor is inactive if it is not used at all for this entry buffer */
            rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;
        }
        else
        {
            /* if the buffer is disabled or full */
            if (rEntryBuf.fFull == TRUE)
            {
                rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
            }
            else
            {
                rEntryBuf.pEbuf->bCursorLoc = fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);
                if(rEntryBuf.bNextCharIndex > rEntryBuf.bLenNonTempl -1)
                {
                    //delete the last character, don't need move cursor.
                    rEntryBuf.pEbuf->bCursorLoc --;
                }
            }
        }

        /* Clear the combining char buf */
        TmpCombiningEntryBuf = 0xFFFF;

        fnUpdateDisplayBuf( rEntryBuf.pEbuf->pDispBuf, rEntryBuf.pEbuf->pBuf,
                            rEntryBuf.bNumTemplateChars, rEntryBuf.pTemplateCharLoc);

        fnChangeShiftState(0);  /* after removing the character, reset shift state to 0 */

    }

    return;
}


/* *************************************************************************
// FUNCTION NAME: fnDeleteCharacterOverLap
// DESCRIPTION: Deletes the character cursor pointered from the currently established
//				entry buffer.  If no characters were entered, this function
//				has no effect.
//				Note: This function works only for entry buffers that are
//						in overlap mode and Align left.  If that is not the buffer
//						configuration, this function has no effect.
//
// AUTHOR: Joe Mozdzer/ David Huang
// INPUTS:	none
// *************************************************************************/
void fnDeleteCharacterOverLap()
{
	unsigned char i;
	
	if (!(rEntryBuf.fEnabled))
		rEntryBuf.fEnabled = TRUE;
	
	if (( rEntryBuf.bNextCharIndex <= rEntryBuf.bCharsEntered ) && (rEntryBuf.bNextCharIndex > 0) && !rEntryBuf.fInsert && rEntryBuf.fEnabled)
	{
		if (rEntryBuf.fAlignL)
		{
	 	      if(  (rEntryBuf.bNextCharIndex == rEntryBuf.bCharsEntered) &&  (rEntryBuf.pEbuf->bCursorLoc == rEntryBuf.bNextCharIndex) ) 
				fnMoveCursorLeft();
			else
			{
				 rEntryBuf.bCharsEntered--;
				 for( i=rEntryBuf.bNextCharIndex-1; i<rEntryBuf.bCharsEntered;i++)
					rEntryBuf.pEbuf->pBuf[i] = rEntryBuf.pEbuf->pBuf[i+1];

				 rEntryBuf.bNextCharIndex--;   									// By David
			 	if( rEntryBuf.bNextCharIndex==0 ) rEntryBuf.bNextCharIndex =1;   // By David

				rEntryBuf.pEbuf->pBuf[rEntryBuf.bCharsEntered] = UNICODE_SPACE;
				rEntryBuf.fFull = false;
			}
		}
		else
		{
//			/* if aligned right shift the whole buffer 1 space to the right */
//			memmove(rEntryBuf.pEbuf->pBuf + 1, rEntryBuf.pEbuf->pBuf, (rEntryBuf.bLenNonTempl - 1) * sizeof(unsigned short));
//			rEntryBuf.pEbuf->pBuf[0] = UNICODE_SPACE;	/* put a space in the vacant spot */
//			rEntryBuf.bCharsEntered--;
//			rEntryBuf.fFull = false;
		} 

		/* update the cursor position/status */

		if (rEntryBuf.fCursorOn != true)
			/* report that the cursor is inactive if it is not used at all for this entry buffer */
			rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;
		else
		{
			/* if the buffer is disabled or full */
			if (rEntryBuf.fFull == true)
				rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
			else
				rEntryBuf.pEbuf->bCursorLoc = fnCalculateCursorPosition(rEntryBuf.bNextCharIndex-1);

		}

		/* Clear the combining char buf */
		TmpCombiningEntryBuf = 0xFFFF; 

		fnUpdateDisplayBuf(rEntryBuf.pEbuf->pDispBuf, rEntryBuf.pEbuf->pBuf, 
							rEntryBuf.bNumTemplateChars, rEntryBuf.pTemplateCharLoc);

		fnChangeShiftState(0);	/* after removing the character, reset shift state to 0 */

	}

	return;
}



/* *************************************************************************
// FUNCTION NAME: 
//       fnMoveCursorLeft
//
// DESCRIPTION:
//       Based on the fnDeleteCharacter(). Move the cursor left without 
//       deleting the last char.
//
// INPUTS:
//       None
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       Only used for overwrite mode.
//
// MODIFICATION  HISTORY:
//       04/29/2010   Jingwei,Li    Modified to fix cursor move issue for RTL.
//       09/15/2006   Raymond Shen  Modified it to deal with cursor's insert mode.
//       04/07/2006   Kan           Modified
//       12/05/2005 Adam Liu     Adapted for FuturePhoenix
//                  Tim Zhang   Initial version   
// *************************************************************************/
void fnMoveCursorLeft(void)
{
    UINT8   ubWritingDir;
    UINT16  wCursorOffset = 0;

    ubWritingDir = fnGetWritingDir();

    if (rEntryBuf.fEnabled == FALSE)
    {   
        rEntryBuf.fEnabled = TRUE;
    }
    
    if ((rEntryBuf.bNextCharIndex > 0) //&& (rEntryBuf.fInsert == FALSE) 
           && (rEntryBuf.fEnabled == TRUE) && (rEntryBuf.fCursorOn == TRUE))
    {
        wCursorOffset = rEntryBuf.bLenNonTempl - rEntryBuf.pEbuf->bCursorLoc + 1;
        if((ubWritingDir == RIGHT_TO_LEFT ) && (wCursorOffset > rEntryBuf.bCharsEntered))
        {
            return;
        }
        
        if (rEntryBuf.fAlignL == TRUE)
        {    
             rEntryBuf.bNextCharIndex--;
        }

        /* update the cursor position/status */
        rEntryBuf.pEbuf->bCursorLoc = fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);

        /* Clear the combining char buf */
        TmpCombiningEntryBuf = 0xFFFF; 

        fnUpdateDisplayBuf(rEntryBuf.pEbuf->pDispBuf, rEntryBuf.pEbuf->pBuf, 
                        rEntryBuf.bNumTemplateChars, rEntryBuf.pTemplateCharLoc);

        fnChangeShiftState(0);  /* after removing the character, reset shift state to 0 */

    }

    return;
}


/* *************************************************************************
// FUNCTION NAME: 
//       fnMoveCursorRight
//
// DESCRIPTION:
//       Based on the fnDeleteCharacter(). Move the cursor right without 
//       deleting the last char.
//
// INPUTS:
//       None
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       Only used for overwrite mode.
//
// MODIFICATION  HISTORY:
//       03/20/2014   Wang Biao   Modified to fix fraca 224734.
//       04/29/2010   Jingwei,Li    Modified to fix cursor move issue for RTL.
//       09/15/2006   Raymond Shen  Modified it to fix a problem of cursor's insert mode.
//       09/11/2006   Kan Jiang   Modified to fix a problem of Cursor displaying. 
//       04/07/2006   Kan Jiang   Modified
//       12/05/2005   Adam Liu    Adapted for FuturePhoenix
//                    Tim Zhang   Initial version   
// *************************************************************************/
void fnMoveCursorRight(void)
{
    UINT8   ubWritingDir;
    UINT16  wCursorOffset = 0;

    ubWritingDir = fnGetWritingDir();


    if (rEntryBuf.fEnabled == FALSE)
    {   
        rEntryBuf.fEnabled = TRUE;
    }
    if(ubWritingDir == RIGHT_TO_LEFT)
    {
        if((rEntryBuf.bNextCharIndex < rEntryBuf.bLenNonTempl -1)
            &&  (rEntryBuf.fEnabled == TRUE) && (rEntryBuf.fCursorOn == TRUE))
        {
           if ((rEntryBuf.fAlignL == TRUE)
                &&(rEntryBuf.bNextCharIndex+1 < rEntryBuf.bLenNonTempl))
            {
                rEntryBuf.bNextCharIndex++;
            }

            rEntryBuf.pEbuf->bCursorLoc = 
                        fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);
        }
    }
    else
    {
        if ((rEntryBuf.bNextCharIndex < rEntryBuf.bCharsEntered ) 
                &&  (rEntryBuf.fEnabled == TRUE) && (rEntryBuf.fCursorOn == TRUE))
        {
            if ((rEntryBuf.fAlignL == TRUE)
                &&(rEntryBuf.bNextCharIndex+1 <= rEntryBuf.bCharsEntered))
                //The cursor can be moved to right to the next location of last char entered.
            {
                rEntryBuf.bNextCharIndex++;
            }
            if(rEntryBuf.bNextCharIndex < rEntryBuf.bLenNonTempl)
            {
                rEntryBuf.pEbuf->bCursorLoc = 
                            fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);
            }
            else
            {
                rEntryBuf.bNextCharIndex--; 
            }
        }
    }

    //else if ((rEntryBuf.bNextCharIndex >= rEntryBuf.bCharsEntered ) &&  
    //                (rEntryBuf.fEnabled == TRUE) && (rEntryBuf.fCursorOn == TRUE))
    //     {   
    //        rEntryBuf.pEbuf->bCursorLoc = 
    //                           fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);
    //     }

    /* Clear the combining char buf */
    TmpCombiningEntryBuf = 0xFFFF; 

    fnUpdateDisplayBuf(rEntryBuf.pEbuf->pDispBuf, rEntryBuf.pEbuf->pBuf, 
                   rEntryBuf.bNumTemplateChars, rEntryBuf.pTemplateCharLoc);

    fnChangeShiftState(0);  /* after removing the character, reset shift state to 0 */

    return;
}



/* *************************************************************************
// FUNCTION NAME:
//       fnGetWritingDir
//
// DESCRIPTION:
//       Get current language writting direction.
//
// INPUTS:
//
//
// RETURNS:
//       Writing direction
//
// WARNINGS/NOTES:
//       None
//
// MODIFICATION  HISTORY:
//    2010-04-13   Jingwei,Li    Initial version
// *************************************************************************/
UINT8 fnGetWritingDir()
{
	UINT8       ubWritingDir;

	UINT8       ubLanguage;

	ubLanguage = CMOSSetupParams.Language;

	ubWritingDir = fnFlashGetByteParm(BP_LANGUAGE_WRITING_DIR);

    ubWritingDir = (ubWritingDir >> ubLanguage) & 0x01;

    return ubWritingDir;

}




/* *************************************************************************
// FUNCTION NAME:
//       fnBufferKey
//
// DESCRIPTION:
//       Takes a keycode as input and adds it to the currently established
//       entry buffer in accordance with the way the field is set up.
//
// INPUTS:
//       bKeyCode
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       None
//
// MODIFICATION  HISTORY:

//      06/24/2010  Rocky Dai       Consider it to be LTR mode when we're on screens that require
//                                  sorting and searching items. (to fix Fraca for Israel)      
//      04/13/2010  Jingwei,LI      Modified to support RTL language writting direction.
//       Mozdzer       Initial version    
// *************************************************************************/
void fnBufferKey(UINT8 bKeyCode)
{
	UINT8       ubWritingDir;

	ubWritingDir = fnGetWritingDir();

    if (fnIsSortListInput() == TRUE)
    {
        ubWritingDir = LEFT_TO_RIGHT;
    }

	switch(ubWritingDir)
	{
		case LEFT_TO_RIGHT:
			fnBufferKeyLTR(bKeyCode);
			break;

		case RIGHT_TO_LEFT:
			fnBufferKeyRTL(bKeyCode);
			break;

		default:
		break;

	}

    return;

}



/* *************************************************************************
// FUNCTION NAME:
//       fnBufferKeyLTR
//
// DESCRIPTION:
//       In LTR language writting direction,takes a keycode as input and adds it
//       to the currently establishe dentry buffer in accordance with the way
//       the field is set up.
//
// INPUTS:
//       bKeyCode
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       None
//
// MODIFICATION  HISTORY:
//      04/13/2010  Jingwei,LI   Initial version(based on fnBufferKey)
// *************************************************************************/
void fnBufferKeyLTR(UINT8 bKeyCode)
{
    UINT16  usUnicode;
    UINT32  ulIndex            = 0;
    BOOL    fSkipNormalProcess = FALSE;
    UINT32  combCharIndex;

    fnUpdateBufferFullStatus();

    /* as long as the buffer is not disabled and not full, lookup the key code
         in the key-unicode lookup table */
    if ((rEntryBuf.fFull != TRUE) && (rEntryBuf.fEnabled == TRUE))
    {

        /* if the key maps to a valid unicode character, add it to the buffer */
        if (fnSearchKeyMap(bKeyCode, &usUnicode) == SUCCESSFUL)
        {           
            /* check to see the entered char is a combining character */
			combCharIndex = fnSearchCombiningChar(usUnicode);
            if(combCharIndex != 0)
            {
                /*if the char entered is a combining char then store that in temp buf
                and skip the normal process of poke in the character to the entry buffer*/
				if (bCombiningCharsEnabled)
                {
                    TmpCombiningEntryBuf = usUnicode; 
                    fSkipNormalProcess = TRUE;
                }
                /* Else if combining charcaters are off on the current screen
                   and the character cannot be uncombined then do nothing */
                else if (!CombiningCharTable[combCharIndex - 1].bCanBeUncombined)
                    fSkipNormalProcess = TRUE;
            }
            else
            {
                /* check to see previously entered char is a combining char */
                if(TmpCombiningEntryBuf != 0xFFFF)
                {
                    /* if it is a combining char search for the matching character
                    from the table. If found assign the new Unicode value else ignore
                    and continue the normal process */
                    while (((CombiningCharTable[ulIndex].wCombiningChars != TmpCombiningEntryBuf) || 
                           (CombiningCharTable[ulIndex].wNormalChars != usUnicode)) && 
                           (CombiningCharTable[ulIndex].wCombiningChars != END_OF_COMBINING_CHAR_MAP))
                    {
                         ulIndex++;
                    }
                    
                    if (CombiningCharTable[ulIndex].wCombiningChars != END_OF_COMBINING_CHAR_MAP)
                    {
                         usUnicode = CombiningCharTable[ulIndex].wSpecialChars ;
                    }
                }
                
            }

            if(fSkipNormalProcess == FALSE)
            {
                /* first make room for the character.  if the buffer is left aligned and using
                 insert mode, and we aren't typing the last character at the right edge of the
                 buffer, shift everything currently in the buffer, starting from the current
                 cursor location, to the right */
                if((rEntryBuf.fInsert && rEntryBuf.fAlignL)&&
                   ((rEntryBuf.bNextCharIndex + 1)<rEntryBuf.bLenNonTempl))
                {
                    memmove( rEntryBuf.pEbuf->pBuf+rEntryBuf.bNextCharIndex+1,
                             rEntryBuf.pEbuf->pBuf+rEntryBuf.bNextCharIndex,
                            (rEntryBuf.bLenNonTempl-rEntryBuf.bNextCharIndex-1)*sizeof(UINT16));
                }

                if ((rEntryBuf.fAlignL == FALSE) && (rEntryBuf.bLenNonTempl > 1))
                {
                    if (rEntryBuf.fInsert == TRUE)
                    {
                        /* if aligned right and insertion mode, shift the whole buffer 1 space to the left */
                        memmove(rEntryBuf.pEbuf->pBuf,rEntryBuf.pEbuf->pBuf+1,
                                (rEntryBuf.bLenNonTempl - 1) * sizeof(UINT16));
                    }
                    else
                    {
                        /* if aligned right and overtype mode, shift only the entered chars to the left */
                        if (rEntryBuf.bCharsEntered == rEntryBuf.bLenNonTempl)
                        {
                            rEntryBuf.bCharsEntered--;  /* if buf already full, we lose the leftmost char */
                        }
                        
                        memmove(rEntryBuf.pEbuf->pBuf+(rEntryBuf.bLenNonTempl-rEntryBuf.bCharsEntered)-1,
                                rEntryBuf.pEbuf->pBuf+(rEntryBuf.bLenNonTempl-rEntryBuf.bCharsEntered),
                                rEntryBuf.bCharsEntered * sizeof(UINT16) );
                    }

                }

                /* poke in the character */
                rEntryBuf.pEbuf->pBuf[rEntryBuf.bNextCharIndex] = usUnicode;
                rEntryBuf.bCharsEntered++;

                /*Clear the combining char buf once a char is poked into the entry buf */
                TmpCombiningEntryBuf = 0xFFFF; 

                /* update the next character position, which changes only if we are aligned left */
                if (rEntryBuf.fAlignL == TRUE)
                {
                    rEntryBuf.bNextCharIndex++;
                }

                /* the buffer may now be full, so re-check for fullness */
                fnUpdateBufferFullStatus();
            }//End of fskipNormalProcess
        }
    }

    /* always update the cursor position/status */

    if (rEntryBuf.fCursorOn != TRUE)
    {
        /* report that the cursor is inactive if it is not used at all for this entry buffer */
        rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;
    }
    else
    {
        /* the cursor is active, but off, if the buffer is disabled or full */
        if ((rEntryBuf.fEnabled != TRUE) || (rEntryBuf.fFull == TRUE))
        {
            rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
        }
        else
        {
            rEntryBuf.pEbuf->bCursorLoc = fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);
        }

    }

    fnUpdateDisplayBuf( rEntryBuf.pEbuf->pDispBuf, rEntryBuf.pEbuf->pBuf, 
                        rEntryBuf.bNumTemplateChars, rEntryBuf.pTemplateCharLoc );

    fnChangeShiftState(0);  /* after attempting to buffer a key, the shift state always goes back to 0.
                              this allows us to implement a mechanism whereby the user is able to shift
                              a letter by first pressing and releasing the shift key and then pressing
                              the desired letter key.  the shift state automatically returns to normal
                              after that because of this function call. */
    return;

}


/* *************************************************************************
// FUNCTION NAME:
//       fnBufferKeyRTL
//
// DESCRIPTION:
//       In RTL language writting direction, takes a keycode as input and adds it
//       to the currently establishe dentry buffer in accordance with the way
//       the field is set up.
//
// INPUTS:
//       bKeyCode
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       None
//
// MODIFICATION  HISTORY:
//      04/22/2010  Deborah Kohl Do memmove when char at cursor is alphanumeric
//                               and not when it's hebrew
//      04/13/2010  Jingwei,LI   Initial version(based on fnBufferKey)
// *************************************************************************/
void fnBufferKeyRTL(UINT8 bKeyCode)
{
    UINT16  usUnicode, unicodeCharAtCursor;
    UINT32  ulIndex            = 0;
    BOOL    fSkipNormalProcess = FALSE;
    UINT32  combCharIndex;

    fnUpdateBufferFullStatus();

    /* as long as the buffer is not disabled and not full, lookup the key code
         in the key-unicode lookup table */
    if ((rEntryBuf.fFull != TRUE) && (rEntryBuf.fEnabled == TRUE))
    {

        /* if the key maps to a valid unicode character, add it to the buffer */
        if (fnSearchKeyMap(bKeyCode, &usUnicode) == SUCCESSFUL)
        {
            /* check to see the entered char is a combining character */
			combCharIndex = fnSearchCombiningChar(usUnicode);
            if(combCharIndex != 0)
            {
                /*if the char entered is a combining char then store that in temp buf
                and skip the normal process of poke in the character to the entry buffer*/
				if (bCombiningCharsEnabled)
                {
                    TmpCombiningEntryBuf = usUnicode;
                    fSkipNormalProcess = TRUE;
                }
                /* Else if combining charcaters are off on the current screen
                   and the character cannot be uncombined then do nothing */
                else if (!CombiningCharTable[combCharIndex - 1].bCanBeUncombined)
                    fSkipNormalProcess = TRUE;
            }
            else
            {
                /* check to see previously entered char is a combining char */
                if(TmpCombiningEntryBuf != 0xFFFF)
                {
                    /* if it is a combining char search for the matching character
                    from the table. If found assign the new Unicode value else ignore
                    and continue the normal process */
                    while (((CombiningCharTable[ulIndex].wCombiningChars != TmpCombiningEntryBuf) ||
                           (CombiningCharTable[ulIndex].wNormalChars != usUnicode)) &&
                           (CombiningCharTable[ulIndex].wCombiningChars != END_OF_COMBINING_CHAR_MAP))
                    {
                         ulIndex++;
                    }

                    if (CombiningCharTable[ulIndex].wCombiningChars != END_OF_COMBINING_CHAR_MAP)
                    {
                         usUnicode = CombiningCharTable[ulIndex].wSpecialChars ;
                    }
                }

            }

            if(fSkipNormalProcess == FALSE)
            {
                if((usUnicode >= 0x30 && usUnicode <= 0x39) ||(usUnicode >= 0x41 && usUnicode <= 0x5A)
                || (usUnicode >= 0x61 && usUnicode <= 0x7A))
                {

                    /* first make room for the character.  if the buffer is left aligned and using
                    insert mode, and we aren't typing the last character at the right edge of the
                    buffer, shift everything currently in the buffer, starting from the current
                    cursor location, to the right */
                    if(rEntryBuf.bNumTemplateChars > 0)
                    {
                        unicodeCharAtCursor = rEntryBuf.pEbuf->pBuf[rEntryBuf.pEbuf->bCursorLoc - rEntryBuf.bNumTemplateChars];
                    }
                    else
                    {
                        unicodeCharAtCursor = rEntryBuf.pEbuf->pBuf[rEntryBuf.pEbuf->bCursorLoc];
                    }

                    if(((unicodeCharAtCursor >= 0x30 && unicodeCharAtCursor <= 0x39) || (unicodeCharAtCursor >= 0x41 && unicodeCharAtCursor <= 0x5A)
                    || (unicodeCharAtCursor >= 0x61 && unicodeCharAtCursor <= 0x7A)))
                    {
                        memmove( rEntryBuf.pEbuf->pBuf,
                        rEntryBuf.pEbuf->pBuf + 1,
                        (rEntryBuf.pEbuf->bCursorLoc)*sizeof(UINT16));
                    }
                    else
                    {
                        rEntryBuf.pEbuf->bCursorLoc = rEntryBuf.bLenNonTempl - 1 - rEntryBuf.bCharsEntered;
                        if(rEntryBuf.bNumTemplateChars > 0)
                        {
                        rEntryBuf.pEbuf->bCursorLoc += rEntryBuf.bNumTemplateChars;
                        }
                    }
                }
                else  //Hebrew character
                {
                     rEntryBuf.pEbuf->bCursorLoc = rEntryBuf.bLenNonTempl - 1 - rEntryBuf.bCharsEntered;
                }

                if(rEntryBuf.bNumTemplateChars > 0)
                {
                    rEntryBuf.bNextCharIndex = rEntryBuf.pEbuf->bCursorLoc - rEntryBuf.bNumTemplateChars;
                }
                else
                {
                    rEntryBuf.bNextCharIndex = rEntryBuf.pEbuf->bCursorLoc;
                }

                /* poke in the character */
                rEntryBuf.pEbuf->pBuf[rEntryBuf.bNextCharIndex] = usUnicode;
                rEntryBuf.bCharsEntered++;

                /*Clear the combining char buf once a char is poked into the entry buf */
                TmpCombiningEntryBuf = 0xFFFF;

                /* update the next character position, which changes only if we are aligned left */
                //if (rEntryBuf.fAlignL == TRUE)
                //{
                //    rEntryBuf.bNextCharIndex++;
                //}

                /* the buffer may now be full, so re-check for fullness */
                fnUpdateBufferFullStatus();
            }//End of fskipNormalProcess
        }
    }

    /* always update the cursor position/status */

    if (rEntryBuf.fCursorOn != TRUE)
    {
        /* report that the cursor is inactive if it is not used at all for this entry buffer */
        rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;
    }
    else
    {
        /* the cursor is active, but off, if the buffer is disabled or full */
        if ((rEntryBuf.fEnabled != TRUE) || (rEntryBuf.fFull == TRUE))
        {
            rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
        }
        else
        {
           // rEntryBuf.pEbuf->bCursorLoc = fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);
        }

    }

    fnUpdateDisplayBuf( rEntryBuf.pEbuf->pDispBuf, rEntryBuf.pEbuf->pBuf,
                        rEntryBuf.bNumTemplateChars, rEntryBuf.pTemplateCharLoc );

    fnChangeShiftState(0);  /* after attempting to buffer a key, the shift state always goes back to 0.
                              this allows us to implement a mechanism whereby the user is able to shift
                              a letter by first pressing and releasing the shift key and then pressing
                              the desired letter key.  the shift state automatically returns to normal
                              after that because of this function call. */
    return;

}

/* *************************************************************************
// FUNCTION NAME: 
//       fnBufferKeyScroll
//
// DESCRIPTION:
//       Takes a keycode as input and adds it to the currently established
//       entry buffer in accordance with the way the field is set up.
//
// INPUTS:
//       bKeyCode
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       Only used for overwrite mode.
//
// MODIFICATION  HISTORY:
//      04/23/2010  Jingwei,LI      Modified to support RTL language writting direction.
//       09/15/2006     Raymond Shen    Modified it to deal with cursor's insert mode.
//       04/07/2006     Oscar/Kan       Initial version    
// *************************************************************************/
void fnBufferKeyScroll(UINT8 bKeyCode)
{
    UINT8       ubWritingDir;

	ubWritingDir = fnGetWritingDir();

	switch(ubWritingDir)
	{
		case LEFT_TO_RIGHT:
			fnBufferKeyScrollLTR(bKeyCode);
			break;

		case RIGHT_TO_LEFT:
			fnBufferKeyScrollRTL(bKeyCode);
			break;

		default:
		break;

	}

    return;

}
/* *************************************************************************
// FUNCTION NAME:
//       fnBufferKeyScrollLTR
//
// DESCRIPTION:
//       Takes a keycode as input and adds it to the currently established
//       entry buffer in accordance with the way the field is set up.
//
// INPUTS:
//       bKeyCode
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       None
//
// MODIFICATION  HISTORY:
//      04/23/2010  Jingwei,LI   Initial version(based on fnBufferKeyScroll)
// *************************************************************************/
void fnBufferKeyScrollLTR(UINT8 bKeyCode)
{
    UINT16  usUnicode;
    UINT32  ulIndex            = 0;
    BOOL    fSkipNormalProcess = FALSE;
    UINT32  combCharIndex;

    /* as long as the buffer is not disabled and not full, lookup the key code
         in the key-unicode lookup table */
    if ((rEntryBuf.fFull != TRUE) && (rEntryBuf.fEnabled == TRUE))
    {

        /* if the key maps to a valid unicode character, add it to the buffer */
        if (fnSearchKeyMap(bKeyCode, &usUnicode) == SUCCESSFUL)
        {           
            /* check to see the entered char is a combining character */
			combCharIndex = fnSearchCombiningChar(usUnicode);
            if(combCharIndex != 0)
            {
                /*if the char entered is a combining char then store that in temp buf
                and skip the normal process of poke in the character to the entry buffer*/
				if (bCombiningCharsEnabled)
                {
                    TmpCombiningEntryBuf = usUnicode; 
                    fSkipNormalProcess = TRUE;
                }
                /* Else if combining charcaters are off on the current screen
                   and the character cannot be uncombined then do nothing */
                else if (!CombiningCharTable[combCharIndex - 1].bCanBeUncombined)
                    fSkipNormalProcess = TRUE;
            }
            else
            {
                /* check to see previously entered char is a combining char */
                if(TmpCombiningEntryBuf != 0xFFFF)
                {
                    /* if it is a combining char search for the matching character
                    from the table. If found assign the new Unicode value else ignore
                    and continue the normal process */
                    while(((CombiningCharTable[ulIndex].wCombiningChars != TmpCombiningEntryBuf)  
                          ||(CombiningCharTable[ulIndex].wNormalChars != usUnicode))  
                          &&(CombiningCharTable[ulIndex].wCombiningChars != END_OF_COMBINING_CHAR_MAP))
                    {
                         ulIndex++;
                    }
                    
                    if (CombiningCharTable[ulIndex].wCombiningChars != END_OF_COMBINING_CHAR_MAP)
                    {
                         usUnicode = CombiningCharTable[ulIndex].wSpecialChars ;
                    }
                }
                
            }

            if(fSkipNormalProcess == FALSE)
            {
                if ((rEntryBuf.fAlignL == FALSE) && (rEntryBuf.bLenNonTempl > 1))
                {    
                     /* if aligned right and overtype mode, shift only the entered chars to the left */
                     if (rEntryBuf.bCharsEntered == rEntryBuf.bLenNonTempl)
                     {
                         rEntryBuf.bCharsEntered--;  /* if buf already full, we lose the leftmost char */
                     }
                        
                     memmove(rEntryBuf.pEbuf->pBuf+(rEntryBuf.bLenNonTempl-rEntryBuf.bCharsEntered)-1,
                             rEntryBuf.pEbuf->pBuf+(rEntryBuf.bLenNonTempl-rEntryBuf.bCharsEntered),
                             rEntryBuf.bCharsEntered * sizeof(UINT16) );

                }

                /* poke in the character */
                rEntryBuf.pEbuf->pBuf[rEntryBuf.bNextCharIndex] = usUnicode;
                
		        if( (rEntryBuf.bCharsEntered !=rEntryBuf.bLen ) 
                  &&(rEntryBuf.bNextCharIndex + 1>rEntryBuf.bCharsEntered)) 
                {
                    rEntryBuf.bCharsEntered++;
                }

                /*Clear the combining char buf once a char is poked into the entry buf */
                TmpCombiningEntryBuf = 0xFFFF; 

                /* update the next character position, which changes only if we are aligned left */
		      if ( ((rEntryBuf.bCharsEntered <= rEntryBuf.bLen ) 
                  //&& (rEntryBuf.bNextCharIndex+1 >= rEntryBuf.bCharsEntered) 
		          && (rEntryBuf.bNextCharIndex+1 < rEntryBuf.bLen) 
		          && (rEntryBuf.fAlignL == TRUE)) 
		          ||(rEntryBuf.fInsert == 1))
                {
                    rEntryBuf.bNextCharIndex++;
                }

            }//End of fskipNormalProcess
        }
    }

    /* always update the cursor position/status */
    if (rEntryBuf.fCursorOn != TRUE)
    {
        /* report that the cursor is inactive if it is not used at all for this entry buffer */
        rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;
    }
    else
    {
        /* the cursor is active, but off, if the buffer is disabled or full */
        if ((rEntryBuf.fEnabled != TRUE) || (rEntryBuf.fFull == TRUE))
        {
            rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
        }
        else
        {
            rEntryBuf.pEbuf->bCursorLoc = fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);
            if( ( rEntryBuf.bNextCharIndex == rEntryBuf.bLenNonTempl )
                && ( rEntryBuf.fInsert == 1 ) )
            {
                rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
                rEntryBuf.fFull = 1;
            }
        }

    }

    fnUpdateDisplayBuf( rEntryBuf.pEbuf->pDispBuf, rEntryBuf.pEbuf->pBuf, 
                        rEntryBuf.bNumTemplateChars, rEntryBuf.pTemplateCharLoc );

    fnChangeShiftState(0);  /* after attempting to buffer a key, the shift state always goes back to 0.
                              this allows us to implement a mechanism whereby the user is able to shift
                              a letter by first pressing and releasing the shift key and then pressing
                              the desired letter key.  the shift state automatically returns to normal
                              after that because of this function call. */
    return;
}


/* *************************************************************************
// FUNCTION NAME:
//       fnBufferKeyScrollRTL
//
// DESCRIPTION:
//       Takes a keycode as input and adds it to the currently established
//       entry buffer in accordance with the way the field is set up.
//
// INPUTS:
//       bKeyCode
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       None
//
// MODIFICATION  HISTORY:
//      06/09/2010  Jingwei,Li   Remove the restriction of insert mode.
//      06/02/2010  Jingwei,Li   Remove the special symbol(.\/...)check.
//      04/28/2010  Jingwei,Li   Re-check the buffer status after the char inserted.
//      04/23/2010  Jingwei,LI   Initial version(based on fnBufferKeyScroll)

// *************************************************************************/
void fnBufferKeyScrollRTL(UINT8 bKeyCode)
{
     UINT16  usUnicode, unicodeCharAtCursor;
    UINT32  ulIndex            = 0;
    BOOL    fSkipNormalProcess = FALSE;
    UINT32  combCharIndex;

    /* as long as the buffer is not disabled and not full, lookup the key code
         in the key-unicode lookup table */
    if ((rEntryBuf.fFull != TRUE) && (rEntryBuf.fEnabled == TRUE))
    {

        /* if the key maps to a valid unicode character, add it to the buffer */
        if (fnSearchKeyMap(bKeyCode, &usUnicode) == SUCCESSFUL)
        {
            /* check to see the entered char is a combining character */
			combCharIndex = fnSearchCombiningChar(usUnicode);
            if(combCharIndex != 0)
            {
                /*if the char entered is a combining char then store that in temp buf
                and skip the normal process of poke in the character to the entry buffer*/
				if (bCombiningCharsEnabled)
                {
                    TmpCombiningEntryBuf = usUnicode;
                    fSkipNormalProcess = TRUE;
                }
                /* Else if combining charcaters are off on the current screen
                   and the character cannot be uncombined then do nothing */
                else if (!CombiningCharTable[combCharIndex - 1].bCanBeUncombined)
                    fSkipNormalProcess = TRUE;
            }
            else
            {
                /* check to see previously entered char is a combining char */
                if(TmpCombiningEntryBuf != 0xFFFF)
                {
                    /* if it is a combining char search for the matching character
                    from the table. If found assign the new Unicode value else ignore
                    and continue the normal process */
                    while(((CombiningCharTable[ulIndex].wCombiningChars != TmpCombiningEntryBuf)
                          ||(CombiningCharTable[ulIndex].wNormalChars != usUnicode))
                          &&(CombiningCharTable[ulIndex].wCombiningChars != END_OF_COMBINING_CHAR_MAP))
                    {
                         ulIndex++;
                    }

                    if (CombiningCharTable[ulIndex].wCombiningChars != END_OF_COMBINING_CHAR_MAP)
                    {
                         usUnicode = CombiningCharTable[ulIndex].wSpecialChars ;
                    }
                }

            }

            if(fSkipNormalProcess == FALSE)
            {
      //        if(rEntryBuf.fInsert == TRUE) //just for insert mode
      //         {
                   if((usUnicode >= 0x30 && usUnicode <= 0x39) ||(usUnicode >= 0x41 && usUnicode <= 0x5A)
                 	|| (usUnicode >= 0x61 && usUnicode <= 0x7A))
                 	{
    					/* first make room for the character.  if the buffer is left aligned and using
    					 insert mode, and we aren't typing the last character at the right edge of the
    					 buffer, shift everything currently in the buffer, starting from the current
    					 cursor location, to the right */
    					unicodeCharAtCursor = rEntryBuf.pEbuf->pBuf[rEntryBuf.pEbuf->bCursorLoc];

    					if(!(unicodeCharAtCursor == 0x20 ||
    					!((unicodeCharAtCursor >= 0x30 && unicodeCharAtCursor <= 0x39) || (unicodeCharAtCursor >= 0x41 && unicodeCharAtCursor <= 0x5A)
                 		|| (unicodeCharAtCursor >= 0x61 && unicodeCharAtCursor <= 0x7A))))
    					{
    						memmove( rEntryBuf.pEbuf->pBuf,
    								 rEntryBuf.pEbuf->pBuf + 1,
    								(rEntryBuf.pEbuf->bCursorLoc)*sizeof(UINT16));
    					}
    					else
    					{
    						rEntryBuf.pEbuf->bCursorLoc = rEntryBuf.bLenNonTempl - 1 - rEntryBuf.bCharsEntered;
    					}
    				}
    				else
    				{
    					rEntryBuf.pEbuf->bCursorLoc = rEntryBuf.bLenNonTempl - 1 - rEntryBuf.bCharsEntered;
    				}
		//		}
                /* poke in the character */
				rEntryBuf.bNextCharIndex = rEntryBuf.pEbuf->bCursorLoc;
                rEntryBuf.pEbuf->pBuf[rEntryBuf.bNextCharIndex] = usUnicode;

                /* Two modes: insert  and overide 
		        if( (rEntryBuf.bCharsEntered < rEntryBuf.bLenNonTempl)
                      &&(rEntryBuf.fInsert == TRUE))
                {
                   rEntryBuf.bCharsEntered++;
                }
                else if(rEntryBuf.bNextCharIndex < rEntryBuf.bLenNonTempl -1)
                {
                    rEntryBuf.bNextCharIndex ++;
                    rEntryBuf.pEbuf->bCursorLoc ++;
                } */
                rEntryBuf.bCharsEntered++;
                /*Clear the combining char buf once a char is poked into the entry buf */
                TmpCombiningEntryBuf = 0xFFFF;

                /* the buffer may now be full, so re-check for fullness */
                fnUpdateBufferFullStatus();
            }//End of fskipNormalProcess
        }
    }

    /* always update the cursor position/status */
    if (rEntryBuf.fCursorOn != TRUE)
    {
        /* report that the cursor is inactive if it is not used at all for this entry buffer */
        rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;
    }
    else
    {
        /* the cursor is active, but off, if the buffer is disabled or full */
        if ((rEntryBuf.fEnabled != TRUE) || (rEntryBuf.fFull == TRUE))
        {
            rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
        }
        else
        {
         /*   rEntryBuf.pEbuf->bCursorLoc = fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);
            if( ( rEntryBuf.bNextCharIndex == rEntryBuf.bLenNonTempl )
                && ( rEntryBuf.fInsert == 1 ) )
            {
                rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
                rEntryBuf.fFull = 1;
            }*/
        }

    }

    fnUpdateDisplayBuf( rEntryBuf.pEbuf->pDispBuf, rEntryBuf.pEbuf->pBuf,
                        rEntryBuf.bNumTemplateChars, rEntryBuf.pTemplateCharLoc );

    fnChangeShiftState(0);  /* after attempting to buffer a key, the shift state always goes back to 0.
                              this allows us to implement a mechanism whereby the user is able to shift
                              a letter by first pressing and releasing the shift key and then pressing
                              the desired letter key.  the shift state automatically returns to normal
                              after that because of this function call. */
    return;

}

/* *************************************************************************
// FUNCTION NAME:
//       fnDistrParamBufferKeyScrollRTL
//
// DESCRIPTION:
//       Takes a keycode as input and adds it to the currently established
//       entry buffer for distributor parameters in RTL input.
//
// INPUTS:
//       bKeyCode
//
// RETURNS:
//       None
//
// WARNINGS/NOTES:
//       None
// MODIFICATION  HISTORY:
//      06/09/2010  Jingwei,Li   Remove the restriction of insert mode.
//      06/02/2010  Jingwei,LI      Initial version(based on fnBufferKeyScrollRTL).    
// *************************************************************************/
void fnDistrParamBufferKeyScrollRTL(UINT8 bKeyCode)
{
    UINT16  usUnicode, unicodeCharAtCursor;
    UINT32  ulIndex            = 0;
    BOOL    fSkipNormalProcess = FALSE;
    UINT32  combCharIndex;

    /* as long as the buffer is not disabled and not full, lookup the key code
         in the key-unicode lookup table */
    if ((rEntryBuf.fFull != TRUE) && (rEntryBuf.fEnabled == TRUE))
    {

        /* if the key maps to a valid unicode character, add it to the buffer */
        if (fnSearchKeyMap(bKeyCode, &usUnicode) == SUCCESSFUL)
        {
            /* check to see the entered char is a combining character */
			combCharIndex = fnSearchCombiningChar(usUnicode);
            if(combCharIndex != 0)
            {
                /*if the char entered is a combining char then store that in temp buf
                and skip the normal process of poke in the character to the entry buffer*/
				if (bCombiningCharsEnabled)
                {
                    TmpCombiningEntryBuf = usUnicode;
                    fSkipNormalProcess = TRUE;
                }
                /* Else if combining charcaters are off on the current screen
                   and the character cannot be uncombined then do nothing */
                else if (!CombiningCharTable[combCharIndex - 1].bCanBeUncombined)
                    fSkipNormalProcess = TRUE;
            }
            else
            {
                /* check to see previously entered char is a combining char */
                if(TmpCombiningEntryBuf != 0xFFFF)
                {
                    /* if it is a combining char search for the matching character
                    from the table. If found assign the new Unicode value else ignore
                    and continue the normal process */
                    while(((CombiningCharTable[ulIndex].wCombiningChars != TmpCombiningEntryBuf)
                          ||(CombiningCharTable[ulIndex].wNormalChars != usUnicode))
                          &&(CombiningCharTable[ulIndex].wCombiningChars != END_OF_COMBINING_CHAR_MAP))
                    {
                         ulIndex++;
                    }

                    if (CombiningCharTable[ulIndex].wCombiningChars != END_OF_COMBINING_CHAR_MAP)
                    {
                         usUnicode = CombiningCharTable[ulIndex].wSpecialChars ;
                    }
                }

            }

            if(fSkipNormalProcess == FALSE)
            {
      //         if(rEntryBuf.fInsert == TRUE) //just for insert mode
      //         {
                   if(!fnIsUnicodeHebrew(usUnicode))
                 	{
    					/* first make room for the character.  if the buffer is left aligned and using
    					 insert mode, and we aren't typing the last character at the right edge of the
    					 buffer, shift everything currently in the buffer, starting from the current
    					 cursor location, to the right */
    					unicodeCharAtCursor = rEntryBuf.pEbuf->pBuf[rEntryBuf.pEbuf->bCursorLoc];

    					if((unicodeCharAtCursor != 0x20) && !fnIsUnicodeHebrew(unicodeCharAtCursor))
    					{
    						memmove( rEntryBuf.pEbuf->pBuf,
    								 rEntryBuf.pEbuf->pBuf + 1,
    								(rEntryBuf.pEbuf->bCursorLoc)*sizeof(UINT16));
    					}
    					else
    					{
    						rEntryBuf.pEbuf->bCursorLoc = rEntryBuf.bLenNonTempl - 1 - rEntryBuf.bCharsEntered;
    					}
    				}
    				else
    				{
    					rEntryBuf.pEbuf->bCursorLoc = rEntryBuf.bLenNonTempl - 1 - rEntryBuf.bCharsEntered;
    				}
		//		}
                /* poke in the character */
				rEntryBuf.bNextCharIndex = rEntryBuf.pEbuf->bCursorLoc;
                rEntryBuf.pEbuf->pBuf[rEntryBuf.bNextCharIndex] = usUnicode;
                /* Two modes: insert  and overide 
		        if( (rEntryBuf.bCharsEntered < rEntryBuf.bLenNonTempl)
                      &&(rEntryBuf.fInsert == TRUE))
                {
                   rEntryBuf.bCharsEntered++;
                }
                else if(rEntryBuf.bNextCharIndex < rEntryBuf.bLenNonTempl -1)
                {
                    rEntryBuf.bNextCharIndex ++;
                    rEntryBuf.pEbuf->bCursorLoc ++;
                }*/
                rEntryBuf.bCharsEntered++;
                /*Clear the combining char buf once a char is poked into the entry buf */
                TmpCombiningEntryBuf = 0xFFFF;

                /* the buffer may now be full, so re-check for fullness */
                fnUpdateBufferFullStatus();
            }//End of fskipNormalProcess
        }
    }

    /* always update the cursor position/status */
    if (rEntryBuf.fCursorOn != TRUE)
    {
        /* report that the cursor is inactive if it is not used at all for this entry buffer */
        rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;
    }
    else
    {
        /* the cursor is active, but off, if the buffer is disabled or full */
        if ((rEntryBuf.fEnabled != TRUE) || (rEntryBuf.fFull == TRUE))
        {
            rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
        }
        else
        {
         /*   rEntryBuf.pEbuf->bCursorLoc = fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);
            if( ( rEntryBuf.bNextCharIndex == rEntryBuf.bLenNonTempl )
                && ( rEntryBuf.fInsert == 1 ) )
            {
                rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
                rEntryBuf.fFull = 1;
            }*/
        }

    }

    fnUpdateDisplayBuf( rEntryBuf.pEbuf->pDispBuf, rEntryBuf.pEbuf->pBuf,
                        rEntryBuf.bNumTemplateChars, rEntryBuf.pTemplateCharLoc );

    fnChangeShiftState(0);  /* after attempting to buffer a key, the shift state always goes back to 0.
                              this allows us to implement a mechanism whereby the user is able to shift
                              a letter by first pressing and releasing the shift key and then pressing
                              the desired letter key.  the shift state automatically returns to normal
                              after that because of this function call. */
    return;

}


/* *************************************************************************
// FUNCTION NAME:
//          fnSearchCombiningChar
//
// DESCRIPTION:
//          Takes a Unicode and checks whether the char is a 
//          combining char.
//
// INPUTS:
//          usUnicode
//
// RETURNS:
//          Index + 1 in the combining chars table or 0 if not found.
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          Thillaikumaran   Initial version 
// *************************************************************************/
UINT32 fnSearchCombiningChar(UINT16 usUnicode) 
{
    UINT32 ulRetVal = 0;
    UINT32 ulIndex = 0;

    
    /* Search for the combining char from the table. 
       if found return TRUE else return FALSE */
    while((CombiningCharTable[ulIndex].wCombiningChars != usUnicode) && 
          (CombiningCharTable[ulIndex].wCombiningChars != END_OF_COMBINING_CHAR_MAP) )
    {
        ulIndex++;
    }
    
    if (CombiningCharTable[ulIndex].wCombiningChars != END_OF_COMBINING_CHAR_MAP)
    {
        ulRetVal = ulIndex+1;
    }
    else
    {
        ulRetVal = 0;
    }

    return ulRetVal;
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnBufferDisable
//
// DESCRIPTION:
//          Disables the currently established entry buffer.  A disabled 
//          buffer will not accept input and has the cursor turned off.
//
// INPUTS:
//          None
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          Mozdzer     Initial version 
// *************************************************************************/
void fnBufferDisable(void)
{
    rEntryBuf.fEnabled = FALSE;
    rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
    
    return;
}


/* *************************************************************************
// FUNCTION NAME: 
//          fnBufferKeyClear1st
//
// DESCRIPTION:
//          If no characters have been typed into the current entry buffer,
//          clear the buffer to spaces, then buffer the key.   Otherwise
//          just buffer the key normally.
//
// INPUTS:
//          bKeyCode
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          Mozdzer    Initial version 
// *************************************************************************/
void fnBufferKeyClear1st(UINT8 bKeyCode)
{
    if (rEntryBuf.bCharsEntered == 0)
    {
        SET_SPACE_UNICODE_NO_NULL (rEntryBuf.pEbuf->pBuf, rEntryBuf.bLenNonTempl);
        /* FP 
           for (i = 0; i < rEntryBuf.bLenNonTempl; i++)
            rEntryBuf.pEbuf->pBuf[i] = UNICODE_SPACE;

        */
    }

    fnBufferKey(bKeyCode);
    return;
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnClearEntryBuffer
//
// DESCRIPTION:
//          Clears the contents of the entry buffer to spaces and resets 
//          its cursor position.  If the buffer was disabled, it is re-enabled.
//
// INPUTS:
//          None
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//         06/24/2010  Rocky Dai    Added an exception when setting bNextCharIndex for Israel.
//         06/09/2010  Jingwei,Li Clear IP address buffer in LTR mode.
//         04/21/2010 Jingwei,Li Updated for RTL.
//          Mozdzer   Initial version 
// *************************************************************************/
void fnClearEntryBuffer(void)
{
    UINT8 ubWritingDir;
    ubWritingDir = fnGetWritingDir();

    /* do some housekeeping in the control structure */ 
    rEntryBuf.fEnabled      = TRUE;
    rEntryBuf.bCharsEntered = 0;
    rEntryBuf.fFull         = FALSE;
    
    /* Clear the combining char buf */
    TmpCombiningEntryBuf = 0xFFFF; 

    /* re-initialize the index where the next character will go.  this depends on 
       the alignment of the field */
    if (rEntryBuf.fAlignL == TRUE)
    {
        rEntryBuf.bNextCharIndex = 0;
    }
    else
    {
        rEntryBuf.bNextCharIndex = rEntryBuf.bLenNonTempl - 1;
    }
    
    if(( ubWritingDir == RIGHT_TO_LEFT ))
    {
        rEntryBuf.bNextCharIndex = rEntryBuf.bLenNonTempl - 1;
    }

    /* re-initialize the cursor position */
    if (rEntryBuf.fCursorOn == TRUE)
    {
        rEntryBuf.pEbuf->bCursorLoc = fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);
        
        if (rEntryBuf.fInsert == TRUE)
        {
            rEntryBuf.pEbuf->bCursorStyle = LINE_CURSOR;
        }
        else
        {
            rEntryBuf.pEbuf->bCursorStyle = BLOCK_CURSOR;
        }
    }
    else
    {
        rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;
    }

    /* clear out the entry buffer */
    SET_SPACE_UNICODE_NO_NULL (rEntryBuf.pEbuf->pBuf, rEntryBuf.bLenNonTempl);
    
    /* update the display buffer */
    fnUpdateDisplayBuf( rEntryBuf.pEbuf->pDispBuf, rEntryBuf.pEbuf->pBuf, 
                        rEntryBuf.bNumTemplateChars, rEntryBuf.pTemplateCharLoc );

    return;
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnGetCurrentEntryBuffer
//
// DESCRIPTION:
//          Returns a pointer to the most recently created entry buffer.
//
// INPUTS:
//          None
//
// RETURNS:
//          pointer to the current entry buffer.  this data may not be valid
//          if the function is called before any entry buffer has been created.
//          it is the caller's responsibility to make sure they call this routine
//          only when a valid entry buffer exists.
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          Mozdzer   Initial version
// *************************************************************************/
ENTRY_BUFFER * fnGetCurrentEntryBuffer(void)
{
    return (rEntryBuf.pEbuf);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnCharsBuffered
//
// DESCRIPTION:
//          Provides the number of characters entered into the entry buffer.
//          Only characters that were individually buffered are counted-       
//          the initial value of the buffer before typing has no relevance.
//
// INPUTS:
//          None
//
// RETURNS:
//          the number of characters entered.
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          Mozdzer  Initial version 
// *************************************************************************/
UINT8 fnCharsBuffered(void)
{
    return (rEntryBuf.bCharsEntered);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnBufferLen
//
// DESCRIPTION:
//          Provides the length of the current entry buffer.  This does
//          NOT return the number of characters actually entered by the
//          user, which may be less.  Also, this is NOT the length of the
//          displayable buffer, which may contain template characters 
//          used only for formatting.  Basically this is the number of
//          characters that can be typed into the buffer.
//
// INPUTS:
//          None
//
// RETURNS:
//          the length of the entry buffer.
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          Mozdzer   Initial version 
// *************************************************************************/
UINT8 fnBufferLen(void)
{
    return (rEntryBuf.bLenNonTempl);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnDisplayableBufferLen
//
// DESCRIPTION:
//          Provides the length of the current entry buffer, including
//          any template characters used only for formatting.  This is
//          the number of characters used to display the buffer contents,
//          (which can be greater than the number that can be typed if
//          a template is being used).
//
// INPUTS:
//          None
//
// RETURNS:
//          the length of the displayable entry buffer.
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//         Mozdzer   Initial version  
// *************************************************************************/
UINT8 fnDisplayableBufferLen(void)
{
    return (rEntryBuf.bLen);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnIsBufferBlank
//
// DESCRIPTION:
//          Checks if the current entry buffer is set to a Unicode string
//          consisting of all space characters.
//
// INPUTS:
//          None
//
// RETURNS:
//          TRUE if the buffer is blank, FALSE otherwise
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          Mozdzer   Initial version 
// *************************************************************************/
BOOL fnIsBufferBlank(void)
{
    BOOL    fRetval = TRUE;
    UINT32  i;

    for (i = 0; i < rEntryBuf.bLenNonTempl; i++)
    {
        if (rEntryBuf.pEbuf->pBuf[i] != UNICODE_SPACE)
        {
            fRetval = FALSE;
            break;
        }
    }

    return (fRetval);
}

/* *************************************************************************
// FUNCTION NAME: fnIsBufferFull
// DESCRIPTION: Provides the full status of the current entry buffer.
// AUTHOR: Wang Biao
//
// INPUTS: none
// RETURNS: the full status of current entry buffer.
//
// *************************************************************************/
BOOL fnIsBufferFull(void)
{
	return (rEntryBuf.fFull);
}


/* *************************************************************************
// FUNCTION NAME: 
//          fnSearchKeyMap
//
// DESCRIPTION:
//          Determines the Unicode character that is mapped to a key.
//
// INPUTS:
//          bKeyCode - the key code to look up
//          pUnicode - the Unicode mapping is returned at this address. 
//                     it is valid only if the function returns SUCCESSFUL.
//
// RETURNS:
//          SUCCESSFUL - if the key is mapped to a Unicode character.
//          FAILURE - if the key was not found in the keymap for this language.
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//         2007-03-09  Simon Fox - Now only replaces decimal separator on keypad.
//         2005-12-02  Adam   Adapted for FuturePhoenix
//          Mozdzer      Initial version 
// *************************************************************************/
BOOL fnSearchKeyMap( UINT8   bKeyCode, 
                     UINT16 *pUnicode)
{
    SINT32   i;
    BOOL     fRetVal;
    UINT8    bKeyControlData;
    UINT8    bHardKey;
    UINT8    bRequiredCtrlData; /* the required key control data value, 
                                  based on the current shift state. */
    UINT8   *pBindingPtr;
    const KEY_MAP *pKeyMap = fnGetKeyMap(); 

    /* the value required in the control data byte in the key binding is equal to the shift state */
    switch (bShiftState)
    {
        case 0:
            bRequiredCtrlData = 0;
            break;
        case 1:
            bRequiredCtrlData = 0x01;
            break;
        case 2:
            bRequiredCtrlData = 0x02;
            break;
        case 3:
            bRequiredCtrlData = 0x03;
            break;
        /* if the shift state gets corrupted, set the required ctrl data byte to FF which should never
           match and will therefore not allow a key to be found */
        default:
            bRequiredCtrlData = 0xFF;
    }       
            
    fRetVal     = FAILURE;
    pBindingPtr = pKeyMap->pFirstBinding;

    for (i = 0; i < pKeyMap->wNumBindings; i++)
    {
        bKeyControlData = *pBindingPtr++;
        bHardKey = *pBindingPtr++;

        /* if we find a match between the key pressed and a binding, grab the
           Unicode assignment and get out of here */
        if ((bHardKey == bKeyCode) && (bKeyControlData == bRequiredCtrlData))
        {
            memcpy(pUnicode, pBindingPtr, sizeof(UINT16));
            fRetVal = SUCCESSFUL;
            break;
        }
        else
        {
            /* otherwise just advance to the start of the next binding */
            pBindingPtr += sizeof(UINT16);
        }
    }

    
    if ( fRetVal == SUCCESSFUL )
    {
        if ( UNICODE_DECIMAL_POINT != fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR) )
        {
            if ( *pUnicode ==  UNICODE_DECIMAL_POINT && bKeyCode == fnDecSepScanCode())  // Only replace the character if it's the one on the keypad
                *pUnicode = fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR) ;  // The Key '.' should be a PCN specific decimal point
        }
    }
    return (fRetVal);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnChangeShiftState
//
// DESCRIPTION:
//          Alters the current shift state for buffering characters.
//
// INPUTS:
//          bNewShiftState - the new shift state
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          Mozdzer     Initial version
// *************************************************************************/
void fnChangeShiftState( UINT8 bNewShiftState )
{
    bShiftState = bNewShiftState;
    return;
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnGetShiftState
//
// DESCRIPTION:
//          Returns the current shift state for buffering characters.
//
// INPUTS:
//          None
//
// RETURNS:
//          The current shift state
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          Mozdzer    Initial version
// *************************************************************************/
UINT8 fnGetShiftState(void)
{
     return (bShiftState);
}



/*-------------------------------------------------------------------------*/
/*--- The functions below are for private use by the entry field engine ---*/
/*-------------------------------------------------------------------------*/

/* *************************************************************************
// FUNCTION NAME: 
//          fnBufferUnicode
//
// DESCRIPTION:
//          Takes a unicode char as input and adds it to the currently established
//          entry buffer in accordance with the way the field is set up.
//
// INPUTS:
//          usUnicode
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//      04/13/2010  Jingwei,LI      Modified to support RTL language writting direction.
//         Craig DeFilippo(based on fnBufferKey())  Initial version  
// *************************************************************************/
void fnBufferUnicode(UINT16 usUnicode)
{
	UINT8       ubWritingDir;

	ubWritingDir = fnGetWritingDir();

	switch(ubWritingDir)
	{
		case LEFT_TO_RIGHT:
			fnBufferUnicodeLTR(usUnicode);
			break;

		case RIGHT_TO_LEFT:
			fnBufferUnicodeRTL(usUnicode);
			break;

		default:
		break;

	}

    return;
}


/* *************************************************************************
// FUNCTION NAME:
//          fnBufferUnicodeLTR
//
// DESCRIPTION:
//          In LTR language writting direction,takes a unicode char as input 
//          and adds it to the currently established  entry buffer in accordance
//          with the way the field is set up.
//
// INPUTS:
//          usUnicode
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//      04/13/2010  Jingwei,LI   Initial version(based on fnBufferUnicode)
// *************************************************************************/
void fnBufferUnicodeLTR(UINT16 usUnicode)
{

    fnUpdateBufferFullStatus();

    /* as long as the buffer is not disabled and not full, lookup the key code
       in the key-unicode lookup table */
    if ((rEntryBuf.fFull != TRUE) && (rEntryBuf.fEnabled == TRUE))
    {

        /* if the key maps to a valid unicode character, add it to the buffer */
        if (usUnicode != 0)
        {
        
            /* first make room for the character.  if the buffer is left aligned and using
               insert mode, and we aren't typing the last character at the right edge of the
               buffer, shift everything currently in the buffer, starting from the current
               cursor location, to the right */
            if ( ((rEntryBuf.fInsert == TRUE) && (rEntryBuf.fAlignL == TRUE)) 
                 &&((rEntryBuf.bNextCharIndex + 1) < rEntryBuf.bLenNonTempl))
            {
                memmove( rEntryBuf.pEbuf->pBuf+rEntryBuf.bNextCharIndex+1,
                         rEntryBuf.pEbuf->pBuf+rEntryBuf.bNextCharIndex,
                         (rEntryBuf.bLenNonTempl-rEntryBuf.bNextCharIndex-1)*sizeof(UINT16));
            }

            if ((rEntryBuf.fAlignL == FALSE) && (rEntryBuf.bLenNonTempl > 1))
            {
                if (rEntryBuf.fInsert == TRUE)
                {
                    /* if aligned right and insertion mode, 
                       shift the whole buffer 1 space to the left */
                    memmove(rEntryBuf.pEbuf->pBuf, rEntryBuf.pEbuf->pBuf + 1, 
                            (rEntryBuf.bLenNonTempl - 1) * sizeof(UINT16));
                }
                else
                {
                    /* if aligned right and overtype mode, 
                       shift only the entered chars to the left */
                    if (rEntryBuf.bCharsEntered == rEntryBuf.bLenNonTempl)
                    {
                        rEntryBuf.bCharsEntered--;  /* if buf already full, 
                                                       we lose the leftmost char */
                    }
                    
                    memmove(rEntryBuf.pEbuf->pBuf+(rEntryBuf.bLenNonTempl-rEntryBuf.bCharsEntered)-1,
                            rEntryBuf.pEbuf->pBuf+(rEntryBuf.bLenNonTempl-rEntryBuf.bCharsEntered),
                            rEntryBuf.bCharsEntered * sizeof(UINT16));
                }

            }

            /* poke in the character */
            rEntryBuf.pEbuf->pBuf[rEntryBuf.bNextCharIndex] = usUnicode;
            rEntryBuf.bCharsEntered++;

            /* update the next character position, which changes only if we are aligned left */
            if (rEntryBuf.fAlignL == TRUE)
            {
                rEntryBuf.bNextCharIndex++;
            }

            /* the buffer may now be full, so re-check for fullness */
            fnUpdateBufferFullStatus();
        }
    }

    /* always update the cursor position/status */

    if (rEntryBuf.fCursorOn != TRUE)
    {
        /* report that the cursor is inactive if it is not used at all for this entry buffer */
        rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;
    }
    else
    {
        /* the cursor is active, but off, if the buffer is disabled or full */
        if ((rEntryBuf.fEnabled != TRUE) || (rEntryBuf.fFull == TRUE))
        {
            rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
        }
        else
        {
            rEntryBuf.pEbuf->bCursorLoc=fnCalculateCursorPosition(rEntryBuf.bNextCharIndex);
        }

    }

    fnUpdateDisplayBuf(rEntryBuf.pEbuf->pDispBuf,rEntryBuf.pEbuf->pBuf, 
                       rEntryBuf.bNumTemplateChars,rEntryBuf.pTemplateCharLoc);

    fnChangeShiftState(0);/* after attempting to buffer a key, the shift state always goes back to 0.
                             this allows us to implement a mechanism whereby the user is able to shift
                             a letter by first pressing and releasing the shift key and then pressing
                             the desired letter key.  the shift state automatically returns to normal
                             after that because of this function call. */
    return;

}

/* *************************************************************************
// FUNCTION NAME:
//          fnBufferUnicodeRTL
//
// DESCRIPTION:
//          In RTL language writting direction,takes a unicode char as input 
//          and adds it to the currently established  entry buffer in accordance
//          with the way the field is set up.
//
// INPUTS:
//          usUnicode
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//      04/27/2010  Deborah Kohl Re-use fnCalculateCursorPosition to set
//                               the cursor location (fix the issue of cursor
//                               position in dialing prefix screen in OOB,
//                               when there is already a prefix)
//      04/13/2010  Jingwei,LI   Initial version(based on fnBufferUnicode)
// *************************************************************************/
void fnBufferUnicodeRTL(UINT16 usUnicode)
{

    fnUpdateBufferFullStatus();

    /* as long as the buffer is not disabled and not full, lookup the key code
       in the key-unicode lookup table */
    if ((rEntryBuf.fFull != TRUE) && (rEntryBuf.fEnabled == TRUE))
    {

        /* if the key maps to a valid unicode character, add it to the buffer */
        if (usUnicode != 0)
        {

			if (rEntryBuf.bLenNonTempl > 1)
			{
				/* if aligned right and insertion mode,
				shift the whole buffer 1 space to the left */
				memmove(rEntryBuf.pEbuf->pBuf, rEntryBuf.pEbuf->pBuf + 1,
							(rEntryBuf.bLenNonTempl - 1) * sizeof(UINT16));
			}

			/* poke in the character */
			rEntryBuf.pEbuf->pBuf[rEntryBuf.bNextCharIndex] = usUnicode;
			rEntryBuf.bCharsEntered++;

			/* the buffer may now be full, so re-check for fullness */
            fnUpdateBufferFullStatus();
        }
    }

    /* always update the cursor position/status */

    if (rEntryBuf.fCursorOn != TRUE)
    {
        /* report that the cursor is inactive if it is not used at all for this entry buffer */
        rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;
    }
    else
    {
        /* the cursor is active, but off, if the buffer is disabled or full */
        if ((rEntryBuf.fEnabled != TRUE) || (rEntryBuf.fFull == TRUE))
        {
            rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
        }
        else
        {
            fnCalculateCursorPosition(rEntryBuf.bNextCharIndex); 
        }

    }

    fnUpdateDisplayBuf(rEntryBuf.pEbuf->pDispBuf,rEntryBuf.pEbuf->pBuf,
                       rEntryBuf.bNumTemplateChars,rEntryBuf.pTemplateCharLoc);

    fnChangeShiftState(0);/* after attempting to buffer a key, the shift state always goes back to 0.
                             this allows us to implement a mechanism whereby the user is able to shift
                             a letter by first pressing and releasing the shift key and then pressing
                             the desired letter key.  the shift state automatically returns to normal
                             after that because of this function call. */
    return;

}


/* *************************************************************************
// FUNCTION NAME: 
//          fnBufferUnicode2
//
// DESCRIPTION:
//          Takes a unicode char as input and adds it to the currently established
//          entry buffer in accordance with the way the field is set up.
//
// INPUTS:
//          bKeyCode
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          12/05/2005 Adam Liu  Adapted for FuturePhoenix
//                     Craig DeFilippo(based on fnBufferKey())  Initial version  
// *************************************************************************/
void fnBufferUnicode2(UINT16 usUnicode)
{

    fnUpdateBufferFullStatus();

    /* as long as the buffer is not disabled and not full, lookup the key code
       in the key-unicode lookup table */
    if (((rEntryBuf.fFull != true)  
         ||( (rEntryBuf.fInsert == FALSE) && (rEntryBuf.bNextCharIndex < rEntryBuf.bLenNonTempl))) 
        && (rEntryBuf.fEnabled == TRUE)  )
    {

        /* if the key maps to a valid unicode character, add it to the buffer */
        if (usUnicode != 0)
        {
            /* first make room for the character.  if the buffer is left aligned and using
               insert mode, and we aren't typing the last character at the right edge of the
               buffer, shift everything currently in the buffer, starting from the current
               cursor location, to the right */
            if ( ((rEntryBuf.fInsert == TRUE) && (rEntryBuf.fAlignL == TRUE)) 
                 &&((rEntryBuf.bNextCharIndex + 1) < rEntryBuf.bLenNonTempl))
            {
                memmove( rEntryBuf.pEbuf->pBuf+rEntryBuf.bNextCharIndex+1,
                         rEntryBuf.pEbuf->pBuf+rEntryBuf.bNextCharIndex,
                         (rEntryBuf.bLenNonTempl-rEntryBuf.bNextCharIndex-1)*sizeof(UINT16));
            }

            if ((rEntryBuf.fAlignL == FALSE) && (rEntryBuf.bLenNonTempl > 1))
            {
                if (rEntryBuf.fInsert == TRUE)
                {
                    /* if aligned right and insertion mode, 
                       shift the whole buffer 1 space to the left */
                    memmove(rEntryBuf.pEbuf->pBuf, rEntryBuf.pEbuf->pBuf + 1, 
                            (rEntryBuf.bLenNonTempl - 1) * sizeof(UINT16));
                }
                else
                {
                    /* if aligned right and overtype mode, 
                       shift only the entered chars to the left */
                    if (rEntryBuf.bCharsEntered == rEntryBuf.bLenNonTempl)
                    {
                        rEntryBuf.bCharsEntered--;  /* if buf already full, 
                                                       we lose the leftmost char */
                    }
                    
                    memmove(rEntryBuf.pEbuf->pBuf+(rEntryBuf.bLenNonTempl-rEntryBuf.bCharsEntered)-1,
                            rEntryBuf.pEbuf->pBuf+(rEntryBuf.bLenNonTempl-rEntryBuf.bCharsEntered),
                            rEntryBuf.bCharsEntered * sizeof(UINT16));
                }

            }

            /* poke in the character */
            rEntryBuf.pEbuf->pBuf[rEntryBuf.bNextCharIndex] = usUnicode;
            rEntryBuf.bCharsEntered++;

            /* update the next character position, which changes only if we are aligned left */
            if (rEntryBuf.fAlignL == TRUE)
            {
                rEntryBuf.bNextCharIndex++;
            }

            /* the buffer may now be full, so re-check for fullness */
            fnUpdateBufferFullStatus();
        }
    }

    /* always update the cursor position/status */

    if (rEntryBuf.fCursorOn != TRUE)
    {
        /* report that the cursor is inactive if it is not used at all for this entry buffer */
        rEntryBuf.pEbuf->bCursorLoc = CURSOR_INACTIVE;
    }
    else
    {
        /* the cursor is active, but off, if the buffer is disabled or full */
       if ((rEntryBuf.fEnabled != TRUE) || (rEntryBuf.fFull == TRUE) 
        && ( (rEntryBuf.bNextCharIndex > rEntryBuf.bLenNonTempl) || (rEntryBuf.fInsert == TRUE)) )
        {
            rEntryBuf.pEbuf->bCursorLoc = CURSOR_OFF;
        }
        else
        {
             rEntryBuf.pEbuf->bCursorLoc = 
                 fnCalculateCursorPosition(rEntryBuf.bNextCharIndex-(UINT8)1);

        }

    }

    fnUpdateDisplayBuf(rEntryBuf.pEbuf->pDispBuf,rEntryBuf.pEbuf->pBuf, 
                       rEntryBuf.bNumTemplateChars,rEntryBuf.pTemplateCharLoc);

    fnChangeShiftState(0);/* after attempting to buffer a key, the shift state always goes back to 0.
                             this allows us to implement a mechanism whereby the user is able to shift
                             a letter by first pressing and releasing the shift key and then pressing
                             the desired letter key.  the shift state automatically returns to normal
                             after that because of this function call. */
    return;

}



/* *************************************************************************
// FUNCTION NAME: 
//          fnBufferUnicodeScroll
//
// DESCRIPTION:
//          Takes a unicode char as input and adds it to the currently established
//          entry buffer in accordance with the way the field is set up.
//
// INPUTS:
//          usUnicode
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:
//          Only used in overwrite mode.
//
// MODIFICATION  HISTORY:
//      04/13/2010  Jingwei,LI      Modified to support RTL language writting direction.
//         09/15/2006  Raymond Shen  Modified it to deal with cursor's insert mode.
//         04/10/2006  Oscar /Kan  Initial version  
// *************************************************************************/
void fnBufferUnicodeScroll(UINT16 usUnicode)
{
    UINT8       ubWritingDir;

    ubWritingDir = fnGetWritingDir();

    switch(ubWritingDir)
    {
        case LEFT_TO_RIGHT:
            fnBufferUnicodeLTR(usUnicode);
            break;

        case RIGHT_TO_LEFT:
            fnBufferUnicodeRTL(usUnicode);
            break;

        default:
        break;

    }

    return;

}

/* *************************************************************************
// FUNCTION NAME: 
//          fnSetEntryBufCharIndex
//
// DESCRIPTION:
//          Set the value of rEntryBuf.bNextCharIndex
//
// INPUTS:
//          the new value.
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          07/13/2006   Dicky Sun   Merge from Mega
//          08/30/2005   Kan Jiang   Initial version 
// *************************************************************************/
void  fnSetEntryBufCharIndex(UINT8 ucIndex)
{
      rEntryBuf.bNextCharIndex = ucIndex;
      return;
}

/* *************************************************************************
// FUNCTION NAME: 
//          fnGetEntryBufCharIndex
//
// DESCRIPTION:
//          Get the value of rEntryBuf.bNextCharIndex
//
// INPUTS:
//          none
//
// RETURNS:
//          the value
//
// WARNINGS/NOTES:
//          None
//
// MODIFICATION  HISTORY:
//          06/01/2007  Raymond Shen    Initial version 
//
// *************************************************************************/
UINT8 fnGetEntryBufCharIndex(void)
{
      return (rEntryBuf.bNextCharIndex);
}
/* *************************************************************************
// FUNCTION NAME: 
//      fnCopyTableTextForRTL
//
// DESCRIPTION: 
//      Copy a table text unicode string to the specified location and trim the 
//      trim the redundant spaces that are padded during screen mirror for RTL.   
//
// INPUTS:  
//      pDest - pointer to the destination unicode string. memory allocated
//                  to the string must be large enough to accomodate bLen
//                  unicode characters.
//      wTextID - ID number of the table text string to copy 
//      bMaxLen - maximum length of the table text string
//
// RETURNS: 
//      The number of characters copied.
//
// NOTES:
//
// MODIFICATION HISTORY:
//   04/21/2010  Jingwei,Li     Initial version
//      
// *************************************************************************/
UINT8 fnCopyTableTextForRTL(UINT16 *pDest, 
                            UINT16 wTextID, 
                            UINT8 bMaxLen)
{

    #define MAX_TABLE_TEXT_LEN  64

    UINT16 usRTLTableText[MAX_TABLE_TEXT_LEN];
    UINT8  bCopyLen = 0;

    (void)memset( (void *) usRTLTableText, 0, sizeof(usRTLTableText) );
    //trim the redundant spaces that are padded during screen mirror for RTL.
    if(fnCopyTableText(usRTLTableText, wTextID, MAX_TABLE_TEXT_LEN ) > 0)
    {
//        unistrtrim(usRTLTableText, usRTLTableText);
    }
   // unistrcpy(pDest, usRTLTableText);
    bCopyLen = unistrlen(usRTLTableText)< bMaxLen ? unistrlen(usRTLTableText):bMaxLen;
    memcpy(pDest, usRTLTableText, bCopyLen *sizeof(UINT16));

    return bCopyLen;
    
    #undef MAX_TABLE_TEXT_LEN
}


/** some wrapper functions for local variables **/




