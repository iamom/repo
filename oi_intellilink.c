/************************************************************************
  PROJECT:        Future Phoenix
  COPYRIGHT:      2005, Pitney Bowes, Inc.
  AUTHOR:         
  MODULE NMAE:    Oi_intellilink.c

  DESCRIPTION:    This file contains all types of screen interface functions related to rate download 
                  (hard key, event, pre, post, field init and field refresh)

//-----------------------------------------------------------------------------
 MODIFICATION HISTORY:

  01-Jun-17 Jennifer Hao on FPHX 02.12 cienet branch
      Commented out functions fnfILinkNewRatesEffectiveDate4, fnpSetupLANFirewallSettingMenu,  because they aren't used by G9.
      
 22-May-17 Jennifer Hao on FPHX 02.12 cienet branch
     Commented out  116 functions  fnpPrepDownloadMenu, fnfSwRateUpdateCaption, fnfScheduleForLater,
     fnfSkip,fnfHighPriorityMiddleLine,fnfHowLongWillUpdateTake, fnhkInfraPromptUpdateAvailableSoftKey3,
     fnhkInfraPromptUpdateAvailableSoftKey4, fnhkInfraPromptUpdateAvailableSoftKey5, fnhkInfraPromptUpdateAvailableRightArrowKey,
     fnfCountDownTime, fnevCountDown, fnpPrepDownloadProgress,fngDownloadUpdateProgress,fnfDownloadUpdateProgress,
     fnfDownloadUpdatePercentComplete,fnfDownloadUpdateType,fnfDownloadUpdateFileName,fnfDownloadTimeRemaining,
     fnfDownloadBytesThisFile, fnfConnectionSpeed, nullifyField, fnprepRateInfo, HowManyRateDataFilesAreThere,getFirstRateDataFileIndex,
     getNextRateDataFileIndex, getPreviousRateDataFileIndex,fnfILinkRateFileDescription, fnfILinkRateFileName,fnfILinkRatePartNumber,
     fnfILinkRateVersion,fnfILinkRateServiceLevel,fnfILinkRateEffectiveDate,fnfILinkRateActivationStatus,fnfILinkRateInfoPageUp,
     fnfILinkUpdateInfoPageUp,fnfILinkUpdateChecksum,fnfILinkUpdateFileSize,fnfILinkUpdateServiceLevel,fnfILinkUpdateEffectiveDate
     fnfILinkUpdateVersion,fnfILinkUpdatePartNumber,fnfILinkUpdateFileName,fnfILinkUpdateFileDescription,fnpostUpdateInfo,
     fnhkILinkUpdateInfoCsrDwn,fnhkILinkUpdateInfoCsrUp,fngILinkUpdateInfoDownArrow,fngILinkUpdateInfoUpArrow,fnfILinkUpdateInfoPageDown,
     fnfILinkRateInfoPageDown, fngILinkRateInfoUpArrow,fngILinkRateInfoDownArrow,fnhkILinkRateInfoCsrUp,fnhkILinkRateInfoCsrDwn,
     fnevDlaInstallDone,fnfILinkNewRatesActivationDateX,fnfILinkNewRatesActivationDate1,fnfILinkNewRatesActivationDate2,fnfILinkNewRatesActivationDate3,
     fnfILinkNewRatesActivationDate4,fnfILinkNewRatesDescription4,fnfILinkNewRatesPageUp,fnfILinkNewRatesPageDown,fngILinkNewRatesUpArrow,
     fngILinkNewRatesDownArrow,fnfNewRatesDownloadCountDown,fnhkILinkNewRatesDownloadedCsrUp,fnhkILinkNewRatesDownloadedCsrDwn,
     fnhkILinkNewRatesDownloadedSoftKey5,fnevILinkNewRatesDownloadedCountDown,fnprepUpdateInfo,fnprepDnldLogView,fnpostDnldLogView,
     fnfILinkUpdateLogViewFileDescription,fnfILinkUpdateLogViewFileName,fnfILinkUpdateLogViewPartNumber,fnfILinkUpdateLogViewVersion,
     fnfILinkUpdateLogViewServiceLevel,fnfILinkUpdateLogViewEffectiveDate,fnfILinkUpdateLogViewFileSize,fnfILinkUpdateLogViewChecksum,
     fnfILinkUpdateLogViewDnldDate,fnfILinkUpdateLogViewStatus,fnfILinkUpdateLogViewPageUp,fnfILinkUpdateLogViewPageDown,
     fnfILinkUpdateLogViewTop,fnfILinkUpdateLogViewBottom,fngILinkUpdateLogViewUpArrow,fngILinkUpdateLogViewDownArrow,
     fnhkUpdateLogViewCsrUp,fnhkILinkUpdateLogViewCsrDwn,fnhkUpdateLogViewTop,fnhkUpdateLogViewBottom,fnhkUpdateLogViewClearLog,
     fnevILinkErrLoadingUpdateCountDown,fnfILinkErrLoadingUpdateCountDown,fnpILinkErrLoadingUpdate,fnevExceptionHasOccurredCountDown,
     fnfExceptionHasOccurredCountDown,fnpExceptionHasOccurred,fnhkIlinkHaveAnyRatesBeenDownloaded,fnhkIlinkAreAnyRatesLoaded, 
     fnhkIlinkDoesDoanloadLogExist,fnhkClearDnldLogDoIt,fnevDownloadCompleteCountDown,fnfDownloadCompleteCountDown, 
     fnpIlinkDownloadComplete,fnevNoUpdateAvailableCountDown,fnhkILinkNoUpdatesAvailableOK,fnfNoUpdateAvailableCountDown,
     fnpPrepNoUpdateAvailable,fnhkIlinkRateInfoServiceModeRemoveRate,fnfIlinkRemoveRatePartNumber,fnhkRemoveRateDoIt,fnhkILinkRateComponentRemovedOK,
     variables totalRateDataFiles& currentRateDataFileIndex &noUpdateAvailableContinueMsgSent,and table updateLogViewParserTable because they aren't used by G9.
 
 2011.05.24 Clarisa Bellamy FPHX 02.07 
                              - Comment out a bunch of code that is not used by G900 screens.
 2011.05.24 Clarisa Bellamy FPHX 02.07 
                              - Arg type changed for external function fnRateMakeCompFileName(), 
                              so (now incorrect) typecasts were removed.
                              - Compile out fnprepUpdateInfo if G900. This is not used.
    06/11/2009  Jingwei,Li    Modified fnhkLANFirewallTHostProcesskey() to fix FRACA GMSE00163348.
    05/07/2009  Fred Xu       Added fnpPostSetupLanFirewallDelTrustedHost and 
                              fnpPostSetupLanFirewallViewTrustedHost to fix FRACA GMSE00161384
    03/23/2009  Raymond Shen  Modified fnfILinkNewRatesDescriptionX(), fnpILinkNewRatesDownloaded(), and
                              fnpPostILinkNewRate() to fix display problems on screen 'RateNewInstalled'.
    08/01/2008  Raymond Shen  Added function fnGetlastIntertaskMsg().
    01/11/2008  Raymond Shen  Replaced Printed Date with System Date in function 
                              fnBuildRatesPendingMenuList() and fnfILinkNewRatesPendingDate()
                              to fix FRACA GMSE00135472.
    11/28/2007  Joey Cui      Modified function fnhkLANFirewallTrustedHostsOnly()
                              to fix FRACA GMSE00133740
    11/02/2007  Oscar Wang    Modified function fnhkLANFirewallTHostIPEnterKey().
    10/30/2007  Oscar Wang    Added function fnhkLANFirewallSetting() to fix FRACA
                              GMSE00132339.
                              Modified function fnhkLANFirewallTHostIPEnterKey()
                              and fnhkLANFirewallDeleteSelectedTHosts().
    09/28/2007  Joey Cui      Added codes to implement Firewall Settings 
    09/28/2007  Adam Liu      Merged fnevRemoteRefill() from mega
    04-Jul-2007 de200ko on FPHX 01.05 Secap branch
      Modify fnfILinkEffDateForActiveNPendingX,fnfILinkNewRatesEffectiveDateX
      fnfILinkNewRatesPendingDate to display the date in the right format
    06/22/2007  Joey Cui      Fix FRACA GMSE00122218. Update functions 
                              fnBuildRatesInstalledMenuList()
    06/04/2007  Oscar Wang    Fix FRACA GMSE00121261. Update functions 
                              fnBuildRatesActiveMenuList()
                              fnBuildRatesInstalledMenuList()
                              fnBuildRatesPendingMenuList()
                              fnpPostILinkNewRate()
*
* 05-Apr-07 sa002pe on FPHX 01.04 shelton branch
*   Added definition of THISFILE.
*   Changed all calls for exception logging to use THISFILE instead of __FILE__.
*
    04/04/2007  Oscar Wang     Added functions
                                fnfILinkEffDateForActiveNPendingX
                                fnfILinkEffDateForActiveNPending1
                                fnfILinkEffDateForActiveNPending2
                                fnfILinkEffDateForActiveNPending3
                                fnfILinkEffDateForActiveNPending4
                               for effective date display in active/pending screens.
    03/29/2007  Oscar Wang     Changes for Rates Change Prompt Feature.
*************************************************************************/
#ifndef INTELLILINK_C
#define INTELLILINK_C 

#include "..\include\rftypes.h"
#include "..\include\oitpriv.h"
#include "..\include\vstack.h"
#include "..\include\download.h"
#include "..\include\intellilink.h"
#include "..\include\oicommon.h"
#include "..\include\unistring.h"
#include "..\include\rateadpt.h"
#include "..\include\urlencode.h"
#include "fcntl.h"
#include "..\include\oi_IntelliLink.h"
#include "..\include\fnames.h"
#include <string.h>
#include <stdlib.h>
#include "posix.h"
#include "oicmenucontrol.h"
#include "custdat.h"
#include "..\include\oifields.h"
#include "..\include\features.h"
#include "Oientry.h"
#include "flashutil.h"

#define THISFILE "oi_intellilink.c"

//new rates downloaded screen
int newRatesDownloadedIndex;    //reflects the total # of rates files
int currentSlotIndex;
static FILE *newRatesDownloadedFilePtr;
NEW_RATES_INFO newRatesInfo[20];
//#define ILINK_NEWRATES_DOWNLOADED_SLOTS 8
static BOOL iLinkNewRatesDownloadedMsgSent = FALSE;


unsigned char bDescLoaded;

NEW_RATES_UPDATES_INFO  NewRatesUpdatesInfo[MAX_CMOS_RATE_COMPONENTS];// for storing of Actived and effictive rates
unsigned char  bNumRatesUpdatesInfo = 0;

#define RATES_PENDING_DATE_BEFORE   5
#define RATES_ACTIVE_DATE_AFTER     3
unsigned short bNewRatesPartNO[MAX_CMOS_RATE_COMPONENTS][SIZE_CMOS_PARTNO];

extern S_MENU_TABLE pSMenuTable;
extern CMOSRATECOMPSMAP CMOSRateCompsMap;  //The Active and Pending Rate Data Elements for the system.
extern CMOSDownloadCtrlBuf  CmosDownloadCtrlBuf;
extern GENERIC_UPDATE_INFO currentFileInfo;
extern unsigned long lwModemConnectionSpeed;

extern  BOOL fCMOSShowRateActive;
extern   BOOL fCMOSShowRateEffective;
extern   BOOL fCMOSUserDenyRateActivePrompt;
extern    BOOL fCMOSUserDenyRateEffectivePrompt;
extern    BOOL  fCMOSDateAdvUseNewRates;

extern CMOSRATECOMPSMAP CMOSRateCompsMap;  // The Active and Pending Rate Data Elements for the system.

extern ENTRY_BUFFER_CTRL    rEntryBuf;

DATETIME newRatesLoadTime;
#define BUFFER_SIZE 2048

INTERTASK lastIntertaskMsg;


RATE_FILE_DESCRIPTION rateFileDescriptionTable[ILINK_NEWRATES_DOWNLOADED_SLOTS] = {
    {"P7US001", 1},
    {"P7US002", 2}
    };


/*Start of LAN Firewall Configuration Feature*/
#define IP_FIELD1              0           
#define IP_FIELD2              1
#define IP_FIELD3              2
#define IP_FIELD4              3

TH_HOSTS TH_hosts;     /*Array of Trusted hosts*/

BOOL bLANFirewallTHostStatus = LAN_FW_THOST_ALL;

OIHostIpInfo  OIHostIpInfoBuf; /* Buffer for add/edit Trusted Host IP info */

UINT8 TH_DelIndicators[TH_LIST_SZ] = {0,0};

/*  add by Joey Cui
    when sHostIndex < 0, means add new Host
    when sHostIndex >=0, means edit Host, and it points to the index of Host menu 
*/
SINT16 sHostIndex = -1;    

extern BOOL bCMOSLANFirewallPingEnabled;

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//maybe going to move these to... vsatack?
BOOL updateContainsNewRates(FILE_UPDATE_LINK *fileUpdateBasePtr)
{
FILE_UPDATE_LINK *tempPtr = fileUpdateBasePtr;

    while(tempPtr)
    {
        if(!strcmp(tempPtr->updateType, "RTE"))
            return TRUE;
            
        tempPtr = tempPtr->nextFileUpdateLinkPtr;       
    }

    return FALSE;
}

int howManyFileTypeUpdates(FILE_UPDATE_LINK *fileUpdateBasePtr)
{
FILE_UPDATE_LINK *tempPtr = fileUpdateBasePtr;
int numberOfFileTypeUpdates = 0;

    while(tempPtr)
    {
        numberOfFileTypeUpdates++;  
        tempPtr = tempPtr->nextFileUpdateLinkPtr;       
    }

    return numberOfFileTypeUpdates;
}
//end maybe move

//Functions and data associated with screen "InfraPromptUpdateAvailable"
static BOOL infraPromptUpdateAvailableMsgSent = FALSE;
static int screenTimer = 0; //usually set to 20 seconds by pre screen func
int totalBytesOfUpdate = 0;   
static BOOL updateContainsRates;    //used later too...

/* *************************************************************************
// FUNCTION NAME: fnLANFirewallTrustedHostIndexSelect
// DESCRIPTION: static functions that know how to handle soft key presses for  
//                        view/edit Trusted Hosts               
//
// AUTHOR: Joey Cui
//
// INPUTS:  Standard hard key function inputs.
// NEXT SCREEN:  
// *************************************************************************/
static T_SCREEN_ID fnLANFirewallTrustedHostIndexSelect(UINT8 bKeyCode, 
                                                UINT8 bNumNext,
                                                T_SCREEN_ID *pNextScreens,
                                                UINT16 usSlotIndex);

/* *************************************************************************
// FUNCTION NAME: fnLANFirewallGetTrustedHostNum
// DESCRIPTION: static functions that know how many hosts are in the file
//
// AUTHOR: Joey Cui
//
// INPUTS: Standard hard key function inputs.
// Notes : Include both file and temp file
// *************************************************************************/
static UINT16 fnLANFirewallGetTrustedHostNum();


/**************************************************************************
// FUNCTION NAME: 
//          fnGetlastIntertaskMsg
//
// DESCRIPTION:
//         Get the pointer of the variable lastIntertaskMsg
//
// AUTHOR:
//          Kan Jiang
//
// INPUTS:
//          None
//
// RETURNS:
//       the pointer of lastIntertaskMsg
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//     
// *************************************************************************/
INTERTASK *fnGetlastIntertaskMsg(void)
{
        return (&lastIntertaskMsg);
}

//---------------------------------------------------------------------------------------------------------------------------------------------------------
//downloading screen



static int downloadprogress;
static int percentComplete;
int bytesDownloadedSoFar = 0;   
extern GENERIC_UPDATE_INFO currentFileInfo;



char fnpILinkNewRatesDownloaded( void (** pContinuationFcn)(), unsigned long *pMsgsAwaited, unsigned short *pNextScr )
{
    *pNextScr = 0;      /* leave next screen unchanged */
    
    return (PREPOST_COMPLETE);
}
                     


BOOL fnfILinkNewRatesEffectiveDateX(UINT16 usOffset,
                UINT16 *pFieldDest,
                            UINT8   ucLen, 
                            UINT8   ucFieldCtrl, 
                            UINT8   ucNumTableItems, 
                            UINT8  *pTableTextIDs, 
                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}

BOOL fnfILinkNewRatesEffectiveDate1(UINT16 *pFieldDest, 
                            UINT8   ucLen, 
                            UINT8   ucFieldCtrl, 
                            UINT8   ucNumTableItems, 
                            UINT8  *pTableTextIDs, 
                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}

BOOL fnfILinkNewRatesEffectiveDate2(UINT16 *pFieldDest, 
                            UINT8   ucLen, 
                            UINT8   ucFieldCtrl, 
                            UINT8   ucNumTableItems, 
                            UINT8  *pTableTextIDs, 
                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}

BOOL fnfILinkNewRatesEffectiveDate3(UINT16 *pFieldDest, 
                            UINT8   ucLen, 
                            UINT8   ucFieldCtrl, 
                            UINT8   ucNumTableItems, 
                            UINT8  *pTableTextIDs, 
                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}

BOOL fnfILinkNewRatesEffectiveDate4(UINT16 *pFieldDest, 
                            UINT8   ucLen, 
                            UINT8   ucFieldCtrl, 
                            UINT8   ucNumTableItems, 
                            UINT8  *pTableTextIDs, 
                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}

BOOL fnfILinkEffDateForActiveNPendingX(UINT16 usOffset,
                UINT16 *pFieldDest, 
                            UINT8   ucLen, 
                            UINT8   ucFieldCtrl, 
                            UINT8   ucNumTableItems, 
                            UINT8  *pTableTextIDs, 
                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}

BOOL fnfILinkEffDateForActiveNPending1(UINT16 *pFieldDest,
                            UINT8   ucLen,
                            UINT8   ucFieldCtrl,
                            UINT8   ucNumTableItems,
                            UINT8  *pTableTextIDs,
                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}

BOOL fnfILinkEffDateForActiveNPending2(UINT16 *pFieldDest,
                            UINT8   ucLen,
                            UINT8   ucFieldCtrl,
                            UINT8   ucNumTableItems,
                            UINT8  *pTableTextIDs,
                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}

BOOL fnfILinkEffDateForActiveNPending3(UINT16 *pFieldDest,
                            UINT8   ucLen,
                            UINT8   ucFieldCtrl,
                            UINT8   ucNumTableItems,
                            UINT8  *pTableTextIDs,
                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}

BOOL fnfILinkEffDateForActiveNPending4(UINT16 *pFieldDest,
                            UINT8   ucLen,
                            UINT8   ucFieldCtrl,
                            UINT8   ucNumTableItems,
                            UINT8  *pTableTextIDs,
                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}

BOOL fnfILinkNewRatesDescriptionX(UINT16 usOffset,
                UINT16 *pFieldDest,
                            UINT8   ucLen,
                            UINT8   ucFieldCtrl,
                            UINT8   ucNumTableItems,
                            UINT8  *pTableTextIDs,
                            UINT8  *pAttributes )
 {
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}

BOOL fnfILinkNewRatesDescription1(UINT16 *pFieldDest,
                            UINT8   ucLen,
                            UINT8   ucFieldCtrl,
                            UINT8   ucNumTableItems,
                            UINT8  *pTableTextIDs,
                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}

BOOL fnfILinkNewRatesDescription2(UINT16 *pFieldDest,
                            UINT8   ucLen,
                            UINT8   ucFieldCtrl,
                            UINT8   ucNumTableItems,
                            UINT8  *pTableTextIDs,
                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}

BOOL fnfILinkNewRatesDescription3(UINT16 *pFieldDest,
                            UINT8   ucLen,
                            UINT8   ucFieldCtrl,
                            UINT8   ucNumTableItems,
                            UINT8  *pTableTextIDs,
                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}


BOOL fnfILinkNewRatesPendingDate(UINT16 *pFieldDest,
                            UINT8   ucLen,
                            UINT8   ucFieldCtrl,
                            UINT8   ucNumTableItems,
                            UINT8  *pTableTextIDs, 
                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, ucLen);
    return ((BOOL) SUCCESSFUL);
}


//utility function to deal with screen mode change not calling meter error network exception prescreen function
void loadScreenTimer(int seconds)
{
    screenTimer = seconds;   
}



//for remote refill
unsigned short fnevRemoteRefill(unsigned char bNumNext, unsigned short *pNextScreens, INTERTASK *pIntertask)
{
    return 0;   // Should be no screen transition
}


char fnpILinkNewRatesActive( void (** pContinuationFcn)(), unsigned long *pMsgsAwaited, unsigned short *pNextScr )
{    
    *pNextScr = 0;      /* leave next screen unchanged */
    return (PREPOST_COMPLETE);
}


/* *************************************************************************
// FUNCTION NAME: 
//
// DESCRIPTION:
//    
//      
//
// INPUTS:
//      
//
// RETURNS:
//
// WARNINGS/NOTES:
//
// TEXT TABLE USAGE:
//     
//
// MODIFICATION HISTORY:
//       Zhang  Yingbo  Initial version
// *************************************************************************/
char fnpILinkNewRatesEffective( void (** pContinuationFcn)(), unsigned long *pMsgsAwaited, unsigned short *pNextScr )
{    
    *pNextScr = 0;      /* leave next screen unchanged */
    
    return (PREPOST_COMPLETE);
}

char fnpPostILinkNewRate( void      (** pContinuationFcn)(), 
                      unsigned long         *   pMsgsAwaited)
{
    return (PREPOST_COMPLETE);
}

unsigned short fnhkDenyRatesPendingPrompt(unsigned char bKeyCode, unsigned char bNumNext, unsigned short *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}

unsigned short fnhkDenyRatesActivePrompt(unsigned char bKeyCode, unsigned char bNumNext, unsigned short *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}


//Pre-Posts
/* *************************************************************************/
// FUNCTION NAME:   fnpSetupLANFirewallTrustedHosts
// DESCRIPTION:     Pre function that sets up the LAN Firewall Trusted Hosts
// AUTHOR:          Zhang Yingbo
// INPUTS:          Standard pre function inputs.
// RETURNS:         PREPOST_COMPLETE, in all cases
// CONTINUATION FUNCTION:  none
// NEXT SCREEN:     unchanged
// NOTES:           
/* *************************************************************************/
SINT8 fnpSetupLANFirewallTrustedHosts(  void (** pContinuationFcn)(), 
                                        UINT32 *pMsgsAwaited,
                                        T_SCREEN_ID *pNextScr )
{   
    *pNextScr = 0;      /* leave next screen unchanged */
    return (PREPOST_COMPLETE);
}

/* *************************************************************************/
// FUNCTION NAME:   fnpSetupLANFirewallAddTrustedHost
// DESCRIPTION:     Pre function that sets up the LAN Firewall Trusted Hosts
// AUTHOR:          Zhang Yingbo
// INPUTS:          Standard pre function inputs.
// RETURNS:         PREPOST_COMPLETE, in all cases
// CONTINUATION FUNCTION:  none
// NEXT SCREEN:     unchanged
// NOTES:   
// MODIFICATION  HISTORY:
//  06/09/2010  Jingwei,Li    Call fnSetIPAddressInput() to set IP address entry flag.
/* *************************************************************************/
SINT8 fnpSetupLANFirewallAddTrustedHost(void (** pContinuationFcn)(), 
                                        UINT32 *pMsgsAwaited,
                                        T_SCREEN_ID *pNextScr )
{   
    /* leave next screen unchanged */
    *pNextScr = 0;
    
    return (PREPOST_COMPLETE);
}

/* *************************************************************************/
// FUNCTION NAME:   fnpSetupLANFirewallDelTrustedHost
// DESCRIPTION:     Pre function that sets up the LAN Firewall Trusted Hosts
// AUTHOR:          Zhang Yingbo
// INPUTS:          Standard pre function inputs.
// RETURNS:         PREPOST_COMPLETE, in all cases
// CONTINUATION FUNCTION:  none
// NEXT SCREEN:     unchanged
// NOTES:           
/* *************************************************************************/
SINT8 fnpSetupLANFirewallDelTrustedHost(void (** pContinuationFcn)(), 
                                        UINT32 *pMsgsAwaited,
                                        T_SCREEN_ID *pNextScr )
{   
     // Stay on same screen
        *pNextScr = 0;
    
    return (PREPOST_COMPLETE);
    
}

/* *************************************************************************/
// FUNCTION NAME:   fnpSetupLANFirewallViewTrustedHost
// DESCRIPTION:     Pre function that sets up the LAN Firewall Trusted Hosts
// AUTHOR:          Zhang Yingbo
// INPUTS:          Standard pre function inputs.
// RETURNS:         PREPOST_COMPLETE, in all cases
// CONTINUATION FUNCTION:  none
// NEXT SCREEN:     unchanged
// NOTES:           
/* *************************************************************************/
SINT8 fnpSetupLANFirewallViewTrustedHost(   void (** pContinuationFcn)(), 
                                            UINT32 *pMsgsAwaited,
                                            T_SCREEN_ID *pNextScr )
{   
     // Stay on same screen
        *pNextScr = 0;
    
    return (PREPOST_COMPLETE);
    
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostSetupLanFirewallDelTrustedHost
//
// DESCRIPTION: 
//      Post function that is called when leaving the firewall delete trusted host
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/07/2009  Fred Xu     Initial version 
//
// *************************************************************************/
SINT8 fnpPostSetupLanFirewallDelTrustedHost (void       (** pContinuationFcn)(), 
                            UINT32  *   pMsgsAwaited)
{
    return (BOOL)PREPOST_COMPLETE;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnpPostSetupLanFirewallViewTrustedHost
//
// DESCRIPTION: 
//      Post function that is called when leaving the firewall view trusted host
//
// INPUTS:
//      Standard screen post function inputs.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      Always PREPOST_COMPLETE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  05/07/2006  Fred Xu     Initial version 
//
// *************************************************************************/
SINT8 fnpPostSetupLanFirewallViewTrustedHost (void      (** pContinuationFcn)(), 
                            UINT32  *   pMsgsAwaited)
{
    return (BOOL)PREPOST_COMPLETE;
}


//Fields
/* *************************************************************************
// FUNCTION NAME:   fnfLANFirewallPingSetting
// DESCRIPTION: 
//
// AUTHOR: Zhang Yingbo
//
// INPUTS:  Standard field function outputs.
// NOTES:   
// *************************************************************************/
BOOL fnfLANFirewallPingSetting(UINT16      *pFieldDest, 
                             UINT8       bLen, 
                             UINT8       bFieldCtrl,
                             UINT8       bNumTableItems, 
                             UINT8       *pTableTextIDs,
                             UINT8       *pAttributes)
{ 
    SET_SPACE_UNICODE(pFieldDest, bLen);
    return ((BOOL)FAILURE);
}


/* *************************************************************************
// FUNCTION NAME:   fnfLANFirewallTrustedHostSetting
// DESCRIPTION: 
//
// AUTHOR: Zhang Yingbo
//
// INPUTS:  Standard field function outputs.
// NOTES:   
// *************************************************************************/
BOOL fnfLANFirewallTrustedHostSetting(UINT16      *pFieldDest, 
                              UINT8       bLen, 
                              UINT8       bFieldCtrl,
                              UINT8       bNumTableItems, 
                              UINT8       *pTableTextIDs,
                              UINT8       *pAttributes)
{ 
    SET_SPACE_UNICODE(pFieldDest, bLen);
    return ((BOOL)FAILURE);
}


/* *************************************************************************
// FUNCTION NAME: fnfLANFirewallTrustNoneIndicator
// DESCRIPTION: Output field function 
//
// AUTHOR: Oscar Wang
//
// INPUTS:  Standard field function inputs.
// *************************************************************************/
BOOL fnfLANFirewallTrustNoneIndicator( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, bLen);
    return ((BOOL)FAILURE);
}

/* *************************************************************************
// FUNCTION NAME: fnfLANFirewallTrustAllIndicator
// DESCRIPTION: Output field function 
//
// AUTHOR: Oscar Wang
//
// INPUTS:  Standard field function inputs.
// *************************************************************************/
BOOL fnfLANFirewallTrustAllIndicator( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, bLen);
    return ((BOOL)FAILURE);
}

/* *************************************************************************
// FUNCTION NAME: fnfLANFirewallTrustHostOnlyIndicator
// DESCRIPTION: Output field function 
//
// AUTHOR: Oscar Wang
//
// INPUTS:  Standard field function inputs.
// *************************************************************************/
BOOL fnfLANFirewallTrustHostOnlyIndicator( UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, bLen);
    return ((BOOL)FAILURE);
}

/* *************************************************************************
// FUNCTION NAME: fnfLANFirewallAddTrustedHostEntryInit
// DESCRIPTION: 
// AUTHOR: Zhang Yingbo
//
// INPUTS:  Standard field function inputs.
// TEXT TABLE USAGE:  None
// MODIFICATION  HISTORY:
//  06/09/2010  Jingwei,Li    Call fnBufferUnicodeLTR() to buffer IP address.
// *************************************************************************/
BOOL fnfLANFirewallAddTrustedHostEntryInit (UINT16 *pFieldDest, 
                                            UINT8   bLen, 
                                            UINT8   bFieldCtrl, 
                                            UINT8   bNumTableItems, 
                                            UINT8  *pTableTextIDs, 
                                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, bLen);
    return ((BOOL)FAILURE);
}

/* *************************************************************************
// FUNCTION NAME: fnfTHostSoftKeyIndicator1 - 4
// DESCRIPTION: Output field function that displays the soft key indicator
//              on for soft key 1 - 4.
//
// AUTHOR: Zhang Yingbo
//
// INPUTS:  Standard field function inputs.
// TEXT TABLE USAGE:    1st string is displayed if using the " Host Unselected" Soft Key Indicator
//                      2nd string is displayed if using the "Host Selected" Soft Key Indicator
// *************************************************************************/
BOOL fnfTHostSoftKeyIndicator1( UINT16 *pFieldDest,  
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, bLen);
    return ((BOOL)FAILURE);
}


BOOL fnfTHostSoftKeyIndicator2( UINT16 *pFieldDest,  
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, bLen);
    return ((BOOL)FAILURE);
}



BOOL fnfTHostSoftKeyIndicator3( UINT16 *pFieldDest,  
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, bLen);
    return ((BOOL)FAILURE);
}



BOOL fnfTHostSoftKeyIndicator4( UINT16 *pFieldDest,  
                                UINT8   bLen, 
                                UINT8   bFieldCtrl, 
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, bLen);
    return ((BOOL)FAILURE);
}

/* *************************************************************************
// FUNCTION NAME: fnfLANFirewallTrustedHostToDelete
// DESCRIPTION: Output field function that displays the number of the host
//              to be deleted.
//
// AUTHOR: Joey Cui
//
// INPUTS:  Standard field function inputs.
// TEXT TABLE USAGE:    
// *************************************************************************/
BOOL fnfLANFirewallTrustedHostToDelete( UINT16 *pFieldDest,  
                                        UINT8   bLen, 
                                        UINT8   bFieldCtrl, 
                                        UINT8   bNumTableItems, 
                                        UINT8  *pTableTextIDs, 
                                        UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, bLen);
    return ((BOOL)FAILURE);
}


/* *************************************************************************
// FUNCTION NAME: fnfTHostSoftKeyIndicator1 - 4
// DESCRIPTION: Output field function that displays the title of the screen.
//
// AUTHOR: Joey Cui
//
// INPUTS:  Standard field function inputs.
// TEXT TABLE USAGE:    
// *************************************************************************/
BOOL fnfLANFirewallTrustedHostEnterCaption( UINT16 *pFieldDest,  
                                            UINT8   bLen, 
                                            UINT8   bFieldCtrl, 
                                            UINT8   bNumTableItems, 
                                            UINT8  *pTableTextIDs, 
                                            UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, bLen);
    return ((BOOL)FAILURE);
}

//Hard-Keys
/* *************************************************************************
// FUNCTION NAME: fnhkLANFirewallSetPingOn
// DESCRIPTION: 
// AUTHOR: Zhang Yingbo
//
// INPUTS: Standard hard key function inputs (format 2).
// OUTPUTS:
//
// NOTES:
// 
// *************************************************************************/
T_SCREEN_ID fnhkLANFirewallSetPing(UINT8          bKeyCode, 
                                    UINT8          bNumNext, 
                                    T_SCREEN_ID *  pNextScreens)
{ 
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}


/* *************************************************************************
// FUNCTION NAME: fnhkRetrieveDefaultLANFirewallSettings
// DESCRIPTION: 
// AUTHOR: Zhang Yingbo
//
// INPUTS: Standard hard key function inputs (format 2).
// OUTPUTS:
//
// NOTES:
// 
// *************************************************************************/
T_SCREEN_ID fnhkRetrieveDefaultLANFirewallSettings(UINT8          bKeyCode, 
                                     UINT8          bNumNext, 
                                     T_SCREEN_ID *  pNextScreens)
{
    T_SCREEN_ID     usNext = 0;
    return usNext;
}

/* *************************************************************************
// FUNCTION NAME: fnhkLANFirewallTrustAll
// DESCRIPTION: 
// AUTHOR: Zhang Yingbo
//
// INPUTS: Standard hard key function inputs (format 2).
// OUTPUTS:
//
// NOTES:
// 
// *************************************************************************/
T_SCREEN_ID fnhkLANFirewallTrustAll(UINT8          bKeyCode, 
                                     UINT8          bNumNext, 
                                     T_SCREEN_ID *  pNextScreens)
{
    T_SCREEN_ID     usNext = 0;
    return usNext;
    
}

/* *************************************************************************
// FUNCTION NAME: fnhkLANFirewallTrustNone
// DESCRIPTION: 
// AUTHOR: Zhang Yingbo
//
// INPUTS: Standard hard key function inputs (format 2).
// OUTPUTS:
//
// NOTES:
// 
// *************************************************************************/
 T_SCREEN_ID fnhkLANFirewallTrustNone(UINT8          bKeyCode, 
                                      UINT8          bNumNext, 
                                      T_SCREEN_ID *  pNextScreens)
 {
    T_SCREEN_ID     usNext = 0;
    return usNext;
}


/* *************************************************************************
// FUNCTION NAME: fnhkLANFirewallTrustedHostsOnly
// DESCRIPTION: 
// AUTHOR: Zhang Yingbo
//
// INPUTS: Standard hard key function inputs (format 2).
// OUTPUTS:
//
// NOTES:
// 
// *************************************************************************/
 T_SCREEN_ID fnhkLANFirewallTrustedHostsOnly(UINT8          bKeyCode, 
                                      UINT8          bNumNext, 
                                      T_SCREEN_ID *  pNextScreens)
{
	    T_SCREEN_ID wNext = 0 ;
	    return (wNext);
}


/* *************************************************************************
// FUNCTION NAME: fnhkLANFirewallTHostAdd
// DESCRIPTION: 
// AUTHOR: Zhang Yingbo
//
// INPUTS: Standard hard key function inputs (format 2).
// OUTPUTS:
//
// NOTES:
// 
// *************************************************************************/
 T_SCREEN_ID fnhkLANFirewallTHostAdd(   UINT8 bKeyCode, 
                                        UINT8 bNumNext,
                                        T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;
    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: fnhkLANFirewallTHostVD
// DESCRIPTION: 
// AUTHOR: Zhang Yingbo
//
// INPUTS: Standard hard key function inputs (format 2).
// OUTPUTS:
//
// NOTES:
// 
// *************************************************************************/
 T_SCREEN_ID fnhkLANFirewallTHostVD(UINT8 bKeyCode, 
                                    UINT8 bNumNext,
                                    T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID usNext = 0;
    return (usNext);
}

/******************************************************************************
   FUNCTION NAME: fnhkLANFirewallTHostIPDotEnter
   AUTHOR       : Zhang Yingbo
   DESCRIPTION  : 
   PARAMETERS   : Standard hard key function inputs (format 2).
   NEXT SCREEN  : unchanged
   RETURN       : None
   NOTES        : 
   MODIFICATION  HISTORY:
     06/09/2010  Jingwei,Li    Call fnBufferUnicodeLTR() to buffer IP address.
******************************************************************************/
T_SCREEN_ID fnhkLANFirewallTHostIPDotEnter( UINT8 bKeyCode,
                                            UINT8 bNumNext,
                                            T_SCREEN_ID *pNextScreens)
{

    T_SCREEN_ID     usNext = 0;
    return (usNext);
}

/******************************************************************************
   FUNCTION NAME: fnhkLANFirewallTHostProcesskey
   AUTHOR       : Zhang Yingbo
   DESCRIPTION  : 
   PARAMETERS   : Standard hard key function inputs (format 2).
   NEXT SCREEN  : unchanged
   RETURN       : None
   NOTES        : 
   MODIFICATION HISTORY:
   06/09/2010  Jingwei,Li    Call fnBufferKeyLTR() to buffer IP address.
   06/10/2009    Jingwei,Li  Fix fraca GMSE00163348,don't allow to entry fixed 
                             zero previous non-zero value.
******************************************************************************/
T_SCREEN_ID fnhkLANFirewallTHostProcesskey(  UINT8 bKeyCode,
                                                UINT8 bNumNext,
                                                T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}

/******************************************************************************
   FUNCTION NAME: fnhkLANFirewallTHostIPDeleteChar
   AUTHOR       : Zhang Yingbo
   DESCRIPTION  : 
   PARAMETERS   : Standard hard key function inputs (format 2).
   NEXT SCREEN  : unchanged
   RETURN       : None
   NOTES        : 
   MODIFICATION HISTORY:
   06/09/2010  Jingwei,Li    Call fnDeleteCharacterLTR() to delete char from IP 
                             address entry buffer.
******************************************************************************/
T_SCREEN_ID fnhkLANFirewallTHostIPDeleteChar(UINT8 bKeyCode, 
                                                UINT8 bNumNext,
                                                T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}

/******************************************************************************
   FUNCTION NAME: fnhkLANFirewallTHostIPClearKey
   AUTHOR       : Zhang Yingbo
   DESCRIPTION  : 
   PARAMETERS   : Standard hard key function inputs (format 2).
   NEXT SCREEN  : unchanged
   RETURN       : None
   NOTES        : 
******************************************************************************/
T_SCREEN_ID fnhkLANFirewallTHostIPClearKey( UINT8 bKeyCode, 
                                            UINT8 bNumNext,
                                            T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}

/******************************************************************************
   FUNCTION NAME: fnhkLANFirewallTHostIPEnterKey
   AUTHOR       : Zhang Yingbo
   DESCRIPTION  : 
   PARAMETERS   : Standard hard key function inputs (format 2).
   NEXT SCREEN  : unchanged
   RETURN       : None
   NOTES        : 
******************************************************************************/
T_SCREEN_ID fnhkLANFirewallTHostIPEnterKey( UINT8 bKeyCode, 
                                            UINT8 bNumNext,
                                            T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}

/* *************************************************************************
// FUNCTION NAME: fnhkSMenuProcessTHostsSoft1 - 5
// DESCRIPTION: Key functions that know how to handle soft key presses for delete 
//                        Trusted Hosts             
//
// AUTHOR: Zhang Yingbo
//
// INPUTS:  Standard hard key function inputs.
// NEXT SCREEN:  
// *************************************************************************/
T_SCREEN_ID fnhkSMenuProcessTHostsSoft1(UINT8 bKeyCode, 
                                        UINT8 bNumNext,
                                        T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}

T_SCREEN_ID fnhkSMenuProcessTHostsSoft2(UINT8 bKeyCode, 
                                        UINT8 bNumNext,
                                        T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}

T_SCREEN_ID fnhkSMenuProcessTHostsSoft3(UINT8 bKeyCode, 
                                        UINT8 bNumNext,
                                        T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}

T_SCREEN_ID fnhkSMenuProcessTHostsSoft4(UINT8 bKeyCode, 
                                        UINT8 bNumNext,
                                        T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}


/******************************************************************************
   FUNCTION NAME: fnhkLANFirewallFinishTHostSelection
   AUTHOR       : Zhang Yingbo
   DESCRIPTION  : 
   PARAMETERS   : Standard hard key function inputs (format 2).
   NEXT SCREEN  : unchanged
   RETURN       : None
   NOTES        : 
******************************************************************************/
T_SCREEN_ID fnhkLANFirewallFinishTHostSelection(UINT8 bKeyCode, 
                                                UINT8 bNumNext,
                                                T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}

/******************************************************************************
   FUNCTION NAME: fnhkLANFirewallDeleteSelectedTHosts
   AUTHOR       : Zhang Yingbo
   DESCRIPTION  : 
   PARAMETERS   : Standard hard key function inputs (format 2).
   NEXT SCREEN  : unchanged
   RETURN       : None
   NOTES        : 
******************************************************************************/
T_SCREEN_ID fnhkLANFirewallDeleteSelectedTHosts(UINT8 bKeyCode, 
                                                UINT8 bNumNext,
                                                T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}

/* *************************************************************************
// FUNCTION NAME: fnhkLANFirewallTrustedHostIndexSelect1 - 5
// DESCRIPTION: Key functions that know how to handle soft key presses for  
//                        view/edit Trusted Hosts               
//
// AUTHOR: Joey Cui
//
// INPUTS:  Standard hard key function inputs.
// NEXT SCREEN:  
// *************************************************************************/
T_SCREEN_ID fnhkLANFirewallTrustedHostIndexSelect1( UINT8 bKeyCode, 
                                                    UINT8 bNumNext,
                                                    T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}

T_SCREEN_ID fnhkLANFirewallTrustedHostIndexSelect2( UINT8 bKeyCode, 
                                                    UINT8 bNumNext,
                                                    T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}

T_SCREEN_ID fnhkLANFirewallTrustedHostIndexSelect3( UINT8 bKeyCode, 
                                                    UINT8 bNumNext,
                                                    T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}

T_SCREEN_ID fnhkLANFirewallTrustedHostIndexSelect4( UINT8 bKeyCode, 
                                                    UINT8 bNumNext,
                                                    T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}

/* *************************************************************************
// FUNCTION NAME: fnhkLANFirewallSetting
// DESCRIPTION: Key functions that go to firewall setting.              
//
// AUTHOR: Oscar Wang
//
// INPUTS:  Standard hard key function inputs.
// NEXT SCREEN:  
// *************************************************************************/
T_SCREEN_ID fnhkLANFirewallSetting( UINT8 bKeyCode, 
                                                    UINT8 bNumNext,
                                                    T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID wNext = 0 ;
    return (wNext);
}


#endif

