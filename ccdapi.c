/******************************************************************************
   PROJECT:        Horizon CSD2-3
   MODULE NAME:    ccdapi.c

   DESCRIPTION:
        Library of functions to support CCD file validation, installation and access

 ----------------------------------------------------------------------
               Copyright (c) 2017 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
*******************************************************************************/

#ifndef CCD_API_C
#define CCD_API_C

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "nucleus.h"
#include "posix.h"

#include "pbioext.h"
#include "pcdisk.h"
#include "global.h"

#include "fcntl.h"
#include "..\include\rftypes.h"
#include "..\include\lcdutils.h"
#include "..\include\ostypes.h"
#include "..\include\padlcard.h"
#include "..\include\remotelog.h"
#include "..\include\features.h"
#include "..\include\exception.h"
#include "..\include\custdat.h"
#include "..\include\sig.h"
#include "sig.h"
#include "networking/nu_networking.h"
#include "connectivity/nu_usb.h"
#include "wsox_tx_queue.h"
#include "cJSON.h"
#include "pbos.h"
#include "ossetup.h"
#include "cJSON.h"
#include "debugTask.h"
#include "errcode.h"
#include "urlencode.h"  // findString()
#include "debugTask.h"
#include "flashutil.h"

#include "ccdapi.h"
#include "rateadpt.h"
#include "cmos.h"
#include "aiservic.h"
#include "grapext.h"
#include "fwrapper.h"

#define THISFILE "ccdapi.c"



// --------------------------------------------------------------------------
//   External function prototypes:
//      For functions that are not prototyped in include files yet.
// --------------------------------------------------------------------------
extern char *HexToAscii( ushort hexVal, char *fCode );


// --------------------------------------------------------------------------
//   External global data:
// --------------------------------------------------------------------------
extern CMOS_Signature           CMOSSignature;
extern CMOSRATECOMPSMAP CMOSRateCompsMap;  //The Active and Pending Rate Data Elements for the system.
extern unsigned char bNumPreviouslyActiveRates;


// --------------------------------------------------------------------------
//   Private data:
// --------------------------------------------------------------------------


// This points to a malloc'd buffer for the CCD file in RAM
//  Need to free the memory and clear the pointer at the same time.
static char                *ccdBuffer = NULL_PTR;
static CCD_CUSTOMER_INFO   *ccdCustInfo_p = NULL_PTR;
static UINT8			   ccdVers;

// These just point to specific parts of the CCD file within the same 
//  RAM area pointed to by ccdCustInfo_P:
static CCD_FCODE_INFO      *ccdFcodeInfo_p = NULL_PTR;
static CCD_GFX_INFO        *ccdGfxInfo_p = NULL_PTR;
static CCD_RATE_INFO       *ccdRateInfo_p = NULL_PTR;


static T_CCD_DONE_FLAGS bmCcdDoneFlags    = 0;
static char    ccdFileName[MAX_CCD_FILENAME_SIZE];


struct feature_failure_list_t
{
    uchar code, type;
};


/*
 ** howManyFeatureCodesInCcd()
 *
 *  DESCRIPTION: Use this function to determine the number of feature Codes in the CCD file.
 *
 *  RETURNS: The number of feature Codes in the CCD file.
 *
 */
int howManyFeatureCodesInCcd( void )
{
    int count=0;
	
	
    if( ccdFcodeInfo_p != NULL_PTR )
    {
        count = atoi( ccdFcodeInfo_p->numFCodes );
    }

    return count;
}

/*
 *  DESCRIPTION:
 *  Get feature code size
 *
 */
int getFeatureCodeSize(void)
{
	return (ccdVers ? CCD_V1_FCODE_SIZE: CCD_V0_FCODE_SIZE);
}

/*
 *  DESCRIPTION:
 *  Get feature code from CCD buffer
 *
 *  Input - feature code index
 */
char *getFeatureCodeFromCCD(int index)
{
    if( ccdFcodeInfo_p != NULL_PTR )
    {
    	return ccdFcodeInfo_p->featrCode + (index * getFeatureCodeSize());
    }
    else
    {
    	return NULL_PTR;
    }
}

/*
 *  DESCRIPTION:
 *  Get feature code section size
 *
 */
int getFeatureCodeSectionSize(void)
{
	return (howManyFeatureCodesInCcd() * getFeatureCodeSize());
}

/*
 ** isFeatureCodePresent()
 *
 *  PARAMETERS:
 *
 *      featurePcn - The feature Code to look for. 
 *
 *  DESCRIPTION:
 */
BOOL isFeatureCodePresent( char *featureCode_p )
{
    int i, numCodes = howManyFeatureCodesInCcd();
    BOOL result = FALSE;


    for (i = 0; i < numCodes; i++)
    {
        if (!strcmp( featureCode_p, getFeatureCodeFromCCD(i) ))
        {
            result = TRUE;
            break;
        }
    }

    return result;
}


/*
 ** getListOfFeatureCodes()
 *
 */
void getListOfFeatureCodes( char *listOfFeatureCodes_p )
{
    if( ccdFcodeInfo_p != NULL_PTR )
    {
        memcpy( listOfFeatureCodes_p, ccdFcodeInfo_p->featrCode, getFeatureCodeSectionSize() );
    }
}

/**********************************************************************************************
 ** howManyGfxFilesInCcd()
 *
 **********************************************************************************************/
int howManyGfxFilesInCcd( void )
{
    int count=0;


    if ( ccdGfxInfo_p != NULL_PTR )
    {
        count = atoi( ccdGfxInfo_p->numGfxFiles );
    }

    return count;
}

/**********************************************************************************************
 ** isGfxFilePresent()
 *
 **********************************************************************************************/
BOOL isGfxFilePresent( char *gfxFileName_p )
{
    int     i;
    int     numFiles;
    BOOL    result = FALSE;


    // This will return 0 if the ccdGfxInfo_p is NULL_PTR 
    numFiles = howManyGfxFilesInCcd();

    for (i = 0; i < numFiles; i++)
    {
        if (!strcmp( gfxFileName_p, ccdGfxInfo_p->gfxFileName[i] ))
        {
            result = TRUE;
            break;
        }
    }

    return result;
}


/*
 ** isGfxFilePresent()
 *
 */
int howManyRatePcnsInCcd( void )
{
    int count = 0;


    if( ccdRateInfo_p != NULL_PTR )
    {
        count = atoi( ccdRateInfo_p->numrPcnDesc );
    }

    return count;
}

/*
 ** getListOfRatePcns()
 *
 */
int getListOfRatePcns( char **listOfRatePcns_p )
{
    char    *info_p;
    int     i, x;
    int     numPnums = 0;
    int     numPcns = 0;


    if( listOfRatePcns_p != NULL_PTR )
    {
        // Returns 0 if ccdRateInfo_p is not setup.
        numPcns = howManyRatePcnsInCcd();
        if( numPcns > 0 )
        {
            info_p = (char *)&ccdRateInfo_p->pcnDesc[0];

            for( i = 0; i < numPcns; i++ )
            {
                listOfRatePcns_p[i] = info_p;

                // Move info_p to the first byte past this pcn descriptor:
                numPnums = atoi( ((CCD_PCN_DESC*)info_p)->numPNums );
                info_p += sizeof( CCD_PCN_DESC );
                for (x = 1; x < numPnums; x++)
                    info_p += CCD_RM_PNUM_SIZE;
            }
        }
    }
    
    return( numPcns );
}

/*
 ** howManyPartNumbersAssociatedWithRatePcn()
 *
 */
int howManyPartNumbersAssociatedWithRatePcn( char *pcn_p )
{
    int     i;
    short   numberOfPartNumbers = 0;
    char    **ccdRPcnList_p = NULL_PTR;  // ptr to a list of pointers.
    int     numPcns;


    // This will return 0 if ccdRateInfo_p is NULL_PTR 
    numPcns = howManyRatePcnsInCcd();

    // Malloc of 0 NG
    if( (pcn_p != NULL_PTR) && (numPcns > 0) )
    {
        // Allocate space for a list of pointers to Rate PCN data
        ccdRPcnList_p = (char **)malloc( sizeof(void *) * numPcns );
        if( ccdRPcnList_p == NULL_PTR )
        {
            throwException( "Invalid Pointer" );
            // throwException does not return.
        }
        
        getListOfRatePcns( ccdRPcnList_p );
        for( i = 0; i < numPcns; i++ )
        {   
            // Check for a match in the list. 
            if( !strcmp( pcn_p, ((CCD_PCN_DESC*)ccdRPcnList_p[i])->pcn ) )   //Match?
            {  
                numberOfPartNumbers = atoi( ((CCD_PCN_DESC*)ccdRPcnList_p[i])->numPNums ); 
                break;  
            }
        }

        if( NU_Deallocate_Memory( ccdRPcnList_p ) )
        {
            throwException( "Invalid Pointer" );
            // throwException does not return.
        }
    }

    return( numberOfPartNumbers );   
}

/*
 ** getCcdCustomerInfo()
 *
 */
CCD_CUSTOMER_INFO *getCcdCustomerInfo( void )
{
    return (ccdCustInfo_p);
}


/* *************************************************************************
// FUNCTION NAME:       isRatePartNumberInUse
// DESCRIPTION: 
//      Given a pointer to a rates module part number, this checks the rating
//      section of the CCD file (the copy in RAM) to see if the part number 
//      is in the list.
// INPUTS:
//      ratePartNumber_p - Pointer to Part Number string of the rate module to 
//                          check.
//      iNumPcns        - Number of PCN lists in the CCD file.
//      ppCcdRPcnList   - Pointer to list of PCN-lists in the RAM copy of the 
//                          ccd file.
// OUTPUTS: 
//       Return TRUE if the Rate Module Part Number is in the CCD list.
//      FALSE if not found in CCD.
// NOTES: 
//  1. .
//
// HISTORY:
//  2011.06.17 Clarisa Bellamy - Remove the memory leak part.  This function 
//                          used to malloc the space for the list and fill it,
//                          but only free the memory if the rate module was NOT
//                          found in the list. Now, the calling function malloc's
//                          the memory and frees it.  This allows the list to 
//                          be populated only once to check all of the modules
//                          in the map, instead of being malloc's and filled 
//                          over and over for each entry (active rate module or
//                          not) in the map.
// --------------------------------------------------------------------------*/
// Returns TRUE if it finds the rating part number in the CCD file. 
BOOL isRatePartNumberInUse( char *ratePartNumber_p, int iNumPcns, CCD_PCN_DESC **ppCcdRPcnList )
{
    int     totalPartNums;      
    int     i, x;
    BOOL    fFound = FALSE;


    if(   (iNumPcns > 0)
       && (ppCcdRPcnList != NULL_PTR) )
    {
        // For each pcn in the list...check the list of PartNums for that PCN.
        for( i = 0; (fFound == FALSE) && (i < iNumPcns); i++ )
        {    
            // Get the number of PartNums associated with that PCN.
            totalPartNums = atoi( ((CCD_PCN_DESC*)ppCcdRPcnList[i])->numPNums );

            // Check if the passed partnum is in the list.
            for( x = 0; x < totalPartNums; x++ )
            {
                if( !strcmp( ratePartNumber_p, ppCcdRPcnList[i]->rPNum[x] ) )
                {
                    // Found 
                    fFound = TRUE;
                    break;
                }
            }
        }
    }

    return( fFound );
}

//---------------------------------------------------------------------
//---------------------------------------------------------------------
//---------------------------------------------------------------------
//	ValidateFeatureCodesWithCCD
//---------------------------------------------------------------------
BOOL ValidateFeatureCodesWithCCD( void )
{
    BOOL    rv;
    int     numFeats;
    char    *featCodeList_p = NULL_PTR;
    char    accCode[CCD_LEGACY_FCODE_SIZE + 1];
    char    fCode[CCD_LEGACY_FCODE_SIZE + 1];
    uchar   i, x, numFeatures, type, code;
    ushort  featureList[MAX_FEATURES];
    FeatureTransType    fType;
    unsigned long num_fails = 0, num_fails2 = 0;
    BOOL bTryAgain = TRUE;

    (void)memset( featureList, 0, sizeof(ushort) * MAX_FEATURES);
    (void)memset( accCode, 0, sizeof(accCode) );

    // DISABLE ALL ENABLED FEATURES THAT ARE NOT IN CCD
    if ((numFeatures = fnGetEnabledFeatureList( featureList )) > 0)
    {
        for (i = 0; i < numFeatures; i++)
        {
            (void)HexToAscii( featureList[i], fCode );

            if (!isFeatureCodePresent( fCode ))
            {
                if ((featureList[i] < 0x97) || (featureList[i] > 0x9a))
                {
                    type = (unsigned char)(fCode[1] - 0x30);
                    code = (unsigned char)(((fCode[2] - 0x30) * 10) + (fCode[3] - 0x30));
                    rv = ModifyFeature( type, code, DISABLE_TRANSACTION, &fType, FALSE );
                }
                else
                {
                    (void)strcpy( accCode, fCode );
                }
            }
        }
    }

    numFeats = howManyFeatureCodesInCcd();
    if( numFeats <= 0 )
    {
		dbgTrace(DBG_LVL_ERROR,"Error: Invalid number of feature codes in CCD: %d", numFeats);
    	return FALSE;
    }

    featCodeList_p = (char *)malloc( (unsigned int) getFeatureCodeSectionSize() );
    if (!featCodeList_p)
    {
		dbgTrace(DBG_LVL_ERROR,"Error: Unable to allocate memory for feature codes (%d) in CCD", numFeats);
    	return FALSE;
    }

    // NOW PROCESS THE CCD FILE AND ENABLE NEW FEATURES
    getListOfFeatureCodes( featCodeList_p );
    /* Some features are dependent on other features.  Attempting to enable
       features in the wrong order can therefore cause failures.  Keep a
       list of the features that fail to enable correctly and then process
       that list again at the end. */
    struct feature_failure_list_t *fail_list = malloc(numFeats * sizeof(struct feature_failure_list_t));
    if (!fail_list)
    {
		dbgTrace(DBG_LVL_ERROR,"Error: Unable to allocate memory for failed feature codes in CCD");
    	return FALSE;
    }

	for (i = 0, x = 0; i < numFeats; i++, x += getFeatureCodeSize())
	{
		if (strlen(featCodeList_p + x) > CCD_LEGACY_FCODE_SIZE)
		{// if code is longer than 4 chars, then it is for tablet and not enabled here
			continue;
		}
	    type = (unsigned char)featCodeList_p[x+1] - 0x30;
	    code = (unsigned char)(((featCodeList_p[x+2] - 0x30) * 10) + (featCodeList_p[x+3] - 0x30));
	    rv = ModifyFeature( type, code, (ENABLE_TRANSACTION | FROM_GPS_STATION), &fType, FALSE );
	    if ((!rv) && fail_list)
	    {
	        /* Add the feature that didn't enable to the failures list.
	           We'll try to enable it again at the end. */
	        fail_list[num_fails].type = type;
	        fail_list[num_fails++].code = code;
	    }
	}

	if (num_fails == 0)
	    bTryAgain = FALSE;

	/* Now try again to enable any features that failed.  They may have
	   required another feature to be enabled first and that other feature
	   may have appeared later in the list and so may now be enabled.
	   Keep looping through the failures list until the number of items in
	   the list stops reducing. */
	while (bTryAgain)
	{
	    struct feature_failure_list_t *fail_list2 = malloc(num_fails * sizeof(struct feature_failure_list_t));


	    if( fail_list2 != NULL_PTR )
	    {
	    	// RD possible bug picked up by compiler,added de-reference to get size of structure
	        (void)memset(fail_list2, 0, sizeof(*fail_list2));

	        for (i = 0; i < num_fails; i++)
	        {
	            rv = ModifyFeature(fail_list[i].type, fail_list[i].code, (ENABLE_TRANSACTION | FROM_GPS_STATION), &fType, FALSE );
	            if ((!rv) && fail_list2)
	            {
	                /* Add the feature that didn't enable to the failures list.
	                We'll try to enable it again next time around. */
	                fail_list2[num_fails2].type = fail_list[i].type;
	                fail_list2[num_fails2++].code = fail_list[i].code;
	            }
	        }

	        (void)memcpy(fail_list, fail_list2, num_fails2 * sizeof(struct feature_failure_list_t));
	        free(fail_list2);

	        if (num_fails2 < num_fails && num_fails2 > 0)
	        {
	            /* We successfully enabled some more features on this pass but
	               there were still some failures too.  Go around again. */
	            num_fails = num_fails2;
	            num_fails2 = 0;
	            bTryAgain = TRUE;
	        }
	        else
	        {
	            /* Either all features are now enabled or we didn't
	               successfully enable any features on this pass.  Time to
	               stop. */
	            bTryAgain = FALSE;
	        }
		}// end allocated fail list2
	}// end while try again

    free(fail_list);
    free(featCodeList_p);

    if (accCode[0] != 0)
    {
        type = (unsigned char)accCode[1] - 0x30;
        code = (unsigned char)(((accCode[2] - 0x30) * 10) + (accCode[3] - 0x30));
        rv = ModifyFeature( type, code, DISABLE_TRANSACTION, &fType, FALSE );
    }

    fnCcdSetCcdDoneFlags( CCD_DONE_FLAGS_VLDT_FEATURES );

    return TRUE;
}


// ********************************************************************************
// FUNCTION NAME:           ValidateRatePcnsWithCCD()
// PURPOSE:
//      Goes through each entry in the CMOSRateCompsMap, and if the rate is an
//      active rate module, it checks to make sure that the partnumber is in
//      one of the Rating PCN lists in the ccd. If it is not, then the module
//      is removed from the Map.
//
// INPUTS:      None
// RETURNS:     None
// OUTPUTS:     May delete one or more rate module files and their remove their
//              entries from the CMOSRateCompsMap.
//---------------------------------------------------------------------
BOOL ValidateRatePcnsWithCCD( void )
{
    UINT8           idx;
    sCMOSRateComponents *pMapEntry;
    int             iNumPcns;
    CCD_PCN_DESC    **ppCcdRPcnList = NULL_PTR;
    BOOL            fRateInList = FALSE;
    BOOL    fbStillHaveRates = FALSE;


    // This will return 0 if ccdRateInfo_p is NULL_PTR
    iNumPcns = howManyRatePcnsInCcd();
    if( iNumPcns > 0 )
    {
        // Get heap memory for a list of pointers to each PCN list.
        ppCcdRPcnList = (CCD_PCN_DESC **)malloc( iNumPcns * sizeof( char** ) );
        if( ppCcdRPcnList == NULL_PTR )
        {
    		dbgTrace(DBG_LVL_ERROR,"Error: Unable to allocate memory for PCNs in CCD");
        	return FALSE;
        }

        // Initialize the list of PCN-list pointers
        getListOfRatePcns( (char **)ppCcdRPcnList );
    }

    for( idx = 0; idx < MAX_CMOS_RATE_COMPONENTS; idx++ )
    {
        // only check the rates components, but check both active and inactive rates so
        // if the CCD is trying to delete all the rates, the inactive rates will also be deleted.
        fRateInList = FALSE;
        pMapEntry = &CMOSRateCompsMap.CMOSRateComponents[idx];
        if(   (pMapEntry->SWPartNO[0] != 0)     // Only look at used entries
           && (pMapEntry->DeviceID == RATES_DATA_COMPONENT)
           /*&& (pMapEntry->bRateComponentActive)*/ )
        {
            // If there are no PCNs in the list, leave fRateInList = FALSE.
            if( iNumPcns > 0 )
            {
                // IF there are PCNs, see if this partnum is in one of the PCN lists.
                fRateInList = isRatePartNumberInUse( (char *)pMapEntry->SWPartNO,
                                                     iNumPcns, ppCcdRPcnList );
            }

            if( fRateInList == FALSE )
            {
                // It's not in the CCD, so delete the entry from the map.
                (void)memset( pMapEntry, 0, sizeof( sCMOSRateComponents ) );
            }
            else
            {
                fbStillHaveRates = TRUE;
            }
        }
    }

    if (fbStillHaveRates == FALSE)
    {
        // if we don't have any more rates files, clear the number of active rates.
        bNumPreviouslyActiveRates = 0;
    }

    // Free the memory allocated for the PCN list pointers.
    free(ppCcdRPcnList);

    fnCcdSetCcdDoneFlags( CCD_DONE_FLAGS_VLDT_RATES );
    return TRUE;
}

// ********************************************************************************
// FUNCTION NAME: ValidateGFXFilesWithCCD()
// PURPOSE:     Goes through each graphic in the .GAR file directory, and for
//              each active entry, it converts the DataNameVer to an ascii
//              string, then calls isGfxFilePresent() to see if that string
//              is in the CCD file.  If it is not, the graphic is "deleted".
//
// AUTHOR:
// INPUTS:      None
// RETURNS:     None
// OUTPUTS:     May delete one or more graphics in the .GAR file.
//
// NOTES:
//  A "deleted" graphic has had its entry in the graphics file cleared.  The
//  graphic may still be taking up space in the graphics file.  The only way
//  to recove that space, is to remove the entire .GAR file, or to do a
//  graphics file compaction.
//---------------------------------------------------------------------
BOOL ValidateGFXFilesWithCCD( void )
{
    char   gfxCcdFName[25]; // Ascii version of DataNameVer.
    UINT8 * pDataNameVer;   // Ptr to dataNameVer for current graphic.
    UINT16  wDirInx;        // Index into GF file directory of the current graphic to check.
    UINT8 * LinkPtr;        // LinkPtr of current graphic to check, if it is NULL_PTR, then there aren't any more.

    // Check the graphics in the graphics File to see if their DataNameVer is listed in the CCD file.
    // Check ALL the graphics in the graphics file.
    wDirInx = 0;
    LinkPtr = fnGFGetNextDirInxByCompType( &wDirInx, ANY_COMPONENT_TYPE, GRFX_ANY_MATCH );

    // Function above returns NULL_PTR when there aren't any more left.
    while( LinkPtr != NULL_PTR )
    {
        // Retrieve the DataNameVer from the Graphic, and conert it to ASCII.
        pDataNameVer = fnGFGetDataNamePtrByDirInx( wDirInx );
        (void)fnGrfxMakeDataNameVerIntoAscii( pDataNameVer, GRFXDATANAMEANDVERSION, gfxCcdFName );

         // This function is in CCDAPI.c, and it checks if the graphics file
         //  is Present IN THE CCD file.
        if( isGfxFilePresent( gfxCcdFName ) == FALSE )
        {
            // If not in the CCD file, then delete the current graphic ...
            fnGFDeleteByDirInx( wDirInx , TRUE);
        }

        // Get the directory index of the NEXT used directory entry.
        wDirInx++;
        LinkPtr = fnGFGetNextDirInxByCompType( &wDirInx, ANY_COMPONENT_TYPE, GRFX_ANY_MATCH );
    } // End the loop when all graphics in the graphics file have been checked.

    fnCcdSetCcdDoneFlags( CCD_DONE_FLAGS_VLDT_GRAPHICS );
    return TRUE;
}


//---------------------------------------------------------------------
//	DeleteCCDFile
//---------------------------------------------------------------------
// This deletes the CCD file and calls fnCcdClearCcdDoneFlagsAndPtrs to
//  remove the ptrs and free any space allocated in RAM for the CCD.
void DeleteCCDFile(char *pFileName )
{
	struct stat fileInfo;
	int fileStatus;

	fileStatus = stat( pFileName, &fileInfo );
    if (fileStatus == POSIX_ERROR)
    {
        fnCheckFileSysCorruption(fileStatus);
    	errno = ENOERR; //clear errno
    }
    else
    {
        fileStatus =  unlink(pFileName);

        if (fileStatus == POSIX_ERROR)
            fnCheckFileSysCorruption(fileStatus);

	    fnCcdClearCcdDoneFlagsAndPtrs();
    }
}



/* *************************************************************************
// FUNCTION NAME:       loadCCDFile
// DESCRIPTION: 
//      .   
// INPUTS:  
//      fileName_p - Pointer to the name of the CCD file
// OUTPUTS: 
//      TRUE if file exists
// NOTES: 
//  1. Some later CCD files have an oversign header, but not all of them, 
//     so the code determines the "start" of the file, without the header
//     by searching for the PCN string within the file.
//
// --------------------------------------------------------------------------*/
BOOL loadCCDFile( char *fileName_p )
{
    INT             fcb_p;
    unsigned int    dsize;
    char            *memPtr, *pcnSer_p;
    int             readCount;
    int             numfcodes, numgfiles, numrpcns;
    CHAR            strPath[MAX_CCD_FULL_PATH_SIZE];
    DSTAT   dstat ;
    STATUS  status;
    NU_MEMORY_POOL *pSysPool ;

    snprintf(strPath, MAX_CCD_FULL_PATH_SIZE, "%s%s", CCD_DEFAULT_DIRECTORY, fileName_p);

    // If an old CCD file buffer exists, free the memory, and clear the pointers.
    fnCcdClearCcdDoneFlagsAndPtrs();

    status = NU_Get_First(&dstat, strPath);
    if( status != NU_SUCCESS)
    {
        fnCheckFileSysCorruption(status);
        dbgTrace(DBG_LVL_ERROR, "Error: Unable to locate CCD file %s, status = %d", strPath, status);
        return( FALSE );
    }
    if (dstat.fsize == 0)
    {
        dbgTrace(DBG_LVL_ERROR, "Error: Empty CCD file %s", strPath);
        (void) NU_Done(&dstat);
        return( FALSE );
    }

    status = NU_System_Memory_Get(&pSysPool, NULL);
    if( status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_ERROR, "Error: Unable to access system memory for CCD file %s, status = %d", strPath, status);
        (void) NU_Done(&dstat);
        return( FALSE );
    }

    dsize = dstat.fsize + 1;// one more byte for null terminator
    status = NU_Allocate_Memory (pSysPool, (void **) &memPtr, dsize, NU_NO_SUSPEND);
    if(status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_ERROR, "Error: Unable to allocate %d bytes for CCD file %s, status = %d", dsize, strPath, status);
        (void) NU_Done(&dstat);
        return( FALSE );
    }
    memPtr[dsize - 1] = 0; // null terminate data buffer

    fcb_p = NU_Open(strPath, (PO_BINARY | PO_RDONLY), PS_IREAD) ;
    if (fcb_p < 0)
    {
        fnCheckFileSysCorruption(fcb_p);
        dbgTrace(DBG_LVL_ERROR, "Error: Unable to open CCD file %s, status = %d", strPath, fcb_p);
        (void) NU_Deallocate_Memory (memPtr);
        (void) NU_Done(&dstat);
        return( FALSE );
    }

    // Read the whole file in first
    readCount = NU_Read( fcb_p, memPtr, dstat.fsize );
    if(readCount < dstat.fsize)
    {
        fnCheckFileSysCorruption(readCount);
        dbgTrace(DBG_LVL_ERROR, "Error: Unable to read entire CCD file %s of %d bytes, status/readcount = %d", strPath, dstat.fsize, readCount);
        (void) NU_Deallocate_Memory (memPtr);
        status = NU_Close(fcb_p) ;
        fnCheckFileSysCorruption(status);
        (void) NU_Done(&dstat);
        return( FALSE );
    }

    status = NU_Close(fcb_p) ;
    if( status != NU_SUCCESS)
    {
        fnCheckFileSysCorruption(status);
        dbgTrace(DBG_LVL_ERROR, "Error: Unable to close CCD file %s, status = %d", strPath, status);
        dbgTrace(DBG_LVL_INFO, "Continuing anyway");
    }

    //  find the PCN within the file.  Everything before the PCN is "header".
    pcnSer_p = findString( CMOSSignature.bUicPcn, memPtr );
    if((pcnSer_p == NULL_PTR) || ((pcnSer_p - memPtr) >= dstat.fsize ))
    {    // If the PCN is not in read file, the file is invalid
        dbgTrace(DBG_LVL_ERROR, "Error: PCN not found in CCD file %s", strPath);
        (void) NU_Deallocate_Memory (memPtr);
        (void) NU_Done(&dstat);
        return( FALSE );
    }

    (void) NU_Done(&dstat); // we don't need dstat anymore

    // Set up all of the pointers to the appropriate places in RAM.
    ccdBuffer = memPtr;
    ccdCustInfo_p  = (CCD_CUSTOMER_INFO *) (memPtr + CCD_PREFIX_SIZE);

    //get minor version number and based on that determine size of field codes area
    ccdVers = atoi(ccdCustInfo_p->versNum + 3); //skip major version
    if (ccdVers > 1)
    {
        dbgTrace(DBG_LVL_ERROR, "Error: Invalid version in CCD file %s: %d", strPath, ccdVers);
        (void) NU_Deallocate_Memory (memPtr);
        return( FALSE );
    }

    ccdFcodeInfo_p = (CCD_FCODE_INFO *)((char *)ccdCustInfo_p + sizeof( CCD_CUSTOMER_INFO ));
    numfcodes = atoi( ccdFcodeInfo_p->numFCodes );

    ccdGfxInfo_p = (CCD_GFX_INFO *)((char *)ccdFcodeInfo_p + CCD_NUM_FCODES_SIZE + getFeatureCodeSectionSize());
    numgfiles = atoi( ccdGfxInfo_p->numGfxFiles );

    ccdRateInfo_p = (CCD_RATE_INFO *)((char *)ccdGfxInfo_p + CCD_NUM_GFILES_SIZE + (numgfiles * CCD_GFILE_NAME_SIZE));
    numrpcns = atoi( ccdRateInfo_p->numrPcnDesc );

    //  validate serial number
    if (strcmp( CMOSSignature.bUicSN, ccdCustInfo_p->serialNum ) != 0)
    {    // If the SN is not in read file, the file is invalid
        dbgTrace(DBG_LVL_ERROR, "Error: Serial Number %s not found in CCD file %s", CMOSSignature.bUicSN, strPath);
        fnCcdClearCcdDoneFlagsAndPtrs();
        return( FALSE );
    }

    dbgTrace(DBG_LVL_INFO, "CCD file %s loaded: Minor version: %d, #Feature Codes: %d, #Graphics Files: %d, #Rate PCNs %d",
            strPath, ccdVers, numfcodes, numgfiles, numrpcns);

    // Indicate that the pointers point to valid data.
    fnCcdSetCcdDoneFlags( CCD_DONE_FLAGS_PTRS_LOADED );

    return( TRUE );
}

/* *************************************************************************
// DESCRIPTION:
    // if install went well, delete old file if any
    // if install was no good, restore old file if any
// --------------------------------------------------------------------------*/
void ProcessOldCCDIfAny( char *fileName_p, BOOL installOK )
{
    struct stat fileInfo;
    int fileStatus;

    fileStatus = stat( TEMP_OLD_CCD_NAME, &fileInfo );
    if (fileStatus != POSIX_SUCCESS)
    {// No old file found so nothing to do
        fnCheckFileSysCorruption(fileStatus);
        errno = ENOERR; //clear errno
        return;
    }

    if (installOK)
    {
        fileStatus = unlink(TEMP_OLD_CCD_NAME);
        fnCheckFileSysCorruption(fileStatus);
    }
    else
    {//restore old file
        //rename file back
        fileStatus = rename(TEMP_OLD_CCD_NAME, fileName_p);
        fnCheckFileSysCorruption(fileStatus);
        OSWakeAfter(0); // relinquish control to OS so it can properly update file system
        //reload file which will set flags
        if (loadCCDFile( fileName_p ))
        {
            dbgTrace(DBG_LVL_INFO,"Old CCD file restored: %s", fileName_p);
        }
        else
        {
            dbgTrace(DBG_LVL_ERROR,"Failure restoring old CCD file: %s", fileName_p);
        }

    }
}

/* *************************************************************************
// DESCRIPTION:
//      Is there a new CCD file in the install directory?
//      Theoretically, the effective date should be checked also but the old
//      G900 code does not do this so it is left out for now.
// --------------------------------------------------------------------------*/
BOOL newCCDFileReadyToInstall( char *fileName_p )
{
    struct stat fileInfo;
    int fileStatus;
    char fullPath[MAX_CCD_FULL_PATH_SIZE];

    strcpy(fullPath, CCD_INSTALL_DIRECTORY);
    strcat(fullPath, fileName_p);

    fileStatus = stat( fullPath, &fileInfo );
    if (fileStatus != POSIX_SUCCESS)
    {
        fnCheckFileSysCorruption(fileStatus);
        errno = ENOERR; //clear errno
        return FALSE;
    }

    dbgTrace(DBG_LVL_INFO,"Found new CCD file to install: %s", fullPath);
    return TRUE;
}

/* *************************************************************************
// DESCRIPTION:
//      Rename old file if it exists.  Then copy new file from install directory
// to root directory.  If that succeeds, delete the old file if it exists and
// delete the file in the install directory.
// --------------------------------------------------------------------------*/
BOOL installNewCCDFile( char *fileName_p )
{
    BOOL    rv = TRUE;
    struct stat fileInfo;
    int fileStatus;
    char curPath[MAX_CCD_FULL_PATH_SIZE];
    char installPath[MAX_CCD_FULL_PATH_SIZE];
    BOOL curFileExists;

    strcpy(curPath, CCD_DEFAULT_DIRECTORY);
    strcat(curPath, fileName_p);

    fileStatus = stat( curPath, &fileInfo );
    if (fileStatus == POSIX_ERROR)
    {
        curFileExists = FALSE;
        fnCheckFileSysCorruption(fileStatus);
        errno = ENOERR; //clear errno
    }
    else
    {
        curFileExists = TRUE;
    }

    if (curFileExists)
    {// rename file so can restore if necessary
        fileStatus = rename(curPath, TEMP_OLD_CCD_NAME);
        if (fileStatus == POSIX_ERROR)
        {
            char aErr[80];
            memset(aErr, 0, sizeof(aErr));          
            strerror_r(errno, aErr, sizeof(aErr)-1);

            dbgTrace(DBG_LVL_ERROR,"Error: Failed to rename current CCD file %s so it can be replaced. Error: %s ", curPath, aErr);
            fnCheckFileSysCorruption(fileStatus);
            return FALSE;
        }
        OSWakeAfter(0); // relinquish control to OS so it can properly update file system
   }

    strcpy(installPath, CCD_INSTALL_DIRECTORY);
    strcat(installPath, fileName_p);

    // copy file
    fileStatus = fsCopyFile(installPath, curPath);
    if (fileStatus == POSIX_ERROR)
    {
        dbgTrace(DBG_LVL_ERROR,"Error: Failed to copy new CCD file %s to current CCD file %s. Error: %d", installPath, curPath, fileStatus);
        // restore current file
        fileStatus = rename(TEMP_OLD_CCD_NAME, curPath);
        OSWakeAfter(0); // relinquish control to OS so it can properly update file system
        fnCheckFileSysCorruption(fileStatus);
        return FALSE;
    }

    if (curFileExists)
    {// do not delete old file just yet
        dbgTrace(DBG_LVL_INFO,"Existing CCD file replaced with downloaded version");
    }
    else
    {
        dbgTrace(DBG_LVL_INFO,"New CCD file created from downloaded version");
    }
    // delete install file as well
    fileStatus = unlink(installPath);
    if (fileStatus == POSIX_ERROR)
        fnCheckFileSysCorruption(fileStatus);
        
    OSWakeAfter(0); // relinquish control to OS so it can properly update file system
    dbgTrace(DBG_LVL_INFO,"Downloaded CCD file deleted");

    return rv;
}

/* *************************************************************************
// FUNCTION NAME:       ValidateCCDFile
// DESCRIPTION: 
//		First determines CCD file name and stores it in static string
//      Calls loadCCDFile to find the CCD file, load the data into RAM, and
//      set the ccd pointers to the appropriate RAM locations.
//      If that succeeds, call the functions to process the CCD data, otherwise
//      set the flag to show that we looked for a CCD and didn't find it.
// INPUTS:
//      None.
// OUTPUTS:
//      BOOL - TRUE if ccd found, loaded and processed.
//             FALSE if not - indicate that there is no CCD (no matter what the problem was.) 
// NOTES: 
//
// HISTORY:
// --------------------------------------------------------------------------*/
BOOL ValidateCCDFile( void )
{
    BOOL    rv = FALSE;
    BOOL	newInstall = FALSE;
	char 	installFailStat[]= "FAILED";
	char 	installGoodStat[]= "SUCCESS";

    memset( ccdFileName, 0, sizeof( ccdFileName ) );
    strcat( ccdFileName, CMOSSignature.bUicPcn );
    strcat( ccdFileName, CMOSSignature.bUicSN );
    strcat( ccdFileName, CCD_DEFAULT_EXTENSION );

    if (newCCDFileReadyToInstall(ccdFileName))
    {
    	if (installNewCCDFile(ccdFileName))
    	{//install succeeded but loading of new file below will determine actual install status
        	newInstall = TRUE;
    	}
    	else
    	{//log that install failed; loading below will use old file
        	UpdateBaseInstallLog(ccdFileName, installFailStat);
    	}
    }
    if (loadCCDFile( ccdFileName ))
    {
        ValidateFeatureCodesWithCCD();
        ValidateRatePcnsWithCCD();
        ValidateGFXFilesWithCCD();
        if (newInstall)
        {
        	UpdateBaseInstallLog(ccdFileName, installGoodStat);
        }
        rv = TRUE;
    }
    else
    {
        // If there is no CCD, we set that flag.
        fnCcdSetCcdDoneFlags( CCD_DONE_FLAGS_NO_CCD );
        // if file exists but we got here, then delete file
        DeleteCCDFile(ccdFileName);
        if (newInstall)
        {
        	UpdateBaseInstallLog(ccdFileName, installFailStat);
        }
    }
    // if install went well, delete old file if any
    // if install was no good, restore old file if any
    ProcessOldCCDIfAny(ccdFileName, rv);

    return (rv);
}


/* *************************************************************************
// FUNCTION NAME:       rLogCcdFileInfo
// DESCRIPTION:
// RD - added output for debugDisplayOnly however to avoid buffer overrun
//      we stage the display output section.
* *************************************************************************/
 void rLogCcdFileInfo( char *fileName )
{
    //BOOL  rv = ValidateCCDFile();
    char    **ccdRPcnList_p = NULL_PTR;
    int  i, x, numPNums, charCount;
    char *myBuffer;
    int  numCodes;
    int  numGfx;
    int  numRPcns;
    char *debugLine;


    // This will return 0 if ccdFcodeInfo_p is NULL_PTR
    numCodes = howManyFeatureCodesInCcd();

    // This will return 0 if ccdGfxInfo_p is NULL_PTR
    numGfx = howManyGfxFilesInCcd();

    // This will return 0 if ccdRateInfo_p is NULL_PTR 
    numRPcns = howManyRatePcnsInCcd();

    //ML Malloc 0 NG
    if( numRPcns > 0 )
    {
        ccdRPcnList_p = (char **)malloc( sizeof( char** ) * numRPcns );
    }
    
    myBuffer = (char *)malloc(8192);

    if( !ccdRPcnList_p && (numRPcns > 0) )
        taskException("Out of memory!", THISFILE, __LINE__);
    
    if (!myBuffer)
        taskException("Out of memory!", THISFILE, __LINE__);
    
    if( ccdCustInfo_p == NULL_PTR )
    {
        if (myBuffer && NU_Deallocate_Memory( myBuffer ))
            throwException( "Invalid Pointer" );

        if (ccdRPcnList_p && NU_Deallocate_Memory( ccdRPcnList_p ))
            throwException( "Invalid Pointer" );

        return; //Guard
    }

    memset(myBuffer, '\0', 4096); 
    debugLine = myBuffer ;

    charCount =  sprintf( myBuffer,"********  CCD File Information Upload  ********\r\n\r\n" );
    charCount += sprintf( (myBuffer + charCount),"Customer information section -\r\n" );
    charCount += sprintf( (myBuffer + charCount),"Product PCN -> %s\r\n", ccdCustInfo_p->pcn );
    charCount += sprintf( (myBuffer + charCount),"Product Serial Number -> %s\r\n", ccdCustInfo_p->serialNum );
    charCount += sprintf( (myBuffer + charCount),"CCD Version Number -> %s\r\n", ccdCustInfo_p->versNum );
    charCount += sprintf( (myBuffer + charCount),"Effective Date -> %s\r\n", ccdCustInfo_p->effDate );
    charCount += sprintf( (myBuffer + charCount),"Country ID -> %s\r\n\r\n", ccdCustInfo_p->countryId );
    
    charCount += sprintf( (myBuffer + charCount), "Feature Code Section -\r\n" );
    charCount += sprintf( (myBuffer + charCount),"There are %d feature Codes:\r\n", numCodes );

    debugDisplayOnly(debugLine);
    debugLine = myBuffer + charCount;

    for (i = 0; i < numCodes; i++)
    {
        charCount += sprintf( (myBuffer + charCount), "\tFeature Code #%2d -> %s\r\n", (i + 1), getFeatureCodeFromCCD(i) );
    }

    charCount += sprintf( (myBuffer + charCount), "\r\nGraphics File Section -\r\n" );
    charCount += sprintf( (myBuffer + charCount),"There are %d Graphic Files:\r\n", numGfx );

    debugDisplayOnly(debugLine);
    debugLine = myBuffer + charCount;

    for (i = 0; i < numGfx; i++)
    {
        charCount += sprintf( (myBuffer + charCount), "\tGFX File #%2d -> %s\r\n", (i + 1), ccdGfxInfo_p->gfxFileName[i] );
        // protect against debug output running out of space.
        if((i % 20) == 0)
        {
            debugDisplayOnly(debugLine);
            debugLine = myBuffer + charCount;
        }
    }

    debugDisplayOnly(debugLine);
    debugLine = myBuffer + charCount;

    charCount += sprintf( (myBuffer + charCount), "\r\n" );
        
    charCount += sprintf( (myBuffer + charCount), "Rating PCN section -\r\n");
    charCount += sprintf( (myBuffer + charCount), "There are %d Rate PCNs:\r\n", numRPcns);
    
    if (ccdRPcnList_p)
    {
        // This fills in the list with a pointer to each Rate PCN record string 
        //  in the RAM copy of the CCD data.
        getListOfRatePcns( ccdRPcnList_p );

        for (i = 0; i < numRPcns; i++)
        {
            charCount += sprintf( (myBuffer + charCount), "\tRate PCN # %2d -> %s\r\n", (i + 1), ((CCD_PCN_DESC*)ccdRPcnList_p[i])->pcn );
            charCount += sprintf( (myBuffer + charCount), "\tRate PCN Type -> %s\r\n", ((CCD_PCN_DESC*)ccdRPcnList_p[i])->pcnType );

            //numPNums = howManyPartNumbersAssociatedWithRatePcn( ((CCD_PCN_DESC*)ccdRPcnList_p[i])->pcn );
            numPNums = atoi( ((CCD_PCN_DESC*)ccdRPcnList_p[i])->numPNums );
            charCount += sprintf( (myBuffer + charCount), "\tThere are %d Part Numbers:\r\n", numPNums );

            for (x = 0; x < numPNums; x++)
                charCount += sprintf( (myBuffer + charCount), "\t\tPart Number %2d -> %s\r\n", (x + 1), ((CCD_PCN_DESC*)ccdRPcnList_p[i])->rPNum[x] );
        }
    }    

    charCount += sprintf(myBuffer + charCount,"\r\n********  End CCD File Information Upload  ********\r\n");
    
    remoteLog( NULL_PTR, myBuffer );
    debugDisplayOnly(debugLine);

    if (NU_Deallocate_Memory( myBuffer ))
        throwException( "Invalid Pointer" );

    if (ccdRPcnList_p)
        if (NU_Deallocate_Memory( ccdRPcnList_p ))
            throwException( "Invalid Pointer" );
}

/* *************************************************************************
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// CCD Done Flags :
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//  This is a group of functions used to access the CCD Done bit flags.
//
// DESCRIPTION: 
//  The flags represent various CCD file processing that should be done on 
//   startup, or when a new CCD file is received.  This allows another function 
//   to find out if a process it requires has completed yet.
//
//  For example, it can cause a problem if new rates are downloaded that 
//   conflict with existing rates.  A CCD should be downloaded with the new
//   rates that enables them and disables the old ones.  But the rates can be
//   loaded to RAM and "sent to" the Rate Manager before the old rates files
//   are deleted via the CCD.
//  Now, when the rates PCNs in the CCD have been processed, a flag is set, 
//   and no rates are loaded into RAM unless that flag is set.  
//  The reason that there is not just one flag for the whole CCD is that, after
//   a new CCD is downloaded, all of it is processed before the graphics part,
//   and if there is a problem with the graphics part, the entire CCD is 
//   deleted.  With the separate flags, the rates can still be loaded even if
//   there was a problem with the graphics.
//
// FUNCTIONS:       
//   fnCcdSetCcdDoneFlags       - Sets one or more CCD done flags.  Input is a bitmap
//                              of the flags to be set.
//   fnCcdCheckCcdDoneFlags     - Checks one or more of the CCD done flags.  If all 
//                              checked flags are set, it returns TRUE, else FALSE.
//   fnCcdClearCcdDoneFlagsAndPtrs -  Uses ccdCustInfo_p to free memory that was 
//                              allocated for storing the CCD data, and then clears 
//                              that pointer.  Clears the pointers that pointed 
//                              within the allcoated space: ccdFcodeInfo_p, 
//                              ccdGfxInfo_p, and ccdCustInfo_p.  Then clears all 
//                              of the done flags, including the NO_CCD flag.
//
// FLAG DEFINITIONS:       
//  Located in ccdapi.h:
//   CCD_DONE_FLAGS_NO_CCD           0x0001   - Set by ValidateCCDFile, if the call to loadCCDFile
//                                              indicates that there is no CCD file.
//                                              and not found.
//   CCD_DONE_FLAGS_PTRS_LOADED      0x0002   - Set by loadCCDFile after the pointers ccdCustInfo_p
//                                              ccdCustInfo_p, ccdFcodeInfo_p, ccdGfxInfo_p and 
//                                              ccdRateInfo_p have been successfully setup to point
//                                              to their respective places in the RAM copy of the 
//                                              CCD data.
//   CCD_DONE_FLAGS_VLDT_FEATURES    0x0004   - Set by ValidateFeatureCodesWithCCD when it finishes.
//   CCD_DONE_FLAGS_VLDT_GRAPHICS    0x0008   - Set by ValidateGFXFilesWithCCD when it finishes.
//   CCD_DONE_FLAGS_VLDT_RATES       0x0010   - Set by ValidateRatePcnsWithCCD when it finishes.
//   CCD_DONE_FLAGS_VLDT_FLEX        0x0020   - G9 does not support this part of CCD file yet.
//
// FLAG CLEARING:
//  All flags are clear on powerup.  
//  fnCcdClearCcdDoneFlagsAndPtrs() is called:
//      1. From download.c:DeleteCCDFile, after the CCD file is deleted.
//      2. From download.c:sortUpdates, when it detects that a new CCD file 
//         has been downloaded.
//      3. At the begining of loadCCDFile, to start with a clean slate, and
//         make sure any previously allocated memory is freed before the ptr 
//         is re-used.
//
// NOTES: 
//  1. The CCD_DONE_FLAGS_PTRS_LOADED must be set in order for any of the functions to 
//     process the CCD and set the other flags (except for the NO_CCD flag.)
//
// HISTORY:
//  2011.05.24 Clarisa Bellamy - Initial. 
// --------------------------------------------------------------------------*/
// Set one or more flags.
void fnCcdSetCcdDoneFlags( T_CCD_DONE_FLAGS flags )
{
    bmCcdDoneFlags |= flags;
}

/* *************************************************************************
// FUNCTION NAME:       fnCcdCheckCcdDoneFlags
// DESCRIPTION:
// Check one or more flags - returns TRUE if ALL checked are set.
* *************************************************************************/
BOOL fnCcdCheckCcdDoneFlags( T_CCD_DONE_FLAGS flags )
{
    BOOL    retval = FALSE;

    if( (flags & bmCcdDoneFlags) == flags )
    {
        retval = TRUE;
    }
    return( retval );
}

/* *************************************************************************
// FUNCTION NAME:       fnCcdClearCcdDoneFlagsAndPtrs
// DESCRIPTION:
// Unallocate the space for the CCD data, clear the pointers and clear all the flags.
* *************************************************************************/
void fnCcdClearCcdDoneFlagsAndPtrs( void )
{
	// clear the flags
    bmCcdDoneFlags = 0;
	
    // free the allocated memory.
    (void) NU_Deallocate_Memory( ccdBuffer );
	
	// make sure all the pointers are clear
    ccdBuffer		= NULL_PTR;
	ccdCustInfo_p 	= NULL_PTR;
	ccdFcodeInfo_p 	= NULL_PTR;
	ccdGfxInfo_p 	= NULL_PTR;
	ccdRateInfo_p 	= NULL_PTR;
}

//-----------------------------------------------------------------------------
//  End of CCD Done flag functions
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
/* ************************************************************************* */

/**************************************************************************
 * on_GetCCDReq
 *
 * main websocket interface call to resturn a JSON representation of the CCD
 * file. Utilize the CMOS signature data to construct the required filename.
--------------------------------------------------------------------------*/
void on_GetCCDReq(UINT32 handle, cJSON *root)
{
	cJSON *rspMsg = NULL;

    rspMsg = ccdFileToJSON(ccdFileName) ;
	if(rspMsg == NULL)
	{
		CHAR strError[MAX_TEMPSTRING_SIZE];
		rspMsg = cJSON_CreateObject();
		snprintf(strError, MAX_TEMPSTRING_SIZE, "No Valid CCD File found");
		cJSON_AddStringToObject(rspMsg, "Error", strError);
	}
	else
	{
		debugDisplayOnly("CCD - JSON CCD message created successfuly\r\n") ;
	}

	cJSON_AddStringToObject(rspMsg, "MsgID", "GetCCDRsp");
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    addEntryToTxQueue(&entry, root, "  GetCCDReq: Added GetCCDRsp to tx queue.\r\n");
}

/**************************************************************************
 * ccdFileToJSON
 *
 * Look for the supplied filename in the ccd directory, if found parse it into
 * JSON.
--------------------------------------------------------------------------*/
cJSON *ccdFileToJSON( char *fileName_p )
{
//We need a file handle
    INT             fcb_p = -1;
    int             errorCode;
    unsigned int    fsize;
    char            scratchPad[128];
    CHAR            strPath[MAX_CCD_FULL_PATH_SIZE];
    CHAR            *memPtr;

    // create the path to look for our file
    snprintf(strPath, MAX_CCD_FULL_PATH_SIZE, "%s%s", CCD_DEFAULT_DIRECTORY, fileName_p);

    DSTAT   dstat ;
    STATUS  status;

    if(( status = NU_Get_First(&dstat, strPath)) == NU_SUCCESS)
    {
        debugDisplayOnly("Located CCD file %s - %u bytes long\r\n", dstat.lfname, dstat.fsize);
        fcb_p = NU_Open(strPath, (PO_BINARY | PO_RDONLY), PS_IREAD) ;
        if(fcb_p < 0) 
            fnCheckFileSysCorruption(fcb_p);
    }
    else
    {
        fcb_p = -1;
        debugDisplayOnly("ERROR Unable to locate any files in %s\r\n", fileName_p);
        fnCheckFileSysCorruption(status);
        return( FALSE );
    }

    // Get the memory pool
    NU_MEMORY_POOL *pSysPool ;
    status = NU_System_Memory_Get(&pSysPool, NULL);

    if(status == NU_SUCCESS)
    {
        fsize = dstat.fsize + 1;
        (void) NU_Done(&dstat); // we don't need dstat anymore

        if((status == NU_SUCCESS) && ( fsize > 0 ))
        {
            memPtr = NULL ;
            status = NU_Allocate_Memory (pSysPool, (VOID **)&memPtr, fsize, NU_NO_SUSPEND);
        }

        if((status != NU_SUCCESS) || ( memPtr == NULL_PTR ))
        {
            debugDisplayOnly("CCD - Unable to allocate %u bytes of memory for ccd file [%d]\r\n", fsize, status);
            if((errorCode = NU_Close( fcb_p )) != NU_SUCCESS)
            {
                debugDisplayOnly("CCD - Unable to close the open file %s, error: %d\r\n", strPath, errorCode);
                fnCheckFileSysCorruption(errorCode);
            }

            return( NULL );
        }

        debugDisplayOnly("CCD - Allocated %d  bytes of memory and opened file\r\n", fsize);
        INT readCount = NU_Read( fcb_p, memPtr, fsize );
        if(readCount < 0)
        {
            debugDisplayOnly("CCD - Error reading CCD file into memory\r\n");
            fnCheckFileSysCorruption(readCount);
        }

        if ((status= NU_Close(fcb_p)) != NU_SUCCESS)
            fnCheckFileSysCorruption(status);
        
        cJSON *pJSON =  buildJSONMsgFromCCDFile(memPtr, memPtr + readCount);
        if(memPtr)
        {
            NU_Deallocate_Memory(memPtr) ;
            memPtr = NULL ;
        }
        return pJSON ;
    }
    else
    { // no memory pool
        dbgTrace(DBG_LVL_INFO, "CCD - Unable to acquire memory pool");
        return NULL ;
    }
}

/**************************************************************************
 * buildJSONMsgFromCCDFile
 *
 *	Input
 *		pMem		- start of CCD file memory dump
 *		pEnd		- end of the supplied CCD memory dump.
 *
 * From the supplied memory dump of a valid CCD file pull the 4 sections:
 * 	Customer information
 * 	Customer Feature Codes
 * 	Customer Graphics Files
 * 	Customer Rating information
--------------------------------------------------------------------------*/
cJSON *buildJSONMsgFromCCDFile(CHAR *pMem, CHAR *pEnd)
{
	cJSON 	*pRes = NULL ;
	BOOL	bValid = (pMem != NULL) ;
	while(bValid)
	{
		if((bValid = (pRes = cJSON_CreateObject()) == NULL)) break ;

		pMem = handleCustomerInformationSection(pMem, pEnd, pRes, &bValid);
		if(!bValid)
		{
			dbgTrace(DBG_LVL_INFO, "CCDToJSON: Failed at CustomerInformation Stage");
			break ;
		}

		pMem = handleCustomerConfigurationFeatureCode(pMem, pEnd, pRes, &bValid);
		if(!bValid)
		{
			dbgTrace(DBG_LVL_INFO, "CCDToJSON: Failed at CustomerFeatureCode Stage");
			break ;
		}

		pMem = handleCustomerConfigurationGraphicFile(pMem, pEnd, pRes, &bValid);
		if(!bValid)
		{
			dbgTrace(DBG_LVL_INFO, "CCDToJSON: Failed at CustomerGraphicsFile Stage");
			break ;
		}

		pMem = handleCustomerConfigurationRatingData(pMem, pEnd, pRes, &bValid);
		if(!bValid)
		{
			dbgTrace(DBG_LVL_INFO, "CCDToJSON: Failed at CustomerRatingFile Stage");
			break ;
		}
		break;
	}

	if(!bValid && pRes)
	{
		cJSON_Delete(pRes) ;
		pRes = NULL;
	}
	return pRes;
}

/**************************************************************************
 * handleCustomerInformationSection
 *
 *	Input
 *		pMem		- start of CCD file memory dump
 *		pEnd		- end of the supplied CCD memory dump.
 *		pJson		- root message to add parsed information to.
 *		pValid		- pointer to a validity flag, we return TRUE if parsed OK else FALSE
 *
 -------------------------------------------------------------------------*/
CHAR *handleCustomerInformationSection(CHAR *pMem, CHAR *pEnd, cJSON *pJson, BOOL *pValid)
{
	cJSON *pCIJSON 	= NULL;
	*pValid 		= TRUE ;

	// create the sub message for the Customer information
	*pValid = ((pCIJSON = cJSON_CreateObject()) != NULL);

	while(*pValid && (pMem <= pEnd))
	{
		if(!(*pValid = getNextField(&pMem, pEnd, KEY_CI_PCN, 			pCIJSON))) break ;
		if(!(*pValid = getNextField(&pMem, pEnd, KEY_CI_SERIALNUMBER, 	pCIJSON))) break ;
		if(!(*pValid = getNextField(&pMem, pEnd, KEY_CI_VERSION, 		pCIJSON))) break ;
		if(!(*pValid = getNextField(&pMem, pEnd, KEY_CI_EFFECTIVEDATE,	pCIJSON))) break ;
		if(!(*pValid = getNextField(&pMem, pEnd, KEY_CI_COUNTRYID, 		pCIJSON))) break ;
		break ;
	}

	if(!*pValid)
	{
		pMem = NULL ;
		if(pCIJSON)
			cJSON_Delete(pCIJSON) ;
		pCIJSON = NULL;
	}
	else
	{
		cJSON_AddItemToObject(pJson, KEY_CI_SECTION, pCIJSON);
	}


	return pMem;
}


/**************************************************************************
 * handleCustomerInformationSection
 *
 *	Input
 *		pMem		- start of CCD file memory dump
 *		pEnd		- end of the supplied CCD memory dump.
 *		pJson		- root message to add parsed information to.
 *		pValid		- pointer to a validity flag, we return TRUE if parsed OK else FALSE
 *
 -------------------------------------------------------------------------*/
CHAR *handleCustomerConfigurationFeatureCode(CHAR *pMem, CHAR *pEnd, cJSON *pJson, BOOL *pValid)
{
	CHAR  strTemp[MAX_TEMPSTRING_SIZE];
	cJSON *pCIJSON = NULL;
	*pValid = TRUE ;

	// grab the number of feature codes
	pMem = skipNULLs(pMem, pEnd) ;
	pMem = getString(pMem, strTemp, MAX_TEMPSTRING_SIZE);

	*pValid = (strlen(strTemp) > 0);
	INT iCount = atoi(strTemp);

	if(*pValid && (iCount > 0))
	{

		*pValid = ((pCIJSON = cJSON_CreateArray()) != NULL);

		while(*pValid && (pMem <= pEnd) && (iCount-- > 0))
		{
			if(!(*pValid = getNextField(&pMem, pEnd, "", 	pCIJSON))) break ;
		}
	}

	if(!*pValid)
	{
		if(pCIJSON)
			cJSON_Delete(pCIJSON);
		pCIJSON = NULL ;
		pMem = pEnd;
	}
	else
	{
		cJSON_AddItemToObject(pJson, KEY_CC_SECTION, pCIJSON);
	}
	return pMem;
}

/**************************************************************************
 * handleCustomerConfigurationGraphicFile
 *
 *	Input
 *		pMem		- start of CCD file memory dump
 *		pEnd		- end of the supplied CCD memory dump.
 *		pJson		- root message to add parsed information to.
 *		pValid		- pointer to a validity flag, we return TRUE if parsed OK else FALSE
 *
 -------------------------------------------------------------------------*/
CHAR *handleCustomerConfigurationGraphicFile(CHAR *pMem, CHAR *pEnd, cJSON *pJson, BOOL *pValid)
{
	CHAR  strTemp[MAX_TEMPSTRING_SIZE];
	cJSON *pCIJSON = NULL;
	*pValid = TRUE ;

	// grab the number of feature codes
	pMem = skipNULLs(pMem, pEnd) ;
	pMem = getString(pMem, strTemp, MAX_TEMPSTRING_SIZE);

	*pValid 	= (strlen(strTemp) > 0);
	INT iCount 	= atoi(strTemp);

	if(*pValid && (iCount > 0))
	{

		*pValid = ((pCIJSON = cJSON_CreateArray()) != NULL);

		while(*pValid && (pMem <= pEnd) && (iCount-- > 0))
		{
			if(!(*pValid = getNextField(&pMem, pEnd, "", 	pCIJSON))) break ;
		}
	}

	if(!*pValid)
	{
		if(pCIJSON)
			cJSON_Delete(pCIJSON) ;
		pCIJSON = NULL;
		pMem = pEnd;
	}
	else
	{
		cJSON_AddItemToObject(pJson, KEY_GF_SECTION, pCIJSON);
	}
	return pMem;
}


/**************************************************************************
 * handleCustomerConfigurationRatingData
 *
 *	Input
 *		pMem		- start of CCD file memory dump
 *		pEnd		- end of the supplied CCD memory dump.
 *		pJson		- root message to add parsed information to.
 *		pValid		- pointer to a validity flag, we return TRUE if parsed OK else FALSE
 *
 -------------------------------------------------------------------------*/
CHAR *handleCustomerConfigurationRatingData(CHAR *pMem, CHAR *pEnd, cJSON *pJson, BOOL *pValid)
{
	CHAR  strTemp[MAX_TEMPSTRING_SIZE];
	CHAR  strRating[MAX_TEMPSTRING_SIZE];
	cJSON *pSectionJSON = NULL;
	cJSON *pCIJSON = NULL;
	cJSON *pJSONPN = NULL;
	*pValid = TRUE ;

	// grab the number of ratin entries
	pMem = skipNULLs(pMem, pEnd) ;
	pMem = getString(pMem, strTemp, MAX_TEMPSTRING_SIZE);

	*pValid = (strlen(strTemp) > 0);
	INT iCount = atoi(strTemp);
	INT iPass;

//	*pValid = ((pSectionJSON = cJSON_CreateObject()) != NULL);
	*pValid = ((pSectionJSON = cJSON_CreateArray()) != NULL);
	// for each of the rate entries...
	for(iPass=0 ; (iPass < iCount) && *pValid ; iPass++)
	{
		*pValid = ((pCIJSON = cJSON_CreateObject()) != NULL);
		if(pValid)
		{
			// grab the PCN and Type
			if(!(*pValid = getNextField(&pMem, pEnd, KEY_RS_PCN, pCIJSON))) break ;
			if(!(*pValid = getNextField(&pMem, pEnd, KEY_RS_PCNTYPE, pCIJSON))) break ;
			pMem = skipNULLs(pMem, pEnd) ;
			pMem = getString(pMem, strTemp, MAX_TEMPSTRING_SIZE);
			INT iCount2 = atoi(strTemp) ;
			if(iCount2 > 0)
			{
				if(!(*pValid = (pJSONPN = cJSON_CreateArray()) != NULL)) break ;

				for( ; iCount2 > 0 ; iCount2--)
				{
					if(!(*pValid = getNextField(&pMem, pEnd, "", pJSONPN))) break ;
				}

				cJSON_AddItemToObject(pCIJSON, KEY_RS_PARTNUMBERS, pJSONPN);
			}

//			snprintf(strRating, MAX_TEMPSTRING_SIZE, "PCN%03d", iPass) ;
//			cJSON_AddItemToObject(pSectionJSON, strRating, pCIJSON);
			cJSON_AddItemToArray(pSectionJSON, pCIJSON);
		}
	}

	if(!*pValid)
	{
		if(pSectionJSON)
			cJSON_Delete(pSectionJSON);
		if(pCIJSON)
			cJSON_Delete(pCIJSON);

		pSectionJSON 	= NULL ;
		pCIJSON 		= NULL ;
		pMem 			= NULL;
	}
	else
	{
		cJSON_AddItemToObject(pJson, KEY_RS_SECTION, pSectionJSON);
	}
	return pMem;
}





/**************************************************************************
 * getNextField
 *
 * Walk the supplied memory looking for an oasis of characters in a sea of NULLs
 * when found taks the string and add it to the supplied JSON message.
 * if the user supplied a key name then add as a string with that keyname, if the
 * keyname is blank then assume the supplied message is an array and just add it
 * as an extra element of that array.
 *
 *	Input
 *		pMem		- start of CCD file memory dump
 *		pEnd		- end of the supplied CCD memory dump.
 *		pMsg		- Output message to add discovered entry to.
 *		strFieldName- keyname to use for the discovered string else an empty string
 *
 -------------------------------------------------------------------------*/
BOOL getNextField(CHAR **pMem, CHAR *pEnd, CHAR *strFieldName, cJSON *pMsg)
{
	CHAR  strTemp[MAX_TEMPSTRING_SIZE];
	BOOL bValid = TRUE ;
	BOOL bArray = strFieldName[0] == 0;  // if no key name supplied then this must be an array

	// skip leading NULLS
	*pMem = skipNULLs(*pMem, pEnd) ;
	if(*pMem > pEnd)
	{
		bValid = FALSE ;
		dbgTrace(DBG_LVL_INFO, "Error moving to next field [%s], overrun end of file", strFieldName) ;
	}
	if(bValid)
	{
		*pMem = getString(*pMem, strTemp, MAX_TEMPSTRING_SIZE);
		if(*pMem > pEnd)
		{
			bValid = FALSE ;
			dbgTrace(DBG_LVL_INFO, "Error extracting field [%s], overrun end of file", strFieldName) ;
		}
	}
	if(bValid)
	{
		if(bArray)
			cJSON_AddItemToArray(pMsg, cJSON_CreateString(strTemp));
		else
			cJSON_AddStringToObject(pMsg, strFieldName, strTemp) ;
	}

	return bValid ;
}



#endif
