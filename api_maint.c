/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    api_maint.c

   DESCRIPTION:    API implementations for maintenance messages

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------

*************************************************************************/
#include "commontypes.h"
#include "cJSON.h"
#include "wsox_tx_queue.h"
#include "wsox_services.h"
#include "pbos.h"
#include "oitcm.h"
#include "cm.h"
#include "cmpublic.h"
#include "api_utils.h"
#include "api_funds.h"
#include "cwrapper.h"
#include "oit.h"
#include "sys.h"

#define  CRLF   "\r\n"
extern const char *api_status_to_string_tbl[];

// Store type of client when doing ink/PH replacement - this will be used to enforce that if PCT moves PH forward, the tablet cannot move it back
static WSOX_CLIENT repl_client = WSOX_CLIENT_NONE;

/*
 * Determines if printer is in an appropriate state for ink replacement.
 */
static BOOL validStateForReplacement(cJSON *failReason)
{

	BOOL passed = FALSE;

	if (fnIsCmFatalError())
	{
		cJSON_AddStringToObject(failReason, "Reason", (char *) api_status_to_string_tbl[API_STATUS_PRINTER_FATAL_ERROR]);
	    return passed;
	}

	if (fnIsCmPaperError())
	{
		cJSON_AddStringToObject(failReason, "Reason", (char *) api_status_to_string_tbl[API_STATUS_PRINTER_PAPER_ERROR]);
	    return passed;
	}

	if (fnIsCmJamLeverOpen())
	{
		cJSON_AddStringToObject(failReason, "Reason", (char *) api_status_to_string_tbl[API_STATUS_JAM_LEVER_OPEN]);
	    return passed;
	}

	if (fnIsSomeSubSystemRunning())
	{
		cJSON_AddStringToObject(failReason, "Reason", (char *) api_status_to_string_tbl[API_STATUS_TRANSPORT_RUNNING]);
	    return passed;
	}

    return TRUE;

}


/* ----------------------------------------------------------------------------------------[on_StartReplacementReq]-- */
/*
 * Starts ink replacement for the base.
 */

void on_StartReplacementReq(UINT32 handle, cJSON *root)
{
	BOOL msgStat;
	cJSON *failReason = cJSON_CreateObject();
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

	msgStat = validStateForReplacement(failReason);
    if (msgStat == FALSE)
    {
		cJSON_AddItemToObject(rspMsg, "Error", failReason);
    	return generateResponse(&entry, rspMsg, "StartReplacement", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
    }

	cJSON_Delete(failReason);// not used from here on

	// Remember the type of client requesting this
	repl_client = wsox_get_client_type(handle);

    msgStat = OSSendIntertask(CM, OIT, OC_RPL_START, NO_DATA, NULL, 0);

    if (msgStat == SUCCESSFUL)
    {
    	generateResponse(&entry, rspMsg, "StartReplacement", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
    }
    else
    {
    	generateResponse(&entry, rspMsg, "StartReplacement", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
    }
}

/* ----------------------------------------------------------------------------------------[on_CompleteReplacementReq]-- */
/*
 * Completes ink replacement for the base.
 */

void on_CompleteReplacementReq(UINT32 handle, cJSON *root)
{
	BOOL msgStat;

    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    WSOX_CLIENT my_client;

    // Reject if PCT called StartReplacementReq and tablet is calling this
    my_client = wsox_get_client_type(handle);
    if ((repl_client == WSOX_CLIENT_PCT) && (my_client == WSOX_CLIENT_TABLET))
    {
		dbgTrace(DBG_LVL_INFO, "CompleteReplacement not allowed: tablet trying to move back carriage moved forward by PCT");
    	return generateResponse(&entry, rspMsg, "CompleteReplacement", api_status_to_string_tbl[API_STATUS_NOT_ALLOWED], root);
    }
    // Clear the StartReplacementReq client now that an allowed client is doing the CompleteReplacement
    repl_client = WSOX_CLIENT_NONE;

    msgStat = OSSendIntertask(CM, OIT, OC_RPL_CMPLT, NO_DATA, NULL, 0);
    if (msgStat == SUCCESSFUL)
    {
    	generateResponse(&entry, rspMsg, "CompleteReplacement", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
    }
    else
    {
    	generateResponse(&entry, rspMsg, "CompleteReplacement", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
    }

}

/* -------------------------------------------------------------------------------------------------[sendMaintMessage]-- */
static BOOL sendMaintMessage(UINT8 maintType)
{
	BOOL msgStat;
    UINT8     pMsgData[2];    /* send 2 bytes */

    pMsgData[0] = maintType;
    pMsgData[1] = 0;

    msgStat = OSSendIntertask(CM, OIT, OC_MTNC_REQ, BYTE_DATA, pMsgData, 2);
    return msgStat;
}


static required_fields_tbl_t required_fields_tbl_PrinterMaintenanceReq =
    {
    "PrinterMaintenanceReq", { "MaintType"
                           , NULL }
    };

static char *valid_maintTypes [] =
				{ "PowerOnPurge", "JamPurge", "WipePurge", "InkTankReplacement", "PrintHeadReplacement" };


/* ----------------------------------------------------------------------------------------[on_PrinterMaintenanceReq]-- */
/*
 * Perform Printer Maintenance.  The only input is the type of maintenance.
 */
void on_PrinterMaintenanceReq(UINT32 handle, cJSON *root)
{

	BOOL msgStat = SUCCESSFUL;
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_PrinterMaintenanceReq))
    {
		int  typeIdx = 0;
		char *pTypeVal;

		pTypeVal = cJSON_GetObjectItem(root, "MaintType")->valuestring;
		for (typeIdx = 0; typeIdx < NELEMENTS(valid_maintTypes); typeIdx++)
		{
			if (0 == NCL_Stricmp(valid_maintTypes[typeIdx], pTypeVal))
			{
				break;
			}
		}

		if (typeIdx < NELEMENTS(valid_maintTypes))
		{//valid maint type
			switch(typeIdx)
			{
				case 0:
					msgStat = sendMaintMessage(PWR_PURGE);
					break;

				case 1:
					msgStat = sendMaintMessage(JAM_PURGE);
					break;

				case 2:
					msgStat = sendMaintMessage(WIPE_PURGE);
					break;

				case 3:
					msgStat = sendMaintMessage(RPL_INKTANK);
					break;

				case 4:
					msgStat = sendMaintMessage(RPL_PHEAD);
					break;


			}
		    if (msgStat == SUCCESSFUL)
		    {
		    	generateResponse(&entry, rspMsg, "PrinterMaintenance", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
		    }
		    else
		    {
		    	generateResponse(&entry, rspMsg, "PrinterMaintenance", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
		    }
		}// valid maint type
		else
		{
	    	generateResponse(&entry, rspMsg, "PrinterMaintenance", api_status_to_string_tbl[API_STATUS_INVALID_PARAMETERS], root);
		}
    }//RequiredFieldsArePresent
    else
    {
    	generateResponse(&entry, rspMsg, "PrinterMaintenance", api_status_to_string_tbl[API_STATUS_INVALID_PARAMETERS], root);
    }

}

/* ----------------------------------------------------------------------------------------[on_GetOutOfBoxPrintStateReq]-- */
/*
 * Gets Out Of Box Print state from the base.
 */

static const char      *valid_oob_ink_states [] = { "NoInkEver", "InkInstalled"};
static const char      *valid_oob_cover_states [] = { "Closed", "Open", "Unknown"};

void on_GetOutOfBoxPrintStateReq(UINT32 handle, cJSON *root)
{
	char evtStat;
    ulong lwCurrentEvent = 0;

    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    // For Cover state, first find out if CM/PM is initialized and sensor status known
    evtStat = OSReceiveEvents(OIT_EVENT_GROUP, SENSOR_STATUS_KNOWN_EVT, OS_NO_SUSPEND, OS_AND, &lwCurrentEvent);
    if ((evtStat == (char) SUCCESSFUL) || (evtStat == (char) OS_NO_EVENT_MATCH))
    {// We can get the status so return it
        cJSON_AddStringToObject(rspMsg, "InkState", valid_oob_ink_states[fnGetNVRAMOOBInkInstalled()]);
        if (evtStat == (char) SUCCESSFUL)
        { // CM/PM  initialized, so get cover state
            cJSON_AddStringToObject(rspMsg, "CoverState", valid_oob_cover_states[fnIsCmTopCoverOpen()]);
        }
        else
        { // CM/PM not initialized, so return unknown state
            cJSON_AddStringToObject(rspMsg, "CoverState", valid_oob_cover_states[2]);
        }
    	generateResponse(&entry, rspMsg, "GetOutOfBoxPrintState", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
    }
    else
    {// Cannot even read sensor status event, so we know nothing
    	generateResponse(&entry, rspMsg, "GetOutOfBoxPrintState", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
    }

}

/* ----------------------------------------------------------------------------------------[on_CompleteOutOfBoxPrintSetupReq]-- */
/*
 * Completes  Out Of Box Print setup on the base.
 */

void on_CompleteOutOfBoxPrintSetupReq(UINT32 handle, cJSON *root)
{
	BOOL msgStat;

    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    msgStat = OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_OOB_INK_COMPLETE, OS_OR);
    if (msgStat == SUCCESSFUL)
    {
    	generateResponse(&entry, rspMsg, "CompleteOutOfBoxPrintSetup", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
    }
    else
    {
    	generateResponse(&entry, rspMsg, "CompleteOutOfBoxPrintSetup", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
    }

}






