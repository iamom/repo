/************************************************************************

*   PROJECT:        CSD 2/3
*   COMPANY:        Pitney Bowes
*   MODULE NAME:    $Workfile:   flashutil.c  $
*   REVISION:       $$
*       
*   DESCRIPTION:	Flash data interface functions
*
* ----------------------------------------------------------------------
*               Copyright (c) 2015 Pitney Bowes Inc.
* ----------------------------------------------------------------------
*
*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "sys/stat.h"
#include "posix.h"
#include "dirent.h"
#include "commontypes.h"
#include "flashutil.h"
#include "pcdisk.h"
#include "pbos.h"
#include "pbio.h"
#include "gtypes.h"

#define FULLPATH_LEN 512
#define FSLASH_SEP '\\'
#define BSLASH_SEP '/'
#define MAX_FOLDER_CREATE_DEPTH 3
/* -------------------------------------------------------------------*/
/* PUBLIC VARIABLE DECLARATIONS										  */
/* -------------------------------------------------------------------*/

/* -------------------------------------------------------------------*/
/* STATIC VARIABLE DECLARATIONS										  */
/* -------------------------------------------------------------------*/

/* ************************************************************************
   / FUNCTION   : EndianAwareCopy
   / DESCRIPTION: Copy data & reverse endianness if necessary.
    * In the interest of performance this function does not support in
    * place conversion, that is having the input and output pointer
    * be the same.
   /
   / *********************************************************************** */
extern void EndianAwareCopy(void * outptr, const void * inptr, size_t size)
{
	unsigned char * outcptr = (unsigned char *) outptr;
	unsigned char * incptr = (unsigned char *) inptr;

	// here you can check if swapping is necessary
	// assume swapping is needed for now
	switch (size){
	case (sizeof(short)):
			*(outcptr + 1) = *incptr;
			*outcptr = *(incptr + 1);
			break;
	case (sizeof(long)):
			*(outcptr + 3) = *incptr;
			*(outcptr + 2) = *(incptr + 1);
			*(outcptr + 1) = *(incptr + 2);
			*outcptr = *(incptr +3);
			break;
	default:
		(void) memcpy(outptr, inptr, size);
	}
}

/* ************************************************************************
   / FUNCTION   : EndianAwareArray16Copy
   / DESCRIPTION: Copy data & reverse endianness if necessary.
    * Input data is assumed to be array of shorts.
    * Input size is the number of bytes in array of shorts.
    * In the interest of performance this function does not support in
    * place conversion, that is having the input and output pointer
    * be the same.
   /
   / *********************************************************************** */
extern void EndianAwareArray16Copy(void * outptr, const void * inptr, size_t size)
{
	if (size < 2) return;

	unsigned char * outcptr = (unsigned char *) outptr;
	unsigned char * incptr = (unsigned char *) inptr;

	// here you can check if swapping is necessary
	// assume swapping is needed for now

	size_t num_elements = size/2;
	size_t cur_element;

	for (cur_element = 0; cur_element < num_elements; cur_element++, incptr += 2, outcptr += 2)
	{
			*(outcptr + 1) = *incptr;
			*outcptr = *(incptr + 1);
	}
}

/* ************************************************************************
   / FUNCTION   : EndianAwareGet16
   / DESCRIPTION: Get short data pointer & return data with reverse endianness if necessary
   /
   / INPUTS     :
   /
   / *********************************************************************** */
extern unsigned short EndianAwareGet16(const void * inptr)
{
	unsigned char * incptr = (unsigned char *) inptr;
	unsigned short temp;

	// here you can check if swapping is necessary
	// assume swapping is needed for now
	temp = *incptr;
	temp <<= 8;
	temp += *(incptr + 1);
	return temp;
}

/* ************************************************************************
   / FUNCTION   : EndianAwareGet32
   / DESCRIPTION: Get long data pointer & return data with reverse endianness if necessary
   /
   / INPUTS     :
   /
   / *********************************************************************** */
extern unsigned long EndianAwareGet32(const void * inptr)
{
	unsigned char * incptr = (unsigned char *) inptr;
	unsigned long temp;

	// here you can check if swapping is necessary
	// assume swapping is needed for now
	temp = *incptr;
	temp <<= 8;
	temp += *(incptr + 1);
	temp <<= 8;
	temp += *(incptr + 2);
	temp <<= 8;
	temp += *(incptr + 3);
	return temp;
}

/* ************************************************************************
   / FUNCTION   : EndianSwap32
   / DESCRIPTION: Get long data & return with reverse endianness if necessary
   /
   / INPUTS     :
   /
   / *********************************************************************** */
extern unsigned long EndianSwap32(const unsigned long inval)
{
	void * incptr = (void *) &inval;

	return EndianAwareGet32(incptr);
}

/* ************************************************************************
   / FUNCTION   : EndianSwap16
   / DESCRIPTION: Get short data & return with reverse endianness if necessary
   /
   / INPUTS     :
   /
   / *********************************************************************** */
extern unsigned short EndianSwap16(const unsigned short inval)
{
	void * incptr = (void *) &inval;

	return EndianAwareGet16(incptr);
}

/* ************************************************************************
   / FUNCTION   : EndianSwapInPlace
   / DESCRIPTION: reverse endianness in place
    * The size can be any positive integer
   /
   / *********************************************************************** */
extern void EndianSwapInPlace(void * dataptr, size_t size)
{
	unsigned int i, numSwaps;
	unsigned char frontByte, backByte;
	unsigned char * frontptr;
	unsigned char * backptr;

	if (size < 2)
		return; // nothing to do

	numSwaps = size/2; // if size is odd, middle byte is left alone
	frontptr = (unsigned char *) dataptr;
	backptr = (unsigned char *) dataptr + size - 1;

	for (i = 0; i < numSwaps; i++)
	{
		frontByte = *frontptr;
		backByte = *backptr;
		*frontptr++ = backByte;
		*backptr-- = frontByte;
	}
}

static char fs_root[4]; // populated by OS at startup

/* -------------------[file system functions]----------------------------------------------------------------------- */

//---------------------------------------------------------------------
//	fnFilePath
//---------------------------------------------------------------------
VOID fnFilePath(CHAR* outPath, CHAR* path, CHAR* fileName)
{
	if((path != NULL) && (fileName != NULL) && (strlen(path) > 0) && (strlen(fileName) > 0))
		sprintf(outPath, "%s%s\\%s", fs_root, path, fileName);
	else if((path != NULL) && ((fileName == NULL) || (strlen(fileName) == 0)))
		sprintf(outPath, "%s%s", fs_root, path);
	else if((fileName != NULL) && (strlen(fileName) > 0))
		sprintf(outPath, "%s%s", fs_root, fileName);
	else //path == 0
		sprintf(outPath, "%s", fs_root);
}

//---------------------------------------------------------------------
//	fnFileExists
//---------------------------------------------------------------------
BOOL fnFileExists(CHAR *fileName)
{
	BOOL 	result = FALSE;
	STATUS 	status = 0;
	INT8	attr;

	status = NU_Get_Attributes((UINT8 *)&attr, fileName);
	if (status != NUF_NOFILE)
	{
		result = TRUE;
	}

	fnCheckFileSysCorruption(status);
	return result;
}

//---------------------------------------------------------------------
//	fnFolderExists
//---------------------------------------------------------------------
BOOL fnFolderExists(CHAR *folderName)
{
	BOOL 	result = FALSE;
	STATUS 	status = 0;
	INT8	attr;

	if(strcmp(folderName, fs_root) == 0)
	{
		result = TRUE;
		return result;
	}
	status = NU_Get_Attributes((UINT8 *)&attr, folderName);
	if (status >= 0)
	{
		result = TRUE;
	}
	else
	{
		fnCheckFileSysCorruption(status);
	}

	return result;
}

//---------------------------------------------------------------------
//	fnFileDelete
//---------------------------------------------------------------------
STATUS fnFileDelete(CHAR *path, CHAR *fileName)
{
	STATUS 	status = NU_SUCCESS;
	CHAR	fullpath[FULLPATH_LEN] = {0};

	do
	{
		fnFilePath(fullpath, path, fileName);

		if(fnFileExists(fullpath))
		{
			status = NU_Delete(fullpath);
		}
	}while(false);

	fnCheckFileSysCorruption(status);

	return status;
}

//---------------------------------------------------------------------
//	fnFileCreate
//---------------------------------------------------------------------
STATUS fnFileCreate(CHAR *path, CHAR *fileName, CHAR *contents, int filesize)
{
	STATUS 	status = NU_RETURN_ERROR;
	int		bytes_written = 0;
	int 	fd = 0;
	CHAR	fullpath[FULLPATH_LEN] = {0};

	do
	{
		fnFilePath(fullpath, path, NULL);

		if(!fnFolderExists(fullpath))
		{
			status = NU_Make_Dir(fullpath);
			if(status != 0)
				break;
		}

		fnFilePath(fullpath, path, fileName);

		fd = NU_Open(fullpath, (PO_TEXT|PO_RDWR|PO_CREAT|PO_TRUNC), (PS_IWRITE | PS_IREAD));
		if (fd < 0)
		{
			//return NUF_* status
			status = fd;
			break;
		}
		else
		{
			bytes_written = NU_Write(fd, contents, filesize);
			if (bytes_written < filesize)//anything less than desired amount is a failure
			{//return NUF_* status or -1 if only some bytes written
				status = (bytes_written < 0 ? bytes_written : NU_RETURN_ERROR);
				(void) NU_Close(fd);
				break;
			}
		}

		status = NU_Close(fd);

	}while(false);

	fnCheckFileSysCorruption(status);

	return status;
}

//---------------------------------------------------------------------
//	fnFileAppend
//---------------------------------------------------------------------
STATUS fnFileAppend(CHAR *path, CHAR *fileName, CHAR *contents, int filesize, bool truncate)
{
	STATUS 	status = 0;
	int		bytes_written = 0;
	int 	fd = 0;
	CHAR	fullpath[FULLPATH_LEN] = {0};

	do
	{
		fnFilePath(fullpath, path, NULL);

		if(!fnFolderExists(fullpath))
		{
			status = NU_Make_Dir(fullpath);
			if(status != 0)
				break;
		}

		fnFilePath(fullpath, path, fileName);

		if(!fnFileExists(fullpath) || truncate)
		{
			fd = NU_Open(fullpath, (PO_TEXT|PO_RDWR|PO_CREAT|PO_TRUNC), (PS_IWRITE | PS_IREAD));
		}
		else
		{
			fd = NU_Open(fullpath,(PO_TEXT|PO_WRONLY|PO_APPEND), PS_IWRITE);
		}
		if(fd < 0)
		{
			status = fd;
			break;
		}
		else
		{
			bytes_written = NU_Write(fd, contents, filesize);
			if (bytes_written < 0)
			{
				fnCheckFileSysCorruption(bytes_written);
				//printf("Failed to write to %s file!\r\n\n", fileName);
				status = 2;
				break;
			}
		}

		status = NU_Close(fd);
		if(status != NU_SUCCESS)
		{
			//printf("Failed to close %s!\r\n\n", fileName);
			fnCheckFileSysCorruption(status);
			status |= 4;
			break;
		}
	}while(false);

	if (status < 0)
		fnCheckFileSysCorruption(status);

	return status;
}

//---------------------------------------------------------------------
//	fnFileRead
//---------------------------------------------------------------------
STATUS fnFileRead(CHAR *path, CHAR *fileName, CHAR *contents, int index, int length)
{
	STATUS 	status = -1;
	int 	bytes_read = 0;
	int 	filesize = 0;
	int 	fd = 0;
	int		READ_BUFFER_SIZE = 1028;
	CHAR 	fullpath[FULLPATH_LEN] = {0};

	do
	{
		fnFilePath(fullpath, path, fileName);

		filesize = fsGetFileSize(fullpath);
		if(filesize < 1)
			break;
		if(index > filesize)
			break;
		if(index + length > filesize)
			length = filesize - index;
		if(length == 0)
			length = filesize;

		fd = NU_Open(fullpath,(PO_TEXT|PO_RDONLY), PS_IREAD);
		if (fd < 0)
		{
			status = fd;
			break;
		}
		else
		{
			if(index != 0)
				fnCheckFileSysCorruption(NU_Seek(fd, index, PSEEK_SET));

			//contents = (char *)malloc(filesize);
			//(VOID)memset (contents, 0, filesize);
			filesize = 0;
		    do
			{
				/* Read the information from the drive */
		    	if(length < READ_BUFFER_SIZE)
		    		bytes_read = NU_Read(fd, (contents + filesize), length);
		    	else
		    		bytes_read = NU_Read(fd, (contents + filesize), READ_BUFFER_SIZE);

				if (bytes_read < 0)
				{
					//printf("Failed to read from %s file!\r\n\n", fileName);
					fnCheckFileSysCorruption(bytes_read);
					break;
				}
				else
				{
					/* Print the information */
					filesize += bytes_read;
				}

			/* Read until we reach the requested length */
			} while ((bytes_read > 0) && (length > filesize));

	        /* Close the log file */
	        status = NU_Close(fd);

	        if (status != NU_SUCCESS)
	        {
	            printf("Failed to re-close %s file!\r\n\n", fileName);
	        }
		}
	}while(false);

	fnCheckFileSysCorruption(status);
	return status;
}

//---------------------------------------------------------------------
//	fnFilesInFolder
//---------------------------------------------------------------------
STATUS fnFilesInFolder(CHAR *folderName, CHAR *fileCSVList)
{
	STATUS status = 0;

	do
	{
		//TODO - Senior Get Dir Files and create a CSV string
	}while(false);

	return status;
}

//---------------------------------------------------------------------
//	fnFileSysCorruption
//---------------------------------------------------------------------
BOOL fnFileSysCorruption(STATUS errcode)
{
    BOOL    result = FALSE;

    if (errcode == NUF_INTERNAL 
        || (errcode == POSIX_ERROR && errno == EIO))
    {
        dbgTrace(DBG_LVL_INFO,"fnFileSysCorruption, errcode=%d, errno=%d", errcode, errno);
        result = TRUE;
    }

    return result;
}

static STATUS GetStorageDrive(char *drive)
{
	STATUS      status = -1;            /* Status */
	INT         i;                      /* General purpose variable */
	MNT_LIST_S  *mount_list = NU_NULL;  /* Pointer to list of mounted drives */

	/* Check every 2 seconds for up to 30 seconds. */
	for(i = 0; ((i < 15) && (status != NU_SUCCESS)); i++)
	{
 //		dbgTrace(DBG_LVL_INFO, "Searching for a device!\r\n\n");
		status = NU_Storage_Device_Wait(NU_NULL, (NU_PLUS_TICKS_PER_SEC*2));
	}

	 /* If we found a device */
	if (status == NU_SUCCESS)
	{
		/******************************************
		 * Get a list of currently mounted drives *
		 ******************************************/

		status = NU_List_Mount(&mount_list);

		if (status != NU_SUCCESS)
		{
			dbgTrace(DBG_LVL_INFO, "No drives mounted!\r\n\n");
		}
		else
		{
			/*****************************************************
			 * Create the root directory string based on the     *
			 * information for the first drive in the mount list *
			 *****************************************************/

			/* Append ":\\" to drive letter to create the root directory string. */
			drive[0] = mount_list->mnt_name[0];
			drive[1] = ':';
			drive[2] = '\\';
			drive[3] = '\0';

			/* Free the mount list memory */
			NU_Free_List((VOID **)&mount_list);
		}
	}

	return(status);

}

const char *fsGetRoot()
{
	return (const char *) fs_root;
}

void fsGetFolderPath(char *fileNamePath, char* folderName)
{
	strcpy(fileNamePath, fs_root);
	strcat(fileNamePath, folderName);
}

/* *************************************************************************
// DESCRIPTION:
//      1. Try to create the path directory
//		2. Limit path creation to MAX_FOLDER_CREATE_DEPTH levels
//		3. Input path must be absolute path
//
// --------------------------------------------------------------------------*/
STATUS fsMakeMultiLevelPath(char * dirpath)
{
	char filepath[FULLPATH_LEN];
	UINT8 depth = 0;
	char curChar;
	size_t rootLen;
	char *ptrPath;
	BOOL done = FALSE;
	STATUS status = NU_SUCCESS;

	// confirm input path buffer big enough
	if (strlen(dirpath) >= FULLPATH_LEN)
	{
		return NUF_LONGPATH;
	}
	// confirm path starts with root
	rootLen = strlen(fsGetRoot());
	if (strncmp(dirpath, fsGetRoot(), rootLen) != 0)
	{
		return NUF_NOFILE;
	}

	ptrPath = dirpath + rootLen; //skip root
	memset(filepath, 0, FULLPATH_LEN); // clear buffer so all strings written into it are null terminated
	// Iterate through each path segment, creating as necessary
	do
	{
		curChar = *ptrPath;
		if ((curChar == FSLASH_SEP) || (curChar == BSLASH_SEP) || (curChar == NULL))
		{// found a segment, try to create it
			if (++depth > MAX_FOLDER_CREATE_DEPTH)
			{
				status = NUF_BADPARM;
				break;
			}
			strncpy(filepath, dirpath, (ptrPath - dirpath)); // Leave out separator
			status = NU_Make_Dir(filepath);
			if (status == NU_SUCCESS)
			{
				dbgTrace(DBG_LVL_INFO,"Multi level create directory %s.", filepath);
			}
			else if (status != NUF_EXIST)
			{// error creating folder so get out; if folder exists, keep going
				fnCheckFileSysCorruption(status);
				break;
			}

		}

		if (curChar == NULL)
		{
			done = TRUE;
		}
		else
		{
			ptrPath++; // go to next char
		}
	}
	while (!done);

	return status;

}


/* *************************************************************************
// DESCRIPTION:
//      1. Try to create the input directory
//		2. If it succeeds or if it fails because directory already exists, return
//		3. If it fails because path to directory does not exist, try to create the path, then the directory
//
// --------------------------------------------------------------------------*/
STATUS fsMakeDir(char * dirpath)
{
	STATUS status = 0;

	status = NU_Make_Dir(dirpath);
	if (status == NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_INFO,"Single level create directory %s.", dirpath);
		return status;
	}
	else
	{
		fnCheckFileSysCorruption(status);
	}

	if (status == NUF_NOFILE)
	{
		return fsMakeMultiLevelPath(dirpath);
	}
	return status;

}

STATUS InitFlashFileSystem(void)
{
	STATUS status = NU_SUCCESS;

	// clear legacy errno
	fsu_set_user_error(0);

	status =  GetStorageDrive(fs_root);
	if(status == NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_INFO, "File System Initialized. Root: %s", fs_root);
	}
	return status;
}

INT fsWriteFile(char *path, char * filename, uint8_t* filedata, int32_t fileLength)
{
	INT status = -1;
	INT fd;

	INT length_rdwr;
	char filepath[FULLPATH_LEN];

	strcpy(filepath, fs_root);

	if(path != NULL)
	{
		strcat(filepath, path);

		status = fsMakeDir(filepath);
		if ( (status == NU_SUCCESS ) || (status == NUF_EXIST))
		{
			status = NU_SUCCESS;
		}
		else
		{
			dbgTrace(DBG_LVL_ERROR, "Failed to create folder %s, reason: %d \r\n", path, status);
			return status;
		}
	}

	strcat(filepath, "\\");
	strcat(filepath, filename);
	fd = NU_Open(filepath, (PO_TEXT | PO_RDWR | PO_CREAT | PO_TRUNC), PS_IWRITE);
	if(fd < 0)
	{
		dbgTrace(DBG_LVL_ERROR, "Failed to open file %s \r\n", filepath);
		status = fd;
	}
	else
	{
		length_rdwr = NU_Write(fd, (CHAR*)filedata, fileLength);
		//length_rdwr = NU_Write(fd, G900EMD_UNC, G900EMD_UNC_len);
		if(length_rdwr != fileLength)
		{
			dbgTrace(DBG_LVL_ERROR, "Failed to write file %s length %lx out of %lx\r\n", filepath, length_rdwr, fileLength);
			status = length_rdwr;
			(void) NU_Close(fd);
		}
		else
		{
			status = NU_Close(fd);
			if (status == NU_SUCCESS)
				status = length_rdwr;
		}


	}

	if(status < 0)
		fnCheckFileSysCorruption(status);

	return status;
}


INT fsAppend(char * filename, CHAR* filedata, int32_t fileLength)
{
	INT status = 0;
	INT fd;

	INT length_rdwr;

	if(!fnFileExists(filename))
	{
		fd = NU_Open(filename, (PO_TEXT|PO_RDWR|PO_CREAT|PO_TRUNC), (PS_IWRITE | PS_IREAD));
	}
	else
	{
		fd = NU_Open(filename,(PO_TEXT|PO_WRONLY|PO_APPEND), PS_IWRITE);
	}


	if(fd < 0)
	{
		dbgTrace(DBG_LVL_ERROR, "Failed to open file %s \r\n", filename);
		fnCheckFileSysCorruption(fd);
		status = 1;
	}
	else
	{
		length_rdwr = NU_Write(fd, filedata, fileLength);
		if(length_rdwr != fileLength)
		{
			dbgTrace(DBG_LVL_ERROR, "Failed to write file %s length %lx out of %lx\r\n", filename, length_rdwr, fileLength);
			fnCheckFileSysCorruption(length_rdwr);
			status = length_rdwr;
		}

		fnCheckFileSysCorruption(NU_Close(fd));

	}
	return status;
}

//INT write_file2(char * filename, CHAR* filedata, int32_t fileLength)
//{
//	INT status = 0;
//	INT fd;
//	int i;
//
//	INT length_rdwr= 0;
//
//	strcat(filpath, fs_root);
//	strcat(filpath, filename);
//
//	for( i = fileLength; i > 0; i -= 1024 )
//	{
//		if(i > 1024)
//			status = fsAppend(filename, filedata, 1024);
//		else
//			status = fsAppend(filename, filedata, i);
//
//	}
//
//
//	return status;
//}

/* *************************************************************************
// DESCRIPTION:
// Copy source to destination
// - source and destination can be in different directories
// - destination should not exist
// - file should not be very big because it is read in one shot into heap memory
// --------------------------------------------------------------------------*/
INT fsCopyFile(char *source, char *destination)
{
    INT status = POSIX_ERROR;
    struct stat fileInfo;
    int fileStatus;
    size_t readlen, fileSize;
    FILE *sourceFileHandle = NULL;
    FILE *destFileHandle = NULL;
    unsigned char *pBuffer;
    BOOL osstat;
    char aErr[80] = {0};

    // check if source exists
    fileStatus = stat( source, &fileInfo );
    if (fileStatus == POSIX_ERROR)
    {
        fnCheckFileSysCorruption(fileStatus);
        strerror_r(errno, aErr, sizeof(aErr)-1);
        dbgTrace(DBG_LVL_ERROR,"CopyFile Error: Source file %s does not exist. Error: %s ", source, aErr);
        return status;
    }
    fileSize = fileInfo.st_size;

    // open source and read it all into buffer
    sourceFileHandle = fopen(source,"rb");
    if (sourceFileHandle == NULL)
    {
        fnCheckFileSysCorruption(POSIX_ERROR);
        strerror_r(errno, aErr, sizeof(aErr)-1);
        /* Couldn't open the file. */
        dbgTrace(DBG_LVL_ERROR,"CopyFile Error: Could not open source file %s. Error: %s ", source, aErr);
        return status;
    }

    // allocate memory for file
    osstat = OSGetMemory ((void **) &pBuffer, fileSize );
    if (osstat == FAILURE)
    {
        fnCheckFileSysCorruption( fclose(sourceFileHandle) );
        dbgTrace(DBG_LVL_ERROR,"CopyFile Error: Could not allocate enough memory for source file %s. Size: %d ", source, fileSize);
        return status;
    }
    memset((void*)pBuffer, 0, fileSize);

    // read char data from file
    readlen = fread((void*)pBuffer, 1, fileSize, sourceFileHandle);
    if (readlen < fileSize)
    {
        fnCheckFileSysCorruption(POSIX_ERROR);
        (void) OSReleaseMemory(pBuffer);
        fnCheckFileSysCorruption(fclose(sourceFileHandle));
        dbgTrace(DBG_LVL_ERROR,"CopyFile Error: Could not read all the data from source file %s. expected: %d, read: %d ", source, fileSize, readlen);
        return status;
    }

    // close input file
    if ((fileStatus = fclose(sourceFileHandle)) == POSIX_ERROR)
        fnCheckFileSysCorruption(fileStatus);

    // open destination file
    destFileHandle = fopen(destination,"wb");
    if (destFileHandle == NULL)
    {
        fnCheckFileSysCorruption(POSIX_ERROR);
        /* Couldn't open the file. */
        (void) OSReleaseMemory(pBuffer);
        strerror_r(errno, aErr, sizeof(aErr)-1);
        dbgTrace(DBG_LVL_ERROR,"CopyFile Error: Could not create destination file %s. Error: %s ", destination, aErr);
        return status;
    }

    if (fwrite((void*)pBuffer, 1, readlen, destFileHandle) != readlen)
    {
        fnCheckFileSysCorruption(POSIX_ERROR);
        dbgTrace(DBG_LVL_ERROR,"CopyFile Error: Could not write all data to destination file %s. expected: %d", destination, readlen);
        (void) OSReleaseMemory(pBuffer);
        fnCheckFileSysCorruption(fclose(destFileHandle));
        fnCheckFileSysCorruption(unlink(destination)); //clean up bad file
        return status;
    }

    (void) OSReleaseMemory(pBuffer);
    fileStatus = fclose(destFileHandle);
    if (fileStatus == POSIX_ERROR)
    {
        fnCheckFileSysCorruption(fileStatus);
        strerror_r(errno, aErr, sizeof(aErr)-1);
        dbgTrace(DBG_LVL_ERROR,"CopyFile Error: Could not close destination file %s. Error: %s ", destination, aErr);
        fnCheckFileSysCorruption(unlink(destination)); //clean up bad file
        return status;
    }

    return POSIX_SUCCESS;
}

/* *************************************************************************
// DESCRIPTION:
// Read contents of file into provided buffer
// - intended for reading entire file into buffer
// - if file is bigger than buffer, then only a partial read is done to fill up buffer
// Returns number of bytes read or a negative status code if failure occurs
// --------------------------------------------------------------------------*/
INT fsReadFile(char *filename, unsigned char * bufptr, int bufsize)
{
	INT status = POSIX_ERROR;
    FILE *fileHandle = NULL;
	size_t readlen;

    // open source and read it all into buffer
    fileHandle = fopen(filename,"rb");
    if (fileHandle == NULL)
    {
		char aErr[80] = {0};
		fnCheckFileSysCorruption(POSIX_ERROR);
		strerror_r(errno, aErr, sizeof(aErr)-1);
        /* Couldn't open the file. */
		dbgTrace(DBG_LVL_ERROR,"fsReadFile Error: Could not open file %s. Error: %s ", filename, aErr);
    	return status;
    }

	// read data from file
	readlen = fread((void*)bufptr, 1, bufsize, fileHandle);
	if (readlen <= 0)
    {
        fnCheckFileSysCorruption(POSIX_ERROR);
        status = fclose(fileHandle);
        fnCheckFileSysCorruption(status);
		dbgTrace(DBG_LVL_ERROR,"fsReadFile Error: Read error from file %s: %d ", filename, readlen);
		return readlen;
    }

	// close input file
	status = fclose(fileHandle);
	fnCheckFileSysCorruption(status);

	return readlen;
}

/* *************************************************************************
// DESCRIPTION:
// Delete file if it exists
// Returns TRUE if file was found and deleted, else FALSE
// --------------------------------------------------------------------------*/
BOOL fsDeleteFile(char *filename)
{
    BOOL retval = FALSE;
    struct stat fileInfo;
    int fileStatus;
    char aErr[80] = {0};

    // check if filename exists
    fileStatus = stat( filename, &fileInfo );
    if (fileStatus == POSIX_ERROR)
    {
        fnCheckFileSysCorruption(fileStatus);
        if (errno != ENOENT) // Don't log error if file does not exist (this is normal)
        {
            strerror_r(errno, aErr, sizeof(aErr)-1);
            dbgTrace(DBG_LVL_ERROR,"fsDeleteFile Error: file %s status retrieval error: %s ", filename, aErr);
        }
        errno = ENOERR; //clear errno
        return retval;
    }

    // File exists so delete it
    fileStatus = unlink( filename);
    if (fileStatus == POSIX_ERROR)
    {
        fnCheckFileSysCorruption(fileStatus);
        strerror_r(errno, aErr, sizeof(aErr)-1);
        dbgTrace(DBG_LVL_ERROR,"fsDeleteFile Error: file %s deletion error: %s ", filename, aErr);
        errno = ENOERR; //clear errno
        return retval;
    }

    return TRUE;
}

/* *************************************************************************
// OUTPUTS:
//      Size of the file.
//		If the file does not exist, the function returns 0.
//
// --------------------------------------------------------------------------*/
UINT32 fsGetFileSize( const char *pFileName )
{
    UINT32  ulSize = 0;
	struct stat fileStat;
	int errorCode;

    errorCode = stat( pFileName, &fileStat );
    if( errorCode == POSIX_SUCCESS )
    {
        ulSize = fileStat.st_size;
    }
    else
    {
        fnCheckFileSysCorruption(errorCode);
    }
    
	errno = ENOERR; //clear errno
    return( ulSize );
}

/* **********************************************************************
// FUNCTION NAME: char *fnFindFirstFileWithExt
// PURPOSE: Scan the file system looking for a file with the given extension
//          The name of the first file that matches the extension is
//          is copied into the name buffer given else null is returned
//          for no match
// INPUTS: Extension String to Match, Starting address of buffer to store name
// RETURNS: returns the start of the name else null if no match found
// **********************************************************************/

unsigned char *fnFindFirstFileWithExt( char *ext , unsigned char *nameBuf )
{
    DIR *dirp;
    struct dirent *dent = NULL;
    //  struct stat sbuf;
    unsigned char *name = ( unsigned char *) NULL;
    char *sp;
    struct dirent entry;
    int ret_status = POSIX_SUCCESS;


    dirp = opendir( fs_root  );
    if(dirp)
    {
        while( ((ret_status = readdir_r( dirp, &entry, &dent )) == POSIX_SUCCESS) && (dent != NULL))
        {
            sp = strstr( ( const char * ) dent->d_name , ( const char *) ext );
            if(sp)
            {
                // if the prefix is found make sure its at the end of the file
                if(strcmp( sp , ext ) == 0 )
                {
                    (void)strcpy( ( char *) nameBuf , ( char *) dent->d_name );
                    name = nameBuf;
                    break;
                }
            }
        }
        (void)closedir(  dirp );
    }
    else
       ret_status = POSIX_ERROR;

    if (ret_status != POSIX_SUCCESS)
    {
        ret_status = POSIX_ERROR;
        fnCheckFileSysCorruption(ret_status);
    }
    
    return(name);
}

