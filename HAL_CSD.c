/*=====================================================================
//
//	FileName:	HAL_CSD.c
//
//	Description: General Hardware Abstraction Layer implementation for CSD
* ----------------------------------------------------------------------
*               Copyright (c) 2016 Pitney Bowes Inc.
*                        37 Executive Dr
*                        Danbury, CT 06810
* ----------------------------------------------------------------------
*   MODIFICATION HISTORY:
*
=====================================================================*/
#include "commontypes.h"
#include "stdio.h"
#include "hal.h"
#include "custdat.h"
//#include "apphdr.h"

/* Dummy specific stuff */
extern unsigned char dummyFlash[];
//extern unsigned char dummyEMDRAM[];
extern BOOL RamEMDPresent;
extern unsigned char G900EMD_UNC[];
extern unsigned int G900EMD_UNC_len;
extern unsigned char dummyKeyBuf[];

// FRAM stuff
//// Memory
extern UINT32       _ld_text_start;
//extern UINT32       __start_sigcmos;
//extern UINT32       __stop_sigcmos;
extern UINT32       __start_cmos;
extern UINT32       __stop_cmos;
//extern UINT32       __start_syslogcmos;
//extern UINT32       __stop_syslogcmos;
extern UINT32       __start_cmos2;
extern UINT32       __stop_cmos2;
extern UINT32		__start_pm_cmos;
//extern UINT32		__stop_pm_cmos;

static BOOL DummyHALInitialized = FALSE;


//static void InitDummyEMD(void)
//{
//	/* Init EMD RAM using data from a real FPHX EMD generated on 12/12/2014.
//	 * For now copy all data keeping original endianness.
//	 */
//#ifndef USE_USBMS_DEV
//	memcpy(dummyEMDRAM, G900EMD_UNC, G900EMD_UNC_len);
//	RamEMDPresent = TRUE;
//#else
//	RamEMDPresent = TRUE;
//	UINT32 length = G900EMD_UNC_len;
//
//	ReadEMDFile(dummyEMDRAM, &length);
//
//#endif
//
//}

static void InitDummyKeyData(void)
{
	memset(dummyKeyBuf, 0xFF, 12);

}

/* no task sync for this because at least one HAL function will be called in Application_Initialize */
static void InitDummyHAL(void)
{

#ifdef USE_USBMS_DEV
	InitUsbmsDev();
#endif

	//InitDummyEMD();
	//InitDummyCMOS();
	InitDummyKeyData();

	DummyHALInitialized = TRUE;

}

/* **********************************************************************
 * Start of abstraction layer for dummy hardware access.
 * For eval board they are just dummy accesses.
 * For a real board they would be implemented in another,
 * processor specific file similar to this one.
*************************************************************************/

// LCD - all of these should go away when lcdfphx.c goes away
void HALLCDSetPixelGroup8bpp(unsigned long lwXCoord, unsigned long lwYCoord, unsigned long lwPattern, unsigned long lwNumPixelsToSet, unsigned long lwBackgroundColor, unsigned long lwForegroundColor)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALLCDSetPixelBlock8bpp(unsigned long lwXCoord, unsigned long lwYCoord, unsigned char *pBitmap, unsigned long lwNumPixelsToSetX, unsigned long lwNumPixelsToSetY, unsigned long lwBackgroundColor, unsigned long lwForegroundColor)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALLCDRefreshASIC160x80(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALLCDClearEpson480x272(unsigned long lwFillColor)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALLCDInitASIC160x80()
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALLCDInitEpson480x272()
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALLCDShowPaletteEpson480x272()
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALLCDOffEpson480x272()
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALLCDOffASIC160x80()
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALLCDInvertPixel8bpp(unsigned long lwXCoord, unsigned long lwYCoord)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALLCDSetContrastASIC160x80(unsigned char bSetting)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALLCDSetBacklightEpson480x272(unsigned char bIntensity)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

unsigned char HALLCDGetBacklightEpson480x272()
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

    return 0;
}

// Main HW
void HALTurnOffHwInterrupts(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

char * HALGetLowVersion(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();
    return("1.0");
}

char * HALGetProtVersion(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();
    return("1.0");
}

UINT8 HALGetMajorHardwareVersion(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();
    return 2;
}

UINT8 HALGetMinorHardwareVersion(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();
    return 2;
}

void HALFlashWriteEnable(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALFlashWriteDisable(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void * HALGetSysRAMBase(void)
{
	return (void *) &_ld_text_start;
}


// Keypad
void HALInitKeyMatrix(unsigned char *aCurrentKeyMatrix)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();
	memset(aCurrentKeyMatrix, 0xFF, 9);
}

void HALGetKeyMatrix(unsigned char *aCurrentKeyMatrix)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();
	memcpy(aCurrentKeyMatrix, dummyKeyBuf, 9);

}

void HALInitKeyScanLines(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

// Feeder
void HALFeederSet27Volts(BOOL onOff)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALFeederSetPower(BOOL onOff)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALFeederEnterBootMode(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALFeederEnterNormalMode(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALFeederReset(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

// IButton
void HALStartIButton(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALClearIButtonIRQ(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALDisableIButtonInts(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALEnableIButtonInts(UINT32 iButtonRVal)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALSetIButtonComputeState(unsigned short OnOff)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}


// LAN
void HALReconnectLAN(char staticIPFlag)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALDisconnectLAN(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

UINT32 HALGetLANIRQ(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();
	return 15;
}

UINT32 HALGetLANIOAddr(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();
	return 0x0;
}

void HALInitLAN(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALSetIPConfigComplete(BOOL onOff)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALSoftResetLAN(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALWriteMACAddressLAN(UINT8 *pMACAddress)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

void HALReadMACAddressLAN(UINT8 *pMACAddress)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();

}

BOOL HALGetLANLinkStatus(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();
	return TRUE;
}

unsigned char * HALGetCMOSBase(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();
	return  (unsigned char *) &__start_cmos;
}

// Returns size in bytes
unsigned int    HALGetCMOSSize(void)
{
	return (unsigned int) (sizeof(UINT32) * (&__stop_cmos - &__start_cmos));
}

unsigned char * HALGetCMOS2Base(void)
{
	return  (unsigned char *) &__start_cmos2;
}

// Returns size in bytes
unsigned int    HALGetCMOS2Size(void)
{
	return (unsigned int) (sizeof(UINT32) * (&__stop_cmos2 - &__start_cmos2));
}

unsigned char * HALGetPM_CMOSBase(void)
{
	if (DummyHALInitialized == FALSE) InitDummyHAL();
	return  (unsigned char *) &__start_pm_cmos;
}



