
#ifdef MEMORYTEST

#include <stdio.h>         /* for printf / sprintf prototype */
#include <stdlib.h>
#include "nucleus.h"
#include "networking/nu_json.h"
#include "memory_tests.h"
#include "math.h"
#include "sys_log_demo.h"
#include "events.h"

#include "framTests.h"
#include "DDR_Test.h"
#include "nortest.h"
#include "utils.h"


static memory_test_data_t test_info= {{0,0,0,0,0,0,0}, 0, 0, 0};
void gettestdata(memory_test_data_t *testdata);

/* -------------------------------------------------------------------------------------------------[init]-- */
void memory_test_init(memory_test_ev_data_t *test_req, UINT32 result, UINT32 addr)
{
	memcpy(&test_info.memory_test_req, test_req, sizeof(memory_test_ev_data_t));

	test_info.result  = result;
	test_info.info = addr;

	    sys_log_add_string(LOG_SRC_MEMORY_TEST, "memory_test_init");
    sys_log_add(LOG_SRC_MEMORY_TEST, lT_MEMORY_TEST, &test_info, sizeof(test_info));
}

/* -----------------------------------------------------------------------------------------[memory_test_get_result]-- */
uint32_t memory_test_get_result()
{
    return test_info.result;
}

/* ------------------------------------------------------------------------------------------------[perform_memory_test]-- */
//bool perform_memory_test(memory_test_data_t *test_data)
//{
//    bool rv = true;
//
//    //TODO
//
//
////    if(test_info.memory_test_req.memory_type == DDR)
////    {
////    	rv  = PerformDDRTests(&test_info);
////    }
////    else if(test_info.memory_test_req.memory_type == FRAM)
////    {
////    	rv  = PerformFRAMTests(&test_info);
////    }
////    else if(test_info.memory_test_req.memory_type == FLASH)
////    {
////    	rv  = PerformFLASHTests(&test_info);
////    }
//
//
//    sys_log_add_sprintf(LOG_SRC_MEMORY_TEST, "Memory test val=%u rv=%d", 0, rv);
//    sys_log_add(LOG_SRC_MEMORY_TEST, lT_MEMORY_TEST, &test_info, sizeof(test_info));
//
//    return rv;
//}

/* ------------------------------------------------------------------------------------------------------[ToString]-- */
static CHAR* ToString(UINT64 val, CHAR *dst)
{
	//UINT64 div = (UINT64) pow(10, psd_regs.NumDecPlaces);
	sprintf(dst, "%llu, %d", val, 0);
	return dst;
}

#define BUFFER_SIZE 512

#ifdef USE_cJSON
/* --------------------------------------------------------------------------------[memory_test_get_json_object_cJSON]-- */
void memory_test_get_json_object_cJSON(cJSON *root)
{
	cJSON *MemoryTestResult;
	CHAR   str[32];
	static CHAR *buffer = NULL;



	cJSON_AddItemToObject(root, "MemoryTestResult", MemoryTestResult=cJSON_CreateObject());


	gettestdata(&test_info);

	cJSON_AddStringToObject(MemoryTestResult,      "MemoryType", MemoryTypeString(test_info.memory_test_req.memory_type, str)     );
	cJSON_AddStringToObject(MemoryTestResult,        "TestType", TestTypeString(test_info.memory_test_req.test_type, str)       );
//	cJSON_AddNumberToObject(MemoryTestResult,        "Test Delay", test_info.memory_test_req.delay	      );
//	cJSON_AddNumberToObject(MemoryTestResult,        "Test Count", test_info.memory_test_req.count	      );
	cJSON_AddNumberToObject(MemoryTestResult,          "Address", test_info.memory_test_req.address         );
	cJSON_AddNumberToObject(MemoryTestResult,     		  "Size", test_info.memory_test_req.size			  );
	cJSON_AddStringToObject(MemoryTestResult, 		    "Result", ((test_info.result)?"Failed":"Success")     );
	cJSON_AddNumberToObject(MemoryTestResult,   		  "Info", test_info.info 				 			  );

	if ( (test_info.buffer != NULL) && (test_info.memory_test_req.size > 4))
	{

		if(buffer == NULL)
			buffer = malloc((test_info.memory_test_req.size * 2) + 2);

		memset(buffer, 0, (test_info.memory_test_req.size * 2) + 2);
		fnPackedBcdToAsciiBcd(buffer, test_info.buffer, test_info.memory_test_req.size );

		cJSON_AddStringToObject(MemoryTestResult,   		  "Buffer", buffer 				 			  );

		//free(buffer);
	}
}
#endif

CHAR* MemoryTypeString(enum_memory_type type, CHAR *dst)
{
	switch(type)
	{
	case DDR:
		sprintf(dst, "%s", "DDR");
		break;
	case FRAM:
		sprintf(dst, "%s", "FRAM");
		break;
	case FLASH:
		sprintf(dst, "%s", "FLASH");
		break;
	default:
		sprintf(dst, "%s", "UNKNOWN");
		break;

	}
	return dst;
}

CHAR* TestTypeString(enum_memory_test_type type, CHAR *dst)
{
	switch(type)
	{
	case WALKING0:
		sprintf(dst, "%s", "WALKING 0");
		break;
	case WALKING1:
		sprintf(dst, "%s", "WALKING 1");
		break;
	case ADDRESS_BUS:
		sprintf(dst, "%s", "ADDRESS_BUS");
		break;
	case ADDRESS_BUS_CRC:
		sprintf(dst, "%s", "ADDRESS_BUS_CRC");
		break;
	case READ:
		sprintf(dst, "%s", "READ");
		break;
	case READ_WORD:
		sprintf(dst, "%s", "READ_WORD");
		break;
	case READ_VERIFY:
		sprintf(dst, "%s", "READ_VERIFY");
		break;
	case WRITE:
		sprintf(dst, "%s", "WRITE");
		break;

	case WRITE_WORD:
		sprintf(dst, "%s", "WRITE_WORD");
		break;
	case FILL:
		sprintf(dst, "%s", "FILL");
		break;
	case ERASE_SECTOR:
		sprintf(dst, "%s", "ERASE_SECTOR");
		break;
	case ERASE_CHIP:
		sprintf(dst, "%s", "ERASE_CHIP");
		break;
	case DATA_RETENTION:
		sprintf(dst, "%s", "DATA_RETENTION");
		break;
	default:

		sprintf(dst, "%s", "UNKNOWN");
		break;

	}
	return dst;
}

/* -----------------------------------------------------------------------------[psd_regs_get_json_object_json_lib]-- */
void memory_test_get_json_object_json_lib(CHAR **p_obj, INT *obj_len)
{
	static BOOLEAN handle_created = NU_FALSE;
	static JSON_GEN_HANDLE jhandle;
	CHAR   str[32];


	if (handle_created)
	{
		NU_JSON_Generator_Destroy(&jhandle);
	}

	NU_JSON_Generator_Create(BUFFER_SIZE, &jhandle);
	handle_created = NU_TRUE;

    NU_JSON_Generator_Start_Token(&jhandle, NU_NULL, JSON_TOKEN_TYPE_OBJECT);
	NU_JSON_Generator_Start_Token(&jhandle, "Header", JSON_TOKEN_TYPE_OBJECT);
	NU_JSON_Generator_Add_Name(&jhandle,"Status"); NU_JSON_Generator_Add_String(&jhandle, "abc", 0);
	NU_JSON_Generator_End_Token(&jhandle, JSON_TOKEN_TYPE_OBJECT);
	NU_JSON_Generator_Start_Token(&jhandle, "MemoryTests", JSON_TOKEN_TYPE_OBJECT);

	NU_JSON_Generator_Add_Name(&jhandle,      "MemoryType"); NU_JSON_Generator_Add_String(&jhandle, MemoryTypeString(test_info.memory_test_req.memory_type, str), 0);
	NU_JSON_Generator_Add_Name(&jhandle,        "TestType"); NU_JSON_Generator_Add_String(&jhandle, TestTypeString(test_info.memory_test_req.test_type, str), 0);
	NU_JSON_Generator_Add_Name(&jhandle,      	  "Address"); NU_JSON_Generator_Add_String(&jhandle, ToString(test_info.memory_test_req.address, str), 0);
	NU_JSON_Generator_Add_Name(&jhandle,      		 "Size"); NU_JSON_Generator_Add_UInt(&jhandle,   test_info.memory_test_req.size);
	NU_JSON_Generator_Add_Name(&jhandle, 		   "Result"); NU_JSON_Generator_Add_UInt(&jhandle,   test_info.result);
	NU_JSON_Generator_Add_Name(&jhandle,   "Info"); NU_JSON_Generator_Add_UInt(&jhandle,   test_info.info);

	NU_JSON_Generator_End_Token(&jhandle, JSON_TOKEN_TYPE_OBJECT);
 	NU_JSON_Generator_End_Token(&jhandle, JSON_TOKEN_TYPE_OBJECT);

 	/* Get a pointer to the generated data, so we can send it. */
 	NU_JSON_Generator_Get_Buffer(&jhandle, p_obj, obj_len);
}

/* ------------------------------------------------------------------------------[memory_test_get_json_object_sprintf]-- */
void memory_test_get_json_object_sprintf(CHAR **p_obj, INT *obj_len)
{
    static CHAR rsp_buff[BUFFER_SIZE];
    CHAR str[32];

    sprintf(rsp_buff,
    		"{ \"Header\":{\"Status\":\"xyz\" }\r\n"
                    "  \"MemoryTests\":  {\r\n"

                    "         \"MemoryType\": \"%s\",\r\n"
                    "          \"TestType\": \"%s\",\r\n"
                    "         \"Address\": \"%s\",\r\n"
                    "        \"Size\": %lu,\r\n"
                    "    \"Result\": %lu,\r\n"
                    "          \"Info\": %lu }\r\n"
                    "    }",
                    MemoryTypeString(test_info.memory_test_req.memory_type, str),
                    TestTypeString(test_info.memory_test_req.test_type, str),
                    ToString(test_info.memory_test_req.address, str),
                    test_info.memory_test_req.size,
                    test_info.result,
                    test_info.info);

    *obj_len = strlen(rsp_buff);
    *p_obj = rsp_buff;
}

/* --------------------------------------------------------------------------------------[memory_test_get_json_object]-- */
void memory_test_get_json_object(CHAR **p_obj, INT *obj_len, int generator)
{
	return (generator==0 ? memory_test_get_json_object_sprintf(p_obj, obj_len)
			             : memory_test_get_json_object_json_lib(p_obj, obj_len) );
}

typedef void (*test_action_fn)(memory_test_data_t *memory_test);



#define DDR_TEST_BUFFER_SIZE 0x10000

uint8_t ddr_test_buffer[DDR_TEST_BUFFER_SIZE];

void ddr_walking0(memory_test_data_t *memory_test)
{
	memory_test->info = memTestDataBus_WalkinZero((void*)(ddr_test_buffer+ memory_test->memory_test_req.address));
	if(memory_test->info)
			memory_test->result = 1;
}

void ddr_walking1(memory_test_data_t *memory_test)
{
	memory_test->info = memTestDataBus((void*)(ddr_test_buffer+ memory_test->memory_test_req.address));
	if(memory_test->info)
			memory_test->result = 1;
}

void ddr_addres_bus(memory_test_data_t *memory_test)
{
	if(memory_test->memory_test_req.size < DDR_TEST_BUFFER_SIZE)
	{
		memory_test->info = (uint32_t)memTestAddressBus((void*)(ddr_test_buffer+ memory_test->memory_test_req.address), memory_test->memory_test_req.size);
		if(memory_test->info)
				memory_test->result = 1;
	}
	else
	{
		memory_test->result = 0x100;
	}
}

void ddr_addres_bus_crc(memory_test_data_t *memory_test)
{
	if(memory_test->memory_test_req.size < DDR_TEST_BUFFER_SIZE)
	{
		memory_test->info = (uint32_t)CRCTestDevice((void*)(ddr_test_buffer+ memory_test->memory_test_req.address), memory_test->memory_test_req.size);
		if(memory_test->info)
			memory_test->result = 1;
	}
	else
	{
		memory_test->result = 0x200;
	}
}

void ddr_read(memory_test_data_t *memory_test)
{
	if(memory_test->memory_test_req.size < DDR_TEST_BUFFER_SIZE)
	{
		memory_test->result = 0;
		memcpy((void*)&memory_test->info, (void*)(ddr_test_buffer+ memory_test->memory_test_req.address), sizeof(memory_test->memory_test_req.value16));

		if(memory_test->memory_test_req.size > 4)
		{
			if(memory_test->memory_test_req.size > 512)
				memcpy((void*)memory_test->buffer, (void*)memory_test->memory_test_req.address, 512);
			else
				memcpy((void*)memory_test->buffer, (void*)memory_test->memory_test_req.address, memory_test->memory_test_req.size);
		}
	}
	else
	{
		memory_test->result = 0x200;
	}
}

void ddr_read_word(memory_test_data_t *memory_test)
{
	if(memory_test->memory_test_req.size < DDR_TEST_BUFFER_SIZE)
	{
		memory_test->result = 0;
		memcpy((void*)&memory_test->info, (void*)(ddr_test_buffer+ memory_test->memory_test_req.address), sizeof(memory_test->memory_test_req.value16));

		if(memory_test->memory_test_req.size > 4)
		{
			if(memory_test->memory_test_req.size > 512)
				memcpy((void*)memory_test->buffer, (void*)memory_test->memory_test_req.address, 512);
			else
				memcpy((void*)memory_test->buffer, (void*)memory_test->memory_test_req.address, memory_test->memory_test_req.size);
		}
	}
	else
	{
		memory_test->result = 0x200;
	}

}

void ddr_read_verify(memory_test_data_t *memory_test)
{
	if(memory_test->memory_test_req.size < DDR_TEST_BUFFER_SIZE)
	{
		memory_test->result = 0;

		memcpy((void*)&memory_test->info, (void*)(ddr_test_buffer+ memory_test->memory_test_req.address), sizeof(memory_test->memory_test_req.value16));
	}
	else
	{
		memory_test->result = 0x200;
	}

}


void ddr_write(memory_test_data_t *memory_test)
{
	if(memory_test->memory_test_req.size < DDR_TEST_BUFFER_SIZE)
	{
		memory_test->result = 0;

		memcpy((void*)(ddr_test_buffer+ memory_test->memory_test_req.address), (void*)&memory_test->memory_test_req.value16, sizeof(memory_test->memory_test_req.value16));
	}
	else
	{
		memory_test->result = 0x200;
	}
}

void ddr_fill(memory_test_data_t *memory_test)
{
	if(memory_test->memory_test_req.size < DDR_TEST_BUFFER_SIZE)
	{
		memory_test->result = 0;
		memcpy((void*)(ddr_test_buffer+ memory_test->memory_test_req.address), (void*)&memory_test->memory_test_req.value8, memory_test->memory_test_req.size);
	}
	else
	{
		memory_test->result = 0x200;
	}


}


/////////////////////////////////////////



///////////////


//bool PerformFRAMTests(memory_test_data_t *memory_test)
//{
//	bool rv = true;
//	uint32_t count = 0;
//
//	while(count < fram_test_tbl_size)
//	{
//		if (fram_test_tbl[count].test  == memory_test->memory_test_req.test_type)
//		{
//			/* found matching event */
//			fram_test_tbl[count].testfn(memory_test);
//
//			break;
//		}
//
//		count++;
//	}
//	return rv;
//}


//bool PerformFLASHTests(memory_test_data_t *memory_test)
//{
//	bool rv = true;return rv;
//}



typedef struct{
	enum_memory_test_type test;
	test_action_fn testfn;
}test_tbl_entry;



static test_tbl_entry ddr_test_tbl[] = {
		{WALKING0,			ddr_walking0},
		{WALKING1,			ddr_walking1},
		{ADDRESS_BUS,		ddr_addres_bus},
		{ADDRESS_BUS_CRC,	ddr_addres_bus_crc},

		{READ,				ddr_read},
		{READ_WORD,			ddr_read_word},
		{READ_VERIFY,		ddr_read_verify},

		{WRITE,				ddr_write},
		{FILL,				ddr_fill},

};

//const uint32_t ddr_test_tbl_size = (sizeof(ddr_test_tbl)/sizeof(test_tbl_entry));
#define DDR_TEST_TBL_SIZE (sizeof(ddr_test_tbl)/sizeof(test_tbl_entry))


test_tbl_entry fram_test_tbl[] = {

		{DATA_RETENTION,	fram_data_retention},

		{READ,				fram_read_test},
		//		{READ_WORD,			fram_read_word},
//		{READ_VERIFY,		fram_read_verify},

		{WRITE,				fram_write_test},
		{WRITE_WORD,		fram_write_16_test},
		{FILL,				fram_fill_test},

};

//const uint32_t fram_test_tbl_size = sizeof(fram_test_tbl)/sizeof(test_tbl_entry);
#define FRAM_TEST_TBL_SIZE  (sizeof(fram_test_tbl)/sizeof(test_tbl_entry))


test_tbl_entry flash_test_tbl[] = {
		{ERASE_CHIP,				flash_erase_chip_test},
		{ERASE_SECTOR,				flash_erase_test},
//
		{DATA_RETENTION,			flash_data_retention},

		{READ,				flash_read_test},
//		{READ_WORD,			flash_read_word},
//		{READ_VERIFY,		flash_read_verify},
//
		{WRITE,				flash_write_test},
		{WRITE,				flash_write_test},
//		{FILL,				flash_fill},

};

//const uint32_t flash_test_tbl_size = sizeof(flash_test_tbl)/sizeof(test_tbl_entry);
#define FLASH_TEST_TBL_SIZE (sizeof(flash_test_tbl)/sizeof(test_tbl_entry))

typedef struct{
	enum_memory_type memory_type;
	test_tbl_entry *test_tbl;
	uint32_t 		test_tbl_size;
}test_memory_tbl_t;

test_memory_tbl_t test_memory_tbl[] = {
		{DDR,				ddr_test_tbl, 		DDR_TEST_TBL_SIZE },
		{FRAM,				fram_test_tbl, 		FRAM_TEST_TBL_SIZE},
		{FLASH,				flash_test_tbl, 	FLASH_TEST_TBL_SIZE}
};

uint32_t test_memory_tbl_size = sizeof(test_memory_tbl)/sizeof(test_memory_tbl_t);


bool PerformMemoryTests(memory_test_data_t *memory_test)
{
	bool rv = true;
	uint32_t cnt_test = 0;
	int cnt_mem;
	bool found = false;

	for(cnt_mem = 0; (found == false) && (cnt_mem < test_memory_tbl_size); cnt_mem++)
	{
		if(test_memory_tbl[cnt_mem].memory_type == memory_test->memory_test_req.memory_type)
		{

			for(cnt_test = 0; (cnt_test < test_memory_tbl[cnt_mem].test_tbl_size); cnt_test++)
			{
				if (test_memory_tbl[cnt_mem].test_tbl[cnt_test].test  == memory_test->memory_test_req.test_type)
				{
					/* found matching event */
					test_memory_tbl[cnt_mem].test_tbl[cnt_test].testfn(memory_test);
					found = true;
					break;
				}

			}
		}
	}

	if(found == false)
	{
		memory_test->result = 0xFFFFFFFF;
	}

	sys_log_add_sprintf(LOG_SRC_MEMORY_TEST, "Memory test val=%u rv=%d", 0, rv);
	sys_log_add(LOG_SRC_MEMORY_TEST, lT_MEMORY_TEST, &memory_test, sizeof(memory_test));

	return rv;
}


//bool PerformDDRTests(memory_test_data_t *memory_test)
//{
//	bool rv = true;
//	uint32_t count = 0;
//
//	while(count < ddr_test_tbl_size)
//	{
//		if (ddr_test_tbl[count].test  == memory_test->memory_test_req.test_type)
//		{
//			/* found matching event */
//			ddr_test_tbl[count].testfn(memory_test);
//
//			break;
//		}
//
//		count++;
//	}
//	return rv;
//}

#endif /* MEMORYTEST */

