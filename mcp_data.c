/*SB
   ************************************************************************
     PROJECT:	MidJet Motion Controller
     AUTHOR:    Gary S. Jacobson                                           
     $Workfile:   mcp_data.c  $
     $Revision:   1.0  $
     MODULE NAME:	MCP_DATA                                              
     DESCRIPTION: 
     			This module contains all of the data structures that are used run-time by the system.
     		This includes the profiles, motor control blocks, step tables, acceleration tables, servo
     		control data and processor configuration.  The functions that create the motor control blocks
     		from the motor configuration data are also in this module.
     		
     
   ----------------------------------------------------------------------
                                                                        
                 Copyright (c) 2001 Pitney Bowes Inc.                   
                      35 WaterView Drive                                
                     Shelton, Connecticut  06484                        
                                                                        
   ----------------------------------------------------------------------
  		PUBLIC FUNCTIONS:	
				ConfigureMotor
				InitMotors
				ResetProfileTrace
				InitMcpData
				SetBoardID
				 			
  		PRIVATE FUNCTIONS:
				SetUpTPU_PWMs
				SetUpTPU_Encoders
                SetUpSolenoids
                
   Revision History Notes:
   -----------------------
   $Log:   H:/group/SPARK/ARCHIVES/SebringBase/H8s_2357/mcp_data.c_v  $
 SE
*/
#include <stddef.h>
#include <string.h>
#include "dtc.h"
#include "bitdef.h" 
//#include "mcp.h"
#include "mcp_data.h"
//#include "appl_msg.h"

INSTALLED_PROFILE InstalledProfile[MAX_PROFILES];
TRACE_HISTORY  *fpTraceHistory;
unsigned int wTraceHistIndx = 0;
COMM_LOG *fpCommLog;
unsigned int wCommLogIndx = 0;

unsigned char * pProfileCfg = NULL;
unsigned char bMicroId;
unsigned char bFirstNvmErrorStatus;
unsigned char bFirstNvmCounter;
unsigned char bBoardId;

//TODO - confirm correctness
/*
		bMicroID's

		Apollo AFIS		0x40
		Apollo SLRSTK	0x41
		Apollo BDT		0x42
		MMS S7000		0x43
		MMS	M3			0x44
		MidJet DM400	0x45
		MidJet DM500	0x45
		MidJet DM550	0x45
		MidJet DM550	0x45
		MidJet DM600	0x45
		MidJet DM819	0x45
		MidJet DM821	0x45
		MidJet DM829	0x45
		DM400C Feeder	0x46

		C-Spark PMC		0x49

		MidJet Stacker	0x4D
		MidJet WoW		0x4E
						0x4F
		Sebring MCP1	0x50
		Sebring MCP2	0x51
		Sebring MCP3	0x52


		DM400c Feeder	0x6C


*/
const char * const szMCP_PCN[] = {  
					"Sebring MCP1",	// 0
					"Sebring MCP2", // 1
					"Sebring MCP3", // 2
					"Sebring MCP4", // 3
					"Sebring MCP5", // 4
					"Sebring MCP6", // 5
					"Sebring MCP7", // 6
					"", // 7
					"", // 8
					"C-Spark", // 9
					"", // A
					"", // B
					"DM400c Feeder",// C
					"MidJet STKR",  // D
					"MidJet WOW",   // E
					"?????"
				 }; 


unsigned char fbSleepModeActive = 0;					
unsigned char fbResumePowerDown=0;				 
unsigned char bPowerDownRequestLevel;
unsigned char fbSendProfileCancelMsg;
unsigned int  uLastMotor;		/* Last Motor Used */
unsigned int  uLastProfile;		/* Last Profile Used */
	

ACCEL_TABLE AccelTable[MAX_ACCEL_TBL];
MAINT_TABLE MaintTable[MAX_MAINT_TBL];

//extern unsigned char fbProfileFlag[];		/* Profile Flag array */
MCP_PROFLAGS profileFlags;

const unsigned char bCWFullStep[] = 
			{
				B01010101,  	// 1
				B10011001,      // 2
				B10101010,      // 3
				B01100110       // 4
			};

const unsigned char bCCWFullStep[] = 
			{
				B01100110,
				B10101010,
				B10011001,							
				B01010101 	  
			};
/*
	L6202
*/
const unsigned char bCWHalfStep[] = 
			{
				B01010101,		// 0x55
				B00010001,		// 0x11
				B10011001,		// 0x99
				B10001000,		// 0x88
				B10101010,		// 0xAA
				B00100010,		// 0x22
				B01100110,		// 0x66
				B01000100       // 0x44
			};

const unsigned char bCCWHalfStep[] = 
			{
				B01000100,      // 0x44
				B01100110,		// 0x66
				B00100010,		// 0x22
				B10101010,		// 0xAA
				B10001000,		// 0x88
				B10011001,		// 0x99
				B00010001,		// 0x11				
				B01010101		// 0x55
			};



const DTC_DIR_CHANGE DtcFwd2RevFull [] =
{
	{0, 0},		// unused
	{2, 1},
	{1, 0},
	{4, 3},
	{3, 2}
};

const DTC_DIR_CHANGE DtcRev2FwdFull [] =
{
	{0, 0},		// unused
	{2, 2},     // 1
	{1, 3},     // 2
	{4, 0},     // 3
	{3, 1}		// 4
};

const DTC_DIR_CHANGE DtcFwd2RevHalf [] =
{
	{0, 0},		// unused
	{7, 6},     // 1
	{6, 5},     // 2
	{5, 4},     // 3
	{4, 3},     // 4
	{3, 2},     // 5
	{2, 1},     // 6
	{1, 0},     // 7
	{8, 7}	    // 8
};

const DTC_DIR_CHANGE DtcRev2FwdHalf [] =
{
	{0, 0},		// unused
	{6, 2},     // 1
	{5, 3},     // 2
	{4, 4},     // 3
	{3, 5},     // 4
	{2, 6},     // 5
	{1, 7},     // 6
	{8, 0},     // 7
	{7, 1}		// 8
};

  

SERVO_BLOCK ServoData[MAX_SERVO];
MOTOR_CFG_BLOCK MotorConfigBlock[MAX_MOTORS];
MOTOR_CFG MotorConfig[MAX_MOTORS];
/*
	Default System Tick Timing
*/
PROC_CFG ProcessorConfig = {188, 	// bDivisor
							  3,	// bPrescaler
							  124,	// bServoDivisor	(DM400C => unused)
							  2,	// bServoPrescaler	(DM400C => unused)
							  30,   //15,	// wLinkPacketTimeout  (set for 30ms)
							  1000, //499	// wLinkDeadmanTimeout (set for 1 sec)
							  };
SOLENOID_CFG_BLOCK SolenoidConfigBlock[MAX_SOLENOIDS];

const unsigned char bMaskBits[] = 	{
						B00000001, /* ITU 0 */
						B00000010, /* ITU 1 */
						B00000100, /* ITU 2 */
						B00001000, /* ITU 3 */
						B00010000, /* ITU 4 */
						B00100000, /* ITU 5 */
						B01000000, 
						B10000000
					};
					

const unsigned char bDriveMode[] = {
					DC_SERVO,	// 0000
					PWM_MODE,	// 0001
					TGL_MODE,	// 0010
					DTC_MODE,	// 0011
					TPC_MODE,	// 0100
					TPC_MODE,	// 0101 - Half step
					UNUSED,		// 0110
					UNUSED,		// 0111
					TPCL_MODE,	// 1000
					CLK_MODE,	// 1001
					DCS_MODE,	// 1010
					UNUSED,		// 1011
					UNUSED,		// 1100
					UNUSED,		// 1101
					UNUSED,		// 1110
					VIRTUAL};	// 1111



void ConfigureMotor(MOTOR_CFG * pMtrCfg)
{
	register MOTOR_CFG_BLOCK * mcb;
	register MOTOR_CFG * cfg;

	DTC_REGISTERS * dtc;
	unsigned long base;

	cfg = pMtrCfg;								/* Pointer to Config Information from Host */

	if (cfg->bDevIdNum >= MAX_MOTORS)
		return;									/* Ignore invalid motors! */

	MotorConfig[cfg->bDevIdNum] = *pMtrCfg;		/* Save copy of cfg block */
	mcb = &MotorConfigBlock[cfg->bDevIdNum];			/* Pointer to Motor Ctrl Block for this Device ID*/

/*
  	Don't allow the MCB to be processed by DoMotion while we are changing it
*/
	mcb->fbChanged 		= 0;            //
	mcb->bState 		= MOTOR_OFF;	// assert motor off state

	mcb->bMotorId 		= cfg->bDevIdNum;				/* Save Device number for error reporting */
	mcb->pfbProfileFlag	= profileFlags.fbProfileFlag + cfg->bDevIdNum;	/* Profile Flag for this Device ID */
	mcb->bLastState		= MOTOR_OFF;
	mcb->bMotorMask		= cfg->bMotorEnableMask;		/* Motor Enable Bit */
	mcb->bITUchan		= cfg->ITUchan;					/* ITU Timer Channel for this motor */
	mcb->bHoldBits 		= cfg->bHoldBitMask;
	mcb->wDirectionBits = cfg->wDirectionMask;
    mcb->bTPCgroup		= cfg->TPCgroup;
    mcb->bPrescaler 	= cfg->Prescaler;
	mcb->bDMAchan		= cfg->DMAchan;

		/* Convert mode bits to mode byte for speed */
	mcb->bMode = bDriveMode[cfg->DriveMode];

	if (mcb->bMode == UNUSED)
		return;

	if (cfg->bDevIdNum + 1 > uLastMotor)
		uLastMotor = cfg->bDevIdNum + 1;		/* Latch Last Motor Used */

							// Ignore invalid drive config
	if (mcb->bMode == DCS_MODE)	{ // Midjet DC with direction
		mcb->bITUchanMask = 0;		// No timer with this motor
		return;
	}


	if ((mcb->bMode == DC_SERVO) && (cfg->Prescaler == LM629_SERVO_TYPE))
			mcb->bMode = LM629_SERVO;


	mcb->pAccelTable = NULL;				/* Default to no Acceleration Table */

	/*

			Add Lead Lag Servo Setup Here

	*/

	if (mcb->bMode == DC_SERVO)
	{
		unsigned char chan;
		SERVO_BLOCK * pServo;

		chan = mcb->bITUchan;    /* bITUchan servo channel number */
		pServo = ServoData + chan;

		pServo->Pd = 0;
		pServo->Gn = 0;
		pServo->Vn = 0;
		pServo->Ac =0;
		pServo->Vf = 0;
		pServo->C1 = mcb->C1 = cfg->wKp;
		pServo->C2 = mcb->C2 = cfg->wKi;
		pServo->C3 = mcb->C3 = cfg->wKd;
		pServo->ErrorLimit = (unsigned) (MAX_ERROR_LIMIT / pServo->C1);
		pServo->Enabled = 0;
		if (cfg->bSRateDivisor)
        	pServo->Qphase = 0x01;		// Use srate divisor for phasing bit
		else
        	pServo->Qphase = 0x00;
		pServo->Direction = cfg->wDirectionMask;   // Use direction bits for full H-Bridge driver
#if 0
// DSD 01/13/2006 - TODO: update this for DM400c
		if (bBoardId == FPFDR) {
			pServo->Pwm = DM400C_PWM_BASE(0);
			pServo->Encoder = DM400C_ENCODER_BASE(1);
		}
		else
		{
			//pServo->Pwm	= (unsigned char *) TCR_PWM_BASE + chan; // 8 bit tmr
//			pServo->Pwm	= (unsigned int *) TCR_PWM_BASE + chan; // 16 bit Timer
			//pServo->Encoder = (unsigned int *) TCR_ENCODER_BASE + (chan << 3);	// 0x10
		}
#endif

		mcb->pServo = pServo;
		mcb->wMaxPosErr = cfg->wMaxCurrent;				/* Max position error before motor shutdown */

		if (mcb->wMaxPosErr)
				pServo->ErrorLimit = mcb->wMaxPosErr;

		return;

	}

}


void InitMcpData(void)
{
	register INSTALLED_PROFILE * p;
	int i;

//	bMicroId = 5; /*(*MICRO_ID & MICRO_ID_MASK) >> MICRO_ID_SHIFT;*/

	for (p = InstalledProfile, i=0; p < InstalledProfile+MAX_PROFILES; i++, p++) {
		p->pProfile = NULL;
		p->bProfile = (unsigned char) i;
		p->bSegment = 0;
		p->uNumberOfSegments = 0;
		p->fbActive = 0;
		p->fbCancel = 0;
	}
	
	for (i = 0; i< MAX_ACCEL_TBL; i++)
		AccelTable[i].pTable = NULL;

	for (i=0; i < MAX_MOTORS; i++) {
		MotorConfig[i].bDevIdNum = MAX_MOTORS;	/* force to unused */
		MotorConfigBlock[i].bState 	 = MOTOR_OFF;	/* assert motor off state */
		MotorConfigBlock[i].bLastState = MOTOR_OFF;
		MotorConfigBlock[i].fbChanged = 0;
		MotorConfigBlock[i].pfbProfileFlag	= profileFlags.fbProfileFlag + i;
	}	
	for (i=0; i< MAX_SERVO; i++) {
		ServoData[i].Vn = 0;
		ServoData[i].Vf = 0;
		ServoData[i].Ac = 0;
		ServoData[i].Pd = 0;
		ServoData[i].Error = 0;
		ServoData[i].Gn = 0;
		ServoData[i].Enabled = 0;
		ServoData[i].Direction = 0;
	}
//	InitLogData();
	uLastProfile = 0;
	uLastMotor = 0;

/*	ResetProfileTrace(); */
}

					
