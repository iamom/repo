/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    api_funds.c

   DESCRIPTION:    API implementations for funds messages

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
* HISTORY: 
*
* 2017.07.27 CWB 
* - Added include of api_mfg.h file, which has a prototype for the 
*   fnCheckPSDModeForState function. 
* - In on_GetPSDInfoReq() and on_GetPSDStateReq(), added a call to that new
*   function to the check that the PSD is really running in the APP mode, and
*   if not, add a ProgMode entry to the response message.   
******************************************************************************/

#include "commontypes.h"
#include "cJSON.h"
#include "wsox_tx_queue.h"
#include "wsox_services.h"
#include "bobutils.h"
#include "ipsd.h"
#include "cjunior.h"
#include "oit.h"
#include "oifpinfra.h"
#include "networkmonitor.h"
#include "utils.h"
#include "flashutil.h"
#include"bob.h"
#include "api_utils.h"
#include "api_status.h"
#include "api_mfg.h"        // fnCheckPSDModeForState()
#include "trmdefs.h"
#include "cm.h"
#define  CRLF   "\r\n"

#define __FUNDS_STATUS_DEFINE_TABLES__
#include "api_funds.h"

extern Diagnostics                CMOSDiagnostics;
extern ulong 			  CMOSTrmUploadDueReqd;
extern BOOL fnGetUpdateNow (UINT8 bServiceCode, INT32 value, INT32 value2);
extern SetupParams        CMOSSetupParams;
extern BOOL fnGetIButtonData(UINT16 msgID);
extern STATUS trmGetTrxFileCount(int *fileCount);
extern CM_SYSTEM_STATUS_t sSystemStatus;
extern tRefundData       CMOS2RefundData;

BOOL gBaseNeedsReboot = FALSE;

int _pbpio_debug_channel = 0;
int _pbpio_broadcast = 0;
int _pbpio_group = 0;

/* Distributor synchronization */
static BOOL DistributorInUse = FALSE;

/* Tier 3 synchronization */
static BOOL tier3TransactionInProgress = FALSE;
static char tier3TransactionID[TRAN_ID_BUF_SIZE];

/* utility function */
#define RESPONSE_BUF_SIZE 256

required_fields_tbl_t required_fields_tbl_PerformRefillReq =
    {
    "PerformRefillReq", { "RefillAmount"
                        , "NumDec"
                        , NULL }
    };

required_fields_tbl_t required_fields_tbl_ShowPbpIoReq =
    {
    "ShowPbpIoReq", { "Active"
                    , NULL }
    };

required_fields_tbl_t required_fields_tbl_PerformWithdrawalReq =
    {
    "PerformWithdrawalReq", { NULL }
    };

/* return the value of static variable tier3TransactionInProgress */
BOOL isTier3TransactionInProgress()
{
	return tier3TransactionInProgress;
}

/* Get transaction ID and optionally end transaction
 * This also supports ending the transaction without getting transaction ID
 * So this is not a true getter - it has an important side effect
 *
 * inputs - buffer to hold result
 *          length of buffer
 *          message ID of API that wants it
 *          flag to indicate whether transaction should end or not */
api_status_t getTransactionID(char *transIDBuf, UINT32 buflen, char *msgID, bool endTransaction)
{
    char  semstat;

    // Get semaphore for accessing Tier3 transaction state
    semstat = OSAcquireSemaphore (TIER3_TRANS_SEM_ID, 100);
    if (semstat != SUCCESSFUL)
    {// cannot acquire semaphore even after waiting 100 ms - something is wrong internally
        dbgTrace(DBG_LVL_ERROR,"Error: Failed to acquire Tier3 transaction semaphore in %s: %d\n", msgID, semstat);
        return API_STATUS_NOT_PERFORMED;
    }

    if (tier3TransactionInProgress == FALSE)
    { // assume transaction closed already so return latest transaction ID
        dbgTrace(DBG_LVL_INFO,"Tier3 transaction already closed so returning latest ID for %s", msgID);
    }

    // If buffer provided, make sure it is big enough
    if ((transIDBuf != NULL) && (buflen < (strlen(tier3TransactionID) + 1)))
    {
        (void) OSReleaseSemaphore(TIER3_TRANS_SEM_ID);
        dbgTrace(DBG_LVL_ERROR,"Error: Buffer of %d bytes is too small for TranID in %s", buflen, msgID);
        return API_STATUS_NOT_PERFORMED;
    }

    // if buffer provided, return transaction ID
    if (transIDBuf != NULL)
    {
        strcpy(transIDBuf, tier3TransactionID);
    }
    // end transaction if desired
    if (endTransaction)
    {
        tier3TransactionInProgress = FALSE;
    }
    (void) OSReleaseSemaphore(TIER3_TRANS_SEM_ID);

    return API_STATUS_SUCCESS;

}

/* Set transaction ID and start transaction
 * inputs - transaction ID
 *          message ID of API that wants it */
api_status_t setTransactionID(char *transID, char *msgID)
{
    char  semstat;
    UINT32 transIDlen;

    if (transID == NULL)
    {// Must provide transaction ID
        dbgTrace(DBG_LVL_ERROR,"Error: Failed to provide transaction ID in %s\n", msgID);
        return API_STATUS_NOT_PERFORMED;
    }

    // Get semaphore for accessing Tier3 transaction state
    semstat = OSAcquireSemaphore (TIER3_TRANS_SEM_ID, 100);
    if (semstat != SUCCESSFUL)
    {// cannot acquire semaphore even after waiting 100 ms - something is wrong internally
        dbgTrace(DBG_LVL_ERROR,"Error: Failed to acquire Tier3 transaction semaphore in %s: %d\n", msgID, semstat);
        return API_STATUS_NOT_PERFORMED;
    }

    if (tier3TransactionInProgress == TRUE)
    { // busy so release semaphore and return
        (void) OSReleaseSemaphore(TIER3_TRANS_SEM_ID);
        return API_TRANSACTION_IN_PROGRESS;
    }

    transIDlen = strlen(transID);
    if ((transIDlen + 1) > TRAN_ID_BUF_SIZE)
    {
        (void) OSReleaseSemaphore(TIER3_TRANS_SEM_ID);
        dbgTrace(DBG_LVL_ERROR,"Error: Tier3 transaction TranID %s is too big in %s", transID, msgID);
        return API_STATUS_INVALID_PARAMETERS;
    }

    //store transaction state and ID
    tier3TransactionInProgress = TRUE;
    strcpy(tier3TransactionID, transID);
    (void) OSReleaseSemaphore(TIER3_TRANS_SEM_ID);

    return API_STATUS_SUCCESS;

}

/* Release Distributor
 *
 * inputs - boolean to indicate forced release
*           message ID of caller */
api_status_t releaseDistributor(BOOL forced, char *msgID)
{
    char  semstat;

    // Get semaphore for accessing Tier3 transaction state
    semstat = OSAcquireSemaphore (DISTRIBUTOR_SEM_ID, 100);
    if (semstat != SUCCESSFUL)
    {// cannot acquire semaphore even after waiting 100 ms - something is wrong internally
        dbgTrace(DBG_LVL_ERROR,"Error: Failed to acquire Distributor semaphore in %s: %d\n", msgID, semstat);
        if (forced)
        {
            DistributorInUse = FALSE;
            dbgTrace(DBG_LVL_INFO,"Forced release of Distributor lock");
        }
        return API_STATUS_NOT_PERFORMED;
    }

    if (DistributorInUse == FALSE)
    {
        (void) OSReleaseSemaphore(DISTRIBUTOR_SEM_ID);
        dbgTrace(DBG_LVL_INFO,"Distributor lock already released in %s", msgID);
        return API_STATUS_NOT_PERFORMED;
    }

    DistributorInUse = FALSE;

    (void) OSReleaseSemaphore(DISTRIBUTOR_SEM_ID);

    return API_STATUS_SUCCESS;

}

/* Lock Distributor
 * inputs - message ID of API that wants it */
api_status_t lockDistributor(char *msgID)
{
    char  semstat;

    // Get semaphore for accessing Tier3 transaction state
    semstat = OSAcquireSemaphore (DISTRIBUTOR_SEM_ID, 100);
    if (semstat != SUCCESSFUL)
    {// cannot acquire semaphore even after waiting 100 ms - something is wrong internally
        dbgTrace(DBG_LVL_ERROR,"Error: Failed to acquire Distributor semaphore in %s: %d\n", msgID, semstat);
        return API_STATUS_NOT_PERFORMED;
    }

    if (DistributorInUse == TRUE)
    { // busy so release semaphore and return
        (void) OSReleaseSemaphore(DISTRIBUTOR_SEM_ID);
        return API_CAN_NOT_CONNECT_TO_DISTRIB;
    }

    //store transaction state and ID
    DistributorInUse = TRUE;
    (void) OSReleaseSemaphore(DISTRIBUTOR_SEM_ID);

    return API_STATUS_SUCCESS;

}

/* --------------------------------------------------------------------------------------------[get_psd_regs_cJSON]-- */
void get_psd_regs_cJSON(cJSON *root)
{
    //TODO: JAH THese are cached values and should be taken from the transaction manager when it is implemented
    cJSON *PsdRegs;
    double dDR = 0;
    double dAR = 0;
    double dCS = 0;
    unsigned long uTotalPC = 0;
    unsigned long uZeroPC = 0;
    //unsigned int uNumDecimals = 0;

    cJSON_AddItemToObject(root, "PSDRegisters", PsdRegs=cJSON_CreateObject());
	 
    //Get Descending registers
    //fnGetDescRegNum(&dDR);
    dDR = fnBinFive2Double(mStd_DescendingReg);
    cJSON_AddNumberToObject(PsdRegs,      "DescReg", dDR);

    //fnGetAscRegNum (&dAR);
    dAR = fnBinFive2Double(m5IPSD_ascReg);
    cJSON_AddNumberToObject(PsdRegs,       "AscReg", dAR);
	 
    //fnGetControlSumNum(&dCS);
    dCS = fnBinFive2Double(mStd_ControlSum);
    cJSON_AddNumberToObject(PsdRegs,      "CtrlSum", dCS);

    //fnGetTotalPieceCountNum(&uTotalPC);
    memcpy(&uTotalPC, pIPSD_pieceCount, sizeof(uTotalPC));
    cJSON_AddNumberToObject(PsdRegs,      "PieceCnt", uTotalPC);

    //fnGetZeroPieceCountNum(&uZeroPC);
    memcpy(&uZeroPC, pIPSD_ZeroPostagePieceCount, sizeof(uZeroPC));
    cJSON_AddNumberToObject(PsdRegs,      "ZeroPieceCnt", uZeroPC);

    //fnGetDecimalNum(&uNumDecimals);
    cJSON_AddNumberToObject(PsdRegs,      "NumDec", bVltDecimalPlaces);
}


/* ------------------------------------------------------------------------------------------------[on_GetFundsReq]-- */
void on_GetFundsReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetFundsRsp");

    get_psd_regs_cJSON(rspMsg);

    addEntryToTxQueue(&entry, root, "  GetFundsReq: Added FundsRsp to tx queue. status=%d" CRLF);
}


/* ------------------------------------------------------------------------------------------------[on_GetPSDInfoReq]-- */
void on_GetPSDInfoReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    char temp[20];

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetPSDInfoRsp");

    // Only get PSD data if Bob task has run through init;
    // Otherwise this task will hang for a minute waiting for PSD and
    // then timeout and send garbage back to the caller
    if ( bobsBeenInitialized == TRUE)
    {
		fnCheckPSDModeForState( rspMsg );

		PSD_INFO sPSDInfo;
		memset(&sPSDInfo, 0x0, sizeof(sPSDInfo));
		GetPSDInfo(&sPSDInfo);

		//PSD Version
		cJSON_AddStringToObject(rspMsg, "Version", sPSDInfo.sPSDVersion);

		//PSD PCN
		cJSON_AddStringToObject(rspMsg, "PCN", (char*)sPSDInfo.PCN);

		//PSD Family code
		char str[2]={0};
		sprintf(str, "0x%2X", sPSDInfo.FamilyCode);
	//  memcpy(str, &sPSDInfo.FamilyCode, sizeof(sPSDInfo.FamilyCode));

		cJSON_AddStringToObject(rspMsg, "Family Code", str);

		//PSD Manuf SN
		cJSON_AddStringToObject(rspMsg, "Model Number", sPSDInfo.modelNumber);

		//PSD Manuf SN
		cJSON_AddStringToObject(rspMsg, "Manufacture SN", sPSDInfo.MfgSNAscii);

		//PSD Indicia SN
		cJSON_AddStringToObject(rspMsg, "Indicia SN", (char*)sPSDInfo.indiciaSN);

		//PSD PBI SN
		cJSON_AddStringToObject(rspMsg, "PBI SN", (char*)sPSDInfo.PBISerialNum);

		//POR Count
		cJSON_AddNumberToObject(rspMsg, "POR Count", CMOSDiagnostics.PowerupCount);

		//Family code + manfucture SN + check sum
		sprintf(temp, "%02X%s%02X", sPSDInfo.checkSum, sPSDInfo.MfgSNAscii, sPSDInfo.FamilyCode);
		cJSON_AddStringToObject(rspMsg, "Enhanced Manufacture SN", temp);

		//PSD Born On Date
		DATETIME tempDate;
		char dispDT[11] = {0};
		fnBobGetPSDBatteryDate(&tempDate);
		(void)sprintf(dispDT, "%02d%02d/%02d/%02d",
								tempDate.bCentury, tempDate.bYear, tempDate.bMonth, tempDate.bDay);
		cJSON_AddStringToObject(rspMsg, "PSD Born On Date", dispDT);

		 //PSD Battery Days Remaining
		 cJSON_AddNumberToObject(rspMsg, "PSD Battery Days Remaining", fnBobGetPSDRemainingDays());
    }

    addEntryToTxQueue(&entry, root, "  GetPSDInfoReq: Added GetPSDInfoRsp to tx queue. status=%d" CRLF);
}


extern int packed_bcd_pbp_acct_num_as_int(unsigned long pbp_acct_num);
/* ------------------------------------------------------------------------------------------------[on_GetCustomerInfoReq]-- */
void on_GetCustomerInfoReq(UINT32 handle, cJSON *root)
{
    char scratchPad[20];
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetCustomerInfoRsp");



    //customer PBP account number

    memset(scratchPad, 0, sizeof(scratchPad));
    sprintf(scratchPad,"%d", packed_bcd_pbp_acct_num_as_int(CMOSSetupParams.lwPBPAcctNumber));
    cJSON_AddStringToObject(rspMsg, "PbP Acct", scratchPad);

    // BPN
    char temp[BPN_SIZE + 1] = {0};
	memset(temp, '0', sizeof(temp) - 1);
    if ((strlen(CMOSSetupParams.BPN) == 0) || (strcmp(CMOSSetupParams.BPN, temp) == 0))
    {
		memset(CMOSSetupParams.BPN, 0, sizeof(CMOSSetupParams.BPN));
		memset(CMOSSetupParams.BPN, '0', BPN_SIZE);
    	if (CMOSSetupParams.lwReserved != 0)
    	{
    		memset(temp, 0, sizeof(temp));
    		sprintf(temp, "%ld", CMOSSetupParams.lwReserved);
    		strcpy(CMOSSetupParams.BPN + (BPN_SIZE - strlen(temp)), temp);
    	}
    }
    cJSON_AddStringToObject(rspMsg, "BPN", CMOSSetupParams.BPN);

    //Origin Postal Code
    cJSON_AddStringToObject(rspMsg, "Postal Code", pIPSD_zipCode);

    //PSD Country code

    addEntryToTxQueue(&entry, root, "  GetCustomerInfoReq: Added GetCustomerInfoRsp to tx queue. status=%d" CRLF);
}

/* -------------------------------------------------------------------------------------[on_SaveBaseLogsReq]-- */
api_status_t saveBaseLogsReq(BOOL reboot)
{
	BOOL msgstate;
	api_status_t distStat;
    char sState[50];

	//check if the base is not in power up
    GetUICStates(sState);
	if (stricmp(sState, "Powerup") == 0)
	{
		return API_STATUS_NOT_ACCEPTED;
	}

	//check if the distributor is available
	distStat = lockDistributor("SaveBaseLogsReq");
    if (distStat != API_STATUS_SUCCESS)
    {
        (void) releaseDistributor(TRUE, "SaveBaseLogsReq");
    	return distStat;
    }

    gBaseNeedsReboot = reboot;
	dbgTrace(DBG_LVL_ERROR,"set gBaseNeedsReboot %d", (int)gBaseNeedsReboot);

    msgstate = fnGetUpdateNow(LOG_SERVICE, 0, 0);
    if (msgstate)
    	return API_STATUS_SUCCESS;
    else
    	return API_STATUS_NOT_PERFORMED;
}

/* -------------------------------------------------------------------------------------[on_SaveBaseLogsReq]-- */
void on_SaveBaseLogsReq(UINT32 handle, cJSON *root)
{
    cJSON              *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry    entry  = { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    api_status_t 		msgStat;
    BOOL 				reboot = FALSE;

    if (cJSON_HasObjectItem(root, "Reboot"))
    {
    	reboot = (stricmp("true", cJSON_GetObjectItem(root, "Reboot")->valuestring) == 0) ? TRUE : FALSE;
    }

    msgStat = saveBaseLogsReq(reboot);
   	return generateResponse(&entry, rspMsg, "SaveBaseLogs", api_status_to_string_tbl[msgStat], root);
}

extern unsigned long int_pbp_acct_num_as_packed_bcd(int pbp_acct_num);
/* --------------------------------------------------------------------------------------------[on_PerformAuditReq]-- */
void on_PerformAuditReq(UINT32 handle, cJSON *root)
{
    BOOL msgStat;
    api_status_t  tranStat, distStat;
    char *pTemp = "";
    int account = 0;
    int numDec = 0;
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    if (cJSON_HasObjectItem(root, "SeqNr"))
        cJSON_AddNumberToObject(rspMsg, "SeqNr", cJSON_GetObjectItem(root, "SeqNr")->valueint);

    //TODO - remove - this is a temporary way to get account num - must be packed bcd
    if (cJSON_HasObjectItem(root, "AcctNum"))
        CMOSSetupParams.lwPBPAcctNumber = int_pbp_acct_num_as_packed_bcd(cJSON_GetObjectItem(root, "AcctNum")->valueint);

    // check if connected to Internet first
    if (!connectedToInternet())
    {
        return generateResponse(&entry, rspMsg, "PerformAudit", api_status_to_string_tbl[API_STATUS_NO_INTERNET], root);
    }

    distStat = lockDistributor("PerformAuditReq");
    if (distStat != API_STATUS_SUCCESS)
    {
        return generateResponse(&entry, rspMsg, "PerformAudit", api_status_to_string_tbl[distStat], root);
    }

    // get transaction ID, if any, and store it
    if (cJSON_HasObjectItem(root, "TranID") == 1)
    {
        // There is a TranID in the req_msg. Copy that
        pTemp = cJSON_GetObjectItem(root, "TranID")->valuestring;
    }
    tranStat = setTransactionID(pTemp, "PerformAuditReq");
    if (tranStat != API_STATUS_SUCCESS)
    {// release locks and return
        (void) releaseDistributor(TRUE, "PerformAuditReq");
        return generateResponse(&entry, rspMsg, "PerformAudit", api_status_to_string_tbl[tranStat], root);
    }

    // Initiate the audit
    msgStat = fnGetUpdateNow(ACCTBAL_SERVICE, account, numDec);
    if (msgStat != SUCCESSFUL)
    {// release locks and return
        (void) releaseDistributor(TRUE, "PerformAuditReq");
        // exit transaction without getting transaction ID
        (void) getTransactionID(NULL, 0, "PerformAuditReq", true);
        return generateResponse(&entry, rspMsg, "PerformAudit", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
    }

    return generateResponse(&entry, rspMsg, "PerformAudit", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
}

void send_AccountBalanceRsp(api_status_t status, double dbtBal, double cdtBal)
{
    api_status_t  tranStat;
    cJSON *rsp = cJSON_CreateObject();
    char tempTransactionID[TRAN_ID_BUF_SIZE];

    cJSON_AddStringToObject(rsp, "MsgID", "AccountBalanceRsp");

    tranStat = getTransactionID(tempTransactionID, sizeof(tempTransactionID), "AccountBalanceRsp", false);
    if (tranStat != API_STATUS_SUCCESS)
    { // internal error so do not send anything to tablet
        dbgTrace(DBG_LVL_ERROR,"Error: Internal error %d getting TranID in %s", tranStat, "AccountBalanceRsp");
        return;
    }
    // add transaction ID to response
    cJSON_AddStringToObject(rsp, "TranID", tempTransactionID);

    cJSON_AddStringToObject(rsp, "Status", api_status_to_string_tbl[status]);
    if (status == API_STATUS_SUCCESS)
    {
        cJSON_AddNumberToObject(rsp, "DebitAccountBalance", dbtBal);
        cJSON_AddNumberToObject(rsp, "CreditAccountBalance", cdtBal);
        cJSON_AddNumberToObject(rsp, "NumDec", bVltDecimalPlaces);
    }

    Send_JSON(rsp,BROADCAST_MASK);
}

void send_DistTaskError(char serviceId, api_status_t errorEnum, STATUS errorCode)
{
    api_status_t  tranStat, distStat;
    char tempTransactionID[TRAN_ID_BUF_SIZE];
    char *msgID;
    cJSON *rsp = cJSON_CreateObject();

    if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  DistTaskError: [%d] %s\r\n", errorCode, api_status_to_string_tbl[errorEnum]);
    switch(serviceId)
    {
        case ACCTBAL_SERVICE:
            msgID = "AccountBalanceRsp";
            break;
        case REFILL_SERVICE:
            msgID = "RefillRsp";
            break;
        case REFUND_SERVICE:
            msgID = "WithdrawalRsp";
            break;
        case SWDOWNLOAD_SERVICE:
            msgID = "GetNetworkDLARsp";
            break;
        case DCAP_UPLOAD_SERVICE:
            msgID = "UploadTrxPkgRsp";
            break;
        case LOG_SERVICE:
            msgID = "SaveBaseLogsRsp";
            break;
        default:
            msgID = "DistTaskRsp";
            break;
    }
    cJSON_AddStringToObject(rsp, "MsgID", msgID);

    // For Tier3 services add transaction ID to response and end transaction
    switch(serviceId)
    {
        case ACCTBAL_SERVICE:
        case REFILL_SERVICE:
        case REFUND_SERVICE:
            tranStat = getTransactionID(tempTransactionID, sizeof(tempTransactionID), msgID, true); // end transaction
            if (tranStat != API_STATUS_SUCCESS)
            {// internal error so do not send anything to tablet
                dbgTrace(DBG_LVL_ERROR,"Error: Internal error %d getting TranID in %s", tranStat, msgID);
                (void) releaseDistributor(TRUE, msgID); //cleanup
                return;
            }
            // add transaction ID to response
            cJSON_AddStringToObject(rsp, "TranID", tempTransactionID);
            break;
        default:
            break;
    }

    distStat = releaseDistributor(TRUE, msgID);
    if (distStat != API_STATUS_SUCCESS)
    {// log error but continue
        dbgTrace(DBG_LVL_ERROR,"Error: Internal error %d releasing Distributor lock in %s", distStat, msgID);
    }

    cJSON_AddStringToObject(rsp, "Status", api_status_to_string_tbl[errorEnum]);
    cJSON_AddNumberToObject(rsp, "ErrorCode", errorCode);

    Send_JSON(rsp,BROADCAST_MASK);
}

void send_Dist_Task_Complete(char serviceId)
{
    api_status_t  tranStat, distStat;
    char tempTransactionID[TRAN_ID_BUF_SIZE];
    char *msgID;
    cJSON *rsp = cJSON_CreateObject();

    switch(serviceId)
    {
        case ACCTBAL_SERVICE:
            msgID = "AccountBalanceRsp";
            break;
        case REFILL_SERVICE:
            msgID = "RefillRsp";
            break;
        case REFUND_SERVICE:
            msgID = "WithdrawalRsp";
            break;
        case SWDOWNLOAD_SERVICE:
            msgID = "GetNetworkDLARsp";
            break;
        case DCAP_UPLOAD_SERVICE:
            msgID = "UploadTrxPkgRsp";
            break;
        case LOG_SERVICE:
            msgID = "SaveBaseLogsRsp";
            break;
        default:
            msgID = "DistTaskRsp";
            break;
    }
    cJSON_AddStringToObject(rsp, "MsgID", msgID);

    // For Tier3 services add transaction ID to response and end transaction
    switch(serviceId)
    {
        case ACCTBAL_SERVICE:
        case REFILL_SERVICE:
        case REFUND_SERVICE:
            tranStat = getTransactionID(tempTransactionID, sizeof(tempTransactionID), msgID, true); // end transaction
            if (tranStat != API_STATUS_SUCCESS)
            {// internal error so do not send anything to tablet
                dbgTrace(DBG_LVL_ERROR,"Error: Internal error %d getting TranID in %s", tranStat, msgID);
                (void) releaseDistributor(TRUE, msgID); //cleanup
                return;
            }
            // add transaction ID to response
            cJSON_AddStringToObject(rsp, "TranID", tempTransactionID);
            break;
        default:
            break;
    }

    distStat = releaseDistributor(TRUE, msgID);
    if (distStat != API_STATUS_SUCCESS)
    {// log error but continue
        dbgTrace(DBG_LVL_ERROR,"Error: Internal error %d releasing Distributor lock in %s", distStat, msgID);
    }

    cJSON_AddStringToObject(rsp, "Status", "Complete");
    Send_JSON(rsp,BROADCAST_MASK);

    dbgTrace(DBG_LVL_ERROR,"Get gBaseNeedsReboot %d", (int)gBaseNeedsReboot);
    //do tasks after sending websocket complete
    if ((serviceId == LOG_SERVICE) && gBaseNeedsReboot)
    {
		//give the web socket time to respond
		NU_Sleep(100);
		//reboot and install the files
        dbgTrace(DBG_LVL_ERROR,"Base Reboot starting . . .");
		RebootSystem();
    }
}

void send_RefillRsp()
{
    api_status_t  tranStat;
    char tempTransactionID[TRAN_ID_BUF_SIZE];
    cJSON *rsp = cJSON_CreateObject();

//  Save latest refill data to CMOS
    fnSaveRefillData();

    cJSON_AddStringToObject(rsp, "MsgID", "RefillRsp");

    tranStat = getTransactionID(tempTransactionID, sizeof(tempTransactionID), "RefillRsp", false);
    if (tranStat != API_STATUS_SUCCESS)
    { // internal error so do not send anything to tablet
        dbgTrace(DBG_LVL_ERROR,"Error: Internal error %d getting TranID in %s", tranStat, "RefillRsp");
        return;
    }
    // add transaction ID to response
    cJSON_AddStringToObject(rsp, "TranID", tempTransactionID);

    get_psd_regs_cJSON(rsp);

    Send_JSON(rsp,BROADCAST_MASK);
}

void send_PbpIO(char* text, int type, char start, char end)
{
    const int hdrLen = 8;
    const int buffLen = 256;
    int groupMask = BROADCAST_MASK;
    int length = 0;
    char hdr[8+1] = {0};
    char buff[256+1] = {0};
    static char lastBuff[256+1] = {0};
    static int result = 1;

    if((_pbpio_debug_channel == 1) || (_pbpio_broadcast == 1))
    {
        length = strlen(text);
        if(length > 0)
        {
            fnGetSubString(buff,text,start,end,buffLen,1,1);
            if((memcmp(buff,"<SignonRsp>",11) == 0) || (memcmp(buff,"<SignOnRsp>",11) == 0) || !result)
            {
                if(!result)
                {
                    //finish what the last function call started
                    memcpy(lastBuff+strlen(lastBuff), text, buffLen-strlen(lastBuff));
                    result = fnGetSubString(buff,lastBuff,start,end,buffLen,1,3);
                }
                else
                {
                    result = fnGetSubString((buff+strlen(buff)),text,start,end,buffLen,2,2);
                    //check if the message was not complete, broken into two
                    if(!result)
                    {
                        //save buffer to be completed on next call to this function
                        memcpy(lastBuff, buff, buffLen);
                        //null the buffer so there is nothing to output
                        buff[0] = 0;
                    }
                }
            }
            if(strlen(buff) > 0)
            {
                switch(type)
                {
                    case Tier3Tx:
                        memcpy(hdr, "Tier3Tx", hdrLen);
                        break;
                    case Tier3Rx:
                        memcpy(hdr, "Tier3Rx", hdrLen);
                        break;
                    case PostTx:
                        memcpy(hdr, "PostTx ", hdrLen);
                        break;
                    case PostRx:
                        memcpy(hdr, "PostRx ", hdrLen);
                        break;
                    case HTTPGet:
                        memcpy(hdr, "HttpGet", hdrLen);
                        break;
                    case Base:
                        memcpy(hdr, "Base", hdrLen);
                        break;
                }

                if((_pbpio_broadcast == 1) || (_pbpio_group == 1))
                {
                    cJSON *rsp = cJSON_CreateObject();
                    cJSON_AddStringToObject(rsp, "MsgID", "PBP IO");
                    cJSON_AddStringToObject(rsp, hdr, buff);

                    if(_pbpio_group == 1)
                        groupMask = PBP_IO_MASK;
                    Send_JSON(rsp, groupMask);
                }

                if(_pbpio_debug_channel == 1)
                {
                    dbgTrace(DBG_LVL_INFO, "MsgID=%s %s\n", hdr, buff);
                }
            }
        }
    }
}

/* --------------------------------------------------------------------------------------------[on_ShowPbpIoReq]-- */
void on_ShowPbpIoReq(UINT32 handle, cJSON *root)
{
    char ack[48] = {0};
    cJSON *rsp = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rsp}}};
    UINT32 groupMask = 0;
    UINT32 active = 0;

    cJSON_AddStringToObject(rsp, "MsgID", "ShowPbpIoRsp");

    if (RequiredFieldsArePresent(root, rsp, &required_fields_tbl_ShowPbpIoReq))
    {
        active = cJSON_GetObjectItem(root, "Active")->valueint;

        if(cJSON_HasObjectItem(root,"DebugChannel"))
            _pbpio_debug_channel = cJSON_GetObjectItem(root, "DebugChannel")->valueint;

        if(cJSON_HasObjectItem(root,"Broadcast"))
            _pbpio_broadcast = cJSON_GetObjectItem(root, "Broadcast")->valueint;

        if(cJSON_HasObjectItem(root,"PbpIoGroup"))
            _pbpio_group = cJSON_GetObjectItem(root, "PbpIoGroup")->valueint;

        if(active == 1)
        {
            //get the current group mask for this handle
            groupMask = wsox_get_handle_group(&entry);
            //add existing group mask bits
            entry.groupMask  =  (groupMask | PBP_IO_MASK);
            //save the updated group mask
            wsox_set_handle_group(&entry);
        }
        else if (active != 1)
        {
            //get the current group mask for this handle
            groupMask = wsox_get_handle_group(&entry);
            //remove existing group mask bits
            entry.groupMask  =  (groupMask ^ PBP_IO_MASK);
            //save the updated group mask
            wsox_set_handle_group(&entry);
        }

        strcpy(ack,api_status_to_string_tbl[API_STATUS_ACCEPTED]);
        strcat(ack,((active == 1) ? " ACT:ON" : " ACT:OFF"));
        strcat(ack,((_pbpio_debug_channel == 1) ? " DBG:ON" : " DBG:OFF"));
        strcat(ack,((_pbpio_broadcast == 1) ? " BCST:ON" : " BCST:OFF"));
        strcat(ack,((_pbpio_group == 1) ? " GRP:ON" : " GRP:OFF"));

        return generateResponse(&entry, rsp, "ShowPbpIo", ack, root);
    }
    else
    {
        return generateResponse(&entry, rsp, "ShowPbpIo", api_status_to_string_tbl[API_STATUS_INVALID_PARAMETERS], root);
    }

}



/* --------------------------------------------------------------------------------------------[on_PerformRefillReq]-- */
void on_PerformRefillReq(UINT32 handle, cJSON *root)
{
    BOOL msgStat;
    api_status_t  tranStat, distStat;
    char *pTemp = "";
    cJSON *rsp = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rsp}}};
    int amount = 0;
    int numDec = 0;

    if (!RequiredFieldsArePresent(root, rsp, &required_fields_tbl_PerformRefillReq))
    {
        return generateResponse(&entry, rsp, "PerformRefill", api_status_to_string_tbl[API_STATUS_INVALID_PARAMETERS], root);
    }

    amount = cJSON_GetObjectItem(root, "RefillAmount")->valueint;
    numDec = cJSON_GetObjectItem(root, "NumDec")->valueint;

//  Save Refill amount to CMOS
    fnStoreRefillAmount(&amount);

    get_psd_regs_cJSON(rsp);

    // check if connected to Internet first
    if (!connectedToInternet())
    {
        return generateResponse(&entry, rsp, "PerformRefill", api_status_to_string_tbl[API_STATUS_NO_INTERNET], root);
    }

    distStat = lockDistributor("PerformRefill");
    if (distStat != API_STATUS_SUCCESS)
    {
        return generateResponse(&entry, rsp, "PerformRefill", api_status_to_string_tbl[distStat], root);
    }

    // get transaction ID, if any, and store it
    if (cJSON_HasObjectItem(root, "TranID") == 1)
    {
        // There is a TranID in the req_msg. Copy that
        pTemp = cJSON_GetObjectItem(root, "TranID")->valuestring;
    }
    tranStat = setTransactionID(pTemp, "PerformRefill");
    if (tranStat != API_STATUS_SUCCESS)
    {// release locks and return
        (void) releaseDistributor(TRUE, "PerformRefill");
        return generateResponse(&entry, rsp, "PerformRefill", api_status_to_string_tbl[tranStat], root);
    }

    msgStat = fnGetUpdateNow(REFILL_SERVICE,amount,numDec);
    if (msgStat != SUCCESSFUL)
    {// release locks and return
        (void) releaseDistributor(TRUE, "PerformRefill");
        // exit transaction without getting transaction ID
        (void) getTransactionID(NULL, 0, "PerformRefill", true);
        return generateResponse(&entry, rsp, "PerformRefill", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
    }

    return generateResponse(&entry, rsp, "PerformRefill", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
}

/* --------------------------------------------------------------------------------------------[on_PerformWithdrawalReq]-- */
void on_PerformWithdrawalReq(UINT32 handle, cJSON *root)
{
    BOOL msgStat;
    api_status_t  tranStat, distStat;
    char *pTemp = "";
    cJSON *rsp = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rsp}}};
    int account = 0;
    int numDec = 0;
	STATUS status = SUCCESS;
	int fileCount = 0;
	unsigned long uTotalPC = 0;
	
	// store values to be used in refund report prior to performing withdraw
	memcpy( CMOS2RefundData.rDR, mStd_DescendingReg, sizeof(mStd_DescendingReg) );
	memcpy( CMOS2RefundData.rAR, m5IPSD_ascReg, sizeof(m5IPSD_ascReg) );
	memcpy( CMOS2RefundData.cSum, mStd_ControlSum, sizeof(mStd_ControlSum) );
	memcpy(&uTotalPC, pIPSD_pieceCount, sizeof(uTotalPC));
	CMOS2RefundData.pCnt = uTotalPC;
    sprintf(CMOS2RefundData.pAcct, "%08.8x", EndianSwap32(CMOSSetupParams.lwPBPAcctNumber));

    cJSON_AddStringToObject(rsp, "MsgID", "PerformWithdrawalRsp");

    if (!RequiredFieldsArePresent(root, rsp, &required_fields_tbl_PerformWithdrawalReq))
    {
        return generateResponse(&entry, rsp, "PerformWithdrawal", api_status_to_string_tbl[API_STATUS_INVALID_PARAMETERS], root);
    }

    if(cJSON_HasObjectItem(root,"AcctNum"))
        account = cJSON_GetObjectItem(root, "AcctNum")->valueint;
	 
    get_psd_regs_cJSON(rsp);

    // check if connected to Internet first
    if (!connectedToInternet())
    {
        return generateResponse(&entry, rsp, "PerformWithdrawal", api_status_to_string_tbl[API_STATUS_NO_INTERNET], root);
    }

    //check if Transaction records are pending to upload
    status = trmGetTrxFileCount(&fileCount);
    if( ((CMOSTrmUploadDueReqd & TRM_UPLOAD_AVAILABLE) == TRM_UPLOAD_AVAILABLE) || (fileCount != 0))
    {
    	return generateResponse(&entry, rsp, "PerformWithdrawal", api_status_to_string_tbl[API_STATUS_TRM_UPLOAD_REQ], root);

    }

    distStat = lockDistributor("PerformWithdrawal");
    if (distStat != API_STATUS_SUCCESS)
    {
        return generateResponse(&entry, rsp, "PerformWithdrawal", api_status_to_string_tbl[distStat], root);
    }

    // get transaction ID, if any, and store it
    if (cJSON_HasObjectItem(root, "TranID") == 1)
    {
        // There is a TranID in the req_msg. Copy that
        pTemp = cJSON_GetObjectItem(root, "TranID")->valuestring;
    }
    tranStat = setTransactionID(pTemp, "PerformWithdrawal");
    if (tranStat != API_STATUS_SUCCESS)
    {// release locks and return
        (void) releaseDistributor(TRUE, "PerformWithdrawal");
        return generateResponse(&entry, rsp, "PerformWithdrawal", api_status_to_string_tbl[tranStat], root);
    }

    msgStat = fnGetUpdateNow(REFUND_SERVICE,account,numDec);
    if (msgStat != SUCCESSFUL)
    {// release locks and return
        (void) releaseDistributor(TRUE, "PerformWithdrawal");
        // exit transaction without getting transaction ID
        (void) getTransactionID(NULL, 0, "PerformWithdrawal", true);
        return generateResponse(&entry, rsp, "PerformWithdrawal", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
    }

    return generateResponse(&entry, rsp, "PerformWithdrawal", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
}

/* ------------------------------------------------------------------------------------------------[on_GetPSDStateReq]-- */
void on_GetPSDStateReq(UINT32 handle, cJSON *root)
{
    UINT32  ulPsdState = 0;

    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    if( fnCheckPSDModeForState( rspMsg ) == TRUE )
    {
        if( fnValidData( BOBAMAT0, GET_PSD_STATUS, &ulPsdState))
        {
            cJSON_AddNumberToObject(rspMsg, "PsdState", EndianAwareGet32(&ulPsdState));
            generateResponse(&entry, rspMsg, "GetPSDState", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
        }
        else
        {
            generateResponse(&entry, rspMsg, "GetPSDState", api_status_to_string_tbl[API_STATUS_NOT_ACCEPTED], root);
        }
    }
    else 
    {
        generateResponse(&entry, rspMsg, "GetPSDState", api_status_to_string_tbl[API_STATUS_NOT_ACCEPTED], root);
    }

}


static required_fields_tbl_t required_fields_tbl_GetPSDLogsReq =
    {
        "GetPSDLogsReq", {
                            "LogType",
                            NULL }
    };


static const char *valid_logTypes [] =
                { "Indicia", "Refill", "Audit" };


cJSON *CreateRefillLogItem(CHAR *pDate, double dAmt, long count, double dDR, double dAR)
{
    cJSON *item = cJSON_CreateObject();
    cJSON_AddStringToObject(item,         "Date", pDate);
    cJSON_AddNumberToObject(item,    "RefillAmt", dAmt);
    cJSON_AddNumberToObject(item,     "RefillCnt", count);
    cJSON_AddNumberToObject(item,      "DescReg", dDR);
    cJSON_AddNumberToObject(item,       "AscReg", dAR);
    cJSON_AddNumberToObject(item,       "NumDec", bVltDecimalPlaces);

    return item;
}


void get_refillLog_cJSON(cJSON *root)
{
    cJSON *logs;
    double dDR = 0;
    double dAR = 0;
    double dAmt = 0;

    MONEY descReg;
    MONEY ascReg;
    MONEY amt;
    long  count;
    DATETIME DateTime;
    long     IPSDtime;
    char sLocalDateTime[50]= {0};
    uchar i;
    tIPSD_LOG_LMNT_REFILL* pRefillLog = NULL_PTR;   

    cJSON_AddItemToObject(root, "Logs", logs=cJSON_CreateArray());

    for( i = 0; i < IPSD_LOG_MAX_REFILL; i++)
    {
        pRefillLog = (tIPSD_LOG_LMNT_REFILL*) &pIPSD_RefillLog[i * sizeof(tIPSD_LOG_LMNT_REFILL)];
    
        // Convert Refill Amount to a 5-byte value, and store.
        memset( amt, 0, SPARK_MONEY_SIZE );
        memcpy( &amt[ SPARK_MONEY_SIZE - IPSD_SZ_POSTAGE ], pRefillLog->m4RefillAmt,  IPSD_SZ_POSTAGE );
        dAmt = fnBinFive2Double(amt);
    
        EndianAwareCopy(&count, pRefillLog->pRefillCount,  IPSD_SZ_REFILL_CNT );

        // Convert Ascending Register to a 5-byte value, and store.
        memset( ascReg, 0, SPARK_MONEY_SIZE );
        memcpy( ascReg, pRefillLog->m5AR,  IPSD_SZ_ASC_REG );
        dAR = fnBinFive2Double(ascReg);

        // Convert Descending Register to a 5-byte value, and store.
        memset( descReg, 0, SPARK_MONEY_SIZE );
        memcpy( &descReg[ SPARK_MONEY_SIZE - IPSD_SZ_DESC_REG ], pRefillLog->m4DR,  IPSD_SZ_DESC_REG );
        dDR = fnBinFive2Double(descReg);
    
        EndianAwareCopy( &IPSDtime, pRefillLog->pRefillDate, sizeof(pRefillLog->pRefillDate) );
        fnIPSDTimeToSys( &DateTime, IPSDtime );
        (void)sprintf(sLocalDateTime, "%02d%02d-%02d-%02dT%02d:%02d:%02d",DateTime.bCentury, DateTime.bYear, DateTime.bMonth, DateTime.bDay, DateTime.bHour, DateTime.bMinutes, DateTime.bSeconds);

        cJSON_AddItemToArray(logs, CreateRefillLogItem(sLocalDateTime, dAmt, count, dDR, dAR));
    }
}


cJSON *CreateAuditLogItem(CHAR *pDate, long status, double dDR, double dAR)
{
    cJSON *item = cJSON_CreateObject();
    cJSON_AddStringToObject(item,         "Date", pDate);
    cJSON_AddNumberToObject(item,      "DescReg", dDR);
    cJSON_AddNumberToObject(item,       "AscReg", dAR);
    cJSON_AddNumberToObject(item,  "AuditStatus", status);
    cJSON_AddNumberToObject(item,       "NumDec", bVltDecimalPlaces);

    return item;
}


void get_auditLog_cJSON(cJSON *root)
{
    cJSON *logs;
    double dDR = 0;
    double dAR = 0;
    MONEY descReg;
    MONEY ascReg;
    long  status;
    DATETIME DateTime;
    long     IPSDtime;
    char sLocalDateTime[50]= {0};
    uchar i;
    tIPSD_LOG_LMNT_AUDIT* pAuditLog = NULL_PTR;   

    cJSON_AddItemToObject(root, "Logs", logs=cJSON_CreateArray());

    for( i = 0; i < IPSD_LOG_MAX_AUDIT; i++)
    {
        pAuditLog = (tIPSD_LOG_LMNT_AUDIT*) &pIPSD_AuditLog[i * sizeof(tIPSD_LOG_LMNT_AUDIT)];
    
    
        EndianAwareCopy(&status, pAuditLog->pAuditStatus,  IPSD_SZ_TIME );

        // Convert Ascending Register to a 5-byte value, and store.
        memset( ascReg, 0, SPARK_MONEY_SIZE );
        memcpy( ascReg, pAuditLog->m5AR,  IPSD_SZ_ASC_REG );
        dAR = fnBinFive2Double(ascReg);

        // Convert Descending Register to a 5-byte value, and store.
        memset( descReg, 0, SPARK_MONEY_SIZE );
        memcpy( &descReg[ SPARK_MONEY_SIZE - IPSD_SZ_DESC_REG ], pAuditLog->m4DR,  IPSD_SZ_DESC_REG );
        dDR = fnBinFive2Double(descReg);
    
        EndianAwareCopy( &IPSDtime, pAuditLog->pAuditDate, sizeof(pAuditLog->pAuditDate) );
        fnIPSDTimeToSys( &DateTime, IPSDtime );
        (void)sprintf(sLocalDateTime, "%02d%02d-%02d-%02dT%02d:%02d:%02d",DateTime.bCentury, DateTime.bYear, DateTime.bMonth, DateTime.bDay, DateTime.bHour, DateTime.bMinutes, DateTime.bSeconds);

        cJSON_AddItemToArray(logs, CreateAuditLogItem(sLocalDateTime, status, dDR, dAR));
    }
}


cJSON *CreateIndiciaLogItem(CHAR *pDate, double dAmt, long count, double dDR, double dAR, CHAR *pTimeStamp)
{
    cJSON *item = cJSON_CreateObject();
    cJSON_AddStringToObject(item,     "MailDate", pDate);
    cJSON_AddNumberToObject(item,   "PostageAmt", dAmt);
    cJSON_AddNumberToObject(item,      "DescReg", dDR);
    cJSON_AddNumberToObject(item,       "AscReg", dAR);
    cJSON_AddStringToObject(item,   "CreateDate", pTimeStamp);
    cJSON_AddNumberToObject(item,     "PieceCnt", count);
    cJSON_AddNumberToObject(item,       "NumDec", bVltDecimalPlaces);

    return item;
}


void get_indiciaLog_cJSON(cJSON *root)
{
    cJSON *logs;
    double dDR = 0;
    double dAR = 0;
    double dAmt = 0;

    MONEY descReg;
    MONEY ascReg;
    MONEY amt;
    long  count;
    DATETIME DateTime;
    long     IPSDtime;
    char sLocalTimeStamp[50]= {0};
    char sMailDateTime[50]= {0};
    uchar i;
    tIPSD_LOG_LMNT_INDCREATE* pIndiciaLog = NULL_PTR;   

    cJSON_AddItemToObject(root, "Logs", logs=cJSON_CreateArray());

    for( i = 0; i < IPSD_LOG_MAX_INDCREATE; i++)
    {
        pIndiciaLog = (tIPSD_LOG_LMNT_INDCREATE*) &pIPSD_IndCreateLog[i * sizeof(tIPSD_LOG_LMNT_INDCREATE)];
    
        // Convert Postage Amount to a 5-byte value, and store.
        memset( amt, 0, SPARK_MONEY_SIZE );
        memcpy( &amt[ SPARK_MONEY_SIZE - IPSD_SZ_POSTAGE ], pIndiciaLog->m4PostageValue,  IPSD_SZ_POSTAGE );
        dAmt = fnBinFive2Double(amt);
    
        EndianAwareCopy(&count, pIndiciaLog->pPieceCount,  IPSD_SZ_PIECE_CNT );

        // Convert Ascending Register to a 5-byte value, and store.
        memset( ascReg, 0, SPARK_MONEY_SIZE );
        memcpy( ascReg, pIndiciaLog->m5AR,  IPSD_SZ_ASC_REG );
        dAR = fnBinFive2Double(ascReg);

        // Convert Descending Register to a 5-byte value, and store.
        memset( descReg, 0, SPARK_MONEY_SIZE );
        memcpy( &descReg[ SPARK_MONEY_SIZE - IPSD_SZ_DESC_REG ], pIndiciaLog->m4DR,  IPSD_SZ_DESC_REG );
        dDR = fnBinFive2Double(descReg);
    
        EndianAwareCopy( &IPSDtime, pIndiciaLog->pMailingDate, sizeof(pIndiciaLog->pMailingDate) );
        fnIPSDTimeToSys( &DateTime, IPSDtime );
        (void)sprintf(sMailDateTime, "%02d%02d-%02d-%02dT%02d:%02d:%02d",DateTime.bCentury, DateTime.bYear, DateTime.bMonth, DateTime.bDay, DateTime.bHour, DateTime.bMinutes, DateTime.bSeconds);

        EndianAwareCopy( &IPSDtime, pIndiciaLog->pTimeStamp, sizeof(pIndiciaLog->pTimeStamp) );
        fnIPSDTimeToSys( &DateTime, IPSDtime );
        (void)sprintf(sLocalTimeStamp, "%02d%02d-%02d-%02dT%02d:%02d:%02d",DateTime.bCentury, DateTime.bYear, DateTime.bMonth, DateTime.bDay, DateTime.bHour, DateTime.bMinutes, DateTime.bSeconds);

        cJSON_AddItemToArray(logs, CreateIndiciaLogItem(sMailDateTime, dAmt, count, dDR, dAR, sLocalTimeStamp));
    }
}


/* ------------------------------------------------------------------------------------------------[on_GetRefillReq]-- */
void on_GetPSDLogReq(UINT32 handle, cJSON *root)
{
    int  i = 0;
    char *pTemp;
    UINT16 logType;
    bool valid_mode;
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetPSDLogRsp");
    if (cJSON_HasObjectItem(root, "SeqNr"))
        cJSON_AddNumberToObject(rspMsg, "SeqNr", cJSON_GetObjectItem(root, "SeqNr")->valueint);
    if (!RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_GetPSDLogsReq))
    {
        //Add missing required item to invalid list
        cJSON_AddStringToObject(rspMsg, required_fields_tbl_GetPSDLogsReq.fieldnames[0], "");
    }
    else
    {
            // validate fields - PrintMode
        pTemp = cJSON_GetObjectItem(root, "LogType")->valuestring;
        for(i=0; i<NELEMENTS(valid_logTypes) ; i++)
        {
            if (0 == NCL_Stricmp(valid_logTypes[i], pTemp))
            {
                valid_mode = true;
                break;
            }
        }
        if (valid_mode == true)
        {
            logType = (UINT16) i;
            cJSON_AddStringToObject(rspMsg, "LogType", pTemp);
        }
        else
        {
            cJSON_AddStringToObject(rspMsg, "Status", "NotAccepted");
        }

        if(valid_mode == true)
        {
            if( fnGetIButtonData(logType) != true)
                cJSON_AddStringToObject(rspMsg, "Status", "Failed");
            else if(logType == 0)
                get_indiciaLog_cJSON(rspMsg); 
            else if (logType == 1)
                get_refillLog_cJSON(rspMsg);
            else //logType must be 2
                get_auditLog_cJSON(rspMsg);
        }
    }

    addEntryToTxQueue(&entry, root, "  GetPSDLogReq: Added GetPSDLogRsp to tx queue. status=%d" CRLF);
}

/* --------------------------------------------------------------------------------------------[send_NetworkDLAReq]-- */
void send_NetworkDLAReq(char *url, char *pcn, char *serialNum, char *xmlDocBuffer, int xmlDocBufferSize)
{
    cJSON *rsp = cJSON_CreateObject();
    cJSON_AddStringToObject(rsp, "MsgID", "NetworkDLAReq");
    cJSON_AddStringToObject(rsp, "DLAUrl", url);
    cJSON_AddStringToObject(rsp, "PCN", pcn);
    cJSON_AddStringToObject(rsp, "serialNumber", serialNum);
    cJSON_AddStringToObject(rsp, "DLAReq", xmlDocBuffer);

    Send_JSON(rsp,BROADCAST_MASK);
}

/* --------------------------------------------------------------------------------------------[on_GetNetworkDLAReq]-- */
void on_GetNetworkDLAReq(UINT32 handle, cJSON *root)
{
    BOOL            msgStat;
    int             numDec = 0;
    short           systemErrorLog = 0;
    char            sState[50];
    api_status_t    distStat;
    cJSON *rsp = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rsp}}};

    if (cJSON_HasObjectItem(root, "SeqNr"))
        cJSON_AddNumberToObject(rsp, "SeqNr", cJSON_GetObjectItem(root, "SeqNr")->valueint);

    //optional flag to include the system error log
    if (cJSON_HasObjectItem(root, "SystemErrorLog"))
        systemErrorLog = (strcmp("true", cJSON_GetObjectItem(root, "SystemErrorLog")->valuestring) == 0) ? 1 : 0;

    GetUICStates(sState);
    cJSON_AddStringToObject(rsp, "BaseState", sState);

    if(strcmp(sState,"Running") == 0)
    {
        return generateResponse(&entry, rsp, "GetNetworkDLA", api_status_to_string_tbl[API_STATUS_NOT_ACCEPTED], root);
    }

    // check if connected to Internet first
    if (!connectedToInternet())
    {
        return generateResponse(&entry, rsp, "GetNetworkDLA", api_status_to_string_tbl[API_STATUS_NO_INTERNET], root);
    }

    distStat = lockDistributor("GetNetworkDLA");
    if (distStat != API_STATUS_SUCCESS)
    {
        return generateResponse(&entry, rsp, "GetNetworkDLA", api_status_to_string_tbl[distStat], root);
    }

    numDec |= (systemErrorLog << 16);
    msgStat = fnGetUpdateNow(SWDOWNLOAD_SERVICE,0,numDec);
    if (msgStat != SUCCESSFUL)
    {// release locks and return
        (void) releaseDistributor(TRUE, "GetNetworkDLA");
        return generateResponse(&entry, rsp, "GetNetworkDLA", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
    }

    return generateResponse(&entry, rsp, "GetNetworkDLA", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
}




