/************************************************************************
*	PROJECT:		Mega
*	MODULE NAME:	$Workfile:   remotelog.c  $
*	
*	DESCRIPTION:	This file contains all routines related to remote 
*					logging. 
*					
*					
*
* ----------------------------------------------------------------------
*               Copyright (c) 2003 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*
* REVISION HISTORY:
 
 02-Jun-16 sa002pe on FPHX 02.12 shelton branch
 	Commented out more stuff now that the remote logging capabilities are being removed.

 11-Aug-14 sa002pe on FPHX 02.12 shelton branch
	Commented out most of the remoteLog function now that the GANDALF server no longer exists.

 18-Feb-14 Kevin Brown on FPHX 02.12 shelton branch
    Added "mySocket.fUseSSL == FALSE" check to the if statement in remoteLog().
    Added GANDALF_CONNECTION parameter to fnConnectSocket() call in remoteLog().

 02-Oct-13 sa002pe on FPHX 02.12 shelton branch
	Merging in Kevin's network proxy changes from his 02.10 dev branch:
----*
	 26-Jun-13 Kevin Brown on FPHX 02.10 shelton branch
	    Changed the parameters of fnGetProxyAuthStringForHeader() in remoteLog().
----*

 28-Aug-13 sa002pe on FPHX 02.12 shelton branch
 	Fixed a syslog message.

 04-Jun-13 sa002pe on FPHX 02.10 shelton branch
	More stuff for having a network proxy:
	1. Moved all the externs together.
	2. Moved uploadLogHdr up with the other structures and made it "const" so it won't be put on the heap.
	3. Changed remoteLog to use the common subroutines in sslinterface.c in order to save code space.

 18-Mar-13 sa002pe on FPHX 02.10 shelton branch
	Preliminary stuff for having a LAN proxy:
	1. Changed uploadLogHdr to removed the "HTTP/1.0\r\n\" part since the proxy info will need to go
		between the HTTP version data and the rest of the header in the message being sent out.
	2. Changed remoteLog to check if a proxy is being used and to act appropriately.

 18-Feb-13 sa002pe on FPHX 02.10 shelton branch
 	Changed the "DNS unable to resolve..." exception log message to:
	1. Use "=" instead of "->" because ">" is used for XML tags, so we don't want this to be
		in the exception log.
	2. Make sure the host name is properly null-terminated.
	3. Actually point to the host name when generating the message.
*
* 05-Apr-07 sa002pe on FPHX 01.04 shelton branch
*	Added definition of THISFILE.
*	Changed all calls for exception logging to use THISFILE instead of __FILE__.
*
*      Rev ??? ge002mo 
*              Changed which variable was being accessed to reference the system memory heap
*	\main\jnus11.00_i1_shelton\1
*
*       Ported changes from the K700 code that supported local logging Added:
*           fnSetLocalLoggingState()
*          fnGetLocalLoggingState()
*
*       Added resumeNormalDistributor() that resets the CMOSNetworkConfig.distributorURL 
*       back to the QA Server when ever this struct element is pointing to the Gandalf 
*       server. This helps to automate the turning on of  Remote Logging feature. 
*
*	\main\jnus11.00_i1_shelton\2
*
*       Created preserveCurrentDistributor() to preserve the current distributor 
*       URL into a temporarily allocated buffer. 
*
*       Modified resumeNormalDistributor() to restore the distributor URL to its
*       previous value before it was momentarily changed to the Gandalf remote logging 
*       server URL.
*       
************************************************************************/
  
#include "commontypes.h"
#include "networking/externs.h"
#include "..\include\remotelog.h"



extern unsigned char remoteLoggingEnabled;



#if defined(JANUS) || defined(K700)
unsigned char localLoggingEnabled = TRUE;
#endif


BOOL remoteLoggingIsOn(void)
{
#if defined(JANUS) || defined(K700)
// Modified for Janus/K700 so that we can put an entry in the System log 
// even if remote logging is not enabled.
	return (remoteLoggingEnabled || localLoggingEnabled);
#else
    return remoteLoggingEnabled;
#endif
}


void fnSetLocalLoggingState(BOOL newstate)
{
    localLoggingEnabled = (unsigned char) newstate;
}

BOOL fnGetLocalLoggingState(void)
{
	return((BOOL) localLoggingEnabled);
}


STATUS remoteLog(char *string, char *buff)
{  
// ----------------------- following isn't needed any more ----------------------------------------
/*  
	SOCKET_DESCRIPTOR	mySocket;
	
	//I Need an address and host entry structures...
	//I'll create a static one so as to conserve stack space
//	static ADDR_STRUCT sockAddress;
//	static NU_HOSTENT hostEnt;
//	char *pHostAddrList=NULL;

	//Define pointers to dynamically allocated buffers
	#define REC_BUFF_SIZE 4096
	char *recvBuffer = 0;

	char *transmitBuffer = 0;

	#define ENCODED_ID_STRING_SIZE 64
	unsigned char *urlEncodingBuffer= 0;

	//The socket descriptor
//	int socketDescriptor;

	//For the NU_Recv() return
	INT32 recByteCount;

	//For total count
	INT32 totalBytesReceived = 0;

	//For indexing into transmit buffer while building a message
	int totalCount = 0;

	//The url encoded data count
	int urlEncodedCount = 0;

	//for "for" loops
	int loopIndex;

	unsigned int msgSize = 0;  
	unsigned int transmitBufferSize = 0;  
	unsigned int encodingBufferSize = 0;  

	//Container for cracked Distributor URL
//	static CRACKED_URL crackedRemoteLoggingUrl;  
    
//	unsigned char bHostIpAddr[4];

	char proxyAuth[350]={0};    
//	BOOL bUseProxy = 0;
*/
// ----------------------- endif of what isn't needed any more ----------------------------------------


#if defined(JANUS) || defined(K700)
// For Janus we want an entry in the System Log with this information.
	if (localLoggingEnabled)
	{
		fnDumpStringToSystemLog(string);
		fnDumpStringToSystemLog(buff);
	}
	
//	if (!remoteLoggingEnabled)
        return NU_SUCCESS;
#else
//    if(!remoteLoggingIsOn())
        return NU_SUCCESS;
#endif
    
}
