/************************************************************************
 *  PROJECT:        CSD 2/3
 *  MODULE NAME:    platform.c
 *  
 *  DESCRIPTION:    Communication Task for Platform Task
 *  				- it receives messages from the serial platform and sends
 *  				them to the platform task
 *
 * ----------------------------------------------------------------------
 *               Copyright (c) 2016 Pitney Bowes Inc.
 * ----------------------------------------------------------------------
 *  REVISION HISTORY:
  *************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include "nucleus.h"
#include "commontypes.h"
#include "pbplatform.h"
#include "pbos.h"
#include "wsox_services.h"
#include "platuart.h"

static BOOL fnPlatSendMessage(const char *pMsg);

/* **********************************************************************
// PURPOSE:     Handler for messages from serial scale
//
// INPUTS:
// **********************************************************************/
void fnPlatformTask(unsigned long argc, void *argv)
{

	 BOOL fRetval = SUCCESSFUL;
	INTERTASK interTaskMsg;

	fnDumpStringToSystemLog("fnPlatformTask started");
	dbgTrace(DBG_LVL_INFO, "Starting platform task...\n") ;

	  while (1)
	  {
	      if( OSReceiveIntertask( PLAT, &interTaskMsg, OS_SUSPEND) == SUCCESSFUL )
	      {

	    	  switch (interTaskMsg.bSource)
	    	  {

				  case WSRXSRVTASK:
				  {
					  int reqLen = interTaskMsg.IntertaskUnion.PointerData.lwLength;
					  char reqBuff[reqLen + 1];
					  memset(reqBuff, 0x0, sizeof(reqBuff));
					  memcpy(reqBuff, (char *)interTaskMsg.IntertaskUnion.PointerData.pData, reqLen);
					  fRetval = fnPlatSendMessage(reqBuff);
					  //strncpy(reqBuff, (char *)interTaskMsg.IntertaskUnion.PointerData.pData, reqLen);
//			          fnPlatProcessMsg( pActivePlatform, &interTaskMsg );
//					  Send_WPlatToTabMsgRsp(reqBuff);
				  }
				  break;
				  case CPLAT:
				  {
					  int reqLen1 = interTaskMsg.IntertaskUnion.CommData.bLength;
					  char reqBuff1[reqLen1 + 1];
					  memset(reqBuff1, 0x0, sizeof(reqBuff1));
					  memcpy(reqBuff1, (char *)interTaskMsg.IntertaskUnion.CommData.bByteData, reqLen1);

					  Send_WPlatToTabMsgRsp(reqBuff1);
				  }
					  break;

				  default:
					  break;
	    	  }


	    	  if(interTaskMsg.bMsgType == PART_PTR_DATA)
	          {
	            OSReleaseMemory( interTaskMsg.IntertaskUnion.PointerData.pData);
	          }
	      }


	  }

}



/* **********************************************************************
// FUNCTION NAME: fnPlatSendMessage
// DESCRIPTION: Sends a message to the platform.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:    pMsg- The message to send.  Must be NULL terminated.
//
// OUTPUT:  TRUE if message sent successfully, FALSE otherwise
// *************************************************************************/
static BOOL fnPlatSendMessage( const char *pMsg)
{
  unsigned long    lwSize;
  BOOL            fRetval = SUCCESSFUL;


  lwSize = strlen(pMsg);
  if (lwSize < 0)
    fRetval = FAILURE;
  else
    {
	  fRetval = fnTransmitPlatformLinkLayerComm(pMsg);
	}

  return (fRetval);
}


