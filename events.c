/*
*********************************************************************
*********************************************************************
CLASSIFICATION:                        PITNEY BOWES RESTRICTED
IDENTITY:                              
TITLE:                                 events.c
COPYRIGHT PITNEY BOWES LIMITED 2005
REQUIREMENT:                           
HISTORY:
         DATE      ISSUE      AUTHOR         PR     SYNOPSIS

      
      INTERFACE:

        CLASSES:
                

    DESCRIPTION: 
        generic event related functions
        

------------------------------------------------------------------------
*/ 

#ifdef MEMORYTEST

#define __events_c__ /* used in events.h */

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include "commontypes.h"
#include "events.h"
#include "lists.h"
//#include "timeout.h"
#include "logging.h"
#include "statemachine.h"
#include "compile_time_asserts.h"
#include "platform.h"


static void compile_time_checks(void);

//extern uint32_t GetNexusCycle(void);
//extern void restart_cycle_count(void);
//extern void new_cycle(void);

list_t ev_queue __attribute__ ((section(".wd_preserved")));
list_t ev_hndlr_list;
list_t ev_timer_list;

int32_t fn_timer_priority(const void *a, const void *b);

void ev_get_stats(ev_stats_t *p_stats)
{
   assert(p_stats != NULL);
   p_stats->events_generated = ev_queue.stats.list_adds_in_order +
                               ev_queue.stats.list_appends + 
                               ev_queue.stats.list_prepends;
   p_stats->handlers_registered = ev_hndlr_list.active.num_entries;
   p_stats->timers_active = ev_timer_list.active.num_entries;
   p_stats->timers_fired = 0; /* don't know that one yet */
}

int32_t fn_timer_priority(const void *a, const void *b)
{
    /* used to keep the timer source list ordered by expiry time */
    return ((ev_timer_event_source_t *)a)->trigger_time - ((ev_timer_event_source_t *)b)->trigger_time;
}

static int fn_assign_timer_id(void *p_context, void *p_data)
{
    if (p_context && p_data)
    {
        timer_id_t *p_timer_id = (timer_id_t *)p_context;
        ((ev_timer_event_source_t*)p_data)->timer_id = *p_timer_id;
        (*p_timer_id)++;
    }
    return 1;   /* inform caller we can process another item */
}

#define EVENT_HANDLER_HASH_SIZE (MIN(256, EV_NUM_OF_EVENTS))

list_entry_t *event_handler_hash[EVENT_HANDLER_HASH_SIZE];

#define CFG_EV_QUEUE_SIZE  64*1024L
#define CFG_EV_HNDLR_SIZE   4*1024L
#define CFG_EV_TIMER_SIZE   1*1024L

char ev_queue_mem[CFG_EV_QUEUE_SIZE] __attribute__ ((section(".wd_preserved")));
char ev_hndlr_list_mem[CFG_EV_HNDLR_SIZE];
char ev_timer_list_mem[CFG_EV_TIMER_SIZE];

bool b_events_not_initialised = TRUE;


void ev_initialise(void)
{
    timer_id_t timer_id=1;

    compile_time_checks();
    memset(&ev_queue_mem, 0, sizeof(ev_queue_mem));
    list_create(&ev_queue,      sizeof(ev_t),                    ev_queue_mem,      sizeof(ev_queue_mem),      NULL);

    list_create(&ev_hndlr_list, sizeof(ev_hndlr_tbl_entry_t),    ev_hndlr_list_mem, sizeof(ev_hndlr_list_mem), NULL);
    list_create(&ev_timer_list, sizeof(ev_timer_event_source_t), ev_timer_list_mem, sizeof(ev_timer_list_mem), fn_timer_priority);

    list_process_free(&ev_timer_list, fn_assign_timer_id, &timer_id);
    memset(event_handler_hash, 0, sizeof(event_handler_hash));
    b_events_not_initialised = FALSE;
}

void ev_register_event_handler(event_id_t ev_id, ev_hndlr_fn_t ev_hndlr_fn)
{
    list_entry_t *p_entry;
    ev_hndlr_tbl_entry_t *p_ev_entry;
    uint16_t hash_index;

    assert(ev_hndlr_fn); /* we must have a non null function pointer */
    p_entry = list_get_new_entry(&ev_hndlr_list);
    assert(p_entry);
    if (p_entry)
    {
        p_ev_entry = (/* ev_hndlr_tbl_entry_t */ void *) p_entry->data;
        p_ev_entry->event_id = ev_id;
        p_ev_entry->fn = ev_hndlr_fn;
        //list_append_entry(&ev_hndlr_list, p_entry);
        hash_index = ev_id % EVENT_HANDLER_HASH_SIZE;
        p_entry->next = event_handler_hash[hash_index];
        event_handler_hash[hash_index] = p_entry;
    }
}

void ev_unregister_event_handler(event_id_t ev_id, ev_hndlr_fn_t ev_hndlr_fn)
{
    list_entry_t *p_entry;
    list_entry_t *p_entry_to_free;
    list_entry_t **p_prev_entry_next_field;
    ev_hndlr_tbl_entry_t *p_ev_entry;
    uint16_t hash_index;
    uint16_t entries_removed = 0;

    hash_index = ev_id % EVENT_HANDLER_HASH_SIZE;

    p_prev_entry_next_field = &event_handler_hash[hash_index];
    p_entry = event_handler_hash[hash_index];
    while (p_entry)
    {
        p_ev_entry = (/*ev_hndlr_tbl_entry_t*/ void *) p_entry->data;
        if (p_ev_entry->event_id == ev_id &&
            p_ev_entry->fn == ev_hndlr_fn)
         {
            /* remove from list */   
            p_entry_to_free = p_entry;
            *p_prev_entry_next_field = p_entry->next;
            p_entry = p_entry->next;
            entries_removed++;
            list_free_entry(&ev_hndlr_list, p_entry_to_free);
         }
         else
         {
            p_prev_entry_next_field = &p_entry->next;
            p_entry = p_entry->next;
         }
    }
    assert(entries_removed != 0); /* inform if an entry couldn't be found */
}


const char * ev_name(event_id_t ev_id)
{
   unsigned int i;
   uint8_t b_found = FALSE;
   const char *result = NULL;

   for (i=0; i<NELEMENTS(event_id_2_name);i++)
   {
      if (event_id_2_name[i].ev_id == ev_id)
      {
         result = event_id_2_name[i].ev_name;
         b_found = TRUE;
         break;
      }
   }
   if (!b_found)
   {
      result = "?";
   }
   return result;
}   

void ev_raise_event_ex(event_id_t ev_id, ev_data_t *ev_data)
{
    list_entry_t *p_entry;
    ev_t *p_ev;

    p_entry = list_get_new_entry(&ev_queue);
    if (p_entry)
    {
        p_ev = (/*ev_t*/ void *) p_entry->data;
        p_ev->ev_id = ev_id;
        if (ev_data)
        {
            p_ev->ev_data = *ev_data;
        }
        else
        {
            p_ev->ev_data.type = EVDT_NO_DATA;
        }
        p_ev->time_raised = GetSystemTime();
        list_append_entry(&ev_queue, p_entry);
    }
    else
    {
       assert(p_entry != NULL);
    }
}

void ev_raise_event(event_id_t ev_id, ev_data_t *ev_data)
{
    list_entry_t *p_entry;
    ev_t *p_ev;

    p_entry = list_get_new_entry(&ev_queue);
    if (p_entry)
    {
        p_ev = (/*ev_t*/ void *) p_entry->data;
        p_ev->ev_id = ev_id;
        if (ev_data)
        {
            p_ev->ev_data = *ev_data;
        }
        else
        {
            p_ev->ev_data.type = EVDT_NO_DATA;
        }
        /* p_ev->ev_data.info = EV_INFO_NORMAL;*/
        p_ev->time_raised = GetSystemTime();
        list_append_entry(&ev_queue, p_entry);
    }
    else
    {
       assert(p_entry != NULL);
    }
}

void ev_raise_high_priority_event(event_id_t ev_id, ev_data_t *ev_data)
{
    list_entry_t *p_entry;
    ev_t *p_ev;

    p_entry = list_get_new_entry(&ev_queue);
    if (p_entry)
    {
        p_ev = (/*ev_t*/ void *) p_entry->data;
        p_ev->ev_id = ev_id;
        if (ev_data)
        {
            p_ev->ev_data = *ev_data;
        }
        else
        {
            p_ev->ev_data.type = EVDT_NO_DATA;
        }
        /* p_ev->ev_data.info = EV_INFO_NORMAL;*/
        p_ev->time_raised = GetSystemTime();
        list_append_entry_high_priority(&ev_queue, p_entry);
    }
    else
    {
       assert(p_entry != NULL);
    }
}

void ev_create_log_event(event_id_t ev_id, ev_data_t *ev_data)
{
    list_entry_t *p_entry;
    ev_t *p_ev;

    p_entry = list_get_new_entry(&ev_queue);
    if (p_entry)
    {
        p_ev = (/*ev_t*/ void *) p_entry->data;
        p_ev->ev_id = ev_id;
        if (ev_data)
        {
            p_ev->ev_data = *ev_data;
        }
        else
        {
            p_ev->ev_data.type = EVDT_NO_DATA;
        }
        p_ev->time_raised = GetSystemTime();
        p_ev->time_dispatched = 0;
        list_free_entry(&ev_queue, p_entry);
    }
    else
    {
       assert(p_entry != NULL);
    }
}   

typedef struct 
{
   ev_t *p_ev;             /* ponter to event to dispatch */
   uint16_t handlers_called;   /* to keep track of the number of handlers called for this event */
} event_dispatch_context_t;

#ifdef DBG_EV_DISPATCH
typedef struct
{
   uint32_t entrys;
   uint32_t events_removed_from_active;
   uint32_t events_appended_to_free;
   uint32_t events_prepended_to_free;
   uint32_t exits;
} ev_dispatch_stats_t;

ev_dispatch_stats_t ev_dispatch_stats = { 0,0,0,0 };
#endif

void ev_dispatch_one_event(void)
{
    list_entry_t *p_ev_list_entry;
    event_dispatch_context_t context;

    #ifdef DBG_EV_DISPATCH
      ev_dispatch_stats.entrys++;
    #endif

    p_ev_list_entry = list_get_first_high_priority(&ev_queue);
    if (p_ev_list_entry == NULL)
      p_ev_list_entry = list_get_first(&ev_queue);

    if (p_ev_list_entry)
    {
        #ifdef WIN32
           printf("ev_dispatch: %d-%d dispatching ev %d\n", GetSystemTime(), GetNexusCycle(), ((ev_t *)p_ev_list_entry->data)->ev_id);
        #endif
        
         /* we got an event, now lets call all handlers registered for this event */
        list_entry_t *p_hndlr_list_entry;
        ev_hndlr_tbl_entry_t *p_hndlr;
        ev_t *p_event;
        uint16_t hash_index;

        #ifdef DBG_EV_DISPATCH
           ev_dispatch_stats.events_removed_from_active++;
        #endif
        //context.handlers_called = 0;
        p_event = ( /*ev_t*/ void *)p_ev_list_entry->data;
        context.p_ev = p_event;
        p_event->time_dispatched = GetSystemTime();
        p_event->stats.num_handlers_called = 0;
        hash_index = p_event->ev_id % EVENT_HANDLER_HASH_SIZE;

        p_hndlr_list_entry = event_handler_hash[hash_index];
        while (p_hndlr_list_entry)
        {
            p_hndlr = (/*ev_hndlr_tbl_entry_t*/ void *) p_hndlr_list_entry->data;
            if (p_hndlr->event_id == p_event->ev_id)
             {
                /* call handler */
                p_hndlr->fn(p_event);
                context.handlers_called++;
                p_event->stats.num_handlers_called++;
             }
             p_hndlr_list_entry = p_hndlr_list_entry->next;
        }

      #ifdef REPORT_UNHANDLED_EVENTS
        if (!p_event->stats.num_handlers_called)
        {
            char s_text[50];
            const char *event_name;
            event_name = ev_name(context.p_ev->ev_id);
            if (event_name)
            {
               sprintf(s_text, "Unhandled event %s", event_name);
            }
            else
            {
               sprintf(s_text, "Unhandled event %d (unknown)", context.p_ev->ev_id);
            }
            term_log(s_text);
        }
      #endif /* REPORT_UNHANDLED_EVENTS */

        /* and free the entry */
        //if (p_event->ev_data.info & EV_INFO_NO_HISTORY ||
        //    p_event->ev_id == EV_USAGE_TIMER || 
        //    p_event->ev_id == EV_MAIN_DRIVE_SPEED_CONTROL_TIMER ||
        //    p_event->ev_id == EV_TIMEOUT_HW_WATCHDOG_RESET
        //   )
        //{
        //    /* we need to free these events but put it at the head of the free list */
        //    /* so that we do not lose our event history unneccessarily            */
        //    list_free_entry_prepend(&ev_queue, p_ev_list_entry);
        //    #ifdef DBG_EV_DISPATCH
        //       ev_dispatch_stats.events_prepended_to_free++;
        //    #endif
        //}
        //else
        {
            list_free_entry(&ev_queue, p_ev_list_entry);
            #ifdef DBG_EV_DISPATCH
               ev_dispatch_stats.events_appended_to_free++;
            #endif
        }
    }
    #ifdef DBG_EV_DISPATCH
      ev_dispatch_stats.exits++;
   #endif
}


static void dump_event_details(ev_t *p_ev)
{
   char sz_log[200];
   switch(p_ev->ev_data.type)
   {
      case EVDT_STATE_CHANGE_LOG:
         //switch(p_ev->ev_data.d.state_change.p_sm_context->type)
         //{
         //   case SM_CONTEXT_DC_MTR_CTRL:
         //      sprintf(sz_log, "mtr %s ",dc_mtr_id_to_str(p_ev->ev_data.d.state_change.p_sm_context->d.dc_mtr_ctrl.mtr_id));
         //      term_tx_str(sz_log);
         //      break;

         //   case SM_CONTEXT_STEP_MTR_CTRL:
         //      sprintf(sz_log, "mtr %s ",step_mtr_id_to_str(p_ev->ev_data.d.state_change.p_sm_context->d.step_mtr_ctrl.mtr_id));
         //      term_tx_str(sz_log);
         //      break;

         //   default:
         //      /* not decoded yet */
         //      break;
         //}

         sprintf(sz_log, "%s-->%s", 
                           p_ev->ev_data.d.state_change.state_from->tbl_name,
                           p_ev->ev_data.d.state_change.state_to->tbl_name);
         term_tx_str(sz_log);
         break;

      case EVDT_TIMER:
         sprintf(sz_log, "tmr id %d", p_ev->ev_data.d.timer.timer_id);
         term_tx_str(sz_log);
         //if (p_ev->ev_id == EV_NO_SPEEDCOUNT_TIMEOUT && p_ev->ev_data.d.timer.p_context != NULL)
         //{
         //   sm_context_t *p_context;

         //   p_context = p_ev->ev_data.d.timer.p_context;
         //   sprintf(sz_log, " context %s", p_context->sm_tbl->tbl_name);
         //   term_tx_str(sz_log);
         //   //if (p_context->type == SM_CONTEXT_DC_MTR_CTRL)
         //   //{
         //   //   sprintf(sz_log, " mtr %s", dc_mtr_id_to_str(p_context->d.dc_mtr_ctrl.mtr_id));
         //   //   term_tx_str(sz_log);
         //   //}
         //}
         break;

      case EVDT_TIMER_LOG:
         switch (p_ev->ev_data.d.timer_log.action)
         {
            case TMR_CREATE:
               sprintf(sz_log, "create timer id=%d trig=%lu period=%lu %s",
                     p_ev->ev_data.d.timer_log.d.create_details.timer_id,
                     p_ev->ev_data.d.timer_log.d.create_details.trigger_time,
                     p_ev->ev_data.d.timer_log.d.create_details.period_time,
                     ev_name(p_ev->ev_data.d.timer_log.d.create_details.event_id));
               break;

            case TMR_KILL:
               sprintf(sz_log, "kill timer id=%d  found_timer=%d",
                     p_ev->ev_data.d.timer_log.d.kill_details.timer_to_kill,
                     p_ev->ev_data.d.timer_log.d.kill_details.timer_id_found);
               break;

            default:
               sprintf(sz_log, "unknown action %d", p_ev->ev_data.d.timer_log.action);
               break;
         }
         term_tx_str(sz_log);
         break;

      
      case EVDT_CIRC_BUF_LOG:
         sprintf(sz_log, "cb %8.8s start=%p end=%p get=%p put=%p size=%d free=%d", 
                           p_ev->ev_data.d.circ_buf_log.str,
                           p_ev->ev_data.d.circ_buf_log.cb.cb_start,
                           p_ev->ev_data.d.circ_buf_log.cb.cb_end,
                           p_ev->ev_data.d.circ_buf_log.cb.cb_get,
                           p_ev->ev_data.d.circ_buf_log.cb.cb_put,
                           p_ev->ev_data.d.circ_buf_log.cb.cb_size,
                           p_ev->ev_data.d.circ_buf_log.cb.cb_free_bytes);
         term_tx_str(sz_log);
         break;

      case EVDT_ASSERT_LOG:
         sprintf(sz_log, "assert failure [%.*s] in %s line %d", 
                           MAX_ASSERT_LOG_TEXT_DATA,
                           p_ev->ev_data.d.assert_log.data,
                           p_ev->ev_data.d.assert_log.file,
                           p_ev->ev_data.d.assert_log.line);
         term_tx_str(sz_log);
         break;

      case EVDT_TIMER_LIST_HDR_LOG:

         sprintf(sz_log, "tmr list hdr #free_entries=%lu free_low_mark=%lu #act_entries=%lu act_high_mark=%lu",
                           p_ev->ev_data.d.timer_list_header_log.free_num_entries,
                           p_ev->ev_data.d.timer_list_header_log.free_low_water_mark,
                           p_ev->ev_data.d.timer_list_header_log.active_num_entries,
                           p_ev->ev_data.d.timer_list_header_log.active_high_water_mark);
         term_tx_str(sz_log);
         sprintf(sz_log, "#add_in_order=%lu current_time=%lu active_head=%p",
                           p_ev->ev_data.d.timer_list_header_log.adds_in_order,
                           p_ev->ev_data.d.timer_list_header_log.current_time,
                           p_ev->ev_data.d.timer_list_header_log.active_head);
         term_tx_str(sz_log);
         break;

      case EVDT_TIMER_LIST_ENTRY_LOG:

         sprintf(sz_log, "tmr entry=%p next=%p trig=%lu per=%lu fn=%p con=%p ev=%s tmr=%u",
                           p_ev->ev_data.d.timer_list_entry_log.this,
                           p_ev->ev_data.d.timer_list_entry_log.next,
                           p_ev->ev_data.d.timer_list_entry_log.timer_details.trigger_time,
                           p_ev->ev_data.d.timer_list_entry_log.timer_details.period_time,
                           p_ev->ev_data.d.timer_list_entry_log.timer_details.timer_fn,
                           p_ev->ev_data.d.timer_list_entry_log.timer_details.p_context,
                           ev_name(p_ev->ev_data.d.timer_list_entry_log.timer_details.event_id),
                           p_ev->ev_data.d.timer_list_entry_log.timer_details.timer_id);
         term_tx_str(sz_log);
         break;

      default:
         /* not decoded yet */
         break;
   }

}

static void dump_raw_event(ev_t *p_ev)
{
   uint8_t *p_data;
   uint8_t i;
   char sz_log[20];

   p_data = (uint8_t *)p_ev;
   for (i=0; i<sizeof(ev_t); i++, p_data++)
   {
      if ( (i & 0x3) == 0 && i != 0)
      {
         term_tx_str(" ");
      }
      sprintf(sz_log, "%.2X", *p_data);
      term_tx_str(sz_log);
   }
}

//const char * getVersionString (void);

void ev_dump_event_log(void)
{
   list_entry_t *p_ev_list_entry;
   ev_t *p_event;
   const char *event_name;
   char sz_log[200];
   uint32_t entry_num = 0;
   uint32_t skipped_entries = 0;

   //stop_clock_tick();

   term_log("=============Event dump start=============\r\n");
   //   sprintf(sz_log, "feeder %s\r\n", getVersionString());
   term_tx_str(sz_log);
   sprintf(sz_log, "#entries=%lu\r\n", ev_queue.free.num_entries);
   term_tx_str(sz_log);
   sprintf(sz_log, "log#\t%-98s\t    raised\tdispatched\tevent", "raw event data");
   term_tx_str(sz_log);
   p_ev_list_entry = ev_queue.free.head;

   while(p_ev_list_entry)
   {
      entry_num++;
      p_event = ( void *)p_ev_list_entry->data;

      if (p_event->ev_id == 0)
      {
         skipped_entries++;
      }
      else
      {
         event_name = ev_name(p_event->ev_id);

         sprintf(sz_log, "\r\n%.4lu\t", entry_num);
         term_tx_str(sz_log);
         dump_raw_event(p_event);
         sprintf(sz_log, "\t%.10lu\t%.10lu\t%.3d-%.2d %40s\t", 
               p_event->time_raised,
               p_event->time_dispatched,
               p_event->ev_id,
               p_event->stats.num_handlers_called,
               event_name);

         term_tx_str(sz_log);

         dump_event_details(p_event);
      }
      p_ev_list_entry = p_ev_list_entry->next;
   }
   sprintf(sz_log, "\r\n skipped %lu null entries\r\n", skipped_entries);
   term_tx_str(sz_log);
   term_log("=============Event dump end===============\r\n");

   //start_clock_tick();
}

void ev_create_timer_list_log(void)
{
   ev_data_t ev_data;
   list_entry_t *p_entry;
   ev_timer_event_source_t *p_timer;
   bool b_processed_free_list = false;

   //stop_clock_tick();

   ev_data.type = EVDT_TIMER_LIST_HDR_LOG;
   ev_data.d.timer_list_header_log.free_num_entries       = ev_timer_list.free.num_entries;
   ev_data.d.timer_list_header_log.free_low_water_mark    = ev_timer_list.free.low_water_mark;
   ev_data.d.timer_list_header_log.active_num_entries     = ev_timer_list.active.num_entries;
   ev_data.d.timer_list_header_log.active_high_water_mark = ev_timer_list.active.high_water_mark;
   ev_data.d.timer_list_header_log.adds_in_order          = ev_timer_list.stats.list_adds_in_order;
   ev_data.d.timer_list_header_log.current_time           = GetSystemTime();
   ev_data.d.timer_list_header_log.active_head            = ev_timer_list.active.head;

   ev_create_log_event(EV_LOG_TIMER_LIST_HEADER, &ev_data);

   /* now walk along active timer list */
   ev_data.type = EVDT_TIMER_LIST_ENTRY_LOG;
   p_entry = ev_timer_list.active.head;

   while(p_entry)
   {
      p_timer = (ev_timer_event_source_t *) (void *)p_entry->data;
      ev_data.d.timer_list_entry_log.this = p_entry;
      ev_data.d.timer_list_entry_log.next = p_entry->next;
      ev_data.d.timer_list_entry_log.timer_details = *p_timer;
      ev_create_log_event(EV_LOG_TIMER_LIST_ENTRY, &ev_data);
      p_entry = p_entry->next;
      if (p_entry == NULL && !b_processed_free_list)
      {
         p_entry = ev_timer_list.free.head;
         b_processed_free_list = true;
      }
   }
   //start_clock_tick();
}
   
timer_id_t ev_create_timer_abs(uint32_t delay, uint32_t period_time, 
                               timer_fn_t timer_fn, void *p_context, 
                               uint16_t event_id)
{
    list_entry_t *p_entry;
    ev_timer_event_source_t *p_timer;
    #ifdef LOG_TIMERS
    ev_data_t ev_data;
    #endif /* LOG_TIMERS */

    p_entry = list_get_new_entry(&ev_timer_list);
    if (p_entry)
    {
        p_timer = (/* ev_timer_event_source_t */ void *) p_entry->data;
        p_timer->event_id = event_id;
        p_timer->period_time = period_time;
        p_timer->trigger_time = delay;
        p_timer->timer_fn = timer_fn;
        p_timer->p_context = p_context;

        list_add_in_order(&ev_timer_list, p_entry);

        #ifdef LOG_TIMERS
        ev_data.type = EVDT_TIMER_LOG;
        ev_data.d.timer_log.action = TMR_CREATE;
        ev_data.d.timer_log.d.create_details.trigger_time = p_timer->trigger_time;
        ev_data.d.timer_log.d.create_details.period_time  = p_timer->period_time;
        ev_data.d.timer_log.d.create_details.timer_id     = p_timer->timer_id;
        ev_data.d.timer_log.d.create_details.event_id     = p_timer->event_id;
        ev_create_log_event(EV_LOG_TIMER, &ev_data);
        #endif /* LOG_TIMERS */
        return p_timer->timer_id;
    }
    else
    {
      assert(p_entry != NULL);
      ev_create_timer_list_log();
      return 0;
    }
   
}


timer_id_t ev_create_timer_rel(uint32_t delay, uint32_t period_time, 
                               timer_fn_t timer_fn, void *p_context, 
                               uint16_t event_id)
{

    return ev_create_timer_abs(GetSystemTime()+delay, period_time, timer_fn, p_context, event_id);
   
}

void ev_timer_tick_changed(uint32_t time)
{
    uint8_t b_done = b_events_not_initialised;

    //restart_cycle_count();
   
    list_entry_t *p_entry;
    while (!b_done)
    {
        p_entry = list_peek_first(&ev_timer_list);
        if (p_entry)
        {
            ev_timer_event_source_t *p_timer;
            ev_data_t ev_data;

            p_timer = (/* ev_timer_event_source_t */ void *) p_entry->data;

            if (p_timer->trigger_time <= time) /* TODO: fix problem with wrap around  (after 49.7 days) */
            {
                uint8_t b_remove_timer = FALSE;
                uint8_t b_reschedule_timer = FALSE;

                /* timer triggered */
                if (p_timer->timer_fn)
                {
                  p_timer->timer_fn(p_timer->p_context);  
                }
                if (p_timer->event_id)
                {
                   ev_data.type = EVDT_TIMER;
                   ev_data.d.timer.expiry_time = time;
                   ev_data.d.timer.timer_id = p_timer->timer_id;
                   ev_data.d.timer.p_context = p_timer->p_context;

                   ev_raise_event(p_timer->event_id, &ev_data);
                }
                if (p_timer->period_time)
                {
                    if (p_entry->next)
                    {
                        ev_timer_event_source_t *p_next_timer = (/* ev_timer_event_source_t */ void *)p_entry->next->data;
                        if (p_next_timer->trigger_time - time > p_timer->period_time)
                        {
                            /* we can keep this timer where it is */
                            b_remove_timer = FALSE;
                            b_done = TRUE;
                        }
                        else
                        {
                            b_remove_timer = TRUE;
                            b_reschedule_timer = TRUE;
                        }
                    }
                    else
                    {
                        /* only timer in list --> keep it where it is */
                        b_remove_timer = FALSE;
                        b_done = TRUE;
                    }
                    p_timer->trigger_time = time + p_timer->period_time;

                }
                else
                {
                    b_remove_timer = TRUE;
                    b_reschedule_timer = FALSE;
                }

                if (b_remove_timer)
                {
                    p_entry = list_get_first(&ev_timer_list);
                    if (b_reschedule_timer)
                    {
                        list_add_in_order(&ev_timer_list, p_entry);
                    }
                    else
                    {
                        list_free_entry(&ev_timer_list, p_entry);
                    }
                }
            }
            else
            {
                b_done = TRUE;
            }
        }
        else
        {
            b_done = TRUE;
        }
    }
}

typedef struct
{
    timer_id_t timer_id;
} tm_loc_con_t; /* locate context */

static list_locate_result_t fn_locate_timer_id(void *p_context, void *p_data)
{
    if (p_context && p_data)
    {
        
        return ((tm_loc_con_t *)p_context)->timer_id == 
            ((ev_timer_event_source_t *)p_data)->timer_id ? LOCATE_MATCH : LOCATE_NO_MATCH;
    }
    return LOCATE_ERROR;   /* Error occured */
}

void ev_kill_timer(timer_id_t timer_id)
{
    list_entry_t *p_entry;
    tm_loc_con_t tm_locate_context;
    #ifdef LOG_TIMERS
    ev_data_t ev_data;
    #endif /* LOG_TIMERS */
    

    tm_locate_context.timer_id = timer_id;
    p_entry = list_get_with_criteria(&ev_timer_list, fn_locate_timer_id, &tm_locate_context);
    assert(p_entry != NULL);

    #ifdef LOG_TIMERS
    ev_data.type = EVDT_TIMER_LOG;
    ev_data.d.timer_log.action = TMR_KILL;
    ev_data.d.timer_log.d.kill_details.timer_to_kill = timer_id;
    #endif /* LOG_TIMERS */
    if (p_entry)
    {
        #ifdef LOG_TIMERS
        ev_data.d.timer_log.d.kill_details.timer_id_found =   ((ev_timer_event_source_t *)(void *)p_entry->data)->timer_id;
        #endif /* LOG_TIMERS */
        list_free_entry(&ev_timer_list, p_entry);
    }
    #ifdef LOG_TIMERS
    else
    {
      ev_data.d.timer_log.d.kill_details.timer_id_found = 0;
    }

    ev_create_log_event(EV_LOG_TIMER, &ev_data);
    #endif /* LOG_TIMERS */
}

static void compile_time_checks(void)
{
   COMPILE_TIME_ASSERT(NELEMENTS(event_id_2_name) <= EV_NUM_OF_EVENTS);

}


#if 0
typedef struct 
{
    int entry_num;
} ev_disp_queue_context_t;

void ev_disp_queue(list_t *p_list, char *prompt, fn_process_t fn_process)
{
    ev_disp_queue_context_t context;

    printf("\n  %s: #active %ld (%ld/%ld) #free %ld (%ld/%ld) max entries %ld \n",
               prompt,
               p_list->active.num_entries,
               p_list->active.low_water_mark,
               p_list->active.high_water_mark,
               p_list->free.num_entries,
               p_list->free.low_water_mark,
               p_list->free.high_water_mark,
               p_list->max_entries
               );

    context.entry_num = 0;
    list_process_active(p_list, fn_process, &context);
    printf("\n");
}

static int ev_disp_ev_q_item(void *p_context, void *p_data)
{
    if (p_context && p_data)
    {
        ev_t *p_ev = (ev_t *)p_data;
         
        if ( ((ev_disp_queue_context_t *)p_context)->entry_num++ == 0)
        {
            printf("\n\tnode | event | data type | detail data\n\t-----+-------|-----------|------------\n");
        }
        printf("\t%4d | %5d | %9d | ", ((ev_disp_queue_context_t *)p_context)->entry_num, 
                                       p_ev->ev_id,
                                       p_ev->ev_data.type);
        switch(p_ev->ev_data.type)
        {
        case 100: 
            printf("mtr_id=%d speed_count=%d\n", p_ev->ev_data.d.dc_mtr_ctrl.mtr_id,
                                                  p_ev->ev_data.d.dc_mtr_ctrl.speed_count);
            break;
        case 101:
            printf("timer_id=%d expiry=%ld\n", p_ev->ev_data.d.timer.timer_id,
                                               p_ev->ev_data.d.timer.expiry_time);
            break;
        default:
            printf("unknown\n");
            break;
        }
    }
    return 1;   /* inform caller we can process another item */
}

static int ev_disp_ev_hndlr_item(void *p_context, void *p_data)
{
    if (p_context && p_data)
    {
         
        if ( ((ev_disp_queue_context_t *)p_context)->entry_num++ == 0)
        {
            printf("\n\tnode | event | fn\n\t-----+-------+--------------\n");
        }
        printf("\t%4d | %5d | %p\n", ((ev_disp_queue_context_t *)p_context)->entry_num, 
                                     ((ev_hndlr_tbl_entry_t *)p_data)->event_id,
                                     ((ev_hndlr_tbl_entry_t *)p_data)->fn);
    }
    return 1;   /* inform caller we can process another item */
}

static int ev_disp_timer_item(void *p_context, void *p_data)
{
    static uint32_t now=0;
    if (p_context && p_data)
    {
         
        if ( ((ev_disp_queue_context_t *)p_context)->entry_num++ == 0)
        {
            now = GetSystemTime();
            printf("\n\tnode | timer | event | trigger   | period \n\t-----+-------+-------+-----------+--------\n");
        }
        printf("\t%4d | %5d | %5d | now%+6ld | %6ld\n", 
                                     ((ev_disp_queue_context_t *)p_context)->entry_num, 
                                     ((ev_timer_event_source_t*)p_data)->timer_id,
                                     ((ev_timer_event_source_t*)p_data)->event_id,
                                     ((ev_timer_event_source_t*)p_data)->trigger_time - now,
                                     ((ev_timer_event_source_t*)p_data)->period_time);
    }
    return 1;   /* inform caller we can process another item */
}



void ev_disp_ev_q(void)
{
    ev_disp_queue(&ev_queue, "ev_q ", ev_disp_ev_q_item);
}

void ev_disp_ev_handlers(void)
{
    ev_disp_queue(&ev_hndlr_list, "ev_hndlrs ", ev_disp_ev_hndlr_item);
}

void ev_disp_timers(void)
{
    ev_disp_queue(&ev_timer_list, "ev_timers ", ev_disp_timer_item);
}

void ev_disp_all(void)
{
    ev_disp_ev_q();
    ev_disp_ev_handlers();
    ev_disp_timers();
}
#endif

/* =================================================[Test code]==== */

#ifdef TEST_EVENTS



void fn_event_hndlr_1(ev_t *ev_ptr)
{
    printf("event handler 1: ev_id %d mtr_id %d\n", ev_ptr->ev_id, ev_ptr->ev_data.d.dc_mtr_ctrl.mtr_id);
}

void fn_event_hndlr_2(ev_t *ev_ptr)
{
    printf("event handler 2: ev_id %d timer_id %d\n", ev_ptr->ev_id, ev_ptr->ev_data.d.timer.timer_id);
}

void fn_test_events(void)
{
    ev_data_t ev_data_1;
    timer_id_t timer_ids[6];

    uint32_t now = GetSystemTime();

    printf("\n================[Initialisation test]==\n");
    ev_initialise();
    ev_disp_all();

    printf("\n\n========[register event handler test]==\n");
    ev_register_event_handler(1, fn_event_hndlr_1);
    ev_register_event_handler(2, fn_event_hndlr_1);
    ev_register_event_handler(3, fn_event_hndlr_1);
    ev_register_event_handler(4, fn_event_hndlr_2);
    ev_register_event_handler(1, fn_event_hndlr_2);
    ev_disp_ev_handlers();

    printf("\n\n==================[raise events test]==\n");
    ev_data_1.type = 100;
    ev_data_1.d.dc_mtr_ctrl.mtr_id = 1;
    ev_data_1.d.dc_mtr_ctrl.speed_count = 50;
    ev_raise_event(1, &ev_data_1);

    ev_data_1.d.dc_mtr_ctrl.mtr_id = 5;
    ev_data_1.d.dc_mtr_ctrl.speed_count = 84;
    ev_raise_event(1, &ev_data_1);

    ev_data_1.type = 101;
    ev_data_1.d.timer.expiry_time = 1000;
    ev_data_1.d.timer.timer_id = 50;
    ev_raise_event(4, &ev_data_1);

    ev_data_1.type = 100;
    ev_data_1.d.dc_mtr_ctrl.mtr_id = 1;
    ev_data_1.d.dc_mtr_ctrl.speed_count = 55;
    ev_raise_event(1, &ev_data_1);

    ev_disp_ev_q();
    

    printf("\n\n======================[Dispatch test]==\n");

    ev_dispatch_one_event();
    ev_disp_ev_q();

    ev_dispatch_one_event();
    ev_disp_ev_q();

    ev_dispatch_one_event();
    ev_disp_ev_q();

    ev_dispatch_one_event();
    ev_disp_ev_q();

    ev_dispatch_one_event();
    ev_disp_ev_q();



    printf("\n\n================[Timer creation test]==\n");

    ev_disp_timers();

    timer_ids[0] = ev_create_timer_rel(20, 0, 5000);
    timer_ids[1] = ev_create_timer_rel(15, 5, 5001);
    timer_ids[2] = ev_create_timer_rel(16, 8, 5002);
    timer_ids[3] = ev_create_timer_rel( 6,10, 5003);
    timer_ids[4] = ev_create_timer_rel( 7, 4, 5004);

    timer_ids[5] = ev_create_timer_abs( 2, 5, 5005);

    ev_disp_timers();


    printf("\n\n=================[Timer running test]==\n");

    {
        int x;
        for(x=0; x<500; x++)
        {
            new_cycle();
            ev_dispatch_one_event();
            //ev_disp_ev_q();
        }
    }
    ev_disp_timers();
    ev_disp_ev_q();

    printf("\n\n================[Timer deletion test]==\n");

    printf(" Killing timer %d\n", timer_ids[5]);
    ev_kill_timer(timer_ids[5]);
    ev_disp_timers();

    printf(" Killing timer %d\n", timer_ids[2]);
    ev_kill_timer(timer_ids[2]);
    ev_disp_timers();

    printf(" Killing timer %d\n", timer_ids[3]);
    ev_kill_timer(timer_ids[3]);
    ev_disp_timers();

    printf(" Killing timer %d\n", timer_ids[4]);
    ev_kill_timer(timer_ids[4]);
    ev_disp_timers();

    printf(" Killing timer %d\n", timer_ids[1]);
    ev_kill_timer(timer_ids[1]);
    ev_disp_timers();

    printf(" Killing timer %d\n", timer_ids[0]);
    ev_kill_timer(timer_ids[0]);
    ev_disp_timers();
  //printf("\n\n================[Initialisation test]==\n");

}

#endif /* TEST_LIST */

#endif /* MEMORYTEST */
/* ==============================================================[ EOF ]== */

