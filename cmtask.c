/************************************************************************
*   PROJECT:        FuturePhoenix
*   COMPANY:        Pitney Bowes
*   AUTHOR :        
*   MODULE NAME:    $Workfile:   CMTASK.C  $
*       
*   DESCRIPTION:    Control Manager Task - Interface to Print Manager 
*
* ----------------------------------------------------------------------
*               Copyright (c) 2003 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*   MODIFICATION HISTORY:
*
* 2018.02.07 - CWB 
* - Remove the EVT_CM_PMNVM_AVAILABLE flag.  Acsess will only be 
*   controlled by the disabling condition DCOND_ACCESSING_PM_NVM. 
*
* Initial version for FuturePhoenix 01/17/05 H. Sun
*************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "commontypes.h"
#include "pbos.h"
#include "global.h"
#include "sys.h"
#include "clock.h"
#include "UTILS.h"

#include "cm.h"
#include "cmpm.h"
#include "cmerr.h"

static char     DbgBuf[MAX_DBG_BUF_LEN];

#define DBG_CM_TASK         // to enable PutString messages to syslog

/* **********************************************************************
// FUNCTION NAME: ControlManagerTask
//
// DESCRIPTION: This task is responsible for all communications with the
//              Print Manager 
//
// AUTHOR: 
//
// *************************************************************************/
void ControlManagerTask(unsigned long argc, void *argv)
{
    INTERTASK               rMsg;                   // received intertask message 
    emCmState               newState, crntState;
    emCmStateEvent          newEvt;
    FN_PROCESS_INT_TASK_MSG fnCmMsgHandler;
    FN_PROCESS_EVT          fnCmEvtHandler;
    tCmEvtElmnt             evtElmnt;
    int                     rtn;
    
    fnSysLogTaskStart( "ControlManagerTask", CM );  
    // set default state to Uninit
    fnCmSetState(v0_Invalid, s1_Uninit);

    OSWakeAfter(500);       // sleep for a second


    fnSysLogTaskLoop( "ControlManagerTask", CM );  
    while(1) 
    {
        if( OSReceiveIntertask (CM, &rMsg, OS_SUSPEND) == SUCCESSFUL )
        {
            crntState = fnCmGetCrntState();

#ifdef DBG_CM_TASK
            sprintf(DbgBuf, "CM>> MsgSrc = %d Id = 0x%x St = %d", rMsg.bSource, rMsg.bMsgId, (int)crntState);
            fnDumpStringToSystemLog(DbgBuf);
#endif

            // Get msg processing function, bMsgID is the index
            fnCmMsgHandler = fnCmGetMsgHandler(rMsg.bSource, rMsg.bMsgId);

            // Extract the event embedded in the msg
            newEvt = v0_Invalid;
            rtn = fnCmMsgHandler(&rMsg, &newEvt);

#ifdef DBG_CM_TASK
            sprintf(DbgBuf, "CM>> State = %d Evt = %d", (int)crntState, (int)newEvt);
            fnDumpStringToSystemLog(DbgBuf);
#endif

            if(rtn == ERR || fnCmMsgHandler == fnPrcsUnknownMsg || newEvt == v0_Invalid) 
            {
                continue;
            }

            // Get event processing element, event is the index
            evtElmnt = fnCmNewStateByEvt(newEvt);
            fnCmEvtHandler = evtElmnt.fnCmEvtHandler;
            newState = evtElmnt.state;

#ifdef DBG_CM_TASK
            sprintf(DbgBuf, "CM>> Evt = %d OldS = %d NewS = %d", (int)newEvt, (int)crntState, (int)newState);
            fnDumpStringToSystemLog(DbgBuf);
#endif

            // Process event, newState may change if it is error condition 
            // NULL handler means no event processing is needed, and new
            // state will not be committed
            if(fnCmEvtHandler == NULL) 
            {
                continue;
            }

            fnCmEvtHandler(newEvt, &rMsg, &newState);

#ifdef DBG_CM_TASK
            sprintf(DbgBuf, "CM>> Evt = %d OldS = %d FNewS = %d", (int)newEvt, (int)crntState, (int)newState);
            fnDumpStringToSystemLog(DbgBuf);
#endif

            if(   newState == s30_NoAction
               || fnCmEvtHandler == fnCmHandleInvalid
               || fnCmEvtHandler == fnCmHandleMismatch) 
            {
                continue;
            }

            // Commit the state transition
            fnCmCommitNewState(newEvt, newState);

        } // if getting message
    } // end of while 
} //end of ControlManagerTask()

