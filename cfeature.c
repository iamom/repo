/****************************************************************
*                                                               
* PROJECT            :   Comet
* MODULE NAME        :   CFEATURE.C 
*
* MODULE PURPOSE     :   Feature enabling routines              
*                                                               
* MODULE DESCRIPTION :   Feature enabling routines              
*                                                               
* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
* WARNING! WARNING! WARNING! WARNING! WARNING!
* Most of the switch cases in the ModifyFeatureProper &
* IsFeatureEnabled functions have been grouped in giant ifdef's based
* on which products actually support the features.  Be sure you make
* your changes in the proper places.
* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*
* ----------------------------------------------------------------------
*               Copyright (c) 1998 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*                                                               *
* PRIVATE ROUTINES   :                                          *
*    NAME                  DESCRIPTION                          *
*                                                               *
* GenerateConfirmationCode This function calculates the         *
*                          enable/disable confirmation code.    *
*                                                               *
* fnFTRFakeModify          Replaces ModifyAdSlogan and 
*                          ModifyInscription
*
* ModifyAdSlogan    OBSOLETE
*                                                               *
* ModifyFeatureProper      This function enables/disables       *
*                          individual features.                 *
*                                                               *
* ModifyInscription OBSOLETE
*                                                               *
* ModifyFeaturePackage     This function enables/disables ads,  *
*                          inscriptions, and/or features as     *
*                          part of a package of features.       *
*                                                               *
* ModifyFeature            This function validates the entered  *
*                          code and calls the appropriate       *
*                          subroutine to do the enabling/       *
*                          disabling.                           *
*                                                               *
*                                                               *
* PUBLIC ROUTINES    :                                          *
*    NAME                  DESCRIPTION                          *
*                                                               *
* EnableFeature            This function is the application-level 
*                          routine for enabling/disabing ads,   *
*                          insciptions, features, etc.          *
*                                                               *
* ModemEuroConversion      This function is the application-level
*                          routine for doing the Euro conversion 
*                          via a modem message.                 *
*                                                               *
* MasterDisableVerifyCode1 This function is the application-level
*                          routine for validating the first of  *
*                          the 2 master disable codes.          *
*                                                               *
* MasterDisableVerifyCode2 This function is the application-level
*                          routine for validating the second of *
*                          the 2 master disable codes. It also  *
*                          does the disabling of all the ads,   *
*                          inscriptions, and features.          *
*                                                               *
* IsFeatureEnabled         This function retrieves the          *
*                          enable/disable status of the features.
*                                                               *
* IsEuroConversionDone     This function determines if the      *
*                          meter has been converted to a euro   *
*                          meter.                               * 
*                                                               *
* IsEuroCapable            This function determines if the      *
*                          meter is capable of being converted  *
*                          to a euro meter.                     *
*                                                               *
*****************************************************************/
/*   MODIFICATION HISTORY:

 22-May-17 Jennifer Hao on FPHX 02.12 cienet branch
       Commented out 19 functions: fnfAcctStatusFieldInit, fnfAcctFeatStatusFieldInit,fnfAreEServicesSupportedFldInit, fnfEServiceFeatStatusFieldInit,
       fnhkFeatureStatusEServiceCheck, fnfDelConFeatStatusFieldInit, fnfSigConFeatStatusFieldInit,fnfCertifiedFeatStatusFieldInit,
        fnfReturnReceiptFeatStatusFieldInit, fnfSloganBoxFeatStatusFieldInit, fnfCourtsFeatStatusFieldInit,fnfDiffWeighingFeatStatusFieldInit,
       fnfManualWeighingFeatStatusFieldInit, fnfEuroCurrFeatStatusFieldInit, fnfDataCaptFeatStatusFieldInit,fnfTextAdFeatStatusFieldInit,
       fnfSmartClassFeatStatusFieldInit,fnfSerialPortFeatStatusFieldInit, fnfSuprvisrPwdFeatStatusFieldInit because they aren't used by G9.

 21-Mar-14 sa002pe on FPHX 02.12 shelton branch
 	Merging in changes from Janus, some are related to U.K. EIB:
*	1. Changed ModifyFeatureProper & IsFeatureEnabled to add cases for
*		the Welsh postal logo feature.

 18-Mar-13 sa002pe on FPHX 02.10 shelton branch
	For Enh Req 220525:
	1. Changed ModifyFeatureProper & IsFeatureEnabled to add cases for
		the DLA Secure Downloads Required feature.
		
	For Janus Fraca 209134:
	1. Changed fnGetEnabledFeatureList to check if the non-packaged features are
		enabled in any way (ENABLD or PERMENABLD) instead of just if they're just plain ENABLD.
*
* 04-Nov-12 sa002pe on FPHX 02.10 shelton branch
*	Fraca 218245: Changed MasterDisableVerifyCode2 to only
*	change the accounting type and log the exception log message if actually
*	changing the accounting type.
*
*  2012.08.20 Clarisa Bellamy  FPHX02.10
  - Merge changes from 02.10 cienet.  Modify Account type logging to remove
    large buffer from stacks and use common headers.  Replaced logException 
    macro with logActgType macro to make future changes easier. 
*
*  2012.08.16 John Gao  FPHX 2.10 cienet branch
*   Modified MasterDisableVerifyCode2() to support accouting type logging.
*
*  2012.08.14 Bob Li    FPHX 2.10 cienet branch
*   Modified ModifyFeatureProper() and IsFeatureEnabled() to support Intelilink tank type checking.
*
*  2012.08.09 Jane Li - fphx 02.09 cienet branch 
*   Merged in change from fphx 02.08.
*   Added changes to support the new HTTPS Management.
*----
    *  2011.04.08  Rocky Dai     Merged from Janus tips branch:
    *   For Germany HTTPS:
    *   1. Changed ModifyFeatureProper & IsFeatureEnabled to add processing
    *       for the Secure Communications Required feature.
*----
*
*  2010.08.11 Clarisa Bellamy - fphx 02.07 branch 
*   While fixing fraca 192991 in cmos.c, replaced inline code with call to new
*     function fnGTS_SaveMaxArchiveSize().
*
*  2010.08.02   Raymond Shen   FPHX2.07
*   Modified ModifyFeatureProper() and IsFeatureEnabled() to support Auto Ads feature.
*
*  2009.05.08   Jingwei,Li   FPHX2.00
*    Modified ModifyFeatureProper() and IsFeatureEnabled() to support 
*    WOW High/Medium/Low throughput speed.
*
*  2009.03.17   Clarsia Bellamy FPHX 1.15
*   Modidify for support of Gemini PSD:
*   - Modify ModifyFeatureProper to check for any IBUTTON type, since the 
*     PSDT_IBUTTON, is now only one type of ipsd. 
* 
*  2008.09.23   Clarsia Bellamy FPHX 1.11 
*   IBI-Lite Changes:
*    - Update IBI-Lite and ultra lite cases in ModifyFeatureProper() to make 
*      sure that the IPSD supports IBI-Lite, if enabling, and to reset the 
*      meterstamp type to Normal if disabling the current selection. 
*   Cleanup:
*   - LINT: Add typecasts. 
*   - For clarity, replace functions ModifyInscription and ModifyAdSlogan with 
*     the new function fnFTRFakeModify.
*   - Add missing include files, remove some unnecessary include files.
*   - Comment out unused local variables.
*
*  2008.09.08   Joe Qu Manage LOG_STORAGE_WARNING in ModifyFeatureProper (Netset2)
*  2008.08.12   Joey Cui - Merged stuffs from Janus to support PC proxy feature.
*
*  2008.08.01   Joey Cui - Modified ModifyFeatureProper() to filter features that FP supports
*
*  2008.07.28   Joey Cui - Merged code from Mega_Tips to support the Budget Manager Lite feature
*
*  2007.04.26   Michael Lepore - Fixed Bud/Bus mgr package enable. Wasn't using mOperation
*
*
*           $Log:   H:/group/SPARK/ARCHIVES/UIC/CFEATURE.C_v  $
*   20-Nov-2007 Adam -  Modified ModifyFeatureProper() to fix fraca GMSE00133198.
*                       Remove the dependencies between E-Cert and ERR/CRN.
*   11-Jun-2007 Oscar - Merge code from Mega 15.13 to fix disabling EService feature problem.
*                       Added function PostDisableLastTrackingFeature() and PostDisableTrackingFeature()
*                       Updated function ModifyFeatureProper() and ModifyFeaturePackage()
*
*   07-May-2007 mi003le - Only RebootSystem() for Abacus if pAbacusReservedPool ptr not already setup
*
*   04-May-2007 mi003le - Added check for AUTO_METER_REFILL_SUPPORT for LAN connectivity for FP.
* 
*    Rev 1.67   06 Jul 2005 15:42:14   sa002pe
* Added cases for AUTO_AD_INSCR, SEBRING_INSERT_INTERFACE,
* & EXT_ACNT_MSGS.
* 
*    Rev 1.66   Jun 01 2005 12:28:22   ST001TE
* Fixed problem with AMR feature being marked as enabled before
* call to AMR feature enable function.
* 
*    Rev 1.65   May 27 2005 15:02:06   SI200FO
* Added code for setting up AMR on feature-enable.
* 
*    Rev 1.64   26 May 2005 18:42:50   sa002pe
* Changed sMainPCNAccounts & sSubPCNAccounts in fnGetEnabledFeatureList from
* shorts to unsigned shorts because that's what they're supposed to be &
* Mega was getting compiler warnings because they weren't.
* 
*    Rev 1.63   26 May 2005 16:01:24   sa002pe
* Added cases for AUTO_METER_REFILL_SUPPORT.
* Disallowed several cases that aren't supported by K700.
* 
*    Rev 1.62   23 May 2005 13:29:36   SA002PE
* Merging in changes from 1.61.1.2.
* Also added comments to the various ifdef's to make it clear
* whom they're for.
* 
*    Rev 1.61.1.2   06 May 2005 14:00:56   sa002pe
* Fixed some stuff for K700.
* 
*    Rev 1.61.1.1   06 May 2005 12:12:24   sa002pe
* Added cases for SMALL_METER_CAPACITY.
* Merged in K700 stuff.
* 
*    Rev 1.61.1.0   26 Apr 2005 17:45:34   sa002pe
* For Janus:
* Changed to have Janus do the GTS stuff the same way that
* Mega is doing it.
* 
* General Cleanup:
* 1. Removed all the stuff that only applied pre-EMD split since
* everyone is now using a split EMD.
* 2. Organized the case statements into common ifdef areas
* (i.e., all the Janus only cases in one ifdef area, all the
* Mega only cases in another, all the Janus & Phoenix only
* cases in another, etc.) to reduce the number of ifdefs by
* avoiding the situation where there were ifdefs on almost
* an individual case basis.
* 
*    Rev 1.61   Jan 12 2005 02:04:34   NEX1PPZ
* Fixed FRACA 13315, Make the comfirmation service fields in FEATURE STATUS screen to be consistent.
* 
*    Rev 1.60   23 Nov 2004 15:59:18   TX08856
* Added Netherlands TPG feature support.
* 
*    Rev 1.59   04 Nov 2004 15:22:40   sa002pe
* Cleaned up some lint warnings.
* Made sure the latest Mega changes don't affect Janus.
* 
*    Rev 1.58   02 Nov 2004 20:05:00   TX08854
* Added SmartClass to feature display list
* 
*    Rev 1.57   28 Oct 2004 11:32:00   JO507TO
* Added support for Feature Code for Remote Refill
* (IntelliLink LAN support)
* 
*    Rev 1.56   26 Oct 2004 15:57:24   sa002pe
* Cleaned up some lint warnings.
* Fixed some stuff so Janus can use this version of this file.
* Combined some conditional compile sections that are
* compiling for the same conditions.
* 
*    Rev 1.55   Sep 29 2004 14:57:56   CL501BE
* Requires suprcode.h 1.5.
* _ Replaced direct access of supervisor password information
*   with he correct function calls.  Supervisor password info
*   should not be accessed directly except by the functions
*   in suprcode.c file. 
* 
*    Rev 1.54   Sep 28 2004 16:32:02   MA507HA
* Use more accurate method to determine if
* reduced functionality Abacus is enabled.
* 
*    Rev 1.53   Sep 23 2004 13:24:52   MA507HA
* Display for BudgetManager Feature the
* base backage accounts and supplemental
* accounts if enabled.
* 
*    Rev 1.52   Sep 20 2004 14:57:04   MA507HA
* Support Reduced functionality Budget Manager
* for Wght Brk Reporting package.
* 
* 
*    Rev 1.51   02 Sep 2004 11:27:18   TX08856
* Do not allow USPS All services package to be enabled if all the EServices are permanently disabled.
* 
*    Rev 1.50   25 Aug 2004 10:31:52   TX08854
* Fix ifdef Comet per Sandra
* 
*    Rev 1.49   19 Aug 2004 07:51:42   TX08854
* Added support for Smart Class feature enable
* 
* 
*    Rev 1.48   09 Aug 2004 16:59:40   TX08856
* Added DUNS disabling condition.
* 
*    Rev 1.47   04 Aug 2004 12:02:06   TX08856
* Added special service support for ERR & Canada.
* Added customer reference support.
* Added picklist support to record for new special services and Canada fees/coverage.
* Added DUNS number picklist ref # support.
* 
*    Rev 1.46   28 Jul 2004 11:24:24   TX08856
* Added screen support for 2 new Mega features - 
* E-Return Receipt for Cert Mail & Cust Ref Req for Cert Mail (courts).
* 
*    Rev 1.45   27 Jul 2004 17:15:02   TX08856
* Added two new Mega features - E-Return Receipt for Cert Mail & 
* Cust Ref Req for Cert Mail (Courts).
* 
*    Rev 1.44   02 Jun 2004 11:41:14   TX08856
* Fix fraca MEGA10061 - Confirmation Services Enable/Disable status screen does not work properly.
* 
*    Rev 1.43   01 Jun 2004 15:10:46   SA002PE
* Fixed the tracking services case for Mega because the "else" that
* Linda Dore put in for rev 1.42 wouldn't actually be reached.
* Added cases for MANUAL_TEXT_MSGS for Janus.
* 
*    Rev 1.42   27 May 2004 13:06:46   TX08856
* Go back to supporting the original three USPS EServices 
* instead of the one generic Tracking Services 
* in the feature table.
* 
*    Rev 1.41   26 May 2004 17:27:24   SA002PE
* Made the 1.40 changes apply only to Mega since
* Janus & Phoenix haven't yet added Tracking Services.
* Added cases for the permit mode feature for Janus.
* 
*    Rev 1.40   12 May 2004 14:56:20   TX08856
* Merge USPS EServices into Generic Tracking Services.
* 
*    Rev 1.39   31 Mar 2004 17:21:38   SA002PE
* Added cases for AUTO_DAYLIGHT_SWITCH_SUPPORT
* for Janus & Mega.
* 
*    Rev 1.38   24 Mar 2004 16:46:16   SA002PE
* Merged in change from 1.35.1.0.
* Activated the Manual Weight Entry case in ModifyFeatureProper
* for Janus.
* Activated all Tracking Services processing for Janus.
* 
*----
    *    Rev 1.35.1.0   Feb 21 2004 16:13:00   tokarjo
    * Added call to RebootSystem() if any of the ABACUS
    * features are enabled to 'dynamically' <grin> allocate
    * the necessary memory for ABACUS to function.
*----
* 
*    Rev 1.37   01 Mar 2004 12:58:32   CR002DE
* 1. add back compile for maual weight entry feature in
* isfeatureenabled.
* 
*    Rev 1.36   09 Feb 2004 17:38:30   SA002PE
* Added checks to make sure the feature index into the feature table
* isn't beyond the size of the feature table as defined by the
* MAX_FEATURES constant.
* 
*    Rev 1.35   02 Feb 2004 14:41:14   SA002PE
* Fixed the index label for tracking services.
* 
*    Rev 1.34   02 Feb 2004 14:35:24   SA002PE
* Removed TRACKING_SVCS_SUPPORT #define now that
* it's in custdat.h for Mega.
* 
*    Rev 1.33   30 Jan 2004 12:22:12   SA002PE
* Added cases for TRACKING_SVCS_SUPPORT.
* 
*    Rev 1.32   08 Dec 2003 16:21:48   SA002PE
* Added cases for DATE_TIME_RECEIPT_SUPPORT 
* for Janus & Phoenix.
* Added a case for ALL_SPECIAL_SERVICES to
* IsFeaturePackageEnabled.
* Changed fnfAreEServicesSupportedFldInit to call
* IsFeaturePackageEnabled to find out if all the USPS
* special services are supported.
* Changed GetEnabledFeatureList to:
* --- return either the package code for the USPS
* special services if all the special services are
* enabled, or return the individual feature codes
* if only a few of the special services are enabled.
* --- to only return the codes for the features that
* are ENBLD, not the ones that are PERMENBLD
* because that is what Rob put in his spec.
* 
*    Rev 1.31   17 Nov 2003 19:23:12   SA002PE
* Added cases for 7 new feature bytes for Janus.
* Turned on the hi & lo platform feature bytes for Janus.
* Added fnGetEnabledFeatureList routine.
* Added USING_SPLIT_EMD #define so Phoenix can get
* the latest changes in this file without having to worry about
* the changes for the EMD split.
* Changed to conditionally compile out the abacus stuff
* for Phoenix.
* Added a case for TEXT_AD_SUPPORT to ModifyFeatureProper.
* Added a case for EXT_AD_SLOGAN_TRANSACTION to Modify Feature.
* Changed ModifyFeatureProper to return an error if the
* feature code is unknown.
* 
*    Rev 1.30   05 Nov 2003 11:09:24   SA002PE
* Changed to have Janus do the same thing as Phoenix
* for everything but the platform features since the
* platform features aren't yet added to Janus.
* Changed ModifyAdSlogan & ModifyInscription to always
* return a successful status so there will be no problem
* with external devices like GPS no that the use of the
* enabled ad & inscription tables in CMOS has be
* eliminated due to the EMD split.
* 
*    Rev 1.29   21 Oct 2003 15:13:12   SA002PE
* Merged in change from 1.22.1.1.
* Added conditional compiles for Janus.
* 
*--- 
    *    Rev 1.22.1.1   26 Aug 2003 23:31:06   CX17598
    * Fixed fraca 7246,7350,7306. -Andy
    * 
    *    Rev 1.27.1.0   08 Oct 2003 13:49:24   SA002PE
    * Forcing a branch until I can determine how much of this
    * file is actually needed for Janus.
*--- 
* 
*    Rev 1.28   17 Oct 2003 17:05:20   BA001PO
*             // I removed calling the Initialize function. I don't believe it
*             // is a good idea to switch the rate back to 9600
*             // the serial port should already be initialized at this
*             // point unless it was re-vectored in which case the 
*             // re-vectoring will initialize it when we are pointing
*             // toward a valid uart. GTM 10/12/03
*             // Setup the communications
*             //    fnEDMInitCommunications ( 9600 , IGNOREBUFS);
* 
* 
*             //    Stick in the current postage value.  We will compare later
*             //  when it changes and inform the device.
*             //    The silly scales need this so they can indicate to the user
*             //  that the postage is not set anymore
*             if( fnValidData( (unsigned short)BOBAMAT0,
*                      (unsigned short)VAULT_DEBIT_VALUE,
*                      &(pScaleStateInfo-> lwCurrentPostageValue) )
*                 != SUCCESSFUL )
*               fnProcessSCMError();
*           }
* Clean up for serial port.
* 
*    Rev 1.27   Aug 16 2003 14:44:14   cristeb
* Do not modify external modem type when setting Serial Port
* enable/disable status; comment out assignment statement in
* function ModifyFeatureProper().
* 
*    Rev 1.26   06 Aug 2003 14:38:28   BA001PO
* Added MODEM_TYPE_SERIAL where appropriate
* for Serial Port enable.
* 
*    Rev 1.25   01 Aug 2003 15:59:52   BA001PO
* Added support for when SERIAL PORT is enabled
* disabled with respect to accounting and the proper 
* procedure for enabling and disabling.
* 
*    Rev 1.24   Jul 22 2003 11:06:02   bellamyc
* Merged in the changes from the branch 1.22.1.0
* Added cases for the two not-so-new phoenix features, platform high capacity 
* and platform low capacity.
*--- 
    *    Rev 1.22.1.0   Jul 15 2003 13:30:08   bellamyc
    * Merged from the 1.16.1 branch: 
    *   Added cases for the 2 new PHoenix features:
    *--- 
        *    Rev 1.16.2.0   Mar 24 2003 17:27:48   ramprasad
        * Modified in functions ModifyFeatureProper()
        * and IsFeatureEnabled() to add the Platform 
        * Feature for Phoenix
*--------------
* 
*    Rev 1.23   Jun 24 2003 09:40:34   monrogt
* Modifed to be used with the EMD split. Eliminated the cmos graphic arrays. removed
* references to the same. deleted the portion of supporting graphcis that dealt with flash compression. Modfied
* the graphic wrappers to use the graphics file for ad,inscriptions, town circles and permits.
* handles installation the the emd into the FFS the list goes on
*
*    Rev 1.22   09 May 2003 09:06:46   MA507HA
* Implement Account switching for Hosted Abacus;
* Enabling/Disabling Abacus Host Task
* 
*    Rev 1.21   Apr 18 2003 12:58:40   MA507HA
* Support Enabling, Checking, disabling Hosted Abacus Accounting
* 
*    Rev 1.20   Apr 10 2003 09:23:14   domettg
* Added function call "fnGetDataNameAndVersion"
* to functions  "ModifyInscription" and "ModifyAdSlogan.
* This fixes a problem whereby enabled Ads 
* and Inscriptions were getting disabled after
* a software download.
* 
*    Rev 1.19   Feb 28 2003 10:31:02   degends
* Lint Patrol
* 
*    Rev 1.18   Jan 24 2003 14:40:48   francrj
* Moved feature index defines to header file features.h
* 
*    Rev 1.17   17 Jan 2003 16:48:50   defilcj
* 1. short term fix for fraca 5560, Budget manager
* feature status not visible on "existomg features" screen
* 
*    Rev 1.16   Oct 16 2002 18:11:44   degends
* Fixed bug that made it impossible to use feature codes
* generated in september during october.  The cause of
* the problem was that we weren't correctly dealing with
* the mod operations and accounting for rollover.
* The EnableFeature function contains the fix.
* 
*    Rev 1.15   09 Sep 2002 17:14:10   defilcj
* 1. fixed some problems with Abacus option
* pack and number of account feature enabling.
* 
*    Rev 1.14   05 Sep 2002 16:45:42   defilcj
* 1. add removal of the Abacus major period
* alarm if master disabling all features.
* 
*    Rev 1.13   16 Aug 2002 17:24:38   BA001PO
* Abacus stuff included in this one too.
* 
*    Rev 1.11.1.0   16 Aug 2002 17:23:42   BA001PO
* Abacus stuff now included
* 
*    Rev 1.11   06 Aug 2002 16:20:16   defilcj
* 1. integrate abacus into the tips
* 
*    Rev 1.10   Jul 18 2002 10:52:38   degends
* Updated the enabe/disable routines to init/reinit
* data capture using the new DCAPI.
*
*    Rev 1.9.1.1   31 Jul 2002 10:34:30   defilcj
* 1. added ReturnFeaturePackageState
* 
*    Rev 1.9.1.0   26 Jul 2002 16:41:28   defilcj
* 1. add support for abacus feature packages
* 2. add support for abacus number of account features
* 3. add IsFeaturePackageEnabled
* 
*    Rev 1.9   29 Apr 2002 16:29:02   defilcj
* 1. removed support for legacy accounting upgrade
* feature codes (ACNTING_25, ACNTING_50, etc).
* 
*    Rev 1.8   Apr 26 2002 16:41:16   degends
* Fixed FRACA 2347.
* 
*    Rev 1.7   19 Apr 2002 12:36:00   defilcj
* 1. changes to support new feature table with 
* entries for account levels A,B,and C.
* 2. changes to support feature enabling for
* ACCOUNTING, ACTING_LEVEL_A, ACTING_LEVEL_B,
* ACTING_LEVEL_C.  
* 
*    Rev 1.6   Apr 18 2002 18:29:06   degends
* Fixed the enabling of the new features (accounting and manual weight entry).
* 
*    Rev 1.5   Apr 16 2002 10:53:56   degends
* Updated IsFeatureEnabled to support new feature
* codes (diff weighing, manual weight entry, accounting).
* 
*    Rev 1.4   Apr 15 2002 16:48:18   degends
* Added some index definitions for new features.
* 
*    Rev 1.3   21 Dec 2001 15:26:14   defilcj
* did some formatting for readability
* 
*    Rev 1.2   14 Dec 2001 16:57:04   defilcj
* initial Department Accounting Integration
* 
*    Rev 1.1   Nov 14 2001 18:09:28   degends
* Lint Patrol: Removed some unused local variables.
* 
*    Rev 1.0   Oct 16 2001 11:20:16   degends
* Initial checkin of comet specific feature enabling module.
*------------------------------------------------------------------------ */

#include <string.h>

#include "commontypes.h"

//#include "abacuscommtask.h" // fnReserveAbacusMemPool(), abacucsdsapi.h
#include "aiservic.h"
#include "bob.h"
#include "bobutils.h"
#include "dcapi.h"
//#include "deptaccount.h"
#include "features.h"
#include "intellilink.h"
#include "oicommon.h"
//#include "oideptaccount.h"
//#include "oirateservice.h"      // fnOIClearRatingInfo()
//#include "pc_daemon.h"

#include "exception.h"  // logException()

#define THISFILE "Cfeature.c"
#define EXCEPTION_BUFF_SIZE     128

/* 
* EMD Split context switch
*/
//#define USING_SPLIT_EMD 1

/* Private Data */
/* Array Indices for the code digits */
#define CKSM1   0
#define CKSM2   2
#define CKSM3   6
#define CKSM4   1
#define CKSUM   4

#define CODE1   2
#define CODE2   6
#define CODE3   1

#define CODESERNUMDGT1 3
#define CODESERNUMDGT2 5

#define DCKSM1   0
#define DCKSM2   6
#define DCKSM3   4
#define DCKSM4   2
#define DCKSUM   1

#define MTHDGT   7

#define MSTR_DIS_DGT11 0
#define MSTR_DIS_DGT12 9
#define MSTR_DIS_DGT21 0
#define MSTR_DIS_DGT22 1

/* Misc. equates */
#define ENABLECHAR          0
#define FEATURE_TYP_OFFSET  CODE1
//#define FIRSTSEVENDAYS      7
#define NUM_CODE_DIGITS     8
#define NUM_DIS_CODE_DIGITS 7


#define PSD_MAX_SERIAL_NO_SZ 16

extern unsigned char bLastFeatureOperationType;
extern unsigned short wLastFeatureCode;
extern void *pAbacusReservedPool;
extern unsigned short usCMOSGTSArchiveMaxSize;


// oideptaccount.c:
extern SINT8 pActTypeScratchPad128[];

// oideptaccount.c:
extern const SINT8  cpActgLogHdr[];       
extern const SINT8  cpActgTypeLogHdr[];   
extern const SINT8  cpActgTypeLogSrcHdr[];


//*****************************************************************************
//           FEATURE TABLE MESSAGE CONTENTS                                   *
//*****************************************************************************
unsigned char fcTable[MAX_FEATURES][2] =
{
//           transaction type,              feature code
    /*  0 */ FEATURE_TRANSACTION,           SERIAL_PORT_MSG_SUPRT,
    /*  1 */ FEATURE_TRANSACTION,           ACCOUNTING           ,
    /*  2 */ FEATURE_TRANSACTION,           SLOGAN_CARD_PORT_ON  ,
    /*  3 */ FEATURE_TRANSACTION,           EURO_CURRENCY_SUPPORT,
    /*  4 */ FEATURE_TRANSACTION,           TEXT_AD_SUPPORT      ,
    /*  5 */ FEATURE_TRANSACTION,           DELIVERY_CONF_SUPPORT,
    /*  6 */ FEATURE_TRANSACTION,           DATA_CAPTURE_SUPPORT ,
    /*  7 */ FEATURE_TRANSACTION,           CERTIFIED_CONF_SUPPORT,
    /*  8 */ FEATURE_TRANSACTION,           SIGNATURE_CONF_SUPPORT,
    /*  9 */ FEATURE_TRANSACTION,           DIFF_WEIGHING_SUPPORT,
    /* 10 */ FEATURE_TRANSACTION,           MANUAL_WEIGHT_ENTRY_SUPPORT,
    /* 11 */ FEATURE_TRANSACTION,           ACNTING_LEVEL_A_SUPPORT,
    /* 12 */ FEATURE_TRANSACTION,           ACNTING_LEVEL_B_SUPPORT,
    /* 13 */ FEATURE_TRANSACTION,           ACNTING_LEVEL_C_SUPPORT,
    /* 14 */ FEATURE_PACKAGE_TRANSACTION,   ABACUS_OPTION_PACK_1,
    /* 15 */ FEATURE_PACKAGE_TRANSACTION,   ABACUS_OPTION_PACK_2,
    /* 16 */ FEATURE_PACKAGE_TRANSACTION,   ABACUS_OPTION_PACK_3,
    /* 17 */ FEATURE_TRANSACTION,           PLATFORM_LOW_CAPACITY_SUPPORT,
    /* 18 */ FEATURE_TRANSACTION,           PLATFORM_HIGH_CAPACITY_SUPPORT,
    /* 19 */ FEATURE_PACKAGE_TRANSACTION,   ABACUS_HOST_PACK,
    /* 20 */ FEATURE_TRANSACTION,           ACNTING_LEVEL_D_SUPPORT,
    /* 21 */ FEATURE_TRANSACTION,           PLATFORM_MED_CAPACITY_SUPPORT,
    /* 22 */ FEATURE_TRANSACTION,           UK_CONF_SVCS_SUPPORT,
    /* 23 */ FEATURE_TRANSACTION,           LOW_THRUPUT_SPEED_SUPPORT,
    /* 24 */ FEATURE_TRANSACTION,           MED_THRUPUT_SPEED_SUPPORT,
    /* 25 */ FEATURE_TRANSACTION,           HI_THRUPUT_SPEED_SUPPORT,
    /* 26 */ FEATURE_TRANSACTION,           CPC_PKG_SVCS_SUPPORT,
    /* 27 */ FEATURE_TRANSACTION,           DATE_TIME_RECEIPT_SUPPORT,
    /* 28 */ FEATURE_TRANSACTION,           TRACKING_SVCS_SUPPORT,
    /* 29 */ FEATURE_TRANSACTION,           AUTO_DAYLIGHT_SWITCH_SUPPORT,
    /* 30 */ FEATURE_TRANSACTION,           PERMIT_MODE_SUPPORT,
    /* 31 */ FEATURE_TRANSACTION,           MANUAL_TEXT_MSGS_SUPPORT,
    /* 32 */ FEATURE_TRANSACTION,           RETURN_RECEIPT_FOR_CERT_MAIL,
    /* 33 */ FEATURE_TRANSACTION,           CUST_REF_REQ_FOR_CERT_MAIL,
    /* 34 */ FEATURE_PACKAGE_TRANSACTION,   ABACUS_OPTION_PACK_4,
    /* 35 */ FEATURE_TRANSACTION,           SMART_CLASS_SUPPORT,
    /* 36 */ FEATURE_TRANSACTION,           REMOTE_REFILL_SUPPORT,
    /* 37 */ FEATURE_TRANSACTION,           NLD_TPG_TRACK_SUPPORT,
    /* 38 */ FEATURE_TRANSACTION,           SMALL_CAPACITY_METER_SUPPORT,
    /* 39 */ FEATURE_TRANSACTION,           AUTO_METER_REFILL_SUPPORT,
    /* 40 */ FEATURE_TRANSACTION,           AUTO_AD_INSCR_SUPPORT,
    /* 41 */ FEATURE_TRANSACTION,           SEBRING_INSERT_INTERFACE_SUPPORT,
    /* 42 */ FEATURE_TRANSACTION,           EXT_ACNT_MSGS_SUPPORT,
    /* 43 */ FEATURE_TRANSACTION,           PACKAGE_PICKUP_SUPPORT,
    /* 44 */ FEATURE_TRANSACTION,           BUDGET_MANAGER_UPLOAD_SUPPORT,
    /* 45 */ FEATURE_TRANSACTION,           BPOD_FOR_SIG_CONF_SUPPORT,
    /* 46 */ FEATURE_TRANSACTION,           CRE_FOR_CFM_SVCS_SUPPORT,
    /* 47 */ FEATURE_TRANSACTION,           PC_PROXY_SUPPORT,
    /* 48 */ FEATURE_TRANSACTION,           RATES_OVER_WGT_DISPLAY_SUPPORT,
    /* 49 */ FEATURE_TRANSACTION,           PC_PROXY_UI_SUPPORT,
    /* 50 */ FEATURE_TRANSACTION,           UNIPOSTA_TRACKING_SUPPORT,
    /* 51 */ FEATURE_TRANSACTION,           IBI_LITE_SUPPORT,
    /* 52 */ FEATURE_TRANSACTION,           IBI_ULTRA_LITE_SUPPORT,
    /* 53 */ FEATURE_TRANSACTION,           PREMIUM_ADDRESS_SUPPORT,
    /* 54 */ FEATURE_PACKAGE_TRANSACTION,   ABACUS_OPTION_PACK_5,
	/* 55 */ FEATURE_TRANSACTION,			IBI_CONFIRM_SUPPORT,
    /* 56 */ FEATURE_TRANSACTION,           LOW_WOW_THRUPUT_SPEED_SUPPORT,
    /* 57 */ FEATURE_TRANSACTION,           MED_WOW_THRUPUT_SPEED_SUPPORT,
    /* 58 */ FEATURE_TRANSACTION,           HI_WOW_THRUPUT_SPEED_SUPPORT,
    /* 59 */ FEATURE_TRANSACTION,           AUTO_AD_SUPPORT,
    /* 60 */ FEATURE_PACKAGE_TRANSACTION,   OVERRIDE_KIP_PARAMETERS,
    /* 61 */ FEATURE_TRANSACTION,           SECURE_COMM_REQUIRED_SUPPORT,
    /* 62 */ FEATURE_TRANSACTION,           INTELLIGENT_INK_CARTRIDGE,
    /* 63 */ FEATURE_TRANSACTION,           DLA_SECURE_DWNLD_REQUIRED_SUPPORT,
    /* 64 */ FEATURE_TRANSACTION,           WELSH_POSTAL_LOGO_SUPPORT,
};

//*****************************************************************************
// FUNCTION NAME:   fnGetProxyFtrSetting
// DESCRIPTION  :   This function determines what PC proxy features has been
//                  supported my the meter.
//
// AUTHOR       :   Sam Thillaikumaran
//
// INPUTS       :   None.
//
// RETURNS      :   PC_FTR_DISBLD - PC Proxy feature not supported
//                  PC_FTR_WITH_LTD_UI - PC Proxy with limited UI
//                  PC_FTR_WITH_FULL_UI - PC Proxy with full UI
// *****************************************************************************
uchar fnGetProxyFtrSetting(void)
{
    BOOL    fEnabled = FALSE;
    uchar   bFeatureState;
    uchar   bFeatureIndex;
    uchar   bRetVal = PC_FTR_DISBLD;

    if(IsFeatureEnabled(&fEnabled, PC_PROXY_UI_SUPPORT, &bFeatureState, &bFeatureIndex) && fEnabled)
    {
        bRetVal = PC_FTR_WITH_FULL_UI;
    }
    else if(IsFeatureEnabled(&fEnabled, PC_PROXY_SUPPORT, &bFeatureState, &bFeatureIndex) && fEnabled)
    {
        bRetVal = PC_FTR_WITH_LTD_UI;
    }
    else
    {
        bRetVal = PC_FTR_DISBLD;
    }

    return bRetVal;
}


/****************************************************************************/
/*                                                                          */
/* FUNCTION NAME        : GenerateConfirmationCode                          */
/*                                                                          */
/* FUNCTION DESCRIPTION : This function calculates the enable/disable       */
/*                        confirmation code.                                */
/*                                                                          */
/* AUTHOR               : S. Peterson                                       */
/*                                                                          */
/* DATE                 : 8 August 1999                                     */
/*                                                                          */
/* CALLING SEQUENCE     : EnableFeature.                                    */
/*                                                                          */
/* PRECONDITIONS        : None.                                             */
/*                                                                          */
/* POSTCONDITIONS       : None.                                             */
/*                                                                          */
/****************************************************************************/

void
GenerateConfirmationCode( char *pConfirmCode,

                                    /* Contains the confirmation code       */
                                    /* if needed, otherwise, contains 0s.   */

                          const char * const pCode,

                                    /* Pointer to the feature code entered  */
                                    /* by the user.                         */

                          const FeatureTransType *pFeatTransType)

                                    /* Pointer to the feature transaction.  */
{
    /* First fill code with zeros. */
    (void)memset( pConfirmCode, 0, NUM_CODE_DIGITS );

    /* Set up a confirmation code only for the transactions that need one. */
    if ((*pFeatTransType == DisableInscr) ||
        (*pFeatTransType == DisableAdSlogan) ||
        (*pFeatTransType == DisableFeat) ||
        (*pFeatTransType == EnableEuro)||
        (*pFeatTransType == DisableAll))
    {
        switch(*pFeatTransType)
        {
        case EnableEuro:

            /* Specify an enable confirmation code. */
            pConfirmCode[0] = '4';
            break;

        default:

            /* Specify a disable confirmation code. */
            pConfirmCode[0] = '3';
            break;
        }

        /* Finish the rest of the code, see B797163 for details. */
        pConfirmCode[2] = pCode[1];
        pConfirmCode[3] = pCode[3];
        pConfirmCode[4] = pCode[6];
        pConfirmCode[5] = pCode[5];
        pConfirmCode[6] = pCode[2];
        pConfirmCode[1] = '0' + (((pConfirmCode[0] - '0') * 7 +
                                  (pConfirmCode[6] - '0') * 5 +
                                  (pConfirmCode[4] - '0') * 3 +
                                  (pConfirmCode[2] - '0')) % 10);
    }
}


//****************************************************************************/
// FUNCTION NAME        : FakeModify
// FUNCTION DESCRIPTION : 
//      This function does nothing except check that the masked operation 
//      is a defined one, and if so, return TRUE.
//
//      This function replaces ModifyAdSlogan and ModfiyInscription, which
//       are obsolete since the invention of the Graphics file in FFS, in
//       which all graphics are always enabled.
// INPUTS:
//  bOperation - byte that contains the operation to fake and possibly a GTS
//                bit that we throw away.
//  pFTT -  ptr to location to store the operation TYPE that we are faking.
//  fAdNotInscription - TRUE if faking an Ad, FALSE if faking an inscription,
//          because they each have different transaction types.
//                                        
// HISTORY:
//  2009.09.22  Clarisa Bellamy     Function created.
//----------------------------------------------------------------------------
#define FAKE_AN_AD              TRUE
#define FAKE_AN_INSCRIPTION     FALSE
static BOOL fnFTRFakeModify( UINT8 bOperation, FeatureTransType *pFTT, BOOL fAdNotInscription )
{
    BOOL returnValue = TRUE;

    // Maks the feature state from the bOperation byte:
    switch( bOperation & ~FROM_GPS_STATION )
    {
        case ENABLE_TRANSACTION:
        case PERM_ENABLE_TRANSACTION:
            if( fAdNotInscription == FAKE_AN_AD )
            {
                *pFTT = EnableAdSlogan;
            }
            else
            {
                *pFTT = EnableInscr;
            }
            break;

        case DISABLE_TRANSACTION:
        case PERM_DISABLE_TRANSACTION:
        case READ_DISABLE_TRANSACTION:
            if( fAdNotInscription == FAKE_AN_AD )
            {
                *pFTT = DisableAdSlogan;
            }
            else
            {
                *pFTT = DisableInscr;
            }
            break;

        default:
            returnValue = FALSE;
            break;
    }

    return( returnValue );
}


/****************************************************************************/
/*                                                                          */
/* FUNCTION NAME        : ModifyFeatureProper                               */
/*                                                                          */
/* FUNCTION DESCRIPTION : This function enables/disables individual features.*/
/*                                                                          */
/* AUTHOR               : S. Peterson                                       */
/*                                                                          */
/* DATE                 : 8 August 1999                                     */
/*                                                                          */
/* CALLING SEQUENCE     : ModifyFeature                                     */
/*                                                                          */
/* PRECONDITIONS        :                                                   */
/*                                                                          */
/* POSTCONDITIONS       : None                                              */
/*                                                                          */
/****************************************************************************/

BOOL
ModifyFeatureProper(const uchar Operation,  /* This indicates if an enable  */
                                            /* or disable operation is      */
                                            /* requested.  Valid values are */
                                            /* ENABLE_TRANSACTION,          */
                                            /* DISABLE_TRANSACTION,         */
                                            /* READ_DISABLE_TRANSACTION,    */
                                            /* PERM_ENABLE_TRANSACTION,     */
                                            /* PERM_DISABLE_TRANSACTION.    */

                    FeatureTransType *pFTT, /* This is a pointer to the     */
                                            /* transaction type returned.   */

                    const uchar FeatureCode)/* This specifies the code of   */
                                            /* the feature to change        */
                                            /* the status of.               */
{
    unsigned short  usStatus;
    uchar   featureIndex;
    uchar   currentEnableStatus;
    uchar   mOperation = Operation & ~FROM_GPS_STATION; // Mask out source of request 
//    uchar bTmpStatus;
//    uchar bTmpIndex;
    BOOL    returnValue;
    BOOL    enabled;
//    BOOL fTmpEnabled;
//#ifdef G900
//    BOOL isUsingProxy = (BOOL)isProxyReachable();
//#endif


    // Retrieve information on the selected feature.
    returnValue = IsFeatureEnabled(&enabled, FeatureCode, &currentEnableStatus, &featureIndex);
    if (returnValue == TRUE)
    {
        // If we recognized the feature code, but it currently isn't in our feature table,
        // return an error
        if (featureIndex >= MAX_FEATURES)
            return (FALSE);
    }

    if( (returnValue == TRUE) ||
        (FeatureCode == ABACUS_ACCOUNTS_100) ||
        (FeatureCode == ABACUS_ACCOUNTS_300) ||
        (FeatureCode == ABACUS_ACCOUNTS_1000) ||
        (FeatureCode == ABACUS_ACCOUNTS_1500) ||
        (FeatureCode == ABACUS_ACCOUNTS_2000) ||
        (FeatureCode == ABACUS_ACCOUNTS_2500) ||
        (FeatureCode == ABACUS_ACCOUNTS_3000) )
    {
        switch(FeatureCode)
        { 
            // for Accounting feature codes just call the drivers feature modification function
            // and update the feature state and transaction type if sucessful
            case ACCOUNTING:
            case ACNTING_LEVEL_A_SUPPORT:   // accounting must be enabled for this to work
#ifndef K700
            case ACNTING_LEVEL_B_SUPPORT:   // accounting must be enabled for this to work  
            case ACNTING_LEVEL_C_SUPPORT:   // accounting must be enabled for this to work

#ifdef JANUS    // must be only Janus or G900 at this point
            case ACNTING_LEVEL_D_SUPPORT:   // accounting must be enabled for this to work
#endif
#endif
         
                // Enable/Disable or increase the number of internal accounts, Mega style 
//                usStatus = fnSetAccountingFeature(FeatureCode, mOperation);
/*
                if (usStatus == ACT_SUCCESS)
                {
                    switch (mOperation)
                    {
                        case ENABLE_TRANSACTION:
                            CMOSFeatureTable.fTbl[featureIndex] = ENBLD;        
                            *pFTT = EnableFeat;                                    
                            break;
                            
                        case PERM_ENABLE_TRANSACTION:                                   
                            CMOSFeatureTable.fTbl[featureIndex] = PERMENBLD;
                            *pFTT = EnableFeat;
                            break;

                        case DISABLE_TRANSACTION:
                            CMOSFeatureTable.fTbl[featureIndex] = DISBLD;
                            *pFTT = DisableFeat;
                            break;

                        case PERM_DISABLE_TRANSACTION:
                            CMOSFeatureTable.fTbl[featureIndex] = PERMDISBLD;
                            *pFTT = DisableFeat;
                            break;

                        default:
                            return (FALSE);
                    }
                    
                    return(TRUE);
                }
                else
                    return(FALSE);
*/
//              break;  // don't actually need the break because of the returns right before it.

            case ABACUS_ACCOUNTS_100:
            case ABACUS_ACCOUNTS_300:
            case ABACUS_ACCOUNTS_1000:
            case ABACUS_ACCOUNTS_1500:
            case ABACUS_ACCOUNTS_2000:

#if !defined (PHOENIX) || defined (G900)  // must be Mega or G900.  
                // increase the number of internal accounts, Abacus style 
/*
                usStatus = (unsigned short)fnAGDInstallFeature(FeatureCode, 
                                                               mOperation);
*/
/*
                if (usStatus == ABACUS_SUCCESS)
                    return(TRUE);
                else
                    return(FALSE);
*/

#else // must be something other than Mega or G900
                return(FALSE);
#endif
//              break;  // don't actually need the break because of the returns right before it.

            case ABACUS_ACCOUNTS_2500:
            case ABACUS_ACCOUNTS_3000:

#ifndef PHOENIX // must be Mega 
                // increase the number of internal accounts, Abacus style 
                usStatus = (unsigned short)fnAGDInstallFeature(FeatureCode, 
                                                               mOperation);
                if (usStatus == ABACUS_SUCCESS)
                    return(TRUE);
                else
                    return(FALSE);

#else // must be something other than Mega
                return(FALSE);
#endif
//              break;  // don't actually need the break because of the returns right before it.

#if !defined (PHOENIX) || defined (G900)  // must be Mega or G900.  //JY changed.
            case RETURN_RECEIPT_FOR_CERT_MAIL:
                // if trying to enable ERR, must make sure E-Certified is enabled

                //Comment out to fix fraca GMSE00133198. - Adam
                //if ( ( (mOperation == ENABLE_TRANSACTION) || (mOperation == PERM_ENABLE_TRANSACTION) ) &&
                //        IsFeatureEnabled(&fTmpEnabled, CERTIFIED_CONF_SUPPORT, &bTmpStatus, &bTmpIndex) && !fTmpEnabled )
                //    return (FALSE);
                
                break;

            case CUST_REF_REQ_FOR_CERT_MAIL:
                // if trying to enable Cust Ref Req/Courts feature, must make sure ERR is enabled

                //Comment out to fix fraca GMSE00133198. - Adam
                //if ( ( (mOperation == ENABLE_TRANSACTION) || (mOperation == PERM_ENABLE_TRANSACTION) ) &&
                //        IsFeatureEnabled(&fTmpEnabled, RETURN_RECEIPT_FOR_CERT_MAIL, &bTmpStatus, &bTmpIndex) && !fTmpEnabled )
                //    return (FALSE);
                
                break;
#endif

#ifndef PHOENIX // must be Mega
            case AUTO_METER_REFILL_SUPPORT:
//              Check if feature is being enabled
                if ((mOperation == ENABLE_TRANSACTION) || 
                    (mOperation == PERM_ENABLE_TRANSACTION))
                {
//                  Attempt to enable AMR feature
                    if (fnAMRFeatureEnable() == FALSE)
                    {
                        return (FALSE);
                    }
                }
                break;

#endif
            // For I-button PSDs, version 9.0 and above supports IBI-Lite features.
            case IBI_LITE_SUPPORT:
            case IBI_ULTRA_LITE_SUPPORT:
                if(    (fnFlashGetByteParm( BP_PSD_TYPES_SUPPORTED ) & PSDT_ALL_IBUTTON)
                    && (   (mOperation == ENABLE_TRANSACTION) 
                        || (mOperation == PERM_ENABLE_TRANSACTION))  )
                       
                {
                    UINT32 ulIPSD_FirmwareVer;

                    if( fnValidData( BOBAMAT0,              // Group ID
                                     LIPSD_FIRMWAREVERNUM,  // Item ID
                                     &ulIPSD_FirmwareVer )  // Parameter Pointer
                                      == BOB_OK )
                    {
                        // If we don't have a v9.00 PSD, don't let it be enabled
                        if( ulIPSD_FirmwareVer < 0x00090000 )
                        {
                            return( FALSE );
                        }
                    }
                    else
                    {
                        return( FALSE );
                    }
                }
                break;
                

            default:
                // The following check will only be true if the cases weren't allowed above
                if ((FeatureCode == ACNTING_LEVEL_B_SUPPORT) ||
                    (FeatureCode == ACNTING_LEVEL_C_SUPPORT) ||
                    (FeatureCode == ACNTING_LEVEL_D_SUPPORT))
                    return (FALSE);

                // all other feature codes that haven't caused a return already get to continue on
                break;
        }
        
        /****************************/
        // Set the new feature state. 
        /****************************/
        switch (mOperation)
        {
            case ENABLE_TRANSACTION:
            case PERM_ENABLE_TRANSACTION:

                *pFTT = EnableFeat;

                // If it is already enabled, return success. 
                if (enabled) // The GPS can override any setting
                {
                    if(Operation & FROM_GPS_STATION)
                    {
                        if(mOperation == PERM_ENABLE_TRANSACTION )
                            CMOSFeatureTable.fTbl[featureIndex] = PERMENBLD;
                        else
                            CMOSFeatureTable.fTbl[featureIndex] = ENBLD;
                    }
                    return (TRUE);
                }

                //we are trying to enable but we are not already enabled
                switch(currentEnableStatus)
                {
                    case DISBLD:
                    case REDISBLD:
                    case PERMDISBLD:
                    case PERMENBLD:
                    case ENBLD:
                    case NOT_PURCHASED:

                        if (Operation & FROM_GPS_STATION) // The GPS can override any setting
                        {
                            if(mOperation == PERM_ENABLE_TRANSACTION )
                                CMOSFeatureTable.fTbl[featureIndex] = PERMENBLD;
                            else
                                CMOSFeatureTable.fTbl[featureIndex] = ENBLD;
                        }
                        else
                        {
                            if (currentEnableStatus == DISBLD ||
                                currentEnableStatus == REDISBLD ||
                                currentEnableStatus == NOT_PURCHASED)
                            {
                                if (FeatureCode != EURO_CURRENCY_SUPPORT)
                                {
                                    if(mOperation == PERM_ENABLE_TRANSACTION )
                                        CMOSFeatureTable.fTbl[featureIndex] = PERMENBLD;
                                    else
                                        CMOSFeatureTable.fTbl[featureIndex] = ENBLD;
                                }
                            }
                            else
                                return(FALSE);
                        }
                        break;

                    default:
                        return (FALSE);
                }
                break;

            case DISABLE_TRANSACTION:
            case PERM_DISABLE_TRANSACTION:
            case READ_DISABLE_TRANSACTION:

                *pFTT = DisableFeat;

                // If it is already disabled, return success. 
                if (!enabled) // The GPS can override any setting 
                {
                    if(Operation & FROM_GPS_STATION )
                    {
                        if(mOperation == PERM_DISABLE_TRANSACTION )
                            CMOSFeatureTable.fTbl[featureIndex] = PERMDISBLD;
                        else
                        {
                            if(mOperation == DISABLE_TRANSACTION )
                                CMOSFeatureTable.fTbl[featureIndex] = DISBLD;
                            else
                                CMOSFeatureTable.fTbl[featureIndex] = REDISBLD;
                        }
                    }

                    return (TRUE);
                }
    
                //we are trying to disable but we are not already enabled
                switch(currentEnableStatus)
                {
                    case DISBLD:
                    case REDISBLD:
                    case PERMDISBLD:
                    case PERMENBLD:
                    case ENBLD:
                    case NOT_PURCHASED:

                        if (Operation & FROM_GPS_STATION) // The GPS can override any setting
                        {
                            if(mOperation == PERM_DISABLE_TRANSACTION )
                                CMOSFeatureTable.fTbl[featureIndex] = PERMDISBLD;
                            else
                            {
                                if(mOperation == READ_DISABLE_TRANSACTION)
                                    CMOSFeatureTable.fTbl[featureIndex] = REDISBLD;
                                else
                                    CMOSFeatureTable.fTbl[featureIndex] = DISBLD;
                            }
                        }
                        else
                        {
                            if (currentEnableStatus == ENBLD)
                            {
                                if (FeatureCode != EURO_CURRENCY_SUPPORT)
                                {
                                    if(mOperation == PERM_DISABLE_TRANSACTION )
                                        CMOSFeatureTable.fTbl[featureIndex] = PERMDISBLD;
                                    else
                                    {
                                        if(mOperation == READ_DISABLE_TRANSACTION)
                                            CMOSFeatureTable.fTbl[featureIndex] = REDISBLD;
                                        else
                                            CMOSFeatureTable.fTbl[featureIndex] = DISBLD;
                                    }
                                }
                            }
                            else
                                return(FALSE);
                        }
                        break;
                  
                    default:
                        return (FALSE);
                }
                break;

            default:
                return (FALSE);
        }

        /***********************************************************/
        // Now perform operations needed to enforce the new features.
        /***********************************************************/
        switch(FeatureCode)
        {
//*******************************************
// cases that are mostly common for everybody
        case SERIAL_PORT_MSG_SUPRT:
                   
#ifdef JANUS    // must be Janus or K700 or G900
            // Enable/Disable CMOSSetupParams.unSerialPortStatus BMP
            if ((CMOSFeatureTable.fTbl[SERIAL_PORT_MSG_SUPRT_INX] == ENBLD) ||
                (CMOSFeatureTable.fTbl[SERIAL_PORT_MSG_SUPRT_INX] == PERMENBLD))
            {
                /*Modify the cmos variable */
                CMOSSetupParams.ucSerialPortStatus = ENBLD;
            }
            else
            {   
                /*Modify the cmos variable  */
                CMOSSetupParams.ucSerialPortStatus = DISBLD;
            }
#else
    #ifndef PHOENIX // must be Mega
            // Enable/Disable CMOSSetupParams.unSerialPortStatus BMP
            if ((CMOSFeatureTable.fTbl[SERIAL_PORT_MSG_SUPRT_INX] == ENBLD) ||
                (CMOSFeatureTable.fTbl[SERIAL_PORT_MSG_SUPRT_INX] == PERMENBLD))
            {
                (void)fnModifySerialPort();
            }
            else
            {   
                (void)fnDeactAcctingForSerialPort();
                (void)fnModifySerialPort();
            }
    // else, must be Phoenix, which does nothing
    #endif
#endif
            break;

        case IBI_LITE_SUPPORT:
            if(   (mOperation == DISABLE_TRANSACTION) 
               || (mOperation == PERM_DISABLE_TRANSACTION) 
               || (mOperation == READ_DISABLE_TRANSACTION)  )
            {
                // If a Lite Indicia is selected - must deselect it and select the 
                //  Traditional indicia.
                if( fnGetCurrentIndiSelection() == LITE_INDICIA_SETTING )
                {
                    (void)fnSetCurrentIndiSelection( fnGetDefaultIndiSelection() );
                }
            }
            break;

        case IBI_ULTRA_LITE_SUPPORT:
            if(   (mOperation == DISABLE_TRANSACTION) 
               || (mOperation == PERM_DISABLE_TRANSACTION) 
               || (mOperation == READ_DISABLE_TRANSACTION)  )
            {
                if( fnGetCurrentIndiSelection() == EXTRA_LITE_INDICIA_SETTING )
                {
                    (void)fnSetCurrentIndiSelection( fnGetDefaultIndiSelection() );
                }
            }
            break;

        case SLOGAN_CARD_PORT_ON:
        case MANUAL_WEIGHT_ENTRY_SUPPORT:
        case MANUAL_TEXT_MSGS_SUPPORT:
        case PERMIT_MODE_SUPPORT:
        case DATE_TIME_RECEIPT_SUPPORT:
        case SECURE_COMM_REQUIRED_SUPPORT:
        case INTELLIGENT_INK_CARTRIDGE:
        case DLA_SECURE_DWNLD_REQUIRED_SUPPORT:
        case WELSH_POSTAL_LOGO_SUPPORT:
        case DIFF_WEIGHING_SUPPORT:
            // Nothing else needs to be done
            break;

//*******************************************
// cases that are only for K700 and G900
#if defined (K700) || defined (G900)
        case SMALL_CAPACITY_METER_SUPPORT:
            // Nothing else needs to be done  
            break;
#endif

//*******************************************
// cases that are only for Janus and G900 (& possibly K700)
#ifdef JANUS
#ifndef K700
        case LOW_THRUPUT_SPEED_SUPPORT:
        case MED_THRUPUT_SPEED_SUPPORT:
        case HI_THRUPUT_SPEED_SUPPORT:
#ifdef G900
        case LOW_WOW_THRUPUT_SPEED_SUPPORT:
        case MED_WOW_THRUPUT_SPEED_SUPPORT:
        case HI_WOW_THRUPUT_SPEED_SUPPORT:
#endif
#endif
        case PLATFORM_MED_CAPACITY_SUPPORT:
        case TEXT_AD_SUPPORT:
            // Nothing else needs to be done
            break;

        case PC_PROXY_SUPPORT:
//      case PC_PROXY_UI_SUPPORT:
            // if currently using proxy and the proxy feature is disabled
            // shouldn't go back to pbp which will throw an exception.
            // TBD US Phase V - need to add proxy code
// no more pc_daemon
//            if (isUsingProxy && (fnGetProxyFtrSetting() == PC_FTR_DISBLD))
            if (fnGetProxyFtrSetting() == PC_FTR_DISBLD)
            {
                clrNeedTogoToPbpFlag();
            }
            // TBD US Phase V
            break;
#endif

//*******************************************
// cases that are only for Janus and G900 (& possibly K700) & Phoenix
#if defined (JANUS) || defined (PHOENIX)    // Janus, G900, K700, Phoenix
        case PLATFORM_LOW_CAPACITY_SUPPORT:

#ifndef K700    // Janus, G900, Phoenix
        case PLATFORM_HIGH_CAPACITY_SUPPORT:
#endif
            // Nothing else needs to be done
            break;
#endif

//*******************************************
// cases that are only for Janus & G900 (& possibly K700) & Mega
#if defined (JANUS) || !defined (PHOENIX)   // must be either Janus or G900 or K700 or
                                            // Mega (because Mega is the only one that doesn't define PHOENIX)
        case AUTO_DAYLIGHT_SWITCH_SUPPORT:
            // Nothing else needs to be done
            break;

#ifndef K700    // Janus, G900, Mega
        case AUTO_AD_INSCR_SUPPORT:
        case AUTO_AD_SUPPORT:
            // For Janus, nothing else needs to be done at this time
            // If something needs to be done for Mega, put it in a #ifndef PHOENIX thing
            break;
#endif
#endif

//*******************************************
// cases that are only for Mega & Phoenix
#ifndef JANUS
        case EURO_CURRENCY_SUPPORT:
            // Call the flash routines to do the context switch
            // Not sure what if anything should be done here GTMjr 12/15/1999
/*          if( !() )
                returnValue = FALSE;
            else
            {
                *pFTT = EnableEuro;
                CMOSFeatureTable.fTbl[ featureIndex ] = PERMENBLD;
            }
*/
            break;
#endif

//*******************************************
// cases that are only for Mega (& possibly G900)
#if !defined (PHOENIX) || defined (G900)  // must be Mega or G900.  //JY changed.
#ifndef G900    // Mega
        case SMART_CLASS_SUPPORT:
        case REMOTE_REFILL_SUPPORT:
        case AUTO_METER_REFILL_SUPPORT:
            // Nothing else needs to be done  
            break;

        case SEBRING_INSERT_INTERFACE_SUPPORT:
            // Nothing else needs to be done  
            break;

        case EXT_ACNT_MSGS_SUPPORT:
            // Nothing else needs to be done  
            break;
#endif

        case BUDGET_MANAGER_UPLOAD_SUPPORT:

            if ((mOperation == DISABLE_TRANSACTION) || (mOperation == PERM_DISABLE_TRANSACTION) ||
                (mOperation == READ_DISABLE_TRANSACTION))
            {
/*
                if(fnADS_GetNumberOfPendingTransactions() > 0)
                {
                    returnValue = FALSE;
                }
                else
                {
                    returnValue = TRUE;
                }
*/

            }
            break;

#endif

//*******************************************
// generic default case for everyone
        default:
            returnValue = FALSE;
            break;
        }
    }
    else // unknown feature
        returnValue = FALSE;

    return (returnValue);
}


/****************************************************************************/
/*                                                                          */
/* FUNCTION NAME        : ModifyInscription                                 */
/*                                                                          */
/* FUNCTION DESCRIPTION : This function does nothing except check that the
//                         masked operation is a defined one, and if so
//                         return TRUE.
//      This function is obsolete, since the invention of the graphics file
//      in FFS, where all graphics are always enabled.                      */
/* AUTHOR               : S. Peterson                                       */
/* DATE                 : 8 August 1999                                     */
/* CALLING SEQUENCE     : ModifyFeature                                     */
/* PRECONDITIONS        :                                                   */
/* POSTCONDITIONS       : None                                              */
/****************************************************************************/
//  This function is obsolete!
//BOOL
//ModifyInscription( const uchar Operation,  /* This indicates if an enable  */
                                           /* or disable operation is      */
                                           /* requested.  Valid values are */
                                           /* ENABLE_TRANSACTION,          */
                                           /* DISABLE_TRANSACTION,         */
                                           /* READ_DISABLE_TRANSACTION,    */
                                           /* PERM_ENABLE_TRANSACTION,     */
                                           /* PERM_DISABLE_TRANSACTION.    */

//                   FeatureTransType *pFTT, /* This is a pointer to the     */
                                           /* transaction type returned.   */

//                   const uchar InscriptionCode)
                                           /* This specifies the code of   */
                                           /* the inscription to change    */
                                           /* the status of.               */
/*{
    BOOL returnValue = FALSE;

    // Set the new feature state.
    switch(Operation & ~FROM_GPS_STATION)
    {
    case ENABLE_TRANSACTION:
    case PERM_ENABLE_TRANSACTION:

        *pFTT = EnableInscr;
        returnValue = TRUE;
        break;

    case DISABLE_TRANSACTION:
    case PERM_DISABLE_TRANSACTION:
    case READ_DISABLE_TRANSACTION:

        *pFTT = DisableInscr;
        returnValue = TRUE;
        break;

    default:

        return (FALSE);
    }

    return (returnValue);
}
*/

/****************************************************************************/
/*                                                                          */
/* FUNCTION NAME        : ModifyFeaturePackage                              */
/*                                                                          */
/* FUNCTION DESCRIPTION : This function enables/disables ads, inscriptions, */
/*                        and/or features as part of a package of features. */
/*                                                                          */
/* AUTHOR               : S. Peterson                                       */
/*                                                                          */
/* DATE                 : 8 August 1999                                     */
/*                                                                          */
/* CALLING SEQUENCE     : ModifyFeature                                     */
/*                                                                          */
/* PRECONDITIONS        : None                                              */
/*                                                                          */
/* POSTCONDITIONS       : None                                              */
/*                                                                          */
/****************************************************************************/

BOOL
ModifyFeaturePackage(const uchar Operation, /* This indicates if an enable  */
                                            /* or disable operation is      */
                                            /* requested.  Valid values are */
                                            /* ENABLE_TRANSACTION,          */
                                            /* DISABLE_TRANSACTION,         */
                                            /* READ_DISABLE_TRANSACTION,    */
                                            /* PERM_ENABLE_TRANSACTION,     */
                                            /* PERM_DISABLE_TRANSACTION.    */

                    FeatureTransType *pFTT, /* This is a pointer to the     */
                                            /* transaction type returned.   */

                    const uchar FeatureCode)/* This specifies the code of   */
                                            /* the feature package. */
{
    unsigned char   featureIndex;
    unsigned char   currentEnableStatus;
    unsigned char   mOperation;
    BOOL            enabled;
    BOOL    returnValue = TRUE;


    if (Operation == PERM_DISABLE_TRANSACTION ||
        Operation == READ_DISABLE_TRANSACTION )
        return (FALSE);

    switch(FeatureCode)
    {
    case ALL_ADS:

        if (Operation != DISABLE_TRANSACTION)
        {
            *pFTT = EnableAdSlogan;
        }
        else
           returnValue = FALSE;
        break;

    case ALL_INSCRIPTIONS:

        if (Operation != DISABLE_TRANSACTION)
        {
            *pFTT = EnableInscr;
        }
        else
           returnValue = FALSE;
        break;

    case ALL_ADS_INSCRS:

        if (Operation != DISABLE_TRANSACTION)
        {
            *pFTT = EnableAll;
        }
        else
           returnValue = FALSE;

        break;

//#ifndef PHOENIX // must be Mega   //JY changed.
#if !defined (PHOENIX) || defined (G900)  // must be Mega or G900.  //JY changed.
    case ABACUS_OPTION_PACK_1:
    case ABACUS_OPTION_PACK_2:
    case ABACUS_OPTION_PACK_3:
    case ABACUS_OPTION_PACK_4:
    case ABACUS_OPTION_PACK_5:
    case ABACUS_HOST_PACK:

        mOperation = Operation & ~FROM_GPS_STATION; /* Mask out source of request */

        /* Retrieve information on the selected feature. */
        if (IsFeaturePackageEnabled(&enabled, FeatureCode,
                          &currentEnableStatus, &featureIndex))
        {
 //           unsigned short  usStatus;
 //           usStatus = (unsigned short)fnAGDInstallFeaturePackage(FeatureCode,
 //                                                                 mOperation);
/*            if (usStatus == ABACUS_SUCCESS)
            {
                switch (mOperation)
                {
                    case ENABLE_TRANSACTION:
                        CMOSFeatureTable.fTbl[featureIndex] = ENBLD;        
                        *pFTT = EnableFeat;                                    
                        break;

                    case DISABLE_TRANSACTION:
                        CMOSFeatureTable.fTbl[featureIndex] = DISBLD;
                        *pFTT = DisableFeat;
                        break;

                    default:
                        return (FALSE);
                }           
                returnValue = TRUE;

                // WARNING WARNING WARNING!!!
                //
                // We are going to reboot the system in the event that one of the
                // ABACUS features was enabled or disabled.  This will correctly
                // take care of the memory needs of ABACUS upon reboot.  This
                // is handled from within fnReserveAbacusMemPool() called during
                // system startup.
                //
                if (pAbacusReservedPool == NULL)    // Only Reboot if 1st time feature enabled
                {
                    (void)fnReserveAbacusMemPool();
                    //#if !defined(JTAG_EMULATOR) && defined(GET_SCREENS_FROM_FLASH) // Manufacture build only
                    //  RebootSystem();
                    //#endif
                }
            }
            else
                returnValue = FALSE;*/
        }
        else
              returnValue = FALSE;
        break;
#endif

     default:

        returnValue = FALSE;
        break;
    }

    return (returnValue);
}


/****************************************************************************/
/*                                                                          */
/* FUNCTION NAME        : ModifyFeature                                     */
/*                                                                          */
/* FUNCTION DESCRIPTION : This function validates the entered code and calls*/
/*                        the appropriate subroutine to do the enabling/    */
/*                        disabling.                                        */
/*                                                                          */
/* AUTHOR               : S. Peterson                                       */
/*                                                                          */
/* DATE                 : 8 August 1999                                     */
/*                                                                          */
/* CALLING SEQUENCE     : HandleEnableFeature                               */
/*                                                                          */
/* PRECONDITIONS        :                                                   */
/*                                                                          */
/* POSTCONDITIONS       : None                                              */
/*                                                                          */
/****************************************************************************/

BOOL
ModifyFeature( const uchar FeatureType,   /* This is the type of feature  */
                                          /* to modify.                   */
                                          /* These are defined by         */
                                          /* constants from the beginning */
                                          /* of this file that end with   */
                                          /* _TRANSACTION.                */

               const uchar FeatureCode,   /* This is C and D from the     */
                                          /* feature code entered by the  */
                                          /* user.  C and D are described */
                                          /* in B797163.                  */

               const uchar Operation,     /* This indicates if an enable  */
                                          /* or disable operation is      */
                                          /* requested.  Valid values are */
                                          /* ENABLE_TRANSACTION,          */
                                          /* DISABLE_TRANSACTION,         */
                                          /* READ_DISABLE_TRANSACTION,    */
                                          /* PERM_DISABLE_TRANSACTION,    */
                                          /* PERM_ENABLE_TRANSACTION.     */
 
               FeatureTransType *pFTT,    /* This is a pointer to the     */
                                          /* transaction type returned.   */

               const BOOL ResetPasswd)    /* This indicates if the code   */
                                          /* was entered while trying to  */
                                          /* bypass the supervisor password*/

{
    BOOL    returnValue = FALSE;

    // If reset password transaction and not a GENERAL_TRANSACTION, then ignore.
    if( ResetPasswd && (FeatureType != GENERAL_TRANSACTION) )
        return(FALSE);

    switch( FeatureType )
    {
        case AD_SLOGAN_TRANSACTION:
            returnValue = fnFTRFakeModify( Operation, pFTT, FAKE_AN_AD );
            //returnValue = ModifyAdSlogan( Operation, pFTT, 0 );
        break;

        case INSCRIPTION_TRANSACTION:
            returnValue = fnFTRFakeModify( Operation, pFTT, FAKE_AN_INSCRIPTION );
            //returnValue = ModifyInscription( Operation, pFTT, 0 );
        break;

        case FEATURE_TRANSACTION:
            returnValue = ModifyFeatureProper( Operation, pFTT, FeatureCode );
        break;

        case GENERAL_TRANSACTION:
            switch( FeatureCode )
            {
                case RESET_SUPER_PSWRD:
                    // Clear the supervisor password and store it in NVM. 
                    returnValue = TRUE;
                    *pFTT = DisableFeat;
                    fnClearSuperPassword();
                break;

                default:
                break;
            }
        break;

        case FEATURE_PACKAGE_TRANSACTION:
            returnValue = ModifyFeaturePackage( Operation, pFTT, FeatureCode );
        break;

        case EXT_AD_SLOGAN_TRANSACTION:
            returnValue = fnFTRFakeModify( Operation, pFTT, FAKE_AN_AD );
            //returnValue = ModifyAdSlogan( Operation, pFTT, 0 );
        break;

        default:
        break;
    }

    return( returnValue );
}


/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : EnableFeature                          */
/*                                                               */
/* FUNCTION DESCRIPTION : This function is the application-level */
/*                        routine for enabling/disabing ads,     */
/*                        insciptions, features, etc.            */
/*                                                               */
/* AUTHOR               : S. Peterson                            */
/*                          Modified by Derek DeGennaro          */
/*                          For Comet 10/16/2001                 */
/*                                                               */
/* DATE                 : 8 August 1999                          */
/*                                                               */
/* CALLING SEQUENCE     :                                        */
/*                                                               */
/* PRECONDITIONS        : NONE                                   */
/*                                                               */
/* POSTCONDITIONS       : NONE                                   */
/*                                                               */
/*****************************************************************/

BOOL
EnableFeature(char *pCode, FeatureTransType *pFeatTransType, 
              const BOOL ResetPasswd)
{
    DATETIME    temp_date;

    FeatureTransType FeatTransType = (int)EnableFeat;

    uchar   i;
    uchar   strlen1;
    uchar   lastmthdgt;
    uchar   intcode[NUM_CODE_DIGITS+1];
    uchar   convertedcode;
    uchar   chkmth;  /* month to be checked against if trying to
                        use the code from previous month */

    char    confirmCode[NUM_CODE_DIGITS+1] = {0};
    char    sernum[PSD_MAX_SERIAL_NO_SZ+1];

    BOOL    retcode = TRUE;


    intcode[0] = '\0';   /* Init at least one byte to satisfy lint */

    strlen1 = (uchar)strlen(pCode);

    if (strlen1 != NUM_CODE_DIGITS)
    {
        return(FALSE);
    }

    for (i = 0; i <= NUM_CODE_DIGITS; i++)
    {
        intcode[i] = (uchar)(pCode[i] - '0'); /* convert to a number */
    }

    convertedcode = (uchar)(intcode[CODE2] * 10 + intcode[CODE3]);

    /* zero out buffer for serial number */
    memset(sernum,0,PSD_MAX_SERIAL_NO_SZ + 1);

    /* determine which serial number to use */
    if (fnFlashGetByteParm(BP_FEATURE_CODES_USE_MSN))
    {
        if (!fnValidData((unsigned short)BOBAMAT0,
                         (unsigned short)VLT_INDI_FILE_PBP_SERIAL, 
                         (void *)sernum))
        {
            return(FALSE);
        }
    }
    else
    {
        if (!fnValidData((unsigned short)BOBAMAT0,
                         (unsigned short)VLT_INDI_FILE_INDICIA_SERIAL, 
                         (void *)sernum))
        {
            return(FALSE);
        }
    }

    /* the result stored in sernum is NULL terminated because it is
    stored in the PSD in this way. */

    strlen1 = 0;
    while ((sernum[strlen1] != 0) && (sernum[strlen1] != ' '))
    {
        strlen1++;
    }

    if (strlen1 >= 2)   /* serial number length must be minimum of 2 */
    {
        if ((sernum[strlen1-2] == pCode[CODESERNUMDGT1]) &&
            (sernum[strlen1-1] == pCode[CODESERNUMDGT2]))
        {
            if (intcode[CKSUM] == ((intcode[CKSM1] * 7 +
                                    intcode[CKSM2] * 5 +
                                    intcode[CKSM3] * 3 +
                                    intcode[CKSM4]) % 10))
            {

                /* get the current date */
                (void)GetSysDateTime(&temp_date, FALSE, TRUE );

                lastmthdgt = ((temp_date.bMonth) % 10);
                if (intcode[MTHDGT] != lastmthdgt)
                {
                    if (temp_date.bMonth > 1)
                    {
                        if (lastmthdgt > 0)
                        {
                            chkmth = lastmthdgt - 1;
                        }
                        else
                        {
                            /* 
                            The month must be in October since
                            Oct (10) MOD 10 = 0.  So check the
                            previous month's code which is
                            Sept (9).
                            */
                            chkmth = 9; /* go from Oct (10) back to Sept (9) */
                        }
                    }
                    else
                        chkmth = 2;  /* go from Jan (x1) back to Dec (x2) */

                    if (intcode[MTHDGT] == chkmth)
                    {
                        /*if (temp_date.bDay <= FIRSTSEVENDAYS)
                        {*/
                            if ( (wLastFeatureCode != NLD_TPG_TRACK_SUPPORT) || 
                                ( (wLastFeatureCode == NLD_TPG_TRACK_SUPPORT) && (bLastFeatureOperationType == ENABLE_TRANSACTION) ) )
                            {
                                /* do not modify feature for NLD TPG disabling ... may need to upload records first */
                                if (ModifyFeature(intcode[FEATURE_TYP_OFFSET],
                                                  convertedcode, intcode[ENABLECHAR],
                                                  &FeatTransType, ResetPasswd))

                                {
                                    GenerateConfirmationCode(confirmCode, pCode, 
                                                             &FeatTransType);
                                }
                                else
                                {
                                    retcode = FALSE;
                                }
                            }
                            else
                            {
                                retcode = TRUE;
                            }
                        /*}
                        else
                            retcode = FALSE;         modify this for TSR010026  */
                    }
                    else
                    {
                        retcode = FALSE;
                    }
                }
                else
                {
                    if ( (wLastFeatureCode != NLD_TPG_TRACK_SUPPORT) || 
                        ( (wLastFeatureCode == NLD_TPG_TRACK_SUPPORT) && (bLastFeatureOperationType == ENABLE_TRANSACTION) ) )
                    {
                        /* do not modify feature for NLD TPG disabling ... may need to upload records first */
                        if (ModifyFeature(intcode[FEATURE_TYP_OFFSET],
                                          convertedcode, intcode[ENABLECHAR],
                                          &FeatTransType, ResetPasswd))
                        {
                            GenerateConfirmationCode(confirmCode, pCode, 
                                                     &FeatTransType);
                        }
                        else
                        {
                            retcode = FALSE;
                        }
                    }
                    else
                    {
                        retcode = TRUE;
                    }
                }
            }
            else
            {
                retcode = FALSE;
            }
        }
        else
        {
            retcode = FALSE;
        }
    }
    else
    {
        retcode = FALSE;
    }

    if (retcode == TRUE)
    {
        *pFeatTransType = FeatTransType;

        if ((*pFeatTransType == DisableInscr) ||
            (*pFeatTransType == DisableAdSlogan) ||
            (*pFeatTransType == DisableFeat) ||
            (*pFeatTransType == EnableEuro)||
            (*pFeatTransType == DisableAll))

            strcpy(pCode, confirmCode);
    }

    return (retcode);
}


/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : MasterDisableVerifyCode1               */
/*                                                               */
/* FUNCTION DESCRIPTION : This function is the application-level */
/*                        routine for validating the first of    */
/*                        the 2 master disable codes.            */
/*                                                               */
/* AUTHOR               : S. Peterson                            */
/*                          Modified by Derek DeGennaro for      */
/*                          Comet 10/16/2001                     */
/*                                                               */
/* DATE                 : 8 August 1999                          */
/*                                                               */
/* CALLING SEQUENCE     :                                        */
/*                                                               */
/* PRECONDITIONS        : NONE                                   */
/*                                                               */
/* POSTCONDITIONS       : NONE                                   */
/*                                                               */
/*****************************************************************/

BOOL
MasterDisableVerifyCode1(const char *pCode)
{
    DATETIME    temp_date;
    uchar       i;
    uchar       lastmthdgt;
    uchar       strlen1;
    uchar       intcode[NUM_CODE_DIGITS+1];
    char        sernum[PSD_MAX_SERIAL_NO_SZ+1];


    intcode[0] = '\0';   /* Init at least one byte to satisfy lint */

    strlen1 = (uchar)strlen(pCode);

    if (strlen1 != NUM_CODE_DIGITS)
        return(FALSE);

    for (i = 0; i <= NUM_CODE_DIGITS; i++)
        intcode[i] = (uchar)(pCode[i] - '0'); /* convert to a number */

    /* zero out buffer for serial number */
    memset(sernum,0,PSD_MAX_SERIAL_NO_SZ + 1);

    /* determine which serial number to use */
    if (fnFlashGetByteParm(BP_FEATURE_CODES_USE_MSN))
    {
        if (!fnValidData((unsigned short)BOBAMAT0,
                         (unsigned short)VLT_INDI_FILE_PBP_SERIAL,
                         (void *)sernum))
        {
            return (FALSE);
        }
    }
    else
    {
        if (!fnValidData((unsigned short)BOBAMAT0,
                         (unsigned short)VLT_INDI_FILE_INDICIA_SERIAL,
                         (void *)sernum))
        {
            return (FALSE);
        }
    }

    /* the result stored in sernum is NULL terminated because it is
    stored in the PSD in this way. */

    strlen1 = (uchar)strlen(sernum);

    if (strlen1 < 2)    /* serial number length must be minimum of 2 */
        return(FALSE);

    if ((sernum[strlen1-2] == pCode[CODESERNUMDGT1]) &&
        (sernum[strlen1-1] == pCode[CODESERNUMDGT2]))
    {
        if (intcode[CKSUM] != ((intcode[CKSM1] * 7 + 
                                intcode[CKSM2] * 5 +
                                intcode[CKSM3] * 3 + 
                                intcode[CKSM4]) % 10))
            return(FALSE);

        if ((intcode[0] != MSTR_DIS_DGT11) ||
            (intcode[2] != MSTR_DIS_DGT12) ||
            (intcode[6] != MSTR_DIS_DGT12) ||
            (intcode[1] != MSTR_DIS_DGT12))

            return(FALSE);

        /* get the current date */
        (void)GetSysDateTime(&temp_date, FALSE, TRUE );
                    
        lastmthdgt = ((temp_date.bMonth) % 10);
        if (intcode[MTHDGT] == lastmthdgt)
            return(TRUE);
        else
            return(FALSE);
    }
    else
        return(FALSE);
}


/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : MasterDisableVerifyCode2               */
/*                                                               */
/* FUNCTION DESCRIPTION : This function is the application-level */
/*                        routine for validating the second of   */
/*                        the 2 master disable codes. It also    */
/*                        does the disabling of all the ads,     */
/*                        inscriptions, and features.            */
/*                                                               */
/* AUTHOR               : S. Peterson                            */
/*                          Modified by Derek DeGennaro for      */
/*                          Comet 10/16/2001                     */
/*                                                               */
/* DATE                 : 8 August 1999                          */
/*                                                               */
/* CALLING SEQUENCE     :                                        */
/*                                                               */
/* PRECONDITIONS        : NONE                                   */
/*                                                               */
/* POSTCONDITIONS       : NONE                                   */
/*                                                               */
/*****************************************************************/

BOOL MasterDisableVerifyCode2(const char *pCode)
{
    uchar   i;
    uchar   strlen1;
    uchar   intcode[NUM_DIS_CODE_DIGITS+1];
    char    sernum[PSD_MAX_SERIAL_NO_SZ+1];

    intcode[0] = '\0';   /* Init at least one byte to satisfy lint */

    strlen1 = (uchar)strlen(pCode);

    if (strlen1 != NUM_DIS_CODE_DIGITS)
        return(FALSE);

    for (i = 0; i <= NUM_DIS_CODE_DIGITS; i++)
        intcode[i] = (uchar)(pCode[i] - '0'); /* convert to a number */

    /* zero out buffer for serial number */
    memset(sernum,0,PSD_MAX_SERIAL_NO_SZ + 1);

    /* determine which serial number to use */
    if (fnFlashGetByteParm(BP_FEATURE_CODES_USE_MSN))
    {
        if (!fnValidData((unsigned short)BOBAMAT0,
                         (unsigned short)VLT_INDI_FILE_PBP_SERIAL,
                         (void *)sernum))
        {
            return (FALSE);
        }
    }
    else
    {
        if (!fnValidData((unsigned short)BOBAMAT0,
                         (unsigned short)VLT_INDI_FILE_INDICIA_SERIAL,
                         (void *)sernum))
        {
            return (FALSE);
        }
    }

    /* the result stored in sernum is NULL terminated because it is
    stored in the PSD in this way. */

    strlen1 = (uchar)strlen(sernum);

    if (strlen1 < 2)    /* serial number length must be minimum of 2 */
        return(FALSE);

    if ((sernum[strlen1-2] == pCode[CODESERNUMDGT1]) &&
        (sernum[strlen1-1] == pCode[CODESERNUMDGT2]))
    {
        if (intcode[DCKSUM] != ((intcode[DCKSM1] * 7 + 
                                 intcode[DCKSM2] * 5 +
                                 intcode[DCKSM3] * 3 +
                                 intcode[DCKSM4]) % 10))
            return(FALSE);

        if ((intcode[0] != MSTR_DIS_DGT21) ||
            (intcode[6] != MSTR_DIS_DGT22) ||
            (intcode[4] != MSTR_DIS_DGT22) ||
            (intcode[2] != MSTR_DIS_DGT22))

            return(FALSE);

        /* Disable the accounting stuff */
/*        fnClearAccountPeriodDateAndAlarm();

#ifndef JANUS  // not Janus or K700
        fnAGDClearAbacusMajorPeriodAlarm();
#endif
        fnSetAccountPasswordsAllowed(false);

        (void)fnSetActiveAccount(ACT_NULL);
        (void)fnSetDefaultAccount(ACT_NULL);
		
		if (fnGetAccountingType() != ACT_SYS_TYPE_NONE)
		{
	        (void)fnSetAccountingType(ACT_SYS_TYPE_NONE);
		
	        // Log source of fnSetAccountingType.            
	        sprintf( pActTypeScratchPad128, "%s Master Disable", 
	                         cpActgTypeLogSrcHdr ); 
	        logActgType( pActTypeScratchPad128 );
		}
*/
				
        /* Disable the features */
        for (i = 0; i < MAX_FEATURES; i++)
        {
            switch (CMOSFeatureTable.fTbl[i])
            {
            case PERMDISBLD:
            case PERMENBLD:
            case DISBLD:

                /* do nothing */
                break;

            case REDISBLD:
            case ENBLD:
            default:

                CMOSFeatureTable.fTbl[i] = DISBLD;
                break;
            }
        }

        return(TRUE);
    }
    else
        return(FALSE);
}


/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : IsFeatureEnabled                       */
/*                                                               */
/* FUNCTION DESCRIPTION : This function retrieves the            */
/*                        enable/disable status of the features. */
/*                                                               */
/* AUTHOR               : S. Peterson                            */
/*                                                               */
/* DATE                 : 8 August 1999                          */
/*                                                               */
/* CALLING SEQUENCE     :                                        */
/*                                                               */
/* PRECONDITIONS        : NONE                                   */
/*                                                               */
/* POSTCONDITIONS       : NONE                                   */
/*                                                               */
/*****************************************************************/

BOOL
IsFeatureEnabled(BOOL *pEnabled,          /* Indicates if the feature is
                                           * enabled or disabled
                                           */

                 const uchar FeatureId,   /* ID of the feature that needs
                                           * to be checked per the codes
                                           * in datdict.h
                                           */

                 uchar *pFeattype,        /* Actual state of the feature per
                                           * the codes in datdict.h
                                           */

                 uchar *pInx)             /* Index of the feature in the
                                           * feature table
                                           */
{
    switch(FeatureId)
    {
//*******************************************
// cases that are common for everybody
    case SERIAL_PORT_MSG_SUPRT:

        *pFeattype = CMOSFeatureTable.fTbl[SERIAL_PORT_MSG_SUPRT_INX];
        *pInx = SERIAL_PORT_MSG_SUPRT_INX;
        break;

    case ACCOUNTING:

        *pFeattype = CMOSFeatureTable.fTbl[ACCOUNTING_INX];
        *pInx = ACCOUNTING_INX;
        break;

    case ACNTING_LEVEL_A_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[MAX_ACCT_LEVEL_A_INX];
        *pInx = MAX_ACCT_LEVEL_A_INX;
        break;

    case DATA_CAPTURE_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[DATA_CAPTURE_INX];
        *pInx = DATA_CAPTURE_INX;
        break;

    case IBI_LITE_SUPPORT:
        *pFeattype = CMOSFeatureTable.fTbl[IBI_LITE_INX];
        *pInx = IBI_LITE_INX;
        break;

    case IBI_ULTRA_LITE_SUPPORT:
        *pFeattype = CMOSFeatureTable.fTbl[IBI_ULTRA_LITE_INX];
        *pInx = IBI_ULTRA_LITE_INX;
        break;

    case SLOGAN_CARD_PORT_ON:

        *pFeattype = CMOSFeatureTable.fTbl[SLOGAN_CARD_PORT_ON_INX];
        *pInx = SLOGAN_CARD_PORT_ON_INX;
        break;

    case MANUAL_WEIGHT_ENTRY_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[MANUAL_WEIGHT_ENTRY_INX];
        *pInx = MANUAL_WEIGHT_ENTRY_INX;
        break;

    case MANUAL_TEXT_MSGS_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[MANUAL_TEXT_MSGS_INX];
        *pInx = MANUAL_TEXT_MSGS_INX;
        break;

    case PERMIT_MODE_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[PERMIT_MODE_INX];
        *pInx = PERMIT_MODE_INX;
        break;

    case DATE_TIME_RECEIPT_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[DATE_TIME_RECEIPT_INX];
        *pInx = DATE_TIME_RECEIPT_INX;
        break;

    case INTELLIGENT_INK_CARTRIDGE:
    
        *pFeattype = CMOSFeatureTable.fTbl[INTELLIGENT_INK_CARTRIDGE_INX];
        *pInx = INTELLIGENT_INK_CARTRIDGE_INX;
        break;
     

    case AUTO_AD_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[AUTO_AD_INX];
        *pInx = AUTO_AD_INX;
        break;
        
    case SECURE_COMM_REQUIRED_SUPPORT:
        
        *pFeattype = CMOSFeatureTable.fTbl[SECURE_COMM_REQUIRED_INX];
        *pInx = SECURE_COMM_REQUIRED_INX;
        break;
        
    case DLA_SECURE_DWNLD_REQUIRED_SUPPORT:
        
        *pFeattype = CMOSFeatureTable.fTbl[DLA_SECURE_DWNLD_REQUIRED_INX];
        *pInx = DLA_SECURE_DWNLD_REQUIRED_INX;
        break;
        
    case WELSH_POSTAL_LOGO_SUPPORT:
        
        *pFeattype = CMOSFeatureTable.fTbl[WELSH_POSTAL_LOGO_INX];
        *pInx = WELSH_POSTAL_LOGO_INX;
        break;

//*******************************************
// cases that are for everybody but K700
#ifndef K700
    case ACNTING_LEVEL_B_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[MAX_ACCT_LEVEL_B_INX];
        *pInx = MAX_ACCT_LEVEL_B_INX;
        break;

    case ACNTING_LEVEL_C_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[MAX_ACCT_LEVEL_C_INX];
        *pInx = MAX_ACCT_LEVEL_C_INX;
        break;

    case CERTIFIED_CONF_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[CERTIFIED_CONFIRMATION_INX];
        *pInx = CERTIFIED_CONFIRMATION_INX;
        break;

    case SIGNATURE_CONF_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[SIGNATURE_CONFIRMATION_INX];
        *pInx = SIGNATURE_CONFIRMATION_INX;
        break;

    case DELIVERY_CONF_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[DELIVERY_CONFIRMATION_INX];
        *pInx = DELIVERY_CONFIRMATION_INX;
        break;

    case DIFF_WEIGHING_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[DIFFERENTIAL_WEIGHING_INX];
        *pInx = DIFFERENTIAL_WEIGHING_INX;
        break;

    case UK_CONF_SVCS_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[UK_CONFIRMATION_SVCS_INX];
        *pInx = UK_CONFIRMATION_SVCS_INX;
        break;
#endif

//*******************************************
// cases that are only for K700 and G900
#if defined (K700) || defined (G900)
    case SMALL_CAPACITY_METER_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[SMALL_CAPACITY_METER_INX];
        *pInx = SMALL_CAPACITY_METER_INX;
        break;
#endif

//*******************************************
// cases that are only for Janus & G900 (& possibly K700)
#ifdef JANUS
#ifndef K700
    case ACNTING_LEVEL_D_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[MAX_ACCT_LEVEL_D_INX];
        *pInx = MAX_ACCT_LEVEL_D_INX;
        break;

    case LOW_THRUPUT_SPEED_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[LOW_THROUGHPUT_SPEED_INX];
        *pInx = LOW_THROUGHPUT_SPEED_INX;
        break;

    case MED_THRUPUT_SPEED_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[MED_THROUGHPUT_SPEED_INX];
        *pInx = MED_THROUGHPUT_SPEED_INX;
        break;

    case HI_THRUPUT_SPEED_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[HI_THROUGHPUT_SPEED_INX];
        *pInx = HI_THROUGHPUT_SPEED_INX;
        break;
#ifdef G900
    case LOW_WOW_THRUPUT_SPEED_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[LOW_WOW_THROUGHPUT_SPEED_INX];
        *pInx = LOW_WOW_THROUGHPUT_SPEED_INX;
        break;

    case MED_WOW_THRUPUT_SPEED_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[MED_WOW_THROUGHPUT_SPEED_INX];
        *pInx = MED_WOW_THROUGHPUT_SPEED_INX;
        break;

    case HI_WOW_THRUPUT_SPEED_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[HI_WOW_THROUGHPUT_SPEED_INX];
        *pInx = HI_WOW_THROUGHPUT_SPEED_INX;
        break;
#endif //end for G900
#endif

    case PLATFORM_MED_CAPACITY_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[ PLATFORM_MED_CAPACITY_INX ];
        *pInx = PLATFORM_MED_CAPACITY_INX;
        break;

    case TEXT_AD_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[TEXT_ENTRY_ADS_INX];
        *pInx = TEXT_ENTRY_ADS_INX;
        break;

    case PC_PROXY_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[PC_PROXY_INX];
        *pInx = PC_PROXY_INX;
        break;

//  case PC_PROXY_UI_SUPPORT:

//      *pFeattype = CMOSFeatureTable.fTbl[PC_PROXY_UI_INX];
//        *pInx = PC_PROXY_UI_INX;
//      break;
#endif

//*******************************************
// cases that are only for Janus & G900 (& possibly K700) & Phoenix
#if defined (JANUS) || defined (PHOENIX)
    case PLATFORM_LOW_CAPACITY_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[ PLATFORM_LOW_CAPACITY_INX ];
        *pInx = PLATFORM_LOW_CAPACITY_INX;
        break;

#ifndef K700
    case PLATFORM_HIGH_CAPACITY_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[ PLATFORM_HIGH_CAPACITY_INX ];
        *pInx = PLATFORM_HIGH_CAPACITY_INX;
        break;
#endif
#endif

//*******************************************
// cases that are only for Janus (& possibly K700) & Mega
#if defined (JANUS) || !defined (PHOENIX)   // must be either Janus or G900 or K700 or
                                            // Mega (because Mega is the only one that doesn't define PHOENIX)
    case AUTO_DAYLIGHT_SWITCH_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[AUTO_DAYLIGHT_SWITCH_INX];
        *pInx = AUTO_DAYLIGHT_SWITCH_INX;
        break;

#ifndef K700
    case TRACKING_SVCS_SUPPORT:
        *pFeattype = CMOSFeatureTable.fTbl[ TRACKING_SERVICES_INX ];
        *pInx = TRACKING_SERVICES_INX;
        break;

    case CPC_PKG_SVCS_SUPPORT:
        *pFeattype = CMOSFeatureTable.fTbl[CPC_PACKAGE_SVCS_INX];
        *pInx = CPC_PACKAGE_SVCS_INX;
        break;

    case NLD_TPG_TRACK_SUPPORT:
        *pFeattype = CMOSFeatureTable.fTbl[NLD_TPG_TRACK_INX];
        *pInx = NLD_TPG_TRACK_INX;
        break;

    // Janus doesn't yet actually support the following case but it's needed so it can be determined
    // that the feature is already disabled as a consequence of other calls in this file.
    case RETURN_RECEIPT_FOR_CERT_MAIL:
        *pFeattype = CMOSFeatureTable.fTbl[RET_RECEIPT_FOR_CERT_MAIL_INX];
        *pInx = RET_RECEIPT_FOR_CERT_MAIL_INX;
        break;

    case AUTO_AD_INSCR_SUPPORT:
        *pFeattype = CMOSFeatureTable.fTbl[AUTO_AD_INSCR_INX];
        *pInx = AUTO_AD_INSCR_INX;
        break;
#endif
#endif

//*******************************************
// cases that are only for Mega & Phoenix
#ifndef JANUS
    case EURO_CURRENCY_SUPPORT:

        *pFeattype = CMOSFeatureTable.fTbl[EURO_CURRENCY_SUPPORT_INX];
        *pInx = EURO_CURRENCY_SUPPORT_INX;
        break;
#endif

//*******************************************
// cases that are only for Mega or G900
#if !defined (PHOENIX) || defined (G900)  // must be Mega or G900.  
    case CUST_REF_REQ_FOR_CERT_MAIL:
        *pFeattype = CMOSFeatureTable.fTbl[CUST_REF_REQ_FOR_CERT_MAIL_INX];
        *pInx = CUST_REF_REQ_FOR_CERT_MAIL_INX;
        break;
#endif

#ifndef PHOENIX // Mega only
    case SMART_CLASS_SUPPORT:
    
        *pFeattype = CMOSFeatureTable.fTbl[SMART_CLASS_SUPPORT_INX];
        *pInx = SMART_CLASS_SUPPORT_INX;
        break;
#endif

#if !defined (PHOENIX) || defined (G900)  // must be Mega or G900.
    case REMOTE_REFILL_SUPPORT:
    
        *pFeattype = CMOSFeatureTable.fTbl[REMOTE_REFILL_SUPPORT_INX];
        *pInx = REMOTE_REFILL_SUPPORT_INX;
        break;

    case BUDGET_MANAGER_UPLOAD_SUPPORT:
        *pFeattype = CMOSFeatureTable.fTbl[BUDGET_MANAGER_UPLOAD_INX];
        *pInx = BUDGET_MANAGER_UPLOAD_INX;
        break;

#endif

#ifndef PHOENIX
    case AUTO_METER_REFILL_SUPPORT:
    
        *pFeattype = CMOSFeatureTable.fTbl[AUTO_METER_REFILL_INX];
        *pInx = AUTO_METER_REFILL_INX;
        break;

    case SEBRING_INSERT_INTERFACE_SUPPORT:
    
        *pFeattype = CMOSFeatureTable.fTbl[SEBRING_INSERT_INTERFACE_INX];
        *pInx = SEBRING_INSERT_INTERFACE_INX;
        break;

    case EXT_ACNT_MSGS_SUPPORT:
    
        *pFeattype = CMOSFeatureTable.fTbl[EXT_ACNT_MSGS_INX];
        *pInx = EXT_ACNT_MSGS_INX;
        break;
#endif

        
//*******************************************
// generic default case for everyone
    default:
        *pFeattype = NOT_PURCHASED;
        *pEnabled = FALSE;
        return(FALSE);
    }

    // If we recognized the feature code, but it currently isn't in our feature table,
    // return an error
    if (*pInx >= MAX_FEATURES)
    {
        *pFeattype = NOT_PURCHASED;
        *pEnabled = FALSE;
        return (FALSE);
    }

    switch(*pFeattype)
    {
    case PERMDISBLD:
    case DISBLD:
    case REDISBLD:
        *pEnabled = FALSE;
        break;

    case PERMENBLD:
    case ENBLD:
        *pEnabled = TRUE;
        break;

    default:
        *pFeattype = NOT_PURCHASED;
        *pEnabled = FALSE;
        break;
    }

    return(TRUE);
}


/*****************************************************************/
/*                                                               */
/* FUNCTION NAME        : IsFeaturePackageEnabled                */
/*                                                               */
/* FUNCTION DESCRIPTION : This function retrieves the            */
/*                        enable/disable status of the features. */
/*                                                               */
/* AUTHOR               : S. Peterson / Craig DeFilippo          */
/*                                                               */
/* DATE                 : 8 August 1999                          */
/*                                                               */
/* CALLING SEQUENCE     :                                        */
/*                                                               */
/* PRECONDITIONS        : NONE                                   */
/*                                                               */
/* POSTCONDITIONS       : NONE                                   */
/*                                                               */
/*****************************************************************/

BOOL
IsFeaturePackageEnabled(BOOL *pEnabled,          /* Indicates if the feature is
                                           * enabled or disabled
                                           */

                 const uchar FeatureId,   /* ID of the feature that needs
                                           * to be checked per the codes
                                           * in datdict.h
                                           */

                 uchar *pFeattype,        /* Actual state of the feature per
                                           * the codes in datdict.h
                                           */

                 uchar *pInx)             /* Index of the feature in the
                                           * feature table
                                           */
{
    switch(FeatureId)
    {
    case ALL_SPECIAL_SERVICES:
        if (((CMOSFeatureTable.fTbl[DELIVERY_CONFIRMATION_INX] == ENBLD) || 
             (CMOSFeatureTable.fTbl[DELIVERY_CONFIRMATION_INX] == PERMENBLD)) &&

            ((CMOSFeatureTable.fTbl[CERTIFIED_CONFIRMATION_INX] == ENBLD) || 
             (CMOSFeatureTable.fTbl[CERTIFIED_CONFIRMATION_INX] == PERMENBLD)) &&

            ((CMOSFeatureTable.fTbl[SIGNATURE_CONFIRMATION_INX] == ENBLD) || 
             (CMOSFeatureTable.fTbl[SIGNATURE_CONFIRMATION_INX] == PERMENBLD)))
        {
            *pFeattype = ENBLD;
            *pEnabled = TRUE;
            return (TRUE);
        }
        else
        {
            *pFeattype = DISBLD;
            *pEnabled = FALSE;
            return (FALSE);
        }
//      break;  // don't actually need the break because of the returns right before it.

#if !defined (PHOENIX) || defined (G900)  // must be Mega or G900.  //JY changed.
    case ABACUS_OPTION_PACK_1:
        *pFeattype = CMOSFeatureTable.fTbl[ABACUS_PACK_1_INX];
        *pInx = ABACUS_PACK_1_INX;
        break;

    case ABACUS_OPTION_PACK_2:
        *pFeattype = CMOSFeatureTable.fTbl[ABACUS_PACK_2_INX];
        *pInx = ABACUS_PACK_2_INX;
        break;

    case ABACUS_OPTION_PACK_3:
        *pFeattype = CMOSFeatureTable.fTbl[ABACUS_PACK_3_INX];
        *pInx = ABACUS_PACK_3_INX;
        break;

    case ABACUS_HOST_PACK:
        *pFeattype = CMOSFeatureTable.fTbl[ABACUS_HOST_PACK_INX];
        *pInx = ABACUS_HOST_PACK_INX;
        break;

    case ABACUS_OPTION_PACK_4:
        *pFeattype = CMOSFeatureTable.fTbl[ABACUS_PACK_4_INX];
        *pInx = ABACUS_PACK_4_INX;
        break;
        
    case ABACUS_OPTION_PACK_5:
        *pFeattype = CMOSFeatureTable.fTbl[ABACUS_PACK_5_INX];
        *pInx = ABACUS_PACK_5_INX;
        break;
#endif

    default:
        *pFeattype = NOT_PURCHASED;
        *pEnabled = FALSE;
        return(FALSE);
    }

    switch(*pFeattype)
    {
    case PERMDISBLD:
    case DISBLD:
    case REDISBLD:
        *pEnabled = FALSE;
        break;

    case PERMENBLD:
    case ENBLD:
        *pEnabled = TRUE;
        break;

    default:
        *pFeattype = NOT_PURCHASED;
        *pEnabled = FALSE;
        break;
    }

    return(TRUE);
}


/****************************************************************************/
/*                                                                          */
/* FUNCTION NAME        : IsEuroConversionDone                              */
/*                                                                          */
/* FUNCTION DESCRIPTION : This function determines if the meter has been    */
/*                        converted to a euro meter.                        */ 
/*                                                                          */
/* AUTHOR               : S. Peterson                                       */
/*                                                                          */
/* DATE                 : 8 August 1999                                     */
/*                                                                          */
/* CALLING SEQUENCE     :                                                   */
/*                                                                          */
/* PRECONDITIONS        : None.                                             */
/*                                                                          */
/* POSTCONDITIONS       : None.                                             */
/*                                                                          */
/****************************************************************************/

BOOL
IsEuroConversionDone(BOOL *pConversionDone)
{
#ifndef JANUS
    BOOL    enabled;
    uchar   status;
    uchar   index;

    if (IsFeatureEnabled(&enabled, EURO_CURRENCY_SUPPORT, &status, &index))
    {
        if (status == PERMENBLD)
            *pConversionDone = TRUE;
        else
            *pConversionDone = FALSE;

        return(TRUE);
    }
#endif

    return (FALSE);
}

/****************************************************************************/
/*                                                                          */
/* FUNCTION NAME        : IsEuroCapable                                     */
/*                                                                          */
/* FUNCTION DESCRIPTION : This function determines if the meter is capable  */
/*                        of being converted to a euro meter.               */
/*                                                                          */
/* AUTHOR               : S. Peterson                                       */
/*                                                                          */
/* DATE                 : 8 August 1999                                     */
/*                                                                          */
/* CALLING SEQUENCE     :                                                   */
/*                                                                          */
/* PRECONDITIONS        : None.                                             */
/*                                                                          */
/* POSTCONDITIONS       : None.                                             */
/*                                                                          */
/****************************************************************************/

BOOL
IsEuroCapable(BOOL *pEuroCapable)
{
#ifndef JANUS
    BOOL    enabled;
    uchar   status;
    uchar   index;

    if (IsFeatureEnabled(&enabled, EURO_CURRENCY_SUPPORT, &status, &index))
    {
        if ((status == DISBLD) || (status == PERMENBLD))
            *pEuroCapable = TRUE;
        else
            *pEuroCapable = FALSE;

        return(TRUE);
    }
#endif

    return (FALSE);
}


BOOL
ReturnFeatureState( const uchar FeatureCode, unsigned char *pState )
{
    uchar   featureIndex;
    uchar   currentEnableStatus;
    BOOL    returnValue = FALSE;
    BOOL    enabled;


    /* Retrieve information on the selected feature. */
    if (IsFeatureEnabled(&enabled, FeatureCode, &currentEnableStatus, &featureIndex))
    {
        *pState = CMOSFeatureTable.fTbl[featureIndex];
        returnValue = TRUE;
    }

    return (returnValue);
}


BOOL
ReturnFeaturePackageState( const uchar FeatureCode, unsigned char *pState )
{
    uchar   featureIndex;
    uchar   currentEnableStatus;
    BOOL    returnValue = FALSE;
    BOOL    enabled;


    /* Retrieve information on the selected feature. */

    if (IsFeaturePackageEnabled(&enabled, FeatureCode, &currentEnableStatus, &featureIndex))
    {
        *pState = CMOSFeatureTable.fTbl[featureIndex];
        returnValue = TRUE;
    }

    return (returnValue);
}


/****************************************************************************/
/*                                                                          */
/* FUNCTION NAME        : fnGetEnabledFeatureList                           */
/*                                                                          */
/* FUNCTION DESCRIPTION : This function gets the list of all the enabled    */
/*                        features in the meter.                            */
/*                                                                          */
/* AUTHOR               : S. Peterson                                       */
/*                                                                          */
/* DATE                 : 12 November 2003                                  */
/*                                                                          */
/* CALLING SEQUENCE     :                                                   */
/*                                                                          */
/* PRECONDITIONS        : None.                                             */
/*                                                                          */
/* POSTCONDITIONS       : None.                                             */
/*                                                                          */
/****************************************************************************/

unsigned char fnGetEnabledFeatureList(unsigned short *pwFeatureList)
{

    unsigned char bNumEnabled = 0;
    unsigned char bInx1;
    unsigned char bFeatureIndex;
    unsigned char bFeatureID;
    unsigned char bFeatureType;
    unsigned char bCurrentEnableStatus;
    unsigned char bUSPSSpecialServicesEnabled = DISBLD;
    BOOL fbServicesEnabled = FALSE;
    BOOL enabled;


    // First check if the USPS Services Package is enabled
    if (IsFeaturePackageEnabled(&enabled, ALL_SPECIAL_SERVICES, &bUSPSSpecialServicesEnabled, &bFeatureIndex))
    {
        if (enabled == TRUE)
        {
            fbServicesEnabled = TRUE;
            pwFeatureList[bNumEnabled++] = (FEATURE_PACKAGE_TRANSACTION * 100) + ALL_SPECIAL_SERVICES;
        }
    }

    // Next cover all the bytes in the feature table
    for (bInx1 = 0; bInx1 < MAX_FEATURES; bInx1++)
    {
        bFeatureType = fcTable[bInx1][0];
        bFeatureID = fcTable[bInx1][1];
        if (bFeatureType == FEATURE_TRANSACTION)
        {
            if (IsFeatureEnabled(&enabled, bFeatureID, &bCurrentEnableStatus, &bFeatureIndex))
            {
                if (enabled == TRUE)
                {
                    switch (bFeatureID)
                    {
                    case CERTIFIED_CONF_SUPPORT:
                    case SIGNATURE_CONF_SUPPORT:
                    case DELIVERY_CONF_SUPPORT:
                        if (fbServicesEnabled == FALSE)
                            pwFeatureList[bNumEnabled++] = (bFeatureType * 100) + bFeatureID;
                        break;

                    default:
                        pwFeatureList[bNumEnabled++] = (bFeatureType * 100) + bFeatureID;
                        break;
                    }
                }
            }
        }
        else    // must be a feature package
        {
            if (IsFeaturePackageEnabled(&enabled, bFeatureID, &bCurrentEnableStatus, &bFeatureIndex))
            {
                if (enabled == TRUE)
                    pwFeatureList[bNumEnabled++] = (bFeatureType * 100) + bFeatureID;
            }
        }
    }

    // Now check the features that aren't covered by the bytes in the feature table
#if !defined (PHOENIX) || defined (G900)  // must be Mega or G900.  //JY changed.
    // Determine which max abacus accounts feature code to use
    /*
    unsigned short wNbrOfAccounts = 0;
    unsigned short sMainPCNAccounts;
    unsigned short sSubPCNAccounts;
    unsigned char ucActType, ucMRUActType;

    ucActType = fnGetAccountingType();
    ucMRUActType = fnGetMRUAccountingType();

    if ( (ucActType == ACT_SYS_TYPE_ABACUS) || (ucActType == ACT_SYS_TYPE_HOSTED_ABACUS) ||
         ( ( ((ucActType == ACT_SYS_TYPE_NONE) && (ucMRUActType == ACT_SYS_TYPE_ABACUS)) || 
             ((ucActType == ACT_SYS_TYPE_NONE) && (ucMRUActType == ACT_SYS_TYPE_HOSTED_ABACUS)) ) &&
           (fnAGDCheckAbacusFeaturePackageEnabled()) ) )
    {
//      (void)fnCheckAbacusAcctingNbr(&wNbrOfAccounts);
        wNbrOfAccounts = fnAGDGetPCNMaxAccounts( &sMainPCNAccounts, &sSubPCNAccounts);
        switch (wNbrOfAccounts)
        {
        case 100:
            pwFeatureList[bNumEnabled++] = (FEATURE_TRANSACTION * 100) + ABACUS_ACCOUNTS_100;
            break;
        
        case 300:
            pwFeatureList[bNumEnabled++] = (FEATURE_TRANSACTION * 100) + ABACUS_ACCOUNTS_300;
            break;
        
        case 1000:
            pwFeatureList[bNumEnabled++] = (FEATURE_TRANSACTION * 100) + ABACUS_ACCOUNTS_1000;
            break;

        case 1500:
            pwFeatureList[bNumEnabled++] = (FEATURE_TRANSACTION * 100) + ABACUS_ACCOUNTS_1500;
            break;

        case 2000:
            pwFeatureList[bNumEnabled++] = (FEATURE_TRANSACTION * 100) + ABACUS_ACCOUNTS_2000;
            break;

        case 2500:
            pwFeatureList[bNumEnabled++] = (FEATURE_TRANSACTION * 100) + ABACUS_ACCOUNTS_2500;
            break;

        case 3000:
            pwFeatureList[bNumEnabled++] = (FEATURE_TRANSACTION * 100) + ABACUS_ACCOUNTS_3000;
            break;

        default:
            break;
        }
    }
*/
#endif

    return (bNumEnabled);
}


