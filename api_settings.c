/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    api_settings.c

   DESCRIPTION:    API implementations for settings messages

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------

*************************************************************************/
#include "commontypes.h"
#include "string.h"
#include "cJSON.h"
#include "wsox_tx_queue.h"
#include "wsox_services.h"
#include "networkmonitor.h"
#include "pbos.h"
#include "ctype.h"
#include "osmsg.h"
#include "settingsmgr.h"
#include "api_utils.h"
#include "clock.h"
#include "intellilink.h"
#include "urlencode.h"
#include "datdict.h"
#include "cwrapper.h"
#include "fwrapper.h"
#include "UTILS.h"
#include "bobutils.h"
#include "debugTask.h"

#define __SETTINGS_BASE_PARAMETERS_TABLES__
#include "api_settings.h"

extern const char *api_status_to_string_tbl[];
extern SetupParams CMOSSetupParams;
extern ulong		CMOSTrmMaxFilesToUpload;
extern ulong		CMOSTrmMailPiecesToUpload;
ulong 				trmMAXMailPiecesToUpload;
extern BOOL     	fUploadInProgress;
#define  CRLF   "\r\n"
#define  PROXY_SETTING_TYPE "Type"
#define  BASE_PARAM_SETTING "Setting"
#define  BASE_PARAM_VALUE	"Value"
#define  NOT_INITIALIZED 255

//extern CSDProxyConfig 			globalProxyConfig;
extern uchar 				bVltDecimalPlaces;
extern BOOL  					fLowFundsWarningPending;
extern unsigned long 			lwPsdState;

unsigned char bNumDec = NOT_INITIALIZED;

/* ------------------------------------------------------------------------------------------[on_GetJobSettingsReq]-- */
void on_GetJobSettingsReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetJobSettingsRsp");

    smgr_add_set_fields_to_rsp(rspMsg);

    addEntryToTxQueue(&entry, root, "  on_GetJobSettingsReq: Added GetJobSettingsRsp to tx queue. status=%d" CRLF);
}

/* ----------------------------------------------------------------------------------------[on_ClearJobSettingsReq]-- */
void on_ClearJobSettingsReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    STATUS status;
    char status_str[50];

    cJSON_AddStringToObject(rspMsg, "MsgID", "ClearJobSettingsRsp");

    status = smgr_invalidate_all();
    if (status == NU_SUCCESS)
        sprintf(status_str, "Success");
    else
        sprintf(status_str, "Failure! Status=%d", status);

    cJSON_AddStringToObject(rspMsg, "Status", status_str);
    addEntryToTxQueue(&entry, root, "  on_ClearJobSettingsReq: Added ClearJobSettingsRsp to tx queue. status=%d" CRLF);
}

// Distributor URL processing

// the order of types below has to match order in DIST_URL_TYPE
static const char      *valid_EMDURLTypes [] = { "PROD", "QA" };

static CRACKED_URL crackedDistURL;

static bool validateAPIURL(char *pValue)
{
	bool valid_url = false;

	if (crackUrl(&crackedDistURL, pValue) != NU_SUCCESS)
	{
		return valid_url;
	}

#ifndef ALLOW_NON_SSL
    if (strncmp(crackedDistURL.protocol,"https",5) != 0)
    {
		return valid_url;
    }
#endif
	valid_url = true;

	return valid_url;
}

static bool processEMDURL(DIST_URL_TYPE urlType)
{
	bool valid_url = false;
    char *retStr;

	// Get URL from EMD and validate it
	retStr = (char *) fnFlashGetAsciiStringParm(((urlType == DIST_URL_TYPE_QA) ? ASP_QA_DISTRIBUTOR_URL : ASP_DISTRIBUTOR_URL));
	if ((retStr == NULL_PTR) || (crackUrl(&crackedDistURL, retStr) != NU_SUCCESS))
	{
		return valid_url;
	}

#ifndef ALLOW_NON_SSL
	if (strncmp(crackedDistURL.protocol,"https",5) != 0)
	{
		return valid_url;
	}
#endif

	// Copy values into NVRAM
	fnSetNVRAMDistributorURLSource(DIST_URL_SOURCE_EMD);
	fnSetNVRAMdistributorEMDURLType(urlType);
	fnSetNVRAMdistributorURL(retStr);
	valid_url = true;

	return valid_url;

}

static bool processAPIURL(char *pValue)
{
	bool valid_url = false;

	valid_url = validateAPIURL(pValue);
	if(valid_url == true)
	{
		// Copy values into NVRAM
		fnSetNVRAMDistributorURLSource(DIST_URL_SOURCE_API);
		fnSetNVRAMdistributorURL(pValue);
	}

	return valid_url;

}

static BOOL processDistributorURL(char *pValue)
{
    int  i = 0;
	bool validEMDtype = false;
	bool statURL;

	for(i=0; i<NELEMENTS(valid_EMDURLTypes) ; i++)
	{
		if (0 == NCL_Stricmp(valid_EMDURLTypes[i], pValue))
		{
			validEMDtype = true;
			break;
		}
	}

	if (validEMDtype == true)
	{
		statURL = processEMDURL((DIST_URL_TYPE) i);
	}
	else
	{
		statURL = processAPIURL(pValue);
	}

	return (statURL ? TRUE : FALSE);
}

/* Adjust fixed decimal value using NumDec value */
static unsigned long adjustFINTWithNumDecValue(unsigned long fint, UINT8 numDec)
{
	UINT32 numDecInternal = (UINT32) bVltDecimalPlaces;
	UINT32 i;

// Convert to internal FINT format - this only works when numDecInternal == numDecMax
	for (i = 0; i < (numDecInternal - numDec); i++)
		fint *= 10;

	return fint;
}


required_fields_tbl_t required_fields_tbl_SetBaseParameterReq =
    {
    "SetBaseParameterReq", { 	BASE_PARAM_SETTING
							  , BASE_PARAM_VALUE
                              , NULL
    						}
    };

/******************************************************************************
   FUNCTION NAME: fnUploadTrmBeforeTimeChange
   DESCRIPTION  : Check if time is going backwards and there is any pending transaction to be uploaded.
   PARAMETERS   : setting - the base paramter to be changed. The valid paramter is time zone and DST_Activated
                            newValue - the new value for the base parameter
                            oldValue - the old value for the base parameter
   RETURN       : If time is going backwards and there is pending transaction to upload, return TURE;
                        Else return FALSE. 
   NOTES        : none
******************************************************************************/
static BOOL fnUploadTrmBeforeTimeChange(settings_base_parameters_t setting, short newValue, short oldValue)
{
    BOOL fUploadRequired = FALSE;
    BOOL fTimeBack = FALSE;

    //Time goes backwards when new time zone < old time zone
    if (setting == SETTINGS_BASE_PARAMETERS_TIMEZONE && newValue < oldValue)
        fTimeBack = TRUE;

    //Time goes backwards when DST is enabled with the current DST_Offset is non zero 
    //and then we get a new DST_Activated that disables DST.
    if (setting == SETTINGS_BASE_PARAMETERS_DST_ACTIVATED
        && CMOSSetupParams.ClockOffset.DaylightSToff != 0 
        && oldValue == 1 && newValue == 0)
        fTimeBack = TRUE;

    //Time goes backwards when DST is enabled and get a new DST_Offset that is less than the current offset.
    if (setting == SETTINGS_BASE_PARAMETERS_DST_OFF && newValue < oldValue
        && CMOSSetupParams.ClockOffset.DaylightSTactivated == TRUE)
        fTimeBack = TRUE;

    if (fTimeBack == TRUE)
    {
        if( (trmTRUploadPending() == FALSE) && (CMOSTrmMailPiecesToUpload == 0) && (CMOSTrmMailPiecesPackaged == 0))
        {
            dbgTrace(DBG_LVL_INFO, "No transaction to be uploaded before time change");
            fUploadRequired = FALSE;
        }
        else
        {
            //if there is any pending transaction, don't allow time going backwards 
            dbgTrace(DBG_LVL_INFO, "Please upload transaction before time change");
            fUploadRequired = TRUE;
        }
    }

    return fUploadRequired;
}


static BOOL SetBaseParameter(settings_base_parameters_t setting, char *pValue, api_status_t *pErrStat)
{
	BOOL msgStat = TRUE;
	char buf[256];
	char *pTemp = NULL;
	short script;
	short oldtzValue, newtzValue;
	unsigned char  oldValue, newValue;
    unsigned char oldDaylightSTactivated, DaylightSTactivated;
    long oldFundsWarn, newFundsWarn;
    unsigned char oldNumDec = 0, newNumDec = 0;
    unsigned char oldOOBTest = 0, newOOBTest = 0;

	sprintf(buf, "SetBaseParam: %s=%s", settings_base_parameters_to_string_tbl[setting], pValue);
	fnDumpStringToSystemLog(buf);

	switch (setting)
	{
		case SETTINGS_BASE_PARAMETERS_DIST_URL:
			msgStat = processDistributorURL(pValue);
		break;

		case SETTINGS_BASE_PARAMETERS_TIMEZONE:
			oldtzValue = CMOSSetupParams.ClockOffset.TimeZone;
			newtzValue = (short) strtol(pValue, &pTemp, 10);
			if(pTemp != pValue)
			{
				fnGetPsdState(&lwPsdState);
				if(lwPsdState == eIPSDSTATE_INCOMMUNICADO)
				{
					msgStat = FALSE;
					dbgTrace(DBG_LVL_INFO, "SETTINGS_BASE_PARAMETERS_TIMEZONE: No PSD");
				}
				else
				{
					if(abs(newtzValue) <= MAX_TMZONE_VAL)
					{
					    if( fnUploadTrmBeforeTimeChange(setting, newtzValue, oldtzValue))
					    {               
					        msgStat = FALSE;
					        *pErrStat = API_STATUS_TRM_UPLOAD_REQ;
					        dbgTrace(DBG_LVL_INFO, "SETTINGS_BASE_PARAMETERS_TIMEZONE: reject new time zone");
					    }
					    else 
					    {
    						CMOSSetupParams.ClockOffset.TimeZone = newtzValue;                           

    						script = SCRIPT_IPSD_INIT_CLOCK;

    						msgStat = OSSendIntertask( SCM, WSRXSRVTASK, 0, PERFORM_SCRIPT, &script, sizeof(script)); //Make bob send it to the PSD
    						if (msgStat == FALSE)
    						{
    							CMOSSetupParams.ClockOffset.TimeZone = oldtzValue;
    						}
    						else
    						{
    							//Wait for few moments and set mid night event
    							NU_Sleep(20);
    							SetClockMidnight();
    						}
					    }
					}
					else
					{
						msgStat = FALSE;
					}
				}
			}
			else
			{
				msgStat = FALSE;
			}

		break;

		case SETTINGS_BASE_PARAMETERS_DST_OFF:
			oldValue = (unsigned char)CMOSSetupParams.ClockOffset.DaylightSToff;
			newValue = strtol(pValue, &pTemp, 10);
			if(pTemp != pValue)
			{
				if(newValue == oldValue)
				{
					msgStat = FALSE;
				}
				else if (abs(newValue) == MAX_DAYLIGHT_VAL || abs(newValue) == 0)
				{
					if( fnUploadTrmBeforeTimeChange(setting, newValue, oldValue))
					{
						msgStat = FALSE;
						*pErrStat = API_STATUS_TRM_UPLOAD_REQ;
						dbgTrace(DBG_LVL_INFO, "SETTINGS_BASE_PARAMETERS_DST_OFF: reject new DST_OFF");
					}
					else
					{
						CMOSSetupParams.ClockOffset.DaylightSToff = newValue;

						script = SCRIPT_IPSD_INIT_CLOCK;

						msgStat = OSSendIntertask( SCM, WSRXSRVTASK, 0, PERFORM_SCRIPT, &script, sizeof(script)); //Make bob send it to the PSD
						if (msgStat == FALSE)
						{
							CMOSSetupParams.ClockOffset.DaylightSToff = oldValue;
						}
						else
						{
							//Wait for few moments and set mid night event
							NU_Sleep(20);
							SetClockMidnight();
						}
					}
				}
				else
				{
					msgStat = FALSE;
				}
			}
			else
			{
				msgStat = FALSE;
			}

		break;

		case SETTINGS_BASE_PARAMETERS_MAX_FILES_TO_UPLOAD:
			if(fUploadInProgress == FALSE)
			{

				long newValue = (long) strtol(pValue, &pTemp, 10);
				if(pTemp != pValue)
				{
					if(abs(newValue) <= MAX_FILE_LIST_EX)
					{
						CMOSTrmMaxFilesToUpload = newValue;
						dbgTrace(DBG_LVL_INFO, "SETTINGS_BASE_PARAMETERS_MAX_FILES_TO_UPLOAD: set value %d", CMOSTrmMaxFilesToUpload);
					}
					else
					{
						msgStat = FALSE;
					}
				}
				else
				{
					msgStat = FALSE;
				}
			}
			else
				dbgTrace(DBG_LVL_INFO, "SETTINGS_BASE_PARAMETERS_MAX_FILES_TO_UPLOAD: Upload in progress try some other time");

			break;
		case SETTINGS_BASE_PARAMETERS_MAX_MPS_TO_UPLOAD:
			if(fUploadInProgress == FALSE)
			{

				long newValue = (long) strtol(pValue, &pTemp, 10);
				if(pTemp != pValue)
				{
					if( (abs(newValue) <= MAX_MP_TO_UPLOAD) &&
						(abs(newValue)/CFG_NUM_RECORDS <= MAX_FILE_LIST_EX) )
					{
						trmMAXMailPiecesToUpload = newValue;
						dbgTrace(DBG_LVL_INFO, "SETTINGS_BASE_PARAMETERS_MAX_MPS_TO_UPLOAD: set value %d", trmMAXMailPiecesToUpload);
					}
					else
					{
						msgStat = FALSE;
						dbgTrace(DBG_LVL_INFO, "SETTINGS_BASE_PARAMETERS_MAX_MPS_TO_UPLOAD: Invalid Value %d", newValue);
					}
				}
				else
				{
					msgStat = FALSE;
				}
			}
			else
				dbgTrace(DBG_LVL_INFO, "SETTINGS_BASE_PARAMETERS_MAX_MPS_TO_UPLOAD: Upload in progress try some other time");

			break;
        case SETTINGS_BASE_PARAMETERS_DST_ACTIVATED:
            oldDaylightSTactivated = CMOSSetupParams.ClockOffset.DaylightSTactivated;
            DaylightSTactivated = strtol(pValue, &pTemp, 10);
            if(pTemp != pValue)
            {
                if(DaylightSTactivated < 2)
                {
                     if(fnUploadTrmBeforeTimeChange(setting, DaylightSTactivated, oldDaylightSTactivated))
                     {
                         msgStat = FALSE;
                         *pErrStat = API_STATUS_TRM_UPLOAD_REQ;
                         dbgTrace(DBG_LVL_INFO, "SETTINGS_BASE_PARAMETERS_DST_ACTIVATED: reject DST change from %d to %d", oldDaylightSTactivated, DaylightSTactivated);                  
                     }
                     else
                     {
                         CMOSSetupParams.ClockOffset.DaylightSTactivated = DaylightSTactivated;
                         dbgTrace(DBG_LVL_INFO, "SETTINGS_BASE_PARAMETERS_DST_ACTIVATED: set value %d", DaylightSTactivated);

                         //Wait for few moments and set mid night event
                         NU_Sleep(20);
                         SetClockMidnight();
                     }
                }
                else
                {
                    msgStat = FALSE;
                    CMOSSetupParams.ClockOffset.DaylightSTactivated = oldDaylightSTactivated;
                }
            }
            else
            {
                msgStat = FALSE;
            }

        break;

        case SETTINGS_BASE_PARAMETERS_LOW_FUNDS_WARN:
        	oldFundsWarn = CMOSSetupParams.lwLowFundsWarning;
        	newFundsWarn = strtol(pValue, &pTemp, 10);
            if(pTemp != pValue)
            {
            	if(newFundsWarn < 0)
            	{
            		msgStat = FALSE;
            		CMOSSetupParams.lwLowFundsWarning = oldFundsWarn;

            	}
            	else
            	{

            		UINT8       pDescRegValue[SPARK_MONEY_SIZE];
            		if(bNumDec == NOT_INITIALIZED)
            			bNumDec = bVltDecimalPlaces;
            		CMOSSetupParams.lwLowFundsWarning = adjustFINTWithNumDecValue(newFundsWarn, bNumDec);

            		if(fnBobGetDescendingReg(pDescRegValue) == TRUE)
            		{
            			if ((double)(fnCMOSSetupGetCMOSSetupParams()->lwLowFundsWarning) >= fnBinFive2Double(pDescRegValue))
            			{
            				fLowFundsWarningPending = TRUE;
            			}
            			else
            				fLowFundsWarningPending = FALSE;
            		}
            		dbgTrace(DBG_LVL_INFO, "SetBaseParameter :  Internal value CMOSSetupParams.lwLowFundsWarning = %d", CMOSSetupParams.lwLowFundsWarning);
            		dbgTrace(DBG_LVL_INFO, "SETTINGS_BASE_PARAMETERS_LOW_FUNDS_WARN: set value %d", newFundsWarn);

            	}

            }
            else
            {
                msgStat = FALSE;
            }

        break;

        case SETTINGS_BASE_PARAMETERS_NUM_DECIMALS:
        	if(bNumDec == NOT_INITIALIZED)
            	bNumDec = bVltDecimalPlaces;
        	oldNumDec = bNumDec;
        	newNumDec = strtol(pValue, &pTemp, 10);
            if(pTemp != pValue)
            {
           		if(newNumDec != oldNumDec){
           			bNumDec = newNumDec;
           			dbgTrace(DBG_LVL_INFO, "SETTINGS_BASE_PARAMETERS_NUM_DECIMALS: set value %d", newNumDec);
           		}
           		else
           		{
           			msgStat = FALSE;
           			bNumDec = oldNumDec;
           		}
            }
            else
            {
                msgStat = FALSE;
            }

            break;

        case SETTINGS_BASE_PARAMETERS_OOB_TEST_PATTERN:
        	oldOOBTest = fnCMOSSetupGetCMOSDiagnostics()->ucWhyDoingOOB;
        	newOOBTest = strtol(pValue, &pTemp, 10);
            if(pTemp != pValue)
            {
           		if(newOOBTest != oldOOBTest){
           			fnCMOSSetupGetCMOSDiagnostics()->ucWhyDoingOOB = newOOBTest;
           			dbgTrace(DBG_LVL_INFO, "SETTINGS_BASE_PARAMETERS_OOB_TEST_PATTERN: set value %d", newOOBTest);
           		}
           		else
           		{
           			msgStat = FALSE;
           			fnCMOSSetupGetCMOSDiagnostics()->ucWhyDoingOOB = oldOOBTest;
           		}
            }
            else
            {
                msgStat = FALSE;
            }

            break;

        default:
		msgStat = FALSE;
		break;
	}

	return(msgStat);
}


static BOOL getBaseParameterSetting(cJSON *root, cJSON *invalid, settings_base_parameters_t *pSetting)
{
	int  i = 0;
	char *pTemp;
	BOOL valid_mode = FALSE;

	pTemp = cJSON_GetObjectItem(root, BASE_PARAM_SETTING)->valuestring;
	for(i=0; i<NELEMENTS(settings_base_parameters_to_string_tbl) ; i++)
	{
		if (0 == NCL_Stricmp(settings_base_parameters_to_string_tbl[i], pTemp))
		{
			valid_mode = TRUE;
			break;
		}
	}

	if (valid_mode == TRUE)
	{
		*pSetting = i;
	}
	else
	{
		//Add invalid item to invalid list
		cJSON_AddItemToArray(invalid, CreateStringItem(BASE_PARAM_SETTING, pTemp));
	}

	return valid_mode;
}


/* ------------------------------------------------------------------------------------------[on_SetBaseParameterReq]-- */
void on_SetBaseParameterReq(UINT32 handle, cJSON *root)
{
    settings_base_parameters_t setting = 0;
	BOOL msgStat = TRUE;
	bool required = FALSE;
	char *pTemp;
	cJSON *rspMsg = cJSON_CreateObject();
	cJSON *invalid = cJSON_CreateArray();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    api_status_t errStat = API_STATUS_SUCCESS;

	required = RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_SetBaseParameterReq);
    if (required == TRUE)
    {
		msgStat = getBaseParameterSetting(root, invalid, &setting);

		if(msgStat == TRUE)
		{
			pTemp = cJSON_GetObjectItem(root, BASE_PARAM_VALUE)->valuestring;
			msgStat = SetBaseParameter(setting, pTemp, &errStat);

			if(msgStat == FALSE)
			{
				//Add invalid item to invalid list
				cJSON_AddItemToArray(invalid, CreateStringItem(BASE_PARAM_VALUE, pTemp));
			}
		}
	}
	else
	{
		msgStat = FALSE;
	}

	if(msgStat == TRUE)
	{
		cJSON_Delete(invalid);
		generateResponse(&entry, rspMsg, "SetBaseParameter", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
	}
	else
	{
		if(required == TRUE)
		{
			cJSON_AddItemToObject(rspMsg, "Invalid", invalid);
		}


		if(errStat != API_STATUS_SUCCESS)           
			generateResponse(&entry, rspMsg, "SetBaseParameter", api_status_to_string_tbl[errStat], root);
		else
			generateResponse(&entry, rspMsg, "SetBaseParameter", api_status_to_string_tbl[API_STATUS_NOT_ACCEPTED], root);
	}

}


/* ------------------------------------------------------------------------------------------[on_GetBaseParameterReq]-- */
void on_GetBaseParameterReq(UINT32 handle, cJSON *root)
{

    settings_base_parameters_t setting = 0;
	char buf[256];
	cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetBaseParameterRsp");
	for(setting=0; setting<NELEMENTS(settings_base_parameters_to_string_tbl) ; setting++)
	{

		switch(setting){

			case SETTINGS_BASE_PARAMETERS_DIST_URL:
			{
				cJSON_AddStringToObject(rspMsg, settings_base_parameters_to_string_tbl[setting], CMOSNetworkConfig.distributorUrl );
			}
			break;
			case SETTINGS_BASE_PARAMETERS_TIMEZONE:
			{
				memset(buf, 0x0, sizeof(buf));
				NCL_Itoa(CMOSSetupParams.ClockOffset.TimeZone, buf, 10);
				cJSON_AddStringToObject(rspMsg, settings_base_parameters_to_string_tbl[setting], buf );
			}
			break;
			case SETTINGS_BASE_PARAMETERS_DST_OFF:
			{
				memset(buf, 0x0, sizeof(buf));
				NCL_Itoa(CMOSSetupParams.ClockOffset.DaylightSToff, buf, 10);
				cJSON_AddStringToObject(rspMsg, settings_base_parameters_to_string_tbl[setting], buf );

			}
			break;
			case SETTINGS_BASE_PARAMETERS_MAX_FILES_TO_UPLOAD:
			{
				memset(buf, 0x0, sizeof(buf));
				NCL_Itoa(CMOSTrmMaxFilesToUpload, buf, 10);
				cJSON_AddStringToObject(rspMsg, settings_base_parameters_to_string_tbl[setting], buf );

			}
			break;
			case SETTINGS_BASE_PARAMETERS_MAX_MPS_TO_UPLOAD:
			{
				memset(buf, 0x0, sizeof(buf));
				NCL_Itoa(trmMAXMailPiecesToUpload, buf, 10);
				cJSON_AddStringToObject(rspMsg, settings_base_parameters_to_string_tbl[setting], buf );

			}
			break;
			case SETTINGS_BASE_PARAMETERS_DST_ACTIVATED:
			{
				memset(buf, 0x0, sizeof(buf));
				NCL_Itoa(CMOSSetupParams.ClockOffset.DaylightSTactivated, buf, 10);
				cJSON_AddStringToObject(rspMsg, settings_base_parameters_to_string_tbl[setting], buf );

			}
			break;
			case SETTINGS_BASE_PARAMETERS_LOW_FUNDS_WARN:
			{
				unsigned long oldLowFundsWarn = CMOSSetupParams.lwLowFundsWarning;

				dbgTrace(DBG_LVL_INFO, "GetBaseParameter :  Internal value CMOSSetupParams.lwLowFundsWarning = %d", CMOSSetupParams.lwLowFundsWarning);
				memset(buf, 0x0, sizeof(buf));
				if(bNumDec == NOT_INITIALIZED)
            		bNumDec = bVltDecimalPlaces;
				if(bNumDec == 2)
					oldLowFundsWarn = CMOSSetupParams.lwLowFundsWarning/10;
				else if(bNumDec == 1)
					oldLowFundsWarn = CMOSSetupParams.lwLowFundsWarning/100;
				else if(bNumDec == 0)
					oldLowFundsWarn = CMOSSetupParams.lwLowFundsWarning/1000;

				NCL_Ultoa(oldLowFundsWarn, buf, 10);
				cJSON_AddStringToObject(rspMsg, settings_base_parameters_to_string_tbl[setting], buf );

			}
			break;
			case SETTINGS_BASE_PARAMETERS_NUM_DECIMALS:
			{
				if(bNumDec == NOT_INITIALIZED)
            		bNumDec = bVltDecimalPlaces;
				memset(buf, 0x0, sizeof(buf));
				NCL_Itoa(bNumDec, buf, 10);
				cJSON_AddStringToObject(rspMsg, settings_base_parameters_to_string_tbl[setting], buf );


			}
			break;
			case SETTINGS_BASE_PARAMETERS_OOB_TEST_PATTERN:
			{
				memset(buf, 0x0, sizeof(buf));
				NCL_Itoa(fnCMOSSetupGetCMOSDiagnostics()->ucWhyDoingOOB, buf, 10);
				cJSON_AddStringToObject(rspMsg, settings_base_parameters_to_string_tbl[setting], buf );


			}
			break;

			default:
				break;
		}

	}

	addEntryToTxQueue(&entry, root, "  GetBaseParameterReq: Added GetBaseParameterRsp to tx queue. status=%d" CRLF);
}


/* ------------------------------------------------------------------------------------------[on_SetProxyReq]-- */
#define PROXY_TYPE_HTTP			"HTTP"
#define PROXY_TYPE_HTTPS		"HTTPS"
#define PROXY_TRUE				"True"
#define PROXY_FALSE				"False"
#define PROXY_KEY_TYPE			"Type"
#define PROXY_KEY_PORT			"Port"
#define PROXY_KEY_ENABLED		"Enabled"
#define PROXY_KEY_ADDRESS		"Address"
#define PROXY_KEY_USERNAME		"Username"
#define PROXY_KEY_PASSWORD 		"Password"
#define PROXY_KEY_EXCLUSIONS	"Exclusions"
required_fields_tbl_t required_fields_tbl_SetProxyReq =
    {
//    "SetProxyReq", { 	PROXY_SETTING_TYPE, PROXY_KEY_PORT, PROXY_KEY_ADDRESS, PROXY_KEY_USERNAME, PROXY_KEY_PASSWORD, NULL
    "SetProxyReq", { 	PROXY_SETTING_TYPE, PROXY_KEY_PORT, PROXY_KEY_ADDRESS, NULL
    						}
    };
void on_SetProxyReq(UINT32 handle, cJSON *root)
{
    cJSON           *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    BOOL             fValidUrl = TRUE;

    cJSON_AddStringToObject(rspMsg, "MsgID", "SetProxyRsp");

	cJSON *pTyp = cJSON_GetObjectItem(root, PROXY_KEY_TYPE) ;
	cJSON *pEnb = cJSON_GetObjectItem(root, PROXY_KEY_ENABLED) ;
	cJSON *pAdd = cJSON_GetObjectItem(root, PROXY_KEY_ADDRESS) ;
	cJSON *pUsr = cJSON_GetObjectItem(root, PROXY_KEY_USERNAME) ;
	cJSON *pPwd = cJSON_GetObjectItem(root, PROXY_KEY_PASSWORD) ;
	cJSON *pExc = cJSON_GetObjectItem(root, PROXY_KEY_EXCLUSIONS) ;


    CSDProxyProtocolSettings *pTmp = NULL ;
    if (pTyp && pAdd)
    {
    	// if we have some allocated memory, clean it up first....
    	if(globalProxyConfig.ExclusionUrl)
    		NU_Deallocate_Memory((VOID *)(globalProxyConfig.ExclusionUrl));

    	memset(&globalProxyConfig, 0, sizeof(CSDProxyConfig));
    	if(stricmp(PROXY_TYPE_HTTP, cJSON_GetObjectItem(root, PROXY_SETTING_TYPE)->valuestring) == 0)
		{
    		pTmp = &globalProxyConfig.Http;
		}
    	else if(stricmp(PROXY_TYPE_HTTPS, cJSON_GetObjectItem(root, PROXY_SETTING_TYPE)->valuestring) == 0)
		{
    		pTmp = &globalProxyConfig.Https;
		}

    	if(pTmp)
    	{
    		//
    		pTmp->uwPort = cJSON_GetObjectItem(root, PROXY_KEY_PORT)->valueint ;

    		if(urlToAddress(pAdd->valuestring, pTmp->pAddress, PROXY_ADDRESS_SIZE) != NU_SUCCESS)
    		{
    			// we have an unusable proxy address, it could not be resolved
    			dbgTrace(DBG_LVL_INFO, "on_SetProxyReq failed to resolve supplied url %s\r\n", pAdd->valuestring);
                fValidUrl = FALSE;
            }

    		if(pUsr)
    		{
    			strncpy(pTmp->pUsername, pUsr->valuestring, PROXY_NAME_PWD_SIZE);
    		}
    		else
    		{
    			pTmp->pUsername[0] = 0;
    		}
    		if(pPwd)
    		{
    			strncpy(pTmp->pPassword, pPwd->valuestring, PROXY_NAME_PWD_SIZE);
    		}
    		else
    		{
    			pTmp->pPassword[0] = 0;
    		}
    		if(pEnb)
    			globalProxyConfig.fEnabled = (stricmp(pEnb->valuestring, PROXY_TRUE) == 0);
    		else
    			globalProxyConfig.fEnabled = TRUE ;

    		if (fValidUrl)
    			cJSON_AddStringToObject(rspMsg, "Status", "Accepted");
    		else
    			cJSON_AddStringToObject(rspMsg, "Status", "Not Accepted");
    	}

    	// handle exclusion list if present...
    	INT iExclusionSize = 0;
    	if(pExc)
    		iExclusionSize = cJSON_GetArraySize(pExc);
    	globalProxyConfig.ExclusionCount = 0;
    	if(iExclusionSize > 0)
    	{
    		// allocate memory for our array
    		NU_MEMORY_POOL *pSys, *pSysUnCache;
    		if(NU_System_Memory_Get(&pSys, &pSysUnCache) == NU_SUCCESS)
    		{
    			STATUS status = NU_Allocate_Memory(pSys, (VOID **)&(globalProxyConfig.ExclusionUrl), sizeof(CSDUrl)*iExclusionSize, NU_NO_SUSPEND);
    			if(status == NU_SUCCESS)
    			{
    				globalProxyConfig.ExclusionCount = iExclusionSize;
    				for(int iPass=0 ; iPass < iExclusionSize; iPass++)
    				{
    					cJSON *cEle = cJSON_GetArrayItem(pExc, iPass);
    					if(cEle && cEle->valuestring && (strlen(cEle->valuestring) < CSDPROXY_ADDRESS_SIZE))
    					{
    						// The tablet seems to include leading (and maybe trailing) spaces in the string provided, we will trim...
    						char *pStart 	= skipSpaces(cEle->valuestring);
    						char *pEnd 		= skipNonspaces(pStart) ;
    						size_t szLen 	= (size_t)((UINT)pEnd - (UINT)pStart);
    						// the trailing space is the awkward one,
    						if(szLen < CSDPROXY_ADDRESS_SIZE)
    						{
    							// strncpy will not add a terminating space if the size param is met before end of source string...
    							strncpy(globalProxyConfig.ExclusionUrl[iPass].url, pStart, szLen);
    							// remove trailing spaces
    							globalProxyConfig.ExclusionUrl[iPass].url[szLen] = 0;
    						}
    					}
    				}
    			}
    		}
    	}
    }
    else if(pEnb)
    {
		globalProxyConfig.fEnabled = (stricmp(pEnb->valuestring, PROXY_TRUE) == 0);
		if(globalProxyConfig.fEnabled == FALSE)
		{
			// special handling if disabled, clear out the whole array
			memset(&globalProxyConfig, 0, sizeof(globalProxyConfig));
		}
		else
		{
			// this is an odd situation found by the tablet team, if you send an incomplete setProxy message but include the enable key set to true
			// you could end up here so we need to capture the issue and send a status message indicating data is missing.
			cJSON_AddStringToObject(rspMsg, "Status", "Rejected - missing data");
		}
    }
    else
		cJSON_AddStringToObject(rspMsg, "Status", "Rejected - missing data");

    addEntryToTxQueue(&entry, root, "  SetProxyReq: Added SetProxyRsp to tx queue. status=%d" CRLF);
}

/* ------------------------------------------------------------------------------------------[urlToAddress]-- */
// return true if we resolve the dns or the user supplied an ip address, return false
// if the DNS lookup failed.
STATUS urlToAddress(char *url, char*adds, size_t szAdds)
{
	STATUS	status = NU_SUCCESS;
	BOOL 	bIsIP  = TRUE ;
	NU_HOSTENT *hentry;
	UINT8 *addr_list;

    //chekc if url is the dotted quad
    bIsIP = isDottedQuad(url) ;

	// If it is not an ip try looking it up using whatever DNS we have been given.
	if(!bIsIP)
	{
		hentry = NU_Get_IP_Node_By_Name(url, NU_FAMILY_IP, 0, &status);
		if(status == NU_SUCCESS)
		{
			// take the first found value
			addr_list = (UINT8 *)(*hentry->h_addr_list);
			snprintf(adds, szAdds, "%d.%d.%d.%d", addr_list[0],
												addr_list[1],
												addr_list[2],
												addr_list[3]);
			debugDisplayOnly("\r\nDNS Lookup found IP %s for URL %s\r\n", adds, url);
			NU_Free_Host_Entry(hentry) ;
		}
	}
	else
	{
		// if it is an IP we copy it straight to the output string...
		strncpy(adds, url, szAdds);
	}

	return status;
}


/* ------------------------------------------------------------------------------------------[on_GetProxyReq]-- */
void on_GetProxyReq(UINT32 handle, cJSON *root)
{
    cJSON           *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetProxyRsp");

    cJSON_AddStringToObject(rspMsg, "Enabled", globalProxyConfig.fEnabled ? "TRUE" : "FALSE");

    if(globalProxyConfig.fEnabled)
    {
    	if(globalProxyConfig.Http.uwPort > 0)
    	{
    	    cJSON_AddStringToObject(rspMsg, PROXY_SETTING_TYPE, PROXY_TYPE_HTTP);
    	    cJSON_AddNumberToObject(rspMsg, PROXY_KEY_PORT, 	globalProxyConfig.Http.uwPort);
    	    cJSON_AddStringToObject(rspMsg, PROXY_KEY_ADDRESS, 	globalProxyConfig.Http.pAddress);
    	    cJSON_AddStringToObject(rspMsg, PROXY_KEY_USERNAME, globalProxyConfig.Http.pUsername);
    	    cJSON_AddStringToObject(rspMsg, PROXY_KEY_PASSWORD, globalProxyConfig.Http.pPassword);
    	}
    	else if(globalProxyConfig.Https.uwPort > 0)
    	{
    	    cJSON_AddStringToObject(rspMsg, PROXY_SETTING_TYPE, PROXY_TYPE_HTTPS);
    	    cJSON_AddNumberToObject(rspMsg, PROXY_KEY_PORT, 	globalProxyConfig.Https.uwPort);
    	    cJSON_AddStringToObject(rspMsg, PROXY_KEY_ADDRESS, 	globalProxyConfig.Https.pAddress);
    	    cJSON_AddStringToObject(rspMsg, PROXY_KEY_USERNAME, globalProxyConfig.Https.pUsername);
    	    cJSON_AddStringToObject(rspMsg, PROXY_KEY_PASSWORD, globalProxyConfig.Https.pPassword);
    	}
    	else
    	    cJSON_AddStringToObject(rspMsg, "Error", "Incomplete settings, no port value");

    }

    // handle exclusion array if we have one
    if(globalProxyConfig.ExclusionCount > 0)
    {
    	cJSON *pArray = cJSON_CreateArray1();
    	if(pArray)
    	{
    		for(int iPass=0 ; iPass < globalProxyConfig.ExclusionCount ; iPass++)
    		{
    			cJSON_AddItemToArray(pArray, cJSON_CreateString1(globalProxyConfig.ExclusionUrl[iPass].url));
    		}
    	}
    	cJSON_AddItemToObject(rspMsg, PROXY_KEY_EXCLUSIONS, pArray);
    }

    addEntryToTxQueue(&entry, root, "  MemoryTestReq: Added SetProxyRsp to tx queue. status=%d" CRLF);
}

/* -----------------------------------------------------------------------------------------------------------[EOF]-- */
