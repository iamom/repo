/**********************************************************************
 PROJECT    :   Horizon CSD
 MODULE     :   systask.c

 DESCRIPTION:
    System Controller Task function and misc. support functions.

* ----------------------------------------------------------------------
*               Copyright (c) 2016 Pitney Bowes Inc.
*                        37 Executive Dr
*                      Danbury, CT 06810
* ----------------------------------------------------------------------
*************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "posix.h"

#include "pbos.h"
#include "bobutils.h"
#include "bob.h"
#include "cm.h"
#include "Clock.h"
#include "cmpublic.h"
#include "custdat.h"
#include "cwrapper.h"
#include "datdict.h"
#include "dcapi.h"
#include "download.h"   // fnDL_PurgeFFSFilesListedInArchive(), fnDL_ExtractFilesFromArchive()
#include "nu_networking.h"
#include "pbptask.h"
#include "errcode.h"
#include "imageerrs.h"
#include "features.h"
#include "global.h"
#include "grapext.h"
#include "HAL_AM335x_I2C.h"
#include "i2c_usb_utilities.h"
#include "hal.h"
#include "ipsd.h"
#include "intellilink.h"
#include "lcd.h"
#include "lcdirect.h"
#include "lcdutils.h"
//#include "oipreset.h"
#include "oi_intellilink.h"
#include "oit.h"
#include "nucleus.h"
#include "nucleus_gen_cfg.h"
#include "picklist.h"
#include "pbos.h"
#include "pbplatform.h"
#include "rateadpt.h"
#include "sys.h"
#include "SysCm.h"
#include "syspriv.h"
#include "networking/externs.h"
#include "services/runlevel_init.h"
#include "nu_usb.h"
#include "MCPCfg.h"
#include "download.h"
#include "cJSON.h"
#include "version.h"
#include "report1.h"
#include "megaemd.h"
#include "pbtarget.h"
#include "ossetup.h"
#include "fram.h"
#include "debugTask.h"
#include "networkmonitor.h"
#include "api_status.h"
#include "api_diags.h"
#include "trmstrg.h"
#include "sslinterface.h"
#include "dhcpsusb.h"
#include "counters/fram_log.h"
#define THISFILE "systask.c"

#ifdef HCC_FS
#include "../hcc/src/api/api_fat.h"
#include "../hcc/src/api/api_fat_test.h"
#endif


//#define NO_IBUTTON        1   /* if not defined, ibutton is assumed to be there if you have a printer */

// Typedef for the plainest type of function: takes no arguments, returns nothing.
typedef void (* T_FUNCPTR) (void);



/* state transition functions */
static void fnsPwrComplete(SYS_STATE *pNextState);
static void fnsPwrCompleteMfg(SYS_STATE *pNextState);
static void fnsOITGoMeterErrorMode(SYS_STATE *pNextState);
static void fnsGoMeterErrorEnblEDM(SYS_STATE *pNextState);
static void fnsOITSendNextMode(SYS_STATE *pNextState);
static void fnsPrepIndiciaMode(SYS_STATE *pNextState);
static void fnsPrepIndiciaMode_MailRun(SYS_STATE *pNextState);  // Howell
static void fnsPrepIndiciaModeSleep(SYS_STATE *pNextState);
static void fnsPrepIndiciaModeLogEnbl(SYS_STATE *pNextState);
static void fnsPrepPermitMode(SYS_STATE *pNextState);
static void fnsPrepPermitMode_MailRun(SYS_STATE *pNextState);  // Howell
static void fnsPrepPermitModeSleep(SYS_STATE *pNextState);
static void fnsPrepPermitModeLogEnbl(SYS_STATE *pNextState);
static void fnsPrepReportMode(SYS_STATE *pNextState);
static void fnsPrepReportModeSleep(SYS_STATE *pNextState);
static void fnsPrepReportModeLogEnbl(SYS_STATE *pNextState);
static void fnsPrepTestPatternMode(SYS_STATE *pNextState);
static void fnsPrepTestPatternModeSleep(SYS_STATE *pNextState);
static void fnsPrepSealOnlyMode(SYS_STATE *pNextState);
static void fnsPrepSealOnlyMode_MailRun(SYS_STATE *pNextState);
static void fnsPrepSealOnlyModeSleep(SYS_STATE *pNextState);
static void fnsPrepSealOnlyModeLogEnbl(SYS_STATE *pNextState);
static void fnsPrepNotReadyModeSleep(SYS_STATE *pNextState);
static void fnsDoNothing(SYS_STATE *pNextState);
static void fnsRun1stMailpiece(SYS_STATE *pNextState);
static void fnsRunNextMailpiece(SYS_STATE *pNextState);
//static void fnsStageMailpiece(SYS_STATE *pNextState);
//static void fnsWaitStageMailpiece(SYS_STATE *pNextState);
static void fnsCMDisable(SYS_STATE *pNextState);
static void fnsCMSleep(SYS_STATE *pNextState);
static void fnsCMDisableLogEnbl(SYS_STATE *pNextState);
static void fnsRejectRunRequest(SYS_STATE *pNextState);
static void fnsStartMailRun(SYS_STATE *pNextState);
static void fnsOITGoDiagnosticsMode (SYS_STATE *pNextState);
static void fnsOITSendDiagnostics(SYS_STATE *pNextState);
static void fnsPrintTapeIndicia(SYS_STATE *pNextState);
static void fnsEDMResendMode(SYS_STATE *pNextState);
static void fnsPMCQuickPause(SYS_STATE *pNextState);
static void fnsOITAttemptEnable(SYS_STATE *pNextState);
static void fnsAbCancelPrintJob(SYS_STATE *pNextState);
static void fnsHandleMailJobComplete(SYS_STATE *pNextState);

/* powerup sequence functions */
static void fnInitEmd( void );
static void fnpwSystemInitialize( void );
static void fnpwSystemInitialize2( void );
static void fnpwSystemInitialize3( void );
static void fnpwSystemInitialize4( void );
static void fnpwSystemInitialize5( void );
static void fnpwFinishPowerup( void );

/* private core functions */
static void fnProcessMessage(TASK_MSG_TABLE_ENTRY *, INTERTASK *);
static void fnSetWaitCondition( unsigned long lwEventsAwaited, unsigned long lwMilliseconds, T_FUNCPTR pResumeFn );
//static void fnSetWaitCondition(unsigned long lwEventsAwaited, unsigned long lwMilliseconds, void (* pResumeFn) ());
static void fnSetPowerupSeqComplete( void );
void fnSendBobPrintMode (unsigned char bMode);
static void fnProcessSTEvent(unsigned char  bEvent);
static void fnSetSYSMessageTimeout(SYS_MSG_TIMEOUT *pTimeoutInfo, unsigned char bNumMessages,
                                    unsigned long lwMilliTimeout, unsigned char bTimeoutType);
static BOOL fnCheckSYSWaitCondsSatisfied();
static BOOL fnCheckSYSMessageTimeout(INTERTASK *pIntertask);

/* intertask message handlers */
static BOOL fnmOITInitializeResponse(INTERTASK *);
static BOOL fnmCMInitializeResponse(INTERTASK *);
static BOOL fnmCMMailJobComplete(INTERTASK *MsgPtr);
static BOOL fnmCMOSRateCallBack(INTERTASK *MsgPtr);
static BOOL fnmStartMailRequest(INTERTASK *MsgPtr);
static BOOL fnmStopMailRequest(INTERTASK *MsgPtr);
static BOOL fnmCMS3Covered(INTERTASK *MsgPtr);
static BOOL fnmCMMailRunStarted(INTERTASK *MsgPtr);
static BOOL fnmCMRunRequest(INTERTASK *MsgPtr);
static BOOL fnmCMModeResponse(INTERTASK *pMsgPtr);

static BOOL fnmOITNotifyFatalError(INTERTASK *MsgPtr);

static void fnsOITSendAnyPREvent(SYS_STATE *pNextState);
static void fnsOITSendLeftPREvent(SYS_STATE *pNextState);
static void fnsEDMSendLeftPREvent(SYS_STATE *pNextState);
static void fnsOITSendAnySleepEvent(SYS_STATE *pNextState);
static void fnsOITSendMeterError(SYS_STATE *pNextState);
static void fnsOITSendManufEvent(SYS_STATE *pNextState);
static void fnsOITSendRunEvent(SYS_STATE *pNextState);
static void fnsOITSendTapeEvent(SYS_STATE *pNextState);
// static void fnsOITSendAck(SYS_STATE *pNextState);
static void fnsOITSendMode(SYS_STATE *pNextState);

// Initialize state table pointers
static void fnInitializeStateTablePtr(void);

// Initialize key system information
static void fnInitializeSystemInfo(void);
static void enableDelayedNetworkComponents(void);

/* externs */
extern TASK_DEFINITION Tasks [];

extern void installStandardErrorHandlerInAllTasks();
extern STATUS fnUpdateZmd();
STATUS fnZmdLookForNewZmd();
STATUS fnGARLookForNewGAR();
// From tskutil.c:
//extern void fnSystemLogResetHeader( void );
extern void    fnInitEventTimer(void);
extern void fnCMOSSecondStageInit();
extern int fnEMDPowerFailCheck( void );
extern void fnValidateExtPrinterFiles(void);
//extern void fnSetNetworkDefaults(void);
extern void fnInitializeNetworkDataFromFlash( void );
extern void fnNetSetToneDialing(BOOL);
extern void fnInitSystemStatus(void);
extern void fnSetGTSPieceProcessed (BOOL fProcessed);
//
extern BOOL ValidateCCDFile( void );
//extern void fnNOVAClearStructures(void);		// no longer needed
extern int fnGFGrfxCleanup( void );
extern void fnPreInitializePsocTask(void);
extern void fnLCDPutStringDirect( unsigned char *pString , int x , int y , int font );

// This is prototyped in lan.h, missing the void argument list.
extern BOOL fnLANInitialize( void );          // can be changed to include public lan header instead once that is cleaned up

extern void InitMcpData(void);

 extern void fnInitPSDNumDecimal();

extern SetupParams CMOSSetupParams;
extern Diagnostics CMOSDiagnostics;
extern const char UicVersionString[];
extern BOOL         fMeterLocked;
extern unsigned char pIntertaskLogMask[32];
extern NU_MEMORY_POOL * pSystemMemoryCached;
extern NU_MEMORY_POOL * pSystemMemoryUncached;
extern picklist_file_header picklist_file_headers[];
extern ulong lwAppActualCheckSum;
extern DATETIME CMOSLastLocalMidnightDT;
extern BOOL fArchiveExisted;
extern NUCNETCONFIG        NucNetworkConfig;

extern const UINT8 	*globalptrEmbeddedFirmwareStart;
extern const UINT 	globalEmbeddedFirmwareSize;

extern HBLogStructure globalHBLogStructure;

extern ulong 			CMOSTrmUploadDueReqd;
extern BOOL 			bGoToReady;
/* declarations */
RAM_ITEMS_XREF  sRamItemsXref;
int  EmdEc = 0;						// separate variable for the error code when trying to
                                    // load the EMD so later on we can tell what the problem was
BOOL NoEMDPresent = TRUE;
BOOL NewEMDPresent = FALSE;         //lint !e714     set here, accessed elsewhere
BOOL RamEMDPresent = FALSE;         //lint !e714     set here, accessed elsewhere
BOOL NoGARFilePresent = TRUE;

BOOL fScreenVersionsOK = TRUE;      // Assume their ok, until we check them.
const char sBlankLine[] = "                          ";     // 26 spaces

//static ulong                cmErrCode = 0;      //lint !e551    // set here, accessed elsewhere
//static ulong    pmErrCode = 0;
static unsigned long    lwPowerupSeqEventsAwaited = 0;
static unsigned long    lwPowerupSeqWaitTimeout = 0;
T_FUNCPTR               pPowerupSeqResumeFn;
//static void                 (* pPowerupSeqResumeFn) (void);
static BOOL fPowerupSeqComplete = FALSE;
//static BOOL fWaitAtStage = FALSE;
static SYS_MSG_TIMEOUT_CTRL rSYSMsgTimeoutCtrl;
//static unsigned short       usThruputTimerIndex = SYS_THRUPUT_TIMER_20;
//static unsigned long      ulThruputTimeoutMs;
static unsigned char  bEDMLastMode = 0xFF;  /* init to a mode that doesn't exist.  this variable is used
                          to keep track of if we need to send a new message to the
                          EDM since that should only happen when the EDM state changes */

/* this is a debugging buffer */
#define STATE_TRANSITION_LOG_SIZE   1500
unsigned char   pStateBuf[STATE_TRANSITION_LOG_SIZE];
unsigned long   lwStateBufIndex = 0;

STATUS InitFlashFileSystem(void);
STATUS UpdateEMD(void);
STATUS InitTrm(void);

// Mode/submode structure
typedef struct
{
    UINT16 usMode;      // Mode
    UINT16 usSubmode;   // Submode
} MODE_SUBMODE_TYPE;


// Flag/mask structure
typedef struct
{
    UINT32 ulFlags;         // State flags
    UINT32 ulFlagsEnable;   // Flag mask
} FLAG_MASK_TYPE;


// Mode information structure
typedef struct
{
    FLAG_MASK_TYPE stFlag;
    UINT8          ucPMCIState;
    UINT8          ucPCState;
} MODE_INFO_TYPE;


// System state structure
typedef struct
{
    MODE_SUBMODE_TYPE stMode;       // mode/submode
    MODE_INFO_TYPE    stInfo;       // Mode information
} SYS_STATE_TYPE;


typedef void (* pFunct) (SYS_STATE *);  /* the format of a state transition function.  the bool returned is true if
                                           the function is returning the next state by reference.  otherwise the
                                           next state from the state table should be used. */

// System state table structure
typedef struct
{
    const MODE_INFO_TYPE*    const pCurrInfo;  //current mode information
    UINT8                          ucEvent;    // System event
    pFunct                         pfnStateTransFn;
    const MODE_SUBMODE_TYPE* const pNextMode;  // next mode/submode
    const MODE_INFO_TYPE*    const pNextInfo;  // next mode information
} STATE_TABLE_TYPE;


/* message tables */
static const TASK_MSG_TABLE_ENTRY StSYSMsgTable[] = {
/* message ID                   byte data [0]       byte data [1]      event (FF means call fn)       fn pointer             */
{ SYS_CMOS_RATE_CALLBACK_REQ,   SYS_DONT_CARE,      SYS_DONT_CARE,          0xFF,               fnmCMOSRateCallBack         },
{ SYS_END_OF_TABLE, 0, 0, 0 } };


static const TASK_MSG_TABLE_ENTRY StCMMsgTable[] = {
/* message ID                   byte data [0]       byte data [1]      event (FF means call fn)       fn pointer             */
{ CS_INIT_RSP,                  SYS_DONT_CARE,      SYS_DONT_CARE,          0xFF,               fnmCMInitializeResponse     },
{ CS_MODE_RSP,                  SYS_DONT_CARE,      SYS_DONT_CARE,          0xFF, /*SEV_CMMODECONFIRM*/  fnmCMModeResponse/*NULL*/},
{ CS_RUN_REQ,                   SYS_DONT_CARE,      SYS_DONT_CARE,          0xFF,               fnmCMRunRequest             },
{ CS_S3_COVERED,                SYS_DONT_CARE,      SYS_DONT_CARE,          0xFF,               fnmCMS3Covered              },
{ CS_PRINT_STARTED,             SYS_DONT_CARE,      SYS_DONT_CARE,          SEV_CMPRINTSTARTED, NULL                        },
{ CS_JOB_CANCEL,                SYS_DONT_CARE,      SYS_DONT_CARE,          SEV_CMCANCELRUN,    NULL                        },
{ CS_MAILRUN_CMPLT,             SYS_DONT_CARE,      SYS_DONT_CARE,          0xFF,               fnmCMMailJobComplete        },
{ CS_MAILRUN_STARTED,           SYS_DONT_CARE,      SYS_DONT_CARE,          0xFF,               fnmCMMailRunStarted         },
//( CS_STATUS_RESPONSE,           SYS_DONT_CARE,        SYS_DONT_CARE,          0xFF,               fnmCMSysStatus              },

{ SYS_END_OF_TABLE, 0, 0, 0 } };


static const TASK_MSG_TABLE_ENTRY StBOBMsgTable[] = {
/* message ID                   byte data [0]       byte data [1]      event (FF means call fn)       fn pointer             */
{ BOB_GEN_STATIC_IMAGE_RSP,     SYS_DONT_CARE,      SYS_DONT_CARE,          SEV_BOBSTATICIMAGE, NULL                        },
{ SYS_END_OF_TABLE, 0, 0, 0 } };

static const TASK_MSG_TABLE_ENTRY StOITMsgTable[] = {
/* message ID                   byte data [0]       byte data [1]      event (FF means call fn)       fn pointer             */
{ OIT_INITIALIZE_RESPONSE,  SYS_DONT_CARE,          SYS_DONT_CARE,          0xFF,               fnmOITInitializeResponse    },
//{ OIT_REBOOT_REQUEST,     SYS_DONT_CARE,          SYS_DONT_CARE,          0xFF,               fnmReboot                   },
{ OIT_START_MAIL_REQ,       SYS_DONT_CARE,          SYS_DONT_CARE,          0xFF,               fnmStartMailRequest         },
{ OIT_STOP_MAIL_REQ,        SYS_DONT_CARE,          SYS_DONT_CARE,          0xFF,               fnmStopMailRequest          },

{ OIT_EVENT,                OIT_LEFT_PRINT_RDY,     SYS_DONT_CARE,          SEV_OINOTREADY,     NULL                        },
{ OIT_EVENT,                OIT_ENTER_PRINT_RDY,    SYS_DONT_CARE,          SEV_OIENBLINDI,     NULL                        },
{ OIT_EVENT,                OIT_PRINT_PERMIT,       SYS_DONT_CARE,          SEV_OIENBLPERMIT,   NULL                        },
{ OIT_EVENT,                OIT_PRINT_NO_INDICIA,   SYS_DONT_CARE,          SEV_OIENBLSEALONLY, NULL                        },
{ OIT_EVENT,                OIT_PREP_FOR_RPT_PRINTING,SYS_DONT_CARE,        SEV_OIENBLREPORT,   NULL                        },
{ OIT_EVENT,                OIT_PRINT_ENV_TEST,     SYS_DONT_CARE,          SEV_OIENBLTEST,     NULL                        },

{ OIT_EVENT,                OIT_PRINT_TAPE,         SYS_DONT_CARE,          SEV_OISTARTTAPE,    NULL                        },
{ OIT_EVENT,                OIT_PRINT_TAPE_PERMIT,  SYS_DONT_CARE,          SEV_OISTARTTAPE,    NULL                        },
{ OIT_EVENT,                OIT_PRINT_TAPE_TEST,    SYS_DONT_CARE,          SEV_OISTARTTAPE,    NULL                        },
{ OIT_EVENT,                OIT_PRINT_RPT_TO_TAPE,  SYS_DONT_CARE,          SEV_OISTARTTAPE,    NULL                        },

{ OIT_EVENT,                OIT_PRINTING,           SYS_DONT_CARE,          SEV_OIPRINT,        NULL                        },
{ OIT_EVENT,                OIT_RUNNING_PERMIT,     SYS_DONT_CARE,          SEV_OIPRINT,        NULL                        },
{ OIT_EVENT,                OIT_RUNNING_SEAL,       SYS_DONT_CARE,          SEV_OIPRINT,        NULL                        },
{ OIT_EVENT,                OIT_RUNNING_REPORT,     SYS_DONT_CARE,          SEV_OIPRINT,        NULL                        },
{ OIT_EVENT,                OIT_RUNNING_TEST,       SYS_DONT_CARE,          SEV_OIPRINT,        NULL                        },
//added by RAM to diable sleep
//{ OIT_EVENT,                OIT_GOTO_SLEEP,         SYS_DONT_CARE,          SEV_OISLEEP,        NULL                        },
//{ OIT_EVENT,                OIT_ENTER_POWERDOWN,    SYS_DONT_CARE,          SEV_OISOFTPWRDOWN,  NULL                        },
{ OIT_EVENT,                OIT_INIT_MAINT_USER,    SYS_DONT_CARE,          SEV_OIMAINTUSER,    NULL                        },
{ OIT_EVENT,                OIT_ERROR_METER,        SYS_DONT_CARE,          SEV_OIMETERERROR,   NULL                        },

{ OIT_EVENT,                OIT_LOCK_EDM,           SYS_DONT_CARE,          SEV_EDMLOCK,        NULL                        },

{ OIT_EVENT,                OIT_ENTER_DIAG_MODE,    SYS_DONT_CARE,          SEV_OIDIAG,         NULL                        },

{ OIT_EVENT,                OIT_ENTER_MANUFACTURING,    SYS_DONT_CARE,          SEV_ENTERMFG,         NULL                        },

{ OIT_EVENT,                OIT_NOTIFY_FATAL_ERROR,    SYS_DONT_CARE,       0xFF,               fnmOITNotifyFatalError      },


{ SYS_END_OF_TABLE, 0, 0, 0 } };

SYS_STATE       rCurrentState = { SYS_POWERUP, SYS_NO_SUBMODE, 0, 0, CM_INITIALIZING, PC_ST_NOT_CONNECT };


// Mode Info structures
static const MODE_INFO_TYPE StDontCare0000   = {{F_CLEAR,
                                                F_CLEAR},
                                                SYS_BYTE_DC,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StDontCare0808   = {{F_MAIL_JOB_COMPLETE,
                                                F_MAIL_JOB_COMPLETE},
                                                SYS_BYTE_DC,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StDontCare0818   = {{F_MAIL_JOB_COMPLETE,
                                                (F_MAIL_JOB_COMPLETE |
                                                 F_RUN_CANCELLED)},
                                                SYS_BYTE_DC,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StDontCare2020   = {{F_RUN_PENDING,
                                                F_RUN_PENDING},
                                                SYS_BYTE_DC,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StInit0000       = {{F_CLEAR,
                                                F_CLEAR},
                                                CM_INITIALIZING,
                                                PC_ST_NOT_CONNECT};

static const MODE_INFO_TYPE StInitDC0000     = {{F_CLEAR,
                                                F_CLEAR},
                                                CM_INITIALIZING,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StDisabled0000   = {{F_CLEAR,
                                                F_CLEAR},
                                                CM_DISABLED,
                                                PC_ST_NOT_CONNECT};

static const MODE_INFO_TYPE StDisabledDC0000 = {{F_CLEAR,
                                                F_CLEAR},
                                                CM_DISABLED,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StDisabledDC0006 = {{F_CLEAR,
                                                (F_CM_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED)},
                                                CM_DISABLED,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StDisabledDC0606 = {{(F_CM_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_CM_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED)},
                                                CM_DISABLED,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StDisabledDC060E = {{(F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED |
                                                 F_MAIL_JOB_COMPLETE)},
                                                CM_DISABLED,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StDisabledDC060F = {{(F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_BOB_UNCONFIRMED |
                                                 F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED |
                                                 F_MAIL_JOB_COMPLETE)},
                                                CM_DISABLED,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StDisabledDC061E = {{(F_CM_UNCONFIRMED    |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_CM_UNCONFIRMED    |
                                                 F_OI_NOT_NOTIFIED   |
                                                 F_MAIL_JOB_COMPLETE |
                                                 F_RUN_CANCELLED)},
                                                CM_DISABLED,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StDisabledDC063E = {{(F_CM_UNCONFIRMED    |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_CM_UNCONFIRMED    |
                                                 F_OI_NOT_NOTIFIED   |
                                                 F_MAIL_JOB_COMPLETE |
                                                 F_RUN_CANCELLED |
                                                 F_RUN_PENDING)},
                                                CM_DISABLED,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StDisabledDC070F = {{(F_BOB_UNCONFIRMED |
                                                 F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_BOB_UNCONFIRMED |
                                                 F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED |
                                                 F_MAIL_JOB_COMPLETE)},
                                                CM_DISABLED,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC0000    = {{F_CLEAR,
                                                F_CLEAR},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC0001    = {{F_CLEAR,
                                                F_BOB_UNCONFIRMED},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC0002    = {{F_CLEAR,
                                                F_CM_UNCONFIRMED},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC0005    = {{F_CLEAR,
                                                (F_BOB_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED)},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC0006    = {{F_CLEAR,
                                                (F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED)},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC0007    = {{F_CLEAR,
                                                (F_BOB_UNCONFIRMED |
                                                 F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED)},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC0507    = {{(F_BOB_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_BOB_UNCONFIRMED |
                                                 F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED)},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC0606    = {{(F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED)},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC0607    = {{(F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_BOB_UNCONFIRMED |
                                                 F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED)},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC060F    = {{(F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_BOB_UNCONFIRMED |
                                                 F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED |
                                                 F_MAIL_JOB_COMPLETE)},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC061F    = {{(F_CM_UNCONFIRMED    |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_BOB_UNCONFIRMED   |
                                                 F_CM_UNCONFIRMED    |
                                                 F_OI_NOT_NOTIFIED   |
                                                 F_MAIL_JOB_COMPLETE |
                                                 F_RUN_CANCELLED)},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC0707    = {{(F_BOB_UNCONFIRMED |
                                                 F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_BOB_UNCONFIRMED |
                                                 F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED)},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC070F    = {{(F_BOB_UNCONFIRMED |
                                                 F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_BOB_UNCONFIRMED |
                                                 F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED |
                                                 F_MAIL_JOB_COMPLETE)},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC071F    = {{(F_BOB_UNCONFIRMED   |
                                                 F_CM_UNCONFIRMED    |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_BOB_UNCONFIRMED   |
                                                 F_CM_UNCONFIRMED    |
                                                 F_OI_NOT_NOTIFIED   |
                                                 F_MAIL_JOB_COMPLETE |
                                                 F_RUN_CANCELLED)},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC2527    = {{(F_BOB_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED |
                                                 F_RUN_PENDING),
                                                (F_BOB_UNCONFIRMED |
                                                 F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED |
                                                 F_RUN_PENDING)},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StReadyDC2627    = {{(F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED |
                                                 F_RUN_PENDING),
                                                (F_BOB_UNCONFIRMED |
                                                 F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED |
                                                 F_RUN_PENDING)},
                                                CM_READY,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StSleepDC0000    = {{F_CLEAR,
                                                F_CLEAR},
                                                CM_SLEEP,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StSleepDC0006    = {{F_CLEAR,
                                                (F_CM_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED)},
                                                CM_SLEEP,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StSleepDC0606    = {{(F_CM_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_CM_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED)},
                                                CM_SLEEP,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StSleepDC060E    = {{(F_CM_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_CM_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED |
                                                 F_MAIL_JOB_COMPLETE)},
                                                CM_SLEEP,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StRunDC0000      = {{F_CLEAR,
                                                F_CLEAR},
                                                CM_RUNNING,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StRunDC0008      = {{F_CLEAR,
                                                F_MAIL_JOB_COMPLETE},
                                                CM_RUNNING,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StRunDC0010      = {{F_CLEAR,
                                                F_RUN_CANCELLED},
                                                CM_RUNNING,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StRunDC0025      = {{F_CLEAR,
                                                (F_BOB_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED |
                                                 F_RUN_PENDING)},
                                                CM_RUNNING,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StRunDC0026      = {{F_CLEAR,
                                                (F_CM_UNCONFIRMED  |
                                                 F_OI_NOT_NOTIFIED |
                                                 F_RUN_PENDING)},
                                                CM_RUNNING,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StRunDC1010      = {{F_RUN_CANCELLED,
                                                F_RUN_CANCELLED},
                                                CM_RUNNING,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StPreRunDC0000   = {{F_CLEAR,
                                                F_CLEAR},
                                                CM_PRERUNNING,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StPreRunDC1010   = {{F_RUN_CANCELLED,
                                                F_RUN_CANCELLED},
                                                CM_PRERUNNING,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StPrintDC0000    = {{F_CLEAR,
                                                F_CLEAR},
                                                CM_PRINTING,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StPrintDC0008    = {{F_CLEAR,
                                                F_MAIL_JOB_COMPLETE},
                                                CM_PRINTING,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StPrintDC0606    = {{(F_CM_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_CM_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED)},
                                                CM_PRINTING,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StDiagDC0000     = {{F_CLEAR,
                                                F_CLEAR},
                                                CM_DIAG,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StDiagDC0006     = {{F_CLEAR,
                                                (F_CM_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED)},
                                                CM_DIAG,
                                                SYS_BYTE_DC};

static const MODE_INFO_TYPE StDiagDC0606     = {{(F_CM_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED),
                                                (F_CM_UNCONFIRMED |
                                                 F_OI_NOT_NOTIFIED)},
                                                CM_DIAG,
                                                SYS_BYTE_DC};



// Mode/submode structures
static const MODE_SUBMODE_TYPE StPwrupMode    = {SYS_POWERUP,
                                                 SYS_NO_SUBMODE};

static const MODE_SUBMODE_TYPE StManfMode     = {SYS_MANUFACTURING,
                                                 SYS_NO_SUBMODE};

static const MODE_SUBMODE_TYPE StPrintNotRdy  = {SYS_PRINT_NOT_READY,
                                                 SYS_NO_SUBMODE};

static const MODE_SUBMODE_TYPE StPrintReady   = {SYS_PRINT_READY,
                                                 SYS_INDICIA};

static const MODE_SUBMODE_TYPE StPrintRdyDC   = {SYS_PRINT_READY,
                                                 SYS_DONT_CARE};

static const MODE_SUBMODE_TYPE StPrintPermit  = {SYS_PRINT_READY,
                                                 SYS_PERMIT};

static const MODE_SUBMODE_TYPE StPrintReport  = {SYS_PRINT_READY,
                                                 SYS_REPORT};

static const MODE_SUBMODE_TYPE StPrintTest    = {SYS_PRINT_READY,
                                                 SYS_TEST_PATTERN};

static const MODE_SUBMODE_TYPE StPrintSeal    = {SYS_PRINT_READY,
                                                 SYS_SEAL_ONLY};

static const MODE_SUBMODE_TYPE StMeterErr     = {SYS_METER_ERROR,
                                                 SYS_NO_SUBMODE};

static const MODE_SUBMODE_TYPE StSleepDC      = {SYS_SLEEP,
                                                 SYS_DONT_CARE};

static const MODE_SUBMODE_TYPE StSleepNorm    = {SYS_SLEEP,
                                                 SYS_NORMAL_SLEEP};

static const MODE_SUBMODE_TYPE StSleepPwrDwn  = {SYS_SLEEP,
                                                 SYS_SOFT_POWER_DOWN};

static const MODE_SUBMODE_TYPE StRunningDC    = {SYS_RUNNING,
                                                 SYS_DONT_CARE};

static const MODE_SUBMODE_TYPE StRunningInd   = {SYS_RUNNING,
                                                 SYS_INDICIA};

static const MODE_SUBMODE_TYPE StRunningPer   = {SYS_RUNNING,
                                                 SYS_PERMIT};

static const MODE_SUBMODE_TYPE StRunningSeal  = {SYS_RUNNING,
                                                 SYS_SEAL_ONLY};

static const MODE_SUBMODE_TYPE StPrintTapeDC  = {SYS_PRINTING_TAPE,
                                                 SYS_DONT_CARE};

static const MODE_SUBMODE_TYPE StPrintTapeInd = {SYS_PRINTING_TAPE,
                                                 SYS_INDICIA};

static const MODE_SUBMODE_TYPE StPrintTapePer = {SYS_PRINTING_TAPE,
                                                 SYS_PERMIT};

static const MODE_SUBMODE_TYPE StDontCareMode = {SYS_DONT_CARE,
                                                 SYS_DONT_CARE};

static const MODE_SUBMODE_TYPE StDiagnostics    = {SYS_DIAGNOSTICS,
                                                 SYS_DONT_CARE};

static const MODE_SUBMODE_TYPE StPrintNotRdyEdmLock  = {SYS_PRINT_NOT_READY,
                                                 SYS_EDM_LOCK};

// System catch-all state table
static const STATE_TABLE_TYPE StCatchallState[] = {
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,           &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,    &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,           &StDontCareMode,   &StDontCare0000},

// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode, &StMeterErr,       &StDisabledDC0000},
{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};



// System powerup state table
static const STATE_TABLE_TYPE StPowerupState[] = {
{&StInit0000,       SEV_ALLRSPND,       fnsPwrComplete,         &StPwrupMode,      &StDisabled0000},
// Power up transition to manufacturing mode- you can get to that mode but you can't get out
{&StInit0000,       SEV_ENTERMFG,       fnsPwrCompleteMfg,      &StManfMode,       &StDisabledDC0000},
// Transition from power up to a not ready to print screen
{&StDisabledDC0000, SEV_OINOTREADY,     fnsCMDisable,           &StPrintNotRdy,    &StDisabledDC060E},
// Transition from power up to indicia ready screen
{&StDisabledDC0000, SEV_OIENBLINDI,     fnsPrepIndiciaMode,     &StPrintReady,     &StReadyDC070F},
// Transition from power up to permit ready screen
{&StDisabledDC0000, SEV_OIENBLPERMIT,   fnsPrepPermitMode,      &StPrintPermit,    &StReadyDC070F},
// Transition from power up to a report screen
{&StDisabledDC0000, SEV_OIENBLREPORT,   fnsPrepReportMode,      &StPrintReport,    &StReadyDC070F},
// Transition from power up to a test pattern screen
{&StDisabledDC0000, SEV_OIENBLTEST,     fnsPrepTestPatternMode, &StPrintTest,      &StReadyDC070F},
// Transition from power up to seal only ready screen
{&StDisabledDC0000, SEV_OIENBLSEALONLY, fnsPrepSealOnlyMode,    &StPrintSeal,      &StReadyDC060F},
// Meter error during powerup requires EDM to receive the power up complete message so diagnostics, downloads, etc. can be done
{&StInitDC0000,     SEV_OIMETERERROR,   fnsGoMeterErrorEnblEDM, &StMeterErr,       &StDisabledDC0000},

// Catch-all states
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,           &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,    &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,           &StDontCareMode,   &StDontCare0000},

// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode, &StMeterErr,       &StDisabledDC0000},
{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};



// System Print Ready Indicia state table
static const STATE_TABLE_TYPE StPRIndiciaState[] = {
// Transition to not ready state from any ready to print state
{&StReadyDC0000, SEV_OINOTREADY,     fnsCMDisable,           &StPrintNotRdy,  &StDisabledDC060E},
// Transition to sleep state from any ready to print state
{&StReadyDC0000, SEV_OISLEEP,        fnsCMSleep,             &StSleepNorm,    &StSleepDC060E},
// Transition to soft power down state from any ready to print state
{&StReadyDC0000, SEV_OISOFTPWRDOWN,  fnsCMSleep,             &StSleepPwrDwn,  &StSleepDC060E},

// Waiting for BOB and CM responses to complete switch to indicia mode.  CM response received first.
{&StReadyDC0707, SEV_CMMODECONFIRM,  fnsDoNothing,           &StPrintRdyDC,   &StReadyDC0002},
// Waiting for BOB and CM responses to complete switch to indicia mode.  BOB response received first.
{&StReadyDC0707, SEV_BOBSTATICIMAGE, fnsDoNothing,           &StPrintRdyDC,   &StReadyDC0001},
// Run Pending (indicia)... go directly to running state
{&StReadyDC2627, SEV_CMMODECONFIRM,  fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0026},
// Run Pending (indicia)... go directly to running state
{&StReadyDC2527, SEV_BOBSTATICIMAGE, fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0025},
// Waiting for CM response only to complete switch to indicia mode.  CM response received means OIT can be notified of mode change.
{&StReadyDC0607, SEV_CMMODECONFIRM,  fnsOITSendAnyPREvent,   &StPrintRdyDC,   &StReadyDC0006},
// Waiting for BOB response only to complete switch to indicia mode.  BOB response received means OIT can be notified of mode change.
{&StReadyDC0507, SEV_BOBSTATICIMAGE, fnsOITSendAnyPREvent,   &StPrintRdyDC,   &StReadyDC0005},

// DM400C: Start Run (any indicia type) in the print ready state, go to running state with CM prerunning state.
{&StReadyDC0007, SEV_OISTARTMAIL,    fnsStartMailRun,        &StRunningDC,    &StPreRunDC0000},
// Tape Run (indicia, permit, report, test pat) in the print ready state, go to tape_printing state with CM prerunning state.
{&StReadyDC0007, SEV_OISTARTTAPE,    fnsStartMailRun,        &StPrintTapeDC,  &StPreRunDC0000},

// Run request received from CM while in the print ready-indicia state.
{&StReadyDC0007, SEV_CMRUN,          fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0000},
// Run request received from OIT while in the print ready-indicia state.  (This does nothing, but exists to prevent unhandled state transition).
{&StReadyDC0007, SEV_OIPRINT,        fnsDoNothing,           &StDontCareMode, &StDontCare0000},
// Switch from indicia ready to report ready
{&StReadyDC0000, SEV_OIENBLREPORT,   fnsPrepReportMode,      &StPrintReport,  &StReadyDC070F},
// Switch from indicia ready to test pattern ready
{&StReadyDC0000, SEV_OIENBLTEST,     fnsPrepTestPatternMode, &StPrintTest,    &StReadyDC070F},
// Switch from indicia ready to seal only ready
{&StReadyDC0000, SEV_OIENBLSEALONLY, fnsPrepSealOnlyMode,    &StPrintSeal,    &StReadyDC060F},

// Waiting for BOB and CM responses to complete switch to report/testpat mode.  CM response received first.
{&StReadyDC0707, SEV_CMMODECONFIRM,  fnsDoNothing,           &StPrintRdyDC,   &StReadyDC0002},
// Waiting for BOB and CM responses to complete switch to report/testpat mode.  BOB response received first.
{&StReadyDC0707, SEV_BOBSTATICIMAGE, fnsDoNothing,           &StPrintRdyDC,   &StReadyDC0001},
// Run Pending (non-indicia)... go directly to running state
{&StReadyDC2627, SEV_CMMODECONFIRM,  fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0026},
// Run Pending (non-indicia)... go directly to running state
{&StReadyDC2527, SEV_BOBSTATICIMAGE, fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0025},
// Waiting for CM response only to complete switch to report/testpat mode.  CM response received means OIT can be notified of mode change.
{&StReadyDC0607, SEV_CMMODECONFIRM,  fnsOITSendAnyPREvent,   &StPrintRdyDC,   &StReadyDC0006},
// Waiting for BOB response only to complete switch to report/testpat mode.  BOB response received means OIT can be notified of mode change.
{&StReadyDC0507, SEV_BOBSTATICIMAGE, fnsOITSendAnyPREvent,   &StPrintRdyDC,   &StReadyDC0005},
// Run request received from CM while in the print ready-report or print ready-testpat state.
{&StReadyDC0007, SEV_CMRUN,          fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0000},
// Run request received from OIT while in the print ready-report or print ready-testpat state.  (This does nothing but prevents unhandled state trans).
{&StReadyDC0007, SEV_OIPRINT,        fnsDoNothing,           &StDontCareMode, &StDontCare0000},
// Switch from report ready or test pat ready to indicia ready
{&StReadyDC0000, SEV_OIENBLINDI,     fnsPrepIndiciaMode,     &StPrintReady,   &StReadyDC070F},
// Switch from report ready or test pat ready to permit ready
{&StReadyDC0000, SEV_OIENBLPERMIT,   fnsPrepPermitMode,      &StPrintPermit,  &StReadyDC070F},
// Switch from report ready or test pat ready to seal only ready
{&StReadyDC0000, SEV_OIENBLSEALONLY, fnsPrepSealOnlyMode,    &StPrintSeal,    &StReadyDC060F},

// Catch-all states
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,           &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,    &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,           &StDontCareMode,   &StDontCare0000},

// Start of transition from indicia ready state to indicia ready state: The graphics have likely changed, and
//  we need to prepare for printing all over again (tell bob to download static image, etc.)
{&StDontCare0000, SEV_OIENBLINDI,       fnsPrepIndiciaMode,     &StPrintReady,  &StReadyDC070F},

// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode, &StMeterErr,       &StDisabledDC0000},

// EDM
{ &StReadyDC0000,     SEV_EDMPRINTTAPE,   fnsPrintTapeIndicia,    &StPrintTapeInd,      &StPreRunDC0000},
{ &StReadyDC0000,     SEV_EDMDISABLE,     fnsCMDisable,           &StPrintNotRdyEdmLock,        &StDisabledDC060E},
{ &StReadyDC0000,     SEV_EDMENABLE,      fnsEDMResendMode,       &StDontCareMode,      &StReadyDC0000},

//Transition to Manufacturing state
{&StDontCare0000,   SEV_ENTERMFG, 		 fnsDoNothing,           &StManfMode,   &StDisabledDC0000},
{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};



// System Print Ready Seal Only state table
static const STATE_TABLE_TYPE StPRSealState[] = {
// Transition to not ready state from any ready to print state
{&StReadyDC0000, SEV_OINOTREADY,     fnsCMDisable,           &StPrintNotRdy,  &StDisabledDC060E},
// Transition to sleep state from any ready to print state
{&StReadyDC0000, SEV_OISLEEP,        fnsCMSleep,             &StSleepNorm,    &StSleepDC060E},
// Transition to soft power down state from any ready to print state
{&StReadyDC0000, SEV_OISOFTPWRDOWN,  fnsCMSleep,             &StSleepPwrDwn,  &StSleepDC060E},

// DM400C: Start Run (any indicia type) in the print ready state, go to running state with CM prerunning state.
{&StReadyDC0007, SEV_OISTARTMAIL,    fnsStartMailRun,        &StRunningDC,    &StPreRunDC0000},
// Tape Run (indicia, permit, report, test pat) in the print ready state, go to tape_printing state with CM prerunning state.
//{&StReadyDC0007, SEV_OISTARTTAPE,    fnsStartMailRun,        &StPrintTapeDC,  &StPreRunDC0000},

// Waiting for BOB and CM responses to complete switch to report/testpat mode.  CM response received first.
{&StReadyDC0707, SEV_CMMODECONFIRM,  fnsDoNothing,           &StPrintRdyDC,   &StReadyDC0002},
// Waiting for BOB and CM responses to complete switch to report/testpat mode.  BOB response received first.
{&StReadyDC0707, SEV_BOBSTATICIMAGE, fnsDoNothing,           &StPrintRdyDC,   &StReadyDC0001},
// Run Pending (non-indicia)... go directly to running state
{&StReadyDC2627, SEV_CMMODECONFIRM,  fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0026},
// Run Pending (non-indicia)... go directly to running state
{&StReadyDC2527, SEV_BOBSTATICIMAGE, fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0025},
// Waiting for CM response only to complete switch to report/testpat mode.  CM response received means OIT can be notified of mode change.
{&StReadyDC0607, SEV_CMMODECONFIRM,  fnsOITSendAnyPREvent,   &StPrintRdyDC,   &StReadyDC0006},
// Waiting for BOB response only to complete switch to report/testpat mode.  BOB response received means OIT can be notified of mode change.
{&StReadyDC0507, SEV_BOBSTATICIMAGE, fnsOITSendAnyPREvent,   &StPrintRdyDC,   &StReadyDC0005},
// Run request received from CM while in the print ready-report or print ready-testpat state.
{&StReadyDC0007, SEV_CMRUN,          fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0000},
// Run request received from OIT while in the print ready-report or print ready-testpat state.  (This does nothing but prevents unhandled state trans).
{&StReadyDC0007, SEV_OIPRINT,        fnsDoNothing,           &StDontCareMode, &StDontCare0000},
// Switch from report ready or test pat ready to indicia ready
{&StReadyDC0000, SEV_OIENBLINDI,     fnsPrepIndiciaMode,     &StPrintReady,   &StReadyDC070F},
// Switch from report ready or test pat ready to permit ready
{&StReadyDC0000, SEV_OIENBLPERMIT,   fnsPrepPermitMode,      &StPrintPermit,  &StReadyDC070F},
// Switch from report ready or test pat ready to seal only ready
{&StReadyDC0000, SEV_OIENBLSEALONLY, fnsPrepSealOnlyMode,    &StPrintSeal,    &StReadyDC060F},

// Catch-all states
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,           &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,    &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,           &StDontCareMode,   &StDontCare0000},

// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode, &StMeterErr,       &StDisabledDC0000},

//12/14/2009 Jingwei,Li 
// When system powerup/wakeup, if need print manifest report, there is no state transaction for this event.
// Switch from indicia ready to report ready
{&StReadyDC0000, SEV_OIENBLREPORT,   fnsPrepReportMode,      &StPrintReport,  &StReadyDC070F},

// Switch from SealOnly to test pattern ready
{&StReadyDC0000, SEV_OIENBLTEST,     fnsPrepTestPatternMode, &StPrintTest,    &StReadyDC070F},


{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};



// System Print Ready Test state table
static const STATE_TABLE_TYPE StPRTestState[] = {
// Transition to not ready state from any ready to print state
{&StReadyDC0000, SEV_OINOTREADY,     fnsCMDisable,           &StPrintNotRdy,  &StDisabledDC060E},
// Transition to sleep state from any ready to print state
{&StReadyDC0000, SEV_OISLEEP,        fnsCMSleep,             &StSleepNorm,    &StSleepDC060E},
// Transition to soft power down state from any redy to print state
{&StReadyDC0000, SEV_OISOFTPWRDOWN,  fnsCMSleep,             &StSleepPwrDwn,  &StSleepDC060E},

// DM400C: Start Run (any indicia type) in the print ready state, go to running state with CM prerunning state.
{&StReadyDC0007, SEV_OISTARTMAIL,    fnsStartMailRun,        &StRunningDC,    &StPreRunDC0000},
// Tape Run (indicia, permit, report, test pat) in the print ready state, go to tape_printing state with CM prerunning state.
{&StReadyDC0007, SEV_OISTARTTAPE,    fnsStartMailRun,        &StPrintTapeDC,  &StPreRunDC0000},

// Waiting for BOB and CM responses to complete switch to report/testpat mode.  CM response received first.
{&StReadyDC0707, SEV_CMMODECONFIRM,  fnsDoNothing,           &StPrintRdyDC,   &StReadyDC0002},
// Waiting for BOB and CM responses to complete switch to report/testpat mode.  BOB response received first.
{&StReadyDC0707, SEV_BOBSTATICIMAGE, fnsDoNothing,           &StPrintRdyDC,   &StReadyDC0001},
// Run Pending (non-indicia)... go directly to running state
{&StReadyDC2627, SEV_CMMODECONFIRM,  fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0026},
// Run Pending (non-indicia)... go directly to running state
{&StReadyDC2527, SEV_BOBSTATICIMAGE, fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0025},
// Waiting for CM response only to complete switch to report/testpat mode.  CM response received means OIT can be notified of mode change.
{&StReadyDC0607, SEV_CMMODECONFIRM,  fnsOITSendAnyPREvent,   &StPrintRdyDC,   &StReadyDC0006},
// Waiting for BOB response only to complete switch to report/testpat mode.  BOB response received means OIT can be notified of mode change.
{&StReadyDC0507, SEV_BOBSTATICIMAGE, fnsOITSendAnyPREvent,   &StPrintRdyDC,   &StReadyDC0005},
// Run request received from CM while in the print ready-report or print ready-testpat state.
{&StReadyDC0007, SEV_CMRUN,          fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0000},
// Run request received from OIT while in the print ready-report or print ready-testpat state.  (This does nothing but prevents unhandled state trans).
{&StReadyDC0007, SEV_OIPRINT,        fnsDoNothing,           &StDontCareMode, &StDontCare0000},
// Switch from report ready or test pat ready to indicia ready
{&StReadyDC0000, SEV_OIENBLINDI,     fnsPrepIndiciaMode,     &StPrintReady,   &StReadyDC070F},
// Switch from report ready or test pat ready to permit ready
{&StReadyDC0000, SEV_OIENBLPERMIT,   fnsPrepPermitMode,      &StPrintPermit,  &StReadyDC070F},
// Switch from report ready or test pat ready to seal only ready
{&StReadyDC0000, SEV_OIENBLSEALONLY, fnsPrepSealOnlyMode,    &StPrintSeal,    &StReadyDC060F},

// Switch from test pat ready to report ready
{&StReadyDC0000, SEV_OIENBLREPORT,   fnsPrepReportMode,      &StPrintReport,  &StReadyDC070F},

// Print Test request
{&StReadyDC0000, SEV_OIENBLTEST,     fnsOITSendAnyPREvent,   &StDontCareMode,   &StDontCare0000},

// Catch-all states
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,           &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,    &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,           &StDontCareMode,   &StDontCare0000},

// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode, &StMeterErr,       &StDisabledDC0000},
{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};



// System Print Ready Report state table
static const STATE_TABLE_TYPE StPRReportState[] = {
// Transition to not ready state from any ready to print state
{&StReadyDC0000, SEV_OINOTREADY,     fnsCMDisable,           &StPrintNotRdy,  &StDisabledDC060E},
// Transition to sleep state from any ready to print state
{&StReadyDC0000, SEV_OISLEEP,        fnsCMSleep,             &StSleepNorm,    &StSleepDC060E},
// Transition to soft power down state from any ready to print state
{&StReadyDC0000, SEV_OISOFTPWRDOWN,  fnsCMSleep,             &StSleepPwrDwn,  &StSleepDC060E},

// DM400C: Start Run (any indicia type) in the print ready state, go to running state with CM prerunning state.
{&StReadyDC0007, SEV_OISTARTMAIL,    fnsStartMailRun,        &StRunningDC,    &StPreRunDC0000},
// Tape Run (indicia, permit, report, test pat) in the print ready state, go to tape_printing state with CM prerunning state.
{&StReadyDC0007, SEV_OISTARTTAPE,    fnsStartMailRun,        &StPrintTapeDC,  &StPreRunDC0000},

// Waiting for BOB and CM responses to complete switch to report/testpat mode.  CM response received first.
{&StReadyDC0707, SEV_CMMODECONFIRM,  fnsDoNothing,           &StPrintRdyDC,   &StReadyDC0002},
// Waiting for BOB and CM responses to complete switch to report/testpat mode.  BOB response received first.
{&StReadyDC0707, SEV_BOBSTATICIMAGE, fnsDoNothing,           &StPrintRdyDC,   &StReadyDC0001},
// Run Pending (non-indicia)... go directly to running state
{&StReadyDC2627, SEV_CMMODECONFIRM,  fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0026},
// Run Pending (non-indicia)... go directly to running state
{&StReadyDC2527, SEV_BOBSTATICIMAGE, fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0025},
// Waiting for CM response only to complete switch to report/testpat mode.  CM response received means OIT can be notified of mode change.
{&StReadyDC0607, SEV_CMMODECONFIRM,  fnsOITSendAnyPREvent,   &StPrintRdyDC,   &StReadyDC0006},
// Waiting for BOB response only to complete switch to report/testpat mode.  BOB response received means OIT can be notified of mode change.
{&StReadyDC0507, SEV_BOBSTATICIMAGE, fnsOITSendAnyPREvent,   &StPrintRdyDC,   &StReadyDC0005},
// Run request received from CM while in the print ready-report or print ready-testpat state.
{&StReadyDC0007, SEV_CMRUN,          fnsRun1stMailpiece,     &StRunningDC,    &StRunDC0000},
// Run request received from OIT while in the print ready-report or print ready-testpat state.  (This does nothing but prevents unhandled state trans).
{&StReadyDC0007, SEV_OIPRINT,        fnsDoNothing,           &StDontCareMode, &StDontCare0000},
// Switch from report ready or test pat ready to indicia ready
{&StReadyDC0000, SEV_OIENBLINDI,     fnsPrepIndiciaMode,     &StPrintReady,   &StReadyDC070F},
// Switch from report ready or test pat ready to permit ready
{&StReadyDC0000, SEV_OIENBLPERMIT,   fnsPrepPermitMode,      &StPrintPermit,  &StReadyDC070F},
// Switch from report ready or test pat ready to seal only ready
{&StReadyDC0000, SEV_OIENBLSEALONLY, fnsPrepSealOnlyMode,    &StPrintSeal,    &StReadyDC060F},

// Switch from report ready to test pat ready
{&StReadyDC0000, SEV_OIENBLTEST,     fnsPrepTestPatternMode, &StPrintTest,    &StReadyDC070F},


// Print Report request
{&StReadyDC0000, SEV_OIENBLREPORT,   fnsOITSendAnyPREvent,   &StDontCareMode,   &StDontCare0000},

// Catch-all states
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,           &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,    &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,           &StDontCareMode,   &StDontCare0000},

// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode, &StMeterErr,       &StDisabledDC0000},
{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};



// System Print Not Ready state table
static const STATE_TABLE_TYPE StPrintNotRdyState[] = {
// Waiting for CM response to complete switch to disabled mode.  CM response received means OIT can be notified of mode change.
{&StDisabledDC0606, SEV_CMMODECONFIRM,  fnsOITSendLeftPREvent,  &StPrintNotRdy, &StDisabledDC0006},
// No disabling conditions, confirm the mode change
//{&StDisabledDC0000, SEV_CMMODECONFIRM,  fnsOITSendAck,  &StPrintNotRdy, &StDisabledDC0000},
// Start of transition from not ready state to indicia ready state
{&StDisabledDC0000, SEV_OIENBLINDI,     fnsPrepIndiciaMode,     &StPrintReady,  &StReadyDC070F},
// Start of transition from not ready state to permit ready state
{&StDisabledDC0000, SEV_OIENBLPERMIT,   fnsPrepPermitMode,      &StPrintPermit, &StReadyDC070F},
// Start of transition from not ready state to report ready state
{&StDisabledDC0000, SEV_OIENBLREPORT,   fnsPrepReportMode,      &StPrintReport, &StReadyDC070F},
// Start of transition from not ready state to test pattern ready state
{&StDisabledDC0000, SEV_OIENBLTEST,     fnsPrepTestPatternMode, &StPrintTest,   &StReadyDC070F},
// Start of transition from not ready state to seal only ready state
{&StDisabledDC0000, SEV_OIENBLSEALONLY, fnsPrepSealOnlyMode,    &StPrintSeal,   &StReadyDC060F},
// Transition from print not ready to sleep
{&StDisabledDC0000, SEV_OISLEEP,        fnsCMSleep,             &StSleepNorm,   &StSleepDC060E},
// Transition from print not ready to soft power down
{&StDisabledDC0000, SEV_OISOFTPWRDOWN,  fnsCMSleep,             &StSleepPwrDwn, &StSleepDC060E},

// Request for Print Not Ready mode
{&StDontCare0000,   SEV_OINOTREADY,     fnsOITSendLeftPREvent,  &StDontCareMode,   &StDontCare0000},

// Catch-all states
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,           &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,    &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,           &StDontCareMode,   &StDontCare0000},

// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode, &StMeterErr,       &StDisabledDC0000},

// Transition to diagnostics mode
{&StDontCare0000,   SEV_OIDIAG,         fnsOITGoDiagnosticsMode, &StDiagnostics,   &StDiagDC0606},

// EDM
{&StDisabledDC0000, SEV_EDMLOCK,  fnsOITSendNextMode,  &StPrintNotRdyEdmLock, &StDisabledDC0000},
//{&StDisabledDC0000, SEV_EDMDISABLE, fnsOITSendNextMode, &StPrintNotRdyEdmLock, &StDisabledDC0000},
{&StDisabledDC0000, SEV_EDMDISABLE, fnsOITSendNextMode, &StPrintNotRdy, &StDisabledDC0000},

{&StDisabledDC0000,   SEV_ENTERMFG, fnsDoNothing,           &StManfMode,   &StDisabledDC0000},

{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};

// System Print Not Ready EDM Lock state table
static const STATE_TABLE_TYPE StPrintNotRdyEdmLockState[] = {
// EDM
{&StDisabledDC0000, SEV_OIENBLINDI,     fnsPrepIndiciaMode,     &StPrintReady,      &StReadyDC070F},
{&StDisabledDC0000, SEV_PMCDISABLED,  fnsOITSendMode,  &StPrintNotRdyEdmLock, &StDisabledDC0000},
{&StDisabledDC0000, SEV_EDMENABLE,  fnsOITAttemptEnable,  &StDontCareMode, &StDisabledDC0000},
{&StDisabledDC0000, SEV_EDMDISABLE,  fnsEDMResendMode,  &StDontCareMode, &StDisabledDC0000},
{&StDisabledDC0000, SEV_OINOTREADY, fnsEDMSendLeftPREvent,  &StDontCareMode,   &StDontCare0000},
{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};

// System Print Tape Indicia state table
static const STATE_TABLE_TYPE StPrintTapeIndiciaState[] = {
// Tape Run request received from CM while in the prerunning state.
{&StPreRunDC0000,   SEV_CMRUN,          fnsRun1stMailpiece,         &StPrintTapeDC,  &StRunDC0000},

// CM cancel current printing job
{&StRunDC0000,      SEV_CMCANCELRUN,    fnsDoNothing,               &StPrintTapeDC, &StRunDC1010},
// In run-cancelled state and a run request received.  User must have tried reinserting the envelope
{&StRunDC1010,      SEV_CMRUN,          fnsRunNextMailpiece,        &StPrintTapeDC, &StRunDC0010},
// Transition from running to printing
{&StRunDC0000,      SEV_CMPRINTSTARTED, fnsDoNothing,               &StPrintTapeDC, &StPrintDC0000},
// Transition from printing to running (used for 2nd, 3rd, 4th, etc. piece in a multipiece run)
{&StPrintDC0008,    SEV_CMRUN,          fnsRunNextMailpiece,        &StPrintTapeDC, &StRunDC0000},
// Transition from running/staging/printing to print not ready.  (used for jam handling or other disabling condition after print)
{&StDontCare0000,   SEV_OINOTREADY,     fnsCMDisable,               &StPrintNotRdy,  &StDisabledDC063E},
// Transition from printing to indicia ready (used for normal end of mailrun)
{&StPrintDC0000,    SEV_OIENBLINDI,     fnsPrepIndiciaMode_MailRun, &StPrintReady,   &StReadyDC070F},
// Transition from running/prerunning to indicia ready
{&StDontCare0000,   SEV_OIENBLINDI,     fnsPrepIndiciaMode,         &StPrintReady,   &StReadyDC071F},
// Transition from running/prerunning/printing to report ready (not common)
{&StDontCare0000,   SEV_OIENBLREPORT,   fnsPrepReportMode,          &StPrintReport,  &StReadyDC071F},

// No paper detected- states besides indicia
{&StRunDC0000,      SEV_CMCANCELRUN,    fnsDoNothing,           &StPrintTapeDC, &StRunDC1010},
// Run cancelled state (for non-indicias), and a run request received.  User must have tried reinserting.
{&StRunDC1010,      SEV_CMRUN,          fnsRunNextMailpiece,    &StPrintTapeDC, &StRunDC0010},
// Transition from running to printing
{&StRunDC0000,      SEV_CMPRINTSTARTED, fnsDoNothing,           &StPrintTapeDC, &StPrintDC0000},
 // Transition from printing to running (used for 2nd, 3rd, 4th, etc. piece in a multipiece run)
{&StPrintDC0008,    SEV_CMRUN,          fnsRunNextMailpiece,    &StPrintTapeDC, &StRunDC0000},
// Transition from running/staging/printing to print not ready.  (used for jam handling or other disabling condition after print)
{&StDontCare0000,   SEV_OINOTREADY,     fnsCMDisable,           &StPrintNotRdy,  &StDisabledDC063E},
// Mail job complete received
{&StDontCare0000,   SEV_CMMJCOMPLETE,   fnsDoNothing,           &StDontCareMode, &StDontCare0818},
// Transition from printing to report ready
{&StPrintDC0000,    SEV_OIENBLREPORT,   fnsPrepReportMode,      &StPrintReport,  &StReadyDC070F},
// Transition from running/prerunning to report ready
{&StDontCare0000,   SEV_OIENBLREPORT,   fnsPrepReportMode,      &StPrintReport,  &StReadyDC070F},
// Transition from running/prerunning/printing report to indicia ready
{&StDontCare0000,   SEV_OIENBLINDI,     fnsPrepIndiciaMode,     &StPrintReady,   &StReadyDC071F},
// Transition from running/prerunning/printing report to permit ready
{&StDontCare0000,   SEV_OIENBLPERMIT,   fnsPrepPermitMode,      &StPrintPermit,  &StReadyDC071F},
// Transition from running/prerunning/printing report to test pattern ready
{&StDontCare0000,   SEV_OIENBLTEST,     fnsPrepTestPatternMode, &StPrintTest,    &StReadyDC071F},
// Transition from running/prerunning/printing seal only ready (staging/printing should not be needed for seal only)
{&StDontCare0000,   SEV_OIENBLSEALONLY, fnsPrepSealOnlyMode, &StPrintSeal,    &StReadyDC060F},


// Start Tape request
{&StDontCare0000,   SEV_OISTARTTAPE,    fnsOITSendTapeEvent,    &StDontCareMode,   &StDontCare0000},

// EDM
{&StPrintDC0000,   SEV_EDMDISABLE,   fnsPMCQuickPause, &StPrintNotRdyEdmLock,       &StDisabledDC0000},

// Catch-all states
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,           &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,    &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,           &StDontCareMode,   &StDontCare0000},

// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode, &StMeterErr,       &StDisabledDC0000},
{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};



// System Print Tape Permit state table
static const STATE_TABLE_TYPE StPrintTapePermitState[] = {
// Tape Run request received from CM while in the prerunning state.
{&StPreRunDC0000,   SEV_CMRUN,          fnsRun1stMailpiece,     &StPrintTapeDC,  &StRunDC0000},

// CM cancel current permit printing job
{&StRunDC0000,      SEV_CMCANCELRUN,    fnsDoNothing,              &StPrintTapePer, &StRunDC1010},
// In run-cancelled state and a run request received.  User must have tried reinserting the envelope
{&StRunDC1010,      SEV_CMRUN,          fnsRunNextMailpiece,       &StPrintTapePer, &StRunDC0010},
// Transition from running to permit printing
{&StRunDC0000,      SEV_CMPRINTSTARTED, fnsDoNothing,              &StPrintTapePer, &StPrintDC0000},
// Transition from permit printing to running (used for 2nd, 3rd, 4th, etc. piece in a multipiece run)
{&StPrintDC0008,    SEV_CMRUN,          fnsRunNextMailpiece,       &StPrintTapePer, &StRunDC0000},
// Transition from running/staging/permit printing to print not ready.  (used for jam handling or other disabling condition after print)
{&StDontCare0000,   SEV_OINOTREADY,     fnsCMDisable,              &StPrintNotRdy,  &StDisabledDC063E},
// Transition from printing to permit printing ready (used for normal end of mailrun)
{&StPrintDC0000,    SEV_OIENBLINDI,     fnsPrepPermitMode_MailRun, &StPrintPermit,  &StReadyDC070F},
// Transition from running/prerunning to permit ready
{&StDontCare0000,   SEV_OIENBLPERMIT,   fnsPrepPermitMode,         &StPrintPermit,  &StReadyDC071F},
// Transition from running/prerunning/printing to report ready (not common)
{&StDontCare0000,   SEV_OIENBLREPORT,   fnsPrepReportMode,         &StPrintReport,  &StReadyDC071F},

// No paper detected- states besides indicia
{&StRunDC0000,      SEV_CMCANCELRUN,    fnsDoNothing,           &StPrintTapeDC, &StRunDC1010},
// Run cancelled state (for non-indicias), and a run request received.  User must have tried reinserting.
{&StRunDC1010,      SEV_CMRUN,          fnsRunNextMailpiece,    &StPrintTapeDC, &StRunDC0010},
// Transition from running to printing
{&StRunDC0000,      SEV_CMPRINTSTARTED, fnsDoNothing,           &StPrintTapeDC, &StPrintDC0000},
 // Transition from printing to running (used for 2nd, 3rd, 4th, etc. piece in a multipiece run)
{&StPrintDC0008,    SEV_CMRUN,          fnsRunNextMailpiece,    &StPrintTapeDC, &StRunDC0000},
// Transition from running/staging/printing to print not ready.  (used for jam handling or other disabling condition after print)
{&StDontCare0000,   SEV_OINOTREADY,     fnsCMDisable,           &StPrintNotRdy,  &StDisabledDC063E},
// Mail job complete received
{&StDontCare0000,   SEV_CMMJCOMPLETE,   fnsDoNothing,           &StDontCareMode, &StDontCare0818},
// Transition from printing to report ready
{&StPrintDC0000,    SEV_OIENBLREPORT,   fnsPrepReportMode,      &StPrintReport,  &StReadyDC070F},
// Transition from running/prerunning to report ready
{&StDontCare0000,   SEV_OIENBLREPORT,   fnsPrepReportMode,      &StPrintReport,  &StReadyDC070F},
// Transition from running/prerunning/printing report to indicia ready
{&StDontCare0000,   SEV_OIENBLINDI,     fnsPrepIndiciaMode,     &StPrintReady,   &StReadyDC071F},
// Transition from running/prerunning/printing report to permit ready
{&StDontCare0000,   SEV_OIENBLPERMIT,   fnsPrepPermitMode,      &StPrintPermit,  &StReadyDC071F},
// Transition from running/prerunning/printing report to test pattern ready
{&StDontCare0000,   SEV_OIENBLTEST,     fnsPrepTestPatternMode, &StPrintTest,    &StReadyDC071F},

// Start Tape request
{&StDontCare0000,   SEV_OISTARTTAPE,    fnsOITSendTapeEvent,    &StDontCareMode,   &StDontCare0000},

// EDM
{&StPrintDC0000,   SEV_EDMDISABLE,   fnsPMCQuickPause, &StPrintNotRdyEdmLock,       &StDisabledDC0000},

// Catch-all states
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,           &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,    &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,           &StDontCareMode,   &StDontCare0000},

// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode, &StMeterErr,       &StDisabledDC0000},
{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};



// System Running Indicia state table
static const STATE_TABLE_TYPE StRunningIndiciaState[] = {
// DM400C: Run request received from CM while in the prerunning state.
{&StPreRunDC0000,   SEV_CMRUN,          fnsRun1stMailpiece,          &StRunningDC,    &StRunDC0000},
// DM400C: CM cancel current printing job when in pre-running mode
{&StPreRunDC0000,   SEV_CMCANCELRUN,    fnsDoNothing,                &StRunningDC,    &StPreRunDC1010},

// CM cancel current printing job
{&StRunDC0000,      SEV_CMCANCELRUN,    fnsDoNothing,                &StRunningDC,   &StRunDC1010},
// In run-cancelled state and a run request received.  User must have tried reinserting the envelope
{&StRunDC1010,      SEV_CMRUN,          fnsRunNextMailpiece,         &StRunningDC,   &StRunDC0010},
// Transition from running to printing
{&StRunDC0000,      SEV_CMPRINTSTARTED, fnsDoNothing,                &StRunningDC,   &StPrintDC0000},
// Transition from printing to running (used for 2nd, 3rd, 4th, etc. piece in a multipiece run)
{&StPrintDC0008,    SEV_CMRUN,          fnsRunNextMailpiece,         &StRunningDC,   &StRunDC0000},
// Transition from running/staging/printing to print not ready.  (used for jam handling or other disabling condition after print)
{&StDontCare0000,   SEV_OINOTREADY,     fnsCMDisable,                &StPrintNotRdy,  &StDisabledDC063E},
// Transition from printing to indicia ready (used for normal end of mailrun)
{&StPrintDC0000,    SEV_OIENBLINDI,     fnsPrepIndiciaMode_MailRun,  &StPrintReady,   &StReadyDC070F},
// Transition from running/prerunning to indicia ready
{&StDontCare0000,   SEV_OIENBLINDI,     fnsPrepIndiciaMode,          &StPrintReady,   &StReadyDC071F},
// Transition from running/prerunning/printing to report ready (not common)
{&StDontCare0000,   SEV_OIENBLREPORT,   fnsPrepReportMode,           &StPrintReport,  &StReadyDC071F},


// No paper detected- states besides indicia
{&StRunDC0000,      SEV_CMCANCELRUN,    fnsDoNothing,                &StRunningDC,    &StRunDC1010},
// Run cancelled state (for non-indicias), and a run request received.  User must have tried reinserting.
{&StRunDC1010,      SEV_CMRUN,          fnsRunNextMailpiece,         &StRunningDC,    &StRunDC0010},

// Transition from running to printing
{&StRunDC0000,      SEV_CMPRINTSTARTED, fnsDoNothing,                &StRunningDC,    &StPrintDC0000},

// Transition from printing to running (used for 2nd, 3rd, 4th, etc. piece in a multipiece run)
{&StPrintDC0008,    SEV_CMRUN,          fnsRunNextMailpiece,         &StRunningDC,    &StRunDC0000},

// Transition from running/staging/printing to print not ready.  (used for jam handling or other disabling condition after print)
{&StDontCare0000,   SEV_OINOTREADY,     fnsCMDisable,                &StPrintNotRdy,  &StDisabledDC061E},
// Mail job complete received
{&StDontCare0000,   SEV_CMMJCOMPLETE,   fnsHandleMailJobComplete,    &StDontCareMode, &StDontCare0818},
// Transition from printing to report ready
{&StPrintDC0000,    SEV_OIENBLREPORT,   fnsPrepReportMode,           &StPrintReport,  &StReadyDC070F},
// Transition from running/prerunning to report ready
{&StDontCare0000,   SEV_OIENBLREPORT,   fnsPrepReportMode,           &StPrintReport,  &StReadyDC070F},
// Transition from running/prerunning/printing report to indicia ready
{&StDontCare0000,   SEV_OIENBLINDI,     fnsPrepIndiciaMode,          &StPrintReady,   &StReadyDC071F},
// Transition from running/prerunning/printing report to permit ready
{&StDontCare0000,   SEV_OIENBLPERMIT,   fnsPrepPermitMode,           &StPrintPermit,  &StReadyDC071F},
// Transition from running/prerunning/printing report to test pattern ready
{&StDontCare0000,   SEV_OIENBLTEST,     fnsPrepTestPatternMode,      &StPrintTest,    &StReadyDC071F},
// Transition from running/prerunning/printing seal only ready (staging/printing should not be needed for seal only)
{&StDontCare0000,   SEV_OIENBLSEALONLY, fnsPrepSealOnlyMode_MailRun, &StPrintSeal,    &StReadyDC061F},

// Request for Running Mode
{&StDontCare0000,   SEV_OIPRINT,        fnsOITSendRunEvent,          &StDontCareMode, &StDontCare0000},

// Catch-all states
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,           &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,    &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,           &StDontCareMode,   &StDontCare0000},

// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode, &StMeterErr,       &StDisabledDC0000},

// EDM
{&StPrintDC0000,   SEV_EDMDISABLE,   fnsPMCQuickPause, &StPrintNotRdyEdmLock,       &StDisabledDC0000},

{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};



// System Running Permit state table
static const STATE_TABLE_TYPE StRunningPermitState[] = {
// DM400C: Run request received from CM while in the prerunning state.
{&StPreRunDC0000,   SEV_CMRUN,          fnsRun1stMailpiece,          &StRunningDC,    &StRunDC0000},
// DM400C: CM cancel current printing job when in pre-running mode
{&StPreRunDC0000,   SEV_CMCANCELRUN,    fnsDoNothing,                &StRunningDC,    &StPreRunDC1010},

// CM cancel current permit printing job
{&StRunDC0000,      SEV_CMCANCELRUN,    fnsDoNothing,                &StRunningPer,   &StRunDC1010},
// In run-cancelled state and a run request received.  User must have tried reinserting the envelope
{&StRunDC1010,      SEV_CMRUN,          fnsRunNextMailpiece,         &StRunningPer,   &StRunDC0010},
// Transition from running to permit printing
{&StRunDC0000,      SEV_CMPRINTSTARTED, fnsDoNothing,                &StRunningPer,   &StPrintDC0000},
// Transition from permit printing to running (used for 2nd, 3rd, 4th, etc. piece in a multipiece run)
{&StPrintDC0008,    SEV_CMRUN,          fnsRunNextMailpiece,         &StRunningPer,   &StRunDC0000},
// Transition from running/staging/permit printing to print not ready.  (used for jam handling or other disabling condition after print)
{&StDontCare0000,   SEV_OINOTREADY,     fnsCMDisable,                &StPrintNotRdy,  &StDisabledDC061E},
// Transition from printing to permit printing ready (used for normal end of mailrun)
{&StPrintDC0000,    SEV_OIENBLINDI,     fnsPrepPermitMode_MailRun,   &StPrintPermit,  &StReadyDC070F},
// Transition from running/prerunning to permit ready
{&StDontCare0000,   SEV_OIENBLPERMIT,   fnsPrepPermitMode,           &StPrintPermit,  &StReadyDC071F},
// Transition from running/prerunning/printing to report ready (not common)
{&StDontCare0000,   SEV_OIENBLREPORT,   fnsPrepReportMode,           &StPrintReport,  &StReadyDC071F},

// No paper detected- states besides indicia
{&StRunDC0000,      SEV_CMCANCELRUN,    fnsDoNothing,                &StRunningDC,    &StRunDC1010},
// Run cancelled state (for non-indicias), and a run request received.  User must have tried reinserting.
{&StRunDC1010,      SEV_CMRUN,          fnsRunNextMailpiece,         &StRunningDC,    &StRunDC0010},

// Transition from running to printing
{&StRunDC0000,      SEV_CMPRINTSTARTED, fnsDoNothing,                &StRunningDC,    &StPrintDC0000},

// Transition from printing to running (used for 2nd, 3rd, 4th, etc. piece in a multipiece run)
{&StPrintDC0008,    SEV_CMRUN,          fnsRunNextMailpiece,         &StRunningDC,    &StRunDC0000},

// Transition from running/staging/printing to print not ready.  (used for jam handling or other disabling condition after print)
{&StDontCare0000,   SEV_OINOTREADY,     fnsCMDisable,                &StPrintNotRdy,  &StDisabledDC063E},
// Mail job complete received
{&StDontCare0000,   SEV_CMMJCOMPLETE,   fnsDoNothing,                &StDontCareMode, &StDontCare0818},
// Transition from printing to report ready
{&StPrintDC0000,    SEV_OIENBLREPORT,   fnsPrepReportMode,           &StPrintReport,  &StReadyDC070F},
// Transition from running/prerunning to report ready
{&StDontCare0000,   SEV_OIENBLREPORT,   fnsPrepReportMode,           &StPrintReport,  &StReadyDC070F},
// Transition from running/prerunning/printing report to indicia ready
{&StDontCare0000,   SEV_OIENBLINDI,     fnsPrepIndiciaMode,          &StPrintReady,   &StReadyDC071F},
// Transition from running/prerunning/printing report to permit ready
{&StDontCare0000,   SEV_OIENBLPERMIT,   fnsPrepPermitMode,           &StPrintPermit,  &StReadyDC071F},
// Transition from running/prerunning/printing report to test pattern ready
{&StDontCare0000,   SEV_OIENBLTEST,     fnsPrepTestPatternMode,      &StPrintTest,    &StReadyDC071F},
// Transition from running/prerunning/printing seal only ready (staging/printing should not be needed for seal only)
{&StDontCare0000,   SEV_OIENBLSEALONLY, fnsPrepSealOnlyMode_MailRun, &StPrintSeal,    &StReadyDC061F},

// Request for Running Mode
{&StDontCare0000,   SEV_OIPRINT,        fnsOITSendRunEvent,          &StDontCareMode, &StDontCare0000},

// Catch-all states
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,           &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,    &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,           &StDontCareMode,   &StDontCare0000},

// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode, &StMeterErr,       &StDisabledDC0000},
{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};



// System Running Seal state table
static const STATE_TABLE_TYPE StRunningSealState[] = {
// DM400C: Run request received from CM while in the prerunning state.
{&StPreRunDC0000,   SEV_CMRUN,          fnsRun1stMailpiece,          &StRunningDC,    &StRunDC0000},
// DM400C: CM cancel current printing job when in pre-running mode
{&StPreRunDC0000,   SEV_CMCANCELRUN,    fnsDoNothing,                &StRunningDC,    &StPreRunDC1010},

// No paper detected- states besides indicia
{&StRunDC0000,      SEV_CMCANCELRUN,    fnsDoNothing,                &StRunningDC,    &StRunDC1010},
// Run cancelled state (for non-indicias), and a run request received.  User must have tried reinserting.
{&StRunDC1010,      SEV_CMRUN,          fnsRunNextMailpiece,         &StRunningDC,    &StRunDC0010},

// Transition from running to printing
{&StRunDC0000,      SEV_CMPRINTSTARTED, fnsDoNothing,                &StRunningDC,    &StPrintDC0000},

// Transition from printing to running (used for 2nd, 3rd, 4th, etc. piece in a multipiece run)
{&StPrintDC0008,    SEV_CMRUN,          fnsRunNextMailpiece,         &StRunningDC,    &StRunDC0000},

// Transition from running/staging/printing to print not ready.  (used for jam handling or other disabling condition after print)
{&StDontCare0000,   SEV_OINOTREADY,     fnsCMDisable,                &StPrintNotRdy,  &StDisabledDC063E},

// In seal only mode, stage and print states are not used so we allow a run request when already running
{&StRunDC0008,      SEV_CMRUN,          fnsRunNextMailpiece,         &StRunningSeal,  &StRunDC0000},

// Mail job complete received - remain in seal only mode
{&StDontCare0000,   SEV_CMMJCOMPLETE,   fnsPrepSealOnlyMode_MailRun, &StPrintSeal,    &StReadyDC061F},
// Transition from printing to report ready
{&StPrintDC0000,    SEV_OIENBLREPORT,   fnsPrepReportMode,           &StPrintReport,  &StReadyDC070F},
// Transition from running/prerunning to report ready
{&StDontCare0000,   SEV_OIENBLREPORT,   fnsPrepReportMode,           &StPrintReport,  &StReadyDC070F},
// Transition from running/prerunning/printing report to indicia ready
{&StDontCare0000,   SEV_OIENBLINDI,     fnsPrepIndiciaMode,          &StPrintReady,   &StReadyDC071F},
// Transition from running/prerunning/printing report to permit ready
{&StDontCare0000,   SEV_OIENBLPERMIT,   fnsPrepPermitMode,           &StPrintPermit,  &StReadyDC071F},
// Transition from running/prerunning/printing report to test pattern ready
{&StDontCare0000,   SEV_OIENBLTEST,     fnsPrepTestPatternMode,      &StPrintTest,    &StReadyDC071F},
// Transition from running/prerunning/printing seal only ready (staging/printing should not be needed for seal only)
{&StDontCare0000,   SEV_OIENBLSEALONLY, fnsPrepSealOnlyMode_MailRun, &StPrintSeal,    &StReadyDC061F},

// Request for Running Mode
{&StDontCare0000,   SEV_OIPRINT,        fnsOITSendRunEvent,          &StDontCareMode, &StDontCare0000},

// Catch-all states
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,           &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,    &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,           &StDontCareMode,   &StDontCare0000},

// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode, &StMeterErr,       &StDisabledDC0000},
{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};



// System Running Test state table
static const STATE_TABLE_TYPE StRunningTestState[] = {
// DM400C: Run request received from CM while in the prerunning state.
{&StPreRunDC0000,   SEV_CMRUN,          fnsRun1stMailpiece,          &StRunningDC,    &StRunDC0000},
// DM400C: CM cancel current printing job when in pre-running mode
{&StPreRunDC0000,   SEV_CMCANCELRUN,    fnsDoNothing,                &StRunningDC,    &StPreRunDC1010},

// No paper detected- states besides indicia
{&StRunDC0000,      SEV_CMCANCELRUN,    fnsDoNothing,                &StRunningDC,    &StRunDC1010},
// Run cancelled state (for non-indicias), and a run request received.  User must have tried reinserting.
{&StRunDC1010,      SEV_CMRUN,          fnsRunNextMailpiece,         &StRunningDC,    &StRunDC0010},

// Transition from running to printing
{&StRunDC0000,      SEV_CMPRINTSTARTED, fnsDoNothing,                &StRunningDC,    &StPrintDC0000},

// Transition from printing to running (used for 2nd, 3rd, 4th, etc. piece in a multipiece run)
{&StPrintDC0008,    SEV_CMRUN,          fnsRunNextMailpiece,         &StRunningDC,    &StRunDC0000},

// Transition from running/staging/printing to print not ready.  (used for jam handling or other disabling condition after print)
{&StDontCare0000,   SEV_OINOTREADY,     fnsCMDisable,                &StPrintNotRdy,  &StDisabledDC063E},

// Mail job complete received - transition to not ready (from there can do print maintenance or another test print)
{&StDontCare0000,   SEV_CMMJCOMPLETE,   fnsCMDisable,                &StPrintNotRdy, &StDisabledDC063E},
// Transition from printing to report ready
{&StPrintDC0000,    SEV_OIENBLREPORT,   fnsPrepReportMode,           &StPrintReport,  &StReadyDC070F},
// Transition from running/prerunning to report ready
{&StDontCare0000,   SEV_OIENBLREPORT,   fnsPrepReportMode,           &StPrintReport,  &StReadyDC070F},
// Transition from running/prerunning/printing report to indicia ready
{&StDontCare0000,   SEV_OIENBLINDI,     fnsPrepIndiciaMode,          &StPrintReady,   &StReadyDC071F},
// Transition from running/prerunning/printing report to permit ready
{&StDontCare0000,   SEV_OIENBLPERMIT,   fnsPrepPermitMode,           &StPrintPermit,  &StReadyDC071F},
// Transition from running/prerunning/printing report to test pattern ready
{&StDontCare0000,   SEV_OIENBLTEST,     fnsPrepTestPatternMode,      &StPrintTest,    &StReadyDC071F},
// Transition from running/prerunning/printing seal only ready (staging/printing should not be needed for seal only)
{&StDontCare0000,   SEV_OIENBLSEALONLY, fnsPrepSealOnlyMode_MailRun, &StPrintSeal,    &StReadyDC061F},

// Request for Running Mode
{&StDontCare0000,   SEV_OIPRINT,        fnsOITSendRunEvent,          &StDontCareMode, &StDontCare0000},

// Catch-all states
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,           &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,    &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,           &StDontCareMode,   &StDontCare0000},

// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode, &StMeterErr,       &StDisabledDC0000},
{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};



// System Sleep state table
static const STATE_TABLE_TYPE StSleepState[] = {
// Catch-all states
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,                &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,         &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,                &StDontCareMode,   &StDontCare0000},

// Waiting for CM response to complete switch to disabled mode.  CM response received means OIT can be notified of mode change.
{&StSleepDC0606,    SEV_CMMODECONFIRM,  fnsOITSendAnySleepEvent,     &StSleepDC,      &StSleepDC0006},
// Transition from sleep to normal print ready
{&StSleepDC0000,    SEV_OIENBLINDI,     fnsPrepIndiciaModeSleep,     &StPrintReady,   &StReadyDC070F},
// Transition from sleep to permit print ready
{&StSleepDC0000,    SEV_OIENBLPERMIT,   fnsPrepPermitModeSleep,      &StPrintPermit,  &StReadyDC070F},
// Start of transition from sleep state to report ready state
{&StSleepDC0000,    SEV_OIENBLREPORT,   fnsPrepReportModeSleep,      &StPrintReport,  &StReadyDC070F},
// Start of transition from sleep state to test pattern ready state
{&StSleepDC0000,    SEV_OIENBLTEST,     fnsPrepTestPatternModeSleep, &StPrintTest,    &StReadyDC070F},
// Start of transition from sleep state to seal only ready state
{&StSleepDC0000,    SEV_OIENBLSEALONLY, fnsPrepSealOnlyModeSleep,    &StPrintSeal,    &StReadyDC060F},
// Transition to print not ready from sleep
{&StSleepDC0000,    SEV_OINOTREADY,     fnsPrepNotReadyModeSleep,    &StPrintNotRdy,  &StDisabledDC060F},
// Transition to meter error from sleep
{&StSleepDC0000,    SEV_OIMETERERROR,   fnsPrepNotReadyModeSleep,    &StMeterErr,     &StDisabledDC060E},
// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode,      &StMeterErr,     &StDisabledDC0000},

// Catch-all states
// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode, &StMeterErr,       &StDisabledDC0000},

// EDM
{&StSleepDC0000,   SEV_EDMENABLE,   fnsOITAttemptEnable, &StDontCareMode,       &StDontCare0000},

{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};



// System Meter Error state table
static const STATE_TABLE_TYPE StMeterErrState[] = {
// Catch-all states
// Run requests that happen after mail job complete but before an OIT mode change are ignored, but we do flag this cond for later handling.
{&StDontCare0808,   SEV_CMRUN,          fnsDoNothing,                &StDontCareMode,   &StDontCare2020},
// Catch-all for all other run requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMRUN,          fnsRejectRunRequest,         &StDontCareMode,   &StDontCare0000},
// Catch-all for print requests that happen in an abnormal state.
{&StDontCare0000,   SEV_CMPRINTSTARTED, fnsDoNothing,                &StDontCareMode,   &StDontCare0000},

// Transition to meter error mode is handled from all possible states with this transition.  May wish to break it up later.
{&StDontCare0000,   SEV_OIMETERERROR,   fnsOITGoMeterErrorMode,      &StMeterErr,       &StDisabledDC0000},

// Mode confirm in meter error state is only possible when waking up to this state from sleep mode
{&StDisabledDC0606, SEV_CMMODECONFIRM,  fnsOITSendMeterError,        &StMeterErr,     &StDisabledDC0006},
// Transition from meter error to a not ready to print screen
{&StDontCare0000,   SEV_OINOTREADY,     fnsCMDisableLogEnbl,         &StPrintNotRdy,  &StDisabledDC061E},
// Transition from meter error to indicia ready screen
{&StDontCare0000,   SEV_OIENBLINDI,     fnsPrepIndiciaModeLogEnbl,   &StPrintReady,   &StReadyDC071F},
// Transition from meter error to permit ready screen
{&StDontCare0000,   SEV_OIENBLPERMIT,   fnsPrepPermitModeLogEnbl,    &StPrintPermit,  &StReadyDC071F},
// Transition from meter error to seal only ready screen
{&StDontCare0000,   SEV_OIENBLSEALONLY, fnsPrepSealOnlyModeLogEnbl,  &StPrintSeal,    &StReadyDC061F},
// Transition from meter error to report ready screen
{&StDontCare0000,   SEV_OIENBLREPORT,   fnsPrepReportModeLogEnbl,    &StPrintReport,  &StReadyDC070F},
// Transition from meter error to test pattern ready screen
{&StDontCare0000,   SEV_OIENBLTEST,     fnsPrepTestPatternMode,      &StPrintTest,    &StReadyDC071F},
{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};

// System Diagnostics state table
static const STATE_TABLE_TYPE StDiagnosticsState[] = {

// Mode confirm in diagnostics state
{&StDiagDC0606,     SEV_CMMODECONFIRM,  fnsOITSendDiagnostics,  &StDiagnostics, &StDiagDC0006},

// Transition from diagnostics to not ready screen
{&StDiagDC0000,     SEV_OINOTREADY,     fnsCMDisable,           &StPrintNotRdy, &StDisabledDC061E},

// Start of transition from diagnostics state to indicia ready state
{&StDiagDC0000,     SEV_OIENBLINDI,     fnsPrepIndiciaMode,     &StPrintReady,  &StReadyDC070F},
// Start of transition from diagnostics state to permit ready state
{&StDiagDC0000,     SEV_OIENBLPERMIT,   fnsPrepPermitMode,      &StPrintPermit, &StReadyDC070F},
// Start of transition from diagnostics state to report ready state
{&StDiagDC0000,     SEV_OIENBLREPORT,   fnsPrepReportMode,      &StPrintReport, &StReadyDC070F},
// Start of transition from diagnostics state to test pattern ready state
{&StDiagDC0000,     SEV_OIENBLTEST,     fnsPrepTestPatternMode, &StPrintTest,   &StReadyDC070F},
// Start of transition from diagnostics state to seal only ready state
{&StDiagDC0000,     SEV_OIENBLSEALONLY, fnsPrepSealOnlyMode,    &StPrintSeal,   &StReadyDC060F},
// Transition from diagnostics to sleep
{&StDiagDC0000,     SEV_OISLEEP,        fnsCMSleep,             &StSleepNorm,   &StSleepDC060E},
// Transition from diagnostics to soft power down
{&StDiagDC0000,     SEV_OISOFTPWRDOWN,  fnsCMSleep,             &StSleepPwrDwn, &StSleepDC060E},

{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};

// System manufactory state table
static const STATE_TABLE_TYPE StMfgState[] = {

// Transition to Disable mode
{&StDontCare0000,   SEV_OINOTREADY,         fnsCMDisable, &StPrintNotRdy,   &StDisabledDC063E},

{0,                 SEV_ENDOFTABLE,     0,                      0,                 0}
};

static STATE_TABLE_TYPE* StStateTablePtr[SYS_NUM_OF_MODES][SYS_NUM_OF_SUBMODES];

static UINT8 UcEventID = 0xFF;
UINT8 UcLastEvent = OIT_ENTER_POWERUP;


/*---------------------------------------*/
/*   State transition functions (fns*)   */
/*---------------------------------------*/

/* **********************************************************************
// FUNCTION NAME: fnsDoNothing
// DESCRIPTION: State transition function that does the following:
//                  Nothing.
//              This allows an entry in the table to exist without calling
//              a function that does any work.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsDoNothing(SYS_STATE *pNextState)
{
    return;
}

/* **********************************************************************
// FUNCTION NAME: fnsPwrComplete
// DESCRIPTION: State transition function that does the following:
//                  Sends a powerup complete message to the OIT.  The
//                  OIT will decide where to go next and send a message
//                  to the system controller.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsPwrComplete(SYS_STATE *pNextState)
{
    if (fnCMOSSetupGetCMOSSetupParams()->AutoAdvTime != 0 )
    {
        fnSetAutoAdvanceTimeEvent();
    }

    OSSendIntertask (OIT, SYS, OIT_POWER_UP_COMPLETE, NO_DATA, "", 0); /* init ok, tell OIT */
}

/* **********************************************************************
// FUNCTION NAME: fnsPwrCompleteMfg
// DESCRIPTION: State transition function that does the following:
//                  Sends a mode change message to the OIT for manufacturing mode
//                  (taken from the table).
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsPwrCompleteMfg(SYS_STATE *pNextState)
{
//  fnsOITSendNextMode(pNextState);
    fnsOITSendManufEvent(pNextState);
}

/* **********************************************************************
// FUNCTION NAME: fnsOITGoMeterErrorMode
// DESCRIPTION: State transition function that does the following:
//                  Sends a mode change to OIT and EDM for meter error
//                      handling mode.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  { OIT_ERROR_HANDLING, OIT_METER_ERROR, ...(rest are unchanged) }
// *************************************************************************/
static void fnsOITGoMeterErrorMode(SYS_STATE *pNextState)
{
    fnsOITSendMeterError(pNextState);

    /* going to the meter error state automatically disables any message
        timeouts we currently have pending.  This will prevent the
        first meter error shown from being overwritten by another. */
    fnSetSYSMessageTimeout(NULL, 0, 0, SYS_TIMEOUT_TYPE_AND);

    // fnIntertaskMsgLogDisable();
}

/* **********************************************************************
// FUNCTION NAME: fnsGoMeterErrorEnblEDM
// DESCRIPTION: State transition function that does the following:
//                  Sends powerup complete message to EDM so that
//                      it enables remote communications
//                  Sends a mode change to OIT and EDM for meter error
//                      handling mode.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  { OIT_ERROR_HANDLING, OIT_METER_ERROR, ...(rest are unchanged) }
// *************************************************************************/
static void fnsGoMeterErrorEnblEDM(SYS_STATE *pNextState)
{
 //   unsigned char   pMsgData[2];

    fnsOITGoMeterErrorMode(pNextState);
}

/* **********************************************************************
// FUNCTION NAME: fnsOITSendNextMode
// DESCRIPTION: State transition function that does the following:
//                  Sends the next mode to OIT and EDM (if necessary).
//                      (Same as fnsOITSendMode except that the next
//                      mode is used instead of the current mode)
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsOITSendNextMode(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2];

  if (pNextState->wSYSMode != SYS_DONT_CARE)
    pMsgData[0] = (unsigned char) pNextState->wSYSMode;
    else
    pMsgData[0] = (unsigned char) rCurrentState.wSYSMode;

  if (pNextState->wSYSSubMode != SYS_DONT_CARE)
    pMsgData[1] = (unsigned char) pNextState->wSYSSubMode;
  else
    pMsgData[1] = (unsigned char) rCurrentState.wSYSSubMode;

  OSSendIntertask (OIT, SYS, OIT_CHANGE_MODE, BYTE_DATA, pMsgData, 2);

}

/* *************************************************************************
// FUNCTION NAME:
//      fnsPrepIndiciaMode
//
// DESCRIPTION:
//      State transition function that does the following:
//      Prepares BOB and CM to enter indicia printing
//      mode.
//
// INPUTS:
//      pNextState  - pointer to next state
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  11/06/2007  Vincent Yi      Moved fnAGDSetupStaticMailRun() called in fnsPrepIndiciaMode()
//                              back to fnsRun1stMailpiece only for envelope on DM300C
//  05/14/2007  Oscar Wang      DM400 has no time to generate image if we 
//                              call function fnAGDSetupStaticMailRun() in 
//                              fnsRun1stMailpiece(), so we move the function 
//                              call early here.
//              Joe Mozdzer     Initial version
// *************************************************************************/
static void fnsPrepIndiciaMode(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[1];
    SYS_MSG_TIMEOUT pTimeouts[2];
    unsigned char   bNumTimeouts = 0;
    MAIL_SIMULATION simuParmsCopy;
    enum BOB_PRINT_MODES bBobMode = BOB_DEBIT_FUNDS; // default to debit funds mode

    if (fnGetMailSimuPara(&simuParmsCopy) == TRUE)
    {
        pMsgData[0] = (unsigned char) CM_SIMULATION_MODE;
    }
    else
    {
        /*  for differential weight */
        if (fnOITGetJanusPrintOption() == PMODE_DIFFERENTIAL_WEIGH)
        {
            pMsgData[0] = (unsigned char) CM_DIFF_WEIGH_MODE;
        }
        else
        {
            pMsgData[0] = (unsigned char) CM_INDICIA_MODE;
        }
    }

    pTimeouts[bNumTimeouts].bTaskID = CM;
    pTimeouts[bNumTimeouts++].bMessageID = CS_MODE_RSP;
    OSSendIntertask (CM, SYS, SC_MODE_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));

    switch (fnOITGetJanusPrintOption())
    {
        case AD_ONLY: // Indicia Ad only
                bBobMode = BOB_AD_ONLY_MODE;
            break;
        case DATE_STAMP_ONLY: // Indicia Date Time Stamp only
                bBobMode = BOB_DATE_STAMP_ONLY_MODE;
            break;
        case DATE_STAMP: // Indicia Ad Date Time
                bBobMode = BOB_DATE_STAMP_MODE;
            break;
        default:
            // debit funds mode
            break;
    }

    // set bob print mode
    fnSendBobPrintMode(bBobMode);

    pTimeouts[bNumTimeouts].bTaskID = SCM;
    pTimeouts[bNumTimeouts++].bMessageID = BOB_GEN_STATIC_IMAGE_RSP;
    OSSendIntertask (SCM, SYS, BOB_GEN_STATIC_IMAGE_REQ, NO_DATA, NULL, 0);

    fnSetSYSMessageTimeout(pTimeouts, bNumTimeouts, 20000, SYS_TIMEOUT_TYPE_AND);

//#ifdef DEBUG_CM
#ifdef RD_CSD3_TEST
    OSSendIntertask(SYS, CM, CS_MODE_RSP, NO_DATA, NULL, 0);
#endif // #ifdef DEBUG_CM

}

/* **********************************************************************
// FUNCTION NAME: fnsPrepPermitMode
// DESCRIPTION: State transition function that does the following:
//                  Prepares BOB and CM to enter permit printing
//                  mode.
//
// AUTHOR: Howell Sun (modified from fnsPrepIndiciaMode())
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsPrepPermitMode(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[1];
    SYS_MSG_TIMEOUT pTimeouts[2];
    unsigned char   bNumTimeouts = 0;

    pMsgData[0] = (unsigned char) CM_PERMIT_MODE;

    /* If data capture is enabled, inform DCAPI of any changes in postage mode. */
    // if (fnDCAPIsDataCaptureActive())
    //  fnDCAPSelectPostageMode();

    pTimeouts[bNumTimeouts].bTaskID = CM;
    pTimeouts[bNumTimeouts++].bMessageID = CS_MODE_RSP;
    OSSendIntertask (CM, SYS, SC_MODE_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));

    fnSendBobPrintMode(BOB_PERMIT_MODE);

    pTimeouts[bNumTimeouts].bTaskID = SCM;
    pTimeouts[bNumTimeouts++].bMessageID = BOB_GEN_STATIC_IMAGE_RSP;
    OSSendIntertask (SCM, SYS, BOB_GEN_STATIC_IMAGE_REQ, NO_DATA, NULL, 0);

    fnSetSYSMessageTimeout(pTimeouts, bNumTimeouts, 20000, SYS_TIMEOUT_TYPE_AND);

} // fnsPrepPermitMode



/* **********************************************************************
// FUNCTION NAME: fnsPrepIndiciaMode_MailRun
// DESCRIPTION: State transition function that does the following:
//                  Prepares BOB and CM to enter indicia printing
//                  mode.
//              Modified so it won't send CM the duplicate mode request,
//              so far SYS sends it for each mail run - Howell
//              Remove comment in front of SC_MODE_REQ intertask message 
//              to avoid printing indicia in report mode after a print 
//              failure report  - I. Le Goff
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsPrepIndiciaMode_MailRun(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[1];
    SYS_MSG_TIMEOUT pTimeouts[2];
    unsigned char   bNumTimeouts = 0;
    MAIL_SIMULATION simuParmsCopy;
    enum BOB_PRINT_MODES bBobMode = BOB_DEBIT_FUNDS; // default to debit funds mode

    ulong       rspData[LEN_3];

    if (fnGetMailSimuPara(&simuParmsCopy) == TRUE)
        pMsgData[0] = (unsigned char) CM_SIMULATION_MODE;
    else
        pMsgData[0] = (unsigned char) CM_INDICIA_MODE;

    pTimeouts[bNumTimeouts].bTaskID = CM;
    pTimeouts[bNumTimeouts++].bMessageID = CS_MODE_RSP;

    rspData[POS_0] = 0;
    rspData[POS_1] = CM_INDICIA_MODE;
    rspData[POS_2] = 0;

    // respond to SYS
    OSSendIntertask(SYS, CM, CS_MODE_RSP, LONG_DATA, rspData, sizeof(rspData));

    // Remove comment to avoid printing indicia in report mode after a print 
    // failure report. The mode needs to be set back to CM_INDICIA_MODE  - I. Le Goff
    OSSendIntertask (CM, SYS, SC_MODE_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));

    switch (fnOITGetJanusPrintOption())
    {
        case AD_ONLY: // Indicia Ad only
                bBobMode = BOB_AD_ONLY_MODE;
            break;
        case DATE_STAMP_ONLY: // Indicia Date Time Stamp only
                bBobMode = BOB_DATE_STAMP_ONLY_MODE;
            break;
        case DATE_STAMP: // Indicia Ad Date Time
                bBobMode = BOB_DATE_STAMP_MODE;
            break;
        default:
            // debit funds mode
            break;
    }

    // set bob print mode
    fnSendBobPrintMode(bBobMode);

    pTimeouts[bNumTimeouts].bTaskID = SCM;
    pTimeouts[bNumTimeouts++].bMessageID = BOB_GEN_STATIC_IMAGE_RSP;
    OSSendIntertask (SCM, SYS, BOB_GEN_STATIC_IMAGE_REQ, NO_DATA, NULL, 0);

    fnSetSYSMessageTimeout(pTimeouts, bNumTimeouts, 20000, SYS_TIMEOUT_TYPE_AND);
    fnDumpStringToSystemLog("SYS: MAIL_RUN");

#ifdef DEBUG_CM
    OSSendIntertask(SYS, CM, CS_MODE_RSP, NO_DATA, NULL, 0);
#endif // #ifdef DEBUG_CM

} // fnsPrepIndiciaMode_MailRun()


/* **********************************************************************
// FUNCTION NAME: fnsPrepPermitMode_MailRun
// DESCRIPTION: State transition function that does the following:
//                  Prepares BOB and CM to enter indicia printing
//                  mode.
//              Modified so it won't send CM the duplicate mode request,
//              so far SYS sends it for each mail run - Howell
//
// AUTHOR: Howell Sun (modified from fnsPrepIndiciaMode_MailRun() )
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsPrepPermitMode_MailRun(SYS_STATE *pNextState)
{
//    unsigned char   pMsgData[1];
    SYS_MSG_TIMEOUT pTimeouts[2];
    unsigned char   bNumTimeouts = 0;

    ulong       rspData[LEN_3];

//    pMsgData[0] = (unsigned char) CM_INDICIA_MODE;

    pTimeouts[bNumTimeouts].bTaskID = CM;
    pTimeouts[bNumTimeouts++].bMessageID = CS_MODE_RSP;

    rspData[POS_0] = 0;
    rspData[POS_1] = CM_PERMIT_MODE;
    rspData[POS_2] = 0;

    // respond to SYS
    OSSendIntertask(SYS, CM, CS_MODE_RSP, LONG_DATA, rspData, sizeof(rspData));

    // OSSendIntertask (CM, SYS, SC_MODE_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));

    fnSendBobPrintMode(BOB_PERMIT_MODE);

    pTimeouts[bNumTimeouts].bTaskID = SCM;
    pTimeouts[bNumTimeouts++].bMessageID = BOB_GEN_STATIC_IMAGE_RSP;
    OSSendIntertask (SCM, SYS, BOB_GEN_STATIC_IMAGE_REQ, NO_DATA, NULL, 0);

    fnSetSYSMessageTimeout(pTimeouts, bNumTimeouts, 20000, SYS_TIMEOUT_TYPE_AND);
    fnDumpStringToSystemLog("SYS: MAIL_RUN");

} // fnsPrepPermitMode_MailRun()



/* **********************************************************************
// FUNCTION NAME: fnsPrepIndiciaModeSleep
// DESCRIPTION: Wakes the CM up from sleep mode, then does the exact
//              same thing as fnsPrepIndiciaMode.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  N/A- same behavior as fnsPrepIndiciaMode
// *************************************************************************/
static void fnsPrepIndiciaModeSleep(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2];
//    unsigned char   chEventStatus;
    unsigned long   lwCurrentEvents;

    if (rCurrentState.wSYSSubMode == OIT_SOFT_POWER_DOWN)
        pMsgData[0] = 0;    /* POWER UP INIT */
    else
        pMsgData[0] = 1;    /* SLEEP_WAKEUP_INIT */
    pMsgData[1] = 20;   /* DISABLED_MODE        */

    OSSetEvents(SYS_PWRUP_EVENT_GROUP, ~ (unsigned long) EV_CM_INIT_COMPLETE, OS_AND);

#ifndef DEBUG_CM
    OSSendIntertask(CM, SYS, SC_INIT_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));
#endif

//    chEventStatus =
    OSReceiveEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE,
                                    OS_SUSPEND, OS_AND, &lwCurrentEvents);

#ifdef DEBUG_CM
    OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE, OS_OR);
    OSSendIntertask(SYS, CM, CS_INIT_RSP, NO_DATA, NULL, 0);
#endif // #ifdef DEBUG_CM

    fnsPrepIndiciaMode(pNextState);
}

/* **********************************************************************
// FUNCTION NAME: fnsPrepPermitModeSleep
// DESCRIPTION: Wakes the CM up from sleep mode, then does the exact
//              same thing as fnsPrepPermitMode.
//
// AUTHOR: Howell Sun (modified from fnsPrepIndiciaModeSleep() )
//
// NEXT STATE:  N/A- same behavior as fnsPrepPermitMode
// *************************************************************************/
static void fnsPrepPermitModeSleep(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2];
//    unsigned char   chEventStatus;
    unsigned long   lwCurrentEvents;

    if (rCurrentState.wSYSSubMode == OIT_SOFT_POWER_DOWN)
        pMsgData[0] = 0;    /* POWER UP INIT */
    else
        pMsgData[0] = 1;    /* SLEEP_WAKEUP_INIT */
    pMsgData[1] = 20;   /* DISABLED_MODE        */

    OSSetEvents(SYS_PWRUP_EVENT_GROUP, ~ (unsigned long) EV_CM_INIT_COMPLETE, OS_AND);

#ifndef DEBUG_CM
    OSSendIntertask(CM, SYS, SC_INIT_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));
#endif

//    chEventStatus =
    OSReceiveEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE,
                                    OS_SUSPEND, OS_AND, &lwCurrentEvents);

#ifdef DEBUG_CM
    OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE, OS_OR);
    OSSendIntertask(SYS, CM, CS_INIT_RSP, NO_DATA, NULL, 0);
#endif // #ifdef DEBUG_CM

    fnsPrepPermitMode(pNextState);

} // fnsPrepPermitModeSleep


/* **********************************************************************
// FUNCTION NAME: fnsPrepNotReadyModeSleep
// DESCRIPTION: Wakes the CM up from sleep mode, then does the exact
//              same thing as fnsCMDisable.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  N/A- same behavior as fnsPrepIndiciaMode
// *************************************************************************/
static void fnsPrepNotReadyModeSleep(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2];
//    unsigned char   chEventStatus;
    unsigned long   lwCurrentEvents;

    if (rCurrentState.wSYSSubMode == OIT_SOFT_POWER_DOWN)
        pMsgData[0] = 0;    /* POWER UP INIT */
    else
        pMsgData[0] = 1;    /* SLEEP_WAKEUP_INIT */
    pMsgData[1] = 20;   /* DISABLED_MODE        */

    OSSetEvents(SYS_PWRUP_EVENT_GROUP, ~ (unsigned long) EV_CM_INIT_COMPLETE, OS_AND);

#ifndef DEBUG_CM
    OSSendIntertask(CM, SYS, SC_INIT_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));
#endif

//    chEventStatus =
    OSReceiveEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE,
                                    OS_SUSPEND, OS_AND, &lwCurrentEvents);

#ifdef DEBUG_CM
    OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE, OS_OR);
    OSSendIntertask(SYS, CM, CS_INIT_RSP, NO_DATA, NULL, 0);
#endif // #ifdef DEBUG_CM

    fnsCMDisable(pNextState);
}

/* **********************************************************************
// FUNCTION NAME: fnsPrepIndiciaModeLogEnbl
// DESCRIPTION: Does the exact same thing as fnsPrepIndiciaMode, plus it enables
//              intertask message logging.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  N/A- same behavior as fnsPrepIndiciaMode
// *************************************************************************/
static void fnsPrepIndiciaModeLogEnbl(SYS_STATE *pNextState)
{
// TODO: function not yet implemented
 //   fnIntertaskMsgLogEnable();
    fnsPrepIndiciaMode(pNextState);
}

/* **********************************************************************
// FUNCTION NAME: fnsPrepPermitModeLogEnbl
// DESCRIPTION: Does the exact same thing as fnsPrepIndiciaMode, plus it enables
//              intertask message logging.
//
// AUTHOR: Howell Sun (modified from fnsPrepIndiciaModeLogEnbl() )
//
// NEXT STATE:  N/A- same behavior as fnsPrepIndiciaMode
// *************************************************************************/
static void fnsPrepPermitModeLogEnbl(SYS_STATE *pNextState)
{
    fnIntertaskMsgLogEnable();
    fnsPrepIndiciaMode(pNextState);

} // fnsPrepPermitModeLogEnbl()


/* **********************************************************************
// FUNCTION NAME: fnsPrepReportMode
// DESCRIPTION: State transition function that does the following:
//                  Prepares BOB and CM to enter report printing
//                  mode.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsPrepReportMode(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[1];
    SYS_MSG_TIMEOUT pTimeouts[2];
    unsigned char   bNumTimeouts = 0;

    pMsgData[0] = (unsigned char) CM_REPORT_MODE;
    pTimeouts[bNumTimeouts].bTaskID = CM;
    pTimeouts[bNumTimeouts++].bMessageID = CS_MODE_RSP;
    OSSendIntertask (CM, SYS, SC_MODE_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));

    fnSendBobPrintMode(BOB_REPORT_MODE);

    pTimeouts[bNumTimeouts].bTaskID = SCM;
    pTimeouts[bNumTimeouts++].bMessageID = BOB_GEN_STATIC_IMAGE_RSP;
    OSSendIntertask (SCM, SYS, BOB_GEN_STATIC_IMAGE_REQ, NO_DATA, NULL, 0);

    fnSetSYSMessageTimeout(pTimeouts, bNumTimeouts, 20000, SYS_TIMEOUT_TYPE_AND);
}

/* **********************************************************************
// FUNCTION NAME: fnsPrepReportModeSleep
// DESCRIPTION: Wakes the CM up from sleep, then does the exact same
//              thing as fnsPrepReportMode.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  N/A- same behavior as fnsPrepReportMode
// *************************************************************************/
static void fnsPrepReportModeSleep(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2];
//    unsigned char   chEventStatus;
    unsigned long   lwCurrentEvents;

    if (rCurrentState.wSYSSubMode == OIT_SOFT_POWER_DOWN)
        pMsgData[0] = 0;    /* POWER UP INIT */
    else
        pMsgData[0] = 1;    /* SLEEP_WAKEUP_INIT */
    pMsgData[1] = 20;   /* DISABLED_MODE        */

    OSSetEvents(SYS_PWRUP_EVENT_GROUP, ~ (unsigned long) EV_CM_INIT_COMPLETE, OS_AND);

#ifndef DEBUG_CM
    OSSendIntertask(CM, SYS, SC_INIT_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));
#endif // #ifdef DEBUG_CM

//    chEventStatus =
    OSReceiveEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE,
                                    OS_SUSPEND, OS_AND, &lwCurrentEvents);

#ifdef DEBUG_CM
    OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE, OS_OR);
    OSSendIntertask(SYS, CM, CS_INIT_RSP, NO_DATA, NULL, 0);
#endif // #ifdef DEBUG_CM

    fnsPrepReportMode(pNextState);
}

/* **********************************************************************
// FUNCTION NAME: fnsPrepReportModeLogEnbl
// DESCRIPTION: Does the exact same thing as fnsPrepReportMode, plus it enables
//              intertask message logging.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  N/A- same behavior as fnsPrepIndiciaMode
// *************************************************************************/
static void fnsPrepReportModeLogEnbl(SYS_STATE *pNextState)
{
    fnIntertaskMsgLogEnable();
    fnsPrepReportMode(pNextState);
}


/* **********************************************************************
// FUNCTION NAME: fnsPrepTestPatternMode
// DESCRIPTION: State transition function that does the following:
//                  Prepares BOB and CM to enter test pattern
//                  printing mode.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsPrepTestPatternMode(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[1];
    SYS_MSG_TIMEOUT pTimeouts[2];
    unsigned char   bNumTimeouts = 0;

    pMsgData[0] = (unsigned char) CM_TEST_PAT_MODE;
    pTimeouts[bNumTimeouts].bTaskID = CM;
    pTimeouts[bNumTimeouts++].bMessageID = CS_MODE_RSP;
    OSSendIntertask (CM, SYS, SC_MODE_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));

    fnSendBobPrintMode(BOB_TEST_PRINT_MODE);

    pTimeouts[bNumTimeouts].bTaskID = SCM;
    pTimeouts[bNumTimeouts++].bMessageID = BOB_GEN_STATIC_IMAGE_RSP;
    OSSendIntertask (SCM, SYS, BOB_GEN_STATIC_IMAGE_REQ, NO_DATA, NULL, 0);

    fnSetSYSMessageTimeout(pTimeouts, bNumTimeouts, 20000, SYS_TIMEOUT_TYPE_AND);

#ifdef DEBUG_CM
    OSSendIntertask(SYS, CM, CS_MODE_RSP, NO_DATA, NULL, 0);
#endif // #ifdef DEBUG_CM

}

/* **********************************************************************
// FUNCTION NAME: fnsPrepTestPatternModeSleep
// DESCRIPTION: Wakes the CM up from sleep, then does the exact same
//              thing as fnsPrepTestPatternMode.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  N/A- same behavior as fnsPrepReportMode
// *************************************************************************/
static void fnsPrepTestPatternModeSleep(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2];
//    unsigned char   chEventStatus;
    unsigned long   lwCurrentEvents;

    if (rCurrentState.wSYSSubMode == OIT_SOFT_POWER_DOWN)
        pMsgData[0] = 0;    /* POWER UP INIT */
    else
        pMsgData[0] = 1;    /* SLEEP_WAKEUP_INIT */
    pMsgData[1] = 20;   /* DISABLED_MODE        */

    OSSetEvents(SYS_PWRUP_EVENT_GROUP, ~ (unsigned long) EV_CM_INIT_COMPLETE, OS_AND);

#ifndef DEBUG_CM
    OSSendIntertask(CM, SYS, SC_INIT_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));
#endif

//    chEventStatus =
    OSReceiveEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE,
                                    OS_SUSPEND, OS_AND, &lwCurrentEvents);

#ifdef DEBUG_CM
    OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE, OS_OR);
    OSSendIntertask(SYS, CM, CS_INIT_RSP, NO_DATA, NULL, 0);
#endif // #ifdef DEBUG_CM

    fnsPrepTestPatternMode(pNextState);
}

/* **********************************************************************
// FUNCTION NAME: fnsPrepSealOnlyMode
// DESCRIPTION: State transition function that does the following:
//                  Prepares BOB and CM to enter seal only mode.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsPrepSealOnlyMode(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[1];
    SYS_MSG_TIMEOUT pTimeouts[2];
    unsigned char   bNumTimeouts = 0;

    pMsgData[0] = (unsigned char) CM_SEAL_ONLY_MODE;
    pTimeouts[bNumTimeouts].bTaskID = CM;
    pTimeouts[bNumTimeouts++].bMessageID = CS_MODE_RSP;
    OSSendIntertask (CM, SYS, SC_MODE_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));

    fnSendBobPrintMode(BOB_SEAL_MODE);

    fnSetSYSMessageTimeout(pTimeouts, bNumTimeouts, 10000, SYS_TIMEOUT_TYPE_AND);

#ifdef DEBUG_CM
    OSSendIntertask(SYS, CM, CS_MODE_RSP, NO_DATA, NULL, 0);
#endif // #ifdef DEBUG_CM
}




/* **********************************************************************
// FUNCTION NAME: fnsPrepSealOnlyMode_MailRun
// DESCRIPTION: State transition function that does the following:
//                  Prepares BOB and CM to enter seal only mode.
//
//              Modified so it won't send CM the duplicate mode request,
//              so far SYS sends it for each mail run - Howell
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsPrepSealOnlyMode_MailRun(SYS_STATE *pNextState)
{
//    unsigned char   pMsgData[1];
    SYS_MSG_TIMEOUT pTimeouts[2];
    unsigned char   bNumTimeouts = 0;

    ulong       rspData[LEN_3];


//    pMsgData[0] = (unsigned char) CM_SEAL_ONLY_MODE;
    pTimeouts[bNumTimeouts].bTaskID = CM;
    pTimeouts[bNumTimeouts++].bMessageID = CS_MODE_RSP;

    rspData[POS_0] = 0;
    rspData[POS_1] = CM_SEAL_ONLY_MODE;
    rspData[POS_2] = 0;

    OSSendIntertask(SYS, CM, CS_MODE_RSP, LONG_DATA, rspData, sizeof(rspData));

    // OSSendIntertask (CM, SYS, SC_MODE_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData)); - Howell

    fnSendBobPrintMode(BOB_SEAL_MODE);

    fnSetSYSMessageTimeout(pTimeouts, bNumTimeouts, 10000, SYS_TIMEOUT_TYPE_AND);
    fnDumpStringToSystemLog("SYS: SEAL_RUN");
}


/* **********************************************************************
// FUNCTION NAME: fnsPrepSealOnlyModeSleep
// DESCRIPTION: Wakes the CM up from sleep, then does the exact same
//              thing as fnsPrepSealOnlyMode.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  N/A- same behavior as fnsPrepReportMode
// *************************************************************************/
static void fnsPrepSealOnlyModeSleep(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2];
//    unsigned char   chEventStatus;
    unsigned long   lwCurrentEvents;

    if (rCurrentState.wSYSSubMode == OIT_SOFT_POWER_DOWN)
        pMsgData[0] = 0;    /* POWER UP INIT */
    else
        pMsgData[0] = 1;    /* SLEEP_WAKEUP_INIT */
    pMsgData[1] = 20;   /* DISABLED_MODE        */

    OSSetEvents(SYS_PWRUP_EVENT_GROUP, ~ (unsigned long) EV_CM_INIT_COMPLETE, OS_AND);

#ifndef DEBUG_CM
    OSSendIntertask(CM, SYS, SC_INIT_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));
#endif

//    chEventStatus =
    OSReceiveEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE,
                                    OS_SUSPEND, OS_AND, &lwCurrentEvents);

#ifdef DEBUG_CM
    OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE, OS_OR);
    OSSendIntertask(SYS, CM, CS_INIT_RSP, NO_DATA, NULL, 0);
#endif // #ifdef DEBUG_CM

    fnsPrepSealOnlyMode(pNextState);
}

/* **********************************************************************
// FUNCTION NAME: fnsPrepSealOnlyModeLogEnbl
// DESCRIPTION: Does the exact same thing as fnsPrepSealOnlyMode, plus it enables
//              intertask message logging.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  N/A- same behavior as fnsPrepSealOnlyMode
// *************************************************************************/
static void fnsPrepSealOnlyModeLogEnbl(SYS_STATE *pNextState)
{
    fnIntertaskMsgLogEnable();
    fnsPrepSealOnlyMode(pNextState);
}

/* *************************************************************************
// FUNCTION NAME:           fnsRun1stMailpiece
// DESCRIPTION:
//      State transition function that handles a transition to the running
//      state for the first mailpiece in a mailrun.
//
// INPUTS:
//      pNextState  - pointer to next state
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
// 1. Currently, the Inview spec indicates that accounting information IS required
//    for TextAdDateTimeStamp mode.  However, that requirement may change.  So, the  
//    external variables fAbAcctRequiredForTextAdDateTime and fHostAbAcctRequiredForTextAdDateTime
//    will be set according to the requirements (someplace else.)  This function 
//    uses those two variables to determine whether to check for a any account 
//    problems when in TextAdDateTimeStamp mode.
//
// MODIFICATION HISTORY:
//  2012.09.14 Clarisa Bellamy - Fraca fix 139154 - Except that the fraca is not 
//                      quite right.  See Note 1 above.  Now, use the new function
//                      fnIsAbActReqForTxtAdDateTime to determine if an account
//                      selection is required in TextAdDateTimeStamp modes. 
//   2012.08.14 Liu Jupeng      Merged the fixing fraca 130246 from the branch fphx02.08_c1_secap.
//      -2011.06.22 Steven Peng merge from fphx02.05_c1_cienet for fixing Fraca#130246:
//      - 01/26/2011  Joey Cui    Modified function fnsRun1stMailpiece() & fnsRunNextMailpiece() 
//                              to fix FRACA GMSE00199820
//  11/06/2007  Vincent Yi      Moved fnAGDSetupStaticMailRun() called in fnsPrepIndiciaMode()
//                              back to fnsRun1stMailpiece only for envelope on DM300C
//  05/11/2007  Oscar Wang      Move fnAGDSetupStaticMailRun to fnsPrepIndiciaMode
//                              since here is too late to call the function.
//  04/24/2007  Raymond Shen    Added code for Abacus transaction.
//  1/15/2006   Vincent Yi      Added code for DM400C
//              Joe Mozdzer     Initial version
// *************************************************************************/
static void fnsRun1stMailpiece(SYS_STATE *pNextState)
{
    short           sRetVal = SUCCESS;
    unsigned long   pMsgData[2];
    SYS_MSG_TIMEOUT pTimeouts[4];
    UINT8           bPrintMode = fnOITGetPrintMode();
    BOOL            fSetupAbacus = FALSE;

    // Check if we need to set up abacus static mailrun info 
    if(   (bPrintMode == PMODE_AD_ONLY)
       || (bPrintMode == PMODE_TIME_STAMP)
       || (bPrintMode == PMODE_AD_TIME_STAMP) )
    {
        // If in TextAdDateTime mode, check if an ab account is required:
//        fSetupAbacus = fnIsAbActReqForTxtAdDateTime();
    }
    else
    {
        // If not in TextAdDateTime mode, only do it if envelope, on DM300, and 
        //  submode is Indicia (meaning not testprint, report, permit or sealonly.)  
        //  an account is required:
        //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
        if(   (fnGetCurrentMediaType() == MEDIA_MAIL)
           && (fnGetMeterModel() == CSD2)
           && (rCurrentState.wSYSSubMode == INDICIAMODE)  )
        {
            fSetupAbacus = TRUE;
        }
    }

    // If our tests indicate we need to setup AbacausGlobalData for a mailrun...
    if( fSetupAbacus == TRUE ) 
    {
//        sRetVal = fnAGDSetupStaticMailRun();
        if(sRetVal != SUCCESS)
        {   // stop mail run
            pMsgData[0] = (unsigned long) 1;        /* OK */
            pMsgData[1] = (unsigned long) 1;        /* OK */
            OSSendIntertask (CM, SYS, SC_RUN_RSP, LONG_DATA, pMsgData, sizeof(pMsgData));
            if (rCurrentState.wSYSMode == SYS_PRINT_READY)
            {
                fnsAbCancelPrintJob( pNextState ) ;
            }

//            fnSetAGDSetupFlag(FALSE);
            // force OIT to run pre-screen function to catch any disabling conditions
            OSSendIntertask (OIT, SYS, OIT_ABACUS_DATA_SETUP_FAILED, NO_DATA, NULL, 0 );
            return;
        }
        else
        {
//            fnSetAGDSetupFlag(TRUE);
        }
    }
    
    if (rCurrentState.wSYSSubMode != OIT_SEAL_ONLY)
    {
        /* for indicia, permit, test pattern, and report modes, after issuing
            the run command, SYS expects a stage request, a mailrun complete
            (error), or a mode request (error) */
        pTimeouts[0].bTaskID = CM;
        pTimeouts[0].bMessageID = CS_S3_COVERED;
        pTimeouts[1].bTaskID = CM;
        pTimeouts[1].bMessageID = CS_MAILRUN_CMPLT;
        pTimeouts[2].bTaskID = OIT;
        pTimeouts[2].bMessageID = OIT_NEW_MODE_REQ;
        pTimeouts[3].bTaskID = CM;
        pTimeouts[3].bMessageID = CS_JOB_CANCEL;
        fnSetSYSMessageTimeout(pTimeouts, 4, 12000, SYS_TIMEOUT_TYPE_OR);

    }
    else
    {
        /* for seal only mode, we expect a mailrun complete, a mode request
            (error) or another run request (next piece). 20 seconds is the
            deadman here (actual max is determined by CM/PM but this should
            be sufficiently large) */
        pTimeouts[0].bTaskID = CM;
        pTimeouts[0].bMessageID = CS_RUN_REQ;
        pTimeouts[1].bTaskID = CM;
        pTimeouts[1].bMessageID = CS_MAILRUN_CMPLT;
        pTimeouts[2].bTaskID = OIT;
        pTimeouts[2].bMessageID = OIT_NEW_MODE_REQ;
        pTimeouts[3].bTaskID = CM;
        pTimeouts[3].bMessageID = CS_JOB_CANCEL;
        fnSetSYSMessageTimeout(pTimeouts, 4, 20000 + fnCMOSSetupGetCMOSSetupParams()->ulMailInactivityTimeout*1000, SYS_TIMEOUT_TYPE_OR);
    }

    pMsgData[0] = (unsigned long) 0;        /* OK */
    pMsgData[1] = (unsigned long) 0;        /* OK */
    OSSendIntertask (CM, SYS, SC_RUN_RSP, LONG_DATA, pMsgData, sizeof(pMsgData));

    if (fnGetMeterModel() == CSD2 && (rCurrentState.wSYSMode == SYS_PRINT_READY || rCurrentState.wSYSMode == SYS_RUNNING))
    {   // CSD2, normal mail run, need to change to running screen

        /* since the OIT mode is changing for the first piece, this call is needed */
        fnsOITSendRunEvent(pNextState);
    }
}

/* **********************************************************************
// FUNCTION NAME: fnsRunNextMailpiece
// DESCRIPTION: State transition function that handles a transition to
//                  the running state for subsequent pieces in a multi-piece
//                  mailrun (i.e. handles everything but the first piece)
//
// AUTHOR: Joe Mozdzer
//
// MODIFICATION HISTORY:
//  2012.08.14 Liu Jupeng    Merged the fixing fraca 130246 from the branch fphx02.08_c1_secap.
//      -2011.06.22 Steven Peng merge from fphx02.05_c1_cienet for fixing Fraca#130246:
//        - 01/26/2011  Joey Cui    Modified function fnsRun1stMailpiece() & fnsRunNextMailpiece() 
//                                  to fix FRACA GMSE00199820
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsRunNextMailpiece(SYS_STATE *pNextState)
{
    unsigned long   pMsgData[2];
    SYS_MSG_TIMEOUT pTimeouts[4];

    if (rCurrentState.wSYSSubMode != OIT_SEAL_ONLY)
    {
        /* for indicia, test pattern, and report modes, after issuing the run command,
            SYS expects a stage request, a mailrun complete (error), or a mode request (error) */
        pTimeouts[0].bTaskID = CM;
        pTimeouts[0].bMessageID = CS_S3_COVERED;
        pTimeouts[1].bTaskID = CM;
        pTimeouts[1].bMessageID = CS_MAILRUN_CMPLT;
        pTimeouts[2].bTaskID = OIT;
        pTimeouts[2].bMessageID = OIT_NEW_MODE_REQ;
        pTimeouts[3].bTaskID = CM;
        pTimeouts[3].bMessageID = CS_JOB_CANCEL;
        fnSetSYSMessageTimeout(pTimeouts, 4, 12000, SYS_TIMEOUT_TYPE_OR);
    }
    else
    {
        /* for seal only mode, we expect a mailrun complete, a mode request (error) or another run request (next piece) */
        pTimeouts[0].bTaskID = CM;
        pTimeouts[0].bMessageID = CS_RUN_REQ;
        pTimeouts[1].bTaskID = CM;
        pTimeouts[1].bMessageID = CS_MAILRUN_CMPLT;
        pTimeouts[2].bTaskID = OIT;
        pTimeouts[2].bMessageID = OIT_NEW_MODE_REQ;
        pTimeouts[3].bTaskID = CM;
        pTimeouts[3].bMessageID = CS_JOB_CANCEL;
        fnSetSYSMessageTimeout(pTimeouts, 4, 20000 + fnCMOSSetupGetCMOSSetupParams()->ulMailInactivityTimeout*1000, SYS_TIMEOUT_TYPE_OR);
    }

    pMsgData[0] = (unsigned long) 0;        /* OK */
    pMsgData[1] = (unsigned long) 0;        /* OK */
    OSSendIntertask (CM, SYS, SC_RUN_RSP, LONG_DATA, pMsgData, sizeof(pMsgData));
}

/* **********************************************************************
// FUNCTION NAME: fnsRejectRunRequest
// DESCRIPTION: State transition function that rejects a run request.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsRejectRunRequest(SYS_STATE *pNextState)
{
    unsigned long   pMsgData[2];

    pMsgData[0] = (unsigned long) 1;        /* anything nonzero means failure */
    pMsgData[1] = (unsigned long) 1;

    OSSendIntertask (CM, SYS, SC_RUN_RSP, LONG_DATA, pMsgData, sizeof(pMsgData));
}

/* **********************************************************************
// FUNCTION NAME: fnThruputTimerExpire
// DESCRIPTION: Thruput Timer expiration callback function
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
void  fnThruputTimerExpire(unsigned long lwTimerID)
{
#if 0
    fWaitAtStage = FALSE;
    fnDumpStringToSystemLog("Thruput Timer Expire");
    OSStopTimer(usThruputTimerIndex);
#endif
}


/* **********************************************************************
// FUNCTION NAME: fnsCMDisable
// DESCRIPTION: State transition function that puts the control manager
//                  in the disabled mode.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsCMDisable(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[1];

    pMsgData[0] = (unsigned char) CM_DISABLED_MODE;
    /* CM_INTEGRATION */

    OSSendIntertask (CM, SYS, SC_MODE_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));

#ifdef DEBUG_CM
    OSSendIntertask(SYS, CM, CS_MODE_RSP, NO_DATA, NULL, 0);
#endif // #ifdef DEBUG_CM

}

/* **********************************************************************
// FUNCTION NAME: fnsCMSleep
// DESCRIPTION: State transition function that puts the control manager
//                  in sleep mode.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsCMSleep(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[3];

    if (pNextState->wSYSSubMode == OIT_SOFT_POWER_DOWN)
        pMsgData[0] = (unsigned char) CM_SOFT_PWROFF_MODE;
    else
        pMsgData[0] = (unsigned char) CM_SLEEP_MODE;

    OSSendIntertask (CM, SYS, SC_MODE_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));
}

/* **********************************************************************
// FUNCTION NAME: fnsCMDisableLogEnbl
// DESCRIPTION: Does the exact same thing as fnsCMDisable, plus it enables
//              intertask message logging.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  N/A- same behavior as fnsCMDisable
// *************************************************************************/
static void fnsCMDisableLogEnbl(SYS_STATE *pNextState)
{
    fnIntertaskMsgLogEnable();
    fnsCMDisable(pNextState);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnsStartMailRun
//
// DESCRIPTION:
//      State transition function that replies a change mode message to OIT to
//      diplay the printing or running screen.
//
// INPUTS:
//      pNextState  -   pointer to the next state info
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  1/15/2006   Vincent Yi      Initial version
//
// *************************************************************************/
static void fnsStartMailRun(SYS_STATE *pNextState)
{
    SYS_MSG_TIMEOUT pTimeouts[2];

    pTimeouts[0].bTaskID = CM;
    pTimeouts[0].bMessageID = CS_MAILRUN_STARTED;
    pTimeouts[1].bTaskID = CM;
    pTimeouts[1].bMessageID = CS_JOB_CANCEL;
    fnSetSYSMessageTimeout(pTimeouts, 2, 30000, SYS_TIMEOUT_TYPE_OR);

    // Reply OIT to change mode (OIT_RUNNING)
    if (pNextState->wSYSMode == SYS_PRINTING_TAPE)
    {
        fnsOITSendTapeEvent(pNextState);
    }
    else
    {
        fnsOITSendRunEvent(pNextState);
    }
}

/* *************************************************************************
// FUNCTION NAME:
//      fnsOITGoDiagnosticsMode
//
// DESCRIPTION:
//      State transition function to diagnostics mode
//
// INPUTS:
//      pNextState  -   pointer to the next state info
//
// RETURNS:
//      None
//
// NOTES:
//      None
//
// MODIFICATION HISTORY:
//  07/17/2006  Vincent Yi      Initial version
//
// *************************************************************************/
static void fnsOITGoDiagnosticsMode (SYS_STATE *pNextState)
{
    UINT8 bMode = CM_DIAG_MODE;
    OSSendIntertask(CM, SYS, SC_MODE_REQ, BYTE_DATA, &bMode, sizeof(bMode));
}

/* *************************************************************************
// FUNCTION NAME:
//      fnsOITSendDiagnostics
//
// DESCRIPTION:
//      This function sends the Diagnostics event to the OIT.
//
// INPUTS:
//      pNextState      - pointer to next state (not used)
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//  07/17/2006  Vincent Yi      Initial version
//
// *************************************************************************/
static void fnsOITSendDiagnostics(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2] = {OIT_ENTER_DIAG_MODE, 0};

    OSSendIntertask (OIT, SYS, OIT_REC_EVENT, BYTE_DATA, pMsgData, 2);
}


/*------------------------*/
/*   Task entry point &   */
/*   main control loop    */
/*------------------------*/
/* **********************************************************************
// FUNCTION NAME: SystemControlTask
// DESCRIPTION: This is the entry point for the system control task.  This
//              contains the main message processing loop.
//
// AUTHOR: Brian Hannigan/Joe Mozdzer
// MODIFICATION HISTORY:
//  09/23/2014  Renhao       Modified code to fix fraca 227302 & 102652.
//  10/13/2014  Renhao       Modified code for a G922 Auto IP Address issue.
//  07/14/2016  RD			 Set tsts.c to pass mem_pool in as argc param, used by I2C init
// *************************************************************************/
void SystemControlTask (unsigned long argc, void *argv)
{
    INTERTASK  	 	rMsg;           // received intertask message
//    uchar       	*paraP;
//    uchar       	bMode = 0; // POWER_UP- temp
    char        	pLogBuf[80];
    STATUS 			status = NU_SUCCESS;
//    unsigned char 	dnsIp[4];
    NU_MEMORY_POOL 	*pnuMemPool = (NU_MEMORY_POOL *)argc;


    //Start up the Watchdog task as soon as we can.
    OSResumeTask(WDTTASK);

    ///////  Init Hub before trying any comm on USB-C port
    // enable on-board USB hub
    if(configureUSBHub(pnuMemPool) == NU_SUCCESS)
    	dbgTrace(DBG_LVL_INFO, "USB Hub Initialization done\r\n" );
    else
    	dbgTrace(DBG_LVL_INFO, "USB Hub Not Initialized - Note: Hub can only be initialized once per power cycle so this is not an error if base unit has been reset\r\n" );

    //Moved from Application_Initialize
    //TODO - confirm this can be called from SYS task init instead of Application_Initialize

    status = InitFlashFileSystem();
	if(status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Error: File System Not Initialized. Error: %d", status);
	}

    if (fnCheckIfCMOSInitialized() == FALSE)
	{
		InitFRAM();
		fnInitializeCMOS();

		//strcpy(CMOSSetupParams.BPN, "0012345678");
	}

    //TestDCAPXMLFormat();
    fnZmdLookForNewZmd();
    fnGARLookForNewGAR();

    fnUpdateZmd();


//    UpdateEMD();
    fnSysLogTaskStart( "SysTask", SYS );  
    OSStartTimer(SYS_HEARTBEAT_TIMER);
    //OSStartTimer(TRM_UPLOAD_TIMER);
    sprintf( pLogBuf, "SYS pwrup sequence START #0x%04X", (int)CMOSDiagnostics.PowerupCount ); 
    fnDumpStringToSystemLog( pLogBuf );

// Initialize USB Function port serial numbers...
#if 0
	status = NU_USBF_DEVCFG_Set_Serial_String(USBF0_SERIAL, 0) | NU_USBF_DEVCFG_Set_Serial_String (USBF1_SERIAL, 1) ;
	if(status != NU_SUCCESS)
	{
		sprintf( pLogBuf, "SYS pwrup sequence - error %d setting USB serial numbers", status );
		fnDumpStringToSystemLog(pLogBuf);
	}
#endif

//    (void)sprintf(pLogBuf,"Start Time TBD");
//   fnSystemLogHeader(pLogBuf, 1);
//    (void)sprintf(pLogBuf,"Set Time TBD");
//    fnSystemLogHeader(pLogBuf, 2);
//    (void)sprintf(pLogBuf,"Local Midnight TBD");
//    fnSystemLogHeader(pLogBuf, 3);

    memset(&rSYSMsgTimeoutCtrl, 0, sizeof(rSYSMsgTimeoutCtrl));

//??
	// Clear out the LAN proxy structure
	// Also clear out the NOVA structures since we aren't actually supporting NOVA yet
	// no longer needed
//	fnNOVAClearStructures();
	
//  Initialize system status storage areas
    fnInitSystemStatus();

//  Initialize state table pointers
    fnInitializeStateTablePtr();

//  Initialize key information
    fnInitializeSystemInfo();

//  Init Model Number
    fnInitModelNumber();


//  Initialize PSOC Task Data
    fnPreInitializePsocTask();

    if (status == NU_SUCCESS)
    {
        // Set the USB MCP Class Driver "Task ID".  This Task ID is not
        // really the same as the normal Tasks ID's like OIT.  Instead
        // it is a special value placed in the bSource of an Intertask
        // message that lets MCP Tasks know the message is from the MCP
        // Class Driver.
        // !!! This MUST be done before the USB MCP Class Driver is initialized. !!!
//        fnMCPDSetMCPDrvrTaskId(MCP_CLASS_DRVR);

        // Set the base MCP Id supported by the USB MCP Class Driver.
        // !!! This MUST be done before the USB MCP Class Driver is initialized. !!!
//        fnMCPDSetMCPBaseID(FUTURE_PHOENIX_BASE_MCP_ID);

        // Set the device supported by the USB MCP Class Driver.
        // !!! This MUST be done before the USB MCP Class Driver is initialized. !!!
//        fnMCPDSetSupportedDeviceInfo(USB_PB_VENDOR_ID, USB_FP_MCP_PRODUCT_ID);
//        fnMCPDSetMCP2DevTypeMap(MCP2DevTypeMap);

    }

    fnInitEmd();

	//Initialize Transaction Record Manager
    status = InitTrm();
	
    // Initialize the acceptable usb printer/lan adaptor device list
    // For debugging:
    sprintf( pLogBuf, "fnInitEmd done" );
    fnSystemLogEntry( SYSLOG_TEXT, pLogBuf, strlen(pLogBuf) );

    // For debugging:
    sprintf( pLogBuf, "Start fnpwSystemInitialize" );
    fnSystemLogEntry( SYSLOG_TEXT, pLogBuf, strlen(pLogBuf) );
	
    fnpwSystemInitialize();

    InitMcpData();


    // initialize the PDM over current pin functionality, this will eventually be used to issue an interrupt on GPIO3:15
    initializeGPIO3_15PDMOverCurrentPin();
    loadPDMAllVersionsCache(pnuMemPool);

    // end of on board hub initialization
    /////////////
    HALSetVM27VPowerState(1) ;
    HALSetS5VSPowerState(1);
    HALSetS5VPowerState(1);

    // RD DEBUG - Need these up for testing so bringing them up here. At some point
    // they need to be controlled by the sleep system.
    // Bring up the power domains

	//////
	// check PDM firmware
	checkFirmwareAndUpdate(pnuMemPool, FALSE);

//   fnUpdateZmd();

    fnSysLogTaskLoop( "SysTask", SYS );  
    while (1)
    {
        if (fPowerupSeqComplete != TRUE)
        {
            unsigned long   lwCurrentEvents = 0;
            char            chEventStatus;
            BOOL            fEventError = FALSE;

            chEventStatus = OSReceiveEvents(SYS_PWRUP_EVENT_GROUP, lwPowerupSeqEventsAwaited,
                            lwPowerupSeqWaitTimeout ? lwPowerupSeqWaitTimeout : OS_SUSPEND,
                            OS_AND, &lwCurrentEvents);

            if (chEventStatus == (char) SUCCESSFUL)
            {
                sprintf(pLogBuf,"Pwrup events received:%08lX", lwCurrentEvents);
                fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
            }
            else if (chEventStatus == (char) OS_TIMEOUT)
            {
                fEventError = TRUE;
                /* retrieve current state of the event group for logging */
                OSReceiveEvents(SYS_PWRUP_EVENT_GROUP, 0xFFFFFFFF, OS_NO_SUSPEND, OS_OR, &lwCurrentEvents);
                sprintf(pLogBuf, "Pwrup event timeout:%08lX %08lX", lwPowerupSeqEventsAwaited, lwCurrentEvents);
                fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
            }
            else
            {
                sprintf(pLogBuf,"Pwrup events received error : %d", chEventStatus);
                fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
            }

            /* process all readily available messages off the SYS queue */
            while ( OSReceiveIntertask (SYS, &rMsg, OS_NO_SUSPEND) == SUCCESSFUL )
            {
                switch (rMsg.bSource)
                {
                    case CM:
                        fnProcessMessage((TASK_MSG_TABLE_ENTRY *)StCMMsgTable, &rMsg);
                        break;

                    case OIT:
                        fnProcessMessage((TASK_MSG_TABLE_ENTRY *)StOITMsgTable, &rMsg);
                        break;

                    case SCM:
                        fnProcessMessage((TASK_MSG_TABLE_ENTRY *)StBOBMsgTable, &rMsg);
                        break;
                    default:
                        // Systask, powerup: record source of ignored message. 
                        putString("Systsk PwrUp: IgnoreUnknownMsgSource ", rMsg.bSource);
                        break;
                }
            }

//          if ((fEventError == TRUE) && (rCurrentState.wSYSMode != OIT_METER_ERROR))
            if ((fEventError == TRUE) && (rCurrentState.wSYSMode != SYS_METER_ERROR))
            {
                unsigned long   lwEventsMissing;
                unsigned char   bErrorCode = ECSYS_PWRUP_TIMEOUT_UNKNOWN;   /* default that should get overridden */

                /* retrieve current state of the event group to see what didn't happen */
                OSReceiveEvents(SYS_PWRUP_EVENT_GROUP, 0xFFFFFFFF, OS_NO_SUSPEND, OS_OR, &lwCurrentEvents);
                lwEventsMissing = (lwPowerupSeqEventsAwaited | lwCurrentEvents) ^ lwCurrentEvents;

                if (lwEventsMissing & EV_OIT_INIT_COMPLETE)
                    bErrorCode = ECSYS_PWRUP_TIMEOUT_OIT;
                if (lwEventsMissing & EV_SCM_INIT_COMPLETE)
                    bErrorCode = ECSYS_PWRUP_TIMEOUT_SCM;
                if (lwEventsMissing & EV_CM_INIT_COMPLETE)
                    bErrorCode = ECSYS_PWRUP_TIMEOUT_CM;
                if (lwEventsMissing & EV_PSOC_INIT_COMPLETE)
                    bErrorCode = ECSYS_PWRUP_TIMEOUT_PSOC;
                fnReportMeterError(ERROR_CLASS_SYS, bErrorCode);
                fnSetPowerupSeqComplete();
            }

            /* now call powerup sequence resume function */
            if (pPowerupSeqResumeFn != NULL)
                pPowerupSeqResumeFn();
        }
        else
        {

            // Wait for a message on the SYS Queue
            if( OSReceiveIntertask (SYS, &rMsg, OS_SUSPEND) == SUCCESSFUL )
            {
                fnCheckSYSMessageTimeout(&rMsg);
                switch (rMsg.bSource)
                {
                    case SYS:
                        fnProcessMessage((TASK_MSG_TABLE_ENTRY *)StSYSMsgTable, &rMsg);
                        break;

                    case CM:
                        fnProcessMessage((TASK_MSG_TABLE_ENTRY *)StCMMsgTable, &rMsg);
                        break;

                    case OIT:
                        fnProcessMessage((TASK_MSG_TABLE_ENTRY *)StOITMsgTable, &rMsg);
                        break;

                    case SCM:
                        fnProcessMessage((TASK_MSG_TABLE_ENTRY *)StBOBMsgTable, &rMsg);
                        break;
                    default:
                        // Systask loop: record source of ignored message. 
                        putString("SYS: IgnoreUnknownMsgSource ", rMsg.bSource);
                        break;
                } 
            } // waiting for messages
        }
    }

} // SystemControlTask


/*----------------------------------------*/
/*   Power up sequence functions (fnpw*)  */
/*----------------------------------------*/

/* **********************************************************************
// DESCRIPTION: Initialize SSL Library
// *************************************************************************/
static void fnInitSSLLibrary( void )
{
    int wolfStatus;
    char * cipherbuf;

    // SSL library init, log ciphers
    wolfStatus = SSL_library_init();
    if (wolfStatus != SSL_SUCCESS)
    {//
        fnReportMeterError(ERROR_CLASS_SYS, ECSYS_SSL_NOT_OK);
    }
    else
    {
        if (OSGetMemory((void **) &cipherbuf, CIPHER_LIST_SIZE) == SUCCESSFUL)
        {
			// Log ciphers supported by wolfSSL configuration
			wolfStatus = wolfSSL_get_ciphers(cipherbuf, CIPHER_LIST_SIZE);
			if (wolfStatus == SSL_SUCCESS)
			{
				fnDumpStringToSystemLog("Available SSL Cipher Suites: ");
				fnDumpStringToSystemLog( cipherbuf );
			}
			else
			{
				fnDumpStringToSystemLog("SSL Cipher List too big to log");
			}
			(void) OSReleaseMemory ((void *) cipherbuf);
        }
    }

#ifdef SSL_DEBUG_LOGGING
	(void) wolfSSL_Debugging_ON();
#endif
}

/* **********************************************************************
// FUNCTION NAME: fnInitEmd
// DESCRIPTION: Powerup sequence function that start LCD and related tasks,
//              and update EMD
// AUTHOR: Joey Cui
// Notes:  Move this part of code from fnpwSystemInitialize()
//         because they should be called ahead
// *************************************************************************/
static void fnInitEmd( void )
{
    char          sbuf[50];
    int               ec;
 //   unsigned char   chEventStatus;
    unsigned long   lwCurrentEvents;
    UINT8 majorHwVer, minorHwVer;

    fnGetHardwareVersion(&majorHwVer, &minorHwVer);
    
    /* activate the tasks that support the display only */
    OSResumeTask(LCDTASK);
    
    /* allow the tasks we have started to initialize and get settled since we
     need them to do some direct writes to the screen. */
    //Let the LCD have time to initialize the display
    //A Better way would be to wait for a message from the LCD Task
    //saying it was initialized
//    chEventStatus =
    OSReceiveEvents(SYS_PWRUP_EVENT_GROUP, EV_LCD_INIT_COMPLETE,
          OS_SUSPEND, OS_AND, &lwCurrentEvents);
    OSResumeTask(CLCD);
    OSResumeTask(KEYTASK);
    
    /* Initialize the display and LED */
    fnLEDSetColor(LED_GREEN);

    sprintf(sbuf,"Initializing EMD");
    fnSystemLogEntry(SYSLOG_TEXT, sbuf, strlen(sbuf));

	// The unzip of the EMD takes long enough that the display has plenty of time to update
	// so no need for a delay.

    // clear the RAM xref before trying to load anything
    (void)memset((unsigned char *)&sRamItemsXref, 0, sizeof(sRamItemsXref));

    // Call the power fail recovery functions for the EMD and Graphics

//    fnEMDPowerFailCheck(  ); //TODO Ram commented not to use Powerfail at this time until file system is fixed
    fnGFPowerUpCheck();

    // Delete files that will be replaced when the archive files are extracted.
    //  TRUE instructs to display errors directly before burning up.
//    fnDL_PurgeFFSFilesListedInArchive( TRUE );

    // Check if the EMD needs to be updated
//    EmdEc = fnEMDCheckUpdate();  //TODO Ram commented not to check EMD to update at this time

    // Check if we have an EMD file. If so start the system as usual
    ec = fnEmdFlashInit();
    if(ec == 0)
      {
        // We have a valid EMD set some state saying so.
        NoEMDPresent = FALSE;
      }

    // Look for an archive file.  If found, extract new files.
    //  TRUE instructs to display errors directly before burning up.
//    fnDL_ExtractFilesFromArchive( TRUE );
}

/* **********************************************************************
// FUNCTION NAME: fnpwSystemInitialize
// DESCRIPTION: Powerup sequence function that starts the initialization
//              process.
// AUTHOR: Brian Hannigan/Joe Mozdzer
// MODS:
//  2009.07.20 Clarisa Bellamy - Modified fnOITValidateScreenVersions so that 
//                      the screen layout is controlled here.  Screen versions
//                      are now 6 digits instead of 4.  Display more info when
//                      the screen version validation returns failure. 
// *************************************************************************/
static void fnpwSystemInitialize( void )
{
  char          sbuf[100];
//  DATETIME      rTime;
//  uchar         bMode = 0,bHwType; // POWER_UP;
  T_FUNCPTR     pResumeFn;
//  void (* pResumeFn) ()   = fnpwSystemInitialize2;
//  BOOL          fScreenVersionsOK;    // Made into a global.
  BOOL          fTreeMismatch = FALSE;
  BOOL          fLangMismatch = FALSE;
  int               ec;
  char          sEMDTree[7], sEMDLang[7];   // Version strings, without decimals, with null-terminator. 
  char          sUICTree[7], sUICLang[7];   // Version strings, without decimals, with null-terminator.
  char          sMeterModel[7]; // i.e "DM300c" - 6 character plus NULL terminator
  UINT8         majorHwVer, minorHwVer;
  emMeterModel  model=fnGetMeterModel();

    pResumeFn = fnpwSystemInitialize2;
    ec = fnGFInitResidentGraphics(0);
    if(ec == NU_SUCCESS){
    	// We have a valid GAR file.
    	NoGARFilePresent = FALSE;
    }

    // For debugging:
    sprintf( sbuf, "fnGFInitResidentGraphics err=%d", ec );
    fnSystemLogEntry( SYSLOG_TEXT, sbuf, strlen(sbuf) );
    
    /* ARC file unzip could have put stuff on the display,
        so have to make sure to clear out what might be there */
    clearDisplay();
    
	// RD - Clear proxy structure
	memset(&globalProxyConfig, 0, sizeof(globalProxyConfig));

	// RD initialize the HB store if it is not already
	if(strncmp(globalHBLogStructure.magicInit, HB_MAGICINIT, 8) != 0)
	{
		memset(&globalHBLogStructure, 0, sizeof(HBLogStructure));
		strncpy(globalHBLogStructure.magicInit, HB_MAGICINIT, 8);
	}

    /* put up the first status message */
    fnLCDPutStringDirect((unsigned char *)"Initializing              ", 0, 0, LCD_CHARACTER_NORMAL);

    /* EMD init is complete at this point.  Log the result */
    if (NoEMDPresent == TRUE){
		sprintf(sbuf, "EMD Not Loaded");
    }
    else
		sprintf(sbuf, "EMD Init Done");
    fnSystemLogEntry( SYSLOG_TEXT, sbuf, strlen(sbuf) );
    
    // Determine if the Second Stage CMOS needs to continue to completed
    // This is also called after a new EMD is installed. The reason it is called
    // from here is that we could lose power after installing the EMD but before
    // the SecondStage Init completed
    if(NoEMDPresent == FALSE )
    {
      fnCMOSSecondStageInit();
	  
        // For debugging:
        fnDumpStringToSystemLog("fnCMOSSecondStageInit done");
	  
    }

	// Fix the graphic flags, as necessary, for the graphics
	// Don't need to do this if there's no EMD because you can't use the graphics if there's no EMD.
	if(NoEMDPresent == FALSE )
	{
		// RD - the structure needs to be initialized to nulls at some point, try it here.
		cleanGFGrfxFileInfo();
		// the function will record any error, so don't care about the returned error value.
		(void)fnGFGrfxCleanup();

        // For debugging:
        fnDumpStringToSystemLog( "fnGFGrfxCleanup done");

    }

    if(ValidateCCDFile())
    	dbgTrace(DBG_LVL_INFO, "CCD File validated");
    else
    	dbgTrace(DBG_LVL_ERROR, "ERROR Validating CCD File");

   // For debugging: RD- Already reported in the normal syslog TASK:... format
//   sprintf( sbuf, "OSResumeTask EDM and CEDM" );
//   fnSystemLogEntry( SYSLOG_TEXT, sbuf, strlen(sbuf) );

    /* increment the diagnostic power up counter.  this is done after all CMOS init
       stuff is taken care of.  */
    CMOSDiagnostics.PowerupCount++;

    // Setup filters for intertask msg log so that it doesn't fill up with useless stuff
    pIntertaskLogMask[CPLAT] = SRC_MASK;            // Do not log messages from CPLAT
    pIntertaskLogMask[WSRXSRVTASK] = SRC_MASK | DEST_MASK;   // Do not log messages to or Websocket RX task

    /* validate screen file version compatibility with this version of source code */
    if (NoEMDPresent)
      fScreenVersionsOK = FALSE;
    else
      fScreenVersionsOK = fnOITValidateScreenVersions( &fTreeMismatch, &fLangMismatch, 
                                                        sEMDTree, sUICTree, 
                                                        sEMDLang, sUICLang );

   // For debugging:
   sprintf( sbuf, "fnOITValidateScreenVersions OK=%d", (int)fScreenVersionsOK );
   fnSystemLogEntry( SYSLOG_TEXT, sbuf, strlen(sbuf) );
   
   // For debugging:
//   sprintf( sbuf, "Test syslogTime" );
//   fnSystemLogEntry( SYSLOG_TEXT, sbuf, strlen(sbuf) );

    /* if we don't have an EMD, do not start the OIT or plaform tasks since they are
       not necessary to fix that problem. */
   if (!NoEMDPresent && fScreenVersionsOK)
    {
      /* as long as we have an EMD we are attempting a normal powerup, so start
         the OIT and CPLAT */
      OSResumeTask(OIT);
      OSResumeTask(CPLAT);
    }

    fnInitEventTimer();  /* init the alarm clock queue and interrupt vectors */
	
   // For debugging:
   sprintf( sbuf, "fnInitEventTimer" );
   fnSystemLogEntry( SYSLOG_TEXT, sbuf, strlen(sbuf) );

// (ML) 10/30/09 - Why not start Plattask as early as possible??      
//  Move from initial step - fnpwsystemInitialize4() then fnpwFinishPowerup() to fix GMSE163991. Now here?
    OSResumeTask(PLAT);
///

   sprintf( sbuf, "OSResumeTask PLAT"  );
   fnSystemLogEntry( SYSLOG_TEXT, sbuf, strlen(sbuf) );

   OSResumeTask(PWRDOWNTASK);
   sprintf( sbuf, "OSResumeTask PWRDOWNTASK"  );
   fnSystemLogEntry( SYSLOG_TEXT, sbuf, strlen(sbuf) );

   /* drop into this permanent black hole if we don't have an EMD */
    if (NoEMDPresent)
    {
    	// Still need to communicate so init networking here
        enableDelayedNetworkComponents();

		/* allow the tasks we have started to initialize and get settled */
		OSWakeAfter(1000);

		if (EmdEc)
		{
			// There was a problem loading the EMD
			/* post an error message and code to the screen */
			fnLCDPutStringDirect((unsigned char *)" EMD not expandable ", 0, 0, LCD_CHARACTER_NORMAL);

			switch(EmdEc)
			{
				case EMDINVALIDVERSION:
					(void)sprintf(sbuf, " Wrong EMD Version");
					break;

				case EMDALLOCERROR:
					(void)sprintf(sbuf, " Not enough memory");
					break;

				default:
					// the error codes are negative value, so have to use %d instead of %u
					(void)sprintf(sbuf, "    Error = %d", EmdEc);
					break;
			}

			fnLCDPutStringDirect((unsigned char *)sbuf, 0, 2, LCD_CHARACTER_NORMAL);
		}
		else
		{
			// Something else went wrong, so we never got to loading the EMD
	        /* post an error message and code to the screen */
	        fnLCDPutStringDirect((unsigned char *)"   EMD not loaded         ", 0, 0, LCD_CHARACTER_NORMAL);
	    	SendBaseEventToTablet(BASE_EVENT_EMD_NOT_LOADED, NULL);

	        sprintf(sbuf, "        %02X%02X", ERROR_CLASS_SYS, ECSYS_NO_EMD_PRESENT);
	        fnLCDPutStringDirect((unsigned char *)sbuf, 0, 1, LCD_CHARACTER_NORMAL);

        }

    	// Set this event so that others know we are stuck
        OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_SYS_INIT_HUNG, OS_OR);

        /* suspend forever */
        while (1)
        {
            OSWakeAfter(10000);
        }
    }

    if (!fScreenVersionsOK)
    {
        unsigned char   bErrorCode;

        /* allow the tasks we have started to initialize and get settled */
        OSWakeAfter(500);

        // Get the right error code:
        if( fTreeMismatch && fLangMismatch )
            bErrorCode = ECSYS_TREE_LANG_VER_MISMATCH;
        else
            if( fTreeMismatch )
                bErrorCode = ECSYS_TREE_VER_MISMATCH;
            else
                bErrorCode = ECSYS_LANG_VER_MISMATCH;

        //Ram added to report error as 0810 when GetBaseStatusReq if Screen/language version mismatch
        fnReportMeterError(ERROR_CLASS_SYS, bErrorCode);
        sprintf( sbuf,       "Screen file mismatch  %02X%02X", ERROR_CLASS_SYS, bErrorCode );
        fnLCDPutStringDirect((unsigned char *)sbuf,       0, 0, LCD_CHARACTER_NORMAL);

        fnLCDPutStringDirect((unsigned char *)"VERSIONS:  TREE   LANGUAGE", 0, 2, LCD_CHARACTER_NORMAL);
        
        sprintf( sbuf,        "From EMD: %s   %s", sEMDTree, sEMDLang ); 
        fnLCDPutStringDirect((unsigned char *)sbuf,       0, 3, LCD_CHARACTER_NORMAL );
        
        sprintf( sbuf, "UIC MIN : %06X   %06X", UIC_TREE_VERSION_A, UIC_LANGUAGE_VERSION_A );
        fnLCDPutStringDirect((unsigned char *)sbuf,       0, 4, LCD_CHARACTER_NORMAL );
        
    	// Set this event so that others know we are stuck
        OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_SYS_INIT_HUNG, OS_OR);

        /* suspend forever */
        while (1)
        {
            OSWakeAfter(10000);
        }
    }

	if (model == CSD2)
    {
        sprintf(sMeterModel, "CSD2");
    }
    else if (model == CSD3)
    {
        sprintf(sMeterModel, "CSD3");
    }
    else
    {
        sprintf(sMeterModel, "Unknown");
    }

    sprintf( sbuf,"Version %s (%s)", UicVersionString, BUILD_MACHINE );
    fnSystemLogHeader(sbuf, 0);

    fnGetHardwareVersion(&majorHwVer, &minorHwVer);
    sprintf(sbuf,"%s Hardware Version = 0x%02X", sMeterModel, majorHwVer);
    fnSystemLogHeader(sbuf, 0);
	
    sprintf(sbuf,"Dip Switch = 0x%02X", minorHwVer);
    fnSystemLogHeader(sbuf, 0);

    // I2C initialization - setup USB Hub operation.

    // send a message to OIT, the power on screen will be loaded once OIT got this message.
    OSSendIntertask( OIT, SYS, OIT_INITIALIZE_REQUEST, NO_DATA, NULL, 0 );

    pResumeFn = fnpwSystemInitialize2;
    // Wait for OIT task to set event flag then go the next function...
    fnSetWaitCondition( EV_OIT_INIT_COMPLETE, 1000, pResumeFn );
}


/* **********************************************************************
// FUNCTION NAME: fnpwSystemInitialize2
// DESCRIPTION: Powerup sequence function that continues the initialization
//              process.
//
//              This one starts the bob task and the I-button driver.
//
// AUTHOR: Brian Hannigan/Joe Mozdzer/Clarisa Bellamy
//
//
// *************************************************************************/
static void fnpwSystemInitialize2( void )
{
  T_FUNCPTR pResumeFn;


    // Setup next function...
//    void (* pResumeFn) () = fnpwSystemInitialize3;
    pResumeFn = fnpwSystemInitialize3;

    // Next, start up the bob task and the ibutton driver task.
	OSResumeTask(CIBUTTON);
    OSResumeTask(SCM);

    // Send the init message to the Ibutton driver to get the serial number.
    OSSendIntertask( SCM, SYS, SCRIPT_INIT_SCM, NO_DATA, NULL, 0 );

    // Wait for bob task to set an event, or for a timeout.
    fnSetWaitCondition( EV_SCM_INIT_COMPLETE, 2000, pResumeFn );
}


/* **********************************************************************
// FUNCTION NAME: fnpwSystemInitialize3
// DESCRIPTION: Powerup sequence function that continues the initialization
//              process.
// AUTHOR: Brian Hannigan/Joe Mozdzer
//
// *************************************************************************/
static void fnpwSystemInitialize3( void )
{
	T_FUNCPTR pResumeFn;
    unsigned long   lwPsdState;
    char            sbuf[50];
	char           sPSDVersion[IPSD_MAXSZ_FWVER_SUB_LEN + 1];

    /* process the manufacturing mode event if the psd is in an applicable state, otherwise
        process the normal event that completes the power up sequence */
    if (fnCheckVaultInMfgState(&lwPsdState))
    {
        fnSetPowerupSeqComplete();
        fnProcessSTEvent(SEV_ENTERMFG);

        //Since this task is now stuck in this mode, enable networking to enable PCT communication
        enableDelayedNetworkComponents();
    }
    else
    {
	    if(fnGetPSDVersion(sPSDVersion) == TRUE)
	    {
	        sprintf(sbuf,"PSD Version: %s", sPSDVersion);
	        fnSystemLogHeader(sbuf, 0);
	    }
    }

    fnInitPSDNumDecimal();

    // enable the PDM overcurrent interrupt detection code
    prepPDMISRHandling() ;

    pResumeFn = fnpwSystemInitialize4;
    // If NVRAM setting of ink install is set then set event flag to go the next function
    // Otherwise, wait for tablet to indicate ink is installed to set event flag then go the next function...
//TODO - uncomment the line below when tablet OOB app is available and ready to control OOB flow via OOB APIs.  Until then, base will always assume OOB is done, so system init can continue
//    if (fnGetNVRAMOOBInkInstalled())
    {
        OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_OOB_INK_COMPLETE, OS_OR);
    }
    // wait indefinitely for tablet
    fnSetWaitCondition( EV_OOB_INK_COMPLETE, 0, pResumeFn );

}

extern tCmStatus               	cmStatus;               // Current status of CM/PM
DATETIME    	    			globalNetlogStartTime;

/* **********************************************************************
// FUNCTION NAME: fnpwSystemInitialize4
// DESCRIPTION: Powerup sequence function that continues the initialization
//              process.
// AUTHOR: Brian Hannigan/Joe Mozdzer
//
// *************************************************************************/
static void fnpwSystemInitialize4( void )
{
    char            sbuf[50];
    DATETIME        rTime;
    unsigned char   pMsgData[2];
    UINT8       bClass;
    UINT8       bCode;
    int nbOfInstalledGraphics = 0;


    OSResumeTask(CM);
    OSResumeTask(BJCTRL);
    OSResumeTask(BJSTATUS);
    OSResumeTask(FDRMCPTASK);

    //Resume PLAT task later in fnpwFinishPowerup() to avoid conflicting with FDRMCPTASK task,
    //thus fix GMSE00163991 (G900/DM475 - Meter prompts 010E & 0802 error when power on meter
    //with connected external platform.)
    //OSResumeTask(PLAT);

    OSResumeTask(DISTTASK);
    OSResumeTask(PBPTASK);

//    OSResumeTask(DLS_TASK);
    OSResumeTask(BKGRND);

// RT clock must be initialized
    (void)GetSysDateTime(&rTime, ADDOFFSETS, GREGORIAN);
    globalNetlogStartTime = rTime;
    (void)sprintf(sbuf,"Start Time  %02d/%02d/%02d  %02d:%02d:%02d",
					rTime.bMonth, rTime.bDay, rTime.bYear, rTime.bHour, rTime.bMinutes, rTime.bSeconds);
    fnSystemLogHeader(sbuf, 1);

  Framlog_add_string_format("======[%s]=======", sbuf);

    (void)memcpy(&CMOSLastLocalMidnightDT, &rTime, sizeof(DATETIME));

    /* Send initialize commmand to Control Manager */
    pMsgData[0] = 0;    /* 0 = power up init... there is no constant defined for this yet */
    pMsgData[1] = CM_DISABLED_MODE;

#ifndef DEBUG_CM
    OSSendIntertask(CM, SYS, SC_INIT_REQ, BYTE_DATA, pMsgData, sizeof(pMsgData));
#endif

    // Initialize the USB PowerDomain controller, note that this  will probably need modification to check current version
    // prior to loading latest version.
//    initializePDController();

    fnSetWaitCondition(EV_CM_INIT_COMPLETE, 20000, fnpwSystemInitialize5);

#ifdef DEBUG_CM
    OSSetEvents(SYS_PWRUP_EVENT_GROUP, EV_CM_INIT_COMPLETE, OS_OR);
    OSSendIntertask(SYS, CM, CS_INIT_RSP, NO_DATA, NULL, 0);
#endif // #ifdef DEBUG_CM

    if(NoGARFilePresent == FALSE)
    {
        nbOfInstalledGraphics = fnInstallGraphics();

        if ((fnBobErrorInfo(&bClass, &bCode) != SUCCESSFUL) && nbOfInstalledGraphics > 0)
        {
            if ((bClass == ERROR_CLASS_JBOB_TASK) && (bCode == JSCM_STATIC_IG_BLOB_ERROR)||
           		(((bClass * 0x100) + bCode) == IG_MISSING_COMPONENT))

                // we are probably instally a missing graphic, reboot so image generator 
                // can be initialized properly
                RebootSystem(); 
        }
    }

}

/* **********************************************************************
// FUNCTION NAME: fnpwSystemInitialize5
// DESCRIPTION: Powerup sequence function that continues the initialization
//              process.
//
// *************************************************************************/
static void fnpwSystemInitialize5( void )
{

    //Start up the PSOC after PM Init.
	OSResumeTask(PSOC);

    fnSetWaitCondition(EV_PSOC_INIT_COMPLETE, 20000, fnpwFinishPowerup);

}

/* **********************************************************************
// FUNCTION NAME: fnpwFinishPowerup
// DESCRIPTION: This is the final powerup sequence function that terminates
//              the powerup sequence.
//
// AUTHOR: Joe Mozdzer
//
// *************************************************************************/
static void fnpwFinishPowerup( void )
{

    enableDelayedNetworkComponents();

    fnInitializeNetworkDataFromFlash();

    /* enable Normal Preset timeout if there is a non-zero number in the normal preset timer.  the only way there would not
        be at this point is if the flash factory value is zero.  note that we must be attached
        to the base to activate normal preset mode.  */
    if (CMOSSetupParams.wpUserSetPresetClearTimeout)
    {
        fnSetNormalPresetTimeout(CMOSSetupParams.wpUserSetPresetClearTimeout * 60 * 1000);  /* adjust minutes to milliseconds */
        fnEnableNormalPreset();
    }

    // Make sure that the lock code is enabled if it needs to be, and that
    //  the lock code is valid, if it needs to be.  Then set MeterLocked if
    //  if Meter lock is enabled.
//    if (fnCheckLockCodeEnabled())
//        fMeterLocked = TRUE;

//   fnInitDeptAccountAndSupervisorPointers();   // Init accounting pointers in case they're

                                                // needed during the CCD stuff
//RAM added
//    sprintf( pLogBuf, "RAM Start after ValidateCCDFile: fnpwFinishPowerup" );
//    fnSystemLogEntry( SYSLOG_TEXT, pLogBuf, strlen(pLogBuf) );
//End
    //fnRMAIInitRate(NULL);
//    (void)fnRateInitRates();

    fnSetPowerupSeqComplete();

    // Moved Checksum checks to oipwrup.c - If metererror prevented powerupcomplete!

//    fnLoadNativeWords();

    /* check for manufacturing mode now happens earlier in the sequence (before we start the CM) */
    fnProcessSTEvent(SEV_ALLRSPND);

    SetClockMidnight(); /* create a midnight event */

    //Inform DLS Task that the power-up sequence has completed
    //dbgTrace(DBG_LVL_ERROR, "DLS_TASK Removed message from %s  line %d", __FILE__, __LINE__);
    //if(!OSSendIntertask (DLS_TASK, SYS, (unsigned char)0, NO_DATA, 0, 0))
    //    taskException("OSSendIntertask() fail", THISFILE, __LINE__);

    {
    	unsigned char   pMsgData[2];
		pMsgData[0] = OIT_PRINT_NOT_READY; //OIT_LEFT_PRINT_RDY;
		pMsgData[1] = 0;
		(void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);
    }

    // RD - install custom error handler in all tasks
    installStandardErrorHandlerInAllTasks();

//xit:
//    return;
}


/*---------------------------------------*/
/*   Intertask message handlers (fnm*)   */
/*---------------------------------------*/

/* **********************************************************************
// FUNCTION NAME: fnmCMMailJobComplete
// DESCRIPTION: This function handled reception of the mail job complete
//              message from the control manager.
//
// AUTHOR: Joe Mozdzer
//
// *************************************************************************/

static BOOL fnmCMMailJobComplete(INTERTASK *MsgPtr)
{
    /* if the CM state is currently running, staging, or printing, this message
        is forwarded to the OIT, which will use it to transition to a different screen (which
        should subsequently lead to a mode change request).
       if the CM state is not one of these, the message is disregarded.  CM does
        know if the rest of the system is in the ready or a running state, therefore
        this check must be done here. */
    if (rCurrentState.bPMCIState == CM_RUNNING      ||
        rCurrentState.bPMCIState == CM_PRINTING     ||
        rCurrentState.bPMCIState == CM_PRERUNNING)
    {
        OSSendIntertask (OIT, SYS, OIT_MAIL_JOB_COMPLETE, NO_DATA, NULL, 0);
        fnProcessSTEvent(SEV_CMMJCOMPLETE);
    }

    // We may have aborted out of the mail run, so we may not be in one of the above states,
    // but we still want to close out the transaction, if necessary.
    return(SUCCESSFUL);
}


static BOOL fnmCMInitializeResponse(INTERTASK *MsgPtr)
{
//    unsigned long eventPtr;     // received events holder

//  fCMInitReceived = TRUE;

//  fnpwFinishPowerup();

    return(SUCCESSFUL);
}

static BOOL fnmOITInitializeResponse(INTERTASK *MsgPtr)
{
//    unsigned long eventPtr;     // received events holder

//  fOITInitReceived = TRUE;

//  fnpwFinishPowerup();

    return(SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnmStartMailRequest
//
// DESCRIPTION:
//      Intertask message handling function for OIT_START_MAIL_REQ that
//      forwards the Start/Tape key pressing message to CM, and creat a event
//      SEV_OISTARTMAIL / SEV_OISTARTTAPE to change mode.
//      The Start Mail message is only handled for DM400c
//      The Start Tape message is not handled in Seal-Only mode.
//
// INPUTS:
//      MsgPtr  -   pointer to the message
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  1/15/2006   Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnmStartMailRequest(INTERTASK *MsgPtr)
{
    UINT8       pMsgData[2];

    pMsgData[0] = MsgPtr->IntertaskUnion.bByteData[0];  // Media type
    pMsgData[1] = MsgPtr->IntertaskUnion.bByteData[1];  // Tape count

    if (pMsgData[0] == MEDIA_TAPE)
    {   // Tape

        // Recheck not in seal-only mode
        if (rCurrentState.wSYSSubMode != OIT_SEAL_ONLY)
        {
            // Forward the message to CM
            OSSendIntertask (CM, SYS, SC_START_MAILRUN, BYTE_DATA, pMsgData,
                                sizeof(pMsgData));
            // Create event to change mode
            fnProcessSTEvent (SEV_OISTARTTAPE);
        }
    }
    else
    {   // Mail

        // Recheck the base type
        //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
        if (fnGetMeterModel() >= CSD3)
        {
            // Forward the message to CM
            OSSendIntertask (CM, SYS, SC_START_MAILRUN, BYTE_DATA, pMsgData,
                                sizeof(pMsgData));
            // Create event to change mode
            fnProcessSTEvent (SEV_OISTARTMAIL);
        }
    }

    return(SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnmStopMailRequest
//
// DESCRIPTION:
//      Intertask message handling function for OIT_STOP_MAIL_REQ that forwards
//      the Stop key pressing message to CM, and reply OIT OIT_STOP_MAIL_RSP
//
// INPUTS:
//      MsgPtr  -   pointer to the message
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  1/15/2006   Vincent Yi      Initial version
//
// *************************************************************************/
static BOOL fnmStopMailRequest(INTERTASK *MsgPtr)
{
    UINT8   ucMsgData = MsgPtr->IntertaskUnion.bByteData[0]; // Media type

    // Forward the message to CM
    OSSendIntertask (CM, SYS, SC_STOP_MAILRUN, BYTE_DATA, &ucMsgData, 1);
    // Reply OIT
    OSSendIntertask (OIT, SYS, OIT_STOP_MAIL_RSP, NO_DATA, NULL, 0);

    return(SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnmCMS3Covered
//
// DESCRIPTION:
//      Intertask message handling function for CS_S3_COVERED
//
// INPUTS:
//      MsgPtr  -   pointer to the message
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  1/15/2006   Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnmCMS3Covered(INTERTASK *MsgPtr)
{
    return(SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnmCMMailRunStarted
//
// DESCRIPTION:
//      Intertask message handling function for CS_MAILRUN_STARTED
//
// INPUTS:
//      MsgPtr  -   pointer to the message
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  1/15/2006   Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnmCMMailRunStarted(INTERTASK *MsgPtr)
{
    // Forward message to OIT
    OSSendIntertask (OIT, SYS, OIT_START_MAIL_RSP, NO_DATA, NULL, 0);

    return(SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnmCMRunRequest
//
// DESCRIPTION:
//      Intertask message handling function for CS_RUN_REQ
//
// INPUTS:
//      MsgPtr  -   pointer to the message
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  1/15/2006   Vincent Yi      Initial version
//
// *************************************************************************/
static BOOL fnmCMRunRequest(INTERTASK *MsgPtr)
{
    BOOL fHandleMsg = FALSE;

    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    if (fnGetMeterModel() == CSD2)
    {
        fHandleMsg = TRUE;
    }
    else if (fnGetMeterModel() >= CSD3)
    {   // For DM400C, CM_RUNREQ only be handled in OIT_RUNNING or
        // OIT_PRINTING_TAPE mode.
        if (rCurrentState.wSYSMode == SYS_RUNNING ||
            rCurrentState.wSYSMode == SYS_PRINTING_TAPE)
        {
            fHandleMsg = TRUE;
        }
    }

    if ( (fHandleMsg == TRUE) &&
    	 ((CMOSTrmUploadDueReqd & TRM_UPLOAD_REQUIRED) != TRM_UPLOAD_REQUIRED) &&
		 (bGoToReady == TRUE) )
    {
    	//CreateTR
    	if(CreateXR() == SUCCESS)
    		fnProcessSTEvent (SEV_CMRUN);
    	else
    	{
    		dbgTrace(DBG_LVL_INFO, "fnmCMRunRequest:  CreateXR Failed - handleMsg: %d, CMOSTrmUploadDueReqd: %d, bGoToReady:%d", fHandleMsg, CMOSTrmUploadDueReqd, bGoToReady);
    		(void)trmCompletePendingWrites();

    	}
    }
    else
    {
    	dbgTrace(DBG_LVL_INFO, "fnmCMRunRequest: Can not take mail - handleMsg: %d, CMOSTrmUploadDueReqd: %d, bGoToReady:%d", fHandleMsg, CMOSTrmUploadDueReqd, bGoToReady);
    }



    return(SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnmCMModeResponse
//
// DESCRIPTION:
//      Intertask message handling function for CS_MODE_RSP
//
// INPUTS:
//      pMsgPtr  -   pointer to the message
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//
// *************************************************************************/
static BOOL fnmCMModeResponse(INTERTASK *pMsgPtr)
{
    if ( (rCurrentState.wSYSMode == SYS_PRINT_NOT_READY) && (rCurrentState.wSYSSubMode == SYS_EDM_LOCK) )
    {
        fnProcessSTEvent(SEV_PMCDISABLED);
    }
    else
    {
        fnProcessSTEvent(SEV_CMMODECONFIRM);
    }

    return (SUCCESSFUL);
}

static BOOL fnmOITNotifyFatalError(INTERTASK *MsgPtr)
{
    (void)OSSendIntertask(CM,SYS,SC_NOTIFY_FATAL_ERROR,NO_DATA,NULL,0);
    return(SUCCESSFUL);
}
static BOOL fnmCMOSRateCallBack(INTERTASK *MsgPtr)
{// this should never be called
    return(SUCCESSFUL);
}

/*------------------------------*/
/*   Private core functions     */
/*------------------------------*/



/* **********************************************************************
// FUNCTION NAME: fnProcessMessage
// DESCRIPTION: Private message processing engine for the system controller.
//              This function takes an intertask message and a lookup table
//              as input, and either calls a message processing function or
//              directly generates a state transition event (depending on
//              the entry in the table).
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pMsgTable- The message table that maps an input message to either
//                      a function call or a state transition event.
//          pMsg- The message being processed.
//
// *************************************************************************/
static void fnProcessMessage(TASK_MSG_TABLE_ENTRY *pMsgTable, INTERTASK *pMsg)
{
    register unsigned long  i = 0;
    unsigned short  wTableMsgID;

    wTableMsgID = pMsgTable[i].wMsgID;

    while (wTableMsgID != SYS_END_OF_TABLE)
    {
        if (wTableMsgID == (unsigned short) pMsg->bMsgId)
        {
            /*---- if checking long/word data, use explicit function instead of byte shortcuts */
            if (((pMsgTable[i].wByte0 == SYS_DONT_CARE) ||
                 (pMsgTable[i].wByte0 == (unsigned short) pMsg->IntertaskUnion.bByteData[0])) &&
                ((pMsgTable[i].wByte1 == SYS_DONT_CARE) ||
                 (pMsgTable[i].wByte1 == (unsigned short) pMsg->IntertaskUnion.bByteData[1])))
            {
                if (wTableMsgID == OIT_EVENT)
                {
                    UcEventID = pMsgTable[i].wByte0;
                    UcLastEvent = UcEventID;
                }

                /* either call the message function or directly generate the event */
                if (pMsgTable[i].bEvent == 0xFF)
                {
                    pMsgTable[i].pfnMsgFunction(pMsg);
                }
                else
                {
                    fnProcessSTEvent(pMsgTable[i].bEvent);
                }

                break;
            }
        }
        wTableMsgID = pMsgTable[++i].wMsgID;
    }
}


/* **********************************************************************
// FUNCTION NAME: fnSetWaitCondition
// DESCRIPTION:
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pMsgTable- The message table that maps an input message to either
//                      a function call or a state transition event.
//          pMsg- The message being processed.
//
// *************************************************************************/
static void fnSetWaitCondition(unsigned long lwEventsAwaited, unsigned long lwMilliseconds, 
                                T_FUNCPTR pResumeFn )
//                                void (* pResumeFn) ())
{
    char    pLogBuf[80];

    sprintf(pLogBuf,"Pwrup wait cond:%08lX ms:%08lX", lwEventsAwaited, lwMilliseconds);
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));

    lwPowerupSeqEventsAwaited = lwEventsAwaited;
    lwPowerupSeqWaitTimeout = lwMilliseconds;
    pPowerupSeqResumeFn = pResumeFn;
}

/* **********************************************************************
// FUNCTION NAME: fnSetPowerupSeqComplete
// DESCRIPTION:
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pMsgTable- The message table that maps an input message to either
//                      a function call or a state transition event.
//          pMsg- The message being processed.
//
// *************************************************************************/
static void fnSetPowerupSeqComplete( void )
{
    fPowerupSeqComplete = TRUE;
    pPowerupSeqResumeFn = NULL;
    fnDumpStringToSystemLog("SYS powerup sequence COMPLETE");
}


BOOL fnGetSysTaskPwrupSeqCompleteFlag( void )
{
    return (fPowerupSeqComplete);
}


/* **********************************************************************
// FUNCTION NAME: fnSendBobPrintMode
// DESCRIPTION: Private utility function that informs the SCM (bob) task
//              of the current print mode.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  bMode- The print mode to send to bob.
//
// *************************************************************************/
void fnSendBobPrintMode (unsigned char bMode)
{
    /* uncomment during BOB_INTEGRATION */

    if (fnWriteData(BOBAMAT0, UIC_PRINT_MODE, (void *) &bMode) != BOB_OK)
    {
        fnProcessSCMError();
    }

}

/* **********************************************************************
// FUNCTION NAME: fnSetSYSMessageTimeout
// DESCRIPTION: Sets up a timeout on an expected message response.  This
//              amounts to saving expected message information in global
//              variables and starting the SYS_MESSAGE_TIMER with the
//              appropriate timeout value.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pTimeoutInfo - pointer to the task ID/message ID combo(s) for
//                          which a wait timeout is being set.  the size
//                          of the array pointed to must be equal to bNumMessages
//          bNumMessages- the number of messages being awaited.  can be set
//                          to 0 to clear all wait conditions.
//          lwMilliTimeout - number of milliseconds to wait
//          bTimeoutType- SYS_TIMEOUT_TYPE_AND means all messages must be received
//                          before lwMilliTimeout milliseconds
//                        SYS_TIMEOUT_TYPE_OR means at least one message must
//                          be received before lwMilliTimeout milliseconds
//
// OUTPUTS: none
// *************************************************************************/
static void fnSetSYSMessageTimeout(SYS_MSG_TIMEOUT *pTimeoutInfo, unsigned char bNumMessages,
                                    unsigned long lwMilliTimeout, unsigned char bTimeoutType)
{
    int     i;


    /* disregard any timeouts above the maximum allowed number */
    if (bNumMessages > SYS_MAX_MSG_TIMEOUTS)
        bNumMessages = SYS_MAX_MSG_TIMEOUTS;

    rSYSMsgTimeoutCtrl.bNumWaitConditions = bNumMessages;

    if (bNumMessages == 0)
    {
        OSStopTimer(SYS_MESSAGE_TIMER);
        goto xit;
    }

    for (i = 0; i < bNumMessages; i++)
    {
        memcpy(&rSYSMsgTimeoutCtrl.pMsgTimeout[i], &pTimeoutInfo[i], sizeof(SYS_MSG_TIMEOUT));
        rSYSMsgTimeoutCtrl.fSatisfied[i] = FALSE;
    }
    rSYSMsgTimeoutCtrl.lwDuration = lwMilliTimeout;
    rSYSMsgTimeoutCtrl.bWaitCondType = bTimeoutType;

    /* set the timer duration and start the timer */
    OSChangeTimerDuration(SYS_MESSAGE_TIMER, lwMilliTimeout);
    OSStartTimer(SYS_MESSAGE_TIMER);
	
xit:
    return;
}

/* **********************************************************************
// FUNCTION NAME: fnCheckSYSMessageTimeout
// DESCRIPTION: Checks if an incoming message satisfies a timeout
//              condition that was previously set.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  pIntertask- pointer to intertask message being examined
//
// OUTPUTS: TRUE if a timeout had been set up on this message
//          FALSE if not
// *************************************************************************/
static BOOL fnCheckSYSMessageTimeout(INTERTASK *pIntertask)
{
    int     i;
    BOOL    fRetval = FALSE;

    for (i = 0; i < rSYSMsgTimeoutCtrl.bNumWaitConditions; i++)
    {
        if ((rSYSMsgTimeoutCtrl.pMsgTimeout[i].bTaskID == pIntertask->bSource) &&
            (rSYSMsgTimeoutCtrl.pMsgTimeout[i].bMessageID == pIntertask->bMsgId))
        {
            rSYSMsgTimeoutCtrl.fSatisfied[i] = TRUE;
            fRetval = TRUE;
        }
    }

    (void) fnCheckSYSWaitCondsSatisfied();

//xit:
    return (fRetval);
}

/* **********************************************************************
// FUNCTION NAME: fnCheckSYSWaitCondsSatisfied
// DESCRIPTION: Checks if an all systask message wait conditions have
//              been satisfied.  If so, the timer is stopped.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none
//
// OUTPUTS: TRUE if all conditions were satisfied (or none were set)
//          FALSE otherwise
// *************************************************************************/
static BOOL fnCheckSYSWaitCondsSatisfied( void )
{
    int     i;
    BOOL    fSatisfied;

    if (rSYSMsgTimeoutCtrl.bNumWaitConditions == 0)
        fSatisfied = TRUE;
    else
    {
        if (rSYSMsgTimeoutCtrl.bWaitCondType == SYS_TIMEOUT_TYPE_AND)
            fSatisfied = TRUE;  /* AND- we need all conditions, so the first not satisfied will make it false */
        else
            fSatisfied = FALSE; /* OR- we need one condition, so the first one satisfied makes it true */
    }

    for (i = 0; i < rSYSMsgTimeoutCtrl.bNumWaitConditions; i++)
    {
        if (rSYSMsgTimeoutCtrl.bWaitCondType == SYS_TIMEOUT_TYPE_AND)
        {
            if (rSYSMsgTimeoutCtrl.fSatisfied[i] == FALSE)
            {
                fSatisfied = FALSE;
                break;
            }
        }
        else
        {
            if (rSYSMsgTimeoutCtrl.fSatisfied[i] == TRUE)
            {
                fSatisfied = TRUE;
                break;
            }
        }
    }

    if (fSatisfied)
    {
        OSStopTimer(SYS_MESSAGE_TIMER);
        rSYSMsgTimeoutCtrl.bNumWaitConditions = 0;
    }

//xit:
    return (fSatisfied);
}


/*------------------------*/
/*   Public functions     */
/*------------------------*/

/* **********************************************************************
// FUNCTION NAME: fnSYSAllowRepeatOIEvent
// DESCRIPTION:
//   If UcLastEvent (the last OI event processed) is set to ucEvent, clear
//  it, so that the event can be processed again anyway.
//
// AUTHOR: Clarisa Bellamy
//
// INPUTS:
//    uchar ucEvent     OIT event to allow to repeat.
//
// NOTES:  This is why this was created in the first place...
//    We sometimes need to allow the OIT_ENTER_PRINT_RDY event to be
//      processed, even if it was the last event that we processed from
//      the OI.  Because sometimes we change what we are printing without
//      leaving the print ready state, and processing this event is what
//      sends the Gen Static Image request to Bobtask.
//    Without this, the data would not get updated before the print.

// **********************************************************************/
void fnSYSAllowRepeatOIEvent( uchar ucEvent )
{
    // We sometimes need to allow this event to be processed, even if it
    // was the last event that we processed from the OI.  Because sometimes
    // we change what we are printing without leaving the print ready state.
    // Processing this event is what sends the Gen Static Image request to Bobtask.
    if( UcLastEvent == ucEvent )
        UcLastEvent = 0;
}

/* **********************************************************************
// FUNCTION NAME: fnSYSMessageTimerExpire
// DESCRIPTION:  Timer expiration routine that handles the system
//                  controller message timer.  This is the timer that
//                  goes off when a message is expected from another
//                  task in the system but is never received.
//
// AUTHOR: Joe Mozdzer
//
// INPUTS:  none used
// **********************************************************************/
void fnSYSMessageTimerExpire(unsigned long lwArg)
{
    unsigned char   bErrorCode;
    int             i;
    char            pLogBuf[80];

    /* figure out which message was the culprit */
    if (rSYSMsgTimeoutCtrl.bWaitCondType == SYS_TIMEOUT_TYPE_AND)
    {
        /* if the AND type was set, look for the first unsatisified condition. */
        for (i = 0; i < rSYSMsgTimeoutCtrl.bNumWaitConditions; i++)
        {
            if (rSYSMsgTimeoutCtrl.fSatisfied[i] == FALSE)
                break;
        }
    }
    else
    {
        i = 0;  /* if the OR type was set, no messages must have been received so
                    the first one is considered the bad one */
    }

    /* map the task we waited for to the error code */
    char *pTaskName = "";
    switch (rSYSMsgTimeoutCtrl.pMsgTimeout[i].bTaskID)
    {
        case OIT:
            bErrorCode = ECSYS_MESSAGE_TIMEOUT_OIT;
            pTaskName = "(OIT)";
            break;
        case CM:
            bErrorCode = ECSYS_MESSAGE_TIMEOUT_CM;
            pTaskName = "(CM)";
            break;
        case SCM:
            bErrorCode = ECSYS_MESSAGE_TIMEOUT_SCM;
            pTaskName = "(SCM)";
            break;
        default:
            bErrorCode = ECSYS_MESSAGE_TIMEOUT_OTHER;
    }

    /* add the error and state info to the log.  for the AND type, log all awaited messages that weren't satisfied.
        for the OR type, log all messages (since none must have been satisfied) */
    for (i = 0; i < rSYSMsgTimeoutCtrl.bNumWaitConditions; i++)
    {
        if (((rSYSMsgTimeoutCtrl.bWaitCondType == SYS_TIMEOUT_TYPE_AND) && (rSYSMsgTimeoutCtrl.fSatisfied[i] == FALSE))
            || rSYSMsgTimeoutCtrl.bWaitCondType == SYS_TIMEOUT_TYPE_OR)
        {
//            sprintf(pLogBuf, "SYS msg timeout: Task 0x%02X%s Msg 0x%02X%s", rSYSMsgTimeoutCtrl.pMsgTimeout[i].bTaskID, pTaskName,
//                    rSYSMsgTimeoutCtrl.pMsgTimeout[i].bMessageID, fnMsgIDToName(rSYSMsgTimeoutCtrl.pMsgTimeout[i].bMessageID));
//            fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
// Split message into 2 parts so we can see it all in the syslog
            sprintf(pLogBuf, "SYS Msg timeout: Task 0x%02X%s",
            		rSYSMsgTimeoutCtrl.pMsgTimeout[i].bTaskID,
            		pTaskName);
            fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));

            sprintf(pLogBuf, "SYS Msg timeout: MsgID 0x%02X%s",
                    rSYSMsgTimeoutCtrl.pMsgTimeout[i].bMessageID,
                    fnMsgIDToName(rSYSMsgTimeoutCtrl.pMsgTimeout[i].bMessageID));
            fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));

        }
    }
    sprintf(pLogBuf, "Cur state-OI:%02X%02X F:%08lX CM:%02X PC:%02X", rCurrentState.wSYSMode,
            rCurrentState.wSYSSubMode, rCurrentState.lwFlags, rCurrentState.bPMCIState, rCurrentState.bPCState);
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));

    /* report the error.  in the future we may want this function to put a message on the SYS queue instead of reporting the
        error directly.  this would enable us to take generate a state transition event that could trigger a customized
        action for the particular timeout/state combo.  */
    fnReportMeterError(ERROR_CLASS_SYS, bErrorCode);
    rSYSMsgTimeoutCtrl.bNumWaitConditions = 0;
    OSStopTimer(SYS_MESSAGE_TIMER);
}

/* **********************************************************************
// FUNCTION NAME: msgIDToName
// DESCRIPTION: map the mesage type number to a more informative string
//
// AUTHOR: Richard Dore
//
// RETURNS: pointer to string description of message type
// *************************************************************************/
char *fnMsgIDToName(unsigned char ucType)
{
	char *pRes = "?";
	switch(ucType)
	{
		case SC_INIT_REQ: 			pRes = SC_INIT_REQ_NAME; break ;
		case SC_MODE_REQ: 			pRes = SC_MODE_REQ_NAME; break ;
		case SC_RUN_RSP: 			pRes = SC_RUN_RSP_NAME; break ;
		case SC_START_MAILRUN: 		pRes = SC_START_MAILRUN_NAME; break ;
		case SC_STOP_MAILRUN: 		pRes = SC_STOP_MAILRUN_NAME; break ;
		case SC_NOTIFY_FATAL_ERROR: pRes = SC_NOTIFY_FATAL_ERROR_NAME; break ;

		case CS_INIT_RSP: 			pRes = CS_INIT_RSP_NAME; break ;
		case CS_MODE_RSP: 			pRes = CS_MODE_RSP_NAME; break ;
		case CS_RUN_REQ: 			pRes = CS_RUN_REQ_NAME; break ;
		case CS_MAILRUN_STARTED: 	pRes = CS_MAILRUN_STARTED_NAME; break ;
		case CS_MAILRUN_CMPLT: 		pRes = CS_MAILRUN_CMPLT_NAME; break ;

		case CS_JOB_CANCEL: 		pRes = CS_JOB_CANCEL_NAME; break ;
		case CS_S3_COVERED: 		pRes = CS_S3_COVERED_NAME; break ;
		case CS_PRINT_STARTED: 		pRes = CS_PRINT_STARTED_NAME; break ;
		case CS_STATUS_RESPONSE: 	pRes = CS_STATUS_RESPONSE_NAME; break ;
	}

	return pRes;
}

/* **********************************************************************
// FUNCTION NAME: fnSYSIsPSDDetected
// DESCRIPTION: Public function that returns TRUE if the UIC has detected
//              that the vault/PSD is present or if it is not available for use
//
// AUTHOR: Joe Mozdzer
//
// RETURNS: TRUE if PSD detected, FALSE if not
// *************************************************************************/
BOOL    fnSYSIsPSDDetected( void )
{
    BOOL retval = TRUE;

// vaultPowerupResults is not supported yet, it needs to be eventually

    //if ((vaultPowerupResults == VLT_PSD_MFG_MODE_SET) || (vaultPowerupResults == VLT_DEVICE_NOT_THERE))
    //  retval = FALSE;

    // start hack
    extern  BOOL    fVltDataInitialized;        // set to TRUE with successful vault power up
    retval = fVltDataInitialized;
    // end hack

    return (retval);
}


/* **********************************************************************
// FUNCTION NAME: fnSYSCheckReadyState
// DESCRIPTION: Public function that determines if the system is in a ready
//              to print state.
//
// AUTHOR: Joe Mozdzer
//
// RETURNS: SYS_NOT_READY_TO_PRINT if not ready to print anything, otherwise
//          one of the other constants defined in sys.h is returned depending
//          on what we are ready to print.
// *************************************************************************/
unsigned char fnSYSCheckReadyState( void )
{
    unsigned char bRetval = SYS_NOT_READY_TO_PRINT;

    if ((rCurrentState.wSYSMode == SYS_PRINT_READY) && (rCurrentState.bPMCIState == CM_READY) &&
        !(rCurrentState.lwFlags & (F_BOB_UNCONFIRMED | F_CM_UNCONFIRMED | F_OI_NOT_NOTIFIED)))
    {
        switch (rCurrentState.wSYSSubMode)
        {
            case SYS_INDICIA:
            case SYS_PERMIT:
                bRetval = SYS_READY_INDICIA;
                break;
            case SYS_REPORT:
                bRetval = SYS_READY_REPORT;
                break;
            case SYS_TEST_PATTERN:
                bRetval = SYS_READY_TEST_PATTERN;
                break;
            case SYS_SEAL_ONLY:
                bRetval = SYS_READY_SEAL_ONLY;
                break;
            default:
                break;
        }
    }

    return (bRetval);
}


/* **********************************************************************
// FUNCTION NAME: fnSYSWhichSubMode
// DESCRIPTION: Public function that lets the EDM task know which mode we're in
//
// AUTHOR: Sandra Peterson
//
// RETURNS: OIT sub mode.
// *************************************************************************/
unsigned char fnSYSWhichSubMode( void )
{
    return (rCurrentState.wSYSSubMode);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnGetSysMode
//
// DESCRIPTION:
//      This function returns the current mode and submode to the caller.
//
// INPUTS:
//      pMode           - pointer to mode variable (for output)
//      pSubmode        - pointer to submode variable (for output)
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      3/13/2006   Steve Terebesi  Initial version
// *************************************************************************/
void fnGetSysMode(UINT8* pMode,
                  UINT8* pSubmode)
{
    *pMode    = (UINT8)rCurrentState.wSYSMode;
    *pSubmode = (UINT8)rCurrentState.wSYSSubMode;
}



/* *************************************************************************
// FUNCTION NAME:
//      fnProcessSTEvent
//
// DESCRIPTION:
//      This function processes a state transition event.  This involves
//      doing the the following:
//          1) Determine the current mode and select the appropriate state table
//          2) Find the entry in the table that matches the given event
//          3) Call the appropriate state transition function
//          4) Update the next state
//
// INPUTS:
//      ucEvent         - system event to be processed
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      3/13/2006   Steve Terebesi  Initial version
// *************************************************************************/
static void fnProcessSTEvent(UINT8 ucEvent)
{
    register unsigned long  i = 0;
    STATE_TABLE_TYPE*       pTable;
    UINT8                   ucPMCIStatus;
    FLAG_MASK_TYPE          stFlags;
    char            pLogBuf[80];

    /* first update the debug buffer */
    memcpy(&pStateBuf[lwStateBufIndex++], &ucEvent, sizeof(unsigned char));
    memcpy(&pStateBuf[lwStateBufIndex], &rCurrentState, sizeof(rCurrentState));

    lwStateBufIndex += sizeof(rCurrentState);

    pStateBuf[lwStateBufIndex++] = 0xEE;    /* this is an end-of-entry marker */

//  Calculate buffer index
    if ((lwStateBufIndex + 2 + sizeof(rCurrentState)) >
        (STATE_TRANSITION_LOG_SIZE - 1))
    {
        lwStateBufIndex = 0;
    }

    // output to syslog
    sprintf(pLogBuf, "SYS>> Event %d, PCSt %d, PMCISt %d", ucEvent, rCurrentState.bPCState, rCurrentState.bPMCIState);
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
    sprintf(pLogBuf, "SYS>> Flags 0x%lx, 0x%lx, Mode %d, SubM %d", rCurrentState.lwFlags, rCurrentState.lwFlagsEnable,
        rCurrentState.wSYSMode, rCurrentState.wSYSSubMode);
    fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));


//  Determine state table pointer
    pTable = StStateTablePtr[rCurrentState.wSYSMode][rCurrentState.wSYSSubMode];

    while (pTable[i].ucEvent != SEV_ENDOFTABLE)
    {
        ucPMCIStatus = pTable[i].pCurrInfo->ucPMCIState;
        stFlags      = pTable[i].pCurrInfo->stFlag;

        if ((ucEvent == pTable[i].ucEvent) &&
            ((ucPMCIStatus == SYS_BYTE_DC) ||
             (rCurrentState.bPMCIState == ucPMCIStatus)) &&
            ((rCurrentState.lwFlags & stFlags.ulFlagsEnable) ==
             (stFlags.ulFlags & stFlags.ulFlagsEnable)))
        {
            SYS_STATE   rNext;
//            SYS_STATE_TYPE stNextState;

            /* the next state that is in the table is what is originally loaded into the structure passed
                into the state transition function.  the function may use and/or modify the structure since
                it is passed by reference. */
            rNext.wSYSMode      = pTable[i].pNextMode->usMode;
            rNext.wSYSSubMode   = pTable[i].pNextMode->usSubmode;
            rNext.lwFlags       = pTable[i].pNextInfo->stFlag.ulFlags;
            rNext.lwFlagsEnable = pTable[i].pNextInfo->stFlag.ulFlagsEnable;
            rNext.bPMCIState    = pTable[i].pNextInfo->ucPMCIState;

//            stNextState.stMode  = *pTable[i].pNextMode;
//            stNextState.stInfo  = *pTable[i].pNextInfo;

            /* call the state transition function */
            pTable[i].pfnStateTransFn(&rNext);

            /* rNext now contains the updated next state.  update the current state.  the
                oit mode and pmci mode are not modified if dont care is the next state. */
            if (rNext.wSYSMode != SYS_DONT_CARE)
            {
                rCurrentState.wSYSMode = rNext.wSYSMode;
            }
            if (rNext.wSYSSubMode != SYS_DONT_CARE)
            {
                rCurrentState.wSYSSubMode = rNext.wSYSSubMode;
            }
            if (rNext.bPMCIState != SYS_BYTE_DC)
            {
                rCurrentState.bPMCIState = rNext.bPMCIState;
            }

            /* the flags enable member specifies which flags should be modified */
            /* start by zeroing those out */
            rCurrentState.lwFlags &= ~rNext.lwFlagsEnable;
            /* now OR with the new value */
            rCurrentState.lwFlags |= rNext.lwFlags & rNext.lwFlagsEnable;

            sprintf(pLogBuf, "SYS>> PCSt %d, PMCISt %d", rCurrentState.bPCState, rCurrentState.bPMCIState);
            fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));
            sprintf(pLogBuf, "SYS>> Flags 0x%lx, 0x%lx, Mode %d, SubM %d", rCurrentState.lwFlags, rCurrentState.lwFlagsEnable,
                rCurrentState.wSYSMode, rCurrentState.wSYSSubMode);
            fnSystemLogEntry(SYSLOG_TEXT, pLogBuf, strlen(pLogBuf));

            break;
        }
        i++;
    }

    if (pTable[i].ucEvent == SEV_ENDOFTABLE)
    {
/*      put a special indicator in the debug buffer if we received an unhandled event */
        pStateBuf[lwStateBufIndex++] = 0xDD;
    }
}



/* *************************************************************************
// FUNCTION NAME:
//      fnInitializeStateTablePtr
//
// DESCRIPTION:
//      This function initializes the array of state table pointers
//
// INPUTS:
//      None
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      3/14/2006   Steve Terebesi  Initial version
// *************************************************************************/
static void fnInitializeStateTablePtr(void)
{
    UINT8 i;
    UINT8 j;

//  Initialize table pointers
    for (i = 0; i < SYS_NUM_OF_MODES; i++)
    {
        for (j = 0; j < SYS_NUM_OF_SUBMODES; j++)
        {
            StStateTablePtr[i][j] = (STATE_TABLE_TYPE *)StCatchallState;
        }
    }

//  Assign specific table to mode/submode combo

//  System Powerup state
    StStateTablePtr[SYS_POWERUP][SYS_NO_SUBMODE]         = (STATE_TABLE_TYPE *)StPowerupState;

//  System Print Ready Indicia state
    StStateTablePtr[SYS_PRINT_READY][SYS_INDICIA]        = (STATE_TABLE_TYPE *)StPRIndiciaState;

//  System Print Ready Permit state
    StStateTablePtr[SYS_PRINT_READY][SYS_PERMIT]         = (STATE_TABLE_TYPE *)StPRIndiciaState;

//  System Print Ready Report state
    StStateTablePtr[SYS_PRINT_READY][SYS_REPORT]         = (STATE_TABLE_TYPE *)StPRReportState;

//  System Print Ready Test state
    StStateTablePtr[SYS_PRINT_READY][SYS_TEST_PATTERN]   = (STATE_TABLE_TYPE *)StPRTestState;

//  System Print Ready Seal Only state
    StStateTablePtr[SYS_PRINT_READY][SYS_SEAL_ONLY]      = (STATE_TABLE_TYPE *)StPRSealState;

//  System Print Ready Report state
    StStateTablePtr[SYS_PRINT_READY][SYS_REPORT]         = (STATE_TABLE_TYPE *)StPRIndiciaState;

//  System Print Not Ready state
    StStateTablePtr[SYS_PRINT_NOT_READY][SYS_NO_SUBMODE] = (STATE_TABLE_TYPE *)StPrintNotRdyState;

//  System Print Not Ready EDM Lock state
    StStateTablePtr[SYS_PRINT_NOT_READY][SYS_EDM_LOCK] = (STATE_TABLE_TYPE *)StPrintNotRdyEdmLockState;

//  System Running Indicia state
    StStateTablePtr[SYS_RUNNING][SYS_INDICIA]            = (STATE_TABLE_TYPE *)StRunningIndiciaState;

//  System Running Permit state
    StStateTablePtr[SYS_RUNNING][SYS_PERMIT]             = (STATE_TABLE_TYPE *)StRunningPermitState;

//  System Running Permit state
    StStateTablePtr[SYS_RUNNING][SYS_REPORT]             = (STATE_TABLE_TYPE *)StRunningIndiciaState;

//  System Running Seal Only state
    StStateTablePtr[SYS_RUNNING][SYS_SEAL_ONLY]          = (STATE_TABLE_TYPE *)StRunningSealState;

//  System Running Test state
    StStateTablePtr[SYS_RUNNING][SYS_TEST_PATTERN]       = (STATE_TABLE_TYPE *)StRunningTestState;

//  System Printing Tape Indicia state
    StStateTablePtr[SYS_PRINTING_TAPE][SYS_INDICIA]      = (STATE_TABLE_TYPE *)StPrintTapeIndiciaState;

//  System Printing Tape Permit state
    StStateTablePtr[SYS_PRINTING_TAPE][SYS_PERMIT]       = (STATE_TABLE_TYPE *)StPrintTapePermitState;

//  System Printing Tape Report state
    StStateTablePtr[SYS_PRINTING_TAPE][SYS_REPORT]       = (STATE_TABLE_TYPE *)StPrintTapeIndiciaState;

//  System Printing Tape Test state
    StStateTablePtr[SYS_PRINTING_TAPE][SYS_TEST_PATTERN] = (STATE_TABLE_TYPE *)StPrintTapeIndiciaState;

//  System Sleep Normal state
    StStateTablePtr[SYS_SLEEP][SYS_NORMAL_SLEEP]         = (STATE_TABLE_TYPE *)StSleepState;

//  System Sleep Soft Power Down state
    StStateTablePtr[SYS_SLEEP][SYS_SOFT_POWER_DOWN]      = (STATE_TABLE_TYPE *)StSleepState;

//  System Meter Error state
    StStateTablePtr[SYS_METER_ERROR][SYS_NO_SUBMODE]     = (STATE_TABLE_TYPE *)StMeterErrState;

//  Ststem Diagnostics state
    StStateTablePtr[SYS_DIAGNOSTICS][SYS_NO_SUBMODE]     = (STATE_TABLE_TYPE *)StDiagnosticsState;

//  System Manufacture state
    StStateTablePtr[SYS_MANUFACTURING][SYS_NO_SUBMODE] = (STATE_TABLE_TYPE *)StMfgState;
}


/* *************************************************************************
// FUNCTION NAME:
//      fnsOITSendAnyPREvent
//
// DESCRIPTION:
//      This function send one of the Print Ready events to the OIT, based
//      on the current submode.
//
// INPUTS:
//      pNextState      - pointer to next state (not used)
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      3/20/2006   Steve Terebesi  Initial version
// *************************************************************************/
static void fnsOITSendAnyPREvent(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2];

//  Send event based on the current submode
    switch (rCurrentState.wSYSSubMode)
    {
        case SYS_PERMIT:
            pMsgData[0] = OIT_PRINT_PERMIT;
            break;
        case SYS_SEAL_ONLY:
            pMsgData[0] = OIT_PRINT_NO_INDICIA;
            break;
        case SYS_REPORT:
            pMsgData[0] = OIT_PREP_FOR_RPT_PRINTING;
            break;
        case SYS_TEST_PATTERN:
            pMsgData[0] = OIT_PRINT_ENV_TEST;
            break;
        default:
            pMsgData[0] = OIT_ENTER_PRINT_RDY;
            break;
    }

    pMsgData[1] = 0;

//  Send event to OIT
    OSSendIntertask (OIT, SYS, OIT_REC_EVENT, BYTE_DATA, pMsgData, 2);

    SendBaseEventToTablet(BASE_EVENT_PRINTER_STATUS_UPDATED, NULL);
}



/* *************************************************************************
// FUNCTION NAME:
//      fnsOITSendLeftPREvent
//
// DESCRIPTION:
//      This function sends the Left Print Ready event to the OIT.
//
// INPUTS:
//      pNextState      - pointer to next state (not used)
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      3/20/2006   Steve Terebesi  Initial version
// *************************************************************************/
static void fnsOITSendLeftPREvent(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2] = {OIT_LEFT_PRINT_RDY, 0};

    OSSendIntertask (OIT, SYS, OIT_REC_EVENT, BYTE_DATA, pMsgData, 2);

    SendBaseEventToTablet(BASE_EVENT_PRINTER_STATUS_UPDATED, NULL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnsEDMSendLeftPREvent
//
// DESCRIPTION:
//      This function sends the Left Print Ready event to the OIT and updates
//      the EDM mode.
//
// INPUTS:
//      pNextState      - pointer to next state (not used)
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      7/23/2009   Raymond Shen  Initial version
// *************************************************************************/
static void fnsEDMSendLeftPREvent(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2] = {OIT_LEFT_PRINT_RDY, 0};

    OSSendIntertask (OIT, SYS, OIT_REC_EVENT, BYTE_DATA, pMsgData, 2);

}


/* *************************************************************************
// FUNCTION NAME:
//      fnsOITSendAnySleepEvent
//
// DESCRIPTION:
//      This function send one of the Sleep events to the OIT, based
//      on the current submode.
//
// INPUTS:
//      pNextState      - pointer to next state (not used)
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      3/20/2006   Steve Terebesi  Initial version
// *************************************************************************/
static void fnsOITSendAnySleepEvent(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2];

//  Send event based on the current submode
    if (rCurrentState.wSYSSubMode == SYS_NORMAL_SLEEP)
    {
        pMsgData[0] = OIT_GOTO_SLEEP;
    }
    else
    {
        pMsgData[0] = OIT_ENTER_POWERDOWN;
    }

    pMsgData[1] = 0;

//  Send event to OIT
    OSSendIntertask (OIT, SYS, OIT_REC_EVENT, BYTE_DATA, pMsgData, 2);

}



/* *************************************************************************
// FUNCTION NAME:
//      fnsOITSendMeterError
//
// DESCRIPTION:
//      This function sends the Meter Error event to the OIT.
//
// INPUTS:
//      pNextState      - pointer to next state (not used)
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      3/20/2006   Steve Terebesi  Initial version
// *************************************************************************/
static void fnsOITSendMeterError(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2] = {OIT_ERROR_METER, 0};

    OSSendIntertask (OIT, SYS, OIT_REC_EVENT, BYTE_DATA, pMsgData, 2);

}



/* *************************************************************************
// FUNCTION NAME:
//      fnsOITSendManufEvent
//
// DESCRIPTION:
//      This function sends the Enter Manufactoring event to the OIT.
//
// INPUTS:
//      pNextState      - pointer to next state (not used)
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      3/23/2006   Steve Terebesi  Initial version
// *************************************************************************/
static void fnsOITSendManufEvent(SYS_STATE *pNextState)
{
    //send mode/sub mode
    unsigned char   pMsgData[2] = {OIT_MANUFACTURING/*OIT_ENTER_MANUFACTURING*/, 0};

    OSSendIntertask (OIT, SYS, OIT_CHANGE_MODE/*OIT_REC_EVENT*/, BYTE_DATA, pMsgData, 2);
}


/* *************************************************************************
// FUNCTION NAME:
//      fnsOITSendRunEvent
//
// DESCRIPTION:
//      This function send one of the Print Ready events to the OIT, based
//      on the current submode.
//
// INPUTS:
//      pNextState      - pointer to next state (not used)
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      3/23/2006   Steve Terebesi  Initial version
// *************************************************************************/
static void fnsOITSendRunEvent(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2];

//  Send event based on the current submode
    switch (rCurrentState.wSYSSubMode)
    {
        case SYS_PERMIT:
            pMsgData[0] = OIT_RUNNING_PERMIT;
            break;
        case SYS_SEAL_ONLY:
            pMsgData[0] = OIT_RUNNING_SEAL;
            break;
        case SYS_REPORT:
            pMsgData[0] = OIT_RUNNING_REPORT;
            break;
        case SYS_TEST_PATTERN:
            pMsgData[0] = OIT_RUNNING_TEST;
            break;
        default:
            pMsgData[0] = OIT_PRINTING;
            break;
    }

    pMsgData[1] = 0;

//  Send event to OIT
    OSSendIntertask (OIT, SYS, OIT_REC_EVENT, BYTE_DATA, pMsgData, 2);
}


/* *************************************************************************
// FUNCTION NAME:
//      fnsOITSendTapeEvent
//
// DESCRIPTION:
//      This function send one of the Print Tape events to the OIT, based
//      on the current submode.
//
// INPUTS:
//      pNextState      - pointer to next state (not used)
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      3/23/2006   Steve Terebesi  Initial version
// *************************************************************************/
static void fnsOITSendTapeEvent(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2];

//  Send event based on the current submode
    switch (rCurrentState.wSYSSubMode)
    {
        case SYS_REPORT:
            pMsgData[0] = OIT_PRINT_RPT_TO_TAPE;
            break;
        case SYS_TEST_PATTERN:
            pMsgData[0] = OIT_PRINT_TAPE_TEST;
            break;
    case SYS_PERMIT:
      pMsgData[0] = OIT_PRINT_TAPE_PERMIT;
      break;
        default:
            pMsgData[0] = OIT_PRINT_TAPE;
            break;
    }

    pMsgData[1] = 0;

//  Send event to OIT
    OSSendIntertask (OIT, SYS, OIT_REC_EVENT, BYTE_DATA, pMsgData, 2);
}



/* *************************************************************************
// FUNCTION NAME:
//      fnOITSendAck
//
// DESCRIPTION:
//      This function ACKs the event request from the OIT
//
// INPUTS:
//      None
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//      05/01/2006  Steve Terebesi  Initial version
// *************************************************************************/
#if 0
static void fnsOITSendAck(SYS_STATE *pNextState)
{
    unsigned char   pMsgData[2];

    if (UcEventID == 0xFF)
    {
        pMsgData[0] = (unsigned char) rCurrentState.wSYSMode;
        pMsgData[1] = (unsigned char) rCurrentState.wSYSSubMode;

        OSSendIntertask (OIT, SYS, OIT_CHANGE_MODE, BYTE_DATA, pMsgData, sizeof(pMsgData));
    }
    else
    {
        pMsgData[0] = UcEventID;
        pMsgData[1] = 0xFF;

        OSSendIntertask (OIT, SYS, OIT_ACK, BYTE_DATA, pMsgData, sizeof(pMsgData));

        UcEventID = 0xFF;
        UcLastEvent = 0xFF;
    }
}
#endif
/* *************************************************************************
// FUNCTION NAME:
//      fnsOITSendMode
//
// DESCRIPTION:
//
//
// INPUTS:
//      None
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
// *************************************************************************/
static void fnsOITSendMode(SYS_STATE *pNextState)
{
  unsigned char pMsgData[2];

  pMsgData[0] = (unsigned char) rCurrentState.wSYSMode;
  pMsgData[1] = (unsigned char) rCurrentState.wSYSSubMode;

  OSSendIntertask (OIT, SYS, OIT_CHANGE_MODE, BYTE_DATA, pMsgData, 2);

}

/* **********************************************************************
// FUNCTION NAME: fnsPrintTapeIndicia
// DESCRIPTION: State transition function that does the following:
//          Puts bob in debit funds mode
//          Sends a mode request to PMCI to get ready to print an
//            indicia on a tape.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsPrintTapeIndicia(SYS_STATE *pNextState)
{
    UINT8   pMsgData[2];

    // Send SYS the message to request to start tape run
    pMsgData[0] = MEDIA_TAPE;
    pMsgData[1] = 1;

    OSSendIntertask (SYS, OIT, OIT_START_MAIL_REQ, BYTE_DATA, pMsgData,
                        sizeof(pMsgData));
}

/* **********************************************************************
// FUNCTION NAME: fnsEDMResendMode
// DESCRIPTION: State transition function that does the following:
//          Sends the current mode to the EDM.  This is done
//            regardless of if we were already in that mode.
//            (Ordinarily a mode change is only send to the EDM
//            when the mode changes; this function is an exception to
//            that rule)
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsEDMResendMode(SYS_STATE *pNextState)
{
  unsigned short  wMode;
  unsigned short  wSubMode;

  if (pNextState->wSYSMode == SYS_DONT_CARE)
    wMode = rCurrentState.wSYSMode;
  else
    wMode = pNextState->wSYSMode;

  if (pNextState->wSYSSubMode == SYS_DONT_CARE)
    wSubMode = rCurrentState.wSYSSubMode;
  else
    wSubMode = pNextState->wSYSSubMode;

  bEDMLastMode = 0xFF;  /* set this to a non real value so that we force notification to EDM even
                if we already send it */
}

/* **********************************************************************
// FUNCTION NAME: fnsPMCQuickPause
// DESCRIPTION: State transition function that does the following:
//        Sends a request to the PMC task to Pause the active profiles.
//        Immediately initiates a mode change, rather than wait for
//        PMCI to return mail job complete.
//        (Integrity?...questionable, defilcj)
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsPMCQuickPause(SYS_STATE *pNextState)
{
    UINT8   pMsgData[1];

    // Send SYS the message to request to start tape run
    pMsgData[0] = MEDIA_TAPE;

    // Forward the message to CM
    OSSendIntertask (CM, SYS, SC_STOP_MAILRUN, BYTE_DATA, &pMsgData, 1);
  fnsOITSendNextMode(pNextState);
}

/* **********************************************************************
// FUNCTION NAME: fnsOITAttemptEnable
// DESCRIPTION: State transition function that does the following:
//          Sends a meter enable request to the OIT.
//
// AUTHOR: Joe Mozdzer
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsOITAttemptEnable(SYS_STATE *pNextState)
{
  OSSendIntertask (OIT, SYS, OIT_METER_ENABLE_REQ, NO_DATA, "", 0);
}

// Initialize key system information
/* *************************************************************************
// FUNCTION NAME:       fnInitializeSystemInfo
// DESCRIPTION: 
//      Initialize a bunch of system info?  This is called near the beginning
//      of the system control task.
//  
// INPUTS:  None  
// OUTPUTS: None
// NOTES: 
//  1. 
//
// HISTORY:
//  2011.08.08 Clarisa Bellamy - Fix bug: When bad PCN is found, replace it 
//                      with an empty string instead of a default, because an 
//                      empty string will cause the FFS file cleanup to be skipped.
//                      The file cleanup was deleting the .ARC file after PCT 
//                      without a PSD, becuase the name did not match the PCN.
//                      - Added function header.
// --------------------------------------------------------------------------*/
static void fnInitializeSystemInfo(void)
{
    BOOL    fBadString = FALSE;
    char *  pString = NULL;
    int     i = 0;
    int     strLength = 0;
    char    pLogBuf[60];

    // determine if PCN is valid
    fBadString = FALSE;
    i = 0;
    strLength = 0;
    pString = (char*)CMOSSignature.bUicPcn;
    while (i < SIZEOFUICPCN && *pString && !fBadString)
    {
        if (!isprint(*pString))
          fBadString = TRUE;
        else
        {
          i++;
          strLength++;
          pString++;
        }
    }
    if (strLength == 0 || strLength >= SIZEOFUICPCN)
    {
        fBadString = TRUE;
    }
	
    // if the PCN is not valid - initialize it to a "safe" default
    if( fBadString )
    {
        // If PCN is bad, make sure it is an EMPTY string, so ffs cleanup does
        //  NOT delete the archive file.  If there is no PSD, then we want to 
        //  keep the archive file.
        CMOSSignature.bUicPcn[0] = 0;
    }
	
    sprintf( pLogBuf, "%sUic PCN= %s", (fBadString ? "BAD " : ""), CMOSSignature.bUicPcn );
    fnSystemLogHeader( pLogBuf, 0 );

  // determine if UIC Serial Number is valid
    fBadString = FALSE;
    i = 0;
    strLength = 0;
    pString = (char*)CMOSSignature.bUicSN;
    while (i < SIZEOFUICSN && *pString && !fBadString)
    {
        if (!isprint(*pString))
            fBadString = TRUE;
        else
        {
            i++;
            strLength++;
            pString++;
        }
    }
    if (strLength == 0 || strLength >= SIZEOFUICSN)
        fBadString = TRUE;
		
    // if the serial number is not valid - initialize it to a "safe" default
    if (fBadString)
        (void)strcpy( CMOSSignature.bUicSN, "0000000" );

    sprintf( pLogBuf, "%sUic SN= %s", (fBadString ? "Default " : ""), CMOSSignature.bUicSN );
    fnSystemLogHeader( pLogBuf, 0 );
}


unsigned short fnSYSGetCurrentMode(void)
{
  return rCurrentState.wSYSMode;
}
unsigned short fnSYSGetCurrentSubMode(void)
{
  return rCurrentState.wSYSSubMode;
}

/* **********************************************************************
// FUNCTION NAME: fnAbCancelPrintJob
// DESCRIPTION: State transition function that does the following:
//                  Sends the next mode to OIT and  sends
//                  an update-display message to OIT so that any
//                disabling condition will be displayed for User.
//
// AUTHOR: "adopted" Oleg Taranda
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsAbCancelPrintJob(SYS_STATE *pNextState)
{
//    unsigned char  ucUpdateMode = (unsigned char) TRUE ;

    pNextState->wSYSMode = SYS_DONT_CARE;
    pNextState->wSYSSubMode = SYS_DONT_CARE;
    pNextState->bPMCIState = SYS_BYTE_DC;
    pNextState->bPCState = SYS_BYTE_DC;
    pNextState->lwFlags = 0x0000;
    pNextState->lwFlagsEnable = 0x0000;

    // do normal messaging on state transition
    fnsOITSendNextMode( pNextState ) ;
}

/* **********************************************************************
// FUNCTION NAME: fnsHandleMailJobComplete
// DESCRIPTION: State transition function that does the following:
//                  If in Report sub mode, leave ready mode, else do nothing
//
//
// NEXT STATE:  Unmodified (next state from table is used)
// *************************************************************************/
static void fnsHandleMailJobComplete(SYS_STATE *pNextState)
{
	unsigned char   pMsgData[2];

	//TODO - confirm this right state to be in
    if (rCurrentState.wSYSSubMode == SYS_REPORT)
    {// Leave ready mode if printed a report
    	pMsgData[0] = OIT_LEFT_PRINT_RDY;
    	pMsgData[1] = 0;
    	(void)OSSendIntertask(SYS, OIT, OIT_EVENT, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);
    }
    else
    {
    	fnsDoNothing(pNextState);
    }
}

// *************************************************************************/
// enableDelayedNetworkComponents
//
// Using the run level 0 init  function kick off the networking modules in the
// correct order.
// *************************************************************************/
static void enableDelayedNetworkComponents()
{
	STATUS status = NU_SUCCESS;
	char *pPath;
	BOOL bResult ;
	char sbuf[100];

	dbgTrace(DBG_LVL_INFO, "Networking delayed initialization started");
	// nu.os.conn.usb.func.stack runlevel 5

	// nu.os.conn.usb.func.comm.class runlevel 7

	// nu.os.net.stack runlevel 7

	// default runlevel 8
	pPath = "/nu/os/conn/usb/func/comm/eth";
	status = NU_RunLevel_0_Init(pPath);
	if(status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Runlevel [%s] FAILED returned %d", pPath, status);
	}

	// default runlevel 11
	pPath = "/csd_2_3/usbf0";
	status = NU_RunLevel_0_Init(pPath);
	if(status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Runlevel [%s] FAILED returned %d", pPath, status);
	}

	// default runlevel 12
	pPath = "/csd_2_3/usbf1";
	status = NU_RunLevel_0_Init(pPath);
	if(status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Runlevel [%s] FAILED returned %d", pPath, status);
	}

	// default runlevel 13
	pPath = "/csd_2_3/ethernet0";
	status = NU_RunLevel_0_Init(pPath);
	if(status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Runlevel [%s] FAILED returned %d", pPath, status);
	}

	// default runlevel 16
	pPath = "/nu/os/net/dhcp/server/ipv4";
	status = NU_RunLevel_0_Init(pPath);
	if(status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Runlevel [%s] FAILED returned %d", pPath, status);
	}

	// default runlevel 16
	pPath = "/nu/os/net/http";
	status = NU_RunLevel_0_Init(pPath);
	if(status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Runlevel [%s] FAILED returned %d", pPath, status);
	}

	// default runlevel 16
	pPath = "/nu/os/net/wsox";
	status = NU_RunLevel_0_Init(pPath);
	if(status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Runlevel [%s] FAILED returned %d", pPath, status);
	}

	// if here all is OK
	dbgTrace(DBG_LVL_INFO, "Networking delayed initialization completed");

	// Suspend waiting for network to come up
	status = NETBOOT_Wait_For_Network_Up(NU_SUSPEND);
	if(status == NU_SUCCESS)
	{
        fnDumpStringToSystemLog( "Networking component initialized" );
    }
	else
	{
		sprintf( sbuf, "Error: Failed Networking Init: %d", status);
		fnSystemLogEntry( SYSLOG_TEXT, sbuf, strlen(sbuf) );
	}

    /************************************************/
    /* Initialize the DHCP Server for USB-C port    */
    /************************************************/
    DHCPS_USB_Init();

	updateNetworkMACAddsFromSITARA(TRUE);

	// now start other tasks
#if DBG_TRACE_TO_TCPIP == 1
	bResult = OSResumeTask(DEBUGTASK);
	sprintf( sbuf, "OSResumeTask %s - %s", Tasks[DEBUGTASK].szTaskName, bResult? "SUCCESS" : "FAILURE"  );
	fnSystemLogEntry( SYSLOG_TEXT, sbuf, strlen(sbuf) );
#endif

	bResult = OSResumeTask(NETMONTASK);
	sprintf( sbuf, "OSResumeTask %s - %s", Tasks[NETMONTASK].szTaskName, bResult? "SUCCESS" : "FAILURE"  );
	fnSystemLogEntry( SYSLOG_TEXT, sbuf, strlen(sbuf) );

	fnInitSSLLibrary();

	bResult = OSResumeTask(WSTXSRVTASK);
	sprintf( sbuf, "OSResumeTask %s - %s", Tasks[WSTXSRVTASK].szTaskName, bResult? "SUCCESS" : "FAILURE"  );
	fnSystemLogEntry( SYSLOG_TEXT, sbuf, strlen(sbuf) );

	bResult = OSResumeTask(WSRXSRVTASK);
	sprintf( sbuf, "OSResumeTask %s - %s", Tasks[WSRXSRVTASK].szTaskName, bResult? "SUCCESS" : "FAILURE"  );
	fnSystemLogEntry( SYSLOG_TEXT, sbuf, strlen(sbuf) );

}
