
#include "nucleus.h"
#include "ossetup.h"

#include "networking/nu_networking.h"
#include "networking/http/inc/http_lite_svr.h"
#include "bsp/csd_2_3_defs.h"
#include "NOVAdriver.h"
#include "custdat.h"
#include "api_status.h"
#include "commontypes.h"
#include "pbos.h"
#include "socketd.h"
#include "cJSON.h"
#include "memory_stats.h"
#include "tcp.h"
#include "socketd.h"
#include "cJSON.h"
#include "wsox_tx_queue.h"
#include "wsox_services.h"
#include "debugTask.h"
#include "utils.h"
#include "networkmonitor.h"
#include "api_funds.h"
#include "api_utils.h"
#include "cmos.h"

#define NET_INTERFACE_1		"net0"
#define NET_INTERFACE_2		"net1"
#define NET_INTERFACE_ETH0	"eth0"
#define	NAT_INTERFACE_COUNT	2
#define	DHCP_RETRY_LIMIT	5

// proxy handling
//extern ProxyConfig globalProxyConfig;

// prototypes
extern unsigned short 			EndianSwap16(const unsigned short inval);
extern unsigned long 			EndianSwap32(const unsigned long inval);

extern DHCPS_CONFIG_CB     		*globalDeviceDHCPCtrlBlk;
extern const char 				*api_status_to_string_tbl[];
extern NetworkInformationStruct	CMOSglobalNetworkInformationStructure;
extern HTTP_SVR_DATA 			*HTTP_SVR_Data;
extern struct addr_struct 		globalTransientDebugAddress;

extern NU_SEMAPHORE Ifconfig_Semaphore;

#define MAX(a,b) ( (a)>(b)?(a):(b) )
static const UINT8 			null_ip []	= {0,0,0,0};

static UINT8 localDHCPRetryCount = 0 ;

UINT8  getDHCPRetryCount() 		{ return localDHCPRetryCount;}
void   clearDHCPRetryCount() 	{ localDHCPRetryCount = 0;}
void   incrementDHCPRetryCount(){ localDHCPRetryCount++;}

UINT8 			globalDefaultIPv4Gateway[MAX_ADDRESS_SIZE] ;
UINT8 			globalDefaultIPv6Gateway[MAX_ADDRESS_SIZE] ;
UINT32 			globalLastIP4RTAB4Route = 0;
CSDProxyConfig 	globalProxyConfig = {FALSE, 0};  // 1st eles are the used flag and exclusion count so initialize as unused and 0 count.

// playing games here to work around a nucleus bug/feature. If dhcp is enabled in the build we cannot touch it since the DHCP structure
// is internal and inaccessible. So the build needs to have this turned off. To try and provide some clue to this we initialize a pointer to
// our own structure, if this pointer is null and we have been asked to turn off dhcp we know there is a problem.
// setup a set of standard user options, these match the ones that Nucleus sets in NETBOOT_Resolve_DHCP4
//UINT8 globalDHCPOptions [7 + MAX_HOST_NAME_LENGTH + 1] = {DHCP_REQUEST_LIST, 3, DHCP_NETMASK, DHCP_ROUTE, DHCP_DNS, DHCP_HOSTNAME, 0};
// adjustment, the netboot version contains a hostname addition for some reason, this sent the hostname localhost to the dhcp server
// which is not very useful, initially I left this as a null character (no length) but some DHCP servers got upset and would not
// send a response so now the options array simply sends the required parameter list and nothing more.
UINT8 globalDHCPOptions [5] = {DHCP_REQUEST_LIST, 3, DHCP_NETMASK, DHCP_ROUTE, DHCP_DNS};
UINT8 globalDHCPOptionsLength = 5;

NU_DHCP_STRUCT 				globalEth0DhcpStruct ;
NU_DHCP_STRUCT				*ptrGlobalEth0DhcpStruct = NULL ;

DeviceNameElement			globalDeviceNameArray[DEVICE_NAME_ARRAY_MAX];

// Networking state variables
static BOOL ethernetLinkUp;
static BOOL tabletLinkUp;
static BOOL wiredInternetConnected;
static BOOL wifiInternetConnected;

// Networking state function table
typedef BOOL (*FN_PROCESS_NET_EVT)(NETWORK_EVENT, NetworkInformationStruct *, char *, int);
static BOOL fnDoNothing(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnInvalid(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnTabLinkUp(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnTabLnUpInt(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnTabLnUpNAT(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnTabLinkDn(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnTabLnDnNAT(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnTabLnDnDNS(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnEthLinkUp(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnEthLnUpTab(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnEthLnUpTbWf(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnEthLinkDn(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnEthLnDnNAT(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnEthLnDnWf(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWifNewAtt(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWifNewConn(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWif2WirSt(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWifClrSetCnf(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWirStNewAtt(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWif2WirDHCP(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWirDHCP(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWirDHCPRfsh(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWirDHCP2St(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWirSt(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWirSt2DHCP(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWirDHCP2Wif(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWirCnf2Wif(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWirCnf(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnWirSt2Wif(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnDHCPAddrOK(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnDHCPAddrOKDNS(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);
static BOOL fnDHCPAddrFail(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize);


//                                                    [ethernetLinkUp][tabletLinkUp][wiredInternetConnected][wifiInternetConnected]
const FN_PROCESS_NET_EVT netStateFunctionTable[NETEVT_MAX][2][2][2][2] = {

// Invalid states:
// ethernetLinkUp 0 AND wiredIntCon 1
// tabletLinkUp 0 AND wifiIntCon 1
// wiredIntCon 1 AND wifiIntCon 1	(for now)

//		NETEVT_NOCHANGE,
						// tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1             wiredIntCon 0               wiredIntCon 1
  { { { {fnDoNothing, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnDoNothing, fnDoNothing}, {fnInvalid, fnInvalid} } }, 	// ethernetLinkUp 0
	{ { {fnDoNothing, fnInvalid}, {fnDoNothing, fnInvalid} }, { {fnDoNothing, fnDoNothing}, {fnDoNothing, fnInvalid} } } }, 	// ethernetLinkUp 1

//	NETEVT_TABLET_LINK_UP,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1             wiredIntCon 0               wiredIntCon 1
  { { { {fnTabLinkUp, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnDoNothing, fnDoNothing}, {fnInvalid, fnInvalid} } },     // ethernetLinkUp 0
	{ { {fnTabLnUpInt, fnInvalid}, {fnTabLnUpNAT, fnInvalid} }, { {fnDoNothing, fnDoNothing}, {fnDoNothing, fnInvalid} } } },   // ethernetLinkUp 1

//	NETEVT_TABLET_LINK_DOWN,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1             wiredIntCon 0               wiredIntCon 1
  { { { {fnDoNothing, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnTabLinkDn, fnTabLnDnDNS}, {fnInvalid, fnInvalid} } },     // ethernetLinkUp 0
	{ { {fnDoNothing, fnInvalid}, {fnDoNothing, fnInvalid} }, { {fnTabLinkDn, fnTabLinkDn}, {fnTabLnDnNAT, fnInvalid} } } },   // ethernetLinkUp 1

//	NETEVT_ETHERNET_LINK_UP,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1             wiredIntCon 0               wiredIntCon 1
  { { { {fnEthLinkUp, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnEthLnUpTab, fnEthLnUpTbWf}, {fnInvalid, fnInvalid} } },     // ethernetLinkUp 0
	{ { {fnDoNothing, fnInvalid}, {fnDoNothing, fnInvalid} }, { {fnDoNothing, fnDoNothing}, {fnDoNothing, fnInvalid} } } },   // ethernetLinkUp 1

//	NETEVT_ETHERNET_LINK_DOWN,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1              wiredIntCon 0               wiredIntCon 1
  { { { {fnDoNothing, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnDoNothing, fnDoNothing}, {fnInvalid, fnInvalid} } },     // ethernetLinkUp 0
    { { {fnEthLinkDn, fnInvalid}, {fnEthLinkDn, fnInvalid} }, { {fnEthLinkDn, fnEthLnDnWf}, {fnEthLnDnNAT, fnInvalid} } } },   // ethernetLinkUp 1

//	NETEVT_WIFI_NEW_ATTRIB,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1              wiredIntCon 0               wiredIntCon 1
  { { { {fnDoNothing, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnWifNewConn, fnWifNewAtt}, {fnInvalid, fnInvalid} } },     // ethernetLinkUp 0
	{ { {fnDoNothing, fnInvalid}, {fnDoNothing, fnInvalid} }, { {fnDoNothing, fnDoNothing}, {fnDoNothing, fnInvalid} } } },   // ethernetLinkUp 1

//	NETEVT_WIFI_TO_WIRED_STATIC,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1              wiredIntCon 0               wiredIntCon 1
  { { { {fnDoNothing, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnWirCnf, fnWifClrSetCnf}, {fnInvalid, fnInvalid} } },     // ethernetLinkUp 0
	{ { {fnDoNothing, fnInvalid}, {fnDoNothing, fnInvalid} }, { {fnWirSt, fnWif2WirSt}, {fnDoNothing, fnInvalid} } } },   // ethernetLinkUp 1

//	NETEVT_WIFI_TO_WIRED_DHCP,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1               wiredIntCon 0               wiredIntCon 1
  { { { {fnDoNothing, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnWirCnf, fnWifClrSetCnf}, {fnInvalid, fnInvalid} } },     // ethernetLinkUp 0
	{ { {fnDoNothing, fnInvalid}, {fnDoNothing, fnInvalid} }, { {fnWirDHCP, fnWif2WirDHCP}, {fnDoNothing, fnInvalid} } } },   // ethernetLinkUp 1

//	NETEVT_WIRED_STATIC_NEW_ATTRIB,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1              wiredIntCon 0               wiredIntCon 1
  { { { {fnDoNothing, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnDoNothing, fnDoNothing}, {fnInvalid, fnInvalid} } },     // ethernetLinkUp 0
	{ { {fnDoNothing, fnInvalid}, {fnDoNothing, fnInvalid} }, { {fnWirStNewAtt, fnWirStNewAtt}, {fnWirStNewAtt, fnInvalid} } } },   // ethernetLinkUp 1

//	NETEVT_WIRED_DHCP_REFRESH,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1              wiredIntCon 0               wiredIntCon 1
  { { { {fnDoNothing, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnDoNothing, fnDoNothing}, {fnInvalid, fnInvalid} } },     // ethernetLinkUp 0
	{ { {fnDoNothing, fnInvalid}, {fnDoNothing, fnInvalid} }, { {fnWirDHCP, fnDoNothing}, {fnWirDHCPRfsh, fnInvalid} } } },   // ethernetLinkUp 1

//	NETEVT_WIRED_DHCP_TO_STATIC,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1              wiredIntCon 0               wiredIntCon 1
  { { { {fnDoNothing, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnDoNothing, fnDoNothing}, {fnInvalid, fnInvalid} } },     // ethernetLinkUp 0
	{ { {fnDoNothing, fnInvalid}, {fnDoNothing, fnInvalid} }, { {fnWirSt, fnDoNothing}, {fnWirDHCP2St, fnInvalid} } } },   // ethernetLinkUp 1

//	NETEVT_WIRED_STATIC_TO_DHCP,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1              wiredIntCon 0               wiredIntCon 1
  { { { {fnDoNothing, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnDoNothing, fnDoNothing}, {fnInvalid, fnInvalid} } },     // ethernetLinkUp 0
	{ { {fnDoNothing, fnInvalid}, {fnDoNothing, fnInvalid} }, { {fnWirSt2DHCP, fnWirSt2DHCP}, {fnWirSt2DHCP, fnInvalid} } } },   // ethernetLinkUp 1

//	NETEVT_WIRED_DHCP_TO_WIFI,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1              wiredIntCon 0               wiredIntCon 1
  { { { {fnDoNothing, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnWirCnf2Wif, fnDoNothing}, {fnWirCnf2Wif, fnInvalid} } },     // ethernetLinkUp 0
    { { {fnDoNothing, fnInvalid}, {fnDoNothing, fnInvalid} }, { {fnWirCnf2Wif, fnDoNothing}, {fnWirDHCP2Wif, fnInvalid} } } },   // ethernetLinkUp 1

//	NETEVT_WIRED_STATIC_TO_WIFI,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1              wiredIntCon 0               wiredIntCon 1
  { { { {fnDoNothing, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnWirCnf2Wif, fnDoNothing}, {fnWirCnf2Wif, fnInvalid} } },     // ethernetLinkUp 0
	{ { {fnDoNothing, fnInvalid}, {fnDoNothing, fnInvalid} }, { {fnWirCnf2Wif, fnDoNothing}, {fnWirSt2Wif, fnInvalid} } } },   // ethernetLinkUp 1

//	NETEVT_WIRED_DHCP_ADDR_SUCCESS,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1               wiredIntCon 0               wiredIntCon 1
  { { { {fnDoNothing, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnDoNothing, fnDoNothing}, {fnInvalid, fnInvalid} } },     // ethernetLinkUp 0
	{ { {fnDoNothing, fnInvalid}, {fnDoNothing, fnInvalid} }, { {fnDHCPAddrOK, fnInvalid},  {fnDHCPAddrOKDNS, fnInvalid} } } },   // ethernetLinkUp 1

//	NETEVT_WIRED_DHCP_ADDR_FAIL,
                         // tabletLinkUp 0                                       // tabletLinkUp 1
           // wiredIntCon 0               wiredIntCon 1               wiredIntCon 0               wiredIntCon 1
  { { { {fnDoNothing, fnInvalid}, {fnInvalid, fnInvalid} }, { {fnDoNothing, fnDoNothing}, {fnInvalid, fnInvalid} } },     // ethernetLinkUp 0
	{ { {fnDoNothing, fnInvalid}, {fnDoNothing, fnInvalid} }, { {fnDHCPAddrFail, fnInvalid}, {fnDoNothing, fnInvalid} } } }    // ethernetLinkUp 1
};


/* -------------------------------------------------------------------------------------------------[fnGetLanNames]-- */
unsigned char fnGetLANNames(char *LanNames, size_t *LanNamesSize)
{
    /* Interface Name Index  */
    struct if_nameindex *NI;
    
    /* will use this to free Name Index */
    struct if_nameindex *NI_tmp;

    size_t wr_idx = 0;              /* where in LanNames to write to next */
    char *sep = ",";
    size_t sep_len = 0;             /* the first item doesn't get a seperator */
    
    if (LanNames == NULL || LanNamesSize == NULL)
        return 0xFF;


    NI = NU_IF_NameIndex();    
    NI_tmp = NI;
    
    while (NI->if_name != NU_NULL) 
    {
        if ((strcmp(NI->if_name, "loopback") != 0))
        {
            size_t name_len = strlen(NI->if_name);

            if (wr_idx + name_len + sep_len < *LanNamesSize)
            {
                memcpy(&LanNames[wr_idx], sep, sep_len);
                wr_idx += sep_len;
                memcpy(&LanNames[wr_idx], NI->if_name, name_len);
                wr_idx += name_len;

                sep_len = 1; /* from now on, each interface name will get a comma prefixed */
            }
            else
            {
                /* not enough space in LanNames */
                return 0xFF;
            }
        }

        /* next interface */
        NI = (struct if_nameindex *)((CHAR *)NI +
                sizeof(struct if_nameindex) + DEV_NAME_LENGTH);
        
    }
    
    /* Free name index */
    NU_IF_FreeNameIndex(NI_tmp);

    LanNames[wr_idx] = 0;  /* Make sure it is null terminated */
    *LanNamesSize = wr_idx;
    return 0;
}
/* Wrapper around Nucleus internal function to get interface configuration */
STATUS fnGetIFConfig(const CHAR *name, IFCONFIG_NODE **ifconfig_ptr)
{
	STATUS status = NU_SUCCESS;

    // Note: Ifconfig_Find_Interface is an internal Nucleus function so requires using the same internal
    // semaphore used by Nucleus.

	/* Obtain semaphore. */
    status = NU_Obtain_Semaphore(&Ifconfig_Semaphore, NU_SUSPEND);

    if (status == NU_SUCCESS)
    {
        status = Ifconfig_Find_Interface(name, ifconfig_ptr);
        NU_Release_Semaphore(&Ifconfig_Semaphore);
    }

	return status;
}


/* ------------------------------------------------------------------------------------------------[fnGetLANConfig]-- */
STATUS fnGetLANConfig(char *LanName, LanInfo *pLanInfo)
{
    NU_IOCTL_OPTION ioctl_opt;
    STATUS          status = -1;
    UINT32		uState = 0;

    memset(pLanInfo, 0, sizeof(LanInfo));
    pLanInfo->if_up = pLanInfo->if6_up = false;
    if (LanName == NULL || pLanInfo == NULL)
        return 0xFF;

    NU_Test_Link_Up(LanName, &uState);
    pLanInfo->if_up = uState ;

    /* Find the IP address attached to the network device. */    memset(&ioctl_opt, 0, sizeof(NU_IOCTL_OPTION));
    ioctl_opt.s_optval = (UINT8*)LanName;
    status = NU_Ioctl(SIOCGIFADDR, &ioctl_opt, sizeof(ioctl_opt));
    if (status == NU_SUCCESS)
    {
        /* Got an UP interface */
        memcpy(pLanInfo->ip_addr, ioctl_opt.s_ret.s_ipaddr, sizeof(pLanInfo->ip_addr));
        //pLanInfo->if_up = true;

        /* find the netmask if address is valid */
        status = NU_Ioctl(SIOCGIFNETMASK, &ioctl_opt, sizeof(ioctl_opt));
        if (status == NU_SUCCESS)
        {
            memcpy(pLanInfo->netmask, ioctl_opt.s_ret.s_ipaddr, sizeof(pLanInfo->netmask));
        }
        else
        {
            memset(pLanInfo->netmask, 0, sizeof(pLanInfo->netmask));
        }
    }

#if (INCLUDE_IPV6 == 1)    memset(&ioctl_opt, 0, sizeof(NU_IOCTL_OPTION));
    ioctl_opt.s_optval = (UINT8*)LanName;
    if((status = NU_Ioctl(SIOCGIFADDR_IN6, &ioctl_opt, sizeof(ioctl_opt))) == NU_SUCCESS) // try IPv6
    {
        // get the link address also.
        UINT32 dev_index = NU_IF_NameToIndex(LanName);
        if(status == NU_SUCCESS)
        {
            status = NU_Get_Link_Local_Addr(dev_index, (UINT8 *)pLanInfo->link6_addr);
        }
        memcpy(pLanInfo->ipv6_addr, ioctl_opt.s_ret.s_ipaddr, MAX_ADDRESS_SIZE);
        pLanInfo->if6_up = true;
    }
#endif
    /* find the mac address */
    memset(&ioctl_opt, 0, sizeof(NU_IOCTL_OPTION));
    ioctl_opt.s_optval = (UINT8*)LanName;
    status = NU_Ioctl(SIOCGIFHWADDR, &ioctl_opt, sizeof(ioctl_opt));
    if (status == NU_SUCCESS)
    {
        memcpy(pLanInfo->mac_addr, ioctl_opt.s_ret.mac_address, sizeof(pLanInfo->mac_addr));
    }
    else
    {
        memset(pLanInfo->mac_addr, 0, sizeof(pLanInfo->mac_addr));
    }
    
    return status;
}


/* ------------------------------------------------------------------------------------------------[getAllLANConfigsAsJSONObject]-- */
cJSON *getAllLANConfigsAsJSONArray()
{
	cJSON *pArr		=  cJSON_CreateArray();
	if(pArr)
	{
		cJSON_AddItemToArray(pArr, getLANConfigAsJSONObject("eth0"));
		cJSON_AddItemToArray(pArr, getLANConfigAsJSONObject("net0"));
		cJSON_AddItemToArray(pArr, getLANConfigAsJSONObject("net1"));
	}
	return pArr ;
}

/* ------------------------------------------------------------------------------------------------[getLANConfigAsJSONObject]-- */
#define	JSON_STRINGLENGTH		50
cJSON *getLANConfigAsJSONObject(char *if_name)
{
	cJSON *rspMsg		=  cJSON_CreateObject();
	char	str[JSON_STRINGLENGTH];
	char	strTmp[50];

	if(rspMsg)
	{
		LanInfo lan_info;

		STATUS status = fnGetLANConfig(if_name, &lan_info);
		if(status == NU_SUCCESS)
		{
			cJSON_AddStringToObject(rspMsg, WS_KEY_LAN_STATE, lan_info.if_up ? "true" : "false");
			cJSON_AddStringToObject(rspMsg, WS_KEY_LAN_NAME, if_name);

			// use the expected state rather than the actual state since the interface has trouble supplying this if it is no up
//			cJSON_AddStringToObject(rspMsg, WS_KEY_DHCP, NU_Ifconfig_Get_DHCP4_Enabled(if_name) == 1 ? "Enabled" : "Disabled");
			cJSON_AddStringToObject(rspMsg, WS_KEY_DHCP, CMOSglobalNetworkInformationStructure.dhcpFlag == NIS_DHCP_ACTIVE ? "Enabled" : "Disabled");

			snprintf(str, JSON_STRINGLENGTH,"%s", IP4ArrayToString(lan_info.ip_addr, strTmp, 50, FALSE));
			cJSON_AddStringToObject(rspMsg, WS_KEY_LAN_IP, str);

			if(lan_info.if6_up)
			{
				snprintf(str, JSON_STRINGLENGTH,"%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X",
						  lan_info.ipv6_addr[0],lan_info.ipv6_addr[1],
						  lan_info.ipv6_addr[2],lan_info.ipv6_addr[3],
						  lan_info.ipv6_addr[4],lan_info.ipv6_addr[5],
						  lan_info.ipv6_addr[6],lan_info.ipv6_addr[7],
						  lan_info.ipv6_addr[8],lan_info.ipv6_addr[9],
						  lan_info.ipv6_addr[10],lan_info.ipv6_addr[11],
						  lan_info.ipv6_addr[12],lan_info.ipv6_addr[13],
						  lan_info.ipv6_addr[14],lan_info.ipv6_addr[15]);

				cJSON_AddStringToObject(rspMsg, WS_KEY_LAN_IPV6, str);

				snprintf(str, JSON_STRINGLENGTH,"%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X",
						  lan_info.link6_addr[0],lan_info.link6_addr[1],
						  lan_info.link6_addr[2],lan_info.link6_addr[3],
						  lan_info.link6_addr[4],lan_info.link6_addr[5],
						  lan_info.link6_addr[6],lan_info.link6_addr[7],
						  lan_info.link6_addr[8],lan_info.link6_addr[9],
						  lan_info.link6_addr[10],lan_info.link6_addr[11],
						  lan_info.link6_addr[12],lan_info.link6_addr[13],
						  lan_info.link6_addr[14],lan_info.link6_addr[15]);

				cJSON_AddStringToObject(rspMsg, WS_KEY_LAN_IPV6LINK, str);
			}

			UINT8 dns[IP4_BYTESIZE * MAX_IP4DNS_COUNT];
			INT iDNSCount = 0 ;
			if((iDNSCount = NU_Get_DNS_Servers(dns, IP4_BYTESIZE * MAX_IP4DNS_COUNT)) > 0)
			{
				INT iPass;
				char strKey[11];
				for(iPass = 0 ; iPass < iDNSCount ; iPass++)
				{
					snprintf(str, JSON_STRINGLENGTH, "%s", IP4ArrayToString(&dns[iPass*IP4_BYTESIZE], strTmp, 50, TRUE));
					snprintf(strKey, 10, "DNS%d", iPass);
					cJSON_AddStringToObject(rspMsg, strKey, str);
				}
			}

			UINT32 u32Adds = 0;
		    status = NU_Obtain_Semaphore(&TCP_Resource, NU_SUSPEND);
		    if (status == NU_SUCCESS)
		    {
				if(RTAB4_Default_Route != NULL)
			    {
					u32Adds = IP_ADDR(NUCLEUS_RTAB4_ROUTE4) ;
			    }
				NU_Release_Semaphore(&TCP_Resource);
				snprintf(str, JSON_STRINGLENGTH, "%s", IP4ArrayToString((UINT8 *)&u32Adds, strTmp, 50, FALSE));
				cJSON_AddStringToObject(rspMsg, WS_KEY_LAN_RTABGATEWAY, str);
		    }
		    else
		    {
				cJSON_AddStringToObject(rspMsg, WS_KEY_LAN_RTABGATEWAY, "0.0.0.0!");
		    }

			if(*(UINT32 *)globalDefaultIPv4Gateway != 0)
			{
				snprintf(str, JSON_STRINGLENGTH, "%s", IP4ArrayToString(globalDefaultIPv4Gateway, strTmp, 50, FALSE));
				cJSON_AddStringToObject(rspMsg, WS_KEY_LAN_GATEWAY, str);
			}


			snprintf(str, JSON_STRINGLENGTH,"%s", IP4ArrayToString(lan_info.netmask, strTmp, 50, FALSE));
			cJSON_AddStringToObject(rspMsg, WS_KEY_LAN_NETMASK, str);


			snprintf(str, JSON_STRINGLENGTH,"%02X:%02X:%02X:%02X:%02X:%02X",
					lan_info.mac_addr[0]
					, lan_info.mac_addr[1]
					, lan_info.mac_addr[2]
					, lan_info.mac_addr[3]
					, lan_info.mac_addr[4]
					, lan_info.mac_addr[5]);
			cJSON_AddStringToObject(rspMsg, WS_KEY_LAN_MACADDRESS, str);
		}
	}
    return rspMsg ;
}


/* ----------------------------------------------------------------------------------------------[print_if_chg_msg]-- */
static void print_if_chg_msg(NET_IF_CHANGE_MSG *msg)
{
	dbgTrace(DBG_LVL_INFO, "  dev_id     = %ld\r\n"
           "  dev_hd     = %ld\r\n"
           "  dev_name   = %s\r\n"
           "  link_state = %d\r\n",
           msg->dev_id,
           msg->dev_hd,
           msg->dev_name,
           msg->link_state);
           
}

/* ------------------------------------------------------------------------------------------------[ip_addr_to_str]-- */

/* for just an ipv4 address the max ip address as a string is 4 x 3 digits, 3 x dot + null = 16 */
/* but for an ipv6 address it is 16 x 2 hexchars + 8 x colon + null = 41                        */
/* IP_ADDR_STR_LEN is defined in networkmonitor.h                                               */

char *ip_addr_to_str(UINT8 *ip_addr, UINT16 family, UINT16 v4Mapped, char *str)
{

#if(INCLUDE_IPV6 == 1)
	if(family == NU_FAMILY_IP6)
	{
		BOOL nullAddress = TRUE ;
		int iPass;
		for(iPass=0 ; (iPass < MAX_ADDRESS_SIZE) && nullAddress ; iPass++)
			nullAddress = (ip_addr[iPass] == 0) ;

		if(nullAddress)
			sprintf(str, " ALL INTERFACES ");
		else
		{
			sprintf(str, "%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:",
					ip_addr[0],	ip_addr[1],
					ip_addr[2],	ip_addr[3],
					ip_addr[4], ip_addr[5],
					ip_addr[6], ip_addr[7],
					ip_addr[8], ip_addr[9],
					ip_addr[10], ip_addr[11],
					ip_addr[12], ip_addr[13],
					ip_addr[14], ip_addr[15]);
		}
	}
	else
#endif
	{
		// if NU claims this is a V4 mapped address then we need the end of the 16 byte buffer (?)
        UINT8 *adds = (SF_V4_MAPPED == v4Mapped) ? ip_addr + 12 : ip_addr;
        UINT32 aligned_ipv4_addr;  /* to prevent alignment issues we copy the address into a UINT32 */
        memcpy(&aligned_ipv4_addr, adds, sizeof(UINT32)); 
		if(aligned_ipv4_addr != 0)
		{
			sprintf(str, "%d.%d.%d.%d", adds[0], adds[1], adds[2], adds[3]);
		}
		else
			sprintf(str, " ALL INTERFACES ");
	}

    return str;
}


/* ------------------------------------------------------------------------------------------------[ip_port_to_str]-- */
static char localPortText[20];
char *ip_port_to_str(UINT port)
{
	char *str = localPortText ;
	sprintf(str, "%d", port) ;

	size_t sz = strlen(str) ;
	char *ptr = str+sz; ;


	for( ; sz < 5 ; sz++, ptr++)
		*ptr = ' ';
	*ptr =  0 ;

	switch(port)
	{
		case 20:
			//No Break
		case 21:
			strcat(str, "FTP   ");
			break ;

		case 22:
			strcat(str,"SSH   ");
			break ;

		case 67:
			//No Break
		case 68:
			strcat(str, "DHCP  ");
			break ;

		case 80:
			strcat(str, "HTTP  ");
			break ;

		case 443:
		case 8443:
			strcat(str, "HTTPS ");
			break ;

		case 505:
			strcat(str, "DEBUG ");
			break ;

		case 8085:
			strcat(str, "EDM   ");
			break ;

		case 6996:
			strcat(str, "DAEMON");
			break ;

		default:
			strcat(str, "      ") ;
			break ;
	}

	return str;
}

/* ---------------------------------------------------------------------------------------------[store_our_ip_addr]-- */
char    local_if_name[20];
uint8_t local_ip_addr[4];

void store_our_ip_addr(char *if_name, uint8_t *ip_addr)
{
    strcpy(local_if_name, if_name);
    memcpy(local_ip_addr, ip_addr, 4);  /* for now only handle ipv4 addresses */
}

/* -------------------------------------------------------------------------------------[get_our_ip_addr_as_string]-- */
char *get_our_ip_addr_as_string(char *ip_addr_str)
{
    return ip_addr_to_str(local_ip_addr, NU_FAMILY_IP, IP_NOT_MAPPED, ip_addr_str);
}

/* -----------------------------------------------------------------------------------------[print_dhcp_if_chg_msg]-- */
static void print_dhcp_if_chg_msg(DHCP_IF_CHANGE_MSG *msg)
{
    char ip_addr_str[IP_ADDR_STR_LEN];
    char net_mask_str[IP_ADDR_STR_LEN];
    char gw_addr_str[IP_ADDR_STR_LEN];

    dbgTrace(DBG_LVL_INFO, "  dev_index     = %ld\r\n"
           "  ip_addr       = %s\r\n"
           "  net_mask      = %s\r\n"
           "  gw_addr       = %s\r\n",
           msg->dev_index,
           ip_addr_to_str(msg->ip_addr,  NU_FAMILY_IP, IP_NOT_MAPPED, ip_addr_str),
           ip_addr_to_str(msg->net_mask, NU_FAMILY_IP, IP_NOT_MAPPED, net_mask_str),
           ip_addr_to_str(msg->gw_addr,  NU_FAMILY_IP, IP_NOT_MAPPED, gw_addr_str)    );
}

/* ------------------------------------------------------------------------------------------[print_available_urls]-- */
static void print_available_urls()
{
    char                   lan_names[100];
    char                  *lan_name;
    size_t                 lan_names_size = sizeof(lan_names);
    unsigned char          res;
    LanInfo                lan_info;
    char                   ip_addr_str[IP_ADDR_STR_LEN];
    char                   *pRest = NULL;

    res = fnGetLANNames(lan_names, &lan_names_size);
    //dbgTrace(DBG_LVL_INFO, "\r\n\n GetLANNames: result = %d, lan_names=[%s]\r\n", res, lan_names);

    lan_name = strtok_r(lan_names, ",", &pRest);
    while (lan_name != NULL)
    {
        res = fnGetLANConfig(lan_name, &lan_info);
        if (res == NU_SUCCESS && lan_info.if_up)
        {
        	dbgTrace(DBG_LVL_INFO, "  interface %s : ws://%s/ws_csd\r\n", lan_name, ip_addr_to_str(lan_info.ip_addr, NU_FAMILY_IP, IP_NOT_MAPPED, ip_addr_str));
            store_our_ip_addr(lan_name, lan_info.ip_addr);
        }
        lan_name = strtok_r(NULL, ",", &pRest);
    }
}

// Log all relevant fields
void logNetworkConfig(NetworkInformationStruct *config)
{
	char	strTmp[50];
	int		i;

	dbgTrace(DBG_LVL_INFO, "Network Source: %s", (config->sourceFlag == NIS_SOURCE_WIRED) ? "Wired" : "WiFi");
	if (config->sourceFlag == NIS_SOURCE_WIRED)
	{
		dbgTrace(DBG_LVL_INFO, "DHCP: %s", (config->dhcpFlag == NIS_DHCP_OFF) ? "Off" : "On");
		if (config->dhcpFlag == NIS_DHCP_OFF)
		{
			dbgTrace(DBG_LVL_INFO, "IP Address: %s", IP4ArrayToString((UINT8 *)config->ipv4Address.ip4u8, strTmp, 50, FALSE));
			dbgTrace(DBG_LVL_INFO, "Netmask: %s", IP4ArrayToString((UINT8 *)config->ipv4Netmask.ip4u8, strTmp, 50, FALSE));
			dbgTrace(DBG_LVL_INFO, "Gateway: %s", IP4ArrayToString((UINT8 *)config->ipv4Gateway.ip4u8, strTmp, 50, FALSE));
			for (i = 0; i < DEVICE_DNS_ARRAY_MAX; i++)
				dbgTrace(DBG_LVL_INFO, "DNS: %s", IP4ArrayToString((UINT8 *)config->ipv4DNS[i].ip4u8, strTmp, 50, FALSE));
		}
	}
	else
	{
		dbgTrace(DBG_LVL_INFO, "Gateway: %s", IP4ArrayToString((UINT8 *)config->ipv4Gateway.ip4u8, strTmp, 50, FALSE));
		for (i = 0; i < DEVICE_DNS_ARRAY_MAX; i++)
			dbgTrace(DBG_LVL_INFO, "DNS: %s", IP4ArrayToString((UINT8 *)config->ipv4DNS[i].ip4u8, strTmp, 50, FALSE));
	}

}

// Init networking state variables
static void initNetworkingState(void)
{
	// Initialize configuration to default values if not already initialized
	if(!CMOSglobalNetworkInformationStructure.initialized)
	{
		dbgTrace(DBG_LVL_INFO, "Setting eth0 initial state - dhcp\r\n");
		// initialize eth0 to a known state...
		// Currently this seems to be dhcp enabled no ipv6.
		CMOSglobalNetworkInformationStructure.initialized	= NIS_CMOS_INITIALIZED;
		CMOSglobalNetworkInformationStructure.dhcpFlag		= NIS_DHCP_ACTIVE;
		CMOSglobalNetworkInformationStructure.ipv6Flag		= NIS_IPV6_OFF;
		CMOSglobalNetworkInformationStructure.sourceFlag 	= NIS_SOURCE_WIRED;
        CMOSglobalNetworkInformationStructure.eth0WsoxFlag	= NIS_ETH0_WSOX_OFF;
	}

	// log config at startup
	dbgTrace(DBG_LVL_INFO, "Initial Network Configuration Settings");
	logNetworkConfig(&CMOSglobalNetworkInformationStructure);

	ethernetLinkUp = FALSE;
	tabletLinkUp = FALSE;
	wiredInternetConnected = FALSE;
	wifiInternetConnected = FALSE;
}

// Return a networking event based upon change in configuration
static NETWORK_EVENT getNetworkEventFromConfigChange(NetworkInformationStruct *currentConfig, NetworkInformationStruct *newConfig)
{
	NETWORK_EVENT event = NETEVT_NOCHANGE;

	if (currentConfig->sourceFlag == NIS_SOURCE_WIFI)
	{// NETEVT_WIFI_*

		if (newConfig->sourceFlag == NIS_SOURCE_WIFI)
		{
			event = NETEVT_WIFI_NEW_ATTRIB;
		}
		else if (newConfig->sourceFlag == NIS_SOURCE_WIRED)
		{//NETEVT_WIFI_TO_WIRED_*

			if (newConfig->dhcpFlag == NIS_DHCP_OFF)
			{
				event = NETEVT_WIFI_TO_WIRED_STATIC;
			}
			else
			{
				event = NETEVT_WIFI_TO_WIRED_DHCP;
			}
		}
	} else if (currentConfig->sourceFlag == NIS_SOURCE_WIRED)
	{// NETEVT_WIRED_*

		if (currentConfig->dhcpFlag == NIS_DHCP_OFF)
		{// NETEVT_WIRED_STATIC_*

			if (newConfig->sourceFlag == NIS_SOURCE_WIFI)
			{
				event = NETEVT_WIRED_STATIC_TO_WIFI;
			}
			else if (newConfig->sourceFlag == NIS_SOURCE_WIRED)
			{
				if (newConfig->dhcpFlag == NIS_DHCP_OFF)
				{
					event = NETEVT_WIRED_STATIC_NEW_ATTRIB;
				}
				else
				{
					event = NETEVT_WIRED_STATIC_TO_DHCP;
				}
			}
		} else
		{// NETEVT_WIRED_DHCP_*

			if (newConfig->sourceFlag == NIS_SOURCE_WIFI)
			{
				event = NETEVT_WIRED_DHCP_TO_WIFI;
			}
			else if (newConfig->sourceFlag == NIS_SOURCE_WIRED)
			{
				if (newConfig->dhcpFlag == NIS_DHCP_OFF)
				{
					event = NETEVT_WIRED_DHCP_TO_STATIC;
				}
				else
				{
					event = NETEVT_WIRED_DHCP_REFRESH;
				}
			}
		}
	}

	return event;
}

// Networking event process functions - START

static BOOL fnDoNothing(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;

	dbgTrace(DBG_LVL_INFO, "Networking function fnDoNothing: event %d", event);

	return retVal;
}

static BOOL fnInvalid(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;

	dbgTrace(DBG_LVL_ERROR, "Error: Networking function fnInvalid: event %d", event);

	return retVal;
}

// Got DHCP Address - apply it
static BOOL fnDHCPAddrOK(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;

	status = setDefaultGateway(globalDefaultIPv4Gateway);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "Set Default Gateway Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnDHCPAddrOK Failed");
		return FAILURE;
	}

	status = setNAT(TRUE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Enable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnDHCPAddrOK Failed");
		return FAILURE;
	}

	status = setDHCPServerDNSAddresses();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "Set DHCP DNS Address Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnDHCPAddrOK Failed");
		return FAILURE;
	}

	//update state variables
	wiredInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnDHCPAddrOK Finished");
	return retVal;
}

// Got DHCP Address message, already connected - just set DHCP Server DNS addresses
static BOOL fnDHCPAddrOKDNS(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;

	status = setDHCPServerDNSAddresses();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "Set DHCP DNS Address Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnDHCPAddrOKDNS Failed");
		return FAILURE;
	}

	dbgTrace(DBG_LVL_INFO, "Networking function fnDHCPAddrOKDNS Finished");
	return retVal;
}

// Failed to get DHCP Address - log and return failure
static BOOL fnDHCPAddrFail(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = FAILURE;

	// Should never get here
	strncpy(errorBuf, "DHCP Get Address Failure", bufSize);
	dbgTrace(DBG_LVL_ERROR, "Error DHCP address failure");

	dbgTrace(DBG_LVL_INFO, "Networking function fnDHCPAddrFail Finished");

	return retVal;
}

// Tablet link is up - if configured for Wifi, turn it on
static BOOL fnTabLinkUp(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;

	//update state variables
	tabletLinkUp = TRUE;

	//If configured for Wifi, turn it on
	if (CMOSglobalNetworkInformationStructure.sourceFlag == NIS_SOURCE_WIFI)
	{
		status = setWifi();
		if (status != NU_SUCCESS)
		{
			strncpy(errorBuf, "WiFi Setup Failure", bufSize);
			dbgTrace(DBG_LVL_ERROR, "Error: fnTabLinkUp failed to set Wifi settings: %d", status);
			return FAILURE;
		}

		//update state variables
		wifiInternetConnected = TRUE;
	}

	dbgTrace(DBG_LVL_INFO, "Networking function fnTabLinkUp Finished");
	return retVal;
}

// Tablet link is up and already connected to Internet via Eth - try to start NAT
static BOOL fnTabLnUpNAT(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;

	//update state variables
	tabletLinkUp = TRUE;
	wiredInternetConnected = FALSE; // not considered connected until NAT is up also

	status = setNAT(TRUE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Enable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnTabLnUpNAT Failed");
		return FAILURE;
	}

	//update state variables
	wiredInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnTabLnUpNAT Finished");
	return retVal;
}

// Tablet link is up, Eth already up - connect to Internet.
static BOOL fnTabLnUpInt(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;

	//update state variables
	tabletLinkUp = TRUE;

	// apply config based on Static IP or DHCP
	if (CMOSglobalNetworkInformationStructure.dhcpFlag == NIS_DHCP_OFF)
	{// static IP

		status = setStaticIP(config);
		if (status != NU_SUCCESS)
		{
			strncpy(errorBuf, "Static IP Setup Failure", bufSize);
			dbgTrace(DBG_LVL_ERROR, "Networking function fnTabLnUpInt:setStaticIP Failed");
			return FAILURE;
		}
	}
	else
	{// DHCP
		status = startDHCP();
		if (status != NU_SUCCESS)
		{
			strncpy(errorBuf, "DHCP Get Address Failure", bufSize);
			dbgTrace(DBG_LVL_ERROR, "Networking function fnTabLnUpInt:startDHCP Failed");
			return FAILURE;
		}
	}

	status = setNAT(TRUE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Enable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnTabLnUpInt Failed");
		return FAILURE;
	}

	//update state variables
	wiredInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnTabLnUpInt Finished");
	return retVal;
}

// Tablet link is down - nothing to do
static BOOL fnTabLinkDn(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;

	//update state variables
	tabletLinkUp = FALSE;
	wifiInternetConnected = FALSE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnTabLinkDn Finished");
	return retVal;
}

// Tablet link is down while Wired - turn off NAT
static BOOL fnTabLnDnNAT(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;

	//update state variables
	tabletLinkUp = FALSE;
	wifiInternetConnected = FALSE;

	status = setNAT(FALSE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Disable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Error Networking function fnTabLnDnNAT Failed");
		return FAILURE;
	}

	dbgTrace(DBG_LVL_INFO, "Networking function fnTabLnDnNAT Finished");
	return retVal;
}

// Tablet link is down while on Wifi - clear DNS entries
static BOOL fnTabLnDnDNS(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;

	//update state variables
	tabletLinkUp = FALSE;
	wifiInternetConnected = FALSE;

	//not removing default gateway because it should not change for Wifi
	status = deleteAllBaseDNSEntries();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "DNS Deletion Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnTabLinkDnDNS Failed");
		return FAILURE;
	}

	dbgTrace(DBG_LVL_INFO, "Networking function fnTabLinkDnDNS Finished");
	return retVal;
}

// Ethernet link is up but tablet not up - update state
static BOOL fnEthLinkUp(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;

	//update state variables
	ethernetLinkUp = TRUE;

	//If configured for Wifi, generate error
	if (CMOSglobalNetworkInformationStructure.sourceFlag == NIS_SOURCE_WIFI)
	{
		strncpy(errorBuf, "Ethernet Up in WiFi Mode", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Error: Ethernet link up but configured for Wifi");
		return FAILURE;
	}

	dbgTrace(DBG_LVL_INFO, "Networking function fnEthLinkUp Finished");
	return retVal;
}

// Ethernet link is up & tablet link is up & no Internet - update state, apply config, set NAT, set Internet
static BOOL fnEthLnUpTab(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;

	//update state variables
	ethernetLinkUp = TRUE;

	//If configured for Wifi, generate error
	if (CMOSglobalNetworkInformationStructure.sourceFlag == NIS_SOURCE_WIFI)
	{
		strncpy(errorBuf, "Ethernet Up in WiFi Mode", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Error: Ethernet link up but configured for Wifi");
		return FAILURE;
	}

	// apply config based on Static IP or DHCP
	if (CMOSglobalNetworkInformationStructure.dhcpFlag == NIS_DHCP_OFF)
	{// static IP

		status = setStaticIP(config);
		if (status != NU_SUCCESS)
		{
			strncpy(errorBuf, "Static IP Setup Failure", bufSize);
			dbgTrace(DBG_LVL_ERROR, "Networking function fnEthLnUpTab:setStaticIP Failed");
			return FAILURE;
		}
	}
	else
	{// DHCP
		status = startDHCP();
		if (status != NU_SUCCESS)
		{
			strncpy(errorBuf, "DHCP Get Address Failure", bufSize);
			dbgTrace(DBG_LVL_ERROR, "Networking function fnEthLnUpTab:startDHCP Failed");
			return FAILURE;
		}
	}

	status = setNAT(TRUE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Enable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnEthLnUpTab:setNAT Failed");
		return FAILURE;
	}

	//update state variables
	wiredInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnEthLnUpTab Finished");
	return retVal;
}

// Ethernet link is up & tablet link is up & Wifi - update state, check config
static BOOL fnEthLnUpTbWf(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;

	//update state variables
	ethernetLinkUp = TRUE;

	//If configured for Wire, generate error
	if (CMOSglobalNetworkInformationStructure.sourceFlag == NIS_SOURCE_WIRED)
	{
		strncpy(errorBuf, "Ethernet Up in WiFi Mode", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Error: Ethernet link up but using Wifi Internet connection");
		return FAILURE;
	}

	dbgTrace(DBG_LVL_INFO, "Networking function fnEthLnUpTbWf Finished");
	return retVal;
}

// Ethernet link is down - update state, clean up
static BOOL fnEthLinkDn(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;

	//update state variables
	ethernetLinkUp = FALSE;
	wiredInternetConnected = FALSE;

	// clear Static IP or DHCP
	if (CMOSglobalNetworkInformationStructure.dhcpFlag == NIS_DHCP_OFF)
	{// static IP

		status = clearStaticIP();
		if (status != NU_SUCCESS)
		{
			strncpy(errorBuf, "Static IP Teardown Failure", bufSize);
			dbgTrace(DBG_LVL_ERROR, "Networking function fnEthLinkDn:clearStaticIP Failed");
			return FAILURE;
		}
	}
	else
	{// DHCP
		status = stopDHCP();
		if (status != NU_SUCCESS)
		{
			strncpy(errorBuf, "DHCP Release Failure", bufSize);
			dbgTrace(DBG_LVL_ERROR, "Networking function fnEthLinkDn:stopDHCP Failed");
			return FAILURE;
		}
	}

	dbgTrace(DBG_LVL_INFO, "Networking function fnEthLinkDn Finished");
	return retVal;

}

// Ethernet link is down - update state, clean up, take down NAT
static BOOL fnEthLnDnNAT(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;

	//update state variables
	ethernetLinkUp = FALSE;
	wiredInternetConnected = FALSE;

	// clear Static IP or DHCP
	if (CMOSglobalNetworkInformationStructure.dhcpFlag == NIS_DHCP_OFF)
	{// static IP

		status = clearStaticIP();
		if (status != NU_SUCCESS)
		{
			strncpy(errorBuf, "Static IP Teardown Failure", bufSize);
			dbgTrace(DBG_LVL_ERROR, "Networking function fnEthLnDnNAT:clearStaticIP Failed");
			return FAILURE;
		}
	}
	else
	{// DHCP
		status = stopDHCP();
		if (status != NU_SUCCESS)
		{
			strncpy(errorBuf, "DHCP Release Failure", bufSize);
			dbgTrace(DBG_LVL_ERROR, "Networking function fnEthLnDnNAT:stopDHCP Failed");
			return FAILURE;
		}
	}

	status = setNAT(FALSE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Disable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnEthLnDnNAT:setNAT(0) Failed");
		return FAILURE;
	}

	dbgTrace(DBG_LVL_INFO, "Networking function fnEthLnDnNAT Finished");
	return retVal;
}

// Ethernet link is down but using Wifi
static BOOL fnEthLnDnWf(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;

	//update state variables
	ethernetLinkUp = FALSE;
	wiredInternetConnected = FALSE;

	//If configured for Wire, generate error
	if (CMOSglobalNetworkInformationStructure.sourceFlag == NIS_SOURCE_WIRED)
	{
		strncpy(errorBuf, "Ethernet Down in WiFi Mode", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Error: Ethernet link down but using Wifi Internet connection");
		return FAILURE;
	}

	dbgTrace(DBG_LVL_INFO, "Networking function fnEthLnDnWf Finished");
	return retVal;

}

// Wifi is up - clean old attributes, apply new attributes
static BOOL fnWifNewAtt(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;
	INT i;

	// verify that config matches state
	if ((CMOSglobalNetworkInformationStructure.sourceFlag != NIS_SOURCE_WIFI) || (config->sourceFlag != NIS_SOURCE_WIFI))
	{
		strncpy(errorBuf, "WiFi Setup in Wired Mode", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWifNewAtt: got new attributes but not configured for WiFi");
		return FAILURE;
	}

	status = clearWifi();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "WiFi Teardown Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Error clearWifi Failed");
		return FAILURE;
	}

	// Apply new values in config
	CMOSglobalNetworkInformationStructure.ipv4Gateway = config->ipv4Gateway;
	for (i = 0; i < DEVICE_DNS_ARRAY_MAX; i++)
		CMOSglobalNetworkInformationStructure.ipv4DNS[i] = config->ipv4DNS[i];

	status = setWifi();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "WiFi Setup Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Error: Failed to set Wifi settings: %d", status);
		return FAILURE;
	}

	dbgTrace(DBG_LVL_INFO, "Networking function fnWifNewAtt Finished");
	return retVal;
}

// Wifi is down - apply new attributes
static BOOL fnWifNewConn(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;
	INT i;

	// verify that config matches state
	if ((CMOSglobalNetworkInformationStructure.sourceFlag != NIS_SOURCE_WIFI) || (config->sourceFlag != NIS_SOURCE_WIFI))
	{
		strncpy(errorBuf, "WiFi Setup in Wired Mode", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWifNewConn: got new attributes but not configured for WiFi");
		return FAILURE;
	}

	// Apply new values in config
	CMOSglobalNetworkInformationStructure.ipv4Gateway = config->ipv4Gateway;
	for (i = 0; i < DEVICE_DNS_ARRAY_MAX; i++)
		CMOSglobalNetworkInformationStructure.ipv4DNS[i] = config->ipv4DNS[i];

	status = setWifi();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "WiFi Setup Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Error: Failed to set Wifi settings: %d", status);
		return FAILURE;
	}

	//update state variables
	wifiInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnWifNewConn Finished");
	return retVal;
}

// Wifi is up - clean old attributes, apply static IP attributes
static BOOL fnWif2WirSt(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;
	INT i;

	//update state variables
	wifiInternetConnected = FALSE;

	// Clear out old wifi values
	status = clearWifi();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "WiFi Teardown Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Error clearWifi Failed");
		return FAILURE;
	}

	status = setStaticIP(config);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "Static IP Setup Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWif2WirSt:setStaticIP Failed");
		return FAILURE;
	}

	// Apply new values in config
	CMOSglobalNetworkInformationStructure.sourceFlag = config->sourceFlag;
	CMOSglobalNetworkInformationStructure.dhcpFlag = config->dhcpFlag;
	CMOSglobalNetworkInformationStructure.ipv4Address = config->ipv4Address;
	CMOSglobalNetworkInformationStructure.ipv4Netmask = config->ipv4Netmask;
	CMOSglobalNetworkInformationStructure.ipv4Gateway = config->ipv4Gateway;
	for (i = 0; i < DEVICE_DNS_ARRAY_MAX; i++)
		CMOSglobalNetworkInformationStructure.ipv4DNS[i] = config->ipv4DNS[i];

	status = setNAT(TRUE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Enable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWif2WirSt:setNAT Failed");
		return FAILURE;
	}

	//update state variables
	wiredInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnWif2WirSt Finished");
	return retVal;
}

// Clear WiFi, store new Wired config
static BOOL fnWifClrSetCnf(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;
	INT i;

	//update state variables
	wifiInternetConnected = FALSE;

	// Clear out old wifi values
	status = clearWifi();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "WiFi Teardown Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Error fnWifClrSetCnf clearWifi Failed");
		return FAILURE;
	}

	// Apply new values in config
	CMOSglobalNetworkInformationStructure.sourceFlag = config->sourceFlag;
	CMOSglobalNetworkInformationStructure.dhcpFlag = config->dhcpFlag;
	if (config->dhcpFlag == NIS_DHCP_OFF)
	{
		CMOSglobalNetworkInformationStructure.ipv4Address = config->ipv4Address;
		CMOSglobalNetworkInformationStructure.ipv4Netmask = config->ipv4Netmask;
		CMOSglobalNetworkInformationStructure.ipv4Gateway = config->ipv4Gateway;
		for (i = 0; i < DEVICE_DNS_ARRAY_MAX; i++)
			CMOSglobalNetworkInformationStructure.ipv4DNS[i] = config->ipv4DNS[i];
	}

	dbgTrace(DBG_LVL_INFO, "Networking function fnWifClrSetCnf Finished");
	return retVal;
}

// Wired static IP is up - clean old attributes, apply WiFi attributes
static BOOL fnWirSt2Wif(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;
	INT i;

	//update state variables
	 wiredInternetConnected = FALSE;

	 status = setNAT(FALSE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Disable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirSt2Wif:setNAT(0) Failed");
		return FAILURE;
	}

	// Clear out old static IP values
	status = clearStaticIP();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "Static IP Teardown Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirSt2Wif:clearStaticIP Failed");
		return FAILURE;
	}

	// Apply new values in config
	CMOSglobalNetworkInformationStructure.sourceFlag = config->sourceFlag;
	CMOSglobalNetworkInformationStructure.dhcpFlag = config->dhcpFlag;
	CMOSglobalNetworkInformationStructure.ipv4Gateway = config->ipv4Gateway;
	for (i = 0; i < DEVICE_DNS_ARRAY_MAX; i++)
		CMOSglobalNetworkInformationStructure.ipv4DNS[i] = config->ipv4DNS[i];

	status = setWifi();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "WiFi Setup Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirSt2Wif:setWifi Failed");
		return FAILURE;
	}

	//update state variables
	 wifiInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnWif2Wired Finished");
	return retVal;
}

// Wired static IP is up - clean old attributes, apply new attributes
static BOOL fnWirStNewAtt(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;
	INT i;

    //update state variables
    wiredInternetConnected = FALSE;
    wifiInternetConnected = FALSE;

	status = setNAT(FALSE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Disable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirStNewAtt:setNAT(0) Failed");
		return FAILURE;
	}

	// Clear out old static IP values
	status = clearStaticIP();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "Static IP Teardown Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirStNewAtt:clearStaticIP Failed");
		return FAILURE;
	}

	status = setStaticIP(config);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "Static IP Setup Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirStNewAtt:setStaticIP Failed");
		return FAILURE;
	}

	// Apply new values in config
	CMOSglobalNetworkInformationStructure.ipv4Address = config->ipv4Address;
	CMOSglobalNetworkInformationStructure.ipv4Netmask = config->ipv4Netmask;
	CMOSglobalNetworkInformationStructure.ipv4Gateway = config->ipv4Gateway;
	for (i = 0; i < DEVICE_DNS_ARRAY_MAX; i++)
		CMOSglobalNetworkInformationStructure.ipv4DNS[i] = config->ipv4DNS[i];

	status = setNAT(TRUE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Enable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirStNewAtt:setNAT Failed");
		return FAILURE;
	}

    //update state variables
    wiredInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnWirStNewAtt Finished");
	return retVal;
}

// Wifi is up - clean old attributes, apply DHCP
static BOOL fnWif2WirDHCP(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;

	//update state variables
	wifiInternetConnected = FALSE;

	// Clear out old wifi values
	status = clearWifi();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "WiFi Teardown Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Error clearWifi Failed");
		return FAILURE;
	}

	// Apply new values in config
	CMOSglobalNetworkInformationStructure.sourceFlag = config->sourceFlag;
	CMOSglobalNetworkInformationStructure.dhcpFlag = config->dhcpFlag;

	status = startDHCP();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "DHCP Get Address Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWif2WirDHCP:startDHCP Failed");
		return FAILURE;
	}

	status = setNAT(TRUE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Enable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWif2WirDHCP:setNAT Failed");
		return FAILURE;
	}

	//update state variables
	wiredInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnWif2WirDHCP Finished");
	return retVal;
}

// DHCP not up - start it and set config in case it changed
static BOOL fnWirDHCP(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;

	//update state variables
	wiredInternetConnected = FALSE;

	// Apply new values in config
	CMOSglobalNetworkInformationStructure.sourceFlag = config->sourceFlag;
	CMOSglobalNetworkInformationStructure.dhcpFlag = config->dhcpFlag;

	status = startDHCP();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "DHCP Get Address Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirDHCP:startDHCP Failed");
		return FAILURE;
	}

	status = setNAT(TRUE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Enable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirDHCP:setNAT Failed");
		return FAILURE;
	}

	//update state variables
	wiredInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnWirDHCP Finished");
	return retVal;
}


// DHCP is up - refresh it
static BOOL fnWirDHCPRfsh(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;

	status = setNAT(FALSE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Disable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirDHCPRfsh:setNAT(0) Failed");
		return FAILURE;
	}

	status = stopDHCP();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "DHCP Release Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirDHCPRfsh:stopDHCP Failed");
		return FAILURE;
	}

	status = startDHCP();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "DHCP Get Address Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirDHCPRfsh:startDHCP Failed");
		return FAILURE;
	}

	status = setNAT(TRUE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Enable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirDHCPRfsh:setNAT Failed");
		return FAILURE;
	}

	dbgTrace(DBG_LVL_INFO, "Networking function fnWirDHCPRfsh Finished");
	return retVal;
}

// DHCP is up - stop it, apply static IP attributes
static BOOL fnWirDHCP2St(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;
	INT i;

	//update state variables
	wiredInternetConnected = FALSE;

	status = setNAT(FALSE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Disable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirDHCP2St:setNAT(0) Failed");
		return FAILURE;
	}

	status = stopDHCP();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "DHCP Release Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirDHCP2St:stopDHCP Failed");
		return FAILURE;
	}

	status = setStaticIP(config);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "Static IP Setup Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirDHCP2St:setStaticIP Failed");
		return FAILURE;
	}

	// Apply new values in config
	CMOSglobalNetworkInformationStructure.dhcpFlag = config->dhcpFlag;
	CMOSglobalNetworkInformationStructure.ipv4Address = config->ipv4Address;
	CMOSglobalNetworkInformationStructure.ipv4Netmask = config->ipv4Netmask;
	CMOSglobalNetworkInformationStructure.ipv4Gateway = config->ipv4Gateway;
	for (i = 0; i < DEVICE_DNS_ARRAY_MAX; i++)
		CMOSglobalNetworkInformationStructure.ipv4DNS[i] = config->ipv4DNS[i];

	status = setNAT(TRUE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Enable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirDHCP2St:setNAT Failed");
		return FAILURE;
	}

	//update state variables
	wiredInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnWirDHCP2St Finished");
	return retVal;
}

// No wired Internet - apply static IP attributes
static BOOL fnWirSt(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;
	INT i;

	status = setStaticIP(config);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "Static IP Setup Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirSt:setStaticIP Failed");
		return FAILURE;
	}

	// Apply new values in config
	CMOSglobalNetworkInformationStructure.dhcpFlag = config->dhcpFlag;
	CMOSglobalNetworkInformationStructure.ipv4Address = config->ipv4Address;
	CMOSglobalNetworkInformationStructure.ipv4Netmask = config->ipv4Netmask;
	CMOSglobalNetworkInformationStructure.ipv4Gateway = config->ipv4Gateway;
	for (i = 0; i < DEVICE_DNS_ARRAY_MAX; i++)
		CMOSglobalNetworkInformationStructure.ipv4DNS[i] = config->ipv4DNS[i];

	status = setNAT(TRUE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Enable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirSt:setNAT Failed");
		return FAILURE;
	}

	//update state variables
	wiredInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnWirSt Finished");
	return retVal;
}

// static IP is up - stop it, start DHCP
static BOOL fnWirSt2DHCP(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;

	//update state variables
	wiredInternetConnected = FALSE;
	wifiInternetConnected = FALSE;

	status = setNAT(FALSE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Disable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirSt2DHCP:setNAT(0) Failed");
		return FAILURE;
	}

	status = clearStaticIP();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "Static IP Teardown Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirSt2DHCP:clearStaticIP Failed");
		return FAILURE;
	}

	// Apply new values in config
	CMOSglobalNetworkInformationStructure.dhcpFlag = config->dhcpFlag;

	status = startDHCP();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "DHCP Get Address Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirSt2DHCP:startDHCP Failed");
		return FAILURE;
	}

	status = setNAT(TRUE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Enable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirSt2DHCP:setNAT Failed");
		return FAILURE;
	}

	//update state variables
	wiredInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnWirSt2DHCP Finished");
	return retVal;
}

// DHCP is up - stop it, apply Wifi attributes
static BOOL fnWirDHCP2Wif(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;
	INT i;

	//update state variables
	wiredInternetConnected = FALSE;

	status = setNAT(FALSE);
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "NAT Disable Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirDHCP2Wif:setNAT(0) Failed");
		return FAILURE;
	}

	status = stopDHCP();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "DHCP Release Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Networking function fnWirDHCP2Wif:stopDHCP Failed");
		return FAILURE;
	}

	// Apply new values in config
	CMOSglobalNetworkInformationStructure.sourceFlag = config->sourceFlag;
	CMOSglobalNetworkInformationStructure.dhcpFlag = config->dhcpFlag;
	CMOSglobalNetworkInformationStructure.ipv4Gateway = config->ipv4Gateway;
	for (i = 0; i < DEVICE_DNS_ARRAY_MAX; i++)
		CMOSglobalNetworkInformationStructure.ipv4DNS[i] = config->ipv4DNS[i];

	status = setWifi();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "WiFi Setup Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Error: Failed to set Wifi settings: %d", status);
		return FAILURE;
	}

	//update state variables
	wifiInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnWirDHCP2Wif Finished");
	return retVal;
}

// Wired is configured, but not up - apply Wifi attributes
static BOOL fnWirCnf2Wif(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	STATUS status;
	INT i;

	//update state variables
	wiredInternetConnected = FALSE;

	// Apply new values in config
	CMOSglobalNetworkInformationStructure.sourceFlag = config->sourceFlag;
	CMOSglobalNetworkInformationStructure.dhcpFlag = config->dhcpFlag;
	CMOSglobalNetworkInformationStructure.ipv4Gateway = config->ipv4Gateway;
	for (i = 0; i < DEVICE_DNS_ARRAY_MAX; i++)
		CMOSglobalNetworkInformationStructure.ipv4DNS[i] = config->ipv4DNS[i];

	status = setWifi();
	if (status != NU_SUCCESS)
	{
		strncpy(errorBuf, "WiFi Setup Failure", bufSize);
		dbgTrace(DBG_LVL_ERROR, "Error: Failed to set Wifi settings: %d", status);
		return FAILURE;
	}

	//update state variables
	wifiInternetConnected = TRUE;

	dbgTrace(DBG_LVL_INFO, "Networking function fnWirCnf2Wif Finished");
	return retVal;
}

// Eth link not up yet - just store wired config
static BOOL fnWirCnf(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	BOOL retVal = SUCCESSFUL;
	INT i;

	// Apply new values in config
	CMOSglobalNetworkInformationStructure.sourceFlag = config->sourceFlag;
	CMOSglobalNetworkInformationStructure.dhcpFlag = config->dhcpFlag;
	if (config->dhcpFlag == NIS_DHCP_OFF)
	{
		CMOSglobalNetworkInformationStructure.ipv4Address = config->ipv4Address;
		CMOSglobalNetworkInformationStructure.ipv4Netmask = config->ipv4Netmask;
		CMOSglobalNetworkInformationStructure.ipv4Gateway = config->ipv4Gateway;
		for (i = 0; i < DEVICE_DNS_ARRAY_MAX; i++)
			CMOSglobalNetworkInformationStructure.ipv4DNS[i] = config->ipv4DNS[i];
	}

	dbgTrace(DBG_LVL_INFO, "Networking function fnWirCnf Finished");
	return retVal;
}


// Networking event process functions - END

// Networking event handler
// - this can be called from multiple tasks and since state machine is singleton a semaphore is used to sync access
static BOOL handleNetEvent(NETWORK_EVENT event, NetworkInformationStruct *config, char *errorBuf, int bufSize)
{
	char	semstat;
	BOOL retVal = FAILURE;

    semstat = OSAcquireSemaphore (NET_SM_SEM_ID, 10000); // wait up to 10 seconds
    if (semstat != SUCCESSFUL)
    {// cannot acquire semaphore even after waiting 10 s
        dbgTrace(DBG_LVL_ERROR,"NET>> ERROR: Networking state machine failed to acquire semaphore: %d\n", semstat);
        return retVal;
    }
	
	dbgTrace(DBG_LVL_INFO, "Networking Event: %d, Eth Link %d, Tab Link %d, Wired Int %d, Wifi Int %d",event,ethernetLinkUp, tabletLinkUp, wiredInternetConnected, wifiInternetConnected);

	//Ensure that states are valid
	if ((ethernetLinkUp < 2) && (tabletLinkUp < 2) && (wiredInternetConnected < 2) && (wifiInternetConnected < 2))
	{
		retVal = netStateFunctionTable[event][ethernetLinkUp][tabletLinkUp][wiredInternetConnected][wifiInternetConnected](event, config, errorBuf, bufSize);
	}
	else
	{
    	dbgTrace(DBG_LVL_ERROR, "Error:  Invalid networking state: Eth Link %d, Tab Link %d, Wired Int %d, Wifi Int %d", ethernetLinkUp, tabletLinkUp, wiredInternetConnected, wifiInternetConnected);
	}

    (void) OSReleaseSemaphore(NET_SM_SEM_ID);

	return retVal;
}

/* --------------------------------------------------------------------------------------------[NetworkMonitorTask]-- */
VOID NetworkMonitorTask(UNSIGNED argc, VOID *argv __attribute__ ((__unused__)) )
{
    UINT32              requested_events_mask;
    UINT32              notification_type;
    VOID               *notification_msg;
    size_t              notification_max_size;
    EQM_EVENT_ID        recvd_event_id = -1;
    EQM_EVENT_HANDLE    recvd_event_handle;
    STATUS              status;

    NU_MEMORY_POOL*     mem_pool;
	char 				strError[NIS_ERROR_LENGTH];
	BOOL				netstat;
    
    status = NU_System_Memory_Get(&mem_pool, NU_NULL);
    if (status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_INFO, "NetMon: Can't retrieve system memory pool");
        return;
    }

    initNetworkingState();

    // clear our mapping array linking Device ID to Device Name
    clearGlobalDeviceNameArray();
    //NU_MEMORY_POOL*   uncached_mem_pool 	= (NU_MEMORY_POOL*)argv ;
    /* Set the events to be retrieved. */
    requested_events_mask = NET_IF_CHANGE_ADD | 
                            NET_IF_CHANGE_DEL | 
                            LINK_CHANGE_STATE |
                            NET_IF_DHCP_ADDR_ASSIGNED | 
                            NET_IF_DHCP_REBIND_FAIL | 
                            NET_IF_DHCP_RENEW_FAIL;

    notification_max_size = MAX(sizeof(DHCP_IF_CHANGE_MSG), MAX(sizeof(NET_IF_CHANGE_MSG),sizeof(NET_IF_LINK_STATE_MSG)) );

    status = NU_Allocate_Memory(mem_pool, &notification_msg, notification_max_size, NU_NO_SUSPEND);

    if (status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_INFO, "NetMon: Can't allocate memory from system memory pool");
        return;
    }

    fnSysLogTaskStart("NetworkMonitorTask", NETMONTASK);
    /* Loop here waiting for, getting and processing event notifications.  */
    for (;;)
    {
        /* Wait for an event */
        NU_EQM_Wait_Event(&NET_Eqm, requested_events_mask, &notification_type, &recvd_event_id, &recvd_event_handle);
        /* Retrieve the event data sent */
        status = NU_EQM_Get_Event_Data(&NET_Eqm, recvd_event_id, recvd_event_handle, (EQM_EVENT *)(notification_msg));

        if (status == NU_SUCCESS)
        {
            switch (notification_type)
            {
                /* An interface has been added to the system, start listening for link-up / link-down events. */
                case NET_IF_CHANGE_ADD:
                	{
//						NET_IF_LINK_STATE_MSG *if_link_state_msg = (NET_IF_LINK_STATE_MSG*)notification_msg;
						addToGlobalDeviceNameArray((NET_IF_CHANGE_MSG *)notification_msg);
						dbgTrace(DBG_LVL_INFO, "\r\n\nNetMon: NET_IF_CHANGE_ADD received. \r\n");
						print_if_chg_msg((NET_IF_CHANGE_MSG *)notification_msg);
                	}
                    break;

                    /* An interface has been removed from the system, stop listening for link-up / link-down events. */
                case NET_IF_CHANGE_DEL:
                	dbgTrace(DBG_LVL_INFO, "\r\n\nNetMon: NET_IF_CHANGE_DEL received\r\n");
                    print_if_chg_msg((NET_IF_CHANGE_MSG *)notification_msg);
                    break;

                    /* Process link-up / link-down events here. */
                case LINK_CHANGE_STATE:
                    {
                        NET_IF_LINK_STATE_MSG *if_link_state_msg = (NET_IF_LINK_STATE_MSG*)notification_msg;
                    	BOOL bNetUp = (strcmp(if_link_state_msg->msg, LINK_UP_TEXT) == 0);
                        dbgTrace(DBG_LVL_INFO, "\r\n\nNetMon: LINK_CHANGE_STATE received. dev_id=%ld [%s] msg=\"%s\"\r\n",
                        			if_link_state_msg->dev_id,
                        			getDeviceNameFromID(if_link_state_msg->dev_id),
                        			if_link_state_msg->msg);

						if(strcmp(getDeviceNameFromID(if_link_state_msg->dev_id), DEFAULT_IF) == 0)
                        {
							// Process event and return error string if there is any failure
							netstat = handleNetEvent((bNetUp ? NETEVT_ETHERNET_LINK_UP : NETEVT_ETHERNET_LINK_DOWN),
													&CMOSglobalNetworkInformationStructure,
													strError,
													NIS_ERROR_LENGTH);
							if (netstat != SUCCESSFUL)
							{
                            	dbgTrace(DBG_LVL_ERROR,"Error: unable to process Ethernet link state change: status %d", netstat);
                            }
                        }
                        else if((strcmp(getDeviceNameFromID(if_link_state_msg->dev_id), NET_INTERFACE_2) == 0))
                        {
							// Process event and return error string if there is any failure
							netstat = handleNetEvent((bNetUp ? NETEVT_TABLET_LINK_UP : NETEVT_TABLET_LINK_DOWN),
													&CMOSglobalNetworkInformationStructure,
													strError,
													NIS_ERROR_LENGTH);
							if (netstat != SUCCESSFUL)
							{
                            	dbgTrace(DBG_LVL_ERROR,"Error: unable to process tablet link state change: status %d", netstat);
                            }
                        }

    					SendBaseEventToTablet(BASE_EVENT_NET_STATE_CHANGED, NULL);
                        print_available_urls();
                    }
                    break;

                case NET_IF_DHCP_ADDR_ASSIGNED:
					{
						dbgTrace(DBG_LVL_INFO, "\r\n\nNetMon: NET_IF_DHCP_ADDR_ASSIGNED received\r\n");
						memcpy(globalDefaultIPv4Gateway, ((DHCP_IF_CHANGE_MSG *)notification_msg)->gw_addr, MAX_ADDRESS_SIZE);
						netstat = handleNetEvent(NETEVT_WIRED_DHCP_ADDR_SUCCESS,
												&CMOSglobalNetworkInformationStructure,
												strError,
												NIS_ERROR_LENGTH);
						if (netstat != SUCCESSFUL)
						{
							dbgTrace(DBG_LVL_ERROR,"Error: unable to process DHCP Address Assignment: status %d", netstat);
						}
						SendBaseEventToTablet(BASE_EVENT_DHCP_UPDATE, NULL);
						print_dhcp_if_chg_msg((DHCP_IF_CHANGE_MSG *)notification_msg);
						print_available_urls();
					}
                    break;

                case NET_IF_DHCP_ADDR_FAILED:
					{
						NET_IF_LINK_STATE_MSG *if_link_state_msg = (NET_IF_LINK_STATE_MSG*)notification_msg;
						dbgTrace(DBG_LVL_INFO, "\r\n\nNetMon:Error NET_IF_DHCP_ADDR_FAILED received for device %s\r\n",
									getDeviceNameFromID(if_link_state_msg->dev_id));
						netstat = handleNetEvent(NETEVT_WIRED_DHCP_ADDR_FAIL,
												&CMOSglobalNetworkInformationStructure,
												strError,
												NIS_ERROR_LENGTH);
						if (netstat != SUCCESSFUL)
						{
                        	dbgTrace(DBG_LVL_ERROR,"Error: unable to process DHCP Address Fail: status %d", netstat);
                        }
						SendBaseEventToTablet(BASE_EVENT_DHCP_ERROR, NULL);
					}
                	break;

                case NET_IF_DHCP_REBIND_FAIL:
                	dbgTrace(DBG_LVL_INFO, "\r\n\nNetMon: NET_IF_DHCP_REBIND_FAIL received\r\n");
                    print_dhcp_if_chg_msg((DHCP_IF_CHANGE_MSG *)notification_msg);
                    print_available_urls();
                    break;

                case NET_IF_DHCP_RENEW_FAIL:
                	dbgTrace(DBG_LVL_INFO, "\r\n\nNetMon: NET_IF_DHCP_RENEW_FAIL received\r\n");
                    print_dhcp_if_chg_msg((DHCP_IF_CHANGE_MSG *)notification_msg);
                    print_available_urls();
                    break;

                default:
                	dbgTrace(DBG_LVL_INFO, "\r\n\nUnknown Network Event received [%08lX]\r\n", notification_type);
                	break ;

            } /* end switch */

        }
    }
}

/* --------------------------------------------------------------------------------------------[setDHCPServerDNSAddresses]-- */
STATUS setDHCPServerDNSAddresses(void)
{
	// iterate through our DNS servers and place them in the DHCPServer
	// start by removing any existing ones....
	UINT8 dnsDHCPServers[DHCPS_MAX_DNS_SERVERS][IP_ADDR_LEN];
	UINT8 dnsOurServers[DHCPS_MAX_DNS_SERVERS*2][IP_ADDR_LEN];
	STATUS status = NU_SUCCESS;

	memset(dnsDHCPServers, 0, sizeof(dnsDHCPServers));
	INT iNumBytes = DHCPS_Get_DNS_Servers(globalDeviceDHCPCtrlBlk,
	                  NULL,
					  (UINT8 *)dnsDHCPServers,
	                  sizeof(dnsDHCPServers));
	if(iNumBytes > 0)
	{
		INT iNumServers = iNumBytes/IP_ADDR_LEN;
		for(int iPass=0 ; (iPass < DHCPS_MAX_DNS_SERVERS) && (iPass < iNumServers) ; iPass++)
		{
			struct id_struct ipDNS;
			memcpy(&ipDNS, dnsDHCPServers[iPass], IP_ADDR_LEN);
			status = DHCPS_Delete_DNS_Server(globalDeviceDHCPCtrlBlk, NU_NULL, &ipDNS);
			if (status != NU_SUCCESS)
			{
		    	dbgTrace(DBG_LVL_ERROR, "Error: DHCPS_Delete_DNS_Server failed: %d; server: %d/%d", status, iPass, iNumServers);
				return status;
			}
		}
	} else if (iNumBytes < 0)
	{// return error status
    	dbgTrace(DBG_LVL_ERROR, "Error: DHCPS_Get_DNS_Servers failed: %d", iNumBytes);
		return iNumBytes;
	}

	memset(dnsOurServers, 0, sizeof(dnsOurServers));
	status = NU_Get_DNS_Servers ((UINT8 *)dnsOurServers, sizeof(dnsOurServers));
	if(status > 0)
	{
		int iCount = status;
		status = NU_SUCCESS;
		for(int iPass=0 ; (status == NU_SUCCESS) && (iPass < iCount) ; iPass++)
		{
			// now grab the DNS servers we have and populate the DHCPServer
			struct id_struct ipDNS;
			// not sure why but the bytes are reversed
			ipDNS.is_ip_addrs[3] = dnsOurServers[iPass][0];
			ipDNS.is_ip_addrs[2] = dnsOurServers[iPass][1];
			ipDNS.is_ip_addrs[1] = dnsOurServers[iPass][2];
			ipDNS.is_ip_addrs[0] = dnsOurServers[iPass][3];
			status = DHCPS_Add_DNS_Server (globalDeviceDHCPCtrlBlk, NULL, &ipDNS);
			if (status != NU_SUCCESS)
			{
		    	dbgTrace(DBG_LVL_ERROR, "Error: DHCPS_Add_DNS_Server failed: %d", status);
			}
		}
	} else
	{
    	dbgTrace(DBG_LVL_ERROR, "Error: NU_Get_DNS_Servers failed: %d", status);
		status = NU_RETURN_ERROR;
	}

	return status;
}

/* --------------------------------------------------------------------------------------------[setNAT]-- */
STATUS setNAT(BOOL on)
{
	NAT_DEVICE natDevices;
	STATUS 	status = NU_SUCCESS;

//    if (IP_NAT_Initialize) - there should be no need to access Nucleus internal variable here

    if (!on)
    {// turn off
    	status = NAT_Shutdown();
    	if(status != NU_SUCCESS)
    	{
        	dbgTrace(DBG_LVL_ERROR, "Error: NAT shutdown failed: %d", status);
    	} else
    	{
        	dbgTrace(DBG_LVL_INFO, "NAT shutdown OK");
    	}
    	return status;
    }

    // If here, turn on NAT
	strcpy(natDevices.nat_dev_name, NET_INTERFACE_2);
	status = NAT_Initialize(&natDevices, 1, NET_INTERFACE_ETH0) ;
	if(status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Error: Unable to initialize NAT [%d]", status) ;
	}
	else
	{
		dbgTrace(DBG_LVL_INFO, "NAT enabled") ;
	}

	return status;
}

// Applies Wifi parameters stored in NVRAM
STATUS setWifi(void)
{
	STATUS 	status = NU_SUCCESS;
	INT iCount = 0;
	char	strTmp[50];

	memcpy(globalDefaultIPv4Gateway, CMOSglobalNetworkInformationStructure.ipv4Gateway.ip4u8, IP4_BYTESIZE) ;
	status = setDefaultGateway(globalDefaultIPv4Gateway) ;
	if (status !=  NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Error failed to set WiFi gateway %s: %d",
				IP4ArrayToString((UINT8 *)CMOSglobalNetworkInformationStructure.ipv4Gateway.ip4u8, strTmp, 50, FALSE), status) ;
		return status;
	}

	for(iCount = 0 ; (iCount < DEVICE_DNS_ARRAY_MAX) && (CMOSglobalNetworkInformationStructure.ipv4DNS[iCount].ip4U32 != 0) ; iCount++)
	{
		status = NU_Add_DNS_Server(CMOSglobalNetworkInformationStructure.ipv4DNS[iCount].ip4u8, DNS_ADD_TO_END);
		if (status !=  NU_SUCCESS)
		{
			dbgTrace(DBG_LVL_ERROR, "Error failed to set WiFi DNS addr %s: %d",
					IP4ArrayToString((UINT8 *)CMOSglobalNetworkInformationStructure.ipv4DNS[iCount].ip4u8, strTmp, 50, FALSE), status) ;
			return status;
		}
	}

	return status;
}

// Clears Wifi parameters in Nucleus
STATUS clearWifi(void)
{
	STATUS 	status = NU_SUCCESS;

	// Clear out old default route
/*
	status = NU_Delete_Route(null_ip);
	if (status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Error unable to delete default default route: %d", status);
		return status;
	}
*/
	status = deleteAllBaseDNSEntries();
	if (status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Error unable to delete old DNS entries");
	}

	return status;
}

// Starts DHCP
STATUS startDHCP(void)
{
	STATUS 	status = NU_SUCCESS;
	IFCONFIG_NODE *ifconfig_ptr;

	status = fnGetIFConfig(DEFAULT_IF, &ifconfig_ptr);
	// check to see if the OS has a DHCP control structure, if present then use it else use our global one.
	if(status != NU_SUCCESS)
	{
    	dbgTrace(DBG_LVL_INFO, "DHCP start with global struct");
		ptrGlobalEth0DhcpStruct = &globalEth0DhcpStruct;
	}
	else
	{
    	dbgTrace(DBG_LVL_INFO, "DHCP start with interface struct");
		ptrGlobalEth0DhcpStruct = &(ifconfig_ptr->ifconfig_dhcp);
	}

	// Add required option parameter list to dhcp control structure.
	ptrGlobalEth0DhcpStruct->dhcp_opts 		= (UINT8 *)&globalDHCPOptions;
	ptrGlobalEth0DhcpStruct->dhcp_opts_len 	= globalDHCPOptionsLength;

	status = handleDHCPWithRetries();
	if(status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Error failed to start DHCP") ;
	}

	return status;
}

// Stops DHCP
STATUS stopDHCP(void)
{
	STATUS 	status = NU_SUCCESS;
    char    ip_addr_str[IP_ADDR_STR_LEN];
    char    ip_addr_str2[IP_ADDR_STR_LEN];
    char    ip_addr_str3[IP_ADDR_STR_LEN];

	if(ptrGlobalEth0DhcpStruct)
	{
		dbgTrace(DBG_LVL_INFO, "DHCP release params: dv_name %s, file %s, giaddr %s, siaddr %s, yiaddr %s, ",
				ptrGlobalEth0DhcpStruct->dhcp_dv_name,
				ptrGlobalEth0DhcpStruct->dhcp_file,
				ip_addr_to_str(ptrGlobalEth0DhcpStruct->dhcp_giaddr, NU_FAMILY_IP, IP_NOT_MAPPED, ip_addr_str),
				ip_addr_to_str(ptrGlobalEth0DhcpStruct->dhcp_siaddr, NU_FAMILY_IP, IP_NOT_MAPPED, ip_addr_str2),
				ip_addr_to_str(ptrGlobalEth0DhcpStruct->dhcp_yiaddr, NU_FAMILY_IP, IP_NOT_MAPPED, ip_addr_str3)
				);
		status = NU_Dhcp_Release(ptrGlobalEth0DhcpStruct, DEFAULT_IF);
		dbgTrace(DBG_LVL_INFO, "DHCP release exit");
		if(status != NU_SUCCESS)
		{
			dbgTrace(DBG_LVL_ERROR, "Error failed to stop DHCP: %d", status) ;
		}
	}

	status = deleteAllBaseDNSEntries();
	if (status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Error unable to delete DNS entries: %d", status);
		return status;
	}

	return status;
}

// Applies Static IP parameters
STATUS setStaticIP(NetworkInformationStruct *config)
{
	STATUS 	status = NU_SUCCESS;
	char	strTmp[50];
	char	strTmp2[50];

	// Nucleus will return NU_INVALID_PARM if we try to attach an IP address that is already there
	// But that return value is used for other error cases as well,
	// so to avoid this error we will first check if IP address already exists in device
    if (isStaticIPNew(config))
    {
		status = NU_Attach_IP_To_Device(DEFAULT_IF, config->ipv4Address.ip4u8, config->ipv4Netmask.ip4u8);
		if (status  != NU_SUCCESS)
		{
			dbgTrace(DBG_LVL_ERROR, "Error unable to assign static IP address %s, netmask %s : %d",
					IP4ArrayToString((UINT8 *)config->ipv4Address.ip4u8, strTmp, 50, FALSE),
					IP4ArrayToString((UINT8 *)config->ipv4Netmask.ip4u8, strTmp2, 50, FALSE),
					status);
			return status;
		}
    }

    status = setGatewayAndDNS(config);
	if (status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Error setStaticIP:setGatewayAndDNS Failed");
		return status;
	}

	status = setDHCPServerDNSAddresses();
	if (status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Error setStaticIP:setDHCPServerDNSAddresses Failed");
		return status;
	}

	return status;
}

// Clears Static IP parameters in Nucleus
STATUS clearStaticIP(void)
{
	STATUS 	status = NU_SUCCESS;

	status = NU_Remove_IP_From_Device(DEFAULT_IF, CMOSglobalNetworkInformationStructure.ipv4Address.ip4u8,  NU_FAMILY_IP);
	if (status  != NU_SUCCESS)
	{// There are some reasons why this may not be a true failure:
		// 1. a previous attach failed so the address was not found
		// 2. the Ethernet link is down so the device is disabled and the address is not found for that reason
		// So if the address is not found, keep going, otherwise it is an error
		if (status  == NU_INVALID_PARM)
		{
			dbgTrace(DBG_LVL_INFO, "Unable to remove the current IP address because it was not found in device");
		}
		else
		{
	        dbgTrace(DBG_LVL_ERROR, "Error unable to remove the current IP : %d", status);
	        return status;
		}
	}

	// Clearing out old default route causes problems, so don't call it
/*
	status = NU_Delete_Route(null_ip);
	if (status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Error unable to delete default route: %d", status);
		return status;
	}
 */
	status = deleteAllBaseDNSEntries();
	if (status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Error unable to delete DNS entries: %d", status);
		return status;
	}

	return status;
}

// Is static IP address changing?
BOOL isStaticIPNew(NetworkInformationStruct *config)
{
    NU_IOCTL_OPTION ioctl_opt;
	STATUS 	status = NU_SUCCESS;

    /* Find the IP address attached to the network device. */
    memset(&ioctl_opt, 0, sizeof(NU_IOCTL_OPTION));
    ioctl_opt.s_optval = (UINT8*) DEFAULT_IF;
    status = NU_Ioctl(SIOCGIFADDR, &ioctl_opt, sizeof(ioctl_opt));
    if (status != NU_SUCCESS)
    {
		dbgTrace(DBG_LVL_INFO, "Unable to get current IP address: %d", status);
		return TRUE;
    }

    return (memcmp(ioctl_opt.s_ret.s_ipaddr, &(config->ipv4Address), 4) != 0);
}

// Applies Gateway and DNS addresses
STATUS setGatewayAndDNS(NetworkInformationStruct *config)
{
	STATUS 	status = NU_SUCCESS;
	INT iCount = 0;
	char	strTmp[50];

	memcpy(globalDefaultIPv4Gateway, config->ipv4Gateway.ip4u8, IP4_BYTESIZE) ;
	status = setDefaultGateway(globalDefaultIPv4Gateway);
	if (status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Error setGatewayAndDNS:setDefaultGateway Failed");
		return status;
	}

	for(iCount = 0 ; (iCount < DEVICE_DNS_ARRAY_MAX) && (config->ipv4DNS[iCount].ip4U32 != 0) ; iCount++)
	{
		status = NU_Add_DNS_Server(config->ipv4DNS[iCount].ip4u8, DNS_ADD_TO_END);
		if (status !=  NU_SUCCESS)
		{
			dbgTrace(DBG_LVL_ERROR, "Error failed to set Static IP DNS addr %s: %d",
					IP4ArrayToString((UINT8 *)config->ipv4DNS[iCount].ip4u8, strTmp, 50, FALSE), status) ;
			return status;
		}
	}

	return status;
}


/* --------------------------------------------------------------------------------------------[setDefaultGateway]-- */
STATUS setDefaultGateway(UINT8 *ipAddress)
{
	STATUS status = NU_SUCCESS;
	char	strTmp[50];
	ROUTE_NODE *default_route;

	if((*(UINT32 *)ipAddress) == 0)
	{
		dbgTrace(DBG_LVL_ERROR, "Error failed to install default route: No address") ;
		status = NU_RETURN_ERROR;
		return status;
	}

	default_route = NU_Get_Default_Route (NU_FAMILY_IP);
	if((default_route != NU_NULL) && (*((UINT32 *)default_route->rt_ip_addr)) == (*(UINT32 *)ipAddress) )
	{// input same as default route so do nothing
		dbgTrace(DBG_LVL_INFO, "Input route same as default route so do nothing") ;
		return status;
	}

	//At this point there is no default route, or if there is one, it needs replacement
	status = NU_Add_Route(null_ip, null_ip, ipAddress);
	if(status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Error failed to install default route for %s [%d]\r\n", IP4ArrayToString(ipAddress, strTmp, 50, FALSE), status) ;
	}
	else
	{
		memcpy(globalDefaultIPv4Gateway, ipAddress, 4);
		dbgTrace(DBG_LVL_INFO, "Installed default route %s\r\n", IP4ArrayToString(ipAddress, strTmp, 50, FALSE)) ;
	}
	return status;
}

/* --------------------------------------------------------------------------------------------[fnIsEthernetAvailable]-- */
BOOL fnIsEthernetAvailable()
{
	BOOL retStatus = FALSE;
    char                   lan_names[100];
    char                  *lan_name;
    size_t                 lan_names_size = sizeof(lan_names);
    unsigned char          res;
    LanInfo                lan_info;
    char                   ip_addr_str[IP_ADDR_STR_LEN];
    char                  *pRest = NULL;

    res = fnGetLANNames(lan_names, &lan_names_size);
    printf("\r\n\n GetLANNames: result = %d, lan_names=[%s]\r\n", res, lan_names);
    lan_name = strtok_r(lan_names, ",", &pRest);
    while (lan_name != NULL)
    {
        res = fnGetLANConfig(lan_name, &lan_info);
        if (res == NU_SUCCESS && lan_info.if_up)
        {
            printf("  interface %s : ws://%s/ws_csd\r\n", lan_name, ip_addr_to_str(lan_info.ip_addr, NU_FAMILY_IP, IP_NOT_MAPPED, ip_addr_str));
            store_our_ip_addr(lan_name, lan_info.ip_addr);
            retStatus = TRUE;
        }
        lan_name = strtok_r(NULL, ",", &pRest);
    }

	return retStatus;
}

/* --------------------------------------------------------------------------------------------[removeFromDNSHostList]-- */
/*
 * Remove the supplied URL from the cached DNS Host list
 */
BOOL removeFromDNSHostList(char *name)
{
	BOOL retval = FALSE;
    STATUS      status;
	DNS_HOST *pHost;
	char    sDbgBuf[SHORT_DBG_BUF_LEN];

// Note: These functions are internal Nucleus functions so require using the same internal
// semaphore used by Nucleus.
    status = NU_Obtain_Semaphore(&DNS_Resource, NU_SUSPEND);
    if (status == NU_SUCCESS)
    {
    	//    DNS_HOST *pHost = DNS_Find_Matching_Host_By_Name(const CHAR *name, CHAR *data, INT16 type)
    	    pHost = DNS_Find_Matching_Host_By_Name(name, NULL, 1) ;
    	    if(pHost)
    	    {
    	    	DNS_Delete_Host(pHost);
    	    	retval = TRUE;
    	    }
    	    NU_Release_Semaphore(&DNS_Resource);
    }
    if (retval)
    {
    	snprintf(sDbgBuf, SHORT_DBG_BUF_LEN, "Deleted existing DNS Host entry for %s", name);
    }
    else
    {
    	snprintf(sDbgBuf, SHORT_DBG_BUF_LEN, "Did not find DNS Host entry for %s; semstat: %d", name, status);
    }
    fnDumpStringToSystemLog(sDbgBuf);
    return retval;
}

/* --------------------------------------------------------------------------------------------[getProxyUseState]-- */
UINT getProxyUseState(char *url)
{
	UINT retVal = PROXY_DISABLED;
	// return the state of the proxy requirement. Returned values are
	//	PROXY_DISABLED
	//	PROXY_URL_EXCLUDED
	//	PROXY_URL_INCLUDED
	if(globalProxyConfig.fEnabled && (strlen(url) < CSDPROXY_ADDRESS_SIZE))
	{
		retVal = PROXY_URL_INCLUDED;
		for(int iPass=0 ; (iPass < globalProxyConfig.ExclusionCount) && (retVal == PROXY_URL_INCLUDED) ; iPass++)
		{
			// try for a partial match, look for the domain at the end of our url
			// search for matching part of Exclusion entry in supplied url, if found note the start point, subtract the start of the supplied
			// url to give us the number of characters before the domain section, Now add on the length of the searched for section
			// and this should equal the full length of the host supplied, if not then the supplied one had more info after the searched for part.
//			char *ptr = findString(globalProxyConfig.ExclusionUrl[iPass].url, url);
//			if(ptr && ( ((ptr - url) + strlen(globalProxyConfig.ExclusionUrl[iPass].url)) == strlen(url)) )
			if(hostCompareMaskedMatch(url, globalProxyConfig.ExclusionUrl[iPass].url))
			{
				dbgTrace(DBG_LVL_INFO,"PROXY use EXCLUDED to %s due to Exclusion entry %s\r\n",url, globalProxyConfig.ExclusionUrl[iPass].url);
				retVal = PROXY_URL_EXCLUDED;
			}
		}
	}
	if(retVal == PROXY_URL_INCLUDED)
		dbgTrace(DBG_LVL_INFO,"PROXY being used for url %s\r\n", url);

	return retVal;
}

/* --------------------------------------------------------------------------------------------[getProxyUseState]-- */
// Masked match will return true if the supplied host name has the maskMatch name at the end, for example
// full host name  distserv.pb.com   will return true compared to maskMatch  pb.com  since pb.com appears at the end
// I guess this is similar but less flexible than a wildcard approach of *.pb.com
//
BOOL hostCompareMaskedMatch(char *fullHostName, char *maskMatch)
{
	BOOL bRetVal = FALSE ;
	char *ptr = findString(maskMatch, fullHostName);
	// if we found a substring AND if all the chars before the start of that substring PLUS the length of our match value
	// equals the complete length of the supplied host then return TRUE (should mean that there are no characters after the match portion).
	if(ptr && ( ((ptr - fullHostName) + strlen(maskMatch)) == strlen(fullHostName)) )
	{
		bRetVal = TRUE ;
	}

	return bRetVal;
}

/* --------------------------------------------------------------------------------------------[displayProxySettings]-- */
void displayProxySettings()
{
	if(globalProxyConfig.fEnabled)
	{
		dbgTrace(DBG_LVL_INFO, "\r\nProxy Active :            Proxy IP = %s", globalProxyConfig.Https.pAddress);
		dbgTrace(DBG_LVL_INFO, "                        Proxy Port = %d", globalProxyConfig.Https.uwPort);
		dbgTrace(DBG_LVL_INFO, "                    Proxy Username = %s", globalProxyConfig.Https.pUsername);
		dbgTrace(DBG_LVL_INFO, "                    Proxy Password = %s", globalProxyConfig.Https.pPassword);
		if(globalProxyConfig.ExclusionCount > 0)
		{
			for(int iPass=0 ; iPass < globalProxyConfig.ExclusionCount ; iPass++)
			{
				dbgTrace(DBG_LVL_INFO, "                      Excluding %02d = %s", iPass, globalProxyConfig.ExclusionUrl[iPass].url);
			}
		}
		dbgTrace(DBG_LVL_INFO, "\r\n");
	}
}

/* --------------------------------------------------------------------------------------------[sendProxyConnectMessage]-- */
/*
 * This function sends the required, formatted CONNECT message to a proxy and waits for the return message.
 * it will return NU_SUCCESS if all goes according to plan
 */
#define PROXY_RESP_BUFF_SIZE 180
STATUS sendProxyConnectMessage(INT32 nuSocket, char *hostName, UINT hostPortNum )
{
	STATUS status = NU_SUCCESS;
	// we need to send the connect request to the proxy, in ssl mode this seems to be required to allow
	// the proxy to start tunneling
//	if(globalProxyConfig.fEnabled && (globalProxyConfig.Https.pAddress != NULL))
	if(getProxyUseState(hostName) == PROXY_URL_INCLUDED)
	{
		char strProxyStr[350];
		char strBuffer[100];
		char strRecv[PROXY_RESP_BUFF_SIZE+1];
		INT	iRcvCount = 0;
		fnGetProxyAuthStringForHeader(PROXY_HTTPS, strBuffer);

		snprintf(strProxyStr, 350, "CONNECT %s:%d HTTP/1.1\r\nHost: %s:%d\r\n%sProxy-Connection: Keep-Alive\r\n\r\n",
				hostName, hostPortNum, hostName, hostPortNum, strBuffer);

		INT iSent = NU_Send(nuSocket, strProxyStr, strlen(strProxyStr), 0);
		if(iSent == strlen(strProxyStr))
		{
			iRcvCount = NU_Recv(nuSocket, strRecv, PROXY_RESP_BUFF_SIZE, 0);
			if(iRcvCount < 0)
			{
				dbgTrace(DBG_LVL_INFO, "ERROR Proxy connect phase, error in response message [sendProxyConnectMessage] NU_Recv returned %d\r\n", iRcvCount);
				status = NU_RETURN_ERROR;
			}
			else
			{
				strRecv[PROXY_RESP_BUFF_SIZE] = 0;
				// RD It seems there is inconformity in the case used in the response message so convert our string to lower case first
				int iPass;
				for(iPass = 0 ; strRecv[iPass] && (iPass < PROXY_RESP_BUFF_SIZE) ; iPass++)
				{
					strRecv[iPass] = tolower(strRecv[iPass]);
				}
				if(strstr(strRecv, "established")!= NULL)
				{

					dbgTrace(DBG_LVL_INFO, "Proxy Connection Established [sendProxyConnectMessage]");
				}
				else
				{
					dbgTrace(DBG_LVL_INFO, "ERROR Proxy cannot establish connection [%s] [sendProxyConnectMessage]", strRecv) ;
					status = NU_RETURN_ERROR;
				}
			}
		}
		else
		{
			dbgTrace(DBG_LVL_INFO, "ERROR Proxy connect phase, unable to write data [%d:%d] [sendProxyConnectMessage]\r\n", strlen(strProxyStr), iSent);
			status = NU_RETURN_ERROR;
		}
	}
	return status;
}

// Get Distributor IP address
BOOL            getDistributorIPAddress(struct id_struct *id)
{
	CRACKED_URL crackedDistURL;
	BOOL retStatus = TRUE;
	STATUS nuStatus;
	NU_HOSTENT *hentry;
	UINT8 *addr_list;

	// Get host name from Distributor URL
	if (crackUrl(&crackedDistURL, CMOSNetworkConfig.distributorUrl) != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Distributor URL %s is invalid", CMOSNetworkConfig.distributorUrl);
		return FALSE;
	}

    // Run a DNS lookup for the reference site, choose a reference site wisely. Note that DNS addresses are
    // cached so for this to be a true test we need to remove the test site cache entry prior to running the test
    removeFromDNSHostList(crackedDistURL.host) ;

	// Get IP address from DNS
	hentry = NU_Get_IP_Node_By_Name(crackedDistURL.host, NU_FAMILY_IP, 0, &nuStatus);
	if (nuStatus != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Error on DNS call for %s, status %d", crackedDistURL.host, nuStatus);
		return FALSE;
	}

	// Get IP address from host entry
	addr_list = (UINT8 *)(*hentry->h_addr_list);
	id->is_ip_addrs[0] = addr_list[0];
	id->is_ip_addrs[1] = addr_list[1];
	id->is_ip_addrs[2] = addr_list[2];
	id->is_ip_addrs[3] = addr_list[3];

	(void) NU_Free_Host_Entry(hentry) ;

	return retStatus;
}

/* --------------------------------------------------------------------------------------------[connectedToInternet]-- */
// Ping Distributor to determine connection
BOOL connectedToInternet()
{
	BOOL retStatus = TRUE;
	struct id_struct ipAddr;

	// if this is a proxy connection we cannot use this approach because Nucleus does not support proxies
	// for now let's ignore and return true.. we need to modify the check at least to check for proxy and maybe to
	// authenticate proxy and perform dns check.
	if (globalProxyConfig.fEnabled)
	{
		dbgTrace(DBG_LVL_INFO, "Using proxy so assuming connected to Internet");
		return TRUE;
	}

	if (getDistributorIPAddress(&ipAddr) == FALSE)
	{
		dbgTrace(DBG_LVL_ERROR, "Not connected to Internet based on no DNS access or bad Distributor URL");
		return FALSE;
	}

#if 0	// Ping of Prod works, but ping of QA does not work
	STATUS nuStatus;
	// ping using default timeout
	nuStatus = NU_Ping(ipAddr.is_ip_addrs, 0);
	if (nuStatus == NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Connected to Internet based on Ping to Distributor IP %d:%d:%d:%d, status %d",
				ipAddr.is_ip_addrs[0],
				ipAddr.is_ip_addrs[1],
				ipAddr.is_ip_addrs[2],
				ipAddr.is_ip_addrs[3],
				nuStatus);
		return TRUE;
	}

	else
	{
		dbgTrace(DBG_LVL_ERROR, "Not connected to Internet based on Ping to Distributor IP %d:%d:%d:%d, status %d",
				ipAddr.is_ip_addrs[0],
				ipAddr.is_ip_addrs[1],
				ipAddr.is_ip_addrs[2],
				ipAddr.is_ip_addrs[3],
				nuStatus);
		return FALSE;
	}
#endif

	return retStatus;
}

/* --------------------------------------------------------------------------------------------[isNULLAddress]-- */
BOOL isNULLIPv4Address(UINT8 *padds)
{
	return (*(UINT32 *)padds == 0);
}

/* --------------------------------------------------------------------------------------------[printNUSocketInformation]-- */
void printNUSocketInformation()
{
	INT 	iPass;
	char	tmpStr1[100];
//	char	tmpStr2[100];
	char	sktStr[501];
	BOOL	bFirstPass = TRUE ;
	//TODO - With new Nucleus release, confirm access of internal structures is still valid and check whether it can be replaced with API call
	struct sock_struct  tempSockStruct;
	struct _TCP_Port    tempTCPStruct;
	BOOL                gotTCPInfo = FALSE;
    STATUS              status;

	for(iPass = 0 ; (iPass < NSOCKETS) ; iPass++)
	{
        status = SCK_Protect_Socket_Block(iPass);
        if (status != NU_SUCCESS)
            continue; // go on to next socket

		//copy internal Nucleus structure; read copy only
		memcpy(&tempSockStruct, SCK_Sockets[iPass], sizeof(tempSockStruct));
	    SCK_Release_Socket();
		if(tempSockStruct.s_accept_list != NULL)
		{
			if(bFirstPass)
			{
				bFirstPass = FALSE ;
				debugDisplayOnly("\r\nListening on:\r\n");
			}
			// this is a listener socket
			debugDisplayOnly("%3d [%s:%s]- %s:%s [%s]\r\n",
					iPass,
					socketFamilyToString(tempSockStruct.s_family),
					socketProtocolToString(tempSockStruct.s_protocol),
					ip_addr_to_str(tempSockStruct.s_local_addr.ip_num.is_ip_addrs, tempSockStruct.s_family, tempSockStruct.s_flags, tmpStr1),
					ip_port_to_str(tempSockStruct.s_local_addr.port_num),
					socketStateToString(tempSockStruct.s_state)
					);
		}
	}



	bFirstPass = TRUE;
	for(iPass = 0 ; (iPass < NSOCKETS) ; iPass++)
	{
        status = SCK_Protect_Socket_Block(iPass);
        if (status != NU_SUCCESS)
            continue; // go on to next socket

		//copy internal Nucleus structures; read copy only
		memcpy(&tempSockStruct, SCK_Sockets[iPass], sizeof(tempSockStruct));
		if(TCP_Ports[tempSockStruct.s_port_index] != 0)
		{
		    memcpy(&tempTCPStruct, TCP_Ports[tempSockStruct.s_port_index], sizeof(tempTCPStruct));
		    gotTCPInfo = TRUE;
		}
		else
		{
			gotTCPInfo = FALSE;
		}
	    SCK_Release_Socket();
		if(tempSockStruct.s_state != 0)
		{
			if(bFirstPass)
			{
				bFirstPass = FALSE ;
				debugDisplayOnly("\r\nConnections:\r\n") ;
			}
			socketIDToString(iPass, &tempSockStruct, sktStr, 500);
			debugDisplayOnly("%s", sktStr);
			if(gotTCPInfo)
			{
				UINT32 inCount = 0;
				UINT32 outCount = 0;

				if((tempTCPStruct.state != 0) && (tempTCPStruct.state != SS_NOFDREF))
				{
					inCount = tempTCPStruct.in.nxt;
					outCount = tempTCPStruct.out.nxt;
				}

				debugDisplayOnly("PORT STATS: out[%10u]  in[%10u] - [%s]\r\n\r\n",
						outCount,
						inCount,
						socketStateToString(tempTCPStruct.state)) ;
			}
		}
	}

	bFirstPass = TRUE;
	for(iPass = 0 ; (iPass < TCP_MAX_PORTS) ; iPass++)
	{
        status = SCK_Protect_Socket_Block(iPass);
        if (status != NU_SUCCESS)
            continue; // go on to next socket

		if(TCP_Ports[iPass] != NULL)
		{
			//copy internal Nucleus structures; read copy only
			memcpy(&tempTCPStruct, TCP_Ports[iPass], sizeof(tempTCPStruct));
	        SCK_Release_Socket();

			if(bFirstPass)
			{
				bFirstPass = FALSE ;
				debugDisplayOnly("\r\nPort Information:\r\n") ;
				debugDisplayOnly("                   ---- Inbound ----  --  ----- Outbound ---- \r\n") ;
				debugDisplayOnly("Index SktId  MTU   Port  QSize Window --  Port  QSize  Window \r\n") ;
			}
			if(tempTCPStruct.p_socketd >= 0)
			{
				debugDisplayOnly("%2d    [%2d]  %5d  %5d  %5d %5d  -- %5d  %5d  %5d\r\n",
						iPass,
						tempTCPStruct.p_socketd,
						tempTCPStruct.sendsize,
						// in/out
						tempTCPStruct.in.port,
						tempTCPStruct.in.contain,
						tempTCPStruct.in.size,
						tempTCPStruct.out.port,
						tempTCPStruct.out.contain,
						tempTCPStruct.out.size
						) ;
			}
		}
		else
		{
	        SCK_Release_Socket();
		}

	}
	debugDisplayOnly("\r\n");
}

UINT killSocketsWithMatchingPorts(UINT16 u16Port)
{
	UINT uRes = 0 ;
	INT	iPass;
    STATUS              status;

	for(iPass=0 ; iPass < NSOCKETS; iPass++)
	{
		//TODO - With new Nucleus release, confirm access of internal structures is still valid and check whether it can be replaced with API call
        status = SCK_Protect_Socket_Block(iPass);
        if (status != NU_SUCCESS)
            continue; // go on to next socket

		if(SCK_Sockets[iPass]->s_local_addr.port_num == u16Port)
		{
			uRes++ ;
	        SCK_Release_Socket();
			break;
		}
        SCK_Release_Socket();
	}

	if (uRes != 0)
		NU_Close_Socket(iPass) ;

	return uRes ;
}
/* --------------------------------------------------------------------------------------------[socketIDToString]-- */
INT socketIDToString(INT iSkt, struct sock_struct *pTmpSocket, char *str, size_t strLen)
{
	char	tmpStr1[100];
	char	tmpStr2[100];
	INT 	iResult = -1;

	*str = 0 ;
	if((iSkt >= 0) && (pTmpSocket != 0))
	{
		iResult = snprintf(str, strLen, "%3d - [%s]%s:%s  <-->  %s:%5d - %s\r\n",
				iSkt,
				socketFamilyToString(pTmpSocket->s_family),
				ip_addr_to_str(pTmpSocket->s_local_addr.ip_num.is_ip_addrs, pTmpSocket->s_family, pTmpSocket->s_flags, tmpStr1),
				ip_port_to_str(pTmpSocket->s_local_addr.port_num),
				ip_addr_to_str(pTmpSocket->s_foreign_addr.ip_num.is_ip_addrs, pTmpSocket->s_family, pTmpSocket->s_flags, tmpStr2),
				pTmpSocket->s_foreign_addr.port_num,
				socketStateToString(pTmpSocket->s_state)
				);
	}
	return iResult ;
}

/* --------------------------------------------------------------------------------------------[on_GetNUSocketInformationReq]-- */
// This acquires a semaphore so do not return without releasing semaphore first
void on_GetNetworkInformationReq(UINT32 handle, cJSON *root)
{
    cJSON 				*rspMsg		=  cJSON_CreateObject();
    WSOX_Queue_Entry 	entry  		=  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    INT 				iPass;
	char				tmpStr1[100];
	//TODO - With new Nucleus release, confirm access of internal structures is still valid and check whether it can be replaced with API call
	struct sock_struct  tempSockStruct;
	struct _TCP_Port    tempTCPStruct;
	BOOL                gotTCPInfo = FALSE;
    STATUS              status;
    char  				semstat;

	if(rspMsg)
	{

	    // Get semaphore for DHCP Server - do not suspend, do not want to hold up tablet communication
	    semstat = OSAcquireSemaphore (ETH_DHCPS_SEM_ID, OS_NO_SUSPEND);
	    if (semstat != SUCCESSFUL)
		{//we are busy getting addresses from DHCP Server so do not access any data - this could destabilize the networking layer
	        dbgTrace(DBG_LVL_ERROR,"Error: GetNetworkInformationReq failed to acquire DHCP Server semaphore: %d\n", semstat);
		    return  generateResponse(&entry, rspMsg, "GetNetworkInformation", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
		}

		cJSON 				*pArr 		= cJSON_CreateArray();
		if(pArr)
		{
			for(iPass = 0 ; (iPass < NSOCKETS) ; iPass++)
			{
		        status = SCK_Protect_Socket_Block(iPass);
		        if (status != NU_SUCCESS)
		            continue; // go on to next socket

				//copy internal Nucleus structure; read copy only
				memcpy(&tempSockStruct, SCK_Sockets[iPass], sizeof(tempSockStruct));
			    SCK_Release_Socket();
				if((tempSockStruct.s_state == 0) && (tempSockStruct.s_accept_list != NULL))
				{
					// this is a listener socket
					cJSON  *eleMsg =  cJSON_CreateObject();
					if(eleMsg)
					{
						cJSON_AddNumberToObject(eleMsg, WS_KEY_SOCKET_ID, 		iPass);
						cJSON_AddStringToObject(eleMsg, WS_KEY_SOCKET_FAMILY, 	socketFamilyToString(tempSockStruct.s_family));
						cJSON_AddStringToObject(eleMsg, WS_KEY_SOCKET_PROTOCOL, socketProtocolToString(tempSockStruct.s_protocol));
						cJSON_AddStringToObject(eleMsg, WS_KEY_SOCKET_SRCADDR, 	ip_addr_to_str(tempSockStruct.s_local_addr.ip_num.is_ip_addrs, tempSockStruct.s_family, tempSockStruct.s_flags, tmpStr1));
						cJSON_AddStringToObject(eleMsg, WS_KEY_SOCKET_SRCPORT, 	ip_port_to_str(tempSockStruct.s_local_addr.port_num));
						cJSON_AddStringToObject(eleMsg, WS_KEY_SOCKET_STATE, 	socketStateToString(tempSockStruct.s_state));
						//
						cJSON_AddItemToArray(pArr, eleMsg);
					}
				}
			}

			cJSON_AddItemToObject(rspMsg, WS_KEY_SOCKET_LISTENERS, pArr) ;
		}

		pArr 		= cJSON_CreateArray();
		if(pArr)
		{
			for(iPass = 0 ; (iPass < NSOCKETS) ; iPass++)
			{
		        status = SCK_Protect_Socket_Block(iPass);
		        if (status != NU_SUCCESS)
		            continue; // go on to next socket

				//copy internal Nucleus structures; read copy only
				memcpy(&tempSockStruct, SCK_Sockets[iPass], sizeof(tempSockStruct));
				if(TCP_Ports[tempSockStruct.s_port_index] != 0)
				{
				    memcpy(&tempTCPStruct, TCP_Ports[tempSockStruct.s_port_index], sizeof(tempTCPStruct));
				    gotTCPInfo = TRUE;
				}
				else
				{
					gotTCPInfo = FALSE;
				}
			    SCK_Release_Socket();
				if(tempSockStruct.s_state != 0)
				{
					cJSON  *eleMsg =  cJSON_CreateObject();
					if(eleMsg)
					{
						cJSON_AddNumberToObject(eleMsg, WS_KEY_SOCKET_ID, iPass);
						cJSON_AddStringToObject(eleMsg, WS_KEY_SOCKET_SRCFAMILY,	socketFamilyToString(tempSockStruct.s_local_addr.family));
						cJSON_AddStringToObject(eleMsg, WS_KEY_SOCKET_SRCADDR, 		ip_addr_to_str(tempSockStruct.s_local_addr.ip_num.is_ip_addrs, tempSockStruct.s_family, tempSockStruct.s_flags, tmpStr1));
						cJSON_AddStringToObject(eleMsg, WS_KEY_SOCKET_SRCPORT, 		ip_port_to_str(tempSockStruct.s_local_addr.port_num));
						cJSON_AddStringToObject(eleMsg, WS_KEY_SOCKET_DSTFAMILY, 	socketFamilyToString(tempSockStruct.s_foreign_addr.family));
						cJSON_AddStringToObject(eleMsg, WS_KEY_SOCKET_DSTADDR, 		ip_addr_to_str(tempSockStruct.s_foreign_addr.ip_num.is_ip_addrs, tempSockStruct.s_family, tempSockStruct.s_flags, tmpStr1));
						cJSON_AddStringToObject(eleMsg, WS_KEY_SOCKET_DSTPORT, 		ip_port_to_str(tempSockStruct.s_foreign_addr.port_num));
						cJSON_AddStringToObject(eleMsg, WS_KEY_SOCKET_STATE, 		socketStateToString(tempSockStruct.s_state));

						if(gotTCPInfo)
						{
							UINT32 inCount 	= 0;
							UINT32 outCount = 0;

							if((tempTCPStruct.state != 0) && (tempTCPStruct.state != SS_NOFDREF))
							{
								inCount = tempTCPStruct.in.nxt;
								outCount = tempTCPStruct.out.nxt;
							}

							cJSON_AddNumberToObject(eleMsg, WS_KEY_PORT_OUTCOUNT, 	outCount);
							cJSON_AddNumberToObject(eleMsg, WS_KEY_PORT_INCOUNT, 	inCount);
							cJSON_AddStringToObject(eleMsg, WS_KEY_PORT_STATE, 		socketStateToString(tempTCPStruct.state));

						}
						//
						cJSON_AddItemToArray(pArr, eleMsg);
					}
				}
			}
			cJSON_AddItemToObject(rspMsg, WS_KEY_SOCKET_CONNECTIONS, pArr) ;
		} /* if(pArr) */

		cJSON_AddItemToObject(rspMsg, WS_KEY_SOCKET_INTERFACES, getAllLANConfigsAsJSONArray()) ;

		char *pSrc = (CMOSglobalNetworkInformationStructure.sourceFlag == NIS_SOURCE_WIRED) ? "Wired" : "WiFi";
		cJSON_AddStringToObject(rspMsg, WS_KEY_SOURCE, pSrc);

		char *pSrc2 = (CMOSglobalNetworkInformationStructure.dhcpFlag == NIS_DHCP_OFF) ? "Off" : "On";
		cJSON_AddStringToObject(rspMsg, WS_KEY_DHCP, pSrc2);

		char *pSrc3 = (CMOSglobalNetworkInformationStructure.ipv6Flag == NIS_IPV6_OFF) ? "Off" : "On";
		cJSON_AddStringToObject(rspMsg, WS_KEY_LAN_IPV6, pSrc3);

		char *pSrc4 = (CMOSglobalNetworkInformationStructure.eth0WsoxFlag == NIS_ETH0_WSOX_OFF) ? "Off" : "On";
		cJSON_AddStringToObject(rspMsg, WS_KEY_ETH0_WEBSOCKETS, pSrc4);

	    (void) OSReleaseSemaphore(ETH_DHCPS_SEM_ID);
	    generateResponse(&entry, rspMsg, "GetNetworkInformation", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);

	} /* if(rspMsg) */
}


// This code is not safe to use because it accesses Nucleus internal data which is volatile
#if 0
/* --------------------------------------------------------------------------------------------[printTCPPortInfo]-- */
#define NETWORK_PORT_NOTUSED		0xFFFFFFFF
//
static BOOL		localPortInfoFirstPass = TRUE ;
static UINT32	localLastNxt[2][TCP_MAX_PORTS];
void getTCPPortInfo(UINT portIdx, UINT32 *inCount, UINT32 *outCount, UINT8 *state)
{
	if(portIdx != NETWORK_PORT_NOTUSED)
	{
		struct _TCP_Port *prt = TCP_Ports[portIdx];
		// if this is the very first time we initialize our local array
		int iPass;
		if(localPortInfoFirstPass)
		{
			for(iPass=0 ; iPass < TCP_MAX_PORTS ; iPass++)
				localLastNxt[0][iPass] = localLastNxt[1][iPass] = NETWORK_PORT_NOTUSED;
			localPortInfoFirstPass = FALSE;
		}

		// first let's spin through all the ports first and update any that may have been discontinued.
		for(iPass=0 ; iPass < TCP_MAX_PORTS ; iPass++)
		{
			if(TCP_Ports[iPass])
			{
				if((TCP_Ports[iPass]->state == 0) || (TCP_Ports[iPass]->state == SS_NOFDREF))
					localLastNxt[0][iPass] = localLastNxt[1][iPass] = NETWORK_PORT_NOTUSED;
			}
		}

		// now for our specific port...
		if((localLastNxt[0][portIdx] == NETWORK_PORT_NOTUSED) || (localLastNxt[1][portIdx] == NETWORK_PORT_NOTUSED))
		{
			localLastNxt[0][portIdx] = (UINT32)prt->out.nxt;
			localLastNxt[1][portIdx] = (UINT32)prt->in.nxt;
		}

		*outCount 	= (UINT32)prt->out.nxt - localLastNxt[0][portIdx];
		*inCount  	= (UINT32)prt->in.nxt  - localLastNxt[1][portIdx];
		*state 		= prt->state ;
	}
	else
	{
		*state = 0 ;
		*outCount = 0 ;
		*inCount = 0 ;
	}
}
#endif

/* --------------------------------------------------------------------------------------------[socketProtocolToString]-- */
static char localProtocolTemp[10];
char *socketProtocolToString(INT16 proto)
{
	char *pRes = localProtocolTemp;

	switch(proto)
	{
		case NU_PROTO_INVALID:
			pRes = "INVALID ";
			break;

		case NU_PROTO_TCP:
			pRes = "   TCP  ";
			break;
		case NU_PROTO_UDP:
			pRes = "   UDP  ";
			break;
		case NU_PROTO_ICMP:
			pRes = "  ICMP  ";
			break;
		default:
			snprintf(pRes, 10,"UNK %4d", proto) ;
			break;
	}

	return pRes;
}


/* --------------------------------------------------------------------------------------------[socketStateToString]-- */
#define LOCAL_SKTSTATE_SIZE	15
static char localSocketStateTemp[LOCAL_SKTSTATE_SIZE];
char *socketStateToString(UINT16 state)
{
	char *pRes = localSocketStateTemp ;
	switch(state)
	{
		case 0:
			pRes = "NOT CONNECTED";
			break ;

		case SCLOSED:					// 1
			pRes = "   CLOSED    ";
			break ;

		case SLISTEN:					// 2
			pRes = "CONNECTED    ";
			break ;

		case SSYNR:						// 3
			pRes = "SYN RCVD     ";
			break ;

		case SSYNS:						// 4
			pRes = "CONNECTING   ";
			break ;

		case SEST:						// 5
			pRes = "ESTABLISHED  ";
			break ;


		case SFW1:						// 6
			pRes = "FIN WAIT 1   ";
			break ;

		case SFW2:						// 7
			pRes = "FIN WAIT 2   ";
			break ;

		case SCLOSING:					// 8
			pRes = "   CLOSING   ";
			break ;

		case STWAIT:					// 9
			pRes = "TIME WAIT    ";
			break ;

		case SS_DEVICEDOWN:				// 10
			pRes = "DEV DOWN     ";
			break ;

		case SLAST:						// 11
			pRes = "SLAST        ";
			break ;

		case SS_TIMEDOUT:				// 20
			pRes = "TIMED OUT    ";
			break ;

		case SS_WAITWINDOW:				// 40
			pRes = "WAIT WINDOW  ";
			break ;

		case SS_CANTRCVMORE:			// 80
			pRes = "RECV DOWN    ";
			break ;

		case SS_CANTWRTMORE:			// 100
			pRes = "WRITE DOWN   ";
			break ;

		default:
			snprintf(pRes, LOCAL_SKTSTATE_SIZE, "STATE %5d  ", state);
			break ;
	}
	return pRes;
}

/* --------------------------------------------------------------------------------------------[socketFamilyToString]-- */
static char localFamilyTemp[10];
char *socketFamilyToString(INT16 type)
{
	char *pRes = localFamilyTemp;
	switch(type)
	{
		case SK_FAM_UNSPEC:
			pRes = "UNSPEC ";
			break;

		case SK_FAM_LOCAL:
			pRes = " LOCAL ";
			break;

		case SK_FAM_IP:
			pRes = "  IP   ";
			break;

		case NU_FAMILY_IP6:
			pRes = " IPV6  ";
			break;

		case SK_FAM_ROUTE:
			pRes = " ROUTE ";
			break;

		case SK_FAM_LINK:
			pRes = "  LINK ";
			break;

		default:
			snprintf(pRes, 10, "UNK %3d", type);
			break;
	}
	return pRes;
}

/* --------------------------------------------------------------------------------------------[printSITARAMACAddresses]-- */


BOOL getSitaraMACAddress(UINT offset, MACBuf *macResult)
{
	BOOL bRes = FALSE ;
	if(offset >= 0 && offset <= 2)
	{
		macResult->u64Val = 0;
		// SITARA should have 3 MAC addresses, we want the middle and the last one (first is being used by the ethernet interface
		macResult->u16LVal = EndianSwap16((UINT16)(ESAL_GE_MEM_READ32(AM335X_CTRL_MODULE_BASE + AM335X_CTRL_MAC_ID0_LOW) & 0xFFFF));
		macResult->u32Val  = EndianSwap32(ESAL_GE_MEM_READ32(AM335X_CTRL_MODULE_BASE + AM335X_CTRL_MAC_ID0_HI));
		macResult->u16LVal += offset;
		bRes = TRUE;
	}

	return bRes ;
}

/* --------------------------------------------------------------------------------------------[updateNetworkMACAddsFromSITARA]-- */
/*
 * Take the assigned MAC addresses supplied by SITARA and plug them into our network stack
 * The 3 addresses are stored as a start and end number, eth0 takes the first so we need (first + 1) and (end)
 */
void updateNetworkMACAddsFromSITARA(BOOL bUpdateHardware)
{
	MACBuf tempMAC0, tempMAC1, tempMACEth  ;

	if(getSitaraMACAddress(MAC_ADDSOFFSET_ETH0, &tempMAC0))
	{
		debugDisplayOnly("\r\nMAC0 = %08X%04X\r\n",
				tempMAC0.u32Val, tempMAC0.u16LVal
		        );
	}
	if(getSitaraMACAddress(MAC_ADDSOFFSET_NET1, &tempMAC1))
	{
		debugDisplayOnly("\r\nMAC1 = %08X%04X\r\n",
				tempMAC1.u32Val, tempMAC1.u16LVal
		        );
	}

	// DUID is included in the Client ID portion of the DHCP discover message and is used by some DHCP servers
	// as a unique ID for the interface requesting an address. We need to update the nucleus value to make it
	// unique for us. This is done by adding our interface MAC address to the DUID and labeling it a
	// link-layer address.
	if(getSitaraMACAddress(MAC_ADDSOFFSET_ETH0, &tempMACEth))
	{
		DHCP_DUID_STRUCT duidTemp;
		STATUS status = NU_Get_DHCP_DUID(&duidTemp);
		if(status != NU_SUCCESS)
			dbgTrace(DBG_LVL_ERROR, "Error: DHCP DUID Data read status %d\r\n", status);

		debugDisplayOnly("DHCP DUID Data access %s [%d]\r\n", status == NU_SUCCESS ? "SUCCESSFULLY" : "FAILED!", status);

		duidTemp.duid_type = DHCP_DUID_LL;
		// update the dhcp client ID
		duidTemp.duid_ll_addr[0] = tempMACEth.bytes[5];
		duidTemp.duid_ll_addr[1] = tempMACEth.bytes[4];
		duidTemp.duid_ll_addr[2] = tempMACEth.bytes[3];
		duidTemp.duid_ll_addr[3] = tempMACEth.bytes[2];
		duidTemp.duid_ll_addr[4] = tempMACEth.bytes[1];
		duidTemp.duid_ll_addr[5] = tempMACEth.bytes[0];
		//
		status = NU_Set_DHCP_DUID(&duidTemp);
		if(status != NU_SUCCESS)
			dbgTrace(DBG_LVL_ERROR, "Error: DHCP DUID Data write status %d\r\n", status);

		debugDisplayOnly("DHCP DUID Data write %s [%d]\r\n", status == NU_SUCCESS ? "SUCCESSFULL" : "FAILED!", status);
	}

	if(bUpdateHardware)
	{
		// get (first + 1)
		tempMAC0.u64Val++;

		// Let's try assigning them....
		NU_IOCTL_OPTION ioctl_opt;
		STATUS      status = -1;

		// Update net0 with MAC0
		ioctl_opt.s_optval = (UINT8*)"net0";
		ioctl_opt.s_ret.mac_address[0] = tempMAC0.bytes[5];
		ioctl_opt.s_ret.mac_address[1] = tempMAC0.bytes[4];
		ioctl_opt.s_ret.mac_address[2] = tempMAC0.bytes[3];
		ioctl_opt.s_ret.mac_address[3] = tempMAC0.bytes[2];
		ioctl_opt.s_ret.mac_address[4] = tempMAC0.bytes[1];
		ioctl_opt.s_ret.mac_address[5] = tempMAC0.bytes[0];

		status = NU_Ethernet_Link_Down("net0");
		if (status != NU_SUCCESS)
			dbgTrace(DBG_LVL_ERROR, "Error: Unable to bring interface net0 down for MAC update: %d", status);

		status = NU_Ioctl(SIOCSIFHWADDR, &ioctl_opt, sizeof(ioctl_opt));
		if (status == NU_SUCCESS)
			dbgTrace(DBG_LVL_INFO,"net0 set to MAC %08X%04X\r\n", tempMAC0.u32Val, tempMAC0.u16LVal);
		else
			dbgTrace(DBG_LVL_ERROR,"Error transferring MAC to net0 - NOTE: Interface must be DOWN to update MAC value: [%d]", status);

		status = NU_Ethernet_Link_Up("net0");
		if (status != NU_SUCCESS)
			dbgTrace(DBG_LVL_ERROR, "Error: Unable to bring interface net0 up after MAC update: %d", status);

		// Update net1 with MAC1
		ioctl_opt.s_optval = (UINT8*)"net1";
		ioctl_opt.s_ret.mac_address[0] = tempMAC1.bytes[5];
		ioctl_opt.s_ret.mac_address[1] = tempMAC1.bytes[4];
		ioctl_opt.s_ret.mac_address[2] = tempMAC1.bytes[3];
		ioctl_opt.s_ret.mac_address[3] = tempMAC1.bytes[2];
		ioctl_opt.s_ret.mac_address[4] = tempMAC1.bytes[1];
		ioctl_opt.s_ret.mac_address[5] = tempMAC1.bytes[0];

		status = NU_Ethernet_Link_Down("net1");
		if (status != NU_SUCCESS)
			dbgTrace(DBG_LVL_ERROR, "Error: Unable to bring interface net1 down for MAC update: %d", status);

		status = NU_Ioctl(SIOCSIFHWADDR, &ioctl_opt, sizeof(ioctl_opt));
		if (status == NU_SUCCESS)
			dbgTrace(DBG_LVL_INFO,"net1 set to MAC %08X%04X\r\n", tempMAC1.u32Val, tempMAC1.u16LVal);
		else
			dbgTrace(DBG_LVL_ERROR,"Error transferring MAC to net1 - NOTE: Interface must be DOWN to update MAC value: [%d]", status);

		status = NU_Ethernet_Link_Up("net1");
		if (status != NU_SUCCESS)
			dbgTrace(DBG_LVL_ERROR, "Error: Unable to bring interface net1 up after MAC update: %d", status);

	}
}

/* ------------------------------------------------------------------------[get_local_ip_addr_str_from_wsox_handle]-- */
char *get_local_ip_addr_str_from_wsox_handle(uint32_t handle, char *ip_addr_str, char *default_ip_addr)
{
    struct addr_struct  local_addr;
    
    if (wsox_get_local_ip_addr_for_handle(handle, &local_addr))
    {
            return ip_addr_to_str(local_addr.id.is_ip_addrs, local_addr.family, IP_NOT_MAPPED, ip_addr_str);
    }

    /* if we got here, use the default */
    return default_ip_addr;
}


/* ------------------------------------------------------------------------[clearGlobalDeviceNameArray]-- */
VOID clearGlobalDeviceNameArray()
{
	INT iPass;
	for(iPass=0 ; iPass < DEVICE_NAME_ARRAY_MAX ; iPass++)
	{
		globalDeviceNameArray[iPass].id = DEVICE_ARRAY_NOTSET;
		globalDeviceNameArray[iPass].name[0] = 0 ;
	}
}

/* ------------------------------------------------------------------------[addToGlobalDeviceNameArray]-- */
STATUS addToGlobalDeviceNameArray(NET_IF_CHANGE_MSG *msg)
{
	INT iPass;
	STATUS status = NU_RETURN_ERROR;
	for(iPass=0 ; (iPass < DEVICE_NAME_ARRAY_MAX) &&
				  (globalDeviceNameArray[iPass].id != msg->dev_id) &&
				  (globalDeviceNameArray[iPass].id != DEVICE_ARRAY_NOTSET) ; iPass++) ; // no op
	//
	if(globalDeviceNameArray[iPass].id == DEVICE_ARRAY_NOTSET)
	{
		globalDeviceNameArray[iPass].id = msg->dev_id;
		strncpy(globalDeviceNameArray[iPass].name, msg->dev_name, 16);
		status = NU_SUCCESS;
	}
	return status;
}

/* ------------------------------------------------------------------------[getDeviceNameFromID]-- */
char *getDeviceNameFromID(DV_DEV_ID id)
{
	char *res = "";
	INT iPass;
	for(iPass=0 ; (iPass < DEVICE_NAME_ARRAY_MAX) && (globalDeviceNameArray[iPass].id != id) ; iPass++); // no op
	if(iPass < DEVICE_NAME_ARRAY_MAX)
		res = globalDeviceNameArray[iPass].name;

	return res;
}

/* ------------------------------------------------------------------------[on_SetNetworkParametersReq]-- */
// Platform task requirements...
static required_fields_tbl_t required_fields_tbl_SetNetworkInformationReq =
{
	"SetNetworkInformationReq",
	{
			"Source",
			0
	}
};

/* sample message
   {"MsgID":"SetNetworkInformationReq",
   "Source":"Wired",
   "DHCP":"Off",
   "IP":"1.5.8.4",
   "Netmask":"255.255.252.0",
   "Gateway":"250.123.45.15",
   "DNS":["8.8.8.8", "8.8.4.4"]}
*/

void on_SetNetworkInformationReq(UINT32 handle, cJSON *root)
{
	NetworkInformationStruct	nisTemp;
	BOOL						bHandled 	= FALSE ;
	BOOL						status;
	char 						strError[NIS_ERROR_LENGTH];
	cJSON						*rspMsg 	= cJSON_CreateObject();
    NETWORK_EVENT               netEvent;
    WSOX_Queue_Entry 			entry  		=  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    strError[0] = 0 ;

   memset(&nisTemp, 0, sizeof(NetworkInformationStruct));
    nisTemp.initialized = NIS_CMOS_INITIALIZED;

    if (CMOSglobalNetworkInformationStructure.initialized)
        nisTemp.eth0WsoxFlag = CMOSglobalNetworkInformationStructure.eth0WsoxFlag;

	if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_SetNetworkInformationReq))
	{
		// Undocumented back door
        cJSON *pEth0Websockets = cJSON_GetObjectItem(root, WS_KEY_ETH0_WEBSOCKETS);
        if (pEth0Websockets)
        {
            char *szValue = pEth0Websockets->valuestring;
            BOOL newFlagValue = (stricmp(szValue, WS_VALUE_ON) == 0);
            BOOL FlagChangeRequired = (nisTemp.eth0WsoxFlag != newFlagValue );

            if (FlagChangeRequired)
            {
            	CMOSglobalNetworkInformationStructure.eth0WsoxFlag = newFlagValue;

                stringToIP4Array(globalTransientDebugAddress.id.is_ip_addrs, (newFlagValue == 0) ? "192.168.10.110" : "0.0.0.0");
            }
        }

		// OK lets start with the routing field
    	char *sRouting 	= cJSON_GetObjectItem(root, WS_KEY_SOURCE)->valuestring;
    	if(strcmp(WS_ROUTING_WIFI, sRouting) == 0)
    	{
    		bHandled = TRUE ;
    		nisTemp.sourceFlag = NIS_SOURCE_WIFI;
			nisTemp.dhcpFlag = NIS_DHCP_OFF;  // No DHCP Server access if Wifi
    	}
    	else if(strcmp(WS_ROUTING_WIRED, sRouting) == 0)

    	{
    		bHandled = TRUE;
    		nisTemp.sourceFlag = NIS_SOURCE_WIRED;
    	}
    	else
    	{
    		// Error, unknown option supplied
    		snprintf(strError, NIS_ERROR_LENGTH, "Unknown source option %s for key %s supplied",sRouting, WS_KEY_SOURCE) ;
    		dbgTrace(DBG_LVL_ERROR, "Error: %s",strError);
    	}

    	// next step depends upon the routing field info so only proceed if no error....
    	if(bHandled)
    	{
    		if (nisTemp.sourceFlag == NIS_SOURCE_WIRED)
    		{
    			// use onboard NIC, so next question is 'is DHCP active'
				if(strcmp(safeCJSONGetString(root, WS_KEY_DHCP), WS_VALUE_ON) == 0)
				{
					nisTemp.dhcpFlag = NIS_DHCP_ACTIVE;
				}
				else
				{
					nisTemp.dhcpFlag = NIS_DHCP_OFF;
					// validate static IP fields
					stringToIP4Array( nisTemp.ipv4Address.ip4u8, safeCJSONGetString(root, WS_KEY_LAN_IP));
					stringToIP4Array( nisTemp.ipv4Netmask.ip4u8, safeCJSONGetString(root, WS_KEY_LAN_NETMASK));
					if(( nisTemp.ipv4Address.ip4U32 == 0) || ( nisTemp.ipv4Address.ip4U32 == 0xFFFFFFFF))
					{
						snprintf(strError, NIS_ERROR_LENGTH, "DHCP OFF - missing IP") ;
			    		dbgTrace(DBG_LVL_ERROR, "Error: %s: %d",strError, nisTemp.ipv4Address.ip4U32);
						bHandled = FALSE;
					}
					else if(nisTemp.ipv4Netmask.ip4U32 == 0)
					{
						snprintf(strError, NIS_ERROR_LENGTH, "DHCP OFF - missing Netmask") ;
			    		dbgTrace(DBG_LVL_ERROR, "Error: %s: %d",strError, nisTemp.ipv4Netmask.ip4U32);
						bHandled = FALSE;
					}
				}
    		}

    		// Need gateway & DNS with Wifi or Wired/StaticIP
    		if ((nisTemp.sourceFlag == NIS_SOURCE_WIFI) || ((nisTemp.sourceFlag == NIS_SOURCE_WIRED) && (nisTemp.dhcpFlag == NIS_DHCP_OFF)))
    		{
    			stringToIP4Array( nisTemp.ipv4Gateway.ip4u8, safeCJSONGetString(root, WS_KEY_LAN_GATEWAY)) ;
    			//barest gateway validation
				if(nisTemp.ipv4Gateway.ip4U32 == 0)
				{
					snprintf(strError, NIS_ERROR_LENGTH, "Invalid Gateway") ;
		    		dbgTrace(DBG_LVL_ERROR, "Error: %s: %d",strError, nisTemp.ipv4Gateway.ip4U32);
					bHandled = FALSE;
				}

				// handle DNS array...
				cJSON *pArr = cJSON_GetObjectItem(root, WS_KEY_DNS);
				INT iCount = 0;
				if(pArr && ((iCount = cJSON_GetArraySize(pArr)) > 0))
				{
					INT iPass ;
					for(iPass=0 ; iPass < MIN(iCount, DEVICE_DNS_ARRAY_MAX) ; iPass++)
					{
						stringToIP4Array( nisTemp.ipv4DNS[iPass].ip4u8, cJSON_GetArrayItem(pArr, iPass)->valuestring);
					}
					//validate the first DNS address, others from Wifi router may be 0
					if(nisTemp.ipv4DNS[0].ip4U32 == 0)
					{
						snprintf(strError, NIS_ERROR_LENGTH, "Invalid Primary DNS Address") ;
			    		dbgTrace(DBG_LVL_ERROR, "Error: %s: %d",strError, nisTemp.ipv4DNS[0].ip4U32);
						bHandled = FALSE;
					}
				}
				else
				{
					snprintf(strError, NIS_ERROR_LENGTH, "Missing DNS information") ;
		    		dbgTrace(DBG_LVL_ERROR, "Error: %s",strError);
					bHandled = FALSE ;
				}
    		}
		}
	}
	else
	{
		// required fields omitted
		snprintf(strError, NIS_ERROR_LENGTH, "Required fields omitted from command");
		dbgTrace(DBG_LVL_ERROR, "Error: %s",strError);
	}

	if(!bHandled)
	{
		cJSON_AddStringToObject(rspMsg, "Error", strError);
	    generateResponse(&entry, rspMsg, "SetNetworkInformation", api_status_to_string_tbl[API_STATUS_NOT_ACCEPTED], root);
	}
	else
	{
		// Find out what changed
		netEvent = getNetworkEventFromConfigChange(&CMOSglobalNetworkInformationStructure, &nisTemp);

		// Process event and return error string if there is any failure
		// Also store new configuration in NVRAM
		status = handleNetEvent(netEvent, &nisTemp, strError, NIS_ERROR_LENGTH);
		if (status == SUCCESSFUL)
		{
		    generateResponse(&entry, rspMsg, "SetNetworkInformation", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
		}
		else
		{// New config data is not stored
			cJSON_AddStringToObject(rspMsg, "Error", strError);
		    generateResponse(&entry, rspMsg, "SetNetworkInformation", api_status_to_string_tbl[API_STATUS_NOT_ACCEPTED], root);
    		dbgTrace(DBG_LVL_ERROR, "Error: Networking event handler response: %d, %s",status, strError);
		}
	}

	debugDisplayNetworkInformationStructure(&CMOSglobalNetworkInformationStructure);
}


/* ------------------------------------------------------------------------[handleDHCPWithRetries]-- */
// to handle the duplicate IP retry possibility and issue event messages to the tablet to assist
// in tracking the process.
// This acquires a semaphore so do not return without releasing semaphore first
STATUS handleDHCPWithRetries()
{
	STATUS status = NU_SUCCESS;
	char	semstat;
    char    ip_addr_str[IP_ADDR_STR_LEN];
    char    ip_addr_str2[IP_ADDR_STR_LEN];
    char    ip_addr_str3[IP_ADDR_STR_LEN];

	clearDHCPRetryCount();

    semstat = OSAcquireSemaphore (ETH_DHCPS_SEM_ID, 100);
    if (semstat != SUCCESSFUL)
    {// cannot acquire semaphore even after waiting 100 ms - can't do this
        dbgTrace(DBG_LVL_ERROR,"Error: handleDHCPWithRetries failed to acquire DHCP Server semaphore: %d\n", semstat);
        return NU_UNAVAILABLE;
    }

	while (getDHCPRetryCount() < DHCP_RETRY_LIMIT)
	{
		dbgTrace(DBG_LVL_INFO, "DHCP start params: dv_name %s, file %s, giaddr %s, siaddr %s, yiaddr %s, ",
				ptrGlobalEth0DhcpStruct->dhcp_dv_name,
				ptrGlobalEth0DhcpStruct->dhcp_file,
				ip_addr_to_str(ptrGlobalEth0DhcpStruct->dhcp_giaddr, NU_FAMILY_IP, IP_NOT_MAPPED, ip_addr_str),
				ip_addr_to_str(ptrGlobalEth0DhcpStruct->dhcp_siaddr, NU_FAMILY_IP, IP_NOT_MAPPED, ip_addr_str2),
				ip_addr_to_str(ptrGlobalEth0DhcpStruct->dhcp_yiaddr, NU_FAMILY_IP, IP_NOT_MAPPED, ip_addr_str3)
				);
		status = NU_Dhcp(ptrGlobalEth0DhcpStruct, DEFAULT_IF);
		dbgTrace(DBG_LVL_INFO, "DHCP start exit");
		if(status == NU_SUCCESS)
		{
			dbgTrace(DBG_LVL_INFO, "Enabled DHCP on interface %s\r\n", DEFAULT_IF) ;
			clearDHCPRetryCount();
			break;
		}
		else
		{
			if(status == NU_INVALID_ADDRESS)
			{
				// if ip is in use then retry hoping for a new address
				dbgTrace(DBG_LVL_INFO, "ERROR Assigned DHCP address is already in use on this segment! Retry %d\r\n", getDHCPRetryCount());
				incrementDHCPRetryCount();
				// send an event message to let the tablet know there is an issue....
				// add the current retry state count.
				char strErr[100];
				snprintf(strErr, 100, "DHCP Duplicate Address Retry %d of %d", getDHCPRetryCount(), DHCP_RETRY_LIMIT);
		        cJSON *rspMsg = cJSON_CreateObject();
		        cJSON_AddStringToObject(rspMsg, "Error", strErr);
				SendBaseEventToTablet(BASE_EVENT_DHCP_DUPLICATE_RETRY, rspMsg);

			}
			else if((status == NU_DHCP_REQUEST_FAILED) || (status == NU_DHCP_INIT_FAILED))
			{
				// Not sure what the error is but likely to be unable to get in touch with the dhcp server, so retry...
				// we also cover the init failed issue since this can also come from the dhcp routine for various reasons.
				dbgTrace(DBG_LVL_INFO, "ERROR DHCP Process failed, resetting interface - Retry %d\r\n", getDHCPRetryCount());
				incrementDHCPRetryCount();
				/* AB
				NU_Ethernet_Link_Down(DEFAULT_IF);
				NU_Sleep(100);
				NU_Ethernet_Link_Up(DEFAULT_IF);
				*/
				// send an event message to let the tablet know there is an issue....
				// add the current retry state count.
				char strErr[100];
				snprintf(strErr, 100, "ERROR DHCP Process failed, resetting interface - Retrying %d of %d", getDHCPRetryCount(), DHCP_RETRY_LIMIT);
		        cJSON *rspMsg = cJSON_CreateObject();
		        cJSON_AddStringToObject(rspMsg, "Error", strErr);
				SendBaseEventToTablet(BASE_EVENT_DHCP_ERROR, rspMsg);

			}
			else
			{
				dbgTrace(DBG_LVL_INFO, "ERROR Unable to activate DHCP on interface %s [%d]\r\n", DEFAULT_IF, status);
				clearDHCPRetryCount() ;
		        cJSON *rspMsg = cJSON_CreateObject();
		        cJSON_AddStringToObject(rspMsg, "Error", "Unable to activate DHCP on this interface");
				SendBaseEventToTablet(BASE_EVENT_DHCP_ERROR, rspMsg);
				/* AB
				NU_Ethernet_Link_Down(DEFAULT_IF);
				NU_Sleep(100);
				NU_Ethernet_Link_Up(DEFAULT_IF);
				*/
				break ;
			}
		}
	}
	// if the DHCP retry limit is exceeded we need to log the final error and issue the event to the tablet.
	if(getDHCPRetryCount() >= DHCP_RETRY_LIMIT)
	{
		dbgTrace(DBG_LVL_INFO, "ERROR DHCP Server unable to assign unique IP address in %d tries\r\n", DHCP_RETRY_LIMIT);
		char strErr[100];
		snprintf(strErr, 100, "DHCP Server unable to assign unique IP address in %d tries", DHCP_RETRY_LIMIT);
        cJSON *rspMsg = cJSON_CreateObject();
        cJSON_AddStringToObject(rspMsg, "Error", strErr);
		SendBaseEventToTablet(BASE_EVENT_DHCP_ERROR, rspMsg);
	}

    (void) OSReleaseSemaphore(ETH_DHCPS_SEM_ID);
    return status;
}

// Delete DNS entries if any
STATUS deleteAllBaseDNSEntries(void)
{
	char	strTmp[50];
	INT iCount = 0;
	UINT8 dns[DEVICE_NU_DNS_ARRAY_MAX * IP4_BYTESIZE];
	STATUS status = NU_SUCCESS;
	INT iIter;

	iCount = NU_Get_DNS_Servers(dns, DEVICE_NU_DNS_ARRAY_MAX * IP4_BYTESIZE);
	if (iCount < 0)
	{
    	dbgTrace(DBG_LVL_ERROR, "Error: NU_Get_DNS_Servers failed: %d", iCount);
		return iCount;
	}

	if (iCount == 0)
	{
    	dbgTrace(DBG_LVL_INFO, "NU_Get_DNS_Servers no DNS servers to delete");
		return status;
	}

	// delete the servers
	for (iIter=0 ; iIter < iCount ; iIter++)
	{
		UINT32 u32Swap = EndianSwap32(*(UINT32 *)&dns[iIter * IP4_BYTESIZE]);
		status = NU_Delete_DNS_Server((UINT8 *)&u32Swap);
		if (status != NU_SUCCESS)
		{
			dbgTrace(DBG_LVL_ERROR, "Error: Unable to remove DNS [%s]",
					IP4ArrayToString((UINT8 *)&u32Swap, strTmp, 50, FALSE));
			break; // get out and return error
		}
		else
		{
			dbgTrace(DBG_LVL_INFO, "Removed DNS [%s]",
					IP4ArrayToString((UINT8 *)&u32Swap, strTmp, 50, FALSE));
		}
	}

	return status;
}


/* ------------------------------------------------------------------------[safeCJSONGetString]-- */
char *safeCJSONGetString(cJSON *root, char *key)
{
	if(cJSON_HasObjectItem(root, key) == 1)
		return  cJSON_GetObjectItem(root, key)->valuestring;
	return "";
}

/* ------------------------------------------------------------------------[stringToIP4Array]-- */
STATUS stringToIP4Array(UINT8 *array, char *str)
{
	STATUS status = NU_RETURN_ERROR;
	// expecting string like "1.168.2.40"
	int iPass ;
	for(iPass=0 ; str && *str && (iPass < 4) ; iPass++)
	{
		array[iPass] = atoi(str);
		str = skipToChar(str, IP4_DELIMITER_CHAR);
		// if on '.' char then shift one more char...
		if(*str)
			str++;
	}
	if(iPass == 4)
		status = NU_SUCCESS;

	return status;
}

/* ------------------------------------------------------------------------[IP4ArrayToString]-- */
char	*IP4ArrayToString(UINT8 *array, char *str, size_t sz, BOOL bReverse)
{
	if(bReverse)
		snprintf(str, sz, "%d.%d.%d.%d", array[3], array[2], array[1], array[0]);
	else
		snprintf(str, sz, "%d.%d.%d.%d", array[0], array[1], array[2], array[3]);
	return str;
}


/* ------------------------------------------------------------------------[debugDisplayNetworkInformationStructure]-- */
void debugDisplayNetworkInformationStructure(NetworkInformationStruct *nis)
{
	char strIP[50];
	INT iPass;

	debugDisplayOnly("NetworkInformationStructure content (%ld  bytes):\r\n", sizeof(NetworkInformationStruct)) ;

	debugDisplayOnly("Network Source %s  --  ", nis->sourceFlag == 1 ? "TABLET" : "WIRED ETHERNET");
    debugDisplayOnly("DHCP %s  --  ", nis->dhcpFlag == 1 ? "ACTIVE" : "INACTIVE");
    debugDisplayOnly("Eth0 Websockets %s\r\n", nis->eth0WsoxFlag == 1 ? "ACTIVE" : "INACTIVE");
	debugDisplayOnly("IP       - %s\r\n", isNULLIPv4Address(nis->ipv4Address.ip4u8) ? "NOT SET" : IP4ArrayToString((UINT8 *)(nis->ipv4Address.ip4u8), strIP, 50, FALSE));
	debugDisplayOnly("Netmask  - %s\r\n", isNULLIPv4Address(nis->ipv4Netmask.ip4u8) ? "NOT SET" : IP4ArrayToString((UINT8 *)(nis->ipv4Netmask.ip4u8), strIP, 50, FALSE));
	debugDisplayOnly("Gateway  - %s\r\n", isNULLIPv4Address(nis->ipv4Gateway.ip4u8) ? "NOT SET" : IP4ArrayToString((UINT8 *)(nis->ipv4Gateway.ip4u8), strIP, 50, FALSE));

	for(iPass=0 ; iPass < 4 ; iPass++)
	{
		debugDisplayOnly(" DNS%d    - %s\r\n", iPass, isNULLIPv4Address(nis->ipv4DNS[iPass].ip4u8) ? "NOT SET" : IP4ArrayToString((UINT8 *)(nis->ipv4DNS[iPass].ip4u8), strIP, 50, FALSE));
	}
}

/* ------------------------------------------------------------------------[debugDisplayIP4DNSServers]-- */
void debugDisplayIP4DNSServers()
{
	static size_t sz = 4 * 6;
	UINT8 dest[sz];
	INT		iPass;
	char	strTmp[50];

	INT iCount = NU_Get_DNS_Servers(dest, sz);
	iCount = MIN(iCount, 6);
	if(iCount > 0)
	{
		for(iPass=0 ; iPass < iCount ; iPass++)
		{
			if(iPass == 0)
			{
				debugDisplayOnly("    %d DNS Servers detected -  DNS%d = %s\r\n", iCount, iPass, IP4ArrayToString(&dest[iPass*4], strTmp, 50, TRUE));
			}
			else
				debugDisplayOnly("                              DNS%d = %s\r\n", iPass, IP4ArrayToString(&dest[iPass*4], strTmp, 50, TRUE));
		}
	}
	else
	{
		debugDisplayOnly("\r\n            No DNS Servers detected\r\n") ;
	}
}


/* ------------------------------------------------------------------------[on_ConnectivityReq]-- */
void on_ConnectivityReq(UINT32 handle, cJSON *root)
{
    cJSON 				*rspMsg		=  cJSON_CreateObject();
    WSOX_Queue_Entry 	entry  		=  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    cJSON_AddStringToObject(rspMsg, "MsgID", "ConnectivityRsp");
	char *strDest = safeCJSONGetString(root, WS_KEY_CONNECT_DEST);
	testSocketConnect(strDest, rspMsg);

	addEntryToTxQueue(&entry, root, "  on_ConnectivityReq: Added response to tx queue.\r\n");
}


/* ------------------------------------------------------------------------[on_GetSocketInformationReq]-- */
#define GSI_ERROR_LENGTH	100

void on_GetSocketInformationReq(UINT32 handle, cJSON *root)
{
    cJSON 				*rspMsg		=  cJSON_CreateObject();
    WSOX_Queue_Entry 	entry  		=  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
	INT					iPass;
	char				tmpStr1[GSI_ERROR_LENGTH];
	cJSON				*array = NULL;
	//TODO - With new Nucleus release, confirm access of internal structures is still valid and check whether it can be replaced with API call
	struct sock_struct  tempSockStruct;
	struct _TCP_Port    tempTCPStruct;
	BOOL                gotTCPInfo = FALSE;
    STATUS              status;

		for(iPass = 0 ; (iPass < NSOCKETS) ; iPass++)
		{
			// first step is to spin through the listeners...

	        status = SCK_Protect_Socket_Block(iPass);
	        if (status != NU_SUCCESS)
	            continue; // go on to next socket

			//copy internal Nucleus structure; read copy only
			memcpy(&tempSockStruct, SCK_Sockets[iPass], sizeof(tempSockStruct));
			if(TCP_Ports[tempSockStruct.s_port_index] != 0)
			{
			    memcpy(&tempTCPStruct, TCP_Ports[tempSockStruct.s_port_index], sizeof(tempTCPStruct));
			    gotTCPInfo = TRUE;
			}
			else
			{
				gotTCPInfo = FALSE;
			}
		    SCK_Release_Socket();

			if(isNULLIPv4Address(tempSockStruct.s_foreign_addr.ip_num.is_ip_addrs) &&
					tempSockStruct.s_state == 0)
			{
				if(array == NULL)
				{
					array = cJSON_CreateArray() ;
				}
				cJSON *element = cJSON_CreateObject() ;
				cJSON_AddNumberToObject(element, WS_KEY_SOCKET_ID, iPass) ;
				cJSON_AddStringToObject(element, WS_KEY_SOCKET_FAMILY, socketFamilyToString(tempSockStruct.s_family)) ;
				cJSON_AddStringToObject(element, WS_KEY_SOCKET_PROTOCOL, socketProtocolToString(tempSockStruct.s_protocol)) ;
				cJSON_AddStringToObject(element, WS_KEY_SOCKET_SRCADDR, ip_addr_to_str(tempSockStruct.s_local_addr.ip_num.is_ip_addrs, tempSockStruct.s_family, tempSockStruct.s_flags, tmpStr1)) ;
				cJSON_AddStringToObject(element, WS_KEY_SOCKET_SRCPORT, ip_port_to_str(tempSockStruct.s_local_addr.port_num)) ;
				cJSON_AddStringToObject(element, WS_KEY_SOCKET_STATE, socketStateToString(tempSockStruct.s_state)) ;
				// add associated port information..
				if(gotTCPInfo)
				{
					UINT32 inCount = 0;
					UINT32 outCount = 0;

					if((tempTCPStruct.state != 0) && (tempTCPStruct.state != SS_NOFDREF))
					{
						inCount = tempTCPStruct.in.nxt;
						outCount = tempTCPStruct.out.nxt;
					}

					cJSON_AddNumberToObject(element, WS_KEY_PORT_INCOUNT, inCount) ;
					cJSON_AddNumberToObject(element, WS_KEY_PORT_OUTCOUNT, outCount) ;
					cJSON_AddStringToObject(element, WS_KEY_PORT_STATE, socketStateToString(tempTCPStruct.state)) ;
				}
				//
				cJSON_AddItemToArray(array, element) ;
				element = NULL ;
			}
		} // end of lister iteration
		cJSON_AddItemToObject(rspMsg,WS_KEY_LISTENERS, array) ;
		array = NULL ;

		// now run through the connections...
		for(iPass = 0 ; (iPass < NSOCKETS) ; iPass++)
		{
	        status = SCK_Protect_Socket_Block(iPass);
	        if (status != NU_SUCCESS)
	            continue; // go on to next socket

			//copy internal Nucleus structure; read copy only
			memcpy(&tempSockStruct, SCK_Sockets[iPass], sizeof(tempSockStruct));
			if(TCP_Ports[tempSockStruct.s_port_index] != 0)
			{
			    memcpy(&tempTCPStruct, TCP_Ports[tempSockStruct.s_port_index], sizeof(tempTCPStruct));
			    gotTCPInfo = TRUE;
			}
			else
			{
				gotTCPInfo = FALSE;
			}
		    SCK_Release_Socket();

			if(tempSockStruct.s_state != 0)
			{
				if(array == NULL)
				{
					array = cJSON_CreateArray() ;
				}
				cJSON *element = cJSON_CreateObject() ;
				cJSON_AddNumberToObject(element, WS_KEY_SOCKET_ID, iPass) ;
				cJSON_AddStringToObject(element, WS_KEY_SOCKET_SRCFAMILY, socketFamilyToString(tempSockStruct.s_family)) ;
				cJSON_AddStringToObject(element, WS_KEY_SOCKET_SRCADDR, ip_addr_to_str(tempSockStruct.s_local_addr.ip_num.is_ip_addrs, tempSockStruct.s_family, tempSockStruct.s_flags, tmpStr1)) ;
				cJSON_AddStringToObject(element, WS_KEY_SOCKET_SRCPORT, ip_port_to_str(tempSockStruct.s_local_addr.port_num)) ;

				cJSON_AddStringToObject(element, WS_KEY_SOCKET_DSTADDR, ip_addr_to_str(tempSockStruct.s_foreign_addr.ip_num.is_ip_addrs, tempSockStruct.s_family, tempSockStruct.s_flags, tmpStr1)) ;
				cJSON_AddStringToObject(element, WS_KEY_SOCKET_DSTPORT, ip_port_to_str(tempSockStruct.s_foreign_addr.port_num)) ;
				cJSON_AddStringToObject(element, WS_KEY_SOCKET_STATE, socketStateToString(tempSockStruct.s_state)) ;

				if(gotTCPInfo)
				{
					UINT32 inCount = 0;
					UINT32 outCount = 0;

					if((tempTCPStruct.state != 0) && (tempTCPStruct.state != SS_NOFDREF))
					{
						inCount = tempTCPStruct.in.nxt;
						outCount = tempTCPStruct.out.nxt;
					}

					cJSON_AddNumberToObject(element, WS_KEY_PORT_INCOUNT, inCount) ;
					cJSON_AddNumberToObject(element, WS_KEY_PORT_OUTCOUNT, outCount) ;
					cJSON_AddStringToObject(element, WS_KEY_PORT_STATE, socketStateToString(tempTCPStruct.state)) ;
				}

				cJSON_AddItemToArray(array, element) ;
				element = NULL ;

			}
		} // end of connections iteration
		cJSON_AddItemToObject(rspMsg,WS_KEY_CONNECTIONS, array) ;
		array = NULL ;

		return  generateResponse(&entry, rspMsg, "GetSocketInformation", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);

}


/* ------------------------------------------------------------------------[NATStateToChar]-- */
// translate the NAT State value to human readable text
char localResult[12];
char *NATStateToString(INT16 iState)
{
	char *strResult = "UNKNOWN ";
	switch(iState)
	{
		case SCLOSED:
			strResult = "SCLOSED ";
			break;

		case SLISTEN:
			strResult = "SLISTEN ";
			break;

		case SSYNR:
			strResult = " SSYNR  ";
			break;

		case SSYNS:
			strResult = " SSYNS  ";
			break;

		case SEST:
			strResult = "  SEST  ";
			break;

		case SCLOSING:
			strResult = "SCLOSING";
			break;

		case SFW1:
			strResult = "  SFW1  ";
			break;

		case SFW2:
			strResult = "  SFW2  ";
			break;

		default:
			snprintf(localResult, 12, "  %3d   ", iState);
			strResult = localResult;
			break;

	}

	return strResult ;
}

