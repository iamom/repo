/*
*********************************************************************
*********************************************************************
CLASSIFICATION:                        PITNEY BOWES RESTRICTED
IDENTITY:                              
TITLE:                                 logging.c
COPYRIGHT PITNEY BOWES LIMITED 2005
REQUIREMENT:                           
HISTORY:
         DATE      ISSUE      AUTHOR         PR     SYNOPSIS

      
      INTERFACE:

        CLASSES:
                

    DESCRIPTION: 

        

------------------------------------------------------------------------
*/ 

#define __LOGGING_C__

#include <stdio.h>
#include "nucleus.h"
#include "externs.h"
#include "target.h"
#include "kernel/nu_kernel.h"
#include "connectivity/ctsystem.h"	// for min definition.
#include "logging.h"
#include "pbos.h"

//#include "am335x_timer.h"
                
//uint32_t GetSystemTime()
//{
//    uint64_t hw_tick = NU_Get_Time_Stamp();  /* There are NU_HW_Ticks_Per_Second */
//    /* convert to #mSec ticks */
//    return (hw_tick /100) * NU_HW_Ticks_Per_Second / 10000;  
//}
/* -------------------------------------------------------------[ Wait ]-- */

void Wait(unsigned long MilliSeconds)
{
  uint32_t start_tick = GetSystemTime();
    uint32_t end_tick;
    UNSIGNED sw_ticks = MilliSeconds * 1000 / NU_PLUS_TICKS_PER_SEC;

    NU_Sleep(sw_ticks-1);  /* do the bulk of the wait as a sleep (normally a sw_tick is 10 mSec) */

    do
    {
        end_tick = GetSystemTime();
    } while(end_tick - start_tick < MilliSeconds);

}

bool is_protocol(uint8_t port, protocol_t protocol)
{
    UNUSED_PARAMETER(port);
    UNUSED_PARAMETER(protocol);
    return true;
}

uint8_t TXBuf(const char *str, uint8_t port, size_t len, uint16_t *bytes_txed)
{
    dbgTrace(DBG_LVL_INFO, str);
    *bytes_txed = (uint16_t) strlen(str);
    return Ser_NoError;
}

void tx_str(const char *str)
{
	uint16_t bytes_txed;
	/* if we add new bytes too quickly they will be lost */
	if (is_protocol(PC_PORT, PROT_ASCII))
	   TXBuf(str, PC_PORT, strlen(str), &bytes_txed);
}   

void tx_buf(uint8_t *buf, uint16_t len)
{
   uint16_t bytes_txed;
   /* if we add new bytes too quickly they will be lost */
   if (is_protocol(PC_PORT, PROT_ASCII))
      TXBuf((const char *)buf, PC_PORT, len, &bytes_txed);
}   

void term_tx_buf(const char * str, uint16_t str_len)
{
	uint16_t bytes_txed = 0;

	if (is_protocol(PC_PORT, PROT_ASCII))
   {
	   while(str_len)
	   {
         if (Ser_NoError == TXBuf(str, PC_PORT, str_len, &bytes_txed))
         {
            str += bytes_txed;
            str_len -= bytes_txed;
         }
	   }
   }
}	

void term_tx_str(const char * str)
{
   term_tx_buf(str, strlen(str));

}	

#define TX_VT100(x) (term_tx_buf(x, sizeof(x)-1))

void term_save_cursor(void)
{
	 
    TX_VT100("\x1B[s");
}

void term_restore_cursor(void)
{
    TX_VT100("\x1B[u");
}

void term_goto_xy(int row, int col)
{
    char msg[10];
	 int str_len;
    str_len = sprintf(msg, "\x1B[%d;%df", row, col);
    term_tx_buf(msg, str_len);
}

void term_update_status(const char *status)
{
    TX_VT100("\x1B[1$");
    tx_str(status);
    TX_VT100("\x1B[0$");
}

void term_erase_screen(void)
{
    TX_VT100("\x1B[2J");
}

void term_config(void)
{
    //tx_str("\x1B""c");  /* reset terminal */
    //term_erase_screen();
    //tx_str("\x1B[?6l"); /* allow cursor to go outside scroll region */
    //tx_str("\x1B[45t");  /* 45 lines per page */
    //tx_str("\x1B[3;42r"); /* scroll area from row 3 to line 42 */


#if 0
    tx_str("\x1B""c\x1Br\x1b[2J\x1b[?6l");
    tx_str("\x1B[25t");  /* 45 lines per page */
    tx_str("\x1B[2;20r");

    tx_str("Started");

    term_goto_xy(3, 1); /* begin of scroll area */
    tx_str("Terminal configured\n\r");
#endif

    TX_VT100("\x1B""c\x1Br\x1b[2J\x1b[?6l\x1B[2$~");
    Wait(500); /* give the terminal a change to reconfigure */
    tx_str("Started\n\r");

}

static uint8_t nibble_to_ascii(uint8_t nibble)
{
   nibble &= 0x0F;   /* make sure we only use lower 4 bits */

   nibble += '0';
   if (nibble > '9') /* must have been A-F */
   {
      nibble += 'A' - '9' - 1; /* adjust to 'A' - 'F' */
   }
   return nibble;
}   

void term_hexdump(const uint8_t *p_data, uint16_t data_len)
{
    char s_line[20];
    uint16_t num_of_bytes_to_convert;
    uint8_t  i;

    while (data_len>0)
    {
      num_of_bytes_to_convert = min(data_len, sizeof(s_line) / 2);
      for (i=0; i<num_of_bytes_to_convert; i++)
      {
         s_line[i*2]   = nibble_to_ascii(*p_data >> 4);
         s_line[i*2+1] = nibble_to_ascii(*p_data & 0x0F);
         p_data++;
      }
      term_tx_buf(s_line, num_of_bytes_to_convert * 2);
      data_len -= num_of_bytes_to_convert;
    }
}

void term_log(const char *entry)
{
   char time[20];
	uint16_t len;
   len = sprintf(time, "\r\n%6.6ld ", GetSystemTime() /* % 1000000 */);
   tx_buf((uint8_t *)time, len);
   tx_str(entry);
}   


/* Storage space for the pertinent values of __FILE__ and __LINE__ */
static const char  *dbg_file;
static int   dbg_line;

/***********************************************************************
 **  FUNCTION:   dbg_info
 **
 **  DESCRIPTION
 **  Assigns <line> and <file> to their global counterparts.  Returns a 
 **  pointer to function dispdbginfo().
 **********************************************************************/
void (*dbg_info(int line, const char *file))(const char *, ...)
{
    
    /* locate the start of base name by looking for path seperator from the end */
    dbg_file = strrchr(file, '\\');  
    if (dbg_file == NULL)
    {
       dbg_file = file; /* there was no path seperator */
    }
    else
    {
      dbg_file++; /* skip one past the path seperator */
   }
    dbg_line = line;
    return disp_dbg_info;
}

/***********************************************************************
 **  FUNCTION:   no_dbg_info
 **
 **  DESCRIPTION
 **  Assigns <line> and <file> to their global counterparts.  Returns a 
 **  pointer to function dispdbginfo().
 **********************************************************************/
void (*no_dbg_info(int line, const char *file))(const char *, ...)
{
    
    UNUSED_PARAMETER(line);
    UNUSED_PARAMETER(file);

    return no_disp_dbg_info;
}


/***********************************************************************
 **  FUNCTION:   disp_dbg_info
 **
 **  DESCRIPTION
 **  fault() converts and writes output to DebugPort, under the control
 **  of <fmt>.
 **********************************************************************/
void disp_dbg_info(const char *fmt, ...)
{

    /* Bounce out of the function if debugging not requested */
#ifdef NDEBUG
    return;
#else
    char errBuf[256];
    va_list argp;

    sprintf(errBuf, "dbg: %s[%d]: ", dbg_file, dbg_line);
    term_log(errBuf);
    va_start(argp, fmt);
    vsprintf(errBuf, fmt, argp);
    term_tx_str(errBuf);
    va_end(argp);
#endif
}

void no_disp_dbg_info(const char *fmt, ...)
{
    UNUSED_PARAMETER(fmt);
    /* Bounce out of the function since debugging not requested */
    return;
}

/* ----------------------------------------------[ State change logger ]-- */

#ifdef STATE_MACHINE_LOGGING

#define EXTRA_DATA_SIZE 4

typedef struct __attribute__ ((__packed__))
{
   unsigned long   tick_count;
   unsigned int    rep_count;
   state_machine_e state_machine;
   unsigned char   new_state;
   unsigned char   extra_data[EXTRA_DATA_SIZE];
} state_log_entry_t;

#define MAX_STATE_LOG_ENTRIES 1600
typedef struct __attribute__ ((__packed__))
{
   unsigned int log_entries;
   unsigned int next_entry;
   state_log_entry_t log[MAX_STATE_LOG_ENTRIES];
} state_log_t;

state_log_t state_log;
static state_log_entry_t *p_prev = &state_log.log[0];

#if 0
static int my_memcmp(unsigned char *p1, unsigned char *p2, unsigned int len)
{
   int rv = 0;
   while ( len && !(rv = *p1++ - *p2++))
   {
      len--;
   }
   return rv;
}
#endif

static void my_memcpy(unsigned char *dest, unsigned char *src, unsigned int len)
{
   while (len)
   {
      *dest++ = *src++;
      len--;
   }
}

void log_state_change(state_machine_e state_machine, volatile const unsigned char *p_data, unsigned int data_len)
{
   if (data_len > EXTRA_DATA_SIZE+1)
   {
      data_len = EXTRA_DATA_SIZE+1;
   }

   if (p_prev->state_machine == state_machine && 
       memcmp(&p_prev->new_state, (void *)p_data, data_len) == 0 && 
       //my_memcmp(&p_prev->new_state, p_data, data_len) == 0 && 
       p_prev->rep_count < UINT_MAX)
   {
      p_prev->rep_count++;
   }
   else
   {
      p_prev = &state_log.log[state_log.next_entry];
      
      p_prev->tick_count    = tickCount;
      p_prev->rep_count     = 1;
      p_prev->state_machine = state_machine;

      my_memcpy(&p_prev->new_state, (void *)p_data, data_len);

      if (data_len < EXTRA_DATA_SIZE+1)
      {
         memset((&p_prev->new_state)+data_len, 0, EXTRA_DATA_SIZE+1 - data_len);
      }

      state_log.next_entry++;
      if (state_log.next_entry >= MAX_STATE_LOG_ENTRIES)
      {
         state_log.next_entry = 0;
      }
      
      if (state_log.log_entries < MAX_STATE_LOG_ENTRIES)
      {
         state_log.log_entries++;
      }
   }
   
}   

void highspeed_log_state_change(state_machine_e state_machine, unsigned char step)
{
   p_prev = &state_log.log[state_log.next_entry];

   p_prev->tick_count    = tickCount;
   p_prev->rep_count     = 1;
   p_prev->state_machine = state_machine;
   p_prev->new_state     = step;

   state_log.next_entry++;
   if (state_log.next_entry >= MAX_STATE_LOG_ENTRIES)
   {
      state_log.next_entry = 0;
   }

   if (state_log.log_entries < MAX_STATE_LOG_ENTRIES)
   {
      state_log.log_entries++;
   }
}   

typedef struct
{
   state_machine_e state_machine;
   const char *prefix_str;
   const state_id_2_name_t * const lookup_tbl;
   size_t num_entries;
} state_machine_decode_t;

#define SM_ID_2_NAME_TBL(sm_id, prefix, tbl_name) {sm_id, prefix, tbl_name, NELEMENTS(tbl_name)}

const state_id_2_name_t sm_dummy_2_name[] = { {0, ""} };

const state_machine_decode_t state_machine_decode_tbl[] = 
{
   SM_ID_2_NAME_TBL(SM_MODE,                    NULL, sm_mode_2_name_tbl),
};
#endif /* STATE_MACHINE_LOGGING */
   
void *get_state_log_addr(void)
{
   #ifdef STATE_MACHINE_LOGGING
      return &state_log;
   #else
      return 0; // NULL was not defined, but ANSI permits and supports 0 for a null pointer.
   #endif /* STATE_MACHINE_LOGGING */
}   

unsigned long get_state_log_size(void)
{
   #ifdef STATE_MACHINE_LOGGING
      return sizeof(state_log);
   #else
      return 0;
   #endif /* STATE_MACHINE_LOGGING */
}   

#ifdef STATE_MACHINE_LOGGING   
static void dump_raw_data(uint8_t *p_data, uint32_t num_bytes)
{
   uint8_t i;
   char sz_log[20];

   for (i=0; i<num_bytes; i++, p_data++)
   {
      //if ( (i & 0x3) == 0 && i != 0)
      //{
      //   term_tx_str(" ");
      //}
      sprintf(sz_log, "%.2X", *p_data);
      term_tx_str(sz_log);
   }
}
#endif /* STATE_MACHINE_LOGGING */

const char * getVersionString (void);

void dump_state_log(void)
{
   char sz_log[200];
   #ifdef STATE_MACHINE_LOGGING   
      uint32_t entry_num = 0;
   #endif /* STATE_MACHINE_LOGGING */

//   stop_clock_tick();

   term_log("=============State Log dump start=============\r\n");
   #ifdef NEXUS_BASE
      sprintf(sz_log, "base %s\r\n", getVersionString());
   #endif
   #ifdef NEXUS_FEEDER
      sprintf(sz_log, "feeder %s\r\n", getVersionString());
   #endif
   term_tx_str(sz_log);
   sprintf(sz_log, "addr=%p\r\nsize=%lu\r\n", get_state_log_addr(), get_state_log_size());
   term_tx_str(sz_log);
   #ifdef STATE_MACHINE_LOGGING   
      sprintf(sz_log, "#entries=%u\r\nnext_entry=%u\r\n\r\n", state_log.log_entries, state_log.next_entry);
      term_tx_str(sz_log);
   #endif /* STATE_MACHINE_LOGGING */

   sprintf(sz_log, "   #|Data\r\n");
   term_tx_str(sz_log);
   sprintf(sz_log, "----+------------------------\r\n");
   term_tx_str(sz_log);

   #ifdef STATE_MACHINE_LOGGING   
      /* dump entire log */
      for (entry_num=0; entry_num < state_log.log_entries; entry_num++)
      {
         sprintf(sz_log, "%4.4u|", entry_num);
         term_tx_str(sz_log);
         dump_raw_data((uint8_t *)&state_log.log[entry_num], sizeof(state_log_entry_t));
         term_tx_str("\n\r");
      }
   
      sprintf(sz_log, "skipped entries=%lu\n\r", MAX_STATE_LOG_ENTRIES - entry_num);
      term_tx_str(sz_log);
   #endif /* STATE_MACHINE_LOGGING */

   term_log("=============State Log dump end===============\r\n");
//   start_clock_tick();
}

