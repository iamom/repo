/*************************************************************************
*   PROJECT:        Horizon CSD2-3
*	MODULE NAME:	exception.c
*
*	DESCRIPTION:	This file contains functions to log exceptions.
*
* ----------------------------------------------------------------------
*               Copyright (c) 2017 Pitney Bowes Inc.
*                    37 Executive drive
*                   Danbury, Connecticut  06810
* ----------------------------------------------------------------------
*
*************************************************************************/

#ifdef __cplusplus
extern  "C" {                              
#endif

#include <string.h>
#include <stdio.h>
#include "sys/stat.h"
#include "posix.h"
#include "commontypes.h"
#include "flashutil.h"
#include "pbos.h"
#include "pbplatform.h"
#include "clock.h"
#include "exception.h"
#include "errcode.h"
#include "oit.h"

#ifdef __cplusplus
}                              
#endif

// --------------------------------------------------------------------------*/
// Local Macro Definitions: 

#define THISFILE "exception.c"

// --------------------------------------------------------------------------*/
// Prototypes for functions not prototyped in an include file:

extern BOOL fnDL_BackupFileIfNeeded( UINT32 ulMaxSize, const char *pBackFileName, const char *pFileName );

extern BOOL fnGetSysTaskPwrupSeqCompleteFlag( void );

// --------------------------------------------------------------------------*/
// Declaration of external variables: 
extern unsigned long    dnldErr;

// --------------------------------------------------------------------------*/
// Definition of global variables: 

// --------------------------------------------------------------------------*/
// Definition of local variables: 

static const char logFileName[] = "Exception.log";
static const char logBackupFileName[] = "Exception.bak";

// --------------------------------------------------------------------------*/
// Definition of local functions: 

/* *************************************************************************
// FUNCTION NAME:       fnDL_BackupFileIfNeeded
// DESCRIPTION:
//      Given a size threshold and fileName, check the file size against the
//       threshold.  If the file is smaller, just return FALSE.  If equal or
//       larger, rename it to the backup filename, after deleting any file
//       that already exists with that name.
// INPUTS:
//      ulMaxSize - Size threshold in bytes.
//      pFileName - Pointer to the name of the file we may want to backup.
//      pBackFileName - Name of the backup file.
// OUTPUTS:
//       Return TRUE if the file was backed up, in case the caller wants to
//       recreate the new file.
//      FALSE if no backup was needed or no backup was done.
// NOTES:
//      If the file does not exist or is empty, no backup is done and the
//      function returns FALSE.
//
// --------------------------------------------------------------------------*/
BOOL fnDL_BackupFileIfNeeded( UINT32 ulMaxSize, const char *pBackFileName, const char *pFileName )
{
    int     iFfsStatus;
    UINT32  ulSize = 0;
    BOOL    fDoBackup = TRUE;
	struct stat fileStat;
	int errorCode;

    // First get the size of the function from the FFS directory.
    ulSize = fsGetFileSize( pFileName );
    if( ulMaxSize > 0 )
    {
        if( ulSize < ulMaxSize )
        {
            fDoBackup = FALSE;
        }
    }
    else
    {
        // If the max size is 0, it means always do a backup, but we want to
        //  make sure the file has something to backup first.
        if( ulSize < 1 )
        {
            fDoBackup = FALSE;
        }
    }

    if( fDoBackup == TRUE )
    {
		char aErr[80];
		memset(aErr, 0, sizeof(aErr));			
        //First delete any existing backup file.
    	errorCode = stat( pBackFileName, &fileStat );
        if( errorCode == POSIX_SUCCESS )
        {
            // If a backup file exists, delete it.
            iFfsStatus = unlink( pBackFileName );
            if( iFfsStatus != POSIX_SUCCESS )
            {
                fnCheckFileSysCorruption(iFfsStatus);
				strerror_r(errno, aErr, sizeof(aErr)-1);
            	dbgTrace(DBG_LVL_ERROR,"Exception log error: file %s deletion error: %s ", pBackFileName, aErr);
                return( 0 ); // indicate no backup was done
            }
        }
        else
            fnCheckFileSysCorruption(POSIX_ERROR);

        // Now rename the file to the backup.
        iFfsStatus = rename( pFileName, pBackFileName );
        if( iFfsStatus != 0 )
        {
			strerror_r(errno, aErr, sizeof(aErr)-1);
			dbgTrace(DBG_LVL_ERROR,"Exception log error: file %s rename error: %s ", pFileName, aErr);
            fnCheckFileSysCorruption(iFfsStatus);
            return( 0 ); // indicate no backup was done
        }
    }

    return( fDoBackup );
}


/*//////////////////////////////////////////////////////////////////////////////
*  Function Name:       taskException()
*  Parameters:   
*    msg - A pointer to the message to be displayed.
*    file - A pointer to the file name from where the exception was thrown.
*    line - The line number from where the exception was thrown.
*
*  Description:
*    Exception handler for general purpose task usage. For now it just logs a 
*    description of the error including the file name and line number from where  
*    the exception was thrown.
*
*  Returns:
*    Nothing.
*
** --------------------------------------------------------------------------*/
void taskException(char *msg, char *file, int line)
{    
    char        scratchPad[512];  // make it twice the desired 256 just to be on the safe side
	STATUS      status;
	int			logLen;
    DATETIME    now;

    // First check if we need to backup the exception.log file.  We never want
    //  it to get too big.  This function will rename it to the backup file if it
    //  is over the threshold size.  So the log will start fresh, but the previous
    //  info is still available (via PHSIMVB.)
    (void)fnDL_BackupFileIfNeeded( (UINT32)FILESIZE_THRESHOLD_EXCEPTIONLOG,
    		logBackupFileName, logFileName );

    // Allow 100 characters for the stuff other than the message, so
    // assuming 256 bytes per entry to the exception log,
    // make sure the message isn't more than 156 bytes
    if (strlen(msg) > 156)
        msg[156] = 0;

    memset(scratchPad, '\0', sizeof(scratchPad));
        
    if(GetSysDateTime(&now, TRUE, TRUE)) //if the clock is up
    {
        sprintf( scratchPad, 
                 "Exception thrown on %02d%02d/%02d/%02d at %02d:%02d:%02d ; %s ; File - %s ; Line - %d\n",
                 now.bCentury, now.bYear, now.bMonth, now.bDay,
                 now.bHour, now.bMinutes, now.bSeconds, 
                 msg, file, line );
    }
    else
        sprintf( scratchPad, "Exception thrown ; %s ; File - %s ; Line - %d\n", msg, file, line );
    
    fnDumpStringToSystemLog(scratchPad);
   
    logLen = strlen(scratchPad);
	status = fsAppend((char *) logFileName, scratchPad, logLen);
	if (status != SUCCESS)
	{
    	dbgTrace(DBG_LVL_ERROR,"Exception log error: log file append failure: %d ", status);
	}

    /* check the platform enable/disable feature before activating it */
    if (fnIsPlatformEnabled())
    {
        OSSendIntertask( PLAT, OIT, PLAT_ACTIVATE_PORT, NO_DATA, NULL, 0 );
    }

    fnReportMeterError( ERROR_CLASS_EXCEPTION, (UINT8)dnldErr );

}


