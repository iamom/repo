/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    tsks.c
   
   DESCRIPTION:    Nucleus entry point for application-level
                   system initialization.

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
 MODIFICATION HISTORY:
  
 06-Sep-18 Jane Li
 	Merged in changes from FPHX 02.12 branch:
	1. Bumped the size of the stack for the OIT Task to *18.
	2. Bumped the size of the stack for the Distributor Task to *14.
	3. Bumped the size of the stack for the PbP Task to *16.
 
 23-Nov-15 sa002pe on FPHX 02.12 shelton branch
 	Merged in changes from the 02.13 branch:
----*
 17-Nov-15 sa002pe on FPHX 02.10 shelton branch
 	For Fraca 230692: Bumped the size of the stack for the SCM/Bob Task from *2 to *3.
----*

*************************************************************************/

#include <stdlib.h>
#include "pbioext.h"
#include "global.h"
#include "clock.h"
#include "nucleus.h"
#include "ossetup.h"
#include "pbos.h"
#include "oit.h"
#include "cJSON.h"
#include "wsox_tx_queue.h"

//#include "oirating.h"
//#include "oirateservice.h"
#include "pbptask.h"
#include "FdrMcpTask.h"
#include "pbio.h"
#include "cmos.h"
#include "error_management.h"
#include "memory_stats.h"
#include "thread_control.h"
#include "pcdisk.h"
#include "blinker.h"
#include "commintf.h"
//#include "abacuscommtask.h"
#include "cmpublic.h"
#include "hal.h"
#include "networkmonitor.h"
#include "platform.h"
#include "wsox_server.h"
#include "debugTask.h"
#include "watchdog.h"
#include "settingsmgr.h"
#include "utils.h"
#include "bkgndmgr.h"
#include "trm.h"
#include "powerdown.h"
#include "am335x_timer.h"
#include "api_mfg.h"
#define __declare_cfi_user_variables__
#include "cfi.h"
#undef __declare_cfi_user_variables__

static STATUS CreateUICQueue(NU_MEMORY_POOL* mem_pool, QUEUE_DEFINITION *pQueueInfo);
static STATUS CreateUICTask(NU_MEMORY_POOL* mem_pool, TASK_DEFINITION *pTaskInfo);
static STATUS CreateUICTimer(NU_MEMORY_POOL* mem_pool, TIMER_DEFINITION *pTimerInfo,
                             int timerID);
static unsigned char UpdateNumberOfTask(const TASK_DEFINITION *pTaskInfo);
static void DetermineStdioUsage(void);
static void InitRTCValue(void);

// Functions called by timer expiration
void fnKeyExpire(unsigned long);
extern void            fnCMMailRunTimerExpire(ulong);
extern void            fnInfraTimeSyncLockoutEndCallback(ulong);
extern void fnEDMTimerExpire(unsigned long);

#ifdef CFG_CSD_2_3_ENABLE
extern void HALInitGPMCClock(void);
extern void HALInitGPMCPins(void);
extern void HALInitGPMCFRAM(void);
extern void HALInitGPMCFPGA(void);
extern void HALInitGPMCNORFlash(void);
extern unsigned char HALGetFunctionCheck(void);
extern void HALInitGPIORegisters(void);
extern void HALInitFPGA(void);
extern void HALInitPanelInterface();
extern void HALInitFeederInterface(void);
extern void HALInitPowerInterface(void);
extern UINT readGPIOPin(UINT gpio, UINT pin);
extern UINT setGPIOPin(UINT gpio, UINT pin);
extern UINT clearGPIOPin(UINT gpio, UINT pin);

#endif

// missing prototypes
void fnSystemHeartBeatTimerExpire();
void fnToggleLED1();


/* -------------------------------------------------------------------*/
/* PUBLIC VARIABLE DECLARATIONS                                       */
/* -------------------------------------------------------------------*/
unsigned long  UIC_ID_TYPE; // Indicates which hardware we are running on

NU_EVENT_GROUP NUC_NET_UP_EVENT;

unsigned char   led_image;

#undef DEBUGGING

extern void fnPlatformTask(unsigned long argc, void *argv);
extern VOID debugTask(unsigned long argc, void *argv) ;
extern void RebootSystem(void);

/* NOTE:  The order of the entries in the Tasks and Queues arrays   */
/*        must match the Task identifiers defined in (PB)OS.H       */
   
TASK_DEFINITION Tasks [] = {   

    /* No init arg, argv, no time slice, nu_preempt, nu_start for all */
   { NULL , "SYS"      , SystemControlTask         , NULL , UIC_MEDIUM_STACK_SIZE*8 , UIC_NORMAL_PRIORITY-1 }  , // 0x00  //IVAN
   { NULL , "CM"       , ControlManagerTask        , NULL , UIC_MEDIUM_STACK_SIZE*2 , UIC_NORMAL_PRIORITY   }  ,
   { NULL , "BJCTRL"   , BJControlTask             , NULL , UIC_MEDIUM_STACK_SIZE*2 , UIC_NORMAL_PRIORITY+1 }  , //IVAN
   { NULL , "BJSTATUS" , BJStatusTask              , NULL , UIC_MEDIUM_STACK_SIZE*2 , UIC_NORMAL_PRIORITY   }  ,
   { NULL , "OIT"      , fnOperatorInterfaceTask   , NULL , UIC_MEDIUM_STACK_SIZE*18 , UIC_NORMAL_PRIORITY   }  ,
   { NULL , "SCM"      , fnBobInterfaceTask        , NULL , UIC_MEDIUM_STACK_SIZE*3 , UIC_NORMAL_PRIORITY-5 }  , // 0x05
   { NULL , "KEY"      , fnKeypadDriverTask        , NULL , UIC_MEDIUM_STACK_SIZE*2 , UIC_NORMAL_PRIORITY+5 }  , // 0x06
   { NULL , "LCD"      , fnLCDDriverTask           , NULL , UIC_MEDIUM_STACK_SIZE*2 , UIC_NORMAL_PRIORITY+5 }  ,
   { NULL , "LCDCOM"   , LCDCommTask               , NULL , UIC_MEDIUM_STACK_SIZE*2 , UIC_NORMAL_PRIORITY   }  ,
   { NULL , "PSOC"     , PsocTask                  , NULL , UIC_MEDIUM_STACK_SIZE*2 , UIC_NORMAL_PRIORITY+6 }  , // 0x09
   { NULL , "CIBTTN"   , fnIButtonCommTask         , NULL , UIC_MEDIUM_STACK_SIZE*2 , UIC_NORMAL_PRIORITY   }  ,
   { NULL , "DIST"     , distributorTask           , NULL , UIC_MEDIUM_STACK_SIZE*14 , UIC_NORMAL_PRIORITY   }  ,
   { NULL , "PLAT"     , fnPlatformTask            , NULL , UIC_MEDIUM_STACK_SIZE*4 , UIC_NORMAL_PRIORITY   }  ,
   { NULL , "CPLAT"    , PlatformCommunicationTask , NULL , UIC_MEDIUM_STACK_SIZE*2 , UIC_NORMAL_PRIORITY   }  , // 0x0D
   { NULL , "PBPTASK"  , fnPBPServicesTask         , NULL , UIC_MEDIUM_STACK_SIZE*16 , UIC_NORMAL_PRIORITY   }  ,	// 0x0E
   { NULL , "FDRTASK"  , FeederMCPControllerTask   , NULL , UIC_MEDIUM_STACK_SIZE*4 , UIC_NORMAL_PRIORITY   }  ,	// 0x0F
   { NULL , "NETMON"   , NetworkMonitorTask        , NULL , UIC_MEDIUM_STACK_SIZE*4 , UIC_NORMAL_PRIORITY   }  , // 0x10
   { NULL , "WSTXSVR"  , WebSocket_Server_tx_Task  , NULL , 20*1024 /*STACK_SIZE*/  , UIC_NORMAL_PRIORITY-1 }  ,
   { NULL , "DBGTASK"  , debugTask                 , NULL , UIC_MEDIUM_STACK_SIZE*4 , UIC_NORMAL_PRIORITY   }  , // 0x12
   { NULL , "WDTTASK"  , WatchdogTimerTask         , NULL , UIC_MEDIUM_STACK_SIZE*2 , UIC_LOW_PRIORITY - 1  }  , // 0x13
   { NULL , "DBGLSTNR" , debugTaskListener         , NULL , UIC_MEDIUM_STACK_SIZE*4 , UIC_NORMAL_PRIORITY   }  , // 0x14
   { NULL , "WSRXSVR"  , WebSocket_Server_rx_Task  , NULL , 20*1024 /*STACK_SIZE*/  , UIC_NORMAL_PRIORITY   }  ,
   { NULL , "BKGRND"   , BackgroundManager_Task    , NULL , UIC_MEDIUM_STACK_SIZE*4 , UIC_LOW_PRIORITY - 2  }  , // 0x16
   { NULL , "PWRDOWN"  , PowerDown_Task            , NULL , UIC_MEDIUM_STACK_SIZE*2 , UIC_NORMAL_PRIORITY-6 }  , // 0x17
   { NULL , ""         , NULL                      , NULL , 0                       , 0                     }
};

/* NOTE:  The order of the entries in the Tasks and Queues arrays   */
/*        must match the Task identifiers defined in (PB)OS.H       */

QUEUE_DEFINITION Queues [] = { /* fixed size, intertask msg size, fifo for all */
   { NULL , "SYSQ"     	, NULL , UIC_MEDIUM_QUEUE_SIZE      }  ,
   { NULL , "CMQ"      	, NULL , UIC_MEDIUM_QUEUE_SIZE      }  ,
   { NULL , "BJCTRLQ"  	, NULL , UIC_MEDIUM_QUEUE_SIZE      }  ,
   { NULL , "BJSTATQ"  	, NULL , UIC_MEDIUM_QUEUE_SIZE      }  ,
   { NULL , "OITQ"     	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "SCMQ"     	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "KEYQ"     	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "LCDQ"     	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "CLCDQ"    	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "PSOCQ"    	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "CIBTTNQ"  	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "DISTQ"    	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "PLATQ"    	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "CPLATQ"   	, NULL , UIC_MEDIUM_QUEUE_SIZE * 3 	}  , /* lots of OI capture messages may require a long queue */
   { NULL , "PBPTASKQ" 	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "FDRTASKQ" 	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "NETMONQ"	, NULL , UIC_MEDIUM_QUEUE_SIZE		}  ,
   { NULL , "WSTXQ"    	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "DEBUGQ"   	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "WDTQ"      , NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "DEBUGLQ"  	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "WSRXQ"    	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "BKGNDQ"   	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , "PWRDOWNQ" 	, NULL , UIC_MEDIUM_QUEUE_SIZE     	}  ,
   { NULL , ""         	, NULL , 0                         	}  ,
   { NULL , ""         	, NULL , 0                         	}
   } ;

/*

    The order and number of semaphores MUST MATCH the
    Constant Def's in pbos.h EXACTLY!!!!!
*/
SEMAPHORE_DEFINITION Semaphores [MAX_UIC_SEMAPHORES] = {
   {NULL, "PH_PSOC", UIC_INITIAL_SEMAPHORE_COUNT, NU_FIFO},
   {NULL, "TR3_TRN", UIC_INITIAL_SEMAPHORE_COUNT, NU_FIFO},
   {NULL, "CERT_FL", UIC_INITIAL_SEMAPHORE_COUNT, NU_FIFO},
   {NULL, "DISTRIB", UIC_INITIAL_SEMAPHORE_COUNT, NU_FIFO},
   {NULL, "DEBUG_A", UIC_INITIAL_SEMAPHORE_COUNT, NU_FIFO},
   {NULL, "SMGR_WR", UIC_INITIAL_SEMAPHORE_COUNT, NU_PRIORITY},
   {NULL, "SMGR_RD", UIC_INITIAL_SEMAPHORE_COUNT, NU_PRIORITY},
   {NULL, "ETHDHCP", UIC_INITIAL_SEMAPHORE_COUNT, NU_FIFO},
   {NULL, "NET_SM",  UIC_INITIAL_SEMAPHORE_COUNT, NU_FIFO},	 
   } ;

/*

    The order and number of event groups MUST MATCH the 
    Constant Def's in (PB)OS.H EXACTLY !!!!!!!!!!!!!!!!

    GSJ
*/
EVENT_GROUP_DEFINITION EventGroups [MAX_UIC_EVENT_GROUPS] = 
{
     { NULL_PTR,  "EVENT1"   }  , //  0
     { NULL_PTR,  "BJEvent"  }  , //  1
     { NULL_PTR , "BUTILSX"  }  , //  2  Used for bob active/inactive flag.
     { NULL_PTR , "CPLAT"    }  , //  3  Platform Channel
     { NULL_PTR , "BUTILS1"  }  , //  4  Bob Utils                          , task tracking for up to
     { NULL_PTR , "BUTILS2"  }  , //  5   48 different tasks.
     { NULL_PTR , "BUTILS3"  }  , //  6
     { NULL_PTR , "PHCI"     }  , //  7
     { NULL_PTR , "PMCI"     }  , //  8
     { NULL_PTR , "USB"      }  , //  9
     { NULL_PTR , "SYSPWRUP" }  , // 10
     { NULL_PTR , "BOBDRIVE" }  , // 11
     { NULL_PTR , "PLAT"     }  , // 12
     { NULL_PTR , "OIT"      }  , // 13
     { NULL_PTR , "CEDM"     }  , // 14
     { NULL_PTR , "FDRWOW"   }  , // 15
     { NULL_PTR , "PWRDOWN"  }  , // 16
};   

// BJStatusTimer, BJ_TIMER_PERIOD},
// NOTE: the order in this array is implicitly linked to the definitions in
// pbos.h - do not alter one without the other.
TIMER_DEFINITION Timers [] = {
   { NULL , "MailRun4" , fnCMMailRunTimerExpire            , CM_QUICK_STOP_TIMER_DURATION           }  ,
   { NULL , "KEYSCAN"  , fnKeyExpire                       , 50                                     }  ,
   { NULL , "BJStatus" , BJStatusTimer                     , BJ_TIMER_PERIOD                        }  ,
   { NULL , "SYSMSG"   , fnSYSMessageTimerExpire           , (30 * SEC)                             }  ,
   { NULL , "SCREENCH" , fnScreenChangeTimerExpire         , (2 * SEC)                              }  ,
   { NULL , "USERTK"   , fnUserTickExpire                  , (60 * SEC)                             }  ,
   { NULL , "OITDEADM" , fnOITDeadmanExpire                , (30 * SEC)                             }  ,
   { NULL , "OITMSG"   , fnOITMessageExpire                , (5 * SEC)                              }  ,
   { NULL , "SCREENTK" , fnScreenTickTimerExpire           , (30 * SEC)                              }  ,//TODO JAH REMOVE me
   { NULL , "MailRun3" , fnCMMailRunTimerExpire            , CM_NORMAL_STOP_TIMER_DURATION          }  ,
   { NULL , "PLATMSG"  , fnPlatMessageExpire               , (10 * SEC)                             }  ,
   { NULL , "PSOCSIG"  , PsocSignalExpire                  , (1 * SEC)                              }  ,
   { NULL , "OIPRESET" , fnNormalPresetTimerExpire         , (1 * SEC)                              }  ,
   { NULL , "OITSLEEP" , fnSleepTimerExpire                , (600 * SEC)                            }  ,
   { NULL , "ASICUART" , UartTxExpire                      , (1 * SEC)                              }  ,
   { NULL , "AlarmTim" , fnClockTimerExpire                , (86400 * SEC)                          }  ,
   { NULL , "Thru_40"  , fnThruputTimerExpire              , 1363                                   }  ,
   { NULL , "Thru_30"  , fnThruputTimerExpire              , 1818                                   }  ,
   { NULL , "Thru_20"  , fnThruputTimerExpire              , 2727                                   }  ,
//   { NULL , "DLS_FTP"  , fnFtpTimerExpire                  , (600 * SEC)                            }  ,
   { NULL , "PLATPOL"  , fnPlatformPollTimerExpire         , 500                                    }  ,
   { NULL , "PLATLOC"  , fnPlatformLocalizationTimerExpire , (6 * SEC)                              }  ,
   { NULL , "SHORTTK"  , fnShortTickTimerExpire            , 100                                    }  ,
// { NULL , "MLPCWGT"  , fnRateZeroWeightTimerExpiry       , (60 * SEC)                             }  ,
   { NULL , "WaitTime" , fnInfraTimeSyncLockoutEndCallback , (60 * SEC)                             }  ,
   { NULL , "FDRSNSR"  , fnFeederQuerySensorTimerExpire    , FEEDER_SENSOR_POLLING_INTERVAL         }  ,
   { NULL , "FDRSTOP"  , fnFeederStopTimerExpire           , FEEDER_STOP_TIMEOUT_VALUE              }  ,
   { NULL , "MailRun5" , fnCMMailRunTimerExpire            , CM_DELAY_TRANSPORT_STOP_TIMER_DURATION }  ,
   { NULL , "FDRROLL"  , fnFeederRollerTimerExpire         , FEEDER_MAIL_FLOW_TIMEOUT_VALUE         }  ,
   { NULL , "FDRINACT" , fnFeederInactivityTimerExpire     , FEEDER_MAIL_FLOW_TIMEOUT_VALUE         }  ,
   { NULL , "MTNCSTOP" , fnCMMailRunTimerExpire            , CM_MAINTENANCE_STOP_TIMER_DURATION     }  ,
   { NULL , "HEARYBE"  , fnSystemHeartBeatTimerExpire      , (1 * SEC)                              }  ,
   { NULL , "FDRSTART" , fnFeederStartTimerExpire          , FEEDER_START_TIMEOUT_VALUE             }  ,
   { NULL , "FDRMSG"   , fnFeederMsgTimerExpire            , FEEDER_MSG_TIMEOUT_VALUE               }  ,
   //{ NULL , "UPLOAD"   , trmUploadTimerExpired             , (600 * SEC)			                }  ,
   { NULL , "UPLOADTO" , trmUploadTimeOut                  , (600 * SEC)			                }  ,
   { NULL , "MFGTBLST" , mfgTabletStatusTimeOut            , (10 * SEC)			                	}  ,
  { NULL  , "MailRunReport" , fnCMMailRunTimerExpire            , CM_REPORT_STOP_TIMER_DURATION           }
//Removed by RAM not to display EMD ->EMD 000000000 in syslog
//   { NULL , "EDMMSG"   , fnEDMTimerExpire                  , (10 * SEC)                           }  , //TODO: JAH temp
};




NU_MEMORY_POOL    *pSystemMemoryCached;
NU_MEMORY_POOL    *pSystemMemoryUncached;

NU_MEMORY_POOL    OSALMemPoolCB;
NU_MEMORY_POOL    *pOSALMemPoolCB = &OSALMemPoolCB;

#define STACKINITPATTERN 0x55aaaa55

/*
 * Set the 'numberOfTasks'
 */

unsigned char numberOfTasks = 0; //ARRAY_LENGTH(Tasks);
unsigned char numberOfTimers = ARRAY_LENGTH(Timers);
int ec;
BOOL stdio_enabled;

// Error handler local variables, use globals to avoid stack usage as much as possible
static INT				globalSTEHIteration = 0 ;
static VOID 			*globalPOldSP 		= NULL;
static VOID 			*globalPStackBackup = NULL ;
static VOID 			*globalFileDumpStack;
static UINT				globalStackSize 	= 0 ;
static UINT				globalRetAddress 	= 0 ;
static NU_MEMORY_POOL 	*globalMPSys 		= NULL ;
static NU_MEMORY_POOL 	*globalMPUSys 		= NULL ;
static CHAR				globalStrCrashMask[150];
static CHAR				fileStrCrashMask[150];
static DSTAT 			globalStatobj;
static UINT				globalEHFlag ;
static CHAR 			globalErrorString[80];
static NU_TASK 			globalTaskFileDump;
static NU_TASK 			*globalPCurThr;
static STATUS			globalErrStatus;


BOOL					globalErrorWritingInProgress = FALSE;



STATUS fnStandardTaskErrorHandler(INT iErrCode);

/* ************************************************************************
/ MODULE     : errorFileDumpTask
/ AUTHOR     : RD
/ DESCRIPTION: This is a support task to dump the required files to the dld/Logs directory in the event of a crash.
/
/ INPUTS     : System provided error number
/ *********************************************************************** */
#define FILEDUMP_STACK_SIZE		5000

void errorFileDumpTask(unsigned long argc, void *argv)
{
	char pLogFileName[50];
	DATETIME rDateTime;

	// argv will be our current thread pointer
	NU_TASK *pCurThr = (NU_TASK *)argv;

	// arg c will be the error code passed into the handler routine
	INT iErrCode = (INT)argc;
	UINT16  i=0, numCrashFiles=0;


	dbgTrace(DBG_LVL_INFO,"*** THREAD CRASH - %s ***\r\n", errorTypeToString(iErrCode));
	dbgTrace(DBG_LVL_INFO, "fnTaskErrorHandler Error %d detected in thread %s  return address at %08X - StackPointer Start:SP:End = %08X:%08X:%08X\r\n",
												iErrCode,
												(pCurThr->tc_name),
												globalRetAddress,
												pCurThr->tc_stack_start,
												(UINT)globalPOldSP,
												pCurThr->tc_stack_end);

	dbgTrace(DBG_LVL_INFO,"*** END OF THREAD CRASHINFO ***\r\n");

	snprintf(globalErrorString, 80,"crash_stack-%s-[%08X]-%08X-%08X-%08X.bin",
												(pCurThr->tc_name),
												globalRetAddress,
												(UINT)pCurThr->tc_stack_start,
												(UINT)globalPOldSP,
												(UINT)pCurThr->tc_stack_end);

	// look for crash files
	snprintf(globalStrCrashMask, 50, "%s/crash_*", POWERDOWN_SYSLOG_DIR);
	globalEHFlag = NU_Get_First(&globalStatobj, globalStrCrashMask);
	while(globalEHFlag == NU_SUCCESS)
	{
		numCrashFiles++;
		globalEHFlag = NU_Get_Next(&globalStatobj);
	}
	fnCheckFileSysCorruption(globalEHFlag);
	NU_Done(&globalStatobj);

	// if more than 6 crash files exist - delete all crash files
	if (numCrashFiles > 6)
	{
		globalEHFlag = NU_Get_First(&globalStatobj, globalStrCrashMask);
		fnCheckFileSysCorruption(globalEHFlag);
		for(i=0; i<numCrashFiles; i++)
		{
			snprintf(fileStrCrashMask, 150, "%s/%s", POWERDOWN_SYSLOG_DIR, globalStatobj.lfname);
			globalEHFlag = NU_Delete(fileStrCrashMask);
			fnCheckFileSysCorruption(globalEHFlag);
			globalEHFlag = NU_Get_Next(&globalStatobj);
			fnCheckFileSysCorruption(globalEHFlag);
		}
		NU_Done(&globalStatobj);		
	}
	
	// now store the required files....
	storeMemoryToFile(POWERDOWN_SYSLOG_DIR, globalErrorString, globalPStackBackup,(void *)((UINT)globalPStackBackup + globalStackSize));

	GetSysDateTime(&rDateTime, ADDOFFSETS, GREGORIAN);	
	sprintf(pLogFileName, "crash_syslog_%02d%02d.bin", rDateTime.bMinutes, rDateTime.bSeconds);
	storeSyslogToFile(POWERDOWN_SYSLOG_DIR, pLogFileName);
	sprintf(pLogFileName, "crash_netlog_%02d%02d.txt", rDateTime.bMinutes, rDateTime.bSeconds);
	storeNetlogToFile(POWERDOWN_SYSLOG_DIR, pLogFileName);

	// clear this flag to allow the caller routine to continue
	// allow re-entry from an alternate source...
	globalErrorWritingInProgress = FALSE;
}

/* ************************************************************************
/ MODULE     : fnStandardTaskErrorHandler
/ AUTHOR     : RD
/ DESCRIPTION: This error handler will try and recover the task enough to dump a copy
/              of the syslog with any remaining information that may help trace the
/              source of the error.
/ INPUTS     : System provided error number
/ *********************************************************************** */
STATUS fnStandardTaskErrorHandler(INT iErrCode)
{
	// do not allow this to be re-entered until processing is done....
	if(globalSTEHIteration > 0) return 0 ;
	globalSTEHIteration++;
	globalErrorWritingInProgress = TRUE;

	globalPCurThr	= NU_Current_Task_Pointer();

	// first step is to store off the stack before it becomes more corrupted, we allocate a chunk of memory the correct size and copy it over
	globalStackSize 		= (UINT)(globalPCurThr->tc_stack_end - globalPCurThr->tc_stack_start) ;
	NU_System_Memory_Get(&globalMPSys, &globalMPUSys);
	NU_Allocate_Memory(globalMPSys, (void **) &globalPStackBackup, globalPCurThr->tc_stack_end - globalPCurThr->tc_stack_start, NU_NO_SUSPEND);
	memcpy(globalPStackBackup, globalPCurThr->tc_stack_start, globalPCurThr->tc_stack_end - globalPCurThr->tc_stack_start);

	// disaster mode ... no coming back from this one...
	// Make a note of the current stack position, reset the stack pointer to the start of the task's original space
	// then update the thread info with this position
	// use a global since we do not want this on the stack....
	globalPOldSP = ESAL_TS_RTE_SP_READ();

	ESAL_TS_RTE_SP_WRITE(globalPCurThr->tc_stack_end);

	globalPCurThr->tc_stack_pointer = ESAL_TS_RTE_SP_READ();

	// load the stack check test pattern in the last location (some OS checks only check this point)
	*(char *)(globalPCurThr->tc_stack_start) = (char)NU_STACK_FILL_PATTERN;


	// We should now have a safe environment to do just about anything we want, except return!
	// see if there is an error block we can examine, if so get some info for logging
	if(globalPCurThr != NULL)
	{
		ERC_CB *errBlk = (ERC_CB *)globalPCurThr->tc_error ;

		if(errBlk)
		{
			NU_ERROR  usrErr = (NU_ERROR)errBlk->user_error;
			globalRetAddress = (UINT)usrErr.return_address;
		}
	}

	// we need to write the dump files however we must not do this from an HISR and may be in one now so spawn a task to do the saving
	// wait for it to finish then flash the led's
	globalErrStatus = NU_Allocate_Memory(globalMPSys, (void **) &globalFileDumpStack, FILEDUMP_STACK_SIZE, NU_NO_SUSPEND);
	if(globalErrStatus == NU_SUCCESS)
	{
		globalErrStatus = NU_Create_Task(&globalTaskFileDump,
				"CrashDMP",
				errorFileDumpTask,
				iErrCode,
				globalPCurThr,
				globalFileDumpStack,
				FILEDUMP_STACK_SIZE,
				UIC_HIGH_PRIORITY,
				NO_TIME_SLICING,
				NU_NO_PREEMPT,
				NU_START );
		// wait for thread to complete its task
		while( globalErrorWritingInProgress == TRUE)
		{ /* no op */}
	}
	// wait for the logging task to complete.

	// DO NOT RETURN!!!!! - stack has been blown away, return would be an exercise in transcendental abstract creativity !
	globalSTEHIteration--;
	
	RebootSystem();

	return NU_SUCCESS;
}

/* ************************************************************************
/ MODULE     : installStandardErrorHandlerInAllTasks
/ AUTHOR     : RD
/ DESCRIPTION: Install the above error handler in all active tasks in the system
/ INPUTS     : none
/ *********************************************************************** */
 void installStandardErrorHandlerInAllTasks()
 {
	 NU_TASK *pTasksArray[MAX_RUNTIME_TASKS]; // Should not have more than 100 tasks
	 UNSIGNED numTasks;
	 STATUS stat;
	 UINT uPass;
	 NU_TASK *pCurrentTask;

	 numTasks = NU_Task_Pointers(&pTasksArray[0], MAX_RUNTIME_TASKS);

	 for(uPass=0; uPass < numTasks; uPass++)
	 {
		 pCurrentTask = pTasksArray[uPass];
		 if (pCurrentTask)//should never be NULL but check anyway
		 {
		 	 stat = NU_Register_Thread_Error_Handler(pCurrentTask, fnStandardTaskErrorHandler) ;
		 	 dbgTrace(DBG_LVL_INFO,"Install error handler %d for task %s, status: %d", uPass, pCurrentTask->tc_name, stat);
		 }
	 }
 }

/* ************************************************************************
/ MODULE     : Application_Initialize
/ AUTHOR     : Wes Kirschner, Joe Mozdzer
/ DESCRIPTION: This is the Nucleus entry point.  It creates the memory regions
/              tasks, queues, events, semaphores, and other system resources.
/ INPUTS     : none
/ NOTES      : If any memory allocation or Nucleus function calls fail, 
/               the system will freeze in this function.
/ *********************************************************************** */
void Application_Initialize(NU_MEMORY_POOL*cached_available_memory, NU_MEMORY_POOL* uncached_available_memory)
{

    TASK_DEFINITION         *pTaskInfo;
    QUEUE_DEFINITION        *pQueueInfo;   
    SEMAPHORE_DEFINITION    *pSemaphoreDefinition;
    EVENT_GROUP_DEFINITION  *pEvents;
    TIMER_DEFINITION        *pTimerInfo;
    
    unsigned char i;
    unsigned char *pStartOSALPool = NULL_PTR;
    STATUS         status;
    unsigned	   numBytes;
    
// If running on BBB, disable watchdog
#ifdef CFG_NU_BSP_BEAGLEBONE_BLACK_ENABLE
    HALSetWatchdogTimer(FALSE);
#endif

    prepTimerForUpCounting(TIMER_TO_TEST);
    // Prior to logging int RTC & figure out if stdio is to be used
    InitRTCValue();
    DetermineStdioUsage();
    OSClearSysLog();

    //copy memory pool pointers to globals for all to use
    pSystemMemoryCached = cached_available_memory;
    pSystemMemoryUncached = uncached_available_memory;

    /* Create OS Abstraction Layer memory pool */
    numBytes = OSAL_MEM_POOL_SIZE;
    status = NU_Allocate_Memory(cached_available_memory, (void **) &(pStartOSALPool), numBytes, NU_NO_SUSPEND);
    
    if ((pStartOSALPool == NULL_PTR) || (status != NU_SUCCESS))
      goto freeze_here;
    
    memset((unsigned char *)(pStartOSALPool), 0x00, numBytes);
    
    status = NU_Create_Memory_Pool(pOSALMemPoolCB, "OSALMEM", pStartOSALPool, numBytes, 4, NU_FIFO);
    
    if (status != NU_SUCCESS)
      goto freeze_here;
    

    //Bump the boot cycle counter
    USBBootCycle++;

    /* It is time to create the semaphores */
	numBytes = sizeof(NU_SEMAPHORE);
    for (pSemaphoreDefinition = &Semaphores[0], i = 0 ; i < MAX_UIC_SEMAPHORES; i++)
      {
	    status = NU_Allocate_Memory(cached_available_memory, (void **) &(pSemaphoreDefinition->pSemaphore), numBytes, NU_NO_SUSPEND);
		memset((unsigned char *)(pSemaphoreDefinition->pSemaphore), 0x00, numBytes);

		if (pSemaphoreDefinition->pSemaphore == NULL_PTR)
		  goto freeze_here;

		status = NU_Create_Semaphore (pSemaphoreDefinition -> pSemaphore,
						  pSemaphoreDefinition -> szSemaphoreName,
						  pSemaphoreDefinition -> lwInitialCount,
						  pSemaphoreDefinition -> type) ;

		if (status != NU_SUCCESS)
		  goto freeze_here;

		pSemaphoreDefinition = pSemaphoreDefinition + 1 ;
      }    
    
    smgr_init(); /* initialize the settings manager */

    status = NU_Create_Event_Group (&NUC_NET_UP_EVENT, "NucNetEvt");
    if (status != NU_SUCCESS)
      goto freeze_here;
    NU_Set_Events (&NUC_NET_UP_EVENT,  0 , NU_AND); 
    
    
    /* Holy events Batman!  Here we go! */
	numBytes = sizeof(NU_EVENT_GROUP);
    for (pEvents = &EventGroups[0], i = 0 ; i < MAX_UIC_EVENT_GROUPS ; i++)
      {
	    status = NU_Allocate_Memory(cached_available_memory, (void **) &(pEvents->pEventGroup), numBytes, NU_NO_SUSPEND);
		memset((unsigned char *)(pEvents->pEventGroup), 0x00, numBytes);

		if (pEvents -> pEventGroup == NULL_PTR)
		  goto freeze_here;

		status = NU_Create_Event_Group (pEvents -> pEventGroup, pEvents -> szEventGroupName);

		if (status != NU_SUCCESS)
		  goto freeze_here;

		pEvents = pEvents + 1;
      }        

    numberOfTasks  = UpdateNumberOfTask(&Tasks[0]);
    
    /* --------------------------------------------------------------*/
    /*  Create UIC tasks and the queue associated with each task     */
    /* --------------------------------------------------------------*/ 
    for (pTaskInfo = &Tasks[0], i=0; i < numberOfTasks; i++)
    {
		/* Create task and start it */
		if (CreateUICTask(cached_available_memory, pTaskInfo) != NU_SUCCESS)
		{
			goto freeze_here;
		}

		pTaskInfo = pTaskInfo + 1;
		pQueueInfo = pQueueInfo + 1;
    }

    // RD 26July2016 - moved out of task init section above because there are more queues than tasks.
    for(pQueueInfo = &Queues[0] ; pQueueInfo->szQueueName[0] != 0 ; pQueueInfo++)
    {
		if (CreateUICQueue(cached_available_memory, pQueueInfo) != NU_SUCCESS)
		{
			goto freeze_here;
		}
    }
    
    OSResumeTask(SYS);
    // OSResumeTask(BJSTATUS);

    /* Create Timers */
    for (pTimerInfo = &Timers[0], i = 0; i < numberOfTimers; i++, pTimerInfo++)
    {
        if (CreateUICTimer(cached_available_memory, pTimerInfo, i) != NU_SUCCESS)
        {
            goto freeze_here;
        }
    }

    // OSStartTimer(0); // Start Timer

    fnIntertaskMsgLogEnable();      // Turn on message logging

//    (void)fnReserveAbacusMemPool();

    /* Successful function exit */
    //    led_image = 8;        // Turn on 4th LED
    //    LED = led_image;
    return;


    /* The system freezes here if there was an error initializing the application. */
freeze_here:
    fnDisplayErrorDirect("App Init Failed", 0, SYS);  //Only need to record this message once
	
freeze_here1:
    goto freeze_here1;
}


/*
 * Name
 *   CreateUICQueue
 *
 * Description
 *   This function performs the necessary operations required to create an
 *   intertask queue for passing messages between tasks.
 *
 * Inputs
 *   pQueueInfo - pointer to a QUEUE_DEFINITION structure containing the information
 *                  about the Queue to be created
 *
 * Returns
 *   NU_SUCCESS   - if the queue was created successfully
 *   !NU_SUCCESS  - if an error occurred while attempting to create the queue
 *
 * Caveats
 *   If successful, a new queue has been created within the system.
 *
 *   Queues and Tasks are created with a one-to-one correspondence; the queue
 *     is created before the task is created.
 *
 * Revision History
 *   05/30/2001 Joseph P. Tokarski - Initial Revision, based upon existing code.
 */
static STATUS CreateUICQueue(NU_MEMORY_POOL* mem_pool, QUEUE_DEFINITION *pQueueInfo)
{
	STATUS status;
	UNSIGNED numBytes;

	/* allocate the necessary resources for the queue */
	numBytes = sizeof(NU_QUEUE);
    status = NU_Allocate_Memory(mem_pool, (void **) &(pQueueInfo->pQueue), numBytes, NU_NO_SUSPEND);
    memset((unsigned char *)(pQueueInfo->pQueue), 0x00, numBytes);

	numBytes = pQueueInfo->lwQueueSize * sizeof(unsigned long);
    status = NU_Allocate_Memory(mem_pool, (void **) &(pQueueInfo->pQueueStartAddress), numBytes, NU_NO_SUSPEND);
    memset((unsigned char *)(pQueueInfo->pQueueStartAddress), 0x00, numBytes);

    if ((pQueueInfo->pQueue == NULL_PTR) || 
        (pQueueInfo->pQueueStartAddress == NULL_PTR))
    {
        return(!NU_SUCCESS);
    }

    /* actually create the queue at the operating system level */
    status = NU_Create_Queue(pQueueInfo->pQueue, pQueueInfo->szQueueName,
                             pQueueInfo->pQueueStartAddress, 
                             pQueueInfo->lwQueueSize,
                             NU_FIXED_SIZE, INTERTASK_MSG_SIZE, NU_FIFO);

    /* return the result of queue creation */
    return(status);
}


/*
 * Name
 *   CreateUICTask
 *
 * Description
 *   This function creates a UIC task, using the information provided in the
 *   Task Definition structure.
 *
 * Inputs
 *   pTaskInfo - pointer to a TASK_DEFINITION structure containing the necessary
 *                 information needed to create the UIC task.
 *
 * Returns
 *   NU_SUCCESS   - if the task was created successfully
 *   !NU_SUCCESS  - if an error occurred while attempting to create the task
 *
 * Caveats
 *   This function assumes a non-NULL pTaskInfo pointer.
 *
 *   If successful, a new task has been created within the system.
 *
 *   Queues and Tasks are created with a one-to-one correspondence; the task
 *     is created after the queue has been created.
 *
 * Revision History
 *   05/30/2001 Joseph P. Tokarski - Initial Revision, based upon existing code.
 *   07/15/2016 Rich D. pass mem_pool into task as argc param.
 */
static STATUS CreateUICTask(NU_MEMORY_POOL* mem_pool, TASK_DEFINITION *pTaskInfo)
{
    STATUS status;
    unsigned long ulSize,*pStk,fPat;
	UNSIGNED numBytes;

    /* allocate the necessary resources for the task */
	numBytes = sizeof(NU_TASK);
    status =  NU_Allocate_Memory(mem_pool, (VOID **)&pTaskInfo->pControlBlock, numBytes, NU_NO_SUSPEND);
    memset((unsigned char *)(pTaskInfo->pControlBlock), 0x00, numBytes);


	numBytes = pTaskInfo->ulStackSize;
    status = NU_Allocate_Memory(mem_pool, &pTaskInfo->pTaskStackAddress, numBytes, NU_NO_SUSPEND);
    memset((unsigned char *)(pTaskInfo->pTaskStackAddress), 0xAA, numBytes);

    if ((pTaskInfo->pControlBlock == NULL_PTR) || 
        (pTaskInfo->pTaskStackAddress == NULL_PTR))
    {
        return(!NU_SUCCESS);
    }
    ulSize = pTaskInfo->ulStackSize / sizeof( unsigned long );
    fPat = ( unsigned long ) pTaskInfo->pTaskStackAddress;
    fPat += pTaskInfo->ulStackSize;
    pStk = pTaskInfo->pTaskStackAddress;
    while(ulSize--)
      *pStk++ = fPat; // pTaskInfo->pControlBlock; // STACKINITPATTERN; 
    /* actually create the task at the Operating System level */            
    status = NU_Create_Task(pTaskInfo->pControlBlock, pTaskInfo->szTaskName,
                            pTaskInfo->pTaskEntryPoint, (INT)mem_pool, NULL_PTR ,
                            pTaskInfo->pTaskStackAddress,
                            pTaskInfo->ulStackSize,
                            pTaskInfo->Priority, NO_TIME_SLICING, 
                            NU_PREEMPT, NU_NO_START);

    /* return the result of the task creation */            
    return(status);
}


/*
 * Name
 *   CreateUICTimer
 *
 * Description
 *   This function creates a UIC timer, using the information provided in the
 *   Timer Definition structure.
 *
 * Inputs
 *   pTimerInfo - pointer to a TIMER_DEFINITION structure containing the necessary
 *                 information needed to create the UIC timer.
 *
 * Returns
 *   NU_SUCCESS   - if the timer was created successfully
 *   !NU_SUCCESS  - if an error occurred while attempting to create the timer
 *
 * Caveats
 *   This function assumes a non-NULL pTimerInfo pointer.
 *
 *   If successful, a new timer has been created within the system.
 *
 * Revision History
 *   05/30/2001 Joseph P. Tokarski - Initial Revision, based upon existing code.
 */
static STATUS CreateUICTimer(NU_MEMORY_POOL* mem_pool,
							TIMER_DEFINITION *pTimerInfo,
                             int timerID)
{
    STATUS status;
	UNSIGNED numBytes;

    /* allocate the necessary resources for the timer */
	numBytes = sizeof(NU_TIMER);
    status = NU_Allocate_Memory(mem_pool, (void **) &(pTimerInfo->pTimer), numBytes, NU_NO_SUSPEND);
    memset((unsigned char *)(pTimerInfo->pTimer), 0x00, numBytes);

    if (pTimerInfo->pTimer == NULL_PTR)
    {
        return(!NU_SUCCESS);
    }

    status = NU_Create_Timer(pTimerInfo->pTimer, pTimerInfo->szTimerName,
                             pTimerInfo->pExpirationRoutine, timerID, 
                             (pTimerInfo->lwMilliseconds)/MILLISECONDS_PER_TICK,
                             (pTimerInfo->lwMilliseconds)/MILLISECONDS_PER_TICK,
                             NU_DISABLE_TIMER);

    /* return the result of the task creation */            
    return(status);
}

/*
 * Name
 *   UpdateUICTimer
 *
 * Description
 *   This function updates an existing Nucleus Timer definition, with the
 *   data maintained in the Timers[] array, for the timer identified by
 *   the offset 'lwTimerID'.
 *
 * Inputs
 *   lwTimerID - identifier (array offset) of the UIC Timer to update
 *
 * Returns
 *   NU_SUCCESS  - if the update was successful
 *   !NU_SUCCESS - if an error occurred
 *
 * Caveats
 *   The existing timer definition is modified with the contents of the
 *     timer array entry.
 *
 *   This function assumes that the timer is NOT currently active.
 *
 * Revision History
 *   05/31/2001 Joseph P. Tokarski (CSS, LLC) - Initial Revision.
 */
static STATUS UpdateUICTimer(unsigned long lwTimerID)
{
    TIMER_DEFINITION *pTimerInfo;
    STATUS timerStatus;

    /* set the pointer to the correct array entry for ease of access */
    pTimerInfo = &Timers[lwTimerID];

    /* update the timer with the new information */
    timerStatus = NU_Reset_Timer(pTimerInfo->pTimer, 
                                 pTimerInfo->pExpirationRoutine,
                                 (pTimerInfo->lwMilliseconds)/MILLISECONDS_PER_TICK,
                                 (pTimerInfo->lwMilliseconds)/MILLISECONDS_PER_TICK,
                                 NU_DISABLE_TIMER);

    return(timerStatus);
}

/*
 * Name
 *   fnUpdateTimerExpiryFunction
 *
 * Description
 *   This function updates an existing timer definition's Expiration
 *   Routine (callback function).  After the timer definition is updated,
 *   this function calls a more generic function to update the Timer
 *   definition at the Operating System level.
 *
 * Inputs
 *   lwTimerID          - UIC Timer Identifier to change the definition of
 *   pExpirationRoutine - pointer to the new expiration routine
 *
 * Returns
 *   Nothing.
 *
 * Caveats
 *   This function changes the Timer definition in the Timers[] array,
 *     identified by lwTimerID, modifying the Expiration Routine.
 *
 *   This function also updates the Timer definition at the Operating System
 *     level.
 *
 * Revision History
 *   05/31/2001 Joseph P. Tokarski (CSS, LLC) - Initial Revision.
 */
void fnUpdateTimerExpiryFunction(unsigned long lwTimerID,
                                 void (*pExpirationRoutine)(unsigned long))
{

    /* update the Timers[] entry */
    Timers[lwTimerID].pExpirationRoutine = pExpirationRoutine;

    /* update the timer definition in the O/S context */
    UpdateUICTimer(lwTimerID);
}

/*
 * Name
 *   fnUpdateTimerInterval
 *
 * Description
 *   This function updates an existing timer definition's Interval Time,
 *   i.e., the interval of time that will expire before calling the
 *   callback function.  After the timer definition is updated,
 *   this function calls a more generic function to update the Timer
 *   definition at the Operating System level.
 *
 * Inputs
 *   lwTimerID       - UIC Timer Identifier to change the definition of
 *   lwTimerInterval - new 'interval' for the timer
 *
 * Returns
 *   Nothing.
 *
 * Caveats
 *   This function changes the Timer definition in the Timers[] array,
 *     identified by lwTimerID, modifying the Timer Interval.
 *
 *   This function also updates the Timer definition at the Operating System
 *     level.
 *
 * Revision History
 *   06/05/2001 Joseph P. Tokarski (CSS, LLC) - Initial Revision.
 */
void fnUpdateTimerInterval(unsigned long lwTimerID,
                           unsigned long lwTimerInterval)
{

    /* update the Timers[] entry */
    Timers[lwTimerID].lwMilliseconds = lwTimerInterval;

    /* update the timer definition in the O/S context */
    UpdateUICTimer(lwTimerID);
}

//=====================================================================
//
//  Function Name:  PBInitTaskCount
//
//  Description: This function is used by the runtime
//      to determine the number of tasks contained
//              in the task array
//
//  Parameters: NONE
//      
//      
//      
//  Returns: the number of tasks
//      
//
//  Preconditions:
//                  
//
//=====================================================================

unsigned PBInitTaskCount()
{
  return(numberOfTasks);
}

/* unused anymore?
static long currentStackCheckTaskIndex = 0;

void ProcessStackCheck()
{
  unsigned long *pStk,*pStkEnd , ulSize;
  TASK_DEFINITION *pTaskInfo;

  if(currentStackCheckTaskIndex >= numberOfTasks)
    currentStackCheckTaskIndex = 0;
  pTaskInfo = &Tasks[currentStackCheckTaskIndex];

  ulSize = pTaskInfo->ulStackSize;
}
*/

/* **********************************************************************
// FUNCTION NAME:fnSystemHeartBeatTimerExpire
// PURPOSE: Heartbeat of the system, toggle the LED
//
// AUTHOR: George Monroe
//
// INPUTS:
// RETURNS:
// **********************************************************************/
void fnSystemHeartBeatTimerExpire()
{
  fnToggleLED1();
}

/*
 * Name
 *   fnGetTaskRemainingStackSize
 *
 * Description
 *   This function checks how much space hasn't been used on a task's stack.
 *
 * Inputs
 *   bTaskID         - task ID number
 *   plStackSize     - pointer to where to load the size of the stack
 *   plRemainingSize - pointer to where to load the size of the unused space on the stack
 *   pbTaskName      - pointer to where to load the name of the task
 *
 * Returns
 *   TRUE if task ID is OK. FALSE otherwise.
 *
 * Caveats
 *   None
 *
 * Revision History
 *   12 Jan 05 Sandra Peterson - Initial Revision.
 */

BOOL fnGetTaskRemainingStackSize(unsigned char bTaskID,
                            unsigned long *plStackSize,
                            unsigned long *plRemainingSize,
                            char *pbTaskName)
{
    unsigned long   *plStartAddr;
    unsigned long   lInx, lEnd,lAddr;
    BOOL    bRetVal = TRUE;

    if (bTaskID >= numberOfTasks)
    {
        bRetVal = FALSE;
    }
    else
    {
        *plStackSize = Tasks[bTaskID].ulStackSize;
        memcpy(pbTaskName, Tasks[bTaskID].szTaskName, MAX_NUCLEUS_NAME);
        pbTaskName[MAX_NUCLEUS_NAME] = 0;

        plStartAddr = Tasks[bTaskID].pTaskStackAddress;
        lAddr = ( unsigned long ) plStartAddr;
        lAddr += Tasks[bTaskID].ulStackSize;

        lInx = 0;
        lEnd = *plStackSize - 4;    // because the first 4 bytes on the stack aren't used

        //while ( (lInx < lEnd) && (*plStartAddr == 0xAAAAAAAA)  && (plStartAddr != NULL_PTR) )     
        while ( (lInx < lEnd) && (*plStartAddr == lAddr)  && (plStartAddr != NULL_PTR) )
        {
            lInx += 4;
            plStartAddr++;
        }

        *plRemainingSize = lInx;
    }

    return (bRetVal);
}


/*
 * Name
 *   fnGetHeapUsageData
 *
 * Description
 *   This function checks the usage of the system heap.
 *
 * Inputs
 *   pPoolCB           - pointer to control block of pool to get usage for
 *   pHeapSize         - pointer to where to load the overall size of the heap
 *   pCurrentRemaining - pointer to where to load the current number of bytes available on the heap
 *   pNumTasksWaiting  - pointer to where to load the current number of tasks waiting for memory on heap
 *
 * Returns
 *   None.
 *
 * Caveats
 *   None
 *
 * Revision History
 *   24 Jan 05 Sandra Peterson - Initial Revision.
 */

void fnGetHeapUsageData(NU_MEMORY_POOL *pPoolCB,
							unsigned *pHeapSize,
                            unsigned *pCurrentRemaining,
                            unsigned *pNumTasksWaiting)
{
    CHAR name[8];
    VOID *start_address;
    UNSIGNED min_allocation;
    OPTION suspend_type;
    NU_TASK *first_task;

    // Only some of the info from API is returned
    (void) NU_Memory_Pool_Information(pPoolCB,
    									name,
										&start_address,
										pHeapSize,
										&min_allocation,
										pCurrentRemaining,
										&suspend_type,
										pNumTasksWaiting,
										&first_task);
    return;
}

/*
 * Name
 *   AddUICTask
 *
 * Description
 *   This function adds (appends) the Task Definition specified in the
 *   pTaskInfo structure to the Queues array.  Once the task definition
 *   has been added, the task is created for use.
 *
 * Inputs
 *   pTaskInfo - pointer to a TASK_DEFINITION structure containing the task
 *                  definition to be created
 *
 * Returns
 *   NU_SUCCESS - if the task was successfully added to the Tasks array, and
 *                  task was successfully created
 *  !NU_SUCCESS - if an error occurred attempting to add the task to the array
 *                  or an error occurred attempting to create the task
 *
 * Caveats
 *   This function assumes that pTaskInfo is valid and references a
 *     non-trivial TASK_DEFINITION.
 *
 *   Once this function completes, the Task definition will be added to the
 *     'Tasks' array, and the task will have been created within the
 *     Operating System.
 *
 *   Assuming that the task was created successfully, the total number of
 *     ACTIVE tasks/queues is updated.
 *
 * Revision History
 *   05/30/2001 Joseph P. Tokarski (CSS, LLC) - Initial Revision.
 */
STATUS AddUICTask(TASK_DEFINITION *pTaskInfo)
{
    STATUS taskStatus;

    /* verify that we have space left in the array for the new queue */
    if (numberOfTasks == ARRAY_LENGTH(Tasks))
    {
        /* there's no room left at the inn; return a failure */
        return(!NU_SUCCESS);
    }

    /* add the queue definition at the current 'numberOfTasks' location */
    memcpy(&Tasks[numberOfTasks], pTaskInfo, sizeof(TASK_DEFINITION));

    /* create the queue, and return the result */
    taskStatus = CreateUICTask(pSystemMemoryCached, &Tasks[numberOfTasks]);

    if (taskStatus == NU_SUCCESS)
    {
        OSResumeTask(numberOfTasks);

        /* since we were able to create the task (and its corresponding queue),
         * increment the number of active Queues/Tasks
         */
        numberOfTasks += 1;
    }

    return(taskStatus);
}

/*
 * Name
 *   UpdateNumberOfTask
 *
 * Description
 *   This function returns task number without NULL task..
 *
 * Inputs
 *   pTaskInfo         - pointer to task array
 *
 * Returns
 *   ucNumOfTask    - number of task
 *
 * Caveats
 *   None
 *
 * Revision History
 *   30 Apr 07 Oscar Wang - Initial Revision.
 */
static unsigned char UpdateNumberOfTask(const TASK_DEFINITION *pTaskInfo)
{
    unsigned char ucIndex;
    unsigned char ucNumOfTask = 0;
    unsigned char ucNumOfTotalTask = ARRAY_LENGTH(Tasks);

    for(ucIndex = 0; ucIndex < ucNumOfTotalTask; ucIndex++)
    {
        if(pTaskInfo[ucIndex].pTaskEntryPoint == NULL)
        {
            break;
        }
        ucNumOfTask++;
    }

    return ucNumOfTask;
}

/*
 * Description
 *   Initialize RTC value to same value as in G900
 *
 */
static void InitRTCValue(void)
{
    DATETIME		dateTimeForRTC;

	dateTimeForRTC.bCentury  = 20;
	dateTimeForRTC.bYear = 03;
	dateTimeForRTC.bMonth = 05;
	dateTimeForRTC.bDay  = 01;
	dateTimeForRTC.bDayOfWeek = 02;
	dateTimeForRTC.bHour = 0;
	dateTimeForRTC.bMinutes = 0;
	dateTimeForRTC.bSeconds = 0;
	HALSetRTC(&dateTimeForRTC);

}

#define FPGA_SERIAL_STDIO_KEY "/csd_2_3/fpga_serial/stdio"
#define SERIAL0_STDIO_KEY "/csd_2_3/serial0/stdio"

/*
 * Description
 *   Sets global flag indicating if stdio has been configured
 *
 */
static void DetermineStdioUsage(void)
{
    STATUS stat;
    BOOLEAN stdio_en = NU_FALSE;
    NU_UNUSED_PARAM(stat);

	stdio_enabled = FALSE;
    /* check FPGA UART */
    stat = NU_Registry_Get_Boolean(FPGA_SERIAL_STDIO_KEY, &stdio_en);
    if(stdio_en == NU_TRUE)
    {
    	stdio_enabled = TRUE;
    	return; // no need to check more; assume only one is stdio
    }

    /* check Sitara UART */
    stat = NU_Registry_Get_Boolean(SERIAL0_STDIO_KEY, &stdio_en);
    if(stdio_en == NU_TRUE)
    {
    	stdio_enabled = TRUE;
    }
}

#ifdef CFG_CSD_2_3_ENABLE
VOID Pre_Kernel_Init_Hook(NU_MEMORY_POOL* system_memory, NU_MEMORY_POOL *uncached_system_memory)
{
	char outChar;
    NU_UNUSED_PARAM(outChar);

#ifdef WATCHDOG_TIMER_ENABLE
    HALInitWDT();
    HALStartWDT(WATCHDOG_INITIAL_TIMEOUT_VALUE);
#endif
	HALInitGPIORegisters();
	//TODO - Do we need to store this somewhere?
	outChar = (char) HALGetFunctionCheck();
    HALInitGPMCClock();
    HALInitGPMCPins();
    HALInitGPMCFRAM();
    HALInitGPMCNORFlash();
    HALInitGPMCFPGA();
    HALInitFPGA();
    HALInitPanelInterface();
    HALInitPowerInterface();
    HALInitFeederInterface();
    HALInitRTCSS();

    prepTimer_0_AsFreerunningTmr();  /* used as a timer reference for tracing */

    cfi.base_addr = CFI_DEVICE_BASE_ADDR;
    cfi_init(&cfi);
}
#endif
