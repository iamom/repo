#include "kernel/nu_kernel.h"
#include "am335x_pbdefs.h"
#include "HAL_AM335x_GPIO.h"
#include "gtypes.h"
#include "pbos.h"
#include "commontypes.h"
#include "stdio.h"
#include "stddef.h"
#include "hal.h"
#include "clock.h"
#include "bsp/csd_2_3_defs.h"
#include "debugTask.h"

#include "feeder_pwm.h"

static GPIO_INFO		feedEnbGPIOInfo = {AM335X_CTRL_PADCONF_GPMC_A9, 		AM335X_GPIO_BIT_25, GPIO_BANK_1, DIR_OUTPUT, GPIO_PULL_OFF};
static PEWPMSS_STRUCT	localPWMSS0		= (PEWPMSS_STRUCT)EPWMSS0_BASE;
static PWM0Control		localPWM0Control ;
static BOOL				localPWM0Initialized = FALSE;

/////////////////////////////////////////////////////////////////////////////////////////////////
// initializePWM0Control
//
// Notes:
//		PWM output ehrpwm0A is active low, so clearing this pin makes motor run.
//
STATUS initializePWM0Control()
{
	static PEPWM_STRUCT		localPWM0 		= (PEPWM_STRUCT)EPWM0_BASE;
	STATUS	status = NU_SUCCESS;
	if(!localPWM0Initialized)
	{
		setupGPIOPin(feedEnbGPIOInfo);
		ESAL_GE_MEM_WRITE32(EPWM0_CLKCTRL, PWM_WKP_ENABLE);
		while((ESAL_GE_MEM_READ32(EPWM0_CLKCTRL) >> 16) != 0) ;

		ESAL_GE_MEM_WRITE32(ESAL_PR_CONTROL_BASE + EPWMSS0_CTRLOFFSET, 1);

		// Set PWM output pin GPIO#,14 == ehrpwm0A (mux1)
		UINT32 *pwmOut = addressGPIOPin(3, 14) ;
		// set mux1  rx disabled
		*pwmOut = 1 + (1 << 3);

		localPWM0->TBCTL = 0;

		localPWMSS0->SYSCONFIG	= (1 << 4) + (1 << 2);		// out of smart standby mode and smart idle mode
		localPWMSS0->CLKCONFIG	= (1 << 8) + (1 << 4) + 1 ; // enable the PWM, QEP and eCAP

		// not sure what the high speed section gets us so zero it.
		localPWM0->TBPHSHR		= 0;
		localPWM0->TBPHS		= 0;
		localPWM0->CMPAHR		= 0;

		// load cmpA on TB== 0
		//						load Ctr=0   CmpA in shadow
		localPWM0->CMPCTL		= (0)     +   (0)             ;
		// lets run with counter compare A. Reset the output on 0 and set it when counter equals compA
		//	                     set=0   set=Period	 clr=cmpA
		localPWM0->AQCTLA		= 2 + 	(2 << 2) + 	(1 << 4);
		localPWM0->AQCTLB		= 0;

		// initialize at a value outside - keep everything off for now.
		localPWM0->CMPA			= 0xFFFF;
		localPWM0->CMPB			= 0xFFFF;

		// Set the counter period (number of ticks before the subsystem resets to 0)
		localPWM0Control.period = 0xFFF0;
		localPWM0->TBPRD 		= localPWM0Control.period;

		localPWM0Control.feedEnabled	= FALSE ;

		// through measurement it appears that the TB source clock is running at 62.5MHz
		// TB count value = SrcClk/Req TB Freq
		// so for a 10KHz TB clock use 62.5e6/10e3
		// = 6250 (0x186A)

		localPWM0Initialized	= TRUE;
	}
	return status;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
VOID displayPWM0Registers()
{
	static PEPWM_STRUCT		localPWM0 		= (PEPWM_STRUCT)EPWM0_BASE;
	debugDisplayOnly("TBCTL %04X  TBSTS %04X  TBCNT %04X  TBPRD %04X  CMPA %04X  AQCTLA %04X\r\n",
			localPWM0->TBCTL,
			localPWM0->TBSTS,
			localPWM0->TBCNT,
			localPWM0->TBPRD,
			localPWM0->CMPA,
			localPWM0->AQCTLA);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
UINT16 readPWM0TBCounterRegister()
{
	static PEPWM_STRUCT		localPWM0 		= (PEPWM_STRUCT)EPWM0_BASE;
	return localPWM0->TBCNT ;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
VOID writePWM0TBPeriodRegister(UINT16 uTB)
{
	static PEPWM_STRUCT		localPWM0 		= (PEPWM_STRUCT)EPWM0_BASE;
	localPWM0Control.period = uTB ;
	localPWM0->TBPRD = uTB ;
}

VOID setPWM0ZeroScalingTBFreq(UINT uHz)
{
	writePWM0TBPeriodRegister((UINT16)(EPWM0_SYSCLKFREQ/uHz));
}


/////////////////////////////////////////////////////////////////////////////////////////////////
VOID setPWM0ScalingFactors(UINT16 hspclkdiv, UINT16 clkdiv)
{
	static PEPWM_STRUCT		localPWM0 		= (PEPWM_STRUCT)EPWM0_BASE;
	hspclkdiv 	= hspclkdiv > 7 ? 7 : hspclkdiv;
	clkdiv 		= clkdiv > 7 ? 7 : clkdiv;

	localPWM0->TBCTL		&= ~(( hspclkdiv << 7) + (clkdiv << 10));
	localPWM0->TBCTL		|= ( hspclkdiv << 7) + (clkdiv << 10);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
VOID setPWM0DutyCyclePercentage(UINT32 uPct)
{
	static PEPWM_STRUCT		localPWM0 		= (PEPWM_STRUCT)EPWM0_BASE;
	uPct = MIN(uPct, 100) ;
	UINT16 uCmp = (UINT16)((uPct * (UINT32)(localPWM0Control.period))/100);
	localPWM0->CMPA = uCmp;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
VOID setPWM0DutyCyclePerThousandths(UINT32 uPthsn)
{
	static PEPWM_STRUCT		localPWM0 		= (PEPWM_STRUCT)EPWM0_BASE;
	uPthsn = MIN(uPthsn, 1000) ;
	UINT16 uCmp = (UINT16)((uPthsn * (UINT32)(localPWM0Control.period))/1000);
	localPWM0->CMPA = uCmp;
}

//////////////////////////////////////////////////////////////////////////////////////
// enableFeeder
//
// set the enable feeder gate
void enableFeeder()
{
	setGPIOPin(feedEnbGPIOInfo, 1);
	localPWM0Control.feedEnabled = TRUE ;
}

//////////////////////////////////////////////////////////////////////////////////////
// disableFeeder
//
// clear the enable feeder gate
void disableFeeder()
{
	setGPIOPin(feedEnbGPIOInfo, 0);
	localPWM0Control.feedEnabled = FALSE;
}

//////////////////////////////////////////////////////////////////////////////////////
// feederEnabled
//
// return the value of the enable feeder gate
BOOL feederEnabled()
{
	return localPWM0Control.feedEnabled ;
}


