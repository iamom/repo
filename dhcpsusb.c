/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    dhcpsusb.c

   DESCRIPTION:    DHCP Server for USB ports initialization.

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------

*************************************************************************/

/* Includes */
#include "networking/nu_networking.h"
#include "dhcpsusb.h"
#include "pbos.h"
#include "ossetup.h"
#include "networkmonitor.h"

#if defined(CFG_NU_OS_NET_DHCP_SERVER_IPV4_ENABLE) || defined(CFG_NU_OS_NET_DHCP_SERVER_IPV6_ENABLE)

/* Configuration Parameters */
CHAR    DHCPSrv_Subnet_Mask[]           = {(CHAR) 255,(CHAR) 255,(CHAR) 255,(CHAR) 240};
CHAR    DHCPSrv_Broadcast_Addr[]        = {(CHAR) 200,(CHAR) 100,(CHAR) 200,(CHAR) 255};
CHAR    DHCPSrv_Subnet_Addr[]           = {(CHAR) 192,(CHAR) 168,(CHAR) 10,(CHAR) 240};
CHAR    *DHCPSrv_Domain_Name            = "pb.com";
UINT32  DHCPSrv_Lease_Time              = 0x0000003C;           /* 1 minutes */
UINT32  DHCPSrv_Renewal_T1_Time         = 0x0000001E;           /* 30 seconds */
UINT32  DHCPSrv_Rebind_T2_Time          = 0x00000032;           /* 50 seconds */
UINT32  DHCPSrv_Offered_Wait_Time       = 0x00000096;           /* 2.5 minutes */

CHAR    DHCPSrv_IP_Range_Low[]          = {(CHAR) 192,(CHAR) 168,(CHAR) 10,(CHAR) 241};
CHAR    DHCPSrv_IP_Range_High[]         = {(CHAR) 192,(CHAR) 168,(CHAR) 10,(CHAR) 244};

/* Static Entry Info */
UINT8   DHCPSrv_Static_Client_Haddr_1[] = {(UINT8) 0x00,(UINT8) 0x01,(UINT8) 0x02,(UINT8) 0x03,
                                           (UINT8) 0x04,(UINT8) 0x05};

CHAR    DHCPSrv_Static_Client_Haddr_Type_1 = ETHERNET_DEVICE; 
UINT8   DHCPSrv_Static_Client_IP_1[]    = {(CHAR) 192,(CHAR) 168,(CHAR) 10,(CHAR) 245};

/* Task parameters */
#define USB_INIT_TASK_PRIORITY            (UIC_NORMAL_PRIORITY - 4)
#define TASK_STACK_SIZE                  5300

extern NU_MEMORY_POOL    *pSystemMemoryCached;

/* Define Application data structures.  */
NU_TASK                 dhcpsrv_init_task_cb;

/* Define prototypes for function references.  */
VOID DHCPS_Init_Task(UNSIGNED argc, VOID *argv);
#endif

/******************************************************************************
*                                                                       
* FUNCTION                                                              
*                                                                       
*      DHCPS_USB_Init
*                                                                       
* DESCRIPTION                                                           
*                                                                       
*       Initializes the tasks, queues and events used by the application.
*                                                                       
*                                                                       
******************************************************************************/
STATUS DHCPS_USB_Init(VOID)
{
    
    VOID           *pointer;
    STATUS         status = NU_SUCCESS;
    
#if defined(CFG_NU_OS_NET_DHCP_SERVER_IPV4_ENABLE) || defined(CFG_NU_OS_NET_DHCP_SERVER_IPV6_ENABLE)

    /* Allocate stack space for and create each task in the system.  */
    
    /* Allocate stack space for and create the DHCPServ Init Task. */
    status = NU_Allocate_Memory(pSystemMemoryCached, &pointer, TASK_STACK_SIZE, NU_NO_SUSPEND);

    if (status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_ERROR,"Can not create memory for DHCPS Init Task.\r\n");
    }
    
    /* Create the DHCPServ_Init task. */
    status = NU_Create_Task(&dhcpsrv_init_task_cb, "DHCPSINIT",
    		DHCPS_Init_Task, 0, NU_NULL, pointer,
                            TASK_STACK_SIZE, USB_INIT_TASK_PRIORITY, 0, NU_PREEMPT,
                            NU_START);

    if (status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_ERROR,"Cannot create DHCP_Server Task\r\n");
    }
#endif

    return (status);

}   /* end DHCPS_System_Init */

#if defined(CFG_NU_OS_NET_DHCP_SERVER_IPV4_ENABLE) || defined(CFG_NU_OS_NET_DHCP_SERVER_IPV6_ENABLE)
/******************************************************************************************
*
* FUNCTION                                                              
*                                                                       
*      DHCPS_Init_Task
*                                                                       
* DESCRIPTION                                                           
*                                                                       
*      Initialize and setup the DHCP server with desired parameters.
*
******************************************************************************************/
// RD- we will probably need access to these structure to update DNS information
DHCPS_CONFIG_CB     *globalGlobalDHCPCtrlBlk;
DHCPS_CONFIG_CB     *globalDeviceDHCPCtrlBlk;
VOID DHCPS_Init_Task (UNSIGNED argc, VOID *argv)
{
    STATUS              status;

    struct id_struct    subnet_mask,
                        subnet_addr,
                        broadcast_addr,
                        static_ip_addr,
                        ip_range_low,
                        ip_range_high;


    DHCPS_CLIENT_ID     client_id;

    UINT8               binding_buff[40],
                        *buff_ptr;
    INT                 buff_size = 40,
                        ret_bytes;
    
    /* Remove compiler warnings */
    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);

    /* Wait until the NET stack is initialized and the driver
       is up and running. */
    NETBOOT_Wait_For_Network_Up(NU_SUSPEND);

    /* Reset the server so that the initial configurations done by Nucleus are reverted and user can
       control the DHCP server through APIs.  */
    while ((status = DHCPS_Server_Reset()) != NU_SUCCESS)
    {
        NU_Sleep(100);
    }

    if (status == NU_SUCCESS)
    {
        /* Create a global configuration global control block. */
#if 1
        if (DHCPS_Create_Config_Entry(&globalGlobalDHCPCtrlBlk, "GLOBAL", 0) == NU_SUCCESS)
        {
            /* We can now add the configuration parameters to the global control block. */

            /* Copy the subnet mask into a structure. */
            memcpy(&subnet_mask, DHCPSrv_Subnet_Mask, IP_ADDR_LEN);

            /* Add the Subnet mask to the global control block. */
            status = DHCPS_Set_Subnet_Mask(globalGlobalDHCPCtrlBlk, NU_NULL, &subnet_mask);

            if (status != NU_SUCCESS)
            {
                dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Set_Subnet_Mask.\r\n");
            }    

            /* Copy the broadcast address into a structure. */
            memcpy(&broadcast_addr, DHCPSrv_Broadcast_Addr, IP_ADDR_LEN);

            /* Add the broadcast address to the global control block. */
            status = DHCPS_Add_Broadcast_Address(globalGlobalDHCPCtrlBlk, NU_NULL, &broadcast_addr);

            if (status != NU_SUCCESS)
            {
                dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Add_Broadcast_Address.\r\n");
            }                



            /* Add the domain name to the global control block. */
            status = DHCPS_Add_Domain_Name(globalGlobalDHCPCtrlBlk, NU_NULL, DHCPSrv_Domain_Name);

            if (status != NU_SUCCESS)
            {
                dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Add_Domain_Name.\r\n");
            }                            

            /* Add the lease time to the global control block. */
            status = DHCPS_Set_Default_Lease_Time(globalGlobalDHCPCtrlBlk, NU_NULL, DHCPSrv_Lease_Time);

            if (status != NU_SUCCESS)
            {
                dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Set_Default_Lease_Time.\r\n");
            }                            

            /* Add the renewal time to the global control block. */
            status = DHCPS_Set_Renewal_Time(globalGlobalDHCPCtrlBlk, NU_NULL, DHCPSrv_Renewal_T1_Time);

            if (status != NU_SUCCESS)
            {
                dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Set_Renewal_Time.\r\n");
            }                

            /* Add the rebinding time to the global control block. */
            status = DHCPS_Set_Rebind_Time(globalGlobalDHCPCtrlBlk, NU_NULL, DHCPSrv_Rebind_T2_Time);

            if (status != NU_SUCCESS)
            {
                dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Set_Rebind_Time.\r\n");
            }                            

            /* Add the offered wait time to the global control block. */
            status = DHCPS_Set_Offered_Wait_Time(globalGlobalDHCPCtrlBlk, NU_NULL, DHCPSrv_Offered_Wait_Time);

            if (status != NU_SUCCESS)
            {
                dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Set_Offered_Wait_Time.\r\n");
            }

            /* Enable the global configuration control block. */
            status = DHCPS_Enable_Configuration(globalGlobalDHCPCtrlBlk);
        }
        else
        {
            dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Create_Config_Entry.\r\n");
        }
#endif
#if 1
		/* Add a new configuration control block for the IP range that we are about to add. */
		if (DHCPS_Create_Config_Entry(&globalDeviceDHCPCtrlBlk, "SUBNET1", "net1") == NU_SUCCESS) // USB-C = net1, USB-B = net0
		{
			/* Copy the IP range hi and low into id_structs. */
			memcpy(&ip_range_low, DHCPSrv_IP_Range_Low, IP_ADDR_LEN);
			memcpy(&ip_range_high, DHCPSrv_IP_Range_High, IP_ADDR_LEN);

			/* Add IP range to the control block. */
			status = DHCPS_Add_IP_Range(globalDeviceDHCPCtrlBlk, NU_NULL, &ip_range_low, &ip_range_high);

			if (status != NU_SUCCESS)
			{
				dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Add_IP_Range.\r\n");
			}

			/* Copy the subnet address into an id_struct. */
			memcpy(&subnet_addr, DHCPSrv_Subnet_Addr, IP_ADDR_LEN);

			/* Add the subnet address to the control block. */
			status = DHCPS_Set_Subnet_Address(globalDeviceDHCPCtrlBlk, NU_NULL, &subnet_addr);

			if (status != NU_SUCCESS)
			{
				dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Set_Subnet_Address.\r\n");
			}

			/* Copy a separate IP address into the id struct. */
			memcpy(&static_ip_addr, DHCPSrv_Static_Client_IP_1, IP_ADDR_LEN);

			/* Copy the hardware address of the client into a hardware address struct. */
			memcpy(client_id.id, DHCPSrv_Static_Client_Haddr_1, DADDLEN);

			/* Set the length of the hardware address. */
			client_id.idlen = DADDLEN;

			/* Set the hardware type to ethernet. */
			client_id.idtype = DHCPSrv_Static_Client_Haddr_Type_1;

			/* Add the address to the control block. */
			status = DHCPS_Add_IP_Range(globalDeviceDHCPCtrlBlk, &client_id , &static_ip_addr, NU_NULL);

			if ((status != NU_SUCCESS) && (status != DHCPSERR_BINDING_ALREADY_PRESENT))
			{
				dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Add_IP_Range.\r\n");
			}

#if 0
			// RD we need to set the gateway address in our DHCP server this should be set to our net1 address...
			struct id_struct routAdds ;
			stringToIP4Array((UINT8 *)&routAdds, NET1_IP_ADDRESS);
			status = DHCPS_Add_Router (globalDeviceDHCPCtrlBlk,NULL,&routAdds);
			if((status != NU_SUCCESS) && (status != DHCPSERR_OPTION_ALREADY_PRESENT))
			{
				//DHCPSERR_OPTION_ALREADY_PRESENT
				dbgTrace(DBG_LVL_INFO, "DHCPServer adding gateway address failed [%s]\r\n", status);
			}
#endif

			/* Enable the device control block. */
			status = DHCPS_Enable_Configuration(globalDeviceDHCPCtrlBlk);

			if (status != NU_SUCCESS)
			{
				dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Enable_Configuration.\r\n");
			}

			/* Set a pointer to the head of the buffer. */
			buff_ptr = &binding_buff[0];

			/* Get the IP addresses of all of the bindings. */
			ret_bytes = DHCPS_Get_IP_Range(globalDeviceDHCPCtrlBlk, buff_ptr, buff_size);

			if (ret_bytes < 0)
			{
				dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Get_IP_Range.\r\n");
			}

		}
		else
		{
			dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Create_Config_Entry SUBNET1 - net1.\r\n");
		}
#endif


		if (status != NU_SUCCESS)
		{
			dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Enable_Configuration.\r\n");
		}
		/* Save off all of the configuration control blocks to a file. */
		status = DHCPS_Save_Config_Data_To_File();

		if (status != NU_SUCCESS)
		{
			dbgTrace(DBG_LVL_ERROR,"Error occurred within DHCPS_Save_Config_Data_To_File.\r\n");
		}
    }    
    else
    {
        dbgTrace(DBG_LVL_ERROR,"DHCPS_Init error.\r\n");
    }    
}
#endif

