#include "kernel/nu_kernel.h"
#include "am335x_pbdefs.h"
#include "HAL_AM335x_GPIO.h"
#include "gtypes.h"
#include "pbos.h"
#include "commontypes.h"
#include "stdio.h"
#include "stddef.h"
#include "hal.h"
#include "clock.h"
#include "bsp/csd_2_3_defs.h"
#include "debugTask.h"
#include "am335x_timer.h"
#include "feeder_qep.h"
#include "feeder_pwm.h"
#include "feeder_timer.h"


static BOOL				localFeederInitialized 		= 0 ;
static VOID             (*old_feederLISR)(INT);
static NU_HISR			feederTimer1HISR_CB;
static UINT32			feeder1msLISRCount			= 0;


//////////////////////////////////////////////////////////////////////////
STATUS initializeFeederControl()
{
	STATUS status = NU_SUCCESS;
	if(!localFeederInitialized)
	{
		// cannot use HAL timer prep code since timer1 is different from the others.
		//prepTimerForUpCounting(FEEDER_TIMER_INDEX);
		prepTimer1() ;
		initializePWM0Control();
		initializeQEP();
		localFeederInitialized = 1;
		debugDisplayOnly("Feeder Initialized\r\n");
	}
	return status;
}

//////////////////////////////////////////////////////////////////////////
//	preTimer1
//
//	Timer 1 register structure is different from all the other timers
//	Set this up for auto reload on overflow and set it to generate
//	an interrupt on overflow.
//
VOID prepTimer1()
{
    STATUS 			status;
    VOID			*hisrStack ;
    NU_MEMORY_POOL 	*mem_pool;
    PTIMER1_STRUCT	localTimer1 				= (PTIMER1_STRUCT)TIMER1_BASE;

	// enable the wakeup clock. First make sure it is off then enable it then reset the module...
	ESAL_GE_MEM_WRITE32(CM_WKUP_TIMER1_CLKCTRL, 0);
	ESAL_GE_MEM_WRITE32(CM_WKUP_TIMER1_CLKCTRL, TIMER1_WKP_ENABLE);
	// loop until module is ready....
	while((ESAL_GE_MEM_READ32(CM_WKUP_TIMER1_CLKCTRL) & TIMER1_WKUPCLOCK_MASK) != 0) ;
	//
	localTimer1->TWER 	= 0;									// no wakeup capability
	localTimer1->TCLR 	|= TIMER1_AUTO_RELOAD ;					// set auto reload
	localTimer1->TLDR 	= TIMER1_INIT_LOAD;						// load  counter start value
	localTimer1->TTGR	= 1;									// any value triggers a reload.
	//
    status = NU_Register_LISR(ESAL_PR_INT_IRQ67_TINT1_1MS, feederTimer1LISR,&old_feederLISR);
    if(status == NU_SUCCESS)
    {
		/* Retrieve a pointer to the system memory resources. */
		status = NU_System_Memory_Get(&mem_pool, NULL);
    }

    // create an HISR for this interrupt handling
    if(status == NU_SUCCESS)
    {
    	status = NU_Allocate_Memory(mem_pool, &hisrStack, TIMER1_HISR_STACKSIZE, NU_SUSPEND);
    }
    if(status == NU_SUCCESS)
    {
    	status = NU_Create_HISR(&feederTimer1HISR_CB, "FEED1MS", feederTimer1HISR, 2, hisrStack, TIMER1_HISR_STACKSIZE);
    }
    if(status != NU_SUCCESS)
    {
    	dbgTrace(DBG_LVL_INFO, "ERROR creating feeder timer1 HISR\r\n");
    }
    // LISR and HISR installed start the timer module...
	//
	localTimer1->TCLR	|= TIMER1_CNT_START ;					// start the timer
}

//////////////////////////////////////////////////////////////////////////////////////
VOID setTimer1PeriodUS(UINT32 uUs)
{
    PTIMER1_STRUCT	localTimer1 				= (PTIMER1_STRUCT)TIMER1_BASE;
	localTimer1->TLDR = 0xFFFFFFFF - (TIMER1_COUNTS_PER_US * uUs) ;
}

//////////////////////////////////////////////////////////////////////////////////////
VOID setTimer1PeriodMS(UINT32 uMs)
{
    PTIMER1_STRUCT	localTimer1 				= (PTIMER1_STRUCT)TIMER1_BASE;
	localTimer1->TLDR = 0xFFFFFFFF - (TIMER1_COUNTS_PER_MS * uMs) ;
}

//////////////////////////////////////////////////////////////////////////////////////
BOOL isTimer1ISRActive()
{
    PTIMER1_STRUCT	localTimer1 				= (PTIMER1_STRUCT)TIMER1_BASE;
	return (localTimer1->TIER & TIMER1_OVERFLOW_ENB) > 0 ? TRUE : FALSE;
}

//////////////////////////////////////////////////////////////////////////////////////
VOID startTimer1ISR()
{
    PTIMER1_STRUCT	localTimer1 				= (PTIMER1_STRUCT)TIMER1_BASE;
    // enable the overflow interrupt
    localTimer1->TIER |= TIMER1_OVERFLOW_ENB;
    ESAL_GE_INT_Enable(ESAL_PR_INT_IRQ67_TINT1_1MS,0,0x5);
}

//////////////////////////////////////////////////////////////////////////////////////
VOID stopTimer1ISR()
{
    PTIMER1_STRUCT	localTimer1 				= (PTIMER1_STRUCT)TIMER1_BASE;
    localTimer1->TIER &= ~TIMER1_OVERFLOW_ENB;
    ESAL_GE_INT_Disable(ESAL_PR_INT_IRQ67_TINT1_1MS);

}

//////////////////////////////////////////////////////////////////////////////////////
UINT32 readFeederTimerValue()
{
    PTIMER1_STRUCT	localTimer1 				= (PTIMER1_STRUCT)TIMER1_BASE;
	return localTimer1->TCRR;
}

//////////////////////////////////////////////////////////////////////////////////////
VOID    feederTimer1LISR(INT vector)
{
	static PTIMER1_STRUCT	localTimer1 	= (PTIMER1_STRUCT)TIMER1_BASE;

	// clear all interrupt flags...
	localTimer1->TISR = 0x7;
	feeder1msLISRCount++;
	NU_Activate_HISR(&feederTimer1HISR_CB) ;
}

//////////////////////////////////////////////////////////////////////////////////////
//	feederTimer1HISR
//
// high level ISR
VOID feederTimer1HISR()
{
#if 0
	// produce a print every HISR call and a timestamp every 10 calls.
	if((feeder1msLISRCount % 10) == 0)
	{
	    DATETIME        rDateTimeStamp;
	    /* get the system date/time so that this can be recorded along with the error */
	    GetSysDateTime(&rDateTimeStamp, ADDOFFSETS, GREGORIAN);
		dbgTrace(DBG_LVL_INFO, "Feeder HISR Activated %5lu  %2d:%2d:%2d\r\n", feeder1msLISRCount,
				(UINT)rDateTimeStamp.bHour, (UINT)rDateTimeStamp.bMinutes, (UINT)rDateTimeStamp.bSeconds) ;

	}
	else
		dbgTrace(DBG_LVL_INFO, "Feeder HISR Activated %5lu\r\n", feeder1msLISRCount) ;
#endif
}
