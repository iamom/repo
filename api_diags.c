/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    api_diags.c

   DESCRIPTION:    API implementations for diagnostic messages

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
*   MODIFICATION HISTORY:
*
* 2018.02.07 - CWB 
* - Remove recent modifications not related to waste tank.
* - Remove the EVT_CM_PMNVM_AVAILABLE flag.  Acsess will only be 
*   controlled by the disabling condition DCOND_ACCESSING_PM_NVM. 
*   This will not only signal to the tablet that the meter is busy 
*   and should not go to printing or maintenance, but also to the 
*   API task that the process and data is still being used. 
*
*************************************************************************/

#include <ctype.h>          /* for isdigit() prototype */
#include "storage/nu_storage.h"
#include "commontypes.h"
#include "pbos.h"
#include "cJSON.h"
#include "wsox_tx_queue.h"
#include "wsox_services.h"
#include "cm.h"
#include "cmpublic.h"
#include "oitcm.h"
#include "oit.h"
#include "UTILS.h"
#include "flashutil.h"
#include "networkmonitor.h"
//#include "apphdr.h"
#include "debugTask.h"
#include "btldr_fram.h"
//#include "nor.h"
#include "cfi.h"
#include "errno.h"
#define __LOG_NAMES__
#include "api_funds.h"
#include "api_utils.h"
#include "sig.h"
#include "cmos.h"
#include "chasing_shadows.h"
#include "hal.h"
#include "btldr_fram.h"
#include "sysdatadefines.h"


#define  CRLF   "\r\n"

extern CMOS_Signature CMOSSignature;
extern SetupParams CMOSSetupParams;
extern NETCONFIG CMOSNetworkConfig;
extern const char *api_status_to_string_tbl[];
extern const char *log_names[];
char *get_our_ip_addr_as_string(char *ip_addr);
extern unsigned char ucInkTankMfgType;
extern UINT8 globalDHCPProcessActive;

// Type of Diagnostic Print transaction
typedef enum {
    DP_START_TRANSACTION,
    DP_END_TRANSACTION,
    DP_COMPLETE_TRANSACTION
} dpTransaction;

#define TIMEOUT_AVAIL   2000
#define TIMEOUT_RESP    600


#define MAX_SIMULATION_INTERVAL  255
#define MAX_NUM_SIMULATIONS      500000

static MAIL_SIMULATION wMailSimulation={0,0,FALSE,FALSE,FALSE,TRUE};

required_fields_tbl_t required_fields_tbl_PrintDiagnosticReq =
    {
    "PrintDiagnosticReq", { "OperationCode"
                           , "OperationParam"
                           , "TransportSpeed"
                           , NULL }
    };

STATUS binaryToASCII(unsigned char *pIn, int inSize, unsigned char *pOut, int outMaxSize, int *outSize);
typedef char CRASH_FILE_NAME[70];



// ----------------------------------------------------------------------------
//  Prototypes for local functions: 

void fnGetPrinterNVRAMData(cJSON *pPrinterNVRAMData, NVRAM_DATA *pPmNvmData);
void fnGetPrintHeadInfo( cJSON *pPrintHead, NVRAM_DATA *pPmNvmData );
void fnGetInkTankInfo( cJSON *pInkTank, NVRAM_DATA *pPmNvmData );
static UINT8 fnGetCrashFilelist(CRASH_FILE_NAME *pList, UINT8 maxListSize);

 

/* -------------------------------------------------------------------------------------------------[sendDiagMessage]-- */
static BOOL sendDiagMessage(UINT8 opCode, UINT8 opParam)
{
    BOOL msgStat = SUCCESSFUL;
    UINT8     pMsgData[2];    /* send 2 bytes- 1st is op code, 2nd is op param */

    pMsgData[0] = opCode;
    pMsgData[1] = opParam;

    msgStat = OSSendIntertask(BJCTRL, CM, CP_PERFORM_PM_DIAG, BYTE_DATA, pMsgData, sizeof(unsigned char) * 2);
    return msgStat;
}

/* -------------------------------------------------------------------------------------------------[sendModeMessage]-- */
static BOOL sendModeMessage(UINT8 mode, UINT8 transportSpeed, UINT8 numRuns, UINT8 interval)
{
    BOOL msgStat = SUCCESSFUL;
    static SET_MODE_DATA modeReq;

    modeReq.bMode = mode;
    modeReq.bTransportSpeed = transportSpeed;
    modeReq.lwInterval = interval;
    modeReq.lwTimes = numRuns;

    msgStat = OSSendIntertask(BJCTRL, CM, CP_SET_MODE, GLOBAL_PTR_DATA, &modeReq, sizeof(SET_MODE_DATA));
    return msgStat;
}

/* -------------------------------------------------------------------------------------------------[doDiagnostic]-- */
bool doDiagnostic(dpTransaction xactionType, UINT8 transportSpeed, UINT8 opCode, UINT8 opParam)
{
    static bool diagMode = false;

    switch(xactionType)
    {
        case DP_COMPLETE_TRANSACTION:
            {// assume in normal mode
                if (sendModeMessage(DIAG_MODE, transportSpeed, 0, 0) != SUCCESSFUL)
                    return false;

                // Wait a little
                OSWakeAfter(100);

                if (sendDiagMessage(opCode, opParam) != SUCCESSFUL)
                    return false;

                // Wait a little
                OSWakeAfter(100);

                if (sendModeMessage(NORMAL_MODE, transportSpeed, 0, 0) != SUCCESSFUL)
                    return false;
            }
            break;

        case DP_START_TRANSACTION:
            {
                if (!diagMode)
                {// enter diag mode if not in it already
                    diagMode = true;
                    if (sendModeMessage(DIAG_MODE, transportSpeed, 0, 0) != SUCCESSFUL)
                        return false;
                    // Wait a little
                    OSWakeAfter(100);
                }

                if (sendDiagMessage(opCode, opParam) != SUCCESSFUL)
                    return false;
            }
            break;

        case DP_END_TRANSACTION:
            {
                if (diagMode)
                {// must be in diag mode already
                    if (sendDiagMessage(opCode, opParam) != SUCCESSFUL)
                        return false;

                    // Wait a little
                    OSWakeAfter(100);

                    if (sendModeMessage(NORMAL_MODE, transportSpeed, 0, 0) != SUCCESSFUL)
                        return false;
                    diagMode = false;
                }
                else
                {// invalid state - not in diag mode
                    return false;
                }
            }
            break;

        default:
            {// invalid transaction type
                return false;
            }
            break;

    }
    return true;
}

static BOOL validatePrintDiagFields(cJSON *invalid, UINT8 transportSpeed, UINT8 opCode, UINT8 opParam)
{
    BOOL passed = TRUE;

    if ((transportSpeed < IPS_7_5) ||
            ((fnGetMeterModel() == CSD2) && (transportSpeed > IPS_12_2)) ||
            ((fnGetMeterModel() == CSD3) && (transportSpeed > IPS_24))) // Not preventing CSD3 from running at low speeds
    {
        passed = FALSE;
        cJSON_AddItemToArray(invalid, CreateNumberItem("TransportSpeed", transportSpeed));
    }

    if (((fnGetMeterModel() == CSD2) && (opCode > DIAG_OPTCODE_WIPE_HEAD)) ||
            ((fnGetMeterModel() == CSD3) && (opCode > DIAG_OPTCODE_ACTV_TAPE_MOTOR)))
    {
        passed = FALSE;
        cJSON_AddItemToArray(invalid, CreateNumberItem("OperationCode", opCode));
    }

    return passed;
}

/* ----------------------------------------------------------------------------------------[on_PrintDiagnosticReq]-- */
/*
 * The parameters TransportSpeed, OperationCode and OperationParam are the same as those specified
 * in the Horizon Print Manager Interface Specification (Set Mode and Perform Print Manager Diagnostic commands).
 * It is assumed that the caller will call the multi-command sequences in the proper order.  There is minimal
 * checking for proper sequences. See the comments below for the proper sequences.
 */
void on_PrintDiagnosticReq(UINT32 handle, cJSON *root)
{
    UINT8     opCode;
    UINT8     opParam;
    UINT8     transportSpeed;
    bool      retVal = false;

    cJSON *invalid;
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    if (!RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_PrintDiagnosticReq))
    {
        return generateResponse(&entry, rspMsg, "PrintDiagnostic", api_status_to_string_tbl[API_STATUS_INVALID_PARAMETERS], root);
    }

    opCode = (UINT8) cJSON_GetObjectItem(root, "OperationCode")->valueint;
    opParam = (UINT8) cJSON_GetObjectItem(root, "OperationParam")->valueint;
    transportSpeed = (UINT8) cJSON_GetObjectItem(root, "TransportSpeed")->valueint;

    invalid = cJSON_CreateArray();
    if (!validatePrintDiagFields(invalid, transportSpeed, opCode, opParam))
    {
        cJSON_AddItemToObject(rspMsg, "Invalid", invalid);
        return generateResponse(&entry, rspMsg, "PrintDiagnostic", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
    }

    // Set mode and type of transaction based on type of operation
    //TODO - more checking for proper multi-command sequences
    switch (opCode)
    {
        case DIAG_OPTCODE_ACTV_PURGE_MOTOR:
            {
                retVal = doDiagnostic(DP_COMPLETE_TRANSACTION, transportSpeed, opCode, opParam);
            }
            break;

        case DIAG_OPTCODE_ACTV_TRNAS_MOTOR:
            {
                switch (opParam)
                {// NORMAL_TEST (START) and STOP are proper set of commands
                    case DIAG_TRANS_MOTOR_STOP:
                        {
                            retVal = doDiagnostic(DP_END_TRANSACTION, transportSpeed, opCode, opParam);
                        }
                        break;
                    case DIAG_TRANS_MOTOR_NORMAL_TEST:
                        {
                            retVal = doDiagnostic(DP_START_TRANSACTION, transportSpeed, opCode, opParam);
                        }
                        break;
                    case DIAG_TRANS_MOTOR_FULL_TEST:
                        {
                            retVal = doDiagnostic(DP_COMPLETE_TRANSACTION, transportSpeed, opCode, opParam);
                        }
                        break;
                    default:
                        {
                        }
                        break;
                }
            }
            break;

        // OPEN CAP and CLOSE CAP are proper set of commands
        case DIAG_OPTCODE_OPEN_CAP:
            {
                retVal = doDiagnostic(DP_START_TRANSACTION, transportSpeed, opCode, opParam);
            }
            break;

        case DIAG_OPTCODE_CLOSE_CAP:
            {
                retVal = doDiagnostic(DP_END_TRANSACTION, transportSpeed, opCode, opParam);
            }
            break;

        // PRINT_POS and HOME_POS are proper set of commands
        case DIAG_OPTCODE_HEAD_TO_PRINT_POS:
            {
                retVal = doDiagnostic(DP_START_TRANSACTION, transportSpeed, opCode, opParam);
            }
            break;

        case DIAG_OPTCODE_HEAD_TO_HOME_POS:
            {
                retVal = doDiagnostic(DP_END_TRANSACTION, transportSpeed, opCode, opParam);
            }
            break;

        // REPL_POS and HOME_POS are proper set of commands
        case DIAG_OPTCODE_HEAD_TO_REPL_POS:
            {
                retVal = doDiagnostic(DP_START_TRANSACTION, transportSpeed, opCode, opParam);
            }
            break;

        case DIAG_OPTCODE_WIPE_HEAD:
            {
                retVal = doDiagnostic(DP_COMPLETE_TRANSACTION, transportSpeed, opCode, opParam);
            }
            break;

        case DIAG_OPTCODE_ACTV_TAPE_MOTOR:
            {
                retVal = doDiagnostic(DP_COMPLETE_TRANSACTION, transportSpeed, opCode, opParam);
            }
            break;

        default:
            {
            }
            break;

    }

    if (retVal == true)
    {
        generateResponse(&entry, rspMsg, "PrintDiagnostic", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
    }
    else
    {
        generateResponse(&entry, rspMsg, "PrintDiagnostic", api_status_to_string_tbl[API_STATUS_NOT_PERFORMED], root);
    }
    cJSON_Delete(invalid);
}


/* *************************************************************************
// FUNCTION NAME: 
//        fnGetCrashFilelist
//
// DESCRIPTION:
//        This function finds all the crash log files in the POWERDOWN_SYSLOG_DIR
//        directory, and returns a list of the filenames, as well as a count,
//        to the calling function.
//
// INPUTS:
//        pList - pointer to filename list (type CRASH_FILE_NAME)
//        maxListSize - maximum number of items in filename list
//
// RETURNS:
//        fileCount - number of crash log files found
//
// WARNINGS/NOTES:
//        pList must point to an existing structure.  NULL pointer will return
//        a file count of 0.  Incorrect maxListSize can cause memory leak
//
// MODIFICATION  HISTORY:
//      06/29/2018    Steve Terebesi  Initial version.  For HORCSD-7089 
// *************************************************************************/
static UINT8 fnGetCrashFilelist(CRASH_FILE_NAME *pList, UINT8 maxListSize)
{
    char   crashMask[15];
    STATUS status = NU_SUCCESS;
    UINT8  fileCount = 0;
    DSTAT  fileInfo;

//  Check if list pointer is valid
    if (pList != NULL_PTR)
    {
//      Create file mask for crash logs
        snprintf(crashMask, 15, "%s/crash_*", POWERDOWN_SYSLOG_DIR);

//      Get the first filename
	    status = NU_Get_First(&fileInfo, crashMask);

        while ((status == NU_SUCCESS) && (fileCount < maxListSize))
        {
//          Store filename in list
            memcpy(&pList[fileCount], fileInfo.lfname, 70);

//          Increment file counter
            fileCount++;

//          Get next filename
		    status = NU_Get_Next(&fileInfo);
        }
//      Finish searching for file
	    NU_Done(&fileInfo);

        if (status < NU_SUCCESS) 
            fnCheckFileSysCorruption(status);
    }

    return (fileCount);
}

/* -------------------------------------------------------------------------------------[fnGetLogURLs]-- */
void fnGetLogURLs(cJSON *pLogURLs)
{
    char                syslog_url[200];
    char                syslogreset_url[200];
    char                netlog_url[200];
    char                errorlog_url[200];
    char                framlog_url[200];
    char                diaglog_url[200];
    char                trmlog_url[200];
    char                baseinst_url[200];
    char                crashlog_url[200];
    CRASH_FILE_NAME     *pFileList;
    UINT8               crashFileCount;
    UINT8               i;
    char                crashURLtag[15];


#ifdef USE_HTTP_URI
    sprintf(syslog_url,"%s://%s/ms/logs/%s",
            "http", get_local_ip_addr_str_from_wsox_handle(handle, addr_str, "192.168.10.246"), log_names[LOG_SYSTEM]);
#else
    sprintf(syslog_url,"https://base.sendpro.pb.com/ms/logs/%s", log_names[LOG_SYSTEM]);
#endif

#ifdef USE_HTTP_URI
    sprintf(netlog_url,"%s://%s/ms/logs/%s",
            "http", get_local_ip_addr_str_from_wsox_handle(handle, addr_str, "192.168.10.246"), log_names[LOG_NETWORK]);
#else
    sprintf(netlog_url,"https://base.sendpro.pb.com/ms/logs/%s", log_names[LOG_NETWORK]);
#endif

#ifdef USE_HTTP_URI
    sprintf(framlog_url,"%s://%s/ms/logs/%s",
            "http", get_local_ip_addr_str_from_wsox_handle(handle, addr_str, "192.168.10.246"), log_names[LOG_FRAM]);
#else
    sprintf(framlog_url,"https://base.sendpro.pb.com/ms/logs/%s", log_names[LOG_FRAM]);
#endif

#ifdef USE_HTTP_URI
    sprintf(errorlog_url,"%s://%s/ms/logs/%s",
            "http", get_local_ip_addr_str_from_wsox_handle(handle, addr_str, "192.168.10.246"), log_names[LOG_ERROR]);
#else
    sprintf(errorlog_url,"https://base.sendpro.pb.com/ms/logs/%s", log_names[LOG_ERROR]);
#endif

#ifdef USE_HTTP_URI
    sprintf(diaglog_url,"%s://%s/ms/logs/%s",
            "http", get_local_ip_addr_str_from_wsox_handle(handle, addr_str, "192.168.10.246"), log_names[LOG_DIAGNOSTICS]);
#else
    sprintf(diaglog_url,"https://base.sendpro.pb.com/ms/logs/%s", log_names[LOG_DIAGNOSTICS]);
#endif

#ifdef USE_HTTP_URI
    sprintf(trmlog_url,"%s://%s/ms/logs/%s",
            "http", get_local_ip_addr_str_from_wsox_handle(handle, addr_str, "192.168.10.246"), log_names[LOG_TRM_FRAM]);
#else
    sprintf(trmlog_url,"https://base.sendpro.pb.com/ms/logs/%s", log_names[LOG_TRM_FRAM]);
#endif

#ifdef USE_HTTP_URI
    sprintf(baseinst_url,"%s://%s/ms/logs/%s",
            "http", get_local_ip_addr_str_from_wsox_handle(handle, addr_str, "192.168.10.246"), log_names[LOG_BASE_INSTALL]);
#else
    sprintf(baseinst_url,"https://base.sendpro.pb.com/ms/dld/logs/%s", log_names[LOG_BASE_INSTALL]);
#endif

    cJSON_AddStringToObject(pLogURLs, "SysLogURL", syslog_url);
    cJSON_AddStringToObject(pLogURLs, "NetLogURL", netlog_url);
    cJSON_AddStringToObject(pLogURLs, "FramLogURL", framlog_url);
    cJSON_AddStringToObject(pLogURLs, "ErrorLogURL", errorlog_url);
    cJSON_AddStringToObject(pLogURLs, "DiagnosticLogURL", diaglog_url);
    cJSON_AddStringToObject(pLogURLs, "TrmFramLogURL", trmlog_url);
    cJSON_AddStringToObject(pLogURLs, "BaseInstallURL", baseinst_url);

//  Allocate space for file list
    pFileList = malloc(sizeof(CRASH_FILE_NAME) * 10);

    if (pFileList != NULL_PTR)
    {

//      Get a list of all the crash logs
        crashFileCount = fnGetCrashFilelist((CRASH_FILE_NAME*)pFileList, 10);

//      Create the URL for each crash file
        for (i = 0; i < crashFileCount; i++)
        {
#ifdef USE_HTTP_URI
            sprintf(crashlog_url,"%s://%s/ms/logs/%s",
                "http", get_local_ip_addr_str_from_wsox_handle(handle, addr_str, "192.168.10.246"), ((CRASH_FILE_NAME*)pFileList)[i]);
#else
            sprintf(crashlog_url,"https://base.sendpro.pb.com/ms/dld/logs/%s", ((CRASH_FILE_NAME*)pFileList)[i]);
#endif

//          Create a unique tag for each file
            snprintf(crashURLtag, 15, "CrashLogURL%1d", i);

//          Add crash log URL string to output 
            cJSON_AddStringToObject(pLogURLs, crashURLtag, crashlog_url);
        }
        //  Free allocated memory for file list
        free(pFileList);
    }

#ifdef USE_HTTP_URI
    sprintf(syslogreset_url,"%s://%s/ms/dld/Logs/%s",
            "http", get_local_ip_addr_str_from_wsox_handle(handle, addr_str, "192.168.10.246"), log_names[LOG_SYSTEM_RESET]);
#else
    sprintf(syslogreset_url,"https://base.sendpro.pb.com/ms/dld/Logs/%s", log_names[LOG_SYSTEM_RESET]);
#endif
    // Add syslog old string to output
    cJSON_AddStringToObject(pLogURLs, "SysLogBeforeResetURL", syslogreset_url);	 
}


/* -------------------------------------------------------------------------------------[on_GetBaseLogURLsReq]-- */
void on_GetBaseLogURLsReq(UINT32 handle, cJSON *root)
{
    char                addr_str[IP_ADDR_STR_LEN];
    cJSON              *rspMsg = cJSON_CreateObject();
    cJSON              *pLogURLs;

    WSOX_Queue_Entry    entry  = { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetBaseLogURLsRsp");

    //LogURLs
    cJSON_AddItemToObject(rspMsg, "LogURLs", pLogURLs = cJSON_CreateObject());
    fnGetLogURLs(pLogURLs);

    addEntryToTxQueue(&entry, root, "  GetBaseLogURLsReq: Added GetBaseLogURLsRsp to tx queue. status=%d" CRLF);
}

/* -------------------------------------------------------------------------------------[on_GetErrorLogUrlReq]-- */
void on_GetErrorLogUrlReq(UINT32 handle, cJSON *root)
{
    char                errorlog_url[200];
    char                addr_str[IP_ADDR_STR_LEN];
    cJSON              *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry    entry  = { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetErrorLogUrlRsp");

#ifdef USE_HTTP_URI
    sprintf(errorlog_url,"%s://%s/ms/logs/%s",
            "http", get_local_ip_addr_str_from_wsox_handle(handle, addr_str, "192.168.10.246"), log_names[LOG_ERROR]);
#else
    sprintf(errorlog_url,"https://base.sendpro.pb.com/ms/logs/%s", log_names[LOG_ERROR]);
#endif

    cJSON_AddStringToObject(rspMsg, "ErrorLogURL", errorlog_url);

    addEntryToTxQueue(&entry, root, "  GetErrorLogUrlReq: Added GetErrorLogUrlRsp to tx queue. status=%d" CRLF);
}

/* -------------------------------------------------------------------------------------[on_GetNetLogUrlReq]-- */
void on_GetNetLogUrlReq(UINT32 handle, cJSON *root)
{
    char                netlog_url[200];
    char                addr_str[IP_ADDR_STR_LEN];
    cJSON              *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry    entry  = { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetNetLogUrlRsp");

#ifdef USE_HTTP_URI
    sprintf(netlog_url,"%s://%s/ms/logs/%s",
            "http", get_local_ip_addr_str_from_wsox_handle(handle, addr_str, "192.168.10.246"), log_names[LOG_NETWORK]);
#else
    sprintf(netlog_url,"https://base.sendpro.pb.com/ms/logs/%s", log_names[LOG_NETWORK]);
#endif

    cJSON_AddStringToObject(rspMsg, "LogURL", netlog_url);

    addEntryToTxQueue(&entry, root, "  GetNetLogUrlReq: Added GetNetLogUrlRsp to tx queue. status=%d" CRLF);
}

/* --------------------------------------------------------------------------------------------[on_GetSystemLogReq]-- */
void on_GetSystemLogReq(UINT32 handle, cJSON *root)
{
    char                syslog_url[200];
    char                addr_str[IP_ADDR_STR_LEN];
    cJSON              *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry    entry  = { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetSystemLogRsp");

#ifdef USE_HTTP_URI
    sprintf(syslog_url,"%s://%s/ms/logs/%s",
            "http", get_local_ip_addr_str_from_wsox_handle(handle, addr_str, "192.168.10.246"), log_names[LOG_SYSTEM]);
#else
    sprintf(syslog_url,"https://base.sendpro.pb.com/ms/logs/%s", log_names[LOG_SYSTEM]);
#endif

    cJSON_AddStringToObject(rspMsg, "SysLogURL", syslog_url);

    addEntryToTxQueue(&entry, root, "  GetSystemLogReq: Added GetSystemLogRsp to tx queue. status=%d" CRLF);
}

 

// ----------------------------------------------------------------------------
// FUNCTION: 							fnGetPrinterNVRAMData
// DESCRIPTION:     
//  	This is called by a function AFTER that function has retrieved a ptr to 
//	a copy of the PM NVM data.  That calling function must make sure that the 
//	access to this data is protected until this function has returned.
//		This function converts and writes the PM NVM data into a json sub-object
//	that is part of a larger json object (message).   
//
//	INPUTS:   pPrinterNVRAMData - ptr to the json sub-object to populate
//			  pPmNvmData	- ptr to the copy of the PM NVM data. 
//  NOTES:  
//	1. PM NVM data shall NOT be accessed directly.  
//  2. This routine only converts the data that is NOT in either the 
//		PRINT_HEAD_DATA structure or the INK_TANK_DATA of the PM NVM data.   
// HISTORY: 
// 2018.02.14 C.Bellamy - modified to use new method of accessing PM NVM data.  
//-------------------------------------------------------------------
void fnGetPrinterNVRAMData( cJSON *pPrinterNVRAMData, NVRAM_DATA *pPmNvmData )
{
    char DateTimeStr[16];
    char sformat[10] = {0};
    ulong dfrmt = pPmNvmData->format;

    //Format
    EndianAwareCopy(sformat, &dfrmt, sizeof(unsigned long));
    cJSON_AddStringToObject(pPrinterNVRAMData, "Format", sformat);

    //Printer Number
    cJSON_AddStringToObject(pPrinterNVRAMData, "Printer Number", (char *)pPmNvmData->printer_number);

    //Feed Paper
    cJSON_AddNumberToObject(pPrinterNVRAMData,      "Feed Paper", pPmNvmData->feed_paper);

    //Print Paper
    cJSON_AddNumberToObject(pPrinterNVRAMData,      "Print Paper", pPmNvmData->print_paper);

    //Print Line
    cJSON_AddNumberToObject(pPrinterNVRAMData,      "Print Line", pPmNvmData->print_line);

    //Head change
    cJSON_AddNumberToObject(pPrinterNVRAMData,      "Head change", pPmNvmData->head_change);

    //Tank change
    cJSON_AddNumberToObject(pPrinterNVRAMData,      "Tank change", pPmNvmData->tank_change);

    //Suck count
    cJSON_AddNumberToObject(pPrinterNVRAMData,      "Suck count", pPmNvmData->suck_count);

    //Waste Ink Count g
    cJSON_AddNumberToObject(pPrinterNVRAMData,      "Waste Ink Count g", pPmNvmData->wast_ink_count_g);

    //Waste Ink Count mg
    cJSON_AddNumberToObject(pPrinterNVRAMData,      "Waste Ink Count mg", pPmNvmData->wast_ink_count_mg);

    //Waste Ink Count ug
    cJSON_AddNumberToObject(pPrinterNVRAMData,      "Waste Ink Count ug", pPmNvmData->wast_ink_count_ug);

    //Waste Ink Count ng
    cJSON_AddNumberToObject(pPrinterNVRAMData,      "Waste Ink Count ng", pPmNvmData->wast_ink_count_ng);

    //cap Ink Count
    cJSON_AddNumberToObject(pPrinterNVRAMData,      "Cap Ink Count", pPmNvmData->cap_ink_count_dot);

    //Head Extra Ink Dot Count
    cJSON_AddNumberToObject(pPrinterNVRAMData,      "Head Extra Ink Dot Count", pPmNvmData->Head_extra_ink_dot_count);

    //Purge Time
    //convert to String
    memset(DateTimeStr, 0x0, sizeof(DateTimeStr));
    sprintf( (char *)DateTimeStr, "20%02d-%02d-%02dT%02d:%02d:%02d",
            pPmNvmData->purge_time_year,
            pPmNvmData->purge_time_month,
            pPmNvmData->purge_time_day,
            pPmNvmData->purge_time_hour,
            pPmNvmData->purge_time_minute,
            pPmNvmData->purge_time_second );

    cJSON_AddStringToObject(pPrinterNVRAMData,      "Purge Time", DateTimeStr);


    //PCB Number
    cJSON_AddStringToObject(pPrinterNVRAMData,      "PCB Number", (char *)pPmNvmData->pcb_number);

    //Battery Install Date
    //convert to string
    memset(DateTimeStr, 0x0, sizeof(DateTimeStr));
    sprintf( (char *)DateTimeStr, "%02d-%02d-%02d",
            pPmNvmData->battery_install_year,
            pPmNvmData->battery_install_month,
            pPmNvmData->battery_install_day);

    cJSON_AddStringToObject(pPrinterNVRAMData,      "Battery Install Date", DateTimeStr);

    //Printer Serial Number
    cJSON_AddStringToObject(pPrinterNVRAMData,      "Printer Serial Number", (char *)pPmNvmData->printer_serial_number);

    //Shut Down Status
    char str[2]={0};
    memcpy(str, &pPmNvmData->shutdown_status, sizeof(pPmNvmData->shutdown_status));
    cJSON_AddStringToObject(pPrinterNVRAMData,      "Shut Down Status", str);

    //Cap Close State
    char str2[2]={0};
    memcpy(str2, &pPmNvmData->cap_close_status, sizeof(pPmNvmData->cap_close_status));
    cJSON_AddStringToObject(pPrinterNVRAMData,      "Cap Close State", str2);

    //Power Fail State
//  cJSON_AddNumberToObject(pPrinterNVRAMData,      "Power Fail State", pPmNvmData.);

}

// ----------------------------------------------------------------------------
// FUNCTION: 							fnGetPrintHeadInfo
// DESCRIPTION:     
//  	This is called by a function AFTER that function has retrieved a ptr to 
//	a copy of the PM NVM data.  That calling function must make sure that the 
//	access to this data is protected until this function has returned.
//		This function converts and writes the PM NVM data into a json sub-object
//	that is part of a larger json object (message).   
//
//	INPUTS:   pPrintHead 	- ptr to the json sub-object to populate
//			  pPmNvmData	- ptr to the copy of the PM NVM data. 
//  NOTES:  
//	1. PM NVM data shall NOT be accessed directly.  
//  2. This routine only converts the data in the PRINT_HEAD_DATA structure in
//		the copy of the PM NVM data.   
// HISTORY: 
// 2018.02.14 C.Bellamy - modified to use new method of accessing PM NVM data.  
//-------------------------------------------------------------------
void fnGetPrintHeadInfo( cJSON *pPrintHead, NVRAM_DATA *pPmNvmData )
{
    PRINT_HEAD_DATA  *pHeadNvmData = &(pPmNvmData->head_eeprom);
    char id[5] = {0};
    char SN[9] ={0};
    char dispDT[9] = {0};
    //ID
    fnPackedBcdToAsciiBcd(id, pHeadNvmData->bId, sizeof(pHeadNvmData->bId));
    cJSON_AddStringToObject(pPrintHead,      "ID", id);

    //SN
    fnPackedBcdToAsciiBcd(SN, pHeadNvmData->bSerialNumber, sizeof( pHeadNvmData->bSerialNumber));
    cJSON_AddStringToObject(pPrintHead,      "SN", SN);

    //Mfg Date
    memset(dispDT, 0x0, sizeof(dispDT));
    sprintf( (char *)dispDT, "%02x-%02x-%02x",
            pHeadNvmData->bManufactureDate[0],
            pHeadNvmData->bManufactureDate[1],
            pHeadNvmData->bManufactureDate[2]);
    cJSON_AddStringToObject(pPrintHead,      "Mfg Date", dispDT);

    //Update
    memset(id, 0x0, sizeof(id));
    fnPackedBcdToAsciiBcd(id, pHeadNvmData->bUpdate, sizeof( pHeadNvmData->bUpdate));
    cJSON_AddStringToObject(pPrintHead,      "Update", id);

    //Printer SN
    char   bPrinterSerialNumber [10] = {0};
    memcpy(bPrinterSerialNumber, pHeadNvmData->bPrinterSerialNumber, sizeof(pHeadNvmData->bPrinterSerialNumber));
    cJSON_AddStringToObject(pPrintHead,      "Printer SN", bPrinterSerialNumber);

    //Install Date
    memset(dispDT, 0x0, sizeof(dispDT));
    sprintf( (char *)dispDT, "%02x-%02x-%02x",
            pHeadNvmData->bInstallDate[0],
            pHeadNvmData->bInstallDate[1],
            pHeadNvmData->bInstallDate[2]);
    cJSON_AddStringToObject(pPrintHead,      "Install Date", dispDT);

    //Dot Count
    cJSON_AddNumberToObject(pPrintHead,      "Dot Count", fnBinSix2Double(pHeadNvmData->bDotCount));

    //Install Count
    cJSON_AddNumberToObject(pPrintHead,      "Install Count", fnBytesToUnsignedLong(pHeadNvmData->bInstallCount, sizeof(pHeadNvmData->bInstallCount)));

    //Ink Color
    cJSON_AddNumberToObject(pPrintHead,      "Ink Color", fnBytesToUnsignedLong(pHeadNvmData->bTankColor, sizeof(pHeadNvmData->bTankColor)));

    //Tank Install Count
    cJSON_AddNumberToObject(pPrintHead,      "Tank Install Count", fnBytesToUnsignedLong(pHeadNvmData->bTankCount, sizeof(pHeadNvmData->bTankCount)));

    //Wipe Count
    cJSON_AddNumberToObject(pPrintHead,      "Wipe Count", fnBytesToUnsignedLong(pHeadNvmData->bWipeCount, sizeof(pHeadNvmData->bWipeCount)));

}

// ----------------------------------------------------------------------------
// FUNCTION: 							fnGetInkTankInfo
// DESCRIPTION:     
//  	This is called by a function AFTER that function has retrieved a ptr to 
//	a copy of the PM NVM data.  That calling function must make sure that the 
//	access to this data is protected until this function has returned.
//		This function converts and writes the PM NVM data into a json sub-object
//	that is part of a larger json object (message).   
//
//	INPUTS:   pInkTank 		- ptr to the json sub-object to populate
//			  pPmNvmData	- ptr to the copy of the PM NVM data. 
//  NOTES:  
//	1. PM NVM data shall NOT be accessed directly.
//  2. This routine only converts the data in the INK_TANK_DATA structure in
//		the copy of the PM NVM data.   
// HISTORY: 
// 2018.02.14 C.Bellamy - modified to use new method of accessing PM NVM data.  
//-------------------------------------------------------------------
void fnGetInkTankInfo( cJSON *pInkTank, NVRAM_DATA *pPmNvmData )
{
    INK_TANK_DATA    *pInkTankData = &(pPmNvmData->tank_eeprom);
    char id[5] = {0};
    char SN[9] ={0};
    char dispDT[9] = {0};
    char inkMfgType[10] = {0};

    //ID
    fnPackedBcdToAsciiBcd(id,pInkTankData->bId, sizeof(pInkTankData->bId));
    cJSON_AddStringToObject(pInkTank,      "ID", id);

    //SN
    fnPackedBcdToAsciiBcd(SN, pInkTankData->bSerialNumber, sizeof( pInkTankData->bSerialNumber));
    cJSON_AddStringToObject(pInkTank,      "SN", SN);

    //Mfg Date
    memset(dispDT, 0x0, sizeof(dispDT));
    sprintf( (char *)dispDT, "%02x-%02x-%02x",
            pInkTankData->bManufactureDate[0],
            pInkTankData->bManufactureDate[1],
            pInkTankData->bManufactureDate[2]);
    cJSON_AddStringToObject(pInkTank,      "Mfg Date", dispDT);

    //Update
    memset(id, 0x0, sizeof(id));
    fnPackedBcdToAsciiBcd(id, pInkTankData->bUpdate, sizeof( pInkTankData->bUpdate));
    cJSON_AddStringToObject(pInkTank,      "Update", id);

    //Gross Dot Count
    cJSON_AddNumberToObject(pInkTank,      "Gross Dot Count", fnBinSix2Double(pInkTankData->bGrossDotCount));

    //No Ink Dot Count
    cJSON_AddNumberToObject(pInkTank,      "No Ink Dot Count", fnBinSix2Double(pInkTankData->bNoInkDotCount));


    //Ink Low Dot Count
    cJSON_AddNumberToObject(pInkTank,      "Ink Low Dot Count", fnBinSix2Double(pInkTankData->bInkLowDotCount));

    //Printer SN
    char   bPrinterSerialNumber [10] = {0};
    memcpy(bPrinterSerialNumber, pInkTankData->bPrinterSerialNumber, sizeof(pInkTankData->bPrinterSerialNumber));
    cJSON_AddStringToObject(pInkTank,      "Printer SN", bPrinterSerialNumber);

    //Install Date
    memset(dispDT, 0x0, sizeof(dispDT));
    sprintf( (char *)dispDT, "%02x-%02x-%02x",
            pInkTankData->bInstallDate[0],
            pInkTankData->bInstallDate[1],
            pInkTankData->bInstallDate[2]);
    cJSON_AddStringToObject(pInkTank,      "Install Date", dispDT);

    //Dot Count
    cJSON_AddNumberToObject(pInkTank,      "Dot Count", fnBinSix2Double(pInkTankData->bDotCount));

    //Install Count
    cJSON_AddNumberToObject(pInkTank,      "Install Count", fnBytesToUnsignedLong(pInkTankData->bInstallCount, sizeof(pInkTankData->bInstallCount)));

    //Purge Count
    cJSON_AddNumberToObject(pInkTank,      "Purge Count", fnBytesToUnsignedLong(pInkTankData->bPurgeCount, sizeof(pInkTankData->bPurgeCount)));


    //Ink Mfg Type

    // indicate ink compatibility
    switch(fnCMGetInkTankMfgType())
    {
        case OUR_OEM_MFG:
            memcpy(inkMfgType, "OEM", 3);
            break;

        case SOME_OTHER_MFG:
            memcpy(inkMfgType, "nonOEM", 7);
            break;

        case UNKNOWN_MFG:
        default:
            memcpy(inkMfgType, "Unknown", 8);
            break;
    }

    cJSON_AddStringToObject(pInkTank,  "Ink Mfg Type", inkMfgType);

}


/* -----------------------------------------------------------------------[on_GetPrinterInfoReq]-- */
// DESCRIPTION:     
//		This function sends an ITmessage to the CM.  If the CM is in the proper 
//	 state, it will send an IT message to the PM to get a copy of the PM NVM 
//	 data.  The CM will set an event flag to indicate that the data was 
//	 successfully retrieved or it failed.  
//		If it succeeded, it will have placed a pointer to the copy of the data 
//	in the tPmNvmAccStatus structure, a pointer to which was passed by this 
//	function to the CM in the original ITmessage.  That copy of the PM NVM data
//  belongs solely to this thread, until the disabling condition is cleared.
//
//  NOTES:  
//	1. PM NVM data shall not be access directly.
//	2. The disabling condition, DCOND_ACCESSING_PM_NVM is being used to control
//		single-thread access to the retrieval function and data.  It must be
//		checked and set at the beginning of this function, and (if it was set)
//		cleared before this function exits.   
// HISTORY: 
// 2018.02.14 C.Bellamy - modified to use new method of accessing PM NVM data.  
//---------------------------------------------------------------------
void on_GetPrinterInfoReq(UINT32 handle, cJSON *root)
{
    tPmNvmAccStatus     sPmNvmAccStatus;
    cJSON 			*pPrinterData, *pPrintHead, *pInkTank;
    UINT32          lEvents;
    char            bStatus;

    cJSON *rspMsg = cJSON_CreateObject();

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetPrinterInfoRsp");

    // Prepare to ask CM to accesss PM NVM: Clear out return structure
    memset( &sPmNvmAccStatus, 0, sizeof( tPmNvmAccStatus ) );
    //This client doesn't want its own copy of the data.
    sPmNvmAccStatus.pDataCopy = NULL_PTR;
 
    // Wait for the functionality to be available  
    if( fnIsDisabledStatusSet(DCOND_ACCESSING_PM_NVM) == TRUE )  
    {
        // The CM's PM NVM Access is already in use. 
        cJSON_AddStringToObject(rspMsg, "Status", "Not Available");
        dbgTrace(DBG_LVL_ERROR, "on_GetPrinterInfoReq: CM Not Available."  );
    }
    else
    {
        // Make sure the result events are clear.  Send a message and await the result events.
        OSSetEvents( EVENT_GROUP_CMPM, ~(EVT_CM_PMNVM_BAD | EVT_CM_PMNVM_GOOD), OS_AND );        
        OSSendIntertask( CM, OIT, OC_GET_NVM_DATA_REQ, GLOBAL_PTR_DATA, &sPmNvmAccStatus, sizeof(sPmNvmAccStatus));
        bStatus = OSReceiveEvents( EVENT_GROUP_CMPM, EVT_CM_PMNVM_BAD | EVT_CM_PMNVM_GOOD, TIMEOUT_RESP, OS_OR, &lEvents );
        // Check status, then events.
        if( bStatus != SUCCESSFUL )
        {
            // Timed out: Has the CM or the PM gone off the rails?. 
            cJSON_AddStringToObject(rspMsg, "Status", "Not Available");
        	dbgTrace(DBG_LVL_ERROR, "on_GetPrinterInfoReq: Request to CM Timed out."  );
        }
        else
        {
            if( lEvents & EVT_CM_PMNVM_BAD ) 
            {
                // If event failed, give as much info as possible for debug. 
                cJSON_AddStringToObject(rspMsg, "Status", "Not Available");
        		dbgTrace(DBG_LVL_ERROR, "on_GetPrinterInfoReq: Request Failed. Error Type= %d, Status= %d, Code=%d.", 
        							sPmNvmAccStatus.bCMErrorType, sPmNvmAccStatus.lStatus, sPmNvmAccStatus.lErrCode );
            }
            else
            {
                cJSON_AddStringToObject(rspMsg, "Status", "Available");
			    //Printer NVRAM DATA
			    cJSON_AddItemToObject(rspMsg, "Printer", pPrinterData = cJSON_CreateObject());
			    fnGetPrinterNVRAMData(pPrinterData, sPmNvmAccStatus.pDataCopy);

			    //Print Head
			    cJSON_AddItemToObject(rspMsg, "Print Head", pPrintHead = cJSON_CreateObject());
			    fnGetPrintHeadInfo(pPrintHead, sPmNvmAccStatus.pDataCopy);

			    //Ink Tank
			    cJSON_AddItemToObject(rspMsg, "Ink Tank", pInkTank = cJSON_CreateObject());
			    fnGetInkTankInfo(pInkTank, sPmNvmAccStatus.pDataCopy);
			}
		}
        // We are done using this service, let someone else use it.
        OSSetEvents( EVENT_GROUP_CMPM, ~(EVT_CM_PMNVM_BAD | EVT_CM_PMNVM_GOOD), OS_AND );        
		fnClearDisabledStatus( DCOND_ACCESSING_PM_NVM ); 
    }

    addEntryToTxQueue(&entry, root, "  GetPrinterInfoReq: Added GetPrinterInfoRsp to tx queue. status=%d" CRLF);
}




// Getter function for static Mail Simulation Parameters
BOOL fnGetMailSimuPara(MAIL_SIMULATION* wMailSimuPara)
{
    if (wMailSimuPara != NULL)
    {
        (*wMailSimuPara).TimeInterval = wMailSimulation.TimeInterval;
        (*wMailSimuPara).NumTrips = wMailSimulation.NumTrips;
        (*wMailSimuPara).ReadyToSimu = wMailSimulation.ReadyToSimu;
        (*wMailSimuPara).blankImage = wMailSimulation.blankImage;
    }
    return( wMailSimulation.ReadyToSimu);
}

// Setter function for static Mail Simulation Parameters
void fnSetMailSimuPara(BOOL readyToSimu, UINT32 timeInterval, UINT32 numTrips, BOOL blankImage)
{
        wMailSimulation.TimeInterval = (unsigned long) timeInterval;
        wMailSimulation.NumTrips = (unsigned long) numTrips;
        wMailSimulation.ReadyToSimu = readyToSimu;
        wMailSimulation.blankImage = blankImage;
}


required_fields_tbl_t required_fields_tbl_MailSimulationReq =
    {
    "MailSimulationReq", {   "NumRuns"
                           , "Interval"
                           , NULL }
    };


/* ----------------------------------------------------------------------------------------[on_MailSimulationReq]-- */
/*
 * The parameter TransportSpeed is the same as that specified
 * in the Horizon Print Manager Interface Specification (Set Mode and Perform Print Manager Diagnostic commands).
 * The other inputs are the number of runs and the interval between runs.
 */
void on_MailSimulationReq(UINT32 handle, cJSON *root)
{
    UINT32     numRuns;
    UINT32     interval;
    BOOL       blankImage = TRUE;

    cJSON *rsp = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rsp}}};

    if (RequiredFieldsArePresent(root, rsp, &required_fields_tbl_MailSimulationReq))
    {
        numRuns = (UINT32) cJSON_GetObjectItem(root, "NumRuns")->valueint;
        interval = (UINT32) cJSON_GetObjectItem(root, "Interval")->valueint;
        if (cJSON_HasObjectItem(root, "BlankImage"))
        {// No validation of BlankImage - any value other than false will result in a blank image
            blankImage = (stricmp("false", cJSON_GetObjectItem(root, "BlankImage")->valuestring) == 0) ? FALSE : TRUE;
        }

        if ((interval > 0) && (interval <= MAX_SIMULATION_INTERVAL) && (numRuns > 0) && (numRuns <= MAX_NUM_SIMULATIONS))
        {
            fnSetMailSimuPara(TRUE, interval, numRuns, blankImage);
            generateResponse(&entry, rsp, "MailSimulation", api_status_to_string_tbl[API_STATUS_ACCEPTED], root);
        }
        else
        {
            generateResponse(&entry, rsp, "MailSimulation", api_status_to_string_tbl[API_STATUS_INVALID_PARAMETERS], root);
        }
    }//RequiredFieldsArePresent
    else
    {
        generateResponse(&entry, rsp, "MailSimulation", api_status_to_string_tbl[API_STATUS_INVALID_PARAMETERS], root);
    }
}

/* -------------------------------------------------------------------------------------------[on_DiagGetMemoryReq]-- */

required_fields_tbl_t required_fields_tbl_DiagGetMemoryReq=
    {
      "DiagGetMemoryReq", {  "Address"
                           , "Length"
                           , NULL }
    };

void on_DiagGetMemoryReq(UINT32 handle, cJSON *root)
{
    UINT32     Address;
    char       AddressStr[12];
    UINT32     Length;
    char       LengthStr[12];
    UINT8     *p_data;
    char      *p_hex_data;
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "DiagGetMemoryRsp");

    if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_DiagGetMemoryReq))
    {
        uint32_t i;
        Address = strtoul( cJSON_GetObjectItem(root, "Address")->valuestring, NU_NULL, 0);  /* base 0 --> allow 0x as hex prefix or 0 as octal prefix */
        Length = strtoul( cJSON_GetObjectItem(root, "Length")->valuestring, NU_NULL, 0);

        p_data = (UINT8 *)Address;

        p_hex_data = malloc(Length*2 + 1);

        if (p_hex_data)
        {
            for (i=0; i<Length; i++)
            {
                sprintf(&p_hex_data[i * 2], "%2.2X", p_data[i]);
            }
            p_hex_data[i*2] = '\0'; // null terminate it

            sprintf(AddressStr, "0x%8.8X", Address);
            cJSON_AddStringToObject(rspMsg, "Address", AddressStr);

            sprintf(LengthStr, "0x%X", Length);
            cJSON_AddStringToObject(rspMsg, "Length", LengthStr);

            cJSON_AddStringToObject(rspMsg, "Data", p_hex_data);

            free(p_hex_data);
        }
        else
        {
            cJSON_AddStringToObject(rspMsg, "Status", "malloc failure!");
        }

    }//RequiredFieldsArePresent
    else
    {
        cJSON_AddStringToObject(rspMsg, "Status", "Invalid Parameters");
    }

    addEntryToTxQueue(&entry, root, "  on_DiagGetMemoryReq: Added DiagGetMemoryRsp to tx queue. status=%d" CRLF);

}
 

/* -----------------------------------------------------------------------------------------------[hex_char_to_bin]-- */
uint8_t hex_char_to_bin(int c)
{
    uint8_t val = 0;

    if (isdigit(c))
        val = c - '0';
    else if (isxdigit(c))
    {
        if (islower(c))
            val = c - 'a' + 10;
        else
            val = c - 'A' + 10;
    }

    return val;
}

/* -------------------------------------------------------------------------------------------[on_DiagSetMemoryReq]-- */
required_fields_tbl_t required_fields_tbl_DiagSetMemoryReq=
    {
      "DiagSetMemoryReq", {  "Address"
                           , "Data"
                           , NULL }
    };

void on_DiagSetMemoryReq(UINT32 handle, cJSON *root)
{
    UINT32     Address;
    char       AddressStr[12];
    UINT32     Length;
    char       LengthStr[12];
    UINT8     *p_data;
    char      *p_hex_data;
    uint32_t   bytes_written = 0;
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "DiagSetMemoryRsp");

    if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_DiagSetMemoryReq))
    {
        uint32_t i;
        Address = strtoul( cJSON_GetObjectItem(root, "Address")->valuestring, NU_NULL, 0);  /* base 0 --> allow 0x as hex prefix or 0 as octal prefix */
        p_hex_data = cJSON_GetObjectItem(root, "Data")->valuestring;

        if (p_hex_data)
        {
            Length = strlen(p_hex_data);
            p_data = (UINT8 *)Address;

            for (i=0; i<Length; i+=2)
            {
                uint8_t val = hex_char_to_bin(p_hex_data[i]) << 4;
                val += hex_char_to_bin(p_hex_data[i+1]);

                *p_data++ = val;
                bytes_written++;
            }

            sprintf(AddressStr, "0x%8.8X", Address);
            cJSON_AddStringToObject(rspMsg, "Address", AddressStr);

            sprintf(LengthStr, "0x%X", bytes_written);
            cJSON_AddStringToObject(rspMsg, "NumBytesWritten", LengthStr);

        }
        else
        {
            cJSON_AddStringToObject(rspMsg, "Status", "Data field empty!");
        }

    }//RequiredFieldsArePresent
    else
    {
        cJSON_AddStringToObject(rspMsg, "Status", "Invalid Parameters");
    }

    addEntryToTxQueue(&entry, root, "  on_DiagSetMemoryReq: Added DiagSetMemoryRsp to tx queue. status=%d" CRLF);

}


/* --------------------------------------------------------------------------------------------[on_GetSensorInfoReq]-- */
void on_GetSensorInfoReq(UINT32 handle, cJSON *root)
{
    extern tCmStatus    cmStatus;
    cJSON *rspMsg = cJSON_CreateObject();

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetSensorInfoRsp");

    //Sensor1 status
    if(cmStatus.snsrState.wSensorData & S1_MASK)
        cJSON_AddStringToObject(rspMsg, "Sensor1_Status", "Blocked");
    else
        cJSON_AddStringToObject(rspMsg, "Sensor1_Status", "Not Blocked");

    //Sensor2 status
    if(cmStatus.snsrState.wSensorData & S2_MASK)
        cJSON_AddStringToObject(rspMsg, "Sensor2_Status", "Blocked");
    else
        cJSON_AddStringToObject(rspMsg, "Sensor2_Status", "Not Blocked");

    //Sensor3 status
    if(cmStatus.snsrState.wSensorData & S3_MASK)
        cJSON_AddStringToObject(rspMsg, "Sensor3_Status", "Blocked");
    else
        cJSON_AddStringToObject(rspMsg, "Sensor3_Status", "Not Blocked");

    //JAM_LEVER_SENSOR_MASK
    if(cmStatus.jamLvrState & JAM_LVR_OPEN)
        cJSON_AddStringToObject(rspMsg, "Jam Lever Sensor", "Open");
    else
        cJSON_AddStringToObject(rspMsg, "Jam Lever Sensor", "Closed");

    //Top Cover sensor
    if(cmStatus.cvrState & CVR_OPEN)
        cJSON_AddStringToObject(rspMsg, "Top Cover Sensor", "Open");
    else
        cJSON_AddStringToObject(rspMsg, "Top Cover Sensor", "Closed");


    addEntryToTxQueue(&entry, root, "  GetSensorInfoReq: Added GetSensorInfoRsp to tx queue. status=%d" CRLF);

}

typedef struct
{
    uint32_t    mode;
    const char *name;
} boot_mode_tbl_entry_t;

const boot_mode_tbl_entry_t boot_mode_lookup_tbl[] = 
    {
        { START_PB_APP,         "App"               },
        { START_CANON_TEST_APP, "Test App"          },
        { USB_DOWNLOAD_MODE,    "USB Bootloader"    },
        { SERIAL_DOWNLOAD_MODE, "Serial Bootloader" },
    };

/* -------------------------------------------------------------------------------------------------[set_boot_mode]-- */
bool set_boot_mode(char *new_boot_mode)
{
    int idx;

    for (idx=0; idx < NELEMENTS(boot_mode_lookup_tbl); idx++)
    {
        if (stricmp(boot_mode_lookup_tbl[idx].name, new_boot_mode) == 0)
        {
            // we have a match, update bootmode in FRAM and return success
            BoardInfo.BootMode = boot_mode_lookup_tbl[idx].mode;
            return true;
        }
    }

    return false;
}

/* ------------------------------------------------------------------------------------------[boot_mode_to_msg_str]-- */
const char *boot_mode_to_msg_str(uint32_t boot_mode)
{
    int idx;

    for (idx=0; idx < NELEMENTS(boot_mode_lookup_tbl); idx++)
    {
        if (boot_mode_lookup_tbl[idx].mode == boot_mode)
        {
            // we have a match, return the name of the current boot mode
            return boot_mode_lookup_tbl[idx].name;
        }
    }

    return "Unknown Boot Mode";
}

/* ---------------------------------------------------------------------------[get_valid_boot_modes_as_cJSON_Array]-- */
cJSON *get_valid_boot_modes_as_cJSON_Array()
{
    cJSON *valid_boot_modes = cJSON_CreateArray();

    if (valid_boot_modes != NULL)
    {
        int idx;

        for (idx=0; idx < NELEMENTS(boot_mode_lookup_tbl); idx++)
        {
            cJSON_AddItemToArray(valid_boot_modes, cJSON_CreateString(boot_mode_lookup_tbl[idx].name));
        }
    }
    return valid_boot_modes;
}

/* ---------------------------------------------------------------------------------------------[on_SetBootModeReq]-- */

required_fields_tbl_t required_fields_tbl_SetBootModeReq=
    {
      "SetBootModeReq", {  "BootMode"
                           , NULL }
    };

void on_SetBootModeReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "SetBootModeRsp");

    if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_SetBootModeReq))
    {
        char *new_boot_mode = cJSON_GetObjectItem(root, "BootMode")->valuestring;

        if (set_boot_mode(new_boot_mode) == true)
        {
            cJSON_AddStringToObject(rspMsg, "Status", "Boot Mode updated");
            cJSON_AddStringToObject(rspMsg, "NewBootMode", new_boot_mode);
        }
        else
        {
            
            cJSON_AddStringToObject(rspMsg, "Status", "Invalid Boot Mode!");
            cJSON_AddStringToObject(rspMsg, "RejectedBootMode", new_boot_mode);
            cJSON_AddItemToObject(rspMsg, "ValidBootModes", get_valid_boot_modes_as_cJSON_Array());
        }

    }//RequiredFieldsArePresent
    else
    {
        cJSON_AddStringToObject(rspMsg, "Status", "Invalid Parameters");
    }

    addEntryToTxQueue(&entry, root, "  on_SetBootModeReq: Added SetBootModeRsp to tx queue. status=%d" CRLF);
}

/* ---------------------------------------------------------------------------------------------[on_GetBootModeReq]-- */
void on_GetBootModeReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetBootModeRsp");

    cJSON_AddStringToObject(rspMsg, "BootMode", boot_mode_to_msg_str(BoardInfo.BootMode));

    addEntryToTxQueue(&entry, root, "  on_GetBootModeReq: Added GetBootModeRsp to tx queue. status=%d" CRLF);
}

/* --------------------------------------------------------------------------------------------------[on_RebootReq]-- */
void on_RebootReq(UINT32 handle, cJSON *root)
{
	storeSyslogToFile(POWERDOWN_SYSLOG_DIR, log_names[LOG_SYSTEM_RESET]);
	
    // lets just do a cold reboot
    //
    uint32 *PRM_DEVICE_REGS = (uint32 *)0x44E00F00;

    *PRM_DEVICE_REGS = 0x02;    /* do the cold reboot */
}

/* -------------------------------------------------------------------------------------------[get_storage_devices]-- */
static cJSON *get_storage_devices()
{
    DEV_LIST_S *dev_list, *dev_idx;
    cJSON *storage_devices = cJSON_CreateArray();

    NU_List_Device(&dev_list);

    dev_idx = dev_list;
    while(dev_idx)
    {
        cJSON_AddItemToArray(storage_devices, cJSON_CreateString(dev_idx->dev_name));

        dev_idx = dev_idx->next;
    }
    NU_Free_List((VOID **)&dev_list);
    return storage_devices;
}


/* ----------------------------------------------------------------------------------------------[get_file_systems]-- */
static cJSON *get_file_systems()
{
    FS_LIST_S *fs_list, *fs_idx;
    cJSON *file_systems = cJSON_CreateArray();

    NU_List_File_System(&fs_list);

    fs_idx = fs_list;
    while(fs_idx)
    {
        cJSON_AddItemToArray(file_systems, cJSON_CreateString(fs_idx->fs_name));

        fs_idx = fs_idx->next;
    }
    NU_Free_List((VOID **)&fs_list);
    return file_systems;
}

/* ----------------------------------------------------------------------------------------------[get_mount_points]-- */
static cJSON *get_mount_points()
{
    MNT_LIST_S *mnt_list, *mnt_idx;
    cJSON *mount_points = cJSON_CreateArray();
    char mnt[100];

    NU_List_Mount(&mnt_list);

    mnt_idx = mnt_list;
    while(mnt_idx)
    {
        sprintf(mnt, "mnt:%s dev:%s fs:%s", mnt_idx->mnt_name, mnt_idx->dev_name, mnt_idx->fs_name);
        cJSON_AddItemToArray(mount_points, cJSON_CreateString(mnt));

        mnt_idx = mnt_idx->next;
    }
    NU_Free_List((VOID **)&mnt_list);
    return mount_points;
}

/* ------------------------------------------------------------------------------------------[on_GetStorageInfoReq]-- */
void on_GetStorageInfoReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};


    cJSON_AddStringToObject(rspMsg, "MsgID", "GetStorageInfoRsp");

    cJSON_AddItemToObject(rspMsg, "StorageDevices", get_storage_devices());
    cJSON_AddItemToObject(rspMsg, "FileSystems",    get_file_systems());
    cJSON_AddItemToObject(rspMsg, "MountPoints",    get_mount_points());

    addEntryToTxQueue(&entry, root, "  on_GetStorageInfoReq: Added GetStorageInfoReq to tx queue. status=%d" CRLF);
}

/* ------------------------------------------------------------------------------------------[on_RespawnDebugTerminalReq]-- */
void on_RespawnDebugTerminalReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};


    cJSON_AddStringToObject(rspMsg, "MsgID", "RespawnDebugTerminalRsp");

    STATUS status = spawnDebugTerminalAcceptor();
    char strTmp[50];
    snprintf(strTmp, 50, "Respawn Debug Terminal returned %d", status);
    cJSON_AddStringToObject(rspMsg, "Status", strTmp);

    addEntryToTxQueue(&entry, root, "  on_RespawnDebugTerminalReq: Added RespawnDebugTerminalReq to tx queue. status=%d" CRLF);
}

/* ------------------------------------------------------------------------------------------[on_GetDHCPActivityReq]-- */
void on_GetDHCPActivityReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};


    cJSON_AddStringToObject(rspMsg, "MsgID", "on_GetDHCPActivityRsp");

    STATUS status = spawnDebugTerminalAcceptor();
    char strTmp[50];
    snprintf(strTmp, 50, "%s", (globalDHCPProcessActive == 0) ? "Inactive" : "Active");
    cJSON_AddStringToObject(rspMsg, "Status", strTmp);

    addEntryToTxQueue(&entry, root, "  on_GetDHCPActivityReq: Added GetDHCPActivityRsp to tx queue. status=%d" CRLF);
}


 
/* ---------------------------------------------------------------------------------------------[on_StorageDiagReq]-- */


int fm_format ( int drivenum, long fattype );
int fm_checkvolume(int drvnumber);
#define f_format( drivenum, fattype )  fm_format( drivenum, fattype )
#define f_checkvolume(drvnumber)       fm_checkvolume(drvnumber) 

required_fields_tbl_t required_fields_tbl_StorageDiagReq=
    {
      "StorageDiagReq", {  "DiagCmd"
                           , NULL }
    };

required_fields_tbl_t required_fields_tbl_StorageDiagReq_erase_flash_sector=
    {
      "StorageDiagReq", {  "DiagCmd"
                          ,"starting-sector"
                          ,"num-of-sectors"
                           , NULL }
    };

required_fields_tbl_t required_fields_tbl_StorageDiagReq_write_to_file=
    {
      "StorageDiagReq", {  "DiagCmd"
                          ,"filename"
                          ,"filemode"
                          ,"data-type"  /* "string" or "hexstring" */
                          ,"file-data"
                           , NULL }
    };

void on_StorageDiagReq(UINT32 handle, cJSON *root)
{
    char   sz_status[128];
    cJSON *rspMsg = cJSON_CreateObject();

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "StorageDiagRsp");

    if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_StorageDiagReq))
    {
        char *diag_cmd = cJSON_GetObjectItem(root, "DiagCmd")->valuestring;

        if (stricmp(diag_cmd, "erase-flash-sector") == 0)
        {
            if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_StorageDiagReq_erase_flash_sector))
            {
                uint32_t sector_num    = cJSON_GetObjectItem(root, "starting-sector" )->valueint;
                uint32_t num_sectors   = cJSON_GetObjectItem(root, "num-of-sectors"  )->valueint;
                STATUS   erase_status  = NU_SUCCESS;

                
                if (sector_num < 8)
                {
                    sprintf(sz_status, "Error! starting-sector must be between 8 and 511. %ld is not allowed!", sector_num);
                }
                else if ((sector_num + num_sectors) > 512)
                {
                    sprintf(sz_status, "Error! starting-sector + num_sectors must be <= 512.");
                }
                else
                {
                    while(num_sectors > 0 && erase_status == NU_SUCCESS)
                    {
                        if (CFI_RES_SUCCESS == (erase_status = cfi_erase_sector(&cfi, sector_num)))
                        {
                            printf("Erased sector %d" CRLF, sector_num);
                            sector_num++;
                            num_sectors--;
                        }
                        else
                        {
                            printf("Failed to erased sector %d. Status=%d (%s)" CRLF, sector_num, erase_status,
                                    cfi_result_to_str(erase_status));
                        }
                    }

                    if (erase_status == NU_SUCCESS)
                    {
                        sprintf(sz_status, "Erased!");
                    }
                    else
                    {
                        sprintf(sz_status, "Error erasing sector %ld. error code=%d!", sector_num, erase_status);
                    }
                }
                cJSON_AddStringToObject(rspMsg, "Status", sz_status);
            }
        }
        else if (stricmp(diag_cmd, "checkvolume") == 0)
        {
            int result = f_checkvolume(0);
            
            sprintf(sz_status, "f_checkvolume result = %d", result);
            cJSON_AddStringToObject(rspMsg, "Status", sz_status);
        }
        else if (stricmp(diag_cmd, "format") == 0)
        {
            int result = f_format( 0 /* HCC_DRIVE_NUM */, 3 /* F_FAT32_MEDIA */ );
            
            sprintf(sz_status, "f_format result = %d", result);
            cJSON_AddStringToObject(rspMsg, "Status", sz_status);
        }
        else if (stricmp(diag_cmd, "write-to-file") == 0)
        {
            if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_StorageDiagReq_write_to_file))
            {
                char *filename    = cJSON_GetObjectItem(root, "filename"  )->valuestring;
                char *filemode    = cJSON_GetObjectItem(root, "filemode"  )->valuestring;
                char *data_type   = cJSON_GetObjectItem(root, "data-type" )->valuestring;
                char *file_data   = cJSON_GetObjectItem(root, "file-data" )->valuestring;
                FILE *stream;
                int result;

                stream = fopen(filename, filemode);

                if (stream == NULL)
                    sprintf(sz_status, "Error fopen(\"%s\", \"%s\"). errno-%d", filename, filemode, errno);
                else
                {
                    result = fputs(file_data, stream);
                    fclose(stream);
                    sprintf(sz_status, "Result of fputs() = %d", result);
                }
                
                cJSON_AddStringToObject(rspMsg, "Status", sz_status);
            }

        }
        else
        {
            sprintf(sz_status, "Unknown diag_cmd [%s]", diag_cmd);
            cJSON_AddStringToObject(rspMsg, "Status", sz_status);
        }


    }//RequiredFieldsArePresent

    addEntryToTxQueue(&entry, root, "  on_StorageDiagReq: Added StorageDiagRsp to tx queue. status=%d" CRLF);
}

/* --------------------------------------------------------------------------------[int_pbp_acct_num_as_packed_bcd]-- */
unsigned long int_pbp_acct_num_as_packed_bcd(int pbp_acct_num)
{
    // we need to convert something like 17907023(10) into 0x23709017
    // like so: 17907023   (int)
    //      --> "17907023" (use sprintf to get string)
    //      --> 0x17907023 (use strtoul(base16))
    //      --> 0x23709017 (swap32)
    char acct_num_ascii[11];

    sprintf(acct_num_ascii, "%08.8d", pbp_acct_num);

    uint32  acct_num_uint32 = strtoul(acct_num_ascii, NULL, 16);

    return EndianSwap32(acct_num_uint32);
}

/* --------------------------------------------------------------------------------[packed_bcd_pbp_acct_num_as_int]-- */
int packed_bcd_pbp_acct_num_as_int(unsigned long pbp_acct_num)
{
    // we need to convert something like 0x23709017 into 17907023(10)
    // like so: 0x23709017 (packed bcd)
    //      --> 0x17907023 (swap32)
    //      --> "17907023" (use sprintf to get string)
    //      -->  17907023 (use strtoul(base10))
    char acct_num_ascii[11];

    sprintf(acct_num_ascii, "%08.8x", EndianSwap32(pbp_acct_num));

    return strtol(acct_num_ascii, NULL, 10);
}

/* -------------------------------------------------------------------------------------------[on_DiagSetTestDataReq]-- */
static const char *valid_fieldnames_DiagSetTestDataReq [] =

                    {"BPN",                 //0
                    "PBPAccount",           //1
                    "DistURL",              //2
                    "EnableSSLCertVerify",  //3
                    NULL
                    };

void on_DiagSetTestDataReq(UINT32 handle, cJSON *root)
{
    char * string;
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "DiagSetTestDataRsp");

    // PBPAccount,
   if (cJSON_HasObjectItem(root, "lwReserved"))
   {
       CMOSSetupParams.lwReserved = cJSON_GetObjectItem(root, "lwReserved")->valueint;
       memset(CMOSSetupParams.BPN, 0, sizeof(CMOSSetupParams.BPN));
   }

   // PBPAccount,
   if (cJSON_HasObjectItem(root, valid_fieldnames_DiagSetTestDataReq[1]))
   {
       CMOSSetupParams.lwPBPAcctNumber = int_pbp_acct_num_as_packed_bcd(cJSON_GetObjectItem(root, valid_fieldnames_DiagSetTestDataReq[1])->valueint);
   }

      // EnableSSLCertVerify,
    if (cJSON_HasObjectItem(root, valid_fieldnames_DiagSetTestDataReq[3]))
    {
        CMOSNetworkConfig.bEnableSSLCertVerify = cJSON_GetObjectItem(root, valid_fieldnames_DiagSetTestDataReq[3])->valueint;
    }

    // BPN,
    if (cJSON_HasObjectItem(root, valid_fieldnames_DiagSetTestDataReq[0]))
    {
        string = cJSON_GetObjectItem(root, valid_fieldnames_DiagSetTestDataReq[0])->valuestring;
        if (string)
        {
            memset(CMOSSetupParams.BPN, 0, sizeof(CMOSSetupParams.BPN));
            memset(CMOSSetupParams.BPN, '0', BPN_SIZE);
            strncpy((char *) CMOSSetupParams.BPN, string, strlen(string)+1);
        }
        else
        {
            return generateResponse(&entry, rspMsg, "DiagSetTestData", api_status_to_string_tbl[API_STATUS_INVALID_PARAMETERS], root);
        }
    }

     // DistURL,
    if (cJSON_HasObjectItem(root, valid_fieldnames_DiagSetTestDataReq[2]))
    {
        string = cJSON_GetObjectItem(root, valid_fieldnames_DiagSetTestDataReq[2])->valuestring;
        if (string)
        {
            strncpy(CMOSNetworkConfig.distributorUrl, string, sizeof(CMOSNetworkConfig.distributorUrl));
        }
        else
        {
            return generateResponse(&entry, rspMsg, "DiagSetTestData", api_status_to_string_tbl[API_STATUS_INVALID_PARAMETERS], root);
        }
    }

    addEntryToTxQueue(&entry, root, "  on_DiagSetTestDataReq: Added DiagSetTestDataRsp to tx queue. status=%d" CRLF);

}

void on_DiagGetTestDataReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "DiagGetTestDataRsp");

    // BPN,
    char temp[BPN_SIZE + 1] = {0};
    memset(temp, '0', sizeof(temp) - 1);
    if ((strlen(CMOSSetupParams.BPN) == 0) || (strcmp(CMOSSetupParams.BPN, temp) == 0))
    {
        memset(CMOSSetupParams.BPN, 0, sizeof(CMOSSetupParams.BPN));
        memset(CMOSSetupParams.BPN, '0', BPN_SIZE);
        if (CMOSSetupParams.lwReserved != 0)
        {
            memset(temp, 0, sizeof(temp));
            sprintf(temp, "%ld", CMOSSetupParams.lwReserved);
            strcpy(CMOSSetupParams.BPN + (BPN_SIZE - strlen(temp)), temp);
        }
    }
    cJSON_AddStringToObject(rspMsg, "BPN", CMOSSetupParams.BPN);

    //PBP Account number
    cJSON_AddNumberToObject(rspMsg, "PBPAccount", packed_bcd_pbp_acct_num_as_int(CMOSSetupParams.lwPBPAcctNumber));

    //Distributor URL
    cJSON_AddStringToObject(rspMsg, "DistURL", CMOSNetworkConfig.distributorUrl);

     // EnableSSLCertVerify,
    cJSON_AddNumberToObject(rspMsg, "EnableSSLCertVerify", CMOSNetworkConfig.bEnableSSLCertVerify);


    cJSON_AddNumberToObject(rspMsg, "lwReserved", CMOSSetupParams.lwReserved);

    addEntryToTxQueue(&entry, root, "  on_DiagGetTestDataReq: Added DiagGetTestDataRsp to tx queue. status=%d" CRLF);

}


/* -------------------------------------------------------------------------------------------[on_DiagWipeCMOSReq]-- */
void on_DiagWipeCMOSReq(UINT32 handle, cJSON *root)
{

    cJSON *rspMsg = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "DiagWipeCMOSRsp");

    // Wipe CMOS
    CMOSSignature.pInitSignature[0] = 0;
    CMOSSignature.pInitSignature[1] = 0;
    CMOSSignature.pInitSignature[2] = 0;
    CMOSSignature.pInitSignature[3] = 0;
    CMOSSignature.ulExtVersion = 0;

    cJSON_AddStringToObject(rspMsg, "Status", "Accepted");

    addEntryToTxQueue(&entry, root, "  on_DiagWipeCMOSReq: Added DiagWipeCMOSRsp to tx queue. status=%d" CRLF);

}



/* -------------------------------------------------------------------------------------------[on_DiagGetShadowDebitLogReq]-- */
cJSON *CreateDebitLogItem(ShadowDebitLogEntry *sDebitLogEntry)
{

    double dDR = 0;
    double dAR = 0;
    char sMailDateTime[50]= {0};

    cJSON *item = cJSON_CreateObject();


    //Debit Data size
    cJSON_AddNumberToObject(item, "DebitDataSize", sDebitLogEntry->wDebitDataSize);

    //Record status
    cJSON_AddNumberToObject(item, "RecordStatus", sDebitLogEntry->bRecordStatus);

    //Data Layout Version
    cJSON_AddNumberToObject(item, "Data Layout Version", sDebitLogEntry->bDataLayoutVersion);

    //Debit Date time
    (void)sprintf(sMailDateTime, "%02d%02d-%02d-%02dT%02d:%02d:%02d",
                                    sDebitLogEntry->sDebitDateTime.bCentury,
                                    sDebitLogEntry->sDebitDateTime.bYear,
                                    sDebitLogEntry->sDebitDateTime.bMonth,
                                    sDebitLogEntry->sDebitDateTime.bDay,
                                    sDebitLogEntry->sDebitDateTime.bHour,
                                    sDebitLogEntry->sDebitDateTime.bMinutes,
                                    sDebitLogEntry->sDebitDateTime.bSeconds);
    cJSON_AddStringToObject(item, "DebitDate", sMailDateTime);

    //Debit piece count
    cJSON_AddNumberToObject(item, "PieceCnt", sDebitLogEntry->lDebitPieceCount);

    //Debit zero piece count
    cJSON_AddNumberToObject(item, "ZeroPieceCnt", sDebitLogEntry->lZeroPieceCount);

    //Get Ascending Registers
    // Convert Ascending Register to a 5-byte value, and store.
    dAR = fnBinFive2Double(sDebitLogEntry->bAR);
    cJSON_AddNumberToObject(item, "AscReg", dAR);

    //Get Descending registers
    dDR = fnBinFive2Double(sDebitLogEntry->bDR);
    cJSON_AddNumberToObject(item, "DescReg", dDR);

    //Transaction Type
    cJSON_AddNumberToObject(item, "Transaction Type", sDebitLogEntry->bTransType);

    //Record Type
    cJSON_AddNumberToObject(item, "Record Type", sDebitLogEntry->bRecordType);

    //Debit Log Data
    unsigned char   bDebitLogData[ MAX_SHADOW_DEBIT_ENTRY_SZ * 2];
    int iB64Size = 0;
    memset(bDebitLogData, 0x0, sizeof(bDebitLogData));
    if(binaryToASCII(sDebitLogEntry->bDebitLogData, sizeof(sDebitLogEntry->bDebitLogData), bDebitLogData, MAX_SHADOW_DEBIT_ENTRY_SZ * 2, &iB64Size) == NU_SUCCESS)
    {
        bDebitLogData[iB64Size] = 0;
        cJSON_AddStringToObject((cJSON *)item, "DebitData", (char *)bDebitLogData);
    }

//  cJSON_AddStringToObject(item, "DebitData", sDebitLogEntry->bDebitLogData);


    return item;
}

extern ShadowDebitLog CMOSShadowDebitLog;
void on_DiagGetShadowDebitLogReq(UINT32 handle, cJSON *root)
{

    int       bRecordIndex = 0;


    cJSON *rspMsg = cJSON_CreateObject();
    cJSON *shadowDebitLog;

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "DiagGetShadowDebitLogRsp");


    //Indicia Record Type
    cJSON_AddNumberToObject(rspMsg, "Indicia Record Type", CMOSShadowDebitLog.bIndicumRecordType);

    //Record version
    cJSON_AddNumberToObject(rspMsg, "Record Version", CMOSShadowDebitLog.bRecordVersion);


    //Layout Version Updated
    cJSON_AddNumberToObject(rspMsg, "Layout Version Updated", CMOSShadowDebitLog.bLayoutVersionUpdated);

    cJSON_AddItemToObject(rspMsg, "ShadowDebitLog", shadowDebitLog = cJSON_CreateArray());

    for(bRecordIndex=0; bRecordIndex < (int)NUMBER_OF_DEBIT_RECORDS; bRecordIndex++ )
    {
            cJSON_AddItemToArray(shadowDebitLog, CreateDebitLogItem(&CMOSShadowDebitLog.ShadowDebitLogTable[bRecordIndex]));
    }

    addEntryToTxQueue(&entry, root, "  on_DiagGetShadowDebitLogReq: Added DiagGetShadowDebitLogRsp to tx queue. status=%d" CRLF);

}


/* -------------------------------------------------------------------------------------------[on_DiagGetShadowRefillLogReq]-- */
cJSON *CreateShadowRefillLogItem(ShadowRefillLogEntry *sRefillLogEntry)
{

    double dDR = 0;
    double dAR = 0;
    char sRefillDateTime[50]= {0};

    cJSON *item = cJSON_CreateObject();


    //Refill Data size
    cJSON_AddNumberToObject(item, "RefillDataSize", sRefillLogEntry->wRefillDataSize);

    //Record status
    cJSON_AddNumberToObject(item, "RecordStatus", sRefillLogEntry->bRecordStatus);

    //Data Layout Version
    cJSON_AddNumberToObject(item, "Data Layout Version", sRefillLogEntry->bDataLayoutVersion);

    //Refill Date time
    (void)sprintf(sRefillDateTime, "%02d%02d-%02d-%02dT%02d:%02d:%02d",
            sRefillLogEntry->sRefillDateTime.bCentury,
            sRefillLogEntry->sRefillDateTime.bYear,
            sRefillLogEntry->sRefillDateTime.bMonth,
            sRefillLogEntry->sRefillDateTime.bDay,
            sRefillLogEntry->sRefillDateTime.bHour,
            sRefillLogEntry->sRefillDateTime.bMinutes,
            sRefillLogEntry->sRefillDateTime.bSeconds);
    cJSON_AddStringToObject(item, "RefillDateTime", sRefillDateTime);

    //Refill piece count
    cJSON_AddNumberToObject(item, "PieceCnt", sRefillLogEntry->lPieceCount);

    //Refill zero piece count
    cJSON_AddNumberToObject(item, "ZeroPieceCnt", sRefillLogEntry->lZeroPieceCount);

    //Get Ascending Registers
    // Convert Ascending Register to a 5-byte value, and store.
    dAR = fnBinFive2Double(sRefillLogEntry->bAR);
    cJSON_AddNumberToObject(item, "AscReg", dAR);

    //Get Descending registers
    dDR = fnBinFive2Double(sRefillLogEntry->bDR);
    cJSON_AddNumberToObject(item, "DescReg", dDR);

    //Transaction Type
    cJSON_AddNumberToObject(item, "Transaction Type", sRefillLogEntry->bTransType);

    //Record Type
    cJSON_AddNumberToObject(item, "Record Type", sRefillLogEntry->bRecordType);

    //Refill Log Data
    unsigned char   bRefillLogData[ MAX_SHADOW_DEBIT_ENTRY_SZ * 2];
    int iB64Size = 0;
    memset(bRefillLogData, 0x0, sizeof(bRefillLogData));
    if(binaryToASCII(sRefillLogEntry->bRefillLogData, sizeof(sRefillLogEntry->bRefillLogData), bRefillLogData, MAX_SHADOW_DEBIT_ENTRY_SZ * 2, &iB64Size) == NU_SUCCESS)
    {
        bRefillLogData[iB64Size] = 0;
        cJSON_AddStringToObject((cJSON *)item, "RefillData", (char *)bRefillLogData);
    }

    return item;
}

extern ShadowRefillLog CMOSShadowRefillLog;
void on_DiagGetShadowRefillLogReq(UINT32 handle, cJSON *root)
{

    int       bRecordIndex = 0;


    cJSON *rspMsg = cJSON_CreateObject();
    cJSON *shadowRefillLog;

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "DiagGetShadowRefillLogRsp");


    //Refill Record Type
    cJSON_AddNumberToObject(rspMsg, "Refill Record Type", CMOSShadowRefillLog.bRefillRecordType);

    //Refund Record Type
    cJSON_AddNumberToObject(rspMsg, "Refund Record Type", CMOSShadowRefillLog.bRefundRecordType);

    //Record version
    cJSON_AddNumberToObject(rspMsg, "Record Version", CMOSShadowRefillLog.bRecordVersion);


    //Layout Version Updated
    cJSON_AddNumberToObject(rspMsg, "Layout Version Updated", CMOSShadowRefillLog.bLayoutVersionUpdated);

    cJSON_AddItemToObject(rspMsg, "ShadowRefillLog", shadowRefillLog = cJSON_CreateArray());

    for(bRecordIndex=0; bRecordIndex < (int)NUMBER_OF_REFILL_RECORDS; bRecordIndex++ )
    {
            cJSON_AddItemToArray(shadowRefillLog, CreateShadowRefillLogItem(&CMOSShadowRefillLog.ShadowRefillLogTable[bRecordIndex]));
    }

    addEntryToTxQueue(&entry, root, "  on_DiagGetShadowRefillLogReq: Added DiagGetShadowRefillLogRsp to tx queue. status=%d" CRLF);

}

/* ----------------------------------------------[on_DiagGetWasteTankReq]-- */
void on_DiagGetWasteTankReq(UINT32 handle, cJSON *root)
{
    tPmNvmAccStatus     sPmNvmAccStatus;
    ulong       lEvents;
    char        status;

    cJSON *rspMsg = cJSON_CreateObject();

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "DiagGetWasteTankRsp");


    // Prepare to ask CM to accesss PM NVM: Clear out return structure
    memset( &sPmNvmAccStatus, 0, sizeof( tPmNvmAccStatus ) );
    //This client doesn't want its own copy of the data.
    sPmNvmAccStatus.pDataCopy = NULL_PTR;
 
    // Wait for the functionality to be available  
    if( fnIsDisabledStatusSet(DCOND_ACCESSING_PM_NVM) == TRUE )  
    {
        // The CM's PM NVM Access is already in use. 
        cJSON_AddStringToObject(rspMsg, "Status", "Not Performed");
        dbgTrace(DBG_LVL_ERROR, "on_DiagGetWasteTankReq: Access Not Available."  );
    }
    else
    {
		// Post a disabling condition until the access is complete. Make sure this function 
		//  clears it further down, but only if this instance set it.   
		fnPostDisabledStatus( DCOND_ACCESSING_PM_NVM );
        // Make sure the result events are clear.  Send a message and await the result events.
        OSSetEvents( EVENT_GROUP_CMPM, ~(EVT_CM_PMNVM_BAD | EVT_CM_PMNVM_GOOD), OS_AND );        
        OSSendIntertask( CM, OIT, OC_GET_NVM_DATA_REQ, GLOBAL_PTR_DATA, &sPmNvmAccStatus, sizeof(sPmNvmAccStatus));
        status = OSReceiveEvents( EVENT_GROUP_CMPM, EVT_CM_PMNVM_BAD | EVT_CM_PMNVM_GOOD, TIMEOUT_RESP, OS_OR, &lEvents );
        // Check status, then events.
        if( status != SUCCESSFUL )
        {
            // Timed out: Has the CM or the PM gone off the rails?. 
            cJSON_AddStringToObject(rspMsg, "Status", "Not Performed");
        	dbgTrace(DBG_LVL_ERROR, "on_DiagGetWasteTankReq: Request timed out."  );
        }
        else
        {
            if( lEvents & EVT_CM_PMNVM_BAD ) 
            {
                // If event failed, give as much info as possible for debug. 
                cJSON_AddStringToObject(rspMsg, "Status", "Not Performed");
        		dbgTrace(DBG_LVL_ERROR, "on_DiagGetWasteTankReq: Request Failed. Error Type= %d, Status= %d, Code=%d.", 
        							sPmNvmAccStatus.bCMErrorType, sPmNvmAccStatus.lStatus, sPmNvmAccStatus.lErrCode );
            }
            else
            {
                // This data should be good until we make teh PMNVM access available again.
                cJSON_AddNumberToObject(rspMsg, "WasteTankCount_g", sPmNvmAccStatus.pDataCopy->wast_ink_count_g);
                cJSON_AddNumberToObject(rspMsg, "WasteTankCount_mg", sPmNvmAccStatus.pDataCopy->wast_ink_count_mg);
                cJSON_AddNumberToObject(rspMsg, "WasteTankCount_ug", sPmNvmAccStatus.pDataCopy->wast_ink_count_ug);
                cJSON_AddNumberToObject(rspMsg, "WasteTankCount_ng", sPmNvmAccStatus.pDataCopy->wast_ink_count_ng);
                cJSON_AddStringToObject(rspMsg, "Status", "Accepted");
            }
        }
        // We are done using this service:  let someone else use it.
        OSSetEvents( EVENT_GROUP_CMPM, ~(EVT_CM_PMNVM_BAD | EVT_CM_PMNVM_GOOD), OS_AND );        
		fnClearDisabledStatus( DCOND_ACCESSING_PM_NVM ); 
    }

    addEntryToTxQueue(&entry, root, "  on_DiagGetWasteTankReq: Added DiagGetWasteTankRsp to tx queue. status=%d" CRLF);

}

/* ----------------------------------------------[on_DiagClearWasteTankReq]-- */
void on_DiagClearWasteTankReq(UINT32 handle, cJSON *root)
{
    tPmNvmAccStatus     sPmNvmAccStatus;
    UINT32          pMsgData[2];
    ulong           lEvents;
    char            status;
    BOOL            bForce;  

    cJSON *rspMsg = cJSON_CreateObject();

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "DiagClearWasteTankRsp");


    // error type, not equal to the last status
    pMsgData[0] = (UINT32) ERTYPE_WARNING;
    if (fnIsCmWasteTankFull() == TRUE)
    {
        pMsgData[1] = (UINT32) WRN_WTANK_FULL;
    }
    else if (fnIsCmWasteTankNearFull() == TRUE)
    {
        pMsgData[1] = (UINT32) WRN_WTANK_NEAR_FULL;
    }

    if( (fnIsCmWasteTankFull() == TRUE) || (fnIsCmWasteTankNearFull() == TRUE) )
    {
        OSSendIntertask(CM, OIT, OC_CLR_ERR_REQ, LONG_DATA, pMsgData, sizeof(pMsgData));

        cJSON_AddStringToObject(rspMsg, "Status", "Accepted");
    }
    else
    {                 
    	if( cJSON_HasObjectItem(root, "Force") )
    	{
        	bForce = (stricmp( "true", cJSON_GetObjectItem(root,"Force")->valuestring ) == 0) ? TRUE : FALSE ;
    	}
		else
		{
			bForce = FALSE;
		}

        if(bForce == FALSE)
        {
            // Unless "Force" is true, we only clear the counts if there is a 
            //  waste tank warning. 
            cJSON_AddStringToObject(rspMsg, "Status", "Accepted");
        	dbgTrace(DBG_LVL_ERROR, "DiagClearWasteTankReq: No waste tank warnings and no FORCE.\n"  );
        }
        else
        {
            // Prepare to ask CM to accesss PM NVM: Clear out return structure
            memset( &sPmNvmAccStatus, 0, sizeof( tPmNvmAccStatus ) );
            //This client doesn't want its own copy of the data.
            sPmNvmAccStatus.pDataCopy = NULL_PTR;
 
            // Wait for the functionality to be available 
    		if( fnIsDisabledStatusSet(DCOND_ACCESSING_PM_NVM) == TRUE )  
            {
                // Timed out: Someone else is using the CM's PM NVM Access right now. 
                cJSON_AddStringToObject(rspMsg, "Status", "Not Performed");
        		dbgTrace(DBG_LVL_ERROR, "DiagClearWasteTankReq: Action Not Available"  );
            }
            else
            {
				// Post a disabling condition until this instance is done with the data. 
				fnPostDisabledStatus( DCOND_ACCESSING_PM_NVM );
                // Make sure the result events are clear.  Send a message and await the result events.
                OSSetEvents( EVENT_GROUP_CMPM, ~(EVT_CM_PMNVM_BAD | EVT_CM_PMNVM_GOOD), OS_AND );        
                OSSendIntertask( CM, OIT, OC_CLR_WASTE_CNTS_REQ, GLOBAL_PTR_DATA, &sPmNvmAccStatus, sizeof(sPmNvmAccStatus));
                status = OSReceiveEvents( EVENT_GROUP_CMPM, EVT_CM_PMNVM_BAD | EVT_CM_PMNVM_GOOD, TIMEOUT_RESP, OS_OR, &lEvents );
                // Check status, then events.
                if( status != SUCCESSFUL )
                {
                    // Timed out: Has the CM or the PM gone off the rails?. 
                    cJSON_AddStringToObject(rspMsg, "Status", "Not Performed");
        			dbgTrace(DBG_LVL_ERROR, "DiagClearWasteTankReq: Request timed out."  );
                }
                else
                {
                    if( lEvents & EVT_CM_PMNVM_BAD ) 
                    {
                        // If event failed, give as much info as possible for debug. 
                        cJSON_AddStringToObject(rspMsg, "Status", "Not Performed");
        				dbgTrace(DBG_LVL_ERROR, "DiagClearWasteTankReq: Request Failed. Error Type= %d, Status=%d, Code=%d.", sPmNvmAccStatus.bCMErrorType,
                                            	sPmNvmAccStatus.lStatus, sPmNvmAccStatus.lErrCode );

                    }
                    else
                    {
                        cJSON_AddStringToObject(rspMsg, "Status", "Accepted");
                    }
                }
                // We are done using this service:  let someone else use it.
                OSSetEvents( EVENT_GROUP_CMPM, ~(EVT_CM_PMNVM_BAD | EVT_CM_PMNVM_GOOD), OS_AND );        
				fnClearDisabledStatus( DCOND_ACCESSING_PM_NVM ); 
            }
        }
    }

    addEntryToTxQueue(&entry, root, "  on_DiagClearWasteTankReq: Added DiagClearWasteTankRsp to tx queue. status=%d" CRLF);

}


/* ---------------------------------------------------------------------------------------------[on_DiagInitFileSystemReq]-- */
void on_DiagInitFileSystemReq(UINT32 handle, cJSON *root)
{
    cJSON *rspMsg = cJSON_CreateObject();

    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "DiagInitFileSystemRsp");

    bldr_info_set_flags(BI_FLAG_ERASE_HCC_FLASH |BI_FLAG_FORMAT_HCC_FFS | BI_FLAG_REBOOT_WHEN_DONE);

    cJSON_AddStringToObject(rspMsg, "Status", "Accepted");

    addEntryToTxQueue(&entry, root, "  on_DiagInitFileSystemReq: Added DiagInitFileSystemqRsp to tx queue. status=%d" CRLF);

}

/* -----------------------------------------------------------------------------------------------------------[EOF]-- */


