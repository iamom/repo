//=====================================================================
//
//	FileName:	HAL_AM335x_GPIO.c
//
//	Description: Hardware abstraction layer implementation for AM335x GPIO utilities
//=====================================================================
#include "commontypes.h"
#include "stdio.h"
#include "stddef.h"
#include "hal.h"
#include "am335x_pbdefs.h"
#include "HAL_AM335x_GPIO.h"
#include "bsp/csd_2_3_defs.h"

const UINT32 kGPIO1_CtrlOffset[] =
{
		0x800,	0x804,	0x808,	0x80C,	0x810,	0x814,	0x818,	0x81C,
		0x968,	0x96C,	0x970,	0x974,	0x830,	0x834,	0x838,	0x83C,
		0x840,	0x844,	0x848,	0x84C,	0x850,	0x854,	0x858,	0x85C,
		0x860,	0x864,	0x868,	0x86C,	0x878,	0x87C,	0x880,	0x884
};


const UINT32 kGPIO2_CtrlOffset[] =
{
		0x888,	0x88C,	0x890,	0x894,	0x898,	0x89C,	0x8A0,	0x8A4,
		0x8A8,	0x8AC,	0x8B0,	0x8B4,	0x8B8,	0x8BC,	0x8C0,	0x8C4,
		0x8C8,	0x8CC,	0x934,	0x938,	0x93C,	0x940,	0x8E0,	0x8E4,
		0x8E8,	0x8EC,	0x8F0,	0x8F4,	0x8F8,	0x8FC,	0x900,	0x904
};

const UINT32 kGPIO3_CtrlOffset[] =
{
		0x908,	0x90C,	0x910,	0x914,	0x918,	0x988,	0x98C,	0x9E4,
		0x9E8,	0x92C,	0x930,	0x000,	0x000,	0xA34,	0x990,	0x994,
		0x998,	0x99C,	0x9A0,	0x9A4,	0x9A8,	0x9AC,	0x000,	0x000,
		0x000,	0x000,	0x000,	0x000,	0x000,	0x000,	0x000,	0x000
};


// this array is indexed by the GPIO bank to get the base address
const unsigned int GPIOAddr[] = {
		AM335X_GPIO0_BASE, AM335X_GPIO1_BASE, AM335X_GPIO2_BASE, AM335X_GPIO3_BASE
};

// Sets up GPIO pin based on configuration
void setupGPIOPin(GPIO_INFO pinInfo)
{
	unsigned writeVal = 0;

	// Muxmode set to 7, slow slew rate
	writeVal |= (AM335X_CTRL_PADCONF_SLEW_SLOW | AM335X_CTRL_PADCONF_MODE7);

	if (pinInfo.output_dir == DIR_OUTPUT)
	{
		writeVal |= AM335X_CTRL_PADCONF_INPUT_DIS;
	}
	else
	{
		writeVal |= AM335X_CTRL_PADCONF_INPUT_EN;

	}

	if (pinInfo.pull_type == GPIO_PULL_UP)
	{
		writeVal |= (AM335X_CTRL_PADCONF_PULL_EN | AM335X_CTRL_PADCONF_PULL_UP);
	}
	else if (pinInfo.pull_type == GPIO_PULL_DOWN)
	{
		writeVal |= (AM335X_CTRL_PADCONF_PULL_EN | AM335X_CTRL_PADCONF_PULL_DOWN);
	}
	else
	{
		writeVal |= AM335X_CTRL_PADCONF_PULL_DIS;
	}

    ESAL_GE_MEM_WRITE32(AM335X_CTRL_MODULE_BASE + pinInfo.cm_offset, writeVal);

}

// Outputs value on specified pin
void setGPIOPin(GPIO_INFO pinInfo, unsigned char value)
{

#if 0  // enable for debugging purposes
	if (pinInfo.output_dir != DIR_OUTPUT)
	{ // originally this is an input or an in/out pin; confirm this pin is set to output now
		if ((ESAL_GE_MEM_READ32(GPIOAddr[pinInfo.bank] + AM335X_GPIO_OE) & pinInfo.bit_position)
		{ // this is an input so just return
			return;
		}
	}
#endif

	if (value)
	{// set high
	    ESAL_GE_MEM_WRITE32(GPIOAddr[pinInfo.bank] + AM335X_GPIO_SETDATAOUT, pinInfo.bit_position);
	}
	else
	{ // set low
	    ESAL_GE_MEM_WRITE32(GPIOAddr[pinInfo.bank] + AM335X_GPIO_CLEARDATAOUT, pinInfo.bit_position);
	}
}

// Outputs 1 faster than setGPIOPin
// Assumes output pin
void setGPIOPinHigh(GPIO_INFO pinInfo)
{
    ESAL_GE_MEM_WRITE32(GPIOAddr[pinInfo.bank] + AM335X_GPIO_SETDATAOUT, pinInfo.bit_position);
}

// Outputs 0 faster than setGPIOPin
// Assumes output pin
void setGPIOPinLow(GPIO_INFO pinInfo)
{
    ESAL_GE_MEM_WRITE32(GPIOAddr[pinInfo.bank] + AM335X_GPIO_CLEARDATAOUT, pinInfo.bit_position);
}

// Reads value on specified pin
unsigned char readGPIOPin(GPIO_INFO pinInfo)
{
    unsigned char retVal = 0;

#if 0  // enable for debugging purposes
    // confirm this is an input pin
	if ((pinInfo.output_dir == DIR_OUTPUT) || (ESAL_GE_MEM_READ32(GPIOAddr[pinInfo.bank] + AM335X_GPIO_OE) & pinInfo.bit_position ) == pinInfo.bit_position))
	{  // this is configured as an output so return error
			return GPIO_ERROR;
	}
#endif

	retVal = ( ( ESAL_GE_MEM_READ32(GPIOAddr[pinInfo.bank] + AM335X_GPIO_DATAIN) & pinInfo.bit_position ) == pinInfo.bit_position );
	return retVal;
}

// Sets direction of specified pin
// On reset, output pin outputs a 0
void setDirGPIOPin(GPIO_INFO pinInfo, unsigned char direction)
{
	unsigned readVal;

    readVal = ESAL_GE_MEM_READ32(GPIOAddr[pinInfo.bank] + AM335X_GPIO_OE);

    // pulls can stay for bidir pins
	pinInfo.output_dir = direction;
    if (direction == DIR_OUTPUT)
    {
	    ESAL_GE_MEM_WRITE32(GPIOAddr[pinInfo.bank] + AM335X_GPIO_OE, readVal & ~(pinInfo.bit_position));
    }
    else
    {
	    ESAL_GE_MEM_WRITE32(GPIOAddr[pinInfo.bank] + AM335X_GPIO_OE, readVal | pinInfo.bit_position);
    }

}

// All pins in input table are set up
// Other pins on GPIO bank should not be affected
void HALInitGPIOPins(P_GPIO_INFO pGPIOTable, unsigned numPinsUsed)
{
    unsigned pinIndex;
    unsigned bank;
    unsigned outputPinPosition;
	unsigned readVal;

    if (numPinsUsed < 1)
    	return;

    for (pinIndex = 0; pinIndex < numPinsUsed; pinIndex++)
    {
    	bank = (*(pGPIOTable + pinIndex)).bank;

        // set clock control to enable module if not already enabled
    	switch(bank)
    	{
    		case GPIO_BANK_0:
    	        ESAL_GE_MEM_WRITE32(AM335X_CM_PER_BASE + AM335X_CM_WKUP_OFFSET + AM335X_CM_WKUP_GPIO0_CLKCTRL, AM335X_CM_CLKCTRL_MODULEMODEEN);
    			break;
    		case GPIO_BANK_1:
    	        ESAL_GE_MEM_WRITE32(AM335X_CM_PER_BASE + AM335X_CM_PER_OFFSET + AM335X_CM_PER_GPIO1_CLKCTRL, AM335X_CM_CLKCTRL_MODULEMODEEN);
    			break;
    		case GPIO_BANK_2:
    	        ESAL_GE_MEM_WRITE32(AM335X_CM_PER_BASE + AM335X_CM_PER_OFFSET + AM335X_CM_PER_GPIO2_CLKCTRL, AM335X_CM_CLKCTRL_MODULEMODEEN);
    			break;
    		case GPIO_BANK_3:
    	        ESAL_GE_MEM_WRITE32(AM335X_CM_PER_BASE + AM335X_CM_PER_OFFSET + AM335X_CM_PER_GPIO3_CLKCTRL, AM335X_CM_CLKCTRL_MODULEMODEEN);
    			break;
    	}

        // Set pin MUX settings - Enable pullup/pulldown as appropriate; set mux to GPIO; pull down input and bi-dir pins
    	setupGPIOPin( *(pGPIOTable + pinIndex) );

        // enable GPIO bank
        readVal = ESAL_GE_MEM_READ32(GPIOAddr[bank] + AM335X_GPIO_CTRL);
	    ESAL_GE_MEM_WRITE32(GPIOAddr[bank] + AM335X_GPIO_CTRL, readVal & ~(AM335X_GPIO_CTRL_DISABLEMODULE));

        // set up output enable based on direction
    	outputPinPosition = (*(pGPIOTable + pinIndex)).bit_position;
        readVal = ESAL_GE_MEM_READ32(GPIOAddr[bank] + AM335X_GPIO_OE);
    	if ((*(pGPIOTable + pinIndex)).output_dir == DIR_OUTPUT)
    	{
            // Set pin to output; the rest are not affected
    	    ESAL_GE_MEM_WRITE32(GPIOAddr[bank] + AM335X_GPIO_OE, readVal & ~(outputPinPosition));
            // Set output pin low
            ESAL_GE_MEM_WRITE32(GPIOAddr[bank] + AM335X_GPIO_CLEARDATAOUT, outputPinPosition);
    	}
    	else
    	{// Set pin to input; the rest are not affected
    	    ESAL_GE_MEM_WRITE32(GPIOAddr[bank] + AM335X_GPIO_OE, readVal | outputPinPosition);

    	}
    }

}

// All registers are initialized.
// All pins are affected so this should be called before all GPIO activity
void HALInitGPIORegisters(void)
{
	unsigned readVal;

    // Clock Enable
    //GPIO0
    ESAL_GE_MEM_WRITE32(AM335X_CM_PER_BASE + AM335X_CM_WKUP_OFFSET + AM335X_CM_WKUP_GPIO0_CLKCTRL,
    		AM335X_CLKCTRL_OPTFCLKEN_GPIO0_GDBCLK |  // same value for all GPIO banks
    		AM335X_CM_CLKCTRL_IDLEST_FUNC |
    		AM335X_CM_CLKCTRL_MODULEMODEEN
    		);

    //GPIO1
    ESAL_GE_MEM_WRITE32(AM335X_CM_PER_BASE + AM335X_CM_PER_OFFSET + AM335X_CM_PER_GPIO1_CLKCTRL,
    		AM335X_CLKCTRL_OPTFCLKEN_GPIO0_GDBCLK |  // same value for all GPIO banks
    		AM335X_CM_CLKCTRL_IDLEST_FUNC |
    		AM335X_CM_CLKCTRL_MODULEMODEEN
    		);

    //GPIO2
    ESAL_GE_MEM_WRITE32(AM335X_CM_PER_BASE + AM335X_CM_PER_OFFSET + AM335X_CM_PER_GPIO2_CLKCTRL,
    		AM335X_CLKCTRL_OPTFCLKEN_GPIO0_GDBCLK |  // same value for all GPIO banks
    		AM335X_CM_CLKCTRL_IDLEST_FUNC |
    		AM335X_CM_CLKCTRL_MODULEMODEEN
    		);

#ifdef BOOTROM  //Do not execute in RAM because of DDR_VTT_EN!
    //GPIO3
    ESAL_GE_MEM_WRITE32(AM335X_CM_PER_BASE + AM335X_CM_PER_OFFSET + AM335X_CM_PER_GPIO3_CLKCTRL,
    		AM335X_CLKCTRL_OPTFCLKEN_GPIO0_GDBCLK |  // same value for all GPIO banks
    		AM335X_CM_CLKCTRL_IDLEST_FUNC |
    		AM335X_CM_CLKCTRL_MODULEMODEEN
    		);
#endif

    // Wait for CM to settle.
    //GPIO0
    while((ESAL_GE_MEM_READ32(AM335X_CM_PER_BASE + AM335X_CM_WKUP_OFFSET + AM335X_CM_WKUP_GPIO0_CLKCTRL) & AM335X_CM_CLKCTRL_IDLEST) !=
            AM335X_CM_CLKCTRL_IDLEST_FUNC);

    //GPIO1
    while((ESAL_GE_MEM_READ32(AM335X_CM_PER_BASE + AM335X_CM_PER_OFFSET + AM335X_CM_PER_GPIO1_CLKCTRL) & AM335X_CM_CLKCTRL_IDLEST) !=
            AM335X_CM_CLKCTRL_IDLEST_FUNC);

    //GPIO2
    while((ESAL_GE_MEM_READ32(AM335X_CM_PER_BASE + AM335X_CM_PER_OFFSET + AM335X_CM_PER_GPIO2_CLKCTRL) & AM335X_CM_CLKCTRL_IDLEST) !=
            AM335X_CM_CLKCTRL_IDLEST_FUNC);

#ifdef BOOTROM  //Do not execute in RAM because of DDR_VTT_EN!
    //GPIO3
    while((ESAL_GE_MEM_READ32(AM335X_CM_PER_BASE + AM335X_CM_PER_OFFSET + AM335X_CM_PER_GPIO3_CLKCTRL) & AM335X_CM_CLKCTRL_IDLEST) !=
            AM335X_CM_CLKCTRL_IDLEST_FUNC);
#endif

    // Module Enable
    //GPIO0
    readVal = ESAL_GE_MEM_READ32(AM335X_GPIO0_BASE + AM335X_GPIO_CTRL);
    ESAL_GE_MEM_WRITE32(AM335X_GPIO0_BASE + AM335X_GPIO_CTRL, readVal & ~(AM335X_GPIO_CTRL_DISABLEMODULE));

    //GPIO1
    readVal = ESAL_GE_MEM_READ32(AM335X_GPIO1_BASE + AM335X_GPIO_CTRL);
    ESAL_GE_MEM_WRITE32(AM335X_GPIO1_BASE + AM335X_GPIO_CTRL, readVal & ~(AM335X_GPIO_CTRL_DISABLEMODULE));

    //GPIO2
    readVal = ESAL_GE_MEM_READ32(AM335X_GPIO2_BASE + AM335X_GPIO_CTRL);
    ESAL_GE_MEM_WRITE32(AM335X_GPIO2_BASE + AM335X_GPIO_CTRL, readVal & ~(AM335X_GPIO_CTRL_DISABLEMODULE));

#ifdef BOOTROM  //Do not execute in RAM because of DDR_VTT_EN!
    //GPIO3
    readVal = ESAL_GE_MEM_READ32(AM335X_GPIO3_BASE + AM335X_GPIO_CTRL);
    ESAL_GE_MEM_WRITE32(AM335X_GPIO3_BASE + AM335X_GPIO_CTRL, readVal & ~(AM335X_GPIO_CTRL_DISABLEMODULE));
#endif

    // Module Reset
    //GPIO0
    readVal = ESAL_GE_MEM_READ32(AM335X_GPIO0_BASE + AM335X_GPIO_SYSCONFIG);
    ESAL_GE_MEM_WRITE32(AM335X_GPIO0_BASE + AM335X_GPIO_SYSCONFIG, readVal | AM335X_GPIO_SOFTRESET);
    while((ESAL_GE_MEM_READ32(AM335X_GPIO0_BASE + AM335X_GPIO_SYSCONFIG) & AM335X_GPIO_SOFTRESET) != 0);

    //GPIO1
    readVal = ESAL_GE_MEM_READ32(AM335X_GPIO1_BASE + AM335X_GPIO_SYSCONFIG);
    ESAL_GE_MEM_WRITE32(AM335X_GPIO1_BASE + AM335X_GPIO_SYSCONFIG, readVal | AM335X_GPIO_SOFTRESET);
    while((ESAL_GE_MEM_READ32(AM335X_GPIO1_BASE + AM335X_GPIO_SYSCONFIG) & AM335X_GPIO_SOFTRESET) != 0);

    //GPIO2
    readVal = ESAL_GE_MEM_READ32(AM335X_GPIO2_BASE + AM335X_GPIO_SYSCONFIG);
    ESAL_GE_MEM_WRITE32(AM335X_GPIO2_BASE + AM335X_GPIO_SYSCONFIG, readVal | AM335X_GPIO_SOFTRESET);
    while((ESAL_GE_MEM_READ32(AM335X_GPIO2_BASE + AM335X_GPIO_SYSCONFIG) & AM335X_GPIO_SOFTRESET) != 0);

#ifdef BOOTROM  //Do not execute in RAM because of DDR_VTT_EN!
    //GPIO3
    readVal = ESAL_GE_MEM_READ32(AM335X_GPIO3_BASE + AM335X_GPIO_SYSCONFIG);
    ESAL_GE_MEM_WRITE32(AM335X_GPIO3_BASE + AM335X_GPIO_SYSCONFIG, readVal | AM335X_GPIO_SOFTRESET);
    while((ESAL_GE_MEM_READ32(AM335X_GPIO3_BASE + AM335X_GPIO_SYSCONFIG) & AM335X_GPIO_SOFTRESET) != 0);
#endif

    // Disable Interrupts
    //GPIO0
    ESAL_GE_MEM_WRITE32 (AM335X_GPIO0_BASE + AM335X_GPIO_IRQSTATUS_CLR_0, AM335X_GPIO_ALL_BITS);
    ESAL_GE_MEM_WRITE32 (AM335X_GPIO0_BASE + AM335X_GPIO_IRQSTATUS_CLR_1, AM335X_GPIO_ALL_BITS);

    //GPIO1
    ESAL_GE_MEM_WRITE32 (AM335X_GPIO1_BASE + AM335X_GPIO_IRQSTATUS_CLR_0, AM335X_GPIO_ALL_BITS);
    ESAL_GE_MEM_WRITE32 (AM335X_GPIO1_BASE + AM335X_GPIO_IRQSTATUS_CLR_1, AM335X_GPIO_ALL_BITS);

    //GPIO2
    ESAL_GE_MEM_WRITE32 (AM335X_GPIO2_BASE + AM335X_GPIO_IRQSTATUS_CLR_0, AM335X_GPIO_ALL_BITS);
    ESAL_GE_MEM_WRITE32 (AM335X_GPIO2_BASE + AM335X_GPIO_IRQSTATUS_CLR_1, AM335X_GPIO_ALL_BITS);

#ifdef BOOTROM  //Do not execute in RAM because of DDR_VTT_EN!
    //GPIO3
    ESAL_GE_MEM_WRITE32 (AM335X_GPIO3_BASE + AM335X_GPIO_IRQSTATUS_CLR_0, AM335X_GPIO_ALL_BITS);
    ESAL_GE_MEM_WRITE32 (AM335X_GPIO3_BASE + AM335X_GPIO_IRQSTATUS_CLR_1, AM335X_GPIO_ALL_BITS);
#endif

}

///////////////////////////////////////////////////////////////////////////////////////
// readGPIOPin
// read the input of the specified GPIO and pin. returs 0 if pin is 0 or 1 if pin is 1
UINT readSimpleGPIOPin(UINT gpio, UINT pin)
{
	UINT val = ESAL_GE_MEM_READ32(GPIOAddr[gpio] + AM335X_GPIO_DATAIN);
	return (val & (1 << pin)) == 0 ? 0 : 1 ;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// addressGPIOPin
// return the control module address for the gpio pin specified.
// a lot of options here so may not have populated all. Will return 0
// if not set.
UINT32 *addressGPIOPin(UINT gpio, UINT pin)
{
	UINT32 *adds = 0x0 ;

	switch(gpio)
	{
		case 0:
			break ;
		case 1:
			adds = (UINT32 *)kGPIO1_CtrlOffset[pin];
			break ;
		case 2:
			adds = (UINT32 *)kGPIO2_CtrlOffset[pin] ;
			break ;
		case 3:
			adds = (UINT32 *)kGPIO3_CtrlOffset[pin];
			break ;
	}

	if(adds != 0)
		adds = (UINT32 *)((UINT32)adds + (UINT32)ESAL_PR_CONTROL_BASE) ;

	return adds ;
}

///////////////////////////////////////////////////////////////////////////////////////
// clearGPIOPinAsOutput
// turn off the output mode of a GPIO pin
void clearGPIOPinAsOutput(UINT gpio, UINT pin)
{
	*((UINT32 *)GPIOAddr[gpio] + AM335X_GPIO_OE/4) |= (1 << (UINT32)pin) ;
}

void setGPIOPinAsOutput(UINT gpio, UINT pin)
{
	*((UINT32 *)GPIOAddr[gpio] + AM335X_GPIO_OE/4) &= ~(1 << (UINT32)pin) ;
}



