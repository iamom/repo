/************************************************************************
 *  PROJECT:        CSD 2/3
 *  MODULE NAME:    cplattask.c
 *  
 *  DESCRIPTION:    Communication Task for Platform Task
 *  				- it receives messages from the serial platform and sends
 *  				them to the platform task
 *
 * ----------------------------------------------------------------------
 *               Copyright (c) 2016 Pitney Bowes Inc.
 * ----------------------------------------------------------------------
 *  REVISION HISTORY:
  *************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>
#include "nucleus.h"
#include "services/reg_api.h"
#include "posix.h"
#include "serial.h"
#include "commontypes.h"
#include "pbos.h"
#include "UTILS.h"
#include "pb232.h"
#include "commglob.h"
#include "platuart.h"

//#define PLAT_BUF_DEBUG
#ifdef PLAT_COMM_BUF_DEBUG
#define PLAT_BUF_SIZE   100
char    rxPlatBuffer[PLAT_BUF_SIZE];
int     rxPlBufIdx=0;
#endif

#define MINIMUM_DLE_CHAR    0x80              /*Byte stuff char should not be less
                                                than 0x80                         */
#define DLE_MASK            0x7f              /*Used to strip DLE bit from data   */
#define SEQ_MASK            0x7f              /*Used to strip sequence bit from Class code. */
#define WAIT_STX            0                 /*State in LinkLayerReceive().         */
#define WAIT_HEADER         1                 /*State in LinkLayerReceive().         */
#define WAIT_CHAR           2                 /*State in LinkLayerReceive().         */
#define GOT_DLE             3                 /*State in LinkLayerReceive().         */

#define PLATFORM_MSG_DELIMITER	0x0D	// Use Carriage Return to delimit msgs
#define MAX_PLATFORM_MSG		20

static DV_DEV_LABEL serial_device = { SERIAL_LABEL };
static LINK_LAYER_COMM llcomm;
static SERIAL_SESSION * pCOM_Port_Session;

// Global to allow platform task to enable/disable platform receiving
BOOL enPlatformReceive;

/******************************************************************************
*
* Description
*   This function specifies the serial device used to talk to the scale.
*
* Inputs
*   Nothing
*
* Returns
*   Nothing
*
* Revision History
*
*****************************************************************************/

void fnInitSerialDevice(void)
{
#ifdef CFG_NU_BSP_CSD_2_3_ENABLE
#define SERIAL_LABEL_KEY "/csd_2_3/fpga_serial/labels/fpga_uart"
#define SERIAL_STDIO_KEY "/csd_2_3/fpga_serial/stdio"
    STATUS stat;
    BOOLEAN stdio_en = NU_FALSE;
    NU_UNUSED_PARAM(stat);

    /* suspend if port is used for STDIO */
    stat = NU_Registry_Get_Boolean(SERIAL_STDIO_KEY, &stdio_en);
    if(stdio_en == NU_TRUE)
    {
    	dbgTrace(DBG_LVL_INFO, "!!!PlatCommTask serial port being used for stdio - suspending");
    	OSSuspendTask( CPLAT );      /* TBD no error reported ??? */
    }

	//Get instance label from Registry and use that
    if (NU_Registry_Get_Bytes(SERIAL_LABEL_KEY, (UINT8 *)(&serial_device), sizeof(DV_DEV_LABEL)) != NU_SUCCESS)
    {
    	dbgTrace(DBG_LVL_ERROR, "\nSerial port label %s not found\n", SERIAL_LABEL_KEY);
        OSSuspendTask(CPLAT);
    }

#endif
}


/******************************************************************************
*
* Description
*   This function initializes the link layer communication protocol used to talk to the scale.
*
* Inputs
*   Nothing
*
* Returns
*   Nothing
*
* Revision History
*
*****************************************************************************/

void fnInitPlatformLinkLayerComm(void)
{
	llcomm.bState = WAIT_STX;
	llcomm.fReceivingFrame = FALSE;
	llcomm.pbBuffer = NULL;
	llcomm.wBufferLength = 0;
}


/******************************************************************************
*
* Description
*   This function parses received characters from the platform and creates
*	messages that are then sent to the Platform task.
*
* Inputs
*   unsigned char bChar  	- Received character
*
* Returns
*   Nothing
*
* Revision History
*
*****************************************************************************/

void fnPlatformLinkLayerReceive(unsigned char bChar)
{
	switch (llcomm.bState)
	{
	case WAIT_STX:
		// This is the first character - reset buffer and check for valid commands
		switch (bChar)
		{
		case PBSTX:		// Start of message, set up buffer, but don't save STX character
			llcomm.fReceivingFrame = TRUE;
			llcomm.bState = WAIT_CHAR;
			llcomm.wBufferLength = 0;
			llcomm.pbBuffer = llcomm.msgBuf.bData;
			break;

		default:
			llcomm.pbBuffer = 0;
			break;
		}  // end switch(bChar)
		break;

	case WAIT_CHAR:
		switch (bChar)
		{
		case PLATFORM_MSG_DELIMITER:
			llcomm.bState = WAIT_STX;  // Msg complete
			llcomm.msgBuf.fBusy = true;
			llcomm.msgBuf.wLength = llcomm.wBufferLength;
 // take this out to avoid filling syslog with scale msgs
 //			dbgTrace(DBG_LVL_INFO, "PlatformCommTask - RX msg len: %d\n", llcomm.msgBuf.wLength);
			// send a copy of message to PLAT task
			(void)OSSendIntertask(PLAT, CPLAT, COMM_RECEIVE_MESSAGE, COMM_DATA, (void *) llcomm.pbBuffer, llcomm.msgBuf.wLength);
			break;

		case PBSTX:		// Received STX in the middle of msg - discard current msg
			// and start capturing new one  (DO WE NEED TO CLEAR BUFFER?)
			llcomm.wBufferLength = 0;
			break;

		default:
			// This is not the end of the message, so save the character			
			if (llcomm.pbBuffer)
			{
				*(llcomm.pbBuffer + llcomm.wBufferLength++) = bChar;
			}
			else //if remove and then attach the serial platform, we need reset the status.
			{
				llcomm.fReceivingFrame = FALSE;
				llcomm.bState = WAIT_STX;
				llcomm.wBufferLength = 0;
			}


			if (llcomm.wBufferLength > MAX_PLATFORM_MSG)
			{	// Message too long - ignore it and look for new message
				llcomm.fReceivingFrame = FALSE;
				llcomm.bState = WAIT_STX;
				llcomm.wBufferLength = 0;
			}
			break;
		}  // end switch (bChar)
		break;

	default:
		llcomm.bState = WAIT_STX;
		llcomm.fReceivingFrame = FALSE;
		break;
	}  // End switch(llcomm.bState))

}


/* **********************************************************************
// PURPOSE:     Handler for messages from serial scale
//
// INPUTS:
// **********************************************************************/
void PlatformCommunicationTask (unsigned long argc, void *argv)
{
	STATUS status;
	int ch;

	fnSysLogTaskStart( "PlatCommTask", CPLAT );

	/* Open device */
	fnInitSerialDevice();
	dbgTrace(DBG_LVL_INFO, "PlatCommTask running ...\n");

	status = NU_Serial_Open(&serial_device, &pCOM_Port_Session);
    if (status != NU_SUCCESS)
    {
    	dbgTrace(DBG_LVL_ERROR, "Serial port open failed: status: %d\n", status);
        OSSuspendTask(CPLAT);
    }

	/* Set read mode to suspend */
	status = NU_Serial_Set_Read_Mode(pCOM_Port_Session, NU_SUSPEND, NULL);
   if (status != NU_SUCCESS)
    {
	    dbgTrace(DBG_LVL_ERROR, "Serial port set read mode failed: status: %d\n", status);
        OSSuspendTask(CPLAT);
    }

    // Initialize link layer
    fnInitPlatformLinkLayerComm();
    enPlatformReceive = TRUE;

    while (enPlatformReceive)
	{
		ch = NU_Serial_Getchar(pCOM_Port_Session);
		// NU_Serial_Putchar (pCOM_Port_Session, ch);  // for loopback
		fnPlatformLinkLayerReceive((unsigned char) ch);
	}

    NU_Serial_Close(pCOM_Port_Session);
    fnSysLogTaskStart( "PlatCommTask Exit", CPLAT );
}

/******************************************************************************
*
* Description
*   This function transmits a message using the link layer protocol used to talk to the scale.
*   It uses the same serial session as the CPLAT task.
*
* Inputs
*   Nothing
*
* Returns
*   Nothing
*
* Revision History
*
*****************************************************************************/

BOOL fnTransmitPlatformLinkLayerComm(const char * pMsg)
{
	  BOOL retval = SUCCESSFUL;
	  int ch;

	  ch = NU_Serial_Puts(pCOM_Port_Session, pMsg);

	  if (ch == NU_EOF)
	  {
		  retval = FAILURE;
	  }
	  return (retval);

}

