/*
 * sys_log_demo.c:  a simple demonstration of a system log retrievable via Websockets & HTTP_Lite server / WebApp 
 *
 */

#include "nucleus.h"
#include "api_utils.h"
#include "networking/nu_networking.h"
#include "os/networking/http/inc/http_lite_svr.h"
#include "storage/nu_storage.h"
#include "sys_log_demo.h"
#include "pbos.h"
#include "debugTask.h"
#include "gtypes.h"
#include "imagegen.h"
#include "clock.h"
#include "pb_tracing.h"
#include "networkmonitor.h"
#include "ossetup.h"
#include "counters/fram_log.h"
#include "api_funds.h"
#include "trmstrg.h"
#include "sig.h"
#include "intellilink.h"

#define HTTP_BUFFER_SIZE		24000

uint32_t GetSystemTime();

extern int verbosity;
extern INTERTASK_LOG_ENTRY pIntertaskMsgLog[];
extern NLOG_ENTRY NLOG_Entry_List[NLOG_MAX_ENTRIES];
extern const char *api_status_to_string_tbl[];
extern const char *log_names[];
extern CMOS_Signature CMOSSignature;

extern HTTP_SVR_SESSION *HTTP_Lite_Get_Server_Session(INT handle);
extern bool is_net0_or_net1_sockaddr(struct sockaddr_struct *addr);

#define SYSLOG_PAYLOAD_BYTES    40

typedef struct {
    uint32_t  timestamp;    // GetSystemTime()
    uint16_t  seq_num;      // sequence number
    uint8_t   source;       // LOG_SRC_*
    uint8_t   type;         // LT_STRING, LT_PSD_REGS, etc
    uint8_t   data[SYSLOG_PAYLOAD_BYTES];   // payload
} SYS_LOG_ENTRY;

#define SYS_LOG_SIZE 300

SYS_LOG_ENTRY SysLog[SYS_LOG_SIZE];

uint16_t log_start;     // index of oldest log entry
uint16_t log_end;       // index of where to write the next log entry
//  log_start == log_end   --> log is empty
//  log_end+1 == log_Start --> log is full
uint16_t seq_num;
bool logging_disabled = true;

static STATUS write_rsp_body_str(INT handle, const char *str);
#define WRITE_RSP_BODY_STR(status,handle,str) do { if ( NU_SUCCESS != (status = write_rsp_body_str(handle, str))) return status; } while (0)

/* --------------------------------------------------------------------------------------------------[sys_log_init]-- */
void sys_log_init()
{
    sys_log_disable();
    memset(SysLog, 0, sizeof(SysLog));
    log_start = log_end = 0;
    seq_num = 0;
    sys_log_enable();
    sys_log_add_string(LOG_SRC_SYSLOG, "Initialized SystemLog");
}

/* -----------------------------------------------------------------------------------------------[sys_log_disable]-- */
void sys_log_disable()
{
    logging_disabled = true;
}

/* ------------------------------------------------------------------------------------------------[sys_log_enable]-- */
void sys_log_enable()
{
    logging_disabled = false;
}

/* ---------------------------------------------------------------------------------------------------[sys_log_add]-- */
void sys_log_add(log_src_e src, log_type_e type, void *p_data, size_t data_len)
{
    SYS_LOG_ENTRY *pLog = &SysLog[log_end];

    if (logging_disabled) return;

    pLog->timestamp = GetSystemTime();
    pLog->seq_num = seq_num++;
    pLog->source = src;
    pLog->type = type;

    if (data_len > SYSLOG_PAYLOAD_BYTES) data_len = SYSLOG_PAYLOAD_BYTES;

    memcpy(pLog->data, p_data, data_len);
    memset(pLog->data+data_len, 0, SYSLOG_PAYLOAD_BYTES - data_len);    // zero out rest of data field

    log_end = (log_end + 1) % SYS_LOG_SIZE;
    if (log_start == log_end)
    {
        log_start = (log_start + 1) % SYS_LOG_SIZE;
    }
}

/* --------------------------------------------------------------------------------------------[sys_log_add_string]-- */
void sys_log_add_string(log_src_e src, char *str)
{
    if (logging_disabled) return;

    sys_log_add(src, LT_STRING, str, strlen(str));
}

/* -------------------------------------------------------------------------------------------[sys_log_add_sprintf]-- */
void sys_log_add_sprintf(log_src_e src, const char *fmt, ...)
{
    char    buf[200];
    va_list argp;

    if (logging_disabled) return;

    va_start(argp, fmt);
    vsprintf(buf, fmt, argp);
    sys_log_add_string(src, buf);
    va_end(argp);
}

/* =======================================================================================[log retrieval functions]== */

/* -----------------------------------------------------------------------------------------------------[dump_dict]-- */
static void dump_dict(HTTP_LITE_DICT *dict)
{
	INT32  dict_index  = 0;
	CHAR   *key        = NU_NULL;
	CHAR   *value      = NU_NULL;


    /* Loop all the tokens in the dictionary. */
    while (DICT_Next(dict, &key, &value, &dict_index) == NU_SUCCESS)
    {
        /* If a value was assigned to the this token.*/
        if (value != NU_NULL)
        	dbgTrace(DBG_LVL_INFO, "     %s : %s\r\n", key, value);
        else
        	dbgTrace(DBG_LVL_INFO, "     %s : [NULL]\r\n", key);
    }
}

/* ------------------------------------------------------------------------------------------[display_request_info]-- */
static void display_request_info(CHAR *handler_name, INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
    if (verbosity > 0)
    {
		dbgTrace(DBG_LVL_INFO, "\r\n===========================[%s]==\r\n", handler_name);
		dbgTrace(DBG_LVL_INFO, "   handle=%d\r\n", handle);
		dbgTrace(DBG_LVL_INFO, "   method=%s\r\n", request->method);
        dbgTrace(DBG_LVL_INFO, "   uri=%s\r\n", request->uri);
        dbgTrace(DBG_LVL_INFO, "   headers:\r\n");
        dump_dict(request->headers);

        dbgTrace(DBG_LVL_INFO, " params:\r\n");
        dump_dict(request->params);
    }
}

/* ---------------------------------------------------------------------------------------------[read_request_body]-- */
static void read_request_body(INT handle, CHAR *rx_buffer, UINT32 *rx_len)
{
    STATUS status = HTTP_ERROR_UNREAD_DATA;

    UINT32 buf_size_left = (*rx_len)-1;
    UINT32 read_size;
    CHAR   *rx_ptr = rx_buffer;

	while (status == HTTP_ERROR_UNREAD_DATA && buf_size_left > 0)
	{
		read_size = buf_size_left;
		status = HTTP_Lite_Read_Request_Body(handle, rx_ptr, &read_size);
		rx_ptr += read_size;
		buf_size_left -= read_size;
	}
	/* Null terminate the buffer. */
	*rx_ptr = '\0';
	*rx_len = rx_ptr - rx_buffer;

    if (verbosity > 1)
    {
		dbgTrace(DBG_LVL_INFO, "                body=%s\r\n", rx_buffer);
		dbgTrace(DBG_LVL_INFO, "\r\n---------------------------\r\n");
    }

}

/* --------------------------------------------------------------------------------------[tx_http_file_get_rsp_hdr]-- */
STATUS tx_http_file_get_rsp_hdr(INT handle, INT filesize)
{
    STATUS status;

    if (verbosity > 1)
    {
        dbgTrace(DBG_LVL_INFO, "              tx_len=%d\r\n", filesize);
        dbgTrace(DBG_LVL_INFO, "Write_Response_header...............");
    }
    status = HTTP_Lite_Write_Response_Header(handle, 200, "All OK", filesize, NU_NULL);
    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "OK\r\n");

    return status;
}

/* -----------------------------------------------------------------------------------------------[write_http_hdrs]-- */
STATUS write_http_hdrs(INT handle, INT payload_size)
{
    STATUS status;
    HTTP_LITE_DICT *rsp_headers;

    if (verbosity > 1)
    {
        dbgTrace(DBG_LVL_INFO, "              payload_size=%d\r\n", payload_size);
        dbgTrace(DBG_LVL_INFO, "Write_Response_header...............");
    }

    /* Initialize the dictionary. */ 
    status = HTTP_LITE_HDR_Init(&rsp_headers, 5, 500); 
    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  HTTP_LITE_HDR_Init() Error = %d (0x%X)\r\n", status, status);
        return status;
    }

    if (payload_size == 0)
    {
        /* Add transfer encoding header in the dictionary. */ 
        status = HTTP_LITE_HDR_Add(rsp_headers, "Transfer-Encoding", 0, "chunked", 0);
        if (status != NU_SUCCESS)
        {
            if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  HTTP_LITE_HDR_Add() Error = %d (0x%X)\r\n", status, status);
            return status;
        }
    }

    status = HTTP_Lite_Write_Response_Header(handle, 200, "All OK", payload_size, rsp_headers);
    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  HTTP_Lite_Write_Response_Header() Error = %d (0x%X)\r\n", status, status);
        return status;
    }

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "OK\r\n");

    return status;
}

/* --------------------------------------------------------------------------[write_http_hdrs_unknown_payload_size]-- */
STATUS write_http_hdrs_unknown_payload_size(INT handle)
{
    return write_http_hdrs(handle, 0);
}

/* ----------------------------------------------------------------------------------------[csd_diagnostics_log_handler]-- */
STATUS csd_diagnostics_log_handler(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
    STATUS status = HTTP_ERROR_UNREAD_DATA;
    uint32_t tick_start = GetSystemTime();
    uint32_t tick_end;

	char	strLine[NLOG_MAX_BUFFER_SIZE+3];
	int bufferSize = 30000;
	char *bufferPtr;

	snprintf(strLine,NLOG_MAX_BUFFER_SIZE, "Start Time  %02d/%02d/%02d  %02d:%02d:%02d\r\n",
			globalNetlogStartTime.bMonth, globalNetlogStartTime.bDay, globalNetlogStartTime.bYear,
			globalNetlogStartTime.bHour, globalNetlogStartTime.bMinutes, globalNetlogStartTime.bSeconds);

    if (NU_SUCCESS != (status = write_http_hdrs_unknown_payload_size(handle)))
    {
    	dbgTrace(DBG_LVL_INFO, "Error in csd_diagnostics_log_handler sending initial response %d", status) ;
        return status;
    }

    status = HTTP_ERROR_UNSENT_DATA;

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "Write_Response_body.................");

    NU_Allocate_Memory(MEM_Cached, (void **)&bufferPtr, bufferSize, NU_NO_SUSPEND);
    if(!bufferPtr)
    {
        dbgTrace(DBG_LVL_ERROR, "csd_diagnostics_log_handler: %s", api_status_to_string_tbl[API_OUT_OF_MEMORY]);
    }
    else
    {

    	bufferPtr[0]=0;
	    WRITE_RSP_BODY_STR(status, handle, "CSDDiagnosticsLog\r\n");
	    WRITE_RSP_BODY_STR(status, handle, "DateTime ");
	    WRITE_RSP_BODY_STR(status, handle, strLine);
	    WRITE_RSP_BODY_STR(status, handle, "\r\n");
	    WRITE_RSP_BODY_STR(status, handle, "Pcn ");
	    WRITE_RSP_BODY_STR(status, handle, CMOSSignature.bUicPcn);
	    WRITE_RSP_BODY_STR(status, handle, "\r\nPSD Serial ");
	    WRITE_RSP_BODY_STR(status, handle, CMOSSignature.bUicSN);
	    WRITE_RSP_BODY_STR(status, handle, "\r\n");

		addDiagnosticsLog(bufferPtr, bufferSize);
		WRITE_RSP_BODY_STR(status, handle, bufferPtr);

		WRITE_RSP_BODY_STR(status, handle, "\r\n");
	    status = HTTP_Lite_Write_Response_Body(handle, NU_NULL, 0);   /* signal end of page */
	    if(status != NU_SUCCESS)
	    {
	        dbgTrace(DBG_LVL_ERROR, "Error csd_diagnostics_log_handler: HTTP_Lite_Write_Response_Body returned %d", status);
	    }
		//release memory
		if((status = NU_Deallocate_Memory(bufferPtr)) != NU_SUCCESS)
	    {
	        dbgTrace(DBG_LVL_ERROR, "csd_diagnostics_log_handler: %s", api_status_to_string_tbl[API_STATUS_DEALLOC_FAILED]);
	    }
	}

    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }
    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "OK\r\n");

    if (status == NU_SUCCESS && verbosity > 1)
    {
    	tick_end = GetSystemTime();
    	dbgTrace(DBG_LVL_INFO, "  duration: %u(ms)", (unsigned int) (tick_end - tick_start));
    }

    return status;
}




/* ----------------------------------------------------------------------------------------[csd_error_log_handler]-- */
STATUS csd_error_log_handler(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
    STATUS 	status 	= HTTP_ERROR_UNREAD_DATA;
    uint32_t tick_start = GetSystemTime();
    uint32_t tick_end;

	char	strLine[NLOG_MAX_BUFFER_SIZE+3];
	int bufferSize = HTTP_BUFFER_SIZE;
	char *bufferPtr;

	snprintf(strLine,NLOG_MAX_BUFFER_SIZE, "Start Time  %02d/%02d/%02d  %02d:%02d:%02d\r\n",
			globalNetlogStartTime.bMonth, globalNetlogStartTime.bDay, globalNetlogStartTime.bYear,
			globalNetlogStartTime.bHour, globalNetlogStartTime.bMinutes, globalNetlogStartTime.bSeconds);

    if (NU_SUCCESS != (status = write_http_hdrs_unknown_payload_size(handle)))
    {
    	dbgTrace(DBG_LVL_INFO, "Error in csd_error_log_handler sending initial response %d", status) ;
        return status;
    }

    status = HTTP_ERROR_UNSENT_DATA;
    NU_Allocate_Memory(MEM_Cached, (void **)&bufferPtr, bufferSize, NU_NO_SUSPEND);
    if(!bufferPtr)
    {
        dbgTrace(DBG_LVL_ERROR, "csd_error_log_handler: %s", api_status_to_string_tbl[API_OUT_OF_MEMORY]);
    }
    else
    {
    	bufferPtr[0] = 0 ;
	    WRITE_RSP_BODY_STR(status, handle, "<CSDErrorLog>");
	    WRITE_RSP_BODY_STR(status, handle, "<DateTime>");
	    WRITE_RSP_BODY_STR(status, handle, strLine);
	    WRITE_RSP_BODY_STR(status, handle, "</DateTime>");
	    WRITE_RSP_BODY_STR(status, handle, "<Pcn>");
	    WRITE_RSP_BODY_STR(status, handle, CMOSSignature.bUicPcn);
	    WRITE_RSP_BODY_STR(status, handle, "</Pcn><PSDSerial>");
	    WRITE_RSP_BODY_STR(status, handle, CMOSSignature.bUicSN);
	    WRITE_RSP_BODY_STR(status, handle, "</PSDSerial>");

		addSystemErrorLog(bufferPtr, bufferSize);
		WRITE_RSP_BODY_STR(status, handle, bufferPtr);

		WRITE_RSP_BODY_STR(status, handle, "</CSDErrorLog>");
		/* terminate message body. */
		status = HTTP_Lite_Write_Response_Body(handle, NULL, 0);

		//release memory
		if((status = NU_Deallocate_Memory(bufferPtr)) != NU_SUCCESS)
	    {
	        dbgTrace(DBG_LVL_ERROR, "csd_error_log_handler: %s", api_status_to_string_tbl[API_STATUS_DEALLOC_FAILED]);
	    }
    }

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "Write_Response_body.................");

    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }
    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "OK\r\n");

    if (status == NU_SUCCESS && verbosity > 1)
    {
    	tick_end = GetSystemTime();
    	dbgTrace(DBG_LVL_INFO, "  duration: %u(ms)", (unsigned int) (tick_end - tick_start));
    }

    return status;
}

/* ----------------------------------------------------------------------------------------[csd_system_log_handler]-- */
STATUS csd_system_log_handler(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
    STATUS 		status = HTTP_ERROR_UNREAD_DATA;
    INT 		tx_len;
    uint16_t 	log_idx=0;
    uint32_t 	tick_start = GetSystemTime();
    uint32_t 	tick_end;

    tx_len = INTERTASK_LOG_SIZE * sizeof(INTERTASK_LOG_ENTRY);

    if (NU_SUCCESS != (status = tx_http_file_get_rsp_hdr(handle, tx_len)))
        return status;

    status = HTTP_ERROR_UNSENT_DATA;

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "Write_Response_body.................");
    
    while (status == HTTP_ERROR_UNSENT_DATA)
    {
        /* Send message body. */
        status = HTTP_Lite_Write_Response_Body(handle, (char *)&pIntertaskMsgLog[log_idx], sizeof(INTERTASK_LOG_ENTRY));
        log_idx = (log_idx + 1) % INTERTASK_LOG_SIZE;
    }

    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }
    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "OK\r\n");

    if (status == NU_SUCCESS && verbosity > 1)
    {
    	tick_end = GetSystemTime();
    	dbgTrace(DBG_LVL_INFO, "  duration: %u(ms)", (unsigned int) (tick_end - tick_start));
    }
    return status;
}

/* ----------------------------------------------------------------------------------------[csd_net_log_handler]-- */
STATUS csd_net_log_handler(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
    STATUS 		status = HTTP_ERROR_UNREAD_DATA;
    uint16_t 	log_idx=0;
    uint32_t 	tick_start = GetSystemTime();
    uint32_t 	tick_end;
	char 		strLine[NLOG_MAX_BUFFER_SIZE+3];
	char 		*strLineEnd = strLine + NLOG_MAX_BUFFER_SIZE+3;
    INT 		iPass;
    CHAR 		*pIn, *pOut;

    if (NU_SUCCESS != (status = write_http_hdrs_unknown_payload_size(handle))) 
    {
    	dbgTrace(DBG_LVL_INFO, "Error in csd_net_log_handler sending initial response %d", status) ;
        return status;
    }

    status = HTTP_ERROR_UNSENT_DATA;

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "NL Write_Response_body.................");

	snprintf(strLine,NLOG_MAX_BUFFER_SIZE, "Start Time  %02d/%02d/%02d  %02d:%02d:%02d\r\n",
			globalNetlogStartTime.bMonth, globalNetlogStartTime.bDay, globalNetlogStartTime.bYear,
			globalNetlogStartTime.bHour, globalNetlogStartTime.bMinutes, globalNetlogStartTime.bSeconds);

	WRITE_RSP_BODY_STR(status, handle, "CSD Net Log\r\n");
	WRITE_RSP_BODY_STR(status, handle, "DateTime ");
	WRITE_RSP_BODY_STR(status, handle, strLine);
	WRITE_RSP_BODY_STR(status, handle, "\r\n");
	WRITE_RSP_BODY_STR(status, handle, "Pcn ");
	WRITE_RSP_BODY_STR(status, handle, CMOSSignature.bUicPcn);
	WRITE_RSP_BODY_STR(status, handle, "\r\nPSD Serial ");
	WRITE_RSP_BODY_STR(status, handle, CMOSSignature.bUicSN);
	WRITE_RSP_BODY_STR(status, handle, "\r\n\r\n");

    do
    {
    	pIn 	= NLOG_Entry_List[log_idx].log_msg ;
    	pOut 	= strLine;
    	memset(pOut, 0, NLOG_MAX_BUFFER_SIZE+2);
    	// remove CR LF from the single entry to make the result easier to read.
    	for(iPass=0 ; *pIn ; iPass++, pIn++, pOut++)
    	{
    		if(*pIn == DEBUG_LF) 		*pOut = '\t';
    		else if(*pIn == DEBUG_CR) 	*pOut = ' ';
    		else 						*pOut = *pIn;
    	}
    	// ensure CR-LF at the end of the line
    	safestrcat(strLine, "\r\n", strLineEnd);

        WRITE_RSP_BODY_STR(status, handle, strLine);
        log_idx = (log_idx + 1) % NLOG_MAX_ENTRIES;
    }while (log_idx != 0);

    WRITE_RSP_BODY_STR(status, handle,"\r\n\r\nnet log end\r\n");

    //---------------------------------------------

    status = HTTP_Lite_Write_Response_Body(handle, NU_NULL, 0);   /* signal end of page */
    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  NL Error = %d (0x%X)\r\n", status, status);
        return status;
    }
    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "NL OK\r\n");

    if (status == NU_SUCCESS && verbosity > 1)
    {
    	tick_end = GetSystemTime();
    	dbgTrace(DBG_LVL_INFO, "  NL duration: %u(ms)", (unsigned int) (tick_end - tick_start));
    }
    return status;
}

/* -----------------------------------------------------------------------------------------[csd_trace_log_handler]-- */
STATUS csd_trace_log_handler(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
    STATUS 		status = HTTP_ERROR_UNREAD_DATA;
    INT 		tx_len, sent_len;
    uint32_t 	tick_start = GetSystemTime();
    uint32_t 	tick_end;
    void 		*blk_1, *blk_2;
    uint32_t 	len_blk_1, len_blk_2;

    start_trace_upload(&blk_1, &len_blk_1, &blk_2, &len_blk_2);
    tx_len = len_blk_1 + len_blk_2;
    sent_len = 0 ;

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "TL Starting Trace: blk_1=0x%p len_1=%d  blk_2=0x%p len_2=%d tx_len=%d ",
                             blk_1, len_blk_1, blk_2, len_blk_2, tx_len);

    if (NU_SUCCESS != (status = tx_http_file_get_rsp_hdr(handle, tx_len)))
    {
    	dbgTrace(DBG_LVL_INFO, "Error in csd_trace_log_handler sending initial response %d", status) ;
        return status;
    }

    status = HTTP_ERROR_UNSENT_DATA;

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "TL Write_Response_body.................");

    // Slight problem here. We have told the system how many bytes we have to send but since this is text it will be a lot less. To save time rujnning the sending routine
    // twice (once to see how many characters and once to actually send them, with the chance that data turns up between the passes) we will send the
    // data then fill the rest with spaces.
    while (status == HTTP_ERROR_UNSENT_DATA)
    {
        if (len_blk_1 > 0)
        {
            /* Send message body. */
            status = HTTP_Lite_Write_Response_Body(handle, blk_1, len_blk_1);
            // track the actual amount of data sent...
            sent_len += len_blk_1;
            len_blk_1 = 0;
        }
        else if (len_blk_2 > 0)
        {
            /* Send message body. */
            status = HTTP_Lite_Write_Response_Body(handle, blk_2, len_blk_2);
            // track the actual amount of data sent...
            sent_len += len_blk_2;
            len_blk_2 = 0;
        }
        else
        {
        	break;  /* break out of the loop */
        }
    }

    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  TL Error = %d (0x%X)\r\n", status, status);
        return status;
    }
    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "TL OK\r\n");

    if (status == NU_SUCCESS && verbosity > 1)
    {
    	tick_end = GetSystemTime();
    	dbgTrace(DBG_LVL_INFO, "  TL duration: %u(ms)", (unsigned int) (tick_end - tick_start));
    }

    end_trace_upload();

    return status;
}

/* --------------------------------------------------------------------------------------------[write_rsp_body_str]-- */
static STATUS write_rsp_body_str(INT handle, const char *str)
{
    STATUS status = HTTP_Lite_Write_Response_Body(handle, (char *)str, strlen(str));
    if (status != NU_SUCCESS && status != HTTP_ERROR_UNSENT_DATA)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
    }
    else
        status = NU_SUCCESS;

    return status;
}


/* ---------------------------------------------------------------------------------------[csd_framlog_log_handler]-- */
STATUS csd_framlog_log_handler(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
    framlog_get_context_t ctx;
    fram_log_entry_hdr_t  hdr;
    STATUS 		status;
    uint32_t 	tick_start = GetSystemTime();
    uint32_t 	tick_end;
    char 		*p_msg;
    char 		entry_hdr_str[50];
	char 		strLine[NLOG_MAX_BUFFER_SIZE+3];

    if (NU_SUCCESS != (status = write_http_hdrs_unknown_payload_size(handle))) 
    {
    	dbgTrace(DBG_LVL_INFO, "Error in csd_framlog_log_handler sending initial response %d", status) ;
        return status;
    }

    status = HTTP_ERROR_UNSENT_DATA;

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "   Write_Response_body.................");

	snprintf(strLine,NLOG_MAX_BUFFER_SIZE, "Start Time  %02d/%02d/%02d  %02d:%02d:%02d\r\n",
			globalNetlogStartTime.bMonth, globalNetlogStartTime.bDay, globalNetlogStartTime.bYear,
			globalNetlogStartTime.bHour, globalNetlogStartTime.bMinutes, globalNetlogStartTime.bSeconds);
	WRITE_RSP_BODY_STR(status, handle, "CSD Fram Log\r\n");
	WRITE_RSP_BODY_STR(status, handle, "DateTime ");
	WRITE_RSP_BODY_STR(status, handle, strLine);
	WRITE_RSP_BODY_STR(status, handle, "\r\n");
	WRITE_RSP_BODY_STR(status, handle, "Pcn ");
	WRITE_RSP_BODY_STR(status, handle, CMOSSignature.bUicPcn);
	WRITE_RSP_BODY_STR(status, handle, "\r\nPSD Serial ");
	WRITE_RSP_BODY_STR(status, handle, CMOSSignature.bUicSN);
	WRITE_RSP_BODY_STR(status, handle, "\r\n\r\n");
    WRITE_RSP_BODY_STR(status, handle,"\r\nFram log\r\n");
    WRITE_RSP_BODY_STR(status, handle,"ctx    seq #  len    repeat  timestamp  msg\r\n");
    WRITE_RSP_BODY_STR(status, handle,"-----  -----  -----  ------  ---------  ------------------------------------------------------------------------------------------");

    if (Framlog_GetFirstEntry(&ctx, &hdr, (void **)&p_msg))
    {
        do
        {
            snprintf(entry_hdr_str, 50,"\r\n%5X  %5d  %5d  %6u  %9u  ", ctx.idx, hdr.seq_nr, hdr.len, hdr.repeat_count, hdr.timestamp * 10);
            WRITE_RSP_BODY_STR(status, handle, entry_hdr_str);
            WRITE_RSP_BODY_STR(status, handle, p_msg);
        } while(Framlog_GetNextEntry(&ctx, &hdr, (void **)&p_msg));
    }
    WRITE_RSP_BODY_STR(status, handle,"\r\n\r\nFram log end\r\n");

    //---------------------------------------------

    status = HTTP_Lite_Write_Response_Body(handle, NU_NULL, 0);   /* signal end of page */
    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "   Error = %d (0x%X)\r\n", status, status);
        return status;
    }

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "  Tx framlog OK\r\n");

    if (status == NU_SUCCESS && verbosity > 1)
    {
    	tick_end = GetSystemTime();
    	dbgTrace(DBG_LVL_INFO, "    Tx framlog duration: %u(ms)", (unsigned int) (tick_end - tick_start));
    }

    return status;
}

STATUS csd_trm_fram_log_handler(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
    STATUS 		status;
    uint32_t	tick_end;
    char		strTime[NLOG_MAX_BUFFER_SIZE+3];

    uint32_t 	tick_start 	= GetSystemTime();
    int32_t 	length 		= 0;
	int 		bufferSize 	= HTTP_BUFFER_SIZE;
	char 		*bufferPtr 	= NULL;
	int32_t 	count 		= 0;

    snprintf(strTime,NLOG_MAX_BUFFER_SIZE, " %02d/%02d/%02dT%02d:%02d:%02d",
    				globalNetlogStartTime.bMonth, globalNetlogStartTime.bDay, globalNetlogStartTime.bYear,
    				globalNetlogStartTime.bHour, globalNetlogStartTime.bMinutes, globalNetlogStartTime.bSeconds);


    if (NU_SUCCESS != (status = write_http_hdrs_unknown_payload_size(handle)))
    {
    	dbgTrace(DBG_LVL_INFO, "Error in csd_framlog_log_handler sending initial response %d", status) ;
        return status;
    }

    status = HTTP_ERROR_UNSENT_DATA;

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "   Write_Response_body.................");

    NU_Allocate_Memory(MEM_Cached, (void **)&bufferPtr, bufferSize, NU_NO_SUSPEND);
	if(!bufferPtr)
	{
		dbgTrace(DBG_LVL_ERROR, "csd_trm_fram_log_handler: %s", api_status_to_string_tbl[API_OUT_OF_MEMORY]);
	}
	else // RD - previously no protection if allocation failed 15/12/2017
	{
		WRITE_RSP_BODY_STR(status, handle, "<CSDTrmLog>");
		WRITE_RSP_BODY_STR(status, handle, "<DateTime>");
		WRITE_RSP_BODY_STR(status, handle, strTime);
		WRITE_RSP_BODY_STR(status, handle, "</DateTime>");
		WRITE_RSP_BODY_STR(status, handle, "<Pcn>");
		WRITE_RSP_BODY_STR(status, handle, CMOSSignature.bUicPcn);
		WRITE_RSP_BODY_STR(status, handle, "</Pcn><PSDSerial>");
		WRITE_RSP_BODY_STR(status, handle, CMOSSignature.bUicSN);
		WRITE_RSP_BODY_STR(status, handle, "</PSDSerial>");

		status = generate_fram_log_xml(bufferPtr, bufferSize, &length);
		WRITE_RSP_BODY_STR(status, handle,bufferPtr);

		if(generate_fram_bcuket_cnt(0))
		{
			//bucket 1
			WRITE_RSP_BODY_STR(status, handle,"<TrmArea1>");
			for( count = 0 ; count < CFG_NUM_RECORDS; count++)
			{
				memset(bufferPtr, 0, bufferSize);
				generate_fram_log_bcuket_xml(bufferPtr, bufferSize, &length, 0, count);
				if(length != 0)
					WRITE_RSP_BODY_STR(status, handle, bufferPtr);
			}
			WRITE_RSP_BODY_STR(status, handle,"</TrmArea1>");
		}


		if(generate_fram_bcuket_cnt(1))
		{
			//Bucket2
			WRITE_RSP_BODY_STR(status, handle,"<TrmArea2>");
			for( count = 0 ; count < CFG_NUM_RECORDS; count++)
			{
				memset(bufferPtr, 0, bufferSize);
				generate_fram_log_bcuket_xml(bufferPtr, bufferSize, &length, 1, count);
				if(length != 0)
					WRITE_RSP_BODY_STR(status, handle, bufferPtr);
			}
			WRITE_RSP_BODY_STR(status, handle,"</TrmArea2>");
		}

		WRITE_RSP_BODY_STR(status, handle,"</CSDTrmLog>");

		// RD previously no cleanup of allocated memory added 15/12/2017
		if(NU_Deallocate_Memory (bufferPtr) != NU_SUCCESS)
		{
			dbgTrace(DBG_LVL_ERROR, "csd_trm_fram_log_handler: Failed to deallocate memory\r\n");
		}
		//
	} // end of allocation failure protection...

    //---------------------------------------------

    status = HTTP_Lite_Write_Response_Body(handle, NU_NULL, 0);   /* signal end of page */
    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "   Error = %d (0x%X)\r\n", status, status);
        return status;
    }

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "  Tx framlog OK\r\n");

    if (status == NU_SUCCESS && verbosity > 1)
    {
    	tick_end = GetSystemTime();
    	dbgTrace(DBG_LVL_INFO, "    Tx framlog duration: %u(ms)", (unsigned int) (tick_end - tick_start));
    }

    return status;
}



/* ------------------------------------------------------------------------------------------------[csd_logs_index]-- */
STATUS csd_logs_index(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
    STATUS status = HTTP_ERROR_UNREAD_DATA;

    if (NU_SUCCESS != (status = write_http_hdrs_unknown_payload_size(handle)))
        return status;
    status = HTTP_ERROR_UNSENT_DATA;

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "Write_Response_body.................");

    // write data dynamically rather than building a strcat string...
    status = NU_SUCCESS;
	WRITE_RSP_BODY_STR(status, handle,"<html><body><H1>Index of /ms/logs</H1>");
	WRITE_RSP_BODY_STR(status, handle,"<p><a href=\"/ms/logs/");
	WRITE_RSP_BODY_STR(status, handle,log_names[LOG_SYSTEM]);
	WRITE_RSP_BODY_STR(status, handle,"\">csd-syslog</a></p>");
	WRITE_RSP_BODY_STR(status, handle,"<p><a href=\"/ms/logs/");
	WRITE_RSP_BODY_STR(status, handle,log_names[LOG_NETWORK]);
	WRITE_RSP_BODY_STR(status, handle,"\">csd-netlog</a></p>");
	WRITE_RSP_BODY_STR(status, handle,"<p><a href=\"/ms/logs/");
	WRITE_RSP_BODY_STR(status, handle,log_names[LOG_FRAM]);
	WRITE_RSP_BODY_STR(status, handle,"\">csd-framlog</a></p>");
	WRITE_RSP_BODY_STR(status, handle,"<p><a href=\"/ms/logs/");
	WRITE_RSP_BODY_STR(status, handle,log_names[LOG_ERROR]);
	WRITE_RSP_BODY_STR(status, handle,"\">csd-errorlog</a></p>");
	WRITE_RSP_BODY_STR(status, handle,"<p><a href=\"/ms/logs/");
	WRITE_RSP_BODY_STR(status, handle,log_names[LOG_DIAGNOSTICS]);
	WRITE_RSP_BODY_STR(status, handle,"\">csd-diagnosticslog</a></p>");
	WRITE_RSP_BODY_STR(status, handle,"<p><a href=\"/ms/logs/");
	WRITE_RSP_BODY_STR(status, handle,log_names[LOG_TRM_FRAM]);
	WRITE_RSP_BODY_STR(status, handle,"\">csd-trm-framlog</a></p>");
	WRITE_RSP_BODY_STR(status, handle,"</body></html>");

    if (status != NU_SUCCESS && status != HTTP_ERROR_UNSENT_DATA)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }
    
    status = HTTP_Lite_Write_Response_Body(handle, NU_NULL, 0);   /* signal end of page */
    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "   Error = %d (0x%X)\r\n", status, status);
        return status;
    }

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "OK\r\n");

    return status;
}

/* --------------------------------------------------------------------------------------------[connection_allowed]-- */
static bool connection_allowed(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
	//TODO - With new Nucleus release, confirm access of internal structures is still valid and check whether it can be replaced with API call
	struct sock_struct  tempSockStruct;
    STATUS              status;
    bool                allowed = false;
    INT 				socket;

    if ( CMOSglobalNetworkInformationStructure.eth0WsoxFlag == 1 )
    {
        /* connection from any interface allowed */
        allowed = true;
    }
    else
    {
        /* now we need to check that the interface the request came from is not eth0  */
        /* we do that by checking it isn't from either net0 or net 1 which have known */
        /* ip addresses                                                               */
        HTTP_SVR_SESSION *session = HTTP_Lite_Get_Server_Session(handle);
        if (session != NULL)
        {
            socket = session->sock.socket;
            status = SCK_Protect_Socket_Block(socket);
            if (status != NU_SUCCESS)
            {
                dbgTrace(DBG_LVL_ERROR, "Error: Invalid socket in connection_allowed(%d): status= %d\r\n", socket, status);
            }
            else
            {
    		    memcpy(&tempSockStruct, SCK_Sockets[socket], sizeof(tempSockStruct));
                SCK_Release_Socket();
                allowed = is_net0_or_net1_sockaddr(&tempSockStruct.s_local_addr);
                dbgTrace(DBG_LVL_ERROR, "  connection_allowed(%d): ip addr = %d.%d.%d.%d allowed=%d\r\n", 
                            handle, 
                            tempSockStruct.s_local_addr.ip_num.is_ip_addrs[0],
                            tempSockStruct.s_local_addr.ip_num.is_ip_addrs[1],
                            tempSockStruct.s_local_addr.ip_num.is_ip_addrs[2],
                            tempSockStruct.s_local_addr.ip_num.is_ip_addrs[3],
                            allowed);
            }
        }
    }

    if (!allowed)
    {
        /* first we need to read (and discard) the request body */
        UINT32 buffer_len = 0;

        STATUS status = HTTP_Lite_Read_Request_Body(handle, NU_NULL, &buffer_len);
        if (status != NU_SUCCESS && verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  connection_allowed(%d): Error discarding request body %d (0x%X)\r\n", handle, status, status);

        /* and then send the 403 response */
        status = HTTP_Lite_Write_Response_Header(handle,  403, "Connection not allowed", 0, NU_NULL);
        if (status != NU_SUCCESS && verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  connection_allowed(%d): Error sending 403 response %d (0x%X)\r\n", handle, status, status);
    }
    else
    {
        /* check to see if we need to change the task priority */
        NU_TASK *task_ptr = NU_Current_Task_Pointer();

        if (task_ptr != NU_NULL)
        {
            if (task_ptr->tc_priority != UIC_NORMAL_PRIORITY+1)
            {
                NU_Change_Priority(task_ptr, UIC_NORMAL_PRIORITY+1);
            }
        }

    }

    return allowed;
}

/* ----------------------------------------------------------------------------------------------[csd_logs_handler]-- */
STATUS csd_logs_handler(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
    STATUS status = HTTP_ERROR_UNREAD_DATA;
    UINT32 rx_len;
    CHAR   buffer[300];
    INT    tx_len;
    char  *rspFmt = "<html><body><H1>csd_logs_handler</H1><p>Unknown file %s</p></body></html>";
    char  *fname;

    if (!connection_allowed(handle, request, app_data))
        return HTTP_FORCE_CLOSE;

    display_request_info(" logs_handler ", handle, request, app_data);

    rx_len = sizeof(buffer);
    read_request_body(handle, buffer, &rx_len);

    fname = request->uri + strlen("/ms/logs/");

         if (strlen(fname                            ) == 0) return csd_logs_index(handle, request, app_data);
    else if (strcmp(fname, log_names[LOG_SYSTEM]     ) == 0) return csd_system_log_handler(handle, request, app_data);
    else if (strcmp(fname, log_names[LOG_SYSTEM_OLD] ) == 0) return csd_system_log_handler(handle, request, app_data);
    else if (strcmp(fname, log_names[LOG_NETWORK]    ) == 0) return csd_net_log_handler(handle, request, app_data);
    else if (strcmp(fname, log_names[LOG_NETWORK_OLD]) == 0) return csd_net_log_handler(handle, request, app_data);
    else if (strcmp(fname, log_names[LOG_TRACE]      ) == 0) return csd_trace_log_handler(handle, request, app_data);
    else if (strcmp(fname, log_names[LOG_TRACE_OLD]  ) == 0) return csd_trace_log_handler(handle, request, app_data);
    else if (strcmp(fname, log_names[LOG_FRAM]       ) == 0) return csd_framlog_log_handler(handle, request, app_data);
    else if (strcmp(fname, log_names[LOG_ERROR]      ) == 0) return csd_error_log_handler(handle, request, app_data);
    else if (strcmp(fname, log_names[LOG_DIAGNOSTICS]) == 0) return csd_diagnostics_log_handler(handle, request, app_data);
    else if (strcmp(fname, log_names[LOG_TRM_FRAM]   ) == 0) return csd_trm_fram_log_handler(handle, request, app_data);

    sprintf(buffer, rspFmt, fname);
    tx_len = strlen(buffer);
    if (verbosity > 1)
    {
        dbgTrace(DBG_LVL_INFO, "              tx_len=%d\r\n", tx_len);
        dbgTrace(DBG_LVL_INFO, "Write_Response_header...............");
    }
    status = HTTP_Lite_Write_Response_Header(handle, 200, "All OK", tx_len, NU_NULL);
    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "OK\r\n");

    status = HTTP_ERROR_UNSENT_DATA;

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "Write_Response_body.................");
    
    /* Send message body. */
    status = HTTP_Lite_Write_Response_Body(handle, buffer, tx_len);

    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }
    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "OK\r\n");

    return status;
}



/* --------------------------------------------------------------------------------------------------[parse_uint32]-- */
bool parse_uint32(const char *str, uint32_t *val)
{
    char *temp;
    bool rc = true;
    *val = strtoul(str, &temp, 0);

    if (temp == str || *temp != '\0' )
        rc = false;

    return rc;
}
 
/* -------------------------------------------------------------------------------------------------[replace_chars]-- */
char *replace_chars(char *s, char target_char, char replacement_char)
{
    char *p = s;
    while(*p != '\0')
    {
        if (*p == target_char)
            *p = replacement_char ;

        p++;
    }
    return s;
}

/* -------------------------------------------------------------------------------------------------[dir_info_page]-- */
STATUS dir_info_page(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data, char *path, char *extra_message)
{
    STATUS status = HTTP_ERROR_UNREAD_DATA;
    STATUS fileStatus = NU_SUCCESS;
    char tx_buf[512];
    char url_prefix[512];
    char url_prefix_fmt[40];;

    /* lets try to find the url prefix from the request uri
     * The request->uri looks something like "/ms/dld/dirname/" or "/ms/drv_b/dirname/"
     *
     * so we need to scan for "/" starting at index 4 
     */
    char *prefix_end = strchr(request->uri+strlen("/ms/"), '/');

    if (prefix_end)
    {
        strncpy(url_prefix_fmt, request->uri, prefix_end - request->uri);  /* "/ms/drv_b"     */
        sprintf(url_prefix_fmt+(prefix_end - request->uri), "%%s/");       /* "/ms/drv_b%%s/" */
    }
    else
        strcpy(url_prefix_fmt, "/ms/dld%s/");

    sprintf(url_prefix, url_prefix_fmt, path+2);
    replace_chars(url_prefix, '\\', '/');

    status = write_http_hdrs_unknown_payload_size(handle);
    if (NU_SUCCESS != status) 
        return status;

    status = HTTP_ERROR_UNSENT_DATA;

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "Write_Response_body.................");
   

    
    sprintf(tx_buf, "<html><head>"
                    "<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">"
                    "</head><body>%s<H1>Index of %s</H1><p/> <table><tr><th>Filename</th><th>Size</th><th>Attributes</th></tr>", 
                        extra_message == NULL ? "": extra_message,path);
    status = HTTP_Lite_Write_Response_Body(handle, tx_buf, strlen(tx_buf));

    if (status != NU_SUCCESS && status != HTTP_ERROR_UNSENT_DATA)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }
    
    /* now find every entry in the root dir */
    DSTAT statobj;
    char pattern[1024];
    sprintf(pattern, "%s\\*.*", path);

   /* A simple directory list subroutine. */
    if ((fileStatus = NU_Get_First(&statobj, pattern)) == NU_SUCCESS)
    {
        while(1)
        {
            char fileattributes[50];
            char dirlink[256];
            
            sprintf(fileattributes, "%s%s%s%s%s%s",
                    (statobj.fattribute & ARDONLY) ? "&lt;RO&gt;"  : "",
                    (statobj.fattribute & AHIDDEN) ? "&lt;HID&gt;" : "",
                    (statobj.fattribute & ASYSTEM) ? "&lt;SyS&gt;" : "",
                    (statobj.fattribute & AVOLUME) ? "&lt;VOL&gt;" : "",
                    (statobj.fattribute & ADIRENT) ? "&lt;DIR&gt;" : "",
                    (statobj.fattribute & ARCHIVE) ? "&lt;ARCHIVE&gt;" : "");

            if (statobj.fattribute & ADIRENT)
                sprintf(dirlink, "<a href=\"%s%s/\">%s</a>", url_prefix, statobj.lfname, statobj.lfname);
            else
                sprintf(dirlink, "<a href=\"%s%s\">%s</a> &nbsp;&nbsp;"
                                 "<a href=\"%s%s?delete=1\"><button type=\"button\"><i class=\"material-icons\" style=\"font-size:18px;color:red\">delete</i></button></a>", 
                                 url_prefix, statobj.lfname, statobj.lfname,        
                                 url_prefix, statobj.lfname);

            sprintf(tx_buf, "<tr><td>%s</td><td>%7u</td><td>%04x:%04x</td><td>%s</td></tr>", dirlink, (unsigned int)statobj.fsize, statobj.fcrdate,statobj.fcrtime, fileattributes);

            status = HTTP_Lite_Write_Response_Body(handle, tx_buf, strlen(tx_buf));

            if (status != NU_SUCCESS && status != HTTP_ERROR_UNSENT_DATA)
            {
                if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
                (void) NU_Done(&statobj);
                return status;
            }

            if ((fileStatus = NU_Get_Next(&statobj)) != NU_SUCCESS)
            {
                (void) NU_Done(&statobj);
                break;
            }
        }
    } 

    if (fileStatus != NU_SUCCESS)
        fnCheckFileSysCorruption(fileStatus);

    /* and write the page footer */
    sprintf(tx_buf, "</table></body></html>");
    status = HTTP_Lite_Write_Response_Body(handle, tx_buf, strlen(tx_buf));

    if (status != NU_SUCCESS && status != HTTP_ERROR_UNSENT_DATA)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }
    
    // indicate that we are done with tx_len == 0 */
    status = HTTP_Lite_Write_Response_Body(handle, NU_NULL, 0);

    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "   Error = %d (0x%X)\r\n", status, status);
        return status;
    }
    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "OK\r\n");

    return status;
}

/* ---------------------------------------------------------------------------------------------------[upload_file]-- */
STATUS upload_file(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data, char *path)
{
	INT fd = 0;
    STATUS status = NU_SUCCESS;

	if ((fd = NU_Open(path, (PO_RDONLY), PS_IREAD)) < 0)
	{
		dbgTrace(DBG_LVL_ERROR, "Can't Open file:%s\n",path);
		// todo: return error page
		fnCheckFileSysCorruption(status);
	}
	else
	{
	    do
        {
            int filesize = NU_Seek(fd, 0, PSEEK_END);
            int bytes_read;
            int tot_bytes_read = 0;
            char buffer[1024];

            fnCheckFileSysCorruption(filesize);
            status = tx_http_file_get_rsp_hdr(handle, filesize);
            if (NU_SUCCESS != status)
                break;

            fnCheckFileSysCorruption(NU_Seek(fd, 0, PSEEK_SET));  // move back to the beginning

            do
            {
                bytes_read = NU_Read(fd, buffer, sizeof(buffer));
                if (bytes_read > 0)
                {
                    tot_bytes_read += bytes_read;

                    /* Send file chunk. */
                    status = HTTP_Lite_Write_Response_Body(handle, buffer, bytes_read);
                }

            }while (bytes_read > 0 && status == HTTP_ERROR_UNSENT_DATA);

            if (bytes_read < 0)
            {
                dbgTrace(DBG_LVL_ERROR, "Error reading from file %s\n", path);
                fnCheckFileSysCorruption(bytes_read);
            }

        } while (0);  // just so that we can break out on error and close the file. Poormans Try-Finally.

        fnCheckFileSysCorruption(NU_Close(fd));
	}

    return status;
}

/* ---------------------------------------------------------------------------------------------------[delete_file]-- */
STATUS delete_file(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data, char *path)
{
    bool file_is_deleted = false;
    STATUS status;
    char result_msg[512];

	status = NU_Delete(path);
	if (status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Can't delete file:%s. status=%d\n",path, status);
		fnCheckFileSysCorruption(status);
	}

    file_is_deleted = (status == NU_SUCCESS);


    sprintf(result_msg, "<H1>%s %s</H1>", file_is_deleted ? "Deleted" : "Couldn't delete", path);

    // replace last "\" with null to get the parent directory path
    if (strrchr(path, '\\'))
      *strrchr(path, '\\') = '\0';

    return dir_info_page(handle, request, app_data, path, result_msg);

}

/* ----------------------------------------------------------------------------------------------------[delete_dir]-- */
STATUS delete_dir(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data, char *path)
{
    bool dir_is_deleted = false;
    STATUS status;
    char result_msg[512];

	status = NU_Remove_Dir(path);
	if (status != NU_SUCCESS)
	{
		dbgTrace(DBG_LVL_ERROR, "Can't delete directory:%s. status=%d\n",path, status);
		fnCheckFileSysCorruption(status);
	}

    dir_is_deleted = (status == NU_SUCCESS);


    sprintf(result_msg, "<H1>%s %s</H1>", dir_is_deleted ? "Deleted" : "Couldn't delete", path);

    // replace last "\" with null to get the parent directory path
    if (strrchr(path, '\\'))
      *strrchr(path, '\\') = '\0';

    return dir_info_page(handle, request, app_data, path, result_msg);

}

/* --------------------------------------------------------------------------------------------------------[is_dir]-- */
bool is_dir(char *path)
{
    UINT8 attr = 0;

    STATUS status = NU_Get_Attributes(&attr, path);
    
    if (status != NU_SUCCESS)
    {
        fnCheckFileSysCorruption(status);
    }

    return (attr & ADIRENT) == ADIRENT;
}

/* -------------------------------------------------------------------------------------------------------[is_file]-- */
bool is_file(char *path)
{
    UINT8 attr;

    STATUS status = NU_Get_Attributes(&attr, path);

    if (status != NU_SUCCESS)
    {
        fnCheckFileSysCorruption(status);
    }

    return status == NU_SUCCESS && ( (attr & ADIRENT) != ADIRENT);
}

/* --------------------------------------------------------------------------------------------[csd_sr_get_handler]-- */
STATUS csd_sr_get_handler(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
    STATUS status = HTTP_ERROR_UNREAD_DATA;
    UINT32 rx_len;
    CHAR   buffer[300];
    INT    tx_len;
    char  *rspFmt = "<html><body><H1>csd_sr_get_handler</H1><p>Unknown file %s</p></body></html>";
    char  fname[EMAXPATH] = {'A',':', 0};
    #define DLD_URI "/ms/dld/"
    #define DRV_A_URI "/ms/drv_a/"
    #define DRV_B_URI "/ms/drv_b/"
    size_t uri_fname_start;
    bool   delete_request = false;

    if (!connection_allowed(handle, request, app_data))
        return HTTP_FORCE_CLOSE;

    display_request_info(" csd_sr_get_handler ", handle, request, app_data);

    rx_len = sizeof(buffer);
    read_request_body(handle, buffer, &rx_len);

    if (strncmp(  DLD_URI, request->uri, strlen(  DLD_URI)) == 0)
    {
        fname[0] = 'A';
        uri_fname_start = strlen(DLD_URI)-1;
    }
    else if (strncmp(DRV_A_URI, request->uri, strlen(DRV_A_URI)) == 0)
    {
        fname[0] = 'A';
        uri_fname_start = strlen(DRV_A_URI)-1;
    }
    else if (strncmp(DRV_B_URI, request->uri, strlen(DRV_B_URI)) == 0)
    {
        fname[0] = 'B';
        uri_fname_start = strlen(DRV_B_URI)-1;
    }


    if (strcmp(  DLD_URI, request->uri) == 0    ||
        strcmp(DRV_A_URI, request->uri) == 0    ||
        strcmp(DRV_B_URI, request->uri) == 0       )
    {
        return dir_info_page(handle, request, app_data, fname, NULL);
    }



    strncpy(fname+2, request->uri + uri_fname_start, sizeof(fname)-2);
    fname[sizeof(fname)-1]='\0'; /* make sure it is null terminated */
    // remove parameters from uri
    replace_chars(fname, '?', '\0');
    /* and fix path separators */
    replace_chars(fname, '/', '\\');

    // remove trailing slash
    if (fname[strlen(fname)-1] == '\\')
        fname[strlen(fname)-1] = '\0';

    {
        /* check if we are being asked to delete the file/directory */
        char *del_val;
        if (NU_SUCCESS == HTTP_LITE_HDR_Get_Key(request->params, "delete", 0, &del_val) && *del_val == '1')
            delete_request = true;
    }

    if (is_dir(fname))
    {
        if (delete_request)
            return delete_dir(handle, request, app_data, fname);
        else
            return dir_info_page(handle, request, app_data, fname, NULL);
    }

    if (is_file(fname))
    {
        if (delete_request)
            return delete_file(handle, request, app_data, fname);
        else
            return upload_file(handle, request, app_data, fname);
    }

    //if (strcmp(fname, "/"              ) == 0) return         csd_sr_root_index(handle, request, app_data);

    sprintf(buffer, rspFmt, fname);
    tx_len = strlen(buffer);
    if (verbosity > 1)
    {
        dbgTrace(DBG_LVL_INFO, "              tx_len=%d\r\n", tx_len);
        dbgTrace(DBG_LVL_INFO, "Write_Response_header...............");
    }
    status = HTTP_Lite_Write_Response_Header(handle, 200, "All OK", tx_len, NU_NULL);
    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "OK\r\n");

    status = HTTP_ERROR_UNSENT_DATA;

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "Write_Response_body.................");
    
    /* Send message body. */
    status = HTTP_Lite_Write_Response_Body(handle, buffer, tx_len);

    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }
    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "OK\r\n");

    return status;
}


INT fsWriteFile(char *path, char * filename, uint8_t* filedata, int32_t fileLength);
#define MAX_FILE_SIZE 0x800000

/* ------------------------------------------------------------------------------------------[remove_boundary_tag]-- */
void remove_boundary_tag(uint8_t *file_data, uint32_t file_len, UINT32 *boundary_length, UINT32 *boundary_start_offset)
{
    const int MAX_BOUNDARY_DASH_LENGTH = 2;
    const int MAX_BOUNDARY_DEPTH = 768;
    const int MAX_CONTENT_LENGTH_DEPTH = 96;
	char *ptr;
	char *ptr2;
	char boundary[50] = {0};
	char content_length[12] = {0};
	char dash[3] = "--";
	uint32_t boundary_end_offset = 0;
	int max_boundary_size = sizeof(boundary) - MAX_BOUNDARY_DASH_LENGTH - MAX_BOUNDARY_DASH_LENGTH - 1;

	//find the start and end of the file data using the boundary tag
	//boundary=a57c760d-a334-47ac-9a99-691cc3029994
	//--a57c760d-a334-47ac-9a99-691cc3029994
	//Content-Length: 1670046
	//  FILE DATA IS BETWEEN
	//--a57c760d-a334-47ac-9a99-691cc3029994--

	*boundary_length = 0;
	*boundary_start_offset = 0;

	if (file_data != NU_NULL)
	{
		ptr2 = (char *)file_data;
		//look for the boundary tag prefix at only the beginning of the file
		ptr = (char *)mystrnstr(ptr2, dash, MAX_BOUNDARY_DEPTH);
		if((ptr != NU_NULL) && ((ptr - ptr2) < MAX_BOUNDARY_DEPTH))
		{
			ptr2 = strchr(ptr, '\r');
			if(ptr2 == NU_NULL)
				ptr2 = strchr(ptr, '\n');
			//if the size of the boundary tag is less than the buffer
			if((ptr2 != NU_NULL) && ((int)(ptr2 - ptr) < max_boundary_size))
			{
				//add the beginning dashes
				strcpy(boundary, dash);
				//add the boundary tag for checking the end boundary tag
				strncpy(boundary, ptr, (int)(ptr2 - ptr));
				//add the end dashes for the end boundary compare
				strcat(boundary, dash);

				//look for the content length on the next line after the boundary tag
				ptr = (char *)mystrnstr(ptr2, "Content-Length:", MAX_CONTENT_LENGTH_DEPTH);
				if((ptr != NU_NULL) && ((int)(ptr - ptr2) < MAX_CONTENT_LENGTH_DEPTH))
				{
					ptr = (ptr + strlen("Content-Length:"));
					while(*ptr == ' ')
						ptr++;
					ptr2 = strchr(ptr, '\r');
					//if the content length is less than the buffer
					if((ptr2 != NU_NULL) && ((int)(ptr2 - ptr) < sizeof(content_length)))
					{
						strncpy(content_length, ptr, (int)(ptr2 - ptr));
						//if the ascii is really a number
						if (!parse_uint32(content_length, boundary_length))
						{
							if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error parsing Content-Length: [%s]\r\n", content_length);
							return;
						}
						else
						{
							if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Content-Length: [%lu]\r\n", *boundary_length);
						}
						//if the boundary length is less than the whole file length
						if (*boundary_length < file_len)
						{
							//go forward after leading CRLF CRLF
							while((*ptr2 == '\r') || (*ptr2 == '\n'))
								ptr2++;

							//this is the real start of the file data
							ptr = ptr2;
							*boundary_start_offset = (ptr - (char *)file_data);
							if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "Content boundary start offset %lu", *boundary_start_offset );

							//Content length says this is the end of the file data
							ptr2 = (ptr + *boundary_length); //end of data
							//locate the end boundary
							ptr = (char *)mystrnstr(ptr2, boundary, sizeof(boundary));
							//if the boundary end tag is very close to the end of the file
							if((ptr != NU_NULL) && ((int)(ptr - ptr2) < sizeof(boundary)))
							{
								//it is safe to set the boundary end offset
								boundary_end_offset = (ptr2 - (char *)file_data);
							}
							if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "Content boundary end offset %lu", boundary_end_offset );
						}
					}
				}
			}
		}
	}
}

#define BL_IMG_ID_v1    0x3176474d495f4c42UL  // "BL_IMGv1" as a uint64_t
#define BL_IMG_ID_v2    0x3276474d495f4c42UL  // "BL_IMGv2" as a uint64_t

#define APP_IMG_ID_v1   0x3176474d49505041UL  // "APPIMGv1" as a uint64_t
#define APP_IMG_ID_v2   0x3276474d49505041UL  // "APPIMGv2" as a uint64_t

#define TST_IMG_ID_v2   0x3276474d49545354UL  // "TSTIMGv2" as a uint64_t

typedef union
{
    uint64_t id;
    // other structures in this union removed 
} img_hdr_t;

bool is_hdr_v2(img_hdr_t *hdr)
{
    return  hdr->id == BL_IMG_ID_v2 || hdr->id == APP_IMG_ID_v2 || hdr->id == TST_IMG_ID_v2;
}

bool image_file_received = false;

/* ------------------------------------------------------------------------------------------[csd_boot_img_handler]-- */
STATUS csd_sr_put_handler(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
    STATUS status = HTTP_ERROR_UNREAD_DATA;
    UINT32 rx_len;
    UINT32 tot_rx_len = 0;
    UINT32 boundary_length = 0;
    UINT32 boundary_start_offset = 0;
    UINT32 processed = 0;
	UINT32 reported_processed = 0;
    CHAR   buffer[1024];
    INT    tx_len;
    char  *fname;
    char *hdr_val;
    uint32_t file_len = 0;
    uint8_t  *file_data = NU_NULL;
    DATETIME startDateTime;
    DATETIME endDateTime;
    long     timeDiffInSecs;

    if (!connection_allowed(handle, request, app_data))
        return HTTP_FORCE_CLOSE;

    display_request_info(" csd_sr_put_handler ", handle, request, app_data);

    /* make sure this is a PUT request */
    //if (stricmp("PUT", request->method) != 0)
    
    if (NU_SUCCESS == HTTP_LITE_HDR_Get_Key(request->headers, "Content-Length", 14, &hdr_val))
    {
        NU_MEMORY_POOL *mem_pool;

        if (!parse_uint32(hdr_val, &file_len))
        {
            if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error parsing content length [%s]\r\n", hdr_val);
            return HTTP_ERROR_UNREAD_DATA;
        }

        if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "allocating %lu bytes\r\n", file_len );

        /* make sure file length is not too big */
        if (file_len > MAX_FILE_SIZE)
        {
            return HTTP_ERROR_UNREAD_DATA;
        }

        /* try to allocate a buffer where the file contents can be stored during download */

        status = NU_System_Memory_Get(&mem_pool, NU_NULL);
        if (status != NU_SUCCESS)
        {
            if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error retrieving system memory pool\r\n");
            return status;
        }
        status = NU_Allocate_Memory(mem_pool, (void **)&file_data, file_len + 1024, NU_NO_SUSPEND);
        if (status != NU_SUCCESS)
        {
            if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error allocating %lu bytes\r\n", file_len);
            return status;
        }

    }
    if (NU_SUCCESS == HTTP_LITE_HDR_Get_Key(request->headers, "Expect", 7, &hdr_val))
    {
        if (strcmp(hdr_val, "100-continue") == 0)
        {
        	if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "sending continuation header\r\n");
            /* we must send a continue header */
            status = HTTP_Lite_Write_Response_Header(handle, 100, "All OK", 0, NU_NULL);
            if (status != NU_SUCCESS)
            {
                if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error sending continuation header = %d (0x%X)\r\n", status, status);
                if (file_data != NU_NULL) NU_Deallocate_Memory(file_data);

                return status;
            }
        }
    }

    fname = request->uri + strlen("/ms/dld") + 1;
    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "filename %s\r\n", fname);

	//get the system date time
	GetSysDateTime(&startDateTime, ADDOFFSETS, GREGORIAN);

    /* read first chunk so that we can determine the full length */
    rx_len = sizeof(buffer);
    status = HTTP_Lite_Read_Request_Body(handle, (char *)file_data, &rx_len);

    tot_rx_len = rx_len;
    /* slurp reset of body */
    while (status == HTTP_ERROR_UNREAD_DATA)
    {
        rx_len = sizeof(buffer);
        status = HTTP_Lite_Read_Request_Body(handle, (char *)(file_data+tot_rx_len), &rx_len);
        tot_rx_len += rx_len;
        processed = (UINT32)((float)((float)tot_rx_len / (float)file_len) * (float)10);
        if (processed > reported_processed )
        {
        	reported_processed = processed;
            if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "received %lu bytes, %d", tot_rx_len,  (reported_processed * 10));
        }
    }

    //if the put contains a boundary tag, remove it from the file data
    remove_boundary_tag(file_data, tot_rx_len, &boundary_length, &boundary_start_offset);
    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "boundary_length  %lu, boundary_start_offset %lu", boundary_length, boundary_start_offset );

	//get the system date time
	GetSysDateTime(&endDateTime, ADDOFFSETS, GREGORIAN);

	//calculate difference between two date times
	CalcDateTimeDifference(&startDateTime, &endDateTime, &timeDiffInSecs);
    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "download took  %lu seconds", timeDiffInSecs );

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "total length %lu bytes\r\n", tot_rx_len );

    ///////////////////////////////////////////////////////////
    /* read first chunk so that we can determine the full length */
//    rx_len = sizeof(buffer);
//    status = HTTP_Lite_Read_Request_Body(handle, file_data, &rx_len);
//    if(status == HTTP_ERROR_UNREAD_DATA)
//    {
//    	status2 = fnFileAppend("dld", fname, file_data, rx_len);
//    }
//    tot_rx_len = rx_len;
//    /* slurp reset of body */
//    while (status == HTTP_ERROR_UNREAD_DATA)
//    {
//        rx_len = sizeof(buffer);
//        status = HTTP_Lite_Read_Request_Body(handle, file_data, &rx_len);
//        if(status == HTTP_ERROR_UNREAD_DATA)
//		{
//			status2 = fnFileAppend("dld", fname, file_data, rx_len);
//		}
//
//        tot_rx_len += rx_len;
//    }

    //////////////////////////////////////////////////////////////

    tx_len = 0;
    if (verbosity > 1)
    {
        dbgTrace(DBG_LVL_ERROR, "          tot_rx_len=%d\r\n", tot_rx_len);
    }

    /* and now write the data to flash */


    //TODO
//    //if(verify_file())
//    {
//
//    }
//    else
//    	sprintf(buffer, "Failed to verify file.");

    if(file_len == tot_rx_len)
    {
    	char *path = NULL;
    	char *ptr = strrchr(fname,'/');
    	bool valid_img_hdr = false;

    	if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "file length equals total length\r\n");
    	if(ptr != NULL)
    	{
    		*ptr = 0;
    		path = fname;
    		fname = (ptr+1);
    	}

    	//get the system date time
    	GetSysDateTime(&startDateTime, ADDOFFSETS, GREGORIAN);

		if ((boundary_length != 0) && (tot_rx_len > boundary_length))
			tot_rx_len = boundary_length;
		status = fsWriteFile(path, fname, (file_data + boundary_start_offset), tot_rx_len);

        img_hdr_t img_hdr;
        memcpy(&img_hdr, (file_data + boundary_start_offset), sizeof(img_hdr_t));
        valid_img_hdr = is_hdr_v2(&img_hdr);

		//get the system date time
		GetSysDateTime(&endDateTime, ADDOFFSETS, GREGORIAN);

		//calculate difference between two date times
		CalcDateTimeDifference(&startDateTime, &endDateTime, &timeDiffInSecs);
	    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "flash write took  %lu seconds", timeDiffInSecs );

		if(status < 0)
		{
			sprintf(buffer, "Failed to write file to flash %d", status);
			dbgTrace(DBG_LVL_ERROR, "Write file failed %s  Reason %d\r\n", fname, status);
		}
		else if(status != tot_rx_len)
		{
			sprintf(buffer, "Failed to write file to flash %u / %u", status, (unsigned int)tot_rx_len);
			dbgTrace(DBG_LVL_ERROR, "Write file failed %s  Total length %0x, Data written length %0x\r\n", fname, tot_rx_len, status);
		}
		else
		{
			/* check if the downloaded file had a valid image header */
			if(valid_img_hdr)
            {
                image_file_received = true;     // on_InstallUpdatesReq uses this flag */
            }

			sprintf(buffer, "Successfully wrote file to flash Status: %u, Size: %u img_file_received:%d", status, (unsigned int)tot_rx_len, image_file_received);
			dbgTrace(DBG_LVL_ERROR, "Write file Success %s  tot_rx_len:%u img_file_received:%d\r\n", fname, tot_rx_len, image_file_received);
		}
    }
    else
    {
    	sprintf(buffer, "Failed to download entire file %u/ %u", tot_rx_len , file_len);
    	dbgTrace(DBG_LVL_ERROR, "Write file failed %s  Total length %0x, Data written length %0x\r\n", fname, tot_rx_len, status);
    }

    /* free allocated memory */
    if (file_data != NU_NULL) NU_Deallocate_Memory(file_data);

    if (verbosity > 1)
    {
        dbgTrace(DBG_LVL_ERROR, "Write_Response_header...............");
    }
    tx_len = strlen(buffer);
    status = HTTP_Lite_Write_Response_Header(handle, 200, "All OK", tx_len, NU_NULL);
    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }

    if (verbosity > 1) dbgTrace(DBG_LVL_ERROR, "OK\r\n");
    if (verbosity > 1) dbgTrace(DBG_LVL_ERROR, "Write_Response_body.................");
    
    /* Send message body. */
    status = HTTP_Lite_Write_Response_Body(handle, buffer, tx_len);

    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }
    if (verbosity > 1) dbgTrace(DBG_LVL_ERROR, "OK\r\n");

    return status;
}


/* ----------------------------------------------------------------------------------------[csd_system_graphics_handler]-- */
STATUS csd_system_graphics_handler(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
    STATUS status = HTTP_ERROR_UNREAD_DATA;
    INT tx_len = 0;
    uint32_t tick_start = GetSystemTime();
    uint32_t tick_end;
    char *pTemp = 0;
    char *fname;
    char fileName[65];
	char *pPng = NULL;

    memset(fileName, 0, sizeof(fileName));

    fname = request->uri + strlen("/ms/graphics/");

    pTemp = strstr(fname, ".png");

    tx_len = pTemp - fname;
    strncpy(fileName, fname, tx_len);

	status = BuildPngFile(fileName, (unsigned char **)&pPng, (size_t **)&tx_len);
	if(status != NU_SUCCESS)
	{
		if(pPng != NULL)
			free(pPng);
		
		status = HTTP_Lite_Write_Response_Header(handle, 404, "File Not Found", 0, NU_NULL);
		if (status != NU_SUCCESS)
		{
			if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
			return status;
		}
		return status; 
	}

    if (NU_SUCCESS != (status = tx_http_file_get_rsp_hdr(handle, tx_len)))
        return status;

    status = HTTP_ERROR_UNSENT_DATA;

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "Write_Response_body.................");
    
    while (status == HTTP_ERROR_UNSENT_DATA)
    {
        /* Send message body. */
        status = HTTP_Lite_Write_Response_Body(handle, pPng, tx_len);
    }

	if(pPng != NULL)
		free(pPng);

    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }
    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "OK\r\n");

    if (status == NU_SUCCESS && verbosity > 1)
    {
    	tick_end = GetSystemTime();
    	dbgTrace(DBG_LVL_INFO, "  duration: %u(ms)", (unsigned int) (tick_end - tick_start));
    }
    return status;
}


/* ----------------------------------------------------------------------------------------------[csd_graphics_handler]-- */
STATUS csd_graphics_handler(INT handle, HTTP_SVR_REQUEST *request, VOID *app_data)
{
    STATUS status = HTTP_ERROR_UNREAD_DATA;
    UINT32 rx_len;
    CHAR   buffer[300];
    INT    tx_len;
    char  *rspFmt = "<html><body><H1>csd_graphics_handler</H1><p>Unknown file %s</p></body></html>";
    char  *fname;

    if (!connection_allowed(handle, request, app_data))
        return HTTP_FORCE_CLOSE;

    display_request_info(" graphics_handler ", handle, request, app_data);

    rx_len = sizeof(buffer);
    read_request_body(handle, buffer, &rx_len);

    fname = request->uri + strlen("/ms/graphics/");

    if(strstr(fname, ".png") ) return csd_system_graphics_handler(handle, request, app_data);

    sprintf(buffer, rspFmt, fname);
    tx_len = strlen(buffer);
    if (verbosity > 1)
    {
        dbgTrace(DBG_LVL_INFO, "              tx_len=%d\r\n", tx_len);
        dbgTrace(DBG_LVL_INFO, "Write_Response_header...............");
    }
    status = HTTP_Lite_Write_Response_Header(handle, 200, "All OK", tx_len, NU_NULL);
    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "OK\r\n");

    status = HTTP_ERROR_UNSENT_DATA;

    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "Write_Response_body.................");
    
    /* Send message body. */
    status = HTTP_Lite_Write_Response_Body(handle, buffer, tx_len);

    if (status != NU_SUCCESS)
    {
        if (verbosity > 0) dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return status;
    }
    if (verbosity > 1) dbgTrace(DBG_LVL_INFO, "OK\r\n");

    return status;
}


/* ----------------------------------------------------------------------------------------[log_handler_initialize]-- */
void log_handler_initialize()
{
    HTTP_LITE_WEBAPP appobj;
    STATUS           status;
 
    memset(&appobj, 0, sizeof(HTTP_LITE_WEBAPP));
    strcpy(appobj.path_prefix, "/ms");
    appobj.port[0] = 443;
#ifdef USE_HTTP_URI
    appobj.port[1] = 80;
#endif

    appobj.handlers[0].callback = csd_logs_handler;
    appobj.handlers[0].path="/logs/*";
    appobj.handlers[0].method = HTTP_METHOD_GET;

    appobj.handlers[1].callback = csd_sr_put_handler;
    appobj.handlers[1].path="/dld/*";
    appobj.handlers[1].method = HTTP_METHOD_PUT;

    appobj.handlers[2].callback = csd_sr_get_handler;
    appobj.handlers[2].path="/dld/*";
    appobj.handlers[2].method = HTTP_METHOD_GET;

    appobj.handlers[3].callback = csd_graphics_handler;
    appobj.handlers[3].path="/graphics/*";
    appobj.handlers[3].method = HTTP_METHOD_GET;

    appobj.handlers[4].callback = csd_sr_get_handler;
    appobj.handlers[4].path="/drv_a/*";
    appobj.handlers[4].method = HTTP_METHOD_GET;

    appobj.handlers[5].callback = csd_sr_get_handler;
    appobj.handlers[5].path="/drv_b/*";
    appobj.handlers[5].method = HTTP_METHOD_GET;

    /* Register the web-app. */
    dbgTrace(DBG_LVL_INFO, "HTTP_Lite_Register_App..................");
    status = HTTP_Lite_Register_App(&appobj);

    if (status != NU_SUCCESS)
    {
        dbgTrace(DBG_LVL_ERROR, "  Error = %d (0x%X)\r\n", status, status);
        return;
    }
    else
    {
        dbgTrace(DBG_LVL_INFO, "OK\r\n");
    }
}

/* -----------------------------------------------------------------------------------------------------------[eof]-- */

