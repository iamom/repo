/*******************************************************************************
 PROJECT    :   Future Phoenix
 COPYRIGHT  :   2005, Pitney Bowes, Inc.  
 AUTHOR     :   Joe Mozdzer
 MODULE     :   OICMENUCONTROL.C
  
 DESCRIPTION:
      This file contains all types of menu controls interface 
      functions (hard key, event, field init and field refresh) 
      to be shared across all projects.

 ----------------------------------------------------------------------
 MODIFICATION HISTORY:

 08-May-18 sa002pe on FPHX 02.12 shelton branch
 	Commented out fnfGlobalText.

 22-May-17 Jennifer Hao on FPHX 02.12 cienet branch
          Commented out  functions  fngBufferEmptyGraphSelect, fngBufferPartialGraphSelect, fngOOBGraphSelect, fngSMenuDownIndicator,
          fngSMenuUpIndicator,fngSMenuThumbnailSlot1~3, fnfSMenuDownIndicator, fnfSMenuUpIndicator,  fnfSMenuIndexMatchCaption,
          fnhkSMenuScrollDown,fnhkSMenuScrollUp, fnhkSMenuPageDown,  fnhkSMenuPageUp, fnhkSMenuProcessDigit,
          fnhkSMenuProcessDigits,fnhkSMenuProcessSoft5, fnhkSMenuAdSelectForDelete, fnhkSMenuInscSelectForDelete,fnhkSMenuProcessAdDelSoft1~3 ,
          fnhkSMenuProcessInscDelSoft1~5 ,fnSMenuPrepCompSelect,fnIsValidCCAlphaChar, fnIsValidCCNumChar, because they aren't used by G9.  
          
    04/09/2013  Bob Li         Fixed fraca 221100 by modifying fnResortMenuByEntryBuffer() to sort the acctList by name.
    09/21/2012  Liu Jupeng     Modified the function fnAdjustDisplayPageByMatchedName()and
                               fnCheckMatchedSpeedCode() for fixing the fraca 146599 with the 
                               second solution as Janus'solution.
    09/21/2012  Bob Li		   Added new function fnSMenuNumberOrLabelSlot() and fnfSMenuNumOrLabelSlot1-4()
								for fixing fraca 118645. At the fee detail screen, display the fee number 1,2,. or "Value"
								at the left side.
    09/10/2012  Liu Jupeng     Modified the function fnAdjustDisplayPageByMatchedName() for fixing 
                               the fraca 146599.
    04/23/2010  Jingwei,Li     Modified fnSMenuLabelSlot() to fix fraca.
    04/21/2010  Jingwei,LI     Modified fnSMenuLabelSlot(),fnSMenuValueSlotInit() and
                               fnSMenuValueSlotRefresh() for RTL display.
    07/10/2009  Raymond Shen    Added fngSMenuBottomLine() to displays a bottom line on the screen
                                if current screen has multiple pages.
    10/29/2008  Raymond Shen    Added function fnInsertMenuSlot().
 2008.06.09 Clarisa Bellamy     FPHX 1.11
  Modifications for Graphics Module Update:
  - Replaced extern data declarations with static data declarations.  They were
    removed from oifpimage.c, where they are no longer used, at all.  (They may 
    soon be removed from here, as well.) 
  - Added (conditionally compiled) function fnSMenuPrepCompSelect.  This is not 
    used on FPHX because we have the sorted menus, without thumbnails.  But this
    keeps it easier to compare with the comt_tips.

    03/20/2008  Raymond Shen    Added function fnSMenuGetSlotIndexOfSlotFn().
   10/15/2007   Raymond Shen    Added code that updates LEDs in function 
                                fnAdjustDisplayPageByMatchedName() to fix fraca 131233.
    04/19/2007  Andy Mo       Added fnGetMenuEntriesTotalNumber()to get current menu items.
    08/17/2006  Kan Jiang     Modified function fnCheckMatchedSpeedCode() for fixing fraca 102101
    07/27/2006  Vincent Yi    Modified a few functions to fix address alignment issue
    12/07/2005  Adam Liu      Merged with Mega Rearch code

 ----------------------------------------------------------------------
   OLD PVCS REVISION HISTORY

*    Rev 1.16   22 Aug 2005 13:52:28   NEX3RPR
* Changed utility function fnSetupMenuSlots, Janus does not need to setup 
* wDisplayNumber defined in pSMenuTable. -David
* 
*******************************************************************************/


/**********************************************************************
        Include Header Files
**********************************************************************/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "aiservic.h"
#include "bob.h"
#include "bobutils.h"
#include "bwrapper.h"
#include "cmos.h"
#include "datdict.h"
#include "dcapi.h"
//#include "DeptAccount.h"
#include "features.h"
#include "fwrapper.h"
#include "cwrapper.h"
#include "keypad.h"
#include "mmcitask.h"
#include "oicmenucontrol.h"
#include "oicommon.h"
#include "oiclock.h"
#include "oifields.h"
#include "oientry.h"
#include "oifunctfp.h"
//#include "oirating.h"
//#include "oirateservice.h"
//#include "oireport.h"
#include "oit.h"
//#include "oiweight.h"
#include "rateadpt.h"
//#include "RatePublic.h"
//#include "ratesvcpublic.h"
//#include "RateTask.h"
#include "report1.h"
#include "scheduler.h"
#include "sys.h"
#include "flashutil.h"
#include "utils.h"



/********************************************************************
               Local functions prototypes 
********************************************************************/
static BOOL fnSMenuNumberOrLabelSlot(UINT16 *pFieldDest, 
                                    UINT8   bLen, 
                                    UINT8   bFieldCtrl, 
                                    UINT8   bNumTableItems, 
                                    UINT8   *pTableTextIDs,
                                    UINT8   *pAttributes,
                                    UINT16  usSlotIndex);

static BOOL   fnSMenuNumber (UINT16    *pFieldDest, 
                             UINT8      bLen, 
                             UINT16     usSlotIndex);

static BOOL   fnCallNestedFieldFunction (UINT16 *pFieldDest, 
                                         UINT8 bFunctionNum, 
                                         BOOL fCallInit, 
                                         UINT8 bParentLen,
                                         UINT8 *pAttributes);

static BOOL   fnSMenuLabelSlot(UINT16 *pFieldDest,
                               UINT8 bLen,
                               UINT8 bFieldCtrl,
                               UINT8 bNumTableItems,
                               UINT8  *pTableTextIDs, 
                               UINT16 usIndex, 
                               UINT8 ucStrNum);

static BOOL   fnSMenuValueSlotRefresh(UINT16 *pFieldDest, 
                                      UINT8 bLen, 
                                      UINT8  bFieldCtrl,
                                      UINT8 bNumTableItems,
                                      UINT8 *pTableTextIDs,
                                      UINT8 *pAttributes,
                                      UINT16 usIndex);

static BOOL   fnSMenuValueSlotInit(UINT16 *pFieldDest, 
                                   UINT8 bLen, 
                                   UINT8  bFieldCtrl,
                                   UINT8 bNumTableItems, 
                                   UINT8 *pTableTextIDs, 
                                   UINT8 *pAttributes,
                                   UINT16 usIndex);

static BOOL   fnSoftKeyIndicator(UINT16 *pFieldDest, 
                                 UINT8 bLen, 
                                 UINT8  bFieldCtrl,
                                 UINT8 bNumTableItems, 
                                 UINT8 *pTableTextIDs, 
                                 UINT16 usIndex);

static BOOL fnResortMenuByEntryBuffer(void);
static UINT8 * fnCheckMatchedSpeedCode(const UNICHAR * pInputCode, 
                                             UINT8   ucStrLen);
static UINT8 * fnCheckMatchedName(const UNICHAR * pInputName);
static BOOL fnAdjustDisplayPageByMatchedName (UINT8 * pName);

/**************************************************************************
              Local defines, variables definition and declaration
**************************************************************************/

#define MENU_SLOT_NUMBER_LEN    3

// Local variables
S_MENU_TABLE    pSMenuTable;

static UNICHAR  pUniTempString[MAX_FIELD_LEN+1];

static UINT16   usMenuTotalEntries;

// Following two arrays are too big to be allocated from memory pool dynamicly.
static S_SORT_INFO      pstListSortedByNames[MAX_SMENU_SLOTS];
static S_SORT_INFO      pstListSortedByCodes[MAX_SMENU_SLOTS];
static pRefillMenuFn    pfnRefillSMenuTable = NULL;  

static unsigned short      wAd2DeleteId = 0;        // temporary containers for the ad or insc ID that */
static unsigned short      wAd2DelMenuIdx = 0;      // the menu slot index of the selected Ad to for deletion */
static unsigned short      wInsc2DeleteId = 0;      // has been selected for deletion but not yet deleted. */
static unsigned short      wInsc2DelMenuIdx = 0;    // the menu slot index of the selected Insc for deletion */


//------------------------------
//  Externs 
//------------------------------

extern  pInitFn    aInitFns[];
extern  pRefreshFn aRefreshFns[];   

extern  UINT8           *pNumKeyCodes;  /* contains scan codes for number keys from 0-9 */
extern  UINT8           *aQwertyGroup;       



/* *************************************************************************
// FUNCTION NAME: 
//      fngSMenuScrollBar
//
// DESCRIPTION: 
//      Variable graphic function that selects a graphic to show scroll bar
//      based on the size of menu and the position of current page in menu.
//
// INPUTS:
//      Standard graphic function inputs.
//
// OUTPUTS:
//      pSymID points to the buffer where stores the displayed Unicode graphics.
//
// RETURNS:
//      always returns successful
//
// MODIFICATION HISTORY:
//  10/17/2005  Vincent Yi      Initial Version
//
// *************************************************************************/
BOOL fngSMenuScrollBar (VARIABLE_GRAPHIC   *pVarGraphic, 
                        UINT16             *pSymID, 
                        UINT8               bNumSymListItems,                       
                        UINT8              *pSymList,
                        UINT8              *pAttributes)
{
    UINT8   bIndex = 0;
    SINT32  lPercentEntriesScrolled;
    UINT16  usScrolledEntries;
    UINT16  usTotalPages;
    UINT16  usCurPageNo;

    //  Calculate the total pages of the menu
    usTotalPages = usMenuTotalEntries / pSMenuTable.bEntriesPerPage;
    if ((usMenuTotalEntries % pSMenuTable.bEntriesPerPage) != 0)
    {
        usTotalPages++;
    }

    // Calculate the current page number
    usCurPageNo = pSMenuTable.usFirstDisplayedSlotIndex / 
                                        pSMenuTable.bEntriesPerPage;

    // Get the index in the scroll bar image list by the total pages and 
    // current page number
    if ((usTotalPages > 0) && (usTotalPages <= SCROLLBAR_MAX_PAGE))
    {
        UINT16 usPages = usTotalPages-1;
        bIndex = usCurPageNo;

        while (usPages>0)
        {
            bIndex += usPages;
            usPages--;
        }
    }
    else if (usTotalPages > SCROLLBAR_MAX_PAGE)
    {
        UINT16 usPages = SCROLLBAR_MAX_PAGE-1;

        // Calculate the percentage of the scrolled entries
        usScrolledEntries = pSMenuTable.usFirstDisplayedSlotIndex + 
                                                pSMenuTable.bEntriesPerPage;
        if (usScrolledEntries > usMenuTotalEntries)
        {
            usScrolledEntries = usMenuTotalEntries;
        }
        
        lPercentEntriesScrolled = ((UINT32)(usScrolledEntries) * 95 ) / 
                                                        usMenuTotalEntries;
        if (lPercentEntriesScrolled >= 19)
        {
            lPercentEntriesScrolled = 50 + (lPercentEntriesScrolled-50) * 90/100;
        }

        bIndex = lPercentEntriesScrolled * SCROLLBAR_MAX_PAGE / 100;
        if (bIndex >= SCROLLBAR_MAX_PAGE)
        {
            bIndex = SCROLLBAR_MAX_PAGE - 1;
        }

        while (usPages>0)
        {
            bIndex += usPages;
            usPages--;
        }
    }

    pVarGraphic->fUsed = FALSE;
    *pSymID = (UINT16) 0x0000;

    if (bNumSymListItems > bIndex)
    {
    	EndianAwareCopy((UINT8 *)pSymID, pSymList + (2 * bIndex), sizeof(UINT16));
    }
    
    return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fngSMenuBottomLine
//
// DESCRIPTION: 
//      Variable graphic function that displays the graphic line if there are
//      multi-pages in the menu.
//
// INPUTS:
//      Standard graphic function inputs.
//
// RETURNS:
//      always returns successful
//
// MODIFICATION HISTORY:
//  07/09/2009  Raymond Shen     Initial Version
//
// *************************************************************************/
BOOL fngSMenuBottomLine (VARIABLE_GRAPHIC   *pVarGraphic, 
                        UINT16             *pSymID, 
                        UINT8               bNumSymListItems,                       
                        UINT8              *pSymList,
                        UINT8              *pAttributes)
{
    pVarGraphic->fUsed = FALSE;
    *pSymID = (UINT16) 0x0000;

    /* if it is within the array bounds, and the slot is being used(bKeyAssignment not 0), 
      then display the bottom line. */
    if ( pSMenuTable.pSlot[pSMenuTable.bEntriesPerPage].bKeyAssignment != 0 )
    {
        if(bNumSymListItems > 0)
        {
        	EndianAwareCopy((UINT8 *)pSymID, pSymList, sizeof(UINT16));
        }
    }

    return (SUCCESSFUL);
}


/*--------------------------*/
/*      Field Functions     */
/*--------------------------*/



/* *************************************************************************
// FUNCTION NAME: fnfSMenuLabelSlot1-7
//
// DESCRIPTION:
//      Output field function that displays the menu label string 
//      that belongs in the given scrollable menu slot.  This can vary because some
//      menu options may not be supported depending on PCN parameters
//      or the current state of the system.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      always returns successful
//
// WARNINGS/NOTES:  
//      The menu label strings in their order of preference.
//
// MODIFICATION HISTORY:
//      Mozdzer   Initial Version
// *************************************************************************/
BOOL fnfSMenuLabelSlot1 (UINT16 *pFieldDest, 
                         UINT8   bLen, 
                         UINT8   bFieldCtrl,
                         UINT8   bNumTableItems, 
                         UINT8  *pTableTextIDs, 
                         UINT8  *pAttributes)
{
    UINT8   ucStrNum = 0;

    return  fnSMenuLabelSlot( pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                              pTableTextIDs, 0, ucStrNum);
}



BOOL fnfSMenuLabelSlot2 (UINT16 *pFieldDest, 
                         UINT8   bLen, 
                         UINT8   bFieldCtrl,
                         UINT8   bNumTableItems,                          
                         UINT8  *pTableTextIDs, 
                         UINT8  *pAttributes)
{
    UINT8   ucStrNum = 0xFF;

    return  fnSMenuLabelSlot( pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                              pTableTextIDs, 1, ucStrNum);
}



BOOL fnfSMenuLabelSlot3(UINT16 *pFieldDest, 
                         UINT8   bLen, 
                         UINT8   bFieldCtrl,
                         UINT8   bNumTableItems, 
                         UINT8  *pTableTextIDs, 
                         UINT8  *pAttributes)
{
    UINT8   ucStrNum = 0xFF;

    return  fnSMenuLabelSlot( pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                              pTableTextIDs, 2, ucStrNum);
}



BOOL fnfSMenuLabelSlot4 (UINT16 *pFieldDest, 
                         UINT8   bLen, 
                         UINT8   bFieldCtrl,
                         UINT8   bNumTableItems, 
                         UINT8  *pTableTextIDs, 
                         UINT8  *pAttributes)
{
    UINT8   ucStrNum = 0xFF;

    return  fnSMenuLabelSlot( pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                              pTableTextIDs, 3, ucStrNum);
}


/**************************************************************************
// FUNCTION NAME:   fnfSMenuNumOrLabelSlot1 - 4
// DESCRIPTION: Output field function that displays the menu number or a required label string that
//              belongs in the given scrollable menu slot.
//
// INPUTS:  Standard field function inputs.
// RETURNS: Stay on the existing screen.
// NOTES:
// MODIFICATION HISTORY:
// 09-21-2012      Bob Li     Initial Version        
// *************************************************************************/
BOOL fnfSMenuNumOrLabelSlot1( UINT16    *pFieldDest, 
                              UINT8     bLen,   
                              UINT8     bFieldCtrl, 
                              UINT8     bNumTableItems, 
                              UINT8     *pTableTextIDs,
                              UINT8     *pAttributes)
{
    return (fnSMenuNumberOrLabelSlot(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                        pTableTextIDs, pAttributes, 0));
}

BOOL fnfSMenuNumOrLabelSlot2( UINT16    *pFieldDest, 
                              UINT8     bLen,   
                              UINT8     bFieldCtrl, 
                              UINT8     bNumTableItems, 
                              UINT8     *pTableTextIDs,
                              UINT8     *pAttributes)
{
    return (fnSMenuNumberOrLabelSlot(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                        pTableTextIDs, pAttributes, 1));
}

BOOL fnfSMenuNumOrLabelSlot3( UINT16    *pFieldDest, 
                              UINT8     bLen,   
                              UINT8     bFieldCtrl, 
                              UINT8     bNumTableItems, 
                              UINT8     *pTableTextIDs,
                              UINT8     *pAttributes)
{
    return (fnSMenuNumberOrLabelSlot(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                        pTableTextIDs, pAttributes, 2));
}

BOOL fnfSMenuNumOrLabelSlot4( UINT16    *pFieldDest, 
                              UINT8     bLen,   
                              UINT8     bFieldCtrl, 
                              UINT8     bNumTableItems, 
                              UINT8     *pTableTextIDs,
                              UINT8     *pAttributes)
{
    return (fnSMenuNumberOrLabelSlot(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                        pTableTextIDs, pAttributes, 3));
}

/* *************************************************************************
// FUNCTION NAME: fnfSMenuNumberSlot1-5
//
// DESCRIPTION:
//      Output field function that displays the scrollable menu selection number 
//      that belongs in the given slot.  This can vary because some
//      menu options may not be supported depending on PCN parameters
//      or the current state of the system.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      always returns successful
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//      Mozdzer   Initial Version
// *************************************************************************/
BOOL fnfSMenuNumberSlot1 (UINT16 *pFieldDest, 
                          UINT8   bLen, 
                          UINT8   bFieldCtrl,
                          UINT8   bNumTableItems, 
                          UINT8  *pTableTextIDs, 
                          UINT8  *pAttributes)
{
    return (fnSMenuNumber(pFieldDest,bLen,pSMenuTable.usFirstDisplayedSlotIndex));
}



BOOL fnfSMenuNumberSlot2 (UINT16 *pFieldDest, 
                          UINT8   bLen, 
                          UINT8   bFieldCtrl,
                          UINT8   bNumTableItems, 
                          UINT8  *pTableTextIDs, 
                          UINT8  *pAttributes)
{
    return (fnSMenuNumber(pFieldDest,bLen,pSMenuTable.usFirstDisplayedSlotIndex+1));
}



BOOL fnfSMenuNumberSlot3 (UINT16 *pFieldDest, 
                          UINT8   bLen, 
                          UINT8   bFieldCtrl,
                          UINT8   bNumTableItems, 
                          UINT8  *pTableTextIDs, 
                          UINT8  *pAttributes)
{
    return (fnSMenuNumber(pFieldDest,bLen,pSMenuTable.usFirstDisplayedSlotIndex+2));
}



BOOL fnfSMenuNumberSlot4 (UINT16 *pFieldDest, 
                          UINT8   bLen, 
                          UINT8   bFieldCtrl,
                          UINT8   bNumTableItems, 
                          UINT8  *pTableTextIDs, 
                          UINT8  *pAttributes)
{
    return (fnSMenuNumber(pFieldDest,bLen,pSMenuTable.usFirstDisplayedSlotIndex+3));
}



BOOL fnfSMenuNumberSlot5 (UINT16 *pFieldDest, 
                          UINT8   bLen, 
                          UINT8   bFieldCtrl,
                          UINT8   bNumTableItems, 
                          UINT8  *pTableTextIDs, 
                          UINT8  *pAttributes)
{
    return (fnSMenuNumber(pFieldDest,bLen,pSMenuTable.usFirstDisplayedSlotIndex+4));
}



/* *************************************************************************
// FUNCTION NAME: fnfSMenuValueSlot1Init - fnfSMenuValueSlot4Init
//
// DESCRIPTION:
//      Output field function that displays the value string that
//      that belongs in the given scrollable menu slot.  The value
//      string is generated by calling another field function for the
//      given screen.  This function's job is to determine which is
//      the correct field function to call and call it.  The init
//      function is called in this variation of the function.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      always returns successful
//
// WARNINGS/NOTES:  
//      The menu label strings in their order of preference.
//
// MODIFICATION HISTORY:
//      Mozdzer   Initial Version
// *************************************************************************/
BOOL fnfSMenuValueSlot1Init (UINT16 *pFieldDest, 
                             UINT8   bLen, 
                             UINT8   bFieldCtrl,
                             UINT8   bNumTableItems, 
                             UINT8  *pTableTextIDs, 
                             UINT8  *pAttributes)
{
    return fnSMenuValueSlotInit(pFieldDest, bLen, bFieldCtrl,
                                bNumTableItems, pTableTextIDs, pAttributes, 0);
}



BOOL fnfSMenuValueSlot2Init (UINT16 *pFieldDest, 
                             UINT8   bLen, 
                             UINT8   bFieldCtrl,
                             UINT8   bNumTableItems, 
                             UINT8  *pTableTextIDs, 
                             UINT8  *pAttributes)
{
    return fnSMenuValueSlotInit(pFieldDest, bLen, bFieldCtrl,
                                bNumTableItems, pTableTextIDs, pAttributes, 1);
}



BOOL fnfSMenuValueSlot3Init (UINT16 *pFieldDest, 
                             UINT8   bLen, 
                             UINT8   bFieldCtrl,
                             UINT8   bNumTableItems, 
                             UINT8  *pTableTextIDs, 
                             UINT8  *pAttributes)
{
    return fnSMenuValueSlotInit(pFieldDest, bLen, bFieldCtrl,
                                bNumTableItems, pTableTextIDs, pAttributes, 2);
}



BOOL fnfSMenuValueSlot4Init (UINT16 *pFieldDest, 
                             UINT8   bLen, 
                             UINT8   bFieldCtrl,
                             UINT8   bNumTableItems, 
                             UINT8  *pTableTextIDs, 
                             UINT8  *pAttributes)
{
    return fnSMenuValueSlotInit(pFieldDest, bLen, bFieldCtrl,
                                bNumTableItems, pTableTextIDs, pAttributes, 3);
}


/* *************************************************************************
// FUNCTION NAME: fnfSMenuValueSlot1Refresh - fnfSMenuValueSlot4Refresh
//
// DESCRIPTION:
//      Output field function that displays the value string that
//      that belongs in the given scrollable menu slot.  The value
//      string is generated by calling another field function for the
//      given screen.  This function's job is to determine which is
//      the correct field function to call and call it.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      always returns successful
//
// WARNINGS/NOTES:  
//      The menu label strings in their order of preference.
//
// MODIFICATION HISTORY:
//      Mozdzer   Initial Version
// *************************************************************************/
BOOL fnfSMenuValueSlot1Refresh (UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes)
{
    return fnSMenuValueSlotRefresh( pFieldDest, bLen, bFieldCtrl,
                                    bNumTableItems, pTableTextIDs, pAttributes, 0);
}



BOOL fnfSMenuValueSlot2Refresh (UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes)
{
    return fnSMenuValueSlotRefresh( pFieldDest, bLen, bFieldCtrl,
                                    bNumTableItems, pTableTextIDs, pAttributes, 1);
}



BOOL fnfSMenuValueSlot3Refresh (UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes)
{
    return fnSMenuValueSlotRefresh( pFieldDest, bLen, bFieldCtrl,
                                    bNumTableItems, pTableTextIDs, pAttributes, 2);
}



BOOL fnfSMenuValueSlot4Refresh (UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT8  *pAttributes)
{
    return fnSMenuValueSlotRefresh( pFieldDest, bLen, bFieldCtrl,
                                    bNumTableItems, pTableTextIDs, pAttributes, 3);
}



/* *************************************************************************
// FUNCTION NAME: fnfSoftKeyIndicator1-5
//
// DESCRIPTION:
//      Output field function that displays the soft key indicator
//      on for soft key x(x equals 1 - 5) if that line is active.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      always returns successful
//
// WARNINGS/NOTES:  
//      1st string is displayed if the soft key is active
//
// MODIFICATION HISTORY:
//      Mozdzer   Initial Version
// *************************************************************************/
BOOL fnfSoftKeyIndicator1(UINT16 *pFieldDest, 
                          UINT8   bLen, 
                          UINT8   bFieldCtrl,
                          UINT8   bNumTableItems, 
                          UINT8  *pTableTextIDs, 
                          UINT8  *pAttributes)
{
    return  fnSoftKeyIndicator( pFieldDest, bLen, bFieldCtrl,
                                bNumTableItems, pTableTextIDs, 0);
}



BOOL fnfSoftKeyIndicator2(UINT16 *pFieldDest, 
                          UINT8   bLen, 
                          UINT8   bFieldCtrl,
                          UINT8   bNumTableItems, 
                          UINT8  *pTableTextIDs, 
                          UINT8  *pAttributes)
{
    return  fnSoftKeyIndicator( pFieldDest, bLen, bFieldCtrl,
                                bNumTableItems, pTableTextIDs, 1);
}



BOOL fnfSoftKeyIndicator3(UINT16 *pFieldDest, 
                          UINT8   bLen, 
                          UINT8   bFieldCtrl,
                          UINT8   bNumTableItems, 
                          UINT8  *pTableTextIDs, 
                          UINT8  *pAttributes)
{
    return  fnSoftKeyIndicator( pFieldDest, bLen, bFieldCtrl,
                                bNumTableItems, pTableTextIDs, 2);
}



BOOL fnfSoftKeyIndicator4(UINT16 *pFieldDest, 
                          UINT8   bLen, 
                          UINT8   bFieldCtrl,
                          UINT8   bNumTableItems, 
                          UINT8  *pTableTextIDs, 
                          UINT8  *pAttributes)
{
    return  fnSoftKeyIndicator( pFieldDest, bLen, bFieldCtrl,
                                bNumTableItems, pTableTextIDs, 3);
}



BOOL fnfSoftKeyIndicator5(UINT16 *pFieldDest, 
                          UINT8   bLen, 
                          UINT8   bFieldCtrl,
                          UINT8   bNumTableItems, 
                          UINT8  *pTableTextIDs, 
                          UINT8  *pAttributes)
{
    return  fnSoftKeyIndicator( pFieldDest, bLen, bFieldCtrl,
                                bNumTableItems, pTableTextIDs, 4);
}

/*--------------------------*/
/*   Hard Key Functions     */
/*--------------------------*/



/* *************************************************************************
// FUNCTION NAME: fnhkJSMenuPageDown
//
// DESCRIPTION:
//      Scrolls a scrollable menu down one page.  If we are already
//     showing the bottom of the list, this function has no effect.
//
// INPUTS:
//      Standard hard key function inputs (format 1).
//
// RETURNS:
//      Next screen
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//      Mozdzer    Initial Version
// *************************************************************************/
T_SCREEN_ID fnhkJSMenuPageDown (UINT8        bKeyCode, 
                                UINT8        bNumNext,
                                T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID   usNext = 0;
    UINT16   usNewLastIndex;

    /* calculate the slot index of the first record on the next page */
    usNewLastIndex = pSMenuTable.usFirstDisplayedSlotIndex + pSMenuTable.bEntriesPerPage;

    /* if it is within the array bounds, and the slot is being used (bKeyAssignment not 0), 
        increment the first displayed slot index by the number of entries per page */
    if ((usNewLastIndex < MAX_SMENU_SLOTS) 
        && (pSMenuTable.pSlot[usNewLastIndex].bKeyAssignment != 0))
    {
        pSMenuTable.usFirstDisplayedSlotIndex += pSMenuTable.bEntriesPerPage;
    }

    //update LEDs
    (void)fnUpdateLEDPageIndicator();

    return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: fnhkJSMenuPageUp
//
// DESCRIPTION:
//      Scrolls a scrollable menu up one page.  If we are at the
//     top of the list already, this function has no effect.
//
// INPUTS:
//      Standard hard key function inputs (format 1).
//
// RETURNS:
//      Next screen
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//      Mozdzer    Initial Version
// *************************************************************************/
T_SCREEN_ID fnhkJSMenuPageUp   (UINT8        bKeyCode, 
                                UINT8        bNumNext,
                                T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID  usNext = 0;

    /* if we are not at the top of the list, decrement the first displayed slot index */
    if (pSMenuTable.usFirstDisplayedSlotIndex != 0)
    {
        if (pSMenuTable.usFirstDisplayedSlotIndex > pSMenuTable.bEntriesPerPage)
        {
            pSMenuTable.usFirstDisplayedSlotIndex -= pSMenuTable.bEntriesPerPage;
        }
        else
        {
            pSMenuTable.usFirstDisplayedSlotIndex = 0;
        }
    }
    //update LEDs
    (void)fnUpdateLEDPageIndicator();

    return (usNext);

}



/* *************************************************************************
// FUNCTION NAME: 
//      fnhkSMenuProgressiveLookup
//
// DESCRIPTION: 
//      General hard key function to buffer the keypress(alpha or numeric) in 
//      the current entry buffer. If the entered code or name in buffer matches 
//      one of listed code or name, resort the list according the entered type
//      and displays the corresponding page.
//      This function will be applied as both Keypad and Keyboard hard key 
//      functions for this kind of lookup.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      Screen no change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  10/27/2005  Vincent Yi      Initial version 
//  02/06/2006  Kan Jiang       Modified
//  02/10/2006  Vincent Yi      Refined the code
//
// *************************************************************************/
T_SCREEN_ID fnhkSMenuProgressiveLookup (UINT8         bKeyCode, 
                                        UINT8         bNumNext, 
                                        T_SCREEN_ID * pNextScreens)
{
    T_SCREEN_ID     usNext = 0;
    UINT8           bBufferedLen;
    ENTRY_BUFFER *  pGenEBuffer = fnGetGenPurposeEbuf();

    // Capture all entry buffer data so it can be later restore
    fnEntryBufferSnapshotCapture();
    // Buffer the key
    fnBufferKeyClear1st(bKeyCode);

    bBufferedLen = fnCharsBuffered();

    // Don't allow the leading space or any hyphen
    if (((bBufferedLen == 1) && (pGenEBuffer->pBuf[0] == UNICODE_SPACE)) ||
        (pGenEBuffer->pBuf[bBufferedLen-1] == UNICODE_MINUS_SIGN))
    {
        fnEntryBufferSnapshotRestore();
        fnSetEntryErrorCode(ENTRY_INVALID_VALUE);  
        return (usNext);
    }
    else
    {
        // Resort the menu and update the display page
        if (fnResortMenuByEntryBuffer() == FAILURE)
        {   // No matched Code or Name
            fnSetEntryErrorCode(ENTRY_INVALID_VALUE);  
            fnEntryBufferSnapshotRestore();
        }
    }
        
    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkSMenuProgressiveLookupSS1
//
// DESCRIPTION: 
//      Silmilar as fnhkSMenuProgressiveLookup, for shift state 1
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      Screen no change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  10/28/2005  Vincent Yi      Initial version 
//
// *************************************************************************/
T_SCREEN_ID fnhkSMenuProgressiveLookupSS1 (UINT8          bKeyCode, 
                                           UINT8          bNumNext, 
                                           T_SCREEN_ID *  pNextScreens)
{
    fnChangeShiftState(1);
    return fnhkSMenuProgressiveLookup(bKeyCode, bNumNext, pNextScreens);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkSMenuProgressiveLookupSS2
//
// DESCRIPTION: 
//      Silmilar as fnhkSMenuProgressiveLookup, for shift state 2
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      Screen no change.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  10/28/2005  Vincent Yi      Initial version 
//
// *************************************************************************/
T_SCREEN_ID fnhkSMenuProgressiveLookupSS2 (UINT8          bKeyCode, 
                                           UINT8          bNumNext, 
                                           T_SCREEN_ID *  pNextScreens)
{
    fnChangeShiftState(2);
    return fnhkSMenuProgressiveLookup(bKeyCode, bNumNext, pNextScreens);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnhkDeleteCharAndResorted
//
// DESCRIPTION: 
//      Hard key function that deletes a char from entry buffer 
//      and re-sort the output string.
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      No screen change
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/06/2006  Kan Jiang       Initial version
//  02/10/2006  Vincent Yi      Rename it from fnhkCleanBufferAndResorted to 
//                              fnhkDeleteCharAndResorted, refined the code
//
// *************************************************************************/
T_SCREEN_ID  fnhkDeleteCharAndResorted(UINT8        bKeyCode, 
                                       UINT8        bNumNext, 
                                       T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID     usNext = 0;
    UINT8           bBufferedLen;

    fnDeleteCharacter();
    bBufferedLen = fnCharsBuffered();
    if (bBufferedLen != 0)
    {
        (void)fnResortMenuByEntryBuffer();
    }

    return usNext;
}   

/* *************************************************************************
// FUNCTION NAME: fnSMenuNumberOrLabelSlot
//
// DESCRIPTION:
//      Utility function that builds a Unicode number string or a required label string
//      for a given scrollable menu slot
//      This is used to handle the numbering of options
//      in a field based menu.
//
// INPUTS:
//      Standard field function inputs.
//      usSlotIndex - the menu slot number.  0 is the first slot, 1 is the second, ...
//
// RETURNS:
//       SUCCESSFUL, the field is left blank if the number string doesn't fit
//
// WARNINGS/NOTES:  
//       None
//
// MODIFICATION HISTORY:
//  2012-09-21  Bob Li      Init Version.
// *************************************************************************/
static BOOL fnSMenuNumberOrLabelSlot (UINT16 *pFieldDest, 
                                        UINT8   bLen, 
                                        UINT8   bFieldCtrl, 
                                        UINT8   bNumTableItems, 
                                        UINT8   *pTableTextIDs,
                                        UINT8   *pAttributes,
                                        UINT16  usItemIndex)
{
    UINT8   ubWritingDir;
    UINT8   ucStrNum;
    UINT16  usSlotIndex;
    S_MENU_TABLE *pMenuTable = fnGetSMenuTable();

    ubWritingDir = fnGetWritingDir();
    
    SET_SPACE_UNICODE(pFieldDest, bLen);
    
    //get the slot index
    usSlotIndex = pMenuTable->usFirstDisplayedSlotIndex + usItemIndex;
    if (usSlotIndex < MAX_SMENU_SLOTS)
    {
        if(pMenuTable->pSlot[usSlotIndex].bLabelString == 0)
        {
            //display the fee number string, Such as 1, 2, 3...
            fnSMenuNumber(pFieldDest,bLen,usSlotIndex);
        }
        else
        {
            //display the required label string
            ucStrNum = pSMenuTable.pSlot[usSlotIndex].bLabelString;

            if ((ucStrNum != 0) && (ucStrNum <= bNumTableItems))
            {
                fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                    pTableTextIDs, ucStrNum-1 );
            }
        }
    }

    return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: fnSMenuNumber
//
// DESCRIPTION:
//      Utility function that builds a Unicode number string for a    
//      given scrollable menu slot.  This is used to handle the numbering of options
//      in a field based menu.
//
// INPUTS:
//      pFieldDest - the output Unicode string is placed here.
//      bLen - length of the field.  must be at least 2.
//      bSlot - the menu slot number.  0 is the first slot, 1 is the second, ...
//
// RETURNS:
//       SUCCESSFUL, the field is left blank if the number string doesn't fit
//
// WARNINGS/NOTES:  
//       None
//
// MODIFICATION HISTORY:
//       Mozdzer   Initial Version
// *************************************************************************/
static BOOL fnSMenuNumber (UINT16 *pFieldDest, 
                           UINT8   bLen, 
                           UINT16  usSlotIndex)
{
    char    cAsciiNum[6];
    SINT32   lStrLen;

    SET_SPACE_UNICODE(pFieldDest, bLen);
    
    if (usSlotIndex < MAX_SMENU_SLOTS)
    {
        if (pSMenuTable.pSlot[usSlotIndex].fActive == TRUE)
        {
            lStrLen = sprintf(cAsciiNum,"%d",pSMenuTable.pSlot[usSlotIndex].wDisplayNumber);
        }
        else
        {
            lStrLen = sprintf(cAsciiNum, "--");
        }
    }
    else
    {
        lStrLen = 0;
        return ((BOOL)SUCCESSFUL);
    }

    /* field length must be big enough to accomodate the string or else leave blank */
    if (bLen < lStrLen)
    {
        return ((BOOL)SUCCESSFUL);
    }

    if (pSMenuTable.pSlot[usSlotIndex].bKeyAssignment > 0)
    {
        fnAscii2Unicode(cAsciiNum, pFieldDest, lStrLen);
    }

    return ((BOOL)SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: fnCallNestedFieldFunction
//
// DESCRIPTION:
//      Utility function that calls a nested field function for the
//      current screen.
//
// INPUTS:
//      usSlotIndex- specifies which slot's function to call (starts w/ 0)
//      pNextScreen- the next screen returned by the function is returned here
//
// RETURNS:
//      SUCCESSFUL if a function was called
//      FAILURE    if a function was not called- either the slot was not mapped
//                 to a key, it was not active, or there was a missing key record
//
// WARNINGS/NOTES:  
//       None
//
// MODIFICATION HISTORY:
//       Mozdzer   Initial Version
// *************************************************************************/
static BOOL fnCallNestedFieldFunction (UINT16 *pFieldDest, 
                                       UINT8   bFunctionNum, 
                                       BOOL    fCallInit,
                                       UINT8   bParentLen,
                                       UINT8   *pAttributes)
{
    BOOL    fFieldFnExists;                                       
    BOOL    fFieldFnStatus = SUCCESSFUL;
    UINT16  usInitID;
    UINT16  usRefreshID;
    UINT8   bChildLen;
    UINT8   bFieldCtrl;
    UINT8   bNumTableItems;
    UINT8  *pTableTextIDs;

    /* don't do anything if the function number is 0, since this is not valid */
    if (bFunctionNum == 0)
    {
        return ( fFieldFnStatus );
    }

    fFieldFnExists = fnGetFieldInfo(bFunctionNum, &usInitID, &usRefreshID, &bChildLen, 
                                    &bFieldCtrl, pAttributes, &bNumTableItems, &pTableTextIDs);

    if (fFieldFnExists == TRUE)
    {
        if (bChildLen > bParentLen)
        {
            return ( fFieldFnStatus );  
        }

        if (fCallInit == TRUE)
        {
            fFieldFnStatus = aInitFns[usInitID](pFieldDest,bChildLen,bFieldCtrl,
                                                bNumTableItems,pTableTextIDs, pAttributes);
        }
        else
        {
            fFieldFnStatus = aRefreshFns[usRefreshID]( pFieldDest,bChildLen, bFieldCtrl, 
                                                       bNumTableItems, pTableTextIDs, pAttributes);
        }
    }

    return (fFieldFnStatus);
}


        
/* *************************************************************************
// FUNCTION NAME: fnSMenuImageSelectForDelete
//
// DESCRIPTION:
//      Utility function that handles selecting an ad or inscription
//      for deletion based on the menu slot being invoked.
//
// INPUTS:
//      wSlotIndex- specifies which slot is being invoked for selection
//      bImageType- pass in either AD_COMP_TYPE or INSCR_COMP_TYPE to indicate
//                  what kind of image we are dealing with.
//      standard format 2 hard key arguments are also passed in
//
// RETURNS:
//      next screen to go to,
//      goes to first next screen if we made a selection 
//      stays on the same screen if the slot we were using was not used
//      or the image id was invalid
//
// WARNINGS/NOTES:  
//       None
//
// MODIFICATION HISTORY:
//       Mozdzer   Initial Version
// *************************************************************************/
UINT16 fnSMenuImageSelectForDelete(UINT8   bKeyCode, 
                                   UINT8   bNumNext, 
                                   UINT16 *pNextScreens,
                                   UINT16  wSlotIndex, 
                                   UINT8   bImageType)
{
    UINT16  usNext = 0;
    BOOL    fValidSelection = FALSE;

    /* determine the key number that goes with the soft key 
      and use the utility function to make the call */
    if (wSlotIndex < MAX_SMENU_SLOTS)
    {
        /* if this is a used slot, there is an ad sitting in it... */
        if (pSMenuTable.pSlot[wSlotIndex].bKeyAssignment != 0)
        {
            UINT16  usImgID;

            usImgID = (UINT16) pSMenuTable.pSlot[wSlotIndex].pGenPurp1;
            if (usImgID <= 255)
            {
                fValidSelection = TRUE;
                if (bImageType == AD_COMP_TYPE)
                {
                    wAd2DeleteId = usImgID;
                    wAd2DelMenuIdx = pSMenuTable.pSlot[wSlotIndex].wDisplayNumber;
                }
                else if (bImageType == INSCR_COMP_TYPE)
                {
                    wInsc2DeleteId = usImgID;
                    wInsc2DelMenuIdx = pSMenuTable.pSlot[wSlotIndex].wDisplayNumber;
                }
            }
        }
    }

    if ( (bNumNext != 0) && (fValidSelection == TRUE))
    {
            usNext = pNextScreens[0];
    }

    return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnSetMenuEntriesTotalNumber
//
// DESCRIPTION: 
//      Accessor function to set usMenuTotalEntries
//
// INPUTS:
//      usTotalEntries  - the total number of the menu entries
//
// NOTES:
//
// MODIFICATION HISTORY:
// 10/24/2005   Vincent Yi      Initial Version
//
// *************************************************************************/
void fnSetMenuEntriesTotalNumber (UINT16 usTotalEntries)
{
    usMenuTotalEntries = usTotalEntries;


}
/* *************************************************************************
// FUNCTION NAME: 
//      fnGetMenuEntriesTotalNumber
//
// DESCRIPTION: 
//      Accessor function to get usMenuTotalEntries
//
// INPUTS:
//      usTotalEntries  - the total number of the menu entries
//
// NOTES:
//
// MODIFICATION HISTORY:
// 04/17/2007   Andy Mo      Initial Version
//
// *************************************************************************/
UINT16 fnGetMenuEntriesTotalNumber (void)
{
    return(usMenuTotalEntries);
}


/* *************************************************************************
// FUNCTION NAME: fnSetupMenuSlots
//
// DESCRIPTION:
//       general purpose menu slot initialization function
//
// INPUTS:
//       pMenuTable, address of table defining initialization values for a
//                   given menu
//       ulSlotIdx, starting value of pSMenuTable array index
//       bPageLength, number of slots per menu page
//
// RETURNS:
//       Updated slot index value (next available slot)
//
// WARNINGS/NOTES:  
// 1.   See menu prep functions in ocimenu.c for examples of slot initialization
//      tables (type S_MENU_SETUP_ENTRY).
// 2.   Last entry must always contain END_MENU_TABLE_MARKER value in the
//      element wDisplayNumber; else, loop will not terminated.
// 3.   Default value assignment for wDisplayNumber is "ulSlotIdx + 1 " as is used
//      throughout most of the menu initialization functions in MEGA UIC.
// 4.   Functions which test conditions for initializating a slot must return a
//      value that can be converted to BOOL (UINT8) and is expected to be
//      interpreted as TRUE/FALSE value.
// 5.   If multiple conditions need to be checked for a slot, a new function can be
//      supplied that checks all the conditions and returns a go/no go to this function.
// 6.   V9.04(cristeb): a slot is initialized for any activity byte value other than
//      OIT_SLOT_NOT_ACTIVE; OIT_SLOT_DISPLAY_ONLY value is defined so that all display data
//      for slot except softkey indicator will be presented onscreen (useful for those options
//      that can be 'momentarily' available based on some run-time condition.) OIT_SLOT_RESERVE
//      value is defined when an option is not available based on current UIC configuration
//      but remaining menu items do not 'slide up' to fill in unused slots.
// 7.   Use OIT_SLOT_NOT_ACTIVE value to skip over menu slot (all succeeding entries will 
//      'slide up' one slot.
//
// MODIFICATION HISTORY:
//       Zamary   Initial Version
// *************************************************************************/
UINT32 fnSetupMenuSlots( S_MENU_SETUP_ENTRY *pMenuInitTable, 
                         UINT32              ulSlotIdx, 
                         UINT8               bPageLength )
{
    S_MENU_SETUP_ENTRY *pTabPtr ;
    UINT32              ulSIdx ;

    pTabPtr = pMenuInitTable ;
    ulSIdx = ulSlotIdx ;

    // only clear menu table on command
    if( pTabPtr->wDisplayNumber == CLEAR_MENU_TABLE ) 
    {
        CLEAR_SMENU_TABLE(pSMenuTable);
        pTabPtr++ ;
    }

    // now process live entries
    while ( pTabPtr->wDisplayNumber != END_MENU_TABLE_MARKER )
    {
        // allocate slot for any entry that is marked either active(&display) or display-only
        if(pTabPtr->bSlotActivity && (pTabPtr->pfctn == NULL_PTR || (pTabPtr->pfctn() == TRUE)) )
        {
            if( pTabPtr->bSlotActivity != OIC_SLOT_RESERVE )
            {
                // setup slot for SLOT_ACTIVE and SLOT_DISPLAY_ONLY cases
                pSMenuTable.pSlot[ulSIdx].bKeyAssignment = pTabPtr->bKeyAssignment ;
                if( pTabPtr->wDisplayNumber == USE_DEFAULT_ASSIGNMENT )
                {
                    pSMenuTable.pSlot[ulSIdx].wDisplayNumber = ulSIdx + 1 ;
                }
                else
                {
                    pSMenuTable.pSlot[ulSIdx].wDisplayNumber = pTabPtr->wDisplayNumber ;
                }
                pSMenuTable.pSlot[ulSIdx].bLabelString = pTabPtr->bLabelStringId ;

                if( pTabPtr->bSlotActivity == OIC_SLOT_ACTIVE ) 
                {
                    pSMenuTable.pSlot[ulSIdx].fActive = TRUE;
                }
            }

            ulSIdx++;
        }

        pTabPtr++ ;
    }

    // use caller's length
    pSMenuTable.bEntriesPerPage = bPageLength ;

    // return index to next available slot
    return ulSIdx ;
}



/* *************************************************************************
// FUNCTION NAME: fnSMenuLabelSlot
//
// DESCRIPTION:
//      utility function  that displays the menu label string 
//      that belongs in the given scrollable menu slot.  This can vary because some
//      menu options may not be supported depending on PCN parameters
//      or the current state of the system.
//
// INPUTS:
//      Standard field function inputs.
//      usIndex: slot index
//
// RETURNS:
//      always returns successful
//
// WARNINGS/NOTES:  
//      The menu label strings in their order of preference.
//
// MODIFICATION HISTORY:
//      2010-04-21 Jingwei,Li Fix fraca for RTL language display.
//      2010-04-21 Jingwei,Li Modified for RTL language display.
//      2005-11-21 Adam Liu   Adapted for FuturePhoenix
//      2005-8-25  Kan Jiang   Initial Version
// *************************************************************************/
 static BOOL  fnSMenuLabelSlot (UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems, 
                                UINT8  *pTableTextIDs, 
                                UINT16  usIndex, 
                                UINT8   ucStrNum)
{
    UINT16  usSlotIndex;
    UINT8   ubWritingDir;
    
    ubWritingDir = fnGetWritingDir();
    
    SET_SPACE_UNICODE(pFieldDest, bLen);
    
    usSlotIndex = pSMenuTable.usFirstDisplayedSlotIndex+usIndex;
    if (usSlotIndex < MAX_SMENU_SLOTS)
    {
        ucStrNum = pSMenuTable.pSlot[usSlotIndex].bLabelString;
    }

    if ((ucStrNum != 0) && (ucStrNum <= bNumTableItems))
    {
        fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                            pTableTextIDs, ucStrNum-1 );
    }
    else if (usSlotIndex < MAX_SMENU_SLOTS)
    {
        if ((ucStrNum == 0) && (pSMenuTable.pSlot[usSlotIndex].pUnicode != NULL_PTR))
        {
            UINT32  ulUniLen;

            ulUniLen = fnUnicodeLen(pSMenuTable.pSlot[usSlotIndex].pUnicode);
            if (ulUniLen > bLen)
            {
                ulUniLen = bLen;
                pFieldDest[ulUniLen] = NULL_VAL;
            }

            memcpy( pFieldDest, pSMenuTable.pSlot[usSlotIndex].pUnicode, 
                    (ulUniLen)*sizeof(UINT16) );


            if ((bFieldCtrl & ALIGN_MASK)||(ubWritingDir == RIGHT_TO_LEFT))
            {
                 fnUnicodePadSpace(pFieldDest, ulUniLen, bLen);
            }
            pFieldDest[bLen] = NULL_VAL;
        }
        else
        {
            SET_SPACE_UNICODE(pFieldDest, bLen);
        }
    }

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: fnSMenuValueSlotInit
//
// DESCRIPTION:
//      utility function that displays the value string that
//      that belongs in the given scrollable menu slot.  The value
//      string is generated by calling another field function for the
//      given screen.  This function's job is to determine which is
//      the correct field function to call and call it.  The init
//      function is called in this variation of the function.
//
// INPUTS:
//      Standard field function inputs.
//      usIndex: slot index
//
// RETURNS:
//      always returns successful
//
// WARNINGS/NOTES:  
//      The menu label strings in their order of preference.
//
// MODIFICATION HISTORY:
//      2010-04-21 Jingwei,Li Modified for RTL language display.
//      2005-11-21 Adam Liu   Adapted for FuturePhoenix
//      2005-08-25 Kan Jiang   Initial Version
// *************************************************************************/
static  BOOL fnSMenuValueSlotInit (UINT16 *pFieldDest, 
                                   UINT8   bLen, 
                                   UINT8   bFieldCtrl,
                                   UINT8   bNumTableItems, 
                                   UINT8  *pTableTextIDs, 
                                   UINT8  *pAttributes,
                                   UINT16  usIndex)
{
    UINT8   bFieldFunctionNum = 0;
    UINT16  usSlotIndex;
    UINT8   ubWritingDir;


    /* initialize the buffer to spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    ubWritingDir = fnGetWritingDir();

    usSlotIndex = pSMenuTable.usFirstDisplayedSlotIndex + usIndex;
    if (usSlotIndex < MAX_SMENU_SLOTS)
    {
        bFieldFunctionNum = pSMenuTable.pSlot[usSlotIndex].bKeyAssignment;
        (void)fnCallNestedFieldFunction(pFieldDest, bFieldFunctionNum, TRUE, bLen, pAttributes);

        if(ubWritingDir == RIGHT_TO_LEFT)
        {
//           unistrtrim(pFieldDest, pFieldDest);
           if(unistrlen(pFieldDest) < bLen)
           {
             fnUnicodePadSpace(pFieldDest, unistrlen(pFieldDest), bLen);
           }
        }

    }

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: fnSMenuValueSlotRefresh
//
// DESCRIPTION:
//      utility function that displays the value string that
//      that belongs in the given scrollable menu slot.  The value
//      string is generated by calling another field function for the
//      given screen.  This function's job is to determine which is
//      the correct field function to call and call it.
//
// INPUTS:
//      Standard field function inputs.
//      usIndex: slot index
//
// RETURNS:
//      always returns successful
//
// WARNINGS/NOTES:  
//      The menu label strings in their order of preference.
//
// MODIFICATION HISTORY:
//      2010-04-21 Jingwei,Li Modified for RTL language display.
//      2005-11-21 Adam Liu   Adapted for FuturePhoenix
//      2005-08-25 Kan Jiang   Initial Version
// *************************************************************************/
 static BOOL fnSMenuValueSlotRefresh (UINT16 *pFieldDest, 
                                      UINT8   bLen, 
                                      UINT8   bFieldCtrl,
                                      UINT8   bNumTableItems, 
                                      UINT8  *pTableTextIDs, 
                                      UINT8  *pAttributes,
                                      UINT16  usIndex)
{
    UINT8   bFieldFunctionNum = 0;
    UINT16  usSlotIndex;
    UINT8   ubWritingDir;

    ubWritingDir = fnGetWritingDir();

    SET_SPACE_UNICODE(pFieldDest, bLen);
    
    usSlotIndex = pSMenuTable.usFirstDisplayedSlotIndex + usIndex;
    if (usSlotIndex < MAX_SMENU_SLOTS)
    {
        bFieldFunctionNum = pSMenuTable.pSlot[usSlotIndex].bKeyAssignment;
       (void)fnCallNestedFieldFunction(pFieldDest, bFieldFunctionNum, FALSE, bLen, pAttributes);
          if(ubWritingDir == RIGHT_TO_LEFT)
        {
//           unistrtrim(pFieldDest, pFieldDest);
           if(unistrlen(pFieldDest) < bLen)
           {
               fnUnicodePadSpace(pFieldDest, unistrlen(pFieldDest), bLen);
           }
        }
    }

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: fnSoftKeyIndicator
//
// DESCRIPTION:
//      uitility function that displays the soft key indicator
//      on for soft key x(x equals 1 - 5) if that line is active.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      always returns successful
//      usIndex: softkey index
//
// WARNINGS/NOTES:  
//      1st string is displayed if the soft key is active
//
// MODIFICATION HISTORY:
//      2005-11-21 Adam Liu   Adapted for FuturePhoenix
//      2005-08-25 Kan Jiang   Initial Version
// *************************************************************************/
 static BOOL fnSoftKeyIndicator(UINT16 *pFieldDest, 
                                UINT8   bLen, 
                                UINT8   bFieldCtrl,
                                UINT8   bNumTableItems,
                                UINT8  *pTableTextIDs, 
                                UINT16  usIndex)
{
    UINT16  usSlotIndex;

    SET_SPACE_UNICODE(pFieldDest, bLen);
    
    usSlotIndex = pSMenuTable.usFirstDisplayedSlotIndex + usIndex;
    if ((usSlotIndex < MAX_SMENU_SLOTS) && (bNumTableItems != 0))
    {
        /* if there is something in the corresponding slot and it is active, show
            the first string in the text table */
        if ( (pSMenuTable.pSlot[usSlotIndex].bKeyAssignment != 0) 
             &&(pSMenuTable.pSlot[usSlotIndex].fActive == TRUE) )
        {
            fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, 
                                bNumTableItems, pTableTextIDs, 0 );
        }
    }

    return ((BOOL) SUCCESSFUL);
}



/**************************************************************************
// FUNCTION NAME: 
//      fnGetSMenuTable
//
// DESCRIPTION:
//      Get the pointer of the variable pSMenuTable
//
// INPUTS:
//      None
//
// OUTPUTS:
//      None
//
// RETURNS:
//      the pointer of pSMenuTable
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//  08/30/2005  Kan Jiang      Initial version
//
// *************************************************************************/
S_MENU_TABLE * fnGetSMenuTable(void)
{
    return (&pSMenuTable);
}

/**************************************************************************
// FUNCTION NAME: 
//      fnClearSMenuTable
//
// DESCRIPTION:
//      Initialize pSMenuTable, replacement of CLEAR_SMENU_TABLE(pSMenuTable);
//
// INPUTS:
//      None
//
// OUTPUTS:
//      memory of pSMenuTable cleared to 0
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//  11/02/2005  Vincent Yi      Initial version
//
// *************************************************************************/
void fnClearSMenuTable(void)
{
    CLEAR_SMENU_TABLE(pSMenuTable);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnUpdateLEDPageIndicator
//
// DESCRIPTION: 
//      Utility function that update the PgUp/PgDown LED indicator.
//
// PRE-CONDITIONS:
//      This function should be called after the menu talbe has been set up
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None
//
// RETURNS:
//      None.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//
//  11/14/2005  Vincent Yi      Code cleanup
//              Henry Liu       Initial version
//
// *************************************************************************/
void fnUpdateLEDPageIndicator(void)
{
    UINT16      usNewLastIndex;

    //PAGE UP indicator
    if (pSMenuTable.usFirstDisplayedSlotIndex)
    {
        SET_PAGEUP_LED_ON();
    }
    else
    {
        SET_PAGEUP_LED_OFF();
    }

    //PAGE DOWN indicator
    /* calculate the slot index of the first record on the next page */
    usNewLastIndex = pSMenuTable.usFirstDisplayedSlotIndex + 
                                            pSMenuTable.bEntriesPerPage;

    /* if it is within the array bounds, and the slot is being used 
        (bKeyAssignment not 0), then display the first string in the list (if 
        there is a list). */
    if ((usNewLastIndex < MAX_SMENU_SLOTS) && 
        (pSMenuTable.pSlot[usNewLastIndex].bKeyAssignment))
    {
        SET_PAGEDOWN_LED_ON();
    }
    else
    {
        SET_PAGEDOWN_LED_OFF();
    }
}

/**************************************************************************
// FUNCTION NAME: 
//      fnObtainPreSortArrays
//
// DESCRIPTION:
//      Public function to get the pointers of the arrays used to store the 
//      list sorted by name and speed code respectively, and the size of the arrays.
//
// INPUTS:
//      ppSortedByName  - pointer of pointer to a array used to store the list sorted by name
//      ppSortedByCode  - pointer of pointer to a array used to store the list sorted by speed code
//      pSize           - pointer to the size  
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:  
//      This function is used in pre function of a progressive lookup screen
//
// MODIFICATION HISTORY:
//  02/10/2006  Vincent Yi      Initial version
//
// *************************************************************************/
void fnObtainPreSortArrays (S_SORT_INFO **  ppSortedByName, 
                            S_SORT_INFO **  ppSortedByCode, 
                            UINT16      *   pSize)
{
    // Initialize the arrays
    memset (pstListSortedByNames, 0, MAX_SMENU_SLOTS * sizeof(S_SORT_INFO));
    memset (pstListSortedByCodes, 0, MAX_SMENU_SLOTS * sizeof(S_SORT_INFO));

    *ppSortedByName = pstListSortedByNames;
    *ppSortedByCode = pstListSortedByCodes;
    *pSize = MAX_SMENU_SLOTS;
}

/**************************************************************************
// FUNCTION NAME: 
//      fnRegisterRefillMenuFunction
//
// DESCRIPTION:
//      Public function to register a specific refill SMenuTable function, 
//      then when doing progressive look-up in the hark function fnhkSMenuProgressiveLookup, 
//      it can be invoked for its specific use.
//
// INPUTS:
//      pfnRefillMenu - function pointer to a specific refill SMenuTable function 
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:  
//      This function should be called in pre function of a progressive lookup screen
//
// MODIFICATION HISTORY:
//  02/10/2006  Vincent Yi      Initial version
//
// *************************************************************************/
void fnRegisterRefillMenuFunction (pRefillMenuFn pfnRefillMenu)
{
    pfnRefillSMenuTable = pfnRefillMenu;
}

/**************************************************************************
// FUNCTION NAME: 
//      fnDeregisterRefillMenuFunction
//
// DESCRIPTION:
//      Public function to deregister the specific refill SMenuTable function
//
// INPUTS:
//      None
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:  
//      This function should be called in pre function of a progressive lookup screen
//
// MODIFICATION HISTORY:
//  02/10/2006  Vincent Yi      Initial version
//
// *************************************************************************/
void fnDeregisterRefillMenuFunction (void)
{
    pfnRefillSMenuTable = NULL;
}




/**********************************************************************
        Private Functions
**********************************************************************/

/* *************************************************************************
// FUNCTION NAME: 
//      fnResortMenuByEntryBuffer
//
// DESCRIPTION: 
//      Resort the menu list according to the entered characters, and make 
//      adjustment to display the appropriate page.
//
// INPUTS:
//      None.
//
// RETURNS:
//      SUCCESSFUL or FAILURE
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  2013.04.09  Bob Li          Modified to fix fraca 221100.
//  02/10/2006  Vincent Yi      Initial version
//
// *************************************************************************/
static BOOL fnResortMenuByEntryBuffer(void)
{
    BOOL            fRet = FAILURE;
    UINT8           bBufferedLen;
    ENTRY_BUFFER *  pGenEBuffer = fnGetGenPurposeEbuf();
    UINT16          usArrayLength = 0;
    UINT8        *  pMatchedName = NULL;

    // Get the number of the items in the list
    while(pstListSortedByCodes[usArrayLength].pUniName != NULL)
    {
        usArrayLength++;
    }

    // Store the entry buffer to pUniTempString
    bBufferedLen = fnCharsBuffered();
    memset (pUniTempString, 0, sizeof(pUniTempString));
    memcpy (pUniTempString, pGenEBuffer->pBuf, bBufferedLen*sizeof(UNICHAR));
    pUniTempString[bBufferedLen] = NULL_VAL; // null terminate
    
    // Check the buffered string is Code or Name
    if (uniisinteger(pUniTempString) == TRUE)
    {
        UINT32 ulInputCode;
           
        fnUnicode2Bin(pUniTempString, &ulInputCode, bBufferedLen);
        if (ulInputCode == 0)
        {   // The speed code "0" always shown on the top
            if (pfnRefillSMenuTable != NULL)
            {
                // call function pointer to invoke special function refilling 
                // menu table.    
                pfnRefillSMenuTable(pstListSortedByCodes, usArrayLength);  
                if (fnAdjustDisplayPageByMatchedName (pMatchedName) == SUCCESSFUL)
                {
                    fRet = SUCCESSFUL;
                }
            }
        }
        else
        {
            if ((pMatchedName = fnCheckMatchedSpeedCode (pUniTempString, bBufferedLen)) 
                    != NULL)
            {   // A Speed Code, pMatchedName points to a name string of the matched 
                // speed code
                if (pfnRefillSMenuTable != NULL)
                {
                    // call function pointer to invoke special function refilling 
                    // menu table. 
                    //Sorted AccList by name
                    pfnRefillSMenuTable(pstListSortedByNames, usArrayLength);  
                    if (fnAdjustDisplayPageByMatchedName (pMatchedName) == SUCCESSFUL)
                    {
                        fRet = SUCCESSFUL;
                    }
                }
            }
        }
    }

    if (fRet == FAILURE)
    {   // A Name, or code lookup above failed, see the code as a name
        if ((pMatchedName = fnCheckMatchedName (pUniTempString))
                != NULL)
        {   // pMatchedName points to a name string of the matched name.
            if (pfnRefillSMenuTable != NULL)
            {
                // call function pointer to invoke special function refilling 
                // menu table.    
                pfnRefillSMenuTable(pstListSortedByNames, usArrayLength);  
                if (fnAdjustDisplayPageByMatchedName (pMatchedName) == SUCCESSFUL)
                {
                    fRet = SUCCESSFUL;
                }
            }
        }
    }

    return fRet;
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnUnistrSubCmp
//
// DESCRIPTION: 
//      performs an unichar string comparision. 
//
// INPUTS:
//
// RETURNS:
//      If src have the substring equals to dst, then return TRUE.
//      Else, return False.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  08/17/2006  Kan Jiang     Initial version
// *************************************************************************/
static BOOL  fnUnistrSubCmp(const UNICHAR* src, 
                            const UNICHAR* dst, 
                                  UINT16 len )
{
    UNICHAR  f;
    UNICHAR  l;
    UINT16 i = 0;

    while ( (i < len) && (src[i]) )
    {
        f = src[i];
        l = dst[i];

        if(f != l)
        {
          break;
        }

        i++;            
    }

    if (i == len) 
    {
        return TRUE;
    }

    return FALSE;
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckMatchedSpeedCode
//
// DESCRIPTION: 
//      Check whether there is a matched code in the pre-sort array 
//      pstListSortedByCodes
//
// INPUTS:
//      None.
//
// RETURNS:
//      If found, return the pointer to the corresponding name
//      Else, return NULL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  09/21/2012  Liu Jupeng      Modified the code to fix fraca 146599 with the 
//                              second solution as Janus' solution.
//  08/17/2006  Kan Jiang     Modified for fixing fraca 102101
//  02/10/2006  Vincent Yi      Initial version
//
// *************************************************************************/
static UINT8 * fnCheckMatchedSpeedCode(const UNICHAR * pInputCode, 
                                             UINT8   ucStrLen)
{
    UINT16    usIndex;
    UINT16    count = 0;
    SINT8     sCmpResult;
    UINT8    *pMatchedName = NULL;
    UINT16    usEntryLen;
    UNICHAR   pTmpStr[MAX_UNI_STR_LEN+1];
        
    usEntryLen = unistrlen(pInputCode);
       
    for (usIndex = 0; usIndex < MAX_SMENU_SLOTS; usIndex++)
    {
        if (pstListSortedByCodes[usIndex].pUniName == NULL)
        {
            break;
        }

        memset(pTmpStr, 0, sizeof(pTmpStr));
                  
        fnBin2Unicode(pTmpStr, pstListSortedByCodes[usIndex].ulSpeedCode);

        /* Before comparing the input value, we will firstly filter the '0' character.
           Because, the '0' value is allowed to input */
        while ('0' == *pInputCode)
        {
            pInputCode++;
            ++count;
        }

        /* the input value fuzzily matches the screen list item from beginning the valid 
           character except the '0' character entry. */
        sCmpResult = fnUnistrSubCmp (pTmpStr, pInputCode, usEntryLen - count);
        if (sCmpResult == TRUE)
        {
           /* the input value exactly matches the screen list item. If the input value does
              not exactly matches the list item, the screen will just show "Use keypad" and not
              show "Done [Enter]" until the input value exactly matches the list item.*/
           if(unistrcmp(pTmpStr, pInputCode) != 0)
           {
               fnSetEntryErrorCode(ENTRY_NEED_TO_BE_INPUT);
           }
           
           pMatchedName = pstListSortedByCodes[usIndex].pUniName;
           break;
        }
    }

    return pMatchedName;    
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnCheckMatchedName
//
// DESCRIPTION: 
//      Check whether there is a matched name in the pre-sort array 
//      pstListSortedByNames
//
// INPUTS:
//      None.
//
// RETURNS:
//      If found, return the pointer to the corresponding name
//      Else, return NULL
//
// WARNINGS/NOTES:
//      Found matched name just the leftmost characters of the names 
//      are matched
//
// MODIFICATION HISTORY:
//  07/27/2006  Vincent Yi      Modified to fix address alignment issue
//  02/10/2006  Vincent Yi      Initial version
//
// *************************************************************************/
static UINT8 * fnCheckMatchedName(const UNICHAR * pInputName)
{
    UINT16      usIndex;
    UINT8    *  pMatchedName = NULL;
    SINT8       sCmpResult;
    UINT16      usEntryLen;
    UNICHAR     pTmpStr[MAX_UNI_STR_LEN+1];
        
    usEntryLen = unistrlen(pInputName);
    
    for (usIndex = 0; usIndex < MAX_SMENU_SLOTS; usIndex++)
    {
        if (pstListSortedByNames[usIndex].pUniName == NULL)
            break;

        // Make the unicode string aligned
        memcpy (pTmpStr, pstListSortedByNames[usIndex].pUniName, sizeof(pTmpStr));
        sCmpResult = unistripartcmp (pTmpStr, pInputName, usEntryLen);
        if (sCmpResult == 0)
        {
            pMatchedName = pstListSortedByNames[usIndex].pUniName;
            break;
        }
    }
    
    return pMatchedName;    
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnAdjustDisplayPageByMatchedName
//
// DESCRIPTION: 
//      Private function to scan all menu slots, , if the input name is equal 
//      to the leftmost characters of one of the display numbers, make the 
//      pSMenuTable.usFirstDisplayedSlotIndex change to corresponding page.
//
// PRE-CONDITIONS:
//      The SMenuTable has been refilled and sorted by name string.
//
// INPUTS:
//      pName   -   pointer to the Name
//
// OUTPUTS:
//      pSMenuTable.usFirstDisplayedSlotIndex change to corresponding page, if 
//      matched code found.
//
// RETURNS:
//      SUCCESSFUL  -   if matched code found
//      FAILURE     -   if no matched code
//
// WARNINGS/NOTES:
//      The SMenuTable may not be sorted by name before entering this function,
//      
//
// MODIFICATION HISTORY:
//  09/21/2012  Liu Jupeng      Modified the code to fix fraca 146599 with the 
//                              second solution as Janus' solution.
//  09/10/2012  Liu Jupeng      Added code to fix fraca 146599.
//  10/15/2007  Raymond Shen    Added code which updates LEDs to fix fraca 131233.
//  07/27/2006  Vincent Yi      Modified to fix address alignment issue
//  02/10/2006  Vincent Yi      Rename from fnMenuIndexLookupByName to 
//                              fnAdjustDisplayPageByMatchedName
//  11/22/2005  Vincent Yi      Initial version
//
// *************************************************************************/
static BOOL fnAdjustDisplayPageByMatchedName (UINT8 * pName)
{
    UINT16      usSlotIndex = 0;
    UINT16      usEntryLen;
    SINT8       sCmpResult;
    BOOL        fRet = FAILURE;
    UNICHAR     pTmpStr1[MAX_UNI_STR_LEN+1];
    UNICHAR     pTmpStr2[MAX_UNI_STR_LEN+1];

    if (pName == NULL)
    {  
        /* According to the first displaying item, the system will judge that the inputting value 
           '0' is valid value or invalid value. 
           If the display screen includes '0' item, the entry '0' is valid input. When you enter '0'
           the screen will show "Done [Enter]".
           If the display screen doesn't include '0' item, the entry '0' is invalid input. When you
           enter '0', the screen will show "Use keypad".the screen will not show the "Done [Enter]" 
           until your entry matches the screen list item.*/
        usSlotIndex = pSMenuTable.usFirstDisplayedSlotIndex;
        if (usSlotIndex < MAX_SMENU_SLOTS && pSMenuTable.pSlot[usSlotIndex].fActive == TRUE)
        {
            if (pSMenuTable.pSlot[usSlotIndex].wDisplayNumber != (UINT16)0)
            {
                fnSetEntryErrorCode(ENTRY_NEED_TO_BE_INPUT); 
            }
        }

                fRet = SUCCESSFUL;
    
        // Default, to first page
        pSMenuTable.usFirstDisplayedSlotIndex = 0;
        
        //update LEDs
        (void)fnUpdateLEDPageIndicator();

        return fRet;    
    }
    
    usEntryLen = fnUnicodeLen(pName);

    //check the menu table for the account ID
    while ((usSlotIndex < MAX_SMENU_SLOTS) && 
           (pSMenuTable.pSlot[usSlotIndex].fActive))
    {
        // Make the unicode string aligned
        memcpy (pTmpStr1, pSMenuTable.pSlot[usSlotIndex].pUnicode, sizeof(pTmpStr1));
        memcpy (pTmpStr2, pName, sizeof(pTmpStr2)); 
        sCmpResult = unistripartcmp(pTmpStr1, pTmpStr2, usEntryLen);
        if (sCmpResult == 0)
        {   // Found the matched code, adjust the index of the first 
            // displayed slot
            // Set the top of the page to the account that matches 
            pSMenuTable.usFirstDisplayedSlotIndex = 
          ((usSlotIndex  /pSMenuTable.bEntriesPerPage) * 
           pSMenuTable.bEntriesPerPage ) + (usSlotIndex % pSMenuTable.bEntriesPerPage );
            fRet = SUCCESSFUL;
            break;
        }
        usSlotIndex++;
    }   // while (...)

    //update LEDs
    (void)fnUpdateLEDPageIndicator();
    return(fRet);   
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnSMenuGetSlotIndexOfSlotFn
//
// DESCRIPTION: 
//      Utility function that scans the key code tables to find the menu slot
//      index of the associated hardkey slot function.
//
// INPUTS:
//      bKeyCode   -   key code of the associated hardkey slot function
//
// RETURNS:
//      usSlotIndex -   slot index of the associated hardkey slot function
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//  03/20/2008  Raymond Shen    Initial version
//
// *************************************************************************/
UINT16 fnSMenuGetSlotIndexOfSlotFn(UINT8 bKeyCode)
{
    UINT16  usSlotIndex = 0;
    UINT16  usKeyCodeIndex = 0;
    
    // scan the key code table to find the keycode index
    while((pNumKeyCodes[usKeyCodeIndex] != bKeyCode)
                    && (pNumKeyCodes[usKeyCodeIndex]!= END_KEY_GROUP))
    {
        usKeyCodeIndex ++;
    }
    
    if(pNumKeyCodes[usKeyCodeIndex] == bKeyCode)
    {
        if(usKeyCodeIndex == 0)
        {
            usKeyCodeIndex = 10;
        }
    }
    else
    {
        usKeyCodeIndex = 0;
        while((aQwertyGroup[usKeyCodeIndex] != bKeyCode)
                        && (aQwertyGroup[usKeyCodeIndex]!= END_KEY_GROUP))
        {
            usKeyCodeIndex ++;
        }
        usKeyCodeIndex += 11;
    }

    // scan the menu slot table to find the slot index
    while((pSMenuTable.pSlot[usSlotIndex].bKeyAssignment != usKeyCodeIndex)
                    && (usSlotIndex < MAX_SMENU_SLOTS))
    {
        usSlotIndex ++;
    }

    if(usSlotIndex == MAX_SMENU_SLOTS)
    {
        usSlotIndex = 0;
    }

    return usSlotIndex;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnInsertMenuSlot
//
// DESCRIPTION: 
//      Utility function that insert an entry into a specific place of the menu.
//
// INPUTS:
//      pSlot       -   pointer of the inputted Menu Slot
//      usSlotIndex -   slot index of the associated hardkey slot function
//      fDisplayNumber  -   Flag to indicate whether need to increase display number
//      fKeyAssignment  -   Flag to indicate whether need to increase key assignment
//
// RETURNS:
//      BOOL   -   TRUE if insert action is successful, otherwise FALSE
//
// WARNINGS/NOTES:
//		None
//
// MODIFICATION HISTORY:
//  10/29/2008  Raymond Shen    Initial version
//
// *************************************************************************/
BOOL fnInsertMenuSlot(S_MENU_SLOT *pSlot, 
                                                UINT16 usSlotIndex, 
                                                BOOL fDisplayNumber, 
                                                BOOL fKeyAssignment)
{
    BOOL    fStatus = FALSE;
    UINT16  i = 0;
    UINT16  usEntriesTotalNum;

    if(usSlotIndex < MAX_SMENU_SLOTS - 1)
    {
        // Get total entry number
        usEntriesTotalNum = fnGetMenuEntriesTotalNumber();

        for(i = usEntriesTotalNum; i > usSlotIndex; i --)
        {
            // Move the original entries downward
            memcpy(&(pSMenuTable.pSlot[i]), &(pSMenuTable.pSlot[i - 1]), sizeof(S_MENU_SLOT));

            // Increas display number if needed.
            if(fDisplayNumber == TRUE)
            {
                pSMenuTable.pSlot[i].wDisplayNumber ++;
            }

            // Increase key assignment if needed.
            if(fKeyAssignment == TRUE)
            {
                pSMenuTable.pSlot[i].bKeyAssignment ++;
            }
        }

        // Copy the inputted entry to the aimed place.
        memcpy(&(pSMenuTable.pSlot[usSlotIndex]), pSlot, sizeof(S_MENU_SLOT));

        // Increase total entry number by 1.
        usEntriesTotalNum ++;
        
        // Set total entry number
        fnSetMenuEntriesTotalNumber(usEntriesTotalNum);
        fStatus = TRUE;
    }

    return  fStatus;
}
