#include <stdlib.h>
#include <ctype.h>
#include "pbioext.h"
#include "global.h"
#include "nucleus.h"
#include "ossetup.h"
#include "pbos.h"
#include "cJSON.h"
#include "wsox_server.h"
#include "wsox_tx_queue.h"
#include "wsox_services.h"
#include "commontypes.h"
#include "oit.h"
#include "FdrMcpTask.h"
#include "pbio.h"
#include "cmos.h"
#include "commintf.h"
#include "cmpublic.h"
#include "hal.h"
#include "platform.h"
#include "networkmonitor.h"
#include "debugTask.h"
#include "watchdog.h"
#include "kernel/plus_common.h"
#include "dynamic_memory.h"
#include "memory_stats.h"

// STACK GAUGE LENGTH is the character length of the displayed stack usage chart
#define STACK_GAUGE_LENGTH 	16



/***********************************************************************
*
*     FUNCTION
*         printTaskStats
*
*     DESCRIPTION
*     	Print the stats for the named task
*
*     INPUTS
*		name of the task to return
*
*     OUTPUTS
*
***********************************************************************/
//void printTaskStats(NU_MEMORY_POOL 	*pnuMemPool1, char *name)
void printTaskStats(char *name)
{
	NU_TASK *pTasksArray[MAX_RUNTIME_TASKS];
	UNSIGNED numTasks;
	INT 	iPass, iPass1;
	char 	strName[MAX_NUCLEUS_NAME+1] ;
	NU_TASK *pTask 			= NULL ;
	STATUS	status ;

    NU_MEMORY_POOL 		*pnuMemPool = getMemoryPool(MEMSTATS_SYSTEMMEMORYPOOL);

    if(pnuMemPool != NULL)
    {
		if((pnuMemPool != NULL) && ((name == NULL) || (*name == 0)))
		{
			TASK_SORT_ELEMENT *arr ;

			numTasks = NU_Task_Pointers(&pTasksArray[0], MAX_RUNTIME_TASKS);

			status = NU_Allocate_Memory(pnuMemPool, (void *)&arr, sizeof(TASK_SORT_ELEMENT) * (numTasks+1), NU_SUSPEND);
			if(status == NU_SUCCESS)
			{
				getSortedTaskArray(arr, numTasks, &pTasksArray[0], NORMALIZED_REMAINING_STACK);
				for(iPass=0 ; iPass < numTasks ; iPass++)
				{
					strncpy(strName, arr[iPass].task->tc_name, MAX_NUCLEUS_NAME);
					for(iPass1 = strlen(strName) ; iPass1 < MAX_NUCLEUS_NAME ; iPass1++)
						strName[iPass1] = ' ';

					strName[MAX_NUCLEUS_NAME] = 0;
					INT32 iHW = getStackHighWaterAddress(arr[iPass].task);

					if(iPass == 0)
					{
						debugDisplayOnly("Nucleus TASK Stats:\r\n");
						debugDisplayOnly("IDX   SCHED    NAME     STACK USAGE CHART   SIZE  UNUSED  PRIORITY PREEMPT  TSLICE\r\n");
						debugDisplayOnly("----------------------------------------------------------------------------------\r\n");
					}

					debugDisplayOnly("%3ld %8ld  %s  %s %5ld (%5ld)%s  %3d     %s  %6ld\r\n",
							arr[iPass].index,
							arr[iPass].task->tc_scheduled,
							strName,
							getStackGauge(arr[iPass].task),
							arr[iPass].task->tc_stack_size,
							iHW - (INT32)arr[iPass].task->tc_stack_start,
							((iHW - (INT32)arr[iPass].task->tc_stack_start)<0) ? "!" : " ",
							arr[iPass].task->tc_priority,
							arr[iPass].task->tc_preemption ? "  Pre" : "NoPre",
							arr[iPass].task->tc_time_slice) ;
				}
			}
            debugDisplayOnly("\r\n");
            NU_Deallocate_Memory(arr) ;
		}
		else if(isdigit(name[0]))
		{
			pTask = getTaskBlockByIndex(atoi(name)) ;
			if(pTask != NULL)
			{
				strncpy(strName, pTask->tc_name, MAX_NUCLEUS_NAME);
				strName[MAX_NUCLEUS_NAME] = 0;
				printFormattedTaskStats(pTask) ;
			}
			else
				debugDisplayOnly("Unable to find task number %3d\r\n", atoi(name)) ;
		}
		else
		{
			pTask = getTaskBlockByName(name) ;
			if(pTask != NULL)
			{
				strName[MAX_NUCLEUS_NAME] = 0;
				strncpy(strName, pTask->tc_name, MAX_NUCLEUS_NAME);
				printFormattedTaskStats(pTask) ;
			}
			else
				debugDisplayOnly("Unable to find task name %s\r\n", name) ;
		}
    } // end of (pnuMemPool != NULL)
}
 
/***********************************************************************
*
*     FUNCTION
*         setTaskPriority
*
*     DESCRIPTION
*     	Sets the priority of the named task to the given priority
*
*     INPUTS
*		name of the task to update
*
*     OUTPUTS
*
***********************************************************************/
void setTaskPriority(NU_MEMORY_POOL 	*pnuMemPool, char *name, INT priority)
{
	NU_TASK *pTask 			= NULL ;

	if((name != NULL) && (*name != 0))
    {

        if (priority < 0) priority = 0;
        if (priority > 255) priority = 255;

        if(isdigit(name[0]))
        {
            pTask = getTaskBlockByIndex(atoi(name)) ;
            if(pTask != NULL)
            {
                NU_Change_Priority(pTask, priority);
                printFormattedTaskStats(pTask) ;
            }
            else
                debugDisplayOnly("Unable to find task number %3d\r\n", atoi(name)) ;
        }
        else
        {
            pTask = getTaskBlockByName(name) ;
            if(pTask != NULL)
            {
                NU_Change_Priority(pTask, priority);
                printFormattedTaskStats(pTask) ;
            }
            else
                debugDisplayOnly("Unable to find task name %s\r\n", name) ;
        }
    }

}

/***********************************************************************
*
*     FUNCTION
*         on_GetTaskStatsReq
*
*     DESCRIPTION
*     	Web Socket handler for the GetTaskStatsReq call, will format a websocket response
*     	and send it
*
*     INPUTS
*		standard input from the websocket command handler 'wsox_services.c' #310
*
*     OUTPUTS
*
***********************************************************************/
void on_GetTaskStatsReq(UINT32 handle, cJSON *root)
{
    cJSON 				*rspMsg		=  cJSON_CreateObject();
    WSOX_Queue_Entry 	entry  		=  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    NU_MEMORY_POOL 		*pnuMemPool = getMemoryPool(MEMSTATS_SYSTEMMEMORYPOOL);
    INT					iPass;
    cJSON 				*pTaskName  = NULL;
    cJSON 				*pTaskID  	= NULL;
    cJSON 				*pSort;
    char				strMsg[MS_MAX_MSGSIZE+1];
	NU_TASK 			*pTasksArray[MAX_RUNTIME_TASKS];
	UNSIGNED 			numTasks;

    // should have the system memory pool at this point
    if(pnuMemPool != NULL)
    {
		STATUS	status ;

		TASK_SORT_ELEMENT *arr ;

		numTasks = NU_Task_Pointers(&pTasksArray[0], MAX_RUNTIME_TASKS);

		status = NU_Allocate_Memory(pnuMemPool, (VOID **)&arr, sizeof(TASK_SORT_ELEMENT) * (numTasks+1), NU_SUSPEND);
		memset(arr, 0, sizeof(TASK_SORT_ELEMENT) * (numTasks+1));
		if(status == NU_SUCCESS)
		{
			cJSON_AddStringToObject(rspMsg, WS_KEY_MESSAGEID, "GetTaskStatsRsp");

			pSort = cJSON_GetObjectItem(root, WS_KEY_SORT);
			if(pSort && pSort->valuestring && *(pSort->valuestring))
			{
				for(iPass=0 ; iPass < strlen(pSort->valuestring) ; iPass++)
				{
					pSort->valuestring[iPass] = toupper(pSort->valuestring[iPass]);
				}
			}

			if(pSort && (pSort->valuestring != NULL) && (*(pSort->valuestring) != 0))
			{
				if(strncmp(pSort->valuestring, WS_SORTTYPE_REMAINING, sizeof(WS_SORTTYPE_REMAINING)-1) == 0)
				{
					getSortedTaskArray(arr, numTasks, &pTasksArray[0], REMAINING_STACK);
					cJSON_AddStringToObject(rspMsg, WS_KEY_INFO, "Sorted by Remaining Stack Space");
				}
				else if(strncmp(pSort->valuestring, WS_SORTTYPE_NORMALIZED, sizeof(WS_SORTTYPE_NORMALIZED)-1) == 0)
				{
					getSortedTaskArray(arr, numTasks, &pTasksArray[0], NORMALIZED_REMAINING_STACK);
					cJSON_AddStringToObject(rspMsg, WS_KEY_INFO, "Sorted by normalized remaining stack space (proportion of allocated space)");
				}
				else if(strncmp(pSort->valuestring, WS_SORTTYPE_USED, sizeof(WS_SORTTYPE_USED)-1) == 0)
				{
					getSortedTaskArray(arr, numTasks, &pTasksArray[0], USED_STACK);
					cJSON_AddStringToObject(rspMsg, WS_KEY_INFO, "Sorted by used stack space");

				}
				else
				{
					getSortedTaskArray(arr, numTasks, &pTasksArray[0], SORT_NONE);
					cJSON_AddStringToObject(rspMsg, WS_KEY_INFO, "No such sort mode returning Unsorted");
				}
			}
			else
			{
				getSortedTaskArray(arr, numTasks, &pTasksArray[0], SORT_NONE);
				cJSON_AddStringToObject(rspMsg, WS_KEY_INFO, "Unsorted");
			}

			cJSON *pArr = cJSON_CreateArray();
			if(pArr != NULL)
			{
				pTaskName 	= cJSON_GetObjectItem(root, WS_KEY_NAME);
				pTaskID 	= cJSON_GetObjectItem(root, WS_KEY_TASKNUMBER);
				if((pTaskName == NULL) && (pTaskID == NULL))
				{
					// no name provided so dump all tasks...
					for(iPass=0 ; (iPass < numTasks) ; iPass++)
					{
						strncpy(arr[iPass].name, arr[iPass].task->tc_name, MAX_NUCLEUS_NAME);
						arr[iPass].name[MAX_NUCLEUS_NAME] = 0;
						// create array member
						cJSON  *eleMsg =  cJSON_ObjectFromTaskElement(&(arr[iPass]));
						cJSON_AddNumberToObject(eleMsg, "#Sort", arr[iPass].sort);
						if(eleMsg != NULL)
							cJSON_AddItemToArray(pArr, eleMsg);
					}
					cJSON_AddItemToObject(rspMsg, WS_KEY_TASKSTITLE, pArr) ;
				}
				else if(pTaskName && pTaskName->valuestring)
				{
					// If we have an explicit name request we search for that name and place its entry into the array.
					BOOL bFound = FALSE ;
					for(iPass=0 ; (iPass < numTasks) ; iPass++)
					{
						strncpy(arr[iPass].name, arr[iPass].task->tc_name, MAX_NUCLEUS_NAME);
						arr[iPass].name[MAX_NUCLEUS_NAME] = 0;
						if((strlen(arr[iPass].name) == strlen(pTaskName->valuestring)) &&
						   (strncmp(arr[iPass].name, pTaskName->valuestring, strlen(pTaskName->valuestring)) == 0) )
						{
							bFound = TRUE ;
							cJSON  *eleMsg =  cJSON_ObjectFromTaskElement(&(arr[iPass]));
							if(eleMsg != NULL)
								cJSON_AddItemToArray(pArr, eleMsg);
						}
					}
					if(bFound == FALSE)
					{
						snprintf(strMsg, MS_MAX_MSGSIZE, "Task name %s not found in list", pTaskName->valuestring) ;
						cJSON_AddStringToObject(rspMsg, WS_KEY_ERROR, strMsg);
					}
					else
					{
						cJSON_AddItemToObject(rspMsg, WS_KEY_TASKSTITLE, pArr) ;
					}
				}
				else if(pTaskID)
				{
					// If we have an explicit name request we search for that name and place its entry into the array.
					BOOL bFound = FALSE ;
					for(iPass=0 ; (iPass < numTasks) ; iPass++)
					{
						// handle request even if user passes in a string representation of the number...
						if(pTaskID->valuestring)
						{
							pTaskID->valueint = atoi(pTaskID->valuestring);
						}
						strncpy(arr[iPass].name, arr[iPass].task->tc_name, MAX_NUCLEUS_NAME);
						arr[iPass].name[MAX_NUCLEUS_NAME] = 0;
						if(arr[iPass].index == pTaskID->valueint)
						{
							bFound = TRUE ;
							cJSON  *eleMsg =  cJSON_ObjectFromTaskElement(&(arr[iPass]));
							if(eleMsg != NULL)
								cJSON_AddItemToArray(pArr, eleMsg);
						}
					}
					if(bFound == FALSE)
					{
						snprintf(strMsg, MS_MAX_MSGSIZE, "Task ID %d not found in list", pTaskName->valueint) ;
						cJSON_AddStringToObject(rspMsg, WS_KEY_ERROR, strMsg);
					}
					else
					{
						cJSON_AddItemToObject(rspMsg, WS_KEY_TASKSTITLE, pArr) ;
					}
				}

			} // end of if(pArr != NULL)
			else
			{
				cJSON_AddStringToObject(rspMsg, WS_KEY_ERROR, "Error creating elements for JSON message [on_GetTaskStatsReq]");
			}

			addEntryToTxQueue(&entry, root, "  memory_stats: Added GetTaskStatsRsp to tx queue. status=%d\r\n");
			//
			NU_Deallocate_Memory(arr) ;
		}
		else
		{
			dbgTrace(DBG_LVL_INFO, "Unable to allocate %lu bytes of memory in websocket helper on_GetTaskStatsReq", sizeof(TASK_SORT_ELEMENT) * (numTasks+1));
		}
    }
    else
    {
    	// unable to get the memory pool
    	dbgTrace(DBG_LVL_INFO, "Unable to access memory pool in websocket handler on_GetTaskStatsReq");
    }
}

/***********************************************************************
*
*     FUNCTION
*         cJSON_ObjectFromTaskElement
*
*     DESCRIPTION
*     	Create a JSON element from the supplied task array element
*
*     INPUTS
*		TASK_SORT_ELEMENT *		an element from a task array
*
*     OUTPUTS
*     	cJSON *					a cJSON instance with the array data stored in it.
*
***********************************************************************/
cJSON *cJSON_ObjectFromTaskElement(TASK_SORT_ELEMENT *ele)
{
    cJSON  *eleMsg =  cJSON_CreateObject();
    if(eleMsg != NULL)
    {
    	// possible problem here with special characters used
    	// '[' ']' '|' and '*'
    	//    			    	cJSON_AddStringToObject(eleMsg, "StackGauge", 	getStackGauge(arr[iPass].task));
    	///
    	cJSON_AddNumberToObject(eleMsg, WS_KEY_TASKNUMBER, 	(ele->index));
    	cJSON_AddNumberToObject(eleMsg, WS_KEY_SCHEDULE, 	(ele->task->tc_scheduled));
    	cJSON_AddStringToObject(eleMsg, WS_KEY_NAME, 		ele->name);
    	cJSON_AddNumberToObject(eleMsg, WS_KEY_STACKSIZE, 	(ele->task->tc_stack_size));
    	cJSON_AddNumberToObject(eleMsg, WS_KEY_UNUSEDSTACK, (INT32)((UINT32)ele->highWater - (UINT32)ele->task->tc_stack_start));
    }

	return eleMsg ;
}

/***********************************************************************
*
*     FUNCTION
*         printHISRStats
*
*     DESCRIPTION
*     	Print the stats for the named HISR
*
*     INPUTS
*		name of the task to return
*
*     OUTPUTS
*
***********************************************************************/
//void printHISRStats(NU_MEMORY_POOL 	*pnuMemPool, char *name)
void printHISRStats(char *name)
{
	NU_HISR *pHISRsArray[MAX_RUNTIME_HISRS];
	UNSIGNED numHISRs;
	INT 	iPass, iPass1;
	char 	strName[MAX_NUCLEUS_NAME+1] ;
	NU_HISR *pHISR 			= NULL ;
	STATUS	status ;

    NU_MEMORY_POOL 		*pnuMemPool = getMemoryPool(MEMSTATS_SYSTEMMEMORYPOOL);
    if(pnuMemPool != NULL)
    {
		if((name == NULL) || (*name == 0))
		{
			HISR_SORT_ELEMENT *arr ;

			numHISRs = NU_HISR_Pointers(&pHISRsArray[0], MAX_RUNTIME_HISRS);
			status = NU_Allocate_Memory(pnuMemPool, (VOID *)&arr, sizeof(HISR_SORT_ELEMENT) * (numHISRs+1), NU_SUSPEND);
			if(status == NU_SUCCESS)
			{
				getSortedHISRArray(arr, numHISRs, &pHISRsArray[0], NORMALIZED_REMAINING_STACK);
				for(iPass=0 ; iPass < numHISRs ; iPass++)
				{
					strncpy(strName, arr[iPass].hisr->tc_name, MAX_NUCLEUS_NAME);
					for(iPass1 = strlen(strName) ; iPass1 < MAX_NUCLEUS_NAME ; iPass1++)
						strName[iPass1] = ' ';

					strName[MAX_NUCLEUS_NAME] = 0;
					INT32 iHW = getHISRStackHighWaterAddress(arr[iPass].hisr);

					if(iPass == 0)
					{
						debugDisplayOnly("Nucleus HISR Stats:\r\n");
						debugDisplayOnly("IDX   SCHED    NAME     STACK USAGE CHART   SIZE  UNUSED Priority  Error*\r\n");
						debugDisplayOnly("-------------------------------------------------------------------------\r\n");
					}

					debugDisplayOnly("%3ld %8ld  %s  %s %5ld (%5ld)%s %4d  %08X\r\n",
							arr[iPass].index,
							arr[iPass].hisr->tc_scheduled,
							strName,
							getHISRStackGauge(arr[iPass].hisr),
							arr[iPass].hisr->tc_stack_size,
							iHW - (INT32)arr[iPass].hisr->tc_stack_start,
							((iHW - (INT32)arr[iPass].hisr->tc_stack_start)<0) ? "!" : "",
							arr[iPass].hisr->tc_priority,
							(UINT)arr[iPass].hisr->tc_error) ;
				}
				debugDisplayOnly("\r\n");
				NU_Deallocate_Memory(arr) ;
			}
		}
		else if(isdigit(name[0]))
		{
			pHISR = getHISRBlockByIndex(atoi(name)) ;
			if(pHISR != NULL)
			{
				strncpy(strName, pHISR->tc_name, MAX_NUCLEUS_NAME);
				strName[MAX_NUCLEUS_NAME] = 0;
				printFormattedHISRStats(pHISR) ;
			}
			else
				debugDisplayOnly("Unable to find task number %3d\r\n", atoi(name)) ;
		}
		else
		{
			pHISR = getHISRBlockByName(name) ;
			if(pHISR != NULL)
			{
				strName[MAX_NUCLEUS_NAME] = 0;
				strncpy(strName, pHISR->tc_name, MAX_NUCLEUS_NAME);
				printFormattedHISRStats(pHISR) ;
			}
			else
				debugDisplayOnly("Unable to find task name %s\r\n", name) ;
		}
    } // end of if (pnuMemPool != NULL)

}

/***********************************************************************
*
*     FUNCTION
*         on_GetHISRStatsReq
*
*     DESCRIPTION
*     	Web Socket handler for the GetHISRStatsReq call, will format a websocket response
*     	and send it.
*     	Expected message formate:
*     		MsgID - "GetHISR_StatsReq"
*     		Sort  - optional if not included "NORMALIZED" is assumed
*     					"REMAINING"	= sort by remaining stack space (bytes)
*     					"NORMALIZED" = sort by proportion of remaining stack space based on allocated amount
*     					"USED"		= sor by the amount of used stack (bytes)
*
*     INPUTS
*		standard input from the websocket command handler 'wsox_services.c' #310
*
*     OUTPUTS
*
***********************************************************************/
void on_GetHISRStatsReq(UINT32 handle, cJSON *root)
{
    cJSON 				*rspMsg		=  cJSON_CreateObject();
    WSOX_Queue_Entry 	entry  		=  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    NU_MEMORY_POOL 		*pnuMemPool = getMemoryPool(MEMSTATS_SYSTEMMEMORYPOOL);
    INT					iPass;
    cJSON 				*pHISRName;
    cJSON 				*pHISRID;
    cJSON				*pSort ;
    char				strMsg[MS_MAX_MSGSIZE+1];
	NU_HISR 			*pHISRsArray[MAX_RUNTIME_HISRS];
	UNSIGNED 			numHISRs;
    // should have the system memory pool at this point
    if((pnuMemPool != NULL) || !rspMsg)
    {

		cJSON_AddStringToObject(rspMsg, WS_KEY_MESSAGEID, "GetHISRStatsRsp");
    	/////////////////////////////////////////////////
		STATUS	status ;

		HISR_SORT_ELEMENT *arr ;

		numHISRs = NU_HISR_Pointers(&pHISRsArray[0], MAX_RUNTIME_HISRS);

		status = NU_Allocate_Memory(pnuMemPool, (VOID *)&arr, sizeof(HISR_SORT_ELEMENT) * (numHISRs+1), NU_SUSPEND);
		if(status == NU_SUCCESS)
		{
			pSort = cJSON_GetObjectItem(root, WS_KEY_SORT);
			if(pSort && (pSort->valuestring != NULL) && (*(pSort->valuestring) != 0))
			{
				if(strncmp(pSort->valuestring, WS_SORTTYPE_REMAINING, sizeof(WS_SORTTYPE_REMAINING)-1) == 0)
				{
					getSortedHISRArray(arr, numHISRs, &pHISRsArray[0], REMAINING_STACK);
					cJSON_AddStringToObject(rspMsg, WS_KEY_INFO, "Sorted by Remaining Stack Space");
				}
				else if(strncmp(pSort->valuestring, WS_SORTTYPE_NORMALIZED, sizeof(WS_SORTTYPE_NORMALIZED)-1) == 0)
				{
					getSortedHISRArray(arr, numHISRs, &pHISRsArray[0], NORMALIZED_REMAINING_STACK);
					cJSON_AddStringToObject(rspMsg, WS_KEY_INFO, "Sorted by normalized remaining stack space (proportion of allocated space)");
				}
				else if(strncmp(pSort->valuestring, WS_SORTTYPE_USED, sizeof(WS_SORTTYPE_USED)-1) == 0)
				{
					getSortedHISRArray(arr, numHISRs, &pHISRsArray[0], USED_STACK);
					cJSON_AddStringToObject(rspMsg, WS_KEY_INFO, "Sorted by used stack space");
				}
				else
				{
					getSortedHISRArray(arr, numHISRs, &pHISRsArray[0], SORT_NONE);
					cJSON_AddStringToObject(rspMsg, WS_KEY_INFO, "Unsorted");
				}

			}
			else
			{
				getSortedHISRArray(arr, numHISRs, &pHISRsArray[0], SORT_NONE);
				cJSON_AddStringToObject(rspMsg, WS_KEY_INFO, "Unsorted");
			}

			cJSON *pArr = cJSON_CreateArray();
			if(pArr != NULL)
			{
				pHISRName 	= cJSON_GetObjectItem(root, WS_KEY_NAME);
				pHISRID 	= cJSON_GetObjectItem(root, WS_KEY_TASKNUMBER);
				if((pHISRName == NULL) && (pHISRID == NULL))
				{
					// no name provided so dump all HISRs...
					for(iPass=0 ; (iPass < numHISRs) ; iPass++)
					{
						strncpy(arr[iPass].name, arr[iPass].hisr->tc_name, MAX_NUCLEUS_NAME);
						arr[iPass].name[MAX_NUCLEUS_NAME] = 0;
						// create array member
						cJSON  *eleMsg =  cJSON_ObjectFromHISRElement(&(arr[iPass]));
						if(eleMsg != NULL)
							cJSON_AddItemToArray(pArr, eleMsg);
					}
					cJSON_AddItemToObject(rspMsg, WS_KEY_TASKSTITLE, pArr) ;
				}
				else if((pHISRName != NULL) && (pHISRName->valuestring != NULL))
				{
					// If we have an explicit name request we search for that name and place its entry into the array.
					BOOL bFound = FALSE ;
					for(iPass=0 ; (iPass < numHISRs) ; iPass++)
					{
						strncpy(arr[iPass].name, arr[iPass].hisr->tc_name, MAX_NUCLEUS_NAME);
						arr[iPass].name[MAX_NUCLEUS_NAME] = 0;
						if((strlen(arr[iPass].name) == strlen(pHISRName->valuestring)) &&
						   (strncmp(arr[iPass].name, pHISRName->valuestring, strlen(pHISRName->valuestring)) == 0) )
						{
							bFound = TRUE ;
							cJSON  *eleMsg =  cJSON_ObjectFromHISRElement(&(arr[iPass]));
							if(eleMsg != NULL)
								cJSON_AddItemToArray(pArr, eleMsg);
						}
					}
					if(bFound == FALSE)
					{
						snprintf(strMsg, MS_MAX_MSGSIZE, "Task name %s not found in list", pHISRName->valuestring) ;
						cJSON_AddStringToObject(rspMsg, WS_KEY_ERROR, strMsg);
					}
					else
					{
						cJSON_AddItemToObject(rspMsg, WS_KEY_TASKSTITLE, pArr) ;
					}
				}
				else if(pHISRID != NULL)
				{
					// If we have an explicit name request we search for that name and place its entry into the array.
					BOOL bFound = FALSE ;
					for(iPass=0 ; (iPass < numHISRs) ; iPass++)
					{
						//handle this even if the user passes in the ID as a string
						if(pHISRID->valuestring)
						{
							pHISRID->valueint = atoi(pHISRID->valuestring);
						}

						strncpy(arr[iPass].name, arr[iPass].hisr->tc_name, MAX_NUCLEUS_NAME);
						arr[iPass].name[MAX_NUCLEUS_NAME] = 0;

						if(pHISRID->valueint == arr[iPass].index)
						{
							bFound = TRUE ;
							cJSON  *eleMsg =  cJSON_ObjectFromHISRElement(&(arr[iPass]));
							if(eleMsg != NULL)
								cJSON_AddItemToArray(pArr, eleMsg);
						}
					}
					if(bFound == FALSE)
					{
						snprintf(strMsg, MS_MAX_MSGSIZE, "Task name %s not found in list", pHISRName->valuestring) ;
						cJSON_AddStringToObject(rspMsg, WS_KEY_ERROR, strMsg);
					}
					else
					{
						cJSON_AddItemToObject(rspMsg, WS_KEY_TASKSTITLE, pArr) ;
					}
				}
			} // end of if(pArr != NULL)
			else
			{
				cJSON_AddStringToObject(rspMsg, WS_KEY_ERROR, "Error creating elements for JSON message [on_GetTaskStatsReq]");
			}

			addEntryToTxQueue(&entry, root, "  memory_stats: Added GetHISRStatsRsp to tx queue. status=%d\r\n");
			//
			NU_Deallocate_Memory(arr) ;
		}
		else
		{
			dbgTrace(DBG_LVL_INFO, "Unable to allocate %lu bytes of memory in websocket helper GetHISRStatsRsp", sizeof(TASK_SORT_ELEMENT) * (numHISRs+1));
		}
		////////////////////////////////////////////////
    }
    else
    {
    	// unable to get the memory pool
    	dbgTrace(DBG_LVL_INFO, "Unable to access memory pool in websocket handler on_GetTaskStatsReq");
    }
}


/***********************************************************************
*
*     FUNCTION
*         cJSON_ObjectFromHISRElement
*
*     DESCRIPTION
*     	Create a JSON element from the supplied HISR array element
*
*     INPUTS
*		TASK_SORT_ELEMENT *		an element from a HISR array
*
*     OUTPUTS
*     	cJSON *					a cJSON instance with the array data stored in it.
*
***********************************************************************/
cJSON *cJSON_ObjectFromHISRElement(HISR_SORT_ELEMENT *ele)
{
    cJSON  *eleMsg =  cJSON_CreateObject();
    if(eleMsg != NULL)
    {
    	// possible problem here with special characters used
    	// '[' ']' '|' and '*'
    	//    			    	cJSON_AddStringToObject(eleMsg, "StackGauge", 	getStackGauge(arr[iPass].task));
    	///
    	cJSON_AddNumberToObject(eleMsg, WS_KEY_TASKNUMBER, 	(ele->index));
    	cJSON_AddNumberToObject(eleMsg, WS_KEY_SCHEDULE, 	(ele->hisr->tc_scheduled));
    	cJSON_AddStringToObject(eleMsg, WS_KEY_NAME, 		ele->name);
    	cJSON_AddNumberToObject(eleMsg, WS_KEY_STACKSIZE, 	(ele->hisr->tc_stack_size));
    	cJSON_AddNumberToObject(eleMsg, WS_KEY_UNUSEDSTACK, (INT32)((UINT32)ele->highWater - (UINT32)ele->hisr->tc_stack_start));
    }

	return eleMsg ;
}





/***********************************************************************
*
*     FUNCTION
*         getSortedHISRArray
*
*     DESCRIPTION
*      fill a supplied array with the sorted list of HISR tasks, sorte by the supplied mode enum
*
*     INPUTS
*		HISR_SORT_ELEMENT	pre-allocated array to place result in
*		UINT				max number of elements allowed in the array
*		ENUM_TASKSORTMODE	sorting rule
*									REMAINING_STACK					- amount of stack space remaining
*									NORMALIZED_REMAINING_STACK		- amount of stack space remaining normalized to graphical table size
*									USED_STACK						- amount of stack space used
*									default							- stack pointer value.
*
*     OUTPUTS
*
***********************************************************************/
void getSortedHISRArray(HISR_SORT_ELEMENT *arrElements, UINT uCount, NU_HISR **pointer_list, ENUM_TASKSORTMODE mode)
{
	NU_HISR *pCurrentHISR = NULL ;
	UINT	uPass = 0 ;
	UINT	uInner;
	UINT	uMark ;
	UINT32	uHighWater = 0;

	memset(arrElements, 0, sizeof(arrElements) * uCount) ;
	for(uPass=0 ; uPass < uCount ; uPass++)
		arrElements[uPass].sort = 0xFFFFFFFF;

	for(uPass=0; uPass < uCount ; uPass++)
	{
		UINT32 uHW ;

		pCurrentHISR = pointer_list[uPass];
		uHighWater = getHISRStackHighWaterAddress(pCurrentHISR);

		switch(mode)
		{
			case REMAINING_STACK:
				// base sorting on total number of bytes left in stack
				uHW = uHighWater - (UINT32)pCurrentHISR->tc_stack_start;
				break ;

			case NORMALIZED_REMAINING_STACK:
				{
					// base sort on a normalized remaining stack value
					UINT32 uNorm = ((UINT32)pCurrentHISR->tc_stack_end - (UINT32)pCurrentHISR->tc_stack_start)/ STACK_GAUGE_LENGTH ;
					uHW = (uHighWater - (UINT32)pCurrentHISR->tc_stack_start)/ uNorm ;
				}
				break ;

			case USED_STACK:
				uHW =  (UINT32)pCurrentHISR->tc_stack_end - uHighWater;
				break ;

			default:
				// base sorting on the high water address value
				uHW = 0 ;
				break ;
		}
		// move to position where the entry has the same or higher remaining stack space.
		for(uInner = 0 ; (uInner < uPass) && (arrElements[uInner].sort < uHW) ; uInner++)
		{ /* no op */}

		uMark = uInner ;
		for( uInner = uPass ; (uInner > uMark); uInner--)
		{
			arrElements[uInner] = arrElements[uInner-1];
		}
		arrElements[uMark].sort 	 = uHW ;
		arrElements[uMark].hisr 	 = pCurrentHISR ;
		arrElements[uMark].index 	 = uPass ;
		arrElements[uMark].highWater = uHighWater;
	}
}



/***********************************************************************
*
*     FUNCTION
*         getSortedTaskArray
*
*     DESCRIPTION
*     	return an array of all the tasks running sorted by the requested mode
*
*     INPUTS
*		TASK_SORT_ELEMENTS *	array of TASK SORT ELEMENTS, this will be emptied and the results place within it
*		UINT					size (in elements) of the supplied array
*		ENUM_TASKSORTMODE		sorting parameter to use:
*									REMAINING_STACK					- amount of stack space remaining
*									NORMALIZED_REMAINING_STACK		- amount of stack space remaining normalized to graphical table size
*									USED_STACK						- amount of stack space used
*									default							- stack pointer value.
*
*     OUTPUTS
*     	pointer to NU_TASK structure requested or NULL
*
***********************************************************************/
void getSortedTaskArray(TASK_SORT_ELEMENT *arrElements, UINT uCount, NU_TASK **pointer_list, ENUM_TASKSORTMODE mode)
{
	NU_TASK *pCurrentTask = NULL ;
	UINT	uPass = 0 ;
	UINT	uInner;
	UINT	uMark ;
	UINT32	uHighWater = 0;

	memset(arrElements, 0, sizeof(arrElements) * uCount) ;
	for(uPass=0 ; uPass < uCount ; uPass++)
		arrElements[uPass].sort = 0xFFFFFFFF;

	for(uPass=0; uPass < uCount ; uPass++)
	{
		UINT32 uHW ;

		pCurrentTask = pointer_list[uPass];
		uHighWater = getStackHighWaterAddress(pCurrentTask);

		switch(mode)
		{
			case REMAINING_STACK:
				// base sorting on total number of bytes left in stack
				uHW = uHighWater - (UINT32)pCurrentTask->tc_stack_start;
				break ;

			case NORMALIZED_REMAINING_STACK:
				{
					// base sort on a normalized remaining stack value
					UINT32 uNorm = ((UINT32)pCurrentTask->tc_stack_end - (UINT32)pCurrentTask->tc_stack_start)/ STACK_GAUGE_LENGTH ;
					uHW = (uHighWater - (UINT32)pCurrentTask->tc_stack_start)/ uNorm ;
				}
				break ;

			case USED_STACK:
				uHW =  (UINT32)pCurrentTask->tc_stack_end - uHighWater;
				break ;

			default:
				// set sorting value to 0 for all tasks
				uHW = 0 ;
				break ;
		}
		// move to position where the entry has the same or higher remaining stack space.
		for(uInner = 0 ; (uInner < uPass) && (arrElements[uInner].sort < uHW) ; uInner++)
		{ /* no op */}

		uMark = uInner ;
		for( uInner = uPass ; (uInner > uMark); uInner--)
		{
			arrElements[uInner] = arrElements[uInner-1];
		}
		arrElements[uMark].sort 	 = uHW ;
		arrElements[uMark].task 	 = pCurrentTask ;
		arrElements[uMark].index 	 = uPass ;
		arrElements[uMark].highWater = uHighWater;
	}
}

/***********************************************************************
*
*     FUNCTION
*         getTaskBlockByIndex
*
*     DESCRIPTION
*     	return the task with the supplied index in the global list
*
*     INPUTS
*		index of task to return
*
*     OUTPUTS
*     	pointer to NU_TASK structure requested or NULL
*
***********************************************************************/
NU_TASK *getTaskBlockByIndex(int iIndex)
{
	NU_TASK *pTasksArray[MAX_RUNTIME_TASKS];
	UNSIGNED numTasks;
	NU_TASK *pRes = NULL ;

	numTasks = NU_Task_Pointers(&pTasksArray[0], MAX_RUNTIME_TASKS);

	if(iIndex < numTasks)
		pRes = pTasksArray[iIndex] ;

	return pRes ;
}

/***********************************************************************
*
*     FUNCTION
*         getHISRBlockByIndex
*
*     DESCRIPTION
*     	return the HISR with the supplied index in the global list
*
*     INPUTS
*		index of HISR to return
*
*     OUTPUTS
*     	pointer to NU_HISR structure requested or NULL
*
***********************************************************************/
NU_HISR *getHISRBlockByIndex(int iIndex)
{
	NU_HISR *pHISRsArray[MAX_RUNTIME_HISRS];
	UNSIGNED numHISRs;
	NU_HISR *pRes = NULL ;

	numHISRs = NU_HISR_Pointers(&pHISRsArray[0], MAX_RUNTIME_HISRS);

	if(iIndex < numHISRs)
		pRes = pHISRsArray[iIndex] ;

	return pRes ;

}


/***********************************************************************
*
*     FUNCTION
*         getTaskBlockByName
*
*     DESCRIPTION
*     	return the task block matching the name supplied
*
*     INPUTS
*		name of the task to return
*
*     OUTPUTS
*     	Task block for the name supplied or NULL
*
***********************************************************************/
NU_TASK *getTaskBlockByName(char *name)
{
	NU_TASK *pTasksArray[MAX_RUNTIME_TASKS];
	UNSIGNED numTasks;
	NU_TASK *pRes;
	INT iPass ;

	numTasks = NU_Task_Pointers(&pTasksArray[0], MAX_RUNTIME_TASKS);
	for(iPass=0; iPass < numTasks; iPass++)
	{
		pRes = pTasksArray[iPass];
		if (strncmp(name, pRes->tc_name, sizeof(pRes->tc_name)) == 0)
			 break;
	}

	if(iPass >= numTasks)
		pRes = NULL ;

	return pRes ;
}


/***********************************************************************
*
*     FUNCTION
*         getHISRBlockByName
*
*     DESCRIPTION
*     	return the HISR block matching the name supplied
*
*     INPUTS
*		name of the HISR to return
*
*     OUTPUTS
*     	HISR block for the name supplied or NULL
*
***********************************************************************/
NU_HISR *getHISRBlockByName(char *name)
{
	NU_HISR *pHISRsArray[MAX_RUNTIME_HISRS];
	UNSIGNED numHISRs;
	NU_HISR *pRes;
	INT iPass ;

	numHISRs = NU_HISR_Pointers(&pHISRsArray[0], MAX_RUNTIME_HISRS);
	for(iPass=0; iPass < numHISRs; iPass++)
	{
		pRes = pHISRsArray[iPass];
		if (strncmp(name, pRes->tc_name, sizeof(pRes->tc_name)) == 0)
			 break;
	}

	if(iPass >= numHISRs)
		pRes = NULL ;

	return pRes ;

}

/***********************************************************************
*
*     FUNCTION
*         printTaskStats
*
*     DESCRIPTION
*     	produce a formatted output of the Tasks state
*
*     INPUTS
*
*     OUTPUTS
*
***********************************************************************/
void printFormattedTaskStats(NU_TASK *pTask)
{
	size_t szPass;
	char strName[MAX_NUCLEUS_NAME+1] ;
	if(pTask != NULL)
	{
		strncpy(strName, pTask->tc_name, MAX_NUCLEUS_NAME) ;
		for(szPass = strlen(strName) ; szPass < MAX_NUCLEUS_NAME ; szPass++)
			strName[szPass] = ' ';
		strName[MAX_NUCLEUS_NAME] = 0;

		INT32 iStackUsed = (INT32)pTask->tc_stack_end - (INT32)getStackHighWaterAddress(pTask) ;

		debugDisplayOnly("[%08lX] : %4d : %s : STACK HI %08lX - SP %08lX - LO %08lX  SZ %ld  Max-used %ld Priority %d\r\n",
				pTask->tc_id,
				pTask->tc_scheduled,
				strName,
				pTask->tc_stack_start,
				pTask->tc_stack_pointer,
				pTask->tc_stack_end,
				pTask->tc_stack_size,
				(INT32)(iStackUsed >= 0) ? iStackUsed : pTask->tc_stack_size,
				pTask->tc_priority
				) ;
	}
}

/***********************************************************************
*
*     FUNCTION
*         printFormattedHISRStats
*
*     DESCRIPTION
*     	produce a formatted output of the HISR state
*
*     INPUTS
*
*     OUTPUTS
*
***********************************************************************/
void printFormattedHISRStats(NU_HISR *pHISR)
{
	size_t szPass;
	char strName[MAX_NUCLEUS_NAME+1] ;
	if(pHISR != NULL)
	{
		strncpy(strName, pHISR->tc_name, MAX_NUCLEUS_NAME) ;
		for(szPass = strlen(strName) ; szPass < MAX_NUCLEUS_NAME ; szPass++)
			strName[szPass] = ' ';
		strName[MAX_NUCLEUS_NAME] = 0;

		INT32 iStackUsed = (INT32)pHISR->tc_stack_end - (INT32)getHISRStackHighWaterAddress(pHISR) ;

		debugDisplayOnly("[%08lX] : %4d : %s : STACK HI %08lX - SP %08lX - LO %08lX  SZ %ld  Max-used %ld\r\n",
				pHISR->tc_id,
				pHISR->tc_scheduled,
				strName,
				pHISR->tc_stack_start,
				pHISR->tc_stack_pointer,
				pHISR->tc_stack_end,
				pHISR->tc_stack_size,
				(INT32)(iStackUsed >= 0) ? iStackUsed : pHISR->tc_stack_size
				) ;
	}
}

/***********************************************************************
*
*     FUNCTION
*         getStackHighWaterAddress
*
*     DESCRIPTION
*     	Scan the stack checking the fill pattern and report the first byte found
*     	not matching that value.
*
*     INPUTS
*		NU_TASK *		pointer to the task to check
*     OUTPUTS
*     	UINT32			address of the first non fill pattern value
*
***********************************************************************/
UINT32 getStackHighWaterAddress(NU_TASK *pTask)
{
	UINT8 *pPos = (UINT8 *)pTask->tc_stack_start ;
	while(*pPos == NU_STACK_FILL_PATTERN) pPos++;
	return (UINT32)pPos ;
}

/***********************************************************************
*
*     FUNCTION
*         getHISRStackHighWaterAddress
*
*     DESCRIPTION
*     	Scan the stack checking the fill pattern and report the first byte found
*     	not matching that value.
*
*     INPUTS
*		NU_HISR *		pointer to the HISR to check
*     OUTPUTS
*     	UINT32			address of the first non fill pattern value
*
***********************************************************************/
UINT32 getHISRStackHighWaterAddress(NU_HISR *pHISR)
{
	UINT8 *pPos = (UINT8 *)pHISR->tc_stack_start ;
	while(*pPos == NU_STACK_FILL_PATTERN) pPos++;
	return (UINT32)pPos ;
}

/***********************************************************************
*
*     FUNCTION
*         getStackGauge
*
*     DESCRIPTION
*     	produce a formatted 'fuel'gauge for the stack parameters along the lines of
*     	 sp limit -> [     |   *       ] <- sp start
*     	 	High watermark ^   ^ current sp
*
*     INPUTS
*     NU_TASK *		pointer to task control block
*
*     OUTPUTS
*
***********************************************************************/
static char strGauge[STACK_GAUGE_LENGTH+4] ;
char *getStackGauge(NU_TASK *pTask)
{
	UINT32 	uIncrement 		= (UINT32)(pTask->tc_stack_end - pTask->tc_stack_start)/STACK_GAUGE_LENGTH;
	UINT32 	uSP 			= (UINT32)(pTask->tc_stack_pointer - pTask->tc_stack_start)/uIncrement ;
	UINT32	uHW				= (getStackHighWaterAddress(pTask) - (UINT32)pTask->tc_stack_start)/uIncrement ;
	char	*pPtr			= strGauge ;
	UINT32 	uPass ;

	*pPtr++ = '[' ;

	for(uPass = 0 ; uPass < STACK_GAUGE_LENGTH ; uPass++, pPtr++)
	{
		if(uPass == uHW)
			*pPtr = '|';
		else if(uPass == uSP)
			*pPtr = '*';
		else
			*pPtr = ' ';
	}

	*pPtr++ = ']';
	*pPtr = 0;

	return strGauge ;
}

/***********************************************************************
*
*     FUNCTION
*         getHISRStackGauge
*
*     DESCRIPTION
*     	produce a formatted 'fuel'gauge for the stack parameters along the lines of
*     	 sp limit -> [        *       ] <- sp start
*     	 				      ^ current sp
*
*     INPUTS
*     NU_HISR *		pointer to HISR control block
*
*     OUTPUTS
*
***********************************************************************/
static char strHISRGauge[STACK_GAUGE_LENGTH+4] ;
char *getHISRStackGauge(NU_HISR *pHISR)
{
	UINT32 	uIncrement 		= (UINT32)(pHISR->tc_stack_end - pHISR->tc_stack_start)/STACK_GAUGE_LENGTH;
	UINT32 	uSP 			= (UINT32)(pHISR->tc_stack_pointer - pHISR->tc_stack_start)/uIncrement ;
	UINT32	uHW				= (getHISRStackHighWaterAddress(pHISR) - (UINT32)pHISR->tc_stack_start)/uIncrement ;
	char	*pPtr			= strHISRGauge ;
	UINT32 	uPass ;

	*pPtr++ = '[' ;

	for(uPass = 0 ; uPass < STACK_GAUGE_LENGTH ; uPass++, pPtr++)
	{
		if(uPass == uHW)
			*pPtr = '|';
		else if(uPass == uSP)
			*pPtr = '*';
		else
			*pPtr = ' ';
	}

	*pPtr++ = ']';
	*pPtr = 0;

	return strHISRGauge ;
}


/***********************************************************************
*
*     FUNCTION
*         printMemoryPoolChain
*
*     DESCRIPTION
*     	walk the nucleus list of memory pools and dub stats regarding amount of space allocated, how much is available
*     	and give some idea of the fragmentation of the pools.
*
*     INPUTS
*  	      NU_MEMORY_POOL *		a nucleus memory pool, used as the starting point to walk the list.
*
*     OUTPUTS
*
***********************************************************************/
void printMemoryPoolStats(NU_MEMORY_POOL *pnuMemPool, char *pArgs)
{
	if(pnuMemPool)
	{
		UINT			uPass;
		CS_NODE			*pNode 		= (CS_NODE *)pnuMemPool;
		CS_NODE			*pNodeStart = pNode ;
		char			name[12];
		MEM_BLOCK_STATS memStats ;

		pArgs = getString(pArgs, name, sizeof(name)-1);

		if(*name != 0)
		{
			// see if the user passed in a name
			NU_MEMORY_POOL *pByName =	getMemoryPool(name);
			if(pByName != NULL)
			{
				 printUsedBlockDetails(pByName);
			}
			else
			{
				debugDisplayOnly("Unable to find a memory pool named [%s]\r\n", name) ;
			}
		}
		else
		{
			debugDisplayOnly("Pool Name  Total Size :  Available           Allocated                     Free\r\n");
			debugDisplayOnly("--------------------------------------------------------------------------------------\r\n");
			do
			{
				NU_MEMORY_POOL *pMP = (NU_MEMORY_POOL *)pNode;
				strncpy(name, pMP->dm_name, 8) ;
				for(uPass=strlen(pMP->dm_name) ; uPass < 8 ; uPass++)
					name[uPass] = ' ';
				name[8] = 0 ;
				countMemoryBlocks(pMP, &memStats);
				debugDisplayOnly("  %s [%7.3f MB:%8.4f MB]  %5d blks (%8.4f MB)  -  %5d blks (%8.4f)\r\n",
						name,
						((double)pMP->dm_pool_size)/(double)(1024*1024),
						((double)pMP->dm_available)/(double)(1024*1024),
						memStats.blocksMem, // - memStats.blocksFree,
						((double)memStats.accumMem)/(double)(1024*1024),
						memStats.blocksFree,
						((double)memStats.accumFree)/(double)(1024*1024)
						);
				pNode = pNode->cs_next ;
			}while(pNode != pNodeStart);
		}
	}
}

/***********************************************************************
*
*     FUNCTION
*         printUsedBlockDetails
*
*     DESCRIPTION
*		dump the first n bytes of all allocated blocks of memory in the supplied
*		Nucleus memory pool. This is intended to aid in the location of memory leaks
*		which will show up as not-freed blocks of memory.
*		The amount of data fto dump from each block is set by the MEMORYDUMP_DUMPSIZE
*		constant (or the block size whichever is smaller).
*
*     INPUTS
*  	      NU_MEMORY_POOL *		a nucleus memory pool to use
*
*     OUTPUTS
*
***********************************************************************/
#define MEMORYDUMP_DUMPSIZE		48
void printUsedBlockDetails(NU_MEMORY_POOL *pMPPool)
{
	char strName[NU_MAX_NAME+1] ;
	DM_HEADER *pDMH, *pStart ;

	if(pMPPool)
	{
		strncpy(strName, pMPPool->dm_name, NU_MAX_NAME);
		strName[NU_MAX_NAME] = 0;
//		pStart = pDMH = pMPPool->dm_memory_list ;
		pStart = pDMH = (DM_HEADER *)pMPPool->dm_start_address ;

		debugDisplayOnly("\r\nDumping allocated memory blocks from pool %s [%08X]\r\n", strName, (UINT32)pStart);
		// walk the allocated memory blocks and count and accumulate them.
		UINT uIndex = 0 ;
		UINT uBlkIdx = 0 ;
		do
		{
			if((!pDMH->dm_memory_free) && ((UINT32)pDMH->dm_next_memory > (UINT32)pDMH))
			{
				UINT32 uSize = ((UINT32)(pDMH->dm_next_memory) - (UINT32)pDMH) - DM_OVERHEAD ;
				debugDisplayOnly("Block %ld - [%ld bytes] offset %d\r\n", uIndex++, uSize, uBlkIdx);
//				dumpHex((UINT8 *)(pDMH + DM_OVERHEAD), MIN(MEMORYDUMP_DUMPSIZE, uSize), (UINT32)pDMH + DM_OVERHEAD);
				dumpHex((UINT8 *)(pDMH), MIN(MEMORYDUMP_DUMPSIZE, uSize), (UINT32)pDMH);
			}
			pDMH = pDMH->dm_next_memory;
			uBlkIdx++;
		}while((pDMH != pStart) && (pDMH != NULL));
		debugDisplayOnly("\r\n");
	}
}


/***********************************************************************
*
*     FUNCTION
*         on_GetMemoryPoolStatsReq
*
*     DESCRIPTION
*     	Websocket interface to memory pool stats
*     	walk the nucleus list of memory pools and dub stats regarding amount of space allocated, how much is available
*     	and give some idea of the fragmentation of the pools.
*
*     INPUTS
*  	      Websocket standard interface
*
*     OUTPUTS
*
***********************************************************************/
void on_GetMemoryPoolStatsReq(UINT32 handle, cJSON *root)
{
    cJSON 				*rspMsg		=  cJSON_CreateObject();
    WSOX_Queue_Entry 	entry  		=  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};
    NU_MEMORY_POOL 		*pnuMemPool = getMemoryPool(MEMSTATS_SYSTEMMEMORYPOOL);
	CS_NODE				*pNode 		= (CS_NODE *)pnuMemPool;
	CS_NODE				*pNodeStart = pNode ;

	char				name[12];
	MEM_BLOCK_STATS 	memStats ;
	cJSON 				*pArr 		= cJSON_CreateArray();


//	debugDisplayOnly("Pool Name  Total Size :  Available           Allocated                     Free\r\n");
//	debugDisplayOnly("--------------------------------------------------------------------------------------\r\n");

	do
	{
		NU_MEMORY_POOL *pMP = (NU_MEMORY_POOL *)pNode;
		strncpy(name, pMP->dm_name, 8) ;
		name[8] = 0 ;
		countMemoryBlocks(pMP, &memStats);
		cJSON  *eleMsg =  cJSON_CreateObject();
		if((eleMsg != NULL) && (pArr != NULL))
		{
			cJSON_AddStringToObject(eleMsg, WS_KEY_NAME, name);
			cJSON_AddNumberToObject(eleMsg, WS_KEY_TOTALSIZE, 		((double)pMP->dm_pool_size)/(double)(1024*1024));
			cJSON_AddNumberToObject(eleMsg, WS_KEY_AVAILABLESIZE, 	((double)pMP->dm_available)/(double)(1024*1024));
			cJSON_AddNumberToObject(eleMsg, WS_KEY_ALLOCATEBLOCKS, 	memStats.blocksMem - memStats.blocksFree);
			cJSON_AddNumberToObject(eleMsg, WS_KEY_ALLOCATEDMEM, 	((double)memStats.accumMem)/(double)(1024*1024));
			cJSON_AddNumberToObject(eleMsg, WS_KEY_FREEBLOCKS, 		memStats.blocksFree);
			cJSON_AddNumberToObject(eleMsg, WS_KEY_FREEMEM, 		((double)memStats.accumFree)/(double)(1024*1024));
			cJSON_AddItemToArray(pArr, eleMsg);
		}

#if 0
		debugDisplayOnly("  %s [%7.3f MB:%8.4f MB]  %5d blks (%8.4f MB)  -  %5d blks (%8.4f)\r\n",
				name,
				((double)pMP->dm_pool_size)/(double)(1024*1024),
				((double)pMP->dm_available)/(double)(1024*1024),
				memStats.blocksMem - memStats.blocksFree,
				((double)memStats.accumMem)/(double)(1024*1024),
				memStats.blocksFree,
				((double)memStats.accumFree)/(double)(1024*1024)
				);
#endif

		pNode = pNode->cs_next ;
	}while(pNode != pNodeStart);

	if(pArr != NULL)
		cJSON_AddItemToObject(rspMsg, WS_KEY_POOLSTITLE, pArr) ;

	cJSON_AddStringToObject(rspMsg, WS_KEY_MESSAGEID, "GetMemoryPoolRsp");
    addEntryToTxQueue(&entry, root, "  memory_stats: Added GetMemoryPoolRsp to tx queue. status=%d\r\n");
}




/***********************************************************************
*
*     FUNCTION
*         countMemoryBlocks
*
*     DESCRIPTION
*     	walk the linked list of memory blocks within a pool
*
*     INPUTS
*  	      NU_MEMORY_POOL *		a nucleus memory pool, used as the starting point to walk the list.
*  	      MEM_BLOCK_STATS *		user allocated structure to hold results from this routine.
*
*     OUTPUTS
*
***********************************************************************/
void countMemoryBlocks(NU_MEMORY_POOL 	*pnuMemPool, MEM_BLOCK_STATS *pBlkStats)
{
	DM_HEADER *pDMH, *pStart ;
	if(pBlkStats)
	{
		memset(pBlkStats, 0, sizeof(MEM_BLOCK_STATS)) ;

		pDMH = pStart = pnuMemPool->dm_start_address ;
		if(pDMH) // avoid null pointer use....
		{
			do
			{
				if(pDMH->dm_memory_free)
				{
					pBlkStats->blocksFree++;
					UINT32 uFree = ((UINT32)(pDMH->dm_next_memory) - (UINT32)pDMH) - DM_OVERHEAD ;
					pBlkStats->accumFree += uFree;
				}
				else //if(!pDMH->dm_memory_free)
				{
					// allocated memory - dummy header at end of memory space points back to start but has no memory associated with it(?)
					if(pDMH->dm_next_memory != pStart)
					{
						if((UINT32)(pDMH->dm_next_memory) < ((UINT32)pDMH + DM_OVERHEAD))
						{
						}
						else
						{
							pBlkStats->accumMem += ((UINT32)(pDMH->dm_next_memory) - (UINT32)pDMH) - DM_OVERHEAD ;
							pBlkStats->blocksMem++;
						}
					}
				}
				pDMH = pDMH->dm_next_memory;
			}while((pDMH != pStart) && (pDMH != NULL));
		}
	}
}

/***********************************************************************
*
*     FUNCTION
*         getMemoryPool
*
*     DESCRIPTION
*		return the memory pool which matches the supplied name if possible else return NULL
*
*     INPUTS
*     	char *		null terminated, case sensitive name of the momory pool to return.
*
*     OUTPUTS
*     	pointer to the SYSMEM memory pool
*
***********************************************************************/
NU_MEMORY_POOL 	*getMemoryPool(char *name)
{
    NU_MEMORY_POOL 	*pnuMemPool, *pnuStart ;
    UNSIGNED uCount =  NU_Memory_Pool_Pointers (&pnuMemPool, 1);
    if(uCount == 1)
    {
    	pnuStart = pnuMemPool;
    	// we have a pool pointer, it is in a circular list so we can move from here...
    	do
    	{
    		size_t sz = sizeof(name); // eliminate compiler warning by assigning the size here.
			if(strncmp(name, pnuMemPool->dm_name, sz) != 0)
			{
				pnuMemPool = (NU_MEMORY_POOL *)pnuMemPool->dm_created.cs_next ;
			}
			else
				break ;
    	}while(pnuMemPool != pnuStart) ;
    }
    else
    {
    	return NULL;
    }

    return pnuMemPool ;
}
