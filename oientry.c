/*******************************************************************************
   PROJECT    :   Future Phoenix
   COPYRIGHT  :   2005, Pitney Bowes, Inc.  
   AUTHOR     :   
   MODULE     :   OIENTRY.C  
    
   DESCRIPTION:
      This file contains all types of screen interface functions 
      (hard key, event, pre, post, field init and field refresh)
      and global variables pertaining to managing entry fields.
      Many of the functions contained in this file are used
      all over the system- wherever there is a need for the
      user to type a value into an on-screen buffer.

* ----------------------------------------------------------------------
 MODIFICATION HISTORY:

 01-Jul-18 sa002pe on FPHX 02.12 shelton branch
      Fixed some comments in fnSelectedItemIndicator.

  01-Jun-17 Jennifer Hao on FPHX 02.12 cienet branch
      Commented out  functions fnhkDeleteCharOrChangeScr,  fnhkDeleteCharOrChangeScrOverLap, because they aren't used by G9.

  22-May-17 Jennifer Hao on FPHX 02.12 cienet branch
    Commented out functions fnfAmPmToggleField,  fnfFeederTimeoutInit,fnfAlphaInit,fnfUpperLowerCaseToggleFieldInit,
    fnfUpperLowerCaseToggleFieldRefresh, fnfUpperLowerCaseToggleWithSwitch, fnhkBufferKeyBoard,
    fnhkBufferKeyAlphaCharOverlap,fnhkBufferKeyAlphaCharWithSwitch,  fnhkBufferKeySelectAD,
    fnhkBufferKeySelectDelAD, fnhkBufferKeySelectIns, fnhkBufferKeySelectDelIns, fnhkShiftState3,
    fnhkBufBlankOrBeep, fnhkValidateAlpha,fnevAlphaEnterMoveCursorOverlap, fnpPrepSetCharToCapital,
    fnGetChoiceFieldCurrentChoice, fnGetCursorStyle, fnFindBufferFirstNumber, fnGetGfxErrorCode,
    fnShowError, fnIsShowError, fnGetCheckedItemProperties, fnGetAlphaEntrySwitch because they aren't used by G9. 

 24-Mar-14 Wang Biao on FPHX 02.12 cienet branch
     Modifeid function fnfScrollableHideEntryRefreshProxyPwd() to fix fraca 224776.
     Modified function fnhkBufClear() to fix fraca 224777.
     
 08-Jan-14 Wang Biao on FPHX 02.12 cienet branch
     Added functions fnfScrollableEntryRefresh2(),fnfScrollableHideEntryRefreshProxyPwd(),fnpBufferInputInit() for
     Network Proxy Settings.
     Modified functions fnhkDeleteCharScrollOrChangeScr(),fnhkDeleteCharacterScroll(),fnevAlphaEnterMoveCursor()
     for Network Proxy Settings.
     
 18-Feb-13 sa002pe on FPHX 02.10 integration branch
	Enh Req 218816:
	1. Added fnhkBufClear.

 10/30/2012  Bob Li   Modified function fnfScrollableEntryRefresh() to fix fraca 218814.
 05/25/2011  Jane, Li    Modified function fnfEntryErrorInfo()

 01-Sep-10 ming.huang on FPHX02.07 cienet branch
    Added three utility functions to the file end.

 27-Aug-10 ming.huang on FPHX02.07 cienet branch
    Changed the function fnfScrollableEntryRefresh to fix fraca 176051, the issue is that 
   if this function is called as an init field function ,the field attribute flags have to be set first.

   
     06/02/2010  Jingwei,Li      Added function fnfScrollableEntryEditOption() to disable "Edit Options" for RTL.
     04/29/2010  Jingwei,Li      Modified fnfScrollableEntryRefresh() to fix cursor move issue.
     04/27/2010  Jingwei,Li      Modified fnfScrollableHideEntryRefresh() and fnfScrollableEntryRefresh() to fix cursor
                                 location issue for RTL.
     04/23/2010  Jingwei,Li      Modified fnfScrollableHideEntryRefresh() and fnfScrollableEntryRefresh() to support RTL.
     04/21/2010   Jingwei,LI     Modified fnfHideEntryRefresh() and fnfScrollableEntryInit() to support RTL.
     04/16/2010   Jingwei,Li     Modified fnfHideEntryRefresh() to trim the redundant spaces that are padded 
                                 during screen mirror for RTL
    12/11/2009    Raymond Shen    Modified fnhkResendKeyDown() to do not accept decimal point for France KIP mode, thus fixed
                                  GMSE00175646(G970/DM475 - The decimal key should be inactive in KIP or Enter Tape Qty screen
                                  per the "UISPEC_Rel 8.x.pdf".).
    09/23/2009    Rocky Dai       Modified fnfHighValueWarningInit() and fnfLowFundsWarningInit()
                                  to fix GMSE00170555(G910/DM300c - The Backspace key should be active on some entry screens)
    06/23/2009    Joey Cui        Modified function fnfEntryErrorInfo() and fnfStdEntryInit()
                                  to fix fraca GMSE00163022 and GMSE00161091
    03/31/2009    Jingwei,Li      Modified function fnfEntryErrorInfo() for no error/warning case.
    03/18/2009    Raymond Shen    Added fnhkBufferKeyHexNumber() and fnhkBufferKeyHexNumberSS1().

  2008.09.01    Ivan Le Goff        FPHX 01.12
   - In fnSelectedItemIndicator() add condition for permits, as different 
     types of permits can share the same GenID
   - Change fnGetCheckedItemIndex() to fnGetCheckedItemProperties()
   - Change fnSetCheckedItemIndex() to fnSetCheckedItemProperties()

  2008.06.16    Clarisa Bellamy     FPHX 01.11
   Modified for Graphics Module Update:
   - Replaced old graphics functions with new graphics functions.

      1 Nov 2007  Simon Fox     Moved fnfDisplayDecimalMoneyOrBlank into
                                oirateservice.c for access to the rating
                                context.
      10/31/2007  Vincent Yi    Added function fnfDisplayDecimalMoneyOrBlank as Simon's request.
      10/29/2007  Andy Mo       Added wrapper function fnGetGfxErrorCode() to access bEntryErrorCode
      09/05/2007  Oscar Wang    Added fnhkBufferKeyScrollIffNotEmpty to fix FRACA GMSE00128857. 
      08/03/2007  Joey Cui      Modified fnfDisplayDecimalMoney
                                Added fnfCurrencySignFront, fnfCurrencySignAfter
                                to fit for different currency conventions
      07/19/2007  Vincent Yi    Fixed lint errors
      06/26/2007  Dan Zhang     Remove the space char to fix 115073
      06/08/2007    Dan Zhang   Remove the modification for GMSE114974.
      06/07/2007   Dan Zhang   Fixed GMSE114974.
      06/05/2007   Vincent Yi   Added function fnInputFieldInit()
      05/10/2007   Oscar Wang   Added function fnSetGfxErrorCode.
      04/12/2007   Oscar Wang   Updated function fnhkBufBlankOrChangeScr
                                for DUNS number screen.
      12/12/2006   Oscar Wang   Added function fnhkBufferKeyBoard() to support
                                key board input, specifically for zip/zone.
      09/15/2006   Raymond Shen Modified fnfScrollableEntryRefresh() to
                   deal with cursor's insert mode.
      09/15/2006   Dicky Sun Update the following functions for clearing
                             error prompt:  
                                  fnhkDeleteCharacter
      09/14/2006  Oscar Wang Modified function fngEntryErrorInfo and 
                             fnSetEntryErrorCode for error graphic display.
      09/01/2006  Dicky Sun  For error prompt in footer area, 
                                          1.add the following functions:
                                             fnShowError
                                             fnIsShowError
                                          2.modify function:
                                             fnfEntryErrorInfo
      12/06/2005  Adam Liu   Merged with Mega Rearch code

  18 oct 2005 \main\jnus11.00_i1_shelton\3    cx17598 
   Replaced the call fnhkModeSetManual to fnModeSetDefault.
 
* ----------------------------------------------------------------------
*   OLD PVCS REVISION HISTORY
*
**************************************************************************/

/**********************************************************************
        Include Header Files
**********************************************************************/

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "AIServic.h"
#include "bob.h"
#include "bobutils.h"
#include "clock.h"
#include "cmos.h"
#include "cwrapper.h"
#include "ctype.h"
#include "datdict.h"
//#include "deptaccount.h"
#include "errcode.h"
#include "fwrapper.h"
#include "global.h"
#include "keypad.h"
#include "oiclock.h"
#include "oicmenucontrol.h"
#include "oicommon.h"
#include "oientry.h"
#include "oipostag.h"
#include "oit.h"
#include "nucleus.h"
//#include "ratesvcpublic.h"
#include "commontypes.h"
#include "uniString.h"
#include "utils.h"      // ReturnProtVersion()
#include "flashutil.h"


/**********************************************************************
        Local Defines, Typedefs, and Variables
**********************************************************************/

// flag that in default value state
static BOOL     fDefValueState = TRUE;      
// Array used to store the default value of the field
static UNICHAR  pUniDefValueStr[MAX_FIELD_LEN]; 

// Used to determine which error string should be displayed, if an 
// invalid value is entered.
static UINT8 bEntryErrorCode = ENTRY_NO_ERROR;
static UINT8 bGfxErrorCode = ENTRY_NO_ERROR;

static BOOL bShowError = FALSE;
static BOOL bShowError2 = FALSE;
static BOOL bRefreshScreen = FALSE;

static ITEM_SELECTOR SelectedItem;  // stores the current properties of the selected Item 
                                    // for showing "check" indicator.
static void fnBufferDefaultValue (void);
static BOOL fnSelectedItemIndicator(UINT16 *    pFieldDest, 
                                     UINT8      bLen,   
                                     UINT8      bFieldCtrl, 
                                     UINT8      bNumTableItems, 
                                     UINT8   *  pTableTextIDs,
                                     UINT16     usItemIndex);

/*---------------------------------------------------------------------*/
/*     Global Variables  Local Defines, Typedefs and Local Variables.  */
/*---------------------------------------------------------------------*/

/* externs */



extern UINT8                wCurrentDistributorParameter;
extern KIPPWInfo*           pKIPPasswordInfo;
extern BOOL                 fHighPostagePending;

/* when we have some time, we should write some functions
   that hide the info in this structure */
extern KEY_BUFFER           rKeyBuffer;         
extern BOOL                 fScreenTickEventQueued;
                                                        
extern ENTRY_BUFFER_CTRL    rEntryBuf;
extern UINT16 TmpCombiningEntryBuf; //extern variable from oifield.c

/* declarations */

#define SHIFT_KEY_STATUS1           1
#define SHIFT_KEY_STATUS2           2
#define SHIFT_KEY_STATUS3           3

#define ACCOUNT_NAME_LEN            (UINT16)12
#define ALPHA_UNICODE_TABLE_OFFSET  (UINT16)2
#define ALPHA_USED_DIGITAL_KEY      10
#define ALPHA_UNICODE_TABLE_LENGTH  16

#define LCD_FIELD_LEN    80
typedef UINT8 alternate_keys[ALPHA_UNICODE_TABLE_LENGTH];
extern UINT8 *pAlphaSpecialUnicode;
extern alternate_keys *pAlphaUnicodeTable;

UINT8  pAlphaZeroOrSpace[]    = { 0x00, '0', 0x00, ' ', 0x00, 0x00 };
UINT16 wKeyUnicodeJustInput;
UINT16 wKeyCurrentDigitalChar = ALPHA_USED_DIGITAL_KEY + 1;
UINT8  wUpperLowerToggle;

SINT8 AccountName[ACCOUNT_NAME_LEN];

// This variable is used to test if the alpha entry is enabled.
UINT8 bAlphaEntrySwitch = ALPHA_ENTRY_ON;
//for sepcial password display way.
BOOL bIsTimeOut = FALSE;

BOOL fPersistLastAsterisk = FALSE;


/* this is a general purpose choice field that has 2 possible selection values.  
  An architecture similar to the one used to manage entry buffers can be used 
  if it becomes necessary to manage multiple choice fields. */
#define ANNUAL_MONTH_NUM     12
#define MAX_MONTH_NUM        12
#define MIN_MONTH_NUM        1
#define DAY_OF_WEEK          1
#define MAX_DAY_OF_MONTH     31
/* choice fields */
CHOICE_FIELD    rGenPurposeBinChoiceField = { 2, 0 };   /* this is a general purpose choice field
                                                        that has 2 possible selection values.  an
                                                        architecture similar to the one used to 
                                                        manage entry buffers can be used if it
                                                        becomes necessary to manage multiple choice
                                                        fields. */

UINT8          bCursorPosition;    /* these two variables are used to communicate cursor info */

UINT8          bCursorStyle;       /*  from the field functions to the field engine that calls */
                                   /*  them.  this could have been done by passing a reference */
                                   /*  parameter to every field function, but the cursor doesn't */
                                   /*  apply to all fields so that would have been a waste. */

ENTRY_BUFFER   rGenPurposeEbuf;    /* this is a general purpose entry buffer which can be used
                                   by most screens that have just a single entry field, since
                                   there is no reason to save what was entered after the 
                                   screen is exited */



/*------------------------------*/
/*  Variable Graphic Functions  */
/*------------------------------*/


/* *************************************************************************
// FUNCTION NAME: 
//      fngEntryErrorInfo
//
// DESCRIPTION: 
//      Variable graphic function that displays the graphic line if invalid
//      input indicator is displayed.
//
// INPUTS:
//      Standard graphic function inputs.
//
// RETURNS:
//      always returns successful
//
// MODIFICATION HISTORY:
//  09/14/2006  Oscar Wang      Modified for error graphic display.
//  12/02/2005  Shiming Sun     Initial Version
//
// *************************************************************************/
BOOL fngEntryErrorInfo (VARIABLE_GRAPHIC  *pVarGraphic, 
                        UINT16            *pSymID, 
                        UINT8             bNumSymListItems,                         
                        UINT8             *pSymList,
                        UINT8             *pAttributes)
{
    pVarGraphic->fUsed = FALSE;
    *pSymID = (UINT16) 0x0000;

    if( (bNumSymListItems > 0) && (bGfxErrorCode != ENTRY_NO_ERROR))
    {
    	EndianAwareCopy((UINT8 *)pSymID, pSymList, sizeof(UINT16));

        if(bShowError == TRUE)
                 {
            bShowError = FALSE;
           }
        else
        {
            bGfxErrorCode = ENTRY_NO_ERROR;
        }
    }
    else
    {
        if(bNumSymListItems > 1)
        {
        	EndianAwareCopy((UINT8 *)pSymID, pSymList+2, sizeof(UINT16));
        }
    }

    return (SUCCESSFUL);
}



/*--------------------------*/
/*   Field Init Functions   */
/*--------------------------*/


/* *************************************************************************
// FUNCTION NAME: 
//      fnfSelectedItemIndicator1~4
//
// DESCRIPTION: 
//      Output field function that displays the soft key indicator on for soft 
//      key 1~4 if that line is active. If the choice is selected, the indicator 
//      will be "check"
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always successful
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  10/24/2005  Vincent Yi      Initial version
//  01/20/2006  Kan jiang       Move from oifpimage to oientry
//  05/22/2006  James Wang      Change it to call a more common function.
//
// *************************************************************************/
BOOL fnfSelectedItemIndicator1(UINT16   *pFieldDest, 
                               UINT8    bLen,   
                               UINT8    bFieldCtrl, 
                               UINT8    bNumTableItems, 
                               UINT8    *pTableTextIDs,
                               UINT8    *pAttributes)
{
    return fnSelectedItemIndicator (pFieldDest, bLen, bFieldCtrl, 
                                    bNumTableItems, pTableTextIDs, 0);
}

BOOL fnfSelectedItemIndicator2(UINT16   *pFieldDest, 
                               UINT8    bLen,   
                               UINT8    bFieldCtrl, 
                               UINT8    bNumTableItems, 
                               UINT8    *pTableTextIDs,
                               UINT8    *pAttributes)
{
    return fnSelectedItemIndicator (pFieldDest, bLen, bFieldCtrl, 
                                    bNumTableItems, pTableTextIDs, 1);
}

BOOL fnfSelectedItemIndicator3(UINT16   *pFieldDest, 
                               UINT8    bLen,   
                               UINT8    bFieldCtrl, 
                               UINT8    bNumTableItems, 
                               UINT8    *pTableTextIDs,
                               UINT8    *pAttributes)
{
    return fnSelectedItemIndicator (pFieldDest, bLen, bFieldCtrl, 
                                    bNumTableItems, pTableTextIDs, 2);
}

BOOL fnfSelectedItemIndicator4(UINT16 * pFieldDest, 
                               UINT8    bLen,   
                               UINT8    bFieldCtrl, 
                               UINT8    bNumTableItems,
                               UINT8  * pTableTextIDs,
                               UINT8    *pAttributes)
{
    return fnSelectedItemIndicator (pFieldDest, bLen, bFieldCtrl, 
                                    bNumTableItems, pTableTextIDs, 3);
}



/* *************************************************************************
// FUNCTION NAME: 
//        fnfStdEntryInit
//
// DESCRIPTION:
//        Standard entry field initialization function.  Creates a general
//        purpose entry buffer that is initialized to spaces.  This function
//        is usable as the field init function for most entry fields that
//        have no special initialization requirements.
//
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:
//        None
//
// MODIFICATION  HISTORY:
//      06/23/2009    Joey Cui     Fix fraca 163022 and 161091      
//      11/30/2005    Adam Liu     Adapted for FuturePhoenix
//                    Mozdzer     Initial version   
// *************************************************************************/
BOOL  fnfStdEntryInit ( UINT16   *pFieldDest, 
                        UINT8     bLen, 
                        UINT8     bFieldCtrl, 
                        UINT8     bNumTableItems, 
                        UINT8    *pTableTextIDs, 
                        UINT8 *pAttributes )
{
    BOOL            fInsert;
    BOOL            fCursorOn;
    BOOL            fAlignL;
    UINT16  pTemplate[MAX_FIELD_LEN];
    UINT8   ucTableTextLen;
    // if it refreshs itself, don't init the buffer, just leave it
    if(bRefreshScreen == FALSE)
    {
        /* initialize the entry buffer to all spaces */
        SET_SPACE_UNICODE(rGenPurposeEbuf.pBuf, bLen);
     }
    /* dissect the field control byte to get the attribute flags */
    fInsert = (((bFieldCtrl & FIELD_TYPE_MASK) == TYPE_INPUT_INSERT) ? TRUE : FALSE);
    fCursorOn = ((bFieldCtrl & CURSOR_MASK) ? TRUE : FALSE);
    fAlignL = ((bFieldCtrl & ALIGN_MASK) ? FALSE : TRUE);
    
    /* if the jump bit is set, this field uses a template which is always the
        first table text string */  
    if( (bFieldCtrl & JUMP_MASK) != 0 )
    {
        UINT16  usTextID;

        /* the first text ID is the jump entry template */
        EndianAwareCopy(&usTextID, pTableTextIDs, 2);
        ucTableTextLen = fnCopyTableText(pTemplate, usTextID, bLen);
        pTemplate[ucTableTextLen] =  0;
    }
    else
    {
        /* if the bit is not set, make the template string a null string
         so that it has no effect on the buffer */
        pTemplate[0] =  0;
    }

    /* "create" the buffer */
    // if it refreshs itself, don't create entry buffer, just leave it
    if(bRefreshScreen == FALSE)
    {
        fnCreateEntryBuffer(&rGenPurposeEbuf, bLen, fAlignL, fInsert, fCursorOn, pTemplate);
    }
    else
    {
       bRefreshScreen = FALSE;
    }
    bCursorPosition = rGenPurposeEbuf.bCursorLoc;
    bCursorStyle = rGenPurposeEbuf.bCursorStyle;

    memcpy(pFieldDest, rGenPurposeEbuf.pDispBuf, (bLen + 1) * sizeof(UINT16));

    return ( (BOOL)SUCCESSFUL );
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnfStdEntryRefresh
//
// DESCRIPTION:
//         Standard entry field refresh routine.  This function refreshes
//         a field with the contents of the general purpose entry buffer.
//         It is usable as the refresh function for most entry fields that 
//         have no special refresh requirements.
//
// INPUTS:
//         Standard field function inputs.
//
// RETURNS:
//         always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      11/23/2005    Adam Liu     Adapted for FuturePhoenix
//                    Mozdzer       Initial version  
// *************************************************************************/
BOOL fnfStdEntryRefresh ( UINT16 *pFieldDest, 
                          UINT8   bLen, 
                          UINT8   bFieldCtrl, 
                          UINT8   bNumTableItems, 
                          UINT8  *pTableTextIDs, 
                          UINT8  *pAttributes )
{
    SET_SPACE_UNICODE(pFieldDest, bLen);
    
    bCursorPosition = rGenPurposeEbuf.bCursorLoc;
    bCursorStyle    = rGenPurposeEbuf.bCursorStyle;

    /* copy input buffer contents to the field */
    memcpy(pFieldDest, rGenPurposeEbuf.pDispBuf, (bLen + 1) * sizeof(UINT16));

    /* if the field is right aligned and the entry buffer is shorter than the field length,
       we need to pad the output with spaces on the left side */
    if ((fnDisplayableBufferLen() < bLen) && (bFieldCtrl & ALIGN_MASK))
    {
        fnUnicodePadSpace(pFieldDest, fnDisplayableBufferLen(), bLen);
        /* the cursor position also needs to be pushed to the right to account for
           the padding, (but not if turned off or inactive) */
        if ((bCursorPosition != CURSOR_INACTIVE) && (bCursorPosition != CURSOR_OFF))
        {
            bCursorPosition += bLen - fnBufferLen();
        }
    }

    return ((BOOL)SUCCESSFUL);
}




/* *************************************************************************
// FUNCTION NAME: 
//          fnfScrollableEntryInit
//
// DESCRIPTION:
//          Entry field initialization function that creates a buffer of
//          length 60 that is intended to be used as a scrollable buffer
//          inside a screen field that is less that 60 characters long.
//          This function is intended to be used in combination with 
//          fnfScrollableEntryRefresh.
//
// INPUTS:
//          Standard field function inputs.
//
// RETURNS:
//          always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//          Right-aligned fields have not been tested or attempted.
//
// MODIFICATION HISTORY:
//      04/21/2010    Jingwei,Li   Modified to support RTL.
//      02/18/2006    Kan Jiang     Modified
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                    David Huang  Initial version  
//
// *************************************************************************/
BOOL fnfScrollableEntryInit ( UINT16 *pFieldDest, 
                              UINT8   bLen, 
                              UINT8   bFieldCtrl, 
                              UINT8   bNumTableItems, 
                              UINT8  *pTableTextIDs, 
                              UINT8  *pAttributes )
{
    //  we extend it to 80 since the max length of pFieldDest is 80 -Kan
    (void)fnfStdEntryInit(pFieldDest, LCD_FIELD_LEN, bFieldCtrl, 
                          bNumTableItems, pTableTextIDs, pAttributes);
 //   fnSetEntryBufCharIndex(0);
    pFieldDest[bLen] = (UINT16) NULL;

    return ((BOOL)SUCCESSFUL);
}




/* *************************************************************************
// FUNCTION NAME:
//        fnfDisplayDecimalMoney
//
// DESCRIPTION:
//        Output field function that displays chooses a table text
//        string based on if the current currency accept decimal point or not.
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:  
//        1st string- displayed if current currency accept decimal point.
//        2nd string- displayed if current currency doesn't accept decimal point.
//
// MODIFICATION HISTORY:
//      08/01/2007    Joey Cui    Add Currency Sign behind if necessary.
//      06/08/2007    Dan Zhang   Remove the modification for GMSE114974.
//      06/07/2007    Dan Zhang   Fixed fraca GMSE114974..
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                    David Huang  Initial version  
//
// *************************************************************************/
BOOL fnfDisplayDecimalMoney ( UINT16 *pFieldDest, 
                              UINT8   bLen, 
                              UINT8   bFieldCtrl, 
                              UINT8   bNumTableItems, 
                              UINT8  *pTableTextIDs, 
                              UINT8  *pAttributes )
{
    UINT8   bWhichString;   
    UINT16  wTextID;
    UINT8   bUniLen;
    size_t  bSymbolLen;
    UINT16  *pUnicodeCurrencySymbol;
    UINT8   bCurrencySymbolPlacement;

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* note this implementation only supports checking for fullness due to 
        entered characters- not characters which may have been in the 
       initial value of the buffer */
       
    bWhichString =  fnFlashGetByteParm(BP_MIN_DISPLAYED_DECIMALS) ;
    /*
    switch(bWhichString)
    {
            case 0:
            bWhichString = 1;
            break;
        case 1:
        case 2:
            bWhichString =3-bWhichString;
            break;
        default :       
            bWhichString =0;
            break;
        
    }
    */

    if ( bWhichString > 3 )
    { 
        bWhichString = 0;
    }
    else
    {
        bWhichString = 3 - bWhichString ;
    }   
/* by Joey Cui, add Currency Sign behind if necessary, see code below
     fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, 
                       bNumTableItems, pTableTextIDs, bWhichString); */

    if (bWhichString < bNumTableItems)
    {
    	EndianAwareCopy(&wTextID, pTableTextIDs + (sizeof(unsigned short) * bWhichString), 2);
        bUniLen = fnCopyTableText(pFieldDest, wTextID, bLen); 

        /* get the currency symbol and location out of flash */
        pUnicodeCurrencySymbol = (UINT16 *) fnFlashGetUnicodeStringParm(USP_DISP_CURRENCY_SYM); 
        if (pUnicodeCurrencySymbol == NULL_PTR)
        {//nothing to do but return
            return ((BOOL) SUCCESSFUL);
        }

        bSymbolLen = unistrlen(pUnicodeCurrencySymbol);
        if ((bSymbolLen == 0) || (bSymbolLen > MAX_CURRENCY_LEN))
        {//either no symbol or symbol is too big
            return ((BOOL) SUCCESSFUL);
        }

        bCurrencySymbolPlacement = fnFlashGetByteParm(BP_CURRENCY_SYM_PLACEMENT);

        /* check if the currency symbol goes before or after the money amount */
        if (bCurrencySymbolPlacement == 0)
        {
            /* Placement = BEFORE */
            bSymbolLen = 0 ;  //  We have already show the sign in front of the digit, so we don't need append it here
        }
        if ( (bUniLen+bSymbolLen) > bLen)
            bSymbolLen = bLen - bUniLen; // Avoid overboundry

            /* copy the symbol into the area immediately after the money string */
        EndianAwareCopy(pFieldDest + bUniLen, pUnicodeCurrencySymbol, (bSymbolLen * sizeof(UINT16)));

        /* if field is right-aligned, pad left of string with spaces */
        if (bFieldCtrl & ALIGN_MASK)
        {
            pFieldDest[bUniLen+bSymbolLen] = (unsigned short) NULL;
            fnUnicodePadSpace(pFieldDest, bUniLen, bLen);
        }

    }

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnfHighValueWarningInit
//
// DESCRIPTION:
//          Entry field function to initialize the entry field where the   
//          user types a new high value warning value.  The field gets
//          initialized to the current setting.
//
// INPUTS:
//          Standard field function inputs.
//
// RETURNS:
//          always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//
//      09/23/2009    Rocky Dai Modified to fix GMSE00170555
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                    Mozdzer   Initial version 
// *************************************************************************/
BOOL fnfHighValueWarningInit ( UINT16 *pFieldDest, 
                               UINT8   bLen, 
                               UINT8   bFieldCtrl, 
                               UINT8   bNumTableItems, 
                               UINT8  *pTableTextIDs, 
                               UINT8  *pAttributes )
{
    BOOL    fInsert;
    BOOL    fCursorOn;
    BOOL    fAlignL;
    UINT8   bInitLen;               
    UINT16  wTemplate = (UINT16) NULL;  /* templates won't be supported for this field */

    /* initialize the entry field to spaces */
    SET_SPACE_UNICODE(rGenPurposeEbuf.pBuf, bLen);
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* stuff the original warning value into the entry buf */
    bInitLen = fnMoney2Unicode(rGenPurposeEbuf.pBuf, 
                              (double) fnCMOSSetupGetCMOSSetupParams()->lwHighValueWarning, TRUE,
                                fnFlashGetByteParm(BP_MIN_DISPLAYED_INTEGERS),
                                fnFlashGetByteParm(BP_MIN_DISPLAYED_DECIMALS));


    /* pad with spaces if right aligned */
    if ((bInitLen < bLen) && (bFieldCtrl & ALIGN_MASK))
    {
        /* make the string null-terminated before calling unicode pad space, which assumes
            that it is */
        rGenPurposeEbuf.pBuf[bInitLen] = (UINT16) NULL;
        fnUnicodePadSpace(rGenPurposeEbuf.pBuf, (UINT16) bInitLen, (UINT16) bLen);
    }

    /* create the entry buffer with the given attributes */
    fInsert = (((bFieldCtrl & FIELD_TYPE_MASK) == TYPE_INPUT_INSERT) ? TRUE : FALSE);
    fCursorOn = ((bFieldCtrl & CURSOR_MASK) ? TRUE : FALSE);
    fAlignL = ((bFieldCtrl & ALIGN_MASK) ? FALSE : TRUE);
    fnCreateEntryBuffer(&rGenPurposeEbuf, bLen,fAlignL, fInsert, fCursorOn, &wTemplate);
                        
    rEntryBuf.bCharsEntered = bInitLen;
    
    bCursorStyle = rGenPurposeEbuf.bCursorStyle;
    bCursorPosition = rGenPurposeEbuf.bCursorLoc;
    /* the cursor position needs to be adjusted if the field is right aligned and the
        entry buffer is shorter than the field length */
    if ((bFieldCtrl & ALIGN_MASK) && (bCursorPosition != CURSOR_INACTIVE) &&
         (bCursorPosition != CURSOR_OFF))
    {
        bCursorPosition += bLen - fnBufferLen();
    }

    memcpy(pFieldDest, rGenPurposeEbuf.pDispBuf, (bLen + 1) * sizeof(UINT16));
    pFieldDest[bLen] = (UINT16) NULL;
    return (SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: 
//          fnfLowFundsWarningInit
//
// DESCRIPTION:
//          Entry field function to initialize the entry field where the   
//          user types a new low funds warning value.  The field gets
//          initialized to the current setting.
//
// INPUTS:
//         Standard field function inputs.
//
// RETURNS:
//         always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//      09/23/2009    Rocky Dai Modified to fix GMSE00170555
//      06/26/2007      Dan Zhang   Remove the space char to fix 115073
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                    Mozdzer   Initial version   
// *************************************************************************/
BOOL fnfLowFundsWarningInit ( UINT16 *pFieldDest, 
                              UINT8   bLen, 
                              UINT8   bFieldCtrl, 
                              UINT8   bNumTableItems, 
                              UINT8  *pTableTextIDs, 
                              UINT8  *pAttributes )
{
    BOOL    fInsert;
    BOOL    fCursorOn;
    BOOL    fAlignL;
    UINT8   bInitLen;               
    UINT16  wTemplate = (UINT16) NULL;  /* templates won't be supported for this field */
    UINT16  *pEntry;
    UINT16  wUnicodeDecimalChar;

    /* initialize the entry field to spaces */
    SET_SPACE_UNICODE(rGenPurposeEbuf.pBuf, bLen);
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* stuff the original warning value into the entry buf */
    bInitLen = fnMoney2Unicode(rGenPurposeEbuf.pBuf, 
                               (double) fnCMOSSetupGetCMOSSetupParams()->lwLowFundsWarning, TRUE,
                               fnFlashGetByteParm(BP_MIN_DISPLAYED_INTEGERS),
                               fnFlashGetByteParm(BP_MIN_DISPLAYED_DECIMALS));

    /* remove the decimal character and space char*/
    wUnicodeDecimalChar = fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR);
    pEntry = rGenPurposeEbuf.pBuf;
    while ((*pEntry != (UINT16) NULL) && ((*pEntry) != wUnicodeDecimalChar)&&((*pEntry) != UNICODE_SPACE))
    {
        pEntry++;
    }
    if (*pEntry == wUnicodeDecimalChar ||*pEntry == UNICODE_SPACE )
    {
        *pEntry = (UINT16)NULL;
    }

    bInitLen = unistrlen(rGenPurposeEbuf.pBuf);

    /* pad with spaces if right aligned */
    if ((bInitLen < bLen) && (bFieldCtrl & ALIGN_MASK))
    {
        /* make the string null-terminated before calling unicode pad space, which assumes
            that it is */
        rGenPurposeEbuf.pBuf[bInitLen] = (UINT16) NULL;
        fnUnicodePadSpace(rGenPurposeEbuf.pBuf, (UINT16) bInitLen, (UINT16) bLen);
    }

    /* create the entry buffer with the given attributes */
    fInsert = (((bFieldCtrl & FIELD_TYPE_MASK) == TYPE_INPUT_INSERT) ? TRUE : FALSE);
    fCursorOn = ((bFieldCtrl & CURSOR_MASK) ? TRUE : FALSE);
    fAlignL = ((bFieldCtrl & ALIGN_MASK) ? FALSE : TRUE);
    fnCreateEntryBuffer(&rGenPurposeEbuf, bLen,fAlignL, fInsert, fCursorOn, &wTemplate);
                        
    rEntryBuf.bCharsEntered = bInitLen;
    
    bCursorStyle = rGenPurposeEbuf.bCursorStyle;
    bCursorPosition = rGenPurposeEbuf.bCursorLoc;
    /* the cursor position needs to be adjusted if the field is right aligned and the
        entry buffer is shorter than the field length */
    if ((bFieldCtrl & ALIGN_MASK) && (bCursorPosition != CURSOR_INACTIVE) &&
         (bCursorPosition != CURSOR_OFF))
    {
        bCursorPosition += bLen - fnBufferLen();
    }

    memcpy(pFieldDest, rGenPurposeEbuf.pDispBuf, (bLen + 1) * sizeof(UINT16));
    pFieldDest[bLen] = (UINT16) NULL;
    return (SUCCESSFUL);
}


/*--------------------------*/
/*  Field Refresh Functions */
/*--------------------------*/


/* *************************************************************************
// FUNCTION NAME: 
//          fnfScrollableEntryRefresh
//
// DESCRIPTION:
//          Scrollable entry field refresh routine.  This function is used
//          to refresh an entry buffer that is longer than the field length.
//          Only the rightmost bLen characters are shown when the number of 
//          characters entered into the entry buffer exceeds the width
//          of the field.
//
// INPUTS:
//          Standard field function inputs.
//
// RETURNS:
//          always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//          Only left aligned fields are supported at this time.
//
// MODIFICATION HISTORY:
//      10/30/2012   Bob Li         Modified to fix fraca 218814.
//      04/29/2010   Jingwei,Li     Modified to fix cursor move issue.
//      04/27/2010   Jingwei,Li     Modified to fix cursor location issue.
//      04/23/2010   Jingwei,Li     Modified for RTL language writting for RTL.
//      09/15/2006    Raymond Shen  Modified deal with cursor's insert mode.
//      02/18/2006    Kan Jiang     Modified       
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                    Joe Mozdzer  Initial version  
//
// *************************************************************************/
BOOL fnfScrollableEntryRefresh ( UINT16 *pFieldDest, 
                                 UINT8   bLen, 
                                 UINT8   bFieldCtrl, 
                                 UINT8   bNumTableItems, 
                                 UINT8  *pTableTextIDs, 
                                 UINT8  *pAttributes )
{
    UINT16  wDispOffset = 0;
    UINT8   ubWritingDir;
    UINT16  wCursorOffset = 0;

    ubWritingDir = fnGetWritingDir();

    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* The following three lines is for fixing fraca 176051, the issue is that if this function
    is called as an init field function ,the field attribute flags have to be set first */
    rEntryBuf.fInsert = (((bFieldCtrl & FIELD_TYPE_MASK) == TYPE_INPUT_INSERT) ? TRUE : FALSE);
    rEntryBuf.fCursorOn = ((bFieldCtrl & CURSOR_MASK) ? TRUE : FALSE);
    rEntryBuf.fAlignL = ((bFieldCtrl & ALIGN_MASK) ? FALSE : TRUE);

    bCursorPosition = rGenPurposeEbuf.bCursorLoc;
    bCursorStyle = rGenPurposeEbuf.bCursorStyle;
    wCursorOffset = rEntryBuf.bLenNonTempl - rEntryBuf.pEbuf->bCursorLoc;

    if (rGenPurposeEbuf.bCursorLoc == CURSOR_OFF)
    {
        if(rEntryBuf.bNextCharIndex > rEntryBuf.bLenNonTempl)
        	wDispOffset = unistrlen(rGenPurposeEbuf.pDispBuf)- bLen;
    }
    else if (rEntryBuf.pEbuf->bCursorLoc >= bLen)
    {
        if(ubWritingDir == RIGHT_TO_LEFT)
        {
           wDispOffset = rEntryBuf.bLenNonTempl - bLen;
           if(wCursorOffset > bLen)
           {
               wDispOffset -= bLen;
           }
        }
        else
        {
            wDispOffset = rEntryBuf.pEbuf->bCursorLoc - bLen + 1;
        }
        
        if ((bCursorPosition != CURSOR_INACTIVE) && (bCursorPosition != CURSOR_OFF))
        {
            if(ubWritingDir == RIGHT_TO_LEFT)
            {
                if(wCursorOffset > bLen)
                {
                    wCursorOffset -= bLen;
                }
                bCursorPosition = bLen - wCursorOffset;
            }
            else
            {
                 bCursorPosition = bLen - 1;
            }
        }
    }
 
    /* copy input buffer contents to the field */
    memcpy(pFieldDest, rGenPurposeEbuf.pDispBuf + wDispOffset,  bLen * sizeof(UINT16));

    return (SUCCESSFUL);
}
/* *************************************************************************
// FUNCTION NAME: 
//          fnfScrollableEntryRefresh2
//
// DESCRIPTION: Scrollable entry field refresh routine.  This function is used
//				to refresh an entry buffer that is longer than the field length.
//				Only the rightmost bLen characters are shown when the number of characters
//				entered into the entry buffer exceeds the width of the field.
//				There are conditions for the displaying rightmost bLen characters.
//				When the buffer is full, the rightmost bLen characters will be displayed.
//				When the buffer is not full, but the length exceeds the bLen, display
//				the rightmost bLen-1 characters and a blank field for allowing more input.
//
// AUTHOR: Wang Biao
//
// INPUTS:	Standard field function inputs.
// TEXT TABLE USAGE:  None
// NOTE:  
// *************************************************************************/
BOOL fnfScrollableEntryRefresh2 ( UINT16 *pFieldDest, 
                                 UINT8   bLen, 
                                 UINT8   bFieldCtrl, 
                                 UINT8   bNumTableItems, 
                                 UINT8  *pTableTextIDs, 
                                 UINT8  *pAttributes )
{
   UINT16  wDispOffset = 0;
    UINT8   ubWritingDir;
    UINT16  wCursorOffset = 0;

    ubWritingDir = fnGetWritingDir();

    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* The following three lines is for fixing fraca 176051, the issue is that if this function
    is called as an init field function ,the field attribute flags have to be set first */
    rEntryBuf.fInsert = (((bFieldCtrl & FIELD_TYPE_MASK) == TYPE_INPUT_INSERT) ? TRUE : FALSE);
    rEntryBuf.fCursorOn = ((bFieldCtrl & CURSOR_MASK) ? TRUE : FALSE);
    rEntryBuf.fAlignL = ((bFieldCtrl & ALIGN_MASK) ? FALSE : TRUE);

    bCursorPosition = rGenPurposeEbuf.bCursorLoc;
    bCursorStyle = rGenPurposeEbuf.bCursorStyle;
    wCursorOffset = rEntryBuf.bLenNonTempl - rEntryBuf.pEbuf->bCursorLoc;

    if (rGenPurposeEbuf.bCursorLoc == CURSOR_OFF)
    {
        //if(rEntryBuf.bNextCharIndex > rEntryBuf.bLenNonTempl)
        	wDispOffset = unistrlen(rGenPurposeEbuf.pDispBuf)- bLen;
    }
    else if (rEntryBuf.pEbuf->bCursorLoc >= bLen)
    {
        if(ubWritingDir == RIGHT_TO_LEFT)
        {
           wDispOffset = rEntryBuf.bLenNonTempl - bLen;
           if(wCursorOffset > bLen)
           {
               wDispOffset -= bLen;
           }
        }
        else
        {
            wDispOffset = rEntryBuf.pEbuf->bCursorLoc - bLen + 1;
        }
        
        if ((bCursorPosition != CURSOR_INACTIVE) && (bCursorPosition != CURSOR_OFF))
        {
            if(ubWritingDir == RIGHT_TO_LEFT)
            {
                if(wCursorOffset > bLen)
                {
                    wCursorOffset -= bLen;
                }
                bCursorPosition = bLen - wCursorOffset;
            }
            else
            {
                 bCursorPosition = bLen - 1;
            }
        }
    }
 
    /* copy input buffer contents to the field */
    memcpy(pFieldDest, rGenPurposeEbuf.pDispBuf + wDispOffset,  bLen * sizeof(UINT16));

    return (SUCCESSFUL); 
}

/* *************************************************************************
// FUNCTION NAME: 
//          fnfScrollableHideEntryRefresh
//
// DESCRIPTION:
//          Scrollable Hide entry field refresh routine.  This function is used
//          to refresh an entry buffer that is longer than the field length.
//          Only the rightmost bLen hiden characters are shown when the number 
//          of characters entered into the entry buffer exceeds the width
//          of the field.
//
// INPUTS:
//          Standard field function inputs.
//
// RETURNS:
//          always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//          Only left aligned fields are supported at this time.
//
// MODIFICATION HISTORY:
//      04/27/2010   Jingwei,Li     Modified to fix cursor location issue for RTL.
//      04/23/2010   Jingwei,Li     Modified for RTL language writting.
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//         Joe Mozdzer, David Huang  Initial version  
//
// *************************************************************************/
BOOL fnfScrollableHideEntryRefresh (  UINT16 *pFieldDest, 
                                      UINT8   bLen, 
                                      UINT8   bFieldCtrl, 
                                      UINT8   bNumTableItems, 
                                      UINT8  *pTableTextIDs, 
                                      UINT8  *pAttributes )
{
    UINT16  wTextID;
    UINT16  wReplacementUnicode; 
    UINT32   i;
    UINT16  wUniChar;
    UINT16  wDispOffset = 0;
    UINT8   ubWritingDir;

    ubWritingDir = fnGetWritingDir();


    bCursorPosition = rGenPurposeEbuf.bCursorLoc;
    bCursorStyle = rGenPurposeEbuf.bCursorStyle;

    if (rEntryBuf.pEbuf->bCursorLoc >= bLen)
    {
        if(ubWritingDir == RIGHT_TO_LEFT)
        {
           wDispOffset = rEntryBuf.bLenNonTempl - bLen;
        }
        else
        {
            wDispOffset = rEntryBuf.pEbuf->bCursorLoc - bLen + 1;
        }
        
        if ((bCursorPosition != CURSOR_INACTIVE) && (bCursorPosition != CURSOR_OFF))
        {
            bCursorPosition = bLen - 1;
        }
    }

    /* the first (and only) table text string for this field should contain the Unicode character  
        that will replace the numbers typed in order to hide them. */
    if (bNumTableItems > 0)
    {
    	EndianAwareCopy(&wTextID, pTableTextIDs, 2);
        if(ubWritingDir == RIGHT_TO_LEFT)
        {
             /* we don't care the number of chars copied, so ignore the return value */
            (void)fnCopyTableTextForRTL(&wReplacementUnicode, wTextID, 1);
        }
        else
        {
            /* we don't care the number of chars copied, so ignore the return value */
           (void)fnCopyTableText(&wReplacementUnicode, wTextID, 1);
        }
        
        /* copy the entry buffer into the destination pointer, replacing any non-space characters
            with the Unicode replacement character as we go */
        for (i = 0; i < bLen; i++)
        {
            wUniChar = rGenPurposeEbuf.pDispBuf[i + wDispOffset];
            if (wUniChar != UNICODE_SPACE)
            {
                pFieldDest[i] = wReplacementUnicode;
            }
            else
            {
                pFieldDest[i] = wUniChar;
            }
        }

        /* append the null terminator */
        pFieldDest[bLen] = (UINT16) NULL;
    }
    else
    {
        /* if for some reason there wasn't a replacement character specified for the
            field, this function does a standard refresh */
        memcpy(pFieldDest, rGenPurposeEbuf.pDispBuf + wDispOffset, (bLen + 1) * sizeof(UINT16));
    }

    return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: fnfScrollableHideEntryRefreshProxyPwd
// DESCRIPTION:   Scrollable Hide entry field refresh routine.  This function is used
//				to refresh an entry buffer that is longer than the field length.
//				Only the rightmost bLen hiden characters are shown when the number of characters
//				entered into the entry buffer exceeds the width of the field.
//                If a number key is pressed within the "character scroll key" time, scroll thru 
//              the possible alpha numeric characters for the key. Display the actual character 
//              so the operator will be able to see what he/she is selecting.
//                Once the "character scroll key" time has expired after a number key was pressed,
//              If the field isn't full, add the appropriate character to the temporary buffer, 
//              display an asterisk in its place, move the cursor as necessary and scroll the data
//              in the field to the left as necessary.
// AUTHOR: Liu Jupeng
//
// INPUTS:	Standard field function inputs.
// TEXT TABLE USAGE:  None
// NOTE:  Only left aligned fields are supported at this time.
// MODIFICATION HISTORY:
//         03/24/2014   Wang Biao Modified to fix fraca 224776.
//         01/07/2014   Wang Biao Merged from Janus to FP for Network Proxy Settings.
//
// *************************************************************************/
BOOL fnfScrollableHideEntryRefreshProxyPwd (  UINT16 *pFieldDest, 
                                      UINT8   bLen, 
                                      UINT8   bFieldCtrl, 
                                      UINT8   bNumTableItems, 
                                      UINT8  *pTableTextIDs, 
                                      UINT8  *pAttributes )
{
    UINT16	wTextID;
	UINT16	wReplacementUnicode; 
	UINT32	i;
	UINT16	wUniChar;
	UINT16	wDispOffset = 0;
	
	bCursorPosition = rGenPurposeEbuf.bCursorLoc;
	bCursorStyle = rGenPurposeEbuf.bCursorStyle;

	if (rEntryBuf.bCharsEntered >= bLen)
	{
		if(rEntryBuf.bCharsEntered < rEntryBuf.bLen)
		{
			wDispOffset = rEntryBuf.bCharsEntered - bLen + 1;  //when the buffer is not full, display the right most bLen-1 chars
		}
		else
		{
			wDispOffset = rEntryBuf.bCharsEntered - bLen;		//when the buffer is not full, display the right most bLen chars
		}
		if ((bCursorPosition != CURSOR_INACTIVE) && (bCursorPosition != CURSOR_OFF))
			bCursorPosition = bLen - 1;
	}
	
	/* the first (and only) table text string for this field should contain the Unicode character  
		that will replace the numbers typed in order to hide them. */
	if (bNumTableItems > 0 )
	{		
		EndianAwareCopy(&wTextID, pTableTextIDs, 2);
    	(void)fnCopyTableText(&wReplacementUnicode, wTextID, 1);
    	

		//When the char scroll key time expired, display the asterisk.
	    if(TRUE == bIsTimeOut)
	    {  
    		/* copy the entry buffer into the destination pointer, replacing any non-space characters
    			with the Unicode replacement character as we go */
    		for (i = 0; i < bLen; i++)
    		{
    			wUniChar = rGenPurposeEbuf.pDispBuf[i + wDispOffset];
    			if (wUniChar != UNICODE_SPACE)
    				pFieldDest[i] = wReplacementUnicode;
    			else
    				pFieldDest[i] = wUniChar;
    		}

    		/* append the null terminator */
    		pFieldDest[bLen] = 0;

    		bIsTimeOut = FALSE;
	    }
	    else
	    {
			/* copy the entry buffer into the destination pointer, replacing any non-space characters
			   with the Unicode replacement character as we go */
    		for (i = 0; i < bLen; i++)
    		{
    			//When the char scroll key time hasn't expired, the right-most key is displayed as actual value.
    			//Others at its left should be displayed as the asterisk.
    			wUniChar = rGenPurposeEbuf.pDispBuf[i + wDispOffset];
    			if (wUniChar != UNICODE_SPACE && (i + wDispOffset < rEntryBuf.bCharsEntered - 1))
    				pFieldDest[i] = wReplacementUnicode; 
    			//For the ascent char, when press the combining key,the password field keeps asterisk.
    			else if((i + wDispOffset == rEntryBuf.bCharsEntered - 1) && TmpCombiningEntryBuf != 0xFFFF)
    			{
    			   pFieldDest[i] = wReplacementUnicode;
    			}
    			else if(TRUE == fPersistLastAsterisk)
    			{
    			    pFieldDest[i] = wReplacementUnicode;
    			}
    			else
    				pFieldDest[i] = wUniChar;
    		}
			bIsTimeOut = FALSE;
    		/* append the null terminator */
    		pFieldDest[bLen] = 0;
	    }
	}
	else
	{			
		/* if for some reason there wasn't a replacement character specified for the
			field, this function does a standard refresh */
		(void)memcpy(pFieldDest, rGenPurposeEbuf.pDispBuf + wDispOffset, (bLen + 1) * sizeof(unsigned short));
    }
	return (SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnfHideEntryRefresh
//
// DESCRIPTION:
//         Refreshes a field using the general purpose entry buffer, 
//         however, every non-space character in the buffer is 
//         shown as a configurable replacement character instead
//         of what was entered.  This is used to hide the entry
//         of passwords so that someone cannot look over the 
//         shoulder of the typist to steal the code.
//
//         Example:  Buffer contains "  145", replacement char = "*"
//                   -> LCD shows  "  ***"
//                   Buffer contains "ABC432", replacement char = "x"
//                   -> LCD shows  "xxxxxx"
//
// INPUTS:
//         Standard field function inputs.
//
// RETURNS:
//         always returns SUCCESSFUL
//
// WARNING/NOTES:
//         This function looks at rGenPurposeEbuf.pBuf
//         instead of rGenPurposeEbuf.pDispBuf, therefore the
//         refresh will not work properly if a template is used.
//         1st string should contain a single character that will
//         be shown instead of the actual characters input into
//         the buffer.
//
// MODIFICATION HISTORY:
//  04/21/2010   Jingwei,Li      Use fnCopyTableTextForRTL() to get table text for RTL.
//  04/16/2010 Jingwei,Li  Trim the redundant spaces that are padded 
//                         during screen mirror for RTL
//         Mozdzer    Initial version
// *************************************************************************/
BOOL fnfHideEntryRefresh ( UINT16 *pFieldDest, 
                           UINT8   bLen, 
                           UINT8   bFieldCtrl, 
                           UINT8   bNumTableItems, 
                           UINT8  *pTableTextIDs, 
                           UINT8  *pAttributes )
{
    UINT16  usTextID;
    UINT16  usReplacementUnicode; 
    UINT16  usTmp;
    UINT16  usUniChar;
    UINT8   ubWritingDir;

    bCursorPosition = rGenPurposeEbuf.bCursorLoc;
    bCursorStyle = rGenPurposeEbuf.bCursorStyle;
    
    ubWritingDir = fnGetWritingDir();

    /* the first (and only) table text string for this field should contain the Unicode character  
       that will replace the numbers typed in order to hide them. */
    if (bNumTableItems > 0)
    {
    	EndianAwareCopy(&usTextID, pTableTextIDs, 2);
        
        if(ubWritingDir == RIGHT_TO_LEFT)
        {
             /* we don't care the number of chars copied, so ignore the return value */
            (void)fnCopyTableTextForRTL(&usReplacementUnicode, usTextID, 1);
        }
        else
        {
            /* we don't care the number of chars copied, so ignore the return value */
           (void)fnCopyTableText(&usReplacementUnicode, usTextID, 1);
        }

        /* copy the entry buffer into the destination pointer, replacing any non-space characters
        with the Unicode replacement character as we go */

        for (usTmp = 0; usTmp < bLen; usTmp++)
        {
            usUniChar = rGenPurposeEbuf.pBuf[usTmp];
            if (usUniChar != UNICODE_SPACE)
            {
                pFieldDest[usTmp] = usReplacementUnicode;
            }
            else
            {
                pFieldDest[usTmp] = usUniChar;
            }
        }
        /* append the null terminator */
        pFieldDest[bLen] = 0;
    }
    else
    {
        /* if for some reason there wasn't a replacement character specified for the
           field, this function does a standard refresh */
        memcpy(pFieldDest, rGenPurposeEbuf.pBuf, (bLen + 1)*sizeof(UINT16));
    }

    return ((BOOL)SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnfBufferFullStringSelect
//
// DESCRIPTION:
//          Output field function that displays chooses a table text
//          string based on if the current entry buffer is full or not.
//
// INPUTS:
//          Standard field function inputs.
//
// RETURNS:
//          always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//          1st string- displayed if current entry buffer is full
//          2nd string- displayed if current entry buffer is not full
//
// MODIFICATION HISTORY:
//          Mozdzer   Initial version
// ************************* ************************************************/
BOOL fnfBufferFullStringSelect ( UINT16 *pFieldDest, 
                                 UINT8   bLen, 
                                 UINT8   bFieldCtrl, 
                                 UINT8   bNumTableItems, 
                                 UINT8  *pTableTextIDs, 
                                 UINT8  *pAttributes )
{
    UINT8  ucStringNum;

    /* note this implementation only supports checking for fullness due to entered characters- not
       characters which may have been in the initial value of the buffer */
    if (fnCharsBuffered() >= fnBufferLen())
    {
        ucStringNum = 0;
    }
    else
    {
        ucStringNum = 1;
    }

    fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, bNumTableItems,
                        pTableTextIDs, ucStringNum );

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//           fnfBufferPartialStringSelect
//
// DESCRIPTION:
//           Output field function that displays chooses a table text
//           string based on if the current entry buffer has something or not.
//
// INPUTS:
//           Standard field function inputs.
//
// RETURNS:
//           always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//           1st string- displayed if current entry buffer has something
//           2nd string- displayed if current entry buffer does not have anything
//           This function is almost the same to the function fnfBufferEmptyStringSelect()
//
// MODIFICATION HISTORY:
//           Mozdzer      Initial version
// *************************************************************************/
BOOL fnfBufferPartialStringSelect ( UINT16 *pFieldDest, 
                                    UINT8   bLen, 
                                    UINT8   bFieldCtrl, 
                                    UINT8   bNumTableItems, 
                                    UINT8  *pTableTextIDs, 
                                    UINT8  *pAttributes )
{
    UINT8  ucStringNum;
    /* note this implementation only supports checking for fullness due to entered characters- not
        characters which may have been in the initial value of the buffer */
    if (fnCharsBuffered() > 0)
    {
        ucStringNum = 0;
    }
    else
    {
        ucStringNum = 1;
    }

    fnDisplayTableText(pFieldDest,bLen,bFieldCtrl,bNumTableItems,pTableTextIDs,ucStringNum);

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnfBufferEmptyStringSelect
//
// DESCRIPTION:
//          Output field function that displays chooses a table text
//          string based on if the current entry buffer is empty or not.
//
// INPUTS:
//          Standard field function inputs.
//
// RETURNS:
//          always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//          1st string- displayed if current entry buffer is not empty
//          2nd string- displayed if current entry buffer is empty
//          This function is almost the same to the function fnfBufferPartialStringSelect()
//
// MODIFICATION HISTORY:
//          Mozdzer    Initial version
// *************************************************************************/
BOOL fnfBufferEmptyStringSelect ( UINT16 *pFieldDest, 
                                  UINT8   bLen, 
                                  UINT8   bFieldCtrl, 
                                  UINT8   bNumTableItems, 
                                  UINT8  *pTableTextIDs, 
                                  UINT8  *pAttributes )
{
    UINT8  ucStringNum;
    
    if (fnIsBufferBlank() != TRUE)
    {
        ucStringNum = 0;
    }
    else
    {
        ucStringNum = 1;
    }

    fnDisplayTableText( pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                        pTableTextIDs, ucStringNum);

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnfSleepModeTimerInit
//
// DESCRIPTION:
//          Entry field function to initialize the entry field where the 
//          sleep mode timer is set in minute(s). Maximum time allowed is 
//          10 minutes (sleep mode ECR) 
//
// INPUTS:
//          Standard field function inputs.
//
// RETURNS:
//          always returns SUCCESSFUL
//
// WARNINGS/NOTES: 
//          None.
//
// MODIFICATION HISTORY:
// 
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                   Victor Li  Initial version  
//
// *************************************************************************/
BOOL fnfSleepModeTimerInit (  UINT16 *pFieldDest, 
                              UINT8   bLen, 
                              UINT8   bFieldCtrl, 
                              UINT8   bNumTableItems, 
                              UINT8  *pTableTextIDs, 
                              UINT8  *pAttributes )
{
    BOOL    fRetval = FAILURE;

    /* use the general timeout field init function */
    fRetval = fnTimeoutInit(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs,
                fnCMOSSetupGetCMOSSetupParams()->wSleepTimeout);

    return (fRetval);
}


/* *************************************************************************
// FUNCTION NAME:
//        fnfFundsOrCreditShow
//
// DESCRIPTION:
//        Output field function for showing the info Funds or Credit.
// 
// INPUTS:
//        Standard field function inputs.
//
// RETURNS:
//        always returns successful
//
// WARNINGS/NOTES:  
//        None.
//
// MODIFICATION HISTORY:
// 
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                    David Huang  Initial version  
//
// *************************************************************************/
BOOL fnfFundsOrCreditShow (   UINT16 *pFieldDest, 
                              UINT8   bLen, 
                              UINT8   bFieldCtrl, 
                              UINT8   bNumTableItems, 
                              UINT8  *pTableTextIDs, 
                              UINT8  *pAttributes )
{
    UINT8   bWhichString = 0xFF;
    UINT8   bMeterRefillType = 0 ;

    /* initialize the buffer to spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);
    bMeterRefillType = fnGetMeterRefillType() ;
    
    if ( REFILL_IS_BY_CREDIT_LIMITING == bMeterRefillType )
    {
        bWhichString = 1;
    }
    else if (  ( REFILL_IS_BY_PV_DOWNLOAD == bMeterRefillType ) ||
                ( REFILL_IS_BY_KEY_PAD == bMeterRefillType) )
    {
        bWhichString = 0;
    }
    else
    {
        bWhichString = 0;
    }

    fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, bWhichString);

    return ((BOOL) SUCCESSFUL); 
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfDefValueEntryInit
//
// DESCRIPTION: 
//      General Init entry field to display default value
//
// PRE-CONDITIONS:
//      The Unicode string of default value has been set into puniDefValueStr.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always successful
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 11/30/2005   Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnfDefValueEntryInit (UINT16   *pFieldDest, 
                           UINT8    bLen,   
                           UINT8    bFieldCtrl, 
                           UINT8    bNumTableItems,
                           UINT8    *pTableTextIDs,
                           UINT8    *pAttributes)
{
    
    (void)fnfStdEntryInit(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                pTableTextIDs, pAttributes);

    // Put Unicode string of the default value into entry buffer
    fnBufferDefaultValue();
    fDefValueState = TRUE;
    
    return (fnfStdEntryRefresh(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                pTableTextIDs, pAttributes));
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfDefValueEntryRefresh
//
// DESCRIPTION: 
//      General Refresh entry field to display default value
//
// PRE-CONDITIONS:
//      The Unicode string of default value has been set into puniDefValueStr.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always successful
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 11/30/2005   Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnfDefValueEntryRefresh (UINT16    *pFieldDest, 
                              UINT8     bLen,   
                              UINT8     bFieldCtrl, 
                              UINT8     bNumTableItems,
                              UINT8     *pTableTextIDs,
                              UINT8     *pAttributes)
{
    return (fnfStdEntryRefresh(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                pTableTextIDs, pAttributes));
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfEntryErrorInfo
//
// DESCRIPTION: 
//      Field function to display the error indicator at footer area. 
//      If you want to show error prompt in a screen which need to
//      refresh from time to time, please add the following lines in
//      your function:
//
//           fnSetEntryErrorCode (ENTRY_INVALID_VALUE);
//
//           fnShowError(TRUE);
//
// INPUTS:
//      Standard field function inputs.
//
// TEXT TABLE USAGE:  
//      1st string- Entry Not Valid
//
// RETURNS:
//      SUCCESSFUL
//
// MODIFICATION HISTORY:
// 05/25/2011   Jane, Li    Don't refresh the whole screen and only 
//                              refresh the field that displays error info.
// 06/23/2009   Joey Cui         Fix fraca 163022 and 161091
// 03/31/2009  Jingwei,Li        Don't display background color when no warning/error.
// 09/01/2006   Shiming Sun      Update for the screens that need to 
//                               refresh from time to time.
// 12/02/2005   Shiming Sun      Initial Build
//
// *************************************************************************/
BOOL fnfEntryErrorInfo(UINT16  *pFieldDest, 
                       UINT8    bLen,   
                       UINT8    bFieldCtrl, 
                       UINT8    bNumTableItems, 
                       UINT8   *pTableTextIDs, 
                       UINT8   *pAttributes)
{
    UINT8 bWhichString = bEntryErrorCode;
    
    T_SCREEN_ID usScreen = 0;

    UINT8           bHardwareMajorVersion;
    UINT8           bDontCare;
    
    SET_SPACE_UNICODE(pFieldDest, bLen);
       
    if( bEntryErrorCode != ENTRY_NO_ERROR )
    {
    
        fnOITSetToNormalVideo(pAttributes);
        if( bNumTableItems > bWhichString )
        {
            fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                            pTableTextIDs, bWhichString );
/*          if(bShowError == TRUE)
            {
                bShowError = FALSE;
            }
            else
            {
*/
            bEntryErrorCode = ENTRY_NO_ERROR;
//          }
            //bShowError2 =TRUE;
        }
    }
    else
    {
       // if it once showed error, refresh itself again
       // to redraw the field
       /*
       if(bShowError2 == TRUE)
       {
            bShowError2 = FALSE;
            bRefreshScreen = TRUE;
            usScreen = fnGetCurrentScreenID();
           fnChangeScreenAfterError(usScreen, FALSE, FALSE); 
        }
       */
       fnOITSetToReverseVideo(pAttributes);
       // *pFieldDest = NULL_VAL;
    }
    return ((BOOL) SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfCurrencySignFront
//
// DESCRIPTION: 
//      Output field function that displays Currency Sign in front of the amount,
//              PCN specific.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// MODIFICATION HISTORY:
//  07/25/2007  Joey Cui    Reuse from JNUS
//
// *************************************************************************/
BOOL fnfCurrencySignFront ( UINT16  *pFieldDest, 
                            UINT8    bLen,   
                            UINT8    bFieldCtrl, 
                            UINT8    bNumTableItems, 
                            UINT8   *pTableTextIDs, 
                            UINT8   *pAttributes)
{
    UINT8   bWhichString;   
    UINT8   bCurrencySymbolPlacement;

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    bCurrencySymbolPlacement = fnFlashGetByteParm(BP_CURRENCY_SYM_PLACEMENT);
    /* check if the currency symbol goes before or after the money amount */
    if (bCurrencySymbolPlacement == 0)        /* Placement = BEFORE */
    {
        fnfMoneySign (pFieldDest,bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, pAttributes);
    }
    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfCurrencySignAfter
//
// DESCRIPTION: 
//      Output field function that displays Currency Sign after the amount,
//              PCN specific.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// MODIFICATION HISTORY:
//  07/25/2007  Joey Cui    Reuse from JNUS
//
// *************************************************************************/
BOOL fnfCurrencySignAfter ( UINT16  *pFieldDest, 
                            UINT8    bLen,   
                            UINT8    bFieldCtrl, 
                            UINT8    bNumTableItems, 
                            UINT8   *pTableTextIDs, 
                            UINT8   *pAttributes)
{
    UINT8   bWhichString;   
    UINT8   bCurrencySymbolPlacement;

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    bCurrencySymbolPlacement = fnFlashGetByteParm(BP_CURRENCY_SYM_PLACEMENT);
    /* check if the currency symbol goes before or after the money amount */
    if (bCurrencySymbolPlacement != 0)        /* Placement = AFTER */
    {
        fnfMoneySign (pFieldDest,bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, pAttributes);
    }
    return ((BOOL) SUCCESSFUL);
}
/* *************************************************************************
// FUNCTION NAME: 
//           fnfScrollableEntryEditOption
//
// DESCRIPTION:
//           Output field function that displays "Edit Options" string, and dont' 
//           allow edit scrollable entry for RTL.
// INPUTS:
//           Standard field function inputs.
//
// RETURNS:
//           always returns SUCCESSFUL
//
// WARNINGS/NOTES:  
//           1st string- displayed if current entry buffer has something
//           2nd string- displayed if current entry buffer does not have anything
//
// MODIFICATION HISTORY:
//           Jingwei,Li 06/01/2010      Initial version
// *************************************************************************/
BOOL fnfScrollableEntryEditOption ( UINT16 *pFieldDest, 
                                    UINT8   bLen, 
                                    UINT8   bFieldCtrl, 
                                    UINT8   bNumTableItems, 
                                    UINT8  *pTableTextIDs, 
                                    UINT8  *pAttributes )
{
    UINT8  ucStringNum;
    UINT8   ubWritingDir;

    ubWritingDir = fnFlashGetByteParm(BP_LANGUAGE_WRITING_DIR);

    //don't allow edit scrollable entry for RTL.
    if ((fnCharsBuffered() > 0) && (ubWritingDir == 0))
    {
        ucStringNum = 0;
    }
    else
    {
        ucStringNum = 1;
    }

    fnDisplayTableText(pFieldDest,bLen,bFieldCtrl,bNumTableItems,pTableTextIDs,ucStringNum);

    return ((BOOL) SUCCESSFUL);
}


/*--------------------------*/
/*   Hard Key Functions     */
/*--------------------------*/



/* *************************************************************************
// FUNCTION NAME: 
//           fnhkBufferKeyStd
//
// DESCRIPTION:
//           Hard key function that buffers the keypress in the current
//           entry buffer using the standard buffering algorithm with no
//           special exceptions.
//
// INPUTS:
//           Standard hard key function inputs.(format 1)
//
// RETURNS:
//           Next screen:  This hard key function always stays on the existing screen. 
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//           Mozdzer   Initial version 
// *************************************************************************/
T_SCREEN_ID   fnhkBufferKeyStd( UINT8       bKeyCode, 
                                T_SCREEN_ID usNextScr1, 
                                T_SCREEN_ID usNextScr2 )
{
    fnBufferKey(bKeyCode);
    
    return (0);
}



/* *************************************************************************
// FUNCTION NAME: 
//           fnhkBufferKeyScroll
//
// DESCRIPTION:
//           Hard key function that buffers the keypress in the scroll entry buffer
//
// INPUTS:
//           Standard hard key function inputs.(format 1)
//
// RETURNS:
//           Next screen:  This hard key function always stays on the existing screen. 
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//           04/07/2006  Kan Jiang/Oscar   Initial version 
// *************************************************************************/
T_SCREEN_ID  fnhkBufferKeyScroll( UINT8       bKeyCode, 
                                  T_SCREEN_ID usNextScr1, 
                                  T_SCREEN_ID usNextScr2 )
{
    fnBufferKeyScroll(bKeyCode);
    
    return (0);
}


T_SCREEN_ID  fnhkBufferKeyScrollSS1( UINT8       bKeyCode, 
                                     T_SCREEN_ID usNextScr1, 
                                     T_SCREEN_ID usNextScr2 )
{
    fnChangeShiftState(SHIFT_KEY_STATUS1);
    fnBufferKeyScroll(bKeyCode);
    
    return (0);
}


T_SCREEN_ID  fnhkBufferKeyScrollSS2( UINT8       bKeyCode, 
                                     T_SCREEN_ID usNextScr1, 
                                     T_SCREEN_ID usNextScr2 )
{
    fnChangeShiftState(SHIFT_KEY_STATUS2);
    fnBufferKeyScroll(bKeyCode);
    
    return (0);
}


/* *************************************************************************
// FUNCTION NAME: 
//          fnhkBufferKeyIffNotEmpty
//
// DESCRIPTION:
//          Hard key function that buffers the keypress in the current
//          entry buffer if and only if the buffer is not currently empty.
//          (used to allow spaces in entry fields but not as the leading character)
//
// INPUTS:
//          Standard hard key function inputs (format 1).
//
// RETURNS:
//          Next screen:always stays on the same screen
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//         04/10/2006  Kan Jiang/Oscar   Modified for overwrite mode field    
//          Zamary     Initial version 
// *************************************************************************/
T_SCREEN_ID    fnhkBufferKeyIffNotEmpty( UINT8       bKeyCode, 
                                         T_SCREEN_ID usNextScr1, 
                                         T_SCREEN_ID usNextScr2 )
{
    if(fnCharsBuffered() > 0)
    {
         if ( rEntryBuf.fInsert == TRUE)
         {
             fnBufferKey(bKeyCode);
         }
         else
         {
             fnBufferKeyScroll( bKeyCode);
         }
    }

    return (0);
}


/* *************************************************************************
// FUNCTION NAME: 
//          fnhkBufferKeyScrollIffNotEmpty
//
// DESCRIPTION:
//          Hard key function that buffers the keypress in the current
//          entry buffer if and only if the buffer is not currently empty.
//          (used to allow spaces in entry fields but not as the leading character)
//
// INPUTS:
//          Standard hard key function inputs (format 1).
//
// RETURNS:
//          Next screen:always stays on the same screen
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//   09/05/2007  Oscar Wang  Initial version to fix FRACA GMSE00128857.
// *************************************************************************/
T_SCREEN_ID    fnhkBufferKeyScrollIffNotEmpty( UINT8       bKeyCode, 
                                         T_SCREEN_ID usNextScr1, 
                                         T_SCREEN_ID usNextScr2 )
{
    if(fnCharsBuffered() > 0)
    {
        fnBufferKeyScroll( bKeyCode);
    }

    return (0);
}

/* *************************************************************************
// FUNCTION NAME: 
//           fnhkBufferKeyStdSS1
//
// DESCRIPTION:
//           Hard key function that buffers the keypress in the current
//           entry buffer using the standard buffering algorithm, after
//           first setting the shift state to 1.
//
// INPUTS:
//           Standard hard key function inputs (format 1).
//
// RETURNS:
//           Next screen:always stays on the same screen
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//           Mozdzer     Initial version
// *************************************************************************/
T_SCREEN_ID    fnhkBufferKeyStdSS1( UINT8       bKeyCode, 
                                    T_SCREEN_ID usNextScr1, 
                                    T_SCREEN_ID usNextScr2 )
{
    fnChangeShiftState(SHIFT_KEY_STATUS1);
    
    fnBufferKey(bKeyCode);
    
    return (0);
}



/* *************************************************************************
// FUNCTION NAME: 
//           fnhkBufferKeyStdSS2
//
// DESCRIPTION:
//           Hard key function that buffers the keypress in the current
//           entry buffer using the standard buffering algorithm, after
//           first setting the shift state to 2.
//
// INPUTS:
//           Standard hard key function inputs (format 1).
//
// RETURNS:
//           Next screen:always stays on the same screen
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//           Mozdzer        Initial version 
// *************************************************************************/
T_SCREEN_ID    fnhkBufferKeyStdSS2( UINT8       bKeyCode, 
                                    T_SCREEN_ID usNextScr1, 
                                    T_SCREEN_ID usNextScr2 )
{
    fnChangeShiftState(SHIFT_KEY_STATUS2);
    
    fnBufferKey(bKeyCode);
    
    return (0);
}


/* *************************************************************************
// FUNCTION NAME: 
//           fnhkBufferKeyStdSS3
//
// DESCRIPTION:
//           Hard key function that buffers the keypress in the current
//           entry buffer using the standard buffering algorithm, after
//           first setting the shift state to 3.
//
// INPUTS:
//           Standard hard key function inputs (format 1).
//
// RETURNS:
//           Next screen:always stays on the same screen
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//           Mozdzer        Initial version 
// *************************************************************************/
T_SCREEN_ID    fnhkBufferKeyStdSS3( UINT8       bKeyCode, 
                                    T_SCREEN_ID usNextScr1, 
                                    T_SCREEN_ID usNextScr2 )
{
    fnChangeShiftState(SHIFT_KEY_STATUS3);

    fnBufferKey(bKeyCode);

    return (0);
}



/* *************************************************************************
// FUNCTION NAME: 
//           fnhkBufferKeyAlphaChar
//
// DESCRIPTION:
//           Hard key function that buffers a key  try to convert to Alpha or
//           digital.   This function filters out invalid characters.
//           The valid characters are numbers '0' - '9' , the decimal point '.'
//           and the comma ',' character.
//
// INPUTS:
//           Standard hard key function inputs (format 1).
//
// RETURNS:
//           usNextScr1
//
// WARNINGS/NOTES:  
//           
//
// MODIFICATION HISTORY:
// 
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                    David Huang  Initial version  
//
// *************************************************************************/
T_SCREEN_ID fnhkBufferKeyAlphaChar( UINT8       bKeyCode, 
                                    T_SCREEN_ID usNextScr1, 
                                    T_SCREEN_ID usNextScr2 )
{
    BOOL            fProcessIt;
    UINT16          usNextChar;
    UINT16          wDecimalChar;
    UINT32          wBinary;
    static UINT8    wKeyPressTimes = 0;
    register UINT16 i = 0;


    fProcessIt = fnSearchKeyMap(bKeyCode, &usNextChar);
    wDecimalChar = fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR);
    fnUnicode2Bin( &usNextChar, &wBinary, 1);
    i = ( UINT16 ) wBinary;

    if ((fProcessIt == TRUE) && 
        ((isdigit(usNextChar) == TRUE) || (usNextChar == wDecimalChar) || (usNextChar == (UINT16)'.')))
    {
        if ((usNextChar == wDecimalChar) || (usNextChar == (UINT16)'.'))
        { 
            wUpperLowerToggle = ( wUpperLowerToggle == 0 ) ? 1 : 0;
        }
        else
        {
            /* stop the screen tick timer here */
            OSStopTimer(SCREEN_TICK_TIMER);

            if ( i<ALPHA_USED_DIGITAL_KEY )
            {
                if ( i == wKeyCurrentDigitalChar )
                {
                       wKeyPressTimes += 2;
                       if (fnCharsBuffered() == TRUE)
                       {
                           fnDeleteCharacter();
                       }
                }
                else
                {
                    wKeyCurrentDigitalChar = i;
                    wKeyPressTimes = 0 ;
                }

                if ( i >= ALPHA_UNICODE_TABLE_OFFSET )
                {
                    UINT16 hi;
                    UINT8 x, lo;
                    if ( fnCharsBuffered() == TRUE)
                    {
                        x = wUpperLowerToggle * 8 + i - ALPHA_UNICODE_TABLE_OFFSET;
                    }
                    else
                    {
                        x = i-ALPHA_UNICODE_TABLE_OFFSET;
                    }

restart:
                    hi = pAlphaUnicodeTable[x][wKeyPressTimes];
                    lo = pAlphaUnicodeTable[x][wKeyPressTimes + 1];
                    wKeyUnicodeJustInput = (hi << 8) | lo;
                    
                    if (wKeyUnicodeJustInput == 0)
                    {
                        /* restart a cycle */
                        wKeyPressTimes = 0;
                        goto restart;
                    }
                }
                else
                {
                    UINT16 hi;
                    UINT8 lo;
restart0:
                    if ( i==0 )
                    {
                        hi = pAlphaZeroOrSpace[wKeyPressTimes];
                        lo = pAlphaZeroOrSpace[wKeyPressTimes + 1];
                    }
                    else
                    {
                        hi = pAlphaSpecialUnicode[wKeyPressTimes];
                        lo = pAlphaSpecialUnicode[wKeyPressTimes + 1];
                    }

                    wKeyUnicodeJustInput = (hi << 8) | lo;

                    if (wKeyUnicodeJustInput == 0)
                    {
                        wKeyPressTimes = 0;
                        goto restart0;
                    }
                }

                fnBufferUnicode2( wKeyUnicodeJustInput );
            }       

            /* restart the timer */
            OSStartTimer(SCREEN_TICK_TIMER);
            /* reset the flag */
            fScreenTickEventQueued = FALSE;
        }

    }
    return (usNextScr1);
}


/* *************************************************************************
// FUNCTION NAME: 
//           fnhkBufferKeyPointWithSwitch
//
// DESCRIPTION:
//           The point '.'  hard key function. Do not buffer the '.' 
//           when ALPHA_ENTRY_OFF.
//
// INPUTS:
//           Standard hard key function inputs (format 1).
//
// RETURNS:
//           usNextScr1
//
// WARNINGS/NOTES:  
//           
//
// MODIFICATION HISTORY:
// 
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                 Tim Zhang      Initial version  
//
// *************************************************************************/
T_SCREEN_ID fnhkBufferKeyPointWithSwitch ( UINT8       bKeyCode, 
                                           T_SCREEN_ID usNextScr1, 
                                           T_SCREEN_ID usNextScr2 )
{
    T_SCREEN_ID usNext = 0;
    
    if ( bAlphaEntrySwitch == ALPHA_ENTRY_ON )
    {
        usNext = fnhkBufferKeyAlphaChar( bKeyCode, usNextScr1, usNextScr2 );
    }

    return ( usNext );
}


/* *************************************************************************
// FUNCTION NAME: 
//           fnhkBufferKeyHexNumber
//
// DESCRIPTION:
//           Hard key function that buffers the keypress in the current entry 
//           buffer using the standard buffering algorithm with limitation.
//           Only allow '0'~'9' and 'a'~'f'
//
// INPUTS:
//           Standard hard key function inputs (format 1).
//
// RETURNS:
//           None.
//
// WARNINGS/NOTES:  
//           This hard key function always stays on the existing screen.
//
// MODIFICATION HISTORY:
// 
//      03/18/2009    Raymond Shen  Initial version  
//
// *************************************************************************/
T_SCREEN_ID fnhkBufferKeyHexNumber( UINT8       bKeyCode, 
                                       T_SCREEN_ID usNextScr1, 
                                       T_SCREEN_ID usNextScr2 )
{
    BOOL            fProcessIt;
    UINT16          usNextChar;

    fProcessIt = fnSearchKeyMap(bKeyCode, &usNextChar);
    
    if ((fProcessIt == TRUE) && (fnIsUnicodeHex(usNextChar) == TRUE) )
    {
        // buffer the key if it's UnicodeHex
        fnBufferKey(bKeyCode);
    }

    return (0);
}

/* *************************************************************************
// FUNCTION NAME: 
//           fnhkBufferKeyHexNumberSS1
//
// DESCRIPTION:
//           Hard key function that buffers the keypress in the current entry 
//           buffer using the standard buffering algorithm with limitation.
//           Only allow '0'~'9' and 'A'~'F'
//
// INPUTS:
//           Standard hard key function inputs (format 1).
//
// RETURNS:
//           None.
//
// WARNINGS/NOTES:  
//           This hard key function always stays on the existing screen.
//
// MODIFICATION HISTORY:
// 
//      03/18/2009    Raymond Shen  Initial version  
//
// *************************************************************************/
T_SCREEN_ID fnhkBufferKeyHexNumberSS1( UINT8       bKeyCode, 
                                       T_SCREEN_ID usNextScr1, 
                                       T_SCREEN_ID usNextScr2 )
{
    BOOL            fProcessIt;
    UINT16          usNextChar;

    fnChangeShiftState(SHIFT_KEY_STATUS1);

    fProcessIt = fnSearchKeyMap(bKeyCode, &usNextChar);
    
    if ((fProcessIt == TRUE) && (fnIsUnicodeHex(usNextChar) == TRUE) )
    {
        // buffer the key if it's UnicodeHex
        fnBufferKey(bKeyCode);
    }

    return (0);
}


/* *************************************************************************
// FUNCTION NAME: 
//           fnhkShiftState1
//
// DESCRIPTION:
//           Hard key function that sets the shift state to 1.
//
// INPUTS:
//           Standard hard key function inputs (format 1).
//
// RETURNS:
//           Next screen:always goes to usNextScr1
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//           Mozdzer    Initial version
// *************************************************************************/
T_SCREEN_ID   fnhkShiftState1 ( UINT8       bKeyCode, 
                                T_SCREEN_ID usNextScr1, 
                                T_SCREEN_ID usNextScr2 )
{
    fnChangeShiftState(SHIFT_KEY_STATUS1);
    
    return (usNextScr1);
}



/* *************************************************************************
// FUNCTION NAME: 
//           fnhkShiftState2
//
// DESCRIPTION:
//           Hard key function that sets the shift state to 2.
//
// INPUTS:
//           Standard hard key function inputs (format 1).
//
// RETURNS:
//           Next screen:always goes to usNextScr1
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//           Mozdzer     Initial version
// *************************************************************************/
T_SCREEN_ID   fnhkShiftState2 ( UINT8       bKeyCode, 
                                T_SCREEN_ID usNextScr1, 
                                T_SCREEN_ID usNextScr2 )
{
    fnChangeShiftState(SHIFT_KEY_STATUS2);
    
    return (usNextScr1);
}


/* *************************************************************************
// FUNCTION NAME: 
//          fnhkBufferKeyClear1st
//
// DESCRIPTION:
//          Hard key function that buffers the keypress in the current
//          entry buffer, clearing the initial value of the buffer beforehand 
//          if it is the first key entered.
//
// INPUTS:
//          Standard hard key function inputs (format 1).
//
// RETURNS:
//          Next screen:This hard key function always stays on the existing screen.
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//          Mozdzer      Initial version
// *************************************************************************/
T_SCREEN_ID    fnhkBufferKeyClear1st( UINT8       bKeyCode, 
                                      T_SCREEN_ID usNextScr1, 
                                      T_SCREEN_ID usNextScr2 )
{
    fnBufferKeyClear1st(bKeyCode);
    
    return (0);
}



/* *************************************************************************
// FUNCTION NAME: 
//           fnhkDeleteCharacter
//
// DESCRIPTION:
//           Deletes a character from the current entry buffer.
//
// INPUTS:
//           Standard hard key function inputs (format 1).
//
// RETURNS:
//           Next screen:This hard key function always stays on the existing screen.
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//  09/15/2006   Dicky Sun   Update for clearing error prompt
//               Mozdzer     Initial version
// *************************************************************************/
T_SCREEN_ID  fnhkDeleteCharacter( UINT8       bKeyCode, 
                                  T_SCREEN_ID usNextScr1, 
                                  T_SCREEN_ID usNextScr2 )
{
    fnDeleteCharacter();
    
    fnSetEntryErrorCode(ENTRY_NO_ERROR);
    return (0);
}


/* *************************************************************************
// FUNCTION NAME: 
//           fnhkDeleteCharacterScroll
//
// DESCRIPTION:
//           Deletes a character from the current entry buffer.
//           Only used for overwrite-type field
//
// INPUTS:
//           Standard hard key function inputs (format 1).
//
// RETURNS:
//           Next screen:This hard key function always stays on the existing screen.
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//           01/08/2014  Wang Biao   Changed codes for Network Proxy Settings.
//           04/07/2006  Kan Jiang/Oscar    Initial version
// *************************************************************************/
T_SCREEN_ID  fnhkDeleteCharacterScroll( UINT8       bKeyCode, 
                                  T_SCREEN_ID usNextScr1, 
                                  T_SCREEN_ID usNextScr2 )
{
    //The following two flags are only working for the proxy address, username and password editing screen
	//Because other screens don't use the two flags, we can just put them in this function. 
	//Don't need a new hard key function.
	bIsTimeOut = TRUE;
	//when enter password edit screen from previous screen, keep the last password char is displayed by asterisk after field is full.
	if(fPersistLastAsterisk == TRUE)
	{
		fPersistLastAsterisk = FALSE;
	}
    fnDeleteCharacterScroll();
    
    return (0);
}



/* *************************************************************************
// FUNCTION NAME: 
//           fnhkDeleteCharScrollOrChangeScr
//
// DESCRIPTION:
//         Deletes a character from the current entry buffer.  If no 
//         characters were previously typed into the buffer (buffer is
//         already clear), go to next screen 1 instead.
//
// INPUTS:
//           Standard hard key function inputs (format 1).
//
// RETURNS:
//         Next screen:  if buffer is already clear, goes to next screen 1 
//                       stays on the same screen otherwise
//
// WARNINGS/NOTES:  
//           None
//
// MODIFICATION HISTORY:
//           01/08/2014  Wang Biao   Changed codes for Network Proxy Settings.
//           04/07/2006  Kan /Oscar    Initial version
// *************************************************************************/
T_SCREEN_ID  fnhkDeleteCharScrollOrChangeScr( UINT8        bKeyCode, 
                                        UINT8        bNumNext,
                                        T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID  usNext = 0;

    if (fnCharsBuffered() > 0)
    {
        //The following two flags are only working for the proxy address, username and password editing screen
		//Because other screens don't use the two flags, we can just put them in this function. 
		//Don't need a new hard key function.
	    bIsTimeOut = TRUE;
	    
        //when enter password edit screen from previous screen, keep the last password char is displayed by asterisk after field is full.
	    if(fPersistLastAsterisk == TRUE)
		{
			fPersistLastAsterisk = FALSE;
		}
		
        fnDeleteCharacterScroll();
    }
    else
    {
        if (bNumNext > 0)
        {
            usNext = pNextScreens[0];
        }
    }

    return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: 
//         fnhkDeleteCharOrChangeScrKIPPwd
//
// DESCRIPTION:
//         Deletes a character from the current entry buffer.  If no 
//         characters were previously typed into the buffer (buffer is
//         already clear), go to next screen 1 instead.
//
// INPUTS:
//         Standard hard key function inputs (format 2).
//
// RETURNS:
//         Next screen:  if buffer is already clear, goes to next screen 1 
//                       stays on the same screen otherwise
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
// 
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                    David Huang  Initial version  
//
// *************************************************************************/
T_SCREEN_ID fnhkDeleteCharOrChangeScrKIPPwd(UINT8        bKeyCode, 
                                            UINT8        bNumNext,
                                            T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID  usNext = 0;

    if (fnCharsBuffered() != 0)
    {
        fnDeleteCharacter();
    }
    else
    {
        switch(pKIPPasswordInfo->ucPasswordSelectPurpose)
        {
            case KIPW_HIGH_VALUE_WARN:
                break;
                
            case KIPW_POSTAGE_ENTER_REQUIRED:
                break;          // lint requirement
                
            case KIPW_BATCH_LIMIT_REACHED :
                    fnModeSetDefault();
                break;
                
            case KIPW_PRESET_ENTER_REQUIRED:
                    fnModeSetDefault();
                break;
                
            default:
                break;
        }
        pKIPPasswordInfo->ucPasswordStatus = KIP_PW_FORCE_TO_EXIT ;
        pKIPPasswordInfo->lwPostageValue = 0;
        pKIPPasswordInfo->ucPasswordSelectPurpose = 0;
        fHighPostagePending = FALSE;

        if( bNumNext != 0 )
        { 
            usNext = pNextScreens[0];
        }
    }

    return (usNext);

}



/* *************************************************************************
// FUNCTION NAME: 
//         fnhkResendKeyDown
//
// DESCRIPTION:
//         Hard key function that transitions to the next screen and resends
//         the key which got us here to the OIT task.  This allows a keypress 
//         to cause a screen transition and then look like it was pressed down
//         on the new screen immediately.  Note that when the key message
//         is resent, any shift information is lost and the direction is
//         always down (regardless of if a shifted or released key got us here).
//
// INPUTS:
//         Standard hard key function inputs (format 1).
//
// RETURNS:
//         Next screen:  Always transitions to the usNextScr1, which should never be 0.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//  12/11/2009  Raymond Shen    Doesn't accept decimal point for France.
//         Mozdzer     Initial version
// *************************************************************************/
T_SCREEN_ID  fnhkResendKeyDown( UINT8       bKeyCode, 
                                T_SCREEN_ID usNextScr1, 
                                T_SCREEN_ID usNextScr2 )
{
    KEY_BUFFER   *pKeyBuffer = fnGetKeyBuffer();
    UINT16          usUnicodeDecimalChar;   // The character used for decimal point in this country.
    UINT16          usNextChar;             // Character the current key maps to.
    BOOL    fIsTheKeyAcceptable = TRUE;
    T_SCREEN_ID     usNext = usNextScr1;

    /* get the unicode decimal point character for this pcn */
    usUnicodeDecimalChar = fnGetDecimalChar();

    /* find out what the entered character maps to */
    if (fnSearchKeyMap(bKeyCode, &usNextChar) == FALSE)
    {
        /* the character is automatically not buffered if it doesn't map to 
            anything */
        fIsTheKeyAcceptable = FALSE;
        usNext = 0;
    }
    else if ( (fnFlashGetByteParm( BP_DEFAULT_RATING_COUNTRY_CODE ) == FRANCE) &&
                    (usNextChar == usUnicodeDecimalChar) )
    {
        // France doesn't allow inputting decimal point for KIP
        fIsTheKeyAcceptable = FALSE;
        usNext = 0;
    }

    if ( (pKeyBuffer->fFull == FALSE) && (fIsTheKeyAcceptable == TRUE) )
    {
        pKeyBuffer->fFull    = TRUE;
        pKeyBuffer->bKeyCode = bKeyCode;
        /* key is always down and unshifted.  if we need other versions of this function, 
           they can be written on a case by case basis. */
        pKeyBuffer->bDirection = KEYDOWN;
        pKeyBuffer->fControl   = 0;
    }
        
    return (usNext);
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnhkBufClear
//
// DESCRIPTION:
//         Hard key function that does one of the following:
//         1) If characters have been typed into the current entry buffer,
//            the entry buffer is cleared and we stay on the same screen.
//         2) If characters have not been typed into the current entry buffer, we
//            don't do change anythign and we stay on the same screen.
//
// INPUTS:
//         Standard hard key function inputs (format 2).
//
// RETURNS:
//         Next screen: we stay on the same screen.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//   24-Mar-14  Wang Biao       Modified to fix fraca 224777.
//   11-Dec-12  S. Peterson     Initial version 
// *************************************************************************/
T_SCREEN_ID fnhkBufClear( UINT8        bKeyCode, 
                          UINT8        bNumNext,
                          T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID  usNext = 0; 

    if (fnCharsBuffered() > 0)
    {
        fnClearEntryBuffer();
    }
    //when enter password edit screen from previous screen, keep the last password char is displayed by asterisk after field is full.
	if(fPersistLastAsterisk == TRUE)
	{
		fPersistLastAsterisk = FALSE;
	}
    return (usNext);
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnhkBufClearOrChangeScr
//
// DESCRIPTION:
//         Hard key function that does one of the following:
//         1) If characters have been typed into the current entry buffer,
//            the entry buffer is cleared and we stay on the same screen.
//         2) If characters have not been typed into the current entry buffer
//            (or any characters which were typed were already cleared), we
//            go to the screen specified in input argument usNextScr1.
//
// INPUTS:
//         Standard hard key function inputs (format 1).
//
// RETURNS:
//         Next screen: If entry buffer is clear, this function transitions to usNextScr1.
//                      If entry buffer is not clear, we stay on the same screen.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//         Mozdzer    Initial version 
// *************************************************************************/
T_SCREEN_ID fnhkBufClearOrChangeScr( UINT8       bKeyCode, 
                                     T_SCREEN_ID usNextScr1, 
                                     T_SCREEN_ID usNextScr2 )
{
    T_SCREEN_ID  usNext = 0; 

    if (fnCharsBuffered() > 0)
    {
        fnClearEntryBuffer();
    }
    else
    {
        usNext = usNextScr1;
    }

    return (usNext);
}


/* *************************************************************************
// FUNCTION NAME: 
//         fnhkBufBlankOrChangeScr
//
// DESCRIPTION:
//         Hard key function that does one of the following:
//         1) If the current entry buffer is not blank (all spaces),
//            the entry buffer is cleared and we stay on the same screen.
//         2) If the current entry buffer is blank, we
//            go to the screen specified in input argument usNextScr1.
//
//         This is the same as fnhkBufClearOrChangeScr except that it
//         looks to see if there is anything in the buffer rather than
//         if anything has been entered to trigger the clear operation.
//         This makes a difference when a buffer is initialized to a
//         non-blank string.
//
// INPUTS:
//         Standard hard key function inputs (format 1).
//
// RETURNS:
//         Next screen: If entry buffer is clear, this function transitions to usNextScr1.
//                      If entry buffer is not clear, we stay on the same screen.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
// 04/12/2007  Oscar Wang  Merge from Mega
//         Mozdzer    Initial version 
// *************************************************************************/
T_SCREEN_ID fnhkBufBlankOrChangeScr( UINT8       bKeyCode, 
                                     T_SCREEN_ID usNextScr1, 
                                     T_SCREEN_ID usNextScr2 )
{
    T_SCREEN_ID  usNext = 0; 

    if (fnIsBufferBlank() == FALSE)
    {
        fnClearEntryBuffer();
    }
    else if (fnGetAutoDunsState() == AUTO_DUNS_REQUESTED)
    {
        usNext = usNextScr2;/* Return to screen that triggered auto duns */
    }
    else
    {
        usNext = usNextScr1;
    }

    return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: 
//         fnhkBufBlankOrToPreviousScr
//
// DESCRIPTION:
//         Hard key function that does one of the following:
//         1) If the current entry buffer is not blank (all spaces),
//            the entry buffer is cleared and we stay on the same screen.
//         2) If the current entry buffer is blank, we go to the previous screen
//
// INPUTS:
//         Standard hard key function inputs (format 2).
//
// RETURNS:
//         Next screen: If entry buffer is clear, this function transitions to 
                                                    previous screen.
//                      If entry buffer is not clear, we stay on the same screen.
//
// WARNINGS/NOTES:  
//         None
//
// MODIFICATION HISTORY:
//         Mozdzer    Initial version 
// *************************************************************************/
T_SCREEN_ID fnhkBufBlankOrToPreviousScr( UINT8       bKeyCode, 
                                         UINT8          bNumNext, 
                                         T_SCREEN_ID *  pNextScreens)
{
    T_SCREEN_ID  usNext = 0; 

    if (fnIsBufferBlank() == FALSE)
    {
        fnClearEntryBuffer();
    }
    else
    {
        usNext = fnGetPreviousScreenID();
    }

    return (usNext);
}

/* *************************************************************************
// FUNCTION NAME: 
//          fnhkToggleField
//
// DESCRIPTION:
//          Hard key function that toggles the selection in the general
//          purpose binary choice field.
//
// INPUTS:
//          Standard hard key function inputs (format 1).
//
// RETURNS:
//          Next screen:   always goes to usNextScr1
//
// WARNINGS/NOTES: 
//          If/when the need arises to handle other types of choice fields,
//          possibly with 3 or more selection possiblities, this function
//          and the choice field architecture should be modified to be
//          more generic.  (This function should be renamed if this happens).
//
// MODIFICATION HISTORY:               
//          Mozdzer        Initial version 
// *************************************************************************/
T_SCREEN_ID fnhkToggleField( UINT8       bKeyCode, 
                             T_SCREEN_ID usNextScr1, 
                             T_SCREEN_ID usNextScr2 )
{
    if (rGenPurposeBinChoiceField.bCurrentChoice == 0)
    {
        rGenPurposeBinChoiceField.bCurrentChoice = 1;
    }
    else
    {
        rGenPurposeBinChoiceField.bCurrentChoice = 0;
    }

    return (usNextScr1);
}


/* *************************************************************************
// FUNCTION NAME: fnpBufferInputInit
// DESCRIPTION: Pre function that detects if the entry buffer allows input.
//
// AUTHOR: Bob Li
//
// INPUTS:  Standard pre function inputs.
// CONTINUATION FUNCTION:  none
// NEXT SCREEN: unchanged
// NOTES:   
// MODIFICATION HISTORY:
//  2013.11.22    Bob Li        Initial version
//  2014.01.09    Wang Biao     Merged from Janus to FP for Network Proxy Settings.
// *************************************************************************/
char fnpBufferInputInit( void (** pContinuationFcn)(), unsigned long *pMsgsAwaited,
                             unsigned short *pNextScr )
{
    *pNextScr = 0;      /* leave next screen unchanged */
    
    //when enter password edit screen from previous screen, keep the last password char is displayed by asterisk after field is full.
	if(!fnIsBufferFull())
    {
		fPersistLastAsterisk = FALSE;
	}
	
	return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//          fnhkMoveCursorLeft
//
// DESCRIPTION:
//          This hardkey function that move the input cursor.
//
// INPUTS:
//          Standard hard key function inputs (format 2).
//
// RETURNS:
//          Next screen.
//
// WARNINGS/NOTES: 
//          None.
//
// MODIFICATION HISTORY:
//      04/07/2006   Kan          Modified
//      12/06/2005   Adam Liu     Adapted for FuturePhoenix
//                   Tim Zhang    Initial version  
// *************************************************************************/
T_SCREEN_ID fnhkMoveCursorLeft (UINT8        bKeyCode, 
                                UINT8        bNumNext,
                                T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID  usNext = 0;
    
    fnMoveCursorLeft();
        
    return (usNext);
}


/* *************************************************************************
// FUNCTION NAME: 
//          fnhkMoveCursorRight
//
// DESCRIPTION:
//          This hardkey function that move the input cursor.
//
// INPUTS:
//          Standard hard key function inputs (format 2).
//
// RETURNS:
//          Next screen.
//
// WARNINGS/NOTES: 
//          None.
//
// MODIFICATION HISTORY:
//      04/07/2006   Kan          Modified 
//      12/06/2005   Adam Liu     Adapted for FuturePhoenix
//                   Tim Zhang    Initial version  
// *************************************************************************/
T_SCREEN_ID fnhkMoveCursorRight(UINT8        bKeyCode, 
                                UINT8        bNumNext,
                                T_SCREEN_ID *pNextScreens)
{
    T_SCREEN_ID  usNext = 0;

    if (  fnIsBufferBlank() == FALSE)
    {
        fnMoveCursorRight();
    }
        
    return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnhkDefValueBufBlankOrChangeScr
//
// DESCRIPTION: 
//      Hard key function to handle Clear/Back key when entry field has 
//      default value
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      Next screens: 
//          If in default state, transition to next screen 1
//          Else, no screen change.
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//  11/30/2005  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkDefValueBufBlankOrChangeScr(UINT8           bKeyCode, 
                                            UINT8           bNumNext, 
                                            T_SCREEN_ID *   pNextScreens)
{
    T_SCREEN_ID     usNext = 0;

    if (fDefValueState == TRUE)
    {
        usNext = pNextScreens[0];   
    }   
    else
    {
        fDefValueState = TRUE;
        fnClearEntryBuffer();
        fnBufferDefaultValue();
    }

    return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnhkDefValueDeleteCharacter
//
// DESCRIPTION: 
//      Hard key function to handle Backspace key when entry field has default 
//      value
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      No screen change
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//  11/30/2005  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkDefValueDeleteCharacter(UINT8           bKeyCode, 
                                        UINT8           bNumNext, 
                                        T_SCREEN_ID *   pNextScreens)
{
    T_SCREEN_ID     usNext = 0;

    if (fDefValueState == FALSE)
    {
        fnDeleteCharacter();

        if (fnIsBufferBlank() == TRUE)
        {
            fDefValueState = TRUE;
            fnBufferDefaultValue();
        }
    }
        
    return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnhkDefValueBufferKey
//
// DESCRIPTION: 
//      Hard key function to handle character key input when entry field has 
//      default value
//
// INPUTS:
//      Standard hard key function inputs (format 2).
//
// RETURNS:
//      No screen change
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//  11/30/2005  Vincent Yi      Initial version
//
// *************************************************************************/
T_SCREEN_ID fnhkDefValueBufferKey(UINT8         bKeyCode, 
                                  UINT8         bNumNext, 
                                  T_SCREEN_ID * pNextScreens)
{
    T_SCREEN_ID     usNext = 0;

    if (fDefValueState == TRUE)
    {
        fDefValueState = FALSE;
        fnClearEntryBuffer();
    }

    fnBufferKey(bKeyCode);

    return (usNext);
}



/*--------------------------*/
/*    Event Functions       */
/*--------------------------*/



/* *************************************************************************
// FUNCTION NAME: 
//          fnevAlphaEnterMoveCursor
//
// DESCRIPTION:
//          Event function that indicates EVENT_SCREEN_TICK_OCCURRED occured.
//          in this peroid there is no enter, so buffer the last input alpha  
//          and move the cursor to the next position.
//
// INPUTS:
//          Standard event function inputs.
//
// RETURNS:
//          This event function always stays on the existing screen.
//
// WARNINGS/NOTES: 
//          None.
//
// MODIFICATION HISTORY:
//      01/08/2014    Wang Biao    Modified to adapt for Network Proxy Password setting.
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                    David Huang  Initial version  
//
// *************************************************************************/
T_SCREEN_ID fnevAlphaEnterMoveCursor(UINT8        bNumNext, 
                                     T_SCREEN_ID *pNextScreens, 
                                     INTERTASK   *pIntertask)
{
    T_SCREEN_ID usNext=0;

    OSStopTimer(SCREEN_TICK_TIMER);

    //So far, the following two flags are only working for the proxy address, username and password editing screen
	//Because other screens don't use the two flags, we can just put them in this function. 
	//Don't need a new event function.
	//If someday in the future, other entry screens also want to follow the logic like the proxy entry screens, just do it.
	//The two flag still work for them.
	bIsTimeOut = TRUE;
	
    //when enter password edit screen from previous screen, keep the last password char is displayed by asterisk after field is full.
    if(fnIsBufferFull())
	{
		fPersistLastAsterisk = TRUE;
	}
	
    /* means it is not a digital , not in KeyCodeOfDigital [ALPHA_USED_DIGITAL_KEY+1] array. */
    wKeyCurrentDigitalChar=ALPHA_USED_DIGITAL_KEY+1;  
    
    if( fnCharsBuffered() == TRUE && !fnIsBufferFull())
    {
        if(wKeyUnicodeJustInput)
		{
			fnDeleteCharacter();
			fnBufferUnicode(wKeyUnicodeJustInput);
		}
    }

    return (usNext);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnevAlphaEnterWithSwitch
//
// DESCRIPTION:
//          Added a switch for the alpha entry functions.
//
// INPUTS:
//          Standard event function inputs.
//
// RETURNS:
//          This event function always stays on the existing screen.
//
// WARNINGS/NOTES: 
//          None.
//
// MODIFICATION HISTORY:
// 
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                    Tim Zhang  Initial version  
//
// *************************************************************************/
T_SCREEN_ID fnevAlphaEnterWithSwitch(UINT8        bNumNext, 
                                     T_SCREEN_ID *pNextScreens, 
                                     INTERTASK   *pIntertask)
{
    T_SCREEN_ID usNext = 0;
    
    if ( bAlphaEntrySwitch == ALPHA_ENTRY_ON )
    {
        usNext = fnevAlphaEnterMoveCursor( bNumNext, pNextScreens, pIntertask );
    }

    return (usNext);
}




/*--------------------------*/
/*    Pre/post functions    */
/*--------------------------*/



/*--------------------------*/
/*  some wrapper functions  */
/*--------------------------*/


/* *************************************************************************
// FUNCTION NAME:
//        fnTimeoutInit
//
// DESCRIPTION:
//        field function for the timeout init field. 
//        
//
// INPUTS:
//        Standard field function inputs.
//        wTimeout - indicate timeout value.  
//
// RETURNS:
//        TBD.
//
// WARNINGS/NOTES:  
//        None.
//
// MODIFICATION HISTORY:
// 
//      12/06/2005    Adam Liu     Adapted for FuturePhoenix
//                    David Huang  Initial version  
//
// *************************************************************************/
BOOL fnTimeoutInit ( UINT16 *pFieldDest, 
                     UINT8   bLen, 
                     UINT8   bFieldCtrl, 
                     UINT8   bNumTableItems, 
                     UINT8  *pTableTextIDs,
                     UINT16  wTimeout)
{       
    UINT32          iStrLen;
    BOOL            fInsert;
    BOOL            fCursorOn;
    BOOL            fAlignL;
    UINT16          wTemplate = (UINT16) NULL;  /* templates won't be supported for this field */
    BOOL            fRetval = FAILURE;

    /* stuff the current setting into the entry buf */
    iStrLen = fnBin2Unicode(rGenPurposeEbuf.pBuf, wTimeout);
                       
    /* pad with spaces if right aligned */
    if ((iStrLen < bLen) && (bFieldCtrl & ALIGN_MASK))
    {
        /* make the string null-terminated before calling unicode pad space, which assumes
            that it is */
        rGenPurposeEbuf.pBuf[iStrLen] = (UINT16) NULL;
        fnUnicodePadSpace(rGenPurposeEbuf.pBuf, (UINT16) iStrLen, (UINT16) bLen);
    }

    /* create the entry buffer with the given attributes */
    fInsert = (((bFieldCtrl & FIELD_TYPE_MASK) == TYPE_INPUT_INSERT) ? TRUE : FALSE);
    fCursorOn = ((bFieldCtrl & CURSOR_MASK) ? TRUE : FALSE);
    fAlignL = ((bFieldCtrl & ALIGN_MASK) ? FALSE : TRUE);
    fnCreateEntryBuffer(&rGenPurposeEbuf, bLen, fAlignL, fInsert, fCursorOn, &wTemplate);

    bCursorPosition = rGenPurposeEbuf.bCursorLoc;
    bCursorStyle = rGenPurposeEbuf.bCursorStyle;

    memcpy(pFieldDest, rGenPurposeEbuf.pDispBuf, (bLen + 1) * sizeof(UINT16));
    fRetval = SUCCESSFUL;

    return (fRetval);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnGetCursorPosition
//
// DESCRIPTION:
//          Wrapper function to  get local variable bCursorPosition;
//
// INPUTS:
//          None
//
// RETURNS:
//          bCursorPosition
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//          08/01/2005   Kan Jiang    Initial version
// *************************************************************************/
UINT8 fnGetCursorPosition(void)
{
         return bCursorPosition;
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnSetCursorPosition
//
// DESCRIPTION:
//          Wrapper function to  set local variable bCursorPosition;
//
// INPUTS:
//          ucPosition: the cursor position
//
// RETURNS:
//          None
//       
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//          08/01/2005   Kan Jiang    Initial version
// *************************************************************************/
void fnSetCursorPosition(UINT8 ucPosition)
{
        bCursorPosition = ucPosition;
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnSetCursorPosition
//
// DESCRIPTION:
//          Wrapper function to  set local variable bCursorStyle;
//
// INPUTS:
//          ucStyle: the cursor style
//
// RETURNS:
//          None
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//         08/01/2005   Kan Jiang    Initial version  
// *************************************************************************/
void  fnSetCursorStyle(UINT8 ucStyle)
{
       bCursorStyle = ucStyle;
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnGetGenPurposeEbuf
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct rGenPurposeEbuf.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of rGenPurposeEbuf
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      8/4/2005 John Gao Initial version
// *************************************************************************/
ENTRY_BUFFER *fnGetGenPurposeEbuf(void)
{
    return(&rGenPurposeEbuf);
}



/* *************************************************************************
// FUNCTION NAME: 
//          fnGetKeyBuffer
//
// DESCRIPTION:
//          Wrapper function to get the pointer of struct rKeyBuffer.
//
// INPUTS:
//          None
//
// RETURNS:
//          The pointer of struct rKeyBuffer
//
// WARNINGS/NOTES:  
//          None
//
// MODIFICATION HISTORY:
//          12/07/2005   Adam Liu    Initial version 
// *************************************************************************/
KEY_BUFFER*  fnGetKeyBuffer(void)
{
        return &rKeyBuffer;
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnBufferDefaultValue
//
// DESCRIPTION: 
//      Put the default value into entry buffer
//
// INPUTS:
//      None
//
// OUTPUTS:
//      the Unicode string pUniDefValueStr has been put into the entry buffer
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//  11/30/2005  Vincent Yi      Initial version
//
// *************************************************************************/
static void fnBufferDefaultValue (void)
{
    UINT16  usIndex = 0;
    UINT16  usStrLen = 0;

    usStrLen = (UINT16)fnUnicodeLen((unsigned char*)pUniDefValueStr);

    while( (*pUniDefValueStr != 0) && (usIndex < usStrLen) )
    {
        fnBufferUnicode(pUniDefValueStr[usIndex++]);
    }
}




/* *************************************************************************
// FUNCTION NAME: 
//      fnStoreDefaultValueString
//
// DESCRIPTION: 
//      Utility function to store the Unicode string of default value into 
//      pUniDefValueStr[]
//
// INPUTS:
//      puniDefVal      - pointer to Unicode string of the default value 
//
// OUTPUTS:
//      None
//
// RETURNS:
//      SUCCESSFUL or FAILURE
//
// WARNINGS/NOTES:
//      None
//
// MODIFICATION HISTORY:
//  12/1/2005   Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnStoreDefaultValueString (UNICHAR * pUniDefVal)
{
    BOOL    fRet = FAILURE;
    UINT16  usStrLen = unistrlen(pUniDefVal);

    if (usStrLen < MAX_FIELD_LEN)
    {
        memset (pUniDefValueStr, 0, MAX_FIELD_LEN);
        unistrcpy (pUniDefValueStr, pUniDefVal);
        fRet = SUCCESSFUL;
    }

    return fRet;
}

/**************************************************************************
// FUNCTION NAME: 
//      fnSetEntryErrorCode
//
// DESCRIPTION:
//      Set the entry error code.
//
// INPUTS:
//      The error code to be set.
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//  09/14/2006  Oscar Wang     Modified for error graphic display.
//  11/15/2005  Dicky Sun      Initial version
//
// *************************************************************************/
void fnSetEntryErrorCode(UINT8 bErrCode)
{
    bEntryErrorCode = bErrCode;
    bGfxErrorCode = bErrCode;
}

/**************************************************************************
// FUNCTION NAME: 
//      fnSetGfxErrorCode
//
// DESCRIPTION:
//      Set the graphic error code.
//
// INPUTS:
//      The error code to be set.
//
// RETURNS:
//      None
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//  05/10/2007  Oscar Wang     Initial version
//
// *************************************************************************/
void fnSetGfxErrorCode(UINT8 bErrCode)
{
    bGfxErrorCode = bErrCode;
}

/**************************************************************************
// FUNCTION NAME: 
//      fnGetEntryErrorCode
//
// DESCRIPTION:
//      Get the entry error code.
//
// INPUTS:
//      None
//
// RETURNS:
//      the entry error code
//
// WARNINGS/NOTES:  
//      None
//
// MODIFICATION HISTORY:
//  11/15/2005  Dicky Sun      Initial version
//
// *************************************************************************/
UINT8 fnGetEntryErrorCode(void)
{
    return bEntryErrorCode;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnSetCheckedItemProperties
//
// DESCRIPTION: 
//      Utility function that Set the value of usSelectedItem.
//
// INPUTS:      
//      usNewIndex
//
// RETURNS:
//      None
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      12/28/2005 Adam Liu Initial version
//    01/20/2006    Kan jiang              Move from oifpimage to oientry
//    09/01/2008    Ivan Le Goff           Changed to fnSetCheckedItemProperties()
//      
// *************************************************************************/
void    fnSetCheckedItemProperties(UINT16 usItemProperty1, UINT16 usItemProperty2)
{
     SelectedItem.usItemProperty1 = usItemProperty1;  // use to be index for most 
                                                      // of the functions
     SelectedItem.usItemProperty2 = usItemProperty2;
}




/**********************************************************************
        Private Functions
**********************************************************************/

/* *************************************************************************
// FUNCTION NAME: 
//      fnSelectedItemIndicator
//
// DESCRIPTION: 
//      Utility function that calls the function associated with the given
//      slot, displays the soft key indicator on for soft key 1~4 if that line 
//      is active. If the choice is selected, the indicator will be "check"
//
// INPUTS:
//      Standard field function inputs.
//      usItemIndex - the item index number.
//
// RETURNS:
//      Always successful
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  10/24/2005  Vincent Yi      Initial version
//  01/20/2006  Kan jiang       Move from oifpimage to oientry
//  05/22/2006  James Wang      Change it to be a more common function.
//  09/01/2008  Ivan Le Goff    Add condition for permits, as different 
//                              types of permits can share the same GenID
//
// *************************************************************************/
static BOOL fnSelectedItemIndicator( UINT16 *    pFieldDest, 
                                     UINT8      bLen,   
                                     UINT8      bFieldCtrl, 
                                     UINT8      bNumTableItems, 
                                     UINT8   *  pTableTextIDs,
                                     UINT16     usItemIndex )
{
    UINT8           ucFieldIndex;
    UINT16          usSlotIndex;
    S_MENU_TABLE *  pMenuTable;
    
       
    pMenuTable = fnGetSMenuTable();    

    SET_SPACE_UNICODE(pFieldDest, bLen);

    usSlotIndex = pMenuTable->usFirstDisplayedSlotIndex + usItemIndex;

    if ((usSlotIndex < MAX_SMENU_SLOTS) && bNumTableItems)
    {
        /* if there is something in the corresponding slot and it is active */
        if ((pMenuTable->pSlot[usSlotIndex].bKeyAssignment != 0) &&
            (pMenuTable->pSlot[usSlotIndex].fActive == TRUE))
        {   
            // Active slot:
            // for all graphics, usItemProperty1 must match the GenID (pGenPurp1)
            // for permits, usItemProperty2 must also match the graphic comp type (pGenPurp2).
            // for everything else, usItemProperty1 must match the label string number, account number,
            //      text msg ID, indicia selection, task/order ID, 3rd party EKP number, etc. (pGenPurp1) and
            //      usItemProperty2 and pGenPurp2 will be equal to zero.
            if(   ((UINT16)pMenuTable->pSlot[usSlotIndex].pGenPurp1 == SelectedItem.usItemProperty1) 
               && ((UINT16)pMenuTable->pSlot[usSlotIndex].pGenPurp2 == SelectedItem.usItemProperty2) )
            {
                // Selected, show 'check'
                ucFieldIndex = 1;
            }
            else
            {
                ucFieldIndex = 0;
            }
            fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                pTableTextIDs, ucFieldIndex);
        }
    }
    return (SUCCESSFUL);
}

//**************************************************************************
//  FUNCTION NAME:  fnSetAlphaEntrySwitch
//
//  DESCRIPTION:
//      This is an UI interface function to set bAlphaEntrySwitch.
//  INPUTS:
//      UINT8 bAlphaEntrySwitch           
//  RETURN:
//      None
//  NOTES:
//  1. This function should be in ioEntry.c
//
//  MODIFICATION HISTORY:
//  09-21-05    Bob Li  Moved this function from oirateutils.c
//  08-30-05    Bob Li  create this function
//**************************************************************************
void fnSetAlphaEntrySwitch(UINT8 bSwitchValue)
{
   bAlphaEntrySwitch = bSwitchValue;
   return;
}


/* *************************************************************************
// FUNCTION NAME:
//        fnInputFieldInit
//
// DESCRIPTION:
//        field function for the init input field. 
//        
//
// INPUTS:
//        Standard field function inputs.
//        ulValue - indicate value.  
//
// RETURNS:
//        TBD.
//
// WARNINGS/NOTES:  
//        None.
//
// MODIFICATION HISTORY:
// 
//  06/05/2007  Vincent Yi      Initial version
//
// *************************************************************************/
BOOL fnInputFieldInit ( UINT16 *    pFieldDest, 
                        UINT8       bLen, 
                        UINT8       bFieldCtrl, 
                        UINT8       bNumTableItems, 
                        UINT8  *    pTableTextIDs,
                        UINT32      ulValue)
{       
    UINT32          iStrLen;
    BOOL            fInsert;
    BOOL            fCursorOn;
    BOOL            fAlignL;
    UINT16          wTemplate = (UINT16) NULL;  /* templates won't be supported for this field */

    /* stuff the current setting into the entry buf */
    iStrLen = fnBin2Unicode(rGenPurposeEbuf.pBuf, ulValue);
                       
    /* pad with spaces if right aligned */
    if ((iStrLen < bLen) && (bFieldCtrl & ALIGN_MASK))
    {
        /* make the string null-terminated before calling unicode pad space, 
            which assumes that it is */
        rGenPurposeEbuf.pBuf[iStrLen] = (UINT16) NULL;
        fnUnicodePadSpace(rGenPurposeEbuf.pBuf, (UINT16) iStrLen, (UINT16)bLen);
    }

    /* create the entry buffer with the given attributes */
    fInsert = (((bFieldCtrl & FIELD_TYPE_MASK) == TYPE_INPUT_INSERT) ? TRUE : FALSE);
    fCursorOn = ((bFieldCtrl & CURSOR_MASK) ? TRUE : FALSE);
    fAlignL = ((bFieldCtrl & ALIGN_MASK) ? FALSE : TRUE);
    fnCreateEntryBuffer(&rGenPurposeEbuf, bLen, fAlignL, fInsert, fCursorOn, &wTemplate);

    bCursorPosition = rGenPurposeEbuf.bCursorLoc;
    bCursorStyle = rGenPurposeEbuf.bCursorStyle;

    memcpy(pFieldDest, rGenPurposeEbuf.pDispBuf, (bLen + 1) * sizeof(UINT16));

    return (SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnResetRefreshFlag
//
// DESCRIPTION: 
//      Because of the side effect of GMSE00161091 fixing,the field function
//      fnfEntryErrorInfo(...) may cause screen re-painted, it will bring 
//      a lot of fraca in the future. We have to use this function to fix them.
//
// MODIFICATION HISTORY:
//      09/01/2010 David Huang Initial version
// *************************************************************************/
void fnResetRefreshFlag(void)
{
    bRefreshScreen = FALSE;
    return;
}





/* *************************************************************************
// FUNCTION NAME: 
//      fnResetShowError2
//
// DESCRIPTION: 
//      Because of the side effect of GMSE00161091 fixing,the field function
//      fnfEntryErrorInfo(...) may cause screen re-painted, it will bring 
//      a lot of fraca in the future. We have to use this function to fix them.
//
// MODIFICATION HISTORY:
//      09/01/2010 David Huang Initial version
// *************************************************************************/
void fnResetShowError2(void)
{
    bShowError2 = FALSE;
    return;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnResetEntryErrCode
//
// DESCRIPTION: 
//      Because of the side effect of GMSE00161091 fixing,the field function
//      fnfEntryErrorInfo(...) may cause screen re-painted, it will bring 
//      a lot of fraca in the future. We have to use this function to fix them.
//
// MODIFICATION HISTORY:
//      09/01/2010 David Huang Initial version
// *************************************************************************/
void fnResetEntryErrCode(void)
{
    bEntryErrorCode = ENTRY_NO_ERROR;
    return;
}

