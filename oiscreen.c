/************************************************************************
	PROJECT:		Future Phoenix
	COPYRIGHT:		2005, Pitney Bowes, Inc.
	AUTHOR:			
	MODULE NMAE:	OiScreen.c

	DESCRIPTION:	This file contains tables and functions required to
					map the screen identification constants in oiscreen.h
					to the actual screen ID for FP.

 	MODIFICATION HISTORY:

	OLD PVCS REVISION HISTORY
 *	
 *22-May-17 Jennifer Hao on FPHX 02.12 cienet branch
 *    Commented out function GetScreenConstant because it isn't used by G9.
 *    
 *    Rev 1.1   Mar 24 2003 13:35:06   BR507HA
 * Change out of range screen to not generate meter error

*************************************************************************/


/**********************************************************************
		Include Header Files
**********************************************************************/
#include "oiscreen.h"
#include "oit.h"

#include "errcode.h"


/**********************************************************************
		Local Function Prototypes
**********************************************************************/



/**********************************************************************
		Local Defines, Typedefs, and Variables
**********************************************************************/



/**********************************************************************
		Global Functions Prototypes
**********************************************************************/



/**********************************************************************
		Global Variables
**********************************************************************/

extern 	const unsigned short pScreenTable[];


/**********************************************************************
		Public Functions
**********************************************************************/
/* Utility functions */


unsigned short GetScreen(unsigned char ScreenIndex)
{
	unsigned short 	wScreenID = 0;
	unsigned short	wTableLength;


	wTableLength = 0;
	while (pScreenTable[wTableLength] != 0xFFFF)
		wTableLength++;

	if (ScreenIndex >= wTableLength)
		fnReportMeterError(ERROR_CLASS_OIT, ECOIT_ILLEGAL_SCREEN_CONSTANT);
	else
		wScreenID = pScreenTable[ScreenIndex];

	return (wScreenID);
}



/* Pre/post functions */

/* Field functions */

/* Hard key functions */

/* Event functions */

/* Variable graphic functions */



/**********************************************************************
		Private Functions
**********************************************************************/


