/**********************************************************************
 PROJECT    :   CoMet - PHC
 COPYRIGHT  :   2000, Pitney Bowes, Inc.  
 AUTHOR     :   John Hurd
 MODULE     :   hash.c  
 
 DESCRIPTION:
    This file is used to define the functions to control the hash 
    engine. 
	 
 MODIFICATION HISTORY:
*
* 05-Apr-07 sa002pe on FPHX 01.04 shelton branch
*	Added definition of THISFILE.
*	Changed all calls for exception logging to use THISFILE instead of __FILE__.
*
 12/01/2001 John Hurd   Removed TestHash for snippets.
 11/08/2001 Terebesi    Updated padMessage function 
 10/08/2001 John Hurd   Update the criteria for doing the SHA1 hash in software.
 10/31/2000 John Hurd   Update the data pointer after each turn of the SW 
                        hash engine.
 10/30/2000 John Hurd   Added alternate hash chaining varialbe.
 10/27/2000 John Hurd   Add capability to perform the SHA-1 hash using the ASIC
                        or software.
 07/27/2000 John Hurd   Remove done and start flags, not used by the boot loader.
 07/14/2000 John Hurd   Moved HASH_COMPARE_FAILED to errordef.h.
 07/10/2000 John Hurd   Added ComputeCompareSHA1Hash.
 06/20/2000	John Hurd	Initial version.
 **********************************************************************/
 
#include "errno.h"
#include "ptypes.h"
#include "psdtypes.h"
#include "hash.h"
#include "errordef.h"
#include "signedrecords.h"
#include "exception.h"
#undef NULL
#include <stdio.h>	//NULL is correctly defined by stdio

#define THISFILE "hash.c"


/* Define a structure to hold a 8 byte number */
typedef struct sUINT64
{
    UINT32 word1;
    UINT32 word2;
}   UINT_64;

//TODO - AB - confirm LITTLE_ENDIAN works
/* Initial SHA-1 chaining data */
#ifdef LITTLE_ENDIAN
const HASH_DATA_TYPE initChainVariable= { 0xF0, 0xE1, 0xD2, 0xC3,   /* h4 */
                                          0x76, 0x54, 0x32, 0x10,   /* h3 */
                                          0xFE, 0xDC, 0xBA, 0x98,   /* h2 */
                                          0x89, 0xAB, 0xCD, 0xEF,   /* h1 */
                                          0x01, 0x23, 0x45, 0x67 }; /* h0 */
#else
const HASH_DATA_TYPE initChainVariable= {
                                          0x67,0x45,0x23,0x01,   /* h0 */
                                          0xef,0xcd,0xab,0x89,   /* h1 */
                                          0x98,0xba,0xdc,0xfe,   /* h2 */
                                          0x10,0x32,0x54,0x76,   /* h3 */
					  0xc3,0xd2,0xe1,0xf0};  /* h4 */
#endif
/* Alternate SHA-1 chaining data */
const HASH_DATA_TYPE prngChainVariable= { 0x01, 0x23, 0x45, 0x67,   /* h5 */
                                          0xF0, 0xE1, 0xD2, 0xC3,   /* h4 */
                                          0x76, 0x54, 0x32, 0x10,   /* h3 */
                                          0xFE, 0xDC, 0xBA, 0x98,   /* h2 */
                                          0x89, 0xAB, 0xCD, 0xEF,   /* h1 */
                                          }; 



/**********************************************************************
DESCRIPTION:
The padMessage function performs padding needed by the SHA1 algorithm.
	 
INPUT PARAMS:
	pM      - Pointer to the input data.    
    lenM    - Total number of bytes to be hashed.
    lenCurr - Number of data bytes in teh current block.
	pData   - Pointer to the place the padded data.    

RETURN VALUE:
    None.
**********************************************************************/

void padMessage(UINT8 *pM, UINT32 lenCurr, UINT32 lenM, UINT8 *pData)
{
    UINT32 i;
    UINT_64 l64lenM;
    UINT8 *plenM;
    UINT8 *pTemp;
  
    /* Compute the length of the data in bits */
#ifdef LITTLE_ENDIAN
    l64lenM.word1 = (lenM & 0x1FFFFFFF) << 3;
    l64lenM.word2 = (lenM & 0xFC000000) >> 26;
    /* Find the last byte of data in the length */
    plenM = (UINT8 *) &l64lenM;
    plenM +=7;
#else
    l64lenM.word2 = (lenM & 0x1FFFFFFF) << 3;
    l64lenM.word1 = (lenM & 0xFC000000) >> 26;
    plenM = (UINT8 *) &l64lenM;
#endif
  
    /* Copy the input data into the pad buffer */
    pTemp = pData;
    for (i = 0; i < lenCurr; i++)
    {
        *pTemp++ = *pM++;  
    }

    /* Set the padding bit if necessary */
    pData += lenCurr;
    if((lenCurr > 0) || ((lenM % 64) == 0))
    {
	    *pData = 0x80;
    }
    else
    {
	    *pData = 0x00;
    }
  
    /* Pad with 0's until the length field */ 
    pData++;
    for(i = lenCurr+1; i < 56; i++)
    {
        *pData++ = 0x00; 
    }

    /* Determine if length is to be put into the padded data this time */
    if (lenCurr >= 56)
    {
        /* No, don't add the length, just pad with 0's */
        for (i = lenCurr+1; i < 64; i++)
        {
            *pData++ = 0x00; 
        }
    }
    else
    {
        /* Fill the length field in the padded data */
        for (i = 56; i < 64; i++ )
        {
#ifdef LITTLE_ENDIAN
            *pData++ = *plenM;
    	    plenM--;
#else
            *pData++ = *plenM++;
#endif
        }
    }
     
}



/**********************************************************************
DESCRIPTION:
The ComputeSHA1InSW function generates a SHA1 hash via software  based 
on the FIPS 180-1 standard.
	 
INPUT PARAMS:
	pH   - Pointer to the input chain variable and result of the hash.    
	pM   - Pointer to the input data.    
    lenM - Number of bytes to be hashed.

RETURN VALUE:
    None.
**********************************************************************/

void ComputeSHA1InSW(UINT32 *pH, UINT8 *pM, UINT32 lenM)
{
    UINT32 temp;              /* intermediate computations            */
    UINT32 K0 = 0x5A827999;   /* SHA-1 constant                       */
    UINT32 K1 = 0x6ED9EBA1;   /* SHA-1 constant                       */
    UINT32 K2 = 0x8F1BBCDC;   /* SHA-1 constant                       */
    UINT32 K3 = 0xCA62C1D6;   /* SHA-1 constant                       */
    UINT32 W[80];             /* SHA-1 work space                     */
    UINT32 A, B, C, D, E;     /* SHA-1 working variables              */
    UINT32 i, n;              /* Loop indices                         */ 
    UINT32 lenLast, block, extra_block; /* SHA-1 working variables    */
    UINT8 *pData;             /* Pointer to data to be hashed         */
    UINT8 data[64];           /* Working memory for SHA-1 padding     */

    /* Determine how many turns of the SHA-1 Engine will be needed */
    block = lenM >> 6;      /* Compute the number of full blocks to be hashed */
    lenLast = lenM % 64;    /* Determine the length of the last block         */
  
    /* The last one or two blocks may need to be padded based on the number  */
    /* of bytes being hashed.  The last 8 bytes are reserved for the message */ 
    /* length, so we may need 2 extra blocks to handle the padding.          */
    if (lenLast >= 56)
    {
	    extra_block = 2;
    }
    else
    {
	    extra_block = 1;
    }

    for(n = 0; (n < (block + extra_block)); n++)
    {
        /* Determine if the next block of data is to be padded */
	    if ((n >= block) && (extra_block > 0))
	    {
		    padMessage(pM, lenLast, lenM, data);
		    lenLast = 0;   /* Set the length of data left to 0 */
            pData = data;  /* Get the input to the SHA-1 from the padded data */
	    }
        else
        {
            pData = pM;    /* Get the input to the SHA-1 from the input data */
        }
        
        /* Read in the next block of data */
        for(i=0; i < 16; i++)
	    {
	      W[i] = *pData++;
	      W[i] = (W[i] << 8) | *pData++;
	      W[i] = (W[i] << 8) | *pData++;
	      W[i] = (W[i] << 8) | *pData++;
        }
        /* Move the data pointer to the next block for the next turn */
        pM = pData;
        
        /* Perform the SHA-1 as defined in FIPS 180-1 */
        
        for(i=16; i < 80; i++)
        {
            temp = W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16];
	        W[i] = (temp << 1) | ( temp >> 31);
        }

#ifdef LITTLE_ENDIAN
        A = *(pH+4);
        B = *(pH+3);
        C = *(pH+2);
        D = *(pH+1);
        E = *pH;
#else
        E = *(pH+4);
        D = *(pH+3);
        C = *(pH+2);
        B = *(pH+1);
        A = *pH;
#endif

        for(i = 0; i < 20; i++)
        {
            temp = (B & C) | ((~B) & D);
            temp += ((A << 5) | (A >> 27)) + E + W[i] + K0;
	        E = D;
	        D = C;
	        C = (B << 30) | (B >> 2);
	        B = A;
	        A = temp;
        }
  
        for(i = 20; i < 40; i++)
        {
            temp = B ^ C ^ D;
            temp += ((A << 5) | (A >> 27)) + E + W[i] + K1;
	        E = D;
	        D = C;
	        C = (B << 30) | (B >> 2);
	        B = A;
	        A = temp;
        }
  
        for(i = 40; i < 60; i++)
        {
	        temp = (B & C) | (B & D) | (C  & D);
            temp += ((A << 5) | (A >> 27)) + E + W[i] + K2;
	        E = D;
	        D = C;
	        C = (B << 30) | (B >> 2);
	        B = A;
	        A = temp;
        }
  
        for(i = 60; i < 80; i++)
        {
	        temp = B ^ C ^ D;
            temp += ((A << 5) | (A >> 27)) + E + W[i] + K3;
	        E = D;
	        D = C;
	        C = (B << 30) | (B >> 2);
	        B = A;
	        A = temp;
        }

        /* Store the intermediate hash */
#ifdef LITTLE_ENDIAN
	    *(pH+4) += A;
	    *(pH+3) += B;
	    *(pH+2) += C;
	    *(pH+1) += D;
	    *pH     += E;
#else
	    *(pH+4) += E;
	    *(pH+3) += D;
	    *(pH+2) += C;
	    *(pH+1) += B;
	    *pH     += A;
#endif

    }
}



/*********************** GetDefaultChainForHash ***********************/

HASH_DATA_TYPE* GetDefaultChainForHash(void)
{
    return((HASH_DATA_TYPE*) &initChainVariable);    
}



/*********************** GetDefaultChainForHash ***********************/

HASH_DATA_TYPE* GetAlternateChainForHash(void)
{
    return((HASH_DATA_TYPE*) &prngChainVariable);    
}



/************************** ComputeSHA1Hash ***************************/

ERR_CODE ComputeSHA1Hash(UINT8 *pSrc, HASH_DATA_TYPE *pDest, UINT32 numBytes, 
                         const HASH_DATA_TYPE *pChain)
{
    UINT32   i;                         /* loop index                       */
    ERR_CODE error = NO_ERROR;          /* error status                     */
    
    /* Perform Hash in software */
    for (i=0; i < sizeof(HASH_DATA_TYPE); i++)
      {
	pDest->value[i] = pChain->value[i];
      }
    
    ComputeSHA1InSW((UINT32 *) pDest, pSrc, numBytes);    
    return(error);
}


//-----------------------------------------------------------------------------------
BOOL ComputeSHA1HashFromFile(FILE *pCertFileHandle, HASH_DATA_TYPE *destHash, long numberOfBytes, const HASH_DATA_TYPE *iChainVariable)
{
    UINT32 temp;              /* intermediate computations            */
    UINT32 K0 = 0x5A827999;   /* SHA-1 constant                       */
    UINT32 K1 = 0x6ED9EBA1;   /* SHA-1 constant                       */
    UINT32 K2 = 0x8F1BBCDC;   /* SHA-1 constant                       */
    UINT32 K3 = 0xCA62C1D6;   /* SHA-1 constant                       */
    UINT32 W[80];             /* SHA-1 work space                     */
    UINT32 A, B, C, D, E;     /* SHA-1 working variables              */
    UINT32 i, n;              /* Loop indices                         */ 
    UINT32 lenLast, block, extra_block; /* SHA-1 working variables    */
    UINT8 *pData;             /* Pointer to data to be hashed         */
    UINT8 data[64];           /* Working memory for SHA-1 padding     */
    UINT8 buffer[64];         //for file read
	int readCount;			  //For file operations
    UINT32 *longPtr = (UINT32 *)destHash;

    if (pCertFileHandle == NULL)
    	return FALSE;

    //Init destination hash from iChainVariable
	for (i=0; i < sizeof(HASH_DATA_TYPE); i++)
	{
		destHash->value[i] = iChainVariable->value[i];
	}
    
    /* Determine how many turns of the SHA-1 Engine will be needed */
    block = numberOfBytes >> 6;      /* Compute the number of full blocks to be hashed */
    lenLast = numberOfBytes % 64;    /* Determine the length of the last block         */
  
    /* The last one or two blocks may need to be padded based on the number  */
    /* of bytes being hashed.  The last 8 bytes are reserved for the message */ 
    /* length, so we may need 2 extra blocks to handle the padding.          */
    if (lenLast >= 56)
    {
	    extra_block = 2;
    }
    else
    {
	    extra_block = 1;
    }

    for(n = 0; (n < (block + extra_block)); n++)
    {
	    //Get a block of data from the file
    	readCount = fread((void*)buffer, 1, sizeof(data), pCertFileHandle);
	    if(readCount < sizeof(data)) //error reading
	    {
	        fnCheckFileSysCorruption(-1);
	    	if (errno != ENOERR)
	    		return FALSE; // hit real error
	    }
        
        // Determine if the block of data needs to be padded
	    if ((n >= block) && (extra_block > 0))
	    {
		    padMessage(buffer, lenLast, numberOfBytes, data);
		    lenLast = 0;   /* Set the length of data left to 0 */
            pData = data;  /* Get the input to the SHA-1 from the padded data */
	    }
        else
        {
        	pData = buffer;	//use local scratchpad data directly 
        }
            
        /* Read in the next block of data */
        for(i=0; i < 16; i++)
	    {
	      W[i] = *pData++;
	      W[i] = (W[i] << 8) | *pData++;
	      W[i] = (W[i] << 8) | *pData++;
	      W[i] = (W[i] << 8) | *pData++;
        }

        /* Perform the SHA-1 as defined in FIPS 180-1 */
        
        for(i=16; i < 80; i++)
        {
            temp = W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16];
	        W[i] = (temp << 1) | ( temp >> 31);
        }

#ifdef LITTLE_ENDIAN
        A = *(longPtr+4);
        B = *(longPtr+3);
        C = *(longPtr+2);
        D = *(longPtr+1);
        E = *longPtr;
#else
        E = *(longPtr+4);
        D = *(longPtr+3);
        C = *(longPtr+2);
        B = *(longPtr+1);
        A = *longPtr;
#endif

        for(i = 0; i < 20; i++)
        {
            temp = (B & C) | ((~B) & D);
            temp += ((A << 5) | (A >> 27)) + E + W[i] + K0;
	        E = D;
	        D = C;
	        C = (B << 30) | (B >> 2);
	        B = A;
	        A = temp;
        }
  
        for(i = 20; i < 40; i++)
        {
            temp = B ^ C ^ D;
            temp += ((A << 5) | (A >> 27)) + E + W[i] + K1;
	        E = D;
	        D = C;
	        C = (B << 30) | (B >> 2);
	        B = A;
	        A = temp;
        }
  
        for(i = 40; i < 60; i++)
        {
	        temp = (B & C) | (B & D) | (C  & D);
            temp += ((A << 5) | (A >> 27)) + E + W[i] + K2;
	        E = D;
	        D = C;
	        C = (B << 30) | (B >> 2);
	        B = A;
	        A = temp;
        }
  
        for(i = 60; i < 80; i++)
        {
	        temp = B ^ C ^ D;
            temp += ((A << 5) | (A >> 27)) + E + W[i] + K3;
	        E = D;
	        D = C;
	        C = (B << 30) | (B >> 2);
	        B = A;
	        A = temp;
        }

        /* Store the intermediate hash */
#ifdef LITTLE_ENDIAN
	    *(longPtr+4) += A;
	    *(longPtr+3) += B;
	    *(longPtr+2) += C;
	    *(longPtr+1) += D;
	    *longPtr     += E;
#else
	    *(longPtr+4) += E;
	    *(longPtr+3) += D;
	    *(longPtr+2) += C;
	    *(longPtr+1) += B;
	    *longPtr     += A;
#endif

    }
    return TRUE;
}

