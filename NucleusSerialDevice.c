/************************************************************************
*	PROJECT:		Horizon CSD
*	MODULE NAME:	NucleusSerialDevice.c
*	
*	DESCRIPTION:	Nucleus serial device middleware interface
*
* ----------------------------------------------------------------------
*               Copyright (c) 2016 Pitney Bowes Inc.
*
*     
*************************************************************************/
#include "commontypes.h"
#include "ibuttoniolow.h"
#include "bsp/csd_2_3_defs.h"
#include "sac.h"

static unsigned char fnReadInterruptRequest( unsigned char *uBase );
static unsigned char fnReadModemStatus( unsigned char *uBase );
static unsigned char fnReadModemControl( unsigned char *uBase );
static unsigned char fnReadLineControl( unsigned char *uBase );
static void fnWriteModemControl( unsigned char *uBase , unsigned char val );
static void fnWriteLineControl( unsigned char *uBase , unsigned char val );
static unsigned char fnReadLineStatus( unsigned char *uBase );
static unsigned char fnReadFIFOStatus( unsigned char *uBase );
static unsigned char fnReadFIFOAvail( unsigned char *uBase );
static void fnWriteFIFO( unsigned char *uBase , unsigned char ch );
static void fnWriteFIFOControl( unsigned char *uBase , unsigned char val );
static unsigned char fnReadFIFO( unsigned char *uBase );
static void fnResetUart( unsigned char *uBase );
static void fnWriteInterruptControl( unsigned char *uBase, unsigned char irqstat);
static unsigned char fnSetBaudRate( unsigned char *uBase, unsigned short rate);
static void fnDisableTransmitIrq( unsigned char *portBase );
static void fnEnableTransmitIrq( unsigned char *portBase );
static void fnWaitXmitDone( unsigned char *uBase);
static void fnDisableReceive(unsigned char *portBase);
static void fnEnableReceive(unsigned char *portBase);


UARTCONTROL NSDUart = {
    fnReadInterruptRequest,
    fnReadModemStatus,
    fnReadModemControl,
    fnReadLineControl,
    fnWriteModemControl,
    fnWriteLineControl,
    fnReadLineStatus,
    fnReadFIFOStatus,
    fnReadFIFOAvail,
    fnWriteFIFO,
    fnWriteFIFOControl,
    fnReadFIFO,
    fnResetUart,
    fnWriteInterruptControl,
    fnSetBaudRate,
    fnDisableTransmitIrq,
    fnEnableTransmitIrq,
    fnWaitXmitDone,
    fnDisableReceive,
    fnEnableReceive
};

/* ************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static unsigned char fnReadInterruptRequest( unsigned char *uBase )
{
	unsigned short cStat;
	unsigned char rStat = 0;

	cStat =  *(uBase + AM3X_UARTLSR_OFFSET);

	if(cStat & AM3X_UARTLSR_RX_DATA_RDY)
	{
		rStat = DATA_RECEIVED_MASK;
	}
	else
	{
		if(cStat & (AM3X_UARTLSR_TX_HOLD_EMPTY | AM3X_UARTLSR_TX_EMPTY))
		{
			rStat = THR_EMPTY_MASK;
		}
		else
		{
			rStat = NOIRQPENDING;
		}
	}
	return(rStat);
}

/* ************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static unsigned char fnReadModemStatus( unsigned char *uBase )
{
  unsigned char rStat = 0;
  return(rStat);
}

/* ************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static unsigned char fnReadModemControl( unsigned char *uBase )
{
  unsigned char rStat = 0;
  return(rStat);
}
/* ************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static unsigned char fnReadLineControl( unsigned char *uBase )
{
  unsigned char rStat = 0;
  return(rStat);
}

/* ************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */

static void fnWriteModemControl( unsigned char *uBase , unsigned char val )
{
}

/* ************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static void fnWriteLineControl( unsigned char *uBase , unsigned char val )
{
}

/* ************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static unsigned char fnReadLineStatus(unsigned char *uBase )
{
  unsigned char rStat = 0;
  return(rStat);
}

/* ************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static unsigned char fnReadFIFOStatus(unsigned char *uBase )
{
  return(0);
}

/* ************************************************************************
/ FUNCTION   : fnReadFIFOAvail
/ DESCRIPTION:  Returns number of characters FIFO can hold else 1
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static unsigned char fnReadFIFOAvail(unsigned char *uBase )
{
  return(1);
}

/* ************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static void fnWriteFIFO( unsigned char *uBase , unsigned char ch )
{
	unsigned char *pBase = ( unsigned char *) uBase;

	*(pBase + AM3X_UARTTHR_OFFSET) = ch;
	fnEnableTransmitIrq(uBase);
}

/* ************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static void fnWriteFIFOControl( unsigned char *uBase , unsigned char val )
{
}

/* ************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static unsigned char fnReadFIFO( unsigned char *uBase )
{
  unsigned char ch = *(uBase + AM3X_UARTRHR_OFFSET);

  return(ch);
}

/* ************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static void fnResetUart( unsigned char *uBase )
{
}

/* ************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static void fnDisableTransmitIrq( unsigned char *uBase )
{
	*(uBase + AM3X_UARTIER_OFFSET) &= ~AM3X_UARTIER_TXHR;
}

/* ************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static void fnEnableTransmitIrq( unsigned char *uBase )
{
	*(uBase + AM3X_UARTIER_OFFSET) |= AM3X_UARTIER_TXHR;
}

/* *************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static void fnWriteInterruptControl( unsigned char *uBase, unsigned char irqstat)
{
	unsigned short bEnb = 0;

	bEnb = *(unsigned short *)(uBase + AM3X_UARTIER_OFFSET);
	bEnb &= ~(AM3X_UARTIER_TXHR|AM3X_UARTIER_RXHR);

	if( irqstat & AM3X_UARTIER_RXHR)
		bEnb |= AM3X_UARTIER_RXHR;
	if( irqstat & AM3X_UARTIER_TXHR)
		bEnb |= AM3X_UARTIER_TXHR;
	*(unsigned short *)(uBase + AM3X_UARTIER_OFFSET) = bEnb;
//	fnReadLineStatus(uBase);
	fnReadFIFO(uBase);
//	fnReadInterruptRequest(uBase);
//	fnReadModemStatus(uBase);
}

/* *******************************************************************************
/ FUNCTION   : 
/ DESCRIPTION:  
/ 
/
/
/              
/ INPUTS     : 
/	
/ NOTES	     : 
/
/ *********************************************************************** */
static unsigned char fnSetBaudRate( unsigned char *uBase, unsigned short rate)
{
	UINT16 tempInts, tempLCR;

	//disable UART interrupts
	// Get the current UART interrupt status
    tempInts = *(volatile UINT16 *)(uBase + AM3X_UARTIER_OFFSET);

    // Disable UART interrupts
    *(volatile UINT16 *)(uBase + AM3X_UARTIER_OFFSET) = 0x0000;
	
	//Switch to register configuration mode B to access the UART0.UART_DLL and UART0.UART_DLH
	//registers:
	//Set the UART0.UART_LCR register value to 0x00BF.
	tempLCR = *(volatile UINT16 *)(uBase + AM3X_UARTLCR_OFFSET);
    *(volatile UINT16 *)(uBase + AM3X_UARTLCR_OFFSET) = 0x00BF;

	//Load the new divisor value:
	//Set the UART0.UART_DLL[7:0] CLOCK_LSB and UART0.UART_DLH[5:0] CLOCK_MSB bit fields to
	//the desired values.
    *(volatile UINT16 *)(uBase + AM3X_UARTDLL_OFFSET) = (rate & 0x00ff);
	*(volatile UINT16 *)(uBase + AM3X_UARTDLH_OFFSET) = ((rate & 0xff00) >> 8);

	//Switch to register operational mode to access the UART0.UART_IER register:
	//Set the UART0.UART_LCR register value to tempLCR.
	*(volatile UINT16 *)(uBase + AM3X_UARTLCR_OFFSET) = tempLCR;
    
	//Restore Interrupts
	*(volatile UINT16 *)(uBase + AM3X_UARTIER_OFFSET) = tempInts;
	  
   return TRUE;
}

/* *************************************************************************
/ FUNCTION   :
/ DESCRIPTION:
/
/
/
/
/ INPUTS     :
/
/ NOTES	     :
/
/ *********************************************************************** */
static void fnWaitXmitDone( unsigned char *uBase)
{
	volatile unsigned short cStat;

	do
	{
		cStat =  *(unsigned short *)(uBase + AM3X_UARTLSR_OFFSET);

		if(cStat & AM3X_UARTLSR_TX_EMPTY)
		{
			break;
		}
	} while(1);

}

/* *************************************************************************
/ FUNCTION   :
/ DESCRIPTION:
/
/
/
/
/ INPUTS     :
/
/ NOTES	     :
/
/ *********************************************************************** */
static void fnDisableReceive(unsigned char *portBase)
{
	*(portBase + AM3X_UARTIER_OFFSET) &= ~AM3X_UARTIER_RXHR;
}

/* *************************************************************************
/ FUNCTION   :
/ DESCRIPTION:
/
/
/
/
/ INPUTS     :
/
/ NOTES	     :
/
/ *********************************************************************** */
static void fnEnableReceive(unsigned char *portBase)
{
//	*(portBase + AM3X_UARTFCR_OFFSET) = (AM3X_UARTFCR_RXTRIG_1 | AM3X_UARTFCR_FIFOEN | AM3X_UARTFCR_RX_FIFO_CLEAR);
	*(portBase + AM3X_UARTIER_OFFSET) |= AM3X_UARTIER_RXHR;
}


