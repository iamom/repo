/*************************************************************************
*	PROJECT:		Spark
*	AUTHOR:			Derek DeGennaro
*	MODULE NAME:	$Workfile:   xmlstk.c  $
*	REVISION:		$Revision:   1.7  $
*	
*	DESCRIPTION:	This file is the implementation for stacks used by 
*					XML related stuff.
*					
*
* ----------------------------------------------------------------------
*               Copyright (c) 1998 Pitney Bowes Inc.
*                    35 Waterview Drive
*                   Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*
*	REVISION HISTORY:
*	$Log:   H:/group/SPARK/ARCHIVES/UIC/xmlstk.c_v  $
* 
*    Rev 1.7   Mar 03 2003 16:57:20   MA507HA
* LINT Patrol - Added includes needed for 
* malloc(), free() and memcpy prototypes.
* 
*    Rev 1.6   Nov 14 2001 18:15:46   degends
* Lint Patrol: Removed unused includes.
* 
*    Rev 1.5   Nov 28 2000 11:09:36   degends
*  
* 
*    Rev 1.4   Oct 02 2000 17:05:12   degends
* Changed Stack structure in xmlstk to use a union instead of a
* struct.  Also added an "IsStackEmpty" function to the stack module.
* The doc-handler makes use of this function when it handles chars.
* 
*    Rev 1.3   Sep 13 2000 17:32:46   degends
* This revision has the usual PVCS header at the top of the files.
* Also, DXML now has Blinding built in.
*
*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xmlstk.h"

#define XMLSTK_DEFAULT_SIZE 100

short fnInitStack(Stack *pStack)
{
	/* make sure we have a valid stack */
	if (pStack == NULL)
		return 0;

	/*
	Stack is empty.  Try to get the default
	amount of stack elements.
	*/
	pStack->pItems = (StackElement*)malloc(sizeof(StackElement) * XMLSTK_DEFAULT_SIZE);
	if (pStack->pItems == NULL)
	{
		/* we could not allocate any memory for the stack */
		pStack->iLimit = 0;
		pStack->iTop = 0;
		pStack->opResult = STACK_MALLOC_ERR;
		return 0;
	}
	/* we have the memory, we can continue */
	pStack->iLimit = XMLSTK_DEFAULT_SIZE;
	pStack->iTop = 0;
	pStack->opResult = STACK_NO_ERR;
	return 1;

}
short fnResetStack(Stack *pStack)
{
	if (pStack == NULL)
		return 0;
	if (pStack->pItems == NULL)
		pStack->iLimit = 0;
	pStack->iTop = 0;
	pStack->opResult = STACK_NO_ERR;
	return 1;
}
short fnReleaseStack(Stack *pStack)
{
	short i;
	/* make sure we have a valid stack */
	if (pStack == NULL)
		return 0;

	if (pStack->pItems != NULL)
	{
		/* there was memory allocated, free it */
		/* first delete any dynamically created ptr entries */
		for (i = 0; i < pStack->iTop; i++)
		{
			if (pStack->pItems[i].bType == STACK_DYN_PTR)
			{
				/* we have a dynamically created ptr, free it */
				free(pStack->pItems[i].data.p);
				pStack->pItems[i].data.p = NULL;
			}
		}
		free(pStack->pItems);
	}

	/* set stack data */
	pStack->pItems = NULL;
	pStack->iLimit = 0;
	pStack->iTop = 0;
	return 1;
}
short fnIsStackEmpty(Stack *pStack)
{
	if (pStack->iTop <= 0)
		return 1;
	else
		return 0;
}
short fnPopStack(Stack *pStack, StackElement *pResult)
{
	if (pStack->iTop <= 0)
	{
		pStack->opResult = STACK_EMPTY_ERR;
		return 0;
	}
	
	pStack->iTop -= 1;
	*pResult = pStack->pItems[pStack->iTop];
	pStack->opResult = STACK_NO_ERR;
	return 1;
}
short fnPushStack(Stack *pStack, StackElement bValue)
{
	short iNewLimit;
	StackElement *pTemp = NULL;

	if (pStack->iTop == (pStack->iLimit - 1))
	{
		/*
		The stack has gotten too big.  We will
		try to allocate some more memory.
		*/
		iNewLimit = pStack->iLimit + XMLSTK_DEFAULT_SIZE;
		pTemp = (StackElement*)malloc(iNewLimit * sizeof(StackElement));
		if (pTemp == NULL)
		{
			/* we could not allocate any more memory */
			pStack->opResult = STACK_OVERFLOW_ERR;
			return 0;
		}
		else
		{
			/* We now have more memory, adjust the limit. */
			memcpy(pTemp, pStack->pItems, pStack->iLimit * sizeof(StackElement));
			free(pStack->pItems);
			pStack->pItems = pTemp;
			pStack->iLimit = iNewLimit;
		}
	}

	pStack->pItems[pStack->iTop++] = bValue;
	pStack->opResult = STACK_NO_ERR;
	return 1;
}
short fnPeekStack(Stack *pStack, StackElement *pResult)
{
	if (pStack->iTop >= pStack->iLimit)
	{
		pStack->opResult = STACK_OVERFLOW_ERR;
		return 0;
	}
	else
	{
		*pResult = pStack->pItems[pStack->iTop - 1];
		pStack->opResult = STACK_NO_ERR;
		return 1;
	}
}