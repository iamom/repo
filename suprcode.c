/******************************************************************************
*	PROJECT:    	Phoenix
*   COMPANY:        Pitney Bowes
*   AUTHOR :        Clarisa Bellamy
*   MODULE NAME:    $Workfile:   Suprcode.c  $
*   REVISION:       $Revision:   1.18  $
*       
*   DESCRIPTION:    Non-OI functions and data storage for Supervisor 
*                   password, lock code, and Master Code.
*                 Also Service Mode Entry Code.
*
*	NOTES:		Many routines copied from DeptAccount.c, and oilock.c
* ----------------------------------------------------------------------
*               Copyright (c) 2002 Pitney Bowes Inc.
*                    35 Waterview Drive
*                 Shelton, Connecticut  06484
* ----------------------------------------------------------------------
*
*	REVISION HISTORY:
* 12-Jul-2010 Raymond Shen
*   Modified fnSetSuperPassword() to clear refill code when disabling the supervisior passowrd.
*
* 15-Oct-2008 Joey Cui
*   Modified fnCheckServiceModeEntryCode(), added logic to check engineering code.
*
* 27-Mar-2008 Raymond Shen
*   Modified fnCheckSuperProtection() to fix fraca GMSE00139912. Now we need supervisor 
*   protection for any refill action, including the condition that OOB is not completed.
*
* 20-Mar-2008 Raymond Shen
*   Modified fnSetSuperPassword() to turn off accounting password when super password is disabled.
*
* 19-Mar-2008 Michael Lepore - now use BP_ACCEPT_ONLY_DEFINED_PSWD_FOR_STD_ACCT.
* 
* 14-Mar-2008 Raymond Shen
*	Changed fnIsSuperPasswordAllowed() to fnCheckOnlyDefinedPasswordCanSelectStdAcct().
*
* 12-Mar-2008 Raymond Shen
*	Added fnIsSuperPasswordAllowed() for Brazil to use an EMD parameter to determine
*	whether Supervisor password can select an account from the Main screen or not.
*
* 03-Mar-2008 Raymond Shen
*	Merged all the supervisor password related code from Janus 15.00 branch.
*
* 24-Jan-2006 sa002pe on the Janus 11.00 shelton branch
*	Fixed a lint warning.
*	Moved the prototype for fnSetupAllPasswordProtection here from suprcode.h because it should
*	be a local function. Changed the prototype to not take any parameters because it's always
*	turning everything on.
*	Changed fnSetupAllPasswordProtection to use the new word parameter WP_SECURE_OPERATIONS_REQUIRED
*	to define what should be turned on.
*
* 19-Jan-2006 sa002pe on the Janus 11.00 shelton branch
*	Changed the order of the checks in fnGetPreventDisableSPWD so the check for refill protection
*	is last since sometimes the need for refill protection is ignored by the OI.
*
* 7-Sep-2005 sa002pe on the Mega 15.00 shelton branch
*	Fixed the conditional compile in the previous change because it would actually activate
*	the code for all projects except Spark instead of activating it for just Mega.
*	Need to send out a memo detailing what conditional compile statements will activate code
*	for the various projects.
*
* $Log:   H:/group/SPARK/ARCHIVES/UIC/Suprcode.c_v  $
* 
*    Rev 1.18   30 Aug 2005 14:57:46   nex1ppz
* Modified fnValidateEnterableUniStr() to allow an empty string check for COMET ONLY, 
* This fixed the issue that can not disable Super Password in Comet UIC.
* 
*    Rev 1.17   23 May 2005 13:34:20   SA002PE
* Merging in changes from 1.16.1.0.
* Changed include of edmmfg.h to an include of edm.h.
* 
*    Rev 1.16.1.0   26 Apr 2005 17:58:00   sa002pe
* Removed some more ifdefs.
* Added fnSetupAllPasswordProtection, mostly for
* use by Janus.
* Added some comments about why some of the
* remaining ifdefs have to stay.
* 
*    Rev 1.16   07 Apr 2005 16:28:04   sa002pe
* Removed another ifdef JANUS.
* Added some comments for clarification.
* 
*    Rev 1.15   Apr 06 2005 03:43:20   NEX3RPR
* In function fnCheckSuperProtection(...),added a "break" for Janus use only. -David
* 
*    Rev 1.14   05 Apr 2005 19:27:56   sa002pe
* From David:
* 1. Removed all uses of FAKE_FRANCE_EMD.
* 2. Removed most of the ifdef JANUS conditionals.
* 3. Changed Janus to use the Mega version of fnSetLockCode.
* 
* From Sandra:
* 1. Merged in the David's changes from 1.13.1.0, except for one
* change in fnSetSuperPassword that wasn't needed.
* 2. Removed some more of the ifdef JANUS stuff.
* 3. Added a case for Low Cost Phoenix to the fnLC_PhoenixStyle function.
* 4. Added fnNumericOnlyPasswordStyle function.
* 5. Just to be on the safe side for now, changed fnCheckSuperProtection
* to say that there is no refill password required if we're still in OOB.
----* 
	*    Rev 1.13.1.0   Mar 30 2005 08:56:00   NEX3RPR
	* Changed functions fnSetSuperPassword() and fnGetPreventDisableSPWD(). -David
----*
* 
*    Rev 1.13   27 Jan 2005 18:50:44   sa002pe
* For Janus:
* Changed to make sure the SUPER_FEATURE_SETUP bit is set
* if setting the supervisor password to a non-disabling
* value when the password can't be disabled.
* 
*    Rev 1.12   16 Dec 2004 17:00:00   sa002pe
* For Janus:
* Changed the setting of the supervisor password &
* the meter lock code so they don't inadvertently reset
* a valid existing password or lock code to the default
* value just because the new value is invalid or will
* disable the password or lock code and disabling
* isn't allowed.
* 
*    Rev 1.11   17 Nov 2004 16:18:48   sa002pe
* For Janus:
* 1. Added fnValidateEnterableUniNumStr because the
* passwords & lock code for Janus can only contain numbers.
* 2. Fixed the check of BP_ALLOW_SUPER_PASSWORD_DISABLE.
* It was backwards.
* 3. Made sure the supervisor protection is turned on when the
* supervisor password is set to a value that isn't the disabling
* value.
* 4. Made sure the lock code enable flag is set in the
* fnLockCodeInitialVerify function if the lock code is
* already set to a value that isn't the disabling value.
* 5. Added a code in fnGetPreventDisableSPWD for
* checking if the PCN parameters are set to allow the
* supervisor password to be disabled.
* 
* In other matters:
* Condense the fnSetLockCode function so the conditional
* compiles are only for the areas that are actually different
* between Janus and everyone else.
* 
*    Rev 1.10   16 Nov 2004 09:22:28   NEX8SGV
* Use EMD parameters for the password features. -Victor
* 
*    Rev 1.9   27 Oct 2004 12:32:00   sa002pe
* Added a case for SUPER_FEATURE_SETUP for Janus to
* fnCheckSuperProtection. - Victor
* 
*    Rev 1.8   26 Oct 2004 16:07:38   sa002pe
* Added fnClearLockCode_Internal, fnClearSuperPassword_Internal
* & fnInitSuperPasswordNoEmd.
* For Janus, changed the processing for setting/clearing the
* supervisor password to not assume that the caller checked if the
* supervisor password can be turned off for France.
* Changed the processing for setting/clearing the lock code to
* not assume that the caller checked if the lock code can be disabled.
* Cleaned up some code.
* Added some comments for Janus.
* 
*    Rev 1.7   25 Oct 2004 23:56:24   sa002pe
* Fixed a couple of bugs with the previous changes.
* 
*    Rev 1.6   25 Oct 2004 22:38:08   sa002pe
* Fixed a lint warning.
* 
*    Rev 1.5   25 Oct 2004 22:33:38   sa002pe
* Merging in the Janus stuff so Janus can use the common file.
* 
*-------------------------------------------------
* History of stuff from the Janus archive:
	*    Rev 1.7   25 Oct 2004 10:02:16   NEX8SGV
	* Integraged the lock code setup for the France. -Victor
	* 
	*    Rev 1.6   23 Sep 2004 15:19:10   SA002PE
	* Added fnGetSuperPassword.
	* 
	*    Rev 1.5   Mar 17 2004 09:36:38   NEX0GVE
	* Added account password checking to the function fnCheckUnlock(). -David
	* 
	*    Rev 1.4   Feb 13 2004 09:47:08   CX17595
	* Added fnClearSuperPassword(). -Tim
	* 
	*    Rev 1.3   08 Jan 2004 17:30:06   defilcj
	* 1. changed master password generation algorithm
	* to behave as the lockcode.exe pc program does, which is
	* to pad a serial number less than 8 digits with zeros on the
	* Left (not the right).
	* 
	*    Rev 1.2   Nov 07 2003 09:42:56   CX17598
	* Added file header. - Tim
	* 
	*    Rev 1.1   Oct 15 2003 11:33:30   CX17598
	* Fixed fraca 8159 and another problem about lock code
	* mentioned in mail. -Tim
	* 
	*    Rev 1.0   Jul 16 2003 13:31:20   CX17598
	* Initial version.  Fetched from Phoenix. - Tim
*-------------------------------------------------
*
*    Rev 1.4   Sep 09 2004 13:18:20   CL501BE
* Added new subroutine fnInitSuperPasswordWithEMD.
* 
*    Rev 1.3   Feb 18 2004 13:49:22   CL501BE
* Removed erroneous semicolon from initial test of 
* CMOS data validity.
* 
*    Rev 1.2   Jan 13 2004 16:00:52   CL501BE
* _ Modified the MasterCode algorithm to LEFT-pad instead of 
*   right-pad if the Serial Number is too short (< 8 digits.)
*    This matches the actions of the lockCode generation tool 
*   used by Service and Support.
* _ Renamed the local function fnInitLockCodeEnabled to fnLockCodeInitialVerify
*   and moved the check of the bLockCodeOKFlag into this function from 
*   the calling function.
* _ Modified fnLockCodeInitialVerify to accomodate PCN's that 
*   don't allow the lockcode to be disabled.
* _ Wrote CMOSInit functions for the 2 conditions NoEmd and WithEmd.
* _ Minor mod to fnCheckUnicode() to eliminate the else, and just
*   keep checking the MatchFound flag.
* _ Updated comments.
* 
*    Rev 1.1   Jan 05 2004 16:46:16   CL501BE
* Fix fnCheckSuperPassword() to return the correct error code
* if the Supervisor Password is NOT defined.
* 
*    Rev 1.0   Jan 05 2004 15:07:14   CL501BE
* Contains basic (non-OI) functions regarding supervisor 
* password, MasterCode and LockCode.
* 
* Copied from Phoeinx version 1.9:
*  
*-------------------------------------------------
* History as Phoenix arcihve:
    *
    *     Rev 1.9   Nov 14 2003 12:59:16   CL501BE
    *  Added include files containing prototypes of functions
    *  used here.
    *  
    *     Rev 1.8   Jul 27 2003 23:21:42   bellamyc
    *  _ Added include of datdict.h and function fnCheckServiceModeEntryCode.
    *  _ Changed a couple of functions to use unistrpartcmp, to limit problems
    *    with non-null-terminated strings.
    *  
    *     Rev 1.7   Jul 25 2003 17:04:28   bellamyc
    *  Added second argument to fnSetLockCode function to support
    *  Comet feature of allowing the user save a lockcode string 
    *  without enabling the lock code.  Phoenix code always enables 
    *  the lock code when a valid string is entered.
    *  
    *     Rev 1.6   Jul 15 2003 09:20:52   CX17598
    *  Changed from unistrcpy() to unistrncpy() in function
    *  fnInitLockCodeEnabled() to fix a potential problem. -Tim
    *  
    *     Rev 1.5   Mar 20 2003 11:37:30   bellamyc
    *  Fixed for lint.  Unused variable, and return value was indeterminate on a function.
    *  Not detected because none of the calling functions use the return value.
    *  
    *     Rev 1.4   Jan 31 2003 16:59:52   bellamyc
    *  Fixed fraca 5766: ANY code unlocked the meter.
    *  Removed an errant semicolon at the end of an if-statement.
    *  
    *  
    *     Rev 1.3   Jan 28 2003 01:00:04   bellamyc
    *  Added the non-OI lock code subroutines.
    *  Add verification for both Supervisor Password and Lock Code that the 
    *  CMOS code is enterable (only alphanumeric symbols.)
    *  Made the fnGetMasterPassword function static. ADded the function 
    *   fnClearSuperPassword() for use by an external command. (?)
    *  Added and fixed some comments.
    *  
    *     Rev 1.2   Dec 30 2002 17:00:30   bellamyc
    *  Add that a supervisor password of "0000" disables the supervisor password
    *  if this is a Phoenix system.  This change depends upon oicommon.c and .h 
    *  Version "cwb021230_Phoenix_OI_ID" or higher.
    *  
    *     Rev 1.1   Dec 27 2002 21:06:40   bellamyc
    *  OI functions and data storage for Supervisor password, 
    *  lock code, and Master Code.
    *   Initial Revision.
    *  
    *     Rev 1.0   Dec 08 2002 23:42:20   bellamyc
    *  on-OI-specific functions and data storage for Supervisor Password, 
    *  MasterCode, and LockCode.
    *
******************************************************************************/

#ifndef JANUS
#include <string.h>
#include <ctype.h>
#endif

#include "gtypes.h"
#include "custdat.h"
#include "commontypes.h"
#include "suprcode.h"
#include "pbos.h"			// Required for include of bob.h
#include "bob.h"		// For BOBAMAT0
#include "bobutils.h"
#include "fwrapper.h"   // for fnFlashGetUnicodeStringParm prototype.
#include "datdict.h"
#include "oit.h"        // For fnProcessSCMError prototype.
#include "cwrapper.h"   // For fnCMOSSetupGetCMOSSetupParams prototype.
//#include "oirefill.h"   // For fnClearRefillCode()
#include "utils.h"

#ifdef JANUS
#include "oicommon.h"
#ifndef G900
#include "oiacctj.h"
#endif
extern unsigned long UIC_ID_TYPE;
extern Diagnostics CMOSDiagnostics;

#else
#include "uDmy.h"
#include "lcd.h"        // For UIC_ID_TYPE extern
#include "deptAccount.h"  // For  fnCheckAccountPasswordsAllowed prototype
#endif

//------------------------------------------------------
//     External Variables                               
//------------------------------------------------------

// These must be in custdat.c, so that they are part of PRESERVED CMOS.

// Supervisor Information. */
extern SupervisorInfo		CMOSSupervisorInfo;

// Location of CMOS Lock Code information:
extern SetupParams			CMOSSetupParams;
extern unsigned char	    fCMOSLockCodeEnable;

//------------------------------------------------------
//     Local Variables                                  
//------------------------------------------------------

// This code is used on Phoenix to DISable the lock code or supervisor password.
const unichar     UNICODE_PHNX_DISABLE_PASSWORD[]  = { '0', '0', '0', '0', 0 };

// If the lock Code cannot be disabled, and the current lockcode is garbage, set it 
//  to this default.  (We've already tried the emd default.)    
const unichar   HARDCODE_DEFAULT_LOCKCODE[] = { '1', '1', '1', '1', 0 };

// This flag indicates that the lockcode has been checked and the LockCodeEnable
//  flag is not set with an INVALID lock code.
static char    bLockCodeOKFlag = FALSE;

#define PSD_SERIALNUM_LEN	10

void fnClearLockCode_Internal( sLOCKCODE sLockCode );
void fnClearSuperPassword_Internal( sSuperPassWord sSuperPW );
void fnSetupAllPasswordProtection( BOOL fDoDefault );

//*****************************************************************************

//--------------------------------------------------------------------------
//    Subroutines used by both lock code and supervisor passwords:          
//--------------------------------------------------------------------------

//******************************************************************************
// FUNCTION NAME: fnLC_PhoenixStyle
// AUTHOR       : Clarisa Bellamy
// DESCRIPTION  : Returns the style of lock code disable.
// PARAMETERS   : none
// RETURN       : TRUE if Phoenix-style disable lockcode: 
//                      lock code set to  "0000" (unicode) disables lock code,
//                      because user has no other way to disable it.
//                FALSE if Comet-style disable lockcode:
//                      "0000" is a valid enabled lock code.
//                      User selects OFF from the Lock Code Menu to disable,
//                      and selects ON, then enters and confirms a lock code
//                      in order to enable.
//
// NOTES		: Makes this decision based upon UIC TYPE.
//  This is for LOCK CODE, Supervisor Password, and MasterCode stuff ONLY.
//  Other modules that make decisions based upon UIC type need to do so internally.
//-----------------------------------------------------------------------------
static BOOL fnLC_PhoenixStyle( void )
{
    /*BOOL retval = FALSE;     // Default to Comet-style lock code clearing.

    switch( UIC_ID_TYPE )
    {
        case UIC_TYPE_PHOENIX:
		case UIC_TYPE_PHOENIX_LC:
        case UIC_TYPE_JANUS_DE:
        case UIC_TYPE_JANUS_DE2:
        case UIC_TYPE_JANUS_MT:
        case UIC_TYPE_JANUS_MP:
        case UIC_TYPE_JANUS_DD:
        case UIC_TYPE_NEXTGEM_P0:
        case UIC_TYPE_NEXTGEM_P1:
        case UIC_TYPE_NEXTGEM_P2:
            retval = TRUE;      // Set to Pheonix-style lockcode clearing.
			break;

		default:
			break;
    } */
    BOOL retval = TRUE; //Lan:For DM300/400, we return TRUE for now to enable Phoenix-style lockcode clearling
    return( retval );
}


//******************************************************************************
// FUNCTION NAME: fnNumericOnlyPasswordStyle
// AUTHOR       : Sandra Peterson
// DESCRIPTION  : Returns the style of password or lock code
// PARAMETERS   : none
// RETURN       : TRUE if only numbers are allowed in the password or lock code.
//                FALSE if number & letters are allowed in the password or lock code.
//
// NOTES		: Makes this decision based upon UIC TYPE.
//  This is for LOCK CODE, Supervisor Password, and MasterCode stuff ONLY.
//  Other modules that make decisions based upon UIC type need to do so internally.
//-----------------------------------------------------------------------------
static BOOL fnNumericOnlyPasswordStyle( void )
{
    BOOL retval = FALSE;     // Default to alph-numeric style

    switch( UIC_ID_TYPE )
    {
        case UIC_TYPE_JANUS_DE:
        case UIC_TYPE_JANUS_DE2:
        case UIC_TYPE_JANUS_MT:
        case UIC_TYPE_JANUS_MP:
        case UIC_TYPE_JANUS_DD:
        case UIC_TYPE_NEXTGEM_P0:
        case UIC_TYPE_NEXTGEM_P1:
        case UIC_TYPE_NEXTGEM_P2:
            retval = TRUE;      // Set to numeric-only style
			break;

		default:
			break;
    }
    return( retval );
}


/******************************************************************************
   FUNCTION NAME: fnValidateEnterableUniNumStr
   AUTHOR       : Sandra Peterson
   DESCRIPTION  : Check if unichar is an enterable numeric character
   PARAMETERS   : string  - Pointer to an array of unichars, 
                  len -     number of entries in array to check.
   RETURN       : Returns TRUE if all chars in string are alphanumeric
   NOTES		: Lock codes and passwords must consist of enterable characters.  
                  For Janus, that means that they are limited to numeric only.
******************************************************************************/
BOOL fnValidateEnterableUniNumStr( const unichar *string, int len )
{
    int     i;
    unichar c;    
    
    for( i = 0; i < len; i++)         
    {
        c = string[ i ];
        if( !isdigit( c ) )
            return FALSE;
    }

	return TRUE;
}

/******************************************************************************
   FUNCTION NAME: fnValidateEnterableUniStr
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : Check if unichar is an enterable character
   PARAMETERS   : string  - Pointer to an array of unichars, 
                  len -     number of entries in array to check.
   RETURN       : Returns TRUE if all chars in string are alphanumeric
   NOTES		: Lock codes and passwords must consist of enterable characters.  
                  For now, that means that they are limited to alphanumeric only.
******************************************************************************/
BOOL fnValidateEnterableUniStr( const unichar *string, int len )
{
    int     i;
    unichar c;

#ifndef PHOENIX		// must be Mega because it's the only one that doesn't have Phoenix defined

	if(string[0] == 0) //For Mega, Empty string means to disable the Super Password, so return TRUE.
		return TRUE;
	
#endif
    
	if (fnNumericOnlyPasswordStyle() == TRUE)
	{
		return(fnValidateEnterableUniNumStr(string, len));
	}
	else
	{
	    for( i = 0; i < len; i++)         
	    {
	        c = string[ i ];
	        if( isdigit( c ) ||
	            isupper( c ) ||
	            islower( c )  )
	                ;
	        else
	            return FALSE;
	    }

		return TRUE;
	}
}

/******************************************************************************
   FUNCTION NAME: fnVerifyCodeEnables
   AUTHOR       : Clarisa Bellamy														
   DESCRIPTION  : Verify that the code is not the "disable" string.
                  Disable string for Comet is an empty string.
   PARAMETERS   : input password, which is a fixed-size, null-terminated 
                                  array of unichars.																
   RETURN       : TRUE if code enables, FALSE if Disables.																	
   NOTES		: Calling routine verifies that the string is either empty or
                   4 chars.  															
******************************************************************************/
//TODO for all funcs with pw input - determine if const or not
BOOL fnVerifyCodeEnables(sSuperPassWord sSuperPW )
{
    // IF it is a null-string, does NOT enable.
    if( sSuperPW[ 0 ] == 0 )
        return( FALSE );

    // For Phoenix & Janus & NextGem, the "0000" string also disables any password or lock code.    
    if( fnLC_PhoenixStyle() == TRUE )
    {
        if( unistrpartcmp( UNICODE_PHNX_DISABLE_PASSWORD, sSuperPW, SUPER_PWD_LEN ) == 0 )
            return( FALSE );   
    }

    // If the string does not consist of only enterable characters, it cannot be valid.
    if( fnValidateEnterableUniStr( sSuperPW, SUPER_PWD_LEN ) == FALSE )
        return( FALSE );

    return( TRUE );
}


//******************************************************************************/

/*--------------------------------------------------------------------------*/
/*                   Supervisor Password Functions                          */
/*--------------------------------------------------------------------------*/

/******************************************************************************
   FUNCTION NAME: fnSetSuperPassword
   AUTHOR       : Craig DeFilippo, Clarisa Bellamy														
   DESCRIPTION  : Set the password for the supervisor
   PARAMETERS   : sSuperPW : password to check, must be ptr to array of 
                            4 unichars with null-terminator.
   RETURN       : none																	
   NOTES		: Calling routine verifies that the string is either empty or
                   4 chars.  If the string disables the Supervisor Password,
                   then disable all SP protection.																
//
// MODIFICATION HISTORY:
//              Craig DeFilippo, Clarisa Bellamy  Initial Version.
//  05/19/2006  James Wang      To check whether user is allowed to turn on/off
//                              the use of the supervisor password feature 
//                              with a new BR function, and replace access to 
//                              CMOSSupervisorInfo with function fnGetCMOSSupervisorInfo.																
//  03/20/2008  Raymond Shen    Turn off accounting password when super password is disabled.
******************************************************************************/
void fnSetSuperPassword( sSuperPassWord sSuperPW )
{
    BOOL    fDoDefault = TRUE;      // Set default protections by default.

    SupervisorInfo *pSupervisorInfo = fnGetCMOSSupervisorInfo();

    // Make sure it is null-terminated.
	sSuperPW[ SUPER_PWD_LEN ] = 0;

	// Check to make sure the password is valid in terms of the type of characters in the password.
	// If the new password has valid characters -OR-
	// if the new password has invalid characters and the current password is bad or disabled,
	// proceed with trying to change the password.
    if( (fnValidateEnterableUniStr( sSuperPW, SUPER_PWD_LEN ) == TRUE) ||
		( (fnValidateEnterableUniStr( sSuperPW, SUPER_PWD_LEN ) == FALSE) &&
		  (fnVerifyCodeEnables( pSupervisorInfo->sPassWord ) == FALSE) ) )
	{
		// Check if it is going to disable the password
	    if( fnVerifyCodeEnables( sSuperPW ) == FALSE  )
	    {
	        // If we CAN disable the supervisor password, do so now.  This parameter
	        //  is actually FALSE if disabling IS allowed.
	        if( fnIsSuperPasswordDisableAllowed() == TRUE )
	       	{
				(void)unistrncpy( pSupervisorInfo->sPassWord, sSuperPW, SUPER_PWD_LEN +1 );

				// Turn off accounting password when super password is disabled.
//				fnActTurnOffPassword();

				// Turn off Refill Code when super password is disabled.
//				fnClearRefillCode();

		        // Disable all the feature SPWD protection.
#if defined (JANUS)
		        pSupervisorInfo->usSuperProtectedFeature = 0;
#else
		        pSupervisorInfo->ucSuperProtectedFeature = 0;
#endif
	       	}
	       	else
	       	{
				// If we can't disable, check if the current password is valid.
	    		if( fnVerifyCodeEnables( pSupervisorInfo->sPassWord ) == FALSE )
				{
					// If it isn't, set to a hardcode default.
					(void)unistrncpy( pSupervisorInfo->sPassWord, HARDCODE_DEFAULT_LOCKCODE, SUPER_PWD_LEN +1 );

					// Turn on the protections as necessary
                    fnSetupAllPasswordProtection( fDoDefault );

				}
			}
	   	}
		else // changing the password to a non-disabling value
		{
            if( fnVerifyCodeEnables( CMOSSupervisorInfo.sPassWord ) == TRUE )
            {
                fDoDefault = FALSE;                
            }
			(void)unistrncpy( pSupervisorInfo->sPassWord, sSuperPW, SUPER_PWD_LEN +1 );
			
			// Turn on the protections as necessary
            fnSetupAllPasswordProtection( fDoDefault );
			
		}
	}
	// else, the new password is bad and the existing password is a good non-disabling password,
	// so don't change the password.
}

/******************************************************************************
   FUNCTION NAME: fnCheckSuperPassword
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Compare supervisor password with one passed in
   PARAMETERS   : sSuperPW : password to check, must be ptr to array of 
                            4 unichars with null-terminator.
   RETURN       : SUPER_SUCCESS
				: error if passwords don't match
   NOTES		: none
******************************************************************************/
ushort fnCheckSuperPassword(sSuperPassWord sSuperPW )
{
    if( fnCheckSuperPasswordDefined() == FALSE )
	    return( SUPER_PASSWORD_UNDEFINED );

	// Get the unistring out of CMOS to a local copy...
	if( unistrcmp( CMOSSupervisorInfo.sPassWord, sSuperPW ) )
		return( SUPER_PASSWORD_MISMATCH );
	else
		return( SUPER_SUCCESS );
}

/******************************************************************************
   FUNCTION NAME: fnCheckSuperPasswordDefined
   AUTHOR       : Craig DeFilippo, Clarisa Bellamy
   DESCRIPTION  : Check if supervisor password is in use
   PARAMETERS   : none
   RETURN       : True if a password exists, false otherwise
   NOTES		: none
******************************************************************************/
BOOL fnCheckSuperPasswordDefined( void )
{
	return( fnVerifyCodeEnables( CMOSSupervisorInfo.sPassWord ) );
}

/******************************************************************************
   FUNCTION NAME: fnGetSuperPassword
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Get the supervisor password
   PARAMETERS   : pass back password, with Null-terminator
   RETURN       : none
   NOTES		: Used for Overriding Account password.
******************************************************************************/
void fnGetSuperPassword( sSuperPassWord sSuperPW )
{
    // Copy the string, including the null-terminator.
    (void)unistrcpy( sSuperPW, CMOSSupervisorInfo.sPassWord );
    return;
}

/******************************************************************************
   FUNCTION NAME: fnSetSuperProtection
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Set supervisor protection for a feature
   PARAMETERS   : feature ID, fStatus (true = protect on, false = protect off)
   RETURN       : none															 
   NOTES		: ucFeature = any of the SUPER_FEATURE_xxx values defined in suprcode.h
******************************************************************************/
void fnSetSuperProtection( uchar ucFeature, BOOL fStatus )
{
#if defined (JANUS)
    unsigned short  usMask;


    usMask = (1 << ucFeature);
    if( fStatus )
    {
        // If fStatus is non-zero, set the bit for the feature. 
        CMOSSupervisorInfo.usSuperProtectedFeature |= usMask;
    }
    else
    {
        // If fStatus is 0, clear the bit for the feature.
        CMOSSupervisorInfo.usSuperProtectedFeature &= ~usMask;      
    }
#else
	unsigned char	ucMask;


	ucMask = (1 << ucFeature);
	if( fStatus )
	{
	    // If fStatus is set, set the bit for the feature. 
		CMOSSupervisorInfo.ucSuperProtectedFeature |= ucMask;
	}
	else
	{
		// If fStatus is 0, clear the bit for the feature.
		CMOSSupervisorInfo.ucSuperProtectedFeature &= ~ucMask;		
    }
#endif                             
}

/******************************************************************************
   FUNCTION NAME: fnCheckSuperProtection
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Check supervisor protection for a feature
   PARAMETERS   : feature ID
   RETURN       : Bool (true = protect on, false = protect off)
   NOTES		: ucFeature = SUPER_FEATURE_ACCOUNTING_SETUP or
   				: SUPER_FEATURE_REFILL or SUPER_FEATURE_BATCH_CLEAR
   MODIFICATION HISTORY:
    03/27/2008  Raymond Shen    Fixed fraca GMSE00139912. Now we need supervisor 
                                protection for any refill action, including the
                                condition that OOB is not completed.
******************************************************************************/
BOOL fnCheckSuperProtection( uchar ucFeature )
{
    BOOL fRetval = FALSE;
    ushort  usMask;

    usMask = 1 << ucFeature;
    switch (ucFeature)
	{
#ifdef JANUS	// for usSuperProtectedFeature vs. ucSuperProtectedFeature

        // Check if the indicated bit is set...
        default:
            if ( CMOSSupervisorInfo.usSuperProtectedFeature & usMask )
                fRetval = TRUE;
            break;
#else

        // Check if the indicated bit is set...
        default:
            if ( CMOSSupervisorInfo.ucSuperProtectedFeature & usMask )
                fRetval = TRUE;
            break;
#endif
	}

	return (fRetval);
}

//******************************************************************************
//   FUNCTION NAME: fnCheckSuperProtectionRequired
//   AUTHOR       : Clarisa Bellamy
//   DESCRIPTION  : Check if supervisor protection for a feature is required or 
//                    can be disabled.
//   PARAMETERS   : feature ID
//   RETURN       : Bool (true = protection required, false = protection can be disabled)
//   NOTES        : ucFeature = SUPER_FEATURE_ACCOUNTING_SETUP or any of the 
//                        other SUPER_FEATURE_s defined in suprcode.h
// -----------------------------------------------------------------------------
BOOL fnCheckSuperProtectionRequired( uchar ucFeature )
{
    UINT16  usMask = 0;
    UINT16  usRequired = 0;
    BOOL    fRetval = FALSE;

    usMask = (1 << ucFeature);
    usRequired = fnFlashGetWordParm( WP_SUPER_PROTECTION_REQUIRED );
    if( usMask & usRequired )
        fRetval = TRUE;
        
    return( fRetval );    
}

/******************************************************************************
//   FUNCTION NAME: fnGetPreventDisableSPWD
//   AUTHOR       : Clarisa Bellamy
//   DESCRIPTION  : Checks for conditions that will not allow the user to 
//                  clear the supervisor password.  It checks in order of 
/                   presedence.  
//   PARAMETERS   : None.
//   RETURN       : SPWD_DIS_OK if the Supervisor Password can be disabled.
//                  otherwise, a cause code for not allowing the disable.
//                  Codes enum'd in suprpwd.h file.
//   NOTES		  : 
******************************************************************************/
SPWD_NODIS_REASON fnGetPreventDisableSPWD( void )
{
    if( fnCheckAccountPasswordsAllowed() )
        return( SPWD_NODIS_ACCT );

// Phoenix also uses this function, so they'll have to handle the SPWD_NODIS_SETUP
// condition when (if ever) they merge to the tips
	if ( fnCheckSuperProtection( SUPER_FEATURE_SETUP ) == TRUE )
        return( SPWD_NODIS_SETUP );

    if( fnCheckSuperProtection( SUPER_FEATURE_REFILL ) == TRUE ) 
        return( SPWD_NODIS_REFILL );

    return( SPWD_DIS_OK );    
}

/******************************************************************************
   FUNCTION NAME: fnClearSuperPassword_Internal
   AUTHOR       : Sandra Peterson														
   DESCRIPTION  : Clear the given supervisor password buffer.
   PARAMETERS   : None
   RETURN       : none																	
   NOTES		: none																
******************************************************************************/
void fnClearSuperPassword_Internal( sSuperPassWord sSuperPW )
{
    if( fnLC_PhoenixStyle() == TRUE )
    {
        // For Phoenix, Janus & NextGem, set the supervisor password to "0000".
        (void)unistrncpy( sSuperPW, UNICODE_PHNX_DISABLE_PASSWORD, SUPER_PWD_LEN +1 );
    }
	else
	{
        // For Comet, setting the code to a null string disables it.
        sSuperPW[ 0 ] = 0;    
	}
}


/******************************************************************************
   FUNCTION NAME: fnClearSuperPassword
   AUTHOR       : Clarisa Bellamy														
   DESCRIPTION  : Set the password for the supervisor to cleared state.
   PARAMETERS   : None
   RETURN       : none																	
   NOTES		: Called by ModifyFeature function																
******************************************************************************/
void fnClearSuperPassword( void )
{
    sSuperPassWord     sSuperPWD;

    fnClearSuperPassword_Internal(sSuperPWD);

    // This will clear all the protected flags if the password can be disabled.
    fnSetSuperPassword( sSuperPWD );
}


// **************************************************************************
// FUNCTION NAME:   fnInitSuperPasswordNoEMD
//   DESCRIPTION:   Even if there IS an EMD, this is called first.
//                  So just make sure the supervisor password is not set to 
//                  something that is unenterable.
//
//        AUTHOR:   Sandra Peterson 
//
//        INPUTS:	nada
//       RETURMS:   nada 
//         NOTES:
// ----------------------------------------------------------------------
void fnInitSuperPasswordNoEMD( void )
{
    // Without an EMD, there should be no supervisor password.
    fnClearSuperPassword_Internal(CMOSSupervisorInfo.sPassWord);

    // Clear the flags.
#if defined (JANUS)
    CMOSSupervisorInfo.usSuperProtectedFeature = 0;
#else
    CMOSSupervisorInfo.ucSuperProtectedFeature = 0;
#endif
}


/******************************************************************************
   FUNCTION NAME: fnInitSuperPasswordWithEMD
   AUTHOR       : Clarisa Bellamy									
   DESCRIPTION  : Set the password for the supervisor to the default value in 
                    the EMD.  IF that default value is not a valid enabling
                    password, clear the password and all the 
                    protected-by-supervisor-password flags.
   PARAMETERS   : None
   RETURN       : none		
   NOTES		: Called by CMOS.C:fnWithEmdInit() function.
******************************************************************************/
void fnInitSuperPasswordWithEMD( void )
{
    sSuperPassWord     sSuperPWD;
    UINT16          *pPassword;

    // Read the default string from EMD.
    pPassword = fnFlashGetUnicodeStringParm(USP_SUPER_PASSWORD_DEFAULT);
    if (pPassword && (unistrlen(pPassword) == SUPER_PWD_LEN))
    {
        (void)unistrncpy( sSuperPWD, pPassword, SUPER_PWD_LEN );
        // Make sure it is null-terminated, since wrapper routine does not.
        sSuperPWD[ SUPER_PWD_LEN ] = 0;

        // If it is not valid, clear the Supervisor password, and clear all protected flags.
        if( fnVerifyCodeEnables( sSuperPWD ) == FALSE )
            fnClearSuperPassword();

        else    // If it IS valid, set it.
            fnSetSuperPassword( sSuperPWD );
    }
    else
    {
        fnClearSuperPassword();
    }

    return;
}


/* *************************************************************************
// FUNCTION NAME: fnSetupAllPasswordProtection
// DESCRIPTION: This function sets all the password protections based on two
//      UIC word parameters, one indicates which features/functions are required
//      to be password protected and which are password protected by default.
//      For a list of the bitmaps of features/functions that can be password 
//      protected see the SUPER_FEATURE_ definitions in suprcode.h file.
//
// AUTHOR: David Huang/Sandra Peterson
//
// INPUTS:  None
// MODIFICATIONS:
//  2006.06.12 Clarisa Bellamy: 
//    For Mega, the new definitions of WP_SUPER_PROTECTION_DEFAULT and 
//      WP_SUPER_PROTECTION_REQUIRED are implemented.
//      Added a parameter that indicates whether the protection should be
//      reset to default.
//      Whether resetting to default or not, always enable all required 
//      protections.
//    USAGE:
//      The default protections do not get set when the password is changed, 
//      only when it goes from disabled to enabled.  
//      The REQUIRED protections will get set every time.

// *************************************************************************/
void fnSetupAllPasswordProtection( BOOL fDoDefault )
{
    UINT16  uwDefMask;
    UINT16  uwReqMask;

    uwDefMask = fnFlashGetWordParm( WP_SUPER_PROTECTION_DEFAULT );
    uwReqMask = fnFlashGetWordParm( WP_SUPER_PROTECTION_REQUIRED );

#ifndef JANUS	// Mega/Phoenix	// for ucSuperProtectedFeature vs. usSuperProtectedFeature
    // Set according to EMD parameters.
    if( fDoDefault )
    {
        // If password was disabled, set the protections to default 
        //  (plus requirements)
        uwDefMask |= uwReqMask;
        CMOSSupervisorInfo.ucSuperProtectedFeature = (unsigned char)uwDefMask;
    }
    else
    {
        // If just changing the password, make sure minimum 
        // requirements for protection are set, but leave the rest.
        CMOSSupervisorInfo.ucSuperProtectedFeature |= (unsigned char)uwReqMask;           
    }

#else	// Janus and later
    // Set according to EMD parameters.
    if( fDoDefault )
    {
        // If password was disabled, set the protections to default 
        //  (plus requirements)
        uwDefMask |= uwReqMask;
        CMOSSupervisorInfo.usSuperProtectedFeature = uwDefMask;
    }
    else
    {
        // If just changing the password, make sure minimum 
        // requirements for protection are set, but leave the rest.
        CMOSSupervisorInfo.usSuperProtectedFeature |= uwReqMask;           
    }

#ifdef JANUS	// need this for Janus even after the everyone changes to usSuperProtectedFeature
    CMOSSupervisorInfo.fPasswordSetBefore = TRUE;
#endif
#endif

    return;
}
#if 0
/* *************************************************************************
// FUNCTION NAME: fnCheckOnlyDefinedPasswordCanSelectStdAcct
//
// DESCRIPTION:
//      Utility function that checks whether only the defined password
//      can select Std Account or not.
//
// INPUTS:
//      None
//
// RETURNS:
//      TRUE or FALSE
//
// WARNINGS/NOTES:  
//      This funciton is added for Brazil in which only
//      the defined password can select Std Account.
//
// MODIFICATION HISTORY:
//  03/12/2008	Raymond Shen   Initial Version
//
// *************************************************************************/
BOOL fnCheckOnlyDefinedPasswordCanSelectStdAcct(void)
{
    BOOL    fRetval = FALSE;

    fRetval = fnFlashGetByteParm( BP_ACCEPT_ONLY_DEFINED_PSWD_FOR_STD_ACCT );

    return fRetval;
}

#endif
//******************************************************************************/

/*--------------------------------------------------------------------------*/
/*                      Lock Code Functions                                */
/*--------------------------------------------------------------------------*/

//*****************************************************************************
// FUNCTION NAME: fnSetLockCode
// AUTHOR       : Clarisa Bellamy														
// DESCRIPTION  : Saves the passed in Meter Lock Code in CMOS.
//                  If fAdjustEnable is set OR we have Phoenix-Style
//                  lockcodes (0000 disables), then examine the new lock
//                  code and update the lock code enable flag in CMOS.
// PARAMETERS   : Lock code - Null-terminated, 4-unichar string.
// RETURN       : BOOL - FALSE if lock code is disabled, TRUE if set.
// NOTES		: Does not check the validity of the input lockcode for proper 
//                  length or null-terminator.  Calling routine must do this.
//                 The CMOS version is NOT null-terminated (array is only
//                  LOCK_CODE_LEN long.)  So must use unistrncpy() instead of
//                  unistrcpy().		
//          VerifyCodeEnables checks that the new lockcode is enterable, and 
//          does not DISable the lock feature.
//
// MODIFICATION HISTORY:
//              Clarisa Bellamy Initial Version
//  05/19/2006  James Wang      To check whether user is allowed to turn on/off
//              the use of the locking code feature with a new BR functionand, 
//              and replace access to variable CMOSSetupParams with global function 
//              fnCMOSSetupGetCMOSSetupParams.
//*****************************************************************************
BOOL fnSetLockCode( sLOCKCODE sLockCode, BOOL fAdjustEnable )
{
    SetupParams *pSetupParams = fnCMOSSetupGetCMOSSetupParams();

    	// Make sure it is null-terminated for the checks.
	sLockCode[ LOCK_CODE_LEN ] = 0;

    // For Comet, only Enable the lock code IF the fAdjustEnable is TRUE
    // For Phoenix & Janus & NextGem, SETTING a valid lock code always enables the lockcode.
    if( fnLC_PhoenixStyle() || fAdjustEnable )
		fCMOSLockCodeEnable = TRUE;

	if( fnVerifyCodeEnables( sLockCode ) == FALSE  )
	{
        // If we CAN disable the lock code, do so now.  This flash variable
        //  is actually FALSE if disabling IS allowed.  Go figure!
		if(fnIsLockCodeDisableAllowed() == TRUE)
		{
			(void)unistrncpy( pSetupParams->pLockCodeUnicode, sLockCode, LOCK_CODE_LEN );
			fCMOSLockCodeEnable = FALSE;
		}
		else
		{
			// If we can't disable, check if the current lock code is valid.
	    	if( fnVerifyCodeEnables( pSetupParams->pLockCodeUnicode ) == FALSE )
			{
				// If it isn't, set to a hardcode default.
				(void)unistrncpy( pSetupParams->pLockCodeUnicode, HARDCODE_DEFAULT_LOCKCODE, LOCK_CODE_LEN );
			}
		}
	}
	else
		(void)unistrncpy( pSetupParams->pLockCodeUnicode, sLockCode, LOCK_CODE_LEN );

	// Return lock code enabled/disabled status.
	return( fCMOSLockCodeEnable ? TRUE : FALSE );
}

/******************************************************************************
   FUNCTION NAME: fnClearLockCode_Internal
   AUTHOR       : Sandra Peterson														
   DESCRIPTION  : Clears a given Lock Code buffer.
   PARAMETERS   : None
   RETURN       : none																	
   NOTES		: This function is only used in this file.
******************************************************************************/
void fnClearLockCode_Internal( sLOCKCODE sLockCode )
{
    if( fnLC_PhoenixStyle() == TRUE )
    {
        // For Phoenix,Janus & NextGem, setting the lock code to "0000" disables it.
        (void)unistrncpy( sLockCode, UNICODE_PHNX_DISABLE_PASSWORD, LOCK_CODE_LEN );
    }
    else
    {
        // For Comet, setting the lock code to a null string disables it.
        sLockCode[ 0 ] = 0;    
    }
}

/******************************************************************************
   FUNCTION NAME: fnClearLockCode
   AUTHOR       : Clarisa Bellamy														
   DESCRIPTION  : Disables the Lock Code.																
   PARAMETERS   : None
   RETURN       : none																	
   NOTES		: This function is used by Comet OI.
******************************************************************************/
void fnClearLockCode( void )
{
	sLOCKCODE sLockCode;

	// clear the lock code
	fnClearLockCode_Internal( sLockCode );

	// store it & set/clear the appropriate flags
    (void)fnSetLockCode( sLockCode, TRUE );
}

/******************************************************************************
   FUNCTION NAME: fnCheckLockCode
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : Compare Meter Lock Code with one passed in
   PARAMETERS   : Lock code - Null-terminated, 4-unichar string.
   RETURN       : BOOL - TRUE if a match.
   NOTES		: Called to compare unistring with the CMOS lock code.
                  Right now, the CMOS version is NOT null-terminated (array is  
                  only LOCK_CODE_LEN long.)  So the must use unistrpartcmp() 
                  instead of unistrcmp().
******************************************************************************/
BOOL fnCheckLockCode( const sLOCKCODE sLockCode )
{
	if( unistrpartcmp( CMOSSetupParams.pLockCodeUnicode, sLockCode, LOCK_CODE_LEN ) == 0 )
    	return( TRUE );

    else
		return( FALSE );
}

/******************************************************************************
   FUNCTION NAME: fnCheckUnlock
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : Compare Input code with Lock Code and any code that
                    can override the lock code: 
                        Supervisor Password
                        MasterCode.
   PARAMETERS   : Lock code - Null-terminated, 4-unichar string.
   RETURN       : BOOL
   NOTES		: Called to compare unistring with ANY code that can unlock
                    the meter.
******************************************************************************/
BOOL fnCheckUnlock( sLOCKCODE sLockCode )
{
    BOOL    fMatchFound = FALSE;

    // Make sure it is null-terminated for the checks.
	sLockCode[ LOCK_CODE_LEN ] = 0;

    // Check if it matches the lock code stored in CMOS.
	if( fnCheckLockCode( sLockCode ) )
        fMatchFound = TRUE;

    if( !fMatchFound )
    {   // Check if it matches the supervisor password, if there is one.
        if( (fnCheckSuperPasswordDefined() == TRUE) &&
            (fnCheckSuperPassword( sLockCode ) == SUPER_SUCCESS) )
            
                fMatchFound = TRUE;
    }

    if( !fMatchFound )
    {   // Check if it matches the MasterCode.
        if( fnCheckMasterCode( sLockCode ) == TRUE )
            fMatchFound = TRUE;
    }

    return( fMatchFound );
}

/******************************************************************************
   FUNCTION NAME: fnLockCodeInitialVerify
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : Verify that the lock code is enterable and valid.  
                  If it is not, then we disable the lock code, but if we are
                   on a system that should NEVER have the lock code disabled,
                   then set the lock code to a HARDCODE DEFAULT value.
   PARAMETERS   : none
   RETURN       : None
      NOTES		: This only needs to be done once each powerup, so set a local
                    static varible to TRUE when it is done. 
                  We also want to do this if we have a new EMD, so the calling 
                  function will clear the OK flag before this is called.

// MODIFICATION HISTORY:
//              Clarisa Bellamy     Initial version
//  05/22/2006  James Wang          To check whether user is allowed to turn  
//              on/off the use of the locking code feature with a new BR function,  
//              and replace access to variable CMOSSetupParams with global  
//              function fnCMOSSetupGetCMOSSetupParams.
//
******************************************************************************/
static void fnLockCodeInitialVerify( void )
{
    sLOCKCODE   sLockCode;

    // Normally, only have to do this once, unless we've just read in new lockcode from EMD.
    if( bLockCodeOKFlag != TRUE )
    {        
        // Create a copy that is null-terminated, bceause the verify function requires 
        //  a null-terminated array to work properly.
        (void) unistrncpy( sLockCode, fnCMOSSetupGetCMOSSetupParams()->pLockCodeUnicode, LOCK_CODE_LEN );
        sLockCode[ LOCK_CODE_LEN ] = 0;
       
#if defined (JANUS) && !defined (K700)
		// Janus is set up to only have a lock code in the EMD when
		// lock codes are required, so for the moment default to the code being enabled.
           fCMOSLockCodeEnable = TRUE;   
		
		// Everyone else, on the other hand, always has a lock code in the EMD even
		// when it isn't needed.  It is then up to the operator to enable the code
		// via the user interface.
#endif

        // If the lock code is disabled, indicate that it is disabled -OR-
		// If the lock code is bad, fix it or disable it.
        if( fnVerifyCodeEnables( sLockCode ) == FALSE )
        {
            // If we CAN disable the lock code, do so now.  This flash variable
            //  is actually FALSE if disabling IS allowed.  Go figure!
            if( fnIsLockCodeDisableAllowed() == TRUE )
            {
                fCMOSLockCodeEnable = FALSE;            
                fnClearLockCode();            
            }
            else
            {   // If we can't disable, set to a hardcode default.
                (void)fnSetLockCode( HARDCODE_DEFAULT_LOCKCODE, TRUE );
            }
        }

        // Indicate the lock code is ok.
        bLockCodeOKFlag = TRUE;
    }
    return;
}
    


/******************************************************************************
   FUNCTION NAME: fnCheckLockCodeEnabled
   AUTHOR       : Craig DeFilippo
   DESCRIPTION  : Check if lockcode is enabled
   PARAMETERS   : none
   RETURN       : True if a lockcode enabled, false otherwise
   NOTES		: none
******************************************************************************/
BOOL fnCheckLockCodeEnabled( void )
{
    // The first time we call this function, it will verify that the lock code 
    //  is valid OR the Enable flag is cleared.
    fnLockCodeInitialVerify();

    return( fCMOSLockCodeEnable );
}


/******************************************************************************
   FUNCTION NAME: fnClearLockCodeEnabled
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : Just clears the CMOS byte enabling lock code.
   PARAMETERS   : none
   RETURN       : True if a lockcode enabled, false otherwise
   NOTES		: This was required for one of the COMET OI functions that
                    was accessing the Enabled flag directly.  
                 If Comet cleared the lock code whenever it was disabled,
                 it would call the fnClearLockCod() function instead.
                 I don't know WHY the Comet code doesn't clear the lockcode
                 if the lock code is feature disabled.
******************************************************************************/
void fnClearLockCodeEnabled( void )
{
    fCMOSLockCodeEnable = FALSE;    
}


// **************************************************************************
// FUNCTION NAME:   fnCMOSInitLockCodeNoEmd
//   DESCRIPTION:   Even if there IS an EMD, this is called first.
//                  So just make sure the lock code is not set to 
//                  something that is unenterable.
//
//        AUTHOR:   Clarisa Bellamy, 
//
//        INPUTS:	nada
//       RETURMS:   nada 
//         NOTES:
// ----------------------------------------------------------------------
void fnCMOSInitLockCodeNoEmd( void )
{
    // Without an EMD, there should be no lock code.
    fnClearLockCode_Internal(CMOSSetupParams.pLockCodeUnicode);

    // Clear the flag.
    fnClearLockCodeEnabled();
}


// **************************************************************************
// FUNCTION NAME:   fnCMOSInitLockCodeWithEmd
//   DESCRIPTION:   Normally the lock code is DISabled on a new unit.  However
//                  the EMD may indicate that disabling the lockcode is not
//                  allowed, in which case the lock code is ENabled on a new 
//                  unit. 
//                  In either case, the lock code itself is set to the default 
//                  in the EMD, then verified that it is valid.  If it is not 
//                  a valid enabling lock code, then the lock code is disabled.
//                  IF disabling is not allowed, then set the lockcode to 
//                  to the hardcoded value
//
//        AUTHOR:   Clarisa Bellamy, 
//
//        INPUTS:	nada
//       RETURMS:   nada 
//         NOTES:   Accomodates France.
// ----------------------------------------------------------------------
void fnCMOSInitLockCodeWithEmd( void )
{
    char *  pDefaultLockCode;

    // Read the default lock code from the EMD, convert it to Unicode and store it in CMOS.
    pDefaultLockCode = (char *)fnFlashGetAsciiStringParm( ASP_DEFAULT_LOCK_CODE );
    if( pDefaultLockCode && (strlen(pDefaultLockCode) == LOCK_CODE_LEN))
        fnAscii2Unicode( pDefaultLockCode,  &CMOSSetupParams.pLockCodeUnicode[0],  LOCK_CODE_LEN );

    // Set enabled flag in CMOS, if disabling is allowed, disable, otherwise, enabled.
    // This flash variable is actually FALSE if disabling IS allowed.  Go figure!
    if( fnFlashGetByteParm(BP_ALLOW_LOCK_CODE_DISABLE) == FALSE )
        fCMOSLockCodeEnable = FALSE;
    else
        fCMOSLockCodeEnable = TRUE;
    
    // Clear this flag so the following function will check stuff, even if it has been called 
    //  before this function was called.
    bLockCodeOKFlag = FALSE;
    // Verify that the lockcode in CMOS is valid, enterable, and enables locking.  If not,
    //   clear enabled flag, or set lock code to a hardcoded, known-good, default.
    fnLockCodeInitialVerify();
}


/******************************************************************************

/*--------------------------------------------------------------------------*/
/*                      MasterCode Functions                                */
/*--------------------------------------------------------------------------*/

/* *************************************************************************
// FUNCTION NAME: fnGetMasterPassword
// DESCRIPTION: This is a PRIVATE utility function that calculates the master
//				lock code for the meter.  The master code is based on the
//				postage by phone serial number.
//
// AUTHOR: Joe Mozdzer, Clarisa Bellamy
//
// INPUTS:	pDest- Container in which to return the master password in
//					Unicode format.  The caller must allocate enough memory
//					to hold it, The master code is by definition, the same
//                  length as the Lock Code.
//
// NOTES:  If an external file needs to check the master code, use the 
//              fnCheckMasterCode() function (below) instead.
// *************************************************************************/

// Since we don't want to know or care which is longer, make room for both. 
//  Which automatically makes room for a null-terminator too.
#define BUFF_SIZE           ( PSD_SERIALNUM_LEN + (LOCK_CODE_LEN * sizeof(unichar)) )

static void fnGetMasterPassword( unichar *pDest )
{
    // This guarantees that the Static value is recalculated the first time
    //  that it is needed after a restart.  It is stored as 2 bytes, even though
    //  it loads 10 bytes (4 unichars, plus a null terminator.)
	static unsigned short	wStaticMasterCode = 0xFFFF;	/* keep the master code in a static so
															that we don't have to recalculate
															it every time this function is
															called.  the initial value of FFFF
															indicates we haven't yet done the
															calculation. */

	if( wStaticMasterCode == 0xFFFF )
	{
	    char   	pSerialAscii[ BUFF_SIZE ];
	    char 	pSerial[ LOCK_CODE_LEN ];
	    int     iStrLen, i;
	    char   *pSerialUse;
        int     pad;

        // Clear the buffer so the string will be null-terminated, whether or not it's nullterminated in the PSD.
        (void)memset( pSerialAscii, '\0', sizeof( pSerialAscii ) );
	    // if the master code has not yet been calculated, we have to first fetch
	    //	the serial number from the bob task.  It should fill pSerialAscii with an
	    //	ASCII numeric string, PSD_SERIALNUM_LEN chars or less. 
		if( fnValidData(BOBAMAT0, VLT_INDI_FILE_PBP_SERIAL, (void *)pSerialAscii) != BOB_OK )
		{
			fnProcessSCMError();
			return;
		}

		/* Make sure the string from Bob is null-terminated. */
		pSerialAscii[ PSD_SERIALNUM_LEN ] = '\0';

		// Get the length of the serialNumber. 
        iStrLen = (int)strlen( pSerialAscii );
		
		// If Serial Number returned is < 8 chars, right-pad it with 0's 
		//while( iStrLen < (LOCK_CODE_LEN * 2) ) 
		//    pSerialAscii[ iStrLen++ ] = '0';
		//pSerialAscii[ iStrLen ] = '\0';

		// The pc tool named "lockcode.exe" uses this algorithm which pads on the LEFT, defilcj
		// If Serial Number returned is < 8 chars, LEFT-pad it with 0's 
		pad = (LOCK_CODE_LEN * 2) - iStrLen;
		if( pad > 0 )
		{
			//shift
			for( i = iStrLen-1; i >= 0; i-- )
			{
				pSerialAscii[ i+pad ] = pSerialAscii[ i ];
			}
			// pad left with zeros
			for( i = 0; i < pad; i++ )
			{
				pSerialAscii[ i ] = '0';
			}
			//update length
            iStrLen = (int)strlen( pSerialAscii );
		} 
		
		// At this point iStrLen is >= LOCK_CODE_LEN *2 ... (8). */
		//  Use the LAST 8 (LOCK_CODE_LEN *2) chars of the serial number. 
		pSerialUse = pSerialAscii + (iStrLen - (LOCK_CODE_LEN * 2));
		
		// Replace any non-digit characters with '0'. 
		for( i =0; i < (LOCK_CODE_LEN *2) ; i++ )
		{
		    if( (pSerialUse[ i ] < '0') || (pSerialUse[ i ] > '9') )
			    pSerialUse[i] = '0';
		}

		// Convert from ascii numeric to Packed BCD. 
        (void)fnAsciiBcdToPackedBcd( (unsigned char *)pSerial, LOCK_CODE_LEN, pSerialUse, TRUE );


		// the serial number in bcd format is scrambled according to the following
		//  algorithm to produce the master code in bcd format.  the algorithm is
		//	the same as the one used for the Gem meter. 		
		do {
			pSerial[1] = -pSerial[1];
			pSerial[0] = pSerial[3] ^ pSerial[0];
			pSerial[0] += pSerial[2];
			pSerial[1] += pSerial[0];
            pSerial[3] = (char)((unsigned char)pSerial[3] << 1);
		} while (pSerial[3] != 0);

		pSerial[0] = pSerial[0] & 0x77;
		pSerial[1] = pSerial[1] & 0x77;
        (void)memcpy( &wStaticMasterCode, pSerial, 2 );

		wStaticMasterCode += 0x1111;
	}

    // Convert the BCD Master code to a unicode version.
	fnBCD2Unicode( (unsigned char *) &wStaticMasterCode, pDest, LOCK_CODE_LEN );
	return;
}




/******************************************************************************
   FUNCTION NAME: fnCheckMasterCode
   AUTHOR       : Clarisa Bellamy
   DESCRIPTION  : Compare Value entered for Meter Lock Code with Master Code
   PARAMETERS   : Lock code - Null-terminated, 4-unichar string.
   RETURN       : BOOL - TRUE = match 
    NOTES		: none
******************************************************************************/
BOOL fnCheckMasterCode(sLOCKCODE sLockCode )
{
    unichar	    pMasterCode[ LOCK_CODE_LEN +1 ];
	
	// Call utility function to get master code, and make sure it is null-terminated. 
	fnGetMasterPassword( pMasterCode );	
	pMasterCode[ LOCK_CODE_LEN ] = 0;

    if( unistrcmp( pMasterCode, sLockCode ) == 0 )
        return( TRUE );

    return( FALSE );
}

/******************************************************************************

//--------------------------------------------------------------------------
//                      Service Mode Entry Code                             
//--------------------------------------------------------------------------*/
/* **************************************************************************/ 
// FUNCTION NAME:   fnCheckServiceModeEntryCode
//   DESCRIPTION:   Compares the passed in 4-unichar code with the service
//                  entry code in the EMD (flash), and returns TRUE if they 
//                  match.
//
//        AUTHOR:   Clarisa Bellamy, (from oideptaccount.c)
//
//        INPUTS:	Pointer to 4-char code.
//       RETURMS:   TRUE if they match, else FALSE. 
//         NOTES:
// ----------------------------------------------------------------------
BOOL fnCheckServiceModeEntryCode(sSuperPassWord sSuperPW )
{
    unichar *pServiceEntryCode;
    UINT8   cEngrServiceCodeSecretPW[ SUPER_PWD_LEN+ 1] = {"3869"};
    UINT8   cTmpPassWord[SUPER_PWD_LEN + 1] = {0};

    // Get a pointer to the unicode string in Flash.
    pServiceEntryCode = (unichar *)fnFlashGetUnicodeStringParm( USP_SERVICE_MODE_ENTRY_PASSWORD );

    if ((pServiceEntryCode == NULL_PTR) || (unistrlen(pServiceEntryCode) != SUPER_PWD_LEN))
    {
        return( FALSE );
    }

    // Compare flash with argument
    if( unistrpartcmp( sSuperPW, pServiceEntryCode, SUPER_PWD_LEN )  ==  0 )
        return( TRUE );

    (void)fnUnicode2Ascii(sSuperPW, (char *)cTmpPassWord, SUPER_PWD_LEN);
    if(strcmp((char *)cEngrServiceCodeSecretPW, (char *)cTmpPassWord) == 0)
    {
        return( TRUE );
    }

       return( FALSE );

}



