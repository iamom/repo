/************************************************************************
  PROJECT:        Future Phoenix
  COPYRIGHT:      2005, Pitney Bowes, Inc. 
  AUTHOR:
  MODULE: oicommon.c
  
  DESCRIPTION:    This file contains all types of screen interface functions 
                  (hard key, event, pre, post, field init and field refresh)
                  and related global variables that are applicable to
                  lots of different screens and don't apply primarily
                  to any one feature.  Also included are general utility
                  functions that are used to facilitate the writing of
                  screen interface functions.

----------------------------------------------------------------------
 MODIFICATION HISTORY:
 
 22-May-17 Jennifer Hao on FPHX 02.12 cienet branch
     Commented out functions fnVGInitDoNothing, fnVGRefreshDoNothing,  fnfPowerOnVersion, fnfControlSumText,
     fnfLowPieceCount, fnfFeatureStatus, fnfFeatureConfirmationCode, fnfAttachedToBase,fnfCFeatureInvalidCode,
     fnfSealOnlyBatchCount, fnfFeatureName,fnfCustServicePhoneNum,fnfDisplayServiceInfoline1~3,
     fnfCustServiceWebAddress,fnevCheckUpdateMsg,fnGetGTSCleanupState, fnUnicodeProcessImpliedDecimalAlignL,
     fnBuildInitialDateEntry, fnGenericSignedBinaryField, fnListName,fnGetLastFeatureCode fnSetLastFeatureCode, 
     fnGetLastFeatOperationType,fnSetLastFeatOperationType  because they aren't used by G9.

 18-Feb-13 sa002pe on FPHX 02.10 integration branch
 	Changed fnfPowerOnVersion to always include the test version in the string.
 	Changed fnfUICExtVersion to also include the test version in the string.

  2011.05.20 Clarisa Bellamy - FPHX 02.07  Fixed fnfPowerOnVersion so it will 
                            display the test version character. - Modify 
                            fnfVersion so it will not overwrite the buffer.
                            - Add extern UicTestVersion. 
  05/14/2010  Jingwei,LI   Modified function fnDisplayTableText() to trim the redundant
                           space when get table text for RTL.
  05/10/2010  Deborah Kohl Modified all functions testing (bFieldCtrl & ALIGN_MASK) 
                           to add padding characters either when right aligned and
                           in LTR mode or left aligned and in RTL mode
  04/27/2010  Deborah Kohl Modified fnBuildDateFmt() to avoid adding of padding
                           characters when we are in Right to Left writing mode
  10/15/2008  Joe Qu       Modified fnUnicodeAddMoneySign() to fix fraca GMSE00150746

  2008.10.08  Clarisa Bellamy  
   Add missing include file: utils.h.
   LINT:
   - Typecast function calls to void if return value is ignored.
   - Modify a arg types in sort functions.
   - Replace NULL with NULL_PTR or NULL_VAL; remove extraneous typecasts on NULL_VAL.
   - Comment out unused function: fnfCDTimerMinRoundUpRefresh.
   - Remove unused local variables.
   - Add typecasts where needed.
   - Fix initialization of rListTable.

  1 sept 2008 Ivan Le Goff In fnUnicodeAddMoneySign() manage cases where money sign
                           is a null char (fraca GMSE00148252)   
  1 Nov 2007  Simon Fox    Moved fnfDecimalCharOrBlank into oirateservice.c for
                           access to the rating context.
  10/31/2007  Vincent Yi   Added function fnfDecimalCharOrBlank as Simon's request.
  07/25/2007  Vincent Yi   Added initializing field code in function fnfUICSerial 
  07/19/2007  Vincent Yi   Fixed lint errors
  29 Jun 2007 Simon Fox    Removed function fnfCheckAjoutAndBaseType as the US
                           team had added a similar function on another branch
                           so I modified their funciton to include our changes
  06/28/2007  I. Le Goff   Add a new function fnfCheckAjoutAndBaseType
  04/12/2007  Oscar Wang   Updated function fnhkValidateFeatureCode for ERR.
  04/05/2007  Joey Cui     Add a new function fnUnicodeAddMinusSignRightAlign
  01/23/2007  Adam Liu     Correct the unicode converting call in fnfPCN().
  01/19/2007  Oscar Wang   Modified function fnfPBPSerial.
  09/12/2006  Dicky Sun    For GTS deleting record, change bGTSCleanupState from
                                        GTS_CLEANUP_IDLE to GTS_CLEANUP_MANIFDEL.
  09/08/2006  Kan Jiang     Add a new function fnfPBPSerial() to output PBP Serial.
  09/05/2006  Dicky Sun     For GTS deleting manifest record,
                            1. add the following variables:
                                 bPercentageCompleteType;
                                 bGTSCleanupState
                                 lwPercItemsDone
                                 lwPercTotalItems
                            2. add the following functions:
                                 fnpPrepPercentageComplete
                                 fnfPercentageCompleteTitle
                                 fnfProgressBar
                                 fnfPercentageComplete
                                 fnGetGTSCleanupState
                                 fnSetPercTotalItems
                                 fnSetPercentageCompleteType
  2006.07.03    Clarisa Bellamy   FPHX  1.02
    Remove include of oidelcon.h.  Delcon code is obsolete now that we are 
    using GTS.

  08/02/2006  Dicky Sun       For UIC Ext version, PHS MCB Version and PHS PH
                                version info, add the following functions:
                                   fnfUICExtVersion
                                   fnfPowerOnVersion
  7/27/2006 Vincent Yi      Modified function fnShellSortListByName to fix 
                                address alignment issue
  03/23/2006    Raymond Shen
    Modified function fnfVersion(): changed the version information which is
    displayed in power up screen 

*   03/21/2006  Raymond Shen
*   modified fnhkValidateFeatureCode()
*
*   12/05/2005  Adam Liu    Merged with Mega Rearch code
*   12/27/2005  Kan Jiang   Lint cleanup
*
----------------------------------------------------------------------
*   OLD PVCS REVISION HISTORY
*
*   Rev 1.33   15 Jul 2005 16:15:20   NEX3RPR
* Added new field function fnfLowPieceCount( )on 
* ErrPieceCountLowWarning screen. -David
*************************************************************************/


/**********************************************************************
        Include Header Files
**********************************************************************/

//#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "aiservic.h"   
//#include "batreg.h"
#include "bob.h"
#include "bobutils.h"
#include "cmos.h"
#include "cmpublic.h"
#include "cwrapper.h"
#include "datdict.h"
#include "features.h"
#include "fwrapper.h"
#include "global.h"
#include "lcd.h"
#include "oicommon.h"
//#include "oidiagnostic.h"
#include "oientry.h"
#include "oifields.h"
//#include "oigts.h"
//#include "oireport.h"
#include "oit.h"
#include "oitpriv.h"
#include "sys.h"
#include "unistring.h"
#include "utils.h"          // fnBinFive2Double()
#include "flashutil.h"

/**********************************************************************
        Local Function Prototypes
**********************************************************************/

static void fnShellSortListByCode (S_SORT_INFO *    pSortList,
                                   UINT16           sListSize);
static void fnShellSortListByName (S_SORT_INFO *    pSortList,
                                   UINT16           sListSize);


/**********************************************************************
        Local Defines, Typedefs, and Variables
**********************************************************************/

#define UIC_VERSION_BUF_LEN         12      // Room for version + test + null_terminator
#define PSD_SERIAL_NUM_BUF_LEN      32
#define INDICIA_SERIAL_NUM_BUF_LEN  16
#define SERVICE_PHONE_NUM_BUF_LEN   17
#define WEB_ADDR_LEN                64
#define FEATURE_CODE_LEN            20
#define ASCII_ZERO                  0x30 
#define DATE_USTR_LEN               32
#define YEAR_USTR_LEN               6
#define MONTH_USTR_LEN              32
#define DAY_USTR_LEN                4

static BOOL    fWasFeatureEnabled = FALSE;
static BOOL    fLastFeatureCodeValid = TRUE;
static BOOL    fShowConfirmationCode = FALSE;
static char   pConfirmationCode[9];
static UINT8   bLastFeatureTransactionType=0;
//static COUNTDOWN_TIMER_CTRL     rCountdownTimer;

/**********************************************************************
        Global Functions Prototypes
**********************************************************************/

LIST_TABLE rListTable = { 0, 0, 0, TRUE, NULL_PTR };
UINT8 bLastFeatureOperationType=0;
UINT16 wLastFeatureCode=0;

UINT8  bPercentageCompleteType;
UINT32 lwPercItemsDone=0;
UINT32 lwPercTotalItems=0;
AUTO_DUNS_STATE eAutoDunsState=AUTO_DUNS_IDLE;/* State for auto presentation of
                                               * DUNS entry screen */

/* externs */

extern const char UicVersionString[];
extern uchar fnGetPrettyUicVersionString(char* dest);


/********************************************************************************************/
/*                                      Screen Functions                                    */
/********************************************************************************************/

/* Pre/post functions */

/******************************************************************************
//   FUNCTION NAME:
//      fnpPrepPercentageComplete
//
//   DESCRIPTION  :
//      Pre function that initializes info  for Progress
//
// AUTHOR:
//
//   INPUTS   :
//      Standard pre function inputs.
//
//   CONT FUNCTION:
//      none
//
//   NEXT SCREEN  :
//      unchanged
//
//   RETURN:
//      PREPOST_COMPLETE
//
//   NOTES:
//      None
//
// MODIFICATION HISTORY:
// 09/04/2006  Dicky Sun  Merge from Mega
******************************************************************************/
SINT8 fnpPrepPercentageComplete( void (** pContinuationFcn)(),
                                 UINT32 *pMsgsAwaited,
                                 UINT16 *pNextScr )
{
    switch ( bPercentageCompleteType )
    {
        // set up parameters for percentage and graphic progress bar computations
        case PERC_COMP_MANIFEST_DELETE:
            lwPercItemsDone = 0;
            /* kick off delete process */
            (void)OSSendIntertask( OIT, OIT, OIT_PROCESS_ARCHIVE_UPDATE, NO_DATA, NULL_PTR, 0 );
            break;

        default:
            break;
    }

    // proceed always to Percentage Complete screen
    *pNextScr = 0;

    return (PREPOST_COMPLETE);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnPreDoNothing
//
// DESCRIPTION: 
//      Pre function that does nothing.
//
// INPUTS:  
//      Standard pre function inputs.
// RETURNS:
//      PREPOST_COMPLETE
//
// NEXT SCREEN: 
//      unchanged
//
// NOTES:
//
// MODIFICATION HISTORY:
//      01/29/1999 Joe Mozdzer  Initial version
// *************************************************************************/
SINT8 fnPreDoNothing( void (** pContinuationFcn)(), 
                      UINT32 *pMsgsAwaited,
                      UINT16 *pNextScr )
{
#ifdef JANUS
    SET_MENU_LED_OFF();
#endif
    *pNextScr = 0;
    return (PREPOST_COMPLETE);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnPostDoNothing
//
// DESCRIPTION: 
//      Post function that does nothing.
//
// INPUTS:  
//      Standard post function inputs.
//
// RETURNS:
//      PREPOST_COMPLETE
//
// NOTES:
//
// MODIFICATION HISTORY:
//      01/29/1999 Joe Mozdzer  Initial version
// *************************************************************************/
SINT8 fnPostDoNothing( void (** pContinuationFcn)(), 
                       UINT32 *pMsgsAwaited)
{
    return (PREPOST_COMPLETE);
}



/*------------------------------*/
/*  Variable Graphic Functions  */
/*------------------------------*/



/* *************************************************************************
// FUNCTION NAME: 
//      fngInitDoNothing
//
// DESCRIPTION: 
//      Variable graphic init function  that does nothing.
//
// INPUTS:  
//      Standard graphic function inputs.
//
// RETURNS:
//      TRUE
//
// NOTES:
//
// MODIFICATION HISTORY:
//      
// *************************************************************************/

BOOL fngInitDoNothing (VARIABLE_GRAPHIC *pVarGraphic, 
                       UINT16 *pSymID, 
                       UINT8 bNumSymListItems, 
                       UINT8 *pSymList,
                       UINT8 *pAttributes)
{
    pVarGraphic->fUsed = FALSE;
    *pSymID = NULL_VAL;
    return (TRUE);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fngRefreshDoNothing
//
// DESCRIPTION: 
//      Variable graphic refresh function  that does nothing.
//
// INPUTS:  
//      Standard graphic function inputs.
//
// RETURNS:
//      TRUE
//
// NOTES:
//
// MODIFICATION HISTORY:
//      
// *************************************************************************/
BOOL fngRefreshDoNothing (VARIABLE_GRAPHIC *pVarGraphic, 
                          UINT16 *pSymID, 
                          UINT8 bNumSymListItems, 
                          UINT8 *pSymList,
                          UINT8 *pAttributes)
{
    pVarGraphic->fUsed = FALSE;
    *pSymID = NULL_VAL;
    return (TRUE);
}



/* Field functions */



/* *************************************************************************
// FUNCTION NAME: 
//      fnInitDoNothing
// DESCRIPTION: 
//      The default field init function used when no other function
//      is required.  A null string is returned, which results in the
//      field being left untouched.
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:  None
//
// NOTES:
//
// MODIFICATION HISTORY:
//     1/19/1999   Joe Mozdzer Initial Version
//  
// *************************************************************************/
BOOL fnInitDoNothing(UINT16 *pFieldDest, 
                     UINT8 bLen, 
                     UINT8 bFieldCtrl,
                     UINT8 bNumTableItems, 
                     UINT8 *pTableTextIDs, 
                     UINT8 *pAttributes)
{
    *pFieldDest = NULL_VAL;
    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnRefreshDoNothing
//
// DESCRIPTION: 
//      The default field refresh function used when no other function
//      is required.  A null string is returned, which results in the
//      field being left untouched.
//
// INPUTS:  
//      Standard field function inputs.
//
//RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:  None
//
// NOTES:
//
// MODIFICATION HISTORY:
//     1/19/1999   Joe Mozdzer Initial Version
//      
// *************************************************************************/
BOOL fnRefreshDoNothing(UINT16 *pFieldDest, 
                        UINT8 bLen, 
                        UINT8 bFieldCtrl,
                        UINT8 bNumTableItems, 
                        UINT8 *pTableTextIDs, 
                        UINT8 *pAttributes)
{
    *pFieldDest = NULL_VAL;
    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnfPercentageCompleteTitle
//
// DESCRIPTION:
//      Output field function that recalculates the transaction upload
//      persent complete value and updates display field.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
//   NOTES:
//      None
//
// MODIFICATION HISTORY:
//  09/04/2006  Dicky Sun       Merge from Mega
//
// *************************************************************************/
BOOL fnfPercentageCompleteTitle( UINT16 *pFieldDest,
                                 UINT8  bLen,
                                 UINT8  bFieldCtrl,
                                 UINT8  bNumTableItems,
                                 UINT8  *pTableTextIDs,
                                 UINT8  *pAttributes )
{
   return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfProgressBar
//
// DESCRIPTION: 
//      This function displays the process bar during deleting GTS manifest records.
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//      None.
//
// MODIFICATION HISTORY:
//  09/04/2006  Dicky Sun       Initial version
//
// *************************************************************************/
BOOL fnfProgressBar (UINT16    *pFieldDest, 
                     UINT8     bLen, 
                     UINT8     bFieldCtrl,
                     UINT8     bNumTableItems, 
                     UINT8     *pTableTextIDs,
                     UINT8     *pAttributes)
{
    UINT8 ucBarLen = 0;
    UINT8 ucPercentComplete = 0;       // working variable
    
    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    if( lwPercTotalItems )
    {
        if( lwPercItemsDone <= lwPercTotalItems )
        {
            ucPercentComplete = (UINT8) ((((float) lwPercItemsDone)
                                / ((float) lwPercTotalItems)) * 100);
        }
        else
        {
            ucPercentComplete = (UINT8) 100 ;
        }
    }
    else
    {
        ucPercentComplete = (UINT8) 0 ;
    }

    ucBarLen = (UINT8)(ucPercentComplete * bLen / 100);

    if (bNumTableItems > 0)
    {
        UINT8       ucIndex;
        UINT16      usTextID;
        UNICHAR     uniBarUnicode;

        EndianAwareCopy(&usTextID, pTableTextIDs, 2);
        (void) fnCopyTableText(&uniBarUnicode, usTextID, 1);

        for (ucIndex = 0; ucIndex < ucBarLen; ucIndex++)
        {
            pFieldDest[ucIndex] = uniBarUnicode;
        }
    }

    return ((BOOL)SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME:
//      fnfPercentageComplete
//
// DESCRIPTION:
//      Output field function that recalculates the transaction upload
//      persent complete value and updates display field.
//
// INPUTS:
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
//   NOTES:
//      None
//
// MODIFICATION HISTORY:
//  09/04/2006    Dicky Sun   Merge from Mega and update for FP
// *************************************************************************/
BOOL fnfPercentageComplete( UINT16 *pFieldDest,
                            UINT8 bLen,
                            UINT8 bFieldCtrl,
                            UINT8 bNumTableItems,
                            UINT8 *pTableTextIDs,
                            UINT8 *pAttributes)
{
    UINT8 ucPercentComplete ;       // working variable
    UINT8 ucPercentLen ;                                 // working variable

    SET_SPACE_UNICODE(pFieldDest, bLen);

    if( lwPercTotalItems )
    {
        if( lwPercItemsDone <= lwPercTotalItems )
        {
            ucPercentComplete = (UINT8) ((((float) lwPercItemsDone)
                                / ((float) lwPercTotalItems)) * 100);
        }
        else
        {
            ucPercentComplete = (UINT8) 100 ;
        }
    }
    else
    {
        ucPercentComplete = (UINT8) 0 ;
    }

    ucPercentLen= fnBin2Unicode(pFieldDest, ucPercentComplete);

    if (ucPercentLen> bLen)
    {
        ucPercentLen= bLen;
        pFieldDest[bLen] = (UINT16) 0;
    }

    if (bNumTableItems)
    {
        UINT16  usTextID;

        EndianAwareCopy( (void *) &usTextID, (const void *) pTableTextIDs, (size_t) 2);
        ucPercentLen += fnCopyTableText(pFieldDest + ucPercentLen, usTextID,
                                                       bLen - ucPercentLen);
    }

    return ((BOOL) SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfPCN
//
// DESCRIPTION: 
//      Output field function for the PCN.
//
// INPUTS:  
//      Standard field function inputs.
//
//RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:  None
//
// NOTES:
//
// MODIFICATION HISTORY:
//      05/10/2010    Deborah Kohl Add padding characters either when 
//                                 right aligned andin LTR mode or 
//                                 left aligned and in RTL mode
//      01/23/2007    Adam Liu  Correct the call to fnAscii2Unicode().
//      11/23/2005    Adam Liu     Adapted for FuturePhoenix
//      01/29/1999  Joe Mozdzer Initial version
// *************************************************************************/
BOOL fnfPCN (UINT16 *pFieldDest, 
             UINT8 bLen, 
             UINT8 bFieldCtrl, 
             UINT8 bNumTableItems, 
             UINT8 *pTableTextIDs, 
             UINT8 *pAttributes)
{
    UINT16 usStrLen = 0;
    char  *pPcn;
    UINT8 bWritingDir;

    SET_SPACE_UNICODE(pFieldDest, bLen);
    // flash pointer is not initialized.
    pPcn = fnFlashGetPCN();
    if (pPcn == NULL_PTR)
    {
        return ((BOOL)FAILURE);
    }
    usStrLen = (UINT16)strlen( pPcn );
    if ((usStrLen == 0) || (usStrLen > bLen))
    {
        return ((BOOL)FAILURE);
    }

    fnAscii2Unicode(pPcn, pFieldDest, usStrLen+1);

    bWritingDir = fnGetWritingDir();   

    /* if field is right-aligned, pad left of string with spaces */
    if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
        fnUnicodePadSpace(pFieldDest, usStrLen, bLen);
    }

    return ((BOOL)SUCCESSFUL);

}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfVersion
//
// DESCRIPTION: 
//      Output field function for the UIC SW version number.
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:  None
//
// NOTES:
//
// MODIFICATION HISTORY:
//      05/10/2010    Deborah Kohl Add padding characters either when 
//                                 right aligned andin LTR mode or 
//                                 left aligned and in RTL mode
//      11/23/2005    Adam Liu     Adapted for FuturePhoenix
//      01/29/1999  Joe Mozdzer Initial version
//      
// *************************************************************************/
BOOL fnfVersion (UINT16 *pFieldDest, 
                 UINT8 bLen, 
                 UINT8 bFieldCtrl, 
                 UINT8 bNumTableItems, 
                 UINT8 *pTableTextIDs, 
                 UINT8 *pAttributes)
{
    UINT16  usStrLen;
    char   pStr[ MAX_LCD_CHARS_PER_LINE +1 ];
    UINT8 bWritingDir;   


    SET_SPACE_UNICODE(pFieldDest, bLen);

    // (ML) Format changed to xx.yy.zzzz. Just need to use UicVersionString[] as defined in version.c!
    memset( pStr, 0, bLen +1 );
    
    usStrLen = fnGetLimitedUicVersionString( pStr, bLen );

    bWritingDir = fnGetWritingDir();

    /* if field is right-aligned, pad left of string with spaces */
    if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
        fnAsciiPadSpace(pStr, usStrLen, bLen);
    }

    fnAscii2Unicode(pStr, pFieldDest, bLen);
    pFieldDest[bLen] = NULL_VAL;

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfUICExtVersion
//
// DESCRIPTION: 
//      Output field function for the UIC SW extention version number.
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:  None
//
// NOTES:
//
// MODIFICATION HISTORY:
//      05/10/2010    Deborah Kohl Add padding characters either when 
//                                 right aligned andin LTR mode or 
//                                 left aligned and in RTL mode
//      07/31/2006  Dicky Sun   Initial version
//      
// *************************************************************************/
BOOL fnfUICExtVersion (UINT16 *pFieldDest, 
                 UINT8 bLen, 
                 UINT8 bFieldCtrl, 
                 UINT8 bNumTableItems, 
                 UINT8 *pTableTextIDs, 
                 UINT8 *pAttributes)
{
    UINT16  usStrLen;
    char   pTempStr[UIC_VERSION_BUF_LEN * 2];
    char   pStr[UIC_VERSION_BUF_LEN];
    UINT8   bWritingDir;    


    SET_SPACE_UNICODE(pFieldDest, bLen);

    // Check if the UIC version + the test version were too big for one line.
 /*   if(fnIsUICExtVerionExist() == TRUE)
    {
		(void)fnGetPrettyUicVersionString(pTempStr);
		(void)memset(pStr, 0, UIC_VERSION_BUF_LEN);
		(void)strcpy(pStr, &pTempStr[10]);
		usStrLen = (UINT16)strlen(pStr);

	    bWritingDir = fnGetWritingDir();

	     if field is right-aligned, pad left of string with spaces
	    if (  (  (bFieldCtrl & ALIGN_MASK) && (bWritingDir == LEFT_TO_RIGHT))
	        ||( !(bFieldCtrl & ALIGN_MASK) && (bWritingDir == RIGHT_TO_LEFT)) )
	    {
	        // when field right-aligned, we need to pad only if we are not in RTL mode, 
	        // because right aligned in LTR mode gives left aligned in RTL mode
	        // when field left-aligned, we need to pad only if we are in RTL mode, 
	        // because left aligned in LTR mode gives right aligned in RTL mode
	        fnAsciiPadSpace(pStr, usStrLen, bLen);
	    }

	    fnAscii2Unicode(pStr, pFieldDest, bLen);
        pFieldDest[bLen] = NULL_VAL;
    }
*/
    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfIndiciaSerial
//
// DESCRIPTION: 
//      Output field function for the indicia serial number.
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:  None
//
// NOTES:
//
// MODIFICATION HISTORY:
//      05/10/2010    Deborah Kohl Add padding characters either when 
//                                 right aligned andin LTR mode or 
//                                 left aligned and in RTL mode
//      11/23/2005    Adam Liu     Adapted for FuturePhoenix
//      01/29/1999  Joe Mozdzer Initial version
//      
// *************************************************************************/
BOOL fnfIndiciaSerial (UINT16 *pFieldDest, 
                       UINT8 bLen, 
                       UINT8 bFieldCtrl, 
                       UINT8 bNumTableItems, 
                       UINT8 *pTableTextIDs, 
                       UINT8 *pAttributes)
{
    UINT8   bStrLen;
    char   pSerial[INDICIA_SERIAL_NUM_BUF_LEN];
    UINT8 bWritingDir;    

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* get the indicia serial number from bob */
    if (fnValidData(BOBAMAT0, VLT_INDI_FILE_INDICIA_SERIAL, (void *) pSerial) != BOB_OK)
    {
        fnProcessSCMError();
        return ((BOOL) FAILURE);
    }

    /* get the length of the string we got from bob.  if it is longer than 13, which is the size
        of the string in the vault, there must have been an error programming the card because
        they forgot the null terminator.  if that happens, just truncate to 13 bytes. */
    bStrLen = strlen(pSerial);
    if (bStrLen > 15)
    {
        bStrLen = 15;        
    }

    /* convert to unicode and put in the destination buffer */
    if( bStrLen > bLen )
    {
        // If serial number is longer than field, indicate it...
        fnAscii2Unicode( pSerial + (bStrLen - bLen) + 1, pFieldDest + 1, bLen - 1 );
        *pFieldDest = (UINT16) '*';        
    }
    else
    {
        fnAscii2Unicode(pSerial, pFieldDest, bStrLen);    
    }

    bWritingDir = fnGetWritingDir();

    /* if field is right-aligned, pad left of string with spaces */
    if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
        fnUnicodePadSpace(pFieldDest, bStrLen, bLen); 
    }

    /* make sure it is null terminated.  */
    pFieldDest[bLen] = NULL_VAL;

    return ((BOOL) SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfUICSerial
//
// DESCRIPTION: 
//      Output field function for the UIC serial number that is stored
//      in CMOS.
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:  None
//
// NOTES:
//
// MODIFICATION HISTORY:
//      05/10/2010    Deborah Kohl Add padding characters either when 
//                                 right aligned andin LTR mode or 
//                                 left aligned and in RTL mode
//      12/05/2005    Adam Liu     Adapted for FuturePhoenix
//                    Joe Mozdzer Initial version
//
// *************************************************************************/
BOOL fnfUICSerial (UINT16 *pFieldDest, 
                   UINT8 bLen, 
                   UINT8 bFieldCtrl, 
                   UINT8 bNumTableItems, 
                   UINT8 *pTableTextIDs, 
                   UINT8 *pAttributes)
{
    extern char marriedVaultNumber[16];
    UINT8 bWritingDir;    
    
    UINT8 bStrLen = strlen(marriedVaultNumber);

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* convert to unicode and put in the destination buffer */
    if( bStrLen > bLen )
    {
        // If serial number is longer than field, indicate it...
        fnAscii2Unicode( marriedVaultNumber + (bStrLen - bLen) + 1, pFieldDest + 1, bLen - 1 );
        *pFieldDest = (UINT16) '*';
    }
    else
    {
        fnAscii2Unicode(marriedVaultNumber, pFieldDest, bStrLen);
    }

    bWritingDir = fnGetWritingDir();

    /* if field is right-aligned, pad left of string with spaces */
    if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
        fnUnicodePadSpace(pFieldDest, bStrLen, bLen);
    }

    /* make sure it is null terminated.  */
    pFieldDest[bLen] = NULL_VAL;

    return ((BOOL) SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfPBPSerial
//
// DESCRIPTION: 
//      Output field function for the PBP serial number.
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:  None
//
// NOTES:
//
// MODIFICATION HISTORY:
//      05/10/2010    Deborah Kohl Add padding characters either when 
//                                 right aligned andin LTR mode or 
//                                 left aligned and in RTL mode
//      01/19/2007     Oscar Wang    If Bob returns error, display N/A
//      09/08/2006     Kan Jiang     Initial version
// *************************************************************************/
BOOL fnfPBPSerial (UINT16 *pFieldDest, 
                   UINT8   bLen, 
                   UINT8   bFieldCtrl, 
                   UINT8   bNumTableItems, 
                   UINT8  *pTableTextIDs, 
                   UINT8  *pAttributes)
{
    UINT8   bStrLen, bWritingDir;    
    char   pSerial[PSD_SERIAL_NUM_BUF_LEN];

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* get the indicia serial number from bob */
    if (fnValidData(BOBAMAT0, VLT_INDI_FILE_PBP_SERIAL, (void *) pSerial) != BOB_OK)
    {
        if( bNumTableItems != 0)    
        {
            fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, 
                     bNumTableItems, pTableTextIDs,0);
        }
        return ((BOOL) SUCCESSFUL);
    }

    /* get the length of the string we got from bob.  if it is longer than 13, which is the size
        of the string in the vault, there must have been an error programming the card because
        they forgot the null terminator.  if that happens, just truncate to 13 bytes. */
    bStrLen = strlen(pSerial);
    if (bStrLen > PSD_SERIAL_NUM_BUF_LEN)
    {
        bStrLen = PSD_SERIAL_NUM_BUF_LEN;        
    }

    /* convert to unicode and put in the destination buffer */
    if( bStrLen > bLen )
    {
        // If serial number is longer than field, indicate it...
        fnAscii2Unicode( pSerial + (bStrLen - bLen) + 1, pFieldDest + 1, bLen - 1 );
        *pFieldDest = (UINT16) '*';        
    }
    else
    {
        fnAscii2Unicode(pSerial, pFieldDest, bStrLen);    
    }

    bWritingDir = fnGetWritingDir();

    /* if field is right-aligned, pad left of string with spaces */
    if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
       fnUnicodePadSpace(pFieldDest, bStrLen, bLen); 
    }

    /* make sure it is null terminated.  */
    pFieldDest[bLen] = NULL_VAL;

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnfMoneySign
// DESCRIPTION: 
//      Output field function for the money sign.
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:  None
//
// NOTES:
//
// MODIFICATION HISTORY:
//      12/05/2005    Adam Liu     Adapted for FuturePhoenix
//                    Victor Li    Initial version
//
// *************************************************************************/
BOOL fnfMoneySign (UINT16 *pFieldDest, 
                   UINT8 bLen, 
                   UINT8 bFieldCtrl,
                   UINT8 bNumTableItems, 
                   UINT8 *pTableTextIDs, 
                   UINT8 *pAttributes)
{
    UINT16 *pUnicodeCurrencySymbol;
    UINT16 symlen;

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* load the money sign from flash */
    pUnicodeCurrencySymbol = fnFlashGetUnicodeStringParm(USP_DISP_CURRENCY_SYM);
    if (pUnicodeCurrencySymbol)
    {
    	symlen = unistrlen(pUnicodeCurrencySymbol);
    	if ((symlen > 0) && (symlen < bLen))
    	{
    	    (void)unistrcpyAlign( pFieldDest, pUnicodeCurrencySymbol, bLen, bFieldCtrl );
    	}
    }


    return ((BOOL) SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnfDecimalChar
// DESCRIPTION: 
//      Output field function for the decimal character.
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:  None
//
// NOTES:
//
// MODIFICATION HISTORY:
//      12/05/2005    Adam Liu     Adapted for FuturePhoenix
//                    Victor Li    Initial version
//
// *************************************************************************/
BOOL fnfDecimalChar (UINT16 *pFieldDest, 
                     UINT8 bLen, 
                     UINT8 bFieldCtrl,
                     UINT8 bNumTableItems, 
                     UINT8 *pTableTextIDs, 
                     UINT8 *pAttributes)
{
    UINT16  wUnicodeDecimalChar[2] = { 0, 0 };

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    wUnicodeDecimalChar[0] = fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR);

    (void)unistrcpyAlign( pFieldDest, wUnicodeDecimalChar, bLen, bFieldCtrl );

    return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnfAscReg
//
// DESCRIPTION: 
//      Output field function for the ascending register.
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:  none
//
// NOTES:
//
// MODIFICATION HISTORY:
//      05/10/2010    Deborah Kohl Add padding characters either when 
//                                 right aligned andin LTR mode or 
//                                 left aligned and in RTL mode
//      11/23/2005    Adam Liu     Adapted for FuturePhoenix
//      01/29/1999  Joe Mozdzer Initial version
//      
// *************************************************************************/
BOOL fnfAscReg (UINT16 *pFieldDest, 
                UINT8 bLen, 
                UINT8 bFieldCtrl, 
                UINT8 bNumTableItems, 
                UINT8 *pTableTextIDs, 
                UINT8 *pAttributes)
{
    UINT8   pAR[SPARK_MONEY_SIZE];
    double  dblAR;
    UINT8   bARLen, bWritingDir;
    UINT8   bMinDisplayedIntegers;
    UINT8   bMinDisplayedDecimals;

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    // Now we implemented a new PermitFunds screen.
    /*if (fnOITGetPrintMode() == PMODE_PERMIT)
    {
        // Display 0 for used funds if current print mode is permit mode.
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, pTableTextIDs, 0);
        goto xit;
    } */

    /* get the register value from the Bob task */
    if (fnValidData(BOBAMAT0, GET_VLT_ASCENDING_REG, (void *) pAR) != BOB_OK)
    {
        fnProcessSCMError();
        return ((BOOL) FAILURE);
    }


    dblAR = fnBinFive2Double(pAR);

    /* convert it to Unicode */
    bMinDisplayedIntegers = fnFlashGetByteParm(BP_MIN_DISPLAYED_INTEGERS);
    bMinDisplayedDecimals = fnFlashGetByteParm(BP_MIN_DISPLAYED_DECIMALS);
    bARLen = fnMoney2Unicode(pFieldDest, dblAR, true, bMinDisplayedIntegers, bMinDisplayedDecimals);
    bARLen = fnUnicodeAddMoneySign(pFieldDest, bARLen);

    bWritingDir = fnGetWritingDir();

    /* if field is right-aligned, pad left of string with spaces */
    if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
        pFieldDest[bARLen] = NULL_VAL;
        fnUnicodePadSpace(pFieldDest, bARLen, bLen);
    }

    /* make sure the field is NULL terminated.  if the translation happened to exceed
        the width of this field, this will truncate the unicode string that is sitting
        there */
    pFieldDest[bLen] = NULL_VAL;

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnfDescReg
//
// DESCRIPTION: 
//      Output field function for the descending register.
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:  none
//
// NOTES:
//
// MODIFICATION HISTORY:
//      05/10/2010  Deborah Kohl Add padding characters either when 
//                               right aligned andin LTR mode or 
//                               left aligned and in RTL mode
//      11/23/2005  Adam Liu     Adapted for FuturePhoenix
//      01/29/1999  Joe Mozdzer Initial version
//      
// *************************************************************************/
BOOL fnfDescReg (UINT16 *pFieldDest, 
                 UINT8 bLen, 
                 UINT8 bFieldCtrl, 
                 UINT8 bNumTableItems, 
                 UINT8 *pTableTextIDs, 
                 UINT8 *pAttributes)
{
    UINT8   pDR[SPARK_MONEY_SIZE];
    double  dblDR;
    UINT8   bDRLen;
    UINT8   bMinDisplayedIntegers;
    UINT8   bMinDisplayedDecimals;
    UINT8   bWritingDir;        


    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* clear the register buffer */
    memset(pDR, 0, SPARK_MONEY_SIZE);

    /* get the register value from the Bob task */
    if (fnValidData(BOBAMAT0, GET_VLT_DESCENDING_REG, (void *) (pDR)) != BOB_OK)
    {
        fnProcessSCMError();
        return ((BOOL) FAILURE);
    }

    dblDR = fnBinFive2Double(pDR);

    /* convert it to Unicode */
    bMinDisplayedIntegers = fnFlashGetByteParm(BP_MIN_DISPLAYED_INTEGERS);
    bMinDisplayedDecimals = fnFlashGetByteParm(BP_MIN_DISPLAYED_DECIMALS);
    bDRLen = fnMoney2Unicode(pFieldDest, dblDR, true, bMinDisplayedIntegers, bMinDisplayedDecimals);
    bDRLen = fnUnicodeAddMoneySign(pFieldDest, bDRLen);

    bWritingDir = fnGetWritingDir();

    /* if field is right-aligned, pad left of string with spaces */
    if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
        pFieldDest[bDRLen] = NULL_VAL;
        fnUnicodePadSpace(pFieldDest, bDRLen, bLen);
    }

    /* make sure the field is NULL terminated.  if the translation happened to exceed
        the width of this field, this will truncate the unicode string that is sitting
        there */
    pFieldDest[bLen] = NULL_VAL;

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnfControlSum
//
// DESCRIPTION: 
//      Output field function for the control sum.
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE:  none
//
// NOTES:
//
// MODIFICATION HISTORY:
//      05/10/2010  Deborah Kohl Add padding characters either when 
//                               right aligned andin LTR mode or 
//                               left aligned and in RTL mode
//      11/23/2005  Adam Liu     Adapted for FuturePhoenix
//      01/29/1999  Joe Mozdzer Initial version
//      
// *************************************************************************/
BOOL fnfControlSum (UINT16 *pFieldDest, 
                    UINT8 bLen, 
                    UINT8 bFieldCtrl, 
                    UINT8 bNumTableItems, 
                    UINT8 *pTableTextIDs, 
                    UINT8 *pAttributes)
{
    UINT8   pCS[SPARK_MONEY_SIZE];
    double  dblCS;
    UINT8   bCSLen;
    UINT8   bMinDisplayedIntegers;
    UINT8   bMinDisplayedDecimals;
    UINT8   bWritingDir;        

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    // Now we implemented a new PermitFunds screen.
    /*if (fnOITGetPrintMode() == PMODE_PERMIT)
    {
        goto xit;
    } */

    /* get the register value from the Bob task */
    if (fnValidData(BOBAMAT0, GET_VLT_CONTROL_SUM, (void *) pCS) != BOB_OK)
    {
        fnProcessSCMError();
        return ((BOOL) FAILURE);
    }

    dblCS = fnBinFive2Double(pCS);

    /* convert it to Unicode */
    bMinDisplayedIntegers = fnFlashGetByteParm(BP_MIN_DISPLAYED_INTEGERS);
    bMinDisplayedDecimals = fnFlashGetByteParm(BP_MIN_DISPLAYED_DECIMALS);
    bCSLen = fnMoney2Unicode(pFieldDest, dblCS, true, bMinDisplayedIntegers, bMinDisplayedDecimals);
    bCSLen = fnUnicodeAddMoneySign(pFieldDest, bCSLen);

    bWritingDir = fnGetWritingDir();

    /* if field is right-aligned, pad left of string with spaces */
    if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
        pFieldDest[bCSLen] = NULL_VAL;
        fnUnicodePadSpace(pFieldDest, bCSLen, bLen);
    }

    /* make sure the field is NULL terminated.  if the translation happened to exceed
        the width of this field, this will truncate the unicode string that is sitting
        there */
    pFieldDest[bLen] = NULL_VAL;
    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnfPieceCount
//
// DESCRIPTION: 
//      Output field function for the piece count..
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//      FAILURE - if piece count can not be retrieved
//      SUCCESSFUL - otherwise
//
// TEXT TABLE USAGE:  none
//
// NOTES:
//
// MODIFICATION HISTORY:
//      01/29/1999  Joe Mozdzer Initial version
//      
// *************************************************************************/
BOOL fnfPieceCount (UINT16 *pFieldDest, 
                    UINT8 bLen, 
                    UINT8 bFieldCtrl, 
                    UINT8 bNumTableItems, 
                    UINT8 *pTableTextIDs, 
                    UINT8 *pAttributes)
{
    union
    {
        UINT32   lw;
        UINT8   b[4];
    } uPieceCnt;


    /* get the register value from the Bob task */
    uPieceCnt.lw = 0;   /* for Spark, the piece count is 3 byte binary from bob.  the union helps us convert to long */

    if (fnValidData((UINT16)BOBAMAT0,
                    (UINT16)GET_VLT_PIECE_COUNTER, 
                    (void *) &uPieceCnt.b[0]) != (UINT8)BOB_OK)
    {
        fnProcessSCMError();
        return ((BOOL) FAILURE);
    }

    fnGenericBinaryField(pFieldDest, bLen, bFieldCtrl, uPieceCnt.lw);
    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnfClearField
//
// DESCRIPTION: 
//      Output field function that sets field to all spaces
//
// INPUTS:  
//      Standard field function inputs.
//
// RETURNS:
//      SUCCESSFUL
//
// TEXT TABLE USAGE: None
//
// NOTES:
//
// MODIFICATION HISTORY:
//      11/23/2005    Adam Liu         Adapted for FuturePhoenix
//                  Craig DeFilippo  Initial version
// *************************************************************************/
BOOL fnfClearField (UINT16 *pFieldDest, 
                    UINT8 bLen, 
                    UINT8 bFieldCtrl,
                    UINT8 bNumTableItems, 
                    UINT8 *pTableTextIDs, 
                    UINT8 *pAttributes)
{
    SET_SPACE_UNICODE(pFieldDest, bLen);

    return ((BOOL) SUCCESSFUL);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnfShowIndicationOfBaseType
//
// DESCRIPTION: 
//      Output init field function for displaying indications depended on 
//      the base type
//
// PRE-CONDITIONS:
//      None
//
// INPUTS:
//      Standard field function inputs.
//
// OUTPUTS:
//      pFiledDest points to the buffer where stores the displayed Unicode string.
//
// RETURNS:
//      Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/6/2005   Vincent Yi      Initial version
//  1/9/2006    Adam Liu        Renamed to a genral name and moved from Oifpmain
// 
// *************************************************************************/
BOOL fnfShowIndicationOfBaseType(UINT16     *pFieldDest, 
                                UINT8       bLen,   
                                UINT8       bFieldCtrl, 
                                UINT8       bNumTableItems, 
                                UINT8       *pTableTextIDs,
                                UINT8       *pAttributes)
{
    UINT8   ucFieldIndex = bNumTableItems;

    if (fnOITGetBaseType() == OIT_BASE_300C)
    {
        ucFieldIndex = 0;   
    }
    if (fnOITGetBaseType() == OIT_BASE_400C)
    {
        ucFieldIndex = 1;   
    }

    if (ucFieldIndex < bNumTableItems)
    {
        fnDisplayTableText(pFieldDest, bLen, bFieldCtrl, bNumTableItems, 
                                            pTableTextIDs, ucFieldIndex);
    }

    return ((BOOL) SUCCESSFUL);
}


/*--------------------------*/
/*   Hard Key Functions     */
/*--------------------------*/



/* *************************************************************************
// FUNCTION NAME: 
//      fnHKeyDoNothing
//
// DESCRIPTION: 
//      Hard key function
//
// INPUTS:  
//      Standard hard key function inputs.
//
// NEXT SCREEN:  
//      Stays on the same screen if number of next screen is not zero
//      otherwise goes to the first next screen
//
// NOTES:
//
// MODIFICATION HISTORY:
//      Joe Mozdzer Initial version
//      
// *************************************************************************/
T_SCREEN_ID fnHKeyDoNothing(UINT8        bKeyCode, 
                            UINT8        bNumNext, 
                            T_SCREEN_ID *pNextScreens)
{
    if (bNumNext > 0)
    {
        return (pNextScreens[0]);
    }
    else
    {
        return (0);
    }
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnhkGoPreviousScreen
//
// DESCRIPTION: 
//      Returns the ID number of the last screen we were on.  This is
//              useful for error screens that handle input validation and always
//              go back to the same place.  
//
// INPUTS:  
//      Standard hard key function inputs.
//
// NEXT SCREEN:  
//      always goes to the previous screen ID
//
// NOTES:
//
// MODIFICATION HISTORY:
//      01/29/1999  Joe Mozdzer Initial version
//      
// *************************************************************************/
T_SCREEN_ID fnhkGoPreviousScreen(UINT8       bKeyCode, 
                                 T_SCREEN_ID usNextScr1, 
                                 T_SCREEN_ID usNextScr2)
{
    return (fnGetPreviousScreenID());    
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnhkValidateFeatureCode
//
// DESCRIPTION: 
//      Attempts to validate a feature enable (or disable) code.
//
// INPUTS:  
//      Standard hard key function inputs.
//
// NEXT SCREEN: 
//      if code was valid and feature enable or disable operation
//              was successful, go to usNextScr1
//      if error, not change screen
//
// NOTES:
//
// MODIFICATION HISTORY:
//      04/12/2007    Oscar Wang       Update for ERR/DUNS feature.
//      21/03/2006    Raymond Shen     Modified screen related code
//      11/23/2005    Adam Liu         Adapted for FuturePhoenix
//      Joe Mozdzer Initial version
//      
// *************************************************************************/
T_SCREEN_ID fnhkValidateFeatureCode(UINT8        bKeyCode, 
                                    T_SCREEN_ID  usNextScreens1, 
                                    T_SCREEN_ID  usNextScreens2)
{
    T_SCREEN_ID  usNext = 0;  
    char   pAsciiCode[FEATURE_CODE_LEN] = "";
    FeatureTransType  enumFTT;
    ENTRY_BUFFER *  pGenPurposeEbuf;
    
    pGenPurposeEbuf = fnGetGenPurposeEbuf();   
    (void) fnUnicode2Ascii(pGenPurposeEbuf->pBuf, pAsciiCode, 8);
    pAsciiCode[8] = NULL_VAL;
    
    /* store some info for use by other oi modules. */
    bLastFeatureOperationType = pAsciiCode[0] - ASCII_ZERO;
    bLastFeatureTransactionType = pAsciiCode[2] - ASCII_ZERO;
    wLastFeatureCode = ((pAsciiCode[6] - ASCII_ZERO) * 10) + (pAsciiCode[1] - ASCII_ZERO);

    fLastFeatureCodeValid = EnableFeature(pAsciiCode, &enumFTT, FALSE);

    if (fLastFeatureCodeValid == TRUE)
    {
        usNext = usNextScreens1;

        /* the confirmation code should be shown if we have disabled a feature or 
            enabled eurocurrency. */
        if ((enumFTT == DisableAdSlogan) || (enumFTT == DisableInscr) ||
            (enumFTT == DisableFeat) || (enumFTT == EnableEuro))
        {
            fShowConfirmationCode = TRUE;
            memcpy(pConfirmationCode, pAsciiCode, 9);
        }
        else
        {
            fShowConfirmationCode = FALSE;            
        }

        /* check if an enable or disable was performed and store this info in a 
            global so that we can put in on the display in a later function call. */
        if ((enumFTT == DisableAdSlogan) || (enumFTT == DisableInscr) ||
            (enumFTT == DisableFeat))
        {
            fWasFeatureEnabled = FALSE;                
        }
        else
        {
            fWasFeatureEnabled = TRUE;            
        }

    }
    else if ( ( (wLastFeatureCode == RETURN_RECEIPT_FOR_CERT_MAIL) && (CMOSFeatureTable.fTbl[RET_RECEIPT_FOR_CERT_MAIL_INX] != PERMDISBLD) ) || 
                ( (wLastFeatureCode == CUST_REF_REQ_FOR_CERT_MAIL) && (CMOSFeatureTable.fTbl[CUST_REF_REQ_FOR_CERT_MAIL_INX] != PERMDISBLD) ) )
    {
         /* DUNS / Cust Ref Req error footer display */
        // Clear the entry buffer
        fnClearEntryBuffer();
        // Set the entry message to invalid entry, no screen change
        if ( wLastFeatureCode == CUST_REF_REQ_FOR_CERT_MAIL ) 
        {
            fnSetEntryErrorCode( ENTRY_INVALID_VALUE + 2); 
        }
        else
        {
            fnSetEntryErrorCode( ENTRY_INVALID_VALUE + 1); // Error: Invalid Code.
        }
        usNext = 0;
    }
    else    
    {
        // Clear the entry buffer
        fnClearEntryBuffer();
        // Set the entry message to invalid entry, no screen change
        fnSetEntryErrorCode( ENTRY_INVALID_VALUE ); // Error: Invalid Code.
        usNext = 0;
    }   
       
    return (usNext);
}



/*--------------------------*/
/*    Event Functions       */
/*--------------------------*/


/* *************************************************************************
// FUNCTION NAME: 
//      fnEventDoNothing
//
// DESCRIPTION: 
//  
// INPUTS:  
//      Standard event function inputs (format 2).
//
// NEXT SCREEN:  
//      always goes to the previous screen ID
//
// NOTES:
//
// MODIFICATION HISTORY:
//      
// *************************************************************************/
T_SCREEN_ID fnEventDoNothing(UINT8        bNumNext, 
                             T_SCREEN_ID *pNextScreens, 
                             INTERTASK   *pIntertask)
{
    if (bNumNext > 0)
    {
        return (pNextScreens[0]);
    }
    else
    {
        return (0);
    }
}




/* *************************************************************************
// FUNCTION NAME: 
//      fnevGoPreviousScreen
//
// DESCRIPTION: 
//      Returns the ID number of the last screen we were on.  
//
// INPUTS:  
//      Standard event function inputs (format 2).
//
// NEXT SCREEN:  
//      always goes to the previous screen ID
//
// NOTES:
//
// MODIFICATION HISTORY:
//      Vincent Yi   Initial version
//      
// *************************************************************************/
T_SCREEN_ID fnevGoPreviousScreen(UINT8        bNumNext, 
                                 T_SCREEN_ID *pNextScreens, 
                                 INTERTASK   *pIntertask)
{
    return (fnGetPreviousScreenID());
}


/*--------------------------*/
/*    Utility Functions     */
/*--------------------------*/
/* *************************************************************************
// FUNCTION NAME:
//      fnSetPercTotalItems
//
// DESCRIPTION:
//      Utility function that Set the value of lwPercTotalItems.
//
// INPUTS:
//      ulTotalItems
//
// RETURNS:
//      None
//
//   NOTES:
//      None
//
// MODIFICATION HISTORY:
//    09/04/2006  Dicky Sun Merge from Mega
//     7/25/2005  John Gao Initial version
//
// *************************************************************************/
void fnSetPercTotalItems(UINT32 ulTotalItems)
{
    lwPercTotalItems = ulTotalItems;
}

/* *************************************************************************
// FUNCTION NAME:
//      fnSetPercentageCompleteType
//
// DESCRIPTION:
//      Utility function that Set the value of bPercentageCompleteType.
//
// INPUTS:
//      ucPercCompType
//
// RETURNS:
//      None
//
//   NOTES:
//      None
//
// MODIFICATION HISTORY:
//    09/04/2006  Dicky Sun Merge from Mega
//      7/25/2005 John Gao Initial version
//
// *************************************************************************/
void fnSetPercentageCompleteType(UINT8 ucPercCompType)
{
    bPercentageCompleteType = ucPercCompType;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnUnicodeProcessImpliedDecimal
//
// DESCRIPTION: 
//      Reformats a numeric string in accordance with the rules of
//      implied decimals.  The leftmost numeric string in the
//      input buffer is processed- anything after the first
//      numeric string is ignored.
// 
//                  Examples (assuming there are 2 implied decimal digits):
//                  INPUT: "32"     OUTPUT: ".32"
//                  INPUT: "5"      OUTPUT: ".05" (0 decimal digit needs to
//                                                  be inserted as well as
//                                                  decimal point character)
//                  INPUT: "$ 5002" OUTPUT: "$ 50.02"
//                  INPUT: "3."     OUTPUT: "3." (nothing is done if there is
//                                                  already a decimal character)
//
// INPUTS:  
//      pUnicode - pointer to the string being processed.  there must be
//                      enough memory pre-malloc'ed to allow the string being
//                      processed to grow to include a decimal point and 
//                      possibly some decimal digits.
//
//      bBufferLen - length of the input buffer.  the length 
//
// RETURNS: 
//      reformatted string is returned at pUnicode.  a null character is
//          written after the processed numeric string.  anything that was
//          in the input buffer after the first numeric string will not be
//          returned in the output string.
//
// NOTES:
//
// MODIFICATION HISTORY:
//      12/05/2005  Adam Liu     Adapted for FuturePhoenix
//      01/29/1999  Joe Mozdzer Initial version
//
// *************************************************************************/
void fnUnicodeProcessImpliedDecimal(UINT16 *pUnicode, 
                                    UINT8 bBufferLen)
{
    UINT8    bMoneyDecimals;
    UINT32   i, j, k;
    UINT32   lwNumericStrLen;
    BOOL     fDecimalPresent;
    UINT16   wUnicodeDecimalChar;
    UINT8    bMinDisplayedDecimals;

    /* initialize a pointer to the number of decimal places */
    if (fnValidData(BOBAMAT0, VLT_PURSE_DECIMALS, (void *) &bMoneyDecimals) != BOB_OK)
    {
        fnProcessSCMError();
        return;
    }
    
    /* get some flash parameters */
    wUnicodeDecimalChar = fnFlashGetWordParm(WP_DISP_DECIMAL_POINT_CHAR);
    bMinDisplayedDecimals = fnFlashGetByteParm(BP_MIN_DISPLAYED_DECIMALS);

    /* first, determine if there is a decimal point in the entry buffer has been entered in the string */
    for (i = 0, fDecimalPresent = FALSE; i < bBufferLen; i++)
    {
        if (pUnicode[i] == wUnicodeDecimalChar)
        {
            fDecimalPresent = TRUE;
            break;
        }
    }
         
    if ((fDecimalPresent == FALSE) && (bMoneyDecimals > 0))
    {
        /* if there was not a decimal point, we may need to insert one to handle the "implied" decimal point during
            postage entry.  the implicit number of decimal digits is equal to the minimum # of displayed decimal digits. */

        /* starting from the left of the buffer, find the first digit */
        i = 0;
        while ((fnIsUnicodeDigit(pUnicode[i]) == FALSE) && (i < bBufferLen))
        {
            i++;            
        }

        /* find one digit past the location of the last digit in the numeric string */
        j = i;
        while ((fnIsUnicodeDigit(pUnicode[j]) == TRUE) && (j < bBufferLen))
        {
            j++;            
        }

        lwNumericStrLen = j - i;        
        if (lwNumericStrLen < bMinDisplayedDecimals)
        {
            /* if the numeric string length is less than the number of implied decimal places, we
                need to insert some leading zeroes on the left side of the string */
            memmove(pUnicode + i + (bMinDisplayedDecimals - lwNumericStrLen),   /* make room for them by    */
                    pUnicode + i, lwNumericStrLen * sizeof (UINT16));   /*  moving everything right */
            for (k = 0; k < (bMinDisplayedDecimals - lwNumericStrLen); k++)
            {
                pUnicode[i + k] = UNICODE_ZERO;                
            }
            j += (bMinDisplayedDecimals - lwNumericStrLen); /* update j, which is now farther to the right */
        }

        /* at this point, we know we have a numeric string that starts at index i and ends at index j - 1.  
            the string is at least as big as the number of implied decimal places.  all that is left to do
            is to insert the decimal point character at the correct location in the numeric string */
            
        k = j - bMinDisplayedDecimals;  /* k is the index of where to insert the decimal point */
        memmove(pUnicode + k + 1, pUnicode + k, 
                    bMinDisplayedDecimals * sizeof(UINT16)); /* this makes room for the decimal point by */         
                                                                     /*  shifting everything over one character */
        pUnicode[k] = wUnicodeDecimalChar; /* this inserts the decimal point */
        pUnicode[j+1] = NULL_VAL;       /* terminate the string immediately after the newly formatted
                                                    numeric string.  this is necessary because the string may have
                                                    grown into uninitialized memory and we don't want it to be
                                                    accidentally concatenated with other digits that may have been
                                                    adjacent. */

    }
    
    return;
}



#ifdef OLD_DATE
/* replaced with  fnBuildDateFmt */
/* *************************************************************************
// FUNCTION NAME: 
//      fnBuildDate
//
// DESCRIPTION: 
//      This is a utility function that builds a Unicode date string 
//              according to the format configured in the PCN parameters.  The
//              date duck status may also affect the output string.
//
// INPUTS:  
//      pDate- pointer to date/time structure containing the date to be
//                  converted to a Unicode string.
//      fIncludeDay- if set to FALSE, the day portion of the date is not
//                          included in the output string.
//      fIncludeYear- if set to FALSE, the year portion of the date is
//                          not included in the output string.
//      fIncludeSeparator- if set to FALSE, the date separation character is
//                              not inserted between the portions of the date
//      fCheckDuck- if set to TRUE, portions of the date string may be replaced
//                      by the date duck replacement character, depending
//                      on the current date duck status.  For example, if the
//                      day only was ducked, DEC 25 1972 would be displayed
//                      as DEC -- 1972 (assuming the replacement char was "-").
//      pDest- the output string is placed here
//      bLen- maximum length of the output string.  this function uses
//                  this for justification purposes and will guarantee that
//                  a null character terminates the output without exceeding
//                  this length.  (enough memory should still be allocated to
//                  pDest to accomodate a larger string.  typically we allocate
//                  a large buffer to pDest even if we only use a small portion.)
//      bNumTableItems- number of table text items associated with the field.
//      pTableTextIDs- pointer to the list of table text IDs
//      bFieldCtrl- field control byte, used for justification purposes.
//
// RETURNS:
//      FAILURE - if there is any error
//      SUCCESSFUL - otherwise
//
// TEXT TABLE USAGE:  1st string- date duck replacement character (required)
//                                  only the 1st char of the string is used
//                    2nd - 13th string: ordered names or abbreviations of months
//                                      (e.g. "JAN", "FEB", etc.).  this is
//                                      required if we are in a configuration that
//                                      uses an alphabetic month.
//
// NOTES:
//
// MODIFICATION HISTORY:
//      11/25/2005    Adam Liu         Moved calc logic to fnBuildDateFmt()
//                  Joe Mozdzer    Initial version
//      
// *************************************************************************/
BOOL fnBuildDate(DATETIME *pDate, 
                 BOOL fIncludeDay, 
                 BOOL fIncludeYear, 
                 BOOL fCheckDuck, 
                 BOOL fIncludeSeparator,
                 UINT16 *pDest, 
                 UINT8 bLen, 
                 UINT8 bNumTableItems,
                 UINT8 *pTableTextIDs, 
                 UINT8 bFieldCtrl)
{
    UINT8   bDateFormat;

    /* get the displayed date format byte from flash */
    bDateFormat = fnFlashGetByteParm(BP_REPORT_DATE_FORMAT);
    
    return fnBuildDateFmt(pDate,fIncludeDay,fIncludeYear,fCheckDuck,fIncludeSeparator,
                    bDateFormat,pDest,bLen,bNumTableItems,pTableTextIDs,bFieldCtrl);
}
#endif



/* *************************************************************************
// FUNCTION NAME: 
//      fnBuildDateFmt
//
// DESCRIPTION: 
//      This is a utility function that builds a Unicode date string 
//              according to the format configured in the PCN parameters.  The
//              date duck status may also affect the output string.
//
// INPUTS:  
//      pDate- pointer to date/time structure containing the date to be
//                  converted to a Unicode string.
//      bDateFormat - date format
//      bDDs -  date duck status
//      usDateSepChar - date sep  character
//      fIncludeDay- if set to FALSE, the day portion of the date is not
//                          included in the output string.
//      fIncludeYear- if set to FALSE, the year portion of the date is
//                          not included in the output string.
//      fIncludeSeparator- if set to FALSE, the date separation character is
//                              not inserted between the portions of the date
//      fCheckDuck- if set to TRUE, portions of the date string may be replaced
//                      by the date duck replacement character, depending
//                      on the current date duck status.  For example, if the
//                      day only was ducked, DEC 25 1972 would be displayed
//                      as DEC -- 1972 (assuming the replacement char was "-").
//      pDest- the output string is placed here
//      bLen- maximum length of the output string.  this function uses
//                  this for justification purposes and will guarantee that
//                  a null character terminates the output without exceeding
//                  this length.  (enough memory should still be allocated to
//                  pDest to accomodate a larger string.  typically we allocate
//                  a large buffer to pDest even if we only use a small portion.)
//      bNumTableItems- number of table text items associated with the field.
//      pTableTextIDs- pointer to the list of table text IDs
//      bFieldCtrl- field control byte, used for justification purposes.
//
// RETURNS:
//      FAILURE - if there is any error
//      SUCCESSFUL - otherwise
//
// TEXT TABLE USAGE:  1st string- date duck replacement character (required)
//                                  only the 1st char of the string is used
//                    2nd - 13th string: ordered names or abbreviations of months
//                                      (e.g. "JAN", "FEB", etc.).  this is
//                                      required if we are in a configuration that
//                                      uses an alphabetic month.
//
// NOTES:
//
// MODIFICATION HISTORY:
//      Adam Liu       Initial version
//      05/10/2010     Deborah Kohl   Add padding characters either when 
//                                    right aligned andin LTR mode or 
//                                    left aligned and in RTL mode
//      04/27/2010     Deborah Kohl   when in RTL writing mode, do not add
//                                    padding characters
//      02/28/2006     John Gao       Business Rules Extraction
// *************************************************************************/
BOOL fnBuildDateFmt(DATETIME *pDate,
                 UINT8 bDateFormat,
                 UINT16 usDDS,
                 UINT16 usDateSepChar, 
                 BOOL fIncludeDay, 
                 BOOL fIncludeYear, 
                 BOOL fCheckDuck, 
                 BOOL fIncludeSeparator,
                 UINT16 *pDest, 
                 UINT8 bLen, 
                 UINT8 bNumTableItems,
                 UINT8 *pTableTextIDs, 
                 UINT8 bFieldCtrl)
{
    UINT16  usTextID;
    UINT32  ulIndex;
    char   pStr[DATE_USTR_LEN] = {0};
    UINT16  pDayUStr[DAY_USTR_LEN] = {0,0};
    UINT16  pMonthUStr[MONTH_USTR_LEN] = {0,0};
    UINT16  pYearUStr[YEAR_USTR_LEN] = {0,0};
    UINT8   bDayLen;
    UINT8   bMonthLen;
    UINT8   bYearLen;
    UINT16  usDuckReplaceChar = NULL_VAL;
    BOOL    fRetval = FAILURE;
    UINT8   bWritingDir;
    //UINT8   bDDS;
    
    bWritingDir = fnGetWritingDir();

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pDest, bLen);

    /* if we are checking for date duck, get the duck replacement INT8acter from the string table.
        it is the first string. */
    if (fCheckDuck == TRUE)
    {
        if (bNumTableItems == 0)
        {
            return (fRetval); 
        }
        EndianAwareCopy(&usTextID, pTableTextIDs + 0, sizeof(UINT16));
        (void)fnCopyTableText(&usDuckReplaceChar, usTextID, 1);
        if( usDuckReplaceChar == NULL_VAL )  /* make sure it is a real character */
        {
            return (fRetval); 
        }
    }



    /* build the day string */
    if (fIncludeDay == TRUE)
    {
        bDayLen = sprintf(pStr, "%02d", pDate->bDay);
        fnAscii2Unicode(pStr, pDayUStr, bDayLen);

        /* if we are checking for date duck and the entire date is ducked or the
            day is ducked, replace the contents of the string with the replacement character */
        if (((usDDS & BOB_DUCK_DAY) || (usDDS & BOB_DUCK_ENTIRE)) && fCheckDuck)
        {
            for (ulIndex = 0; ulIndex < bDayLen; ulIndex++)
            {
                pDayUStr[ulIndex] = usDuckReplaceChar;
            }
        }
    }
    else
    {
        bDayLen = 0;
    }


    /* build the year string */
    if (fIncludeYear == TRUE)
    {
        /* check if we need to use a 2 or 4 digit year.  it depends on the configuration. */
        if ((bDateFormat == DATE_FORMAT_DDMMMYYYY) || 
            (bDateFormat == DATE_FORMAT_MMMDDYYYY) ||
            (bDateFormat == DATE_FORMAT_DDMMYYYY) || 
            (bDateFormat == DATE_FORMAT_MMDDYYYY) ||
            (bDateFormat == DATE_FORMAT_YYYYMMDD))
        {
            bYearLen = sprintf(pStr, "%02d%02d", pDate->bCentury, pDate->bYear);
        }
        else
        {
            bYearLen = sprintf(pStr, "%02d", pDate->bYear);
        }

        fnAscii2Unicode(pStr, pYearUStr, bYearLen);

        /* if we are checking for date duck and the entire date is ducked, replace
            the contents of the string with the replacement character */
        if ((usDDS & BOB_DUCK_ENTIRE) && fCheckDuck)
        {
            for (ulIndex = 0; ulIndex < bYearLen; ulIndex++)
            {
                pYearUStr[ulIndex] = usDuckReplaceChar;
            }
        }
    }
    else
    {
        bYearLen = 0;
    }


    /* build the month string */
    /* check if alpha or numeric format */
    if ((bDateFormat == DATE_FORMAT_MMMDDYY) || (bDateFormat == DATE_FORMAT_DDMMMYY) || 
        (bDateFormat == DATE_FORMAT_DDMMMYYYY) || (bDateFormat == DATE_FORMAT_MMMDDYYYY))
    {
        /* it's alpha.  make sure we have a list of month name strings.  fail if we don't. */
        if (bNumTableItems < 13)
        {
             return (fRetval);  
        }
        else
        {
            /* copy the appropriate name string */
        	EndianAwareCopy(&usTextID, pTableTextIDs + (pDate->bMonth * sizeof(UINT16)), sizeof(UINT16));
            bMonthLen = fnCopyTableText(pMonthUStr, usTextID, bLen);
        }
    }
    else
    {
        /* it's numeric */
        bMonthLen = sprintf(pStr, "%02d", pDate->bMonth);
        fnAscii2Unicode(pStr, pMonthUStr, bMonthLen);
    }
    /* if we are checking for date duck and the entire date is ducked, replace
        the contents of the month string with the replacement character */
    if ((usDDS & BOB_DUCK_ENTIRE) && fCheckDuck)
    {
        for (ulIndex = 0; ulIndex < bMonthLen; ulIndex++)
        {
            pMonthUStr[ulIndex] = usDuckReplaceChar;
        }
    }

    /* we now have the day, month, and year strings built.  now it's time to
        assemble the goods in the correct order */
    switch (bDateFormat)
    {
        case DATE_FORMAT_MMMDDYY:
        case DATE_FORMAT_MMDDYY:
        case DATE_FORMAT_MMMDDYYYY:
        case DATE_FORMAT_MMDDYYYY:
            /* all of these are month-day-year order */
            ulIndex = 0;
            memcpy(pDest + ulIndex, pMonthUStr, bMonthLen * sizeof(UINT16));
            ulIndex += bMonthLen;
            if (bDayLen)
            {
                if (fIncludeSeparator)
                {
                    pDest[ulIndex++] = usDateSepChar;
                }
                memcpy(pDest + ulIndex, pDayUStr, bDayLen * sizeof(UINT16));
                ulIndex += bDayLen;
            }
            if (bYearLen)
            {
                if (fIncludeSeparator)
                {
                    pDest[ulIndex++] = usDateSepChar;
                }
                memcpy(pDest + ulIndex, pYearUStr, bYearLen * sizeof(UINT16));
                ulIndex+= bYearLen;
            }
            break;
                    
        case DATE_FORMAT_DDMMYY:
        case DATE_FORMAT_DDMMMYY:
        case DATE_FORMAT_DDMMMYYYY:
        case DATE_FORMAT_DDMMYYYY:
            /* these are day-month-year order */
            ulIndex = 0;
            if (bDayLen)
            {
                memcpy(pDest + ulIndex, pDayUStr, bDayLen * sizeof(UINT16));
                ulIndex += bDayLen;
                if (fIncludeSeparator)
                {
                    pDest[ulIndex++] = usDateSepChar;
                }
            }
            memcpy(pDest + ulIndex, pMonthUStr, bMonthLen * sizeof(UINT16));
            ulIndex += bMonthLen;
            if (bYearLen)
            {
                if (fIncludeSeparator)
                {
                    pDest[ulIndex++] = usDateSepChar;
                }
                memcpy(pDest + ulIndex, pYearUStr, bYearLen * sizeof(UINT16));
                ulIndex+= bYearLen;
            }
            break;

        case DATE_FORMAT_YYMMDD:
        case DATE_FORMAT_YYYYMMDD:
            /* these are year-month-day order */
            ulIndex = 0;
            if (bYearLen)
            {
                memcpy(pDest + ulIndex, pYearUStr, bYearLen * sizeof(UINT16));
                ulIndex += bYearLen;
                if (fIncludeSeparator)
                {
                    pDest[ulIndex++] = usDateSepChar;
                }
            }
            memcpy(pDest + ulIndex, pMonthUStr, bMonthLen * sizeof(UINT16));
            ulIndex += bMonthLen;
            if (bDayLen)
            {
                if (fIncludeSeparator)
                {
                    pDest[ulIndex++] = usDateSepChar;
                }
                memcpy(pDest + ulIndex, pDayUStr, bDayLen * sizeof(UINT16));
                ulIndex += bDayLen;
            }
            break;

        default:
            /* anything else is an error */
             return (fRetval);  
    }


    /* if field is right-aligned, pad left of string with spaces */
    if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
        pDest[ulIndex] = NULL_VAL;
        fnUnicodePadSpace(pDest, ulIndex, bLen);
    }

    /* make sure we are null terminated in case the date was bigger than the field */
    pDest[bLen] = NULL_VAL;

    /* we are done.. return success */
    fRetval = SUCCESSFUL;

    return (fRetval);   

}




/* *************************************************************************
// FUNCTION NAME: 
//      fnGenericBinaryField
//
// DESCRIPTION: 
//      A utility function for processing output fields that converts
//              a binary number to Unicode and justifies it.
//
// INPUTS:  Standard field function inputs, minus the table text stuff and
//          plus the following:
//          
//          lwBinary - the binary number to display
//
// RETURNS:
//      NONE
//
// TEXT TABLE USAGE:  none
//
// NOTES:
//
// MODIFICATION HISTORY:
//      05/10/2010    Deborah Kohl Add padding characters either when 
//                                 right aligned andin LTR mode or 
//                                 left aligned and in RTL mode
//      Joe Mozdzer       Initial version
//      
// *************************************************************************/
void fnGenericBinaryField(UINT16 *pFieldDest, 
                          UINT8 bLen,
                          UINT8 bFieldCtrl, 
                          UINT32 lwBinary)
{
    UINT8   bUniLen=0;
    UINT8   bWritingDir;    
    
    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    /* convert the binary number to Unicode */
    bUniLen += fnBin2Unicode(pFieldDest, lwBinary);

    bWritingDir = fnGetWritingDir();

    /* if field is right-aligned, pad left of string with spaces */
    if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
        ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
    {
        // when field right-aligned, we need to pad only if we are not in RTL mode, 
        // because right aligned in LTR mode gives left aligned in RTL mode
        // when field left-aligned, we need to pad only if we are in RTL mode, 
        // because left aligned in LTR mode gives right aligned in RTL mode
        pFieldDest[bUniLen] = NULL_VAL;
        fnUnicodePadSpace(pFieldDest, bUniLen, bLen);
    }

    /* make sure the field is NULL terminated.  if the translation happened to exceed
        the width of this field, this will truncate the unicode string that is sitting
        there */
    pFieldDest[bLen] = NULL_VAL;
    return;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnDisplayTableText
//
// DESCRIPTION: 
//      A utility function for processing output fields that simply
//      need to display an item from the table text list as-is, without
//      any further processing other than justification.
//
// INPUTS:  
//      Standard field function inputs, plus the following:
//      bItemNum - the table text string to display.  0 would be the first
//                  string, 1 the second, and so on.  if the number is
//                  out of valid range, the field is left blank.
//
// RETURNS:
//      NONE
//
// TEXT TABLE USAGE:  none
//
// NOTES:
//
// MODIFICATION HISTORY:
//      05/14/2010    Jingwei,Li   Trim the redundant space when get table text for RTL.
//      05/10/2010    Deborah Kohl Add padding characters either when 
//                                 right aligned andin LTR mode or 
//                                 left aligned and in RTL mode
//      Joe Mozdzer       Initial version
//      
// *************************************************************************/
void fnDisplayTableText(UINT16 *pFieldDest, 
                        UINT8 bLen, 
                        UINT8 bFieldCtrl,
                        UINT8 bNumTableItems, 
                        UINT8 *pTableTextIDs, 
                        UINT8 bItemNum)
{
    UINT16  usTextID;
    UINT8   bUniLen;
    UINT8   bWritingDir;        

    /* initialize the field to all spaces */
    SET_SPACE_UNICODE(pFieldDest, bLen);

    if (bItemNum < bNumTableItems)
    {
    	EndianAwareCopy(&usTextID, pTableTextIDs + (sizeof(UINT16) * bItemNum), 2);
        bWritingDir = fnGetWritingDir();
        if(bWritingDir == RIGHT_TO_LEFT)
        {
             bUniLen = fnCopyTableTextForRTL(pFieldDest, usTextID, bLen);
        }
        else
        {
             bUniLen = fnCopyTableText(pFieldDest, usTextID, bLen);
        }

        /* if field is right-aligned, pad left of string with spaces */
        if (  (   (bFieldCtrl & ALIGN_MASK)  && (bWritingDir == LEFT_TO_RIGHT))
            ||( (!(bFieldCtrl & ALIGN_MASK)) && (bWritingDir == RIGHT_TO_LEFT)) )
        {
            // when field right-aligned, we need to pad only if we are not in RTL mode, 
            // because right aligned in LTR mode gives left aligned in RTL mode
            // when field left-aligned, we need to pad only if we are in RTL mode, 
            // because left aligned in LTR mode gives right aligned in RTL mode
            pFieldDest[bUniLen] = NULL_VAL;
            fnUnicodePadSpace(pFieldDest, bUniLen, bLen);
        }
    }

    return;
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnCopyTableText
//
// DESCRIPTION: 
//      Copies a table text unicode string to the specified location.
//      The number of characters copied will be the lesser of a) the
//      maximum length of the copy as specified by an input parameter,
//      or b) the length of the table text string (which is terminated
//      by a null character)
//
// INPUTS:  
//      pDest - pointer to the destination unicode string. memory allocated
//                  to the string must be large enough to accomodate bLen
//                  unicode characters.
//      wTextID - ID number of the table text string to copy 
//      bMaxLen - maximum length of the table text string
//
// RETURNS: 
//      The number of characters copied.
//
// NOTES:
//
// MODIFICATION HISTORY:
//      Joe Mozdzer    Initial version
//      
// *************************************************************************/
UINT8 fnCopyTableText(UINT16 *pDest, 
                      UINT16 wTextID, 
                      UINT8 bMaxLen)
{
    UINT32   ulOffset;
    UINT16  usUnicodeChar;
    UINT32   ulLenIndex;
    const ELEMENT_DIRECTORY *pElementDir = fnOITGetTableTextDirectory();
    const LANGUAGE_DIRECTORY *pLanguageDir = fnOITGetLanguageDirectory();
    

    /* get the offset within the table text element to the string we need */
    EndianAwareCopy(&ulOffset, pElementDir->pOffsetList + wTextID, 4);

    /* copy the table text string into the field, stopping when we hit a NULL character or reach
        the maximum length */
    ulLenIndex= 0;
    while (ulLenIndex < bMaxLen)
    {
    	EndianAwareCopy(&usUnicodeChar, pLanguageDir->pTableTextElement +
                     ulOffset + (ulLenIndex * sizeof(UINT16)),sizeof(UINT16));
        if (usUnicodeChar == NULL_VAL)
        {
            break;
        }
        else
        {
            pDest[ulLenIndex++] = usUnicodeChar;
        }
    }
        
    return ((UINT8) ulLenIndex);
}



/* *************************************************************************
// FUNCTION NAME: 
//      fnUnicodeAddMoneySign
//
// DESCRIPTION: 
//      Adds the currency sign to the beginning or end of a Unicode string.
//      The location of the currency sign depends on a flash PCN 
//      parameter.  If the currency sign goes before the value, the value 
//      is shifted to the right to make room for it.  If is goes
//      after the value, it is tacked on immediately to the right
//      of the last value character.
//
// INPUTS:  
//      pMoneyString - pointer to a Unicode string.  it must be pre-malloced
//                          to allow room for the currency sign to be added.
//
//      bMoneyLen - Initial length of the Unicode string.
//
// RETURNS: 
//      length, in words, of the Unicode string after the currency sign
//          has been added
//
// NOTES:
//
// MODIFICATION HISTORY:
//      10/15/2008  Joe Qu      Modified to fix fraca GMSE00150746
//      09/01/2008  Ivan Le Goff Manage a case for when the currency symbol is
//                               a null char
//      12/05/2005  Adam Liu     Adapted for FuturePhoenix
//      01/29/1999  Joe Mozdzer Initial version
//
// *************************************************************************/
UINT8 fnUnicodeAddMoneySign(UINT16 *pMoneyString, 
                            UINT8 bMoneyLen)
{
    size_t  bSymbolLen;
    UINT16  *pUnicodeCurrencySymbol;
    UINT8   bCurrencySymbolPlacement;

    /* get the currency symbol and location out of flash */
    pUnicodeCurrencySymbol = fnFlashGetUnicodeStringParm(USP_DISP_CURRENCY_SYM);
    if (pUnicodeCurrencySymbol == NULL_PTR)
    {//nothing to do but return
    	return bMoneyLen;
    }

    bSymbolLen = unistrlen(pUnicodeCurrencySymbol);
    if ((bSymbolLen == 0) || (bSymbolLen > MAX_CURRENCY_LEN))
    {//either no symbol or symbol is too big
    	return bMoneyLen;
    }

    bCurrencySymbolPlacement = fnFlashGetByteParm(BP_CURRENCY_SYM_PLACEMENT);

    /* check if the currency symbol goes before or after the money amount */
    if (bCurrencySymbolPlacement == 0)
    {
        /* Placement = BEFORE */
        /* make room for the symbol by shifting the money string to the right */
        memmove(pMoneyString + bSymbolLen, pMoneyString, (bMoneyLen * sizeof(UINT16)));

        /* copy the symbol into the space we just opened up */
        memcpy(pMoneyString, pUnicodeCurrencySymbol, bSymbolLen * sizeof(UINT16));
    }
    else
    {
        /* Placement = AFTER */
        /* copy the symbol into the area immediately after the money string */
        memcpy(pMoneyString + bMoneyLen, pUnicodeCurrencySymbol, (bSymbolLen * sizeof(UINT16)));
    }
    
    return (bMoneyLen + bSymbolLen);    

}



/* *************************************************************************
// FUNCTION NAME: 
//      fnUnicodeAddMinusSign
//
// DESCRIPTION: 
//      Adds the minus sign to the beginning of a Unicode string.
//
// INPUTS:  
//      pUnicode -  pointer to a Unicode string.  it must be pre-malloced
//                          to allow room for the minus sign to be added.
//
//      bInputLen - Initial length of the Unicode string.
//
// RETURNS: 
//      length, in words, of the Unicode string after the minus sign
//          has been added
//
// NOTES:
//
// MODIFICATION HISTORY:
//      12/05/2005  Adam Liu     Adapted for FuturePhoenix
//      01/29/1999  Joe Mozdzer Initial version
//
// *************************************************************************/
UINT8 fnUnicodeAddMinusSign(UINT16 *pUnicode, 
                            UINT8 bInputLen)
{

    memmove(pUnicode + 1, pUnicode, bInputLen * sizeof(UINT16));
    pUnicode[0] = UNICODE_MINUS_SIGN;

    return (bInputLen + 1);
}


/* *************************************************************************
// FUNCTION NAME:
//      fnUnicodeAddMinusSignRightAlign
//
// DESCRIPTION:
//      Adds the minus sign to the beginning of a Unicode string.
//
//
// INPUTS:
//      pUnicode -  pointer to a Unicode string.  it must have been
//                      right aligned.
//
//          bInputLen - Initial length of the Unicode string
//
// RETURNS:
//      length, in words, of the Unicode string after the minus sign
//          has been added
//
// NOTES:
//
// MODIFICATION HISTORY:
//      04/05/2007      Joey Cui      Reuse from Mega
//
// *************************************************************************/
UINT8 fnUnicodeAddMinusSignRightAlign(UINT16 *pUnicode,
                                      UINT8 bInputLen)
{
    UINT8   bIndex;

    // Search the first non-SPACE character, add minus sign ahead
    for (bIndex = 0; bIndex < bInputLen; bIndex++)
    {
        if (*(pUnicode+bIndex) != UNICODE_SPACE)
        {
            if (bIndex > 0)
            {
                pUnicode[bIndex-1] = UNICODE_MINUS_SIGN;
            }
            break;
        }
    }

    return bInputLen;
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnOIFormatMoneyForDisplay
//
// DESCRIPTION: 
//      Common utilty function that creates a null-terminated unicode
//      money value string formatted for display either in standard UIC field
//      or in mimic area
//
// INPUTS:
//       pDest = address of caller's output buffer
//          caller is responsible for insuring buffer is large enough
//       dblValue = money value to be formatted
//          this function will accomodate negative money values
//          fDisplayMimic = true, if formatted string is for mimic
//          false, for all other display fields mimic version does 
//          not get dollar sign
//
// OUTPUTS:
//       pDest, buffer pointed to by pDest argument contains null-terminated money
//          value string with pre-fixed dollar sign, if not mimic version, and 
//          prefixed minus sign if value is negative for all versions
//
// RETURNS:
//        bLen = length of formatted 
//
// NOTES:
//
// MODIFICATION HISTORY:
//      cristeb        Initial version
//      
// *************************************************************************/
UINT8 fnOIFormatMoneyForDisplay( UINT16 *pDest, 
                                 double dblValue, 
                                 BOOL fDisplayMimic )
{
    UINT8       bLen ;                      // length returned to caller
    UINT8       bMinDisplayedIntegers ;     // formatting info
    UINT8       bMinDisplayedDecimals ;                     // formatting info
    // initialized locals
    UINT16      usIntegerArg = BP_MIN_DISPLAYED_INTEGERS ;   // function argument
    UINT16      usDecimalArg = BP_MIN_DISPLAYED_DECIMALS ;   // function argument
    BOOL        fNegative = FALSE ;

    if( fDisplayMimic == TRUE)
    {
        // override typical argument value for Mimic display purposes
        usIntegerArg = (UINT16) BP_INDICIA_MIMIC_MIN_DISPLAYED_INT ;
        usDecimalArg = (UINT16) BP_INDICIA_MIMIC_MIN_DISPLAYED_DEC ;
    }

    // get system-specific formatting info
    bMinDisplayedIntegers = fnFlashGetByteParm( usIntegerArg );
    bMinDisplayedDecimals = fnFlashGetByteParm( usDecimalArg );

    if ( dblValue < 0.0 )
    {
        // Record need for minus sign and convert number to postive value
        fNegative = TRUE ;
        dblValue = -dblValue ;
    }

    // prep value
    bLen = fnMoney2Unicode( pDest, dblValue, TRUE, bMinDisplayedIntegers, 
                                                   bMinDisplayedDecimals );

    // mimic version doesn't get dollar sign here
    if( fDisplayMimic == FALSE )
    {
        bLen = fnUnicodeAddMoneySign( pDest, bLen );
    }

    // prepend minus sign and adjust string length
    if (fNegative == TRUE)
    {
        bLen = fnUnicodeAddMinusSign( pDest, bLen );
    }

    return bLen ;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnQuickSortList
//
// DESCRIPTION: 
//      Utility function to sort the input list. This function could sort the 
//      array by its name or speed codes and this depends the parameter ucSortType
//
// INPUTS:
//      pListInfo     -   pointer to a S_SORT_INFO array with name string
//                                 pointer and corresponding speed code.
//      usArrayLength -   length of the array
//      ucSortType    -   sort type (by name or by speed code )
//
// OUTPUTS:
//      pListInfo  -   pointer to sorted S_SORT_INFO array.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  02/06/2006  Kan Jiang       Initial version
//  02/10/2006  Vincent Yi      Renamed to fnQuickSortList and refined
//
// *************************************************************************/
void fnQuickSortList (S_SORT_INFO * pListInfo, 
                      UINT16        usArrayLength,
                      SINT8         ucSortType)
{
    if (usArrayLength == 0)
    {
        return;
    }
     
    switch(ucSortType)
    {
    case SORTBYCODE:
       fnShellSortListByCode(pListInfo, usArrayLength);
       break;
    
    case SORTBYNAME:
       fnShellSortListByName(pListInfo, usArrayLength); 
       break;
    
    default:
       break;
    }

    return;
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnGetListTable
//
// DESCRIPTION: 
//      Utility function that get the pointer of struct rListTable.
//
// INPUTS:      
//      None.
//
// RETURNS:
//      The pointer of rListTable
//
//   NOTES: 
//      None
//
// MODIFICATION HISTORY:
//      7/25/2005 John Gao Initial version
//      
// *************************************************************************/
LIST_TABLE  *fnGetListTable(void)
{
    return(&rListTable);
}


/* *************************************************************************
// FUNCTION NAME: 
//      fnOITGetBaseType
//
// DESCRIPTION: 
//      Get current base type
//
// INPUTS:
//      None.
//
// OUTPUTS:
//      None.
//
// RETURNS:
//      OIT_BASE_300C           1
//      OIT_BASE_400C           2
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/2/2005   Vincent Yi      Initial version
//
// *************************************************************************/
UINT8 fnOITGetBaseType (void)
{
    UINT8   ucBaseType = OIT_BASE_300C;

    //TODO - confirm CSD2/CSD3 behaves the same as DM300C/DM400C here
    if (fnGetMeterModel() == CSD2)
    {
        ucBaseType = OIT_BASE_300C;
    }
    else if (fnGetMeterModel() >= CSD3)
    {
        ucBaseType = OIT_BASE_400C;
    }

    return ucBaseType;
}

/**********************************************************************
        Private Functions
**********************************************************************/

/* *************************************************************************
// FUNCTION NAME: 
//      fnShellSortListByCode
//
// DESCRIPTION: 
//      Shell sort algorithm to sort the input codes in pSortList[].ulSpeedCode, 
//      their Unicode strings move correspondingly after sorting.
//
// PRE-CONDITIONS:
//      All codes to be sorted have been stored in pSortList[].ulSpeedCode, 
//      their corresponding pointers of the Unicode strings stored in pSortList[].pUniName
//
// INPUTS:
//      pSortList    -   pointer to a S_SORT_INFO array with name string
//                          pointer and corresponding speed code.
//      sListSize    -   size of the array
//
// OUTPUTS:
//      pSortList    -   the S_SORT_INFO array with sorted speed code
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/13/2005  Vincent Yi      Initial version
//
// *************************************************************************/
static void fnShellSortListByCode (S_SORT_INFO *    pSortList,
                                   UINT16           sListSize)
{
    SINT16      sScanIndex;
    UINT32      ulTempCode;
    UINT8   *   pUniTempName;
    SINT16      j;
    SINT16      k;
    
    // Totally scan log2(n+1) rounds
    for( sScanIndex = (SINT16)(sListSize/2); sScanIndex>0; sScanIndex=sScanIndex/2 )
    {
        for( j=sScanIndex; j<sListSize; j++ )
        {
            ulTempCode  = pSortList[j].ulSpeedCode;
            pUniTempName= pSortList[j].pUniName;
            // Search the insert position and move
            for (k = j-sScanIndex; 
                 k>=0 && ulTempCode<pSortList[k].ulSpeedCode;
                 k -= sScanIndex)
            {
                pSortList[k+sScanIndex].ulSpeedCode = pSortList[k].ulSpeedCode;
                pSortList[k+sScanIndex].pUniName = pSortList[k].pUniName;
            }
            // Insert
            pSortList[k+sScanIndex].ulSpeedCode = ulTempCode;
            pSortList[k+sScanIndex].pUniName = pUniTempName;
        }
    }
}

/* *************************************************************************
// FUNCTION NAME: 
//      fnShellSortListByName
//
// DESCRIPTION: 
//      Shell sort algorithm to sort the input Unicode strings in 
//      pSortList[].pUniName, their codes move correspondingly after sorting.
//
// PRE-CONDITIONS:
//      All pointers of the Unicode strings to be sorted have been stored in
//      pSortList[].pUniName, their corresponding codes stored in pSortList[].ulSpeedCode
//
// INPUTS:
//      pSortList    -   pointer to a S_SORT_INFO array with name string
//                          pointer and corresponding code.
//      sListSize    -   size of the array
//
// OUTPUTS:
//      pSortList    -   the S_SORT_INFO array with sorted name strings.
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  07/27/2006  Vincent Yi      Modified to fix address alignment issue
//  12/13/2005  Vincent Yi      Initial version
//
// *************************************************************************/
static void fnShellSortListByName (S_SORT_INFO *    pSortList,
                                   UINT16           sListSize)
{
    SINT16      sScanIndex;
    UINT32      ulTempCode;
    UINT8   *   pUniTempName;
    SINT16      j;
    SINT16      k;
    UNICHAR     pTmpStr1[MAX_UNI_STR_LEN+1];
    UNICHAR     pTmpStr2[MAX_UNI_STR_LEN+1];
    
    // Totally scan log2(n+1) rounds
    for( sScanIndex = (SINT16)(sListSize/2); sScanIndex>0; sScanIndex = sScanIndex/2 )
    {
        for (j=sScanIndex; j<sListSize; j++)
        {
            ulTempCode  = pSortList[j].ulSpeedCode;
            pUniTempName= pSortList[j].pUniName;
            // Search the insert position and move
            k = j-sScanIndex;
            while (k>=0)
            {
                // Make the unicode string aligned
                memcpy (pTmpStr1, pUniTempName, sizeof(pTmpStr1)); 
                memcpy (pTmpStr2, pSortList[k].pUniName, sizeof(pTmpStr2)); 
                if (unistrcmp(pTmpStr1, pTmpStr2)<0)
                {
                    pSortList[k+sScanIndex].ulSpeedCode = pSortList[k].ulSpeedCode;
                    pSortList[k+sScanIndex].pUniName = pSortList[k].pUniName;
                    k -= sScanIndex;
                }
                else
                {
                    break;
                } 
            } 
            // Insert
            pSortList[k+sScanIndex].ulSpeedCode = ulTempCode;
            pSortList[k+sScanIndex].pUniName = pUniTempName;
        }
    }
}

/* *************************************************************************
// FUNCTION NAME:
//      fnGetAutoDunsState
//
// DESCRIPTION:
//      Utility function that gets the value of eAutoDunsState
//
// INPUTS:
//      None
//
// RETURNS:
//      The value of eAutoDunsState (in AUTO_DUNS_STATE enum range)
//
//   NOTES:
//      None
//
// MODIFICATION HISTORY:
//      7/25/2005 John Gao Initial version
//
// *************************************************************************/
AUTO_DUNS_STATE fnGetAutoDunsState(void)
{
    return(eAutoDunsState);
}



/* *************************************************************************
// FUNCTION NAME:
//      fnSetAutoDunsState
//
// DESCRIPTION:
//      Utility function that sets the value of eAutoDunsState
//
// INPUTS:
//      AUTO_DUNS_STATE value
//
// RETURNS:
//      None
//
//   NOTES:
//      None
//
// MODIFICATION HISTORY:
//      7/25/2005 John Gao Initial version
//
// *************************************************************************/
void fnSetAutoDunsState(AUTO_DUNS_STATE eNewState)
{
    eAutoDunsState =  eNewState;
}


/*end oicommon.c*/
