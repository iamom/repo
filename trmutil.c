/************************************************************************
   PROJECT:        Horizon CSD
   MODULE NAME:    trmutil.c

   DESCRIPTION:    This file contains Transaction Record Manager utilities.
   	   	   	   	   Primarily they are for uploading

 ----------------------------------------------------------------------
                   Copyright (c) 2017 Pitney Bowes Inc.
                   37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------

*************************************************************************/
#include "stdio.h"
#include "stdlib.h"
#include <string.h>
#include "nucleus.h"
#include "storage/nu_storage.h"
#include "cmos.h"
#include "datdict.h"
#include "clock.h"      /* needed until integration with real DCAP include files */
#include "dcaputil.h"
#include "xmltypes.h" 
#include "xmlparse.h"
#include "xmlform.h"
#include "bob.h"
#include "bobutils.h"
#include "features.h"
#include "fwrapper.h"
#include "pbptask.h"
#include "dcapi.h"
#include "gtypes.h"
#include "bwrapper.h"
#include "custdat.h"
#include "download.h"
#include "global.h"
//#include "oit.h"
//#include "rateadpt.h"

#include "asrapi.h"
#include "trmstrg.h"
#include "trmdefs.h"
#include "trm.h"
#include "api_status.h"

#include "cwrapper.h"
#include "sysdata.h"
#include "api_temporary.h"
#include "sysdatadefines.h"

/* Period Types */
#define PT_PERIODICAL  1 // MUST START WITH 1 !!
#define PT_BETWEEN_UPLOADS 2
#define PT_PERIOD_LIST 3
#define PT_BETWEEN_UPLOADS_W_TIME 4
#define PT_NUMBER_OF_PERIOD_TYPES 5 // MUST BE LAST IN LIST!!

//---------------------------------------------------------------------
// External Function Prototypes:
//---------------------------------------------------------------------

extern DATETIME fnDCAP_PM_NormaliseDate( DATETIME *pDateTime );
extern unsigned char fnDblMoney2Ascii( double dblMoney, unsigned short *pLen, char * pAscii );
extern void fnProcessSCMError( void );
extern BOOL fnOiIsItABalanceInquiry( void );
extern void *fnPBPS_GetData( unsigned long ulDataID,
                      unsigned long ulDataItemNo,
                      unsigned char bFormatType, 
                      unsigned short *bsize );

extern void fnDCAP_PM_ScheduleUploadDateTime(DATETIME *pDateTime, BOOL fAddGraceDays);
VOID fnFilePath(CHAR* outPath, CHAR* path, CHAR* fileName);

extern NU_MEMORY_POOL    *pSystemMemoryCached;

extern BOOL     		fUploadInProgress;



//---------------------------------------------------------------------
/*
 * Macros
 */

//#define DCAPDNL_PUTSTUFF(p,x) fnDCAPDnlTryAddChars(p, x, m_bDCAPDnlCharDataBuffer, &m_wDCAPDnlCharDataBuffLen, m_wMaxDcpDnlCharDataLen)

/*
 * Global Variables
 */


BOOL bNewUploadDates = FALSE;
DATETIME sDateTime; // TDT
double dCreditLimit; // TDT

unsigned short  usDcapWriteStatus = 0;          /* write status byte for DCAP / UIC API interface */

unsigned short  usDcapActionStatus = 0;         /* action status byte for DCAP / UIC API interface */

unsigned char   ucGraceDays = 0;    // TDT
unsigned char   ucPeriodIndex = 0; // TDT
unsigned char   ucNextUploadDateIndex = 0; // TDT
BOOL bDCAP_DnlStarted = FALSE; // TDT 
BOOL bNewUploadDatesDnlInProgress = FALSE; //TDT
BOOL bNewPeriodDatesDnlInProgress = FALSE; // TDT
DATETIME TempPeriodEndList[ MAX_PERIOD_END_DATES ]; // TDT
DATETIME TempNextUploadDateList[ MAX_NEXT_UPLOAD_DATES ]; //TDT

#define DCAP_MAX_MTR_IDX_REC_ATTR_LEN 16

//-------------------------------------------------------------
//  External Variable Declarations
//-------------------------------------------------------------

extern DCB_PARAMETERS_t *pParameters;  // TDT
extern DCB_CURRENT_PERIOD_t_EXP *pCurPeriod; // TDT
extern Daily_Register_Logs *pDailyLogs;     //Daily logs
extern Tech_Supervision_Data *pTechData;    //Technical Supervision data
extern Adjustable_Log *pAdjustableLog;      //circular buffer

#ifndef G900
extern const unsigned short wUICSoftwareVersion;
#else
extern const char UicVersionString[];
#endif


extern CMOSRATECOMPSMAP CMOSRateCompsMap;

/*
 * function prototypes 
 */
void fnDCAPHexString2Ascii(char *, int , BOOL , char *);
static BOOL fnCheckIgnorePeriodDownload( void );


// The following array is used to hold a list of the locations in the RateCompsMap
// that contain a data capture module.  T.Dometios 4/18/2006
static unsigned char DcmComponentList[ MAX_CMOS_RATE_COMPONENTS ]; 



DATETIME fnDCAP_PM_NormaliseDate(DATETIME *pDateTime)
{
   DATETIME retval;

   retval = *pDateTime;
   retval.bHour = 0;
   retval.bMinutes = 0;
   retval.bSeconds = 0;

   return(retval);
}


/****************************************************************************/
/* Title      : fnDCAPCMOSGetDataCaptureArea                                */
/*                                                                          */
/* Author     : WFB                                                         */
/*                                                                          */
/* Date       : 9/12/2000                                                   */
/*                                                                          */
/* Description :                                                            */
/*              Called by DCAP API when a flash write is needed             */
/*                                                                          */
/* Input : none                                                             */
/*                                                                          */
/* Output: none                                                             */
/*                                                                          */
/* Notes:     Modifys usDcapWriteStatus.                                    */
/*                                                                          */
/****************************************************************************/
void    fnDCAPFlashWritePending(void) 
{ 
        usDcapWriteStatus |= DCAP_FLASH_WRITE_PENDING;  /* set write pending bit flag */
} 

/****************************************************************************/
/* Title      : fnDCAPIsDataCaptureActive()                                 */
/*                                                                          */ 
/* Author     : WFB                                                         */
/*                                                                          */
/* Date       : 9/13/2000                                                   */
/*                                                                          */
/* Description :                                                            */
/*              Called by DCAP API to determine if DCAp is feature enabled  */                                                                  
/*                                                                          */
/* Output: returns TRUE or FALSE                                            */
/* Notes:                                                                   */
/*                                                                          */
/****************************************************************************/
BOOL    fnDCAPIsDataCaptureActive(void) 
{
	return (TRUE);
}


/****************************************************************************/
/* Title      : fnGetDcapActionStatus(void)                                 */
/*                                                                          */
/* Author     : WFB                                                         */
/*                                                                          */
/* Date       : 9/22/2000                                                   */
/*                                                                          */
/* Description :                                                            */
/*              Called by OI to retrieve current DCAP action status         */
/* Input : none                                                             */
/*                                                                          */
/* Output: returns usDcapActionStatus                                       */
/* Notes:                                                                   */
/*                                                                          */
/****************************************************************************/
unsigned short  fnGetDcapActionStatusFlag(void) 
{
    return (usDcapActionStatus);
}

/****************************************************************************/
/* Title      : fnClearDcapActionStatusFlag( unsigned short )               */
/*                                                                          */
/* Author     : WFB                                                         */
/*                                                                          */
/* Date       : 9/22/2000                                                   */
/*                                                                          */
/* Description :                                                            */
/*              Called by OI to clear a specific (or group of)  DCAP        */
/*              action status flags                                         */
/* Input :      clearing mask is passed in unsigned short that has bits     */
/*              SET TO 1 in the postions to be cleared                      */
/*                                                                          */
/* Output: None                                                             */
/* Notes:  More than on flag bit can be cleared by setting multiple bits    */
/*          in the mask.  To Clear all, pass all 1's.                       */
/*                                                                          */
/****************************************************************************/
void    fnClearDcapActionStatusFlag(unsigned short DASFmask) 
{
    usDcapActionStatus &= (~DASFmask);  
}

/****************************************************************************/
/* Title      : fnDCAPStatusChanged( )                                      */
/*                                                                          */
/* Author     : Derek DeGennaro                                             */
/*                                                                          */
/* Date       : 11/20/2000                                                  */
/*                                                                          */
/* Description :                                                            */
/*              called by dcapi to inform uic that the internal dcap        */
/*              status has changed.  The status can be retrieved using      */
/*              the various dcapi routines.                                 */
/* Input : None                                                             */
/*                                                                          */
/* Output: None                                                             */
/*                                                                          */
/****************************************************************************/
void fnDCAPStatusChanged(void)
{
    /* not being used now, maybe later */
}

/***************************************************************************/
// Title      : fnDCAPGetMeterAuditRecord                                   
// Author     : Derek DeGennaro                                             
// Description :                                                            
// This function retrieves an Audit record from the Meter (PSD) and copies
// it into a buffer.  The buffer is allocated by the caller of the function.
// Input :                                                                  
// pBuffer - Pointer to the buffer where the record will be copied to.
// lwMaxBufferLength - size(number of bytes) of the buffer 
// plwResultLength - Pointer to a variable where the function outputs the
//  number of bytes copied into pBuffer
// Output:
// *plwResultLength bytes copied into pBuffer if successfull.
// Return Value:
// TRUE if the record has been copied successfully, FALSE otherwise.
//
// NOTE: The actual length of the Audit Record MUST NEVER be greater than
// DC_MAX_AUDIT_RECORD_LENGTH.  If it is then the value of DC_MAX_AUDIT_RECORD_LENGTH
// must be increased.
/***************************************************************************/
BOOL fnDCAPGetMeterAuditRecord( unsigned char *pBuffer, unsigned long lwMaxBufferLength, unsigned long *plwResultLength)
{
    BOOL fReturnValue = FALSE;
#ifndef JANUS
    unsigned long lwBytesCopied = 0;
    struct TransferControlStructure psdXferStruct;
    unsigned char bPSDStatus = BOB_OK;
    

    /* check input */
    if (fnDCAPIsDataCaptureActive() == TRUE &&
        pBuffer != NULL && 
        lwMaxBufferLength > 0 && lwMaxBufferLength <= DC_MAX_AUDIT_RECORD_LENGTH)
    {
        /* set up the xfer struct */
        (void)memset((void*)&psdXferStruct,0,sizeof(psdXferStruct));
        psdXferStruct.ptrToDataToSendToDevice = NULL;
        psdXferStruct.ptrToCallersDestBuffer = (char*)pBuffer;
        psdXferStruct.ptrToDeviceReplyData[0] = NULL;
        psdXferStruct.bytesInSource = 0;
        (void)memset(psdXferStruct.deviceReplyStatus,0,4);
        psdXferStruct.bytesWrittenToDest[0] = 0;
        
        /* get the audit record */
        bPSDStatus = fnWriteData((unsigned short)BOBAMAT0,
                                (unsigned short)GET_PSD_AUDIT_RECORD,
                                (void*)&psdXferStruct);
        /* check the results from the bob task */
        if (bPSDStatus == BOB_OK && 
            psdXferStruct.bytesWrittenToDest[0] > 4 && psdXferStruct.bytesWrittenToDest[0] <= DC_MAX_AUDIT_RECORD_LENGTH)
        {
            /* get the length of the record from the xfer structure. */
            lwBytesCopied = (unsigned long)psdXferStruct.bytesWrittenToDest[0];

            /* the length reported from the psd and bob includes a 4-byte status code.  
            the record itself does not include this 4-bytes.  so this function 
            reduces the length it returns by 4. */
            lwBytesCopied -= 4;
            
            fReturnValue = TRUE;                
        }   
    }
    
    /* copy the return length */
    if (plwResultLength != NULL)
        *plwResultLength = lwBytesCopied;
#else
        *plwResultLength = 0;
#endif      
        
    return fReturnValue;
}


/**********************************
    DCAP Rules Download
    Document Handler
***********************************/

/**********************************************************
Actual DCAP API functions
**********************************************************/

/***************************************************************
XML Document Handler Wrappers for DCAP
***************************************************************/

enum DCAP_DCPBKTNUM_CONTEXT m_DCAPCurrBktNumContext = NODCPBKTNUM_CONTEXT;
enum DCAP_WBNNUM_CONTEXT m_DCAPCurrWbnNumContext = NO_WBNNUM_CONTEXT;

/* const used to create buffers for elem names in dcap wrapper fcns */
//static const short m_wMaxDcpDnlElemNameLen = 15;


/* buffer & curr len for buff */
//static unsigned char m_bDCAPDnlCharDataBuffer[m_wMaxDcpDnlCharDataLen + 1];
unsigned short m_wDCAPDnlCharDataBuffLen = 0;
unsigned char m_bDCAPDnlBktNameBuff[m_wMaxDcpDnlCharDataLen + 1];
unsigned char m_bDCAPDnlBktNumBuff[m_wMaxDcpDnlCharDataLen + 1];
unsigned short m_wDCAPDnlBktNameBuffLen = 0;
unsigned short m_wDCAPDnlBktNumBuffLen = 0;

//static unsigned char m_bDCAPDnl_select_Buff[m_wMaxDcpDnlCharDataLen];
//static unsigned char m_bDCAPDnl_accVal_Buff[m_wMaxDcpDnlCharDataLen];
//static unsigned char m_bDCAPDnl_accCnt_Buff[m_wMaxDcpDnlCharDataLen];
//static unsigned char m_bDCAPDnl_accWgt_Buff[m_wMaxDcpDnlCharDataLen];
//static unsigned char m_bDCAPDnl_mulFac_Buff[m_wMaxDcpDnlCharDataLen];
//static unsigned short m_wDCAPDnl_select_BuffLen = 0;
//static unsigned short m_wDCAPDnl_accVal_BuffLen = 0;
//static unsigned short m_wDCAPDnl_accCnt_BuffLen = 0;
//static unsigned short m_wDCAPDnl_accWgt_BuffLen = 0;
//static unsigned short m_wDCAPDnl_mulFac_BuffLen = 0;

//static unsigned char m_bDCAPWgtBandLo[m_wMaxDcpDnlCharDataLen];
//static unsigned char m_bDCAPWgtBandlHi[m_wMaxDcpDnlCharDataLen]; 
//static unsigned char m_bDCAPWgtBandNo[m_wMaxDcpDnlCharDataLen];
//static unsigned short m_wDCAPWgtBandLoLen = 0;
//static unsigned short m_wDCAPWgtBandHiLen = 0;
//static unsigned short m_wDCAPWgtBandNoLen = 0;

//#define DCAP_PSTR_BUFF_LEN 20
//#define DCAP_PSTR_ERR               (XML_USER_ERROR_BASE + 400)
#define DCAP_UIC_BASE_LOCK_TBL_DNLD_ERR (XML_USER_ERROR_BASE + 401)

//static unsigned char bDCAPPrintedStringIndex = 0;
//static unsigned char bDCAPPrintedStringBuff[DCAP_PSTR_BUFF_LEN+1]; /* includes space for a null. */
//static short wDCAPPrintedStringLen = 0;



/***********************************
    Data Capture Upload
***********************************/


/*******************************************************************
m_pTempBuff - used by fake DCAP upload functions & fake Bob
function to create stuff.
*******************************************************************/
static char m_pTempBuff[50];



void *fnGetDCAPBobData (unsigned long ulDataID,
                    unsigned long ulDataItemNo,
                    unsigned char bFormatType, 
                    unsigned short *usSize)
{
    short bWeightUnits;
    unsigned char bMinDisplayedDecimals;
//  unsigned char pFirmware[5]="", pSvcLevel[6]="",pEffDate[7]="",pCarLevel[4]="";
    unsigned char bCurrencyType;

#if !defined(K700) && !defined(G900)
    unsigned char bVerWhole, bVerDec;
#endif

    *usSize = 0;
    
    switch (ulDataID) {
    case DCAPBOB_SERIALNUM:
        /* call bob to get the meter (PSD) serial number */
        if (fnValidData(BOBAMAT0, VLT_INDI_FILE_PBP_SERIAL, (void *) m_pTempBuff) != BOB_OK)
        {
            fnProcessSCMError();
            return NULL;
        }       
        *usSize = (unsigned short)strlen( m_pTempBuff );
        return m_pTempBuff;
        
    case DCAPBOB_MODELNUM:
        /* call bob to get the meter (PSD) pcn */
        if (fnValidData(BOBAMAT0, GET_VLT_METER_PCN, (void *) m_pTempBuff) != BOB_OK)
        {
            fnProcessSCMError();
            return NULL;
        }
        *usSize = (unsigned short)strlen( m_pTempBuff );
        return m_pTempBuff;
        
    case DCAPBOB_PBPACC:
        (void)memset(m_pTempBuff,0,sizeof(CMOSSetupParams.lwPBPAcctNumber)*2 + 1);
        fnPackedBcdToAsciiBcd((char*)m_pTempBuff,(const unsigned char *)&CMOSSetupParams.lwPBPAcctNumber,sizeof(CMOSSetupParams.lwPBPAcctNumber));
        *usSize = (unsigned short)strlen(m_pTempBuff);
        return m_pTempBuff;
        
    case DCAPBOB_EIUMOD:
        /* get the UIC PCN from CMOS. */
        (void)memset((void*)m_pTempBuff,0,sizeof(CMOSSignature.bUicPcn) + 1);       
        (void)strncpy(m_pTempBuff,CMOSSignature.bUicPcn,sizeof(CMOSSignature.bUicPcn));
        *usSize = (unsigned short)strlen(m_pTempBuff);
        return m_pTempBuff;
        
    case DCAPBOB_EIUVER:
        /* generate the version number string.  the number comes from a constant stored
        in version.c */
#if !defined(K700) && !defined(G900)
        bVerWhole = (wUICSoftwareVersion & 0xFF00) >> 8;
        bVerDec = (wUICSoftwareVersion & 0x00FF);
        *usSize = (unsigned short)sprintf(m_pTempBuff, "%01X.%02X", bVerWhole, bVerDec);        
        #else
#if defined(G900)
        // (ML) Format changed to xx.yy.zzzz
        *usSize = (unsigned short)sprintf(m_pTempBuff, "%s", UicVersionString);
#else
        fnGetPrettyUicVersionString(m_pTempBuff);
        *usSize = (unsigned short)strlen( m_pTempBuff);
        #endif
#endif
        return m_pTempBuff;
        
    case DCAPBOB_CARNAM:
        /* Note: figure out how to get this from the rates engine. */
        *usSize = 0;
        return NULL;
        
    case DCAPBOB_CAREFF:
        /* Note: figure out how to get this from the rates engine. */
        *usSize = 0;
        return NULL;
        
        
    case DCAPBOB_CARLEV:
        /* Note: figure out how to get this from the rates engine. */
        *usSize = 0;
        return NULL;
        
    case DCAPBOB_MTRPARCUR:
        if (fnValidData(BOBAMAT0, GET_VLT_ACTIVE_CURRENCY, (void *) &bCurrencyType) != BOB_OK)
        {
            fnProcessSCMError();
            return NULL;
        }
        (void)sprintf(m_pTempBuff,"%02d",bCurrencyType);
        *usSize = (unsigned short)strlen(m_pTempBuff);
        return m_pTempBuff;
        
    case DCAPBOB_MTRPARWGT:
//        bWeightUnits = fnRateGetWeightUnit(); // 0 = AVOIRDUPOIS, 1 = METRIC, -1 = WEIGHT_UNIT_NOT_AVAILABLE
//        if( bWeightUnits == -1 )
            bWeightUnits = 255; // rates manager not initialized - invalid weight unit
        (void)sprintf(m_pTempBuff,"%03d",bWeightUnits);     
        *usSize = (unsigned short)strlen(m_pTempBuff);
        return m_pTempBuff;
        
    case DCAPBOB_MTRPARDFM:     
        (void)strcpy(m_pTempBuff,"YYYYMMDD");
        *usSize = (unsigned short)strlen(m_pTempBuff);
        return m_pTempBuff;
        
    case DCAPBOB_MTRPARTFM:     
        (void)strcpy(m_pTempBuff,"hhmmss");
        *usSize = (unsigned short)strlen(m_pTempBuff);
        return m_pTempBuff;
        
    case DCAPBOB_MTRPARDPL:
        bMinDisplayedDecimals = fnFlashGetByteParm(BP_MIN_DISPLAYED_DECIMALS);
        (void)sprintf(m_pTempBuff, "%d", bMinDisplayedDecimals);
        *usSize = (unsigned short)strlen(m_pTempBuff);
        return m_pTempBuff;
        
    case RATE_INFO_ATTRIBUTE:       
        /* Note: figure out how to get this from the rates engine. */
        (void)sprintf(m_pTempBuff, "fwVer='0' svcLev='0'");
        *usSize = (unsigned short)strlen(m_pTempBuff);
        return m_pTempBuff;
    
    case DCE_INFO_ATTRIBUTE:
        (void)strcpy( m_pTempBuff, "fwVer=\"" );
/*
        if( fnIsDCapEngineAvailable() == TRUE )
        {
            (void)strncat( m_pTempBuff, (const char *)fnRateGetDCESWVersion(), LENVERSION );
        }
        else
            (void)strcat( m_pTempBuff, "0" ); 
*/

        (void)strcat( m_pTempBuff, "\"" );
        *usSize = (unsigned short)strlen( m_pTempBuff );
        
        return m_pTempBuff;

    case DCAPBOB_DCMNAM:
        (void)strcpy( m_pTempBuff, (const char *)CMOSRateCompsMap.CMOSRateComponents[DcmComponentList[ulDataItemNo]].SWPartNO ); 
        *usSize = (unsigned short)strlen( m_pTempBuff );
        return m_pTempBuff;

    case DCAPBOB_DCMLEV:
        (void)strcpy( m_pTempBuff, (const char *)CMOSRateCompsMap.CMOSRateComponents[DcmComponentList[ulDataItemNo]].ServiceLevel );    
        *usSize = (unsigned short)strlen( m_pTempBuff );
        return m_pTempBuff;
    
    case TRAN_REC_FILE_NAME:
    {
    	int size;

    	//trmGetFileName(m_pTempBuff, &size, true, false);
		trmGetDcapFileName(m_pTempBuff, &size, false);
		dbgTrace(DBG_LVL_INFO, "fnGetDCAPBobData: uplod fileName: %s, size:%d ", m_pTempBuff, size);
		*usSize = size;
		return m_pTempBuff;
    }
    default:
        return NULL;
    }
}


static char *m_pDcapBuff = NULL;
static int m_DcapBuffLen = 0;

void CleanDCAPBuffer(void)
{
	if(m_pDcapBuff != NULL)
	{
		NU_Deallocate_Memory(m_pDcapBuff);
		m_pDcapBuff = NULL;
	}

}

void *fnGetDCAPPkgData (unsigned long ulDataID,
                    unsigned long ulDataItemNo,
                    unsigned char bFormatType,
                    unsigned short *usSize)
{
    short bWeightUnits;
    unsigned char bMinDisplayedDecimals;
    unsigned char bCurrencyType;

    *usSize = 0;

    switch (ulDataID) {
    case TRAN_RECORD_PKG_FILE:
     {
     	unsigned char * buffer = NULL;
     	int len = 0;
 		int fd;
 		STATUS status = SUCCESS;
 		char commpFileName[TRM_MAX_FILE_NAME_SIZE];
 		char path[64];
 		int readLength;

 		len = sizeof(commpFileName);

		trmGetDcapFileName(commpFileName, &len, true);
		fnFilePath(path, "upld", commpFileName);

		dbgTrace(DBG_LVL_ERROR, "fnGetDCAPPkgData: compressed file %s \r\n", commpFileName);
 		fd = NU_Open(path, (PO_BINARY | PO_RDONLY), PS_IREAD);
 		if(fd < 0)
 		{
 			dbgTrace(DBG_LVL_ERROR, "fnGetDCAPPkgData: Failed to open compressed file:%s, fd:%d \r\n", commpFileName, fd);
 			fnCheckFileSysCorruption(fd);
 			status = 1;
 		}
 		else
 		{
 			if((len = NU_Seek(fd, 0, PSEEK_END)) < 0)
 				fnCheckFileSysCorruption(len);
 			dbgTrace(DBG_LVL_ERROR, "fnGetDCAPPkgData: Compressed file Length: %d\n", len);
 			status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &buffer, len , NU_NO_SUSPEND);
 			if(status == SUCCESS)
 			{
 				STATUS seekStatus = NU_Seek(fd, 0, PSEEK_SET );
 				if(seekStatus < 0)
 					fnCheckFileSysCorruption(seekStatus);

 				readLength = NU_Read(fd, (void*)buffer, len);
 				if ((readLength < 0) || (readLength != len))
 				{
 					fnCheckFileSysCorruption(readLength);
 					dbgTrace(DBG_LVL_ERROR, "fnGetDCAPPkgData: Failed to write compressed file: %s, writelength: %0x / %0x \r\n", commpFileName, readLength, len);
 					status = 2;
 				}
 				else
 				{
 					//allocate if necessary
					if(m_pDcapBuff != NULL)
					{
						NU_Deallocate_Memory(m_pDcapBuff);
						m_pDcapBuff = NULL;
					}
					m_DcapBuffLen =  ((len / 3) *4)  + 4 + 32;

					status = NU_Allocate_Memory(pSystemMemoryCached, ( void *) &m_pDcapBuff, m_DcapBuffLen, NU_NO_SUSPEND);

					if(m_pDcapBuff != NULL)
					{
						if (B64Encode((unsigned char*)buffer, len, m_pDcapBuff, m_DcapBuffLen, &readLength, TRUE) == CONVERSION_OK)
						{
							*usSize = (unsigned short)readLength;
							dbgTrace(DBG_LVL_ERROR, "fnGetDCAPPkgData: Compressed Len:%d, Base64 Length: %d\n", len, readLength);
						}
						else
						{
							*usSize = (unsigned short)sprintf(m_pDcapBuff," ");
							dbgTrace(DBG_LVL_ERROR, "fnGetDCAPPkgData:TRAN_REC Base64 conversion failed: %u \r\n", len);
						}
					}
					else
					{
						dbgTrace(DBG_LVL_ERROR, "fnGetDCAPPkgData: TRAN_REC-1 Failed to allocate memory: %u \r\n", m_DcapBuffLen);
						status = 3;
					}
 				}
 				NU_Deallocate_Memory(buffer);
 			}
 			else
 			{
 				dbgTrace(DBG_LVL_ERROR, "fnGetDCAPPkgData: TRAN_REC-2 Failed to allocate memory: %u \r\n", len);
 				status = 4;
 			}

 			//close file : need cleanup once upload is success
 			STATUS closeStatus = NU_Close(fd);
 			fnCheckFileSysCorruption(closeStatus);
 		}

 		if(status != SUCCESS)
 		{
 			dbgTrace(DBG_LVL_ERROR, "fnGetDCAPPkgData: Reading Upload PKG Failed-status: %d \r\n", status);

// 			fUploadInProgress = FALSE;
//			sendUploadProgress(UPLOAD_EVENT_TRX_UPLOAD_FAILED, 0, 0);
//			trmSetUploadState(TRM_NONE);
//			SendBaseEventToTablet(BASE_EVENT_TX_UPLOAD_FAILED, NULL);
 			trmUploadFailed(status);

			if(m_pDcapBuff != NULL)
			{
				NU_Deallocate_Memory(m_pDcapBuff);
				m_pDcapBuff = NULL;
			}
 		}

     	return m_pDcapBuff;
     }
     default:
         return NULL;
     }
}



short fnDCAPHandleDcpUplPerMgtETag( XmlChar *pName )
{
    DATETIME Today;
    int i;
    long lDateDiff;
    //char closedPeriodOffset = 0;
    BOOL bValidDate = FALSE;

    dbgTrace(0, "fnDCAPHandleDcpUplPerMgtETag");

    // Copy the end of period info to cmos now that we know that all of the data was received.

    if( bNewPeriodDatesDnlInProgress == TRUE )
    {
        bNewPeriodDatesDnlInProgress = FALSE;

            // Get today's date and normalize it
            (void)GetSysDateTime(&Today, ADDOFFSETS, GREGORIAN);
            Today = fnDCAP_PM_NormaliseDate( &Today);

            // we're actually going to use the downloaded dates, so save them.
            pParameters->Period.ucNumberOfPeriodsInList = ucPeriodIndex;
            pParameters->Period.ucPeriodListIndex = 0;

            for (i = 0; i < ucPeriodIndex; ++i )
            {
            	 bValidDate = CalcDateTimeDifference( &Today, &TempPeriodEndList[i], &lDateDiff );
            	 if ((bValidDate == FALSE) || (lDateDiff < 0))
                 {
            		 memset(&pParameters->Period.periodList[i], 0, sizeof(pParameters->Period.periodList[i]));
                 }
            	 else
            	 {
            		 pParameters->Period.periodList[i] = TempPeriodEndList[i];
            	 }
            }


			///////////////////////////////////////////////////////
			//TODO:Remove this once period mgmt is working
			for( int i = 0; i < ucPeriodIndex; i++)
			{
				dbgTrace(0, "Period.periodList: %d: %02d%02d-%02d-%02d %02d:%02d:%02d",
						i+1,
						pParameters->Period.periodList[i].bCentury,
						pParameters->Period.periodList[i].bYear,
						pParameters->Period.periodList[i].bMonth,
						pParameters->Period.periodList[i].bDay,
						pParameters->Period.periodList[i].bHour,
						pParameters->Period.periodList[i].bMinutes,
						pParameters->Period.periodList[i].bSeconds);
			}
    }


    return XML_STATUS_OK;
}

short fnDCAPHandleDcpPerEndSTag( XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr ) // TDT
{

    bNewPeriodDatesDnlInProgress = TRUE;


    return XML_STATUS_OK;
}

short fnDCAPHandleDcpPerEndETag( XmlChar *pName )
{
    if( ucPeriodIndex < MAX_PERIOD_END_DATES )
        ucPeriodIndex++;

    return XML_STATUS_OK;
}

short fnDCAPHandleDcpGraDaySTag( XmlChar *pName, XMLAttribute *pAttrList, short iNumAttr ) // TDT
{
	short sStatus = XML_STATUS_OK;

    return sStatus;
}


short fnDCAPHandleDcpGraDayChars( XmlChar *pName, XmlChar *pChars, unsigned long iLen ) // TDT
{
	unsigned char TempString[ m_wMaxDcpDnlCharDataLen + 1 ]; // allow for null terminator
	short sStatus = XML_STATUS_OK;

    if( iLen > m_wMaxDcpDnlCharDataLen )
        sStatus = XML_DATA_BUFF_ERR;
    else
    {
        (void)memcpy( TempString, pChars, (unsigned short)iLen );
        TempString[ iLen ] = '\0';
        ucGraceDays = (unsigned char)atoi( (char *)TempString );
    }

    return sStatus;
}

short fnDCAPHandleDcpGraDayETag( XmlChar *pName )
{
    pParameters->ucGraceDays = ucGraceDays;

    return XML_STATUS_OK;
}

short fnDCAPHandleDcpPerEndDatChars( XmlChar *pName, XmlChar *pChars, unsigned long iLen ) // TDT
{
	XmlChar *pIndex;
	XmlChar TempString[3];
	int iTemp;

    if( ucPeriodIndex < MAX_PERIOD_END_DATES )
    {
        pIndex = pChars;
        TempString[2] = '\0';

        (void)memcpy( TempString, pIndex, 2 );
        pIndex += 2;
        iTemp = atoi( (char*)TempString );
        TempPeriodEndList[ucPeriodIndex].bCentury = (unsigned char)iTemp;

        (void)memcpy( TempString, pIndex, 2 );
        pIndex += 2;
        iTemp = atoi( (char*)TempString );
        TempPeriodEndList[ucPeriodIndex].bYear = (unsigned char)iTemp;

        (void)memcpy( TempString, pIndex, 2 );
        pIndex += 2;
        iTemp = atoi( (char*)TempString );
        TempPeriodEndList[ucPeriodIndex].bMonth = (unsigned char)iTemp;

        (void)memcpy( TempString, pIndex, 2 );
        iTemp = atoi( (char*)TempString );
        TempPeriodEndList[ucPeriodIndex].bDay = (unsigned char)iTemp;

        TempPeriodEndList[ucPeriodIndex].bDayOfWeek = 1;
    }

    return XML_STATUS_OK;
}

short fnDCAPHandleDcpPerEndTimChars( XmlChar *pName, XmlChar *pChars, unsigned long iLen ) // TDT
{
	XmlChar *pIndex;
	XmlChar TempString[3];
	int iTemp;

    if( ucPeriodIndex < MAX_PERIOD_END_DATES )
    {
        pIndex = pChars;
        TempString[2] = '\0';

        (void)memcpy( TempString, pIndex, 2 );
        pIndex += 3;  // skip the colon ':'
        iTemp = atoi( (char*)TempString );
        TempPeriodEndList[ucPeriodIndex].bHour = (unsigned char)iTemp;

        (void)memcpy( TempString, pIndex, 2 );
        pIndex += 3;
        iTemp = atoi( (char*)TempString );
        TempPeriodEndList[ucPeriodIndex].bMinutes = (unsigned char)iTemp;

        (void)memcpy( TempString, pIndex, 2 );
        iTemp = atoi( (char*)TempString );
        TempPeriodEndList[ucPeriodIndex].bSeconds = (unsigned char)iTemp;
    }
    return XML_STATUS_OK;
}

short fnDCAPHandleNxtUplDatChars( XmlChar *pName, XmlChar *pChars, unsigned long iLen ) // TDT
{
    XmlChar *pIndex;
    XmlChar TempString[3];
    int iTemp;

    if( ucNextUploadDateIndex < MAX_NEXT_UPLOAD_DATES )
    {
        pIndex = pChars;
        TempString[2] = '\0';

        (void)memcpy( TempString, pIndex, 2 );
        pIndex += 2;
        iTemp = atoi( (char*)TempString );
        TempNextUploadDateList[ucNextUploadDateIndex].bCentury = (unsigned char)iTemp;

        (void)memcpy( TempString, pIndex, 2 );
        pIndex += 2;
        iTemp = atoi( (char*)TempString );
        TempNextUploadDateList[ucNextUploadDateIndex].bYear = (unsigned char)iTemp;

        (void)memcpy( TempString, pIndex, 2 );
        pIndex += 2;
        iTemp = atoi( (char*)TempString );
        TempNextUploadDateList[ucNextUploadDateIndex].bMonth = (unsigned char)iTemp;

        (void)memcpy( TempString, pIndex, 2 );
        iTemp = atoi( (char*)TempString );
        TempNextUploadDateList[ucNextUploadDateIndex].bDay = (unsigned char)iTemp;

        TempNextUploadDateList[ucNextUploadDateIndex].bDayOfWeek = 1;
    }

    return XML_STATUS_OK;
}

short fnDCAPHandleNxtUplTimChars( XmlChar *pName, XmlChar *pChars, unsigned long iLen ) // TDT
{
	XmlChar *pIndex;
	XmlChar TempString[3];
	int iTemp;

    if( ucNextUploadDateIndex < MAX_NEXT_UPLOAD_DATES )
    {
        pIndex = pChars;
        TempString[2] = '\0';

        (void)memcpy( TempString, pIndex, 2 );
        pIndex += 3;  // skip the colon ':'
        iTemp = atoi( (char*)TempString );
        TempNextUploadDateList[ucNextUploadDateIndex].bHour = (unsigned char)iTemp;

        (void)memcpy( TempString, pIndex, 2 );
        pIndex += 3;
        iTemp = atoi( (char*)TempString );
        TempNextUploadDateList[ucNextUploadDateIndex].bMinutes = (unsigned char)iTemp;

        (void)memcpy( TempString, pIndex, 2 );
        iTemp = atoi( (char*)TempString );
        TempNextUploadDateList[ucNextUploadDateIndex].bSeconds = (unsigned char)iTemp;
    }
    return XML_STATUS_OK;
}


int trmGetNextPeriod(void)
{
	DATETIME 		Today;
	int i = 0;
	long 			lDateDiff;
	bool 			bValidDate;

	(void)GetSysDateTime(&Today, ADDOFFSETS, GREGORIAN);

	if(pParameters->Period.ucNumberOfPeriodsInList <= MAX_PERIOD_END_DATES)
	{
		for (i = 0; i < pParameters->Period.ucNumberOfPeriodsInList; ++i )
		{
			 bValidDate = CalcDateTimeDifference( &Today, &pParameters->Period.periodList[i], &lDateDiff );
			 if ((bValidDate == FALSE) || (lDateDiff < 0))
			 {
				 memset(&pParameters->Period.periodList[i], 0, sizeof(pParameters->Period.periodList[i]));
			 }
			 else
			 {
				 pParameters->Period.ucPeriodListIndex = i;
				 break;
			 }
		}
	}

    if( (pParameters->Period.ucPeriodListIndex < 0 || pParameters->Period.ucPeriodListIndex > 3) ||(bValidDate == FALSE) || (lDateDiff < 0))
    	pParameters->Period.ucPeriodListIndex = 0;

    dbgTrace(DBG_LVL_INFO, "trmGetNextPeriod- Upload Period.periodList[%d] : %02d%02d-%02d-%02d %02d:%02d:%02d ", pParameters->Period.ucPeriodListIndex,
    								pParameters->Period.periodList[pParameters->Period.ucPeriodListIndex].bCentury,
    								pParameters->Period.periodList[pParameters->Period.ucPeriodListIndex].bYear,
    								pParameters->Period.periodList[pParameters->Period.ucPeriodListIndex].bMonth,
    								pParameters->Period.periodList[pParameters->Period.ucPeriodListIndex].bDay,
    								pParameters->Period.periodList[pParameters->Period.ucPeriodListIndex].bHour,
    								pParameters->Period.periodList[pParameters->Period.ucPeriodListIndex].bMinutes,
    								pParameters->Period.periodList[pParameters->Period.ucPeriodListIndex].bSeconds);


	return pParameters->Period.ucPeriodListIndex;
}


/************************************************************************************************************/
//                                                                           
// The following macros are used to define the scripts that are used by the  
// formatter.  The macros are an attempt to simplify the scripts by just     
// declaring formal parameters that are compile time variables within the    
// context of a given script.  Thus, all the other structure members of the  
// scripts that are constant within the context of a given script are        
// "hidden" within the body of the macro definition and don't have to be     
// repeated everytime a script is declared.                                  
//
//
//       definition of a script entry
//      -------------------------------
// typedef struct tagXMLScriptEntry {
//  const short iCommand;               // the command 
//  const unsigned long iDefaultCount;  // how many times to execute entry 
//  const BOOL fStartWithZero;          // counter goes 0 - (N-1) instead of 1 - N 
//  unsigned long (*fcnGetCount)(unsigned char);    // fcn to get count that replaces default count 
//  XmlChar *pDataTag;                  // tag used w/ Retrieve N Tag command 
//  const unsigned long ulDataID;       // ID to get data & attributes 
//  void* (*fcnGetData)(unsigned long ulID, unsigned long index, unsigned char bFormat, unsigned short *pSize);
//  void* (*fcnGetAttrString)(unsigned long ulID, unsigned long index, unsigned char bFormat, unsigned short *pSize);
//  void *pSubscript;                   // what script for a MK_SCRIPT command 
//  const BOOL fResetCntr;              // call pre counter update fcn ? 
//  const BOOL fIncCntr;                // call pre counter update fcn ? 
//  const BOOL fInheritCntr;            // do we use the parent ? 
//  const BOOL fUseCountAsBool;         // the count fcn will return a number, if > 0 we execute subscript, 
//                                      //  else don't execute subscript 
//  const BOOL fUseDiffAttrID;
//  const unsigned long ulAttrID;
//  unsigned char ucCounter;            // counter used in fcnGetData & fcnGetAttrString calls 
// } XMLScriptEntry;
//
//     definition of a script
//     -----------------------
// typedef struct tagXMLFormatScript {
//  const short iEntries;               // number of entries in pEntry 
//  XmlChar *pTag;                      // (required) tag for script 
//  unsigned long ulDataID;             // data id for get attr fcn 
//  void* (*fcnGetAttrString)(unsigned long ulID, unsigned long index, unsigned char bFormat, unsigned short *pSize);
//  const BOOL fShowEmptyElemTag;       // for empty scripts (count = 0) use EmptyElemTag instead of STag ETag 
//  XMLScriptEntry *pEntry;             // pointer to entries 
//  const BOOL fResetCntr;              // call pre counter update fcn ? 
//  const BOOL fIncCntr;                // call pre counter update fcn ? 
//  const BOOL fUseParentCtr;           // do we use parent counter 
//  const BOOL fStartWithZero;          // counter goes 0 - (N-1) instead of 1 - N 
//  unsigned char ucCounter;            // counter used in fcnGetAttrString calls 
// } XMLFormatScript;
//
/************************************************************************************************************/  

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpointer-sign"

XMLFORM_SIMPLE_SCRIPT( DCAPUplScript, FileXferInd, 2, pDCAPUplEntry )

XMLFORM_SIMPLE_SCRIPT( DCAPUplScript2, DcpUpl, 2, pDCAPUplEntry2 )

//XMLFORM_SIMPLE_SCRIPT_WITH_ATTR( DcpDevScript, DcpDev, DATA_CAPTURE_TYPE, fnDCAPGetData, 5, pDcpDevScript )
XMLFORM_SIMPLE_SCRIPT( DcpDevScript, DcpDev, 5, pDcpDevScript )


XMLFORM_SIMPLE_SCRIPT( FileInfoScript, FileInfo, 2, pFileInfoScript )



XMLFORM_SIMPLE_SCRIPT_WITH_ATTR( DceInfScript, DceInf, DCE_INFO_ATTRIBUTE, fnGetDCAPBobData, 1, pDceInfScript )

XMLFORM_SIMPLE_SCRIPT_PARENT_CNT( DcmScript, DceInfDcm, 2, pDcmScript )

XMLFORM_SIMPLE_SCRIPT( MtrEiuScript, MtrEiu, 2, pMtrEiuScript )

XMLFORM_SIMPLE_SCRIPT_WITH_ATTR_PARENT_CNT( MtrIdxScript, MtrIdx, CURRENT_INDEX_PERIOD_ATTR, fnDCAPGetData, 2, pMtrIdxScript )

XMLFORM_SIMPLE_SCRIPT_PARENT_CNT( DcpPerBegScript, DcpPerBeg, 1, pDcpPerBegScript )

XMLFORM_SIMPLE_SCRIPT_PARENT_CNT( DcpPerEndScript, DcpPerEnd, 1, pDcpPerEndScript )


XMLFORM_SIMPLE_SCRIPT(TranRecScript, FileData, 1, pTransRecEntry)


_XMLFORM_START_SCRIPT_ENTRIES( pDCAPUplEntry )
    _XMLFORM_SIMPLE_TAG( SesIdCur, CURRENT_SESSION_ID, fnPBPS_GetData )
    _XMLFORM_SIMPLE_SUBSCRIPT( DCAPUplScript2 )
_XMLFORM_END_SCRIPT_ENTRIES()



_XMLFORM_START_SCRIPT_ENTRIES( pDCAPUplEntry2 )
    _XMLFORM_SIMPLE_SUBSCRIPT( DcpDevScript )
    _XMLFORM_SIMPLE_SUBSCRIPT( FileInfoScript )
    //_XMLFORM_SUBSCRIPT_WITH_CNT_CTRL( MtrEvtScript, fnDCAPGetNumEvents, FALSE, FALSE, FALSE, FALSE, TRUE )
    //_XMLFORM_SIMPLE_SUBSCRIPT( MtrParScript )
//    _XMLFORM_SUBSCRIPT_WITH_CNT_CTRL( DcpPerScript, fnDCAPGetNumPeriodsToUpload, FALSE, TRUE, TRUE, FALSE, FALSE )
//    _XMLFORM_SUBSCRIPT_WITH_CNT_CTRL( MtrIdxScript, fnDCAPGetNumMtrIdxToUpload, TRUE, TRUE, TRUE, FALSE, FALSE )
//    _XMLFORM_SUBSCRIPT_WITH_CNT_CTRL( AdjLogScript, fnDCAPGetNumAdjLogsToUpload, TRUE, TRUE, TRUE, FALSE, FALSE )
//    _XMLFORM_SUBSCRIPT_WITH_CNT_CTRL( MtrBasLogScript, fnDCAPGetNumBasesInLog, FALSE, FALSE, FALSE, FALSE, TRUE )
//    _XMLFORM_SIMPLE_OPT_SUBSCRIPT( fnDCAPDoTecSuperLogUpload, TecSupDtaScript )
//    _XMLFORM_SIMPLE_OPT_SUBSCRIPT( fnDCAPDoPstDatRegUpload, PstDatRegScript )
    //_XMLFORM_SIMPLE_OPT_SUBSCRIPT( fnASRASRCmosLogUploadRequired, AdsBktScript )
    //_XMLFORM_SUBSCRIPT_WITH_CNT_CTRL( AdsPerScript, fnASRNbFFSLogToUpload, TRUE, TRUE, TRUE, TRUE, FALSE  )
_XMLFORM_END_SCRIPT_ENTRIES()


_XMLFORM_START_SCRIPT_ENTRIES( pDcpDevScript )
    _XMLFORM_SIMPLE_TAG( MtrIdtSer,     DCAPBOB_SERIALNUM,  fnGetDCAPBobData )
    _XMLFORM_SIMPLE_TAG( MtrIdtMod,     DCAPBOB_MODELNUM,   fnGetDCAPBobData )
    _XMLFORM_SIMPLE_TAG( CusPbpAcc,     DCAPBOB_PBPACC,     fnGetDCAPBobData )
   // _XMLFORM_SIMPLE_TAG( DcpRulVer,     RULES_VERSION,      fnDCAPGetData )
    _XMLFORM_SIMPLE_SUBSCRIPT( MtrEiuScript )
    _XMLFORM_SIMPLE_TAG( CusPbpAcc,     DCAPBOB_PBPACC,     fnGetDCAPBobData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pFileInfoScript )
    _XMLFORM_SIMPLE_TAG( FileName,     TRAN_REC_FILE_NAME,  fnGetDCAPBobData )
	_XMLFORM_SIMPLE_TAG( FileData,     TRAN_RECORD_PKG_FILE,  fnGetDCAPPkgData )
    //_XMLFORM_SIMPLE_SUBSCRIPT( TranRecScript )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pDcmScript )
    _XMLFORM_SIMPLE_TAG_PARENT_CNT( DceInfDcmNam, DCAPBOB_DCMNAM, fnGetDCAPBobData )
//  _XMLFORM_SIMPLE_TAG_PARENT_CNT( DceInfCarEff, DCAPBOB_CAREFF, fnGetDCAPBobData )
    _XMLFORM_SIMPLE_TAG_PARENT_CNT( DceInfDcmLev, DCAPBOB_DCMLEV, fnGetDCAPBobData )
_XMLFORM_END_SCRIPT_ENTRIES()

_XMLFORM_START_SCRIPT_ENTRIES( pMtrEiuScript )
    _XMLFORM_SIMPLE_TAG( MtrEiuMod,     DCAPBOB_EIUMOD,     fnGetDCAPBobData )
    _XMLFORM_SIMPLE_TAG( MtrEiuVer,     DCAPBOB_EIUVER,     fnGetDCAPBobData )
_XMLFORM_END_SCRIPT_ENTRIES()


#pragma GCC diagnostic pop

