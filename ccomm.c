/************************************************************************
 *  PROJECT:        Horizon CSD
 *  MODULE NAME:    ccomm.c
 *  
 *  DESCRIPTION:    Entry point for serial communication tasks.  Serial
 *                  LISR and HISR routines.
 *
 * ----------------------------------------------------------------------
 *              Copyright (c) 2016 Pitney Bowes Inc.
 *                   37 Executive Drive
 *                  Danbury, Connecticut  06810
 * ----------------------------------------------------------------------
 *  
 *************************************************************************/
#include <stdio.h>
#include "ossetup.h"
#include "pbos.h"
#include "commglob.h"
#include "sac.h"
#include "bob.h"
#include "stdio.h"  // for syslog entries using sprintf
#include "cm.h"  //for PC_AUTOSTART
#include "settingsmgr.h"


/******************************************** COMET *********************************************/
/************************************************************************************************/
extern void JanusLinkLayerReceive(unsigned char bChar,COMMDEVICE *dev);
extern void fnPlatformLinkLayerReceive(unsigned char bChar,COMMDEVICE *dev);
extern SetupParams CMOSSetupParams;

unsigned long lwRXISRIn;
unsigned long lwRXISROut;
unsigned long lwTXISRIn;
unsigned long lwTXISROut;


#define DUMMYUART 0L
#define DUMMYUARTRX 76  //We are stealing the ADC interrupt as a dummy vector used with the UART

extern UARTCONTROL NSDUart;

/* Declare Lisr's */


//TODO - remove dummy key data
unsigned char dummyKeyBuf[12];




/* **********************************************************************
// FUNCTION NAME: LCDCommTask
// PURPOSE:
//
// AUTHOR: 
//
// INPUTS:
// **********************************************************************/
//TODO - AB - after terminal IO in here is no longer needed, remove this whole task
void LCDCommTask (unsigned long argc, void *argv) 
{
  volatile int loop = 0;
  unsigned char data[2];
  unsigned int pv = 0;


  while(1)
    {
	  OSWakeAfter(1000);
	  switch (loop)
	  {
	  case 1:
          (void)smgr_set_setting_data(SETTING_ITEM_POSTAGE, &pv, sizeof(UINT32));

		  data[0] = 0;
		  data[1] = 0;
		  (void)OSSendIntertask(CM, BJCTRL, PC_AUTOSTART, BYTE_DATA, &data, 2);
		  loop++;
		  break;

	  case 3:
		  data[0] = 0;
		  data[1] = 0;
		  (void)OSSendIntertask(BJCTRL, BJSTATUS, 0x80, NO_DATA, 0, 0);
		  loop++;
		  break;

	  default:
		  break;
	  }
    }
}


