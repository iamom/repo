//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EMD Handlers
//
// triggered from the webserver interface from message format:
//  MsgID:      "GetEMDDataReq"
//  "Class"     msg class - "UIC", "UIC-EXTENDED", "PSD"
//  "Type"      data type - "BYTE", "WORD", "LONG", "MONEY", "ASCII", "UNICODE", "PACKED"
//  "PARAMID"   Numeric value of the paraeter required pased as a text string, ie "11"
//

#include "nucleus.h"
#include "networking/nu_networking.h"
#include "commontypes.h"
#include "datdict.h"
#include "cJSON.h"
#include "custdat.h"
#include "ipsd.h"
#include "bob.h"
#include "bobutils.h"
#include "ibobutils.h"
#include "xmltypes.h"
#include "flashutil.h"
#include "fwrapper.h"
#include "utils.h"
#include "wsox_tx_queue.h"
#include "wsox_services.h"
#include "wsox_EMDHandlers.h"
#include "unistring.h"

#define CRLF "\r\n"

extern BOOL NoEMDPresent;

unsigned char   *globalBase64OutBuffer = NULL;


// For the IPSD, each pair is (the minimum ID, the MaxID +1). So if they are the same, 
//  then there are no valid Parameter IDs for that type.
const EMDLimits kIPSDByteParamLimits    = {0x70, BAD_MFG_VAULT_IPSD_BYTE_ID};
const EMDLimits kIPSDWordParamLimits    = {0x70, BAD_MFG_VAULT_IPSD_WORD_ID};
const EMDLimits kIPSDLongParamLimits    = {0x70, BAD_MFG_VAULT_IPSD_LONG_ID};
const EMDLimits kIPSDMoneyParamLimits   = {0x70, BAD_MFG_VAULT_IPSD_MONY_ID};
const EMDLimits kIPSDASCIIParamLimits   = {0x70, BAD_MFG_VAULT_IPSD_STRING_ID};
const EMDLimits kIPSDUNICODEParamLimits = {0x70, BAD_MFG_VAULT_IPSD_UNICOD_ID};
const EMDLimits kIPSDPackedParamLimits  = {0x70, BAD_MFG_VAULT_IPSD_PACKED_ID};
const EMDLimits kIPSDMulitPackedParamLimits = {0x70, BAD_MFG_VAULT_IPSD_MULTI_PACKED_ID};

                                                  
const EMDLimits kUICByteParamLimits     = {0x00, MAX_BYTE_PARM};
const EMDLimits kUICWordParamLimits     = {0x00, MAX_WORD_PARM};
const EMDLimits kUICLongParamLimits     = {0x00, MAX_LONG_PARM};
const EMDLimits kUICMoneyParamLimits    = {0x00, MAX_MONETARY_PARM};
const EMDLimits kUICASCIIParamLimits    = {0x00, MAX_ASCII_STR_PARM};
const EMDLimits kUICUNICODEParamLimits  = {0x00, MAX_UNICODE_PARM};
const EMDLimits kUICPackedParamLimits   = {0x00, MAX_PACKED_BYTE_PARM};

const EMDLimits kUICExByteParamLimits   = {0x00, MAX_IBYTE_PARM};
const EMDLimits kUICExWordParamLimits   = {0x00, MAX_IWORD_PARM};
const EMDLimits kUICExLongParamLimits   = {0x00, MAX_ILONG_PARM};
const EMDLimits kUICExPackedParamLimits = {0x00, MAX_IPACKED_PARM};
const EMDLimits kUICExMultiPackedParamLimits = {0x00, MAX_IMULTIPACKED_PARM};

required_fields_tbl_t required_fields_tbl_GetEMDGroupReq =
    {
    "GetEMDGroupReq", {      EMD_KEY_GROUP
                           , NULL
                    }
    };

required_fields_tbl_t required_fields_tbl_GetEMDDataReq =
    {
    "GetEMDDataReq", {      EMD_KEY_CLASS
                           , EMD_KEY_TYPE
                           , EMD_KEY_PARAMID
                           , NULL
                    }
    };

// on_GetEMDGroupReq message values
//
// this takes an array of individual request messages in the format of on_GetEMDDataReq
//
// if any fail this returns an error message accounting why otherwise it returns an array of result messages
//
void on_GetEMDGroupReq(UINT32 handle, cJSON *root)
{
    char    strError[EMD_MAX_ERRMSGLEN] ;
    INT     handled     = EMD_UNHANDLED ;
    cJSON   *rspMsg     = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetEMDGroupRsp");

    if(globalBase64OutBuffer == NULL)
    {
        globalBase64OutBuffer = (unsigned char *)malloc(EMD_MAX_OUTBUFF_SIZE+1) ;
        if(globalBase64OutBuffer == NULL)
        {
            snprintf(strError, EMD_MAX_ERRMSGLEN, "Insufficient memory error, unable to allocate Base64 output buffer") ;
            cJSON_AddStringToObject(rspMsg, "Error", strError);
        }
    }

    if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_GetEMDGroupReq) && (NoEMDPresent != TRUE))
    {
        INT iArrCount = 0 ;
        INT iSuccessCount = 0 ;
        char *sClass    = NULL;
        char *sType     = NULL;
        char *sParmID   = NULL;

        cJSON *srcArray = cJSON_GetObjectItem(root, EMD_KEY_GROUP);
        if(srcArray && ((iArrCount = cJSON_GetArraySize(srcArray)) > 0))
        {
            INT iArrIter = 0 ;
            cJSON *dstArray = cJSON_CreateArray();
            for(iArrIter = 0 ; (iArrIter < iArrCount) && dstArray ; iArrIter++)
            {
                cJSON *srcEle = cJSON_GetArrayItem(srcArray, iArrIter) ;
                cJSON *dstEle = cJSON_CreateObject();
                if(srcEle && dstEle)
                {
                    if (RequiredFieldsArePresent(srcEle, dstEle, &required_fields_tbl_GetEMDDataReq))
                    {
	                    sClass  = cJSON_GetObjectItem(srcEle, EMD_KEY_CLASS)->valuestring;
	                    sType   = cJSON_GetObjectItem(srcEle, EMD_KEY_TYPE)->valuestring;
	                    sParmID = cJSON_GetObjectItem(srcEle, EMD_KEY_PARAMID)->valuestring;

	                    handled = handleJSONMessage(srcEle, dstEle) ;
                    }

                    if(handled)
                    {
                        cJSON_AddStringToObject(dstEle, EMD_KEY_CLASS,   sClass) ;
                        cJSON_AddStringToObject(dstEle, EMD_KEY_TYPE,    sType) ;
                        cJSON_AddStringToObject(dstEle, EMD_KEY_PARAMID, sParmID) ;
                        cJSON_AddItemToArray(dstArray, dstEle) ;
                        iSuccessCount++;
						handled = EMD_UNHANDLED ;
                    }
                    else
                    {
                        if(dstEle)
                            cJSON_Delete(dstEle);
                        break ;
                    }
                }
            } // end of array iteration
            if(iSuccessCount == iArrCount)
            {
                handled = EMD_HANDLED;
                cJSON_AddItemToObject(rspMsg, EMD_KEY_GROUP, dstArray) ;
            }
            else
            {
                char strErr[100];
                handled = EMD_HANDLED;
                cJSON_Delete(dstArray);
                snprintf(strErr, 100, "Error with Class:%s Type:%s Param:%s", sClass, sType, sParmID);
                cJSON_AddStringToObject(rspMsg, "Error", strErr);
                dbgTrace(DBG_LVL_INFO, "Unable to complete on_GetEMDGroupReq, failed to handle %s", strErr);
            }
        } // if(srcArray
    }

    if(handled == EMD_UNHANDLED)
    {
        /* an error has been added to rspMsg by RequiredFieldsArePresent() */
        cJSON_AddStringToObject(rspMsg, "Error", "Unhandled request");
    }
    addEntryToTxQueue(&entry, root, "on_GetEMDGroupReq: Added error response to tx queue." CRLF);
}




// on_GetEMDDataReq message values
// Class =
//          PSD
//          IBUTTON
//          UIC
//
//  Type =
//          BOOL
//          BYTE
//          WORD
//          LONG
//          MONETRY
//          PACKED
//          STRING
//          UINCODE
//          MULTI
//
//  PARAMID =
//          <numeric value - 1 byte 0x00 - 0xFF>
//
///
// Routine will return original message with additional
//  Value = <value of requested param> - OPTIONAL only present if success
//  Error = <details of error if one occurred> - OPTIONAL only present if error occurred.

void on_GetEMDDataReq(UINT32 handle, cJSON *root)
{
    char    strError[EMD_MAX_ERRMSGLEN] ;
    INT     handled     = EMD_UNHANDLED ;
    cJSON   *rspMsg     = cJSON_CreateObject();
    WSOX_Queue_Entry entry  =  { WSOX_QET_CJSON, handle, {.cjson_info = {rspMsg}}};

    cJSON_AddStringToObject(rspMsg, "MsgID", "GetEMDDataRsp");

    if(globalBase64OutBuffer == NULL)
    {
        globalBase64OutBuffer = (unsigned char *)malloc(EMD_MAX_OUTBUFF_SIZE+1) ;
        if(globalBase64OutBuffer == NULL)
        {
            snprintf(strError, EMD_MAX_ERRMSGLEN, "Insufficient memory error, unable to allocate Base64 output buffer") ;
            cJSON_AddStringToObject(rspMsg, "Error", strError);
        }
    }

    if (RequiredFieldsArePresent(root, rspMsg, &required_fields_tbl_GetEMDDataReq) && (NoEMDPresent != TRUE))
    {
        cJSON_AddStringToObject(rspMsg, EMD_KEY_CLASS, cJSON_GetObjectItem(root, EMD_KEY_CLASS)->valuestring);
        cJSON_AddStringToObject(rspMsg, EMD_KEY_TYPE, cJSON_GetObjectItem(root, EMD_KEY_TYPE)->valuestring);
        cJSON_AddStringToObject(rspMsg, EMD_KEY_PARAMID, cJSON_GetObjectItem(root, EMD_KEY_PARAMID)->valuestring);

        handled = handleJSONMessage(root, rspMsg) ;
    }

    //If EMD value is not returned, sometimes the error info is added into the response, but 
    //sometimes no error info. So add a common error info.
    if(handled == EMD_UNHANDLED && (cJSON_HasObjectItem(rspMsg, "Error")==0))
    {
        /* an error has been added to rspMsg by RequiredFieldsArePresent() */
        cJSON_AddStringToObject(rspMsg, "Error", "Unhandled request");
    }
    addEntryToTxQueue(&entry, root, "on_GetEMDDataReq: Added error response to tx queue." CRLF);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
INT handlePSDMessage( UINT16 uParmID, char *sType, cJSON *rspMsg )
{
    INT     retValue = EMD_UNHANDLED ;
    char    strError[EMD_MAX_ERRMSGLEN] ;


    snprintf( strError, EMD_MAX_ERRMSGLEN, "No PSD parameters. Try IBUTTON" );
    cJSON_AddStringToObject( rspMsg, "Error", strError );
    return retValue;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
INT handleJSONMessage(cJSON *srcMsg, cJSON *rspMsg)
{
    INT handled     = EMD_UNHANDLED;
    char *sClass    = cJSON_GetObjectItem(srcMsg, EMD_KEY_CLASS)->valuestring;
    char *sType     = cJSON_GetObjectItem(srcMsg, EMD_KEY_TYPE)->valuestring;
    char *sParmID   = cJSON_GetObjectItem(srcMsg, EMD_KEY_PARAMID)->valuestring;


    // sID should be an unsigned short
    if(sParmID != NULL)
    {
        UINT16 uParmID = (UINT16)atoi(sParmID);

        // mind the ordering here since UIC is a substring of UIC-EXTENDED
        if(strncmp(sClass, EMD_CLASS_UIC_EXTENDED, strlen(EMD_CLASS_UIC_EXTENDED)) == 0)
        {
            handled = handleUICExtendedMessage(uParmID, sType, rspMsg) ;
        }
        else if(strncmp(sClass, EMD_CLASS_UIC, strlen(EMD_CLASS_UIC)) == 0)
        {
            handled = handleUICMessage(uParmID, sType, rspMsg) ;
        }
        else if(strncmp(sClass, EMD_CLASS_IBUTTON, strlen(EMD_CLASS_IBUTTON)) == 0)
        {
            handled = handleIPSDMessage( uParmID, sType, rspMsg ) ;
        }
        else if(strncmp(sClass, EMD_CLASS_PSD, strlen(EMD_CLASS_PSD)) == 0)
        {
            handled = handlePSDMessage( uParmID, sType, rspMsg ) ;
        }
    }
    return handled;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
INT handleIPSDMessage( UINT16 uParmID, char *sType, cJSON *rspMsg )
{
    INT     retValue = EMD_UNHANDLED ;
    char    strError[EMD_MAX_ERRMSGLEN] ;
    UINT16  *pLength;
    MONEY   *pMoney ;
    UINT32  *pLong;
    UINT16  *pWord;
    UINT8   *pByte;
    char    *pAscii;
    double  dMoney;                          

    if( globalBase64OutBuffer != NULL )
    {   // Check the type:
        if( strncmp(sType,EMD_TYPE_BYTE, strlen(EMD_TYPE_BYTE)) == 0 )
        {   // Check the ParamID is in range for type BYTE:
            if( (uParmID >= kIPSDByteParamLimits.lower) && (uParmID < kIPSDByteParamLimits.upper) )
            {
                if( fnParameterJanitor( MFG_BYTE_READ, uParmID, (void**) &pByte )        // Source
                    == BOB_OK)
                {
                    retValue = EMD_HANDLED;
                    snprintf(strError, EMD_MAX_ERRMSGLEN, "%u", *pByte);
                    cJSON_AddStringToObject(rspMsg, "Value", strError);
                }
                else
                {
                    snprintf( strError, EMD_MAX_ERRMSGLEN, "BOB SICK error" );
                    cJSON_AddStringToObject( rspMsg, "Error", strError );
                }
            }
            else
            {
                snprintf( strError, EMD_MAX_ERRMSGLEN, "No IPSD Byte Param[%d]", uParmID );
                cJSON_AddStringToObject(rspMsg, "Error", strError);
            }
        }
        else if( strncmp(sType,EMD_TYPE_WORD, strlen(EMD_TYPE_WORD)) == 0 )
        {   // Check the ParamID is in range for type WORD:
            if( (uParmID >= kIPSDWordParamLimits.lower) && (uParmID < kIPSDWordParamLimits.upper) )
            {
                if( fnParameterJanitor( MFG_WORD_READ, uParmID, (void**) &pWord )        // Source
                    == BOB_OK)
                {
                    UINT16  u16Res = 0 ;
                    retValue = EMD_HANDLED;
                    u16Res = EndianAwareGet16((const void *) pWord);
                    snprintf(strError, EMD_MAX_ERRMSGLEN, "%u", u16Res);
                    cJSON_AddStringToObject(rspMsg, "Value", strError);
                }
                else
                {
                    snprintf( strError, EMD_MAX_ERRMSGLEN, "BOB SICK error" );
                    cJSON_AddStringToObject( rspMsg, "Error", strError );
                }
            }
            else
            {
                snprintf( strError, EMD_MAX_ERRMSGLEN, "No IPSD Word Param[%d]", uParmID );
                cJSON_AddStringToObject(rspMsg, "Error", strError);
            }
        }
        else if( strncmp(sType,EMD_TYPE_LONG, strlen(EMD_TYPE_LONG)) == 0 )
        {   // Check the ParamID is in range for type LONG:
            if( (uParmID >= kIPSDLongParamLimits.lower) && (uParmID < kIPSDLongParamLimits.upper) )
            {
                if( fnParameterJanitor( MFG_LONG_READ, uParmID, (void**) &pLong )
                    == BOB_OK)
                {
                    UINT32  u32Res = 0 ;
                    retValue = EMD_HANDLED;
                    // NOTE: Some Bob data is big endian and some is little endian.  The checking below relies on knowing which
                    // parameters are big endian and which are little endian.  This information is in the pMapGlobVarToBobVar table.
                    if (uParmID == MFG_IPSDZEROPOSTAGEPC 
                     || uParmID == MFG_IPSDDATEADVANCEDLIMIT
                     || uParmID == MFG_IPSDBACKDATELIMIT)
                    {
                        u32Res = *pLong;
                    }
                    else
                    {
                        u32Res = EndianAwareGet32((const void *) pLong);
                    }

                    if (fnIsSignedParamValue(EMD_CLASS_UIC_EXTENDED, EMD_TYPE_LONG, uParmID))
                        snprintf(strError, EMD_MAX_ERRMSGLEN, "%d", (INT32)u32Res);
                    else
                        snprintf(strError, EMD_MAX_ERRMSGLEN, "%u", u32Res);

                    cJSON_AddStringToObject(rspMsg, "Value", strError);
                }
                else
                {
                    snprintf( strError, EMD_MAX_ERRMSGLEN, "BOB SICK error" );
                    cJSON_AddStringToObject( rspMsg, "Error", strError );
                }
            }
            else
            {
                snprintf( strError, EMD_MAX_ERRMSGLEN, "No IPSD Long Param[%d]", uParmID) ;
                cJSON_AddStringToObject( rspMsg, "Error", strError );
            }
        }
        else if(strncmp( sType, EMD_TYPE_MONEY,  strlen(EMD_TYPE_MONEY)) == 0 )
        {// Check the ParamID is in range for type MONEY:
            if( (uParmID >= kIPSDMoneyParamLimits.lower) && (uParmID < kIPSDMoneyParamLimits.upper) )
            {
                // Get ptr to Parameter length, ptr to data, and decimal places
                if(   (fnParameterJanitor( MFG_MONETARY_LEN, uParmID, (void**) &pLength ) == BOB_OK)
                   && (fnParameterJanitor( MFG_MONETARY_READ, uParmID, (void**) &pMoney ) == BOB_OK) )
                {
                    if( *pLength == 5 )
                    {					  
                      dMoney = fnBinFive2Double( (unsigned char*)pMoney );
                      retValue = EMD_HANDLED ;

                      snprintf(strError, EMD_MAX_ERRMSGLEN, "%.f", dMoney);
                      cJSON_AddStringToObject(rspMsg, "Value", strError);
                    }
                }
                else
                {
                    snprintf( strError, EMD_MAX_ERRMSGLEN, "BOB SICK error" );
                    cJSON_AddStringToObject( rspMsg, "Error", strError );
                }
            }
            else
            {
                snprintf( strError, EMD_MAX_ERRMSGLEN, "No IPSD Money Param[%d]", uParmID) ;
                cJSON_AddStringToObject( rspMsg, "Error", strError );
            }
        }
        else if(strncmp( sType, EMD_TYPE_ASCII,  strlen(EMD_TYPE_ASCII)) == 0 )
        {// Check the ParamID is in range for type MONEY:
            if( (uParmID >= kIPSDASCIIParamLimits.lower) && (uParmID < kIPSDASCIIParamLimits.upper) )
            {
                if(   (fnParameterJanitor( MFG_STRING_LEN, uParmID, (void**) &pLength ) == BOB_OK)
                   && (fnParameterJanitor( MFG_STRING_READ, (UINT8)uParmID, (void**) &pAscii )  == BOB_OK) )
                {
                    retValue = EMD_HANDLED;
                    memcpy( globalBase64OutBuffer, pAscii, *pLength );
                    globalBase64OutBuffer[ *pLength ] = 0;
                    cJSON_AddStringToObject( rspMsg, "Value", pAscii );
                }
                else
                {
                    snprintf( strError, EMD_MAX_ERRMSGLEN, "BOB SICK error" );
                    cJSON_AddStringToObject( rspMsg, "Error", strError );
                }
            }
            else
            {
                snprintf(strError, EMD_MAX_ERRMSGLEN, "No IPSD ASCII Param[%d]", uParmID) ;
                cJSON_AddStringToObject(rspMsg, "Error", strError);
            }
        }
        else if(strncmp( sType, EMD_TYPE_PACKED,  strlen(EMD_TYPE_PACKED)) == 0 )
        {// Check the ParamID is in range for type MONEY:
            if( (uParmID >= kIPSDPackedParamLimits.lower) && (uParmID < kIPSDPackedParamLimits.upper) )
            {
                // Unicode is not supported yet. 
                snprintf( strError, EMD_MAX_ERRMSGLEN, "Cannot yet retrieve IPSD PACKED types." );
                cJSON_AddStringToObject( rspMsg, "Error", strError );
                /*
                if(   (fnParameterJanitor( MFG_PACKED_LEN, uParmID, &pLength ) == BOB_OK) 
                   && (fnParameterJanitor( MFG_PACKED_BYTE_READ, uParmID, &pByte ) == BOB_OK) )
                {
                    // Don't know how to send this data yet. 
                }
                else
                {
                    snprintf( strError, EMD_MAX_ERRMSGLEN, "BOB SICK error" );
                    cJSON_AddStringToObject( rspMsg, "Error", strError );
                }
                */
            }
            else
            {
                snprintf(strError, EMD_MAX_ERRMSGLEN, "No IPSD PackedByte Param[%d]", uParmID) ;
                cJSON_AddStringToObject(rspMsg, "Error", strError);
            }
        }
        else if(strncmp( sType, EMD_TYPE_UNICODE,  strlen(EMD_TYPE_UNICODE)) == 0 )
        {// Check the ParamID is in range for type MONEY:
            if( (uParmID >= kIPSDASCIIParamLimits.lower) && (uParmID < kIPSDASCIIParamLimits.upper) )
            {
                // Unicode is not supported yet. 
                snprintf( strError, EMD_MAX_ERRMSGLEN, "Cannot yet retrieve IPSD UNICODE types" );
                cJSON_AddStringToObject( rspMsg, "Error", strError );
                /*
                if(   (fnParameterJanitor( MFG_UNICODE_LEN, uParmID, &pLength ) == BOB_OK) 
                   && (fnParameterJanitor( MFG_UNICODE_READ, uParmID, &pWord )  == BOB_OK) )
                {
                    // Don't know how to send this data yet. 
                }
                else
                {
                    snprintf( strError, EMD_MAX_ERRMSGLEN, "BOB SICK error" );
                    cJSON_AddStringToObject( rspMsg, "Error", strError );
                }
                */
            }
            else
            {
                snprintf(strError, EMD_MAX_ERRMSGLEN, "No IPSD UNICODE Param[%d]", uParmID) ;
                cJSON_AddStringToObject(rspMsg, "Error", strError);
            }
        }
        // Multi-Packed Parameters is not supported yet. 

    }// End of buffer has been allocated. 

    return retValue;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
INT handleUICMessage(UINT16 uParmID, char *sType, cJSON *rspMsg)
{
    INT     iB64Size = 0;
    INT     retValue = EMD_UNHANDLED ;
    char    strError[EMD_MAX_ERRMSGLEN] ;
    char    *pTmp;

    if(globalBase64OutBuffer != NULL)
    {
        if(strncmp(sType,EMD_TYPE_BYTE, strlen(EMD_TYPE_BYTE)) == 0)
        {
            // NOTE! RD - Not sure what defines the extents of this, currently it comes from MM97016T1_DataDictionary
            if((uParmID >= kUICByteParamLimits.lower) && (uParmID < kUICByteParamLimits.upper))
            {
                UINT8   u8Res = 0 ;
                retValue = EMD_HANDLED ;

                u8Res = fnFlashGetByteParm(uParmID);

                if (fnIsSignedParamValue(EMD_CLASS_UIC, EMD_TYPE_BYTE, uParmID))
                    snprintf(strError, EMD_MAX_ERRMSGLEN, "%d", (signed char)u8Res);
                else
                    snprintf(strError, EMD_MAX_ERRMSGLEN, "%u", u8Res);

                cJSON_AddStringToObject(rspMsg, "Value", strError);
            }
        }
        else if(strncmp(sType,EMD_TYPE_WORD,  strlen(EMD_TYPE_WORD)) == 0)
        {
            // NOTE! RD - Not sure what defines the extents of this, currently it comes from MM97016T1_DataDictionary
            if((uParmID >= kUICWordParamLimits.lower) && (uParmID < kUICWordParamLimits.upper))
            {
                UINT16  u16Res = 0 ;
                retValue = EMD_HANDLED ;
                u16Res = fnFlashGetWordParm(uParmID);

                if (fnIsSignedParamValue(EMD_CLASS_UIC, EMD_TYPE_WORD, uParmID))
                    snprintf(strError, EMD_MAX_ERRMSGLEN, "%d", (INT16)u16Res);
                else
                    snprintf(strError, EMD_MAX_ERRMSGLEN, "%u", u16Res);

                cJSON_AddStringToObject(rspMsg, "Value", strError);
            }
        }
        else if(strncmp(sType,EMD_TYPE_LONG,  strlen(EMD_TYPE_LONG)) == 0)
        {
            // NOTE! RD - Not sure what defines the extents of this, currently it comes from MM97016T1_DataDictionary
            if((uParmID >= kUICLongParamLimits.lower) && (uParmID < kUICLongParamLimits.upper))
            {
                UINT32  u32Res = 0 ;
                retValue = EMD_HANDLED ;
                u32Res = fnFlashGetLongParm(uParmID);
                // we need to had print this else JSON may handle it wrong
                snprintf(strError, EMD_MAX_ERRMSGLEN, "%u", u32Res);
                cJSON_AddStringToObject(rspMsg, "Value", strError);
            }
        }
        else if(strncmp(sType,EMD_TYPE_MONEY,  strlen(EMD_TYPE_MONEY)) == 0)
        {
            // NOTE! RD - Not sure what defines the extents of this, currently it comes from MM97016T1_DataDictionary
            if((uParmID >= kUICMoneyParamLimits.lower) && (uParmID < kUICMoneyParamLimits.upper))
            {
                unsigned char *pMoney = fnFlashGetMoneyParm(uParmID);
                double dbRetVal = fnBinFive2Double(pMoney);

                retValue = EMD_HANDLED ;
                snprintf(strError, EMD_MAX_ERRMSGLEN, "%.f", dbRetVal);
                cJSON_AddStringToObject(rspMsg, "Value", strError);
            }
        }
        else if(strncmp(sType,EMD_TYPE_ASCII,  strlen(EMD_TYPE_ASCII)) == 0)
        {
            if((uParmID >= kUICASCIIParamLimits.lower) && (uParmID < kUICASCIIParamLimits.upper))
            {
                retValue = EMD_HANDLED ;
                pTmp = (char *)fnFlashGetAsciiStringParm(uParmID);
        		if (pTmp && strlen(pTmp) > 0)
        		{
        			cJSON_AddStringToObject(rspMsg, "Value", pTmp);
        		}
        		else
        		{
                    snprintf((char *)globalBase64OutBuffer, EMD_MAX_OUTBUFF_SIZE, "Requested parameter %d not found", (INT)uParmID) ;
                    cJSON_AddStringToObject((cJSON *)rspMsg, "Error", (char *)globalBase64OutBuffer);
        		}
            }
        }
        else if(strncmp(sType,EMD_TYPE_PACKED,  strlen(EMD_TYPE_PACKED)) == 0)
        {
            int             iHandled    = 0 ;
            unsigned char   *ptr        = NULL ;
            unsigned char   *ptrNext    = NULL ;
            int             iSize       = 0;
            int             iB64Size    = 0;

            if((uParmID >= kUICPackedParamLimits.lower) && (uParmID < kUICPackedParamLimits.upper))
            {

                retValue    = EMD_UNHANDLED ;

                if ((ptr = fnFlashGetPackedByteParm(uParmID)) != NULL)
                    iHandled    = TRUE;

                if (uParmID == (kUICPackedParamLimits.upper - 1))
                {//size of last element cannot be calculated so it is hardcoded
                    iSize = LAST_PACKED_BYTE_PARM_SIZE;
                }
                else
                {// compute size of all other elements by getting ptr to next element
                    ptrNext = fnFlashGetPackedByteParm(uParmID + 1);
                    //If the request element is defined in EMD, but the next element is not in EMD, ptrNext is NULL.
                    if (ptrNext && ptr)
                        iSize = ptrNext - ptr;
                    else
                        iSize = LAST_PACKED_BYTE_PARM_SIZE;
                }
            }

            if(iHandled)
            {
                if(binaryToASCII(ptr, iSize, globalBase64OutBuffer, EMD_MAX_OUTBUFF_SIZE, &iB64Size) == NU_SUCCESS)
                {
                    // note: if this fails the size is returned as 0 so we end up with an empty buffer, safe to continue
                    // add a terminating null - note allocated size is EMD_MAX_OUTBUFF_SIZE+1
                    globalBase64OutBuffer[iB64Size]=0;
                    cJSON_AddStringToObject((cJSON *)rspMsg, "Value", (char *)globalBase64OutBuffer);
                    retValue = EMD_HANDLED;
                }
                else
                {
                    // if here then amount of data was too large...
                    snprintf((char*)globalBase64OutBuffer, EMD_MAX_OUTBUFF_SIZE, "Data request too large to handle (%d bytes to be returned)", iSize) ;
                    cJSON_AddStringToObject((cJSON *)rspMsg, "Error", (char *)globalBase64OutBuffer);
                }
            }
            else
            {
                // if here then amount of data was too large...
                snprintf((char*)globalBase64OutBuffer, EMD_MAX_OUTBUFF_SIZE, "Unknown UIC-EXTENDED PACKED parameter %d requested", (INT)uParmID) ;
                cJSON_AddStringToObject((cJSON *)rspMsg, "Error", (char *)globalBase64OutBuffer);
            }
        }
        else if(strncmp(sType,EMD_TYPE_UNICODE,  strlen(EMD_TYPE_UNICODE)) == 0)
        {
            if((uParmID >= kUICUNICODEParamLimits.lower) && (uParmID < kUICUNICODEParamLimits.upper))
            {
                UINT8 *ptr = (UINT8 *)fnFlashGetUnicodeStringParm(uParmID);

                if (ptr)
                {
					// the data should end with a 16 bit 0 however previous code sets a mac character limit of 100 on the search to
					// stop a runaway.
					int iSize =  0 ;
					for( ;(ptr[iSize] != 0 || ptr[iSize+1] != 0) && (iSize < 200) ; iSize+=2)
						;

					// null terminator
					if(ptr[iSize]==0 && ptr[iSize+1]==0)
					    iSize+=2;

					if(binaryToASCII((unsigned char *)ptr, iSize, globalBase64OutBuffer, EMD_MAX_OUTBUFF_SIZE, &iB64Size) == NU_SUCCESS)
					{
						// note: if this fails the size is returned as 0 so we end up with an empty buffer, safe to continue
						// add a terminating null - note allocated size is EMD_MAX_OUTBUFF_SIZE+1
						globalBase64OutBuffer[iB64Size]=0;
						cJSON_AddStringToObject((cJSON *)rspMsg, "Value", (char *)globalBase64OutBuffer);
						retValue = EMD_HANDLED;
					}
					else
					{
						// if here then amount of data was too large...
						snprintf((char *)globalBase64OutBuffer, EMD_MAX_OUTBUFF_SIZE, "Data request too large to handle (%d bytes to be returned)", iSize) ;
						cJSON_AddStringToObject((cJSON *)rspMsg, "Error", (char *)globalBase64OutBuffer);
					}
                }
                else
                {
                    snprintf((char *)globalBase64OutBuffer, EMD_MAX_OUTBUFF_SIZE, "Requested parameter %d not found", (INT)uParmID) ;
                    cJSON_AddStringToObject((cJSON *)rspMsg, "Error", (char *)globalBase64OutBuffer);
                }
            }
            else
            {
                // if here then amount of data was too large...
                snprintf((char *)globalBase64OutBuffer, EMD_MAX_OUTBUFF_SIZE, "Unknown UIC-UNICODE parameter %d requested", (INT)uParmID) ;
                cJSON_AddStringToObject((cJSON *)rspMsg, "Error", (char *)globalBase64OutBuffer);
            }
        }
    }

    return retValue;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
INT handleUICExtendedMessage(UINT16 uParmID, char *sType, cJSON *rspMsg)
{
    INT     retValue = EMD_UNHANDLED ;
    char    strError[EMD_MAX_ERRMSGLEN] ;

    if(globalBase64OutBuffer != NULL)
    {
        //
        if(strncmp(sType,EMD_TYPE_BYTE, strlen(EMD_TYPE_BYTE)) == 0)
        {
            if((uParmID >= kUICExByteParamLimits.lower) && (uParmID < kUICExByteParamLimits.upper))
            {
                UINT8   u8Res = 0 ;
                retValue = EMD_HANDLED ;
                u8Res = fnFlashGetIBByteParm(uParmID);
                snprintf(strError, EMD_MAX_ERRMSGLEN, "%u", u8Res);
                cJSON_AddStringToObject(rspMsg, "Value", strError);
            }
            else
            {
                snprintf(strError, EMD_MAX_ERRMSGLEN, "No Extended Byte Param [%d]", uParmID) ;
                cJSON_AddStringToObject(rspMsg, "Error", strError);
            }
        }
        else if(strncmp(sType,EMD_TYPE_WORD, strlen(EMD_TYPE_WORD)) == 0)
        {
            UINT16 u16Res = 0;
            retValue = EMD_HANDLED ;
            if((uParmID >= kUICExWordParamLimits.lower) && (uParmID < kUICExWordParamLimits.upper))
            {
                u16Res = fnFlashGetIBWordParm(uParmID);
                snprintf(strError, EMD_MAX_ERRMSGLEN, "%u", u16Res);
                cJSON_AddStringToObject(rspMsg, "Value", strError);
            }
            else
            {
                snprintf(strError, EMD_MAX_ERRMSGLEN, "No Word Param [%d]", uParmID) ;
                cJSON_AddStringToObject(rspMsg, "Error", strError);
            }

        }
        else if(strncmp(sType,EMD_TYPE_LONG, strlen(EMD_TYPE_LONG)) == 0)
        {
            UINT32 u32Res = 0;
            retValue = EMD_HANDLED ;
            if((uParmID >= kUICExLongParamLimits.lower) && (uParmID < kUICExLongParamLimits.upper))
            {
                u32Res = fnFlashGetIBLongParm(uParmID);

                if (fnIsSignedParamValue(EMD_CLASS_UIC_EXTENDED, EMD_TYPE_LONG, uParmID))
                    snprintf(strError, EMD_MAX_ERRMSGLEN, "%d", (INT32)u32Res);
                else
                    snprintf(strError, EMD_MAX_ERRMSGLEN, "%u", u32Res);

                cJSON_AddStringToObject(rspMsg, "Value", strError);
            }
            else
            {
                snprintf(strError, EMD_MAX_ERRMSGLEN, "No Long Param [%d]", uParmID) ;
                cJSON_AddStringToObject(rspMsg, "Error", strError);
            }
        }
        else if(strncmp(sType,EMD_TYPE_PACKED, strlen(EMD_TYPE_PACKED)) == 0)
        {
            int             iHandled    = 0 ;
            unsigned char   *ptr        = NULL ;
            unsigned char   *ptrNext    = NULL ;
            int             iSize       = 0;
            int             iB64Size    = 0;

            retValue    = EMD_UNHANDLED ;

            if((uParmID >= kUICExPackedParamLimits.lower) && (uParmID < kUICExPackedParamLimits.upper))
            {
                if((ptr = fnFlashGetIBPackedByteParm(uParmID)) != NULL)
                    iHandled = TRUE;

                if (uParmID == (kUICExPackedParamLimits.upper - 1))
                {//size of last element cannot be calculated so it is hardcoded
                    iSize = LAST_IPACKED_PARM_SIZE;
                }
                else
                {// compute size of all other elements by getting ptr to next element
                    ptrNext = fnFlashGetIBPackedByteParm(uParmID + 1);
                    //If the request element is defined in EMD, but the next element is not in EMD, ptrNext is NULL.
                    if(ptrNext && ptr)
                        iSize = ptrNext - ptr;
                    else
                        iSize = LAST_IPACKED_PARM_SIZE; 
                }
            }

            if(iHandled)
            {
                if(binaryToASCII(ptr, iSize, globalBase64OutBuffer, EMD_MAX_OUTBUFF_SIZE, &iB64Size) == NU_SUCCESS)
                {
                    // note: if this fails the size is returned as 0 so we end up with an empty buffer, safe to continue
                    // add a terminating null - note allocated size is EMD_MAX_OUTBUFF_SIZE+1
                    globalBase64OutBuffer[iB64Size]=0;
                    cJSON_AddStringToObject((cJSON *)rspMsg, "Value", (char *)globalBase64OutBuffer);
                    retValue = EMD_HANDLED;
                }
                else
                {
                    // if here then amount of data was too large...
                    snprintf((char *)globalBase64OutBuffer, EMD_MAX_OUTBUFF_SIZE, "Data request too large to handle (%d bytes to be returned)", iSize) ;
                    cJSON_AddStringToObject((cJSON *)rspMsg, "Error", (char *)globalBase64OutBuffer);
                }
            }
            else
            {
                // if here then amount of data was too large...
                snprintf((char *)globalBase64OutBuffer, EMD_MAX_OUTBUFF_SIZE, "Unknown UIC-EXTENDED PACKED parameter %d requested", (INT)uParmID) ;
                cJSON_AddStringToObject((cJSON *)rspMsg, "Error", (char *)globalBase64OutBuffer);
            }

        } // if packed       
        else if(strncmp(sType,EMD_TYPE_MULTIPACKED, strlen(EMD_TYPE_MULTIPACKED)) == 0)
        {
            int             iHandled    = 0 ;
            unsigned char   *ptr        = NULL ;
            unsigned char   *ptrNext    = NULL ;
            int             iSize       = 0;
            int             iB64Size    = 0;

            retValue    = EMD_UNHANDLED ;

            if((uParmID >= kUICExMultiPackedParamLimits.lower) && (uParmID < kUICExMultiPackedParamLimits.upper))
            {
                if((ptr = fnFlashGetIBMPackedByteParm(uParmID))!=NULL)
                    iHandled = TRUE;

                if (uParmID == (kUICExMultiPackedParamLimits.upper - 1))
                {//size of last element cannot be calculated so it is hardcoded
                    iSize = LAST_IMULTIPACKED_PARM_SIZE;
                }
                else
                {// compute size of all other elements by getting ptr to next element
                	ptrNext = fnFlashGetIBMPackedByteParm(uParmID + 1);
                    //If the request element is defined in EMD, but the next element is not in EMD, ptrNext is NULL.
                    if(ptrNext && ptr)
                        iSize = ptrNext - ptr;
                    else
                        iSize = LAST_IMULTIPACKED_PARM_SIZE; 
                }
            }

            if(iHandled)
            {
                if(binaryToASCII(ptr, iSize, globalBase64OutBuffer, EMD_MAX_OUTBUFF_SIZE, &iB64Size) == NU_SUCCESS)
                {
                    // note: if this fails the size is returned as 0 so we end up with an empty buffer, safe to continue
                    // add a terminating null - note allocated size is EMD_MAX_OUTBUFF_SIZE+1
                    globalBase64OutBuffer[iB64Size]=0;
                    cJSON_AddStringToObject((cJSON *)rspMsg, "Value", (char *)globalBase64OutBuffer);
                    retValue = EMD_HANDLED;
                }
                else
                {
                    // if here then amount of data was too large...
                    snprintf((char *)globalBase64OutBuffer, EMD_MAX_OUTBUFF_SIZE, "Data request too large to handle (%d bytes to be returned)", iSize) ;
                    cJSON_AddStringToObject((cJSON *)rspMsg, "Error", (char *)globalBase64OutBuffer);
                }
            }
            else
            {
                // if here then amount of data was too large...
                snprintf((char *)globalBase64OutBuffer, EMD_MAX_OUTBUFF_SIZE, "Unknown UIC-EXTENDED PACKED parameter %d requested", (INT)uParmID) ;
                cJSON_AddStringToObject((cJSON *)rspMsg, "Error", (char *)globalBase64OutBuffer);
            }

        }//if multi-packed
    } // end of if globalBuffer
    return retValue;
}

/////////////////////////////////////// Utils
// binaryToASCII
//
//  take a binary buffer in and convert it to straight ASCII returning it in the outbuffer
//
//  Input
//      pIn         Input buffer containing binary to be converted
//      inSize      amount of data to convert
//      pOut        Buffer in which ot place the conversion data
//      outMaxSize  Size of the supplied output buffer
//      outSize     return the size of the converted data
////
STATUS binaryToASCII(unsigned char *pIn, int inSize, unsigned char *pOut, int outMaxSize, int *outSize)
{
    const unsigned char kBinToASCII[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    STATUS status = NU_RETURN_ERROR ;
    *outSize = 0 ;

    if(pIn && pOut && (outMaxSize >= inSize*2))
    {
        int iIter ;
        for(iIter = 0 ; iIter < inSize ; iIter++, pIn++, pOut+=2)
        {
            *pOut = kBinToASCII[*pIn>>4];
            *(pOut+1) = kBinToASCII[*pIn & 0x0F];
            (*outSize)+=2;
        }
        status = NU_SUCCESS ;
    }

    return status ;
}

BOOL fnIsSignedParamValue(char *sClass, char *sType, UINT16 uParmID)
{
    BOOL fIsSignedValue = FALSE;

    if(strncmp(sClass, EMD_CLASS_UIC_EXTENDED, strlen(EMD_CLASS_UIC_EXTENDED)) == 0)
    {
        if(strncmp(sType,EMD_TYPE_LONG, strlen(EMD_TYPE_LONG)) == 0)
        {
            //Check if UIC Extended Long parameters can be negative
            switch(uParmID)
            {
            case ILP_GMT_OFFSET:
                fIsSignedValue = TRUE;
                break;
            
            default:
                break;
            }
            
        }
    }
    else if(strncmp(sClass, EMD_CLASS_UIC, strlen(EMD_CLASS_UIC)) == 0)
    {
        if( strncmp(sType,EMD_TYPE_BYTE, strlen(EMD_TYPE_BYTE)) == 0 )
        {
            //Check if UIC Byte parameters can be negative
            switch(uParmID)
            {
            case BP_FACTORY_DST_OFFSET:
                fIsSignedValue = TRUE;
                break;
            
            default:
                break;
            }
        }
        else if(strncmp(sType,EMD_TYPE_WORD,  strlen(EMD_TYPE_WORD)) == 0)
        {
           //Check if UIC Word parameters can be negative
           switch(uParmID)
           {
           case WP_TIME_LOCALIZATION_OFFSET:
           case WP_MIN_PRINTED_COLUMNS_OFFSET:
               fIsSignedValue = TRUE;
               break;
           
           default:
               break;
           }
        }

    }   
    else if(strncmp(sClass, EMD_CLASS_IBUTTON, strlen(EMD_CLASS_IBUTTON)) == 0)
    {
        if( strncmp(sType,EMD_TYPE_LONG, strlen(EMD_TYPE_LONG)) == 0 )
        {
            //Check if PSD Long parameters can be negative
            switch(uParmID)
            {
            case MFG_IPSDCLOCKOFFSET:
                fIsSignedValue = TRUE;
                break;
            
            default:
                break;
            }
        }
    }
        
    return fIsSignedValue;
}
