//=====================================================================
//
//	FileName:	HAL_FPGA.c
//
//	Description: Hardware abstraction layer implementation for FPGA programming interface
//=====================================================================
#include "commontypes.h"
#include "stdio.h"
#include "stddef.h"
#include "hal.h"
#include "am335x_pbdefs.h"
#include "HAL_FPGA.h"
#include "nucleus_gen_cfg.h"
#include "fpga_config.h"

extern void HALInitGPIOPins(P_GPIO_INFO pGPIOTable, unsigned numPinsUsed);
extern void setGPIOPin(GPIO_INFO pinInfo, unsigned char value);
extern void setGPIOPinHigh(GPIO_INFO pinInfo);
extern void setGPIOPinLow(GPIO_INFO pinInfo);
extern unsigned char readGPIOPin(GPIO_INFO pinInfo);
extern void  HALDelayTimerTicks (UINT32 no_of_ticks);

// Global variables
unsigned char revPCB = 0xFF;

// The order of the entries in the following table must match the order of the entries in
// the GPIO_INFO enum in HAL_FPGA.h
// After a reset, a pin configured for output will output a 0.

#ifdef CFG_NU_BSP_BEAGLEBONE_BLACK_ENABLE
// *** indicates different pin from CSD
static GPIO_INFO BBB_GPIO_Table[] = {
							{AM335X_CTRL_PADCONF_GPMC_A0, AM335X_GPIO_BIT_16, GPIO_BANK_1, DIR_OUTPUT, GPIO_PULL_UP}, //*** CRESET_B/FPGA_NCONFIG
							{AM335X_CTRL_PADCONF_GPMC_A1, AM335X_GPIO_BIT_17, GPIO_BANK_1, DIR_OUTPUT, GPIO_PULL_DOWN}, //*** FPGA_DATA
							{AM335X_CTRL_PADCONF_ECAP0_IN_PWM0_OUT, AM335X_GPIO_BIT_7, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_DOWN}, // FPGA_CLK
							{AM335X_CTRL_PADCONF_GPMC_CLK, AM335X_GPIO_BIT_1, GPIO_BANK_2, DIR_OUTPUT, GPIO_PULL_DOWN} // FPGA_RESET
};
static const unsigned numPinsUsed = sizeof(BBB_GPIO_Table) / sizeof(GPIO_INFO);
static P_GPIO_INFO pGPIOTable = &BBB_GPIO_Table[0];

#endif
#ifdef CFG_CSD_2_3_ENABLE

// CSD FPGA programming - pins should match schematic
static GPIO_INFO CSD_GPIO_Table[] = {
		{AM335X_CTRL_PADCONF_LCD_DATA0, AM335X_GPIO_BIT_6, GPIO_BANK_2, DIR_OUTPUT, GPIO_PULL_UP}, // CRESET_B/FPGA_NCONFIG
		{AM335X_CTRL_PADCONF_UART0_RTSN, AM335X_GPIO_BIT_9, GPIO_BANK_1, DIR_OUTPUT, GPIO_PULL_DOWN}, // FPGA_DATA
		{AM335X_CTRL_PADCONF_ECAP0_IN_PWM0_OUT, AM335X_GPIO_BIT_7, GPIO_BANK_0, DIR_OUTPUT, GPIO_PULL_DOWN}, // FPGA_CLK
		{AM335X_CTRL_PADCONF_GPMC_CLK, AM335X_GPIO_BIT_1, GPIO_BANK_2, DIR_OUTPUT, GPIO_PULL_DOWN} // FPGA_RESET
};
static const unsigned numPinsUsed = sizeof(CSD_GPIO_Table) / sizeof(GPIO_INFO);
static P_GPIO_INFO pGPIOTable = &CSD_GPIO_Table[0];

// CSD PCB Revision reading - pins should match schematic
static GPIO_INFO CSD_PCBREV_GPIO_Table[] = {
		{AM335X_CTRL_PADCONF_XDMA_EVENT_INTR1, AM335X_GPIO_BIT_20, GPIO_BANK_0, DIR_INPUT, GPIO_PULL_OFF}, //*** FPGA_INTR1
		{AM335X_CTRL_PADCONF_XDMA_EVENT_INTR0, AM335X_GPIO_BIT_19, GPIO_BANK_0, DIR_INPUT, GPIO_PULL_OFF}, //*** FPGA_INTR0
		{AM335X_CTRL_PADCONF_UART0_RTSN, AM335X_GPIO_BIT_9, GPIO_BANK_1, DIR_INPUT, GPIO_PULL_OFF}, //*** FPGA_DATA
		{AM335X_CTRL_PADCONF_GPMC_CLK, AM335X_GPIO_BIT_1, GPIO_BANK_2, DIR_INPUT, GPIO_PULL_OFF} // FPGA_RESET
};
static const unsigned numPCBREVPinsUsed = sizeof(CSD_PCBREV_GPIO_Table) / sizeof(GPIO_INFO);
static P_GPIO_INFO pPCBREVGPIOTable = &CSD_PCBREV_GPIO_Table[0];

#endif


void HALInitFPGAInterface()
{
	// Output pins are set to output (default value 0)
	// Input pins are set to input
	// In/out pins are set to input (default state)
	HALInitGPIOPins(pGPIOTable, numPinsUsed);

	// For FPGA set initial output levels
	setGPIOPin(*(pGPIOTable + FPGA_IDX_NCONFIG), 1);
	setGPIOPin(*(pGPIOTable + FPGA_IDX_DATA), 1);
	setGPIOPin(*(pGPIOTable + FPGA_IDX_CLOCK), 1);
	setGPIOPin(*(pGPIOTable + FPGA_IDX_RESET), 0);

}

void HALResetFPGA()
{// assume reset pin is normally 1
	setGPIOPin(*(pGPIOTable + FPGA_IDX_NCONFIG), 0);
	ESAL_PR_Delay_USec( FPGA_RESET_DELAY );
	setGPIOPin(*(pGPIOTable + FPGA_IDX_NCONFIG), 1);
}

// Includes delay after value is set to enforce max 25 MHz clock
void HALRunCLK(unsigned numCycles)
{
    int curCycle;
    GPIO_INFO pinInfo = *(pGPIOTable + FPGA_IDX_CLOCK);

    for(curCycle=0; curCycle < numCycles; curCycle++)
    {
    	//  direction = LOW_TO_HIGH
    	setGPIOPinLow(pinInfo);
    	HALDelayTimerTicks(FPGA_25MHZ_CLK_DELAY);
    	setGPIOPinHigh(pinInfo);
    	HALDelayTimerTicks(FPGA_25MHZ_CLK_DELAY);
    }

}

void HALSendByte(unsigned char configByte)
{
    unsigned long curBit = 0;
    GPIO_INFO pinInfo = *(pGPIOTable + FPGA_IDX_DATA);

    for(curBit=0; curBit < 8; curBit++)
    {
    	// shift out MSB first
		if (configByte & 0x80)
		{
            // Send a '1'
			setGPIOPinHigh(pinInfo);
		}
        else
        {
            // Send a '0'
        	setGPIOPinLow(pinInfo);
        }
		HALRunCLK(1); // if too slow, inline it
		configByte = configByte << 1;
    }

}

// Must be preceded by call to HALInitFPGAInterface
void HALWriteFPGAConfig(unsigned char *pConfigData, unsigned int numChars)
{
	int old_level;
	unsigned int curChar;


	old_level = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);

	HALResetFPGA();
	ESAL_PR_Delay_USec( FPGA_POST_RESET_DELAY );

	for (curChar = 0; curChar < numChars; curChar++)
	{
		HALSendByte(*(pConfigData + curChar));
	}
	HALRunCLK(FPGA_POST_CONFIG_CLOCKS);

	// Delays & reset as specified by Canon
	ESAL_PR_Delay_USec( FPGA_POST_PROG_DELAY );
	setGPIOPin(*(pGPIOTable + FPGA_IDX_RESET), 1);
	ESAL_PR_Delay_USec( FPGA_POST_PROG_DELAY );

	NU_Local_Control_Interrupts(old_level);

}

#ifdef CFG_CSD_2_3_ENABLE
// WARNING! - this resets the FPGA to get the PCB Rev
// so FPGA must be reprogrammed after this!
// This is only available on CSD.
unsigned char HALGetPCBRevision(void)
{
	unsigned char rev = 0;

	// assume reset pin is normally 1, hold FPGA in reset and wait
	setGPIOPin(*(pGPIOTable + FPGA_IDX_NCONFIG), 0);

	// Setup PCB Rev pins
	HALInitGPIOPins(pPCBREVGPIOTable, numPCBREVPinsUsed);
	ESAL_PR_Delay_USec( PCB_REV_READ_DELAY );

	// read PCB Rev bits
	rev |= readGPIOPin(*(pPCBREVGPIOTable + PCBREV_IDX_INTR1));
	rev <<= 1;
	rev |= readGPIOPin(*(pPCBREVGPIOTable + PCBREV_IDX_INTR0));
	rev <<= 1;
	rev |= readGPIOPin(*(pPCBREVGPIOTable + PCBREV_IDX_DATA));
	rev <<= 1;
	rev |= readGPIOPin(*(pPCBREVGPIOTable + PCBREV_IDX_RESET));

	setGPIOPin(*(pGPIOTable + FPGA_IDX_NCONFIG), 1);

	// Initialize FPGA Interface again so that shared FPGA_DATA signal can be an output again
	HALInitFPGAInterface();

	return rev;
}

void HALSetVM27VPowerState(unsigned char powerOn)
{
	if (powerOn)
	{
	    ESAL_GE_MEM_WRITE16(CSD_FPGA_BASE + VM27V_POWER_EN_REG, 1);
	}
	else
	{
	    ESAL_GE_MEM_WRITE16(CSD_FPGA_BASE + VM27V_POWER_EN_REG, 0);
	}

}

void HALSetS5VSPowerState(unsigned char powerOn)
{
	if (powerOn)
	{
	    ESAL_GE_MEM_WRITE16(CSD_FPGA_BASE + S5VS_POWER_EN_REG, 1);
	}
	else
	{
	    ESAL_GE_MEM_WRITE16(CSD_FPGA_BASE + S5VS_POWER_EN_REG, 0);
	}

}

void HALSetS5VPowerState(unsigned char powerOn)
{
	if (powerOn)
	{
	    ESAL_GE_MEM_WRITE16(CSD_FPGA_BASE + S5V_POWER_EN_REG, 1);
	}
	else
	{
	    ESAL_GE_MEM_WRITE16(CSD_FPGA_BASE + S5V_POWER_EN_REG, 0);
	}

}

/* **********************************************************************
// DESCRIPTION: Get FPGA config from initialized RAM and program FPGA
//              Also display PCB revision.
//
// INPUTS:
// RETURNS:
// **********************************************************************/
void HALInitFPGA(void)
{
	unsigned bytesToWrite;

	//Init GPIO pins used to interface with FPGA
	HALInitFPGAInterface();

	// Get PCB Rev first
	revPCB = HALGetPCBRevision();

	bytesToWrite = sizeof(fpga_config_data0);

	// Pass buffer to routine that programs FPGA.
	HALWriteFPGAConfig((unsigned char *) &fpga_config_data0[0], bytesToWrite);


}

// LEDs 1 - 8, ledNum should be 0 - 7, respectively
void HALToggleFPGALED(unsigned char ledNum)
{
	unsigned short readVal;

    readVal = ESAL_GE_MEM_READ16(CSD_FPGA_BASE + LED_REG);
    ESAL_GE_MEM_WRITE16(CSD_FPGA_BASE + LED_REG, readVal ^ (1 << ledNum));

}

// Disables FPGA Watchdog timer in case timer expiration causes emulator problems
void HALDisableFPGAWatchdog(void)
{
    ESAL_GE_MEM_WRITE16(CSD_FPGA_BASE + WATCHDOG_REG, WATCHDOG_DISABLE);
}


#endif

