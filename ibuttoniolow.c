/************************************************************************
 PROJECT:        Horizon CSD
 MODULE NAME:    ibuttoniolow.c

 DESCRIPTION:    
   This file contains the functions that make up the iButton driver.

        The following reference documents
        were used in the design of the driver:
			Dallas/Maxim DS1955B-PSD IBIP Postal Secuity Device
			Dallas DS2480 Serial 1-wire Line driver
			Dallas App. Note 192: 
                Using the DS2480B Serial 1-Wire Line Driver
			Dallas App. Note 126:
                1-Wire Communication Through Software

 ----------------------------------------------------------------------
               Copyright (c) 2016 Pitney Bowes Inc.
                    37 Executive Drive
                   Danbury, Connecticut  06810
 ----------------------------------------------------------------------
 *************************************************************************/

#include <string.h> 
#include "commontypes.h"
#include "bob.h"
#include "am335x_pbdefs.h"
#include "ibuttoniolow.h"
#include "pbos.h"
#include "sac.h"

//----------------------------------------------------------------------------
//  Prototypes for functions that are not yet prototyped in an include file: 
extern void  HALDelayTimerTicks (UINT32 no_of_ticks);

//TODO: JAH Remove me after debugging 253D causes
extern UINT8 DebugIBtnDriver[];
extern UINT8 DebugIBtnDriverCtr;


//----------------------------------------------------------------------------
//      External variable declarations:

extern UARTCHANNEL iButtonChannel;


//----------------------------------------------------------------------------
//      Global variables created here...

unsigned char ucDS2480Mode; // holds the current mode (command or data) of Dallas DS2480 chip

UINT8 iButtonDiagBuf[ SIZEOF_IBTNDIAGBUF ];
unsigned short iButtonDiagBufIndex;


// LMD testing
#ifdef IBUTTON_TEST_BUFFER
unsigned char iButtonBuf[IBUTTON_TEST_BUFFER_SIZE];
unsigned short iButtonBufIdx=0;
#endif

//----------------------------------------------------------------
//  Local function prototypes:
 

//----------------------------------------------------------------
//  Local variables: 
                    
volatile unsigned short CRCTable[256];



/******************************************************************************/
/*                                                                            */
/*  Function Name:          CRC16TGen                                         */
/*                                                                            */
/*  Author: Mike Ernandez                                                     */
/*                                                                            */
/*  Function Description:   Generate 1 crc lookup table entry                 */
/*                                                                            */
/*  Inputs:                  x:   index of value being computed               */
/*                           crc: input crc value                             */
/*                                                                            */
/*  Outputs:                 none                                             */
/*                                                                            */
/*  Returns:                 crc: crc lookup table entry                      */
/*                                                                            */
/******************************************************************************/
unsigned short CRC16TGen(unsigned char x, unsigned short crc)
{
	unsigned char bit;
	unsigned char n;

	for(n = 0; n < 8; n++)
	{
		bit = (unsigned char)((crc ^ x) & 1);
		crc >>= 1;

		if(bit)
			crc ^= 0xA001;

		x >>= 1;
	}

	return crc;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name:          GenCRC16Table                                     */
/*                                                                            */
/*  Author: Mike Ernandez                                                     */
/*                                                                            */
/*  Function Description:   Generate 256 word table for CRC16 lookup.         */
/*                                                                            */
/*  Inputs:                 none                                              */
/*                                                                            */
/*  Outputs:                none                                              */
/*                                                                            */
/*  Returns:                none                                              */
/*                                                                            */
/******************************************************************************/
void GenCRC16Table()
{
	unsigned short i;

	for(i = 0; i < 256; ++i)
		CRCTable[i] = CRC16TGen((unsigned char) i, 0);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name:          CRC16                                             */
/*                                                                            */
/*  Author: Mike Ernandez                                                     */
/*                                                                            */
/*  Function Description:   Partial crc computation                           */
/*                                                                            */
/*  Inputs:                 x: value to be added to crc                       */
/*                          crc: current crc value                            */
/*                                                                            */
/*  Outputs:                none                                              */
/*                                                                            */
/*  Returns:                Updated crc                                       */
/*                                                                            */
/******************************************************************************/
UINT16 CRC16( UINT8 x, UINT16 crc )
{
	return( CRCTable[(crc ^ x) & 0xFF] ^ crc >> 8 );
}

/******************************************************************************/
/*                                                                            */
/*  Function Name:          CRC16Buf                                          */
/*                                                                            */
/*  Author: Mike Ernandez                                                     */
/*                                                                            */
/*  Function Description:   Compute 16 bit crc of data pointed to by bp       */
/*                                                                            */
/*  Inputs:                 bp: pointer to data                               */
/*                          crc: starting value for crc                       */
/*                          Count: Number of bytes to use in buffer           */
/*                                                                            */
/*  Outputs:                none                                              */
/*                                                                            */
/*  Returns:                updated crc                                       */
/*                                                                            */
/******************************************************************************/
UINT16 CRC16Buf( const UINT8 *bp, UINT16 crc, UINT16 Count)
{
    UINT16 i;

    for( i = 0; i < Count; i++ )
        crc = CRCTable[(crc ^ bp[i]) & 0xFF] ^ crc >> 8;
    
    return( crc );
}


/********************************************************************************/
/*                                                                              */
/*  Function Name: iButtonSegmentAPDU                                           */
/*                                                                              */
/*  Author: Mike Ernandez                                                       */
/*                                                                              */
/*  Function Description:                                                       */
/*      This function takes the message data  and breaks it up into             */
/*      128-byte segments in order to meet the requirements of the iButton.     */
/*                                                                              */
/*  The following data structure is the Tx segment header and gets written to   */
/*  the iButtons's I/O Buffer prior to writing the IPR Register.                */
/*                                                                              */         
/*  uchar  blockNbr;        // Segment block number - add 0x80 for last block   */
/*  uchar  blockLth;        // Length of the IPR segment                        */
/*  uchar  remainLthLow;    // Remaining Length, includes this block - Low byte */
/*  uchar  remainLthHigh;   // Remaining Length, includes this block - High byte*/
/*  uchar  CRCLow;          // CRC16 of length and data in block - Low byte     */
/*  uchar  CRCHigh;         // CRC16 of length and data in block - High byte    */
/*  uchar  CSUMLow;         // mod16 bit checksum over data & hdr - Low byte    */ 
/*  uchar  CSUMHigh;        // mod16 bit checksum over data & hdr - High byte   */
/*                                                                              */
/*                                                                              */
/*  Inputs:                                                                     */
/*  Outputs:                                                                    */
/*  Returns:   
// NOTES:
//  The segments are used differently if using the Extended CB or the IPR.
//  When using the IPR:
//     A.  ReadStatus, Write_I/O, RelSeq, WriteIPR, WriteStatus, RelSeq,
//          Start, RelSeq, ReadStatus, RelSeq.  
//       if detect continue input, repeat until done...
//     B.     Write_I/O, RelSeq, WriteIPR, WriteStatus, RelSeq, Start, RelSeq, 
//             ReadStatus, RelSeq 
//       when done, if detect continue running, repeat until done...
//     C.     WriteStatus, RelSeq, Continue, RelSeq, ReadStatus, RelSeq 
//       when done... 
//     D.     Read_I/O, RelSeq, ReadIPR, 
//       if more to read, go to B.  
//          
//  When using the Extended CB:
//     A.     ReadStatus, WriteCBExt, 
//       if more than one segment, repeat until done:
//     B.     ContinueCB_Write
//       when done...
//     C.     WriteStatus, RelSeq, Start, RelSeq, ReadStatus, RelSeq.  
//       if detect continue running, repeat until done...
//     D.     WriteStatus, RelSeq, Continue, RelSeq, ReadStatus, RelSeq 
//       when done... 
//     E.     ReadCB_Ext
//       if detect more to read, continue until done...
//     F.     ContinueCB_Read
//                                                                   */
//----------------------------------------------------------------------------/
IBUTTON_STATUS iButtonSegmentAPDU( IBUTTON_DEVICE *dev, const BOBS_DRIVER_INTERFACE *bobIfPtr)
{
    IBUTTON_STATUS iButtonStatus = IBUTTON_OK;

    UINT16  remainLength;
    UINT16  segIndex = 0;
    UINT8   blockNbr = 0;
    UINT16  bobIndex = 0;
    UINT16  copyLth;
    UINT8   buffIndex, i;
    UINT16  crc = 0, csum = 0;
    UINT8  bPayloadSize = PAYLOAD_SIZE;
     
    // Calculate the segment count from the BOB bMsgLen+3. The extra bytes accomodate
    // the 2 magic bytes, and the overall APDU length byte.
    if( fnIbuttonGetFlagBootMsg( dev ) == TRUE )
    {
        remainLength = bobIfPtr->bMsgLen + IBTN_APDU_MAGIC_LEN;
        if( remainLength > IBTN_BL_PAYLOAD_SIZE )
        
        {
            return( IBUTTON_TX_ERROR );
        }
    }
    else 
    {
        remainLength = bobIfPtr->bMsgLen + IBTN_APDU_MAGIC_LEN;
    }

    // Initialize the segment index and count
    dev->Trans.iButtonTxCurSegment = 0;
    dev->Trans.iButtonTxSegCount = 0;
    
    // Create all buffer(for WriteIPR's) and hdr(for WriteIO's) contents
    while( remainLength ) 
    {
        // Segment buffer index points to first element
        buffIndex = 0;
        
        // Set the remaining length in the header
        dev->Trans.iButtonTxSegHdr[ segIndex]->remainLthLow = remainLength & 0x00ff;
        dev->Trans.iButtonTxSegHdr[ segIndex]->remainLthHigh = (remainLength >> 8) & 0x00ff;
        
        // Compute the segment Length
        if( remainLength > bPayloadSize ) 
        {
            // Decrement the totalLength
            remainLength -= bPayloadSize;
            // Set the header block length to max
            dev->Trans.iButtonTxSegHdr[ segIndex]->blockLth = bPayloadSize;
        }
        else 
        {
            // Set the header block length to what's left
            dev->Trans.iButtonTxSegHdr[ segIndex]->blockLth = (UINT8)remainLength;
            // This is the last packet so turn on the bit
            blockNbr |= 0x80;

            // Done with the data
            remainLength = 0;
        }

        // If the first segment, add magic bytes...
        if( segIndex == 0 )
        {
            if( fnIbuttonGetFlagBootMsg( dev ) == FALSE )
            {
                // Insert overall APDU length and magic bytes into the buffer
                // Since the command structure only allows 1 byte for the message 
                //  length, if bobIfPtr->bMsgLen > 253, the rest is just cut off, and the
                //  message will probably fail.
                dev->Trans.iButtonTxSegBuff[ segIndex][ buffIndex++] = bobIfPtr->bMsgLen + 2;
                dev->Trans.iButtonTxSegBuff[ segIndex][ buffIndex++] = IBTN_APDU_MAGIC_BYTE1;
                dev->Trans.iButtonTxSegBuff[ segIndex][ buffIndex++] = IBTN_APDU_MAGIC_BYTE2;
                // Set the copy length
                copyLth = dev->Trans.iButtonTxSegHdr[ segIndex ]->blockLth - IBTN_APDU_MAGIC_LEN;
            }
            else
            {
                memcpy( &dev->Trans.iButtonTxSegBuff[ segIndex][ buffIndex ], 
                        IBTN_BL_MAGIC_BYTES, 
                        IBTN_BL_MAGIC_LEN );
                buffIndex += IBTN_BL_MAGIC_LEN;
                // Set the copy length
                copyLth = dev->Trans.iButtonTxSegHdr[ segIndex ]->blockLth - IBTN_BL_MAGIC_LEN;
            }
        }
        else
        {
            copyLth = dev->Trans.iButtonTxSegHdr[ segIndex]->blockLth;
        }

        // Copy the data into the buffer
        for( i = 0; i < copyLth; i++ ) 
        {
            dev->Trans.iButtonTxSegBuff[ segIndex][buffIndex] = bobIfPtr->pMessage[ bobIndex];
            buffIndex++;
            bobIndex++;
        }
                                
        // Set the block number in the header
        dev->Trans.iButtonTxSegHdr[ segIndex]->blockNbr = blockNbr;

        // If it is not a BL message, Compute the checksum and CRC for each block...
        if( fnIbuttonGetFlagBootMsg( dev ) == FALSE )
        {   
            // Compute CRC
            crc = CRC16( dev->Trans.iButtonTxSegHdr[ segIndex]->blockLth , 0);
            crc = CRC16Buf( dev->Trans.iButtonTxSegBuff[ segIndex], 
                            crc, 
                            dev->Trans.iButtonTxSegHdr[ segIndex]->blockLth);
            
			// Set the value into the header (l/h)
 			dev->Trans.iButtonTxSegHdr[ segIndex]->CRCLow = crc & 0x00ff;
            dev->Trans.iButtonTxSegHdr[ segIndex]->CRCHigh = (crc >>8) & 0x00ff;

            // Compute the running checksum from the header and buffer
            // ...the header includes 
            csum += dev->Trans.iButtonTxSegHdr[ segIndex]->blockNbr;    
            csum += dev->Trans.iButtonTxSegHdr[ segIndex]->blockLth;    
            csum += dev->Trans.iButtonTxSegHdr[ segIndex]->remainLthLow;
            csum += dev->Trans.iButtonTxSegHdr[ segIndex]->remainLthHigh;
            csum += dev->Trans.iButtonTxSegHdr[ segIndex]->CRCLow;      
            csum += dev->Trans.iButtonTxSegHdr[ segIndex]->CRCHigh;     
        
            // ...and the data
            for( i=0; i<dev->Trans.iButtonTxSegHdr[ segIndex]->blockLth; i++)
                csum += dev->Trans.iButtonTxSegBuff[ segIndex][ i];
        
            // Set the value into the header (l/h)
            dev->Trans.iButtonTxSegHdr[ segIndex]->CSUMLow = csum  & 0x00ff;         
            dev->Trans.iButtonTxSegHdr[ segIndex]->CSUMHigh = (csum >>8) & 0x00ff;

            // Add in the current checksum
            csum += dev->Trans.iButtonTxSegHdr[ segIndex]->CSUMLow; 
            csum += dev->Trans.iButtonTxSegHdr[ segIndex]->CSUMHigh;    
        }

        // Bump the segment index and block number
        segIndex++;
        blockNbr++;
        dev->Trans.iButtonTxSegCount++;
    }

    // Set the return status 
    return( iButtonStatus);
}



//******************************************************************************/
//                                        
//  Function Name: iButtonReassembleAPDU  
//                                        
//  Author: Mike Ernandez                 
//                                        
//  Function Description: 
//                                        
//  Author: Mike Ernandez                 
//                                        
//  Inputs:                               
//  Outputs:                              
//  Returns:                              
// NOTES: 
//  1. The receive buffer is 2058 bytes long (juniorReplyMsg[ REPLY_BUF_SIZ ])
//----------------------------------------------------------------------------/
IBUTTON_STATUS iButtonReassembleAPDU( IBUTTON_DEVICE *dev, BOBS_DRIVER_INTERFACE *bobIfPtr)
{
    IBUTTON_STATUS iButtonStatus = IBUTTON_OK;

    if( fnIbuttonGetFlagBootMsg( dev ) == TRUE )
    {
        // Append the received data to bob's Rx buffer, without the CRC,
        //  and update bob's reply length.
        memcpy( &bobIfPtr->pDataRecv[ 0 ][ bobIfPtr->replyLen[ 0] ], 
                dev->Link.iButtonRxResponseBuff,   
                dev->Link.iButtonRxResponseLength - 2); 
        // Update the Data Length
        bobIfPtr->replyLen[ 0] += dev->Link.iButtonRxResponseLength - 2;
    }
    else
    {
        // Append the current Rx converted data to Bob receive buffer
        // Just want the payload no header or CRC - the payload data returned
        // contains 3 bytes of data that is not part of the data to be reassembled
        // and is not copied, viz length, 3 magic bytes, data, 2 bytes CRC
        memcpy( &bobIfPtr->pDataRecv[ 0][ bobIfPtr->replyLen[ 0]], 
                dev->Link.iButtonRxResponseBuff+3,   
                dev->Link.iButtonRxResponseLength - 5); 
        // Update the Data Length
        bobIfPtr->replyLen[ 0] += dev->Link.iButtonRxResponseLength - 5;
    }

    // Set the return status 
    return( iButtonStatus);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name: iButtonGenerateCmd                                         */
/*                                                                            */
/*  Author: Mike Ernandez/Modified by Tom Dometios for Phoenix                */
/*                                                                            */
/*  Function Description:                                                     */
/*      Formats a command to sent to either the DS2480 interface chip or to   */
/*      the DS1955 iButton.  The two command target types are:                */
/*          DS2480_CMD and IBUTTON_CMD                                        */
/*      The DS2480_CMD target type is further broken down into 2 command      */
/*      types:  IBD_CMD_TYPE_BREAK                                                    */
/*              IBD_CMD_TYPE_DS2480                                                   */
/*      The IBD_CMD_TYPE_BREAK is used to send a UART BREAK sequence to the 2480.     */
/*      The IBD_CMD_TYPE_DS2480 is used for all other DS2480 commands.                */
/*                                                                            */
/*  Inputs:                                                                   */
/*  Outputs:                                                                  */
/*  Returns:                                                                  */
// NOTES: 
//----------------------------------------------------------------------------/
IBUTTON_STATUS iButtonGenerateCmd( UINT8 cmdTarget, UINT8 cmd, UINT16 *calcCRC, 
                                   IBUTTON_DEVICE *dev ) 
{
    IBUTTON_STATUS iButtonStatus = IBUTTON_OK;
    IBUTTON_CMD_TYPE  cmdType = UNKNOWN_CMD_TYPE;
    UINT16  stuffedLength = 0;   
    UINT8   cmdHdrBuff[ 64 ]; //cmd byte and any non-message data info, e.g., length bytes, etc. 
    UINT8   cmdHdrLength = 0;
    UINT8   rspOffset = 0;
    UINT8   rspLength = 0;
    UINT16  crc = 0;
    void    *pMsgData = NULL_PTR;   // ptr to the data part of a message, if any
    UINT8   msgDataLength = 0;      // length of the data part of a message
    
    BOOL fCalcCRC = FALSE;   // tells the iButtonByteStuff function whether to calc. the CRC or not

#ifdef IBUTTON_LOG
    UINT8   logDataOffset=1;  // data follows cmd byte
    UINT8   logCmd = cmd;     // command will not be remapped
#endif

    // Pre-set the TxBuff for the read segment.  A 0xFF has to be written to the DS2480 for every response byte
    // that is expected to be returned from the iButton.  The 2480 will generate a "write-1" time slot on the
    // 1-wire bus which is the same as a "read-data" time slot.
    memset( dev->Link.iButtonTxBuff, 0xff, TX_BUFF_LEN );
    
    cmdHdrBuff[ 0 ] = cmd;
    
    if( cmdTarget == DS2480_CMD )
    {
        switch( cmd )
        {
            case SEND_BREAK:
                // Format: Cmd->
                
                //
                // Set the clock divisor in the Baud Rate Register: clock divisor = 48MHz / ( baudrate * 16 )
                // This will allow us to generate a break condtion (Tx = 0 for at least 2mS by writing 0x00 at 
                // 4800 baud.  The start break and stop break bits in the uart control register are not being used
                // to generate the break since there is no easy way to set a 2mS timer because we are in a HISR at this
                // point.  The utility OSWakeAfter() will not work in an HISR because it is a suspend function.
                //
                

                (void)(*iButtonChannel.uartControl->fnSetBaudRate)( dev->pbUartBase , BRRBREAK );
                cmdType = IBD_CMD_TYPE_BREAK;
                cmdHdrBuff[ 0 ] = 0;
                cmdHdrLength = 1;
                rspLength = 0;
                rspOffset = 0;
                break;
                
            // the first reset following a BREAK signal (master reset cycle) does not get passed through to the iButton,  
            // thus no response is produced.
            case DS2480_RESET:
                // Format: Cmd->
                cmdType = IBD_CMD_TYPE_DS2480;
                cmdHdrLength = 1;
                rspLength = 0;
                rspOffset = 0;
                //dev->Link.iButtonNoResponse = TRUE;   // tell the TxRx ISR not to wait for a response
                break;
            
            // this is the 2nd reset to the 2480 which will be passed through to the iButton and produce
            // a response, where as the first reset produces no response.           
            case DS2480_OWRESET: 
                // Format: Cmd->, <-presence detect(1byte)
                cmdHdrBuff[ 0 ] = DS2480_RESET;
#ifdef IBUTTON_LOG
                logCmd = DS2480_RESET;
#endif
                cmdType = IBD_CMD_TYPE_DS2480;
                cmdHdrLength = 1;
                rspLength = 1;      
                rspOffset = 0;
                break;
            
            case DS2480_FAST_BAUD:
                // Format: Cmd->   // The response is ignored by turning off the receiver.  
#ifdef IBUTTON_LOG
                logCmd = 3;     // Need to remap b/c DS2480_FAST_BAUD command matched IButton START_PROGRAM Command
#endif
                cmdType = IBD_CMD_TYPE_DS2480;
                cmdHdrLength = 1;
                rspLength = 0;
                rspOffset = 0;
                break;
            
            case DS2480_TEST_BAUD:
                // Format: Cmd->, <-read baud rate(1byte)
#ifdef IBUTTON_LOG
                logCmd = 4;     // Need to remap b/c DS2480_FAST_BAUD command matched IButton WRITE_IPR_REGISTER cmd
#endif
                cmdType = IBD_CMD_TYPE_DS2480;
                cmdHdrLength = 1;
                rspLength = 1;
                rspOffset = 0;
                break;
            
            case DS2480_OWRESET_OD: // set an overdrive reset
                // Format: Cmd->, <-presence detect(1byte)
                cmdType = IBD_CMD_TYPE_DS2480;
                cmdHdrLength = 1;
                rspLength = 1;      
                rspOffset = 0;
                break;
                
            case DS2480_SINGLE_BIT_WRITE_0:
                // Format: Cmd->, <-1-wire read back(1byte)
                cmdType = IBD_CMD_TYPE_DS2480;
                cmdHdrLength = 1;
                rspLength = 1;
                rspOffset = 0;
                break;
 
            case DS2480_SINGLE_BIT_WRITE_1:
                // Format: Cmd->, <-1-wire read back(1byte)
                cmdType = IBD_CMD_TYPE_DS2480;
                cmdHdrLength = 1;
                rspLength = 1;
                rspOffset = 0;
                break;

            default:
                break;
        }
    }    
    else if( cmdTarget == IBUTTON_CMD )
    {
        switch( cmd )
        {
            case READ_ROM:          
                cmdType = IBD_CMD_TYPE_ROM;
                // Format: Cmd->, <-FamilyCode(1byte), <-Serial#(6bytes), <-CRC(1byte)
                cmdHdrLength = 1;
                rspLength = 8; 
                rspOffset = 1;
                break;

            case SKIP_ROM:
            // fall through
            case OVERDRIVE_SKIP_ROM:
                // ROM or Extended cmd
                cmdType = IBD_CMD_TYPE_ROM;
                // Format: Cmd->
                cmdHdrLength = 1;
                rspLength = 0;
                rspOffset = 0;
                break;

            case WRITE_STATUS:
                // ROM or Extended cmd
                cmdType = IBD_CMD_TYPE_DTC;
                // Format: Cmd->, OWUS(1byte)->,<-CRC16(2bytes)
                // Set the maximum time in the OWUS register - should always run to completion
                cmdHdrBuff[ 1] = MICRO_RUN_LIMIT_3812;
                fCalcCRC = TRUE;
                cmdHdrLength = 2;
                rspLength = 2;
                rspOffset = 2;
                break;

            case WRITE_IO_BUFFER:   
                // ROM or Extended cmd
                cmdType = IBD_CMD_TYPE_DTC;
                // Format: Cmd->,CmdLength(1Byte)->,Block#(1byte)->,IPR data length(1byte)->, 
                //          remainLthLow(1byte)->,remainLthHigh(1byte)->,CRC16(2bytes)->,
                //          ChkSum(2bytes)->,<-CRC16(2bytes)
                // Set the lengths - including the magic bytes needed for the IPR write
                cmdHdrBuff[ 1] = 8; // Write IO Frame size always 8 bytes
                cmdHdrLength = 2;
                
                // Insert the Write IO header created during the segmentation
                pMsgData = (IBUTTON_TX_HDR *)dev->Trans.iButtonTxSegHdr[ dev->Trans.iButtonTxCurSegment ]; 
                msgDataLength = 8;
                rspLength = 2;
                rspOffset = cmdHdrLength + msgDataLength;
                // Set the Calculate CRC flag
                fCalcCRC = TRUE;
                break;

            case WRITE_IPR_REGISTER:
                // ROM or Extended cmd
                cmdType = IBD_CMD_TYPE_DTC;
                // Data has 3 magic bytes: the first is the datalength = FrameLength - 1.  These bytes
                //  have been added on by the segmenting stage though.
                // Format: Cmd->,frame length(1byte)->,data(length bytes)->,<-CRC16(2bytes)
                // Load the frame length from the header
                cmdHdrBuff[ 1] = 
                    dev->Trans.iButtonTxSegHdr[ dev->Trans.iButtonTxCurSegment ]->blockLth;
                cmdHdrLength = 2;
                pMsgData =  dev->Trans.iButtonTxSegBuff[ dev->Trans.iButtonTxCurSegment ];
                msgDataLength = dev->Trans.iButtonTxSegHdr[ dev->Trans.iButtonTxCurSegment ]->blockLth;
                rspLength = 2;
                rspOffset = cmdHdrLength + msgDataLength;

                fCalcCRC = TRUE;
                break;

            case EXTENDED_WRITE_CB:
                // ROM or Extended cmd
                cmdType = IBD_CMD_TYPE_DTC;
                // Format: Cmd->,Length(2bytes,LSBFirst)->,data(Length bytes)->,<-CRC16(2bytes)
                // The first block may have fewer than Length bytes.  The max number of 
                //  data bytes in each block is 256.
                // Load the length of all blocks, Little Endian.
                cmdHdrBuff[ 1 ] = dev->Trans.iButtonTxSegHdr[0]->remainLthLow;
                cmdHdrBuff[ 2 ] = dev->Trans.iButtonTxSegHdr[0]->remainLthHigh;
                cmdHdrLength = 3;
                pMsgData =  dev->Trans.iButtonTxSegBuff[ 0 ];
                // This is max 256.
                msgDataLength = dev->Trans.iButtonTxSegHdr[ 0 ]->blockLth;
                if( dev->Trans.iButtonTxSegHdr[ 0 ]->blockNbr & 0x80 )
                {
                    rspLength = 2;  // After the last block, collect the CRC.
                }
                else
                {
                    rspLength = 0;      // No response until the last block.
                }
                rspOffset = cmdHdrLength + msgDataLength;

                fCalcCRC = TRUE;
                break;


            case START_PROGRAM:         
                // ROM or Extended cmd
                cmdType = IBD_CMD_TYPE_DTC;
                // Format: Cmd->, release sequence(2byte)
                cmdHdrBuff[ 1] = START_PROGRAM_REL & 0xff;
                cmdHdrBuff[ 2] = (START_PROGRAM_REL>>8) & 0xff;
                cmdHdrLength = 3;
                rspLength = 0;
                rspOffset = 0;
#ifdef IBUTTON_LOG
                logDataOffset = 2;  // skip "OD Skip ROM" & Start Program command
#endif
                break;

            case CONTINUE_PROGRAM:
                // ROM or Extended cmd
                cmdType = IBD_CMD_TYPE_DTC;
                // Format: Cmd->, release sequence(2byte)
                cmdHdrBuff[ 1] = CONTINUE_PROGRAM_REL & 0xff;
                cmdHdrBuff[ 2] = (CONTINUE_PROGRAM_REL>>8) & 0xff;
                cmdHdrLength = 3;
                rspLength = 0;
                rspOffset = 0;
                break;
        
            case RESET_MICRO:
                // ROM or Extended cmd
                cmdType = IBD_CMD_TYPE_DTC;
                // Format: Cmd->, release sequence(2byte)-> ?? TEMPORARY FF read a byte.
                cmdHdrBuff[ 1] = RESET_MICRO_REL & 0xff;
                cmdHdrBuff[ 2] = (RESET_MICRO_REL>>8) & 0xff;

                cmdHdrLength = 3;
                rspLength = 1;
                rspOffset = cmdHdrLength;
#ifdef IBUTTON_LOG
                logDataOffset = 2;  // skip "OD Skip ROM" & Reset command
#endif
                break;

            case READ_IO_BUFFER:
#ifdef IBUTTON_LOG
                logDataOffset = 2;  // skip "OD Skip ROM" & Read IO Buf Cmnd
#endif
                // ROM or Extended cmd
                cmdType = IBD_CMD_TYPE_DTC;
                // Format:  Cmd->,length(1byte)->,<-data(8 bytes),<-CRC16(2bytes)
                cmdHdrBuff[ 1] = 0x08;      // Fixed length

                cmdHdrLength = 2;
                //msgDataLength = 8;
                rspLength = 10;
                rspOffset = cmdHdrLength;
                break;  

            case READ_IPR_REGISTER:
#ifdef IBUTTON_LOG
                logDataOffset = 2;  // skip "OD Skip ROM" & Read IPR Reg Cmd
#endif
                // ROM or Extended cmd
                cmdType = IBD_CMD_TYPE_DTC;
                // Format:  Cmd->,length(1byte)->,<-data(length bytes),<-CRC16(2bytes)
                cmdHdrBuff[ 1] = (UINT8)dev->Trans.iButtonRxSegLength; // Only the payload length

                cmdHdrLength = 2;
                // Seg Length should not be larger than 128.
                rspLength = (UINT8)(dev->Trans.iButtonRxSegLength + 2);
                rspOffset = cmdHdrLength;
                break;

            case READ_STATUS:           
                // ROM or Extended cmd
                cmdType = IBD_CMD_TYPE_DTC;
                // Format:  Cmd->,<-Status(6bytes)

                cmdHdrLength = 1;
                rspLength = 6;
                rspOffset = cmdHdrLength;
                break;

            case EXTENDED_READ_CB:
                // ROM or Extended cmd
                cmdType = IBD_CMD_TYPE_DTC;
                // Format:  Cmd->,<-Length(2bytes)
                cmdHdrLength = 1;
                rspLength = 2;
                rspOffset = cmdHdrLength;
                break;
        
            // These are not needed and haven't been implemented
            //case EXT_WRITE_CB_AND_START:
            //case EXT_WRITE_CB_AND_CONTINUE:
            //case MATCH_ROM:             // Since there is a single IButton
            //case SEARCH_ROM:            // Search & Match are superfluous   
            //case OVERDRIVE_MATCH_ROM:   
            default:
                break;
        }
    }

#ifdef IB_DRIVER_DEBUG
    // Check if this is the start of a Compute cycle.
    if( cmd == DS2480_SINGLE_BIT_WRITE_0 )
    {
        // Start a new line in the diag buffer...
        fnIButtonDiagBuffAdd( TRUE, "COMPUTE", 7, FALSE ); 
    }
    else
    {
        // Start a new line in the diag buffer for the Tx data...
        fnIButtonDiagBuffAdd( TRUE, "Tx", 2, FALSE );
    }
#endif      


    switch( cmdType )
    {
        case IBD_CMD_TYPE_BREAK:    
#ifdef IB_DRIVER_DEBUG
            // Indicate the type of message...
            fnIButtonDiagBuffAdd( FALSE, "2B:", 3, FALSE );
#endif      
            dev->Link.iButtonTxLength = cmdHdrLength;
            dev->Link.iButtonNumBytesToReceive = 0;    
            memcpy( dev->Link.iButtonTxBuff, cmdHdrBuff, cmdHdrLength );
            break;
            
        case IBD_CMD_TYPE_DS2480:
#ifdef IB_DRIVER_DEBUG
            if( cmd != DS2480_SINGLE_BIT_WRITE_0 )
            {
                // Indicate the type of message...
                fnIButtonDiagBuffAdd( FALSE, "2C:", 3, FALSE );
            }
#endif      
            if( ucDS2480Mode == DS2480_DATA_MODE )
            {
                dev->Link.iButtonTxLength = cmdHdrLength + 1;    // add one for the cmd mode byte
                dev->Link.iButtonNumBytesToReceive = rspLength;  // there is no response for the cmd mode byte
                ucDS2480Mode = DS2480_CMD_MODE;                  // update the mode status
                dev->Link.iButtonTxBuff[ 0 ] = DS2480_CMD_MODE;   
                memcpy( &dev->Link.iButtonTxBuff[ 1 ], cmdHdrBuff, cmdHdrLength );     
            }
            else if( ucDS2480Mode == DS2480_CMD_MODE )
            {
                dev->Link.iButtonTxLength = cmdHdrLength;
                dev->Link.iButtonNumBytesToReceive = rspLength;    
                memcpy( &dev->Link.iButtonTxBuff[ 0 ], cmdHdrBuff, cmdHdrLength );     
            }
            break;
            
        case IBD_CMD_TYPE_ROM:
#ifdef IB_DRIVER_DEBUG
            // Indicate the type of message...
            fnIButtonDiagBuffAdd( FALSE, "IR:", 3, FALSE );
#endif      
            if( ucDS2480Mode == DS2480_CMD_MODE )
            {
#ifdef IBUTTON_LOG
                logDataOffset++;  // skip 2480 change-to-Data-mode byte.
#endif
                dev->Link.iButtonTxLength = cmdHdrLength + rspLength + 1;         // add one for the data mode byte
                dev->Link.iButtonNumBytesToReceive = (UINT8)dev->Link.iButtonTxLength - 1;  // there is no response for 
                                                                                     //  the cmd mode byte 
                ucDS2480Mode = DS2480_DATA_MODE;                 // update the mode status
                dev->Link.iButtonTxBuff[0] = DS2480_DATA_MODE;
                memcpy( &dev->Link.iButtonTxBuff[ 1 ], cmdHdrBuff, cmdHdrLength );     
            }
            else if( ucDS2480Mode == DS2480_DATA_MODE )
            {
                dev->Link.iButtonTxLength = cmdHdrLength + rspLength;
                dev->Link.iButtonNumBytesToReceive = (UINT8)dev->Link.iButtonTxLength;
                memcpy( &dev->Link.iButtonTxBuff[ 0 ], cmdHdrBuff, cmdHdrLength );     
            }
            break;
        
        case IBD_CMD_TYPE_DTC: 
#ifdef IB_DRIVER_DEBUG
            // Indicate the type of message is I-button DTC
            fnIButtonDiagBuffAdd( FALSE, "ID:", 3, FALSE );
#endif      
            // If we need to calculate the CRC, do so, and store it.
            if( fCalcCRC )
            {
                crc = CRC16Buf( cmdHdrBuff, 0, cmdHdrLength );  // Calculate on the data
                if( pMsgData ) // if there is also message data, calulcate the rest of the crc
                    crc = CRC16Buf( pMsgData, crc, msgDataLength ); // Calculate on the data
                *calcCRC = crc;
            }

            // Set the number of bytes to Rx.  Includes echo of bytes output to
            //  i-button (unstuffed) plus the number of 0xFFs sent in order to Rx data. 
            //  Add 1 for the OD SKIP ROM cmd echo.  
            //  (NOTE: If the DS2480_DATA_MODE byte is sent, it is not echoed back.)
            dev->Link.iButtonNumBytesToReceive = 1 + cmdHdrLength + msgDataLength + rspLength;  
            // Add 1 to the response offset for the echo of the OVERDRIVE_SKIP_ROM command
            rspOffset++;  

            // Now the data must be "stuffed": any E3s are converted to two E3s.
            // Since 0xFFs are sent out in order to receive data, they do not
            //  need to be stuffed (they are preloaded into the background of
            //  the buffer.), but they should be counted in the total Tx length.
            if( ucDS2480Mode == DS2480_CMD_MODE )
            {
                // If the 2480 is not in DataMode, the message must be preceeded by 
                //  a DataMode Command byte.
#ifdef IBUTTON_LOG
                logDataOffset++;    // skip 2480 change-to-Data-mode byte in syslog.
#endif
                ucDS2480Mode = DS2480_DATA_MODE;                 // update the mode status
                dev->Link.iButtonTxBuff[0] = DS2480_DATA_MODE;   // Prepend the DataMode command
                dev->Link.iButtonTxBuff[1] = OVERDRIVE_SKIP_ROM; 
                
                stuffedLength = 2;  // data mode byte + OVERDRIVE_SKIP_ROM byte
            }
            else if( ucDS2480Mode == DS2480_DATA_MODE )
            {
                dev->Link.iButtonTxBuff[0] = OVERDRIVE_SKIP_ROM; 

                stuffedLength = 1;  // OVERDRIVE_SKIP_ROM byte
            }

            // Stuff and load the cmdHdr and the MsgData...
            stuffedLength += iButtonByteStuffCmd( &dev->Link.iButtonTxBuff[ stuffedLength ], 
                                                  cmdHdrBuff, cmdHdrLength ); 
            if( pMsgData )
            {
                stuffedLength += iButtonByteStuffCmd( &dev->Link.iButtonTxBuff[ stuffedLength ], 
                                                      pMsgData, msgDataLength ); 
            }
            // Set the Tx length = stuffed length + response length
            dev->Link.iButtonTxLength = stuffedLength + rspLength;
            break;
            
        case UNKNOWN_CMD_TYPE:
        default:
            break;
    }   

#ifdef IBUTTON_LOG
    // Log the stuffed data, skipping over the OverDriveSkipRom byte and the command byte, and 
    //  the DataMode byte if there is one.  
    // We use logCmd instead of cmd, because some command values have different meanings
    //  depending on the command type, and we don't log the type, so we remap those 
    //  commands just for the syslog.
    fnIbuttonLogEntry( SYSLOG_IBTN_DRV, SL_IBTN_COMMAND, SL_IBTN_DRVR, logCmd, 
                       (char *)&dev->Link.iButtonTxBuff[logDataOffset], 
                       dev->Link.iButtonTxLength - logDataOffset ); 
#endif

    // set the index of where to start reading the response in the Rx buffer
    dev->Link.iButtonRxResponseOffset = rspOffset; 

    // Set the unsent length to the total to start
    dev->Link.iButtonTxUnsentLength = dev->Link.iButtonTxLength; 
  
    // Send the status
    return( iButtonStatus );
}


//*****************************************************************************
// FUNCTION NAME:       fniButtonContinueExtendedWrite
// DESCRIPTION:
//      The boot loader messages may be longer than the UART buffer, so they
//      have been packetized.  The first has the Command prefix, but the 
//      rest just keep sending data until the last packet is sent.
//
//      This function is used in place of iButtonGenerateCmd to load up 
//      the transmit buffer with the next (up to) 256 bytes of data, without a
//      command.  
// ARGUMENTS:
//      pDev - Pointer to the device structure, from which all low-level data 
//              can be found.  (Not sure why we pass this all over the place, 
//              since there is the only one, but all the functions that need 
//              it, pass it.)
// RETURNS:
//      None.  Sends more of the Command Buffer (a.k.a. the IPR).
// NOTES: 
// HISTORY:
//  2009.03.03    Clarisa Bellamy - New function. 
//---------------------------------------------------------------------------*/
IBUTTON_STATUS fniButtonContinueExtendedWrite( IBUTTON_DEVICE *dev, UINT16 *pCrc ) 
{
    UINT8   rspLength = 0;
    UINT16  stuffedLength = 0;
    UINT8   bSegmentNum;
    UINT8   msgDataLength;
    UINT8   rspOffset;
    void *  pMsgData;
    UINT16  uwLocalCrc;

#ifdef IBUTTON_LOG
    unsigned char logDataOffset = 0;              // data follows cmd byte
    unsigned char logCmd = EXTENDED_WRITE_CB;   // command will not be remapped
#endif
 

    // Pre-set the TxBuff for the read segment.  A 0xFF has to be written to the DS2480 for every response byte
    // that is expected to be returned from the iButton.  The 2480 will generate a "write-1" time slot on the
    // 1-wire bus which is the same as a "read-data" time slot.
    memset( dev->Link.iButtonTxBuff, 0xff, TX_BUFF_LEN );

    bSegmentNum = dev->Trans.iButtonTxCurSegment;
    // Format for middle block:   data(length bytes)->                  
    // Format for last block:     data(length bytes)->,<-CRC16(2bytes)  
    pMsgData =  dev->Trans.iButtonTxSegBuff[ bSegmentNum ];
    msgDataLength = dev->Trans.iButtonTxSegHdr[ bSegmentNum ]->blockLth;
    if( dev->Trans.iButtonTxSegHdr[ bSegmentNum ]->blockNbr & 0x80 )
    {
        rspLength = 2;  // After the last block, collect the CRC.
    }
    else
    {
        rspLength = 0;      // No response until the last block.
    }
    rspOffset = msgDataLength;

    // Continue to calculate the crc... starting with the crc from the last block...
    uwLocalCrc = CRC16Buf( pMsgData, *pCrc, msgDataLength ); 
    *pCrc = uwLocalCrc;
    
    // Number of virtual bytes to send/receive:
    dev->Link.iButtonNumBytesToReceive = rspLength + msgDataLength; 
    
    // Now we must convert any accidental E3s to E3,E3.
    stuffedLength = 0;  
    stuffedLength += iButtonByteStuffCmd( &dev->Link.iButtonTxBuff[ stuffedLength ], 
                                          pMsgData, msgDataLength ); 
    // Set the number of bytes to actually send.
    dev->Link.iButtonTxLength = stuffedLength + rspLength;

#ifdef IBUTTON_LOG
    fnIbuttonLogEntry( SYSLOG_IBTN_DRV, SL_IBTN_COMMAND, SL_IBTN_DRVR, logCmd, 
                      (char *)&dev->Link.iButtonTxBuff[logDataOffset], 
                      dev->Link.iButtonTxLength - logDataOffset ); 
#endif
    // set the index of where to start reading the response in the Rx buffer
    dev->Link.iButtonRxResponseOffset = rspOffset; 

    // Set the unsent length to the total to start
    dev->Link.iButtonTxUnsentLength = dev->Link.iButtonTxLength; 

#ifdef IB_DRIVER_DEBUG
    // Start a new line in the diag buffer...
    fnIButtonDiagBuffAdd( TRUE, "TxX:", 4, FALSE ); 
#endif      
        
    // Send the status
    return( IBUTTON_OK );
}


//*****************************************************************************
// FUNCTION NAME:       fniButtonContinueExtendedRead
// DESCRIPTION:
//      The boot loader messages do not use the I/O buffer, so the length
//      of the response must be known, OR come from the iButton in the 
//      Extended Read CB message reply. 
//      After receiving the length bytes, in an ExtendedReadCommandBuffer 
//      message, proceed to read the rest of the buffer, up to 256 bytes 
//      at a time.
//      This function is used in place of iButtonGenerateCmd to load up 
//      the transmit buffer with up to 256 0xFFs to send out, without a
//      command, in order to read the correct number of bytes in this bunch.
// ARGUMENTS:
//      pDev - Pointer to the device structure, from which all low-level data 
//              can be found.  (Not sure why we pass this all over the place, 
//              since there is the only one, but all the functions that need 
//              it, pass it.)
// RETURNS:
//      None.  Sends FFs to read more of the Command Buffer (a.k.a. the IPR).
// NOTES: 
//---------------------------------------------------------------------------*/
IBUTTON_STATUS fniButtonContinueExtendedRead( IBUTTON_DEVICE *dev  ) 
{
    UINT16  rspLength = 0;
    UINT16  *pwRspLenRemaining;
    UINT16  bMaxSegmentLen;

    bMaxSegmentLen = RX_BUFF_LEN;

    pwRspLenRemaining = &(dev->Link.iButtonExtCmdRxLenRemaining);
    // Pre-set the TxBuff for the read segment.  A 0xFF has to be written to the DS2480 for every response byte
    // that is expected to be returned from the iButton.  The 2480 will generate a "write-1" time slot on the
    // 1-wire bus which is the same as a "read-data" time slot.
    memset( dev->Link.iButtonTxBuff, 0xff, TX_BUFF_LEN );
    if( *pwRspLenRemaining > 0 )
    {
        // Get the data 256 bytes at a time, due to size limit of iButtonRxDataBuff.
        if( *pwRspLenRemaining > bMaxSegmentLen )
        {
            rspLength = bMaxSegmentLen;  
        }
        else
        {
            rspLength = *pwRspLenRemaining;
        }
        *pwRspLenRemaining -= rspLength;

        dev->Link.iButtonNumBytesToReceive = (UINT8)rspLength; 
        dev->Link.iButtonTxLength = rspLength;
    }   

    // Set the index of where to start reading the response in the Rx buffer.
    // There is no data to transmit, only receive.
    dev->Link.iButtonRxResponseOffset = 0; 

    // Set the unsent length to the total to start
    dev->Link.iButtonTxUnsentLength = dev->Link.iButtonTxLength; 
#ifdef IB_DRIVER_DEBUG
    // Set up header for transmission of FFs for extended read...
    fnIButtonDiagBuffAdd( TRUE, "TxX:", 4, FALSE ); 
#endif      
        
    // Send the status
    return( IBUTTON_OK );
}

/******************************************************************************/
/*                                                                            */
/*  Function Name: iButtonByteStuffCmd                                        */
/*                                                                            */
/*  Author: Tom Dometios                                                      */
/*                                                                            */
/*  Function Description:                                                     */
/*      This function checks the data iButton command data stream to see if   */
/*      it contains the reserved code used to switch the DS2480 back to from  */
/*      data mode to command mode. This reserved code = 0xE3.  If this byte   */
/*      is detected in the data stream, then it is sent twice which instructs */
/*      the 2480 to stay in data mode and transmit one byte of 0xE3 as a      */
/*      data byte to the iButton.                                             */
/*                                                                            */
/*  Inputs:                                                                   */
/*                                                                            */
/*  Outputs:                                                                  */
/*                                                                            */
/*                                                                            */
/*  Returns:  Number of bytes in destBuf including stuffing                                                               */
/*                                                                            */
/******************************************************************************/
UINT16 iButtonByteStuffCmd( UINT8 *destBuf, const UINT8 *sourceBuf, 
                            UINT8 length ) 
{
    UINT16 i, j;
    
    for( i = 0, j = 0; i < length; i++, j++ )
    {
        if( sourceBuf[ i ] ==  DS2480_CMD_MODE )
        {
            destBuf[ j++ ] = sourceBuf[ i ];
            destBuf[ j ] = DS2480_CMD_MODE;    
        }
        else
        {
            destBuf[ j ] = sourceBuf[ i ]; 
        }
    }
    
    return( j );
}

/******************************************************************************/
/*                                                                            */
/*  Function Name: iButtonGenerateReleasSeq                                   */
/*                                                                            */
/*  Author: Mike Ernandez/modified by Tom Dometios                            */
/*                                                                            */
/*  Function Description:                                                     */
/*      Except for the IPR and reading the Status Reg., communication with    */
/*      the iButton requires not only issuing the appropriate command         */
/*      sequence but also providing a command-dependent, 16-bit release       */
/*      sequence at the appropriate time.  Depending on the command the       */
/*      iButton will either send back a byte or a single bit to the host      */
/*      to confirm acceptance of the release sequence.  In the case of a      */
/*      single bit response, a '1' indicates that the release sequence was    */
/*      not accepted, a '0' indicates that it was accepted.  In the case of a */
/*      byte, all 1's indicates that the release sequence was not accepted    */
/*      and all 0's indicates it was.                                         */ 
/*      Note: in most if not all cases, we ignore the release sequence        */
/*            response byte or bit since a problem will show up in a          */
/*            bad CRC anyway.                                                 */
/*  Inputs:                                                                   */
/*                                                                            */
/*  Outputs:                                                                  */
/*                                                                            */
/*                                                                            */
/*  Returns:                                                                  */
/*                                                                            */
/******************************************************************************/
IBUTTON_STATUS iButtonGenerateReleaseSeq( unsigned short releaseSeq, IBUTTON_DEVICE *dev)
{

        IBUTTON_STATUS  iButtonStatus = IBUTTON_OK;

        // Each release sequence format is release(2 bytes)
        dev->Link.iButtonTxBuff[ 0 ] = releaseSeq & 0xff;           // Lower byte
        dev->Link.iButtonTxBuff[ 1 ] = (releaseSeq>>8) & 0xff;  // Upper byte
        dev->Link.iButtonTxBuff[ 2 ] = 0xff; // for readback of rel.seq. confirmation byte

#ifdef IBUTTON_LOG
        fnIbuttonLogEntry( SYSLOG_IBTN_DRV, SL_IBTN_REL_SEQ, SL_IBTN_DRVR, 
                           0, (char *)dev->Link.iButtonTxBuff, 3 ); 
#endif
#ifdef IB_DRIVER_DEBUG
        // Start a new line in the diag buffer... (wrap if 2 more bytes won't fit.)
        fnIButtonDiagBuffAdd( TRUE, "RSQ:", 4, TRUE );
#endif

        // Set the length - 3 bytes in the release sequence
        dev->Link.iButtonTxLength = dev->Link.iButtonTxUnsentLength = 3;     
        dev->Link.iButtonNumBytesToReceive = 3;
        dev->Link.iButtonRxResponseOffset = 2;
        // Return the status
        return( iButtonStatus);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name: iButtonCheckCRC                                            */
/*                                                                            */
/*  Author: Mike Ernandez/modified by Tom Dometios                            */
/*                                                                            */
/*  Function Description:                                                     */
/*      The process of checking the crc is broken down according to the       */
/*      command type as follows:                                              */
/*      "Write" commands:                                                     */
/*          When the message block for a write command is assembled, the crc  */
/*          is calculated and tucked away.  When the iButton response is      */
/*          received, the last 2 bytes of the response are the inverted crc   */
/*          of the data the iButton received, i.e., the data block that was   */
/*          transmitted by the host.  The inverted crc is inverted again      */
/*          and checked against the crc that was tucked away.                 */
/*      "Read" commands:                                                      */
/*          The host calculates the crc of the received data from the iButton */
/*          response and if applicable, adds the command and length bytes     */
/*          to the crc calulation.  As with the "write" commands, the last 2  */
/*          bytes of the response are the inverted crc of the response, not   */
/*          including the crc itself.  This crc is inverted and compared to   */
/*          the crc that was calculated.                                      */            
/*                                                                            */
/*                                                                            */
/*  Inputs:                                                                   */
/*  Outputs:                                                                  */
/*  Returns:                                                                  */
//  NOTES:
//----------------------------------------------------------------------------/
IBUTTON_STATUS iButtonCheckCRC( UINT8 cmd, UINT16 calcCRC, const IBUTTON_DEVICE *dev )
{
    unsigned short crc = calcCRC;
    unsigned char invertedBuffCRC[2];
    unsigned short length;

    // Get the length & offset from the converted buffer
    length = dev->Link.iButtonRxResponseLength - 2; // don't include the 2 crc bytes

    // Check the calculated with what was received
    switch( cmd )
    {
        case READ_STATUS:
            // Calculate the crc Rx data
            crc = CRC16( cmd, 0);
            crc = CRC16Buf( &dev->Link.iButtonRxResponseBuff[ 0 ], crc, length );
            break;
        
        case READ_IO_BUFFER:
        case READ_IPR_REGISTER:
            // Calculate the crc Rx data 
            crc = CRC16( cmd, 0);
            crc = CRC16( (UINT8)length, crc);
            crc = CRC16Buf( &dev->Link.iButtonRxResponseBuff[ 0 ], crc, length );
            break;

        case EXTENDED_READ_CB:
            // Calculate the crc Rx data 
            crc = CRC16( cmd, 0);
            crc = CRC16( (UINT8)(length & 0xFF), crc);
            crc = CRC16( (UINT8)(length >> 8), crc);
            crc = CRC16Buf( &dev->Link.iButtonRxResponseBuff[ 0 ], crc, length );
            break;


        case WRITE_STATUS:
        case WRITE_IO_BUFFER:
        case WRITE_IPR_REGISTER:
        case EXTENDED_WRITE_CB:
           // Use the previously calculated crc for the write commands
            crc = calcCRC;
            break;

        default:
            break;
    }

    // Get the buffer CRC and invert
    invertedBuffCRC[ 0] = ~dev->Link.iButtonRxResponseBuff[ length ];
    invertedBuffCRC[ 1] = ~dev->Link.iButtonRxResponseBuff[ length + 1 ];
    
    // Return the result
    if( *(unsigned short*)invertedBuffCRC == crc )
    {
#ifdef IBUTTON_LOG
        fnIbuttonLogEntry( SYSLOG_IBTN_DRV, SL_IBTN_CRC_OK, SL_IBTN_DRVR, cmd,
                           (char *)dev->Link.iButtonRxResponseBuff, 
                           dev->Link.iButtonRxResponseLength ); 
#endif
        return( IBUTTON_OK);
    }
    else
    {
#ifdef IBUTTON_LOG
        fnIbuttonLogEntry( SYSLOG_IBTN_DRV, SL_IBTN_CRC_FAIL, SL_IBTN_DRVR, cmd,
                           (char *)dev->Link.iButtonRxResponseBuff, 
                           dev->Link.iButtonRxResponseLength ); 
#endif
        return( IBUTTON_CRC_ERROR);
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name: iButtonPutTxFIFO                                           */
/*                                                                            */
/*  Author: Mike Ernandez/modified by Tom Dometios                            */
/*                                                                            */
/*  Function Description:                                                     */
/*      Loads the ASIC UART's 16-byte transmit fifo. If iButtonStartFlag      */
/*      is TRUE, this means that all of the data for this command has been    */
/*      sent and we are now going to send the "start bit" using the           */
/*      DS2480_SINGLE_BIT_WRITE_0 command.  As soon as all of the data has    */
/*      been sent from the UART, i.e., the transmit shift register and fifo   */
/*      are empty, the strong pullup that supplies the extra power necessary  */
/*      for the compute cycle will be enabled.                                */
/*  Inputs:                                                                   */
/*                                                                            */
/*  Outputs:                                                                  */
/*                                                                            */
/*                                                                            */
/*  Returns:                                                                  */
/*                                                                            */
/******************************************************************************/

void iButtonPutTxFIFO( unsigned short iButtonStartFlag, IBUTTON_DEVICE *dev)
{
    unsigned char ucTemp;
    unsigned long i;
    

    // Put the data in the FIFO. Always start writing from the current index.  However, we need to check for
    // any byte stuffed data, i.e., 0xe3 0xe3.  When we wish to send a data byte of 0xe3 to the iButton, we have to 
    // send 0xe3 twice.  The reason for this is that 0xe3 is the command byte that is used to put the DS2480 into
    // command mode, thus if we don't want to put the 2480 into command mode, sending 0xe3 twice tells the 2480 that
    // we are just sending a data byte of 0xe3 to the iButton.  Consequently, the first 0xe3 is just dropped by the 
    // 2480 so no response byte is generated for it.  This means that if this pattern is detected when we are 
    // filling the transmit FIFO, we subtract 1 from the number of bytes that are expected to be returned in the 
    // receive FIFO. Remember that this message had already been byte stuffed in the function iButtonGenerateCmd.

    ucTemp = dev->Link.iButtonTxBuff[ dev->Link.iButtonTxCurrentIndex++ ];

    if( dev->Link.iButtonTxUnsentLength > 1 ) 
    {
        dev->Link.iButtonTxUnsentLength -= 1;
    }
    else 
    {
        dev->Link.iButtonTxCurrentIndex = 0;
        dev->Link.iButtonTxUnsentLength = 0;
    }

    (*iButtonChannel.uartControl->fnWriteFIFO)( dev->pbUartBase , ucTemp);

	DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xFF;
	DebugIBtnDriver[DebugIBtnDriverCtr++] = ucTemp;

#ifdef IBUTTON_TEST_BUFFER
    iButtonBuf[iButtonBufIdx++] = 's';
    iButtonBufIdx %= IBUTTON_TEST_BUFFER_SIZE;
    iButtonBuf[iButtonBufIdx++] = ucTemp;
    iButtonBufIdx %= IBUTTON_TEST_BUFFER_SIZE;
#endif

#ifdef IB_DRIVER_DEBUG
    // Add byte to be transmitted to the diagnostic buffer, don't start a new line. 
    fnIButtonDiagBuffAdd( FALSE, &ucTemp, 1, FALSE ); 
#endif      
           
    // Check for strong pull-up needed
    if( iButtonStartFlag ) 
    {   
// TODO move to HAL
#if 0 //move to HAL
		//Setupu DMA to raise the strong pull up after the last part of the message has been sent
        ulSrc = 0xA0000000 | ( unsigned long ) &ucStrongPU;
        ucStrongPU = *dev->puPort;
        ucStrongPU |= dev->puMask; // enable the strong pull-up
        ulDes = ( unsigned long ) dev->puPort; // enable the strong pull-up
    
        //Setup the compare match timer and dma to assert the strong pullup
        CMCSR4 &= ~CMT4START;
        DMASAR4 = ulSrc;
        DMADAR4 = ulDes;
        DMARS2 &= 0xFF00;
        DMARS2 |= DMA4MODE;
        DMATCR4 = 1;          //Transfer a single byte
        DMACR4 = DMA4CRINIT;  //Initialize first
        DMACR4 |= DMA4CRFINAL; // then enable
        DMAOP |= DMAMASTERENB; //master enable  for all dma channels
        CMCNT4 = 0;
        CMCOR4 = ulCmt4Cor;
        CMSTR |= CSR4VAL;
        CMCSR4 = CMT4START;
#else
		//Not DMA, doing polling
		// Disable ALL Interrupts in the system because it is critical that the strong pullup is enabled immediately
		// after the start bit is sent otherwise, the compute cycle could start before the strongpullup is enabled.
	    
		// Wait for the xmitter to be empty before enabling the strong pull-up.  If the strong pull-up is activated
	    // too soon, the start bit that is generated by the DS2480 will not go down to 0 volts because the low-side 
	    // driver of the DS2480 is too weak to sink that much current.
        // LMD while( !( *( pUartBase + STAT_REG ) & TDFE ) ) ;
        
		
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xFC;
		while( !( *( dev->pbUartBase + AM3X_UARTLSR_OFFSET ) & AM3X_UARTLSR_TX_EMPTY ) ) ;
        // need to wait a little longer
        for (i=0; i<1000; i++); 
        
		// Set the strong pull-up
		iButtonPullUp( PULLUP_ON , dev->puPort , dev->puMask , dev->stMask);
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xFD;

#endif
    }  
    // - OLD Method - Changed so don't have to disable interrupts! DM400c has 139 usec clk. We missed clokc pulses if
    // disabled too long!!!
    
    // Non longer applies....Wait for the xmitter to be empty before enabling the strong pull-up.  If the strong pull-up is activated
    // too soon, the start bit that is generated by the DS2480 will not go down to 0 volts because the low-side 
    // driver of the DS2480 is too weak to sink that much current.
           
    // Enable the Tx/Rx interrupts - ONLY if a regular byte - DMA IRQ will restore after SPU asserted for CMD bytes
    if( !iButtonStartFlag ) 
    {
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xFB;
		(*iButtonChannel.uartControl->fnEnableReceive)( dev->pbUartBase );
		(*iButtonChannel.uartControl->fnEnableTransmitIrq)( dev->pbUartBase );
    }
}


//=====================================================================
//
//  Method Name:    iButton_DMA_ISR
//
//  Description: After we use the DMA to assert the strong pullup
//      we need to enable the Uart TX interrupt to complete
//              the operation
//
//  Parameters: IBUTTON_DEVICE *dev
//      
//      
//      
//  Returns: NONE
//      
//
//  Preconditions:
//                  
//
//=====================================================================
void iButton_DMA_ISR( const IBUTTON_DEVICE *dev )
{
	// TODO confirm this is correct
#if 0
  unsigned short *pUartBase = (unsigned short *)dev->pbUartBase;

    DMACR4 &= ~DMA4CRFINAL; // Clear Irq Enable/Dma Enable

    *(volatile UINT32 *)(AM335X_GPIO1_BASE + GPIO_IRQSTATUS_SET_1) = IBTN_DONE; // The dma/cmt has asserted the IBUTTON_SPU - enable for I-button response
  *( pUartBase + CTRL_REG ) |= TIE+TE+RE; 
#endif
// TODO: JAH   HALClearIButtonIRQ();
    (*iButtonChannel.uartControl->fnEnableTransmitIrq)( dev->pbUartBase );
    (*iButtonChannel.uartControl->fnEnableReceive)( dev->pbUartBase );

}

/******************************************************************************/
/*                                                                            */
/*  Function Name: void iButtonDisableInts                                  */
/*                                                                            */
/*  Author: Mike Ernandez/modified by Tom Dometios                           */
/*                                                                            */
/*  Function Description:                                                     */
/*      This function disables all iButton interrupts by saving the           */
/*      enable/disable status of the interrupts so that they can be restored  */
/*      to their pre-disable state.  This function is called prior to setting */
/*      an iButton event.  The reason for this is that the iButton events can */
/*      be set by both an LISR and HISR so by disabling iButton interrupts    */
/*      first, we avoid any possible contention for this event.               */
/*                                                                            */
/*  Inputs:                                                                   */
/*                                                                            */
/*  Outputs:                                                                  */
/*                                                                            */
/*                                                                            */
/*  Returns:                                                                  */
/*                                                                            */
/******************************************************************************/
UINT32 iButtonDisableInts( const IBUTTON_DEVICE *dev )
{
    UINT32  iButtonIVal = 0;
    P_GPIO_REGLAYOUT pGPIOReg 	= (P_GPIO_REGLAYOUT) dev->puPort;

	// Get the current UART interrupt status, store in upper word
    iButtonIVal = (unsigned long)((*(volatile UINT16 *)(dev->pbUartBase + AM3X_UARTIER_OFFSET))<<16);

	if (( pGPIOReg->GPIO_IRQSTATUS_1 & IBTN_DONE) == IBTN_DONE)
    // Get the current status of the IBUtton Done interrupt setting
    iButtonIVal |= 1;

    // Disable UART and Ibutton done
    *(volatile UINT16 *)(dev->pbUartBase + AM3X_UARTIER_OFFSET) = 0x0000;
    pGPIOReg->GPIO_IRQSTATUS_CLR_1 = IBTN_DONE;

	// Return the saved value
    return( iButtonIVal);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name: void iButtonRestoreInts                                    */
/*                                                                            */
/*  Author: Mike Ernandez/modified by Tom Dometios                            */
/*                                                                            */
/*  Function Description:                                                     */
/*      This function restores iButton interrupt to the state saved by the    */
/*      iButtonDisableInts function.  This will be called after setting an    */
/*      iButton event.                                                        */
/*                                                                            */
/*  Inputs:                                                                   */
/*                                                                            */
/*  Outputs:                                                                  */
/*                                                                            */
/*                                                                            */
/*  Returns:                                                                  */
/*                                                                            */
/******************************************************************************/
void iButtonRestoreInts( const IBUTTON_DEVICE *dev, UINT32 iButtonRVal ) 
{
    P_GPIO_REGLAYOUT pGPIOReg 	= (P_GPIO_REGLAYOUT) dev->puPort;

	if((iButtonRVal & 1) == 1)
	{
		pGPIOReg->GPIO_IRQSTATUS_SET_1 = IBTN_DONE;
	}

    // Restore UART Interrupts from the high word of iButtonRVal
	*(volatile UINT16 *)(dev->pbUartBase + AM3X_UARTIER_OFFSET) = (unsigned short)(iButtonRVal>>16);

}

/******************************************************************************/
/*                                                                            */
/*  Function Name: iButtonSetPullUp                                           */
/*                                                                            */
/*  Author: Mike Ernandez/modified by Tom Dometios                            */
/*                                                                            */
/*  Function Description:                                                     */
/*      This function enables the strong pull-up FET to supply the extra      */
/*      current required by the iButton during it's compute cycle.            */
/*  Inputs:                                                                   */
/*                                                                            */
/*  Outputs:                                                                  */
/*                                                                            */
/*                                                                            */
/*  Returns:                                                                  */
/*                                                                            */
/******************************************************************************/
IBUTTON_STATUS iButtonPullUp( unsigned short OnOff , volatile unsigned char *puPort,
                  unsigned char puMask, unsigned char stMask)
{
    P_GPIO_REGLAYOUT pGPIOReg 	= (P_GPIO_REGLAYOUT) puPort;

    pGPIOReg->GPIO_IRQSTATUS_0 = IBTN_DONE; //Clears any interrupt request
    pGPIOReg->GPIO_IRQSTATUS_1 = IBTN_DONE; //Clears any interrupt request

    if( OnOff)  // Enable the strong pull-up 
    { 
		pGPIOReg->GPIO_SETDATAOUT = IBTN_PULLUP;   // enable/turn on strong pull up (SPU)

        
        // NOTE: WE ARE NOT ENABLING THE CURRENT SENSE FLIP-FLOP AT THIS POINT BECAUSE
        //       A FALSE INTERRUPT WILL GET GENERATED WITH THE RISE OF THE 1-WIRE LINE.
        //       EVEN THOUGH THE STRONG PULL-UP IS ENABLED FIRST, THE CONTROLLED EDGE RATES
        //       RESULT IN THE CURRENT SENSE FLIP-FLOP BEING ENABLED FIRST.  INSTEAD, WE ENABLE
        //       THE FLIP-FLOP IN THE "START_BIT_DONE" SUB-STATE.  EVEN WITH THIS DELAY,
        //       THE FLOP-FLOP IS ENABLED WAY BEFORE THE SHORTEST COMPUTE CYCLE IS OVER
        //       SO THIS IS NOT A PROBLEM.

        // Enable the spare interrupt input in the FPGA.
        // The output of the current sense flip-flop goes into this input to generate an interrupt
        pGPIOReg->GPIO_IRQSTATUS_SET_0 = IBTN_DONE;
        pGPIOReg->GPIO_IRQSTATUS_SET_1 = IBTN_DONE;
    }
    else    // Disable the strong pull-up 
    {           
        pGPIOReg->GPIO_IRQSTATUS_CLR_0 = IBTN_DONE;
        pGPIOReg->GPIO_IRQSTATUS_CLR_1 = IBTN_DONE;

		pGPIOReg->GPIO_CLEARDATAOUT = IBTN_PULLUP;   // disable/turnoff strong pull up (SPU)
		pGPIOReg->GPIO_CLEARDATAOUT = IBTN_INT_ENB;  // disable/inhibit the current sense flip-flop by clearing it
    }
    
    return( IBUTTON_OK);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name: void iButton_Compu_Done_ISR                                */
/*                                                                            */
/*  Author: Mike Ernandez                                                     */
/*                                                                            */
/*  Function Description:                                                     */
/*      This ISR is invoked at the end of an iButton compute cycle.  There    */
/*      is a current sense ciruit that senses a drop is current draw by the   */
/*      iButton which generates an IRQ into the ASIC.  The ASIC in turn       */
/*      encodes this into IRQ(0:3) into the 7720 uP.  This ISR turns off the  */
/*      strong pullup FET that supplied the high current for the compute      */
/*      cycle, sets the EV_IB_COMP_DONE event flag and activates the HISR     */
/*      to advance the state machine.                                         */
/*                                                                            */
/*                                                                            */
/*  Inputs:                                                                   */
/*                                                                            */
/*  Outputs:                                                                  */
/*                                                                            */
/*                                                                            */
/*  Returns:                                                                  */
/*                                                                            */
/******************************************************************************/
void iButton_Compu_Done_ISR( IBUTTON_DEVICE *dev )
{
    unsigned long ival;

    // Disable the interrupt, turn off the pull-up
    (void)iButtonPullUp( PULLUP_OFF, dev->puPort, dev->puMask, dev->stMask );

    // Signal the event and invoke the HISR
    // Disable
    ival = NU_Local_Control_Interrupts(NU_DISABLE_INTERRUPTS);
    // Set the event
    dev->lwActivateEvent |= EV_IB_COMP_DONE;
    // Enable Interrupts
    NU_Local_Control_Interrupts(ival);

#ifdef IB_DRIVER_DEBUG
    // Start a new line in the diag buffer... 
    fnIButtonDiagBuffAdd( TRUE, "DONE", 4, FALSE ); 
#endif

    (void)OSActivateHISR( (OS_HISR *)&dev->devHisr );
								DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xDC;
								DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) dev->lwActivateEvent;
}

unsigned char rcvData=0xff, rcvError=0;
//static unsigned short regVal;


/******************************************************************************/
/*                                                                            */
/*  Function Name: void iButton_TxRx_ISR                                      */
/*                                                                            */
/*  Author: Mike Ernandez/modified by Tom Dometios                            */
/*                                                                            */
/*  Function Description:                                                     */
/*      This is the low level interrupt service routine (LISR) that handles   */
/*      the data flow between the ASIC UART and the DS2480/iButton.           */
/*      There is one vector for both the transmit and receive interrupts      */
/*      thus this routine handles both.  The basic sequence for generating    */
/*      an interrupt is as follows:                                           */
/*      1.  The UART transmits the data in the Tx FIFO which then generates   */
/*          a TXRDY IRQ and invokes this ISR.                                 */
/*      2.  Since this is a xmit interrupt, the xmit interrupts are disabled  */
/*          to prevent any more xmit interrupts until this one is serviced.   */
/*      3.  Assuming the command that was just sent is supposed to generate   */
/*          a response, the UART status register is checked until the         */
/*          RXRDY_IRQ bit is set.  This indicates that the number of bytes    */
/*          in the Rx FIFO has reached the FIFO count.                        */
/*      4.  At this point, the UART's Rx timeout counter is started and the   */
/*          data in the Rx FIFO is copied into the RxDataBuff until the       */
/*          number of bytes that are supposed to be in the Rx FIFO is reached */
/*          OR there is an Rx timeout.                                        */
/*      5.  If TxUnsentLength != 0, then more data is sent to the Tx FIFO and */
/*          whole thing repeats.  If it is 0, then all the data has been sent */
/*          and  OSActivateHISR is called to advance the state machine and    */
/*          we are done here!                                                 */
/*                                                                            */
/*  Inputs:                                                                   */
/*                                                                            */
/*  Outputs:                                                                  */
/*                                                                            */
/*                                                                            */
/*  Returns:                                                                  */
/*                                                                            */
/******************************************************************************/
void iButton_Rx_ISR( IBUTTON_DEVICE *dev )
{
    volatile unsigned short *regLSR;
    volatile unsigned short *regFIFOLvl;
    unsigned long ival;

    regLSR = (volatile unsigned short *)(dev->pbUartBase + AM3X_UARTLSR_OFFSET);
    regFIFOLvl = (volatile unsigned short *)(dev->pbUartBase + AM3X_UARTRXFIFO_LVL);

    if ( *regLSR & AM3X_UARTLSR_FIFO_ERROR )
    {
        while ( *regLSR & AM3X_UARTLSR_FIFO_ERROR )
        {
            while ( *regFIFOLvl != 0 )       // LMD
            {
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xFA;
                rcvError = (*iButtonChannel.uartControl->fnReadFIFO)( dev->pbUartBase);
#ifdef IBUTTON_TEST_BUFFER
                iButtonBuf[iButtonBufIdx++] = '*';
				iButtonBufIdx %= IBUTTON_TEST_BUFFER_SIZE;
#endif
            }
        }
    }
    else    // no errors, read data
    {
		while ( *regFIFOLvl != 0 )
		{
			rcvData = (*iButtonChannel.uartControl->fnReadFIFO)( dev->pbUartBase);
			dev->Link.iButtonRxDataBuff[dev->Link.iButtonRxDataIndex++] = rcvData;
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xFE;
	DebugIBtnDriver[DebugIBtnDriverCtr++] = rcvData;
	#ifdef IBUTTON_TEST_BUFFER
			iButtonBuf[iButtonBufIdx++] = rcvData;
			iButtonBufIdx %= IBUTTON_TEST_BUFFER_SIZE;
	#endif

			if ( dev->Link.iButtonNumBytesToReceive == dev->Link.iButtonRxDataIndex )
			{
				// Disable interrupts
				ival = iButtonDisableInts( dev );

				// Set the event
				dev->lwActivateEvent |= EV_IB_RCV_DONE;

				// Enable Interrupts
				iButtonRestoreInts( dev, ival);
	DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xF9;

				// Invoke the HISR
				dev->Link.iButtonRxDataLength = dev->Link.iButtonRxDataIndex;
				dev->Link.iButtonRxDataIndex = 0;    
				(void)OSActivateHISR( (OS_HISR *)&dev->devHisr );
								DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xDC;
								DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) dev->lwActivateEvent;
				break;
			}
		}
    }
DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xF5;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name: void iButton_TxRx_ISR                                      */
/*                                                                            */
/*  Author: Mike Ernandez/modified by Tom Dometios                            */
/*                                                                            */
/*  Function Description:                                                     */
/*      This is the low level interrupt service routine (LISR) that handles   */
/*      the data flow between the ASIC UART and the DS2480/iButton.           */
/*      There is one vector for both the transmit and receive interrupts      */
/*      thus this routine handles both.  The basic sequence for generating    */
/*      an interrupt is as follows:                                           */
/*      1.  The UART transmits the data in the Tx FIFO which then generates   */
/*          a TXRDY IRQ and invokes this ISR.                                 */
/*      2.  Since this is a xmit interrupt, the xmit interrupts are disabled  */
/*          to prevent any more xmit interrupts until this one is serviced.   */
/*      3.  Assuming the command that was just sent is supposed to generate   */
/*          a response, the UART status register is checked until the         */
/*          RXRDY_IRQ bit is set.  This indicates that the number of bytes    */
/*          in the Rx FIFO has reached the FIFO count.                        */
/*      4.  At this point, the UART's Rx timeout counter is started and the   */
/*          data in the Rx FIFO is copied into the RxDataBuff until the       */
/*          number of bytes that are supposed to be in the Rx FIFO is reached */
/*          OR there is an Rx timeout.                                        */
/*      5.  If TxUnsentLength != 0, then more data is sent to the Tx FIFO and */
/*          whole thing repeats.  If it is 0, then all the data has been sent */
/*          and  OSActivateHISR is called to advance the state machine and    */
/*          we are done here!                                                 */
/*                                                                            */
/*  Inputs:                                                                   */
/*  Outputs:                                                                  */
/*  Returns:                                                                  */
// NOTES: 
// HISTORY:
//  2009.02.11 Clarisa Bellamy - Replace direct access of HISR substate 
//                  with calls to wrapper functions.     
//----------------------------------------------------------------------------/
void iButton_Tx_ISR( IBUTTON_DEVICE *dev )
{
    unsigned long       ival;
    IB_SUB_STATES_E     uwHISRsubState;
	int didSomething = 0;

	*(dev->pbUartBase + AM3X_UARTIER_OFFSET) &= ~AM3X_UARTIER_TXHR;

    if(   ( dev->Link.iButtonTxUnsentLength == 0 ) 
       && ( dev->Link.iButtonNoResponse == TRUE )  )
    {
		didSomething++;
        // nothing left to send and NOT waiting for a response OR
        // NOT waiting for a response
        // set event and invoke HISR

        // Disable interrupts
        ival = iButtonDisableInts( dev );

        // Set the event
        dev->lwActivateEvent |= EV_IB_RCV_DONE;

        // Enable Interrupts
        iButtonRestoreInts( dev, ival);

		DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xF8;
        // Invoke the HISR
        (void)OSActivateHISR( (OS_HISR *)&dev->devHisr );
        dev->Link.iButtonRxDataLength = dev->Link.iButtonRxDataIndex;
        dev->Link.iButtonRxDataIndex = 0;
 								DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xDC;
								DebugIBtnDriver[DebugIBtnDriverCtr++] = (UINT8) dev->lwActivateEvent;
   }
            
    if( dev->Link.iButtonTxUnsentLength != 0 )
    {
		didSomething++;
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xF7;
       // more to send 
        // Write the next byte
        uwHISRsubState = fnIbuttonGetHISRSubState();
        // If this is the last byte, certain states need to have the pullup set.
        if(   (dev->Link.iButtonTxUnsentLength == 1)
           && (   (uwHISRsubState == START_BIT_DONE) 
               || (uwHISRsubState == READ_CONTINUE_PROGRAM_DONE) ) )
        // Write the next byte
        {
            iButtonPutTxFIFO( START, dev);
        }
        else
        {
            iButtonPutTxFIFO( NO_START, dev);
        }
    }
	if(didSomething == 0)
	{
		DebugIBtnDriver[DebugIBtnDriverCtr++] = 0xF6;
	}
}


/******************************************************************************/
/*                                                                            */
/*  Function Name: iButtonInitUart                                            */
/*                                                                            */
/*  Author: Tom Dometios                                                      */
/*                                                                            */
/*  Function Description:                                                     */
/*      Sets up the ASIC UART for communication with the DS2480 ibutton       */
/*      driver chip.                                                          */
/*                                                                            */
/*                                                                            */
/*  Inputs:                                                                   */
/*      iButtonBRRValue:   clock divisor for baud rate generator              */
/*                                                                            */
/*  Outputs:                                                                  */
/*                                                                            */
/*                                                                            */
/*  Returns:                                                                  */
/*                                                                            */
/******************************************************************************/
void iButtonInitUart( const IBUTTON_DEVICE *dev, unsigned iButtonBRRValue ) 
{
    unsigned char *pUartBase = dev->pbUartBase;
    UINT16 tempLCR;
    unsigned char *base_addr = dev->pbUartBase;
    unsigned temp32;

	//Enable UART0 clock
    ESAL_GE_MEM_WRITE32(AM335X_CM_WKUP_BASE + AM335X_CM_WKUP_UART0_CLKCTRL, 0x00000002);
	HALDelayTimerTicks (1);  //TODO add check instead of delay
	
    /* Disable UART interrupts */
    ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTIER_OFFSET, AM3X_UARTIER_DISINT);

    /* Force modem control register inactive */
    ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTMCR_OFFSET, AM3X_UARTMCR_FORCE_INACTIVE);

	ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTLCR_OFFSET, AM3X_UARTLCR_8_BIT_WORD);
    
    /* Disable the UART */
    ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTMDR_OFFSET, AM3X_UARTMDR_RESET_MODE);

    /* Enable FIFO */
    ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTFCR_OFFSET, (AM3X_UARTFCR_RXTRIG_1 | AM3X_UARTFCR_FIFOEN));

	/* Disable wakeup */
	ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTWER_OFFSET, 0x00); 

	/* Enable EFR Enhance functions */
	tempLCR = ESAL_GE_MEM_READ32(base_addr + AM3X_UARTLCR_OFFSET);
	ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTLCR_OFFSET, 0x00BF);
    ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTEFR_OFFSET, AM3X_UARTEFR_ENHANCE_FEATURE);
	ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTLCR_OFFSET, 0x00BE);
    
	/* Enable access to TLR and TCR registers */
    ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTMCR_OFFSET, AM3X_UARTMCR_TCR_TLR);

    /* Set 56 byte buffer for Transmit and Receive & set Restart/Halt buffers */

    ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTTLR_OFFSET, AM3X_UARTTLR_SET_TLR_BUFFERS);
    ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTSCR_OFFSET,
               ESAL_GE_MEM_READ32(base_addr + AM3X_UARTSCR_OFFSET) | 0x88); //Enables granularity of 1 for RX level, THR when FIFO and Shift reg empty
    ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTTCR_OFFSET, (AM3X_UARTTCR_HALT_LEVEL | AM3X_UARTTCR_RESTART_LEVEL));

    /* Put in Config mode - Disable Enhancement feature - put back in operational */

    ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTMCR_OFFSET,
               ESAL_GE_MEM_READ32(base_addr + AM3X_UARTMCR_OFFSET) & ~(AM3X_UARTMCR_TCR_TLR));
	ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTLCR_OFFSET, 0x00BF);
    ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTEFR_OFFSET,
               ESAL_GE_MEM_READ32(base_addr + AM3X_UARTEFR_OFFSET) & ~(AM3X_UARTEFR_ENHANCE_FEATURE));
    
	ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTLCR_OFFSET, tempLCR);

    /* Clear all pending interrupts */
    while (ESAL_GE_MEM_READ32(base_addr + AM3X_UARTLSR_OFFSET) & AM3X_UARTLSR_RX_DATA_RDY)
    {
        /* Flush the Receive Holding Register */
        temp32 = ESAL_GE_MEM_READ32(base_addr + AM3X_UARTRHR_OFFSET);
    }

    /* Write to the divisor latch bit to enable the DLH and DLL registers */
    ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTLCR_OFFSET, 
				ESAL_GE_MEM_READ32(base_addr + AM3X_UARTLCR_OFFSET) | AM3X_UARTLCR_DIV_EN);	

	ESAL_GE_MEM_WRITE32(pUartBase + AM3X_UARTDLL_OFFSET, BRRRESET & 0xFF); 
	ESAL_GE_MEM_WRITE32(pUartBase + AM3X_UARTDLH_OFFSET, (BRRRESET >> 8) & 0xFF);

    /* Disable the Divisor Latch bit (Put in Operational mode) */
    ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTLCR_OFFSET,
				ESAL_GE_MEM_READ32(base_addr + AM3X_UARTLCR_OFFSET) & ~(AM3X_UARTLCR_DIV_EN));

    
	ESAL_GE_MEM_WRITE32(ESAL_PR_INT_CNTRL_BASE_ADDR + 0x220, 0x14);
	ESAL_GE_MEM_WRITE32(ESAL_PR_INT_CNTRL_BASE_ADDR + 0xC8, 0x00000100);
	
	/* Set the serial port to UART mode  */
    ESAL_GE_MEM_WRITE32(base_addr + AM3X_UARTMDR_OFFSET, AM3X_UARTMDR_UART_MODE);
}

//*****************************************************************************
// FUNCTION NAME:       fniButtonResetResponseData
// DESCRIPTION:
//      Resets the Total Response length to 0.  
//      This allows for finer control, since some messages must now be 
//      pieced together from more than one response.
// ARGUMENTS:
//      pDev - Pointer to the device structure, from which all low-level data 
//              can be found.  (Not sure why we pass this all over the place, 
//              since there is only one, but all the functions that need it,
//              pass it.)
// RETURNS:
//      Nothing.
// NOTES: 
// HISTORY:
//  2009.02.03    Clarisa Bellamy - New function. 
//---------------------------------------------------------------------------*/
void fniButtonResetResponseData( IBUTTON_DEVICE *dev )
{
    dev->Link.iButtonRxResponseLength = 0;
    // Clear the overrun flag.
    fnIbuttonClrFlagOverrun( dev );

    return;
}


/******************************************************************************/
/*                                                                            */
/*  Function Name: iButtonGetResponseData                                     */
/*                                                                            */
/*  Author: Tom Dometios                                                      */
/*                                                                            */
/*  Function Description:                                                     */
/*      Copies the response part of the rx data buffer into the response buf  */
/*                                                                            */
/*  Inputs:                                                                   */
/*                                                                            */
/*  Outputs:                                                                  */
/*                                                                            */
//  NOTES:
//  1.  In the Link layer structure, the RxData is the raw data from the 
//      i-button, including the transmitted data that may have been echoed 
//      back during transmission of the command.  This buffer is only 
//      256 bytes long.  The RxResponse, is the actual response, the data 
//      read while the transmission 0xFFs were being sent.  It is longer to
//      allow for the accumulation of multiple reads.
/*                                                                            */
/*  Returns:                                                                  */
/*                                                                            */
/******************************************************************************/
void iButtonGetResponseData( IBUTTON_DEVICE *dev )
{
    UINT8       *tempBuff;
    UINT8       *byteBuff;
    UINT16      wRxDataLen;

    // tempBuff will point to where in the Rx Data Buff the response data starts
    tempBuff = &dev->Link.iButtonRxDataBuff[ dev->Link.iButtonRxResponseOffset ];
    byteBuff = dev->Link.iButtonRxResponseBuff;

    // Calculate the length to convert, and store it.
    wRxDataLen = dev->Link.iButtonRxDataLength - dev->Link.iButtonRxResponseOffset;
    dev->Link.iButtonRxResponseLength = wRxDataLen;

    // Thanks to dS2480, convert is just copy: 
    // Copy the response into the response buffer
    memcpy( byteBuff, tempBuff, wRxDataLen );

#ifdef IB_DRIVER_DEBUG
    // Start a new line in the diag buffer...
    fnIButtonDiagBuffAdd( TRUE, "Rx:", 3, FALSE ); 
    // Copy response into the Diagnostic buffer too.
    fnIButtonDiagBuffAdd( FALSE, tempBuff, wRxDataLen, TRUE ); 
#endif

   return;
}

//*****************************************************************************
// FUNCTION NAME:       fniButtonGetExtendedResponse
// DESCRIPTION:
//      The boot loader messages do not use the I/O buffer, so they will use 
//      the extended read command.  Immediately after the command is sent, the
//      i-button replies with the length, then the host sends that many 0xFFs
//      to read the reply.  We do it in 256-byte blocks because of the size-limit
//      of the RxData buffer.
//      This is called instead of iButtonGetResponseData, to accumulate the 
//      successive Responses from the RxData buffer into the RxResponse buffer.
// ARGUMENTS:
//      pDev - Pointer to the device structure, from which all low-level data 
//              can be found.  (Not sure why we pass this all over the place, 
//              since there is the only one, but all the functions that need 
//              it, pass it.)
// RETURNS:
//      None.  .
// NOTES: 
//  1.  In the Link layer structure, the RxData is the raw data from the 
//      i-button, including the transmitted data that may have been echoed 
//      back during transmission of the command.  This buffer is only 
//      256 bytes long.  The RxResponse, is the actual response, the data 
//      read while the transmission 0xFFs were being sent.  It is longer to
//      allow for the accumulation of multiple reads.
// HISTORY:
//  2009.02.09    Clarisa Bellamy - New function. 
//---------------------------------------------------------------------------*/
void fniButtonGetExtendedResponse( IBUTTON_DEVICE *dev )
{
    //UINT16      i;
    UINT8       *pTempBuff;
    UINT8       *pRspDest;
    UINT16      wRxDataLen;

    // Set pTempBuff to actual response (first byte was "read" while command 
    //  byte was written.)
    pTempBuff = &dev->Link.iButtonRxDataBuff[ dev->Link.iButtonRxResponseOffset ];
    // Destination is at the end of the response data already stored.
    pRspDest = dev->Link.iButtonRxResponseBuff + dev->Link.iButtonRxResponseLength;

    // Calculate the length of the data that was just read.
    wRxDataLen = dev->Link.iButtonRxDataLength - dev->Link.iButtonRxResponseOffset;
    // Update the total Rx'd response length with the length just received.
    dev->Link.iButtonRxResponseLength += wRxDataLen;

    // Copy the response into the accumulated-response buffer
    memcpy( pRspDest, pTempBuff, wRxDataLen );

#ifdef IB_DRIVER_DEBUG
    // Start a new line in the diag buffer... Get Extended Read data.
    fnIButtonDiagBuffAdd( TRUE, "RxX:", 4, FALSE ); 
    // Copy it into the Diagnostic buffer too.
    fnIButtonDiagBuffAdd( FALSE, pTempBuff, wRxDataLen, TRUE ); 
#endif

   return;

}

//*****************************************************************************
// FUNCTION NAME:       fnIButtonDiagBuffAdd
// DESCRIPTION:
//      Loads data into the ibutton diagnostic buffer.
// ARGUMENTS:
//  fNewLine - TRUE if we want this data to start on a "new line", sets the 
//                  Index to a multiple of 8.
//  pSrc - Pointer to the data to write to the buffer.
//  uwLen - Number of bytes of data to copy to the buffer.
//  fCheckXtrWrap - If TRUE, then this data is only part of a bigger message,
//          and we want to check for the wrap condition where this part of the 
//          message fits, but the next 2 bytes would not.
// RETURNS:
//      None.
// NOTES: 
//  1.  
// HISTORY:
//  2009.08.13    Clarisa Bellamy - New function. 
//---------------------------------------------------------------------------*/
#ifdef IB_DRIVER_DEBUG
#define XTR_WRAP_LEN    2
void fnIButtonDiagBuffAdd( BOOL fNewLine, void *pSrc, UINT16 uwLen, BOOL fCheckXtrWrap )
{
    UINT16  uwTempVal;
    UINT16  uwRemainingLen;
    UINT8   *pByteSrc;
    UINT16  uwTestLen;

    pByteSrc = pSrc;
    // If the message is too long, don't bother to record it.
    if( uwLen > SIZEOF_IBTNDIAGBUF - 8 )
    {
        pSrc = "MsgTooLong";
        uwLen = strlen( pSrc );
    }    

    if( fNewLine )
    {
        // If there is not at least 1 line left, wrap to the beginning.
        if( iButtonDiagBufIndex > (SIZEOF_IBTNDIAGBUF - 8) ) 
        { 
            memset( &iButtonDiagBuf[iButtonDiagBufIndex], '_', 
                    (SIZEOF_IBTNDIAGBUF - iButtonDiagBufIndex) );
            iButtonDiagBufIndex = 0;
        }
        else
        {
            // Start the next Rx'd data block on a new line.
            uwTempVal = iButtonDiagBufIndex % 8;
            if( uwTempVal ) 
                iButtonDiagBufIndex += ((~uwTempVal) & 0x07) + 1;
        }
    }
    
    // Set the length that we'll add to the buffer.
    uwRemainingLen = uwLen;
    uwTestLen = iButtonDiagBufIndex + uwLen;
    if(   (uwTestLen > SIZEOF_IBTNDIAGBUF)
       || (   (fCheckXtrWrap == TRUE) 
           && (uwTestLen > (SIZEOF_IBTNDIAGBUF - XTR_WRAP_LEN)) ) )
    {
        // If there is more data than space remaining in the buffer,
        //  then we need to wrap.
        if(  (SIZEOF_IBTNDIAGBUF - iButtonDiagBufIndex) > XTR_WRAP_LEN )
        {
            // If there is more than 2 bytes left, put some stuff 
            //  at the end, before wrapping.  Adjust the 
            uwTempVal = (SIZEOF_IBTNDIAGBUF - iButtonDiagBufIndex) - XTR_WRAP_LEN;
            memcpy( &iButtonDiagBuf[ iButtonDiagBufIndex ], pSrc, uwTempVal );
            iButtonDiagBufIndex += uwTempVal;
            uwRemainingLen = uwLen - uwTempVal;
        }
        // If there is at least one byte left, fill it up with a known value.
        while( iButtonDiagBufIndex < SIZEOF_IBTNDIAGBUF )
        {
            iButtonDiagBuf[ iButtonDiagBufIndex++ ] = '*';
        }
        // Wrap, and give an indication that we wrapped...
        iButtonDiagBufIndex = 0;
        iButtonDiagBuf[ iButtonDiagBufIndex++ ] = 'w';
        iButtonDiagBuf[ iButtonDiagBufIndex++ ] = 'r';
        iButtonDiagBuf[ iButtonDiagBufIndex++ ] = 'a';
        iButtonDiagBuf[ iButtonDiagBufIndex++ ] = 'p';
    }
    // Write the (remaining) data to the buffer, and update the Index:
    memcpy( &iButtonDiagBuf[ iButtonDiagBufIndex ], 
            &pByteSrc[ uwLen - uwRemainingLen ], uwRemainingLen );
    iButtonDiagBufIndex += uwRemainingLen;

    return;
}
#endif
