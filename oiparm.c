/************************************************************************
	PROJECT:		Future Phoenix
	COPYRIGHT:		2005, Pitney Bowes, Inc.
	AUTHOR:			
	MODULE NMAE:	OiParm.c

	DESCRIPTION:	Contains the OI parmeter compare engine and related
					functions.  Parameter comparisons are used to qualify
					hard key and event records so that the tree component
					can have custom behavior for different PCN configurations
					or system states.

 	MODIFICATION HISTORY:
 
 02-Dec-16 sa002pe on FPHX 02.12 shelton branch
 	Merging in a change from K7 as a preventative measure
----*
	* 02-Dec-16 sa002pe on K7 tips shelton branch
	*	Changed fnCheckKeyOrEventParameter to return SUCCESSFUL if the parameter ID is zero.
----*


 	07/29/2009  Jingwei,Li      Add a new flag to indicate that the fees detail screen comes 
 	                            from the review selection screen.
    07/23/2007  Vincent Yi      Fixed lint errors
	03/28/2007	Vincent Yi		Added P_AB_LIST_SCR_PUR
	01/19/2007  Oscar Wang      Added P_FROM_MANUFACTURING.
 	12/14/2006  Adam Liu        Add P_DCAP_ACTIVATE and merged function
 	                            fnpfDCAPIsActive() from K700.
	07/07/2006  Dicky Sun       Add P_DEST_ZZC_ENTRY_MODE, P_E_SERVICE_SELECTED
	                            and function fnpfEServiceSelected for GTS.
	06/22/2006	Raymond Shen	Added P_OOB_MODE and function fnpfIsInOOBMode().
	OLD PVCS REVISION HISTORY
 *    Rev 1.17   02 Dec 2004 10:26:46   CX17605
 * Added a new parameter P_CREDIT_METER.  -David 

*************************************************************************/


/**********************************************************************
		Include Header Files
**********************************************************************/
//#include "oiabacus5.h"
#include "oicommon.h"
#include "oifunctfp.h"
#include "oifpprint.h"
#include "oiparm.h"
#include "oit.h"
//#include "oioob.h"
//#include "oiweight.h"
#include "datdict.h"
#include "fwrapper.h"


/**********************************************************************
		Local Function Prototypes
**********************************************************************/
//extern UINT8	ucCurrentTextMode;
extern UINT8   ucDestZZCEntryMode;	   // Notes: It is only public for the benefit of oiparm.c.
extern BOOL fIsFromManufacturing;
extern BOOL fCanadaPackServFlag;


static BOOL fnpfBaseType (UINT16 *pwParm);
static BOOL fnpfTapePrintCompleteToReady (UINT16 *pwParm);
static BOOL fnpfKeyInPostageModeOK(UINT16 *pwParm);
static BOOL fnpfResumePrint(UINT16 *pwParm);
static BOOL fnpfIsInOOBMode(UINT16 *pwParm);

static BOOL fnpfEServiceSelected(UINT16 *pwParm);
static BOOL fnpfDCAPIsActive(UINT16 *pwParm);

static BOOL fnpfAbListScrPurpose (UINT16 * pwParm);

/**********************************************************************
		Local Defines, Typedefs, and Variables
**********************************************************************/
static const PARM_FETCH_TABLE	pParmFetchTable[] =
		// parameter constant			retrieval type    	variable or fcn addr
{
		{ P_BASE_TYPE, 					P_FUNCTION,			fnpfBaseType		},
		{ P_TAPE_PRINT_TO_READY, 		P_FUNCTION,			fnpfTapePrintCompleteToReady},
		{ P_MODE_MANUAL,				P_FUNCTION,			fnpfKeyInPostageModeOK},
		{ P_RESUME_PRINT,				P_FUNCTION,			fnpfResumePrint},
//		{ P_TEXT_MODE,					P_BYTE,				&ucCurrentTextMode	},
		{ P_OOB_MODE,					P_FUNCTION,			fnpfIsInOOBMode	},
//		{ P_DEST_ZZC_ENTRY_MODE,        P_BYTE,     	    &ucDestZZCEntryMode },
		{ P_E_SERVICE_SELECTED,         P_FUNCTION,     	fnpfEServiceSelected }, 
		{ P_DCAP_ACTIVATE,              P_FUNCTION, 	    fnpfDCAPIsActive },
		{ P_FROM_MANUFACTURING,             P_BYTE,     	&fIsFromManufacturing},
		{ P_AB_LIST_SCR_PUR,			P_FUNCTION,			fnpfAbListScrPurpose },
		{ P_CANADA_PACKAGE_SERVICE_FLAG,             P_BYTE,     	& fCanadaPackServFlag },
		{ 0,	P_END_OF_TABLE,	(void *) 0 }
};




/**********************************************************************
		Global Functions Prototypes
**********************************************************************/



/**********************************************************************
		Global Variables
**********************************************************************/



/**********************************************************************
		Public Functions
**********************************************************************/
/* Utility functions */

/* **********************************************************************
// FUNCTION NAME: fnCheckKeyOrEventParameter
// DESCRIPTION: Checks if the parameter criteria associated with a hard
//				key record or event record are satisfied.
// AUTHOR: Joe Mozdzer
//
// INPUTS:	bParmID - ID number of the parameter.  the parameters are
//						enumerated in oifunct.h
//
// 			bCompType - Comparison type.  Bit 7 indicates if we are using
//						a 1 (0) or 2 (1) byte operand.  Bits 2-0 indicate
//						the type of comparison as follows:
//							PCT_NO_COMPARE - No operation
//							PCT_EQUAL - Equal (embedded value must equal operand)
//							PCT_NOT_EQUAL - Not equal
//							PCT_LESS_THAN - Less than (embedded value less than operand)
//							PCT_GREATER_THAN - Greater than
//							PCT_AND_EQ_0 - (logical AND of embedded val & operand must = 0)
//							PCT_OR_EQ_F - (logical OR must equal FF or FFFF depending on
//										   operand size)
//
//			wOperand - The operand we are comparing the embedded value against.
//
//			pResult - The result is returned here.  TRUE means that the parameter
//						criteria is satisfied.  FALSE means it isn't.  This value
//						is valid only if SUCCESSFUL is returned by the function.
//
// RETURNS:	SUCCESSFUL-	The parameter was successfully evaluated and the result
//							of the comparison is returned in pResult
//			FAILURE- The parameter ID or comparison type was invalid.
//
// *************************************************************************/
BOOL fnCheckKeyOrEventParameter(unsigned char bParmID, unsigned char bCompType,
								unsigned short wOperand, BOOL *pResult)
{
	BOOL			fRetval = FAILURE;
	unsigned long	i;
	unsigned short	wEmbedded;
	unsigned char	bOpSize;
	unsigned char	bCType;
	

	bCType = bCompType & 0x07;

    /* if the comparison type is "no compare" or the parameter ID is "do nothing", the function always succeeds */
    if ((bCType == PCT_NO_COMPARE) || (bParmID == ParameterDoNothing))
	{
		*pResult = TRUE;
		fRetval = SUCCESSFUL;
		goto xit;
	}

	/* look for the parameter ID in the parameter fetch table */
	for (i = 0; pParmFetchTable[i].bRetrievalType != P_END_OF_TABLE; i++)
		if (pParmFetchTable[i].bParmID == bParmID)
			break;

	/* if we didn't find it return an error */
	if (pParmFetchTable[i].bRetrievalType == P_END_OF_TABLE)
		goto xit;

	/* get the embedded value associated with the parameter.  this may be either
		an immediate variable lookup or a function may have to be called */
	switch (pParmFetchTable[i].bRetrievalType)
	{
		case P_BYTE:
			wEmbedded = * (unsigned char *) pParmFetchTable[i].pDataOrFn;
			break;

		case P_WORD:
			memcpy(&wEmbedded, pParmFetchTable[i].pDataOrFn, sizeof(unsigned short));
			break;

		case P_FUNCTION:
			if (((pParmFn) pParmFetchTable[i].pDataOrFn) (&wEmbedded) != SUCCESSFUL)
				goto xit;
			break;

		default:
			goto xit;
	}

	/* get the comparison operand size */
	bOpSize = (bCompType & 0x80) ? 2 : 1;

	/* zero out the high byte if we are using a 1 byte operand.  this simplifies
		the comparison in all cases except the OR = FF one. */
	if (bOpSize == 1)
	{
		wOperand = wOperand & 0xFF;
		wEmbedded = wEmbedded & 0xFF;
	}

	/* do the comparison and fill in the return result */
	switch (bCType)
	{
		case PCT_EQUAL:
			if (wOperand == wEmbedded)
				*pResult = TRUE;
			else
				*pResult = FALSE;

			break;

		case PCT_NOT_EQUAL:
			if (wOperand != wEmbedded)
				*pResult = TRUE;
			else
				*pResult = FALSE;

			break;

		case PCT_LESS_THAN:
			if (wEmbedded < wOperand)
				*pResult = TRUE;
			else
				*pResult = FALSE;

			break;

		case PCT_GREATER_THAN:
			if (wEmbedded > wOperand)
				*pResult = TRUE;
			else
				*pResult = FALSE;

			break;

		case PCT_AND_EQ_0:
			if ((wEmbedded & wOperand) == 0)
				*pResult = TRUE;
			else
				*pResult = FALSE;

			break;

		case PCT_OR_EQ_F:
			if (bOpSize == 1)
			{
				wEmbedded = wEmbedded | 0xFF00;
				wOperand = wOperand | 0xFF00;
			}
			
			if ((wEmbedded | wOperand) == 0xFFFF)
				*pResult = TRUE;
			else
				*pResult = FALSE;

			break;

		default:	/* return failure if none of the above */
			goto xit;
		
	}

	fRetval = SUCCESSFUL;

xit:		
	return (fRetval);
}




/**********************************************************************
		Private Functions
**********************************************************************/

/* Parameter functions */

/* *************************************************************************
// FUNCTION NAME: 
//		fnpfBaseType
//
// DESCRIPTION: 
// 		Parameter function that check the base type
//
// INPUTS:
//		pwParm	- the parameter value is returned at this address.  
//				  The parameter is returned as a word regardless of whether 	
//				  one or two bytes are really used. 
//
// OUTPUTS:
//		*pwParm	-  	OIT_BASE_300C			1
//					OIT_BASE_400C			2
//
// RETURNS:
//		 Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	12/5/2005	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnpfBaseType (UINT16 *pwParm)
{
	*pwParm = fnOITGetBaseType();
	return ((BOOL) SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpfTapePrintCompleteToReady
//
// DESCRIPTION: 
// 		Parameter function that check whether screen can transition to Home 
//		Ready after tape print completed.
//
// INPUTS:
//		pwParm	- the parameter value is returned at this address.  
//				  The parameter is returned as a word regardless of whether 	
//				  one or two bytes are really used. 
//
// OUTPUTS:
//		*pwParm	-  1, can transition to Home Ready screen
//				   0, can not.
//
// RETURNS:
//		 Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	12/1/2005	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnpfTapePrintCompleteToReady (UINT16 *pwParm)
{
	if (fnTapePrintCompleteToReady() == SUCCESSFUL)
	{
		*pwParm = 1;
	}
	else
	{
		*pwParm = 0;
	}

	return ((BOOL) SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: 
//		fnpfDCAPIsActive
//
// DESCRIPTION: 
// 		Determines if a data capture is active.
//              0 is returned if data capture not active
//              1 is returned if data capture is active
//
// INPUTS:
//		pwParm	- the parameter value is returned at this address.  
//				  The parameter is returned as a word regardless of whether 	
//				  one or two bytes are really used. 
//
// OUTPUTS:
//		*pwParm	-  1, DCAP is active
//				   0, not.
//
// RETURNS:
//		 Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
//  12/14/2006    Adam Liu                     Merged from K700 and Code cleanup
// 		          Joe Mozdzer, Craig DeFilippo	Initial version
//
// *************************************************************************/
BOOL fnpfDCAPIsActive(UINT16 *pwParm)
{
#if 0 //TODO JAH remove dcap 
	if (fnDCAPIsDataCaptureActive() == TRUE)
        *pwParm = 1;
    else
#endif //TODO JAH remove dcap 
        *pwParm = 0;
    return (SUCCESSFUL);
}


/* *************************************************************************
// FUNCTION NAME: 
//		fnpfKeyInPostageModeOK
//
// DESCRIPTION: 
// 		Parameter function that check whether current print mode is key in 
//		postage mode
//
// INPUTS:
//		pwParm	- the parameter value is returned at this address.  
//				  The parameter is returned as a word regardless of whether 	
//				  one or two bytes are really used. 
//
// OUTPUTS:
//		*pwParm	-  1, in KIP mode
//				   0, not.
//
// RETURNS:
//		 Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	12/1/2005	Vincent Yi		Initial version
//
// *************************************************************************/
BOOL fnpfKeyInPostageModeOK(UINT16 *pwParm)
{
	if (fnOITGetPrintMode() == PMODE_MANUAL)
		*pwParm = 1;
	else
		*pwParm = 0;

	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpfResumePrint
//
// DESCRIPTION: 
// 		Parameter function that check whether resume printing or cancel it.
//
// INPUTS:
//		pwParm	- the parameter value is returned at this address.  
//				  The parameter is returned as a word regardless of whether 	
//				  one or two bytes are really used. 
//
// OUTPUTS:
//		*pwParm	-  1, resume printing
//				   0, cancel printing
//
// RETURNS:
//		 Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	03/17/2006	Vincent Yi		Initial version
//
// *************************************************************************/
static BOOL fnpfResumePrint(UINT16 *pwParm)
{
	if (fnGetResumePrintFlag() == TRUE)
		*pwParm = 1;
	else
		*pwParm = 0;

	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpfIsInOOBMode
//
// DESCRIPTION: 
// 		Parameter function that check whether it is currently in OOB mode.
//
// INPUTS:
//		pwParm	- the parameter value is returned at this address.  
//				  The parameter is returned as a word regardless of whether 	
//				  one or two bytes are really used. 
//
// OUTPUTS:
//		*pwParm	-  1, Is in OOB Mode
//				   0, Isn't in OOB Mode
//
// RETURNS:
//		 Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	05/16/2006	Raymond Shen		Initial version
//
// *************************************************************************/
static BOOL fnpfIsInOOBMode(UINT16 *pwParm)
{
/*
	if( fnIsInOOBMode() == TRUE )
	{
		*pwParm = 1;
	}
	else
	{
		*pwParm = 0;
	}
*/
	return (SUCCESSFUL);
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpfEServiceSelected
//
// DESCRIPTION: 
// 		Parameter function that check whether EService is selected.
//
// INPUTS:
//		pwParm	- the parameter value is returned at this address.  
//				  The parameter is returned as a word regardless of whether 	
//				  one or two bytes are really used. 
//
// OUTPUTS:
//		*pwParm	-  1, resume printing
//				   0, cancel printing
//
// RETURNS:
//		 Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	07/07/2006	Dicky Sun		Initial version
// *************************************************************************/
static BOOL fnpfEServiceSelected(UINT16 *pwParm)
{

    *pwParm = 0;

    return( SUCCESSFUL );
}

/* *************************************************************************
// FUNCTION NAME: 
//		fnpfAbListScrPurpose
//
// DESCRIPTION: 
// 		Parameter function that check the purpose of Abacus List screen
//
// INPUTS:
//		pwParm	- the parameter value is returned at this address.  
//				  The parameter is returned as a word regardless of whether 	
//				  one or two bytes are really used. 
//
// OUTPUTS:
//		*pwParm	-  as the definition as following

			//List Screen Purpose
				#define  ABACUS_ACT_LIST_INDEX       		1
				#define  ABACUS_ACT_LIST_LEVEL       		2
			//for Host mode
				#define  ABACUS_ACT_LIST_HOST     		   	3
			//for Report selection
				#define  ABACUS_ACT_LIST_RPT_TOP        	4
				#define  ABACUS_ACT_LIST_RPT_SUB        	5
				#define  ABACUS_ACT_LIST_RPT_SUBSUB     	6
//
//
// RETURNS:
//		 Always SUCCESSFUL
//
// WARNINGS/NOTES:
//
// MODIFICATION HISTORY:
// 	07/07/2006	Dicky Sun		Initial version
// *************************************************************************/
static BOOL fnpfAbListScrPurpose (UINT16 * pwParm)
{
//	*pwParm = fnOiAbGetListScrPurpose ();
    return (SUCCESSFUL);
}
