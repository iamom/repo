/*
*********************************************************************
*********************************************************************
CLASSIFICATION:                        PITNEY BOWES RESTRICTED
IDENTITY:                              
TITLE:                                 varassert.c
COPYRIGHT PITNEY BOWES LIMITED 2005
REQUIREMENT:                           
HISTORY:
         DATE      ISSUE      AUTHOR         PR     SYNOPSIS

      
      INTERFACE:

        CLASSES:
                

    DESCRIPTION: 

        implements the varassert function that allows us to use assert 
        of the form :-

            varassert( a == b, "a=%d b=%d", a, b);

        which will case an output like 
          "Assert Failure File xxxx line yy: a === b: a=12 b=13"

------------------------------------------------------------------------
*/ 


#include <stdio.h>
#include <stdarg.h>
#include <assert.h>
#include "varassert.h"


#ifndef NDEBUG
char varassert_buf[512];

void __varassert(const char *file, int line, const char *func, const char *s_cond, const char *fmt, ...)
{
    char *p_pos;
    va_list argp;

    p_pos = varassert_buf;
    p_pos += sprintf(varassert_buf, "%s: ",s_cond);

    va_start(argp, fmt);
    vsprintf(p_pos, fmt, argp);
    va_end(argp);

    __assert_func(file, line, func, varassert_buf);
}


#endif

/*
 *===========================================================================
 *
 *  The information in this section will be part of PVCS tracking information
 *
 *  $Workfile:   varassert.c  $
 *
 *  $Revision:   1.0  $
 *
 *  $Log:   W:/Pvcs-Arc/Nexus/archives/wave2/Common/src/varassert.c-arc  $
 * 
 *    Rev 1.0   Oct 26 2005 16:58:04   vi200ro
 * Initial revision.
 *
 *===========================================================================
*/

/* ==============================================================[ EOF ]== */

